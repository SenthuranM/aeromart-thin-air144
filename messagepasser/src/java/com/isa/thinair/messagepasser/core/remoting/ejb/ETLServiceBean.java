package com.isa.thinair.messagepasser.core.remoting.ejb;

import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Command;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.messagepasser.api.dto.common.MessageConfigDTO;
import com.isa.thinair.messagepasser.api.dto.etl.ETLProcessPaxResponseDTO;
import com.isa.thinair.messagepasser.api.dto.etl.LogDTO;
import com.isa.thinair.messagepasser.api.model.CodeShareETL;
import com.isa.thinair.messagepasser.api.model.ETL;
import com.isa.thinair.messagepasser.api.model.ETLPaxEntry;
import com.isa.thinair.messagepasser.api.utils.MessagePasserModuleUtils;
import com.isa.thinair.messagepasser.api.utils.ParserConstants.MessageTypes;
import com.isa.thinair.messagepasser.core.bl.ETLPRLFileNamer;
import com.isa.thinair.messagepasser.core.bl.ETLServices;
import com.isa.thinair.messagepasser.core.bl.MailError;
import com.isa.thinair.messagepasser.core.bl.MessagePasserDAOUtils;
import com.isa.thinair.messagepasser.core.config.MessagePasserConfig;
import com.isa.thinair.messagepasser.core.persistence.dao.ETLDAO;
import com.isa.thinair.messagepasser.core.service.bd.ETLBDImpl;
import com.isa.thinair.messagepasser.core.service.bd.ETLBDLocalImpl;
import com.isa.thinair.messagepasser.core.util.ParserUtil;
import com.isa.thinair.messagepasser.core.utils.CommandNames;
import com.isa.thinair.platform.api.ServiceResponce;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.ejb.TransactionTimeout;
import org.jboss.annotation.security.SecurityDomain;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;
import java.text.ParseException;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * Session bean to provide ETL Processing Service related functionalities
 * 
 * @author Ishan
 */
@Stateless
@RemoteBinding(jndiBinding = "ETLService.remote")
@LocalBinding(jndiBinding = "ETLService.local")
@RolesAllowed("user")
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class ETLServiceBean extends PlatformBaseSessionBean implements ETLBDImpl, ETLBDLocalImpl {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(ETLServiceBean.class);

	/**
	 * Reconcile ETL Reservations
	 * 
	 * @param etlId
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ServiceResponce reconcileETLReservations(int etlId, boolean isConsiderFutureDateValidation) throws ModuleException {
		try {
			CredentialsDTO credentialsDTO = this.getCallerCredentials(null);
			Command command = (Command) MessagePasserModuleUtils.getBean(CommandNames.RECONCILE_E_T_L_RESERVATION);
			command.setParameter(CommandParamNames.CREDENTIALS_DTO, credentialsDTO);
			command.setParameter(CommandParamNames.ETL_ID, new Integer(etlId));
			command.setParameter(CommandParamNames.IS_CONSIDER_FUTURE_DATE_VALIDATION, isConsiderFutureDateValidation);

			return command.execute();
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::reconcileETLReservations ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::reconcileETLReservations ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Process ETL message
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@TransactionTimeout(600)
	public ServiceResponce processETLMessage() throws ModuleException {

		// One Thread at a Time
		synchronized (this) {
			final String generalLogFileName = "ETL-Operation Failure " + new Date().toString();

			Throwable exceptionToLog = null;
			HashMap<String, Throwable> fileExceptionMap = new HashMap<String, Throwable>();
			Collection<String> colMsgNames = null;
			boolean isRollBack = false;

			log.info("######### ETL PARSING : INITIALIZING OPERATION  ##########");
			MessagePasserConfig messagePasserConfig = MessagePasserModuleUtils.getMessagePasserConfig();

			String etlProcessPath = messagePasserConfig.getEtlProcessPath();
			String etlParsedPath = messagePasserConfig.getEtlParsedPath();
			String etlErrorPath = messagePasserConfig.getEtlErrorPath();
			String partialDownloadPath = messagePasserConfig.getEtlPartialDownloadPath();
			String messageType = MessageTypes.ETL.toString();

			MessageConfigDTO msgCofigDTO = new MessageConfigDTO();
			msgCofigDTO.setPopMailServer(messagePasserConfig.getPopMailServer());
			msgCofigDTO.setMailServerUsername(messagePasserConfig.getEtlMailServerUsername());
			msgCofigDTO.setMailServerPassword(messagePasserConfig.getEtlMailServerPassword());

			msgCofigDTO.setTtyMailServer(messagePasserConfig.getTtyMailServer());
			msgCofigDTO.setTtyMailServerUserName(messagePasserConfig.getTtyMailServerUserName());
			msgCofigDTO.setTtyMailServerPassword(messagePasserConfig.getTtyMailServerPassword());

			msgCofigDTO.setFetchFromMailServer(
					messagePasserConfig.getEtlFetchFromMailServer().equalsIgnoreCase("true") ? true : false);
			msgCofigDTO.setDeleteFromMailServer(
					messagePasserConfig.getEtlDeleteFromMailServer().equalsIgnoreCase("true") ? true : false);
			msgCofigDTO.setProcessPath(etlProcessPath);

			try {
				// Downloading the ETL Messages
				MessagePasserModuleUtils.getETLBD().downloadMessagesFromMailServer(msgCofigDTO, messageType);

				log.info("########################## ETL - before validating the documents ########################## ");
				colMsgNames = MessagePasserModuleUtils.getETLBD().getValidatedDocuments(etlProcessPath, etlErrorPath,
						partialDownloadPath, messageType);
				log.info("########################## ETL - after validating the documents ########################## ");
				Iterator<String> itColMsgNames = colMsgNames.iterator();
				String strMsgName;
				Integer etlId = null;
				// parse every file one by one - insert to db and get the etl id
				// for processing

				while (itColMsgNames.hasNext()) {
					strMsgName = (String) itColMsgNames.next();

					// need to log the format error in a separate file.
					try {
						log.info("### ETL - before parsed the document " + strMsgName);
						etlId = MessagePasserModuleUtils.getETLBD().parseDocument(colMsgNames, strMsgName, MessageTypes.ETL);

					} catch (ModuleException e) {
						log.error("########################## ERROR AFTER PARSING FOR " + strMsgName + e.getLocalizedMessage(),
								e);
						fileExceptionMap.put(strMsgName, exceptionToLog = e);
						continue;
					} catch (Exception e) {
						log.error("########################## ERROR AFTER PARSING FOR " + strMsgName + e.getLocalizedMessage(),
								e);
						fileExceptionMap.put(strMsgName, exceptionToLog = e);
						continue;
					}

					if (etlId != null) {

						Throwable xception = null;
						try {
							MessagePasserModuleUtils.getETLBD().reconcileETLReservations(etlId,
									AppSysParamsUtil.isAllowProcessPfsBeforeFlightDeparture());
						} catch (ModuleException e) {
							log.error(" ERROR OCCURED WHILE RECONCILING ETL ID : " + etlId + " DATE :" + new Date(), e);
							xception = e;
						} catch (Exception e) {
							log.error(" ERROR OCCURED WHILE RECONCILING ETL ID : " + etlId + " DATE :" + new Date(), e);
							xception = e;

						} finally {
							MailError.notifyError(etlId, xception, strMsgName);
						}

					}
				}

			} catch (ModuleException ex) {
				this.sessionContext.setRollbackOnly();
				isRollBack = true;
				log.error(" ((o)) ModuleException::processETLMessage ", ex);
				fileExceptionMap.put(generalLogFileName, exceptionToLog = ex);
				throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
			} catch (CommonsDataAccessException cdaex) {
				fileExceptionMap.put(generalLogFileName, exceptionToLog = cdaex);
				this.sessionContext.setRollbackOnly();
				isRollBack = true;
				log.error(" ((o)) CommonsDataAccessException::processETLMessage ", cdaex);
				throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
			} finally {

				if (exceptionToLog != null) {
					log.info("########################## ETL ERRORS EXIST STARTING TO LOG   ##########");
					Iterator<String> iter = fileExceptionMap.keySet().iterator();
					LogDTO logDTO;

					while (iter.hasNext()) {
						String fileName = (String) iter.next();
						ParserUtil.moveAndDeleteFiles(etlProcessPath, etlErrorPath, fileName);
						Throwable xceptionToLog = (Throwable) fileExceptionMap.get(fileName);

						logDTO = new LogDTO();
						logDTO.setFileName(fileName);
						logDTO.setStackTraceElements(xceptionToLog.getStackTrace());

						if (xceptionToLog instanceof ModuleException) {
							ModuleException e = (ModuleException) xceptionToLog;
							// do not send mail for this..as mail is already
							// sent.
							if (e.getExceptionCode().equals("messagepasser.etl.cannotParseETL")) {
								continue;
							}
							logDTO.setExceptionDescription(e.getMessageString());

						} else {

							logDTO.setExceptionDescription(xceptionToLog.getLocalizedMessage());
						}

						MailError.notifyError(logDTO);

					}
					log.info("########################## ETL ERRORS EXIST ... FINISHED  LOGING   ##########");
				}
				if (!isRollBack) {
					ParserUtil.moveAndDeleteAnyRemainingFiles(etlProcessPath, etlParsedPath, colMsgNames);
				}
			}
			log.info("########################## ETL PARSING : OPERATION COMPLETED ##########");

			return new DefaultServiceResponse(true);
		}
	}

	/**
	 * Returns caller credentials information
	 * 
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	private CredentialsDTO getCallerCredentials(TrackInfoDTO trackInfoDTO) throws ModuleException {
		UserPrincipal userPrincipal = this.getUserPrincipal();
		return ReservationApiUtils.getCallerCredentials(trackInfoDTO, userPrincipal);
	}

	/**
	 * Returns ETL entry paged data
	 * 
	 * @param criteria
	 * @param startIndex
	 * @param noRecs
	 * @param orderByFieldList
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Page getPagedETLData(List criteria, int startIndex, int noRecs, List orderByFieldList) throws ModuleException {
		try {
			return MessagePasserDAOUtils.DAOInstance.ETL_DAO.getPagedETLData(criteria, startIndex, noRecs, orderByFieldList);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPagedETLData ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Returns etl parse entries
	 * 
	 * @param etlId
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection getEtlParseEntries(int etlId) throws ModuleException {
		try {
			return MessagePasserDAOUtils.DAOInstance.ETL_DAO.getEtlParseEntries(etlId);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getEtlParseEntries ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Returns etl entry
	 * 
	 * @param etlID
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public ETL getETL(int etlID) throws ModuleException {
		try {
			return MessagePasserDAOUtils.DAOInstance.ETL_DAO.getETL(etlID);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPFS ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

	}

	public ETLPaxEntry getETLPaxEntry(int etlPaxId) throws ModuleException {
		return MessagePasserDAOUtils.DAOInstance.ETL_DAO.getETLPaxEntry(etlPaxId);
	}

	/**
	 * Return etl parse entries count
	 * 
	 * @param etlId
	 * @param processedStatus
	 * 
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public int getEtlParseEntryCount(int etlId, String processedStatus) throws ModuleException {
		try {
			ETLDAO etlDAO = MessagePasserDAOUtils.DAOInstance.ETL_DAO;
			return etlDAO.getEtlParseEntriesCount(etlId, processedStatus);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPfsParseEntryCount ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Integer parseDocument(Collection<String> msgNamesThatMayHavePartTwoThree, String strMsgName, MessageTypes messageType)
			throws ModuleException,ParseException{
		try {
			return ParserUtil.parseDocument(msgNamesThatMayHavePartTwoThree, strMsgName, messageType);
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::parseDocument ", ex);
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::parseDocument ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<String> getValidatedDocuments(String processPath, String errorPath, String partialDownloadPath,
			String messageType) {
		return ParserUtil.getValidatedDocuments(processPath, errorPath, partialDownloadPath, messageType);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void downloadMessagesFromMailServer(MessageConfigDTO msgCofigDTO, String messageType) throws ModuleException {
		ParserUtil.downloadMessagesFromMailServer(msgCofigDTO, messageType, new ETLPRLFileNamer(MessageTypes.ETL));
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ETLProcessPaxResponseDTO processETLPaxEntry(ETLPaxEntry etlPaxEntry, ETL etl) throws ModuleException {
		try {
			return (new ETLServices()).processETLPaxEntry(etlPaxEntry, etl);
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::processETLPaxEntry", ex);
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::processETLPaxEntry ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	public void saveETLParseEntry(ETLPaxEntry etlPaxEntry) throws ModuleException {
		MessagePasserDAOUtils.DAOInstance.ETL_DAO.saveETLPaxEntry(etlPaxEntry);
	}

	public void deleteETLPaxEntry(int etlPaxId) throws ModuleException {
		MessagePasserDAOUtils.DAOInstance.ETL_DAO.deleteETLPaxEntry(etlPaxId);
	}

	public void saveETLEntry(ETL etl) throws ModuleException {
		MessagePasserDAOUtils.DAOInstance.ETL_DAO.saveETLEntry(etl);
	}

	public boolean hasETL(String flightNumber, Date departureDate, String airport, Collection<String> processedStatus) {
		return MessagePasserDAOUtils.DAOInstance.ETL_DAO.hasETL(flightNumber, departureDate, airport, processedStatus);
	}

	public void updateETLEntry(ETL etl) {
		MessagePasserDAOUtils.DAOInstance.ETL_DAO.updateETLEntry(etl);
	}

	@Override
	public void getEtlPrlFilesViaSitaTex(List<String> messages, String messageType) throws ModuleException {
		ParserUtil.getEtlPrlFilesViaSitaTex(messages, messageType);
	}

	@Override
	public void saveCodeShareETLEntry(CodeShareETL etl) throws ModuleException {
		MessagePasserDAOUtils.DAOInstance.ETL_DAO.saveCodeShareETLEntry(etl);
	}	
	
}