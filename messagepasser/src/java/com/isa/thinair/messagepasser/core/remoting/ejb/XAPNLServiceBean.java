/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.messagepasser.core.remoting.ejb;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Command;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.messagepasser.api.dto.emirates.XAPnlLogDTO;
import com.isa.thinair.messagepasser.api.service.XApnlBD;
import com.isa.thinair.messagepasser.api.utils.MessagePasserModuleUtils;
import com.isa.thinair.messagepasser.core.bl.MessagePasserDAOUtils;
import com.isa.thinair.messagepasser.core.bl.Pop3Client;
import com.isa.thinair.messagepasser.core.bl.emirates.PNLDTOParser;
import com.isa.thinair.messagepasser.core.bl.emirates.XAMailError;
import com.isa.thinair.messagepasser.core.bl.emirates.XAPNLFormatUtils;
import com.isa.thinair.messagepasser.core.bl.emirates.XAPnlReconcilationLogger;
import com.isa.thinair.messagepasser.core.config.MessagePasserConfig;
import com.isa.thinair.messagepasser.core.service.bd.XApnlBDImpl;
import com.isa.thinair.messagepasser.core.service.bd.XApnlBDLocalImpl;
import com.isa.thinair.messagepasser.core.utils.CommandNames;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Session bean to provide XA-PNL Reservation Service realted functionalities
 * 
 * @author Dhanushka Ranatunga 
 */
@Stateless
@RemoteBinding(jndiBinding = "XAPNLService.remote")
@LocalBinding(jndiBinding = "XAPNLService.local")
@RolesAllowed("user")
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class XAPNLServiceBean extends PlatformBaseSessionBean implements XApnlBDImpl, XApnlBDLocalImpl {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(XAPNLServiceBean.class);

	/**
	 * Reconcile XA PNL Reservations
	 * 
	 * @param xaPnlId
	 * @return
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ServiceResponce reconcileXAPnlReservations(int xaPnlId) throws ModuleException {
		try {
			Command command = (Command) MessagePasserModuleUtils.getBean(CommandNames.RECONCILE_X_A_PNL_RESERVATION);
			command.setParameter(CommandParamNames.XA_PNL_ID, new Integer(xaPnlId));

			return command.execute();
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::reconcileXAPnlReservations ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::reconcileXAPnlReservations ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Download xapnl messages from mail server
	 * 
	 * @return
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void downloadXAPnlMessagesFromMailServer() throws ModuleException {
		MessagePasserConfig messagePasserConfig = MessagePasserModuleUtils.getMessagePasserConfig();
		String processPath = messagePasserConfig.getXaPnlProcessPath();
		String defaultMailServer = messagePasserConfig.getPopMailServer();
		String username = messagePasserConfig.getXaPnlMailServerUsername();
		String password = messagePasserConfig.getXaPnlMailServerPassword();
		boolean fetchStatus = messagePasserConfig.getXaFetchPnlFromMailServer().equalsIgnoreCase("true") ? true : false;
		boolean deleteStatus = messagePasserConfig.getXaDeleteFromMailServer().equalsIgnoreCase("true") ? true : false;

		try {
			// If specified to fetch emails then fetching emails
			if (fetchStatus) {
				Pop3Client.getMessages(processPath, defaultMailServer, username, password,
						ReservationInternalConstants.PopMessageType.PNL_MESSAGE, deleteStatus, null);
			} else {
				log.info(" FetchXAPnlFromMailServer IS SET TO FALSE THEREFORE NOT FETCHING ANY EMAILS ");

			}
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::downloadXAPnlMessagesFromMailServer ", ex);
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::downloadXAPnlMessagesFromMailServer ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Parse pnl document
	 * 
	 * @param strMsgName
	 * @return
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Integer parseXAPnlDocument(Collection msgNamesThatMayHavePartTwoThree, String strMsgName) throws ModuleException {
		Integer pnlId = null;

		try {
			pnlId = new Integer(PNLDTOParser.insertPnlToDatabase(msgNamesThatMayHavePartTwoThree, strMsgName));
		} catch (ModuleException me) {
			this.sessionContext.setRollbackOnly();
			// Error pnl documents will there as it is...
			// So later they can correct it and fix it
			log.error("################ Error on PNL document named " + strMsgName + " will remain as it is... ");
			log.error("################ Error on insert PNL to database ", me);
			throw me;
		} catch (CommonsDataAccessException cdaex) {
			this.sessionContext.setRollbackOnly();
			// Error pnl documents will there as it is...
			// So later they can correct it and fix it
			log.error("################ Error on PNL document named " + strMsgName + " will remain as it is... ");
			log.error("################ Error on insert PNL to database ", cdaex);
			throw cdaex;
		}

		return pnlId;
	}

	/**
	 * Returns validated xaPnl documents
	 * 
	 * @return
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection getValidatedXAPnlDocuments() {
		MessagePasserConfig messagePasserConfig = MessagePasserModuleUtils.getMessagePasserConfig();
		String xaPnlProcessPath = messagePasserConfig.getXaPnlProcessPath();
		String xaPnlErrorPath = messagePasserConfig.getXaPnlErrorPath();

		// Validate the messages
		XAPNLFormatUtils.validateMessages(xaPnlProcessPath, xaPnlErrorPath);

		// Return the message names
		return XAPNLFormatUtils.getMessageNames(xaPnlProcessPath);
	}

	/**
	 * Prcoess xa pnl reconcilation
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ServiceResponce processXAPnlReconcilation() throws ModuleException {

		// One Thread at a Time
		synchronized (this) {
			final String generalLogFileName = "XAPnl-Operation Failure " + new Date().toString();

			Throwable exceptionToLog = null;
			HashMap fileExceptionMap = new HashMap();

			log.info("######### XAPNL PARSING : INITIALIZING OPERATION  ##########");
			String pnlProcessPath = MessagePasserModuleUtils.getMessagePasserConfig().getXaPnlProcessPath();
			String pnlParsedPath = MessagePasserModuleUtils.getMessagePasserConfig().getXaPnlParsedPath();
			String pnlErrorPath = MessagePasserModuleUtils.getMessagePasserConfig().getXaPnlErrorPath();

			XApnlBD xapnlBD = MessagePasserModuleUtils.getXApnlBD();

			try {
				// Downloading the XA PNLs
				xapnlBD.downloadXAPnlMessagesFromMailServer();

				// after validating the XA Pnls - checks for formats
				Collection colMsgNames = xapnlBD.getValidatedXAPnlDocuments();

				Iterator itColMsgNames = colMsgNames.iterator();
				String strMsgName;
				Integer pnlId = null;

				// parse every file one by one - insert to db and get the pnl id
				// for res reconcile

				while (itColMsgNames.hasNext()) {
					strMsgName = (String) itColMsgNames.next();

					log.info("##########################  BEFORE PARSING XA PNL : " + strMsgName);

					// need to log the format error in a separate file.
					try {
						pnlId = xapnlBD.parseXAPnlDocument(colMsgNames, strMsgName);

						log.info("##########################AFTER PARSING XA PNL : " + strMsgName);
					} catch (ModuleException e) {
						log.error("########################## ERROR AFTER PARSING FOR " + strMsgName + e.getLocalizedMessage(), e);
						fileExceptionMap.put(strMsgName, exceptionToLog = e);
						continue;
					} catch (Exception e) {
						log.error("########################## ERROR AFTER PARSING FOR " + strMsgName + e.getLocalizedMessage(), e);
						fileExceptionMap.put(strMsgName, exceptionToLog = e);
						continue;
					}

					// if code comes here it means the document was passed and
					// saved.
					if (pnlId != null) {
						try {
							log.info("########################## BEFORE RECONCILING XA PNL : " + strMsgName + " XA PNL ID :"
									+ pnlId);

							xapnlBD.reconcileXAPnlReservations(pnlId.intValue());

							log.info("########################## AFTER RECONCILING XA PNL : " + strMsgName + " XA PNL ID :"
									+ pnlId);

						}

						// Practically this can not happend. Just in case if
						// there is any exceptional case
						catch (ModuleException e) {
							fileExceptionMap.put(strMsgName, exceptionToLog = e);
							log.error(" ########################## ERROR OCCURED WHILE RECONCILING PNL ID : " + pnlId + " DATE :"
									+ new Date(), e);

						} catch (Exception e) {

							fileExceptionMap.put(strMsgName, exceptionToLog = e);
							log.error("########################## ERROR OCCURED WHEN CREATING EK RES", e);
						} finally {

							XAMailError.notifyError(pnlId, exceptionToLog, strMsgName);

						}
					}
				}
				// check for failed xa pnls
				log.info("########################## GOING TO CHECK FOR FAILED XA PNL RESERVATIONS");
				Collection xaPnlIds = xapnlBD.getFailedXAPnlIds();
				if (xaPnlIds != null && xaPnlIds.size() > 0) {
					Iterator iterXaPnlIds = xaPnlIds.iterator();
					while (iterXaPnlIds.hasNext()) {
						Integer xaPnlId = (Integer) iterXaPnlIds.next();

						try {
							xapnlBD.reconcileXAPnlReservations(xaPnlId.intValue());

						} catch (Exception e) {
							XAMailError.notifyError(xaPnlId, e, "RETRY XA ID " + xaPnlId.toString());
						}
					}
				}
				log.info("########################## AFTER CHECKING FAILED XA PNL RESERVATIONS");
			} catch (ModuleException ex) {
				this.sessionContext.setRollbackOnly();
				log.error(" ((o)) ModuleException::processXAPnlReconcilation ", ex);
				fileExceptionMap.put(generalLogFileName, exceptionToLog = ex);
				throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
			} catch (CommonsDataAccessException cdaex) {
				fileExceptionMap.put(generalLogFileName, exceptionToLog = cdaex);
				this.sessionContext.setRollbackOnly();
				log.error(" ((o)) CommonsDataAccessException::processXAPnlReconcilation ", cdaex);
				throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
			} finally {

				// if there was an exception in the above code only all the
				// exceptions will get loged.
				// log for each file will be created
				if (exceptionToLog != null) {
					log.info("########################## XAPNL ERRORS EXIST STARTING TO LOG   ##########");
					Iterator iter = fileExceptionMap.keySet().iterator();
					XAPnlLogDTO logDTO;

					while (iter.hasNext()) {
						String fileName = (String) iter.next();
						XAPNLFormatUtils.moveAndDeleteXAPnls(pnlProcessPath, pnlErrorPath, fileName);
						Throwable xceptionToLog = (Throwable) fileExceptionMap.get(fileName);

						logDTO = new XAPnlLogDTO();
						logDTO.setFileName(fileName);
						logDTO.setStackTraceElements(xceptionToLog.getStackTrace());

						if (xceptionToLog instanceof ModuleException) {
							ModuleException e = (ModuleException) xceptionToLog;
							logDTO.setExceptionDescription(e.getMessageString());
						} else {

							logDTO.setExceptionDescription(xceptionToLog.getLocalizedMessage());
						}

						XAPnlReconcilationLogger.createErrorLog(logDTO);
						XAMailError.notifyError(logDTO);

					}
					log.info("########################## XAPNL ERRORS EXIST ... FINISHED  LOGING   ##########");
				}
				XAPNLFormatUtils.moveAndDeleteAnyRemainingXAPnls(pnlProcessPath, pnlParsedPath);
			}

			log.info("########################## PNL PARSING : OPERATION COMPLETED ##########");

			return new DefaultServiceResponse(true);
		}
	}

	/**
	 * Get Failed XA Pnl Ids. Will update all "E" to "N"
	 * 
	 * 
	 * @return
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Collection getFailedXAPnlIds() throws ModuleException {
		return PNLDTOParser.getFailedXAPnls();
	}

	/**
	 * Returns XA Pnl entry paged data
	 * 
	 * @param criteria
	 * @param startIndex
	 * @param noRecs
	 * @param orderByFieldList
	 * @return
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Page getPagedXAPNLData(List criteria, int startIndex, int noRecs, List orderByFieldList) throws ModuleException {
		try {
			return MessagePasserDAOUtils.DAOInstance.XA_PNL_DAO.getPagedXAPNLData(criteria, startIndex, noRecs, orderByFieldList);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getPagedXAPNLData ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Return XA Pnl Passenger entries
	 * 
	 * @param xaPnlId
	 * @param status
	 * @return
	 * @throws ModuleException
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection getXAPnlPaxEntries(int xaPnlId, String status) throws ModuleException {
		try {
			return MessagePasserDAOUtils.DAOInstance.XA_PNL_DAO.getXAPnlPaxEntries(xaPnlId, status);
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::getXAPnlPaxEntries ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

}
