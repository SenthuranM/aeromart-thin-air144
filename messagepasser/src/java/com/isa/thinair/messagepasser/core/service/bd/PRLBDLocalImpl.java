package com.isa.thinair.messagepasser.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.messagepasser.api.service.PRLBD;

/**
 * @author Ishan
 */
@Local
public interface PRLBDLocalImpl extends PRLBD {

}
