package com.isa.thinair.messagepasser.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.messagepasser.api.service.ETLBD;

/**
 * Implementation of all the methods required to access the etl details <strong> Remote Access </strong>
 * 
 * @author Ishan
 */
@Remote
public interface ETLBDImpl extends ETLBD {

}
