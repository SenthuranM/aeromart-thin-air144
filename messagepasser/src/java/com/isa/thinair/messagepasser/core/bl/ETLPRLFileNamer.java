package com.isa.thinair.messagepasser.core.bl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.messagepasser.api.utils.ParserConstants.MessageTypes;
import com.isa.thinair.messagepasser.core.util.ParserUtil;

public class ETLPRLFileNamer extends FileNamer {
	private static Log log = LogFactory.getLog(ETLPRLFileNamer.class);

	private static final String FILE_EXTENSION = ".txt";
	private static final String ALL = "ALL";
	private static final String PART = "PART";
	private MessageTypes messageType;

	public ETLPRLFileNamer(MessageTypes messageType) {
		this.messageType = messageType;
	}

	private String buildCombinedFileNameOldWay(Collection<String> colFileNames) {
		try {
			String fileName = null;

			for (Iterator<String> itFileNames = colFileNames.iterator(); itFileNames.hasNext();) {
				if (fileName != null) {
					String value = (String) itFileNames.next();

					int lastIndex = value.lastIndexOf("-");
					int lastNumber = Integer.parseInt(value.substring(lastIndex + 1, value.indexOf(FILE_EXTENSION)));

					if (fileName.indexOf(FILE_EXTENSION) == -1) {
						fileName = fileName + "," + lastNumber;
					} else {
						fileName = fileName.substring(0, fileName.indexOf(FILE_EXTENSION)) + "," + lastNumber;
					}
				} else {
					fileName = (String) itFileNames.next();
				}
			}

			if (fileName == null) {
				return "error-" + ParserUtil.getFileName(new Date(), 0, messageType.toString());
			} else {
				return fileName + FILE_EXTENSION;
			}
		} catch (Exception e) {
			log.error("Failure in generating common file name (using old method) for :" + colFileNames);
		}
		return "error-" + ParserUtil.getFileName(new Date(), 0, messageType.toString());
	}

	public String getCombinedFileName(Collection<String> fileNames) {
		String combinedName = null;
		if (fileNames != null && fileNames.size() > 0) {
			for (String fileName : fileNames) {
				if (combinedName == null) {
					if (fileName.indexOf(PART) > 0) {
						combinedName = fileName.substring(0, fileName.indexOf(PART)) + ALL + FILE_EXTENSION;
						break;
					}
				}
			}
		}

		if (combinedName == null) {
			combinedName = buildCombinedFileNameOldWay(fileNames);
		}

		return combinedName;
	}

	protected String getUniqueName(File sourceFile) {
		String name = null;
		try {
			FileReader fileReader = new FileReader(sourceFile);
			BufferedReader buffer = new BufferedReader(fileReader);
			String line = null;
			boolean hit = false;
			while ((line = buffer.readLine()) != null) {
				if (hit) {
					name = messageType.toString() + "_" + normalizeName(line) + FILE_EXTENSION;
					break;
				} else if (line.indexOf(messageType.toString()) == 0) {
					hit = true;
				}
			}
			buffer.close();
			fileReader.close();
		} catch (IOException e) {
			log.error("Failure in generating unique file name for :" + sourceFile != null
					? sourceFile.getAbsolutePath()
					: "(null filename)");
		}
		return name;
	}

	private String normalizeName(String line) {
		return line.trim().replaceAll("\\s", "_").replaceAll("/", "_");
	}

	public static void main(String args[]) {
		System.out.println("Started...");
		ETLPRLFileNamer fn = new ETLPRLFileNamer(MessageTypes.ETL);
		File messagesDir = new File("/home/dilan/etl/");
		File sourceFile = new File(messagesDir, "ETL-13082016231001-0,1,2,3,4,5.txt");
		fn.rename(sourceFile, messagesDir);

		Collection<String> fileNames = new ArrayList<>();
		fileNames.add("ETL_G9668_14AUG_KRT_PART1.txt");
		fileNames.add("ETL_G9668_14AUG_KRT_PART2.txt");
		fileNames.add("ETL_G9668_14AUG_KRT_PART3.txt");
		fileNames.add("ETL_G9668_14AUG_KRT_PART4.txt");
		fileNames.add("ETL_G9668_14AUG_KRT_PART5.txt");
		String outName = fn.getCombinedFileName(fileNames);
		System.out.println("Fiels :" + fileNames);
		System.out.println("CombinedName :" + outName);
		System.out.println("Ended...");
	}
}
