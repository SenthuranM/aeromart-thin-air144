package com.isa.thinair.messagepasser.core.bl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservation;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientTransferSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientTransferSegmentTO.SegmentDetails;
import com.isa.thinair.airproxy.api.model.reservation.core.ModificationParamRQInfo;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.PaxTypeUtils;
import com.isa.thinair.airreservation.api.dto.FlightReconcileDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegmentETicket;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.service.SegmentBD;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationPaxStatus;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ReservationSegmentStatus;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.CommonsConstants.EticketStatus;
import com.isa.thinair.commons.api.constants.CommonsConstants.FinalEticketStatus;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.messagepasser.api.dto.etl.ETLProcessPaxResponseDTO;
import com.isa.thinair.messagepasser.api.model.ETL;
import com.isa.thinair.messagepasser.api.model.ETLPaxEntry;
import com.isa.thinair.messagepasser.api.utils.MessagePasserModuleUtils;
import com.isa.thinair.messagepasser.api.utils.ParserConstants;
import com.isa.thinair.messagepasser.core.persistence.dao.ETLDAO;

public class ETLServices {

	private static Log log = LogFactory.getLog(ETLServices.class);

	private ReservationBD reservationBD;
	private SegmentBD segmentBD;
	private ETLDAO etlDAO;

	private static String FLOWN = "FLOWN";
	private static String NOT_BOARDED = "NOT BOARDED";

	public ETLServices() {
		reservationBD = MessagePasserModuleUtils.getReservationBD();
		segmentBD = MessagePasserModuleUtils.getSegmentBD();
		etlDAO = MessagePasserDAOUtils.DAOInstance.ETL_DAO;
	}

	public ETLProcessPaxResponseDTO processETLPaxEntry(ETLPaxEntry etlPaxEntry, ETL etl) throws ModuleException {

		ETLProcessPaxResponseDTO etlPPRDTO = new ETLProcessPaxResponseDTO();
		ReservationAudit reservationAudit = new ReservationAudit();
		Collection<Integer> ppfsIds = new ArrayList<Integer>();
		Collection<ReservationPaxFareSegmentETicket> eTickets = new ArrayList<ReservationPaxFareSegmentETicket>();
		Collection<Object[]> eticketNumbersToReconcile = new ArrayList<Object[]>();
		String messageType = ParserConstants.MessageTypes.ETL.toString();
		if (etlPaxEntry.getEticketNumber() != null) {
			eticketNumbersToReconcile.add(new Object[] { etlPaxEntry.getEticketNumber(), etlPaxEntry.getCoupNumber() });
		}
		if (etlPaxEntry.getInfEticketNumber() != null) {
			eticketNumbersToReconcile.add(new Object[] { etlPaxEntry.getInfEticketNumber(), etlPaxEntry.getInfCoupNumber() });
		}
		
		boolean isCodeSharePaxSeg = false;
		if(!StringUtil.isNullOrEmpty(etlPaxEntry.getMarketingFlightElement())){
			isCodeSharePaxSeg = true;
		}
		
		boolean alreadyTransfered = false;
		for (Object[] eticketDetails : eticketNumbersToReconcile) {
			String eticketNumber = eticketDetails[0].toString();
			Integer couponNumber = new Integer(eticketDetails[1].toString());
			ReservationPaxFareSegmentETicket resPaxFareSegmentEticket = reservationBD
					.getPaxFareSegmentEticket(eticketNumber, couponNumber, messageType);
			if(isCodeSharePaxSeg && etlPaxEntry.getCodeShareFlightNo() == null){
				etlPaxEntry.setProcessedStatus(ParserConstants.ETLProcessStatus.ERROR_OCCURED);
				etlPaxEntry.setErrorDescription(new ModuleException("messagepasser.etl.skip.norec.codeshare.pnr.not.codeshare.flight")
						.getMessageString());
			}else{
				if (resPaxFareSegmentEticket != null) {
					if (etlPaxEntry.getPaxStatus().equals(ReservationInternalConstants.PaxFareSegmentTypes.FLOWN)) {
						String bookingClassType = reservationBD.getBookingClassType(eticketNumber, couponNumber);
						if ((eticketNumber.equals(etlPaxEntry.getInfEticketNumber()))
								|| (bookingClassType != null && !bookingClassType.equals(BookingClass.BookingClassType.OPEN_RETURN))) {
							EticketTO eticketTO = reservationBD.getEticketInfo(eticketNumber, couponNumber);
							boolean skipNoRecForCodeShare = this.isNorec(this.getETLFltSegIds(etl, etlPaxEntry),
									eticketTO.getFlightSegId())
									&& isCodeSharePaxSeg;
							if (!AppSysParamsUtil.isProcessNorecWithPFS() && !skipNoRecForCodeShare) {
								Integer ppfsId = this.updatePaxStatusForNorec(etl, etlPaxEntry, eticketNumber, couponNumber,
										alreadyTransfered);
								if (ppfsId != null) {
									ppfsIds.add(ppfsId);
									if (!alreadyTransfered) {
										// Reloading paxfaresegmenteticket as it been changed under transfer
										resPaxFareSegmentEticket = reservationBD
												.getPaxFareSegmentEticket(eticketNumber, couponNumber, messageType);
										alreadyTransfered = true;
									}
								}
							}
							
							if (eticketTO != null
									&& (eticketTO.getPaxStatus().equals(ReservationInternalConstants.PaxFareSegmentTypes.CONFIRMED) || eticketTO
											.getPaxStatus().equals(ReservationInternalConstants.PaxFareSegmentTypes.FLOWN))) {
								if (!AppSysParamsUtil.isProcessNorecWithPFS()
										&& this.isNorec(this.getETLFltSegIds(etl, etlPaxEntry), eticketTO.getFlightSegId())
										&& !alreadyTransfered 
										&& !skipNoRecForCodeShare) {
									// This means NOREC pax came with wrong details
									etlPaxEntry.setProcessedStatus(ParserConstants.ETLProcessStatus.ERROR_OCCURED);
									etlPaxEntry.setErrorDescription(new ModuleException("messagepasser.etl.invalidETicketData")
											.getMessageString());
									reservationAudit.setModificationType(AuditTemplateEnum.PAX_ETL_ENTRY.getCode());
									etlPPRDTO.setReservationAudit(this.createETLAudit(reservationAudit, etlPaxEntry, etl));
								} else {
									if (validateEtktTransition(resPaxFareSegmentEticket.getStatus())) {
										reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ETLEntry.PAX_STATUS, FLOWN);
										resPaxFareSegmentEticket.setStatus(EticketStatus.FLOWN.code());
										if (isCodeSharePaxSeg) {
											resPaxFareSegmentEticket.setExternalCouponStatus(EticketStatus.FLOWN.code());
										}
										etlPaxEntry.setProcessedStatus(ParserConstants.ETLProcessStatus.PROCESSED);
									}
								}
								
								if (skipNoRecForCodeShare) {
									etlPaxEntry.setErrorDescription(new ModuleException("messagepasser.etl.skip.norec.codeshare.pnr")
											.getMessageString());
									reservationAudit.setModificationType(AuditTemplateEnum.PAX_ETL_ENTRY.getCode());
									etlPPRDTO.setReservationAudit(this.createETLAudit(reservationAudit, etlPaxEntry, etl));
								}
							} else {
								// This means something like noshow pax details came in etl etc...
								etlPaxEntry.setProcessedStatus(ParserConstants.ETLProcessStatus.ERROR_OCCURED);
								etlPaxEntry.setErrorDescription(new ModuleException("airreservations.arg.passengerDoesNotExist")
										.getMessageString());
							}
	
						} else {
							// Open return segments will not transfer untill confirmed
							etlPaxEntry.setProcessedStatus(ParserConstants.ETLProcessStatus.ERROR_OCCURED);
							etlPaxEntry.setErrorDescription(new ModuleException("messagepasser.etl.cannotTransferORSegment")
									.getMessageString());
							reservationAudit.setModificationType(AuditTemplateEnum.PAX_ETL_ENTRY.getCode());
							etlPPRDTO.setReservationAudit(this.createETLAudit(reservationAudit, etlPaxEntry, etl));
							break;
						}
					} else {
						if (validateEtktTransition(resPaxFareSegmentEticket.getStatus())) {
							reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ETLEntry.PAX_STATUS, NOT_BOARDED);
							resPaxFareSegmentEticket.setStatus(EticketStatus.OPEN.code());
							etlPaxEntry.setProcessedStatus(ParserConstants.ETLProcessStatus.PROCESSED);
						}
					}
					eTickets.add(resPaxFareSegmentEticket);
	
				} else {
					// UPdate error details
					etlPaxEntry.setProcessedStatus(ParserConstants.ETLProcessStatus.ERROR_OCCURED);
					etlPaxEntry.setErrorDescription(new ModuleException("airreservations.arg.passengerDoesNotExist")
							.getMessageString());
				}
			}
			reservationAudit.setModificationType(AuditTemplateEnum.PAX_ETL_ENTRY.getCode());
			etlPPRDTO.setReservationAudit(this.createETLAudit(reservationAudit, etlPaxEntry, etl));

		}
		if (eTickets.size() > 0) {
			reservationBD.saveOrUpdate(eTickets);
		}
		if (ppfsIds.size() > 0) {
			segmentBD.updatePaxFareSegments(ppfsIds, ReservationInternalConstants.PaxFareSegmentTypes.NO_REC);
		}
		etlDAO.saveETLPaxEntry(etlPaxEntry);
		etlPPRDTO.setEtlPaxEntry(etlPaxEntry);

		return etlPPRDTO;

	}
	
	private static boolean validateEtktTransition(String oldEticketStatus) {
		// final status validation
		for (FinalEticketStatus et : FinalEticketStatus.values()) {
			if (et.name().equals(oldEticketStatus)) {
				return false;
			}
		}
		return true;
	}

	private Integer updatePaxStatusForNorec(ETL etl, ETLPaxEntry etlPaxEntry, String eticketNumber, Integer couponNumber,
			boolean alreadyTransfered) throws ModuleException {
		// Loading flight segments from etl details
		Collection<Integer> flightSegIDs = getETLFltSegIds(etl, etlPaxEntry);

		// Loading the relavant eticket data from eticket and coupon number
		EticketTO eticketTO = reservationBD.getEticketInfo(eticketNumber, couponNumber);

		if (eticketTO != null && this.isNorec(flightSegIDs, eticketTO.getFlightSegId())
				&& !eticketTO.getPaxStatus().equals(ReservationInternalConstants.PaxFareSegmentTypes.NO_SHORE)
				&& !eticketTO.getPaxStatus().equals(ReservationInternalConstants.PaxFareSegmentTypes.FLOWN)) {
			if (!alreadyTransfered) {
				if (this.transferPassengers(etlPaxEntry, etl, eticketTO.getPnrPaxId())) {
					return eticketTO.getPnrPaxFareSegId();
				}
			} else {
				return eticketTO.getPnrPaxFareSegId();
			}
		}
		return null;
	}

	private boolean isNorec(Collection<Integer> flightSegIds, Integer flightSegId) throws ModuleException {
		if (flightSegIds.contains(flightSegId)) {
			return false;
		}
		return true;

	}

	private Collection<Integer> getETLFltSegIds(ETL etl, ETLPaxEntry etlPaxEntry) throws ModuleException {
		Collection<Integer> flightSegIDs = segmentBD.getFlightSegmentIds(etl.getFlightNumber(), etl.getDepartureDate(),
				etl.getFromAirport(), etlPaxEntry.getArrivalAirport());

		return flightSegIDs;
	}

	private boolean transferPassengers(ETLPaxEntry etlParsedEntry, ETL etl, Integer pnrPaxId) throws ModuleException {

		Integer flightSegID = segmentBD.getFlightSegmentId(etl.getFlightNumber(), etl.getDepartureDate(), etl.getFromAirport(),
				etlParsedEntry.getArrivalAirport());

		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(etlParsedEntry.getPnr());
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadEtickets(true);
		Reservation reservation = reservationBD.getReservation(pnrModesDTO, null);
		boolean isGroupPNR = reservation.getOriginatorPnr() != null && !reservation.getOriginatorPnr().equals("");
		boolean segmentTransfered = false;

		if (hasValidETicketWithCoupon(reservation, etlParsedEntry, pnrPaxId)) {
			// get total pax count
			int totPaxCount = reservation.getTotalPaxAdultCount() + reservation.getTotalPaxChildCount();

			// If more than 1 pax, then split that passenger
			if (totPaxCount > 1) {
				Collection<Integer> colPax = new ArrayList<Integer>();
				Set<ReservationPax> passengers = reservation.getPassengers();
				Iterator<ReservationPax> itrPax = passengers.iterator();
				// int pnrPaxId = 0;
				ReservationPax resPax = null;
				while (itrPax.hasNext()) {
					resPax = (ReservationPax) itrPax.next();
					if (resPax.getPnrPaxId().intValue() == pnrPaxId.intValue()) {
						pnrPaxId = resPax.getPnrPaxId();
						break;
					}
				}

				if (pnrPaxId != 0) {
					colPax.add(pnrPaxId);
					String splitNewPnr = null;

					LCCClientPnrModesDTO pnrModesDTOForSplit = getPnrModesDTO(reservation.getPnr(), isGroupPNR);
					ModificationParamRQInfo paramRQInfo = new ModificationParamRQInfo();
					paramRQInfo.setAppIndicator(AppIndicatorEnum.APP_XBE);
					TrackInfoDTO trackInfo = new TrackInfoDTO();
					LCCClientReservation lccReservation = ReservationModuleUtils.getAirproxyReservationBD()
							.searchReservationByPNR(pnrModesDTOForSplit, paramRQInfo, trackInfo);

					LCCClientReservation lccClientReservationForSplit = new LCCClientReservation();
					lccClientReservationForSplit.setPNR(reservation.getPnr());
					lccClientReservationForSplit.setVersion(lccReservation.getVersion());
					lccClientReservationForSplit.setStatus(lccReservation.getStatus());
					lccClientReservationForSplit.setBookingCategory(lccReservation.getBookingCategory());

					LCCClientReservationPax lccReservationPax = trasformResPaxToLCCClientResPax(resPax);
					lccClientReservationForSplit.addPassenger(lccReservationPax);

					LCCClientReservation clientReservation = ReservationModuleUtils.getAirproxyReservationBD().splitReservation(
							lccClientReservationForSplit, isGroupPNR, trackInfo);
					splitNewPnr = clientReservation.getPNR();

					if (splitNewPnr != null && !splitNewPnr.equals("")) {

						pnrModesDTO.setPnr(splitNewPnr);
						Reservation newResvation = reservationBD.getReservation(pnrModesDTO, null);

						etlParsedEntry.setPnr(splitNewPnr);

						// Populate FlightReconcileDTO objects by identifying correct ReservationSegment from new
						// reservation
						Collection<FlightReconcileDTO> colNorecFlightReconcileDTO = new ArrayList<FlightReconcileDTO>();
						List<ReservationSegmentDTO> reservationSegmentDTO = new ArrayList<ReservationSegmentDTO>(
								newResvation.getSegmentsView());
						Collections.sort(reservationSegmentDTO);
						for (ReservationSegmentDTO resSegment : reservationSegmentDTO) {
							if (resSegment.getSegmentCode().startsWith(etlParsedEntry.getDepartureAirport())
									&& resSegment.getSegmentCode().endsWith(etlParsedEntry.getArrivalAirport())
									&& resSegment.getStatus().equals(ReservationSegmentStatus.CONFIRMED)
									&& (resSegment.getDepartureDate().after(etlParsedEntry.getFlightDate()) || resSegment
											.getDepartureDate().getTime() == etlParsedEntry.getFlightDate().getTime())) {
								FlightReconcileDTO flightReconcileDTO = new FlightReconcileDTO();
								flightReconcileDTO.setPnr(splitNewPnr);
								flightReconcileDTO.setPnrSegId(resSegment.getPnrSegId());
								flightReconcileDTO.setFlightSegId(resSegment.getFlightSegId());
								flightReconcileDTO.setCabinClassCode(resSegment.getCabinClassCode());
								flightReconcileDTO.setFlightNumber(resSegment.getFlightNo());
								flightReconcileDTO.setArrivalDateTimeLocal(resSegment.getArrivalDate());
								flightReconcileDTO.setArrivalDateTimeZulu(resSegment.getZuluArrivalDate());
								flightReconcileDTO.setDepartureDateTimeLocal(resSegment.getDepartureDate());
								flightReconcileDTO.setDepartureDateTimeZulu(resSegment.getZuluDepartureDate());
								flightReconcileDTO.setSegementCode(resSegment.getSegmentCode());
								colNorecFlightReconcileDTO.add(flightReconcileDTO);
								// Setting the new PNR
								etlParsedEntry.setPnr(splitNewPnr);
								break;
							}
						}

						transferReservationSegmentForNoRec(colNorecFlightReconcileDTO, newResvation.getVersion(), flightSegID,
								etlParsedEntry, isGroupPNR);
						if (colNorecFlightReconcileDTO.size() > 0) {
							segmentTransfered = true;
						}

					} else {
						log.info("Invalid split operation detected. PNR - " + etlParsedEntry.getPnr() + " PNR pax id - "
								+ pnrPaxId);
						// throw new ModuleException("airreservations.splitReservation.unsuccess");
						etlParsedEntry.setErrorDescription(new ModuleException("airreservations.splitReservation.unsuccess")
								.getMessageString());
					}

				} else {
					// Pax not found
					etlParsedEntry.setErrorDescription(new ModuleException("airreservations.arg.passengerDoesNotExist")
							.getMessageString());
				}

			} else {
				// Populate FlightReconcileDTO objects by identifying correct ReservationSegment from reservation
				Collection<FlightReconcileDTO> colNorecFlightReconcileDTO = new ArrayList<FlightReconcileDTO>();
				List<ReservationSegmentDTO> reservationSegmentDTO = new ArrayList<ReservationSegmentDTO>(
						reservation.getSegmentsView());
				Collections.sort(reservationSegmentDTO);
				for (ReservationSegmentDTO resSegment : reservationSegmentDTO) {
					if (resSegment.getSegmentCode().startsWith(etlParsedEntry.getDepartureAirport())
							&& resSegment.getSegmentCode().endsWith(etlParsedEntry.getArrivalAirport())
							&& resSegment.getStatus().equals(ReservationSegmentStatus.CONFIRMED)
							&& (resSegment.getDepartureDate().after(etlParsedEntry.getFlightDate()) || resSegment
									.getDepartureDate().getTime() == etlParsedEntry.getFlightDate().getTime())) {
						FlightReconcileDTO flightReconcileDTO = new FlightReconcileDTO();
						flightReconcileDTO.setPnr(etlParsedEntry.getPnr());
						flightReconcileDTO.setPnrSegId(resSegment.getPnrSegId());
						flightReconcileDTO.setFlightSegId(resSegment.getFlightSegId());
						flightReconcileDTO.setCabinClassCode(resSegment.getCabinClassCode());
						flightReconcileDTO.setFlightNumber(resSegment.getFlightNo());
						flightReconcileDTO.setArrivalDateTimeLocal(resSegment.getArrivalDate());
						flightReconcileDTO.setArrivalDateTimeZulu(resSegment.getZuluArrivalDate());
						flightReconcileDTO.setDepartureDateTimeLocal(resSegment.getDepartureDate());
						flightReconcileDTO.setDepartureDateTimeZulu(resSegment.getZuluDepartureDate());
						flightReconcileDTO.setSegementCode(resSegment.getSegmentCode());
						colNorecFlightReconcileDTO.add(flightReconcileDTO);
						break;
					}
				}
				transferReservationSegmentForNoRec(colNorecFlightReconcileDTO, reservation.getVersion(), flightSegID,
						etlParsedEntry, isGroupPNR);
				if (colNorecFlightReconcileDTO.size() > 0) {
					segmentTransfered = true;
				}
			}
		}
		return segmentTransfered;
	}

	private LCCClientReservationPax trasformResPaxToLCCClientResPax(ReservationPax resPax) {
		LCCClientReservationPax lccResPax = new LCCClientReservationPax();
		lccResPax.setTitle(resPax.getTitle());
		lccResPax.setFirstName(resPax.getFirstName());
		lccResPax.setLastName(resPax.getLastName());
		lccResPax.setPaxSequence(resPax.getPaxSequence());
		String strCarrierCode = AppSysParamsUtil.getDefaultCarrierCode();
		lccResPax.setTravelerRefNumber(strCarrierCode + "|" + PaxTypeUtils.travelerReference(resPax));
		return lccResPax;
	}

	private Long transferReservationSegmentForNoRec(Collection<FlightReconcileDTO> colSourceFlightReconcileDTO,
			Long currentVersion, Integer flightSegID, ETLPaxEntry etlParsedEntry, boolean isGroupPNR) throws ModuleException {
		Iterator<FlightReconcileDTO> itrColSourceFlightReconcileDTO = colSourceFlightReconcileDTO.iterator();
		Long reservationVersion = currentVersion;

		TrackInfoDTO trackInfo = new TrackInfoDTO();
		LCCClientTransferSegmentTO transferSegmentTO = new LCCClientTransferSegmentTO();

		String carrierCode = null;
		if (isGroupPNR) {
			Map<String, SegmentDetails> oldSegmentDetails = new HashMap<String, SegmentDetails>();
			FlightReconcileDTO flightReconcileDTO = null;
			while (itrColSourceFlightReconcileDTO.hasNext()) {
				flightReconcileDTO = (FlightReconcileDTO) itrColSourceFlightReconcileDTO.next();
				SegmentDetails segmentDetails = (new LCCClientTransferSegmentTO()).new SegmentDetails();
				segmentDetails.setDepartureDate(flightReconcileDTO.getDepartureDateTimeLocal());
				segmentDetails.setDepartureDateZulu(flightReconcileDTO.getDepartureDateTimeZulu());
				segmentDetails.setArrivalDate(flightReconcileDTO.getArrivalDateTimeLocal());
				segmentDetails.setArrivalDateZulu(flightReconcileDTO.getArrivalDateTimeZulu());

				segmentDetails.setPnrSegId(flightReconcileDTO.getPnrSegId());
				segmentDetails.setSegmentCode(flightReconcileDTO.getSegementCode());
				carrierCode = AppSysParamsUtil.extractCarrierCode(flightReconcileDTO.getFlightNumber());
				segmentDetails.setCarrier(carrierCode);
				segmentDetails.setCabinClass(flightReconcileDTO.getCabinClassCode());
				segmentDetails.setFlightNumber(etlParsedEntry.getFlightNumber());
				oldSegmentDetails.put(segmentDetails.getSegmentCode(), segmentDetails);

			}
			transferSegmentTO.setOldSegmentDetails(oldSegmentDetails);

			Map<String, SegmentDetails> newSegmentDetails = new HashMap<String, SegmentDetails>();
			FlightSegement segment = ReservationModuleUtils.getFlightBD().getFlightSegment(flightSegID);
			SegmentDetails segmentDetails = (new LCCClientTransferSegmentTO()).new SegmentDetails();
			segmentDetails.setDepartureDate(segment.getEstTimeDepatureLocal());
			segmentDetails.setDepartureDateZulu(segment.getEstTimeDepatureZulu());
			segmentDetails.setArrivalDate(segment.getEstTimeArrivalLocal());
			segmentDetails.setArrivalDateZulu(segment.getEstTimeArrivalZulu());
			segmentDetails.setFlightSegId(segment.getFltSegId());
			segmentDetails.setSegmentCode(segment.getSegmentCode());
			segmentDetails.setCarrier(carrierCode);
			segmentDetails.setCabinClass(flightReconcileDTO.getCabinClassCode());
			segmentDetails.setFlightNumber(flightReconcileDTO.getFlightNumber());
			newSegmentDetails.put(segmentDetails.getSegmentCode(), segmentDetails);

			transferSegmentTO.setNewSegmentDetails(newSegmentDetails);
			transferSegmentTO.setReservationVersion(carrierCode + "-" + currentVersion.toString());
		} else {
			Map<String, Integer> pnrSegIdAndNewFlgSegId = new HashMap<String, Integer>();
			while (itrColSourceFlightReconcileDTO.hasNext()) {
				FlightReconcileDTO flightReconcileDTO = (FlightReconcileDTO) itrColSourceFlightReconcileDTO.next();
				carrierCode = AppSysParamsUtil.extractCarrierCode(flightReconcileDTO.getFlightNumber());

				pnrSegIdAndNewFlgSegId.put(FlightRefNumberUtil.composeFlightRPH(flightReconcileDTO, carrierCode), flightSegID);

			}
			transferSegmentTO.setOldNewFlightSegId(pnrSegIdAndNewFlgSegId);
			transferSegmentTO.setReservationVersion(currentVersion.toString());
		}

		transferSegmentTO.setPnr(etlParsedEntry.getPnr());
		transferSegmentTO.setGroupPNR(isGroupPNR);

		transferSegmentTO.setAlert(false);

		ReservationModuleUtils.getAirproxySegmentBD().transferSegments(transferSegmentTO, trackInfo, null);

		return reservationVersion;
	}

	/**
	 * Find out the passenger
	 * 
	 * @param reservationPax
	 * @param pfsParsedEntry
	 * @return
	 */
	private boolean isPaxFound(ReservationPax reservationPax, ETLPaxEntry etlParsedEntry) {
		String strDbTitle = BeanUtils.nullHandler(reservationPax.getTitle()).toUpperCase();
		String strDbFirstName = BeanUtils.nullHandler(reservationPax.getFirstName()).toUpperCase().replaceAll(" ", "")
				.replaceAll("-", "");
		String strDbLastName = BeanUtils.nullHandler(reservationPax.getLastName()).toUpperCase().replaceAll(" ", "")
				.replaceAll("-", "");

		String strDbPaxType = BeanUtils.nullHandler(reservationPax.getPaxType());

		String strPfsTitle = BeanUtils.nullHandler(etlParsedEntry.getTitle()).toUpperCase();
		String strPfsFirstName = BeanUtils.nullHandler(etlParsedEntry.getFirstName()).toUpperCase().replaceAll(" ", "")
				.replaceAll("-", "");
		String strPfsLastName = BeanUtils.nullHandler(etlParsedEntry.getLastName()).toUpperCase().replaceAll(" ", "")
				.replaceAll("-", "");

		String strPfsPaxType = BeanUtils.nullHandler(etlParsedEntry.getPaxType());

		String strDbFullName = strDbFirstName + strDbLastName;
		String strPfsFullName = strPfsFirstName + strPfsLastName;

		if (strDbPaxType.equals(strPfsPaxType) && strDbFullName.equals(strPfsFullName)) {
			// If it's an adult title should be same
			if (ReservationInternalConstants.PassengerType.ADULT.equals(strDbPaxType)) {
				if (strDbTitle.equals(strPfsTitle)) {
					return true;
				}
				// If it's a child we are not comparing the titles. Cos for PNL/ADL we send the child as CHD. Titles
				// will be ignored.
				// Please improve this with a later implementation
			} else if (ReservationInternalConstants.PassengerType.CHILD.equals(strDbPaxType)) {
				return true;
				// If it's an infant
			} else if (ReservationInternalConstants.PassengerType.INFANT.equals(strDbPaxType)) {
				return true;
			}
			return false;
		} else {
			return false;
		}
	}

	public static LCCClientPnrModesDTO getPnrModesDTO(String pnr, boolean isGroupPNR) {
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		boolean loadLocalTimes = false;
		if (isGroupPNR) {
			pnrModesDTO.setGroupPNR(pnr);
			loadLocalTimes = true;
		} else {
			pnrModesDTO.setPnr(pnr);
			loadLocalTimes = false;
		}

		boolean loadFares = true;
		boolean loadLastUserNote = false;
		boolean loadOndChargesView = true;
		boolean loadPaxAvaBalance = true;
		boolean loadSegView = true;

		pnrModesDTO.setLoadFares(loadFares);
		pnrModesDTO.setLoadLastUserNote(loadLastUserNote);
		pnrModesDTO.setLoadLocalTimes(loadLocalTimes);
		pnrModesDTO.setLoadOndChargesView(loadOndChargesView);
		pnrModesDTO.setLoadPaxAvaBalance(loadPaxAvaBalance);
		pnrModesDTO.setLoadSegView(loadSegView);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);
		pnrModesDTO.setLoadSeatingInfo(true);
		pnrModesDTO.setLoadSSRInfo(true);
		pnrModesDTO.setLoadSegViewReturnGroupId(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadMealInfo(true);
		pnrModesDTO.setLoadBaggageInfo(true);
		pnrModesDTO.setAppIndicator(ApplicationEngine.XBE);
		pnrModesDTO.setLoadExternalPaxPayments(true);
		pnrModesDTO.setLoadPaxPaymentOndBreakdownView(true);
		pnrModesDTO.setLoadEtickets(true);

		return pnrModesDTO;
	}

	private ReservationAudit createETLAudit(ReservationAudit reservationAudit, ETLPaxEntry etlPaxEntry, ETL etl) {

		/** Holds the date,time format */
		SimpleDateFormat sdfmt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		reservationAudit.setPnr(etlPaxEntry.getPnr());
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ETLEntry.PAX_NAME, etlPaxEntry.getFirstName() + " "
				+ etlPaxEntry.getLastName());
		if (etlPaxEntry.getEticketNumber() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ETLEntry.E_TICKET_NO, etlPaxEntry.getEticketNumber());
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ETLEntry.COUPON_ID,
					Integer.toString(etlPaxEntry.getCoupNumber()));
		} else if (etlPaxEntry.getInfEticketNumber() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ETLEntry.E_TICKET_NO,
					etlPaxEntry.getInfEticketNumber());
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ETLEntry.COUPON_ID,
					Integer.toString(etlPaxEntry.getInfCoupNumber()));
		}
		if (ParserConstants.ETLProcessStatus.NOT_PROCESSED.equals(etlPaxEntry.getProcessedStatus())
				|| ParserConstants.ETLProcessStatus.ERROR_OCCURED.equals(etlPaxEntry.getProcessedStatus())) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ETLEntry.PROCESS_ERROR,
					etlPaxEntry.getErrorDescription());
		} else {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ETLEntry.FLIGHT_NO, etlPaxEntry.getFlightNumber());
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ETLEntry.TRAVEL_DATE,
					sdfmt.format(etl.getDepartureDate()));

		}

		return reservationAudit;
	}

	private boolean hasValidETicketWithCoupon(Reservation reservation, ETLPaxEntry etlParsedEntry, Integer pnrPaxId) {
		Integer pnrSegId = null;
		// get correct pnrsegid for eticket and coupon
		for (Iterator<ReservationPax> paxIt = reservation.getPassengers().iterator(); paxIt.hasNext();) {
			ReservationPax pax = paxIt.next();
			if (pax.getPnrPaxId().equals(pnrPaxId)) {
				Collection<EticketTO> eTickets = pax.geteTickets();
				Iterator<EticketTO> eTicketIte = eTickets.iterator();
				while (eTicketIte.hasNext()) {
					EticketTO eticketTO = eTicketIte.next();
					if (pax.getStatus().equals(ReservationPaxStatus.CONFIRMED)
							&& eticketTO.getTicketStatus().equals(EticketStatus.OPEN.code())
							&& eticketTO.getEticketNumber().equals(etlParsedEntry.getEticketNumber())
							&& eticketTO.getCouponNo().equals(etlParsedEntry.getCoupNumber())) {
						pnrSegId = eticketTO.getPnrSegId();
						break;
					}
				}
			}
		}

		// check whether pnrSegment valid with ETL processing flight
		if (pnrSegId != null) {
			List<ReservationSegmentDTO> reservationSegmentDTO = new ArrayList<ReservationSegmentDTO>(
					reservation.getSegmentsView());
			Collections.sort(reservationSegmentDTO);
			for (ReservationSegmentDTO resSegment : reservationSegmentDTO) {
				if (resSegment.getPnrSegId().equals(pnrSegId)
						&& resSegment.getSegmentCode().startsWith(etlParsedEntry.getDepartureAirport())
						&& resSegment.getSegmentCode().endsWith(etlParsedEntry.getArrivalAirport())
						&& resSegment.getStatus().equals(ReservationSegmentStatus.CONFIRMED)
						&& (resSegment.getDepartureDate().after(etlParsedEntry.getFlightDate()) || resSegment.getDepartureDate()
								.getTime() == etlParsedEntry.getFlightDate().getTime())) {
					return true;
				}
			}
		}
		return false;
	}

}
