package com.isa.thinair.messagepasser.core.bl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.model.AircraftCabinCapacity;
import com.isa.thinair.airmaster.api.model.Airport;
import com.isa.thinair.airmaster.api.model.AirportDST;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.etl.ETLDTO;
import com.isa.thinair.airreservation.api.dto.etl.ETLPaxInfo;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.service.AuditorBD;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.dto.ets.CouponStatusTKCREQRequestDTO;
import com.isa.thinair.commons.api.dto.ets.FlightStatusUpdateReqDTO;
import com.isa.thinair.commons.api.dto.ets.FlightStatusUpdateResDTO;
import com.isa.thinair.commons.api.dto.ets.RequestIdentity;
import com.isa.thinair.commons.api.dto.ets.Tenant;
import com.isa.thinair.commons.api.dto.ets.TicketUpdateStatusDetails;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.gdsservices.api.util.TypeAConstants;
import com.isa.thinair.messagepasser.api.dto.etl.ETLProcessPaxResponseDTO;
import com.isa.thinair.messagepasser.api.model.CodeShareETL;
import com.isa.thinair.messagepasser.api.model.ETL;
import com.isa.thinair.messagepasser.api.model.ETLPaxEntry;
import com.isa.thinair.messagepasser.api.utils.MessagePasserModuleUtils;
import com.isa.thinair.messagepasser.api.utils.ParserConstants;
import com.isa.thinair.messagepasser.core.persistence.dao.ETLDAO;
import com.isa.thinair.msgbroker.api.model.ServiceBearer;
import com.isa.thinair.msgbroker.api.model.ServiceClient;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command for reconciling reservation
 * 
 * 
 * @author Ishan
 * @since 1.0
 * @isa.module.command name="reconcileETLReservation"
 */
public class ReconcileETLReservation extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(ReconcileETLReservation.class);

	/** Holds the etl dao instance */
	private ETLDAO etlDAO;

	/**
	 * Construct ReconcileETLReservation
	 * 
	 * @throws ModuleException
	 */
	private ReconcileETLReservation() throws ModuleException {
		etlDAO = MessagePasserDAOUtils.DAOInstance.ETL_DAO;
	}

	/**
	 * Execute method of the ReconcileETLReservation command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside execute");
		
		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		Integer etlId = (Integer) this.getParameter(CommandParamNames.ETL_ID);
		boolean isConsiderFutureDateValidation = (Boolean) this
				.getParameter(CommandParamNames.IS_CONSIDER_FUTURE_DATE_VALIDATION);

		Collection<ReservationAudit> colReservationAudit = new ArrayList<ReservationAudit>();

		this.validateParams(etlId);

		ETL etl = etlDAO.getEtlEntry(etlId.intValue());
		
		if (etl != null) {
			Date serverDate = new Date();
			AirportDST aptDST = MessagePasserModuleUtils.getAirportBD().getEffectiveAirportDST(etl.getFromAirport(),
					etl.getDepartureDate());
			int gmtOffset = getGMTOffset(etl.getFromAirport());
			int dstOffset = (aptDST != null) ? aptDST.getDstAdjustTime() : 0;
			Date zuluDepDate = CalendarUtil.getZuluDate(etl.getDepartureDate(), gmtOffset, dstOffset);

			if ((zuluDepDate.compareTo(serverDate) < 0)
					|| ((zuluDepDate.compareTo(CalendarUtil.getEndTimeOfDate(serverDate)) <= 0) && isConsiderFutureDateValidation)) {
				// Returns ETL entries
				Collection<ETLPaxEntry> colETLPaxEntry = etlDAO.getEtlPaxEntries(etlId.intValue(), null);

				Collection<ETLPaxEntry> colErrorETLPaxEntry = new ArrayList<ETLPaxEntry>();
				Iterator<ETLPaxEntry> itColETLPaxEntry = colETLPaxEntry.iterator();
				ETLPaxEntry etlPaxEntry;
				
				List<String> couponUpdateFailedTickets = new ArrayList<String>();
			
				if(AppSysParamsUtil.isEnableAeroMartETS()){
					
					List<TicketUpdateStatusDetails> ticketUpdateStatusDetails = new ArrayList<TicketUpdateStatusDetails>();
					getCouponUpdatedTicketList(colETLPaxEntry,ticketUpdateStatusDetails);
					if (!ticketUpdateStatusDetails.isEmpty()) {

						FlightStatusUpdateReqDTO flightStatusUpdateRequest = new FlightStatusUpdateReqDTO();
						flightStatusUpdateRequest.setDepartureDate(etl.getDepartureDate());
						flightStatusUpdateRequest.setFlightNumber(etl.getFlightNumber());
						flightStatusUpdateRequest.setRecords(ticketUpdateStatusDetails);
						
						FlightStatusUpdateResDTO flightStatusUpdateResponse = updateETSCouponStatus(flightStatusUpdateRequest);
						if(flightStatusUpdateResponse != null){
							couponUpdateFailedTickets = getUpdateFailedTickets(flightStatusUpdateResponse);
						}
						 
					}
				}
				
				while (itColETLPaxEntry.hasNext()) {
					etlPaxEntry = (ETLPaxEntry) itColETLPaxEntry.next();

					// If not processed record exist
					if (ParserConstants.ETLProcessStatus.NOT_PROCESSED.equals(etlPaxEntry.getProcessedStatus())
							|| ParserConstants.ETLProcessStatus.ERROR_OCCURED.equals(etlPaxEntry.getProcessedStatus())) {

						try {
							if (!couponUpdateFailedTickets.contains(etlPaxEntry.getEticketNumber())) {

								ETLProcessPaxResponseDTO etlPPRDTO = MessagePasserModuleUtils.getETLBD().processETLPaxEntry(
										etlPaxEntry, etl);
								colReservationAudit.add(etlPPRDTO.getReservationAudit());
								etlPaxEntry = etlPPRDTO.getEtlPaxEntry();
								
							} else {

								etlPaxEntry.setErrorDescription("Error occured while updating ETS");
								etlPaxEntry.setProcessedStatus(ParserConstants.ETLProcessStatus.ERROR_OCCURED);
								etlDAO.saveETLPaxEntry(etlPaxEntry);
								log.error("Error while updating ETS :: " + etlPaxEntry.getEticketNumber());
								
							}
						} catch (ModuleException e) {
							etlPaxEntry.setErrorDescription(e.getMessageString());
							etlPaxEntry.setProcessedStatus(ParserConstants.ETLProcessStatus.ERROR_OCCURED);
							etlDAO.saveETLPaxEntry(etlPaxEntry);
							log.error("Exception Occurred while parsing ETL Entry " + etlPaxEntry.getFirstName() + " "
									+ etlPaxEntry.getLastName(), e);
						} catch (Exception e) {
							etlPaxEntry.setErrorDescription(e.getMessage());
							etlPaxEntry.setProcessedStatus(ParserConstants.ETLProcessStatus.ERROR_OCCURED);
							etlDAO.saveETLPaxEntry(etlPaxEntry);
							log.error("Exception Occurred while parsing ETL Entry " + etlPaxEntry.getFirstName() + " "
									+ etlPaxEntry.getLastName(), e);
						}

					}
					if (ParserConstants.ETLProcessStatus.ERROR_OCCURED.equals(etlPaxEntry.getProcessedStatus())) {
						colErrorETLPaxEntry.add(etlPaxEntry);
					}
				}

				if (colErrorETLPaxEntry.size() == 0) {
					etl.setProcessedStatus(ParserConstants.ETLProcessStatus.PROCESSED);
				} else {
					etl.setProcessedStatus(ParserConstants.ETLProcessStatus.ERROR_OCCURED);
				}
				
				
				if (colETLPaxEntry != null && !colETLPaxEntry.isEmpty()) {
					List<ETLDTO> pfsDTOList = generateETLEntries(colETLPaxEntry);

					try {
						sendETLForMarketingAirlines(pfsDTOList);
					} catch (Exception ex) {
						log.error("Error while sending ETL for MC :: ");
					}
					try {
						sendETLUpdateForMessageAdaptor(pfsDTOList);
					} catch (Exception ex) {
						log.error("Error while sending ETL for aeroMessage-Adaptor:: ");
					}
				}

			} else {
				etl.setProcessedStatus(ParserConstants.ETLProcessStatus.NOT_PROCESSED);
			}
			etlDAO.saveETLEntry(etl);
		}
		DefaultServiceResponse response = new DefaultServiceResponse(true);

		this.postETLAuditRecord(colReservationAudit, credentialsDTO);
		log.debug("Exit execute");
		return response;
	}

	@SuppressWarnings("unchecked")
	private void postETLAuditRecord(Collection<ReservationAudit> colReservationAudit, CredentialsDTO credentialsDTO)
			throws ModuleException {
		// Get the auditor deleagate
		AuditorBD auditorBD = MessagePasserModuleUtils.getAuditorBD();
		Iterator<ReservationAudit> itReservationAuditCol = colReservationAudit.iterator();
		while (itReservationAuditCol.hasNext()) {
			ReservationAudit reservationAudit = (ReservationAudit) itReservationAuditCol.next();

			reservationAudit.setCustomerId(credentialsDTO.getCustomerId());
			reservationAudit.setUserId(credentialsDTO.getUserId());
			reservationAudit.setZuluModificationDate(new Date());
			reservationAudit.setSalesChannelCode(credentialsDTO.getSalesChannelCode());

			if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getIpAddress() != null) {
				reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ETLEntry.IP_ADDDRESS, credentialsDTO
						.getTrackInfoDTO().getIpAddress());
			}

			if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getCarrierCode() != null) {
				reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.ETLEntry.ORIGIN_CARRIER, credentialsDTO
						.getTrackInfoDTO().getCarrierCode());
			}

			reservationAudit.getContentMap().put(
					AuditTemplateEnum.TemplateParams.ETLEntry.REMOTE_USER,
					credentialsDTO.getDefaultCarrierCode() + "/" + "[" + credentialsDTO.getAgentCode() + "]" + "/" + "["
							+ credentialsDTO.getUserId() + "]");

			reservationAudit.setDisplayName(ReservationApiUtils.getDisplayName(credentialsDTO));
			auditorBD.audit(reservationAudit, reservationAudit.getContentMap());
		}
	}

	/**
	 * Validate Parameters
	 * 
	 * @param etlId
	 * @throws ModuleException
	 */
	private void validateParams(Integer etlId) throws ModuleException {
		log.debug("Inside validateParams");

		if (etlId == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		log.debug("Exit validateParams");
	}

	private static int getGMTOffset(String airport) throws ModuleException {

		Airport apt = MessagePasserModuleUtils.getAirportBD().getAirport(airport);
		int gmtOffset = 0;
		if (apt != null) {

			gmtOffset = (apt.getGmtOffsetAction().equals("-")) ? (-1 * apt.getGmtOffsetHours()) : apt.getGmtOffsetHours();
		}

		return gmtOffset;
	}

	private static void sendETLForMarketingAirlines(List<ETLDTO> pfsDTOList) throws ModuleException {
		
			if (pfsDTOList != null && !pfsDTOList.isEmpty()) {
				String airlineCode = MessagePasserModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE);
				MessagePasserModuleUtils.getPFSETLServiceBD().sendETLMessages(pfsDTOList, airlineCode);
			}
		
	}

	private static List<ETLDTO> generateETLEntries(Collection<ETLPaxEntry> colPfsParsedEntry) throws ModuleException {
		Map<String, List<ETLPaxEntry>> flightWisePaxEntry = new HashMap<String, List<ETLPaxEntry>>();
		List<ETLDTO> etlDTOList = new ArrayList<ETLDTO>();
		if (colPfsParsedEntry != null && !colPfsParsedEntry.isEmpty()) {
			for (ETLPaxEntry etlPaxEntry : colPfsParsedEntry) {
				if (ParserConstants.ETLProcessStatus.PROCESSED.equals(etlPaxEntry.getProcessedStatus())
						&& !StringUtil.isNullOrEmpty(etlPaxEntry.getMarketingFlightElement())) {

					String codeShareFlight = etlPaxEntry.getCodeShareFlightNo();
					if (flightWisePaxEntry.get(codeShareFlight) == null) {
						flightWisePaxEntry.put(codeShareFlight, new ArrayList<ETLPaxEntry>());
					}
					flightWisePaxEntry.get(codeShareFlight).add(etlPaxEntry);

				}
			}

		}

		if (flightWisePaxEntry != null && !flightWisePaxEntry.isEmpty()) {
			for (String flightNo : flightWisePaxEntry.keySet()) {
				List<ETLPaxEntry> etlEntryList = flightWisePaxEntry.get(flightNo);
				ETLDTO etlDTO = null;

				for (ETLPaxEntry etlPaxEntry : etlEntryList) {
					Integer gdsId = etlPaxEntry.getGdsId();
					if (gdsId == null) {
						continue;
					}
					if (etlDTO == null) {

						GDSStatusTO gdsStatusTO = ReservationApiUtils.getGDSShareCarrierTO(etlPaxEntry.getGdsId());
						Map<String, Integer> cabinCapacityMap = new HashMap<String, Integer>();
						List<AircraftCabinCapacity> cabinCapacityList = MessagePasserModuleUtils.getAircraftBD()
								.getAircraftCabinCapacity(etlPaxEntry.getFlightNumber(), etlPaxEntry.getFlightDate());

						for (AircraftCabinCapacity cabinCapacity : cabinCapacityList) {
							cabinCapacity.getCabinClassCode();
							cabinCapacityMap.put(cabinCapacity.getCabinClassCode(), cabinCapacity.getSeatCapacity());
						}

						if (gdsStatusTO != null) {
							etlDTO = new ETLDTO();
							etlDTO.setArrivalAirport(etlPaxEntry.getArrivalAirport());
							etlDTO.setDepartureAirport(etlPaxEntry.getDepartureAirport());
							etlDTO.setFlightDate(etlPaxEntry.getFlightDate());
							etlDTO.setFlightNumber(flightNo);
							etlDTO.setGdsCode(gdsStatusTO.getGdsCode());
							etlDTO.setCarrierCode(gdsStatusTO.getCarrierCode());
							etlDTO.setCabinCapacityMap(cabinCapacityMap);
						}

					}

					ETLPaxInfo paxInfo = new ETLPaxInfo();
					paxInfo.setPnr(etlPaxEntry.getExternalRecordLocator());
					paxInfo.setTitle(toUpperCase(etlPaxEntry.getTitle()));
					paxInfo.setLastName(toUpperCase(etlPaxEntry.getLastName()));
					paxInfo.setFirstName(toUpperCase(etlPaxEntry.getFirstName()));
					paxInfo.setPaxStatus(etlPaxEntry.getPaxStatus());
					paxInfo.setBookingClass(etlPaxEntry.getCodeShareBc());
					paxInfo.setCabinClass(etlPaxEntry.getCabinClassCode());
					paxInfo.setArrivalAirport(etlPaxEntry.getArrivalAirport());

					paxInfo.setEticketNumber(etlPaxEntry.getExternalEticketNumber());
					paxInfo.setCoupNumber(etlPaxEntry.getExternalCouponNo());
					paxInfo.setInfEticketNumber(etlPaxEntry.getExternalInfEticketNumber());
					paxInfo.setInfCoupNumber(etlPaxEntry.getExternalInfCoupNumber());

					etlDTO.addPassengerName(paxInfo);

				}
				etlDTOList.add(etlDTO);
			}
		}

		return etlDTOList;
	}

	private static String toUpperCase(String name) {
		if (!StringUtil.isNullOrEmpty(name)) {
			name = name.trim().toUpperCase();
		}
		return name;
	}
	
	private void getCouponUpdatedTicketList(Collection<ETLPaxEntry> etlPaxEntryCollection,
			List<TicketUpdateStatusDetails> ticketUpdateStatusDetails) {

		for (ETLPaxEntry etlPaxEntry : etlPaxEntryCollection) {

			if (etlPaxEntry.isUseAeroMartETS()) {
				TicketUpdateStatusDetails updatedTicketDetails = new TicketUpdateStatusDetails();
				updatedTicketDetails.setTicketNumber(etlPaxEntry.getEticketNumber());
				updatedTicketDetails.setCouponNumber(etlPaxEntry.getCoupNumber());
				updatedTicketDetails.setTargetStatus(ReservationInternalConstants.PaxFareSegmentTypes.FLOWN);
				updatedTicketDetails.setSuccess(true);
				ticketUpdateStatusDetails.add(updatedTicketDetails);
			}
		}
	}
	
	
	private FlightStatusUpdateResDTO updateETSCouponStatus(FlightStatusUpdateReqDTO flightStatusUpdateRequest) {

		FlightStatusUpdateResDTO flightStatusUpdateResponse = null;
		try {
			RequestIdentity requestIdentity = RequestIdentity.getInstance(AppSysParamsUtil.getDefaultCarrierCode(),
					TypeAConstants.RequestIdentitySystem.ETS);
			flightStatusUpdateRequest.setRequestIdentity(requestIdentity);

			flightStatusUpdateResponse = MessagePasserModuleUtils.getETSTicketCouponStatusUpdateBD()
					.updateETicketCoupnStatus(flightStatusUpdateRequest);

		} catch (ModuleException e) {
			log.error("ERROR :: ETS update failed" + e.getCause());
		}

		return flightStatusUpdateResponse;
	} 
	
	private List<String> getUpdateFailedTickets(FlightStatusUpdateResDTO flightStatusUpdateResponse) {

		List<String> couponUpdateFailedTickets = new ArrayList<String>();

		if (flightStatusUpdateResponse != null) {
			Collection<TicketUpdateStatusDetails> ticketStatusDetails = flightStatusUpdateResponse.getPayload().getRecords();
			for (TicketUpdateStatusDetails updatedTicket : ticketStatusDetails) {
				if (!updatedTicket.isSuccess()) {
					couponUpdateFailedTickets.add(updatedTicket.getTicketNumber());
				}
			}
		}

		return couponUpdateFailedTickets;
	}
	
	private static void sendETLUpdateForMessageAdaptor(List<ETLDTO> pfsDTOList) throws ModuleException {
		if (AppSysParamsUtil.isEnableAeroMartETS()) {

			if (pfsDTOList != null && !pfsDTOList.isEmpty()) {
				/*
				 * String airlineCode = MessagePasserModuleUtils.getGlobalConfig()
				 * .getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE);
				 */
				for (ETLDTO etlDTO : pfsDTOList) {
					MessagePasserModuleUtils.getTKCREQCouponStatusUpdateBD().updateCouponStatus(etlDTO);
				}
			}

		}

	}
	
	
	
}	
