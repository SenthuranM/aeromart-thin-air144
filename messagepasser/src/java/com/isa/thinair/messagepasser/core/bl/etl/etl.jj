options {
STATIC = false ;
}
PARSER_BEGIN(ETLParser)
package com.isa.thinair.messagepasser.core.bl.etl;

import java.util.Date;

import java.io.Reader;
import java.io.StringReader;

import com.isa.thinair.commons.api.exception.ModuleException;

import com.isa.thinair.messagepasser.api.dto.etl.ETLRecordDTO;
import com.isa.thinair.messagepasser.api.dto.etl.ETLMetaDataDTO;
import com.isa.thinair.messagepasser.api.dto.etl.ETLPAXRecordDTO;
import com.isa.thinair.messagepasser.api.dto.etl.ETLRecordRemarkDTO;
/**
 * The ETL Parser
 * 
 * @author Ishan
 * @since 1.0
 */
public class ETLParser {
 /**
  * Start the parser process 
  *	  
  * @param inString
  * @return
  * @throws ModuleException
  */
  public static ETLMetaDataDTO process(String inString) throws ModuleException {
     Reader reader = new StringReader(inString) ;
     ETLParser parser = new ETLParser(reader) ;
     try {
  	   return parser.parse(inString);
     } catch(TokenMgrError e) { 
       throw new ModuleException(e, e.getMessage()); 
     } catch(ParseException e) { 
       throw new ModuleException(e, e.getMessage()); 
     } catch(Exception e) { 
  	   throw new ModuleException(e, e.getMessage()); 
     }
  }
}
PARSER_END(ETLParser)
TOKEN : { < #LETTER : ["a"-"z","A"-"Z"] > }
TOKEN : { < #DIGIT : ["0"-"9"] > }
TOKEN : { <#NEW_LINE:["\n"]>}
TOKEN : { <#ALPHANUMERIC:  (<LETTER>|<DIGIT>) >}



TOKEN :{<ONE_LETTER_WORD  :(<LETTER>){1} > }
TOKEN :{<TWO_LETTER_WORD  :(<LETTER>){2} > }
TOKEN :{<THREE_LETTER_WORD:(<LETTER>){3}> }
TOKEN :{<FOUR_LETTER_WORD :(<LETTER>){4} > }
TOKEN :{<FIVE_LETTER_WORD :(<LETTER>){5}>}
TOKEN :{<SIX_OR_MORE_LETTER_WORD :(<LETTER>){6}(<LETTER>)* > }

TOKEN:{<ONE_DIGIT_NUMBER:(<DIGIT>){1}>}
TOKEN:{<TWO_DIGIT_NUMBER:(<DIGIT>){2}>}
TOKEN:{<THREE_DIGIT_NUMBER:(<DIGIT>){3}>}
TOKEN:{<FOUR_DIGIT_NUMBER:(<DIGIT>){4}>}
TOKEN:{<FIVE_DIGIT_NUMBER:(<DIGIT>){5}>}
TOKEN:{<SIX_DIGIT_NUMBER:(<DIGIT>){6}>}
TOKEN:{<SIX_OR_MORE_DIGIT_NUMBER:(<DIGIT>){6}(<DIGIT>)* > }
TOKEN:{<ONE_OR_MORE_DIGIT_NUMBER:(<DIGIT>){1}(<DIGIT>)* > }

TOKEN :{<TWO_LETTER_ALNU  :(<ALPHANUMERIC>){2} > }
TOKEN :{<THREE_LETTER_ALNU  :(<ALPHANUMERIC>){3} > }
TOKEN :{<FOUR_LETTER_ALNU  :(<ALPHANUMERIC>){4} > }
TOKEN :{<FIVE_LETTER_ALNU  :(<ALPHANUMERIC>){5} > }
TOKEN :{<SIX_LETTER_ALNU  :(<ALPHANUMERIC>){6} > }
TOKEN :{<SEVEN_LETTER_ALNU  :(<ALPHANUMERIC>){7} > }
TOKEN :{<EIGHT_LETTER_ALNU  :(<ALPHANUMERIC>){8} > }
TOKEN :{<EIGHT_OR_MORE_LETTER_ALNU :(<ALPHANUMERIC>){8}(<ALPHANUMERIC>)* > }


TOKEN:{<B_SLASH:"/">}
TOKEN:{<N_LINE:<NEW_LINE>>}
TOKEN:{<H_PHEN:"-">}
TOKEN:{<D_OT:".">}
TOKEN:{<S_PACE:" ">}

TOKEN:{<TICKETNUM_CODE:"L"|"l">}
TOKEN:{<COPORATE_CODE:"C"|"c">}
TOKEN:{<INBOUND_CODE:"I"|"i">}
TOKEN:{<OUTBOUND_CODE:"O"|"o">}
TOKEN:{<REMARK_CODE:"R"|"r">}
TOKEN:{<W_CODE:"W"|"w">}
TOKEN:{<N_CODE:"N"|"n">}
TOKEN:{<NIL:"NIL">}

TOKEN:{<T_NUMBER:((<D_OT>("L")<B_SLASH>(<FIVE_LETTER_ALNU>|<SIX_LETTER_ALNU>|<SEVEN_LETTER_ALNU>|<EIGHT_LETTER_ALNU>|<EIGHT_OR_MORE_LETTER_ALNU>))(<S_PACE>)*)|((<D_OT><ONE_LETTER_WORD><B_SLASH>(<FIVE_LETTER_ALNU>|<SIX_LETTER_ALNU>|<SEVEN_LETTER_ALNU>|<EIGHT_LETTER_ALNU>)<B_SLASH>(<TWO_LETTER_ALNU>|<SIX_LETTER_ALNU>|<SEVEN_LETTER_ALNU>|<EIGHT_LETTER_ALNU>|<EIGHT_OR_MORE_LETTER_ALNU>))(<S_PACE>)*) >}


TOKEN:{<DATE_ID:<TWO_DIGIT_NUMBER><THREE_LETTER_WORD>>}
TOKEN:{<DEPARTURE_NUMPAX_CCCODE:<THREE_LETTER_WORD>(<ONE_DIGIT_NUMBER>|<TWO_DIGIT_NUMBER>|<THREE_DIGIT_NUMBER>)<ONE_LETTER_WORD>>}

TOKEN:{<END_OF_ETL:"ENDETL"(<S_PACE>)*<N_LINE>>}

TOKEN:{<ADDITIONAL:"&"|":"|"%"|"*"|"#"|"@"|"!"|"^"|"("|")">}

TOKEN:{<TOUR_ID:<H_PHEN>(<TWO_LETTER_ALNU>|<THREE_LETTER_ALNU>|<FOUR_LETTER_ALNU>)>}
TOKEN:{<DESNUMPAX:<H_PHEN>(<SIX_LETTER_ALNU>|<SEVEN_LETTER_ALNU>|<EIGHT_LETTER_ALNU>|<EIGHT_OR_MORE_LETTER_ALNU>)>}
TOKEN:{<INBOUND:<D_OT><INBOUND_CODE><B_SLASH><EIGHT_OR_MORE_LETTER_ALNU>>}
TOKEN:{<OUTBOUND:<D_OT><OUTBOUND_CODE><B_SLASH><EIGHT_OR_MORE_LETTER_ALNU>>}
TOKEN:{<OUTBOUNDTWO:<D_OT><TWO_LETTER_ALNU><B_SLASH><EIGHT_OR_MORE_LETTER_ALNU>>}
TOKEN:{<COPORATE:<D_OT><COPORATE_CODE><B_SLASH>(<TWO_LETTER_ALNU>|<THREE_LETTER_ALNU>|<FOUR_LETTER_ALNU>|<FIVE_LETTER_ALNU>|<SIX_LETTER_ALNU>|<SEVEN_LETTER_ALNU>|<EIGHT_OR_MORE_LETTER_ALNU>)>}
TOKEN:{<REMARK:<D_OT><REMARK_CODE><B_SLASH>>}
TOKEN:{<WINFO:<D_OT><W_CODE><B_SLASH>>}
TOKEN:{<NINFO:<D_OT><N_CODE><B_SLASH>>}
TOKEN:{<E_TICKET:<SIX_OR_MORE_DIGIT_NUMBER>>}
TOKEN:{<NOT_BOARDED:"NOT BOA"<N_LINE>>}
TOKEN:{<ENDPART:"ENDPART"<ONE_DIGIT_NUMBER>>}
TOKEN:{<SSR_CODE_SEC:<REMARK>(<TWO_LETTER_ALNU>|<THREE_LETTER_ALNU>|<FOUR_LETTER_ALNU>|<FIVE_LETTER_ALNU>|<FIVE_LETTER_ALNU>|<SIX_LETTER_ALNU>|<SEVEN_LETTER_ALNU>|<EIGHT_LETTER_ALNU>|<EIGHT_OR_MORE_LETTER_ALNU>|<FOUR_LETTER_WORD>|<THREE_LETTER_WORD>|<SIX_OR_MORE_LETTER_WORD>|(<FOUR_LETTER_WORD><B_SLASH>)|(<FOUR_LETTER_WORD><B_SLASH>(<ONE_LETTER_WORD>|<TWO_LETTER_WORD>|<THREE_LETTER_WORD>|<FOUR_LETTER_WORD>|<FIVE_LETTER_WORD>|<SIX_OR_MORE_LETTER_WORD>)))>}
SKIP :{<".RN/">} 

TOKEN:{<MARK_FLIGHT:((<D_OT>("M")<B_SLASH>(<ALPHANUMERIC>)*)(<S_PACE>)*)>}

private ETLMetaDataDTO parse(String inString) :
{
Token etlTerm;
Token flightId;
Token departureDayMonth;
Token boardingAirport;
Token partNumber;
Token desNumOfPaxCCCode;
Token totalNoOfPax;
Token arrivalDestination;
Token paxNumber;
Token lName;
Token fName;
Token ticketNumber;
Token marketingFlight;
Token tourId;
Token inbound;
Token outbound;
Token wInfo;

Token ssrCode;
Token ssrText;
Token infantName;
Token adviceTo;
Token eTicketNumber;
Token coupNumber;
Token infETicketNumber;
Token infCoupNumber;

ETLMetaDataDTO empnlMetaDataDTO = new ETLMetaDataDTO();
empnlMetaDataDTO.setDateDownloaded(new Date());
}
{
(
	etlTerm=<THREE_LETTER_WORD><N_LINE>	
	(flightId=<FOUR_LETTER_ALNU>{empnlMetaDataDTO.setFlightNumber(flightId.image);}|flightId=<FIVE_LETTER_ALNU>{empnlMetaDataDTO.setFlightNumber(flightId.image);}|flightId=<SIX_LETTER_ALNU>{empnlMetaDataDTO.setFlightNumber(flightId.image);}|flightId=<SEVEN_LETTER_ALNU>{empnlMetaDataDTO.setFlightNumber(flightId.image);})

	<B_SLASH>(departureDayMonth=<FOUR_LETTER_ALNU>{empnlMetaDataDTO.setDayMonth(departureDayMonth.image);}|departureDayMonth=<FIVE_LETTER_ALNU>{empnlMetaDataDTO.setDayMonth(departureDayMonth.image);})
	(<S_PACE>)+boardingAirport=<THREE_LETTER_WORD>{empnlMetaDataDTO.setBoardingAirport(boardingAirport.image);}
	(<S_PACE>)+partNumber=<FIVE_LETTER_ALNU>{empnlMetaDataDTO.setPartNumber(partNumber.image);}
	(<S_PACE>)*<N_LINE>
	((<NIL>)*(<S_PACE>)*<N_LINE>)*
	(<THREE_LETTER_WORD><B_SLASH>(<SIX_DIGIT_NUMBER>|<EIGHT_LETTER_ALNU>)(<S_PACE>)*<N_LINE>)*
		
		(
		{ETLRecordDTO etlRecordDTO = new ETLRecordDTO();}
		{String paxStatus = "F";}
		(desNumOfPaxCCCode=<DESNUMPAX>{etlRecordDTO.setDesAirportNumPaxCCCode(desNumOfPaxCCCode.image);}) (<N_LINE>)

		//start of new line
		(
			(<NOT_BOARDED>{paxStatus = "B";})|
		 	{ETLPAXRecordDTO recordDTO = new ETLPAXRecordDTO();}
			
					
					//last name
					(
						(paxNumber=<ONE_DIGIT_NUMBER>{etlRecordDTO.setPaxNumber(paxNumber.image);}|
						paxNumber=<TWO_DIGIT_NUMBER>{etlRecordDTO.setPaxNumber(paxNumber.image);})
						(<S_PACE>)*
					    (lName=<ONE_LETTER_WORD>{recordDTO.setLastName(lName.image);}| 
						 lName=<TWO_LETTER_WORD>{recordDTO.setLastName(lName.image);}| 
		  				 lName=<THREE_LETTER_WORD>{recordDTO.setLastName(lName.image);}|
					     lName=<FOUR_LETTER_WORD>{recordDTO.setLastName(lName.image);}|
					     lName=<FIVE_LETTER_WORD>{recordDTO.setLastName(lName.image);}|
						 lName=<SIX_OR_MORE_LETTER_WORD>{recordDTO.setLastName(lName.image);})
					)
					//end of last name	
			
				
					(<B_SLASH>)*(<S_PACE>)*
				
					//first name with tour id
					
					(
					(
					(fName=<ONE_LETTER_WORD>{recordDTO.addFName(fName.image);}|fName=<TWO_LETTER_WORD>{recordDTO.addFName(fName.image);}|
					fName=<THREE_LETTER_WORD>{recordDTO.addFName(fName.image);}|fName=<FOUR_LETTER_WORD>{recordDTO.addFName(fName.image);}|
					fName=<FIVE_LETTER_WORD>{recordDTO.addFName(fName.image);}|fName=<SIX_OR_MORE_LETTER_WORD>{recordDTO.addFName(fName.image);})  
					(<S_PACE>)*(<TWO_LETTER_WORD> | <THREE_LETTER_WORD>)*
					(tourId=<TOUR_ID>{recordDTO.setTourId(tourId.image);})*(<S_PACE>)*(<B_SLASH>)*
					)*
					((ticketNumber=<T_NUMBER>{recordDTO.setTicketnumber(ticketNumber.image);})*)
					(<S_PACE>)*
					((marketingFlight=<MARK_FLIGHT>{recordDTO.setMarketingFlight(marketingFlight.image);})*)
					(<S_PACE>)*
					((".W/P"|".W/K")<B_SLASH>(<ONE_DIGIT_NUMBER>|<TWO_DIGIT_NUMBER>|<THREE_DIGIT_NUMBER>|<FOUR_DIGIT_NUMBER>|<FIVE_DIGIT_NUMBER>|<SIX_OR_MORE_DIGIT_NUMBER>)(<B_SLASH>(<ONE_DIGIT_NUMBER>|<TWO_DIGIT_NUMBER>|<THREE_DIGIT_NUMBER>|<FOUR_DIGIT_NUMBER>|<FIVE_DIGIT_NUMBER>|<SIX_OR_MORE_DIGIT_NUMBER>))*)*
					(<S_PACE>)*
					(<NINFO><SIX_OR_MORE_DIGIT_NUMBER><B_SLASH><THREE_LETTER_WORD>)*
					(<S_PACE>)*
					)	
					
								
					
					//start remark
						
					    //MULTIPLE .R/ AND CODE WITH HK'S					
						(
						{ETLRecordRemarkDTO remarkDTO = new ETLRecordRemarkDTO();}
						(ssrCode=<SSR_CODE_SEC>{remarkDTO.setSsrCode(ssrCode.image);recordDTO.setSsrCode(ssrCode.image);})
						(
						(<S_PACE>)
						(<THREE_LETTER_ALNU><S_PACE>)*
						(((eTicketNumber=<SIX_OR_MORE_DIGIT_NUMBER>{recordDTO.setEticketNumber(eTicketNumber.image);})
						<B_SLASH>
						(coupNumber=<ONE_DIGIT_NUMBER>{recordDTO.setCoupNumber(coupNumber.image);recordDTO.setPaxStatus(paxStatus);})|
						(infETicketNumber=<EIGHT_OR_MORE_LETTER_ALNU>{recordDTO.setInfEticketNumber(infETicketNumber.image);})
						<B_SLASH>
						(infCoupNumber=<ONE_DIGIT_NUMBER>{recordDTO.setInfCoupNumber(infCoupNumber.image);})
						))*		
						(ssrText=<ONE_LETTER_WORD>{remarkDTO.setSsrText(remarkDTO.getSsrText() + " " + ssrText.image);}|
	 					ssrText=<TWO_LETTER_WORD>{remarkDTO.setSsrText(remarkDTO.getSsrText() + " " + ssrText.image);}|
						ssrText=<THREE_LETTER_WORD>{remarkDTO.setSsrText(remarkDTO.getSsrText() + " " + ssrText.image);}|
						ssrText=<FOUR_LETTER_WORD>{remarkDTO.setSsrText(remarkDTO.getSsrText() + " " + ssrText.image);}|
						ssrText=<FIVE_LETTER_WORD>{remarkDTO.setSsrText(remarkDTO.getSsrText() + " " + ssrText.image);}|
						ssrText=<SIX_OR_MORE_LETTER_WORD>{remarkDTO.setSsrText(remarkDTO.getSsrText() + " " + ssrText.image);}|
						ssrText=<TWO_LETTER_ALNU>|
						ssrText=<THREE_LETTER_ALNU>|
						ssrText=<FOUR_LETTER_ALNU>|
						ssrText=<FIVE_LETTER_ALNU>|
						ssrText=<SIX_LETTER_ALNU>|
						ssrText=<SEVEN_LETTER_ALNU>|
						ssrText=<EIGHT_LETTER_ALNU>|
						ssrText=<EIGHT_OR_MORE_LETTER_ALNU>|
						<H_PHEN>|
						ssrText=<ONE_DIGIT_NUMBER>|
						ssrText=<TWO_DIGIT_NUMBER>|
						ssrText=<THREE_DIGIT_NUMBER>|
						ssrText=<FOUR_DIGIT_NUMBER>|
						ssrText=<FIVE_DIGIT_NUMBER>|
						ssrText=<SIX_DIGIT_NUMBER>|
						ssrText=<SIX_OR_MORE_DIGIT_NUMBER>)*			
						)*
						{remarkDTO.constructAdviceTo(); etlRecordDTO.addRemarkRecord(remarkDTO);}  
						)*
					  	//end of remark   
					  	(<S_PACE>)*
						(
						(".W/P"|".W/K")<B_SLASH>(<ONE_DIGIT_NUMBER>|<TWO_DIGIT_NUMBER>|<THREE_DIGIT_NUMBER>|<FOUR_DIGIT_NUMBER>|<FIVE_DIGIT_NUMBER>|<SIX_OR_MORE_DIGIT_NUMBER>)(<B_SLASH>(<ONE_DIGIT_NUMBER>|<TWO_DIGIT_NUMBER>|<THREE_DIGIT_NUMBER>|<FOUR_DIGIT_NUMBER>|<FIVE_DIGIT_NUMBER>|<SIX_OR_MORE_DIGIT_NUMBER>))*
						)*
						(<S_PACE>)*
						(
						<NINFO><SIX_OR_MORE_DIGIT_NUMBER><B_SLASH><THREE_LETTER_WORD>
						)*
						(<S_PACE>)*
					
				{ empnlMetaDataDTO.addTourIdTicketNumber(recordDTO.getTourId(), recordDTO.getTicketnumber()); 
				etlRecordDTO.addRecordDTO(recordDTO);}<N_LINE>
				
			)*
			{empnlMetaDataDTO.addPassengerRecords(etlRecordDTO);})*
			//end of line
			
	(<END_OF_ETL>)*
	(<ENDPART>)*
)
{return empnlMetaDataDTO;}

}
