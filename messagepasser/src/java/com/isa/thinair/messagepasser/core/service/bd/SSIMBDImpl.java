package com.isa.thinair.messagepasser.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.messagepasser.api.service.SSIMBD;

/**
 * Implementation of all the methods required to access the SSIM details <strong> Remote Access </strong>
 * 
 * @author Rikaz
 */
@Remote
public interface SSIMBDImpl extends SSIMBD {

}
