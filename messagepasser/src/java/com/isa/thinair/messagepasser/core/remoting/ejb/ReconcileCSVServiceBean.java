/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.messagepasser.core.remoting.ejb;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.messagepasser.core.bl.ReconcileCSVReservation;
import com.isa.thinair.messagepasser.core.service.bd.ReconcileCsvBDImpl;
import com.isa.thinair.messagepasser.core.service.bd.ReconcileCsvBDLocalImpl;
import com.isa.thinair.platform.api.ServiceResponce;

@Stateless
@RemoteBinding(jndiBinding = "ReconcileCSVService.remote")
@LocalBinding(jndiBinding = "ReconcileCSVService.local")
@RolesAllowed("user")
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class ReconcileCSVServiceBean extends PlatformBaseSessionBean implements ReconcileCsvBDImpl, ReconcileCsvBDLocalImpl {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(ReconcileCSVServiceBean.class);

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public ServiceResponce reconcileCSVReservations() throws ModuleException {
		try {
			ReconcileCSVReservation rec = new ReconcileCSVReservation();
			rec.setExecuteBookingReconcile(true);
			rec.setExecuteDepDateFormatting(false);
			return rec.execute();
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::reconcileCSVReservations ", ex);
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::reconcileCSVReservations ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		} catch (Exception e) {
			log.error(" ((o)) Exception::reconcileCSVReservations ", e);
			throw new ModuleException("");
		}

	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public ServiceResponce processDepDateText() throws ModuleException {
		try {
			ReconcileCSVReservation rec = new ReconcileCSVReservation();
			rec.setExecuteBookingReconcile(false);
			rec.setExecuteDepDateFormatting(true);
			return rec.execute();
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::processDepDateText ", ex);
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::processDepDateText ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		} catch (Exception e) {
			log.error(" ((o)) Exception::processDepDateText ", e);
			throw new ModuleException("");
		}

	}

}
