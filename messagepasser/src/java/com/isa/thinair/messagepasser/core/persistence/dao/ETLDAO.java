package com.isa.thinair.messagepasser.core.persistence.dao;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.messagepasser.api.model.CodeShareETL;
import com.isa.thinair.messagepasser.api.model.ETL;
import com.isa.thinair.messagepasser.api.model.ETLPaxEntry;

public interface ETLDAO {

	/**
	 * Returns the ETL
	 * 
	 * @param etlId
	 * @return
	 */
	public ETL getEtlEntry(int etlId);

	public ETLPaxEntry getETLPaxEntry(int etlPaxId);

	/**
	 * Save ETL Entry
	 * 
	 * @param etl
	 */
	public void saveETLEntry(ETL etl);

	/**
	 * Save ETL Pax Entry
	 * 
	 * @param etlPaxEntry
	 */
	public void saveETLPaxEntry(ETLPaxEntry etlPaxEntry);

	public void deleteETLPaxEntry(int etlPaxId);

	/**
	 * Save Etl Pax Entries
	 * 
	 * @param colETLPaxEntry
	 */
	public void saveEtlPaxEntries(Collection colETLPaxEntry);

	/**
	 * Return ETL Passenger entries
	 * 
	 * @param etlId
	 * @param status
	 * @return
	 */
	public Collection<ETLPaxEntry> getEtlPaxEntries(int etlId, String status);

	/**
	 * 
	 * @param flightNumber
	 * @param airportCode
	 * @param departureDate
	 * @param partNumber
	 * @param pnlContent
	 * @return
	 */
	public boolean hasAnEqualEtl(String flightNumber, String airportCode, Date departureDate, Integer partNumber);

	/**
	 * Returns Page object with ETL data
	 * 
	 * @param criteria
	 * @param startIndex
	 * @param noRecs
	 * @param orderByFieldList
	 * @return
	 */
	public Page getPagedETLData(List criteria, int startIndex, int noRecs, List orderByFieldList);

	/**
	 * Return etl parse entries
	 * 
	 * @param etlId
	 * @return
	 * @throws CommonsDataAccessException
	 */
	public Collection getEtlParseEntries(int etlId) throws CommonsDataAccessException;

	/**
	 * 
	 * @param etlID
	 * @return
	 */
	public ETL getETL(int etlID);

	public int getEtlParseEntriesCount(int etlId, String processedStatus) throws CommonsDataAccessException;

	/**
	 * 
	 * @param etlID
	 * @param statuss
	 * @return
	 */
	public Collection<String> getPaxNamesForMail(Integer etlID, Collection<String> statuss);

	public boolean hasETL(String flightNo, Date departureDate, String airport, Collection<String> processedStatus);

	public void updateETLEntry(ETL etl);
	
	public void saveCodeShareETLEntry(CodeShareETL etl);
}
