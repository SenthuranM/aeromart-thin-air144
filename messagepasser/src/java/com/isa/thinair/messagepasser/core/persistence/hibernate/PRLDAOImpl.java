package com.isa.thinair.messagepasser.core.persistence.hibernate;

import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.messagepasser.api.model.PRL;
import com.isa.thinair.messagepasser.api.model.PRLPaxEntry;
import com.isa.thinair.messagepasser.api.utils.MessagePasserModuleUtils;
import com.isa.thinair.messagepasser.api.utils.ParserConstants;
import com.isa.thinair.messagepasser.core.persistence.dao.PRLDAO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * PRLDAO is the business DAO hibernate implementation
 * 
 * @author Ishan
 * @since 1.0
 * @isa.module.dao-impl dao-name="PRLDAO"
 */
public class PRLDAOImpl extends PlatformBaseHibernateDaoSupport implements PRLDAO {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(PRLDAOImpl.class);

	/**
	 * Returns the prl
	 * 
	 * @param prlId
	 * @return
	 */
	public PRL getPRLEntry(int prlId) {
		return (PRL) get(PRL.class, new Integer(prlId));
	}

	/**
	 * 
	 * @param flightNumber
	 * @param airportCode
	 * @param departureDate
	 * @param partNumber
	 * @return
	 */
	public boolean hasAnEqualPRL(String flightNumber, String airportCode, Date departureDate, Integer partNumber) {

		String sql = "SELECT count(prl_id)" + " FROM t_prl "
				+ " where flight_number=? and departure_date=? and  from_airport=? and part_number=? ";

		DataSource ds = MessagePasserModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);
		Object params[] = { flightNumber, departureDate, airportCode, partNumber };

		Object intObj = template.query(sql, params, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Integer count = null;
				while (rs.next()) {
					count = new Integer(rs.getInt(1));

				}
				return count;

			}

		});

		int val = ((Integer) intObj).intValue();
		if (val > 0) {
			return true;
		}
		return false;

	}

	/**
	 * Save PRL Entry
	 * 
	 * @param prl
	 */
	public void savePRLEntry(PRL prl) {
		log.debug("Inside savePRLEntry");

		hibernateSaveOrUpdate(prl);

		log.debug("Exit savePRLEntry");
	}

	public void savePRLPaxEntry(PRLPaxEntry prlPaxEntry) {
		log.debug("Inside savePRLPaxEntry");

		hibernateSaveOrUpdate(prlPaxEntry);

		log.debug("Exit savePRLPaxEntry");
	}

	/**
	 * Save PRL Pax Entry
	 * 
	 * @param colPRLPaxEntry
	 */
	public void savePRLPaxEntries(Collection colPRLPaxEntry) {
		log.debug("Inside savePRLPaxEntries");

		hibernateSaveOrUpdateAll(colPRLPaxEntry);

		log.debug("Exit savePRLPaxEntries");
	}

	/**
	 * Return PRL Pax entries
	 * 
	 * @param prlId
	 * @param status
	 * @return
	 */
	public Collection getPRLPaxEntries(int prlId, String status) {
		if (status == null) {
			String hql = " SELECT prlPaxEntry FROM PRLPaxEntry AS prlPaxEntry " + " WHERE prlPaxEntry.prlId = ? ";

			return find(hql, new Object[] { new Integer(prlId) }, PRLPaxEntry.class);
		} else {
			String hql = " SELECT prlPaxEntry FROM PRLPaxEntry AS prlPaxEntry "
					+ " WHERE prlPaxEntry.pnlId = ? AND prlPaxEntry.processedStatus = ? ";

			return find(hql, new Object[] { new Integer(prlId), status }, PRLPaxEntry.class);
		}
	}

	public Page getPagedPRLData(List criteria, int startIndex, int noRecs, List orderByFieldList) {
		return getPagedData(criteria, startIndex, noRecs, PRL.class, orderByFieldList);
	}

	/**
	 * Return prl parse entries
	 * 
	 * @param prlId
	 * @return
	 * @throws CommonsDataAccessException
	 */
	public Collection getPRLParseEntries(int prlId) throws CommonsDataAccessException {

		String hql = " SELECT prlEntry FROM PRLPaxEntry AS prlEntry " + " WHERE prlEntry.prlId = ? ";

		return find(hql, new Object[] { new Integer(prlId) }, PRLPaxEntry.class);
	}

	/**
	 * Returns the prl
	 * 
	 * @param prlID
	 * @return
	 */
	public PRL getPRL(int prlID) {
		return (PRL) get(PRL.class, new Integer(prlID));
	}

	/**
	 * Return prl parse entries
	 * 
	 * @param prlId
	 * @return
	 * @throws CommonsDataAccessException
	 */
	public int getPRLParseEntriesCount(int prlId, String processedStatus) throws CommonsDataAccessException {

		log.debug("Inside getPRLParseEntriesCount");
		JdbcTemplate jt = new JdbcTemplate(MessagePasserModuleUtils.getDatasource());

		// Final SQL
		String sql = "";
		Object[] params;
		if (processedStatus == null) {
			params = new Object[] { new Integer(prlId) };
			sql = " SELECT count(*) PAXCOUNT FROM T_PRL_PARSED WHERE PRL_ID = ? ";
		} else {
			params = new Object[] { new Integer(prlId), processedStatus };
			sql = " SELECT count(*) PAXCOUNT FROM T_PRL_PARSED WHERE PRL_ID = ? and PROC_STATUS = ? ";
		}

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql);
		log.debug(" SQL argument             : " + prlId);
		log.debug("############################################");

		Integer paxCount = (Integer) jt.query(sql, params, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				if (rs != null) {
					while (rs.next()) {
						return new Integer(rs.getInt("PAXCOUNT"));
					}
				}

				return new Integer(0);
			}
		});

		log.debug("Exit getPRLParseEntriesCount");
		return paxCount.intValue();
	}

	/**
	 * Delete prl
	 *
	 * @param prlId
	 */
	public void deletePrlEntry(int prlId) {
		log.debug("Inside deletePrlParseEntry");
		PRL prl = (PRL) get(PRL.class, prlId);
		if (!prl.getProcessedStatus().equals(ParserConstants.PRLStatus.RECONCILE_SUCCESS)) {
			Collection<PRLPaxEntry> prlPassengers = getPRLParseEntries(prlId);
			if (prlPassengers != null) {
				for (PRLPaxEntry paxEntry : prlPassengers) {
					deletePrlPaxEntry(paxEntry.getPrlPaxId());
				}
			}
			delete(prl);
		}
		log.debug("Exit deletePrlParseEntry");

	}

	/**
	 * Delete prlPaxEntry
	 *
	 * @param prlPaxId
	 */
	public void deletePrlPaxEntry(int prlPaxId) {
		log.debug("Inside deletePrlPaxEntry");
		PRLPaxEntry prlPaxEntry = get(PRLPaxEntry.class, prlPaxId);
		delete(prlPaxEntry);
		log.debug("Exit deletePrlPaxEntry");
	}

}
