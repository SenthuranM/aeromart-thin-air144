package com.isa.thinair.messagepasser.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.messagepasser.api.service.PRLBD;

/**
 * Implementation of all the methods required to access the etl details <strong> Remote Access </strong>
 * 
 * @author Ishan
 */
@Remote
public interface PRLBDImpl extends PRLBD {

}
