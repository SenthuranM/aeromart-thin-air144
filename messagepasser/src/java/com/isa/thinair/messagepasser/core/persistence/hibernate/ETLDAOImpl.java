package com.isa.thinair.messagepasser.core.persistence.hibernate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.messagepasser.api.model.CodeShareETL;
import com.isa.thinair.messagepasser.api.model.ETL;
import com.isa.thinair.messagepasser.api.model.ETLPaxEntry;
import com.isa.thinair.messagepasser.api.utils.MessagePasserModuleUtils;
import com.isa.thinair.messagepasser.api.utils.ParserConstants;
import com.isa.thinair.messagepasser.core.persistence.dao.ETLDAO;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * ETLDAO is the business DAO hibernate implementation
 * 
 * @author Ishan
 * @since 1.0
 * @isa.module.dao-impl dao-name="ETLDAO"
 */
public class ETLDAOImpl extends PlatformBaseHibernateDaoSupport implements ETLDAO {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(ETLDAOImpl.class);

	/**
	 * Returns the etl
	 * 
	 * @param etlId
	 * @return
	 */
	public ETL getEtlEntry(int etlId) {
		return (ETL) get(ETL.class, new Integer(etlId));
	}

	public ETLPaxEntry getETLPaxEntry(int etlPaxId) {
		return (ETLPaxEntry) get(ETLPaxEntry.class, etlPaxId);
	}

	/**
	 * 
	 * @param flightNumber
	 * @param airportCode
	 * @param departureDate
	 * @param partNumber
	 * @param pnlContent
	 * @return
	 */
	public boolean hasAnEqualEtl(String flightNumber, String airportCode, Date departureDate, Integer partNumber) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		String sql = "SELECT count(etl_id)" + " FROM t_etl "
				+ " where flight_number=? and trunc(departure_date)=? and  from_airport=? and part_number=? ";

		DataSource ds = MessagePasserModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);
		Object params[] = { flightNumber, dateFormat.format(departureDate), airportCode, partNumber };

		Object intObj = template.query(sql, params, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Integer count = null;
				while (rs.next()) {
					count = new Integer(rs.getInt(1));

				}
				return count;

			}

		});

		int val = ((Integer) intObj).intValue();
		if (val > 0) {
			return true;
		}
		return false;

	}

	/**
	 * Save ETL Entry
	 * 
	 * @param ETL
	 */
	public void saveETLEntry(ETL etl) {
		log.debug("Inside saveETLEntry");

		hibernateSaveOrUpdate(etl);

		log.debug("Exit saveETLEntry");
	}

	public void saveETLPaxEntry(ETLPaxEntry etlPaxEntry) {
		log.debug("Inside saveETLPaxEntry");

		hibernateSaveOrUpdate(etlPaxEntry);

		log.debug("Exit saveETLPaxEntry");
	}

	/**
	 * Save ETL Pax Entry
	 * 
	 * @param colEtlPaxEntry
	 */
	public void saveEtlPaxEntries(Collection colEtlPaxEntry) {
		log.debug("Inside saveEtlPaxEntries");

		hibernateSaveOrUpdateAll(colEtlPaxEntry);

		log.debug("Exit saveEtlPaxEntries");
	}

	public void deleteETLPaxEntry(int etlPaxId) {
		ETLPaxEntry etlPaxEntry = (ETLPaxEntry) get(ETLPaxEntry.class, etlPaxId);
		if (!etlPaxEntry.getProcessedStatus().equals(ParserConstants.ETLProcessStatus.PROCESSED)) {
			delete(etlPaxEntry);
		}
	}

	/**
	 * Return ETL Pax entries
	 * 
	 * @param etlId
	 * @param status
	 * @return
	 */
	public Collection<ETLPaxEntry> getEtlPaxEntries(int etlId, String status) {
		if (status == null) {
			String hql = " SELECT etlPaxEntry FROM ETLPaxEntry AS etlPaxEntry " + " WHERE etlPaxEntry.etlId = ? ";

			return find(hql, new Object[] { new Integer(etlId) }, ETLPaxEntry.class);
		} else {
			String hql = " SELECT etlPaxEntry FROM ETLPaxEntry AS etlPaxEntry "
					+ " WHERE etlPaxEntry.pnlId = ? AND etlPaxEntry.processedStatus = ? ";

			return find(hql, new Object[] { new Integer(etlId), status }, ETLPaxEntry.class);
		}
	}

	public Page getPagedETLData(List criteria, int startIndex, int noRecs, List orderByFieldList) {
		return getPagedData(criteria, startIndex, noRecs, ETL.class, orderByFieldList);
	}

	/**
	 * Return etl parse entries
	 * 
	 * @param etlId
	 * @return
	 * @throws CommonsDataAccessException
	 */
	public Collection getEtlParseEntries(int etlId) throws CommonsDataAccessException {

		String hql = " SELECT etlEntry FROM ETLPaxEntry AS etlEntry " + " WHERE etlEntry.etlId = ? ";

		return find(hql, new Object[] { new Integer(etlId) }, ETLPaxEntry.class);
	}

	/**
	 * Returns the etl
	 * 
	 * @param etlID
	 * @return
	 */
	public ETL getETL(int etlID) {
		return (ETL) get(ETL.class, new Integer(etlID));
	}

	/**
	 * Return etl parse entries
	 * 
	 * @param etlId
	 * @return
	 * @throws CommonsDataAccessException
	 */
	public int getEtlParseEntriesCount(int etlId, String processedStatus) throws CommonsDataAccessException {

		log.debug("Inside getEtlParseEntriesCount");
		JdbcTemplate jt = new JdbcTemplate(MessagePasserModuleUtils.getDatasource());

		// Final SQL
		String sql = "";
		Object[] params;
		if (processedStatus == null) {
			params = new Object[] { new Integer(etlId) };
			sql = " SELECT count(*) PAXCOUNT FROM T_ETL_PARSED WHERE ETL_ID = ? ";
		} else {
			params = new Object[] { new Integer(etlId), processedStatus };
			sql = " SELECT count(*) PAXCOUNT FROM T_ETL_PARSED WHERE ETL_ID = ? and PROC_STATUS = ? ";
		}

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql);
		log.debug(" SQL argument             : " + etlId);
		log.debug("############################################");

		Integer paxCount = (Integer) jt.query(sql, params, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				if (rs != null) {
					while (rs.next()) {
						return new Integer(rs.getInt("PAXCOUNT"));
					}
				}

				return new Integer(0);
			}
		});

		log.debug("Exit getPfsParseEntryCount");
		return paxCount.intValue();
	}

	/**
	 * Get Pax Names having error
	 * 
	 * @param etlID
	 * @param Status
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Collection<String> getPaxNamesForMail(Integer etlID, Collection<String> statuss) {

		if (etlID == null || statuss == null || statuss.size() == 0) {
			return null;
		}
		String sqlInStr = PlatformUtiltiies.constructINStringForCharactors(statuss);
		String sql = " SELECT pnr, title, first_name, last_name, pax_status, error_description, pax_type_code, et_no "
				+ " FROM t_etl_parsed where etl_id = ? and proc_status in ( " + sqlInStr + ")" + " order by pax_status ";

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);
		Object params[] = { etlID };

		Collection<String> paxDetails = (Collection<String>) template.query(sql, params, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<String> paxDetails = new ArrayList<String>();

				while (rs.next()) {
					String status = BeanUtils.nullHandler(rs.getString("pax_status"));
					String paxType = BeanUtils.nullHandler(rs.getString("pax_type_code"));

					if (status.equals(ReservationInternalConstants.PaxFareSegmentTypes.FLOWN)) {
						status = "FLOWN";
					} else {
						status = "NOT BOARDED";
					}

					String paxDetail = "ETICKET: " + BeanUtils.nullHandler(rs.getString("et_no")) + " PNR: "
							+ BeanUtils.nullHandler(rs.getString("pnr")) + " NAME : ["
							+ ReservationPax.getPassengerTypeDescription(paxType) + "] "
							+ BeanUtils.nullHandler(rs.getString("title")) + " "
							+ BeanUtils.nullHandler(rs.getString("first_name")) + " "
							+ BeanUtils.nullHandler(rs.getString("last_name")) + " " + status + "  ||"
							+ BeanUtils.nullHandler(rs.getString("error_description"));
					paxDetails.add(paxDetail);

				}

				return paxDetails;
			}

		});

		return paxDetails;
	}

	public boolean hasETL(String flightNo, Date departureDate, String airport, Collection<String> processedStatus) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		String hql = "select count(*) from ETL etl where etl.flightNumber = '" + flightNo + "' and trunc(etl.departureDate) ='"
				+ dateFormat.format(departureDate) + "' and etl.fromAirport='" + airport + "' ";
		if (processedStatus != null) {
			hql += " and etl.processedStatus in (" + Util.buildStringInClauseContent(processedStatus) + ")";
		}

		Query qCount = getSession().createQuery(hql);
		int count = ((Long) qCount.uniqueResult()).intValue();
		if (count > 0) {
			return true;
		} else {
			return false;
		}
	}

	public void updateETLEntry(ETL etl) {
		String hqlUpdate = "Update ETL set processedStatus = :status where etlId = :etlId";
		getSession().createQuery(hqlUpdate).setString("status", etl.getProcessedStatus()).setInteger("etlId", etl.getEtlId())
				.executeUpdate();
	}

	@Override
	public void saveCodeShareETLEntry(CodeShareETL etl) {
		log.debug("Inside saveCodeShareETLEntry");
		hibernateSaveOrUpdate(etl);
		log.debug("Exit saveCodeShareETLEntry");
	}
}