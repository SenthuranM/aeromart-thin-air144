package com.isa.thinair.messagepasser.core.bl.ssim;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.messagepasser.api.dto.ssim.CarrierRecordDTO;
import com.isa.thinair.messagepasser.api.dto.ssim.FlightScheduleRecordDTO;
import com.isa.thinair.messagepasser.api.dto.ssim.SSIMRecordDTO;
import com.isa.thinair.messagepasser.api.dto.ssim.FlightLegDTO;
import com.isa.thinair.messagepasser.api.model.SSIM;
import com.isa.thinair.messagepasser.api.model.SSIMParsedEntry;
import com.isa.thinair.messagepasser.api.utils.ParserConstants.SSIMProcessStatus;
import com.isa.thinair.messagepasser.api.utils.ParserConstants.SSIMScheduleProcessStatus;
import com.isa.thinair.messagepasser.core.bl.MessagePasserDAOUtils;
import com.isa.thinair.messagepasser.core.persistence.dao.SSIMDAO;

public class SSIMParser {

	public static final String HEADER_REC = "HEADER";
	public static final String CARRIER_REC = "CARRIER";
	public static final String FLIGHT_LEG = "FLT_LEG";
	public static final String SEGMENT_DATA = "SEG_DATA";

	public static final String HEADER_ELEM = "(1)(AIRLINE STANDARD SCHEDULE DATA SET)([0-9A-Z  ]{156}[0-9 ]{3})(000001)";
	// (1)(AIRLINE STANDARD SCHEDULE DATA SET)([0-9A-Z ]{156}[0-9 ]{3})(000001)
	public static final String CARRIER_ELEM = "(2)([UL]{1})([A-Z0-9 ]{3})([0-9 ]{5})([A-Z0-9 ]{3})([ ]{1})([A-Z0-9]{7})([A-Z0-9]{7})([A-Z0-9]{7}|)([A-Z0-9 ]{165,172})";

	public static final String FLIGHT_ELEM = "(3)([ ]{1})([A-Z0-9 ]{7})([0-9]{2})([0-9]{2})([A-Z]{1})([0-9]{2}[A-Z]{3}[0-9]{2})([0-9]{2}[A-Z]{3}[0-9]{2})([0-9 ]{0,7})([ ]{1})([A-Z]{3})([0-9]{4})([0-9]{4})([0-9-+]{5})([A-Z0-9 ]{2})([A-Z]{3})([0-9]{4})([0-9]{4})([0-9-+]{5})([A-Z0-9 ]{2})([A-Z0-9 ]{3})([A-Z0-9 ]{20})([A-Z ]{5})([A-Z ]{10})([A-Z0-9 ]{9})([DI ]{2})([ ]{6})([0-9 ]{1})([ ]{3})([ ]{3})([ ]{3})([A-Z0-9 ]{9})([ ]{2})([ A-Z]{24})([A-Z0-9 ]{20})([0-9 ]{2})([A-Z0-9 +]{6})";

	public static final String SEGMENT_ELEM = "(4)";
	// (4)([ ]{1})([A-Z0-9 ]{7})([0-9]{2})([0-9]{2})([A-Z]{1})([ ]{13})([
	// ]{1})([A-Z]{1})([A-Z]{1})([0-9]{3})([A-Z]{3})([A-Z]{3})([A-Z0-9 ]{155})([A-Z0-9 ]{6})

	private static Log log = LogFactory.getLog(SSIMParser.class);

	private int parsedFlightLegCount;
	private int failedFlightLegCount;
	private int multiLegScheduleCount;

	private SSIMDAO ssimDAO;

	public SSIMParser() {
		ssimDAO = MessagePasserDAOUtils.DAOInstance.SSIM_DAO;
	}

	private static String readFile(String fileName) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(fileName));
		try {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append("\n");
				line = br.readLine();
			}
			return sb.toString();
		} finally {
			br.close();
			br = null;
		}
	}

	private Map<String, String> getKeyTokensMap() {
		Map<String, String> keyTokensMap = new HashMap<String, String>();
		keyTokensMap.put(HEADER_REC, HEADER_ELEM);
		keyTokensMap.put(CARRIER_REC, CARRIER_ELEM);
		keyTokensMap.put(FLIGHT_LEG, FLIGHT_ELEM);
		keyTokensMap.put(SEGMENT_DATA, SEGMENT_ELEM);

		return keyTokensMap;

	}

	public boolean validSSIMMessageFormat(String rawMessage) {

		if (rawMessage != null) {
			Pattern pattern;
			Matcher matcher;

			String[] messageTokens = rawMessage.split("\n");
			boolean isHeaderDataMatched = false;
			boolean isCarrierDataMatched = false;
			boolean isFlightLegDataMatched = false;
			if (messageTokens != null && messageTokens.length > 0) {
				Map<String, String> keyTokensMap = getKeyTokensMap();
				for (int i = 0; i < messageTokens.length; i++) {
					String sElement = messageTokens[i];
					if (sElement != null) {
						sElement = sElement.trim();
					}

					Iterator<String> keyTokensMapKeysItr = keyTokensMap.keySet().iterator();

					while (keyTokensMapKeysItr.hasNext()) {
						String tokenKey = keyTokensMapKeysItr.next();
						String token = keyTokensMap.get(tokenKey);

						pattern = Pattern.compile(token);
						matcher = pattern.matcher(sElement);

						if (matcher.matches()) {
							if (tokenKey.equals(HEADER_REC)) {
								isHeaderDataMatched = true;
								keyTokensMapKeysItr.remove();
							} else if (tokenKey.equals(FLIGHT_LEG)) {
								isFlightLegDataMatched = true;
							} else if (tokenKey.equals(CARRIER_REC)) {
								isCarrierDataMatched = true;
								keyTokensMapKeysItr.remove();
							}

						}

					}

				}
			}

			if (isHeaderDataMatched && isCarrierDataMatched && isFlightLegDataMatched) {
				return true;
			}

		}
		return false;
	}

	public static void main(String[] args) {
		String path = "/home/rikaz/Desktop/GDS/SSIM/";
		String fileName = path;
		// fileName += "UF-SSIM.export";
		fileName += "ssim w15.txt";
		// fileName += "AirArabia_DSU-sent-2-6.txt";
		// fileName += "ku_ssim w16.txt";
		// fileName += "airkioskskd.last-skd.export";
		SSIMParser ss = new SSIMParser();
		String rawMessage = null;
		try {
			rawMessage = readFile(fileName);
			// log.info(rawMessage);

			if (rawMessage != null && ss.validSSIMMessageFormat(rawMessage)) {
				ss.parseSSIMMessage(rawMessage);
			} else {
				log.info("NOT-SSIM-FILE");
			}

		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}

	}

	public SSIMRecordDTO parseSSIMMessage(String rawMessage) throws ModuleException {
		log.info("Inside Parsing SSIM message.");
		SSIMRecordDTO sSIMRecordDTO = null;
		if (rawMessage != null) {
			sSIMRecordDTO = new SSIMRecordDTO();
			Pattern pattern;
			Matcher matcher;
			FlightScheduleRecordDTO masterSchedule = null;
			CarrierRecordDTO carrierRecordDTO = null;
			String[] messageTokens = rawMessage.split("\n");
			if (messageTokens != null && messageTokens.length > 0) {
				Map<String, String> keyTokensMap = getKeyTokensMap();
				for (int i = 0; i < messageTokens.length; i++) {
					String sElement = messageTokens[i];
					if (sElement != null) {
						sElement = sElement.trim();
					}
					// log.info("=> " + sElement);
					boolean parsedRaw = false;
					Iterator<String> keyTokensMapKeysItr = keyTokensMap.keySet().iterator();

					while (keyTokensMapKeysItr.hasNext()) {
						String tokenKey = keyTokensMapKeysItr.next();
						String token = keyTokensMap.get(tokenKey);

						pattern = Pattern.compile(token);
						matcher = pattern.matcher(sElement);

						if (matcher.matches()) {
							if (tokenKey.equals(HEADER_REC)) {
								parsedRaw = true;
								keyTokensMapKeysItr.remove();
							} else if (tokenKey.equals(FLIGHT_LEG)) {

								FlightScheduleRecordDTO flightLegRecord = extractFlightLegDetails(matcher, masterSchedule);

								if (flightLegRecord != null) {
									sSIMRecordDTO.addFlightLegRecord(flightLegRecord);
									masterSchedule = flightLegRecord;
								}
								parsedFlightLegCount++;
								parsedRaw = true;

							} else if (tokenKey.equals(CARRIER_REC)) {
								carrierRecordDTO = extractCarrierRecordDetails(matcher);
								sSIMRecordDTO.setCarrierRecord(carrierRecordDTO);
								insertSSIMCarrierInfo(carrierRecordDTO, rawMessage, sSIMRecordDTO);
								parsedRaw = true;
								keyTokensMapKeysItr.remove();
							}

						}

					}

					if (sElement.startsWith("3") && !parsedRaw) {
						failedFlightLegCount++;
						// log.info("=> " + sElement);
					}

				}
			}

			List<FlightScheduleRecordDTO> flightLegRecords = sSIMRecordDTO.getFlightLegRecords();
			Iterator<FlightScheduleRecordDTO> flightLegRecordItr = flightLegRecords.iterator();
			while (flightLegRecordItr.hasNext()) {
				FlightScheduleRecordDTO fltLegRecordDTO = flightLegRecordItr.next();
				insertSSIMParsedEntry(fltLegRecordDTO, sSIMRecordDTO.getSsimId());
				if (fltLegRecordDTO.getLegs() != null && fltLegRecordDTO.getLegs().size() > 1) {
					multiLegScheduleCount++;
				}
			}

			sSIMRecordDTO.setParsedFlightLegCount(this.parsedFlightLegCount);
			sSIMRecordDTO.setFailedFlightLegCount(this.failedFlightLegCount);
			sSIMRecordDTO.setMultiLegScheduleCount(this.multiLegScheduleCount);

			log.info("Total Schedule : " + this.parsedFlightLegCount);
			log.info("Failed schedule : " + this.failedFlightLegCount);
			log.info("Multileg Schedule : " + this.multiLegScheduleCount);

		}
		return sSIMRecordDTO;
	}

	private CarrierRecordDTO extractCarrierRecordDetails(Matcher matcher) {
		CarrierRecordDTO carrierRecordDTO = new CarrierRecordDTO();

		carrierRecordDTO.setRecordType(matcher.toMatchResult().group(1));
		carrierRecordDTO.setTimeMode(matcher.toMatchResult().group(2));
		carrierRecordDTO.setAirlineDesignator(matcher.toMatchResult().group(3));
		carrierRecordDTO.setSpare1(matcher.toMatchResult().group(4));
		carrierRecordDTO.setSeason(matcher.toMatchResult().group(5));
		carrierRecordDTO.setSpare2(matcher.toMatchResult().group(6));
		carrierRecordDTO.setScheduleValidityFrom(matcher.toMatchResult().group(7));
		carrierRecordDTO.setScheduleValidityTo(matcher.toMatchResult().group(8));
		carrierRecordDTO.setCreationDate(matcher.toMatchResult().group(9));
		carrierRecordDTO.setFreeText(matcher.toMatchResult().group(10));

		return carrierRecordDTO;

	}

	private FlightScheduleRecordDTO extractFlightLegDetails(Matcher matcher, FlightScheduleRecordDTO masterSchedule) {

		FlightScheduleRecordDTO fltLegRecordDTO = new FlightScheduleRecordDTO();

		int legSeq = 1;
		try {
			legSeq = Integer.parseInt(matcher.toMatchResult().group(5));
		} catch (Exception e) {
			legSeq = 1;
		}
		FlightLegDTO flightLegDTO = new FlightLegDTO();

		flightLegDTO.setDepartueStation(matcher.toMatchResult().group(11));
		flightLegDTO.setPassengerSTD(matcher.toMatchResult().group(12));
		flightLegDTO.setAircraftDeparture(matcher.toMatchResult().group(13));
		flightLegDTO.setDepartureVariation(matcher.toMatchResult().group(14));
		flightLegDTO.setDepartureTerminal(matcher.toMatchResult().group(15));
		flightLegDTO.setArrivalStation(matcher.toMatchResult().group(16));
		flightLegDTO.setAircraftArrival(matcher.toMatchResult().group(17));
		flightLegDTO.setPassengerSTA(matcher.toMatchResult().group(18));
		flightLegDTO.setArrivalVariation(matcher.toMatchResult().group(19));
		flightLegDTO.setArrivalTerminal(matcher.toMatchResult().group(20));

		if (legSeq > 1 && masterSchedule != null) {
			masterSchedule.addLegContent(matcher.group());
			masterSchedule.addLeg(flightLegDTO);
			masterSchedule.setArrivalStation(flightLegDTO.getArrivalStation());
			return null;
		} else {
			fltLegRecordDTO.addLegContent(matcher.group());
			fltLegRecordDTO.addLeg(flightLegDTO);
			fltLegRecordDTO.setRecordType(matcher.toMatchResult().group(1));
			fltLegRecordDTO.setOperationalSuffix(matcher.toMatchResult().group(2));
			fltLegRecordDTO.setFlightDesignator(matcher.toMatchResult().group(3));
			fltLegRecordDTO.setItineraryVariationIdentifier(matcher.toMatchResult().group(4));
			fltLegRecordDTO.setLegSequenceNo(legSeq + "");
			fltLegRecordDTO.setServiceType(matcher.toMatchResult().group(6));
			fltLegRecordDTO.setOperationPeriodFrom(matcher.toMatchResult().group(7));
			fltLegRecordDTO.setOperationPeriodTo(matcher.toMatchResult().group(8));
			fltLegRecordDTO.setOperationDays(matcher.toMatchResult().group(9));
			fltLegRecordDTO.setFrequencyRate(matcher.toMatchResult().group(10));

			fltLegRecordDTO.setDepartueStation(matcher.toMatchResult().group(11));
			// fltLegRecordDTO.setPassengerSTD(matcher.toMatchResult().group(12));
			// fltLegRecordDTO.setAircraftDeparture(matcher.toMatchResult().group(13));
			// fltLegRecordDTO.setDepartureVariation(matcher.toMatchResult().group(14));
			// fltLegRecordDTO.setDepartureTerminal(matcher.toMatchResult().group(15));
			fltLegRecordDTO.setArrivalStation(matcher.toMatchResult().group(16));
			// fltLegRecordDTO.setAircraftArrival(matcher.toMatchResult().group(17));
			// fltLegRecordDTO.setPassengerSTA(matcher.toMatchResult().group(18));
			// fltLegRecordDTO.setArrivalVariation(matcher.toMatchResult().group(19));
			// fltLegRecordDTO.setArrivalTerminal(matcher.toMatchResult().group(20));
			fltLegRecordDTO.setAircraftType(matcher.toMatchResult().group(21));
			fltLegRecordDTO.setPaxRBD(matcher.toMatchResult().group(22));
			fltLegRecordDTO.setPaxRBM(matcher.toMatchResult().group(23));
			fltLegRecordDTO.setMealServiceNote(matcher.toMatchResult().group(24));
			fltLegRecordDTO.setJointOperationAirline(matcher.toMatchResult().group(25));
			fltLegRecordDTO.setMinConnTimeStatus(matcher.toMatchResult().group(26));
			fltLegRecordDTO.setSpare(matcher.toMatchResult().group(27));
			fltLegRecordDTO.setItineraryVariationIdOverflow(matcher.toMatchResult().group(28));
			fltLegRecordDTO.setAircraftOwner(matcher.toMatchResult().group(29));
			fltLegRecordDTO.setCockpitCrew(matcher.toMatchResult().group(30));
			fltLegRecordDTO.setCabinCrew(matcher.toMatchResult().group(31));
			fltLegRecordDTO.setOnwardFlight(matcher.toMatchResult().group(32));
			fltLegRecordDTO.setFlightTransitLayover(matcher.toMatchResult().group(33));
			fltLegRecordDTO.setSkipped(matcher.toMatchResult().group(34));
			fltLegRecordDTO.setAircraftConfiguration(matcher.toMatchResult().group(35));
			fltLegRecordDTO.setDateVariation(matcher.toMatchResult().group(36));
			fltLegRecordDTO.setRecordSerialNo(matcher.toMatchResult().group(37));
		}
		return fltLegRecordDTO;

	}

	private void insertSSIMCarrierInfo(CarrierRecordDTO carrierRecordDTO, String rawMessage, SSIMRecordDTO sSIMRecordDTO)
			throws ModuleException {
		SSIM ssim = new SSIM();
		ssim.setProcessDate(CalendarUtil.getCurrentSystemTimeInZulu());
		ssim.setProcessStatus(SSIMProcessStatus.PARSED);
		ssim.setAirlineCode(carrierRecordDTO.getAirlineDesignator());
		try {
			if (!StringUtil.isNullOrEmpty(carrierRecordDTO.getCreationDate())) {
				ssim.setCreationDate(CalendarUtil.getParsedTime(carrierRecordDTO.getCreationDate(), CalendarUtil.PATTERN6));
			} else {
				ssim.setCreationDate(CalendarUtil.getCurrentSystemTimeInZulu());
			}
		} catch (ParseException e) {
			// TODO: handle exception
		}

		ssim.setMessageText(rawMessage);

		ssimDAO.saveSSIMEntry(ssim);
		sSIMRecordDTO.setSsimId(ssim.getSsimId());
	}

	private void insertSSIMParsedEntry(FlightScheduleRecordDTO flightScheduleRecordDTO, int ssimId) throws ModuleException {
		SSIMParsedEntry ssimParsedEntry = new SSIMParsedEntry();
		ssimParsedEntry.setSsimId(ssimId);
		ssimParsedEntry.setFlightDesignator(flightScheduleRecordDTO.getFlightDesignator());
		try {
			ssimParsedEntry.setPeriodFrom(CalendarUtil.getParsedTime(flightScheduleRecordDTO.getOperationPeriodFrom(),
					CalendarUtil.PATTERN6));
			ssimParsedEntry.setPeriodTo(CalendarUtil.getParsedTime(flightScheduleRecordDTO.getOperationPeriodTo(),
					CalendarUtil.PATTERN6));
		} catch (ParseException e) {
			// TODO: handle exception
		}
		ssimParsedEntry.setOrigin(flightScheduleRecordDTO.getDepartueStation());
		ssimParsedEntry.setDestination(flightScheduleRecordDTO.getArrivalStation());
		ssimParsedEntry.setScheduleStatus(SSIMScheduleProcessStatus.PARSED);
		ssimParsedEntry.setNoLegs(flightScheduleRecordDTO.getLegs().size());
		ssimParsedEntry.setMessageText(flightScheduleRecordDTO.getFlightLegContent());
		ssimDAO.saveSSIMParsedEntry(ssimParsedEntry);
		flightScheduleRecordDTO.setSsimParsedId(ssimParsedEntry.getSsimParsedId());
	}

}
