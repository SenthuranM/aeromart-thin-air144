/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.messagepasser.core.bl.emirates;

import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.messagepasser.api.dto.emirates.XAPnlLogDTO;
import com.isa.thinair.messagepasser.api.utils.MessagePasserModuleUtils;
import com.isa.thinair.messaging.api.dto.LogDetailDTO;
import com.isa.thinair.platform.api.constants.PlatformConstants;

/**
 * @author Byorn
 * 
 * @TODO: Can refactor this method as similar method exists in ReservationAuxilliaryBL.savePNLADLErrorLog()
 * 
 *        Following will except a DTO and will create a log under /isalogs/audits
 */
public class XAPnlReconcilationLogger {

	private static Log log = LogFactory.getLog(XAPnlReconcilationLogger.class);

	public static String createErrorLog(XAPnlLogDTO logDto) {
		try {
			LogDetailDTO logDetailDTO = new LogDetailDTO();
			String fileName = logDto.getFileName() + ".log";

			logDetailDTO.setSaveFileName(PlatformConstants.getAbsXAPnlAuditLogsPath() + "/" + fileName);

			logDetailDTO.setTemplateName("xa_pnl_log");

			HashMap map = new HashMap();
			map.put("xaPnlLogDTO", logDto);
			logDetailDTO.setTemplateParams(map);
			MessagePasserModuleUtils.getMessagingServiceBD().recordMessage(logDetailDTO);

			return PlatformConstants.getAbsXAPnlAuditLogsPath() + "/" + fileName;
		} catch (Exception ex) {
			log.error(ex);
		}
		return null;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			throw new Exception();
		} catch (Exception e) {

			XAPnlLogDTO logDTO = new XAPnlLogDTO();
			logDTO.setExceptionDescription("something fsfsdf dsf sdfsomthing");
			logDTO.setStackTraceElements(e.getStackTrace());
			logDTO.setFileName("XAPNL-000123323.txt");
			XAPnlReconcilationLogger.createErrorLog(logDTO);

		}

	}

}
