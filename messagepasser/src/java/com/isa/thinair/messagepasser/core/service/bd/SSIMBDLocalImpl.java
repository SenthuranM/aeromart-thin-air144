package com.isa.thinair.messagepasser.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.messagepasser.api.service.SSIMBD;

/**
 * @author Rikaz
 */
@Local
public interface SSIMBDLocalImpl extends SSIMBD {

}
