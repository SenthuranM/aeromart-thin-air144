package com.isa.thinair.messagepasser.core.bl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airschedules.api.dto.FlightAlertDTO;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.FlightScheduleLeg;
import com.isa.thinair.airschedules.api.model.FlightScheduleSegment;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.service.ScheduleBD;
import com.isa.thinair.airschedules.api.utils.FlightUtil;
import com.isa.thinair.airschedules.api.utils.GDSSchedConstants;
import com.isa.thinair.commons.api.constants.DayOfWeek;
import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.gdsservices.api.util.GDSApiUtils;
import com.isa.thinair.messagepasser.api.dto.ssim.CarrierRecordDTO;
import com.isa.thinair.messagepasser.api.dto.ssim.FlightLegDTO;
import com.isa.thinair.messagepasser.api.dto.ssim.FlightScheduleRecordDTO;
import com.isa.thinair.messagepasser.api.dto.ssim.SSIMRecordDTO;
import com.isa.thinair.messagepasser.api.model.SSIM;
import com.isa.thinair.messagepasser.api.model.SSIMParsedEntry;
import com.isa.thinair.messagepasser.api.utils.MessagePasserModuleUtils;
import com.isa.thinair.messagepasser.api.utils.ParserConstants.SSIMProcessStatus;
import com.isa.thinair.messagepasser.api.utils.ParserConstants.SSIMScheduleProcessStatus;
import com.isa.thinair.messagepasser.core.audit.ProcSSIMAudit;
import com.isa.thinair.messagepasser.core.persistence.dao.SSIMDAO;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command for reconciling reservation
 * 
 * 
 * @author Rikaz
 * @since 1.0
 * @isa.module.command name="scheduleCreation"
 */
public class ScheduleCreation extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(ScheduleCreation.class);

	private static String TIME_FORMAT = "HHmm";
	private static String LOCAL_TIME = "L";
	private static String marketingFltDesignatorPrefix = "2";

	private SSIMDAO ssimDAO;
	private ScheduleBD scheduleBD;

	/**
	 * Construct ScheduleCreation
	 * 
	 * @throws ModuleException
	 */

	private ScheduleCreation() throws ModuleException {
		ssimDAO = MessagePasserDAOUtils.DAOInstance.SSIM_DAO;
		scheduleBD = MessagePasserModuleUtils.getScheduleBD();
	}

	/**
	 * Execute method of the ScheduleCreation command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside ScheduleCreation execute");
		SSIMRecordDTO SSIMRecordDTO = (SSIMRecordDTO) this.getParameter(GDSSchedConstants.ParamNames.SSIM_RECORD_DTO);

		CarrierRecordDTO carrierRecordDTO = SSIMRecordDTO.getCarrierRecord();
		String carrierCode = carrierRecordDTO.getAirlineDesignator();
		if (!StringUtil.isNullOrEmpty(carrierCode)) {
			carrierCode = carrierCode.replaceAll("\\s", "");
		}
		boolean isDefaultCarrier = false;
		boolean isCodeShareCarrier = false;
		String defaultCarrier = AppSysParamsUtil.getDefaultCarrierCode();
		GDSStatusTO gdsCarrierTO = null;
		if (!defaultCarrier.equals(carrierCode)) {
			Map<String, GDSStatusTO> gdsStatusMap = CommonsServices.getGlobalConfig().getActiveGdsMap();
			for (GDSStatusTO gdsStatusTO : gdsStatusMap.values()) {
				if (gdsStatusTO.getCarrierCode() != null && gdsStatusTO.getCarrierCode().equals(carrierCode)) {
					gdsCarrierTO = gdsStatusTO;
					if (gdsStatusTO.isCodeShareCarrier()) {
						isCodeShareCarrier = true;
					}
				}
			}

		} else {
			isDefaultCarrier = true;
		}

		SSIM ssim = new SSIM();
		ssim.setSsimId(SSIMRecordDTO.getSsimId());
		if (gdsCarrierTO == null && !isDefaultCarrier) {
			// SKIP Processing SSIM...... Carrier code not configured, NOT A CODESHARE or GDS carrier
			ssim.setProcessStatus(SSIMProcessStatus.ERROR_OCCURED);
			ssim.setErrorDescription("RECEIVED SSIM UN-KNOWN AIRLINE CODE");

		} else {

			Collection<String> existingFlightNumbers = scheduleBD.getFlightNumbers();

			String timeMode = carrierRecordDTO.getTimeMode();
			ssim.setProcessStatus(SSIMProcessStatus.COMPLETED);
			String errorAt = "";
			// validate carrier code
			// if code-share need a flag & prefix element for flight no,
			List<FlightScheduleRecordDTO> flightLegRecords = SSIMRecordDTO.getFlightLegRecords();
			Iterator<FlightScheduleRecordDTO> flightLegRecordItr = flightLegRecords.iterator();
			while (flightLegRecordItr.hasNext()) {
				SSIMParsedEntry ssimParsedEntry = new SSIMParsedEntry();
				FlightScheduleRecordDTO flightLegRecordDTO = flightLegRecordItr.next();
				ssimParsedEntry.setSsimParsedId(flightLegRecordDTO.getSsimParsedId());
				try {

					FlightSchedule schedule = new FlightSchedule();
					String modelNo = getAircraftModelNo(flightLegRecordDTO.getAircraftType());
					String flightDesignator = flightLegRecordDTO.getFlightDesignator();
					// String flightNo = flightDesignator.replaceAll("\\s", "");

					Date startDate = CalendarUtil.parseDate(flightLegRecordDTO.getOperationPeriodFrom(), false);
					Date stopDate = CalendarUtil.parseDate(flightLegRecordDTO.getOperationPeriodTo(), false);

					Collection<DayOfWeek> dayList = new ArrayList<DayOfWeek>();
					String abc = flightLegRecordDTO.getOperationDays();
					// int i = 0;
					if (abc != null && !abc.trim().equals("") && abc.trim().length() > 0) {
						String operationDay[] = abc.split("");

						for (int h = 0; h < operationDay.length; h++) {
							if (!StringUtil.isNullOrEmpty(operationDay[h])) {

								int key = Integer.parseInt(operationDay[h]);
								switch (key) {
								case 1:
									dayList.add(DayOfWeek.MONDAY);
									break;
								case 2:
									dayList.add(DayOfWeek.TUESDAY);
									break;
								case 3:
									dayList.add(DayOfWeek.WEDNESDAY);
									break;
								case 4:
									dayList.add(DayOfWeek.THURSDAY);
									break;
								case 5:
									dayList.add(DayOfWeek.FRIDAY);
									break;
								case 6:
									dayList.add(DayOfWeek.SATURDAY);
									break;
								case 7:
									dayList.add(DayOfWeek.SUNDAY);
									break;
								}

							}
						}

					}

					if (!StringUtil.isNullOrEmpty(flightDesignator) && flightDesignator.length() <= 7) {
						flightDesignator = flightDesignator.replaceAll("\\s", "");
						if (isCodeShareCarrier) {
							if (!StringUtil.isNullOrEmpty(gdsCarrierTO.getMarketingFlightPrefix())) {
								marketingFltDesignatorPrefix = gdsCarrierTO.getMarketingFlightPrefix();
							}
							String marketingFlightDesignator = GDSApiUtils.composeMarketingFlightDesignator(flightDesignator.trim(),
									marketingFltDesignatorPrefix, carrierCode);
							if (!StringUtil.isNullOrEmpty(marketingFlightDesignator)) {
								schedule.setCsOCCarrierCode(carrierCode);
								schedule.setCsOCFlightNumber(flightDesignator);
								schedule.setFlightNumber(marketingFlightDesignator);
							} else {
								throw new ModuleException("error.composing.marketing.flight.designator");
							}

						} else {
							schedule.setFlightNumber(flightDesignator.replaceAll("\\s", ""));
						}
					} else {
						// Invalid FlightDesignator
						throw new ModuleException("invalid.flight.designator");
					}

					schedule.setOperationTypeId(2);

					Frequency fqn = CalendarUtil.getFrequencyFromDays(dayList);

					String origin = flightLegRecordDTO.getDepartueStation();
					String destination = flightLegRecordDTO.getArrivalStation();

					Collection<FlightLegDTO> legs = flightLegRecordDTO.getLegs();
					Set<FlightScheduleLeg> legSet = new HashSet<FlightScheduleLeg>();
					int n = 1;
					schedule.setDepartureStnCode(origin);
					schedule.setArrivalStnCode(destination);

					if (legs != null && !legs.isEmpty()) {
						for (FlightLegDTO legDTO : legs) {

							FlightScheduleLeg fScheLeg = new FlightScheduleLeg();
							fScheLeg.setOrigin(legDTO.getDepartueStation());
							fScheLeg.setDestination(legDTO.getArrivalStation());
							String depaturTime = legDTO.getAircraftDeparture();
							String arrivalTime = legDTO.getAircraftArrival();

							boolean arrivalNextDay = isArrivalNextDay(CalendarUtil.getParsedTime(depaturTime, TIME_FORMAT),
									CalendarUtil.getParsedTime(arrivalTime, TIME_FORMAT), legDTO.getDepartureVariation(),
									legDTO.getArrivalVariation());

							int arrivalOffset = 0;
							if (arrivalNextDay) {
								arrivalOffset = 1;
							}

							if (LOCAL_TIME.equalsIgnoreCase(timeMode)) {
								fScheLeg.setEstDepartureTimeLocal(CalendarUtil.getParsedTime(depaturTime, TIME_FORMAT));
								fScheLeg.setEstArrivalTimeLocal(CalendarUtil.getParsedTime(arrivalTime, TIME_FORMAT));
								fScheLeg.setEstArrivalDayOffsetLocal(arrivalOffset);
								// fScheLeg.setEstArrivalDayOffsetLocal(legDTO.getArrivalOffset());
								// fScheLeg.setEstDepartureDayOffsetLocal(legDTO.getDepatureOffiset());
								schedule.setFrequencyLocal(fqn);
								schedule.setStartDateLocal(startDate);
								schedule.setStopDateLocal(stopDate);
								schedule.setLocalChang(true);
								schedule.setStartDate(null);
								schedule.setStopDate(null);
								schedule.setFrequency(null);

							} else {
								fScheLeg.setEstDepartureTimeZulu(CalendarUtil.getParsedTime(depaturTime, TIME_FORMAT));
								fScheLeg.setEstArrivalTimeZulu(CalendarUtil.getParsedTime(arrivalTime, TIME_FORMAT));
								fScheLeg.setEstArrivalDayOffset(arrivalOffset);
								// fScheLeg.setEstArrivalDayOffset(legDTO.getArrivalOffset());
								// fScheLeg.setEstDepartureDayOffset(legDTO.getDepatureOffiset());
								schedule.setFrequency(fqn);
								schedule.setStartDate(startDate);
								schedule.setStopDate(stopDate);
								schedule.setLocalChang(false);
								schedule.setStartDateLocal(null);
								schedule.setStopDateLocal(null);
								schedule.setFrequencyLocal(null);
							}
							fScheLeg.setLegNumber(n);
							n++;
							legSet.add(fScheLeg);

						}
					}

					schedule.setFlightScheduleLegs(legSet);
					updateScheduleSegments(schedule, legSet);

					setFlightType(schedule.getDepartureStnCode(), schedule.getArrivalStnCode(), schedule);

					// check flight no exist && schedule overlap
					if (existingFlightNumbers.contains(schedule.getFlightNumber())) {
						log.info("possible for flight conflict");
						// check schedule overlap
					}

					schedule.setModelNumber(modelNo);
					schedule.setViaScheduleMessaging(true);

					errorAt = "schedule creation";
					ServiceResponce serviceResponce = MessagePasserModuleUtils.getScheduleBD().createSchedule(schedule, true,
							AppSysParamsUtil.isEnableAttachRouteWiseDefAnciTemplAtSSIMProc());
					Integer scheduleId = (Integer) serviceResponce.getResponseParam(GDSSchedConstants.ParamNames.SCHEDULE_ID);
					ProcSSIMAudit.doAuditProcSSIM(null, null, scheduleId, schedule, ProcSSIMAudit.Operation.CREATE,
							serviceResponce.getResponseCode(), serviceResponce.isSuccess());
					if (serviceResponce != null && serviceResponce.isSuccess()) {

						errorAt = "schedule created with id : " + scheduleId;
						if (scheduleId != null) {
							Collection<Integer> scheduleIds = new ArrayList<Integer>();
							scheduleIds.add(scheduleId);
							errorAt = "flight creation for : " + scheduleId;
							serviceResponce = MessagePasserModuleUtils.getScheduleBD().buildSchedule(scheduleIds, false);
							if (!serviceResponce.isSuccess()) {
								errorAt = "flight creation failed cancelling schedule : " + scheduleId;
								MessagePasserModuleUtils.getScheduleBD().cancelSchedule(scheduleId, false, new FlightAlertDTO(),
										false, false);
							}
						}
						ssimParsedEntry.setScheduleStatus(SSIMScheduleProcessStatus.SCHEDULE_CREATED);
						ssimParsedEntry.setErrorDescription(null);

					} else {
						// schedule creation failed
						ssimParsedEntry.setScheduleStatus(SSIMScheduleProcessStatus.ERROR_OCCURED);
						ssimParsedEntry.setErrorDescription(serviceResponce.getResponseCode());

					}
				} catch (ModuleException ex) {
					log.error("ERROR @ FlightSchedule");
					ssimParsedEntry.setScheduleStatus(SSIMScheduleProcessStatus.ERROR_OCCURED);
					ssimParsedEntry.setErrorDescription("ERROR @ " + errorAt + " - " + ex.getExceptionCode());
				} catch (Exception e) {
					ssimParsedEntry.setScheduleStatus(SSIMScheduleProcessStatus.ERROR_OCCURED);
					ssimParsedEntry.setErrorDescription("ERROR @ " + errorAt);
					log.error("ERROR @ FlightSchedule");
				}

				ssimDAO.updateSSIMParsedEntryStatus(ssimParsedEntry);
			}
		}
		ssimDAO.updateSSIMEntryStatus(ssim);
		DefaultServiceResponse response = new DefaultServiceResponse(true);
		log.debug("Exit execute");
		return response;
	}

	private static void updateScheduleSegments(FlightSchedule schedule, Set<FlightScheduleLeg> legSet) {
		HashSet<FlightScheduleSegment> segset = new HashSet<FlightScheduleSegment>();
		HashMap<String, FlightSegement> map = new HashMap<String, FlightSegement>();
		for (int i = 1; i < legSet.size() + 1; i++) {

			Iterator<FlightScheduleLeg> legIt = legSet.iterator();
			while (legIt.hasNext()) {

				FlightScheduleLeg leg1 = (FlightScheduleLeg) legIt.next();
				if (leg1.getLegNumber() == i) {

					String segmentCode = leg1.getOrigin() + "/" + leg1.getDestination();

					// create the segment
					FlightScheduleSegment seg1 = new FlightScheduleSegment();
					seg1.setSegmentCode(segmentCode);
					// This is to set terminal ids for the flight
					if (!map.isEmpty() && null != map.get(segmentCode)) {
						seg1.setArrivalTerminalId(((FlightSegement) map.get(segmentCode)).getArrivalTerminalId());
						seg1.setDepartureTerminalId(((FlightSegement) map.get(segmentCode)).getDepartureTerminalId());
					}
					segset.add(seg1);

					// creating following segments
					for (int j = i + 1; j < legSet.size() + 1; j++) {

						Iterator<FlightScheduleLeg> restLegIt = legSet.iterator();
						while (restLegIt.hasNext()) {

							FlightScheduleLeg leg2 = (FlightScheduleLeg) restLegIt.next();
							if (leg2.getLegNumber() == j) {

								segmentCode = segmentCode + "/" + leg2.getDestination();

								// create folllowing segment
								FlightScheduleSegment seg2 = new FlightScheduleSegment();
								seg2.setSegmentCode(segmentCode);
								// This is to set terminal ids for the flight
								if (!map.isEmpty() && null != map.get(segmentCode)) {
									seg2.setArrivalTerminalId(((FlightSegement) map.get(segmentCode)).getArrivalTerminalId());
									seg2.setDepartureTerminalId(((FlightSegement) map.get(segmentCode)).getDepartureTerminalId());
								}
								segset.add(seg2);

								break;
							}
						}
					}
					break;
				}
			}
		}
		schedule.setFlightScheduleSegments(segset);
	}

	public static String getAircraftModelNo(String iataType) throws ModuleException {
		// TODO:pass aircraft version/configuration for equipment accuracy
		String modelNumber = GDSApiUtils.getActiveAircraftModelNumberByIataType(iataType, null);

		if (StringUtil.isNullOrEmpty(modelNumber)) {
			throw new ModuleException("gdsservices.airschedules.logic.bl.aircraft.type.invalid");
		}

		return modelNumber;
	}

	private static void setFlightType(String origin, String destination, FlightSchedule schedule) {
		if (!StringUtil.isNullOrEmpty(origin) && !StringUtil.isNullOrEmpty(destination)) {
			boolean isDomesticRoute = FlightUtil.isDomesticRoute(origin, destination);
			if (isDomesticRoute) {
				schedule.setFlightType(FlightUtil.FLIGHT_TYPE_DOM);
			} else {
				schedule.setFlightType(FlightUtil.FLIGHT_TYPE_INT);
			}
		} else {
			schedule.setFlightType(FlightUtil.FLIGHT_TYPE_INT);
		}

	}

	private static boolean isArrivalNextDay(Date startTime, Date endTime, String depVariation, String arrVariation)
			throws ParseException {
		boolean isNextDay = false;

		String reg = "[+-]{1}[0]{4}";
		if (!StringUtil.isNullOrEmpty(depVariation) && !StringUtil.isNullOrEmpty(arrVariation) && arrVariation.matches(reg)
				&& depVariation.matches(reg)) {
			// Start Time
			Calendar startCalendar = Calendar.getInstance();
			startCalendar.setTime(startTime);

			// Current Time
			java.util.Date currentTime = new SimpleDateFormat("HH:mm:ss").parse("00:00:00");
			Calendar currentCalendar = Calendar.getInstance();
			currentCalendar.setTime(currentTime);

			// End Time
			Calendar endCalendar = Calendar.getInstance();
			endCalendar.setTime(endTime);

			//
			if (currentTime.compareTo(endTime) < 0) {

				currentCalendar.add(Calendar.DATE, 1);
				currentTime = currentCalendar.getTime();

			}

			if (startTime.compareTo(endTime) < 0) {

				startCalendar.add(Calendar.DATE, 1);
				startTime = startCalendar.getTime();

			}

			if (currentTime.before(startTime)) {
				// Time is Lesser
				isNextDay = false;
			} else {
				if (currentTime.after(endTime)) {
					// day +
					endCalendar.add(Calendar.DATE, 1);
					endTime = endCalendar.getTime();

				}

				if (currentTime.before(endTime)) {
					// RESULT, Time lies b/w
					isNextDay = true;
				} else {
					// RESULT, Time does not lies b/w
					isNextDay = false;

				}

			}
		}

		return isNextDay;
	}
}