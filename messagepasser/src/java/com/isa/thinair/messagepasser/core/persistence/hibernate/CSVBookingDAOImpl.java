/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.messagepasser.core.persistence.hibernate;

// Java API
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.messagepasser.api.model.CSVBookingEntry;
import com.isa.thinair.messagepasser.api.utils.MessagePasserModuleUtils;
import com.isa.thinair.messagepasser.core.persistence.dao.CSVBookingDAO;

/**
 * 
 * @author
 * @since 1.0
 * @isa.module.dao-impl dao-name="CSVBookingDAO"
 */
public class CSVBookingDAOImpl extends PlatformBaseHibernateDaoSupport implements CSVBookingDAO {

	public CSVBookingEntry getBookingById(int paxId) {

		return (CSVBookingEntry) get(CSVBookingEntry.class, new Integer(paxId));
	}

	public void saveCSVBookingEntry(CSVBookingEntry csvBookingEntry) {
		hibernateSaveOrUpdate(csvBookingEntry);
	}

	public Collection getBookingEntries(String processedStatus) {

		String hql = " SELECT csvBookingEntry FROM CSVBookingEntry AS csvBookingEntry";

		if (processedStatus != null) {
			hql += " where csvBookingEntry.processedStatus like '" + processedStatus + "'";
		}

		return find(hql, CSVBookingEntry.class);

	}

	public Collection getBookingEntriesByPnr(String processStatus, String pnr) {

		String hql = " SELECT csvBookingEntry FROM CSVBookingEntry AS csvBookingEntry where csvBookingEntry.processedStatus like '"
				+ processStatus + "' AND csvBookingEntry.pnr ='" + pnr + "'";

		return find(hql, CSVBookingEntry.class);
	}

	/**
	 * INITIATED = "I" PROCESSED = "P" NOT_PROCESSED = "N" ERROR_OCCURED = "E"
	 * */

	public Collection getUniquePnrByStatus(String processedStatus, int recordCount) {

		String sql = "SELECT DISTINCT pnr FROM t_csv_booking_temp WHERE processstatus=? AND ROWNUM < ?";

		DataSource ds = MessagePasserModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);

		Object params[] = { processedStatus, recordCount };

		Collection uniquePnrColl = (Collection) template.query(sql, params, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection csvBookPnrs = new ArrayList();

				while (rs.next()) {
					csvBookPnrs.add(rs.getString(1));
				}
				return csvBookPnrs;

			}

		});

		return uniquePnrColl;

	}

	public boolean isGroupPnrDataConsistent(String pnr) {

		String sql = "select distinct processstatus from t_csv_booking_temp where pnr=?";

		DataSource ds = MessagePasserModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);

		Object params[] = { pnr };

		Boolean validDataEntries = (Boolean) template.query(sql, params, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<String> statusList = new ArrayList<String>();

				while (rs.next()) {
					statusList.add(rs.getString(1));
				}

				if (statusList.size() > 1) {
					return false;
				}

				return true;

			}

		});

		return validDataEntries.booleanValue();

	}
}