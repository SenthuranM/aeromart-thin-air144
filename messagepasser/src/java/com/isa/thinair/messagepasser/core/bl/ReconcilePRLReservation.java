package com.isa.thinair.messagepasser.core.bl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegmentETicket;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.core.persistence.dao.ReservationDAO;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.commons.core.util.Util;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airreservation.core.persistence.dao.ETicketDAO;
import com.isa.thinair.airreservation.core.persistence.dao.ReservarionPaxFareSegDAO;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.service.AuditorBD;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.messagepasser.api.model.PRL;
import com.isa.thinair.messagepasser.api.model.PRLPaxEntry;
import com.isa.thinair.messagepasser.api.utils.ParserConstants;
import com.isa.thinair.messagepasser.core.persistence.dao.PRLDAO;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command for reconciling reservation
 * 
 * 
 * @author Ishan
 * @since 1.0
 * @isa.module.command name="reconcilePRLReservation"
 */
@SuppressWarnings("unchecked") public class ReconcilePRLReservation extends DefaultBaseCommand {

	/** Holds the logger instance */
	private static final Log log = LogFactory.getLog(ReconcilePRLReservation.class);

	/** Holds the prl dao instance */
	private PRLDAO prlDAO;

	private ETicketDAO eticketDAO;

	private ReservarionPaxFareSegDAO reservarionPaxFareSegDAO;

	/**
	 * Holds the reservation dao instance
	 */
	private ReservationDAO reservationDAO;

	/**
	 * Hold the reservation bd instance
	 **/
	private ReservationBD reservationBD;

	private static String FLOWN = "FLOWN";
	private static String NOT_BOARDED = "NOT BOARDED";
	private static final String PAX_CHILD_TYPE = "CH";
	private static final String PAX_CHILD = "CHILD";

	/**
	 * Construct ReconcilePRLReservation
	 * 
	 * @throws ModuleException
	 */
	private ReconcilePRLReservation() throws ModuleException {
		prlDAO = MessagePasserDAOUtils.DAOInstance.PRL_DAO;
		reservarionPaxFareSegDAO = ReservationDAOUtils.DAOInstance.RESERVATION_PAXFARE_SEG_DAO;
		eticketDAO = ReservationDAOUtils.DAOInstance.ETicketDAO;
		reservationDAO = ReservationDAOUtils.DAOInstance.RESERVATION_DAO;
		reservationBD = ReservationModuleUtils.getReservationBD();
	}

	/**
	 * Execute method of the ReconcilePRLReservation command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {
		log.debug("Inside execute");

		CredentialsDTO credentialsDTO = (CredentialsDTO) this.getParameter(CommandParamNames.CREDENTIALS_DTO);
		Integer prlId = (Integer) this.getParameter(CommandParamNames.PRL_ID);
		ReservationAudit reservationAudit = new ReservationAudit();
		Collection colReservationAudit = new ArrayList();

		this.validateParams(prlId);

		PRL prl = prlDAO.getPRLEntry(prlId);

		// Returns PRL entries
		Collection colPRLPaxEntry = prlDAO.getPRLPaxEntries(prlId, null);

		log.debug(" ###################### Starting the process of PRL ID: " + prlId);

		Collection<PRLPaxEntry> colErrorPRLPaxEntry = new ArrayList<PRLPaxEntry>();
		Iterator itColPRLPaxEntry = colPRLPaxEntry.iterator();
		PRLPaxEntry prlPaxEntry;
		Collection<ReservationPaxFareSegment> paxFareSegmentsToSave = new ArrayList<ReservationPaxFareSegment>();
		while (itColPRLPaxEntry.hasNext()) {
			prlPaxEntry = (PRLPaxEntry) itColPRLPaxEntry.next();
			reservationAudit = new ReservationAudit();

			try {
				boolean isValidETDetails = validateEticketDeatils(reservationAudit, colReservationAudit, prl,
						prlPaxEntry, paxFareSegmentsToSave);

				if (validatePNR(prlPaxEntry) && isValidETDetails && validatePassportDetails(prlPaxEntry)) {
					prlPaxEntry.setProcessedStatus(ParserConstants.PRLProcessStatus.PROCESSED);
				} else {
					prlPaxEntry.setProcessedStatus(ParserConstants.PRLProcessStatus.ERROR_OCCURED);
					colErrorPRLPaxEntry.add(prlPaxEntry);
				}

			} catch (Exception ex) {
				prlPaxEntry.setPnr(prlPaxEntry.getPnr());
				prlPaxEntry.setErrorDescription(String.valueOf(ex));
				prlPaxEntry.setProcessedStatus(ParserConstants.PRLProcessStatus.ERROR_OCCURED);
				colErrorPRLPaxEntry.add(prlPaxEntry);
				prlDAO.savePRLPaxEntry(prlPaxEntry);
				log.error("Exception Occurred while parsing PRL " + prlPaxEntry.getPrlId());
			}
		}

		reservarionPaxFareSegDAO.savePaxFareSegments(paxFareSegmentsToSave);
		prlDAO.savePRLPaxEntries(colPRLPaxEntry);

		if (colErrorPRLPaxEntry.size() == 0) {
			prl.setProcessedStatus(ParserConstants.PRLStatus.RECONCILE_SUCCESS);
		} else {
			prl.setProcessedStatus(ParserConstants.PRLProcessStatus.ERROR_OCCURED);
		}
		log.debug(" ###################### Ended the PRL processing ");
		prlDAO.savePRLEntry(prl);

		DefaultServiceResponse response = this.composeResponse(colErrorPRLPaxEntry);

		//this.postPRLAuditRecord(colReservationAudit, credentialsDTO);
		log.debug("Exit execute");
		return response;
	}

	/**
	 * Validate Etickets Details
	 *
	 * @param reservationAudit
	 * @param colReservationAudit
	 * @param prl
	 * @param prlPaxEntry
	 * @param paxFareSegmentsToSave
	 * @throws ModuleException
	 */
	private boolean validateEticketDeatils(ReservationAudit reservationAudit, Collection colReservationAudit, PRL prl,
			PRLPaxEntry prlPaxEntry, Collection<ReservationPaxFareSegment> paxFareSegmentsToSave)
			throws ModuleException {
		boolean isValidET = true;
		String messageType = ParserConstants.MessageTypes.PRL.toString();
		Collection<Object[]> eticketNumbersToReconcile = new ArrayList<>();
		if (!StringUtil.isNullOrEmpty(prlPaxEntry.getEticketNumber())) {
			eticketNumbersToReconcile.add(new Object[] { prlPaxEntry.getEticketNumber(), prlPaxEntry.getCoupNumber() });
		}
		// If infant details is attached to this adult reconcile those
		// as well.
		if (!StringUtil.isNullOrEmpty(prlPaxEntry.getInfEticketNumber())) {
			eticketNumbersToReconcile
					.add(new Object[] { prlPaxEntry.getInfEticketNumber(), prlPaxEntry.getInfCoupNumber() });
		}
		for (Object[] eticketDetails : eticketNumbersToReconcile) {
			ReservationPaxFareSegmentETicket paxFareSegmentETicket = eticketDAO
					.getPaxFareSegmentEticket(eticketDetails[0].toString(), new Integer(eticketDetails[1].toString()),
							messageType);
			// TODO implement this
			ReservationPaxFareSegment resPaxFareSeg;
			if (paxFareSegmentETicket != null) {
				Integer validPaxFareSegId = paxFareSegmentETicket.getPnrPaxFareSegId();
				resPaxFareSeg = reservarionPaxFareSegDAO.getReservationPaxFareSegment(validPaxFareSegId);

				if (prlPaxEntry.getPaxStatus().equals(ReservationInternalConstants.PaxFareSegmentTypes.FLOWN)) {
					reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PRLEntry.PAX_STATUS, FLOWN);
					resPaxFareSeg.setStatus(ReservationInternalConstants.PaxFareSegmentTypes.FLOWN);
				} else {
					// TODO change this to Not Boarded.
					reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PRLEntry.PAX_STATUS, NOT_BOARDED);
					resPaxFareSeg.setStatus(ReservationInternalConstants.PaxFareSegmentTypes.NO_SHORE);
				}
				prlPaxEntry.setProcessedStatus(ParserConstants.PRLProcessStatus.PROCESSED);
				paxFareSegmentsToSave.add(resPaxFareSeg);
				reservationAudit.setModificationType(AuditTemplateEnum.PAX_PRL_ENTRY.getCode());
				this.createPRLAudit(colReservationAudit, reservationAudit, prlPaxEntry, prl);
			} else {
				prlPaxEntry.setProcessedStatus(ParserConstants.PRLProcessStatus.ERROR_OCCURED);
				prlPaxEntry.setErrorDescription("Invalid Eticket Details.");
				isValidET = false;
			}
		}
		return isValidET;
	}

	/**
	 * Validate Passport Details such as passportNumber, passportExpDate and countryOfIssue
	 *
	 * @param prlPaxEntry
	 */
	private boolean validatePassportDetails(PRLPaxEntry prlPaxEntry) {
		boolean isValidPassport;

		if (isValidPassportNumber(prlPaxEntry.getPassportNumber())) {
			if (!CalendarUtil.isLessThan(prlPaxEntry.getPassportExpiryDate(), prlPaxEntry.getFlightDate())) {
				isValidPassport = true;
			} else {
				prlPaxEntry.setErrorDescription("Expired Passport");
				isValidPassport = false;
				log.debug(prlPaxEntry.getPnr() + " has an expired Passport");
			}
		} else {
			prlPaxEntry.setErrorDescription("Invalid Passport Number");
			isValidPassport = false;
			log.debug("Passport Number of " + prlPaxEntry.getPnr() + " is invalid");
		}

		return isValidPassport;

	}

	/**
	 * Validate Passport Number
	 *
	 * @param passportNumber
	 * @return
	 */
	private boolean isValidPassportNumber(String passportNumber) {
		boolean isValidPassportNo = false;
		if (!StringUtil.isNullOrEmpty(passportNumber) && StringUtils.isAlphanumeric(passportNumber)
				&& passportNumber.length() <= 15) {
			isValidPassportNo = true;
		}
		return isValidPassportNo;
	}

	/**
	 * validate PNR
	 *
	 * @param prlPaxEntry
	 * @throws ModuleException
	 */
	private boolean validatePNR(PRLPaxEntry prlPaxEntry) throws ModuleException {
		boolean isValidPNR;
		Reservation reservation;
		if (!StringUtil.isNullOrEmpty(prlPaxEntry.getPnr())) {
			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setPnr(prlPaxEntry.getPnr());
			pnrModesDTO.setLoadPaxAvaBalance(true);
			reservation = reservationBD.getReservation(pnrModesDTO, null);
			Set<ReservationPax> passengers = reservation.getPassengers();

			// Returns the PNR List
			Collection<String> getPNRList = reservationDAO
					.getPnrList(prlPaxEntry.getFlightNumber(), prlPaxEntry.getFlightDate());

			if (getPNRList.size() > 0 && getPNRList.contains(prlPaxEntry.getPnr())) {
				if (validatePassengerDetails(prlPaxEntry, passengers)) {
					isValidPNR = true;
				} else {
					prlPaxEntry.setErrorDescription("Incorrect PNR Details");
					isValidPNR = false;
					log.debug("Having incorrect PNR Details for " + prlPaxEntry.getPnr());
				}
			} else {
				prlPaxEntry.setErrorDescription("PNR does not exist in the flight");
				isValidPNR = false;
				log.debug("PNR number " + prlPaxEntry.getPnr() + " does not exist in the flight");
			}
		} else {
			prlPaxEntry.setErrorDescription("PNR is not received");
			isValidPNR = false;
		}

		return isValidPNR;
	}

	/**
	 * Check whether passenger details are match with the given pnr or not
	 * @param prlPaxEntry
	 * @param passengers
	 * @return
	 */
	private static boolean validatePassengerDetails(PRLPaxEntry prlPaxEntry, Set<ReservationPax> passengers) {
		boolean resPaxFound = false;
		ReservationPax reservationPax;
		for (Iterator<ReservationPax> iterPaxs = passengers.iterator(); iterPaxs.hasNext(); ) {
			reservationPax = iterPaxs.next();

			if (isPaxExistInResPassengers(prlPaxEntry, reservationPax)) {
				resPaxFound = true;
				break;
			}
		}
		return resPaxFound;
	}

	/**
	 * Validate the passenger with the Reservations Detail
	 *
	 * @param prlPaxEntry
	 * @param reservationPax
	 * @return
	 */
	private static boolean isPaxExistInResPassengers(PRLPaxEntry prlPaxEntry, ReservationPax reservationPax) {
		boolean isValidPax = false;
		String strTitle = Util.makeStringCompliant(prlPaxEntry.getTitle().toUpperCase());
		String strFirstName = Util.makeStringCompliant(prlPaxEntry.getFirstName());
		String strLastName = Util.makeStringCompliant(prlPaxEntry.getLastName());
		String strPaxType = Util.makeStringCompliant(prlPaxEntry.getPaxType());
		String paxTitle = Util.makeStringCompliant(reservationPax.getTitle());
		String paxFName = Util.makeStringCompliant(reservationPax.getFirstName());
		String paxLName = Util.makeStringCompliant(reservationPax.getLastName());
		String paxType = Util.makeStringCompliant(reservationPax.getPaxType());

		if (strTitle.equals(paxTitle) && strFirstName.equals(paxFName) && strLastName.equals(paxLName) && strPaxType
				.equals(paxType)) {
			isValidPax = true;
		} else if (PAX_CHILD_TYPE.equals(paxType) && strTitle.equals("") && PAX_CHILD.equalsIgnoreCase(paxTitle)) {
			if (strFirstName.equals(paxFName) && strLastName.equals(paxLName)) {
				isValidPax = true;
			}
		}
		return isValidPax;
	}


	/**
	 * Constructing and returning the command response
	 *
	 * @param colErrorPRLPaxEntry
	 * @return
	 */
	private DefaultServiceResponse composeResponse(Collection<PRLPaxEntry> colErrorPRLPaxEntry) {
		DefaultServiceResponse response;
		if (colErrorPRLPaxEntry.size() == 0) {

			response = new DefaultServiceResponse(true);
		} else {
			response = new DefaultServiceResponse(false);
		}
		return response;
	}

	private void createPRLAudit(Collection colReservationAudit, ReservationAudit reservationAudit, PRLPaxEntry prlPaxEntry,
			PRL prl) {

		/** Holds the date,time format */
		SimpleDateFormat sdfmt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		String bookingClass = "";
		String segment = "";

		reservationAudit.setPnr(prlPaxEntry.getPnr());
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PRLEntry.PAX_NAME, prlPaxEntry.getFirstName() + " "
				+ prlPaxEntry.getLastName());
		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PRLEntry.E_TICKET_NO, prlPaxEntry.getEticketNumber());
		if (ParserConstants.PRLProcessStatus.NOT_PROCESSED.equals(prlPaxEntry.getProcessedStatus())
				|| ParserConstants.PRLProcessStatus.ERROR_OCCURED.equals(prlPaxEntry.getProcessedStatus())) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PRLEntry.PROCESS_ERROR,
					prlPaxEntry.getErrorDescription());
		} else {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PRLEntry.FLIGHT_NO, prlPaxEntry.getFlightNumber());
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PRLEntry.SEGMENT, segment);

			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PRLEntry.BOOKING_CLASS, bookingClass);
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PRLEntry.TRAVEL_DATE,
					sdfmt.format(prl.getDepartureDate()));

			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PRLEntry.BOOKED_DATE,
					sdfmt.format(prlPaxEntry.getFlightDate()));

		}

		colReservationAudit.add(reservationAudit);
	}

	private void postPRLAuditRecord(Collection colReservationAudit, CredentialsDTO credentialsDTO) throws ModuleException {
		// Get the auditor deleagate
		AuditorBD auditorBD = ReservationModuleUtils.getAuditorBD();
		Iterator itReservationAuditCol = colReservationAudit.iterator();
		while (itReservationAuditCol.hasNext()) {
			ReservationAudit reservationAudit = (ReservationAudit) itReservationAuditCol.next();

			reservationAudit.setCustomerId(credentialsDTO.getCustomerId());
			reservationAudit.setUserId(credentialsDTO.getUserId());
			reservationAudit.setZuluModificationDate(new Date());
			reservationAudit.setSalesChannelCode(credentialsDTO.getSalesChannelCode());

			if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getIpAddress() != null) {
				reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PRLEntry.IP_ADDDRESS, credentialsDTO
						.getTrackInfoDTO().getIpAddress());
			}

			if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getCarrierCode() != null) {
				reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.PRLEntry.ORIGIN_CARRIER, credentialsDTO
						.getTrackInfoDTO().getCarrierCode());
			}

			reservationAudit.getContentMap().put(
					AuditTemplateEnum.TemplateParams.PRLEntry.REMOTE_USER,
					credentialsDTO.getDefaultCarrierCode() + "/" + "[" + credentialsDTO.getAgentCode() + "]" + "/" + "["
							+ credentialsDTO.getUserId() + "]");

			reservationAudit.setDisplayName(ReservationApiUtils.getDisplayName(credentialsDTO));
			auditorBD.audit(reservationAudit, reservationAudit.getContentMap());
		}
	}

	/**
	 * Validate Parameters
	 * 
	 * @param prlId
	 * @throws ModuleException
	 */
	private void validateParams(Integer prlId) throws ModuleException {
		log.debug("Inside validateParams");

		if (prlId == null) {
			throw new ModuleException("airreservations.arg.invalid.null");
		}

		log.debug("Exit validateParams");
	}
}
