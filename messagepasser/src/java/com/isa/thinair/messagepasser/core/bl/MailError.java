package com.isa.thinair.messagepasser.core.bl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.messagepasser.api.dto.etl.LogDTO;
import com.isa.thinair.messagepasser.api.model.ETL;
import com.isa.thinair.messagepasser.api.utils.MessagePasserModuleUtils;
import com.isa.thinair.messagepasser.api.utils.ParserConstants;
import com.isa.thinair.messagepasser.core.persistence.dao.ETLDAO;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;

public abstract class MailError {

	private static Log log = LogFactory.getLog(MailError.class);

	/**
	 * Notify Error
	 * 
	 * @param id
	 * @param e
	 * @param fileName
	 * @throws ModuleException
	 */
	public static void notifyError(Integer id, Throwable e, String fileName) throws ModuleException {

		ETLDAO etlDAO = MessagePasserDAOUtils.DAOInstance.ETL_DAO;

		try {

			LogDTO logDTO = new LogDTO();

			// get the content and format it.
			String content = "";

			ETL etl = etlDAO.getETL(id.intValue());
			if (etl != null) {
				content = etl.getEtlContent();

				if (content != null) {
					content.replaceAll("\n", "<br>");
				}
			}
			logDTO.setContent(content);

			// if exception
			if (e != null) {

				// if module exception show custom msg, else show exceptions
				// local msg
				if (e instanceof ModuleException || e instanceof CommonsDataAccessException) {
					ModuleException me = (ModuleException) e;
					logDTO.setExceptionDescription(me.getMessageString());
				} else {

					logDTO.setExceptionDescription(e.getLocalizedMessage());
				}

				logDTO.setStackTraceElements(e.getStackTrace());
			}

			// get the error pax
			Collection<String> statuss = new ArrayList<String>();
			statuss.add(ParserConstants.ETLProcessStatus.ERROR_OCCURED);
			statuss.add(ParserConstants.ETLProcessStatus.NOT_PROCESSED);
			Collection<String> errorPax = etlDAO.getPaxNamesForMail(id, statuss);

			if (errorPax != null && errorPax.size() > 0) {
				logDTO.setPaxWithErrors(errorPax);
			}
			statuss.clear();

			// set success pax
			statuss.add(ParserConstants.ETLProcessStatus.PROCESSED);
			Collection<String> reservationPax = etlDAO.getPaxNamesForMail(id, statuss);
			if (reservationPax != null && reservationPax.size() > 0) {
				logDTO.setPaxWithReservations(reservationPax);
			}

			logDTO.setFileName(fileName);

			sendMailtoRelaventAuthorities(logDTO);

		} catch (Exception x) {
			log.error("################ ERROR IN NOTIFY ERROR METHOD PARSING PNL ID)", x);
			throw new ModuleException(x, "airreservations.xaPnl.sendMail");

		}

	}

	/**
	 * Notify Error
	 * 
	 * @param logDTO
	 * @throws ModuleException
	 */
	public static void notifyError(LogDTO logDTO) throws ModuleException {
		try {
			sendMailtoRelaventAuthorities(logDTO);
		} catch (Exception e) {
			log.error("############### ERROR IN NOTIFY ERROR METHOD ", e);
			throw new ModuleException(e, "airreservations.xaPnl.sendMail");

		}

	}

	/**
	 * Will take a list of of email addresses from a config file and will send email to them without exception details
	 * 
	 * @param logDTO
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static void sendMailtoRelaventAuthorities(LogDTO logDTO) {

		Properties props = MessagePasserModuleUtils.getMessagePasserConfig().getEmailEtlErrorTo();
		Enumeration enumeration = props.elements();
		List messageList = new ArrayList();

		while (enumeration.hasMoreElements()) {

			String emailAddress = (String) enumeration.nextElement();
			logDTO.setEmailTo(emailAddress);
			UserMessaging user = new UserMessaging();
			user.setFirstName(emailAddress);
			user.setToAddres(emailAddress);
			messageList.add(user);
		}
		sendMail(messageList, logDTO);
	}

	/**
	 * Is called by the above two methods
	 * 
	 * @param messageList
	 * @param logDTO
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static void sendMail(List messageList, LogDTO logDTO) {

		// MessageProfileList to be emailed
		List list = new ArrayList();

		Iterator iterator = messageList.iterator();
		while (iterator.hasNext()) {

			MessageProfile profile = new MessageProfile();
			List msgList = new ArrayList();
			msgList.add(iterator.next());
			profile.setUserMessagings(msgList);
			Topic topic = new Topic();
			HashMap map = new HashMap();
			map.put("logDTO", logDTO);

			topic.setTopicParams(map);
			topic.setTopicName("etl_error");
			profile.setTopic(topic);
			list.add(profile);
		}

		MessagePasserModuleUtils.getMessagingServiceBD().sendMessages(list);
	}

}
