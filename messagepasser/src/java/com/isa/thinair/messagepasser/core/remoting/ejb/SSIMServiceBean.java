package com.isa.thinair.messagepasser.core.remoting.ejb;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airschedules.api.utils.GDSSchedConstants;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Command;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.messagepasser.api.dto.common.MessageConfigDTO;
import com.isa.thinair.messagepasser.api.dto.common.MessageConfigDTO.MAIL_CLIENT;
import com.isa.thinair.messagepasser.api.dto.ssim.SSIMRecordDTO;
import com.isa.thinair.messagepasser.api.utils.MessagePasserModuleUtils;
import com.isa.thinair.messagepasser.api.utils.ParserConstants.MessageTypes;
import com.isa.thinair.messagepasser.core.config.MessagePasserConfig;
import com.isa.thinair.messagepasser.core.service.bd.SSIMBDImpl;
import com.isa.thinair.messagepasser.core.service.bd.SSIMBDLocalImpl;
import com.isa.thinair.messagepasser.core.util.ParserUtil;
import com.isa.thinair.messagepasser.core.utils.CommandNames;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Session bean to provide SSIM Processing Service related functionalities
 * 
 * @author Rikaz
 */
@Stateless
@RemoteBinding(jndiBinding = "SSIMService.remote")
@LocalBinding(jndiBinding = "SSIMService.local")
@RolesAllowed("user")
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class SSIMServiceBean extends PlatformBaseSessionBean implements SSIMBDImpl, SSIMBDLocalImpl {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(SSIMServiceBean.class);

	/**
	 * Process SSIM message
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public ServiceResponce processScheduleMessage() throws ModuleException {

		// One Thread at a Time
		synchronized (this) {

			log.info("######### SSIM PARSING : INITIALIZING OPERATION  ##########");
			MessagePasserConfig messagePasserConfig = MessagePasserModuleUtils.getMessagePasserConfig();

			String ssimProcessPath = messagePasserConfig.getSsimProcessPath();
			String ssimParsedPath = messagePasserConfig.getSsimParsedPath();
			String ssimErrorPath = messagePasserConfig.getSsimErrorPath();
			String partialDownloadPath = messagePasserConfig.getSsimPartialDownloadPath();
			String messageType = MessageTypes.SSIM.toString();

			MessageConfigDTO msgCofigDTO = new MessageConfigDTO();
			msgCofigDTO.setPopMailServer(messagePasserConfig.getPopMailServer());
			msgCofigDTO.setMailServerUsername(messagePasserConfig.getSsimMailServerUsername());
			msgCofigDTO.setMailServerPassword(messagePasserConfig.getSsimMailServerPassword());

			msgCofigDTO.setTtyMailServer(messagePasserConfig.getTtyMailServer());
			msgCofigDTO.setTtyMailServerUserName(messagePasserConfig.getTtyMailServerUserName());
			msgCofigDTO.setTtyMailServerPassword(messagePasserConfig.getTtyMailServerPassword());

			msgCofigDTO.setFetchFromMailServer(messagePasserConfig.getEtlFetchFromMailServer().equalsIgnoreCase("true") ? true
					: false);
			msgCofigDTO.setDeleteFromMailServer(messagePasserConfig.getEtlDeleteFromMailServer().equalsIgnoreCase("true") ? true
					: false);
			msgCofigDTO.setProcessPath(ssimProcessPath);
			msgCofigDTO.setPreferedmailClient(MAIL_CLIENT.JAVAX_MAIL);			
			msgCofigDTO.setPopMailServerPort("110");
			
			try {
				// Downloading the SSIM Messages
				ParserUtil.downloadMessagesFromMailServer(msgCofigDTO, messageType, null);
			} catch (Exception ex) {
				log.error("########## ERROR OCCURED WHILE DOWNLOADING SSIM MESSAGES FROM E-MAIL ##########");
			}
			
			try {			
				
				Collection<SSIMRecordDTO> parsedSSIMDTOColl = ParserUtil.validateAndParseSSIMDocuments(ssimProcessPath,
						ssimErrorPath, partialDownloadPath, ssimParsedPath, messageType);
				log.info("######### READING DOWNLOADED SSIM MESSAGES  ##########");
				Iterator<SSIMRecordDTO> parsedSSIMDTOItr = parsedSSIMDTOColl.iterator();
				while (parsedSSIMDTOItr.hasNext()) {
					SSIMRecordDTO SSIMRecordDTO = parsedSSIMDTOItr.next();

					if (SSIMRecordDTO != null) {

						try {
							MessagePasserModuleUtils.getSSIMBD().createSchedulesForReceivedSSIM(SSIMRecordDTO);

						} catch (ModuleException e) {
							log.error(" ERROR OCCURED WHILE CREATING SCHEDULE SSIM-ID : " + SSIMRecordDTO.getSsimId() + " DATE :"
									+ new Date(), e);

						} catch (Exception e) {
							log.error(" ERROR OCCURED WHILE CREATING SCHEDULE SSIM-ID : " + SSIMRecordDTO.getSsimId() + " DATE :"
									+ new Date(), e);

						}

					}

				}

			} catch (ModuleException ex) {
				log.error(" ((o)) ModuleException::processScheduleMessage ", ex);
				throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
			} catch (CommonsDataAccessException cdaex) {
				log.error(" ((o)) CommonsDataAccessException::processScheduleMessage ", cdaex);
				throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
			}
			log.info("########################## SSIM PARSING : OPERATION COMPLETED ##########");
			return new DefaultServiceResponse(true);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public ServiceResponce createSchedulesForReceivedSSIM(SSIMRecordDTO ssimRecordDTO) throws ModuleException {

		try {
			Command command = (Command) MessagePasserModuleUtils.getBean(CommandNames.SCHEDULE_CREATION);
			command.setParameter(GDSSchedConstants.ParamNames.SSIM_RECORD_DTO, ssimRecordDTO);

			return command.execute();
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::createSchedules ", ex);
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::createSchedules ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

	}
}
