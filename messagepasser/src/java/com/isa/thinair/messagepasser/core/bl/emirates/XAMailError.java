package com.isa.thinair.messagepasser.core.bl.emirates;

/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.messagepasser.api.dto.emirates.XAPnlLogDTO;
import com.isa.thinair.messagepasser.api.utils.MessagePasserModuleUtils;
import com.isa.thinair.messagepasser.core.bl.MessagePasserDAOUtils;
import com.isa.thinair.messagepasser.core.persistence.dao.XAPnlDAO;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;

/**
 * Class to implement the bussiness logic related to the Mailing of Failed XA Pnls
 * 
 * @author Byorn
 */
public abstract class XAMailError {

	private static Log log = LogFactory.getLog(XAMailError.class);

	/**
	 * GETTING DATA FROM DATABASE AND SENDING MAIL TO RELAVENT PARTISE
	 * 
	 * @param pnlId
	 *            for data pax
	 * @param e
	 *            for admin
	 * @param fileName
	 *            for admin
	 * @throws ModuleException
	 */
	public static void notifyError(Integer pnlId, Throwable e, String fileName) throws ModuleException {

		XAPnlDAO pnlDAO = MessagePasserDAOUtils.DAOInstance.XA_PNL_DAO;

		try {

			XAPnlLogDTO logDTO = new XAPnlLogDTO();
			String pnlContent = pnlDAO.getXAPnlEntry(pnlId.intValue()).getXaPnlContent();
			if (pnlContent != null) {
				pnlContent.replaceAll("\n", "<br>");
			}
			logDTO.setXaPnlContent(pnlContent);

			if (e != null) {

				if (e instanceof ModuleException || e instanceof CommonsDataAccessException) {
					ModuleException me = (ModuleException) e;
					logDTO.setExceptionDescription(me.getMessageString());
				} else {

					logDTO.setExceptionDescription(e.getLocalizedMessage());
				}

				logDTO.setStackTraceElements(e.getStackTrace());
			}

			Collection errorPax = pnlDAO.getPaxNamesHavingError(pnlId);
			if (errorPax != null && errorPax.size() > 0) {
				logDTO.setPaxWithErrors(errorPax);
			}

			Collection reservationPax = pnlDAO.getPaxNamesWithReservations(pnlId);
			if (reservationPax != null && reservationPax.size() > 0) {
				logDTO.setPaxWithReservations(reservationPax);
			}

			logDTO.setFileName(fileName);

			sendMailtoRelaventAuthorities(logDTO);

		} catch (Exception x) {
			log.error("################ ERROR IN NOTIFY ERROR METHOD PARSING PNL ID)", e);
			throw new ModuleException(x, "airreservations.xaPnl.sendMail");

		}

	}

	/**
	 * Notify Error
	 * 
	 * @param logDTO
	 * @throws ModuleException
	 */
	public static void notifyError(XAPnlLogDTO logDTO) throws ModuleException {

		try {
			sendMailtoRelaventAuthorities(logDTO);
		} catch (Exception e) {
			log.error("############### ERROR IN NOTIFY ERROR METHOD ", e);
			throw new ModuleException(e, "airreservations.xaPnl.sendMail");

		}

	}

	/**
	 * Will take a list of of email addresses from a config file and will send email to them without exception details
	 * 
	 * @param logDTO
	 * @throws ModuleException
	 */
	private static void sendMailtoRelaventAuthorities(XAPnlLogDTO logDTO) {

		Properties props = MessagePasserModuleUtils.getMessagePasserConfig().getEmailXAPnlErrorTo();
		Enumeration enumeration = props.elements();
		List messageList = new ArrayList();

		while (enumeration.hasMoreElements()) {

			String emailAddress = (String) enumeration.nextElement();
			logDTO.setEmailTo(emailAddress);
			UserMessaging user = new UserMessaging();
			user.setFirstName(emailAddress);
			// user.setLastName(alertDTO.get
			user.setToAddres(emailAddress);
			messageList.add(user);
		}

		sendMail(messageList, logDTO);
	}

	/**
	 * Is called by the above two methods
	 * 
	 * @param messageList
	 * @param logDTO
	 */
	private static void sendMail(List messageList, XAPnlLogDTO logDTO) {

		List list = new ArrayList();

		Iterator iterator = messageList.iterator();
		while (iterator.hasNext()) {

			MessageProfile profile = new MessageProfile();
			List msgList = new ArrayList();
			msgList.add(iterator.next());
			profile.setUserMessagings(msgList);
			Topic topic = new Topic();
			HashMap map = new HashMap();
			map.put("xaPnlLogDTO", logDTO);

			topic.setTopicParams(map);
			topic.setTopicName("xapnl_error");
			profile.setTopic(topic);
			list.add(profile);

		}
		MessagePasserModuleUtils.getMessagingServiceBD().sendMessages(list);
	}

}
