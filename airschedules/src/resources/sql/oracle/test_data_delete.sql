delete from T_FLIGHT_LEG where FLIGHT_LEG_ID > 9999;
delete from T_FLIGHT_SEGMENT where FLT_SEG_ID > 9999;
delete from T_FLIGHT where FLIGHT_ID > 9999;

delete from T_FLIGHT_SCHEDULE_LEG where FSL_ID > 9999;
delete from T_FLIGHT_SCHEDULE_SEGMENT where FL_SCH_SEG_ID > 9999;
delete from T_FLIGHT_SCHEDULE where SCHEDULE_ID > 9999;  

delete from T_VALID_SEGMENT where SEGMENT_CODE='CMB/DOH';
delete from T_VALID_SEGMENT where SEGMENT_CODE='CMB/DEL';
delete from T_VALID_SEGMENT where SEGMENT_CODE='DEL/KHI';
delete from T_VALID_SEGMENT where SEGMENT_CODE='CMB/DEL/KHI';
delete from T_VALID_SEGMENT where SEGMENT_CODE='CMB/SIN';
delete from T_VALID_SEGMENT where SEGMENT_CODE='HYD/KWI';
delete from T_VALID_SEGMENT where SEGMENT_CODE='HYD/SHJ';
delete from T_VALID_SEGMENT where SEGMENT_CODE='SHJ/KWI';
delete from T_VALID_SEGMENT where SEGMENT_CODE='HYD/SHJ/KWI';

delete from  T_ROUTE_INFO where ROUTE_ID='CMB/DOH';
delete from  T_ROUTE_INFO where ROUTE_ID='CMB/DEL';
delete from  T_ROUTE_INFO where ROUTE_ID='DEL/KHI';
delete from  T_ROUTE_INFO where ROUTE_ID='CMB/SIN';
delete from  T_ROUTE_INFO where ROUTE_ID='HYD/KWI';
delete from  T_ROUTE_INFO where ROUTE_ID='HYD/SHJ';
delete from  T_ROUTE_INFO where ROUTE_ID='SHJ/KWI';

delete from T_APP_PARAMETER where PARAM_KEY='OFFST';
delete from T_APP_PARAMETER where PARAM_KEY='HUBFS';
delete from T_APP_PARAMETER where PARAM_KEY='MNTRT';
delete from T_APP_PARAMETER where PARAM_KEY='MXTRT';
delete from T_APP_PARAMETER where PARAM_KEY='DFOPT';
delete from T_APP_PARAMETER where PARAM_KEY='DFSST';
delete from T_APP_PARAMETER where PARAM_KEY='DTFMT';


delete from T_USER where USER_ID='famizm';
delete from T_USER where USER_ID='jhonw';