/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules;

import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.isa.thinair.airschedules.api.dto.AffectedPeriod;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightLeg;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.FlightScheduleLeg;
import com.isa.thinair.airschedules.api.model.FlightScheduleSegment;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.airschedules.api.utils.ScheduleBuildStatusEnum;
import com.isa.thinair.airschedules.api.utils.ScheduleStatusEnum;
import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.api.dto.ModuleCriterion;

/**
 * @author Lasantha Pambagoda
 */
public class UtilityForTesting {
	
	/**
	 * method to populate flight schedule
	 * @return
	 */
	public static FlightSchedule cerateFlightSchedule() {

		Frequency fqn = new Frequency();
		fqn.setDay0(true);
		fqn.setDay1(false);
		fqn.setDay2(false);
		fqn.setDay3(true);
		fqn.setDay4(true);
		fqn.setDay5(false);
		fqn.setDay6(true);
		
		FlightSchedule flightSchedule = new FlightSchedule();
		//settign fields
		flightSchedule.setArrivalStnCode("DOH");
		flightSchedule.setAvailableSeatKilometers(new Integer(10));
		flightSchedule.setBuildStatusCode(ScheduleBuildStatusEnum.PENDING.getCode());
		flightSchedule.setCreatedBy("jhonw");
		flightSchedule.setCreatedDate(new GregorianCalendar(2005,8,1).getTime());
		flightSchedule.setDepartureStnCode("CMB");
		flightSchedule.setFlightNumber("GG1111");
		flightSchedule.setFrequency(fqn);
		flightSchedule.setModelNumber("MDL_NBR_UNIT_TSTING");
		flightSchedule.setModifiedBy("jhonw");
		flightSchedule.setModifiedDate(new GregorianCalendar(2005,8,2).getTime());
		flightSchedule.setNumberOfDepartures(new Integer(1));
		flightSchedule.setOperationTypeId(2);
		flightSchedule.setStartDate(new GregorianCalendar(2006,0,10).getTime());
		flightSchedule.setStatusCode(ScheduleStatusEnum.ACTIVE.getCode());
		flightSchedule.setStopDate(new GregorianCalendar(2006,0,25).getTime());
		
		return flightSchedule;
	}
	
	
	public static FlightScheduleLeg createFlightScheduleLeg(){
		FlightScheduleLeg fliScheLeg = new FlightScheduleLeg();
		
		// setting feilds
		fliScheLeg.setDestination("DOH");
		fliScheLeg.setDuration(10);
		fliScheLeg.setLegNumber(1);
		fliScheLeg.setOrigin("CMB");
		fliScheLeg.setEstArrivalTimeLocal(new GregorianCalendar(0,0,0,10,30,0).getTime());
		fliScheLeg.setEstArrivalTimeZulu(new GregorianCalendar(0,0,0,12,30,0).getTime());
		fliScheLeg.setEstDepartureTimeLocal(new GregorianCalendar(0,0,0,6,30,0).getTime());
		fliScheLeg.setEstDepartureTimeZulu(new GregorianCalendar(0,0,0,8,30,0).getTime());
		
		return fliScheLeg;
	}
	
	public static FlightScheduleSegment createFlightScheduleSegment(){
		
		FlightScheduleSegment fss = new FlightScheduleSegment();

		//setting  feilds
		//fss.setPriority(2);
		fss.setValidFlag(true);
		fss.setSegmentCode("CMB/DOH");
		
		return fss;
	}
	
	public static List createSerchCriteriaForSchedules(){
		
		ModuleCriterion criterea = new ModuleCriterion();
		criterea.setCondition(ModuleCriterion.CONDITION_EQUALS);
		criterea.setFieldName("scheduleId");
		List list = new ArrayList();
		list.add(new Integer(10000));
		criterea.setValue(list);
		
		List criList = new ArrayList();
		criList.add(criterea);
		
		return criList;
	}
	
	public static List createSearchCriteriaForFlights(){
		
		ModuleCriterion criterea = new ModuleCriterion();
		criterea.setCondition(ModuleCriterion.CONDITION_EQUALS);
		criterea.setFieldName("flightId");
		List list = new ArrayList();
		list.add(new Integer(10001));
		criterea.setValue(list);
		
		List criList = new ArrayList();
		criList.add(criterea);
		
		return criList;		
	}
	
	public static Flight createFlight(){
		
		Flight flight = new Flight();
		
		//setting fields
		flight.setAvailableSeatKilometers(new Integer(10));
		flight.setCreatedBy("jhonw");
		flight.setCreatedDate(new GregorianCalendar(2005,8,1).getTime());
		flight.setDayNumber(1);
		flight.setDepartureDate(new GregorianCalendar(2009,0,1).getTime());
		flight.setDestinationAptCode("DOH");
		flight.setFlightNumber("GG1111");
		flight.setModelNumber("MDL_NBR_UNIT_TSTING");
		flight.setModifiedBy("jhonw");
		flight.setModifiedDate(new GregorianCalendar(2005,8,2).getTime());
		flight.setOperationTypeId(2);
		flight.setOriginAptCode("CMB");
		flight.setScheduleId(new Integer(10000));
		flight.setStatus(FlightStatusEnum.CREATED.getCode());
		flight.setTailNumber("TN100");
		
		return flight;
	}
	
	public static FlightLeg createFlightLeg(){
		
		FlightLeg flightLeg = new FlightLeg();

		// setting fields
		flightLeg.setDestination("DOH");
		flightLeg.setDuration(10);
		flightLeg.setLegNumber(1);
		flightLeg.setOrigin("CMB");
		flightLeg.setEstArrivalTimeLocal(new GregorianCalendar(0,0,0,10,30,0).getTime());
		flightLeg.setEstArrivalTimeZulu(new GregorianCalendar(0,0,0,12,30,0).getTime());
		flightLeg.setEstDepartureTimeLocal(new GregorianCalendar(0,0,0,6,30,0).getTime());
		flightLeg.setEstDepartureTimeZulu(new GregorianCalendar(0,0,0,8,30,0).getTime());
			
		return flightLeg;
	}
	
	public static FlightSegement createFlightSegment(){
		
		FlightSegement fliSeg = new FlightSegement();

		//setting fields
		//fliSeg.setPriority(1);
		fliSeg.setSegmentCode("CMB/DOH");
		fliSeg.setValidFlag(true);
		
		return fliSeg;
	}
	
	public static FlightSchedule getConflictingFlightScheduleWithSchedules() {
		
		FlightSchedule flightSchedule = cerateFlightSchedule();
		flightSchedule.setStartDate(new GregorianCalendar(2006,0,10).getTime());
		flightSchedule.setStopDate(new GregorianCalendar(2006,0,25).getTime());
		
		return flightSchedule;
	}
	
	public static FlightSchedule getConflictingFlightScheduleWithFlights() {
		
		FlightSchedule flightSchedule = cerateFlightSchedule();
		
		Frequency fqn = new Frequency();
		fqn.setDay0(false);
		fqn.setDay1(false);
		fqn.setDay2(false);
		fqn.setDay3(true);
		fqn.setDay4(false);
		fqn.setDay5(false);
		fqn.setDay6(true);
		
		flightSchedule.setFrequency(fqn);
		flightSchedule.setStartDate(new GregorianCalendar(2006,0,10).getTime());
		flightSchedule.setStopDate(new GregorianCalendar(2006,8,25).getTime());
		
		return flightSchedule;
	}
	
	public static FlightSchedule getFlightScheduleToSaveByMacroCommand(){
		
		FlightSchedule flightSchedule = cerateFlightSchedule();
		flightSchedule.setStartDate(new GregorianCalendar(2007,0,10).getTime());
		flightSchedule.setStopDate(new GregorianCalendar(2007,2,25).getTime());
		
		HashSet legSet = new HashSet();
		legSet.add(createFlightScheduleLeg());
		flightSchedule.setFlightScheduleLegs(legSet);
		
		HashSet segmentSet = new HashSet();
		segmentSet.add(createFlightScheduleSegment());
		flightSchedule.setFlightScheduleSegments(segmentSet);
		
		return flightSchedule;
	}
	
	public static Flight getFlightToSaveByMacroCommand(){
		
		Flight flight = createFlight();

		HashSet legSet = new HashSet();
		legSet.add(createFlightLeg());
		flight.setFlightLegs(legSet);
		
		HashSet segmentSet = new HashSet();
		segmentSet.add(createFlightSegment());
		flight.setFlightSegements(segmentSet);
		
		return flight;
	}
	
	public static Collection getFlyDates(){
		
		ArrayList list = new ArrayList();
		
		list.add(new GregorianCalendar(2006,2,2).getTime());
		list.add(new GregorianCalendar(2006,2,4).getTime());
		
		return list;
	}

	public static FlightSchedule getOverLappingFlightSchedule(){
		
		Frequency fqn = new Frequency();
		fqn.setDay0(true);
		fqn.setDay1(false);
		fqn.setDay2(false);
		fqn.setDay3(true);
		fqn.setDay4(true);
		fqn.setDay5(false);
		fqn.setDay6(true);
		
		FlightSchedule flightSchedule = new FlightSchedule();
		//settign fields
		flightSchedule.setArrivalStnCode("CMB");
		flightSchedule.setAvailableSeatKilometers(new Integer(10));
		flightSchedule.setBuildStatusCode(ScheduleBuildStatusEnum.PENDING.getCode());
		flightSchedule.setCreatedBy("jhonw");
		flightSchedule.setCreatedDate(new GregorianCalendar(2005,8,1).getTime());
		flightSchedule.setDepartureStnCode("DEL");
		flightSchedule.setFlightNumber("HH1111");
		flightSchedule.setFrequency(fqn);
		flightSchedule.setModelNumber("MDL_NBR_UNIT_TSTING");
		flightSchedule.setModifiedBy("jhonw");
		flightSchedule.setModifiedDate(new GregorianCalendar(2005,8,2).getTime());
		flightSchedule.setNumberOfDepartures(new Integer(1));
		flightSchedule.setOperationTypeId(2);
		flightSchedule.setStartDate(new GregorianCalendar(2006,0,1).getTime());
		flightSchedule.setStatusCode(ScheduleStatusEnum.ACTIVE.getCode());
		flightSchedule.setStopDate(new GregorianCalendar(2006,0,20).getTime());
		
		FlightScheduleLeg fliScheLeg1 = new FlightScheduleLeg();
		
		// setting feilds
		fliScheLeg1.setDestination("KHI");
		fliScheLeg1.setDuration(10);
		fliScheLeg1.setLegNumber(1);
		fliScheLeg1.setOrigin("DEL");
		fliScheLeg1.setEstArrivalTimeZulu(new GregorianCalendar(0,0,0,12,30,0).getTime());
		fliScheLeg1.setEstDepartureTimeZulu(new GregorianCalendar(0,0,0,8,30,0).getTime());

		
		FlightScheduleLeg fliScheLeg2 = new FlightScheduleLeg();
		
		// setting feilds
		fliScheLeg2.setDestination("CMB");
		fliScheLeg2.setDuration(10);
		fliScheLeg2.setLegNumber(2);
		fliScheLeg2.setOrigin("KHI");
		fliScheLeg2.setEstArrivalTimeZulu(new GregorianCalendar(0,0,0,14,30,0).getTime());
		fliScheLeg2.setEstDepartureTimeZulu(new GregorianCalendar(0,0,0,12,30,0).getTime());
		
		Set legSet = new HashSet();
		legSet.add(fliScheLeg1);
		legSet.add(fliScheLeg2);
		flightSchedule.setFlightScheduleLegs(legSet);
		
		
		FlightScheduleSegment fss1 = new FlightScheduleSegment();

		//setting  feilds
		//fss1.setPriority(2);
		fss1.setValidFlag(true);
		fss1.setSegmentCode("DEL/KHI");
		
		FlightScheduleSegment fss2 = new FlightScheduleSegment();

		//setting  feilds
		//fss2.setPriority(2);
		fss2.setValidFlag(true);
		fss2.setSegmentCode("KHI/CMB");
		
		FlightScheduleSegment fss3 = new FlightScheduleSegment();

		//setting  feilds
		//fss3.setPriority(2);
		fss3.setValidFlag(true);
		fss3.setSegmentCode("DEL/KHI/CMB");
		
		Set segSet = new HashSet();
		segSet.add(fss1);
		segSet.add(fss2);
		segSet.add(fss3);
		flightSchedule.setFlightScheduleSegments(segSet);
		
		return flightSchedule;		
	}
	
	public static AffectedPeriod getAffectedPeriodForFlightUpdate(){
		
		Frequency fqn = new Frequency();
		fqn.setDay0(true);
		fqn.setDay1(false);
		fqn.setDay2(true);
		fqn.setDay3(true);
		fqn.setDay4(false);
		fqn.setDay5(true);
		fqn.setDay6(false);
		
		AffectedPeriod period = new AffectedPeriod();
		
		period.setFrequency(fqn);
		period.setStartDate(new GregorianCalendar(2006,2,1).getTime());
		period.setEndDate(new GregorianCalendar(2006,2,5).getTime());
		
		return period;
	}
	
	public static AffectedPeriod getSplitPeriod() {
				
		Frequency fqn = new Frequency();
		fqn.setDay0(false);
		fqn.setDay1(false);
		fqn.setDay2(false);
		fqn.setDay3(true);
		fqn.setDay4(false);
		fqn.setDay5(true);
		fqn.setDay6(false);
		
		AffectedPeriod period = new AffectedPeriod();
		period.setFrequency(fqn);
		period.setStartDate(new GregorianCalendar(2006,8,8).getTime());
		period.setEndDate(new GregorianCalendar(2006,8,20).getTime());
		
		return period;
	}
}
