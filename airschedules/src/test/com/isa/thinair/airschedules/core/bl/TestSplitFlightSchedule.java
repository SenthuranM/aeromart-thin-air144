/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl;

import java.util.GregorianCalendar;

import com.isa.thinair.airschedules.UtilityForTesting;
import com.isa.thinair.airschedules.api.dto.AffectedPeriod;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.core.persistence.dao.FlightScheduleDAO;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.airschedules.core.util.LookUpUtils;
import com.isa.thinair.airschedules.core.utils.CommandNames;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Command;

import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformTestCase;

/**
 * test case for test the split flight schedule command 
 * @author Lasantha Pambagoda
 */
public class TestSplitFlightSchedule extends PlatformTestCase {
	
	//splitFlightScheduleCommand to be test
	private Command splitFlightScheduleCommand;
	private FlightScheduleDAO flightScheduleDAO;
		
	/**
	 * set op method of the test case
	 */
	protected void setUp() throws Exception {
		
		super.setUp();
		splitFlightScheduleCommand = (Command) AirSchedulesUtil
				.getInstance().getLocalBean(CommandNames.SPLIT_SCHEDULE_MACRO);
		flightScheduleDAO = (FlightScheduleDAO) AirSchedulesUtil.getInstance()
				.getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_DAO);
	}
	
	/**
	 * teat down method of the test case
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	
	/**
	 * testing the execute method of the create flight schedule macro command
	 * for schedule befour build
	 * @throws ModuleException 
	 */
	public void testExecuteForBuild() throws ModuleException{
			
		FlightSchedule schedule = flightScheduleDAO.getFlightSchedule(10101); 
		AffectedPeriod period = UtilityForTesting.getSplitPeriod();
		period.setStartDate(new GregorianCalendar(2006,9,10).getTime());
		period.setEndDate(new GregorianCalendar(2006,9,18).getTime());
		
		splitFlightScheduleCommand.setParameter( CommandParamNames.FLIGHT_SCHEDULE , schedule);
		splitFlightScheduleCommand.setParameter( CommandParamNames.AFFECTED_PERIOD , period);
		ServiceResponce nbresponce =  (ServiceResponce)splitFlightScheduleCommand.execute();
		
		assertTrue("Cancel schedule (AFTER BUILD) is not successfull", nbresponce.isSuccess());
		assertNull("Respond code is not null", nbresponce.getResponseCode());
	}
	
	/**
	 * testing the execute method of the create flight schedule macro command
	 * for schedule after build
	 * @throws ModuleException 
	 */
	public void testExecuteForNotBuild() throws ModuleException{
			
			FlightSchedule schedule = flightScheduleDAO.getFlightSchedule(10100); 
			AffectedPeriod period = UtilityForTesting.getSplitPeriod();
			
			splitFlightScheduleCommand.setParameter( CommandParamNames.FLIGHT_SCHEDULE , schedule);
			splitFlightScheduleCommand.setParameter( CommandParamNames.AFFECTED_PERIOD , period);
			ServiceResponce nbresponce =  (ServiceResponce)splitFlightScheduleCommand.execute();
			
			assertTrue("Cancel schedule (AFTER BUILD) is not successfull", nbresponce.isSuccess());
			assertNull("Respond code is not null", nbresponce.getResponseCode());
	}
}
