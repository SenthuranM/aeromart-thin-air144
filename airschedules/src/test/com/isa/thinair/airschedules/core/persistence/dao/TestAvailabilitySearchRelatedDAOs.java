/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.persistence.dao;

import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;

import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.AvailabilitySearchTypeEnum;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.airschedules.core.util.LookUpUtils;
import com.isa.thinair.platform.api.util.PlatformTestCase;

/** 
 * test case to test availability related daos
 * @author Lasantha Pambagoda
 */
public class TestAvailabilitySearchRelatedDAOs extends PlatformTestCase {

	//flight dao to be test
	private FlightDAO flightDAO;

	/**
	 * set op method of the test case
	 */
	protected void setUp() throws Exception {
		super.setUp();
		flightDAO = (FlightDAO)AirSchedulesUtil
				.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_DAO);
	}

	/**
	 * teat down method of the test case
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	
	public void testGetMachingDirectFlights(){
		
		Date minDate = new GregorianCalendar(2005,0,7).getTime();
		Date maxDate = new GregorianCalendar(2010,0,7).getTime();
		String origin = "HYD";
		String destination = "KWI";
		
		/*Collection collection  
			= flightDAO.getMachingDirectFlightSegments(minDate, maxDate, origin, destination, 
					AvailabilitySearchTypeEnum.MULTIPLE_FLIGHTS_FOR_PERIOD, 2, 2);
	
		assertNotNull("returned list from the getMachingDirectFlights was null", collection);
		assertTrue(
				"database has maching records of direct flights, " +
				"but the returned list from the getMachingDirectFlights " +
				"method returned zero elements",
				(collection.size() > 0));
		assertEquals(collection.size(), 2);
		*/
	}
}
