/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl;

import java.util.GregorianCalendar;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airschedules.api.utils.AvailabilitySearchTypeEnum;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.LookUpUtils;
import com.isa.thinair.airschedules.core.utils.CommandNames;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Command;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformTestCase;

/**
 * @author Lasantha Pambagoda
 */
public class TestAvailabilitySearch extends PlatformTestCase {

	//ammendFlightScheduleCommand to be test
	private Command availabilitySearchCommand;
		
	/**
	 * set op method of the test case
	 */
	protected void setUp() throws Exception {
		
		super.setUp();
		/*availabilitySearchCommand
			=  (Command) LookUpUtils.getBean(CommandNames.FLIGHT_AVAILABILITY_SEARCH);
		*/
	}
	
	/**
	 * teat down method of the test case
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	
	/**
	 * testing the execute method of the ammend flight schedule macro command
	 * @throws ModuleException 
	 */
	public void testMultipleFlightForPeriod() throws ModuleException{
		
		String str = "CMB/DOH";
		String sbStr = str.substring(str.indexOf("/") + 1);
		
		
		AvailableFlightSearchDTO avifltSearchDTO  = new AvailableFlightSearchDTO();
		GregorianCalendar calander = new GregorianCalendar(2006,1,15);
		avifltSearchDTO.setDepatureDate(calander.getTime());
		avifltSearchDTO.setAdultCount(2);
		avifltSearchDTO.setInfantCount(2);
		avifltSearchDTO.setDepartureVariance(20);
		avifltSearchDTO.setFromAirport("CMB");
		avifltSearchDTO.setToAirport("DOH");
		
		AvailabilitySearchTypeEnum searchType = AvailabilitySearchTypeEnum.MULTIPLE_FLIGHTS_FOR_PERIOD;
		
		availabilitySearchCommand.setParameter( CommandParamNames.AVILABLE_FLIGHT_SEARCH_DTO , avifltSearchDTO);
		availabilitySearchCommand.setParameter( CommandParamNames.AVILABLE_FLIGHT_SEARCH_TYPE, searchType);
		ServiceResponce responce =  availabilitySearchCommand.execute();
		
		assertNotNull("returned ServiceResponce is null", responce);
		AvailableFlightDTO availableFlights 
			= (AvailableFlightDTO)responce.getResponseParam(ResponceCodes.AVAILABILITY_SEARCH_SUCCESSFULL);
		assertNotNull("returned AvailableFlightDTO is null", availableFlights);
	}
	
	
	/**
	 * testing the execute method of the ammend flight schedule macro command
	 * @throws ModuleException 
	 */
	public void testMultipleFlightForDay() throws ModuleException{
		
		AvailableFlightSearchDTO avifltSearchDTO  = new AvailableFlightSearchDTO();
		GregorianCalendar calander = new GregorianCalendar(2006,1,15);
		avifltSearchDTO.setDepatureDate(calander.getTime());
		avifltSearchDTO.setAdultCount(2);
		avifltSearchDTO.setInfantCount(2);
		avifltSearchDTO.setDepartureVariance(20);
		avifltSearchDTO.setFromAirport("CMB");
		avifltSearchDTO.setToAirport("DOH");
		
		AvailabilitySearchTypeEnum searchType = AvailabilitySearchTypeEnum.MULTIPLE_FLIGHTS_FOR_DAY;
		
		availabilitySearchCommand.setParameter( CommandParamNames.AVILABLE_FLIGHT_SEARCH_DTO , avifltSearchDTO);
		availabilitySearchCommand.setParameter( CommandParamNames.AVILABLE_FLIGHT_SEARCH_TYPE, searchType);
		ServiceResponce responce =  availabilitySearchCommand.execute();
		
		assertNotNull("returned ServiceResponce is null", responce);
		AvailableFlightDTO availableFlights 
			= (AvailableFlightDTO)responce.getResponseParam(ResponceCodes.AVAILABILITY_SEARCH_SUCCESSFULL);
		assertNotNull("returned AvailableFlightDTO is null", availableFlights);
	}
	
	/**
	 * testing the execute method of the ammend flight schedule macro command
	 * @throws ModuleException 
	 */
	public void testSingleFlightForDay() throws ModuleException{
		
		AvailableFlightSearchDTO avifltSearchDTO  = new AvailableFlightSearchDTO();
		GregorianCalendar calander = new GregorianCalendar(2006,1,15);
		avifltSearchDTO.setDepatureDate(calander.getTime());
		avifltSearchDTO.setAdultCount(2);
		avifltSearchDTO.setInfantCount(2);
		avifltSearchDTO.setDepartureVariance(20);
		avifltSearchDTO.setFromAirport("CMB");
		avifltSearchDTO.setToAirport("DOH");
		
		AvailabilitySearchTypeEnum searchType = AvailabilitySearchTypeEnum.SINGLE_FLIGHTS_FOR_PERIOD;
		
		availabilitySearchCommand.setParameter( CommandParamNames.AVILABLE_FLIGHT_SEARCH_DTO , avifltSearchDTO);
		availabilitySearchCommand.setParameter( CommandParamNames.AVILABLE_FLIGHT_SEARCH_TYPE, searchType);
		ServiceResponce responce =  availabilitySearchCommand.execute();
		
		assertNotNull("returned ServiceResponce is null", responce);
		AvailableFlightDTO availableFlights 
			= (AvailableFlightDTO)responce.getResponseParam(ResponceCodes.AVAILABILITY_SEARCH_SUCCESSFULL);
		assertNotNull("returned AvailableFlightDTO is null", availableFlights);
	}
	
	/**
	 * testing the execute method of the ammend flight schedule macro command
	 * @throws ModuleException 
	 */
	public void testFilterdMultipleFlightForPeriod() throws ModuleException{
		
		AvailableFlightSearchDTO avifltSearchDTO  = new AvailableFlightSearchDTO();
		GregorianCalendar calander = new GregorianCalendar(2006,1,15);
		avifltSearchDTO.setDepatureDate(calander.getTime());
		avifltSearchDTO.setAdultCount(2);
		avifltSearchDTO.setInfantCount(2);
		avifltSearchDTO.setDepartureVariance(20);
		avifltSearchDTO.setFromAirport("CMB");
		avifltSearchDTO.setToAirport("DOH");
		
		AvailabilitySearchTypeEnum searchType = AvailabilitySearchTypeEnum.FILTERED_MULTIPLE_FLIGHTS_FOR_PERIOD;
		
		availabilitySearchCommand.setParameter( CommandParamNames.AVILABLE_FLIGHT_SEARCH_DTO , avifltSearchDTO);
		availabilitySearchCommand.setParameter( CommandParamNames.AVILABLE_FLIGHT_SEARCH_TYPE, searchType);
		ServiceResponce responce =  availabilitySearchCommand.execute();
		
		assertNotNull("returned ServiceResponce is null", responce);
		AvailableFlightDTO availableFlights 
			= (AvailableFlightDTO)responce.getResponseParam(ResponceCodes.AVAILABILITY_SEARCH_SUCCESSFULL);
		assertNotNull("returned AvailableFlightDTO is null", availableFlights);
	}
}
