/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl;

import java.util.GregorianCalendar;

import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.core.persistence.dao.FlightScheduleDAO;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.airschedules.core.util.LookUpUtils;
import com.isa.thinair.airschedules.core.utils.CommandNames;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Command;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformTestCase;

/**
 * testing ammend flight schedule command
 * @author Lasantha Pambagoda
 */
public class TestAmmendFlightSchedule extends PlatformTestCase {

	//ammendFlightScheduleCommand to be test
	private Command ammendFlightScheduleCommand;
	private FlightScheduleDAO flightScheduleDAO;
		
	/**
	 * set op method of the test case
	 */
	protected void setUp() throws Exception {
		
		super.setUp();
		/*ammendFlightScheduleCommand
			=  (Command) LookUpUtils.getBean(CommandNames.AMMEND_SCHEDULE_MACRO);
		flightScheduleDAO 
			=  (FlightScheduleDAO)LookUpUtils.getBean(
			   InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_DAO);
		*/
	}
	
	/**
	 * teat down method of the test case
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	
	/**
	 * testing the execute method of the ammend flight schedule macro command
	 * @throws ModuleException 
	 */
	public void testExecuteForBuildCompeted() throws ModuleException{
		
		FlightSchedule schedule = flightScheduleDAO.getFlightSchedule(20087);
		schedule.setStartDate(new GregorianCalendar(2006,12,15).getTime());
		schedule.setStopDate(new GregorianCalendar(2006,12,20).getTime());
		schedule.setOperationTypeId(1);
		
		Boolean force = new Boolean(false);
		
		ammendFlightScheduleCommand.setParameter( CommandParamNames.FLIGHT_SCHEDULE , schedule);
		ammendFlightScheduleCommand.setParameter( CommandParamNames.FORCE , force);
		ServiceResponce responce =  (ServiceResponce)ammendFlightScheduleCommand.execute();
		
		assertTrue("build schedule is not success full", responce.isSuccess());
		assertNull("Respond code is not null", responce.getResponseCode());
	}
}
