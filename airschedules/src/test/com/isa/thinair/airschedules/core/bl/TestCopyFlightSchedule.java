/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl;

import java.util.Date;
import java.util.GregorianCalendar;

import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.LookUpUtils;
import com.isa.thinair.airschedules.core.utils.CommandNames;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Command;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformTestCase;

/**
 * test case to test copy flight schedule
 * @author Lasantha Pambagoda
 */
public class TestCopyFlightSchedule extends PlatformTestCase {
	
	//copyFlightScheduleCommand to be test
	private Command copyFlightScheduleCommand;
		
	/**
	 * set op method of the test case
	 */
	protected void setUp() throws Exception {
		
		super.setUp();
		copyFlightScheduleCommand = (Command) AirSchedulesUtil
				.getInstance().getLocalBean(CommandNames.COPY_SCHEDULE_MACRO);
	}
	
	/**
	 * teat down method of the test case
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	
	/**
	 * testing the execute method of the copy flight schedule macro command
	 * for schedule befour build
	 * @throws ModuleException 
	 */
	public void testExecute() throws ModuleException{
			
			Integer bsId = new Integer(10000);
			Date startDate = new GregorianCalendar(2008,0,1).getTime();
			Date endDate = new GregorianCalendar(2008,1,10).getTime();
			
			copyFlightScheduleCommand.setParameter( CommandParamNames.SCHEDULE_ID , bsId);
			copyFlightScheduleCommand.setParameter( CommandParamNames.START_DATE , startDate);
			copyFlightScheduleCommand.setParameter( CommandParamNames.END_DATE , endDate);
			
			ServiceResponce bresponce =  (ServiceResponce)copyFlightScheduleCommand.execute();
			
			assertTrue("copy schedule is not successfull", bresponce.isSuccess());
			assertNull("Respond code is not null", bresponce.getResponseCode());
	}
}
