/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 15, 2005 17:17:16
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.persistence.dao;

import java.util.List;

import com.isa.thinair.airschedules.UtilityForTesting;
import com.isa.thinair.airschedules.api.model.FlightLeg;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.airschedules.core.util.LookUpUtils;
import com.isa.thinair.platform.api.util.PlatformTestCase;

/**
 * method to test flight leg dao impl
 * 
 * @author Lasantha Pambagoda
 */
public class TestFlightLegDAOImpl extends PlatformTestCase {

	// flight dao to be test
	private FlightLegDAO flightLegDAO;

	/**
	 * set ou method of the test case
	 */
	protected void setUp() throws Exception {
		super.setUp();
		flightLegDAO = (FlightLegDAO)AirSchedulesUtil
				.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_LEG_DAO);
	}

	/**
	 * teat down method of the test case
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	/**
	 * test method to test getFlightLegs
	 */
	public void testGetFlightLegs() {
		List list = flightLegDAO.getFlightLegs();
		assertNotNull("returned list from the getFlightLegs was null", list);
		assertTrue(
				"database has records of flight legs, "
						+ "but the returned list from the getFlightLegs method has zero elements",
				(list.size() > 0));
	}

	/**
	 * test method to test getFlightLeg
	 */
	public void testGetFlightLeg() {
		FlightLeg flightleg = flightLegDAO.getFlightLeg(10010);
		assertNotNull("database has record with id=10010 for the flight leg,"
				+ " but returned flight leg for the id=10010 was null", flightleg);
	}

	/**
	 * method to test, save new flight leg update existing flight leg remove
	 * existing flight leg
	 */
	public void testSaveUpdateRemoveFlight() {

		FlightLeg flightLeg = UtilityForTesting.createFlightLeg();
		
		// testing save new flight
		saveFlightLegTesting(flightLeg);

		// testing updaing flight
		updateFlightLegTesting(flightLeg.getId().intValue());

		// testing remove flight
		removeFlightLegTesting(flightLeg.getId().intValue());
	}

	/**
	 * method to test save FlightLegs
	 */
	private void saveFlightLegTesting(FlightLeg flightLeg) {
		try {

			flightLegDAO.saveFlightLeg(flightLeg);

		} catch (Exception ex) {

			fail("Failed in the saving new flight leg, error = "
					+ ex.getMessage());
		}
	}

	/**
	 * method to test update flight leg
	 */
	private void updateFlightLegTesting(int id) {

		try {

			FlightLeg fltleg = flightLegDAO.getFlightLeg(id);
			int duration = fltleg.getDuration();
			fltleg.setDuration(20);
			flightLegDAO.saveFlightLeg(fltleg);

			FlightLeg fltLegUpdated = flightLegDAO.getFlightLeg(id);

			assertFalse("Flight leg is not updated", duration == fltLegUpdated
					.getDuration());
			assertEquals("Version number of the updated flight leg is wrong",
					1, fltLegUpdated.getVersion());

		} catch (Exception ex) {
			fail("Failed in the updating flight leg, error = "
					+ ex.getMessage());
		}
	}

	/**
	 * method to test remove FlightLegs
	 */
	private void removeFlightLegTesting(int id) {
		try {

			flightLegDAO.removeFlightLeg(id);
			FlightLeg removedfleg = flightLegDAO.getFlightLeg(id);
			assertNull(
					"Flight Leg has not removed by the removeFlightLeg method in the DAO",
					removedfleg);

		} catch (Exception ex) {

			fail("Failed in the removing flight leg, error = "
					+ ex.getMessage());
		}
	}
}
