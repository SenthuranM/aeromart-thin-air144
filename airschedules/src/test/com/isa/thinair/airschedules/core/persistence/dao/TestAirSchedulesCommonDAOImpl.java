/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.persistence.dao;

import java.util.Collection;

import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.airschedules.core.util.LookUpUtils;
import com.isa.thinair.platform.api.util.PlatformTestCase;

/**
 * test class to test AirSchedulesCommonDAOImpl
 * @author Lasantha Pambagoda
 */
public class TestAirSchedulesCommonDAOImpl extends PlatformTestCase{

	//flight dao to be test
	private AirSchedulesCommonDAO airSchedulesDAO;

	/**
	 * set op method of the test case
	 */
	protected void setUp() throws Exception {
		super.setUp();
		airSchedulesDAO = (AirSchedulesCommonDAO)AirSchedulesUtil
				.getInstance().getLocalBean(InternalConstants.DAOProxyNames.AIR_SCHEDULES_COMMON_DAO);
	}

	/**
	 * teat down method of the test case
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	/**
	 * method to test getFlights
	 */
	public void testGetAllOpertationTypes() {
		
		Collection col = airSchedulesDAO.getAllOpertationTypes();
		assertNotNull("returned list from the getAllOpertationTypes was null", col);
		assertTrue(	"database has records of operation types, but the returned list " +
				"from the getAllOpertationTypes method has zero elements"
				, (col.size() > 0));
	}
}
