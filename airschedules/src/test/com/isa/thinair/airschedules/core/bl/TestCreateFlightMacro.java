/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl;

import com.isa.thinair.airschedules.UtilityForTesting;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.LookUpUtils;
import com.isa.thinair.airschedules.core.utils.CommandNames;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Command;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformTestCase;

/**
 * testing case to test create flight macro
 * @author Lasantha Pambagoda
 */
public class TestCreateFlightMacro extends PlatformTestCase {
	
	//createFlightMacroCommand to be test
	private Command createFlightMacroCommand;
		
	/**
	 * set op method of the test case
	 */
	protected void setUp() throws Exception {
		
		super.setUp();
		createFlightMacroCommand = (Command) AirSchedulesUtil
				.getInstance().getLocalBean(CommandNames.CREATE_FLIGHT_MACRO);
	}
	
	/**
	 * teat down method of the test case
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	
	/**
	 * testing the execute method of the create flight  macro command
	 * @throws ModuleException 
	 */
	public void testExecute() throws ModuleException{
		
		Flight flight  = UtilityForTesting.getFlightToSaveByMacroCommand();
			
		createFlightMacroCommand.setParameter( CommandParamNames.FLIGHT , flight);
		ServiceResponce responce =  (ServiceResponce)createFlightMacroCommand.execute();
		
		assertTrue("Conflicts found creating the flight schedule", responce.isSuccess());
		assertNull("Respond code is not null", responce.getResponseCode());
	}
}
