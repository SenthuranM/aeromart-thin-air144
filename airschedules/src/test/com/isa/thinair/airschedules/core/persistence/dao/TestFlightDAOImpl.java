/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 15, 2005 17:17:16
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.persistence.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import com.isa.thinair.airschedules.UtilityForTesting;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.airschedules.core.util.LookUpUtils;

import com.isa.thinair.platform.api.util.PlatformTestCase;

/**
 * test case to test FlightDAOImpl
 * @author Lasantha Pambagoda
 */
public class TestFlightDAOImpl extends PlatformTestCase {

	//flight dao to be test
	private FlightDAO flightDAO;

	/**
	 * set op method of the test case
	 */
	protected void setUp() throws Exception {
		super.setUp();
		flightDAO = (FlightDAO)AirSchedulesUtil
				.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_DAO);
	}

	/**
	 * teat down method of the test case
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	//////////////////////////////////////////////////
	// Test mehtods of the Flight Schedule DAO
	////////////////////////////////////////////////
	/**
	 * method to test getFlights
	 */
	/*public void testGetFlights() {
		Page Page = flightDAO.getFlights(0,10);
		assertNotNull("returned list from the getFlights was null", Page);
		assertTrue(
				"database has records of flight, but the returned list from the getFlights"
						+ "method has zero elements", (Page.getPageData().size() > 0));
	}*/

	/**
	 * method to test getFlight
	 */
	/*public void testGetFlight() {
		Flight flight = flightDAO.getFlight(10010);
		assertNotNull("database has record with id=10010, "
				+ "but returned flight for the id was null", flight);
	}*/
	
	/**
	 * method to test,
	 * 		save new flight
	 * 		update existing flight
	 * 		remove existing flight
	 */
	/*public void testSaveUpdateRemoveFlight() {
		
		Flight flight = UtilityForTesting.createFlight();

		//testing save new flight
		createFlightTesting(flight);

		//testing updaing flight
		updateFlightTesting(flight.getFlightId().intValue());

		//testing remove flight
		removeFlightTesting(flight.getFlightId().intValue());
	}*/

	/**
	 * method to test batch update of the flight status
	 */
	//public void testBatchUpdateFlightStatus(){
		
	
		/*flightDAO.batchUpdateFlightStatus(10001, UtilityForTesting.getFlyDates(), 
				FlightStatusEnum.CANCELLED.getCode());
		*/
		/*Flight flight1 = flightDAO.getFlight(10001);
		assertEquals("Status not batch updated for flightid=10001", FlightStatusEnum.CANCELLED.getCode(),
				flight1.getStatus());
		Flight flight2 = flightDAO.getFlight(10002);
		assertEquals("Status not batch updated for flightid=10002", FlightStatusEnum.CANCELLED.getCode(),
				flight2.getStatus());
		*/
		//revert back db state
		/*flightDAO.batchUpdateFlightStatus(10001, UtilityForTesting.getFlyDates(), 
				FlightStatusEnum.ACTIVE.getCode());
		*/
	//}

	/**
	 * method to test batch update schedule is of flights
	 */
	/*public void testBatchUpdateScheduleIdOfFlights(){
		
		flightDAO.batchUpdateScheduleIdOfFlights(10000, 10001, 
				UtilityForTesting.getAffectedPeriodForFlightUpdate());
		
		Flight flight1 = flightDAO.getFlight(10001);
		assertEquals("Status not batch updated for flightid=10001", 10000,
				flight1.getScheduleId().intValue());
		Flight flight2 = flightDAO.getFlight(10002);
		assertEquals("Status not batch updated for flightid=10002", 10000,
				flight2.getScheduleId().intValue());
		
		//revert back db state
		flightDAO.batchUpdateScheduleIdOfFlights(10001, 10000, 
				UtilityForTesting.getAffectedPeriodForFlightUpdate());
	}*/
	
	/**
	 * method to test get flights for given flydates
	 */
	public void TestGetFlightsForFlyDates(){
		
		/*Collection collection  = flightDAO.getFlightsForFlyDates(10001, UtilityForTesting.getFlyDates());
		assertNotNull("returned list from the getFlightsForFlyDates was null", collection);
		assertTrue(
				"database has maching records of flights, "
						+ "but the returned list from the getFlightsForFlyDates method has zero elements",
				(collection.size() > 0));
		assertEquals("schedule 10001 has 2 flights for given fly dates", 2, collection.size());
		*/
		
	}
	
	/**
	 * method to test serch flights
	 */
	/*public void TestSearchFlightSchedules(){
		
		List criList = UtilityForTesting.createSearchCriteriaForFlights();
		
		Page page = flightDAO.searchFlight(criList, 0, 10);
		assertNotNull("returned list from the searchFlight was null", page);
		assertTrue(
				"database has maching records of flights, "
						+ "but the returned list from the searchFlight method has zero elements",
				(page.getPageData().size() > 0));
	}*/
	
	/**
	 * method to test get conflicting schedules
	 */
	public void TestGetConflictingSchedules() {
		
		/*Collection collection  
			= flightDAO.getConflictingSchedules( 0, "GG1111" , new GregorianCalendar(2006,0,7).getTime());
		
		assertNotNull("returned list from the getConflictingSchedules was null", collection);
		assertTrue(
				"database has maching records of conflicting flights, " +
				"but the returned list from the getConflictingSchedules " +
				"method returned zero elements",
				(collection.size() > 0));
		*/
	}
	
	/**
	 * method to test get conflicting schedules
	 */
	public void TestGetPossibleDestinations() {
		
		/*Collection collection  
			= flightDAO.getConflictingSchedules( 0, "GG1111" , new GregorianCalendar(2006,0,7).getTime());
		
		assertNotNull("returned list from the getConflictingSchedules was null", collection);
		assertTrue(
				"database has maching records of conflicting flights, " +
				"but the returned list from the getConflictingSchedules " +
				"method returned zero elements",
				(collection.size() > 0));
		*/
	}
	
	/**
	 * method to test get conflicting schedules
	 */
	public void testGetFlightNumbers() {
	/*	
		Collection collection = flightDAO.getFlightNumbers();
		
		assertNotNull("returned list from the getFlightNumbers was null", collection);
		assertTrue("there are more than one unique flight numbers",
				(collection.size() > 0));*/
	}
	
	///////////////////////////////////////////
	// private methods to help tests
	//////////////////////////////////////////
	/**
	 * method to test saveFlight
	 */
	private void createFlightTesting(Flight flight) {
/*
		try {

			flightDAO.saveFlight(flight);

		} catch (Exception ex) {

			fail("Failed in the saving new flight, error = " + ex.getMessage());
		}*/
	}

	/**
	 * method to test removeFlight
	 */
	private void removeFlightTesting(int flightID) {
/*
		try {

			flightDAO.removeFlight(flightID);
			Flight removedFlt = flightDAO.getFlight(flightID);
			assertNull(
					"Flight has not removed by the removeFlight method in the DAO",
					removedFlt);

		} catch (Exception ex) {

			fail("Failed in the removing flight, error = " + ex.getMessage());
		}*/
	}

	/**
	 * method to test updating flight
	 */
	private void updateFlightTesting(int flightID) {
/*
		try {

			Flight flt = flightDAO.getFlight(flightID);
			Integer avaiKilo = flt.getAvailableSeatKilometers();
			flt.setAvailableSeatKilometers(new Integer(20));
			flightDAO.saveFlight(flt);

			Flight fltUpdated = flightDAO.getFlight(flightID);

			assertFalse("Flight is not updated", avaiKilo.intValue() == fltUpdated
					.getAvailableSeatKilometers().intValue());
			assertEquals("Vwersion number of the updated flight is wrong", 1,
					fltUpdated.getVersion());

		} catch (Exception ex) {
			fail("Failed in the updating flight, error = " + ex.getMessage());
		}*/
	}
	
	
	/**
	 * method to test updating flight
	 */
	public void testGetScheduleGDSIdsForFlight() {

		try {
			
			ArrayList<Integer> ids = new ArrayList<Integer>();
			ids.add(137407);
			ids.add(137408);
			ids.add(137409);
			HashMap l = flightDAO.getScheduleGDSIdsForFlight(ids);


//			assertFalse("Flight is not updated", avaiKilo.intValue() == fltUpdated
//					.getAvailableSeatKilometers().intValue());
//			assertEquals("Vwersion number of the updated flight is wrong", 1,
//					fltUpdated.getVersion());

		} catch (Exception ex) {
			ex.printStackTrace();
			fail("Failed in the updating flight, error = " + ex.getMessage());
		}
	}
}
