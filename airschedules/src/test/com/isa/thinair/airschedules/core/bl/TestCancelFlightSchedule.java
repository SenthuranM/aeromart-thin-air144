/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl;

import com.isa.thinair.airschedules.api.dto.FlightAlertDTO;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.LookUpUtils;
import com.isa.thinair.airschedules.core.utils.CommandNames;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Command;

import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformTestCase;

/**
 * test case for test the cancel flight schedule command
 * @author Lasantha Pambagoda
 */
public class TestCancelFlightSchedule extends PlatformTestCase {
	
	//cancelFlightScheduleCommand to be test
	private Command cancelFlightScheduleCommand;
		
	/**
	 * set op method of the test case
	 */
	protected void setUp() throws Exception {
		
		super.setUp();
		cancelFlightScheduleCommand	= (Command) AirSchedulesUtil
				.getInstance().getLocalBean(CommandNames.CANCEL_SCHEDULE_MACRO);
	}
	
	/**
	 * teat down method of the test case
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	
	/**
	 * testing the execute method of the create flight schedule macro command
	 * for schedule befour build
	 * @throws ModuleException 
	 */
	public void testExecuteForBuild() throws ModuleException{
        Integer bsId = new Integer(20066);
        Boolean force = new Boolean(true);
        FlightAlertDTO flightAlertDTO = new FlightAlertDTO();
        flightAlertDTO.setAlertForCancelledFlight(true);
        
        cancelFlightScheduleCommand.setParameter( CommandParamNames.SCHEDULE_ID , bsId);
        cancelFlightScheduleCommand.setParameter( CommandParamNames.FORCE , force);
        cancelFlightScheduleCommand.setParameter( CommandParamNames.FLIGHT_ALERT_DTO , flightAlertDTO);
        ServiceResponce bresponce = (ServiceResponce) cancelFlightScheduleCommand.execute();
        
        assertTrue("Cancel Flight Schedule is not successfull", bresponce.isSuccess());
        assertNull("Respond code is not null", bresponce.getResponseCode());
	}
	
	/**
	 * testing the execute method of the create flight schedule macro command
	 * for schedule after build
	 * @throws ModuleException 
	 */
	public void testExecuteForNotBuild() throws ModuleException{
			
			Integer nbsId = new Integer(10004);
			Boolean force = new Boolean(false);
			
			cancelFlightScheduleCommand.setParameter( CommandParamNames.SCHEDULE_ID , nbsId);
			cancelFlightScheduleCommand.setParameter( CommandParamNames.FORCE , force);
			ServiceResponce nbresponce =  (ServiceResponce)cancelFlightScheduleCommand.execute();
			
			assertTrue("Cancel schedule (AFTER BUILD) is not successfull", nbresponce.isSuccess());
			assertNull("Respond code is not null", nbresponce.getResponseCode());
	}
}
