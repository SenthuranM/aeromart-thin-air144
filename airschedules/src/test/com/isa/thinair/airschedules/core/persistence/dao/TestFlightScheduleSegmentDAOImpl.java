/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 15, 2005 17:17:16
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.persistence.dao;

import java.util.List;

import com.isa.thinair.airschedules.UtilityForTesting;
import com.isa.thinair.airschedules.api.model.FlightScheduleSegment;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.airschedules.core.util.LookUpUtils;
import com.isa.thinair.platform.api.util.PlatformTestCase;

/**
 * test case to test FlightScheduleSegmentDAOImpl 
 * @author Lasantha Pambagoda
 */
public class TestFlightScheduleSegmentDAOImpl extends PlatformTestCase {


	//flight schedule segment dao to be test
	private FlightScheduleSegmentDAO flightScheduleSegmentDAO;
		
	/**
	 * set op method of the test case
	 */
	protected void setUp() throws Exception {
		super.setUp();
		flightScheduleSegmentDAO = (FlightScheduleSegmentDAO) AirSchedulesUtil.getInstance()
				.getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_SEGMENT_DAO);
	}
	
	/**
	 * teat down method of the test case
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	

	/**
	 * method to test getFlightScheduleSegments
	 */
	public void testGetFlightScheduleSegments() {
		List list = flightScheduleSegmentDAO.getFlightScheduleSegments();
		assertNotNull("returned list from the getFlightScheduleSegments was null", list);
		assertTrue(
				"database has records of flight schedule segments, "
						+ "but the returned list from the getFlightScheduleSegments method has zero elements",
				(list.size() > 0));
	}

	/**
	 * method to test getFlightScheduleSegment
	 */
	public void testGetFlightScheduleSegment() {
		FlightScheduleSegment fliScheSeg = flightScheduleSegmentDAO.getFlightScheduleSegment(10000);
		assertNotNull("database has record with id=10000 for the flight schedule segment,"
				+ " but returned flight schedule segment for the id=10000 was null", fliScheSeg);
	}

	
	/**
	 * method to test,
	 * 		save new flight schedule segment
	 * 		update existing flight schedule segment
	 * 		remove existing flight schedule segment
	 */
	public void testSaveUpdateRemoveFlightScheduleSegment() {
		
		FlightScheduleSegment fss = UtilityForTesting.createFlightScheduleSegment();
		fss.setSegmentCode("CMB/DEL");
		
		fss.setScheduleId(new Integer(10000));

		//testing save new flight schedule segement
		saveFlightScheduleSegmnetTesting(fss);

		//testing updaing flight schedule segement
		updateFlightScheduleSegmnetTesting(fss.getFlSchSegId().intValue());

		//testing remove flight schedule segement
		removeFlightScheduleSegmnetTesting(fss.getFlSchSegId().intValue());
	}	
	
	/**
	 * method to test save new flight schedule segment
	 */
	private void saveFlightScheduleSegmnetTesting( FlightScheduleSegment fss ) {
		
		try {

			flightScheduleSegmentDAO.saveFlightScheduleSegment(fss);

		} catch (Exception ex) {

			fail("Failed in the saving new flight schedule segment, error = " + ex.getMessage());
		}
	}

	/**
	 * method to test update flight schedule segment
	 */
	private void updateFlightScheduleSegmnetTesting(int id) {
		
		try {

			FlightScheduleSegment fltss = flightScheduleSegmentDAO.getFlightScheduleSegment(id);
			boolean valid = fltss.getValidFlag();
			fltss.setValidFlag(false);
			flightScheduleSegmentDAO.saveFlightScheduleSegment(fltss);

			FlightScheduleSegment fssUpdated = flightScheduleSegmentDAO.getFlightScheduleSegment(id);

			assertFalse("FlightScheduleSegment is not updated", valid == fssUpdated.getValidFlag());
			assertEquals("Version number of the updated flight schedule segmnet is wrong", 1,
					fssUpdated.getVersion());

		} catch (Exception ex) {
			fail("Failed in the updating flight schedule segmnet, error = " + ex.getMessage());
		}		
	}
	
	/**
	 * method to test remove flight schedule segment
	 */
	private void removeFlightScheduleSegmnetTesting(int id) {
		
		try {

			flightScheduleSegmentDAO.removeFlightScheduleSegment(id);
			FlightScheduleSegment removedFltScheSeg = flightScheduleSegmentDAO.getFlightScheduleSegment(id);
			assertNull(
					"FlightScheduleSegment has not removed by the " +
					"removeFlightScheduleSegment method in the DAO",
					removedFltScheSeg);

		} catch (Exception ex) {

			fail("Failed in the removing flight schedule segment, error = " + ex.getMessage());
		}
	}

}
