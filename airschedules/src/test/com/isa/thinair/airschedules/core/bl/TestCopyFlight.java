/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl;

import java.util.Date;
import java.util.GregorianCalendar;

import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.core.persistence.dao.FlightDAO;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.airschedules.core.util.LookUpUtils;
import com.isa.thinair.airschedules.core.utils.CommandNames;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Command;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformTestCase;

/**
 * testing the copy flight command
 * @author Lasantha Pambagoda
 */
public class TestCopyFlight extends PlatformTestCase {
	
	//createFlightMacroCommand to be test
	private Command copyFlightMacroCommand;
	private FlightDAO flightDAO;
		
	/**
	 * set op method of the test case
	 */
	protected void setUp() throws Exception {
		
		super.setUp();
		copyFlightMacroCommand = (Command) AirSchedulesUtil
				.getInstance().getLocalBean(CommandNames.COPY_FLIGHT_MACRO);
		flightDAO = (FlightDAO) AirSchedulesUtil.getInstance()
				.getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_DAO);	
	}
	
	/**
	 * teat down method of the test case
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	
	/**
	 * testing the execute method of the create flight  macro command
	 * @throws ModuleException 
	 */
	public void testExecuteForScheduledFlight() throws ModuleException{
		
		Flight flight = flightDAO.getFlight(10001);
		Date date = new GregorianCalendar(2009,2,2).getTime();
			
		copyFlightMacroCommand.setParameter( CommandParamNames.FLIGHT , flight);
		copyFlightMacroCommand.setParameter( CommandParamNames.DEPARTURE_DATE , date);
		ServiceResponce responce =  (ServiceResponce)copyFlightMacroCommand.execute();
		
		assertTrue("Conflicts found creating the flight", responce.isSuccess());
		assertNull("Respond code is not null", responce.getResponseCode());	
	}
}
