
/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules;

import junit.framework.Test;
import junit.framework.TestSuite;

import com.isa.thinair.airschedules.core.bl.TestAmmendFlightSchedule;
import com.isa.thinair.airschedules.core.bl.TestBuildFlightSchedule;
import com.isa.thinair.airschedules.core.bl.TestCancelFlight;
import com.isa.thinair.airschedules.core.bl.TestCancelFlightSchedule;
import com.isa.thinair.airschedules.core.bl.TestCopyFlight;
import com.isa.thinair.airschedules.core.bl.TestCopyFlightSchedule;
import com.isa.thinair.airschedules.core.bl.TestCreateFlightMacro;
import com.isa.thinair.airschedules.core.bl.TestCreateFlightScheduleMacro;
import com.isa.thinair.airschedules.core.bl.TestSplitFlightSchedule;
import com.isa.thinair.airschedules.core.bl.TestValidateConflictingFlightSchedules;
import com.isa.thinair.airschedules.core.bl.TestValidateConflictingFlights;
import com.isa.thinair.airschedules.core.persistence.dao.TestAirSchedulesCommonDAOImpl;
import com.isa.thinair.airschedules.core.persistence.dao.TestFlightDAOImpl;
import com.isa.thinair.airschedules.core.persistence.dao.TestFlightLegDAOImpl;
import com.isa.thinair.airschedules.core.persistence.dao.TestFlightScheduleDAOImpl;
import com.isa.thinair.airschedules.core.persistence.dao.TestFlightScheduleLegDAOImpl;
import com.isa.thinair.airschedules.core.persistence.dao.TestFlightScheduleSegmentDAOImpl;
import com.isa.thinair.airschedules.core.persistence.dao.TestFlightSegementDAOImpl;

/**
 * test suite to run all the methods
 * @author Lasantha Pambagoda
 *
 */
public class RunAllAirSchedueleTests {

	/**
	 * reuns all the air schedule tests
	 * @return
	 */
    public static Test suite() { 
    	
        TestSuite suite = new TestSuite("All Air Schedule Tests");

        //testing module initialization
        suite.addTest(new TestSuite(TestModuleInitialization.class));
        
        //testing DAOs
        suite.addTest(new TestSuite(TestFlightDAOImpl.class));        
        suite.addTest(new TestSuite(TestFlightLegDAOImpl.class));
        suite.addTest(new TestSuite(TestFlightSegementDAOImpl.class));
        suite.addTest(new TestSuite(TestFlightScheduleDAOImpl.class));
        suite.addTest(new TestSuite(TestFlightScheduleLegDAOImpl.class));
        suite.addTest(new TestSuite(TestFlightScheduleSegmentDAOImpl.class));        
        suite.addTest(new TestSuite(TestAirSchedulesCommonDAOImpl.class));
        
        //testing commands for schedule
        suite.addTest(new TestSuite(TestValidateConflictingFlightSchedules.class));
        suite.addTest(new TestSuite(TestValidateConflictingFlights.class));
        suite.addTest(new TestSuite(TestCreateFlightScheduleMacro.class));
        suite.addTest(new TestSuite(TestBuildFlightSchedule.class));
        suite.addTest(new TestSuite(TestCancelFlightSchedule.class));
        suite.addTest(new TestSuite(TestCopyFlightSchedule.class));
        suite.addTest(new TestSuite(TestSplitFlightSchedule.class));
        suite.addTest(new TestSuite(TestAmmendFlightSchedule.class));
        
        //testing commands for flight
        suite.addTest(new TestSuite(TestCreateFlightMacro.class));
        suite.addTest(new TestSuite(TestCancelFlight.class));
        suite.addTest(new TestSuite(TestCopyFlight.class));
        
        //testing remote ejbs
        //suite.addTest(new TestSuite(TestFlightServiceRemote.class));
        //suite.addTest(new TestSuite(TestScheduleServiceRemote.class));
        
        return suite; 
   }

}
