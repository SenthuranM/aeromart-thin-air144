package com.isa.thinair.airschedules.api.utils;

public enum FlightEventCode {
	FLIGHT_CREATED, FLIGHT_CLOSED, FLIGHT_CANCELLED, FLIGHT_GDS_PUBLISHED_CHANGED, FLIGHT_REOPENED;
}
