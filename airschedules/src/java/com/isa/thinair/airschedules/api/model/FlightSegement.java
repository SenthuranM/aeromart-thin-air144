/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:19:25
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.api.model;

import java.util.Date;

import com.isa.thinair.airschedules.api.utils.FlightSegmentStatusEnum;

/**
 * FlightSegement is the entity class to repesent a FlightSegement model
 * 
 * @author Code Generation Tool, Lasantha Pambagoda
 * @version Ver 1.0.0 *
 * @hibernate.class table = "T_FLIGHT_SEGMENT"
 */
public class FlightSegement extends FlySegement {

	private static final long serialVersionUID = 4908858102087803167L;

	// private fields
	private String segmentCode;

	private Integer fltSegId;

	private Integer flightId;

	private Date estTimeDepatureLocal;

	private Date estTimeArrivalLocal;

	private Date estTimeDepatureZulu;

	private Date estTimeArrivalZulu;

	private Integer overlapSegmentId;

	private String status = FlightSegmentStatusEnum.OPEN.getCode();

	private String checkinStatus = CHECKIN_NOT_OPEN;

	private Integer departureTerminalId;

	private Integer arrivalTerminalId;
	
	private String baggageAllowanceRemarks;

	/* Constants to represent checkin status. */
	public static final String CHECKIN_CLOSE = "CLS";
	public static final String CHECKIN_COMPLETE = "COM";
	public static final String CHECKIN_NOT_OPEN = "NPN";
	public static final String CHECKIN_OPEN = "OPN";
	public static final String CHECKIN_RE_OPEN = "RPN";

	public FlightSegement() {

	}

	public FlightSegement(Date arrivalDateZulu) {
		this.estTimeArrivalZulu = arrivalDateZulu;
	}

	/**
	 * returns the fltSegId
	 * 
	 * @return Returns the fltSegId.
	 * @hibernate.id column = "FLT_SEG_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_FLIGHT_SEGMENT"
	 */
	public Integer getFltSegId() {
		return fltSegId;
	}

	/**
	 * sets the fltSegId
	 * 
	 * @param fltSegId
	 */
	public void setFltSegId(Integer fltSegId) {
		this.fltSegId = fltSegId;
	}

	/**
	 * returns the flightId
	 * 
	 * @return Returns the flightId.
	 * @hibernate.property column = "FLIGHT_ID"
	 */
	public Integer getFlightId() {
		return flightId;
	}

	/**
	 * sets the flightId
	 * 
	 * @param flightId
	 */
	public void setFlightId(Integer flightId) {
		this.flightId = flightId;
	}

	/**
	 * @return Returns the estTimeDepatureLocal.
	 * @hibernate.property column = "EST_TIME_DEPARTURE_LOCAL"
	 */
	public Date getEstTimeDepatureLocal() {
		return estTimeDepatureLocal;
	}

	/**
	 * @param estTimeDepatureLocal
	 *            The estTimeDepatureLocal to set.
	 */
	public void setEstTimeDepatureLocal(Date estTimeDepatureLocal) {
		this.estTimeDepatureLocal = estTimeDepatureLocal;
	}

	/**
	 * @return Returns the estTimeArrivalLocal.
	 * @hibernate.property column = "EST_TIME_ARRIVAL_LOCAL"
	 */
	public Date getEstTimeArrivalLocal() {
		return estTimeArrivalLocal;
	}

	/**
	 * @param estTimeArrivalLocal
	 *            The estTimeArrivalLocal to set.
	 */
	public void setEstTimeArrivalLocal(Date estTimeArrivalLocal) {
		this.estTimeArrivalLocal = estTimeArrivalLocal;
	}

	/**
	 * @return Returns the overlapSegmentId.
	 * @hibernate.property column = "OVERLAP_FLT_SEG_ID"
	 */
	public Integer getOverlapSegmentId() {
		return overlapSegmentId;
	}

	/**
	 * @param overlapSegmentId
	 *            The overlapSegmentId to set.
	 */
	public void setOverlapSegmentId(Integer overlapSegmentId) {
		this.overlapSegmentId = overlapSegmentId;
	}

	/**
	 * check is overlapping
	 * 
	 * @return boolean
	 */
	public boolean isOverlapping() {
		return (overlapSegmentId == null) ? false : true;
	}

	/**
	 * @return Returns the estTimeArrivalZulu.
	 * @hibernate.property column = "EST_TIME_ARRIVAL_ZULU"
	 */
	public Date getEstTimeArrivalZulu() {
		return estTimeArrivalZulu;
	}

	/**
	 * @param estTimeArrivalZulu
	 *            The estTimeArrivalZulu to set.
	 */
	public void setEstTimeArrivalZulu(Date estTimeArrivalZulu) {
		this.estTimeArrivalZulu = estTimeArrivalZulu;
	}

	/**
	 * @return Returns the estTimeDepatureZulu.
	 * @hibernate.property column = "EST_TIME_DEPARTURE_ZULU"
	 */
	public Date getEstTimeDepatureZulu() {
		return estTimeDepatureZulu;
	}

	/**
	 * @param estTimeDepatureZulu
	 *            The estTimeDepatureZulu to set.
	 */
	public void setEstTimeDepatureZulu(Date estTimeDepatureZulu) {
		this.estTimeDepatureZulu = estTimeDepatureZulu;
	}

	/**
	 * @return Returns the segmentCode.
	 * @hibernate.property column = "SEGMENT_CODE"
	 */
	public String getSegmentCode() {
		return segmentCode;
	}

	/**
	 * @param segmentCode
	 *            The segmentCode to set.
	 */
	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	/**
	 * @return Returns the status.
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * gets the Checkin Status
	 * 
	 * @return Returns the Checkin Status
	 * @hibernate.property column = "CHECKIN_STATUS"
	 */
	public String getCheckinStatus() {
		return checkinStatus;
	}

	/**
	 * @param checkinStatus
	 *            The checkinStatus to set.
	 */
	public void setCheckinStatus(String checkinStatus) {
		this.checkinStatus = checkinStatus;
	}

	/**
	 * gets the departure terminal id.
	 * 
	 * @return the departure terminal id.
	 * @hibernate.property column = "DEPARTURE_TERMINAL_ID"
	 */
	public Integer getDepartureTerminalId() {
		return departureTerminalId;
	}

	/**
	 * @param departureTerminalId
	 *            The departure terminal id.
	 */
	public void setDepartureTerminalId(Integer departureTerminalId) {
		this.departureTerminalId = departureTerminalId;
	}

	/**
	 * gets the arrival terminal id.
	 * 
	 * @return the arrival terminal id.
	 * @hibernate.property column = "ARRIVAL_TERMINAL_ID"
	 */
	public Integer getArrivalTerminalId() {
		return arrivalTerminalId;
	}

	/**
	 * @param arrivalTerminalId
	 *            The arrival terminal id.
	 */
	public void setArrivalTerminalId(Integer arrivalTerminalId) {
		this.arrivalTerminalId = arrivalTerminalId;
	}

	/**
	 * gets the baggage allowance remarks.
	 * 
	 * @return the baggage allowance remarks.
	 * @hibernate.property column = "BAGGAGE_ALLOWANCE_REMARKS"
	 */
	public String getBaggageAllowanceRemarks() {
		return baggageAllowanceRemarks;
	}

	/**
	 * @param baggageAllowanceRemarks
	 *            The baggage allowance remarks.
	 */
	public void setBaggageAllowanceRemarks(String baggageAllowanceRemarks) {
		this.baggageAllowanceRemarks = baggageAllowanceRemarks;
	}
}
