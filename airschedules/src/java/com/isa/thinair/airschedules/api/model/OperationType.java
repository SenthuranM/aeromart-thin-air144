/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.api.model;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * OperationType is the entity class to repesent a OperationType model
 * 
 * @author Lasantha Pambagoda
 * @hibernate.class table = "T_OPERATION_TYPE"
 */
public class OperationType extends Persistent {

	private int id;

	private String description;

	/** Holds the visible flag */
	public static final String VISIBLE_IN_ITINERARY = "Y";

	/** Holds the not visible flag */
	public static final String NOT_VISIBLE_IN_ITINERARY = "N";

	/**
	 * @return Returns the description.
	 * @hibernate.property column = "OPERATION_TYPE_DESCRIPTION"
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            The description to set.
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return Returns the id.
	 * @hibernate.id column = "OPERATION_TYPE_ID" generator-class = "assigned"
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            The id to set.
	 */
	public void setId(int id) {
		this.id = id;
	}

}
