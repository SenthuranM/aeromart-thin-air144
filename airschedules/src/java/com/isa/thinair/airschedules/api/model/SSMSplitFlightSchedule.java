/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:18:22
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.api.model;

import java.util.Date;
import java.util.Set;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * 
 * @author Code Generation Tool
 * @version Ver 1.0.0
 * @hibernate.class table = "T_SSM_SPLIT_SCHEDULE"
 */
public class SSMSplitFlightSchedule extends Persistent implements Comparable<SSMSplitFlightSchedule> {

	private static final long serialVersionUID = -1339805293681625902L;

	private Integer splitScheduleId;

	private Integer scheduleId;

	private Date startDate;

	private Date stopDate;

	private Date startDateLocal;

	private Date stopDateLocal;

	private Set<SSMSplitFlightScheduleLeg> ssmSplitFlightScheduleLeg;

	/**
	 * returns the scheduleId
	 * 
	 * @return Returns the scheduleId.
	 * @hibernate.id column = "SSM_SPLIT_SCHEDULE_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_SSM_SPLIT_SCHEDULE"
	 */
	public Integer getSplitScheduleId() {
		return splitScheduleId;
	}

	public void setSplitScheduleId(Integer splitScheduleId) {
		this.splitScheduleId = splitScheduleId;
	}

	/**
	 * returns the scheduleId
	 * 
	 * @return scheduleId
	 * @hibernate.property column = "SCHEDULE_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_SSM_SPLIT_SCHEDULE"
	 */
	public Integer getScheduleId() {
		return scheduleId;
	}

	/**
	 * sets the scheduleId
	 * 
	 * @param scheduleId
	 *            The scheduleId to set.
	 */
	public void setScheduleId(Integer scheduleId) {
		this.scheduleId = scheduleId;
	}

	/**
	 * returns the startDate
	 * 
	 * @return Returns the startDate.
	 * @hibernate.property column = "START_DATE"
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * sets the startDate
	 * 
	 * @param startDate
	 *            The startDate to set.
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * returns the stopDate
	 * 
	 * @return Returns the stopDate.
	 * @hibernate.property column = "STOP_DATE"
	 */
	public Date getStopDate() {
		return stopDate;
	}

	/**
	 * sets the stopDate
	 * 
	 * @param stopDate
	 *            The stopDate to set.
	 */
	public void setStopDate(Date stopDate) {
		this.stopDate = stopDate;
	}

	/**
	 * @return Returns the ssmSplitFlightScheduleLeg.
	 * @hibernate.set lazy="false" cascade="all"
	 * @hibernate.collection-key column="SSM_SPLIT_SCHEDULE_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airschedules.api.model.SSMSplitFlightScheduleLeg"
	 */
	public Set<SSMSplitFlightScheduleLeg> getSsmSplitFlightScheduleLeg() {
		return ssmSplitFlightScheduleLeg;
	}

	public void setSsmSplitFlightScheduleLeg(Set<SSMSplitFlightScheduleLeg> ssmSplitFlightScheduleLeg) {
		this.ssmSplitFlightScheduleLeg = ssmSplitFlightScheduleLeg;
	}

	/**
	 * returns the startDateLocal
	 * 
	 * @return Returns the startDateLocal.
	 * @hibernate.property column = "START_DATE_LOCAL"
	 */
	public Date getStartDateLocal() {
		return startDateLocal;
	}

	/**
	 * @param startDateLocal
	 *            The startDateLocal to set.
	 */
	public void setStartDateLocal(Date startDateLocal) {
		this.startDateLocal = startDateLocal;
	}

	/**
	 * returns the stopDateLocal
	 * 
	 * @return Returns the stopDateLocal.
	 * @hibernate.property column = "STOP_DATE_LOCAL"
	 */
	public Date getStopDateLocal() {
		return stopDateLocal;
	}

	/**
	 * @param stopDateLocal
	 *            The stopDateLocal to set.
	 */
	public void setStopDateLocal(Date stopDateLocal) {
		this.stopDateLocal = stopDateLocal;
	}

	public int hashCode() {
		return new HashCodeBuilder().append(getSplitScheduleId()).toHashCode();
	}

	/**
	 * Overides the equals
	 * 
	 * @param o
	 * @return
	 */
	public boolean equals(Object o) {
		// If it's a same class instance
		if (o instanceof SSMSplitFlightSchedule) {
			SSMSplitFlightSchedule castObject = (SSMSplitFlightSchedule) o;
			if (castObject.getSplitScheduleId() == null || this.getSplitScheduleId() == null) {
				return false;
			} else if (castObject.getSplitScheduleId().intValue() == this.getSplitScheduleId().intValue()) {
				return true;
			} else {
				return false;
			}
		}
		// If it's not
		else {
			return false;
		}
	}

	@Override
	public int compareTo(SSMSplitFlightSchedule obj) {
		if (this.getStartDate() != null && obj.getStartDate() != null) {
			return this.getStartDate().compareTo(obj.getStartDate());
		}
		return 0;
	}

}
