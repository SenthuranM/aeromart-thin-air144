/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:16:12
 * 
 * $Id$
 * 
 * ===============================================================================
 */

package com.isa.thinair.airschedules.api.criteria;

import java.io.Serializable;
import java.util.Date;

public class FlightFlightSegmentSearchCriteria implements Serializable {

	private static final long serialVersionUID = 1L;
	private String flightNumber;
	private Date estStartTimeZulu;
	private Date estEndTimeZulu;
	private String status;
	private String checkinStatus;
	private String segmentCode;

	/**
	 * @return Returns the flightNumber
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * @param flightNumber
	 *            The flightNumber to set.
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * @return Returns the estStartTimeZulu
	 */
	public Date getEstStartTimeZulu() {
		return estStartTimeZulu;
	}

	/**
	 * @param estStartTimeZulu
	 *            The estStartTimeZulu to set.
	 */
	public void setEstStartTimeZulu(Date estStartTimeZulu) {
		this.estStartTimeZulu = estStartTimeZulu;
	}

	/**
	 * @return Returns the estEndTimeZulu
	 */
	public Date getEstEndTimeZulu() {
		return estEndTimeZulu;
	}

	/**
	 * @param estEndTimeZulu
	 *            The estEndTimeZulu to set.
	 */
	public void setEstEndTimeZulu(Date estEndTimeZulu) {
		this.estEndTimeZulu = estEndTimeZulu;
	}

	/**
	 * @return Returns the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return Returns the checkinStatus
	 */
	public String getCheckinStatus() {
		return checkinStatus;
	}

	/**
	 * @param checkinStatus
	 *            The checkinStatus to set.
	 */
	public void setCheckinStatus(String checkinStatus) {
		this.checkinStatus = checkinStatus;
	}

	/**
	 * @return Returns the segmentCode
	 */
	public String getSegmentCode() {
		return segmentCode;
	}

	/**
	 * @param segmentCode
	 *            The segmentCode to set.
	 */
	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

}
