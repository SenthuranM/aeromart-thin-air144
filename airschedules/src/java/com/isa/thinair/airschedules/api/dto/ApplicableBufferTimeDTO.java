package com.isa.thinair.airschedules.api.dto;

import java.io.Serializable;

/**
 * 
 * @author rumesh
 * 
 */
public class ApplicableBufferTimeDTO implements Serializable {
	private static final long serialVersionUID = -308163649350877080L;

	private long applicableBufferTime;
	private boolean fareRuleBufferUsed;

	public ApplicableBufferTimeDTO() {

	}

	public ApplicableBufferTimeDTO(long applicableBufferTime, boolean fareRuleBufferUsed) {
		this.applicableBufferTime = applicableBufferTime;
		this.fareRuleBufferUsed = fareRuleBufferUsed;
	}

	/**
	 * @return the applicableBufferTime
	 */
	public long getApplicableBufferTime() {
		return applicableBufferTime;
	}

	/**
	 * @param applicableBufferTime
	 *            the applicableBufferTime to set
	 */
	public void setApplicableBufferTime(long applicableBufferTime) {
		this.applicableBufferTime = applicableBufferTime;
	}

	/**
	 * @return the fareRuleBufferUsed
	 */
	public boolean isFareRuleBufferUsed() {
		return fareRuleBufferUsed;
	}

	/**
	 * @param fareRuleBufferUsed
	 *            the fareRuleBufferUsed to set
	 */
	public void setFareRuleBufferUsed(boolean fareRuleBufferUsed) {
		this.fareRuleBufferUsed = fareRuleBufferUsed;
	}
}
