package com.isa.thinair.airschedules.api.utils;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableIBOBFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.SelectedFlightDTO;
import com.isa.thinair.airschedules.api.dto.ApplicableBufferTimeDTO;
import com.isa.thinair.airschedules.api.dto.ConnectedAvailableFlightSegmentsDTO;
import com.isa.thinair.airschedules.api.dto.PastFlightInfoDTO;
import com.isa.thinair.airschedules.api.model.CodeShareMCFlight;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.gdsservices.api.util.GDSServicesModuleUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * utility class for flight related utility functions
 * 
 * @author Chandana Kithalagama
 */
public class FlightUtil {

	public static final String FLIGHT_TYPE_INT = "INT";
	public static final String FLIGHT_TYPE_DOM = "DOM";
	public static final String FLIGHT_TYPE_INT_DESC = "International";
	public static final String FLIGHT_TYPE_DOM_DESC = "Domestic";

	private static Log log = LogFactory.getLog(FlightUtil.class);

	/**
	 * private method to get copy of given flight. (used for cloning a flight)
	 * 
	 * @param flight
	 * @return flight
	 */
	public static Flight getCopyOfFlight(Flight flight) {

		// copy all the fields
		Flight copy = copyOnlyFlightAttributes(flight, new Flight());

		// setting flight schedule legs
		copy.setFlightLegs(LegUtil.cloneLegs(flight.getFlightLegs()));

		// setting flight schedule segments
		copy.setFlightSegements(SegmentUtil.cloneSegments(flight.getFlightSegements()));

		return copy;
	}

	/**
	 * private method to copy only flight attributes
	 * 
	 * @param from
	 * @param to
	 * @return flight
	 */
	private static Flight copyOnlyFlightAttributes(Flight from, Flight to) {

		to.setOperationTypeId(from.getOperationTypeId());

		if (from.getFlightId() != null) {
			to.setFlightId(new Integer(from.getFlightId().intValue()));
		}

		if (from.getFlightNumber() != null) {
			to.setFlightNumber(new String(from.getFlightNumber()));
		}

		if (from.getDepartureDate() != null) {
			to.setDepartureDate(new Date(from.getDepartureDate().getTime()));
		}

		if (from.getDepartureDateLocal() != null) {
			to.setDepartureDateLocal(new Date(from.getDepartureDateLocal().getTime()));
		}

		if (from.getModelNumber() != null) {
			to.setModelNumber(new String(from.getModelNumber()));
		}

		if (from.getStatus() != null) {
			to.setStatus(new String(from.getStatus()));
		}

		if (from.getOriginAptCode() != null) {
			to.setOriginAptCode(new String(from.getOriginAptCode()));
		}

		if (from.getDestinationAptCode() != null) {
			to.setDestinationAptCode(new String(from.getDestinationAptCode()));
		}

		if (from.getScheduleId() != null) {
			to.setScheduleId(new Integer(from.getScheduleId().intValue()));
		}

		if (from.getAvailableSeatKilometers() != null) {
			to.setAvailableSeatKilometers(new Integer(from.getAvailableSeatKilometers().intValue()));
		}

		if (from.getTailNumber() != null) {
			to.setTailNumber(new String(from.getTailNumber()));
		}

		// setting the GDS Ids list
		if (from.getGdsIds() != null) {
			Set<Integer> hashSet = new HashSet<Integer>();
			Iterator<Integer> itGds = from.getGdsIds().iterator();
			while (itGds.hasNext()) {
				Integer gdsId = itGds.next();
				hashSet.add(new Integer(gdsId));
			}
			to.setGdsIds(hashSet);
		}

		if (from.getFlightType() != null) {
			to.setFlightType(from.getFlightType());
		}

		if (from.getRemarks() != null) {
			to.setRemarks(from.getRemarks());
		}

		if (from.getMealTemplateId() != null) {
			to.setMealTemplateId(from.getMealTemplateId());
		}

		if (from.getSeatChargeTemplateId() != null) {
			to.setSeatChargeTemplateId(from.getSeatChargeTemplateId());
		}

		if (from.getCsOCCarrierCode() != null) {
			to.setCsOCCarrierCode(from.getCsOCCarrierCode());
		}

		if (from.getCsOCFlightNumber() != null) {
			to.setCsOCFlightNumber(from.getCsOCFlightNumber());
		}

		// setting the code share flight NOs
		if (from.getCodeShareMCFlights() != null) {
			Set<CodeShareMCFlight> csFlightNoSet = new HashSet<CodeShareMCFlight>();
			Iterator<CodeShareMCFlight> csFlightNosItr = from.getCodeShareMCFlights().iterator();
			while (csFlightNosItr.hasNext()) {
				CodeShareMCFlight csFlightNo = csFlightNosItr.next();
				csFlightNoSet.add(csFlightNo);
			}
			to.setCodeShareMCFlights(csFlightNoSet);
		}

		return to;
	}

	/*
	 * This will return flight type description. Should add new flight types accordingly
	 */
	public static String getFlightTypeDescription(String flightTypeCode) {
		if (FLIGHT_TYPE_INT.equalsIgnoreCase(flightTypeCode)) {
			return FLIGHT_TYPE_INT_DESC;
		} else if (FLIGHT_TYPE_DOM.equalsIgnoreCase(flightTypeCode)) {
			return FLIGHT_TYPE_DOM_DESC;
		}

		return null;
	}

	public static ApplicableBufferTimeDTO getApplicableModifyBufferTime(long internationalModifyBuffer, long domesticModifyBuffer,
			Integer flightId) throws ModuleException {
		long modifyBufferTime = AppSysParamsUtil.getInternationalBufferDurationInMillis();
		boolean isFareRuleBufferUsed = false;

		if (flightId != null && flightId != 0) {
			Flight flight = AirSchedulesUtil.getFlightBD().getFlight(flightId);

			if (FLIGHT_TYPE_INT.equals(flight.getFlightType())) {
				if (internationalModifyBuffer == 0) {
					modifyBufferTime = AppSysParamsUtil.getInternationalBufferDurationInMillis();
				} else {
					isFareRuleBufferUsed = true;
					modifyBufferTime = internationalModifyBuffer;
				}
			} else if (FLIGHT_TYPE_DOM.equals(flight.getFlightType())) {
				if (domesticModifyBuffer == 0) {
					modifyBufferTime = AppSysParamsUtil.getDomesticBufferDurationInMillis();
				} else {
					isFareRuleBufferUsed = true;
					modifyBufferTime = domesticModifyBuffer;
				}
			}
		}

		return new ApplicableBufferTimeDTO(modifyBufferTime, isFareRuleBufferUsed);
	}

	public static String getApplicableResCutoffTime(Integer flightId) throws ModuleException {

		String internationalCutoffTime = CommonsServices.getGlobalConfig()
				.getBizParam(SystemParamKeys.AGENT_RESERVATION_CUTOFF_TIME);
		String domesticCutoffTime = CommonsServices.getGlobalConfig()
				.getBizParam(SystemParamKeys.RESERVATION_CUTOFF_TIME_FOR_DOMESTIC_FLIGHTS);

		if (flightId != null && flightId != 0) {
			Flight flight = AirSchedulesUtil.getFlightBD().getFlight(flightId);

			if (FLIGHT_TYPE_INT.equals(flight.getFlightType())) {
				return internationalCutoffTime;
			} else if (FLIGHT_TYPE_DOM.equals(flight.getFlightType())) {
				return domesticCutoffTime;
			}
		}
		return internationalCutoffTime;
	}

	public static String getDuration(Date departureDate, Date arrivalDate) {

		NumberFormat nfr = new DecimalFormat("##00");

		Calendar depCal = Calendar.getInstance();
		depCal.setTime(departureDate);

		Calendar arrCal = Calendar.getInstance();
		arrCal.setTime(arrivalDate);

		long diff = arrCal.getTimeInMillis() - depCal.getTimeInMillis();

		long diffMinutes = diff / (60 * 1000);

		String durationHoursStr = nfr.format((diffMinutes / 60)) + ":" + nfr.format((diffMinutes % 60));

		return durationHoursStr;
	}

	public static List<AvailableIBOBFlightSegment> getAvailableOBIBFlights(Collection<Integer> flightSegmentIds,
			String classOfService, Integer ondSequence, boolean isInboundFlight, boolean ignoreFlightStatus,
			boolean isUnTouchedOnd, Integer reqOndSequence) throws ModuleException {
		Collection<AvailableFlightSegment> availableFlightSegments = AirSchedulesUtil.getFlightBD().getAvailableFlightSegments(
				flightSegmentIds, classOfService, isInboundFlight, ondSequence, ignoreFlightStatus, isUnTouchedOnd);

		if ((availableFlightSegments.size() == 1 && availableFlightSegments.iterator().next().getFlightSegmentDTOs().size() == 0)
				|| availableFlightSegments.size() != flightSegmentIds.size()) {
			log.error("Flight retrieval error ---- FlightSegmentIds : " + flightSegmentIds.toString() + " ----  ClassOfService : "
					+ classOfService);
			throw new ModuleException("airinventory.arg.farequote.flightclosed");
		}

		AvailableIBOBFlightSegment availableFlightSegment = null;
		if (availableFlightSegments.size() == 1) {
			availableFlightSegment = availableFlightSegments.iterator().next();
		} else {
			ConnectedAvailableFlightSegmentsDTO connectedAvailableFlightSegmentsDTO = new ConnectedAvailableFlightSegmentsDTO();
			if (availableFlightSegments.size() > 0) {
				connectedAvailableFlightSegmentsDTO.getAvailableFlightSegments().addAll(availableFlightSegments);
				availableFlightSegment = connectedAvailableFlightSegmentsDTO.constructAvailableConnectedFlight();
			}
		}
		if (availableFlightSegment != null) {
			if (reqOndSequence != null) {
				availableFlightSegment.setRequestedOndSequence(reqOndSequence);
			} else {
				availableFlightSegment.setRequestedOndSequence(ondSequence);

			}
		}

		return Arrays.asList(availableFlightSegment);
	}

	public static void updateAvailableFlightSegments(Collection<Integer> flightSegmentIds, String classOfService,
			SelectedFlightDTO selectedFlight, Integer ondSequence, boolean isInboundFlight, boolean ignoreFlightStatus,
			boolean isUnTouchedOnd, Integer reqOndSequence) throws ModuleException {

		selectedFlight.setOndWiseSelectedDateAllFlights(ondSequence, getAvailableOBIBFlights(flightSegmentIds, classOfService,
				ondSequence, isInboundFlight, ignoreFlightStatus, isUnTouchedOnd, null));
	}

	public static boolean isFlightWithinCutOffTime(Integer flightSegId) throws ModuleException {
		List<Flight> flightList = AirSchedulesUtil.getFlightBD().getActiveFlightWithinCutOffTime(flightSegId);
		long reservationCutOffTime = AppSysParamsUtil.getAgentReservationCutoverInMillis();
		long reservationCutOffTimeForDomestic = AppSysParamsUtil.getReservationCutoverForDomesticInMillis();
		long timeToDeparture = flightList.get(0).getDepartureDate().getTime()
				- CalendarUtil.getCurrentSystemTimeInZulu().getTime();
		if (FlightUtil.FLIGHT_TYPE_INT.equals(flightList.get(0).getFlightType())) {
			if (timeToDeparture > 0 && (timeToDeparture < reservationCutOffTime)) {
				return true;
			}
		} else if (FlightUtil.FLIGHT_TYPE_DOM.equals(flightList.get(0).getFlightType())) {
			if (timeToDeparture > 0 && (timeToDeparture < reservationCutOffTimeForDomestic)) {
				return true;
			}
		}
		return false;
	}

	public static boolean isDomesticRoute(String origin, String destination) {
		if (!StringUtil.isNullOrEmpty(origin) && !StringUtil.isNullOrEmpty(destination)) {
			Object[] startOndAirportInfo = CommonsServices.getGlobalConfig().getAirportInfo(origin, false);
			Object[] endOndAirportInfo = CommonsServices.getGlobalConfig().getAirportInfo(destination, false);

			if (startOndAirportInfo != null && endOndAirportInfo != null && startOndAirportInfo.length >= 4
					&& endOndAirportInfo.length >= 4) {
				return startOndAirportInfo[3].equals(endOndAirportInfo[3]);
			}

		}
		return false;
	}

	/**
	 * This method expects the time in {dd-MM-yyyy HH:mm} format
	 *
	 * @param flightDeptTime time in {dd-MM-yyyy HH:mm}
	 * @return
	 * @throws ParseException
	 */
	public static boolean showETOpeningSection(String flightDeptTime) throws ParseException {
		String currentZuluTime = CalendarUtil
				.getDateInFormattedString(CalendarUtil.PATTERN11, CalendarUtil.getCurrentSystemTimeInZulu());

		Date estFlightDeptDate = CalendarUtil.getParsedTime(flightDeptTime, CalendarUtil.PATTERN11);
		Date currentDate = CalendarUtil.getParsedTime(currentZuluTime, CalendarUtil.PATTERN11);

		return CalendarUtil.isLessThan(estFlightDeptDate, currentDate) && AppSysParamsUtil
				.isOpenAllTheEticketsInPastFlights();
	}

	public static Collection<Integer> getPaxFareSegIdsInPastFlight(String flightId) throws ParseException {
		return GDSServicesModuleUtil.getFlightBD().getPaxFareSegETIds(flightId);
	}

	public static void updatePaxETStatus(PastFlightInfoDTO pastFlightInfoDTO) throws ParseException, ModuleException {
		Map<String, String[]> contentMap = createUpdatedDataMapForAudit(pastFlightInfoDTO);
		pastFlightInfoDTO.setContentMap(contentMap);
		GDSServicesModuleUtil.getFlightBD().updateFlownReservationsDetails(pastFlightInfoDTO);
	}

	public static Map<String, String[]> createUpdatedDataMapForAudit(PastFlightInfoDTO pastFlightInfoDTO) {
		SimpleDateFormat psDateFormat = new SimpleDateFormat("dd/MM/yyyy");

		List<String> changedValList = new ArrayList<>();
		try {
			if (!StringUtil.isNullOrEmpty(pastFlightInfoDTO.getDepartureDate())) {
				changedValList.add("Departure Date= " + CalendarUtil.getDateInFormattedString("dd/MM/yyyy",
						psDateFormat.parse(pastFlightInfoDTO.getDepartureDate())));
			}
			if (!StringUtil.isNullOrEmpty(pastFlightInfoDTO.getFlightNumber())) {
				changedValList.add("Flight Number= " + pastFlightInfoDTO.getFlightNumber());
			}
			if (changedValList.size() > 0) {

				SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
				Date today = new Date();
				changedValList.add("Modified Date= " + formatter.format(today));
			}
		} catch (ParseException e) {
			log.error("Parse Exception" + e.getMessage());
		}
		Map<String, String[]> contentMap = new HashMap<>();
		contentMap.put("updated_PAX/ET_status", changedValList.toArray(new String[changedValList.size()]));
		return contentMap;

	}

	public static boolean isWithinActiveTimeDuration(Date scheduleDate, Date currentDate) {
		int pastFlightActivationDurationInDays = AppSysParamsUtil.getPastFlightActivationDurationInDays();
		int diffDays = CalendarUtil.getTimeDifferenceInDays(scheduleDate, currentDate);
		boolean status = false;
		if (diffDays < pastFlightActivationDurationInDays) {
			status = true;
		}
		return status;
	}

}
