package com.isa.thinair.airschedules.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FlightMealNotifyStatusDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2209980009873662170L;
	List<Integer> flightIds;
	String status;

	/**
	 * @return the flightIds
	 */
	public List<Integer> getFlightIds() {
		return flightIds;
	}

	/**
	 * @param flightIds
	 *            the flightIds to set
	 */
	public void addFlightId(Integer flightId) {
		if (this.flightIds == null) {
			this.flightIds = new ArrayList<Integer>();
		}
		this.flightIds.add(flightId);
	}

	public void addFlightIds(List<Integer> flightIds) {
		if (this.flightIds == null) {
			this.flightIds = new ArrayList<Integer>();
		}
		this.flightIds.addAll(flightIds);
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

}
