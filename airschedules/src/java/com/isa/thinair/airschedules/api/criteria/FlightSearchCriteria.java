package com.isa.thinair.airschedules.api.criteria;

import java.io.Serializable;
import java.util.Date;

public class FlightSearchCriteria implements Serializable{

	private String flightNumber;
	private Date estDepartureDate;
	private String status;
	private String origin;
	private String destination;
	private String csOcFlightNumber;
	
	public String getCsOcFlightNumber() {
		return csOcFlightNumber;
	}
	public void setCsOcFlightNumber(String csOcFlightNumber) {
		this.csOcFlightNumber = csOcFlightNumber;
	}
	public String getFlightNumber() {
		return flightNumber;
	}
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}
	public Date getEstDepartureDate() {
		return estDepartureDate;
	}
	public void setEstDepartureDate(Date estDepartureDate) {
		this.estDepartureDate = estDepartureDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
}
