package com.isa.thinair.airschedules.api.dto;

import java.io.Serializable;
import java.util.Date;

public class BasicFlightDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String origin;
	private String destination;
	private String flightNumber;
	private Date localDepartureDate;

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public Date getLocalDepartureDate() {
		return localDepartureDate;
	}

	public void setLocalDepartureDate(Date localDepartureDate) {
		this.localDepartureDate = localDepartureDate;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	@Override
	public String toString() {
		return "BasicFlightDTO [origin=" + origin + ", destination=" + destination + ", flightNumber=" + flightNumber
				+ ", localDepartureDate=" + localDepartureDate + "]";
	}

}
