/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:18:39
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.api.model;

import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * 
 * 
 * @author Code Generation Tool
 * @version Ver 1.0.0
 * @hibernate.class table = "T_SSM_SPLIT_SCHEDULE_LEG"
 */
public class SSMSplitFlightScheduleLeg extends Persistent {

	private static final long serialVersionUID = 5845610563061477671L;

	private Integer id;

	private Integer scheduleId;

	private int legNumber;

	private Date estDepartureTimeZulu;

	private Date estArrivalTimeZulu;

	private Date estDepartureTimeLocal;

	private Date estArrivalTimeLocal;

	private int duration;

	private int estDepartureDayOffset;

	private int estArrivalDayOffset;

	private int estDepartureDayOffsetLocal;

	private int estArrivalDayOffsetLocal;

	private String origin;

	private String destination;

	private String modelRouteId;

	private int durationError;

	/**
	 * returns the id
	 *
	 * @return Returns the id.
	 * @hibernate.id column = "SSM_SPLIT_SCHEDULE_LEG_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_SSM_SPLIT_SCHEDULE_LEG"
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * sets the id
	 * 
	 * @param fslId
	 *            The id to set.
	 */
	public void setId(Integer id) {
		this.id = id;
	}


	/**
	 * returns the scheduleId
	 * 
	 * @return scheduleId
	 * @hibernate.property column = "SSM_SPLIT_SCHEDULE_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_SSM_SPLIT_SCHEDULE_LEG"
	 */
	public Integer getScheduleId() {
		return scheduleId;
	}

	/**
	 * sets the scheduleId
	 * 
	 * @param scheduleId
	 */
	public void setScheduleId(Integer scheduleId) {
		this.scheduleId = scheduleId;
	}

	/**
	 * @return Returns the durationError.
	 */
	public int getDurationError() {
		return durationError;
	}

	/**
	 * @param durationError
	 *            The durationError to set.
	 */
	public void setDurationError(int durationError) {
		this.durationError = durationError;
	}

	/**
	 * returns the legNumber
	 * 
	 * @return Returns the legNumber.
	 * @hibernate.property column = "LEG_NUMBER"
	 */
	public int getLegNumber() {
		return legNumber;
	}

	/**
	 * sets the legNumber
	 * 
	 * @param legNumber
	 *            The legNumber to set.
	 */
	public void setLegNumber(int legNumber) {
		this.legNumber = legNumber;
	}

	/**
	 * returns the duration
	 * 
	 * @return Returns the duration.
	 * @hibernate.property column = "DURATION"
	 */
	public int getDuration() {
		return duration;
	}

	/**
	 * sets the duration
	 * 
	 * @param duration
	 *            The duration to set.
	 */
	public void setDuration(int duration) {
		this.duration = duration;
	}

	/**
	 * returns the destination
	 * 
	 * @return destination
	 * @hibernate.property column = "DESTINATION"
	 */
	public String getDestination() {
		return destination;
	}

	/**
	 * sets the destination
	 * 
	 * @param destination
	 */
	public void setDestination(String destination) {
		this.destination = destination;
	}

	/**
	 * returns the origin
	 * 
	 * @return origin
	 * @hibernate.property column = "ORIGIN"
	 */
	public String getOrigin() {
		return origin;
	}

	/**
	 * sets the origin
	 * 
	 * @param origin
	 */
	public void setOrigin(String origin) {
		this.origin = origin;
	}

	/**
	 * @return Returns the modelRouteId.
	 * @hibernate.property column = "ROUTE_ID"
	 */
	public String getModelRouteId() {
		return modelRouteId;
	}

	/**
	 * @param modelRouteId
	 *            The modelRouteId to set.
	 */
	public void setModelRouteId(String modelRouteId) {
		this.modelRouteId = modelRouteId;
	}

	/**
	 * @return Returns the estArrivalDayOffset.
	 * @hibernate.property column = "EST_ARRIVAL_DAY_OFFSET"
	 */
	public int getEstArrivalDayOffset() {
		return estArrivalDayOffset;
	}

	/**
	 * @param estArrivalDayOffset
	 *            The estArrivalDayOffset to set.
	 */
	public void setEstArrivalDayOffset(int estArrivalDayOffset) {
		this.estArrivalDayOffset = estArrivalDayOffset;
	}

	/**
	 * @return Returns the estArrivalTimeLocal.
	 * @hibernate.property column = "EST_TIME_ARRIVAL_LOCAL"
	 */
	public Date getEstArrivalTimeLocal() {
		return estArrivalTimeLocal;
	}

	/**
	 * @param estArrivalTimeLocal
	 *            The estArrivalTimeLocal to set.
	 */
	public void setEstArrivalTimeLocal(Date estArrivalTimeLocal) {
		this.estArrivalTimeLocal = estArrivalTimeLocal;
	}

	/**
	 * @return Returns the estArrivalTimeZulu.
	 * @hibernate.property column = "EST_TIME_ARRIVAL_ZULU"
	 */
	public Date getEstArrivalTimeZulu() {
		return estArrivalTimeZulu;
	}

	/**
	 * @param estArrivalTimeZulu
	 *            The estArrivalTimeZulu to set.
	 */
	public void setEstArrivalTimeZulu(Date estArrivalTimeZulu) {
		this.estArrivalTimeZulu = estArrivalTimeZulu;
	}

	/**
	 * @return Returns the estDepartureDayOffset.
	 * @hibernate.property column = "EST_DEPARTURE_DAY_OFFSET"
	 */
	public int getEstDepartureDayOffset() {
		return estDepartureDayOffset;
	}

	/**
	 * @param estDepartureDayOffset
	 *            The estDepartureDayOffset to set.
	 */
	public void setEstDepartureDayOffset(int estDepartureDayOffset) {
		this.estDepartureDayOffset = estDepartureDayOffset;
	}

	/**
	 * @return Returns the estDepartureTimeLocal.
	 * @hibernate.property column = "EST_TIME_DEPARTURE_LOCAL"
	 */
	public Date getEstDepartureTimeLocal() {
		return estDepartureTimeLocal;
	}

	/**
	 * @param estDepartureTimeLocal
	 *            The estDepartureTimeLocal to set.
	 */
	public void setEstDepartureTimeLocal(Date estDepartureTimeLocal) {
		this.estDepartureTimeLocal = estDepartureTimeLocal;
	}

	/**
	 * @return Returns the estDepartureTimeZulu.
	 * @hibernate.property column = "EST_TIME_DEPARTURE_ZULU"
	 */
	public Date getEstDepartureTimeZulu() {
		return estDepartureTimeZulu;
	}

	/**
	 * @param estDepartureTimeZulu
	 *            The estDepartureTimeZulu to set.
	 */
	public void setEstDepartureTimeZulu(Date estDepartureTimeZulu) {
		this.estDepartureTimeZulu = estDepartureTimeZulu;
	}

	/**
	 * @return Returns the estArrivalDayOffsetLocal.
	 * @hibernate.property column = "EST_ARRIVAL_DAY_OFFSET_LOCAL"
	 */
	public int getEstArrivalDayOffsetLocal() {
		return estArrivalDayOffsetLocal;
	}

	/**
	 * @param estArrivalDayOffsetLocal
	 *            The estArrivalDayOffsetLocal to set.
	 */
	public void setEstArrivalDayOffsetLocal(int estArrivalDayOffsetLocal) {
		this.estArrivalDayOffsetLocal = estArrivalDayOffsetLocal;
	}

	/**
	 * @return Returns the estDepartureDayOffsetLocal.
	 * @hibernate.property column = "EST_DEPARTURE_DAY_OFFSET_LOCAL"
	 */
	public int getEstDepartureDayOffsetLocal() {
		return estDepartureDayOffsetLocal;
	}

	/**
	 * @param estDepartureDayOffsetLocal
	 *            The estDepartureDayOffsetLocal to set.
	 */
	public void setEstDepartureDayOffsetLocal(int estDepartureDayOffsetLocal) {
		this.estDepartureDayOffsetLocal = estDepartureDayOffsetLocal;
	}
}
