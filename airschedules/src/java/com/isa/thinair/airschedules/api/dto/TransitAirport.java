package com.isa.thinair.airschedules.api.dto;

import java.io.Serializable;

import org.apache.commons.lang.builder.HashCodeBuilder;

public class TransitAirport implements Serializable {

	/**
	 * Please do not confuse with Airport model class. This is used for availability check where we need to skip the
	 * availability search if this airport is not a origin. Eg. We had a requirement for mahan, where passenger can fly
	 * from DUS/IKA then go to THR airport by a taxi or by some other medium and then fly THR/MHD. In this case IKA is
	 * not a origin
	 */
	private static final long serialVersionUID = 2616157938653324964L;

	private final String airportCode;

	private final boolean isOrigin;
	
	private final String transitCityCode;

	public TransitAirport(String airportCode, boolean isOrigin, String transitCityCode) {
		this.airportCode = airportCode;
		this.isOrigin = isOrigin;
		this.transitCityCode = transitCityCode;
	}

	public String getAirportCode() {
		return airportCode;
	}

	public boolean isOrigin() {
		return isOrigin;
	}

	public String getTransitCityCode() {
		return transitCityCode;
	}
	
	@Override
	public boolean equals(Object object) {
		if (object instanceof TransitAirport) {
			if (((TransitAirport) object).getAirportCode().equals(this.getAirportCode())) {
				return true;
			}
			return false;
		} else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(this.getAirportCode()).toHashCode();
	}


}
