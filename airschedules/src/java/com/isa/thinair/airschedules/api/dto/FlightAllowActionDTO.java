/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.api.dto;

import java.io.Serializable;

/**
 * dto to hold allow actions for ammend flight
 * 
 * @author Lasantha Pambagoda
 * 
 */
public class FlightAllowActionDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3099692084706764701L;

	private boolean allowConflicts = false;

	private boolean allowAnyDuration = false;

	private boolean allowSegmentV2I = false;

	private boolean allowModelForAnyCC = false;

	private boolean allowOverlapForAnyCC = false;

	/**
	 * @return Returns the allowAnyDuration.
	 */
	public boolean isAllowAnyDuration() {
		return allowAnyDuration;
	}

	/**
	 * @param allowAnyDuration
	 *            The allowAnyDuration to set.
	 */
	public void setAllowAnyDuration(boolean allowAnyDuration) {
		this.allowAnyDuration = allowAnyDuration;
	}

	/**
	 * @return Returns the allowConflicts.
	 */
	public boolean isAllowConflicts() {
		return allowConflicts;
	}

	/**
	 * @param allowConflicts
	 *            The allowConflicts to set.
	 */
	public void setAllowConflicts(boolean allowConflicts) {
		this.allowConflicts = allowConflicts;
	}

	/**
	 * @return Returns the allowModelForAnyCC.
	 */
	public boolean isAllowModelForAnyCC() {
		return allowModelForAnyCC;
	}

	/**
	 * @param allowModelForAnyCC
	 *            The allowModelForAnyCC to set.
	 */
	public void setAllowModelForAnyCC(boolean allowModelForAnyCC) {
		this.allowModelForAnyCC = allowModelForAnyCC;
	}

	/**
	 * @return Returns the allowOverlapForAnyCC.
	 */
	public boolean isAllowOverlapForAnyCC() {
		return allowOverlapForAnyCC;
	}

	/**
	 * @param allowOverlapForAnyCC
	 *            The allowOverlapForAnyCC to set.
	 */
	public void setAllowOverlapForAnyCC(boolean allowOverlapForAnyCC) {
		this.allowOverlapForAnyCC = allowOverlapForAnyCC;
	}

	/**
	 * @return Returns the allowSegmentV2I.
	 */
	public boolean isAllowSegmentV2I() {
		return allowSegmentV2I;
	}

	/**
	 * @param allowSegmentV2I
	 *            The allowSegmentV2I to set.
	 */
	public void setAllowSegmentV2I(boolean allowSegmentV2I) {
		this.allowSegmentV2I = allowSegmentV2I;
	}
}
