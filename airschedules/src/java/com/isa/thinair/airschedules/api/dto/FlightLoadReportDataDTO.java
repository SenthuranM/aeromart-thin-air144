package com.isa.thinair.airschedules.api.dto;

import java.io.Serializable;

public class FlightLoadReportDataDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4045318893700944074L;

	private String originFlightnumber;

	private String originSegment;

	private String originMale;

	private String originFemale;

	private String originOther;

	private String originChild;

	private String originInfant;

	private String originTotal;

	private String connectingSegment;

	private String connMale;

	private String connFemale;

	private String connOther;

	private String connChild;

	private String connInfant;

	private String connTotal;

	private String originDate;

	private String connectingFlightNumber;

	public String getOriginFlightnumber() {
		return originFlightnumber;
	}

	public void setOriginFlightnumber(String originFlightnumber) {
		this.originFlightnumber = originFlightnumber;
	}

	public String getOriginSegment() {
		return originSegment;
	}

	public void setOriginSegment(String originSegment) {
		this.originSegment = originSegment;
	}

	public String getOriginMale() {
		return originMale;
	}

	public void setOriginMale(String originMale) {
		this.originMale = originMale;
	}

	public String getOriginFemale() {
		return originFemale;
	}

	public void setOriginFemale(String originFemale) {
		this.originFemale = originFemale;
	}

	public String getOriginOther() {
		return originOther;
	}

	public void setOriginOther(String originOther) {
		this.originOther = originOther;
	}

	public String getOriginChild() {
		return originChild;
	}

	public void setOriginChild(String originChild) {
		this.originChild = originChild;
	}

	public String getOriginInfant() {
		return originInfant;
	}

	public void setOriginInfant(String originInfant) {
		this.originInfant = originInfant;
	}

	public String getOriginTotal() {
		return originTotal;
	}

	public void setOriginTotal(String originTotal) {
		this.originTotal = originTotal;
	}

	public String getConnectingSegment() {
		return connectingSegment;
	}

	public void setConnectingSegment(String connectingSegment) {
		this.connectingSegment = connectingSegment;
	}

	public String getConnMale() {
		return connMale;
	}

	public void setConnMale(String connMale) {
		this.connMale = connMale;
	}

	public String getConnFemale() {
		return connFemale;
	}

	public void setConnFemale(String connFemale) {
		this.connFemale = connFemale;
	}

	public String getConnOther() {
		return connOther;
	}

	public void setConnOther(String connOther) {
		this.connOther = connOther;
	}

	public String getConnChild() {
		return connChild;
	}

	public void setConnChild(String connChild) {
		this.connChild = connChild;
	}

	public String getConnInfant() {
		return connInfant;
	}

	public void setConnInfant(String connInfant) {
		this.connInfant = connInfant;
	}

	public String getConnTotal() {
		return connTotal;
	}

	public void setConnTotal(String connTotal) {
		this.connTotal = connTotal;
	}

	public String getOriginDate() {
		return originDate;
	}

	public void setOriginDate(String originDate) {
		this.originDate = originDate;
	}

	public String getConnectingFlightNumber() {
		return connectingFlightNumber;
	}

	public void setConnectingFlightNumber(String connectingFlightNumber) {
		this.connectingFlightNumber = connectingFlightNumber;
	}

}
