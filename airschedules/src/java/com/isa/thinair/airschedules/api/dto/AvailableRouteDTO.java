package com.isa.thinair.airschedules.api.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Capture available route for reaching from origin to destination.
 * 
 * @author Mohamed Nasly
 */
public class AvailableRouteDTO {

	private String originAirportCode;

	private String destinationAirportCode;

	private List transitAirports;

	/**
	 * @return the destinationAirportCode
	 */
	public String getDestinationAirportCode() {
		return destinationAirportCode;
	}

	/**
	 * @param destinationAirportCode
	 *            the destinationAirportCode to set
	 */
	public void setDestinationAirportCode(String destinationAirportCode) {
		this.destinationAirportCode = destinationAirportCode;
	}

	/**
	 * @return the originAirportCode
	 */
	public String getOriginAirportCode() {
		return originAirportCode;
	}

	/**
	 * @param originAirportCode
	 *            the originAirportCode to set
	 */
	public void setOriginAirportCode(String originAirportCode) {
		this.originAirportCode = originAirportCode;
	}

	/**
	 * @return the transitAirports
	 */
	public List getTransitAirports() {
		return transitAirports;
	}

	/**
	 * @param transitAirports
	 *            the transitAirports to set
	 */
	public void setTransitAirports(List transitAirports) {
		this.transitAirports = transitAirports;
	}

	public void addTransitAirport(String transitAirportCode) {
		if (this.transitAirports == null) {
			this.transitAirports = new ArrayList();
		}
		this.transitAirports.add(transitAirportCode);
	}

	public boolean isDirectRoute() {
		return (this.transitAirports == null || this.transitAirports.size() == 0);
	}
}
