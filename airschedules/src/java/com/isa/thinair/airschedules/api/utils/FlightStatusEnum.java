/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.api.utils;

import java.io.Serializable;

/**
 * @author Lasantha Pambagoda
 */
public class FlightStatusEnum implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3293946098520668639L;
	private String code;
	private String description;

	private FlightStatusEnum(String code, String description) {
		this.code = code;
		this.description = description;
	}

	public String toString() {
		return this.code;
	}

	public boolean equals(ScheduleBuildStatusEnum status) {
		return status.toString().equals(this.code);
	}

	public String getDescription() {
		return description;
	}

	public String getCode() {
		return code;
	}

	public static FlightStatusEnum getBuildStatus(String code) {

		FlightStatusEnum statusEnum = null;
		code = (code == null) ? "" : code;

		if (code.equals(CREATED.toString())) {
			statusEnum = CREATED;
		} else if (code.equals(ACTIVE.toString())) {
			statusEnum = ACTIVE;
		} else if (code.equals(CANCELLED.toString())) {
			statusEnum = CANCELLED;
		} else if (code.equals(CLOSED.toString())) {
			statusEnum = CLOSED;
		} else if (code.equals(CLOSED_FOR_RESERVATION.toString())) {
			statusEnum = CLOSED_FOR_RESERVATION;
		}

		return statusEnum;
	}

	public static final FlightStatusEnum CREATED = new FlightStatusEnum("CRE", "Created");
	public static final FlightStatusEnum ACTIVE = new FlightStatusEnum("ACT", "Active");
	public static final FlightStatusEnum CANCELLED = new FlightStatusEnum("CNX", "Cancelled");
	public static final FlightStatusEnum CLOSED = new FlightStatusEnum("CLS", "Closed");
	public static final FlightStatusEnum CLOSED_FOR_RESERVATION = new FlightStatusEnum("CLR", "Closed for Reservation");

}
