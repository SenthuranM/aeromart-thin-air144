package com.isa.thinair.airschedules.api.model;

import java.io.Serializable;

/**
 * 
 * @author manoj
 * @hibernate.class table = "T_CS_FLIGHT_SCHEDULE"
 */
public class CodeShareMCFlightSchedule implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Integer csScheduleId;
	
	private Integer scheduleId;

	private String csMCFlightNumber;
	
	private String csMCCarrierCode;

	/**
	 * @return Returns the csScheduleId.
	 * 
	 * @hibernate.id column = "CS_FLIGHT_SCHEDULE_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_CS_FLIGHT_SCHEDULE"
	 */
	public Integer getCsScheduleId() {
		return csScheduleId;
	}

	/**
	 * returns the scheduleId
	 * 
	 * @return Returns the scheduleId.
	 * @hibernate.property column = "SCHEDULE_ID"
	 */
	public Integer getScheduleId() {
		return scheduleId;
	}

	/**
	 * returns the csMCFlightNumber
	 * 
	 * @return csMCFlightNumber
	 * @hibernate.property column = "CS_FLIGHT_NUMBER"
	 */
	public String getCsMCFlightNumber() {
		return csMCFlightNumber;
	}

	/**
	 * returns the csMCCarrierCode
	 * 
	 * @return csMCCarrierCode
	 * @hibernate.property column = "CS_CARRIER_CODE"
	 */
	public String getCsMCCarrierCode() {
		return csMCCarrierCode;
	}

	public void setCsScheduleId(Integer csScheduleId) {
		this.csScheduleId = csScheduleId;
	}

	public void setScheduleId(Integer scheduleId) {
		this.scheduleId = scheduleId;
	}

	public void setCsMCFlightNumber(String csMCFlightNumber) {
		if (csMCFlightNumber != null) {
			this.csMCFlightNumber = csMCFlightNumber.toUpperCase();
		}
	}

	public void setCsMCCarrierCode(String csMCCarrierCode) {
		this.csMCCarrierCode = csMCCarrierCode;
	}

}
