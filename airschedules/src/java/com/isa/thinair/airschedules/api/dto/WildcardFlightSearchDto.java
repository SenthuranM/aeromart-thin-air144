package com.isa.thinair.airschedules.api.dto;



import java.io.Serializable;
import java.util.Date;

public class WildcardFlightSearchDto implements Serializable {

	private String flightNumber;
	private String codeShareFlightNumber;
	private String codeShareCarrierCode;
	private Date departureDate;
	private boolean departureDateExact;
	private Date arrivalDate;
	private boolean arrivalDateExact;
	private String departureAirport;
	private String arrivalAirport;
	private boolean nonCNXFlight;
	private String agentCode;



	public WildcardFlightSearchDto() {
		departureDateExact = true;
		arrivalDateExact = true;
	}

	public WildcardFlightSearchDto(com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO flightSegmentDTO) {
		this();
		flightNumber = flightSegmentDTO.getFlightNumber();
		departureDate = flightSegmentDTO.getDepartureDateTime();
		arrivalDate = flightSegmentDTO.getArrivalDateTime();
		departureAirport = flightSegmentDTO.getFromAirport();
		arrivalAirport = flightSegmentDTO.getToAirport();
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getCodeShareFlightNumber() {
		return codeShareFlightNumber;
	}

	public void setCodeShareFlightNumber(String codeShareFlightNumber) {
		this.codeShareFlightNumber = codeShareFlightNumber;
	}

	public String getCodeShareCarrierCode() {
		return codeShareCarrierCode;
	}

	public void setCodeShareCarrierCode(String codeShareCarrierCode) {
		this.codeShareCarrierCode = codeShareCarrierCode;
	}

	public String getDepartureAirport() {
		return departureAirport;
	}

	public void setDepartureAirport(String departureAirport) {
		this.departureAirport = departureAirport;
	}

	public boolean isDepartureDateExact() {
		return departureDateExact;
	}

	public void setDepartureDateExact(boolean departureDateExact) {
		this.departureDateExact = departureDateExact;
	}

	public String getArrivalAirport() {
		return arrivalAirport;
	}

	public void setArrivalAirport(String arrivalAirport) {
		this.arrivalAirport = arrivalAirport;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public Date getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public boolean isArrivalDateExact() {
		return arrivalDateExact;
	}

	public void setArrivalDateExact(boolean arrivalDateExact) {
		this.arrivalDateExact = arrivalDateExact;
	}

	public boolean isNonCNXFlight() {
		return nonCNXFlight;
	}

	public void setNonCNXFlight(boolean nonCNXFlight) {
		this.nonCNXFlight = nonCNXFlight;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}
	
	

}
