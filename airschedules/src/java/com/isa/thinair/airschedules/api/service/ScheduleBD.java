/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.api.service;

import com.isa.thinair.airmaster.api.model.AirportDSTHistory;
import com.isa.thinair.airschedules.api.dto.FlightAlertDTO;
import com.isa.thinair.airschedules.api.dto.FlightAllowActionDTO;
import com.isa.thinair.airschedules.api.dto.FlightScheduleInfoDTO;
import com.isa.thinair.airschedules.api.dto.ScheduleSearchDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.OperationType;
import com.isa.thinair.airschedules.api.model.ScheduleBuildStatus;
import com.isa.thinair.airschedules.api.model.ScheduleStatus;
import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.dto.ScheduleRevisionTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.ServiceResponce;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Interface to define methods required for the Schedule
 * 
 * @author Lasantha Pambagoda
 */
public interface ScheduleBD {

	public static final String SERVICE_NAME = "ScheduleService";

	public FlightSchedule getFlightSchedule(int schduleId) throws ModuleException;

	public Page getAllFlightSchedules(int startIndex, int pageSize) throws ModuleException;

	public ServiceResponce createSchedule(FlightSchedule flightSchedule, boolean isReviseFrequency,
			boolean isEnableAttachDefaultAnciTemplate) throws ModuleException;

	public ServiceResponce updateSchedule(FlightSchedule flightSchedule, boolean forceCancel, FlightAlertDTO flightAlertDTO,
			boolean overwrite, boolean downgradeToAnyModel, boolean isScheduleMessageProcessing,
			boolean isEnableAttachDefaultAnciTemplate) throws ModuleException;

	public ServiceResponce updateScheduleLegs(FlightSchedule flightSchedule, boolean forceCancel, FlightAlertDTO flightAlertDTO,
			boolean overwrite, int firstLegDayDiff) throws ModuleException;

	public Page searchFlightSchedules(List<ModuleCriterion> criteria, int startIndex, int pageSize) throws ModuleException;

	public Page searchFlightSchedulesForDisplay(List<ModuleCriterion> criteria, int startIndex, int pageSize,
			boolean isDatesInLocal) throws ModuleException;

	public Page[] searchAndReassembleSchedules(List<ModuleCriterion> criteria, int startIndex, int pageSize,
			boolean isDatesInLocal, boolean isAdhocFlightDataEnabled) throws ModuleException;

	public ServiceResponce cancelSchedule(int scheduleId, boolean forceCancel, FlightAlertDTO flightAlertDTO, boolean overwrite, boolean isScheduleMessageProcess)
			throws ModuleException;

	public int getNoOfFlightsForSchedule(int scheduleId) throws ModuleException;

	public Collection<Flight> getFlights(int scheduleId) throws ModuleException;

	public ServiceResponce splitSchedule(int scheduleId, Date fromDate, Date toDate, Frequency days, boolean isDatesInLocal, boolean isReviseFrequency, boolean isScheduleMessageProcess)
			throws ModuleException;

	public ServiceResponce buildSchedule(Collection<Integer> scheduleIds, boolean isScheduleMessageProcess) throws ModuleException;

	public ServiceResponce copySchedule(int scheduleId, Date fromDate, Date toDate, boolean isDatesInLocal)
			throws ModuleException;

	public Collection<OperationType> getAllOperationTypes() throws ModuleException;

	public Collection<ScheduleBuildStatus> getAllScheduleBuildStatus() throws ModuleException;

	public Collection<ScheduleStatus> getAllScheduleStatus() throws ModuleException;

	public Collection<String> getFlightNumbers() throws ModuleException;

	public Collection<FlightSchedule> getPossibleOverlappingSchedules(FlightSchedule schdule) throws ModuleException;

	public ServiceResponce getFlightReservationsForCancel(int scheduleId, boolean forceCancel) throws ModuleException;

	public void sendFlightSchedules() throws ModuleException;

	public ServiceResponce publishScheduleToGDS(Integer scheduleId, Collection<Integer> gdsIds, Integer version)
			throws ModuleException;
	
	public void publishAllSchedulesToGDS(List<Integer> gdsIds) throws ModuleException;

	public Collection<FlightScheduleInfoDTO> getFlightSchedulesInfomation(ScheduleSearchDTO scheduleSearchDTO)
			throws ModuleException;

	public Collection<FlightScheduleInfoDTO> getFlightSchedulesToGDSPublish(Date lastPublishTime, String codeShareCarrier) throws ModuleException;

	public Collection<Flight> getFutureFlightsOfSchedule(int scheduleId, Date fromDate, Date toDate) throws ModuleException;

	public void rollForwardUserNote(Collection<Integer> getfightIDList, String userNote, String schedId) throws ModuleException;

	public ServiceResponce splitBulkSchedule(String modelNumber,String newModelNumeber) throws ModuleException;

	public void scheduleJob(Map<String,String> jobdataMap) throws ModuleException;

	public ServiceResponce publishSchedulesToGDS(int gdsID, Date scheduleStartDate, Date scheduleEndDate, boolean isSelectedSchedules,
			String emailAddress) throws ModuleException;
	
	public Collection<FlightSchedule> getDSTApplicableFutureSchedules(AirportDSTHistory airportDST) throws ModuleException;

	public ServiceResponce publishSchedulesToExternal(Date scheduleStartDate, Date scheduleEndDate, boolean isSelectedSchedules,
			String emailAddress, String sitaAddress) throws ModuleException;

	public void scheduleSSMRecap(Map<String, String> jobDataMap) throws ModuleException;
	
	public ServiceResponce splitSubScheduleForSSM(FlightSchedule schedule) throws ModuleException;
	
	public void updateDelayCancelFlightSchedule(Collection<Integer> scheduleIds, boolean status, Date holdTill, String remarks)
			throws ModuleException;	
	
	public void resumeSsmAsmWaitingCancelActions() throws ModuleException;
	
	public Collection<FlightSchedule> getPossibleFlightDesignatorChange(Date startDate, Date stopDate, String segmentCode,
			boolean isLocalTime) throws ModuleException;
	
	public ServiceResponce cancelScheduleWithRequiresNew(int scheduleId, boolean forceCancel, FlightAlertDTO flightAlertDTO,
			boolean overwrite, boolean isScheduleMessageProcess) throws ModuleException;
	
	public void removeGdsAutopublishRoutes(String gdsId) throws ModuleException;

	public ServiceResponce updateMissingFlightsWithinPeriod(ScheduleRevisionTO scheduleRevisionTO, FlightAlertDTO flightAlertDTO,
			FlightAllowActionDTO flightAllowActionDTO) throws ModuleException;
}
