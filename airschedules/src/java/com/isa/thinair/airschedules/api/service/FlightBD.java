/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.api.service;

import com.isa.thinair.airinventory.api.dto.InvRollForwardFlightsSearchCriteria;
import com.isa.thinair.airinventory.api.dto.baggage.FlightBaggageDTO;
import com.isa.thinair.airinventory.api.dto.meal.FlightMealDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableConnectedFlight;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.RollForwardFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatmap.FlightSeatsDTO;
import com.isa.thinair.airmaster.api.model.Airport;
import com.isa.thinair.airmaster.api.model.AirportDST;
import com.isa.thinair.airproxy.api.model.reservation.availability.CalendarSearchRQ;
import com.isa.thinair.airschedules.api.criteria.FlightFlightSegmentSearchCriteria;
import com.isa.thinair.airschedules.api.criteria.FlightSearchCriteria;
import com.isa.thinair.airschedules.api.dto.BasicFlightDTO;
import com.isa.thinair.airschedules.api.dto.DisplayFlightDTO;
import com.isa.thinair.airschedules.api.dto.FlightAlertDTO;
import com.isa.thinair.airschedules.api.dto.FlightAllowActionDTO;
import com.isa.thinair.airschedules.api.dto.FlightBasicInfoDTO;
import com.isa.thinair.airschedules.api.dto.FlightMealNotifyStatusDTO;
import com.isa.thinair.airschedules.api.dto.FlightSummaryDTO;
import com.isa.thinair.airschedules.api.dto.PastFlightInfoDTO;
import com.isa.thinair.airschedules.api.dto.RouteInfoTO;
import com.isa.thinair.airschedules.api.dto.ScheduleSearchDTO;
import com.isa.thinair.airschedules.api.dto.TransferRollForwardFlightsSearchCriteria;
import com.isa.thinair.airschedules.api.dto.TransitAirport;
import com.isa.thinair.airschedules.api.dto.WildcardFlightSearchDto;
import com.isa.thinair.airschedules.api.dto.WildcardFlightSearchRespDto;
import com.isa.thinair.airschedules.api.dto.rm.RMFlightSummaryDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.FlightSegNotificationEvent;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.utils.AnciDefaultTemplStatusEnum;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.ServiceResponce;

import java.sql.Blob;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Interface to define all the methods required to access the Flight details
 * 
 * @author Lasantha Pambagoda
 */
public interface FlightBD {

	public static final String SERVICE_NAME = "FlightService";

	public ServiceResponce recalculateLocalTime(Airport airport, Date gmtDstEffectiveFrom, Date gmtDstEffectiveTo,
			Collection<AirportDST> dstTos, FlightAlertDTO alertDTO) throws ModuleException;

	public Flight getFlight(int flightId) throws ModuleException;

	public List<Flight> getLightWeightFlightDetails(Collection<Integer> flightIds) throws ModuleException;

	public List<Flight> getFlightDetails(String flightNumber) throws ModuleException;

	public Flight getSpecificFlightDetail(FlightSearchCriteria searchCriteria) throws ModuleException;

	public FlightSegement getFlightSegmentForLocalDate(String flightNumber, Date departureDateLocal) throws ModuleException;

	public String getFlightDates(Collection<Integer> flightIds, int pageSize, SimpleDateFormat format) throws ModuleException;

	public Page searchFlightsForDisplay(List<ModuleCriterion> criteria, int startIndex, int pageSize, boolean isDatesInLoacal,
			FlightFlightSegmentSearchCriteria flightCriteria, boolean isFlightManifest, boolean isReporting, List<String> agentApplicableOnds)
			throws ModuleException;

	public ServiceResponce createFlight(FlightAllowActionDTO allowAction, Flight flight, boolean isEnableAttachDefaultAnciTemplate) throws ModuleException;

	public ServiceResponce cancelFlight(int flightId, boolean forceCancel, FlightAlertDTO flightAlertDTO) throws ModuleException;

	public ServiceResponce cancelFlights(Collection<Integer> flightIds, boolean forceCancel) throws ModuleException;

	public ServiceResponce updateFlight(FlightAllowActionDTO allowAction, Flight flight, FlightAlertDTO flightAlertDTO,
			boolean downgradeToAnyModel, boolean activatePastFlights, boolean isScheduleMessageProcessing,
			boolean isEnableAttachDefaultAnciTemplate) throws ModuleException;

	// public void reprotectSeats (Collection flightIds, Collection seats,
	// boolean reprotectAlert) throws ModuleException;

	public ServiceResponce copyFlight(FlightAllowActionDTO allowAction, int flightId, Date newDate, boolean isTimeInLocal)
			throws ModuleException;

	public Collection<FlightSegement> getSegments(int flightId) throws ModuleException;

	public FlightSegement getFlightSegment(int flightSegmentIds) throws ModuleException;

	public void updateFlightSegment(FlightSegement flightSegment) throws ModuleException;

	public boolean closeFlightForReservation(int flightId, String segmentCode) throws ModuleException;

	public Collection<Flight> getPossibleOverlappingFlights(Flight flight) throws ModuleException;

	public void changeFlightStatus(Collection<Integer> flights, FlightStatusEnum status) throws ModuleException;

	public void manageFlightStatus(Collection<Integer> flights, FlightStatusEnum status) throws ModuleException;

	public void changeFlightStatus(Integer scheduleID, FlightStatusEnum status) throws ModuleException;

	// public Collection getFlightSummary(List flightSummaryCriteria ) throws
	// ModuleException;

	public Collection<FlightSummaryDTO> getFlightSummary(InvRollForwardFlightsSearchCriteria criteria) throws ModuleException;

	public Collection<FlightSummaryDTO> getLikeFlightSummary(InvRollForwardFlightsSearchCriteria criteria) throws ModuleException;

	public List<Integer> getSeatTemplateAttachedFlights(Collection<Integer> flightIdList);

	public List<Flight> getLikeFlights(TransferRollForwardFlightsSearchCriteria criteria) throws ModuleException;

	public ServiceResponce searchAvailableFlights(AvailableFlightSearchDTO avifltSearchDTO) throws ModuleException;

	/**
	 * This will return the flights available for OHD rollforward facility
	 * 
	 * @param rollForwardSearch
	 * @return
	 * @throws ModuleException
	 */
	public ServiceResponce searchAvailableFlights(RollForwardFlightSearchDTO rollForwardSearch) throws ModuleException;

	public Collection<Integer> getFlightIDsForModel(String modelNumber, Date departureDateFrom) throws ModuleException;

	public Collection<Flight> getFlightsForModel(String modelNumber, Date departureDateFrom) throws ModuleException;

	public Collection<Flight> getFutureNonCNXFlightsForModel(String modelNumber, Date departureDateFrom) throws ModuleException;
	
	public ServiceResponce getFilledSelectedFlight(AvailableFlightSearchDTO avifltSearchDTO, boolean isTransactional) throws ModuleException;

	public Frequency getScheduleFrequencyOfFlight(int flightId) throws ModuleException;

	public List<AvailableFlightSegment> getAvailableFlightSegments(Collection<Integer> logicalCabinClassSelection,
			String classOfService, boolean isInboundFlight, int ondSequence, boolean ignoreFlightStatus, boolean isUnTouchedOnd)
			throws ModuleException;

	public boolean isModelUsedByFlight(String modelNumber) throws ModuleException;

	public HashMap<String, String> getUsedAirports(Collection<String> airports) throws ModuleException;

	public List<String> getAirportSequenceListForFlight(int flightId) throws ModuleException;

	public ServiceResponce getFlightReservationsForCancel(int flightId, boolean forceCancel) throws ModuleException;

	public Collection<FlightSegmentDTO> getFlightSegments(Collection<Integer> flightSegmentIds) throws ModuleException;

	public Timestamp getFlightForDepartureStation(int flightId, String departureStation) throws ModuleException;

	public List<String> getPossibleDestinations(String flightNumber, Date departureDate, String airportCode)
			throws ModuleException;

	public Collection<FlightSegement> getFlightSegmentsForLocalDate(String originAirportCode, String destAirportCode,
			String flightNo, Date departureDateLocal, boolean dateOnly) throws ModuleException;

	public Collection<String> getFlightNumbers() throws ModuleException;

	/**
	 * Fills flight segment information identifying the flight segment based on segmentcode, flightNumber and
	 * departureDateTime
	 * 
	 * @param flightSegmentDTOs
	 * @return
	 * @throws ModuleException
	 */
	public Collection<FlightSegmentDTO> getFilledFlightSegmentDTOs(Collection<FlightSegmentDTO> flightSegmentDTOs)
			throws ModuleException;
	
	public Map<String, String> getFlightLegs(int flightID) throws ModuleException;
	
	public Integer getFlightID(String flightNumber, Date dateOfflight, String departureStation) throws ModuleException;

	public Integer getFlightID(String segCode, String fltNum, Date depDateZulu) throws ModuleException;

	public RMFlightSummaryDTO getFlightSummary(String fltNum, Date depDateZulu) throws ModuleException;

	public Collection<Integer> getFlightSegIdsforSchedule(int scheduleId) throws ModuleException;

	public Collection<FlightSeatsDTO> getFlightSegIdsforFlight(Collection<Integer> flightIds) throws ModuleException;

	// Added by Haider 02Sep08
	public HashMap<Integer, ArrayList<Integer>> getScheduleGDSIdsForFlight(List<Integer> flightIds) throws ModuleException;

	public ServiceResponce publishFlightToGDS(Integer scheduleId, Collection<Integer> gdsIds, Integer version)
			throws ModuleException;

	public void publishAllAdhocFlightsToGDS(List<Integer> gdsIds) throws ModuleException;

	public Collection<FlightMealDTO> getFlightSegIdsforFlightforMeal(Collection<Integer> flightIds) throws ModuleException;

	public void updateFlightMealNotifyStatus(FlightMealNotifyStatusDTO flightMealNotifyStatusDTO) throws ModuleException;

	public void updateFlightSegmentCheckinStatus(int flightId, String segmentCode, String status) throws ModuleException;

	public FlightSegement getFlightSegmentForAirport(int flightId, String segmentCode) throws ModuleException;

	public int getFlightStandbyCount(int flightId, String airport) throws ModuleException;

	public int getFlightCheckinCount(int flightId, String airport) throws ModuleException;

	public Integer[] getNumberOfFlightsInSchedule(Integer scheduleId) throws ModuleException;

	public HashMap<Integer, Integer> getOpenFlightCount(List<Integer> scheduleIds) throws ModuleException;

	public HashMap<Integer, Integer> getFutureFlights(List<Integer> scheduleIds) throws ModuleException;

	public Collection<DisplayFlightDTO> getFlightsToGDSPublish(Date lastPublishTime, String codeShareCarrier) throws ModuleException;

	public Collection<DisplayFlightDTO> getFlightsToPublish(Date lastPublishTime) throws ModuleException;

	public Integer updateFlightSegmentNotifyStatus(Integer flightSegmentId, Integer flightMsgEventId, String newStatus,
			String failureReason, Integer notificationCount, String notificationType);

	public FlightSegNotificationEvent updateFlightSegmentNotifyStatus(Integer flightSegmentId, Integer flightMsgEventId,
			String newStatus, String failureReason, Integer notificationCount, String notificationType, String insFirstResponse,
			Blob insSecResponse) throws ModuleException;

	public Integer getFlightSegNotificationAttempts(Integer flightMsgEventId) throws ModuleException;

	public List<RouteInfoTO> getAvailableRoutes(String fromAirport, String toAirport, int availabilityRestrictionLevel)
			throws ModuleException;

    public List<RouteInfoTO> getAvailableRoutes(List<String> fromAirport) throws ModuleException;

	public List<FlightSegmentDTO> getMatchingFlightSegmentsForOndGeneration(String origin, String destination, Date startDate,
			Date endDate) throws ModuleException;

	public void updateFlightSegmentTerminals(String airport, Integer terminalId, boolean isNull, boolean isOnlySet)
			throws ModuleException;

	public boolean isTerminalUsedByFlightOrSchedule(Integer terminalId) throws ModuleException;

	public List<String> getBookingClassForFareAmount(String segmentCode, Date flightDepDate, Double adultFareAmount,
			String fareType) throws ModuleException;

	public Collection<FlightBaggageDTO> getFlightSegIdsforFlightforBaggage(Collection<Integer> flightIds) throws ModuleException;

	public Collection<FlightSegmentDTO> getFlightSegments(Collection<Integer> flightSIds, String segmentCode)
			throws ModuleException;

	public String getDefaultBusCarrierCode() throws ModuleException;

	public String getDefaultBusCarrierCode(String carrierCode) throws ModuleException;

	public Map<String, String> getBusAirCarrierCodes() throws ModuleException;

	public List<AvailableConnectedFlight> getAvailableConnectedFlightSegments(Date dateTimeStart, Date dateTimeEnd,
			String origin, String destination, List<TransitAirport> stopOverAirports, int adults, int infants, String ccCode,
			int availabilityRestriction, Map<String, Date> allowableLatesDatesZuluForInterliningMap,
			Collection<String> externalBookingClasses, Collection<Integer> flightIdList) throws ModuleException;

	public void assignDefaultMealChargesTemplate() throws ModuleException;

	public void assignDefaultSeatMapChargesTemplate() throws ModuleException;

	public void updateFltAnciAssignStatusByFltSegId(Collection<Integer> flightSegIds, String ancillaryType,
			AnciDefaultTemplStatusEnum status) throws ModuleException;

	public FlightSegement getSpecificFlightSegment(int pnrSegId) throws ModuleException;

	public Collection<FlightSegement> getFlightSegmentList(String segmentCode, Date departureDate);

	public List<FlightSegement>
			getSuggestedSegmentListForADate(String flightNumber, String departureDateLocal, String segmentCode);

	public int getFlightSegmentCount(String flightNumber, Date departureDateLocal, Date arrivalDateLocal, String segmentCode,
			String agentCode) throws ModuleException;

	public ArrayList<FlightSegmentDTO> getGoshowBCOpeningFilghtList(Date date);

	public List<Flight> getActiveFlightWithinCutOffTime(Integer flightSegId);

	public boolean areSameDaySameFlights(int flightSegID1, int flightSegID2);

	public Collection<FlightSchedule> getFlightSchdulesForModel(String modelNumber) throws ModuleException;

	public void activatePastFlights(int flightId);

	public boolean isAirModelHasSeatTemplate(String modelNumber) throws ModuleException;

	public boolean isAirModelHasChargeTemplate(String modelNumber) throws ModuleException;

	public String getOverbookFlightDetails(String flightIds) throws ModuleException;

	public List<String> getFLightsWithAirModelHasChargeTemplate(String modelNumber) throws ModuleException;

	public boolean isSeatMapAvailableForFlightSegement(Integer flightSegId) throws ModuleException;

	public Integer getFlightID(String pnrSegID) throws ModuleException;

	public WildcardFlightSearchRespDto searchFlightsWildcardBased(WildcardFlightSearchDto flightSearchDto) throws ModuleException;

	public WildcardFlightSearchRespDto searchCodeShareFlightsWildcardBased(WildcardFlightSearchDto flightSearchDto) throws ModuleException;

	public Map<Integer, Boolean> isAtLeastOneSegmentHavingCSOCFlight(Collection<Integer> colFlightSegIds) throws ModuleException;

	public Collection<Flight> getAvailableAdhocFlights(ScheduleSearchDTO scheduleSearchDTO) throws ModuleException;
	
	public Collection<FlightBasicInfoDTO> getFlightDetails(FlightBasicInfoDTO flownFlightsSearchDTO,int startIndex, int pageLength)throws ModuleException;

	public String getMCFlightNumber(Integer flightId, String marketingCarrierCode) throws ModuleException;

	public Collection<Integer> getFlightSegIdsforFlight(int flightID) throws ModuleException;
	
	public int getNumberOfNonCanceledFlights(int flightScheduleID) throws ModuleException;
	
	public void updateDelayCancelFlight(Collection<Integer> flightIds, boolean status, Date holdTill) throws ModuleException;
	
	public Collection<Integer> getPossibleFlightDesignatorChange(Date departureDate, String segmentCode, boolean isLocalTime,
			Date flightsUntil, boolean isAdHocFlightsOnly) throws ModuleException;

	public ServiceResponce searchAvailableFlights(CalendarSearchRQ searchRQ) throws ModuleException;
	
	public boolean showRollFwdButton(Integer flightId) throws ModuleException;
	
	public ServiceResponce cancelFlightWithRequiresNew(int flightId, boolean forceCancel, FlightAlertDTO flightAlertDTO)
			throws ModuleException;

	public void updateFlownReservationsDetails(PastFlightInfoDTO pastFlightInfoDTO) throws ModuleException;

	public Collection<Integer> getPaxFareSegETIds(String flightId) throws ParseException;

	public ServiceResponce prepareCopyFlight(int flightId, Date newDate, boolean isTimeInLocal) throws ModuleException;
	
	public String getCountryCodeForOrigin(Integer flightId);

	public Collection<String> getFutureFlightsNoByOriginDestination(String originAptCode, String destinationAptCode) throws ModuleException;

	public Integer getFlightId(String flightNumber, Date departureDate);
	
	public Collection<BasicFlightDTO> getBasicFlightDTOs(Collection<Integer> flightIds) throws ModuleException;
	
	public List<AvailableFlightSegment> getAvailableFlightSegments(String transitAirport, String destination, String cabinClass,
			Date lastFlightArrivalTime, int adultCount) throws ModuleException;

	public FlightSegement getAllFlightSegmentForLocalDate(String flightNumber, Date departureDateLocal) throws ModuleException;
}
