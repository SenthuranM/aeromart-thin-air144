package com.isa.thinair.airschedules.api.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.SSMSplitFlightSchedule;

public class GDSScheduleEventCollector implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3254340537557784901L;
	public static final String PERIOD_CHANGED = "PERIOD_CHANGED";
	public static final String FLIGHT_NUMBER_CHANGED = "FLIGHT_NUMBER_CHANGED";
	public static final String MODEL_CHANGED = "MODEL_CHANGED";
	public static final String LEG_TIME_CHANGE = "LEG_TIME_CHANGE";
	public static final String LEG_ADDED_DELETED = "LEG_ADDED_DELETED";
	public static final String GDS_ADDED = "GDS_ADDED";
	public static final String GDS_DELETED = "GDS_DELETED";
	public static final String REPLACE_EXISTING_INFO = "REPLACE_EXISTING_INFO";
	public static final String PERIOD_CHANGED_LOCAL_TIME = "PERIOD_CHANGED_LOCAL_TIME";
	public static final String AIRPORT_DST_CHANGED = "AIRPORT_DST_CHANGED";
	public static final String SPLIT_SCHEDULE = "SPLIT_SCHEDULE";
	public static final String EXTERNAL_SSM_RECAP = "EXTERNAL_SSM_RECAP";

	// param names for rev

	private Collection<String> actions;

	private FlightSchedule existingSchedule;

	private FlightSchedule existingOverlapSchedule;

	private FlightSchedule updatedSchedule;

	private FlightSchedule updatedOverlapSchedule;

	private Collection<Integer> addedGDSList;

	private Collection<Integer> deletedGDSList;
	
	private Collection<String> airports;

	private String serviceType;
	
	private Collection<SSMSplitFlightSchedule> existingSubSchedules;
	
	private Collection<SSMSplitFlightSchedule> updatedSubSchedules;
	
	private String externalSitaAddress;
	
	private String externalEmailAddress;
	
	private boolean externalSSMRecap;
	
	private Collection<Integer> createdFlightIds;
	private Collection<FlightSchedule> splitSchedules;

	private String supplementaryInfo;

	public Collection<String> getActions() {
		return actions;
	}

	public void setActions(Collection<String> actions) {
		this.actions = actions;
	}

	public void addAction(String aciton) {
		if (this.actions == null)
			this.actions = new ArrayList<String>();
		this.actions.add(aciton);
	}

	public FlightSchedule getExistingSchedule() {
		return existingSchedule;
	}

	public void setExistingSchedule(FlightSchedule existingSchedule) {
		this.existingSchedule = existingSchedule;
	}

	public FlightSchedule getExistingOverlapSchedule() {
		return existingOverlapSchedule;
	}

	public void setExistingOverlapSchedule(FlightSchedule existingOverlapSchedule) {
		this.existingOverlapSchedule = existingOverlapSchedule;
	}

	public FlightSchedule getUpdatedSchedule() {
		return updatedSchedule;
	}

	public void setUpdatedSchedule(FlightSchedule updatedSchedule) {
		this.updatedSchedule = updatedSchedule;
	}

	public FlightSchedule getUpdatedOverlapSchedule() {
		return updatedOverlapSchedule;
	}

	public void setUpdatedOverlapSchedule(FlightSchedule updatedOverlapSchedule) {
		this.updatedOverlapSchedule = updatedOverlapSchedule;
	}

	public Collection<Integer> getAddedGDSList() {
		return addedGDSList;
	}

	public void setAddedGDSList(Collection<Integer> addedGDSList) {
		this.addedGDSList = addedGDSList;
	}

	public Collection<Integer> getDeletedGDSList() {
		return deletedGDSList;
	}

	public void setDeletedGDSList(Collection<Integer> deletedGDSList) {
		this.deletedGDSList = deletedGDSList;
	}

	public boolean containsAction(String action) {
		return (this.actions != null) ? (this.actions.contains(action)) : false;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getExternalSitaAddress() {
		return externalSitaAddress;
	}

	public void setExternalSitaAddress(String externalSitaAddress) {
		this.externalSitaAddress = externalSitaAddress;
	}

	public String getExternalEmailAddress() {
		return externalEmailAddress;
	}

	public void setExternalEmailAddress(String externalEmailAddress) {
		this.externalEmailAddress = externalEmailAddress;
	}

	public boolean isExternalSSMRecap() {
		return externalSSMRecap;
	}

	public void setExternalSSMRecap(boolean externalSSMRecap) {
		this.externalSSMRecap = externalSSMRecap;
	}

	
	public Collection<String> getAirports() {
		return airports;
	}

	public void addAirports(String airport) {
		if (this.airports == null) {
			this.airports = new ArrayList<String>();
		}
		this.airports.add(airport);
	}

	public Collection<SSMSplitFlightSchedule> getExistingSubSchedules() {
		return existingSubSchedules;
	}

	public void setExistingSubSchedules(Collection<SSMSplitFlightSchedule> existingSubSchedules) {
		this.existingSubSchedules = existingSubSchedules;
	}

	public Collection<SSMSplitFlightSchedule> getUpdatedSubSchedules() {
		return updatedSubSchedules;
	}

	public void setUpdatedSubSchedules(Collection<SSMSplitFlightSchedule> updatedSubSchedules) {
		this.updatedSubSchedules = updatedSubSchedules;
	}
	

	public Collection<Integer> getCreatedFlightIds() {
		return createdFlightIds;
	}

	public void setCreatedFlightIds(Collection<Integer> createdFlightIds) {
		this.createdFlightIds = createdFlightIds;
	}

	public Collection<FlightSchedule> getSplitSchedules() {
		return splitSchedules;
	}

	public void setSplitSchedules(Collection<FlightSchedule> splitSchedules) {
		this.splitSchedules = splitSchedules;
	}

	public String getSupplementaryInfo() {
		return supplementaryInfo;
	}

	public void setSupplementaryInfo(String supplementaryInfo) {
		this.supplementaryInfo = supplementaryInfo;
	}

}
