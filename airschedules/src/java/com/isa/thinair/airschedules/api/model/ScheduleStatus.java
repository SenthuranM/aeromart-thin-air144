/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.api.model;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * ScheduleStatus is the entity class to repesent a ScheduleStatus model
 * 
 * @author Lasantha Pambagoda
 * @hibernate.class table = "T_SCHEDULE_STATUS"
 */
public class ScheduleStatus extends Persistent {

	private String code;

	private String description;

	/**
	 * @return Returns the code.
	 * @hibernate.id column = "SCHEDULE_STATUS_CODE" generator-class = "assigned"
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code
	 *            The code to set.
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return Returns the description.
	 * @hibernate.property column = "SCHEDULE_STATUS_DESCRIPTION"
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            The description to set.
	 */
	public void setDescription(String description) {
		this.description = description;
	}

}
