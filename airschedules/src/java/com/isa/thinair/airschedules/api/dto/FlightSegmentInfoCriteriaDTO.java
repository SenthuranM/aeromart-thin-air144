package com.isa.thinair.airschedules.api.dto;

import java.io.Serializable;
import java.util.List;

public class FlightSegmentInfoCriteriaDTO implements Serializable {

	private List<FlightSegmentDTO> flightSegmentDTOs;
	private boolean calculateSeatFactor;
	private boolean claculateBaggageWeight;

	public List<FlightSegmentDTO> getFlightSegmentDTOs() {
		return flightSegmentDTOs;
	}

	public void setFlightSegmentDTOs(List<FlightSegmentDTO> flightSegmentDTOs) {
		this.flightSegmentDTOs = flightSegmentDTOs;
	}

	public boolean isCalculateSeatFactor() {
		return calculateSeatFactor;
	}

	public void setCalculateSeatFactor(boolean calculateSeatFactor) {
		this.calculateSeatFactor = calculateSeatFactor;
	}

	public boolean isClaculateBaggageWeight() {
		return claculateBaggageWeight;
	}

	public void setClaculateBaggageWeight(boolean claculateBaggageWeight) {
		this.claculateBaggageWeight = claculateBaggageWeight;
	}
}
