package com.isa.thinair.airschedules.api.dto;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

/**
 * To hlod the past flight details
 */
public class PastFlightInfoDTO implements Serializable {

	private static final long serialVersionUID = -4045318893700944074L;

	private String flightId;

	private String flightNumber;

	private String departureDate;

	private String hdnMode;

	Map<String, String[]> contentMap;

	Collection<Integer> paxFareSegETIds;

	public String getFlightId() {
		return flightId;
	}

	public void setFlightId(String flightId) {
		this.flightId = flightId;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}

	public String getHdnMode() {
		return hdnMode;
	}

	public void setHdnMode(String hdnMode) {
		this.hdnMode = hdnMode;
	}

	public Map<String, String[]> getContentMap() {
		return contentMap;
	}

	public void setContentMap(Map<String, String[]> contentMap) {
		this.contentMap = contentMap;
	}

	public Collection<Integer> getPaxFareSegETIds() {
		return paxFareSegETIds;
	}

	public void setPaxFareSegETIds(Collection<Integer> paxFareSegETIds) {
		this.paxFareSegETIds = paxFareSegETIds;
	}

}
