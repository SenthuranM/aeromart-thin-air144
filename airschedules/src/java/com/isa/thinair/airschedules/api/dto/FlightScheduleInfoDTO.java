package com.isa.thinair.airschedules.api.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import com.isa.thinair.commons.api.dto.Frequency;

public class FlightScheduleInfoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3810288852991048186L;

	private Integer scheduleId;

	private Date startDate;

	private Date stopDate;

	private Date startDateLocal;

	private Date stopDateLocal;

	private String flightNumber;

	private String departureStnCode;

	private String arrivalStnCode;

	private String departureAptName;

	private String arrivalAptName;

	private String modelNumber;

	private Frequency frequency;

	private Frequency frequencyLocal;

	private Set<FlightScheduleLegInfoDTO> flightScheduleLegs;

	private Set<FlightScheduleSegmentInfoDTO> flightScheduleSegments;

	private Integer overlapingScheduleId;

	private Integer availableSeatKilometers;

	private Integer numberOfDepartures;

	private int operationTypeId;

	private String buildStatusCode;

	private String statusCode;

	private boolean manuallyChanged = false;

	private boolean localChange = false;

	private Set<Integer> gdsIds;

	private int numberOfOpenFlights;

	private int sequenceId;
	
	private long version;

	/**
	 * @return the sequenceId
	 */
	public int getSequenceId() {
		return sequenceId;
	}

	/**
	 * @param sequenceId
	 *            the sequenceId to set
	 */
	public void setSequenceId(int sequenceId) {
		this.sequenceId = sequenceId;
	}

	/**
	 * @return the departureAptName
	 */
	public String getDepartureAptName() {
		return departureAptName;
	}

	/**
	 * @param departureAptName
	 *            the departureAptName to set
	 */
	public void setDepartureAptName(String departureAptName) {
		this.departureAptName = departureAptName;
	}

	/**
	 * @return the arrivalAptName
	 */
	public String getArrivalAptName() {
		return arrivalAptName;
	}

	/**
	 * @param arrivalAptName
	 *            the arrivalAptName to set
	 */
	public void setArrivalAptName(String arrivalAptName) {
		this.arrivalAptName = arrivalAptName;
	}

	/**
	 * @return the overlapingScheduleId
	 */
	public Integer getOverlapingScheduleId() {
		return overlapingScheduleId;
	}

	/**
	 * @param overlapingScheduleId
	 *            the overlapingScheduleId to set
	 */
	public void setOverlapingScheduleId(Integer overlapingScheduleId) {
		this.overlapingScheduleId = overlapingScheduleId;
	}

	/**
	 * @return the availableSeatKilometers
	 */
	public Integer getAvailableSeatKilometers() {
		return availableSeatKilometers;
	}

	/**
	 * @param availableSeatKilometers
	 *            the availableSeatKilometers to set
	 */
	public void setAvailableSeatKilometers(Integer availableSeatKilometers) {
		this.availableSeatKilometers = availableSeatKilometers;
	}

	/**
	 * @return the numberOfDepartures
	 */
	public Integer getNumberOfDepartures() {
		return numberOfDepartures;
	}

	/**
	 * @param numberOfDepartures
	 *            the numberOfDepartures to set
	 */
	public void setNumberOfDepartures(Integer numberOfDepartures) {
		this.numberOfDepartures = numberOfDepartures;
	}

	/**
	 * @return the operationTypeId
	 */
	public int getOperationTypeId() {
		return operationTypeId;
	}

	/**
	 * @param operationTypeId
	 *            the operationTypeId to set
	 */
	public void setOperationTypeId(int operationTypeId) {
		this.operationTypeId = operationTypeId;
	}

	/**
	 * @return the buildStatusCode
	 */
	public String getBuildStatusCode() {
		return buildStatusCode;
	}

	/**
	 * @param buildStatusCode
	 *            the buildStatusCode to set
	 */
	public void setBuildStatusCode(String buildStatusCode) {
		this.buildStatusCode = buildStatusCode;
	}

	/**
	 * @return the statusCode
	 */
	public String getStatusCode() {
		return statusCode;
	}

	/**
	 * @param statusCode
	 *            the statusCode to set
	 */
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	/**
	 * @return the manuallyChanged
	 */
	public boolean isManuallyChanged() {
		return manuallyChanged;
	}

	/**
	 * @param manuallyChanged
	 *            the manuallyChanged to set
	 */
	public void setManuallyChanged(boolean manuallyChanged) {
		this.manuallyChanged = manuallyChanged;
	}

	/**
	 * @return the localChange
	 */
	public boolean isLocalChange() {
		return localChange;
	}

	/**
	 * @param localChange
	 *            the localChange to set
	 */
	public void setLocalChange(boolean localChange) {
		this.localChange = localChange;
	}

	/**
	 * @return the gdsIds
	 */
	public Set<Integer> getGdsIds() {
		return gdsIds;
	}

	/**
	 * @param gdsIds
	 *            the gdsIds to set
	 */
	public void setGdsIds(Set<Integer> gdsIds) {
		this.gdsIds = gdsIds;
	}

	/**
	 * @return the numberOfOpenFlights
	 */
	public int getNumberOfOpenFlights() {
		return numberOfOpenFlights;
	}

	/**
	 * @param numberOfOpenFlights
	 *            the numberOfOpenFlights to set
	 */
	public void setNumberOfOpenFlights(int numberOfOpenFlights) {
		this.numberOfOpenFlights = numberOfOpenFlights;
	}

	/**
	 * @return the scheduleId
	 */
	public Integer getScheduleId() {
		return scheduleId;
	}

	/**
	 * @param scheduleId
	 *            the scheduleId to set
	 */
	public void setScheduleId(Integer scheduleId) {
		this.scheduleId = scheduleId;
	}

	/**
	 * @return the frequencyLocal
	 */
	public Frequency getFrequencyLocal() {
		return frequencyLocal;
	}

	/**
	 * @param frequencyLocal
	 *            the frequencyLocal to set
	 */
	public void setFrequencyLocal(Frequency frequencyLocal) {
		this.frequencyLocal = frequencyLocal;
	}

	/**
	 * @return the startDate
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate
	 *            the startDate to set
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the stopDate
	 */
	public Date getStopDate() {
		return stopDate;
	}

	/**
	 * @param stopDate
	 *            the stopDate to set
	 */
	public void setStopDate(Date stopDate) {
		this.stopDate = stopDate;
	}

	/**
	 * @return the startDateLocal
	 */
	public Date getStartDateLocal() {
		return startDateLocal;
	}

	/**
	 * @param startDateLocal
	 *            the startDateLocal to set
	 */
	public void setStartDateLocal(Date startDateLocal) {
		this.startDateLocal = startDateLocal;
	}

	/**
	 * @return the stopDateLocal
	 */
	public Date getStopDateLocal() {
		return stopDateLocal;
	}

	/**
	 * @param stopDateLocal
	 *            the stopDateLocal to set
	 */
	public void setStopDateLocal(Date stopDateLocal) {
		this.stopDateLocal = stopDateLocal;
	}

	/**
	 * @return the flightNumber
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * @param flightNumber
	 *            the flightNumber to set
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * @return the departureStnCode
	 */
	public String getDepartureStnCode() {
		return departureStnCode;
	}

	/**
	 * @param departureStnCode
	 *            the departureStnCode to set
	 */
	public void setDepartureStnCode(String departureStnCode) {
		this.departureStnCode = departureStnCode;
	}

	/**
	 * @return the arrivalStnCode
	 */
	public String getArrivalStnCode() {
		return arrivalStnCode;
	}

	/**
	 * @param arrivalStnCode
	 *            the arrivalStnCode to set
	 */
	public void setArrivalStnCode(String arrivalStnCode) {
		this.arrivalStnCode = arrivalStnCode;
	}

	/**
	 * @return the modelNumber
	 */
	public String getModelNumber() {
		return modelNumber;
	}

	/**
	 * @param modelNumber
	 *            the modelNumber to set
	 */
	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}

	/**
	 * @return the frequency
	 */
	public Frequency getFrequency() {
		return frequency;
	}

	/**
	 * @param frequency
	 *            the frequency to set
	 */
	public void setFrequency(Frequency frequency) {
		this.frequency = frequency;
	}

	/**
	 * @return the flightScheduleLegs
	 */
	public Set<FlightScheduleLegInfoDTO> getFlightScheduleLegs() {
		return flightScheduleLegs;
	}

	/**
	 * @param flightScheduleLegs
	 *            the flightScheduleLegs to set
	 */
	public void setFlightScheduleLegs(Set<FlightScheduleLegInfoDTO> flightScheduleLegs) {
		this.flightScheduleLegs = flightScheduleLegs;
	}

	/**
	 * @return the flightScheduleSegments
	 */
	public Set<FlightScheduleSegmentInfoDTO> getFlightScheduleSegments() {
		return flightScheduleSegments;
	}

	/**
	 * @param flightScheduleSegments
	 *            the flightScheduleSegments to set
	 */
	public void setFlightScheduleSegments(Set<FlightScheduleSegmentInfoDTO> flightScheduleSegments) {
		this.flightScheduleSegments = flightScheduleSegments;
	}

	public FlightScheduleInfoDTO() {
	}

	/**
	 * @return the version
	 */
	public long getVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(long version) {
		this.version = version;
	}


}
