/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.api.utils;

/**
 * constant class to hold responce codes
 * 
 * @author Lasantha Pambagoda
 */
public abstract class ResponceCodes {

	// not success codes
	public static final String SCHEDULE_FLIGHT_CONFLICT = "airschedules.logic.bl.schedule.flight.overlapped";

	public static final String SCHEDULE_SCHEDULE_CONFLICT = "airschedules.logic.bl.schedule.schedule.overlapped";

	public static final String RESERVATIONS_FOUND_FOR_SCHEDULE = "airschedules.logic.bl.schedule.reservationsexist";

	public static final String RESERVATIONS_FOUND_FOR_FLIGHT = "airschedules.logic.bl.flight.reservationsexist";

	public static final String RESERVATIONS_NOT_FOUND_FOR_SCHEDULE = "airschedules.logic.bl.schedule.reservationsnotexist";

	public static final String RESERVATIONS_NOT_FOUND_FOR_FLIGHT = "airschedules.logic.bl.flight.reservationsnotexist";

	public static final String RESERVATIONS_FOUND_FOR_LEGCHANGED_SCHEDULE = "airschedules.logic.bl.schedule.legchanged.reservationsexist";

	public static final String INVALID_OVERLAPPING_SCHEDULE = "airschedules.logic.bl.schedule.overlapschedule.invalid";

	public static final String INVALID_OVERLAPPING_SEGMENT_LEG = "airschedules.logic.bl.schedule.overlapsegment.invalid";

	public static final String INVALID_OVERLAPPING_SCHEDULE_STATUS = "airschedules.logic.bl.schedule.overlapbuildstatus.invalid";

	public static final String OVERLAPPING_ALREADY_OVERLAPPING_SCHEDULE = "airschedules.logic.bl.schedule.alreadyoverlap";

	public static final String CANNOT_DELETE_BC_INVENTORY = "airschedules.logic.bl.bc.inventory.not.deletable";

	public static final String CABIN_CLASS_NOT_FOUND = "airschedules.logic.bl.cabin.class.not.found";

	public static final String DATE = "airschedules.logic.bl.error.date";

	public static final String FLIGHT = "airschedules.logic.bl.error.flight";

	public static final String CC_CAPACITY_INSUFFICIENT = "airschedules.logic.bl.cc.capacity.insufficient";

	public static final String INVALID_LEG_DURATION = "airschedules.logic.bl.schedule.invalidduration";

	public static final String AllOWED_DURATION_ZERO = "airschedules.logic.bl.schedule.allowed.durationzero";

	public static final String WARNING_FOR_FLIGHT_UPDATE = "airschedules.logic.bl.flight.updation.warning";

	public static final String TRYING_ILLEGAL_FLIGHT_UPDATION = "airschedules.logic.bl.flight.illegalupdation";

	public static final String INVALID_FLIGHT_STATUS_CHANGE = "airschedules.logic.bl.flight.status.invalid";

	public static final String RESERVATIONS_FOUND_FOR_LEGCHANGED_FLIGHT = "airschedules.logic.bl.flight.legchanged.reservationsexist";

	public static final String INVALID_OVERLAPPING_FLIGHT = "airschedules.logic.bl.flight.invalidoverlap";

	public static final String INVALID_OVERLAPPING_FLIGHT_EXISTING_FLIGHT = "airschedules.logic.bl.flight.invalidoverlap.existingflight";

	public static final String OVERLAPPING_ALREADY_OVERLAPPING_FLIGHT = "airschedules.logic.bl.flight.alreadyoverlap";

	public static final String FLIGHT_FLIGHT_CONFLICT = "airschedules.logic.bl.flight.flight.overlapped";

	public static final String FLIGHT_SCHEDULE_CONFLICT = "airschedules.logic.bl.flight.schedule.overlapped";

	public static final String RESERVATIONS_EXIST_FOR_INVALIDATED_SEGMENT = "airschedules.logic.bl.flight.reservationsexist.invalidatedsegment";

	public static final String RESERVATIONS_EXIST_FOR_OVERLAP_REMOVED_SEGMENT = "airschedules.logic.bl.flight.reservationsexist.removedoverlap";

	public static final String ALL_SEGMENTS_INVALID = "airschedules.logic.bl.allsegments.invalid";

	public static final String INVALID_SCHEDULE_FREQUENCE_FOR_DATE_RANGE = "airschedules.logic.bl.schedule.copy.invalid.daterange";

	// success codes
	public static final String SEND_ALLERTY_SUCCESSFULL = "airschedules.logic.bl.flight.sendallert.success";

	public static final String CREATE_SCHEDULE_SUCCESSFULL = "airschedules.logic.bl.schedule.createschedule.success";
	
	public static final String SCHEDULE_GDS_AUTOPUBLISH_SUCCESSFULL = "airschedules.logic.bl.schedule.autopublish.success";

	public static final String COPY_SCHEDULE_SUCCESSFULL = "airschedules.logic.bl.schedule.copyschedule.success";

	public static final String AMMEND_SCHEDULE_SUCCESSFULL = "airschedules.logic.bl.schedule.ammendschedule.success";

	public static final String CANCEL_SCHEDULE_SUCCESSFULL = "airschedules.logic.bl.schedule.cancelschedule.success";

	public static final String SPLIT_SCHEDULE_SUCCESSFULL = "airschedules.logic.bl.schedule.splitschedule.success";

	public static final String BUILD_SCHEDULE_SUCCESSFULL = "airschedules.logic.bl.schedule.buildschedule.success";

	public static final String BUILD_SCHEDULE_MAXTIME_EXCEEDED = "airschedules.logic.bl.schedule.buildschedule.maxtime.exceeded";

	public static final String BUILD_SCHEDULE_PARTIALLY_SUCCESSFUL = "airschedules.logic.bl.schedule.buildschedule.partially.successful";

	public static final String BUILD_SCHEDULE_FAILED = "airschedules.logic.bl.schedule.buildschedule.failed";

	public static final String CREATE_FLIGHT_SUCCESSFULL = "airschedules.logic.bl.flight.createflight.success";
	
	public static final String FLIGHT_GDS_AUTOPUBLISH_SUCCESSFULL = "airschedules.logic.bl.flight.autopublish.success";

	public static final String COPY_FLIGHT_SUCCESSFULL = "airschedules.logic.bl.flight.copyflight.success";

	public static final String AMMEND_FLIGHT_SUCCESSFULL = "airschedules.logic.bl.flight.ammendflight.success";

	public static final String CANCEL_FLIGHT_SUCCESSFULL = "airschedules.logic.bl.flight.cancelflight.success";

	public static final String AVAILABILITY_SEARCH_SUCCESSFULL = "airschedules.logic.bl.availability.search.success";

	public static final String AVAILABILITY_FLIGHT_CALENDAR_SEARCH_SUCCESSFULL = "airschedules.logic.bl.availability.calendar.search.success";

	public static final String FILL_SELECTED_FLIGHT_SUCCESSFULL = "airschedules.logic.bl.fillselected.success";

	public static final String RECALCULATE_LOCAL_TIME_SUCCESS = "airschedules.logic.bl.recalculate.localtime.success";

	public static final String ADD_REMOVE_GDS_SUCCESS = "airschedules.logic.bl.addremovegds.success";

	public static final String SEARCH_FLT_DESTINATION_LOCAL = "airschedules.logic.bl.destination.local";

	public static final String AIRCRAFT_UPDATED_MODEL_SEATATTACHED = "airschedules.logic.bl.flight.seatattached";

	public static final String ROLL_FORWARD_AVAILABILITY_SEARCH_SUCCESSFULL = "airschedules.logic.bl.roll.forward.availability.search.success";

	public static final String CC_INFANT_CAPACITY_INSUFFICIENT = "um.airschedules.logic.bl.cc.infantcapacity.insufficient";

	public static final String CC_FIXED_SEATS_INSUFFICIENT = "airschedules.logic.bl.cc.fixedseats.insufficient";

	public static final String CC_CURTAILED_CAPACITY_INSUFFICIENT = "um.airschedules.logic.bl.cc.curtailedcapacity.insufficient";

	public static final String CC_WAITLISTED_CAPACITY_INSUFFICIENT = "um.airschedules.logic.bl.cc.waitlistedcapacity.insufficient";

	public static final String INVALID_DEPARTURE_DATE = "um.flightschedule.leg.invaliddeptDate";
	
	public static final String SKIPPED_FLIGHT_DATES = "airschedules.logic.bl.ammend.schedule.skippedflightdates";


}
