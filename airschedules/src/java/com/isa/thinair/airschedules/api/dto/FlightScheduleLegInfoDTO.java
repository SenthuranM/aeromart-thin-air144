package com.isa.thinair.airschedules.api.dto;

import java.io.Serializable;
import java.util.Date;

public class FlightScheduleLegInfoDTO implements Serializable, Comparable<FlightScheduleLegInfoDTO> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6749339471299877562L;

	private int legNumber;

	private Integer scheduleId;

	private String modelRouteId;

	private Date estDepartureTimeZulu;

	private Date estArrivalTimeZulu;

	private Date estDepartureTimeLocal;

	private Date estArrivalTimeLocal;

	private int duration;

	private int estDepartureDayOffset;

	private int estArrivalDayOffset;

	private int estDepartureDayOffsetLocal;

	private int estArrivalDayOffsetLocal;

	private String origin;

	private String destination;

	private Integer id;

	public FlightScheduleLegInfoDTO() {
	}

	/**
	 * @return the modelRouteId
	 */
	public String getModelRouteId() {
		return modelRouteId;
	}

	/**
	 * @param modelRouteId
	 *            the modelRouteId to set
	 */
	public void setModelRouteId(String modelRouteId) {
		this.modelRouteId = modelRouteId;
	}

	/**
	 * @return the scheduleId
	 */
	public Integer getScheduleId() {
		return scheduleId;
	}

	/**
	 * @param scheduleId
	 *            the scheduleId to set
	 */
	public void setScheduleId(Integer scheduleId) {
		this.scheduleId = scheduleId;
	}

	/**
	 * @return the legNumber
	 */
	public int getLegNumber() {
		return legNumber;
	}

	/**
	 * @param legNumber
	 *            the legNumber to set
	 */
	public void setLegNumber(int legNumber) {
		this.legNumber = legNumber;
	}

	/**
	 * @return the estDepartureTimeZulu
	 */
	public Date getEstDepartureTimeZulu() {
		return estDepartureTimeZulu;
	}

	/**
	 * @param estDepartureTimeZulu
	 *            the estDepartureTimeZulu to set
	 */
	public void setEstDepartureTimeZulu(Date estDepartureTimeZulu) {
		this.estDepartureTimeZulu = estDepartureTimeZulu;
	}

	/**
	 * @return the estArrivalTimeZulu
	 */
	public Date getEstArrivalTimeZulu() {
		return estArrivalTimeZulu;
	}

	/**
	 * @param estArrivalTimeZulu
	 *            the estArrivalTimeZulu to set
	 */
	public void setEstArrivalTimeZulu(Date estArrivalTimeZulu) {
		this.estArrivalTimeZulu = estArrivalTimeZulu;
	}

	/**
	 * @return the estDepartureTimeLocal
	 */
	public Date getEstDepartureTimeLocal() {
		return estDepartureTimeLocal;
	}

	/**
	 * @param estDepartureTimeLocal
	 *            the estDepartureTimeLocal to set
	 */
	public void setEstDepartureTimeLocal(Date estDepartureTimeLocal) {
		this.estDepartureTimeLocal = estDepartureTimeLocal;
	}

	/**
	 * @return the estArrivalTimeLocal
	 */
	public Date getEstArrivalTimeLocal() {
		return estArrivalTimeLocal;
	}

	/**
	 * @param estArrivalTimeLocal
	 *            the estArrivalTimeLocal to set
	 */
	public void setEstArrivalTimeLocal(Date estArrivalTimeLocal) {
		this.estArrivalTimeLocal = estArrivalTimeLocal;
	}

	/**
	 * @return the duration
	 */
	public int getDuration() {
		return duration;
	}

	/**
	 * @param duration
	 *            the duration to set
	 */
	public void setDuration(int duration) {
		this.duration = duration;
	}

	/**
	 * @return the estDepartureDayOffset
	 */
	public int getEstDepartureDayOffset() {
		return estDepartureDayOffset;
	}

	/**
	 * @param estDepartureDayOffset
	 *            the estDepartureDayOffset to set
	 */
	public void setEstDepartureDayOffset(int estDepartureDayOffset) {
		this.estDepartureDayOffset = estDepartureDayOffset;
	}

	/**
	 * @return the estArrivalDayOffset
	 */
	public int getEstArrivalDayOffset() {
		return estArrivalDayOffset;
	}

	/**
	 * @param estArrivalDayOffset
	 *            the estArrivalDayOffset to set
	 */
	public void setEstArrivalDayOffset(int estArrivalDayOffset) {
		this.estArrivalDayOffset = estArrivalDayOffset;
	}

	/**
	 * @return the estDepartureDayOffsetLocal
	 */
	public int getEstDepartureDayOffsetLocal() {
		return estDepartureDayOffsetLocal;
	}

	/**
	 * @param estDepartureDayOffsetLocal
	 *            the estDepartureDayOffsetLocal to set
	 */
	public void setEstDepartureDayOffsetLocal(int estDepartureDayOffsetLocal) {
		this.estDepartureDayOffsetLocal = estDepartureDayOffsetLocal;
	}

	/**
	 * @return the estArrivalDayOffsetLocal
	 */
	public int getEstArrivalDayOffsetLocal() {
		return estArrivalDayOffsetLocal;
	}

	/**
	 * @param estArrivalDayOffsetLocal
	 *            the estArrivalDayOffsetLocal to set
	 */
	public void setEstArrivalDayOffsetLocal(int estArrivalDayOffsetLocal) {
		this.estArrivalDayOffsetLocal = estArrivalDayOffsetLocal;
	}

	/**
	 * @return the origin
	 */
	public String getOrigin() {
		return origin;
	}

	/**
	 * @param origin
	 *            the origin to set
	 */
	public void setOrigin(String origin) {
		this.origin = origin;
	}

	/**
	 * @return the destination
	 */
	public String getDestination() {
		return destination;
	}

	/**
	 * @param destination
	 *            the destination to set
	 */
	public void setDestination(String destination) {
		this.destination = destination;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public int compareTo(FlightScheduleLegInfoDTO o) {
		return Integer.valueOf(getLegNumber()).compareTo(o.getLegNumber());
	}

}
