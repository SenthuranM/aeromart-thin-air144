package com.isa.thinair.airschedules.api.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airschedules.api.model.FlightSegement;

public class FlightAlertInfoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 810019742143404550L;
	private Integer flightId;
	private String flightNumber;
	private Date departureDate;
	private String modelNumber;
	private String status;
	private String originAptCode;
	private String destinationAptCode;
	private Integer scheduleId;
	private Set<FlightSegmentDTO> changedSegments;
	// used by schedule module -------------
	private Set<FlightSegement> segments;
	// -------------------------------------

	private String action;	
	private Integer numberOfFlights;
	private Date startDate;	
	private Date endDate;	
	private String operationDays;
	private Integer flightSegmentId; 

	// for messaging

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	private String legDetails;
	private String segmentDetails;
	private String route;

	public String getSegmentDetails() {
		return segmentDetails;
	}

	public void setSegmentDetails(String segmentDetails) {
		this.segmentDetails = segmentDetails;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	/**
	 * Return departure date in a format
	 * 
	 * @param dateFormat
	 * @return
	 */
	public String getDepartureStringDate(String dateFormat) {
		return BeanUtils.parseDateFormat(this.getDepartureDate(), dateFormat);
	}

	public String getDestinationAptCode() {
		return destinationAptCode;
	}

	public void setDestinationAptCode(String destinationAptCode) {
		this.destinationAptCode = destinationAptCode;
	}

	public Integer getFlightId() {
		return flightId;
	}

	public void setFlightId(Integer flightId) {
		this.flightId = flightId;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getLegDetails() {
		return legDetails;
	}

	public void setLegDetails(String legDetails) {
		this.legDetails = legDetails;
	}

	public String getModelNumber() {
		return modelNumber;
	}

	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}

	public String getOriginAptCode() {
		return originAptCode;
	}

	public void setOriginAptCode(String originAptCode) {
		this.originAptCode = originAptCode;
	}

	public String getRoute() {
		return route;
	}

	public void setRoute(String route) {
		this.route = route;
	}

	public Integer getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(Integer scheduleId) {
		this.scheduleId = scheduleId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Set<FlightSegement> getSegments() {
		return segments;
	}

	public void setSegments(Set<FlightSegement> segments) {
		this.segments = segments;
	}

	public Set<FlightSegmentDTO> getChangedSegments() {
		return changedSegments;
	}

	public void setChangedSegments(Set<FlightSegmentDTO> changedSegments) {
		this.changedSegments = changedSegments;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	public String getStartDateFormatted(String dateFormat) {
		return BeanUtils.parseDateFormat(this.getStartDate(), dateFormat);
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	public String getEndDateFormatted(String dateFormat) {
		return BeanUtils.parseDateFormat(this.getEndDate(), dateFormat);
	}

	public String getOperationDays() {
		return operationDays;
	}

	public void setOperationDays(String operationDays) {
		this.operationDays = operationDays;
	}

	public Integer getNumberOfFlights() {
		return numberOfFlights;
	}

	public void setNumberOfFlights(Integer numberOfFlights) {
		this.numberOfFlights = numberOfFlights;
	}

	public Integer getFlightSegmentId() {
		return flightSegmentId;
	}

	public void setFlightSegmentId(Integer flightSegmentId) {
		this.flightSegmentId = flightSegmentId;
	}
}
