/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.api.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Lasantha Pambagoda
 */
public class OverlappedFlightDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6008550059437103155L;

	private String flightNumber;

	private String operationType;

	private Date departureDate;

	/**
	 * @return Returns the endDate.
	 */
	public Date getDepartureDate() {
		return departureDate;
	}

	/**
	 * @param endDate
	 *            The endDate to set.
	 */
	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	/**
	 * @return Returns the flightNumber.
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * @param flightNumber
	 *            The flightNumber to set.
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * @return Returns the operationType.
	 */
	public String getOperationType() {
		return operationType;
	}

	/**
	 * @param operationType
	 *            The operationType to set.
	 */
	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

}
