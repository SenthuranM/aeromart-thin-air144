/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:17:34
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.api.model;

/**
 * FlightLeg is the entity class to repesent a FlightLeg model
 * 
 * @author Code Generation Tool, Lasantha Pambagoda
 * @version Ver 1.0.0
 * @hibernate.class table = "T_FLIGHT_LEG"
 */
public class FlightLeg extends Leg {

	private static final long serialVersionUID = -2229207593664251318L;

	// private fields
	private Integer id;

	private Integer flightId;

	/**
	 * returns the id
	 * 
	 * @return Returns the id.
	 * @hibernate.id column = "FLIGHT_LEG_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_FLIGHT_LEG"
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * sets the id
	 * 
	 * @param id
	 *            The id to set.
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * returns the flightId
	 * 
	 * @return flightId
	 * @hibernate.property column = "FLIGHT_ID"
	 */
	public Integer getFlightId() {
		return flightId;
	}

	/**
	 * sets the flightId
	 * 
	 * @param flightId
	 */
	public void setFlightId(Integer flightId) {
		this.flightId = flightId;
	}
}
