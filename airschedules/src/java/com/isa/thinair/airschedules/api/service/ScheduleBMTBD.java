/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.api.service;

import java.util.Collection;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Interface to define methods required for the Schedule
 * 
 * @author Lasantha Pambagoda
 */
public interface ScheduleBMTBD {
	public static final String SERVICE_NAME = "ScheduleServiceBMT";

	public ServiceResponce buildSchedulesBMT(Collection<Integer> scheduleIds, boolean buildDefaultInventory)
			throws ModuleException;
}
