package com.isa.thinair.airschedules.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class CalendarFlightRS implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Map<Integer, List<AvailableONDFlight>> ondWiseAvailableFlights = null;

	private Map<Integer, Set<String>> ondWiseDistinctOndCodes = null;

	public void addOndFlight(Integer ondSequence, AvailableONDFlight ondFlight) {
		getAvailableONDFlights(ondSequence).add(ondFlight);
		getDistinctOndCodesFor(ondSequence).addAll(ondFlight.getDistinctOndCodes());
	}

	private Set<String> getDistinctOndCodesFor(Integer ondSequence) {
		if (!getOndWiseDistinctOndCodes().containsKey(ondSequence)) {
			getOndWiseDistinctOndCodes().put(ondSequence, new HashSet<String>());
		}
		return getOndWiseDistinctOndCodes().get(ondSequence);
	}

	public Map<Integer, Set<String>> getOndWiseDistinctOndCodes() {
		if (this.ondWiseDistinctOndCodes == null) {
			this.ondWiseDistinctOndCodes = new HashMap<Integer, Set<String>>();
		}
		return this.ondWiseDistinctOndCodes;
	}

	public List<AvailableONDFlight> getAvailableONDFlights(int ondSequence) {
		if (getondWiseAvailableFlights().get(ondSequence) == null) {
			getondWiseAvailableFlights().put(ondSequence, new ArrayList<AvailableONDFlight>());
		}
		return getondWiseAvailableFlights().get(ondSequence);
	}

	private Map<Integer, List<AvailableONDFlight>> getondWiseAvailableFlights() {
		if (ondWiseAvailableFlights == null) {
			ondWiseAvailableFlights = new HashMap<Integer, List<AvailableONDFlight>>();
		}
		return ondWiseAvailableFlights;
	}

}
