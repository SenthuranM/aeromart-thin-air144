package com.isa.thinair.airschedules.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class FlightSegmentInfoDTO implements Serializable {
	private int flightSegId;
	private BigDecimal loadFactor;
	private BigDecimal baggageWeight;

	public int getFlightSegId() {
		return flightSegId;
	}

	public void setFlightSegId(int flightSegId) {
		this.flightSegId = flightSegId;
	}

	public BigDecimal getLoadFactor() {
		return loadFactor;
	}

	public void setLoadFactor(BigDecimal loadFactor) {
		this.loadFactor = loadFactor;
	}

	public BigDecimal getBaggageWeight() {
		return baggageWeight;
	}

	public void setBaggageWeight(BigDecimal baggageWeight) {
		this.baggageWeight = baggageWeight;
	}
}
