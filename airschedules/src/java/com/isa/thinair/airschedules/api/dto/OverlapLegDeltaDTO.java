package com.isa.thinair.airschedules.api.dto;

import java.io.Serializable;

/**
 * @author Lasantha Pambagoda
 * 
 */
public class OverlapLegDeltaDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4939224311056608302L;
	private int legNumber;
	private long arrivalDeltaInMillis;
	private long departureDeltaInMillis;

	public long getArrivalDeltaInMillis() {
		return arrivalDeltaInMillis;
	}

	public void setArrivalDeltaInMillis(long arrivalDeltaInMillis) {
		this.arrivalDeltaInMillis = arrivalDeltaInMillis;
	}

	public long getDepartureDeltaInMillis() {
		return departureDeltaInMillis;
	}

	public void setDepartureDeltaInMillis(long departureDeltaInMillis) {
		this.departureDeltaInMillis = departureDeltaInMillis;
	}

	public int getLegNumber() {
		return legNumber;
	}

	public void setLegNumber(int legNumber) {
		this.legNumber = legNumber;
	}
}
