package com.isa.thinair.airschedules.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableConnectedFlight;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airproxy.api.utils.AirProxyReservationUtil;

/**
 * DTO for constructing a connection journey during flight availability search.
 * 
 * @author Mohamed Nasly
 * 
 */
public class ConnectedAvailableFlightSegmentsDTO implements Serializable {

	private static final long serialVersionUID = 4214867899584470666L;

	/** Holds flight segments that makes the connection */
	private final List<AvailableFlightSegment> availableFlightSegments = new ArrayList<AvailableFlightSegment>();

	/**
	 * Returns all the flight segments.
	 * 
	 * @return List list of {@link AvailableFlightSegment}
	 */
	public List<AvailableFlightSegment> getAvailableFlightSegments() {
		return this.availableFlightSegments;
	}

	/**
	 * Adds {@link FlightSegmentDTO} at the end of the connection.
	 * 
	 * @param {@link FlightSegmentDTO}
	 */
	public void addAvailableFlightSegment(AvailableFlightSegment availableFlightSegment) {
		this.availableFlightSegments.add(availableFlightSegment);
	}

	/**
	 * Returns last flight segment of the connection.
	 * 
	 * @return {@link FlightSegmentDTO} last flight segment
	 */
	public FlightSegmentDTO getLastFlightSegmentDTO() {
		FlightSegmentDTO lastFlightSegmentDTO = null;
		if (!availableFlightSegments.isEmpty()) {
			lastFlightSegmentDTO = (availableFlightSegments.get(availableFlightSegments.size() - 1)).getFlightSegmentDTO();
		}
		return lastFlightSegmentDTO;
	}

	/**
	 * Constructs AvailableConnectedFlight with deep copying of {@link AvailableFlightSegment}.
	 * 
	 * @return AvailableConnectedFlight
	 */
	public AvailableConnectedFlight constructAvailableConnectedFlight() {
		AvailableConnectedFlight availableConnectedFlight = new AvailableConnectedFlight();
		Set<String> availableLogicalCabinClasses = new HashSet<String>();
		String ondCode = null;
		Collections.sort(availableFlightSegments);
		boolean isUntouched = true;
		for (AvailableFlightSegment availableFlightSegment : availableFlightSegments) {
			String flightSegmentCode = availableFlightSegment.getFlightSegmentDTO().getSegmentCode();
			if (availableLogicalCabinClasses.isEmpty()) {
				availableLogicalCabinClasses.addAll(availableFlightSegment.getAvailableLogicalCabinClasses());
			} else {
				availableLogicalCabinClasses.retainAll(availableFlightSegment.getAvailableLogicalCabinClasses());
			}
			ondCode = AirProxyReservationUtil.buildOndCode(ondCode, flightSegmentCode);
			if (availableConnectedFlight.getOndSequence() == -1) {
				availableConnectedFlight.setOndSequence(availableFlightSegment.getOndSequence());
			}
			if (!availableFlightSegment.isUnTouchedOnd()) {
				isUntouched = false;
			}
			availableConnectedFlight.setUnTouchedOnd(isUntouched);
			availableConnectedFlight.addAvailableFlightSegment(availableFlightSegment.getCleanedInstance());
		}

		availableConnectedFlight.setOndCode(ondCode);
		for (String availableLogicalCabinClass : availableLogicalCabinClasses) {
			availableConnectedFlight.setSeatsAvailable(availableLogicalCabinClass, true);
		}
		return availableConnectedFlight;
	}

	/**
	 * Returs true if at least one marketing carrier segment is present in the connection.
	 * 
	 * @return boolean
	 */
	public boolean hasSystemCarriersSegments(Collection<String> systemCarriers) {
		if (systemCarriers != null) {
			for (AvailableFlightSegment availableFlightSegment : availableFlightSegments) {
				if (systemCarriers.contains(availableFlightSegment.getFlightSegmentDTO().getCarrierCode())) {
					return true;
				}
			}
		}
		return false;
	}

	public ConnectedAvailableFlightSegmentsDTO shallowClone() {
		ConnectedAvailableFlightSegmentsDTO shallowClone = new ConnectedAvailableFlightSegmentsDTO();
		for (AvailableFlightSegment availableFlightSegment : availableFlightSegments) {
			shallowClone.addAvailableFlightSegment(availableFlightSegment);
		}
		return shallowClone;
	}

}
