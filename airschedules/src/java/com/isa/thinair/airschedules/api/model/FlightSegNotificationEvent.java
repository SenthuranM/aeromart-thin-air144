package com.isa.thinair.airschedules.api.model;

import java.sql.Blob;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * FlightSegNotificationEvent is the entity class to repesent a Flight Segment Notification status
 * 
 * @author Code Generation Tool
 * @version Ver 1.0.0
 * @hibernate.class table = "T_FLIGHT_SEG_NOTIFICATION_EVNT"
 */
public class FlightSegNotificationEvent extends Persistent {

	private static final long serialVersionUID = 7437146861457346575L;
	private Integer flightMsgEventId;
	private Integer flightSegId;
	private String ancillaryNotifyStatus;
	private String failureReason;
	private Date timeStamp;
	private Integer noOtAttempts;
	private Date startTimeStamp;
	private Date endTimeStamp;
	private Integer notificationCount;
	private String notificationType;
	private String insFirstResponse;
	private Blob insSecResponse;

	/**
	 * @return Returns the flightMsgEventId
	 * @hibernate.id column = "FLT_SEG_NOTIFICATION_EVNT_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_FLIGHT_SEG_NOTIFICATION_EVNT"
	 * @return
	 */
	public Integer getFlightMsgEventId() {
		return flightMsgEventId;
	}

	/**
	 * @param flightMsgEventId
	 *            the flightMsgEventId to set
	 */
	public void setFlightMsgEventId(Integer flightMsgEventId) {
		this.flightMsgEventId = flightMsgEventId;
	}

	/**
	 * @hibernate.property column = "FLT_SEG_ID"
	 * @return
	 */
	public Integer getFlightSegId() {
		return flightSegId;
	}

	/**
	 * @param flightId
	 *            the flightId to set
	 */
	public void setFlightSegId(Integer flightSegId) {
		this.flightSegId = flightSegId;
	}

	/**
	 * @hibernate.property column = "ANCI_NOTIFY_STATUS"
	 * @return
	 */
	public String getAncillaryNotifyStatus() {
		return ancillaryNotifyStatus;
	}

	/**
	 * @param ancillaryNotifyStatus
	 *            the ancillaryNotifyStatus to set
	 */
	public void setAncillaryNotifyStatus(String ancillaryNotifyStatus) {
		this.ancillaryNotifyStatus = ancillaryNotifyStatus;
	}

	/**
	 * @hibernate.property column = "FAILURE_REASON"
	 * @return
	 */
	public String getFailureReason() {
		return failureReason;
	}

	/**
	 * @param failureReason
	 *            the failureReason to set
	 */
	public void setFailureReason(String failureReason) {
		this.failureReason = failureReason;
	}

	/**
	 * @hibernate.property column = "TIMESTAMP"
	 * @return
	 */
	public Date getTimeStamp() {
		return timeStamp;
	}

	/**
	 * @param timeStamp
	 *            the timeStamp to set
	 */
	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}

	/**
	 * @hibernate.property column = "NO_OF_ATTEMPTS"
	 * @return
	 */
	public Integer getNoOtAttempts() {
		return noOtAttempts;
	}

	/**
	 * @param noOtAttempts
	 *            the noOtAttempts to set
	 */
	public void setNoOtAttempts(Integer noOtAttempts) {
		this.noOtAttempts = noOtAttempts;
	}

	/**
	 * @hibernate.property column = "START_TIMESTAMP"
	 * @return the startTimeStamp
	 */
	public Date getStartTimeStamp() {
		return startTimeStamp;
	}

	/**
	 * @param startTimeStamp
	 *            the startTimeStamp to set
	 */
	public void setStartTimeStamp(Date startTimeStamp) {
		this.startTimeStamp = startTimeStamp;
	}

	/**
	 * 
	 * @hibernate.property column = "END_TIMESTAMP"
	 * @return the endTimeStamp
	 */
	public Date getEndTimeStamp() {
		return endTimeStamp;
	}

	/**
	 * @param endTimeStamp
	 *            the endTimeStamp to set
	 */
	public void setEndTimeStamp(Date endTimeStamp) {
		this.endTimeStamp = endTimeStamp;
	}

	/**
	 * @hibernate.property column = "NO_OF_NOTIFICATIONS"
	 * @return the notificationCount
	 */
	public Integer getNotificationCount() {
		return notificationCount;
	}

	/**
	 * @param notificationCount
	 *            the notificationCount to set
	 */
	public void setNotificationCount(Integer notificationCount) {
		this.notificationCount = notificationCount;
	}

	/**
	 * @hibernate.property column = "NOTIFICATION_TYPE"
	 * @return the notification type
	 */
	public String getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}

	/**
	 * @hibernate.property column = "INS_FIRST_RESPONSE"
	 * @return the insFirstResponse
	 */
	public String getInsFirstResponse() {
		return insFirstResponse;
	}

	public void setInsFirstResponse(String insFirstResponse) {
		this.insFirstResponse = insFirstResponse;
	}

	/**
	 * @return Returns the blob.
	 * @hibernate.property column = "INS_SEC_RESPONSE"
	 */
	public Blob getInsSecResponse() {
		return insSecResponse;
	}

	public void setInsSecResponse(Blob insSecResponse) {
		this.insSecResponse = insSecResponse;
	}

}
