package com.isa.thinair.airschedules.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RouteInfoTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 271926989917153003L;

	private long id;

	private String ondCode;

	private String fromAirportCode;

	private String toAirportCode;

	private boolean isDirectRoute;

	private List<TransitAirport> transitAirportCodesList;

	/**
	 * @return Returns the fromAirportCode.
	 */
	public String getFromAirportCode() {
		return fromAirportCode;
	}

	/**
	 * @param fromAirportCode
	 *            The fromAirportCode to set.
	 */
	public void setFromAirportCode(String fromAirportCode) {
		this.fromAirportCode = fromAirportCode;
	}

	/**
	 * @return Returns the id.
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id
	 *            The id to set.
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return Returns the isDirectRoute.
	 */
	public boolean isDirectRoute() {
		return isDirectRoute;
	}

	/**
	 * @param isDirectRoute
	 *            The isDirectRoute to set.
	 */
	public void setDirectRoute(boolean isDirectRoute) {
		this.isDirectRoute = isDirectRoute;
	}

	/**
	 * @return Returns the ondCode.
	 */
	public String getOndCode() {
		return ondCode;
	}

	/**
	 * @param ondCode
	 *            The ondCode to set.
	 */
	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}

	/**
	 * @return Returns the toAirportCode.
	 */
	public String getToAirportCode() {
		return toAirportCode;
	}

	/**
	 * @param toAirportCode
	 *            The toAirportCode to set.
	 */
	public void setToAirportCode(String toAirportCode) {
		this.toAirportCode = toAirportCode;
	}

	/**
	 * @return Returns the transitAirportCodesList.
	 */
	public List<TransitAirport> getTransitAirportCodesList() {
		if (this.transitAirportCodesList == null) {
			this.transitAirportCodesList = new ArrayList<TransitAirport>();
		}
		return transitAirportCodesList;
	}

	/**
	 * @param transitAirportCodesList
	 *            The transitAirportCodesList to set.
	 */
	public void setTransitAirportCodesList(List<TransitAirport> transitAirportCodesList) {
		this.transitAirportCodesList = transitAirportCodesList;
	}

	/**
	 * 
	 * @param transitAirportCode
	 *            The transitAirportCode to set.
	 */
	public void addTransitAirportCode(String transitAirportCode, boolean isOrigin, String transitCityCode) {
		this.getTransitAirportCodesList().add(new TransitAirport(transitAirportCode, isOrigin, transitCityCode));
	}

	@Override
	public RouteInfoTO clone() {
		RouteInfoTO routeInfoTO = new RouteInfoTO();
		routeInfoTO.setDirectRoute(this.isDirectRoute());
		routeInfoTO.setFromAirportCode(this.getFromAirportCode());
		routeInfoTO.setId(this.getId());
		routeInfoTO.setOndCode(this.getOndCode());
		routeInfoTO.setToAirportCode(this.getToAirportCode());
		for (TransitAirport transitAirport : this.getTransitAirportCodesList()) {
			routeInfoTO.addTransitAirportCode(transitAirport.getAirportCode(), transitAirport.isOrigin(),
					transitAirport.getTransitCityCode());
		}
		return routeInfoTO;
	}

}
