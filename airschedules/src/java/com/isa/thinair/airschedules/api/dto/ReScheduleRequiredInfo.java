package com.isa.thinair.airschedules.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.SSMSplitFlightSchedule;
import com.isa.thinair.commons.api.dto.GDSStatusTO;

public class ReScheduleRequiredInfo implements Serializable {

	private static final long serialVersionUID = -3254340537557784901L;

	private Collection<String> actions;

	private Map<String, GDSStatusTO> gdsStatusMap;

	private Map<String, String> aircraftTypeMap;

	private String serviceType;

	private String airLineCode;

	private Collection<Integer> publishedGdsIds;

	private Map<String, Collection<String>> bcMap;

	private FlightSchedule updatedSchedule;

	private FlightSchedule existingSchedule;

	private Collection<SSMSplitFlightSchedule> existingSubSchedules;

	private Collection<SSMSplitFlightSchedule> updatedSubSchedules;

	private boolean cancelExistingSubSchedule;

	private String supplementaryInfo;
	
	private Collection<FlightSchedule> splitSchedules;
	
	private boolean cancelExistingSchedule;

	public Collection<String> getActions() {
		return actions;
	}

	public void setActions(Collection<String> actions) {
		this.actions = actions;
	}

	public void addAction(String aciton) {
		if (this.actions == null)
			this.actions = new ArrayList<String>();
		this.actions.add(aciton);
	}

	public boolean containsAction(String action) {
		return (this.actions != null) ? (this.actions.contains(action)) : false;
	}

	public FlightSchedule getExistingSchedule() {
		return existingSchedule;
	}

	public void setExistingSchedule(FlightSchedule existingSchedule) {
		this.existingSchedule = existingSchedule;
	}

	public FlightSchedule getUpdatedSchedule() {
		return updatedSchedule;
	}

	public void setUpdatedSchedule(FlightSchedule updatedSchedule) {
		this.updatedSchedule = updatedSchedule;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public Collection<SSMSplitFlightSchedule> getExistingSubSchedules() {
		return existingSubSchedules;
	}

	public void setExistingSubSchedules(Collection<SSMSplitFlightSchedule> existingSubSchedules) {
		this.existingSubSchedules = existingSubSchedules;
	}

	public Collection<SSMSplitFlightSchedule> getUpdatedSubSchedules() {
		return updatedSubSchedules;
	}

	public void setUpdatedSubSchedules(Collection<SSMSplitFlightSchedule> updatedSubSchedules) {
		this.updatedSubSchedules = updatedSubSchedules;
	}

	public Map<String, GDSStatusTO> getGdsStatusMap() {
		return gdsStatusMap;
	}

	public Map<String, String> getAircraftTypeMap() {
		return aircraftTypeMap;
	}

	public String getAirLineCode() {
		return airLineCode;
	}

	public Collection<Integer> getPublishedGdsIds() {
		return publishedGdsIds;
	}

	public Map<String, Collection<String>> getBcMap() {
		return bcMap;
	}

	public void setGdsStatusMap(Map<String, GDSStatusTO> gdsStatusMap) {
		this.gdsStatusMap = gdsStatusMap;
	}

	public void setAircraftTypeMap(Map<String, String> aircraftTypeMap) {
		this.aircraftTypeMap = aircraftTypeMap;
	}

	public void setAirLineCode(String airLineCode) {
		this.airLineCode = airLineCode;
	}

	public void setPublishedGdsIds(Collection<Integer> publishedGdsIds) {
		this.publishedGdsIds = publishedGdsIds;
	}

	public void setBcMap(Map<String, Collection<String>> bcMap) {
		this.bcMap = bcMap;
	}

	public boolean isCancelExistingSubSchedule() {
		return cancelExistingSubSchedule;
	}

	public void setCancelExistingSubSchedule(boolean cancelExistingSubSchedule) {
		this.cancelExistingSubSchedule = cancelExistingSubSchedule;
	}

	public String getSupplementaryInfo() {
		return supplementaryInfo;
	}

	public void setSupplementaryInfo(String supplementaryInfo) {
		this.supplementaryInfo = supplementaryInfo;
	}

	public Collection<FlightSchedule> getSplitSchedules() {
		return splitSchedules;
	}

	public void setSplitSchedules(Collection<FlightSchedule> splitSchedules) {
		this.splitSchedules = splitSchedules;
	}

	public boolean isCancelExistingSchedule() {
		return cancelExistingSchedule;
	}

	public void setCancelExistingSchedule(boolean cancelExistingSchedule) {
		this.cancelExistingSchedule = cancelExistingSchedule;
	}

}
