package com.isa.thinair.airschedules.api.dto;

import java.util.LinkedHashMap;

public class OverlapDeltaDTO {

	private LinkedHashMap<String,OverlapLegDeltaDTO> legDeltaMap = new LinkedHashMap<String,OverlapLegDeltaDTO>();

	private int startDateDelta = 0;

	public LinkedHashMap<String,OverlapLegDeltaDTO> getLegDeltaMap() {
		return legDeltaMap;
	}

	public void setLegDeltaMap(LinkedHashMap<String,OverlapLegDeltaDTO> legDeltaMap) {
		this.legDeltaMap = legDeltaMap;
	}

	public int getStartDateDelta() {
		return startDateDelta;
	}

	public void setStartDateDelta(int startDateDelta) {
		this.startDateDelta = startDateDelta;
	}
}
