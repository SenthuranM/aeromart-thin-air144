/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.api.dto;

import java.io.Serializable;

/**
 * data transfer object to hold the flight schedule details
 * 
 * @author Lasantha Pambagoda
 */
public class FlightSchedulesDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1677296886208144067L;

}
