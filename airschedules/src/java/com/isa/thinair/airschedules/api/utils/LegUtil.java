/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.api.utils;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightLeg;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.FlightScheduleLeg;
import com.isa.thinair.airschedules.api.model.Leg;
import com.isa.thinair.airschedules.api.model.SSMSplitFlightSchedule;
import com.isa.thinair.airschedules.api.model.SSMSplitFlightScheduleLeg;
import com.isa.thinair.airschedules.api.utils.ExternalConstants.ScheduleTimeFormat;

/**
 * utility class for schedule leg manipulation
 * 
 * @author Lasantha Pambagoda
 */
public class LegUtil {

	/**
	 * method to get the first leg of the given schedule leg list
	 * 
	 * @param legList
	 * @return flight schedule leg
	 */
	public static FlightScheduleLeg getFirstScheduleLeg(Set<FlightScheduleLeg> legList) {

		Iterator<FlightScheduleLeg> it = legList.iterator();
		FlightScheduleLeg leg = null;

		while (it.hasNext()) {

			leg = (FlightScheduleLeg) it.next();
			if (leg.getLegNumber() == 1)
				break;
		}

		return leg;
	}

	/**
	 * method to get last leg of the given schedule leg list
	 * 
	 * @param legList
	 * @return flight schedule leg
	 */
	public static FlightScheduleLeg getLastScheduleLeg(Set<FlightScheduleLeg> legList) {

		Iterator<FlightScheduleLeg> it = legList.iterator();
		FlightScheduleLeg leg = null;
		int legnum = 0;

		while (it.hasNext()) {

			FlightScheduleLeg currentLeg = (FlightScheduleLeg) it.next();
			if (legnum < currentLeg.getLegNumber()) {
				legnum = currentLeg.getLegNumber();
				leg = currentLeg;
			}
		}

		return leg;
	}

	/**
	 * method to get the first leg of the given Flight leg list
	 * 
	 * @param legList
	 * @return flight leg
	 */
	public static FlightLeg getFirstFlightLeg(Set<FlightLeg> legList) {

		Iterator<FlightLeg> it = legList.iterator();
		FlightLeg leg = null;

		while (it.hasNext()) {

			leg = (FlightLeg) it.next();
			if (leg.getLegNumber() == 1)
				break;
		}

		return leg;
	}

	/**
	 * method to get last leg of the given Flight leg list
	 * 
	 * @param legList
	 * @return flight leg
	 */
	public static FlightLeg getLastFlightLeg(Set<FlightLeg> legList) {

		Iterator<FlightLeg> it = legList.iterator();
		FlightLeg leg = null;
		int legnum = 0;

		while (it.hasNext()) {

			FlightLeg currentLeg = (FlightLeg) it.next();
			if (legnum < currentLeg.getLegNumber()) {
				legnum = currentLeg.getLegNumber();
				leg = currentLeg;
			}
		}

		return leg;
	}

	/**
	 * method to populate flight leg form schedule leg
	 * 
	 * @param flsLeg
	 * @return flight leg
	 */
	public static FlightLeg populateFlightLeg(FlightScheduleLeg flsLeg, FlightLeg fLeg) {

		fLeg.setModelRouteId(flsLeg.getModelRouteId());

		fLeg.setEstArrivalDayOffset(flsLeg.getEstArrivalDayOffset());
		fLeg.setEstArrivalTimeLocal(flsLeg.getEstArrivalTimeLocal());
		fLeg.setEstArrivalTimeZulu(flsLeg.getEstArrivalTimeZulu());
		fLeg.setEstArrivalDayOffsetLocal(flsLeg.getEstArrivalDayOffsetLocal());

		fLeg.setEstDepartureDayOffset(flsLeg.getEstDepartureDayOffset());
		fLeg.setEstDepartureTimeLocal(flsLeg.getEstDepartureTimeLocal());
		fLeg.setEstDepartureTimeZulu(flsLeg.getEstDepartureTimeZulu());
		fLeg.setEstDepartureDayOffsetLocal(flsLeg.getEstDepartureDayOffsetLocal());

		fLeg.setDestination(flsLeg.getDestination());
		fLeg.setDuration(flsLeg.getDuration());
		fLeg.setLegNumber(flsLeg.getLegNumber());
		fLeg.setOrigin(flsLeg.getOrigin());

		return fLeg;
	}

	// modified by Haider 10Aug08 create new instance of Date Object
	public static FlightScheduleLeg copyLeg(FlightScheduleLeg form, FlightScheduleLeg to) {

		// copy all the fealds of the leg
		to.setModelRouteId(form.getModelRouteId());

		to.setEstArrivalDayOffset(form.getEstArrivalDayOffset());
		Date tmp = form.getEstArrivalTimeLocal();
		to.setEstArrivalTimeLocal(tmp != null ? new Date(tmp.getTime()) : null);
		to.setEstArrivalTimeZulu(new Date(form.getEstArrivalTimeZulu().getTime()));
		to.setEstDepartureDayOffset(form.getEstDepartureDayOffset());
		tmp = form.getEstDepartureTimeLocal();
		to.setEstDepartureTimeLocal(tmp != null ? new Date(tmp.getTime()) : null);
		to.setEstDepartureTimeZulu(new Date(form.getEstDepartureTimeZulu().getTime()));
		to.setEstArrivalDayOffsetLocal(form.getEstArrivalDayOffsetLocal());
		to.setEstDepartureDayOffsetLocal(form.getEstDepartureDayOffsetLocal());

		to.setDestination(form.getDestination());
		to.setDuration(form.getDuration());
		to.setLegNumber(form.getLegNumber());
		to.setOrigin(form.getOrigin());

		return to;
	}

	public static FlightLeg copyLeg(FlightLeg form, FlightLeg to) {

		// copy all the fealds of the leg
		to.setModelRouteId(form.getModelRouteId());

		to.setEstArrivalDayOffset(form.getEstArrivalDayOffset());
		to.setEstArrivalTimeLocal(form.getEstArrivalTimeLocal());
		to.setEstArrivalTimeZulu(form.getEstArrivalTimeZulu());
		to.setEstDepartureDayOffset(form.getEstDepartureDayOffset());
		to.setEstDepartureTimeLocal(form.getEstDepartureTimeLocal());
		;
		to.setEstDepartureTimeZulu(form.getEstDepartureTimeZulu());
		to.setEstArrivalDayOffsetLocal(form.getEstArrivalDayOffsetLocal());
		to.setEstDepartureDayOffsetLocal(form.getEstDepartureDayOffsetLocal());

		to.setDestination(form.getDestination());
		to.setDuration(form.getDuration());
		to.setLegNumber(form.getLegNumber());
		to.setOrigin(form.getOrigin());

		return to;
	}

	/**
	 * public method to create a separate copy of flight segments
	 * 
	 * @param flightLegs
	 * @return
	 */
	public static Set<FlightLeg> cloneLegs(Set<FlightLeg> flightLegs) {

		Iterator<FlightLeg> legsIt = flightLegs.iterator();

		Set<FlightLeg> copedLegSet = new HashSet<FlightLeg>();
		FlightLeg leg;
		FlightLeg legCopy;

		while (legsIt.hasNext()) {

			leg = (FlightLeg) legsIt.next();
			legCopy = new FlightLeg();
			legCopy.setModelRouteId(new String(leg.getModelRouteId()));

			legCopy.setEstArrivalDayOffset(leg.getEstArrivalDayOffset());
			legCopy.setEstArrivalTimeZulu(new Date(leg.getEstArrivalTimeZulu().getTime()));
			legCopy.setEstDepartureDayOffset(leg.getEstDepartureDayOffset());
			legCopy.setEstDepartureTimeZulu(new Date(leg.getEstDepartureTimeZulu().getTime()));

			legCopy.setDestination(new String(leg.getDestination()));
			legCopy.setDuration(leg.getDuration());
			legCopy.setLegNumber(leg.getLegNumber());
			legCopy.setOrigin(new String(leg.getOrigin()));

			copedLegSet.add(legCopy);
		}

		return copedLegSet;
	}

	/**
	 * method to create flight legs for schedule legs
	 * 
	 * @param scheduleLegSet
	 * @return flight leg list
	 */
	public static Set<FlightLeg> createFlightLegs(Set<FlightScheduleLeg> scheduleLegSet) {

		Set<FlightLeg> legSet = new HashSet<FlightLeg>();
		Iterator<FlightScheduleLeg> scheduleLegIt = scheduleLegSet.iterator();

		while (scheduleLegIt.hasNext()) {

			FlightScheduleLeg flsLeg = (FlightScheduleLeg) scheduleLegIt.next();
			FlightLeg fLeg = LegUtil.populateFlightLeg(flsLeg, new FlightLeg());
			legSet.add(fLeg);
		}

		return legSet;
	}

	/**
	 * checks legs added or deleted
	 * 
	 * @param existing
	 * @param updated
	 * @return
	 */
	public static boolean checkLegsAddedDeleted(FlightSchedule existing, FlightSchedule updated) {

		int noOfExistingLegs = existing.getFlightScheduleLegs().size();
		int noOfUpdatedLegs = updated.getFlightScheduleLegs().size();
		Set<FlightScheduleLeg> existingLegs = existing.getFlightScheduleLegs();
		Set<FlightScheduleLeg> updatedLegs = updated.getFlightScheduleLegs();

		boolean legsOk = (noOfExistingLegs == 0 || noOfExistingLegs != noOfUpdatedLegs) ? false : true;
		if (!legsOk)
			return !legsOk;

		for (int i = 0; i < noOfExistingLegs; i++) {

			Iterator<FlightScheduleLeg> existLegsIt = existingLegs.iterator();
			FlightScheduleLeg existingLeg = null;

			boolean existingFound = false;
			while (existLegsIt.hasNext()) {

				existingLeg = (FlightScheduleLeg) existLegsIt.next();
				if (existingLeg.getLegNumber() == i + 1) {
					existingFound = true;
					break;
				}
			}

			Iterator<FlightScheduleLeg> updatedLegsIt = updatedLegs.iterator();
			FlightScheduleLeg updatedLeg = null;
			boolean updatedFound = false;

			while (updatedLegsIt.hasNext()) {

				updatedLeg = (FlightScheduleLeg) updatedLegsIt.next();
				if (updatedLeg.getLegNumber() == i + 1) {
					updatedFound = true;
					break;
				}
			}

			// check the while loop exit condition
			if (existingFound && updatedFound && (updatedLeg.getOrigin().equals(existingLeg.getOrigin()))
					&& (updatedLeg.getDestination().equals(existingLeg.getDestination()))) {

				legsOk = true;

			} else {

				legsOk = false;
			}

			if (!legsOk)
				break;
		}

		return !legsOk;
	}

	public static boolean isLegTimeChanged(FlightSchedule existing, FlightSchedule updated) {

		boolean islegTimeChanged = false;

		Set<FlightScheduleLeg> updaLegs = updated.getFlightScheduleLegs();
		Iterator<FlightScheduleLeg> updaIt = null;

		Set<FlightScheduleLeg> exisLegs = existing.getFlightScheduleLegs();
		Iterator<FlightScheduleLeg> exisLegsIt = exisLegs.iterator();
		while (exisLegsIt.hasNext()) {
			FlightScheduleLeg exisLeg = (FlightScheduleLeg) exisLegsIt.next();

			updaIt = updaLegs.iterator();
			while (updaIt.hasNext()) {

				FlightScheduleLeg updaLeg = (FlightScheduleLeg) updaIt.next();
				if (exisLeg.getLegNumber() == updaLeg.getLegNumber()) {

					if (!(exisLeg.getEstArrivalTimeZulu().getTime() == updaLeg.getEstArrivalTimeZulu().getTime()
							&& exisLeg.getEstDepartureTimeZulu().getTime() == updaLeg.getEstDepartureTimeZulu().getTime()
							&& exisLeg.getEstArrivalDayOffset() == updaLeg.getEstArrivalDayOffset() && exisLeg
							.getEstDepartureDayOffset() == updaLeg.getEstDepartureDayOffset())) {
						islegTimeChanged = true;
					}
					break;
				}
			}

			if (islegTimeChanged)
				break;
		}

		return islegTimeChanged;
	}

	public static boolean isLegTimeChanged(Flight existing, Flight updated) {

		boolean islegTimeChanged = false;

		Set<FlightLeg> updaLegs = updated.getFlightLegs();
		Iterator<FlightLeg> updaIt = null;

		Set<FlightLeg> exisLegs = existing.getFlightLegs();
		Iterator<FlightLeg> exisLegsIt = exisLegs.iterator();
		while (exisLegsIt.hasNext()) {
			FlightLeg exisLeg = (FlightLeg) exisLegsIt.next();

			updaIt = updaLegs.iterator();
			while (updaIt.hasNext()) {

				FlightLeg updaLeg = (FlightLeg) updaIt.next();
				if (exisLeg.getLegNumber() == updaLeg.getLegNumber()) {

					if (!(exisLeg.getEstArrivalTimeZulu().getTime() == updaLeg.getEstArrivalTimeZulu().getTime()
							&& exisLeg.getEstDepartureTimeZulu().getTime() == updaLeg.getEstDepartureTimeZulu().getTime()
							&& exisLeg.getEstArrivalDayOffset() == updaLeg.getEstArrivalDayOffset() && exisLeg
							.getEstDepartureDayOffset() == updaLeg.getEstDepartureDayOffset())) {
						islegTimeChanged = true;
					}
					break;
				}
			}

			if (islegTimeChanged)
				break;
		}

		return islegTimeChanged;
	}

	/**
	 * checks legs added or deleted
	 * 
	 * @param existing
	 * @param updated
	 * @return
	 */
	public static boolean checkLegsAddedDeleted(Flight existing, Flight updated) {

		int noOfExistingLegs = existing.getFlightLegs().size();
		int noOfUpdatedLegs = updated.getFlightLegs().size();
		Set<FlightLeg> existingLegs = existing.getFlightLegs();
		Set<FlightLeg> updatedLegs = updated.getFlightLegs();

		boolean legsOk = (noOfExistingLegs == 0 || noOfExistingLegs != noOfUpdatedLegs) ? false : true;
		if (!legsOk)
			return !legsOk;

		for (int i = 0; i < noOfExistingLegs; i++) {

			Iterator<FlightLeg> existLegsIt = existingLegs.iterator();
			FlightLeg existingLeg = null;

			boolean existingFound = false;
			while (existLegsIt.hasNext()) {

				existingLeg = (FlightLeg) existLegsIt.next();
				if (existingLeg.getLegNumber() == i + 1) {
					existingFound = true;
					break;
				}
			}

			Iterator<FlightLeg> updatedLegsIt = updatedLegs.iterator();
			FlightLeg updatedLeg = null;
			boolean updatedFound = false;

			while (updatedLegsIt.hasNext()) {

				updatedLeg = (FlightLeg) updatedLegsIt.next();
				if (updatedLeg.getLegNumber() == i + 1) {
					updatedFound = true;
					break;
				}
			}

			// check the while loop exit condition
			if (existingFound && updatedFound && (updatedLeg.getOrigin().equals(existingLeg.getOrigin()))
					&& (updatedLeg.getDestination().equals(existingLeg.getDestination()))) {

				legsOk = true;

			} else {

				legsOk = false;
			}

			if (!legsOk)
				break;
		}

		return !legsOk;
	}

	/**
	 * 
	 * @param legs
	 * @return
	 */
	public static String getLegInfo(Collection legs, String timeFormat) {

		StringBuffer legDetails = new StringBuffer();
		SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
		HashMap<Integer, String> legsInOrder = new HashMap<Integer, String>();

		Iterator itLegs = legs.iterator();

		while (itLegs.hasNext()) {
			Leg leg = (Leg) itLegs.next();

			StringBuffer legInfo = new StringBuffer();
			legInfo.append("( ");
			legInfo.append(leg.getOrigin() + " ");
			legInfo.append((timeFormat.equals(ScheduleTimeFormat.Zulu)) ? dateFormat.format(leg.getEstDepartureTimeZulu()) + " "
					: dateFormat.format(leg.getEstDepartureTimeLocal()) + " ");
			legInfo.append("Offset ");
			legInfo.append((timeFormat.equals(ScheduleTimeFormat.Zulu)) ? leg.getEstDepartureDayOffset() + " " : leg
					.getEstDepartureDayOffsetLocal() + " ");
			legInfo.append(" - ");
			legInfo.append(leg.getDestination() + " ");
			legInfo.append((timeFormat.equals(ScheduleTimeFormat.Zulu)) ? dateFormat.format(leg.getEstArrivalTimeZulu()) + " "
					: dateFormat.format(leg.getEstArrivalTimeLocal()) + " ");
			legInfo.append("Offset ");
			legInfo.append((timeFormat.equals(ScheduleTimeFormat.Zulu)) ? leg.getEstArrivalDayOffset() : leg
					.getEstArrivalDayOffsetLocal());
			legInfo.append(" ) ");

			legsInOrder.put(new Integer(leg.getLegNumber()), legInfo.toString());
		}

		for (int i = 1; i <= legsInOrder.size(); i++)
			legDetails.append(legsInOrder.get(new Integer(i)));

		return legDetails.toString();
	}
	
	public static void changeLegOffsetZulu(Set<FlightScheduleLeg> flightScheduleLegs, int changeByDays) {
		for (FlightScheduleLeg leg : flightScheduleLegs) {
			leg.setEstArrivalDayOffset(leg.getEstArrivalDayOffset() - changeByDays);
			leg.setEstDepartureDayOffset(leg.getEstDepartureDayOffset() - changeByDays);
		}
	}
	
	public static void changeLegOffsetLocal(Set<FlightScheduleLeg> flightScheduleLegs, int changeByDays) {
		for (FlightScheduleLeg leg : flightScheduleLegs) {
			leg.setEstArrivalDayOffsetLocal(leg.getEstArrivalDayOffsetLocal() - changeByDays);
			leg.setEstDepartureDayOffsetLocal(leg.getEstDepartureDayOffsetLocal() - changeByDays);
		}
	}
	
	public static boolean isLegLocalTimeChanged(SSMSplitFlightSchedule existing, SSMSplitFlightSchedule updated) {

		boolean islegTimeChanged = false;

		Set<SSMSplitFlightScheduleLeg> updaLegs = updated.getSsmSplitFlightScheduleLeg();
		Iterator<SSMSplitFlightScheduleLeg> updaIt = null;

		Set<SSMSplitFlightScheduleLeg> exisLegs = existing.getSsmSplitFlightScheduleLeg();
		Iterator<SSMSplitFlightScheduleLeg> exisLegsIt = exisLegs.iterator();
		while (exisLegsIt.hasNext()) {
			SSMSplitFlightScheduleLeg exisLeg = (SSMSplitFlightScheduleLeg) exisLegsIt.next();

			updaIt = updaLegs.iterator();
			while (updaIt.hasNext()) {

				SSMSplitFlightScheduleLeg updaLeg = (SSMSplitFlightScheduleLeg) updaIt.next();
				if (exisLeg.getLegNumber() == updaLeg.getLegNumber()) {

					if (!(exisLeg.getEstArrivalTimeLocal().getTime() == updaLeg.getEstArrivalTimeLocal().getTime()
							&& exisLeg.getEstDepartureTimeLocal().getTime() == updaLeg.getEstDepartureTimeLocal().getTime()
							&& exisLeg.getEstArrivalDayOffsetLocal() == updaLeg.getEstArrivalDayOffsetLocal() && exisLeg
								.getEstDepartureDayOffsetLocal() == updaLeg.getEstDepartureDayOffsetLocal())) {
						islegTimeChanged = true;
					}
					break;
				}
			}

			if (islegTimeChanged)
				break;
		}

		return islegTimeChanged;
	}
	
	public static Set<SSMSplitFlightScheduleLeg> adaptScheduleLegs(Set<FlightScheduleLeg> flightScheduleLegs) {
		Set<SSMSplitFlightScheduleLeg> ssmSplitScheduleLegs = new HashSet<SSMSplitFlightScheduleLeg>();
		if (flightScheduleLegs != null && !flightScheduleLegs.isEmpty()) {
			for (FlightScheduleLeg leg : flightScheduleLegs) {
				SSMSplitFlightScheduleLeg to = new SSMSplitFlightScheduleLeg();
				to.setModelRouteId(leg.getModelRouteId());

				to.setEstArrivalDayOffset(leg.getEstArrivalDayOffset());
				Date tmp = leg.getEstArrivalTimeLocal();
				to.setEstArrivalTimeLocal(tmp != null ? new Date(tmp.getTime()) : null);
				to.setEstArrivalTimeZulu(new Date(leg.getEstArrivalTimeZulu().getTime()));
				to.setEstDepartureDayOffset(leg.getEstDepartureDayOffset());
				tmp = leg.getEstDepartureTimeLocal();
				to.setEstDepartureTimeLocal(tmp != null ? new Date(tmp.getTime()) : null);
				to.setEstDepartureTimeZulu(new Date(leg.getEstDepartureTimeZulu().getTime()));
				to.setEstArrivalDayOffsetLocal(leg.getEstArrivalDayOffsetLocal());
				to.setEstDepartureDayOffsetLocal(leg.getEstDepartureDayOffsetLocal());

				to.setDestination(leg.getDestination());
				to.setDuration(leg.getDuration());
				to.setLegNumber(leg.getLegNumber());
				to.setOrigin(leg.getOrigin());

				ssmSplitScheduleLegs.add(to);

			}
		}
		return ssmSplitScheduleLegs;
	}
	
	public static boolean isLegLocalTimeChanged(FlightSchedule existing, FlightSchedule updated) {

		boolean islegTimeChanged = false;

		Set<FlightScheduleLeg> updaLegs = updated.getFlightScheduleLegs();
		Iterator<FlightScheduleLeg> updaIt = null;

		Set<FlightScheduleLeg> exisLegs = existing.getFlightScheduleLegs();
		Iterator<FlightScheduleLeg> exisLegsIt = exisLegs.iterator();
		while (exisLegsIt.hasNext()) {
			FlightScheduleLeg exisLeg = (FlightScheduleLeg) exisLegsIt.next();

			updaIt = updaLegs.iterator();
			while (updaIt.hasNext()) {

				FlightScheduleLeg updaLeg = (FlightScheduleLeg) updaIt.next();
				if (exisLeg.getLegNumber() == updaLeg.getLegNumber()) {

					if (!(exisLeg.getEstArrivalTimeLocal().getTime() == updaLeg.getEstArrivalTimeLocal().getTime()
							&& exisLeg.getEstDepartureTimeLocal().getTime() == updaLeg.getEstDepartureTimeLocal().getTime()
							&& exisLeg.getEstArrivalDayOffsetLocal() == updaLeg.getEstArrivalDayOffsetLocal() && exisLeg
							.getEstDepartureDayOffsetLocal() == updaLeg.getEstDepartureDayOffsetLocal())) {
						islegTimeChanged = true;
					}
					break;
				}
			}

			if (islegTimeChanged)
				break;
		}

		return islegTimeChanged;
	}
	
	public static boolean isLegLocalTimeChanged(Flight existing, Flight updated) {

		boolean islegTimeChanged = false;

		Set<FlightLeg> updaLegs = updated.getFlightLegs();
		Iterator<FlightLeg> updaIt = null;

		Set<FlightLeg> exisLegs = existing.getFlightLegs();
		Iterator<FlightLeg> exisLegsIt = exisLegs.iterator();
		while (exisLegsIt.hasNext()) {
			FlightLeg exisLeg = (FlightLeg) exisLegsIt.next();

			updaIt = updaLegs.iterator();
			while (updaIt.hasNext()) {

				FlightLeg updaLeg = (FlightLeg) updaIt.next();
				if (exisLeg.getLegNumber() == updaLeg.getLegNumber()) {

					if (!(exisLeg.getEstArrivalTimeLocal().getTime() == updaLeg.getEstArrivalTimeLocal().getTime()
							&& exisLeg.getEstDepartureTimeLocal().getTime() == updaLeg.getEstDepartureTimeLocal().getTime()
							&& exisLeg.getEstArrivalDayOffsetLocal() == updaLeg.getEstArrivalDayOffsetLocal() && exisLeg
							.getEstDepartureDayOffsetLocal() == updaLeg.getEstDepartureDayOffsetLocal())) {
						islegTimeChanged = true;
					}
					break;
				}
			}

			if (islegTimeChanged)
				break;
		}

		return islegTimeChanged;
	}
	
	public static boolean checkFlightVsScheduleLegsAddedDeleted(Flight existing, FlightSchedule updated) {

		int noOfExistingLegs = existing.getFlightLegs().size();
		int noOfUpdatedLegs = updated.getFlightScheduleLegs().size();
		Set<FlightLeg> existingLegs = existing.getFlightLegs();
		Set<FlightScheduleLeg> updatedLegs = updated.getFlightScheduleLegs();

		boolean legsOk = (noOfExistingLegs == 0 || noOfExistingLegs != noOfUpdatedLegs) ? false : true;
		if (!legsOk)
			return !legsOk;

		for (int i = 0; i < noOfExistingLegs; i++) {

			Iterator<FlightLeg> existLegsIt = existingLegs.iterator();
			FlightLeg existingLeg = null;

			boolean existingFound = false;
			while (existLegsIt.hasNext()) {

				existingLeg = (FlightLeg) existLegsIt.next();
				if (existingLeg.getLegNumber() == i + 1) {
					existingFound = true;
					break;
				}
			}

			Iterator<FlightScheduleLeg> updatedLegsIt = updatedLegs.iterator();
			FlightScheduleLeg updatedLeg = null;
			boolean updatedFound = false;

			while (updatedLegsIt.hasNext()) {

				updatedLeg = (FlightScheduleLeg) updatedLegsIt.next();
				if (updatedLeg.getLegNumber() == i + 1) {
					updatedFound = true;
					break;
				}
			}

			// check the while loop exit condition
			if (existingFound && updatedFound && (updatedLeg.getOrigin().equals(existingLeg.getOrigin()))
					&& (updatedLeg.getDestination().equals(existingLeg.getDestination()))) {

				legsOk = true;

			} else {

				legsOk = false;
			}

			if (!legsOk)
				break;
		}

		return !legsOk;
	}
	
}
