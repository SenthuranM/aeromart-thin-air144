/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.api.dto;

/**
 * @author Lasantha Pambagoda
 */
public class SeatDistribution {

	private String bookingClassCode;

	private int noOfSeats;

	/**
	 * @return Returns the bookingClassCode.
	 */
	public String getBookingClassCode() {
		return bookingClassCode;
	}

	/**
	 * @param bookingClassCode
	 *            The bookingClassCode to set.
	 */
	public void setBookingClassCode(String bookingClassCode) {
		this.bookingClassCode = bookingClassCode;
	}

	/**
	 * @return Returns the noOfSeats.
	 */
	public int getNoOfSeats() {
		return noOfSeats;
	}

	/**
	 * @param noOfSeats
	 *            The noOfSeats to set.
	 */
	public void setNoOfSeats(int noOfSeats) {
		this.noOfSeats = noOfSeats;
	}
}
