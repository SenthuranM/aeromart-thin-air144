/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.api.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Lasantha Pambagoda
 * 
 */
public class FlightSummaryDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6213438435959278397L;

	private int flightId;

	private Date depatureDate;

	private String flightNumber;

	public FlightSummaryDTO(int flightId, Date depatureDate) {
		this.flightId = flightId;
		this.depatureDate = depatureDate;
	}

	public FlightSummaryDTO(int flightId, Date depatureDate, String flightNumber) {
		this.flightId = flightId;
		this.depatureDate = depatureDate;
		this.flightNumber = flightNumber;
	}

	/**
	 * @return Returns the depatureDate.
	 */
	public Date getDepatureDate() {
		return depatureDate;
	}

	/**
	 * @param depatureDate
	 *            The depatureDate to set.
	 */
	public void setDepatureDate(Date depatureDate) {
		this.depatureDate = depatureDate;
	}

	/**
	 * @return Returns the flightId.
	 */
	public int getFlightId() {
		return flightId;
	}

	/**
	 * @param flightId
	 *            The flightId to set.
	 */
	public void setFlightId(int flightId) {
		this.flightId = flightId;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

}
