/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.api.dto;

import java.io.Serializable;

import com.isa.thinair.airschedules.api.model.Flight;

/**
 * dto to represent flights to display
 * 
 * @author Lasantha Pambagoda
 */
public class DisplayFlightDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -308163649360877080L;

	private Flight flight;

	private int seatsSold = 0;

	private int onHold = 0;

	private int allocated = 0;

	private boolean reservatinsExists = false;

	private int stndBy = 0;

	private int checkedIn = 0;

	public DisplayFlightDTO(Flight flight) {
		this.flight = flight;
	}

	/**
	 * @return Returns the flight.
	 */
	public Flight getFlight() {
		return flight;
	}

	/**
	 * @param flight
	 *            The flight to set.
	 */
	public void setFlight(Flight flight) {
		this.flight = flight;
	}

	/**
	 * @return Returns the seatsSold.
	 */
	public int getSeatsSold() {
		return seatsSold;
	}

	/**
	 * @param seatsSold
	 *            The seatsSold to set.
	 */
	public void setSeatsSold(int seatsSold) {
		this.seatsSold = seatsSold;
	}

	/**
	 * @return Returns the allocated.
	 */
	public int getAllocated() {
		return allocated;
	}

	/**
	 * @param allocated
	 *            The allocated to set.
	 */
	public void setAllocated(int allocated) {
		this.allocated = allocated;
	}

	/**
	 * @return Returns the onHold.
	 */
	public int getOnHold() {
		return onHold;
	}

	/**
	 * @param onHold
	 *            The onHold to set.
	 */
	public void setOnHold(int onHold) {
		this.onHold = onHold;
	}

	/**
	 * @return Returns the reservatinsExists.
	 */
	public boolean getReservatinsExists() {
		return reservatinsExists;
	}

	/**
	 * @param reservatinsExists
	 *            The reservatinsExists to set.
	 */
	public void setReservatinsExists(boolean reservatinsExists) {
		this.reservatinsExists = reservatinsExists;
	}

	/**
	 * @return Returns the stndBy.
	 */
	public int getStndBy() {
		return stndBy;
	}

	/**
	 * @param stndBy
	 *            The stndBy to set.
	 */

	public void setStndBy(int stndBy) {
		this.stndBy = stndBy;
	}

	/**
	 * @return Returns the checkedIn.
	 */
	public int getCheckedIn() {
		return checkedIn;
	}

	/**
	 * @param checkedIn
	 *            The checkedIn to set.
	 */
	public void setCheckedIn(int checkedIn) {
		this.checkedIn = checkedIn;
	}

}
