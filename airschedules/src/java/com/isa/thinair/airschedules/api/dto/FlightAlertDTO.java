/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.api.dto;

import java.io.Serializable;
import java.util.Set;

/**
 * @author
 */
public class FlightAlertDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4106064078493031447L;

	private boolean alertForCancelledFlight = false;

	private boolean alertForRescheduledFlight = false;

	private boolean emailForCancelledFlight = false;

	private boolean emailForRescheduledFlight = false;

	private boolean sendEmailsForFlightAlterations = false;

	private boolean sendSMSForFlightAlterations = false;

	private boolean sendSMSForConfirmedBookings = false;

	private boolean sendSMSForOnHoldBookings = false;

	private boolean sendEmailsForRescheduledFlight = false;

	private boolean sendSMSForRescheduledFlight = false;

	private boolean sendEmailToOriginAgent = false;

	private boolean sendEmailToOwnerAgent = false;

	private boolean sendEmailWhenSeatLost = false;

	private String emailTo;

	private String emailSubject;

	private String emailContent;

	private String visibleIbeOnly;

	private String transferSelectedFlights;

	private Set<Integer> fligthSegIds;

	public String getVisibleIbeOnly() {
		return visibleIbeOnly;
	}

	public void setVisibleIbeOnly(String visibleIbeOnly) {
		this.visibleIbeOnly = visibleIbeOnly;
	}

	public String getTransferSelectedFlights() {
		return transferSelectedFlights;
	}

	public void setTransferSelectedFlights(String transferSelectedFlights) {
		this.transferSelectedFlights = transferSelectedFlights;
	}

	public Set<Integer> getFligthSegIds() {
		return fligthSegIds;
	}

	public void setFligthSegIds(Set<Integer> fligthSegIds) {
		this.fligthSegIds = fligthSegIds;
	}

	public boolean isAlertForCancelledFlight() {
		return alertForCancelledFlight;
	}

	public void setAlertForCancelledFlight(boolean alertForCancelledFlight) {
		this.alertForCancelledFlight = alertForCancelledFlight;
	}

	public boolean isAlertForRescheduledFlight() {
		return alertForRescheduledFlight;
	}

	public void setAlertForRescheduledFlight(boolean alertForRescheduledFlight) {
		this.alertForRescheduledFlight = alertForRescheduledFlight;
	}

	public boolean isEmailForCancelledFlight() {
		return emailForCancelledFlight;
	}

	public void setEmailForCancelledFlight(boolean emailForCancelledFlight) {
		this.emailForCancelledFlight = emailForCancelledFlight;
	}

	public boolean isEmailForRescheduledFlight() {
		return emailForRescheduledFlight;
	}

	public void setEmailForRescheduledFlight(boolean emailForRescheduledFlight) {
		this.emailForRescheduledFlight = emailForRescheduledFlight;
	}

	public String getEmailTo() {
		return emailTo;
	}

	public void setEmailTo(String emailTo) {
		this.emailTo = emailTo;
	}

	public String getEmailSubject() {
		return emailSubject;
	}

	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}

	public String getEmailContent() {
		return emailContent;
	}

	public void setEmailContent(String emailContent) {
		this.emailContent = emailContent;
	}

	public boolean isSendEmailsForFlightAlterations() {
		return sendEmailsForFlightAlterations;
	}

	public void setSendEmailsForFlightAlterations(boolean sendEmailsForFlightAlterations) {
		this.sendEmailsForFlightAlterations = sendEmailsForFlightAlterations;
	}

	public boolean isSendSMSForFlightAlterations() {
		return sendSMSForFlightAlterations;
	}

	public void setSendSMSForFlightAlterations(boolean sendSMSForFlightAlterations) {
		this.sendSMSForFlightAlterations = sendSMSForFlightAlterations;
	}

	public boolean isSendSMSForConfirmedBookings() {
		return sendSMSForConfirmedBookings;
	}

	public void setSendSMSForConfirmedBookings(boolean sendSMSForConfirmedBookings) {
		this.sendSMSForConfirmedBookings = sendSMSForConfirmedBookings;
	}

	public boolean isSendSMSForOnHoldBookings() {
		return sendSMSForOnHoldBookings;
	}

	public void setSendSMSForOnHoldBookings(boolean sendSMSForOnHoldBookings) {
		this.sendSMSForOnHoldBookings = sendSMSForOnHoldBookings;
	}

	/**
	 * @return Returns the sendEmailsForRescheduledFlight.
	 */
	public boolean isSendEmailsForRescheduledFlight() {
		return sendEmailsForRescheduledFlight;
	}

	/**
	 * @param sendEmailsForRescheduledFlight
	 *            The sendEmailsForRescheduledFlight to set.
	 */
	public void setSendEmailsForRescheduledFlight(boolean sendEmailsForRescheduledFlight) {
		this.sendEmailsForRescheduledFlight = sendEmailsForRescheduledFlight;
	}

	/**
	 * @return Returns the sendSMSForRescheduledFlight.
	 */
	public boolean isSendSMSForRescheduledFlight() {
		return sendSMSForRescheduledFlight;
	}

	/**
	 * @param sendSMSForRescheduledFlight
	 *            The sendSMSForRescheduledFlight to set.
	 */
	public void setSendSMSForRescheduledFlight(boolean sendSMSForRescheduledFlight) {
		this.sendSMSForRescheduledFlight = sendSMSForRescheduledFlight;
	}

	/**
	 * @return the sendEmailToOriginAgent
	 */
	public boolean isSendEmailToOriginAgent() {
		return sendEmailToOriginAgent;
	}

	/**
	 * @param sendEmailToOriginAgent
	 *            the sendEmailToOriginAgent to set
	 */
	public void setSendEmailToOriginAgent(boolean sendEmailToOriginAgent) {
		this.sendEmailToOriginAgent = sendEmailToOriginAgent;
	}

	/**
	 * @return the sendEmailToOwnerAgent
	 */
	public boolean isSendEmailToOwnerAgent() {
		return sendEmailToOwnerAgent;
	}

	/**
	 * @param sendEmailToOwnerAgent
	 *            the sendEmailToOwnerAgent to set
	 */
	public void setSendEmailToOwnerAgent(boolean sendEmailToOwnerAgent) {
		this.sendEmailToOwnerAgent = sendEmailToOwnerAgent;
	}

	public boolean isSendEmailWhenSeatLost() {
		return sendEmailWhenSeatLost;
	}

	public void setSendEmailWhenSeatLost(boolean sendEmailWhenSeatLost) {
		this.sendEmailWhenSeatLost = sendEmailWhenSeatLost;
	}

}
