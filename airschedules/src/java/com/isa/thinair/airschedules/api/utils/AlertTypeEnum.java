package com.isa.thinair.airschedules.api.utils;

import java.io.Serializable;

/**
 * @author Lasantha Pambagoda
 */
public class AlertTypeEnum implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -150914622633832173L;
	private int code;

	private AlertTypeEnum(int code) {
		this.code = code;
	}

	public boolean equals(AlertTypeEnum alertType) {
		return (alertType.code == this.code);
	}

	public int getCode() {
		return code;
	}

	public static final AlertTypeEnum ALERT_FLIGHT_CANCEL = new AlertTypeEnum(1);
	public static final AlertTypeEnum ALERT_DST_CHANGED = new AlertTypeEnum(2);
}
