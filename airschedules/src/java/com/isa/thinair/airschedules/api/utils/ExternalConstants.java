/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.api.utils;

/**
 * Class to keep constants related to AirSchedule. The Constants defined in this class should only use within the module
 * 
 * @author Lasantha Pambagoda
 */
public abstract class ExternalConstants {

	public interface ScheduleTimeFormat {

		public static String Zulu = "zulu";

		public static String Local = "local";

	}
}
