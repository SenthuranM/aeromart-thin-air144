/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.api.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.isa.thinair.airinventory.api.dto.InterceptingFlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.SegmentDTO;
import com.isa.thinair.airschedules.api.dto.FlightScheduleInfoDTO;
import com.isa.thinair.airschedules.api.dto.FlightScheduleLegInfoDTO;
import com.isa.thinair.airschedules.api.dto.FlightScheduleSegmentInfoDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightLeg;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.FlightScheduleLeg;
import com.isa.thinair.airschedules.api.model.FlightScheduleSegment;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.model.FlySegement;

/**
 * utility class to manipulate schedule segments
 * 
 * @author Lasantha Pambagoda
 */
public class SegmentUtil {

	/**
	 * method to populate flight segment from schedule segments
	 * 
	 * @param flsSeg
	 * @return flight segment
	 */
	public static FlightSegement populateFlightSegment(FlightScheduleSegment flsSeg, FlightSegement fSeg) {

		fSeg.setSegmentCode(flsSeg.getSegmentCode());
		fSeg.setValidFlag(flsSeg.getValidFlag());
		fSeg.setArrivalTerminalId(flsSeg.getArrivalTerminalId());
		fSeg.setDepartureTerminalId(flsSeg.getDepartureTerminalId());
		return fSeg;
	}

	/**
	 * mehtod to create flight segment list form flight schedule segments
	 * 
	 * @param scheduleSegSet
	 * @return flight segment list
	 */
	public static Set<FlightSegement> createFlightSegments(Set<FlightScheduleSegment> scheduleSegSet) {

		Set<FlightSegement> segmentSet = new HashSet<FlightSegement>();
		for (FlightScheduleSegment flsSeg : scheduleSegSet) {
			FlightSegement fSeg = populateFlightSegment(flsSeg, new FlightSegement());
			segmentSet.add(fSeg);
		}

		return segmentSet;
	}

	/**
	 * method to copy flight schedule segment form given segment
	 * 
	 * @param from
	 * @param to
	 * @return
	 */
	public static FlightScheduleSegment copySegment(FlightScheduleSegment from, FlightScheduleSegment to) {

		// copy all the fields in the flight segment
		to.setSegmentCode(from.getSegmentCode());
		to.setValidFlag(from.getValidFlag());
		to.setArrivalTerminalId(from.getArrivalTerminalId());
		to.setDepartureTerminalId(from.getDepartureTerminalId());
		return to;
	}

	/**
	 * method to copy flight schedule segment form given segment
	 * 
	 * @param from
	 * @param to
	 * @return
	 */
	public static FlightSegement copySegment(FlightSegement from, FlightSegement to) {

		// copy all the fields in the flight segment
		to.setSegmentCode(from.getSegmentCode());
		to.setValidFlag(from.getValidFlag());
		to.setArrivalTerminalId(from.getArrivalTerminalId());
		to.setDepartureTerminalId(from.getDepartureTerminalId());

		return to;
	}

	/**
	 * 
	 * @param from
	 * @param to
	 * @return
	 */
	public static Set<FlightSegement> cloneSegments(Collection<FlightSegement> flightSegments) {

		Set<FlightSegement> newFlightSegments = new HashSet<FlightSegement>();

		for (FlightSegement flightSegment : flightSegments) {
			FlightSegement newFlightSegment = new FlightSegement();

			if (flightSegment.getFltSegId() != null) {
				newFlightSegment.setFltSegId(new Integer(flightSegment.getFltSegId().intValue()));
			}

			if (flightSegment.getFlightId() != null) {
				newFlightSegment.setFlightId(new Integer(flightSegment.getFlightId().intValue()));
			}

			if (flightSegment.getSegmentCode() != null) {
				newFlightSegment.setSegmentCode(new String(flightSegment.getSegmentCode()));
			}

			newFlightSegment.setValidFlag(flightSegment.getValidFlag());
			newFlightSegment.setEstTimeArrivalZulu(flightSegment.getEstTimeArrivalZulu());
			newFlightSegment.setEstTimeDepatureZulu(flightSegment.getEstTimeDepatureZulu());
			newFlightSegment.setEstTimeArrivalLocal(flightSegment.getEstTimeArrivalLocal());
			newFlightSegment.setEstTimeDepatureLocal(flightSegment.getEstTimeDepatureLocal());
			// clone terminal ids.
			newFlightSegment.setDepartureTerminalId(flightSegment.getDepartureTerminalId());
			newFlightSegment.setArrivalTerminalId(flightSegment.getArrivalTerminalId());

			if (flightSegment.getOverlapSegmentId() != null) {
				newFlightSegment.setOverlapSegmentId(new Integer(flightSegment.getOverlapSegmentId().intValue()));
			}

			newFlightSegments.add(newFlightSegment);
		}

		return newFlightSegments;
	}

	/**
	 * method to create segemnt dto list to send to the inventry
	 * 
	 * @param flightId
	 * @param segments
	 * @param overlapFlightID
	 * @param overlapSegments
	 * @return segmentDTO list
	 */
	public static Collection<SegmentDTO> createSegmentDTOList(int flightId, Collection<FlightSegement> segments,
			Integer overlapFlightID, Collection<FlightSegement> overlapSegments) {

		Collection<SegmentDTO> segmentDTOList = new ArrayList<SegmentDTO>();

		if (segments != null) {

			for (FlightSegement outerSeg : segments) {
				SegmentDTO segmetDTO = new SegmentDTO();
				// segment code of the segment
				String segmentCode = outerSeg.getSegmentCode();

				// setting the segment dto
				segmetDTO.setSegmentCode(segmentCode);
				segmetDTO.setSegmentId(outerSeg.getFltSegId().intValue());
				segmetDTO.setInvalidSegment(!outerSeg.getValidFlag());

				// setting the segment dto's overlapping segs
				Collection<InterceptingFlightSegmentDTO> interseptingSegs = new ArrayList<InterceptingFlightSegmentDTO>();

				// get the intercepting segments
				for (FlightSegement innerSeg : segments) {
					if (isIntercepts(segmentCode, innerSeg.getSegmentCode())) {
						InterceptingFlightSegmentDTO intersepDTO = new InterceptingFlightSegmentDTO();
						intersepDTO.setFlightId(flightId);
						intersepDTO.setInterceptingSegCode(innerSeg.getSegmentCode());
						interseptingSegs.add(intersepDTO);
					}
				}

				if (overlapFlightID != null && overlapSegments != null) {

					// get the intercepting segments of overlapping flight
					for (FlightSegement innerSeg : overlapSegments) {
						if (isIntercepts(segmentCode, innerSeg.getSegmentCode())) {
							InterceptingFlightSegmentDTO intersepDTO = new InterceptingFlightSegmentDTO();
							intersepDTO.setFlightId(overlapFlightID.intValue());
							intersepDTO.setInterceptingSegCode(innerSeg.getSegmentCode());
							interseptingSegs.add(intersepDTO);
						}
					}
				}

				// set the intersepting seg list
				segmetDTO.setInterceptingFlightSeg(interseptingSegs);
				segmetDTO.setFlightId(flightId);
				segmentDTOList.add(segmetDTO);
			}
		}
		return segmentDTOList;

	}

	public static Collection<SegmentDTO> getSegmentDTOs(Collection<Flight> flights) {
		Collection<SegmentDTO> segmentDTOList = new ArrayList<SegmentDTO>();
		for (Flight flight : flights) {
			Collection<SegmentDTO> perFlightSDTOs = createSegmentDTOList(flight.getFlightId(), flight.getFlightSegements(), null,
					null);
			segmentDTOList.addAll(perFlightSDTOs);
		}
		return segmentDTOList;
	}

	private static boolean isIntercepts(String sourceSegCode, String targetSegCode) {
		if (sourceSegCode.equals(targetSegCode)) {
			return false;
		}
		String[] targetStations = StringUtils.split(targetSegCode, "/");

		for (int i = 0; i < (targetStations.length - 1); i++) {
			String segmentCode = targetStations[i] + "/" + targetStations[i + 1];
			if (sourceSegCode.indexOf(segmentCode) != -1) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Method to get the flight segment Ids
	 * 
	 * @param flightSegs
	 * @return
	 */
	public static List<Integer> getFlightSegIds(Collection<FlightSegement> flightSegs) {

		List<Integer> idList = new ArrayList<Integer>();
		for (FlightSegement flightSeg : flightSegs) {
			idList.add(flightSeg.getFltSegId());
		}
		return idList;
	}

	/**
	 * Method to get HashMap of the flight segment Ids and segment codes
	 * 
	 * @param flightSegs
	 * @return
	 */
	public static HashMap<Integer, String> getFlightSegIdsAndCodes(Collection<FlightSegement> flightSegs) {

		HashMap<Integer, String> segmentIdCodeList = new HashMap<Integer, String>();
		for (FlightSegement flightSeg : flightSegs) {
			segmentIdCodeList.put(flightSeg.getFltSegId(), flightSeg.getSegmentCode());
		}
		return segmentIdCodeList;
	}

	/**
	 * 
	 * @param leg
	 * @param schedule
	 * @return
	 */
	public static boolean isOverlappingLeg(FlightScheduleLeg leg, FlightSchedule schedule) {

		Set<FlightScheduleSegment> scheduleSegs = schedule.getFlightScheduleSegments();
		String segmentCode = leg.getOrigin() + "/" + leg.getDestination();
		for (FlightScheduleSegment segment : scheduleSegs) {
			if (segment.getSegmentCode().equals(segmentCode) && segment.isOverlapping()) {
				return true;
			}
		}

		return false;
	}

	/**
	 * 
	 * @param leg
	 * @param schedule
	 * @return
	 */
	public static boolean isOverlappingLeg(FlightScheduleLegInfoDTO leg, FlightScheduleInfoDTO schedule) {

		Set<FlightScheduleSegmentInfoDTO> scheduleSegs = schedule.getFlightScheduleSegments();
		String segmentCode = leg.getOrigin() + "/" + leg.getDestination();
		for (FlightScheduleSegmentInfoDTO segment : scheduleSegs) {
			if (segment.getSegmentCode().equals(segmentCode) && segment.isOverlapping()) {
				return true;
			}
		}

		return false;
	}

	/**
	 * 
	 * @param leg
	 * @param flight
	 * @return
	 */
	public static boolean isOverlappingLeg(FlightLeg leg, Flight flight) {

		Set<FlightSegement> flightSegs = flight.getFlightSegements();
		String segmentCode = leg.getOrigin() + "/" + leg.getDestination();
		for (FlightSegement segment : flightSegs) {
			if (segment.getSegmentCode().equals(segmentCode) && segment.isOverlapping()) {
				return true;
			}
		}

		return false;
	}

	/**
	 * 
	 * @param set
	 * @return
	 */
	public static FlightSegement getDetailsOfLongestSegment(Set<FlightSegement> set) {
		String strSeg = null;
		int prevLen = 0;
		FlightSegement flightSeg = null;
		if (set != null) {
			for (FlightSegement seg : set) {
				strSeg = seg.getSegmentCode();
				if (prevLen < strSeg.length()) {
					prevLen = strSeg.length();
					flightSeg = seg;
				}
			}
		}
		return flightSeg;
	}

	/**
	 * 
	 * @param segments
	 * @return
	 */
	public static String getSegmentAuditInfo(Collection<? extends FlySegement> segments) {

		StringBuffer segmentDetails = new StringBuffer();
		String details = "";
		for (FlySegement segment : segments) {
			segmentDetails.append(segment.getSegmentCode());
			segmentDetails.append(" ");
			segmentDetails.append((segment.getValidFlag() ? "Y" : "N"));
			segmentDetails.append(", ");
		}
		if (segmentDetails.lastIndexOf(",") > 0) {
			details = segmentDetails.substring(0, segmentDetails.lastIndexOf(","));
		} else {
			details = segmentDetails.toString();
		}

		return details;
	}

	/**
	 * 
	 * @param segments
	 * @param affectedSegments
	 * @return
	 */
	public static String getSegmentAlertInfo(Collection<FlightSegement> segments, Collection<FlightSegement> affectedSegments) {

		StringBuffer segmentDetails = new StringBuffer();
		String details = "";
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		for (FlightSegement segment : segments) {
			boolean affectedFound = false;
			if (affectedSegments != null) {
				for (FlightSegement affectedSegment : affectedSegments) {
					if (segment.getFltSegId().intValue() == affectedSegment.getFltSegId().intValue()) {
						affectedFound = true;
					}
				}
			}
			if (affectedFound) {
				segmentDetails.append(segment.getSegmentCode());
				segmentDetails.append(" ");
				segmentDetails.append(dateFormat.format(segment.getEstTimeDepatureLocal()));
				segmentDetails.append("/");
				segmentDetails.append(dateFormat.format(segment.getEstTimeArrivalLocal()));
				segmentDetails.append(" ,");
			}
		}
		if (segmentDetails.lastIndexOf(",") > 0) {
			details = segmentDetails.substring(0, segmentDetails.lastIndexOf(","));
		} else {
			details = segmentDetails.toString();
		}

		return details;
	}

	/**
	 * 
	 * @param segments
	 * @return
	 */
	public static String getSegmentAlertInfo(Collection<FlightSegement> segments) {

		StringBuffer segmentDetails = new StringBuffer();
		String details = "";
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		for (FlightSegement segment : segments) {
			segmentDetails.append(segment.getSegmentCode());
			segmentDetails.append(" ");
			segmentDetails.append(dateFormat.format(segment.getEstTimeDepatureLocal()));
			segmentDetails.append("/");
			segmentDetails.append(dateFormat.format(segment.getEstTimeArrivalLocal()));
		}

		if (segmentDetails.lastIndexOf(",") > 0) {
			details = segmentDetails.substring(0, segmentDetails.lastIndexOf(","));
		} else {
			details = segmentDetails.toString();
		}

		return details;
	}

	/**
	 * public method to return updated flight segments Nature of update could be 1. with delay/advance for
	 * departure/arrival time 2. deleted segments
	 * 
	 * @param flight
	 * @param updatedFlight
	 * @return list of affected segment ids
	 */
	public static List<FlightSegement>
			getAffectedFlightSegments(Set<FlightSegement> oldSegments, Set<FlightSegement> newSegments) {

		List<FlightSegement> affectedSegs = new ArrayList<FlightSegement>();
		boolean segmentFound = false;
		for (FlightSegement flightSeg : oldSegments) {
			segmentFound = false;
			for (FlightSegement updFlightSeg : newSegments) {
				if (flightSeg.getSegmentCode().equals(updFlightSeg.getSegmentCode())) {
					segmentFound = true;
					if (flightSeg.getEstTimeDepatureZulu().getTime() != updFlightSeg.getEstTimeDepatureZulu().getTime()
							|| flightSeg.getEstTimeArrivalZulu().getTime() != updFlightSeg.getEstTimeArrivalZulu().getTime()) {
						affectedSegs.add(flightSeg);
						break;
					}
				}
			}
			// removed leg found
			if (!segmentFound) {
				affectedSegs.add(flightSeg);
			}
		}

		return affectedSegs;
	}

	/**
	 * 
	 * @param existingSegs
	 * @param updatedSegents
	 * @return
	 */
	public static List<FlySegement> getInvalidatedSegments(Set<? extends FlySegement> existingSegs,
			Set<? extends FlySegement> updatedSegents) {
		List<FlySegement> invalidSegments = new ArrayList<FlySegement>();
		for (FlySegement updatedSeg : updatedSegents) {
			for (FlySegement existingSeg : existingSegs) {
				if (existingSeg.getSegmentCode().equals(updatedSeg.getSegmentCode())) {
					if (!updatedSeg.getValidFlag() && existingSeg.getValidFlag()) {
						invalidSegments.add(existingSeg);
					}
					break;
				}
			}
		}

		return invalidSegments;
	}

	// TODO -- doesn't support a 3-char carrier code at the moment
	public static List<String> getPrioratizedFlightNumbers(String flightNumber) {

		int carrierCodeLength = 2;
		int flightNumberFixedLength = 4;
		List<String> flightNumbers = new ArrayList<String>();
		String carrierCode = flightNumber.substring(0, carrierCodeLength);
		String flightNumberPart = flightNumber.substring(carrierCodeLength);
		String flightNumberOption;

		flightNumbers.add(flightNumber); // exact match highest priority

		String flightNumberPaddingRemoved = flightNumberPart.replaceFirst("^0+", "");

		for (int a = flightNumberPaddingRemoved.length(); a <= flightNumberFixedLength; a++ ) {
			flightNumberOption = carrierCode + String.format("%1$" + a + "s", flightNumberPaddingRemoved).replaceAll(" ", "0");
			if (!flightNumber.equals(flightNumberOption)) {
				flightNumbers.add(flightNumberOption);
			}
		}

		return flightNumbers;
	}
	
	public static FlightScheduleSegment getDetailsOfLongestFlightScheduleSegment(Set<FlightScheduleSegment> set) {
		String strSeg = null;
		int prevLen = 0;
		FlightScheduleSegment flightSeg = null;
		if (set != null) {
			for (FlightScheduleSegment seg : set) {
				strSeg = seg.getSegmentCode();
				if (prevLen < strSeg.length()) {
					prevLen = strSeg.length();
					flightSeg = seg;
				}
			}
		}
		return flightSeg;
	}

}
