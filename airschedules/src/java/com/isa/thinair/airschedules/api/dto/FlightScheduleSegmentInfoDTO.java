package com.isa.thinair.airschedules.api.dto;

import java.io.Serializable;

public class FlightScheduleSegmentInfoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2006280505453269848L;

	private Integer flSchSegId;

	private Integer overlapSegmentId;

	private String segmentCode;

	private boolean validFlag = true;

	private Integer scheduleId;

	/**
	 * @return the scheduleId
	 */
	public Integer getScheduleId() {
		return scheduleId;
	}

	/**
	 * @param scheduleId
	 *            the scheduleId to set
	 */
	public void setScheduleId(Integer scheduleId) {
		this.scheduleId = scheduleId;
	}

	public FlightScheduleSegmentInfoDTO() {
	}

	/**
	 * @return the flSchSegId
	 */
	public Integer getFlSchSegId() {
		return flSchSegId;
	}

	/**
	 * @param flSchSegId
	 *            the flSchSegId to set
	 */
	public void setFlSchSegId(Integer flSchSegId) {
		this.flSchSegId = flSchSegId;
	}

	/**
	 * @return the overlapSegmentId
	 */
	public Integer getOverlapSegmentId() {
		return overlapSegmentId;
	}

	/**
	 * @param overlapSegmentId
	 *            the overlapSegmentId to set
	 */
	public void setOverlapSegmentId(Integer overlapSegmentId) {
		this.overlapSegmentId = overlapSegmentId;
	}

	/**
	 * check is overlapping
	 * 
	 * @return boolean
	 */
	public boolean isOverlapping() {
		return (overlapSegmentId == null) ? false : true;
	}

	/**
	 * @return the segmentCode
	 */
	public String getSegmentCode() {
		return segmentCode;
	}

	/**
	 * @param segmentCode
	 *            the segmentCode to set
	 */
	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	/**
	 * @return the validFlag
	 */
	public boolean isValidFlag() {
		return validFlag;
	}

	/**
	 * @param validFlag
	 *            the validFlag to set
	 */
	public void setValidFlag(boolean validFlag) {
		this.validFlag = validFlag;
	}
}
