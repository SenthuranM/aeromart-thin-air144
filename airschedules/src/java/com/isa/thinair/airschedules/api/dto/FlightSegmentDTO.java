/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.api.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Lasantha Pambagoda
 */
public class FlightSegmentDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5970488804533399022L;

	private Integer flightId;

	private String flightNumber;

	private String segmentCode;

	private Date estTimeDepatureLocal;

	private Date estTimeArrivalLocal;

	private Integer flightSegId;

	private String departureTimeLocal;
	
	private String arrivalTimeLocal;

	private String operatingAirline;

	/**
	 * @return Returns the estTimeArrivalLocal.
	 */
	public Date getEstTimeArrivalLocal() {
		return estTimeArrivalLocal;
	}

	/**
	 * @param estTimeArrivalLocal
	 *            The estTimeArrivalLocal to set.
	 */
	public void setEstTimeArrivalLocal(Date estTimeArrivalLocal) {
		this.estTimeArrivalLocal = estTimeArrivalLocal;
	}

	/**
	 * @return Returns the estTimeDepatureLocal.
	 */
	public Date getEstTimeDepatureLocal() {
		return estTimeDepatureLocal;
	}

	/**
	 * @param estTimeDepatureLocal
	 *            The estTimeDepatureLocal to set.
	 */
	public void setEstTimeDepatureLocal(Date estTimeDepatureLocal) {
		this.estTimeDepatureLocal = estTimeDepatureLocal;
	}

	/**
	 * @return Returns the flightId.
	 */
	public Integer getFlightId() {
		return flightId;
	}

	/**
	 * @param flightId
	 *            The flightId to set.
	 */
	public void setFlightId(Integer flightId) {
		this.flightId = flightId;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * @return Returns the segmentCode.
	 */
	public String getSegmentCode() {
		return segmentCode;
	}

	/**
	 * @param segmentCode
	 *            The segmentCode to set.
	 */
	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	/**
	 * @return the flightSegId
	 */
	public Integer getFlightSegId() {
		return flightSegId;
	}

	/**
	 * @param flightSegId
	 *            the flightSegId to set
	 */
	public void setFlightSegId(Integer flightSegId) {
		this.flightSegId = flightSegId;
	}

	public String getDepartureTimeLocal() {
		return departureTimeLocal;
	}

	public void setDepartureTimeLocal(String departureTimeLocal) {
		this.departureTimeLocal = departureTimeLocal;
	}

	public String getArrivalTimeLocal() {
		return arrivalTimeLocal;
	}

	public void setArrivalTimeLocal(String arrivalTimeLocal) {
		this.arrivalTimeLocal = arrivalTimeLocal;
	}

	public String getOperatingAirline() {
		return operatingAirline;
	}

	public void setOperatingAirline(String operatingAirline) {
		this.operatingAirline = operatingAirline;
	}
}
