/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.api.utils;

/**
 * constant file for model names and there field names
 * 
 * @author Lasantha Pambagoda
 */
public abstract class ModelNamesAndFieldNames {

	public static interface Flight {

		public static final String NAME = "Flight";

		public static interface FieldNames {

			public static final String FLIGHT_ID = "flightId";

			public static final String FLIGHT_NUMBER = "flightNumber";

			public static final String DEPARTURE_DATE = "departureDate";

			public static final String DEPARTURE_DATE_LOCAL = "departureDateLocal";

			public static final String AVAILABLE_SEAT_KILOMETERS = "availableSeatKilometers";

			public static final String OVERLAPING_FLIGHT_ID = "overlapingFlightId";

			public static final String TAIL_NUMBER = "tailNumber";

			public static final String MODEL_NUMBER = "modelNumber";

			public static final String STATUS = "status";

			public static final String DAY_NUMBER = "dayNumber";

			public static final String OPERATION_TYPE_ID = "operationTypeId";

			public static final String ORIGIN_APT_CODE = "originAptCode";

			public static final String DESTINATION_APT_CODE = "destinationAptCode";

			public static final String SCHEDULE_ID = "scheduleId";

			public static final String MANNUALLY_CHANGED = "manuallyChanged";

			public static final String FLIGHT_LEGS = "flightLegs";

			public static final String FLIGHT_SEGMENTS = "flightSegements";

			public static final String CHECKIN_STATUS = "checkinStatus";

			public static final String FLIGHT_TYPE = "flightType";
		}
	}

	public static interface FlightSchedule {

		public static final String NAME = "FlightSchedule";

		public static interface FieldNames {

			public static final String SCHEDULE_ID = "scheduleId";

			public static final String FLIGHT_NUMBER = "flightNumber";

			public static final String START_DATE = "startDate";

			public static final String STOP_DATE = "stopDate";

			public static final String START_DATE_LOCAL = "startDateLocal";

			public static final String STOP_DATE_LOCAL = "stopDateLocal";

			public static final String OVERLAPPING_SCHEDULE_ID = "overlapingScheduleId";

			public static final String AVAILABLE_SEAT_KILOMETERS = "availableSeatKilometers";

			public static final String NUMBER_OF_DEPATRURES = "numberOfDepartures";

			public static final String DEPARTURE_APT_CODE = "departureStnCode";

			public static final String ARRIVAL_APT_CODE = "arrivalStnCode";

			public static final String MODEL_NUMBER = "modelNumber";

			public static final String OPERATION_TYPE_ID = "operationTypeId";

			public static final String BUILD_STATUS_CODE = "buildStatusCode";

			public static final String STATUS_CODE = "statusCode";

			public static final String FREQUENCY = "frequency";

			public static final String FREQUENCY_LOCAL = "frequencyLocal";

			public static final String FLIGHT_SCHEDULE_LEGS = "flightScheduleLegs";

			public static final String FLIGHT_SCHEDULE_SEGMENT = "flightScheduleSegments";

			public static final String MANUALLY_CHANGED = "manuallyChanged";

			public static final String FLIGHT_TYPE = "flightType";
		}
	}

	public static interface FlightSegment {

		public static final String NAME = "FlightSegement";

		public static interface FieldNames {

			public static final String SEGMENT_CODE = "segmentCode";

		}
	}

}
