package com.isa.thinair.airschedules.api.dto;

import java.io.Serializable;

public class FlightBasicInfoDTO implements Serializable {

	private static final long serialVersionUID = -1L;
	
	
	private String flightNumber;
	
	private String departureDate;
	
	private String segment;
	

	public String getFlightNumber() {
		return flightNumber;
	}


	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}


	public String getDepartureDate() {
		return departureDate;
	}


	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}


	public String getSegment() {
		return segment;
	}


	public void setSegment(String segment) {
		this.segment = segment;
	}


}