package com.isa.thinair.airschedules.api.utils;

import java.util.Iterator;
import java.util.Set;

import com.isa.thinair.airmaster.api.model.AirportTerminal;
import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * Airport Terminal Utility Class. Adds terminal information to the segments of a flight.
 * 
 * @author sanjaya
 */
public class AirportTerminalUtil {

	private AirportBD airportBD;

	public AirportTerminalUtil(AirportBD airportBD) {
		this.airportBD = airportBD;
	}

	/**
	 * Adds the terminal information to the flight segments.
	 * 
	 * @param flight
	 * @return
	 * @throws ModuleException
	 */
	public Flight addTerminalInformation(Flight flight) throws ModuleException {

		Set<FlightSegement> flightSegements = flight.getFlightSegements();

		Iterator<FlightSegement> it = flightSegements.iterator();
		while (it.hasNext()) {

			FlightSegement segement = (FlightSegement) it.next();
			String segementCode = segement.getSegmentCode();
			String[] segementCodeArr = segementCode.split("/");
			if (null == segement.getArrivalTerminalId()) {
				String arrivalAirportCode = segementCodeArr[segementCodeArr.length - 1];
				AirportTerminal arrivalTerminal = airportBD.getDefaultTerminal(arrivalAirportCode);

				if (arrivalTerminal != null) {
					segement.setArrivalTerminalId(arrivalTerminal.getTerminalId());
				}
			}
			if (null == segement.getDepartureTerminalId()) {
				String departureAirportCode = segementCodeArr[0];
				AirportTerminal departureTerminal = airportBD.getDefaultTerminal(departureAirportCode);
				if (departureTerminal != null) {
					segement.setDepartureTerminalId(departureTerminal.getTerminalId());
				}
			}

		}
		return flight;
	}
}
