package com.isa.thinair.airschedules.api.utils;

public class GDSSchedConstants {

	public interface TargetLogicNames {

		public static final String PUBLISH_SSM_NEW = "publishSSMNew";

		public static final String PUBLISH_ADD_REMOVE_SSM = "publishAddRemoveSSM";

		public static final String PUBLISH_SSM_CANCEL = "publishSSMCancel";

		public static final String PUBLISH_AMMEND_SCHEDULE = "publishUpdateSSM";

		public static final String PUBLISH_ASM_NEW = "publishASMNew";

		public static final String PUBLISH_ADD_REMOVE_ASM = "publishAddRemoveASM";

		public static final String PUBLISH_ASM_CANCEL = "publishASMCancel";

		public static final String PUBLISH_AMMEND_FLIGHT = "publishUpdateASM";
		
		public static final String PUBLISH_DST_AMMEND_SCHEDULE = "publishSSMDSTUpdate";
		
		public static final String PUBLISH_SCHEDULE_SPLIT = "publishSSMSplitSchedule";
	}

	public interface ParamNames {

		public static final String SCHEDULE_ID_LIST = "scheduleIdList";

		public static final String SCHEDULE_ID = "scheduleId";

		public static final String SCHEDULE = "schedule";

		public static final String OVERLAPPING_SCHEDULE = "overlappingSchedule";

		public static final String FORCE = "force";

		public static final String SERVICE_TYPE = "serviceType";

		public static final String AIRCRAFT_TYPE_MAP = "aircraftTypeMap";

		public static final String AIRCRAFT_TYPE = "aircraftType";

		public static final String GDS_EVENT_COLLECTOR = "gdsEventCollector";

		public static final String FLIGHT = "flight";

		public static final String OVERLAPPING_FLIGHT = "overlappingFlight";

		public static String FLIGHT_IDS_MAP_FOR_SCHEDULES = "flightIdsMapForSchedules";

		public static final String CARRIER_CODE = "carrierCode";
		
		public static String EXISITNG_SSM_SUB_SCHEDULES = "existingSubSchedules";
		
		public static String NEW_SSM_SUB_SCHEDULES = "newSubSchedules";	
		
		public static String IS_SCHEDULE_PERIOD_CHANGE = "isPeriodChange";	
		
		public static String FLIGHT_SCHEDULE = "flightSchedule";

		public static final String SUPPLEMENTARY_INFO = "supplementaryInfo";
		
		public static String NEW_SCHEDULE_IDS_FROM_SPLIT = "newSheduleIDsFromSplit";
		
		public static String SSIM_RECORD_DTO = "ssimRecordDTO";	
	}
}
