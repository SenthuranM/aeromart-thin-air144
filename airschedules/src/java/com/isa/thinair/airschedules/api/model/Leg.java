/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:17:34
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.api.model;

import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * class to represent the Leg
 * 
 * @author Lasantha Pambagoda
 */
public class Leg extends Persistent {

	// public constants
	/** Constant LEG_NUMBER_1 */
	public static final int LEG_NUMBER_1 = 1;

	/** Constant LEG_NUMBER_2 */
	public static final int LEG_NUMBER_2 = 2;

	/** Constant LEG_NUMBER_3 */
	public static final int LEG_NUMBER_3 = 3;

	/** Constant LEG_NUMBER_4 */
	public static final int LEG_NUMBER_4 = 4;

	/** Constant LEG_NUMBER_5 */
	public static final int LEG_NUMBER_5 = 5;

	/** Constant LEG_NUMBER_6 */
	public static final int LEG_NUMBER_6 = 6;

	/** Constant LEG_NUMBER_7 */
	public static final int LEG_NUMBER_7 = 7;

	/** Constant LEG_NUMBER_8 */
	public static final int LEG_NUMBER_8 = 8;

	/** Constant LEG_NUMBER_9 */
	public static final int LEG_NUMBER_9 = 9;

	// private fields
	private int legNumber;

	private Date estDepartureTimeZulu;

	private Date estArrivalTimeZulu;

	private Date estDepartureTimeLocal;

	private Date estArrivalTimeLocal;

	private int duration;

	private int estDepartureDayOffset;

	private int estArrivalDayOffset;

	private int estDepartureDayOffsetLocal;

	private int estArrivalDayOffsetLocal;

	private String origin;

	private String destination;

	private String modelRouteId;

	private int durationError;

	/**
	 * @return Returns the durationError.
	 */
	public int getDurationError() {
		return durationError;
	}

	/**
	 * @param durationError
	 *            The durationError to set.
	 */
	public void setDurationError(int durationError) {
		this.durationError = durationError;
	}

	/**
	 * returns the legNumber
	 * 
	 * @return Returns the legNumber.
	 * @hibernate.property column = "LEG_NUMBER"
	 */
	public int getLegNumber() {
		return legNumber;
	}

	/**
	 * sets the legNumber
	 * 
	 * @param legNumber
	 *            The legNumber to set.
	 */
	public void setLegNumber(int legNumber) {
		this.legNumber = legNumber;
	}

	/**
	 * returns the duration
	 * 
	 * @return Returns the duration.
	 * @hibernate.property column = "DURATION"
	 */
	public int getDuration() {
		return duration;
	}

	/**
	 * sets the duration
	 * 
	 * @param duration
	 *            The duration to set.
	 */
	public void setDuration(int duration) {
		this.duration = duration;
	}

	/**
	 * returns the destination
	 * 
	 * @return destination
	 * @hibernate.property column = "DESTINATION"
	 */
	public String getDestination() {
		return destination;
	}

	/**
	 * sets the destination
	 * 
	 * @param destination
	 */
	public void setDestination(String destination) {
		this.destination = destination;
	}

	/**
	 * returns the origin
	 * 
	 * @return origin
	 * @hibernate.property column = "ORIGIN"
	 */
	public String getOrigin() {
		return origin;
	}

	/**
	 * sets the origin
	 * 
	 * @param origin
	 */
	public void setOrigin(String origin) {
		this.origin = origin;
	}

	/**
	 * @return Returns the modelRouteId.
	 * @hibernate.property column = "ROUTE_ID"
	 */
	public String getModelRouteId() {
		return modelRouteId;
	}

	/**
	 * @param modelRouteId
	 *            The modelRouteId to set.
	 */
	public void setModelRouteId(String modelRouteId) {
		this.modelRouteId = modelRouteId;
	}

	/**
	 * @return Returns the estArrivalDayOffset.
	 * @hibernate.property column = "EST_ARRIVAL_DAY_OFFSET"
	 */
	public int getEstArrivalDayOffset() {
		return estArrivalDayOffset;
	}

	/**
	 * @param estArrivalDayOffset
	 *            The estArrivalDayOffset to set.
	 */
	public void setEstArrivalDayOffset(int estArrivalDayOffset) {
		this.estArrivalDayOffset = estArrivalDayOffset;
	}

	/**
	 * @return Returns the estArrivalTimeLocal.
	 */
	public Date getEstArrivalTimeLocal() {
		return estArrivalTimeLocal;
	}

	/**
	 * @param estArrivalTimeLocal
	 *            The estArrivalTimeLocal to set.
	 */
	public void setEstArrivalTimeLocal(Date estArrivalTimeLocal) {
		this.estArrivalTimeLocal = estArrivalTimeLocal;
	}

	/**
	 * @return Returns the estArrivalTimeZulu.
	 * @hibernate.property column = "EST_TIME_ARRIVAL_ZULU"
	 */
	public Date getEstArrivalTimeZulu() {
		return estArrivalTimeZulu;
	}

	/**
	 * @param estArrivalTimeZulu
	 *            The estArrivalTimeZulu to set.
	 */
	public void setEstArrivalTimeZulu(Date estArrivalTimeZulu) {
		this.estArrivalTimeZulu = estArrivalTimeZulu;
	}

	/**
	 * @return Returns the estDepartureDayOffset.
	 * @hibernate.property column = "EST_DEPARTURE_DAY_OFFSET"
	 */
	public int getEstDepartureDayOffset() {
		return estDepartureDayOffset;
	}

	/**
	 * @param estDepartureDayOffset
	 *            The estDepartureDayOffset to set.
	 */
	public void setEstDepartureDayOffset(int estDepartureDayOffset) {
		this.estDepartureDayOffset = estDepartureDayOffset;
	}

	/**
	 * @return Returns the estDepartureTimeLocal.
	 */
	public Date getEstDepartureTimeLocal() {
		return estDepartureTimeLocal;
	}

	/**
	 * @param estDepartureTimeLocal
	 *            The estDepartureTimeLocal to set.
	 */
	public void setEstDepartureTimeLocal(Date estDepartureTimeLocal) {
		this.estDepartureTimeLocal = estDepartureTimeLocal;
	}

	/**
	 * @return Returns the estDepartureTimeZulu.
	 * @hibernate.property column = "EST_TIME_DEPARTURE_ZULU"
	 */
	public Date getEstDepartureTimeZulu() {
		return estDepartureTimeZulu;
	}

	/**
	 * @param estDepartureTimeZulu
	 *            The estDepartureTimeZulu to set.
	 */
	public void setEstDepartureTimeZulu(Date estDepartureTimeZulu) {
		this.estDepartureTimeZulu = estDepartureTimeZulu;
	}

	/**
	 * @return Returns the estArrivalDayOffsetLocal.
	 */
	public int getEstArrivalDayOffsetLocal() {
		return estArrivalDayOffsetLocal;
	}

	/**
	 * @param estArrivalDayOffsetLocal
	 *            The estArrivalDayOffsetLocal to set.
	 */
	public void setEstArrivalDayOffsetLocal(int estArrivalDayOffsetLocal) {
		this.estArrivalDayOffsetLocal = estArrivalDayOffsetLocal;
	}

	/**
	 * @return Returns the estDepartureDayOffsetLocal.
	 */
	public int getEstDepartureDayOffsetLocal() {
		return estDepartureDayOffsetLocal;
	}

	/**
	 * @param estDepartureDayOffsetLocal
	 *            The estDepartureDayOffsetLocal to set.
	 */
	public void setEstDepartureDayOffsetLocal(int estDepartureDayOffsetLocal) {
		this.estDepartureDayOffsetLocal = estDepartureDayOffsetLocal;
	}
}
