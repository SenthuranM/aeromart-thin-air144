package com.isa.thinair.airschedules.api.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import com.isa.thinair.airschedules.api.model.Flight;

public class GDSFlightEventCollector implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -158481039463428382L;
	public static final String DATE_CHANGED = "DATE_CHANGED";
	public static final String FLIGHT_NUMBER_CHANGED = "FLIGHT_NUMBER_CHANGED";
	public static final String MODEL_CHANGED = "MODEL_CHANGED";
	public static final String LEG_TIME_CHANGE = "LEG_TIME_CHANGE";
	public static final String LEG_ADDED_DELETED = "LEG_ADDED_DELETED";
	public static final String GDS_ADDED = "GDS_ADDED";
	public static final String GDS_DELETED = "GDS_DELETED";
	public static final String REPLACE_EXISTING_INFO = "REPLACE_EXISTING_INFO";
	public static final String DATE_CHANGED_LOCAL_TIME = "DATE_CHANGED_LOCAL_TIME";
	public static final String EXTERNAL_SSM_RECAP = "EXTERNAL_SSM_RECAP";

	// param names for rev

	private Collection<String> actions;

	private Flight existingFlight;

	private Flight existingOverlapFlight;

	private Flight updatedFlight;

	private Flight updatedOverlapFlight;

	private Collection<Integer> addedGDSList;

	private Collection<Integer> deletedGDSList;

	private String serviceType;
	
	private boolean closeFlight;
	
	private boolean reopenFlight;	
	
	private String externalSitaAddress;
	
	private String externalEmailAddress;
	
	private boolean externalSSMRecap;

	private String supplementaryInfo;

	public Collection<String> getActions() {
		return actions;
	}

	public void setActions(Collection<String> actions) {
		this.actions = actions;
	}

	public void addAction(String aciton) {
		if (this.actions == null)
			this.actions = new ArrayList<String>();
		this.actions.add(aciton);
	}

	public Flight getExistingFlight() {
		return existingFlight;
	}

	public void setExistingFlight(Flight existingFlight) {
		this.existingFlight = existingFlight;
	}

	public Flight getExistingOverlapFlight() {
		return existingOverlapFlight;
	}

	public void setExistingOverlapFlight(Flight existingOverlapFlight) {
		this.existingOverlapFlight = existingOverlapFlight;
	}

	public Flight getUpdatedFlight() {
		return updatedFlight;
	}

	public void setUpdatedFlight(Flight updatedFlight) {
		this.updatedFlight = updatedFlight;
	}

	public Flight getUpdatedOverlapFlight() {
		return updatedOverlapFlight;
	}

	public void setUpdatedOverlapFlight(Flight updatedOverlapFlight) {
		this.updatedOverlapFlight = updatedOverlapFlight;
	}

	public Collection<Integer> getAddedGDSList() {
		return addedGDSList;
	}

	public void setAddedGDSList(Collection<Integer> addedGDSList) {
		this.addedGDSList = addedGDSList;
	}

	public Collection<Integer> getDeletedGDSList() {
		return deletedGDSList;
	}

	public void setDeletedGDSList(Collection<Integer> deletedGDSList) {
		this.deletedGDSList = deletedGDSList;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public boolean containsAction(String action) {
		return (this.actions != null) ? (this.actions.contains(action)) : false;
	}

	public boolean isCloseFlight() {
		return closeFlight;
	}

	public void setCloseFlight(boolean closeFlight) {
		this.closeFlight = closeFlight;
	}
	
	public boolean isReopenFlight() {
		return reopenFlight;
	}

	public void setReopenFlight(boolean reopenFlight) {
		this.reopenFlight = reopenFlight;
	}

	public String getExternalSitaAddress() {
		return externalSitaAddress;
	}

	public void setExternalSitaAddress(String externalSitaAddress) {
		this.externalSitaAddress = externalSitaAddress;
	}

	public String getExternalEmailAddress() {
		return externalEmailAddress;
	}

	public void setExternalEmailAddress(String externalEmailAddress) {
		this.externalEmailAddress = externalEmailAddress;
	}

	public boolean isExternalSSMRecap() {
		return externalSSMRecap;
	}

	public void setExternalSSMRecap(boolean externalSSMRecap) {
		this.externalSSMRecap = externalSSMRecap;
	}

	public String getSupplementaryInfo() {
		return supplementaryInfo;
	}

	public void setSupplementaryInfo(String supplementaryInfo) {
		this.supplementaryInfo = supplementaryInfo;
	}
}
