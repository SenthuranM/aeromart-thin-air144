/**
 * 
 */
package com.isa.thinair.airschedules.api.utils;

/**
 * @author Indika Athauda
 * 
 */
public class AirScheduleCustomConstants {

	public static interface OperationTypes {

		public static int CHARTER = 1;

		public static int STANDARD = 2;

		public static int WET_LEASE = 3;

		public static int PSEDUO = 4;

		public static int BUS_SERVICE = 5;

		public static int OPERATIONS = 6;
	}
	
	public static interface SegmentTypes {

		public static int BUS_SEGMENT = 1;

		public static int AIR_SEGMENT = 2;
	}
}
