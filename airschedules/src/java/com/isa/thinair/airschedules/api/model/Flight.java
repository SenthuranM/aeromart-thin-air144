/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:17:16
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.api.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.isa.thinair.airschedules.api.utils.FlightUtil;
import com.isa.thinair.commons.core.framework.Tracking;

/**
 * Flight is the entity class to repesent a Flight model
 * 
 * @author Code Generation Tool, Lasantha Pambagoda
 * @version Ver 1.0.0
 * @hibernate.class table = "T_FLIGHT"
 */
public class Flight extends Tracking implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 166306164246990796L;

	// public constants
	/** Constant to represent the Sunday */
	public static final String DAY_OF_DEPARTURE_SUN = "SUN";

	/** Constant to represent the Monday */
	public static final String DAY_OF_DEPARTURE_MON = "MON";

	/** Constant to represent the Tuesday */
	public static final String DAY_OF_DEPARTURE_TUE = "TUE";

	/** Constant to represent the Wednesday */
	public static final String DAY_OF_DEPARTURE_WED = "WED";

	/** Constant to represent the Thuesday */
	public static final String DAY_OF_DEPARTURE_THU = "THU";

	/** Constant to represent the Friday */
	public static final String DAY_OF_DEPARTURE_FRI = "FRI";

	/** Constant to represent the Saturday */
	public static final String DAY_OF_DEPARTURE_SAT = "SAT";

	/* Constants to represent meal notification status. */
	public static final String MEAL_NOTIFICATION_NOT_SENT = "N";
	public static final String MEAL_NOTIFICATION_SENT = "S";
	public static final String MEAL_NOTIFICATION_FAILED = "F";
	public static final String MEAL_NOTIFICATION_EMPTY = "E";

	public static final String FLIGHT_ACTIVE = "ACT";

	// private variables
	private Integer flightId;

	private String flightNumber;

	private Date departureDate;

	private Date departureDateLocal;

	private Integer availableSeatKilometers;

	private Integer overlapingFlightId;

	private String tailNumber;

	private String modelNumber;

	private String modelDesc;

	private String oldModelNumber;

	private String status;

	private int dayNumber;

	private int operationTypeId;

	private String originAptCode;

	private String destinationAptCode;

	private Integer scheduleId;

	private boolean manuallyChanged;

	private Set<FlightLeg> flightLegs;

	private Set<FlightSegement> flightSegements;

	private Set<Integer> gdsIds;

	private String mealNotificationStatus = MEAL_NOTIFICATION_NOT_SENT; // Default value.
	private Integer mealNotificationAttempts = 0;

	private String flightType;

	private String remarks;

	private Integer mealTemplateId;

	private Integer seatChargeTemplateId;

	private Integer baggabeTemplateId;

	private String mealTemplAssignStatus;

	private String seatTemplAssignStatus;

	private String mealTemplAssignErrorDesc;

	private String seatTemplAssignErrorDesc;

	private String userNotes;
	
	private Set<CodeShareMCFlight> codeShareMCFlights;
	
	private String csOCCarrierCode;
	
	private String csOCFlightNumber;

	private boolean viaScheduleMessaging = false;
	
	private boolean cancelMsgWaiting = false;
	
	private Date holdCancelMsgTill;
	

	public Flight() {

	}

	public Flight(Integer flightId, String flightNumber, Date departureDate, String originAptCode, String modelNumber,
			Date estTimeArrivalZulu) {
		this.flightId = flightId;
		this.flightNumber = flightNumber;
		this.departureDate = departureDate;
		this.originAptCode = originAptCode;
		this.modelNumber = modelNumber;
		FlightSegement lastFlightSegment = new FlightSegement(estTimeArrivalZulu);
		addsegment(lastFlightSegment);
	}

	/**
	 * @hibernate.property column = "MEAL_NOTIFICATION_ATTEMPTS"
	 * @return
	 */
	public Integer getMealNotificationAttempts() {
		return mealNotificationAttempts;
	}

	/**
	 * Sets the value
	 * 
	 * @param mealNotificationAttempts
	 */
	public void setMealNotificationAttempts(Integer mealNotificationAttempts) {
		this.mealNotificationAttempts = mealNotificationAttempts;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "MEAL_NOTIFICATION_STATUS"
	 */
	public String getMealNotificationStatus() {
		return mealNotificationStatus;
	}

	public void setMealNotificationStatus(String mealNotificationStatus) {
		this.mealNotificationStatus = mealNotificationStatus;
	}

	/**
	 * returns the flightId
	 * 
	 * @return Returns the flightId.
	 * @hibernate.id column = "FLIGHT_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_FLIGHT"
	 */
	public Integer getFlightId() {
		return flightId;
	}

	/**
	 * sets the flightId
	 * 
	 * @param flightId
	 *            The flightId to set.
	 */
	public void setFlightId(Integer flightId) {
		this.flightId = flightId;
	}

	/**
	 * returns the flightNumber
	 * 
	 * @return Returns the flightNumber.
	 * @hibernate.property column = "FLIGHT_NUMBER"
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * sets the flightNumber
	 * 
	 * @param flightNumber
	 *            The flightNumber to set.
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * returns the departureDate
	 * 
	 * @return Returns the departureDate.
	 * @hibernate.property column = "DEPARTURE_DATE"
	 */
	public Date getDepartureDate() {
		return departureDate;
	}

	/**
	 * sets the departureDate
	 * 
	 * @param departureDate
	 *            The departureDate to set.
	 */
	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	/**
	 * returns the availableSeatKilometers
	 * 
	 * @return Returns the availableSeatKilometers.
	 * @hibernate.property column = "AVAILABLE_SEAT_KILOMETERS"
	 */
	public Integer getAvailableSeatKilometers() {
		return availableSeatKilometers;
	}

	/**
	 * sets the availableSeatKilometers
	 * 
	 * @param availableSeatKilometers
	 *            The availableSeatKilometers to set.
	 */
	public void setAvailableSeatKilometers(Integer availableSeatKilometers) {
		this.availableSeatKilometers = availableSeatKilometers;
	}

	/**
	 * returns the tailNumber
	 * 
	 * @return Returns the tailNumber.
	 * @hibernate.property column = "TAIL_NUMBER"
	 */
	public String getTailNumber() {
		return tailNumber;
	}

	/**
	 * sets the tailNumber
	 * 
	 * @param tailNumber
	 *            The tailNumber to set.
	 */
	public void setTailNumber(String tailNumber) {
		this.tailNumber = tailNumber;
	}

	/**
	 * returns the modelNumber
	 * 
	 * @return Returns the modelNumber.
	 * @hibernate.property column = "MODEL_NUMBER"
	 */
	public String getModelNumber() {
		return modelNumber;
	}

	/**
	 * sets the modelNumber
	 * 
	 * @param modelNumber
	 *            The modelNumber to set.
	 */
	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}

	/**
	 * returns the status
	 * 
	 * @return Returns the status.
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * sets the status
	 * 
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * returns dayNumber
	 * 
	 * @return dayNumber
	 * @hibernate.property column = "DAY_NUMBER"
	 * 
	 */
	public int getDayNumber() {
		return dayNumber;
	}

	/**
	 * sets the dayNumber
	 * 
	 * @param dayNumber
	 */
	public void setDayNumber(int dayNumber) {
		this.dayNumber = dayNumber;
	}

	/**
	 * returns the gestination airport code
	 * 
	 * @return destinationAptCode
	 * @hibernate.property column = "DESTINATION"
	 */
	public String getDestinationAptCode() {
		return destinationAptCode;
	}

	/**
	 * sets the destination airport code
	 * 
	 * @param destinationAptCode
	 */
	public void setDestinationAptCode(String destinationStnCode) {
		this.destinationAptCode = destinationStnCode;
	}

	/**
	 * returns the operation type id
	 * 
	 * @return operationTypeId
	 * @hibernate.property column = "OPERATION_TYPE_ID"
	 */
	public int getOperationTypeId() {
		return operationTypeId;
	}

	/**
	 * sets the operationTypeId
	 * 
	 * @param operationTypeId
	 */
	public void setOperationTypeId(int operationTypeId) {
		this.operationTypeId = operationTypeId;
	}

	/**
	 * returns the originAptCode
	 * 
	 * @return originAptCode
	 * @hibernate.property column = "ORIGIN"
	 */
	public String getOriginAptCode() {
		return originAptCode;
	}

	/**
	 * sets the originAptCode
	 * 
	 * @param originAptCode
	 */
	public void setOriginAptCode(String originStnCode) {
		this.originAptCode = originStnCode;
	}

	/**
	 * returns the scheduleId
	 * 
	 * @return scheduleId
	 * @hibernate.property column = "SCHEDULE_ID"
	 */
	public Integer getScheduleId() {
		return scheduleId;
	}

	/**
	 * sets teh scheduleId
	 * 
	 * @param scheduleId
	 */
	public void setScheduleId(Integer scheduleId) {
		this.scheduleId = scheduleId;
	}

	/**
	 * returns the legs related with this flight
	 * 
	 * @hibernate.set lazy="false" cascade="all"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airschedules.api.model.FlightLeg"
	 * @hibernate.collection-key column="FLIGHT_ID"
	 */
	public Set<FlightLeg> getFlightLegs() {
		return flightLegs;
	}

	/**
	 * sets the legs related with this flight
	 */
	public void setFlightLegs(Set<FlightLeg> set) {
		this.flightLegs = set;
	}

	/**
	 * Add flight leg
	 * 
	 * @param flightLeg
	 */
	public void addLeg(FlightLeg flightLeg) {
		if (this.getFlightLegs() == null) {
			this.setFlightLegs(new HashSet<FlightLeg>());
		}

		flightLeg.setFlightId(this.flightId);
		this.getFlightLegs().add(flightLeg);
	}

	/**
	 * Add flight segment
	 * 
	 * @param flightSegment
	 */
	public void addsegment(FlightSegement flightSegment) {
		if (this.getFlightSegements() == null) {
			this.setFlightSegements(new HashSet<FlightSegement>());
		}

		flightSegment.setFlightId(this.flightId);
		this.getFlightSegements().add(flightSegment);
	}

	/**
	 * returns the flight segments related with this flight
	 * 
	 * @hibernate.set lazy="false" cascade="all"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airschedules.api.model.FlightSegement"
	 * @hibernate.collection-key column="FLIGHT_ID"
	 */
	public Set<FlightSegement> getFlightSegements() {
		return flightSegements;
	}

	/**
	 * sets the flight segments related with this flight
	 */
	public void setFlightSegements(Set<FlightSegement> set) {
		this.flightSegements = set;
	}

	/**
	 * gets the manually changed
	 * 
	 * @return Returns the manuallyChanged.
	 * @hibernate.property column = "MANUALLY_CHANGED" type="yes_no"
	 */
	public boolean getManuallyChanged() {
		return manuallyChanged;
	}

	/**
	 * @param manuallyChanged
	 *            The manuallyChanged to set.
	 */
	public void setManuallyChanged(boolean manuallyChanged) {
		this.manuallyChanged = manuallyChanged;
	}

	/**
	 * @return Returns the overlapingFlightId.
	 * @hibernate.property column = "OVERLAP_FLIGHT_INSTANCE_ID"
	 */
	public Integer getOverlapingFlightId() {
		return overlapingFlightId;
	}

	/**
	 * @param overlapingFlightId
	 *            The overlapingFlightId to set.
	 */
	public void setOverlapingFlightId(Integer overlapingFlightId) {
		this.overlapingFlightId = overlapingFlightId;
	}

	/**
	 * returns flight is overlapping or not
	 * 
	 * @return boolean
	 */
	public boolean isOverlapping() {
		return (overlapingFlightId == null) ? false : true;
	}

	/**
	 * @return Returns the departureDateLocal.
	 * @hibernate.property column = "DEPARTURE_DATE_LOCAL"
	 */
	public Date getDepartureDateLocal() {
		return departureDateLocal;
	}

	/**
	 * @param departureDateLocal
	 *            The departureDateLocal to set.
	 */
	public void setDepartureDateLocal(Date departureDateLocal) {
		this.departureDateLocal = departureDateLocal;
	}

	/**
	 * returns the gds ids related with this flight
	 * 
	 * @hibernate.set lazy="false" cascade="all" table="T_FLIGHT_GDS"
	 * @hibernate.collection-element column="GDS_ID" type="integer"
	 * @hibernate.collection-key column="FLIGHT_ID"
	 */
	public Set<Integer> getGdsIds() {
		return gdsIds;
	}

	public void setGdsIds(Set<Integer> gdsIds) {
		this.gdsIds = gdsIds;
	}

	public void addGdsId(Integer gdsId) {
		if (this.gdsIds == null)
			this.gdsIds = new HashSet<Integer>();
		gdsIds.add(gdsId);
	}

	/**
	 * @return the flightType
	 * 
	 * @hibernate.property column = "FLIGHT_TYPE"
	 */
	public String getFlightType() {
		return flightType;
	}

	/**
	 * @param flightType
	 *            the flightType to set
	 */
	public void setFlightType(String flightType) {
		this.flightType = flightType;
	}

	public boolean isInternationalFlight() {
		if (FlightUtil.FLIGHT_TYPE_INT.equals(this.flightType)) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isDomesticFlight() {
		if (FlightUtil.FLIGHT_TYPE_DOM.equals(this.flightType)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @return the remarks
	 * 
	 * @hibernate.property column = "REMARKS"
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @param remarks
	 *            the remarks to set
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * @return the mealTemplateCode
	 * 
	 * @hibernate.property column = "MEAL_TEMPLATE_ID"
	 */
	public Integer getMealTemplateId() {
		return mealTemplateId;
	}

	/**
	 * @param mealTemplateCode
	 *            the mealTemplateCode to set
	 */
	public void setMealTemplateId(Integer mealTemplateId) {
		this.mealTemplateId = mealTemplateId;
	}

	/**
	 * @return the seatChargeTemplateCode
	 * 
	 * @hibernate.property column = "AMC_TEMPLATE_ID"
	 */
	public Integer getSeatChargeTemplateId() {
		return seatChargeTemplateId;
	}

	/**
	 * @param seatChargeTemplateCode
	 *            the seatChargeTemplateCode to set
	 */
	public void setSeatChargeTemplateId(Integer seatChargeTemplateId) {
		this.seatChargeTemplateId = seatChargeTemplateId;
	}

	/**
	 * @return the mealTemplAssignStatus
	 * 
	 * @hibernate.property column = "MEAL_TEMPL_ASSIGN_STATUS"
	 */
	public String getMealTemplAssignStatus() {
		return mealTemplAssignStatus;
	}

	/**
	 * @param mealTemplAssignStatus
	 *            the mealTemplAssignStatus to set
	 */
	public void setMealTemplAssignStatus(String mealTemplAssignStatus) {
		this.mealTemplAssignStatus = mealTemplAssignStatus;
	}

	/**
	 * @return the seatTemplAssignStatus
	 * 
	 * @hibernate.property column = "AMC_TEMPL_ASSIGN_STATUS"
	 */
	public String getSeatTemplAssignStatus() {
		return seatTemplAssignStatus;
	}

	/**
	 * @param seatTemplAssignStatus
	 *            the seatTemplAssignStatus to set
	 */
	public void setSeatTemplAssignStatus(String seatTemplAssignStatus) {
		this.seatTemplAssignStatus = seatTemplAssignStatus;
	}

	/**
	 * @return the mealTemplAssignErrorDesc
	 * 
	 * @hibernate.property column = "MEAL_TEMPL_ERROR_DESC"
	 */
	public String getMealTemplAssignErrorDesc() {
		return mealTemplAssignErrorDesc;
	}

	/**
	 * @param mealTemplAssignErrorDesc
	 *            the mealTemplAssignErrorDesc to set
	 */
	public void setMealTemplAssignErrorDesc(String mealTemplAssignErrorDesc) {
		this.mealTemplAssignErrorDesc = mealTemplAssignErrorDesc;
	}

	/**
	 * @return the seatTemplAssignErrorDesc
	 * 
	 * @hibernate.property column = "AMC_TEMPL_ERROR_DESC"
	 */
	public String getSeatTemplAssignErrorDesc() {
		return seatTemplAssignErrorDesc;
	}

	/**
	 * @param seatTemplAssignErrorDesc
	 *            the seatTemplAssignErrorDesc to set
	 */
	public void setSeatTemplAssignErrorDesc(String seatTemplAssignErrorDesc) {
		this.seatTemplAssignErrorDesc = seatTemplAssignErrorDesc;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "NOTES"
	 */
	public String getUserNotes() {
		return userNotes;
	}

	public void setUserNotes(String userNotes) {
		this.userNotes = userNotes;
	}

	public String getOldModelNumber() {
		return oldModelNumber;
	}

	public void setOldModelNumber(String oldModelNumber) {
		this.oldModelNumber = oldModelNumber;
	}

	public String getModelDesc() {
		return modelDesc;
	}

	public void setModelDesc(String modelDesc) {
		this.modelDesc = modelDesc;
	}
	
	/**
	 * returns the CodeShareMCFlights related with this flight
	 * 
	 * @hibernate.set lazy="false" cascade="all"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airschedules.api.model.CodeShareMCFlight"
	 * @hibernate.collection-key column="FLIGHT_ID"
	 */
	public Set<CodeShareMCFlight> getCodeShareMCFlights() {
		return codeShareMCFlights;
	}

	public void setCodeShareMCFlights(Set<CodeShareMCFlight> codeShareMCFlights) {
		this.codeShareMCFlights = codeShareMCFlights;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "CS_OC_CARRIER_CODE"
	 */
	public String getCsOCCarrierCode() {
		return csOCCarrierCode;
	}

	public void setCsOCCarrierCode(String csOCCarrierCode) {
		this.csOCCarrierCode = csOCCarrierCode;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "CS_OC_FLIGHT_NUMBER"
	 */
	public String getCsOCFlightNumber() {
		return csOCFlightNumber;
	}

	public void setCsOCFlightNumber(String csOCFlightNumber) {
		this.csOCFlightNumber = csOCFlightNumber;
	}

	/**
	 * @return the baggabeTemplateId
	 */
	public Integer getBaggabeTemplateId() {
		return baggabeTemplateId;
	}

	/**
	 * @param baggabeTemplateId the baggabeTemplateId to set
	 */
	public void setBaggabeTemplateId(Integer baggabeTemplateId) {
		this.baggabeTemplateId = baggabeTemplateId;
	}

	/**
	 * @return viaScheduleMessaging
	 * 
	 * @hibernate.property column = "VIA_SCHEDULE_MESSAGE" type="yes_no"
	 * 
	 */
	public boolean getViaScheduleMessaging() {
		return viaScheduleMessaging;
	}

	/**
	 * @param viaScheduleMesseging
	 */
	public void setViaScheduleMessaging(boolean viaScheduleMessaging) {
		this.viaScheduleMessaging = viaScheduleMessaging;
	}
	
	/**
	 * @return viaScheduleMessaging
	 * 
	 * @hibernate.property column = "CNL_MSG_WAITING" type="yes_no"
	 * 
	 */
	public boolean isCancelMsgWaiting() {
		return cancelMsgWaiting;
	}

	public void setCancelMsgWaiting(boolean cancelMsgWaiting) {
		this.cancelMsgWaiting = cancelMsgWaiting;
	}

	/**
	 * @return viaScheduleMessaging
	 * 
	 * @hibernate.property column = "HOLD_CNL_MSG_TILL"
	 * 
	 */
	public Date getHoldCancelMsgTill() {
		return holdCancelMsgTill;
	}

	public void setHoldCancelMsgTill(Date holdCancelMsgTill) {
		this.holdCancelMsgTill = holdCancelMsgTill;
	}
}
