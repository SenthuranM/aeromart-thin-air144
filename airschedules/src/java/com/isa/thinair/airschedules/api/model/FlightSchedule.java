/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:18:22
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.api.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.core.framework.Tracking;

/**
 * FlightSchedule is the entity class to repesent a FlightSchedule model
 * 
 * @author Code Generation Tool
 * @version Ver 1.0.0
 * @hibernate.class table = "T_FLIGHT_SCHEDULE"
 */
public class FlightSchedule extends Tracking {

	private static final long serialVersionUID = -1339805293681625902L;

	public static final boolean FREQUENCY_D0_Y = true; // "Y";

	public static final boolean FREQUENCY_D0_N = false; // "N";

	// private fields
	private Integer scheduleId;

	private String flightNumber;

	private Date startDate;

	private Date stopDate;

	private Date startDateLocal;

	private Date stopDateLocal;

	private Integer overlapingScheduleId;

	private Integer availableSeatKilometers;

	private Integer numberOfDepartures;

	private String departureStnCode;

	private String arrivalStnCode;

	private String modelNumber;

	private int operationTypeId;

	private String buildStatusCode;

	private String statusCode;

	private Frequency frequency;

	private Frequency frequencyLocal;

	private Set<FlightScheduleLeg> flightScheduleLegs;

	private Set<FlightScheduleSegment> flightScheduleSegments;

	private boolean manuallyChanged = false;

	private boolean localChange = false;

	private Set<Integer> gdsIds;

	private int numberOfOpenFlights;

	private String flightType;

	private String remarks;

	private Integer mealTemplateId;

	private Integer seatChargeTemplateId;

	private Integer baggabeTemplateId;

	private String mealTemplAssignStatus;

	private String seatTemplAssignStatus;

	private String mealTemplAssignErrorDesc;

	private String seatTemplAssignErrorDesc;

	private Set<CodeShareMCFlightSchedule> codeShareMCFlightSchedules;

	private String csOCCarrierCode;

	private String csOCFlightNumber;

	private Set<SSMSplitFlightSchedule> ssmSplitFlightSchedule;

	private boolean viaScheduleMessaging = false;
	
	private boolean cancelMsgWaiting = false;
	
	private Date holdCancelMsgTill;
	

	/**
	 * Externaly set. Hibernate doesnt support to directly get child result count
	 */

	public int getNumberOfFlights() {
		return numberOfOpenFlights;
	}

	/**
	 * @param numberOfFlights
	 *            the numberOfFlights to set
	 */
	public void setNumberOfFlights(int numberOfFlights) {
		this.numberOfOpenFlights = numberOfFlights;
	}

	/**
	 * returns the scheduleId
	 * 
	 * @return Returns the scheduleId.
	 * @hibernate.id column = "SCHEDULE_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_FLIGHT_SCHEDULE"
	 */
	public Integer getScheduleId() {
		return scheduleId;
	}

	/**
	 * sets the scheduleId
	 * 
	 * @param scheduleId
	 *            The scheduleId to set.
	 */
	public void setScheduleId(Integer scheduleId) {
		this.scheduleId = scheduleId;
	}

	/**
	 * returns the flightNumber
	 * 
	 * @return Returns the flightNumber.
	 * @hibernate.property column = "FLIGHT_NUMBER"
	 */
	public String getFlightNumber() {
		return flightNumber;
	}

	/**
	 * sets the flightNumber
	 * 
	 * @param flightNumber
	 *            The flightNumber to set.
	 */
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	/**
	 * returns the startDate
	 * 
	 * @return Returns the startDate.
	 * @hibernate.property column = "START_DATE"
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * sets the startDate
	 * 
	 * @param startDate
	 *            The startDate to set.
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * returns the stopDate
	 * 
	 * @return Returns the stopDate.
	 * @hibernate.property column = "STOP_DATE"
	 */
	public Date getStopDate() {
		return stopDate;
	}

	/**
	 * sets the stopDate
	 * 
	 * @param stopDate
	 *            The stopDate to set.
	 */
	public void setStopDate(Date stopDate) {
		this.stopDate = stopDate;
	}

	/**
	 * returns the availableSeatKilometers
	 * 
	 * @return Returns the availableSeatKilometers.
	 * @hibernate.property column = "AVAILABLE_SEAT_KILOMETERS"
	 */
	public Integer getAvailableSeatKilometers() {
		return availableSeatKilometers;
	}

	/**
	 * sets the availableSeatKilometers
	 * 
	 * @param availableSeatKilometers
	 *            The availableSeatKilometers to set.
	 */
	public void setAvailableSeatKilometers(Integer availableSeatKilometers) {
		this.availableSeatKilometers = availableSeatKilometers;
	}

	/**
	 * returns the numberOfDepartures
	 * 
	 * @return Returns the numberOfDepartures.
	 * @hibernate.property column = "NUMBER_OF_DEPARTURES"
	 */
	public Integer getNumberOfDepartures() {
		return numberOfDepartures;
	}

	/**
	 * sets the numberOfDepartures
	 * 
	 * @param numberOfDepartures
	 *            The numberOfDepartures to set.
	 */
	public void setNumberOfDepartures(Integer numberOfDepartures) {
		this.numberOfDepartures = numberOfDepartures;
	}

	/**
	 * returns the departureStnCode
	 * 
	 * @return departureStnCode
	 * @hibernate.property column = "ORIGIN"
	 */
	public String getDepartureStnCode() {
		return departureStnCode;
	}

	/**
	 * sets the departureStnCode
	 * 
	 * @param departureStnCode
	 */
	public void setDepartureStnCode(String departureStnCode) {
		this.departureStnCode = departureStnCode;
	}

	/**
	 * returns the arrivalStnCode
	 * 
	 * @return arrivalStnCode
	 * @hibernate.property column = "DESTINATION"
	 */
	public String getArrivalStnCode() {
		return arrivalStnCode;
	}

	/**
	 * sets the arrivalStnCode
	 * 
	 * @param arrivalStnCode
	 */
	public void setArrivalStnCode(String arrivalStnCode) {
		this.arrivalStnCode = arrivalStnCode;
	}

	/**
	 * returns the buildStatusCode
	 * 
	 * @return buildStatusCode
	 * @hibernate.property column = "SCHEDULE_BUILD_STATUS_CODE"
	 */
	public String getBuildStatusCode() {
		return buildStatusCode;
	}

	/**
	 * sets the buildStatusCode
	 * 
	 * @param buildStatusCode
	 */
	public void setBuildStatusCode(String buildStatusCode) {
		this.buildStatusCode = buildStatusCode;
	}

	/**
	 * returns the modelNumber
	 * 
	 * @return modelNumber
	 * @hibernate.property column = "MODEL_NUMBER"
	 */
	public String getModelNumber() {
		return modelNumber;
	}

	/**
	 * sets the modelNumber
	 * 
	 * @param modelNumber
	 */
	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}

	/**
	 * returns teh operationTypeId
	 * 
	 * @return operationTypeId
	 * @hibernate.property column = "OPERATION_TYPE_ID"
	 */
	public int getOperationTypeId() {
		return operationTypeId;
	}

	/**
	 * sets the operationTypeId
	 * 
	 * @param operationTypeId
	 */
	public void setOperationTypeId(int operationTypeId) {
		this.operationTypeId = operationTypeId;
	}

	/**
	 * returns the statusCode
	 * 
	 * @return statusCode
	 * @hibernate.property column = "SCHEDULE_STATUS_CODE"
	 */
	public String getStatusCode() {
		return statusCode;
	}

	/**
	 * sets the statusCode
	 * 
	 * @param statusCode
	 * 
	 */
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	/**
	 * returns the frequency
	 * 
	 * @return frequency
	 * @hibernate.component class="com.isa.thinair.commons.api.dto.Frequency"
	 */
	public Frequency getFrequency() {
		return frequency;
	}

	/**
	 * sets the frequency
	 * 
	 * @param frequency
	 */
	public void setFrequency(Frequency frequency) {
		this.frequency = frequency;
	}

	/**
	 * returns the flight schedule leg related with this flight schedule
	 * 
	 * @hibernate.set lazy="false" cascade="all"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airschedules.api.model.FlightScheduleLeg"
	 * @hibernate.collection-key column="SCHEDULE_ID"
	 */
	public Set<FlightScheduleLeg> getFlightScheduleLegs() {
		return flightScheduleLegs;
	}

	/**
	 * sets the flight schedule legs related with this flight schedule
	 */
	public void setFlightScheduleLegs(Set<FlightScheduleLeg> set) {
		this.flightScheduleLegs = set;
	}

	/**
	 * returns the flight schedule segments related with this flight schedule
	 * 
	 * @hibernate.set lazy="false" cascade="all"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airschedules.api.model.FlightScheduleSegment"
	 * @hibernate.collection-key column="SCHEDULE_ID"
	 */
	public Set<FlightScheduleSegment> getFlightScheduleSegments() {
		return flightScheduleSegments;
	}

	/**
	 * sets the flight schedule segments related with this flight schedule
	 */
	public void setFlightScheduleSegments(Set<FlightScheduleSegment> set) {
		this.flightScheduleSegments = set;
	}

	/**
	 * gets the manually changed
	 * 
	 * @return Returns the manuallyChanged.
	 * @hibernate.property column = "MANUALLY_CHANGED" type="yes_no"
	 */
	public boolean getManuallyChanged() {
		return manuallyChanged;
	}

	/**
	 * @param manuallyChanged
	 *            The manuallyChanged to set.
	 */
	public void setManuallyChanged(boolean manuallyChanged) {
		this.manuallyChanged = manuallyChanged;
	}

	/**
	 * @return Returns the overlapingScheduleId.
	 * @hibernate.property column = "OVERLAP_SCHEDULE_ID"
	 */
	public Integer getOverlapingScheduleId() {
		return overlapingScheduleId;
	}

	/**
	 * @param overlapingScheduleId
	 *            The overlapingScheduleId to set.
	 */
	public void setOverlapingScheduleId(Integer overlapingScheduleId) {
		this.overlapingScheduleId = overlapingScheduleId;
	}

	/**
	 * returns overlapping schedule or not
	 * 
	 * @return boolean
	 */
	public boolean isOverlapping() {
		return (overlapingScheduleId == null) ? false : true;
	}

	/**
	 * @return Returns the startDateLocal.
	 * @hibernate.property column = "START_DATE_LOCAL"
	 */
	public Date getStartDateLocal() {
		return startDateLocal;
	}

	/**
	 * @param startDateLocal
	 *            The startDateLocal to set.
	 */
	public void setStartDateLocal(Date startDateLocal) {
		this.startDateLocal = startDateLocal;
	}

	/**
	 * @return Returns the stopDateLocal.
	 * @hibernate.property column = "STOP_DATE_LOCAL"
	 */
	public Date getStopDateLocal() {
		return stopDateLocal;
	}

	/**
	 * @param stopDateLocal
	 *            The stopDateLocal to set.
	 */
	public void setStopDateLocal(Date stopDateLocal) {
		this.stopDateLocal = stopDateLocal;
	}

	public Frequency getFrequencyLocal() {
		return frequencyLocal;
	}

	public void setFrequencyLocal(Frequency frequencyLocal) {
		this.frequencyLocal = frequencyLocal;
	}

	public boolean getLocalChange() {
		return localChange;
	}

	public void setLocalChang(boolean localChange) {
		this.localChange = localChange;
	}

	/**
	 * returns the gds ids related with flight schedule
	 * 
	 * @hibernate.set lazy="false" cascade="all" table="T_FLIGHT_SCHEDULE_GDS"
	 * @hibernate.collection-element column="GDS_ID" type="integer"
	 * @hibernate.collection-key column="SCHEDULE_ID"
	 */
	public Set<Integer> getGdsIds() {
		return gdsIds;
	}

	public void setGdsIds(Set<Integer> gdsIds) {
		this.gdsIds = gdsIds;
	}

	public void addGdsId(Integer gdsId) {
		if (this.gdsIds == null)
			this.gdsIds = new HashSet<Integer>();
		gdsIds.add(gdsId);
	}

	/**
	 * @return the flightType
	 * 
	 * @hibernate.property column = "FLIGHT_TYPE"
	 */
	public String getFlightType() {
		return flightType;
	}

	/**
	 * @param flightType
	 *            the flightType to set
	 */
	public void setFlightType(String flightType) {
		this.flightType = flightType;
	}

	/**
	 * @return the remarks
	 * 
	 * @hibernate.property column = "REMARKS"
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @param remarks
	 *            the remarks to set
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * @return the mealTemplateCode
	 * 
	 * @hibernate.property column = "MEAL_TEMPLATE_ID"
	 */
	public Integer getMealTemplateId() {
		return mealTemplateId;
	}

	/**
	 * @param mealTemplateCode
	 *            the mealTemplateCode to set
	 */
	public void setMealTemplateId(Integer mealTemplateId) {
		this.mealTemplateId = mealTemplateId;
	}

	/**
	 * @return the seatChargeTemplateCode
	 * 
	 * @hibernate.property column = "AMC_TEMPLATE_ID"
	 */
	public Integer getSeatChargeTemplateId() {
		return seatChargeTemplateId;
	}

	/**
	 * @param seatChargeTemplateCode
	 *            the seatChargeTemplateCode to set
	 */
	public void setSeatChargeTemplateId(Integer seatChargeTemplateId) {
		this.seatChargeTemplateId = seatChargeTemplateId;
	}

	/**
	 * @return the mealTemplAssignStatus
	 * 
	 * @hibernate.property column = "MEAL_TEMPL_ASSIGN_STATUS"
	 */
	public String getMealTemplAssignStatus() {
		return mealTemplAssignStatus;
	}

	/**
	 * @param mealTemplAssignStatus
	 *            the mealTemplAssignStatus to set
	 */
	public void setMealTemplAssignStatus(String mealTemplAssignStatus) {
		this.mealTemplAssignStatus = mealTemplAssignStatus;
	}

	/**
	 * @return the seatTemplAssignStatus
	 * 
	 * @hibernate.property column = "AMC_TEMPL_ASSIGN_STATUS"
	 */
	public String getSeatTemplAssignStatus() {
		return seatTemplAssignStatus;
	}

	/**
	 * @param seatTemplAssignStatus
	 *            the seatTemplAssignStatus to set
	 */
	public void setSeatTemplAssignStatus(String seatTemplAssignStatus) {
		this.seatTemplAssignStatus = seatTemplAssignStatus;
	}

	/**
	 * @return the mealTemplAssignErrorDesc
	 * 
	 * @hibernate.property column = "MEAL_TEMPL_ERROR_DESC"
	 */
	public String getMealTemplAssignErrorDesc() {
		return mealTemplAssignErrorDesc;
	}

	/**
	 * @param mealTemplAssignErrorDesc
	 *            the mealTemplAssignErrorDesc to set
	 */
	public void setMealTemplAssignErrorDesc(String mealTemplAssignErrorDesc) {
		this.mealTemplAssignErrorDesc = mealTemplAssignErrorDesc;
	}

	/**
	 * @return the seatTemplAssignErrorDesc
	 * 
	 * @hibernate.property column = "AMC_TEMPL_ERROR_DESC"
	 */
	public String getSeatTemplAssignErrorDesc() {
		return seatTemplAssignErrorDesc;
	}

	/**
	 * @param seatTemplAssignErrorDesc
	 *            the seatTemplAssignErrorDesc to set
	 */
	public void setSeatTemplAssignErrorDesc(String seatTemplAssignErrorDesc) {
		this.seatTemplAssignErrorDesc = seatTemplAssignErrorDesc;
	}

	/**
	 * returns the codeShareMCFlightSchedule related with this flight
	 * 
	 * @hibernate.set lazy="false" cascade="all"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airschedules.api.model.CodeShareMCFlightSchedule"
	 * @hibernate.collection-key column="SCHEDULE_ID"
	 */
	public Set<CodeShareMCFlightSchedule> getCodeShareMCFlightSchedules() {
		return codeShareMCFlightSchedules;
	}

	public void setCodeShareMCFlightSchedules(Set<CodeShareMCFlightSchedule> codeShareMCFlightSchedules) {
		this.codeShareMCFlightSchedules = codeShareMCFlightSchedules;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "CS_OC_CARRIER_CODE"
	 */
	public String getCsOCCarrierCode() {
		return csOCCarrierCode;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "CS_OC_FLIGHT_NUMBER"
	 */
	public String getCsOCFlightNumber() {
		return csOCFlightNumber;
	}

	public void setCsOCCarrierCode(String csOCCarrierCode) {
		this.csOCCarrierCode = csOCCarrierCode;
	}

	public void setCsOCFlightNumber(String csOCFlightNumber) {
		this.csOCFlightNumber = csOCFlightNumber;
	}

	/**
	 * @return Returns the ssmSplitFlightSchedule.
	 * @hibernate.set lazy="false" cascade="all-delete-orphan" inverse="true"
	 * @hibernate.collection-key column="SCHEDULE_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airschedules.api.model.SSMSplitFlightSchedule"
	 */
	public Set<SSMSplitFlightSchedule> getSsmSplitFlightSchedule() {
		if (ssmSplitFlightSchedule == null)
			ssmSplitFlightSchedule = new HashSet<SSMSplitFlightSchedule>();

		return ssmSplitFlightSchedule;
	}

	public void setSsmSplitFlightSchedule(Set<SSMSplitFlightSchedule> ssmSplitFlightSchedule) {
		this.ssmSplitFlightSchedule = ssmSplitFlightSchedule;
	}

	/**
	 * @return the baggabeTemplateId
	 */
	public Integer getBaggabeTemplateId() {
		return baggabeTemplateId;
	}

	/**
	 * @param baggabeTemplateId
	 *            the baggabeTemplateId to set
	 */
	public void setBaggabeTemplateId(Integer baggabeTemplateId) {
		this.baggabeTemplateId = baggabeTemplateId;
	}

	/**
	 * @return viaScheduleMessaging
	 * 
	 * @hibernate.property column = "VIA_SCHEDULE_MESSAGE" type="yes_no"
	 * 
	 */
	public boolean getViaScheduleMessaging() {
		return viaScheduleMessaging;
	}

	/**
	 * @param viaScheduleMesseging
	 */
	public void setViaScheduleMessaging(boolean viaScheduleMessaging) {
		this.viaScheduleMessaging = viaScheduleMessaging;
	}

	/**
	 * @return viaScheduleMessaging
	 * 
	 * @hibernate.property column = "CNL_MSG_WAITING" type="yes_no"
	 * 
	 */
	public boolean isCancelMsgWaiting() {
		return cancelMsgWaiting;
	}

	public void setCancelMsgWaiting(boolean cancelMsgWaiting) {
		this.cancelMsgWaiting = cancelMsgWaiting;
	}

	/**
	 * @return viaScheduleMessaging
	 * 
	 * @hibernate.property column = "HOLD_CNL_MSG_TILL"
	 * 
	 */
	public Date getHoldCancelMsgTill() {
		return holdCancelMsgTill;
	}

	public void setHoldCancelMsgTill(Date holdCancelMsgTill) {
		this.holdCancelMsgTill = holdCancelMsgTill;
	}

}
