/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:19:25
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.api.model;

import java.io.Serializable;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * FlySegement is the repesent a FlySegement in the object model
 * 
 * @author Lasantha Pambagoda
 */
public class FlySegement extends Persistent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5904211564859467353L;

	// public constants
	/** Constant to represent validity flag yes of the segment */
	public static final boolean SEGMENT_VALID_FLAG_Y = true;

	/** Constant to represent validity flag no of the segment */
	public static final boolean SEGMENT_VALID_FLAG_N = false;

	private String segmentCode;

	private boolean validFlag = true;

	/**
	 * returns code
	 * 
	 * @return code
	 * @hibernate.property column = "SEGMENT_CODE"
	 */
	public String getSegmentCode() {
		return segmentCode;
	}

	/**
	 * sets the code
	 * 
	 * @param code
	 */
	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	/**
	 * returns the validFlag
	 * 
	 * @return Returns the validFlag.
	 * @hibernate.property column = "SEGMENT_VALID_FLAG" type="yes_no"
	 */
	public boolean getValidFlag() {
		return validFlag;
	}

	/**
	 * sets the validFlag
	 * 
	 * @param validFlag
	 */
	public void setValidFlag(boolean validFlag) {
		this.validFlag = validFlag;
	}
}
