/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:19:10
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.api.model;

/**
 * FlightScheduleSegment is the entity class to repesent a FlightScheduleSegment model
 * 
 * @author Code Generation Tool, Lasantha Pambagoada
 * @version Ver 1.0.0
 * @hibernate.class table = "T_FLIGHT_SCHEDULE_SEGMENT"
 */
public class FlightScheduleSegment extends FlySegement {

	private static final long serialVersionUID = 1865680952935748473L;

	private Integer flSchSegId;

	private Integer scheduleId;

	private Integer overlapSegmentId;

	private Integer departureTerminalId;

	private Integer arrivalTerminalId;

	/**
	 * returns the flSchSegId
	 * 
	 * @return Returns the flSchSegId.
	 * @hibernate.id column = "FL_SCH_SEG_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_FLIGHT_SCHEDULE_SEGMENT"
	 */
	public Integer getFlSchSegId() {
		return flSchSegId;
	}

	/**
	 * sets the flSchSegId
	 * 
	 * @param flSchSegId
	 *            .
	 */
	public void setFlSchSegId(Integer flSchSegId) {
		this.flSchSegId = flSchSegId;
	}

	/**
	 * returns the scheduleId
	 * 
	 * @return Returns the scheduleId.
	 * @hibernate.property column = "SCHEDULE_ID"
	 */
	public Integer getScheduleId() {
		return scheduleId;
	}

	/**
	 * sets the scheduleId
	 * 
	 * @param scheduleId
	 *            .
	 */
	public void setScheduleId(Integer scheduleId) {
		this.scheduleId = scheduleId;
	}

	/**
	 * @return Returns the overlapSegmentId.
	 * @hibernate.property column = "OVERLAP_FL_SCH_SEG_ID"
	 */
	public Integer getOverlapSegmentId() {
		return overlapSegmentId;
	}

	/**
	 * @param overlapSegmentId
	 *            The overlapSegmentId to set.
	 */
	public void setOverlapSegmentId(Integer overlapSegmentId) {
		this.overlapSegmentId = overlapSegmentId;
	}

	/**
	 * checks is overlapping
	 * 
	 * @return boolean
	 */
	public boolean isOverlapping() {
		return (overlapSegmentId == null) ? false : true;
	}

	/**
	 * gets the departure terminal id.
	 * 
	 * @return the departure terminal id.
	 * @hibernate.property column = "DEPARTURE_TERMINAL_ID"
	 */
	public Integer getDepartureTerminalId() {
		return departureTerminalId;
	}

	/**
	 * @param departureTerminalId
	 *            The departure terminal id.
	 */
	public void setDepartureTerminalId(Integer departureTerminalId) {
		this.departureTerminalId = departureTerminalId;
	}

	/**
	 * gets the arrival terminal id.
	 * 
	 * @return the arrival terminal id.
	 * @hibernate.property column = "ARRIVAL_TERMINAL_ID"
	 */
	public Integer getArrivalTerminalId() {
		return arrivalTerminalId;
	}

	/**
	 * @param arrivalTerminalId
	 *            The arrival terminal id.
	 */
	public void setArrivalTerminalId(Integer arrivalTerminalId) {
		this.arrivalTerminalId = arrivalTerminalId;
	}
}
