/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * @Virsion $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.search;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableConnectedFlight;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableIBOBFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airinventory.api.dto.seatavailability.OriginDestinationInfoDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SelectedFlightDTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airschedules.api.dto.ConnectedAvailableFlightSegmentsDTO;
import com.isa.thinair.airschedules.api.utils.AirScheduleCustomConstants;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.FlightUtil;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.utils.CommandNames;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Command;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command to fill selected available flight
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="fillSelectedAvailableFlight"
 */
public class FillSelectedAvailableFlight extends DefaultBaseCommand {

	/**
	 * constructor of the FillSelectedAvailableFlight command
	 */
	public FillSelectedAvailableFlight() {
	}

	/**
	 * execute method of the FlightAvailabilitySearch command
	 * 
	 * @throws ModuleException
	 */
	@Override
	public ServiceResponce execute() throws ModuleException {

		// getting command params
		AvailableFlightSearchDTO avifltSearchDTO = (AvailableFlightSearchDTO) this
				.getParameter(CommandParamNames.AVILABLE_FLIGHT_SEARCH_DTO);
		boolean isTransactional = (Boolean) this.getParameter(CommandParamNames.IS_TRANSACTIONAL);

		// checking params
		this.checkParams(avifltSearchDTO);

		SelectedFlightDTO selectedFlightDTO;

		boolean isQuoteRequiredONDOnly = AppSysParamsUtil.isReQuoteOnlyRequiredONDs();

		ReservationApiUtils.splitPossibleOnDs(avifltSearchDTO);

		// merge possible ONDs for possible connection FareQuote
		ReservationApiUtils.mergePossibleOnD(avifltSearchDTO);

		// validate same fare quote is valid for current OND collection
		if (isQuoteRequiredONDOnly) {
			ReservationApiUtils.validateSkipSameFareQuote(avifltSearchDTO);
		}

		avifltSearchDTO.setSkipInvCheckForFlown(AppSysParamsUtil.skipInvChkForFlownInRequote());

		if (!avifltSearchDTO.isOpenReturnSearch() && AppSysParamsUtil.isSubJourneyDetectionEnabled()
				&& avifltSearchDTO.getOriginDestinationInfoDTOs().size() > 2) {
			List<Integer> appliedFlightSegIdList = new ArrayList<Integer>();
			List<OriginDestinationInfoDTO> allOndList = new ArrayList<OriginDestinationInfoDTO>();
			for (OriginDestinationInfoDTO ondInfo : avifltSearchDTO.getOriginDestinationInfoDTOs()) {

				// if same OND with another date added by add OND, need to skip that from quoting same fare by
				// existing date change behavior, that should be treated as NEW OND
				if (isQuoteRequiredONDOnly) {
					if ((!ondInfo.isUnTouchedOnd() || !ondInfo.isFlownOnd()) && ondInfo.getOldPerPaxFareTO() != null) {

						Integer appliedFltSegId = ondInfo.getOldPerPaxFareTO().getAppliedFlightSegId();

						if (appliedFlightSegIdList.contains(appliedFltSegId)) {
							// already old flight fare has been considered
							ondInfo.setOldPerPaxFareTO(null);
						}
						if (appliedFltSegId != null) {
							appliedFlightSegIdList.add(appliedFltSegId);
						}

					}

				}

				allOndList.add(ondInfo.clone());
			}
			avifltSearchDTO.setQuoteReturnSegmentDiscount(false);
			List<AvailableFlightSearchDTO> avifltSearchDTOList = ReservationApiUtils
					.getAvailableSubJourneyCollection(avifltSearchDTO);

			boolean isAllJourneyFareRequired = avifltSearchDTO.isOndSplitOperationInvoked()
					|| avifltSearchDTO.isRequoteFlightSearch() || avifltSearchDTO.isMultiCitySearch();
			selectedFlightDTO = getFareQuoteForSubJourney(avifltSearchDTOList, allOndList, isAllJourneyFareRequired,
					isTransactional);

		} else {
			selectedFlightDTO = getFareQuoteForJourney(avifltSearchDTO, false, isTransactional);
		}

		if(avifltSearchDTO.isOndSplitOperationInvoked()){ //AEROMART-3948 Re validate the param
			for(AvailableIBOBFlightSegment segmentFareFlight : selectedFlightDTO.getSelectedOndFlights()){
				segmentFareFlight.setSplitOperationForBusInvoked(true);
			}
		}
		
		// constructing and return command responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);
		responce.addResponceParam(ResponceCodes.FILL_SELECTED_FLIGHT_SUCCESSFULL, selectedFlightDTO);
		return responce;
	}

	private SelectedFlightDTO getFareQuoteForJourney(AvailableFlightSearchDTO availableFlightSearchDTO, boolean skipChargeQuote,
			boolean isTransactional) throws ModuleException {
		SelectedFlightDTO selectedFlightDTO = new SelectedFlightDTO();

		if (availableFlightSearchDTO.isOpenReturnSearch()) {
			selectedFlightDTO = getOpenReturnFareQuote(availableFlightSearchDTO, isTransactional);
		} else {
			selectedFlightDTO = getNormalFareQuote(availableFlightSearchDTO, skipChargeQuote, isTransactional);
		}

		// when same fare quote fails or returns no fares available, system should fire a fresh quote to see any
		// valid fares
		selectedFlightDTO = quoteFreshFaresForCurrentJourney(availableFlightSearchDTO, selectedFlightDTO, isTransactional);

		return selectedFlightDTO;
	}

	private SelectedFlightDTO quoteChargesForSelectedFlight(SelectedFlightDTO selectedFlightDTO,
			AvailableFlightSearchDTO availableFlightSearchDTO) throws ModuleException {
		return AirSchedulesUtil.getFlightInventoryResBD().chargeQuoteSelectedFlight(selectedFlightDTO, availableFlightSearchDTO);
	}

	/**
	 * Returns the normal fare quote
	 * 
	 * @param availableFlightSearchDTO
	 * @param skipChargeQuote
	 *            TODO
	 * @return
	 * @throws ModuleException
	 */
	private SelectedFlightDTO getNormalFareQuote(AvailableFlightSearchDTO availableFlightSearchDTO, boolean skipChargeQuote,
			boolean isTransactional) throws ModuleException {
		SelectedFlightDTO selectedFlightDTO = new SelectedFlightDTO();
		SelectedFlightDTO resultSelectedFlightDTO;

		int ondSequence = 0;
		for (OriginDestinationInfoDTO ondInfo : availableFlightSearchDTO.getOrderedOriginDestinations()) {
			Map<Integer, Boolean> ondSequenceReturnMap = availableFlightSearchDTO.getOndSequenceReturnMap();
			boolean isIBFlight = false;

			if (ondSequenceReturnMap != null && ondSequenceReturnMap.size() > 1
					&& ondSequenceReturnMap.get(ondSequence) != null) {
				isIBFlight = ondSequenceReturnMap.get(ondSequence);
			}

			FlightUtil.updateAvailableFlightSegments(ondInfo.getFlightSegmentIds(), ondInfo.getPreferredClassOfService(),
					selectedFlightDTO, ondSequence++, isIBFlight, ondInfo.isFlownOnd(), ondInfo.isUnTouchedOnd(), null);
		}

		// TODO charith need to pass selected logical cabin class for fare filtering
		updateOverbookSeats(selectedFlightDTO, availableFlightSearchDTO);

		if (isTransactional) {
			resultSelectedFlightDTO = AirSchedulesUtil.getFlightInventoryResBD()
					.searchSelectedFlightsSeatAvailabilityTnx(selectedFlightDTO, availableFlightSearchDTO, skipChargeQuote);
		} else {
			resultSelectedFlightDTO = AirSchedulesUtil.getFlightInventoryResBD()
					.searchSelectedFlightsSeatAvailability(selectedFlightDTO, availableFlightSearchDTO, skipChargeQuote);
		}
		return resultSelectedFlightDTO;
	}

	private void updateOverbookSeats(SelectedFlightDTO selectedFlightDTO, AvailableFlightSearchDTO availableFlightSearchDTO) {
		List<List<AvailableIBOBFlightSegment>> ondAvailFlightSegments = selectedFlightDTO.getOndWiseSelectedDateAllFlights();
		for (List<AvailableIBOBFlightSegment> fltSegments : ondAvailFlightSegments) {
			for (AvailableIBOBFlightSegment fltSegment : fltSegments) {
				for (FlightSegmentDTO fltSegDTO : fltSegment.getFlightSegmentDTOs()) {
					if (fltSegDTO.getAvailableAdultCount() != null) {
						if (fltSegDTO.isFlightWithinCutoff() && availableFlightSearchDTO.isAllowDoOverbookAfterCutOffTime()) {
							for (Integer adultCount : fltSegDTO.getAvailableAdultCount().values()) {
								if ((availableFlightSearchDTO.getAdultCount()
										+ availableFlightSearchDTO.getChildCount()) > adultCount) {
									fltSegDTO.setOverBookEnabled(true);
								}
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Composes the out bound flights
	 * 
	 * @param avifltSearchDTO
	 * @return
	 * @throws ModuleException
	 */
	private Object[] getOutBoundFlightsInfo(AvailableFlightSearchDTO avifltSearchDTO) throws ModuleException {
		Collection<Integer> outboundFlights = avifltSearchDTO.getOndInfo(OndSequence.OUT_BOUND).getFlightSegmentIds();
		AvailableIBOBFlightSegment availableFlightSegment = null;
		Set<Date> dateWiseFlights = new TreeSet<Date>();

		if (!outboundFlights.isEmpty()) {
			Collection<AvailableFlightSegment> segmentsList = AirSchedulesUtil.getFlightBD().getAvailableFlightSegments(
					outboundFlights, avifltSearchDTO.getOndInfo(OndSequence.OUT_BOUND).getPreferredClassOfService(), false,
					OndSequence.OUT_BOUND, false, false);

			if (segmentsList.size() != outboundFlights.size()) {
				throw new ModuleException("airinventory.arg.farequote.flightclosed");
			}

			// fill the out bound flight
			if (segmentsList.size() == 1) {
				AvailableFlightSegment availSeg = segmentsList.iterator().next();
				availableFlightSegment = availSeg;
				dateWiseFlights.add(availSeg.getFlightSegmentDTO().getDepartureDateTime());
			} else {
				ConnectedAvailableFlightSegmentsDTO connectedAvailableFlightSegmentsDTO = new ConnectedAvailableFlightSegmentsDTO();
				if (segmentsList.size() > 0) {

					Iterator<Integer> it = outboundFlights.iterator();
					while (it.hasNext()) {
						int curernt = (it.next()).intValue();
						AvailableFlightSegment avaiseg = getAvailableFlightSegemnt(segmentsList, curernt);
						dateWiseFlights.add(avaiseg.getFlightSegmentDTO().getDepartureDateTime());
					}

					connectedAvailableFlightSegmentsDTO.getAvailableFlightSegments().addAll(segmentsList);
					availableFlightSegment = connectedAvailableFlightSegmentsDTO.constructAvailableConnectedFlight();
				}
			}
		}

		Date departureDate = BeanUtils.getFirstElement(dateWiseFlights);
		return new Object[] { availableFlightSegment, departureDate };
	}

	/**
	 * Returns the inbound flight information
	 * 
	 * @param avifltSearchDTO
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	private SelectedFlightDTO getOpenReturnFareQuote(AvailableFlightSearchDTO avifltSearchDTO, boolean isTransactional)
			throws ModuleException {

		Object[] outboundInfo = getOutBoundFlightsInfo(avifltSearchDTO);
		SelectedFlightDTO selectedFlightDTO;

		if (outboundInfo[1] != null) {
			Date depDateTimeStart = CalendarUtil.getStartTimeOfDate((Date) outboundInfo[1]);
			Date depDateTimeEnd = CalendarUtil.getEndTimeOfDate((Date) outboundInfo[1]);

			// Update Departure date start/end time in OND information
			for (OriginDestinationInfoDTO ondInfo : avifltSearchDTO.getOrderedOriginDestinations()) {
				ondInfo.setDepartureDateTimeStart(depDateTimeStart);
				ondInfo.setDepartureDateTimeEnd(depDateTimeEnd);
				ondInfo.setPreferredDateTimeStart(depDateTimeStart);
				ondInfo.setPreferredDateTimeEnd(depDateTimeEnd);
			}
		}

		Command command = (Command) AirSchedulesUtil.getInstance().getLocalBean(CommandNames.FLIGHT_AVAILABILITY_SEARCH);
		command.setParameter(CommandParamNames.AVILABLE_FLIGHT_SEARCH_DTO, avifltSearchDTO);
		command.setParameter(CommandParamNames.SEARCH_OUT_BOUND_FLIGHTS, Boolean.FALSE);
		ServiceResponce serviceResponce = command.execute();

		AvailableFlightDTO availableFlightDTO = (AvailableFlightDTO) serviceResponce
				.getResponseParam(ResponceCodes.AVAILABILITY_SEARCH_SUCCESSFULL);

		if (availableFlightDTO != null) {

			if (outboundInfo[0] != null) {
				List<AvailableIBOBFlightSegment> availIBOBList = Arrays.asList((AvailableIBOBFlightSegment) outboundInfo[0]);
				availableFlightDTO.getAvailableOndFlights(OndSequence.OUT_BOUND).addAll(availIBOBList);
				availableFlightDTO.getSelectedFlight().getOndWiseSelectedDateAllFlights(OndSequence.OUT_BOUND).clear();
				availableFlightDTO.getSelectedFlight().getOndWiseSelectedDateAllFlights(OndSequence.OUT_BOUND)
						.addAll(availIBOBList);
			}

			if (isTransactional) {
				selectedFlightDTO = AirSchedulesUtil.getFlightInventoryResBD()
						.searchSelectedFlightsSeatAvailabilityTnx(availableFlightDTO.getSelectedFlight(), avifltSearchDTO, false);
			} else {
				selectedFlightDTO = AirSchedulesUtil.getFlightInventoryResBD()
						.searchSelectedFlightsSeatAvailability(availableFlightDTO.getSelectedFlight(), avifltSearchDTO, false);
			}
			return selectedFlightDTO;
		}

		throw new ModuleException("airinventory.logic.farequote.invalidflight");
	}

	/**
	 * privte method to validate parameters
	 * 
	 * @param schedule
	 * @throws ModuleException
	 */
	private void checkParams(AvailableFlightSearchDTO avifltSearchDTO) throws ModuleException {

		if (avifltSearchDTO == null) {
			throw new ModuleException("airschedules.arg.invalid.null");
		}
	}

	/**
	 * private method to get the available flight segment
	 * 
	 * @param avaiSegList
	 * @param segemntID
	 * @return
	 */
	private AvailableFlightSegment getAvailableFlightSegemnt(Collection<AvailableFlightSegment> avaiSegList, int segemntID) {
		Iterator<AvailableFlightSegment> it = avaiSegList.iterator();
		while (it.hasNext()) {

			AvailableFlightSegment current = (AvailableFlightSegment) it.next();
			if (current.getFlightSegmentDTO().getSegmentId().intValue() == segemntID) {
				return current;
			}
		}

		return null;
	}

	private int getOndSequence(List<OriginDestinationInfoDTO> allOndList, ArrayList<Integer> fltSegIdList) {
		int ondSequence = 0;
		if (allOndList != null && allOndList.size() > 0 && fltSegIdList != null && fltSegIdList.size() > 0) {
			for (OriginDestinationInfoDTO ondInfoTemp : allOndList) {
				if (ondInfoTemp.getFlightSegmentIds().containsAll(fltSegIdList)) {
					// break;
					return ondSequence;
				}
				ondSequence++;
			}
		}

		return 0;
	}

	private ArrayList<Integer> getFlightSegIdList(List<FlightSegmentDTO> flightSegmentDTOs) {
		ArrayList<Integer> fltSegIdList = new ArrayList<Integer>();
		for (FlightSegmentDTO fltSegtDTO : flightSegmentDTOs) {
			fltSegIdList.add(fltSegtDTO.getSegmentId());
		}
		return fltSegIdList;
	}

	private SelectedFlightDTO getFareQuoteForSubJourney(List<AvailableFlightSearchDTO> avifltSearchDTOList,
			List<OriginDestinationInfoDTO> allOndList, boolean isAllJourneyFareRequired, boolean isTransactional)
			throws ModuleException {

		List<SelectedFlightDTO> selectedFlightDTOList = new ArrayList<SelectedFlightDTO>();

		Iterator<AvailableFlightSearchDTO> avifltSearchDTOListItr = avifltSearchDTOList.iterator();
		while (avifltSearchDTOListItr.hasNext()) {
			AvailableFlightSearchDTO avifltSearchDTODum = avifltSearchDTOListItr.next();
			SelectedFlightDTO subJourneySelFltDTO = getFareQuoteForJourney(avifltSearchDTODum, true, isTransactional);

			selectedFlightDTOList.add(subJourneySelFltDTO);
		}
		// boolean isBusSegmentExists = false;
		// Map<Integer, Integer> sameTypeSegs = new HashMap<Integer, Integer>();
		// if (AppSysParamsUtil.isEnableOndSplitForBusFareQuote()) {
		// for (SelectedFlightDTO selFltSearchDTO : selectedFlightDTOList) {
		// if (selFltSearchDTO.getOndWiseSelectedDateAllFlights() != null
		// && selFltSearchDTO.getOndWiseSelectedDateAllFlights().size() > 0) {
		//
		// for (List<AvailableIBOBFlightSegment> availIBOBFltSegList : selFltSearchDTO
		// .getOndWiseSelectedDateAllFlights()) {
		// for (AvailableIBOBFlightSegment availIBOBFltSeg : availIBOBFltSegList) {
		// for (FlightSegmentDTO seg : availIBOBFltSeg.getFlightSegmentDTOs()) {
		// if (seg.getOperationTypeID() == AirScheduleCustomConstants.OperationTypes.BUS_SERVICE) {
		// isBusSegmentExists = true;
		// }
		// sameTypeSegs.put(seg.getSegmentId(), availIBOBFltSeg.getOndSequence());
		// }
		// }
		// }
		// }
		// }
		// }

		int subJourneyGroupId = 1;
		SelectedFlightDTO selectedFlightDTO = new SelectedFlightDTO();
		for (SelectedFlightDTO selFltSearchDTO : selectedFlightDTOList) {

			if (selFltSearchDTO.getOndWiseSelectedDateAllFlights() != null
					&& selFltSearchDTO.getOndWiseSelectedDateAllFlights().size() > 0) {

				for (List<AvailableIBOBFlightSegment> availIBOBFltSegList : selFltSearchDTO.getOndWiseSelectedDateAllFlights()) {
					for (AvailableIBOBFlightSegment availIBOBFltSeg : availIBOBFltSegList) {

						ArrayList<Integer> fltSegIdList = getFlightSegIdList(availIBOBFltSeg.getFlightSegmentDTOs());

						int ondSequence = getOndSequence(allOndList, fltSegIdList);
						// for(Integer segId : fltSegIdList){
						// if(sameTypeSegs.get(segId) != null && isBusSegmentExists){
						// ondSequence = sameTypeSegs.get(segId);
						// }
						// }
						availIBOBFltSeg.setOndSequence(ondSequence);
						availIBOBFltSeg.setSubJourneyGroup(subJourneyGroupId);

						updateConnectedFlightSubJourneyInfo(availIBOBFltSeg);

					}
				}

				selectedFlightDTO.getOndWiseSelectedDateAllFlights().addAll(selFltSearchDTO.getOndWiseSelectedDateAllFlights());

			}

			if (selFltSearchDTO.getSelectedOndFlights() != null && selFltSearchDTO.getSelectedOndFlights().size() > 0) {

				for (AvailableIBOBFlightSegment availIBOBFltSeg : selFltSearchDTO.getSelectedOndFlights()) {

					ArrayList<Integer> fltSegIdList = getFlightSegIdList(availIBOBFltSeg.getFlightSegmentDTOs());

					int ondSequence = getOndSequence(allOndList, fltSegIdList);
					// for (Integer segId : fltSegIdList) {
					// if (sameTypeSegs.get(segId) != null && isBusSegmentExists) {
					// ondSequence = sameTypeSegs.get(segId);
					// }
					// }
					availIBOBFltSeg.setOndSequence(ondSequence);
					availIBOBFltSeg.setSubJourneyGroup(subJourneyGroupId);
					availIBOBFltSeg.setSelectedFlightDTO(selectedFlightDTO);
					updateConnectedFlightSubJourneyInfo(availIBOBFltSeg);
					selectedFlightDTO.setSelectedOndFlight(ondSequence, availIBOBFltSeg);

				}
			}

			if (selFltSearchDTO.getOndWiseMinimumSegFareFlights() != null
					&& selFltSearchDTO.getOndWiseMinimumSegFareFlights().size() > 0) {

				for (AvailableIBOBFlightSegment availIBOBFltSeg : selFltSearchDTO.getOndWiseMinimumSegFareFlights()) {

					ArrayList<Integer> fltSegIdList = getFlightSegIdList(availIBOBFltSeg.getFlightSegmentDTOs());

					int ondSequence = getOndSequence(allOndList, fltSegIdList);
					// for (Integer segId : fltSegIdList) {
					// if (sameTypeSegs.get(segId) != null && isBusSegmentExists) {
					// ondSequence = sameTypeSegs.get(segId);
					// }
					// }
					availIBOBFltSeg.setOndSequence(ondSequence);
					availIBOBFltSeg.setSubJourneyGroup(subJourneyGroupId);

					updateConnectedFlightSubJourneyInfo(availIBOBFltSeg);

					selectedFlightDTO.setSelectedOndSegFlight(ondSequence, availIBOBFltSeg);
				}

			}

			if (selFltSearchDTO.getOndWiseSelectedDateAllSegFareFlights() != null
					&& selFltSearchDTO.getOndWiseSelectedDateAllSegFareFlights().size() > 0) {

				for (List<AvailableIBOBFlightSegment> availIBOBFltSegList : selFltSearchDTO
						.getOndWiseSelectedDateAllSegFareFlights()) {
					for (AvailableIBOBFlightSegment availIBOBFltSeg : availIBOBFltSegList) {

						ArrayList<Integer> fltSegIdList = getFlightSegIdList(availIBOBFltSeg.getFlightSegmentDTOs());

						int ondSequence = getOndSequence(allOndList, fltSegIdList);
						// for (Integer segId : fltSegIdList) {
						// if (sameTypeSegs.get(segId) != null && isBusSegmentExists) {
						// ondSequence = sameTypeSegs.get(segId);
						// }
						// }
						availIBOBFltSeg.setOndSequence(ondSequence);
						availIBOBFltSeg.setSubJourneyGroup(subJourneyGroupId);

						updateConnectedFlightSubJourneyInfo(availIBOBFltSeg);

					}
				}

				selectedFlightDTO.getOndWiseSelectedDateAllSegFareFlights()
						.addAll(selFltSearchDTO.getOndWiseSelectedDateAllSegFareFlights());

			}

			subJourneyGroupId++;
		}
		boolean isJourneyWithNoFares = validateTotalJourneyFareQuote(selectedFlightDTO, isAllJourneyFareRequired);
		// when multiple journey quoted then all journey ONDs must have a fare, if not no need to fire charge quote
		if (!isJourneyWithNoFares) {
			AvailableFlightSearchDTO allOndFlightSearchDTO = avifltSearchDTOList.get(0).clone();
			allOndFlightSearchDTO.getOriginDestinationInfoDTOs().clear();
			for (OriginDestinationInfoDTO ondInfoDTO : allOndList) {
				allOndFlightSearchDTO.addOriginDestination(ondInfoDTO);
			}
			selectedFlightDTO = quoteChargesForSelectedFlight(selectedFlightDTO, allOndFlightSearchDTO);
		}
		return selectedFlightDTO;
	}

	private void updateConnectedFlightSubJourneyInfo(AvailableIBOBFlightSegment availIBOBFltSeg) {
		if (availIBOBFltSeg instanceof AvailableConnectedFlight) {
			((AvailableConnectedFlight) availIBOBFltSeg).getAvailableFlightSegments();

			for (AvailableFlightSegment availableFlightSegment : ((AvailableConnectedFlight) availIBOBFltSeg)
					.getAvailableFlightSegments()) {
				availableFlightSegment.setSubJourneyGroup(availIBOBFltSeg.getSubJourneyGroup());
				availableFlightSegment.setOndSequence(availIBOBFltSeg.getOndSequence());
			}

		}
	}

	/**
	 * In Re-Quote/Multi-city/OnD Split enabled for bus fare flow, system should check for the all sub-journeys has
	 * valid fare. if a single journey is without fare then rest of the sub-journey cannot be served or modified.
	 */
	public boolean validateTotalJourneyFareQuote(SelectedFlightDTO selectedFlightDTO, boolean isAllJourneyFareRequired) {
		boolean isJourneyWithNoFares = false;
		if (isAllJourneyFareRequired && selectedFlightDTO != null && selectedFlightDTO.getSelectedOndFlights() != null
				&& selectedFlightDTO.getOndWiseSelectedDateAllFlights() != null
				&& selectedFlightDTO.getSelectedOndFlights().size() > 0) {
			if (selectedFlightDTO.getOndWiseSelectedDateAllFlights().size() != selectedFlightDTO.getSelectedOndFlights().size()) {
				selectedFlightDTO.getSelectedOndFlights().clear();
				isJourneyWithNoFares = true;
			} else {
				for (int a = 0; a < selectedFlightDTO.getSelectedOndFlights().size(); a++) {
					if (selectedFlightDTO.getSelectedOndFlights().get(a) == null) {
						selectedFlightDTO.getSelectedOndFlights().clear();
						isJourneyWithNoFares = true;
						break;
					}
				}
			}

		}
		return isJourneyWithNoFares;
	}

	private SelectedFlightDTO quoteFreshFaresForCurrentJourney(AvailableFlightSearchDTO avifltSearchDTO,
			SelectedFlightDTO journeySelFltDTO, boolean isTransactional) throws ModuleException {
		if (avifltSearchDTO != null && avifltSearchDTO.isQuoteSameFareBucket()
				&& (journeySelFltDTO == null || (journeySelFltDTO != null && journeySelFltDTO.getOndWiseSelectedDateAllFlights()
						.size() != journeySelFltDTO.getSelectedOndFlights().size()))) {
			avifltSearchDTO.setSkipSameFareBucketQuote(true);

			for (OriginDestinationInfoDTO ondInfoDTO : avifltSearchDTO.getOrderedOriginDestinations()) {
				ondInfoDTO.setUnTouchedOnd(false);
			}
			journeySelFltDTO = getFareQuoteForJourney(avifltSearchDTO, false, isTransactional);

		}

		return journeySelFltDTO;
	}
}
