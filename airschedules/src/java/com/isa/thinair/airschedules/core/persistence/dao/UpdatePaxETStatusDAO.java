package com.isa.thinair.airschedules.core.persistence.dao;


import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airschedules.api.dto.FlightBasicInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

import java.util.Collection;

public interface UpdatePaxETStatusDAO {

	public Collection<FlightBasicInfoDTO> getFlownFlights(FlightBasicInfoDTO flownFlightsSearchDTO, int startIndex, int pageLength) throws ModuleException;

	public void updatePaxETStatus(String flightNumber, String departureDate) throws ModuleException;

	public void updatePaxETStatus(Collection<Integer> getPaxFareSegIds) throws ModuleException;

	public void updateReservationAudit(String flightNumber, String flightDeptDate, CredentialsDTO credentialsDTO)
			throws ModuleException;


}
