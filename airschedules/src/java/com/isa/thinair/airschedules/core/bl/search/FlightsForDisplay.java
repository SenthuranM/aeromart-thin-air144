/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.search;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airschedules.api.criteria.FlightFlightSegmentSearchCriteria;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.LocalZuluTimeAdder;
import com.isa.thinair.airschedules.api.utils.ModelNamesAndFieldNames;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.airschedules.core.persistence.dao.FlightDAO;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.CriteriaQueryUtil;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.commons.api.dto.ModuleCriteria;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for search flights for display
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="flightsForDisplay"
 */
public class FlightsForDisplay extends DefaultBaseCommand {

	// DAOs
	private final FlightDAO flightDAO;

	// BDs
	private final AirportBD airportBD;

	// helpers
	private final LocalZuluTimeAdder timeAdder;

	/**
	 * constructor of the FlightsForDisplay command
	 */
	public FlightsForDisplay() {

		// DAOs
		flightDAO = (FlightDAO) AirSchedulesUtil.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_DAO);

		// BDs
		airportBD = AirSchedulesUtil.getAirportBD();

		// helpers
		timeAdder = new LocalZuluTimeAdder(airportBD);
	}

	/**
	 * execute method of the FlightsForDisplay command
	 * 
	 * @throws ModuleException
	 */
	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ServiceResponce execute() throws ModuleException {

		// getting command params
		List criteria = (List) this.getParameter(CommandParamNames.CRITERIA_LIST);
		Integer startIndex = (Integer) this.getParameter(CommandParamNames.START_INDEX);
		Integer pageSize = (Integer) this.getParameter(CommandParamNames.PAGE_SIZE);
		Boolean datesInLocal = (Boolean) this.getParameter(CommandParamNames.IS_DATE_IN_LOCAL);
		Boolean isFlightManifest = (Boolean) this.getParameter(CommandParamNames.IS_FLIGHT_MANIFEST);
		Boolean isDataSourceReporting = (Boolean) this.getParameter(CommandParamNames.IS_REPORTING);
		FlightFlightSegmentSearchCriteria flightCriteria = (FlightFlightSegmentSearchCriteria) this
				.getParameter(CommandParamNames.FLIGHT_FLIGHTSEGMENT_CRITERIA_LIST);
		List<String> agentApplicableOnds = (List<String>) this.getParameter(CommandParamNames.AGENT_APPLICABLE_ONDS);
		boolean blnLocalinDestTime = false;

		// checking params
		checkParams(criteria, startIndex, pageSize, datesInLocal, isFlightManifest, flightCriteria);

		String origin = CriteriaQueryUtil.getOriginOfFligthSearch(criteria);
		String destination = CriteriaQueryUtil.getDesinationOfFligthSearch(criteria);
		Page<Flight> page = null;

		if (isFlightManifest) {
			if (!datesInLocal.booleanValue()) {
				// Dates in zulu time and any origin
				Date searchStartDate = CriteriaQueryUtil.getStartDateOfFligthSearch(criteria);
				Date searchEndDate = CriteriaQueryUtil.getEndDateOfFligthSearch(criteria);

				if (searchStartDate != null) {

					Date startDate = CalendarUtil.getStartTimeOfDate(searchStartDate);
					criteria = CriteriaQueryUtil.setStartDateOfFligthSearch(criteria, startDate);
				}

				if (searchEndDate != null) {
					Date endDate = CalendarUtil.getEndTimeOfDate(searchEndDate);
					criteria = CriteriaQueryUtil.setEndDateOfFligthSearch(criteria, endDate);
				}
			} else if (datesInLocal.booleanValue() && (origin != null || destination != null)) {
				// Dates in local and origin is given
				String station = "";
				if (origin != null) {
					station = origin;
				} else {
					station = destination;
					blnLocalinDestTime = true;
				}

				Date searchStartDate = CriteriaQueryUtil.getStartDateOfFligthSearch(criteria);
				Date searchEndDate = CriteriaQueryUtil.getEndDateOfFligthSearch(criteria);

				if (searchStartDate != null) {
					Date startDate = timeAdder.getZuluDateTime(station, searchStartDate);
					criteria = CriteriaQueryUtil.setStartDateOfFligthSearch(criteria, startDate);

					if (searchEndDate != null) {

						Date endDate = timeAdder.getZuluDateTimeAsEffective(station, startDate,
								CalendarUtil.getEndTimeOfDate(searchEndDate));
						criteria = CriteriaQueryUtil.setEndDateOfFligthSearch(criteria, endDate);
					}
				}
			} else {
				// Dates in local time and origin is not given
				Date searchStartDate = CriteriaQueryUtil.getStartDateOfFligthSearch(criteria);
				Date searchEndDate = CriteriaQueryUtil.getEndDateOfFligthSearch(criteria);
				criteria = CriteriaQueryUtil.removeStartDateOfFligthSearch(criteria);
				criteria = CriteriaQueryUtil.removeEndDateOfFligthSearch(criteria);

				String flightNumber = CriteriaQueryUtil.getFlightNumberOfFligthSearch(criteria);

				if (flightNumber != null) {
					// Get departing stations of the flight number
					List<String> origins = flightDAO.getFlightOrigins(flightNumber);

					// Get new origin criteria list
					List<ModuleCriteria> originCriteriaList = new ArrayList<ModuleCriteria>();

					// Find start and end dates of the stations in local time
					for (String string : origins) {
						String oneOfOrigins = string;
						Date startDate = null;
						Date endDate = null;
						if (searchStartDate != null) {
							startDate = timeAdder.getZuluDateTime(oneOfOrigins, searchStartDate);
							if (searchEndDate != null) {
								endDate = timeAdder.getZuluDateTimeAsEffective(oneOfOrigins, startDate,
										CalendarUtil.getEndTimeOfDate(searchEndDate));
							}
						}
						// Add new conditions to the search criteria

						// Build the criteria
						ModuleCriteria moduleCriteria = new ModuleCriteria();
						moduleCriteria.setJoinCondition(ModuleCriteria.JOIN_CONDITION_AND);

						// departure airport code of the flight
						ModuleCriterion originCriterion = new ModuleCriterion();
						originCriterion.setCondition(ModuleCriterion.CONDITION_EQUALS);
						originCriterion.setFieldName(ModelNamesAndFieldNames.Flight.FieldNames.ORIGIN_APT_CODE);
						List<String> valueFromStn = new ArrayList<String>();
						valueFromStn.add(oneOfOrigins);
						originCriterion.setValue(valueFromStn);
						moduleCriteria.setLeftCriterion(originCriterion);

						if (startDate != null && endDate != null) {
							// start date and end date of the schedule
							ModuleCriterion moduleCriterionDates = new ModuleCriterion();
							moduleCriterionDates.setCondition(ModuleCriterion.CONDITION_BETWEEN);
							moduleCriterionDates.setFieldName(ModelNamesAndFieldNames.Flight.FieldNames.DEPARTURE_DATE);
							List<Date> valueDates = new ArrayList<Date>();
							valueDates.add(startDate);
							valueDates.add(endDate);
							moduleCriterionDates.setValue(valueDates);

							moduleCriteria.setRightCriterion(moduleCriterionDates);

						} else {
							// setting the start date as criteria
							if (startDate != null) {

								ModuleCriterion moduleCriterionStartD = new ModuleCriterion();

								moduleCriterionStartD.setCondition(ModuleCriterion.CONDITION_GREATER_THAN_OR_EQUALS);
								// start date of the schedule
								moduleCriterionStartD.setFieldName(ModelNamesAndFieldNames.Flight.FieldNames.DEPARTURE_DATE);
								List<Date> valueStartDate = new ArrayList<Date>();
								valueStartDate.add(startDate);

								moduleCriterionStartD.setValue(valueStartDate);

								moduleCriteria.setRightCriterion(moduleCriterionStartD);

							}
							// setting the stop date as criteria
							if (endDate != null) {

								ModuleCriterion moduleCriterionStopD = new ModuleCriterion();

								moduleCriterionStopD.setCondition(ModuleCriterion.CONDITION_LESS_THAN_OR_EQUALS);
								// stop date of the schedule
								moduleCriterionStopD.setFieldName(ModelNamesAndFieldNames.Flight.FieldNames.DEPARTURE_DATE);

								List<Date> valueStopDate = new ArrayList<Date>();
								valueStopDate.add(endDate);

								moduleCriterionStopD.setValue(valueStopDate);

								moduleCriteria.setRightCriterion(moduleCriterionStopD);

							}

						}
						originCriteriaList.add(moduleCriteria);
					}
					if (!originCriteriaList.isEmpty()) {
						criteria.add(BuildOriginCriteria(originCriteriaList));
					}

				}
			}

			List<ModuleCriterion> terrList = CriteriaQueryUtil.getTerrotoryList(criteria);

			if (terrList != null && !terrList.isEmpty()) {
				criteria = CriteriaQueryUtil.removeTerritorys(criteria);
				
				// Build the criteria
				ModuleCriteria moduleCriteria = new ModuleCriteria();
				moduleCriteria.setJoinCondition(ModuleCriteria.JOIN_CONDITION_OR);

				ModuleCriterion orginTerritoryes = new ModuleCriterion();
				orginTerritoryes.setFieldName(ModelNamesAndFieldNames.Flight.FieldNames.DESTINATION_APT_CODE);
				orginTerritoryes.setCondition(ModuleCriterion.CONDITION_IN);
				orginTerritoryes.setValue(terrList);
				moduleCriteria.setLeftCriterion(orginTerritoryes);

				ModuleCriterion destTerritoryes = new ModuleCriterion();
				destTerritoryes.setFieldName(ModelNamesAndFieldNames.Flight.FieldNames.ORIGIN_APT_CODE);
				destTerritoryes.setCondition(ModuleCriterion.CONDITION_IN);
				destTerritoryes.setValue(terrList);
				moduleCriteria.setRightCriterion(destTerritoryes);

				List terrCriteriaList = new ArrayList();
				terrCriteriaList.add(moduleCriteria);
				
				if (agentApplicableOnds != null && !agentApplicableOnds.isEmpty()) {
					if (!agentApplicableOnds.contains("***/***")) {
						List<String> airportsToAnyDestination = getAllowedAirports(agentApplicableOnds, 1);
						List<String> airportsFromAnyOrigin = getAllowedAirports(agentApplicableOnds, 0);
						List<String> namedOnds = getNamedOnds(agentApplicableOnds);
						
						if(airportsToAnyDestination != null && !airportsToAnyDestination.isEmpty()) {
							ModuleCriterion anyDestinationCriterian = new ModuleCriterion();
							anyDestinationCriterian.setFieldName(ModelNamesAndFieldNames.Flight.FieldNames.ORIGIN_APT_CODE);
							anyDestinationCriterian.setCondition(ModuleCriterion.CONDITION_IN);
							anyDestinationCriterian.setValue(airportsToAnyDestination);
							terrCriteriaList.add(anyDestinationCriterian);						
						}
						
						if(airportsFromAnyOrigin != null && !airportsFromAnyOrigin.isEmpty()) {
							ModuleCriterion anyOriginCriterian = new ModuleCriterion();
							anyOriginCriterian.setFieldName(ModelNamesAndFieldNames.Flight.FieldNames.DESTINATION_APT_CODE);
							anyOriginCriterian.setCondition(ModuleCriterion.CONDITION_IN);
							anyOriginCriterian.setValue(airportsFromAnyOrigin);
							terrCriteriaList.add(anyOriginCriterian);
						}
						
						if (namedOnds != null && namedOnds.size() > 0) {
							for (String namedOnd : namedOnds) {
								ModuleCriteria definedOndCriteria = new ModuleCriteria();
								definedOndCriteria.setJoinCondition(ModuleCriteria.JOIN_CONDITION_AND);
								ModuleCriterion originCriterian = new ModuleCriterion();
								originCriterian.setCondition(ModuleCriterion.CONDITION_IN);
								originCriterian.setFieldName(ModelNamesAndFieldNames.Flight.FieldNames.ORIGIN_APT_CODE);
								originCriterian.setValue(Arrays.asList(namedOnd.split("/")[0]));
								definedOndCriteria.setLeftCriterion(originCriterian);

								ModuleCriterion destinationCriterian = new ModuleCriterion();
								destinationCriterian.setCondition(ModuleCriterion.CONDITION_IN);
								destinationCriterian.setFieldName(ModelNamesAndFieldNames.Flight.FieldNames.DESTINATION_APT_CODE);
								destinationCriterian.setValue(Arrays.asList(namedOnd.split("/")[1]));
								definedOndCriteria.setRightCriterion(destinationCriterian);

								terrCriteriaList.add(definedOndCriteria);	
								
							}
						}
						criteria.add(BuildOriginCriteria(terrCriteriaList));						
						
					}
				} else {
					criteria.add(BuildOriginCriteria(terrCriteriaList));
				}		

			}
			
			try {
				page = flightDAO.searchFlightNew(criteria, startIndex.intValue(), pageSize.intValue(),
						isDataSourceReporting);
			} catch (Exception e) {
				throw new ModuleException("", e);
			}
		} else {
			// For checkin
			page = flightDAO.searchFlightForCheckin(flightCriteria, startIndex.intValue(), pageSize.intValue());
		}

		if (datesInLocal.booleanValue()) {
			// Ajust zulu time to local in details list
			List<Flight> retrievedFlightList = (page == null || page.getPageData() == null || page.getPageData().size() < 1)
					? new ArrayList<Flight>()
					: (List<Flight>) page.getPageData();

			// if flight list has flights
			if (retrievedFlightList.size() > 0) {
				for (int i = 0; i < retrievedFlightList.size(); i++) {
					Flight tempFlight = retrievedFlightList.get(i);
					(retrievedFlightList.get(i)).setDepartureDate(timeAdder.getLocalDateTime(tempFlight.getOriginAptCode(),
							tempFlight.getDepartureDate()));
				}
			}
		}

		// constructing and return command responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);
		responce.addResponceParam(CommandParamNames.PAGE, page);
		if (blnLocalinDestTime) {
			responce.setResponseCode(ResponceCodes.SEARCH_FLT_DESTINATION_LOCAL);
		}
		return responce;
	}

	@SuppressWarnings("rawtypes")
	private ModuleCriteria BuildOriginCriteria(List originCriteriaList) {

		if (!originCriteriaList.isEmpty()) {
			if (originCriteriaList.size() > 0 && originCriteriaList.size() == 1) {
				return (ModuleCriteria) originCriteriaList.get(0);
			} else {
				ModuleCriteria moduleCriteriaA = new ModuleCriteria();
				moduleCriteriaA.setJoinCondition(ModuleCriteria.JOIN_CONDITION_OR);
				ModuleCriteria moduleCriteriaB = moduleCriteriaA;
				moduleCriteriaA.setLeftCriterion((ModuleCriterion) originCriteriaList.get(0));
				int i = 1;
				while (i < originCriteriaList.size()) {
					moduleCriteriaA.setRightCriterion((ModuleCriterion) originCriteriaList.get(i));
					if (i < originCriteriaList.size() - 1) {
						moduleCriteriaA = new ModuleCriteria();
						moduleCriteriaA.setJoinCondition(ModuleCriteria.JOIN_CONDITION_OR);
						moduleCriteriaA.setLeftCriterion(moduleCriteriaB);
						moduleCriteriaB = moduleCriteriaA;
					}
					i++;
				}
				return moduleCriteriaA;
			}
		}
		return null;
	}

	/**
	 * privte method to validate parameters
	 * 
	 * @param schedule
	 * @throws ModuleException
	 */
	@SuppressWarnings("rawtypes")
	private void checkParams(List criteria, Integer startIndex, Integer pageSize, Boolean datesInLocal, Boolean isFlightManifest,
			FlightFlightSegmentSearchCriteria flightCriteria) throws ModuleException {

		if (startIndex == null || pageSize == null || datesInLocal == null || isFlightManifest == null) {
			// exception
			throw new ModuleException("airschedules.arg.invalid.null");
		}
		if (isFlightManifest) {
			if (criteria == null) {
				throw new ModuleException("airschedules.arg.invalid.null");
			}
		} else {
			if (flightCriteria == null) {
				throw new ModuleException("airschedules.arg.invalid.null");
			}
		}
	}
	
	private List<String> getAllowedAirports(List<String> onds, int index) {
		List<String> airports = new ArrayList<String>();
		for(String ond : onds) {
			String elements[] = ond.split("/");
			if(elements[index].equals("***")) {
				airports.add(elements[index == 1 ? 0: 1]);
			}
		}
		return airports;
	}
	
	private List<String> getNamedOnds(List<String> onds) {
		List<String> namedOnds = new ArrayList<String>();
		for(String ond : onds) {
			if(!ond.contains("***")) {
				namedOnds.add(ond);
			}
		}
		return namedOnds;
	}
}
