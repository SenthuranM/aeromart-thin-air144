package com.isa.thinair.airschedules.core.bl.audit;

/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */

import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.auditor.api.model.Audit;
import com.isa.thinair.auditor.api.service.AuditorBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.TasksUtil;
import com.isa.thinair.commons.core.security.UserPrincipal;

import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Class to implement the logics to save audits for flight information changes.
 * 
 * @author dumindag
 * @version 0.1
 */
public class AuditAirSchedules {
	AuditorBD auditorBD = AirSchedulesUtil.getAuditorBD();

	public void doAuditForFlightStatusChange(Collection<Integer> flightIDs, FlightStatusEnum newStatus, UserPrincipal principal)
			throws ModuleException {

		Audit audit = new Audit();
		if (principal != null) {
			audit.setUserId(principal.getName());
		} else {
			audit.setUserId("");
		}

		FlightStatusEnum oldStatus = (newStatus.getCode().equals(FlightStatusEnum.ACTIVE.getCode())) ? FlightStatusEnum.CLOSED
				: FlightStatusEnum.ACTIVE;

		audit.setTimestamp(new Date());
		LinkedHashMap<String, String> contents = new LinkedHashMap<String, String>();
		contents.put("flight id", flightIDs.toString());
		contents.put("newStatus", newStatus.getCode());
		contents.put("oldStatus", oldStatus.getCode());
		audit.setTaskCode(String.valueOf(TasksUtil.FLIGHT_EDIT_FLIGHTS));

		auditorBD.audit(audit, contents);

	}

	public void doAuditForFlightStatusChange(Integer scheduleID, FlightStatusEnum newStatus, UserPrincipal principal)
			throws ModuleException {

		Audit audit = new Audit();
		if (principal != null) {
			audit.setUserId(principal.getName());
		} else {
			audit.setUserId("");
		}

		FlightStatusEnum oldStatus = (newStatus.getCode().equals(FlightStatusEnum.ACTIVE.getCode())) ? FlightStatusEnum.CLOSED
				: FlightStatusEnum.ACTIVE;

		audit.setTimestamp(new Date());
		LinkedHashMap<String, String> contents = new LinkedHashMap<String, String>();
		contents.put("scheduleID", String.valueOf(scheduleID));
		contents.put("newStatus", newStatus.getCode());
		contents.put("oldStatus", oldStatus.getCode());
		audit.setTaskCode(String.valueOf(TasksUtil.FLIGHT_EDIT_FLIGHT_SCHEDULES));

		auditorBD.audit(audit, contents);

	}

	public void auditForFlownFlightsDetailsModification(UserPrincipal userPrincipal, Map<String, String[]> contentMap)
			throws ModuleException {

		Audit audit = new Audit();
		audit.setTimestamp(new Date());
		audit.setUserId(userPrincipal.getUserId());

		audit.setTaskCode(TasksUtil.MASTER_UPDATE_PAX_ET_STATUS);

		LinkedHashMap contents = new LinkedHashMap();

		if (contentMap != null && contentMap.size() > 0) {
			for (Entry<String, String[]> entry : contentMap.entrySet()) {
				contents.put(entry.getKey(), entry.getValue());
			}
		}
		contents.put("updatedBy", userPrincipal.getUserId());

		auditorBD.audit(audit, contents);

	}
}
