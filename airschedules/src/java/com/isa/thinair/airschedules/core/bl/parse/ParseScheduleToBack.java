/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.parse;

import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.LocalZuluTimeAdder;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command to parse schedules to back end
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="parseScheduleToBack"
 */
public class ParseScheduleToBack extends DefaultBaseCommand {

	// BDs
	private AirportBD airportBD;

	// helpers
	private LocalZuluTimeAdder timeAdder;

	/**
	 * constructor of the ParseScheduleToBack command
	 */
	public ParseScheduleToBack() {

		airportBD = AirSchedulesUtil.getAirportBD();

		timeAdder = new LocalZuluTimeAdder(airportBD);
	}

	/**
	 * execute method of the ParseScheduleToBack command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {

		// getting command params
		FlightSchedule schedule = (FlightSchedule) this.getParameter(CommandParamNames.FLIGHT_SCHEDULE);

		// check params
		this.checkParams(schedule);

		// constructing command responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);

		// if flight list has flights
		if (schedule.getStartDate() == null || schedule.getStopDate() == null) {

			schedule = timeAdder.addZuluTimeDetailsToSchedule(schedule);

		} else {

			schedule = timeAdder.recalculateZuluTimeDetailsToSchedule(schedule);
		}

		responce.addResponceParam(CommandParamNames.FLIGHT_SCHEDULE, schedule);
		// return command responce
		return responce;
	}

	/**
	 * privte method to validate parameters
	 * 
	 * @param schedule
	 * @throws ModuleException
	 */
	private void checkParams(FlightSchedule schedule) throws ModuleException {

		if (schedule == null)// throw exception
			throw new ModuleException("airschedules.arg.invalid.null");
	}
}
