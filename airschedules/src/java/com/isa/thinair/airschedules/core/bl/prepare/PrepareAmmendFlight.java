/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.prepare;

import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Set;

import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightLeg;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.LegUtil;
import com.isa.thinair.airschedules.api.utils.LocalZuluTimeAdder;
import com.isa.thinair.airschedules.core.persistence.dao.FlightDAO;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command for the prepare ammend flight
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="prepareAmmendFlight"
 */
public class PrepareAmmendFlight extends DefaultBaseCommand {

	// Dao's
	private FlightDAO flightDAO;

	// BDs
	private AirportBD airportBD;

	// helpers
	private LocalZuluTimeAdder timeAdder;

	/**
	 * constructor of the PrepareAmmendFlight command
	 */
	public PrepareAmmendFlight() {

		// looking up daos
		flightDAO = (FlightDAO) AirSchedulesUtil.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_DAO);

		// BDs
		airportBD = AirSchedulesUtil.getAirportBD();

		// helpers
		timeAdder = new LocalZuluTimeAdder(airportBD);
	}

	/**
	 * execute method of the PrepareAmmendFlight command
	 * 
	 * @throws ModuleException
	 */
	@Override
	public ServiceResponce execute() throws ModuleException {

		// getting command params
		Flight updated = (Flight) this.getParameter(CommandParamNames.FLIGHT);

		// checking params
		this.checkParams(updated);

		// get the flight from database
		Flight existing = flightDAO.getFlight(updated.getFlightId().intValue());
		// if no flight found for id throw exception
		if (existing == null)
			throw new ModuleException("airschedules.logic.bl.flight.noflight");

		Flight overlappign = null;
		Flight updatedOverlappign = null;

		if (updated.isOverlapping()) {

			if (existing.getOverlapingFlightId() != null)
				overlappign = flightDAO.getFlight(existing.getOverlapingFlightId().intValue());

			updatedOverlappign = flightDAO.getFlight(updated.getOverlapingFlightId().intValue());
			// if no flight found for id throw exception
			if (updatedOverlappign == null)
				throw new ModuleException("airschedules.logic.bl.flight.noflight");
		}

		FlightLeg firstLeg = LegUtil.getFirstFlightLeg(updated.getFlightLegs());
		boolean depDayoffsetChanged = (firstLeg.getEstDepartureDayOffset() != 0);

		if (firstLeg.getEstDepartureDayOffset() != 0) {

			GregorianCalendar calander = new GregorianCalendar();
			calander.setTime(updated.getDepartureDate());
			calander.add(GregorianCalendar.DATE, firstLeg.getEstDepartureDayOffset());

			int firstLegDepDayOffset = firstLeg.getEstDepartureDayOffset();

			updated.setDepartureDate(calander.getTime());
			firstLeg.setEstDepartureDayOffset(0);

			// adjust other leg times
			Set<FlightLeg> legs = updated.getFlightLegs();
			Iterator<FlightLeg> itLegs = legs.iterator();

			while (itLegs.hasNext()) {
				FlightLeg leg = itLegs.next();
				if (leg.getLegNumber() == 1) {
					// departure time offset already updated
					leg.setEstArrivalDayOffset(leg.getEstArrivalDayOffset() - firstLegDepDayOffset);
				} else {
					leg.setEstArrivalDayOffset(leg.getEstArrivalDayOffset() - firstLegDepDayOffset);
					leg.setEstDepartureDayOffset(leg.getEstDepartureDayOffset() - firstLegDepDayOffset);
				}
			}

			updated = timeAdder.addOnlySegementTimeDetails(updated);
		}

		// constructing and return command responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);

		// set the responce params
		responce.addResponceParam(CommandParamNames.FLIGHT, updated);
		responce.addResponceParam(CommandParamNames.EXISTING_FLIGHT, existing);
		if (overlappign != null)
			responce.addResponceParam(CommandParamNames.EXISTING_OVERLAPPING_FLIGHT, overlappign);
		if (updatedOverlappign != null)
			responce.addResponceParam(CommandParamNames.OVERLAPPING_FLIGHT, updatedOverlappign);

		// use this flag to decide leg time delay vs. dep date change
		responce.addResponceParam(CommandParamNames.FIRST_LEG_DEP_OFFSET_CHANGED_FLAG, new Boolean(depDayoffsetChanged));

		return responce;
	}

	/**
	 * privte method to validate parameters
	 * 
	 * @param schedule
	 * @throws ModuleException
	 */
	private void checkParams(Flight flight) throws ModuleException {

		if (flight == null)// throw exception
			throw new ModuleException("airschedules.arg.invalid.null");
	}
}
