/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.operation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.CreateFlightInventoryRQ;
import com.isa.thinair.airinventory.api.dto.SegmentDTO;
import com.isa.thinair.airinventory.api.dto.inventory.InvTempCCAllocDTO;
import com.isa.thinair.airinventory.api.dto.inventory.InvTempDTO;
import com.isa.thinair.airinventory.api.service.FlightInventoryBD;
import com.isa.thinair.airinventory.api.service.InventoryTemplateBD;
import com.isa.thinair.airinventory.api.service.LogicalCabinClassBD;
import com.isa.thinair.airmaster.api.model.AircraftCabinCapacity;
import com.isa.thinair.airmaster.api.model.AircraftModel;
import com.isa.thinair.airmaster.api.model.RouteInfo;
import com.isa.thinair.airmaster.api.service.AircraftBD;
import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.airschedules.api.dto.AffectedPeriod;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.FlightScheduleLeg;
import com.isa.thinair.airschedules.api.model.FlightScheduleSegment;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.AirportTerminalUtil;
import com.isa.thinair.airschedules.api.utils.AnciDefaultTemplStatusEnum;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.airschedules.api.utils.LocalZuluTimeAdder;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.airschedules.api.utils.ScheduleBuildStatusEnum;
import com.isa.thinair.airschedules.api.utils.SegmentUtil;
import com.isa.thinair.airschedules.core.persistence.dao.FlightDAO;
import com.isa.thinair.airschedules.core.persistence.dao.FlightScheduleDAO;
import com.isa.thinair.airschedules.core.util.CalculationUtil;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.airschedules.core.util.OverlapDetailUtil;
import com.isa.thinair.airschedules.core.util.ScheduledFlightUtil;
import com.isa.thinair.commons.api.dto.CabinClassWiseDefaultLogicalCCDetailDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for the build flight schedule
 * 
 * @author Lasantha Pambagoada
 * @isa.module.command name="buildSchedule"
 */
public class BuildSchedule extends DefaultBaseCommand {

	// Dao's
	private final FlightScheduleDAO flightSchedleDAO;
	private final FlightDAO flightDAO;

	// BD's
	private final FlightInventoryBD flightInventryBD;
	private final AircraftBD aircraftBD;
	private final CommonMasterBD commonMasterBD;
	private final AirportBD airportBD;
	private final LogicalCabinClassBD logicalCabinClassBD;
	private final InventoryTemplateBD inventoryTemplateBD;

	private final Log log = LogFactory.getLog(getClass());

	/**
	 * constructor of the BuildSchedule command
	 */
	public BuildSchedule() {

		// looking up daos
		flightDAO = (FlightDAO) AirSchedulesUtil.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_DAO);
		flightSchedleDAO = (FlightScheduleDAO) AirSchedulesUtil.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_DAO);

		// looking up bd's
		airportBD = AirSchedulesUtil.getAirportBD();
		commonMasterBD = AirSchedulesUtil.getCommonMasterBD();
		aircraftBD = AirSchedulesUtil.getAircraftBD();
		flightInventryBD = AirSchedulesUtil.getFlightInventoryBD();
		logicalCabinClassBD = AirSchedulesUtil.getLogicalCabinClassBD();
		inventoryTemplateBD = AirSchedulesUtil.getInventoryTemplateBD();

	}

	/**
	 * execute method of the BuildSchedule command
	 * 
	 * @throws ModuleException
	 */
	@Override
	@SuppressWarnings("unchecked")
	public ServiceResponce execute() throws ModuleException {

		// getting command params
		Collection<Integer> scheduleIds = (Collection<Integer>) this.getParameter(CommandParamNames.SCHEDULE_IDS);
		UserPrincipal principal = (UserPrincipal) this.getParameter(CommandParamNames.USER_PRINCIPAL);
		String userName=null;
		
		if (principal != null){
			userName = principal.getUserId();
		}
		
		
		boolean isScheduleMessageProcess = getParameter(CommandParamNames.IS_SHEDULE_MESSAGE_PROCESS, Boolean.FALSE,
				Boolean.class);
		Boolean buildDefaultInventory = (Boolean) this.getParameter(CommandParamNames.BUILD_DEFAULT_INVENTORY);

		// building each schedule
		Iterator<Integer> it = scheduleIds.iterator();
		HashMap<Integer, Collection<Integer>> buildScheduleFlightIds = new HashMap<Integer, Collection<Integer>>();
		
		//Collecting Flights to Publish 
		Collection<Flight> flightsToPublish = new ArrayList<Flight>();

		while (it.hasNext()) {

			// get the flight schedule to build
			int scheduleId = (it.next()).intValue();

			FlightSchedule schedule = flightSchedleDAO.getFlightSchedule(scheduleId);
			boolean isOverlapping = schedule.isOverlapping();

			if (log.isDebugEnabled()) {
				log.debug("scheduleId = " + scheduleId + " start date = " + schedule.getStartDate().toString() + " stop date = "
						+ schedule.getStopDate().toString());
			}

			int overlapingOffsetInMils = 0;

			if (isOverlapping) {

				FlightSchedule overlappingSchedule = flightSchedleDAO.getFlightSchedule(schedule.getOverlapingScheduleId()
						.intValue());
				overlapingOffsetInMils = (int) (overlappingSchedule.getStartDate().getTime() - schedule.getStartDate().getTime());
			}

			if (schedule.getBuildStatusCode().equals(ScheduleBuildStatusEnum.PENDING.getCode())) {

				// creating flights for the schedule
				AffectedPeriod period = new AffectedPeriod(schedule.getStartDate(), schedule.getStopDate(),
						schedule.getFrequency());
				Collection<Flight> flights = ScheduledFlightUtil.getFlightsForSchdule(schedule, period);

				// number of flights for schedule
				int numberOfFlights = flights.size();
				// aircraft model number
				String modelNumber = schedule.getModelNumber();

				Map<String, Map<String, List<InvTempCCAllocDTO>>> ccSegmentWiseInvTemplate = new HashMap<String, Map<String, List<InvTempCCAllocDTO>>>();
				AircraftModel model = aircraftBD.getAircraftModel(modelNumber);

				if (AppSysParamsUtil.isEnableDefaultInvCreationWithBuildFlights() && buildDefaultInventory) {
					Map<String, InvTempDTO> segmentWiseInvTemplate = new HashMap<String, InvTempDTO>();
					ArrayList<InvTempDTO> defaultInventoryTemplates = inventoryTemplateBD
							.getInventoryTemplatesForAirCraftModel(modelNumber);

						for (FlightScheduleSegment flightScheduleSegment : schedule.getFlightScheduleSegments()) {
							boolean isSegmentTemplate = false;
							for (InvTempDTO invTempDTO : defaultInventoryTemplates) {
							if (invTempDTO.getSegment() != null
									&& flightScheduleSegment.getSegmentCode().equals(invTempDTO.getSegment())) {
									isSegmentTemplate = true;
									segmentWiseInvTemplate.put(flightScheduleSegment.getSegmentCode(), invTempDTO);
									break;
								}
							}

							if (!isSegmentTemplate) {
								for (InvTempDTO invTempDTO : defaultInventoryTemplates) {
									if (invTempDTO.getSegment() == null) {
										segmentWiseInvTemplate.put(flightScheduleSegment.getSegmentCode(), invTempDTO);
										break;
									}
								}
							}
						}

					for (AircraftCabinCapacity ccCapacity : model.getAircraftCabinCapacitys()) {
						String cabinClassCode = ccCapacity.getCabinClassCode();
						for (String segmentCode : segmentWiseInvTemplate.keySet()) {
							InvTempDTO invTempDTO = segmentWiseInvTemplate.get(segmentCode);
							for (InvTempCCAllocDTO invTempCCAllocDTO : invTempDTO.getInvTempCCAllocDTOList()) {
								if (invTempCCAllocDTO.getCabinClassCode().equals(cabinClassCode)) {
									Map<String, List<InvTempCCAllocDTO>> tmpSegmentWiseInvTemplate;
									if (ccSegmentWiseInvTemplate.containsKey(cabinClassCode)) {
										tmpSegmentWiseInvTemplate = ccSegmentWiseInvTemplate.get(cabinClassCode);
									} else {
										tmpSegmentWiseInvTemplate = new HashMap<String, List<InvTempCCAllocDTO>>();
									}

									List<InvTempCCAllocDTO> invTempCCAllocDTOs = new ArrayList<InvTempCCAllocDTO>();
									if (tmpSegmentWiseInvTemplate.containsKey(segmentCode)) {
										invTempCCAllocDTOs = tmpSegmentWiseInvTemplate.get(segmentCode);
									} else {
										invTempCCAllocDTOs = new ArrayList<InvTempCCAllocDTO>();
									}
									invTempCCAllocDTOs.add(invTempCCAllocDTO);
									tmpSegmentWiseInvTemplate.put(segmentCode, invTempCCAllocDTOs);
									ccSegmentWiseInvTemplate.put(cabinClassCode, tmpSegmentWiseInvTemplate);
								}
							}
						}

					}

				}

				Map<String, String> defaultLogicalCCs = new HashMap<String, String>();
				for (AircraftCabinCapacity ccCapacity : model.getAircraftCabinCapacitys()) {
					for (CabinClassWiseDefaultLogicalCCDetailDTO defaultLogicalCCDetailDTO : CommonsServices.getGlobalConfig()
							.getDefaultLogicalCCDetails()) {
						if (ccCapacity.getCabinClassCode() != null
								&& ccCapacity.getCabinClassCode().equalsIgnoreCase(defaultLogicalCCDetailDTO.getCcCode())) {
							defaultLogicalCCs.put(ccCapacity.getCabinClassCode(),
									defaultLogicalCCDetailDTO.getDefaultLogicalCCCode());
							break;
						}
					}
				}

				// avilable seat kilometers
				int allocation = CalculationUtil.getAllocation(model.getAircraftCabinCapacitys());
				double flightDistance = this.getFlightDistance(schedule.getFlightScheduleLegs()).doubleValue();

				int avilableSeatKilosFlt = CalculationUtil.getASKforFlight(allocation, flightDistance);
				int availableSeatKilosShed = CalculationUtil.getASKforSchedule(numberOfFlights, allocation, flightDistance);

				Iterator<Flight> fit = flights.iterator();
				Collection<Integer> flightIdsOfSchedule = new ArrayList<Integer>();

				while (fit.hasNext()) {
					Flight flight = fit.next();
					if (!isSkipFlightUpdate(isScheduleMessageProcess, flight.getDepartureDate())) {
						// set the fields should set @ build
						
						flight.setAvailableSeatKilometers(new Integer(avilableSeatKilosFlt));
						flight.setCreatedBy(userName);
						flight.setStatus(FlightStatusEnum.CREATED.getCode());

						// prepare segment details of the flight
						LocalZuluTimeAdder timeAdder = new LocalZuluTimeAdder(airportBD);
						flight = timeAdder.addOnlySegementTimeDetails(flight);
						// setting terminal ids for the respective flights.
						AirportTerminalUtil terminalUtil = new AirportTerminalUtil(airportBD);
						flight = terminalUtil.addTerminalInformation(flight);

						// set overlap details
						Integer overlapId = null;
						Collection<FlightSegement> overlapSegments = null;
						Flight overlapFlight = null;

						if (isOverlapping) {

							int overlapSchedId = schedule.getOverlapingScheduleId().intValue();
							Date overlapDate = CalendarUtil.getOfssetInMilisecondAddedDate(flight.getDepartureDate(),
									overlapingOffsetInMils);
							overlapFlight = flightDAO.getOverlappingFlight(overlapSchedId, overlapDate);

							if (overlapFlight != null) {

								overlapId = overlapFlight.getFlightId();
								overlapSegments = overlapFlight.getFlightSegements();

								flight = OverlapDetailUtil.addOverlapDetailsToFlight(flight, overlapFlight);
							}
						}

						flight.setMealTemplAssignStatus(AnciDefaultTemplStatusEnum.SCHEDULED.getCode());
						flight.setSeatTemplAssignStatus(AnciDefaultTemplStatusEnum.SCHEDULED.getCode());
						// save the flight
						flight = timeAdder.addLocalTimeDetailsToFlight(flight);
						flightDAO.saveFlight(flight);

						// save the overlapping flight
						if (isOverlapping && overlapFlight != null) {

							overlapFlight = OverlapDetailUtil.addOverlapDetailsToFlight(overlapFlight, flight);
							overlapFlight = timeAdder.addLocalTimeDetailsToFlight(overlapFlight);
							flightDAO.saveFlight(overlapFlight);
						}

						// collect the flight ids
						flightIdsOfSchedule.add(flight.getFlightId());

						// Add the flights to the list which holds the publish flight list
						flightsToPublish.add(flight);

						// create inventry for flight and segments
						int flightID = flight.getFlightId().intValue();
						Collection<SegmentDTO> intersepSegList = SegmentUtil.createSegmentDTOList(flightID,
								flight.getFlightSegements(), overlapId, overlapSegments);

						CreateFlightInventoryRQ createFlightInventoryRQ = new CreateFlightInventoryRQ();
						createFlightInventoryRQ.setFlightId(flightID);
						createFlightInventoryRQ.setAircraftModel(model);
						createFlightInventoryRQ.setSegmentDTOs(intersepSegList);
						createFlightInventoryRQ.setOverlappingFlightId(overlapId);
						createFlightInventoryRQ.setDefaultLogicalCCs(defaultLogicalCCs);
						createFlightInventoryRQ.setCcSegmentWiseInvTemplate(ccSegmentWiseInvTemplate);
						flightInventryBD.createFlightInventory(createFlightInventoryRQ);
					}
				}

				// set the new state of the schedule and save it
				schedule.setBuildStatusCode(ScheduleBuildStatusEnum.BUILT.getCode());
				schedule.setNumberOfDepartures(new Integer(numberOfFlights));
				schedule.setAvailableSeatKilometers(new Integer(availableSeatKilosShed));
				schedule.setModifiedDate(new Date(System.currentTimeMillis()));
				schedule.setModifiedBy(userName);
				flightSchedleDAO.saveFlightSchedule(schedule);

				buildScheduleFlightIds.put(schedule.getScheduleId(), flightIdsOfSchedule);

				if (log.isDebugEnabled()) {
					log.debug("finished schedule : " + scheduleId);
				}

			}
		}

		// return command responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);
		if (responce.isSuccess()) {
			responce.setResponseCode(ResponceCodes.BUILD_SCHEDULE_SUCCESSFULL);
			responce.addResponceParam(CommandParamNames.LAST_RESPONSE, responce);
			responce.addResponceParam(CommandParamNames.BUILDED_SCHEDULE_FLIGHT_IDS, buildScheduleFlightIds);
			responce.addResponceParam(CommandParamNames.FLIGHTS_TO_PUBLISH, flightsToPublish);
		}
		return responce;
	}

	/**
	 * private method to get the flight distance for the flight
	 * 
	 * @param legList
	 * @return flight distance
	 * @throws ModuleException
	 */
	private BigDecimal getFlightDistance(Collection<FlightScheduleLeg> legList) throws ModuleException {
		BigDecimal flightDistance = AccelAeroCalculator.getDefaultBigDecimalZero();

		Iterator<FlightScheduleLeg> itLegs = legList.iterator();
		while (itLegs.hasNext()) {
			FlightScheduleLeg leg = itLegs.next();
			RouteInfo routeInfo = commonMasterBD.getRouteInfo(leg.getModelRouteId());
			flightDistance = AccelAeroCalculator.add(flightDistance, routeInfo.getDistance());
		}

		return flightDistance;
	}

	private boolean isSkipFlightUpdate(boolean isScheduleMessageProcess, Date departureDate) {
		boolean isSkip = false;
		if (isScheduleMessageProcess && departureDate.before(CalendarUtil.getCurrentZuluDateTime())) {
			isSkip = true;
		}
		return isSkip;
	}
}
