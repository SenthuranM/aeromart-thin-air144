/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.parse;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import com.isa.thinair.airinventory.api.dto.FlightInventorySummaryDTO;
import com.isa.thinair.airinventory.api.service.FlightInventoryBD;
import com.isa.thinair.airmaster.api.model.AircraftModel;
import com.isa.thinair.airmaster.api.service.AircraftBD;
import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airschedules.api.criteria.FlightFlightSegmentSearchCriteria;
import com.isa.thinair.airschedules.api.dto.DisplayFlightDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.LocalZuluTimeAdder;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.ScheduledFlightUtil;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command to parse flights to front end
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="parseFlightToFront"
 */
public class ParseFlightsToFront extends DefaultBaseCommand {

	// BDs
	private FlightInventoryBD flightInventryBD;
	private AirportBD airportBD;
	private AircraftBD aircraftBD;
	private FlightBD flightBD;

	/**
	 * constructor of the ParseFlightsToFront command
	 */
	public ParseFlightsToFront() {

		airportBD = AirSchedulesUtil.getAirportBD();
		aircraftBD = AirSchedulesUtil.getAircraftBD();
		flightInventryBD = AirSchedulesUtil.getFlightInventoryBD();
		flightBD = AirSchedulesUtil.getFlightBD();
	}

	/**
	 * execute method of the ParseFlightsToFront command
	 * 
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	public ServiceResponce execute() throws ModuleException {

		// getting command params
		Page page = (Page) this.getParameter(CommandParamNames.PAGE);
		Boolean display = (Boolean) this.getParameter(CommandParamNames.DISPLAY);
		FlightFlightSegmentSearchCriteria flightCriteria = (FlightFlightSegmentSearchCriteria) this
				.getParameter(CommandParamNames.FLIGHT_FLIGHTSEGMENT_CRITERIA_LIST);
		Boolean isDataSourceReporting = (Boolean) this.getParameter(CommandParamNames.IS_REPORTING);

		// prepare the input variables
		boolean isToDisplay = (display == null || display.booleanValue() == false) ? false : true;
		Collection<Flight> flightList = (page == null || page.getPageData() == null) ? new ArrayList<Flight>() : page
				.getPageData();

		// constructing command responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);

		// if flight list has flights
		if (flightList.size() > 0) {

			Collection<DisplayFlightDTO> parsedFlightList = new ArrayList<DisplayFlightDTO>();

			// if flights to display
			if (isToDisplay) {

				Collection<Integer> flightIds = ScheduledFlightUtil.getFlightIds(flightList);
				HashMap<Integer, FlightInventorySummaryDTO> inventrySummaryList = flightInventryBD
						.getFlightInventoriesSummary(flightIds, isDataSourceReporting);
				Collection<String> modelNumbers = ScheduledFlightUtil.getModelNumbers(flightList);
				Collection<AircraftModel> aircraftModels = aircraftBD.getAircraftModels(modelNumbers);
				HashMap<String, Integer> modelBaseCapcity = ScheduledFlightUtil.getAircraftBaseCapacity(aircraftModels);
				int baseCapacity = 0;

				LocalZuluTimeAdder timeAdder = new LocalZuluTimeAdder(airportBD);

				Iterator<Flight> itFlights = flightList.iterator();
				while (itFlights.hasNext()) {

					Flight flight = (Flight) itFlights.next();
					flight = timeAdder.addLocalTimeDetailsToFlight(flight);

					FlightInventorySummaryDTO fltInvSummaryDTO = (FlightInventorySummaryDTO) inventrySummaryList.get(flight
							.getFlightId());
					AircraftModel model = aircraftBD.getAircraftModel(flight.getModelNumber());
					if (model != null) {
						flight.setModelDesc(model.getDescription());
					}
					DisplayFlightDTO flightDTO = new DisplayFlightDTO(flight);
					baseCapacity = ((Integer) modelBaseCapcity.get(flight.getModelNumber())).intValue();

					if (fltInvSummaryDTO != null) {

						flightDTO.setSeatsSold(fltInvSummaryDTO.getSoldAdultSeats());
						flightDTO.setOnHold(fltInvSummaryDTO.getOnholdAdultSeats());
						if (flightCriteria != null) {
							flightDTO.setStndBy(flightBD.getFlightStandbyCount(flight.getFlightId(),
									flightCriteria.getSegmentCode()));
							flightDTO.setCheckedIn(flightBD.getFlightCheckinCount(flight.getFlightId(),
									flightCriteria.getSegmentCode()));
						}
						// [REQ] allocation should be calculated for a single segment
						// [temporary fix] total allocation for all CC and all seg / valid segments
						// [FIX] inventory should provide a function with correct seat allocation count
						// (need to include overallocate seats and curtailed seats)

						/*
						 * int segmentCount = 0; Collection segs = flight.getFlightSegements(); Iterator itSegs =
						 * segs.iterator();
						 * 
						 * while (itSegs.hasNext()) { FlightSegement seg = (FlightSegement)itSegs.next(); if
						 * (seg.getValidFlag()) segmentCount++; } if (segmentCount <= 0) throw new
						 * ModuleException("airschedules.arg.invalid");
						 * flightDTO.setAllocated((int)(fltInvSummaryDTO.getAllocatedAdultSeats() / segmentCount));
						 */
						flightDTO.setAllocated(baseCapacity);
					}

					parsedFlightList.add(flightDTO);
				}

				Page pasrsedPage = new Page(page.getTotalNoOfRecords(), page.getStartPosition(), page.getEndPosition(),
						parsedFlightList);
				responce.addResponceParam(CommandParamNames.PAGE, pasrsedPage);

				// if flights not to display
			} else {

			}
		} else {
			responce.addResponceParam(CommandParamNames.PAGE, page);
		}

		// return command responce
		return responce;
	}
}
