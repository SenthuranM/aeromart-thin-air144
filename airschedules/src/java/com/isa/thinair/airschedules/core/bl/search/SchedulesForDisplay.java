/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.search;

import java.util.Date;
import java.util.List;

import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airschedules.api.dto.ScheduleSearchDTO;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.LocalZuluTimeAdder;
import com.isa.thinair.airschedules.core.persistence.dao.FlightScheduleDAO;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.CriteriaQueryUtil;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command to search the schedules
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="schedulesForDisplay"
 */
public class SchedulesForDisplay extends DefaultBaseCommand {

	// Dao's
	private FlightScheduleDAO flightSchedleDAO;

	// BDs
	private AirportBD airportBD;

	// helpers
	private LocalZuluTimeAdder timeAdder;

	/**
	 * constructor of the SchedulesForDisplay command
	 */
	public SchedulesForDisplay() {

		// looking up daos
		flightSchedleDAO = (FlightScheduleDAO) AirSchedulesUtil.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_DAO);

		// BDs
		airportBD = AirSchedulesUtil.getAirportBD();

		// helpers
		timeAdder = new LocalZuluTimeAdder(airportBD);
	}

	/**
	 * execute method of the SchedulesForDisplay command
	 * 
	 * @throws ModuleException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ServiceResponce execute() throws ModuleException {

		// getting command params
		List criteria = (List) this.getParameter(CommandParamNames.CRITERIA_LIST);
		Integer startIndex = (Integer) this.getParameter(CommandParamNames.START_INDEX);
		Integer pageSize = (Integer) this.getParameter(CommandParamNames.PAGE_SIZE);
		Boolean datesInLocal = (Boolean) this.getParameter(CommandParamNames.IS_DATE_IN_LOCAL);
		Boolean isAdhocFlightDataEnabled = (Boolean) this.getParameter(CommandParamNames.IS_ADHOC_FLIGHT_DATA_ENABLED);

		// checking params
		checkParams(criteria, startIndex, pageSize, datesInLocal);

		ScheduleSearchDTO dto = CriteriaQueryUtil.getScheduleSearchDTO(criteria);
		String orign = dto.getFromAirport();

		if (datesInLocal.booleanValue() && orign != null) {

			Date startDate = timeAdder.getZuluDateTime(orign, CalendarUtil.getStartTimeOfDate(dto.getStartDate()));
			Date endDate = timeAdder
					.getZuluDateTimeAsEffective(orign, startDate, CalendarUtil.getEndTimeOfDate(dto.getEndDate()));

			dto.setStartDate(startDate);
			dto.setEndDate(endDate);

		} else {

			Date startDate = CalendarUtil.getStartTimeOfDate(dto.getStartDate());
			Date endDate = CalendarUtil.getEndTimeOfDate(dto.getEndDate());

			dto.setStartDate(startDate);
			dto.setEndDate(endDate);
		}

		Page page = flightSchedleDAO.searchFlightSchedulesForDisplay(dto, startIndex.intValue(), pageSize.intValue(),isAdhocFlightDataEnabled);

		// constructing and return command responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);
		responce.addResponceParam(CommandParamNames.PAGE, page);		
		if(isAdhocFlightDataEnabled){
			Page adhocFlightPage = flightSchedleDAO.searchAdhocFlightsForDisplay(dto, startIndex.intValue(), pageSize.intValue());
			responce.addResponceParam(CommandParamNames.ADHOC_FLIGHT_PAGE, adhocFlightPage);
		}
		return responce;
	}

	/**
	 * privte method to validate parameters
	 * 
	 * @param schedule
	 * @throws ModuleException
	 */
	@SuppressWarnings("rawtypes")
	private void checkParams(List criteria, Integer startIndex, Integer pageSize, Boolean datesInLocal) throws ModuleException {

		if (criteria == null || startIndex == null || pageSize == null || datesInLocal == null)// throw exception
			throw new ModuleException("airschedules.arg.invalid.null");
	}
}
