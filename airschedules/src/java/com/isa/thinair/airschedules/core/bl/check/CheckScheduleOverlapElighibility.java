/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.check;

import java.util.Date;
import java.util.Iterator;
import java.util.Set;

import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.FlightScheduleLeg;
import com.isa.thinair.airschedules.api.model.FlightScheduleSegment;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.LegUtil;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.airschedules.api.utils.ScheduleBuildStatusEnum;
import com.isa.thinair.airschedules.core.persistence.dao.FlightScheduleDAO;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.FrequencyUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command to check overlapping situations
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="checkScheduleOverlap"
 */
public class CheckScheduleOverlapElighibility extends DefaultBaseCommand {

	// Dao's
	private FlightScheduleDAO flightSchedleDAO;

	/**
	 * constructor of the CheckScheduleOverpalElighibility command
	 */
	public CheckScheduleOverlapElighibility() {

		// looking up daos
		flightSchedleDAO = (FlightScheduleDAO) AirSchedulesUtil.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_DAO);
	}

	/**
	 * execute method of the CreateSchedule command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {

		// getting command params
		FlightSchedule schedule = (FlightSchedule) this.getParameter(CommandParamNames.FLIGHT_SCHEDULE);

		// checking params
		this.checkParams(schedule);

		// constructing command responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);

		boolean overlapping = schedule.isOverlapping();

		if (overlapping) {

			FlightSchedule overlappingSchedule = flightSchedleDAO
					.getFlightSchedule(schedule.getOverlapingScheduleId().intValue());
			// overlap id is wrong
			if (overlappingSchedule == null) {

				responce.setSuccess(false);
				responce.setResponseCode(ResponceCodes.INVALID_OVERLAPPING_SCHEDULE);
				return responce;
			}

			// one has to be in status = 'PND'
			if (schedule.getBuildStatusCode() != null && schedule.getBuildStatusCode().equals(ScheduleBuildStatusEnum.BUILT)
					&& overlappingSchedule.getBuildStatusCode().equals(ScheduleBuildStatusEnum.BUILT)) {

				responce.setSuccess(false);
				responce.setResponseCode(ResponceCodes.INVALID_OVERLAPPING_SCHEDULE_STATUS);
				return responce;
			}

			// if schedule is new one
			if (schedule.getScheduleId() == null) {

				this.checkOverlappsForNewOverlaps(schedule, overlappingSchedule, responce);

				// if schedule is already existing one
			} else {

				FlightSchedule existing = flightSchedleDAO.getFlightSchedule(schedule.getScheduleId().intValue());

				// if existing is not overlapping
				if (!existing.isOverlapping()) {

					this.checkOverlappsForNewOverlaps(schedule, overlappingSchedule, responce);

					// if existing is also overlapping
				} else {

					// if existing is overlapped with some other scehduel
					if (existing.getOverlapingScheduleId().intValue() != schedule.getOverlapingScheduleId().intValue()) {

						responce.setSuccess(false);
						responce.setResponseCode(ResponceCodes.OVERLAPPING_ALREADY_OVERLAPPING_SCHEDULE);

						// if overlapping shcedule has overlapped with some other schedule
					} else if (overlappingSchedule.getOverlapingScheduleId() != null
							&& overlappingSchedule.getOverlapingScheduleId().intValue() != schedule.getScheduleId().intValue()) {

						responce.setSuccess(false);
						responce.setResponseCode(ResponceCodes.OVERLAPPING_ALREADY_OVERLAPPING_SCHEDULE);

					} else {
						// basic checking for overlap is ok, have to check for segments
						boolean isSegsOk = false;

						Set<FlightScheduleSegment> segments = schedule.getFlightScheduleSegments();
						Iterator<FlightScheduleSegment> itSegs = segments.iterator();

						while (itSegs.hasNext()) {

							FlightScheduleSegment segment = (FlightScheduleSegment) itSegs.next();

							Set<FlightScheduleSegment> overlapSegs = overlappingSchedule.getFlightScheduleSegments();
							Iterator<FlightScheduleSegment> itOverlapSegs = overlapSegs.iterator();

							while (itOverlapSegs.hasNext()) {

								FlightScheduleSegment overlapSeg = (FlightScheduleSegment) itOverlapSegs.next();
								if (overlapSeg.getSegmentCode().equals(segment.getSegmentCode())) {
									isSegsOk = true;
									break;
								}
							}
						}

						/*
						 * [NOTE] If the timings of the overlapping segment is not allowed to change uncomment the
						 * following block When commented, it allows chaning the overlap segment timing actual time
						 * changes in the overlapping segment is handled in the AmmendSchedule cmd
						 */
						boolean isLegsOk = true;

						/*
						 * Set legs = schedule.getFlightScheduleLegs(); Iterator itLegs = legs.iterator();
						 * 
						 * while(itLegs.hasNext()){
						 * 
						 * FlightScheduleLeg leg = (FlightScheduleLeg)itLegs.next();
						 * 
						 * Set overlapLegs = overlappingSchedule.getFlightScheduleLegs(); Iterator itOverlapLegs =
						 * overlapLegs.iterator();
						 * 
						 * while(itOverlapLegs.hasNext()){
						 * 
						 * FlightScheduleLeg overlapLeg = (FlightScheduleLeg)itOverlapLegs.next();
						 * if(overlapLeg.getOrigin().equals(leg.getOrigin()) &&
						 * overlapLeg.getDestination().equals(leg.getDestination()) ){
						 * 
						 * if(!(overlapLeg.getEstDepartureTimeZulu().getTime() ==
						 * leg.getEstDepartureTimeZulu().getTime() && overlapLeg.getEstArrivalTimeZulu().getTime() ==
						 * leg.getEstArrivalTimeZulu().getTime()) ){
						 * 
						 * isLegsOk = false; break; } } } }
						 * 
						 * //check on dayoffset change to overlapping segment should be done //compare original
						 * schedule.leg with updated schedule leg for same dayoffset
						 */

						if (!isSegsOk || !isLegsOk) {

							responce.setSuccess(false);
							responce.setResponseCode(ResponceCodes.INVALID_OVERLAPPING_SEGMENT_LEG);
						}
					}
				}
			}
		}

		// return command responce
		return responce;
	}

	/**
	 * privte method to validate parameters
	 * 
	 * @param schedule
	 * @throws ModuleException
	 */
	private void checkParams(FlightSchedule schedule) throws ModuleException {

		if (schedule == null)// throw exception
			throw new ModuleException("airschedules.arg.invalid.null");

		Set<FlightScheduleLeg> legs = schedule.getFlightScheduleLegs();
		Set<FlightScheduleSegment> segs = schedule.getFlightScheduleSegments();

		if (legs == null || legs.size() == 0 || segs == null || segs.size() == 0) // throw exception
			throw new ModuleException("airschedules.arg.invalid");
	}

	/**
	 * check method for overlaps
	 * 
	 * @param schedule
	 * @param responce
	 * @return
	 */
	private DefaultServiceResponse checkOverlappsForNewOverlaps(FlightSchedule schedule, FlightSchedule overlappingSchedule,
			DefaultServiceResponse responce) {

		FlightSchedule first = null;
		FlightSchedule second = null;
		boolean isOk = false;

		if (schedule.getStartDate().getTime() < overlappingSchedule.getStartDate().getTime()) {

			first = schedule;
			second = overlappingSchedule;

		} else {

			first = overlappingSchedule;
			second = schedule;
		}

		FlightScheduleLeg firstLegOfSecondSchedule = LegUtil.getFirstScheduleLeg(second.getFlightScheduleLegs());
		// match models
		if (first.getModelNumber().equals(second.getModelNumber())) {

			Iterator<FlightScheduleLeg> itFirstLegs = first.getFlightScheduleLegs().iterator();
			while (itFirstLegs.hasNext()) {
				// check for origin, destination, dep & arr time for possible leg matches
				FlightScheduleLeg nextLeg = (FlightScheduleLeg) itFirstLegs.next();
				if (nextLeg.getOrigin().equals(firstLegOfSecondSchedule.getOrigin())
						&& nextLeg.getDestination().equals(firstLegOfSecondSchedule.getDestination())
						&& nextLeg.getEstDepartureTimeZulu().getTime() == firstLegOfSecondSchedule.getEstDepartureTimeZulu()
								.getTime()
						&& nextLeg.getEstArrivalTimeZulu().getTime() == firstLegOfSecondSchedule.getEstArrivalTimeZulu()
								.getTime()) {
					// match schedule start and end dates
					Date secondStartDate = CalendarUtil.getOfssetAndTimeAddedDate(first.getStartDate(),
							nextLeg.getEstDepartureTimeZulu(), nextLeg.getEstDepartureDayOffset());
					Date secondEndDate = CalendarUtil.getOfssetAndTimeAddedDate(first.getStopDate(),
							nextLeg.getEstDepartureTimeZulu(), nextLeg.getEstDepartureDayOffset());

					if (secondStartDate.getTime() == second.getStartDate().getTime()
							&& secondEndDate.getTime() == second.getStopDate().getTime()) {
						// match schedule frequencies
						Frequency fqn = FrequencyUtil.getCopyOfFrequency(first.getFrequency());
						fqn = FrequencyUtil.shiftFrequencyBy(fqn, nextLeg.getEstDepartureDayOffset());
						if (FrequencyUtil.isEqualFrequency(fqn, second.getFrequency())) {
							isOk = true;
						}
					}
					break;
				}
			}
		}

		if (!isOk) {

			responce.setSuccess(false);
			responce.setResponseCode(ResponceCodes.INVALID_OVERLAPPING_SCHEDULE);
		}

		return responce;
	}

}
