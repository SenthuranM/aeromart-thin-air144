/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.gds;

import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.GDSSchedConstants;
import com.isa.thinair.airschedules.core.config.AirScheduleModuleConfig;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.gdsservices.api.dto.publishing.Envelope;
import com.isa.thinair.gdsservices.api.service.GDSPublishingBD;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for integrate create schedule with gds
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="integrateCreateFlight"
 */
public class IntegrateCreateFlight extends DefaultBaseCommand {

	// BDs
	private GDSPublishingBD gdsPublishingBD;

	/**
	 * constructor of the IntegrateCreateFlight command
	 */
	public IntegrateCreateFlight() {

		// looking up BDs
		gdsPublishingBD = AirSchedulesUtil.getGDSPublishingBD();
	}

	/**
	 * execute method of the ValidateCreateFlights command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {

		DefaultServiceResponse lastResponse = (DefaultServiceResponse) this.getParameter(CommandParamNames.LAST_RESPONSE);
		if ("Y".equalsIgnoreCase(CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.GDS_INTEGRATED))) {

			Flight flight = (Flight) this.getParameter(CommandParamNames.FLIGHT);

			String serviceType = ((AirScheduleModuleConfig) AirSchedulesUtil.getInstance().getModuleConfig())
					.getServiceTypeForAdHokFlight();

			Envelope env = new Envelope(GDSSchedConstants.TargetLogicNames.PUBLISH_ASM_NEW);
			env.addParam(GDSSchedConstants.ParamNames.FLIGHT, flight);
			env.addParam(GDSSchedConstants.ParamNames.SERVICE_TYPE, serviceType);

			gdsPublishingBD.publishMessage(env);
		}
		// return last command responce
		return lastResponse;
	}
}
