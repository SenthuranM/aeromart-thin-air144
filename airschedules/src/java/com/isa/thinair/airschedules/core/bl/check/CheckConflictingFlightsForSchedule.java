/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.check;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airschedules.api.dto.AffectedPeriod;
import com.isa.thinair.airschedules.api.dto.OverlappedFlightDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.LocalZuluTimeAdder;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.airschedules.core.persistence.dao.FlightScheduleDAO;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for validate conflicting flights for given schedule
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="checkConflictingFlightsForSchedule"
 */
public class CheckConflictingFlightsForSchedule extends DefaultBaseCommand {

	// Dao's
	private FlightScheduleDAO flightSchedleDAO;
	
	private AirportBD airportBD;

	/**
	 * Constructor of the ValidateConflictingFlights command
	 */
	public CheckConflictingFlightsForSchedule() {

		// looking up daos
		flightSchedleDAO = (FlightScheduleDAO) AirSchedulesUtil.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_DAO);
		airportBD = AirSchedulesUtil.getAirportBD();
	}

	/**
	 * execute method of the validate conflicting flights command
	 */
	public ServiceResponce execute() throws ModuleException {

		// getting command params
		FlightSchedule schedule = (FlightSchedule) this.getParameter(CommandParamNames.FLIGHT_SCHEDULE);
		Collection<AffectedPeriod> periodList = (Collection) this.getParameter(CommandParamNames.CREATE_PERIOD_LIST);

		// checking params
		this.checkParams(schedule);

		// create the responce to return
		DefaultServiceResponse responce = new DefaultServiceResponse(true);

		// if affected create periods or empty null construct it
		if (periodList == null || periodList.size() == 0) {
			AffectedPeriod period = new AffectedPeriod(schedule.getStartDate(), schedule.getStopDate(), schedule.getFrequency());
			periodList = new ArrayList<AffectedPeriod>();
			periodList.add(period);
		}
		
		LocalZuluTimeAdder timeAdder = new LocalZuluTimeAdder(airportBD);
		schedule = timeAdder.addLocalTimeDetailsToFlightSchedule(schedule, false);
		// check all affected periods
		Iterator<AffectedPeriod> periodListIt = periodList.iterator();
		while (periodListIt.hasNext()) {

			AffectedPeriod period = (AffectedPeriod) periodListIt.next();

			// gets the conflicting schedules
			List<Flight> conflictList = flightSchedleDAO.getConflictingFlights(schedule, period);
			// if conflicts exists
			if (conflictList != null && conflictList.size() > 0) {

				// collection to keep conflict schedule details
				Collection<OverlappedFlightDTO> collection = new ArrayList<OverlappedFlightDTO>();

				Flight flt;
				OverlappedFlightDTO overlapped;

				Iterator<Flight> it = conflictList.iterator();

				while (it.hasNext()) {

					flt = (Flight) it.next();
					
					flt = timeAdder.addLocalTimeDetailsToFlight(flt);
					
					if(conflictingLocalDepartureDates(schedule, flt)){
						// construct the overlapping DTO
						overlapped = new OverlappedFlightDTO();
						overlapped.setFlightNumber(flt.getFlightNumber());
						overlapped.setDepartureDate(flt.getDepartureDate());
						overlapped.setOperationType(flt.getOperationTypeId() + "");

						// add DTO to the list
						collection.add(overlapped);
					}
					
				}
				
				if(collection.size() > 0){
					responce.setSuccess(false);
					responce.setResponseCode(ResponceCodes.SCHEDULE_FLIGHT_CONFLICT);
					responce.addResponceParam(ResponceCodes.SCHEDULE_FLIGHT_CONFLICT, collection);
					break;
				}
				
			}
		}
		return responce;
	}

	/**
	 * privte method to validate parameters
	 * 
	 * @param schedule
	 * @throws ModuleException
	 */
	private void checkParams(FlightSchedule schedule) throws ModuleException {

		if (schedule == null || schedule.getStartDate() == null || schedule.getStopDate() == null
				|| schedule.getFrequency() == null)// throw exception
			throw new ModuleException("airschedules.arg.invalid.null");
	}
	
	private boolean conflictingLocalDepartureDates(FlightSchedule  schedule, Flight flt) throws ModuleException {
		boolean   blnCoplicying = false;
		Frequency checkingFrequency = schedule.getFrequencyLocal();
		int dayNumber = flt.getDayNumber();
		
		Calendar  cal1 = new GregorianCalendar();
		cal1.setTime(flt.getDepartureDate());
		
		Calendar  cal2 = new GregorianCalendar();
		cal2.setTime(flt.getDepartureDateLocal());
		
		int dayDiff = cal2.get(Calendar.DAY_OF_MONTH) - cal1.get(Calendar.DAY_OF_MONTH);
		
		if(dayDiff > 2){
			dayDiff = 1;
		}else if(dayDiff < -2){
			dayDiff = -1;
		}
		
		dayNumber = dayNumber + dayDiff;
		if(dayNumber ==7){
			dayNumber= 0;
		}else if(dayNumber ==-1){
			dayNumber= 6;
		}
		
		if((checkingFrequency.getDay0() && (dayNumber ==0))
				||(checkingFrequency.getDay1() && (dayNumber ==1))
				||(checkingFrequency.getDay2() && (dayNumber ==2))
				||(checkingFrequency.getDay3() && (dayNumber ==3))
				||(checkingFrequency.getDay4() && (dayNumber ==4))
				||(checkingFrequency.getDay5() && (dayNumber ==5))
				||(checkingFrequency.getDay6() && (dayNumber ==6))){
			
			blnCoplicying =  true;
						
		}	
		
		return blnCoplicying;
	}
}
