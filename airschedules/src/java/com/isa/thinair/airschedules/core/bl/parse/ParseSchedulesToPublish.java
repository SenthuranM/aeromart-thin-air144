package com.isa.thinair.airschedules.core.bl.parse;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airschedules.api.dto.FlightScheduleInfoDTO;
import com.isa.thinair.airschedules.api.dto.FlightScheduleLegInfoDTO;
import com.isa.thinair.airschedules.api.dto.FlightScheduleSegmentInfoDTO;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.FlightScheduleLeg;
import com.isa.thinair.airschedules.api.model.FlightScheduleSegment;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.LocalZuluTimeAdder;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command to parse flights to publish
 * 
 * @author Manoj Dhanushka
 * @isa.module.command name="parseSchedulesToPublish"
 */
public class ParseSchedulesToPublish extends DefaultBaseCommand {

	// BDs
	private AirportBD airportBD;

	/**
	 * constructor of the ParseScheduleToPublish command
	 */
	public ParseSchedulesToPublish() {

		airportBD = AirSchedulesUtil.getAirportBD();
	}

	/**
	 * execute method of the ParseScheduleToPublish command
	 * 
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	public ServiceResponce execute() throws ModuleException {

		Collection<FlightSchedule> schedList = (Collection<FlightSchedule>) this.getParameter(CommandParamNames.FLIGHT_SCHEDULE_LIST);

		// constructing command responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);
		Collection<FlightScheduleInfoDTO> colFlightScheduleInfoDTOs = null;

		// if flight list has flights
		if (schedList != null && schedList.size() > 0) {
			colFlightScheduleInfoDTOs = new ArrayList<FlightScheduleInfoDTO>();
			LocalZuluTimeAdder timeAdder = new LocalZuluTimeAdder(airportBD);

			Iterator<FlightSchedule> itScheds = schedList.iterator();
			while (itScheds.hasNext()) {
				FlightScheduleInfoDTO flightScheduleInfoDTO = new FlightScheduleInfoDTO();

				FlightSchedule flightSchedule = (FlightSchedule) itScheds.next();
				flightSchedule = timeAdder.addLocalTimeDetailsToFlightSchedule(flightSchedule, false);

				flightScheduleInfoDTO.setArrivalStnCode(flightSchedule.getArrivalStnCode());
				flightScheduleInfoDTO.setDepartureStnCode(flightSchedule.getDepartureStnCode());
				flightScheduleInfoDTO.setFlightNumber(flightSchedule.getFlightNumber());
				flightScheduleInfoDTO.setFlightScheduleLegs(createFlightScheduleLegs(flightSchedule.getFlightScheduleLegs()));
				flightScheduleInfoDTO.setFlightScheduleSegments(createFlightScheduleSegments(flightSchedule
						.getFlightScheduleSegments()));
				flightScheduleInfoDTO.setFrequency(flightSchedule.getFrequency());
				flightScheduleInfoDTO.setFrequencyLocal(flightSchedule.getFrequency());
				flightScheduleInfoDTO.setModelNumber(flightSchedule.getModelNumber());
				flightScheduleInfoDTO.setStartDate(flightSchedule.getStartDate());
				flightScheduleInfoDTO.setStartDateLocal(flightSchedule.getStartDateLocal());
				flightScheduleInfoDTO.setStopDate(flightSchedule.getStopDate());
				flightScheduleInfoDTO.setStopDateLocal(flightSchedule.getStopDateLocal());
				flightScheduleInfoDTO.setScheduleId(flightSchedule.getScheduleId());

				colFlightScheduleInfoDTOs.add(flightScheduleInfoDTO);
			}

			responce.addResponceParam(CommandParamNames.FLIGHT_SCHEDULE_INFO_LIST, colFlightScheduleInfoDTOs);

		} else {
			responce.addResponceParam(CommandParamNames.FLIGHT_SCHEDULE_INFO_LIST, colFlightScheduleInfoDTOs);
		}

		// return command responce
		return responce;
	}

	private Set<FlightScheduleLegInfoDTO> createFlightScheduleLegs(Set<FlightScheduleLeg> setFlightScheduleLegs) {

		Set<FlightScheduleLegInfoDTO> setFlightScheduleLegInfoDTOs = new HashSet<FlightScheduleLegInfoDTO>();

		if (setFlightScheduleLegs != null && !setFlightScheduleLegs.isEmpty()) {

			for (FlightScheduleLeg flightScheduleLeg : setFlightScheduleLegs) {
				FlightScheduleLegInfoDTO flightScheduleLegInfoDTO = new FlightScheduleLegInfoDTO();
				flightScheduleLegInfoDTO.setDestination(flightScheduleLeg.getDestination());
				flightScheduleLegInfoDTO.setDuration(flightScheduleLeg.getDuration());
				flightScheduleLegInfoDTO.setEstArrivalDayOffset(flightScheduleLeg.getEstArrivalDayOffset());
				flightScheduleLegInfoDTO.setEstArrivalDayOffsetLocal(flightScheduleLeg.getEstArrivalDayOffsetLocal());
				flightScheduleLegInfoDTO.setEstArrivalTimeLocal(flightScheduleLeg.getEstArrivalTimeLocal());
				flightScheduleLegInfoDTO.setEstArrivalTimeZulu(flightScheduleLeg.getEstArrivalTimeZulu());
				flightScheduleLegInfoDTO.setEstDepartureDayOffset(flightScheduleLeg.getEstDepartureDayOffset());
				flightScheduleLegInfoDTO.setEstDepartureDayOffsetLocal(flightScheduleLeg.getEstDepartureDayOffsetLocal());
				flightScheduleLegInfoDTO.setEstDepartureTimeLocal(flightScheduleLeg.getEstDepartureTimeLocal());
				flightScheduleLegInfoDTO.setEstDepartureTimeZulu(flightScheduleLeg.getEstDepartureTimeZulu());
				flightScheduleLegInfoDTO.setId(flightScheduleLeg.getId());
				flightScheduleLegInfoDTO.setLegNumber(flightScheduleLeg.getLegNumber());
				flightScheduleLegInfoDTO.setOrigin(flightScheduleLeg.getOrigin());

				setFlightScheduleLegInfoDTOs.add(flightScheduleLegInfoDTO);
			}
		}
		return setFlightScheduleLegInfoDTOs;
	}

	private Set<FlightScheduleSegmentInfoDTO> createFlightScheduleSegments(Set<FlightScheduleSegment> setFlightScheduleSegments) {

		Set<FlightScheduleSegmentInfoDTO> setFlightScheduleLegInfoDTOs = new HashSet<FlightScheduleSegmentInfoDTO>();

		if (setFlightScheduleSegments != null && !setFlightScheduleSegments.isEmpty()) {

			for (FlightScheduleSegment flightScheduleSegment : setFlightScheduleSegments) {

				FlightScheduleSegmentInfoDTO flightScheduleSegmentInfoDTO = new FlightScheduleSegmentInfoDTO();
				flightScheduleSegmentInfoDTO.setFlSchSegId(flightScheduleSegment.getFlSchSegId());
				flightScheduleSegmentInfoDTO.setOverlapSegmentId(flightScheduleSegment.getOverlapSegmentId());
				flightScheduleSegmentInfoDTO.setSegmentCode(flightScheduleSegment.getSegmentCode());
				flightScheduleSegmentInfoDTO.setValidFlag(flightScheduleSegment.getValidFlag());

				setFlightScheduleLegInfoDTOs.add(flightScheduleSegmentInfoDTO);
			}
		}
		return setFlightScheduleLegInfoDTOs;
	}
}
