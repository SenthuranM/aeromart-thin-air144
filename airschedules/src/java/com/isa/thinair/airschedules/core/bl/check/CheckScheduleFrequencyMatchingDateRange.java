package com.isa.thinair.airschedules.core.bl.check;

import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.airschedules.core.persistence.dao.FlightScheduleDAO;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * method to check update eligibility
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="checkScheduleFrequencyMatchingDateRange"
 */
public class CheckScheduleFrequencyMatchingDateRange extends DefaultBaseCommand {

	// Dao's
	private FlightScheduleDAO flightSchedleDAO;

	/**
	 * constructor of the CheckScheduleFrequencyMatchingDateRange command
	 */
	public CheckScheduleFrequencyMatchingDateRange() {

		// looking up daos
		flightSchedleDAO = (FlightScheduleDAO) AirSchedulesUtil.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_DAO);
	}

	/**
	 * execute method of the CheckScheduleFrequencyMatchingDateRange command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {

		// getting command params
		FlightSchedule schedule = (FlightSchedule) this.getParameter(CommandParamNames.FLIGHT_SCHEDULE);
		boolean isReviseFrequency = this.getParameter(CommandParamNames.IS_REVISE_FREQUENCY, Boolean.FALSE, Boolean.class);

		// checking params
		checkParams(schedule);

		// constructing command responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);

		// check for invalid frequency for the given date range
		boolean freqMatched = CalendarUtil.frequencyMatchesDateRange(schedule.getStartDate(), schedule.getStopDate(),
				schedule.getFrequency());		
		
		// schedule frequency will be revised if its invalid when processing SSM/SSIM
		if (!freqMatched && isReviseFrequency) {
			Frequency frequency = CalendarUtil.getValidFrequencyMatchesDateRange(schedule.getStartDate(), schedule.getStopDate(),
					schedule.getFrequency());
			if (reValidateRevisedFrequency(frequency)
					&& CalendarUtil.frequencyMatchesDateRange(schedule.getStartDate(), schedule.getStopDate(), frequency)) {
				schedule.setFrequency(frequency);
				freqMatched = true;
			}
		}
		
		if (!freqMatched) {
			responce.setSuccess(false);
			responce.setResponseCode(ResponceCodes.INVALID_SCHEDULE_FREQUENCE_FOR_DATE_RANGE);
		}

		return responce;
	}

	/**
	 * privte method to validate parameters
	 * 
	 * @param schedule
	 * @throws ModuleException
	 */
	private void checkParams(FlightSchedule schedule) throws ModuleException {

		if (schedule == null)// throw exception
			throw new ModuleException("airschedules.arg.invalid.null");
	}
	
	private boolean reValidateRevisedFrequency(Frequency frequency) {
		if (frequency.getDayCount(true) > 0) {
			return true;
		}
		return false;
	}
}
