/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:18:39
 * 
 * $Id$
 * 
 * ===============================================================================
 */

package com.isa.thinair.airschedules.core.persistence.hibernate;

import java.util.List;

import com.isa.thinair.airschedules.api.model.FlightScheduleLeg;
import com.isa.thinair.airschedules.core.persistence.dao.FlightScheduleLegDAO;
import com.isa.thinair.commons.core.framework.PlatformHibernateDaoSupport;

/**
 * FlightScheduleLegDAOImpl is the FlightScheduleLegtDAO implementatiion for hibernate
 * 
 * @author Lasantha Pambagoda
 * @isa.module.dao-impl dao-name="flightScheduleLegDAO"
 */
public class FlightScheduleLegDAOImpl extends PlatformHibernateDaoSupport implements FlightScheduleLegDAO {

	/**
	 * method to get all the flight schedule legs
	 */
	public List<FlightScheduleLeg> getFlightScheduleLegs() {
		return find("from FlightScheduleLeg", FlightScheduleLeg.class);
	}

	/**
	 * method to get flight schedule leg from a given flight schedule leg id
	 */
	public FlightScheduleLeg getFlightScheduleLeg(int id) {
		return (FlightScheduleLeg) get(FlightScheduleLeg.class, new Integer(id));
	}

	/**
	 * method to save flight schedule leg
	 */
	public void saveFlightScheduleLeg(FlightScheduleLeg flightscheduleleg) {
		hibernateSaveOrUpdate(flightscheduleleg);
	}

	/**
	 * method to remove the flight schedule
	 */
	public void removeFlightScheduleLeg(int id) {
		Object flightscheduleleg = load(FlightScheduleLeg.class, new Integer(id));
		delete(flightscheduleleg);
	}
}
