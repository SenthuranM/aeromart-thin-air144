/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:17:16
 * 
 * 
 * ===============================================================================
 */

package com.isa.thinair.airschedules.core.persistence.hibernate;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.sql.DataSource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.StatelessSession;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.isa.thinair.airinventory.api.dto.InvRollForwardFlightsSearchCriteria;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightRollForwardDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SeatAllocationDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.model.FCCSegBCInventory;
import com.isa.thinair.airinventory.api.util.BookingClassUtil;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.PNLConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airschedules.api.criteria.FlightFlightSegmentSearchCriteria;
import com.isa.thinair.airschedules.api.criteria.FlightSearchCriteria;
import com.isa.thinair.airschedules.api.criteria.RollForwardFlightSearchCriteria;
import com.isa.thinair.airschedules.api.dto.AffectedPeriod;
import com.isa.thinair.airschedules.api.dto.AvailableFlightSearchCriteria;
import com.isa.thinair.airschedules.api.dto.BasicFlightDTO;
import com.isa.thinair.airschedules.api.dto.FlightMealNotifyStatusDTO;
import com.isa.thinair.airschedules.api.dto.FlightSummaryDTO;
import com.isa.thinair.airschedules.api.dto.RouteInfoTO;
import com.isa.thinair.airschedules.api.dto.TransferRollForwardFlightsSearchCriteria;
import com.isa.thinair.airschedules.api.dto.TransitAirport;
import com.isa.thinair.airschedules.api.dto.rm.RMFlightSummaryDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightLeg;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.model.FlightsToPublish;
import com.isa.thinair.airschedules.api.utils.AirScheduleCustomConstants;
import com.isa.thinair.airschedules.api.utils.AnciDefaultTemplStatusEnum;
import com.isa.thinair.airschedules.api.utils.FlightSegmentStatusEnum;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.airschedules.api.utils.FlightUtil;
import com.isa.thinair.airschedules.api.utils.ScheduleBuildStatusEnum;
import com.isa.thinair.airschedules.api.utils.ScheduleStatusEnum;
import com.isa.thinair.airschedules.core.persistence.dao.FlightDAO;
import com.isa.thinair.airschedules.core.util.LookUpUtils;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.CommonsConstants.FlightType;
import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;

/**
 * FlightDAOImpl is the FlightDAO implementatiion for hibernate
 * 
 * @author Lasantha Pambagoda
 * @isa.module.dao-impl dao-name="flightDAO"
 */
public class FlightDAOImpl extends PlatformBaseHibernateDaoSupport implements FlightDAO {

	Log log = LogFactory.getLog(getClass());

	private static String systemCarriersSQL = null;
	private static String systemCarriersForInterline = null;
	private static String interlineCarriersExclude = null;
	private static String systemCarriersWithoutInterline = null;

	/**
	 * methos to get flight for a given flight id
	 * 
	 * @param id
	 * @return Flight
	 */
	@Override
	public Flight getFlight(int id) {
		if (log.isDebugEnabled()) {
			log.debug("getFlight called for flightID : " + id);
		}
		return get(Flight.class, new Integer(id));
	}

	@Override
	public List<Flight> getLightWeightFlightDetails(Collection<Integer> flightIds) {
		DataSource ds = LookUpUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);
		String sql = "SELECT f.flight_id, f.flight_number, f.departure_date, f.origin, f.model_number,"
				+ " (SELECT MAX(fs.est_time_arrival_zulu) FROM t_flight_segment fs WHERE fs.flight_id = f.flight_id ) "
				+ "as est_time_arrival_zulu FROM t_flight f WHERE f.flight_id IN (" + Util.buildIntegerInClauseContent(flightIds)
				+ ")";
		List<Flight> flights = templete.query(sql.toString(), new ResultSetExtractor<List<Flight>>() {
			@Override
			public List<Flight> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<Flight> flights = new ArrayList<Flight>();
				while (rs.next()) {
					Flight flight = new Flight(rs.getInt("FLIGHT_ID"), rs.getString("FLIGHT_NUMBER"), rs
							.getTimestamp("DEPARTURE_DATE"), rs.getString("ORIGIN"), rs.getString("MODEL_NUMBER"), rs
							.getTimestamp("EST_TIME_ARRIVAL_ZULU"));
					flights.add(flight);
				}
				return flights;
			}
		});
		return flights;

	}

	/**
	 * method to get no of flights
	 */
	@Override
	public int getNoOfFlights() {

		int noOfFlights = 0;
		try {

			noOfFlights = ((Long) getSession().createQuery("select count(*) from Flight").uniqueResult()).intValue();

		} catch (Exception e) {
			throw new CommonsDataAccessException(e, null);
		}

		return noOfFlights;
	}

	/**
	 * Retrieve the flight details.
	 */
	@Override
	public List<Flight> getFlightDetails(String flightNumber) {
		return find("from Flight flight where flight.flightNumber = ?", flightNumber, Flight.class);
	}

	/**
	 * Retrieve the flight detail for a given flight number and departure date.
	 */
	@Override
	public Flight getSpecificFlightDetail(FlightSearchCriteria searchCriteria) {

		String hql = "select f from Flight as f " + " where f.departureDate >= ? "
				+ " and f.departureDate <= ? and f.status =? ";
		if (searchCriteria.getFlightNumber() != null) {
			hql += " and f.flightNumber='" + searchCriteria.getFlightNumber() + "'";
		}
		if (searchCriteria.getOrigin() != null) {
			hql += " and f.originAptCode ='" + searchCriteria.getOrigin() + "'";
		}
		if (searchCriteria.getDestination() != null) {
			hql += " and destinationAptCode ='" + searchCriteria.getDestination() + "'";
		}
		if (searchCriteria.getCsOcFlightNumber() != null) {
			hql += " and csOCFlightNumber ='" + searchCriteria.getCsOcFlightNumber() + "'";
		}

		Object[] params = { CalendarUtil.getStartTimeOfDate(searchCriteria.getEstDepartureDate()), CalendarUtil.getEndTimeOfDate(searchCriteria.getEstDepartureDate()),
				FlightStatusEnum.ACTIVE.getCode() };
		List<Flight> results = find(hql, params, Flight.class);
		if (results != null && results.size() == 1) {
			return results.get(0);
		} else {
			return null;
		}
	}

	/**
	 * Retrieve the flight detail for a given flight number and departure date in local.
	 */
	@Override
	public FlightSegement getFlightSegmentForLocalDate(String flightNumber, Date departureDateLocal) {

		String hql = "select fs from Flight as f, FlightSegement as fs " + "where f.flightId = fs.flightId and "
				+ "f.flightNumber='" + flightNumber + "' and " + "fs.estTimeDepatureLocal >= ? and "
				+ "fs.estTimeDepatureLocal <= ? and " + "fs.status ='OPN' and f.status = 'ACT'";

		Object[] params = { CalendarUtil.getStartTimeOfDate(departureDateLocal),
				CalendarUtil.getEndTimeOfDate(departureDateLocal) };
		List<FlightSegement> results = find(hql, params, FlightSegement.class);
		if (results != null && results.size() == 1) {
			return results.get(0);
		} else {
			return null;
		}
	}

	/**
	 * Retrieve the flight detail for a given flight number and departure date in local.
	 */
	@Override
	public FlightSegement getAllFlightSegmentForLocalDate(String flightNumber, Date departureDateLocal) {

		String hql = "select fs from Flight as f, FlightSegement as fs " + "where f.flightId = fs.flightId and "
				+ "f.flightNumber='" + flightNumber + "' and " + "fs.estTimeDepatureLocal >= ? and "
				+ "fs.estTimeDepatureLocal <= ? ";

		Object[] params = { CalendarUtil.getStartTimeOfDate(departureDateLocal),
				CalendarUtil.getEndTimeOfDate(departureDateLocal) };
		List<FlightSegement> results = find(hql, params, FlightSegement.class);
		if (results != null && results.size() == 1) {
			return results.get(0);
		} else {
			return null;
		}
	}

	@Override
	public List<Date> getFlightDates(Collection<Integer> flightIds) {
		String hql = "select f.departureDate from Flight as f where f.flightId in ("
				+ BeanUtils.constructINStringForInts(flightIds) + ")";
		return find(hql, Date.class);
	}

	/**
	 * method to serch flights
	 */
	@Override
	public Page<Flight> searchFlightNew(List<ModuleCriterion> criteria, int startIndex, int pageSize,
			boolean isDataSourceReporting) {

		Page<Flight> result = null;
		StatelessSession reportingDbSession = null;
		Connection connection = null;
		try {
			if (AppSysParamsUtil.isReportingDbAvailable() && isDataSourceReporting) {
				DataSource ds = LookUpUtils.getReportingDatasource();
				connection = ds.getConnection();
				reportingDbSession = getSessionFactory().openStatelessSession(connection);
			}

			List<String> orderBy = new ArrayList<String>();
			orderBy.add("departureDate");
			orderBy.add("flightNumber");
			result = this.getPagedDataNew(criteria, startIndex, pageSize, Flight.class, orderBy, (Session) reportingDbSession);

			if (isDataSourceReporting && connection != null && reportingDbSession != null) {
				reportingDbSession.close();
				connection.close();
			}

		} catch (Exception e) {
			log.error(e.getStackTrace());
			throw new CommonsDataAccessException(e, null);
		}
		return result;
	}

	/**
	 * method to serch flights for checkin
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Page<Flight> searchFlightForCheckin(FlightFlightSegmentSearchCriteria criteria, int startIndex, int pageSize) {
		log.debug("searchFlightForCheckin(FlightFlightSegmentSearchCriteria criteria()");
		String hql = getHql(criteria);

		if (log.isDebugEnabled()) {
			log.debug("GET CHECKIN FLIGHTS : HQL : " + hql);
		}

		Query mainQuery = getSession().createQuery(hql);

		if (criteria.getFlightNumber() != null) {
			mainQuery.setParameter("flightNumber", criteria.getFlightNumber());
		}

		if (criteria.getEstStartTimeZulu() != null) {
			mainQuery.setParameter("estStartTimeZulu", criteria.getEstStartTimeZulu());
		}

		if (criteria.getEstEndTimeZulu() != null) {
			mainQuery.setParameter("estEndTimeZulu", criteria.getEstEndTimeZulu());
		}

		if (criteria.getSegmentCode() != null) {
			mainQuery.setParameter("segmentCode", criteria.getSegmentCode());
		}

		if (criteria.getCheckinStatus() != null) {
			mainQuery.setParameter("checkinStatus", criteria.getCheckinStatus());
		}

		if (criteria.getStatus() != null) {
			mainQuery.setParameter("status", criteria.getStatus());
		}

		List<Flight> uniqueResults = new ArrayList<Flight>(new LinkedHashSet<Flight>(mainQuery.setFirstResult(startIndex)
				.setMaxResults(pageSize).list()));
		return new Page<Flight>(uniqueResults.size(), startIndex, startIndex + pageSize, uniqueResults);
	}

	/**
	 * method to save the flight
	 */
	@Override
	public void saveFlight(Flight flight) {
		hibernateSaveOrUpdate(flight);
	}

	/**
	 * method to remove the flight
	 */
	@Override
	public void removeFlight(int id) {
		Object flight = load(Flight.class, new Integer(id));
		delete(flight);
	}

	/**
	 * batch update schedule id of the flights
	 */
	@Override
	public void batchUpdateScheduleIdOfFlights(int newScheduleId, int originalScheduleId, AffectedPeriod period) {
		try {

			String frequancyComparison = (period.getFrequency().getDay0() ? "dayNumber=0 or " : "")
					+ (period.getFrequency().getDay1() ? "dayNumber=1 or " : "")
					+ (period.getFrequency().getDay2() ? "dayNumber=2 or " : "")
					+ (period.getFrequency().getDay3() ? "dayNumber=3 or " : "")
					+ (period.getFrequency().getDay4() ? "dayNumber=4 or " : "")
					+ (period.getFrequency().getDay5() ? "dayNumber=5 or " : "")
					+ (period.getFrequency().getDay6() ? "dayNumber=6 or " : "");

			if (frequancyComparison.lastIndexOf("or") != -1) {
				frequancyComparison = frequancyComparison.substring(0, frequancyComparison.lastIndexOf("or"));
			}

			String hqlUpdate = "update Flight set scheduleId =:newScheduleId " + "where "
					+ "scheduleId = :originalScheduleId and " + "departureDate >= :startDate and "
					+ "departureDate <= :stopDate and " + " ( " + frequancyComparison + " )";

			getSession().createQuery(hqlUpdate).setParameter("newScheduleId", new Integer(newScheduleId))
					.setParameter("originalScheduleId", new Integer(originalScheduleId))
					.setParameter("startDate", period.getStartDate()).setParameter("stopDate", period.getEndDate())
					.executeUpdate();

		} catch (Exception e) {
			throw new CommonsDataAccessException(e, null);
		}
	}

	/**
	 * batch update flight status of the flights
	 */
	@Override
	public void batchUpdateFlightStatus(Collection<Integer> flightIds, String status) {
		try {

			String hqlUpdate = "update Flight set status =:status " + "where " + "flightId in (:flightIds)";

			getSession().createQuery(hqlUpdate).setParameter("status", status).setParameterList("flightIds", flightIds)
					.executeUpdate();

		} catch (Exception e) {
			throw new CommonsDataAccessException(e, null);
		}
	}

	/**
	 * get the flights for a given period
	 */
	@Override
	public int getNumberOfNonCanceledFlights(int scheduleId) {

		int noOfFlights = 0;
		try {

			noOfFlights = ((Long) getSession().createQuery(
					"select count(*) from Flight where scheduleId = ? and status != ?").setParameter(0, scheduleId)
					.setParameter(1, FlightStatusEnum.CANCELLED.getCode()).uniqueResult()).intValue();

		} catch (Exception e) {
			throw new CommonsDataAccessException(e, null);
		}

		return noOfFlights;
	}

	/**
	 * gets conflicting schedules for given date
	 */
	@Override
	public List<FlightSchedule>
			getConflictingSchedules(int dayNumber, String flightNumber, Date depatureDate, Integer scheduleId) {

		String frequancyComparison = "fs.frequency.day0=1";
		if (dayNumber == 1) {
			frequancyComparison = "fs.frequency.day1=1";
		}
		if (dayNumber == 2) {
			frequancyComparison = "fs.frequency.day2=1";
		}
		if (dayNumber == 3) {
			frequancyComparison = "fs.frequency.day3=1";
		}
		if (dayNumber == 4) {
			frequancyComparison = "fs.frequency.day4=1";
		}
		if (dayNumber == 5) {
			frequancyComparison = "fs.frequency.day5=1";
		}
		if (dayNumber == 6) {
			frequancyComparison = "fs.frequency.day6=1";
		}

		String hql = "select fs from FlightSchedule as fs " + "where " + "fs.flightNumber='" + flightNumber + "' and "
				+ "fs.buildStatusCode='" + ScheduleBuildStatusEnum.PENDING.getCode() + "' and " + "trunc(fs.startDate) <= ? and "
				+ "trunc(fs.stopDate) >= ? and "
				+ ((scheduleId != null) ? ("fs.scheduleId != " + scheduleId.intValue() + " and ") : "") + frequancyComparison;

		Object[] params = { depatureDate, depatureDate };

		return find(hql, params, FlightSchedule.class);
	}

	/**
	 * public method to check conflicting flights [criteria] flight#, deprature date, status = ACT/CRE/CLS exclude
	 * flightId passed
	 */
	@Override
	public List<Flight> getConflictingFlights(String flightNumber, Date departureDate, Integer flightId) {

		String hql = "select f from Flight as f " + "where " + "f.flightNumber= ? and "
				+ "f.departureDate >= ? and " + "f.departureDate <= ? and " + "(f.status =? or f.status =? or f.status =?) "
				+ ((flightId != null) ? "and f.flightId != " + flightId.intValue() : "");

		Object[] params = { flightNumber, CalendarUtil.getStartTimeOfDate(departureDate),
				CalendarUtil.getEndTimeOfDate(departureDate), FlightStatusEnum.ACTIVE.getCode(),
				FlightStatusEnum.CREATED.getCode(), FlightStatusEnum.CLOSED.getCode() };

		return find(hql, params, Flight.class);
	}

	/**
	 * get flight summary details
	 */
	@Override
	public List<FlightSummaryDTO> getFlightSummary(InvRollForwardFlightsSearchCriteria criteria) {

		String hql = "select new com.isa.thinair.airschedules.api.dto.FlightSummaryDTO(f.flightId, f.departureDate) "
				+ "from Flight as f " + "where " + "f.departureDate >= ? and " + "f.departureDate <= ?";

		ArrayList<Date> params = new ArrayList<Date>();
		params.add(criteria.getFromDate());
		params.add(criteria.getToDate());

		if (criteria.getFrequency() != null) {

			String frequancyComparison = (criteria.getFrequency().getDay0() ? "f.dayNumber=0 or " : "")
					+ (criteria.getFrequency().getDay1() ? "f.dayNumber=1 or " : "")
					+ (criteria.getFrequency().getDay2() ? "f.dayNumber=2 or " : "")
					+ (criteria.getFrequency().getDay3() ? "f.dayNumber=3 or " : "")
					+ (criteria.getFrequency().getDay4() ? "f.dayNumber=4 or " : "")
					+ (criteria.getFrequency().getDay5() ? "f.dayNumber=5 or " : "")
					+ (criteria.getFrequency().getDay6() ? "f.dayNumber=6 or " : "");

			if (frequancyComparison.lastIndexOf("or") != -1) {
				frequancyComparison = frequancyComparison.substring(0, frequancyComparison.lastIndexOf("or"));
			}

			hql = hql + " and ( " + frequancyComparison + " )";
		}

		String flightNum = criteria.getFlightNumber();
		if (!(flightNum == null || flightNum.trim().equals(""))) {
			hql = hql + " and f.flightNumber = '" + flightNum + "'";
		}

		String origin = criteria.getOriginAirportCode();
		if (!(origin == null || origin.trim().equals(""))) {
			hql = hql + " and f.originAptCode = '" + origin + "'";
		}

		String destination = criteria.getDestinationAirportCode();
		if (!(destination == null || destination.trim().equals(""))) {
			hql = hql + " and f.destinationAptCode = '" + destination + "'";
		}

		Set<String> set = criteria.getSegmentCodes();
		if (set.size() > 0) {

			hql = hql + " and ( select count(*) from FlightSegement as fs " + "where "
					+ "f.flightId = fs.flightId and fs.segmentCode in (";

			Iterator<String> it = set.iterator();
			while (it.hasNext()) {

				String segmentCode = it.next();
				hql = hql + "'" + segmentCode + "',";
			}

			if (hql.lastIndexOf(",") != -1) {
				hql = hql.substring(0, hql.lastIndexOf(","));
			}
			hql = hql + ")) = " + set.size();
		}

		return find(hql, params.toArray(), FlightSummaryDTO.class);
	}

	/**
	 * get flight summary details
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<Flight> getLikeFlights(TransferRollForwardFlightsSearchCriteria criteria) {

		String hql = "select f " + "from Flight as f " + "where " + "f.departureDate >= ? and " + "f.departureDate <= ?"
				+ " and (f.status = '" + FlightStatusEnum.ACTIVE.getCode() + "'";
		
		
		if(criteria.isSource()){
			hql = hql + " or f.status = '" + FlightStatusEnum.CANCELLED.getCode() + "'";
		}

		if (criteria.isIncludeClsFlights()) {
			hql = hql + " or f.status = '" + FlightStatusEnum.CLOSED.getCode() + "')";
		} else {
			hql = hql + ")";
		}

		ArrayList<Date> params = new ArrayList<Date>();
		params.add(criteria.getFromDate());
		params.add(criteria.getToDate());
		criteria.setToDate(CalendarUtil.getEndTimeOfDate(criteria.getToDate()));
		if (criteria.getFrequency() != null) {

			String frequancyComparison = (criteria.getFrequency().getDay0() ? "f.dayNumber=0 or " : "")
					+ (criteria.getFrequency().getDay1() ? "f.dayNumber=1 or " : "")
					+ (criteria.getFrequency().getDay2() ? "f.dayNumber=2 or " : "")
					+ (criteria.getFrequency().getDay3() ? "f.dayNumber=3 or " : "")
					+ (criteria.getFrequency().getDay4() ? "f.dayNumber=4 or " : "")
					+ (criteria.getFrequency().getDay5() ? "f.dayNumber=5 or " : "")
					+ (criteria.getFrequency().getDay6() ? "f.dayNumber=6 or " : "");

			if (frequancyComparison.lastIndexOf("or") != -1) {
				frequancyComparison = frequancyComparison.substring(0, frequancyComparison.lastIndexOf("or"));
			}

			hql = hql + " and ( " + frequancyComparison + " )";
		}

		/*
		 * ---------------------------------------------------------------------- --
		 */
		/*
		 * uncomment this portion if non-scheduled flights should also be selected
		 */
		/*
		 * if (criteria.getFlight().getScheduleId() == null) { //add flight # check String flightNum =
		 * criteria.getFlight().getFlightNumber(); if (!(flightNum == null || flightNum.trim().equals(""))) { hql = hql
		 * + " and f.flightNumber = '" + flightNum + "'"; }
		 * 
		 * //add segment departure/arrival time check Set set = criteria.getFlight().getFlightSegements(); if
		 * (set.size() > 0) {
		 * 
		 * hql = hql + " and ( select count(fs.flightId) from FlightSegement as fs " + "where " +
		 * "f.flightId = fs.flightId and (";
		 * 
		 * Iterator it = set.iterator(); while (it.hasNext()) { FlightSegement segment = (FlightSegement) it.next();
		 * String segmentCode = segment.getSegmentCode(); hql = hql + "( fs.segmentCode = '" + segmentCode + "' and ";
		 * hql = hql + "fs.estTimeDepatureZulu = " + segment.getEstTimeDepatureZulu() + " and "; hql = hql +
		 * "fs.estTimeArrivalZulu = " + segment.getEstTimeArrivalZulu() + " ) or ";
		 * 
		 * }
		 * 
		 * if (hql.lastIndexOf("or") != -1) hql = hql.substring(0, hql.lastIndexOf("or")); hql = hql + ")) = " +
		 * set.size(); } } else { hql = hql + " and f.scheduleId = " + criteria.getFlight().getScheduleId().intValue() +
		 * ""; }
		 */
		/*
		 * ---------------------------------------------------------------------- --
		 */

		hql = hql + " and f.scheduleId = " + criteria.getFlight().getScheduleId().intValue() + " order by f.departureDate";

		/*
		 * ---------------------------------------------------------------------- --
		 */
		/*
		 * uncomment this portion if non-scheduled flights should also be selected
		 */
		/*
		 * Set set = criteria.getFlight().getFlightSegements(); if (set.size() > 0) {
		 * 
		 * hql = hql + " and ( select count(fs.flightId) from FlightSegement as fs " + "where " +
		 * "f.flightId = fs.flightId and fs.segmentCode in (";
		 * 
		 * Iterator it = set.iterator(); while (it.hasNext()) { FlightSegement segment = (FlightSegement) it.next();
		 * String segmentCode = segment.getSegmentCode(); hql = hql + "'" + segmentCode + "',"; }
		 * 
		 * if (hql.lastIndexOf(",") != -1) hql = hql.substring(0, hql.lastIndexOf(",")); hql = hql + ")) = " +
		 * set.size(); }
		 */
		/*
		 * ---------------------------------------------------------------------- --
		 */

		if (log.isDebugEnabled()) {
			log.debug("Get Like Flights hql: " + hql);
		}
		return getSession().createQuery(hql).setTimestamp(0, criteria.getFromDate()).setTimestamp(1, criteria.getToDate()).list();
	}

	/**
	 * get flight summary details
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<FlightSummaryDTO> getFlightSummary(Collection<Integer> flightIDs) {

		List<FlightSummaryDTO> list = null;

		try {

			String hql = "select new com.isa.thinair.airschedules.api.dto.FlightSummaryDTO(f.flightId, f.departureDate) "
					+ "from Flight as f " + "where " + "f.flightId in (:flightIDs) and " + "f.status != :statusCancelled "
					+ "order by f.departureDate";

			list = getSession().createQuery(hql).setParameterList("flightIDs", flightIDs)
					.setParameter("statusCancelled", FlightStatusEnum.CANCELLED.getCode()).list();

		} catch (Exception e) {

			throw new CommonsDataAccessException(e, null);
		}

		return list;
	}

	@Override
	public List<Integer> getSeatTemplateAttachedFlights(Collection<Integer> flightIdList) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		String sql = "select fs.flight_id as FLGHTID from sm_t_flight_am_seat fas,t_flight_segment fs where fs.flt_seg_id=fas.flt_seg_id "
				+ " and fs.flight_id in ( " + Util.buildIntegerInClauseContent(flightIdList) + ") group by fs.flight_id ";
		return jdbcTemplate.query(sql, new ResultSetExtractor<List<Integer>>() {
			List<Integer> alreadySeatTemplateAttachedFlightIds = new ArrayList<Integer>();

			@Override
			public List<Integer> extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs != null) {
					while (rs.next()) {
						alreadySeatTemplateAttachedFlightIds.add(rs.getInt("FLGHTID"));
					}
				}
				return alreadySeatTemplateAttachedFlightIds;
			}
		});

	}

	/**
	 * get flight summary details
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<FlightSummaryDTO> getLikeFlightSummary(InvRollForwardFlightsSearchCriteria criteria) {

		String hql = "select * from ( select f.flight_id, f.departure_date, f.flight_number " + "from t_flight f " + "where "
				+ "f.departure_date >= ? and " + "f.departure_date <= ?";

		Object[] params = new Object[2];
		params[0] = criteria.getFromDate();
		params[1] = criteria.getToDate();

		// ArrayList params = new ArrayList();
		// params.add(criteria.getFromDate());
		// params.add(criteria.getToDate());

		if (criteria.getFrequency() != null) {

			String frequancyComparison = (criteria.getFrequency().getDay0() ? "f.day_number=0 or " : "")
					+ (criteria.getFrequency().getDay1() ? "f.day_number=1 or " : "")
					+ (criteria.getFrequency().getDay2() ? "f.day_number=2 or " : "")
					+ (criteria.getFrequency().getDay3() ? "f.day_number=3 or " : "")
					+ (criteria.getFrequency().getDay4() ? "f.day_number=4 or " : "")
					+ (criteria.getFrequency().getDay5() ? "f.day_number=5 or " : "")
					+ (criteria.getFrequency().getDay6() ? "f.day_number=6 or " : "");

			if (frequancyComparison.lastIndexOf("or") != -1) {
				frequancyComparison = frequancyComparison.substring(0, frequancyComparison.lastIndexOf("or"));
			}

			hql = hql + " and ( " + frequancyComparison + " )";
		}

		String flightNum = criteria.getFlightNumber();
		if (!(flightNum == null || flightNum.trim().equals(""))) {
			hql = hql + " and f.flight_number = '" + flightNum + "'";
		}

		String origin = criteria.getOriginAirportCode();
		if (!(origin == null || origin.trim().equals(""))) {
			hql = hql + " and f.origin = '" + origin + "'";
		}

		String destination = criteria.getDestinationAirportCode();
		if (!(destination == null || destination.trim().equals(""))) {
			hql = hql + " and f.destination = '" + destination + "'";
		}

		Set<String> set = criteria.getSegmentVsSelectedBookingClassesMap().keySet();
		if (set.size() > 0) {
			hql = hql + " and f.flight_id in ( select distinct f2.flight_id from t_flight f2, t_flight_segment fs "; // ,
																														// t_fcc_seg_alloc
																														// fsa
																														// "
			if (criteria.getSeatFactorMin() != null || criteria.getSeatFactorMax() != null) {
				hql = hql + " , t_fcc_seg_alloc fsa ";
			}
			hql = hql + "where " + "f2.flight_id = fs.flight_id and ( fs.segment_code like ";

			Iterator<String> it = set.iterator();
			while (it.hasNext()) {

				String segmentCode = it.next();
				if (segmentCode.contains("***")) {
					segmentCode = segmentCode.replaceAll("\\*", "_");
				}
				hql = hql + "'" + segmentCode + "' or fs.segment_code like ";
			}

			if (hql.lastIndexOf("or fs.segment_code like") != -1) {
				hql = hql.substring(0, hql.lastIndexOf("or fs.segment_code like"));
			}
			hql = hql + ")";
			// seat factor inclusion for the query
			if (criteria.getSeatFactorMin() != null && criteria.getSeatFactorMax() != null) {
				hql = hql + " and fsa.flight_id= f2.flight_id " + " and fsa.segment_code = fs.segment_code "
						+ " and (((fsa.sold_seats+ fsa.on_hold_seats)/ fsa.allocated_seats)*100) between "
						+ criteria.getSeatFactorMin() + " and " + criteria.getSeatFactorMax() + " ";
			} else if (criteria.getSeatFactorMin() != null && criteria.getSeatFactorMax() == null) {
				hql = hql + " and fsa.flight_id= f2.flight_id " + " and fsa.segment_code = fs.segment_code "
						+ " and (((fsa.sold_seats+ fsa.on_hold_seats)/ fsa.allocated_seats)*100) >= "
						+ criteria.getSeatFactorMin() + " ";
			} else if (criteria.getSeatFactorMin() == null && criteria.getSeatFactorMax() != null) {
				hql = hql + " and fsa.flight_id= f2.flight_id " + " and fsa.segment_code = fs.segment_code "
						+ " and (((fsa.sold_seats+ fsa.on_hold_seats)/ fsa.allocated_seats)*100) <= "
						+ criteria.getSeatFactorMax() + " ";
			}

			hql = hql + " )";
		}

		hql += " AND f.status <> 'CNX' order by f.departure_date, f.flight_number )";
		if (criteria.isRestrictNumberOfRecords()) {
			int rownum = Integer.parseInt(CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.OPTIMIZE_MAX_FLIGHTS));
			hql += " where rownum <= " + rownum + " ";
		}

		JdbcTemplate jt = new JdbcTemplate(getDataSource());
		List<FlightSummaryDTO> list = jt.query(hql, params,
				new ResultSetExtractor<List<FlightSummaryDTO>>() {

					@Override
					public List<FlightSummaryDTO> extractData(ResultSet rs) throws SQLException, DataAccessException {

						FlightSummaryDTO flightSummeryDTO;
						List<FlightSummaryDTO> summeries = new ArrayList<FlightSummaryDTO>();

						if (rs != null) {
							while (rs.next()) {
								flightSummeryDTO = new FlightSummaryDTO(0, null, null);

								flightSummeryDTO.setFlightId(rs.getInt("flight_id"));
								flightSummeryDTO.setDepatureDate(rs.getDate("departure_date"));
								flightSummeryDTO.setFlightNumber(rs.getString("flight_number"));

								summeries.add(flightSummeryDTO);
							}
						}

						return summeries;
					}
				});

		return list;

	}

	/**
	 * gets overlapping flight
	 */
	@Override
	public Flight getOverlappingFlight(int scheduleId, Date flyDate) {

		Flight flight = null;

		try {

			String hqlUpdate = "select flt from Flight as flt " + "where " + "flt.scheduleId = :scheduleId and "
					+ "flt.departureDate = :flyDate and " + "( flt.status = :statusActive or " + "flt.status = :statusCreated )";

			flight = (Flight) getSession().createQuery(hqlUpdate).setParameter("scheduleId", new Integer(scheduleId))
					.setParameter("flyDate", flyDate).setParameter("statusActive", FlightStatusEnum.ACTIVE.getCode())
					.setParameter("statusCreated", FlightStatusEnum.CREATED.getCode()).uniqueResult();

		} catch (Exception e) {

			throw new CommonsDataAccessException(e, null);
		}

		return flight;
	}

	/**
	 * mehtod to get possible overlapping flights
	 */
	@SuppressWarnings("unchecked")
	public Collection<Flight> getPossibleOverlappingFlights2(Flight flight) {

		Collection<Flight> flights = new ArrayList<Flight>();
		Set<FlightLeg> legs = flight.getFlightLegs();
		Iterator<FlightLeg> it = legs.iterator();

		while (it.hasNext()) {

			FlightLeg leg = it.next();

			Date depature = leg.getEstDepartureTimeZulu();
			Date arrival = leg.getEstArrivalTimeZulu();

			try {

				String hql = "select distinct f " + "from FlightLeg as fl, Flight as f " + "where "
						+ "fl.estArrivalTimeZulu = :arivalTime and " + "fl.estDepartureTimeZulu = :departureTime and "
						+ "fl.origin = :origin and " + "fl.destination = :destination and "
						+ "f.departureDate = :departureDate and " + "f.status =:status and " + "f.modelNumber = :modelNumber"
						+ ((flight.getFlightId() != null) ? (" and f.flightId != " + flight.getFlightId().intValue()) : "");

				Collection<Flight> schedsForLeg = getSession().createQuery(hql).setParameter("arivalTime", arrival)
						.setParameter("departureTime", depature).setParameter("origin", leg.getOrigin())
						.setParameter("destination", leg.getDestination())
						.setParameter("departureDate", flight.getDepartureDate()).setParameter("status", flight.getStatus())
						.setParameter("modelNumber", flight.getModelNumber()).list();

				flights.addAll(schedsForLeg);

			} catch (Exception e) {
				throw new CommonsDataAccessException(e, null);
			}
		}
		return flights;
	}

	/**
	 * mehtod to get possible overlapping flights
	 */
	@Override
	public Collection<Flight> getPossibleOverlappingFlights(Flight flight) {

		ArrayList<Object> paramList = new ArrayList<Object>();

		Set<FlightSegement> legs = flight.getFlightSegements();
		Iterator<FlightSegement> it = legs.iterator();

		String dinamicPart = "( ";

		while (it.hasNext()) {

			FlightSegement fseg = it.next();
			dinamicPart = dinamicPart + "( fs.estTimeArrivalZulu = ? and " + "fs.estTimeDepatureZulu = ? and "
					+ "fs.segmentCode = ? ) or ";

			paramList.add(fseg.getEstTimeArrivalZulu());
			paramList.add(fseg.getEstTimeDepatureZulu());
			paramList.add(fseg.getSegmentCode());
		}

		if (dinamicPart.lastIndexOf("or") != -1) {
			dinamicPart = dinamicPart.substring(0, dinamicPart.lastIndexOf("or"));
		}

		dinamicPart = dinamicPart + " ) and ";

		String hql = "select distinct f " + "from FlightSegement as fs, Flight as f " + "where "
				+ "fs.flightId = f.flightId and " + dinamicPart + "( f.status = ?  or f.status = ? or f.status = ? ) and "
				+ "f.modelNumber = ? and " + "f.operationTypeId = ? "
				+ ((flight.getFlightId() != null) ? (" and f.flightId != " + flight.getFlightId().intValue()) : "")
				+ " and f.scheduleId = null ";

		paramList.add(FlightStatusEnum.ACTIVE.getCode());
		paramList.add(FlightStatusEnum.CLOSED.getCode());
		paramList.add(FlightStatusEnum.CREATED.getCode());
		paramList.add(flight.getModelNumber());
		paramList.add(new Integer(flight.getOperationTypeId()));

		return find(hql, paramList.toArray(), Flight.class);
	}

	/**
	 * method to change flight status
	 */
	@Override
	public void changeFlightStatus(Collection<Integer> flights, FlightStatusEnum status) {

		try {

			if ((status.getCode().equals(FlightStatusEnum.ACTIVE.getCode()) || status.getCode().equals(
					FlightStatusEnum.CREATED.getCode()))
					&& (flights != null && flights.size() != 0)) {

				FlightStatusEnum oldStatus = (status.getCode().equals(FlightStatusEnum.ACTIVE.getCode()))
						? FlightStatusEnum.CREATED
						: FlightStatusEnum.ACTIVE;

				String hqlUpdate = "update Flight set status =:status " + "where " + "status = :oldStatus and "
						+ Util.buildIntegerOrClauseContent(flights, "flightId");

				getSession().createQuery(hqlUpdate).setParameter("status", status.getCode())
						.setParameter("oldStatus", oldStatus.getCode()).executeUpdate();
			}

		} catch (Exception e) {
			throw new CommonsDataAccessException(e, null);
		}

	}

	/**
	 * method to change flight status for Manage Flts <b>Validations are done on front end and only appropiate flights
	 * are forwarded for updates.</b>
	 */
	@Override
	public void manageFlightStatus(Collection<Integer> flights, FlightStatusEnum status) {

		try {

			String hqlUpdate = "update Flight set status =:status " + "where " + "flightId in (:flightIds)";

			getSession().createQuery(hqlUpdate).setParameter("status", status.getCode()).setParameterList("flightIds", flights)
					.executeUpdate();

		} catch (Exception e) {
			throw new CommonsDataAccessException(e, null);
		}
	}

	/**
	 * method to change flight status for Manage Flts
	 * <p>
	 * Validated updated to ACT only if current status is CLS wise versa
	 * </p>
	 */
	@Override
	public void reinstateFlights(Integer scheduleID, FlightStatusEnum newStatus) {

		try {

			FlightStatusEnum oldStatus = (newStatus.getCode().equals(FlightStatusEnum.ACTIVE.getCode()))
					? FlightStatusEnum.CLOSED
					: FlightStatusEnum.ACTIVE;

			String hqlUpdate = "update Flight set status =:status " + "where "
					+ "schedule_id = :scheduleID and status = :oldStatus and departure_date > sysdate";
			getSession().createQuery(hqlUpdate).setParameter("status", newStatus.getCode())
					.setParameter("scheduleID", scheduleID).setParameter("oldStatus", oldStatus.getCode()).executeUpdate();

		} catch (Exception e) {
			throw new CommonsDataAccessException(e, null);
		}
	}

	@Override
	public List<AvailableFlightSegment> getDirectFlightSegments(AvailableFlightSearchCriteria searchCriteria)
			throws ModuleException {

		boolean isAllowMakeReservationsInCutOffTime = searchCriteria.isAllowFlightSearchAfterCutOffTime();
		boolean isAllowOverbookAfterCutoffTime = searchCriteria.isAllowDoOverbookAfterCutOffTime();
		final boolean isIBFlight = searchCriteria.isInboundFlight();
		final int ondSequence = searchCriteria.getOndSequence();
		final boolean isMultiCitySearch = searchCriteria.isMultiCitySearch();
		final boolean isOverbookBeforeCutoffTime = (searchCriteria.getAvailabilityRestriction() == AvailableFlightSearchDTO.AVAILABILTY_NO_RESTRICTION)
				? true
				: false;

		// if no external booking classes for interline search then no further
		// search.
		if (searchCriteria.isOnlyInterline()
				&& (searchCriteria.getExternalBookingClasses() == null || searchCriteria.getExternalBookingClasses().size() == 0)) {
			return new ArrayList<AvailableFlightSegment>();
		}

		StringBuffer sql = new StringBuffer();

		sql.append("SELECT FLT_SEG_ID, CS_OC_CARRIER_CODE, FLIGHT_ID, SEGMENT_CODE, EST_TIME_DEPARTURE_LOCAL, EST_TIME_ARRIVAL_LOCAL, EST_TIME_DEPARTURE_ZULU, ");
		sql.append(" EST_TIME_ARRIVAL_ZULU, AVAILABLE_SEATS, LOGICAL_CABIN_CLASS_CODE, CABIN_CLASS_CODE, AVAILABLE_INFANT_SEATS, ORIGIN, ");
		sql.append(" DESTINATION, FLIGHT_NUMBER, operation_type_id, DEPARTURE_DATE, FLIGHT_TYPE, DEPARTURE_TERMINAL_NAME, ARRIVAL_TERMINAL_NAME, ");
		sql.append(" MODEL_NUMBER, ITIN_DESCRIPTION, remarks, status, SUM(sold_seats) AS sold_fixed_seats, FIXED_SEATS FROM ( ");
		sql.append("SELECT ");
		sql.append(" DISTINCT DECODE (bc.fixed_flag, 'Y', SUM(fsba.sold_seats), 0) sold_seats,flt.cs_oc_carrier_code, fltseg.FLT_SEG_ID, fltseg.FLIGHT_ID, fltseg.SEGMENT_CODE, fltseg.EST_TIME_DEPARTURE_LOCAL, ");
		sql.append("fltseg.EST_TIME_ARRIVAL_LOCAL, fltseg.EST_TIME_DEPARTURE_ZULU, fltseg.EST_TIME_ARRIVAL_ZULU, ");
		sql.append("fccseg.AVAILABLE_SEATS, logcc.LOGICAL_CABIN_CLASS_CODE, logcc.CABIN_CLASS_CODE,");
		sql.append(" fccseg.AVAILABLE_INFANT_SEATS, flt.ORIGIN, flt.DESTINATION, flt.FLIGHT_NUMBER, flt.operation_type_id,");
		sql.append(" flt.DEPARTURE_DATE, flt.FLIGHT_TYPE, depterminal.terminal_shortname AS DEPARTURE_TERMINAL_NAME, ");
		sql.append(" arrterminal.terminal_shortname AS ARRIVAL_TERMINAL_NAME, ");
		sql.append(" fltmod.MODEL_NUMBER,fltmod.ITIN_DESCRIPTION,flt.REMARKS,fltseg.STATUS,fccseg.fixed_seats AS FIXED_SEATS ");
		sql.append("FROM ");
		sql.append("T_FLIGHT flt,T_AIRCRAFT_MODEL fltmod,T_FLIGHT_SEGMENT fltseg ");
		sql.append(" LEFT OUTER JOIN t_airport_terminal arrterminal ");
		sql.append(" ON (fltseg.arrival_terminal_id = arrterminal.airport_terminal_id) ");
		sql.append(" LEFT OUTER JOIN t_airport_terminal depterminal ");
		sql.append(" ON (fltseg.departure_terminal_id = depterminal.airport_terminal_id), ");
		sql.append(" T_FCC_SEG_ALLOC fccseg, ");
		sql.append(" T_LOGICAL_CABIN_CLASS logcc, ");
		sql.append(" T_FCC_SEG_BC_ALLOC fsba, ");
		sql.append(" t_booking_class bc ");
		if (searchCriteria.isOnlyInterline()) {
			sql.append(", T_FCC_SEG_BC_ALLOC fccbc ");
		}
		sql.append("WHERE ");
		sql.append("fltseg.FLIGHT_ID = flt.FLIGHT_ID and ");
		sql.append("fccseg.SEGMENT_CODE = fltseg.SEGMENT_CODE and ");
		sql.append("fccseg.FLIGHT_ID = fltseg.FLIGHT_ID and ");
		sql.append("fltseg.SEGMENT_VALID_FLAG = 'Y' and ");
		sql.append("fccseg.logical_cabin_class_code = logcc.logical_cabin_class_code and ");
		if (searchCriteria.isOnlyInterline()) {
			sql.append("fccbc.FCCSA_ID =  fccseg.FCCSA_ID  and ");
			sql.append("fccbc.STATUS =  '" + FCCSegBCInventory.Status.OPEN + "'  and ");
			sql.append("fccbc.BOOKING_CODE in (");
			sql.append(Util.buildStringInClauseContent(searchCriteria.getExternalBookingClasses()));
			sql.append(") and ");
		}

		if (isMultiCitySearch
				&& !SalesChannelsUtil.isAppEngineWebOrMobile(searchCriteria.getAppIndicator())) {
			sql.append("logcc.CABIN_CLASS_CODE IN (SELECT CABIN_CLASS_CODE FROM T_LOGICAL_CABIN_CLASS WHERE status='ACT') and ");
		} else {
			if (searchCriteria.getLogicalCabinClass() != null && !searchCriteria.getLogicalCabinClass().equals("")) {
				sql.append("(logcc.LOGICAL_CABIN_CLASS_CODE = '" + searchCriteria.getLogicalCabinClass()
						+ "' or flt.operation_type_id = " + AirScheduleCustomConstants.OperationTypes.BUS_SERVICE + ") and ");
			} else {
				sql.append("logcc.CABIN_CLASS_CODE = '" + searchCriteria.getCabinClass() + "' and ");
			}
		}
		sql.append("fltseg.STATUS = '" + FlightSegmentStatusEnum.OPEN.getCode() + "' and ");
		sql.append("flt.STATUS = '" + FlightStatusEnum.ACTIVE.getCode() + "' and logcc.STATUS = 'ACT' and ");
		if (searchCriteria.getMaxDateZulu() != null) {
			sql.append("fltseg.EST_TIME_DEPARTURE_ZULU >= to_date('"
					+ CalendarUtil.getDateInFormattedString("dd-MMM-yyyy HH:mm:ss", searchCriteria.getMinDate())
					+ "', 'DD-MON-YYYY HH24:MI:SS') and ");
		} else {
			sql.append("fltseg.EST_TIME_DEPARTURE_LOCAL >= to_date('"
					+ CalendarUtil.getDateInFormattedString("dd-MMM-yyyy HH:mm:ss", searchCriteria.getMinDate())
					+ "', 'DD-MON-YYYY HH24:MI:SS') and ");
		}

		if (searchCriteria.getMaxDateZulu() != null) {
			sql.append("fltseg.EST_TIME_DEPARTURE_ZULU <= to_date('"
					+ CalendarUtil.getDateInFormattedString("dd-MMM-yyyy HH:mm:ss", searchCriteria.getMaxDate())
					+ "', 'DD-MON-YYYY HH24:MI:SS') and ");
		} else {
			sql.append("fltseg.EST_TIME_DEPARTURE_LOCAL <= to_date('"
					+ CalendarUtil.getDateInFormattedString("dd-MMM-yyyy HH:mm:ss", searchCriteria.getMaxDate())
					+ "', 'DD-MON-YYYY HH24:MI:SS') and ");
		}
		if (searchCriteria.getArrMinDate() != null && searchCriteria.getArrMaxDate() != null) {
			sql.append("fltseg.EST_TIME_ARRIVAL_LOCAL >= to_date('"
					+ CalendarUtil.getDateInFormattedString("dd-MMM-yyyy HH:mm:ss", searchCriteria.getArrMinDate())
					+ "', 'DD-MON-YYYY HH24:MI:SS') and ");
			sql.append("fltseg.EST_TIME_ARRIVAL_LOCAL <= to_date('"
					+ CalendarUtil.getDateInFormattedString("dd-MMM-yyyy HH:mm:ss", searchCriteria.getArrMaxDate())
					+ "', 'DD-MON-YYYY HH24:MI:SS') and ");
		}

		String segmentCode = searchCriteria.getOrigin() + "/%" + searchCriteria.getDestination();
		sql.append("fltseg.SEGMENT_CODE like  '" + segmentCode + "'");

		sql.append(" AND fltseg.est_time_departure_zulu > sysdate ");

		if (searchCriteria.isIncludeOnlyMarketingCarrierSegs()) {
			sql.append(prepareFlightNumberSQL(searchCriteria.isInterline()));
		} else if (searchCriteria.isInterline()) {
			sql.append(getInterlineExcludeQuery());
		}

		if (isAllowOverbookAfterCutoffTime && isAllowMakeReservationsInCutOffTime) {
			searchCriteria.setAvailabilityRestriction(AvailableFlightSearchDTO.AVAILABILTY_NO_RESTRICTION);
		}
		if (searchCriteria.getAvailabilityRestriction() == AvailableFlightSearchDTO.AVAILABILTY_RESTRICTED) {
			sql.append("and fccseg.AVAILABLE_SEATS >=  " + new Integer(searchCriteria.getAdults()));
			sql.append("and fccseg.AVAILABLE_INFANT_SEATS >= " + new Integer(searchCriteria.getInfants()));
		}
		sql.append("AND flt.model_number = fltmod.model_number ");
		if (!isAllowMakeReservationsInCutOffTime
				&& !(searchCriteria.isModifyBooking() && searchCriteria.isSourceFlightInCutOffTime() && SalesChannelsUtil
						.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_APIAGENCIES_KEY) == searchCriteria.getChannelCode())) {
			sql.append("AND ((flt.flight_type ='INT' AND sysdate<=fltseg.est_time_departure_zulu-"
					+ searchCriteria.getCutOffTimeForInternational());
			sql.append("/86400000) OR (flt.flight_type = 'DOM' AND sysdate <=fltseg.est_time_departure_zulu - ");
			sql.append(searchCriteria.getCutOffTimeForDomestic() + "/86400000))");
		}
		sql.append(" AND fsba.fccsa_id = fccseg.fccsa_id ");
		sql.append(" AND fsba.booking_code = bc.booking_code ");
		
		// Restricting CodeShare MC flight for Open Return
		if (searchCriteria.isOpenReturn()) {
			sql.append("   AND flt.cs_oc_flight_number  IS NULL ");
		}
		
		sql.append(" GROUP BY fltseg.FLT_SEG_ID, flt.cs_oc_carrier_code, fltseg.FLIGHT_ID, fltseg.SEGMENT_CODE, fltseg.EST_TIME_DEPARTURE_LOCAL, fltseg.EST_TIME_ARRIVAL_LOCAL, ");
		sql.append(" fltseg.EST_TIME_DEPARTURE_ZULU, fltseg.EST_TIME_ARRIVAL_ZULU, fccseg.AVAILABLE_SEATS, logcc.LOGICAL_CABIN_CLASS_CODE, ");
		sql.append(" logcc.CABIN_CLASS_CODE, fccseg.AVAILABLE_INFANT_SEATS, flt.ORIGIN, flt.DESTINATION, flt.FLIGHT_NUMBER, flt.operation_type_id, ");
		sql.append(" flt.DEPARTURE_DATE, flt.flight_type, depterminal.terminal_shortname , arrterminal.terminal_shortname , fltmod.MODEL_NUMBER, ");
		sql.append(" fltmod.ITIN_DESCRIPTION, flt.REMARKS, fltseg.status, fccseg.fixed_seats , bc.fixed_flag ");
		sql.append(" order by flt.DEPARTURE_DATE ");
		sql.append(" ) GROUP BY FLT_SEG_ID, CS_OC_CARRIER_CODE, FLIGHT_ID, SEGMENT_CODE, EST_TIME_DEPARTURE_LOCAL, EST_TIME_ARRIVAL_LOCAL, EST_TIME_DEPARTURE_ZULU, ");
		sql.append(" EST_TIME_ARRIVAL_ZULU, AVAILABLE_SEATS, LOGICAL_CABIN_CLASS_CODE, CABIN_CLASS_CODE, AVAILABLE_INFANT_SEATS, ORIGIN, ");
		sql.append(" DESTINATION, FLIGHT_NUMBER, operation_type_id, DEPARTURE_DATE, FLIGHT_TYPE, DEPARTURE_TERMINAL_NAME, ARRIVAL_TERMINAL_NAME, ");
		sql.append(" MODEL_NUMBER, ITIN_DESCRIPTION, remarks, status, FIXED_SEATS ");
		sql.append(" ORDER BY DEPARTURE_DATE ");

		if (log.isDebugEnabled()) {
			log.debug("Flight search query for : from :" + searchCriteria.getOrigin() + "destination : "
					+ searchCriteria.getDestination() + " is : " + sql.toString());
		}
		final long currentTime = CalendarUtil.getCurrentSystemTimeInZulu().getTime();
		final long cutOffForInternational = searchCriteria.getCutOffTimeForInternational();
		final long cutOffForDomestic = searchCriteria.getCutOffTimeForDomestic();
		final boolean allowDoOverbookingAfterCutOffTime = searchCriteria.isAllowDoOverbookAfterCutOffTime();
		final int adultCount = searchCriteria.getAdults();
		final int childCount = searchCriteria.getChilds();

		JdbcTemplate jt = new JdbcTemplate(getDataSource());
		List<AvailableFlightSegment> list = jt.query(sql.toString(),
				new ResultSetExtractor<List<AvailableFlightSegment>>() {
					@Override
					public List<AvailableFlightSegment> extractData(ResultSet rs) throws SQLException, DataAccessException {
						Map<Integer, AvailableFlightSegment> availableFlightSegments = new LinkedHashMap<Integer, AvailableFlightSegment>();
						if (rs != null) {
							while (rs.next()) {
								long flightCutoffDiff = 0;
								Integer flightId = rs.getInt("FLIGHT_ID");
								String logicalCabinClass = rs.getString("LOGICAL_CABIN_CLASS_CODE");
								if (!availableFlightSegments.keySet().contains(flightId)) {
									AvailableFlightSegment availableFlightSegment = new AvailableFlightSegment();
									FlightSegmentDTO flightSegmentDTO = new FlightSegmentDTO();
									flightSegmentDTO.setFlightId(flightId);
									flightSegmentDTO.setArrivalDateTime(rs.getTimestamp("EST_TIME_ARRIVAL_LOCAL"));
									flightSegmentDTO.setDepartureDateTime(rs.getTimestamp("EST_TIME_DEPARTURE_LOCAL"));
									flightSegmentDTO.setArrivalDateTimeZulu(rs.getTimestamp("EST_TIME_ARRIVAL_ZULU"));
									flightSegmentDTO.setDepartureDateTimeZulu(rs.getTimestamp("EST_TIME_DEPARTURE_ZULU"));
									flightSegmentDTO.setSegmentCode(rs.getString("SEGMENT_CODE"));
									flightSegmentDTO.setFromAirport(rs.getString("ORIGIN"));
									flightSegmentDTO.setToAirport(rs.getString("DESTINATION"));
									flightSegmentDTO.setFlightNumber(rs.getString("FLIGHT_NUMBER"));
									flightSegmentDTO.setSegmentId(new Integer(rs.getInt("FLT_SEG_ID")));
									flightSegmentDTO.setOperationTypeID(new Integer(rs.getInt("operation_type_id")));
									flightSegmentDTO.setFlightModelNumber(rs.getString("MODEL_NUMBER"));
									flightSegmentDTO.setFlightModelDescription(rs.getString("ITIN_DESCRIPTION"));
									String durationTime = FlightUtil.getDuration(rs.getTimestamp("EST_TIME_DEPARTURE_ZULU"),
											rs.getTimestamp("EST_TIME_ARRIVAL_ZULU"));
									flightSegmentDTO.setFlightDuration(durationTime);
									flightSegmentDTO.setRemarks(rs.getString("REMARKS"));
									flightSegmentDTO.setFlightSegmentStatus(rs.getString("STATUS"));

									String flightType = rs.getString("FLIGHT_TYPE");
									flightSegmentDTO.setFlightType(flightType);
									if (FlightType.DOMESTIC.getType().equals(flightType)) {
										flightSegmentDTO.setDomesticFlight(true);
										flightCutoffDiff = (flightSegmentDTO.getDepartureDateTimeZulu().getTime() - cutOffForDomestic)
												- currentTime;
									} else {
										flightCutoffDiff = (flightSegmentDTO.getDepartureDateTimeZulu().getTime() - cutOffForInternational)
												- currentTime;
									}
									int nonSoldFixedSeats = rs.getInt("FIXED_SEATS") - rs.getInt("sold_fixed_seats");
									if (rs.getInt("AVAILABLE_SEATS") >= nonSoldFixedSeats) {
										if ((rs.getInt("AVAILABLE_SEATS") - nonSoldFixedSeats) < (adultCount + childCount)) {
											if (flightCutoffDiff < 0 && allowDoOverbookingAfterCutOffTime) {
												flightSegmentDTO.setOverBookEnabled(true);
											}
											if (flightCutoffDiff > 0 && !isOverbookBeforeCutoffTime) {
												continue;
											}
										}
									} else {
										if (flightCutoffDiff < 0 && allowDoOverbookingAfterCutOffTime) {
											flightSegmentDTO.setOverBookEnabled(true);
										}
										if (flightCutoffDiff > 0 && !isOverbookBeforeCutoffTime
												&& (rs.getInt("AVAILABLE_SEATS") < adultCount)) {
											continue;
										}
									}
									if ((rs.getInt("AVAILABLE_SEATS") - nonSoldFixedSeats) < adultCount
											&& (flightCutoffDiff < 0 && allowDoOverbookingAfterCutOffTime)) {
										flightSegmentDTO.setOverBookEnabled(true);
									}

									if (flightCutoffDiff < 0) {
										flightSegmentDTO.setFlightWithinCutoff(true);
									}
									if (rs.getString("DEPARTURE_TERMINAL_NAME") != null) {
										flightSegmentDTO.setDepartureTerminal(rs.getString("DEPARTURE_TERMINAL_NAME"));
									}
									if (rs.getString("ARRIVAL_TERMINAL_NAME") != null) {
										flightSegmentDTO.setArrivalTerminal(rs.getString("ARRIVAL_TERMINAL_NAME"));
									}
									flightSegmentDTO.setCsOcCarrierCode(rs.getString("CS_OC_CARRIER_CODE"));
									availableFlightSegment.setCabinClassCode(rs.getString("CABIN_CLASS_CODE"));
									availableFlightSegment.getFlightSegmentDTOs().add(flightSegmentDTO);
									availableFlightSegment.setInboundFlightSegment(isIBFlight);
									availableFlightSegment.setOndSequence(ondSequence);
									availableFlightSegments.put(flightId, availableFlightSegment);
								}
								availableFlightSegments.get(flightId).getFlightSegmentDTO()
										.setAvailableAdultConunt(logicalCabinClass, rs.getInt("AVAILABLE_SEATS"));
								availableFlightSegments.get(flightId).getFlightSegmentDTO()
										.setAvailableInfantConunt(logicalCabinClass, rs.getInt("AVAILABLE_INFANT_SEATS"));
							}
						}
						return new ArrayList<AvailableFlightSegment>(availableFlightSegments.values());
					}
				});
		return list;
	}

	private static String getInterlineExcludeQuery() {
		if (interlineCarriersExclude == null) {
			synchronized (FlightDAOImpl.class) {
				Set<String> blkEnabledInterlineCodes = CommonsServices.getGlobalConfig().getInterlinedAirlineCodes();
				int cntInt = 0;
				for (String carrierCode : blkEnabledInterlineCodes) {
					if (cntInt == 0) {
						interlineCarriersExclude = " AND ( ";
						interlineCarriersExclude += " flt.FLIGHT_NUMBER NOT like '" + carrierCode + "%' ";
					} else {
						interlineCarriersExclude += " AND flt.FLIGHT_NUMBER NOT like '" + carrierCode + "%' ";
					}
					++cntInt;
				}
				if (cntInt > 0) {
					interlineCarriersExclude += " ) ";
				} else {
					interlineCarriersExclude = "";
				}
			}
		}
		return interlineCarriersExclude;
	}

	private static String getFlightNoSQLWithoutInterline() {
		if (systemCarriersWithoutInterline == null) {
			synchronized (FlightDAOImpl.class) {
				Set<String> ownCarrierCodes = CommonsServices.getGlobalConfig().getOwnAirlineCarrierCodes();
				int cntInt = 0;

				for (String carrierCode : ownCarrierCodes) {
					if (cntInt == 0) {
						systemCarriersWithoutInterline = " AND ( ";
						systemCarriersWithoutInterline += " flt.FLIGHT_NUMBER like '" + carrierCode + "%' ";
					} else {
						systemCarriersWithoutInterline += " OR flt.FLIGHT_NUMBER like '" + carrierCode + "%' ";
					}
					++cntInt;
				}

				if (cntInt > 0) {
					systemCarriersWithoutInterline += " ) ";
				} else {
					systemCarriersWithoutInterline = "";
				}
			}
		}
		return systemCarriersWithoutInterline;
	}

	private static String prepareFlightNumberSQL(boolean isInterline) {
		if (systemCarriersSQL == null) {
			synchronized (FlightDAOImpl.class) {
				if (systemCarriersSQL == null) {
					Collection<String> systemCarriers = CommonsServices.getGlobalConfig().getActiveSysCarriersMap().keySet();
					Set<String> blkEnabledInterlineCodes = CommonsServices.getGlobalConfig().getInterlinedAirlineCodes();

					Iterator<String> it = systemCarriers.iterator();
					int cnt = 0;
					int cntInt = 0;
					while (it.hasNext()) {
						String item = it.next();
						if (!blkEnabledInterlineCodes.contains(item)) {
							if (cntInt == 0) {
								systemCarriersForInterline = " AND ( ";
								systemCarriersForInterline += " flt.FLIGHT_NUMBER like '" + item + "%' ";
							} else {
								systemCarriersForInterline += " OR flt.FLIGHT_NUMBER like '" + item + "%' ";
							}
							++cntInt;
						}
						if (cnt == 0) {
							systemCarriersSQL = " AND ( ";
							systemCarriersSQL += " flt.FLIGHT_NUMBER like '" + item + "%' ";
						} else {
							systemCarriersSQL += " OR flt.FLIGHT_NUMBER like '" + item + "%' ";
						}
						++cnt;
					}
					if (cnt > 0) {
						systemCarriersSQL += " ) ";
					} else {
						systemCarriersSQL = "";
					}
					if (cntInt > 0) {
						systemCarriersForInterline += " ) ";
					} else {
						systemCarriersForInterline = "";
					}
				}
			}
		}
		if (isInterline) {
			return systemCarriersForInterline;
		} else {
			return systemCarriersSQL;
		}
	}

	/**
	 * gets AvailableFlightSegments
	 */
	@Override
	public AvailableFlightSegment getAvailableFlightSegments(Integer segmentId, String cabinClassCode,
			final boolean isInboundFlight, final int ondSequence, boolean ignoreFlightStatus, final boolean isUnTouchedOnd)
			throws ModuleException {

		final boolean isIBFlight = isInboundFlight;
		String sql = "SELECT fltseg.FLIGHT_ID, fltseg.SEGMENT_CODE, fltseg.EST_TIME_DEPARTURE_LOCAL, "
				+ "fltseg.EST_TIME_ARRIVAL_LOCAL, fltseg.EST_TIME_DEPARTURE_ZULU, fltseg.EST_TIME_ARRIVAL_ZULU, "
				+ "fccseg.AVAILABLE_SEATS, fccseg.AVAILABLE_INFANT_SEATS, flt.ORIGIN, flt.DESTINATION, "
				+ "flt.FLIGHT_NUMBER, fltseg.FLT_SEG_ID, flt.operation_type_id, flt.FLIGHT_TYPE, "
				+ "depterminal.terminal_shortname AS DEPARTURE_TERMINAL_NAME, arrterminal.terminal_shortname "
				+ "AS ARRIVAL_TERMINAL_NAME,fltmod.MODEL_NUMBER,fltmod.ITIN_DESCRIPTION,flt.REMARKS,fltseg.STATUS, "
				+ "flt.CS_OC_CARRIER_CODE, fccseg.LOGICAL_CABIN_CLASS_CODE, logcc.CABIN_CLASS_CODE "
				+ "FROM T_FLIGHT flt,T_AIRCRAFT_MODEL fltmod, T_FLIGHT_SEGMENT fltseg "
				+ "LEFT OUTER JOIN t_airport_terminal arrterminal ON fltseg.arrival_terminal_id = arrterminal.airport_terminal_id "
				+ "LEFT OUTER JOIN t_airport_terminal depterminal ON fltseg.departure_terminal_id = depterminal.airport_terminal_id, "
				+ "T_FCC_SEG_ALLOC fccseg , t_logical_cabin_class logcc " + "WHERE fltseg.FLIGHT_ID = flt.FLIGHT_ID and "
				+ ((ignoreFlightStatus || isUnTouchedOnd) ? "" : "flt.status = 'ACT' and ")
				+ "fccseg.SEGMENT_CODE = fltseg.SEGMENT_CODE and fccseg.FLIGHT_ID = fltseg.FLIGHT_ID and "
				+ "fccseg.logical_cabin_class_code = logcc.logical_cabin_class_code and fltseg.FLT_SEG_ID = " + segmentId
				+ " AND flt.model_number = fltmod.model_number and logcc.cabin_class_code = '" + cabinClassCode + "'"
				+ " ORDER BY flt.DEPARTURE_DATE";

		final long currentTime = CalendarUtil.getCurrentSystemTimeInZulu().getTime();
		AvailableFlightSegment availableFlightSegment = new JdbcTemplate(getDataSource()).query(sql,
				new ResultSetExtractor<AvailableFlightSegment>() {
					@Override
					public AvailableFlightSegment extractData(ResultSet rs) throws SQLException, DataAccessException {
						AvailableFlightSegment availFlightSegment = new AvailableFlightSegment();
						int iteration = 0;
						if (rs != null) {
							while (rs.next()) {
								long flightCutoffDiff = 0;
								String logicalCabinClass = rs.getString("LOGICAL_CABIN_CLASS_CODE");
								if (iteration == 0) {
									FlightSegmentDTO flightSegmentDTO = new FlightSegmentDTO();
									flightSegmentDTO.setFlightId(new Integer(rs.getInt("FLIGHT_ID")));
									flightSegmentDTO.setArrivalDateTime(rs.getTimestamp("EST_TIME_ARRIVAL_LOCAL"));
									flightSegmentDTO.setDepartureDateTime(rs.getTimestamp("EST_TIME_DEPARTURE_LOCAL"));
									flightSegmentDTO.setArrivalDateTimeZulu(rs.getTimestamp("EST_TIME_ARRIVAL_ZULU"));
									flightSegmentDTO.setDepartureDateTimeZulu(rs.getTimestamp("EST_TIME_DEPARTURE_ZULU"));
									flightSegmentDTO.setSegmentCode(rs.getString("SEGMENT_CODE"));
									flightSegmentDTO.setFromAirport(rs.getString("ORIGIN"));
									flightSegmentDTO.setToAirport(rs.getString("DESTINATION"));
									flightSegmentDTO.setFlightNumber(rs.getString("FLIGHT_NUMBER"));
									flightSegmentDTO.setDepartureTerminal(rs.getString("DEPARTURE_TERMINAL_NAME"));
									flightSegmentDTO.setArrivalTerminal(rs.getString("ARRIVAL_TERMINAL_NAME"));
									flightSegmentDTO.setOperationTypeID(new Integer(rs.getInt("operation_type_id")));
									flightSegmentDTO.setSegmentId(new Integer(rs.getInt("FLT_SEG_ID")));
									if (FlightType.DOMESTIC.getType().equals(rs.getString("FLIGHT_TYPE"))) {
										flightSegmentDTO.setDomesticFlight(true);
									}
									flightSegmentDTO.setFlightModelNumber(rs.getString("MODEL_NUMBER"));
									flightSegmentDTO.setFlightModelDescription(rs.getString("ITIN_DESCRIPTION"));
									String durationTime = FlightUtil.getDuration(rs.getTimestamp("EST_TIME_DEPARTURE_ZULU"),
											rs.getTimestamp("EST_TIME_ARRIVAL_ZULU"));
									flightSegmentDTO.setFlightDuration(durationTime);
									flightSegmentDTO.setRemarks(rs.getString("REMARKS"));
									flightSegmentDTO.setFlightSegmentStatus(rs.getString("STATUS"));
									flightSegmentDTO.setCsOcCarrierCode(rs.getString("CS_OC_CARRIER_CODE"));
									availFlightSegment.getFlightSegmentDTOs().add(flightSegmentDTO);
									availFlightSegment.setInboundFlightSegment(isInboundFlight);
									availFlightSegment.setOndSequence(ondSequence);
									availFlightSegment.setCabinClassCode(rs.getString("CABIN_CLASS_CODE"));
									availFlightSegment.setUnTouchedOnd(isUnTouchedOnd);
									iteration++;
								}

								String cutoffTimeStr = null;
								try {
									cutoffTimeStr = FlightUtil.getApplicableResCutoffTime(new Integer(rs.getInt("FLIGHT_ID")));
								} catch (ModuleException e) {
									e.printStackTrace();
								}
								if (cutoffTimeStr != null) {
									flightCutoffDiff = (availFlightSegment.getFlightSegmentDTO().getDepartureDateTimeZulu()
											.getTime() - getClosureInMilliseconds(cutoffTimeStr))
											- currentTime;
								}
								if (flightCutoffDiff < 0) {
									availFlightSegment.getFlightSegmentDTO().setFlightWithinCutoff(true);
								}
								availFlightSegment.getFlightSegmentDTO().setAvailableAdultConunt(logicalCabinClass,
										rs.getInt("AVAILABLE_SEATS"));
								availFlightSegment.getFlightSegmentDTO().setAvailableInfantConunt(logicalCabinClass,
										rs.getInt("AVAILABLE_INFANT_SEATS"));
							}
						}
						return availFlightSegment;
					}
				});
		return availableFlightSegment;
	}

	private Long getClosureInMilliseconds(String cutoffTimeStr) {
		String hours = cutoffTimeStr.substring(0, cutoffTimeStr.indexOf(":"));
		String mins = cutoffTimeStr.substring(cutoffTimeStr.indexOf(":") + 1);
		return (Long.parseLong(hours) * 60 + Long.parseLong(mins)) * 60 * 1000;
	}

	/**
	 * method to get flight ids related with the given aircraft model
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Integer> getFlightIDsForModel(String modelNumber, Date departureDateFrom) {

		List<Integer> list = null;

		try {

			String hql = "select f.flightId " + "from Flight as f " + "where " + "f.modelNumber = :modelNumber ";
			if (departureDateFrom != null) {
				hql += " and f.departureDate >= :depatureDate";
			}

			Query q = getSession().createQuery(hql).setParameter("modelNumber", modelNumber);
			if (departureDateFrom != null) {
				q.setTimestamp("depatureDate", departureDateFrom);
			}
			list = q.list();

		} catch (Exception e) {

			throw new CommonsDataAccessException(e, null);
		}

		return list;
	}

	/**
	 * Method to get flights related with the given aircraft model
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Flight> getFlightsForModel(String modelNumber, Date departureDateFrom) {

		List<Flight> list = null;

		try {

			String hql = "select f " + "from Flight as f " + "where " + "f.modelNumber = :modelNumber ";
			if (departureDateFrom != null) {
				hql += " and f.departureDate >= :depatureDate";
			}

			Query q = getSession().createQuery(hql).setParameter("modelNumber", modelNumber);
			if (departureDateFrom != null) {
				q.setTimestamp("depatureDate", departureDateFrom);
			}
			list = q.list();

		} catch (Exception e) {

			throw new CommonsDataAccessException(e, null);
		}

		return list;
	}

	/**
	 * Method to get non canceled future flights for a given aircraft model
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Flight> getFutureNonCNXFlightsForModel(String modelNumber, Date departureDateFrom) {

		List<Flight> list = null;

		try {

			String hql = "select f " + "from Flight as f " + "where " + "f.modelNumber = :modelNumber ";
			if (departureDateFrom != null) {
				hql += " and f.departureDate >= :depatureDate and f.status <> :status";
			}

			Query q = getSession().createQuery(hql).setParameter("modelNumber", modelNumber).setParameter("status", "CNX");
			if (departureDateFrom != null) {
				q.setTimestamp("depatureDate", departureDateFrom);
			}
			list = q.list();

		} catch (Exception e) {

			throw new CommonsDataAccessException(e, null);
		}

		return list;
	}

	/**
	 * method to get flights for terminal
	 */
	@Override
	public List<Integer> getFlightsForTerminal(Integer terminalId) {

		List<Integer> list = null;
		try {

			String hql = "select fltSegId from FlightSegement where " + " departureTerminalId = '" + terminalId + "' or "
					+ " arrivalTerminalId = '" + terminalId + "'";

			list = find(hql, Integer.class);

		} catch (Exception e) {

			throw new CommonsDataAccessException(e, null);
		}

		return list;
	}

	/**
	 * method to get frequency of the schedule of the given filght
	 */
	@Override
	public Frequency getScheduleFrequencyOfFlight(int flightId) {

		Frequency frequency = null;

		try {

			String hql = "select fs.frequency from FlightSchedule as fs, Flight as f " + "where "
					+ "fs.scheduleId = f.scheduleId " + "and f.flightId = :flightId";

			frequency = (Frequency) getSession().createQuery(hql).setParameter("flightId", new Integer(flightId)).uniqueResult();

		} catch (Exception e) {

			throw new CommonsDataAccessException(e, null);
		}

		return frequency;
	}

	/**
	 * 
	 */

	@Override
	public boolean closeFlightForReservation(int flightId, String departureStation) throws CommonsDataAccessException {

		// String
		// hql="UPDATE  FlightSegement as fseg SET  fseg.status=CLOSED WHERE fseg.flightId="+flightId+"  AND fseg.segmentCode="+segmentCode;
		boolean isClosed = false;

		String sqlQuery = "update T_FLIGHT_SEGMENT  set STATUS='CLS' where FLIGHT_ID=? and substr(SEGMENT_CODE,0,3)=?  ";

		DataSource ds = LookUpUtils.getDatasource();

		JdbcTemplate templete = new JdbcTemplate(ds);

		Object[] params = { new Integer(flightId), departureStation };

		int updatedRows = templete.update(sqlQuery, params);

		if (updatedRows > 0) {
			isClosed = true;
		}
		return isClosed;
	}

	// //////////////////////////////////////////////////
	// NEW METHODS FOR GET FLIGHTS FOR AFFECTED PERIOD
	// ///////////////////////////////////////////////////

	/**
	 * to get the not overwritten flight ids for fly dates
	 */

	@SuppressWarnings("unchecked")
	@Override
	public List<Integer> getNotOverwritenFlightIdsForFlyDates(int scheduleId, Collection<Date> flyDates) {

		if (flyDates == null || flyDates.size() == 0) {
			return new ArrayList<Integer>();
		}
		List<Integer> list = null;
		try {

			String hql = "select flt.flightId from Flight as flt where " + "flt.scheduleId = :scheduleId and "
					+ "flt.manuallyChanged = 'N' and " + "flt.status != :status and " + "flt.departureDate in (:flyDates)";

			list = getSession().createQuery(hql).setParameter("scheduleId", new Integer(scheduleId))
					.setParameter("status", FlightStatusEnum.CANCELLED.getCode()).setParameterList("flyDates", flyDates).list();

		} catch (Exception e) {

			throw new CommonsDataAccessException(e, null);
		}

		return list;
	}

	/**
	 * to get the overwriten flight ids for fly dates
	 */
	@Override
	public List<Integer> getOverwritenFlightIdsForFlyDates(int scheduleId, Collection<Date> flyDates) {

		if (flyDates == null || flyDates.size() == 0) {
			return new ArrayList<Integer>();
		}
		List<Integer> list = null;

		String hql = "select flt.flightId from Flight as flt where " + "flt.scheduleId = " + scheduleId
				+ " and flt.status != ? and (";

		String dyanmicHql = "";
		ArrayList<Object> paramList = new ArrayList<Object>();
		paramList.add(FlightStatusEnum.CANCELLED.getCode());

		for (Date date : flyDates) {
			GregorianCalendar calander = new GregorianCalendar();
			calander.setTime(CalendarUtil.getStartTimeOfDate(date));
			calander.add(GregorianCalendar.DATE, 1);

			Date endTime = calander.getTime();

			dyanmicHql += "( flt.departureDate >= ? and flt.departureDate < ? ) or ";
			paramList.add(CalendarUtil.getStartTimeOfDate(date));
			paramList.add(endTime);
		}

		if (dyanmicHql.lastIndexOf("or") != -1) {
			dyanmicHql = dyanmicHql.substring(0, dyanmicHql.lastIndexOf("or"));
		}

		hql = hql + dyanmicHql + ")";

		list = this.find(hql, paramList.toArray(), Integer.class);

		return list;
	}

	/**
	 * to get overwriten flight ids for periods
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Integer> getOverwritenFlightIdsForPeriod(int scheduleId, Date startDate, Date endDate) {

		List<Integer> list = null;

		try {

			String hql = "select flt.flightId from Flight as flt where " + "flt.scheduleId = :scheduleId and "
					+ "flt.status != :status and " + "flt.departureDate >= :startDate and flt.departureDate <= :endDate ";

			list = getSession().createQuery(hql).setParameter("scheduleId", new Integer(scheduleId))
					.setParameter("status", FlightStatusEnum.CANCELLED.getCode()).setParameter("startDate", startDate)
					.setParameter("endDate", endDate).list();

		} catch (Exception e) {
			throw new CommonsDataAccessException(e, null);
		}

		return list;
	}

	/**
	 * to get the not overwritten flight ids for fly dates
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Flight> getNotOverwritenFlightsForFlyDates(int scheduleId, Collection<Date> flyDates) {

		if (flyDates == null || flyDates.size() == 0) {
			return new ArrayList<Flight>();
		}
		List<Flight> list = null;
		try {

			String hql = "select flt from Flight as flt where " + "flt.scheduleId = :scheduleId and "
					+ "flt.manuallyChanged = 'N' and " + "flt.status != :status and " + "flt.departureDate in (:flyDates)";

			list = getSession().createQuery(hql).setParameter("scheduleId", new Integer(scheduleId))
					.setParameter("status", FlightStatusEnum.CANCELLED.getCode()).setParameterList("flyDates", flyDates).list();

		} catch (Exception e) {

			throw new CommonsDataAccessException(e, null);
		}

		return list;
	}

	/**
	 * to get the overwriten flight ids for fly dates
	 */
	@Override
	public List<Flight> getOverwritenFlightsForFlyDates(int scheduleId, Collection<Date> flyDates) {

		if (flyDates == null || flyDates.size() == 0) {
			return new ArrayList<Flight>();
		}
		List<Flight> list = null;

		String hql = "select flt from Flight as flt where " + "flt.scheduleId = " + scheduleId + " and flt.status != ? and ( ";

		String dyanmicHql = "";
		ArrayList<Object> paramList = new ArrayList<Object>();
		paramList.add(FlightStatusEnum.CANCELLED.getCode());

		Iterator<Date> it = flyDates.iterator();
		while (it.hasNext()) {

			Date date = it.next();
			GregorianCalendar calander = new GregorianCalendar();
			calander.setTime(CalendarUtil.getStartTimeOfDate(date));
			calander.add(GregorianCalendar.DATE, 1);

			Date endTime = calander.getTime();

			dyanmicHql += "( flt.departureDate >= ? and flt.departureDate < ? ) or ";
			paramList.add(CalendarUtil.getStartTimeOfDate(date));
			paramList.add(endTime);
		}

		if (dyanmicHql.lastIndexOf("or") != -1) {
			dyanmicHql = dyanmicHql.substring(0, dyanmicHql.lastIndexOf("or"));
		}

		hql = hql + dyanmicHql + ")";

		list = this.find(hql, paramList.toArray(), Flight.class);

		return list;
	}

	/**
	 * to get overwriten flight ids for periods
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Flight> getOverwritenFlightsForPeriod(int scheduleId, Date startDate, Date endDate) {

		List<Flight> list = null;

		try {

			String hql = "select flt from Flight as flt where " + "flt.scheduleId = :scheduleId and "
					+ "flt.status != :status and " + "flt.departureDate >= :startDate and flt.departureDate <= :endDate ";

			list = getSession().createQuery(hql).setParameter("scheduleId", new Integer(scheduleId))
					.setParameter("status", FlightStatusEnum.CANCELLED.getCode()).setParameter("startDate", startDate)
					.setParameter("endDate", endDate).list();

		} catch (Exception e) {
			throw new CommonsDataAccessException(e, null);
		}

		return list;
	}

	/**
	 * to get all overwriten flight ids
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Flight> getAllOverwritenFlights(int scheduleId) {

		List<Flight> list = null;

		try {

			String hql = "select flt from Flight as flt where " + "flt.scheduleId = :scheduleId and " + "flt.status != :status ";

			list = getSession().createQuery(hql).setParameter("scheduleId", new Integer(scheduleId))
					.setParameter("status", FlightStatusEnum.CANCELLED.getCode()).list();

		} catch (Exception e) {

			throw new CommonsDataAccessException(e, null);
		}

		return list;
	}

	/**
	 * to get the flight segment ids for a given schedule segment code
	 */
	@Override
	public List<Integer> getFlightSegmentIDs(int scheduleId, String segmentCode) {

		List<Integer> list = null;

		String hql = "select fs.fltSegId from FlightSegement as fs, Flight as flt where "
				+ "flt.flightId = fs.flightId and flt.scheduleId = " + scheduleId + " and " + "fs.segmentCode = '" + segmentCode
				+ "'";

		list = find(hql, Integer.class);

		return list;
	}

	/**
	 * 
	 */
	@Override
	public Timestamp getFlightForDepartureStation(int flightId, String departureStation) {

		DataSource ds = LookUpUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		Object params[] = { new Integer(flightId), departureStation };
		final ArrayList<Date> timeList = new ArrayList<Date>();

		String sql = " select EST_TIME_DEPARTURE_ZULU from T_FLIGHT_SEGMENT "
				+ " where FLIGHT_ID=? and substr(SEGMENT_CODE,0,3)=? ";

		templete.query(sql, params, new ResultSetExtractor<Object>() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					Timestamp ts = rs.getTimestamp("EST_TIME_DEPARTURE_ZULU");
					timeList.add(ts);
				}

				return null;
			}
		});

		return (Timestamp) (timeList.get(0));
	}

	/**
	 * Get origin station IDs of the given flight number
	 */
	@Override
	public List<String> getFlightOrigins(String flightNumber) {
		DataSource ds = LookUpUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		Object params[] = { flightNumber };
		final ArrayList<String> originList = new ArrayList<String>();

		String sql = " select unique ORIGIN from T_FLIGHT " + " where FLIGHT_NUMBER=? ";

		templete.query(sql, params, new ResultSetExtractor<Object>() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					String origin = rs.getString("ORIGIN");
					originList.add(origin);
				}

				return null;
			}
		});

		return originList;
	}

	/**
	 * Returns destinations touched after a given airport by the flight
	 * 
	 * @param flightNumber
	 * @param departureDate
	 * @param airportCode
	 * @return
	 */
	@Override
	public List<String> getPossibleDestinations(String flightNumber, Date departureDate, String airportCode) {

		if (airportCode == null || flightNumber == null || departureDate == null) {
			return new ArrayList<String>();
		}

		String segmentCode = airportCode + "/%";

		DataSource ds = LookUpUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		Object params[] = { departureDate, segmentCode, flightNumber, FlightStatusEnum.CANCELLED.getCode() };
		final ArrayList<String> destinationList = new ArrayList<String>();

		String sql = "SELECT substr(fs.segment_code, length(fs.segment_code)-2) destination "
				+ "  FROM t_flight f, t_flight_segment fs " + " WHERE f.flight_id = fs.flight_id "
				+ "   AND fs.est_time_departure_local = ? " + "   AND fs.segment_code like ? " + "   AND f.flight_number = ? "
				+ "   AND f.status <> ? " + "   AND fs.SEGMENT_VALID_FLAG = 'Y' ";

		templete.query(sql, params, new ResultSetExtractor<Object>() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					String destination = rs.getString("destination");
					destinationList.add(destination);
				}

				return null;
			}
		});

		return destinationList;
	}

	/**
	 * public method to return all non-cancelled flight numbers
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<String> getFlightNumbers() {

		List<String> list = null;

		try {

			String hql = "select distinct f.flightNumber " + " from Flight as f " + " where f.status != :status ";

			list = getSession().createQuery(hql).setParameter("status", FlightStatusEnum.CANCELLED.getCode()).list();

		} catch (Exception e) {

			throw new CommonsDataAccessException(e, null);
		}

		return list;
	}

	@Override
	public Integer getFlightID(String segCode, String fltNum, Date depDateZulu) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		String dateZulu = dateFormat.format(depDateZulu);

		DataSource ds = LookUpUtils.getDatasource();

		final JdbcTemplate templete = new JdbcTemplate(ds);
		Object params[] = { fltNum, segCode, dateZulu, PNLConstants.FlightStatus.FLIGHT_CANCELED,
				PNLConstants.FightSegmentValidity.VALID_SEGMENT };

		String sql = " select f.FLIGHT_ID " + " from T_FLIGHT f, T_FLIGHT_SEGMENT fs  where "
				+ " f.flight_id = fs.flight_id and f.flight_number = ? " + " and fs.segment_code = ? "
				+ " and trunc(fs.EST_TIME_DEPARTURE_ZULU)= ? " + " and f.status != ? and fs.segment_valid_flag = ?";

		final ArrayList<Integer> list = new ArrayList<Integer>();

		templete.query(sql, params,

		new ResultSetExtractor<Object>() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				while (rs.next()) {

					int fid = rs.getInt("FLIGHT_ID");
					list.add(new Integer(fid));

				}
				return null;
			}
		});

		Integer flightId = null;
		if (list.size() > 0) {
			flightId = list.get(0);
		}
		return flightId;

	}
	
	@Override
	public Map<String, String> getFlightLegs(int flightID) {

		DataSource ds = ReservationModuleUtils.getDatasource();
		final JdbcTemplate templete = new JdbcTemplate(ds);
		Object params[] = { new Integer(flightID) };
		String sql = "select LEG_NUMBER, ORIGIN, DESTINATION from T_FLIGHT_LEG where FLIGHT_ID=?";
		final Map<String, String> legNumberWiseOndMap = new HashMap<String, String>();

		templete.query(sql, params,

		new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				while (rs.next()) {
					String legNumber = rs.getString("LEG_NUMBER");
					String origin = rs.getString("ORIGIN");
					String destination = rs.getString("DESTINATION");
					String ond = origin + "/" + destination;
					
					legNumberWiseOndMap.put(legNumber, ond);
				}
				return null;
			}
		});

		return legNumberWiseOndMap;

	}
	
	@Override
	public Integer getFlightID(String flightNumber, Date dateOfflight, String departureStation) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		String dte = dateFormat.format(dateOfflight);

		DataSource ds = ReservationModuleUtils.getDatasource();

		final JdbcTemplate templete = new JdbcTemplate(ds);
		Object params[] = { flightNumber, departureStation, dte, PNLConstants.FlightStatus.FLIGHT_CANCELED,
				PNLConstants.FightSegmentValidity.VALID_SEGMENT };

		// String sql = "select FLIGHT_ID from T_FLIGHT where FLIGHT_NUMBER=?
		// and ORIGIN=? and trunc(DEPARTURE_DATE)=?";

		String sql = " select f.FLIGHT_ID " + " from T_FLIGHT f, T_FLIGHT_SEGMENT fs " + " where "
				+ " f.flight_id = fs.flight_id and " + " f.flight_number = ? " + " and substr(fs.segment_code,1,3)=? "
				+ " and trunc(fs.EST_TIME_DEPARTURE_LOCAL)=? " + " and f.status != ? and fs.segment_valid_flag = ? "
				+ " and f.cs_oc_flight_number IS NULL AND f.cs_oc_carrier_code IS NULL";

		final ArrayList<Integer> list = new ArrayList<Integer>();

		templete.query(sql, params,

		new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				while (rs.next()) {

					int fid = rs.getInt("FLIGHT_ID");
					list.add(new Integer(fid));

				}
				return null;
			}
		});

		Integer flightId = null;
		if (list.size() > 0) {
			flightId = list.get(0);
		} else {
			flightId = new Integer(-1);
		}
		return flightId.intValue();

	}

	@Override
	public Integer getFlightID(String pnrSegId) {

		DataSource ds = LookUpUtils.getDatasource();

		final JdbcTemplate templete = new JdbcTemplate(ds);
		Object params[] = { pnrSegId };

		String sql = "SELECT fs.flight_id FROM t_flight_segment fs INNER JOIN t_pnr_segment ps ON fs.flt_seg_id=ps.flt_seg_id "
				+ "WHERE pnr_seg_id= ? ";

		final ArrayList<Integer> list = new ArrayList<Integer>();

		templete.query(sql, params,

		new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				while (rs.next()) {

					int fid = rs.getInt("FLIGHT_ID");
					list.add(new Integer(fid));

				}
				return null;
			}
		});

		Integer flightId = null;
		if (list.size() > 0) {
			flightId = list.get(0);
		}
		return flightId;

	}

	@Override
	public RMFlightSummaryDTO getFlightSummary(String fltNum, Date depDateZulu) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		String dateZulu = dateFormat.format(depDateZulu);

		DataSource ds = LookUpUtils.getDatasource();

		final JdbcTemplate templete = new JdbcTemplate(ds);
		Object params[] = { fltNum, dateZulu, PNLConstants.FlightStatus.FLIGHT_CANCELED,
				PNLConstants.FightSegmentValidity.VALID_SEGMENT };

		String sql = " select f.flight_id, f.overlap_flight_instance_id, fs.FLT_SEG_ID, fs.segment_code, f.departure_date, f.flight_number, fs.EST_TIME_DEPARTURE_LOCAL "
				+ " from T_FLIGHT f,  T_FLIGHT_SEGMENT fs  where "
				+ " f.flight_id = fs.flight_id and f.flight_number = ? "
				+ " and trunc(fs.EST_TIME_DEPARTURE_ZULU)= ? " + " and f.status != ? and fs.segment_valid_flag = ?";

		return (RMFlightSummaryDTO) templete.query(sql, params,

		new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				RMFlightSummaryDTO summaryDTO = null;
				Collection<com.isa.thinair.airschedules.api.dto.FlightSegmentDTO> connectedSegments = new ArrayList<com.isa.thinair.airschedules.api.dto.FlightSegmentDTO>();

				while (rs.next()) {

					if (summaryDTO == null) {
						summaryDTO = new RMFlightSummaryDTO(rs.getInt("flight_id"), rs.getDate("departure_date"), rs
								.getString("flight_number"));
						summaryDTO.setOverlappling_flightId(rs.getInt("overlap_flight_instance_id"));

					}

					if (summaryDTO != null) {
						com.isa.thinair.airschedules.api.dto.FlightSegmentDTO segmentDTO = new com.isa.thinair.airschedules.api.dto.FlightSegmentDTO();
						segmentDTO.setFlightSegId(rs.getInt("FLT_SEG_ID"));
						segmentDTO.setSegmentCode(rs.getString("segment_code"));
						segmentDTO.setEstTimeDepatureLocal(rs.getDate("EST_TIME_DEPARTURE_LOCAL"));

						connectedSegments.add(segmentDTO);
					}

				}
				if (summaryDTO != null) {
					summaryDTO.setConnectedSegments(connectedSegments);
				}
				return summaryDTO;
			}
		});

	}

	@Override
	public List<RouteInfoTO> getAvailableRoutes(String fromAirport, String toAirport, int availabilityRestrictionLevel) {
		String directFlag = "";
		if (availabilityRestrictionLevel == AvailableFlightSearchDTO.CONNECTED_FLIGHTS_ONLY) {
			directFlag = " and ri.direct_flag = 'N'";
		} else if (availabilityRestrictionLevel == AvailableFlightSearchDTO.SINGLE_FLIGHTS_ONLY) {
			directFlag = " and ri.direct_flag = 'Y'";
		}

		String sql = "SELECT ri.route_id, ri.from_airport, ri.to_airport, ri.ond_code, ri.direct_flag, "
				+ " ri.no_of_transits, b.transit_airport_code, tci.city_code "
				+ " FROM t_route_info ri, t_route_transits b,  t_city tci, t_airport ta "
				+ " WHERE ri.route_id = b.route_id (+) AND ri.from_airport like ? AND ri.to_airport like ? AND ri.status = 'ACT' "
				+ " AND b.transit_airport_code = ta.airport_code(+) AND ta.city_id = tci.city_id(+)"
				+ directFlag + " ORDER BY ri.route_id, ri.no_of_transits, b.seq";

		final JdbcTemplate templete = new JdbcTemplate(LookUpUtils.getDatasource());
		final Map<Long, RouteInfoTO> routesMap = new HashMap<Long, RouteInfoTO>();
		final String origin = fromAirport;
		final String destination = toAirport;
		
		PreparedStatementSetter preparedStatementSetter = new PreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement prepStatement) throws SQLException {
				prepStatement.setString(1, origin);
				prepStatement.setString(2, destination);
			}
		};
		
		templete.query(sql,preparedStatementSetter,new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					boolean isDirect = "Y".equals(rs.getString("direct_flag")) ? true : false;
					Long routeId = rs.getLong("route_id");
					String transit = rs.getString("transit_airport_code");
					String fromAirport = rs.getString("from_airport");
					String toAirport = rs.getString("to_airport");
					String ondCode = rs.getString("ond_code");
					String cityCode = rs.getString("city_code");
					fillRouteDetails(routesMap, routeId, fromAirport, toAirport, isDirect, transit, ondCode, cityCode);
				}
				return routesMap;
			}
		});

		List<RouteInfoTO> routesList = new ArrayList<RouteInfoTO>();
		if (routesMap.isEmpty() && AppSysParamsUtil.isCityBasedAvailabilityEnabled()) {
			sql = "SELECT * FROM (SELECT route_id, from_airport, to_airport, ond_code, direct_flag, no_of_transits,"
					+ " transit_airport_code, city_id, transit_city_code, COUNT (*) OVER (PARTITION BY city_id) AS cnt"
					+ " FROM (("
					+ " SELECT UNIQUE ri.route_id, ri.from_airport, ri.to_airport, ri.ond_code, ri.direct_flag, "
					+ " ri.no_of_transits, b.transit_airport_code, ci.city_id , tci.city_code AS transit_city_code "
					+ " FROM t_route_info ri, t_city ci, t_airport a, t_city tci, t_airport ta, t_route_transits b "
					+ " WHERE ri.from_airport = ? AND ri.route_id = b.route_id(+) AND ri.to_airport = a.airport_code "
					+ " AND a.city_id = ci.city_id AND a.status = 'ACT' AND ci.status = 'ACT' AND ri.status = 'ACT' " 
					+ " AND b.transit_airport_code = ta.airport_code(+) AND ta.city_id = tci.city_id(+) " + directFlag
					+ " ) UNION ("
					+ " SELECT UNIQUE ri.route_id, ri.from_airport, ri.to_airport, ri.ond_code, ri.direct_flag, "
					+ " ri.no_of_transits, b.transit_airport_code, ci.city_id, tci.city_code AS transit_city_code "
					+ " FROM t_route_info ri, t_city ci, t_airport a, t_city tci, t_airport ta, t_route_transits b "
					+ " WHERE ri.to_airport = ?  AND ri.route_id = b.route_id(+) AND ri.from_airport = a.airport_code "
					+ " AND a.city_id = ci.city_id AND a.status = 'ACT' AND ci.status = 'ACT' AND ri.status = 'ACT' "
					+ " AND b.transit_airport_code = ta.airport_code(+) AND ta.city_id = tci.city_id(+) " + directFlag 
					+ "))) WHERE cnt > 1";

			templete.query(sql, preparedStatementSetter, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					Map<Long, RouteInfoTO> originRoutesMap = new HashMap<Long, RouteInfoTO>();
					Map<Long, RouteInfoTO> destinationRoutesMap = new HashMap<Long, RouteInfoTO>();
					while (rs.next()) {
						boolean isDirect = "Y".equals(rs.getString("direct_flag")) ? true : false;
						Long routeId = rs.getLong("route_id");
						String transit = rs.getString("transit_airport_code");
						String fromAirport = rs.getString("from_airport");
						String toAirport = rs.getString("to_airport");
						String ondCode = rs.getString("ond_code");
						String transitCityCode = rs.getString("transit_city_code");
						if (fromAirport.equals(origin)) {
							fillRouteDetails(originRoutesMap, routeId, fromAirport, toAirport, isDirect, transit, ondCode, transitCityCode);
						} else {
							fillRouteDetails(destinationRoutesMap, routeId, fromAirport, toAirport, isDirect, transit, ondCode, transitCityCode);
						}
					}

					int iteration = 0;
					for (RouteInfoTO originRouteInfoTO : originRoutesMap.values()) {
						for (RouteInfoTO destinationRouteInfoTO : destinationRoutesMap.values()) {
							if (!originRouteInfoTO.getToAirportCode().equals(destinationRouteInfoTO.getFromAirportCode())
									&& !destinationRouteInfoTO.getTransitAirportCodesList().contains(
											new TransitAirport(originRouteInfoTO.getToAirportCode(), false, null))
									&& !originRouteInfoTO.getTransitAirportCodesList().contains(
											new TransitAirport(destinationRouteInfoTO.getFromAirportCode(), false, null))
									&& !CollectionUtils.containsAny(originRouteInfoTO.getTransitAirportCodesList(),
											destinationRouteInfoTO.getTransitAirportCodesList())) {
								RouteInfoTO routeInfoTO = originRouteInfoTO.clone();
								routeInfoTO.setToAirportCode(destinationRouteInfoTO.getToAirportCode());
								routeInfoTO.addTransitAirportCode(originRouteInfoTO.getToAirportCode(), false, null);
								routeInfoTO.addTransitAirportCode(destinationRouteInfoTO.getFromAirportCode(), true, null);
								routeInfoTO.getTransitAirportCodesList().addAll(
										destinationRouteInfoTO.getTransitAirportCodesList());
								routeInfoTO.setDirectRoute(false);
								routesMap.put(new Long(iteration), routeInfoTO);
								iteration++;
							}
						}
					}
					return routesMap;
				}
			});
		}
		if (routesMap.size() > 0) {
			for (RouteInfoTO routeInfoTO : routesMap.values()) {
				routesList.add(routeInfoTO);
			}
		}
		return routesList;
	}

    @Override
    public List<RouteInfoTO> getAvailableRoutes(List<String> fromAirports) {

        if(fromAirports == null || fromAirports.isEmpty()) {
            return Collections.emptyList();
        }

        final List<RouteInfoTO> routeInfoTOs = new ArrayList<>();

        String sql = "SELECT ROUTE_ID," +
                "  OND_CODE, " +
                "  DIRECT_FLAG, " +
                "  NO_OF_TRANSITS, " +
                "  FROM_AIRPORT " +
                " FROM T_ROUTE_INFO " +
                " WHERE FROM_AIRPORT IN (:aiports) " +
                " AND STATUS = 'ACT' ";

        NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(new JdbcTemplate(LookUpUtils.getDatasource()).getDataSource());

        Map<String,Object> params = new HashMap<>();

        params.put("aiports", fromAirports);

        jdbcTemplate.query(sql,params,new ResultSetExtractor() {
            @Override
            public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
                while (rs.next()) {

                    RouteInfoTO routeInfoTO = new RouteInfoTO();
                    routeInfoTO.setId(rs.getLong("ROUTE_ID"));
                    routeInfoTO.setFromAirportCode(rs.getString("FROM_AIRPORT"));
                    routeInfoTO.setOndCode(rs.getString("OND_CODE"));
                    routeInfoTO.setDirectRoute("Y".equals(rs.getString("DIRECT_FLAG")));
                    routeInfoTOs.add(routeInfoTO);

                }
                return routeInfoTOs;
            }
        });

        return routeInfoTOs;
    }


	private void fillRouteDetails(Map<Long, RouteInfoTO> routesMap, Long routeId, String fromAirport, String toAirport,
			boolean isDirect, String transit, String ondCode, String transitCityCode) {
		RouteInfoTO routeInfoTO;
		if (routesMap.containsKey(routeId)) {
			routeInfoTO = routesMap.get(routeId);
		} else {
			routeInfoTO = new RouteInfoTO();
			routeInfoTO.setId(routeId);
			routeInfoTO.setOndCode(ondCode);
			routeInfoTO.setFromAirportCode(fromAirport);
			routeInfoTO.setToAirportCode(toAirport);
			routeInfoTO.setDirectRoute(isDirect);
			routesMap.put(routeId, routeInfoTO);
		}
		if (transit != null) {
			routeInfoTO.addTransitAirportCode(transit, true, transitCityCode);
		}
	}

	// Added by Haider 03Sep08
	@Override
	public HashMap<Integer, ArrayList<Integer>> getScheduleGDSIdsForFlight(List<Integer> flightIds) {
		String s = "";
		if (flightIds != null) {
			Object[] ids = flightIds.toArray();
			for (int i = 0; i < ids.length; i++) {
				s += ids[i];
				if (i < ids.length - 1) {
					s += ",";
				}
			}
		}
		String sql = "select gds.gds_id, f.flight_id from T_FLIGHT_SCHEDULE_GDS gds"
				+ " left join (T_FLIGHT_SCHEDULE sch left join T_FLIGHT f on sch.SCHEDULE_ID=f.schedule_id)"
				+ " on sch.SCHEDULE_ID=gds.schedule_id where f.flight_id in (" + s + ")";

		DataSource ds = LookUpUtils.getDatasource();

		final JdbcTemplate templete = new JdbcTemplate(ds);

		final HashMap<Integer, ArrayList<Integer>> gdsIdsMap = new HashMap<Integer, ArrayList<Integer>>();
		templete.query(sql,

		new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					Integer gdsId = rs.getInt("GDS_ID");
					Integer flightId = rs.getInt("FLIGHT_ID");
					ArrayList<Integer> gdsIds = null;
					if (gdsIdsMap.containsKey(flightId)) {
						gdsIds = gdsIdsMap.get(flightId);
					} else {
						gdsIds = new ArrayList<Integer>();
						gdsIdsMap.put(flightId, gdsIds);
					}
					gdsIds.add(gdsId);
				}
				return gdsIdsMap;
			}
		});
		/*
		 * if (gdsIdsMap.size() > 0){ for (Iterator gdsIdsMapIt = gdsIdsMap.keySet().iterator(); gdsIdsMapIt.hasNext();
		 * ){ Integer fid = (Integer)gdsIdsMapIt.next(); System.out.println(fid+" = "+gdsIdsMap.get(fid)); } }
		 */
		return gdsIdsMap;

	}

	@Override
	public boolean isSeatMapAttached(List<Integer> flightIds) {
		String s = "";

		if (flightIds != null) {
			Object[] ids = flightIds.toArray();
			for (int i = 0; i < ids.length; i++) {
				s += ids[i];
				if (i < ids.length - 1) {
					s += ",";
				}
			}
		}
		String sql = "select distinct(flt_seg_id) from sm_t_flight_am_seat " + "	where flt_seg_id in ( "
				+ "   select flt_seg_id from t_flight_segment  where flight_id in (" + s + ")) "
				+ " 	and ams_charge_id is not null ";

		DataSource ds = LookUpUtils.getDatasource();

		JdbcTemplate templete = new JdbcTemplate(ds);

		final List<Integer> lstSegs = new ArrayList<Integer>();
		templete.query(sql,

		new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					lstSegs.add(rs.getInt("flt_seg_id"));
				}
				return lstSegs;
			}
		});

		if (lstSegs.size() > 0) {
			return true;
		}

		return false;
	}

	@Override
	public boolean isSeatMapSeatsUsed(List<Integer> flightIds) {
		String s = "";

		if (flightIds != null) {
			Object[] ids = flightIds.toArray();
			for (int i = 0; i < ids.length; i++) {
				s += ids[i];
				if (i < ids.length - 1) {
					s += ",";
				}
			}
		}
		String sql = "select distinct(flt_seg_id) from sm_t_flight_am_seat " + "	where flt_seg_id in ( "
				+ "   select flt_seg_id from t_flight_segment  where flight_id in (" + s + ")) "
				+ " 	and ams_charge_id is not null and status='RES' ";

		DataSource ds = LookUpUtils.getDatasource();

		JdbcTemplate templete = new JdbcTemplate(ds);

		final List<Integer> lstSegs = new ArrayList<Integer>();
		templete.query(sql,

		new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					lstSegs.add(rs.getInt("flt_seg_id"));
				}
				return lstSegs;
			}
		});

		if (lstSegs.size() > 0) {
			return true;
		}

		return false;
	}

	@Override
	public void updateFlightMealNotifyStatus(FlightMealNotifyStatusDTO flightMealNotifyStatusDTO) {
		try {
			if (flightMealNotifyStatusDTO != null && flightMealNotifyStatusDTO.getFlightIds() != null) {
				String hqlUpdate = "update Flight " + " set mealNotificationStatus =:status, "
						+ " mealNotificationAttempts = mealNotificationAttempts + 1 " + " where flightId in (:flightIds)";

				getSession().createQuery(hqlUpdate).setParameter("status", flightMealNotifyStatusDTO.getStatus())
						.setParameterList("flightIds", flightMealNotifyStatusDTO.getFlightIds()).executeUpdate();
			}
		} catch (Exception e) {
			throw new CommonsDataAccessException(e, null);
		}
	}

	@Override
	public void updateFlightSegmentCheckinStatus(int flightId, String segmentCode, String status) {
		try {
			String hqlUpdate = "update FlightSegement set checkinStatus =:status where flightId =:flightId and segmentCode like :segmentCode)";

			getSession().createQuery(hqlUpdate).setParameter("flightId", flightId).setParameter("status", status)
					.setParameter("segmentCode", segmentCode).executeUpdate();

		} catch (Exception e) {
			throw new CommonsDataAccessException(e, null);
		}
	}

	@Override
	public void updateFlightSegmentTerminals(String airport, Integer terminalId, boolean isNull, boolean isOnlySet) {
		try {
			String where = "";
			if (isOnlySet) {
				where = " and arrivalTerminalId ='" + terminalId + "'";
			}
			if (isNull) {
				where = " and arrivalTerminalId is null";
			}

			String hqlUpdate = "update FlightSegement set arrivalTerminalId =" + terminalId
					+ " where segmentCode like :segmentCode " + where + ")";
			getSession().createQuery(hqlUpdate).setParameter("segmentCode", "%/" + airport).executeUpdate();

			if (isOnlySet) {
				where = " and departureTerminalId ='" + terminalId + "'";
			}

			if (isNull) {
				where = " and departureTerminalId is null";
			}

			hqlUpdate = "update FlightSegement set departureTerminalId =" + terminalId + " where segmentCode like :segmentCode "
					+ where + ")";

			getSession().createQuery(hqlUpdate).setParameter("segmentCode", airport + "/%").executeUpdate();

		} catch (Exception e) {
			throw new CommonsDataAccessException(e, null);
		}
	}

	private String getHql(FlightFlightSegmentSearchCriteria criteria) {

		String hql = " select flight from Flight flight, FlightSegement flightSegment ";
		String hqlWhere = " where flight.flightId = flightSegment.flightId ";
		String hqlOrderby = " order by flight.flightNumber, flightSegment.estTimeDepatureZulu asc ";

		if (criteria.getFlightNumber() != null) {
			hqlWhere += " and flight.flightNumber = :flightNumber ";
		}

		if (criteria.getEstStartTimeZulu() != null) {
			hqlWhere += " and flightSegment.estTimeDepatureZulu >= :estStartTimeZulu ";
		}

		if (criteria.getEstEndTimeZulu() != null) {
			hqlWhere += " and flightSegment.estTimeDepatureZulu <= :estEndTimeZulu ";
		}

		if (criteria.getSegmentCode() != null) {
			hqlWhere += " and flightSegment.segmentCode like :segmentCode ";
		}

		if (criteria.getCheckinStatus() != null) {
			hqlWhere += " and flightSegment.checkinStatus = :checkinStatus ";
		}

		if (criteria.getStatus() != null) {
			hqlWhere += " and flight.status = :status ";
		}

		hql += hqlWhere + hqlOrderby;

		return hql;
	}

	@SuppressWarnings("unchecked")
	@Override
	public FlightSegement getFlightSegmentForAirport(int flightId, String segmentCode) {

		FlightSegement segment = null;

		try {
			String hql = " select flightSegment from Flight flight, FlightSegement flightSegment where flight.flightId = flightSegment.flightId "
					+ "and flight.flightId =:flightId and flightSegment.segmentCode like :segmentCode";
			List<FlightSegement> list = getSession().createQuery(hql).setParameter("flightId", flightId)
					.setParameter("segmentCode", segmentCode).list();
			if (list != null) {
				segment = list.get(0);
			}

		} catch (Exception e) {
			throw new CommonsDataAccessException(e, null);
		}
		return segment;
	}

	@Override
	public int getFlightStandbyCount(int flightId, String airport) {

		String sql = "SELECT count(bc.bc_type) count " + " FROM T_PNR_SEGMENT ps, T_FLIGHT_SEGMENT fs, T_PNR_PAX_FARE ppf, "
				+ " T_PNR_PAX_FARE_SEGMENT ppfs, T_BOOKING_CLASS bc " + " WHERE ps.flt_seg_id = fs.flt_seg_id "
				+ " AND ps.pnr_seg_id = ppfs.pnr_seg_id " + " AND ppfs.booking_code = bc.booking_code "
				+ " AND ppfs.ppf_id = ppf.ppf_id " + " AND fs.flight_id = " + flightId + " AND fs.segment_code like '" + airport
				+ "' " + " AND ps.status <> 'CNX' " + " and bc.bc_type = '" + BookingClass.BookingClassType.STANDBY + "' "
				+ " AND (BC.bc_type is null OR BC.bc_type <> 'CNX') " + " GROUP BY  bc.bc_type ";

		JdbcTemplate jt = new JdbcTemplate(getDataSource());
		Object count = jt.query(sql, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				int flightSegId = 0;
				if (rs != null) {
					while (rs.next()) {
						flightSegId = rs.getInt("count");
					}
				}
				return new Integer(flightSegId);
			}
		});

		if (count != null) {
			return ((Integer) count).intValue();
		}
		return 0;
	}

	@Override
	public int getFlightCheckinCount(int flightId, String airport) {

		String sql = "SELECT count(unique pp.pnr_pax_id) cnt " + "FROM T_PNR_SEGMENT ps, T_FLIGHT_SEGMENT fs, "
				+ "T_PNR_PAX_FARE_SEGMENT ppfs, T_PNR_PAX_FARE ppf , t_pnr_passenger pp "
				+ "WHERE ps.flt_seg_id = fs.flt_seg_id " + "AND ps.pnr_seg_id = ppfs.pnr_seg_id "
				+ "AND ppfs.ppf_id = ppf.ppf_id " + "AND fs.flight_id = " + flightId + " AND fs.segment_code like '" + airport
				+ "' " + "AND ps.status <> 'CNX' " + "AND ppfs.checkin_status = '"
				+ ReservationInternalConstants.PassengerCheckinStatus.CHECKED_IN + "' " + "and ppf.pnr_pax_id = pp.pnr_pax_id "
				+ "and pp.pax_type_code <> '" + ReservationInternalConstants.PassengerType.INFANT + "'";

		JdbcTemplate jt = new JdbcTemplate(getDataSource());
		Object count = jt.query(sql, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				int flightSegId = 0;
				if (rs != null) {
					while (rs.next()) {
						flightSegId = rs.getInt("cnt");
					}
				}
				return new Integer(flightSegId);
			}
		});

		if (count != null) {
			return ((Integer) count).intValue();
		}
		return 0;
	}

	/**
	 * get Number of flights flights FIXME: NEED TO OPTIMIZE THIS INTO USING A SINGLE QUERY
	 */
	@Override
	public Integer[] getNumberOfFlightsInSchedule(Integer scheduleId) {

		int noOfActFlights = 0;
		int noOfInaFlights = 0;
		int noOfCnxFlights = 0;
		try {

			noOfActFlights = ((Long) getSession().createQuery(
					"select count(*) from Flight where scheduleId = ? and status != ?").setParameter(0, scheduleId)
					.setParameter(1, FlightStatusEnum.ACTIVE.getCode()).uniqueResult()).intValue();

			noOfInaFlights = ((Long) getSession().createQuery(
					"select count(*) from Flight where scheduleId = ? and status != ?").setParameter(0, scheduleId)
					.setParameter(1, FlightStatusEnum.CLOSED.getCode()).uniqueResult()).intValue();

			noOfCnxFlights = ((Long) getSession().createQuery(
					"select count(*) from Flight where scheduleId = ? and status != ?").setParameter(0, scheduleId)
					.setParameter(1, FlightStatusEnum.CANCELLED.getCode()).uniqueResult()).intValue();

		} catch (Exception e) {
			throw new CommonsDataAccessException(e, null);
		}

		return new Integer[] { noOfActFlights, noOfInaFlights, noOfCnxFlights };
	}

	/**
	 * Load number of open flights
	 * 
	 * @param scheduleIds
	 * @return
	 */
	@Override
	public HashMap<Integer, Integer> getOpenFlightCount(List<Integer> scheduleIds) {
		final HashMap<Integer, Integer> flightDetailMap = new HashMap<Integer, Integer>();

		DataSource ds = LookUpUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);
		String inValues = "";
		if (scheduleIds != null && scheduleIds.size() > 0) {

			Iterator<Integer> itShcedIds = scheduleIds.iterator();
			while (itShcedIds.hasNext()) {
				Integer schedId = itShcedIds.next();
				inValues += "," + schedId;
			}
			inValues = inValues.substring(1);

			String sql = "select F.schedule_id, count(f.flight_id) as count_flt from t_flight f "
					+ "where f.departure_date > sysdate and  f.status='ACT' and f.schedule_id in(" + inValues + ") "
					+ "GROUP BY F.schedule_id";

			templete.query(sql, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					while (rs.next()) {
						Integer id = rs.getInt("schedule_id");
						Integer strCount = rs.getInt("count_flt");
						flightDetailMap.put(id, strCount);
					}

					return null;
				}
			});
		}
		return flightDetailMap;
	}

	/**
	 * Load number of open flights FIXME: rename
	 * 
	 * @param scheduleIds
	 * @return
	 */
	@Override
	public HashMap<Integer, Integer> hasFutureFlights(List<Integer> scheduleIds) {
		final HashMap<Integer, Integer> flightDetailMap = new HashMap<Integer, Integer>();

		DataSource ds = LookUpUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);
		String inValues = "";
		if (scheduleIds != null && scheduleIds.size() > 0) {

			Iterator<Integer> itShcedIds = scheduleIds.iterator();
			while (itShcedIds.hasNext()) {
				Integer schedId = itShcedIds.next();
				inValues += "," + schedId;
			}
			inValues = inValues.substring(1);

			String sql = "select F.schedule_id, count(f.flight_id) as count_flt from t_flight f "
					+ "where f.departure_date > sysdate and f.schedule_id in(" + inValues + ") " + "GROUP BY F.schedule_id";

			templete.query(sql, new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					while (rs.next()) {
						Integer id = rs.getInt("schedule_id");
						Integer strCount = rs.getInt("count_flt");
						flightDetailMap.put(id, strCount);
					}

					return null;
				}
			});
		}
		return flightDetailMap;
	}

	/**
	 * get Flights to GDS Publish
	 */

	@Override
	@SuppressWarnings("unchecked")
	public Collection<Flight> getFlightsToGDSPublish(Date lastPublishTime, String codeShareCarrier) {
		Collection<Flight> resultList = null;
		DataSource ds = LookUpUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		if (lastPublishTime != null) {
			String sql = "select (select count(*) from t_flight where modified_date >= ?) + (select count(*) from t_flight_schedule where modified_date >= ?) as total from dual";
			String hql = "select flt from Flight as flt " + "where flt.departureDate >= sysdate " + "and flt.status = '"
					+ FlightStatusEnum.ACTIVE.getCode() + "' " + "and ((flt.scheduleId = " + null + ") or (flt.scheduleId != "
					+ null + " and flt.manuallyChanged = 'Y'))";

			if (!codeShareCarrier.equals("ALL")) {
				sql = "select (select count(*) from t_flight f, t_cs_flight csf where f.flight_id = csf.flight_id and f.modified_date >= ? ) +"
						+ " (select count(*) from t_flight_schedule fs, t_cs_flight_schedule csfs where fs.schedule_id = csfs.schedule_id and fs.modified_date >= ?) as total from dual";
				hql = "select flt from Flight as flt " + "where flt.departureDate >= sysdate " + "and flt.status = '"
						+ FlightStatusEnum.ACTIVE.getCode() + "' " + "and ((flt.scheduleId = " + null
						+ ") or (flt.scheduleId != " + null
						+ " and flt.manuallyChanged = 'Y')) and flt.codeShareMCFlights.csMCCarrierCode = '" + codeShareCarrier
						+ "'";
			}

			Object[] params = { lastPublishTime, lastPublishTime };
			Integer count = 0;
			try {
				count = (Integer) templete.query(sql, params, new ResultSetExtractor() {
					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						int total = 0;
						while (rs.next()) {
							total = rs.getInt("total");
							break;
						}
						return total;
					}
				});
				if (count > 0) {
					resultList = getSession().createQuery(hql).list();
				}
			} catch (Exception e) {
				throw new CommonsDataAccessException(e, null);
			}
		}
		return resultList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Flight> getFlightsToPublish(Date lastPublishTime) {
		Collection<Flight> resultList = null;
		Collection<Flight> scheduledFlight = null;
		DataSource ds = LookUpUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		String manuallyChanged = "";

		if (lastPublishTime == null) {
			GregorianCalendar gc = new GregorianCalendar();
			gc.set(2000, 0, 1);
			lastPublishTime = gc.getTime();
		} else {
			manuallyChanged = " and flt.manuallyChanged = 'Y'";
		}

		if (lastPublishTime != null) {
			String sql = "select (select count(*) from t_flight where modified_date >= ?) + (select count(*) from t_flight_schedule where modified_date >= ?) as total from dual";
			String hql = "select flt from Flight as flt where flt.modifiedDate >= :lastPubDate and flt.status = '"
					+ FlightStatusEnum.ACTIVE.getCode() + "' and ((flt.scheduleId = " + null + ") or (flt.scheduleId != " + null
					+ manuallyChanged + "))";

			/**
			 * The query to newly created schedule flights
			 */
			String scheduledFlightsSQL = "SELECT * FROM t_flight WHERE ROWID IN ( SELECT MAX(ROWID) FROM t_flight f where "
					+ " f.schedule_id is not null and f.created_date >=:lastPubDate  and manually_changed = 'N' GROUP BY f.flight_number )";

			Object[] params = { lastPublishTime, lastPublishTime };
			Integer count = 0;
			try {
				count = (Integer) templete.query(sql, params, new ResultSetExtractor() {
					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						int total = 0;
						while (rs.next()) {
							total = rs.getInt("total");
							break;
						}
						return total;
					}
				});
				if (count > 0) {
					resultList = (getSession().createQuery(hql).setTimestamp("lastPubDate", lastPublishTime)).list();

					scheduledFlight = (getSession().createSQLQuery(scheduledFlightsSQL).addEntity(Flight.class).setTimestamp(
							"lastPubDate", lastPublishTime)).list();
					resultList.addAll(scheduledFlight);

				}
			} catch (Exception e) {
				throw new CommonsDataAccessException(e, null);
			}
		}
		return resultList;
	}

	@Override
	public List<FlightSegmentDTO> getMatchingFlightSegmentsForOndGeneration(String origin, String destination, Date startDate,
			Date endDate) {

		StringBuilder sql = new StringBuilder();
		sql.append("SELECT ");
		sql.append(" DISTINCT fltseg.FLT_SEG_ID, fltseg.FLIGHT_ID, fltseg.SEGMENT_CODE, fltseg.EST_TIME_DEPARTURE_LOCAL, ");
		sql.append("fltseg.EST_TIME_ARRIVAL_LOCAL, fltseg.EST_TIME_DEPARTURE_ZULU, fltseg.EST_TIME_ARRIVAL_ZULU, ");
		sql.append("fccseg.AVAILABLE_SEATS, fccseg.AVAILABLE_INFANT_SEATS, ");
		sql.append("flt.ORIGIN, flt.DESTINATION, flt.FLIGHT_NUMBER, flt.operation_type_id, flt.DEPARTURE_DATE ");
		sql.append("FROM ");
		sql.append("T_FLIGHT flt, T_FLIGHT_SEGMENT fltseg,T_FCC_SEG_ALLOC fccseg, T_FCC_SEG_BC_ALLOC fccsegbc ");
		sql.append("WHERE ");
		sql.append("fltseg.FLIGHT_ID = flt.FLIGHT_ID and ");
		sql.append("fccseg.SEGMENT_CODE = fltseg.SEGMENT_CODE and ");
		sql.append("fccseg.FLIGHT_ID = fltseg.FLIGHT_ID and ");
		sql.append("fltseg.SEGMENT_VALID_FLAG = 'Y' and ");
		sql.append("fltseg.STATUS = ? and ");
		sql.append("flt.STATUS = ? and ");
		sql.append("fccseg.AVAILABLE_SEATS >= 0 and ");
		sql.append("fltseg.EST_TIME_DEPARTURE_LOCAL >= ? and ");
		sql.append("fltseg.EST_TIME_DEPARTURE_LOCAL <= ? and ");
		sql.append("fltseg.SEGMENT_CODE like ? and ");
		sql.append("flt.FLIGHT_ID = fccsegbc.FLIGHT_ID and ");
		sql.append("fccsegbc.BOOKING_CODE in (");
		sql.append("select tfare.BOOKING_CODE from T_OND_FARE tfare where ");
		sql.append("fltseg.EST_TIME_DEPARTURE_ZULU between tfare.EFFECTIVE_FROM_DATE  and TFARE.EFFECTIVE_TO_DATE and ");
		sql.append("tfare.OND_CODE = fccseg.SEGMENT_CODE and ");
		sql.append("SYSDATE between tfare.SALES_VALID_FROM and tfare.SALES_VALID_TO) ");
		sql.append(getFlightNoSQLWithoutInterline());
		sql.append("order by flt.DEPARTURE_DATE");

		String segmentCode = origin + "/%" + destination;

		Object[] params = { FlightSegmentStatusEnum.OPEN.getCode(), FlightStatusEnum.ACTIVE.getCode(), startDate, endDate,
				segmentCode };

		JdbcTemplate jt = new JdbcTemplate(getDataSource());
		@SuppressWarnings("unchecked")
		List<FlightSegmentDTO> list = (List<FlightSegmentDTO>) jt.query(sql.toString(), params, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				FlightSegmentDTO flightSegmentDTO;
				List<FlightSegmentDTO> segments = new ArrayList<FlightSegmentDTO>();

				if (rs != null) {
					while (rs.next()) {

						flightSegmentDTO = new FlightSegmentDTO();

						flightSegmentDTO.setFlightId(new Integer(rs.getInt("FLIGHT_ID")));
						flightSegmentDTO.setArrivalDateTime(rs.getTimestamp("EST_TIME_ARRIVAL_LOCAL"));
						flightSegmentDTO.setDepartureDateTime(rs.getTimestamp("EST_TIME_DEPARTURE_LOCAL"));
						flightSegmentDTO.setArrivalDateTimeZulu(rs.getTimestamp("EST_TIME_ARRIVAL_ZULU"));
						flightSegmentDTO.setDepartureDateTimeZulu(rs.getTimestamp("EST_TIME_DEPARTURE_ZULU"));
						flightSegmentDTO.setSegmentCode(rs.getString("SEGMENT_CODE"));
						flightSegmentDTO.setFromAirport(rs.getString("ORIGIN"));
						flightSegmentDTO.setToAirport(rs.getString("DESTINATION"));
						flightSegmentDTO.setFlightNumber(rs.getString("FLIGHT_NUMBER"));
						flightSegmentDTO.setSegmentId(new Integer(rs.getInt("FLT_SEG_ID")));
						flightSegmentDTO.setOperationTypeID(new Integer(rs.getInt("operation_type_id")));
						segments.add(flightSegmentDTO);
					}
				}

				return segments;
			}
		});

		return list;
	}

	/**
	 * get matching booking class for RM integration for given adult fare and flight details
	 */

	@Override
	public List<String> getMatchingBookingClassForRM(String segmentCode, Date flightDepDate, Double adultFareAmount,
			String fareType) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		String depDateZulu = dateFormat.format(flightDepDate);

		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT unique BC.NEST_RANK,BC.BOOKING_CODE ");
		sql.append(" FROM T_OND_FARE ODF, T_FARE_VISIBILITY FV, T_BOOKING_CLASS BC ");
		sql.append(" WHERE ODF.STATUS = 'ACT' ");
		sql.append(" AND FV.FARE_RULE_ID = ODF.FARE_RULE_ID  ");
		sql.append(" AND FV.SALES_CHANNEL_CODE IN (1, 4) ");
		sql.append(" AND ODF.BOOKING_CODE = BC.BOOKING_CODE ");
		sql.append(" AND BC.STANDARD_CODE = 'Y' ");
		if (fareType.equals(BookingClass.AllocationType.CONNECTION)) {
			sql.append(" AND ODF.OND_CODE like '%" + segmentCode + "%' ");
			sql.append(" AND BC.ALLOCATION_TYPE = '" + fareType + "' ");
		} else {
			sql.append(" AND ODF.OND_CODE = '" + segmentCode + "' ");
			sql.append(" AND BC.ALLOCATION_TYPE = '" + fareType + "' " + " AND ODF.FARE_AMOUNT = " + adultFareAmount);
		}
		sql.append(" AND ( '" + depDateZulu + "'  BETWEEN ODF.SALES_VALID_FROM AND ODF.SALES_VALID_TO) ");
		sql.append(" AND ( '" + depDateZulu + "' BETWEEN ODF.EFFECTIVE_FROM_DATE AND ODF.EFFECTIVE_TO_DATE) ");
		sql.append(" ORDER BY BC.NEST_RANK ");

		JdbcTemplate jt = new JdbcTemplate(getDataSource());
		@SuppressWarnings("unchecked")
		List<String> list = (List<String>) jt.query(sql.toString(), new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<String> bcToNRList = new ArrayList<String>();
				while (rs.next()) {
					String bc = rs.getString("BOOKING_CODE");
					bcToNRList.add(bc);
				}
				return bcToNRList;
			}
		});
		return list;
	}

	@Override
	public List<AvailableFlightRollForwardDTO> availableFlightSegments(RollForwardFlightSearchCriteria searchCriteria) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT DISTINCT (CASE WHEN bc.fixed_flag = ? THEN fccseg.available_seats WHEN bc.fixed_flag = ? THEN ");
		sql.append(" (fccseg.available_seats - (SELECT NVL(SUM(inner_fccbc.available_seats), 0) FROM t_fcc_seg_bc_alloc inner_fccbc, ");
		sql.append(" t_booking_class inner_bc WHERE inner_fccbc.fccsa_id=fccseg.fccsa_id AND inner_bc.booking_code = inner_fccbc.booking_code ");
		sql.append(" AND inner_bc.fixed_flag = ? AND inner_bc.bc_type NOT  IN (?) )) END )  ADULT_SEAT_SEG , ");
		sql.append(" fccseg.available_infant_seats INFANT_SEAT_SEG, '" + searchCriteria.getReturnFlag() + "' return_flag, ");
		sql.append(" fccbc.available_seats ADULT_SEAT_BC, bc.cabin_class_code, fccbc.booking_code, ondf.fare_id, ondf.fare_rule_id, "
				+ searchCriteria.getSegmentSeq()
				+ " segment_seq, fs.*, f.flight_type flight_type, fccbc.status BC_ALLOC_STATUS, fccbc.status_chg_action status_chg_action ");
		sql.append(" FROM t_flight f, t_flight_segment fs, t_fare_rule fr, t_ond_fare ondf, t_fcc_seg_alloc fccseg, t_fcc_seg_bc_alloc fccbc, t_booking_class bc,t_fare_agent fa ");
		sql.append(" WHERE f.schedule_id IN (SELECT innerf.schedule_id FROM t_flight innerf, t_flight_segment innerfs WHERE innerf.flight_id= innerfs.flight_id AND flt_seg_id=?) ");
		sql.append(" AND f.flight_id=fs.flight_id AND fccseg.flight_id=f.flight_id AND fccbc.flight_id=f.flight_id ");
		sql.append(" AND fccbc.booking_code = bc.booking_code AND fccbc.booking_code= ondf.booking_code AND ondf.fare_rule_id = fr.fare_rule_id AND ondf.fare_id=? ");
		sql.append(" AND fr.fare_rule_id = fa.fare_rule_id (+) ");
		sql.append(" AND fr.fare_rule_id=? AND fccseg.flight_id= f.flight_id ");
		sql.append(" AND fccseg.fccsa_id=fccbc.fccsa_id AND fccseg.logical_cabin_class_code=? ");
		sql.append(" AND fccbc.booking_code=bc.booking_code ");
		sql.append(" AND fs.SEGMENT_VALID_FLAG='Y' AND f.STATUS ='ACT' AND fs.segment_code=? AND fccseg.segment_code=? AND fccbc.segment_code=? ");
		sql.append(" AND fs.EST_TIME_DEPARTURE_LOCAL >= to_date(?, 'MON DD HH24:MI:SS YYYY') ");
		sql.append(" AND fs.EST_TIME_DEPARTURE_LOCAL <= to_date(?, 'MON DD HH24:MI:SS YYYY') ");
		sql.append(" AND ondf.sales_valid_from<= sysdate AND ondf.sales_valid_to>= sysdate ");

		if (searchCriteria.isOpenRerurn()) {
			sql.append(" AND ondf.rt_effective_from_date <= fs.EST_TIME_DEPARTURE_LOCAL ");
			sql.append(" AND ondf.rt_effective_to_date >= fs.EST_TIME_DEPARTURE_LOCAL ");
		} else if (ReservationInternalConstants.ReturnTypes.RETURN_FLAG_FALSE.equals(searchCriteria.getReturnFlag())) {
			sql.append(" AND ondf.effective_from_date <= fs.EST_TIME_DEPARTURE_LOCAL ");
			sql.append(" AND ondf.effective_to_date >= fs.EST_TIME_DEPARTURE_LOCAL ");
		} else {
			sql.append(" AND ((fr.return_flag = 'N' AND ondf.EFFECTIVE_FROM_DATE <= fs.EST_TIME_DEPARTURE_LOCAL AND ondf.EFFECTIVE_TO_DATE >= fs.EST_TIME_DEPARTURE_LOCAL) ");
			sql.append(" OR (fr.return_flag = 'Y' AND ondf.RT_EFFECTIVE_FROM_DATE <= fs.EST_TIME_DEPARTURE_LOCAL AND ondf.RT_EFFECTIVE_TO_DATE >= fs.EST_TIME_DEPARTURE_LOCAL)) ");
		}

		sql.append(" AND (fa.agent_code like "
				+ ((searchCriteria.getAgentCode() == null || searchCriteria.getAgentCode().equals("")) ? "'%'" : "'"
						+ searchCriteria.getAgentCode() + "' ") + " OR fa.agent_code is null) ");

		DataSource ds = LookUpUtils.getDatasource();
		final JdbcTemplate jt = new JdbcTemplate(ds);

		Object[] params = { BookingClass.FIXED_FLAG_Y, BookingClass.FIXED_FLAG_N, BookingClass.FIXED_FLAG_Y,
				Util.buildStringInClauseContent(BookingClassUtil.getBcTypes(true)), searchCriteria.getFlightSegId(),
				searchCriteria.getFareId(), searchCriteria.getFareRuleId(), searchCriteria.getCabinClassCode(),
				searchCriteria.getSegmentCode(), searchCriteria.getSegmentCode(), searchCriteria.getSegmentCode(),
				searchCriteria.getStartDate(), searchCriteria.getEndDate() };

		@SuppressWarnings("unchecked")
		List<AvailableFlightRollForwardDTO> list = (List<AvailableFlightRollForwardDTO>) jt.query(sql.toString(), params,
				new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						List<AvailableFlightRollForwardDTO> tempList = new ArrayList<AvailableFlightRollForwardDTO>();
						while (rs.next()) {
							AvailableFlightRollForwardDTO dto = new AvailableFlightRollForwardDTO();
							dto.setAvailableAdultSeatForSeg(rs.getInt("ADULT_SEAT_SEG"));
							dto.setAvailableInfantSeatForSeg(rs.getInt("INFANT_SEAT_SEG"));
							dto.setAvailableAdultSeatsForBC(rs.getInt("ADULT_SEAT_BC"));
							dto.setBookingClasscode(rs.getString("BOOKING_CODE"));
							dto.setCabinClassCode(rs.getString("CABIN_CLASS_CODE"));
							dto.setFareId(rs.getInt("FARE_ID"));
							dto.setFareRuleId(rs.getInt("FARE_RULE_ID"));
							dto.setReturnflag(rs.getString("RETURN_FLAG"));
							dto.setSegmentSeq(rs.getInt("SEGMENT_SEQ"));
							dto.setBcAllocStatus(rs.getString("BC_ALLOC_STATUS"));
							dto.setStatusChgAction(rs.getString("STATUS_CHG_ACTION"));
							FlightSegmentDTO fltSegDTO = new FlightSegmentDTO();
							fltSegDTO.setFlightId(rs.getInt("FLIGHT_ID"));
							fltSegDTO.setSegmentCode(rs.getString("SEGMENT_CODE"));
							fltSegDTO.setSegmentId(rs.getInt("FLT_SEG_ID"));
							fltSegDTO.setFlightStatus(rs.getString("STATUS"));
							fltSegDTO.setDepartureDateTime(rs.getTimestamp("EST_TIME_DEPARTURE_LOCAL"));
							fltSegDTO.setDepartureDateTimeZulu(rs.getTimestamp("EST_TIME_DEPARTURE_ZULU"));

							String flightType = rs.getString("FLIGHT_TYPE");
							if (FlightUtil.FLIGHT_TYPE_DOM.equals(flightType)) {
								fltSegDTO.setDomesticFlight(true);
							}

							dto.setFlightSegmentDTO(fltSegDTO);

							tempList.add(dto);
						}
						return tempList;
					}
				});

		return list;
	}

	@Override
	public List<AvailableFlightSegment> getMatchingFlightSegmentsWithInventoryDetails(Date minDate, Date maxDate, String origin,
			String destination, int adults, int infants, String ccCode, int availabilityRestriction,
			boolean includeOnlyMarketingCarrierSegs, boolean isOnlyInterline, boolean isInterline,
			Collection<String> externalBookingClasses, Collection<Integer> colFlightSegIds, Date arrMinDate, Date arrMaxDate) {

		// if no external booking classes for interline search then no further
		// search.
		if (isOnlyInterline && (externalBookingClasses == null || externalBookingClasses.size() == 0)) {
			return new ArrayList<AvailableFlightSegment>();
		}

		StringBuilder sql = new StringBuilder();
		sql.append("SELECT ");
		sql.append(" DISTINCT fltseg.FLT_SEG_ID, fltseg.FLIGHT_ID, fltseg.SEGMENT_CODE, fltseg.EST_TIME_DEPARTURE_LOCAL, ");
		sql.append("fltseg.EST_TIME_ARRIVAL_LOCAL, fltseg.EST_TIME_DEPARTURE_ZULU, fltseg.EST_TIME_ARRIVAL_ZULU, ");
		sql.append("fccseg.FCCSA_ID,fccseg.SEGMENT_CODE, fccseg.ALLOCATED_SEATS, fccseg.SOLD_SEATS,fccseg.ON_HOLD_SEATS, ");
		sql.append("fccseg.INFANT_ALLOCATION, fccseg.SOLD_INFANT_SEATS,fccseg.ONHOLD_INFANT_SEATS, fccseg.AVAILABLE_SEATS, fccseg.AVAILABLE_INFANT_SEATS,fccseg.CABIN_CLASS_CODE, ");
		sql.append("flt.ORIGIN, flt.DESTINATION, flt.FLIGHT_NUMBER, flt.operation_type_id, flt.DEPARTURE_DATE, flt.FLIGHT_TYPE, ");

		sql.append(" depterminal.terminal_shortname AS DEPARTURE_TERMINAL_NAME, ");
		sql.append(" arrterminal.terminal_shortname AS ARRIVAL_TERMINAL_NAME, ");
		sql.append(" fltmod.MODEL_NUMBER,fltmod.ITIN_DESCRIPTION,flt.REMARKS, flt.SCHEDULE_ID,fltseg.STATUS ");

		sql.append("FROM ");
		sql.append("T_FLIGHT flt,T_AIRCRAFT_MODEL fltmod,T_FLIGHT_SEGMENT fltseg ");

		sql.append(" LEFT OUTER JOIN t_airport_terminal arrterminal ");
		sql.append(" ON (fltseg.arrival_terminal_id = arrterminal.airport_terminal_id) ");
		sql.append(" LEFT OUTER JOIN t_airport_terminal depterminal ");
		sql.append(" ON (fltseg.departure_terminal_id = depterminal.airport_terminal_id), ");

		sql.append(" T_FCC_SEG_ALLOC fccseg ");
		if (isOnlyInterline) {
			sql.append(", T_FCC_SEG_BC_ALLOC fccbc ");
		}
		sql.append("WHERE ");
		sql.append("fltseg.FLIGHT_ID = flt.FLIGHT_ID and ");
		sql.append("fccseg.SEGMENT_CODE = fltseg.SEGMENT_CODE and ");
		sql.append("fccseg.FLIGHT_ID = fltseg.FLIGHT_ID and ");
		sql.append("fltseg.SEGMENT_VALID_FLAG = 'Y' and ");
		if (isOnlyInterline) {
			sql.append("fccbc.FCCSA_ID =  fccseg.FCCSA_ID  and ");
			sql.append("fccbc.STATUS =  'OPN'  and ");
			sql.append("fccbc.BOOKING_CODE in (");
			for (String bookingClass : externalBookingClasses) {
				sql.append("'");
				sql.append(bookingClass);
				sql.append("',");
			}
			sql.setLength(sql.length() - 1);
			sql.append(") and ");
		}

		if (colFlightSegIds != null && colFlightSegIds.size() > 0) {
			sql.append("fltseg.FLT_SEG_ID in (" + Util.buildIntegerInClauseContent(colFlightSegIds) + ") and ");
		}

		sql.append("fltseg.STATUS = ? and ");
		sql.append("flt.STATUS = ? and ");
		sql.append("fccseg.CABIN_CLASS_CODE = ? and ");
		sql.append("fltseg.EST_TIME_DEPARTURE_LOCAL >= ? and ");
		sql.append("fltseg.EST_TIME_DEPARTURE_LOCAL <= ? and ");
		if (arrMinDate != null && arrMaxDate != null) {
			sql.append("fltseg.EST_TIME_ARRIVAL_LOCAL >= ? and ");
			sql.append("fltseg.EST_TIME_ARRIVAL_LOCAL <= ? and ");
		}
		sql.append("fltseg.SEGMENT_CODE like ? ");
		if (includeOnlyMarketingCarrierSegs) {
			sql.append(prepareFlightNumberSQL(isInterline));
		} else if (isInterline) {
			sql.append(getInterlineExcludeQuery());
		}
		// TODO Clear this
		/*
		 * if (type.equals(AvailabilitySearchTypeEnum.SINGLE_FLIGHTS_FOR_PERIOD)) {
		 * sql.append("and fccseg.AVAILABLE_SEATS >= ? "); sql.append("and fccseg.AVAILABLE_INFANT_SEATS >= ? ");
		 * sql.append( "and (trunc(EST_TIME_DEPARTURE_LOCAL),EST_TIME_DEPARTURE_LOCAL) in (  " ); sql.append(
		 * "select trunc(EST_TIME_DEPARTURE_LOCAL),min(EST_TIME_DEPARTURE_LOCAL) " ); sql.append(
		 * "from t_flight_segment group by trunc(EST_TIME_DEPARTURE_LOCAL)) "); } if
		 * ((type.equals(AvailabilitySearchTypeEnum.MULTIPLE_FLIGHTS_FOR_DAY) || type
		 * .equals(AvailabilitySearchTypeEnum.FILTERED_MULTIPLE_FLIGHTS_FOR_PERIOD )) && availabilityRestriction ==
		 * AvailableFlightSearchDTO.AVAILABILTY_RESTRICTED) { sql.append("and fccseg.AVAILABLE_SEATS >= ? ");
		 * sql.append("and fccseg.AVAILABLE_INFANT_SEATS >= ? "); }
		 */
		sql.append("AND flt.model_number = fltmod.model_number ");
		sql.append("order by flt.DEPARTURE_DATE, fccseg.CABIN_CLASS_CODE");

		String segmentCode = origin + "/%" + destination;
		Object[] params = { FlightSegmentStatusEnum.OPEN.getCode(), FlightStatusEnum.ACTIVE.getCode(), ccCode, minDate, maxDate,
				segmentCode };
		if (arrMinDate != null && arrMaxDate != null) {
			params = new Object[] { FlightSegmentStatusEnum.OPEN.getCode(), FlightStatusEnum.ACTIVE.getCode(), ccCode, minDate,
					maxDate, arrMinDate, arrMaxDate, segmentCode };
		}
		/*
		 * if (type.equals(AvailabilitySearchTypeEnum.SINGLE_FLIGHTS_FOR_PERIOD) ||
		 * ((type.equals(AvailabilitySearchTypeEnum.MULTIPLE_FLIGHTS_FOR_DAY) || type
		 * .equals(AvailabilitySearchTypeEnum.FILTERED_MULTIPLE_FLIGHTS_FOR_PERIOD )) && availabilityRestriction ==
		 * AvailableFlightSearchDTO.AVAILABILTY_RESTRICTED)) {
		 * 
		 * if (arrMinDate != null && arrMaxDate != null) { params = new Object[] {
		 * FlightSegmentStatusEnum.OPEN.getCode(), FlightStatusEnum.ACTIVE.getCode(), ccCode, minDate, maxDate,
		 * arrMinDate, arrMaxDate, segmentCode, new Integer(adults), new Integer(infants) }; } else { params = new
		 * Object[] { FlightSegmentStatusEnum.OPEN.getCode(), FlightStatusEnum.ACTIVE.getCode(), ccCode, minDate,
		 * maxDate, segmentCode, new Integer(adults), new Integer(infants) }; } }
		 */
		LookupService lookup = LookupServiceFactory.getInstance();
		JdbcTemplate jt = new JdbcTemplate((DataSource) lookup.getBean(PlatformBeanUrls.Commons.DEFAULT_DATA_SOURCE_BEAN));

		@SuppressWarnings("unchecked")
		List<AvailableFlightSegment> list = (List<AvailableFlightSegment>) jt.query(sql.toString(), params,
				new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

						AvailableFlightSegment availableFlightSegment;
						FlightSegmentDTO flightSegmentDTO;
						List<AvailableFlightSegment> segments = new ArrayList<AvailableFlightSegment>();

						if (rs != null) {
							while (rs.next()) {
								availableFlightSegment = new AvailableFlightSegment();
								flightSegmentDTO = new FlightSegmentDTO();
								flightSegmentDTO.setFlightId(new Integer(rs.getInt("FLIGHT_ID")));
								flightSegmentDTO.setFlightScheduleId(new Integer(rs.getInt("SCHEDULE_ID")));
								flightSegmentDTO.setArrivalDateTime(rs.getTimestamp("EST_TIME_ARRIVAL_LOCAL"));
								flightSegmentDTO.setDepartureDateTime(rs.getTimestamp("EST_TIME_DEPARTURE_LOCAL"));
								flightSegmentDTO.setArrivalDateTimeZulu(rs.getTimestamp("EST_TIME_ARRIVAL_ZULU"));
								flightSegmentDTO.setDepartureDateTimeZulu(rs.getTimestamp("EST_TIME_DEPARTURE_ZULU"));
								flightSegmentDTO.setSegmentCode(rs.getString("SEGMENT_CODE"));
								flightSegmentDTO.setFromAirport(rs.getString("ORIGIN"));
								flightSegmentDTO.setToAirport(rs.getString("DESTINATION"));
								flightSegmentDTO.setFlightNumber(rs.getString("FLIGHT_NUMBER"));
								flightSegmentDTO.setSegmentId(new Integer(rs.getInt("FLT_SEG_ID")));
								flightSegmentDTO.setOperationTypeID(new Integer(rs.getInt("operation_type_id")));

								flightSegmentDTO.setFlightModelNumber(rs.getString("MODEL_NUMBER"));
								flightSegmentDTO.setFlightModelDescription(rs.getString("ITIN_DESCRIPTION"));
								String durationTime = FlightUtil.getDuration(rs.getTimestamp("EST_TIME_DEPARTURE_ZULU"),
										rs.getTimestamp("EST_TIME_ARRIVAL_ZULU"));
								flightSegmentDTO.setFlightDuration(durationTime);
								flightSegmentDTO.setRemarks(rs.getString("REMARKS"));
								flightSegmentDTO.setFlightSegmentStatus(rs.getString("STATUS"));

								if (FlightType.DOMESTIC.getType().equals(rs.getString("FLIGHT_TYPE")))
									flightSegmentDTO.setDomesticFlight(true);

								// availableFlightSegment.setAvailableAdultConunt(rs.getInt("AVAILABLE_SEATS"));
								// availableFlightSegment.setAvailableInfantConunt(rs.getInt("AVAILABLE_INFANT_SEATS"));

								// availableFlightSegment.setAllocatedSeats(rs.getInt("ALLOCATED_SEATS"));
								// availableFlightSegment.setSoldSeats(rs.getInt("SOLD_SEATS"));
								// availableFlightSegment.setOnHoldSeats(rs.getInt("ON_HOLD_SEATS"));
								// availableFlightSegment.setInfantAllocation(rs.getInt("INFANT_ALLOCATION"));
								// availableFlightSegment.setSoldInfantSeats(rs.getInt("SOLD_INFANT_SEATS"));
								// availableFlightSegment.setOnholdInfantSeats(rs.getInt("ONHOLD_INFANT_SEATS"));
								// availableFlightSegment.setFccsaId(rs.getInt("FCCSA_ID"));

								SeatAllocationDTO seatAllocation = new SeatAllocationDTO();
								seatAllocation.setFccsaId(rs.getInt("FCCSA_ID"));
								seatAllocation.setAllocatedSeats(rs.getInt("ALLOCATED_SEATS"));
								seatAllocation.setInfantAllocation(rs.getInt("INFANT_ALLOCATION"));
								seatAllocation.setOnholdInfantSeats(rs.getInt("ONHOLD_INFANT_SEATS"));
								seatAllocation.setOnHoldSeats(rs.getInt("ON_HOLD_SEATS"));
								seatAllocation.setSoldInfantSeats(rs.getInt("SOLD_INFANT_SEATS"));
								seatAllocation.setSoldSeats(rs.getInt("SOLD_SEATS"));
								availableFlightSegment.setSeatAllocationDTO(seatAllocation);

								availableFlightSegment.setCabinClassCode(rs.getString("CABIN_CLASS_CODE"));
								availableFlightSegment.getFlightSegmentDTOs().add(flightSegmentDTO);

								segments.add(availableFlightSegment);
							}
						}

						return segments;
					}
				});

		return list;
	}

	@Override
	public List<Flight> getFlightsByAnciDefTemplStatus(String ancillaryType, AnciDefaultTemplStatusEnum status) {

		List<Flight> results = new ArrayList<Flight>();

		if (ancillaryType != null
				&& (ReservationInternalConstants.SERVICE_CALLER.MEAL.equalsIgnoreCase(ancillaryType) || ReservationInternalConstants.SERVICE_CALLER.SEAT_MAP
						.equalsIgnoreCase(ancillaryType))) {
			String hql = "from Flight as f where ";
			if (ReservationInternalConstants.SERVICE_CALLER.MEAL.equalsIgnoreCase(ancillaryType)) {
				hql += "f.mealTemplAssignStatus =? ";

			} else if (ReservationInternalConstants.SERVICE_CALLER.SEAT_MAP.equalsIgnoreCase(ancillaryType)) {
				hql += "f.seatTemplAssignStatus =? ";
			}

			hql += "and (f.status != ? OR f.status!=?)";

			Object[] params = { status.getCode(), FlightStatusEnum.CANCELLED.getCode(), FlightStatusEnum.CLOSED.getCode() };
			results = find(hql, params, Flight.class);
		}

		return results;
	}

	@Override
	public void updateFlightAnciAssignStatus(Integer flightId, String ancillaryType, AnciDefaultTemplStatusEnum status) {
		try {

			if (ancillaryType != null
					&& (ReservationInternalConstants.SERVICE_CALLER.MEAL.equalsIgnoreCase(ancillaryType) || ReservationInternalConstants.SERVICE_CALLER.SEAT_MAP
							.equalsIgnoreCase(ancillaryType))) {
				String hqlUpdate = "update Flight set ";
				if (ReservationInternalConstants.SERVICE_CALLER.MEAL.equalsIgnoreCase(ancillaryType)) {
					hqlUpdate += "mealTemplAssignStatus =:status ";
				} else if (ReservationInternalConstants.SERVICE_CALLER.SEAT_MAP.equalsIgnoreCase(ancillaryType)) {
					hqlUpdate += "seatTemplAssignStatus =:status ";
				}
				hqlUpdate += "where flightId =:flightId";

				getSession().createQuery(hqlUpdate).setParameter("status", status.getCode()).setParameter("flightId", flightId)
						.executeUpdate();
			}

		} catch (Exception e) {
			throw new CommonsDataAccessException(e, null);
		}
	}

	@Override
	public void updateFltAnciAssignStatusByFltSegId(Collection<Integer> flightSegIds, String ancillaryType,
			AnciDefaultTemplStatusEnum status) {
		try {

			if (ancillaryType != null
					&& (ReservationInternalConstants.SERVICE_CALLER.MEAL.equalsIgnoreCase(ancillaryType) || ReservationInternalConstants.SERVICE_CALLER.SEAT_MAP
							.equalsIgnoreCase(ancillaryType))) {

				String sqlUpdateQuery = "UPDATE t_flight SET ";
				if (ReservationInternalConstants.SERVICE_CALLER.MEAL.equalsIgnoreCase(ancillaryType)) {
					sqlUpdateQuery += "MEAL_TEMPL_ASSIGN_STATUS=? ";

					if (status.getCode().equals(AnciDefaultTemplStatusEnum.CREATED)) {
						sqlUpdateQuery += ",MEAL_TEMPL_ERROR_DESC=''";
					}

				} else if (ReservationInternalConstants.SERVICE_CALLER.SEAT_MAP.equalsIgnoreCase(ancillaryType)) {
					sqlUpdateQuery += "AMC_TEMPL_ASSIGN_STATUS=? ";

					if (status.getCode().equals(AnciDefaultTemplStatusEnum.CREATED)) {
						sqlUpdateQuery += ",AMC_TEMPL_ERROR_DESC='' ";
					}

				}

				String sqlFlightIDQuery = "SELECT FLIGHT_ID FROM t_flight_segment WHERE "
						+ Util.getReplaceStringForIN("FLT_SEG_ID", flightSegIds);

				DataSource ds = LookUpUtils.getDatasource();

				JdbcTemplate templete = new JdbcTemplate(ds);

				@SuppressWarnings("unchecked")
				List<String> flightIDList = (List<String>) templete.query(sqlFlightIDQuery, new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						List<String> resList = new ArrayList<String>();
						while (rs.next()) {
							resList.add(rs.getString("FLIGHT_ID"));

						}
						return resList;
					}
				});

				sqlUpdateQuery += "WHERE " + Util.getReplaceStringForIN("FLIGHT_ID", flightIDList);
				if (ReservationInternalConstants.SERVICE_CALLER.MEAL.equalsIgnoreCase(ancillaryType)) {
					sqlUpdateQuery += "AND MEAL_TEMPL_ASSIGN_STATUS != 'CRE' ";
				} else if (ReservationInternalConstants.SERVICE_CALLER.SEAT_MAP.equalsIgnoreCase(ancillaryType)) {
					sqlUpdateQuery += "AND AMC_TEMPL_ASSIGN_STATUS != 'CRE' ";
				}

				Object[] params = { status.getCode() };

				templete.update(sqlUpdateQuery, params);

			}

		} catch (Exception e) {
			throw new CommonsDataAccessException(e, null);
		}
	}

	@Override
	public boolean isAirModelHasSeatTemplate(String modelNumber) {
		try {
			String sql = "SELECT AMC_TEMPLATE_ID FROM sm_t_am_charge_template WHERE MODEL_NUMBER='" + modelNumber + "'";
			DataSource ds = LookUpUtils.getDatasource();
			JdbcTemplate templete = new JdbcTemplate(ds);

			@SuppressWarnings("unchecked")
			Collection<Integer> amcTemplIdColl = (Collection<Integer>) templete.query(sql, new ResultSetExtractor() {

				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

					Collection<Integer> pnrs = new ArrayList<Integer>();
					if (rs != null) {
						while (rs.next()) {
							pnrs.add(rs.getInt("AMC_TEMPLATE_ID"));
						}
					}
					return pnrs;
				}
			});

			if (amcTemplIdColl != null && amcTemplIdColl.size() > 0) {
				return true;
			}

			return false;
		} catch (Exception e) {
			throw new CommonsDataAccessException(e, null);
		}

	}

	@Override
	public void updateFltAnciAssignStatusAsError(Integer flightId, String ancillaryType, String errorDesc) {
		try {

			if (ancillaryType != null
					&& (ReservationInternalConstants.SERVICE_CALLER.MEAL.equalsIgnoreCase(ancillaryType) || ReservationInternalConstants.SERVICE_CALLER.SEAT_MAP
							.equalsIgnoreCase(ancillaryType))) {

				String sqlUpdateQuery = "UPDATE t_flight SET ";
				if (ReservationInternalConstants.SERVICE_CALLER.MEAL.equalsIgnoreCase(ancillaryType)) {
					sqlUpdateQuery += "MEAL_TEMPL_ASSIGN_STATUS=?,MEAL_TEMPL_ERROR_DESC=? ";
				} else if (ReservationInternalConstants.SERVICE_CALLER.SEAT_MAP.equalsIgnoreCase(ancillaryType)) {
					sqlUpdateQuery += "AMC_TEMPL_ASSIGN_STATUS=?,AMC_TEMPL_ERROR_DESC=? ";
				}
				sqlUpdateQuery += "WHERE FLIGHT_ID =?";

				DataSource ds = LookUpUtils.getDatasource();

				JdbcTemplate templete = new JdbcTemplate(ds);

				Object[] params = { AnciDefaultTemplStatusEnum.CREATED_W_ERRORS.getCode(), errorDesc, flightId };

				templete.update(sqlUpdateQuery, params);

			}

		} catch (Exception e) {
			throw new CommonsDataAccessException(e, null);
		}
	}

	@Override
	public HashMap<String, Integer> defAnciAssignSummaryByFltScheduleId(Integer scheduleId, String ancillaryType) {
		try {
			if (ancillaryType != null
					&& (ReservationInternalConstants.SERVICE_CALLER.MEAL.equalsIgnoreCase(ancillaryType) || ReservationInternalConstants.SERVICE_CALLER.SEAT_MAP
							.equalsIgnoreCase(ancillaryType))) {
				String anciTypeStatusColumn = null;

				if (ReservationInternalConstants.SERVICE_CALLER.MEAL.equalsIgnoreCase(ancillaryType)) {
					anciTypeStatusColumn = "meal_templ_assign_status";
				} else if (ReservationInternalConstants.SERVICE_CALLER.SEAT_MAP.equalsIgnoreCase(ancillaryType)) {
					anciTypeStatusColumn = "amc_templ_assign_status";
				}

				String sql = "SELECT " + anciTypeStatusColumn + " AS anci_status,COUNT(*) AS cnt ";
				sql += "FROM t_flight WHERE " + anciTypeStatusColumn + " IS NOT NULL AND schedule_id IS NOT NULL ";
				sql += "AND schedule_id =" + scheduleId + " AND (status != 'CNX' or status != 'CLS') ";
				sql += "GROUP BY schedule_id," + anciTypeStatusColumn;

				DataSource ds = LookUpUtils.getDatasource();
				JdbcTemplate templete = new JdbcTemplate(ds);

				@SuppressWarnings("unchecked")
				HashMap<String, Integer> fltAnciStatusMap = (HashMap<String, Integer>) templete.query(sql,
						new ResultSetExtractor() {

							@Override
							public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
								HashMap<String, Integer> fltAnciStatusMap1 = new HashMap<String, Integer>();
								if (rs != null) {
									while (rs.next()) {
										if (rs.getString("anci_status") != null) {
											fltAnciStatusMap1.put(rs.getString("anci_status"), rs.getInt("cnt"));
										}
									}
								}
								return fltAnciStatusMap1;
							}
						});
				return fltAnciStatusMap;
			}
			return null;
		} catch (Exception e) {
			throw new CommonsDataAccessException(e, null);
		}

	}

	@Override
	public void updateFltScheduleAnciAssignStatusSummary(Integer scheduleId, String ancillaryType, String status,
			String summaryTxt) {
		try {

			if (ancillaryType != null
					&& (ReservationInternalConstants.SERVICE_CALLER.MEAL.equalsIgnoreCase(ancillaryType) || ReservationInternalConstants.SERVICE_CALLER.SEAT_MAP
							.equalsIgnoreCase(ancillaryType))) {

				String sqlUpdateQuery = "UPDATE t_flight_schedule SET ";
				if (ReservationInternalConstants.SERVICE_CALLER.MEAL.equalsIgnoreCase(ancillaryType)) {
					sqlUpdateQuery += "MEAL_TEMPL_ASSIGN_STATUS=?,MEAL_TEMPL_ERROR_DESC=? ";
				} else if (ReservationInternalConstants.SERVICE_CALLER.SEAT_MAP.equalsIgnoreCase(ancillaryType)) {
					sqlUpdateQuery += "AMC_TEMPL_ASSIGN_STATUS=?,AMC_TEMPL_ERROR_DESC=? ";
				}
				sqlUpdateQuery += "WHERE schedule_id =?";

				DataSource ds = LookUpUtils.getDatasource();

				JdbcTemplate templete = new JdbcTemplate(ds);

				Object[] params = { status, summaryTxt, scheduleId };

				templete.update(sqlUpdateQuery, params);

			}

		} catch (Exception e) {
			throw new CommonsDataAccessException(e, null);
		}
	}

	@Override
	public List<Integer> getAllBuildFltSchedIdByService(Collection<Integer> flightScheduleIds, String ancillaryType) {
		try {
			if (ancillaryType != null
					&& (ReservationInternalConstants.SERVICE_CALLER.MEAL.equalsIgnoreCase(ancillaryType) || ReservationInternalConstants.SERVICE_CALLER.SEAT_MAP
							.equalsIgnoreCase(ancillaryType))) {
				String anciTypeStatusColumn = null;
				String anciTypeDescColumn = null;

				if (ReservationInternalConstants.SERVICE_CALLER.MEAL.equalsIgnoreCase(ancillaryType)) {
					anciTypeStatusColumn = "meal_templ_assign_status";
					anciTypeDescColumn = "meal_templ_error_desc";
				} else if (ReservationInternalConstants.SERVICE_CALLER.SEAT_MAP.equalsIgnoreCase(ancillaryType)) {
					anciTypeStatusColumn = "amc_templ_assign_status";
					anciTypeDescColumn = "amc_templ_error_desc";
				}

				String sql = "SELECT DISTINCT schedule_id FROM t_flight WHERE schedule_id ";
				sql += "IN (SELECT schedule_id FROM t_flight_schedule WHERE " + anciTypeStatusColumn + " IS NULL OR "
						+ anciTypeDescColumn + " IS NULL) ";
				sql += "AND schedule_id IS NOT NULL AND " + anciTypeStatusColumn + " NOT IN ('"
						+ AnciDefaultTemplStatusEnum.SCHEDULED.getCode() + "','"
						+ AnciDefaultTemplStatusEnum.IN_PROGRESS.getCode() + "') ";

				if (flightScheduleIds != null && flightScheduleIds.size() > 0) {
					sql += "AND schedule_id NOT IN (" + Util.buildIntegerInClauseContent(flightScheduleIds) + ")";
				}

				DataSource ds = LookUpUtils.getDatasource();
				JdbcTemplate templete = new JdbcTemplate(ds);

				@SuppressWarnings("unchecked")
				List<Integer> fltSchedIdList = (List<Integer>) templete.query(sql, new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						List<Integer> scheduleIdList = new ArrayList<Integer>();
						if (rs != null) {
							while (rs.next()) {
								scheduleIdList.add(rs.getInt("schedule_id"));
							}
						}
						return scheduleIdList;
					}
				});
				return fltSchedIdList;
			}
			return null;
		} catch (Exception e) {
			throw new CommonsDataAccessException(e, null);
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<FlightsToPublish> getFlightsToPublishByStatus(List<String> statusList, Date startDate, Date endDate) {

		if (startDate != null && endDate != null) {
			String hql = "from FlightsToPublish as f where f.processStatus IN (:statuses) and f.flightEditTime between :startDate and :endDate ";
			return getSession().createQuery(hql).setParameterList("statuses", statusList).setParameter("startDate", startDate)
					.setParameter("endDate", endDate).list();
		} else {
			String hql = "from FlightsToPublish as f where f.processStatus IN (:statuses) order by f.flightEditTime desc ";
			return getSession().createQuery(hql).setMaxResults(2000).setParameterList("statuses", statusList).list();
		}
	}

	@Override
	public void updateFlightsToPublishStatus(List<Integer> flightsToPublishIds, String status) {
		String hqlUpdate = "update FlightsToPublish set processStatus =:status , processedTime =:currentDate" + " where "
				+ "flightsToPublishId in (:flightsToPublishIds)";

		getSession().createQuery(hqlUpdate).setParameter("status", status)
				.setParameter("currentDate", CalendarUtil.getCurrentSystemTimeInZulu())
				.setParameterList("flightsToPublishIds", flightsToPublishIds).executeUpdate();
	}

	@Override
	public void saveOrUpdateFlightsToPublish(FlightsToPublish flightsToPublish) {
		hibernateSaveOrUpdate(flightsToPublish);

	}

	@Override
	public FlightsToPublish getFlightToPublish(Integer flightId) {
		List<FlightsToPublish> availableFlightList = find("from FlightsToPublish pub where pub.processStatus = '"
				+ FlightsToPublish.PENDING + "' and pub.flightId = ? ", flightId, FlightsToPublish.class);

		// Only one pending object can be available because we will be updating the pending
		// with the latest update
		if (availableFlightList != null && !availableFlightList.isEmpty()) {
			return availableFlightList.get(0);
		}

		return null;
	}

	@Override
	@SuppressWarnings("unchecked")
	public ArrayList<FlightSegmentDTO> getGoshowBCOpeningFilghtList(Date date) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());

		Timestamp dateLowerLimit = new Timestamp(date.getTime() + ((24 * 1) * 3600 * 1000));
		Timestamp dateUpperLimit = new Timestamp(date.getTime() + ((24 * 2) * 3600 * 1000));
		Object params[] = { dateUpperLimit, dateLowerLimit, "Y" };

		String sql = "SELECT flight.FLIGHT_ID, flightseg.FLT_SEG_ID,flightseg.EST_TIME_DEPARTURE_ZULU,flight.FLIGHT_TYPE FROM T_FLIGHT flight, T_FLIGHT_SEGMENT flightseg"
				+ " WHERE flightseg.FLIGHT_ID = flight.FLIGHT_ID AND flightseg.EST_TIME_DEPARTURE_ZULU <= ? AND flightseg.EST_TIME_DEPARTURE_ZULU  > ?"
				+ " AND flight.STATUS = '"
				+ Flight.FLIGHT_ACTIVE
				+ "' AND flightseg.SEGMENT_VALID_FLAG =? ORDER BY flightseg.flight_id, flightseg.EST_TIME_DEPARTURE_ZULU";

		return (ArrayList<FlightSegmentDTO>) template.query(sql, params, new ResultSetExtractor() {

			@Override
			public Object extractData(final ResultSet rs) throws SQLException, DataAccessException {
				final ArrayList<FlightSegmentDTO> flightSegList = new ArrayList<FlightSegmentDTO>();
				while (rs.next()) {
					final FlightSegmentDTO fltSegDTO = new FlightSegmentDTO();
					fltSegDTO.setFlightId(rs.getInt("FLIGHT_ID"));
					fltSegDTO.setSegmentId(rs.getInt("FLT_SEG_ID"));
					fltSegDTO.setDepartureDateTimeZulu(rs.getTimestamp("EST_TIME_DEPARTURE_ZULU"));
					fltSegDTO.setFlightType(rs.getString("FLIGHT_TYPE"));
					flightSegList.add(fltSegDTO);
				}
				return flightSegList;
			}
		});
	}

	@Override
	public List<Flight> getActiveFlightWithinCutOffTime(Integer flightSegId) {
		String hql = "SELECT f FROM FlightSegement fs, Flight f WHERE fs.fltSegId =" + flightSegId
				+ " AND f.flightId = fs.flightId AND f.status = '" + Flight.FLIGHT_ACTIVE + "'";
		return find(hql, Flight.class);
	}

	@Override
	public void activatePastFlights(int flightId) {
		String sqlQuery = "update T_FLIGHT_SEGMENT  set STATUS='OPN' where FLIGHT_ID=" + flightId + " AND STATUS='CLS'";

		DataSource ds = LookUpUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);
		templete.update(sqlQuery);
	}

	private DataSource getDataSource() {
		return (DataSource) LookupServiceFactory.getInstance().getBean(PlatformBeanUrls.Commons.DEFAULT_DATA_SOURCE_BEAN);
	}

	@Override
	public List<FlightSchedule> getFlightScheduleForModel(String modelNumber) {
		List<FlightSchedule> list = null;

		try {

			String hql = "select f " + "from FlightSchedule as f " + "where "
					+ "f.modelNumber = :modelNumber and f.stopDate > :currentDate ";

			Date cuurentDate = new Date(System.currentTimeMillis());
			Query q = getSession().createQuery(hql).setParameter("modelNumber", modelNumber);
			if (cuurentDate != null) {
				q.setTimestamp("currentDate", cuurentDate);
			}
			list = q.list();

		} catch (Exception e) {

			throw new CommonsDataAccessException(e, null);
		}

		return list;
	}

	@Override
	public void updatedFlights(String modelNumber, String newModuleNumber, Map<Integer, Integer> scheduleIdsMap) {
		try {
			String hqlUpdate = "update Flight set modelNumber =:newModuleNumber " + "where "
					+ "modelNumber  = :modelNumber and (departureDate <:currentDate or status = :status)";
			Date cuurentDate = new Date(System.currentTimeMillis());
			getSession().createQuery(hqlUpdate).setParameter("modelNumber", modelNumber)
					.setParameter("newModuleNumber", newModuleNumber).setParameter("currentDate", cuurentDate)
					.setParameter("status", "CNX").executeUpdate();

			for (Integer oldScheduleId : scheduleIdsMap.keySet()) {
				String hqlUpdateflights = "update Flight set scheduleId =:newScheduleId where "
						+ "modelNumber  = :modelNumber and departureDate >:currentDate and scheduleId =:oldScheduleId";
				getSession().createQuery(hqlUpdateflights).setParameter("modelNumber", modelNumber)
						.setParameter("currentDate", cuurentDate).setParameter("oldScheduleId", oldScheduleId)
						.setParameter("newScheduleId", scheduleIdsMap.get(oldScheduleId)).executeUpdate();
			}

		} catch (Exception e) {
			throw new CommonsDataAccessException(e, null);
		}

	}

	@Override
	public boolean isAirModelHasChargeTemplate(String modelNumber) {
		try {
			String sql = "select PPSAM_SEAT_ID from sm_t_pnr_pax_seg_am_seat smtfltseat left outer join t_pnr_segment tps"
					+ " on smtfltseat.pnr_seg_id=tps.pnr_seg_id  inner join t_flight_segment tfs on tfs.flt_seg_id=tps.flt_seg_id"
					+ " inner join t_flight tf on tfs.flight_id=tf.flight_id where tf.model_number='" + modelNumber + "'";
			DataSource ds = LookUpUtils.getDatasource();
			JdbcTemplate templete = new JdbcTemplate(ds);

			@SuppressWarnings("unchecked")
			Collection<Integer> amcTemplIdColl = (Collection<Integer>) templete.query(sql, new ResultSetExtractor() {

				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

					Collection<Integer> pnrs = new ArrayList<Integer>();
					if (rs != null) {
						while (rs.next()) {
							pnrs.add(rs.getInt("PPSAM_SEAT_ID"));
						}
					}
					return pnrs;
				}
			});

			if (amcTemplIdColl != null && amcTemplIdColl.size() > 0) {
				return true;
			}

			return false;
		} catch (Exception e) {
			throw new CommonsDataAccessException(e, null);
		}
	}

	@Override
	public boolean isSeatMapSeatsUsedWithFlight(List<Integer> flightIds) {
		String s = "";

		if (flightIds != null) {
			Object[] ids = flightIds.toArray();
			for (int i = 0; i < ids.length; i++) {
				s += ids[i];
				if (i < ids.length - 1) {
					s += ",";
				}
			}
		}
		String sql = "select distinct(flight_id) from t_flight " + " where flight_id in (" + s + ") "
				+ " 	and amc_template_id >0  ";

		DataSource ds = LookUpUtils.getDatasource();

		JdbcTemplate templete = new JdbcTemplate(ds);

		final List<Integer> lstSegs = new ArrayList<Integer>();
		templete.query(sql,

		new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					lstSegs.add(rs.getInt("flight_id"));
				}
				return lstSegs;
			}
		});

		if (lstSegs.size() > 0) {
			return true;
		}

		return false;
	}

	@Override
	public List<String> getFLightsWithAirModelHasChargeTemplate(String modelNumber) {
		try {

			String sql = "SELECT distinct tf.flight_number,tf.departure_date FROM sm_t_am_seat_charge sc inner join sm_t_flight_am_seat fas "
					+ "on sc.ams_charge_id=fas.ams_charge_id inner join sm_t_am_charge_template act on act.amc_template_id=sc.amc_template_id "
					+ "INNER JOIN t_flight_segment tfs ON tfs.flt_seg_id=fas.flt_seg_id INNER JOIN t_flight tf ON tfs.flight_id     =tf.flight_id where act.model_number='"
					+ modelNumber + "'";
			DataSource ds = LookUpUtils.getDatasource();
			JdbcTemplate templete = new JdbcTemplate(ds);

			@SuppressWarnings("unchecked")
			List<String> fltSchedIdList = (List<String>) templete.query(sql, new ResultSetExtractor() {

				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					List<String> scheduleIdList = new ArrayList<String>();
					if (rs != null) {
						while (rs.next()) {
							scheduleIdList.add(rs.getString("flight_number") + "|" + rs.getDate("departure_date"));
						}
					}
					return scheduleIdList;
				}
			});
			return fltSchedIdList;

		} catch (Exception e) {
			throw new CommonsDataAccessException(e, null);
		}
	}
	
	/**
	 * Retrieve the CS MC flight detail for a given flight id and MC code.
	 */
	@Override
	@SuppressWarnings("unchecked")
	public String getMCFlightNumber(Integer flightId, String marketingCarrierCode) {

		String hql = "select f.csMCFlightNumber from CodeShareMCFlight as f where f.flightId= ? and f.csMCCarrierCode = ?";

		Object[] params = { flightId, marketingCarrierCode };
		List<String> results = find(hql, params, String.class);
		if (results != null && results.size() == 1) {
			return results.get(0);
		} else {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Flight> getAdHocFlightsForASMRecap(Date startDate, Date endDate, boolean isSelectedRange) {

		Collection<Flight> resultList = null;
		String hql = null;
		if (isSelectedRange) {
			hql = "select flight from Flight as flight " + "where flight.departureDate >= :startDate "
					+ "and flight.departureDate <= :endDate " + " and flight.scheduleId is null and flight.status = '"
					+ ScheduleStatusEnum.ACTIVE.getCode() + "'";
			resultList = getSession().createQuery(hql).setParameter("startDate", startDate)
					.setParameter("endDate", endDate).list();

		} else {
			hql = " select flight from Flight as flight " + "where flight.departureDate >= sysdate"
					+ " and flight.scheduleId is null and flight.status = '" + ScheduleStatusEnum.ACTIVE.getCode() + "'";
			resultList = getSession().createQuery(hql).list();
		}

		return resultList;
	}
	
	@Override
	public void updateDelayCancelFlight(Collection<Integer> flightIds, boolean status, Date holdTill, String remarks) {
		try {

			if (flightIds != null && !flightIds.isEmpty()) {
				String hqlUpdate = "update Flight set cancelMsgWaiting =:cancelMsgWaiting,holdCancelMsgTill=:holdCancelMsgTill, "
						+ "remarks=:remarks where " + " flightId in (" + Util.buildIntegerInClauseContent(flightIds) + ")";

				getSession().createQuery(hqlUpdate).setParameter("cancelMsgWaiting", status)
						.setParameter("holdCancelMsgTill", holdTill)
						.setParameter("remarks", remarks).executeUpdate();
			}

		} catch (Exception e) {
			throw new CommonsDataAccessException(e, null);
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public Collection<Integer> getCancelledDelayedFlightsId(Date fromDateTime) {
		StringBuilder sbSql = new StringBuilder();

		sbSql.append("SELECT FLIGHT_ID FROM T_FLIGHT WHERE CNL_MSG_WAITING=? ");
		sbSql.append("AND HOLD_CNL_MSG_TILL < TO_DATE(" + CalendarUtil.formatForSQL_toDate(fromDateTime) + ") ");
		sbSql.append("AND STATUS <>? ");

		DataSource ds = LookUpUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		Object[] params = { "Y", ScheduleStatusEnum.CANCELLED.getCode() };

		List<Integer> flightIds = (List<Integer>) templete.query(sbSql.toString(), params, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<Integer> flightIds = new ArrayList<Integer>();
				while (rs.next()) {
					flightIds.add(rs.getInt("FLIGHT_ID"));
				}
				return flightIds;
			}
		});

		return flightIds;
	}
	
	@Override
	@SuppressWarnings({ "unchecked" })
	public Collection<Integer> getPossibleFlightDesignatorChange(Date departureDate, String segmentCode, boolean isLocalTime,
			Date flightsUntil, boolean isAdHocFlightsOnly) {
		StringBuilder sbSql = new StringBuilder();

		sbSql.append("SELECT F.FLIGHT_ID AS FLIGHT_ID FROM T_FLIGHT F, T_FLIGHT_SEGMENT FS, T_FLIGHT_LEG FL ");
		sbSql.append("WHERE F.FLIGHT_ID = FS.FLIGHT_ID AND F.FLIGHT_ID=FL.FLIGHT_ID ");
		if (flightsUntil == null) {
			if (isLocalTime) {
				sbSql.append("AND TRUNC(FS.EST_TIME_DEPARTURE_LOCAL)= ?  ");
			} else {
				sbSql.append("AND TRUNC(F.DEPARTURE_DATE)= ?  ");
			}
		} else {
			String loadFlightsTill = CalendarUtil.formatSQLDate(flightsUntil);
			if (isLocalTime) {
				sbSql.append("AND TRUNC(FS.EST_TIME_DEPARTURE_LOCAL) >= ?  AND TRUNC(FS.EST_TIME_DEPARTURE_LOCAL) <='"
						+ loadFlightsTill + "' ");
			} else {
				sbSql.append("AND TRUNC(F.DEPARTURE_DATE) >= ?  AND TRUNC(F.DEPARTURE_DATE) <='" + loadFlightsTill + "' ");
			}
		}

		sbSql.append("AND FS.SEGMENT_CODE = ? ");
		sbSql.append("AND F.STATUS != ? ");
		sbSql.append("AND F.CNL_MSG_WAITING = ? ");
		sbSql.append("AND FS.SEGMENT_VALID_FLAG = ? ");

		if (isAdHocFlightsOnly) {
			sbSql.append("AND F.SCHEDULE_ID is null ");
		}
		String depDate = CalendarUtil.formatSQLDate(departureDate);

		DataSource ds = LookUpUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		Object[] params = { depDate, segmentCode, ScheduleStatusEnum.CANCELLED.getCode(), "Y", "Y" };

		List<Integer> flightIds = (List<Integer>) templete.query(sbSql.toString(), params, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<Integer> flightIds = new ArrayList<Integer>();
				while (rs.next()) {
					flightIds.add(rs.getInt("FLIGHT_ID"));
				}
				return flightIds;
			}
		});

		return flightIds;
	}

	@Override
	public Collection<String> getFutureFlightsNoByOriginDestination(String originAptCode, String destinationAptCode) {
		
		Collection<String> resultList = null;
		String hql = null;
		
		hql = " select distinct flightNumber from Flight as flight " + "where flight.departureDate >= sysdate"
				+ " and flight.status IN('" + FlightStatusEnum.ACTIVE.getCode() + "','"+FlightStatusEnum.CREATED.getCode()+"')"
				+ " and flight.originAptCode=:originAptCode and flight.destinationAptCode=:destinationAptCode";
		
		resultList = getSession().createQuery(hql)
				.setParameter("originAptCode", originAptCode)
				.setParameter("destinationAptCode", destinationAptCode).list();
		
		return resultList;
	}
	
	@Override
	public Collection<BasicFlightDTO> getBasicFlightDTOs(Collection<Integer> flightIds) {

		Collection<BasicFlightDTO> basicFlightDTOs = new ArrayList<BasicFlightDTO>();

		StringBuilder sql = new StringBuilder();
		sql.append("select flight.flightNumber, flight.originAptCode, flight.destinationAptCode, flight.departureDateLocal ");
		sql.append(" from Flight as flight ");
		sql.append(" where flight.flightId in");
		sql.append(" (:flightIds) ");
		sql.append(" order by flight.departureDate");

		List<Object[]> results = getSession().createQuery(sql.toString()).setParameterList("flightIds", flightIds).list();

		for (Object[] result : results) {
			BasicFlightDTO basicFlightDTO = new BasicFlightDTO();
			basicFlightDTO.setFlightNumber((String) result[0]);
			basicFlightDTO.setOrigin((String) result[1]);
			basicFlightDTO.setDestination((String) result[2]);
			basicFlightDTO.setLocalDepartureDate((Date) result[3]);
			basicFlightDTOs.add(basicFlightDTO);
		}

		return basicFlightDTOs;
	}

	@Override
	public Integer getFlightId(String flightNumber, Date departureDate) {
		Integer flightId = 0;
			String hql = "from Flight f "
					+ " where f.departureDate=to_timestamp(to_char( ? ,'dd/MM/yyyy HH24:mi:ss'),'dd/MM/yyyy HH24:mi:ss') and"
					+ " f.flightNumber= ? ";
			Object[] params = { new java.sql.Timestamp(departureDate.getTime()), flightNumber };
			List<Flight> flights = find(hql, params, Flight.class);
			return (flights.size() > 0) ? flights.get(0).getFlightId() : flightId;
	}
}
