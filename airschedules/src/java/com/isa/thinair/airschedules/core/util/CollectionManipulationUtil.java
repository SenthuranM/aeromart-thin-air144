package com.isa.thinair.airschedules.core.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * utility class for collection manipulation
 * 
 * @author Lasantha Pambagoda
 */
public class CollectionManipulationUtil {

	/**
	 * returns the elements not found in the findWithn collection belongs the findFrom Collection that is notFoundList =
	 * (findFrom intersection ( inversion of findWithin))
	 * 
	 * @param findFrom
	 * @param findWithin
	 * @return notFoundList
	 */
	public static Collection<Integer> getNotFoundList(Collection<Integer> findFrom, Collection<Integer> findWithin) {

		ArrayList<Integer> notFoundList = new ArrayList<Integer>();

		Iterator<Integer> findWithinIt = null;

		Iterator<Integer> findFromIt = findFrom.iterator();
		while (findFromIt.hasNext()) {
			Integer findFromId = (Integer) findFromIt.next();

			findWithinIt = findWithin.iterator();
			boolean notfound = true;
			while (findWithinIt.hasNext()) {
				Integer findWithinId = (Integer) findWithinIt.next();

				if (findFromId.intValue() == findWithinId.intValue()) {
					notfound = false;
					break;
				}
			}

			if (notfound)
				notFoundList.add(new Integer(findFromId.intValue()));
		}

		return notFoundList;
	}
	
}
