/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:18:22
 * 
 * $Id$
 * 
 * ===============================================================================
 */

package com.isa.thinair.airschedules.core.persistence.hibernate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.sql.DataSource;

import com.isa.thinair.airschedules.api.dto.WildcardFlightSearchDto;
import com.isa.thinair.airschedules.api.dto.WildcardFlightSearchRespDto;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airmaster.api.model.AirportDSTHistory;
import com.isa.thinair.airschedules.api.dto.AffectedPeriod;
import com.isa.thinair.airschedules.api.dto.FlightScheduleInfoDTO;
import com.isa.thinair.airschedules.api.dto.FlightScheduleLegInfoDTO;
import com.isa.thinair.airschedules.api.dto.FlightScheduleSegmentInfoDTO;
import com.isa.thinair.airschedules.api.dto.ScheduleSearchDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.FlightScheduleLeg;
import com.isa.thinair.airschedules.api.model.FlightScheduleSegment;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.airschedules.api.utils.LegUtil;
import com.isa.thinair.airschedules.api.utils.ScheduleBuildStatusEnum;
import com.isa.thinair.airschedules.api.utils.ScheduleStatusEnum;
import com.isa.thinair.airschedules.api.utils.SegmentUtil;
import com.isa.thinair.airschedules.core.persistence.dao.FlightScheduleDAO;
import com.isa.thinair.airschedules.core.util.LookUpUtils;
import com.isa.thinair.airschedules.core.util.ScheduledFlightUtil;
import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.FrequencyUtil;
import com.isa.thinair.commons.core.util.Util;

/**
 * FlightScheduleDAOImpl is the IFlightScheduletDAO implementatiion for hibernate
 * 
 * @author Lasantha Pambagoda
 * @isa.module.dao-impl dao-name="flightScheduleDAO"
 */
public class FlightScheduleDAOImpl extends PlatformBaseHibernateDaoSupport implements FlightScheduleDAO {

	private static Log log = LogFactory.getLog(FlightScheduleDAOImpl.class);

	/**
	 * method to get all flight schedules
	 */
	public Page getAllFlightSchedules(int startIndex, int pageSize) {

		List<String> orderBy = new ArrayList<String>();
		orderBy.add("flightNumber");
		orderBy.add("startDate");
		return this.getPagedData(null, startIndex, pageSize, FlightSchedule.class, orderBy);
	}

	/**
	 * method to get flight schedule for given flight schedule id
	 */
	public FlightSchedule getFlightSchedule(int id) {
		return (FlightSchedule) get(FlightSchedule.class, new Integer(id));
	}

	/**
	 * method to save flight schedule
	 */
	public void saveFlightSchedule(FlightSchedule flightschedule) {
		hibernateSaveOrUpdate(flightschedule);
	}

	/**
	 * method to save flights
	 */
	public void rollForwardUserNote(Collection<Integer> ids, String userNote, String shedId) {
		Collection<Flight> flights = getFlightsForSchedule(Integer.parseInt(shedId));
		for (Iterator<Flight> iterator = flights.iterator(); iterator.hasNext();) {
			Flight flight = (Flight) iterator.next();
			if (ids.contains(flight.getFlightId())) {
				flight.setUserNotes(userNote);
			}
		}
		hibernateSaveOrUpdateAll(flights);
	}

	/**
	 * method to remove flight schedule
	 */
	public void removeFlightSchedule(int id) {
		Object flightschedule = load(FlightSchedule.class, new Integer(id));
		delete(flightschedule);
	}

	/**
	 * method to get overlapping schedules for given flight schedule
	 * 
	 * @throws IllegalStateException
	 * @throws HibernateException
	 * @throws DataAccessResourceFailureException
	 */
	public List<FlightSchedule> getConflictingSchedules(FlightSchedule schedule, AffectedPeriod affectedPeriod) {

		String frequancyComparison = (affectedPeriod.getFrequency().getDay0() ? "fs.frequency.day0=1 or " : "")
				+ (affectedPeriod.getFrequency().getDay1() ? "fs.frequency.day1=1 or " : "")
				+ (affectedPeriod.getFrequency().getDay2() ? "fs.frequency.day2=1 or " : "")
				+ (affectedPeriod.getFrequency().getDay3() ? "fs.frequency.day3=1 or " : "")
				+ (affectedPeriod.getFrequency().getDay4() ? "fs.frequency.day4=1 or " : "")
				+ (affectedPeriod.getFrequency().getDay5() ? "fs.frequency.day5=1 or " : "")
				+ (affectedPeriod.getFrequency().getDay6() ? "fs.frequency.day6=1 or " : "");

		if (frequancyComparison.lastIndexOf("or") != -1)
			frequancyComparison = frequancyComparison.substring(0, frequancyComparison.lastIndexOf("or"));

		String hql = "select fs from FlightSchedule as fs " + "where "
				+ ((schedule.getScheduleId() != null)
						? ("fs.scheduleId != " + schedule.getScheduleId().intValue() + " and ")
						: "")
				+ "fs.flightNumber='" + schedule.getFlightNumber() + "' and " + "(( fs.startDate <= ? and fs.stopDate >= ? ) or "
				+ "( fs.startDate > ? and fs.stopDate < ? ) or " + "( fs.startDate > ? and fs.startDate <= ? ) or "
				+ "( fs.stopDate >= ? and fs.stopDate < ? )) and " + "fs.statusCode = ? and " + " ( " + frequancyComparison
				+ " )";

		Object[] params = { affectedPeriod.getStartDate(), affectedPeriod.getEndDate(), affectedPeriod.getStartDate(),
				affectedPeriod.getEndDate(), affectedPeriod.getStartDate(), affectedPeriod.getEndDate(),
				affectedPeriod.getStartDate(), affectedPeriod.getEndDate(), ScheduleStatusEnum.ACTIVE.getCode() };

		return find(hql, params, FlightSchedule.class);
	}

	/**
	 * method to get overlapping flights for given flight schedule
	 */
	public List<Flight> getConflictingFlights(FlightSchedule schedule, AffectedPeriod affectedPeriod) {

		String frequancyComparison = (affectedPeriod.getFrequency().getDay0() ? "flt.dayNumber=0 or " : "")
				+ (affectedPeriod.getFrequency().getDay1() ? "flt.dayNumber=1 or " : "")
				+ (affectedPeriod.getFrequency().getDay2() ? "flt.dayNumber=2 or " : "")
				+ (affectedPeriod.getFrequency().getDay3() ? "flt.dayNumber=3 or " : "")
				+ (affectedPeriod.getFrequency().getDay4() ? "flt.dayNumber=4 or " : "")
				+ (affectedPeriod.getFrequency().getDay5() ? "flt.dayNumber=5 or " : "")
				+ (affectedPeriod.getFrequency().getDay6() ? "flt.dayNumber=6 or " : "");

		if (frequancyComparison.lastIndexOf("or") != -1)
			frequancyComparison = frequancyComparison.substring(0, frequancyComparison.lastIndexOf("or"));

		String hql = "select flt from Flight as flt " + "where "
				+ ((schedule.getScheduleId() != null)
						? ("flt.scheduleId != " + schedule.getScheduleId().intValue() + " and ")
						: "")
				+ "( flt.status=? or flt.status=? ) and " + "flt.flightNumber=? and " +
				// "flt.scheduleId != null and " + //removed to catch manually changed flights
				"flt.departureDate >= ? and " + "flt.departureDate <= ? and " + " ( " + frequancyComparison + " )";

		Object[] params = { FlightStatusEnum.ACTIVE.getCode(), FlightStatusEnum.CREATED.getCode(), schedule.getFlightNumber(),
				CalendarUtil.getStartTimeOfDate(affectedPeriod.getStartDate()),
				CalendarUtil.getEndTimeOfDate(affectedPeriod.getEndDate()) };

		return find(hql, params, Flight.class);
	}

	/**
	 * search flight schedules
	 */
	public Page searchFlightSchedules(List<ModuleCriterion> criteria, int startIndex, int pageSize) {

		List<String> orderBy = new ArrayList<String>();
		orderBy.add("flightNumber");
		orderBy.add("startDate");
		return this.getPagedData(criteria, startIndex, pageSize, FlightSchedule.class, orderBy);
	}

	@SuppressWarnings("unchecked")
	public Collection<FlightScheduleInfoDTO> getAvailableFlightSchedules(ScheduleSearchDTO dto) {

		Collection<FlightScheduleInfoDTO> colFlightScheduleInfoDTOs = new ArrayList<FlightScheduleInfoDTO>();

		if (dto != null && dto.getStartDate() != null && dto.getEndDate() != null) {
			String hql = "select fs from FlightSchedule as fs ";
			String dateFilter = null;
			if (dto.isLocalTime()) {
				dateFilter = " ( ( fs.startDateLocal >= :startDate or fs.stopDateLocal >= :startDate ) and ( fs.stopDateLocal <= :stopDate or fs.startDateLocal <= :stopDate ) ) ";
			} else {
				dateFilter = "( ( fs.startDate >= :startDate or fs.stopDate >= :startDate ) and ( fs.stopDate <= :stopDate or fs.startDate <= :stopDate ) )";
			}

			String where = "where " + dateFilter + ((dto.getScheduleStatus() != null) ? "and fs.statusCode = :statusCode " : "")
					+ ((dto.getBuildStatus() != null) ? "and fs.buildStatusCode = :buildStatusCode " : "")
					+ ((dto.getOpType() != null) ? "and fs.operationTypeId = :operationTypeId " : "")
					+ ((dto.getFromAirport() != null) ? "and fs.departureStnCode = :fromAirport " : "")
					+ ((dto.getCsOCFlightNumber() != null) ? "and fs.csOCFlightNumber = :csOCFlightNumber " : "")
					+ ((dto.getCsOCCarrierCode() != null) ? "and fs.csOCCarrierCode = :csOCCarrierCode " : "")
					+ ((dto.getFlightNumber() != null) ? "and fs.flightNumber = :flightNumber " : "")
					+ ((dto.getToAirport() != null) ? "and fs.arrivalStnCode = :toAirport " : "");		

			if (dto.getStatusFilter() != null && !dto.getStatusFilter().isEmpty()) {
				where += " and fs.statusCode in (" + Util.buildStringInClauseContent(dto.getStatusFilter()) + ") ";
			}

			String orderby = " order by fs.flightNumber, fs.startDate";

			String hqlSelect = hql + where + orderby;

			try {

				Query select = getSession().createQuery(hqlSelect).setParameter("startDate", dto.getStartDate())
						.setParameter("stopDate", dto.getEndDate());

				if (dto.getScheduleStatus() != null)
					select = select.setParameter("statusCode", dto.getScheduleStatus());
				if (dto.getBuildStatus() != null)
					select = select.setParameter("buildStatusCode", dto.getBuildStatus());
				if (dto.getOpType() != null)
					select = select.setParameter("operationTypeId", dto.getOpType());
				if (dto.getFromAirport() != null)
					select = select.setParameter("fromAirport", dto.getFromAirport());
				if (dto.getCsOCFlightNumber() != null)
					select = select.setParameter("csOCFlightNumber", dto.getCsOCFlightNumber());
				if (dto.getCsOCCarrierCode() != null)
					select = select.setParameter("csOCCarrierCode", dto.getCsOCCarrierCode());
				if (dto.getFlightNumber() != null)
					select = select.setParameter("flightNumber", dto.getFlightNumber());
				if (dto.getToAirport() != null)
					select = select.setParameter("toAirport", dto.getToAirport());

				Collection<FlightSchedule> resultList = select.list();

				for (FlightSchedule flightSchedule : resultList) {

					FlightScheduleInfoDTO flightScheduleInfoDTO = new FlightScheduleInfoDTO();

					flightScheduleInfoDTO.setScheduleId(flightSchedule.getScheduleId());
					flightScheduleInfoDTO.setArrivalStnCode(flightSchedule.getArrivalStnCode());
					flightScheduleInfoDTO.setDepartureStnCode(flightSchedule.getDepartureStnCode());
					flightScheduleInfoDTO.setFlightNumber(flightSchedule.getFlightNumber());
					flightScheduleInfoDTO.setFlightScheduleLegs(createFlightScheduleLegs(flightSchedule.getFlightScheduleLegs()));
					flightScheduleInfoDTO
							.setFlightScheduleSegments(createFlightScheduleSegments(flightSchedule.getFlightScheduleSegments()));
					flightScheduleInfoDTO.setFrequency(flightSchedule.getFrequency());
					flightScheduleInfoDTO.setFrequencyLocal(flightSchedule.getFrequency());
					flightScheduleInfoDTO.setModelNumber(flightSchedule.getModelNumber());
					flightScheduleInfoDTO.setStartDate(flightSchedule.getStartDate());
					flightScheduleInfoDTO.setStartDateLocal(flightSchedule.getStartDateLocal());
					flightScheduleInfoDTO.setStopDate(flightSchedule.getStopDate());
					flightScheduleInfoDTO.setStopDateLocal(flightSchedule.getStopDateLocal());
					flightScheduleInfoDTO.setStatusCode(flightSchedule.getStatusCode());
					flightScheduleInfoDTO.setVersion(flightSchedule.getVersion());

					colFlightScheduleInfoDTOs.add(flightScheduleInfoDTO);
				}
			} catch (Exception e) {

				throw new CommonsDataAccessException(e, null);
			}
		}
		return colFlightScheduleInfoDTOs;
	}
	
	public Collection<Flight> getAvailableAdhocFlights(ScheduleSearchDTO dto) {

		String flightNumber = dto.getFlightNumber();
		String codeShareFlightNumber = dto.getCsOCFlightNumber();

		if (dto.isEnableWildcard()) {
			boolean isValid = false;

			WildcardFlightSearchDto searchDto = new WildcardFlightSearchDto();

			if (dto.getCsOCCarrierCode() != null && dto.getCsOCFlightNumber() != null) {
				searchDto.setCodeShareFlightNumber(dto.getCsOCFlightNumber());
				searchDto.setCodeShareCarrierCode(dto.getCsOCCarrierCode());
				isValid = true;
			}

			if (isValid) {
				searchDto.setDepartureDate(dto.getStartDate());
				searchDto.setDepartureDateExact(false);
				searchDto.setNonCNXFlight(true);

				try {
					WildcardFlightSearchRespDto wildcardFlightSearchRespDto = AirSchedulesUtil.getFlightBD().searchCodeShareFlightsWildcardBased(searchDto);

					if (wildcardFlightSearchRespDto.getBestMatch() != null) {
						codeShareFlightNumber = wildcardFlightSearchRespDto.getBestMatch().getFlightNumber();
					}

				} catch (ModuleException e) {
					log.error("wild-card search error ---- " + e.getExceptionCode(), e);
				}
			}
		}

		Collection<Flight> colFlights = new ArrayList<Flight>();
		boolean isLocalTime = dto.isLocalTime();
		String searchDateFilter = "";
		String orderby = "";
		if (dto != null && dto.getStartDate() != null && dto.getEndDate() != null) {
			String hql = "select f from Flight as f ";
			if (isLocalTime) {
				hql += ", FlightSegement as fs ";
				searchDateFilter = " f.flightId=fs.flightId and (fs.estTimeDepatureLocal >=:startDate and fs.estTimeDepatureLocal <=:stopDate ) ";
				orderby = " order by f.flightNumber, fs.estTimeDepatureLocal";
			} else {
				searchDateFilter = "( f.departureDate >= :startDate and  f.departureDate <= :stopDate ) ";
				orderby = " order by f.flightNumber, f.departureDate";
			}
			
			String filterByStatus = "";
			if (dto.getStatusFilter() != null && !dto.getStatusFilter().isEmpty()) {
				filterByStatus = " and f.status in (" + Util.buildStringInClauseContent(dto.getStatusFilter()) + ") ";
			}

			String where = "where " + searchDateFilter + filterByStatus
					+ ((dto.getScheduleStatus() != null) ? "and f.status = :statusCode " : "")
					+ ((dto.getOpType() != null) ? "and f.operationTypeId = :operationTypeId " : "")
					+ ((dto.getFlightNumber() != null) ? "and f.flightNumber like :flightNumber " : "")
					+ ((dto.getFromAirport() != null) ? "and f.originAptCode = :fromAirport " : "")
					+ ((dto.getToAirport() != null) ? "and f.destinationAptCode = :toAirport " : "")
					+ ((dto.getCsOCFlightNumber() != null) ? "and f.csOCFlightNumber = :csOCFlightNumber " : "")
					+ ((dto.getCsOCCarrierCode() != null) ? "and f.csOCCarrierCode = :csOCCarrierCode " : "")
					+ ((dto.isAdHocFlightsOnly()) ? "and f.scheduleId is null " : "")
					+ ((dto.getFlightType() != null) ? "and f.flightType = :flightType " : "");

			String hqlSelect = hql + where + orderby;

			try {
				Query select = getSession().createQuery(hqlSelect).setParameter("startDate", dto.getStartDate())
						.setParameter("stopDate", dto.getEndDate());

				if (dto.getScheduleStatus() != null)
					select = select.setParameter("statusCode", dto.getScheduleStatus());

				if (dto.getOpType() != null)
					select = select.setParameter("operationTypeId", dto.getOpType());

				if (dto.getFlightNumber() != null)
					select = select.setParameter("flightNumber", flightNumber);

				if (dto.getFromAirport() != null)
					select = select.setParameter("fromAirport", dto.getFromAirport());
				if (dto.getToAirport() != null)
					select = select.setParameter("toAirport", dto.getToAirport());
				if (dto.getCsOCFlightNumber() != null)
					select = select.setParameter("csOCFlightNumber", codeShareFlightNumber);

				if (dto.getCsOCCarrierCode() != null)
					select = select.setParameter("csOCCarrierCode", dto.getCsOCCarrierCode());
				if (dto.getOpType() != null)
					select = select.setParameter("flightType", dto.getFlightType());

				colFlights = select.list();

			} catch (Exception e) {
				e.printStackTrace();
				throw new CommonsDataAccessException(e, null);
			}
		}
		return colFlights;
	}

	private Set<FlightScheduleLegInfoDTO> createFlightScheduleLegs(Set<FlightScheduleLeg> setFlightScheduleLegs) {

		Set<FlightScheduleLegInfoDTO> setFlightScheduleLegInfoDTOs = new HashSet<FlightScheduleLegInfoDTO>();

		if (setFlightScheduleLegs != null && !setFlightScheduleLegs.isEmpty()) {

			for (FlightScheduleLeg flightScheduleLeg : setFlightScheduleLegs) {
				FlightScheduleLegInfoDTO flightScheduleLegInfoDTO = new FlightScheduleLegInfoDTO();
				flightScheduleLegInfoDTO.setDestination(flightScheduleLeg.getDestination());
				flightScheduleLegInfoDTO.setDuration(flightScheduleLeg.getDuration());
				flightScheduleLegInfoDTO.setEstArrivalDayOffset(flightScheduleLeg.getEstArrivalDayOffset());
				flightScheduleLegInfoDTO.setEstArrivalDayOffsetLocal(flightScheduleLeg.getEstArrivalDayOffsetLocal());
				flightScheduleLegInfoDTO.setEstArrivalTimeLocal(flightScheduleLeg.getEstArrivalTimeLocal());
				flightScheduleLegInfoDTO.setEstArrivalTimeZulu(flightScheduleLeg.getEstArrivalTimeZulu());
				flightScheduleLegInfoDTO.setEstDepartureDayOffset(flightScheduleLeg.getEstDepartureDayOffset());
				flightScheduleLegInfoDTO.setEstDepartureDayOffsetLocal(flightScheduleLeg.getEstDepartureDayOffsetLocal());
				flightScheduleLegInfoDTO.setEstDepartureTimeLocal(flightScheduleLeg.getEstDepartureTimeLocal());
				flightScheduleLegInfoDTO.setEstDepartureTimeZulu(flightScheduleLeg.getEstDepartureTimeZulu());
				flightScheduleLegInfoDTO.setId(flightScheduleLeg.getId());
				flightScheduleLegInfoDTO.setLegNumber(flightScheduleLeg.getLegNumber());
				flightScheduleLegInfoDTO.setOrigin(flightScheduleLeg.getOrigin());

				setFlightScheduleLegInfoDTOs.add(flightScheduleLegInfoDTO);
			}
		}
		return setFlightScheduleLegInfoDTOs;
	}

	private Set<FlightScheduleSegmentInfoDTO> createFlightScheduleSegments(Set<FlightScheduleSegment> setFlightScheduleSegments) {

		Set<FlightScheduleSegmentInfoDTO> setFlightScheduleLegInfoDTOs = new HashSet<FlightScheduleSegmentInfoDTO>();

		if (setFlightScheduleSegments != null && !setFlightScheduleSegments.isEmpty()) {

			for (FlightScheduleSegment flightScheduleSegment : setFlightScheduleSegments) {

				FlightScheduleSegmentInfoDTO flightScheduleSegmentInfoDTO = new FlightScheduleSegmentInfoDTO();
				flightScheduleSegmentInfoDTO.setFlSchSegId(flightScheduleSegment.getFlSchSegId());
				flightScheduleSegmentInfoDTO.setOverlapSegmentId(flightScheduleSegment.getOverlapSegmentId());
				flightScheduleSegmentInfoDTO.setSegmentCode(flightScheduleSegment.getSegmentCode());
				flightScheduleSegmentInfoDTO.setValidFlag(flightScheduleSegment.getValidFlag());

				setFlightScheduleLegInfoDTOs.add(flightScheduleSegmentInfoDTO);
			}
		}
		return setFlightScheduleLegInfoDTOs;
	}

	/**
	 * searchs Flight schedules for display
	 */
	@SuppressWarnings("rawtypes")
	public Page searchFlightSchedulesForDisplay(ScheduleSearchDTO dto, int startIndex, int pageSize,
			Boolean isAdhocFlightDataEnabled) {

		Page page = new Page(0, startIndex, startIndex, new ArrayList());

		if (dto != null && dto.getStartDate() != null && dto.getEndDate() != null) {

			String hql = "select fs from FlightSchedule as fs ";

			String hqlc = "select count(*) from FlightSchedule as fs ";

			String where = "where " + "( ( fs.startDate >= :startDate or fs.stopDate >= :startDate ) "
					+ "and ( fs.stopDate <= :stopDate or fs.startDate <= :stopDate ) ) "
					+ ((dto.getScheduleStatus() != null) ? "and fs.statusCode = :statusCode " : "")
					+ ((dto.getBuildStatus() != null) ? "and fs.buildStatusCode = :buildStatusCode " : "")
					+ ((dto.getOpType() != null) ? "and fs.operationTypeId = :operationTypeId " : "")
					+ ((dto.getFlightNumber() != null) ? "and fs.flightNumber like :flightNumber " : "")
					+ ((dto.getFromAirport() != null) ? "and fs.departureStnCode = :departureStnCode " : "")
					+ ((dto.getToAirport() != null) ? "and fs.arrivalStnCode = :arrivalStnCode " : "")
					+ ((dto.getFlightType() != null) ? "and fs.flightType = :flightType " : "");

			if (dto.getFlightNumers() != null && dto.getFlightNumers().size() > 1) {
				for (int i = 0; i < dto.getFlightNumers().size(); i++) {
					if (i == 0) {
						where += " and ( fs.flightNumber like :flightNumber_" + i;
					} else {
						where += " or fs.flightNumber like :flightNumber_" + i;
					}

				}
				where += " )";
			}

			String orderby = " order by fs.flightNumber, fs.startDate";

			String hqlSelect = hql + where + orderby;
			String hqlCount = hqlc + where;

			try {

				Query select = getSession().createQuery(hqlSelect).setParameter("startDate", dto.getStartDate())
						.setParameter("stopDate", dto.getEndDate());

				if (dto.getScheduleStatus() != null)
					select = select.setParameter("statusCode", dto.getScheduleStatus());
				if (dto.getBuildStatus() != null)
					select = select.setParameter("buildStatusCode", dto.getBuildStatus());
				if (dto.getOpType() != null)
					select = select.setParameter("operationTypeId", dto.getOpType());

				if (dto.getFlightNumber() != null)
					select = select.setParameter("flightNumber", dto.getFlightNumber());

				if (dto.getFromAirport() != null)
					select = select.setParameter("departureStnCode", dto.getFromAirport());

				if (dto.getToAirport() != null)
					select = select.setParameter("arrivalStnCode", dto.getToAirport());

				if (dto.getFlightType() != null)
					select = select.setParameter("flightType", dto.getFlightType());

				if (dto.getFlightNumers() != null && dto.getFlightNumers().size() > 1) {
					for (int i = 0; i < dto.getFlightNumers().size(); i++) {
						select = select.setParameter("flightNumber_" + i, dto.getFlightNumers().get(i));
					}
				}

				Query count = getSession().createQuery(hqlCount).setParameter("startDate", dto.getStartDate())
						.setParameter("stopDate", dto.getEndDate());

				if (dto.getScheduleStatus() != null)
					count = count.setParameter("statusCode", dto.getScheduleStatus());
				if (dto.getBuildStatus() != null)
					count = count.setParameter("buildStatusCode", dto.getBuildStatus());
				if (dto.getOpType() != null)
					count = count.setParameter("operationTypeId", dto.getOpType());

				if (dto.getFlightNumber() != null)
					count = count.setParameter("flightNumber", dto.getFlightNumber());

				if (dto.getFromAirport() != null)
					count = count.setParameter("departureStnCode", dto.getFromAirport());

				if (dto.getToAirport() != null)
					count = count.setParameter("arrivalStnCode", dto.getToAirport());

				if (dto.getFlightType() != null)
					count = count.setParameter("flightType", dto.getFlightType());

				if (dto.getFlightNumers() != null && dto.getFlightNumers().size() > 1) {
					for (int i = 0; i < dto.getFlightNumers().size(); i++) {
						count = count.setParameter("flightNumber_" + i, dto.getFlightNumers().get(i));
					}
				}

				Long total = (Long) count.uniqueResult();

				if (total.intValue() > pageSize && isAdhocFlightDataEnabled)
					pageSize = total.intValue();

				select = select.setFirstResult(startIndex).setMaxResults(pageSize);

				Collection resultList = select.list();

				page = new Page(total.intValue(), startIndex, resultList.size() + startIndex, resultList);

			} catch (Exception e) {

				throw new CommonsDataAccessException(e, null);
			}
		}

		return page;
	}

	public Page searchAdhocFlightsForDisplay(ScheduleSearchDTO dto, int startIndex, int pageSize) {
		Page page = new Page(0, startIndex, startIndex, new ArrayList());

		if (dto != null && dto.getStartDate() != null && dto.getEndDate() != null) {
			String hql = "select f from Flight as f ";
			String hqlc = "select count(*) from Flight as f ";
			String where = "where " + "( f.departureDate >= :startDate and  f.departureDate <= :stopDate ) "
					+ " and f.scheduleId is null " + ((dto.getScheduleStatus() != null) ? "and f.status = :statusCode " : "")
					+ ((dto.getOpType() != null) ? "and f.operationTypeId = :operationTypeId " : "")
					+ ((dto.getFlightNumber() != null) ? "and f.flightNumber like :flightNumber " : "")
					+ ((dto.getFromAirport() != null) ? "and f.originAptCode = :fromAirport " : "")
					+ ((dto.getToAirport() != null) ? "and f.destinationAptCode = :toAirport " : "")
					+ ((dto.getFlightType() != null) ? "and f.flightType = :flightType " : "");

			String orderby = " order by f.flightNumber, f.departureDate";

			String hqlSelect = hql + where + orderby;
			String hqlCount = hqlc + where;

			try {
				Query select = getSession().createQuery(hqlSelect).setParameter("startDate", dto.getStartDate())
						.setParameter("stopDate", dto.getEndDate());

				if (dto.getScheduleStatus() != null)
					select = select.setParameter("statusCode", dto.getScheduleStatus());

				if (dto.getOpType() != null)
					select = select.setParameter("operationTypeId", dto.getOpType());

				if (dto.getFlightNumber() != null)
					select = select.setParameter("flightNumber", dto.getFlightNumber());

				if (dto.getFromAirport() != null)
					select = select.setParameter("fromAirport", dto.getFromAirport());

				if (dto.getToAirport() != null)
					select = select.setParameter("toAirport", dto.getToAirport());

				if (dto.getFlightType() != null)
					select = select.setParameter("flightType", dto.getFlightType());

				Query count = getSession().createQuery(hqlCount).setParameter("startDate", dto.getStartDate())
						.setParameter("stopDate", dto.getEndDate());

				if (dto.getScheduleStatus() != null)
					count = count.setParameter("statusCode", dto.getScheduleStatus());

				if (dto.getOpType() != null)
					count = count.setParameter("operationTypeId", dto.getOpType());

				if (dto.getFlightNumber() != null)
					count = count.setParameter("flightNumber", dto.getFlightNumber());

				if (dto.getFromAirport() != null)
					count = count.setParameter("fromAirport", dto.getFromAirport());

				if (dto.getToAirport() != null)
					count = count.setParameter("toAirport", dto.getToAirport());

				if (dto.getFlightType() != null)
					count = count.setParameter("flightType", dto.getFlightType());

				Long total = (Long) count.uniqueResult();

				if (total.intValue() > pageSize)
					pageSize = total.intValue();

				select = select.setFirstResult(startIndex).setMaxResults(pageSize);

				Collection resultList = select.list();

				page = new Page(total.intValue(), startIndex, resultList.size() + startIndex, resultList);

			} catch (Exception e) {
				throw new CommonsDataAccessException(e, null);
			}
		}
		return page;
	}

	/**
	 * get Flight Schedules to GDS Publish
	 */
	@SuppressWarnings("unchecked")
	public Collection<FlightSchedule> getFlightSchedulesToGDSPublish(Date lastPublishTime, String codeShareCarrier) {
		Collection<FlightSchedule> resultList = null;
		DataSource ds = LookUpUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		if (lastPublishTime != null) {
			String sql = "select (select count(*) from t_flight where modified_date >= ?) + (select count(*) from t_flight_schedule where modified_date >= ?) as total from dual";
			String hql = "select fs from FlightSchedule as fs " + "where (fs.startDate >= sysdate or fs.stopDate >= sysdate)"
					+ " and fs.statusCode = '" + ScheduleStatusEnum.ACTIVE.getCode() + "'" + " and fs.buildStatusCode = '"
					+ ScheduleBuildStatusEnum.BUILT.getCode() + "'";

			if (!codeShareCarrier.equals("ALL")) {
				sql = "select (select count(*) from t_flight f, t_cs_flight csf where f.flight_id = csf.flight_id and f.modified_date >= ? ) +"
						+ " (select count(*) from t_flight_schedule fs, t_cs_flight_schedule csfs where fs.schedule_id = csfs.schedule_id and fs.modified_date >= ?) as total from dual";
				hql = "select fs from FlightSchedule as fs " + "where (fs.startDate >= sysdate or fs.stopDate >= sysdate)"
						+ " and fs.statusCode = '" + ScheduleStatusEnum.ACTIVE.getCode() + "'" + " and fs.buildStatusCode = '"
						+ ScheduleBuildStatusEnum.BUILT.getCode()
						+ "' and fs.codeShareMCFlightSchedules.csMCCarrierCode = :csMCCarrierCode ";
			}

			Object[] params = { lastPublishTime, lastPublishTime };

			Integer count = 0;
			try {
				count = (Integer) templete.query(sql, params, new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						int total = 0;
						while (rs.next()) {
							total = rs.getInt("total");
							break;
						}
						return total;
					}
				});
				if (count > 0) {
					Query query = getSession().createQuery(hql);
					if (!codeShareCarrier.equals("ALL")) {
						query.setParameter("csMCCarrierCode", codeShareCarrier);
					}
					resultList = query.list();
				}
			} catch (Exception e) {
				throw new CommonsDataAccessException(e, null);
			}
		}
		return resultList;
	}

	@SuppressWarnings("unchecked")
	public Collection<FlightSchedule> getFlightSchedulesForSSMRecap(Date startDate, Date endDate, boolean isSelectedRange) {
		Collection<FlightSchedule> resultList = null;
		String hql = null;

		if (isSelectedRange) {
			hql = "select fs from FlightSchedule as fs " + "where (( fs.startDate >= :startDate or fs.stopDate >= :startDate ) "
					+ "and ( fs.stopDate <= :endDate or fs.startDate <= :endDate ))" + " and fs.statusCode = '"
					+ ScheduleStatusEnum.ACTIVE.getCode() + "'" + " and fs.buildStatusCode = '"
					+ ScheduleBuildStatusEnum.BUILT.getCode() + "'";
			resultList = (Collection<FlightSchedule>) getSession().createQuery(hql).setParameter("startDate", startDate)
					.setParameter("endDate", endDate).list();

		} else {
			hql = "select fs from FlightSchedule as fs " + "where (fs.startDate >= sysdate or fs.stopDate <= sysdate)"
					+ " and fs.statusCode = '" + ScheduleStatusEnum.ACTIVE.getCode() + "'" + " and fs.buildStatusCode = '"
					+ ScheduleBuildStatusEnum.BUILT.getCode() + "'";
			resultList = getSession().createQuery(hql).list();
		}

		return resultList;
	}

	/**
	 * searchs Flight numbers for display
	 */
	public Collection<String> getFlightNumbers() {
		return find("select distinct fs.flightNumber from FlightSchedule fs", String.class);
	}

	/**
	 * get no of flights for schedule
	 */
	public int getNoOfFlightsForSchedule(int scheduleId) {

		int noOFFlights = 0;
		try {

			Long count = (Long) getSession().createQuery("select count(*) from Flight as f where f.scheduleId=:scheduleId")
					.setParameter("scheduleId", new Integer(scheduleId)).uniqueResult();
			if (count != null)
				noOFFlights = count.intValue();

		} catch (Exception e) {
			throw new CommonsDataAccessException(e, null);
		}

		return noOFFlights;
	}

	/**
	 * gets flighs for schedule
	 */
	public Collection<Flight> getFlightsForSchedule(int scheduleId) {
		return find("select f from Flight as f where f.scheduleId=?", new Integer(scheduleId), Flight.class);
	}

	/**
	 * gets future flights in a given schedule for a given date
	 */
	@SuppressWarnings("unchecked")
	public Collection<Flight> getFutureFlightsForSchedule(int scheduleId, Date fromDate, Date toDate) {
		Collection<Flight> flights = getFlightsForSchedule(scheduleId);
		Collection<Flight> futureFlights = new ArrayList<Flight>();
		for (Iterator<Flight> iterator = flights.iterator(); iterator.hasNext();) {
			Flight flight = (Flight) iterator.next();
			Date deptDate = flight.getDepartureDate();
			if (flight.getStatus().equals(Flight.FLIGHT_ACTIVE) && deptDate.after(fromDate) && !deptDate.after(toDate)) {
				futureFlights.add(flight);
			}
		}
		return futureFlights;
	}

	/**
	 * gets the possible overlapping schedule ids
	 */
	@SuppressWarnings("unchecked")
	public Collection<FlightSchedule> getPossibleLaterOverlappingSchedules(FlightSchedule schedule) {

		Collection<FlightSchedule> scheds = new ArrayList<FlightSchedule>();
		Set<FlightScheduleLeg> legs = schedule.getFlightScheduleLegs();
		Iterator<FlightScheduleLeg> it = legs.iterator();
		boolean isBuilt = ((schedule.getBuildStatusCode() != null)
				&& (schedule.getBuildStatusCode().equals(ScheduleBuildStatusEnum.BUILT.getCode()))) ? true : false;

		while (it.hasNext()) {

			FlightScheduleLeg leg = (FlightScheduleLeg) it.next();

			Date estDepartureTimeZulu = leg.getEstDepartureTimeZulu();
			Date estArrivalTimeZulu = leg.getEstArrivalTimeZulu();

			Date startDate = CalendarUtil.getOfssetAndTimeAddedDate(schedule.getStartDate(), estDepartureTimeZulu,
					leg.getEstDepartureDayOffset());
			Date stopDate = CalendarUtil.getOfssetAndTimeAddedDate(schedule.getStopDate(), estDepartureTimeZulu,
					leg.getEstDepartureDayOffset());

			int diff = CalendarUtil.daysUntil(schedule.getStartDate(), startDate);
			Frequency frequency = FrequencyUtil.getCopyOfFrequency(schedule.getFrequency());
			frequency = FrequencyUtil.shiftFrequencyBy(frequency, diff);

			try {

				String hql = "select distinct fs " + "from FlightScheduleLeg as fsl, FlightSchedule as fs " + "where "
						+ "fsl.scheduleId = fs.scheduleId and " + "fsl.legNumber = 1 and "
						+ "fsl.estArrivalTimeZulu = :estArrivalTimeZulu and "
						+ "fsl.estDepartureTimeZulu = :estDepartureTimeZulu and " + "fsl.origin = :origin and "
						+ "fsl.destination = :destination and " + "fs.startDate = :startDate and "
						+ "fs.stopDate = :stopDate and " + "fs.statusCode =:statusCode and "
						+ "fs.modelNumber = :modelNumber and " + "( fs.overlapingScheduleId = null "
						+ ((schedule.getScheduleId() != null)
								? (" or fs.overlapingScheduleId = " + schedule.getScheduleId().intValue() + ") and ")
								: ") and ")
						+ "fs.operationTypeId = :operationTypeId and "
						+ ((schedule.getScheduleId() != null)
								? ("fs.scheduleId != " + schedule.getScheduleId().intValue() + " and ")
								: "")
						+ (isBuilt ? ("fs.buildStatusCode = '" + ScheduleBuildStatusEnum.PENDING.getCode() + "' and ") : "")
						+ "fs.frequency.day0 = :day0 and " + "fs.frequency.day1 = :day1 and " + "fs.frequency.day2 = :day2 and "
						+ "fs.frequency.day3 = :day3 and " + "fs.frequency.day4 = :day4 and " + "fs.frequency.day5 = :day5 and "
						+ "fs.frequency.day6 = :day6";

				Collection<FlightSchedule> schedsForLeg = (Collection<FlightSchedule>) getSession().createQuery(hql)
						.setParameter("estArrivalTimeZulu", estArrivalTimeZulu)
						.setParameter("estDepartureTimeZulu", estDepartureTimeZulu).setParameter("origin", leg.getOrigin())
						.setParameter("destination", leg.getDestination()).setParameter("startDate", startDate)
						.setParameter("stopDate", stopDate).setParameter("statusCode", ScheduleStatusEnum.ACTIVE.getCode())
						.setParameter("modelNumber", schedule.getModelNumber())
						.setParameter("operationTypeId", new Integer(schedule.getOperationTypeId()))
						.setParameter("day0", new Boolean(frequency.getDay0()))
						.setParameter("day1", new Boolean(frequency.getDay1()))
						.setParameter("day2", new Boolean(frequency.getDay2()))
						.setParameter("day3", new Boolean(frequency.getDay3()))
						.setParameter("day4", new Boolean(frequency.getDay4()))
						.setParameter("day5", new Boolean(frequency.getDay5()))
						.setParameter("day6", new Boolean(frequency.getDay6())).list();

				scheds.addAll(schedsForLeg);

			} catch (Exception e) {
				throw new CommonsDataAccessException(e, null);
			}
		}
		return scheds;
	}

	/**
	 * gets the possible overlapping schedule ids
	 */
	@SuppressWarnings("unchecked")
	public Collection<FlightSchedule> getPossibleEarliarOverlappingSchedules2(FlightSchedule schedule) {

		Collection<FlightSchedule> scheds = new ArrayList<FlightSchedule>();
		Set<FlightScheduleLeg> legs = schedule.getFlightScheduleLegs();
		Iterator<FlightScheduleLeg> it = legs.iterator();
		boolean isBuilt = ((schedule.getBuildStatusCode() != null)
				&& (schedule.getBuildStatusCode().equals(ScheduleBuildStatusEnum.BUILT.getCode()))) ? true : false;

		while (it.hasNext()) {

			FlightScheduleLeg leg = (FlightScheduleLeg) it.next();

			Date estDepartureTimeZulu = leg.getEstDepartureTimeZulu();
			Date estArrivalTimeZulu = leg.getEstArrivalTimeZulu();

			Date shStDate = schedule.getStartDate();
			Date shEdDate = schedule.getStopDate();
			Frequency shFqn = schedule.getFrequency();

			Date startDate0 = CalendarUtil.getOfssetAndTimeAddedDate(shStDate, estDepartureTimeZulu, 0);
			Date stopDate0 = CalendarUtil.getOfssetAndTimeAddedDate(shEdDate, estDepartureTimeZulu, 0);
			Frequency frequency0 = FrequencyUtil.getCopyOfFrequency(shFqn);
			frequency0 = FrequencyUtil.shiftFrequencyBy(frequency0, 0);

			Date startDate1 = CalendarUtil.getOfssetAndTimeAddedDate(shStDate, estDepartureTimeZulu, 1);
			Date stopDate1 = CalendarUtil.getOfssetAndTimeAddedDate(shEdDate, estDepartureTimeZulu, 1);
			Frequency frequency1 = FrequencyUtil.getCopyOfFrequency(shFqn);
			frequency1 = FrequencyUtil.shiftFrequencyBy(frequency1, 1);

			Date startDate2 = CalendarUtil.getOfssetAndTimeAddedDate(shStDate, estDepartureTimeZulu, 2);
			Date stopDate2 = CalendarUtil.getOfssetAndTimeAddedDate(shEdDate, estDepartureTimeZulu, 2);
			Frequency frequency2 = FrequencyUtil.getCopyOfFrequency(shFqn);
			frequency2 = FrequencyUtil.shiftFrequencyBy(frequency2, 2);

			Date startDate3 = CalendarUtil.getOfssetAndTimeAddedDate(shStDate, estDepartureTimeZulu, 3);
			Date stopDate3 = CalendarUtil.getOfssetAndTimeAddedDate(shEdDate, estDepartureTimeZulu, 3);
			Frequency frequency3 = FrequencyUtil.getCopyOfFrequency(shFqn);
			frequency3 = FrequencyUtil.shiftFrequencyBy(frequency3, 3);

			Date startDate4 = CalendarUtil.getOfssetAndTimeAddedDate(shStDate, estDepartureTimeZulu, 4);
			Date stopDate4 = CalendarUtil.getOfssetAndTimeAddedDate(shEdDate, estDepartureTimeZulu, 4);
			Frequency frequency4 = FrequencyUtil.getCopyOfFrequency(shFqn);
			frequency4 = FrequencyUtil.shiftFrequencyBy(frequency4, 4);

			Date startDate5 = CalendarUtil.getOfssetAndTimeAddedDate(shStDate, estDepartureTimeZulu, 5);
			Date stopDate5 = CalendarUtil.getOfssetAndTimeAddedDate(shEdDate, estDepartureTimeZulu, 5);
			Frequency frequency5 = FrequencyUtil.getCopyOfFrequency(shFqn);
			frequency5 = FrequencyUtil.shiftFrequencyBy(frequency5, 5);

			try {

				String hql = "select distinct fs " + "from FlightScheduleLeg as fsl, FlightSchedule as fs " + "where "
						+ "fsl.scheduleId = fs.scheduleId and " + "fsl.estArrivalTimeZulu = :estArrivalTimeZulu and "
						+ "fsl.estDepartureTimeZulu = :estDepartureTimeZulu and " + "fsl.origin = :origin and "
						+ "fsl.destination = :destination and " + "fs.departureStnCode = :departureStnCode and "
						+ "fs.statusCode =:statusCode and " + "fs.modelNumber = :modelNumber and "
						+ "fs.operationTypeId = :operationTypeId and "
						+ ((schedule.getScheduleId() != null)
								? ("fs.scheduleId != " + schedule.getScheduleId().intValue() + " and ")
								: "")
						+ (isBuilt ? ("fs.buildStatusCode = '" + ScheduleBuildStatusEnum.PENDING.getCode() + "' and ") : "") + "("
						+ "(fs.startDate = :startDate0 and " + "fs.stopDate = :stopDate0 and " + "fs.frequency.day0 = :day00 and "
						+ "fs.frequency.day1 = :day01 and " + "fs.frequency.day2 = :day02 and "
						+ "fs.frequency.day3 = :day03 and " + "fs.frequency.day4 = :day04 and "
						+ "fs.frequency.day5 = :day05 and " + "fs.frequency.day6 = :day06 ) or " +

						"(fs.startDate = :startDate1 and " + "fs.stopDate = :stopDate1 and " + "fs.frequency.day0 = :day10 and "
						+ "fs.frequency.day1 = :day11 and " + "fs.frequency.day2 = :day12 and "
						+ "fs.frequency.day3 = :day13 and " + "fs.frequency.day4 = :day14 and "
						+ "fs.frequency.day5 = :day15 and " + "fs.frequency.day6 = :day16 ) or " +

						"(fs.startDate = :startDate2 and " + "fs.stopDate = :stopDate2 and " + "fs.frequency.day0 = :day20 and "
						+ "fs.frequency.day1 = :day21 and " + "fs.frequency.day2 = :day22 and "
						+ "fs.frequency.day3 = :day23 and " + "fs.frequency.day4 = :day24 and "
						+ "fs.frequency.day5 = :day25 and " + "fs.frequency.day6 = :day26 ) or " +

						"(fs.startDate = :startDate3 and " + "fs.stopDate = :stopDate3 and " + "fs.frequency.day0 = :day30 and "
						+ "fs.frequency.day1 = :day31 and " + "fs.frequency.day2 = :day32 and "
						+ "fs.frequency.day3 = :day33 and " + "fs.frequency.day4 = :day34 and "
						+ "fs.frequency.day5 = :day35 and " + "fs.frequency.day6 = :day36 ) or " +

						"(fs.startDate = :startDate4 and " + "fs.stopDate = :stopDate4 and " + "fs.frequency.day0 = :day40 and "
						+ "fs.frequency.day1 = :day41 and " + "fs.frequency.day2 = :day42 and "
						+ "fs.frequency.day3 = :day43 and " + "fs.frequency.day4 = :day44 and "
						+ "fs.frequency.day5 = :day45 and " + "fs.frequency.day6 = :day46 ) or " +

						"(fs.startDate = :startDate5 and " + "fs.stopDate = :stopDate5 and " + "fs.frequency.day0 = :day50 and "
						+ "fs.frequency.day1 = :day51 and " + "fs.frequency.day2 = :day52 and "
						+ "fs.frequency.day3 = :day53 and " + "fs.frequency.day4 = :day54 and "
						+ "fs.frequency.day5 = :day55 and " + "fs.frequency.day6 = :day56 )" +

						")";

				Collection<FlightSchedule> schedsForLeg = (Collection<FlightSchedule>) getSession().createQuery(hql)
						.setParameter("estArrivalTimeZulu", estArrivalTimeZulu)
						.setParameter("estDepartureTimeZulu", estDepartureTimeZulu).setParameter("origin", leg.getOrigin())
						.setParameter("destination", leg.getDestination()).setParameter("departureStnCode", leg.getDestination())
						.setParameter("statusCode", ScheduleStatusEnum.ACTIVE.getCode())
						.setParameter("modelNumber", schedule.getModelNumber())
						.setParameter("operationTypeId", new Integer(schedule.getOperationTypeId()))

						.setParameter("startDate0", startDate0).setParameter("stopDate0", stopDate0)
						.setParameter("day00", new Boolean(frequency0.getDay0()))
						.setParameter("day01", new Boolean(frequency0.getDay1()))
						.setParameter("day02", new Boolean(frequency0.getDay2()))
						.setParameter("day03", new Boolean(frequency0.getDay3()))
						.setParameter("day04", new Boolean(frequency0.getDay4()))
						.setParameter("day05", new Boolean(frequency0.getDay5()))
						.setParameter("day06", new Boolean(frequency0.getDay6()))

						.setParameter("startDate1", startDate1).setParameter("stopDate1", stopDate1)
						.setParameter("day10", new Boolean(frequency1.getDay0()))
						.setParameter("day11", new Boolean(frequency1.getDay1()))
						.setParameter("day12", new Boolean(frequency1.getDay2()))
						.setParameter("day13", new Boolean(frequency1.getDay3()))
						.setParameter("day14", new Boolean(frequency1.getDay4()))
						.setParameter("day15", new Boolean(frequency1.getDay5()))
						.setParameter("day16", new Boolean(frequency1.getDay6()))

						.setParameter("startDate2", startDate2).setParameter("stopDate2", stopDate2)
						.setParameter("day20", new Boolean(frequency2.getDay0()))
						.setParameter("day21", new Boolean(frequency2.getDay1()))
						.setParameter("day22", new Boolean(frequency2.getDay2()))
						.setParameter("day23", new Boolean(frequency2.getDay3()))
						.setParameter("day24", new Boolean(frequency2.getDay4()))
						.setParameter("day25", new Boolean(frequency2.getDay5()))
						.setParameter("day26", new Boolean(frequency2.getDay6()))

						.setParameter("startDate3", startDate3).setParameter("stopDate3", stopDate3)
						.setParameter("day30", new Boolean(frequency3.getDay0()))
						.setParameter("day31", new Boolean(frequency3.getDay1()))
						.setParameter("day32", new Boolean(frequency3.getDay2()))
						.setParameter("day33", new Boolean(frequency3.getDay3()))
						.setParameter("day34", new Boolean(frequency3.getDay4()))
						.setParameter("day35", new Boolean(frequency3.getDay5()))
						.setParameter("day36", new Boolean(frequency3.getDay6()))

						.setParameter("startDate4", startDate4).setParameter("stopDate4", stopDate4)
						.setParameter("day40", new Boolean(frequency4.getDay0()))
						.setParameter("day41", new Boolean(frequency4.getDay1()))
						.setParameter("day42", new Boolean(frequency4.getDay2()))
						.setParameter("day43", new Boolean(frequency4.getDay3()))
						.setParameter("day44", new Boolean(frequency4.getDay4()))
						.setParameter("day45", new Boolean(frequency4.getDay5()))
						.setParameter("day46", new Boolean(frequency4.getDay6()))

						.setParameter("startDate5", startDate5).setParameter("stopDate5", stopDate5)
						.setParameter("day50", new Boolean(frequency5.getDay0()))
						.setParameter("day51", new Boolean(frequency5.getDay1()))
						.setParameter("day52", new Boolean(frequency5.getDay2()))
						.setParameter("day53", new Boolean(frequency5.getDay3()))
						.setParameter("day54", new Boolean(frequency5.getDay4()))
						.setParameter("day55", new Boolean(frequency5.getDay5()))
						.setParameter("day56", new Boolean(frequency5.getDay6()))

						.list();

				scheds.addAll(schedsForLeg);

			} catch (Exception e) {
				throw new CommonsDataAccessException(e, null);
			}
		}
		return scheds;
	}

	/**
	 * gets the possible overlapping schedule ids
	 */
	@SuppressWarnings("unchecked")
	public Collection<FlightSchedule> getPossibleEarliarOverlappingSchedules(FlightSchedule schedule) {

		Collection<FlightSchedule> scheds = new ArrayList<FlightSchedule>();
		Set<FlightScheduleLeg> legs = schedule.getFlightScheduleLegs();
		Iterator<FlightScheduleLeg> it = legs.iterator();
		boolean isBuilt = ((schedule.getBuildStatusCode() != null)
				&& (schedule.getBuildStatusCode().equals(ScheduleBuildStatusEnum.BUILT.getCode()))) ? true : false;

		while (it.hasNext()) {

			FlightScheduleLeg leg = (FlightScheduleLeg) it.next();

			Date estDepartureTimeZulu = leg.getEstDepartureTimeZulu();
			Date estArrivalTimeZulu = leg.getEstArrivalTimeZulu();

			Date shStDate = schedule.getStartDate();
			Date shEdDate = schedule.getStopDate();
			Frequency shFqn = schedule.getFrequency();

			Date startDate = CalendarUtil.getOfssetAndTimeAddedDate(shStDate, estDepartureTimeZulu, -6);
			Date stopDate = CalendarUtil.getOfssetAndTimeAddedDate(shEdDate, estDepartureTimeZulu, 0);

			Frequency frequency0 = FrequencyUtil.getCopyOfFrequency(shFqn);
			frequency0 = FrequencyUtil.shiftFrequencyBy(frequency0, 0);

			Frequency frequency1 = FrequencyUtil.getCopyOfFrequency(shFqn);
			frequency1 = FrequencyUtil.shiftFrequencyBy(frequency1, -1);

			Frequency frequency2 = FrequencyUtil.getCopyOfFrequency(shFqn);
			frequency2 = FrequencyUtil.shiftFrequencyBy(frequency2, -2);
			;
			Frequency frequency3 = FrequencyUtil.getCopyOfFrequency(shFqn);
			frequency3 = FrequencyUtil.shiftFrequencyBy(frequency3, -3);

			Frequency frequency4 = FrequencyUtil.getCopyOfFrequency(shFqn);
			frequency4 = FrequencyUtil.shiftFrequencyBy(frequency4, -4);

			Frequency frequency5 = FrequencyUtil.getCopyOfFrequency(shFqn);
			frequency5 = FrequencyUtil.shiftFrequencyBy(frequency5, -5);

			try {

				String hql = "select distinct fs " + "from FlightScheduleLeg as fsl, FlightSchedule as fs " + "where "
						+ "fsl.scheduleId = fs.scheduleId and " + "fsl.estArrivalTimeZulu = :estArrivalTimeZulu and "
						+ "fsl.estDepartureTimeZulu = :estDepartureTimeZulu and " + "fsl.origin = :origin and "
						+ "fsl.destination = :destination and " + "fs.arrivalStnCode = :arrivalStnCode and "
						+ "fs.statusCode =:statusCode and " + "fs.modelNumber = :modelNumber and "
						+ "( fs.overlapingScheduleId = null "
						+ ((schedule.getScheduleId() != null)
								? (" or fs.overlapingScheduleId = " + schedule.getScheduleId().intValue() + ") and ")
								: ") and ")
						+ "fs.operationTypeId = :operationTypeId and " + "fs.startDate >=:startDate and "
						+ "fs.stopDate <= :stopDate and "
						+ ((schedule.getScheduleId() != null)
								? ("fs.scheduleId != " + schedule.getScheduleId().intValue() + " and ")
								: "")
						+ (isBuilt ? ("fs.buildStatusCode = '" + ScheduleBuildStatusEnum.PENDING.getCode() + "' and ") : "") + "("
						+ "( fs.frequency.day0 = :day00 and " + "fs.frequency.day1 = :day01 and "
						+ "fs.frequency.day2 = :day02 and " + "fs.frequency.day3 = :day03 and "
						+ "fs.frequency.day4 = :day04 and " + "fs.frequency.day5 = :day05 and "
						+ "fs.frequency.day6 = :day06 ) or " +

						"( fs.frequency.day0 = :day10 and " + "fs.frequency.day1 = :day11 and "
						+ "fs.frequency.day2 = :day12 and " + "fs.frequency.day3 = :day13 and "
						+ "fs.frequency.day4 = :day14 and " + "fs.frequency.day5 = :day15 and "
						+ "fs.frequency.day6 = :day16 ) or " +

						"( fs.frequency.day0 = :day20 and " + "fs.frequency.day1 = :day21 and "
						+ "fs.frequency.day2 = :day22 and " + "fs.frequency.day3 = :day23 and "
						+ "fs.frequency.day4 = :day24 and " + "fs.frequency.day5 = :day25 and "
						+ "fs.frequency.day6 = :day26 ) or " +

						"( fs.frequency.day0 = :day30 and " + "fs.frequency.day1 = :day31 and "
						+ "fs.frequency.day2 = :day32 and " + "fs.frequency.day3 = :day33 and "
						+ "fs.frequency.day4 = :day34 and " + "fs.frequency.day5 = :day35 and "
						+ "fs.frequency.day6 = :day36 ) or " +

						"( fs.frequency.day0 = :day40 and " + "fs.frequency.day1 = :day41 and "
						+ "fs.frequency.day2 = :day42 and " + "fs.frequency.day3 = :day43 and "
						+ "fs.frequency.day4 = :day44 and " + "fs.frequency.day5 = :day45 and "
						+ "fs.frequency.day6 = :day46 ) or " +

						"( fs.frequency.day0 = :day50 and " + "fs.frequency.day1 = :day51 and "
						+ "fs.frequency.day2 = :day52 and " + "fs.frequency.day3 = :day53 and "
						+ "fs.frequency.day4 = :day54 and " + "fs.frequency.day5 = :day55 and " + "fs.frequency.day6 = :day56 )" +

						")";

				Collection<FlightSchedule> schedsForLeg = (Collection<FlightSchedule>) getSession().createQuery(hql)
						.setParameter("estArrivalTimeZulu", estArrivalTimeZulu)
						.setParameter("estDepartureTimeZulu", estDepartureTimeZulu).setParameter("origin", leg.getOrigin())
						.setParameter("destination", leg.getDestination()).setParameter("arrivalStnCode", leg.getDestination())
						.setParameter("statusCode", ScheduleStatusEnum.ACTIVE.getCode())
						.setParameter("modelNumber", schedule.getModelNumber())
						.setParameter("operationTypeId", new Integer(schedule.getOperationTypeId()))
						.setParameter("startDate", startDate).setParameter("stopDate", stopDate)

						.setParameter("day00", new Boolean(frequency0.getDay0()))
						.setParameter("day01", new Boolean(frequency0.getDay1()))
						.setParameter("day02", new Boolean(frequency0.getDay2()))
						.setParameter("day03", new Boolean(frequency0.getDay3()))
						.setParameter("day04", new Boolean(frequency0.getDay4()))
						.setParameter("day05", new Boolean(frequency0.getDay5()))
						.setParameter("day06", new Boolean(frequency0.getDay6()))

						.setParameter("day10", new Boolean(frequency1.getDay0()))
						.setParameter("day11", new Boolean(frequency1.getDay1()))
						.setParameter("day12", new Boolean(frequency1.getDay2()))
						.setParameter("day13", new Boolean(frequency1.getDay3()))
						.setParameter("day14", new Boolean(frequency1.getDay4()))
						.setParameter("day15", new Boolean(frequency1.getDay5()))
						.setParameter("day16", new Boolean(frequency1.getDay6()))

						.setParameter("day20", new Boolean(frequency2.getDay0()))
						.setParameter("day21", new Boolean(frequency2.getDay1()))
						.setParameter("day22", new Boolean(frequency2.getDay2()))
						.setParameter("day23", new Boolean(frequency2.getDay3()))
						.setParameter("day24", new Boolean(frequency2.getDay4()))
						.setParameter("day25", new Boolean(frequency2.getDay5()))
						.setParameter("day26", new Boolean(frequency2.getDay6()))

						.setParameter("day30", new Boolean(frequency3.getDay0()))
						.setParameter("day31", new Boolean(frequency3.getDay1()))
						.setParameter("day32", new Boolean(frequency3.getDay2()))
						.setParameter("day33", new Boolean(frequency3.getDay3()))
						.setParameter("day34", new Boolean(frequency3.getDay4()))
						.setParameter("day35", new Boolean(frequency3.getDay5()))
						.setParameter("day36", new Boolean(frequency3.getDay6()))

						.setParameter("day40", new Boolean(frequency4.getDay0()))
						.setParameter("day41", new Boolean(frequency4.getDay1()))
						.setParameter("day42", new Boolean(frequency4.getDay2()))
						.setParameter("day43", new Boolean(frequency4.getDay3()))
						.setParameter("day44", new Boolean(frequency4.getDay4()))
						.setParameter("day45", new Boolean(frequency4.getDay5()))
						.setParameter("day46", new Boolean(frequency4.getDay6()))

						.setParameter("day50", new Boolean(frequency5.getDay0()))
						.setParameter("day51", new Boolean(frequency5.getDay1()))
						.setParameter("day52", new Boolean(frequency5.getDay2()))
						.setParameter("day53", new Boolean(frequency5.getDay3()))
						.setParameter("day54", new Boolean(frequency5.getDay4()))
						.setParameter("day55", new Boolean(frequency5.getDay5()))
						.setParameter("day56", new Boolean(frequency5.getDay6()))

						.list();

				scheds.addAll(schedsForLeg);

			} catch (Exception e) {
				throw new CommonsDataAccessException(e, null);
			}
		}
		return scheds;
	}

	/**
	 * get the total ASK for the passed schedule
	 */
	public int getTotalASKForSchedule(int scheduleId) {
		try {
			return ((Long) getSession().createQuery("select sum(f.availableSeatKilometers) from Flight as f where f.scheduleId = "
					+ scheduleId + " and f.status != '" + FlightStatusEnum.CANCELLED.getCode() + "'").uniqueResult()).intValue();
		} catch (Exception e) {

			throw new CommonsDataAccessException(e, null);
		}
	}

	/**
	 * method to get aircraft types for given schedule ids
	 */
	public HashMap<Integer, String> getAircraftTypesForSchedules(Collection<Integer> scheduleIds) {

		DataSource ds = LookUpUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		final HashMap<Integer, String> aircraftMap = new HashMap<Integer, String>();

		if (scheduleIds != null && scheduleIds.size() > 0) {

			String inValues = "";
			Iterator<Integer> itShcedIds = scheduleIds.iterator();
			while (itShcedIds.hasNext()) {
				Integer schedId = (Integer) itShcedIds.next();
				inValues += "," + schedId;
			}

			inValues = inValues.substring(1);

			String sql = "SELECT a.schedule_id, b.iata_aircraft_type " + "FROM t_flight_schedule a, t_aircraft_model b "
					+ "WHERE a.model_number = b.model_number and a.schedule_id in (" + inValues + ")";

			templete.query(sql, new ResultSetExtractor() {
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					while (rs.next()) {
						Integer id = rs.getInt("schedule_id");
						String aircraftType = rs.getString("iata_aircraft_type");
						aircraftMap.put(id, aircraftType);
					}

					return null;
				}
			});

		}
		return aircraftMap;
	}

	/**
	 * method to get Schedules for terminal
	 */
	public List<Integer> getSchedulesForTerminal(Integer terminalId) {

		List<Integer> list = null;
		try {

			String hql = "select flSchSegId from FlightScheduleSegment where " + " departureTerminalId = '" + terminalId + "' or "
					+ " arrivalTerminalId = '" + terminalId + "'";

			list = find(hql, Integer.class);

		} catch (Exception e) {

			throw new CommonsDataAccessException(e, null);
		}

		return list;
	}

	@Override
	public List<FlightSchedule> getFlightScheduleForModel(String modelNumber) {
		List<FlightSchedule> list = null;

		try {

			String hql = "select f " + "from FlightSchedule as f " + "where "
					+ "f.modelNumber = :modelNumber and f.stopDate > :currentDate ";

			Date cuurentDate = new Date(System.currentTimeMillis());
			Query q = getSession().createQuery(hql).setParameter("modelNumber", modelNumber);
			if (cuurentDate != null) {
				q.setTimestamp("currentDate", cuurentDate);
			}
			list = q.list();

		} catch (Exception e) {

			throw new CommonsDataAccessException(e, null);
		}

		return list;
	}

	@Override
	public Map<Integer, Integer> insertSpliitedSchedules(String modelNumber) {

		Collection<FlightSchedule> list = null;
		Map<Integer, Integer> scheduleIds = new HashMap<Integer, Integer>();

		try {

			String hql = "select f " + "from FlightSchedule as f " + "where "
					+ "f.modelNumber = :modelNumber and f.stopDate > :currentDate and f.startDate < :currentDate";

			Date cuurentDate = new Date(System.currentTimeMillis());
			Query q = getSession().createQuery(hql).setParameter("modelNumber", modelNumber);
			if (cuurentDate != null) {
				q.setTimestamp("currentDate", cuurentDate);
			}
			list = q.list();
			Collection<FlightSchedule> newScheduleList = new ArrayList<FlightSchedule>();
			for (FlightSchedule schedule : list) {
				FlightSchedule newScheduleFromSplit = ScheduledFlightUtil.copyOnlyScheduleAttributes(schedule,
						new FlightSchedule(), true);
				newScheduleFromSplit.setModifiedDate(cuurentDate);
				newScheduleFromSplit.setStartDate(cuurentDate);
				newScheduleFromSplit.setStartDateLocal(cuurentDate); // set the correct local time

				// setting flight schedule legs
				Set<FlightScheduleLeg> scheduleLegs = schedule.getFlightScheduleLegs();
				Iterator<FlightScheduleLeg> legsIt = scheduleLegs.iterator();

				Set<FlightScheduleLeg> copedLegSet = new HashSet<FlightScheduleLeg>();
				FlightScheduleLeg leg;
				FlightScheduleLeg legCopy;

				while (legsIt.hasNext()) {

					leg = (FlightScheduleLeg) legsIt.next();
					legCopy = new FlightScheduleLeg();
					legCopy = LegUtil.copyLeg(leg, legCopy);
					copedLegSet.add(legCopy);
				}

				// setting flight schedule segments
				Set<FlightScheduleSegment> scheduleSegments = schedule.getFlightScheduleSegments();
				Iterator<FlightScheduleSegment> segmentIt = scheduleSegments.iterator();

				Set<FlightScheduleSegment> copiedSegmentSet = new HashSet<FlightScheduleSegment>();
				FlightScheduleSegment segment;
				FlightScheduleSegment segmentCopy;

				while (segmentIt.hasNext()) {

					segment = (FlightScheduleSegment) segmentIt.next();
					segmentCopy = new FlightScheduleSegment();
					segmentCopy = SegmentUtil.copySegment(segment, segmentCopy);
					copiedSegmentSet.add(segmentCopy);
				}

				newScheduleFromSplit.setFlightScheduleSegments(copiedSegmentSet);
				newScheduleFromSplit.setFlightScheduleLegs(copedLegSet);
				ScheduledFlightUtil.copyCodeShareMCSchedule(schedule, newScheduleFromSplit);

				getSession().saveOrUpdate(newScheduleFromSplit);
				getSession().flush();
				scheduleIds.put(schedule.getScheduleId(), newScheduleFromSplit.getScheduleId());

			}

		} catch (Exception e) {

			throw new CommonsDataAccessException(e, null);
		}
		return scheduleIds;
	}

	@Override
	public void updatedExistingSchedules(String modelNumber, String newModuleNumber, Map<Integer, Integer> scheduleIds) {
		try {
			String hqlUpdate = "update FlightSchedule set modelNumber =:newModuleNumber,stopDate=:currentDate " + "where "
					+ "modelNumber  = :modelNumber and stopDate >:currentDate and scheduleId in ("
					+ Util.buildIntegerInClauseContent(scheduleIds.keySet()) + ")";
			Date cuurentDate = new Date(System.currentTimeMillis());
			getSession().createQuery(hqlUpdate).setParameter("modelNumber", modelNumber)
					.setParameter("newModuleNumber", newModuleNumber).setParameter("currentDate", cuurentDate).executeUpdate();

		} catch (Exception e) {
			throw new CommonsDataAccessException(e, null);
		}
	}

	/**
	 * Find flights which status are equal to the old status
	 */
	@Override
	public Collection<Flight> getFlightsForSchedule(int scheduleId, FlightStatusEnum status) {

		FlightStatusEnum oldStatus = (status.getCode().equals(FlightStatusEnum.ACTIVE.getCode()))
				? FlightStatusEnum.CLOSED
				: FlightStatusEnum.ACTIVE;

		return find("select f from Flight as f where f.scheduleId=? and f.status = '" + oldStatus.getCode() + "' "
				+ "and f.departureDate > sysdate", new Integer(scheduleId), Flight.class);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Collection<FlightSchedule> getDSTApplicableFutureSchedules(AirportDSTHistory airportDST) {

		StringBuilder sbSql = new StringBuilder();
		String airportCode = airportDST.getAirportCode();

		sbSql.append("SELECT FS.SCHEDULE_ID ");
		sbSql.append("FROM T_FLIGHT_SCHEDULE FS,T_FLIGHT_SCHEDULE_LEG FSLEG ");
		sbSql.append("WHERE FSLEG.SCHEDULE_ID=FS.SCHEDULE_ID ");
		sbSql.append(
				"AND (((FS.START_DATE >=TO_DATE(" + CalendarUtil.formatForSQL_toDate(airportDST.getDstStartDateTime()) + ") ");
		sbSql.append("OR FS.STOP_DATE >=TO_DATE(" + CalendarUtil.formatForSQL_toDate(airportDST.getDstStartDateTime()) + ")) ");
		sbSql.append("AND (FS.STOP_DATE <=TO_DATE(" + CalendarUtil.formatForSQL_toDate(airportDST.getDstEndDateTime()) + ") ");
		sbSql.append("OR FS.START_DATE <=TO_DATE(" + CalendarUtil.formatForSQL_toDate(airportDST.getDstEndDateTime()) + ")) ) ");
		sbSql.append(
				"OR ((FS.START_DATE >=TO_DATE(" + CalendarUtil.formatForSQL_toDate(airportDST.getDstStartDateTimeOld()) + ") ");
		sbSql.append(
				"OR FS.STOP_DATE >=TO_DATE(" + CalendarUtil.formatForSQL_toDate(airportDST.getDstStartDateTimeOld()) + ")) ");
		sbSql.append("AND (FS.STOP_DATE <=TO_DATE(" + CalendarUtil.formatForSQL_toDate(airportDST.getDstEndDateTimeOld()) + ") ");
		sbSql.append(
				"OR FS.START_DATE <=TO_DATE(" + CalendarUtil.formatForSQL_toDate(airportDST.getDstEndDateTimeOld()) + ")))) ");
		sbSql.append(" AND FS.SCHEDULE_STATUS_CODE= ? AND FS.STOP_DATE >=SYSDATE ");
		sbSql.append(" AND (FSLEG.ORIGIN=? OR FSLEG.DESTINATION=? ) ");

		DataSource ds = LookUpUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		Object[] params = { ScheduleStatusEnum.ACTIVE.getCode(), airportCode, airportCode };

		List<Integer> scheduleIds = (List<Integer>) templete.query(sbSql.toString(), params, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<Integer> scheduleIds = new ArrayList<Integer>();
				while (rs.next()) {
					scheduleIds.add(rs.getInt("SCHEDULE_ID"));
				}
				return scheduleIds;
			}
		});

		Collection<FlightSchedule> resultList = getFlightSchedules(scheduleIds);
		return resultList;
	}

	@SuppressWarnings({ "unchecked" })
	public Collection<FlightSchedule> getFlightSchedules(Collection<Integer> scheduleIds) {
		Collection<FlightSchedule> resultList = null;
		if (scheduleIds != null && !scheduleIds.isEmpty()) {
			String hql = "select fs from FlightSchedule as fs where fs.scheduleId in ("
					+ Util.buildIntegerInClauseContent(scheduleIds) + ") ";
			resultList = getSession().createQuery(hql).list();
		}
		return resultList;
	}
	
	public void updateDelayCancelFlightSchedule(Collection<Integer> scheduleIds, boolean status, Date holdTill, String remarks){
		try {

			if (scheduleIds != null && !scheduleIds.isEmpty()) {
				String hqlUpdate = "update FlightSchedule set cancelMsgWaiting =:cancelMsgWaiting,holdCancelMsgTill=:holdCancelMsgTill, "
						+ "remarks=:remarks where " + " scheduleId in (" + Util.buildIntegerInClauseContent(scheduleIds) + ")";

				getSession().createQuery(hqlUpdate).setParameter("cancelMsgWaiting", status)
						.setParameter("holdCancelMsgTill", holdTill)
						.setParameter("remarks", remarks).executeUpdate();
			}

		} catch (Exception e) {
			throw new CommonsDataAccessException(e, null);
		}
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Integer> getCancelledDelayedSchedulesId(Date fromDateTime) {

		StringBuilder sbSql = new StringBuilder();

		sbSql.append("SELECT SCHEDULE_ID FROM T_FLIGHT_SCHEDULE WHERE CNL_MSG_WAITING=? ");
		sbSql.append("AND HOLD_CNL_MSG_TILL < TO_DATE(" + CalendarUtil.formatForSQL_toDate(fromDateTime) + ") ");
		sbSql.append("AND SCHEDULE_STATUS_CODE <>? ");

		DataSource ds = LookUpUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		Object[] params = { "Y", ScheduleStatusEnum.CANCELLED.getCode() };

		List<Integer> scheduleIds = (List<Integer>) templete.query(sbSql.toString(), params, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<Integer> scheduleIds = new ArrayList<Integer>();
				while (rs.next()) {
					scheduleIds.add(rs.getInt("SCHEDULE_ID"));
				}
				return scheduleIds;
			}
		});

		return scheduleIds;
	}

	@Override
	@SuppressWarnings({ "unchecked" })
	public Collection<FlightSchedule> getPossibleFlightDesignatorChange(Date startDate, Date stopDate, String segmentCode,
			boolean isLocalTime) {
		StringBuilder sbSql = new StringBuilder();
		Collection<FlightSchedule> resultList = null;
		sbSql.append(
				"SELECT S.SCHEDULE_ID AS SCHEDULE_ID FROM T_FLIGHT_SCHEDULE S,T_FLIGHT_SCHEDULE_SEGMENT FS, T_FLIGHT_SCHEDULE_LEG SL ");
		sbSql.append("WHERE S.SCHEDULE_ID = FS.SCHEDULE_ID AND S.SCHEDULE_ID=SL.SCHEDULE_ID ");
		if (isLocalTime) {
			sbSql.append("AND (TRUNC(S.START_DATE_LOCAL) >=? OR TRUNC(S.STOP_DATE_LOCAL) <=?  ) ");
		} else {
			sbSql.append("AND (TRUNC(S.START_DATE) >=? OR TRUNC(S.STOP_DATE) <=? ) ");
		}

		sbSql.append("AND FS.SEGMENT_CODE =? ");
		sbSql.append("AND S.SCHEDULE_STATUS_CODE !=? ");
		sbSql.append("AND S.CNL_MSG_WAITING ='Y' ");
		sbSql.append("AND FS.SEGMENT_VALID_FLAG =? ");

		String periodFrom = CalendarUtil.formatSQLDate(startDate);
		String periodTo = CalendarUtil.formatSQLDate(stopDate);

		DataSource ds = LookUpUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		Object[] params = { periodFrom, periodTo, segmentCode, ScheduleStatusEnum.CANCELLED.getCode(), "Y" };

		List<Integer> scheduleIds = (List<Integer>) templete.query(sbSql.toString(), params, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<Integer> scheduleIds = new ArrayList<Integer>();
				while (rs.next()) {
					scheduleIds.add(rs.getInt("SCHEDULE_ID"));
				}
				return scheduleIds;
			}
		});

		if (scheduleIds != null && !scheduleIds.isEmpty()) {
			resultList = getFlightSchedules(scheduleIds);
		}

		return resultList;
	}
}
