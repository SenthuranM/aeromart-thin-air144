/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.operation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.service.FlightInventoryBD;
import com.isa.thinair.airmaster.api.model.AircraftCabinCapacity;
import com.isa.thinair.airmaster.api.model.AircraftModel;
import com.isa.thinair.airmaster.api.service.AircraftBD;
import com.isa.thinair.airreservation.api.dto.FlightChangeInfo;
import com.isa.thinair.airreservation.api.dto.FlightReservationAlertDTO;
import com.isa.thinair.airreservation.api.dto.ReservationAlertDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationInsurance;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.service.ReservationQueryBD;
import com.isa.thinair.airreservation.api.service.SegmentBD;
import com.isa.thinair.airschedules.api.dto.FlightAlertDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.AlertUtil;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.utils.CommandNames;
import com.isa.thinair.alerting.api.util.AlertTemplateEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for updating reservations of the updated/cancelled flights used to invoke reservation alerts and emails
 * 
 * @author Chandana Kithalagama
 * @isa.module.command name="updateReservation"
 */
public class UpdateReservation extends DefaultBaseCommand {

	// Dao's
	// BD's
	private SegmentBD segmentBD;
	private AircraftBD aircraftBD;
	private FlightInventoryBD flightInventryBD;
	private ReservationBD reservationBD;
	private FlightBD flightBD;
	private ReservationQueryBD reservationQBD;

	/**
	 * constructor of the UpdateReservation command
	 */
	public UpdateReservation() {

		segmentBD = AirSchedulesUtil.getSegmentBD();
		aircraftBD = AirSchedulesUtil.getAircraftBD();
		flightInventryBD = AirSchedulesUtil.getFlightInventoryBD();
		reservationBD = AirSchedulesUtil.getReservationBD();
		flightBD = AirSchedulesUtil.getFlightBD();
		reservationQBD = AirSchedulesUtil.getReservationQueryBD();
	}

	/**
	 * execute method of the UpdateReservation command
	 * 
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	public ServiceResponce execute() throws ModuleException {

		// getting command params
		String commandName = (String) this.getParameter(CommandParamNames.COMMAND_NAME);
		FlightAlertDTO alertDTO = (FlightAlertDTO) this.getParameter(CommandParamNames.FLIGHT_ALERT_DTO);
		Collection<FlightReservationAlertDTO> flightReservationAlertDTOList = (Collection<FlightReservationAlertDTO>) this
				.getParameter(CommandParamNames.FLIGHT_SEGMENT_EMAIL_DTO_COLLECTION);
		DefaultServiceResponse lastResponse = (DefaultServiceResponse) this.getParameter(CommandParamNames.LAST_RESPONSE);
		UserPrincipal principal = (UserPrincipal) this.getParameter(CommandParamNames.USER_PRINCIPAL);
		Flight updated = (Flight) this.getParameter(CommandParamNames.FLIGHT);
		Flight existingFlt = (Flight) this.getParameter(CommandParamNames.EXISTING_FLIGHT);
		String userId = (principal != null) ? principal.getName() : "";
		boolean isModelChangeWithSeatTemplate = false;
		if (AppSysParamsUtil.isEnableAircraftCapacityDowngradeToDifferentCC()
				&& commandName.equals(CommandNames.AMMEND_FLIGHT_MACRO)) {
			AircraftModel model = aircraftBD.getAircraftModel(updated.getModelNumber());
			AircraftModel existingModel = aircraftBD.getAircraftModel(existingFlt.getModelNumber());

			Map<String, Integer> availableCabinClasses = new HashMap<String, Integer>();
			for (AircraftCabinCapacity acc : model.getAircraftCabinCapacitys()) {
				availableCabinClasses.put(acc.getCabinClassCode(), acc.getInfantCapacity());
			}

			Collection<String> cabinClassesToBeDeleted = flightInventryBD.getCabinClassesToBeDeleted(model, existingModel);

			if (cabinClassesToBeDeleted.size() > 0) {
				// This means some cabin classes to be removed from new aircraft
				Set<FlightSegement> segments = existingFlt.getFlightSegements();
				for (FlightSegement seg : segments) {
					// update reservations and inventory for deleting cabin classes if there are any
					reservationBD.changeReservationCabinClass(seg, cabinClassesToBeDeleted, availableCabinClasses);
				}
			}

			if (!existingFlt.getModelNumber().equals(updated.getModelNumber())
					|| (existingFlt.getSeatChargeTemplateId() != null && existingFlt.getSeatChargeTemplateId() != updated
							.getSeatChargeTemplateId())) {
				isModelChangeWithSeatTemplate = true;
				alertDTO.setSendEmailWhenSeatLost(true);
			}

			Set<FlightSegement> segments = existingFlt.getFlightSegements();

			for (FlightSegement seg : segments) {
				FlightReservationAlertDTO newFlightAlert = new FlightReservationAlertDTO();
				Collection<FlightChangeInfo> flightChangeInfo = flightInventryBD.getFlightSeatChangeData(seg,
						updated.getModelNumber());
				newFlightAlert.setFlightSeatChangesInfo(flightChangeInfo);
				flightReservationAlertDTOList.add(newFlightAlert);

			}
		}
		// checking params
		// this.checkParams(alertDTO);

		boolean flightCancelled = false;
		if (commandName != null
				&& (commandName.equals(CommandNames.CANCEL_FLIGHT_MACRO)
						|| commandName.equals(CommandNames.CANCEL_SCHEDULE_MACRO)
						|| commandName.equals(CommandNames.AMMEND_SCHEDULE_MACRO)
						|| commandName.equals(CommandNames.AMMEND_SCHEDULE_LEGS_MACRO) || commandName
							.equals(CommandNames.RECALCULATE_LOCAL_TIME_MACRO)))

			flightCancelled = true;

		Collection<Integer> fltSegIDs = new ArrayList<Integer>();
		Collection<FlightSegement> updatedSegs = new ArrayList<FlightSegement>();
		boolean isOverlappingSegmentsModified = false;

		if (updated != null && existingFlt != null) {
						
			for (FlightSegement updatedSeg : updated.getFlightSegements()) {
				for (FlightSegement seg : existingFlt.getFlightSegements()) {
					if (!fltSegIDs.contains(seg.getFltSegId())) {
						fltSegIDs.add(seg.getFltSegId());
					}

					if (seg.getSegmentCode().equals(updatedSeg.getSegmentCode()) && seg.getSegmentCode().split("/").length == 2) {
						if (seg.getEstTimeDepatureLocal().getTime() != updatedSeg.getEstTimeDepatureLocal().getTime()
								|| seg.getEstTimeArrivalLocal().getTime() != updatedSeg.getEstTimeArrivalLocal().getTime()) {
							updatedSegs.add(updatedSeg);
							if(updatedSeg.getOverlapSegmentId() != null ){
							    isOverlappingSegmentsModified = true;
							}
						}
					}
				}
			}
			
			if(isOverlappingSegmentsModified){
				fltSegIDs.addAll(flightBD.getFlightSegIdsforFlight(updated.getOverlapingFlightId()));
			}			
			segmentBD.auditFlightTimeChanges(fltSegIDs, updatedSegs, existingFlt.getFlightNumber(), userId);
		}

		// store Alerts in the DB first (before sending out emails)
		if (AlertUtil.isNotifyAnyReservationAffections(alertDTO) || flightCancelled || isModelChangeWithSeatTemplate) {
			// stop alerts going more than 999 assuming flight doesn't carry more than 200
			if (flightReservationAlertDTOList != null) {

				if (flightReservationAlertDTOList.size() < 5) {
					// preparing new DTO
					ReservationAlertDTO reservationAlertDto = AlertUtil.prepareReservationAlertInfo(
							flightReservationAlertDTOList, alertDTO);
					segmentBD.alertReservations(reservationAlertDto);
				} else {
					Iterator<FlightReservationAlertDTO> iter = flightReservationAlertDTOList.iterator();
					Collection<FlightReservationAlertDTO> alertCol = new HashSet<FlightReservationAlertDTO>();
					int count = 0;
					int noofAlerts = flightReservationAlertDTOList.size();
					int times = noofAlerts / 5;
					int modtimes = noofAlerts % 5;
					if (modtimes > 0)
						times = times + 1;
					if (iter != null) {
						for (int i = 0; i < times; i++) {
							for (int j = 0; j < 5 && count < noofAlerts; j++) {
								alertCol.add(iter.next());
								count++;
							}
							// preparing new DTO
							ReservationAlertDTO reservationAlertDto = AlertUtil.prepareReservationAlertInfo(alertCol, alertDTO);
							segmentBD.alertReservations(reservationAlertDto);
							alertCol = new HashSet<FlightReservationAlertDTO>();

						}
					}
				}
			}
			if (alertDTO.isAlertForRescheduledFlight()) {
				// Update the reservation's insurance as well
				updateReservationInsuranceForRescheduling(flightReservationAlertDTOList);
			}
		}

		return lastResponse;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void updateReservationInsuranceForRescheduling(Collection<FlightReservationAlertDTO> flightReservationAlertDTOList)
			throws ModuleException {

		Map<String, List<ReservationInsurance>> pnrInsuranceMap = new HashMap<String, List<ReservationInsurance>>();
		for (FlightReservationAlertDTO flightResAlert : (List<FlightReservationAlertDTO>) flightReservationAlertDTOList) {
			Map<Integer, HashMap> fSegs = flightResAlert.getFlightSegments();
			if (fSegs != null && !fSegs.keySet().isEmpty()) {
				for (Integer flightSegId : fSegs.keySet()) {
					Collection<Reservation> pnrList = segmentBD.getPnrsForSegment(flightSegId);
					HashMap flightSegmentAlertDetailsMap = fSegs.get(flightSegId);
					if (fSegs != null & fSegs.size() > 0) {
						for (Reservation reservation : pnrList) {
							if (!pnrInsuranceMap.keySet().contains(reservation.getPnr())) {
								List<ReservationInsurance> resIns = reservationQBD.getReservationInsuranceByPnr(reservation.getPnr());
								if (resIns != null && !resIns.isEmpty()) {
									pnrInsuranceMap.put(reservation.getPnr(), resIns);
								}

							}
							if (pnrInsuranceMap.get(reservation.getPnr()) != null) {
								List<ReservationInsurance> resIns = pnrInsuranceMap.get(reservation.getPnr());
								String segCode = (String) flightSegmentAlertDetailsMap
										.get(AlertTemplateEnum.TemplateParams.SEGMENT);
								Date newSegDepTime = (Date) flightSegmentAlertDetailsMap
										.get(AlertTemplateEnum.TemplateParams.NEW_SEGMENT_DEPARTURE);
								Date newSegArrivalTime = (Date) flightSegmentAlertDetailsMap
										.get(AlertTemplateEnum.TemplateParams.NEW_SEGMENT_ARRIVAL);
								
								for (ReservationInsurance insurance : resIns) {
									if (segCode.startsWith(insurance.getOrigin())) {// The origin segment has been
																					// rescheduled,
										// so
										// update the new departure time
										insurance.setDateOfTravel(newSegDepTime);
									}

									if (segCode.endsWith(insurance.getDestination())) {// The destination segment has
																						// been
										// rescheduled, so update the new
										// arrival
										// time
										insurance.setDateOfReturn(newSegArrivalTime);
									}
								}
								
								// Update new Departure time for the Insurance
								
							}
						}
					}
				}

			}
			// update all insurances
			for (Map.Entry entry : pnrInsuranceMap.entrySet()) {	
				List<ReservationInsurance> resInsList = (List<ReservationInsurance>) entry.getValue();
				if(resInsList != null && !resInsList.isEmpty()){	
					for(ReservationInsurance ins:resInsList){
						reservationBD.saveReservationInsurance(ins);
					}					
				}	
			}
		}
	}
}
