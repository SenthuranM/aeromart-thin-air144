package com.isa.thinair.airschedules.core.bl.check;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import com.isa.thinair.airinventory.api.service.FlightInventoryResBD;
import com.isa.thinair.airschedules.api.dto.AffectedPeriod;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.LegUtil;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.airschedules.core.bl.noncmd.FlightsForAffectedPeriod;
import com.isa.thinair.airschedules.core.persistence.dao.FlightDAO;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command to check Reservations for the exsiting Schedule Legs
 * 
 * @author Chandana Kithalagama
 * @isa.module.command name="checkReservationsForLegs"
 */
public class CheckReservationsForLegs extends DefaultBaseCommand {

	// Dao's
	private FlightDAO flightDAO;

	// BD's
	private FlightInventoryResBD flightInventryResBD;

	// helpers
	private FlightsForAffectedPeriod fltsForAffectedPeriod;

	/**
	 * constructor of the CheckReservationsForLegs command
	 */
	public CheckReservationsForLegs() {

		// looking up daos
		flightDAO = (FlightDAO) AirSchedulesUtil.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_DAO);

		// looking up bd's
		flightInventryResBD = AirSchedulesUtil.getFlightInventoryResBD();

		// helpers
		fltsForAffectedPeriod = new FlightsForAffectedPeriod(flightDAO);
	}

	/**
	 * execute method of the CheckReservationsForLegs command
	 * 
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	public ServiceResponce execute() throws ModuleException {

		// getting command params
		Collection<AffectedPeriod> updatePeriods = (Collection<AffectedPeriod>) this.getParameter(CommandParamNames.UPDATE_PERIOD_LIST);
		FlightSchedule updated = (FlightSchedule) this.getParameter(CommandParamNames.FLIGHT_SCHEDULE);
		FlightSchedule existing = (FlightSchedule) this.getParameter(CommandParamNames.LOADED_EXISTING_SCHEDULE);
		Boolean isOverwrite = (Boolean) this.getParameter(CommandParamNames.FLIGHT_OVERWRITE_FLAG);

		// checking params
		this.checkParams(updatePeriods);

		// constructing command responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);

		Iterator<AffectedPeriod> itUpdatedPeriods = updatePeriods.iterator();
		while (itUpdatedPeriods.hasNext()) {

			AffectedPeriod period = (AffectedPeriod) itUpdatedPeriods.next();
			List<Integer> flightIds = fltsForAffectedPeriod.getFlightIdsForUpdatePeriod(period, updated.getScheduleId().intValue(),
					isOverwrite.booleanValue());

			// check legs added deleted validation
			boolean legsaddedDeleted = LegUtil.checkLegsAddedDeleted(existing, updated);

			if (legsaddedDeleted) {
				if (flightIds != null && flightIds.size() > 0) {
					Collection<Integer> resFlights = flightInventryResBD.filterFlightsHavingReservations(flightIds);
					if (resFlights.size() > 0) {

						responce.setSuccess(false);
						responce.setResponseCode(ResponceCodes.RESERVATIONS_FOUND_FOR_LEGCHANGED_SCHEDULE);
						return responce;
					}
				}
			}
		}

		// return command responce
		return responce;
	}

	/**
	 * privte method to validate parameters
	 * 
	 * @param schedule
	 * @throws ModuleException
	 */
	private void checkParams(Collection<AffectedPeriod> cancelPeriods) throws ModuleException {

		if (cancelPeriods == null)// throw exception
			throw new ModuleException("airschedules.arg.invalid.null");
	}
}
