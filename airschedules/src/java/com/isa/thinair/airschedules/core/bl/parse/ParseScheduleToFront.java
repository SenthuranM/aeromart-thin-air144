/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.parse;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.LocalZuluTimeAdder;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command to parse schedules to front end
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="parseScheduleToFront"
 */
public class ParseScheduleToFront extends DefaultBaseCommand {

	// BDs
	private AirportBD airportBD;

	/**
	 * constructor of the ParseScheduleToFront command
	 */
	public ParseScheduleToFront() {

		airportBD = AirSchedulesUtil.getAirportBD();
	}

	/**
	 * execute method of the ParseScheduleToFront command
	 * 
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	public ServiceResponce execute() throws ModuleException {

		Page page = (Page) this.getParameter(CommandParamNames.PAGE);

		Collection<FlightSchedule> schedList = (page == null || page.getPageData() == null) ? new ArrayList<FlightSchedule>() : page.getPageData();

		// constructing command responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);

		// if flight list has flights
		if (schedList.size() > 0) {

			LocalZuluTimeAdder timeAdder = new LocalZuluTimeAdder(airportBD);

			Iterator<FlightSchedule> itScheds = schedList.iterator();
			while (itScheds.hasNext()) {

				FlightSchedule schedule = (FlightSchedule) itScheds.next();
				schedule = timeAdder.addLocalTimeDetailsToFlightSchedule(schedule, false);
			}

			responce.addResponceParam(CommandParamNames.PAGE, page);

		} else {
			responce.addResponceParam(CommandParamNames.PAGE, page);
		}

		// return command responce
		return responce;
	}
}
