/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:19:25
 * 
 * $Id$
 * 
 * ===============================================================================
 */

package com.isa.thinair.airschedules.core.persistence.hibernate;

import com.isa.thinair.airinventory.api.dto.baggage.FlightBaggageDTO;
import com.isa.thinair.airinventory.api.dto.meal.FlightMealDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.CodeshareMcFlightDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatmap.FlightSeatsDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.rak.InsurancePublisherDetailDTO;
import com.isa.thinair.airschedules.api.dto.PastFlightInfoDTO;
import com.isa.thinair.airschedules.api.dto.WildcardFlightSearchDto;
import com.isa.thinair.airschedules.api.model.FlightSegNotificationEvent;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.airschedules.api.utils.SegmentUtil;
import com.isa.thinair.airschedules.core.persistence.dao.FlightSegementDAO;
import com.isa.thinair.airschedules.core.util.LookUpUtils;
import com.isa.thinair.commons.api.constants.CommonsConstants.CarrierServiceType;
import com.isa.thinair.commons.api.constants.CommonsConstants.FlightType;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformHibernateDaoSupport;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.Util;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.Blob;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * FlightSegementDAOImpl is the FlightSegementtDAO implementatiion for hibernate
 * 
 * @author Lasantha Pambagoda
 * @isa.module.dao-impl dao-name="flightSegementDAO"
 */
public class FlightSegementDAOImpl extends PlatformHibernateDaoSupport implements FlightSegementDAO {

	/**
	 * method to get all flight segments
	 */
	public List<FlightSegement> getFlightSegements() {
		return find("from FlightSegement", FlightSegement.class);
	}

	/**
	 * method to get flight segment for given id
	 */
	public FlightSegement getFlightSegement(int id) {
		return (FlightSegement) get(FlightSegement.class, new Integer(id));
	}

	/**
	 * method to get flight segment for a given flight id and segment code
	 */
	public FlightSegement getFlightSegement(int flightId, String segmentCode) {
		FlightSegement segment = null;
		String hql = "select fs from FlightSegement as fs where fs.flightId=? " + "and fs.segmentCode=? ";

		Object[] params = { new Integer(flightId), segmentCode };
		Collection<FlightSegement> segments = find(hql, params, FlightSegement.class);
		if (segments.size() > 0) {
			Iterator<FlightSegement> itSegment = segments.iterator();
			segment = (FlightSegement) itSegment.next();
		}
		return segment;
	}

	public Collection<FlightSegement> getFlightSegmentList(String segmentCode, Date departureDate) {

		String hql = "Select fs from FlightSegement fs where fs.segmentCode like ? and fs.estTimeDepatureLocal=?";
		Object[] params = { segmentCode, departureDate };
		return (Collection<FlightSegement>) find(hql, params, FlightSegement.class);
	}

	/**
	 * method to save flight segment
	 */
	public void saveFlightSegement(FlightSegement flightsegement) {
		hibernateSaveOrUpdate(flightsegement);
	}

	/**
	 * method to remove flight segment
	 */
	public void removeFlightSegement(int id) {
		Object flightsegement = load(FlightSegement.class, new Integer(id));
		delete(flightsegement);
	}

	public Collection<FlightSegement> getFlightSegments(int flightId) {
		return find("select fs from FlightSegement as fs " + "where " + "fs.flightId=?",
				new Integer(flightId), FlightSegement.class);
	}

	public Collection<FlightSegement> getFlightSegmentIds(int flightId) {
		return find("select fs from FlightSegement as fs " + "where " + "fs.flightId=?",
				new Integer(flightId), FlightSegement.class);
	}

	@SuppressWarnings("unchecked")
	public Collection<FlightSegement> getDepartureFlightForStation(String airportCode, Date gmtDstEffectiveFrom,
			Date gmtDstEffectiveTo) {

		String hql = "select fs from FlightSegement as fs where fs.segmentCode "
				+ "like ? and fs.estTimeDepatureZulu between ? and ?";

		String depAirport = airportCode + "/%";

		return getSession().createQuery(hql).setString(0, depAirport).setTimestamp(1, gmtDstEffectiveFrom)
				.setTimestamp(2, gmtDstEffectiveTo).list();

	}

	@SuppressWarnings("unchecked")
	public Collection<FlightSegement> getArrivalFlightForStation(String airportCode, Date gmtDstEffectiveFrom,
			Date gmtDstEffectiveTo) {

		String arrAirport = "%/" + airportCode;
		String hql = "select fs from FlightSegement as fs where fs.segmentCode "
				+ "like ? and fs.estTimeArrivalZulu between ? and ?";
		return getSession().createQuery(hql).setString(0, arrAirport).setTimestamp(1, gmtDstEffectiveFrom)
				.setTimestamp(2, gmtDstEffectiveTo).list();

	}

	/**
	 * Return the flight and flight segment details for the given flightSegmentIds collection
	 * 
	 * @param flightSegmentIds
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Collection<FlightSegmentDTO> getFlightSegments(Collection<Integer> flightSegmentIds) {

		String sql = "SELECT "
				+ "fseg.flt_seg_id, fseg.flight_id, fseg.segment_code, f.flight_number,f.model_number, f.flight_type, f.cs_oc_carrier_code, "
				+ "f.cs_oc_flight_number, fseg.est_time_departure_local, f.origin, f.destination,f.flight_type, fseg.est_time_departure_zulu, "
				+ "fseg.est_time_arrival_local, fseg.est_time_arrival_zulu, fseg.status FSEGSTATUS, f.status FSTATUS ,f.OPERATION_TYPE_ID, "
				+ "cf.cs_carrier_code, cf.cs_flight_number FROM T_FLIGHT_SEGMENT fseg, T_FLIGHT f left join t_cs_flight cf on f.flight_id = cf.flight_id "
				+ "WHERE f.flight_id = fseg.flight_id "
				+ "and fseg.flt_seg_id in (" + Util.buildIntegerInClauseContent(flightSegmentIds) + ") order by fseg.est_time_departure_zulu";
		
		final long currentTime = CalendarUtil.getCurrentSystemTimeInZulu().getTime();
		final long cutOffForInternational = AppSysParamsUtil.getInternationalBufferDurationInMillis();
		final long cutOffForDomestic = AppSysParamsUtil.getReservationCutoverForDomesticInMillis();
		
		JdbcTemplate jt = new JdbcTemplate(LookUpUtils.getDatasource());

		Object queryResults = jt.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<FlightSegmentDTO> colFlightSegmentDTO = new ArrayList<FlightSegmentDTO>();
				Map<Integer, FlightSegmentDTO> flightSegmentDTOs = new HashMap<Integer, FlightSegmentDTO>();
				if (rs != null) {
					while (rs.next()) {
						long flightCutoffDiff = 0;
						int flightSegId = rs.getInt("FLT_SEG_ID");
						
						if (!flightSegmentDTOs.containsKey(flightSegId)) {
							int flightId = rs.getInt("FLIGHT_ID");
	
							String segmentCode = rs.getString("SEGMENT_CODE");
							String flightNumber = rs.getString("FLIGHT_NUMBER");
							String origin = rs.getString("ORIGIN");
							String destination = rs.getString("DESTINATION");
							Date departureDate = rs.getTimestamp("EST_TIME_DEPARTURE_LOCAL");
							Date departureDateTimeZulu = rs.getTimestamp("EST_TIME_DEPARTURE_ZULU");
							Date arrivalDate = rs.getTimestamp("EST_TIME_ARRIVAL_LOCAL");
							Date arrivalDateTimeZulu = rs.getTimestamp("EST_TIME_ARRIVAL_ZULU");
							Integer operationTypeId = rs.getInt("OPERATION_TYPE_ID");
							String flightModelNumber = rs.getString("MODEL_NUMBER");
							String flightType = rs.getString("FLIGHT_TYPE");
							String csOcCarrierCode = rs.getString("CS_OC_CARRIER_CODE");
							String csOcFlightNumber = rs.getString("CS_OC_FLIGHT_NUMBER");
							String csMcCarrierCode = rs.getString("CS_CARRIER_CODE");
							String csMcFlightNumber = rs.getString("CS_FLIGHT_NUMBER");
	
							FlightSegmentDTO flightSegmentDTO = new FlightSegmentDTO();
							flightSegmentDTO.setSegmentId(new Integer(flightSegId));
							flightSegmentDTO.setFlightId(new Integer(flightId));
							flightSegmentDTO.setOperationTypeID(operationTypeId);
							flightSegmentDTO.setSegmentCode(segmentCode);
							flightSegmentDTO.setFlightNumber(flightNumber);
							flightSegmentDTO.setFlightType(flightType);
							flightSegmentDTO.setCsOcCarrierCode(csOcCarrierCode);
							flightSegmentDTO.setCsOcFlightNumber(csOcFlightNumber);					
							if (origin != null) {
								flightSegmentDTO.setFromAirport(origin);
							}
							if (destination != null) {
								flightSegmentDTO.setToAirport(destination);
							}
							flightSegmentDTO.setDepartureDateTime(departureDate);
							flightSegmentDTO.setDepartureDateTimeZulu(departureDateTimeZulu);
							flightSegmentDTO.setArrivalDateTime(arrivalDate);
							flightSegmentDTO.setArrivalDateTimeZulu(arrivalDateTimeZulu);
							flightSegmentDTO.setFlightSegmentStatus(rs.getString("FSEGSTATUS"));
							flightSegmentDTO.setFlightStatus(rs.getString("FSTATUS"));
							
							if (csMcCarrierCode != null && csMcFlightNumber != null) {
								CodeshareMcFlightDTO codeshareMcFlightDTO = new CodeshareMcFlightDTO();
								codeshareMcFlightDTO.setCsMcCarrierCode(csMcCarrierCode);
								codeshareMcFlightDTO.setCsMcFlightNumber(csMcFlightNumber);
								List<CodeshareMcFlightDTO> csMcFlights = new ArrayList<CodeshareMcFlightDTO>();
								csMcFlights.add(codeshareMcFlightDTO);
								flightSegmentDTO.setCsMcFlights(csMcFlights);
							}
	
							if (flightModelNumber != null) {
								flightSegmentDTO.setFlightModelNumber(flightModelNumber);
							}
							
							if (FlightType.DOMESTIC.getType().equals(rs.getString("FLIGHT_TYPE"))) {
								flightSegmentDTO.setDomesticFlight(true);
								flightCutoffDiff = (flightSegmentDTO.getDepartureDateTimeZulu().getTime() - cutOffForDomestic)
										- currentTime;
							} else {
								flightCutoffDiff = (flightSegmentDTO.getDepartureDateTimeZulu().getTime() - cutOffForInternational)
										- currentTime;
							}
							
							if (flightCutoffDiff < 0) {
								flightSegmentDTO.setFlightWithinCutoff(true);
							}
	
							colFlightSegmentDTO.add(flightSegmentDTO);
							flightSegmentDTOs.put(flightSegId, flightSegmentDTO);
						} else {
							
							FlightSegmentDTO flightSegmentDTO = flightSegmentDTOs.get(flightSegId);

							String csMcCarrierCode = rs.getString("CS_CARRIER_CODE");
							String csMcFlightNumber = rs.getString("CS_FLIGHT_NUMBER");
							if (csMcCarrierCode != null && csMcFlightNumber != null) {
								CodeshareMcFlightDTO codeshareMcFlightDTO = new CodeshareMcFlightDTO();
								codeshareMcFlightDTO.setCsMcCarrierCode(csMcCarrierCode);
								codeshareMcFlightDTO.setCsMcFlightNumber(csMcFlightNumber);
								flightSegmentDTO.getCsMcFlights().add(codeshareMcFlightDTO);
							}
						}
					}
				}
				return colFlightSegmentDTO;
			}
		});

		return (Collection<FlightSegmentDTO>) queryResults;
	}

	/**
	 * Return the flight and flight segment details for the given flightSegmentIds collection
	 * 
	 * @param flightSegmentIds
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Collection<FlightSegmentDTO> getFlightSegments(Collection<Integer> flightIds, String segmentCode) {

		String sql = "SELECT " + "fseg.flt_seg_id, fseg.flight_id, fseg.segment_code, f.flight_number, "
				+ "fseg.est_time_departure_local, f.origin, f.destination,f.flight_type, fseg.est_time_departure_zulu, "
				+ "fseg.est_time_arrival_local, fseg.est_time_arrival_zulu, fseg.status FSEGSTATUS, f.status FSTATUS " + "FROM "
				+ "T_FLIGHT_SEGMENT fseg, T_FLIGHT f " + "WHERE f.flight_id = fseg.flight_id " + " and fseg.segment_code='"
				+ segmentCode + "' " + "and " + Util.getReplaceStringForIN("f.flight_id", flightIds);

		JdbcTemplate jt = new JdbcTemplate(LookUpUtils.getDatasource());

		Object queryResults = jt.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<FlightSegmentDTO> colFlightSegmentDTO = new ArrayList<FlightSegmentDTO>();
				if (rs != null) {
					while (rs.next()) {
						int flightSegId = rs.getInt("FLT_SEG_ID");
						int flightId = rs.getInt("FLIGHT_ID");

						String segmentCode = rs.getString("SEGMENT_CODE");
						String flightNumber = rs.getString("FLIGHT_NUMBER");
						String origin = rs.getString("ORIGIN");
						String destination = rs.getString("DESTINATION");
						Date departureDate = rs.getTimestamp("EST_TIME_DEPARTURE_LOCAL");
						Date departureDateTimeZulu = rs.getTimestamp("EST_TIME_DEPARTURE_ZULU");
						Date arrivalDate = rs.getTimestamp("EST_TIME_ARRIVAL_LOCAL");
						Date arrivalDateTimeZulu = rs.getTimestamp("EST_TIME_ARRIVAL_ZULU");

						FlightSegmentDTO flightSegmentDTO = new FlightSegmentDTO();
						flightSegmentDTO.setSegmentId(new Integer(flightSegId));
						flightSegmentDTO.setFlightId(new Integer(flightId));
						flightSegmentDTO.setSegmentCode(segmentCode);
						flightSegmentDTO.setFlightNumber(flightNumber);
						if (origin != null) {
							flightSegmentDTO.setFromAirport(origin);
						}
						if (destination != null) {
							flightSegmentDTO.setToAirport(destination);
						}
						flightSegmentDTO.setDepartureDateTime(departureDate);
						flightSegmentDTO.setDepartureDateTimeZulu(departureDateTimeZulu);
						flightSegmentDTO.setArrivalDateTime(arrivalDate);
						flightSegmentDTO.setArrivalDateTimeZulu(arrivalDateTimeZulu);
						flightSegmentDTO.setFlightSegmentStatus(rs.getString("FSEGSTATUS"));
						flightSegmentDTO.setFlightStatus(rs.getString("FSTATUS"));

						if (FlightType.DOMESTIC.getType().equals(rs.getString("FLIGHT_TYPE")))
							flightSegmentDTO.setDomesticFlight(true);

						colFlightSegmentDTO.add(flightSegmentDTO);
					}
				}
				return colFlightSegmentDTO;
			}
		});

		return (Collection<FlightSegmentDTO>) queryResults;
	}

	/**
	 * 
	 * @param flightSegmentIds
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Collection<Integer> getFlightIds(Collection<Integer> flightSegmentIds) {

		String hql = "select distinct f.flightId " + "from FlightSegement as fs, Flight as f " + "where "
				+ "fs.flightId = f.flightId and " + "fs.fltSegId in (" + Util.buildIntegerInClauseContent(flightSegmentIds) + ")";

		return getSession().createQuery(hql).list();
	}

	public void updateDepartureFlightForStation(String stationCode, Date gmtDstEffectiveFrom, Date gmtDstEffectiveTo) {
		// String hqlUpdate = "update FlightSegement set scheduleId =:newScheduleId "
		// + "where " + "scheduleId = :originalScheduleId and "
		// + "departureDate >= :startDate and "
		// + "departureDate <= :stopDate and " + " ( "
		// + frequancyComparison + " )";
		//
		// getSession().createQuery(hqlUpdate).setParameter("newScheduleId",
		// new Integer(newScheduleId)).setParameter(
		// "originalScheduleId", new Integer(originalScheduleId))
		// .setParameter("startDate", period.getStartDate())
		// .setParameter("stopDate", period.getEndDate())
		// .executeUpdate();

	}

	public void updateArrivalFlightForStation(String stationCode, Date gmtDstEffectiveFrom, Date gmtDstEffectiveTo) {

	}

	/**
	 * returns flight segments matching with date time or date passed
	 * 
	 * @param airportCode
	 * @param departureDateLocal
	 * @param flightNo
	 * @param dateOnly
	 * @return
	 */
	public Collection<FlightSegement> getFlightSegmentsForLocalDate(String originAirportCode, String destAirportCode,
			String flightNo, Date departureDateLocal, boolean dateOnly) {

		if (originAirportCode == null || flightNo == null || departureDateLocal == null)
			return new ArrayList<FlightSegement>();

		List<FlightSegement> list = null;


		String hql = "select fs " + "from FlightSegement as fs, Flight as f " + "where fs.flightId = f.flightId and "
				+ " f.flightNumber = ? and " + "f.status != ? and " + "fs.validFlag = 'Y' ";
		ArrayList<Object> paramList = new ArrayList<Object>();
		paramList.add(flightNo);
		paramList.add(FlightStatusEnum.CANCELLED.getCode());

		String segmentCode;
		if (destAirportCode == null) {
			segmentCode = originAirportCode + "/___";
			hql += " and fs.segmentCode like ? ";
			paramList.add(segmentCode);
		} else {
			segmentCode = originAirportCode + "/%/" + destAirportCode;
			String segCodeExact = originAirportCode + "/" + destAirportCode;

			hql += " and ( fs.segmentCode like ? OR fs.segmentCode = ? ) ";
			paramList.add(segmentCode);
			paramList.add(segCodeExact);
		}

		// paramList.add(new Boolean(true));

		if (dateOnly) {
			hql = hql + " and fs.estTimeDepatureLocal between ? and ? ";
			Date startDate = CalendarUtil.getStartTimeOfDate(departureDateLocal);
			Date endDate = CalendarUtil.getEndTimeOfDate(departureDateLocal);
			paramList.add(startDate);
			paramList.add(endDate);
		} else {
			hql = hql + " and fs.estTimeDepatureLocal = ? ";
			paramList.add(departureDateLocal);
		}

		list = this.find(hql, paramList.toArray(), FlightSegement.class);

		return list;
	}

	@SuppressWarnings("unchecked")
	public Collection<FlightSegmentDTO> getFilledFlightSegmentDTOs(Collection<FlightSegmentDTO> flightSegmentDTOs)
			throws ModuleException {
		final Collection<FlightSegmentDTO> flightSegmentDTOsLocal = flightSegmentDTOs;
		final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		String sql = " SELECT F.FLIGHT_ID, F.FLIGHT_NUMBER, FS.FLT_SEG_ID, FS.SEGMENT_CODE, FS.EST_TIME_DEPARTURE_LOCAL, FS.EST_TIME_DEPARTURE_ZULU FROM "
				+ " T_FLIGHT_SEGMENT FS, T_FLIGHT F "
				+ " WHERE FS.FLIGHT_ID = F.FLIGHT_ID AND "
				+ " F.STATUS <> '"
				+ FlightStatusEnum.CANCELLED.getCode() + "' AND";

		for (Iterator<FlightSegmentDTO> flightSegmentDTOsIt = flightSegmentDTOs.iterator(); flightSegmentDTOsIt.hasNext();) {
			FlightSegmentDTO flightSegmentDTO = (FlightSegmentDTO) flightSegmentDTOsIt.next();

			String segmentCode = flightSegmentDTO.getFromAirport() + "/%" + flightSegmentDTO.getToAirport();

			String criteria = " (FS.SEGMENT_CODE LIKE '" + segmentCode + "' AND " + "F.FLIGHT_NUMBER LIKE '"
					+ flightSegmentDTO.getFlightNumber() + "' AND ";
			if (flightSegmentDTO.getDepartureDateTime() != null) {
				criteria += "FS.EST_TIME_DEPARTURE_LOCAL = TO_DATE('"
						+ dateFormat.format(flightSegmentDTO.getDepartureDateTime()) + "', 'DD/MM/YYYY HH24:MI') " + " ) ";

			} else if (flightSegmentDTO.getDepartureDateTimeZulu() != null) {
				criteria += "FS.EST_TIME_DEPARTURE_ZULU = TO_DATE('"
						+ dateFormat.format(flightSegmentDTO.getDepartureDateTimeZulu()) + "', 'DD/MM/YYYY HH24:MI') " + " ) ";
			} else {
				throw new ModuleException("invalid.departure.date");
			}

			if (sql.endsWith("AND")) {
				sql = sql + "(" + criteria;
			} else {
				sql = sql + " OR " + criteria;
			}
		}
		sql += ") ORDER BY FS.EST_TIME_DEPARTURE_ZULU";

		JdbcTemplate jt = new JdbcTemplate(LookUpUtils.getDatasource());
		return (Collection<FlightSegmentDTO>) jt.query(sql, new ResultSetExtractor() {
			Collection<FlightSegmentDTO> filledFlightSegmentDTOs = new LinkedList<FlightSegmentDTO>();

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					FlightSegmentDTO flightSegmentDTO = getMatchingFlightSegmentDTO(rs);
					if (flightSegmentDTO != null) {
						flightSegmentDTO.setSegmentId(new Integer(rs.getInt("FLT_SEG_ID")));
						flightSegmentDTO.setFlightId(new Integer(rs.getInt("FLIGHT_ID")));
						flightSegmentDTO.setSegmentCode(rs.getString("SEGMENT_CODE"));

						filledFlightSegmentDTOs.add(flightSegmentDTO);
					}
				}
				return filledFlightSegmentDTOs;
			}

			private FlightSegmentDTO getMatchingFlightSegmentDTO(ResultSet rs) throws SQLException {
				String flightNumber = rs.getString("FLIGHT_NUMBER");
				Date localDepDate = rs.getTimestamp("EST_TIME_DEPARTURE_LOCAL");
				Date zuluDepDate = rs.getTimestamp("EST_TIME_DEPARTURE_ZULU");
				for (Iterator<FlightSegmentDTO> flightSegmentDTOsIt = flightSegmentDTOsLocal.iterator(); flightSegmentDTOsIt
						.hasNext();) {
					FlightSegmentDTO flightSegmentDTO = (FlightSegmentDTO) flightSegmentDTOsIt.next();
					if (flightSegmentDTO.getDepartureDateTime() != null
							&& flightSegmentDTO.getFlightNumber().equals(flightNumber)
							&& dateFormat.format(flightSegmentDTO.getDepartureDateTime()).equals(dateFormat.format(localDepDate))) {
						flightSegmentDTO.setDepartureDateTimeZulu(zuluDepDate);
						return flightSegmentDTO;
					} else if (flightSegmentDTO.getDepartureDateTimeZulu() != null
							&& flightSegmentDTO.getFlightNumber().equals(flightNumber)
							&& dateFormat.format(flightSegmentDTO.getDepartureDateTimeZulu()).equals(
									dateFormat.format(zuluDepDate))) {
						flightSegmentDTO.setDepartureDateTime(localDepDate);
						return flightSegmentDTO;
					}
				}
				return null;// if this line is reached, one or more requested flight segments does not exists
			}
		});
	}

	/**
	 * Return the flight and flight segment Ids collection for a given scheduleID
	 * 
	 * @param flightSegmentIds
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Collection<Integer> getFlightSegIdsforSchedule(int scheduleId) throws ModuleException {

		String sql = "SELECT " + "fseg.flt_seg_id, fseg.flight_id, fseg.segment_code, f.flight_number, "
				+ "fseg.est_time_departure_local " + "FROM " + "T_FLIGHT_SEGMENT fseg, T_FLIGHT f "
				+ "WHERE f.flight_id = fseg.flight_id " + "and fseg.SCHEDULE_ID IN (" + scheduleId + ")";

		JdbcTemplate jt = new JdbcTemplate(LookUpUtils.getDatasource());

		return (Collection<Integer>) jt.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<Integer> colFlightSegmentIds = new ArrayList<Integer>();
				if (rs != null) {
					while (rs.next()) {
						int flightSegId = rs.getInt("FLT_SEG_ID");

						colFlightSegmentIds.add(flightSegId);
					}
				}
				return colFlightSegmentIds;
			}
		});
	}

	/**
	 * Return the flight and flight segment Ids collection for a given scheduleID
	 * 
	 * @param flightSegmentIds
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Collection<FlightSeatsDTO> getFlightSegIdsforFlight(Collection<Integer> colFlightId) throws ModuleException {

		String sql = "SELECT " + "fseg.flt_seg_id, fseg.flight_id, fseg.segment_code  " + "FROM " + "T_FLIGHT_SEGMENT fseg "
				+ "WHERE fseg.segment_valid_flag = 'Y' " + "and fseg.FLIGHT_ID IN ("
				+ Util.buildIntegerInClauseContent(colFlightId) + ")";

		JdbcTemplate jt = new JdbcTemplate(LookUpUtils.getDatasource());

		return (Collection<FlightSeatsDTO>) jt.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<FlightSeatsDTO> colFlightSegmentIds = new ArrayList<FlightSeatsDTO>();
				if (rs != null) {
					while (rs.next()) {
						FlightSeatsDTO flightSeatDTO = new FlightSeatsDTO();
						flightSeatDTO.setFlightSegmentID(rs.getInt("FLT_SEG_ID"));
						flightSeatDTO.setSegmentCode(rs.getString("segment_code"));
						flightSeatDTO.setFlightId(rs.getInt("flight_id"));
						colFlightSegmentIds.add(flightSeatDTO);
					}
				}
				return colFlightSegmentIds;
			}
		});
	}

	/**
	 * Return the flight and flight segment Ids collection for a given scheduleID
	 * 
	 * @param flightSegmentIds
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Collection<FlightMealDTO> getFlightSegIdsforFlightforMeal(Collection<Integer> colFlightId) throws ModuleException {

		String sql = "SELECT " + "flightseg.flt_seg_id, flightseg.segment_code  " + "FROM T_FLIGHT_SEGMENT flightseg "
				+ "WHERE flightseg.segment_valid_flag = 'Y' " + "and flightseg.FLIGHT_ID IN ("
				+ Util.buildIntegerInClauseContent(colFlightId) + ")";

		JdbcTemplate jt = new JdbcTemplate(LookUpUtils.getDatasource());

		return (Collection<FlightMealDTO>) jt.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<FlightMealDTO> colFlightSegmentIds = new ArrayList<FlightMealDTO>();
				if (rs != null) {
					while (rs.next()) {
						FlightMealDTO flightMeal = new FlightMealDTO();
						flightMeal.setFlightSegmentID(rs.getInt("FLT_SEG_ID"));
						flightMeal.setFlightSegmentCode(rs.getString("segment_code"));
						colFlightSegmentIds.add(flightMeal);
					}
				}
				return colFlightSegmentIds;
			}
		});
	}

	@SuppressWarnings("unchecked")
	public Collection<FlightBaggageDTO> getFlightSegIdsforFlightforBaggage(Collection<Integer> colFlightId)
			throws ModuleException {

		String sql = "SELECT " + "flightseg.flt_seg_id, flightseg.segment_code  " + "FROM T_FLIGHT_SEGMENT flightseg "
				+ "WHERE flightseg.segment_valid_flag = 'Y' " + "and flightseg.FLIGHT_ID IN ("
				+ Util.buildIntegerInClauseContent(colFlightId) + ")";

		JdbcTemplate jt = new JdbcTemplate(LookUpUtils.getDatasource());

		return (Collection<FlightBaggageDTO>) jt.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<FlightBaggageDTO> colFlightSegmentIds = new ArrayList<FlightBaggageDTO>();
				if (rs != null) {
					while (rs.next()) {
						FlightBaggageDTO flightBaggage = new FlightBaggageDTO();
						flightBaggage.setFlightSegmentID(rs.getInt("FLT_SEG_ID"));
						flightBaggage.setFlightSegmentCode(rs.getString("segment_code"));
						colFlightSegmentIds.add(flightBaggage);
					}
				}
				return colFlightSegmentIds;
			}
		});
	}

	public Integer saveFlightSegNotificationEventStatus(Integer flightSegmentId, Integer flightMsgEventId, String newStatus,
			String failureReason, Integer notificationCount, String notificationType) {

		FlightSegNotificationEvent notificationEvent;

		if (flightMsgEventId == null || flightMsgEventId.intValue() == 0) {
			notificationEvent = new FlightSegNotificationEvent();
			notificationEvent.setFlightSegId(flightSegmentId);
			notificationEvent.setAncillaryNotifyStatus(newStatus);
			notificationEvent.setFailureReason(failureReason);
			notificationEvent.setNoOtAttempts(1);
			notificationEvent.setNotificationCount(notificationCount);
			notificationEvent.setTimeStamp(new Date());
			notificationEvent.setStartTimeStamp(new Date());
			if (notificationType != null) {
				notificationEvent.setNotificationType(notificationType);
			}
		} else {
			notificationEvent = (FlightSegNotificationEvent) get(FlightSegNotificationEvent.class,
					flightMsgEventId);
			notificationEvent.setAncillaryNotifyStatus(newStatus);
			notificationEvent.setFailureReason(failureReason);
			if (newStatus != null && newStatus.equals("INPROGRESS")) {
				notificationEvent.setNoOtAttempts(notificationEvent.getNoOtAttempts() + 1);
			}
			if (newStatus != null && (newStatus.equals("FAILED") || newStatus.equals("SENT"))) {
				notificationEvent.setEndTimeStamp(new Date());
			}
			notificationEvent.setNotificationCount(notificationCount);
			notificationEvent.setTimeStamp(new Date());
			hibernateSaveOrUpdate(notificationEvent);
		}
		hibernateSaveOrUpdate(notificationEvent);
		return notificationEvent.getFlightMsgEventId();
	}

	public Integer saveFlightSegNotificationEventStatus(Integer flightSegmentId, Integer flightMsgEventId, String newStatus,
			String failureReason, Integer notificationCount, String notificationType, String insFirstResponse,
			String insSecResponse) {

		FlightSegNotificationEvent notificationEvent;

		if (flightMsgEventId == null || flightMsgEventId.intValue() == 0) {
			notificationEvent = new FlightSegNotificationEvent();
			notificationEvent.setFlightSegId(flightSegmentId);
			notificationEvent.setAncillaryNotifyStatus(newStatus);
			notificationEvent.setFailureReason(failureReason);
			notificationEvent.setNoOtAttempts(1);
			notificationEvent.setNotificationCount(notificationCount);
			notificationEvent.setTimeStamp(new Date());
			notificationEvent.setStartTimeStamp(new Date());
			if (notificationType != null) {
				notificationEvent.setNotificationType(notificationType);
			}
		} else {
			notificationEvent = (FlightSegNotificationEvent) get(FlightSegNotificationEvent.class,
					flightMsgEventId);
			notificationEvent.setAncillaryNotifyStatus(newStatus);
			notificationEvent.setFailureReason(failureReason);
			if (newStatus != null && newStatus.equals("INPROGRESS")) {
				notificationEvent.setNoOtAttempts(notificationEvent.getNoOtAttempts() + 1);
			}
			if (newStatus != null && (newStatus.equals("FAILED") || newStatus.equals("SENT"))) {
				notificationEvent.setEndTimeStamp(new Date());
			}
			notificationEvent.setNotificationCount(notificationCount);
			notificationEvent.setTimeStamp(new Date());
			hibernateSaveOrUpdate(notificationEvent);
		}
		hibernateSaveOrUpdate(notificationEvent);
		return notificationEvent.getFlightMsgEventId();
	}

	@Override
	public FlightSegNotificationEvent saveFlightSegNotificationEventStatus(Integer flightSegmentId, Integer flightMsgEventId,
			String newStatus, String failureReason, Integer notificationCount, String notificationType, String insFirstResponse,
			Blob insSecResponse) {
		FlightSegNotificationEvent notificationEvent;

		if (flightMsgEventId == null || flightMsgEventId.intValue() == 0) {
			notificationEvent = new FlightSegNotificationEvent();
			notificationEvent.setFlightSegId(flightSegmentId);
			notificationEvent.setAncillaryNotifyStatus(newStatus);
			notificationEvent.setFailureReason(failureReason);
			notificationEvent.setNoOtAttempts(1);
			notificationEvent.setNotificationCount(notificationCount);
			notificationEvent.setTimeStamp(new Date());
			notificationEvent.setStartTimeStamp(new Date());
			notificationEvent.setInsFirstResponse(insFirstResponse);
			notificationEvent.setInsSecResponse(insSecResponse);
			if (notificationType != null) {
				notificationEvent.setNotificationType(notificationType);
			}
		} else {
			notificationEvent = (FlightSegNotificationEvent) get(FlightSegNotificationEvent.class,
					flightMsgEventId);

			if (notificationEvent.getInsFirstResponse() == null) {
				notificationEvent.setInsFirstResponse(insFirstResponse);
			}
			notificationEvent.setInsSecResponse(insSecResponse);
			notificationEvent.setAncillaryNotifyStatus(newStatus);
			notificationEvent.setFailureReason(failureReason);
			if (newStatus != null
					&& (newStatus.equals(InsurancePublisherDetailDTO.INS_PUBLISH_STATE_FIRST_SENT)
							|| newStatus.equals(InsurancePublisherDetailDTO.INS_PUBLISH_STATE_LAST_SENT) || newStatus
								.equals(InsurancePublisherDetailDTO.INS_PUBLISH_STATE_NOT_SENT))) {
				notificationEvent.setNoOtAttempts(notificationEvent.getNoOtAttempts() + 1);
			} else {

			}
			notificationEvent.setNotificationCount(notificationCount);
			notificationEvent.setTimeStamp(new Date());
			hibernateSaveOrUpdate(notificationEvent);
		}
		hibernateSaveOrUpdate(notificationEvent);
		return notificationEvent;
	}

	@Override
	public Integer getFlightSegNotificationAttempts(Integer flightMsgEventId) {
		FlightSegNotificationEvent notificationEvent = (FlightSegNotificationEvent) get(
				FlightSegNotificationEvent.class, flightMsgEventId);

		if (notificationEvent != null) {
			return notificationEvent.getNoOtAttempts();
		} else {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public Collection<String> getBusCarrierCode() {

		String sql = "select carrier_code from t_carrier where service_type = '" + CarrierServiceType.BUS_SERVICE + "'";

		JdbcTemplate jt = new JdbcTemplate(LookUpUtils.getDatasource());

		Object queryResults = jt.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<String> busCarrierCodeList = new ArrayList<String>();
				if (rs != null) {
					while (rs.next()) {
						busCarrierCodeList.add(rs.getString("carrier_code"));
					}
				}
				return busCarrierCodeList;
			}
		});

		return (Collection<String>) queryResults;
	}

	@SuppressWarnings("unchecked")
	public Map<String, String> getBusAirCarrierCodes() {

		String sql = "SELECT BUS.CARRIER_CODE BUS, AIR.CARRIER_CODE AIR FROM T_CARRIER BUS, T_CARRIER AIR "
				+ " WHERE BUS.AIRLINE_CODE=AIR.AIRLINE_CODE AND " + " BUS.SERVICE_TYPE = '" + CarrierServiceType.BUS_SERVICE
				+ "'" + " AND AIR.SERVICE_TYPE='" + CarrierServiceType.AIRCRAFT_SERVICE + "'";

		JdbcTemplate jt = new JdbcTemplate(LookUpUtils.getDatasource());

		Object queryResults = jt.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<String, String> busAirCarrierCodes = new HashMap<String, String>();
				if (rs != null) {
					while (rs.next()) {
						busAirCarrierCodes.put(rs.getString("BUS"), rs.getString("AIR"));
					}
				}
				return busAirCarrierCodes;
			}
		});

		return (Map<String, String>) queryResults;

	}

	public List<FlightSegement>
			getSuggestedSegmentListForADate(String flightNumber, String departureDateLocal, String segmentCode) {
		String hql = " Select fs FROM Flight f, FlightSegement fs " + "WHERE f.flightNumber           = ? "
				+ "AND fs.segmentCode             = ?" + "AND trunc(fs.estTimeDepatureLocal) = ?"
				+ "AND fs.flightId                = f.flightId ";
		
		Date depDate = new Date();
		DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		try {
			depDate = dateFormat.parse(departureDateLocal);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Object[] params = { flightNumber, segmentCode, depDate };

		return find(hql, params, FlightSegement.class);
	}

	public FlightSegement getSpecificFlightSegment(int pnrSegId) {
		String sql = "select fs.* from t_flight_segment fs,t_pnr_segment ps where ps.pnr_seg_id=" + pnrSegId
				+ " and ps.flt_seg_id=fs.flt_seg_id";

		JdbcTemplate jdbctemplate = new JdbcTemplate(LookUpUtils.getDatasource());

		FlightSegement fltSeg = (FlightSegement) jdbctemplate.query(sql, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs != null) {
					while (rs.next()) {
						FlightSegement flightSegment = new FlightSegement();
						flightSegment.setFlightId(rs.getInt("FLIGHT_ID"));
						flightSegment.setFlightId(rs.getInt("FLT_SEG_ID"));
						flightSegment.setSegmentCode(rs.getString("SEGMENT_CODE"));
						flightSegment.setStatus(rs.getString("STATUS"));
						flightSegment.setEstTimeDepatureLocal(rs.getTimestamp("EST_TIME_DEPARTURE_LOCAL"));
						flightSegment.setEstTimeArrivalLocal(rs.getTimestamp("EST_TIME_ARRIVAL_LOCAL"));
						flightSegment.setEstTimeArrivalZulu(rs.getTimestamp("EST_TIME_ARRIVAL_ZULU"));
						flightSegment.setCheckinStatus(rs.getString("CHECKIN_STATUS"));
						flightSegment.setValidFlag(rs.getString("SEGMENT_VALID_FLAG") == "Y" ? true : false);
						flightSegment.setVersion(rs.getLong("VERSION"));
						flightSegment.setCheckinStatus(rs.getString("CHECKIN_STATUS"));
						flightSegment.setDepartureTerminalId(rs.getInt("DEPARTURE_TERMINAL_ID"));
						flightSegment.setArrivalTerminalId(rs.getInt("ARRIVAL_TERMINAL_ID"));
						flightSegment.setEstTimeDepatureZulu(rs.getTimestamp("EST_TIME_DEPARTURE_ZULU"));
						return flightSegment;
					}

				}
				return null;
			}
		});
		return fltSeg;
	}

	public int getFlightSegmentCount(String flightNumber, Date departureDateLocal, Date arrivalDateLocal, String segmentCode) {

		String hql = " Select fs FROM Flight f, FlightSegement fs " + "WHERE f.flightNumber           = ? "
				+ "AND fs.segmentCode             = ?" + "AND fs.estTimeDepatureLocal = ?" + " AND fs.estTimeArrivalLocal = ?"
				+ "AND fs.flightId                = f.flightId ";

		Object[] params = { flightNumber, segmentCode, departureDateLocal, arrivalDateLocal };

		return find(hql, params, FlightSegement.class).size();
	}

	public boolean areSameDaySameFlights(int flightSegID1, int flightSegID2) {

		String hql = "SELECT FS.segmentCode, TRUNC(FS.estTimeDepatureLocal), F.flightNumber";
		hql += " FROM FlightSegement FS, Flight F WHERE FS.fltSegId IN (?,?)";
		hql += " AND F.flightId = FS.flightId";
		hql += " group by FS.segmentCode, FS.estTimeDepatureLocal, F.flightNumber";

		Object[] params = { flightSegID1, flightSegID2 };
		if (find(hql, params, Object[].class).size() == 1) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isSeatMapAvailableForFlightSegement(Integer flightSegId) throws ModuleException {
		boolean seatMapAvaliable = false;
		String sql = "select COUNT(fas.flight_am_seat_id) as cnt from sm_t_flight_am_seat fas where fas.flt_seg_id ="
				+ flightSegId;
		JdbcTemplate template = new JdbcTemplate(LookUpUtils.getDatasource());
		Integer mappedNoOfFlightSeats = (Integer) template.query(sql, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					return rs.getInt("cnt");
				}
				return null;
			}
		});

		if (mappedNoOfFlightSeats != null && mappedNoOfFlightSeats.intValue() > 0) {
			seatMapAvaliable = true;
		}
		return seatMapAvaliable;

	}

	@Override
	public Map<String, List<FlightSegmentTO>> searchFlightsWildcardBased(WildcardFlightSearchDto flightSearchDto) throws ModuleException {

		List<Object> params = new ArrayList<Object>();
		StringBuilder sql = new StringBuilder();
		sql
				.append("SELECT f.flight_number, fs.flt_seg_id, fs.est_time_departure_local, fs.est_time_departure_zulu, ")
				.append("  fs.est_time_arrival_local, fs.segment_code, fs.status ")
				.append("FROM t_flight f, t_flight_segment fs ")
				.append("WHERE f.flight_id = fs.flight_id ");

		if (flightSearchDto.getDepartureDate() != null) {
			if (flightSearchDto.isDepartureDateExact()) {
				sql.append(" AND fs.est_time_departure_local = ? ");
			} else {
				sql.append(" AND trunc ( fs.est_time_departure_local ) = trunc( ? ) ");
			}
			params.add(flightSearchDto.getDepartureDate());
		}

		if (flightSearchDto.getArrivalDate() != null) {
			if (flightSearchDto.isArrivalDateExact()) {
				sql.append(" AND fs.est_time_arrival_local = ? ");
			} else {
				sql.append(" AND trunc ( fs.est_time_arrival_local ) = trunc ( ? ) ");
			}
			params.add(flightSearchDto.getArrivalDate());
		}

		if (flightSearchDto.getArrivalAirport() != null) {
			sql.append("  AND (fs.segment_code = ? OR fs.segment_code like ?) ");
			params.add(flightSearchDto.getDepartureAirport() + "/" + flightSearchDto.getArrivalAirport());
			params.add(flightSearchDto.getDepartureAirport() + "/%/" + flightSearchDto.getArrivalAirport());
		}  else {
			sql.append("  AND fs.segment_code like ? ");
			params.add(flightSearchDto.getDepartureAirport() + "/___");
		}
		
		if (flightSearchDto.isNonCNXFlight()) {		
		    sql.append(" AND f.status <> 'CNX' ");	
		}
		
		sql.append("  AND f.flight_number IN ( " +
						Util.buildStringInClauseContent(SegmentUtil.getPrioratizedFlightNumbers(flightSearchDto.getFlightNumber())) + " ) ");


		JdbcTemplate jdbctemplate = new JdbcTemplate(LookUpUtils.getDatasource());
		Map<String, List<FlightSegmentTO>> flightSegmentTOMap = (Map<String, List<FlightSegmentTO>>) jdbctemplate
				.query(sql.toString(), params.toArray(), new ResultSetExtractor() {
					public Object extractData(ResultSet resultSet) throws SQLException, DataAccessException {
						String flightNumber;
						FlightSegmentTO flightSegmentTO;
						Map<String, List<FlightSegmentTO>> map = new HashMap<String, List<FlightSegmentTO>>();

						while (resultSet.next()) {
							flightNumber = resultSet.getString("flight_number");

							if (!map.containsKey(flightNumber)) {
								map.put(flightNumber, new ArrayList<FlightSegmentTO>());
							}

							flightSegmentTO = new FlightSegmentTO();
							flightSegmentTO.setFlightNumber(resultSet.getString("flight_number"));
							flightSegmentTO.setFlightSegId(resultSet.getInt("flt_seg_id"));
							flightSegmentTO.setDepartureDateTime(resultSet.getTimestamp("est_time_departure_local"));
							flightSegmentTO.setDepartureDateTimeZulu(resultSet.getTimestamp("est_time_departure_zulu"));
							flightSegmentTO.setArrivalDateTime(resultSet.getTimestamp("est_time_arrival_local"));
							flightSegmentTO.setSegmentCode(resultSet.getString("segment_code"));
							flightSegmentTO.setOperatingAirline(AppSysParamsUtil.getDefaultCarrierCode());
							flightSegmentTO.setFlightRefNumber(FlightRefNumberUtil.composeFlightRPH(flightSegmentTO));
							flightSegmentTO.setFlightSegmentStatus(resultSet.getString("status"));

							map.get(flightNumber).add(flightSegmentTO);
						}

						return map;
					}
				});

		return flightSegmentTOMap;
	}

	public Map<String, List<FlightSegmentTO>> searchCodeShareFlightsWildcardBased(WildcardFlightSearchDto flightSearchDto) throws ModuleException {

		List<Object> params = new ArrayList<Object>();
		StringBuilder sql = new StringBuilder();
		sql
				.append("SELECT f.cs_oc_flight_number, fs.flt_seg_id, fs.est_time_departure_local, fs.est_time_departure_zulu, ")
				.append("  fs.est_time_arrival_local, fs.segment_code, fs.status ")
				.append("FROM t_flight f, t_flight_segment fs ")
				.append("WHERE f.flight_id = fs.flight_id ");

		if (flightSearchDto.getDepartureDate() != null) {
			if (flightSearchDto.isDepartureDateExact()) {
				sql.append(" AND fs.est_time_departure_local = ? ");
			} else {
				sql.append(" AND trunc ( fs.est_time_departure_local ) = trunc( ? ) ");
			}
			params.add(flightSearchDto.getDepartureDate());
		}

		if (flightSearchDto.getArrivalDate() != null) {
			if (flightSearchDto.isArrivalDateExact()) {
				sql.append(" AND fs.est_time_arrival_local = ? ");
			} else {
				sql.append(" AND trunc ( fs.est_time_arrival_local ) = trunc ( ? ) ");
			}
			params.add(flightSearchDto.getArrivalDate());
		}

		if (flightSearchDto.getDepartureAirport() != null) {
			if (flightSearchDto.getArrivalAirport() != null) {
				sql.append("  AND (fs.segment_code = ? OR fs.segment_code like ?) ");
				params.add(flightSearchDto.getDepartureAirport() + "/" + flightSearchDto.getArrivalAirport());
				params.add(flightSearchDto.getDepartureAirport() + "/%/" + flightSearchDto.getArrivalAirport());
			} else {
				sql.append("  AND fs.segment_code like ? ");
				params.add(flightSearchDto.getDepartureAirport() + "/___");
			}
		}

		if (flightSearchDto.isNonCNXFlight()) {
		    sql.append(" AND f.status <> 'CNX' ");
		}

		sql.append("  AND f.cs_oc_flight_number IN ( " +
						Util.buildStringInClauseContent(SegmentUtil.getPrioratizedFlightNumbers(flightSearchDto.getCodeShareFlightNumber())) + " ) ");

		if (flightSearchDto.getCodeShareCarrierCode() != null) {
			sql.append(" AND f.cs_oc_carrier_code = ? ");
			params.add(flightSearchDto.getCodeShareCarrierCode());
		}


		JdbcTemplate jdbctemplate = new JdbcTemplate(LookUpUtils.getDatasource());
		Map<String, List<FlightSegmentTO>> flightSegmentTOMap = (Map<String, List<FlightSegmentTO>>) jdbctemplate
				.query(sql.toString(), params.toArray(), new ResultSetExtractor() {
					public Object extractData(ResultSet resultSet) throws SQLException, DataAccessException {
						String flightNumber;
						FlightSegmentTO flightSegmentTO;
						Map<String, List<FlightSegmentTO>> map = new HashMap<String, List<FlightSegmentTO>>();

						while (resultSet.next()) {
							flightNumber = resultSet.getString("cs_oc_flight_number");

							if (!map.containsKey(flightNumber)) {
								map.put(flightNumber, new ArrayList<FlightSegmentTO>());
							}

							flightSegmentTO = new FlightSegmentTO();
							flightSegmentTO.setFlightNumber(resultSet.getString("cs_oc_flight_number"));
							flightSegmentTO.setFlightSegId(resultSet.getInt("flt_seg_id"));
							flightSegmentTO.setDepartureDateTime(resultSet.getTimestamp("est_time_departure_local"));
							flightSegmentTO.setDepartureDateTimeZulu(resultSet.getTimestamp("est_time_departure_zulu"));
							flightSegmentTO.setArrivalDateTime(resultSet.getTimestamp("est_time_arrival_local"));
							flightSegmentTO.setSegmentCode(resultSet.getString("segment_code"));
							flightSegmentTO.setOperatingAirline(AppSysParamsUtil.getDefaultCarrierCode());
							flightSegmentTO.setFlightRefNumber(FlightRefNumberUtil.composeFlightRPH(flightSegmentTO));
							flightSegmentTO.setFlightSegmentStatus(resultSet.getString("status"));

							map.get(flightNumber).add(flightSegmentTO);
						}

						return map;
					}
				});

		return flightSegmentTOMap;
	}
	
	/**
	 * Return the flight and flight segment Ids collection for a given flightID
	 * 
	 * @param flightSegmentIds
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Collection<Integer> getFlightSegIdsforFlight(int flightId) throws ModuleException {

		String sql = "SELECT flt_seg_id FROM T_FLIGHT_SEGMENT  WHERE flight_id = " + flightId;

		JdbcTemplate jt = new JdbcTemplate(LookUpUtils.getDatasource());

		return (Collection<Integer>) jt.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<Integer> colFlightSegmentIds = new ArrayList<Integer>();
				if (rs != null) {
					while (rs.next()) {
						int flightSegId = rs.getInt("FLT_SEG_ID");

						colFlightSegmentIds.add(flightSegId);
					}
				}
				return colFlightSegmentIds;
			}
		});
	}

	/**
	 * Return the Pax fare segment Ids collection of a given flight for update Etickets and Pax status
	 * @param pastFlightInfoDTO
	 * @return
	 * @throws ParseException
	 */
	public Collection<Integer> getPaxFareSegETIds(String flightId) throws ParseException {

		String paxFareSegIds = "SELECT DISTINCT(PPFST.PPFS_ID)  "
				+ " FROM T_FLIGHT F, T_FLIGHT_SEGMENT FS ,T_PNR_SEGMENT PS,T_RESERVATION R,"
				+ " T_PNR_PAX_FARE_SEGMENT PPFS,T_PNR_PAX_FARE_SEG_E_TICKET PPFST , T_PNR_PAX_FARE PPF "
				+ " WHERE F.FLIGHT_ID='" + flightId + "'AND FS.FLIGHT_ID = F.FLIGHT_ID AND PS.FLT_SEG_ID = FS.FLT_SEG_ID AND R.PNR = PS.PNR "
				+ " AND PS.PNR_SEG_ID = PPFS.PNR_SEG_ID AND PPFST.PPFS_ID = PPFS.PPFS_ID AND PPF.PPF_ID = PPFS.PPF_ID "
				+ " AND PPFST.STATUS IN ('F')";

		JdbcTemplate jt = new JdbcTemplate(LookUpUtils.getDatasource());

		return (Collection<Integer>) jt.query(paxFareSegIds, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<Integer> colPaxFareSegtIds = new ArrayList<>();
				if (rs != null) {
					while (rs.next()) {
						colPaxFareSegtIds.add(rs.getInt("PPFS_ID"));
					}
				}
				return colPaxFareSegtIds;
			}
		});
	}

	public Map<Integer, Boolean> isAtLeastOneSegmentHavingCSOCFlight(Collection<Integer> colFlightSegIds) throws ModuleException {

		String sql = "SELECT fseg.flt_seg_id, flight.cs_oc_carrier_code, flight.cs_oc_flight_number FROM T_FLIGHT_SEGMENT fseg, T_FLIGHT flight "
				+ "WHERE fseg.segment_valid_flag = 'Y' AND flight.flight_id = fseg.flight_id "
				+ "AND fseg.FLT_SEG_ID IN ("
				+ Util.buildIntegerInClauseContent(colFlightSegIds) + ") ";

		JdbcTemplate jt = new JdbcTemplate(LookUpUtils.getDatasource());

		return (Map<Integer, Boolean>) jt.query(sql, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<Integer, Boolean> csOCFlightMapping = new HashMap<Integer, Boolean>();
				if (rs != null) {
					while (rs.next()) {
						boolean isCSOCFlight = (rs.getString("CS_OC_CARRIER_CODE") != null || rs.getString("CS_OC_FLIGHT_NUMBER") != null)
								? true
								: false;
						csOCFlightMapping.put(rs.getInt("FLT_SEG_ID"), isCSOCFlight);
					}
				}
				return csOCFlightMapping;
			}
		});
	}
}
