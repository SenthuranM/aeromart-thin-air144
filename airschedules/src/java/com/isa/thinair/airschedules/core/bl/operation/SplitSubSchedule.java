/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.operation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.FlightScheduleLeg;
import com.isa.thinair.airschedules.api.model.FlightScheduleSegment;
import com.isa.thinair.airschedules.api.model.SSMSplitFlightSchedule;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.LegUtil;
import com.isa.thinair.airschedules.api.utils.LocalZuluTimeAdder;
import com.isa.thinair.airschedules.core.persistence.dao.FlightScheduleDAO;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.airschedules.core.util.ScheduledFlightUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for the create flight sub-schedules, this sub-schedules will be referred when sending SSM messages.
 * 
 * @author M.Rikaz
 * @isa.module.command name="splitSubSchedule"
 */
public class SplitSubSchedule extends DefaultBaseCommand {

	// Dao's
	private FlightScheduleDAO flightSchedleDAO;

	private static Log log = LogFactory.getLog(SplitSubSchedule.class);

	/**
	 * constructor of the SplitSubSchedule command
	 */
	public SplitSubSchedule() {

		// looking up daos
		flightSchedleDAO = (FlightScheduleDAO) AirSchedulesUtil.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_DAO);

		// helpers
	}

	/**
	 * execute method of the SplitSubSchedule command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {

		DefaultServiceResponse responce = new DefaultServiceResponse(true);

		// getting command params
		FlightSchedule scheduleC = (FlightSchedule) this.getParameter(CommandParamNames.FLIGHT_SCHEDULE);

		boolean isPeriodChange = this.getParameter(CommandParamNames.IS_SCHEDULE_PERIOD_CHANGE, Boolean.FALSE, Boolean.class);
		boolean isScheduleSplit = this.getParameter(CommandParamNames.IS_SCHEDULE_SPLIT, Boolean.FALSE, Boolean.class);
		Collection<Integer> scheduleIds = (Collection<Integer>) this.getParameter(CommandParamNames.SCHEDULE_IDS);
		FlightSchedule schedule = null;

		Collection<SSMSplitFlightSchedule> existingSubSchedules = null;
		Collection<SSMSplitFlightSchedule> newSubSchedules = new ArrayList<SSMSplitFlightSchedule>();
		try {
			// checking params
			this.checkParams(scheduleC);
			Collection<FlightSchedule> subSchedules = new ArrayList<FlightSchedule>();
			schedule = flightSchedleDAO.getFlightSchedule(scheduleC.getScheduleId());
			if (schedule != null) {
				log.info("processing schedule == > " + schedule.getScheduleId() + " Period => "
						+ CalendarUtil.formatDate(schedule.getStartDate(), CalendarUtil.PATTERN1) + " - "
						+ CalendarUtil.formatDate(schedule.getStopDate(), CalendarUtil.PATTERN1));

				boolean subScheduleExist = false;
				if (schedule.getSsmSplitFlightSchedule() != null && !schedule.getSsmSplitFlightSchedule().isEmpty()) {
					existingSubSchedules = new HashSet<SSMSplitFlightSchedule>(schedule.getSsmSplitFlightSchedule());

					if (existingSubSchedules.size() > 0)
						subScheduleExist = true;

					log.info("existing schedule periods");
					printSubSchedule(existingSubSchedules);
				}

				if (isScheduleSplitEnabledForDSTChanges(subScheduleExist)) {

					if (isScheduleSplit) {
						if (scheduleIds != null && !scheduleIds.isEmpty()) {
							for (Integer scheduleId : scheduleIds) {
								FlightSchedule scheduleTemp = flightSchedleDAO.getFlightSchedule(scheduleId);
								subSchedules = AirSchedulesUtil.getGDSServicesBD().getDSTApplicableSubSchedules(scheduleTemp);

								Set<SSMSplitFlightSchedule> ssmSplitFlightSchedule = tarnsformSchedulesToSSMSplitFlightSchedules(
										subSchedules, scheduleId);

								if (scheduleTemp.getSsmSplitFlightSchedule() != null) {
									scheduleTemp.getSsmSplitFlightSchedule().clear();
								}

								if (!ssmSplitFlightSchedule.isEmpty()) {
									scheduleTemp.getSsmSplitFlightSchedule().addAll(ssmSplitFlightSchedule);
								}
								flightSchedleDAO.saveFlightSchedule(scheduleTemp);

								newSubSchedules.addAll(scheduleTemp.getSsmSplitFlightSchedule());
							}
						}

					} else {
						if (!isPeriodChange && AppSysParamsUtil.isSplitFromPreviousSubSchedules() && subScheduleExist) {
							for (SSMSplitFlightSchedule existingSubSchedule : existingSubSchedules) {
								FlightSchedule scheduleCopied = ScheduledFlightUtil.getCopyOfSchedule(schedule);
								scheduleCopied.setStartDate(existingSubSchedule.getStartDate());
								scheduleCopied.setStopDate(existingSubSchedule.getStopDate());
								Collection<FlightSchedule> dstSplitSchedules = AirSchedulesUtil.getGDSServicesBD()
										.getDSTApplicableSubSchedules(scheduleCopied);
								if (dstSplitSchedules != null && !dstSplitSchedules.isEmpty()) {
									subSchedules.addAll(dstSplitSchedules);
								}
							}
						} else {
							subSchedules = AirSchedulesUtil.getGDSServicesBD().getDSTApplicableSubSchedules(schedule);
						}

						if (!subScheduleExist) {
							existingSubSchedules = convertScheduleToSubSchedule(existingSubSchedules, schedule);
						}

						Set<SSMSplitFlightSchedule> ssmSplitFlightSchedule = tarnsformSchedulesToSSMSplitFlightSchedules(
								subSchedules, schedule.getScheduleId());

						if (schedule.getSsmSplitFlightSchedule() != null) {
							schedule.getSsmSplitFlightSchedule().clear();
						}

						if (!ssmSplitFlightSchedule.isEmpty()) {
							schedule.getSsmSplitFlightSchedule().addAll(ssmSplitFlightSchedule);
						}
						flightSchedleDAO.saveFlightSchedule(schedule);

						newSubSchedules = schedule.getSsmSplitFlightSchedule();
					}
				}

				log.info("updated schedule periods");
				printSubSchedule(newSubSchedules);
			}

		} catch (ModuleException me) {
			responce = new DefaultServiceResponse(false);
			log.error("ERROR @ SplitSubSchedules " + me.getExceptionCode());
			me.printStackTrace();
		} catch (Exception ex) {
			responce = new DefaultServiceResponse(false);
			log.error("ERROR @ SplitSubSchedules ");
			ex.printStackTrace();
		}

		responce.addResponceParam(CommandParamNames.EXISITNG_SSM_SUB_SCHEDULES, existingSubSchedules);
		responce.addResponceParam(CommandParamNames.NEW_SSM_SUB_SCHEDULES, newSubSchedules);
		responce.addResponceParam(CommandParamNames.FLIGHT_SCHEDULE, schedule);

		return responce;
	}

	/**
	 * privte method to validate parameters
	 * 
	 * @param schedule
	 * @throws ModuleException
	 */
	private void checkParams(FlightSchedule schedule) throws ModuleException {
		if (schedule == null || schedule.getScheduleId()==null)// throw exception
			throw new ModuleException("airschedules.arg.invalid.null");
	}

	private Collection<SSMSplitFlightSchedule> convertScheduleToSubSchedule(Collection<SSMSplitFlightSchedule> subScheduleColl,
			FlightSchedule schedule) throws ModuleException {
		if (subScheduleColl == null || subScheduleColl.isEmpty()) {
			LocalZuluTimeAdder localZuluTimeAdder = new LocalZuluTimeAdder(AirSchedulesUtil.getAirportBD());
			localZuluTimeAdder.addLocalTimeDetailsToFlightSchedule(schedule, false);
			subScheduleColl = new ArrayList<SSMSplitFlightSchedule>();
			SSMSplitFlightSchedule splitFlightSchedule = new SSMSplitFlightSchedule();
			splitFlightSchedule.setStartDate(schedule.getStartDate());
			splitFlightSchedule.setStopDate(schedule.getStopDate());
			splitFlightSchedule.setStartDateLocal(schedule.getStartDateLocal());
			splitFlightSchedule.setStopDateLocal(schedule.getStopDateLocal());
			splitFlightSchedule.setScheduleId(schedule.getScheduleId());
			splitFlightSchedule.setSsmSplitFlightScheduleLeg(LegUtil.adaptScheduleLegs(schedule.getFlightScheduleLegs()));

			subScheduleColl.add(splitFlightSchedule);
		}
		return subScheduleColl;
	}

	private Set<SSMSplitFlightSchedule> tarnsformSchedulesToSSMSplitFlightSchedules(Collection<FlightSchedule> subSchedules,
			Integer scheduleId) throws ModuleException {
		Set<SSMSplitFlightSchedule> ssmSplitFlightSchedule = new HashSet<SSMSplitFlightSchedule>();
		if (scheduleId != null && subSchedules != null && !subSchedules.isEmpty()) {
			LocalZuluTimeAdder localZuluTimeAdder = new LocalZuluTimeAdder(AirSchedulesUtil.getAirportBD());
			for (FlightSchedule subSchedule : subSchedules) {

				SSMSplitFlightSchedule splitFlightSchedule = new SSMSplitFlightSchedule();
				splitFlightSchedule.setStartDate(subSchedule.getStartDate());
				splitFlightSchedule.setStopDate(subSchedule.getStopDate());
				localZuluTimeAdder.addLocalTimeDetailsToSubSchedule(subSchedule, splitFlightSchedule);
				splitFlightSchedule.setStartDateLocal(subSchedule.getStartDateLocal());
				splitFlightSchedule.setStopDateLocal(subSchedule.getStopDateLocal());

				splitFlightSchedule.setSsmSplitFlightScheduleLeg(LegUtil.adaptScheduleLegs(subSchedule.getFlightScheduleLegs()));
				splitFlightSchedule.setScheduleId(scheduleId);
				splitFlightSchedule.setVersion(-1);

				ssmSplitFlightSchedule.add(splitFlightSchedule);
			}
		}

		return ssmSplitFlightSchedule;
	}

	private void printSubSchedule(Collection<SSMSplitFlightSchedule> splitFlightSchedules) {
		if (splitFlightSchedules != null && !splitFlightSchedules.isEmpty()) {
			List<SSMSplitFlightSchedule> existingSubSchedules = new ArrayList<SSMSplitFlightSchedule>(splitFlightSchedules);
			Collections.sort(existingSubSchedules);
			for (SSMSplitFlightSchedule subSchedule : existingSubSchedules) {
				log.info("Sub-Schedule :" + CalendarUtil.formatDate(subSchedule.getStartDate(), CalendarUtil.PATTERN1) + " - "
						+ CalendarUtil.formatDate(subSchedule.getStopDate(), CalendarUtil.PATTERN1));
			}
		} else {
			log.info("Sub-Schedule : EMPTY");
		}
	}

	private boolean isScheduleSplitEnabledForDSTChanges(boolean subScheduleExist) {
		if (AppSysParamsUtil.isEnableSplitScheduleWhenDSTApplicable() || subScheduleExist) {
			return true;
		}
		return false;
	}
}
