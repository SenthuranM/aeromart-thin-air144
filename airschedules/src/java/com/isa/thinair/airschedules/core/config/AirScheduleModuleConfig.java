/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * AirScheduleModuleConfig.java
 *
 * ===============================================================================
 *
 *@Virsion $$Id$$
 */
package com.isa.thinair.airschedules.core.config;

import com.isa.thinair.platform.api.DefaultModuleConfig;

/**
 * @author Lasantha Pambagoda
 * @isa.module.config-bean
 */
public class AirScheduleModuleConfig extends DefaultModuleConfig {

	private String serviceTypeForScheduleFlight;

	private String serviceTypeForAdHokFlight;

	public String getServiceTypeForScheduleFlight() {
		return serviceTypeForScheduleFlight;
	}

	public void setServiceTypeForScheduleFlight(String serviceTypeForScheduleFlight) {
		this.serviceTypeForScheduleFlight = serviceTypeForScheduleFlight;
	}

	public String getServiceTypeForAdHokFlight() {
		return serviceTypeForAdHokFlight;
	}

	public void setServiceTypeForAdHokFlight(String serviceTypeForAdHokFlight) {
		this.serviceTypeForAdHokFlight = serviceTypeForAdHokFlight;
	}
}
