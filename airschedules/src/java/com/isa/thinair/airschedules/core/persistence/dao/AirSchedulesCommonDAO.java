/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.persistence.dao;

import java.util.Collection;

import com.isa.thinair.airschedules.api.model.OperationType;
import com.isa.thinair.airschedules.api.model.ScheduleBuildStatus;
import com.isa.thinair.airschedules.api.model.ScheduleStatus;

/**
 * @author Lasantha Pambagoda
 */
public interface AirSchedulesCommonDAO {

	public OperationType getOperationType(int id);

	public Collection<OperationType> getAllOpertationTypes();

	public Collection<ScheduleBuildStatus> getAllScheduleBuildStatus();

	public Collection<ScheduleStatus> getAllScheduleStatus();
}
