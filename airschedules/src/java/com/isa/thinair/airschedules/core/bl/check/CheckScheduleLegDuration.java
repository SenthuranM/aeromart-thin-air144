/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.check;

import java.util.Collection;
import java.util.Iterator;

import com.isa.thinair.airmaster.api.model.RouteInfo;
import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.FlightScheduleLeg;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.airschedules.core.bl.noncmd.DurationCalculator;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command to warn flight leg duration changes
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="checkScheduleLegDuration"
 */
public class CheckScheduleLegDuration extends DefaultBaseCommand {

	// BD's
	private CommonMasterBD commonMasterBD;

	// helpers
	DurationCalculator durationCalculator;

	/**
	 * constructor of the WarnFlightLegDuration command
	 */
	public CheckScheduleLegDuration() {

		// looking up daos
		commonMasterBD = AirSchedulesUtil.getCommonMasterBD();

		// helpers
		durationCalculator = new DurationCalculator();
	}

	/**
	 * execute method of the WarnFlightLegDuration command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {

		// getting command params
		FlightSchedule updated = (FlightSchedule) this.getParameter(CommandParamNames.FLIGHT_SCHEDULE);

		// checking params
		this.checkParams(updated);

		// constructing responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);

		Collection<FlightScheduleLeg> legs = updated.getFlightScheduleLegs();
		Iterator<FlightScheduleLeg> itLegs = legs.iterator();
		boolean tolaranceErrorFound = false;

		while (itLegs.hasNext()) {

			FlightScheduleLeg fleg = (FlightScheduleLeg) itLegs.next();

			// set the route info
			fleg.setModelRouteId(fleg.getOrigin() + "/" + fleg.getDestination());

			RouteInfo routeInfo = commonMasterBD.getRouteInfo(fleg.getModelRouteId());
			double allowedDureation = (routeInfo != null) ? routeInfo.getDuration() : 0;
			double errorPercentage = (routeInfo != null) ? routeInfo.getDurationTolerance().doubleValue() : 0;

			if (allowedDureation == 0) {

				responce.setSuccess(false);
				responce.setResponseCode(ResponceCodes.AllOWED_DURATION_ZERO);
				responce.addResponceParam(ResponceCodes.AllOWED_DURATION_ZERO, fleg);
				return responce;
			}

			boolean skipDurationValidation = false;
			if (updated.getViaScheduleMessaging() && AppSysParamsUtil.isSkipDurationToleranceForShedMsgs()) {
				skipDurationValidation = true;
			}
			if (!skipDurationValidation) {
				boolean hasError = durationCalculator.calculeteLegDuration(fleg, allowedDureation, errorPercentage);
				if (hasError)
					tolaranceErrorFound = hasError;
			}
		}

		if (tolaranceErrorFound) {

			responce.setSuccess(false);
			responce.setResponseCode(ResponceCodes.INVALID_LEG_DURATION);
			responce.addResponceParam(ResponceCodes.INVALID_LEG_DURATION, updated);
			return responce;
		}

		return responce;
	}

	/**
	 * privte method to validate parameters
	 * 
	 * @param schedule
	 * @throws ModuleException
	 */
	private void checkParams(FlightSchedule schedule) throws ModuleException {

		if (schedule == null)// throw exception
			throw new ModuleException("airschedules.arg.invalid.null");
	}
}
