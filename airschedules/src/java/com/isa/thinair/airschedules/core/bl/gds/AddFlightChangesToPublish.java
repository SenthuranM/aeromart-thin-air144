/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.gds;

import java.util.Collection;

import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightsToPublish;
import com.isa.thinair.airschedules.api.utils.AirScheduleCustomConstants;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.core.persistence.dao.FlightDAO;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.DataConverterUtil;
import com.isa.thinair.commons.core.util.XMLStreamer;
import com.isa.thinair.platform.api.ServiceResponce;


/**
 * command for the build flight schedule
 * 
 * @author Nafly Azhar
 * @isa.module.command name="addFlightChangesToPublish"
 */
public class AddFlightChangesToPublish extends DefaultBaseCommand{
	
	private final FlightDAO flightDAO;

	public AddFlightChangesToPublish(){
		// looking up daos
		flightDAO = (FlightDAO) AirSchedulesUtil.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_DAO);
	}
	
	
	@Override
	@SuppressWarnings("unchecked")
	public ServiceResponce execute() throws ModuleException {
		
		DefaultServiceResponse lastResponse = (DefaultServiceResponse) this.getParameter(CommandParamNames.LAST_RESPONSE);
		
		if(AppSysParamsUtil.isFlightDetailsPublishingEnabled()){
			//Get command Params
			Collection<Flight> flightsToPublish = (Collection<Flight>)this.getParameter(CommandParamNames.FLIGHTS_TO_PUBLISH);
			
			
			if(flightsToPublish != null){
				
				FlightsToPublish newUpdate = null;
				String flightDetails = null;
				
				for(Flight flight:flightsToPublish){
					
					if (flight.getOperationTypeId() != AirScheduleCustomConstants.OperationTypes.BUS_SERVICE) {// drop
																												// bus
						// if already available it will be updated with the latest changes.
						newUpdate = flightDAO.getFlightToPublish(flight.getFlightId());

						if (newUpdate == null) {
							newUpdate = new FlightsToPublish();
							// Set the default status to Pending
							newUpdate.setProcessStatus(FlightsToPublish.PENDING);
							newUpdate.setFlightId(flight.getFlightId());
						}

						// convert Flight object to XML String
						flightDetails = XMLStreamer.compose(flight);
						newUpdate.setFlightDetails(DataConverterUtil.createClob(flightDetails));
						newUpdate.setFlightEditTime(CalendarUtil.getCurrentSystemTimeInZulu());

						flightDAO.saveOrUpdateFlightsToPublish(newUpdate);
					}
				}
			}
		
		}
		
		if(lastResponse == null) {
			lastResponse = new DefaultServiceResponse(true);
		}
		return lastResponse;
	}
	
	
	

}
