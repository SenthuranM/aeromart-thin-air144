/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.noncmd;

import java.util.Date;

import com.isa.thinair.airschedules.api.model.Leg;
import com.isa.thinair.commons.core.util.CalendarUtil;

/**
 * class to calculate the dureation
 * 
 * @author Lasantha PAmbagoda
 */
public class DurationCalculator {

	public DurationCalculator() {
	}

	public boolean calculeteLegDuration(Leg leg, double allowedDureation, double errorPercentage) {

		boolean tolaranceErrorFound = false;

		double tolarance = (allowedDureation * errorPercentage / 100);
		double allowedMin = allowedDureation - tolarance;
		double allowedMax = allowedDureation + tolarance;

		Date estArrivalTimeZulu = CalendarUtil.getOfssetAddedTime(leg.getEstArrivalTimeZulu(), leg.getEstArrivalDayOffset());
		Date estDepartureTimeZulu = CalendarUtil
				.getOfssetAddedTime(leg.getEstDepartureTimeZulu(), leg.getEstDepartureDayOffset());

		double actualDuration = ((double) (estArrivalTimeZulu.getTime() - (double) estDepartureTimeZulu.getTime()) / 60000);

		if (actualDuration < allowedMin || actualDuration > allowedMax) {
			tolaranceErrorFound = true;
			leg.setDurationError((int) (actualDuration - allowedDureation));
		}

		leg.setDuration((int) actualDuration);

		return tolaranceErrorFound;
	}
}
