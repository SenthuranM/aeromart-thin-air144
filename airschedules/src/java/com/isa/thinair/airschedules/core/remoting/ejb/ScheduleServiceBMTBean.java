/**
 * 
 */
package com.isa.thinair.airschedules.core.remoting.ejb;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.interceptor.Interceptors;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;

import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.airschedules.core.service.bd.ScheduleBMTBDImpl;
import com.isa.thinair.airschedules.core.service.bd.ScheduleBMTBDLocalImpl;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.utils.CommandNames;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Command;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Session bean to provide Flight Schedule Service realted functionalities that needs user transactions
 * 
 * @author Chandana Kithalagama
 */
@Stateless
@RemoteBinding(jndiBinding = "ScheduleServiceBMT.remote")
@LocalBinding(jndiBinding = "ScheduleServiceBMT.local")
@RolesAllowed("user")
@TransactionManagement(TransactionManagementType.BEAN)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class ScheduleServiceBMTBean extends PlatformBaseSessionBean implements ScheduleBMTBDImpl, ScheduleBMTBDLocalImpl {

	private static final int SINGLE_SCHEDULE_BUILD_TRANSACTION_TIMEOUT = 15 * 60;

	private static final int ALL_SCHEDULES_BUILD_TIMEOUT = 20 * 60 * 1000;

	/**
	 * method to build schedule
	 */
	public ServiceResponce buildSchedulesBMT(Collection<Integer> scheduleIds, boolean buildDefaultInventory)
			throws ModuleException {

		boolean totalBuildSuccess = true;
		boolean oneSuccessful = false;

		DefaultServiceResponse response = new DefaultServiceResponse(true);

		Iterator<Integer> itScheduleId = scheduleIds.iterator();
		Date startBuild = new Date();

		while (itScheduleId.hasNext()) {

			Integer scheduleId = (Integer) itScheduleId.next();
			boolean success = this.buildSchedule(scheduleId, buildDefaultInventory);
			if (success) {
				this.integrateSchedule(scheduleId);
			}
			Date endBuild = new Date();
			long buildPeriod = endBuild.getTime() - startBuild.getTime();

			if (buildPeriod > ALL_SCHEDULES_BUILD_TIMEOUT) {

				totalBuildSuccess = false;
				break;
			}
			if (!success)
				totalBuildSuccess = false;
			else
				oneSuccessful = true;
		}

		if (totalBuildSuccess) {

			response.setResponseCode(ResponceCodes.BUILD_SCHEDULE_SUCCESSFULL);

		} else if (oneSuccessful) {

			response.setSuccess(false);
			response.setResponseCode(ResponceCodes.BUILD_SCHEDULE_PARTIALLY_SUCCESSFUL);

		} else {

			response.setSuccess(false);
			response.setResponseCode(ResponceCodes.BUILD_SCHEDULE_FAILED);
		}

		return response;
	}

	/**
	 * method to build schedule
	 * 
	 * @param buildDefaultInventory
	 */
	public boolean buildSchedule(Integer scheduleId, boolean buildDefaultInventory) throws ModuleException {

		try {

			this.sessionContext.getUserTransaction().setTransactionTimeout(SINGLE_SCHEDULE_BUILD_TRANSACTION_TIMEOUT);
			this.sessionContext.getUserTransaction().begin();
			Collection<Integer> scheduleIds = new ArrayList<Integer>();
			scheduleIds.add(scheduleId);

			Command command = (Command) AirSchedulesUtil.getInstance().getLocalBean(CommandNames.BUILD_FLIGHT_SCHEDULE_MACRO);
			command.setParameter(CommandParamNames.COMMAND_NAME, CommandNames.BUILD_FLIGHT_SCHEDULE_MACRO);
			command.setParameter(CommandParamNames.SCHEDULE_IDS, scheduleIds);
			command.setParameter(CommandParamNames.BUILD_DEFAULT_INVENTORY, buildDefaultInventory);
			command.setParameter(CommandParamNames.USER_PRINCIPAL, getUserPrincipal());
			command.execute();
			this.sessionContext.getUserTransaction().commit();

			return true;

		} catch (RollbackException ex) {

			return false;

		} catch (ModuleException ex) {

			try {
				this.sessionContext.getUserTransaction().rollback();
			} catch (SystemException se) {
				// nothing
			}
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());

		} catch (CommonsDataAccessException cdaex) {

			try {
				this.sessionContext.getUserTransaction().rollback();
			} catch (SystemException se) {
				// nothing
			}
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());

		} catch (Exception ex) {

			try {
				this.sessionContext.getUserTransaction().rollback();
			} catch (SystemException se) {
				ex = se;
			}
			throw new ModuleException(ex.getMessage());
		}
	}

	public boolean integrateSchedule(Integer scheduleId) throws ModuleException {

		try {

			this.sessionContext.getUserTransaction().setTransactionTimeout(SINGLE_SCHEDULE_BUILD_TRANSACTION_TIMEOUT);
			this.sessionContext.getUserTransaction().begin();
			Collection<Integer> scheduleIds = new ArrayList<Integer>();
			scheduleIds.add(scheduleId);

			Command command = (Command) AirSchedulesUtil.getInstance().getLocalBean(CommandNames.INTEGRATE_BUILD_SCHEDULE);
			command.setParameter(CommandParamNames.COMMAND_NAME, CommandNames.INTEGRATE_BUILD_SCHEDULE);
			command.setParameter(CommandParamNames.SCHEDULE_IDS, scheduleIds);
			command.setParameter(CommandParamNames.USER_PRINCIPAL, getUserPrincipal());
			command.execute();
			this.sessionContext.getUserTransaction().commit();
			return true;

		} catch (RollbackException ex) {

			return false;

		} catch (ModuleException ex) {

			try {
				this.sessionContext.getUserTransaction().rollback();
			} catch (SystemException se) {
				// nothing
			}
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());

		} catch (CommonsDataAccessException cdaex) {

			try {
				this.sessionContext.getUserTransaction().rollback();
			} catch (SystemException se) {
				// nothing
			}
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());

		} catch (Exception ex) {

			try {
				this.sessionContext.getUserTransaction().rollback();
			} catch (SystemException se) {
				ex = se;
			}
			throw new ModuleException(ex.getMessage());
		}
	}
}
