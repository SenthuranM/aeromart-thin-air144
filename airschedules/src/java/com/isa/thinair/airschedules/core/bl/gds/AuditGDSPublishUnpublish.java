package com.isa.thinair.airschedules.core.bl.gds;

import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Set;

import com.isa.thinair.airschedules.api.model.CodeShareMCFlight;
import com.isa.thinair.airschedules.api.model.CodeShareMCFlightSchedule;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.core.bl.audit.AuditParams;
import com.isa.thinair.auditor.api.model.Audit;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.TasksUtil;
import com.isa.thinair.commons.core.security.UserPrincipal;

public class AuditGDSPublishUnpublish {
	
	private static final String SYSTEM_USER = "SYSTEM";

	public static void publishUnpublishSchedule(FlightSchedule schedule, Collection<Integer> addedGDSIds,
			Collection<Integer> deletedGDSIds, UserPrincipal userPrincipal) throws ModuleException {

		if (addedGDSIds.size() > 0 || deletedGDSIds.size() > 0) {
			LinkedHashMap<String, Object> contents = new LinkedHashMap<String, Object>();
			String codeShareFlighttNo = getCodeShareFltNumStringForSchedule(schedule.getCodeShareMCFlightSchedules());
			String gdsIds = "";
			Audit audit = new Audit();
			audit.setTimestamp(new Date());
			if (userPrincipal != null) {
				audit.setUserId(userPrincipal.getUserId());
			} else {
				audit.setUserId(SYSTEM_USER);
			}
			if (addedGDSIds.size() > 0) {
				gdsIds = getGDSIdsString(addedGDSIds);
				audit.setTaskCode(TasksUtil.FLIGHT_PUBLISH_FLIGHT_SCHEDULE);
			} else if (deletedGDSIds.size() > 0) {
				gdsIds = getGDSIdsString(deletedGDSIds);
				audit.setTaskCode(TasksUtil.FLIGHT_UNPUBLISH_FLIGHT_SCHEDULE);
			}
			contents = createContentMap(schedule.getScheduleId(), schedule.getFlightNumber(), codeShareFlighttNo, gdsIds, true);
			AirSchedulesUtil.getAuditorBD().audit(audit, contents);
		}
	}
	
	public static void publishUnpublishFlight(Flight flight, Collection<Integer> addedGDSIds,
			Collection<Integer> deletedGDSIds, UserPrincipal userPrincipal) throws ModuleException {
		
		if (addedGDSIds.size() > 0 || deletedGDSIds.size() > 0) {
			LinkedHashMap<String, Object> contents = new LinkedHashMap<String, Object>();
			String codeShareFlighttNo = getCodeShareFltNumStringForFlight(flight.getCodeShareMCFlights());
			String gdsIds = "";
			Audit audit = new Audit();
			audit.setTimestamp(new Date());
			if (userPrincipal != null) {
				audit.setUserId(userPrincipal.getUserId());
			} else {
				audit.setUserId(SYSTEM_USER);
			}
			if (addedGDSIds.size() > 0) {
				gdsIds = getGDSIdsString(addedGDSIds);
				audit.setTaskCode(TasksUtil.FLIGHT_PUBLISH_FLIGHT);
				
			} else if (deletedGDSIds.size() > 0) {
				gdsIds = getGDSIdsString(deletedGDSIds);
				audit.setTaskCode(TasksUtil.FLIGHT_UNPUBLISH_FLIGHT);			
			}
			contents = createContentMap(flight.getFlightId(), flight.getFlightNumber(), codeShareFlighttNo, gdsIds, false);
			AirSchedulesUtil.getAuditorBD().audit(audit, contents);
		}
	}
	

	private static LinkedHashMap<String, Object> createContentMap(Integer id, String flightNo, String csFlightNumber,
			String gdsIDs, boolean isSchedule) {
		LinkedHashMap<String, Object> contents = new LinkedHashMap<String, Object>();
		if (isSchedule) {
			contents.put(AuditParams.SCHEDULE_ID, id);
		} else {
			contents.put(AuditParams.FLIGHT_ID, id);
		}
		contents.put(AuditParams.FLIGHT_NO, flightNo);
		contents.put(AuditParams.GDS_IDS, gdsIDs);

		if (!"".equals(csFlightNumber)) {
			contents.put(AuditParams.CS_FLIGHT_NO, csFlightNumber);
		}

		return contents;
	}
	
	private static String getCodeShareFltNumStringForSchedule(Set<CodeShareMCFlightSchedule> csMCFlightSchedules) {
		StringBuilder code = new StringBuilder("");
		if (csMCFlightSchedules != null && csMCFlightSchedules.size() > 0) {
			int i = 0;
			for (CodeShareMCFlightSchedule schedule : csMCFlightSchedules) {
				if (i > 0) {
					code.append(",");
				}
				code.append(schedule.getCsMCCarrierCode() + "|" + schedule.getCsMCFlightNumber());
				i++;
			}
		}
		return code.toString();
	}
	
	private static String getCodeShareFltNumStringForFlight(Set<CodeShareMCFlight> csMCFlights) {
		StringBuilder code = new StringBuilder("");
		if (csMCFlights != null && csMCFlights.size() > 0) {
			int i = 0;
			for (CodeShareMCFlight flight : csMCFlights) {
				if (i > 0) {
					code.append(",");
				}
				code.append(flight.getCsMCCarrierCode() + "|" + flight.getCsMCFlightNumber());
				i++;
			}
		}
		return code.toString();
	}
	
	private static String getGDSIdsString(Collection<Integer> gdsIds) {
		StringBuilder ids = new StringBuilder("");
		if (gdsIds != null && gdsIds.size() > 0) {
			int i = 0;
			for (Integer gdsId : gdsIds) {
				if (i > 0) {
					ids.append(",");
				}
				ids.append(gdsId.toString());
				i++;
			}
		}
		return ids.toString();
	}
}
