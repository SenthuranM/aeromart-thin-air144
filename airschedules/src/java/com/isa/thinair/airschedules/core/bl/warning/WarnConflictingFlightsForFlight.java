/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.warning;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import com.isa.thinair.airschedules.api.dto.FlightAllowActionDTO;
import com.isa.thinair.airschedules.api.dto.OverlappedFlightDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.airschedules.core.persistence.dao.FlightDAO;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for validate conflicting flights with given flight
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="warnConflictingFlightsForFlight"
 */
public class WarnConflictingFlightsForFlight extends DefaultBaseCommand {

	// Dao's
	private FlightDAO flightDAO;

	/**
	 * constructor of the ValidateCreateFlights command
	 */
	public WarnConflictingFlightsForFlight() {

		// looking up daos
		flightDAO = (FlightDAO) AirSchedulesUtil.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_DAO);
	}

	/**
	 * execute method of the ValidateCreateFlights command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {

		// getting command params
		Flight flight = (Flight) this.getParameter(CommandParamNames.FLIGHT);

		FlightAllowActionDTO allowDTO = (FlightAllowActionDTO) this.getParameter(CommandParamNames.FLIGHT_ALLOW_ACTION_DTO);

		DefaultServiceResponse warning = (DefaultServiceResponse) this.getParameter(CommandParamNames.WARNING);

		// checking params
		this.checkParams(flight, allowDTO);

		if (!allowDTO.isAllowConflicts()) {

			String flightNumber = flight.getFlightNumber();
			Date depatureDate = flight.getDepartureDate();
			Integer flightId = flight.getFlightId();

			Collection<Flight> conflicting = flightDAO.getConflictingFlights(flightNumber, depatureDate, flightId);

			if (conflicting.size() > 0) {

				if (warning == null)
					warning = new DefaultServiceResponse(false);
				warning.setResponseCode(ResponceCodes.WARNING_FOR_FLIGHT_UPDATE);

				Iterator<Flight> itConf = conflicting.iterator();
				ArrayList<OverlappedFlightDTO> list = new ArrayList<OverlappedFlightDTO>();

				while (itConf.hasNext()) {
					Flight overlapping = (Flight) itConf.next();

					OverlappedFlightDTO dto = new OverlappedFlightDTO();
					dto.setFlightNumber(overlapping.getFlightNumber());
					dto.setDepartureDate(overlapping.getDepartureDate());
					dto.setOperationType(overlapping.getOperationTypeId() + "");
					list.add(dto);
				}

				warning.addResponceParam(ResponceCodes.FLIGHT_FLIGHT_CONFLICT, list);
			}
		}

		// constructing responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);
		if (warning != null)
			responce.addResponceParam(CommandParamNames.WARNING, warning);
		// return command responce
		return responce;
	}

	/**
	 * privte method to validate parameters
	 * 
	 * @param schedule
	 * @throws ModuleException
	 */
	private void checkParams(Flight flight, FlightAllowActionDTO allowDTO) throws ModuleException {

		if (flight == null || allowDTO == null)// throw exception
			throw new ModuleException("airschedules.arg.invalid.null");
	}
}
