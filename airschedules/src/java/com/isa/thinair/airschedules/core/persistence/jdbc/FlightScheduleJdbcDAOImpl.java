package com.isa.thinair.airschedules.core.persistence.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airmaster.api.model.Gds;
import com.isa.thinair.airschedules.api.dto.EmailFlightSchedulesDTO;
import com.isa.thinair.airschedules.core.persistence.dao.FlightScheduleJdbcDAO;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * FlightScheduleDAOImpl is the IFlightScheduleJdbcDAO implementation
 * 
 * @author Duminda G
 * 
 */
public class FlightScheduleJdbcDAOImpl implements FlightScheduleJdbcDAO {

	private DataSource dataSource;
	private Properties queryString;

	@SuppressWarnings("unchecked")
	public Collection<EmailFlightSchedulesDTO> PublishFlightSchedule(String fromDate, String toDate) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());

		String sql = "SELECT s.flight_id, s.flt_seg_id, f.flight_number, s.status, "
				+ "       substr(s.segment_code,1,3) departure_airport, "
				+ "       substr(s.segment_code,length(s.segment_code)-2,3) arrival_airport, "
				+ "		to_char(s.est_time_departure_zulu,'dd/mm/yyyy HH24:mm:ss') est_time_departure_zulu, "
				+ "		to_char(s.est_time_arrival_zulu, 'dd/mm/yyyy HH24:mm:ss') est_time_arrival_zulu, f.model_number, "
				+ "       da.station_code d_city_code, aa.station_code a_city_code "
				+ "  FROM t_flight_segment s, t_flight f, t_airport da, t_airport aa " + "  WHERE f.flight_id = s.flight_id "
				+ " AND  est_time_arrival_zulu  " + " between  to_date('" + fromDate + "','dd/MM/yyyy HH24:mi:ss') and "
				+ " to_date('" + toDate + "','dd/MM/yyyy HH24:mi:ss')" + "    AND substr(s.segment_code,1,3) = da.airport_code "
				+ "    AND substr(s.segment_code,length(s.segment_code)-2,3) = aa.airport_code "
				+ "ORDER BY f.flight_number,s.flight_id,s.flt_seg_id ";

		Collection<EmailFlightSchedulesDTO> schedule = (Collection<EmailFlightSchedulesDTO>) jdbcTemplate.query(sql, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				Collection<EmailFlightSchedulesDTO> colEmailFlightSchedulesDTO = new ArrayList<EmailFlightSchedulesDTO>();

				if (rs != null) {
					while (rs.next()) {
						EmailFlightSchedulesDTO emailFlightSchedulesDTO = new EmailFlightSchedulesDTO();
						emailFlightSchedulesDTO.setFlight_id(rs.getInt("flight_id"));
						emailFlightSchedulesDTO.setFlight_number(rs.getString("flight_number"));
						emailFlightSchedulesDTO.setFlt_seg_id(rs.getInt("flt_seg_id"));
						emailFlightSchedulesDTO.setStatus(rs.getString("status"));
						emailFlightSchedulesDTO.setDeparture_airport(rs.getString("departure_airport"));
						emailFlightSchedulesDTO.setArrival_airport(rs.getString("arrival_airport"));
						emailFlightSchedulesDTO.setEst_time_departure_zulu(rs.getString("est_time_departure_zulu"));
						emailFlightSchedulesDTO.setEst_time_arrival_zulu(rs.getString("est_time_arrival_zulu"));
						emailFlightSchedulesDTO.setModel_number(rs.getString("model_number"));
						emailFlightSchedulesDTO.setD_city_code(rs.getString("d_city_code"));
						emailFlightSchedulesDTO.setA_city_code(rs.getString("a_city_code"));
						colEmailFlightSchedulesDTO.add(emailFlightSchedulesDTO);
					}
				}

				return colEmailFlightSchedulesDTO;
			}
		});

		return schedule;
	}


	@Override
	@SuppressWarnings("unchecked")
	public Set<Gds> getAutoPublishedGds(String origin, String destination) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());

		String ondConde = origin + "/" + destination;
		String originAnyDestination = origin + "/" + "***";
		String anyOriginToDestination = "***" + "/" + destination;

		String sql = "SELECT * FROM t_gds_auto_publish_routes join t_gds on t_gds_auto_publish_routes.gds_id = t_gds.gds_id where ( ond_code='"
				+ ondConde + "' OR ond_code='" + originAnyDestination + "' or ond_code='" + anyOriginToDestination
				+ "') AND SCHEDULING_SYSTEM ='N' AND t_gds.ENABLE_ROUTE_WISE_AUTO_PUBLISH='Y'";

		Set<Gds> publishedGds = (Set<Gds>) jdbcTemplate.query(sql, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				Set<Gds> publishedGdsSet = new HashSet<Gds>();

				if (rs != null) {
					while (rs.next()) {
						Gds gds = new Gds();
						gds.setGdsId(rs.getInt("gds_id"));
						publishedGdsSet.add(gds);
					}
				}

				return publishedGdsSet;
			}
		});

		return publishedGds;
	}
	
	@Override
	public void removeGdsAutopublishRoutes(String gdsId) throws ModuleException {
		
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		
		String sql = "DELETE FROM t_gds_auto_publish_routes WHERE gds_id = '"+ gdsId+"'";
		
		try{
			jdbcTemplate.execute(sql);
		}catch(Exception ex){
			throw new ModuleException(ex ,"error");
		}
	}
	
	

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public Properties getQueryString() {
		return queryString;
	}

	public void setQueryString(Properties queryString) {
		this.queryString = queryString;
	}


	@Override
	@SuppressWarnings({"unchecked","rawtypes"})
	public boolean showRollFwdButton(Integer flightId) throws ModuleException {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());

		boolean showRollFwdButton = false;

		String status = "CNX";

		StringBuilder sql = new StringBuilder();
		sql.append("select s.schedule_id from t_flight f,t_flight_schedule s ");
		sql.append("where f.schedule_id=s.schedule_id and ");
		sql.append("s.schedule_status_code=? and f.status=? and f.flight_id = ?");

		Integer scheduleId = (Integer) jdbcTemplate.query(sql.toString(), new Object[] { status, status, flightId },
				new ResultSetExtractor() {

					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

						Integer sId = null;

						if (rs != null) {
							while (rs.next()) {
								sId = rs.getInt("schedule_id");
							}
						}

						return sId;
					}
				});

		if (scheduleId != null && scheduleId > 0) {

			String newStatus = "ACT";

			StringBuilder querryForFltId = new StringBuilder();
			querryForFltId.append("select flight_id from t_flight where schedule_id= ? and status=?");

			List<Integer> activeFlights = new ArrayList<>();

			List<Integer> flights = (List<Integer>) jdbcTemplate.query(querryForFltId.toString(),
					new Object[] { scheduleId, newStatus }, new ResultSetExtractor() {

						public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

							if (rs != null) {
								while (rs.next()) {
									activeFlights.add(new Integer(rs.getInt("flight_id")));
								}
							}

							return activeFlights;
						}
					});

			if (flights.isEmpty()) {
				showRollFwdButton = true;
				return showRollFwdButton;
			}

		}

		return showRollFwdButton;

	}
	

}
