/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * @Virsion $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.operation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.AlertUtil;
import com.isa.thinair.airschedules.core.bl.email.FlightScheduleEmail;
import com.isa.thinair.airschedules.core.persistence.dao.FlightDAO;
import com.isa.thinair.airschedules.core.persistence.dao.FlightScheduleDAO;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.auditor.api.model.Audit;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.TasksUtil;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for invoke cancel action for those flights/schedules which has delayed SSM/ASM CNL actions.
 * 
 * @author M.Rikaz
 * @isa.module.command name="resumeCancelSchedule"
 */
public class ResumeCancelSchedule extends DefaultBaseCommand {

	// Dao's
	private FlightScheduleDAO flightSchedleDAO;
	private FlightDAO flightDAO;

	private static Log log = LogFactory.getLog(ResumeCancelSchedule.class);

	/**
	 * constructor of the CancelSchedule command
	 */
	public ResumeCancelSchedule() {

		// looking up daos
		flightDAO = (FlightDAO) AirSchedulesUtil.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_DAO);
		flightSchedleDAO = (FlightScheduleDAO) AirSchedulesUtil.getInstance()
				.getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_DAO);

	}

	/**
	 * execute method of the ResumeCancelSchedule command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {

		if (AppSysParamsUtil.isEnableDelayProcessingSsmAsmCnlAction()) {

			log.info("Invoking Waiting SSM/ASM CNL Actions ResumeCancelSchedule ");
			Date currentSystemTime = getParameter(CommandParamNames.HOLD_CANCEL_MSG_TILL, Date.class);

			if (currentSystemTime == null) {
				currentSystemTime = CalendarUtil.getCurrentZuluDateTime();
			}

			Collection<Integer> scheduleIds = flightSchedleDAO.getCancelledDelayedSchedulesId(currentSystemTime);
			if (scheduleIds != null && !scheduleIds.isEmpty()) {
				for (Integer scheduleId : scheduleIds) {
					FlightSchedule schedule = flightSchedleDAO.getFlightSchedule(scheduleId.intValue());
					try {
						ServiceResponce serviceResponce = AirSchedulesUtil.getScheduleBD()
								.cancelScheduleWithRequiresNew(scheduleId, true, AlertUtil.generateFlightAlertDTOForSsmAsmProcessing(true), false, true);
						if (serviceResponce.isSuccess()) {
							addAuditsForCancelledSchedules(schedule, true);
						} else {
							log.error("Unable to cancel the schedule - " + scheduleId );
							schedule.setRemarks("Unable to cancel the schedule");
							addAuditsForCancelledSchedules(schedule, false);
							sendScheduleCancellationOfficerNotificationEmail(schedule, 0 , false);
						}
					} catch (Exception e) {
						log.error("Error occured while cancellling schedule - " + scheduleId);
						schedule.setRemarks(e.getMessage());
						addAuditsForCancelledSchedules(schedule, false);
						sendScheduleCancellationOfficerNotificationEmail(schedule, 0 , false);
					}
				}
			}

			Collection<Integer> flightIds = flightDAO.getCancelledDelayedFlightsId(currentSystemTime);
			if (flightIds != null && !flightIds.isEmpty()) {
				for (Integer flightId : flightIds) {
					Flight flight = flightDAO.getFlight(flightId.intValue());
					try {
						ServiceResponce serviceResponce = AirSchedulesUtil.getFlightBD().cancelFlightWithRequiresNew(flightId,
								true, AlertUtil.generateFlightAlertDTOForSsmAsmProcessing(true));

						if (serviceResponce.isSuccess()) {
							addAuditsForCancelledFlights(flight, true);
						} else {
							log.error("Unable to cancel the flight - " + flightId);
							flight.setRemarks("Unable to cancel the flight - " + flightId);
							addAuditsForCancelledFlights(flight, false);
							sendFlightCancellationOfficerNotificationEmail(flight, false);
						}
					} catch (Exception e) {
						log.error("Error occured while cancellling flight - " + flightId);
						flight.setRemarks(e.getMessage());
						addAuditsForCancelledFlights(flight, false);
						sendFlightCancellationOfficerNotificationEmail(flight, false);
					}
				}
			}
		}
		log.info("End of Invoking Waiting SSM/ASM CNL Actions ResumeCancelSchedule ");
		// constructing command responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);

		return responce;
	}


	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void addAuditsForCancelledSchedules(FlightSchedule schedule, boolean success) {

		try {
			if (schedule != null) {

				Audit audit = new Audit();
				audit.setTimestamp(new Date());
				audit.setUserId("");
				audit.setTaskCode(String.valueOf(TasksUtil.PROCESS_SSM));

				LinkedHashMap contents = new LinkedHashMap();
				contents.put("action", "CNL");
				contents.put("in messege id", "");
				contents.put("flight designator", schedule.getStartDate());
				contents.put("start date", schedule.getStartDate());
				contents.put("end date", schedule.getStopDate());
				contents.put("schedule id", schedule.getScheduleId());
				contents.put("subMessage", "");
				if (success) {
					contents.put("status", "CANCEL SUCCESS");
				} else {
					contents.put("status", "ERROR IN CANCEL");
				}

				if (schedule.getRemarks() == null || schedule.getRemarks().isEmpty()) {
					contents.put("remarks", "SSM CNL Action resumed from Hold");
				} else {
					contents.put("remarks", schedule.getRemarks());
				}
				AirSchedulesUtil.getAuditorBD().audit(audit, contents);
			}

			Collection<Integer> scheduleIds = new ArrayList<Integer>();
			scheduleIds.add(schedule.getScheduleId());
			flightSchedleDAO.updateDelayCancelFlightSchedule(scheduleIds, false, null,
					StringUtil.trimStringMessage(schedule.getRemarks(), 300));

		} catch (Exception e) {			
			log.error("error while addAuditsForCancelledSchedules " + e.getMessage());
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void addAuditsForCancelledFlights(Flight flight, boolean success) {

		try {
			if (flight != null) {

				Audit audit = new Audit();
				audit.setTimestamp(new Date());
				audit.setUserId("");
				audit.setTaskCode(String.valueOf(TasksUtil.PROCESS_ASM));

				LinkedHashMap contents = new LinkedHashMap();
				contents.put("action", "CNL");
				contents.put("in messege id", "");
				contents.put("flight designator", flight.getDepartureDate());
				contents.put("flight id", flight.getFlightId());
				contents.put("subMessage", "");
				if (success) {
					contents.put("status", "CANCEL SUCCESS");
				} else {
					contents.put("status", "ERROR IN CANCEL");
				}

				if (flight.getRemarks() == null || flight.getRemarks().isEmpty()) {
					contents.put("remarks", "ASM CNL Action resumed from Hold");
				} else {
					contents.put("remarks", flight.getRemarks());
				}

				AirSchedulesUtil.getAuditorBD().audit(audit, contents);
			}
			Collection<Integer> flightIds = new ArrayList<Integer>();
			flightIds.add(flight.getFlightId());
			flightDAO.updateDelayCancelFlight(flightIds, false, null, StringUtil.trimStringMessage(flight.getRemarks(), 300));

		} catch (Exception e) {
			log.error("error while addAuditsForCancelledSchedules " + e.getMessage());
		}
	}
	
	private void sendFlightCancellationOfficerNotificationEmail(Flight cancelledFlight, boolean success)
			throws ModuleException {

		if (AppSysParamsUtil.isEnableOfficerEmailAlertAtFlightCnx()) {
			Collection<Flight> flightsToPublish = new ArrayList<Flight>();
			flightsToPublish.add(cancelledFlight);
			FlightScheduleEmail.sentFlightCancellationOfficerNotification(flightsToPublish, success);
		}
	}
	
	private void sendScheduleCancellationOfficerNotificationEmail(FlightSchedule schedule, int numberOfFlights, boolean success)
			throws ModuleException {
		if (AppSysParamsUtil.isEnableOfficerEmailAlertAtFlightCnx()) {
			FlightScheduleEmail.sendScheduleCancellationOfficerNotification(schedule, numberOfFlights, success);
		}
	}

}
