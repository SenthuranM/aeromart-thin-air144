package com.isa.thinair.airschedules.core.bl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.meal.FlightMealDTO;
import com.isa.thinair.airinventory.api.dto.seatmap.FlightSeatsDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.AnciDefaultTemplStatusEnum;
import com.isa.thinair.airschedules.core.persistence.dao.FlightDAO;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * @author Rikaz
 * 
 */
public class FlightScheduleBL implements Serializable {

	private static final long serialVersionUID = 7500043051722330473L;

	private static final Log log = LogFactory.getLog(FlightScheduleBL.class);

	private FlightDAO flightDAO;

	public FlightScheduleBL() {
		flightDAO = (FlightDAO) AirSchedulesUtil.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_DAO);
	}

	private static Collection<Integer> getFlightSegmentIdColl(Flight flight) {
		Collection<Integer> fltSegmentIdColl = new ArrayList<Integer>();
		if (flight != null && flight.getFlightSegements() != null) {
			Collection<FlightSegement> flightSegmentsColl = flight.getFlightSegements();

			Iterator<FlightSegement> flightSegmentsItr = flightSegmentsColl.iterator();

			while (flightSegmentsItr.hasNext()) {
				FlightSegement flightSegement = flightSegmentsItr.next();

				if (flightSegement.getFltSegId() != null)
					fltSegmentIdColl.add(flightSegement.getFltSegId());

			}
		}
		return fltSegmentIdColl;
	}

	public void assignDefaultMealChargesTemplate() throws ModuleException {

		String serviceType = null;
		List<Integer> scheduleIdsList = new ArrayList<Integer>();

		serviceType = ReservationInternalConstants.SERVICE_CALLER.MEAL;

		List<Flight> flightList = flightDAO.getFlightsByAnciDefTemplStatus(serviceType, AnciDefaultTemplStatusEnum.SCHEDULED);

		if (flightList != null && flightList.size() > 0) {
			Iterator<Flight> flightsItr = flightList.iterator();

			while (flightsItr.hasNext()) {

				Flight flightObj = flightsItr.next();

				Integer currentFlightId = flightObj.getFlightId();

				Collection<Integer> fltSegmentIdColl = getFlightSegmentIdColl(flightObj);
				Integer flightScheduleId = flightObj.getScheduleId();

				if (flightScheduleId != null && !scheduleIdsList.contains(flightScheduleId)) {
					scheduleIdsList.add(flightObj.getScheduleId());
				}

				if (flightObj.getMealTemplateId() != null) {

					flightDAO.updateFlightAnciAssignStatus(flightObj.getFlightId(), serviceType,
							AnciDefaultTemplStatusEnum.IN_PROGRESS);

					Collection<FlightMealDTO> colSegs = new HashSet<FlightMealDTO>();
					FlightMealDTO fltMealDto = null;

					Iterator<Integer> fltSegmentIdItr = fltSegmentIdColl.iterator();

					while (fltSegmentIdItr.hasNext()) {
						Integer flightSegtId = fltSegmentIdItr.next();
						fltMealDto = new FlightMealDTO();

						fltMealDto.setFlightSegmentID(flightSegtId);
						fltMealDto.setTemplateId(flightObj.getMealTemplateId());
						fltMealDto.setCabinClassCode("Y");
						colSegs.add(fltMealDto);

					}

					ServiceResponce serviceResp = null;
					try {
						serviceResp = AirSchedulesUtil.getMealBD().assignFlightMealCharges(colSegs, null, false);
						if (serviceResp != null && !serviceResp.isSuccess()) {
							updateErrors(currentFlightId, serviceType, "unable to assign");
						}
					} catch (Exception ex) {
						updateErrors(currentFlightId, serviceType, ex.toString());
					}

				}
			}

			updateFlightScheduleAnciAssignSummary(scheduleIdsList, serviceType);
		}

	}

	public void assignDefaultSeatMapChargesTemplate() throws ModuleException {

		String serviceType = null;
		List<Integer> scheduleIdsList = new ArrayList<Integer>();

		serviceType = ReservationInternalConstants.SERVICE_CALLER.SEAT_MAP;
		List<Flight> flightList = flightDAO.getFlightsByAnciDefTemplStatus(serviceType, AnciDefaultTemplStatusEnum.SCHEDULED);

		if (flightList != null && flightList.size() > 0) {
			Iterator<Flight> flightsItr = flightList.iterator();

			while (flightsItr.hasNext()) {

				Flight flightObj = flightsItr.next();
				Integer currentFlightId = flightObj.getFlightId();
				Integer flightScheduleId = flightObj.getScheduleId();

				if (flightScheduleId != null && !scheduleIdsList.contains(flightScheduleId)) {
					scheduleIdsList.add(flightObj.getScheduleId());
				}

				boolean seatMapTemplateExistForAirModel = flightDAO.isAirModelHasSeatTemplate(flightObj.getModelNumber());

				if (seatMapTemplateExistForAirModel) {
					Collection<Integer> fltSegmentIdColl = getFlightSegmentIdColl(flightObj);

					if (flightObj.getSeatChargeTemplateId() != null) {

						flightDAO.updateFlightAnciAssignStatus(flightObj.getFlightId(), serviceType,
								AnciDefaultTemplStatusEnum.IN_PROGRESS);

						Collection<FlightSeatsDTO> colSegs = new HashSet<FlightSeatsDTO>();
						FlightSeatsDTO fltSetDto = null;
						Iterator<Integer> fltSegmentIdItr = fltSegmentIdColl.iterator();

						while (fltSegmentIdItr.hasNext()) {

							Integer flightSegtId = fltSegmentIdItr.next();
							fltSetDto = new FlightSeatsDTO();
							fltSetDto.setSegmentCode(flightObj.getOriginAptCode() + "/" + flightObj.getDestinationAptCode());
							fltSetDto.setFlightSegmentID(flightSegtId);
							fltSetDto.setTemplateId(flightObj.getSeatChargeTemplateId());

							colSegs.add(fltSetDto);
						}

						ServiceResponce serviceResp = null;
						try {
							serviceResp = AirSchedulesUtil.getSeatMapBD().assignFlightSeatCharges(colSegs,
									flightObj.getModelNumber());
							if (serviceResp != null && !serviceResp.isSuccess()) {
								updateErrors(currentFlightId, serviceType, "unable to assign");
							}
						} catch (Exception ex) {
							updateErrors(currentFlightId, serviceType, ex.toString());
						}

					}
				}

			}

			updateFlightScheduleAnciAssignSummary(scheduleIdsList, serviceType);
		}

	}

	private void updateErrors(Integer flightId, String ancillaryType, String description) {
		if (flightId != null && ancillaryType != null) {
			flightDAO.updateFltAnciAssignStatusAsError(flightId, ancillaryType, description);
		}
	}

	private void updateFlightScheduleAnciAssignSummary(Collection<Integer> scheduleIdsList, String ancillaryType)
			throws ModuleException {

		// add flight schedule raw default anci template update summary
		addScheduleIdForMissingSummary(scheduleIdsList, ancillaryType);

		if (scheduleIdsList != null && scheduleIdsList.size() > 0) {
			Iterator<Integer> scheduleIdsItr = scheduleIdsList.iterator();

			while (scheduleIdsItr.hasNext()) {
				Integer scheduleId = scheduleIdsItr.next();
				HashMap<String, Integer> fltAnciStatusMap = flightDAO.defAnciAssignSummaryByFltScheduleId(scheduleId,
						ancillaryType);
				String summaryStr = "";
				int totalProcessed = 0;
				int success = 0;
				int failed = 0;
				int progress = 0;
				int scheduled = 0;

				String scheduleStatus = AnciDefaultTemplStatusEnum.SCHEDULED.getCode();

				if (fltAnciStatusMap != null && fltAnciStatusMap.size() > 0) {
					for (String anciStatus : fltAnciStatusMap.keySet()) {
						int anciFlightCount = fltAnciStatusMap.get(anciStatus);
						totalProcessed = totalProcessed + anciFlightCount;
						if (anciStatus.equals(AnciDefaultTemplStatusEnum.CREATED.getCode())) {
							success = anciFlightCount;
						} else if (anciStatus.equals(AnciDefaultTemplStatusEnum.CREATED_W_ERRORS.getCode())) {
							failed = anciFlightCount;
						} else if (anciStatus.equals(AnciDefaultTemplStatusEnum.SCHEDULED.getCode())) {
							scheduled = anciFlightCount;
						} else if (anciStatus.equals(AnciDefaultTemplStatusEnum.IN_PROGRESS.getCode())) {
							progress = anciFlightCount;
						}
					}
					summaryStr = AnciDefaultTemplStatusEnum.CREATED.getCode() + " : " + success + ",";
					summaryStr += AnciDefaultTemplStatusEnum.CREATED_W_ERRORS.getCode() + " : " + failed + ",";
					summaryStr += AnciDefaultTemplStatusEnum.IN_PROGRESS.getCode() + " : " + progress + ",";
					summaryStr += AnciDefaultTemplStatusEnum.SCHEDULED.getCode() + " : " + scheduled;
				}

				if (totalProcessed == success) {
					scheduleStatus = AnciDefaultTemplStatusEnum.CREATED.getCode();
				} else if (totalProcessed == scheduled) {
					scheduleStatus = AnciDefaultTemplStatusEnum.SCHEDULED.getCode();
				} else if (totalProcessed == progress) {
					scheduleStatus = AnciDefaultTemplStatusEnum.IN_PROGRESS.getCode();
				} else {
					scheduleStatus = AnciDefaultTemplStatusEnum.CREATED_W_ERRORS.getCode();
				}

				flightDAO.updateFltScheduleAnciAssignStatusSummary(scheduleId, ancillaryType, scheduleStatus, summaryStr);

			}

		}

	}

	private void addScheduleIdForMissingSummary(Collection<Integer> scheduleIdsList, String ancillaryType) {
		List<Integer> allSchedId = flightDAO.getAllBuildFltSchedIdByService(scheduleIdsList, ancillaryType);

		if (allSchedId != null && allSchedId.size() > 0) {
			scheduleIdsList.addAll(allSchedId);
		}
	}
}
