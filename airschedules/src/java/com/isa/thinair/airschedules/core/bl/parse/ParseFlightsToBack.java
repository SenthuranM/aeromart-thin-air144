/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.parse;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightLeg;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.AirportTerminalUtil;
import com.isa.thinair.airschedules.api.utils.LegUtil;
import com.isa.thinair.airschedules.api.utils.LocalZuluTimeAdder;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command to parse flights to back-end
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="parseFlightsToBack"
 */
public class ParseFlightsToBack extends DefaultBaseCommand {

	// BDs
	private AirportBD airportBD;

	/**
	 * constructor of the ParseFlightsToBack command
	 */
	public ParseFlightsToBack() {

		airportBD = AirSchedulesUtil.getAirportBD();
	}

	/**
	 * execute method of the ParseFlightsToBack command
	 * 
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	public ServiceResponce execute() throws ModuleException {

		// getting command params
		Collection<Flight> collection = (Collection<Flight>) this.getParameter(CommandParamNames.FLIGHT_LIST);
		Flight flight = (Flight) this.getParameter(CommandParamNames.FLIGHT);

		if (flight != null) {

			collection = new ArrayList<Flight>();
			collection.add(flight);
		}

		// check params
		this.checkParams(collection);

		// constructing command responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);

		// if flight list has flights
		if (collection.size() > 0) {

			LocalZuluTimeAdder timeAdder = new LocalZuluTimeAdder(airportBD);
			Iterator<Flight> itFlights = collection.iterator();
			while (itFlights.hasNext()) {

				Flight currentFlight = (Flight) itFlights.next();

				if (currentFlight.getDepartureDate() == null) {

					currentFlight = timeAdder.addZuluTimeDetailsforFlight(currentFlight);

				} else {

					currentFlight = timeAdder.addOnlySegementTimeDetails(currentFlight);
					FlightLeg firstLeg = LegUtil.getFirstFlightLeg(flight.getFlightLegs());
					currentFlight.setDepartureDate(CalendarUtil.getTimeAddedDate(currentFlight.getDepartureDate(),
							firstLeg.getEstDepartureTimeZulu()));
				}

				// add the terminal information to the flight legs.
				AirportTerminalUtil terminalUtil = new AirportTerminalUtil(airportBD);
				currentFlight = terminalUtil.addTerminalInformation(currentFlight);
			}

			if (flight != null) {

				responce.addResponceParam(CommandParamNames.FLIGHT, flight);

			} else if (collection != null) {

				responce.addResponceParam(CommandParamNames.FLIGHT_LIST, collection);
			}

		} else {
			// do nothing
		}

		// return command responce
		return responce;
	}

	/**
	 * privte method to validate parameters
	 * 
	 * @param schedule
	 * @throws ModuleException
	 */
	private void checkParams(Collection<Flight> flight) throws ModuleException {

		if (flight == null)// throw exception
			throw new ModuleException("airschedules.arg.invalid.null");
	}

}
