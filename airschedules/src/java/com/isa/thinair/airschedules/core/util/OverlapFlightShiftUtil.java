package com.isa.thinair.airschedules.core.util;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Set;

import com.isa.thinair.airschedules.api.dto.OverlapDeltaDTO;
import com.isa.thinair.airschedules.api.dto.OverlapLegDeltaDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightLeg;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.FlightScheduleLeg;
import com.isa.thinair.airschedules.api.utils.SegmentUtil;
import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.FrequencyUtil;

public class OverlapFlightShiftUtil {

	/**
	 * private method to find time difference(s) (delta) of overlapping leg(s) of two schedules populates a
	 * LinkedHashMap of OverlapLegDeltaDTO(s)
	 * 
	 * @param updated
	 * @param overlapping
	 * @return
	 */
	private static LinkedHashMap<String,OverlapLegDeltaDTO> getTimeDeltaForOverlappingSegments(FlightSchedule updated, FlightSchedule existing) {

		LinkedHashMap<String,OverlapLegDeltaDTO> legDeltaMap = new LinkedHashMap<String,OverlapLegDeltaDTO>();

		Collection<FlightScheduleLeg> updLegs = updated.getFlightScheduleLegs();
		Iterator<FlightScheduleLeg> itUpdLegs = updLegs.iterator();

		while (itUpdLegs.hasNext()) {

			FlightScheduleLeg updLeg = (FlightScheduleLeg) itUpdLegs.next();
			Collection<FlightScheduleLeg> extLegs = existing.getFlightScheduleLegs();
			Iterator<FlightScheduleLeg> itExtLeg = extLegs.iterator();

			while (itExtLeg.hasNext()) {

				FlightScheduleLeg extLeg = (FlightScheduleLeg) itExtLeg.next();
				if (updLeg.getOrigin().equals(extLeg.getOrigin()) && updLeg.getDestination().equals(extLeg.getDestination())
						&& SegmentUtil.isOverlappingLeg(updLeg, existing)) {

					Date updLegDepartureTime = CalendarUtil.getOfssetAddedTime(updLeg.getEstDepartureTimeZulu(),
							updLeg.getEstDepartureDayOffset());
					Date extLegDepartureTime = CalendarUtil.getOfssetAddedTime(extLeg.getEstDepartureTimeZulu(),
							extLeg.getEstDepartureDayOffset());
					Date updLegArrivalTime = CalendarUtil.getOfssetAddedTime(updLeg.getEstArrivalTimeZulu(),
							updLeg.getEstArrivalDayOffset());
					Date extLegArrivalTime = CalendarUtil.getOfssetAddedTime(extLeg.getEstArrivalTimeZulu(),
							extLeg.getEstArrivalDayOffset());
					long depDelta = updLegDepartureTime.getTime() - extLegDepartureTime.getTime();
					long arrDelta = updLegArrivalTime.getTime() - extLegArrivalTime.getTime();

					if (depDelta != 0 || arrDelta != 0) {

						OverlapLegDeltaDTO delta = new OverlapLegDeltaDTO();
						delta.setDepartureDeltaInMillis(depDelta);
						delta.setArrivalDeltaInMillis(arrDelta);
						String mapKey = updLeg.getOrigin() + "/" + updLeg.getDestination();

						legDeltaMap.put(mapKey, delta);
					}
				}
			}
		}
		return legDeltaMap;
	}

	private static OverlapDeltaDTO getOverlappingDeltaMapForAllLegs(LinkedHashMap<String,OverlapLegDeltaDTO> extUpdLegDeltaMap,
			FlightSchedule overlappingSche) {

		OverlapDeltaDTO overlapDTO = new OverlapDeltaDTO();
		LinkedHashMap<String,OverlapLegDeltaDTO> olpDeltaMappForAllLegs = new LinkedHashMap<String,OverlapLegDeltaDTO>();

		// init shift params
		int lowestLegNo = 0;
		int arrivalDelta = 0;
		int departureDelta = 0;
		int overlappingLegCount = 0;
		int startDateDelta = 0;

		boolean overlappedShifted = (extUpdLegDeltaMap != null && extUpdLegDeltaMap.size() > 0);

		// proceed only if overlap shift exists
		if (overlappedShifted) {

			// [step 1] shift the overlapping legs
			Iterator<String> keySet = extUpdLegDeltaMap.keySet().iterator();

			while (keySet.hasNext()) {

				String key = (String) keySet.next();
				String tokens[] = key.split("/");

				String origin = tokens[0];
				String destination = tokens[1];

				Set<FlightScheduleLeg> overlapLegs = overlappingSche.getFlightScheduleLegs();
				Iterator<FlightScheduleLeg> itOverlapLegs = overlapLegs.iterator();

				while (itOverlapLegs.hasNext()) {

					FlightScheduleLeg overlapLeg = (FlightScheduleLeg) itOverlapLegs.next();

					if (origin.equals(overlapLeg.getOrigin()) && destination.equals(overlapLeg.getDestination())) {

						OverlapLegDeltaDTO legDelta = (OverlapLegDeltaDTO) extUpdLegDeltaMap.get(key);
						legDelta.setLegNumber(overlapLeg.getLegNumber());
						olpDeltaMappForAllLegs.put(key, legDelta);

						overlappingLegCount++;
						if (lowestLegNo == 0) {
							lowestLegNo = overlapLeg.getLegNumber();
							arrivalDelta = (int) legDelta.getArrivalDeltaInMillis();
							departureDelta = (int) legDelta.getDepartureDeltaInMillis();
						} else if (overlapLeg.getLegNumber() < lowestLegNo) {
							lowestLegNo = overlapLeg.getLegNumber();
							arrivalDelta = (int) legDelta.getArrivalDeltaInMillis();
						} else {
							departureDelta = (int) legDelta.getDepartureDeltaInMillis();
						}

						if (overlapLeg.getLegNumber() == 1) {
							startDateDelta = departureDelta;
						}
					}
				}
			}

			Set<FlightScheduleLeg> overlapLegs = overlappingSche.getFlightScheduleLegs();
			Iterator<FlightScheduleLeg> itOvlLegs = overlapLegs.iterator();

			while (itOvlLegs.hasNext()) {

				FlightScheduleLeg ovlScheduleLeg = (FlightScheduleLeg) itOvlLegs.next();

				String key = ovlScheduleLeg.getOrigin() + "/" + ovlScheduleLeg.getDestination();
				int shiftDelta = 0;

				// if legNo less than lowest Overlapping LegNo
				if (ovlScheduleLeg.getLegNumber() < lowestLegNo)
					shiftDelta = departureDelta;
				// if legNo greater than highest Overlapping LegNo (= lowestLegNo + overlappingLegCount - 1 )
				else if (ovlScheduleLeg.getLegNumber() > (lowestLegNo + overlappingLegCount - 1))
					shiftDelta = arrivalDelta;

				if (!olpDeltaMappForAllLegs.containsKey(key)) {

					OverlapLegDeltaDTO legDelta = new OverlapLegDeltaDTO();

					legDelta.setArrivalDeltaInMillis(shiftDelta);
					legDelta.setDepartureDeltaInMillis(shiftDelta);
					legDelta.setLegNumber(ovlScheduleLeg.getLegNumber());

					olpDeltaMappForAllLegs.put(key, legDelta);

					if (ovlScheduleLeg.getLegNumber() == 1) {
						startDateDelta = shiftDelta;
					}
				}
			}
		}

		overlapDTO.setLegDeltaMap(olpDeltaMappForAllLegs);
		overlapDTO.setStartDateDelta(startDateDelta);

		return overlapDTO;
	}

	/**
	 * private method to get time difference(s) of ovelapping leg(s) of two flights returns a LinkedHashMap of
	 * OverlapLegDeltaDTOs
	 * 
	 * @param updated
	 * @param overlapping
	 * @return
	 */
	private static LinkedHashMap<String, OverlapLegDeltaDTO> getTimeDeltaForOverlappingSegments(Flight updated, Flight existing) {

		LinkedHashMap<String, OverlapLegDeltaDTO> legDeltaMap = new LinkedHashMap<String, OverlapLegDeltaDTO>();

		Set<FlightLeg> updLegs = updated.getFlightLegs();
		Iterator<FlightLeg> itUpdLeg = updLegs.iterator();

		while (itUpdLeg.hasNext()) {

			FlightLeg updleg = (FlightLeg) itUpdLeg.next();
			Set<FlightLeg> extLegs = existing.getFlightLegs();
			Iterator<FlightLeg> itExtLeg = extLegs.iterator();

			while (itExtLeg.hasNext()) {

				FlightLeg extleg = (FlightLeg) itExtLeg.next();

				// get the corresponding legs
				if (updleg.getOrigin().equals(extleg.getOrigin()) && updleg.getDestination().equals(extleg.getDestination())
						&& SegmentUtil.isOverlappingLeg(updleg, existing)) {

					Date updLegDepartureTime = CalendarUtil.getOfssetAndTimeAddedDate(updated.getDepartureDate(),
							updleg.getEstDepartureTimeZulu(), updleg.getEstDepartureDayOffset());
					Date extLegDepartureTime = CalendarUtil.getOfssetAndTimeAddedDate(existing.getDepartureDate(),
							extleg.getEstDepartureTimeZulu(), extleg.getEstDepartureDayOffset());
					Date updLegArrivalTime = CalendarUtil.getOfssetAndTimeAddedDate(updated.getDepartureDate(),
							updleg.getEstArrivalTimeZulu(), updleg.getEstArrivalDayOffset());
					Date extLegArrivalTime = CalendarUtil.getOfssetAndTimeAddedDate(existing.getDepartureDate(),
							extleg.getEstArrivalTimeZulu(), extleg.getEstArrivalDayOffset());

					long depDelta = updLegDepartureTime.getTime() - extLegDepartureTime.getTime();
					long arrDelta = updLegArrivalTime.getTime() - extLegArrivalTime.getTime();

					if (depDelta != 0 || arrDelta != 0) {

						OverlapLegDeltaDTO delta = new OverlapLegDeltaDTO();
						delta.setDepartureDeltaInMillis(depDelta);
						delta.setArrivalDeltaInMillis(arrDelta);
						String mapKey = updleg.getOrigin() + "/" + updleg.getDestination();

						legDeltaMap.put(mapKey, delta);
					}
				}
			}
		}

		return legDeltaMap;
	}

	private static OverlapDeltaDTO getOverlappingDeltaMapForAllLegs(LinkedHashMap<String, OverlapLegDeltaDTO> extUpdLegDeltaMap, Flight overlappingFlt) {

		OverlapDeltaDTO overlapDTO = new OverlapDeltaDTO();
		LinkedHashMap<String, OverlapLegDeltaDTO> olpDeltaMappForAllLegs = new LinkedHashMap<String, OverlapLegDeltaDTO>();

		// init shift params
		int lowestLegNo = 0;
		int arrivalDelta = 0;
		int departureDelta = 0;
		int overlappingLegCount = 0;
		int departureDateDelta = 0;

		boolean overlappedShifted = (extUpdLegDeltaMap != null && extUpdLegDeltaMap.size() > 0);

		// proceed only if overlap shift exists
		if (overlappedShifted) {

			// [step 1] shift the overlapping legs
			Iterator<String> keySet = extUpdLegDeltaMap.keySet().iterator();

			while (keySet.hasNext()) {

				String key = (String) keySet.next();
				String tokens[] = key.split("/");

				String origin = tokens[0];
				String destination = tokens[1];

				Set<FlightLeg> overlapLegs = overlappingFlt.getFlightLegs();
				Iterator<FlightLeg> itOverlapLegs = overlapLegs.iterator();

				while (itOverlapLegs.hasNext()) {

					FlightLeg overlapLeg = (FlightLeg) itOverlapLegs.next();

					if (origin.equals(overlapLeg.getOrigin()) && destination.equals(overlapLeg.getDestination())) {

						OverlapLegDeltaDTO legDelta = (OverlapLegDeltaDTO) extUpdLegDeltaMap.get(key);
						legDelta.setLegNumber(overlapLeg.getLegNumber());
						olpDeltaMappForAllLegs.put(key, legDelta);

						overlappingLegCount++;
						if (lowestLegNo == 0) {
							lowestLegNo = overlapLeg.getLegNumber();
							arrivalDelta = (int) legDelta.getArrivalDeltaInMillis();
							departureDelta = (int) legDelta.getDepartureDeltaInMillis();
						} else if (overlapLeg.getLegNumber() < lowestLegNo) {
							lowestLegNo = overlapLeg.getLegNumber();
							arrivalDelta = (int) legDelta.getArrivalDeltaInMillis();
						} else {
							departureDelta = (int) legDelta.getDepartureDeltaInMillis();
						}

						if (overlapLeg.getLegNumber() == 1) {
							departureDateDelta = departureDelta;
						}
					}
				}
			}

			Set<FlightLeg> overlapLegs = overlappingFlt.getFlightLegs();
			Iterator<FlightLeg> itOvlLegs = overlapLegs.iterator();

			while (itOvlLegs.hasNext()) {

				FlightLeg ovlFlightLeg = (FlightLeg) itOvlLegs.next();

				String key = ovlFlightLeg.getOrigin() + "/" + ovlFlightLeg.getDestination();
				int shiftDelta = 0;

				// if legNo less than lowest Overlapping LegNo
				if (ovlFlightLeg.getLegNumber() < lowestLegNo)
					shiftDelta = departureDelta;
				// if legNo greater than highest Overlapping LegNo (= lowestLegNo + overlappingLegCount - 1 )
				else if (ovlFlightLeg.getLegNumber() > (lowestLegNo + overlappingLegCount - 1))
					shiftDelta = arrivalDelta;

				if (!olpDeltaMappForAllLegs.containsKey(key)) {

					OverlapLegDeltaDTO legDelta = new OverlapLegDeltaDTO();

					legDelta.setArrivalDeltaInMillis(shiftDelta);
					legDelta.setDepartureDeltaInMillis(shiftDelta);
					legDelta.setLegNumber(ovlFlightLeg.getLegNumber());

					olpDeltaMappForAllLegs.put(key, legDelta);

					if (ovlFlightLeg.getLegNumber() == 1) {
						departureDateDelta = shiftDelta;
					}
				}
			}
		}

		overlapDTO.setLegDeltaMap(olpDeltaMappForAllLegs);
		overlapDTO.setStartDateDelta(departureDateDelta);

		return overlapDTO;
	}

	/**
	 * private method to update the ovelapping segment timings (overlapping schedule) to match with updated schedule
	 * overlapping segment timings.
	 * 
	 * @param overlapping
	 * @param updated
	 * @param existing
	 * @return
	 */

	public static long updateOverlappingFromUpdated(FlightSchedule overlappingSche, FlightSchedule updatedSche,
			FlightSchedule existingSche) throws ModuleException {

		long startDateShiftInMills = 0;
		// get overlap leg time change (update and existing)
		LinkedHashMap<String,OverlapLegDeltaDTO> legDeltaMap = OverlapFlightShiftUtil.getTimeDeltaForOverlappingSegments(updatedSche, existingSche);

		boolean overlappedShifted = (legDeltaMap != null && legDeltaMap.size() > 0);
		// proceed only if overlap shift exists
		if (overlappedShifted) {
			OverlapDeltaDTO overlapDeltaDTO = getOverlappingDeltaMapForAllLegs(legDeltaMap, overlappingSche);

			// update the overlapping schedule
			Date originalStartDate = overlappingSche.getStartDate();
			Date shiftedStartDate = CalendarUtil.getOfssetInMilisecondAddedDate(overlappingSche.getStartDate(),
					overlapDeltaDTO.getStartDateDelta());
			Date shiftedEndDate = CalendarUtil.getOfssetInMilisecondAddedDate(overlappingSche.getStopDate(),
					overlapDeltaDTO.getStartDateDelta());

			Frequency fqn = FrequencyUtil.getCopyOfFrequency(overlappingSche.getFrequency());
			int daysDiff = CalendarUtil.daysUntil(overlappingSche.getStartDate(), shiftedStartDate);
			fqn = FrequencyUtil.shiftFrequencyBy(fqn, daysDiff);

			overlappingSche.setStartDate(shiftedStartDate);
			overlappingSche.setStopDate(shiftedEndDate);
			overlappingSche.setFrequency(fqn);

			// update all overlapping schedule legs
			Set<FlightScheduleLeg> scheduleLegs = overlappingSche.getFlightScheduleLegs();
			Iterator<FlightScheduleLeg> itSchedLeg = scheduleLegs.iterator();

			while (itSchedLeg.hasNext()) {

				FlightScheduleLeg schedLeg = (FlightScheduleLeg) itSchedLeg.next();

				Date originalEstDepatureDate = CalendarUtil.getOfssetAndTimeAddedDate(originalStartDate,
						schedLeg.getEstDepartureTimeZulu(), schedLeg.getEstDepartureDayOffset());
				Date originalEstArrivalDate = CalendarUtil.getOfssetAndTimeAddedDate(originalStartDate,
						schedLeg.getEstArrivalTimeZulu(), schedLeg.getEstArrivalDayOffset());

				String key = schedLeg.getOrigin() + "/" + schedLeg.getDestination();
				OverlapLegDeltaDTO delta = (OverlapLegDeltaDTO) overlapDeltaDTO.getLegDeltaMap().get(key);

				Date shiftedEstDepatureDate = CalendarUtil.getOfssetInMilisecondAddedDate(originalEstDepatureDate,
						(int) delta.getDepartureDeltaInMillis());
				Date shiftedEstArrivalDate = CalendarUtil.getOfssetInMilisecondAddedDate(originalEstArrivalDate,
						(int) delta.getArrivalDeltaInMillis());

				int depatureOffsetAfterShift = CalendarUtil.daysUntil(shiftedStartDate, shiftedEstDepatureDate);
				Date depatureTimeAfterShift = CalendarUtil.getOnlyTime(shiftedEstDepatureDate);

				int arrivalOffsetAfterShift = CalendarUtil.daysUntil(shiftedStartDate, shiftedEstArrivalDate);
				Date arrivalTimeAfterShift = CalendarUtil.getOnlyTime(shiftedEstArrivalDate);

				schedLeg.setEstDepartureDayOffset(depatureOffsetAfterShift);
				schedLeg.setEstDepartureTimeZulu(depatureTimeAfterShift);

				schedLeg.setEstArrivalDayOffset(arrivalOffsetAfterShift);
				schedLeg.setEstArrivalTimeZulu(arrivalTimeAfterShift);
			}

			startDateShiftInMills = overlapDeltaDTO.getStartDateDelta();
		}

		return startDateShiftInMills;
	}

	/**
	 * private method to update overlapping flight with the overlapping leg timings of the updated flight
	 * 
	 * @param overlapping
	 * @param updated
	 * @param existing
	 * @return
	 */
	public static long updateOverlappingFromUpdated(Flight overlappingFlt, Flight updatedFlt, Flight existingFlt)
			throws ModuleException {

		long departureDateShiftInMills = 0;
		// get overlap leg time change (update and existing)
		LinkedHashMap<String,OverlapLegDeltaDTO> legDeltaMap = OverlapFlightShiftUtil.getTimeDeltaForOverlappingSegments(updatedFlt, existingFlt);

		boolean overlappedShifted = (legDeltaMap != null && legDeltaMap.size() > 0);
		// proceed only if overlap shift exists
		if (overlappedShifted) {
			OverlapDeltaDTO overlapDeltaDTO = getOverlappingDeltaMapForAllLegs(legDeltaMap, overlappingFlt);

			// update the overlapping flight
			Date originalDepartureDate = overlappingFlt.getDepartureDate();
			Date shiftedDepartureDate = CalendarUtil.getOfssetInMilisecondAddedDate(overlappingFlt.getDepartureDate(),
					overlapDeltaDTO.getStartDateDelta());

			overlappingFlt.setDepartureDate(shiftedDepartureDate);
			overlappingFlt.setScheduleId(null); // AARESAA-17249
			overlappingFlt.setDayNumber(CalendarUtil.getDayNumber(shiftedDepartureDate));

			// update all overlapping schedule legs
			Set<FlightLeg> flightLegs = overlappingFlt.getFlightLegs();
			Iterator<FlightLeg> itFltLeg = flightLegs.iterator();

			while (itFltLeg.hasNext()) {

				FlightLeg flightLeg = (FlightLeg) itFltLeg.next();

				Date originalEstDepatureDate = CalendarUtil.getOfssetAndTimeAddedDate(originalDepartureDate,
						flightLeg.getEstDepartureTimeZulu(), flightLeg.getEstDepartureDayOffset());
				Date originalEstArrivalDate = CalendarUtil.getOfssetAndTimeAddedDate(originalDepartureDate,
						flightLeg.getEstArrivalTimeZulu(), flightLeg.getEstArrivalDayOffset());

				String key = flightLeg.getOrigin() + "/" + flightLeg.getDestination();
				OverlapLegDeltaDTO delta = (OverlapLegDeltaDTO) overlapDeltaDTO.getLegDeltaMap().get(key);

				Date shiftedEstDepatureDate = CalendarUtil.getOfssetInMilisecondAddedDate(originalEstDepatureDate,
						(int) delta.getDepartureDeltaInMillis());
				Date shiftedEstArrivalDate = CalendarUtil.getOfssetInMilisecondAddedDate(originalEstArrivalDate,
						(int) delta.getArrivalDeltaInMillis());

				int depatureOffsetAfterShift = CalendarUtil.daysUntil(shiftedDepartureDate, shiftedEstDepatureDate);
				Date depatureTimeAfterShift = CalendarUtil.getOnlyTime(shiftedEstDepatureDate);

				int arrivalOffsetAfterShift = CalendarUtil.daysUntil(shiftedDepartureDate, shiftedEstArrivalDate);
				Date arrivalTimeAfterShift = CalendarUtil.getOnlyTime(shiftedEstArrivalDate);

				flightLeg.setEstDepartureDayOffset(depatureOffsetAfterShift);
				flightLeg.setEstDepartureTimeZulu(depatureTimeAfterShift);

				flightLeg.setEstArrivalDayOffset(arrivalOffsetAfterShift);
				flightLeg.setEstArrivalTimeZulu(arrivalTimeAfterShift);
			}

			departureDateShiftInMills = overlapDeltaDTO.getStartDateDelta();
		}

		return departureDateShiftInMills;
	}
}
