package com.isa.thinair.airschedules.core.bl.check;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import com.isa.thinair.airinventory.api.dto.FlightInventorySummaryDTO;
import com.isa.thinair.airinventory.api.service.FlightInventoryBD;
import com.isa.thinair.airmaster.api.model.AircraftModel;
import com.isa.thinair.airmaster.api.service.AircraftBD;
import com.isa.thinair.airschedules.api.dto.AffectedPeriod;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.core.bl.noncmd.FlightsForAffectedPeriod;
import com.isa.thinair.airschedules.core.persistence.dao.FlightDAO;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.airschedules.core.util.PeriodsUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command to check whether the aircraft model change is allowed
 * 
 * @author Chandana Kithalagama
 * @isa.module.command name="checkAircraftModel"
 */
public class CheckAircraftModel extends DefaultBaseCommand {

	private static final int CHECK_FOR_FLIGHTS = 0;
	private static final int CHECK_FOR_PERIODS = 1;

	// Dao's
	private FlightDAO flightDAO;

	// BD's
	private FlightInventoryBD flightInventryBD;
	private AircraftBD aircraftBD;

	// helpers
	private FlightsForAffectedPeriod fltsForAffectedPeriod;

	/**
	 * constructor of the CheckAircraftModel command
	 */
	public CheckAircraftModel() {

		// looking up daos
		flightDAO = (FlightDAO) AirSchedulesUtil.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_DAO);

		// looking up bd's
		flightInventryBD = AirSchedulesUtil.getFlightInventoryBD();

		aircraftBD = AirSchedulesUtil.getAircraftBD();

		// helpers
		fltsForAffectedPeriod = new FlightsForAffectedPeriod(flightDAO);
	}

	/**
	 * execute method of the CheckAircraftModel command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {

		// getting command params
		@SuppressWarnings("unchecked")
		Collection<AffectedPeriod> updatePeriods = (Collection<AffectedPeriod>) this
				.getParameter(CommandParamNames.UPDATE_PERIOD_LIST);
		FlightSchedule updated = (FlightSchedule) this.getParameter(CommandParamNames.FLIGHT_SCHEDULE);
		FlightSchedule existing = (FlightSchedule) this.getParameter(CommandParamNames.LOADED_EXISTING_SCHEDULE);
		FlightSchedule existingOverlapping = (FlightSchedule) this
				.getParameter(CommandParamNames.LOADED_EXISTING_OVERLAPPING_SCHEDULE);
		Boolean isOverwrite = (Boolean) this.getParameter(CommandParamNames.FLIGHT_OVERWRITE_FLAG);
		Flight updatedFlt = (Flight) this.getParameter(CommandParamNames.FLIGHT);
		Flight existingFlt = (Flight) this.getParameter(CommandParamNames.EXISTING_FLIGHT);
		Flight overlappingFlt = (Flight) this.getParameter(CommandParamNames.OVERLAPPING_FLIGHT);

		Boolean downGradeToAnyModel = (Boolean) this.getParameter(CommandParamNames.FLIGHT_DOWNGRADE_TO_ANY_MODEL);

		// checking params
		int type = this.checkParams(updatePeriods, updatedFlt);

		// constructing command responce
		DefaultServiceResponse response = new DefaultServiceResponse(true);

		// check periods for schedule
		if (type == CheckAircraftModel.CHECK_FOR_PERIODS) {

			for (AffectedPeriod period : updatePeriods) {
				AircraftModel model = aircraftBD.getAircraftModel(updated.getModelNumber());
				AircraftModel existingModel = aircraftBD.getAircraftModel(existing.getModelNumber());

				List<Integer> flightIds = fltsForAffectedPeriod.getFlightIdsForUpdatePeriod(period, updated.getScheduleId()
						.intValue(), isOverwrite.booleanValue());

				if (existing.isOverlapping() && existingOverlapping != null) {

					int millsDiff = (int) (existingOverlapping.getStartDate().getTime() - existing.getStartDate().getTime());
					int dayDiff = CalendarUtil.daysUntil(existing.getStartDate(), existingOverlapping.getStartDate());

					AffectedPeriod overlapPeriod = PeriodsUtil.getPeriodForOverlapping(period, millsDiff, dayDiff);

					FlightsForAffectedPeriod fltsForAffectedPeriod = new FlightsForAffectedPeriod(flightDAO);
					List<Integer> ovlFlightIds = fltsForAffectedPeriod.getFlightIdsForCancelPeriod(overlapPeriod, existing
							.getOverlapingScheduleId().intValue(), isOverwrite.booleanValue());

					if (ovlFlightIds.size() > 0) {
						flightIds.addAll(ovlFlightIds);
					}
				}

				// checking the model changes
				if (!existing.getModelNumber().equals(updated.getModelNumber())) {
					// TODO support for overwrite = true operation
					// where flight model differs from schedule model
					// [INVENTORY] change function to accept a collection of DTOs (flightid, model and existingModel)
					if (flightIds != null && flightIds.size() > 0) {
						// validation for seat map exist or not throw an error
						/*
						 * if (isFlightBooked(flightIds) && AppSysParamsUtil.isShowSeatMap()) { if
						 * (flightDAO.isSeatMapAttached(flightIds)) { response.setSuccess(false);
						 * response.setResponseCode(ResponceCodes.AIRCRAFT_UPDATED_MODEL_SEATATTACHED); return response;
						 * } }
						 */
						response = flightInventryBD.checkFCCInventoryModelUpdatability(flightIds, model, existingModel,
								downGradeToAnyModel, true);
					}
				}
			}
			// check for flights
		} else if (type == CheckAircraftModel.CHECK_FOR_FLIGHTS) {
			List<Integer> flightIds = new ArrayList<Integer>();
			if (updatedFlt != null) {
				flightIds.add(updatedFlt.getFlightId());
			}
			if (overlappingFlt != null) {
				flightIds.add(overlappingFlt.getFlightId());
			}

			AircraftModel model = aircraftBD.getAircraftModel(updatedFlt.getModelNumber());
			AircraftModel existingModel = aircraftBD.getAircraftModel(existingFlt.getModelNumber());
			// checking the model changes
			if (!existingFlt.getModelNumber().equals(updatedFlt.getModelNumber()) && flightIds != null && flightIds.size() > 0) {
				// validation for seat map exist or not throw an error
				/*
				 * if (isFlightBooked(flightIds) && AppSysParamsUtil.isShowSeatMap()) { if
				 * (flightDAO.isSeatMapAttached(flightIds) && flightDAO.isSeatMapSeatsUsed(flightIds) &&
				 * flightDAO.isSeatMapSeatsUsedWithFlight(flightIds)) { response.setSuccess(false);
				 * response.setResponseCode(ResponceCodes.AIRCRAFT_UPDATED_MODEL_SEATATTACHED); return response; } }
				 */
				response = flightInventryBD.checkFCCInventoryModelUpdatability(flightIds, model, existingModel,
						downGradeToAnyModel, false);
			}
		}
		return response;
	}

	private boolean isFlightBooked(List<Integer> flightIds) throws ModuleException {
		return getReservedSeatCount(flightIds) > 0;
	}
	
	private int getReservedSeatCount(List<Integer> flightIds) throws ModuleException {
		HashMap<Integer, FlightInventorySummaryDTO> inventrySummaryList = flightInventryBD.getFlightInventoriesSummary(flightIds, false);
		
		int resCount = 0;
		for (FlightInventorySummaryDTO dto : inventrySummaryList.values()) {
			resCount += dto.getOnholdAdultSeats();
			resCount += dto.getSoldAdultSeats();
		}
		return resCount;
	}

	/**
	 * privte method to validate parameters
	 * 
	 * @param schedule
	 * @throws ModuleException
	 */
	private int checkParams(Collection<AffectedPeriod> updatePeriods, Flight flight) throws ModuleException {

		int validationType = -1;

		if (flight != null) {

			validationType = CheckAircraftModel.CHECK_FOR_FLIGHTS;

		} else if (updatePeriods != null) {

			validationType = CheckAircraftModel.CHECK_FOR_PERIODS;

		} else {
			// throw exception
			throw new ModuleException("airschedules.arg.invalid.null");
		}

		return validationType;
	}

}
