/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;

import com.isa.thinair.airschedules.api.dto.AffectedPeriod;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.FrequencyUtil;

/**
 * utility class for periods
 * 
 * @author Lasantha Pambagoda
 */
public class PeriodsUtil extends ScheduledFlightUtil {

	/**
	 * returns the flights creation period for the given two schedules
	 * 
	 * @param existing
	 * @param updated
	 * @return
	 */
	public static Collection<AffectedPeriod> getCreatePeriods(FlightSchedule existing, FlightSchedule updated, int firstLegDayDiff) {

		Collection<AffectedPeriod> periods = new ArrayList<AffectedPeriod>();

		Date upStartDate = updated.getStartDate();
		Date upEndDate = updated.getStopDate();

		Date exStartDate = existing.getStartDate();
		Date exEndDate = existing.getStopDate();
		Frequency exFrequency = FrequencyUtil.getCopyOfFrequency(existing.getFrequency());
		
		if (firstLegDayDiff != 0) {
			exStartDate = CalendarUtil.addDateVarience(exStartDate, firstLegDayDiff);
			exEndDate = CalendarUtil.addDateVarience(exEndDate, firstLegDayDiff);
			exFrequency = FrequencyUtil.shiftFrequencyBy(exFrequency, firstLegDayDiff);
		}

		boolean excluded = false;

		// start date change creation period
		int updStToextStDiff = CalendarUtil.daysUntil(upStartDate, exStartDate);
		int extStToupdEndDiff = CalendarUtil.daysUntil(exStartDate, upEndDate);

		if (updStToextStDiff > 0 && extStToupdEndDiff > 0) {

			GregorianCalendar calander = new GregorianCalendar();
			calander.setTime(exStartDate);
			calander.add(GregorianCalendar.DATE, -1);
			AffectedPeriod period = new AffectedPeriod(upStartDate, calander.getTime(), updated.getFrequency(), false);
			periods.add(period);

		} else if (updStToextStDiff > 0 && extStToupdEndDiff < 0) {

			AffectedPeriod period = new AffectedPeriod(upStartDate, upEndDate, updated.getFrequency(), false);
			periods.add(period);
			excluded = true;
		} else if (updStToextStDiff > 0 && extStToupdEndDiff == 0) {

			AffectedPeriod period = new AffectedPeriod(upStartDate, CalendarUtil.addDateVarience(upEndDate, -1),
					updated.getFrequency(), false);
			periods.add(period);
			excluded = true;
		}

		// end date change creation period
		int updStToextEndDiff = CalendarUtil.daysUntil(upStartDate, exEndDate);
		int extEndToupdEnd = CalendarUtil.daysUntil(exEndDate, upEndDate);

		if (updStToextEndDiff > 0 && extEndToupdEnd > 0) {

			GregorianCalendar calander = new GregorianCalendar();
			calander.setTime(exEndDate);
			calander.add(GregorianCalendar.DATE, 1);
			AffectedPeriod period = new AffectedPeriod(calander.getTime(), upEndDate, updated.getFrequency(), false);
			periods.add(period);

		} else if (extEndToupdEnd > 0 && updStToextEndDiff < 0) {

			AffectedPeriod period = new AffectedPeriod(upStartDate, upEndDate, updated.getFrequency(), false);
			periods.add(period);
			excluded = true;
		} else if (extEndToupdEnd > 0 && updStToextEndDiff == 0) {

			AffectedPeriod period = new AffectedPeriod(CalendarUtil.addDateVarience(upStartDate, 1), upEndDate,
					updated.getFrequency(), false);
			periods.add(period);
			excluded = true;
		}

		if (!excluded) {

			// frequency change creation period
			Frequency fqn = FrequencyUtil.getRestOfTheFrequency(updated.getFrequency(), exFrequency);
			if (!FrequencyUtil.isAllFalse(fqn)) {

				Date startCreateDate = (updStToextStDiff < 0) ? upStartDate : exStartDate;
				Date endCreateDate = (extEndToupdEnd < 0) ? upEndDate : exEndDate;
				AffectedPeriod period = new AffectedPeriod(startCreateDate, endCreateDate, fqn, true);
				periods.add(period);
			}
		}

		return periods;
	}

	/**
	 * method to get update periods from given two schedules
	 * 
	 * @param existing
	 * @param updated
	 * @return
	 */
	public static Collection<AffectedPeriod> getUpdatePeriods(FlightSchedule existing, FlightSchedule updated, int firstLegDayDiff) {

		Collection<AffectedPeriod> periods = new ArrayList<AffectedPeriod>();

		Date upStartDate = updated.getStartDate();
		Date upEndDate = updated.getStopDate();

		Date exStartDate = existing.getStartDate();
		Date exEndDate = existing.getStopDate();
		
		if (firstLegDayDiff != 0) {
			exStartDate = CalendarUtil.addDateVarience(exStartDate, firstLegDayDiff);
			exEndDate = CalendarUtil.addDateVarience(exEndDate, firstLegDayDiff);
		}

		boolean excluded = false;

		int updStToextStDiff = CalendarUtil.daysUntil(upStartDate, exStartDate);
		int updEndToextStDiff = CalendarUtil.daysUntil(upEndDate, exStartDate);

		int extEndToupdStDiff = CalendarUtil.daysUntil(exEndDate, upStartDate);
		int extEndToupdEnd = CalendarUtil.daysUntil(exEndDate, upEndDate);

		if ((updStToextStDiff > 0 && updEndToextStDiff > 0) || (extEndToupdEnd > 0 && extEndToupdStDiff > 0)) {
			excluded = true;
		}

		if (!excluded) {
			// get update pereod
			Frequency fqn = updated.getFrequency();
			if (firstLegDayDiff == 0) {
				fqn = FrequencyUtil.getTrueOverlapFrequency(updated.getFrequency(), existing.getFrequency());
			}

			Date startCreateDate = (updStToextStDiff < 0) ? upStartDate : exStartDate;
			Date endCreateDate = (extEndToupdEnd < 0) ? upEndDate : exEndDate;
			AffectedPeriod period = new AffectedPeriod(startCreateDate, endCreateDate, fqn, true);

			periods.add(period);
		}

		if (extEndToupdStDiff == 0 && extEndToupdEnd > 0) {
			Frequency fqn = updated.getFrequency();
			if (firstLegDayDiff == 0) {
				fqn = FrequencyUtil.getTrueOverlapFrequency(updated.getFrequency(), existing.getFrequency());
			}
			AffectedPeriod period = new AffectedPeriod(upStartDate, upStartDate, fqn, true);
			periods.add(period);
		} else if (updEndToextStDiff == 0 && updStToextStDiff > 0) {
			Frequency fqn = updated.getFrequency();
			if (firstLegDayDiff == 0) {
				fqn = FrequencyUtil.getTrueOverlapFrequency(updated.getFrequency(), existing.getFrequency());
			}
			AffectedPeriod period = new AffectedPeriod(exStartDate, exStartDate, fqn, true);
			periods.add(period);
		}

		return periods;
	}

	/**
	 * method to get cancel periods from given two schedules
	 * 
	 * @param existing
	 * @param updated
	 * @return
	 */
	public static Collection<AffectedPeriod> getCancelPeriods(FlightSchedule existing, FlightSchedule updated, int firstLegDayDiff) {

		Collection<AffectedPeriod> periods = new ArrayList<AffectedPeriod>();

		Date upStartDate = updated.getStartDate();
		Date upEndDate = updated.getStopDate();

		Date exStartDate = existing.getStartDate();
		Date exEndDate = existing.getStopDate();
		Frequency exFrequency = FrequencyUtil.getCopyOfFrequency(existing.getFrequency());
		
		if (firstLegDayDiff != 0) {
			exStartDate = CalendarUtil.addDateVarience(exStartDate, firstLegDayDiff);
			exEndDate = CalendarUtil.addDateVarience(exEndDate, firstLegDayDiff);
			exFrequency = FrequencyUtil.shiftFrequencyBy(exFrequency, firstLegDayDiff);
		}

		boolean excluded = false;

		// start date change cancel period
		int extStToupdStDiff = CalendarUtil.daysUntil(exStartDate, upStartDate);
		int updStToextEndDiff = CalendarUtil.daysUntil(upStartDate, exEndDate);
		if (extStToupdStDiff > 0 && updStToextEndDiff > 0) {

			GregorianCalendar calander = new GregorianCalendar();
			calander.setTime(upStartDate);
			calander.add(GregorianCalendar.DATE, -1);
			calander.add(GregorianCalendar.DATE, (-1 * firstLegDayDiff));
			AffectedPeriod period = new AffectedPeriod(existing.getStartDate(), calander.getTime(), updated.getFrequency(), false);
			periods.add(period);

		} else if (extStToupdStDiff > 0 && updStToextEndDiff < 0) {

			AffectedPeriod period = new AffectedPeriod(existing.getStartDate(), existing.getStopDate(), existing.getFrequency(), false);
			periods.add(period);
			excluded = true;
		} else if (extStToupdStDiff > 0 && updStToextEndDiff == 0) {

			AffectedPeriod period = new AffectedPeriod(existing.getStartDate(), CalendarUtil.addDateVarience(
					existing.getStopDate(), -1), existing.getFrequency(), false);
			periods.add(period);
			excluded = true;
		}

		// end date change cancel period
		int extStToupdEndDiff = CalendarUtil.daysUntil(exStartDate, upEndDate);
		int updEndToextEndDiff = CalendarUtil.daysUntil(upEndDate, exEndDate);
		if (extStToupdEndDiff > 0 && updEndToextEndDiff > 0) {

			GregorianCalendar calander = new GregorianCalendar();
			calander.setTime(upEndDate);
			calander.add(GregorianCalendar.DATE, 1);
			calander.add(GregorianCalendar.DATE, (-1 * firstLegDayDiff));
			AffectedPeriod period = new AffectedPeriod(calander.getTime(), exEndDate, updated.getFrequency(), false);
			periods.add(period);

		} else if (extStToupdEndDiff < 0 && updEndToextEndDiff > 0) {

			AffectedPeriod period = new AffectedPeriod(exStartDate, existing.getStopDate(), existing.getFrequency(), false);
			periods.add(period);
			excluded = true;
		} else if (extStToupdEndDiff == 0 && updEndToextEndDiff > 0) {

			AffectedPeriod period = new AffectedPeriod(CalendarUtil.addDateVarience(exStartDate, 1), existing.getStopDate(),
					existing.getFrequency(), false);
			periods.add(period);
			excluded = true;
		}

		if (!excluded) {
			// frequency change creation period
			Frequency fqn = FrequencyUtil.getRestOfTheFrequency(exFrequency, updated.getFrequency());
			if (!FrequencyUtil.isAllFalse(fqn)) {

				Date startCreateDate = (extStToupdStDiff > 0) ? upStartDate : exStartDate;
				Date endCreateDate = (updEndToextEndDiff > 0) ? upEndDate : exEndDate;
				AffectedPeriod period = new AffectedPeriod(startCreateDate, endCreateDate, fqn, true);
				periods.add(period);
			}
		}

		return periods;
	}

	public static AffectedPeriod getPeriodForOverlapping(AffectedPeriod period, int mills, int fqnDiff) {

		AffectedPeriod newOverlapPeriod = new AffectedPeriod();

		// find the new frequency for the overlapping schedule
		Frequency newOverlapFqn = FrequencyUtil.getCopyOfFrequency(period.getFrequency());
		newOverlapFqn = FrequencyUtil.shiftFrequencyBy(newOverlapFqn, fqnDiff);

		// find the new start date for the overlapping flight
		Date newOverlapStartDate = CalendarUtil.getOfssetInMilisecondAddedDate(period.getStartDate(), mills);

		// find the new stop date for the overlapping flight
		Date newOverlapStopDate = CalendarUtil.getOfssetInMilisecondAddedDate(period.getEndDate(), mills);

		newOverlapPeriod.setStartDate(newOverlapStartDate);
		newOverlapPeriod.setEndDate(newOverlapStopDate);
		newOverlapPeriod.setFrequency(newOverlapFqn);
		newOverlapPeriod.setMiddlePeriod(period.isMiddlePeriod());

		return newOverlapPeriod;
	}
}
