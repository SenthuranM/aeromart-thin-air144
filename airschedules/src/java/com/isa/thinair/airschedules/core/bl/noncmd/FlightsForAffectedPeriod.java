/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.noncmd;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.isa.thinair.airschedules.api.dto.AffectedPeriod;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.core.persistence.dao.FlightDAO;
import com.isa.thinair.airschedules.core.util.ScheduledFlightUtil;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Lasantha Pambagoda
 * 
 */
public class FlightsForAffectedPeriod {

	// DAO's
	private FlightDAO flightDAO;

	public FlightsForAffectedPeriod(FlightDAO flightDAO) {
		this.flightDAO = flightDAO;
	}

	public List<Integer> getFlightIdsForCancelPeriod(AffectedPeriod period, int scheduleId, boolean includeManuallyChanged)
			throws ModuleException {

		List<Integer> flightIds = null;

		if (includeManuallyChanged) {

			if (period.isMiddlePeriod()) {

				Collection<Date> flyDates = ScheduledFlightUtil.getFlightDatesForSchedule(period);
				flightIds = flightDAO.getOverwritenFlightIdsForFlyDates(scheduleId, flyDates);

			} else {

				flightIds = flightDAO.getOverwritenFlightIdsForPeriod(scheduleId, period.getStartDate(), period.getEndDate());
			}

		} else {

			Collection<Date> flyDates = ScheduledFlightUtil.getFlightDatesForSchedule(period);
			flightIds = flightDAO.getNotOverwritenFlightIdsForFlyDates(scheduleId, flyDates);
		}

		return flightIds;
	}

	public List<Flight> getFlightsForCancelPeriod(AffectedPeriod period, int scheduleId, boolean includeManuallyChanged)
			throws ModuleException {

		List<Flight> flights = null;

		if (includeManuallyChanged) {

			if (period.isMiddlePeriod()) {

				List<Date> flyDates = (List<Date>) ScheduledFlightUtil.getFlightDatesForSchedule(period);
				flights = flightDAO.getOverwritenFlightsForFlyDates(scheduleId, flyDates);

			} else {

				flights = flightDAO.getOverwritenFlightsForPeriod(scheduleId, period.getStartDate(), period.getEndDate());
			}

		} else {

			List<Date> flyDates = (List<Date>) ScheduledFlightUtil.getFlightDatesForSchedule(period);
			flights = flightDAO.getNotOverwritenFlightsForFlyDates(scheduleId, flyDates);
		}

		return flights;
	}

	public List<Integer> getFlightIdsForUpdatePeriod(AffectedPeriod period, int scheduleId, boolean includeManuallyChanged)
			throws ModuleException {

		List<Integer> flightIds = null;

		if (includeManuallyChanged) {

			flightIds = flightDAO.getOverwritenFlightIdsForPeriod(scheduleId, period.getStartDate(), period.getEndDate());

		} else {

			Collection<Date> flyDates = ScheduledFlightUtil.getFlightDatesForSchedule(period);
			flightIds = flightDAO.getNotOverwritenFlightIdsForFlyDates(scheduleId, flyDates);
		}

		return flightIds;
	}

	public List<Flight> getFlightsForUpdatePeriod(AffectedPeriod period, int scheduleId, boolean includeManuallyChanged)
			throws ModuleException {

		List<Flight> flights = null;

		if (includeManuallyChanged) {
			// uncomment to get flights for the period
			/*
			 * flights = flightDAO.getOverwritenFlightsForPeriod(scheduleId, period.getStartDate(),
			 * period.getEndDate());
			 */
			// changed to get all flights for the schedule (period not considered)
			flights = flightDAO.getAllOverwritenFlights(scheduleId);

		} else {

			List<Date> flyDates = (List<Date>) ScheduledFlightUtil.getFlightDatesForSchedule(period);
			flights = flightDAO.getNotOverwritenFlightsForFlyDates(scheduleId, flyDates);
		}

		return flights;
	}
}
