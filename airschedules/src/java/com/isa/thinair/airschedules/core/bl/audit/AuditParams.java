package com.isa.thinair.airschedules.core.bl.audit;

public interface AuditParams {

	// common params
	public final static String FLIGHT_NO = "flight no";

	public final static String SOURCE_FLIGHT_NO = "source flight No";

	public final static String SOURCE_FLIGHT_DEPT_TIME = "source_flight_dept_date";

	public final static String ORIGIN_AIRPORT = "origin";

	public final static String DESTINATION_AIRPORT = "destination";

	public final static String OP_TYPE = "op type";

	public final static String AIRCRAFT_MODEL_NO = "model no";

	public final static String LEG_DETAILS = "leg details";

	public final static String SEGMENT_DETAILS = "segment details";

	public final static String USER_NOTES = "user notes";
	
	public final static String GDS_IDS = "gds ids";
	
	public final static String CS_FLIGHT_NO = "cs flight no";

	// schedule params
	public final static String SCHEDULE_ID = "schedule id";

	public final static String SCHEDULE_START_DATE = "start date";

	public final static String SCHEDULE_STOP_DATE = "stop date";

	public final static String FREQUENCY = "frequency";

	public final static String SCHEDULE_SPLIT_FROM = "split from";

	public final static String SCHEDULE_SPLIT_TO = "split to";

	public final static String SPLIT_FREQUENCY = "split frequency";

	public final static String OVERLAPPING_SCHEDULE_ID = "overlapping schedule id";
	
	public final static String NEW_SCHEDULE_IDS_FROM_SPLIT = "new schedule ids from split";

	// flight params public final static String FLIGHT_ID = "flight id";
	public final static String FLIGHT_ID = "flight id";

	public final static String DEPARTURE_DATE = "departure date";

	public final static String STOP_BOOKING_TIME = "stop booking time";

	public final static String COPIED_TO_DATE = "copied to";

	public final static String OVERLAPPING_FLIGHT_ID = "overlapping flight id";

	public final static String USER_NAME = "agent code";
	
	public final static String FLIGHT_TYPE = "flight type";
	
	public final static String STATUS = "status";



}
