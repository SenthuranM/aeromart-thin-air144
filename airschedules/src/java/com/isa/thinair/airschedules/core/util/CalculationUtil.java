/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.util;

import java.util.Collection;
import java.util.Iterator;

import com.isa.thinair.airmaster.api.model.AircraftCabinCapacity;

/**
 * utility class to provide calculations
 * 
 * @author Lasantha Pambagoda
 */
public class CalculationUtil {

	/**
	 * method to get the allocation for the flight
	 * 
	 * @param ccList
	 * @return allocation
	 */
	public static int getAllocation(Collection<AircraftCabinCapacity> ccList) {

		Iterator<AircraftCabinCapacity> itcc = ccList.iterator();
		int allocation = 0;
		while (itcc.hasNext()) {

			AircraftCabinCapacity cc = (AircraftCabinCapacity) itcc.next();
			allocation = allocation + cc.getSeatCapacity();
		}

		return allocation;
	}

	/**
	 * method to get avilable seat kilometers for flight
	 * 
	 * @param allocation
	 * @param flightDistance
	 * @return ask for flight
	 */
	public static int getASKforFlight(int allocation, double flightDistance) {

		return allocation * (int) flightDistance;
	}

	/**
	 * method to get avilable seat kilometers for flight
	 * 
	 * @param allocation
	 * @param flightDistance
	 * @return ask for flight
	 */
	public static int getASKforSchedule(int numberOfFlights, int allocation, double flightDistance) {

		return numberOfFlights * allocation * (int) flightDistance;
	}
}
