/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.gds;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.GDSSchedConstants;
import com.isa.thinair.airschedules.core.config.AirScheduleModuleConfig;
import com.isa.thinair.airschedules.core.persistence.dao.FlightScheduleDAO;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.gdsservices.api.dto.publishing.Envelope;
import com.isa.thinair.gdsservices.api.service.GDSPublishingBD;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for integrate create schedule with gds
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="integrateBuildSchedule"
 */
public class IntegrateBuildSchedule extends DefaultBaseCommand {

	// BDs
	private GDSPublishingBD gdsPublishingBD;
	private FlightBD flightBD;
	private final FlightScheduleDAO flightSchedleDAO;

	/**
	 * constructor of the ValidateCreateFlights command
	 */
	public IntegrateBuildSchedule() {

		// looking up BDs
		gdsPublishingBD = AirSchedulesUtil.getGDSPublishingBD();
		flightBD = AirSchedulesUtil.getFlightBD();
		flightSchedleDAO = (FlightScheduleDAO) AirSchedulesUtil.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_DAO);

	}

	/**
	 * execute method of the ValidateCreateFlights command
	 * 
	 * @throws ModuleException
	 */
	@SuppressWarnings("rawtypes")
	public ServiceResponce execute() throws ModuleException {

		DefaultServiceResponse lastResponse = (DefaultServiceResponse) this.getParameter(CommandParamNames.LAST_RESPONSE);
		if ("Y".equalsIgnoreCase(CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.GDS_INTEGRATED))) {

			HashMap buildFlightIds = (HashMap) this.getParameter(CommandParamNames.BUILDED_SCHEDULE_FLIGHT_IDS);
			if (buildFlightIds == null) {
				buildFlightIds = new HashMap<Integer, Collection<Integer>>();
				Collection<Integer> scheduleIds = (Collection<Integer>) this.getParameter(CommandParamNames.SCHEDULE_IDS);
				Iterator<Integer> it = scheduleIds.iterator();
				while (it.hasNext()) {

					// get the flight schedule to build
					int scheduleId = (it.next()).intValue();
					Collection<Flight> flights = flightSchedleDAO.getFlightsForSchedule(scheduleId);
					Collection<Integer> flightIds = new ArrayList<Integer>();
					for (Flight flight : flights) {
						flightIds.add(flight.getFlightId());
					}
					buildFlightIds.put(scheduleId, flightIds);
				}
			}

			String serviceType = ((AirScheduleModuleConfig) AirSchedulesUtil.getInstance().getModuleConfig())
					.getServiceTypeForScheduleFlight();

			Envelope env = new Envelope(GDSSchedConstants.TargetLogicNames.PUBLISH_SSM_NEW);
			env.addParam(GDSSchedConstants.ParamNames.SERVICE_TYPE, serviceType);
			env.addParam(GDSSchedConstants.ParamNames.FLIGHT_IDS_MAP_FOR_SCHEDULES, buildFlightIds);

			gdsPublishingBD.publishMessage(env);
			auditFlightSchedulePublish(buildFlightIds);
		}
		// return last command responce
		return lastResponse;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void auditFlightSchedulePublish(HashMap buildFlightIds) throws ModuleException {

		Iterator<Integer> it = buildFlightIds.keySet().iterator();
		try {
			while (it.hasNext()) {
				Integer scheduleId = it.next();
				Collection<Integer> flightIDs = (Collection<Integer>) buildFlightIds.get(scheduleId);
				FlightSchedule schedule = ((FlightScheduleDAO) AirSchedulesUtil.getInstance().getLocalBean(
						InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_DAO)).getFlightSchedule(scheduleId);
				Collection<Integer> addedGDSList = new ArrayList<Integer>();
				addedGDSList.addAll(schedule.getGdsIds());

				AuditGDSPublishUnpublish.publishUnpublishSchedule(schedule, addedGDSList, new ArrayList<Integer>(), null);

				for (Integer flightID : flightIDs ) {
					Flight flight = flightBD.getFlight(flightID);
					Collection<Integer> addedGDSListForFlight = new ArrayList<Integer>();
					addedGDSListForFlight.addAll(flight.getGdsIds());
					AuditGDSPublishUnpublish
							.publishUnpublishFlight(flight, addedGDSListForFlight, new ArrayList<Integer>(), null);
				}
			}
		} catch (ModuleException e) {
			//failed to record audit for publishing
			throw new ModuleException(e.getMessage());
		}
	}
}
