/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.check;

import java.util.ArrayList;
import java.util.Collection;

import com.isa.thinair.airinventory.api.service.FlightInventoryResBD;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.LegUtil;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for check add delete legs for flight with reservations
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="checkAddDeleteLegsForFlightWithRes"
 */
public class CheckAddDeleteLegsForFlightWithRes extends DefaultBaseCommand {

	private FlightInventoryResBD flightInventryResBD;

	/**
	 * constructor of the CheckAddDeleteLegsForFlightWithRes command
	 */
	public CheckAddDeleteLegsForFlightWithRes() {

		flightInventryResBD = AirSchedulesUtil.getFlightInventoryResBD();

	}

	/**
	 * execute method of the CheckAddDeleteLegsForFlightWithRes command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {

		// getting command params
		Flight updated = (Flight) this.getParameter(CommandParamNames.FLIGHT);
		Flight existing = (Flight) this.getParameter(CommandParamNames.EXISTING_FLIGHT);

		// checking params
		this.checkParams(updated, existing);

		// constructing command responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);

		boolean legsAddedDeleted = LegUtil.checkLegsAddedDeleted(existing, updated);

		if (legsAddedDeleted) {

			ArrayList<Integer> flightIds = new ArrayList<Integer>();
			flightIds.add(updated.getFlightId());

			Collection<Integer> resFlights = flightInventryResBD.filterFlightsHavingReservations(flightIds);
			if (resFlights.size() > 0) {

				responce.setSuccess(false);
				responce.setResponseCode(ResponceCodes.RESERVATIONS_FOUND_FOR_LEGCHANGED_FLIGHT);
				return responce;
			}
		}

		// return command responce
		return responce;
	}

	/**
	 * privte method to validate parameters
	 * 
	 * @param schedule
	 * @throws ModuleException
	 */
	private void checkParams(Flight flight, Flight existing) throws ModuleException {

		if (flight == null || existing == null)// throw exception
			throw new ModuleException("airschedules.arg.invalid.null");
	}
}
