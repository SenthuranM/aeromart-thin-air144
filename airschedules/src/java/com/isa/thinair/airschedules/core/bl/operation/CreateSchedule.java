/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.operation;

import java.util.Collection;
import java.util.Date;
import java.util.Set;

import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airschedules.api.dto.FlightAlertDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.FlightScheduleLeg;
import com.isa.thinair.airschedules.api.model.FlightScheduleSegment;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.LegUtil;
import com.isa.thinair.airschedules.api.utils.LocalZuluTimeAdder;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.airschedules.api.utils.ScheduleBuildStatusEnum;
import com.isa.thinair.airschedules.api.utils.ScheduleStatusEnum;
import com.isa.thinair.airschedules.core.bl.DefaultAnciTemplateAssignBL;
import com.isa.thinair.airschedules.core.bl.noncmd.DurationCalculator;
import com.isa.thinair.airschedules.core.persistence.dao.FlightScheduleDAO;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.airschedules.core.util.OverlapDetailUtil;
import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for the create flight schedule
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="createSchedule"
 */
public class CreateSchedule extends DefaultBaseCommand {

	// Dao's
	private FlightScheduleDAO flightSchedleDAO;

	
	//BDs
	private AirportBD airportBD;
	
	// helpers
	DurationCalculator durationCalculator;
	private LocalZuluTimeAdder timeAdder;

	/**
	 * constructor of the CreateSchedule command
	 */
	public CreateSchedule() {

		// looking up daos
		flightSchedleDAO = (FlightScheduleDAO) AirSchedulesUtil.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_DAO);

		airportBD = AirSchedulesUtil.getAirportBD();
		
		// helpers
		timeAdder = new LocalZuluTimeAdder(airportBD);
	}

	/**
	 * execute method of the CreateSchedule command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {

		// getting command params
		FlightSchedule schedule = (FlightSchedule) this.getParameter(CommandParamNames.FLIGHT_SCHEDULE);

		boolean isEnableAttachDefaultAnciTemplate = getParameter(CommandParamNames.IS_ENABLE_ATACH_DEFAULT_ANCI_TEMPL,
				Boolean.FALSE, Boolean.class);

		// checking params
		this.checkParams(schedule);

		// constructing command responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);

		// seting nessasary feilds
		schedule.setBuildStatusCode(ScheduleBuildStatusEnum.PENDING.getCode());
		schedule.setStatusCode(ScheduleStatusEnum.ACTIVE.getCode());
		schedule.setDepartureStnCode(LegUtil.getFirstScheduleLeg(schedule.getFlightScheduleLegs()).getOrigin());
		schedule.setArrivalStnCode(LegUtil.getLastScheduleLeg(schedule.getFlightScheduleLegs()).getDestination());

		boolean overlapping = schedule.isOverlapping();
		FlightSchedule overlappingSchedule = null;

		if (overlapping) {

			overlappingSchedule = flightSchedleDAO.getFlightSchedule(schedule.getOverlapingScheduleId().intValue());
			schedule = OverlapDetailUtil.addOverlapDetailsToSchedule(schedule, overlappingSchedule);
		}

		if (schedule.getViaScheduleMessaging() && isEnableAttachDefaultAnciTemplate) {
			DefaultAnciTemplateAssignBL.setDefaultAnciTemplatesToCreateFlightSchedule(schedule);
		}
		// saving the flight schedule
		flightSchedleDAO.saveFlightSchedule(schedule);
		
		//remove invalid schedules/days/frequency
		reviseScheduleFrequencyPeriodDeleteInvalidSchedules(schedule.getScheduleId());		

		if (schedule.getViaScheduleMessaging() && isEnableAttachDefaultAnciTemplate
				&& schedule.getBaggabeTemplateId() != null) {
			Collection<Flight> flightCol = flightSchedleDAO.getFlightsForSchedule(schedule.getScheduleId());
			DefaultAnciTemplateAssignBL.updadeFlightBaggages(flightCol, schedule.getModelNumber());
		}

		// if schedule is overlapping load the overlapping schedule and save it
		if (overlapping && overlappingSchedule != null) {

			overlappingSchedule = OverlapDetailUtil.addOverlapDetailsToSchedule(overlappingSchedule, schedule);
			// save the overlapping segment
			flightSchedleDAO.saveFlightSchedule(overlappingSchedule);
		}

		// return command responce
		if (responce.isSuccess()) {
			responce.setResponseCode(ResponceCodes.CREATE_SCHEDULE_SUCCESSFULL);
			responce.addResponceParam(CommandParamNames.LAST_RESPONSE, responce);
			responce.addResponceParam(CommandParamNames.SCHEDULE_ID, schedule.getScheduleId());
		}
		return responce;
	}

	/**
	 * privte method to validate parameters
	 * 
	 * @param schedule
	 * @throws ModuleException
	 */
	private void checkParams(FlightSchedule schedule) throws ModuleException {

		if (schedule == null)// throw exception
			throw new ModuleException("airschedules.arg.invalid.null");

		Set<FlightScheduleLeg> legs = schedule.getFlightScheduleLegs();
		Set<FlightScheduleSegment> segs = schedule.getFlightScheduleSegments();

		if (legs == null || legs.size() == 0 || segs == null || segs.size() == 0) // throw exception
			throw new ModuleException("airschedules.arg.invalid");
	}
	
	private void reviseScheduleFrequencyPeriodDeleteInvalidSchedules(Integer scheduleId) throws ModuleException {

		if (scheduleId != null) {

			FlightSchedule schedule = flightSchedleDAO.getFlightSchedule(scheduleId.intValue());

			boolean freqMatched = CalendarUtil.frequencyMatchesDateRange(schedule.getStartDate(), schedule.getStopDate(),
					schedule.getFrequency());
			boolean isRemoved = false;
			// schedule frequency will be revised if its invalid when processing SSM/SSIM
			if (!freqMatched) {
				Frequency frequency = CalendarUtil.getValidFrequencyMatchesDateRange(schedule.getStartDate(),
						schedule.getStopDate(), schedule.getFrequency());
				if (!reValidateRevisedFrequency(frequency)) {
					// cancel schedule
					AirSchedulesUtil.getScheduleBD().cancelSchedule(scheduleId, true, new FlightAlertDTO(), false, false);

					isRemoved = true;
				} else {
					schedule.setFrequency(frequency);
					flightSchedleDAO.saveFlightSchedule(schedule);
				}
			}

			if (!isRemoved && trimSchedulePeriod(schedule)) {
				FlightSchedule scheduleTrim = flightSchedleDAO.getFlightSchedule(scheduleId.intValue());
				scheduleTrim.setStartDate(CalendarUtil.getEffectiveStartDate(schedule.getStartDate(), schedule.getFrequency()));
				scheduleTrim
						.setStartDateLocal(timeAdder.getLocalDateTime(schedule.getDepartureStnCode(), schedule.getStartDate()));
				scheduleTrim.setStopDate(CalendarUtil.getEffectiveStopDate(schedule.getStopDate(), schedule.getFrequency()));
				scheduleTrim.setStopDateLocal(timeAdder.getLocalDateTime(schedule.getDepartureStnCode(), schedule.getStopDate()));
				flightSchedleDAO.saveFlightSchedule(scheduleTrim);
			}

		}
	}

	private boolean trimSchedulePeriod(FlightSchedule schedule) {
		Date splitStartDateZulu = schedule.getStartDate();
		Date splitEndDateZulu = schedule.getStopDate();
		Frequency splitFrequencyZulu = schedule.getFrequency();
		Frequency frequency = CalendarUtil.getValidFrequencyMatchesDateRange(splitStartDateZulu, splitEndDateZulu,
				splitFrequencyZulu);

		Date firstFlightDate = CalendarUtil.getEffectiveStartDate(splitStartDateZulu, frequency);
		Date lastFlightDate = CalendarUtil.getEffectiveStopDate(splitEndDateZulu, frequency);

		if (!CalendarUtil.isSameDay(splitStartDateZulu, firstFlightDate)
				|| !CalendarUtil.isSameDay(splitEndDateZulu, lastFlightDate)) {
			return true;
		}
		return false;
	}

	private boolean reValidateRevisedFrequency(Frequency frequency) {
		if (frequency.getDayCount(true) > 0) {
			return true;
		}
		return false;
	}

}
