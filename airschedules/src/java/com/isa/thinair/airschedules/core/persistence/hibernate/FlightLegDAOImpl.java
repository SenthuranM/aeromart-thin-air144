/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:17:34
 * 
 * $Id$
 * 
 * ===============================================================================
 */

package com.isa.thinair.airschedules.core.persistence.hibernate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airschedules.api.model.FlightLeg;
import com.isa.thinair.airschedules.core.persistence.dao.FlightLegDAO;
import com.isa.thinair.airschedules.core.util.LookUpUtils;
import com.isa.thinair.commons.core.framework.PlatformHibernateDaoSupport;
import com.isa.thinair.commons.core.util.Util;

/**
 * This class is responcible for provide all the data access methods for the Flight Leg
 * 
 * @author Lasantha Pambagoda
 * @isa.module.dao-impl dao-name="flightLegDAO"
 */
public class FlightLegDAOImpl extends PlatformHibernateDaoSupport implements FlightLegDAO {

	/**
	 * metod to get all flight legs
	 */
	public List<FlightLeg> getFlightLegs() {
		return find("from FlightLeg", FlightLeg.class);
	}

	/**
	 * method to get the flight leg
	 */
	public FlightLeg getFlightLeg(int id) {
		return (FlightLeg) get(FlightLeg.class, new Integer(id));
	}

	/**
	 * method to save flight leg
	 */
	public void saveFlightLeg(FlightLeg flightleg) {
		hibernateSaveOrUpdate(flightleg);
	}

	/**
	 * method to remove flight leg
	 */
	public void removeFlightLeg(int id) {
		Object flightleg = load(FlightLeg.class, new Integer(id));
		delete(flightleg);
	}

	/**
	 * public method to return used airports in flight and schedule legs
	 * 
	 * @param airports
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public HashMap<String,String> getUsedAirports(Collection<String> airports) {
		String sql = "SELECT A.airport_code " + " FROM T_AIRPORT A " + " WHERE A.airport_code IN ("
				+ Util.buildStringInClauseContent(airports) + ")" + " AND ( EXISTS (  SELECT L.flight_leg_id "
				+ " 					FROM t_flight_leg L " + "   			   WHERE L.origin = A.airport_code "
				+ "     				  OR L.destination = A.airport_code ) " + "       OR  EXISTS( SELECT S.fsl_id "
				+ " 					  FROM t_flight_schedule_leg S " + "             		 WHERE S.origin = A.airport_code "
				+ "                       OR S.destination = A.airport_code ) " + " 	  )";

		JdbcTemplate jt = new JdbcTemplate(LookUpUtils.getDatasource());

		HashMap<String, String> usedAirports = (HashMap<String, String>) jt.query(sql, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				HashMap<String, String> usedAirports = new HashMap<String, String>();
				if (rs != null) {
					while (rs.next()) {
						usedAirports.put(rs.getString("airport_code"), "1");
					}
				}
				return usedAirports;
			}
		});
		return usedAirports;
	}

	/**
	 * Returns the list of airport code sequence the given flightId
	 * 
	 * @param flightId
	 */
	@SuppressWarnings("unchecked")
	public List<String> getAirportSequenceListForFlight(int flightId) {

		String sql = "SELECT tfl.origin, tfl.destination FROM T_FLIGHT_LEG tfl" + " WHERE tfl.flight_id = " + flightId
				+ " ORDER BY tfl.est_time_departure_zulu";

		JdbcTemplate jt = new JdbcTemplate(LookUpUtils.getDatasource());

		List<String> airportsSeq = (List<String>) jt.query(sql, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				List<String> airportsSeq = new ArrayList<String>();
				if (rs != null) {
					while (rs.next()) {

						if (!airportsSeq.contains(rs.getString("origin"))) {
							airportsSeq.add(rs.getString("origin"));
						}
						if (!airportsSeq.contains(rs.getString("destination"))) {
							airportsSeq.add(rs.getString("destination"));
						}

					}
				}
				return airportsSeq;
			}
		});
		return airportsSeq;
	}

}
