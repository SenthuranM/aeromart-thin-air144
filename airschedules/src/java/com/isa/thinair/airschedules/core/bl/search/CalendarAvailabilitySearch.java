package com.isa.thinair.airschedules.core.bl.search;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableIBOBFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.CalendarSearchRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.converters.AvailabilityConvertUtil;
import com.isa.thinair.airschedules.api.dto.AvailableONDFlight;
import com.isa.thinair.airschedules.api.dto.CalendarFlightRS;
import com.isa.thinair.airschedules.api.dto.LogicalCabinAvailability;
import com.isa.thinair.airschedules.api.dto.SegmentInvAvailability;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.utils.CommandNames;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Command;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * 
 * @isa.module.command name="calendarAvailabilitySearch"
 */
public class CalendarAvailabilitySearch extends DefaultBaseCommand {

	private CalendarSearchRQ calendarSearchRQ;

	@Override
	public ServiceResponce execute() throws ModuleException {
		this.calendarSearchRQ = (CalendarSearchRQ) this.getParameter(CommandParamNames.CALENDAR_SEARCH_REQUEST);
		Command command = (Command) AirSchedulesUtil.getInstance().getLocalBean(CommandNames.FLIGHT_AVAILABILITY_SEARCH);
		AvailableFlightSearchDTO avifltSearchDTO = createAvailSearch();
		command.setParameter(CommandParamNames.AVILABLE_FLIGHT_SEARCH_DTO, avifltSearchDTO);
		command.setParameter(CommandParamNames.SEARCH_OUT_BOUND_FLIGHTS, Boolean.TRUE);
		ServiceResponce response = command.execute();

		AvailableFlightDTO availableFlightDTO = (AvailableFlightDTO) response
				.getResponseParam(ResponceCodes.AVAILABILITY_SEARCH_SUCCESSFULL);

		CalendarFlightRS availFlightRS = new CalendarFlightRS();
		Set<Integer> ondSequences = availableFlightDTO.getOndSequenceIds();
		for (Integer ondSeq : ondSequences) {
			List<AvailableIBOBFlightSegment> ondFlights = availableFlightDTO.getAvailableOndFlights(ondSeq);
			for (AvailableIBOBFlightSegment ondFlight : ondFlights) {
				AvailableONDFlight avlONDFlight = convertOndFlight(ondFlight);
				availFlightRS.addOndFlight(ondSeq, avlONDFlight);
			}
		}
		ServiceResponce finalResponse = new DefaultServiceResponse();
		finalResponse.addResponceParam(ResponceCodes.AVAILABILITY_FLIGHT_CALENDAR_SEARCH_SUCCESSFULL, availFlightRS);

		return finalResponse;
	}

	private AvailableFlightSearchDTO createAvailSearch() throws ModuleException {
		int salesChannelCode = SalesChannelsUtil.SALES_CHANNEL_PUBLIC;
		if (calendarSearchRQ.getAvailPreferences().getAppIndicator() == ApplicationEngine.IBE) {
			salesChannelCode = SalesChannelsUtil.SALES_CHANNEL_WEB;
		} else if (calendarSearchRQ.getAvailPreferences().getAppIndicator() == ApplicationEngine.XBE) {
			salesChannelCode = SalesChannelsUtil.SALES_CHANNEL_AGENT;
		}

		String agentCode = calendarSearchRQ.getAvailPreferences().getTravelAgentCode();
		String agentStation = null;
		UserPrincipal userPrincipal = (UserPrincipal) UserPrincipal.createIdentity("", salesChannelCode, agentCode, agentStation,
				null, null, null, null, null, null, null, null, null, null, null, null, null);
		AvailableFlightSearchDTO availDTO = AvailabilityConvertUtil.getAvailableFlightSearchDTO(calendarSearchRQ, userPrincipal,
				false, null);
		return availDTO;
	}

	private AvailableONDFlight convertOndFlight(AvailableIBOBFlightSegment ondFlight) {
		AvailableONDFlight ondFlt = new AvailableONDFlight();
		ondFlt.setOndSequence(ondFlight.getOndSequence());
		List<FlightSegmentTO> flightSegments = new ArrayList<FlightSegmentTO>();
		List<SegmentInvAvailability> inventoryAvailability = new ArrayList<SegmentInvAvailability>();

		for (FlightSegmentDTO flight : ondFlight.getFlightSegmentDTOs()) {
			FlightSegmentTO flightSegment = convertFlightSegment(flight);
			flightSegments.add(flightSegment);

			Set<String> logicalCabins = new HashSet<String>(flight.getAvailableAdultCount().keySet());

			SegmentInvAvailability segInvAvail = new SegmentInvAvailability();
			List<LogicalCabinAvailability> availablities = new ArrayList<LogicalCabinAvailability>();
			segInvAvail.setAvailablities(availablities);
			segInvAvail.setFlightSegment(flightSegment);
			for (String logicalCabin : logicalCabins) {
				LogicalCabinAvailability lccAvail = new LogicalCabinAvailability();

				lccAvail.setAvaialbleAdultSeats(flight.getAvailableAdultCount().get(logicalCabin));
				lccAvail.setAvailableInfantSeats(flight.getAvailableInfantCount().get(logicalCabin));
				lccAvail.setLogicalCabinClass(logicalCabin);
				lccAvail.setLoadFactor(lccAvail.getAvaialbleAdultSeats() / 168);// FIXME
				availablities.add(lccAvail);
			}
			inventoryAvailability.add(segInvAvail);
		}
		ondFlt.setFlightSegments(flightSegments);
		ondFlt.setInventoryAvailability(inventoryAvailability);

		return ondFlt;
	}

	private FlightSegmentTO convertFlightSegment(FlightSegmentDTO fltDTO) {
		FlightSegmentTO fltSegmentTO = new FlightSegmentTO();
		fltSegmentTO.setArrivalDateTime(fltDTO.getArrivalDateTime());
		fltSegmentTO.setArrivalDateTimeZulu(fltDTO.getArrivalDateTimeZulu());
		// cmnSegDTO.setCabinClassCode(onewayAvailableFlight.getCabinClassCode());
		fltSegmentTO.setDepartureDateTime(fltDTO.getDepartureDateTime());
		fltSegmentTO.setDepartureDateTimeZulu(fltDTO.getDepartureDateTimeZulu());
		fltSegmentTO.setFlightNumber(fltDTO.getFlightNumber());
		fltSegmentTO.setSegmentCode(fltDTO.getSegmentCode());
		fltSegmentTO.setOperatingAirline(fltDTO.getCarrierCode());
		fltSegmentTO.setOperationType(fltDTO.getOperationTypeID());
		// cmnSegDTO.setReturnFlag(returnFlag);
		fltSegmentTO.setFlightRefNumber(FlightRefNumberUtil.composeFlightRPH(fltDTO));
		fltSegmentTO.setFlightSegId(fltDTO.getSegmentId());
		fltSegmentTO.setFlightId(fltDTO.getFlightId());
		fltSegmentTO.setDomesticFlight(fltDTO.isDomesticFlight());

		fltSegmentTO.setArrivalTerminalName(fltDTO.getArrivalTerminal());
		fltSegmentTO.setDepartureTerminalName(fltDTO.getDepartureTerminal());
		fltSegmentTO.setAdultCount(fltDTO.getTotalAvailableAdultCount());
		fltSegmentTO.setInfantCount(fltDTO.getTotalAvailableInfantCount());
		fltSegmentTO.setChildCount(0);
		// if (ondSequence != null) {
		// cmnSegDTO.setOndSequence(ondSequence);
		// }

		fltSegmentTO.setFlightDuration(fltDTO.getFlightDuration());
		fltSegmentTO.setFlightModelDescription(fltDTO.getFlightModelDescription());
		fltSegmentTO.setFlightModelNumber(fltDTO.getFlightModelNumber());
		fltSegmentTO.setRemarks(fltDTO.getRemarks());
		fltSegmentTO.setFlightSegmentStatus(fltDTO.getFlightSegmentStatus());
		fltSegmentTO.setAvailableAdultCountMap(fltDTO.getAvailableAdultCount());
		fltSegmentTO.setAvailableInfantCountMap(fltDTO.getAvailableInfantCount());
		// cmnSegDTO.setSubJourneyGroup(onewayAvailableFlight.getSubJourneyGroup());

		fltSegmentTO.setWaitListed(fltDTO.isWaitListed());
		fltSegmentTO.setOverbookEnabled(fltDTO.isOverBookEnabled());
		return fltSegmentTO;
	}

}
