package com.isa.thinair.airschedules.core.bl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.baggage.FlightBaggageDTO;
import com.isa.thinair.airinventory.api.service.BaggageBusinessDelegate;
import com.isa.thinair.airmaster.api.model.AircraftCabinCapacity;
import com.isa.thinair.airmaster.api.model.AircraftModel;
import com.isa.thinair.airpricing.api.model.DefaultAnciTemplate;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightLeg;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.FlightScheduleLeg;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.model.Leg;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.commons.api.exception.ModuleException;

public class DefaultAnciTemplateAssignBL {

	public static void setDefaultAnciTemplatesToCreateFlight(Flight flight) throws ModuleException {

		Map<String, Integer> anciTypeWiseTemplIdMap = getDefaultAnciTemplateForFlight(
				getFlightOndCodeFromFlightLegs(flight.getFlightLegs()), flight.getModelNumber());

		flight.setSeatChargeTemplateId(anciTypeWiseTemplIdMap.get(DefaultAnciTemplate.ANCI_TEMPLATES.SEAT.toString()));
		flight.setMealTemplateId(anciTypeWiseTemplIdMap.get(DefaultAnciTemplate.ANCI_TEMPLATES.MEAL.toString()));
		flight.setBaggabeTemplateId(anciTypeWiseTemplIdMap.get(DefaultAnciTemplate.ANCI_TEMPLATES.BAGGAGE.toString()));

	}

	public static void setDefaultAnciTemplatesToCreateFlightSchedule(FlightSchedule flightSchedule) throws ModuleException {

		Map<String, Integer> anciTypeWiseTemplIdMap = getDefaultAnciTemplateForFlight(
				getFlightOndCodeFromFlightScheduleLegs(flightSchedule.getFlightScheduleLegs()), flightSchedule.getModelNumber());

		flightSchedule.setSeatChargeTemplateId(anciTypeWiseTemplIdMap.get(DefaultAnciTemplate.ANCI_TEMPLATES.SEAT.toString()));
		flightSchedule.setMealTemplateId(anciTypeWiseTemplIdMap.get(DefaultAnciTemplate.ANCI_TEMPLATES.MEAL.toString()));
		flightSchedule.setBaggabeTemplateId(anciTypeWiseTemplIdMap.get(DefaultAnciTemplate.ANCI_TEMPLATES.BAGGAGE.toString()));
	}

	public static void setDefaultAnciTemplatesToUpdatedFlight(Flight flight, Integer extMealTmplId, Integer extSeatTmplId,
			boolean isScheduleMessageProcessing) throws ModuleException {

		if (isScheduleMessageProcessing && flight.getOldModelNumber() != null && !flight.getOldModelNumber().equals("")
				&& !flight.getOldModelNumber().equals(flight.getFlightNumber())) {
			Map<String, Integer> anciTypeWiseTemplIdMap = getDefaultAnciTemplateForFlight(
					getFlightOndCodeFromFlightLegs(flight.getFlightLegs()), flight.getModelNumber());
			flight.setSeatChargeTemplateId(anciTypeWiseTemplIdMap.get(DefaultAnciTemplate.ANCI_TEMPLATES.SEAT.toString()));
			flight.setMealTemplateId(extMealTmplId);

		} else if (isScheduleMessageProcessing) {
			flight.setMealTemplateId(extMealTmplId);
			flight.setSeatChargeTemplateId(extSeatTmplId);
		}
	}

	public static void updadeFlightBaggages(Flight flight, List<String> ccList) throws ModuleException {
	
		for(String classOfService : ccList){
			Collection<FlightBaggageDTO> colSegs = new HashSet<FlightBaggageDTO>();
			for (FlightSegement segment : flight.getFlightSegements()) {
	
				FlightBaggageDTO fltBaggageDto = new FlightBaggageDTO();
				fltBaggageDto.setFlightSegmentID(segment.getFltSegId());
				fltBaggageDto.setTemplateId(flight.getBaggabeTemplateId());
				fltBaggageDto.setCabinClassCode(classOfService);
				colSegs.add(fltBaggageDto);
			}
	
			BaggageBusinessDelegate baggageBd = AirSchedulesUtil.getBaggageBD();
			baggageBd.assignFlightBaggageCharges(colSegs, null, false, classOfService);
		}
	}

	public static void updadeFlightBaggages(Collection<Flight> flightCol, String modelNumber) throws ModuleException {
		AircraftModel model = AirSchedulesUtil.getAircraftBD().getAircraftModel(modelNumber);
		if (flightCol != null && flightCol.size()>0) {
			for (Flight flt : flightCol) {
				updadeFlightBaggages(flt,getModelsCabinClasses(model));
			}
		}
	}

	public static void setDefaultAnciTemplatesToUpdatedFltSchdule(FlightSchedule existingFltSchdule,
			FlightSchedule updatedFltSchdule, boolean isModelChange, boolean isScheduleMessageProcessing) throws ModuleException {

		if (isModelChange && isScheduleMessageProcessing) {

			Map<String, Integer> anciTypeWiseTemplIdMap = getDefaultAnciTemplateForFlight(
					getFlightOndCodeFromFlightScheduleLegs(existingFltSchdule.getFlightScheduleLegs()),
					updatedFltSchdule.getModelNumber());
			updatedFltSchdule.setSeatChargeTemplateId(anciTypeWiseTemplIdMap.get(DefaultAnciTemplate.ANCI_TEMPLATES.SEAT
					.toString()));
			updatedFltSchdule.setMealTemplateId(existingFltSchdule.getMealTemplateId());
		} else if (isScheduleMessageProcessing) {
			updatedFltSchdule.setSeatChargeTemplateId(existingFltSchdule.getSeatChargeTemplateId());
			updatedFltSchdule.setMealTemplateId(existingFltSchdule.getMealTemplateId());
		}

	}
	
	public static List<String> getModelsCabinClasses(AircraftModel model){
		List<String> ccList = new ArrayList<String>();
		for(AircraftCabinCapacity capacitys : model.getAircraftCabinCapacitys()){
			ccList.add(capacitys.getCabinClassCode());
		}
		return ccList;
	}

	private static Map<String, Integer> getDefaultAnciTemplateForFlight(String flightOndCode, String ModelNumber)
			throws ModuleException {
		return AirSchedulesUtil.getCommonAncillaryServiceBD().getDefaultAnciTemplateForFlight(flightOndCode, ModelNumber);
	}

	private static String getFlightOndCodeFromFlightLegs(Set<FlightLeg> flightLsgs) {

		Map<Integer, String> map = new HashMap<>();
		for (Leg leg : flightLsgs) {
			if (leg.getLegNumber() == 1) {
				map.put(1, leg.getOrigin() + "/" + leg.getDestination());
			} else {
				map.put(leg.getLegNumber(), "/" + leg.getDestination());
			}
		}

		return composeOndCode(map, flightLsgs.size());
	}

	private static String getFlightOndCodeFromFlightScheduleLegs(Set<FlightScheduleLeg> flightLsgs) {

		Map<Integer, String> map = new HashMap<>();
		for (Leg leg : flightLsgs) {
			if (leg.getLegNumber() == 1) {
				map.put(1, leg.getOrigin() + "/" + leg.getDestination());
			} else {
				map.put(leg.getLegNumber(), "/" + leg.getDestination());
			}
		}

		return composeOndCode(map, flightLsgs.size());
	}

	private static String composeOndCode(Map<Integer, String> map, int numOfLegs) {
		StringBuilder sb = new StringBuilder();
		for (int i = 1; i <= numOfLegs; i++) {
			sb.append(map.get(i));
		}
		return sb.toString();
	}
}
