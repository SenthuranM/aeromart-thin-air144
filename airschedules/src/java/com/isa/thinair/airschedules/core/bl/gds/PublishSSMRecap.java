package com.isa.thinair.airschedules.core.bl.gds;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.GDSFlightEventCollector;
import com.isa.thinair.airschedules.api.utils.GDSSchedConstants;
import com.isa.thinair.airschedules.api.utils.GDSScheduleEventCollector;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.airschedules.core.config.AirScheduleModuleConfig;
import com.isa.thinair.airschedules.core.persistence.dao.FlightDAO;
import com.isa.thinair.airschedules.core.persistence.dao.FlightScheduleDAO;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.airschedules.core.util.ScheduledFlightUtil;
import com.isa.thinair.auditor.api.model.Audit;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.constants.TasksUtil;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.gdsservices.api.dto.publishing.Envelope;
import com.isa.thinair.gdsservices.api.service.GDSPublishingBD;
import com.isa.thinair.msgbroker.api.model.TypeBRecipientExtSystem;
import com.isa.thinair.msgbroker.api.model.ServiceBearer.MessagingMode;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * @isa.module.command name="publishSSMRecap"
 */

public class PublishSSMRecap extends DefaultBaseCommand { 
	

	private FlightScheduleDAO flightSchedleDAO;
	private FlightDAO flightDAO;
	private GDSPublishingBD gdsPublishingBD;
	private static final String UNPROCESSED = "N";
	
	
	public PublishSSMRecap(){
		flightSchedleDAO = (FlightScheduleDAO) AirSchedulesUtil.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_DAO);
		flightDAO = (FlightDAO) AirSchedulesUtil.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_DAO);
		gdsPublishingBD = AirSchedulesUtil.getGDSPublishingBD();
	}

	@Override
	public ServiceResponce execute() throws ModuleException {
		
		DefaultServiceResponse response = new DefaultServiceResponse(true);
		Boolean isExternalSSMSending = (Boolean) this.getParameter(CommandParamNames.EXTERNAL_SSM_SENDING);
		boolean systemEnable = false;
		if (isExternalSSMSending) {
			systemEnable = "Y".equalsIgnoreCase(CommonsServices.getGlobalConfig().getBizParam(
					SystemParamKeys.EXTERNAL_SSM_RECAP_INTEGRATED));
		} else {
			systemEnable = "Y".equalsIgnoreCase(CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.GDS_INTEGRATED));
		}
		
		if (systemEnable) {			
			
			Date scheduleStartDate = (Date) this.getParameter(CommandParamNames.SCHEDULE_START_DATE);
			Date scheduleEndDate = (Date) this.getParameter(CommandParamNames.SCHEDULE_END_DATE);
			Boolean isSelectedSchedules = (Boolean) this.getParameter(CommandParamNames.IS_SELECTED_SCHEDULES);
			Integer gdsID = 0;
			String externalEmailAddress = "";
			String externalSitaAddress = "";

			if (!isExternalSSMSending) {
				gdsID = (Integer) this.getParameter(CommandParamNames.GDS_ID);
				String emailAddress = (String) this.getParameter(CommandParamNames.EMAIL_ADDRESS);
				this.checkParams(gdsID, scheduleStartDate, scheduleEndDate, isSelectedSchedules, emailAddress);
			} else {
				externalEmailAddress = (String) this.getParameter(CommandParamNames.EXTERNAL_EMAIL_ADDRESS);
				externalSitaAddress = (String) this.getParameter(CommandParamNames.EXTERNAL_SITA_ADDRESS);			
				this.checkParamsForExtSending(scheduleStartDate, scheduleEndDate, isSelectedSchedules, externalEmailAddress,externalSitaAddress);
			}			
			Collection<FlightSchedule> schedules = ((FlightScheduleDAO) AirSchedulesUtil.getInstance().getLocalBean(
					InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_DAO)).getFlightSchedulesForSSMRecap(scheduleStartDate, scheduleEndDate, isSelectedSchedules);
			String serviceType = ((AirScheduleModuleConfig) AirSchedulesUtil.getInstance().getModuleConfig())
					.getServiceTypeForScheduleFlight();			
			if (schedules != null) {
				for (FlightSchedule flightSchedule : schedules) {
					Set<Integer> gdsIDsSchedule = (flightSchedule.getGdsIds() != null) ? flightSchedule.getGdsIds()
							: new HashSet<Integer>();
					Collection<Flight> flights = flightSchedleDAO.getFlightsForSchedule(flightSchedule.getScheduleId());
					GDSScheduleEventCollector eventCollector = new GDSScheduleEventCollector();
				
					if (!isExternalSSMSending) {						
						gdsIDsSchedule.add(gdsID);
						Collection<Integer> newlyAddedGDSList = new ArrayList<Integer>();
						newlyAddedGDSList.add(gdsID);
						for (Flight flight : flights) {
							Set<Integer> gdsIDsFlight = (flight.getGdsIds() != null) ? flight.getGdsIds() : new HashSet<Integer>();
							gdsIDsFlight.add(gdsID);
							flight.setGdsIds(gdsIDsFlight);
							flightDAO.saveFlight(flight);
						}
						flightSchedule.setGdsIds(gdsIDsSchedule);
						flightSchedleDAO.saveFlightSchedule(flightSchedule);
						eventCollector.addAction(GDSFlightEventCollector.GDS_ADDED);
						eventCollector.setAddedGDSList(newlyAddedGDSList);
					} else {
						eventCollector.setExternalEmailAddress(externalEmailAddress);
						eventCollector.setExternalSitaAddress(externalSitaAddress);	
						eventCollector.setExternalSSMRecap(true);
						eventCollector.addAction(GDSFlightEventCollector.EXTERNAL_SSM_RECAP);
					}		
					eventCollector.setServiceType(serviceType);
					eventCollector.setUpdatedSchedule(flightSchedule);
					
					HashMap<Integer, List<Integer>> flightIdMap = new HashMap<Integer, List<Integer>>();
					flightIdMap.put(flightSchedule.getScheduleId(), ScheduledFlightUtil.getFlightIds(flights));

					Envelope env = new Envelope(GDSSchedConstants.TargetLogicNames.PUBLISH_ADD_REMOVE_SSM);
					env.addParam(GDSSchedConstants.ParamNames.GDS_EVENT_COLLECTOR, eventCollector);
					env.addParam(GDSSchedConstants.ParamNames.FLIGHT_IDS_MAP_FOR_SCHEDULES, flightIdMap);
					env.addParam(GDSSchedConstants.ParamNames.CARRIER_CODE, getCarrierCode(flightSchedule.getFlightNumber()));

					gdsPublishingBD.publishMessage(env);

				}

			}
			
			Collection<Flight> flights = flightDAO
					.getAdHocFlightsForASMRecap(scheduleStartDate, scheduleEndDate, isSelectedSchedules);
			
			if (flights != null) {
				if (isExternalSSMSending) {
					sendASMsForExternalSystem(flights, externalEmailAddress, externalSitaAddress, serviceType);
				} else {
					sendASMsToGDS(flights, gdsID, serviceType);
				}
			}
			
			if(flights == null && schedules == null) {
				throw new ModuleException("airschedules.logic.bl.schedule.noschedule");				
			} else {

				if (isExternalSSMSending) {

					Audit externalSSMRecapAudit = generateAuditForExternalSSMRecap(externalSitaAddress, externalEmailAddress);
					AirSchedulesUtil.getAuditorBD().audit(externalSSMRecapAudit, null);
					TypeBRecipientExtSystem typeBRecipientExtSystem = createTypeBRecipientExtSystem(externalEmailAddress,
							externalSitaAddress);
					AirSchedulesUtil.getMessageBrokerServiceBD().saveTypeBRecipientExtSystem(typeBRecipientExtSystem);
				} else {
					Audit gdsSSMRecapAudit =generateAuditForGDSSSMRecap(gdsID);
					AirSchedulesUtil.getAuditorBD().audit(gdsSSMRecapAudit, null);
				}
			}			
		}	
		
		response.setResponseCode(ResponceCodes.ADD_REMOVE_GDS_SUCCESS);
		response.addResponceParam(ResponceCodes.ADD_REMOVE_GDS_SUCCESS, true);
		
		return response;
	} 
	
	private void checkParams(Integer gdsID, Date scheduleStartDate, Date scheduleEndDate, Boolean isSelectedSchedules,
			String emailAddress) throws ModuleException {
		if(gdsID == null){
			throw new ModuleException("airschedules.arg.invalid.null");
		}else if(isSelectedSchedules){
			if(scheduleStartDate ==null || scheduleEndDate == null){
				throw new ModuleException("airschedules.arg.invalid.null");
			}
		}		
	}
	
	private void checkParamsForExtSending(Date scheduleStartDate, Date scheduleEndDate, Boolean isSelectedSchedules,
			String email, String sita) throws ModuleException {
		if (isSelectedSchedules) {
			if (scheduleStartDate == null || scheduleEndDate == null) {
				throw new ModuleException("airschedules.arg.invalid.null");
			}
		}

		if (email.equals("") && sita.equals("")) {
			throw new ModuleException("airschedules.arg.invalid.null");
		}
	}

	private String getCarrierCode(String flightNumebr) {

		String carrierCode = "";
		if (flightNumebr != null && flightNumebr.length() >= 5) {
			carrierCode = flightNumebr.substring(0, 2);
		}
		return carrierCode;
	} 
	
	private Audit generateAuditForExternalSSMRecap(String sitaAddress, String emailAddress) {
		String content = "sita:=" + sitaAddress + "||email:=" + emailAddress;
		return new Audit(TasksUtil.MASTER_SEND_SSM_RECAP_EXTERNAL, new Date(), "airadmin", "SYSTEM", content);
	}
	
	private Audit generateAuditForGDSSSMRecap(Integer gdsID) {
		String content = "gds id:=" + gdsID.toString();
		return new Audit(TasksUtil.MASTER_SEND_SSM_RECAP_GDS, new Date(), "airadmin", "SYSTEM", content);
	}
	
	private TypeBRecipientExtSystem createTypeBRecipientExtSystem(String emailAddress, String sitaAddress) {
		TypeBRecipientExtSystem typeBRecipientExtSystem = new TypeBRecipientExtSystem();
		typeBRecipientExtSystem.setProcessedDate(new Date());
		typeBRecipientExtSystem.setProcessState(UNPROCESSED);
		if (!"".equals(emailAddress)) {
			typeBRecipientExtSystem.setRecipientAddress(emailAddress);
			typeBRecipientExtSystem.setMessagingMode(MessagingMode.MAIL.toString());

		} else {
			typeBRecipientExtSystem.setRecipientAddress(sitaAddress);
			typeBRecipientExtSystem.setMessagingMode(MessagingMode.FILE.toString());
		}
		return typeBRecipientExtSystem;
	}
	
	private void sendASMsForExternalSystem(Collection<Flight> flights, String externalEmailAddress, String externalSitaAddress, String serviceType)
			throws ModuleException {

		for (Flight flight : flights) {
			GDSFlightEventCollector eventCollector = new GDSFlightEventCollector();
			eventCollector.setExternalEmailAddress(externalEmailAddress);
			eventCollector.setExternalSitaAddress(externalSitaAddress);
			eventCollector.setExternalSSMRecap(true);
			eventCollector.setServiceType(serviceType);
			eventCollector.setUpdatedFlight(flight);
			eventCollector.addAction(GDSFlightEventCollector.EXTERNAL_SSM_RECAP);

			Envelope env = new Envelope(GDSSchedConstants.TargetLogicNames.PUBLISH_ADD_REMOVE_ASM);
			env.addParam(GDSSchedConstants.ParamNames.GDS_EVENT_COLLECTOR, eventCollector);

			gdsPublishingBD.publishMessage(env);
		}
	}
	
	private void sendASMsToGDS(Collection<Flight> flights,Integer gdsID, String serviceType) throws ModuleException {
		for (Flight flight : flights) {
			Collection<Integer> existingGdsList = (flight.getGdsIds() != null) ? flight.getGdsIds() : new ArrayList<Integer>();
			Set<Integer> gdsCodes = new HashSet<Integer>();
			if (flight.getScheduleId() == null) {
				gdsCodes = new HashSet<Integer>();
				gdsCodes.addAll(existingGdsList);
				gdsCodes.add(gdsID);
				flight.setGdsIds(gdsCodes);
				flightDAO.saveFlight(flight);
			}
			GDSFlightEventCollector eventCollector = new GDSFlightEventCollector();

			eventCollector.addAction(GDSFlightEventCollector.GDS_ADDED);
			eventCollector.setAddedGDSList(gdsCodes);
			eventCollector.setServiceType(serviceType);
			eventCollector.setUpdatedFlight(flight);

			Envelope env = new Envelope(GDSSchedConstants.TargetLogicNames.PUBLISH_ADD_REMOVE_ASM);
			env.addParam(GDSSchedConstants.ParamNames.GDS_EVENT_COLLECTOR, eventCollector);
			gdsPublishingBD.publishMessage(env);

		}
	}

}
