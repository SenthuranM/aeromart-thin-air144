/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.gds;

import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.GDSScheduleEventCollector;
import com.isa.thinair.airschedules.api.utils.LegUtil;
import com.isa.thinair.airschedules.api.utils.LocalZuluTimeAdder;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.ScheduledFlightUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.FrequencyUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for integrate create schedule with gds
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="collectAmmendScheduleGDSEvents"
 */
public class CollectAmmendScheduleGDSEvents extends DefaultBaseCommand {

	/**
	 * constructor of the CollectAmmendScheduleGDSEvents command
	 */
	public CollectAmmendScheduleGDSEvents() {
	}

	/**
	 * execute method of the ValidateCreateFlights command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {

		DefaultServiceResponse response = new DefaultServiceResponse(true);
		if ("Y".equalsIgnoreCase(CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.GDS_INTEGRATED))) {

			FlightSchedule updated = (FlightSchedule) this.getParameter(CommandParamNames.FLIGHT_SCHEDULE);
			FlightSchedule existing = (FlightSchedule) this.getParameter(CommandParamNames.LOADED_EXISTING_SCHEDULE);
			FlightSchedule existingOverlapping = (FlightSchedule) this
					.getParameter(CommandParamNames.LOADED_EXISTING_OVERLAPPING_SCHEDULE);

			// checking params
			this.checkParams(updated);

			GDSScheduleEventCollector sGdsEvents = new GDSScheduleEventCollector();

			identifyPeriodChanges(sGdsEvents, updated, existing);
			identifyFlightNumberChanges(sGdsEvents, updated, existing);
			identifyModelChanges(sGdsEvents, updated, existing);
			identifyLegsOrTimeChanges(sGdsEvents, updated, existing);
			identifyServiceTypeChanges(sGdsEvents, updated, existing);

			// setting updated schedule info
			sGdsEvents.setUpdatedSchedule(ScheduledFlightUtil.getCopyOfSchedule(updated));

			// setting existing schedule info
			sGdsEvents.setExistingSchedule(ScheduledFlightUtil.getCopyOfSchedule(existing));

			// scheduele ids are not coping from the util copy
			// so set them manually
			sGdsEvents.getUpdatedSchedule().setScheduleId(existing.getScheduleId());
			sGdsEvents.getExistingSchedule().setScheduleId(existing.getScheduleId());

			if (existingOverlapping != null) {
				sGdsEvents.setExistingOverlapSchedule(ScheduledFlightUtil.getCopyOfSchedule(existingOverlapping));
				sGdsEvents.getExistingOverlapSchedule().setScheduleId(existingOverlapping.getScheduleId());
			}

			// constructing response
			response.addResponceParam(CommandParamNames.GDS_PARAM_COLLECTOR, sGdsEvents);
		}
		// return command response
		return response;
	}

	/**
	 * privte method to validate parameters
	 * 
	 * @param schedule
	 * @throws ModuleException
	 */
	private void checkParams(FlightSchedule schedule) throws ModuleException {

		if (schedule == null)// throw exception
			throw new ModuleException("airschedules.arg.invalid.null");
	}

	private void identifyPeriodChanges(GDSScheduleEventCollector sGdsEvents, FlightSchedule updated, FlightSchedule existing)
			throws ModuleException {

		// if existing and update not in same start date, end date and frequency
		if (!(CalendarUtil.isSameDay(updated.getStartDate(), existing.getStartDate())
				&& CalendarUtil.isSameDay(updated.getStopDate(), existing.getStopDate()) && FrequencyUtil.isEqualFrequency(
				updated.getFrequency(), existing.getFrequency()))) {

			sGdsEvents.addAction(GDSScheduleEventCollector.PERIOD_CHANGED);
		}

		addLocalTimeDetailsToFlightSchedule(existing);
		addLocalTimeDetailsToFlightSchedule(updated);
		if (!(CalendarUtil.isSameDay(updated.getStartDateLocal(), existing.getStartDateLocal())
				&& CalendarUtil.isSameDay(updated.getStopDateLocal(), existing.getStopDateLocal()) && FrequencyUtil
					.isEqualFrequency(updated.getFrequencyLocal(), existing.getFrequencyLocal()))) {

			sGdsEvents.addAction(GDSScheduleEventCollector.PERIOD_CHANGED_LOCAL_TIME);
		}


	}

	private void
			identifyFlightNumberChanges(GDSScheduleEventCollector sGdsEvents, FlightSchedule updated, FlightSchedule existing) {

		// if existing and update not in same start date, end date and frequency
		if (!updated.getFlightNumber().equals(existing.getFlightNumber())) {

			sGdsEvents.addAction(GDSScheduleEventCollector.FLIGHT_NUMBER_CHANGED);
		}
	}

	private void identifyModelChanges(GDSScheduleEventCollector sGdsEvents, FlightSchedule updated, FlightSchedule existing) {

		if (!updated.getModelNumber().equals(existing.getModelNumber())) {

			sGdsEvents.addAction(GDSScheduleEventCollector.MODEL_CHANGED);
		}
	}

	private void identifyLegsOrTimeChanges(GDSScheduleEventCollector sGdsEvents, FlightSchedule updated, FlightSchedule existing) {

		boolean legsAddedDeleted = LegUtil.checkLegsAddedDeleted(existing, updated);

		if (legsAddedDeleted) {

			sGdsEvents.addAction(GDSScheduleEventCollector.LEG_ADDED_DELETED);
			sGdsEvents.addAction(GDSScheduleEventCollector.REPLACE_EXISTING_INFO);
		} else {

			boolean legTimeChanged = LegUtil.isLegTimeChanged(existing, updated);
			if (legTimeChanged) {
				sGdsEvents.addAction(GDSScheduleEventCollector.LEG_TIME_CHANGE);
			}
		}
	}
	
	private void
			identifyServiceTypeChanges(GDSScheduleEventCollector sGdsEvents, FlightSchedule updated, FlightSchedule existing) {
		if (updated.getOperationTypeId() != existing.getOperationTypeId()) {
			sGdsEvents.addAction(GDSScheduleEventCollector.REPLACE_EXISTING_INFO);
		}
	}
	
	private void addLocalTimeDetailsToFlightSchedule(FlightSchedule schedule) throws ModuleException {
		// Add local timings to the schdule
		LocalZuluTimeAdder localZuluTimeAdder = new LocalZuluTimeAdder(AirSchedulesUtil.getAirportBD());
		localZuluTimeAdder.addLocalTimeDetailsToFlightSchedule(schedule, false);
	}
}
