/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl;

import com.isa.thinair.commons.core.framework.Command;

/**
 * macro command definitions goes here
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command.macro-def
 */
public final class MacroCommandDef {

	private MacroCommandDef() {
	}

	/**
	 * @isa.module.command.macro-command name="createScheduleMacro"
	 * @isa.module.command.macro-map order="1" inner-command="parseScheduleToBack"
	 * @isa.module.command.macro-map order="2" inner-command="checkScheduleFrequencyMatchingDateRange"
	 * @isa.module.command.macro-map order="3" inner-command="checkValidSegments"
	 * @isa.module.command.macro-map order="4" inner-command="checkScheduleLegDuration"
	 * @isa.module.command.macro-map order="5" inner-command="checkConflictingSchedulesForSchedule"
	 * @isa.module.command.macro-map order="6" inner-command="checkConflictingFlightsForSchedule"
	 * @isa.module.command.macro-map order="7" inner-command="checkScheduleOverlap"
	 * @isa.module.command.macro-map order="8" inner-command="createSchedule"
	 * @isa.module.command.macro-map order="9" inner-command="splitSubSchedule"
	 * @isa.module.command.macro-map order="10" inner-command="autoPublishSchedules"
	 * @isa.module.command.macro-map order="11" inner-command="auditSchedule"
	 */
	private Command createScheduleMacro;

	/**
	 * @isa.module.command.macro-command name="copyScheduleMacro"
	 * @isa.module.command.macro-map order="1" inner-command="prepareCopySchedule"
	 * @isa.module.command.macro-map order="2" inner-command="checkScheduleFrequencyMatchingDateRange"
	 * @isa.module.command.macro-map order="3" inner-command="checkConflictingSchedulesForSchedule"
	 * @isa.module.command.macro-map order="4" inner-command="checkConflictingFlightsForSchedule"
	 * @isa.module.command.macro-map order="5" inner-command="createSchedule"
	 * @isa.module.command.macro-map order="6" inner-command="splitSubSchedule"
	 * @isa.module.command.macro-map order="7" inner-command="autoPublishSchedules"
	 * @isa.module.command.macro-map order="8" inner-command="auditSchedule"
	 */
	private Command copyScheduleMacro;

	/**
	 * @isa.module.command.macro-command name="ammendScheduleMacro"
	 * @isa.module.command.macro-map order="1" inner-command="parseScheduleToBack"
	 * @isa.module.command.macro-map order="2" inner-command="checkScheduleFrequencyMatchingDateRange"
	 * @isa.module.command.macro-map order="3" inner-command="prepareAmmendSchedule"
	 * @isa.module.command.macro-map order="4" inner-command="checkScheduleLegDuration"
	 * @isa.module.command.macro-map order="5" inner-command="checkConflictingSchedulesForSchedule"
	 * @isa.module.command.macro-map order="6" inner-command="checkConflictingFlightsForSchedule"
	 * @isa.module.command.macro-map order="7" inner-command="checkScheduleOverlap"
	 * @isa.module.command.macro-map order="8" inner-command="checkAircraftModel"
	 * @isa.module.command.macro-map order="9" inner-command="checkReservations"
	 * @isa.module.command.macro-map order="10" inner-command="collectAmmendScheduleGDSEvents"
	 * @isa.module.command.macro-map order="11" inner-command="ammendSchedule"
	 * @isa.module.command.macro-map order="12" inner-command="auditSchedule"
	 * @isa.module.command.macro-map order="13" inner-command="updateReservation"
	 * @isa.module.command.macro-map order="14" inner-command="integrateAmmendSchedule"
	 * @isa.module.command.macro-map order="15" inner-command="addFlightChangesToPublish"
	 */
	private Command ammendScheduleMacro;

	/**
	 * @isa.module.command.macro-command name="ammendScheduleLegsMacro"
	 * @isa.module.command.macro-map order="1" inner-command="parseScheduleToBack"
	 * @isa.module.command.macro-map order="2" inner-command="prepareAmmendSchedule"
	 * @isa.module.command.macro-map order="3" inner-command="checkValidSegments"
	 * @isa.module.command.macro-map order="4" inner-command="checkScheduleLegDuration"
	 * @isa.module.command.macro-map order="5" inner-command="checkReservationsForInvalidSegments"
	 * @isa.module.command.macro-map order="6" inner-command="checkScheduleOverlap"
	 * @isa.module.command.macro-map order="7" inner-command="checkReservationsForLegs"
	 * @isa.module.command.macro-map order="8" inner-command="collectAmmendScheduleGDSEvents"
	 * @isa.module.command.macro-map order="9" inner-command="ammendSchedule"
	 * @isa.module.command.macro-map order="10" inner-command="auditSchedule"
	 * @isa.module.command.macro-map order="11" inner-command="updateReservation"
	 * @isa.module.command.macro-map order="12" inner-command="integrateAmmendSchedule"
	 * @isa.module.command.macro-map order="13" inner-command="addFlightChangesToPublish"
	 */
	private Command ammendScheduleLegsMacro;

	/**
	 * @isa.module.command.macro-command name="addReomveScheduleGDSMacro"
	 * @isa.module.command.macro-map order="1" inner-command="addRemoveGDSfromSchedule"
	 */
	private Command addReomveScheduleGDSMacro;

	/**
	 * @isa.module.command.macro-command name="addReomveFlightGDSMacro"
	 * @isa.module.command.macro-map order="1" inner-command="addRemoveGDSfromFlight"
	 */
	private Command addReomveFlightGDSMacro;

	/**
	 * @isa.module.command.macro-command name="splitScheduleMacro"
	 * @isa.module.command.macro-map order="1" inner-command="splitSchedule"
	 * @isa.module.command.macro-map order="2" inner-command="auditSchedule"
	 */
	private Command splitScheduleMacro;
	
	/**
	 * @isa.module.command.macro-command name="splitBulkScheduleMacro"
	 * @isa.module.command.macro-map order="1" inner-command="splitBulkSchedule"
	 *
	 */
	private Command splitBulkScheduleMacro;


	/**
	 * @isa.module.command.macro-command name="buildScheduleMacro"
	 * @isa.module.command.macro-map order="1" inner-command="buildSchedule"
	 * @isa.module.command.macro-map order="2" inner-command="auditSchedule"
	 * @isa.module.command.macro-map order="3" inner-command="addFlightChangesToPublish"
	 * @isa.module.command.macro-map order="4" inner-command="integrateBuildSchedule"
	 */
	private Command buildScheduleMacro;

	/**
	 * @isa.module.command.macro-command name="cancelScheduleMacro"
	 * @isa.module.command.macro-map order="1" inner-command="cancelSchedule"
	 * @isa.module.command.macro-map order="2" inner-command="auditSchedule"
	 * @isa.module.command.macro-map order="3" inner-command="updateReservation"
	 * @isa.module.command.macro-map order="4" inner-command="integrateCancelSchedule"
	 * @isa.module.command.macro-map order="5" inner-command="addFlightChangesToPublish"
	 * @isa.module.command.macro-map order="6" inner-command="sendScheduleCancellationOfficerNotification"

	 */
	private Command cancelScheduleMacro;

	/**
	 * @isa.module.command.macro-command name="createFlightMacro"
	 * @isa.module.command.macro-map order="1" inner-command="parseFlightsToBack"
	 * @isa.module.command.macro-map order="2" inner-command="checkValidSegments"
	 * @isa.module.command.macro-map order="3" inner-command="warnFlightLegDuration"
	 * @isa.module.command.macro-map order="4" inner-command="warnConflictingFlightsForFlight"
	 * @isa.module.command.macro-map order="5" inner-command="warnConflictingSchedulesForFlight"
	 * @isa.module.command.macro-map order="6" inner-command="createFlight"
	 * @isa.module.command.macro-map order="7" inner-command="auditFlight"
	 * @isa.module.command.macro-map order="8" inner-command="addFlightChangesToPublish"
	 * @isa.module.command.macro-map order="9" inner-command="autoPublishFlights"
	 */
	private Command createFlightMacro;

	/**
	 * @isa.module.command.macro-command name="copyFlightMacro"
	 * @isa.module.command.macro-map order="1" inner-command="prepareCopyFlight"
	 * @isa.module.command.macro-map order="2" inner-command="warnConflictingFlightsForFlight"
	 * @isa.module.command.macro-map order="3" inner-command="warnConflictingSchedulesForFlight"
	 * @isa.module.command.macro-map order="4" inner-command="createFlight"
	 * @isa.module.command.macro-map order="5" inner-command="auditFlight"
	 * @isa.module.command.macro-map order="6" inner-command="integrateCreateFlight"
	 * @isa.module.command.macro-map order="7" inner-command="addFlightChangesToPublish"
	 * @isa.module.command.macro-map order="8" inner-command="autoPublishFlights"
	 */
	private Command copyFlightMacro;

	/**
	 * @isa.module.command.macro-command name="cancelFlightMacro"
	 * @isa.module.command.macro-map order="1" inner-command="checkReservations"
	 * @isa.module.command.macro-map order="2" inner-command="cancelFlight"
	 * @isa.module.command.macro-map order="3" inner-command="deLinkFlight"
	 * @isa.module.command.macro-map order="4" inner-command="auditFlight"
	 * @isa.module.command.macro-map order="5" inner-command="updateReservation"
	 * @isa.module.command.macro-map order="6" inner-command="integrateCancelFlight"
	 * @isa.module.command.macro-map order="7" inner-command="addFlightChangesToPublish"
	 * @isa.module.command.macro-map order="8" inner-command="sendFlightCancellationOfficerNotification"
	 */
	private Command cancelFlightMacro;

	/**
	 * @isa.module.command.macro-command name="checkAmmendFlightMacro"
	 * @isa.module.command.macro-map order="1" inner-command="checkValidSegments"
	 * @isa.module.command.macro-map order="2" inner-command="checkInvalidFlightUpdate"
	 * @isa.module.command.macro-map order="3" inner-command="checkFlightStatusChange"
	 * @isa.module.command.macro-map order="4" inner-command="checkAddDeleteLegsForFlightWithRes"
	 * @isa.module.command.macro-map order="5" inner-command="checkFlightOverlapEligibility"
	 * @isa.module.command.macro-map order="6" inner-command="checkAircraftModel"
	 */
	private Command checkAmmendFlightMacro;

	/**
	 * @isa.module.command.macro-command name="warnAmmendFlightMacro"
	 * @isa.module.command.macro-map order="1" inner-command="warnConflictingFlightsForFlight"
	 * @isa.module.command.macro-map order="2" inner-command="warnConflictingSchedulesForFlight"
	 * @isa.module.command.macro-map order="3" inner-command="warnFlightLegDuration"
	 * @isa.module.command.macro-map order="4" inner-command="warnReservationErrorInjection"
	 */
	private Command warnAmmendFlightMacro;

	/**
	 * @isa.module.command.macro-command name="ammendFlightMacro"
	 * @isa.module.command.macro-map order="1" inner-command="parseFlightsToBack"
	 * @isa.module.command.macro-map order="2" inner-command="prepareAmmendFlight"
	 * @isa.module.command.macro-map order="3" inner-command="checkAmmendFlightMacro"
	 * @isa.module.command.macro-map order="4" inner-command="warnAmmendFlightMacro"
	 * @isa.module.command.macro-map order="5" inner-command="collectAmmendFlightGDSEvents"
	 * @isa.module.command.macro-map order="6" inner-command="ammendFlight"
	 * @isa.module.command.macro-map order="7" inner-command="deLinkFlight" 
	 * @isa.module.command.macro-map order="8" inner-command="auditFlight"
	 * @isa.module.command.macro-map order="9" inner-command="updateReservation"
	 * @isa.module.command.macro-map order="10" inner-command="integrateAmmendFlight"
	 * @isa.module.command.macro-map order="11" inner-command="addFlightChangesToPublish"
	 */
	private Command ammendFlightMacro;

	/**
	 * @isa.module.command.macro-command name="recalculateLocalTimeMacro"
	 * @isa.module.command.macro-map order="1" inner-command="recalculateLocalTime"
	 * @isa.module.command.macro-map order="2" inner-command="updateReservation"
	 * @isa.module.command.macro-map order="3" inner-command="addFlightChangesToPublish"
	 */
	private Command recalculateLocalTimeMacro;

	/**
	 * @isa.module.command.macro-command name="searchSchedulesForDisplayMacro"
	 * @isa.module.command.macro-map order="1" inner-command="schedulesForDisplay"
	 * @isa.module.command.macro-map order="2" inner-command="parseScheduleToFront"
	 */
	private Command searchSchedulesForDisplayMacro;

	/**
	 * @isa.module.command.macro-command name="searchAndReassembleSchedules"
	 * @isa.module.command.macro-map order="1" inner-command="schedulesForDisplay"
	 * @isa.module.command.macro-map order="2" inner-command="reassembleSchedules"
	 */
	private Command searchAndReassembleSchedules;

	/**
	 * @isa.module.command.macro-command name="searchFlightsForDisplayMacro"
	 * @isa.module.command.macro-map order="1" inner-command="flightsForDisplay"
	 * @isa.module.command.macro-map order="2" inner-command="parseFlightToFront"
	 */
	private Command searchFlightsForDisplayMacro;

	/**
	 * @isa.module.command.macro-command name="possibleOverlappingSchedulesMacro"
	 * @isa.module.command.macro-map order="1" inner-command="parseScheduleToBack"
	 * @isa.module.command.macro-map order="2" inner-command="possibleOverlappingSchedules"
	 */
	private Command possibleOverlappingSchedulesMacro;

	/**
	 * @isa.module.command.macro-command name="schedulesForPublishMacro"
	 * @isa.module.command.macro-map order="1" inner-command="parseSchedulesToPublish"
	 */
	private Command schedulesForPublishMacro;

	/**
	 * @isa.module.command.macro-command name="flightsForPublishMacro"
	 * @isa.module.command.macro-map order="1" inner-command="parseFlightsToPublish"
	 */
	private Command flightsForPublishMacro;
	
	/**
	 * @isa.module.command.macro-command name="publishSSMRecapMacro"
	 * @isa.module.command.macro-map order="1" inner-command="publishSSMRecap"
	 */
	private Command publishSSMRecapMacro;

	/**
	 * @isa.module.command.macro-command name="buildFlightScheduleMacro"
	 * @isa.module.command.macro-map order="1" inner-command="buildSchedule"
	 * @isa.module.command.macro-map order="2" inner-command="auditSchedule"
	 * @isa.module.command.macro-map order="3" inner-command="addFlightChangesToPublish"
	 */
	private Command buildFlightScheduleMacro;
}
