package com.isa.thinair.airschedules.core.bl.email;

import java.util.Collection;

import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for send officer notification about cancelled flight/schedules
 * 
 * @isa.module.command name="sendFlightCancellationOfficerNotification"
 */

public class SendFlightCancellationOfficerNotification extends DefaultBaseCommand{

	/**
	 * execute method of the sendFlightCancellationOfficerNotification command
	 * 
	 * @throws ModuleException
	 */
	
	public ServiceResponce execute() throws ModuleException {
		
		Collection<Flight> flightsToPublish = (Collection<Flight>)this.getParameter(CommandParamNames.FLIGHTS_TO_PUBLISH);
		DefaultServiceResponse lastResponse = (DefaultServiceResponse) this.getParameter(CommandParamNames.LAST_RESPONSE);
		

		if (AppSysParamsUtil.isEnableOfficerEmailAlertAtFlightCnx()) {
			FlightScheduleEmail.sentFlightCancellationOfficerNotification(flightsToPublish, true);
		}
		
		return lastResponse;
		
	}

}
