/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.operation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.CreateFlightInventoryRQ;
import com.isa.thinair.airinventory.api.dto.SegmentDTO;
import com.isa.thinair.airinventory.api.service.FlightInventoryBD;
import com.isa.thinair.airinventory.api.service.LogicalCabinClassBD;
import com.isa.thinair.airmaster.api.model.AircraftCabinCapacity;
import com.isa.thinair.airmaster.api.model.AircraftModel;
import com.isa.thinair.airmaster.api.model.RouteInfo;
import com.isa.thinair.airmaster.api.service.AircraftBD;
import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightLeg;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.utils.AirScheduleCustomConstants;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.AnciDefaultTemplStatusEnum;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.airschedules.api.utils.LegUtil;
import com.isa.thinair.airschedules.api.utils.LocalZuluTimeAdder;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.airschedules.api.utils.SegmentUtil;
import com.isa.thinair.airschedules.core.bl.DefaultAnciTemplateAssignBL;
import com.isa.thinair.airschedules.core.persistence.dao.FlightDAO;
import com.isa.thinair.airschedules.core.util.CalculationUtil;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.airschedules.core.util.OverlapDetailUtil;
import com.isa.thinair.commons.api.dto.CabinClassWiseDefaultLogicalCCDetailDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for the create flights
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="createFlight"
 */
public class CreateFlight extends DefaultBaseCommand {

	// Dao's
	private final FlightDAO flightDAO;

	// BD's
	private final FlightInventoryBD flightInventryBD;
	private final AircraftBD aircraftBD;
	private final CommonMasterBD commonMasterBD;
	private final LogicalCabinClassBD logicalCabinClassBD;
	private final AirportBD airportBD;

	/**
	 * constructor of the CreateFlights command
	 */
	public CreateFlight() {

		// looking up daos
		flightDAO = (FlightDAO) AirSchedulesUtil.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_DAO);

		// looking up bd's
		commonMasterBD = AirSchedulesUtil.getCommonMasterBD();
		aircraftBD = AirSchedulesUtil.getAircraftBD();
		flightInventryBD = AirSchedulesUtil.getFlightInventoryBD();
		logicalCabinClassBD = AirSchedulesUtil.getLogicalCabinClassBD();
		airportBD = AirSchedulesUtil.getAirportBD();
	}

	/**
	 * execute method of the CreateFlights command
	 * 
	 * @throws ModuleException
	 */
	@Override
	public ServiceResponce execute() throws ModuleException {

		// getting command params
		Flight flight = (Flight) this.getParameter(CommandParamNames.FLIGHT);

		boolean isEnableAttachDefaultAnciTemplate = getParameter(CommandParamNames.IS_ENABLE_ATACH_DEFAULT_ANCI_TEMPL,
				Boolean.FALSE, Boolean.class);

		DefaultServiceResponse warning = (DefaultServiceResponse) this.getParameter(CommandParamNames.WARNING);

		LocalZuluTimeAdder timeAdder = new LocalZuluTimeAdder(airportBD);

		if (warning != null) {
			return warning;
		}

		// checking params
		this.checkParams(flight);
		
		//Collecting Flights to Publish 
		Collection<Flight> flightsToPublish = new ArrayList<Flight>();

		// aircraft model number
		String modelNumber = flight.getModelNumber();
		AircraftModel model = aircraftBD.getAircraftModel(modelNumber);

		// avilable seat kilometers
		int avilableSeatKilosFlt = 0;

		if (flight.getOperationTypeId() != AirScheduleCustomConstants.OperationTypes.OPERATIONS) {
			int allocation = CalculationUtil.getAllocation(model.getAircraftCabinCapacitys());
			double flightDistance = this.getFlightDistance(flight.getFlightLegs()).doubleValue();
			avilableSeatKilosFlt = CalculationUtil.getASKforFlight(allocation, flightDistance);
		}

		flight.setOriginAptCode(LegUtil.getFirstFlightLeg(flight.getFlightLegs()).getOrigin());
		flight.setDestinationAptCode(LegUtil.getLastFlightLeg(flight.getFlightLegs()).getDestination());
		flight.setStatus(FlightStatusEnum.CREATED.getCode());
		flight.setDayNumber(CalendarUtil.getDayNumber(flight.getDepartureDate()));
		flight.setAvailableSeatKilometers(new Integer(avilableSeatKilosFlt));
		flight.setManuallyChanged(false);
		flight.setMealTemplAssignStatus(AnciDefaultTemplStatusEnum.SCHEDULED.getCode());
		flight.setSeatTemplAssignStatus(AnciDefaultTemplStatusEnum.SCHEDULED.getCode());

		if (flight.getViaScheduleMessaging() && isEnableAttachDefaultAnciTemplate) {
			DefaultAnciTemplateAssignBL.setDefaultAnciTemplatesToCreateFlight(flight);
		}

		boolean isOverlapping = flight.isOverlapping();

		// set overlap details
		Integer overlapId = null;
		Collection<FlightSegement> overlapSegments = null;
		Flight overlapFlight = null;

		if (isOverlapping) {

			overlapFlight = flightDAO.getFlight(flight.getOverlapingFlightId().intValue());

			if (overlapFlight != null) {

				overlapId = overlapFlight.getFlightId();
				overlapSegments = overlapFlight.getFlightSegements();

				flight = OverlapDetailUtil.addOverlapDetailsToFlight(flight, overlapFlight);
			}
		}

		flight = timeAdder.addLocalTimeDetailsToFlight(flight);
		flightDAO.saveFlight(flight);

		if (flight.getViaScheduleMessaging() && isEnableAttachDefaultAnciTemplate && flight.getBaggabeTemplateId() != null) {
			DefaultAnciTemplateAssignBL.updadeFlightBaggages(flight, DefaultAnciTemplateAssignBL.getModelsCabinClasses(model));
		}
		//Add the flights to the list which holds the publish flight list
		flightsToPublish.add(flight);

		// save the overlapping flight
		if (isOverlapping && overlapFlight != null) {

			overlapFlight = OverlapDetailUtil.addOverlapDetailsToFlight(overlapFlight, flight);
			overlapFlight = timeAdder.addLocalTimeDetailsToFlight(overlapFlight);
			flightDAO.saveFlight(overlapFlight);
		}

		// create inventry for flight and segments
		if (flight.getOperationTypeId() != AirScheduleCustomConstants.OperationTypes.OPERATIONS) {

			int flightID = flight.getFlightId().intValue();
			Collection<SegmentDTO> intersepSegList = SegmentUtil.createSegmentDTOList(flightID, flight.getFlightSegements(),
					overlapId, overlapSegments);

			CreateFlightInventoryRQ createFlightInventoryRQ = new CreateFlightInventoryRQ();
			createFlightInventoryRQ.setFlightId(flightID);
			createFlightInventoryRQ.setAircraftModel(model);
			createFlightInventoryRQ.setSegmentDTOs(intersepSegList);
			createFlightInventoryRQ.setOverlappingFlightId(overlapId);
			Map<String, String> defaultLogicalCCs = new HashMap<String, String>();
			for (AircraftCabinCapacity ccCapacity : model.getAircraftCabinCapacitys()) {
				for (CabinClassWiseDefaultLogicalCCDetailDTO defaultLogicalCCDetailDTO : CommonsServices.getGlobalConfig()
						.getDefaultLogicalCCDetails()) {
					if (ccCapacity.getCabinClassCode() != null
							&& ccCapacity.getCabinClassCode().equalsIgnoreCase(defaultLogicalCCDetailDTO.getCcCode())) {
						defaultLogicalCCs
								.put(ccCapacity.getCabinClassCode(), defaultLogicalCCDetailDTO.getDefaultLogicalCCCode());
						break;
					}
				}
			}
			createFlightInventoryRQ.setDefaultLogicalCCs(defaultLogicalCCs);

			flightInventryBD.createFlightInventory(createFlightInventoryRQ);
		}

		// constructing and return command responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);
		if (responce.isSuccess()) {
			responce.setResponseCode(ResponceCodes.CREATE_FLIGHT_SUCCESSFULL);
			responce.addResponceParam(CommandParamNames.LAST_RESPONSE, responce);
			responce.addResponceParam(CommandParamNames.FLIGHTS_TO_PUBLISH, flightsToPublish);
		}
		return responce;
	}

	/**
	 * privte method to validate parameters
	 * 
	 * @param schedule
	 * @throws ModuleException
	 */
	private void checkParams(Flight flight) throws ModuleException {

		if (flight == null) {
			throw new ModuleException("airschedules.arg.invalid.null");
		}
	}

	/**
	 * private method to get the flight distance for the flight
	 * 
	 * @param legList
	 * @return flight distance
	 * @throws ModuleException
	 */
	private BigDecimal getFlightDistance(Collection<FlightLeg> legList) throws ModuleException {
		BigDecimal flightDistance = AccelAeroCalculator.getDefaultBigDecimalZero();

		Iterator<FlightLeg> itLegs = legList.iterator();
		while (itLegs.hasNext()) {

			FlightLeg leg = itLegs.next();
			String routeId = leg.getOrigin() + "/" + leg.getDestination();
			leg.setModelRouteId(routeId);
			RouteInfo routeInfo = commonMasterBD.getRouteInfo(routeId);
			flightDistance = AccelAeroCalculator.add(flightDistance, routeInfo.getDistance());
		}

		return flightDistance;
	}

}
