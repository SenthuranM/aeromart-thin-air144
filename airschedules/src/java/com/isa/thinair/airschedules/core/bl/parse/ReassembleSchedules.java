/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.parse;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.isa.thinair.airmaster.api.model.AirportDST;
import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.FlightScheduleLeg;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.LocalZuluTimeAdder;
import com.isa.thinair.airschedules.api.utils.ModelNamesAndFieldNames;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.ScheduledFlightUtil;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command to parse schedules to front end
 * 
 * @author Lasantha and Haider 5Aug08
 * @isa.module.command name="reassembleSchedules"
 */
public class ReassembleSchedules extends DefaultBaseCommand {

	// BDs
	private AirportBD airportBD;

	/**
	 * constructor of the ParseScheduleToFront command
	 */
	public ReassembleSchedules() {

		airportBD = AirSchedulesUtil.getAirportBD();
	}

	/**
	 * execute method of the ParseScheduleToFront command
	 * 
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	public ServiceResponce execute() throws ModuleException {

		Page page = (Page) this.getParameter(CommandParamNames.PAGE);
		Page adhocFlightpage = (Page) this.getParameter(CommandParamNames.ADHOC_FLIGHT_PAGE);
		List<ModuleCriterion> crit = (List<ModuleCriterion>) this.getParameter(CommandParamNames.CRITERIA_LIST);
		List<ModuleCriterion> critrian = (List<ModuleCriterion>) crit;
		Iterator<ModuleCriterion> it = critrian.iterator();
		Date effectiveStartDate = null;
		Date effectiveStopDate = null;
		Date effectiveStartDateTmp = null;
		Date effectiveStopDateTmp = null;

		while (it.hasNext()) {
			ModuleCriterion criterion = (ModuleCriterion) it.next();

			if (criterion.getFieldName().equals(ModelNamesAndFieldNames.FlightSchedule.FieldNames.START_DATE)) {

				effectiveStartDate = (Date) criterion.getValue().get(0);

			} else if (criterion.getFieldName().equals(ModelNamesAndFieldNames.FlightSchedule.FieldNames.STOP_DATE)) {

				effectiveStopDate = (Date) criterion.getValue().get(0);

			}
		}
		Collection<FlightSchedule> schedList = (page == null || page.getPageData() == null) ? new ArrayList<FlightSchedule>()
				: page.getPageData();
		Collection<Flight> adhocSchedList = (adhocFlightpage == null || adhocFlightpage.getPageData() == null) ? new ArrayList<Flight>()
				: adhocFlightpage.getPageData();

		// constructing command responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);

		ArrayList<Flight> adhocFlights = new ArrayList<Flight>();
		if (adhocSchedList.size() > 0) {
			LocalZuluTimeAdder timeAdder = new LocalZuluTimeAdder(airportBD);
			for (Flight flight : adhocSchedList) {
				timeAdder.addLocalTimeDetailsToFlight(flight);
				adhocFlights.add(flight);
			}
		}

		// added by Haider 05Aug08
		// split schedule according to DST
		ArrayList<FlightSchedule> tempList = new ArrayList<FlightSchedule>();
		int size = schedList.size();
		if (size > 0) {
			// Collection schedListNew = new ArrayList();
			// LocalZuluTimeAdder timeAdder = new LocalZuluTimeAdder(airportBD);
			LocalZuluTimeAdder timeAdder = new LocalZuluTimeAdder(airportBD);

			Iterator<FlightSchedule> itScheds = schedList.iterator();
			while (itScheds.hasNext()) {

				FlightSchedule schedule = (FlightSchedule) itScheds.next();

				String origin = schedule.getDepartureStnCode();
				String arrival = schedule.getArrivalStnCode();

				// set the start date of the schedule to the select date
				Date tmpDate = CalendarUtil.getTimeAddedDate(effectiveStartDate, schedule.getStartDate());
				Date tmpDate2 = timeAdder.getLocalDateTimeAsEffective(origin, tmpDate, tmpDate);
				GregorianCalendar d1 = new GregorianCalendar();
				d1.setTime(tmpDate);
				GregorianCalendar d2 = new GregorianCalendar();
				d2.setTime(tmpDate2);

				if (d1.get(Calendar.DAY_OF_MONTH) != d2.get(Calendar.DAY_OF_MONTH)) {
					d1.add(Calendar.DAY_OF_MONTH, -1);
					tmpDate = d1.getTime();
				}
				if (tmpDate.after(schedule.getStartDate())) {
					if (tmpDate.after(schedule.getStopDate()))
						schedule.setStartDate(schedule.getStopDate());
					else
						schedule.setStartDate(tmpDate);
				}

				// origin
				effectiveStartDateTmp = effectiveStartDate;
				effectiveStopDateTmp = effectiveStopDate;
				// if selected start date is before the schedule start date we should use the sch. date to get the DST
				Date tmpDate3 = timeAdder.getLocalDateTimeAsEffective(origin, schedule.getStartDate(), schedule.getStartDate());
				if (effectiveStartDate.before(tmpDate3))
					effectiveStartDateTmp = tmpDate3;

				// if the selected stop date after the sch. stop date we should use the sch. stop date to get DST
				Date tmpDate4 = timeAdder.getLocalDateTimeAsEffective(origin, schedule.getStopDate(), schedule.getStopDate());
				if (effectiveStopDate.after(tmpDate4))
					effectiveStopDateTmp = tmpDate4;

				AirportDST aptOrigStart = airportBD.getEffectiveAirportDST(origin, effectiveStartDateTmp);
				AirportDST aptOrigStop = airportBD.getEffectiveAirportDST(origin, effectiveStopDateTmp);

				// Destination
				effectiveStartDateTmp = effectiveStartDate;
				effectiveStopDateTmp = effectiveStopDate;
				// if selected start date is before the schedule start date we should use the sch. date to get the DST
				tmpDate3 = timeAdder.getLocalDateTimeAsEffective(arrival, schedule.getStartDate(), schedule.getStartDate());
				if (effectiveStartDate.before(tmpDate3))
					effectiveStartDateTmp = tmpDate3;

				// if the selected stop date after the sch. stop date we should use the sch. stop date to get DST
				tmpDate4 = timeAdder.getLocalDateTimeAsEffective(arrival, schedule.getStopDate(), schedule.getStopDate());
				if (effectiveStopDate.after(tmpDate4))
					effectiveStopDateTmp = tmpDate4;

				AirportDST aptArrStart = airportBD.getEffectiveAirportDST(arrival, effectiveStartDateTmp);
				AirportDST aptArrStop = airportBD.getEffectiveAirportDST(arrival, effectiveStopDateTmp);

				if ((aptOrigStart != null && aptOrigStop == null) && (aptArrStart != null && aptArrStop == null)) {// both
																													// oriqin
																													// and
																													// dest
																													// start
																													// date
																													// in
																													// DST
					if (aptOrigStart.getDstEndDateTime().before(aptArrStart.getDstEndDateTime())) {
						modifyAndAddTwoSchedule(schedule, tempList, page, aptOrigStart.getDstEndDateTime(),
								aptArrStart.getDstEndDateTime(), true, timeAdder);
					} else if (aptOrigStart.getDstEndDateTime().after(aptArrStart.getDstEndDateTime())) {
						modifyAndAddTwoSchedule(schedule, tempList, page, aptArrStart.getDstEndDateTime(),
								aptOrigStart.getDstEndDateTime(), true, timeAdder);
					} else if (aptOrigStart.getDstEndDateTime().equals(aptArrStart.getDstEndDateTime())) {
						modifyAndAddOneSchedule(schedule, tempList, page, aptArrStart.getDstEndDateTime(), true, timeAdder);
					}
				} else if (aptOrigStart != null && aptOrigStop == null) {// orig start date in DST
					modifyAndAddOneSchedule(schedule, tempList, page, aptOrigStart.getDstEndDateTime(), true, timeAdder);
				} else if (aptArrStart != null && aptArrStop == null) { // dest start date in DST
					modifyAndAddOneSchedule(schedule, tempList, page, aptArrStart.getDstEndDateTime(), true, timeAdder);
				}

				if ((aptOrigStart == null && aptOrigStop != null) && (aptArrStart == null && aptArrStop != null)) {// both
																													// oriqin
																													// and
																													// dest
																													// stop
																													// date
																													// in
																													// DST
					if (aptOrigStop.getDstStartDateTime().before(aptArrStop.getDstStartDateTime())) {
						modifyAndAddTwoSchedule(schedule, tempList, page, aptOrigStop.getDstStartDateTime(),
								aptArrStop.getDstStartDateTime(), false, timeAdder);
					} else if (aptOrigStop.getDstStartDateTime().after(aptArrStop.getDstStartDateTime())) {
						modifyAndAddTwoSchedule(schedule, tempList, page, aptArrStop.getDstStartDateTime(),
								aptOrigStop.getDstStartDateTime(), false, timeAdder);
					} else if (aptOrigStop.getDstStartDateTime().equals(aptArrStop.getDstStartDateTime())) {
						modifyAndAddOneSchedule(schedule, tempList, page, aptArrStop.getDstStartDateTime(), false, timeAdder);
					}
				} else if (aptOrigStart == null && aptOrigStop != null) {// orig start date in DST
					modifyAndAddOneSchedule(schedule, tempList, page, aptOrigStop.getDstStartDateTime(), false, timeAdder);
				} else if (aptArrStart == null && aptArrStop != null) { // dest start date in DST
					modifyAndAddOneSchedule(schedule, tempList, page, aptArrStop.getDstStartDateTime(), false, timeAdder);
				}
				Set<FlightScheduleLeg> schedLegs = schedule.getFlightScheduleLegs();
				boolean scheduleLegNotInLocal = false;
				for(FlightScheduleLeg scheduleLeg :schedLegs){
					if(scheduleLeg.getEstDepartureTimeLocal() == null || scheduleLeg.getEstDepartureTimeLocal() == null ){
						scheduleLegNotInLocal = true;
					}
				}
				
				if (schedule.getStartDateLocal() == null || schedule.getStopDateLocal() == null || scheduleLegNotInLocal)
					timeAdder.addLocalTimeDetailsToFlightSchedule(schedule, false);
			}

		} // if flight list has flights
		for (int i = 0; i < tempList.size(); i++) {
			schedList.add(tempList.get(i));
			page.setEndPosition(page.getEndPosition() + 1);
			page.setTotalNoOfRecords(page.getTotalNoOfRecords() + 1);
		}

		Collection<Flight> adhocSchedListNew = new ArrayList<Flight>();
		for (int i = 0; i < adhocFlights.size(); i++) {
			adhocSchedListNew.add(adhocFlights.get(i));
			adhocFlightpage.setEndPosition(adhocFlightpage.getEndPosition() + 1);
			adhocFlightpage.setTotalNoOfRecords(adhocFlightpage.getTotalNoOfRecords() + 1);
		}
		adhocFlightpage.setPageData(adhocSchedListNew);
		responce.addResponceParam(CommandParamNames.PAGE, page);
		responce.addResponceParam(CommandParamNames.ADHOC_FLIGHT_PAGE, adhocFlightpage);
		/*
		 * int newSize =schedList.size(); if(newSize > 0) {
		 * 
		 * LocalZuluTimeAdder timeAdder = new LocalZuluTimeAdder(airportBD);
		 * 
		 * Iterator itScheds = schedList.iterator(); while(itScheds.hasNext()) {
		 * 
		 * FlightSchedule schedule = (FlightSchedule)itScheds.next(); schedule =
		 * timeAdder.addLocalTimeDetailsToFlightSchedule(schedule); }
		 * 
		 * responce.addResponceParam(CommandParamNames.PAGE, page);
		 * 
		 * }else { responce.addResponceParam(CommandParamNames.PAGE, page); }
		 */
		// return command responce
		return responce;
	}

	private void modifyAndAddOneSchedule(FlightSchedule schedule, ArrayList<FlightSchedule> schedList, Page page, Date date,
			boolean bStartDate, LocalZuluTimeAdder timeAdder) throws ModuleException {
		FlightSchedule newSch = ScheduledFlightUtil.getCopyOfSchedule(schedule);
		Date newDate = CalendarUtil.getTimeAddedDate(date, schedule.getStopDate());
		Date date1, date2;
		if (bStartDate) {
			schedule.setStopDate(newDate);
			Calendar aCal = Calendar.getInstance();
			aCal.setTime(newDate);
			aCal.add(Calendar.DATE, 1);
			newSch.setStartDate(aCal.getTime());
			date1 = newDate;
			date2 = aCal.getTime();
		} else {
			Calendar aCal = Calendar.getInstance();
			aCal.setTime(newDate);
			aCal.add(Calendar.DATE, -1);
			schedule.setStopDate(aCal.getTime());
			newSch.setStartDate(newDate);
			date2 = newDate;
			date1 = aCal.getTime();
		}
		timeAdder.addLocalTimeDetailsToFlightSchedule(schedule, false);
		timeAdder.addLocalTimeDetailsToFlightSchedule(newSch, false);
		schedule.setStopDateLocal(addDatePartToDate(schedule.getStopDateLocal(), date1));
		// schedule.setStartDateLocal(addDatePartToDate(schedule.getStartDateLocal(), schedule.getStartDate()));
		newSch.setStartDateLocal(addDatePartToDate(newSch.getStartDateLocal(), date2));
		schedList.add(newSch);
	}

	private Date addDatePartToDate(Date date1, Date date2) {
		GregorianCalendar date1Calender = new GregorianCalendar();
		date1Calender.setTime(date1);

		GregorianCalendar date2Calender = new GregorianCalendar();
		date2Calender.setTime(date2);

		date1Calender.set(GregorianCalendar.DATE, date2Calender.get(GregorianCalendar.DATE));
		date1Calender.set(GregorianCalendar.MONTH, date2Calender.get(GregorianCalendar.MONTH));
		date1Calender.set(GregorianCalendar.YEAR, date2Calender.get(GregorianCalendar.YEAR));
		return date1Calender.getTime();

	}

	private void modifyAndAddTwoSchedule(FlightSchedule schedule, ArrayList<FlightSchedule> schedList, Page page, Date date1,
			Date date2, boolean bStartDate, LocalZuluTimeAdder timeAdder) throws ModuleException {
		FlightSchedule newSch = ScheduledFlightUtil.getCopyOfSchedule(schedule);
		FlightSchedule newSch2 = ScheduledFlightUtil.getCopyOfSchedule(schedule);
		Date newDate1 = CalendarUtil.getTimeAddedDate(date1, schedule.getStopDate());
		Date newDate2 = CalendarUtil.getTimeAddedDate(date2, schedule.getStopDate());
		Date d1, d2, d3, d4;
		if (bStartDate) {// when the start date for both origin and dest in DST time
			schedule.setStopDate(newDate1);
			Calendar aCal = Calendar.getInstance();
			aCal.setTime(newDate1);
			aCal.add(Calendar.DATE, 1);
			newSch.setStartDate(aCal.getTime());
			newSch.setStopDate(newDate2);
			d2 = aCal.getTime();
			d4 = newDate2;
			aCal = Calendar.getInstance();
			aCal.setTime(newDate2);
			aCal.add(Calendar.DATE, 1);
			newSch2.setStartDate(aCal.getTime());
			d1 = newDate1;
			d3 = aCal.getTime();
		} else {// when the stop date for both origin and dest in DST time
			Calendar aCal = Calendar.getInstance();
			aCal.setTime(newDate1);
			aCal.add(Calendar.DATE, -1);
			schedule.setStopDate(aCal.getTime());
			newSch.setStartDate(newDate1);
			aCal = Calendar.getInstance();
			aCal.setTime(newDate2);
			aCal.add(Calendar.DATE, -1);
			newSch.setStopDate(aCal.getTime());
			d2 = newDate1;
			d4 = aCal.getTime();

			newSch2.setStartDate(newDate2);
			d3 = newDate2;
			d1 = aCal.getTime();
		}
		timeAdder.addLocalTimeDetailsToFlightSchedule(schedule, false);
		timeAdder.addLocalTimeDetailsToFlightSchedule(newSch, false);
		timeAdder.addLocalTimeDetailsToFlightSchedule(newSch2, false);
		schedule.setStopDateLocal(addDatePartToDate(schedule.getStopDateLocal(), d1));
		schedule.setStartDateLocal(addDatePartToDate(schedule.getStartDateLocal(), schedule.getStartDate()));
		newSch.setStartDateLocal(addDatePartToDate(newSch.getStartDateLocal(), d3));
		newSch2.setStartDateLocal(addDatePartToDate(newSch2.getStartDateLocal(), d2));
		newSch2.setStopDateLocal(addDatePartToDate(newSch2.getStartDateLocal(), d4));

		schedList.add(newSch);
		schedList.add(newSch2);
	}
}
