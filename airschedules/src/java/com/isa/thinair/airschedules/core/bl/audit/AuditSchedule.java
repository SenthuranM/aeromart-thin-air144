package com.isa.thinair.airschedules.core.bl.audit;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;

import com.isa.thinair.airschedules.api.dto.AffectedPeriod;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.core.persistence.dao.FlightScheduleDAO;
import com.isa.thinair.airschedules.core.util.AuditUtil;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.airschedules.core.utils.CommandNames;
import com.isa.thinair.auditor.api.model.Audit;
import com.isa.thinair.auditor.api.service.AuditorBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.TasksUtil;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for audit flight schedule
 * 
 * @author Chandana Kithalagama
 * @isa.module.command name="auditSchedule"
 */
public class AuditSchedule extends DefaultBaseCommand {

	private static final int AUDIT_ADD_SCHEDULE = 0;
	private static final int AUDIT_AMMEND_SCHEDULE = 1;
	private static final int AUDIT_CANCEL_SCHEDULE = 2;
	private static final int AUDIT_COPY_SCHEDULE = 3;
	private static final int AUDIT_BUILD_SCHEDULE = 4;
	private static final int AUDIT_SPLIT_SCHEDULE = 5;
	private final FlightScheduleDAO flightSchedleDAO;
	AuditorBD auditorBD = null;

	/**
	 * constructor of the AuditSchedule command
	 */
	public AuditSchedule() {

		auditorBD = AirSchedulesUtil.getAuditorBD();
		flightSchedleDAO = (FlightScheduleDAO) AirSchedulesUtil.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_DAO);
	}

	/**
	 * execute method of the AuditSchedule command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {

		String commandName = (String) this.getParameter(CommandParamNames.COMMAND_NAME);
		UserPrincipal principal = (UserPrincipal) this.getParameter(CommandParamNames.USER_PRINCIPAL);
		FlightSchedule schedule = (FlightSchedule) this.getParameter(CommandParamNames.FLIGHT_SCHEDULE);
		FlightSchedule overlappingSchedule = (FlightSchedule) this.getParameter(CommandParamNames.OVERLAPPING_SCHEDULE);
		Integer scheduleId = (Integer) this.getParameter(CommandParamNames.SCHEDULE_ID);
		Date fromDate = (Date) this.getParameter(CommandParamNames.START_DATE);
		Date toDate = (Date) this.getParameter(CommandParamNames.END_DATE);
		Collection<Integer> scheduleIds = (Collection) this.getParameter(CommandParamNames.SCHEDULE_IDS);
		AffectedPeriod period = (AffectedPeriod) this.getParameter(CommandParamNames.AFFECTED_PERIOD);
		DefaultServiceResponse lastResponse = (DefaultServiceResponse) this.getParameter(CommandParamNames.LAST_RESPONSE);
		Collection<Integer> newScheduleIdsFromSplit = (Collection) this.getParameter(CommandParamNames.NEW_SCHEDULE_IDS_FROM_SPLIT);

		// checking params
		int type = this.checkParams(commandName, principal, schedule, scheduleId, fromDate, toDate, scheduleIds, period);

		Audit audit = new Audit();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		if (principal != null)
			audit.setUserId(principal.getName());
		else
			audit.setUserId("");

		audit.setTimestamp(new Date());

		LinkedHashMap<String, Object> contents = new LinkedHashMap<String, Object>();

		// check for schedules
		if (type == AuditSchedule.AUDIT_ADD_SCHEDULE || type == AuditSchedule.AUDIT_AMMEND_SCHEDULE) {

			if (type == AuditSchedule.AUDIT_ADD_SCHEDULE)
				audit.setTaskCode(String.valueOf(TasksUtil.FLIGHT_ADD_FLIGHT_SCHEDULES));
			else
				audit.setTaskCode(String.valueOf(TasksUtil.FLIGHT_EDIT_FLIGHT_SCHEDULES));

			contents = AuditUtil.addScheduleInfo(schedule);

		} else if (type == AuditSchedule.AUDIT_COPY_SCHEDULE) {

			audit.setTaskCode(String.valueOf(TasksUtil.FLIGHT_COPY_FLIGHT_SCHEDULES));

			contents.put(AuditParams.SCHEDULE_ID, scheduleId);
			contents.put(AuditParams.SCHEDULE_START_DATE, dateFormat.format(fromDate));
			contents.put(AuditParams.SCHEDULE_STOP_DATE, dateFormat.format(toDate));

		} else if (type == AuditSchedule.AUDIT_CANCEL_SCHEDULE) {

			audit.setTaskCode(String.valueOf(TasksUtil.FLIGHT_CANCEL_FLIGHT_SCHEDULES));

			contents = AuditUtil.addScheduleInfo(schedule);
			// contents.put(AuditParams.SCHEDULE_ID, scheduleId);

		} else if (type == AuditSchedule.AUDIT_BUILD_SCHEDULE) {

			audit.setTaskCode(String.valueOf(TasksUtil.FLIGHT_BUILD_FLIGHT_SCHEDULES));
			Iterator<Integer> it = scheduleIds.iterator();
			String schedIds = "";

			while (it.hasNext()) {
				schedIds = ((Integer) it.next()).toString() + ", ";
			}

			if (schedIds.lastIndexOf(',') > 0)
				schedIds = schedIds.substring(0, schedIds.lastIndexOf(','));
			FlightSchedule exitsSchedule = flightSchedleDAO.getFlightSchedule(Integer.parseInt(schedIds));
			contents = AuditUtil.addScheduleInfo(exitsSchedule);
			contents.put(AuditParams.SCHEDULE_ID, schedIds);

		} else if (type == AuditSchedule.AUDIT_SPLIT_SCHEDULE) {

			audit.setTaskCode(String.valueOf(TasksUtil.FLIGHT_SPLIT_FLIGHT_SCHEDULES));

			contents.put(AuditParams.SCHEDULE_ID, scheduleId);
			contents.put(AuditParams.SCHEDULE_SPLIT_FROM, dateFormat.format(period.getStartDate()));
			contents.put(AuditParams.SCHEDULE_SPLIT_TO, dateFormat.format(period.getEndDate()));
			contents.put(AuditParams.SPLIT_FREQUENCY, AuditUtil.getFrequencyAuditInfo(period.getFrequency()));
			contents.put(AuditParams.NEW_SCHEDULE_IDS_FROM_SPLIT, AuditUtil.getScheduleIdsString(newScheduleIdsFromSplit));
		}

		// check for overlapping schedules
		Audit auditOverlap = new Audit();
		LinkedHashMap<String, Object> contentsOverlap = new LinkedHashMap<String, Object>();

		if (overlappingSchedule != null) {

			if (principal != null)
				auditOverlap.setUserId(principal.getName());
			else
				auditOverlap.setUserId("");

			auditOverlap.setTimestamp(new Date());

			if (type == AuditSchedule.AUDIT_ADD_SCHEDULE || type == AuditSchedule.AUDIT_AMMEND_SCHEDULE) {

				if (type == AuditSchedule.AUDIT_ADD_SCHEDULE)
					auditOverlap.setTaskCode(String.valueOf(TasksUtil.FLIGHT_ADD_FLIGHT_SCHEDULES));
				else
					auditOverlap.setTaskCode(String.valueOf(TasksUtil.FLIGHT_EDIT_FLIGHT_SCHEDULES));

				contentsOverlap = AuditUtil.addScheduleInfo(overlappingSchedule);

			} else if (type == AuditSchedule.AUDIT_CANCEL_SCHEDULE) {

				auditOverlap.setTaskCode(String.valueOf(TasksUtil.FLIGHT_CANCEL_FLIGHT_SCHEDULES));

				contentsOverlap = AuditUtil.addScheduleInfo(overlappingSchedule);
				// contentsOverlap.put(AuditParams.SCHEDULE_ID, overlappingSchedule.getScheduleId());

			} else if (type == AuditSchedule.AUDIT_SPLIT_SCHEDULE) {

				auditOverlap.setTaskCode(String.valueOf(TasksUtil.FLIGHT_SPLIT_FLIGHT_SCHEDULES));

				contentsOverlap.put(AuditParams.SCHEDULE_ID, overlappingSchedule.getScheduleId());
				// start and stop date of overlapping schedule could be different to schedule values
				contentsOverlap.put(AuditParams.SCHEDULE_SPLIT_FROM, dateFormat.format(period.getStartDate()));
				contentsOverlap.put(AuditParams.SCHEDULE_SPLIT_TO, dateFormat.format(period.getEndDate()));
				//
				contentsOverlap.put(AuditParams.SPLIT_FREQUENCY, AuditUtil.getFrequencyAuditInfo(period.getFrequency()));
			}
		}

		if (contents.size() > 0)
			auditorBD.audit(audit, contents);
		if (overlappingSchedule != null && contentsOverlap.size() > 0)
			auditorBD.audit(auditOverlap, contentsOverlap);

		return lastResponse;
	}

	/**
	 * privte method to validate parameters
	 * 
	 * @param schedule
	 * @throws ModuleException
	 */
	private int checkParams(String commandName, UserPrincipal principal, FlightSchedule schedule, Integer scheduleId,
			Date fromDate, Date toDate, Collection<Integer> scheduleIds, AffectedPeriod period) throws ModuleException {

		if (principal == null)
			throw new ModuleException("airschedules.arg.invalid.null");

		int validationType = -1;

		if ((schedule != null) && commandName == CommandNames.CREATE_SCHEDULE_MACRO) {

			validationType = AuditSchedule.AUDIT_ADD_SCHEDULE;

		} else if (schedule != null
				&& (commandName == CommandNames.AMMEND_SCHEDULE_MACRO || commandName == CommandNames.AMMEND_SCHEDULE_LEGS_MACRO)) {

			validationType = AuditSchedule.AUDIT_AMMEND_SCHEDULE;

		} else if (scheduleId != null && commandName == CommandNames.CANCEL_SCHEDULE_MACRO) {

			validationType = AuditSchedule.AUDIT_CANCEL_SCHEDULE;

		} else if (scheduleId != null && fromDate != null && toDate != null && commandName == CommandNames.COPY_SCHEDULE_MACRO) {

			validationType = AuditSchedule.AUDIT_COPY_SCHEDULE;

		} else if (scheduleIds != null && commandName == CommandNames.BUILD_SCHEDULE_MACRO) {

			validationType = AuditSchedule.AUDIT_BUILD_SCHEDULE;

		} else if (scheduleId != null && period != null && commandName == CommandNames.SPLIT_SCHEDULE_MACRO) {

			validationType = AuditSchedule.AUDIT_SPLIT_SCHEDULE;

		} else if (scheduleIds != null && (commandName == CommandNames.BUILD_FLIGHT_SCHEDULE_MACRO)
				|| commandName == CommandNames.INTEGRATE_BUILD_SCHEDULE) {

			validationType = AuditSchedule.AUDIT_BUILD_SCHEDULE;

		} else {
			// throw exception
			throw new ModuleException("airschedules.arg.invalid.null");
		}

		return validationType;
	}
}
