/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.util;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.isa.thinair.airschedules.api.dto.ScheduleSearchDTO;
import com.isa.thinair.airschedules.api.utils.ModelNamesAndFieldNames;
import com.isa.thinair.commons.api.dto.ModuleCriteria;
import com.isa.thinair.commons.api.dto.ModuleCriterion;

/**
 * @author Lasantha Pambagoda
 */
public class CriteriaQueryUtil {

	public static ScheduleSearchDTO getScheduleSearchDTO(List<ModuleCriterion> criteriaList) {

		ScheduleSearchDTO dto = null;

		if ((criteriaList != null) && (criteriaList.size() > 0)) {

			dto = new ScheduleSearchDTO();

			Iterator<ModuleCriterion> it = criteriaList.iterator();
			while (it.hasNext()) {
				ModuleCriterion criterion = (ModuleCriterion) it.next();

				if (criterion.getFieldName().equals(ModelNamesAndFieldNames.FlightSchedule.FieldNames.START_DATE)) {

					dto.setStartDate((Date) criterion.getValue().get(0));

				} else if (criterion.getFieldName().equals(ModelNamesAndFieldNames.FlightSchedule.FieldNames.STOP_DATE)) {

					dto.setEndDate((Date) criterion.getValue().get(0));

				} else if (criterion.getFieldName().equals(ModelNamesAndFieldNames.FlightSchedule.FieldNames.STATUS_CODE)) {

					dto.setScheduleStatus((String) criterion.getValue().get(0));

				} else if (criterion.getFieldName().equals(ModelNamesAndFieldNames.FlightSchedule.FieldNames.BUILD_STATUS_CODE)) {

					dto.setBuildStatus((String) criterion.getValue().get(0));

				} else if (criterion.getFieldName().equals(ModelNamesAndFieldNames.FlightSchedule.FieldNames.OPERATION_TYPE_ID)) {

					dto.setOpType((Integer) criterion.getValue().get(0));

				} else if (criterion.getFieldName().equals(ModelNamesAndFieldNames.FlightSchedule.FieldNames.FLIGHT_NUMBER)) {
					if (!criterion.isLeaf() && criterion.getValue() == null) {
						ModuleCriteria c = (ModuleCriteria) criterion;
						for (int i = 0; i < c.getModulecriterionList().size(); i++) {
							ModuleCriterion mc = (ModuleCriterion) c.getModulecriterionList().get(i);
							dto.getFlightNumers().add((String)mc.getValue().get(0));
						}
					} else {

						dto.setFlightNumber((String) criterion.getValue().get(0));
					}

				} else if (criterion.getFieldName().equals(ModelNamesAndFieldNames.FlightSchedule.FieldNames.DEPARTURE_APT_CODE)) {

					dto.setFromAirport((String) criterion.getValue().get(0));

				} else if (criterion.getFieldName().equals(ModelNamesAndFieldNames.FlightSchedule.FieldNames.ARRIVAL_APT_CODE)) {

					dto.setToAirport((String) criterion.getValue().get(0));
				} else if (criterion.getFieldName().equals(ModelNamesAndFieldNames.FlightSchedule.FieldNames.FLIGHT_TYPE)) {
					dto.setFlightType((String) criterion.getValue().get(0));
				}

			}
		}
		return dto;
	}

	public static Date getStartDateOfFligthSearch(List<ModuleCriterion> criteriaList) {

		Date startDate = null;

		if ((criteriaList != null) && (criteriaList.size() > 0)) {

			Iterator<ModuleCriterion> it = criteriaList.iterator();
			while (it.hasNext()) {

				ModuleCriterion criterion = (ModuleCriterion) it.next();

				if (criterion.getFieldName().equals(ModelNamesAndFieldNames.Flight.FieldNames.DEPARTURE_DATE)
						&& criterion.getCondition().equals(ModuleCriterion.CONDITION_GREATER_THAN_OR_EQUALS)) {

					startDate = (Date) criterion.getValue().get(0);
					break;
				}
			}
		}

		return startDate;
	}

	public static List<ModuleCriterion> setStartDateOfFligthSearch(List<ModuleCriterion> criteriaList, Date startDate) {

		if ((criteriaList != null) && (criteriaList.size() > 0)) {

			Iterator<ModuleCriterion> it = criteriaList.iterator();
			while (it.hasNext()) {

				ModuleCriterion criterion = (ModuleCriterion) it.next();

				if (criterion.getFieldName().equals(ModelNamesAndFieldNames.Flight.FieldNames.DEPARTURE_DATE)
						&& criterion.getCondition().equals(ModuleCriterion.CONDITION_GREATER_THAN_OR_EQUALS)) {

					criterion.getValue().set(0, startDate);
					break;
				}
			}
		}

		return criteriaList;
	}

	public static List<ModuleCriterion> removeStartDateOfFligthSearch(List<ModuleCriterion> criteriaList) {

		if ((criteriaList != null) && (criteriaList.size() > 0)) {

			for (int i = 0; i < criteriaList.size(); i++) {
				ModuleCriterion criterion = (ModuleCriterion) criteriaList.get(i);

				if (criterion.getFieldName().equals(ModelNamesAndFieldNames.Flight.FieldNames.DEPARTURE_DATE)
						&& criterion.getCondition().equals(ModuleCriterion.CONDITION_GREATER_THAN_OR_EQUALS)) {

					criteriaList.remove(i);
					break;
				}
			}
		}

		return criteriaList;
	}

	public static List<ModuleCriterion> removeEndDateOfFligthSearch(List<ModuleCriterion> criteriaList) {

		if ((criteriaList != null) && (criteriaList.size() > 0)) {

			for (int i = 0; i < criteriaList.size(); i++) {
				ModuleCriterion criterion = (ModuleCriterion) criteriaList.get(i);

				if (criterion.getFieldName().equals(ModelNamesAndFieldNames.Flight.FieldNames.DEPARTURE_DATE)
						&& criterion.getCondition().equals(ModuleCriterion.CONDITION_LESS_THAN_OR_EQUALS)) {

					criteriaList.remove(i);
					break;
				}
			}
		}

		return criteriaList;
	}

	public static Date getEndDateOfFligthSearch(List<ModuleCriterion> criteriaList) {

		Date endDate = null;

		if ((criteriaList != null) && (criteriaList.size() > 0)) {

			Iterator<ModuleCriterion> it = criteriaList.iterator();
			while (it.hasNext()) {

				ModuleCriterion criterion = (ModuleCriterion) it.next();

				if (criterion.getFieldName().equals(ModelNamesAndFieldNames.Flight.FieldNames.DEPARTURE_DATE)
						&& criterion.getCondition().equals(ModuleCriterion.CONDITION_LESS_THAN_OR_EQUALS)) {

					endDate = (Date) criterion.getValue().get(0);
					break;
				}
			}
		}

		return endDate;
	}

	public static List<ModuleCriterion> setEndDateOfFligthSearch(List<ModuleCriterion> criteriaList, Date endDate) {

		if ((criteriaList != null) && (criteriaList.size() > 0)) {

			Iterator<ModuleCriterion> it = criteriaList.iterator();
			while (it.hasNext()) {

				ModuleCriterion criterion = (ModuleCriterion) it.next();

				if (criterion.getFieldName().equals(ModelNamesAndFieldNames.Flight.FieldNames.DEPARTURE_DATE)
						&& criterion.getCondition().equals(ModuleCriterion.CONDITION_LESS_THAN_OR_EQUALS)) {

					criterion.getValue().set(0, endDate);
					break;
				}
			}
		}

		return criteriaList;
	}

	public static String getOriginOfFligthSearch(List<ModuleCriterion> criteriaList) {

		String origin = null;

		if ((criteriaList != null) && (criteriaList.size() > 0)) {

			Iterator<ModuleCriterion> it = criteriaList.iterator();
			while (it.hasNext()) {
				ModuleCriterion criterion = (ModuleCriterion) it.next();

				if (criterion.getFieldName().equals(ModelNamesAndFieldNames.Flight.FieldNames.ORIGIN_APT_CODE)) {

					origin = (String) criterion.getValue().get(0);
					break;
				}
			}
		}
		return origin;
	}

	public static String getDesinationOfFligthSearch(List<ModuleCriterion> criteriaList) {

		String destination = null;

		if ((criteriaList != null) && (criteriaList.size() > 0)) {

			Iterator<ModuleCriterion> it = criteriaList.iterator();
			while (it.hasNext()) {
				ModuleCriterion criterion = (ModuleCriterion) it.next();

				if (criterion.getFieldName().equals(ModelNamesAndFieldNames.Flight.FieldNames.DESTINATION_APT_CODE)) {

					destination = (String) criterion.getValue().get(0);
					break;
				}
			}
		}
		return destination;
	}

	public static String getFlightNumberOfFligthSearch(List<ModuleCriterion> criteriaList) {

		String flightNumber = null;

		if ((criteriaList != null) && (criteriaList.size() > 0)) {

			Iterator<ModuleCriterion> it = criteriaList.iterator();
			while (it.hasNext()) {
				ModuleCriterion criterion = (ModuleCriterion) it.next();

				if (criterion.getFieldName().equals(ModelNamesAndFieldNames.Flight.FieldNames.FLIGHT_NUMBER)) {

					flightNumber = (String) criterion.getValue().get(0);
					break;
				}
			}
		}
		return flightNumber;
	}

	/**
	 * Method will identify if any airport station restriction applied for search AARESAA-1950
	 * 
	 * @param criteriaList
	 */
	public static List<ModuleCriterion> getTerrotoryList(List<ModuleCriterion> criteriaList) {

		List<ModuleCriterion> terotaryList = null;

		if ((criteriaList != null) && (criteriaList.size() > 0)) {

			Iterator<ModuleCriterion> it = criteriaList.iterator();
			while (it.hasNext()) {
				ModuleCriterion criterion = (ModuleCriterion) it.next();

				if (criterion != null && criterion.getFieldName() != null
						&& criterion.getFieldName().equals(ModelNamesAndFieldNames.Flight.FieldNames.DESTINATION_APT_CODE)
						&& criterion.getCondition().equals(ModuleCriterion.CONDITION_IN)) {

					terotaryList = (List) criterion.getValue();
					break;
				}
			}
		}
		return terotaryList;

	}

	/**
	 * Method will identify if any airport station restriction applied for search if it will remove it for temporally .
	 * AARESAA-1950
	 * 
	 * @param criteriaList
	 */
	public static List<ModuleCriterion> removeTerritorys(List<ModuleCriterion> criteriaList) {

		if ((criteriaList != null) && (criteriaList.size() > 0)) {

			for (int i = 0; i < criteriaList.size(); i++) {
				ModuleCriterion criterion = (ModuleCriterion) criteriaList.get(i);

				if (criterion != null && criterion.getFieldName() != null
						&& criterion.getFieldName().equals(ModelNamesAndFieldNames.Flight.FieldNames.DESTINATION_APT_CODE)
						&& criterion.getCondition().equals(ModuleCriterion.CONDITION_IN)) {

					criteriaList.remove(i);
					break;
				}
			}
		}

		return criteriaList;
	}
}
