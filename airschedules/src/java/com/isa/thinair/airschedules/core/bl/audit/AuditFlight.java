package com.isa.thinair.airschedules.core.bl.audit;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;

import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.core.util.AuditUtil;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.utils.CommandNames;
import com.isa.thinair.auditor.api.model.Audit;
import com.isa.thinair.auditor.api.service.AuditorBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.TasksUtil;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for audit flight
 * 
 * @author Chandana Kithalagama
 * @isa.module.command name="auditFlight"
 */
public class AuditFlight extends DefaultBaseCommand {

	private static final int AUDIT_ADD_FLIGHT = 0;
	private static final int AUDIT_AMMEND_FLIGHT = 1;
	private static final int AUDIT_CANCEL_FLIGHT = 2;
	private static final int AUDIT_COPY_FLIGHT = 3;

	AuditorBD auditorBD = null;

	/**
	 * constructor of the AuditFlight command
	 */
	public AuditFlight() {

		auditorBD = AirSchedulesUtil.getAuditorBD();

	}

	/**
	 * execute method of the AuditFlight command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {

		String commandName = (String) this.getParameter(CommandParamNames.COMMAND_NAME);
		UserPrincipal principal = (UserPrincipal) this.getParameter(CommandParamNames.USER_PRINCIPAL);
		Flight flight = (Flight) this.getParameter(CommandParamNames.FLIGHT);
		Flight overlappingFlight = (Flight) this.getParameter(CommandParamNames.OVERLAPPING_FLIGHT);
		Integer flightId = (Integer) this.getParameter(CommandParamNames.FLIGHT_ID);
		DefaultServiceResponse lastResponse = (DefaultServiceResponse) this.getParameter(CommandParamNames.LAST_RESPONSE);

		// checking params
		int type = this.checkParams(commandName, principal, flight, flightId);

		Audit audit = new Audit();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		if (principal != null)
			audit.setUserId(principal.getName());
		else
			audit.setUserId("");

		audit.setTimestamp(new Date());

		LinkedHashMap<String, Object> contents = null;

		// check for schedules
		if (type == AuditFlight.AUDIT_ADD_FLIGHT || type == AuditFlight.AUDIT_AMMEND_FLIGHT) {

			if (type == AuditFlight.AUDIT_ADD_FLIGHT)
				audit.setTaskCode(String.valueOf(TasksUtil.FLIGHT_ADD_FLIGHTS));
			else
				audit.setTaskCode(String.valueOf(TasksUtil.FLIGHT_EDIT_FLIGHTS));

			contents = AuditUtil.addFlightInfo(flight);

		} else if (type == AuditFlight.AUDIT_CANCEL_FLIGHT) {

			audit.setTaskCode(String.valueOf(TasksUtil.FLIGHT_CANCEL_FLIGHTS));

			contents = AuditUtil.addFlightInfo(flight);

		} else if (type == AuditFlight.AUDIT_COPY_FLIGHT) {

			audit.setTaskCode(String.valueOf(TasksUtil.FLIGHT_COPY_FLIGHTS));

			contents = new LinkedHashMap<String, Object>();
			contents.put(AuditParams.FLIGHT_ID, flightId);
			contents.put(AuditParams.FLIGHT_NO, flight.getFlightNumber());
			contents.put(AuditParams.COPIED_TO_DATE, dateFormat.format(flight.getDepartureDate()));

		}
		contents.put(AuditParams.USER_NAME, principal.getAgentCode());
		// check for overlapping schedules
		Audit auditOverlap = new Audit();
		LinkedHashMap<String, Object> contentsOverlap = null;

		if (overlappingFlight != null) {

			if (principal != null)
				auditOverlap.setUserId(principal.getName());
			else
				auditOverlap.setUserId("");

			auditOverlap.setTimestamp(new Date());

			if (type == AuditFlight.AUDIT_AMMEND_FLIGHT) {

				auditOverlap.setTaskCode(String.valueOf(TasksUtil.FLIGHT_EDIT_FLIGHTS));

				contentsOverlap = AuditUtil.addFlightInfo(overlappingFlight);

			} else if (type == AuditFlight.AUDIT_CANCEL_FLIGHT) {

				auditOverlap.setTaskCode(String.valueOf(TasksUtil.FLIGHT_CANCEL_FLIGHTS));

				contentsOverlap = AuditUtil.addFlightInfo(overlappingFlight);
			}
		}

		auditorBD.audit(audit, contents);
		if (overlappingFlight != null)
			auditorBD.audit(auditOverlap, contentsOverlap);

		return lastResponse;
	}

	/**
	 * privte method to validate parameters
	 * 
	 * @param schedule
	 * @throws ModuleException
	 */
	private int checkParams(String commandName, UserPrincipal principal, Flight flight, Integer flightId) throws ModuleException {

		if (principal == null)
			throw new ModuleException("airschedules.arg.invalid.null");

		int validationType = -1;

		if (flight != null && commandName == CommandNames.CREATE_FLIGHT_MACRO) {

			validationType = AuditFlight.AUDIT_ADD_FLIGHT;

		} else if (flight != null && commandName == CommandNames.AMMEND_FLIGHT_MACRO) {

			validationType = AuditFlight.AUDIT_AMMEND_FLIGHT;

		} else if (flight != null && commandName == CommandNames.CANCEL_FLIGHT_MACRO) {

			validationType = AuditFlight.AUDIT_CANCEL_FLIGHT;

		} else if (flightId != null && commandName == CommandNames.COPY_FLIGHT_MACRO) {

			validationType = AuditFlight.AUDIT_COPY_FLIGHT;

		} else {
			// throw exception
			throw new ModuleException("airschedules.arg.invalid.null");
		}

		return validationType;
	}
}
