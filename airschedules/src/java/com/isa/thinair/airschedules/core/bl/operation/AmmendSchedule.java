/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.operation;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.CreateFlightInventoryRQ;
import com.isa.thinair.airinventory.api.dto.SegmentDTO;
import com.isa.thinair.airinventory.api.service.FlightInventoryBD;
import com.isa.thinair.airinventory.api.service.LogicalCabinClassBD;
import com.isa.thinair.airmaster.api.model.AircraftCabinCapacity;
import com.isa.thinair.airmaster.api.model.AircraftModel;
import com.isa.thinair.airmaster.api.model.RouteInfo;
import com.isa.thinair.airmaster.api.service.AircraftBD;
import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.airreservation.api.dto.FlightReservationAlertDTO;
import com.isa.thinair.airreservation.api.service.SegmentBD;
import com.isa.thinair.airschedules.api.dto.AffectedPeriod;
import com.isa.thinair.airschedules.api.dto.FlightAlertDTO;
import com.isa.thinair.airschedules.api.model.CodeShareMCFlight;
import com.isa.thinair.airschedules.api.model.CodeShareMCFlightSchedule;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightLeg;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.FlightScheduleLeg;
import com.isa.thinair.airschedules.api.model.FlightScheduleSegment;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.model.SSMSplitFlightSchedule;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.AirportTerminalUtil;
import com.isa.thinair.airschedules.api.utils.AlertUtil;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.airschedules.api.utils.FlightUtil;
import com.isa.thinair.airschedules.api.utils.GDSScheduleEventCollector;
import com.isa.thinair.airschedules.api.utils.LegUtil;
import com.isa.thinair.airschedules.api.utils.LocalZuluTimeAdder;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.airschedules.api.utils.ScheduleBuildStatusEnum;
import com.isa.thinair.airschedules.api.utils.SegmentUtil;
import com.isa.thinair.airschedules.core.bl.DefaultAnciTemplateAssignBL;
import com.isa.thinair.airschedules.core.bl.noncmd.FlightsForAffectedPeriod;
import com.isa.thinair.airschedules.core.persistence.dao.FlightDAO;
import com.isa.thinair.airschedules.core.persistence.dao.FlightLegDAO;
import com.isa.thinair.airschedules.core.persistence.dao.FlightScheduleDAO;
import com.isa.thinair.airschedules.core.persistence.dao.FlightScheduleLegDAO;
import com.isa.thinair.airschedules.core.persistence.dao.FlightScheduleSegmentDAO;
import com.isa.thinair.airschedules.core.persistence.dao.FlightSegementDAO;
import com.isa.thinair.airschedules.core.util.CalculationUtil;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.airschedules.core.util.OverlapDetailUtil;
import com.isa.thinair.airschedules.core.util.PeriodsUtil;
import com.isa.thinair.airschedules.core.util.ScheduledFlightUtil;
import com.isa.thinair.airschedules.core.utils.CommandNames;
import com.isa.thinair.commons.api.dto.CabinClassWiseDefaultLogicalCCDetailDTO;
import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Command;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.FrequencyUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for ammend flight schedule
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="ammendSchedule"
 */
public class AmmendSchedule extends DefaultBaseCommand {

	private final Log log = LogFactory.getLog(getClass());

	// Dao's
	private final FlightScheduleDAO flightSchedleDAO;
	private final FlightScheduleSegmentDAO flightScheduleSegmentDAO;
	private final FlightScheduleLegDAO flightScheduleLegDAO;
	private final FlightDAO flightDAO;
	private final FlightSegementDAO flightSegmentDAO;
	private final FlightLegDAO flightLegDAO;

	// BD's
	private final AirportBD airportBD;
	private final AircraftBD aircraftBD;
	private final CommonMasterBD commonMasterBD;
	private final FlightInventoryBD flightInventryBD;
	private final LogicalCabinClassBD logicalCabinClassBD;
	private final SegmentBD segmentBD;

	// helpers
	private final LocalZuluTimeAdder timeAdder;
	private final FlightsForAffectedPeriod fltsForAffectedPeriod;
	private final AirportTerminalUtil terminalUtil;

	private Collection<Integer> updatedFlightSegIds;

	private Collection<FlightSegement> updatedFlightSegs;
	
	private boolean isScheduleMessageProcess;

	/**
	 * constructor of the AmmendSchedule command
	 */
	public AmmendSchedule() {

		// looking up daos
		flightDAO = (FlightDAO) AirSchedulesUtil.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_DAO);
		flightSchedleDAO = (FlightScheduleDAO) AirSchedulesUtil.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_DAO);
		flightSegmentDAO = (FlightSegementDAO) AirSchedulesUtil.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.FLIGHT_SEGMENT_DAO);
		flightLegDAO = (FlightLegDAO) AirSchedulesUtil.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_LEG_DAO);
		flightScheduleSegmentDAO = (FlightScheduleSegmentDAO) AirSchedulesUtil.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_SEGMENT_DAO);
		flightScheduleLegDAO = (FlightScheduleLegDAO) AirSchedulesUtil.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_LEG_DAO);

		// looking up bd's
		airportBD = AirSchedulesUtil.getAirportBD();
		commonMasterBD = AirSchedulesUtil.getCommonMasterBD();
		aircraftBD = AirSchedulesUtil.getAircraftBD();
		flightInventryBD = AirSchedulesUtil.getFlightInventoryBD();
		logicalCabinClassBD = AirSchedulesUtil.getLogicalCabinClassBD();
		segmentBD = AirSchedulesUtil.getSegmentBD();

		// helpers
		timeAdder = new LocalZuluTimeAdder(airportBD);
		fltsForAffectedPeriod = new FlightsForAffectedPeriod(flightDAO);
		terminalUtil = new AirportTerminalUtil(airportBD);

		updatedFlightSegIds = new ArrayList<>();
		updatedFlightSegs = new ArrayList<>();
	}

	/**
	 * execute method of the AmmendSchedule command
	 * 
	 * @throws ModuleException
	 */
	@Override
	@SuppressWarnings("unchecked")
	public ServiceResponce execute() throws ModuleException {

		// getting command params
		FlightSchedule updated = (FlightSchedule) this.getParameter(CommandParamNames.FLIGHT_SCHEDULE);
		Collection<AffectedPeriod> createPeriods = (Collection<AffectedPeriod>) this
				.getParameter(CommandParamNames.CREATE_PERIOD_LIST);
		Collection<AffectedPeriod> updatePeriods = (Collection<AffectedPeriod>) this
				.getParameter(CommandParamNames.UPDATE_PERIOD_LIST);
		Collection<AffectedPeriod> cancelPeriods = (Collection<AffectedPeriod>) this
				.getParameter(CommandParamNames.CANCEL_PERIOD_LIST);
		String userName = (String) this.getParameter(CommandParamNames.USER_NAME);
		UserPrincipal userPrincipal = (UserPrincipal) this.getParameter(CommandParamNames.USER_PRINCIPAL);
		FlightAlertDTO flightAlertDTO = (FlightAlertDTO) this.getParameter(CommandParamNames.FLIGHT_ALERT_DTO);
		// param for overwritting manually changed flights
		Boolean isOverwrite = (Boolean) this.getParameter(CommandParamNames.FLIGHT_OVERWRITE_FLAG);
		Boolean isUpdaingLegs = (Boolean) this.getParameter(CommandParamNames.IS_UPDATING_LEGS);
		Boolean downgradeToAnyModel = (Boolean) this.getParameter(CommandParamNames.FLIGHT_DOWNGRADE_TO_ANY_MODEL);
		int firstLegDayDiff = 0;
		if (this.getParameter(CommandParamNames.FIRST_LEG_DAY_DIFFERENCE) != null) {
			firstLegDayDiff = (Integer) this.getParameter(CommandParamNames.FIRST_LEG_DAY_DIFFERENCE);
		}
		this.isScheduleMessageProcess = getParameter(CommandParamNames.IS_SHEDULE_MESSAGE_PROCESS, Boolean.FALSE, Boolean.class);
		boolean isEnableAttachDefaultAnciTemplate = getParameter(CommandParamNames.IS_ENABLE_ATACH_DEFAULT_ANCI_TEMPL,
				Boolean.FALSE, Boolean.class);

		Collection<FlightReservationAlertDTO> flightReservationAlertDTOList = new ArrayList<FlightReservationAlertDTO>();

		// checking params
		this.checkParams(updated);
		
		//Collecting Flights to Publish 
		Collection<Flight> flightsToPublish = new ArrayList<Flight>();
		
		DefaultServiceResponse responce = new DefaultServiceResponse(true);
		
		Collection<Integer> newFlightIds = new ArrayList<Integer>();
		
		//Skipped aircraft model change for manually changed flight
		Collection<String> skippedFlightDates = new ArrayList<String>();

		// set the depature and arrival airport codes of the schedule
		updated.setDepartureStnCode(LegUtil.getFirstScheduleLeg(updated.getFlightScheduleLegs()).getOrigin());
		updated.setArrivalStnCode(LegUtil.getLastScheduleLeg(updated.getFlightScheduleLegs()).getDestination());

		FlightSchedule existing = flightSchedleDAO.getFlightSchedule(updated.getScheduleId().intValue());
		// if no flight found for the ammending flight schedule
		if (existing == null) {
			throw new ModuleException("airschedules.logic.bl.schedule.noschedule");
		}

		// setting the unchangeble fields of the updated
		updated.setBuildStatusCode(existing.getBuildStatusCode());
		updated.setStatusCode(existing.getStatusCode());

		// yo, validations passed
		// now do the actual operation
		// aircraft model number
		String modelNumber = updated.getModelNumber();
		AircraftModel updatedModel = aircraftBD.getAircraftModel(modelNumber);

		// allocation
		int allocation = CalculationUtil.getAllocation(updatedModel.getAircraftCabinCapacitys());

		boolean modelChanged = (existing.getModelNumber().equals(updated.getModelNumber())) ? false : true;
		boolean legsAddedDeleted = LegUtil.checkLegsAddedDeleted(existing, updated);
		boolean buildcompleted = existing.getBuildStatusCode().equals(ScheduleBuildStatusEnum.BUILT.getCode());

		// checking overlapping situations
		boolean overlapAdded = updated.isOverlapping() && !existing.isOverlapping();
		boolean overlapDeleted = existing.isOverlapping() && !updated.isOverlapping();
		boolean onlyEditedOverlap = updated.isOverlapping() && existing.isOverlapping();

		if (isEnableAttachDefaultAnciTemplate) {
			DefaultAnciTemplateAssignBL.setDefaultAnciTemplatesToUpdatedFltSchdule(existing, updated, modelChanged,
					isScheduleMessageProcess);
		}

		FlightSchedule overlapping = null;
		int millsDiff = 0;
		int dayDiff = 0;
		long startDateDeltaForOlpSched = 0;

		if (overlapAdded || onlyEditedOverlap) {

			overlapping = flightSchedleDAO.getFlightSchedule(updated.getOverlapingScheduleId().intValue());
			// if no flight found for the ammending flight schedule
			if (overlapping == null) {
				throw new ModuleException("airschedules.logic.bl.schedule.noschedule");
			}

			millsDiff = (int) (overlapping.getStartDate().getTime() - existing.getStartDate().getTime());
			dayDiff = CalendarUtil.daysUntil(existing.getStartDate(), overlapping.getStartDate());

		} else if (overlapDeleted) {

			overlapping = flightSchedleDAO.getFlightSchedule(existing.getOverlapingScheduleId().intValue());
			// if no flight found for the ammending flight schedule
			if (overlapping == null) {
				throw new ModuleException("airschedules.logic.bl.schedule.noschedule");
			}

			millsDiff = (int) (overlapping.getStartDate().getTime() - existing.getStartDate().getTime());
			dayDiff = CalendarUtil.daysUntil(existing.getStartDate(), overlapping.getStartDate());
		}

		// if edited overlapped schedule then update that schedule first
		if (onlyEditedOverlap) {

			double fdOverlap = this.getFlightDistance(overlapping.getFlightScheduleLegs()).doubleValue();

			int askOverlapFlt = CalculationUtil.getASKforFlight(allocation, fdOverlap);

			boolean overlapbuild = overlapping.getBuildStatusCode().equals(ScheduleBuildStatusEnum.BUILT.getCode());

			if (!isUpdaingLegs.booleanValue()) {

				// update the overlapping flight
				// find the new frequncy for the overlapping schedule
				Frequency newOverlapFqn = FrequencyUtil.getCopyOfFrequency(updated.getFrequency());
				newOverlapFqn = FrequencyUtil.shiftFrequencyBy(newOverlapFqn, dayDiff);

				// find the new start date for the overlapping flight
				Date newOverlapStartDate = CalendarUtil.getOfssetInMilisecondAddedDate(updated.getStartDate(), millsDiff);

				// find the new stop date for the overlapping flight
				Date newOverlapStopDate = CalendarUtil.getOfssetInMilisecondAddedDate(updated.getStopDate(), millsDiff);

				overlapping.setFrequency(newOverlapFqn);
				overlapping.setStartDate(newOverlapStartDate);
				overlapping.setStopDate(newOverlapStopDate);
			}

			overlapping.setModelNumber(updated.getModelNumber());
			overlapping.setOperationTypeId(updated.getOperationTypeId());

			overlapping.setFlightType(updated.getFlightType());
			overlapping.setRemarks(updated.getRemarks());

			overlapping.setMealTemplateId(updated.getMealTemplateId());
			overlapping.setSeatChargeTemplateId(updated.getSeatChargeTemplateId());
			overlapping.setCodeShareMCFlightSchedules(updated.getCodeShareMCFlightSchedules());
			overlapping.setCsOCCarrierCode(updated.getCsOCCarrierCode());
			overlapping.setCsOCFlightNumber(updated.getCsOCFlightNumber());

			// populate the overlapping from updated
			// [NOTE] this can affect the create/cancel/update logic below
			startDateDeltaForOlpSched = OverlapDetailUtil.updateOverlappingFromUpdated(overlapping, updated, existing);

			// if schedule is already build then add changes to those flights
			if (overlapbuild) {

				if (createPeriods != null && createPeriods.size() > 0) {
					// create flights for create period
					Iterator<AffectedPeriod> itCreatePeriods = createPeriods.iterator();
					while (itCreatePeriods.hasNext()) {

						AffectedPeriod period = itCreatePeriods.next();
						AffectedPeriod overlapPeriod = PeriodsUtil.getPeriodForOverlapping(period, millsDiff, dayDiff);
						// create the flights for period
						this.createOverlappingFlights(overlapping, overlapPeriod, userName, askOverlapFlt, updatedModel,
								flightsToPublish, isScheduleMessageProcess);
					}
				}

				if (cancelPeriods != null && cancelPeriods.size() > 0) {
					// cancel flights for cancel period
					Iterator<AffectedPeriod> itCancelPeriods = cancelPeriods.iterator();
					// Collection cancelFligtDateList = new ArrayList();
					List<Integer> cancelFligtListIDs = new ArrayList<Integer>();
					while (itCancelPeriods.hasNext()) {

						AffectedPeriod period = itCancelPeriods.next();
						AffectedPeriod overlapPeriod = PeriodsUtil.getPeriodForOverlapping(period, millsDiff, dayDiff);
						Collection<Integer> cancelflightIds = fltsForAffectedPeriod.getFlightIdsForCancelPeriod(overlapPeriod,
								updated.getOverlapingScheduleId().intValue(), isOverwrite.booleanValue());
						cancelFligtListIDs.addAll(cancelflightIds);
					}

					// cancel the flights
					this.cancelFlights(overlapping.getScheduleId().intValue(), cancelFligtListIDs, flightAlertDTO,
							flightReservationAlertDTOList,flightsToPublish, isScheduleMessageProcess);
				}

				if (updatePeriods != null && updatePeriods.size() > 0) {
					// update flights for update period
					Iterator<AffectedPeriod> itUpdatedPeriods = updatePeriods.iterator();
					while (itUpdatedPeriods.hasNext()) {

						AffectedPeriod period = itUpdatedPeriods.next();
						AffectedPeriod overlapPeriod = PeriodsUtil.getPeriodForOverlapping(period, millsDiff, dayDiff);
						// this.updateFlights(overlapping, overlapPeriod, updated, existing, askOverlapFlt, userName,
						// updatedModel,aircraftBD.getAircraftModel(existing.getModelNumber()),
						// false, false, false, onlyEditedOverlap, modelChanged, millsDiff, isOverwrite.booleanValue()
						// );
						this.updateOverlappingFlights(overlapping, overlapPeriod, updated, existing, askOverlapFlt, userName,
								updatedModel, aircraftBD.getAircraftModel(existing.getModelNumber()), modelChanged, millsDiff,
								isOverwrite.booleanValue(), startDateDeltaForOlpSched, flightAlertDTO,
								flightReservationAlertDTOList, flightsToPublish, isScheduleMessageProcess);
					}
				}

				// number of flights
				int numberOfFlights = flightDAO.getNumberOfNonCanceledFlights(updated.getOverlapingScheduleId().intValue());
				// int askOverlapShed = CalculationUtil.getASKforSchedule(numberOfFlights, allocation, fdOverlap);
				int askOverlapShed = flightSchedleDAO.getTotalASKForSchedule(updated.getOverlapingScheduleId().intValue());

				overlapping.setNumberOfDepartures(new Integer(numberOfFlights));
				overlapping.setAvailableSeatKilometers(new Integer(askOverlapShed));
			}

			flightSchedleDAO.saveFlightSchedule(overlapping);
		}

		double flightDistance = this.getFlightDistance(updated.getFlightScheduleLegs()).doubleValue();
		int askFlt = CalculationUtil.getASKforFlight(allocation, flightDistance);

		// if schedule is already build then add changes to those flights
		if (buildcompleted) {

			if (createPeriods != null && createPeriods.size() > 0) {
				// create flights for create period
				Iterator<AffectedPeriod> itCreatePeriods = createPeriods.iterator();
				while (itCreatePeriods.hasNext()) {

					AffectedPeriod period = itCreatePeriods.next();
					// create the flights for period
					this.createFlights(updated, period, userName, askFlt, (overlapAdded || onlyEditedOverlap), updatedModel,
							millsDiff, flightsToPublish, newFlightIds, isScheduleMessageProcess);
				}
			}

			if (cancelPeriods != null && cancelPeriods.size() > 0) {
				// cancel flights for cancel period
				Iterator<AffectedPeriod> itCancelPeriods = cancelPeriods.iterator();
				// Collection cancelFligtDateList = new ArrayList();
				List<Integer> cancelFligtListIds = new ArrayList<Integer>();
				while (itCancelPeriods.hasNext()) {

					AffectedPeriod period = itCancelPeriods.next();

					Collection<Integer> cancelflightIds = fltsForAffectedPeriod.getFlightIdsForCancelPeriod(period, updated
							.getScheduleId().intValue(), isOverwrite.booleanValue());
					cancelFligtListIds.addAll(cancelflightIds);
				}

				// cancel the flights
				this.cancelFlights(updated.getScheduleId().intValue(), cancelFligtListIds, flightAlertDTO,
						flightReservationAlertDTOList, flightsToPublish, isScheduleMessageProcess);
			}

			if (updatePeriods != null && updatePeriods.size() > 0) {
				// update flights for update period
				Iterator<AffectedPeriod> itUpdatedPeriods = updatePeriods.iterator();
				while (itUpdatedPeriods.hasNext()) {

					int adjustedMillsDiff = millsDiff - (int) startDateDeltaForOlpSched;

					AffectedPeriod period = itUpdatedPeriods.next();
					this.updateFlights(updated, period, overlapping, existing, askFlt, userName, updatedModel,
							aircraftBD.getAircraftModel(existing.getModelNumber()), legsAddedDeleted, overlapAdded,
							overlapDeleted, onlyEditedOverlap, modelChanged, adjustedMillsDiff, isOverwrite.booleanValue(),
							flightAlertDTO, flightReservationAlertDTOList, downgradeToAnyModel, firstLegDayDiff, flightsToPublish,
							skippedFlightDates, isScheduleMessageProcess, userPrincipal.getUserId());

				}
			}
		}

		// populate the existing from updated
		existing = updateExistingFormUpdated(existing, updated, legsAddedDeleted);

		// add remove overlap details form updated schedule
		if (overlapping != null && (overlapAdded || legsAddedDeleted)) {

			existing = OverlapDetailUtil.addOverlapDetailsToSchedule(existing, overlapping);

		} else if (overlapDeleted) {

			existing = OverlapDetailUtil.removeOverlapDetailsFromSchedule(existing);
		}

		// update the schedule
		if (buildcompleted) {

			// number of flights
			int numberOfFlights = flightDAO.getNumberOfNonCanceledFlights(updated.getScheduleId().intValue());
			// int askShed = CalculationUtil.getASKforSchedule(numberOfFlights, allocation, flightDistance);
			int askShed = flightSchedleDAO.getTotalASKForSchedule(updated.getScheduleId().intValue());

			existing.setNumberOfDepartures(new Integer(numberOfFlights));
			existing.setAvailableSeatKilometers(new Integer(askShed));
		}
		existing.setVersion(updated.getVersion());
		existing.setModifiedBy(updated.getModifiedBy());
		existing.setModifiedDate(new Date(System.currentTimeMillis()));
		//update CodeShare
		existing.setCodeShareMCFlightSchedules(updated.getCodeShareMCFlightSchedules());
		
		flightSchedleDAO.saveFlightSchedule(existing);
		
		reviseScheduleFrequencyPeriodDeleteInvalidSchedules(existing.getScheduleId());

		// add remove overlap details to overlapping schedule
		if (overlapAdded || (overlapping != null && legsAddedDeleted)) {

			overlapping = OverlapDetailUtil.addOverlapDetailsToSchedule(overlapping, existing);
			flightSchedleDAO.saveFlightSchedule(overlapping);

		} else if (overlapDeleted) {

			overlapping = OverlapDetailUtil.removeOverlapDetailsFromSchedule(overlapping);
			flightSchedleDAO.saveFlightSchedule(overlapping);
		}

		if (responce.isSuccess()) {
			
			GDSScheduleEventCollector sGdsEvents = (GDSScheduleEventCollector) this
					.getParameter(CommandParamNames.GDS_PARAM_COLLECTOR);

			if (sGdsEvents != null
					&& (sGdsEvents.containsAction(GDSScheduleEventCollector.PERIOD_CHANGED_LOCAL_TIME) || sGdsEvents
							.containsAction(GDSScheduleEventCollector.LEG_TIME_CHANGE))) {
				Command command = (Command) AirSchedulesUtil.getBean(CommandNames.SPLIT_SUB_SCHEDULE);
				command.setParameter(CommandParamNames.FLIGHT_SCHEDULE, existing);
				command.setParameter(CommandParamNames.IS_SCHEDULE_PERIOD_CHANGE, true);
				
				ServiceResponce serviceResponce = command.execute();

				if (serviceResponce.isSuccess()) {
					Collection<SSMSplitFlightSchedule> existingSubSchedules = (Collection<SSMSplitFlightSchedule>) serviceResponce
							.getResponseParam(CommandParamNames.EXISITNG_SSM_SUB_SCHEDULES);
					Collection<SSMSplitFlightSchedule> updatedSubSchedules = (Collection<SSMSplitFlightSchedule>) serviceResponce
							.getResponseParam(CommandParamNames.NEW_SSM_SUB_SCHEDULES);
					sGdsEvents.setExistingSubSchedules(existingSubSchedules);
					sGdsEvents.setUpdatedSubSchedules(updatedSubSchedules);
					if(!newFlightIds.isEmpty() && sGdsEvents != null){						
						sGdsEvents.setCreatedFlightIds(newFlightIds);
					}
				}
			}
			
			if (!newFlightIds.isEmpty() && sGdsEvents != null) {
				sGdsEvents.setCreatedFlightIds(newFlightIds);
				responce.addResponceParam(CommandParamNames.GDS_PARAM_COLLECTOR, sGdsEvents);
			}
			
			responce.setResponseCode(ResponceCodes.AMMEND_SCHEDULE_SUCCESSFULL);
			responce.addResponceParam(CommandParamNames.FLIGHT_SEGMENT_EMAIL_DTO_COLLECTION, flightReservationAlertDTOList);
			if (overlapAdded || overlapDeleted || onlyEditedOverlap) {
				responce.addResponceParam(CommandParamNames.OVERLAPPING_SCHEDULE, overlapping);
			}
			responce.addResponceParam(ResponceCodes.SKIPPED_FLIGHT_DATES, skippedFlightDates);
			responce.addResponceParam(CommandParamNames.LAST_RESPONSE, responce);
			responce.addResponceParam(CommandParamNames.FLIGHTS_TO_PUBLISH, flightsToPublish);
		}
		return responce;
	}

	/**
	 * privte method to validate parameters
	 * 
	 * @param schedule
	 * @throws ModuleException
	 */
	private void checkParams(FlightSchedule schedule) throws ModuleException {

		if (schedule == null) {
			throw new ModuleException("airschedules.arg.invalid.null");
		}
	}

	/**
	 * private method to creates the flights for create period
	 * 
	 * @param schedule
	 * @param period
	 * @param userName
	 * @param newFlightIds 
	 * @param isScheduleMessageProcess TODO
	 * @throws ModuleException
	 */
	private void createFlights(FlightSchedule schedule, AffectedPeriod period, String userName, int askFlt,
			boolean isOverlapping, AircraftModel model, int milsDiff,Collection<Flight> flightsToPublish, Collection<Integer> newFlightIds, boolean isScheduleMessageProcess) throws ModuleException {

		Collection<Flight> flights = ScheduledFlightUtil.getFlightsForSchdule(schedule, period);
		Map<String, String> defaultLogicalCCs = new HashMap<String, String>();
		for (AircraftCabinCapacity ccCapacity : model.getAircraftCabinCapacitys()) {
			for (CabinClassWiseDefaultLogicalCCDetailDTO defaultLogicalCCDetailDTO : CommonsServices.getGlobalConfig()
					.getDefaultLogicalCCDetails()) {
				if (ccCapacity.getCabinClassCode() != null
						&& ccCapacity.getCabinClassCode().equalsIgnoreCase(defaultLogicalCCDetailDTO.getCcCode())) {
					defaultLogicalCCs.put(ccCapacity.getCabinClassCode(), defaultLogicalCCDetailDTO.getDefaultLogicalCCCode());
					break;
				}
			}
		}

		Iterator<Flight> fit = flights.iterator();
		while (fit.hasNext()) {

			Flight flight = fit.next();
			if (!isSkipFlightUpdate(isScheduleMessageProcess, flight.getDepartureDate())) {
				// set the fields should set @ build
				flight.setAvailableSeatKilometers(new Integer(askFlt));
				flight.setCreatedBy(userName);
				flight.setStatus(FlightStatusEnum.CREATED.getCode());

				// prepare segment details of the flight
				flight = timeAdder.addOnlySegementTimeDetails(flight);

				// set the terminal ids for each segment of the flight.
				flight = terminalUtil.addTerminalInformation(flight);

				// set overlap details
				Integer overlapId = null;
				Collection<FlightSegement> overlapSegments = null;
				Flight overlapFlight = null;

				// if overlapp added to the updated schedule
				if (isOverlapping) {

					int overlapSchedId = schedule.getOverlapingScheduleId().intValue();

					Date overlapDate = CalendarUtil.getOfssetInMilisecondAddedDate(flight.getDepartureDate(), milsDiff);
					overlapFlight = flightDAO.getOverlappingFlight(overlapSchedId, overlapDate);

					if (overlapFlight != null) {

						overlapId = overlapFlight.getFlightId();
						overlapSegments = overlapFlight.getFlightSegements();

						flight = OverlapDetailUtil.addOverlapDetailsToFlight(flight, overlapFlight);
					}
				}

				// save the flight
				flight = timeAdder.addLocalTimeDetailsToFlight(flight);
				flightDAO.saveFlight(flight);

				// Add the flights to the list which holds the publish flight list
				flightsToPublish.add(flight);

				// save the overlapping flight
				if (isOverlapping && overlapFlight != null) {

					overlapFlight = OverlapDetailUtil.addOverlapDetailsToFlight(overlapFlight, flight);
					overlapFlight = timeAdder.addLocalTimeDetailsToFlight(overlapFlight);
					flightDAO.saveFlight(overlapFlight);
				}

				// create inventory for flight and segments
				int flightID = flight.getFlightId().intValue();
				newFlightIds.add(flightID);
				Collection<SegmentDTO> intersepSegList = SegmentUtil.createSegmentDTOList(flightID, flight.getFlightSegements(),
						overlapId, overlapSegments);

				CreateFlightInventoryRQ createFlightInventoryRQ = new CreateFlightInventoryRQ();
				createFlightInventoryRQ.setFlightId(flightID);
				createFlightInventoryRQ.setAircraftModel(model);
				createFlightInventoryRQ.setSegmentDTOs(intersepSegList);
				createFlightInventoryRQ.setOverlappingFlightId(overlapId);
				createFlightInventoryRQ.setDefaultLogicalCCs(defaultLogicalCCs);

				flightInventryBD.createFlightInventory(createFlightInventoryRQ);
			}
		}
	}

	/**
	 * private method to creates overlapping flights for create period
	 * 
	 * @param schedule
	 * @param period
	 * @param userName
	 * @param isSCheduleMessageProcess TODO
	 * @throws ModuleException
	 */
	private void createOverlappingFlights(FlightSchedule schedule, AffectedPeriod period, String userName, int askFlt,
			AircraftModel model,Collection<Flight> flightsToPublish, boolean isSCheduleMessageProcess) throws ModuleException {

		Collection<Flight> flights = ScheduledFlightUtil.getFlightsForSchdule(schedule, period);
		Map<String, String> defaultLogicalCCs = new HashMap<String, String>();
		for (AircraftCabinCapacity ccCapacity : model.getAircraftCabinCapacitys()) {
			for (CabinClassWiseDefaultLogicalCCDetailDTO defaultLogicalCCDetailDTO : CommonsServices.getGlobalConfig()
					.getDefaultLogicalCCDetails()) {
				if (ccCapacity.getCabinClassCode() != null
						&& ccCapacity.getCabinClassCode().equalsIgnoreCase(defaultLogicalCCDetailDTO.getCcCode())) {
					defaultLogicalCCs.put(ccCapacity.getCabinClassCode(), defaultLogicalCCDetailDTO.getDefaultLogicalCCCode());
					break;
				}
			}
		}

		Iterator<Flight> fit = flights.iterator();
		while (fit.hasNext()) {

			Flight flight = fit.next();
			if (!isSkipFlightUpdate(isSCheduleMessageProcess, flight.getDepartureDate())) {
				// set the fields should set @ build
				flight.setAvailableSeatKilometers(new Integer(askFlt));
				flight.setCreatedBy(userName);
				flight.setStatus(FlightStatusEnum.CREATED.getCode());

				// prepare segment details of the flight
				flight = timeAdder.addOnlySegementTimeDetails(flight);

				// set the terminal ids for each segment of the flight.
				flight = terminalUtil.addTerminalInformation(flight);

				// save the flight
				flight = timeAdder.addLocalTimeDetailsToFlight(flight);
				flightDAO.saveFlight(flight);

				// Add the flights to the list which holds the publish flight list
				flightsToPublish.add(flight);

				// create inventory for flight and segments
				int flightID = flight.getFlightId().intValue();
				Collection<SegmentDTO> intersepSegList = SegmentUtil.createSegmentDTOList(flightID, flight.getFlightSegements(),
						null, null);

				CreateFlightInventoryRQ createFlightInventoryRQ = new CreateFlightInventoryRQ();
				createFlightInventoryRQ.setFlightId(flightID);
				createFlightInventoryRQ.setAircraftModel(model);
				createFlightInventoryRQ.setSegmentDTOs(intersepSegList);
				createFlightInventoryRQ.setOverlappingFlightId(null);
				createFlightInventoryRQ.setDefaultLogicalCCs(defaultLogicalCCs);

				flightInventryBD.createFlightInventory(createFlightInventoryRQ);
			}
		}
	}

	/**
	 * private method to cancel the flights for given dates
	 * @param isScheduleMessageProcess TODO
	 * @param schedule
	 * @param period
	 * 
	 * @throws ModuleException
	 */
	// modifications: [2nd Arg] flyDates -> cancelFlights
	private void cancelFlights(int scheduleId, List<Integer> flightIds, FlightAlertDTO flightAlertDTO,
			Collection<FlightReservationAlertDTO> flightReservationAlertDTOList,Collection<Flight> flightsToPublish, boolean isScheduleMessageProcess) throws ModuleException {

		if (flightIds.size() == 0) {
			return;
		}

		// flightDAO.batchUpdateFlightStatus(flightIds, FlightStatusEnum.CANCELLED.getCode());
		/*
		 * not required to invalidate inventories for cancelled flights [NOTES] if called, causes invalidating all
		 * segments this violates following inventory module assumption: -- at least on valid segment should exist
		 */
		// flightInventryBD.invalidateFlightInventories(flightIds);
		List<Integer> unFlownFlightIds = new ArrayList<Integer>();
		List<Integer> flightSegmentIds = new ArrayList<Integer>();
		Collection<Flight> flights = new ArrayList<Flight>();
		Iterator<Integer> itFlightId = flightIds.iterator();
		while (itFlightId.hasNext()) {
			Integer flightId = itFlightId.next();
			Flight flight = flightDAO.getFlight(flightId.intValue());
			if (!isSkipFlightUpdate(isScheduleMessageProcess, flight.getDepartureDate())) {
				flight = timeAdder.addLocalTimeDetailsToFlight(flight);
				flights.add(flight);
				flightSegmentIds.addAll(SegmentUtil.getFlightSegIds(flight.getFlightSegements()));
				unFlownFlightIds.add(flightId);
			}
		}

		flightDAO.batchUpdateFlightStatus((isScheduleMessageProcess) ? unFlownFlightIds : flightIds,
				FlightStatusEnum.CANCELLED.getCode());

		// [AirReservation integration] cancel reservations
		// credit the payment to pax
		// cancellation charge for fligth cancellation is ZERO
		// Double cancelCharge = new Double(0.0);
		// ip address is not tracked - used only for ibe
		// segmentBD.cancelSegments(flightSegmentIds, cancelCharge, null);

		flightReservationAlertDTOList.addAll(AlertUtil.prepareFlightAlertInfo(flights));
		
		//Add the flights to the list which holds the publish flight list
		flightsToPublish.addAll(flights);
	}

	/**
	 * private method to get the updated flights
	 * 
	 * @param period
	 * @param existing
	 * @param firstLegDayDiff
	 *            TODO
	 * @param isScheduleMessageProcess
	 *            TODO
	 * @param string
	 * @param updated
	 * 
	 * @throws ModuleException
	 *             [NOTES] call to update inventory is required here passing both flightIds and overlapping flightIds
	 *             omit the invenotry update call when updating overlapping flights
	 */
	private void updateFlights(FlightSchedule schedule, AffectedPeriod period, FlightSchedule overlapping,
			FlightSchedule existing, int askFlt, String userName, AircraftModel updatedScheduleModel,
			AircraftModel existingScheduleModel, boolean legsaddedDeleted, boolean overlapAdded, boolean overlapDeleted,
			boolean overlapEdited, boolean modelChanged, int milsDiff, boolean isOverwrite, FlightAlertDTO flightAlertDTO,
			Collection<FlightReservationAlertDTO> flightReservationAlertDTOList, boolean downgradeToAnyModel,
			int firstLegDayDiff, Collection<Flight> flightsToPublish, Collection<String> skippedFlightDates,
			boolean isScheduleMessageProcess, String userID) throws ModuleException {

		Collection<Flight> updationFlights = fltsForAffectedPeriod.getFlightsForUpdatePeriod(period, schedule.getScheduleId()
				.intValue(), isOverwrite);

		// -------------for use by inventory for cabinclass update
		boolean newCabinClassesAdded = ScheduledFlightUtil.isNewCabinClassesAdded(updatedScheduleModel, existingScheduleModel);
		Collection<SegmentDTO> segmentDTOsCollection = null;
		Collection<Integer> olFlightIds = new ArrayList<Integer>();
		Collection<Integer> flightIds = new ArrayList<Integer>();
		if (newCabinClassesAdded) {
			segmentDTOsCollection = new ArrayList<SegmentDTO>();
			// --------------------------------------------------------
		}

		Iterator<Flight> it = updationFlights.iterator();
		while (it.hasNext()) {

			Flight flight = it.next();
			if (!isSkipFlightUpdate(isScheduleMessageProcess, flight.getDepartureDate())) {
				// prepare alert/email DTO
				Flight oldFlight = null;
				if (AlertUtil.shouldRescheduleNotified(flightAlertDTO)) {

					oldFlight = FlightUtil.getCopyOfFlight(flight);
					oldFlight = timeAdder.addLocalTimeDetailsToFlight(oldFlight);
				}

				// set overlap details
				Flight overlapFlight = null;

				// get aircraft model
				String exisingModelNumber = flight.getModelNumber();

				if (overlapping != null) {

					int overlapSchedId = overlapping.getScheduleId().intValue();
					Date overlapDate = CalendarUtil.getOfssetInMilisecondAddedDate(flight.getDepartureDate(), milsDiff);
					overlapFlight = flightDAO.getOverlappingFlight(overlapSchedId, overlapDate);
				}

				flight = this.getUpdatedFlight(flight, overlapFlight, schedule, schedule, existing, legsaddedDeleted, false,
						userName, askFlt, firstLegDayDiff);

				if (overlapFlight != null && (overlapAdded || overlapDeleted)) {

					if (overlapAdded) {

						flight = OverlapDetailUtil.addOverlapDetailsToFlight(flight, overlapFlight);

					} else if (overlapDeleted) {

						flight = OverlapDetailUtil.removeOverlapDetailsFromFlight(flight);
					}
				}

				flight = timeAdder.addLocalTimeDetailsToFlight(flight);
				flightDAO.saveFlight(flight);

				// Add the flights to the list which holds the publish flight list
				flightsToPublish.add(flight);

				// save the overlapping flight
				if (overlapFlight != null && (overlapAdded || overlapDeleted || legsaddedDeleted)) {

					if (overlapAdded || legsaddedDeleted) {

						overlapFlight = OverlapDetailUtil.addOverlapDetailsToFlight(overlapFlight, flight);
						overlapFlight = timeAdder.addLocalTimeDetailsToFlight(overlapFlight);
						flightDAO.saveFlight(overlapFlight);
						// TODO: add inventory overlap -- INVENTORY does not support this operation yet!

					} else if (overlapDeleted) {

						overlapFlight = OverlapDetailUtil.removeOverlapDetailsFromFlight(overlapFlight);
						overlapFlight = timeAdder.addLocalTimeDetailsToFlight(overlapFlight);
						flightDAO.saveFlight(overlapFlight);

						// [TODO:test]remove inventory overlap
						Collection<Integer> fltIds = new ArrayList<Integer>();
						fltIds.add(flight.getFlightId());
						fltIds.add(overlapFlight.getFlightId());
						flightInventryBD.removeInventoryOverlap(fltIds);
					}
				}

				// Add the flights to the list which holds the publish flight list
				// From above If and else, any way overlapFlight will be updated, so adding it after the whole operation
				// is
				// finished.
				if (overlapFlight != null) {
					flightsToPublish.add(overlapFlight);
				}

				// ----------for use by inventory for cabinclass update-----------------------
				// create flight inventory
				Flight olFlight = null;
				if (overlapAdded || overlapEdited) {
					olFlight = overlapFlight;
				}

				if (legsaddedDeleted) {
					Collection<SegmentDTO> intersepSegList = SegmentUtil.createSegmentDTOList(flight.getFlightId().intValue(),
							flight.getFlightSegements(), olFlight != null ? olFlight.getFlightId() : null, olFlight != null
									? olFlight.getFlightSegements()
									: null);

					AircraftModel model = aircraftBD.getAircraftModel(flight.getModelNumber());
					CreateFlightInventoryRQ createFlightInventoryRQ = new CreateFlightInventoryRQ();
					createFlightInventoryRQ.setFlightId(flight.getFlightId());
					createFlightInventoryRQ.setAircraftModel(model);
					createFlightInventoryRQ.setSegmentDTOs(intersepSegList);
					createFlightInventoryRQ.setOverlappingFlightId(olFlight != null ? olFlight.getFlightId() : null);
					Map<String, String> defaultLogicalCCs = new HashMap<String, String>();
					for (AircraftCabinCapacity ccCapacity : model.getAircraftCabinCapacitys()) {
						for (CabinClassWiseDefaultLogicalCCDetailDTO defaultLogicalCCDetailDTO : CommonsServices
								.getGlobalConfig().getDefaultLogicalCCDetails()) {
							if (ccCapacity.getCabinClassCode() != null
									&& ccCapacity.getCabinClassCode().equalsIgnoreCase(defaultLogicalCCDetailDTO.getCcCode())) {
								defaultLogicalCCs.put(ccCapacity.getCabinClassCode(),
										defaultLogicalCCDetailDTO.getDefaultLogicalCCCode());
								break;
							}
						}
					}
					createFlightInventoryRQ.setDefaultLogicalCCs(defaultLogicalCCs);

					flightInventryBD.createFlightInventory(createFlightInventoryRQ);
				}
				// if model change fill the following inventory prams
				// flightIds, olFlightIds, segmentDTOsCollection
				if (modelChanged) {
					this.prepareModelChangeInventoryParams(flightIds, olFlightIds, segmentDTOsCollection, flight, olFlight,
							newCabinClassesAdded, existingScheduleModel, exisingModelNumber, skippedFlightDates);
				}

				// Do reservation audit for schedule ammend schedule legs
				segmentBD.auditFlightTimeChanges(updatedFlightSegIds, updatedFlightSegs, flight.getFlightNumber(), userID);

				updatedFlightSegIds.clear();
				updatedFlightSegs.clear();
				// prepare alert/email DTO
				if (AlertUtil.shouldRescheduleNotified(flightAlertDTO)) {

					Flight newFlight = FlightUtil.getCopyOfFlight(flight);
					newFlight = timeAdder.addLocalTimeDetailsToFlight(newFlight);

					FlightReservationAlertDTO resAlert = AlertUtil.prepareFlightAlertInfo(oldFlight, newFlight);
					// add only updated flights
					if (resAlert != null) {
						flightReservationAlertDTOList.add(resAlert);
					}
				}
			}

		}
		if (modelChanged) {
			String flightSegmentCode = "";

			Iterator<Flight> tmpFlightIte = updationFlights.iterator();
			if (tmpFlightIte.hasNext()) {
				Flight tmpFlight = tmpFlightIte.next();
				FlightSegement tmpSegment = SegmentUtil.getDetailsOfLongestSegment(tmpFlight.getFlightSegements());
				flightSegmentCode = tmpSegment.getSegmentCode();
			}

			flightInventryBD.updateFCCInventoriesForModelChange(flightIds, updatedScheduleModel, existingScheduleModel,
					olFlightIds, segmentDTOsCollection, flightSegmentCode, downgradeToAnyModel);
		}
	}

	/**
	 * private method to get the updated flights
	 * @param period
	 * @param updated
	 * @param existing
	 * @param isScheduleMessageProcess TODO
	 * 
	 * @throws ModuleException
	 *             [NOTES] allocating inventory should be done when updating the flights (not here when updating the
	 *             overlapping flights) inventory module accepts both flights and overlapping flights in a single call
	 *             and updates inventory of both
	 */
	private void updateOverlappingFlights(FlightSchedule overlapping, AffectedPeriod period, FlightSchedule updated,
			FlightSchedule existing, int askFlt, String userName, AircraftModel updatedScheduleModel,
			AircraftModel existingScheduleModel, boolean modelChanged, int milsDiff, boolean isOverwrite,
			long startDateDeltaForOlpSched, FlightAlertDTO flightAlertDTO,
			Collection<FlightReservationAlertDTO> flightReservationAlertDTOList,Collection<Flight> flightsToPublish, boolean isScheduleMessageProcess) throws ModuleException {

		Collection<Flight> updationFlights = fltsForAffectedPeriod.getFlightsForUpdatePeriod(period, overlapping.getScheduleId()
				.intValue(), isOverwrite);

		Iterator<Flight> it = updationFlights.iterator();
		while (it.hasNext()) {

			Flight overlappingFlight = it.next();
			if (!isSkipFlightUpdate(isScheduleMessageProcess, overlappingFlight.getDepartureDate())) {
				// prepare alert/email DTO
				Flight oldOvlFlight = null;
				if ((flightAlertDTO != null)
						&& (flightAlertDTO.isEmailForRescheduledFlight() || flightAlertDTO.isAlertForRescheduledFlight())) {

					oldOvlFlight = FlightUtil.getCopyOfFlight(overlappingFlight);
					oldOvlFlight = timeAdder.addLocalTimeDetailsToFlight(oldOvlFlight);
				}

				overlappingFlight = getUpdatedOverlappingFlight(overlappingFlight, overlapping, userName, askFlt,
					startDateDeltaForOlpSched, existing);

				// prepare alert/email DTO
				if ((flightAlertDTO != null)
						&& (flightAlertDTO.isEmailForRescheduledFlight() || flightAlertDTO.isAlertForRescheduledFlight())) {

					Flight newOvlFlight = FlightUtil.getCopyOfFlight(overlappingFlight);
					newOvlFlight = timeAdder.addLocalTimeDetailsToFlight(newOvlFlight);

					FlightReservationAlertDTO resAlert = AlertUtil.prepareFlightAlertInfo(oldOvlFlight, newOvlFlight);
					if (resAlert != null) {
						flightReservationAlertDTOList.add(resAlert);
					}
				}

				overlappingFlight = timeAdder.addLocalTimeDetailsToFlight(overlappingFlight);
				flightDAO.saveFlight(overlappingFlight);

				// Add the flights to the list which holds the publish flight list : Added because local time might get
				// updated from
				// getUpdatedOverlappingFlight
				flightsToPublish.add(overlappingFlight);
			}
		}
	}

	/**
	 * private methid to fill inventory params from the flight/overlappign flight passed
	 * 
	 * @param flightIds
	 * @param olFlightIds
	 * @param segmentDTOsCollection
	 * @param flight
	 * @param olFlight
	 * @param newCabinClassesAdded
	 * @param existingScheduleModel
	 * @param exisingModelNumber
	 * @param skippedFlightDates 
	 */
	private void prepareModelChangeInventoryParams(Collection<Integer> flightIds, Collection<Integer> olFlightIds,
			Collection<SegmentDTO> segmentDTOsCollection, Flight flight, Flight olFlight, boolean newCabinClassesAdded,
			AircraftModel existingScheduleModel, String exisingModelNumber, Collection<String> skippedFlightDates) {
		SimpleDateFormat smdt = new SimpleDateFormat("dd/MM/yyyy");
		// check for manually changed aircraft model in flight
		if (existingScheduleModel.getModelNumber().equals(exisingModelNumber)) {

			flightIds.add(flight.getFlightId());
			if (olFlight != null) {
				olFlightIds.add(olFlight.getFlightId());
			}

			if (newCabinClassesAdded) {
				segmentDTOsCollection.addAll(SegmentUtil.createSegmentDTOList(flight.getFlightId().intValue(),
						flight.getFlightSegements(), null, null));
			}

			// for updating overlapping flight
			if (olFlight != null) {
				flightIds.add(olFlight.getFlightId());
				if (newCabinClassesAdded) {
					olFlightIds.add(flight.getFlightId());
					segmentDTOsCollection.addAll(SegmentUtil.createSegmentDTOList(olFlight.getFlightId().intValue(),
							olFlight.getFlightSegements(), flight.getFlightId(), flight.getFlightSegements()));
				}
			}
		} else {
			// [TODO:implement]apply new model to the manually changed flights, depeding on caller's preference
			log.info("Skipping aircraft model change for manually changed flight [flightId=" + flight.getFlightId() + "]");
			skippedFlightDates.add(smdt.format(flight.getDepartureDate()));
		}
	}

	/**
	 * private method to get the updated flight
	 * 
	 * @param flight
	 * @param schedule
	 * @param legsAddedDeleted
	 * @param userName
	 * @param firstLegDayDiff
	 * @return flight
	 * @throws ModuleException
	 */
	private Flight getUpdatedFlight(Flight flight, Flight overlapFlight, FlightSchedule schedule, FlightSchedule updated,
			FlightSchedule existing, boolean legsAddedDeleted, boolean isOverlappingFlight, String userName, int askFlt,
			int firstLegDayDiff)
			throws ModuleException {

		flight.setDestinationAptCode(schedule.getArrivalStnCode());
		flight.setFlightNumber(schedule.getFlightNumber());
		// TODO remove check when inventory supports updating model changes for manually changed flights
		if (!(flight.getManuallyChanged() && !existing.getModelNumber().equals(flight.getModelNumber()))) {
			flight.setModelNumber(schedule.getModelNumber());
		}
		flight.setModifiedBy(userName);
		flight.setModifiedDate(new Date(System.currentTimeMillis()));
		flight.setOperationTypeId(schedule.getOperationTypeId());
		flight.setOriginAptCode(schedule.getDepartureStnCode());
		flight.setAvailableSeatKilometers(new Integer(askFlt));
		flight.setFlightType(schedule.getFlightType());
		flight.setRemarks(schedule.getRemarks());
		flight.setMealTemplateId(schedule.getMealTemplateId());
		flight.setSeatChargeTemplateId(schedule.getSeatChargeTemplateId());
		flight.setCsOCCarrierCode(schedule.getCsOCCarrierCode());
		flight.setCsOCFlightNumber(schedule.getCsOCFlightNumber());

		Set<CodeShareMCFlight> codeShareMCFlights = new HashSet<CodeShareMCFlight>();
		for (CodeShareMCFlightSchedule codeShareMCFlightSchedule : schedule.getCodeShareMCFlightSchedules()) {

			CodeShareMCFlight codeShareMCFlight = new CodeShareMCFlight();
			codeShareMCFlight.setCsMCFlightNumber(codeShareMCFlightSchedule.getCsMCFlightNumber());
			codeShareMCFlight.setCsMCCarrierCode(codeShareMCFlightSchedule.getCsMCCarrierCode());
			codeShareMCFlights.add(codeShareMCFlight);
		}
		flight.setCodeShareMCFlights(codeShareMCFlights);

	// If a date shift involved with modification, setting the new date.
		if (firstLegDayDiff != 0) {
			flight.setDepartureDate(CalendarUtil.addDateVarience(flight.getDepartureDate(), firstLegDayDiff));
			flight.setDayNumber(CalendarUtil.getDayNumber(flight.getDepartureDate()));
		}

		// if legs not added deleted then update the existing legs
		if (!legsAddedDeleted) {

			// update flight legs
			Set<FlightLeg> flightLegs = flight.getFlightLegs();

			Set<FlightScheduleLeg> scheduleLegs = schedule.getFlightScheduleLegs();
			Iterator<FlightScheduleLeg> slit = scheduleLegs.iterator();

			while (slit.hasNext()) {

				FlightScheduleLeg fsleg = slit.next();

				Iterator<FlightLeg> fit = flightLegs.iterator();
				while (fit.hasNext()) {

					FlightLeg fleg = fit.next();
					if (fleg.getLegNumber() == fsleg.getLegNumber()) {

						this.updateFlightLeg(fleg, fsleg);
						break;
					}
				}
			}

			// update flight segments
			Set<FlightSegement> flightSegments = flight.getFlightSegements();

			Set<FlightScheduleSegment> scheduleSegments = schedule.getFlightScheduleSegments();
			Iterator<FlightScheduleSegment> ssit = scheduleSegments.iterator();

			while (ssit.hasNext()) {

				FlightScheduleSegment fsSeg = ssit.next();
				Iterator<FlightSegement> fSegIt = flightSegments.iterator();

				while (fSegIt.hasNext()) {

					FlightSegement fSeg = fSegIt.next();
					if (fsSeg.getSegmentCode().equals(fSeg.getSegmentCode())) {
						this.updatedFlightSegIds.add(fSeg.getFltSegId());
						this.updatedFlightSegs.add(fSeg);
						this.updateFlighSegmet(fSeg, fsSeg);
						break;
					}
				}
			}

			// prepare segment details of the flight
			flight = timeAdder.addOnlySegementTimeDetails(flight);

			// set terminal information for the flight
			flight = terminalUtil.addTerminalInformation(flight);

			// else (legs added/deleted) then create new legs for the schedule
		} else {
			Collection<Integer> flightIds = new ArrayList<Integer>();
			flightIds.add(flight.getFlightId());

			// if overlap exists
			if (existing.isOverlapping()) {
				flightIds.add(flight.getOverlapingFlightId());

				// remove overlap before adding/deleting legs
				overlapFlight = OverlapDetailUtil.removeOverlapDetailsFromFlight(overlapFlight);
				overlapFlight = timeAdder.addLocalTimeDetailsToFlight(overlapFlight);
				flightDAO.saveFlight(overlapFlight);
				// remove overlap info from flight inventories
				flightInventryBD.removeInventoryOverlap(flightIds);

				flightIds.remove(flight.getOverlapingFlightId());
			}

			// Remove segment inventories
			flightInventryBD.deleteFlightInventories(flightIds);

			Set<FlightLeg> oldLegs = flight.getFlightLegs();
			Iterator<FlightLeg> itOldLegs = oldLegs.iterator();
			while (itOldLegs.hasNext()) {
				FlightLeg oldLeg = itOldLegs.next();
				flightLegDAO.removeFlightLeg(oldLeg.getId().intValue());
			}

			Set<FlightSegement> oldSegments = flight.getFlightSegements();
			Iterator<FlightSegement> itOldsegments = oldSegments.iterator();
			while (itOldsegments.hasNext()) {
				FlightSegement oldSegment = itOldsegments.next();
				flightSegmentDAO.removeFlightSegement(oldSegment.getFltSegId().intValue());
			}

			// create flight legs for flight
			flight.setFlightLegs(LegUtil.createFlightLegs(schedule.getFlightScheduleLegs()));
			// create flight segments for flight
			flight.setFlightSegements(SegmentUtil.createFlightSegments(schedule.getFlightScheduleSegments()));

			// prepare segment details of the flight
			flight = timeAdder.addOnlySegementTimeDetails(flight);

			// set terminal information for the flight
			flight = terminalUtil.addTerminalInformation(flight);

			// if overlapp exists
			if (existing.isOverlapping()) {
				// add overlapping flight details to both flights
				flight = OverlapDetailUtil.addOverlapDetailsToFlight(flight, overlapFlight);
				flight = timeAdder.addLocalTimeDetailsToFlight(flight);
				flightDAO.saveFlight(flight);
			}
		}

		// update flight departure time with first leg departure time
		FlightLeg firstLeg = LegUtil.getFirstFlightLeg(flight.getFlightLegs());
		flight.setDepartureDate(CalendarUtil.getTimeAddedDate(flight.getDepartureDate(), firstLeg.getEstDepartureTimeZulu()));

		return flight;
	}

	private Flight getUpdatedOverlappingFlight(Flight overlappingFlight, FlightSchedule overlapping, String userName, int askFlt,
			long startDateDeltaForOlpSched, FlightSchedule existing) throws ModuleException {

		// TODO remove check when inventory supports updating model changes for manually changed flights
		if (!(overlappingFlight.getManuallyChanged() && !existing.getModelNumber().equals(overlappingFlight.getModelNumber()))) {
			overlappingFlight.setModelNumber(overlapping.getModelNumber());
		}
		overlappingFlight.setModifiedBy(userName);
		overlappingFlight.setModifiedDate(new Date(System.currentTimeMillis()));
		overlappingFlight.setOperationTypeId(overlapping.getOperationTypeId());
		overlappingFlight.setAvailableSeatKilometers(new Integer(askFlt));
		overlappingFlight.setFlightType(overlapping.getFlightType());
		overlappingFlight.setRemarks(overlapping.getRemarks());
		overlappingFlight.setMealTemplateId(overlapping.getMealTemplateId());
		overlappingFlight.setSeatChargeTemplateId(overlapping.getSeatChargeTemplateId());
		overlappingFlight.setCsOCCarrierCode(overlapping.getCsOCCarrierCode());
		overlappingFlight.setCsOCFlightNumber(overlapping.getCsOCFlightNumber());
		
		Set<CodeShareMCFlight> codeShareMCFlights = new HashSet<CodeShareMCFlight>();
		for (CodeShareMCFlightSchedule codeShareMCFlightSchedule : overlapping.getCodeShareMCFlightSchedules()) {

			CodeShareMCFlight codeShareMCFlight = new CodeShareMCFlight();
			codeShareMCFlight.setCsMCFlightNumber(codeShareMCFlightSchedule.getCsMCFlightNumber());
			codeShareMCFlight.setCsMCCarrierCode(codeShareMCFlightSchedule.getCsMCCarrierCode());
			codeShareMCFlights.add(codeShareMCFlight);
		}
		overlappingFlight.setCodeShareMCFlights(codeShareMCFlights);


		// update flight departure time with first leg departure time
		overlappingFlight.setDepartureDate(CalendarUtil.getOfssetInMilisecondAddedDate(overlappingFlight.getDepartureDate(),
				(int) startDateDeltaForOlpSched));

		// update flight's depature date and the day number

		// update flight legs
		Set<FlightLeg> flightLegs = overlappingFlight.getFlightLegs();

		Set<FlightScheduleLeg> scheduleLegs = overlapping.getFlightScheduleLegs();
		Iterator<FlightScheduleLeg> slit = scheduleLegs.iterator();

		while (slit.hasNext()) {

			FlightScheduleLeg fsleg = slit.next();

			Iterator<FlightLeg> fit = flightLegs.iterator();
			while (fit.hasNext()) {

				FlightLeg fleg = fit.next();
				if (fleg.getLegNumber() == fsleg.getLegNumber()) {

					this.updateFlightLeg(fleg, fsleg);
					break;
				}
			}
		}

		// update flight segments
		Set<FlightSegement> flightSegments = overlappingFlight.getFlightSegements();

		Set<FlightScheduleSegment> scheduleSegments = overlapping.getFlightScheduleSegments();
		Iterator<FlightScheduleSegment> ssit = scheduleSegments.iterator();

		while (ssit.hasNext()) {

			FlightScheduleSegment fsSeg = ssit.next();
			Iterator<FlightSegement> fSegIt = flightSegments.iterator();

			while (fSegIt.hasNext()) {

				FlightSegement fSeg = fSegIt.next();
				if (fsSeg.getSegmentCode().equals(fSeg.getSegmentCode())) {

					this.updateFlighSegmet(fSeg, fsSeg);
					break;
				}
			}
		}

		// prepare segment details of the flight
		overlappingFlight = timeAdder.addOnlySegementTimeDetails(overlappingFlight);

		// set terminal information to the flight
		overlappingFlight = terminalUtil.addTerminalInformation(overlappingFlight);

		return overlappingFlight;
	}

	/**
	 * private method to update the flight leg
	 * 
	 * @param fl
	 * @param fsl
	 * @return flightLeg
	 */
	private FlightLeg updateFlightLeg(FlightLeg fl, FlightScheduleLeg fsl) {

		fl.setEstArrivalDayOffset(fsl.getEstArrivalDayOffset());
		fl.setEstArrivalTimeLocal(fsl.getEstArrivalTimeLocal());
		fl.setEstArrivalTimeZulu(fsl.getEstArrivalTimeZulu());
		fl.setEstDepartureDayOffset(fsl.getEstDepartureDayOffset());
		fl.setEstDepartureTimeLocal(fsl.getEstDepartureTimeLocal());
		fl.setEstDepartureTimeZulu(fsl.getEstDepartureTimeZulu());
		fl.setModelRouteId(fsl.getModelRouteId());

		return fl;
	}

	/**
	 * private method to update the flight segment
	 * 
	 * @param fs
	 * @param fss
	 * @return flightSegment
	 * @throws ModuleException
	 */
	private FlightSegement updateFlighSegmet(FlightSegement fs, FlightScheduleSegment fss) throws ModuleException {

		// [TODO:enhance] do invalidate/validate operation only once for a schedule
		if (fs.getValidFlag() && !fss.getValidFlag()) {
			Collection<Integer> fltSegId = new ArrayList<Integer>();
			fltSegId.add(fs.getFltSegId());
			flightInventryBD.invalidateFlightSegmentInventories(fltSegId);
		} else if (!fs.getValidFlag() && fss.getValidFlag()) {
			Collection<Integer> fltSegId = new ArrayList<Integer>();
			fltSegId.add(fs.getFltSegId());
			flightInventryBD.makeInvalidFltSegInvsValid(fltSegId);
		}
		fs.setValidFlag(fss.getValidFlag());
		fs.setDepartureTerminalId(fss.getDepartureTerminalId());
		fs.setArrivalTerminalId(fss.getArrivalTerminalId());
		return fs;
	}

	/**
	 * private method to get the updated existing form the updated
	 * 
	 * @param exising
	 * @param updated
	 * @return updatedExisting
	 */
	private FlightSchedule updateExistingFormUpdated(FlightSchedule exising, FlightSchedule updated, boolean legsAddedDeleted) {

		exising = ScheduledFlightUtil.copyOnlyScheduleAttributes(updated, exising, false);

		// if legs not added deleted then update the existing legs
		if (!legsAddedDeleted) {

			// update flight schedule legs
			Set<FlightScheduleLeg> existingLegs = exising.getFlightScheduleLegs();
			Set<FlightScheduleLeg> updatedLegs = updated.getFlightScheduleLegs();
			Iterator<FlightScheduleLeg> uit = updatedLegs.iterator();

			while (uit.hasNext()) {

				FlightScheduleLeg uleg = uit.next();

				Iterator<FlightScheduleLeg> eit = existingLegs.iterator();
				while (eit.hasNext()) {

					FlightScheduleLeg eleg = eit.next();
					if (eleg.getLegNumber() == uleg.getLegNumber()) {
						eleg = LegUtil.copyLeg(uleg, eleg);
						break;
					}
				}
			}

			// update flight segments
			Set<FlightScheduleSegment> existingSeg = exising.getFlightScheduleSegments();
			Set<FlightScheduleSegment> updatedSeg = updated.getFlightScheduleSegments();
			Iterator<FlightScheduleSegment> usit = updatedSeg.iterator();

			while (usit.hasNext()) {

				FlightScheduleSegment uSeg = usit.next();

				Iterator<FlightScheduleSegment> esit = existingSeg.iterator();
				while (esit.hasNext()) {

					FlightScheduleSegment eSeg = esit.next();
					if (uSeg.getSegmentCode().equals(eSeg.getSegmentCode())) {
						eSeg = SegmentUtil.copySegment(uSeg, eSeg);
						break;
					}
				}
			}
			// else (legs added/deleted) then create new legs for the schedule
		} else {

			// no need to remove overlap segment id from schedule segment as no FK constraint exists

			// clean up the old legs and segments
			Set<FlightScheduleLeg> oldLegs = exising.getFlightScheduleLegs();
			Iterator<FlightScheduleLeg> itOldLegs = oldLegs.iterator();
			while (itOldLegs.hasNext()) {

				FlightScheduleLeg oldLeg = itOldLegs.next();
				flightScheduleLegDAO.removeFlightScheduleLeg(oldLeg.getId().intValue());
			}

			Set<FlightScheduleSegment> oldSegments = exising.getFlightScheduleSegments();
			Iterator<FlightScheduleSegment> itOldSegments = oldSegments.iterator();
			while (itOldSegments.hasNext()) {
				FlightScheduleSegment oldSegment = itOldSegments.next();
				flightScheduleSegmentDAO.removeFlightScheduleSegment(oldSegment.getFlSchSegId().intValue());
			}

			// update flight schedule legs
			Set<FlightScheduleLeg> existingLegs = new HashSet<FlightScheduleLeg>();
			Set<FlightScheduleLeg> updatedLegs = updated.getFlightScheduleLegs();
			Iterator<FlightScheduleLeg> uit = updatedLegs.iterator();

			while (uit.hasNext()) {

				FlightScheduleLeg uleg = uit.next();
				existingLegs.add(LegUtil.copyLeg(uleg, new FlightScheduleLeg()));
			}
			exising.setFlightScheduleLegs(existingLegs);

			// update flight segments
			Set<FlightScheduleSegment> existingSeg = new HashSet<FlightScheduleSegment>();
			Set<FlightScheduleSegment> updatedSeg = updated.getFlightScheduleSegments();
			Iterator<FlightScheduleSegment> usit = updatedSeg.iterator();

			while (usit.hasNext()) {

				FlightScheduleSegment uSeg = usit.next();
				existingSeg.add(SegmentUtil.copySegment(uSeg, new FlightScheduleSegment()));
			}
			exising.setFlightScheduleSegments(existingSeg);
		}

		return exising;
	}

	/**
	 * private method to get the flight distance for the flight
	 * 
	 * @param legList
	 * @return flight distance
	 * @throws ModuleException
	 */
	private BigDecimal getFlightDistance(Collection<FlightScheduleLeg> legList) throws ModuleException {
		BigDecimal flightDistance = AccelAeroCalculator.getDefaultBigDecimalZero();

		Iterator<FlightScheduleLeg> itLegs = legList.iterator();
		while (itLegs.hasNext()) {
			FlightScheduleLeg leg = itLegs.next();
			RouteInfo routeInfo = commonMasterBD.getRouteInfo(leg.getModelRouteId());
			flightDistance = AccelAeroCalculator.add(flightDistance, routeInfo.getDistance());
		}

		return flightDistance;
	}


	private boolean isSkipFlightUpdate(boolean isScheduleProcess, Date departureDate) {
		boolean isSkip = false;
		if (isScheduleProcess && departureDate.before(CalendarUtil.getCurrentZuluDateTime())) {
			isSkip = true;
		}
		return isSkip;
	}
	
	/**
	 * Method to generate alerts Never used
	 * 
	 * @param flights
	 * @throws ModuleException
	 */
	/**
	 * private void generateFlightAlerts(List<Integer> flightIds) throws ModuleException { Collection<FlightSegement>
	 * flightSegments = new ArrayList<FlightSegement>(); Collection<ReservationSegment> pnrSegments = new
	 * ArrayList<ReservationSegment>(); Collection<Alert> alerts = new ArrayList<Alert>();
	 * Collection<ReservationSegment> temp; List<Integer> flightSegIds; int flightId;
	 * 
	 * for (int i = 0; i < flightIds.size(); i++) { flightId = ((Integer) flightIds.get(i)).intValue(); flightSegments =
	 * flightSegmentDAO.getFlightSegments(flightId); flightSegIds = SegmentUtil.getFlightSegIds(flightSegments); temp =
	 * segmentBD.getPnrSegments(flightSegIds); if ((temp != null) && (!temp.isEmpty())) { pnrSegments.addAll(temp); } }
	 * 
	 * Iterator<ReservationSegment> pnrSegIt = pnrSegments.iterator(); while (pnrSegIt.hasNext()) { ReservationSegment
	 * resSeg = (ReservationSegment) pnrSegIt.next(); Alert alert = new Alert("Flight Cancelled", new
	 * Date(System.currentTimeMillis()), null, new Long(1), new Long(1), new Long(resSeg.getPnrSegId().intValue()), new
	 * Date(System.currentTimeMillis())); alerts.add(alert); }
	 * 
	 * alertingBD.addAlerts(alerts); }
	 */
	
	private void reviseScheduleFrequencyPeriodDeleteInvalidSchedules(Integer scheduleId) throws ModuleException {

		if (scheduleId != null) {

			FlightSchedule schedule = flightSchedleDAO.getFlightSchedule(scheduleId.intValue());

			boolean freqMatched = CalendarUtil.frequencyMatchesDateRange(schedule.getStartDate(), schedule.getStopDate(),
					schedule.getFrequency());
			boolean isRemoved = false;
			// schedule frequency will be revised if its invalid when processing SSM/SSIM
			if (!freqMatched) {
				Frequency frequency = CalendarUtil.getValidFrequencyMatchesDateRange(schedule.getStartDate(),
						schedule.getStopDate(), schedule.getFrequency());
				if (!reValidateRevisedFrequency(frequency)) {
					// cancel schedule
					AirSchedulesUtil.getScheduleBD().cancelSchedule(scheduleId, true, AlertUtil.generateFlightAlertDTO(this.isScheduleMessageProcess), false,
							false);

					isRemoved = true;
				} else {
					schedule.setFrequency(frequency);
					flightSchedleDAO.saveFlightSchedule(schedule);
				}
			}

			if (!isRemoved && trimSchedulePeriod(schedule)) {
				FlightSchedule scheduleTrim = flightSchedleDAO.getFlightSchedule(scheduleId.intValue());
				scheduleTrim.setStartDate(CalendarUtil.getEffectiveStartDate(schedule.getStartDate(), schedule.getFrequency()));
				scheduleTrim
						.setStartDateLocal(timeAdder.getLocalDateTime(schedule.getDepartureStnCode(), schedule.getStartDate()));
				scheduleTrim.setStopDate(CalendarUtil.getEffectiveStopDate(schedule.getStopDate(), schedule.getFrequency()));
				scheduleTrim.setStopDateLocal(timeAdder.getLocalDateTime(schedule.getDepartureStnCode(), schedule.getStopDate()));
				flightSchedleDAO.saveFlightSchedule(scheduleTrim);
			}

		}
	}

	private boolean trimSchedulePeriod(FlightSchedule schedule) {
		Date splitStartDateZulu = schedule.getStartDate();
		Date splitEndDateZulu = schedule.getStopDate();
		Frequency splitFrequencyZulu = schedule.getFrequency();
		Frequency frequency = CalendarUtil.getValidFrequencyMatchesDateRange(splitStartDateZulu, splitEndDateZulu,
				splitFrequencyZulu);

		Date firstFlightDate = CalendarUtil.getEffectiveStartDate(splitStartDateZulu, frequency);
		Date lastFlightDate = CalendarUtil.getEffectiveStopDate(splitEndDateZulu, frequency);

		if (!CalendarUtil.isSameDay(splitStartDateZulu, firstFlightDate)
				|| !CalendarUtil.isSameDay(splitEndDateZulu, lastFlightDate)) {
			return true;
		}
		return false;
	}

	private boolean reValidateRevisedFrequency(Frequency frequency) {
		if (frequency.getDayCount(true) > 0) {
			return true;
		}
		return false;
	}
}
