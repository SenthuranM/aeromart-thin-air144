/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.util;

/**
 * Class to keep constants related to AirSchedule. The Constants defined in this class should only use within the module
 * 
 * @author Lasantha Pambagoda
 */
public abstract class InternalConstants {

	public static String MODULE_NAME = "airschedules";

	public static interface DAOProxyNames {

		public static String FLIGHT_DAO = "flightDAOProxy";

		public static String FLIGHT_LEG_DAO = "flightLegDAOProxy";

		public static String FLIGHT_SCHEDULE_DAO = "flightScheduleDAOProxy";

		public static String FLIGHT_SCHEDULE_LEG_DAO = "flightScheduleLegDAOProxy";

		public static String FLIGHT_SCHEDULE_SEGMENT_DAO = "flightScheduleSegmentDAOProxy";

		public static String FLIGHT_SEGMENT_DAO = "flightSegementDAOProxy";

		public static String AIR_SCHEDULES_COMMON_DAO = "airSchedulesCommonDAOProxy";

		public static String FLIGHT_SCHEDULE_JDBC_DAO = "flightScheduleJdbcDAO";
		
		public static String FLOWN_FLIGHTS_DTO = "UpdatePaxETStatusDAOProxy";
		
		public static String STATION_DAO = "StationDAOProxy";

	}
}
