package com.isa.thinair.airschedules.core.bl.search;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableConnectedFlight;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airschedules.api.dto.ConnectedAvailableFlightSegmentsDTO;
import com.isa.thinair.airschedules.api.dto.TransitAirport;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.core.persistence.dao.FlightDAO;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CommonsServices;

public class SearchAvailableConnectionsForReprotection {
	
	public List<AvailableConnectedFlight> getAvailableConnectedFlightSegments(Date dateTimeStart, Date dateTimeEnd,
			String origin, String destination, List<TransitAirport> stopOverAirports, int adults, int infants,
			String ccCode, int availabilityRestriction, Map<String, Date> allowableLatesDatesZuluForInterliningMap,
			Collection<String> externalBookingClasses, Collection<Integer> flightIdList) throws ModuleException {

		List<ConnectedAvailableFlightSegmentsDTO> previousConnectedFlightsList = null;
		List<ConnectedAvailableFlightSegmentsDTO> currentConnectedFlightsList = null;

		//AvailabilitySearchTypeEnum searchType = AvailabilitySearchTypeEnum.MULTIPLE_FLIGHTS_FOR_PERIOD;

		if (stopOverAirports != null && stopOverAirports.size() > 0) {
			List<AvailableFlightSegment> previousDirectFltSegs = null;

			for (int segmentSequence = 0; segmentSequence <= stopOverAirports.size(); ++segmentSequence) {
				List<AvailableFlightSegment> currentDirectFltSegs = null;
				// minDate and maxDate
				Date minDate = null;
				Date maxDate = null;
				String fromAirport = null;
				String toAirport = null;

				if (segmentSequence == 0) {
					// First segment of the journey
					fromAirport = origin;
					toAirport = stopOverAirports.get(segmentSequence).getAirportCode();

					minDate = dateTimeStart;
					maxDate = dateTimeEnd;
				} else if (stopOverAirports.get(segmentSequence - 1).isOrigin()) {
					fromAirport = stopOverAirports.get(segmentSequence - 1).getAirportCode();
					if (segmentSequence == stopOverAirports.size()) {
						toAirport = destination;
					} else {
						toAirport = stopOverAirports.get(segmentSequence).getAirportCode();
					}

					// minDate and maxDate - for the Nth connecting segment
					String[] minMaxTransitDurations = CommonsServices.getGlobalConfig().getMixMaxTransitDurations(
							fromAirport, null, null);
					AvailableFlightSegment firstSegment = previousDirectFltSegs.get(0);
					Date firstTime = firstSegment.getFlightSegmentDTO().getArrivalDateTime();
					Date minTrDate = this.getMinTransitTime(firstTime, minMaxTransitDurations[0]);

					AvailableFlightSegment lastSegment = previousDirectFltSegs.get(previousDirectFltSegs.size() - 1);
					Date lastTime = lastSegment.getFlightSegmentDTO().getArrivalDateTime();
					Date maxTrDate = this.getMaxTransitTime(lastTime, minMaxTransitDurations[1]);

					minDate = minTrDate;
					maxDate = maxTrDate;
				} else {
					continue;
				}

				currentDirectFltSegs = ((FlightDAO) AirSchedulesUtil.getInstance().getLocalBean(
						InternalConstants.DAOProxyNames.FLIGHT_DAO)).getMatchingFlightSegmentsWithInventoryDetails(
						minDate, maxDate, fromAirport, toAirport, adults, infants, ccCode,
						availabilityRestriction, false, false, false, externalBookingClasses, flightIdList, null, null);

				if (currentDirectFltSegs == null || currentDirectFltSegs.size() == 0) {
					// Clear the previous results
					if (currentConnectedFlightsList != null && currentConnectedFlightsList.size() > 0) {
						for (Iterator<ConnectedAvailableFlightSegmentsDTO> connectedFlightsListIt = currentConnectedFlightsList
								.iterator(); connectedFlightsListIt.hasNext();) {
							ConnectedAvailableFlightSegmentsDTO connectedFlights = (ConnectedAvailableFlightSegmentsDTO) connectedFlightsListIt
									.next();
							if (connectedFlights.getAvailableFlightSegments() != null) {
								connectedFlights.getAvailableFlightSegments().clear();
							}
						}
						currentConnectedFlightsList.clear();
					}

					break;
				}

				currentConnectedFlightsList = new ArrayList<ConnectedAvailableFlightSegmentsDTO>();

				// Construct connections
				if (segmentSequence == 0) {
					for (Iterator<AvailableFlightSegment> curDirectFltSegsIt = currentDirectFltSegs.iterator(); curDirectFltSegsIt
							.hasNext();) {
						AvailableFlightSegment currectAvailableFlightSegment = (AvailableFlightSegment) curDirectFltSegsIt
								.next();
						if (!isWithinInterlineCutover(currectAvailableFlightSegment,
								allowableLatesDatesZuluForInterliningMap)) {
							ConnectedAvailableFlightSegmentsDTO connectedFlightDTO = new ConnectedAvailableFlightSegmentsDTO();
							connectedFlightDTO.addAvailableFlightSegment(currectAvailableFlightSegment);
							currentConnectedFlightsList.add(connectedFlightDTO);
						}
					}
				} else {
					Iterator<ConnectedAvailableFlightSegmentsDTO> connectedFlightsListIt = previousConnectedFlightsList
							.iterator();
					while (connectedFlightsListIt.hasNext()) {
						ConnectedAvailableFlightSegmentsDTO existingConnectedFlights = (ConnectedAvailableFlightSegmentsDTO) connectedFlightsListIt
								.next();
						FlightSegmentDTO preceedingFlightSegmentDTO = existingConnectedFlights
								.getLastFlightSegmentDTO();

						Iterator<AvailableFlightSegment> currentDirectFltSegsIt = currentDirectFltSegs.iterator();
						while (currentDirectFltSegsIt.hasNext()) {

							AvailableFlightSegment currentDirectFltSeg = (AvailableFlightSegment) currentDirectFltSegsIt
									.next();
							if ((preceedingFlightSegmentDTO.getFlightId().intValue() != currentDirectFltSeg
									.getFlightSegmentDTO().getFlightId().intValue())) {// Exclude
																						// multi-leg
																						// flights

								String[] minMaxTransitDurations = CommonsServices.getGlobalConfig()
										.getMixMaxTransitDurations(fromAirport,
												preceedingFlightSegmentDTO.getCarrierCode(),
												currentDirectFltSeg.getFlightSegmentDTO().getCarrierCode());

								Date minDepartTime = this.getMinTransitTime(
										preceedingFlightSegmentDTO.getArrivalDateTime(), minMaxTransitDurations[0]);
								Date maxDepartTime = this.getMaxTransitTime(
										preceedingFlightSegmentDTO.getArrivalDateTime(), minMaxTransitDurations[1]);
								Date actualDepartTime = currentDirectFltSeg.getFlightSegmentDTO()
										.getDepartureDateTime();

								if ((minDepartTime.getTime() <= actualDepartTime.getTime())
										&& (actualDepartTime.getTime()) <= maxDepartTime.getTime()
										&& !isWithinInterlineCutover(currentDirectFltSeg,
												allowableLatesDatesZuluForInterliningMap)) {
									ConnectedAvailableFlightSegmentsDTO currentConnectedFlightSegmentsDTO = existingConnectedFlights
											.shallowClone();
									currentConnectedFlightSegmentsDTO.addAvailableFlightSegment(currentDirectFltSeg);
									currentConnectedFlightsList.add(currentConnectedFlightSegmentsDTO);
								}
							}
						}
						// empty the flight segment list
						existingConnectedFlights.getAvailableFlightSegments().clear();
					}
				}

				if (previousConnectedFlightsList != null) {
					// empty the previous connected flights list
					previousConnectedFlightsList.clear();
				}
				previousConnectedFlightsList = currentConnectedFlightsList;
				previousDirectFltSegs = currentDirectFltSegs;
			}
		}

		List<AvailableConnectedFlight> connectedAvailableFlightSegmentsList = null;
		if (currentConnectedFlightsList != null && currentConnectedFlightsList.size() > 0) {

			// Prepare AvailableConnectedFlights
			for (ConnectedAvailableFlightSegmentsDTO connectedFlights : currentConnectedFlightsList) {
				// Construct the connected OnD code
				String ondCode = "";
				int count = 0;
				for (Iterator<AvailableFlightSegment> connectedFlightSegmentsIt = connectedFlights
						.getAvailableFlightSegments().iterator(); connectedFlightSegmentsIt.hasNext();) {
					AvailableFlightSegment availableFlightSegment = (AvailableFlightSegment) connectedFlightSegmentsIt
							.next();
					if (count == 0) {
						ondCode = ondCode + availableFlightSegment.getFlightSegmentDTO().getSegmentCode();
					} else {
						String transit = availableFlightSegment.getFlightSegmentDTO().getSegmentCode().substring(0, 3);
						if (ondCode.endsWith(transit)) {
							ondCode = ondCode
									+ availableFlightSegment.getFlightSegmentDTO().getSegmentCode().substring(3);
						} else {
							ondCode = ondCode + "/" + availableFlightSegment.getFlightSegmentDTO().getSegmentCode();
						}
					}
					count++;
				}

				if (isConnectionValid(connectedFlights)) {
					if (connectedAvailableFlightSegmentsList == null) {
						connectedAvailableFlightSegmentsList = new ArrayList<AvailableConnectedFlight>();
					}
					//connectedAvailableFlightSegmentsList.add(connectedFlights.constructAvailableConnectedFlight(ondCode));
					connectedAvailableFlightSegmentsList.add(connectedFlights.constructAvailableConnectedFlight());
				}
			}
		}
		return connectedAvailableFlightSegmentsList;
	
	}
	
	/** method to get minimum transit time
	 * 
	 * @param date
	 * @param minTransitTimeValue
	 * @return min transit date
	 */
	private Date getMinTransitTime(Date date, String minTrValueStr) {

		String hours = minTrValueStr.substring(0, minTrValueStr.indexOf(":"));
		String mins = minTrValueStr.substring(minTrValueStr.indexOf(":") + 1);

		GregorianCalendar calander = new GregorianCalendar();
		calander.setTime(date);
		calander.add(GregorianCalendar.HOUR, Integer.parseInt(hours));
		calander.add(GregorianCalendar.MINUTE, Integer.parseInt(mins));

		return calander.getTime();
	}
	/**
	 * method to get maximum transit time
	 * 
	 * @param date
	 * @param maxTransitTimeValue
	 * @return max transit date
	 */
	private Date getMaxTransitTime(Date date, String maxTrValueStr) {

		String hours = maxTrValueStr.substring(0, maxTrValueStr.indexOf(":"));
		String mins = maxTrValueStr.substring(maxTrValueStr.indexOf(":") + 1);

		GregorianCalendar calander = new GregorianCalendar();
		calander.setTime(date);
		calander.add(GregorianCalendar.HOUR, Integer.parseInt(hours));
		calander.add(GregorianCalendar.MINUTE, Integer.parseInt(mins));

		return calander.getTime();
	}
	private boolean isWithinInterlineCutover(AvailableFlightSegment currectAvailableFlightSegment,
			Map<String,Date> allowableLatesDatesZuluForInterliningMap) {
		boolean isWithinInterlineCutover = false;
		FlightSegmentDTO flightSegmentDTO = currectAvailableFlightSegment.getFlightSegmentDTO();
		if (allowableLatesDatesZuluForInterliningMap.containsKey(flightSegmentDTO.getCarrierCode())) {
			if (flightSegmentDTO.getDepartureDateTimeZulu().before(
					(Date) allowableLatesDatesZuluForInterliningMap.get(flightSegmentDTO.getCarrierCode()))) {
				isWithinInterlineCutover = true;
			}
		}

		return isWithinInterlineCutover;

	}
	private boolean isConnectionValid(ConnectedAvailableFlightSegmentsDTO connectedFlights) {
		boolean isValid = false;
		if (connectedFlights.hasSystemCarriersSegments(CommonsServices.getGlobalConfig().getActiveSysCarriersMap().keySet())) {
			isValid = true;
		}
		return isValid;
	}
}
