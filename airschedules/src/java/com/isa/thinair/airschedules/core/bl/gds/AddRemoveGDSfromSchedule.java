/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.gds;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.isa.thinair.airschedules.api.dto.FlightScheduleSegmentInfoDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.FlightScheduleSegment;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.GDSFlightEventCollector;
import com.isa.thinair.airschedules.api.utils.GDSSchedConstants;
import com.isa.thinair.airschedules.api.utils.GDSScheduleEventCollector;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.airschedules.api.utils.ScheduleBuildStatusEnum;
import com.isa.thinair.airschedules.core.config.AirScheduleModuleConfig;
import com.isa.thinair.airschedules.core.persistence.dao.FlightDAO;
import com.isa.thinair.airschedules.core.persistence.dao.FlightScheduleDAO;
import com.isa.thinair.airschedules.core.util.CollectionManipulationUtil;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.airschedules.core.util.ScheduledFlightUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.gdsservices.api.dto.publishing.Envelope;
import com.isa.thinair.gdsservices.api.service.GDSPublishingBD;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for prepare ammend flight schedule
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="addRemoveGDSfromSchedule"
 */
public class AddRemoveGDSfromSchedule extends DefaultBaseCommand {

	// Dao's
	private FlightScheduleDAO flightSchedleDAO;

	private FlightDAO flightDAO;

	// BDs
	private GDSPublishingBD gdsPublishingBD;

	/**
	 * constructor of the addRemoveGDSfromSchedule command
	 */
	public AddRemoveGDSfromSchedule() {

		// looking up daos
		flightSchedleDAO = (FlightScheduleDAO) AirSchedulesUtil.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_DAO);

		flightDAO = (FlightDAO) AirSchedulesUtil.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_DAO);

		// looking up BDs
		gdsPublishingBD = AirSchedulesUtil.getGDSPublishingBD();
	}

	/**
	 * execute method of the addRemoveGDSfromSchedule command
	 * 
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	public ServiceResponce execute() throws ModuleException {

		DefaultServiceResponse responce = new DefaultServiceResponse(true);
		if ("Y".equalsIgnoreCase(CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.GDS_INTEGRATED))) {

			// getting command params
			Integer scheduleId = (Integer) this.getParameter(CommandParamNames.SCHEDULE_ID);
			Collection<Integer> updatedGdsList = (Collection<Integer>) this.getParameter(CommandParamNames.GDS_IDS);
			Integer versionId = (Integer) this.getParameter(CommandParamNames.VERSION_ID);
			UserPrincipal userPrincipal = (UserPrincipal)this.getParameter(CommandParamNames.USER_PRINCIPAL);

			// checking params
			this.checkParams(scheduleId, updatedGdsList, versionId);

			FlightSchedule schedule = flightSchedleDAO.getFlightSchedule(scheduleId.intValue());
			// if no flight found for the ammending flight schedule
			if (schedule == null) // throw exception
				throw new ModuleException("airschedules.logic.bl.schedule.noschedule");

			// if schedule not build only save the schedule
			if (schedule.getBuildStatusCode().equals(ScheduleBuildStatusEnum.PENDING.getCode())) {

				Set<Integer> gdsCodes = new HashSet<Integer>();
				gdsCodes.addAll(updatedGdsList);
				schedule.setGdsIds(gdsCodes);
				schedule.setVersion(versionId);

				flightSchedleDAO.saveFlightSchedule(schedule);

			} else {

				Collection<Integer> existingGdsList = (schedule.getGdsIds() != null) ? schedule.getGdsIds() : new ArrayList<Integer>();

				Collection<Integer> addedIds = CollectionManipulationUtil.getNotFoundList(updatedGdsList, existingGdsList);
				Collection<Integer> deletedIds = CollectionManipulationUtil.getNotFoundList(existingGdsList, updatedGdsList);

				Collection<Flight> flights = flightSchedleDAO.getFlightsForSchedule(scheduleId);
				Iterator<Flight> itFlights = flights.iterator();

				Set<Integer> gdsCodes = new HashSet<Integer>();
				gdsCodes.addAll(updatedGdsList);

				while (itFlights.hasNext()) {

					Flight flight = (Flight) itFlights.next();
					flight.setGdsIds(gdsCodes);

					flightDAO.saveFlight(flight);
				}

				schedule.setGdsIds(gdsCodes);
				schedule.setVersion(versionId);
				flightSchedleDAO.saveFlightSchedule(schedule);

				GDSScheduleEventCollector eventCollector = new GDSScheduleEventCollector();

				// String serviceType = ((AirScheduleModuleConfig) AirSchedulesUtil.getInstance().getModuleConfig())
				// .getServiceTypeForAdHokFlight();
				// Since this is schedule creation or removal we should use getServiceTypeForScheduleFlight
				String serviceType = ((AirScheduleModuleConfig) AirSchedulesUtil.getInstance().getModuleConfig())
						.getServiceTypeForScheduleFlight();
				eventCollector.setServiceType(serviceType);
				eventCollector.setUpdatedSchedule(schedule);

				
				HashMap<Integer, List<Integer>> flightIdMap = new HashMap<Integer, List<Integer>>();
				flightIdMap.put(schedule.getScheduleId(), ScheduledFlightUtil.getFlightIds(flights));
				
				String siOverlapElement = getSupplementaryOverlapElement(schedule);
				
				if (addedIds.size() > 0) {
					eventCollector.addAction(GDSFlightEventCollector.GDS_ADDED);
					eventCollector.setAddedGDSList(addedIds);
					
					if(siOverlapElement.length()>6){
						eventCollector.setSupplementaryInfo(siOverlapElement);
					}
				}

				if (deletedIds.size() > 0) {
					eventCollector.addAction(GDSFlightEventCollector.GDS_DELETED);
					eventCollector.setDeletedGDSList(deletedIds);
				}
				
				Envelope env = new Envelope(GDSSchedConstants.TargetLogicNames.PUBLISH_ADD_REMOVE_SSM);
				env.addParam(GDSSchedConstants.ParamNames.GDS_EVENT_COLLECTOR, eventCollector);
				env.addParam(GDSSchedConstants.ParamNames.FLIGHT_IDS_MAP_FOR_SCHEDULES, flightIdMap);
				env.addParam(GDSSchedConstants.ParamNames.CARRIER_CODE, getCarrierCode(schedule.getFlightNumber()));

				gdsPublishingBD.publishMessage(env);
				
				if(addedIds.size() > 0 || deletedIds.size() > 0 ) {
					AuditGDSPublishUnpublish.publishUnpublishSchedule(schedule, addedIds, deletedIds, userPrincipal);
					Iterator<Flight> itFlightsForAudit = flights.iterator();
					while (itFlightsForAudit.hasNext()) {
						Flight flight = (Flight) itFlightsForAudit.next();
						AuditGDSPublishUnpublish.publishUnpublishFlight(flight, addedIds, deletedIds, userPrincipal);					
					}
				}				
				
			}

			// constructing and return command responce
			responce.setResponseCode(ResponceCodes.ADD_REMOVE_GDS_SUCCESS);
			responce.addResponceParam(ResponceCodes.ADD_REMOVE_GDS_SUCCESS, schedule.getVersion());
		}

		return responce;
	}

	private String getSupplementaryOverlapElement(FlightSchedule schedule) {
		
		StringBuilder siOLAP = new StringBuilder();
		if (schedule.getOverlapingScheduleId() != null){
			FlightSchedule overlapPingSchedule = flightSchedleDAO.getFlightSchedule(schedule.getOverlapingScheduleId());
			if(!overlapPingSchedule.getGdsIds().isEmpty()){
				
				Collection<FlightScheduleSegment> flightScheduleSegments = schedule.getFlightScheduleSegments();
				int i = 0;
				siOLAP.append("OLAP ");
				for (FlightScheduleSegment segment : flightScheduleSegments) {
					if (segment.getOverlapSegmentId()== null){
						if(i>0){
							siOLAP.append("#");
						}
						siOLAP.append(segment.getSegmentCode());
						i++;
					}
				}
			}
		}

		return siOLAP.toString();
	}

	/**
	 * privte method to validate parameters
	 * 
	 * @param schedule
	 * @throws ModuleException
	 */
	private void checkParams(Integer scheduleId, Collection<Integer> gdsIds, Integer versionId) throws ModuleException {

		if (scheduleId == null || gdsIds == null || versionId == null)
			throw new ModuleException("airschedules.arg.invalid.null");
	}

	/**
	 * Get Carrier Code from flight number, assuming first 2 char represent carrier code
	 * 
	 * @param flightNumebr
	 * @return
	 */
	private String getCarrierCode(String flightNumebr) {

		String carrierCode = "";
		if (flightNumebr != null && flightNumebr.length() >= 5) {
			carrierCode = flightNumebr.substring(0, 2);
		}
		return carrierCode;
	}
}
