/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.check;

import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for the flight status update eligibility
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="checkFlightStatusChange"
 */
public class CheckFlightStatusChange extends DefaultBaseCommand {

	/**
	 * constructor of the CheckFlightStatusChange command
	 */
	public CheckFlightStatusChange() {

	}

	/**
	 * execute method of the CheckFlightStatusChange command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {

		// getting command params
		Flight updated = (Flight) this.getParameter(CommandParamNames.FLIGHT);
		Flight existing = (Flight) this.getParameter(CommandParamNames.EXISTING_FLIGHT);

		// checking params
		this.checkParams(updated, existing);

		// constructing command responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);

		String exStatus = existing.getStatus();
		String updStatus = updated.getStatus();

		String act = FlightStatusEnum.ACTIVE.getCode();
		String cls = FlightStatusEnum.CLOSED.getCode();

		// Following validation is removed with AARESAA-18554
		// if (!CalendarUtil.getStartTimeOfDate(updated.getDepartureDate()).equals(
		// CalendarUtil.getStartTimeOfDate(existing.getDepartureDate()))) {
		// responce.setSuccess(false);
		// responce.setResponseCode(ResponceCodes.INVALID_DEPARTURE_DATE);
		// return responce;
		// }

		if (!updated.getDepartureDate().after(CalendarUtil.getCurrentSystemTimeInZulu())) {
			responce.setSuccess(false);
			responce.setResponseCode(ResponceCodes.INVALID_DEPARTURE_DATE);
			return responce;
		}

		if ((exStatus.equals(act) && updStatus.equals(cls)) || (exStatus.equals(cls) && updStatus.equals(act))
				|| exStatus.equals(updStatus)) {
			// status change is ok

		} else {
			// do not allow the chenge

			responce.setSuccess(false);
			responce.setResponseCode(ResponceCodes.INVALID_FLIGHT_STATUS_CHANGE);
			return responce;
		}

		// return command responce
		return responce;
	}

	/**
	 * privte method to validate parameters
	 * 
	 * @param schedule
	 * @throws ModuleException
	 */
	private void checkParams(Flight flight, Flight existing) throws ModuleException {

		if (flight == null || existing == null)// throw exception
			throw new ModuleException("airschedules.arg.invalid.null");
	}
}
