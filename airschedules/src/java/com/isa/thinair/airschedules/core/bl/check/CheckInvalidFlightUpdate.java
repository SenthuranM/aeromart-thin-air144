/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.check;

import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.LegUtil;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.platform.api.ServiceResponce;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

/**
 * command for the flight update eligibility
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="checkInvalidFlightUpdate"
 */
public class CheckInvalidFlightUpdate extends DefaultBaseCommand {

	private final Log log = LogFactory.getLog(CheckInvalidFlightUpdate.class);
	/**
	 * constructor of the CheckInvalidUpdatesForFlight command
	 */
	public CheckInvalidFlightUpdate() {

	}

	/**
	 * execute method of the CheckInvalidUpdatesForFlight command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {

		// getting command params
		Flight updated = (Flight) this.getParameter(CommandParamNames.FLIGHT);
		Flight existing = (Flight) this.getParameter(CommandParamNames.EXISTING_FLIGHT);
		boolean hasPriviledgeToActivatePastFlight = (Boolean) this.getParameter(CommandParamNames.ACTIVATE_PAST_FLIGHT);
		boolean isScheduleMessageProcess = getParameter(CommandParamNames.IS_SHEDULE_MESSAGE_PROCESS, Boolean.FALSE,
				Boolean.class);
		// checking params
		this.checkParams(updated, existing);

		// constructing command responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);

		if (existing.getScheduleId() != null) {

			// op type and date cannot be changed for scheduled flight
			if (!isScheduleMessageProcess && ((updated.getOperationTypeId() != existing.getOperationTypeId())
					|| LegUtil.checkLegsAddedDeleted(existing, updated))) {

				responce.setSuccess(false);
				responce.setResponseCode(ResponceCodes.TRYING_ILLEGAL_FLIGHT_UPDATION);
				return responce;
			}
		}

		// get the calander with current time
		GregorianCalendar toCalander = new GregorianCalendar(TimeZone.getTimeZone("GMT"));

		String minAmmendPeriod = CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.MINIMUM_TIME_ALLOWED_FOR_SCHEDULE);
		String dateFormat = CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.DATE_FORMAT_FOR_DATE_STRINGS);

		Date date;
		try {
			date = CalendarUtil.getParsedTime(minAmmendPeriod, dateFormat);
		} catch (ParseException e) {
			throw new ModuleException("airschedules.arg.invalid");
		}

		if (date != null) {

			GregorianCalendar period = new GregorianCalendar();
			period.setTime(date);

			toCalander.add(GregorianCalendar.DAY_OF_MONTH, period.get(GregorianCalendar.DAY_OF_MONTH) - 1);
			toCalander.add(GregorianCalendar.HOUR_OF_DAY, period.get(GregorianCalendar.HOUR_OF_DAY));
			toCalander.add(GregorianCalendar.MINUTE, period.get(GregorianCalendar.MINUTE));
		}

		GregorianCalendar departureDateCalender = new GregorianCalendar(TimeZone.getTimeZone("GMT"));
		Calendar depCalendar = Calendar.getInstance();
		depCalendar.setTime(existing.getDepartureDate());

		departureDateCalender.set(GregorianCalendar.DATE, depCalendar.get(Calendar.DATE));
		departureDateCalender.set(GregorianCalendar.MONTH, depCalendar.get(Calendar.MONTH));
		departureDateCalender.set(GregorianCalendar.YEAR, depCalendar.get(Calendar.YEAR));
		departureDateCalender.set(GregorianCalendar.HOUR_OF_DAY, depCalendar.get(Calendar.HOUR_OF_DAY));
		departureDateCalender.set(GregorianCalendar.MINUTE, depCalendar.get(Calendar.MINUTE));
		departureDateCalender.set(GregorianCalendar.SECOND, depCalendar.get(Calendar.SECOND));

		String jobGroupName;
		String className;
		if (departureDateCalender.getTimeInMillis() <= toCalander.getTimeInMillis()) {
			if (hasPriviledgeToActivatePastFlight && AppSysParamsUtil.isAllowedModificationForPastFlight()) {
				if (updated.getDepartureDate().getTime() > toCalander.getTimeInMillis()) {
					// Schedule the job for close flight status
					jobGroupName = "FLIGHT_CLOSER";
					className = "com.isa.thinair.scheduledservices.core.client.FlightClosurer";
					scheduleJob(updated, existing, jobGroupName, className);

					// Schedule the job for update ETicket and Pax status
					jobGroupName = "FLIGHT_PNR_FLOWN";
					className = "com.isa.thinair.scheduledservices.core.client.FlightBookingFlownJob";
					scheduleJob(updated, existing, jobGroupName, className);
				} else {
					responce.setSuccess(false);
					responce.setResponseCode(ResponceCodes.TRYING_ILLEGAL_FLIGHT_UPDATION);
					return responce;
				}
			} else {
				responce.setSuccess(false);
				responce.setResponseCode(ResponceCodes.TRYING_ILLEGAL_FLIGHT_UPDATION);
				return responce;
			}
		} else {
			if (updated.getDepartureDate().getTime() > toCalander.getTimeInMillis()) {
				// Remove existing flown scheduler job
				jobGroupName = "FLIGHT_PNR_FLOWN";
				removeJob(existing, jobGroupName);

				//Reopen flown scheduler job if flight is delayed before the scheduled time of departure
				jobGroupName = "FLIGHT_PNR_FLOWN";
				className = "com.isa.thinair.scheduledservices.core.client.FlightBookingFlownJob";
				scheduleJob(updated, existing, jobGroupName, className);
			}
		}
		return responce;
	}

	private void scheduleJob(Flight updated, Flight existing, String jobGroupName, String className)
			throws ModuleException {
		for (FlightSegement leg : updated.getFlightSegements()) {
			SimpleDateFormat df = new SimpleDateFormat(CalendarUtil.PATTERN7);
			Date updatedClosureTime = null;
			if ("FLIGHT_CLOSER".equalsIgnoreCase(jobGroupName)) {
				updatedClosureTime = new Date(getFlightClosuringTime(leg.getEstTimeDepatureZulu()).getTime());
			} else if ("FLIGHT_PNR_FLOWN".equalsIgnoreCase(jobGroupName)) {
				updatedClosureTime = leg.getEstTimeDepatureZulu();
			}
			String formattedDate = df.format(updatedClosureTime);
			String deptAirPort = leg.getSegmentCode().split("/")[0];
			String jobName = updated.getFlightNumber() + "/" + deptAirPort + "/" + formattedDate;
			Map<String, String> jobDataMap = getJobDataMap(existing, deptAirPort, formattedDate, jobName, jobGroupName,
					className);
			if (!AirSchedulesUtil.getSchedularBD().wasJobScheduled(jobName, jobGroupName)) {
				try {
					AirSchedulesUtil.getSchedularBD().scheduleJob(jobDataMap);
					log.info(
							"[###############JOB=" + jobName + ", MARK TIME=" + formattedDate + "####################");
				} catch (Exception e) {
					log.error("DID NOT SCHEDULE JOB=" + jobName, e);
				}
			}

		}
	}

	private void removeJob(Flight existing, String jobGroupName) throws ModuleException {
		for (FlightSegement leg : existing.getFlightSegements()) {
			SimpleDateFormat df = new SimpleDateFormat(CalendarUtil.PATTERN7);
			Date existingDeptTime = leg.getEstTimeDepatureZulu();
			String formattedDate = df.format(existingDeptTime);
			String deptAirPort = leg.getSegmentCode().split("/")[0];
			String jobName = existing.getFlightNumber() + "/" + deptAirPort + "/" + formattedDate;

			if (AirSchedulesUtil.getSchedularBD().wasJobScheduled(jobName, jobGroupName)) {
				try {
					// REMOVE THE JOB FROM QUARTZ TABLE
					if (log.isInfoEnabled()) {
						log.info("Removing execution waiting pnr flown job. [JobName=" + jobName + ", JobGroupName="
								+ jobGroupName + "]");
					}
					AirSchedulesUtil.getSchedularBD().removeJob(jobName, jobGroupName);
				} catch (Exception exception) {
					log.error("Exception in removing pnr flown quartz job [JobName=" + jobName + ", JobGroupName="
							+ jobGroupName + "]", exception);
				}
			}
		}
	}

	private Map<String, String> getJobDataMap(Flight existing, String deptAirPort, String formattedDate, String jobName,
			String jobGroupName, String className) {
		Map<String, String> jobDataMap = new HashMap<>();
		jobDataMap.put("FLIGHT_ID", existing.getFlightId().toString());
		jobDataMap.put("FLIGHT_NUMBER", existing.getFlightNumber());
		jobDataMap.put("DEPT_AIRPORT", deptAirPort);
		jobDataMap.put("SCHEDULED_TIME", formattedDate);
		jobDataMap.put("JOB_NAME", jobName);
		jobDataMap.put("JOB_GROUP_NAME", jobGroupName);
		jobDataMap.put("CLASS_NAME", className);
		return jobDataMap;
	}

	private Timestamp getFlightClosuringTime(Date ZuluDepTime) {
		long flightclosurerdeparturegap = Long.parseLong(CommonsServices.getGlobalConfig().getBizParam(
				SystemParamKeys.FLIGHT_CLOSURE_DEPARTURE_GAP));
		return new Timestamp(ZuluDepTime.getTime() - flightclosurerdeparturegap * 60 * 1000);
	}

	/**
	 * private method to validate parameters
	 * 
	 * @throws ModuleException
	 */
	private void checkParams(Flight flight, Flight existing) throws ModuleException {

		if (flight == null || existing == null)// throw exception
			throw new ModuleException("airschedules.arg.invalid.null");
	}
}
