/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.prepare;

import java.util.Date;

import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightLeg;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.LegUtil;
import com.isa.thinair.airschedules.api.utils.LocalZuluTimeAdder;
import com.isa.thinair.airschedules.core.persistence.dao.FlightDAO;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.airschedules.core.util.ScheduledFlightUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for prepare the copy fliight
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="prepareCopyFlight"
 */
public class PrepareCopyFlight extends DefaultBaseCommand {

	// DAOs
	private FlightDAO flightDAO;

	// BDs
	private AirportBD airportBD;

	// helpers
	private LocalZuluTimeAdder timeAdder;

	/**
	 * constructor of the PrepareCopyFlight command
	 */
	public PrepareCopyFlight() {

		// DAOs
		flightDAO = (FlightDAO) AirSchedulesUtil.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_DAO);

		// BDs
		airportBD = AirSchedulesUtil.getAirportBD();

		// helpers
		timeAdder = new LocalZuluTimeAdder(airportBD);
	}

	/**
	 * execute method of the PrepareCopyFlight command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {

		// getting command params
		Integer flightId = (Integer) this.getParameter(CommandParamNames.FLIGHT_ID);
		Date date = (Date) this.getParameter(CommandParamNames.DEPARTURE_DATE);
		Boolean datesInLocal = (Boolean) this.getParameter(CommandParamNames.IS_DATE_IN_LOCAL);

		// checking params
		this.checkParams(flightId, date, datesInLocal);

		// get the flight from database
		Flight flight = flightDAO.getFlight(flightId.intValue());
		// if no flight found for id throw exception
		if (flight == null)
			throw new ModuleException("airschedules.logic.bl.flight.noflight");

		// get the copy of flight
		Flight copyedFlight = ScheduledFlightUtil.getCopyOfFlight(flight);

		// set the fields not comming from the coping flight
		FlightLeg copyedFirstLeg = LegUtil.getFirstFlightLeg(copyedFlight.getFlightLegs());

		if (datesInLocal.booleanValue()) {

			String origin = flight.getDestinationAptCode();
			Date effectiveDate = flight.getDepartureDate();

			Date flightLocalStartTime = timeAdder.getLocalDateTimeAsEffective(origin, effectiveDate,
					copyedFirstLeg.getEstDepartureTimeZulu());

			Date copyDepatureDate = timeAdder.getZuluDateTimeAsEffective(origin, effectiveDate,
					CalendarUtil.getTimeAddedDate(date, flightLocalStartTime));

			copyedFlight.setDepartureDate(copyDepatureDate);

		} else {

			Date copyDepatureDate = CalendarUtil.getTimeAddedDate(date, copyedFirstLeg.getEstDepartureTimeZulu());
			copyedFlight.setDepartureDate(copyDepatureDate);
		}

		timeAdder.addOnlySegementTimeDetails(copyedFlight);

		// constructing and return command responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);
		responce.addResponceParam(CommandParamNames.FLIGHT, copyedFlight);
		return responce;
	}

	/**
	 * privte method to validate parameters
	 * 
	 * @param schedule
	 * @throws ModuleException
	 */
	private void checkParams(Integer flightId, Date date, Boolean datesInLocal) throws ModuleException {

		if (flightId == null || date == null || datesInLocal == null)// throw exception
			throw new ModuleException("airschedules.arg.invalid.null");
	}

}
