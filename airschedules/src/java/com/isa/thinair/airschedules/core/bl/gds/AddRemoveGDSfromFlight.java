/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.gds;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.FlightScheduleSegment;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.GDSFlightEventCollector;
import com.isa.thinair.airschedules.api.utils.GDSSchedConstants;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.airschedules.core.config.AirScheduleModuleConfig;
import com.isa.thinair.airschedules.core.persistence.dao.FlightDAO;
import com.isa.thinair.airschedules.core.persistence.dao.FlightScheduleDAO;
import com.isa.thinair.airschedules.core.util.CollectionManipulationUtil;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.gdsservices.api.dto.publishing.Envelope;
import com.isa.thinair.gdsservices.api.service.GDSPublishingBD;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for prepare ammend flight schedule
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="addRemoveGDSfromFlight"
 */
public class AddRemoveGDSfromFlight extends DefaultBaseCommand {

	// BDs
	private GDSPublishingBD gdsPublishingBD;

	// Dao's
	private FlightDAO flightDAO;
	
	private FlightScheduleDAO flightScheduleDAO;

	/**
	 * constructor of the addRemoveGDSfromFlight command
	 */
	public AddRemoveGDSfromFlight() {

		// looking up daos
		flightDAO = (FlightDAO) AirSchedulesUtil.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_DAO);
		
		flightScheduleDAO = (FlightScheduleDAO) AirSchedulesUtil.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_DAO);

		// looking up BDs
		gdsPublishingBD = AirSchedulesUtil.getGDSPublishingBD();
	}

	/**
	 * execute method of the addRemoveGDSfromFlight command
	 * 
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	public ServiceResponce execute() throws ModuleException {

		DefaultServiceResponse response = new DefaultServiceResponse(true);
		if ("Y".equalsIgnoreCase(CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.GDS_INTEGRATED))) {

			// getting command params
			Integer flightId = (Integer) this.getParameter(CommandParamNames.FLIGHT_ID);
			Integer versionId = (Integer) this.getParameter(CommandParamNames.VERSION_ID);
			Collection<Integer> updatedGdsList = (this.getParameter(CommandParamNames.GDS_IDS) != null) ? (Collection<Integer>) this
					.getParameter(CommandParamNames.GDS_IDS) : new ArrayList<Integer>();
			UserPrincipal userPrincipal = (UserPrincipal) this.getParameter(CommandParamNames.USER_PRINCIPAL);
			// checking params
			this.checkParams(flightId, updatedGdsList, versionId);

			Flight flight = flightDAO.getFlight(flightId.intValue());

			// if no flight found for the given flight number
			if (flight == null) // throw exception
				throw new ModuleException("airschedules.logic.bl.schedule.noschedule");

			Collection<Integer> existingGdsList = (flight.getGdsIds() != null) ? flight.getGdsIds() : new ArrayList<Integer>();

			Collection<Integer> addedIds = CollectionManipulationUtil.getNotFoundList(updatedGdsList, existingGdsList);
			Collection<Integer> deletedIds = CollectionManipulationUtil.getNotFoundList(existingGdsList, updatedGdsList);
		
			// if flight is not a scheduled flight
			//Accept scheduled flight if schedule have only that flight
			if (flight.getScheduleId() == null || flightScheduleDAO.getNoOfFlightsForSchedule(flight.getScheduleId()) == 1) {

				Set<Integer> gdsCodes = new HashSet<Integer>();
				gdsCodes.addAll(updatedGdsList);
				flight.setGdsIds(gdsCodes);
				flight.setVersion(versionId);

				flightDAO.saveFlight(flight);
			}

			GDSFlightEventCollector eventCollector = new GDSFlightEventCollector();
			
			String siOverlapElement = getSupplementaryOverlapElement(flight);

			if (addedIds.size() > 0) {
				eventCollector.addAction(GDSFlightEventCollector.GDS_ADDED);
				eventCollector.setAddedGDSList(addedIds);
				
				if(siOverlapElement.length()>6){
					eventCollector.setSupplementaryInfo(siOverlapElement);
				}
			}

			if (deletedIds.size() > 0) {
				eventCollector.addAction(GDSFlightEventCollector.GDS_DELETED);
				eventCollector.setDeletedGDSList(deletedIds);
			}

			String serviceType = ((AirScheduleModuleConfig) AirSchedulesUtil.getInstance().getModuleConfig())
					.getServiceTypeForAdHokFlight();
			eventCollector.setServiceType(serviceType);
			eventCollector.setUpdatedFlight(flight);

			Envelope env = new Envelope(GDSSchedConstants.TargetLogicNames.PUBLISH_ADD_REMOVE_ASM);
			env.addParam(GDSSchedConstants.ParamNames.GDS_EVENT_COLLECTOR, eventCollector);

			gdsPublishingBD.publishMessage(env);
			
			if(addedIds.size() > 0 || deletedIds.size() > 0 ) {
				AuditGDSPublishUnpublish.publishUnpublishFlight(flight, addedIds, deletedIds, userPrincipal);
			}

			// constructing and return command response
			response.setResponseCode(ResponceCodes.ADD_REMOVE_GDS_SUCCESS);
			response.addResponceParam(ResponceCodes.ADD_REMOVE_GDS_SUCCESS, flight.getVersion());
		}

		return response;
	}

	/**
	 * privte method to validate parameters
	 * 
	 * @param schedule
	 * @throws ModuleException
	 */
	private void checkParams(Integer flightId, Collection<Integer> gdsIds, Integer versionId) throws ModuleException {

		if (flightId == null || gdsIds == null || versionId == null)
			throw new ModuleException("airschedules.arg.invalid.null");
	}
	
	private String getSupplementaryOverlapElement(Flight flight) {
		
		StringBuilder siOLAP = new StringBuilder();
		if (flight.getOverlapingFlightId() != null){
			Flight overlapPingFlight = flightDAO.getFlight(flight.getOverlapingFlightId());
			if(!overlapPingFlight.getGdsIds().isEmpty()){
				
				Collection<FlightSegement> flightSegments = flight.getFlightSegements();
				int i = 0;
				siOLAP.append("OLAP ");
				for (FlightSegement segment : flightSegments) {
					if (segment.getOverlapSegmentId()== null){
						if(i>0){
							siOLAP.append("#");
						}
						siOLAP.append(segment.getSegmentCode());
						i++;
					}
				}
			}
		}

		return siOLAP.toString();
	}

}
