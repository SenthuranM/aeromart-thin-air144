package com.isa.thinair.airschedules.core.bl.operation;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airmaster.api.model.Gds;
import com.isa.thinair.airschedules.api.model.CodeShareMCFlightSchedule;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.airschedules.core.persistence.dao.FlightScheduleDAO;
import com.isa.thinair.airschedules.core.persistence.dao.FlightScheduleJdbcDAO;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for the auto publish schedule
 * 
 * @author rajitha
 * @isa.module.command name="autoPublishSchedules"
 */
public class AutoPublishSchedules extends DefaultBaseCommand {

	private Map<String, GDSStatusTO> gdsStatusMap;
	
	@Override
	public ServiceResponce execute() throws ModuleException {

		FlightSchedule schedule = (FlightSchedule) this.getParameter(CommandParamNames.FLIGHT_SCHEDULE);
		this.checkParams(schedule);
		DefaultServiceResponse response = processRequest(schedule);
		

		if (response.isSuccess()) {
			response.setResponseCode(ResponceCodes.SCHEDULE_GDS_AUTOPUBLISH_SUCCESSFULL);
			response.addResponceParam(CommandParamNames.LAST_RESPONSE, response);
			response.addResponceParam(CommandParamNames.SCHEDULE_ID, schedule.getScheduleId());
		}
		return response;
	}

	private DefaultServiceResponse processRequest(FlightSchedule schedule) throws ModuleException {

		DefaultServiceResponse response = new DefaultServiceResponse(true);

		if (AppSysParamsUtil.isEnableAutoPublishSchedule()) {

			// operated by other carrier, code-shared schedule info
			if (StringUtil.isNullOrEmpty(schedule.getCsOCCarrierCode())
					|| StringUtil.isNullOrEmpty(schedule.getCsOCFlightNumber())) {

				setGdsStatusToMap();
				Set<Integer> activeGdsIds = getActiveGdsIdsForRoute(schedule);

				if (checkGdsActiveForAutoPublish(activeGdsIds)) {

					schedule.setGdsIds(activeGdsIds);
					setCodeShareMCForFlightSchedule(schedule, activeGdsIds);
					AirSchedulesUtil.getScheduleBD().publishScheduleToGDS(schedule.getScheduleId(), activeGdsIds,
							Integer.parseInt(new Long(schedule.getVersion()).toString()) );
				}

			}
		}

		return response;

	}

	private boolean checkGdsActiveForAutoPublish(Set<Integer> setToCheck) {

		if (setToCheck == null || setToCheck.isEmpty()) {
			return false;
		}

		return true;

	}

	private void checkParams(FlightSchedule schedule) throws ModuleException {

		if (schedule == null) {
			throw new ModuleException("airschedules.arg.invalid.null");
		}
	}

	private Set<Integer> getActiveGdsIdsForRoute(FlightSchedule schedule) throws ModuleException {

		Set<Integer> publishedGdsIdsForRoute = getAutoPublishedGdsIdsForRoute(schedule);
		Set<Integer> activeGdsIds = new HashSet<Integer>();

		if (publishedGdsIdsForRoute != null && !publishedGdsIdsForRoute.isEmpty()) {
			Map<String, GDSStatusTO> gdsStatusMap = this.getGdsStatusMap();
			for (GDSStatusTO gdsStatusTo : gdsStatusMap.values()) {

				if (publishedGdsIdsForRoute.contains(gdsStatusTo.getGdsId())) {
					activeGdsIds.add(gdsStatusTo.getGdsId());

				}

			}
		}

		return activeGdsIds;
	}

	private Set<Integer> getAutoPublishedGdsIdsForRoute(FlightSchedule schedule) throws ModuleException {

		String origin = schedule.getDepartureStnCode();
		String destination = schedule.getArrivalStnCode();

		Set<Gds> publishedGdsSet = lookupFlightScheduleJdbcDAO().getAutoPublishedGds(origin, destination);
		Set<Integer> gdsIdSet = new HashSet<Integer>();
		if (publishedGdsSet != null && !publishedGdsSet.isEmpty()) {

			for (Gds gds : publishedGdsSet) {
				gdsIdSet.add(gds.getGdsId());
			}

		} else {

			return gdsIdSet;
		}

		return gdsIdSet;
	}

	private void setCodeShareMCForFlightSchedule(FlightSchedule schedule, Set<Integer> activeGdsIds) {

		Map<String, GDSStatusTO> gdsStatusMap = this.getGdsStatusMap();

		Set<CodeShareMCFlightSchedule> csMcFlightScheduleSet = new HashSet<CodeShareMCFlightSchedule>();

		for (GDSStatusTO gdsTo : gdsStatusMap.values()) {
			if (activeGdsIds.contains(gdsTo.getGdsId()) && gdsTo.isCodeShareCarrier()) {
				CodeShareMCFlightSchedule csMcFlightSchedule = new CodeShareMCFlightSchedule();
				csMcFlightSchedule.setScheduleId(schedule.getScheduleId());
				csMcFlightSchedule.setCsMCFlightNumber(schedule.getFlightNumber());
				csMcFlightSchedule.setCsMCCarrierCode(gdsTo.getCarrierCode());
				csMcFlightScheduleSet.add(csMcFlightSchedule);
			}

		}

		schedule.setCodeShareMCFlightSchedules(csMcFlightScheduleSet);

	}

	private FlightScheduleJdbcDAO lookupFlightScheduleJdbcDAO() {

		FlightScheduleJdbcDAO dao = null;

		dao = (FlightScheduleJdbcDAO) AirSchedulesUtil.getInstance()
				.getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_JDBC_DAO);

		return dao;
	}

	private FlightScheduleDAO getFlightScheduleDAO() {
		FlightScheduleDAO flightSchedleDAO = (FlightScheduleDAO) AirSchedulesUtil.getInstance()
				.getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_DAO);
		return flightSchedleDAO;
	}

	private void setGdsStatusToMap() {
		this.setGdsStatusMap(CommonsServices.getGlobalConfig().getActiveGdsMap());

	}

	public Map<String, GDSStatusTO> getGdsStatusMap() {
		return gdsStatusMap;
	}

	public void setGdsStatusMap(Map<String, GDSStatusTO> gdsStatusMap) {
		this.gdsStatusMap = gdsStatusMap;
	}

}
