/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.check;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.airschedules.core.persistence.dao.FlightDAO;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for the flight overlap eligibility
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="checkFlightOverlapEligibility"
 */
public class CheckFlightOverlapEligibility extends DefaultBaseCommand {

	// Dao's
	private FlightDAO flightDAO;

	/**
	 * constructor of the CheckFlightOverlapEligibility command
	 */
	public CheckFlightOverlapEligibility() {

		// looking up daos
		flightDAO = (FlightDAO) AirSchedulesUtil.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_DAO);
	}

	/**
	 * execute method of the CheckFlightOverlapEligibility command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {

		// getting command params
		Flight updated = (Flight) this.getParameter(CommandParamNames.FLIGHT);
		Flight existing = (Flight) this.getParameter(CommandParamNames.EXISTING_FLIGHT);
		Flight updatedOverlapping = (Flight) this.getParameter(CommandParamNames.OVERLAPPING_FLIGHT);

		// checking params
		this.checkParams(updated);

		// constructing command responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);

		boolean overlapping = updated.isOverlapping();

		if (overlapping) {

			// if flight is new flight
			if (updated.getFlightId() == null) {

				this.checkOverlappsForNewOverlaps(updated, responce);

				// if flight is already existing one
			} else {

				// if existing is not overlapping
				if (!existing.isOverlapping()) {

					// if overlapping flight changed, abort updating
					// ---------------------------------------------------------
					// remove this section when adding/changing overlapping flight
					// for alreay existing flight is supported by inventory
					// ---------------------------------------------------------
					responce.setSuccess(false);
					responce.setResponseCode(ResponceCodes.INVALID_OVERLAPPING_FLIGHT_EXISTING_FLIGHT);
					// ---------------------------------------------------------
					// uncomment below for above action to work
					// this.checkOverlappsForNewOverlaps(updated, responce);

					// if existing is also overlapping
				} else {

					// if existing is overlapped with some other flight
					if (existing.getOverlapingFlightId().intValue() != updated.getOverlapingFlightId().intValue()) {

						responce.setSuccess(false);
						responce.setResponseCode(ResponceCodes.OVERLAPPING_ALREADY_OVERLAPPING_FLIGHT);

						// if overlapping shcedule has overlapped with some other flight
					} else if (updatedOverlapping.getOverlapingFlightId() != null
							&& updatedOverlapping.getOverlapingFlightId().intValue() != updated.getFlightId().intValue()) {

						responce.setSuccess(false);
						responce.setResponseCode(ResponceCodes.OVERLAPPING_ALREADY_OVERLAPPING_FLIGHT);

					} else {
						// basic checking for overlap is ok, have to check for segments
						boolean isSegsOk = false;

						Set<FlightSegement> segments = updated.getFlightSegements();
						Iterator<FlightSegement> itSegs = segments.iterator();

						while (itSegs.hasNext()) {

							FlightSegement segment = (FlightSegement) itSegs.next();

							Set<FlightSegement> overlapSegs = updatedOverlapping.getFlightSegements();
							Iterator<FlightSegement> itOverlapSegs = overlapSegs.iterator();

							while (itOverlapSegs.hasNext()) {

								FlightSegement overlapSeg = (FlightSegement) itOverlapSegs.next();
								if (overlapSeg.getSegmentCode().equals(segment.getSegmentCode())) {
									isSegsOk = true;
									break;
								}
							}
						}

						boolean isLegsOk = true;
						// [NOTE] if the dep/arr times of the overlapping leg is not allowed to change,
						// uncomment the following block to restrict it
						/*
						 * Set legs = updated.getFlightLegs(); Iterator itLegs = legs.iterator();
						 * 
						 * while(itLegs.hasNext()){
						 * 
						 * FlightLeg leg = (FlightLeg)itLegs.next();
						 * 
						 * Set overlapLegs = updatedOverlapping.getFlightLegs(); Iterator itOverlapLegs =
						 * overlapLegs.iterator();
						 * 
						 * while(itOverlapLegs.hasNext()){
						 * 
						 * FlightLeg overlapLeg = (FlightLeg)itOverlapLegs.next();
						 * if(overlapLeg.getOrigin().equals(leg.getOrigin()) &&
						 * overlapLeg.getDestination().equals(leg.getDestination()) ){
						 * 
						 * if(!(overlapLeg.getEstDepartureTimeZulu().getTime() ==
						 * leg.getEstDepartureTimeZulu().getTime() && overlapLeg.getEstArrivalTimeZulu().getTime() ==
						 * leg.getEstArrivalTimeZulu().getTime()) ){
						 * 
						 * isLegsOk = false; break; } } } }
						 */
						if (!isSegsOk || !isLegsOk) {

							responce.setSuccess(false);
							responce.setResponseCode(ResponceCodes.INVALID_OVERLAPPING_FLIGHT);
						}
					}

				}
			}

		}

		// return command responce
		return responce;
	}

	/**
	 * privte method to validate parameters
	 * 
	 * @param schedule
	 * @throws ModuleException
	 */
	private void checkParams(Flight flight) throws ModuleException {

		if (flight == null)// throw exception
			throw new ModuleException("airschedules.arg.invalid.null");
	}

	/**
	 * check method for overlaps
	 * 
	 * @param schedule
	 * @param responce
	 * @return
	 */
	private DefaultServiceResponse checkOverlappsForNewOverlaps(Flight updated, DefaultServiceResponse responce) {

		Collection<Flight> list = flightDAO.getPossibleOverlappingFlights(updated);
		Iterator<Flight> itPossibleOverlap = list.iterator();
		boolean foundOverlapping = false;

		while (itPossibleOverlap.hasNext()) {

			Flight overlappingFlight = (Flight) itPossibleOverlap.next();
			if (overlappingFlight.getFlightId().intValue() == updated.getOverlapingFlightId().intValue()) {
				foundOverlapping = true;
				break;
			}
		}

		if (!foundOverlapping) {

			responce.setSuccess(false);
			responce.setResponseCode(ResponceCodes.INVALID_OVERLAPPING_FLIGHT);
		}

		return responce;
	}
}
