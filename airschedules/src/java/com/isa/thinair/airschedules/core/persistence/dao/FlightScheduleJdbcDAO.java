/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:12:34
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.persistence.dao;

import java.util.Collection;
import java.util.Set;

import com.isa.thinair.airmaster.api.model.Gds;
import com.isa.thinair.airschedules.api.dto.EmailFlightSchedulesDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * BHive Commons: Air schedules
 * 
 * @author Duminda G
 */
public interface FlightScheduleJdbcDAO {

	public Collection<EmailFlightSchedulesDTO> PublishFlightSchedule(String fromDate, String toDate);
	
	public Set<Gds> getAutoPublishedGds(String origin, String destination);
	
	public void removeGdsAutopublishRoutes(String gdsId) throws ModuleException;
	
	public boolean showRollFwdButton(Integer flightId) throws ModuleException;

}
