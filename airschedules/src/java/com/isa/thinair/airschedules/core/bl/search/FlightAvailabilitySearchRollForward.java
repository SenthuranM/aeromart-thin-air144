package com.isa.thinair.airschedules.core.bl.search;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightRollForwardDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.RollForwardFlightAvailRS;
import com.isa.thinair.airinventory.api.dto.seatavailability.RollForwardFlightDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.RollForwardFlightSearchDTO;
import com.isa.thinair.airinventory.api.model.FCCSegBCInventory;
import com.isa.thinair.airinventory.api.util.BookingClassUtil;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airschedules.api.criteria.RollForwardFlightSearchCriteria;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.airschedules.core.persistence.dao.FlightDAO;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Command class for flight availability search for OHD roll forward
 * 
 * @author rumesh
 * @isa.module.command name="flightAvailabilitySearchRollForward"
 */
public class FlightAvailabilitySearchRollForward extends DefaultBaseCommand {

	private final FlightDAO flightDAO;
	private RollForwardFlightSearchDTO rollForwardFlightSearch;

	public FlightAvailabilitySearchRollForward() {
		// looking up daos
		flightDAO = (FlightDAO) AirSchedulesUtil.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_DAO);
	}

	@Override
	public ServiceResponce execute() throws ModuleException {

		rollForwardFlightSearch = (RollForwardFlightSearchDTO) this.getParameter(CommandParamNames.ROLL_FORWARD_FLIGHT_SEARCH);

		Map<Integer, List<AvailableFlightRollForwardDTO>> segSeqWiseAvailFlights = new HashMap<Integer, List<AvailableFlightRollForwardDTO>>();

		List<RollForwardFlightDTO> allFlights = new ArrayList<RollForwardFlightDTO>();
		for (List<RollForwardFlightDTO> ondFlights : rollForwardFlightSearch.getAllFltMap().values()) {
			if (ondFlights != null && ondFlights.size() > 0) {
				for (RollForwardFlightDTO flightDTO : ondFlights) {
					allFlights.add(flightDTO);
					List<AvailableFlightRollForwardDTO> tempList = getAvailFlightList(flightDTO, "N");
					if (tempList != null & tempList.size() > 0) {
						segSeqWiseAvailFlights.put(flightDTO.getSegmentSeq(), tempList);
					}
				}
			}
		}

		Map<Integer, List<AvailableFlightRollForwardDTO>> dateDiffWiseFltMap = new HashMap<Integer, List<AvailableFlightRollForwardDTO>>();
		setDefaultListForMap(dateDiffWiseFltMap);

		// Arrange flight segments based on source reservation
		for (RollForwardFlightDTO rollForwardFlightDTO : allFlights) {
			int segmentSeq = rollForwardFlightDTO.getSegmentSeq();
			List<AvailableFlightRollForwardDTO> fltList = segSeqWiseAvailFlights.get(segmentSeq);
			if (fltList != null && fltList.size() > 0) {
				for (AvailableFlightRollForwardDTO availFlight : fltList) {
					int dateDiff = CalendarUtil.getTimeDifferenceInDays(rollForwardFlightDTO.getDepartureDateTime(), availFlight
							.getFlightSegmentDTO().getDepartureDateTime());

					if (dateDiffWiseFltMap.get(dateDiff) == null) {
						dateDiffWiseFltMap.put(dateDiff, new ArrayList<AvailableFlightRollForwardDTO>());
					}

					dateDiffWiseFltMap.get(dateDiff).add(availFlight);
				}
			}
		}

		DefaultServiceResponse response = new DefaultServiceResponse(true);
		response.addResponceParam(ResponceCodes.ROLL_FORWARD_AVAILABILITY_SEARCH_SUCCESSFULL,
				prepareRollForwardAvailabilityResponse(dateDiffWiseFltMap, rollForwardFlightSearch.getBcType()));
		return response;
	}

	private void setDefaultListForMap(Map<Integer, List<AvailableFlightRollForwardDTO>> dateDiffWiseFltMap) {
		for (int i = rollForwardFlightSearch.getFirstDepartureSegStartGap(); i <= rollForwardFlightSearch
				.getFirstDepartureSegEndGap(); i++) {
			dateDiffWiseFltMap.put(i, new ArrayList<AvailableFlightRollForwardDTO>());
		}
	}

	private List<RollForwardFlightAvailRS> prepareRollForwardAvailabilityResponse(
			Map<Integer, List<AvailableFlightRollForwardDTO>> dateDiffWiseFltMap, String bcType) {
		List<RollForwardFlightAvailRS> availResponse = new ArrayList<RollForwardFlightAvailRS>();
		Date sourceDepartureDateTime = getFirstSegDepartureDate();

		if (dateDiffWiseFltMap != null) {
			for (Entry<Integer, List<AvailableFlightRollForwardDTO>> entry : dateDiffWiseFltMap.entrySet()) {
				List<AvailableFlightRollForwardDTO> fltList = entry.getValue();

				RollForwardFlightAvailRS availRS = new RollForwardFlightAvailRS();
				if (fltList != null && fltList.size() > 0) {
					if (isFlightRollForwardable(fltList, bcType)) {
						Collections.sort(fltList);
						AvailableFlightRollForwardDTO firstAvailFltSeg = BeanUtils.getFirstElement(fltList);

						availRS.setRollForwardEnabled(true);
						availRS.setDepartureDate(firstAvailFltSeg.getFlightSegmentDTO().getDepartureDateTime());

						for (AvailableFlightRollForwardDTO availFlt : fltList) {
							availRS.getRollForwardFlights().add(populateRollForwardFlightDTO(availFlt));
						}
					} else {
						availRS.setDepartureDate(CalendarUtil.add(sourceDepartureDateTime, 0, 0, entry.getKey(), 0, 0, 0));
					}
				} else {
					availRS.setDepartureDate(CalendarUtil.add(sourceDepartureDateTime, 0, 0, entry.getKey(), 0, 0, 0));
				}

				availResponse.add(availRS);

			}
		}

		return availResponse;
	}

	private RollForwardFlightDTO populateRollForwardFlightDTO(AvailableFlightRollForwardDTO availFlt) {
		RollForwardFlightDTO rollForwardFlightDTO = new RollForwardFlightDTO();
		rollForwardFlightDTO.setBookingClass(availFlt.getBookingClasscode());
		rollForwardFlightDTO.setCabinClass(availFlt.getCabinClassCode());
		rollForwardFlightDTO.setDepartureDateTime(availFlt.getFlightSegmentDTO().getDepartureDateTime());
		rollForwardFlightDTO.setDepartureDateTimeZulu(availFlt.getFlightSegmentDTO().getDepartureDateTimeZulu());
		rollForwardFlightDTO.setFareId(availFlt.getFareId());
		rollForwardFlightDTO.setFareRuleId(availFlt.getFareRuleId());
		rollForwardFlightDTO.setFlightSegId(availFlt.getFlightSegmentDTO().getSegmentId());
		rollForwardFlightDTO.setSegmentCode(availFlt.getFlightSegmentDTO().getSegmentCode());
		rollForwardFlightDTO.setSegmentSeq(availFlt.getSegmentSeq());
		rollForwardFlightDTO.setReturnFlag(availFlt.getReturnflag());
		rollForwardFlightDTO.setDomesticFlight(availFlt.getFlightSegmentDTO().isDomesticFlight());
		rollForwardFlightDTO.setOpenReturn(availFlt.isOpenReturn());
		return rollForwardFlightDTO;
	}

	private boolean isFlightRollForwardable(List<AvailableFlightRollForwardDTO> fltList, String bcType) {
		int sourceSegCount = 0;
		for (List<RollForwardFlightDTO> entry : rollForwardFlightSearch.getAllFltMap().values()) {
			sourceSegCount += entry.size();
		}
		Collection<String> bcTypes = new ArrayList<String>();
		bcTypes.add(bcType);
		if (fltList != null && fltList.size() > 0) {
			if (fltList.size() == sourceSegCount) {
				int adultCount = rollForwardFlightSearch.getAdultCount() + rollForwardFlightSearch.getChildCount();
				for (AvailableFlightRollForwardDTO availFlt : fltList) {
					if (!BookingClassUtil.byPassSegInvUpdateForOverbook(bcTypes)) {
						if (!(availFlt.getAvailableAdultSeatForSeg() >= adultCount
								&& availFlt.getAvailableInfantSeatForSeg() >= rollForwardFlightSearch.getInfantCount() && availFlt
								.getAvailableAdultSeatsForBC() >= adultCount)) {
							return false;
						}
						if (ReservationInternalConstants.BookingClassAllocStatus.CLOSE.equals(availFlt.getBcAllocStatus())) {
							return false;
						}
					} else {
						if (!(availFlt.getAvailableInfantSeatForSeg() >= rollForwardFlightSearch.getInfantCount())) {
							return false;
						}
						if (ReservationInternalConstants.BookingClassAllocStatus.CLOSE.equals(availFlt.getBcAllocStatus())
								&& FCCSegBCInventory.StatusAction.MANUAL.equals(availFlt.getStatusChgAction())) {
							return false;
						}
					}
				}
			} else {
				return false;
			}
		} else {
			return false;
		}

		return true;
	}

	private List<AvailableFlightRollForwardDTO> getAvailFlightList(RollForwardFlightDTO flightDTO, String returnFlag) {
		RollForwardFlightSearchCriteria criteria = new RollForwardFlightSearchCriteria();

		criteria.setBookingClassCode(flightDTO.getBookingClass());
		criteria.setCabinClassCode(flightDTO.getCabinClass());
		criteria.setFareId(flightDTO.getFareId());
		criteria.setFareRuleId(flightDTO.getFareRuleId());
		criteria.setFlightSegId(flightDTO.getFlightSegId());
		criteria.setSegmentCode(flightDTO.getSegmentCode());
		criteria.setSegmentSeq(flightDTO.getSegmentSeq());
		criteria.setStartDate(addAndFormatDate(flightDTO.getDepartureDateTime(),
				rollForwardFlightSearch.getFirstDepartureSegStartGap()));
		criteria.setEndDate(addAndFormatDate(flightDTO.getDepartureDateTime(),
				rollForwardFlightSearch.getFirstDepartureSegEndGap()));
		criteria.setReturnFlag(returnFlag);
		criteria.setOpenRerurn(flightDTO.isOpenReturn());
		criteria.setAgentCode(rollForwardFlightSearch.getAgentCode());

		List<AvailableFlightRollForwardDTO> availableFlightSegments = flightDAO.availableFlightSegments(criteria);
		if (flightDTO.isOpenReturn() && availableFlightSegments != null) {
			for (AvailableFlightRollForwardDTO availableFlightRollForwardDTO : availableFlightSegments) {
				availableFlightRollForwardDTO.setOpenReturn(true);
			}
		}
		return availableFlightSegments;
	}

	private String addAndFormatDate(Date dt, int dateGap) {
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(dt);
		cal.add(GregorianCalendar.DATE, dateGap);

		String pattern = "MMM dd HH:mm:ss yyyy";
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);

		return sdf.format(cal.getTime());
	}

	private Date getFirstSegDepartureDate() {
		List<RollForwardFlightDTO> sourceOBflights = rollForwardFlightSearch.getAllFltMap().get(0);
		Collections.sort(sourceOBflights);
		RollForwardFlightDTO firstSourceFlt = BeanUtils.getFirstElement(sourceOBflights);

		return firstSourceFlt.getDepartureDateTime();
	}

}
