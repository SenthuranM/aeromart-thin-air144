/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.search;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableConnectedFlight;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableIBOBFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OriginDestinationInfoDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SelectedFlightDTO;
import com.isa.thinair.airinventory.api.util.FareQuoteUtil;
import com.isa.thinair.airschedules.api.dto.AvailableFlightSearchCriteria;
import com.isa.thinair.airschedules.api.dto.ConnectedAvailableFlightSegmentsDTO;
import com.isa.thinair.airschedules.api.dto.RouteInfoTO;
import com.isa.thinair.airschedules.api.dto.TransitAirport;
import com.isa.thinair.airschedules.api.utils.AirScheduleCustomConstants;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.FlightUtil;
import com.isa.thinair.airschedules.api.utils.LocalZuluTimeAdder;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.airschedules.core.persistence.dao.FlightDAO;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.commons.api.dto.InterlinedAirLineTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.util.OndCombinationUtil;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for the flight avilability search
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="flightAvailabilitySearch"
 */
public class FlightAvailabilitySearch extends DefaultBaseCommand {
	private final Log log = LogFactory.getLog(getClass());
	// Dao's
	private final FlightDAO flightDAO;

	private LocalZuluTimeAdder localZuluTimeAdder = null;

	private AvailableFlightSearchDTO _avlDTO = null;
	private SEARCH_TYPE availabilitySearchFor;
	private long reservationCutOffTimeForInternational;
	private long reservationCutOffTimeForDomestic;
	private Map<String, Date> cutoverTimesForInterlining;
	private AvailableFlightSearchCriteria _defaultFltSearchCriteria;

	private static enum OPERATION_FILTER {
		AT_LEAST_ONE_AIR_SEGMENT, ANY
	};

	private static enum SEARCH_TYPE {
		NEW, MOD, ADD, WS_BUS_SEGMENT_ONLY
	};

	/**
	 * constructor of the FlightAvailabilitySearch command
	 */
	public FlightAvailabilitySearch() {

		// looking up daos
		flightDAO = (FlightDAO) AirSchedulesUtil.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_DAO);
		localZuluTimeAdder = new LocalZuluTimeAdder(AirSchedulesUtil.getAirportBD());
	}

	/**
	 * execute method of the FlightAvailabilitySearch command
	 * 
	 * @throws ModuleException
	 */
	@Override
	public ServiceResponce execute() throws ModuleException {

		SEARCH_TYPE availabilitySearchFor = null;

		this._avlDTO = (AvailableFlightSearchDTO) this.getParameter(CommandParamNames.AVILABLE_FLIGHT_SEARCH_DTO);

		// TODO charith : possibly we may able to remove this parameter
		Boolean searchOutBoundFlights = (Boolean) this.getParameter(CommandParamNames.SEARCH_OUT_BOUND_FLIGHTS);

		this.checkParams(_avlDTO, searchOutBoundFlights);

		this.setSearchType(searchOutBoundFlights);

		this.setCutoffTimes();

		// construct the available flight dto
		AvailableFlightDTO availableFlightDTO = new AvailableFlightDTO();
		SelectedFlightDTO selectedFlightDTO = new SelectedFlightDTO();
		// two types of flights can be identified matching with this search
		// 1) outboundFlights - out going flights
		// 2) inboundFlights - return flights

		// search for out going flights in this case,
		
		int ondSequence = 0;
		for (OriginDestinationInfoDTO ondInfo : _avlDTO.getOrderedOriginDestinations()) {
			List<AvailableIBOBFlightSegment> availableOndFlights = null;
			Collection<AvailableIBOBFlightSegment> selectedDateOndFlights = null;
			boolean isSelectedFlightsAlreadyUpdated = false;
			Collection<Integer> fltSegmentIds = ondInfo.getFlightSegmentIds();

			if (ondInfo.isSpecificFlightsAvailability() && fltSegmentIds != null && fltSegmentIds.size() > 0) {
				availableOndFlights = getAvailableOndFlightsForSelectedFlights(ondInfo, ondSequence);
				selectedDateOndFlights = availableOndFlights;
			} else {
				AvailableIBOBFlightSegment obOndFlightSegment = null;
				if (availableFlightDTO.getAvailableOndFlights(0).size() > 0) {
					obOndFlightSegment = availableFlightDTO.getAvailableOndFlights(0).get(0);
				}

				Collection<OriginDestinationInfoDTO> possibleOndDTOs = populateOriginDestinationCollectionForCityBasedSearch(ondInfo);

				if (!possibleOndDTOs.isEmpty() && possibleOndDTOs.size() > 1) {
					for (OriginDestinationInfoDTO clonedOnd : possibleOndDTOs) {
						availableOndFlights = getAvailableOndFlightsForCriteria(clonedOnd, ondSequence, obOndFlightSegment);
						selectedDateOndFlights = getSelectedDateAvailableFlights(availableOndFlights,
								clonedOnd.getPreferredDateTimeStart(), clonedOnd.getPreferredDateTimeEnd(),
								clonedOnd.isOpenOnd());

						if (availableOndFlights != null) {
							isSelectedFlightsAlreadyUpdated = true;
							availableFlightDTO.getAvailableOndFlights(ondSequence).addAll(availableOndFlights);
							Collections.sort(availableFlightDTO.getAvailableOndFlights(ondSequence));
							if (selectedDateOndFlights != null) {
								selectedFlightDTO.getOndWiseSelectedDateAllFlights(ondSequence).addAll(selectedDateOndFlights);
							}
						}

					}
				} else {
					availableOndFlights = getAvailableOndFlightsForCriteria(ondInfo, ondSequence, obOndFlightSegment);
					selectedDateOndFlights = getSelectedDateAvailableFlights(availableOndFlights,
							ondInfo.getPreferredDateTimeStart(), ondInfo.getPreferredDateTimeEnd(), ondInfo.isOpenOnd());
				}
			}
			if (!isSelectedFlightsAlreadyUpdated && availableOndFlights != null) {
				availableFlightDTO.getAvailableOndFlights(ondSequence).addAll(availableOndFlights);
				Collections.sort(availableFlightDTO.getAvailableOndFlights(ondSequence));
				if (selectedDateOndFlights != null) {
					selectedFlightDTO.getOndWiseSelectedDateAllFlights(ondSequence).addAll(selectedDateOndFlights);
				}
			}
			selectedFlightDTO.setOpenReturn(_avlDTO.isOpenReturnSearch() && ondInfo.isOpenOnd());
			ondSequence++;
		}

		DefaultServiceResponse responce = new DefaultServiceResponse(true);
		if (!selectedFlightDTO.getSelectedDateAllFlights().isEmpty()) {
			availableFlightDTO.setSelectedFlight(selectedFlightDTO);
		}
		responce.addResponceParam(ResponceCodes.AVAILABILITY_SEARCH_SUCCESSFULL, availableFlightDTO);
		return responce;
	}

	private int getFlightSearchGap() {
		return _avlDTO.getFlightSearchGap() != null ? _avlDTO.getFlightSearchGap() : -3;
	}

	private List<AvailableIBOBFlightSegment> getAvailableOndFlightsForCriteria(OriginDestinationInfoDTO ondInfo, int ondSequence,
			AvailableIBOBFlightSegment obSegment) throws ModuleException {
		Collection<AvailableFlightSegment> directONDSegments = new ArrayList<AvailableFlightSegment>();
		Set<AvailableConnectedFlight> connectedONDSegments = new HashSet<AvailableConnectedFlight>();
		List<RouteInfoTO> availableONDRoutes = flightDAO.getAvailableRoutes(ondInfo.getOrigin(), ondInfo.getDestination(),
				_avlDTO.getFlightsPerOndRestriction());
		AvailableFlightSearchCriteria fltSearchCriteria = getFreshFlightSearchCriteria();
		fltSearchCriteria.setOndSequence(ondSequence);
		Set<String> ondCodeSet = new HashSet<String>();
		// Availability search we have either single cabin class or single logical cabin class selected. We will get
		// multiple selection only in fare quote in xbe
		if (AppSysParamsUtil.isEnableRouteSelectionForAgents() && _avlDTO.getAgentCode() != null) {
			availableONDRoutes = getAgentWiseApplicableRoutes(_avlDTO.getAgentCode(), availableONDRoutes);
		}	
		String cabinClassCode = null;
		String logicalCabinClassCode = null;
		cabinClassCode = ondInfo.getPreferredClassOfService();
		logicalCabinClassCode = ondInfo.getPreferredLogicalCabin();
		for (RouteInfoTO availableRoute : availableONDRoutes) {
			boolean isFirstIBSearch = true;
			if (availableRoute.isDirectRoute()) {
				if (_avlDTO.isOpenReturnSearch() && ondInfo.isOpenOnd()) {
					// TODO Please revise this implementation.
					Date minDate = getMaxDate(ondInfo.getDepartureDateTimeStart(), 0, true);
					Date maxDate = getMaxDate(
							_avlDTO.getReturnValidityPeriodTO().getDerivedBoundaryDate(ondInfo.getDepartureDateTimeStart()), 0,
							true);
					if (obSegment != null) {
						Date deptTime = obSegment.getFlightSegmentDTOs().get(0).getDepartureDateTimeZulu();
						Date originalMaxDate = maxDate;
						Date maxDateZulu = null;
						if (isFirstIBSearch) {
							maxDateZulu = _avlDTO.getReturnValidityPeriodTO().getDerivedBoundaryDate(deptTime);
							maxDate = maxDateZulu;
						}
						Date tmp = maxDate;
						boolean lastRecordReached = false;

						while (directONDSegments == null || directONDSegments.size() == 0) {
							if (isFirstIBSearch) {
								tmp = CalendarUtil.getStartTimeOfDate(tmp);
							} else {
								tmp = CalendarUtil.add(tmp, 0, 0, getFlightSearchGap(), 0, 0, 0);
							}
							if (tmp.compareTo(minDate) <= 0) {
								tmp = minDate;
								lastRecordReached = true;
							}
							fltSearchCriteria.setOriginDestination(ondInfo.getOrigin(), ondInfo.getDestination());
							fltSearchCriteria.setDates(tmp, maxDate, null, null, maxDateZulu);
							fltSearchCriteria.setCabins(cabinClassCode, logicalCabinClassCode);
							fltSearchCriteria.setIncludeOnlyMarketingCarrierSegs(true);
							fltSearchCriteria.setInboundFlight(true);

							directONDSegments = flightDAO.getDirectFlightSegments(fltSearchCriteria);
							if (isFirstIBSearch) {
								maxDate = CalendarUtil.add(originalMaxDate, 0, 0, -1, 0, 0, 0);
								tmp = maxDate;
							}
							isFirstIBSearch = false;
							maxDateZulu = null;
							if (lastRecordReached) {
								break;
							}
						}
					}
				} else {
					// Direct route
					// Can directly assign this because we have only one direct route for given origin and
					// destination
					// but we may have multiple flight for this origin and destination
					fltSearchCriteria.setOriginDestination(ondInfo.getOrigin(), ondInfo.getDestination());
					fltSearchCriteria.setDates(ondInfo.getDepartureDateTimeStart(), ondInfo.getDepartureDateTimeEnd(),
							ondInfo.getArrivalDateTimeStart(), ondInfo.getArrivalDateTimeEnd(), null);
					fltSearchCriteria.setCabins(cabinClassCode, logicalCabinClassCode);
					fltSearchCriteria.setIncludeOnlyMarketingCarrierSegs(true);
					fltSearchCriteria.setInboundFlight(_avlDTO.isReturnOnd(ondSequence));

					directONDSegments = flightDAO.getDirectFlightSegments(fltSearchCriteria);
				}
			} else {
				// Route(s) via one or more hub airports

				if (_avlDTO.isOpenReturnSearch() && ondInfo.isOpenOnd()) {
					// TODO Please revise this implementation.
					Date minDate = getMaxDate(ondInfo.getDepartureDateTimeStart(), 0, true);
					Date maxDate = getMaxDate(
							_avlDTO.getReturnValidityPeriodTO().getDerivedBoundaryDate(ondInfo.getDepartureDateTimeStart()), 0,
							true);
					Date originalMaxDate = maxDate;
					Date maxDateZulu = null;
					if (obSegment != null) {
						Date deptTime = obSegment.getFlightSegmentDTOs().get(0).getDepartureDateTimeZulu();
						originalMaxDate = maxDate;
						if (isFirstIBSearch) {
							maxDate = _avlDTO.getReturnValidityPeriodTO().getDerivedBoundaryDate(deptTime);
							maxDateZulu = maxDate;
						}
					}
					Date tmp = maxDate;
					boolean lastRecordReached = false;
					while (!ondCodeSet.contains(availableRoute.getOndCode())) {
						if (isFirstIBSearch) {
							tmp = CalendarUtil.getStartTimeOfDate(tmp);
						} else {
							tmp = CalendarUtil.add(tmp, 0, 0, getFlightSearchGap(), 0, 0, 0);
						}
						if (tmp.compareTo(minDate) <= 0) {
							tmp = minDate;
							lastRecordReached = true;
						}
						List<AvailableConnectedFlight> connectedAvailableFlightSegmentsList = this
								.getAvailableConnectedFlightSegments(tmp, maxDate, ondInfo.getOrigin(), ondInfo.getDestination(),
										availableRoute.getTransitAirportCodesList(), cabinClassCode, logicalCabinClassCode, true,
										ondSequence, ondInfo.getHubTimeDetailMap(), maxDateZulu);
						if (connectedAvailableFlightSegmentsList != null && connectedAvailableFlightSegmentsList.size() > 0) {
							ondCodeSet.add(availableRoute.getOndCode());
						}
						connectedONDSegments.addAll(connectedAvailableFlightSegmentsList);
						if (isFirstIBSearch && originalMaxDate != null) {
							maxDate = CalendarUtil.add(originalMaxDate, 0, 0, -1, 0, 0, 0);
							tmp = maxDate;
							maxDateZulu = null;
						}
						isFirstIBSearch = false;
						if (lastRecordReached) {
							break;
						}
					}
				} else {
					connectedONDSegments.addAll(this.getAvailableConnectedFlightSegments(ondInfo.getDepartureDateTimeStart(),
							ondInfo.getDepartureDateTimeEnd(), ondInfo.getOrigin(), ondInfo.getDestination(),
							availableRoute.getTransitAirportCodesList(), cabinClassCode, logicalCabinClassCode, false,
							ondSequence, ondInfo.getHubTimeDetailMap(), null));
				}
			}
		}

		SEARCH_TYPE availabilitySearchFor = null;
		// Surface segment filtering
		if (availabilitySearchFor != SEARCH_TYPE.WS_BUS_SEGMENT_ONLY || availabilitySearchFor == SEARCH_TYPE.ADD
				|| availabilitySearchFor == SEARCH_TYPE.NEW || availabilitySearchFor == SEARCH_TYPE.MOD) {
			filterSurfaceSegments(directONDSegments, connectedONDSegments, availabilitySearchFor);
		}

		List<AvailableIBOBFlightSegment> availableOndFlights = new ArrayList<AvailableIBOBFlightSegment>();
		availableOndFlights.addAll(directONDSegments);
		availableOndFlights.addAll(connectedONDSegments);
		return availableOndFlights;
	}

	private List<AvailableIBOBFlightSegment> getAvailableOndFlightsForSelectedFlights(OriginDestinationInfoDTO ondInfo,
			int ondSequence) throws ModuleException {
		Map<Integer, Boolean> ondSequenceReturnMap = _avlDTO.getOndSequenceReturnMap();
		boolean isIBFlight = false;

		if (ondSequenceReturnMap != null && ondSequenceReturnMap.size() > 1 && ondSequenceReturnMap.get(ondSequence) != null) {
			isIBFlight = ondSequenceReturnMap.get(ondSequence);
		}
		List<AvailableIBOBFlightSegment> availableOndFlights = FlightUtil.getAvailableOBIBFlights(ondInfo.getFlightSegmentIds(),
				ondInfo.getPreferredClassOfService(), ondSequence, isIBFlight, ondInfo.isFlownOnd(), ondInfo.isUnTouchedOnd(), null);
		return availableOndFlights;

	}

	private AvailableFlightSearchCriteria getFreshFlightSearchCriteria() {
		if (this._defaultFltSearchCriteria == null) {
			this._defaultFltSearchCriteria = new AvailableFlightSearchCriteria();
		}
		_defaultFltSearchCriteria.setAdults(_avlDTO.getAdultCount());
		_defaultFltSearchCriteria.setChilds(_avlDTO.getChildCount());
		_defaultFltSearchCriteria.setInfants(_avlDTO.getInfantCount());
		_defaultFltSearchCriteria.setAvailabilityRestriction(_avlDTO.getAvailabilityRestrictionLevel());
		_defaultFltSearchCriteria.setOnlyInterline(_avlDTO.isSearchOnlyInterlineFlights());
		_defaultFltSearchCriteria.setInterline(_avlDTO.isSearchInitiatedFromInterline());
		_defaultFltSearchCriteria.setExternalBookingClasses(_avlDTO.getExternalBookingClasses());
		_defaultFltSearchCriteria.setCutOffTimeForInternational(reservationCutOffTimeForInternational);
		_defaultFltSearchCriteria.setCutOffTimeForDomestic(reservationCutOffTimeForDomestic);
		_defaultFltSearchCriteria.setMultiCitySearch(_avlDTO.isMultiCitySearch());
		_defaultFltSearchCriteria.setAllowFlightSearchAfterCutOffTime(_avlDTO.isAllowFlightSearchAfterCutOffTime());
		_defaultFltSearchCriteria.setAllowDoOverbookAfterCutOffTime(_avlDTO.isAllowDoOverbookAfterCutOffTime());
		_defaultFltSearchCriteria.setAllowDoOverbookBeforeCutOffTime(_avlDTO.isAllowDoOverbookBeforeCutOffTime());
		_defaultFltSearchCriteria.setAppIndicator(_avlDTO.getAppIndicator());
		_defaultFltSearchCriteria.setModifyBooking(_avlDTO.isModifyBooking());
		_defaultFltSearchCriteria.setChannelCode(_avlDTO.getChannelCode());
		_defaultFltSearchCriteria.setSourceFlightInCutOffTime(_avlDTO.isSourceFlightInCutOffTime());
		_defaultFltSearchCriteria.setOpenReturn(_avlDTO.isOpenReturnSearch());
		_defaultFltSearchCriteria.setAgentCode(_avlDTO.getAgentCode());

		return _defaultFltSearchCriteria;
	}

	private void setCutoffTimes() throws ModuleException {
		Date currentZuluTime = CalendarUtil.getCurrentSystemTimeInZulu();
		Date fromAirportLocalTime = localZuluTimeAdder.getLocalDateTime(_avlDTO.getFirstDepartingAirport(), currentZuluTime);

		this.reservationCutOffTimeForInternational = 0;
		this.reservationCutOffTimeForDomestic = 0;
		if (_avlDTO.getChannelCode() == SalesChannelsUtil.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_WEB_KEY)) {
			reservationCutOffTimeForInternational = AppSysParamsUtil.getPublicReservationCutoverInMillis();
			reservationCutOffTimeForDomestic = AppSysParamsUtil.getPublicReservationCutoverInMillisForDomestic();
		} else if (_avlDTO.getChannelCode() == SalesChannelsUtil
				.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_TRAVELAGNET_KEY)) {
			reservationCutOffTimeForInternational = AppSysParamsUtil.getAgentReservationCutoverInMillis();
			reservationCutOffTimeForDomestic = AppSysParamsUtil.getReservationCutoverForDomesticInMillis();
		} else if (_avlDTO.getChannelCode() == SalesChannelsUtil
				.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_APIAGENCIES_KEY)) {
			reservationCutOffTimeForInternational = AppSysParamsUtil.getAPIReservationCutoverForInternationalInMillis();
			reservationCutOffTimeForDomestic = AppSysParamsUtil.getAPIReservationCutoverForDomesticInMillis();
		}

		this.cutoverTimesForInterlining = getEffectiveFromDateZuluForInterlining(currentZuluTime, CommonsServices
				.getGlobalConfig().getActiveInterlinedCarriersMap());

		if (log.isDebugEnabled()) {
			log.debug("Current system time in zulu =	" + currentZuluTime + "\n\r Current local time in "
					+ _avlDTO.getFirstDepartingAirport() + " airport	= " + fromAirportLocalTime + "\n\r");
		}
	}

	private void setSearchType(Boolean searchOutBoundFlights) {
		if (_avlDTO.getExistingFlightSegments() == null
				&& (_avlDTO.getModifiedFlightSegments() == null || _avlDTO.getModifiedFlightSegments().isEmpty())
				&& _avlDTO.isGroundSegmentAvailability()) {
			this.availabilitySearchFor = SEARCH_TYPE.WS_BUS_SEGMENT_ONLY;
		} else if (_avlDTO.getExistingFlightSegments() == null
				&& (_avlDTO.getModifiedFlightSegments() == null || _avlDTO.getModifiedFlightSegments().isEmpty())) {
			this.availabilitySearchFor = SEARCH_TYPE.NEW;
		} else if (_avlDTO.getExistingFlightSegments() != null
				&& (_avlDTO.getModifiedFlightSegments() == null || _avlDTO.getModifiedFlightSegments().isEmpty())) {
			this.availabilitySearchFor = SEARCH_TYPE.ADD;
		} else if (_avlDTO.getExistingFlightSegments() != null && _avlDTO.getModifiedFlightSegments() != null
				&& !_avlDTO.getModifiedFlightSegments().isEmpty()) {
			this.availabilitySearchFor = SEARCH_TYPE.MOD;
		}

	}

	private String overridClassOfService(Map<String, List<Integer>> cosSelection, Collection<Integer> fltSegIds) {
		// Since All outbound/inbound flight segment will have same logical cabin class, take first flight segment Id
		Integer fltSegId = fltSegIds.iterator().next();
		return FareQuoteUtil.getClassOfserviceFromFlightSegId(cosSelection, fltSegId);
	}

	private void filterSurfaceSegments(Collection<AvailableFlightSegment> directOutBoundSegments,
			Collection<AvailableConnectedFlight> connectedOutBoundSegments, SEARCH_TYPE searchType) {
		if (searchType == SEARCH_TYPE.NEW) {
			removeFilteredSurfaceSegments(directOutBoundSegments);
		}
		filterConnectedFlights(connectedOutBoundSegments, OPERATION_FILTER.AT_LEAST_ONE_AIR_SEGMENT);
	}

	private void filterConnectedFlights(Collection<AvailableConnectedFlight> connectedOutBoundSegments,
			OPERATION_FILTER operation_filter) {
		if (operation_filter == OPERATION_FILTER.AT_LEAST_ONE_AIR_SEGMENT) {
			Iterator<AvailableConnectedFlight> itrConnectedFlights = connectedOutBoundSegments.iterator();
			while (itrConnectedFlights.hasNext()) {
				AvailableConnectedFlight availableConnectedFlight = itrConnectedFlights.next();
				Iterator<AvailableFlightSegment> itrAvailableConnectedSegments = availableConnectedFlight
						.getAvailableFlightSegments().iterator();
				boolean hasAirSegment = false;
				while (!hasAirSegment) {
					AvailableFlightSegment availableFlightSegment = itrAvailableConnectedSegments.next();
					if (availableFlightSegment.getFlightSegmentDTO().getOperationTypeID() != AirScheduleCustomConstants.OperationTypes.BUS_SERVICE) {
						hasAirSegment = true;
					}
				}
				// Still any segment doesnt have air portion remove flight
				if (hasAirSegment == false) {
					itrConnectedFlights.remove();
				}
			}
		}

	}

	private void removeFilteredSurfaceSegments(Collection<AvailableFlightSegment> availableFlightSegments) {
		Iterator<AvailableFlightSegment> itrAvailableFlightSeg = availableFlightSegments.iterator();

		while (itrAvailableFlightSeg.hasNext()) {
			AvailableFlightSegment availableFlightSegment = itrAvailableFlightSeg.next();
			if (availableFlightSegment.getFlightSegmentDTO().getOperationTypeID() == AirScheduleCustomConstants.OperationTypes.BUS_SERVICE) {
				itrAvailableFlightSeg.remove();
			}
		}
	}

	private Map<String, Date> getEffectiveFromDateZuluForInterlining(Date currentSysTimeInZulu,
			Map<String, InterlinedAirLineTO> interlinedAirlinesMap) {
		Map<String, Date> effectiveFromDatesZuluMap = new HashMap<String, Date>();

		for (String carrierCode : interlinedAirlinesMap.keySet()) {
			InterlinedAirLineTO interlinedAirLineTO = interlinedAirlinesMap.get(carrierCode);

			int interlinedCutover = interlinedAirLineTO.getInterlineCutOverInMinutes();

			GregorianCalendar calander = new GregorianCalendar();
			calander.setTime(currentSysTimeInZulu);
			calander.add(GregorianCalendar.MINUTE, interlinedCutover);

			effectiveFromDatesZuluMap.put(carrierCode, calander.getTime());
		}

		return effectiveFromDatesZuluMap;
	}

	/**
	 * Privte method to validate parameters
	 * 
	 * @param avifltSearchDTO
	 * @param searchType
	 * @param searchOutBoundFlights
	 * @param searchInBoundFlights
	 * @throws ModuleException
	 */
	private void checkParams(AvailableFlightSearchDTO avifltSearchDTO, Boolean searchOutBoundFlights) throws ModuleException {

		if (avifltSearchDTO == null || searchOutBoundFlights == null) {
			throw new ModuleException("airschedules.arg.invalid.null");
		}
	}

	/**
	 * For searching availability for routes consists of 2 or more number of flights.
	 * 
	 * @param departureStartTime
	 * @param departureEndTime
	 * @param origin
	 * @param destination
	 * @param stopOverAirports
	 * @param cabinClass
	 * @param ondSequence
	 * @param maxDateZulu
	 *            TODO
	 * @param returnDestination
	 * @param returnOrigin
	 * @param date
	 * @param variation
	 * @param searchType
	 * @param adults
	 * @param infants
	 * @param availabilityRestriction
	 * 
	 * @return List of {@link AvailableConnectedFlight}
	 * @throws ModuleException
	 */
	private List<AvailableConnectedFlight> getAvailableConnectedFlightSegments(Date departureStartTime, Date departureEndTime,
			String origin, String destination, List<TransitAirport> stopOverAirports, String cabinClass,
			String logicalCabinClass, boolean isInboundFlight, int ondSequence, Map<String, Integer> hubTimeDetails,
			Date maxDateZulu) throws ModuleException {

		List<ConnectedAvailableFlightSegmentsDTO> availableConnectedFlights = new ArrayList<ConnectedAvailableFlightSegmentsDTO>();
		List<ConnectedAvailableFlightSegmentsDTO> availableConnectedFlightsUpToTransit = new ArrayList<ConnectedAvailableFlightSegmentsDTO>();

		AvailableFlightSearchCriteria fltSearchCriteria = getFreshFlightSearchCriteria();
		fltSearchCriteria.setOndSequence(ondSequence);
		boolean enableUserDefineTransit = AppSysParamsUtil.isEnableUserDefineTransitTime();

		if (stopOverAirports != null && stopOverAirports.size() > 0) {
			for (int segmentSequence = 0; segmentSequence <= stopOverAirports.size(); ++segmentSequence) {
				String transitAirport = null;
				if (segmentSequence == 0) {
					transitAirport = stopOverAirports.get(segmentSequence).getAirportCode();
				} else if (stopOverAirports.get(segmentSequence - 1).isOrigin()) {
					origin = stopOverAirports.get(segmentSequence - 1).getAirportCode();
					if (segmentSequence == stopOverAirports.size()) {
						transitAirport = destination;
					} else {
						transitAirport = stopOverAirports.get(segmentSequence).getAirportCode();
					}

					if (enableUserDefineTransit && hubTimeDetails != null && hubTimeDetails.size() > 0
							&& hubTimeDetails.get(origin) != null) {
						Integer noOfDays = hubTimeDetails.get(origin);
						if (noOfDays.intValue() != 0) {
							Date tmpDepartureStartTime = CalendarUtil.getStartTimeOfDate(CalendarUtil.getOfssetAddedTime(
									departureStartTime, noOfDays));
							Date tmpDepartureEndTime = CalendarUtil.getEndTimeOfDate(CalendarUtil.getOfssetAddedTime(
									departureEndTime, noOfDays));

							// If user define time within standard transit time, should get standard time
							String[] minMaxTransitDurations = CommonsServices.getGlobalConfig().getMixMaxTransitDurations(origin,
									null, null);

							departureStartTime = this.getMinTransitTime(departureStartTime, minMaxTransitDurations[0]);
							departureEndTime = this.getMaxTransitTime(departureEndTime, minMaxTransitDurations[1]);

							if (tmpDepartureStartTime.after(departureStartTime)) {
								departureStartTime = tmpDepartureStartTime;
							}

							if (tmpDepartureEndTime.after(departureEndTime)) {
								departureEndTime = tmpDepartureEndTime;
							}

						} else {
							String[] minMaxTransitDurations = CommonsServices.getGlobalConfig().getMixMaxTransitDurations(origin,
									null, null);

							departureStartTime = this.getMinTransitTime(departureStartTime, minMaxTransitDurations[0]);
							departureEndTime = this.getMaxTransitTime(departureEndTime, minMaxTransitDurations[1]);
						}

					} else {
						String[] minMaxTransitDurations = CommonsServices.getGlobalConfig().getMixMaxTransitDurations(origin,
								null, null);

						departureStartTime = this.getMinTransitTime(departureStartTime, minMaxTransitDurations[0]);
						departureEndTime = this.getMaxTransitTime(departureEndTime, minMaxTransitDurations[1]);
					}

				} else {
					continue;
				}
				fltSearchCriteria.setOriginDestination(origin, transitAirport);
				fltSearchCriteria.setDates(departureStartTime, departureEndTime, null, null, maxDateZulu);
				fltSearchCriteria.setCabins(cabinClass, logicalCabinClass);
				fltSearchCriteria.setIncludeOnlyMarketingCarrierSegs(false);
				fltSearchCriteria.setInboundFlight(isInboundFlight);

				List<AvailableFlightSegment> currentDirectFlightSegments = flightDAO.getDirectFlightSegments(fltSearchCriteria);

				if (currentDirectFlightSegments.isEmpty()) {
					return new ArrayList<AvailableConnectedFlight>();
				} else {
					departureStartTime = currentDirectFlightSegments.get(0).getArrivalDate();
					departureEndTime = currentDirectFlightSegments.get(currentDirectFlightSegments.size() - 1).getArrivalDate();
				}

				availableConnectedFlightsUpToTransit.clear();

				// Construct connections
				if (segmentSequence == 0) {
					for (AvailableFlightSegment currectAvailableFlightSegment : currentDirectFlightSegments) {
						if (!isWithinInterlineCutover(currectAvailableFlightSegment, cutoverTimesForInterlining)
								&& isValidFlightSegmentForConnection(currectAvailableFlightSegment, origin, destination)) {
							ConnectedAvailableFlightSegmentsDTO connectedFlightDTO = new ConnectedAvailableFlightSegmentsDTO();
							connectedFlightDTO.addAvailableFlightSegment(currectAvailableFlightSegment);
							availableConnectedFlightsUpToTransit.add(connectedFlightDTO);
						}
					}
				} else {
					for (ConnectedAvailableFlightSegmentsDTO existingConnectedFlights : availableConnectedFlights) {
						FlightSegmentDTO preceedingFlightSegmentDTO = existingConnectedFlights.getLastFlightSegmentDTO();
						for (AvailableFlightSegment currectAvailableFlightSegment : currentDirectFlightSegments) {
							if ((preceedingFlightSegmentDTO.getFlightId().intValue() != currectAvailableFlightSegment
									.getFlightSegmentDTO().getFlightId().intValue())) {
								// Exclude multi-leg flights
								String[] minMaxTransitDurations = CommonsServices.getGlobalConfig().getMixMaxTransitDurations(
										origin, preceedingFlightSegmentDTO.getCarrierCode(),
										currectAvailableFlightSegment.getFlightSegmentDTO().getCarrierCode());
								Date lastFlightArrivalTime = preceedingFlightSegmentDTO.getArrivalDateTime();

								Date minDepartTime;
								Date maxDepartTime;

								if (enableUserDefineTransit && hubTimeDetails != null && hubTimeDetails.size() > 0
										&& hubTimeDetails.get(origin) != null) {
									Integer noOfDays = hubTimeDetails.get(origin);
									if (noOfDays.intValue() != 0) {
										Date tmpMinDepartTime = CalendarUtil.getStartTimeOfDate(CalendarUtil.getOfssetAddedTime(
												lastFlightArrivalTime, noOfDays));
										Date tmpMaxDepartTime = CalendarUtil.getEndTimeOfDate(tmpMinDepartTime);

										// If user define time within standard transit time, should get standard time
										minDepartTime = this.getMinTransitTime(lastFlightArrivalTime, minMaxTransitDurations[0]);
										maxDepartTime = this.getMaxTransitTime(lastFlightArrivalTime, minMaxTransitDurations[1]);

										if (tmpMinDepartTime.after(minDepartTime)) {
											minDepartTime = tmpMinDepartTime;
										}

										if (tmpMaxDepartTime.after(maxDepartTime)) {
											maxDepartTime = tmpMaxDepartTime;
										}
									} else {
										minDepartTime = this.getMinTransitTime(lastFlightArrivalTime, minMaxTransitDurations[0]);
										maxDepartTime = this.getMaxTransitTime(lastFlightArrivalTime, minMaxTransitDurations[1]);
									}

								} else {
									minDepartTime = this.getMinTransitTime(lastFlightArrivalTime, minMaxTransitDurations[0]);
									maxDepartTime = this.getMaxTransitTime(lastFlightArrivalTime, minMaxTransitDurations[1]);
								}

								Date currentFlightDepartureTime = currectAvailableFlightSegment.getDepartureDate();

								if (!minDepartTime.after(currentFlightDepartureTime)
										&& !currentFlightDepartureTime.after(maxDepartTime)
										&& !isWithinInterlineCutover(currectAvailableFlightSegment, cutoverTimesForInterlining)
										&& isValidFlightSegmentForConnection(currectAvailableFlightSegment, origin, destination)) {
									ConnectedAvailableFlightSegmentsDTO currentConnectedFlightSegmentsDTO = existingConnectedFlights
											.shallowClone();
									currentConnectedFlightSegmentsDTO.addAvailableFlightSegment(currectAvailableFlightSegment);
									availableConnectedFlightsUpToTransit.add(currentConnectedFlightSegmentsDTO);
								}
							}
						}
					}
				}
				if (!availableConnectedFlights.isEmpty()) {
					// empty the previous connected flights list
					availableConnectedFlights.clear();
				}
				availableConnectedFlights.addAll(availableConnectedFlightsUpToTransit);
			}
		}

		List<AvailableConnectedFlight> connectedAvailableFlightSegmentsList = new ArrayList<AvailableConnectedFlight>();
		// Prepare AvailableConnectedFlights
		for (ConnectedAvailableFlightSegmentsDTO availableConnectedFlight : availableConnectedFlights) {
			if (isConnectionValid(availableConnectedFlight)) {
				connectedAvailableFlightSegmentsList.add(availableConnectedFlight.constructAvailableConnectedFlight());
			}
		}
		return connectedAvailableFlightSegmentsList;
	}

	private boolean isWithinInterlineCutover(AvailableFlightSegment currectAvailableFlightSegment,
			Map<String, Date> allowableLatesDatesZuluForInterliningMap) {
		boolean isWithinInterlineCutover = false;
		FlightSegmentDTO flightSegmentDTO = currectAvailableFlightSegment.getFlightSegmentDTO();
		if (allowableLatesDatesZuluForInterliningMap.containsKey(flightSegmentDTO.getCarrierCode())) {
			if (flightSegmentDTO.getDepartureDateTimeZulu().before(
					allowableLatesDatesZuluForInterliningMap.get(flightSegmentDTO.getCarrierCode()))) {
				isWithinInterlineCutover = true;
			}
		}

		return isWithinInterlineCutover;

	}

	private boolean isConnectionValid(ConnectedAvailableFlightSegmentsDTO connectedFlights) {
		boolean isValid = false;
		if (connectedFlights.hasSystemCarriersSegments(CommonsServices.getGlobalConfig().getActiveSysCarriersMap().keySet())) {
			isValid = true;
		}
		return isValid;
	}

	/**
	 * method to get macDate
	 * 
	 * @param avifltSearchDTO
	 * @return maxDate
	 */
	private Date getMaxDate(Date date, int vaitation, boolean modifyTime) {

		GregorianCalendar calander = new GregorianCalendar();
		calander.setTime(date);
		calander.add(GregorianCalendar.DATE, vaitation);
		if (modifyTime) {
			calander.set(Calendar.HOUR, 23);
			calander.set(Calendar.MINUTE, 59);
			calander.set(Calendar.SECOND, 59);
			calander.set(Calendar.MILLISECOND, 999);
		}
		return calander.getTime();
	}

	/**
	 * method to get minimum transit time
	 * 
	 * @param date
	 * @param minTransitTimeValue
	 * @return min transit date
	 */
	private Date getMinTransitTime(Date date, String minTrValueStr) {

		String hours = minTrValueStr.substring(0, minTrValueStr.indexOf(":"));
		String mins = minTrValueStr.substring(minTrValueStr.indexOf(":") + 1);

		GregorianCalendar calander = new GregorianCalendar();
		calander.setTime(date);
		calander.add(GregorianCalendar.HOUR, Integer.parseInt(hours));
		calander.add(GregorianCalendar.MINUTE, Integer.parseInt(mins));

		return calander.getTime();
	}

	/**
	 * method to get maximum transit time
	 * 
	 * @param date
	 * @param maxTransitTimeValue
	 * @return max transit date
	 */
	private Date getMaxTransitTime(Date date, String maxTrValueStr) {

		String hours = maxTrValueStr.substring(0, maxTrValueStr.indexOf(":"));
		String mins = maxTrValueStr.substring(maxTrValueStr.indexOf(":") + 1);

		GregorianCalendar calander = new GregorianCalendar();
		calander.setTime(date);
		calander.add(GregorianCalendar.HOUR, Integer.parseInt(hours));
		calander.add(GregorianCalendar.MINUTE, Integer.parseInt(mins));

		return calander.getTime();
	}

	private Collection<AvailableIBOBFlightSegment> getSelectedDateAvailableFlights(
			List<? extends AvailableIBOBFlightSegment> availableFlights, Date selectedDepartureStartTime,
			Date selectedDepartureEndTime, boolean isOpenReturnSearch) {
		Collection<AvailableIBOBFlightSegment> selectedDateAvailableFlights = new ArrayList<AvailableIBOBFlightSegment>();
		for (AvailableIBOBFlightSegment availableFlight : availableFlights) {
			if (CalendarUtil.isBetweenIncludingLimits(availableFlight.getDepartureDate(), selectedDepartureStartTime,
					selectedDepartureEndTime) || isOpenReturnSearch) {
				selectedDateAvailableFlights.add(availableFlight);
			}
		}
		return selectedDateAvailableFlights;
	}

	private boolean isValidFlightSegmentForConnection(AvailableFlightSegment currectAvailableFlightSegment, String origin,
			String detination) {
		FlightSegmentDTO flightSegmentDTO = currectAvailableFlightSegment.getFlightSegmentDTO();

		if (flightSegmentDTO.getSegmentCode().contains("/" + detination + "/")
				|| flightSegmentDTO.getSegmentCode().contains("/" + origin + "/")) {
			return false;
		}

		return true;

	}
	
	private List<RouteInfoTO> getAgentWiseApplicableRoutes(String agentCode, List<RouteInfoTO> routeInfoList)
			throws ModuleException {
		List<RouteInfoTO> fliteredRouteList = new ArrayList<RouteInfoTO>();
		if (!routeInfoList.isEmpty() && agentCode != null) {
			List<String> agentApplicableRoutes = AirSchedulesUtil.getTravelAgentBD().getAgentWiseApplicableRoutes(agentCode);
			for (RouteInfoTO routeInfo : routeInfoList) {
				List<String> airports = Arrays.asList(routeInfo.getOndCode().split("\\s*/\\s*"));
				List<String> applicableOndCodeSelections = OndCombinationUtil.getApplicableOndCodeSelections(airports);
				if (!Collections.disjoint(agentApplicableRoutes, applicableOndCodeSelections)) {
					fliteredRouteList.add(routeInfo);
				}
			}
		}
		return fliteredRouteList;
	}
	
	private Collection<OriginDestinationInfoDTO>
			populateOriginDestinationCollectionForCityBasedSearch(OriginDestinationInfoDTO ondInfo) throws ModuleException {

		Collection<OriginDestinationInfoDTO> possibleOndDTOs = new ArrayList<>();

		if (AppSysParamsUtil.enableCityBasesFunctionality()) {
			Collection<String> departureAirports = new ArrayList<>();
			if (ondInfo.isDepartureCitySearch()) {
				departureAirports = AirSchedulesUtil.getAirportBD().getActiveAirportsForCity(ondInfo.getOrigin());

			}
			Collection<String> arrivalAirports = new ArrayList<>();
			if (ondInfo.isArrivalCitySearch()) {
				arrivalAirports = AirSchedulesUtil.getAirportBD().getActiveAirportsForCity(ondInfo.getDestination());

			}

			if ((departureAirports != null && !departureAirports.isEmpty())
					|| (arrivalAirports != null && !arrivalAirports.isEmpty())) {

				if ((departureAirports != null && !departureAirports.isEmpty())
						&& (arrivalAirports != null && !arrivalAirports.isEmpty())) {
					for (String origin : departureAirports) {
						for (String destination : arrivalAirports) {
							OriginDestinationInfoDTO clonedOnd = ondInfo.clone();
							clonedOnd.setDepartureCitySearch(false);
							clonedOnd.setArrivalCitySearch(false);
							clonedOnd.setOrigin(origin);
							clonedOnd.setDestination(destination);
							possibleOndDTOs.add(clonedOnd);
						}

					}
				} else if ((departureAirports != null && !departureAirports.isEmpty())
						&& (arrivalAirports == null || arrivalAirports.isEmpty())) {
					for (String origin : departureAirports) {
						OriginDestinationInfoDTO clonedOnd = ondInfo.clone();
						clonedOnd.setDepartureCitySearch(false);
						clonedOnd.setArrivalCitySearch(false);
						clonedOnd.setOrigin(origin);
						possibleOndDTOs.add(clonedOnd);
					}
				} else if ((departureAirports == null || departureAirports.isEmpty())
						&& (arrivalAirports != null && !arrivalAirports.isEmpty())) {

					for (String destination : arrivalAirports) {
						OriginDestinationInfoDTO clonedOnd = ondInfo.clone();
						clonedOnd.setDepartureCitySearch(false);
						clonedOnd.setArrivalCitySearch(false);
						clonedOnd.setDestination(destination);
						possibleOndDTOs.add(clonedOnd);
					}

				}

			}
		}
		return possibleOndDTOs;

	}

}
