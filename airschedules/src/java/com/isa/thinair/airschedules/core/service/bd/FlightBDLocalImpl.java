/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.airschedules.api.service.FlightBD;

/**
 * deligate implementation of the FlightDB to call functionalities remotely localy
 * 
 * @author Lasantha Pambagoda
 */
@Local
public interface FlightBDLocalImpl extends FlightBD {

}
