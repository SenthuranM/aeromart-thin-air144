/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.util;

import java.util.Collection;
import java.util.Iterator;

import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightLeg;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.FlightScheduleSegment;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * utility class to provide functions related to overlap details
 * 
 * @author Lasantha Pambagoda
 */
public class OverlapDetailUtil {

	/**
	 * method to add overlapp details to the flight
	 * 
	 * @param flight
	 * @param overlapFlihgt
	 * @return flight
	 */
	public static Flight addOverlapDetailsToFlight(Flight flight, Flight overlapFlihgt) {

		flight.setOverlapingFlightId(overlapFlihgt.getFlightId());

		Collection<FlightSegement> segments = flight.getFlightSegements();
		Iterator<FlightSegement> itSegments = segments.iterator();

		while (itSegments.hasNext()) {

			FlightSegement segment = (FlightSegement) itSegments.next();

			Collection<FlightSegement> overlapSegments = overlapFlihgt.getFlightSegements();
			Iterator<FlightSegement> itOverSeg = overlapSegments.iterator();

			while (itOverSeg.hasNext()) {

				FlightSegement overSegment = (FlightSegement) itOverSeg.next();
				if (overSegment.getSegmentCode().equals(segment.getSegmentCode())) {

					segment.setOverlapSegmentId(overSegment.getFltSegId());
					break;
				}
			}
		}
		return flight;
	}

	/**
	 * method to remove overlapp details to the flight
	 * 
	 * @param flight
	 * @param overlapFlihgt
	 * @return flight
	 */
	public static Flight removeOverlapDetailsFromFlight(Flight flight) {

		flight.setOverlapingFlightId(null);

		Collection<FlightSegement> segments = flight.getFlightSegements();
		Iterator<FlightSegement> itSegments = segments.iterator();

		while (itSegments.hasNext()) {

			FlightSegement segment = (FlightSegement) itSegments.next();
			segment.setOverlapSegmentId(null);
		}
		return flight;
	}

	/**
	 * method to add overlapping details to the flight schedule
	 * 
	 * @param schedule
	 * @param overlapSched
	 * @return flight schedule
	 */
	public static FlightSchedule addOverlapDetailsToSchedule(FlightSchedule schedule, FlightSchedule overlapSched) {

		schedule.setOverlapingScheduleId(overlapSched.getScheduleId());

		Collection<FlightScheduleSegment> schedSegs = schedule.getFlightScheduleSegments();
		Iterator<FlightScheduleSegment> itSegs = schedSegs.iterator();

		while (itSegs.hasNext()) {
			FlightScheduleSegment sSeg = (FlightScheduleSegment) itSegs.next();

			Collection<FlightScheduleSegment> overlpSegs = overlapSched.getFlightScheduleSegments();
			Iterator<FlightScheduleSegment> itOverlapsegs = overlpSegs.iterator();

			while (itOverlapsegs.hasNext()) {

				FlightScheduleSegment overlapSeg = (FlightScheduleSegment) itOverlapsegs.next();
				if (overlapSeg.getSegmentCode().equals(sSeg.getSegmentCode())) {

					sSeg.setOverlapSegmentId(overlapSeg.getFlSchSegId());
					break;
				}
			}
		}

		return schedule;
	}

	/**
	 * method to add overlapping details to the flight schedule
	 * 
	 * @param schedule
	 * @param overlapSched
	 * @return flight schedule
	 */
	public static FlightSchedule removeOverlapDetailsFromSchedule(FlightSchedule schedule) {

		schedule.setOverlapingScheduleId(null);

		Collection<FlightScheduleSegment> schedSegs = schedule.getFlightScheduleSegments();
		Iterator<FlightScheduleSegment> itSegs = schedSegs.iterator();

		while (itSegs.hasNext()) {
			FlightScheduleSegment sSeg = (FlightScheduleSegment) itSegs.next();
			sSeg.setOverlapSegmentId(null);
		}

		return schedule;
	}

	/**
	 * private method to update the ovelapping segment timings (overlapping schedule) to match with updated schedule
	 * overlapping segment timings.
	 * 
	 * @param overlapping
	 * @param updated
	 * @param existing
	 * @return
	 */

	public static long updateOverlappingFromUpdated(FlightSchedule overlappingSche, FlightSchedule updatedSche,
			FlightSchedule existingSche) throws ModuleException {

		return OverlapFlightShiftUtil.updateOverlappingFromUpdated(overlappingSche, updatedSche, existingSche);

	}

	/**
	 * private method to update overlapping flight with the overlapping leg timings of the updated flight
	 * 
	 * @param overlapping
	 * @param updated
	 * @param existing
	 * @return
	 */
	public static long updateOverlappingFromUpdated(Flight overlappingFlt, Flight updatedFlt, Flight existingFlt)
			throws ModuleException {

		return OverlapFlightShiftUtil.updateOverlappingFromUpdated(overlappingFlt, updatedFlt, existingFlt);
	}

	/**
	 * private method to update the ovelapping segment (overlapping flight) timings to match with updated schedule
	 * overlapping segment timings.
	 * 
	 * @param overlapping
	 * @param updated
	 * @param existing
	 * @return
	 */

	public static Flight updateOverlappingFromUpdated(Flight overlappingFlt, FlightLeg overlapFlightLeg,
			FlightSchedule updatedSche, Flight existingFlt, FlightSchedule existingSche) throws ModuleException {

		return OverlapLegShiftUtil.updateOverlappingFromUpdated(overlappingFlt, overlapFlightLeg, updatedSche, existingFlt,
				existingSche);
	}

}
