/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:19:10
 * 
 * $Id$
 * 
 * ===============================================================================
 */

package com.isa.thinair.airschedules.core.persistence.hibernate;

import java.util.List;

import com.isa.thinair.airschedules.api.model.FlightScheduleSegment;
import com.isa.thinair.airschedules.core.persistence.dao.FlightScheduleSegmentDAO;
import com.isa.thinair.commons.core.framework.PlatformHibernateDaoSupport;

/**
 * FlightScheduleSegmentDAOImpl is the FlightScheduleSegmenttDAO implementatiion for hibernate
 * 
 * @author Lasantha Pambagoda
 * @isa.module.dao-impl dao-name="flightScheduleSegmentDAO"
 */
public class FlightScheduleSegmentDAOImpl extends PlatformHibernateDaoSupport implements FlightScheduleSegmentDAO {

	/**
	 * method to get all flight schedule segments
	 */
	public List<FlightScheduleSegment> getFlightScheduleSegments() {
		return find("from FlightScheduleSegment", FlightScheduleSegment.class);
	}

	/**
	 * method to get flight schedule segment for given id
	 */
	public FlightScheduleSegment getFlightScheduleSegment(int id) {
		return (FlightScheduleSegment) get(FlightScheduleSegment.class, new Integer(id));
	}

	/**
	 * method to save flight schedule segment
	 */
	public void saveFlightScheduleSegment(FlightScheduleSegment flightschedulesegment) {
		hibernateSaveOrUpdate(flightschedulesegment);
	}

	/**
	 * method to remove flight schedule segment
	 */
	public void removeFlightScheduleSegment(int id) {
		Object flightschedulesegment = load(FlightScheduleSegment.class, new Integer(id));
		delete(flightschedulesegment);
	}
}
