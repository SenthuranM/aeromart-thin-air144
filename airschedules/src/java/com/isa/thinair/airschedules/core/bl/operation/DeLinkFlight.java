/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.operation;

import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.LocalZuluTimeAdder;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.airschedules.core.persistence.dao.FlightDAO;
import com.isa.thinair.airschedules.core.persistence.dao.FlightScheduleDAO;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for DeLink Flights from schedule
 * 
 * @author M.Rikaz
 * @isa.module.command name="deLinkFlight"
 */
public class DeLinkFlight extends DefaultBaseCommand {

	private FlightDAO flightDAO;
	private final FlightScheduleDAO flightSchedleDAO;

	private AirportBD airportBD;

	// helpers
	private LocalZuluTimeAdder timeAdder;

	/**
	 * constructor of the SplitSchedule command
	 */
	public DeLinkFlight() {

		// looking up daos
		flightDAO = (FlightDAO) AirSchedulesUtil.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_DAO);
		flightSchedleDAO = (FlightScheduleDAO) AirSchedulesUtil.getInstance()
				.getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_DAO);

		// BDs
		airportBD = AirSchedulesUtil.getAirportBD();

		// helpers
		timeAdder = new LocalZuluTimeAdder(airportBD);
	}

	public ServiceResponce execute() throws ModuleException {

		Flight updatedFlight = (Flight) this.getParameter(CommandParamNames.FLIGHT);
		boolean isSplitSchedule = this.getParameter(CommandParamNames.IS_SCHEDULE_SPLIT, Boolean.FALSE, Boolean.class);
		boolean isScheduleMessageProcess = this.getParameter(CommandParamNames.IS_SHEDULE_MESSAGE_PROCESS, Boolean.FALSE,
				Boolean.class);

		
		Integer scheduleID = this.getParameter(CommandParamNames.SCHEDULE_ID, Integer.class);
		
		Flight existing = flightDAO.getFlight(updatedFlight.getFlightId().intValue());
		// Unlink schedule
		if (AppSysParamsUtil.isSplitAndUpdateScheduleForScheduleConflicts() && isSplitSchedule && existing != null
				&& scheduleID != null) {

			FlightSchedule schedule = flightSchedleDAO.getFlightSchedule(scheduleID);

			if (!CalendarUtil.isSameDay(schedule.getStartDate(), schedule.getStopDate())) {
				if (CalendarUtil.daysUntil(schedule.getStartDate(), schedule.getStopDate()) > 0
						|| CalendarUtil.daysUntil(schedule.getStartDateLocal(), schedule.getStopDateLocal()) > 0) {

					Frequency frq = getDummyFrequency();
					AirSchedulesUtil.getScheduleBD().splitSchedule(scheduleID,
							CalendarUtil.getStartTimeOfDate(existing.getDepartureDate()),
							CalendarUtil.getEndTimeOfDate(existing.getDepartureDate()),
							CalendarUtil.getValidFrequencyMatchesDateRange(existing.getDepartureDate(),
									existing.getDepartureDate(), frq),
							false, true, true);
				}

			}

		}

		// constructing and return command responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);
		if (responce.isSuccess()) {
			responce.setResponseCode(ResponceCodes.SPLIT_SCHEDULE_SUCCESSFULL);
			responce.addResponceParam(CommandParamNames.LAST_RESPONSE, responce);

		}
		return responce;
	}

	private Frequency getDummyFrequency() {
		Frequency frq = new Frequency();
		frq.setDay0(true);
		frq.setDay1(true);
		frq.setDay2(true);
		frq.setDay3(true);
		frq.setDay4(true);
		frq.setDay5(true);
		frq.setDay6(true);
		return frq;
	}

}
