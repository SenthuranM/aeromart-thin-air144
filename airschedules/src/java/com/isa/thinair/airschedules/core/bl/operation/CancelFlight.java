/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.operation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.service.FlightInventoryBD;
import com.isa.thinair.airinventory.api.service.FlightInventoryResBD;
import com.isa.thinair.airinventory.api.service.MealBD;
import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airreservation.api.dto.FlightReservationAlertDTO;
import com.isa.thinair.airschedules.api.dto.FlightAlertDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.AlertTypeEnum;
import com.isa.thinair.airschedules.api.utils.AlertUtil;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.airschedules.api.utils.LocalZuluTimeAdder;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.airschedules.api.utils.SegmentUtil;
import com.isa.thinair.airschedules.core.bl.email.FlightScheduleEmail;
import com.isa.thinair.airschedules.core.persistence.dao.FlightDAO;
import com.isa.thinair.airschedules.core.persistence.dao.FlightScheduleDAO;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.airschedules.core.util.OverlapDetailUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for the cancel flight
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="cancelFlight"
 */
public class CancelFlight extends DefaultBaseCommand {

	private static final Log log = LogFactory.getLog(CancelFlight.class);

	// Dao's
	private FlightScheduleDAO flightSchedleDAO;
	private FlightDAO flightDAO;

	// BD's
	private FlightInventoryResBD flightInventryResBD;
	private FlightInventoryBD flightInventryBD;
	private AirportBD airportBD;
	private MealBD mealBD;

	// helpers
	private LocalZuluTimeAdder timeAdder;

	/**
	 * constructor of the CancelFlight command
	 */
	public CancelFlight() {

		// looking up daos
		flightDAO = (FlightDAO) AirSchedulesUtil.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_DAO);
		flightSchedleDAO = (FlightScheduleDAO) AirSchedulesUtil.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_DAO);

		// looking up bd's
		flightInventryBD = AirSchedulesUtil.getFlightInventoryBD();
		flightInventryResBD = AirSchedulesUtil.getFlightInventoryResBD();
		airportBD = AirSchedulesUtil.getAirportBD();
		mealBD = AirSchedulesUtil.getMealBD();

		// helpers
		timeAdder = new LocalZuluTimeAdder(airportBD);
	}

	/**
	 * execute method of the CancelFlight command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {
		if (log.isDebugEnabled()) {
			log.debug("CancelFlight command execute called");
		}
		// getting command params
		Integer flightId = (Integer) this.getParameter(CommandParamNames.FLIGHT_ID);
		FlightAlertDTO flightAlertDTO = (FlightAlertDTO) this.getParameter(CommandParamNames.FLIGHT_ALERT_DTO);

		if (log.isDebugEnabled()) {
			log.debug("Command parameters passed:");
			log.debug("flightId : " + flightId);
			log.debug("flight alert DTO : " + flightAlertDTO);
		}

		// checking params
		this.checkParams(flightId);
		
		//Collecting Flights to Publish 
		//Note : Flights will be added to collection only if its removed or canceled.
		Collection<Flight> flightsToPublish = new ArrayList<Flight>();

		if (log.isDebugEnabled()) {
			log.debug("Command parameters validated");
		}

		Flight flight = null;
		try {
			// get the flight from database
			flight = flightDAO.getFlight(flightId.intValue());
		} catch (RuntimeException e) {
			log.error(e.getMessage(), e);
			throw e;
		}

		// if no flight found for id throw exception
		if (flight == null)
			throw new ModuleException("airschedules.logic.bl.flight.noflight");

		if (log.isDebugEnabled()) {
			log.debug("Flight to be cancelled retrieved, flightID : " + flightId);
		}

		Flight overlapFlight = null;
		if (flight.isOverlapping()) {
			if (log.isDebugEnabled()) {
				log.debug("Overlapping flights exist, retrieving the same.");
			}
			overlapFlight = flightDAO.getFlight(flight.getOverlapingFlightId().intValue());

			if (log.isDebugEnabled()) {
				log.debug("Overlapping flight retrieved : " + overlapFlight);
			}
		}

		// prepare alert/email DTO for flight
		Collection<FlightReservationAlertDTO> flightReservationAlertDTOList = new ArrayList<FlightReservationAlertDTO>();

		Collection<Flight> flights = new ArrayList<Flight>();
		flight = timeAdder.addLocalTimeDetailsToFlight(flight);
		flights.add(flight);
		if (overlapFlight != null) {
			overlapFlight = timeAdder.addLocalTimeDetailsToFlight(overlapFlight);
			flights.add(overlapFlight);
		}
		flightReservationAlertDTOList.addAll(AlertUtil.prepareFlightAlertInfo(flights));
		if (log.isDebugEnabled()) {
			log.debug("Prepared alert/email DTO for flight : " + flightReservationAlertDTOList);
		}

		// collection of cancanlled flightIds for sending alerts
		Collection<Integer> alertFlightIds = new ArrayList<Integer>();

		// if flight is scheduled then cancel it
		if (flight.getScheduleId() != null) {

			if (log.isDebugEnabled()) {
				log.debug("Flights are scheduled, so cancelling them.");
			}

			flight.setStatus(FlightStatusEnum.CANCELLED.getCode());
			flight.setManuallyChanged(true);
			flight.setCancelMsgWaiting(false);
			flight.setHoldCancelMsgTill(null);
			flightDAO.saveFlight(flight);
			//Add the flights to the list which holds the publish flight list
			flightsToPublish.add(flight);
			
			
			// get flight segments to cancel reservations
			List<Integer> flightSegmentIds = SegmentUtil.getFlightSegIds(flight.getFlightSegements());

			if (log.isDebugEnabled()) {
				log.debug("Retrieved relavant flightSegmentIDs : " + flightSegmentIds);
			}

			// alert cancelled flight
			alertFlightIds.add(flight.getFlightId());

			if (log.isDebugEnabled()) {
				log.debug("Retrieving flight schedule for cancelled flight update : " + flight.getScheduleId());
			}

			FlightSchedule scheduleForFlight = flightSchedleDAO.getFlightSchedule(flight.getScheduleId().intValue());

			Integer newScheduleASK = new Integer(scheduleForFlight.getAvailableSeatKilometers().intValue()
					- flight.getAvailableSeatKilometers().intValue());

			scheduleForFlight.setAvailableSeatKilometers(newScheduleASK);
			scheduleForFlight.setManuallyChanged(true);

			flightSchedleDAO.saveFlightSchedule(scheduleForFlight);

			if (log.isDebugEnabled()) {
				log.debug("Saved updated flight schedule : " + scheduleForFlight);
			}

			// if flight is overlapping cancel the overlapping flight as well
			if (flight.isOverlapping() && overlapFlight != null) {

				if (log.isDebugEnabled()) {
					log.debug("Flight is overlapping.");
				}

				overlapFlight.setStatus(FlightStatusEnum.CANCELLED.getCode());
				overlapFlight.setManuallyChanged(true);
				flightDAO.saveFlight(overlapFlight);
				
				//Add the flights to the list which holds the publish flight list
				flightsToPublish.add(overlapFlight);
				
				if (log.isDebugEnabled()) {
					log.debug("Status changed for flight : " + FlightStatusEnum.CANCELLED.getCode());
				}

				// get flight segments to cancel reservations (for the overlapping)
				flightSegmentIds.addAll(SegmentUtil.getFlightSegIds(overlapFlight.getFlightSegements()));
				// alert overlap flight
				alertFlightIds.add(overlapFlight.getFlightId());

				if (overlapFlight.getScheduleId() != null) {

					FlightSchedule overlapSchedule = flightSchedleDAO.getFlightSchedule(overlapFlight.getScheduleId().intValue());
					if (log.isDebugEnabled()) {
						log.debug("Flight schedules retrieved : " + overlapSchedule);
					}

					Integer overlapScheduleASK = new Integer(overlapSchedule.getAvailableSeatKilometers().intValue()
							- overlapFlight.getAvailableSeatKilometers().intValue());

					overlapSchedule.setAvailableSeatKilometers(overlapScheduleASK);
					overlapSchedule.setManuallyChanged(true);

					flightSchedleDAO.saveFlightSchedule(overlapSchedule);

					if (log.isDebugEnabled()) {
						log.debug("Saved updated flight schedule : " + overlapSchedule);
					}
				}
			}

			// [AirReservation integration] cancel reservations
			// segmentBD.cancelSegments(flightSegmentIds, cancelCharge, null);

		} else {
			if (log.isDebugEnabled()) {
				log.debug("Flight is not scheduled.");
			}
			// check if this flight has reservations
			List<Integer> flightIds = new ArrayList<Integer>();
			flightIds.add(flight.getFlightId());
			if (flight.isOverlapping())
				flightIds.add(flight.getOverlapingFlightId());

			Collection<Integer> flightsHasRes = flightInventryResBD.filterFlightsHavingReservations(flightIds);

			boolean hasReservations = (flightsHasRes != null && flightsHasRes.size() > 0) ? true : false;

			if (hasReservations || flight.getStatus().equals(FlightStatusEnum.ACTIVE.getCode())) {
				if (log.isDebugEnabled()) {
					log.debug("Flight has reservations.");
				}

				flight.setStatus(FlightStatusEnum.CANCELLED.getCode());
				flight.setCancelMsgWaiting(false);
				flight.setHoldCancelMsgTill(null);
				flightDAO.saveFlight(flight);
				
				//Add the flights to the list which holds the publish flight list
				flightsToPublish.add(flight);
				
				// alert flight
				alertFlightIds.add(flight.getFlightId());
				// get flight segments to cancel reservations
				List<Integer> flightSegmentIds = SegmentUtil.getFlightSegIds(flight.getFlightSegements());

				// if flight is overlapping cancel it as well
				if (flight.isOverlapping() && overlapFlight != null) {
					if (log.isDebugEnabled()) {
						log.debug("Flight is overlapping.");
					}

					overlapFlight.setStatus(FlightStatusEnum.CANCELLED.getCode());
					flightDAO.saveFlight(overlapFlight);
					
					flight.setStatus(FlightStatusEnum.CANCELLED.getCode());
					flight.setCancelMsgWaiting(false);
					flight.setHoldCancelMsgTill(null);
					flightDAO.saveFlight(overlapFlight);
					
					//Add the flights to the list which holds the publish flight list
					flightsToPublish.add(overlapFlight);

					if (log.isDebugEnabled()) {
						log.debug("Status changed for flight : " + FlightStatusEnum.CANCELLED.getCode());
					}

					// alert overlapped flight
					alertFlightIds.add(overlapFlight.getFlightId());
					// get flight segments to cancel reservations (for the overlapping)
					flightSegmentIds.addAll(SegmentUtil.getFlightSegIds(overlapFlight.getFlightSegements()));
				}

				// [AirReservation integration] cancel reservations
				// segmentBD.cancelSegments(flightSegmentIds, cancelCharge, null);

			} else {
				if (log.isDebugEnabled()) {
					log.debug("Flight has no reservations.");
				}

				// remove inventory
				ArrayList<Integer> flightsToRemoveInven = new ArrayList<Integer>();
				flightsToRemoveInven.add(flight.getFlightId());
				flightInventryBD.deleteFlightInventories(flightsToRemoveInven);

				if (log.isDebugEnabled()) {
					log.debug("Flight removed from inventory.");
				}

				if (flight.isOverlapping() && overlapFlight != null) {
					if (log.isDebugEnabled()) {
						log.debug("Flight is overlapping.");
					}

					overlapFlight = OverlapDetailUtil.removeOverlapDetailsFromFlight(overlapFlight);
					flight = OverlapDetailUtil.removeOverlapDetailsFromFlight(flight);

					flightDAO.saveFlight(flight);
					if (log.isDebugEnabled()) {
						log.debug("Flight for cancellation - updated and saved");
					}

					flightDAO.saveFlight(overlapFlight);
					if (log.isDebugEnabled()) {
						log.debug("Overlapping flight - updated for cancellation and saved");
					}

					// remove inventory
					ArrayList<Integer> ovrlapFlightsToRemoveInven = new ArrayList<Integer>();
					ovrlapFlightsToRemoveInven.add(overlapFlight.getFlightId());
					flightInventryBD.deleteFlightInventories(ovrlapFlightsToRemoveInven);

					if (log.isDebugEnabled()) {
						log.debug("Removed overlapping flight from inventory.");
					}

					flightDAO.removeFlight(overlapFlight.getFlightId().intValue());
					
					//Add the flights to the list which holds the publish flight list
					overlapFlight.setStatus(FlightStatusEnum.CANCELLED.getCode());
					flightsToPublish.add(overlapFlight);

					if (log.isDebugEnabled()) {
						log.debug("Removed overlapping flight : " + overlapFlight.getFlightId());
					}

					// alert overlapped flight
					alertFlightIds.add(overlapFlight.getFlightId());
				}
				if (flight.getStatus().equals(FlightStatusEnum.CREATED.getCode())) {
					List<Integer> flightSegmentIds = SegmentUtil.getFlightSegIds(flight.getFlightSegements());
					mealBD.deleteFlightMeals(flightSegmentIds);
				}
				flightDAO.removeFlight(flight.getFlightId().intValue());
				
				//Add the flights to the list which holds the publish flight list
				flight.setStatus(FlightStatusEnum.CANCELLED.getCode());
				flight.setCancelMsgWaiting(false);
				flight.setHoldCancelMsgTill(null);
				flightsToPublish.add(flight);

				if (log.isDebugEnabled()) {
					log.debug("Removed flight : " + flight.getFlightId());
				}
				// alert flight
				alertFlightIds.add(flight.getFlightId());
			}
		}

//		if (AppSysParamsUtil.isEnableOfficerEmailAlertAtFlightCnx()) {
//			FlightScheduleEmail.sentFlightCancellationOfficerNotification(flightsToPublish, true);
//		}
		// constructing and return command responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);
		if (responce.isSuccess()) {

			if (log.isDebugEnabled()) {
				log.debug("Returning success response.");
			}

			responce.setResponseCode(ResponceCodes.CANCEL_FLIGHT_SUCCESSFULL);
			responce.addResponceParam(CommandParamNames.FLIGHT_SEGMENT_EMAIL_DTO_COLLECTION, flightReservationAlertDTOList);
			if ((flightAlertDTO != null) && (flightAlertDTO.isAlertForCancelledFlight())) {
				responce.addResponceParam(CommandParamNames.IDS, alertFlightIds);
				responce.addResponceParam(CommandParamNames.ALLERT_TYPE, AlertTypeEnum.ALERT_FLIGHT_CANCEL);
			}
			if (overlapFlight != null)
				responce.addResponceParam(CommandParamNames.OVERLAPPING_FLIGHT, overlapFlight);
			responce.addResponceParam(CommandParamNames.FLIGHT, flight);
			responce.addResponceParam(CommandParamNames.FLIGHTS_TO_PUBLISH, flightsToPublish);
			responce.addResponceParam(CommandParamNames.LAST_RESPONSE, responce);
			responce.addResponceParam(CommandParamNames.SCHEDULE_ID, flight.getScheduleId());
			
			// Unlink schedule
			if (AppSysParamsUtil.isSplitAndUpdateScheduleForScheduleConflicts()) {
				responce.addResponceParam(CommandParamNames.IS_SCHEDULE_SPLIT, true);
			}
		}

		return responce;
	}

	/**
	 * privte method to validate parameters
	 * 
	 * @param schedule
	 * @throws ModuleException
	 */
	private void checkParams(Integer flightId) throws ModuleException {

		if (flightId == null)// throw exception
			throw new ModuleException("airschedules.arg.invalid.null");
	}

}
