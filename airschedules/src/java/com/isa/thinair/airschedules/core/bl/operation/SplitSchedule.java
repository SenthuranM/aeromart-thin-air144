/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.operation;

import com.isa.thinair.airmaster.api.model.AircraftModel;
import com.isa.thinair.airmaster.api.model.RouteInfo;
import com.isa.thinair.airmaster.api.service.AircraftBD;
import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.airschedules.api.dto.AffectedPeriod;
import com.isa.thinair.airschedules.api.dto.FlightAlertDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.FlightScheduleLeg;
import com.isa.thinair.airschedules.api.model.SSMSplitFlightSchedule;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.airschedules.api.utils.GDSSchedConstants;
import com.isa.thinair.airschedules.api.utils.GDSScheduleEventCollector;
import com.isa.thinair.airschedules.api.utils.LegUtil;
import com.isa.thinair.airschedules.api.utils.LocalZuluTimeAdder;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.airschedules.api.utils.ScheduleBuildStatusEnum;
import com.isa.thinair.airschedules.core.persistence.dao.FlightDAO;
import com.isa.thinair.airschedules.core.persistence.dao.FlightScheduleDAO;
import com.isa.thinair.airschedules.core.util.CalculationUtil;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.airschedules.core.util.OverlapDetailUtil;
import com.isa.thinair.airschedules.core.util.PeriodsUtil;
import com.isa.thinair.airschedules.core.util.ScheduledFlightUtil;
import com.isa.thinair.airschedules.core.util.SplitInstructions;
import com.isa.thinair.airschedules.core.utils.CommandNames;
import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.framework.Command;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.FrequencyUtil;
import com.isa.thinair.gdsservices.api.dto.publishing.Envelope;
import com.isa.thinair.gdsservices.api.service.GDSPublishingBD;
import com.isa.thinair.platform.api.ServiceResponce;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Set;
import java.util.Stack;

/**
 * command for split schedule
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="splitSchedule"
 */
public class SplitSchedule extends DefaultBaseCommand {

	// Dao's
	private FlightScheduleDAO flightSchedleDAO;
	private FlightDAO flightDAO;

	// BDs
	private AircraftBD aircraftBD;
	private CommonMasterBD commonMasterBD;
	private AirportBD airportBD;

	// helpers
	private LocalZuluTimeAdder timeAdder;

	/**
	 * constructor of the SplitSchedule command
	 */
	public SplitSchedule() {

		// looking up daos
		flightDAO = (FlightDAO) AirSchedulesUtil.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_DAO);
		flightSchedleDAO = (FlightScheduleDAO) AirSchedulesUtil.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_DAO);

		// BDs
		airportBD = AirSchedulesUtil.getAirportBD();
		commonMasterBD = AirSchedulesUtil.getCommonMasterBD();
		aircraftBD = AirSchedulesUtil.getAircraftBD();

		// helpers
		timeAdder = new LocalZuluTimeAdder(airportBD);
	}

	/**
	 * execute method of the SplitSchedule command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {

		// getting command params
		Integer scheduleId = (Integer) this.getParameter(CommandParamNames.SCHEDULE_ID);
		AffectedPeriod period = (AffectedPeriod) this.getParameter(CommandParamNames.AFFECTED_PERIOD);
		Boolean datesInLocal = (Boolean) this.getParameter(CommandParamNames.IS_DATE_IN_LOCAL);
		boolean isReviseFrequency = this.getParameter(CommandParamNames.IS_REVISE_FREQUENCY, Boolean.FALSE, Boolean.class);
		boolean isScheduleMessageProcess = this.getParameter(CommandParamNames.IS_SHEDULE_MESSAGE_PROCESS, Boolean.FALSE,
				Boolean.class);

		// checking params
		this.checkParams(scheduleId, period, datesInLocal);

		// load the corresponding flight schedule
		FlightSchedule schedule = flightSchedleDAO.getFlightSchedule(scheduleId.intValue());
		FlightSchedule copyFullSchedule = null;
		boolean isGdsIntegrated = false;
		if ("Y".equalsIgnoreCase(CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.GDS_INTEGRATED))) {
			isGdsIntegrated = true;
			copyFullSchedule = ScheduledFlightUtil.getCopyOfSchedule(schedule);
			copyFullSchedule.setScheduleId(scheduleId);
		}
		
		
		// if no schedule found for id throw exception
		if (schedule == null)
			throw new ModuleException("airschedules.logic.bl.schedule.noschedule");

		FlightSchedule overlapping = null;
		if (schedule.isOverlapping())
			overlapping = flightSchedleDAO.getFlightSchedule(schedule.getOverlapingScheduleId().intValue());

		String origin = schedule.getDepartureStnCode();
		Date effectiveDate = schedule.getStartDate();
		FlightScheduleLeg firstLeg = LegUtil.getFirstScheduleLeg(schedule.getFlightScheduleLegs());

		Date splitStartDateZulu = null;
		Date splitEndDateZulu = null;
		Frequency splitFrequencyZulu = null;
		Date splitStartDateLocal = null;
		Date splitEndDateLocal = null;

		int millsDiff = 0;
		int dayDiff = 0;

		// if dates in local then calculate the zulu values
		if (datesInLocal.booleanValue()) {

			Date scheduleLocalStartTime = timeAdder.getLocalDateTimeAsEffective(origin, effectiveDate,
					firstLeg.getEstDepartureTimeZulu());
			splitStartDateLocal = CalendarUtil.getTimeAddedDate(period.getStartDate(), scheduleLocalStartTime);
			splitEndDateLocal = CalendarUtil.getTimeAddedDate(period.getEndDate(), scheduleLocalStartTime);

			splitStartDateZulu = timeAdder.getZuluDateTimeAsEffective(origin, effectiveDate, splitStartDateLocal);
			splitEndDateZulu = timeAdder.getZuluDateTimeAsEffective(origin, effectiveDate, splitEndDateLocal);

			int diff = CalendarUtil.daysUntil(splitStartDateLocal, splitStartDateZulu);
			splitFrequencyZulu = FrequencyUtil.shiftFrequencyBy(period.getFrequency(), diff);

		} else {

			Date scheduleZuluStartTime = firstLeg.getEstDepartureTimeZulu();

			splitStartDateZulu = CalendarUtil.getTimeAddedDate(period.getStartDate(), scheduleZuluStartTime);
			splitEndDateZulu = CalendarUtil.getTimeAddedDate(period.getEndDate(), scheduleZuluStartTime);

			schedule.setStartDateLocal(timeAdder.getLocalDateTime(origin, schedule.getStartDate()));
			schedule.setStopDateLocal(timeAdder.getLocalDateTime(origin, schedule.getStopDate()));

			splitFrequencyZulu = period.getFrequency();
		}

		Frequency frequency = CalendarUtil.getValidFrequencyMatchesDateRange(splitStartDateZulu, splitEndDateZulu,
				splitFrequencyZulu);

		Date firstFlightDate = CalendarUtil.getEffectiveStartDate(splitStartDateZulu, frequency);
		Date lastFlightDate = CalendarUtil.getEffectiveStopDate(splitEndDateZulu, frequency);

		if (!CalendarUtil.isSameDay(splitStartDateZulu, firstFlightDate)
				|| !CalendarUtil.isSameDay(splitEndDateZulu, lastFlightDate)) {
			splitStartDateZulu = firstFlightDate;
			splitEndDateZulu = lastFlightDate;
			splitFrequencyZulu = frequency;

			period.setStartDate(splitStartDateZulu);
			period.setEndDate(splitEndDateZulu);
			period.setFrequency(splitFrequencyZulu);
		}
		// schedule date period
		Date schedStart = schedule.getStartDate();
		Date schedStop = schedule.getStopDate();

		int startDiff = CalendarUtil.daysUntil(schedStart, splitStartDateZulu);
		int endDiff = CalendarUtil.daysUntil(splitEndDateZulu, schedStop);
		int splitDiff = CalendarUtil.daysUntil(splitStartDateZulu, splitEndDateZulu);

		if (!AppSysParamsUtil.isAllowedUpdateScheduledFlightToOutOfSchedOrFreq()) {

			if (startDiff < 0 || endDiff < 0 || splitDiff < 0) {
				throw new ModuleException("airschedules.arg.invalid");
			}
		}
		Stack<SplitInstructions> stack = new Stack<SplitInstructions>();

		boolean isFrequencySplit = false;
		if (!FrequencyUtil.isEqualFrequency(schedule.getFrequency(), splitFrequencyZulu)) {

			SplitInstructions instructions = new SplitInstructions();
			instructions.setOperation(SplitInstructions.FREQUENCY_SPLIT);
			instructions.setFrequency(splitFrequencyZulu);
			stack.push(instructions);
			isFrequencySplit = true;
		}

		boolean skipLeftSplit = false;
		boolean skipRightSplit = false;
		if (isFrequencySplit) {
			if (endDiff > 0 && endDiff < 8) {
				// frequencyMatchesDateRange
				Frequency scheduleFqn = FrequencyUtil.getCopyOfFrequency(schedule.getFrequency());

				Frequency restFqn = FrequencyUtil.getRestOfTheFrequency(scheduleFqn, splitFrequencyZulu);

				Frequency validOperationDays = CalendarUtil.getValidFrequencyMatchesDateRange(
						CalendarUtil.addDateVarience(splitEndDateZulu, 1), schedule.getStopDate(), scheduleFqn);

				skipLeftSplit = FrequencyUtil.chackIfSubFrequency(restFqn, validOperationDays);
			}

			if (startDiff > 0 && startDiff < 8) {
				// frequencyMatchesDateRange
				Frequency scheduleFqn = FrequencyUtil.getCopyOfFrequency(schedule.getFrequency());

				Frequency restFqn = FrequencyUtil.getRestOfTheFrequency(scheduleFqn, splitFrequencyZulu);

				Frequency validOperationDays = CalendarUtil.getValidFrequencyMatchesDateRange(schedule.getStartDate(),
						CalendarUtil.addDateVarience(splitStartDateZulu, -1), scheduleFqn);

				skipRightSplit = FrequencyUtil.chackIfSubFrequency(restFqn, validOperationDays);
			}
		}



		if (endDiff > 0 && !skipLeftSplit) {

			SplitInstructions instructions = new SplitInstructions();
			instructions.setDate(splitEndDateZulu);
			instructions.setOperation(SplitInstructions.LEFT_SPLIT);
			stack.push(instructions);
		}

		if (startDiff > 0 && !skipRightSplit) {

			SplitInstructions instructions = new SplitInstructions();
			instructions.setDate(splitStartDateZulu);
			instructions.setOperation(SplitInstructions.RIGHT_SPLIT);
			stack.push(instructions);
		}

		if (schedule.isOverlapping()) {
			millsDiff = (int) (overlapping.getStartDate().getTime() - schedule.getStartDate().getTime());
			dayDiff = CalendarUtil.daysUntil(schedule.getStartDate(), overlapping.getStartDate());
		}

		// constructing and return command responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);
		Collection<Integer> newScheduleIdsFromSplit = new ArrayList<Integer>();
		this.splitSchedule(schedule, stack, schedule.getScheduleId().intValue(), overlapping, schedule.getOverlapingScheduleId(),
				millsDiff, dayDiff, responce, newScheduleIdsFromSplit, isScheduleMessageProcess);

		if (responce.isSuccess()) {
			responce.setResponseCode(ResponceCodes.SPLIT_SCHEDULE_SUCCESSFULL);
			responce.addResponceParam(CommandParamNames.OVERLAPPING_SCHEDULE, overlapping);
			responce.addResponceParam(CommandParamNames.LAST_RESPONSE, responce);
			if (isReviseFrequency) {
				this.reviseScheduleFrequencyPeriodDeleteInvalidSchedules(newScheduleIdsFromSplit, isScheduleMessageProcess);
				this.deleteAdHocFlightSchedules(newScheduleIdsFromSplit);
			}
			responce.addResponceParam(CommandParamNames.NEW_SCHEDULE_IDS_FROM_SPLIT, newScheduleIdsFromSplit);
			if (isGdsIntegrated) {
				FlightSchedule targetSchedule = getTargetedSplitSchedule( period,  datesInLocal,newScheduleIdsFromSplit);
				splitDSTSubScheduleAndPublishSSM(copyFullSchedule, newScheduleIdsFromSplit, targetSchedule);
			}						
		}
		return responce;
	}
	/**
	 * privte method to validate parameters
	 * @param scheduleId
	 * @param period
	 * @param datesInLocal
	 * @throws ModuleException
	 */
	private void checkParams(Integer scheduleId, AffectedPeriod period, Boolean datesInLocal) throws ModuleException {

		if (period.getStartDate() == null)
			period.setStartDate(period.getEndDate());
		if (period.getEndDate() == null)
			period.setEndDate(period.getEndDate());

		if (scheduleId == null || period.getStartDate() == null || period.getEndDate() == null || period.getFrequency() == null
				|| datesInLocal == null)// throw exception
			throw new ModuleException("airschedules.arg.invalid.null");

		if (FrequencyUtil.isAllFalse(period.getFrequency()))
			throw new ModuleException("airschedules.arg.invalid");
	}

	/**
	 * method to split scheduele
	 * 
	 * @param schedule
	 * @param opStack
	 * @param originalScheduleId
	 * @param isScheduleMessageProcess
	 * @throws ModuleException
	 */
	@SuppressWarnings("unused")
	private void splitSchedule(FlightSchedule schedule, Stack<SplitInstructions> opStack, int originalScheduleId,
			FlightSchedule overlap, Integer originalOverlapId, int millsDiff, int daysDiff, DefaultServiceResponse responce,
			Collection<Integer> newScheduleIdsFromSplit, boolean isScheduleMessageProcess) throws ModuleException {

		SplitInstructions instructions = (SplitInstructions) opStack.pop();
		if (instructions == null)
			return;
		
		Date splitDate = instructions.getDate();
		Frequency frequency = instructions.getFrequency();
		int operation = instructions.getOperation();
		schedule.setModifiedDate(new Date(System.currentTimeMillis()));
		Date startDateLocal = null;
		Date stopDateLocal = null;

		FlightSchedule newScheduleFromSplit = ScheduledFlightUtil.getCopyOfSchedule(schedule);
		newScheduleFromSplit.setViaScheduleMessaging(isScheduleMessageProcess);
		// get the overlap
		boolean overlapinng = ((overlap != null && originalOverlapId != null)) ? true : false;
		FlightSchedule newOverlapFromSplit = null;
		if (overlapinng)
			newOverlapFromSplit = ScheduledFlightUtil.getCopyOfSchedule(overlap);

		if (operation == SplitInstructions.RIGHT_SPLIT) {

			startDateLocal = timeAdder.getLocalDateTime(newScheduleFromSplit.getDepartureStnCode(), splitDate);
			newScheduleFromSplit.setStartDate(splitDate);
			newScheduleFromSplit.setStartDateLocal(startDateLocal);

			if (overlapinng) {
				newOverlapFromSplit.setStartDate(CalendarUtil.getOfssetInMilisecondAddedDate(splitDate, millsDiff));
				startDateLocal = timeAdder.getLocalDateTime(newOverlapFromSplit.getDepartureStnCode(),
						CalendarUtil.getOfssetInMilisecondAddedDate(splitDate, millsDiff));
				newOverlapFromSplit.setStartDateLocal(startDateLocal);
			}

			GregorianCalendar calander = new GregorianCalendar();
			calander.setTime(splitDate);
			calander.add(GregorianCalendar.DATE, -1);

			stopDateLocal = timeAdder.getLocalDateTime(newScheduleFromSplit.getDepartureStnCode(), calander.getTime());
			schedule.setStopDate(calander.getTime());
			schedule.setStopDateLocal(stopDateLocal);

			if (overlapinng) {
				overlap.setStopDate(CalendarUtil.getOfssetInMilisecondAddedDate(calander.getTime(), millsDiff));
				stopDateLocal = timeAdder.getLocalDateTime(overlap.getDepartureStnCode(),
						CalendarUtil.getOfssetInMilisecondAddedDate(calander.getTime(), millsDiff));
				overlap.setStopDateLocal(stopDateLocal);
			}

			// save the splitted flight schedule
			this.recalcualteASKsAndSaveSchedule(schedule);
			if (overlapinng) {

				overlap = OverlapDetailUtil.addOverlapDetailsToSchedule(overlap, schedule);
				this.recalcualteASKsAndSaveSchedule(overlap);

				schedule = OverlapDetailUtil.addOverlapDetailsToSchedule(schedule, overlap);
				schedule = setSegmentIDToLeg(schedule);
				flightSchedleDAO.saveFlightSchedule(schedule);
			}
			
			if (schedule.getScheduleId() != null) {
				newScheduleIdsFromSplit.add(schedule.getScheduleId());
			}

			// update relevent flights
			AffectedPeriod period = new AffectedPeriod(schedule.getStartDate(), schedule.getStopDate(), schedule.getFrequency());
			flightDAO.batchUpdateScheduleIdOfFlights(schedule.getScheduleId().intValue(), originalScheduleId, period);
			if (overlapinng)
				flightDAO.batchUpdateScheduleIdOfFlights(overlap.getScheduleId().intValue(), originalOverlapId.intValue(),
						PeriodsUtil.getPeriodForOverlapping(period, millsDiff, daysDiff));

			if (opStack.size() == 0) {

				this.recalcualteASKsAndSaveSchedule(newScheduleFromSplit);
				if (overlapinng) {

					newOverlapFromSplit = OverlapDetailUtil
							.addOverlapDetailsToSchedule(newOverlapFromSplit, newScheduleFromSplit);
					this.recalcualteASKsAndSaveSchedule(newOverlapFromSplit);

					newScheduleFromSplit = OverlapDetailUtil.addOverlapDetailsToSchedule(newScheduleFromSplit,
							newOverlapFromSplit);
					newScheduleFromSplit = setSegmentIDToLeg(newScheduleFromSplit);
					flightSchedleDAO.saveFlightSchedule(newScheduleFromSplit);
				}

				// update relevent flights
				AffectedPeriod newPeriod = new AffectedPeriod(newScheduleFromSplit.getStartDate(),
						newScheduleFromSplit.getStopDate(), newScheduleFromSplit.getFrequency());
				flightDAO.batchUpdateScheduleIdOfFlights(newScheduleFromSplit.getScheduleId().intValue(), originalScheduleId,
						newPeriod);
				newScheduleIdsFromSplit.add(newScheduleFromSplit.getScheduleId());
				if (overlapinng)
					flightDAO.batchUpdateScheduleIdOfFlights(newOverlapFromSplit.getScheduleId().intValue(),
							originalOverlapId.intValue(), PeriodsUtil.getPeriodForOverlapping(newPeriod, millsDiff, daysDiff));

				return;
			} else {

				// call the Split for the right hand part
				this.splitSchedule(newScheduleFromSplit, opStack, originalScheduleId, newOverlapFromSplit, originalOverlapId,
						millsDiff, daysDiff, responce, newScheduleIdsFromSplit, isScheduleMessageProcess);
			}
		} else if (operation == SplitInstructions.LEFT_SPLIT) {

			newScheduleFromSplit.setStopDate(splitDate);
			stopDateLocal = timeAdder.getLocalDateTime(newScheduleFromSplit.getDepartureStnCode(), splitDate);
			newScheduleFromSplit.setStopDateLocal(stopDateLocal);
			if (overlapinng) {
				newOverlapFromSplit.setStopDate(CalendarUtil.getOfssetInMilisecondAddedDate(splitDate, millsDiff));
				stopDateLocal = timeAdder.getLocalDateTime(newOverlapFromSplit.getDepartureStnCode(),
						CalendarUtil.getOfssetInMilisecondAddedDate(splitDate, millsDiff));
				newOverlapFromSplit.setStopDateLocal(stopDateLocal);
			}
			GregorianCalendar calander = new GregorianCalendar();
			calander.setTime(splitDate);
			calander.add(GregorianCalendar.DATE, 1);
			schedule.setStartDate(calander.getTime());
			startDateLocal = timeAdder.getLocalDateTime(schedule.getDepartureStnCode(), calander.getTime());
			schedule.setStartDateLocal(startDateLocal);

			if (overlapinng) {
				overlap.setStartDate(CalendarUtil.getOfssetInMilisecondAddedDate(calander.getTime(), millsDiff));
				startDateLocal = timeAdder.getLocalDateTime(overlap.getDepartureStnCode(),
						CalendarUtil.getOfssetInMilisecondAddedDate(calander.getTime(), millsDiff));
				overlap.setStartDateLocal(startDateLocal);
			}

			// save the splitted flight schedule
			this.recalcualteASKsAndSaveSchedule(schedule);
			if (overlapinng) {

				overlap = OverlapDetailUtil.addOverlapDetailsToSchedule(overlap, schedule);
				this.recalcualteASKsAndSaveSchedule(overlap);

				schedule = OverlapDetailUtil.addOverlapDetailsToSchedule(schedule, overlap);
				schedule = setSegmentIDToLeg(schedule);
				flightSchedleDAO.saveFlightSchedule(schedule);
			}
			if (schedule.getScheduleId() != null) {
				newScheduleIdsFromSplit.add(schedule.getScheduleId());
			}
			// update relevent flights
			AffectedPeriod period = new AffectedPeriod(schedule.getStartDate(), schedule.getStopDate(), schedule.getFrequency());
			flightDAO.batchUpdateScheduleIdOfFlights(schedule.getScheduleId().intValue(), originalScheduleId, period);
			if (overlapinng)
				flightDAO.batchUpdateScheduleIdOfFlights(overlap.getScheduleId().intValue(), originalOverlapId.intValue(),
						PeriodsUtil.getPeriodForOverlapping(period, millsDiff, daysDiff));

			if (opStack.size() == 0) {

				this.recalcualteASKsAndSaveSchedule(newScheduleFromSplit);
				if (overlapinng) {

					newOverlapFromSplit = OverlapDetailUtil
							.addOverlapDetailsToSchedule(newOverlapFromSplit, newScheduleFromSplit);
					this.recalcualteASKsAndSaveSchedule(newOverlapFromSplit);

					newScheduleFromSplit = OverlapDetailUtil.addOverlapDetailsToSchedule(newScheduleFromSplit,
							newOverlapFromSplit);
					newScheduleFromSplit = setSegmentIDToLeg(newScheduleFromSplit);
					flightSchedleDAO.saveFlightSchedule(newScheduleFromSplit);
				}

				// update relevent flights
				AffectedPeriod newPeriod = new AffectedPeriod(newScheduleFromSplit.getStartDate(),
						newScheduleFromSplit.getStopDate(), newScheduleFromSplit.getFrequency());
				flightDAO.batchUpdateScheduleIdOfFlights(newScheduleFromSplit.getScheduleId().intValue(), originalScheduleId,
						newPeriod);
				newScheduleIdsFromSplit.add(newScheduleFromSplit.getScheduleId());
				if (overlapinng)
					flightDAO.batchUpdateScheduleIdOfFlights(newOverlapFromSplit.getScheduleId().intValue(),
							originalOverlapId.intValue(), PeriodsUtil.getPeriodForOverlapping(newPeriod, millsDiff, daysDiff));

				return;
			} else {

				// call the Split for the left hand part
				this.splitSchedule(newScheduleFromSplit, opStack, originalScheduleId, newOverlapFromSplit, originalOverlapId,
						millsDiff, daysDiff, responce, newScheduleIdsFromSplit, isScheduleMessageProcess);
			}
		} else if (operation == SplitInstructions.FREQUENCY_SPLIT) {

			if (!AppSysParamsUtil.isAllowedUpdateScheduledFlightToOutOfSchedOrFreq()) {

				if (!FrequencyUtil.chackIfSubFrequency(schedule.getFrequency(), frequency)) { // throw exception
					throw new ModuleException("airschedules.logic.bl.schedule.invalidfrequncy");
				}
			}
			// save the splitted flight schedule
			newScheduleFromSplit.setFrequency(frequency);

			this.recalcualteASKsAndSaveSchedule(newScheduleFromSplit);
			if (overlapinng) {
				Frequency fqn = FrequencyUtil.getCopyOfFrequency(frequency);
				newOverlapFromSplit.setFrequency(FrequencyUtil.shiftFrequencyBy(fqn, daysDiff));
				newOverlapFromSplit = OverlapDetailUtil.addOverlapDetailsToSchedule(newOverlapFromSplit, newScheduleFromSplit);
				this.recalcualteASKsAndSaveSchedule(newOverlapFromSplit);

				newScheduleFromSplit = OverlapDetailUtil.addOverlapDetailsToSchedule(newScheduleFromSplit, newOverlapFromSplit);
				newScheduleFromSplit = setSegmentIDToLeg(newScheduleFromSplit);
				flightSchedleDAO.saveFlightSchedule(newScheduleFromSplit);
			}

			// update relevent flights
			AffectedPeriod periodfn = new AffectedPeriod(schedule.getStartDate(), schedule.getStopDate(), frequency);
			flightDAO.batchUpdateScheduleIdOfFlights(newScheduleFromSplit.getScheduleId().intValue(), originalScheduleId,
					periodfn);			
			newScheduleIdsFromSplit.add(newScheduleFromSplit.getScheduleId());
			
			if (overlapinng)
				flightDAO.batchUpdateScheduleIdOfFlights(newOverlapFromSplit.getScheduleId().intValue(),
						originalOverlapId.intValue(), PeriodsUtil.getPeriodForOverlapping(periodfn, millsDiff, daysDiff));

			// save the splitted flight schedule
			Frequency restFqn = FrequencyUtil.getRestOfTheFrequency(schedule.getFrequency(), frequency);
			schedule.setFrequency(restFqn);

			this.recalcualteASKsAndSaveSchedule(schedule);
			if (overlapinng) {
				Frequency fqn = FrequencyUtil.getCopyOfFrequency(frequency);
				overlap.setFrequency(FrequencyUtil.shiftFrequencyBy(restFqn, daysDiff));
				overlap = OverlapDetailUtil.addOverlapDetailsToSchedule(overlap, schedule);
				this.recalcualteASKsAndSaveSchedule(overlap);

				schedule = OverlapDetailUtil.addOverlapDetailsToSchedule(schedule, overlap);
				schedule = setSegmentIDToLeg(schedule);
				flightSchedleDAO.saveFlightSchedule(schedule);
			}
			if (schedule.getScheduleId() != null) {
				newScheduleIdsFromSplit.add(schedule.getScheduleId());
			}
			// update relevent flights
			AffectedPeriod period = new AffectedPeriod(schedule.getStartDate(), schedule.getStopDate(), restFqn);
			flightDAO.batchUpdateScheduleIdOfFlights(schedule.getScheduleId().intValue(), originalScheduleId, period);
			if (overlapinng)
				flightDAO.batchUpdateScheduleIdOfFlights(overlap.getScheduleId().intValue(), originalOverlapId.intValue(),
						PeriodsUtil.getPeriodForOverlapping(period, millsDiff, daysDiff));

		} else {
			// thorow exception
			throw new ModuleException("airschedules.arg.invalid");
		}
	}

	private void recalcualteASKsAndSaveSchedule(FlightSchedule schedule) throws ModuleException {

		if (schedule.getBuildStatusCode().equals(ScheduleBuildStatusEnum.BUILT.getCode())) {

			// creating flights for the schedule
			AffectedPeriod period = new AffectedPeriod(schedule.getStartDate(), schedule.getStopDate(), schedule.getFrequency());
			Collection<Flight> flights = ScheduledFlightUtil.getFlightsForSchdule(schedule, period);

			// number of flights for schedule
			int numberOfFlights = flights.size();
			// aircraft model number
			String modelNumber = schedule.getModelNumber();
			AircraftModel model = aircraftBD.getAircraftModel(modelNumber);

			// avilable seat kilometers
			int allocation = CalculationUtil.getAllocation(model.getAircraftCabinCapacitys());
			double flightDistance = this.getFlightDistance(schedule.getFlightScheduleLegs()).doubleValue();

			int availableSeatKilosShed = CalculationUtil.getASKforSchedule(numberOfFlights, allocation, flightDistance);

			schedule.setNumberOfDepartures(new Integer(numberOfFlights));
			schedule.setAvailableSeatKilometers(new Integer(availableSeatKilosShed));
		}

		flightSchedleDAO.saveFlightSchedule(schedule);
	}

	/**
	 * private method to get the flight distance for the flight
	 * 
	 * @param legList
	 * @return flight distance
	 * @throws ModuleException
	 */
	private BigDecimal getFlightDistance(Collection<FlightScheduleLeg> legList) throws ModuleException {
		BigDecimal flightDistance = AccelAeroCalculator.getDefaultBigDecimalZero();

		Iterator<FlightScheduleLeg> itLegs = legList.iterator();
		while (itLegs.hasNext()) {

			FlightScheduleLeg leg = (FlightScheduleLeg) itLegs.next();
			RouteInfo routeInfo = commonMasterBD.getRouteInfo(leg.getModelRouteId());
			flightDistance = AccelAeroCalculator.add(flightDistance, routeInfo.getDistance());
		}

		return flightDistance;
	}

	private FlightSchedule setSegmentIDToLeg(FlightSchedule schedule) {

		Set<FlightScheduleLeg> legSet = schedule.getFlightScheduleLegs();
		Iterator<FlightScheduleLeg> it = legSet.iterator();
		while (it.hasNext()) {

			FlightScheduleLeg leg = (FlightScheduleLeg) it.next();
			leg.setScheduleId(schedule.getScheduleId());
		}
		return schedule;
	}
	
	@SuppressWarnings("unchecked")
	private void splitDSTSubScheduleAndPublishSSM(FlightSchedule schedule, Collection<Integer> newScheduleIdsFromSplit, FlightSchedule targetSchedule)
			throws ModuleException {

		Command command = (Command) AirSchedulesUtil.getBean(CommandNames.SPLIT_SUB_SCHEDULE);
		command.setParameter(CommandParamNames.FLIGHT_SCHEDULE, schedule);
		command.setParameter(CommandParamNames.IS_SCHEDULE_SPLIT, true);
		command.setParameter(CommandParamNames.SCHEDULE_IDS, newScheduleIdsFromSplit);

		ServiceResponce serviceResponce = command.execute();

		if (serviceResponce.isSuccess()) {
			Collection<SSMSplitFlightSchedule> existingSubSchedules = (Collection<SSMSplitFlightSchedule>) serviceResponce
					.getResponseParam(CommandParamNames.EXISITNG_SSM_SUB_SCHEDULES);
			Collection<SSMSplitFlightSchedule> updatedSubSchedules = (Collection<SSMSplitFlightSchedule>) serviceResponce
					.getResponseParam(CommandParamNames.NEW_SSM_SUB_SCHEDULES);

			GDSPublishingBD gdsPublishingBD = AirSchedulesUtil.getGDSPublishingBD();
			GDSScheduleEventCollector sGdsEvents = new GDSScheduleEventCollector();
			sGdsEvents.addAction(GDSScheduleEventCollector.SPLIT_SCHEDULE);
			sGdsEvents.setExistingSubSchedules(existingSubSchedules);
			sGdsEvents.setUpdatedSubSchedules(updatedSubSchedules);
			Collection<FlightSchedule> splitSchedules = new ArrayList<FlightSchedule>();
			
			if (targetSchedule != null) {
				splitSchedules.add(targetSchedule);
			} else {
				splitSchedules = flightSchedleDAO.getFlightSchedules(newScheduleIdsFromSplit);
			}
			sGdsEvents.setSplitSchedules(splitSchedules);
			sGdsEvents.setExistingSchedule(schedule);
			sGdsEvents.setSupplementaryInfo("SCHEDULE SPLIT");

			Envelope env = new Envelope(GDSSchedConstants.TargetLogicNames.PUBLISH_SCHEDULE_SPLIT);
			env.addParam(GDSSchedConstants.ParamNames.GDS_EVENT_COLLECTOR, sGdsEvents);

			gdsPublishingBD.publishMessage(env);
		}

	}

	private void reviseScheduleFrequencyPeriodDeleteInvalidSchedules(Collection<Integer> newScheduleIdsFromSplit,
			boolean isScheduleMessageProcess) throws ModuleException {

		if (!newScheduleIdsFromSplit.isEmpty()) {
			Iterator<Integer> newScheduleIdsFromSplitItr = newScheduleIdsFromSplit.iterator();
			while (newScheduleIdsFromSplitItr.hasNext()) {
				Integer scheduleId = newScheduleIdsFromSplitItr.next();
				FlightSchedule schedule = flightSchedleDAO.getFlightSchedule(scheduleId.intValue());

				boolean freqMatched = CalendarUtil.frequencyMatchesDateRange(schedule.getStartDate(), schedule.getStopDate(),
						schedule.getFrequency());
				boolean isRemoved = false;
				// schedule frequency will be revised if its invalid when processing SSM/SSIM
				if (!freqMatched) {
					Frequency frequency = CalendarUtil.getValidFrequencyMatchesDateRange(schedule.getStartDate(),
							schedule.getStopDate(), schedule.getFrequency());
					if (!reValidateRevisedFrequency(frequency)) {
						// cancel schedule
						AirSchedulesUtil.getScheduleBD().cancelSchedule(scheduleId, true, new FlightAlertDTO(),
								true, true);
						newScheduleIdsFromSplitItr.remove();
						isRemoved = true;
					} else {						
						schedule.setFrequency(frequency);
						flightSchedleDAO.saveFlightSchedule(schedule);
					}
				}				
				

				if (!isRemoved && trimSchedulePeriod(schedule)) {
					FlightSchedule scheduleTrim = flightSchedleDAO.getFlightSchedule(scheduleId.intValue());
					scheduleTrim
							.setStartDate(CalendarUtil.getEffectiveStartDate(schedule.getStartDate(), schedule.getFrequency()));
					scheduleTrim.setStartDateLocal(
							timeAdder.getLocalDateTime(schedule.getDepartureStnCode(), schedule.getStartDate()));
					scheduleTrim.setStopDate(CalendarUtil.getEffectiveStopDate(schedule.getStopDate(), schedule.getFrequency()));
					scheduleTrim
							.setStopDateLocal(timeAdder.getLocalDateTime(schedule.getDepartureStnCode(), schedule.getStopDate()));
					flightSchedleDAO.saveFlightSchedule(scheduleTrim);
				}
				
			}
		}
	}
	
	private boolean reValidateRevisedFrequency(Frequency frequency) {
		if (frequency.getDayCount(true) > 0) {
			return true;
		}
		return false;
	}	
	
	private FlightSchedule getTargetedSplitSchedule(AffectedPeriod period, boolean datesInLocal,
			Collection<Integer> newScheduleIdsFromSplit) throws ModuleException {

		if (newScheduleIdsFromSplit != null && !newScheduleIdsFromSplit.isEmpty()) {

			Frequency frq = CalendarUtil.getValidFrequencyMatchesDateRange(period.getStartDate(), period.getEndDate(),
					period.getFrequency());

			Iterator<Integer> newScheduleIdsFromSplitItr = newScheduleIdsFromSplit.iterator();
			while (newScheduleIdsFromSplitItr.hasNext()) {
				Integer scheduleId = newScheduleIdsFromSplitItr.next();
				FlightSchedule schedule = flightSchedleDAO.getFlightSchedule(scheduleId.intValue());
				Date startDate = schedule.getStartDate();
				Date stopDate = schedule.getStopDate();
				Frequency frequency = schedule.getFrequency();
				if (datesInLocal) {
					timeAdder.addLocalTimeDetailsToFlightSchedule(schedule, false);
					startDate = schedule.getStartDateLocal();
					stopDate = schedule.getStopDateLocal();
					frequency = schedule.getFrequencyLocal();
				}

				boolean startDaySame = CalendarUtil.isSameDay(startDate, period.getStartDate());
				boolean endDaySame = CalendarUtil.isSameDay(stopDate, period.getEndDate());
				boolean isSameDays = FrequencyUtil.isEqualFrequency(period.getFrequency(), frq);

				if (startDaySame && endDaySame && isSameDays) {
					return schedule;
				}

			}
		}
		return null;
	}
	
	private boolean trimSchedulePeriod(FlightSchedule schedule) {
		Date splitStartDateZulu = schedule.getStartDate();
		Date splitEndDateZulu = schedule.getStopDate();
		Frequency splitFrequencyZulu = schedule.getFrequency();
		Frequency frequency = CalendarUtil.getValidFrequencyMatchesDateRange(splitStartDateZulu, splitEndDateZulu,
				splitFrequencyZulu);

		Date firstFlightDate = CalendarUtil.getEffectiveStartDate(splitStartDateZulu, frequency);
		Date lastFlightDate = CalendarUtil.getEffectiveStopDate(splitEndDateZulu, frequency);

		if (!CalendarUtil.isSameDay(splitStartDateZulu, firstFlightDate)
				|| !CalendarUtil.isSameDay(splitEndDateZulu, lastFlightDate)) {
			return true;
		}
		return false;
	}
	
	private void deleteAdHocFlightSchedules(Collection<Integer> newScheduleIdsFromSplit) throws ModuleException {
		if (!newScheduleIdsFromSplit.isEmpty()) {
			Iterator<Integer> newScheduleIdsFromSplitItr = newScheduleIdsFromSplit.iterator();
			while (newScheduleIdsFromSplitItr.hasNext()) {
				Integer scheduleId = newScheduleIdsFromSplitItr.next();
				FlightSchedule schedule = flightSchedleDAO.getFlightSchedule(scheduleId.intValue());

				if (CalendarUtil.isSameDay(schedule.getStartDate(), schedule.getStopDate())) {
					Collection<Flight> flights = flightSchedleDAO.getFlightsForSchedule(scheduleId);
					if (flights != null && !flights.isEmpty()) {
						for (Flight flight : flights) {
							if (!FlightStatusEnum.CANCELLED.getCode().equals(flight.getStatus())) {
								flight.setScheduleId(null);
								flightDAO.saveFlight(flight);

							}
						}
					}

					AirSchedulesUtil.getScheduleBD().cancelSchedule(scheduleId, true, new FlightAlertDTO(), true, true);
					newScheduleIdsFromSplitItr.remove();
				}

			}
		}

	}
}
