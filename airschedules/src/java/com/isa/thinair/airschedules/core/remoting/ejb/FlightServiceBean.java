/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.remoting.ejb;

import com.isa.thinair.airmaster.api.service.AirmasterUtils;

import java.sql.Blob;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;
import org.springframework.util.CollectionUtils;

import com.isa.thinair.airinventory.api.dto.InvRollForwardFlightsSearchCriteria;
import com.isa.thinair.airinventory.api.dto.baggage.FlightBaggageDTO;
import com.isa.thinair.airinventory.api.dto.meal.FlightMealDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableConnectedFlight;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.RollForwardFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatmap.FlightSeatsDTO;
import com.isa.thinair.airmaster.api.model.Airport;
import com.isa.thinair.airmaster.api.model.AirportDST;
import com.isa.thinair.airmaster.api.model.Station;
import com.isa.thinair.airmaster.api.service.AirmasterUtils;
import com.isa.thinair.airmaster.core.persistence.dao.StationDAO;
import com.isa.thinair.airproxy.api.model.reservation.availability.CalendarSearchRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airschedules.api.criteria.FlightFlightSegmentSearchCriteria;
import com.isa.thinair.airschedules.api.criteria.FlightSearchCriteria;
import com.isa.thinair.airschedules.api.dto.AvailableFlightSearchCriteria;
import com.isa.thinair.airschedules.api.dto.BasicFlightDTO;
import com.isa.thinair.airschedules.api.dto.DisplayFlightDTO;
import com.isa.thinair.airschedules.api.dto.FlightAlertDTO;
import com.isa.thinair.airschedules.api.dto.FlightAllowActionDTO;
import com.isa.thinair.airschedules.api.dto.FlightBasicInfoDTO;
import com.isa.thinair.airschedules.api.dto.FlightMealNotifyStatusDTO;
import com.isa.thinair.airschedules.api.dto.FlightSummaryDTO;
import com.isa.thinair.airschedules.api.dto.PastFlightInfoDTO;
import com.isa.thinair.airschedules.api.dto.RouteInfoTO;
import com.isa.thinair.airschedules.api.dto.ScheduleSearchDTO;
import com.isa.thinair.airschedules.api.dto.TransferRollForwardFlightsSearchCriteria;
import com.isa.thinair.airschedules.api.dto.TransitAirport;
import com.isa.thinair.airschedules.api.dto.WildcardFlightSearchDto;
import com.isa.thinair.airschedules.api.dto.WildcardFlightSearchRespDto;
import com.isa.thinair.airschedules.api.dto.rm.RMFlightSummaryDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.FlightSegNotificationEvent;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.AnciDefaultTemplStatusEnum;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.airschedules.api.utils.ScheduleBuildStatusEnum;
import com.isa.thinair.airschedules.api.utils.ScheduleStatusEnum;
import com.isa.thinair.airschedules.api.utils.SegmentUtil;
import com.isa.thinair.airschedules.core.bl.FlightScheduleBL;
import com.isa.thinair.airschedules.core.bl.audit.AuditAirSchedules;
import com.isa.thinair.airschedules.core.bl.search.SearchAvailableConnectionsForReprotection;
import com.isa.thinair.airschedules.core.persistence.dao.FlightDAO;
import com.isa.thinair.airschedules.core.persistence.dao.FlightLegDAO;
import com.isa.thinair.airschedules.core.persistence.dao.FlightScheduleDAO;
import com.isa.thinair.airschedules.core.persistence.dao.FlightScheduleJdbcDAO;
import com.isa.thinair.airschedules.core.persistence.dao.FlightSegementDAO;
import com.isa.thinair.airschedules.core.persistence.dao.UpdatePaxETStatusDAO;
import com.isa.thinair.airschedules.core.service.bd.FlightBDImpl;
import com.isa.thinair.airschedules.core.service.bd.FlightBDLocalImpl;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.airschedules.core.utils.CommandNames;
import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.util.OndCombinationUtil;
import com.isa.thinair.commons.core.framework.Command;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.ServiceResponce;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;
import org.springframework.util.CollectionUtils;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import java.sql.Blob;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Session bean to provide Flight Service related functionalities
 * 
 * @author Lasantha Pambagoda
 */
@Stateless
@RemoteBinding(jndiBinding = "FlightService.remote")
@LocalBinding(jndiBinding = "FlightService.local")
@RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class FlightServiceBean extends PlatformBaseSessionBean implements FlightBDImpl, FlightBDLocalImpl {

	private static final Log log = LogFactory.getLog(FlightServiceBean.class);

	FlightScheduleBL flightScheduleBL = null;

	/**
	 * method to create the flight
	 * 
	 * 
	 */
	@Override
	public ServiceResponce recalculateLocalTime(Airport airport, Date gmtDstEffectiveFrom, Date gmtDstEffectiveTo,
			Collection<AirportDST> dsttos, FlightAlertDTO flightAlertDTO) throws ModuleException {

		Command command = (Command) AirSchedulesUtil.getInstance().getLocalBean(CommandNames.RECALCULATE_LOCAL_TIME_MACRO);
		command.setParameter(CommandParamNames.COMMAND_NAME, CommandNames.RECALCULATE_LOCAL_TIME_MACRO);
		command.setParameter(CommandParamNames.AIRPORT, airport);
		command.setParameter(CommandParamNames.START_DATE, gmtDstEffectiveFrom);
		command.setParameter(CommandParamNames.END_DATE, gmtDstEffectiveTo);
		command.setParameter(CommandParamNames.AIRPORT_DST, dsttos);
		command.setParameter(CommandParamNames.FLIGHT_ALERT_DTO, flightAlertDTO);
		return this.execute(command);
	}

	/**
	 * methos to get flight for a given flight id
	 * 
	 * 
	 */
	@Override
	public Flight getFlight(int id) throws ModuleException {
		try {
			return getFlightDAO().getFlight(id);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	public List<Flight> getLightWeightFlightDetails(Collection<Integer> flightIds) throws ModuleException {
		try {
			return getFlightDAO().getLightWeightFlightDetails(flightIds);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * method to get flight for a given flight number
	 * 
	 * 
	 */
	@Override
	public List<Flight> getFlightDetails(String flightNumber) throws ModuleException {
		try {
			return getFlightDAO().getFlightDetails(flightNumber);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * Get the details of specific flight for given date
	 */
	@Override
	public Flight getSpecificFlightDetail(FlightSearchCriteria searchCriteria)
			throws ModuleException {
		try {
			return getFlightDAO().getSpecificFlightDetail(searchCriteria);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());

		}
	}
	
	/**
	 * Get the details of specific flight for given date
	 */
	@Override
	public FlightSegement getFlightSegmentForLocalDate(String flightNumber, Date departureDateLocal) throws ModuleException {
		try {
			return getFlightDAO().getFlightSegmentForLocalDate(flightNumber, departureDateLocal);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());

		}
	}

	/**
	 * Get the details of specific flight for given date
	 */
	@Override
	public FlightSegement getAllFlightSegmentForLocalDate(String flightNumber, Date departureDateLocal) throws ModuleException {
		try {
			return getFlightDAO().getAllFlightSegmentForLocalDate(flightNumber, departureDateLocal);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());

		}
	}

	@Override
	public String getFlightDates(Collection<Integer> flightIds, int pageSize, SimpleDateFormat format) throws ModuleException {
		try {
			List<Date> dates = getFlightDAO().getFlightDates(flightIds);
			String formattedDates = "";
			int maxRecords = Math.min(pageSize, dates.size());
			for (int i = 0; i < maxRecords; i++) {
				if (i != 0) {
					formattedDates += ",";
				}
				formattedDates += format.format(dates.get(i));
			}
			if (pageSize < dates.size()) {
				formattedDates += ", and " + (dates.size() - maxRecords) + " more days...";
			}
			return formattedDates;
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * mehtod to cancel getFlightSummary
	 * 
	 * 
	 */
	@Override
	public Collection<FlightSummaryDTO> getFlightSummary(InvRollForwardFlightsSearchCriteria criteria) throws ModuleException {
		try {
			return getFlightDAO().getLikeFlightSummary(criteria);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * mehtod to cancel getLikeFlightSummary
	 * 
	 * 
	 */
	@Override
	public Collection<FlightSummaryDTO> getLikeFlightSummary(InvRollForwardFlightsSearchCriteria criteria) throws ModuleException {
		try {
			return getFlightDAO().getLikeFlightSummary(criteria);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	public List<Integer> getSeatTemplateAttachedFlights(Collection<Integer> flightIdList) {
		return getFlightDAO().getSeatTemplateAttachedFlights(flightIdList);
	}

	/**
	 * mehtod to cancel getLikeFlights
	 * 
	 * 
	 */
	@Override
	public List<Flight> getLikeFlights(TransferRollForwardFlightsSearchCriteria criteria) throws ModuleException {
		try {
			return getFlightDAO().getLikeFlights(criteria);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * method to getFlightsForModel
	 * 
	 * @throws ModuleException
	 * 
	 */
	@Override
	public Collection<Integer> getFlightIDsForModel(String modelNumber, Date departureDateFrom) throws ModuleException {
		try {
			return getFlightDAO().getFlightIDsForModel(modelNumber, departureDateFrom);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	public Collection<Flight> getFlightsForModel(String modelNumber, Date departureDateFrom) throws ModuleException {
		try {
			return getFlightDAO().getFlightsForModel(modelNumber, departureDateFrom);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}
	
	@Override
	public Collection<Flight> getFutureNonCNXFlightsForModel(String modelNumber, Date departureDateFrom) throws ModuleException {
		try {
			return getFlightDAO().getFutureNonCNXFlightsForModel(modelNumber, departureDateFrom);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	public Collection<FlightSchedule> getFlightSchdulesForModel(String modelNumber) throws ModuleException {
		try {
			return getFlightDAO().getFlightScheduleForModel(modelNumber);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * method to getFlightsForTerminal
	 * 
	 * @throws ModuleException
	 * 
	 */
	public Collection<Integer> getFlightsForTerminal(Integer terminalId) throws ModuleException {
		try {
			return getFlightDAO().getFlightsForTerminal(terminalId);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * method to getSchedulesForTerminal
	 * 
	 * @throws ModuleException
	 * 
	 */
	public Collection<Integer> getSchedulesForTerminal(Integer terminalId) throws ModuleException {
		try {
			List<Integer> list = getFlightScheduleDAO().getSchedulesForTerminal(terminalId);
			return list;
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * method to cancel flights
	 * 
	 * 
	 */
	@Override
	public ServiceResponce cancelFlights(Collection<Integer> flightIds, boolean forceCancel) throws ModuleException {
		return null;
	}

	/**
	 * method to serch flights according to the given criteria
	 * 
	 * 
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Page searchFlightsForDisplay(List<ModuleCriterion> criteria, int startIndex, int pageSize, boolean isDatesInLoacal,
			FlightFlightSegmentSearchCriteria flightCriteria, boolean isFlightManifest, boolean isReporting, List<String> agentApplicableOnds)
			throws ModuleException {

		Command command = (Command) AirSchedulesUtil.getInstance().getLocalBean(CommandNames.SEARCH_FLIGHTS_FOR_DISPLAY_MACRO);
		command.setParameter(CommandParamNames.DISPLAY, new Boolean(true));
		command.setParameter(CommandParamNames.CRITERIA_LIST, criteria);
		command.setParameter(CommandParamNames.START_INDEX, new Integer(startIndex));
		command.setParameter(CommandParamNames.PAGE_SIZE, new Integer(pageSize));
		command.setParameter(CommandParamNames.IS_DATE_IN_LOCAL, new Boolean(isDatesInLoacal));
		command.setParameter(CommandParamNames.FLIGHT_FLIGHTSEGMENT_CRITERIA_LIST, flightCriteria);
		command.setParameter(CommandParamNames.IS_FLIGHT_MANIFEST, isFlightManifest);
		command.setParameter(CommandParamNames.IS_REPORTING, isReporting);
		command.setParameter(CommandParamNames.AGENT_APPLICABLE_ONDS, agentApplicableOnds);

		DefaultServiceResponse responce = (DefaultServiceResponse) command.execute();

		return (Page) responce.getResponseParam(CommandParamNames.PAGE);
	}

	/**
	 * method to create the flight
	 * 
	 * 
	 */
	@Override
	public ServiceResponce createFlight(FlightAllowActionDTO allowAction, Flight flight, boolean isEnableAttachDefaultAnciTemplate) throws ModuleException {

		Command command = (Command) AirSchedulesUtil.getInstance().getLocalBean(CommandNames.CREATE_FLIGHT_MACRO);
		command.setParameter(CommandParamNames.COMMAND_NAME, CommandNames.CREATE_FLIGHT_MACRO);
		flight = (Flight)setUserDetails(flight);
		command.setParameter(CommandParamNames.FLIGHT, flight);
		command.setParameter(CommandParamNames.FLIGHT_ALLOW_ACTION_DTO, allowAction);
		command.setParameter(CommandParamNames.USER_PRINCIPAL, getUserPrincipal());
		command.setParameter(CommandParamNames.IS_ENABLE_ATACH_DEFAULT_ANCI_TEMPL, isEnableAttachDefaultAnciTemplate);
		return this.execute(command);
	}

	/**
	 * mehtod to cancel flight
	 * 
	 * 
	 */
	@Override
	public ServiceResponce cancelFlight(int flightId, boolean forceCancel, FlightAlertDTO flightAlertDTO) throws ModuleException {

		Command command = (Command) AirSchedulesUtil.getInstance().getLocalBean(CommandNames.CANCEL_FLIGHT_MACRO);
		command.setParameter(CommandParamNames.COMMAND_NAME, CommandNames.CANCEL_FLIGHT_MACRO);
		command.setParameter(CommandParamNames.FLIGHT_ID, new Integer(flightId));
		command.setParameter(CommandParamNames.FORCE, new Boolean(forceCancel));
		command.setParameter(CommandParamNames.FLIGHT_ALERT_DTO, flightAlertDTO);
		command.setParameter(CommandParamNames.USER_PRINCIPAL, getUserPrincipal());
		return this.execute(command);
	}

	/**
	 * method to update the flight
	 * 
	 * 
	 */
	@Override
	public ServiceResponce updateFlight(FlightAllowActionDTO allowAction, Flight flight, FlightAlertDTO flightAlertDTO,
			boolean downgradeToAnyModel, boolean activatePastFlights, boolean isScheduleMessageProcessing,
			boolean isEnableAttachDefaultAnciTemplate) throws ModuleException {

		Command command = (Command) AirSchedulesUtil.getInstance().getLocalBean(CommandNames.AMMEND_FLIGHT_MACRO);
		command.setParameter(CommandParamNames.COMMAND_NAME, CommandNames.AMMEND_FLIGHT_MACRO);
		flight = (Flight)setUserDetails(flight);
		command.setParameter(CommandParamNames.FLIGHT, flight);
		command.setParameter(CommandParamNames.FLIGHT_ALLOW_ACTION_DTO, allowAction);
		command.setParameter(CommandParamNames.FLIGHT_ALERT_DTO, flightAlertDTO);
		command.setParameter(CommandParamNames.USER_PRINCIPAL, getUserPrincipal());
		command.setParameter(CommandParamNames.FLIGHT_DOWNGRADE_TO_ANY_MODEL, new Boolean(downgradeToAnyModel));
		command.setParameter(CommandParamNames.ACTIVATE_PAST_FLIGHT, new Boolean(activatePastFlights));
		command.setParameter(CommandParamNames.IS_SHEDULE_MESSAGE_PROCESS, isScheduleMessageProcessing);
		command.setParameter(CommandParamNames.IS_ENABLE_ATACH_DEFAULT_ANCI_TEMPL, isEnableAttachDefaultAnciTemplate);
		return this.execute(command);
	}

	/**
	 * method to get segments for given flight id
	 * 
	 * 
	 */
	@Override
	public Collection<FlightSegement> getSegments(int flightId) throws ModuleException {
		try {
			return ((FlightSegementDAO) AirSchedulesUtil.getInstance().getLocalBean(
					InternalConstants.DAOProxyNames.FLIGHT_SEGMENT_DAO)).getFlightSegments(flightId);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	public FlightSegement getFlightSegment(int flightSegmentIds) throws ModuleException {
		try {
			return ((FlightSegementDAO) AirSchedulesUtil.getInstance().getLocalBean(
					InternalConstants.DAOProxyNames.FLIGHT_SEGMENT_DAO)).getFlightSegement(flightSegmentIds);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	public void updateFlightSegment(FlightSegement flightSegment) throws ModuleException {
		try {
			((FlightSegementDAO) AirSchedulesUtil.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_SEGMENT_DAO))
					.saveFlightSegement(flightSegment);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * method to cancel flight
	 * 
	 * 
	 */
	@Override
	public ServiceResponce copyFlight(FlightAllowActionDTO allowAction, int flightId, Date newDate, boolean isTimeInLocal)
			throws ModuleException {

		Command command = (Command) AirSchedulesUtil.getInstance().getLocalBean(CommandNames.COPY_FLIGHT_MACRO);
		command.setParameter(CommandParamNames.COMMAND_NAME, CommandNames.COPY_FLIGHT_MACRO);
		command.setParameter(CommandParamNames.FLIGHT_ID, new Integer(flightId));
		command.setParameter(CommandParamNames.DEPARTURE_DATE, newDate);
		command.setParameter(CommandParamNames.FLIGHT_ALLOW_ACTION_DTO, allowAction);
		command.setParameter(CommandParamNames.IS_DATE_IN_LOCAL, new Boolean(isTimeInLocal));
		command.setParameter(CommandParamNames.USER_PRINCIPAL, getUserPrincipal());
		return this.execute(command);
	}

	/**
	 * method to get Possible Overlapping Flights
	 * 
	 * 
	 */
	@Override
	public Collection<Flight> getPossibleOverlappingFlights(Flight flight) throws ModuleException {

		Command command = (Command) AirSchedulesUtil.getInstance().getLocalBean(CommandNames.PARSE_FLIGHTS_TO_BACK);
		command.setParameter(CommandParamNames.FLIGHT, flight);
		ServiceResponce responce = command.execute();
		try {
			return getFlightDAO().getPossibleOverlappingFlights((Flight) responce.getResponseParam(CommandParamNames.FLIGHT));
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * method to closeFlightForReservation
	 * 
	 * 
	 */
	@Override
	public boolean closeFlightForReservation(int flightId, String segmentCode) throws ModuleException {
		try {
			return getFlightDAO().closeFlightForReservation(flightId, segmentCode);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * method to changeFlightStatus
	 * 
	 * 
	 */
	@Override
	public void changeFlightStatus(Collection<Integer> flights, FlightStatusEnum status) throws ModuleException {
		try {
			getFlightDAO().changeFlightStatus(flights, status);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * get the flight list using the given id to be published
	 * 
	 * @param flights
	 * @param status
	 * @param scheduleId
	 * @throws ModuleException
	 */
	private void addListOfFlightsToPublish(Collection<Integer> flights, FlightStatusEnum status, Integer scheduleId)
			throws ModuleException {

		if (AppSysParamsUtil.isFlightDetailsPublishingEnabled()) {// No point of doing a DB call if operation is not
																	// enabled

			Collection<Flight> flightsToPublish = new ArrayList<Flight>();

			if (flights != null && !flights.isEmpty()) {
				Flight flight = null;

				for (Integer flightId : flights) {
					flight = getFlight(flightId);
					flightsToPublish.add(flight);

				}
			} else if (scheduleId != null) {
				flightsToPublish.addAll(getFlightScheduleDAO().getFlightsForSchedule(scheduleId, status));

			}

			Command command = (Command) AirSchedulesUtil.getInstance().getLocalBean(CommandNames.ADD_FLIGHT_CHANGES_TO_PUBLISH);
			command.setParameter(CommandParamNames.FLIGHTS_TO_PUBLISH, flightsToPublish);
			ServiceResponce responce = command.execute();

		}
	}

	/**
	 * method to changeFlightStatus
	 * 
	 * 
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void manageFlightStatus(Collection<Integer> flights, FlightStatusEnum status) throws ModuleException {
		try {

			AuditAirSchedules auditAirSchedules = new AuditAirSchedules();
			auditAirSchedules.doAuditForFlightStatusChange(flights, status, getUserPrincipal());
			getFlightDAO().manageFlightStatus(flights, status);
			addListOfFlightsToPublish(flights, status, null);
		} catch (CommonsDataAccessException cdaex) {
			log.error("Module Exception manageFlightStatus", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * change flights status by passing schedule id
	 * <p>
	 * No need to validate current flight status here.
	 * 
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void changeFlightStatus(Integer scheduleID, FlightStatusEnum status) throws ModuleException {

		try {
			AuditAirSchedules auditAirSchedules = new AuditAirSchedules();
			auditAirSchedules.doAuditForFlightStatusChange(scheduleID, status, getUserPrincipal());
			// This call has to be before the update to identify the exact flights which needs to be published
			addListOfFlightsToPublish(null, status, scheduleID);
			getFlightDAO().reinstateFlights(scheduleID, status);
		} catch (CommonsDataAccessException cdaex) {
			log.error("Module Exception changeFlightStatus", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * method to searchAvailableFlights
	 * 
	 * 
	 */
	@Override
	public ServiceResponce searchAvailableFlights(AvailableFlightSearchDTO avifltSearchDTO) throws ModuleException {
		Command command = (Command) AirSchedulesUtil.getInstance().getLocalBean(CommandNames.FLIGHT_AVAILABILITY_SEARCH);
		command.setParameter(CommandParamNames.AVILABLE_FLIGHT_SEARCH_DTO, avifltSearchDTO);
		command.setParameter(CommandParamNames.SEARCH_OUT_BOUND_FLIGHTS, Boolean.TRUE);
		return command.execute();
	}

	@Override
	public ServiceResponce searchAvailableFlights(RollForwardFlightSearchDTO rollForwardSearch) throws ModuleException {
		Command command = (Command) AirSchedulesUtil.getInstance().getLocalBean(
				CommandNames.FLIGHT_AVAILABILITY_SEARCH_ROLL_FORWARD);
		command.setParameter(CommandParamNames.ROLL_FORWARD_FLIGHT_SEARCH, rollForwardSearch);
		return command.execute();
	}

	@Override
	public ServiceResponce searchAvailableFlights(CalendarSearchRQ searchRQ) throws ModuleException {
		Command command = (Command) AirSchedulesUtil.getInstance().getLocalBean(CommandNames.CALENDAR_AVAILABILITY_SEARCH);
		command.setParameter(CommandParamNames.CALENDAR_SEARCH_REQUEST, searchRQ);
		return command.execute();
	}

	/**
	 * method to getFilledSelectedFlight
	 * 
	 * 
	 */
	@Override
	public ServiceResponce getFilledSelectedFlight(AvailableFlightSearchDTO avifltSearchDTO, boolean isTransactional) throws ModuleException {

		Command command = (Command) AirSchedulesUtil.getInstance().getLocalBean(CommandNames.FILL_SELECTED_AVAILABLE_FLIGHT);
		command.setParameter(CommandParamNames.AVILABLE_FLIGHT_SEARCH_DTO, avifltSearchDTO);
		command.setParameter(CommandParamNames.IS_TRANSACTIONAL, isTransactional);
		return command.execute();
	}

	/**
	 * method to getScheduleFrequencyOfFlight
	 * 
	 * 
	 */
	@Override
	public Frequency getScheduleFrequencyOfFlight(int flightId) throws ModuleException {
		try {
			return getFlightDAO().getScheduleFrequencyOfFlight(flightId);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	public List<AvailableFlightSegment> getAvailableFlightSegments(Collection<Integer> flightSegmentIds, String classOfService,
			boolean isInboundFlight, int ondSequence, boolean ignoreFlightStatus, boolean isUnTouchedOnd) throws ModuleException {
		List<AvailableFlightSegment> allAvailableFlightSegments = new ArrayList<AvailableFlightSegment>();
		for (Integer fltSegId : flightSegmentIds) {
			AvailableFlightSegment availableFlightSegment = getFlightDAO().getAvailableFlightSegments(fltSegId, classOfService,
					isInboundFlight, ondSequence, ignoreFlightStatus, isUnTouchedOnd);
			allAvailableFlightSegments.add(availableFlightSegment);
		}

		return allAvailableFlightSegments;

	}

	/**
	 * method to getUsedAirports
	 * 
	 * 
	 */
	@Override
	public HashMap<String, String> getUsedAirports(Collection<String> airports) throws ModuleException {
		try {
			return ((FlightLegDAO) AirSchedulesUtil.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_LEG_DAO))
					.getUsedAirports(airports);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	public List<String> getAirportSequenceListForFlight(int flightId) throws ModuleException {

		try {
			return ((FlightLegDAO) AirSchedulesUtil.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_LEG_DAO))
					.getAirportSequenceListForFlight(flightId);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

	}

	/**
	 * public method to check whether reservations exist for a cancelled flight
	 * 
	 * @param flightId
	 * @param forceCancel
	 * @return hashMap of flight details with reservation info
	 * @throws ModuleException
	 * 
	 */
	@Override
	public ServiceResponce getFlightReservationsForCancel(int flightId, boolean forceCancel) throws ModuleException {

		Command command = (Command) AirSchedulesUtil.getInstance().getLocalBean(CommandNames.CHECK_RESERVATIONS);
		command.setParameter(CommandParamNames.COMMAND_NAME, CommandNames.CHECK_RESERVATIONS);
		command.setParameter(CommandParamNames.CHECK_RESERVATIONS_ONLY, new Boolean(true));
		command.setParameter(CommandParamNames.FLIGHT_ID, new Integer(flightId));
		command.setParameter(CommandParamNames.FORCE, new Boolean(forceCancel));
		return this.execute(command);
	}

	/**
	 * 
	 * @throws ModuleException
	 * 
	 */

	@Override
	public Timestamp getFlightForDepartureStation(int flightId, String departureStation) throws ModuleException {
		return getFlightDAO().getFlightForDepartureStation(flightId, departureStation);
	}

	/**
	 * public method to return collection of FlightSegments for the given flightSegmentIds
	 * 
	 * @param flightSegmentIds
	 * @return Collection of FlightSegments
	 * @throws ModuleException
	 * 
	 */
	@Override
	public Collection<FlightSegmentDTO> getFlightSegments(Collection<Integer> flightSegmentIds) throws ModuleException {

		try {
			FlightSegementDAO flightSegementDAO = (FlightSegementDAO) AirSchedulesUtil.getInstance().getLocalBean(
					InternalConstants.DAOProxyNames.FLIGHT_SEGMENT_DAO);
			return flightSegementDAO.getFlightSegments(flightSegmentIds);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * public method to return collection of FlightSegments for the given flightIds and segment code. If segmentCode is
	 * null will not filter by segment code.
	 * 
	 * @param flightIds
	 * @param segmentCode
	 *            Segment code the results should be filtered with. Will ignore filtering and return all results it if
	 *            set to null.
	 * @return Collection of FlightSegments
	 * @throws ModuleException
	 * 
	 */
	@Override
	public Collection<FlightSegmentDTO> getFlightSegments(Collection<Integer> flightSIds, String segmentCode)
			throws ModuleException {

		try {
			FlightSegementDAO flightSegementDAO = (FlightSegementDAO) AirSchedulesUtil.getInstance().getLocalBean(
					InternalConstants.DAOProxyNames.FLIGHT_SEGMENT_DAO);
			return flightSegementDAO.getFlightSegments(flightSIds, segmentCode);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * public method to return a list of destinations of a flight (identified by flight # and departure date) from a
	 * given airport in its route
	 * 
	 * @param flightNumber
	 * @param departureDate
	 * @param airportCode
	 * @return collection of detinations
	 * 
	 */
	@Override
	public List<String> getPossibleDestinations(String flightNumber, Date departureDate, String airportCode)
			throws ModuleException {

		try {
			return getFlightDAO().getPossibleDestinations(flightNumber, departureDate, airportCode);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * public method to return a collection of flight segments for a given flight no, airport and date/datetime
	 * 
	 * @param originAirportCode
	 * @param destAirportCode
	 * @param flightNo
	 * @param departureDateLocal
	 * @param dateOnly
	 * @return
	 * @throws ModuleException
	 * 
	 */
	@Override
	public Collection<FlightSegement> getFlightSegmentsForLocalDate(String originAirportCode, String destAirportCode,
			String flightNo, Date departureDateLocal, boolean dateOnly) throws ModuleException {

		try {
			FlightSegementDAO flightSegmentDao = ((FlightSegementDAO) AirSchedulesUtil.getInstance().getLocalBean(
					InternalConstants.DAOProxyNames.FLIGHT_SEGMENT_DAO));

			Collection<FlightSegement> segmentsList = null;

			if (!dateOnly) {
				segmentsList = flightSegmentDao.getFlightSegmentsForLocalDate(originAirportCode, destAirportCode, flightNo,
						departureDateLocal, dateOnly);
				if (segmentsList == null || segmentsList.size() == 0) {
					segmentsList = flightSegmentDao.getFlightSegmentsForLocalDate(originAirportCode, destAirportCode, flightNo,
							departureDateLocal, true);
				}
			} else {
				segmentsList = flightSegmentDao.getFlightSegmentsForLocalDate(originAirportCode, destAirportCode, flightNo,
						departureDateLocal, dateOnly);
			}

			return segmentsList;

		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	public WildcardFlightSearchRespDto searchFlightsWildcardBased(WildcardFlightSearchDto flightSearchDto) throws ModuleException {

		WildcardFlightSearchRespDto resp = new WildcardFlightSearchRespDto();
		FlightSegementDAO flightSegmentDao = ((FlightSegementDAO) AirSchedulesUtil.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.FLIGHT_SEGMENT_DAO));
		Map<String, List<FlightSegmentTO>> flightSegments;

		if (AppSysParamsUtil.isEnableRouteSelectionForAgents() && flightSearchDto.getAgentCode() != null) {
			if (!isEligibleAgentToSearchTheGivenRoute(flightSearchDto.getDepartureAirport(), flightSearchDto.getArrivalAirport(),
					flightSearchDto.getAgentCode())) {
				log.error("Agent " + flightSearchDto.getAgentCode() + " is not eligible for route "
						+ flightSearchDto.getDepartureAirport() + "/" + flightSearchDto.getArrivalAirport());
				throw new ModuleException("gdsservices.validators.agent.routeNotApplcable");
			}
		}

		flightSegments = flightSegmentDao.searchFlightsWildcardBased(flightSearchDto);
		List<String> flightNumberOptions = SegmentUtil.getPrioratizedFlightNumbers(flightSearchDto.getFlightNumber());

		for (String flightNumber : flightNumberOptions) {
			if (flightSegments.containsKey(flightNumber)) {
				resp.setBestMatch(flightSegments.get(flightNumber).get(0));
				resp.setDepartureTimeAmbiguous(flightSegments.get(flightNumber).size() > 1);
				break;
			}
		}

		resp.setFlightNumberAmbiguous(flightSegments.size() > 1);

		return resp;
	}

	public WildcardFlightSearchRespDto searchCodeShareFlightsWildcardBased(WildcardFlightSearchDto flightSearchDto) throws ModuleException {

		WildcardFlightSearchRespDto resp = new WildcardFlightSearchRespDto();
		FlightSegementDAO flightSegmentDao = ((FlightSegementDAO) AirSchedulesUtil.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.FLIGHT_SEGMENT_DAO));
		Map<String, List<FlightSegmentTO>> flightSegments;

		if (AppSysParamsUtil.isEnableRouteSelectionForAgents() && flightSearchDto.getAgentCode() != null) {
			if (!isEligibleAgentToSearchTheGivenRoute(flightSearchDto.getDepartureAirport(), flightSearchDto.getArrivalAirport(),
					flightSearchDto.getAgentCode())) {
				log.error("Agent " + flightSearchDto.getAgentCode() + " is not eligible for route "
						+ flightSearchDto.getDepartureAirport() + "/" + flightSearchDto.getArrivalAirport());
				throw new ModuleException("gdsservices.validators.agent.routeNotApplcable");
			}
		}

		flightSegments = flightSegmentDao.searchCodeShareFlightsWildcardBased(flightSearchDto);
		List<String> flightNumberOptions = SegmentUtil.getPrioratizedFlightNumbers(flightSearchDto.getCodeShareFlightNumber());

		for (String flightNumber : flightNumberOptions) {
			if (flightSegments.containsKey(flightNumber)) {
				resp.setBestMatch(flightSegments.get(flightNumber).get(0));
				resp.setDepartureTimeAmbiguous(flightSegments.get(flightNumber).size() > 1);
				break;
			}
		}

		resp.setFlightNumberAmbiguous(flightSegments.size() > 1);

		return resp;
	}

	/**
	 * public method to return all unique flight numbers for non-cancelled flights
	 * 
	 * @return
	 * @throws ModuleException
	 * 
	 */
	@Override
	public Collection<String> getFlightNumbers() throws ModuleException {

		try {
			return getFlightDAO().getFlightNumbers();
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * 
	 * @param flightSegmentDTOs
	 * @return
	 * @throws ModuleException
	 * 
	 */
	@Override
	public Collection<FlightSegmentDTO> getFilledFlightSegmentDTOs(Collection<FlightSegmentDTO> flightSegmentDTOs)
			throws ModuleException {
		try {
			return ((FlightSegementDAO) AirSchedulesUtil.getInstance().getLocalBean(
					InternalConstants.DAOProxyNames.FLIGHT_SEGMENT_DAO)).getFilledFlightSegmentDTOs(flightSegmentDTOs);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * publish the flights to GDS
	 * 
	 * @param scheduleId
	 * @param gdsIds
	 * @return
	 * @throws ModuleException
	 * 
	 */
	@Override
	public ServiceResponce publishFlightToGDS(Integer flightId, Collection<Integer> gdsIds, Integer version)
			throws ModuleException {
		Command command = (Command) AirSchedulesUtil.getInstance().getLocalBean(CommandNames.ADD_REOMVE_FLIGHT_G_D_S_MACRO);
		command.setParameter(CommandParamNames.FLIGHT_ID, flightId);
		command.setParameter(CommandParamNames.GDS_IDS, gdsIds);
		command.setParameter(CommandParamNames.VERSION_ID, new Integer(version));
		command.setParameter(CommandParamNames.USER_PRINCIPAL, getUserPrincipal());
		return this.execute(command);
	}

	/**
	 * 
	 * @return Flight ID
	 * @throws ModuleException
	 * 
	 */
	@Override
	public Integer getFlightID(String segCode, String fltNum, Date depDateZulu) throws ModuleException {
		try {
			return getFlightDAO().getFlightID(segCode, fltNum, depDateZulu);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}
	
	@Override
	public Integer getFlightID(String flightNumber, Date dateOfflight, String departureStation) throws ModuleException {
		try {
			return getFlightDAO().getFlightID(flightNumber, dateOfflight, departureStation);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}
	
	@Override
	public Map<String, String> getFlightLegs(int flightID) throws ModuleException {
		try {
			return getFlightDAO().getFlightLegs(flightID);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * 
	 * @return Flight Segment ID
	 * @throws ModuleException
	 * 
	 */
	@Override
	public RMFlightSummaryDTO getFlightSummary(String fltNum, Date depDateZulu) throws ModuleException {
		try {
			return getFlightDAO().getFlightSummary(fltNum, depDateZulu);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * 
	 * @return Flight Segment ID
	 * @throws ModuleException
	 * 
	 */
	@Override
	public Collection<Integer> getFlightSegIdsforSchedule(int scheduleId) throws ModuleException {
		try {
			return getFlightSegmentDAO().getFlightSegIdsforSchedule(scheduleId);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * 
	 * @return Flight Segment ID
	 * @throws ModuleException
	 * 
	 */
	@Override
	public Collection<FlightSeatsDTO> getFlightSegIdsforFlight(Collection<Integer> colFlightId) throws ModuleException {
		try {
			return getFlightSegmentDAO().getFlightSegIdsforFlight(colFlightId);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	private FlightSegementDAO getFlightSegmentDAO() {
		return ((FlightSegementDAO) AirSchedulesUtil.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.FLIGHT_SEGMENT_DAO));
	}

	/**
	 * method to getNumberOfFlightsInSchedule
	 * 
	 * 
	 */
	@Override
	public Integer[] getNumberOfFlightsInSchedule(Integer scheduleId) throws ModuleException {
		try {
			return getFlightDAO().getNumberOfFlightsInSchedule(scheduleId);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * method to serch flights according to the given criteria
	 * 
	 * @author Haider 03Sep08
	 * 
	 */
	@Override
	public HashMap<Integer, ArrayList<Integer>> getScheduleGDSIdsForFlight(List<Integer> flightIds) throws ModuleException {
		try {
			return getFlightDAO().getScheduleGDSIdsForFlight(flightIds);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * private method to execute the command
	 * 
	 * @param command
	 * @return response
	 * @throws ModuleException
	 */
	public ServiceResponce execute(Command command) throws ModuleException {
		if (log.isDebugEnabled()) {
			log.debug("FlightServiceBean execute method called");
			log.debug("passing command parameters : " + command.getParameterNames());
		}
		try {
			ServiceResponce responce = command.execute();
			if (log.isDebugEnabled()) {
				log.debug("Command response recieved : " + responce.getResponseCode());
			}
			return responce;

		} catch (ModuleException ex) {
			log.error("Module Exception", ex);
			this.sessionContext.setRollbackOnly();
			throw ex;

		} catch (CommonsDataAccessException cdaex) {
			log.error("CommonsDataAccessException Exception", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());

		} catch (Exception ex) {
			log.error("Generic Exception", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex.getMessage());
		}
	}

	@Override
	public boolean isModelUsedByFlight(String modelNumber) throws ModuleException {
		Collection<Integer> flightIds = getFlightIDsForModel(modelNumber, null);
		return ((flightIds != null) && (flightIds.size() > 0));
	}

	/**
	 * 
	 * @return Flight Segment ID
	 * @throws ModuleException
	 * 
	 */
	@Override
	public Collection<FlightMealDTO> getFlightSegIdsforFlightforMeal(Collection<Integer> colFlightId) throws ModuleException {
		try {
			return getFlightSegmentDAO().getFlightSegIdsforFlightforMeal(colFlightId);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void updateFlightMealNotifyStatus(FlightMealNotifyStatusDTO flightMealNotifyStatusDTO) throws ModuleException {
		try {
			getFlightDAO().updateFlightMealNotifyStatus(flightMealNotifyStatusDTO);
		} catch (CommonsDataAccessException cdaex) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		} catch (Exception ex) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex.getMessage());
		}
	}

	private FlightDAO getFlightDAO() {
		return (FlightDAO) AirSchedulesUtil.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_DAO);
	}

	private FlightScheduleDAO getFlightScheduleDAO() {
		return (FlightScheduleDAO) AirSchedulesUtil.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_DAO);
	}

	/**
	 * Method to Lookup the FlightScheduleJdbcDAO FlightScheduleJdbcDAO
	 * 
	 * @return
	 */
	private FlightScheduleJdbcDAO lookupFlightScheduleJdbcDAO() {
		return (FlightScheduleJdbcDAO) AirSchedulesUtil.getInstance()
				.getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_JDBC_DAO);

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void updateFlightSegmentCheckinStatus(int flightId, String segmentCode, String status) throws ModuleException {
		try {
			getFlightDAO().updateFlightSegmentCheckinStatus(flightId, segmentCode, status);
		} catch (CommonsDataAccessException cdaex) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		} catch (Exception ex) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex.getMessage());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public FlightSegement getFlightSegmentForAirport(int flightId, String segmentCode) throws ModuleException {
		try {
			return getFlightDAO().getFlightSegmentForAirport(flightId, segmentCode);
		} catch (CommonsDataAccessException cdaex) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		} catch (Exception ex) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex.getMessage());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public int getFlightStandbyCount(int flightId, String airport) throws ModuleException {
		try {
			return getFlightDAO().getFlightStandbyCount(flightId, airport);
		} catch (CommonsDataAccessException cdaex) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		} catch (Exception ex) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex.getMessage());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public int getFlightCheckinCount(int flightId, String airport) throws ModuleException {
		try {
			return getFlightDAO().getFlightCheckinCount(flightId, airport);
		} catch (CommonsDataAccessException cdaex) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		} catch (Exception ex) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex.getMessage());
		}
	}

	/**
	 * method to getNumberOfFlightsInSchedule
	 * 
	 * 
	 */
	@Override
	public HashMap<Integer, Integer> getOpenFlightCount(List<Integer> scheduleIds) throws ModuleException {
		try {
			return getFlightDAO().getOpenFlightCount(scheduleIds);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * method to getNumberOfFlightsInSchedule
	 * 
	 * 
	 */
	@Override
	public HashMap<Integer, Integer> getFutureFlights(List<Integer> scheduleIds) throws ModuleException {
		try {
			return getFlightDAO().hasFutureFlights(scheduleIds);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * get Flights to GDS Publish
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<DisplayFlightDTO> getFlightsToGDSPublish(Date lastPublishTime, String codeShareCarrier) throws ModuleException {
		try {
			Collection<Flight> flightList = getFlightDAO().getFlightsToGDSPublish(lastPublishTime, codeShareCarrier);
			Command command = (Command) AirSchedulesUtil.getInstance().getLocalBean(CommandNames.FLIGHTS_FOR_PUBLISH_MACRO);
			command.setParameter(CommandParamNames.FLIGHT_LIST, flightList);
			ServiceResponce responce = command.execute();

			return ((Collection<DisplayFlightDTO>) responce.getResponseParam(CommandParamNames.FLIGHT_INFO_LIST));

		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	public Collection<DisplayFlightDTO> getFlightsToPublish(Date lastPublishTime) throws ModuleException {
		try {
			Collection<Flight> flightList = getFlightDAO().getFlightsToPublish(lastPublishTime);
			Command command = (Command) AirSchedulesUtil.getInstance().getLocalBean(CommandNames.FLIGHT_FOR_PUBLISH_FOR_LOYALTY);
			command.setParameter(CommandParamNames.FLIGHT_LIST, flightList);
			ServiceResponce responce = command.execute();

			return ((Collection<DisplayFlightDTO>) responce.getResponseParam(CommandParamNames.FLIGHT_INFO_LIST));

		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Integer updateFlightSegmentNotifyStatus(Integer flightSegmentId, Integer flightMsgEventId, String newStatus,
			String failureReason, Integer notificationCount, String notificationType) {
		Integer flightSegNotificationEventId = getFlightSegmentDAO().saveFlightSegNotificationEventStatus(flightSegmentId,
				flightMsgEventId, newStatus, failureReason, notificationCount, notificationType);
		return flightSegNotificationEventId;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public FlightSegNotificationEvent updateFlightSegmentNotifyStatus(Integer flightSegmentId, Integer flightMsgEventId,
			String newStatus, String failureReason, Integer notificationCount, String notificationType, String insFirstResponse,
			Blob insSecResponse) {
		FlightSegNotificationEvent flightSegNotificationEventId = getFlightSegmentDAO().saveFlightSegNotificationEventStatus(
				flightSegmentId, flightMsgEventId, newStatus, failureReason, notificationCount, notificationType,
				insFirstResponse, insSecResponse);
		return flightSegNotificationEventId;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Integer getFlightSegNotificationAttempts(Integer flightMsgEventId) {
		return getFlightSegmentDAO().getFlightSegNotificationAttempts(flightMsgEventId);
	}

	@Override
	public List<RouteInfoTO> getAvailableRoutes(String fromAirport, String toAirport, int availabilityRestrictionLevel)
			throws ModuleException {
		try {
			return getFlightDAO().getAvailableRoutes(fromAirport, toAirport, availabilityRestrictionLevel);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

    @Override
    public List<RouteInfoTO> getAvailableRoutes(List<String> fromAirport) throws ModuleException {
        try {
            return getFlightDAO().getAvailableRoutes(fromAirport);
        } catch (CommonsDataAccessException cdaex) {
            throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
        }
    }

	@Override
	public List<FlightSegmentDTO> getMatchingFlightSegmentsForOndGeneration(String origin, String destination, Date startDate,
			Date endDate) throws ModuleException {
		try {
			return getFlightDAO().getMatchingFlightSegmentsForOndGeneration(origin, destination, startDate, endDate);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void updateFlightSegmentTerminals(String airport, Integer terminalId, boolean isNull, boolean isOnlySet)
			throws ModuleException {
		try {
			getFlightDAO().updateFlightSegmentTerminals(airport, terminalId, isNull, isOnlySet);
		} catch (CommonsDataAccessException cdaex) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		} catch (Exception ex) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex.getMessage());
		}
	}

	@Override
	public boolean isTerminalUsedByFlightOrSchedule(Integer terminalId) throws ModuleException {
		Collection<Integer> flghts = getFlightsForTerminal(terminalId);
		Collection<Integer> schedules = getSchedulesForTerminal(terminalId);
		return ((flghts != null) && (flghts.size() > 0)) || ((schedules != null) && (schedules.size() > 0));
	}

	/**
	 * 
	 * @return String Booking_Class
	 * @throws ModuleException
	 * 
	 */
	@Override
	public List<String> getBookingClassForFareAmount(String segmentCode, Date flightDepDate, Double adultFareAmount,
			String fareType) throws ModuleException {
		try {
			return getFlightDAO().getMatchingBookingClassForRM(segmentCode, flightDepDate, adultFareAmount, fareType);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	public Collection<FlightBaggageDTO> getFlightSegIdsforFlightforBaggage(Collection<Integer> colFlightId)
			throws ModuleException {
		try {
			return getFlightSegmentDAO().getFlightSegIdsforFlightforBaggage(colFlightId);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	public String getDefaultBusCarrierCode() throws ModuleException {

		try {

			String defaultBusCarrier = null;

			FlightSegementDAO flightSegementDAO = (FlightSegementDAO) AirSchedulesUtil.getInstance().getLocalBean(
					InternalConstants.DAOProxyNames.FLIGHT_SEGMENT_DAO);

			Collection<String> busCarrierCodeColl = flightSegementDAO.getBusCarrierCode();

			if (busCarrierCodeColl != null && busCarrierCodeColl.size() > 0) {
				Iterator<String> itrBusCarrierCodeColl = busCarrierCodeColl.iterator();
				while (itrBusCarrierCodeColl.hasNext()) {
					defaultBusCarrier = itrBusCarrierCodeColl.next();
				}
			}
			return defaultBusCarrier;
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		} catch (Exception ex) {
			throw new ModuleException(ex.getMessage());
		}
	}

	public String getDefaultBusCarrierCode(String carrierCode) throws ModuleException {
		Map<String, String> busAirMap = getBusAirCarrierCodes();
		Map<String, String> airBusMap = new HashMap<String, String>();
		for (Entry<String, String> x : busAirMap.entrySet()) {
			airBusMap.put(x.getValue(), x.getKey());
		}
		return airBusMap.get(carrierCode);
	}

	@Override
	public Map<String, String> getBusAirCarrierCodes() throws ModuleException {
		try {

			FlightSegementDAO flightSegementDAO = (FlightSegementDAO) AirSchedulesUtil.getInstance().getLocalBean(
					InternalConstants.DAOProxyNames.FLIGHT_SEGMENT_DAO);

			return flightSegementDAO.getBusAirCarrierCodes();

		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		} catch (Exception ex) {
			throw new ModuleException(ex.getMessage());
		}
	}

	public List<AvailableConnectedFlight> getAvailableConnectedFlightSegments(Date dateTimeStart, Date dateTimeEnd,
			String origin, String destination, List<TransitAirport> stopOverAirports, int adults, int infants, String ccCode,
			int availabilityRestriction, Map<String, Date> allowableLatesDatesZuluForInterliningMap,
			Collection<String> externalBookingClasses, Collection<Integer> flightIdList) throws ModuleException {
		try {
			return (new SearchAvailableConnectionsForReprotection()).getAvailableConnectedFlightSegments(dateTimeStart,
					dateTimeEnd, origin, destination, stopOverAirports, adults, infants, ccCode, availabilityRestriction,
					allowableLatesDatesZuluForInterliningMap, externalBookingClasses, flightIdList);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		} catch (Exception ex) {
			throw new ModuleException(ex.getMessage());
		}

	}

	private FlightScheduleBL getFlightInventoryBL() {
		if (flightScheduleBL != null) {
			return flightScheduleBL;
		}

		flightScheduleBL = new FlightScheduleBL();
		return flightScheduleBL;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void assignDefaultMealChargesTemplate() throws ModuleException {
		try {
			getFlightInventoryBL().assignDefaultMealChargesTemplate();
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void assignDefaultSeatMapChargesTemplate() throws ModuleException {
		try {
			getFlightInventoryBL().assignDefaultSeatMapChargesTemplate();
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void updateFltAnciAssignStatusByFltSegId(Collection<Integer> flightSegIds, String ancillaryType,
			AnciDefaultTemplStatusEnum status) throws ModuleException {
		try {
			getFlightDAO().updateFltAnciAssignStatusByFltSegId(flightSegIds, ancillaryType, status);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		} catch (Exception ex) {
			throw new ModuleException(ex.getMessage());
		}
	}

	/**
	 * publish the All AdhocFlights to GDSes
	 * 
	 * @param scheduleId
	 * @param gdsIds
	 * @return
	 * @throws ModuleException
	 * 
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void publishAllAdhocFlightsToGDS(List<Integer> gdsIds) throws ModuleException {

		try {
			ScheduleSearchDTO dto = new ScheduleSearchDTO();
			dto.setStartDate(new Date());
			GregorianCalendar currentDate = new GregorianCalendar();
			// Schedules for next 3 years
			currentDate.set(GregorianCalendar.YEAR, currentDate.get(GregorianCalendar.YEAR) + 3);
			dto.setEndDate(currentDate.getTime());
			dto.setScheduleStatus(ScheduleStatusEnum.ACTIVE.getCode());
			dto.setBuildStatus(ScheduleBuildStatusEnum.BUILT.getCode());
			Collection<Flight> adhocFlights = getFlightScheduleDAO().getAvailableAdhocFlights(dto);

			for (Flight flight : adhocFlights) {
				if (flight.getFlightId() != null) {
					publishFlightToGDS(flight.getFlightId(), gdsIds, (int) flight.getVersion());
				}
			}
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		} catch (Exception ex) {
			throw new ModuleException(ex.getMessage());
		}
	}

	public Collection<FlightSegement> getFlightSegmentList(String segmentCode, Date departureDate) {
		FlightSegementDAO flightSegmentDAO = ((FlightSegementDAO) AirSchedulesUtil.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.FLIGHT_SEGMENT_DAO));
		return flightSegmentDAO.getFlightSegmentList(segmentCode, departureDate);
	}

	public FlightSegement getSpecificFlightSegment(int pnrSegId) throws ModuleException {
		try {
			return getFlightSegmentDAO().getSpecificFlightSegment(pnrSegId);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), e.getModuleCode());
		} catch (Exception e) {
			throw new ModuleException(e.getMessage());
		}
	}

	public List<FlightSegement>
			getSuggestedSegmentListForADate(String flightNumber, String departureDateLocal, String segmentCode) {
		FlightSegementDAO flightSegmentDAO = ((FlightSegementDAO) AirSchedulesUtil.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.FLIGHT_SEGMENT_DAO));
		return flightSegmentDAO.getSuggestedSegmentListForADate(flightNumber, departureDateLocal, segmentCode);
	}

	@Override
	public int getFlightSegmentCount(String flightNumber, Date departureDateLocal, Date arrivalDateLocal, String segmentCode,
			String agentCode) throws ModuleException {
		FlightSegementDAO flightSegmentDAO = ((FlightSegementDAO) AirSchedulesUtil.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.FLIGHT_SEGMENT_DAO));
		if (AppSysParamsUtil.isEnableRouteSelectionForAgents() && agentCode != null) {
			if (!isEligibleAgentToSearchTheGivenRoute(segmentCode.split("/")[0], segmentCode.split("/")[1], agentCode)) {
				log.error("Agent " + agentCode + " is not eligible for route "+ segmentCode);
				throw new ModuleException("gdsservices.validators.agent.routeNotApplcable");
			}
		}
		return flightSegmentDAO.getFlightSegmentCount(flightNumber, departureDateLocal, arrivalDateLocal, segmentCode);
	}

	public ArrayList<FlightSegmentDTO> getGoshowBCOpeningFilghtList(Date date) {
		return getFlightDAO().getGoshowBCOpeningFilghtList(date);
	}

	public List<Flight> getActiveFlightWithinCutOffTime(Integer flightSegId) {
		return getFlightDAO().getActiveFlightWithinCutOffTime(flightSegId);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean areSameDaySameFlights(int flightSegID1, int flightSegID2) {
		FlightSegementDAO flightSegmentDAO = ((FlightSegementDAO) AirSchedulesUtil.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.FLIGHT_SEGMENT_DAO));
		return flightSegmentDAO.areSameDaySameFlights(flightSegID1, flightSegID2);

	}

	public void activatePastFlights(int flightId) {
		getFlightDAO().activatePastFlights(flightId);
	}

	@Override
	public boolean isAirModelHasSeatTemplate(String modelNumber) throws ModuleException {
		return getFlightDAO().isAirModelHasSeatTemplate(modelNumber);

	}

	@Override
	public boolean isAirModelHasChargeTemplate(String modelNumber) throws ModuleException {
		return getFlightDAO().isAirModelHasChargeTemplate(modelNumber);
	}

	@Override
	public boolean isSeatMapAvailableForFlightSegement(Integer flightSegId) throws ModuleException {
		FlightSegementDAO flightSegmentDAO = ((FlightSegementDAO) AirSchedulesUtil.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.FLIGHT_SEGMENT_DAO));
		return flightSegmentDAO.isSeatMapAvailableForFlightSegement(flightSegId);
	}

	@Override
	public String getOverbookFlightDetails(String flightIds) throws ModuleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> getFLightsWithAirModelHasChargeTemplate(String modelNumber) throws ModuleException {
		return getFlightDAO().getFLightsWithAirModelHasChargeTemplate(modelNumber);
	}

	@Override
	public Integer getFlightID(String pnrSegID) throws ModuleException {
		return getFlightDAO().getFlightID(pnrSegID);
	}

	public Map<Integer, Boolean> isAtLeastOneSegmentHavingCSOCFlight(Collection<Integer> colFlightSegIds) throws ModuleException {
		return getFlightSegmentDAO().isAtLeastOneSegmentHavingCSOCFlight(colFlightSegIds);
	}

	public Collection<Flight> getAvailableAdhocFlights(ScheduleSearchDTO scheduleSearchDTO) throws ModuleException {

		try {
			return getFlightScheduleDAO().getAvailableAdhocFlights(scheduleSearchDTO);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Collection<FlightBasicInfoDTO> getFlightDetails(FlightBasicInfoDTO flownFlightsSearchDTO, int startIndex, int pageLength) throws ModuleException {
		UpdatePaxETStatusDAO updatePaxETStatusDAO = (UpdatePaxETStatusDAO) AirSchedulesUtil.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.FLOWN_FLIGHTS_DTO);
		return updatePaxETStatusDAO.getFlownFlights(flownFlightsSearchDTO, startIndex, pageLength);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void updateFlownReservationsDetails(PastFlightInfoDTO pastFlightInfoDTO) throws ModuleException {
		try {
			UpdatePaxETStatusDAO updatePaxETStatusDAO = (UpdatePaxETStatusDAO) AirSchedulesUtil.getInstance()
					.getLocalBean(InternalConstants.DAOProxyNames.FLOWN_FLIGHTS_DTO);

			TrackInfoDTO trackInfo = null;
			CredentialsDTO credentialsDTO = ReservationApiUtils.getCallerCredentials(trackInfo, getUserPrincipal());

			updatePaxETStatusDAO
					.updateReservationAudit(pastFlightInfoDTO.getFlightNumber(), pastFlightInfoDTO.getDepartureDate(),
							credentialsDTO);
			if ("UPDATE".equals(pastFlightInfoDTO.getHdnMode()) && !CollectionUtils
					.isEmpty(pastFlightInfoDTO.getPaxFareSegETIds())) {
				updatePaxETStatusDAO.updatePaxETStatus(pastFlightInfoDTO.getPaxFareSegETIds());
			} else if ("UPDATE".equals(pastFlightInfoDTO.getHdnMode()) && CollectionUtils
					.isEmpty(pastFlightInfoDTO.getPaxFareSegETIds())) {
				updatePaxETStatusDAO
						.updatePaxETStatus(pastFlightInfoDTO.getFlightNumber(), pastFlightInfoDTO.getDepartureDate());
			} else if ("CONFIRM.REPROTECT".equals(pastFlightInfoDTO.getHdnMode()) && !CollectionUtils
					.isEmpty(pastFlightInfoDTO.getPaxFareSegETIds())) {
				updatePaxETStatusDAO.updatePaxETStatus(pastFlightInfoDTO.getPaxFareSegETIds());
			}

			AuditAirSchedules auditAirSchedules = new AuditAirSchedules();
			auditAirSchedules
					.auditForFlownFlightsDetailsModification(getUserPrincipal(), pastFlightInfoDTO.getContentMap());

		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::updateFlownFlightDetails ", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

	}

	/**
	 * 
	 * @return Flight Segment ID
	 * @throws ModuleException
	 * 
	 */
	@Override
	public Collection<Integer> getFlightSegIdsforFlight(int flightId) throws ModuleException {
		try {
			return getFlightSegmentDAO().getFlightSegIdsforFlight(flightId);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	/**
	 * 
	 * get number of non canceled flights
	 * 
	 * @return flight Schedule ID
	 * @throws ModuleException
	 * 
	 */
	@Override
	public int getNumberOfNonCanceledFlights(int flightScheduleID) throws ModuleException {
		try {
			return getFlightDAO().getNumberOfNonCanceledFlights(flightScheduleID);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	public String getMCFlightNumber(Integer flightId, String marketingCarrierCode) throws ModuleException {
		return getFlightDAO().getMCFlightNumber(flightId, marketingCarrierCode);
	}

	@Override
	public void updateDelayCancelFlight(Collection<Integer> flightIds, boolean status, Date holdTill) throws ModuleException {
		try {
			getFlightDAO().updateDelayCancelFlight(flightIds, status, holdTill, null);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

	}

	@Override
	public Collection<Integer> getPossibleFlightDesignatorChange(Date departureDate, String segmentCode, boolean isLocalTime,
			Date flightsUntil, boolean isAdHocFlightsOnly) throws ModuleException {
		try {
			return getFlightDAO().getPossibleFlightDesignatorChange(departureDate, segmentCode, isLocalTime, flightsUntil, false);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

	}

	@Override
	public boolean showRollFwdButton(Integer flightId) throws ModuleException{
		try {
			return lookupFlightScheduleJdbcDAO().showRollFwdButton(flightId);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ServiceResponce cancelFlightWithRequiresNew(int flightId, boolean forceCancel, FlightAlertDTO flightAlertDTO)
			throws ModuleException {
		return cancelFlight(flightId, forceCancel, flightAlertDTO);
	}

	private boolean isEligibleAgentToSearchTheGivenRoute(String origin, String destination, String agentCode)
			throws ModuleException {
		List<String> airports = Arrays.asList(origin, destination);
		List<String> possibleOndCombinationsToBeChecked = OndCombinationUtil.getApplicableOndCodeSelections(airports);
		List<String> agentApplicableRoutes = AirSchedulesUtil.getTravelAgentBD().getAgentWiseApplicableRoutes(agentCode);
		return !Collections.disjoint(possibleOndCombinationsToBeChecked, agentApplicableRoutes);
	}	

	/**
	 * return the collection of pax fare segment Ids for a given flight
	 *
	 * @param flightId
	 * @return
	 * @throws ParseException
	 */
	@Override public Collection<Integer> getPaxFareSegETIds(String flightId) throws ParseException {
		return getFlightSegmentDAO().getPaxFareSegETIds(flightId);
	}
	
	private StationDAO lookupStationDAO() {
		return (StationDAO) AirmasterUtils.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.STATION_DAO);
	}
	


	@Override
	public ServiceResponce prepareCopyFlight(int flightId, Date newDate, boolean isTimeInLocal) throws ModuleException {

		Command command = (Command) AirSchedulesUtil.getInstance().getLocalBean(CommandNames.PREPARE_COPY_FLIGHT);
		command.setParameter(CommandParamNames.FLIGHT_ID, new Integer(flightId));
		command.setParameter(CommandParamNames.DEPARTURE_DATE, newDate);
		command.setParameter(CommandParamNames.IS_DATE_IN_LOCAL, new Boolean(isTimeInLocal));

		return this.execute(command);
	}

	@Override
	public String getCountryCodeForOrigin(Integer flightId) {
		try {
			Flight flight = getFlight(flightId);
			if (flight != null) {
				Station station = lookupStationDAO().getStation(flight.getOriginAptCode());
				return station.getCountryCode();
			}
		} catch (ModuleException e) {
			log.error("Flight unavailable", e);
		}
		return null;
	}

	@Override
	public Collection<String> getFutureFlightsNoByOriginDestination(String originAptCode, String destinationAptCode) throws ModuleException {
		try {
			return getFlightDAO().getFutureFlightsNoByOriginDestination(originAptCode, destinationAptCode);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	public Collection<BasicFlightDTO> getBasicFlightDTOs(Collection<Integer> flightIds) throws ModuleException {
		try {
			return getFlightDAO().getBasicFlightDTOs(flightIds);
		} catch (CommonsDataAccessException cdaex) {
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}

	}

	@Override
	public Integer getFlightId(String flightNumber, Date departureDate) {
		Integer flightId = 0;
		try {
			FlightDAO flightdao = getFlightDAO();
			flightId = flightdao.getFlightId(flightNumber, departureDate);
		} catch (CommonsDataAccessException cdaex) {
			log.error("Flight unavailable", cdaex);
		}
		return flightId;
	}
	
	@Override
	public List<AvailableFlightSegment> getAvailableFlightSegments(String transitAirport, String destination, String cabinClass,
			Date lastFlightArrivalTime, int adultCount) throws ModuleException {
		FlightDAO flightDAO = getFlightDAO();

		AvailableFlightSearchCriteria fltSearchCriteria = getFreshFlightSearchCriteria(adultCount);

		// warning hardcoded
		int ondSequence = 0;
		// FIXME remove hardcoded value, get logicalCabinClass
		String logicalCabinClass = null;
		// nothing related to query
		boolean isInboundFlight = false;

		boolean enableUserDefineTransit = AppSysParamsUtil.isEnableUserDefineTransitTime();
		
		// FIXME remove hardcoded values for hubTimeDetails
		Map<String, Integer> hubTimeDetails = null;

		// FIXME check this
		Date maxDateZulu = null;

		// Exclude multi-leg flights
		String[] minMaxTransitDurations = CommonsServices.getGlobalConfig().getMixMaxTransitDurations(transitAirport, null, null);

		Date minDepartTime;
		Date maxDepartTime;

		if (enableUserDefineTransit && hubTimeDetails != null && hubTimeDetails.size() > 0
				&& hubTimeDetails.get(transitAirport) != null) {
			Integer noOfDays = hubTimeDetails.get(transitAirport);
			if (noOfDays.intValue() != 0) {
				Date tmpMinDepartTime = CalendarUtil.getStartTimeOfDate(CalendarUtil.getOfssetAddedTime(lastFlightArrivalTime,
						noOfDays));
				Date tmpMaxDepartTime = CalendarUtil.getEndTimeOfDate(tmpMinDepartTime);

				// If user define time within standard transit time, should get standard time
				minDepartTime = this.getMinTransitTime(lastFlightArrivalTime, minMaxTransitDurations[0]);
				maxDepartTime = this.getMaxTransitTime(lastFlightArrivalTime, minMaxTransitDurations[1]);

				if (tmpMinDepartTime.after(minDepartTime)) {
					minDepartTime = tmpMinDepartTime;
				}

				if (tmpMaxDepartTime.after(maxDepartTime)) {
					maxDepartTime = tmpMaxDepartTime;
				}
			} else {
				minDepartTime = this.getMinTransitTime(lastFlightArrivalTime, minMaxTransitDurations[0]);
				maxDepartTime = this.getMaxTransitTime(lastFlightArrivalTime, minMaxTransitDurations[1]);
			}

		} else {
			minDepartTime = this.getMinTransitTime(lastFlightArrivalTime, minMaxTransitDurations[0]);
			maxDepartTime = this.getMaxTransitTime(lastFlightArrivalTime, minMaxTransitDurations[1]);
		}

		fltSearchCriteria.setOndSequence(ondSequence);
		fltSearchCriteria.setOriginDestination(transitAirport, destination);
		fltSearchCriteria.setDates(minDepartTime, maxDepartTime, null, null, maxDateZulu);
		fltSearchCriteria.setCabins(cabinClass, logicalCabinClass);
		fltSearchCriteria.setIncludeOnlyMarketingCarrierSegs(false);
		fltSearchCriteria.setInboundFlight(isInboundFlight);
		List<AvailableFlightSegment> currentDirectFlightSegments = flightDAO.getDirectFlightSegments(fltSearchCriteria);
		return currentDirectFlightSegments;
	}

	private Date getMinTransitTime(Date date, String minTrValueStr) {

		String hours = minTrValueStr.substring(0, minTrValueStr.indexOf(":"));
		String mins = minTrValueStr.substring(minTrValueStr.indexOf(":") + 1);

		GregorianCalendar calander = new GregorianCalendar();
		calander.setTime(date);
		calander.add(GregorianCalendar.HOUR, Integer.parseInt(hours));
		calander.add(GregorianCalendar.MINUTE, Integer.parseInt(mins));

		return calander.getTime();
	}

	private Date getMaxTransitTime(Date date, String maxTrValueStr) {

		String hours = maxTrValueStr.substring(0, maxTrValueStr.indexOf(":"));
		String mins = maxTrValueStr.substring(maxTrValueStr.indexOf(":") + 1);

		GregorianCalendar calander = new GregorianCalendar();
		calander.setTime(date);
		calander.add(GregorianCalendar.HOUR, Integer.parseInt(hours));
		calander.add(GregorianCalendar.MINUTE, Integer.parseInt(mins));

		return calander.getTime();
	}

	private AvailableFlightSearchCriteria getFreshFlightSearchCriteria(int adultCount) {
		AvailableFlightSearchCriteria _defaultFltSearchCriteria = new AvailableFlightSearchCriteria();

		// FIXME hardcoded
		_defaultFltSearchCriteria.setAdults(adultCount);
		_defaultFltSearchCriteria.setChilds(0);
		_defaultFltSearchCriteria.setInfants(0);
		_defaultFltSearchCriteria.setAvailabilityRestriction(2);
		_defaultFltSearchCriteria.setOnlyInterline(false);
		_defaultFltSearchCriteria.setInterline(false);
		_defaultFltSearchCriteria.setExternalBookingClasses(null);
		_defaultFltSearchCriteria.setCutOffTimeForInternational(AppSysParamsUtil.getAgentReservationCutoverInMillis());
		_defaultFltSearchCriteria.setCutOffTimeForDomestic(AppSysParamsUtil.getReservationCutoverForDomesticInMillis());
		_defaultFltSearchCriteria.setMultiCitySearch(false);
		_defaultFltSearchCriteria.setAllowFlightSearchAfterCutOffTime(true);
		_defaultFltSearchCriteria.setAllowDoOverbookAfterCutOffTime(false);
		_defaultFltSearchCriteria.setAllowDoOverbookBeforeCutOffTime(false);
		_defaultFltSearchCriteria.setAppIndicator(null);
		_defaultFltSearchCriteria.setModifyBooking(false);
		_defaultFltSearchCriteria.setChannelCode(3);
		_defaultFltSearchCriteria.setSourceFlightInCutOffTime(false);
		_defaultFltSearchCriteria.setOpenReturn(false);
		_defaultFltSearchCriteria.setAgentCode(null);

		return _defaultFltSearchCriteria;
	}

}
