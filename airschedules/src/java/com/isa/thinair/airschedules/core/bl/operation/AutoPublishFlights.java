package com.isa.thinair.airschedules.core.bl.operation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airmaster.api.model.Gds;
import com.isa.thinair.airschedules.api.model.CodeShareMCFlight;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.airschedules.core.persistence.dao.FlightDAO;
import com.isa.thinair.airschedules.core.persistence.dao.FlightScheduleJdbcDAO;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for the auto publish flights
 * 
 * @author rajitha
 * @isa.module.command name="autoPublishFlights"
 */
public class AutoPublishFlights extends DefaultBaseCommand{
	
	private Map<String, GDSStatusTO> gdsStatusMap;

	@Override
	public ServiceResponce execute() throws ModuleException {
		
		Flight flight = (Flight) this.getParameter(CommandParamNames.FLIGHT);
		checkParams(flight);
		DefaultServiceResponse response = processRequest(flight);
		Collection<Flight> flightsToPublish = new ArrayList<Flight>();
		flightsToPublish.add(flight);
		
		if(response.isSuccess()){
			response.setResponseCode(ResponceCodes.FLIGHT_GDS_AUTOPUBLISH_SUCCESSFULL);
			response.addResponceParam(CommandParamNames.LAST_RESPONSE, response);
			response.addResponceParam(CommandParamNames.FLIGHT_ID, flight.getFlightId());
			response.addResponceParam(CommandParamNames.FLIGHTS_TO_PUBLISH, flightsToPublish);
		}
		
		return response;
	}

	
	private DefaultServiceResponse processRequest(Flight flight) throws ModuleException{
		
		DefaultServiceResponse response = new DefaultServiceResponse(true);
		int flightId = flight.getFlightId();
		
		if (AppSysParamsUtil.isEnableAutoPublishSchedule()) {

			if (StringUtil.isNullOrEmpty(flight.getCsOCCarrierCode())
					|| StringUtil.isNullOrEmpty(flight.getCsOCFlightNumber())) {

				setGdsStatusToMap();
				Set<Integer> activeGdsIds = getActiveGdsIdsForRoute(flight);

				if (checkGdsActiveForAutoPublish(activeGdsIds)) {

					flight.setGdsIds(activeGdsIds);
					setCodeShareMCForFlight(flight, activeGdsIds);
					AirSchedulesUtil.getFlightBD().publishFlightToGDS(Integer.valueOf(flightId), activeGdsIds,
							Integer.parseInt(new Long(flight.getVersion()).toString()));

				}

			}

		}
		return response;
	}

	
	private boolean checkGdsActiveForAutoPublish(Set<Integer> setToCheck) {

		if (setToCheck == null || setToCheck.isEmpty()) {
			return false;
		}

		return true;

	}

	
	private Set<Integer> getActiveGdsIdsForRoute(Flight flight) {
		
		Set<Integer> publishedGdsIdsForRoute = getAutoPublishedGdsIdsForRoute(flight);
		Set<Integer> activeGdsIds = new HashSet<Integer>();

		
		if (publishedGdsIdsForRoute != null && !publishedGdsIdsForRoute.isEmpty()) {
			Map<String, GDSStatusTO> gdsStatusMap = this.getGdsStatusMap();
			for (GDSStatusTO gdsStatusTo : gdsStatusMap.values()) {

				if (publishedGdsIdsForRoute.contains(gdsStatusTo.getGdsId())) {
					activeGdsIds.add(gdsStatusTo.getGdsId());

				}

			}
		}

		return activeGdsIds;
		
	}


	private Set<Integer> getAutoPublishedGdsIdsForRoute(Flight flight) {
		String origin = flight.getOriginAptCode();
		String destination = flight.getDestinationAptCode();
		
		Set<Gds> publishedGdsSet = lookupFlightScheduleJdbcDAO().getAutoPublishedGds(origin, destination);
		
		Set<Integer> gdsIdSet = new HashSet<Integer>();
		if (publishedGdsSet != null && !publishedGdsSet.isEmpty()) {

			for (Gds gds : publishedGdsSet) {
				gdsIdSet.add(gds.getGdsId());
			}

		} else {

			return gdsIdSet;
		}
		
		
		return gdsIdSet;
	}

	
	private void setCodeShareMCForFlight(Flight flight, Set<Integer> activeGdsIds) {

		Map<String, GDSStatusTO> gdsStatusMap = this.getGdsStatusMap();

		Set<CodeShareMCFlight> csMcFlightSet = new HashSet<CodeShareMCFlight>();

		for (GDSStatusTO gdsTo : gdsStatusMap.values()) {
			if (activeGdsIds.contains(gdsTo.getGdsId()) && gdsTo.isCodeShareCarrier()) {
				CodeShareMCFlight csMcFlight = new CodeShareMCFlight();
				
				csMcFlight.setCsFlightId(flight.getScheduleId());
				csMcFlight.setCsMCFlightNumber(flight.getFlightNumber());
				csMcFlight.setCsMCCarrierCode(gdsTo.getCarrierCode());
				csMcFlightSet.add(csMcFlight);
			}

		}

		flight.setCodeShareMCFlights(csMcFlightSet);

	}


	private void checkParams(Flight flight) throws ModuleException{
		if(flight == null){
			throw new ModuleException("airschedules.arg.invalid.null");
		}
	}

	
	private FlightScheduleJdbcDAO lookupFlightScheduleJdbcDAO() {

		FlightScheduleJdbcDAO dao = null;

		dao = (FlightScheduleJdbcDAO) AirSchedulesUtil.getInstance()
				.getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_JDBC_DAO);

		return dao;
	}
	
	
	private FlightDAO getFlightDAO() {
		FlightDAO flightDAO = (FlightDAO) AirSchedulesUtil.getInstance()
				.getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_DAO);
		return flightDAO;
	}
	
	private void setGdsStatusToMap() {
		this.setGdsStatusMap(CommonsServices.getGlobalConfig().getActiveGdsMap());
		
	}


	public Map<String, GDSStatusTO> getGdsStatusMap() {
		return gdsStatusMap;
	}


	public void setGdsStatusMap(Map<String, GDSStatusTO> gdsStatusMap) {
		this.gdsStatusMap = gdsStatusMap;
	}
}
