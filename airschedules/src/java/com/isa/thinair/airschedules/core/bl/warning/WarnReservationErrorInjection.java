/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.warning;

import java.util.ArrayList;

import com.isa.thinair.airinventory.api.service.FlightInventoryBD;
import com.isa.thinair.airmaster.api.model.AircraftModel;
import com.isa.thinair.airmaster.api.service.AircraftBD;
import com.isa.thinair.airschedules.api.dto.FlightAllowActionDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command to warn reservation error induction
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="warnReservationErrorInjection"
 */
public class WarnReservationErrorInjection extends DefaultBaseCommand {

	// BD's
	private AircraftBD aircraftBD;
	private FlightInventoryBD flightInventryBD;

	/**
	 * constructor of the WarnReservationErrorInjection command
	 */
	public WarnReservationErrorInjection() {

		// looking up bd's
		aircraftBD = AirSchedulesUtil.getAircraftBD();
		flightInventryBD = AirSchedulesUtil.getFlightInventoryBD();
	}

	/**
	 * execute method of the WarnReservationErrorInjection command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {

		// getting command params
		Flight updated = (Flight) this.getParameter(CommandParamNames.FLIGHT);

		Flight existing = (Flight) this.getParameter(CommandParamNames.EXISTING_FLIGHT);

		Flight updatedOverlapping = (Flight) this.getParameter(CommandParamNames.OVERLAPPING_FLIGHT);

		FlightAllowActionDTO allowDTO = (FlightAllowActionDTO) this.getParameter(CommandParamNames.FLIGHT_ALLOW_ACTION_DTO);

		DefaultServiceResponse warning = (DefaultServiceResponse) this.getParameter(CommandParamNames.WARNING);

		Boolean downGradeToAnyModel = (Boolean) this.getParameter(CommandParamNames.FLIGHT_DOWNGRADE_TO_ANY_MODEL);
		// checking params
		this.checkParams(updated, allowDTO);

		/*
		 * This section is not currently used for issuing warning for existing reservations for the invalidated segment.
		 * logic moved to 'CheckReservationsForInvalidSegments' command
		 */
		/*
		 * if(!allowDTO.isAllowSegmentV2I()) {
		 * 
		 * Set updatedSegments = updated.getFlightSegements(); Iterator itUpdSegs = updatedSegments.iterator(); while
		 * (itUpdSegs.hasNext()) {
		 * 
		 * FlightSegement updatedSeg = (FlightSegement) itUpdSegs.next();
		 * 
		 * Set existingSegments = existing.getFlightSegements(); Iterator itExtSegs = existingSegments.iterator();
		 * 
		 * while (itExtSegs.hasNext()) {
		 * 
		 * FlightSegement existingSeg = (FlightSegement) itExtSegs.next();
		 * if(updatedSeg.getSegmentCode().equals(existingSeg.getSegmentCode())){
		 * 
		 * if( !updatedSeg.getValidFlag() && existingSeg.getValidFlag()){ //TODO check the reservation, call the
		 * inventry to validate this } break; } } } }
		 */

		if (!allowDTO.isAllowModelForAnyCC()) {

			AircraftModel model = aircraftBD.getAircraftModel(updated.getModelNumber());
			AircraftModel existingModel = aircraftBD.getAircraftModel(existing.getModelNumber());

			if (model.getModelNumber() != existingModel.getModelNumber()) {

				ArrayList<Integer> flightIds = new ArrayList<Integer>();

				flightIds.add(updated.getFlightId());
				if (updatedOverlapping != null)
					flightIds.add(updatedOverlapping.getFlightId());

				DefaultServiceResponse response = flightInventryBD.checkFCCInventoryModelUpdatability(flightIds, model,
						existingModel, downGradeToAnyModel, true);

				if (!response.isSuccess()) {
					if (warning == null) {
						warning = new DefaultServiceResponse(true);
					}
					warning.setSuccess(false);
					warning.setResponseCode(ResponceCodes.WARNING_FOR_FLIGHT_UPDATE);
					for (Object responseKey : response.getResponseParamNames()) {
						String key = (String) responseKey;
						warning.addResponceParam(response.getResponseCode(), response.getResponseParam(key));
					}
				}
			}
		}

		if (!allowDTO.isAllowOverlapForAnyCC()) {
			// TODO call the inventry to validate
		}

		// constructing responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);
		if (warning != null)
			responce.addResponceParam(CommandParamNames.WARNING, warning);
		// return command responce
		return responce;
	}

	/**
	 * private method to validate parameters
	 * 
	 * @param schedule
	 * @throws ModuleException
	 */
	private void checkParams(Flight flight, FlightAllowActionDTO allowDTO) throws ModuleException {

		if (flight == null || allowDTO == null)// throw exception
			throw new ModuleException("airschedules.arg.invalid.null");
	}
}
