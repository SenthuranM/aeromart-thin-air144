package com.isa.thinair.airschedules.core.persistence.hibernate;

import com.isa.thinair.airproxy.api.model.reservation.dto.LccClientPassengerEticketInfoTO;
import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationAuditCreator;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airreservation.core.activators.ReservationDAOUtils;
import com.isa.thinair.airschedules.api.dto.FlightBasicInfoDTO;
import com.isa.thinair.airschedules.core.persistence.dao.UpdatePaxETStatusDAO;
import com.isa.thinair.airschedules.core.util.LookUpUtils;
import com.isa.thinair.auditor.api.dto.PassengerTicketCouponAuditDTO;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.commons.core.util.CalendarUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

/**
 * 
 * @isa.module.dao-impl dao-name="UpdatePaxETStatusDAO"
 */

public class UpdatePaxETStatusDAOImpl extends PlatformBaseHibernateDaoSupport implements UpdatePaxETStatusDAO {

	SimpleDateFormat inDateFormat = new SimpleDateFormat("dd/MM/yyyy");
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yy HH.mm.ss");
	Date currentDate = new Date();

	private static Log log = LogFactory.getLog(UpdatePaxETStatusDAOImpl.class);

	public Collection<FlightBasicInfoDTO> getFlownFlights(FlightBasicInfoDTO flownFlightsSearchDTO, int startIndex, int pageLength) throws ModuleException {

		String searchCriteria = getSearchCriteria(flownFlightsSearchDTO, startIndex, pageLength);
		Collection<FlightBasicInfoDTO> flightsDetails = getFlownFlightsDetails(searchCriteria);

		return flightsDetails;
		//return new Page(1, startIndex, startIndex + pageLength, flownFlightsDetails);

	}

	private String getSearchCriteria(FlightBasicInfoDTO flownFlightsSearchDTO, int startIndex, int pageLength) {

		String searchCritria = new String();

		try {
			Date startDate = null;
			Date endDate = null;
			
			if (flownFlightsSearchDTO.getDepartureDate() != null && flownFlightsSearchDTO.getDepartureDate().trim().length() > 0) {
				log.debug("Search Flight");
				startDate = CalendarUtil.getStartTimeOfDate(inDateFormat.parse(flownFlightsSearchDTO.getDepartureDate()));

				String strCurrDate = dateFormat.format(currentDate);
				endDate = CalendarUtil.getEndTimeOfDate(inDateFormat.parse(flownFlightsSearchDTO.getDepartureDate()));
				

				// Query for search flown flight
				StringBuilder sbResultSearch = new StringBuilder(
						"SELECT  f.flight_number,f.departure_date,fs.segment_code FROM t_flight f, "
								+ "t_flight_segment fs where");

//				sbResultSearch.append(" flight_number = '" + flownFlightsSearchDTO.getFlightNumber() + "' AND " );

				sbResultSearch.append(" f.departure_date  BETWEEN " + "TO_DATE('" + dateFormat.format(startDate)
						+ "','dd-mm-yy HH24:mi:ss')" + " AND TO_DATE('" + dateFormat.format(endDate)
						+ "', 'dd-mm-yy HH24:mi:ss')");

				sbResultSearch.append(" AND  f.flight_id=fs.flight_id  order by departure_date desc");

				searchCritria = sbResultSearch.toString();

			}

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return searchCritria;
	}

	private Collection<FlightBasicInfoDTO> getFlownFlightsDetails(String searchCriteria) {

		JdbcTemplate jbdcTemplate = new JdbcTemplate(com.isa.thinair.airschedules.core.util.LookUpUtils.getDatasource());

		@SuppressWarnings("unchecked")
		Collection<FlightBasicInfoDTO> colFlownFlight = (Collection<FlightBasicInfoDTO>) jbdcTemplate.query(
				searchCriteria.toString(), new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Collection<FlightBasicInfoDTO> colFlownFlightsDetails = new ArrayList<FlightBasicInfoDTO>();
						while (rs.next()) {
							FlightBasicInfoDTO flownFlightsDetails = new FlightBasicInfoDTO();

							flownFlightsDetails.setFlightNumber(rs.getString("flight_number"));
							flownFlightsDetails.setDepartureDate(dateFormat.format(rs.getTimestamp("departure_date")));
							flownFlightsDetails.setSegment(rs.getString("segment_code"));

							colFlownFlightsDetails.add(flownFlightsDetails);
						}
						return colFlownFlightsDetails;
					}
				});
		return colFlownFlight;
	}

	/**
	 * update Pax and Eticket status when activate past flown flight
	 * @param flightNumber
	 * @param departureDate
	 * @throws ModuleException
	 */
	@Override
	public void updatePaxETStatus(String flightNumber, String departureDate) throws ModuleException {
		try {

			Date startDate = CalendarUtil.getStartTimeOfDate(inDateFormat.parse(departureDate));
			Date endDate = CalendarUtil.getEndTimeOfDate(inDateFormat.parse(departureDate));

			log.debug("Update PAX status to 'C' ");
			updatePaxStatus(flightNumber, startDate, endDate);

			log.debug("Update ET status to 'O' ");
			updateETStatus(flightNumber, startDate, endDate);

		} catch (ParseException e) {
			log.debug("Date parse exception ", e);
		}
	}

	@Override
	public void updateReservationAudit(String flightNumber, String flightDeptDate, CredentialsDTO credentialsDTO) throws ModuleException {
		try {

			Date stDt = CalendarUtil.getStartTimeOfDate(inDateFormat.parse(flightDeptDate));
			Date endDt = CalendarUtil.getEndTimeOfDate(inDateFormat.parse(flightDeptDate));

			// Update Reservation Audit
			log.debug("Update Reservation Audit ");
			Collection<PassengerTicketCouponAuditDTO> paxAuditDetails;
			paxAuditDetails = getUpdatedETicketId(flightNumber, stDt, endDt);
			if (!paxAuditDetails.isEmpty()) {
				ReservationAuditUpdate(paxAuditDetails, credentialsDTO);
			}

		} catch (ParseException e) {
			log.debug("Date parse exception ", e);
		}
	}

	//update pax and Eticket status when cancel/reprotect past flown flight
	public void updatePaxETStatus(Collection<Integer> getPaxFareSegIds) throws ModuleException {
		// Update Pax status to "C"
		updatePaxStatus(getPaxFareSegIds);

		// Update ET status to "O"
		updateETStatus(getPaxFareSegIds);

	}

	private void updatePaxStatus(Collection<Integer> getPaxFareSegIds) {
		JdbcTemplate jtPax = new JdbcTemplate(LookUpUtils.getDatasource());

		String sqlPax = " UPDATE T_PNR_PAX_FARE_SEGMENT SET PAX_STATUS='C' " + " WHERE PPFS_ID IN (" + BeanUtils
				.constructINStringForInts(getPaxFareSegIds) + ")";

		jtPax.update(sqlPax);

	}

	private void updateETStatus(Collection<Integer> getPaxFareSegIds) {
		JdbcTemplate jtET = new JdbcTemplate(LookUpUtils.getDatasource());

		String sqlET = "UPDATE T_PNR_PAX_FARE_SEG_E_TICKET SET STATUS='O' " + " WHERE PPFS_ID IN (" + BeanUtils
				.constructINStringForInts(getPaxFareSegIds) + ")";

		jtET.update(sqlET);
	}

	private void updateETStatus(Object FlightNum, Date stDt, Date endDt) {
		// Update ET status to "O"
		JdbcTemplate jtET = new JdbcTemplate(LookUpUtils.getDatasource());

	String sqlET = "UPDATE T_PNR_PAX_FARE_SEG_E_TICKET SET STATUS='O'  "
				+ " WHERE PPFS_ID IN (SELECT DISTINCT(PPFST.PPFS_ID)  "
				+ " FROM T_FLIGHT F, T_FLIGHT_SEGMENT FS ,T_PNR_SEGMENT PS,T_RESERVATION R,"
				+ " T_PNR_PAX_FARE_SEGMENT PPFS,T_PNR_PAX_FARE_SEG_E_TICKET PPFST , T_PNR_PAX_FARE PPF " 
				+ " WHERE F.FLIGHT_NUMBER='" + FlightNum + "' AND  F.DEPARTURE_DATE BETWEEN " 
				+ " TO_DATE('" + dateFormat.format(stDt) + "','dd-mm-yy HH24:mi:ss') "
				+ " AND TO_DATE('" + dateFormat.format(endDt) + "', 'dd-mm-yy HH24:mi:ss') "
				+ " AND FS.FLIGHT_ID = F.FLIGHT_ID AND PS.FLT_SEG_ID = FS.FLT_SEG_ID AND R.PNR = PS.PNR "
				+ " AND PS.PNR_SEG_ID = PPFS.PNR_SEG_ID AND PPFST.PPFS_ID = PPFS.PPFS_ID AND PPF.PPF_ID = PPFS.PPF_ID "
				+ " AND PPFS.PAX_STATUS NOT IN ('N')  and PPFST.STATUS NOT IN ('Z','E','V','R','S') ) ";

		jtET.update(sqlET);

	}

	private void updatePaxStatus(Object Flightnum, Date stDt, Date endDt) {
		// Update PAX status to "C"
		JdbcTemplate jtPax = new JdbcTemplate(LookUpUtils.getDatasource());

		String sqlPax = " UPDATE T_PNR_PAX_FARE_SEGMENT SET PAX_STATUS='C'   " + " WHERE PPFS_ID IN "
				+ " (SELECT DISTINCT(PPFS.PPFS_ID) FROM T_FLIGHT F, T_FLIGHT_SEGMENT FS, "
				+ " T_PNR_SEGMENT PS,T_PNR_PAX_FARE_SEGMENT PPFS, T_PNR_PAX_FARE_SEG_E_TICKET PPFST , T_PNR_PAX_FARE PPF " 
				+ " WHERE F.FLIGHT_NUMBER= '" + Flightnum + "' AND  F.DEPARTURE_DATE BETWEEN "
				+ " TO_DATE('" + dateFormat.format(stDt) + "','dd-mm-yy HH24:mi:ss') "
				+ " AND TO_DATE('" + dateFormat.format(endDt) + "', 'dd-mm-yy HH24:mi:ss') "
				+ " AND FS.FLIGHT_ID = F.FLIGHT_ID AND PS.FLT_SEG_ID = FS.FLT_SEG_ID AND "
				+ " PPFST.PPFS_ID = PPFS.PPFS_ID AND PS.PNR_SEG_ID = PPFS.PNR_SEG_ID AND PPF.PPF_ID = PPFS.PPF_ID  "
				+ " AND PPFS.PAX_STATUS NOT IN ('N') and PPFST.STATUS NOT IN ('Z','E','V','R','S')) ";

		jtPax.update(sqlPax);

	}

	private Collection<PassengerTicketCouponAuditDTO> getUpdatedETicketId(Object flightNum, Date stDt, Date endDt) {

		String sqlETId = "	SELECT DISTINCT PP.PNR_PAX_ID , PPFST.PNR_PAX_FARE_SEG_E_TICKET_ID,"
				+ " PP.TITLE,PP.FIRST_NAME, PP.LAST_NAME,PP.PNR "
				+ " FROM T_FLIGHT F, T_FLIGHT_SEGMENT FS, T_PNR_SEGMENT PS, T_PNR_PAX_FARE_SEGMENT PPFS,"
				+ " T_PNR_PAX_FARE_SEG_E_TICKET PPFST, T_PNR_PASSENGER PP, T_PNR_PAX_FARE PPF "

				+ " WHERE F.FLIGHT_NUMBER='"	+ flightNum + "' AND F.DEPARTURE_DATE BETWEEN "
				+ " TO_DATE('" + dateFormat.format(stDt) + "','dd-mm-yy HH24:mi:ss')"
				+ " AND TO_DATE('" + dateFormat.format(endDt) + "', 'dd-mm-yy HH24:mi:ss') "

				+ " AND FS.FLIGHT_ID = F.FLIGHT_ID AND PS.FLT_SEG_ID = FS.FLT_SEG_ID AND PS.PNR_SEG_ID = PPFS.PNR_SEG_ID "
				+ " AND PP.PNR_PAX_ID = PPF.PNR_PAX_ID	AND PPF.PPF_ID = PPFS.PPF_ID AND PPFST.PPFS_ID = PPFS.PPFS_ID "
				+ " AND PPFST.STATUS NOT IN ('Z','E','V','R','S') AND PPFS.PAX_STATUS NOT IN ('N') ORDER BY PP.PNR_PAX_ID ";

		JdbcTemplate jbdcTemplate = new JdbcTemplate(com.isa.thinair.airschedules.core.util.LookUpUtils.getDatasource());

		@SuppressWarnings("unchecked")
		Collection<PassengerTicketCouponAuditDTO> paxAuditDetails = (Collection<PassengerTicketCouponAuditDTO>) jbdcTemplate
				.query(sqlETId.toString(), new ResultSetExtractor() {
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Collection<PassengerTicketCouponAuditDTO> paxAuditDetail = new ArrayList<PassengerTicketCouponAuditDTO>();
						while (rs.next()) {
							PassengerTicketCouponAuditDTO couponAuditDetail = new PassengerTicketCouponAuditDTO();

							couponAuditDetail.seteTicketId(rs.getInt("PNR_PAX_FARE_SEG_E_TICKET_ID"));
							couponAuditDetail.setPnr(rs.getString("PNR"));
							couponAuditDetail.setPnrPaxId(rs.getString("PNR_PAX_ID"));
							couponAuditDetail.setPassengerName(rs.getString("TITLE") + " " + rs.getString("FIRST_NAME") + " "
									+ rs.getString("LAST_NAME"));

							paxAuditDetail.add(couponAuditDetail);
						}
						return paxAuditDetail;
					}
				});
		return paxAuditDetails;
	}

	private void ReservationAuditUpdate(Collection<PassengerTicketCouponAuditDTO> paxAuditDetails, CredentialsDTO credentialsDTO)
			throws ModuleException {
		String eTicketStatus = "O";
		String paxStatus = "C";
		Iterator<PassengerTicketCouponAuditDTO> passengerTicketCouponAuditDTO = paxAuditDetails.iterator();
		while (passengerTicketCouponAuditDTO.hasNext()) {

			PassengerTicketCouponAuditDTO paxEtAuditDetails = passengerTicketCouponAuditDTO.next();
			LccClientPassengerEticketInfoTO currentPaxCoupon = ReservationDAOUtils.DAOInstance.ETicketDAO
					.getEticketInfo(paxEtAuditDetails.geteTicketId());
			ReservationAudit reservationAudit = ReservationAuditCreator.createPassengerCouponUpdateAudit(currentPaxCoupon,
					eTicketStatus, paxStatus, paxEtAuditDetails);

			ReservationAudit.createReservationAudit(reservationAudit, credentialsDTO, false);
			reservationAudit.setDisplayName(credentialsDTO.getDisplayName());
			// Saves the reservation audit
			ReservationModuleUtils.getAuditorBD().audit(reservationAudit, reservationAudit.getContentMap());
			log.debug("Reservation Audit Updated ");

		}

	}

}
