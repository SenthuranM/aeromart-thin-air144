/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.check;

import java.text.SimpleDateFormat;
import java.util.Set;

import com.isa.thinair.airmaster.api.model.AircraftCabinCapacity;
import com.isa.thinair.airmaster.api.model.AircraftModel;
import com.isa.thinair.airmaster.api.service.AircraftBD;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command class to check model cabin class chenges
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="checkModelCabinClasses"
 */
public class CheckModelCabinClasses extends DefaultBaseCommand {

	private final AircraftBD aircraftBD;

	/**
	 * constructor of the CheckModelCabinClasses command
	 */
	public CheckModelCabinClasses() {

		aircraftBD = AirSchedulesUtil.getAircraftBD();

	}

	/**
	 * execute method of the CheckModelCabinClasses command
	 * 
	 * @throws ModuleException
	 */
	@Override
	public ServiceResponce execute() throws ModuleException {

		// getting command params
		Flight updated = (Flight) this.getParameter(CommandParamNames.FLIGHT);
		Flight existing = (Flight) this.getParameter(CommandParamNames.EXISTING_FLIGHT);

		// checking params
		this.checkParams(updated, existing);

		// constructing command responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);

		AircraftModel model = aircraftBD.getAircraftModel(updated.getModelNumber());
		AircraftModel existingModel = aircraftBD.getAircraftModel(existing.getModelNumber());

		// checking the model changes
		if (existing.getModelNumber() != updated.getModelNumber()) {
			Set<AircraftCabinCapacity> existingCCs = existingModel.getAircraftCabinCapacitys();
			boolean ccOk = true;
			if (existingCCs != null) {
				for (AircraftCabinCapacity ccEx : existingCCs) {
					Set<AircraftCabinCapacity> updCCs = model.getAircraftCabinCapacitys();
					if (updCCs != null) {
						boolean machinFound = false;
						for (AircraftCabinCapacity ccUpd : updCCs) {
							if (ccUpd.getCabinClassCode().equals(ccEx.getCabinClassCode())) {
								machinFound = true;
								break;
							}
						}
						if (!machinFound) {
							ccOk = false;
						}
						break;
					} else {
						ccOk = false;
					}
				}
			} else {
				ccOk = false;
			}

			if (!ccOk) {
				responce.setSuccess(false);
				responce.setResponseCode(ResponceCodes.CABIN_CLASS_NOT_FOUND);
				SimpleDateFormat format = new SimpleDateFormat("dd-MM");
				responce.addResponceParam(ResponceCodes.DATE, format.format(existing.getDepartureDate()));
				return responce;
			}
		}
		// return command responce
		return responce;
	}

	/**
	 * private method to validate parameters
	 * 
	 * @param schedule
	 * @throws ModuleException
	 */
	private void checkParams(Flight flight, Flight existing) throws ModuleException {

		if (flight == null || existing == null) {
			throw new ModuleException("airschedules.arg.invalid.null");
		}
	}
}
