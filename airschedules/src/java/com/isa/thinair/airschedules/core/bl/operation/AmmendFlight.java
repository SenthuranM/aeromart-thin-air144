/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.operation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.CreateFlightInventoryRQ;
import com.isa.thinair.airinventory.api.dto.SegmentDTO;
import com.isa.thinair.airinventory.api.service.FlightInventoryBD;
import com.isa.thinair.airinventory.api.service.LogicalCabinClassBD;
import com.isa.thinair.airmaster.api.model.AircraftCabinCapacity;
import com.isa.thinair.airmaster.api.model.AircraftModel;
import com.isa.thinair.airmaster.api.model.RouteInfo;
import com.isa.thinair.airmaster.api.service.AircraftBD;
import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.airreservation.api.dto.FlightReservationAlertDTO;
import com.isa.thinair.airschedules.api.dto.FlightAlertDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightLeg;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.AlertUtil;
import com.isa.thinair.airschedules.api.utils.FlightUtil;
import com.isa.thinair.airschedules.api.utils.LegUtil;
import com.isa.thinair.airschedules.api.utils.LocalZuluTimeAdder;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.airschedules.api.utils.SegmentUtil;
import com.isa.thinair.airschedules.core.bl.DefaultAnciTemplateAssignBL;
import com.isa.thinair.airschedules.core.persistence.dao.FlightDAO;
import com.isa.thinair.airschedules.core.persistence.dao.FlightLegDAO;
import com.isa.thinair.airschedules.core.persistence.dao.FlightScheduleDAO;
import com.isa.thinair.airschedules.core.persistence.dao.FlightSegementDAO;
import com.isa.thinair.airschedules.core.util.CalculationUtil;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.InternalConstants;
import com.isa.thinair.airschedules.core.util.OverlapDetailUtil;
import com.isa.thinair.airschedules.core.util.ScheduledFlightUtil;
import com.isa.thinair.commons.api.dto.CabinClassWiseDefaultLogicalCCDetailDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.Pair;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for the ammend flight
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="ammendFlight"
 */
public class AmmendFlight extends DefaultBaseCommand {

	// Dao's
	private final FlightScheduleDAO flightSchedleDAO;
	private final FlightDAO flightDAO;
	private final FlightSegementDAO flightSegmentDAO;
	private final FlightLegDAO flightLegDAO;
	private final AirportBD airportBD;
	private final AircraftBD aircraftBD;
	private final CommonMasterBD commonMasterBD;
	private final FlightInventoryBD inventoryBD;
	private final LogicalCabinClassBD logicalCabinClassBD;

	// helpers
	private final LocalZuluTimeAdder timeAdder;

	/**
	 * constructor of the AmmendFlight command
	 */
	public AmmendFlight() {

		// looking up daos
		flightDAO = (FlightDAO) AirSchedulesUtil.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_DAO);
		flightSchedleDAO = (FlightScheduleDAO) AirSchedulesUtil.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.FLIGHT_SCHEDULE_DAO);
		flightSegmentDAO = (FlightSegementDAO) AirSchedulesUtil.getInstance().getLocalBean(
				InternalConstants.DAOProxyNames.FLIGHT_SEGMENT_DAO);
		flightLegDAO = (FlightLegDAO) AirSchedulesUtil.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FLIGHT_LEG_DAO);

		// look up BDs
		airportBD = AirSchedulesUtil.getAirportBD();
		commonMasterBD = AirSchedulesUtil.getCommonMasterBD();
		aircraftBD = AirSchedulesUtil.getAircraftBD();
		inventoryBD = AirSchedulesUtil.getFlightInventoryBD();
		logicalCabinClassBD = AirSchedulesUtil.getLogicalCabinClassBD();
		// helpers
		timeAdder = new LocalZuluTimeAdder(airportBD);
	}

	/**
	 * execute method of the AmmendFlight command
	 * 
	 * @throws ModuleException
	 */
	@Override
	@SuppressWarnings("unused")
	public ServiceResponce execute() throws ModuleException {

		// getting command params
		Flight updated = (Flight) this.getParameter(CommandParamNames.FLIGHT);
		FlightAlertDTO flightAlertDTO = (FlightAlertDTO) this.getParameter(CommandParamNames.FLIGHT_ALERT_DTO);
		Boolean downgradeToAnyModel = (Boolean) this.getParameter(CommandParamNames.FLIGHT_DOWNGRADE_TO_ANY_MODEL);
		boolean isScheduleMessageProcessing = getParameter(CommandParamNames.IS_SHEDULE_MESSAGE_PROCESS, Boolean.FALSE,
				Boolean.class);
		boolean isEnableAttachDefaultAnciTemplate = getParameter(CommandParamNames.IS_ENABLE_ATACH_DEFAULT_ANCI_TEMPL,
				Boolean.FALSE, Boolean.class);

		DefaultServiceResponse warning = (DefaultServiceResponse) this.getParameter(CommandParamNames.WARNING);

		if (warning != null) {
			return warning;
		}

		// checking params
		this.checkParams(updated);
		
		//Collecting Flights to Publish 
		Collection<Flight> flightsToPublish = new ArrayList<Flight>();

		// get the flight from database
		Flight existing = flightDAO.getFlight(updated.getFlightId().intValue());
		// if no flight found for id throw exception
		if (existing == null) {
			throw new ModuleException("airschedules.logic.bl.flight.noflight");
		}

		// set the depature and arrival airport codes of the schedule
		existing.setOriginAptCode(LegUtil.getFirstFlightLeg(updated.getFlightLegs()).getOrigin());
		existing.setDestinationAptCode(LegUtil.getLastFlightLeg(updated.getFlightLegs()).getDestination());

		// collection of altered flights for clear the cache
		Collection<Pair<Integer, String>> alertFlightIds = new ArrayList<Pair<Integer, String>>();

		// checking overlapping situations
		boolean overlapAdded = updated.isOverlapping() && !existing.isOverlapping();
		boolean overlapDeleted = existing.isOverlapping() && !updated.isOverlapping();
		boolean overlapEdited = updated.isOverlapping() && existing.isOverlapping();

		Flight overlapping = null;
		int millsDiff = 0;
		long departureDateDeltaForOlpFlt = 0;

		if (overlapAdded || overlapEdited) {

			overlapping = flightDAO.getFlight(updated.getOverlapingFlightId().intValue());
			// if no flight found for the ammending flight schedule
			if (overlapping == null) {
				throw new ModuleException("airschedules.logic.bl.flight.noflight");
			}

			millsDiff = (int) (overlapping.getDepartureDate().getTime() - existing.getDepartureDate().getTime());

		} else if (overlapDeleted) {

			overlapping = flightDAO.getFlight(existing.getOverlapingFlightId().intValue());
			// if no flight found for the ammending flight schedule
			if (overlapping == null) {
				throw new ModuleException("airschedules.logic.bl.flight.noflight");
			}

			millsDiff = (int) (overlapping.getDepartureDate().getTime() - existing.getDepartureDate().getTime());

		}

		// prepare alert/email DTO for flight
		Collection<FlightReservationAlertDTO> flightReservationAlertDTOList = new ArrayList<FlightReservationAlertDTO>();

		Flight oldFlight = null;
		Flight oldOvlFlight = null;

		if (AlertUtil.shouldRescheduleNotified(flightAlertDTO)) {
			oldFlight = FlightUtil.getCopyOfFlight(existing);
			oldFlight = timeAdder.addLocalTimeDetailsToFlight(oldFlight);
			if (overlapping != null) {
				oldOvlFlight = FlightUtil.getCopyOfFlight(overlapping);
				oldOvlFlight = timeAdder.addLocalTimeDetailsToFlight(oldOvlFlight);
			}
		}

		boolean departureDateChanged = false;
		boolean manuallyChanged = overlapAdded || overlapDeleted;
		boolean scheduled = (existing.getScheduleId() != null) ? true : false;
		boolean isSplitSchedule = false;
		String existingAircraftModelNumber = existing.getModelNumber();
		Integer existingMealTemplateId = existing.getMealTemplateId();
		Integer existingSeatTemplateId = existing.getSeatChargeTemplateId();
		Integer scheduleId = null;
		if(scheduled){
			scheduleId = existing.getScheduleId();
		}
		
		// updating flight type
		if (updated.getFlightType() != null && !"".equals(updated.getFlightType())
				&& !updated.getFlightType().equals(existing.getFlightType())) {
			existing.setFlightType(updated.getFlightType());
			manuallyChanged = true;
		}

		if (isFieldModified(existing.getRemarks(), updated.getRemarks())) {
			existing.setRemarks(updated.getRemarks());
			manuallyChanged = true;
		}

		if (isFieldModified(existing.getUserNotes(), updated.getUserNotes())) {
			existing.setUserNotes(updated.getUserNotes());
			manuallyChanged = true;
		}

		if (existing.getMealTemplateId() == null
				|| (updated.getMealTemplateId() != null && updated.getMealTemplateId() != existing.getMealTemplateId())) {
			existing.setMealTemplateId(updated.getMealTemplateId());
			manuallyChanged = true;
		}

		if (existing.getSeatChargeTemplateId() == null
				|| updated.getSeatChargeTemplateId() == null
				|| (updated.getSeatChargeTemplateId() != null && updated.getSeatChargeTemplateId() != existing
						.getSeatChargeTemplateId())) {
			existing.setSeatChargeTemplateId(updated.getSeatChargeTemplateId());
			manuallyChanged = true;
		}
		
		if (isFieldModified(existing.getCsOCCarrierCode(), updated.getCsOCCarrierCode())) {
			existing.setCsOCCarrierCode(updated.getCsOCCarrierCode());
			manuallyChanged = true;			
		}
		
		if (isFieldModified(existing.getCsOCFlightNumber(), updated.getCsOCFlightNumber())) {
			existing.setCsOCFlightNumber(updated.getCsOCFlightNumber());
			manuallyChanged = true;			
		}

		if (overlapEdited) {
			// update all flight before any operation
			departureDateDeltaForOlpFlt = OverlapDetailUtil.updateOverlappingFromUpdated(overlapping, updated, existing);

			if (scheduled) {
				overlapping.setManuallyChanged(true);
			}

			// prepare segment details of the flight
			overlapping = timeAdder.addOnlySegementTimeDetails(overlapping);
		}

		// updating depature date
		if (updated.getDepartureDate().getTime() != existing.getDepartureDate().getTime()) {

			existing.setDepartureDate(updated.getDepartureDate());
			existing.setDayNumber(CalendarUtil.getDayNumber(updated.getDepartureDate()));
			existing.setScheduleId(null); // AARESAA-17249
			manuallyChanged = true;
			isSplitSchedule = true;
			departureDateChanged = true;
			for (Iterator<FlightSegement> fltSegIte = updated.getFlightSegements().iterator(); fltSegIte.hasNext();) {
				FlightSegement tmpFltSeg = fltSegIte.next();
				alertFlightIds.add(Pair.of(updated.getFlightId(), tmpFltSeg.getSegmentCode()));
			}
		}

		// updating flight number
		if (!updated.getFlightNumber().equals(existing.getFlightNumber())) {

			existing.setFlightNumber(updated.getFlightNumber());
			manuallyChanged = true;
			isSplitSchedule = true;
			for (Iterator<FlightSegement> fltSegIte = updated.getFlightSegements().iterator(); fltSegIte.hasNext();) {
				FlightSegement tmpFltSeg = fltSegIte.next();
				alertFlightIds.add(Pair.of(updated.getFlightId(), tmpFltSeg.getSegmentCode()));
			}
		}

		// updating the optype
		if (updated.getOperationTypeId() != existing.getOperationTypeId()) {

			existing.setOperationTypeId(updated.getOperationTypeId());
			manuallyChanged = true;
			isSplitSchedule = true;
			if (overlapEdited) {

				overlapping.setOperationTypeId(updated.getOperationTypeId());
				if (scheduled) {
					overlapping.setManuallyChanged(true);
				}
			}
		}

		// model number changes
		if (!updated.getModelNumber().equals(existing.getModelNumber())) {
			existing.setOldModelNumber(existing.getModelNumber());
			existing.setModelNumber(updated.getModelNumber());
			manuallyChanged = true;
			isSplitSchedule = true;
			if (overlapEdited) {

				overlapping.setModelNumber(updated.getModelNumber());
				if (scheduled) {
					overlapping.setManuallyChanged(true);
				}
			}
		}

		// updating legs and segments
		boolean legsAddedDeleted = LegUtil.checkLegsAddedDeleted(existing, updated);

		// protection against concurrent updates
		existing.setVersion(updated.getVersion());
		existing.setModifiedBy(updated.getModifiedBy());
		existing.setModifiedDate(updated.getModifiedDate());

		if (!legsAddedDeleted) {

			// update the legs and segments
			// [NOTE] if departure date changed, update flight segment timings
			boolean isUpdated = this.updateLegsAndSegments(updated, existing, departureDateChanged);
			if (isUpdated) {
				manuallyChanged = isUpdated;
			}

		} else {

			Collection<Integer> flightIds = new ArrayList<Integer>();
			flightIds.add(existing.getFlightId());

			boolean overlapExists = existing.isOverlapping();

			if (overlapExists) {
				// remove overlap before adding/deleting legs
				overlapping = OverlapDetailUtil.removeOverlapDetailsFromFlight(overlapping);
				overlapping = timeAdder.addLocalTimeDetailsToFlight(overlapping);
				flightDAO.saveFlight(overlapping);

				// remove overlaping in inventory
				Collection<Integer> bothFlightIds = new ArrayList<Integer>();
				bothFlightIds.add(existing.getFlightId());
				bothFlightIds.add(overlapping.getFlightId());
				inventoryBD.removeInventoryOverlap(bothFlightIds);
			}
			// remove inventory
			inventoryBD.deleteFlightInventories(flightIds);

			// delete existing legs and add new legs
			this.createNewLegsAndSegments(updated, existing);

			// creating inventory done later

			if (overlapExists) {
				// add overlap after adding/deleting legs
				existing = OverlapDetailUtil.addOverlapDetailsToFlight(existing, overlapping);
				// flightDAO.saveFlight(existing);
			}
			manuallyChanged = true;
		}

		// updating the status
		if (!existing.getStatus().equals(updated.getStatus())) {

			existing.setStatus(updated.getStatus());
			manuallyChanged = true;
		}

		// save the flight
		if (manuallyChanged) {

			// add remove overlap details form updated schedule
			if (overlapAdded) {

				existing = OverlapDetailUtil.addOverlapDetailsToFlight(existing, overlapping);

			} else if (overlapDeleted) {

				existing = OverlapDetailUtil.removeOverlapDetailsFromFlight(existing);
			}

			if (scheduled) {
				existing.setManuallyChanged(true);
				// existing.setVersion(updated.getVersion());
			}

			AircraftModel updatedAircraftModel = aircraftBD.getAircraftModel(existing.getModelNumber());
			int allocation = CalculationUtil.getAllocation(updatedAircraftModel.getAircraftCabinCapacitys());

			// recalculate ASK and set to the flight
			if (legsAddedDeleted || !updated.getModelNumber().equals(existingAircraftModelNumber)) {

				// avilable seat kilometers
				double flightDistance = this.getFlightDistance(existing.getFlightLegs()).doubleValue();

				int avilableSeatKilosFlt = CalculationUtil.getASKforFlight(allocation, flightDistance);
				existing.setAvailableSeatKilometers(new Integer(avilableSeatKilosFlt));

				// recalculate ASK and set to overlapping flight
				if (overlapAdded || overlapEdited) {

					flightDistance = this.getFlightDistance(overlapping.getFlightLegs()).doubleValue();

					avilableSeatKilosFlt = CalculationUtil.getASKforFlight(allocation, flightDistance);
					overlapping.setAvailableSeatKilometers(new Integer(avilableSeatKilosFlt));
				}
			}
			

			if (updated.getCodeShareMCFlights() != existing.getCodeShareMCFlights()) {
				existing.setCodeShareMCFlights(updated.getCodeShareMCFlights());
			}

			if (isEnableAttachDefaultAnciTemplate) {
				DefaultAnciTemplateAssignBL.setDefaultAnciTemplatesToUpdatedFlight(existing, existingMealTemplateId,
						existingSeatTemplateId, isScheduleMessageProcessing);
			}

			existing = timeAdder.addLocalTimeDetailsToFlight(existing);
			flightDAO.saveFlight(existing);
			
			//Add the flights to the list which holds the publish flight list
			flightsToPublish.add(existing);

			this.addManuallyChangedDetailsToSchedule(existing);

			// add remove overlap details to overlapping schedule
			if (overlapping != null && (overlapAdded || legsAddedDeleted)) {

				overlapping = OverlapDetailUtil.addOverlapDetailsToFlight(overlapping, existing);
				overlapping = timeAdder.addLocalTimeDetailsToFlight(overlapping);
				flightDAO.saveFlight(overlapping);
				this.addManuallyChangedDetailsToSchedule(overlapping);
				// TODO: add inventory overlap -- INVENTORY does not support this operation yet!
				
			} else if (overlapDeleted) {

				overlapping = OverlapDetailUtil.removeOverlapDetailsFromFlight(overlapping);
				overlapping = timeAdder.addLocalTimeDetailsToFlight(overlapping);
				flightDAO.saveFlight(overlapping);
				this.addManuallyChangedDetailsToSchedule(overlapping);

				// [TODO:test]remove inventory overlap
				Collection<Integer> flightIds = new ArrayList<Integer>();
				flightIds.add(existing.getFlightId());
				flightIds.add(overlapping.getFlightId());
				inventoryBD.removeInventoryOverlap(flightIds);

			} else if (overlapEdited) {
				// update overlapping flight information
				overlapping = timeAdder.addLocalTimeDetailsToFlight(overlapping);
				flightDAO.saveFlight(overlapping);
				this.addManuallyChangedDetailsToSchedule(overlapping);
			}
			
			//Add the flights to the list which holds the publish flight list
			//Add overlapping because overlapping can be edited or deleted from above block
			if(overlapping !=null && (overlapAdded || overlapEdited || overlapDeleted)){
				flightsToPublish.add(overlapping);
			}

			if (legsAddedDeleted || !updated.getModelNumber().equals(existingAircraftModelNumber)) {

				this.addASKToSchedule(existing);

				if (overlapAdded || overlapEdited) {
					this.addASKToSchedule(overlapping);
				}
			}

			// prepare alert/email DTO
			if (AlertUtil.shouldRescheduleNotified(flightAlertDTO)) {

				Flight newFlight = FlightUtil.getCopyOfFlight(existing);
				newFlight = timeAdder.addLocalTimeDetailsToFlight(newFlight);
				FlightReservationAlertDTO resAlert = AlertUtil.prepareFlightAlertInfo(oldFlight, newFlight);
				// add only updated flights
				if (resAlert != null) {
					flightReservationAlertDTOList.add(resAlert);
				}

				if (overlapping != null) {
					Flight newOvlFlight = FlightUtil.getCopyOfFlight(overlapping);
					newOvlFlight = timeAdder.addLocalTimeDetailsToFlight(newOvlFlight);
					FlightReservationAlertDTO resOvlAlert = AlertUtil.prepareFlightAlertInfo(oldOvlFlight, newOvlFlight);
					if (resOvlAlert != null) {
						flightReservationAlertDTOList.add(resOvlAlert);
					}
				}
			}

			Flight olFlight = null;
			if (overlapAdded || overlapEdited) {
				olFlight = overlapping;
			}

			if (legsAddedDeleted) {
				// [TODO:enhance]Implement add segment inventory, remove segment inventory functionality

				// create flight inventory
				Collection<SegmentDTO> intersepSegList = SegmentUtil.createSegmentDTOList(existing.getFlightId().intValue(),
						existing.getFlightSegements(), olFlight != null ? olFlight.getFlightId() : null,
						olFlight != null ? olFlight.getFlightSegements() : null);

				AircraftModel model = aircraftBD.getAircraftModel(updated.getModelNumber());
				CreateFlightInventoryRQ createFlightInventoryRQ = new CreateFlightInventoryRQ();
				createFlightInventoryRQ.setFlightId(updated.getFlightId());
				createFlightInventoryRQ.setAircraftModel(model);
				createFlightInventoryRQ.setSegmentDTOs(intersepSegList);
				createFlightInventoryRQ.setOverlappingFlightId(olFlight != null ? olFlight.getFlightId() : null);
				Map<String, String> defaultLogicalCCs = new HashMap<String, String>();
				for (AircraftCabinCapacity ccCapacity : model.getAircraftCabinCapacitys()) {
					for (CabinClassWiseDefaultLogicalCCDetailDTO defaultLogicalCCDetailDTO : CommonsServices.getGlobalConfig()
							.getDefaultLogicalCCDetails()) {
						if (ccCapacity.getCabinClassCode() != null
								&& ccCapacity.getCabinClassCode().equalsIgnoreCase(defaultLogicalCCDetailDTO.getCcCode())) {
							defaultLogicalCCs.put(ccCapacity.getCabinClassCode(),
									defaultLogicalCCDetailDTO.getDefaultLogicalCCCode());
							break;
						}
					}
				}
				createFlightInventoryRQ.setDefaultLogicalCCs(defaultLogicalCCs);

				inventoryBD.createFlightInventory(createFlightInventoryRQ);

			} else if (!updated.getModelNumber().equals(existingAircraftModelNumber)) {
				// ---------------------Update inventory if aircraft model changed
				Collection<Integer> flightIds = new ArrayList<Integer>();
				flightIds.add(existing.getFlightId());
				AircraftModel existingAircraftModel = aircraftBD.getAircraftModel(existingAircraftModelNumber);
				Collection<SegmentDTO> segmentDTOsCollection = new ArrayList<SegmentDTO>();
				Collection<Integer> olFlightIds = new ArrayList<Integer>();

				boolean newCabinClassesAdded = ScheduledFlightUtil.isNewCabinClassesAdded(updatedAircraftModel,
						existingAircraftModel);
				if (newCabinClassesAdded) {
					if (olFlight != null) {
						olFlightIds.add(olFlight.getFlightId());
					}
					segmentDTOsCollection.addAll(SegmentUtil.createSegmentDTOList(existing.getFlightId().intValue(),
							existing.getFlightSegements(), null, null));
				}

				// for applying aircraft model change to the overlapping flight
				if (olFlight != null) {
					flightIds.add(olFlight.getFlightId());
					if (newCabinClassesAdded) {
						olFlightIds.add(existing.getFlightId());
						segmentDTOsCollection.addAll(SegmentUtil.createSegmentDTOList(olFlight.getFlightId().intValue(),
								olFlight.getFlightSegements(), existing.getFlightId(), existing.getFlightSegements()));
					}
				}

				String flightSegmentCode = "";
				FlightSegement tmpSegment = SegmentUtil.getDetailsOfLongestSegment(existing.getFlightSegements());
				flightSegmentCode = tmpSegment.getSegmentCode();

				inventoryBD.updateFCCInventoriesForModelChange(flightIds, updatedAircraftModel, existingAircraftModel,
						olFlightIds, segmentDTOsCollection, flightSegmentCode, downgradeToAnyModel);
			}

			if (alertFlightIds.size() != 0) {
				inventoryBD.clearCachedFlightIds(alertFlightIds);
			}
		}

		// constructing and return command responce
		DefaultServiceResponse responce = new DefaultServiceResponse(true);
		if (responce.isSuccess()) {
			responce.setResponseCode(ResponceCodes.AMMEND_FLIGHT_SUCCESSFULL);
			responce.addResponceParam(CommandParamNames.FLIGHT_SEGMENT_EMAIL_DTO_COLLECTION, flightReservationAlertDTOList);
			if (overlapAdded || overlapEdited || overlapDeleted) {
				responce.addResponceParam(CommandParamNames.OVERLAPPING_FLIGHT, overlapping);
			}
			responce.addResponceParam(CommandParamNames.LAST_RESPONSE, responce);
			responce.addResponceParam(CommandParamNames.FLIGHTS_TO_PUBLISH, flightsToPublish);
			responce.addResponceParam(CommandParamNames.FLIGHT, existing);
			responce.addResponceParam(CommandParamNames.SCHEDULE_ID, scheduleId);
			// Unlink schedule
			if (AppSysParamsUtil.isSplitAndUpdateScheduleForScheduleConflicts() && scheduled && isSplitSchedule) {
				responce.addResponceParam(CommandParamNames.IS_SCHEDULE_SPLIT, true);
			}
		}
		return responce;
	}

	/**
	 * privte method to validate parameters
	 * 
	 * @param schedule
	 * @throws ModuleException
	 */
	private void checkParams(Flight flight) throws ModuleException {

		if (flight == null) {
			throw new ModuleException("airschedules.arg.invalid.null");
		}
		if ((flight.getFlightNumber() == null) || (flight.getFlightNumber().trim().equals(new String("")))) {
			throw new ModuleException("airschedules.arg.invalid.null");
			// TODO must check all attributes set from front end
			// causes lot of errors due to front end not setting all requied fields in the flight object
		}

	}

	/**
	 * private method to update legs and segments
	 * 
	 * @param updated
	 * @param existing
	 * @return
	 * @throws ModuleException
	 */
	private boolean updateLegsAndSegments(Flight updated, Flight existing, boolean departureDateChanged) throws ModuleException {

		boolean legTimechanged = false;
		boolean segmentChnged = false;

		// updating the legs
		Set<FlightLeg> updLegs = updated.getFlightLegs();
		Set<FlightLeg> extLegs = existing.getFlightLegs();

		Iterator<FlightLeg> itUpdLeg = updLegs.iterator();
		while (itUpdLeg.hasNext()) {

			FlightLeg updleg = itUpdLeg.next();

			Iterator<FlightLeg> itExtLeg = extLegs.iterator();

			while (itExtLeg.hasNext()) {

				FlightLeg extleg = itExtLeg.next();

				// get the corresponding legs
				if (updleg.getLegNumber() == extleg.getLegNumber()) {

					// check the leg arr/dep times and arr/dep offset
					if ((updleg.getEstArrivalTimeZulu().getTime() != extleg.getEstArrivalTimeZulu().getTime())
							|| (updleg.getEstDepartureTimeZulu().getTime() != extleg.getEstDepartureTimeZulu().getTime())
							|| extleg.getEstArrivalDayOffset() != updleg.getEstArrivalDayOffset()
							|| extleg.getEstDepartureDayOffset() != updleg.getEstDepartureDayOffset()) {

						extleg.setEstArrivalTimeZulu(updleg.getEstArrivalTimeZulu());
						extleg.setEstArrivalTimeLocal(updleg.getEstArrivalTimeLocal());
						extleg.setEstArrivalDayOffset(updleg.getEstArrivalDayOffset());
						extleg.setEstArrivalDayOffsetLocal(updleg.getEstArrivalDayOffsetLocal());

						extleg.setEstDepartureTimeZulu(updleg.getEstDepartureTimeZulu());
						extleg.setEstDepartureTimeLocal(updleg.getEstDepartureTimeLocal());
						extleg.setEstDepartureDayOffset(updleg.getEstDepartureDayOffset());
						extleg.setEstDepartureDayOffsetLocal(updleg.getEstDepartureDayOffsetLocal());

						extleg.setDuration(updleg.getDuration());
						legTimechanged = true;
					}

					break;
				}
			}
		}

		// updating the segments
		Set<FlightSegement> updSegs = updated.getFlightSegements();
		Set<FlightSegement> extSegs = existing.getFlightSegements();

		// [NOTE] if (leg timing OR departure date) changed, update flight segment timings
		if (legTimechanged || departureDateChanged) {

			// updating the segment times
			LocalZuluTimeAdder timeAdder = new LocalZuluTimeAdder(airportBD);
			existing = timeAdder.addOnlySegementTimeDetails(existing);
		}

		// updating valid invalid
		Iterator<FlightSegement> itUpdSeg = updSegs.iterator();
		while (itUpdSeg.hasNext()) {

			FlightSegement updseg = itUpdSeg.next();

			Iterator<FlightSegement> itExtSeg = extSegs.iterator();
			while (itExtSeg.hasNext()) {

				FlightSegement extseg = itExtSeg.next();

				// get the corresponding segments
				if (updseg.getSegmentCode().equals(extseg.getSegmentCode())) {

					// if invalid --> valid
					if (updseg.getValidFlag() == true && extseg.getValidFlag() == false) {
						extseg.setValidFlag(true);
						segmentChnged = true;

						// [TODO:test] inventory integration
						Collection<Integer> fltSegmentIds = new ArrayList<Integer>();
						fltSegmentIds.add(extseg.getFltSegId());
						inventoryBD.makeInvalidFltSegInvsValid(fltSegmentIds);

						// if valid --> invalid
					} else if (updseg.getValidFlag() == false && extseg.getValidFlag() == true) {

						extseg.setValidFlag(false);
						segmentChnged = true;

						// [TODO:test] inventory integration
						Collection<Integer> fltSegmentIds = new ArrayList<Integer>();
						fltSegmentIds.add(extseg.getFltSegId());
						inventoryBD.invalidateFlightSegmentInventories(fltSegmentIds);
					}
					if (isTerminalEdited(updated, existing)) {
						// Added to update arrival and departure terminals
						extseg.setArrivalTerminalId(updseg.getArrivalTerminalId());
						extseg.setDepartureTerminalId(updseg.getDepartureTerminalId());
						segmentChnged = true;
					}
					break;
				}
			}
		}

		return (legTimechanged || segmentChnged);
	}

	/**
	 * private method to create legs and segments
	 * 
	 * @param updated
	 * @param existing
	 * @throws ModuleException
	 */
	private void createNewLegsAndSegments(Flight updated, Flight existing) throws ModuleException {

		Set<FlightLeg> oldLegs = existing.getFlightLegs();
		Iterator<FlightLeg> itOldLegs = oldLegs.iterator();
		while (itOldLegs.hasNext()) {
			FlightLeg oldLeg = itOldLegs.next();
			flightLegDAO.removeFlightLeg(oldLeg.getId().intValue());
		}

		Set<FlightSegement> oldSegments = existing.getFlightSegements();
		Iterator<FlightSegement> itOldsegments = oldSegments.iterator();
		while (itOldsegments.hasNext()) {
			FlightSegement oldSegment = itOldsegments.next();
			flightSegmentDAO.removeFlightSegement(oldSegment.getFltSegId().intValue());
		}

		existing.getFlightLegs().clear();
		existing.getFlightSegements().clear();

		Set<FlightLeg> updatedLegs = updated.getFlightLegs();
		Iterator<FlightLeg> uit = updatedLegs.iterator();

		while (uit.hasNext()) {

			FlightLeg uleg = uit.next();
			existing.addLeg(LegUtil.copyLeg(uleg, new FlightLeg()));
		}

		// update flight segments
		Set<FlightSegement> updatedSeg = updated.getFlightSegements();
		Iterator<FlightSegement> usit = updatedSeg.iterator();

		while (usit.hasNext()) {

			FlightSegement uSeg = usit.next();
			existing.addsegment(SegmentUtil.copySegment(uSeg, new FlightSegement()));

		}

		LocalZuluTimeAdder timeAdder = new LocalZuluTimeAdder(airportBD);
		existing = timeAdder.addOnlySegementTimeDetails(existing);
	}

	/**
	 * methos to add manually changed details to the flight
	 * 
	 * @param flight
	 */
	private void addManuallyChangedDetailsToSchedule(Flight flight) {

		if (flight.getScheduleId() != null) {

			FlightSchedule schedule = flightSchedleDAO.getFlightSchedule(flight.getScheduleId().intValue());
			schedule.setManuallyChanged(true);
			flightSchedleDAO.saveFlightSchedule(schedule);
		}
	}

	/**
	 * methos to add manually changed details to the flight
	 * 
	 * @param flight
	 */
	private void addASKToSchedule(Flight flight) throws ModuleException {

		if (flight.getScheduleId() != null) {

			FlightSchedule schedule = flightSchedleDAO.getFlightSchedule(flight.getScheduleId().intValue());

			int ask = flightSchedleDAO.getTotalASKForSchedule(schedule.getScheduleId().intValue());
			schedule.setAvailableSeatKilometers(new Integer(ask));

			flightSchedleDAO.saveFlightSchedule(schedule);
		}
	}

	/**
	 * private method to get the flight distance for the flight
	 * 
	 * @param legList
	 * @return flight distance
	 * @throws ModuleException
	 */
	private BigDecimal getFlightDistance(Collection<FlightLeg> legList) throws ModuleException {

		BigDecimal flightDistance = AccelAeroCalculator.getDefaultBigDecimalZero();

		Iterator<FlightLeg> itLegs = legList.iterator();
		while (itLegs.hasNext()) {
			FlightLeg leg = itLegs.next();
			String routeId = leg.getOrigin() + "/" + leg.getDestination();
			leg.setModelRouteId(routeId);
			RouteInfo routeInfo = commonMasterBD.getRouteInfo(routeId);
			flightDistance = AccelAeroCalculator.add(flightDistance, routeInfo.getDistance());
		}

		return flightDistance;
	}

	private boolean isTerminalEdited(Flight updated, Flight existing) {

		Set<FlightSegement> oldSegments = existing.getFlightSegements();
		Iterator<FlightSegement> itOldsegments = oldSegments.iterator();
		// update flight segments
		Set<FlightSegement> updatedSeg = updated.getFlightSegements();
		Iterator<FlightSegement> usit = updatedSeg.iterator();

		while (usit.hasNext()) {
			while (itOldsegments.hasNext()) {
				FlightSegement uSeg = usit.next();
				FlightSegement oldSegment = itOldsegments.next();
				if (uSeg.getSegmentCode().equals(oldSegment.getSegmentCode())) {
					if (uSeg.getArrivalTerminalId() != oldSegment.getArrivalTerminalId()
							|| uSeg.getDepartureTerminalId() != oldSegment.getDepartureTerminalId()) {
						return true;
					}
				}
			}

		}

		return false;
	}

	private static boolean isFieldModified(String oldValue, String newValue) {

		if (oldValue == null) {
			oldValue = "";
		}

		if (newValue == null) {
			newValue = "";
		}

		oldValue = oldValue.trim();
		newValue = newValue.trim();

		if (!oldValue.equals(newValue)) {
			return true;
		}

		return false;
	}
}
