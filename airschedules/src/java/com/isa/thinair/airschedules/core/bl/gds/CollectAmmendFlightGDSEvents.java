/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.gds;

import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.airschedules.api.utils.FlightUtil;
import com.isa.thinair.airschedules.api.utils.GDSFlightEventCollector;
import com.isa.thinair.airschedules.api.utils.LegUtil;
import com.isa.thinair.airschedules.api.utils.LocalZuluTimeAdder;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for integrate create schedule with gds
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="collectAmmendFlightGDSEvents"
 */
public class CollectAmmendFlightGDSEvents extends DefaultBaseCommand {

	/**
	 * constructor of the CollectAmmendFlightGDSEvents command
	 */
	public CollectAmmendFlightGDSEvents() {
	}

	/**
	 * execute method of the ValidateCreateFlights command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {

		DefaultServiceResponse response = new DefaultServiceResponse(true);
		if ("Y".equalsIgnoreCase(CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.GDS_INTEGRATED))) {

			Flight updated = (Flight) this.getParameter(CommandParamNames.FLIGHT);
			Flight existing = (Flight) this.getParameter(CommandParamNames.EXISTING_FLIGHT);
			Flight existingOverlapping = (Flight) this.getParameter(CommandParamNames.EXISTING_OVERLAPPING_FLIGHT);

			GDSFlightEventCollector sGdsEvents = new GDSFlightEventCollector();

			identifyDateChanges(sGdsEvents, updated, existing);
			identifyFlightNumberChanges(sGdsEvents, updated, existing);
			identifyModelChanges(sGdsEvents, updated, existing);
			identifyLegsOrTimeChanges(sGdsEvents, updated, existing);
			identifyFlightClosure(sGdsEvents, updated, existing);
			identifyFlightReopen(sGdsEvents, updated, existing);
			identifyServiceTypeChanges(sGdsEvents, updated, existing);
		
			// setting updated schedule info
			updated.setOriginAptCode(LegUtil.getFirstFlightLeg(updated.getFlightLegs()).getOrigin());
			updated.setDestinationAptCode(LegUtil.getLastFlightLeg(updated.getFlightLegs()).getDestination());
			sGdsEvents.setUpdatedFlight(FlightUtil.getCopyOfFlight(updated));

			// setting existing schedule info
			sGdsEvents.setExistingFlight(FlightUtil.getCopyOfFlight(existing));
			if (existingOverlapping != null)
				sGdsEvents.setExistingOverlapFlight(FlightUtil.getCopyOfFlight(existingOverlapping));

			// constructing response
			response.addResponceParam(CommandParamNames.GDS_PARAM_COLLECTOR, sGdsEvents);
		}
		// return command response
		return response;
	}

	private void identifyDateChanges(GDSFlightEventCollector sGdsEvents, Flight updated, Flight existing) throws ModuleException {

		// if existing and update not in same start date, end date and frequency
		if (!(CalendarUtil.isSameDay(updated.getDepartureDate(), existing.getDepartureDate()))) {

			sGdsEvents.addAction(GDSFlightEventCollector.DATE_CHANGED);
		}
		identifyLocalDateChanges(sGdsEvents, updated, existing);
	}
	
	private void identifyFlightClosure(GDSFlightEventCollector sGdsEvents, Flight updated, Flight existing) {

		// if existing and update not in same start date, end date and frequency
		if (updated.getStatus().equals(FlightStatusEnum.CLOSED.getCode())
				&& existing.getStatus().equals(FlightStatusEnum.ACTIVE.getCode())) {

			sGdsEvents.setCloseFlight(true);
		}
	}

	private void identifyFlightReopen(GDSFlightEventCollector sGdsEvents, Flight updated, Flight existing) {

		// if existing and update not in same start date, end date and frequency
		if (updated.getStatus().equals(FlightStatusEnum.ACTIVE.getCode())
				&& existing.getStatus().equals(FlightStatusEnum.CLOSED.getCode())) {

			sGdsEvents.setReopenFlight(true);
		}
	}

	
	private void identifyFlightNumberChanges(GDSFlightEventCollector sGdsEvents, Flight updated, Flight existing) {

		// if existing and update not in same start date, end date and frequency
		if (!updated.getFlightNumber().equals(existing.getFlightNumber())) {

			sGdsEvents.addAction(GDSFlightEventCollector.FLIGHT_NUMBER_CHANGED);
		}
	}

	private void identifyModelChanges(GDSFlightEventCollector sGdsEvents, Flight updated, Flight existing) {

		if (!updated.getModelNumber().equals(existing.getModelNumber())) {

			sGdsEvents.addAction(GDSFlightEventCollector.MODEL_CHANGED);
		}
	}

	private void identifyLegsOrTimeChanges(GDSFlightEventCollector sGdsEvents, Flight updated, Flight existing) {

		boolean legsAddedDeleted = LegUtil.checkLegsAddedDeleted(existing, updated);

		if (legsAddedDeleted) {

			sGdsEvents.addAction(GDSFlightEventCollector.LEG_ADDED_DELETED);
			sGdsEvents.addAction(GDSFlightEventCollector.REPLACE_EXISTING_INFO);
		} else {

			boolean legTimeChanged = LegUtil.isLegTimeChanged(existing, updated);
			if (legTimeChanged) {
				sGdsEvents.addAction(GDSFlightEventCollector.LEG_TIME_CHANGE);
			}
		}
	}
	
	private void identifyServiceTypeChanges(GDSFlightEventCollector sGdsEvents, Flight updated, Flight existing) {

		if (updated.getOperationTypeId() != existing.getOperationTypeId()) {
			sGdsEvents.addAction(GDSFlightEventCollector.REPLACE_EXISTING_INFO);
		}
	}
	
	private void addLocalTimeDetailsToFlight(Flight flight) throws ModuleException {
		LocalZuluTimeAdder localZuluTimeAdder = new LocalZuluTimeAdder(AirSchedulesUtil.getAirportBD());
		localZuluTimeAdder.addLocalTimeDetailsToFlight(flight);
	}

	private void identifyLocalDateChanges(GDSFlightEventCollector sGdsEvents, Flight updated, Flight existing)
			throws ModuleException {
		addLocalTimeDetailsToFlight(updated);
		addLocalTimeDetailsToFlight(existing);
		if (!(CalendarUtil.isSameDay(updated.getDepartureDateLocal(), existing.getDepartureDateLocal()))) {
			sGdsEvents.addAction(GDSFlightEventCollector.DATE_CHANGED_LOCAL_TIME);
		}
	}
}
