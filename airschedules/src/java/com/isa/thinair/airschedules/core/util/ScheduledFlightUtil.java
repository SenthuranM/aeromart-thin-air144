/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.isa.thinair.airmaster.api.model.AircraftCabinCapacity;
import com.isa.thinair.airmaster.api.model.AircraftModel;
import com.isa.thinair.airschedules.api.dto.AffectedPeriod;
import com.isa.thinair.airschedules.api.model.CodeShareMCFlight;
import com.isa.thinair.airschedules.api.model.CodeShareMCFlightSchedule;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightLeg;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.FlightScheduleLeg;
import com.isa.thinair.airschedules.api.model.FlightScheduleSegment;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.model.SSMSplitFlightSchedule;
import com.isa.thinair.airschedules.api.model.SSMSplitFlightScheduleLeg;
import com.isa.thinair.airschedules.api.utils.LegUtil;
import com.isa.thinair.airschedules.api.utils.SegmentUtil;
import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.FrequencyUtil;

/**
 * utility class for scheduled flight util
 * 
 * @author Lasantha Pambagoda
 */
public class ScheduledFlightUtil {

	/**
	 * private method to create flights for build schedule
	 * 
	 * @param schedule
	 * @param period
	 * @throws ModuleException
	 * @return flight list
	 */
	public static Collection<Flight> getFlightsForSchdule(FlightSchedule schedule, AffectedPeriod period) throws ModuleException {

		// collection of flights
		Collection<Flight> flights = new ArrayList<Flight>();

		// get the fly dates
		Collection<Date> flyDayes = getFlightDatesForSchedule(period);
		Iterator<Date> itFlyDates = flyDayes.iterator();

		while (itFlyDates.hasNext()) {

			Date flyDate = itFlyDates.next();

			// create flight
			Flight flight = ScheduledFlightUtil.populateFlightFromSchedule(schedule, new Flight());

			// set the fields calculated
			flight.setDayNumber(CalendarUtil.getDayNumber(flyDate));
			flight.setDepartureDate(flyDate);

			// create flight legs for flight
			flight.setFlightLegs(LegUtil.createFlightLegs(schedule.getFlightScheduleLegs()));
			// create flight segments for flight
			flight.setFlightSegements(SegmentUtil.createFlightSegments(schedule.getFlightScheduleSegments()));
			//CodeShare
			flight.setCodeShareMCFlights(createCodeShareMCFlights(schedule.getCodeShareMCFlightSchedules()));

			flight.setViaScheduleMessaging(schedule.getViaScheduleMessaging());
			// add the flight to list
			flights.add(flight);
		}

		return flights;
	}

	/**
	 * gets the flight dates for schedule
	 * 
	 * @param schedule
	 * @param period
	 * @throws ModuleException
	 */
	public static Collection<Date> getFlightDatesForSchedule(AffectedPeriod period) throws ModuleException {

		Date startDate = period.getStartDate();
		Date endDate = period.getEndDate();
		Frequency frequency = period.getFrequency();

		// fly date collection
		Collection<Date> flyDates = new ArrayList<Date>();

		// get the first day has flight
		Date flyDate = ScheduledFlightUtil.getNextDateHasFlight(startDate, frequency, true);

		while (flyDate.getTime() <= endDate.getTime()) {

			flyDates.add(flyDate);

			// get the next day has flight
			flyDate = getNextDateHasFlight(flyDate, frequency, false);
		}

		return flyDates;
	}

	public static FlightSchedule copyOnlyScheduleAttributes(FlightSchedule from, FlightSchedule to, boolean isWithGdsInfo) {

		// fields which can set directly
		to.setArrivalStnCode(from.getArrivalStnCode());
		to.setBuildStatusCode(from.getBuildStatusCode());
		to.setDepartureStnCode(from.getDepartureStnCode());
		to.setFlightNumber(from.getFlightNumber());
		to.setModelNumber(from.getModelNumber());
		to.setNumberOfDepartures(from.getNumberOfDepartures());
		to.setOperationTypeId(from.getOperationTypeId());
		to.setStartDate(new Date(from.getStartDate().getTime()));
		to.setStopDate(new Date(from.getStopDate().getTime()));
		to.setStatusCode(from.getStatusCode());
		to.setFlightType(from.getFlightType());
		to.setRemarks(from.getRemarks());
		to.setMealTemplateId(from.getMealTemplateId());
		to.setSeatChargeTemplateId(from.getSeatChargeTemplateId());
		to.setCsOCCarrierCode(from.getCsOCCarrierCode());
		to.setCsOCFlightNumber(from.getCsOCFlightNumber());
		
		// setting the code share flight NOs
		if (from.getCodeShareMCFlightSchedules() != null) {
			Set<CodeShareMCFlightSchedule> csFlightNoSet = new HashSet<CodeShareMCFlightSchedule>();
			Iterator<CodeShareMCFlightSchedule> csFlightNosItr = from.getCodeShareMCFlightSchedules().iterator();
					while (csFlightNosItr.hasNext()) {
				CodeShareMCFlightSchedule csFlightNo = csFlightNosItr.next();
						csFlightNoSet.add(csFlightNo);
					}
			to.setCodeShareMCFlightSchedules(csFlightNoSet);
		}

		// setting the GDS Ids list
		if (isWithGdsInfo) {
			if (from.getGdsIds() != null) {
				Set<Integer> hashSet = new HashSet<Integer>();
				Iterator<Integer> itGds = from.getGdsIds().iterator();
				while (itGds.hasNext()) {
					Integer gdsId = itGds.next();
					hashSet.add(new Integer(gdsId));
				}
				to.setGdsIds(hashSet);
			}
		}

		// fields setting with utils returning clone
		to.setFrequency(FrequencyUtil.getCopyOfFrequency(from.getFrequency()));
		to.setFrequencyLocal(FrequencyUtil.getCopyOfFrequency(from.getFrequencyLocal()));
		to.setStartDate(CalendarUtil.getCopyofDate(from.getStartDate()));
		to.setStopDate(CalendarUtil.getCopyofDate(from.getStopDate()));
		to.setModifiedDate(CalendarUtil.getCopyofDate(from.getModifiedDate()));

		// fields manually cloning

		if (from.getStartDateLocal() != null)
			to.setStartDateLocal(new Date(from.getStartDateLocal().getTime()));
		if (from.getStopDateLocal() != null)
			to.setStopDateLocal(new Date(from.getStopDateLocal().getTime()));

		if (from.getNumberOfDepartures() != null)
			to.setNumberOfDepartures(new Integer(from.getNumberOfDepartures()));
		if (from.getAvailableSeatKilometers() != null)
			to.setAvailableSeatKilometers(new Integer(from.getAvailableSeatKilometers().intValue()));

		return to;
	}

	/**
	 * private method to get copy of given flight schecdule. this method will give exact copy of the given flight
	 * schedule by removing version infomation and overlap information
	 * 
	 * @param schedule
	 * @return
	 */
	public static FlightSchedule getCopyOfSchedule(FlightSchedule schedule) {

		// copy all the fields
		FlightSchedule copy = copyOnlyScheduleAttributes(schedule, new FlightSchedule(), true);

		// setting flight schedule legs
		Set<FlightScheduleLeg> scheduleLegs = schedule.getFlightScheduleLegs();
		Iterator<FlightScheduleLeg> legsIt = scheduleLegs.iterator();

		Set<FlightScheduleLeg> copedLegSet = new HashSet<FlightScheduleLeg>();
		FlightScheduleLeg leg;
		FlightScheduleLeg legCopy;

		while (legsIt.hasNext()) {

			leg = legsIt.next();
			legCopy = new FlightScheduleLeg();
			legCopy = LegUtil.copyLeg(leg, legCopy);
			copedLegSet.add(legCopy);
		}

		copy.setFlightScheduleLegs(copedLegSet);

		// setting flight schedule segments
		Set<FlightScheduleSegment> scheduleSegments = schedule.getFlightScheduleSegments();
		Iterator<FlightScheduleSegment> segmentIt = scheduleSegments.iterator();

		Set<FlightScheduleSegment> copedSegmentSet = new HashSet<FlightScheduleSegment>();
		FlightScheduleSegment segment;
		FlightScheduleSegment segmentCopy;

		while (segmentIt.hasNext()) {

			segment = segmentIt.next();
			segmentCopy = new FlightScheduleSegment();
			segmentCopy = SegmentUtil.copySegment(segment, segmentCopy);
			copedSegmentSet.add(segmentCopy);
		}

		copy.setFlightScheduleSegments(copedSegmentSet);

		copy.setFlightType(schedule.getFlightType());
		copy.setRemarks(schedule.getRemarks());
		copy.setMealTemplateId(schedule.getMealTemplateId());
		copy.setSeatChargeTemplateId(schedule.getSeatChargeTemplateId());	
		copy.setCsOCCarrierCode(schedule.getCsOCCarrierCode());
		copy.setCsOCFlightNumber(schedule.getCsOCFlightNumber());
		
		copyCodeShareMCSchedule(schedule, copy);		

		return copy;
	}
	
	public static void copyCodeShareMCSchedule(FlightSchedule schedule, FlightSchedule copy) {
		if (schedule.getCodeShareMCFlightSchedules() != null && !schedule.getCodeShareMCFlightSchedules().isEmpty()) {
			Set<CodeShareMCFlightSchedule> copedExternalFltNo = new HashSet<CodeShareMCFlightSchedule>();
			Set<CodeShareMCFlightSchedule> scheduleCSFlights = schedule.getCodeShareMCFlightSchedules();
			Iterator<CodeShareMCFlightSchedule> csflightIt = scheduleCSFlights.iterator();
			CodeShareMCFlightSchedule csFlight;
			CodeShareMCFlightSchedule csFlightCopy;

			while (csflightIt.hasNext()) {
				csFlight = csflightIt.next();
				csFlightCopy = new CodeShareMCFlightSchedule();
				csFlightCopy = ScheduledFlightUtil.getCopyOfCSFlights(csFlight, csFlightCopy);
				copedExternalFltNo.add(csFlightCopy);
			}
			copy.setCodeShareMCFlightSchedules(copedExternalFltNo);
		} else {
			copy.setCodeShareMCFlightSchedules(null);
		}
	}
	
	public static CodeShareMCFlightSchedule getCopyOfCSFlights(CodeShareMCFlightSchedule from, CodeShareMCFlightSchedule to) {
		to.setCsMCCarrierCode(from.getCsMCCarrierCode());
		to.setCsMCFlightNumber(from.getCsMCFlightNumber());
		to.setScheduleId(from.getScheduleId());
		return to;
	}

	/**
	 * method to populate flight from flight schedule
	 * 
	 * @param schedule
	 * @return flight
	 */
	public static Flight populateFlightFromSchedule(FlightSchedule schedule, Flight flight) {

		// populate from the fields directly available at schedule
		flight.setDestinationAptCode(schedule.getArrivalStnCode());
		flight.setFlightNumber(schedule.getFlightNumber());
		flight.setModelNumber(schedule.getModelNumber());
		flight.setOperationTypeId(schedule.getOperationTypeId());
		flight.setOriginAptCode(schedule.getDepartureStnCode());
		flight.setScheduleId(schedule.getScheduleId());
		flight.setFlightType(schedule.getFlightType());
		HashSet<Integer> gdsSet = new HashSet<Integer>();
		if (schedule.getGdsIds() != null) {
			gdsSet.addAll(schedule.getGdsIds());
		}
		flight.setGdsIds(gdsSet);
		flight.setRemarks(schedule.getRemarks());
		flight.setMealTemplateId(schedule.getMealTemplateId());
		flight.setSeatChargeTemplateId(schedule.getSeatChargeTemplateId());
		flight.setCsOCCarrierCode(schedule.getCsOCCarrierCode());
		flight.setCsOCFlightNumber(schedule.getCsOCFlightNumber());
		
		if (schedule.getCodeShareMCFlightSchedules() != null && !schedule.getCodeShareMCFlightSchedules().isEmpty()) {
			Set<CodeShareMCFlight> copedExternalFlts = new HashSet<CodeShareMCFlight>();
			for (CodeShareMCFlightSchedule codeShareMCFlightSchedule: schedule.getCodeShareMCFlightSchedules()) {
				CodeShareMCFlight copedExternalFlt = new CodeShareMCFlight();
				copedExternalFlt.setCsMCCarrierCode(codeShareMCFlightSchedule.getCsMCCarrierCode());
				copedExternalFlt.setCsMCFlightNumber(codeShareMCFlightSchedule.getCsMCFlightNumber());
				copedExternalFlts.add(copedExternalFlt);
			}
		}

		return flight;
	}

	/**
	 * private method to get copy of given flight schecdule. this method will give exact copy of the given flight
	 * schedule by removing version infomation and overlap information
	 * 
	 * @param schedule
	 * @return flight
	 */
	public static Flight getCopyOfFlight(Flight flight) {

		// copy all the fields
		Flight copy = copyOnlyFlightAttributes(flight, new Flight());

		// setting flight schedule legs
		Set<FlightLeg> flightLegs = flight.getFlightLegs();
		Iterator<FlightLeg> legsIt = flightLegs.iterator();

		Set<FlightLeg> copedLegSet = new HashSet<FlightLeg>();
		FlightLeg leg;
		FlightLeg legCopy;

		while (legsIt.hasNext()) {

			leg = legsIt.next();
			legCopy = new FlightLeg();
			legCopy = LegUtil.copyLeg(leg, legCopy);
			copedLegSet.add(legCopy);
		}

		copy.setFlightLegs(copedLegSet);

		// setting flight schedule segments
		Set<FlightSegement> flightSegments = flight.getFlightSegements();
		Iterator<FlightSegement> segmentIt = flightSegments.iterator();

		Set<FlightSegement> copedSegmentSet = new HashSet<FlightSegement>();
		FlightSegement segment;
		FlightSegement segmentCopy;

		while (segmentIt.hasNext()) {

			segment = segmentIt.next();
			segmentCopy = new FlightSegement();
			segmentCopy = SegmentUtil.copySegment(segment, segmentCopy);
			copedSegmentSet.add(segmentCopy);
		}

		copy.setFlightSegements(copedSegmentSet);
		
		
		if (flight.getCodeShareMCFlights() != null && !flight.getCodeShareMCFlights().isEmpty()) {
			Set<CodeShareMCFlight> copedExternalFlts = new HashSet<CodeShareMCFlight>();
			for (CodeShareMCFlight copedExternalFlt : flight.getCodeShareMCFlights()) {
				CodeShareMCFlight newExternalFlt = new CodeShareMCFlight();
				newExternalFlt.setFlightId(copedExternalFlt.getFlightId());
				newExternalFlt.setCsMCCarrierCode(copedExternalFlt.getCsMCCarrierCode());
				newExternalFlt.setCsMCFlightNumber(copedExternalFlt.getCsMCFlightNumber());
				copedExternalFlts.add(newExternalFlt);
			}
			copy.setCodeShareMCFlights(copedExternalFlts);
		}	
		
		return copy;
	}

	/**
	 * method to copy only flight attributes
	 * 
	 * @param from
	 * @param to
	 * @return flight
	 */
	public static Flight copyOnlyFlightAttributes(Flight from, Flight to) {

		to.setFlightId(from.getFlightId());
		to.setAvailableSeatKilometers(from.getAvailableSeatKilometers());
		to.setDestinationAptCode(from.getDestinationAptCode());
		to.setFlightNumber(from.getFlightNumber());
		to.setModelNumber(from.getModelNumber());
		to.setOperationTypeId(from.getOperationTypeId());
		to.setOriginAptCode(from.getOriginAptCode());
		to.setTailNumber(from.getTailNumber());
		to.setFlightType(from.getFlightType());
		to.setRemarks(from.getRemarks());
		to.setMealTemplateId(from.getMealTemplateId());
		to.setSeatChargeTemplateId(from.getSeatChargeTemplateId());
		to.setCsOCCarrierCode(from.getCsOCCarrierCode());
		to.setCsOCFlightNumber(from.getCsOCFlightNumber());
		to.setDepartureDate(from.getDepartureDate());
	
		// setting the code share flight NOs
		if (from.getCodeShareMCFlights() != null) {
			Set<CodeShareMCFlight> csFlightNoSet = new HashSet<CodeShareMCFlight>();
			Iterator<CodeShareMCFlight> csFlightNosItr = from.getCodeShareMCFlights().iterator();
			while (csFlightNosItr.hasNext()) {
				CodeShareMCFlight csFlightNo = csFlightNosItr.next();
				csFlightNoSet.add(csFlightNo);
			}
			to.setCodeShareMCFlights(csFlightNoSet);
		}
		
		return to;
	}

	/**
	 * method to get the next date has Flight
	 * 
	 * @param date
	 * @param frequency
	 * @param isFirstTime
	 * @return
	 * @throws ModuleException
	 */
	public static Date getNextDateHasFlight(Date date, Frequency frequency, boolean isFirstTime) throws ModuleException {

		if (!frequency.getDay0() && !frequency.getDay1() && !frequency.getDay2() && !frequency.getDay3() && !frequency.getDay4()
				&& !frequency.getDay5() && !frequency.getDay6()) {
			// throw exception
			throw new ModuleException("airschedules.logic.bl.schedule.invalidfrequncy");
		}

		GregorianCalendar calander = new GregorianCalendar();

		calander.setTime(date);
		if (!isFirstTime)
			calander.add(GregorianCalendar.DATE, 1);
		int convertedDay = CalendarUtil.getDayNumber(calander.getTime());

		if ((convertedDay == 0 && frequency.getDay0()) || (convertedDay == 1 && frequency.getDay1())
				|| (convertedDay == 2 && frequency.getDay2()) || (convertedDay == 3 && frequency.getDay3())
				|| (convertedDay == 4 && frequency.getDay4()) || (convertedDay == 5 && frequency.getDay5())
				|| (convertedDay == 6 && frequency.getDay6())) {
			// do nothing
		} else {
			calander.setTime(getNextDateHasFlight(calander.getTime(), frequency, false));
		}

		return calander.getTime();
	}

	/**
	 * method to get flight ids
	 * 
	 * @param flights
	 * @return flight id's list
	 */
	public static List<Integer> getFlightIds(Collection<Flight> flights) {

		if (flights == null || flights.size() == 0)
			return null;

		List<Integer> idList = new ArrayList<Integer>();
		Iterator<Flight> itFlights = flights.iterator();

		while (itFlights.hasNext()) {

			Flight flight = itFlights.next();
			idList.add(flight.getFlightId());
		}

		return idList;
	}

	/**
	 * method to get model numbers
	 * 
	 * @param flights
	 * @return flight id's list
	 */
	public static List<String> getModelNumbers(Collection<Flight> flights) {

		if (flights == null || flights.size() == 0)
			return null;

		List<String> modelList = new ArrayList<String>();
		Iterator<Flight> itFlights = flights.iterator();

		while (itFlights.hasNext()) {

			Flight flight = itFlights.next();
			if (!modelList.contains(flight.getModelNumber())) {
				modelList.add(flight.getModelNumber());
			}
		}

		return modelList;
	}

	/**
	 * Check whether new aircraft model has additional cabinclasses
	 * 
	 * @param updatedAircraftModel
	 * @param existingModel
	 * @return
	 */
	public static boolean isNewCabinClassesAdded(AircraftModel updatedAircraftModel, AircraftModel existingModel) {
		Iterator<AircraftCabinCapacity> updatedCCIt = updatedAircraftModel.getAircraftCabinCapacitys().iterator();
		boolean isNew = true;
		while (updatedCCIt.hasNext()) {
			AircraftCabinCapacity cc = updatedCCIt.next();
			Iterator<AircraftCabinCapacity> existingCCIt = existingModel.getAircraftCabinCapacitys().iterator();
			isNew = true;
			while (existingCCIt.hasNext()) {
				if (cc.getCabinClassCode().equals(existingCCIt.next().getCabinClassCode())) {
					isNew = false;
					break;
				}
			}
			if (isNew) {
				return true;
			}
		}
		return false;
	}

	/**
	 * public method to combine two FlightSchedule Collection without duplicating same Schedule assumes that each
	 * collection is free of duplicates
	 * 
	 * @param s1
	 *            - collection of flight schedules
	 * @param s2
	 *            - another collection of flight schedules
	 * @return - colelction of combined flight schedules
	 */
	public static Collection<FlightSchedule>
			appendWithoutDuplicates(Collection<FlightSchedule> c1, Collection<FlightSchedule> c2) {

		Collection<FlightSchedule> all = new ArrayList<FlightSchedule>();
		Collection<FlightSchedule> combined = new ArrayList<FlightSchedule>();

		all.addAll(c1);
		all.addAll(c2);

		// iterate through remamining set
		Iterator<FlightSchedule> itAll = all.iterator();
		while (itAll.hasNext()) {
			boolean duplicate = false;
			FlightSchedule allSchedule = itAll.next();
			// check in the combined
			Iterator<FlightSchedule> itCombined = combined.iterator();
			while (itCombined.hasNext()) {
				FlightSchedule combSchedule = itCombined.next();
				if (combSchedule.getScheduleId().intValue() == allSchedule.getScheduleId().intValue()) {
					duplicate = true;
					break;
				}
			}
			if (!duplicate)
				combined.add(allSchedule);
		}

		return combined;
	}

	/**
	 * Returns the base capacity of aircraft models
	 * 
	 * @param aircraftModel
	 * @return
	 */
	public static HashMap<String, Integer> getAircraftBaseCapacity(Collection<AircraftModel> aircraftModels) {
		HashMap<String, Integer> modelCapacity = new HashMap<String, Integer>();
		Iterator<AircraftCabinCapacity> aircraftModelCCIt;
		AircraftModel aircraftModel;
		AircraftCabinCapacity aircraftCC;
		int baseCapacity = 0;
		Iterator<AircraftModel> aircraftModelIt = aircraftModels.iterator();
		while (aircraftModelIt.hasNext()) {
			baseCapacity = 0;
			aircraftModel = aircraftModelIt.next();
			aircraftModelCCIt = aircraftModel.getAircraftCabinCapacitys().iterator();
			while (aircraftModelCCIt.hasNext()) {
				aircraftCC = aircraftModelCCIt.next();
				baseCapacity += aircraftCC.getSeatCapacity();
			}
			modelCapacity.put(aircraftModel.getModelNumber(), new Integer(baseCapacity));
		}
		return modelCapacity;
	}

	public static Collection<Integer> getFlightSegmentIDs(Collection<Flight> flights) {
		ArrayList<Integer> flightSegIDs = new ArrayList<Integer>();
		if (flights != null && !flights.isEmpty()) {
			for (Flight flight : flights) {
				if (flight.getFlightSegements() != null && !flight.getFlightSegements().isEmpty()) {
					for (FlightSegement flightSegment : flight.getFlightSegements()) {
						flightSegIDs.add(flightSegment.getFltSegId());
					}
				}
			}
		}
		return flightSegIDs;
	}
	/**
	 * Create CodeShare Carrier & Flight set in Flight from Schedule
	 * 
	 * @param Set
	 *            the CodeShare Set
	 * @return HashSet the codeShareMCFlights
	 */
	public static HashSet<CodeShareMCFlight> createCodeShareMCFlights(Set<CodeShareMCFlightSchedule> codeShareSet) {
	
		HashSet<CodeShareMCFlight> CodeShareMCFlight = new HashSet<CodeShareMCFlight>();
		if(codeShareSet!=null && !codeShareSet.isEmpty()){
		for (CodeShareMCFlightSchedule codeShareSch : codeShareSet) {
			CodeShareMCFlight CodeShareFly = new CodeShareMCFlight();
			CodeShareFly.setCsMCFlightNumber(codeShareSch.getCsMCFlightNumber());
			CodeShareFly.setCsMCCarrierCode(codeShareSch.getCsMCCarrierCode());
			CodeShareMCFlight.add(CodeShareFly);
		}
		}

		return CodeShareMCFlight;
	
	}

	public static Set<SSMSplitFlightSchedule> getCopyOfSSMSplitFlightSchedules(Set<SSMSplitFlightSchedule> subSchedule) {
		Set<SSMSplitFlightSchedule> ssmSubSchedulesCopied = new HashSet<SSMSplitFlightSchedule>();

		if (subSchedule != null && !subSchedule.isEmpty()) {
			Iterator<SSMSplitFlightSchedule> ssmSubSchedulesItr = subSchedule.iterator();
			while (ssmSubSchedulesItr.hasNext()) {
				SSMSplitFlightSchedule ssmSubSchedule = ssmSubSchedulesItr.next();
				SSMSplitFlightSchedule ssmSubScheduleCopy = copySubSchedule(ssmSubSchedule);
				ssmSubSchedulesCopied.add(ssmSubScheduleCopy);
			}
		}

		return ssmSubSchedulesCopied;
	}

	public static SSMSplitFlightSchedule copySubSchedule(SSMSplitFlightSchedule from) {
		SSMSplitFlightSchedule subSchedule = new SSMSplitFlightSchedule();
		subSchedule.setScheduleId(from.getScheduleId());
		subSchedule.setStartDate(from.getStartDate());
		subSchedule.setStopDate(from.getStopDate());
		subSchedule.setStartDateLocal(from.getStartDateLocal());
		subSchedule.setStopDateLocal(from.getStopDateLocal());
		
		Set<SSMSplitFlightScheduleLeg> legs = from.getSsmSplitFlightScheduleLeg();
		if(legs!=null && !legs.isEmpty()){
		Iterator<SSMSplitFlightScheduleLeg> legsItr =legs.iterator();
		while(legsItr.hasNext()){
			SSMSplitFlightScheduleLeg leg =	legsItr.next();			
			SSMSplitFlightScheduleLeg to = new SSMSplitFlightScheduleLeg();
			to.setModelRouteId(leg.getModelRouteId());

			to.setEstArrivalDayOffset(leg.getEstArrivalDayOffset());
			Date tmp = leg.getEstArrivalTimeLocal();
			to.setEstArrivalTimeLocal(tmp != null ? new Date(tmp.getTime()) : null);
			to.setEstArrivalTimeZulu(new Date(leg.getEstArrivalTimeZulu().getTime()));
			to.setEstDepartureDayOffset(leg.getEstDepartureDayOffset());
			tmp = leg.getEstDepartureTimeLocal();
			to.setEstDepartureTimeLocal(tmp != null ? new Date(tmp.getTime()) : null);
			to.setEstDepartureTimeZulu(new Date(leg.getEstDepartureTimeZulu().getTime()));
			to.setEstArrivalDayOffsetLocal(leg.getEstArrivalDayOffsetLocal());
			to.setEstDepartureDayOffsetLocal(leg.getEstDepartureDayOffsetLocal());

			to.setDestination(leg.getDestination());
			to.setDuration(leg.getDuration());
			to.setLegNumber(leg.getLegNumber());
			to.setOrigin(leg.getOrigin());
			
			
		}
		}
		subSchedule.setSsmSplitFlightScheduleLeg(from.getSsmSplitFlightScheduleLeg());
		return subSchedule;
	}
}
