/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airschedules.core.bl.gds;

import java.util.Set;

import com.isa.thinair.airinventory.api.dto.NotifyFlightEventRQ;
import com.isa.thinair.airinventory.api.dto.NotifyFlightEventsRQ;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.utils.AirSchedulesUtil;
import com.isa.thinair.airschedules.api.utils.FlightEventCode;
import com.isa.thinair.airschedules.api.utils.GDSFlightEventCollector;
import com.isa.thinair.airschedules.api.utils.GDSSchedConstants;
import com.isa.thinair.airschedules.core.config.AirScheduleModuleConfig;
import com.isa.thinair.airschedules.core.util.CommandParamNames;
import com.isa.thinair.airschedules.core.util.ScheduledFlightUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.framework.DefaultBaseCommand;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.gdsservices.api.dto.publishing.Envelope;
import com.isa.thinair.gdsservices.api.service.GDSPublishingBD;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * command for integrate create schedule with gds
 * 
 * @author Lasantha Pambagoda
 * @isa.module.command name="integrateAmmendFlight"
 */
public class IntegrateAmmendFlight extends DefaultBaseCommand {

	// BDs
	private GDSPublishingBD gdsPublishingBD;

	/**
	 * constructor of the IntegrateAmmendFlight command
	 */
	public IntegrateAmmendFlight() {

		// looking up BDs
		gdsPublishingBD = AirSchedulesUtil.getGDSPublishingBD();
	}

	/**
	 * execute method of the IntegrateAmmendFlight command
	 * 
	 * @throws ModuleException
	 */
	public ServiceResponce execute() throws ModuleException {

		DefaultServiceResponse lastResponse = (DefaultServiceResponse) this.getParameter(CommandParamNames.LAST_RESPONSE);
		if ("Y".equalsIgnoreCase(CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.GDS_INTEGRATED))) {

			GDSFlightEventCollector sGdsEvents = (GDSFlightEventCollector) this
					.getParameter(CommandParamNames.GDS_PARAM_COLLECTOR);
			Flight overlapingUpdatedFlight = (Flight) this.getParameter(CommandParamNames.OVERLAPPING_FLIGHT);

//			if (sGdsEvents.getUpdatedFlight().getScheduleId() == null) {

			if (overlapingUpdatedFlight != null)
				sGdsEvents.setUpdatedOverlapFlight(ScheduledFlightUtil.getCopyOfFlight(overlapingUpdatedFlight));

			String serviceType = ((AirScheduleModuleConfig) AirSchedulesUtil.getInstance().getModuleConfig())
					.getServiceTypeForAdHokFlight();

			sGdsEvents.setServiceType(serviceType);

			Envelope env = new Envelope(GDSSchedConstants.TargetLogicNames.PUBLISH_AMMEND_FLIGHT);
			env.addParam(GDSSchedConstants.ParamNames.GDS_EVENT_COLLECTOR, sGdsEvents);

			gdsPublishingBD.publishMessage(env);
//			}
			
			if (sGdsEvents.isCloseFlight()||sGdsEvents.isReopenFlight() || sGdsEvents.containsAction(GDSFlightEventCollector.MODEL_CHANGED)) {
				NotifyFlightEventsRQ notifyFlightEventsRQ = new NotifyFlightEventsRQ();
				NotifyFlightEventRQ newFlightEvent = new NotifyFlightEventRQ();
				if (sGdsEvents.isCloseFlight()) {
					newFlightEvent
							.setFlightEventCode(FlightEventCode.FLIGHT_CLOSED);
				}

				else if (sGdsEvents.isReopenFlight()) {
					newFlightEvent
							.setFlightEventCode(FlightEventCode.FLIGHT_REOPENED);
				}
				Set<Integer> gDSIds = sGdsEvents.getExistingFlight()
						.getGdsIds();
				
				if (!sGdsEvents.isCloseFlight() && !sGdsEvents.isReopenFlight()
						&& sGdsEvents.containsAction(GDSFlightEventCollector.MODEL_CHANGED)) {
					newFlightEvent.setGdsIdsAdded(gDSIds);
					newFlightEvent.setFlightEventCode(FlightEventCode.FLIGHT_GDS_PUBLISHED_CHANGED);
					newFlightEvent.addFlightId(sGdsEvents.getUpdatedFlight().getFlightId());
				} else {
					newFlightEvent.addFlightId(sGdsEvents.getExistingFlight().getFlightId());
				}
				
				if (sGdsEvents.isCloseFlight()) {
					newFlightEvent.setGdsIdsRemoved(gDSIds);
				} else if (sGdsEvents.isReopenFlight()) {
					newFlightEvent.setGdsIdsAdded(gDSIds);
				}
				notifyFlightEventsRQ.addNotifyFlightEventRQ(newFlightEvent);
				AirSchedulesUtil.getFlightInventoryBD().notifyFlightEvent(notifyFlightEventsRQ);
			}
		}
		// return command responce
		return lastResponse;
	}
}
