/*
 * Created on Jul 6, 2005
 *
 * 
 */
package com.isa.thinair.alerting.core.persistence.hibernate;

import java.util.List;

import com.isa.thinair.alerting.api.model.Alert;
import com.isa.thinair.alerting.core.persistence.dao.AlertDAO;
import com.isa.thinair.commons.core.framework.PlatformHibernateDaoSupport;

/**
 * @author sumudu
 * 
 * 
 */
public class AlertDAOImpl extends PlatformHibernateDaoSupport implements AlertDAO {
	public List getAlerts() {
		return find("from Alert", Alert.class);
	}

	public Alert getAlert(String alertID) {
		return (Alert) get(Alert.class, alertID);
	}

	public void saveAlert(Alert alert) {
		hibernateSaveOrUpdate(alert);
	}

	// needs to changed
	public void removeAlert(String alertTypeID, String flightId) {
		Object alertType = load(Alert.class, alertTypeID);
		delete(alertType);
	}
}
