/*
 * Created on Jul 6, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.isa.thinair.alerting.core.persistence.dao;

import java.util.List;

import com.isa.thinair.alerting.api.model.Alert;

/**
 * @author sumudu
 * 
 * 
 */
public interface AlertDAO {
	public List getAlerts();

	public Alert getAlert(String alertId);

	public void saveAlert(Alert alert);

	public void removeAlert(String alertId, String flightId);
}
