package com.isa.thinair.alerting.core.persistence.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.isa.thinair.alerting.api.dto.RequestQueueUser;
import com.isa.thinair.alerting.api.model.Alert;
import com.isa.thinair.alerting.api.model.AlertTemplate;
import com.isa.thinair.alerting.api.model.ApplicablePage;
import com.isa.thinair.alerting.api.model.FlightNotification;
import com.isa.thinair.alerting.api.model.FlightPnrNotification;
import com.isa.thinair.alerting.api.model.Queue;
import com.isa.thinair.alerting.api.model.QueueRequest;
import com.isa.thinair.alerting.api.model.RequestHistory;
import com.isa.thinair.alerting.api.util.AlertTemplateEnum;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * 
 * @author Sumudu
 * 
 */
public interface AlertingDAO {

	public List retrieveAlerts(Collection alertIds);

	public AlertTemplate getAlertTemplate(AlertTemplateEnum templateID);

	public List retrieveAlertsForPrivileges(List privilegeIds);

	public List retrieveAlertsForPnrSegments(List pnrSegmentIds);

	public Map getAlertsForPnrSegments(Collection pnrSegmentIds);

	public void addAlert(Alert alert);

	public void addAlerts(Collection alert);

	public void updateAlertStatus(Long alertId, String status);

	public void updateAlert(Alert alert);

	public void updateAlerts(Collection<Alert> alerts);

	public void removeAlerts(Collection alertIds);

	public void removeAlertsOfPnrSegments(List pnrSegmentIds);

	public Collection<Integer> getPNRSegIDsOfAlerts(Collection<Integer> colAlertIDs);

	public ArrayList getNotificationsForPnr(int pnrSegId);

	public int saveFlightNotification(FlightNotification obj) throws ModuleException;

	public void saveFlightPnrNotification(FlightPnrNotification obj) throws ModuleException;

	public ArrayList getAllNotificationsForFlight(int flightId, String pnr) throws ModuleException;

	public Integer getFlightID(String fltNum, Date depDateLocal) throws ModuleException;
	
	public List<Integer> getFlightIDList(String fltNum, Date startDateLocal,Date endDateLocal) throws ModuleException;

	public void submitOrEditRequest(QueueRequest queueRequest) throws ModuleException;
	
	public QueueRequest getQueueRequest(int requestId) throws ModuleException;
	
	public void addRequestHistory(RequestHistory requestHistory) throws ModuleException;
	
	public void deleteRequest(int requestId) throws ModuleException;
	
	public ServiceResponce deleteRequestQueue(int queueId) throws ModuleException;
	
	public Page searchRequestQueue(int page, int totalper,String queueName, String applicablePage) throws ModuleException;
	
	public List<RequestQueueUser> searchUser(String userNamePart) throws ModuleException;
	
	public void updateRequestQueue(Queue queue, List<String> pageList) throws ModuleException;
	
	public void saveRequestQueue(Queue queue,List<String> pagelist,List<String> add) throws ModuleException;
	
	public void addRemoveUsers(List<String> add,List<String> remove,int queue) throws ModuleException;
	
	public Queue readRequestQueue(int queue) throws ModuleException;
	
	public List<ApplicablePage> searchPages(String pageNamePart) throws ModuleException;
	
	public Page getRequestStatusForPnr(int page, int maxResult,String pnrId) throws ModuleException;
	
	public Page getHistoryForRequest(int page, int maxResult,int requestId) throws ModuleException;
	
}
