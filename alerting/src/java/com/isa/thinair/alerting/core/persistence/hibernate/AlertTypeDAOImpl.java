/*
 * Created on Jul 6, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.isa.thinair.alerting.core.persistence.hibernate;

import java.util.List;

import com.isa.thinair.alerting.api.model.AlertType;
import com.isa.thinair.alerting.core.persistence.dao.AlertTypeDAO;
import com.isa.thinair.commons.core.framework.PlatformHibernateDaoSupport;

/**
 * @author sumudu
 * 
 * 
 */
public class AlertTypeDAOImpl extends PlatformHibernateDaoSupport implements AlertTypeDAO {

	public List getAlertTypes() {
		return find("from AlertType", AlertType.class);
	}

	public AlertType getAlertType(int alertTypeID) {
		return (AlertType) get(AlertType.class, new Integer(alertTypeID));
	}

	public void saveAlertType(AlertType alertType) {
		hibernateSaveOrUpdate(alertType);
	}

	public void removeAlertType(int alertTypeID) {
		Object alertType = load(AlertType.class, new Integer(alertTypeID));
		delete(alertType);
	}

	public List getAlertTypesOnPermission(int permissionId) {

		return find("from AlertType where role_id= " + permissionId, AlertType.class);

	}

}
