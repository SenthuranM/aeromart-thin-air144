package com.isa.thinair.alerting.core.persistence.hibernate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.sql.DataSource;

import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import oracle.sql.CLOB;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.alerting.api.dto.PnrRequestStatusDTO;
import com.isa.thinair.alerting.api.dto.RequestHistoryDTO;
import com.isa.thinair.alerting.api.dto.RequestQueueUser;
import com.isa.thinair.alerting.api.model.Alert;
import com.isa.thinair.alerting.api.model.AlertTemplate;
import com.isa.thinair.alerting.api.model.ApplicablePage;
import com.isa.thinair.alerting.api.model.FlightNotification;
import com.isa.thinair.alerting.api.model.FlightPnrNotification;
import com.isa.thinair.alerting.api.model.Queue;
import com.isa.thinair.alerting.api.model.QueueRequest;
import com.isa.thinair.alerting.api.model.QueueUserDTO;
import com.isa.thinair.alerting.api.model.RequestHistory;
import com.isa.thinair.alerting.api.model.RequestQueueSearchResult;
import com.isa.thinair.alerting.api.service.AlertingModuleUtils;
import com.isa.thinair.alerting.api.util.AlertTemplateEnum;
import com.isa.thinair.alerting.core.persistence.dao.AlertingDAO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformHibernateDaoSupport;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * 
 * @author Sumudu
 * @isa.module.dao-impl dao-name="AlertingDAO"
 */
public class AlertingDAOImpl extends PlatformHibernateDaoSupport implements AlertingDAO {

	private final Log log = LogFactory.getLog(getClass());

	public List retrieveAlerts(Collection alertIds) {
		String hibernateQuery = "select alert from Alert alert " + " where alert.alertId in ("
				+ Util.buildIntegerInClauseContent(alertIds) + ") ";
		return getSession().createQuery(hibernateQuery).list();
	}

	public AlertTemplate getAlertTemplate(AlertTemplateEnum templateID) {
		return (AlertTemplate) get(AlertTemplate.class, new Integer(templateID.getCode()));
	}

	public List retrieveAlertsForPrivileges(List privilegeIds) {
		String hibernateQuery = "select alert from Alert alert, " + " AlertType alertType "
				+ " where alert.alertTypeId = alertType.alertTypeId " + " and alertType.privilegeId in ("
				+ Util.buildStringInClauseContent(privilegeIds) + ")";
		return getSession().createQuery(hibernateQuery).list();
	}

	public List retrieveAlertsForPnrSegments(List pnrSegmentIds) {
		String hibernateQuery = "select alert from Alert alert " + " where alert.pnrSegId in ( :pnrSegmentIds ) " + " order by alert.pnrSegId";

		Query query = getSession().createQuery(hibernateQuery);
		query.setParameterList("pnrSegmentIds", pnrSegmentIds, StandardBasicTypes.INTEGER);

		return query.list();
	}

	public Map getAlertsForPnrSegments(Collection pnrSegmentIds) {
		Collection colAlerts = this.retrieveAlertsForPnrSegments(new ArrayList(pnrSegmentIds));
		Iterator itColAlerts = colAlerts.iterator();
		Map mapAlerts = new HashMap();
		Alert alert;

		while (itColAlerts.hasNext()) {
			alert = (Alert) itColAlerts.next();

			if (mapAlerts.containsKey(alert.getPnrSegId())) {
				Collection colSegmentAlerts = (Collection) mapAlerts.get(alert.getPnrSegId());
				colSegmentAlerts.add(alert);
			} else {
				Collection colSegmentAlerts = new ArrayList();
				colSegmentAlerts.add(alert);
				mapAlerts.put(alert.getPnrSegId(), colSegmentAlerts);
			}
		}

		return mapAlerts;
	}

	public void addAlert(Alert alert) {
		hibernateSave(alert);
	}

	public void addAlerts(Collection alerts) {
		Iterator iterator = alerts.iterator();
		while (iterator.hasNext()) {
			Alert alert = (Alert) iterator.next();
			hibernateSave(alert);
		}
	}

	public void updateAlertStatus(Long alertId, String status) {
		Alert alert = (Alert) get(Alert.class, alertId);
		alert.setStatus(status);
		update(alert);
	}

	public void updateAlert(Alert alert) {
		update(alert);
	}

	public void updateAlerts(Collection<Alert> alerts) {
		Iterator<Alert> iterator = alerts.iterator();
		while (iterator.hasNext()) {
			Alert alert = iterator.next();
			update(alert);
		}
	}

	public void removeAlerts(Collection alertIds) {
		deleteAll(retrieveAlerts(alertIds));
	}

	public void removeAlertsOfPnrSegments(List pnrSegmentIds) {
		deleteAll(retrieveAlertsForPnrSegments(pnrSegmentIds));
	}

	/**
	 * Get collection of PNR Seg IDs, if only used by given alerts
	 * 
	 * @param colAlertIDs
	 */
	public Collection<Integer> getPNRSegIDsOfAlerts(Collection<Integer> colAlertIDs) {
		// String sql = "SELECT a.pnr_seg_id " +
		// "  FROM t_alert a " +
		// " WHERE a.alert_id IN (" + Util.buildIntegerInClauseContent(colAlertIDs) + ") " +
		// "GROUP BY a.pnr_seg_id " +
		// " HAVING COUNT(a.alert_id) = 1 ";
		//
		// String sql = "SELECT b.pnr_seg_id " +
		// "  FROM t_alert b " +
		// " WHERE b.pnr_seg_id IN ( " +
		// "    SELECT a.pnr_seg_id    " +
		// "    FROM t_alert a   " +
		// "    WHERE a.alert_id IN (" + Util.buildIntegerInClauseContent(colAlertIDs) + ") " +
		// "    ) " +
		// "GROUP BY b.pnr_seg_id " +
		// "HAVING COUNT(b.alert_id) = 1 " ;

		String sql = "  SELECT a.pnr_seg_id FROM t_alert a WHERE a.alert_id IN (" + Util.buildIntegerInClauseContent(colAlertIDs)
				+ ") ";

		JdbcTemplate jt = new JdbcTemplate(AlertingModuleUtils.getDatasource());

		Collection<Integer> pnrSegIDs = (Collection<Integer>) jt.query(sql, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<Integer> pnrSegIDs = new HashSet<Integer>();
				if (rs != null) {
					while (rs.next()) {
						pnrSegIDs.add(new Integer(rs.getInt("pnr_seg_id")));
					}
				}
				return pnrSegIDs;
			}
		});

		return pnrSegIDs;
	}

	@SuppressWarnings("rawtypes")
	public ArrayList getNotificationsForPnr(int pnrSegId) {

		return (ArrayList) find("from FlightPnrNotification fpn Where fpn.pnrSegId = ? ",Long.valueOf(pnrSegId), FlightPnrNotification.class);

	}

	@SuppressWarnings("rawtypes")
	public int saveFlightNotification(FlightNotification obj) throws ModuleException {
		obj.setNotificationCode(obj.getNotificationCode().toUpperCase());
		ArrayList aList = (ArrayList) find("from FlightNotification fn Where fn.notificationCode = ? ",
				obj.getNotificationCode(), FlightPnrNotification.class);
		if (aList != null && aList.size() > 0)
			return (int) ((FlightNotification) aList.get(0)).getFlightNotificationId();
		else {
			hibernateSaveOrUpdate(obj);
			return (int) obj.getFlightNotificationId();
		}
	}

	public void saveFlightPnrNotification(FlightPnrNotification obj) throws ModuleException {
		hibernateSaveOrUpdate(obj);
	}

	@SuppressWarnings("rawtypes")
	public ArrayList getAllNotificationsForFlight(int flightId, String pnr) throws ModuleException {

		if (pnr == null) {
			return (ArrayList) find(
					"from FlightNotification fn Where fn.flightId = ? order by fn.notificationCode", Long.valueOf(flightId), FlightPnrNotification.class);
		} else {
			String sql = "SELECT a.*, b.message "
					+ " FROM (SELECT DISTINCT fn.flight_notification_id, fn.notification_code, fn.lang "
					+ " FROM T_Flight_Notification fn,  " + " T_Flight_Pnr_Notification fpn,  t_pnr_segment pnrseg  "
					+ " WHERE fn.flight_Notification_Id =  fpn.flight_Notification_Id  "
					+ " AND fpn.pnr_Seg_Id = pnrseg.pnr_Seg_Id AND pnrSeg.pnr =  '" + pnr + "' ORDER BY fn.notification_code) a "
					+ " INNER JOIN t_flight_notification b ON a.notification_code=b.notification_code";

			JdbcTemplate jt = new JdbcTemplate(AlertingModuleUtils.getDatasource());

			ArrayList<FlightNotification> lstNotifications = (ArrayList<FlightNotification>) jt.query(sql,
					new ResultSetExtractor() {

						public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
							ArrayList<FlightNotification> aList = new ArrayList<FlightNotification>();
							FlightNotification objFn;
							if (rs != null) {
								while (rs.next()) {
									objFn = new FlightNotification();
									objFn.setFlightNotificationId(rs.getInt("FLIGHT_NOTIFICATION_ID"));
									objFn.setNotificationCode(rs.getString("NOTIFICATION_CODE"));
									Object obj = rs.getObject("MESSAGE");
									if (obj != null && obj instanceof CLOB) {
										objFn.setMessage((CLOB) obj);
									}
									objFn.setLanguage(rs.getString("LANG"));
									aList.add(objFn);
								}
							}
							return aList;
						}
					});
			return lstNotifications;
		}
	}

	public Integer getFlightID(String fltNum, Date depDateLocal) throws ModuleException {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		String dateLocal = dateFormat.format(depDateLocal);

		DataSource ds = AlertingModuleUtils.getDatasource();

		JdbcTemplate templete = new JdbcTemplate(ds);
		Object params[] = { fltNum, dateLocal };

		String sql = " select f.FLIGHT_ID " + " from T_FLIGHT f, T_FLIGHT_SEGMENT fs  where "
				+ " f.flight_id = fs.flight_id and f.flight_number = ? " + " and trunc(fs.est_time_departure_local)= ? ";

		final List list = new ArrayList();

		templete.query(sql, params,

		new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				while (rs.next()) {
					list.add(rs.getInt("FLIGHT_ID"));
				}

				return null;
			}
		});

		Integer flightId = null;

		if (list.size() > 0) {
			flightId = (Integer) list.get(0);
		}

		if (flightId == null) {
			throw new ModuleException("Given Flight Number or Schedule Donot Exists");
		}

		return flightId;
	}
	
    public List<Integer> getFlightIDList(String fltNum, Date startDateLocal,Date endDateLocal) throws ModuleException {

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yy");
        String fDateLocal = dateFormat.format(startDateLocal);
        String tDateLocal = dateFormat.format(endDateLocal);


        DataSource ds = AlertingModuleUtils.getDatasource();

		JdbcTemplate templete = new JdbcTemplate(ds);
		Object params[] = { fltNum, fDateLocal, tDateLocal };

		String sql = " select f.FLIGHT_ID from T_FLIGHT f, T_FLIGHT_SEGMENT fs  where "
				+ " f.flight_id = fs.flight_id and f.flight_number = ? and trunc(fs.est_time_departure_local) between ? and ? ";

		final List<Integer> list = new ArrayList();

		templete.query(sql, params,

		new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				while (rs.next()) {
					list.add(rs.getInt("FLIGHT_ID"));
				}

				return null;
			}
		});


		if (list.isEmpty()) {
			throw new ModuleException("Given Flight Number Dose Not Exists");
		}

		return list;
	}

	public void submitOrEditRequest(QueueRequest queueRequest) throws ModuleException{
		
		hibernateSaveOrUpdate(queueRequest);
		
	}
	
	public QueueRequest getQueueRequest(int requestId) throws ModuleException{
		
		Integer reqID = new Integer(requestId);
		return get(QueueRequest.class, reqID);
		
	}
	
	public void addRequestHistory(RequestHistory requestHistory) throws ModuleException{
		
		hibernateSaveOrUpdate(requestHistory);
		
	}
	
	@SuppressWarnings("unchecked")
	public void deleteRequest(int requestId) throws ModuleException{
		
		Object queueRequest = load(QueueRequest.class, requestId);
		delete(queueRequest);
		
	}
	
	public ServiceResponce deleteRequestQueue(int queueId) throws ModuleException{ 
		DefaultServiceResponse response=new DefaultServiceResponse();
		List queueRequestList=find("from QueueRequest qr Where qr.queueId = ? ",queueId, QueueRequest.class); //////		
		if(queueRequestList.size()==0){
			Queue queue=load(Queue.class, queueId);  // will throw object not found eXCCEPTION IF NOT FOUND
			delete(queue); 
			addRemoveUsers(null, null, queueId);
		}else{
			response.setSuccess(false);
			response.setResponseCode("um.requestqueue.delete.failed");
			log.info("Cannot deleted request queue, there are requests in that request queue");
		}
		return response;
	} 
	
	public Page searchRequestQueue(int start, int maxResult,String queueName, String applicablePage) throws ModuleException{
		Query query = getSession().createQuery("select DISTINCT NEW com.isa.thinair.alerting.api.model.RequestQueueSearchResult(q.queueId,q.description,q.queueName) from Queue q INNER JOIN q.applicablePages a where q.queueName LIKE :p1 and a.pageId LIKE :p2").setFirstResult(start).setMaxResults(maxResult);
		Query queryNatural = getSession().createQuery("select DISTINCT NEW com.isa.thinair.alerting.api.model.RequestQueueSearchResult(q.queueId,q.description,q.queueName) from Queue q INNER JOIN q.applicablePages a where q.queueName LIKE :p1 and a.pageId LIKE :p2");///
		
		query.setParameter("p1", "%"+queueName+"%");
		query.setParameter("p2", "%"+applicablePage+"%");
		
		queryNatural.setParameter("p1", "%"+queueName+"%");
		queryNatural.setParameter("p2", "%"+applicablePage+"%");
		
        List<RequestQueueSearchResult> resultList = query.list();
        List<ApplicablePage> pageList=null;
        List<QueueUserDTO> userList=null;
        for (RequestQueueSearchResult searchResult : resultList) {
        	userList=new ArrayList<QueueUserDTO>();
        	pageList=new ArrayList<ApplicablePage>();
        	List<Object[]> pageDetails =getSession().createSQLQuery("select t_page.page_id as pageId,t_page.page_description as description, t_page.page_name as pageName from t_page, t_queue_page  where t_page.page_id=t_queue_page.page_id and t_queue_page.queue_id="+searchResult.getQueueId()).list();
        	ApplicablePage pageInResult=null;
        	for (Object[] pageDetail : pageDetails) {
        		pageInResult=new ApplicablePage();
        		pageInResult.setPageId(pageDetail[0].toString());
        		pageInResult.setDescription(pageDetail[1].toString());
        		pageInResult.setPageName(pageDetail[2].toString());
        		pageList.add(pageInResult);
        	}
        	
        	searchResult.setApplicablePages(pageList);
            
        	List<Object[]> users = getSession().createSQLQuery("select t_user.display_name, t_user.user_id from t_user , t_queue_user where t_user.user_id = t_queue_user.user_id and t_queue_user.queue_id="+searchResult.getQueueId()).list();
        	QueueUserDTO queueUserDto=null;
        	for (Object[] y : users) {
        		queueUserDto=new QueueUserDTO();
        		queueUserDto.setDisplayName(y[0].toString());
        		queueUserDto.setUserId(y[1].toString());
        		userList.add(queueUserDto);
        	}
        	
        	searchResult.setQueueUsers(userList);          
        }
        
        int count=queryNatural.list().size();
        return new Page<RequestQueueSearchResult>(count, start, start + resultList.size(),count,resultList);
        
	}
	
	public List<RequestQueueUser> searchUser(String userNamePart) throws ModuleException{
		Query query = getSession().createSQLQuery("select t_user.display_name, t_user.user_id from t_user where t_user.display_name LIKE :p3");
		query.setParameter("p3", "%"+userNamePart+"%");
		List<Object[]> userList= query.list();
		List<RequestQueueUser> users=new ArrayList<RequestQueueUser>();
		RequestQueueUser queueUser=null;
		for (Object[] user : userList) {
			queueUser=new RequestQueueUser();
			queueUser.setDisplayName(user[0].toString());
			queueUser.setUserId(user[1].toString());	
			users.add(queueUser);
		}
		return users;
	}  
	
	public List<ApplicablePage> searchPages(String pageNamePart) throws ModuleException{
		Query query = getSession().createSQLQuery("select page_id,page_name from t_page where page_name LIKE :p1");
		query.setParameter("p1", "%"+pageNamePart+"%");
		List<Object[]> pageList= query.list();
		List<ApplicablePage> pages=new ArrayList<ApplicablePage>();
		ApplicablePage applicablePage=null;
		for (Object[] page: pageList) {
			applicablePage=new ApplicablePage();
			applicablePage.setPageId(page[0].toString());
			applicablePage.setPageName(page[1].toString());

			pages.add(applicablePage);
		}
		return pages;
	}
	
	
	public void saveRequestQueue(Queue queue,List<String> pagelist,List<String> add) throws ModuleException{
		Set<ApplicablePage> pageList=new HashSet<ApplicablePage>();
		for(String applicablePageID : pagelist){
			ApplicablePage t_page = load(ApplicablePage.class,applicablePageID);
			pageList.add(t_page);
		}
		queue.setApplicablePages(pageList);	
		hibernateSave(queue);	
		flush();
		
		if(add!=null){
			for(String applicablePageID : add){
				Query query = getSession().createSQLQuery("INSERT INTO t_queue_user (queue_user_id, queue_id,user_id) VALUES ( S_QUEUE_USER.NEXTVAL, :p1 , :p2)");
				query.setParameter("p1", queue.getQueueId());
				query.setParameter("p2", applicablePageID);
				query.executeUpdate();
				
			}
		}		
		
	}

	@Override
	public void updateRequestQueue(Queue queue, List<String> pagelist) throws ModuleException {
		Set<ApplicablePage> pageList=new HashSet<ApplicablePage>();
		for(String applicablePageID : pagelist){
			ApplicablePage t_page = load(ApplicablePage.class,applicablePageID);
			pageList.add(t_page);
		}
		queue.setApplicablePages(pageList);
		update(queue);		
	}
	
	@Override
	public void addRemoveUsers(List<String> add,List<String> remove,int queue) throws ModuleException {
		if(add!=null){
			for(String applicablePageID : add){
				Query query = getSession().createSQLQuery("INSERT INTO t_queue_user (queue_user_id, queue_id,user_id) VALUES ( S_QUEUE_USER.NEXTVAL, :p1 , :p2)");
				query.setParameter("p1", queue);
				query.setParameter("p2", applicablePageID);
				query.executeUpdate();
				
			}
		}
		if(remove!=null){
			for(String applicablePageID : remove){
				Query query = getSession().createSQLQuery("delete from t_queue_user where queue_id = :p1 and user_id = :p2");
				query.setParameter("p1", queue);
				query.setParameter("p2", applicablePageID);
				query.executeUpdate();
			}
		}
		if(add==null && remove==null){
			Query query = getSession().createSQLQuery("delete from t_queue_user where queue_id in ( :p1)");
			query.setParameter("p1", queue);
			query.executeUpdate();
			
		}
	}
	
	@Override
	public Queue readRequestQueue(int queueId) throws ModuleException {
		return get(Queue.class, queueId);
	}
	
	@Override
	public Page getRequestStatusForPnr(int page, int maxResult,String pnrId) throws ModuleException {
		Query query = getSession().createQuery("select DISTINCT NEW com.isa.thinair.alerting.api.dto.PnrRequestStatusDTO(qr.requestId ,qr.queueId, qr.description, qr.status, qr.assignedTo, qr.assignedOn) from QueueRequest qr where qr.pnr = :pnr ").setFirstResult(page).setMaxResults(maxResult);
		Query naturalQuery = getSession().createQuery("select DISTINCT NEW com.isa.thinair.alerting.api.dto.PnrRequestStatusDTO(qr.requestId ,qr.queueId, qr.description, qr.status, qr.assignedTo, qr.assignedOn) from QueueRequest qr where qr.pnr = :pnr ");
		query.setParameter("pnr", pnrId);
		naturalQuery.setParameter("pnr", pnrId);
		List<PnrRequestStatusDTO> resultList=query.list();		
		int allRecordsInSystem=naturalQuery.list().size();
		return new Page<PnrRequestStatusDTO>(allRecordsInSystem, page, page + resultList.size(),allRecordsInSystem,resultList);		
	}
	
	@Override
	public Page getHistoryForRequest(int page, int maxResult,int requestId) throws ModuleException {
		String hql = "from RequestHistory where requestId = :requestId";
		
		Query query = getSession().createQuery(hql);
		Query pagedQuery = getSession().createQuery(hql).setFirstResult(page).setMaxResults(maxResult);
		
		query.setParameter("requestId", requestId);	
		pagedQuery.setParameter("requestId", requestId);
		
		List<RequestHistory> historyList = pagedQuery.list();
		int allRecordsInSystem=query.list().size();

		return new Page<RequestHistory>(allRecordsInSystem, page, page + historyList.size(),allRecordsInSystem,historyList);		
	}
}
