package com.isa.thinair.alerting.core.config;

import com.isa.thinair.platform.api.DefaultModuleConfig;

/**
 * @isa.module.config-bean
 */
public class AlertingConfig extends DefaultModuleConfig {

	public AlertingConfig() {
		super();
	}
}