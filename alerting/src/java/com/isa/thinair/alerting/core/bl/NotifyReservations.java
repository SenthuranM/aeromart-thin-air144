package com.isa.thinair.alerting.core.bl;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import com.isa.thinair.airreservation.api.utils.BeanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import com.isa.thinair.airreservation.api.dto.CredentialsDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airschedules.api.dto.FlightAlertInfoDTO;
import com.isa.thinair.alerting.api.dto.FlightNotificationDTO;
import com.isa.thinair.alerting.api.dto.FlightPnrNotificationDTO;
import com.isa.thinair.alerting.api.model.AlertTemplate;
import com.isa.thinair.alerting.api.model.FlightNotification;
import com.isa.thinair.alerting.api.model.FlightPnrNotification;
import com.isa.thinair.alerting.api.service.AlertingModuleUtils;
import com.isa.thinair.alerting.api.util.AlertTemplateEnum;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.dto.CommonTemplatingDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.AccelAeroClients;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.DataConverterUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.StaticFileNameUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.commons.core.util.TemplateEngine;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.messaging.api.model.Message;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.messaging.api.service.MessagingModuleUtils;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;
import com.isa.thinair.messaging.core.config.MessagingModuleConfig;
import com.isa.thinair.messaging.core.config.TopicConfig;

/**
 * 
 * @author Dhanya
 * @since 1.0
 */
public class NotifyReservations {

	/** Holds the logger instance */
	private static Log log = LogFactory.getLog(NotifyReservations.class);

	/** Holds the global configurations */
	private static GlobalConfig globalConfig = AlertingModuleUtils.getGlobalConfig();

	/** Customer SMS Notification Change Flight */
	public static final String CUSTOMER_SMS_NOTIFICATION_CHANGE_FLIGHT = "customer_sms_notification_change_flight";
	/** Customer Email Notification Change Flight */
	public static final String CUSTOMER_EMAIL_NOTIFICATION_CHANGE_FLIGHT = "customer_email_notification_change_flight";

	public static final String CUSTOMER_EMAIL_NOTIFICATION_REPROTECT_FLIGHT = "customer_email_notification_reprotect_flight";

	public static final String CUSTOMER_SMS_NOTIFICATION_REPROTECT_FLIGHT = "customer_sms_notification_reprotect_flight";

	private static final String ACTION_CANCEL = "CANCEL";

	private static final String ACTION_UPDATE = "UPDATE";

	private static final String ACTION_REPROTECT = "REPROTECT";

	private static final String SMS_MESSAGE = "SMS";

	private static final String EMAIL_MESSAGE = "EMAIL";

	private static final String PLACE_HOLDER = "$";

	private int flightId;

	private FlightNotificationDTO fltNotifyDto;

	private Collection colFlightPnrDtoList;

	private boolean sendSMS;

	private boolean sendEmail;

	private String action;

	private UserPrincipal userPrincipal;

	private CredentialsDTO credentialsDTO;

	private String language;

	/**
	 * Constructor for NotifyReservations
	 * 
	 * @throws ModuleException
	 */

	public NotifyReservations(int flightId, FlightNotificationDTO fltDTO, Collection colFlightPnrDTO, UserPrincipal userPrincipal)
			throws ModuleException {
		this.flightId = flightId;
		this.fltNotifyDto = fltDTO;
		this.colFlightPnrDtoList = colFlightPnrDTO;
		this.userPrincipal = userPrincipal;
		validateParams(this.flightId, this.fltNotifyDto, this.colFlightPnrDtoList);
		if (fltDTO != null) {
			this.sendSMS = fltDTO.isNotifyBySms();
			this.sendEmail = fltDTO.isNotifyByEmail();
			this.action = fltDTO.getAction();
			this.language = fltDTO.getPreferredLanguage();
		}
		this.credentialsDTO = getCallerCredentials(null);
	}

	/**
	 * Generate Customer Notifications for Pnr
	 * 
	 */

	public void generateCustomerNotifications() throws ModuleException {
		// Profile List
		List<MessageProfile> messageProfiles = new ArrayList<MessageProfile>();

		// Reservation Object list
		List<FlightPnrNotificationDTO> emailPnrList = new ArrayList<FlightPnrNotificationDTO>();
		List<FlightPnrNotificationDTO> smsPnrList = new ArrayList<FlightPnrNotificationDTO>();

		for (Iterator itColFlightPnrDtoList = this.colFlightPnrDtoList.iterator(); itColFlightPnrDtoList.hasNext();) {
			FlightPnrNotificationDTO flightPnrDto = (FlightPnrNotificationDTO) itColFlightPnrDtoList.next();

			if (flightPnrDto != null) {
				if (flightPnrDto.isSendSms() && flightPnrDto.getMobile() != null && flightPnrDto.getMobile().length() > 0) {
					smsPnrList.add(flightPnrDto);
				}
				if (flightPnrDto.isSendEmail() && flightPnrDto.getEmail() != null && flightPnrDto.getEmail().length() > 0
						&& flightPnrDto.getEmail().indexOf("@") != -1) {
					emailPnrList.add(flightPnrDto);
				}

			}
		}

		String templateName = "";

		int flightNotificationId = saveNotification(this.userPrincipal);

		if (this.sendSMS && smsPnrList.size() > 0) {
			FlightPnrNotificationDTO fltPnrDto = null;

			if (this.action != null) {
				if (this.action.equals(ACTION_UPDATE)) {
					templateName = ReservationInternalConstants.PnrTemplateNames.FLIGHT_CHANGE_SMS;
				} else if (this.action.equals(ACTION_CANCEL)) {
					templateName = ReservationInternalConstants.PnrTemplateNames.FLIGHT_CANCEL_SMS;
				} else if (this.action.equals(ACTION_REPROTECT)) {
					templateName = ReservationInternalConstants.PnrTemplateNames.FLIGHT_REPROTECT_SMS;
				}
			}

			for (Iterator itSmsPnrList = smsPnrList.iterator(); itSmsPnrList.hasNext();) {
				fltPnrDto = (FlightPnrNotificationDTO) itSmsPnrList.next();

				Collection colReservationAudit = composeAudit(fltPnrDto.getPnr(), this.fltNotifyDto.getOldFlightAlertInfoDTO(),
						this.fltNotifyDto.getAlertTemplateIds(), fltPnrDto.getMobile(), "SMS");

				FlightPnrNotification objFltPnr = new FlightPnrNotification();
				objFltPnr.setPnrSegId(fltPnrDto.getPnrSegmentId());
				objFltPnr.setNotificationDate(new java.util.Date(System.currentTimeMillis()));
				objFltPnr.setNotificationChannel("SMS");
				objFltPnr.setRecepients(fltPnrDto.getMobile());
				objFltPnr.setDeliveryStatus("S");
				objFltPnr.setFlightNotificationId(flightNotificationId);
				objFltPnr.setUserId(this.userPrincipal.getUserId());

				ArrayList aList = new ArrayList();
				aList.add(objFltPnr);

				MessageProfile profile = getMessageProfile(this.fltNotifyDto, fltPnrDto, templateName, fltPnrDto.getMobile(),
						colReservationAudit, aList, this.SMS_MESSAGE);
				messageProfiles.add(profile);

			}
		
			// Sending SMS to configured Numbers.
			String enabledFlag = globalConfig.getBizParam(SystemParamKeys.IS_TO_NOTIFY_AN_OFFICIAL_XBE);
			
			if (enabledFlag != null && enabledFlag.equals("Y")) {
				
				List<String>  mobileNumbers = null;

				try {
					mobileNumbers = ReservationModuleUtils.getReservationBD().getOfficersMobileNumbersList();
				} catch (ModuleException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				for(int i=0;i<mobileNumbers.size();i++){
					
					FlightPnrNotification objFltPnr = new FlightPnrNotification();
					objFltPnr.setNotificationDate(new java.util.Date(System.currentTimeMillis()));
					objFltPnr.setNotificationChannel("SMS");
					objFltPnr.setRecepients(mobileNumbers.get(i));
					objFltPnr.setDeliveryStatus("S");
					objFltPnr.setFlightNotificationId(flightNotificationId);
					objFltPnr.setUserId(this.userPrincipal.getUserId());

					ArrayList aList = new ArrayList();
					aList.add(objFltPnr);

					MessageProfile profile = getMessageProfile(this.fltNotifyDto, fltPnrDto, templateName,mobileNumbers.get(i),
							null, aList, this.SMS_MESSAGE);
					messageProfiles.add(profile);

					
				}	
			}
			
			// Sending SMS to Supervisor's mobile number for each set of outgoing notifications
			if (AlertingModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.CC_SUPERVISOR_MOBILE_NO) != null
					&& !"".equals(AlertingModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.CC_SUPERVISOR_MOBILE_NO)
							.trim())) {
				String strSupervisorMobile = AlertingModuleUtils.getGlobalConfig().getBizParam(
						SystemParamKeys.CC_SUPERVISOR_MOBILE_NO);
				String[] arrMobile = strSupervisorMobile.split(",");
				String strMobileNo = "";
				boolean isDigit = true;
				for (int arrInd = 0; arrInd < arrMobile.length; arrInd++) {
					strMobileNo = arrMobile[arrInd].replaceAll("-", "");
					isDigit = true;
					if (!"".equals(strMobileNo)) {
						for (int j = 0; j < strMobileNo.length(); j++) { // validation to see whether the mobile number
																			// contains allowed digits only
							if (!Character.isDigit(strMobileNo.charAt(j)))
								isDigit = false;
						}
						if (isDigit) { // Send SMS only if mobile number is valid
							MessageProfile profile = getMessageProfile(this.fltNotifyDto, null, templateName, strMobileNo, null,
									null, this.SMS_MESSAGE);
							messageProfiles.add(profile);
						}
					}
				}
			}
		}

		if ((this.sendEmail) && emailPnrList.size() > 0) {
			FlightPnrNotificationDTO fltPnrDto = null;

			if (this.action != null) {
				if (this.action.equals(ACTION_UPDATE)) {
					templateName = ReservationInternalConstants.PnrTemplateNames.FLIGHT_CHANGE_EMAIL;
				} else if (this.action.equals(ACTION_CANCEL)) {
					templateName = ReservationInternalConstants.PnrTemplateNames.FLIGHT_CANCEL_EMAIL;
				} else if (this.action.equals(ACTION_REPROTECT)) {
					templateName = ReservationInternalConstants.PnrTemplateNames.FLIGHT_REPROTECT_EMAIL;
				}
			}

			for (Iterator itEmailPnrList = emailPnrList.iterator(); itEmailPnrList.hasNext();) {
				fltPnrDto = (FlightPnrNotificationDTO) itEmailPnrList.next();

				Collection colReservationAudit = composeAudit(fltPnrDto.getPnr(), this.fltNotifyDto.getOldFlightAlertInfoDTO(),
						this.fltNotifyDto.getAlertTemplateIds(), fltPnrDto.getEmail(), "EMAIL");

				FlightPnrNotification objFltPnr = new FlightPnrNotification();
				objFltPnr.setPnrSegId(fltPnrDto.getPnrSegmentId());
				objFltPnr.setNotificationDate(new java.util.Date(System.currentTimeMillis()));
				objFltPnr.setNotificationChannel("EMAIL");
				objFltPnr.setRecepients(fltPnrDto.getEmail());
				objFltPnr.setDeliveryStatus("S");
				objFltPnr.setFlightNotificationId(flightNotificationId);
				objFltPnr.setUserId(this.userPrincipal.getUserId());

				ArrayList aList = new ArrayList();
				aList.add(objFltPnr);
				MessageProfile profile = getMessageProfile(this.fltNotifyDto, fltPnrDto, templateName, fltPnrDto.getEmail(),
						colReservationAudit, aList, this.EMAIL_MESSAGE);
				messageProfiles.add(profile);

			}
			// Sending Email to Supervisor's email id for each set of outgoing notifications
			if (AlertingModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.CC_SUPERVISOR_EMAIL) != null
					&& !"".equals(AlertingModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.CC_SUPERVISOR_EMAIL).trim())) {
				String strSupervisorEmail = AlertingModuleUtils.getGlobalConfig()
						.getBizParam(SystemParamKeys.CC_SUPERVISOR_EMAIL);
				String[] arrEmail = strSupervisorEmail.split(",");
				for (int arrInd = 0; arrInd < arrEmail.length; arrInd++) {
					if (arrEmail[arrInd] != null && !"".equals(arrEmail[arrInd].trim())) {
						if (arrEmail[arrInd].indexOf("@") != -1) {
							MessageProfile profile = getMessageProfile(this.fltNotifyDto, fltPnrDto, templateName,
									arrEmail[arrInd], null, null, this.EMAIL_MESSAGE);
							messageProfiles.add(profile);
						}
					}
				}
			}

		}

		if (messageProfiles.size() > 0) {
			MessagingServiceBD messagingServiceBD = AlertingModuleUtils.getMessagingServiceBD();
			messagingServiceBD.sendMessages(messageProfiles);
		}

	}

	/**
	 * Create Message profile for flight alterations
	 * 
	 * @param reservation
	 * @param flightAlertDTO
	 * @param flightReservationAlertDTO
	 * @param topicName
	 * @param toAddress
	 * @param colReservationAudit
	 * @return
	 * @throws ModuleException
	 */
	private MessageProfile getMessageProfile(FlightNotificationDTO fltDto, FlightPnrNotificationDTO fltPnrDto, String topicName,
			String toAddress, Collection colReservationAudit, Collection colObject, String messageType) throws ModuleException {

		String carrierCode = AppSysParamsUtil.extractCarrierCode(fltDto.getOldFlightAlertInfoDTO().getFlightNumber());
		CommonTemplatingDTO commonTemplatingDTO = AppSysParamsUtil.composeCommonTemplatingDTO(carrierCode);

		// Email ids list
		List messageList = new ArrayList();
		UserMessaging user = new UserMessaging();
		user.setToAddres(toAddress);
		messageList.add(user);

		MessageProfile profile = new MessageProfile();
		profile.setUserMessagings(messageList);

		Topic topic = new Topic();
		HashMap map = new HashMap();

		topic.setTopicName(topicName);
		topic.setLocale(new Locale(this.language));

		map.put("alertTemplateEnum", fltDto.getAlertTemplateIds());
		map.put("oldFlightAlertInfoDTO", fltDto.getOldFlightAlertInfoDTO());
		map.put("newFlightAlertInfoDTO", fltDto.getNewFlightAlertInfoDTO());
		map.put("emailSubject", fltDto.getEmailSubject());
		if (fltPnrDto != null) {
			map.put("user", fltPnrDto.getPassengerName());
			map.put("pnr", fltPnrDto.getPnr());
			map.put("firstName",  BeanUtils.getFirst20Characters(fltPnrDto.getFirstName(), 20));
			map.put("title", BeanUtils.makeFirstLetterCapital(fltPnrDto.getTitle()));
			map.put("from", fltDto.getDepartureAirport());
			map.put("to", fltDto.getArrivalAirport());
		}

		String strNotifyMessage = Util.setStringToPlaceHolderIndex(fltDto.getSmsMessageDesc(), map, PLACE_HOLDER);
		// This is due to AARESAA-8724
		strNotifyMessage = Util.replaceRoute(strNotifyMessage, fltPnrDto.getRoute());

		map.put("message", strNotifyMessage);

		// Set the userEdited parameter only for SMS.This is done for separate email and SMS template.
		if (messageType.equals(this.SMS_MESSAGE)) {
			if (fltDto.getSmsMessageDesc() != null && (!fltDto.getSmsMessageDesc().equals(""))) {
				map.put("userEdited", true);
			} else {
				map.put("userEdited", false);
			}
		}

		map.put("flight", fltDto.getOldFlightAlertInfoDTO().getFlightNumber());
		Calendar cal = Calendar.getInstance();
		if (fltDto.getOldFlightAlertInfoDTO() != null)
			cal.setTime(fltDto.getOldFlightAlertInfoDTO().getDepartureDate());
		String hourOfDay = (cal.get(Calendar.HOUR_OF_DAY) >= 0 && cal.get(Calendar.HOUR_OF_DAY) <= 9) ? "0"
				+ cal.get(Calendar.HOUR_OF_DAY) : String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
		String minute = (cal.get(Calendar.MINUTE) >= 0 && cal.get(Calendar.MINUTE) <= 9)
				? "0" + cal.get(Calendar.MINUTE)
				: String.valueOf(cal.get(Calendar.MINUTE));
		map.put("deptTime", fltDto.getOldFlightAlertInfoDTO().getDepartureStringDate("dd/MM/yy") + " " + hourOfDay + ":" + minute);
		map.put("companyAddress", getCompanyAddressInformation(carrierCode));
		// map.put("logoImageName", getLogoImageName(carrierCode));
		map.put("phone", AlertingModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.SMS_PHONE_NO));
		map.put("ibeURL", CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.ACCELAERO_IBE_URL));
		map.put("logoImageName", StaticFileNameUtil.getCorrected("Curve_logo.jpg")); // hard coded
		map.put("carrierCode", StaticFileNameUtil.getCorrected("Curve_logo.jpg")); // hard coded
		if (commonTemplatingDTO != null) {
			map.put("commonTemplatingDTO", commonTemplatingDTO);
		}

		topic.setTopicParams(map);
		topic.setAuditInfo(colReservationAudit);
		topic.setObjectInfo(colObject);
		topic.setAttachMessageBody(true);
		profile.setTopic(topic);

		return profile;
	}

	/**
	 * Validate Parameters
	 * 
	 * @param colFlightReservationAlertDTO
	 * @param isRescheduleFlight
	 * @param isCancelFlight
	 * @param flightAlertDTO
	 * @param credentialsDTO
	 * @throws ModuleException
	 */
	private void validateParams(int flightId, FlightNotificationDTO fltDTO, Collection colFlightPnrDTO) throws ModuleException {
		if (log.isDebugEnabled())
			log.debug("Inside validateParams");

		if (flightId <= 0 || fltDTO == null || colFlightPnrDTO == null) {
			throw new ModuleException("alerting.arg.invalid.null");
		}

		if (log.isDebugEnabled())
			log.debug("Exit validateParams");
	}

	/**
	 * Returns the logo image name
	 * 
	 * @param carrierCode
	 * @return
	 */
	private static String getLogoImageName(String carrierCode) {
		String logoImageName = "LogoAni.gif";
		if (carrierCode != null) {
			logoImageName = "LogoAni" + carrierCode + ".gif";
		}
		return StaticFileNameUtil.getCorrected(logoImageName);
	}

	/**
	 * sets the sms message description
	 * 
	 * @param fltDto
	 * @return
	 */
	public static boolean setMessageDesc(FlightNotificationDTO fltDto) throws ModuleException {
		HashMap mapAlertDetails = fltDto.getAlertDetails();
		AlertTemplateEnum templateID;
		String desc = "";
		boolean newFormat = false;
		if (mapAlertDetails != null) {

			for (Iterator iterAlertDetails = mapAlertDetails.keySet().iterator(); iterAlertDetails.hasNext();) {
				templateID = (AlertTemplateEnum) iterAlertDetails.next();

				HashMap<String, Object> mapNotificationDetails = (HashMap) mapAlertDetails.get(templateID);

				Calendar cal = Calendar.getInstance();
				if (fltDto.getNewFlightAlertInfoDTO() != null)
					cal.setTime(fltDto.getNewFlightAlertInfoDTO().getDepartureDate());
				if (templateID.equals(AlertTemplateEnum.TEMPLATE_FLIGHT_REPROTECT_MSG)) {
					if (fltDto.getOldFlightAlertInfoDTO() != null)
						cal.setTime(fltDto.getOldFlightAlertInfoDTO().getDepartureDate());
				}

				String hourOfDay = (cal.get(Calendar.HOUR_OF_DAY) >= 0 && cal.get(Calendar.HOUR_OF_DAY) <= 9) ? "0"
						+ cal.get(Calendar.HOUR_OF_DAY) : String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
				String minute = (cal.get(Calendar.MINUTE) >= 0 && cal.get(Calendar.MINUTE) <= 9)
						? "0" + cal.get(Calendar.MINUTE)
						: String.valueOf(cal.get(Calendar.MINUTE));

				mapNotificationDetails.put(AlertTemplateEnum.TemplateParams.OLD_DEPARTURE_DATE, fltDto.getOldFlightAlertInfoDTO()
						.getDepartureStringDate("dd/MM/yy"));
				if (templateID.equals(AlertTemplateEnum.TEMPLATE_FLIGHT_DELAY)
						|| templateID.equals(AlertTemplateEnum.TEMPLATE_CHANGE_DATE_TIME)) {
					mapNotificationDetails.put(AlertTemplateEnum.TemplateParams.NEW_DEPARTURE_DATE, fltDto
							.getNewFlightAlertInfoDTO().getDepartureStringDate("dd/MM/yy") + " " + hourOfDay + ":" + minute);
				}
				if (templateID.equals(AlertTemplateEnum.TEMPLATE_FLIGHT_REPROTECT_MSG)) {
					mapNotificationDetails.put(AlertTemplateEnum.TemplateParams.OLD_SEGMENT_DEPARTURE, fltDto
							.getOldFlightAlertInfoDTO().getDepartureStringDate("dd/MM/yy") + " " + hourOfDay + ":" + minute);
				}
				mapNotificationDetails.put(AlertTemplateEnum.TemplateParams.OLD_ROUTE, fltDto.getOldFlightAlertInfoDTO()
						.getOriginAptCode() + "/" + fltDto.getOldFlightAlertInfoDTO().getDestinationAptCode());
				mapNotificationDetails.put("phone",
						AlertingModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.SMS_PHONE_NO));
				mapAlertDetails.put(templateID, mapNotificationDetails);

				String notificationMessage = buildGeneralNotificationMessage(mapAlertDetails);

				fltDto.setSmsMessageDesc(notificationMessage);
				newFormat = true;
			}

		}
		return newFormat;
	}

	/**
	 * Return the company address information
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static String getCompanyAddressInformation(String carrierCode) throws ModuleException {
		String companyAddress = "";
		if (AccelAeroClients.AIR_ARABIA.equals(globalConfig.getBizParam(SystemParamKeys.ACCELAERO_CLIENT_IDENTIFIER))
				|| AccelAeroClients.AIR_ARABIA_GROUP
						.equals(globalConfig.getBizParam(SystemParamKeys.ACCELAERO_CLIENT_IDENTIFIER))) {
			companyAddress += " <br>" + globalConfig.getBizParam(SystemParamKeys.COMP_TEL, carrierCode) + " <br>"
					+ globalConfig.getBizParam(SystemParamKeys.COMP_EMAIL, carrierCode);
		} else {
			companyAddress += " <br>" + globalConfig.getBizParam(SystemParamKeys.COMP_ADD1, carrierCode) + " <br>"
					+ globalConfig.getBizParam(SystemParamKeys.COMP_ADD2, carrierCode) + " <br>"
					+ globalConfig.getBizParam(SystemParamKeys.COMP_TEL, carrierCode) + " <br>"
					+ globalConfig.getBizParam(SystemParamKeys.COMP_EMAIL, carrierCode);
		}
		return companyAddress;
	}

	/**
	 * Return the generated notification message
	 * 
	 * @return String
	 * @throws ModuleException
	 */
	public static String getGeneratedNotificationMessage(FlightNotificationDTO fltDto, FlightPnrNotificationDTO fltPnrDto, String method)
			throws ModuleException {

		String carrierCode = AppSysParamsUtil.extractCarrierCode(fltDto.getOldFlightAlertInfoDTO().getFlightNumber());
		CommonTemplatingDTO commonTemplatingDTO = AppSysParamsUtil.composeCommonTemplatingDTO(carrierCode);

		/*
		 * if (fltDto.getSmsMessageDesc() == null || "".equals(fltDto.getSmsMessageDesc())) setMessageDesc(fltDto);
		 */

		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("method", method);
		map.put("alertTemplateEnum", fltDto.getAlertTemplateIds());
		map.put("oldFlightAlertInfoDTO", fltDto.getOldFlightAlertInfoDTO());
		map.put("newFlightAlertInfoDTO", fltDto.getNewFlightAlertInfoDTO());
		map.put("ibeURL", CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.ACCELAERO_IBE_URL));
		// map.put("logoImageName", getLogoImageName(carrierCode));
		map.put("companyAddress", getCompanyAddressInformation(carrierCode));
		map.put("phone", AlertingModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.SMS_PHONE_NO));
		map.put("message", fltDto.getSmsMessageDesc());
		map.put("logoImageName", StaticFileNameUtil.getCorrected("Curve_logo.jpg")); // hard coded

		if (commonTemplatingDTO != null) {
			map.put("commonTemplatingDTO", commonTemplatingDTO);
		}
		if (fltPnrDto != null) {
			map.put("user", fltPnrDto.getPassengerName());
			map.put("pnr", fltPnrDto.getPnr());
		}

		TemplateEngine engine = null;
		String templateName = "";

		// Used the same template to display the message from front end.Because it is easier to edit the notification.

		/*
		 * if(fltDto.isNotifyBySms()){ // SMS template used for Sms Only, Sms And Email, Sms Or email notification
		 * channels
		 */
		if (fltDto.getAction() != null && fltDto.getAction().equals(ACTION_CANCEL)) {
			templateName = ReservationInternalConstants.PnrTemplateNames.FLIGHT_CANCEL_SMS;
		} else if (fltDto.getAction() != null && fltDto.getAction().equals(ACTION_UPDATE)) {
			templateName = ReservationInternalConstants.PnrTemplateNames.FLIGHT_CHANGE_SMS;
		} else if (fltDto.getAction() != null && fltDto.getAction().equals(ACTION_REPROTECT)) {
			templateName = ReservationInternalConstants.PnrTemplateNames.FLIGHT_REPROTECT_SMS;
		}
		/*
		 * } else if(fltDto.isNotifyByEmail()) { // Email template used if notification channel is "Email Only"
		 * 
		 * if(fltDto.getAction() != null && fltDto.getAction().equals(ACTION_CANCEL)){ templateName =
		 * ReservationInternalConstants.PnrTemplateNames.FLIGHT_CANCEL_EMAIL; } else if(fltDto.getAction() != null &&
		 * fltDto.getAction().equals(ACTION_UPDATE)){ templateName =
		 * ReservationInternalConstants.PnrTemplateNames.FLIGHT_CHANGE_EMAIL; } else if(fltDto.getAction() != null &&
		 * fltDto.getAction().equals(ACTION_REPROTECT)){ templateName =
		 * ReservationInternalConstants.PnrTemplateNames.FLIGHT_REPROTECT_EMAIL; } }
		 */
		TopicConfig topicConfig = (TopicConfig) ((MessagingModuleConfig) MessagingModuleUtils.getInstance().getModuleConfig())
				.getTopicConfigurationMap().get(templateName);

		String templateFileName = topicConfig.getTemplateFileName();
		String prefLang = Locale.ENGLISH.toString().toUpperCase();

		if (fltDto.getPreferredLanguage() != null && (!fltDto.getPreferredLanguage().equals(""))) {
			prefLang = fltDto.getPreferredLanguage().toUpperCase();
		}

		if (templateFileName.indexOf(".") > 0) {
			templateFileName = templateFileName.substring(0, templateFileName.indexOf(".")) + "_" + prefLang
					+ templateFileName.substring(templateFileName.indexOf("."));
		} else {
			templateFileName = templateFileName + "_" + prefLang;
		}

		StringWriter writer = null;

		try {
			engine = new TemplateEngine();
			writer = new StringWriter();
		} catch (Exception e) {
			throw new ModuleException("alerting.errorCreatingFromTemplate", e);
		}
		if (map != null) {
			engine.writeTemplate(map, templateFileName, writer);
		}

		// Convert to input stream
		ByteArrayInputStream inputStream = null;
		try {
			byte arr[] = writer.toString().getBytes(Message.CONTENT_BYTE_ARRAY_TYPE);
			inputStream = new ByteArrayInputStream(arr);
		} catch (Exception e) {
			log.error("Preparing message failed", e);
		}

		return parseMessage(inputStream);
	}

	private static String parseMessage(InputStream inputStream) {
		StringBuffer content = new StringBuffer();
		try {
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			docBuilderFactory.setCoalescing(true);
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(inputStream);

			doc.getDocumentElement().normalize();
			NodeList listOfRecords = doc.getElementsByTagName("body");

			int intRecords = listOfRecords.getLength();

			for (int i = 0; i < intRecords; i++) {
				Node menuNode = listOfRecords.item(i);
				content.append(menuNode.getFirstChild().getNodeValue());
			}

		} catch (SAXParseException spe) {
			log.error("Parsing error" + ", line " + spe.getLineNumber() + ", uri " + spe.getSystemId(), spe);
		} catch (SAXException e) {
			log.error(e);
		} catch (Throwable t) {
			log.error(t);
		}

		return content.toString();
	}

	private int saveNotification(UserPrincipal userPrincipal) throws ModuleException {
		FlightNotification objN = new FlightNotification();
		objN.setNotificationCode(this.fltNotifyDto.getCurrentNotificationCode());

		String hexString = "";
		if (this.language.equals(Locale.ENGLISH.toString())) {
			hexString = this.fltNotifyDto.getCurrentNotificationMsg();
		} else {
			hexString = StringUtil.convertToHex(this.fltNotifyDto.getCurrentNotificationMsg());
		}

		objN.setMessage(DataConverterUtil.createClob(hexString));
		objN.setFlightId(this.flightId);
		objN.setUserId(userPrincipal.getUserId());
		objN.setLanguage(this.language);
		int fltNotificationId = AlertingServicesBL.saveFlightNotification(objN);
		return fltNotificationId;

	}

	/**
	 * Returns caller credentials information
	 * 
	 * @param trackInfoDTO
	 * @return
	 * @throws ModuleException
	 */
	private CredentialsDTO getCallerCredentials(TrackInfoDTO trackInfoDTO) throws ModuleException {
		UserPrincipal userPrincipal = this.userPrincipal;

		if (userPrincipal == null || userPrincipal.getName() == null) {
			throw new ModuleException("module.credential.error");
		}

		CredentialsDTO credentialsDTO = new CredentialsDTO();
		credentialsDTO.setTrackInfoDTO(trackInfoDTO);

		if (SalesChannelsUtil.isSalesChannelWebOrMobile(userPrincipal.getSalesChannel())) {
			if (trackInfoDTO == null) {
				throw new ModuleException("module.credential.error");
			}
		} else {
			if (trackInfoDTO != null) {
				throw new ModuleException("module.credential.error");
			}
		}

		credentialsDTO.setUserId(userPrincipal.getUserId());
		credentialsDTO.setCustomerId(userPrincipal.getCustomerId());
		credentialsDTO.setAgentCode(userPrincipal.getAgentCode());
		credentialsDTO.setAgentStation(userPrincipal.getAgentStation());
		credentialsDTO.setSalesChannelCode(new Integer(userPrincipal.getSalesChannel()));
		credentialsDTO.setColUserDST(userPrincipal.getColUserDST());
		credentialsDTO.setDefaultCarrierCode(userPrincipal.getDefaultCarrierCode());
		return credentialsDTO;
	}

	/**
	 * Compose the Email Audit
	 * 
	 * @param pnr
	 * @param flightAlertInfoDTO
	 * @param colAlertTemplateIds
	 * @return
	 */
	private Collection composeAudit(String pnr, FlightAlertInfoDTO flightAlertInfoDTO, Collection colAlertTemplateIds,
			String recepient, String channel) {
		ReservationAudit reservationAudit = new ReservationAudit();
		reservationAudit.setModificationType(AuditTemplateEnum.NOTIFY_PNR.getCode());
		ReservationAudit.createReservationAudit(reservationAudit, pnr, null, credentialsDTO);
		// Setting the ip address
		if (credentialsDTO.getTrackInfoDTO() != null && credentialsDTO.getTrackInfoDTO().getIpAddress() != null) {
			reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.NotifyPnr.IP_ADDDRESS, credentialsDTO
					.getTrackInfoDTO().getIpAddress());
		}

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.NotifyPnr.ORIGIN_CARRIER,
				AppSysParamsUtil.extractCarrierCode(flightAlertInfoDTO.getFlightNumber()));

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.NotifyPnr.NOTIFICATION_CODE,
				this.fltNotifyDto.getCurrentNotificationCode());

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.NotifyPnr.NOTIFICATION_RECEPIENT, recepient);

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.NotifyPnr.NOTIFICATION_CHANNEL, channel);

		reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.NotifyPnr.NOTIFICATION_CONTENT,
				getFlightAlteration(this.fltNotifyDto.getOldFlightAlertInfoDTO(), this.fltNotifyDto.getAlertTemplateIds()));

		Collection colReservationAudit = new ArrayList();
		colReservationAudit.add(reservationAudit);

		return colReservationAudit;
	}

	/**
	 * Return Flight Alterations
	 * 
	 * @param flightAlertInfoDTO
	 * @param colAlertTemplateIds
	 * @return
	 */
	private String getFlightAlteration(FlightAlertInfoDTO flightAlertInfoDTO, Collection colAlertTemplateIds) {
		StringBuilder changes = new StringBuilder();
		changes.append(" Flight Number : " + flightAlertInfoDTO.getFlightNumber() + ",");
		changes.append(" Departure Date : " + flightAlertInfoDTO.getDepartureStringDate("EEE, dd MMM yyyy"));

		if (flightAlertInfoDTO.getSegmentDetails() != null) {
			changes.append(" Segment Details : " + flightAlertInfoDTO.getSegmentDetails());
		}

		if (ACTION_UPDATE.equals(this.action)) {
			// changes.append("- Flight Delayed ");
			changes.append("- Flight Time Changed ");
		} else if (ACTION_CANCEL.equals(this.action)) {
			changes.append("- Flight Cancelled ");
		}

		/*
		 * changes.append(" Effected by : "); for (Iterator iter = colAlertTemplateIds.iterator(); iter.hasNext();) {
		 * AlertTemplateEnum alertTemplateEnum = (AlertTemplateEnum) iter.next(); changes.append(" " +
		 * alertTemplateEnum.getCodeDesc() + " "); }
		 */

		return changes.toString();
	}

	/**
	 * Builds general notification message for given parameter values in the HashMap
	 * 
	 * @param mapAlertDetails
	 * @return
	 * @throws ModuleException
	 */
	private static String buildGeneralNotificationMessage(HashMap mapAlertDetails) throws ModuleException {
		AlertTemplateEnum templateID;
		AlertTemplate notificationTemplate = null;
		String notificationMessage = "";

		// Get each reffering alert Template
		for (Iterator iterAlertDetails = mapAlertDetails.keySet().iterator(); iterAlertDetails.hasNext();) {
			templateID = (AlertTemplateEnum) iterAlertDetails.next();

			if (templateID == null) {
				throw new ModuleException("No valid notification template found");
			}

			// Get alert template
			if (templateID.equals(AlertTemplateEnum.TEMPLATE_CANCELLATION))
				notificationTemplate = AlertingServicesBL.getAlertTemplate(AlertTemplateEnum.TEMPLATE_FLIGHT_CANCEL_MSG);
			else if (templateID.equals(AlertTemplateEnum.TEMPLATE_FLIGHT_DELAY)
					|| templateID.equals(AlertTemplateEnum.TEMPLATE_CHANGE_DATE_TIME))
				notificationTemplate = AlertingServicesBL.getAlertTemplate(AlertTemplateEnum.TEMPLATE_FLIGHT_DELAY_MSG);
			else if (templateID.equals(AlertTemplateEnum.TEMPLATE_FLIGHT_REPROTECT_MSG))
				notificationTemplate = AlertingServicesBL.getAlertTemplate(AlertTemplateEnum.TEMPLATE_FLIGHT_REPROTECT_MSG);
			if (notificationTemplate == null) {
				throw new ModuleException("No valid notification template found");
			}

			// Build alert message for current template
			notificationMessage += Util.setStringToPlaceHolderIndex(notificationTemplate.getAlertTemplateContent(),
					(HashMap) mapAlertDetails.get(templateID));

		}

		return notificationMessage;
	}

}
