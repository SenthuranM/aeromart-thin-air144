/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Juil 4, 2005
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.alerting.core.bl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.alerting.api.dto.AlertDetailDTO;
import com.isa.thinair.alerting.api.dto.FlightNotificationDTO;
import com.isa.thinair.alerting.api.dto.FlightPnrNotificationDTO;
import com.isa.thinair.alerting.api.dto.FlightPnrSearchNotificationDTO;
import com.isa.thinair.alerting.api.dto.QueueRequestDetailsDTO;
import com.isa.thinair.alerting.api.dto.RequestQueueUser;
import com.isa.thinair.alerting.api.model.Alert;
import com.isa.thinair.alerting.api.model.AlertTemplate;
import com.isa.thinair.alerting.api.model.ApplicablePage;
import com.isa.thinair.alerting.api.model.FlightNotification;
import com.isa.thinair.alerting.api.model.FlightPnrNotification;
import com.isa.thinair.alerting.api.model.Queue;
import com.isa.thinair.alerting.api.model.QueueRequest;
import com.isa.thinair.alerting.api.model.RequestHistory;
import com.isa.thinair.alerting.api.service.AlertingModuleUtils;
import com.isa.thinair.alerting.api.util.AlertTemplateEnum;
import com.isa.thinair.alerting.core.persistence.dao.AlertingDAO;
import com.isa.thinair.alerting.core.persistence.dao.AlertingJdbcDAO;
import com.isa.thinair.alerting.core.util.AlertingInternalConstants;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Alerting business related services
 * 
 * @author Nilindra Fernando
 * @since 2.0
 */
public class AlertingServicesBL {

	private static final long serialVersionUID = 4043042233531319460L;

	private static final Log log = LogFactory.getLog(AlertingServicesBL.class);

	public static List retrieveAlerts(Collection alertIds) throws ModuleException {
		try {
			return lookupAlertingDAO().retrieveAlerts(alertIds);
		} catch (CommonsDataAccessException e) {
			log.error("Retrieving alerts is failed.", e);
			throw new ModuleException(e.getExceptionCode(), "messaging.desc");
		}
	}

	public static AlertTemplate getAlertTemplate(AlertTemplateEnum templateID) throws ModuleException {
		try {
			return lookupAlertingDAO().getAlertTemplate(templateID);
		} catch (CommonsDataAccessException e) {
			log.error("Getting alerts template is failed.", e);
			throw new ModuleException(e.getExceptionCode(), "messaging.desc");
		}
	}

	public static List retrieveAlertsForPrivileges(List privilegeIds) throws ModuleException {
		try {
			return lookupAlertingDAO().retrieveAlertsForPrivileges(privilegeIds);
		} catch (CommonsDataAccessException e) {
			log.error("Retrieving alerts for privileges is failed.", e);
			throw new ModuleException(e.getExceptionCode(), "messaging.desc");
		}
	}

	public static List retrieveAlertsForPnrSegments(List pnrSegmentIds) throws ModuleException {
		try {
			return lookupAlertingDAO().retrieveAlertsForPnrSegments(pnrSegmentIds);
		} catch (CommonsDataAccessException e) {
			log.error("Retrieving alerts for PNR segments is failed.", e);
			throw new ModuleException(e.getExceptionCode(), "messaging.desc");
		}
	}

	public static Map getAlertsForPnrSegments(Collection pnrSegmentIds) throws ModuleException {
		try {
			return lookupAlertingDAO().getAlertsForPnrSegments(pnrSegmentIds);
		} catch (CommonsDataAccessException e) {
			log.error("Getting alerts for PNR segment IDs is failed.", e);
			throw new ModuleException(e, e.getExceptionCode(), e.getModuleCode());
		}
	}

	public static void updateAlertStatus(Long alertId, String status) throws ModuleException {
		try {
			lookupAlertingDAO().updateAlertStatus(alertId, status);
		} catch (CommonsDataAccessException e) {
			log.error("Updating alert status is failed.", e);
			throw new ModuleException(e.getExceptionCode(), "messaging.desc");
		}
	}

	public static void addAlert(Alert alert) throws ModuleException {
		try {
			lookupAlertingDAO().addAlert(alert);
		} catch (CommonsDataAccessException e) {
			log.error("Adding alert is failed.", e);
			throw new ModuleException(e.getExceptionCode(), "messaging.desc");
		}
	}

	public static void addAlerts(Collection alerts) throws ModuleException {
		try {
			lookupAlertingDAO().addAlerts(alerts);
		} catch (CommonsDataAccessException e) {
			log.error("Adding alerts is failed.", e);
			throw new ModuleException(e.getExceptionCode(), "messaging.desc");
		}
	}

	public static void updateAlert(Alert alert) throws ModuleException {
		try {
			lookupAlertingDAO().updateAlert(alert);
		} catch (CommonsDataAccessException e) {
			log.error("Updating alert is failed.", e);
			throw new ModuleException(e.getExceptionCode(), "messaging.desc");
		}
	}

	public static void updateAlerts(Collection<Alert> alerts) throws ModuleException {
		try {
			lookupAlertingDAO().updateAlerts(alerts);
		} catch (CommonsDataAccessException e) {
			log.error("Updating alert is failed.", e);
			throw new ModuleException(e.getExceptionCode(), "messaging.desc");
		}
	}

	public static List retrieveAlertsForUser(String userId) throws ModuleException {
		try {
			return getAlertingJdbcDAO().retrieveAlertsForUser(userId);
		} catch (CommonsDataAccessException e) {
			log.error("Retrieving alerts for user is failed.", e);
			throw new ModuleException(e.getExceptionCode(), "messaging.desc");
		}
	}

	public static Page retrieveAlertsForSearchCriteria(AlertDetailDTO searchCriteria) throws ModuleException {
		try {
			return getAlertingJdbcDAO().retrieveAlertsForSearchCriteria(searchCriteria);
		} catch (CommonsDataAccessException e) {
			log.error("Retrieving alerts for search criteria is failed.", e);
			throw new ModuleException(e.getExceptionCode(), "messaging.desc");
		}
	}

	public static void removeAlerts(Collection alertIds) throws ModuleException {
		try {
			lookupAlertingDAO().removeAlerts(alertIds);
		} catch (CommonsDataAccessException e) {
			log.error("Removing alerts is failed.", e);
			throw new ModuleException(e.getExceptionCode(), "messaging.desc");
		}
	}

	public static void removeAlertsOfPnrSegments(List pnrSegmentIds) throws ModuleException {
		try {
			lookupAlertingDAO().removeAlertsOfPnrSegments(pnrSegmentIds);
		} catch (CommonsDataAccessException e) {
			log.error("Removing alerts of PNR segment IDs is failed.", e);
			throw new ModuleException(e.getExceptionCode(), "messaging.desc");
		}
	}

	public static Collection<Integer> getPNRSegIDsOfAlerts(Collection<Integer> colAlertIDs) throws ModuleException {
		try {
			return lookupAlertingDAO().getPNRSegIDsOfAlerts(colAlertIDs);
		} catch (CommonsDataAccessException e) {
			log.error("Getting PNR segment IDs of alerts is failed.", e);
			throw new ModuleException(e.getExceptionCode(), "messaging.desc");
		}
	}

	private static AlertingDAO lookupAlertingDAO() {
		return (AlertingDAO) AlertingModuleUtils.getInstance().getLocalBean(AlertingInternalConstants.DAOProxyNames.ALERTING_DAO);
	}

	private static AlertingJdbcDAO getAlertingJdbcDAO() {
		return (AlertingJdbcDAO) AlertingModuleUtils.getInstance().getLocalBean(
				AlertingInternalConstants.DAOProxyNames.ALERTING_JDBC_DAO);
	}

	public static Page retrieveFlightPnrDetailsForSearchCriteria(FlightPnrNotificationDTO searchCriteria) throws ModuleException {
		try {
			return getAlertingJdbcDAO().retrieveFlightPnrDetailsForSearchCriteria(searchCriteria);
		} catch (CommonsDataAccessException e) {
			log.error("Retrieving Flight Pnr Details for search criteria is failed.", e);
			throw new ModuleException(e.getExceptionCode(), "messaging.desc");
		}
	}

	public static Page retrieveFlightPnrDetailsForViewSearchCriteria(FlightPnrNotificationDTO searchCriteria)
			throws ModuleException {
		try {
			return getAlertingJdbcDAO().retrieveFlightPnrDetailsForViewSearchCriteria(searchCriteria);
		} catch (CommonsDataAccessException e) {
			log.error("Retrieving Flight Pnr Details for search criteria is failed.", e);
			throw new ModuleException(e.getExceptionCode(), "messaging.desc");
		}
	}

	/**
	 * method to notify reservations
	 * 
	 * 
	 */
	public static void notifyReservations(int flightId, FlightNotificationDTO fltDTO, Collection colFlightPnrDTO,
			UserPrincipal userPrincipal) throws ModuleException {
		try {
			NotifyReservations notifyReservations = new NotifyReservations(flightId, fltDTO, colFlightPnrDTO, userPrincipal);
			notifyReservations.generateCustomerNotifications();
		} catch (CommonsDataAccessException e) {
			log.error("notifying reservations is failed.", e);
			throw new ModuleException(e.getExceptionCode(), "messaging.desc");
		}

	}

	public static String getNextNotificationCode(String baseCode) throws ModuleException {
		try {
			return getAlertingJdbcDAO().getNextNotificationCode(baseCode);
		} catch (CommonsDataAccessException e) {
			log.error("get notification code for flight notification failed.", e);
			throw new ModuleException(e.getExceptionCode(), "messaging.desc");
		}
	}

	public static ArrayList getNotificationsForPnr(int pnrSegId) throws ModuleException {
		try {
			return lookupAlertingDAO().getNotificationsForPnr(pnrSegId);
		} catch (CommonsDataAccessException e) {
			log.error("get notifications for pnr is failed.", e);
			throw new ModuleException(e.getExceptionCode(), "messaging.desc");
		}
	}

	public static int saveFlightNotification(FlightNotification obj) throws ModuleException {
		try {
			return lookupAlertingDAO().saveFlightNotification(obj);
		} catch (CommonsDataAccessException e) {
			log.error("save notifications for flight is failed.", e);
			throw new ModuleException(e.getExceptionCode(), "messaging.desc");
		}
	}

	public static void saveFlightPnrNotification(FlightPnrNotification obj) throws ModuleException {
		try {
			lookupAlertingDAO().saveFlightPnrNotification(obj);
		} catch (CommonsDataAccessException e) {
			log.error("save notifications for pnr is failed.", e);
			throw new ModuleException(e.getExceptionCode(), "messaging.desc");
		}
	}

	public static ArrayList getAllNotificationsForFlight(int flightId, String pnr) throws ModuleException {
		try {
			return lookupAlertingDAO().getAllNotificationsForFlight(flightId, pnr);
		} catch (CommonsDataAccessException e) {
			log.error("get all notifications for flight is failed.", e);
			throw new ModuleException(e.getExceptionCode(), "messaging.desc");
		}
	}

	public static Page retrieveDetailedNotificationList(int fltNotificationId, Date notifyDate, String pnr) throws ModuleException {
		try {
			return getAlertingJdbcDAO().retrieveDetailedNotificationList(fltNotificationId, notifyDate, pnr);
		} catch (CommonsDataAccessException e) {
			log.error("Retrieving detailed notification list is failed.", e);
			throw new ModuleException(e.getExceptionCode(), "messaging.desc");
		}
	}

	public static Integer getFlightID(String fltNum, Date depDateLocal) throws ModuleException {
		try {
			return lookupAlertingDAO().getFlightID(fltNum, depDateLocal);
		} catch (CommonsDataAccessException e) {
			log.error("Retrieving flight id is failed.", e);
			throw new ModuleException(e.getExceptionCode(), "messaging.desc");
		}
	}

    public static List<Integer> getFlightIDList(String fltNum, Date startDateLocal,Date endDateLocal) throws ModuleException {
		try {
			return lookupAlertingDAO().getFlightIDList(fltNum, startDateLocal, endDateLocal);
		} catch (CommonsDataAccessException e) {
			log.error("Retrieving flight id is failed.", e);
			throw new ModuleException(e.getExceptionCode(), "messaging.desc");
		}
	}
	
	public static Page retrieveSubmittedRequests(QueueRequestDetailsDTO searchCriteria) throws ModuleException{
		
		try {
			return getAlertingJdbcDAO().retrieveSubmittedRequests(searchCriteria);
		} catch (CommonsDataAccessException e) {
			log.error("Retrieving submitted request details failed.", e);
			throw new ModuleException(e.getExceptionCode(), "messaging.desc");
		}
		
	}
	
	public static void submitOrEditRequest(QueueRequest queueRequest) throws ModuleException{
		
		try{
			
			lookupAlertingDAO().submitOrEditRequest(queueRequest);
			
		}catch (CommonsDataAccessException e) { 
			
			log.error("submitting the request has failed.", e);
			throw new ModuleException(e.getExceptionCode(), "messaging.desc");
			
		} 	
		
	}
	
	public static QueueRequest getQueueRequest(int requestId) throws ModuleException{
		
		try{
			
			return lookupAlertingDAO().getQueueRequest(requestId);
			
		}catch (CommonsDataAccessException e) { 
			
			log.error("getting request by request id has failed.", e);
			throw new ModuleException(e.getExceptionCode(), "messaging.desc");
			
		} 
		
	}
	
	public static void addRequestHistory(RequestHistory requestHistory) throws ModuleException{
		
		try{
			
			lookupAlertingDAO().addRequestHistory(requestHistory);
			
		}catch (CommonsDataAccessException e) { 
			
			log.error("adding history of a request has failed.", e);
			throw new ModuleException(e.getExceptionCode(), "messaging.desc");
			
		} 
		
	}
	
	public static void deleteRequest(int requestId) throws ModuleException{
		
		try{
			
			lookupAlertingDAO().deleteRequest(requestId);
			
		}catch (CommonsDataAccessException e) { 
			
			log.error("adding history of a request has failed.", e);
			throw new ModuleException(e.getExceptionCode(), "messaging.desc");
			
		} 
		
	}
	
	public static Page getRequestHistory(int requestId) throws ModuleException{
		
		try {
			return getAlertingJdbcDAO().getRequestHistory(requestId);
		} catch (CommonsDataAccessException e) {
			log.error("Retrieving request history details failed.", e);
			throw new ModuleException(e.getExceptionCode(), "messaging.desc");
		}
		
	}
	
	public static Page retrieveSubmittedRequestsForAction(String userID , String queueId , String status) throws ModuleException{
		
		try {
			return getAlertingJdbcDAO().retrieveSubmittedRequestsForAction(userID , queueId , status);
		} catch (CommonsDataAccessException e) {
			log.error("Retrieving submitted request details failed.", e);
			throw new ModuleException(e.getExceptionCode(), "messaging.desc");
		}
		
	}
	
public static ServiceResponce deleteRequestQueue(int queueId) throws ModuleException{
		
		try{
			
			return lookupAlertingDAO().deleteRequestQueue(queueId);
			
		}catch (CommonsDataAccessException e) { 
			
			log.error("Delete request queue has failed.", e);
			throw new ModuleException(e.getExceptionCode(), "messaging.desc");
			
		} 
		
	}
	
	public static Page searchRequestQueue(int page, int maxResult,String queueName, String applicablePage) throws ModuleException{
		
		try{
			
			return lookupAlertingDAO().searchRequestQueue(page,maxResult,queueName,applicablePage);
			
		}catch (CommonsDataAccessException e) { 
			
			log.error("Searching request queue has failed.", e);
			throw new ModuleException(e.getExceptionCode(), "messaging.desc");
			
		} 
		
	}
	
	public static List<ApplicablePage> searchPages(String pageNamePart) throws ModuleException{
		
		try{
			
			return lookupAlertingDAO().searchPages(pageNamePart);
			
		}catch (CommonsDataAccessException e) { 
			
			log.error("Searching pages has failed.", e);
			throw new ModuleException(e.getExceptionCode(), "messaging.desc");
			
		} 
		
	}  
	
	public static List<RequestQueueUser> searchUser(String userNamePart) throws ModuleException{
		
		try{
			
			return lookupAlertingDAO().searchUser(userNamePart);
			
		}catch (CommonsDataAccessException e) { 
			
			log.error("Searching users has failed.", e);
			throw new ModuleException(e.getExceptionCode(), "messaging.desc");
			
		} 
		
	} 
	
	public static void updateRequestQueue(Queue queue, List<String> pageList) throws ModuleException{
		
		try{
			
			lookupAlertingDAO().updateRequestQueue(queue,pageList);
			
		}catch (CommonsDataAccessException e) { 
			
			log.error("Updating request queue has failed.", e);
			throw new ModuleException(e.getExceptionCode(), "messaging.desc");
			
		} 
		
	}
	
	public static void saveRequestQueue(Queue queue,List<String> pagelist,List<String> add) throws ModuleException{
		
		try{
			
			lookupAlertingDAO().saveRequestQueue(queue,pagelist,add);
			
		}catch (CommonsDataAccessException e) { 
			
			log.error("Creating a new request queue has failed.", e);
			throw new ModuleException(e.getExceptionCode(), "messaging.desc");
			
		} 
		
	}
	
	public static void addRemoveUsers(List<String> add,List<String> remove,int queue) throws ModuleException{
		
		try{
			
			lookupAlertingDAO().addRemoveUsers(add,remove,queue);
			
		}catch (CommonsDataAccessException e) { 
			
			log.error("Add/Remove users has failed.", e);
			throw new ModuleException(e.getExceptionCode(), "messaging.desc");
			
		} 
		
	}
	
	public static Queue readRequestQueue(int queue) throws ModuleException{
		
		try{
			
			return lookupAlertingDAO().readRequestQueue(queue);
			
		}catch (CommonsDataAccessException e) { 
			
			log.error("Read a request queue has failed.", e);
			throw new ModuleException(e.getExceptionCode(), "messaging.desc");
			
		} 
		
	}
	
	public static Page getRequestStatusForPnr(int page, int maxResult,String pnrId)throws ModuleException{
		
		try{
			
			return lookupAlertingDAO().getRequestStatusForPnr(page,maxResult,pnrId);
			
			
		}catch (Exception e) { 
			
			log.error("Reading Request Queue Status has failed.", e);
			throw new ModuleException(e.getMessage(), "messaging.desc");
			
		} 
		
	}
	
	public static Page getHistoryForRequest(int page, int maxResult,int requestId) throws ModuleException{
		
		try{
			
			return lookupAlertingDAO().getHistoryForRequest(page,maxResult,requestId);
			
			
		}catch (Exception e) { 
			
			log.error("Reading Request History has failed.", e);
			throw new ModuleException(e.getMessage(), "messaging.desc");
			
		} 
		
	}

	public static Page retrieveFlightDetailsForSearchCriteria(FlightPnrSearchNotificationDTO searchCriteria)
			throws ModuleException {
		try {
			return getAlertingJdbcDAO().retrieveFlightDetailsForSearchCriteria(searchCriteria);
		} catch (CommonsDataAccessException e) {
			log.error("Retrieving Flight Details for search criteria is failed.", e);
			throw new ModuleException(e.getExceptionCode(), "messaging.desc");
		}
	}

	public static Page retrieveMessageListForLoadCriteria(Date depDate) throws ModuleException {
		try {
			return getAlertingJdbcDAO().retrieveMessageListForLoadCriteria(depDate);
		} catch (CommonsDataAccessException e) {
			log.error("Retrieving Message Details for load criteria is failed.", e);
			throw new ModuleException(e.getExceptionCode(), "messaging.desc");
		}
	}

}
