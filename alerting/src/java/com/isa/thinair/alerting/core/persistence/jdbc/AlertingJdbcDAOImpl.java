package com.isa.thinair.alerting.core.persistence.jdbc;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import oracle.sql.CLOB;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.alerting.api.dto.AlertDetailDTO;
import com.isa.thinair.alerting.api.dto.FlightPnrNotificationDTO;
import com.isa.thinair.alerting.api.dto.FlightPnrSearchNotificationDTO;
import com.isa.thinair.alerting.api.dto.QueueRequestDTO;
import com.isa.thinair.alerting.api.dto.QueueRequestDetailsDTO;
import com.isa.thinair.alerting.api.dto.RequestHistoryDTO;
import com.isa.thinair.alerting.api.model.Alert;
import com.isa.thinair.alerting.core.bl.AlertingServicesBL;
import com.isa.thinair.alerting.core.persistence.dao.AlertingJdbcDAO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.commons.core.framework.PlatformBaseJdbcDAOSupport;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.StringUtil;

public class AlertingJdbcDAOImpl extends PlatformBaseJdbcDAOSupport implements AlertingJdbcDAO {

	private final Log log = LogFactory.getLog(AlertingJdbcDAOImpl.class);

	private static final String ALERTS_FOR_USER = "ALERTS_FOR_USER";

	@Override
	public List retrieveAlertsForUser(String userId) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		String query = getQuery(ALERTS_FOR_USER);
		return (List) jdbcTemplate.query(query, new String[] { userId }, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List list = new ArrayList();
				Alert alert;
				if (rs != null) {
					while (rs.next()) {
						alert = new Alert();
						alert.setAlertId(new Long(rs.getLong("alert_id")));
						alert.setAlertTypeId(new Long(rs.getLong("alert_type_id")));
						alert.setContent(rs.getString("content"));
						alert.setPnrSegId(new Long(rs.getLong("pnr_seg_id")));
						alert.setPriorityCode(new Long(rs.getLong("priority_code")));
						alert.setStatus(rs.getString("status"));
						alert.setTimestamp(rs.getDate("timestamp"));
						alert.setVersion(rs.getLong("version"));
						alert.setOriginalDepDate(rs.getDate("original_dep_date"));
						list.add(alert);
					}
				}
				log.debug("list.size() = " + list.size());
				return list;
			}
		});
	}

	/* added by indika */
	/**
	 * Serch criteria used for Paging
	 */
	@Override
	public Page retrieveAlertsForSearchCriteria(AlertDetailDTO searchCriteria) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());

		Integer alertCount = getAlertsCountingSQL(searchCriteria);

		Integer allAlertCount = getAlertsCountingSQL(null);

		if (searchCriteria.getStartRec() > alertCount) {
			int pageSize = searchCriteria.getEndRec() - searchCriteria.getStartRec();
			searchCriteria.setStartRec(searchCriteria.getStartRec() - (pageSize + 1));
			searchCriteria.setEndRec(searchCriteria.getStartRec() + pageSize);
		}

		if (searchCriteria.getEndRec() > alertCount) {
			searchCriteria.setEndRec(alertCount);
		}

		return new Page((alertCount == null ? 0 : alertCount.intValue()), searchCriteria.getStartRec(),
				searchCriteria.getEndRec(), (allAlertCount == null ? 0 : allAlertCount.intValue()),
				(Collection) jdbcTemplate.query(new RetreiveAlertsPrepareStmtCreator(searchCriteria),
						new RetreiveAlertsResultSetExtractor()));

	}

	private Integer getAlertsCountingSQL(AlertDetailDTO criteria) {

		List values = new ArrayList();
		List criteriaList = new ArrayList();
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		StringBuilder sql = new StringBuilder();

		sql.append(" SELECT count(*) TotRecs " + " FROM t_alert alert, t_alert_type alertType, "
				+ " t_pnr_segment pnrSeg, t_flight_segment flgSeg, t_flight flg");

		if (criteria != null && criteria.getAgentCode() != null)
			sql.append(", t_reservation res ");

		sql.append(" WHERE alert.alert_type_id = alertType.alert_type_id ");

		if (criteria != null && criteria.getAgentCode() != null)
			sql.append(" AND pnrSeg.pnr = res.pnr ");

		sql.append(" AND alert.pnr_seg_id = pnrSeg.pnr_seg_id " + " AND pnrSeg.flt_seg_id = flgSeg.flt_seg_id "
				+ " AND flgSeg.flight_id = flg.flight_id ");

		if (criteria != null) {
			if ((criteria.getFromDate() != null) && (criteria.getToDate() != null)) {
				criteriaList.add(" (alert.timestamp BETWEEN ? AND ? )");
				values.add(new java.sql.Date(criteria.getFromDate().getTime()));
				values.add(new java.sql.Date(criteria.getToDate().getTime() + 24 * 60 * 60 * 1000));
			}
			if ((criteria.getFlightNumber() != null) && (!criteria.getFlightNumber().trim().equals(""))) {
				criteriaList.add(" flg.flight_number = ? ");
				values.add(criteria.getFlightNumber());
			}

			if ((criteria.getFlightStatus() != null) && (!criteria.getFlightStatus().trim().equals(""))) {
				criteriaList.add(" flg.status = ? ");
				values.add(criteria.getFlightStatus());
			}

			if (criteria.isCheckForRescheduled()) {
				criteriaList.add(" flg.manually_changed = 'Y' AND flg.schedule_id is not null ");
			}

			if (criteria.getDepDate() != null) {
				criteriaList.add(" flgSeg.est_time_departure_local >= ? AND flgSeg.est_time_departure_local < ?");
				values.add(new java.sql.Date(criteria.getDepDate().getTime()));
				values.add(new java.sql.Date(criteria.getDepDate().getTime() + 24 * 60 * 60 * 1000));

			}
			if (criteria.getOriginDepDate() != null) {
				criteriaList.add(" alert.original_dep_date >= ? AND alert.original_dep_date < ?");
				values.add(new java.sql.Date(criteria.getOriginDepDate().getTime()));
				values.add(new java.sql.Date(criteria.getOriginDepDate().getTime() + 24 * 60 * 60 * 1000));
			}

			if (criteria.getOriginAirport() != null) {
				criteriaList.add(" flg.origin = ? ");
				values.add(criteria.getOriginAirport());
			}

			if (!criteria.isViewAnyAlert()) {
				if (criteria.isViewReptAgentAlert()) {
					String repAgentsSql = "SELECT a.agent_code FROM t_agent a WHERE a.agent_code = ? "
							+ " UNION ALL "
							+ " SELECT agent_code FROM T_AGENT WHERE REPORT_TO_GSA='Y' "
							+ "	AND GSA_CODE IN(SELECT AG.AGENT_CODE FROM T_AGENT AG WHERE AG.AGENT_TYPE_CODE='GSA' AND AG.AGENT_CODE=?)";

					criteriaList.add(" res.owner_agent_code in (" + repAgentsSql + ") ");
					values.add(criteria.getAgentCode());
					values.add(criteria.getAgentCode());

				} else if (criteria.isViewOwnAlert()) {
					criteriaList.add(" res.owner_agent_code = ? ");
					values.add(criteria.getAgentCode());
				}
			}

			for (int i = 0; i < criteriaList.size(); i++) {
				sql.append(" AND ");
				sql.append(criteriaList.get(i));
			}
		}
		Integer alertCount = jdbcTemplate.queryForObject(sql.toString(), values.toArray(), Integer.class);

		return alertCount;
	}

	public class RetreiveAlertsPrepareStmtCreator implements PreparedStatementCreator {
		private final String queryString;

		private final List queryValues;

		public RetreiveAlertsPrepareStmtCreator(AlertDetailDTO criteria) {
			List values = new ArrayList();
			List criteriaList = new ArrayList();

			StringBuilder sql = new StringBuilder();

			sql.append("SELECT b.alert_id, b.timestamp, b.content, " + " b.privilege_id, b.pnr, "
					+ " b.est_time_departure_local, b.flight_number,b.original_dep_date, b.originator_pnr  FROM (");
			sql.append("SELECT a.*, rownum rid FROM ( ");

			sql.append(" SELECT alert.alert_id, alert.timestamp, alert.content, " + " alertType.privilege_id, pnrSeg.pnr, "
					+ " flgSeg.est_time_departure_local, flg.flight_number,alert.original_dep_date, res.originator_pnr"
					+ " FROM t_alert alert, t_alert_type alertType, "
					+ " t_pnr_segment pnrSeg, t_flight_segment flgSeg, t_flight flg, t_reservation res  "
					+ " WHERE alert.alert_type_id = alertType.alert_type_id " + " AND pnrSeg.pnr = res.pnr "
					+ " AND alert.pnr_seg_id = pnrSeg.pnr_seg_id " + " AND pnrSeg.flt_seg_id = flgSeg.flt_seg_id "
					+ " AND flgSeg.flight_id = flg.flight_id ");

			if ((criteria.getFromDate() != null) && (criteria.getToDate() != null)) {
				criteriaList.add(" (alert.timestamp BETWEEN ? AND ? )");
				values.add(criteria.getFromDate());
				values.add(new Date(criteria.getToDate().getTime() + 24 * 60 * 60 * 1000));

			}

			if ((criteria.getFlightNumber() != null) && (!criteria.getFlightNumber().trim().equals(""))) {
				criteriaList.add(" flg.flight_number = ? ");
				values.add(criteria.getFlightNumber());
			}

			if ((criteria.getFlightStatus() != null) && (!criteria.getFlightStatus().trim().equals(""))) {
				criteriaList.add(" flg.status = ? ");
				values.add(criteria.getFlightStatus());
			}

			if (criteria.isCheckForRescheduled()) {
				criteriaList.add(" flg.manually_changed = 'Y' AND flg.schedule_id is not null ");
			}

			if (criteria.getDepDate() != null) {
				criteriaList.add(" flgSeg.est_time_departure_local >= ? AND flgSeg.est_time_departure_local < ? ");

				values.add(criteria.getDepDate());
				values.add(new Date(criteria.getDepDate().getTime() + 24 * 60 * 60 * 1000));

			}
			if (criteria.getOriginDepDate() != null) {
				criteriaList.add(" alert.original_dep_date >= ? AND alert.original_dep_date < ?");
				values.add(new java.sql.Date(criteria.getOriginDepDate().getTime()));
				values.add(new java.sql.Date(criteria.getOriginDepDate().getTime() + 24 * 60 * 60 * 1000));
			}

			if (criteria.getOriginAirport() != null) {
				criteriaList.add(" flg.origin = ? ");
				values.add(criteria.getOriginAirport());
			}

			if (criteria.getAnciReprotectStatus() != null) {
				if (FlightPnrNotificationDTO.SEARCH_REPROTECT_ANCI_FAILED.equals(criteria.getAnciReprotectStatus())) {
					criteriaList
							.add(" (pnrSeg.MEAL_REPROTECT_STATUS = 1 OR pnrSeg.SEAT_REPROTECT_STATUS = 1 OR pnrseg.BAGGAGE_REPROTECT_STATUS = 1) ");
				} else if (FlightPnrNotificationDTO.SEARCH_REPROTECT_ANCI_MEAL_FAILED.equals(criteria.getAnciReprotectStatus())) {
					criteriaList.add(" pnrSeg.MEAL_REPROTECT_STATUS = 1 ");
				} else if (FlightPnrNotificationDTO.SEARCH_REPROTECT_ANCI_SEAT_FAILED.equals(criteria.getAnciReprotectStatus())) {
					criteriaList.add(" pnrSeg.SEAT_REPROTECT_STATUS = 1 ");
				} else if (FlightPnrNotificationDTO.SEARCH_REPROTECT_ANCI_BAGGAGE_FAILED
						.equals(criteria.getAnciReprotectStatus())) {
					criteriaList.add(" pnrseg.BAGGAGE_REPROTECT_STATUS = 1 ");
				}
			}

			if (!criteria.isViewAnyAlert()) {
				if (criteria.isViewReptAgentAlert()) {
					String repAgentsSql = "SELECT a.agent_code FROM t_agent a WHERE a.agent_code = ? "
							+ " UNION ALL "
							+ " SELECT agent_code FROM T_AGENT WHERE REPORT_TO_GSA='Y' "
							+ "	AND GSA_CODE IN(SELECT AG.AGENT_CODE FROM T_AGENT AG WHERE AG.AGENT_TYPE_CODE='GSA' AND AG.AGENT_CODE=?)";

					criteriaList.add(" res.owner_agent_code in (" + repAgentsSql + ") ");
					values.add(criteria.getAgentCode());
					values.add(criteria.getAgentCode());

				} else if (criteria.isViewOwnAlert()) {
					criteriaList.add(" res.owner_agent_code = ? ");
					values.add(criteria.getAgentCode());
				}
			}

			for (int i = 0; i < criteriaList.size(); i++) {
				sql.append(" AND ");
				sql.append(criteriaList.get(i));
			}
			sql.append(" ORDER BY alert.timestamp DESC,pnrSeg.pnr,flg.flight_number,flgSeg.est_time_departure_local,alert.content ");

			/*
			 * if (criteria.getTotalRecs() >= 1) { sql.append(") WHERE ROWNUM <= ? "); values.add(new
			 * Integer(criteria.getTotalRecs())); }
			 */

			sql.append(")  a ) b WHERE b.rid BETWEEN ? ");
			values.add(new Integer(criteria.getStartRec()));

			sql.append("AND ? ");
			values.add(new Integer(criteria.getEndRec()));

			this.queryString = sql.toString();
			this.queryValues = values;

			log.debug("Retrieve Alerts - Query String [ " + queryString + " ]");
			log.debug("Retrieve Alerts - Query Values " + queryValues);
		}

		@Override
		public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
			PreparedStatement prepStmt = con.prepareStatement(queryString);
			for (int i = 0; i < queryValues.size(); i++) {
				Object value = queryValues.get(i);
				if (value instanceof String) {
					prepStmt.setString((i + 1), (String) value);
				} else if (value instanceof Integer) {
					prepStmt.setInt((i + 1), ((Integer) value).intValue());
				} else if (value instanceof Long) {
					prepStmt.setLong((i + 1), ((Long) value).longValue());
				} else if (value instanceof Date) {
					prepStmt.setDate((i + 1), new java.sql.Date(((Date) value).getTime()));
				}
			}
			return prepStmt;
		}
	}

	public class RetreiveAlertsResultSetExtractor implements ResultSetExtractor {
		@Override
		public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
			List list = new ArrayList();
			AlertDetailDTO alertDetailDTO;
			if (rs != null) {
				while (rs.next()) {
					alertDetailDTO = new AlertDetailDTO();
					alertDetailDTO.setAlertId(new Long(rs.getLong("alert_id")));
					alertDetailDTO.setAlertDate(rs.getDate("timestamp"));
					alertDetailDTO.setContent(rs.getString("content"));
					alertDetailDTO.setPrivilege(rs.getString("privilege_id"));
					alertDetailDTO.setPnr(rs.getString("pnr"));
					alertDetailDTO.setDepDate(rs.getDate("est_time_departure_local"));
					alertDetailDTO.setFlightNumber(rs.getString("flight_number"));
					alertDetailDTO.setOriginDepDate(rs.getDate("original_dep_date"));
					// Add new attribute for checking interline reservation
					alertDetailDTO.setOriginatorPnr(rs.getString("originator_pnr"));
					list.add(alertDetailDTO);
				}
			}
			log.debug("list.size() = " + list.size());
			return list;
		}
	}

	/* added by Dhanya */

	@Override
	public Page retrieveFlightPnrDetailsForSearchCriteria(FlightPnrNotificationDTO searchCriteria) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());

		Collection colObj = (Collection) jdbcTemplate.query(new RetreiveFlightPnrDetailsPrepareStmtCreator(searchCriteria),
				new RetreiveFlightPnrDetailsResultSetExtractor());

		int recordCnt = (colObj == null) ? 0 : colObj.size();
		getInBoundConnectionList(colObj);
		return new Page(recordCnt, 1, recordCnt, recordCnt, colObj);

	}

	private void getInBoundConnectionList(Collection col) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		Iterator it = null;

		final String CurrentPNRSegmentDepartureTime = "CurrDeptTime";
		final String CurrentPNRSegmentDestination = "CurrDestCode";
		final String InBoundFlightNo = "InBoundFlightNo";
		final String InBoundDepartureDate = "InBoundDepartureDate";
		final String InBoundDepartureStation = "InBoundDepartureStation";

		String[] minMaxTransitDurations = null;
		it = col.iterator();

		while (it.hasNext()) {
			FlightPnrNotificationDTO fpnDTO = (FlightPnrNotificationDTO) it.next();
			String pnr = fpnDTO.getPnr();
			int flightID = fpnDTO.getFlightId();
			String departureStation = fpnDTO.getRoute().substring(0, 3);

			try {
				minMaxTransitDurations = CommonsServices.getGlobalConfig()
						.getMixMaxTransitDurations(departureStation, null, null);
			} catch (ModuleRuntimeException ex) {
				if ("commons.data.transitdurations.notconfigured".equalsIgnoreCase(ex.getExceptionCode())) {
					return;
				}
			}

			String[] minTransitHoursMin = minMaxTransitDurations[0].split(":");
			String[] maxTransitHoursMin = minMaxTransitDurations[1].split(":");

			Double minConnectTimeInMins = (Double.valueOf(minTransitHoursMin[1]) / 60);
			Double minConnectTimeInHours = Double.valueOf(minTransitHoursMin[0]);
			Double maxConnectTimeInMins = (Double.valueOf(maxTransitHoursMin[1]) / 60);
			Double maxConnectTimeInHours = Double.valueOf(maxTransitHoursMin[0]);

			Double minConnectTime = (minConnectTimeInHours + minConnectTimeInMins) * -1;
			Double maxConnectTime = (maxConnectTimeInHours + maxConnectTimeInMins) * -1;

			final double doubleMinConnectTime = (minConnectTime).doubleValue();
			final double negativeMaxConnectTime = (maxConnectTime).doubleValue();

			StringBuilder sql1 = new StringBuilder();
			sql1.append("SELECT * FROM");
			sql1.append(" (SELECT TO_CHAR(s.EST_TIME_DEPARTURE_LOCAL, 'DD-MON-YYYY HH24:MI:SS') DEPARTURE_LOCAL,");
			sql1.append(" SUBSTR(s.segment_code,LENGTH(s.segment_code) - 2,3) destination");
			sql1.append(" FROM V_PNR_SEGMENT s");
			sql1.append(" WHERE s.pnr ='" + pnr + "'");
			sql1.append(" AND s.segment_code LIKE '" + departureStation + "/%'");
			sql1.append(" AND s.pnr_status <> 'CNX'");
			sql1.append(" and s.flight_id = '" + flightID + "'");
			sql1.append(" order by SEGMENT_SEQ )");
			sql1.append(" where rownum = 1");

			HashMap currentPNRSegmentRecord = (HashMap) jdbcTemplate.query(sql1.toString(), new ResultSetExtractor() {

				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					HashMap<String, String> currentPNRSegmentRecord = new HashMap<String, String>();
					if (rs.next()) {
						currentPNRSegmentRecord.put(CurrentPNRSegmentDepartureTime, rs.getString("DEPARTURE_LOCAL"));
						currentPNRSegmentRecord.put(CurrentPNRSegmentDestination, rs.getString("destination"));
					}
					return currentPNRSegmentRecord;
				}
			});

			StringBuilder sql2 = new StringBuilder();
			sql2.append("SELECT s.FLIGHT_NUMBER,s.SEGMENT_CODE,s.EST_TIME_DEPARTURE_LOCAL,");
			sql2.append(" (s.EST_TIME_ARRIVAL_LOCAL - TO_DATE('" + currentPNRSegmentRecord.get(CurrentPNRSegmentDepartureTime)
					+ "','DD-MON-YYYY HH24:MI:SS'))*24 connectTime");
			sql2.append(" FROM V_EXT_PNR_SEGMENT s,t_pnr_segment ps,t_flight_segment fs,t_flight f");
			sql2.append(" WHERE s.pnr = '" + pnr + "'");
			sql2.append(" AND ps.pnr_seg_id(+)    = s.pnr_seg_id");
			sql2.append(" AND ps.flt_seg_id       = fs.flt_seg_id(+)");
			sql2.append(" AND fs.flight_id        = f.flight_id(+)");
			sql2.append(" AND (s.pnr_seg_id       = -1");
			sql2.append(" OR f.operation_type_id != '5' )");
			sql2.append(" AND s.segment_code LIKE '%/" + departureStation + "'");
			sql2.append(" and s.pnr_status <> 'CNX'");
			sql2.append(" order by s.EST_TIME_ARRIVAL_LOCAL desc");

			HashMap inBoundConnectionRecord = (HashMap) jdbcTemplate.query(sql2.toString(), new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

					HashMap<String, String> inBoundConnectionRecord = new HashMap<String, String>();
					while (rs.next()) {
						if ((rs.getDouble("connectTime") < 0) && (rs.getDouble("connectTime") >= negativeMaxConnectTime)
								&& (rs.getDouble("connectTime") <= doubleMinConnectTime)) {
							/**
							 * Bussiness Rule: Consider only latest arriving flights within maximum connection time
							 */
							inBoundConnectionRecord.put(InBoundFlightNo, rs.getString("FLIGHT_NUMBER"));
							inBoundConnectionRecord.put(InBoundDepartureDate, rs.getDate("EST_TIME_DEPARTURE_LOCAL").toString());
							inBoundConnectionRecord.put(InBoundDepartureStation, rs.getString("SEGMENT_CODE"));
							return inBoundConnectionRecord;
						}
					}
					return null;
				}
			});
			if (inBoundConnectionRecord != null) {
				fpnDTO.setInBoundFlightNo((String) inBoundConnectionRecord.get(InBoundFlightNo));
				fpnDTO.setInBoundDepartureDate((String) inBoundConnectionRecord.get(InBoundDepartureDate));
				fpnDTO.setInBoundDepartureStation((String) inBoundConnectionRecord.get(InBoundDepartureStation));
			}

		}

	}

	// Added by Dhanya

	public class RetreiveFlightPnrDetailsPrepareStmtCreator implements PreparedStatementCreator {

		private final String queryString;

		private final List queryValues;

		public RetreiveFlightPnrDetailsPrepareStmtCreator(FlightPnrNotificationDTO criteria) {
			List values = new ArrayList();
			List criteriaList = new ArrayList();

			StringBuilder sql = new StringBuilder();

			sql.append("select a.*, rownum rid,alt.timestamp from ( ");
			sql.append("select res.Pnr, rCon.C_Mobile_No, rCon.c_email, rCon.c_phone_no, rCon.c_title, rCon.c_first_name, fltseg.segment_code,");
			sql.append("flt.flight_id, pnrseg.pnr_seg_id, flt.flight_number, pnrseg.alert_flag, fltseg.est_time_departure_local,  ");
			sql.append(" (initcap(rCon.C_First_Name) || ' ' || initcap(rCon.C_last_Name)) As Name,");
			sql.append(" (res.total_pax_count + res.total_pax_child_count) As TotalPax,");
			sql.append(" res.total_pax_count,res.total_pax_child_count,res.total_pax_infant_count");
			sql.append(" from T_Reservation res, t_reservation_contact rCon, T_Pnr_Segment pnrseg, t_flight_segment fltseg, t_flight flt");
			sql.append(" where res.PNR=rCon.PNR AND pnrseg.Pnr = res.pnr " + " and fltseg.flt_seg_id = pnrseg.flt_seg_id "
					+ " and fltseg.flight_id = flt.flight_id ");

			// if(criteria.getPnr() == null)
			// {
			if ((criteria.getFlightNumber() != null) && (!criteria.getFlightNumber().trim().equals(""))) {
				criteriaList.add(" flt.flight_number = ? ");
				values.add(criteria.getFlightNumber());
			}
			if (criteria.getDepDate() != null) {
				// criteriaList.add(" fltSeg.est_time_departure_local = ?");
				// values.add(criteria.getDepDate());
				criteriaList.add(" fltSeg.est_time_departure_local >= ? AND fltSeg.est_time_departure_local < ?");
				values.add(new java.sql.Date(criteria.getDepDate().getTime()));
				values.add(new java.sql.Date(criteria.getDepDate().getTime() + 24 * 60 * 60 * 1000));
			}

			// }
			if (criteria.getPnr() != null) {
				criteriaList.add(" res.Pnr = ?");

				values.add(criteria.getPnr());
			}

			if (criteria.getNoOfAdults() != -1) {
				criteriaList.add(" res.total_pax_count = ?");
				values.add(criteria.getNoOfAdults());
			}

			if (criteria.getNoOfChildren() != -1) {
				criteriaList.add(" res.total_pax_child_count = ?");
				values.add(criteria.getNoOfChildren());
			}

			if (criteria.getNoOfInfants() != -1) {
				criteriaList.add(" res.total_pax_infant_count = ?");
				values.add(criteria.getNoOfInfants());
			}

			if (criteria.getSearchMobileOption() != null && !"".equals(criteria.getSearchMobileOption())) {
				if (criteria.getSearchMobileOption().equals(FlightPnrNotificationDTO.SEARCH_ALL_VALID))
					criteriaList.add(" replace(rCon.C_Mobile_No,'-') IS NOT NULL ");
				else if (criteria.getSearchMobileOption().equals(FlightPnrNotificationDTO.SEARCH_WITHOUT_EMAIL))
					criteriaList.add(" replace(rCon.C_Mobile_No,'-') IS NOT NULL And rCon.C_Email is null");

			}
			if (criteria.getSearchEmailOption() != null && !"".equals(criteria.getSearchEmailOption())) {
				if (criteria.getSearchEmailOption().equals(FlightPnrNotificationDTO.SEARCH_ALL_VALID))
					criteriaList.add(" rCon.C_Email IS NOT NULL ");
				else if (criteria.getSearchEmailOption().equals(FlightPnrNotificationDTO.SEARCH_WITHOUT_MOBILE))
					criteriaList.add(" rCon.C_Email IS NOT NULL And replace(rCon.C_Mobile_No,'-') is null");

			}
			if (criteria.getSearchLandPhoneOption() != null && !"".equals(criteria.getSearchEmailOption())) {
				if (criteria.getSearchLandPhoneOption().equals(FlightPnrNotificationDTO.SEARCH_ALL_VALID))
					criteriaList.add(" replace(rCon.C_phone_no,'-') IS NOT NULL ");
				else if (criteria.getSearchLandPhoneOption().equals(FlightPnrNotificationDTO.SEARCH_WITHOUT_MOBILE))
					criteriaList.add(" replace(rCon.C_phone_no,'-') IS NOT NULL And replace(rCon.C_Mobile_No,'-') is null");
				else if (criteria.getSearchLandPhoneOption().equals(FlightPnrNotificationDTO.SEARCH_WITHOUT_EMAIL))
					criteriaList.add(" replace(rCon.C_phone_no,'-') IS NOT NULL And rCon.C_Email is null");
				else if (criteria.getSearchLandPhoneOption().equals(FlightPnrNotificationDTO.SEARCH_WITHOUT_MOBILE_EMAIL))
					criteriaList
							.add(" replace(rCon.C_phone_no,'-') IS NOT NULL And rCon.C_Email is null And replace(rCon.C_Mobile_No,'-') is null ");

			}
			if (criteria.getAlert() != null) {
				if (criteria.getAlert().equals(FlightPnrNotificationDTO.WITH_ALERTS)) {
					criteriaList.add("pnrseg.alert_flag = 1 ");
				} else if (criteria.getAlert().equals(FlightPnrNotificationDTO.WITHOUT_ALERTS)) {
					criteriaList.add(" pnrseg.alert_flag = 0 ");
				}
			}

			if (criteria.getSearchReprotectAnciOption() != null) {
				if (FlightPnrNotificationDTO.SEARCH_REPROTECT_ANCI_FAILED.equals(criteria.getSearchReprotectAnciOption())) {
					criteriaList
							.add(" (pnrSeg.MEAL_REPROTECT_STATUS = 1 OR pnrSeg.SEAT_REPROTECT_STATUS = 1 OR pnrseg.BAGGAGE_REPROTECT_STATUS = 1) ");
				} else if (FlightPnrNotificationDTO.SEARCH_REPROTECT_ANCI_MEAL_FAILED.equals(criteria
						.getSearchReprotectAnciOption())) {
					criteriaList.add(" pnrseg.MEAL_REPROTECT_STATUS = 1 ");
				} else if (FlightPnrNotificationDTO.SEARCH_REPROTECT_ANCI_SEAT_FAILED.equals(criteria
						.getSearchReprotectAnciOption())) {
					criteriaList.add(" pnrseg.SEAT_REPROTECT_STATUS = 1 ");
				} else if (FlightPnrNotificationDTO.SEARCH_REPROTECT_ANCI_BAGGAGE_FAILED.equals(criteria
						.getSearchReprotectAnciOption())) {
					criteriaList.add(" pnrseg.BAGGAGE_REPROTECT_STATUS = 1 ");
				}
			}

			if (criteria.getMobileNumberPattern() != null) {
				String[] arrPattern = criteria.getMobileNumberPattern().split(";");
				StringBuffer sbPattern = new StringBuffer();
				if (arrPattern != null && arrPattern.length > 0) {
					sbPattern.append(" ( ");
					for (int var = 0; var < arrPattern.length; var++) {
						sbPattern.append(" replace(c_Mobile_no,'-') like '" + arrPattern[var] + "%'");
						if (var + 1 < arrPattern.length)
							sbPattern.append(" OR ");
					}
					sbPattern.append(" ) ");
				}
				criteriaList.add(sbPattern.toString());
			}

			if (!criteria.getPreferredLanguage().equals("")) {
				if (criteria.getLanguageOption().equals(FlightPnrNotificationDTO.LANG_INCLUDE)) {
					sql.append(" AND lower(rCon.PREFERRED_LANG) = lower('" + criteria.getPreferredLanguage() + "') ");
				} else if (criteria.getLanguageOption().equals(FlightPnrNotificationDTO.LANG_EXCLUDE)) {
					sql.append(" AND lower(rCon.PREFERRED_LANG) <> lower('" + criteria.getPreferredLanguage() + "') ");
				}
			}

			for (int i = 0; i < criteriaList.size(); i++) {
				sql.append(" AND ");
				sql.append(criteriaList.get(i));
			}
			sql.append(" And pnrseg.Status <> 'CNX' "); // should fetch only the Confirmed and On-Hold pnr

			if (criteria.getSegmentFrom() != null) {
				sql.append(" AND fltseg.segment_code LIKE '" + criteria.getSegmentFrom() + "/%'");
			}

			if (criteria.getSegmentTo() != null) {
				sql.append(" AND fltseg.segment_code LIKE '%/" + criteria.getSegmentTo() + "'");
			}

			if (!criteria.getSortBy().equals("")) {
				if (criteria.getSortBy().equalsIgnoreCase("PNR")) {
					sql.append(" order by res.pnr");
				} else if (criteria.getSortBy().equalsIgnoreCase("SEGMENT")) {
					sql.append(" order by flt.flight_id,fltseg.segment_code");
				} else if (criteria.getSortBy().equalsIgnoreCase("NOOFPAX")) {
					sql.append(" order by TotalPax");
				}
			}

			if (!criteria.getSortByOrder().equals("") && !criteria.getSortBy().equalsIgnoreCase("CREATEDDATE")) {
				if (criteria.getSortByOrder().equalsIgnoreCase("ASC")) {
					sql.append(" ASC");
				} else if (criteria.getSortByOrder().equalsIgnoreCase("DESC")) {
					sql.append(" DESC");
				}
			}

			sql.append(") a,t_alert alt");
			sql.append(" where a.pnr_seg_id  = alt.pnr_seg_id (+) ");

			if (criteria.getAlertCreatedFromDate() != null && criteria.getAlertCreatedToDate() != null) {
				sql.append(" AND ((alt.timestamp BETWEEN ? AND ? ) OR alt.timestamp is null)");
				values.add(new java.sql.Date(criteria.getAlertCreatedFromDate().getTime()));
				values.add(new java.sql.Date(criteria.getAlertCreatedToDate().getTime()));
			}

			if (criteria.getSortBy().equalsIgnoreCase("CREATEDDATE")) {
				sql.append(" order by alt.timestamp");
				if (criteria.getSortByOrder().equalsIgnoreCase("ASC")) {
					sql.append(" ASC");
				} else if (criteria.getSortByOrder().equalsIgnoreCase("DESC")) {
					sql.append(" DESC");
				}
			}
			// kasun

			this.queryString = sql.toString();
			this.queryValues = values;

			log.debug("Retrieve Alerts - Query String [ " + queryString + " ]");
			log.debug("Retrieve Alerts - Query Values " + queryValues);
		}

		@Override
		public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
			PreparedStatement prepStmt = con.prepareStatement(queryString);
			for (int i = 0; i < queryValues.size(); i++) {
				Object value = queryValues.get(i);
				if (value instanceof String) {
					prepStmt.setString((i + 1), (String) value);
				} else if (value instanceof Integer) {
					prepStmt.setInt((i + 1), ((Integer) value).intValue());
				} else if (value instanceof Long) {
					prepStmt.setLong((i + 1), ((Long) value).longValue());
				} else if (value instanceof Date) {
					prepStmt.setDate((i + 1), new java.sql.Date(((Date) value).getTime()));
				}
			}

			return prepStmt;
		}
	}

	public class RetreiveFlightPnrDetailsResultSetExtractor implements ResultSetExtractor {
		@Override
		public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
			List list = new ArrayList();
			FlightPnrNotificationDTO flightPnrDTO;

			if (rs != null) {
				while (rs.next()) {
					flightPnrDTO = new FlightPnrNotificationDTO();
					flightPnrDTO.setFlightNumber(rs.getString("FLIGHT_NUMBER"));
					flightPnrDTO.setDepDate(rs.getDate("EST_TIME_DEPARTURE_LOCAL"));
					flightPnrDTO.setPnr(rs.getString("PNR"));
					flightPnrDTO.setMobile(rs.getString("C_MOBILE_NO"));
					flightPnrDTO.setTitle(rs.getString("C_TITLE"));
					flightPnrDTO.setEmail(rs.getString("C_EMAIL"));
					flightPnrDTO.setLandPhone(rs.getString("C_PHONE_NO"));
					flightPnrDTO.setRoute(rs.getString("SEGMENT_CODE"));
					flightPnrDTO.setFlightId(rs.getInt("FLIGHT_ID"));
					flightPnrDTO.setPnrSegmentId(rs.getInt("PNR_SEG_ID"));
					flightPnrDTO.setPassengerName(rs.getString("NAME"));
					flightPnrDTO.setFirstName(rs.getString("C_FIRST_NAME"));
					flightPnrDTO.setAlert(rs.getString("alert_flag"));
					flightPnrDTO.setTotalPax(rs.getInt("TotalPax"));
					flightPnrDTO.setNoOfAdults(rs.getInt("total_pax_count"));
					flightPnrDTO.setNoOfChildren(rs.getInt("total_pax_child_count"));
					flightPnrDTO.setNoOfInfants(rs.getInt("total_pax_infant_count"));
					flightPnrDTO.setAlertCreatedActualDate(rs.getDate("timestamp"));

					try {
						ArrayList notifications = AlertingServicesBL.getNotificationsForPnr(rs.getInt("PNR_SEG_ID"));
						flightPnrDTO.setNotifications(notifications);
					} catch (Exception e) {
						log.error("Getting notifications for pnr failed.", e);
					}
					list.add(flightPnrDTO);
				}
			}
			log.debug("list.size() = " + list.size());
			return list;
		}
	}

	@Override
	public String getNextNotificationCode(String baseCode) {
		String code = null;
		int cnt = 1;

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		String sql = "select max(notification_code) from t_flight_notification " + " where notification_code like '" + baseCode
				+ "%' ";
		code = (String) jdbcTemplate.query(sql, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				String nCode = null;
				while (rs.next()) {
					nCode = rs.getString(1);
				}
				return nCode;
			}

		});
		if (code == null || "".equals(code))
			code = baseCode + cnt;
		else {
			String temp = code.replaceAll(baseCode, "").trim().equals("") ? "0" : code.replaceAll(baseCode, "").trim();
			int nextCnt = Integer.parseInt(temp) + cnt;
			code = baseCode + nextCnt;
		}
		return code;
	}

	/* added by Dhanya */

	@Override
	public Page retrieveFlightPnrDetailsForViewSearchCriteria(FlightPnrNotificationDTO searchCriteria) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());

		Collection colObj = (Collection) jdbcTemplate.query(new ViewFlightPnrDetailsPrepareStmtCreator(searchCriteria),
				new ViewFlightPnrDetailsResultSetExtractor());

		int recordCnt = (colObj == null) ? 0 : colObj.size();
		return new Page(recordCnt, 1, recordCnt, recordCnt, colObj);

	}

	// Added by Dhanya

	public class ViewFlightPnrDetailsPrepareStmtCreator implements PreparedStatementCreator {

		private final String queryString;

		private final List queryValues;

		public ViewFlightPnrDetailsPrepareStmtCreator(FlightPnrNotificationDTO criteria) {
			List values = new ArrayList();
			List criteriaList = new ArrayList();

			StringBuilder sql = new StringBuilder();

			sql.append("select a.*, b.message, rownum rid from (  ");
			sql.append("Select distinct to_timestamp(to_char(fpn.notification_date,'DD/MM/YYYY HH24:mi:ss'),'DD/MM/YYYY HH24:mi:ss')notification_date, fn.notification_code, fpn.flight_notification_id, fn.lang, ");
			sql.append("(select count(*) from t_flight_pnr_notification fpn1 where fpn1.flight_notification_id = fn.flight_notification_id AND fpn1.notification_channel=upper('SMS')  AND fpn1.notification_date=fpn.notification_date ) totalsms, ");
			sql.append("(select count(*) from t_flight_pnr_notification fpn2 where fpn2.flight_notification_id = fn.flight_notification_id AND fpn2.notification_channel=upper('EMAIL')  AND fpn2.notification_date=fpn.notification_date) totalemail, ");
			sql.append("(select count(*) from t_flight_pnr_notification fpn1  where fpn1.flight_notification_id = fn.flight_notification_id  AND fpn1.notification_channel=upper('SMS')  AND fpn1.notification_date=fpn.notification_date AND fpn1.delivery_status='D') deliveredsms,  ");
			sql.append("(select count(*) from t_flight_pnr_notification fpn1 where fpn1.flight_notification_id = fn.flight_notification_id AND fpn1.notification_channel=upper('SMS')  AND fpn1.notification_date=fpn.notification_date AND fpn1.delivery_status='F') failedsms, ");
			sql.append("(select count(*) from t_flight_pnr_notification fpn1 where fpn1.flight_notification_id = fn.flight_notification_id AND fpn1.notification_channel=upper('EMAIL')  AND fpn1.notification_date=fpn.notification_date AND fpn1.delivery_status='D') deliveredemail, ");
			sql.append("(select count(*) from t_flight_pnr_notification fpn1 where fpn1.flight_notification_id = fn.flight_notification_id AND fpn1.notification_channel=upper('EMAIL')  AND fpn1.notification_date=fpn.notification_date AND fpn1.delivery_status='F') failedemail ");
			sql.append("from t_flight_pnr_notification fpn ");
			sql.append("inner join t_flight_notification fn on (fn.flight_notification_id = fpn.flight_notification_id)");
			sql.append("inner join T_Pnr_Segment pnrseg on (pnrseg.pnr_seg_id = fpn.pnr_seg_id)");
			sql.append("inner join t_flight_segment fltseg on (fltseg.flight_id = fn.flight_id )");
			sql.append("inner join t_flight flt on (flt.flight_id = fltseg.flight_id)");

			if (criteria.getPnr() == null) {
				if ((criteria.getFlightNumber() != null) && (!criteria.getFlightNumber().trim().equals(""))) {
					criteriaList.add(" flt.flight_number = ? ");
					values.add(criteria.getFlightNumber());
				}
				if (criteria.getDateFrom() != null && criteria.getDateTo() != null) {
					criteriaList.add(" fltSeg.est_time_departure_local BETWEEN ? AND ? ");
					values.add(new java.sql.Date(criteria.getDateFrom().getTime()));
					values.add(new java.sql.Date(criteria.getDateTo().getTime()));
				}

			}
			if (criteria.getPnr() != null) {
				criteriaList.add(" pnrseg.Pnr = ?");

				values.add(criteria.getPnr());
			}

			if (criteria.getSearchReprotectAnciOption() != null) {
				if (FlightPnrNotificationDTO.SEARCH_REPROTECT_ANCI_FAILED.equals(criteria.getSearchReprotectAnciOption())) {
					criteriaList
							.add(" (pnrSeg.MEAL_REPROTECT_STATUS = 1 OR pnrSeg.SEAT_REPROTECT_STATUS = 1 OR pnrseg.BAGGAGE_REPROTECT_STATUS = 1) ");
				} else if (FlightPnrNotificationDTO.SEARCH_REPROTECT_ANCI_MEAL_FAILED.equals(criteria
						.getSearchReprotectAnciOption())) {
					criteriaList.add(" pnrseg.MEAL_REPROTECT_STATUS = 1 ");
				} else if (FlightPnrNotificationDTO.SEARCH_REPROTECT_ANCI_SEAT_FAILED.equals(criteria
						.getSearchReprotectAnciOption())) {
					criteriaList.add(" pnrseg.SEAT_REPROTECT_STATUS = 1 ");
				} else if (FlightPnrNotificationDTO.SEARCH_REPROTECT_ANCI_BAGGAGE_FAILED.equals(criteria
						.getSearchReprotectAnciOption())) {
					criteriaList.add(" pnrseg.BAGGAGE_REPROTECT_STATUS = 1 ");
				}
			}

			if (criteria.getDateFrom() != null) {
				criteriaList.add(" fpn.notification_date >= ?");
				values.add(new java.sql.Date(criteria.getDateFrom().getTime()));
			}

			if (criteria.getDateTo() != null) {
				criteriaList.add(" fpn.notification_date <= ?");
				values.add(new java.sql.Date(criteria.getDateTo().getTime() + 24 * 60 * 60 * 1000));
			}

			if (criteria.getFlightNotificationId() > 0) {
				criteriaList.add(" fpn.flight_notification_id = ?");
				values.add(new Integer(criteria.getFlightNotificationId()));
			}

			for (int i = 0; i < criteriaList.size(); i++) {
				sql.append(" AND ");
				sql.append(criteriaList.get(i));
			}
			sql.append("  order by to_timestamp(to_char(fpn.notification_date,'DD/MM/YYYY HH24:mi:ss'),'DD/MM/YYYY HH24:mi:ss') desc) a ");
			sql.append(" INNER JOIN t_flight_notification b ON a.notification_code = b.notification_code");

			this.queryString = sql.toString();
			this.queryValues = values;

			log.debug("Retrieve Notifications - Query String [ " + queryString + " ]");
			log.debug("Retrieve Notifications - Query Values " + queryValues);
		}

		@Override
		public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
			PreparedStatement prepStmt = con.prepareStatement(queryString);
			for (int i = 0; i < queryValues.size(); i++) {
				Object value = queryValues.get(i);
				if (value instanceof String) {
					prepStmt.setString((i + 1), (String) value);
				} else if (value instanceof Integer) {
					prepStmt.setInt((i + 1), ((Integer) value).intValue());
				} else if (value instanceof Long) {
					prepStmt.setLong((i + 1), ((Long) value).longValue());
				} else if (value instanceof Date) {
					prepStmt.setDate((i + 1), new java.sql.Date(((Date) value).getTime()));
				}
			}

			return prepStmt;
		}
	}

	public class ViewFlightPnrDetailsResultSetExtractor implements ResultSetExtractor {
		@Override
		public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
			List list = new ArrayList();
			FlightPnrNotificationDTO flightPnrDTO;

			if (rs != null) {
				while (rs.next()) {
					flightPnrDTO = new FlightPnrNotificationDTO();
					flightPnrDTO.setNotificationCode(rs.getString("NOTIFICATION_CODE"));

					Object obj = rs.getObject("MESSAGE");
					String msg = "";
					if (obj != null && obj instanceof CLOB) {
						Clob c = (CLOB) obj;
						StringBuffer strOut = new StringBuffer();
						String aux;
						try {
							BufferedReader br = new BufferedReader(c.getCharacterStream());
							while ((aux = br.readLine()) != null) {
								strOut.append(aux);
							}
						} catch (IOException e) {
							log.error("IOException in reading Clob message", e);
						} catch (SQLException se) {
							log.error("SQL Exception in reading Clob message", se);
						}

						msg = strOut.toString();
					}

					String lang = rs.getString("LANG");
					if (lang != null && (!lang.equals(Locale.ENGLISH.toString()))) {
						flightPnrDTO.setNotificationMsg(StringUtil.getUnicode(msg));
					} else {
						flightPnrDTO.setNotificationMsg(msg);
					}

					flightPnrDTO.setNotificationtDate(rs.getTimestamp("NOTIFICATION_DATE"));
					flightPnrDTO.setTotalSms(rs.getInt("TOTALSMS"));
					flightPnrDTO.setTotalEmail(rs.getInt("TOTALEMAIL"));
					flightPnrDTO.setTotalSmsDelivered(rs.getInt("DELIVEREDSMS"));
					flightPnrDTO.setTotalSmsFailed(rs.getInt("FAILEDSMS"));
					flightPnrDTO.setTotalEmailDelivered(rs.getInt("DELIVEREDEMAIL"));
					flightPnrDTO.setTotalEmailFailed(rs.getInt("FAILEDEMAIL"));
					flightPnrDTO.setFlightNotificationId(rs.getInt("FLIGHT_NOTIFICATION_ID"));

					list.add(flightPnrDTO);
				}
			}
			log.debug("list.size() = " + list.size());
			return list;
		}
	}

	@Override
	public Page retrieveDetailedNotificationList(int fltNotificationId, Date notifyDate, String pnr) throws ModuleException {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());

		Collection colObj = (Collection) jdbcTemplate.query(new ViewNotificationDetailsPrepareStmtCreator(fltNotificationId,
				notifyDate, pnr), new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List list = new ArrayList();
				FlightPnrNotificationDTO flightPnrDTO;
				HashMap objHm;
				if (rs != null) {
					while (rs.next()) {
						flightPnrDTO = new FlightPnrNotificationDTO();
						flightPnrDTO.setNotificationtDate(rs.getDate("NOTIFICATION_DATE"));
						flightPnrDTO.setNotificationCode(rs.getString("NOTIFICATION_CODE"));
						flightPnrDTO.setFlightNotificationId(rs.getInt("FLIGHT_NOTIFICATION_ID"));
						flightPnrDTO.setPnr(rs.getString("PNR"));
						flightPnrDTO.setMobile(rs.getString("MOBILE"));
						flightPnrDTO.setEmail(rs.getString("EMAIL"));
						flightPnrDTO.setLandPhone(rs.getString("C_PHONE_NO"));
						flightPnrDTO.setRoute(rs.getString("SEGMENT_CODE"));
						flightPnrDTO.setFlightId(rs.getInt("FLIGHT_ID"));
						flightPnrDTO.setNoOfAdults(rs.getInt("TOTAL_PAX_COUNT"));
						flightPnrDTO.setNoOfInfants(rs.getInt("TOTAL_PAX_INFANT_COUNT"));
						flightPnrDTO.setNoOfChildren(rs.getInt("TOTAL_PAX_CHILD_COUNT"));
						flightPnrDTO.setPnrSegmentId(rs.getInt("PNRSEG"));
                        flightPnrDTO.setDepDate(rs.getDate("DEPARTURE"));
						objHm = new HashMap();
						objHm.put("SMS", rs.getString("SMSSTATUS"));
						objHm.put("EMAIL", rs.getString("EMAILSTATUS"));
						flightPnrDTO.setDeliveryStatus(objHm);

						list.add(flightPnrDTO);
					}
				}

				log.debug("list.size() = " + list.size());
				return list;
			}
		});

		int recordCnt = (colObj == null) ? 0 : colObj.size();
		return new Page(recordCnt, 1, recordCnt, recordCnt, colObj);

	}

	// Added by Dhanya

	public class ViewNotificationDetailsPrepareStmtCreator implements PreparedStatementCreator {

		private final String queryString;

		private final List queryValues;

		public ViewNotificationDetailsPrepareStmtCreator(int fltNotificationId, Date notifyDate, String pnr) {
			List values = new ArrayList();
			List criteriaList = new ArrayList();

			StringBuffer sb = new StringBuffer();
			sb.append("select a.*, rownum rid from ( ");
			sb.append("Select distinct fpn.notification_date, fpn.pnr_seg_id as PNRSEG, fn.notification_code, fn.flight_id, fpn.flight_notification_id, res.pnr, res.total_pax_infant_count, res.total_pax_child_count, res.total_pax_count,");
			sb.append(" nvl((select recepients from t_flight_pnr_notification fpn1 where  fpn1.flight_notification_id = fn.flight_notification_id ");
			sb.append(" and fpn1.pnr_seg_id = fpn.pnr_seg_id and fpn1.notification_channel='SMS' and fpn1.notification_date = to_timestamp(to_char(fpn.notification_date,'dd/MM/yyyy HH24:mi:ss'),'dd/MM/yyyy HH24:mi:ss')),rCon.C_Mobile_no) Land, ");
			sb.append(" (select delivery_status from t_flight_pnr_notification fpn1 where  fpn1.flight_notification_id = fn.flight_notification_id ");
			sb.append(" and fpn1.pnr_seg_id = fpn.pnr_seg_id and fpn1.notification_channel='SMS' and fpn1.notification_date = to_timestamp(to_char(fpn.notification_date,'dd/MM/yyyy HH24:mi:ss'),'dd/MM/yyyy HH24:mi:ss')) SmsStatus,  ");
			sb.append(" nvl((select recepients from t_flight_pnr_notification fpn1 where  fpn1.flight_notification_id = fn.flight_notification_id ");
			sb.append(" and fpn1.pnr_seg_id = fpn.pnr_seg_id and fpn1.notification_channel='EMAIL' and fpn1.notification_date = to_timestamp(to_char(fpn.notification_date,'dd/MM/yyyy HH24:mi:ss'),'dd/MM/yyyy HH24:mi:ss')),rCon.C_Email) mail, ");
			sb.append(" (select delivery_status from t_flight_pnr_notification fpn1 where  fpn1.flight_notification_id = fn.flight_notification_id ");
			sb.append(" and fpn1.pnr_seg_id = fpn.pnr_seg_id and fpn1.notification_channel='EMAIL' and fpn1.notification_date = to_timestamp(to_char(fpn.notification_date,'dd/MM/yyyy HH24:mi:ss'),'dd/MM/yyyy HH24:mi:ss')) EmailStatus, ");
			sb.append(" rCon.c_phone_no, rCon.C_Mobile_no as Mobile, rCon.C_Email as Email, fltseg.segment_code, fltseg.est_time_departure_local as departure ");
			sb.append("from t_flight_pnr_notification fpn  ");
			sb.append("inner join t_flight_notification fn on (fn.flight_notification_id = fpn.flight_notification_id) ");
			sb.append("inner join T_Pnr_Segment pnrseg on (pnrseg.pnr_seg_id = fpn.pnr_seg_id) ");
			sb.append("inner join t_flight_segment fltseg on (fltseg.flt_seg_id = pnrseg.flt_seg_id ) ");
			sb.append("inner join T_reservation res on (res.pnr = pnrseg.pnr) ");
			sb.append("inner join T_reservation_contact rCon on (res.pnr = rCon.pnr) ");
			if (fltNotificationId != -1) {
				criteriaList.add(" fpn.flight_notification_id = ?");
				values.add(new Integer(fltNotificationId));
			}
			if (notifyDate != null) {
				criteriaList
						.add(" fpn.notification_date = to_timestamp(to_char(?,'dd/MM/yyyy HH24:mi:ss'),'dd/MM/yyyy HH24:mi:ss')");
				values.add(new java.sql.Timestamp(notifyDate.getTime()));

			}
			if (!StringUtil.isNullOrEmpty(pnr)) {
				criteriaList.add(" res.pnr = ?");
				values.add(pnr);
			}

			for (int i = 0; i < criteriaList.size(); i++) {
				if (sb.indexOf(" WHERE ") == -1)
					sb.append(" WHERE ");
				else
					sb.append(" AND ");
				sb.append(criteriaList.get(i));
			}
			sb.append("order by res.pnr) a");

			this.queryString = sb.toString();
			this.queryValues = values;

			log.debug("Retrieve Notifications - Query String [ " + queryString + " ]");
			log.debug("Retrieve Notifications - Query Values " + queryValues);
		}

		@Override
		public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
			PreparedStatement prepStmt = con.prepareStatement(queryString);
			for (int i = 0; i < queryValues.size(); i++) {
				Object value = queryValues.get(i);
				if (value instanceof String) {
					prepStmt.setString((i + 1), (String) value);
				} else if (value instanceof Integer) {
					prepStmt.setInt((i + 1), ((Integer) value).intValue());
				} else if (value instanceof Long) {
					prepStmt.setLong((i + 1), ((Long) value).longValue());
				} else if (value instanceof java.sql.Timestamp) {
					prepStmt.setTimestamp((i + 1), (java.sql.Timestamp) value);
				} else if (value instanceof Date) {
					prepStmt.setDate((i + 1), new java.sql.Date(((Date) value).getTime()));
				}
			}

			return prepStmt;
		}
	}
	
	
	@Override
	public Page retrieveSubmittedRequests(QueueRequestDetailsDTO searchCriteria) throws ModuleException{
		
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT DISTINCT qr.queue_request_id ,");
		sb.append(" q.queue_id ,");
		sb.append(" q.queue_name ,");
		sb.append(" qr.created_by ,");
		sb.append(" qr.created_date ,");
		sb.append(" qrs.queue_request_status_desc ,");
		sb.append(" qrs.queue_request_status_code ,");
		sb.append(" qr.modified_by ,");
		sb.append(" qr.modified_date ,");
		sb.append(" qr.pnr ,");
		sb.append(" qr.assigned_to ,");
		sb.append(" qr.assigned_on ,");
		sb.append(" qr.queue_request_description ");
		sb.append("FROM t_queue q ");
		sb.append("INNER JOIN t_queue_request qr ");
		sb.append("ON q.queue_id = qr.queue_id ");
		sb.append("INNER JOIN t_queue_request_status qrs ");
		sb.append("ON qr.queue_request_status_code = qrs.queue_request_status_code ");
		sb.append("FULL JOIN t_queue_page qp ");
		sb.append("ON qp.queue_id = q.queue_id ");
		sb.append("FULL JOIN t_page p ");
		sb.append("ON p.page_id      = qp.page_id ");
		sb.append("WHERE q.status    ='ACT' ");
		sb.append("AND qr.created_by = '"+ searchCriteria.getUserId() +"' ");
		
		if(!searchCriteria.getQueueId().equals("")){
			
			sb.append("AND q.queue_id = "+ searchCriteria.getQueueId() +" ");
			
		}
		
		if(!searchCriteria.getApplicablePageID().equals("")){
			
			sb.append("AND p.page_id = '"+ searchCriteria.getApplicablePageID() +"' ");
			//sb.append("AND (p.page_id LIKE '%"+ searchCriteria.getApplicablePageID() +"%' or p.page_id is null) ");
			
		}
		
		if(!searchCriteria.getPnr().equals("")){
			
			//sb.append("AND (qr.pnr LIKE '%"+ searchCriteria.getPnr() +"%' or qr.pnr is null) ");
			sb.append("AND qr.pnr LIKE '%"+ searchCriteria.getPnr() +"%' ");
			
		}
		
		if(!searchCriteria.getStatusCode().equals("")){
			
			//sb.append("AND (qr.queue_request_status_code LIKE '%"+ searchCriteria.getPnr() +"%' or qr.queue_request_status_code is null) ");
			sb.append("AND qr.queue_request_status_code = '"+ searchCriteria.getStatusCode() +"' ");
		}
		
		if(!searchCriteria.getFromDate().equals("")){
			if(!searchCriteria.getToDate().equals("")){
				sb.append("AND qr.created_date >= '" + searchCriteria.getFromDate() + "' AND qr.created_date <= '" + searchCriteria.getToDate() + "' " );
			}else{
				sb.append("AND qr.created_date >= '" + searchCriteria.getFromDate() + "' " );
			}
			
		}
		
		sb.append("ORDER BY qr.created_date DESC");
			
		String query = sb.toString();
		
		Collection colObj = (Collection) jdbcTemplate.query( query , new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				
				List list = new ArrayList();
				QueueRequestDTO queueRequestDTO;

				if (rs != null) {
					while (rs.next()) {
						
						queueRequestDTO = new QueueRequestDTO();
						queueRequestDTO.setRequestId(rs.getInt("QUEUE_REQUEST_ID"));
						queueRequestDTO.setQueueId(rs.getInt("QUEUE_ID"));
						queueRequestDTO.setQueueName(rs.getString("QUEUE_NAME"));
						queueRequestDTO.setDescription(rs.getString("QUEUE_REQUEST_DESCRIPTION"));
						queueRequestDTO.setPnr(rs.getString("PNR"));
						queueRequestDTO.setAssignedTo(rs.getString("ASSIGNED_TO"));
						queueRequestDTO.setAssignedOn(rs.getDate("ASSIGNED_ON"));
						queueRequestDTO.setCreatedDate(rs.getTimestamp("CREATED_DATE"));
						queueRequestDTO.setModifiedDate(rs.getDate("MODIFIED_DATE"));
						queueRequestDTO.setCreatedBy(rs.getString("CREATED_BY"));
						queueRequestDTO.setModifiedBy(rs.getString("MODIFIED_BY"));
						queueRequestDTO.setStatus(rs.getString("QUEUE_REQUEST_STATUS_DESC"));
						queueRequestDTO.setStatusCode(rs.getString("QUEUE_REQUEST_STATUS_CODE"));
						list.add(queueRequestDTO);
						
					}
				}

				log.debug("list.size() = " + list.size());
				return list;
			}
		});

		int recordCnt = (colObj == null) ? 0 : colObj.size();
		return new Page(recordCnt, 1, recordCnt, recordCnt, colObj);
		
	}
	
	
	public Page getRequestHistory(int requestId) throws ModuleException{
		
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		
		String query;
		
		query = "SELECT * FROM t_queue_request_status";
		
		final Map<String, String> requestStatus = (Map) jdbcTemplate.query( query , new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				
				Map map = new HashMap<String, String>();
				  
				  while (rs.next()) {
				    String key = rs.getString("QUEUE_REQUEST_STATUS_CODE"); 
				    String value = rs.getString("QUEUE_REQUEST_STATUS_DESC"); 
				    map.put(key, value); 
				  }
				  return map; 

			}
		});
		
		
		query = "SELECT * FROM t_queue_request_history WHERE queue_request_id = " + requestId + " ORDER BY TIMESTAMP DESC";
		
		Collection colObj = (Collection) jdbcTemplate.query( query , new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				
				List list = new ArrayList();
				RequestHistoryDTO requestHistoryDTO;

				if (rs != null) {
					while (rs.next()) {
						
						requestHistoryDTO = new RequestHistoryDTO();
						requestHistoryDTO.setAction(requestStatus.get(rs.getString("ACTION")));
						requestHistoryDTO.setRemark(rs.getString("REMARKS"));
						requestHistoryDTO.setUserId(rs.getString("USER_ID"));
						requestHistoryDTO.setTimestamp(rs.getTimestamp("TIMESTAMP"));
						
						String statusChange = rs.getString("STATUS_CHANGE");
						String statusFrom = statusChange.split("->")[0];
						String statusTo = statusChange.split("->")[1];
						
						requestHistoryDTO.setStatusFrom(requestStatus.get(statusFrom));
						requestHistoryDTO.setStatusTo(requestStatus.get(statusTo));
						
						list.add(requestHistoryDTO);
						
					}
				}

				log.debug("list.size() = " + list.size());
				return list;
			}
		});

		int recordCnt = (colObj == null) ? 0 : colObj.size();
		return new Page(recordCnt, 1, recordCnt, recordCnt, colObj);
		
	}
	
	@Override
	public Page retrieveSubmittedRequestsForAction(String userID , String queueId , String status) throws ModuleException{
		
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT qr.created_by ,");
		sb.append(" qr.queue_request_id ,");
		sb.append(" q.queue_name ,");
		sb.append(" q.queue_id ,");
		sb.append(" qr.queue_request_description ,");
		sb.append(" qr.queue_request_status_code ,");
		sb.append(" qrs.queue_request_status_desc ,");
		sb.append(" qr.created_date ,");
		sb.append(" qr.pnr ");
		sb.append("FROM t_queue q ");
		sb.append("INNER JOIN t_queue_request qr ");
		sb.append("ON q.queue_id = qr.queue_id ");
		sb.append("INNER JOIN t_queue_request_status qrs ");
		sb.append("ON qr.queue_request_status_code = qrs.queue_request_status_code ");
		sb.append("WHERE qr.queue_id IN");
		sb.append("  (SELECT q.queue_id");
		sb.append("  FROM t_queue q");
		sb.append("  WHERE q.queue_id IN");
		sb.append("    (SELECT qu.queue_id FROM t_queue_user qu WHERE qu.user_id = '"+ userID +"' ");
		sb.append("    )");
		sb.append("  ) ");
		
		if(queueId.equals("")){
			
			sb.append("AND q.queue_id LIKE '%"+ queueId +"%' ");
			
		}else{
			
			sb.append("AND q.queue_id = "+ queueId +" ");
			
		}
		
		sb.append("AND qr.queue_request_status_code LIKE '%"+ status +"%' ");
		sb.append("AND NOT (qr.queue_request_status_code = 'INP' ");
		sb.append("AND qr.modified_by != '"+ userID +"') ");
		sb.append("ORDER BY qr.created_date ASC");
		
		String query = sb.toString();
		
		Collection colObj = (Collection) jdbcTemplate.query( query , new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				
				List list = new ArrayList();
				QueueRequestDTO queueRequestDTO;

				if (rs != null) {
					while (rs.next()) {
						
						queueRequestDTO = new QueueRequestDTO();
						
						queueRequestDTO.setRequestId(rs.getInt("QUEUE_REQUEST_ID"));
						queueRequestDTO.setQueueName(rs.getString("QUEUE_NAME"));
						queueRequestDTO.setDescription(rs.getString("QUEUE_REQUEST_DESCRIPTION"));
						queueRequestDTO.setCreatedBy(rs.getString("CREATED_BY"));
						queueRequestDTO.setStatus(rs.getString("QUEUE_REQUEST_STATUS_DESC"));
						queueRequestDTO.setStatusCode(rs.getString("QUEUE_REQUEST_STATUS_CODE"));
						queueRequestDTO.setCreatedDate(rs.getTimestamp("CREATED_DATE"));
						queueRequestDTO.setQueueId(rs.getInt("QUEUE_ID"));
						queueRequestDTO.setPnr(rs.getString("PNR"));
						
						list.add(queueRequestDTO);
						
					}
				}

				log.debug("list.size() = " + list.size());
				return list;
			}
		});

		int recordCnt = (colObj == null) ? 0 : colObj.size();
		return new Page(recordCnt, 1, recordCnt, recordCnt, colObj);
		
	}

	@Override
	public Page retrieveFlightDetailsForSearchCriteria(FlightPnrSearchNotificationDTO searchCriteria) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());

		StringBuilder sql = new StringBuilder();

		sql.append("SELECT a.*,alt.timestamp AS alert_created_date FROM (");
		sql.append("SELECT fl.flight_id,fl.flight_number,fs.est_time_departure_local AS departure_date,res.pnr,");
		sql.append(" (res.total_pax_count + res.total_pax_child_count) As TotalPax, ");
		sql.append("res.total_pax_count,res.total_pax_child_count,res.total_pax_infant_count,");
		sql.append("ps.pnr_seg_id,fs.segment_code AS route,ps.alert_flag ");
		sql.append("FROM T_PNR_SEGMENT ps,T_FLIGHT_SEGMENT fs,T_FLIGHT fl,");
		sql.append("T_RESERVATION res,T_PNR_PASSENGER pax ");
		sql.append("WHERE ps.FLT_SEG_ID = fs.FLT_SEG_ID AND fl.FLIGHT_ID = fs.FLIGHT_ID ");
		sql.append("AND ps.PNR = res.PNR AND res.PNR = pax.PNR ");

		if (searchCriteria.getFlightNumber() != null) {
			sql.append("AND fl.FLIGHT_NUMBER = UPPER('" + searchCriteria.getFlightNumber() + "') ");
		}

		if (searchCriteria.getPnr() != null) {
			sql.append("AND res.PNR = UPPER('" + searchCriteria.getPnr() + "') ");
		}

		if ((searchCriteria.getFlightFromDate() != null) && (searchCriteria.getFlightToDate() != null)) {
			sql.append("AND fs.est_time_departure_local BETWEEN TO_DATE("
					+ getStartOrEndTimeOfDateInSqlFormat(true, searchCriteria.getFlightFromDate()) + ")" + " AND TO_DATE("
					+ getStartOrEndTimeOfDateInSqlFormat(false, searchCriteria.getFlightToDate()) + ") ");
		} else if (searchCriteria.getFlightFromDate() != null) {
			sql.append("AND fs.est_time_departure_local BETWEEN TO_DATE("
					+ getStartOrEndTimeOfDateInSqlFormat(true, searchCriteria.getFlightFromDate()) + ")" + " AND TO_DATE("
					+ getStartOrEndTimeOfDateInSqlFormat(false, addNoOfDays(searchCriteria.getFlightFromDate(), 30)) + ") ");
		} else {
			Date date = new Date();
			sql.append("AND fs.est_time_departure_local BETWEEN TO_DATE(" + getStartOrEndTimeOfDateInSqlFormat(true, date) + ")"
					+ " AND TO_DATE(" + getStartOrEndTimeOfDateInSqlFormat(false, addNoOfDays(date, 30)) + ") ");
		}

		if (searchCriteria.getSegmentFrom() != null) {
			sql.append(" AND fs.segment_code LIKE '" + searchCriteria.getSegmentFrom() + "/%'");
		}

		if (searchCriteria.getSegmentTo() != null) {
			sql.append(" AND fs.segment_code LIKE '%/" + searchCriteria.getSegmentTo() + "'");
		}

		if (searchCriteria.getDaysOfOperation() != null && !(searchCriteria.getDaysOfOperation().isEmpty())) {
			sql.append(" AND fl.day_number in (" + searchCriteria.getDaysOfOperation().replaceAll("[\\[\\](){}]", "") + ") ");
		}
		
		if (searchCriteria.getAlertFlag().equals(FlightPnrSearchNotificationDTO.WITH_ALERTS)) {
			sql.append(" AND ps.alert_flag = 1 ");
		} else if (searchCriteria.getAlertFlag().equals(FlightPnrSearchNotificationDTO.WITHOUT_ALERTS)) {
			sql.append(" AND ps.alert_flag = 0 ");
		}

		if (searchCriteria.getSearchReprotectAnciOption() != null) {
			if (FlightPnrSearchNotificationDTO.SEARCH_REPROTECT_ANCI_FAILED
					.equals(searchCriteria.getSearchReprotectAnciOption())) {
				sql.append(
						" AND (ps.MEAL_REPROTECT_STATUS = 1 OR ps.SEAT_REPROTECT_STATUS = 1 OR ps.BAGGAGE_REPROTECT_STATUS = 1) ");
			} else if (FlightPnrSearchNotificationDTO.SEARCH_REPROTECT_ANCI_MEAL_FAILED
					.equals(searchCriteria.getSearchReprotectAnciOption())) {
				sql.append(" AND ps.MEAL_REPROTECT_STATUS = 1 ");
			} else if (FlightPnrSearchNotificationDTO.SEARCH_REPROTECT_ANCI_SEAT_FAILED
					.equals(searchCriteria.getSearchReprotectAnciOption())) {
				sql.append(" AND ps.SEAT_REPROTECT_STATUS = 1 ");
			} else if (FlightPnrSearchNotificationDTO.SEARCH_REPROTECT_ANCI_BAGGAGE_FAILED
					.equals(searchCriteria.getSearchReprotectAnciOption())) {
				sql.append(" AND ps.BAGGAGE_REPROTECT_STATUS = 1 ");
			}
		}

		sql.append(" AND ps.Status <> 'CNX' ");
		sql.append(" GROUP BY fl.flight_number,fs.est_time_departure_local,res.pnr,");
		sql.append("res.total_pax_count,res.total_pax_child_count,res.total_pax_infant_count,");
		sql.append("ps.pnr_seg_id,fs.segment_code,ps.alert_flag,fl.flight_id ");
		
		sql.append(")a,t_alert alt WHERE a.pnr_seg_id = alt.pnr_seg_id (+)");
		
//		if (searchCriteria.getSpecifyDelay() > 0) {
//			sql.append(" AND ( alt.original_dep_date -  a.departure_date) * 24 * 60 >= " + searchCriteria.getSpecifyDelay());
//		}
		
		if ((searchCriteria.getAlertCreatedFromDate() != null) && (searchCriteria.getAlertCreatedToDate() != null)) {
			sql.append(" AND alt.TIMESTAMP BETWEEN TO_DATE("
					+ getStartOrEndTimeOfDateInSqlFormat(true, searchCriteria.getAlertCreatedFromDate()) + ")" + " AND TO_DATE("
					+ getStartOrEndTimeOfDateInSqlFormat(false, searchCriteria.getAlertCreatedToDate()) + ") ");
		} else if (searchCriteria.getAlertCreatedFromDate() != null) {
			sql.append(" AND alt.TIMESTAMP BETWEEN TO_DATE("
					+ getStartOrEndTimeOfDateInSqlFormat(true, searchCriteria.getAlertCreatedFromDate()) + ")" + " AND TO_DATE("
					+ getStartOrEndTimeOfDateInSqlFormat(false, addNoOfDays(searchCriteria.getAlertCreatedFromDate(), 30))
					+ ") ");
		}
		
		if (searchCriteria.getSortBy().equalsIgnoreCase("DEP_DATE")) {
			sql.append("order by a.departure_date");
			if (searchCriteria.getSortByOrder().equalsIgnoreCase("ASC")) {
				sql.append(" ASC");
			} else if (searchCriteria.getSortByOrder().equalsIgnoreCase("DESC")) {
				sql.append(" DESC");
			}
		}
		
		Collection colObj = (Collection) jdbcTemplate.query(sql.toString(), new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<FlightPnrSearchNotificationDTO> list = new ArrayList<>();
				Map<Integer, FlightPnrSearchNotificationDTO> flightSearchDetails = new LinkedHashMap<>();
				FlightPnrSearchNotificationDTO flightPnrSearchDTO;
				SimpleDateFormat localDateFormat = new SimpleDateFormat("HH:mm:ss");
				if (rs != null) {
					while (rs.next()) {
						flightPnrSearchDTO = new FlightPnrSearchNotificationDTO();
						flightPnrSearchDTO.setFlightId(rs.getInt("FLIGHT_ID"));
						flightPnrSearchDTO.setFlightNumber(rs.getString("FLIGHT_NUMBER"));
						flightPnrSearchDTO.setDepDate(rs.getDate("DEPARTURE_DATE"));
						flightPnrSearchDTO.setPnr(rs.getString("PNR"));
						flightPnrSearchDTO.setRoute(rs.getString("ROUTE"));
						flightPnrSearchDTO.setNoOfAdults(rs.getInt("TOTAL_PAX_COUNT"));
						flightPnrSearchDTO.setNoOfChildren(rs.getInt("TOTAL_PAX_CHILD_COUNT"));
						flightPnrSearchDTO.setNoOfInfants(rs.getInt("TOTAL_PAX_INFANT_COUNT"));
						flightPnrSearchDTO.setAlertFlag(rs.getString("ALERT_FLAG"));
						flightPnrSearchDTO.setAlertCreatedDate(rs.getString("ALERT_CREATED_DATE"));
						Date date;
						try {
							date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(rs.getString("DEPARTURE_DATE"));
							flightPnrSearchDTO.setOriginalFlightTime(new SimpleDateFormat("HH:mm:ss").format(date));
						} catch (ParseException e) {
							log.error("Invalid Date - Parse Exception", e);
						}
						list.add(flightPnrSearchDTO);
					}

					for (FlightPnrSearchNotificationDTO fpsDTO : list) {
						if (flightSearchDetails.containsKey(fpsDTO.getFlightId())) {
							FlightPnrSearchNotificationDTO fpsDTO1 = flightSearchDetails.get(fpsDTO.getFlightId());
							if (!fpsDTO1.getPnr().toLowerCase().contains(fpsDTO.getPnr().toLowerCase())) {
								fpsDTO1.setPnr(fpsDTO1.getPnr() + "," + fpsDTO.getPnr());
								fpsDTO1.setTotalPnrCount(fpsDTO1.getTotalPnrCount() + 1);
								fpsDTO1.setNoOfAdults(fpsDTO1.getNoOfAdults() + fpsDTO.getNoOfAdults());
								fpsDTO1.setNoOfChildren(fpsDTO1.getNoOfChildren() + fpsDTO.getNoOfChildren());
								fpsDTO1.setNoOfInfants(fpsDTO1.getNoOfInfants() + fpsDTO.getNoOfInfants());
							}
							if (fpsDTO.getAlertFlag().equals("1") && fpsDTO1.getAlertCreatedDate() != null) {
								fpsDTO1.setAlertCreatedDate(fpsDTO1.getAlertCreatedDate() + ", " + fpsDTO.getAlertCreatedDate());
							} else if (fpsDTO.getAlertFlag().equals("1")) {
								fpsDTO1.setAlertCreatedDate(fpsDTO.getAlertCreatedDate());
							}
							flightSearchDetails.put(fpsDTO.getFlightId(), fpsDTO1);
						} else {
							fpsDTO.setTotalPnrCount(1);
							flightSearchDetails.put(fpsDTO.getFlightId(), fpsDTO);
						}
					}
				}

				return new ArrayList(flightSearchDetails.values());
			}
		});

		int recordCnt = (colObj == null) ? 0 : colObj.size();
		getInBoundConnectionListForFlightSearch(colObj);
		return new Page(recordCnt, 1, recordCnt, recordCnt, colObj);

	}

	@Override
	public Page retrieveMessageListForLoadCriteria(Date depDate) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());

		final String FLIGHT_NUMBER = "flightNumber";
		final String TOTAL_MESSAGE_COUNT = "totalMessageCount";
		final String NOTIFICATION_CHANNEL = "notificationChannel";
		final String DELIVERY_STATUS = "delivaryStatus";
		final String RECEPIENTS_LIST = "recepientsList";
		final String SEGMENT_CODE = "segmentCode";

		StringBuilder sql = new StringBuilder();

		sql.append("SELECT tf.FLIGHT_NUMBER,tfs.SEGMENT_CODE,count(*) AS TOTAL_MESSAGE,");
		sql.append("tfpn.NOTIFICATION_CHANNEL,tfpn.DELIVERY_STATUS,LISTAGG(tfpn.RECEPIENTS,',') ");
		sql.append("WITHIN GROUP(ORDER BY tfpn.FLIGHT_PNR_NOTIFICATION_ID) AS RECEPIENTS_LIST FROM ");
		sql.append("T_FLIGHT_NOTIFICATION tfn,T_FLIGHT_PNR_NOTIFICATION tfpn,");
		sql.append("T_FLIGHT tf,T_FLIGHT_SEGMENT tfs WHERE tfs.FLIGHT_ID = tf.FLIGHT_ID ");
		sql.append("AND tfn.FLIGHT_ID = tf.FLIGHT_ID AND tfpn.FLIGHT_NOTIFICATION_ID = tfn.FLIGHT_NOTIFICATION_ID ");

		if (depDate != null) {
			sql.append("AND tfs.EST_TIME_DEPARTURE_LOCAL BETWEEN TO_DATE("
					+ CalendarUtil.formatForSQL_toDate(CalendarUtil.getStartTimeOfDate(depDate)) + ")" + " AND TO_DATE("
					+ CalendarUtil.formatForSQL_toDate(CalendarUtil.getEndTimeOfDate(depDate)) + ") ");
		}

		sql.append("GROUP BY tfpn.NOTIFICATION_CHANNEL,tf.FLIGHT_NUMBER,tfpn.DELIVERY_STATUS,tfs.SEGMENT_CODE");
		
		Collection colObj = (Collection) jdbcTemplate.query(sql.toString(), new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<Map<String, String>> msgList = new ArrayList<>();
				Map<String, FlightPnrNotificationDTO> msgDetails = new LinkedHashMap<>();
				Map<String, String> messageDetail;
				if (rs != null) {
					while (rs.next()) {
						messageDetail = new HashMap<>();
						messageDetail.put(FLIGHT_NUMBER, rs.getString("FLIGHT_NUMBER"));
						messageDetail.put(SEGMENT_CODE, rs.getString("SEGMENT_CODE"));
						messageDetail.put(TOTAL_MESSAGE_COUNT, rs.getString("TOTAL_MESSAGE"));
						messageDetail.put(NOTIFICATION_CHANNEL, rs.getString("NOTIFICATION_CHANNEL"));
						messageDetail.put(DELIVERY_STATUS, rs.getString("DELIVERY_STATUS"));
						messageDetail.put(RECEPIENTS_LIST, rs.getString("RECEPIENTS_LIST"));
						msgList.add(messageDetail);
					}

					for (Map messages : msgList) {
						FlightPnrNotificationDTO fpnDTO;
						String fltNo = (String) messages.get(FLIGHT_NUMBER);
						String strMsgCount = (String) messages.get(TOTAL_MESSAGE_COUNT);
						int msgCount = Integer.valueOf(strMsgCount);
						String notificationChannel = (String) messages.get(NOTIFICATION_CHANNEL);
						String deliveryStatus = (String) messages.get(DELIVERY_STATUS);
						String recepientsList = (String) messages.get(RECEPIENTS_LIST);
						String segmentCode = (String) messages.get(SEGMENT_CODE);

						if (msgDetails.containsKey(fltNo)) {
							fpnDTO = msgDetails.get(fltNo);
						} else {
							fpnDTO = new FlightPnrNotificationDTO();
							fpnDTO.setFlightNumber(fltNo);
							fpnDTO.setSegmentCode(segmentCode);
						}

						if (notificationChannel.equals("EMAIL")) {
							fpnDTO.setTotalEmail(fpnDTO.getTotalEmail() + msgCount);
							if (deliveryStatus.equals("D")) {
								fpnDTO.setTotalEmailDelivered(fpnDTO.getTotalEmailDelivered() + msgCount);
							} else if (deliveryStatus.equals("F")) {
								fpnDTO.setTotalEmailFailed(fpnDTO.getTotalEmailFailed() + msgCount);
								fpnDTO.setFailedEmailList(recepientsList);
							}
						} else if (notificationChannel.equals("SMS")) {
							fpnDTO.setTotalSms(fpnDTO.getTotalSms() + msgCount);
							if (deliveryStatus.equals("D")) {
								fpnDTO.setTotalSmsDelivered(fpnDTO.getTotalSmsDelivered() + msgCount);
							} else if (deliveryStatus.equals("F")) {
								fpnDTO.setTotalSmsFailed(fpnDTO.getTotalSmsFailed() + msgCount);
								fpnDTO.setFailedSmsList(recepientsList);
							}
						}

						msgDetails.put(fltNo, fpnDTO);
					}
				}

				return new ArrayList(msgDetails.values());
			}
		});

		int recordCnt = (colObj == null) ? 0 : colObj.size();
		return new Page(recordCnt, 1, recordCnt, recordCnt, colObj);
	}

	private void getInBoundConnectionListForFlightSearch(Collection col) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		Iterator it = null;

		final String CurrentPNRSegmentDepartureTime = "CurrDeptTime";
		final String CurrentPNRSegmentDestination = "CurrDestCode";
		final String InBoundFlightNo = "InBoundFlightNo";
		final String InBoundDepartureDate = "InBoundDepartureDate";
		final String InBoundDepartureStation = "InBoundDepartureStation";

		String[] minMaxTransitDurations = null;
		it = col.iterator();

		while (it.hasNext()) {
			FlightPnrSearchNotificationDTO fpnDTO = (FlightPnrSearchNotificationDTO) it.next();
			String[] pnrArray = fpnDTO.getPnr().split(",");
			int flightID = fpnDTO.getFlightId();
			String departureStation = fpnDTO.getRoute().substring(0, 3);

			if (!CommonsServices.getGlobalConfig().isHubAirport(departureStation)) {
				continue;
			}

			try {
				minMaxTransitDurations = CommonsServices.getGlobalConfig().getMixMaxTransitDurations(departureStation, null,
						null);
			} catch (ModuleRuntimeException ex) {
				if ("commons.data.transitdurations.notconfigured".equalsIgnoreCase(ex.getExceptionCode())) {
					return;
				}
			}

			String[] minTransitHoursMin = minMaxTransitDurations[0].split(":");
			String[] maxTransitHoursMin = minMaxTransitDurations[1].split(":");

			Double minConnectTimeInMins = (Double.valueOf(minTransitHoursMin[1]) / 60);
			Double minConnectTimeInHours = Double.valueOf(minTransitHoursMin[0]);
			Double maxConnectTimeInMins = (Double.valueOf(maxTransitHoursMin[1]) / 60);
			Double maxConnectTimeInHours = Double.valueOf(maxTransitHoursMin[0]);

			Double minConnectTime = (minConnectTimeInHours + minConnectTimeInMins) * -1;
			Double maxConnectTime = (maxConnectTimeInHours + maxConnectTimeInMins) * -1;

			final double doubleMinConnectTime = (minConnectTime).doubleValue();
			final double negativeMaxConnectTime = (maxConnectTime).doubleValue();

			for (String pnr : pnrArray) {
				StringBuilder sql1 = new StringBuilder();
				sql1.append("SELECT * FROM");
				sql1.append(" (SELECT TO_CHAR(s.EST_TIME_DEPARTURE_LOCAL, 'DD-MON-YYYY HH24:MI:SS') DEPARTURE_LOCAL,");
				sql1.append(" SUBSTR(s.segment_code,LENGTH(s.segment_code) - 2,3) destination");
				sql1.append(" FROM V_PNR_SEGMENT s");
				sql1.append(" WHERE s.pnr ='" + pnr + "'");
				sql1.append(" AND s.segment_code LIKE '" + departureStation + "/%'");
				sql1.append(" AND s.pnr_status <> 'CNX'");
				sql1.append(" and s.flight_id = '" + flightID + "'");
				sql1.append(" order by SEGMENT_SEQ )");
				sql1.append(" where rownum = 1");

				HashMap currentPNRSegmentRecord = (HashMap) jdbcTemplate.query(sql1.toString(), new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						HashMap<String, String> currentPNRSegmentRecord = new HashMap<String, String>();
						if (rs.next()) {
							currentPNRSegmentRecord.put(CurrentPNRSegmentDepartureTime, rs.getString("DEPARTURE_LOCAL"));
							currentPNRSegmentRecord.put(CurrentPNRSegmentDestination, rs.getString("destination"));
						}
						return currentPNRSegmentRecord;
					}
				});

				StringBuilder sql2 = new StringBuilder();
				sql2.append("SELECT s.FLIGHT_NUMBER,s.SEGMENT_CODE,s.EST_TIME_DEPARTURE_LOCAL,");
				sql2.append(" (s.EST_TIME_ARRIVAL_LOCAL - TO_DATE('" + currentPNRSegmentRecord.get(CurrentPNRSegmentDepartureTime)
						+ "','DD-MON-YYYY HH24:MI:SS'))*24 connectTime");
				sql2.append(" FROM V_EXT_PNR_SEGMENT s,t_pnr_segment ps,t_flight_segment fs,t_flight f");
				sql2.append(" WHERE s.pnr = '" + pnr + "'");
				sql2.append(" AND ps.pnr_seg_id(+)    = s.pnr_seg_id");
				sql2.append(" AND ps.flt_seg_id       = fs.flt_seg_id(+)");
				sql2.append(" AND fs.flight_id        = f.flight_id(+)");
				sql2.append(" AND (s.pnr_seg_id       = -1");
				sql2.append(" OR f.operation_type_id != '5' )");
				sql2.append(" AND s.segment_code LIKE '%/" + departureStation + "'");
				sql2.append(" and s.pnr_status <> 'CNX'");
				sql2.append(" order by s.EST_TIME_ARRIVAL_LOCAL desc");

				HashMap inBoundConnectionRecord = (HashMap) jdbcTemplate.query(sql2.toString(), new ResultSetExtractor() {
					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

						HashMap<String, String> inBoundConnectionRecord = new HashMap<String, String>();
						while (rs.next()) {
							if ((rs.getDouble("connectTime") < 0) && (rs.getDouble("connectTime") >= negativeMaxConnectTime)
									&& (rs.getDouble("connectTime") <= doubleMinConnectTime)) {
								/**
								 * Bussiness Rule: Consider only latest arriving flights within maximum connection time
								 */
								inBoundConnectionRecord.put(InBoundFlightNo, rs.getString("FLIGHT_NUMBER"));
								inBoundConnectionRecord.put(InBoundDepartureDate,
										rs.getDate("EST_TIME_DEPARTURE_LOCAL").toString());
								inBoundConnectionRecord.put(InBoundDepartureStation,
										rs.getString("SEGMENT_CODE").substring(4, 7));
								return inBoundConnectionRecord;
							}
						}
						return null;
					}
				});
				if (inBoundConnectionRecord != null) {
					if (fpnDTO.getInBoundConnections() != null) {
						fpnDTO.setInBoundConnections(fpnDTO.getInBoundConnections() + ","
								+ (String) inBoundConnectionRecord.get(InBoundDepartureStation));
					} else {
						fpnDTO.setInBoundConnections((String) inBoundConnectionRecord.get(InBoundDepartureStation));
					}
				}
			}

		}

	}

	private Date addNoOfDays(Date date, int days) {
		java.sql.Date fromDate = new java.sql.Date(date.getTime());
		java.util.GregorianCalendar calendar = new java.util.GregorianCalendar();
		calendar.setTime(fromDate);
		calendar.add(java.util.GregorianCalendar.DATE, days);
		return calendar.getTime();
	}
	
	private String getStartOrEndTimeOfDateInSqlFormat(boolean isStart, Date date) {
		if (isStart) {
			return CalendarUtil.formatForSQL_toDate(CalendarUtil.getStartTimeOfDate(date));
		} else {
			return CalendarUtil.formatForSQL_toDate(CalendarUtil.getEndTimeOfDate(date));
		}
	}
}
