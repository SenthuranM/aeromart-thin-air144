/*
 * Created on Jul 6, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.isa.thinair.alerting.core.persistence.dao;

import java.util.List;

import com.isa.thinair.alerting.api.model.AlertType;

/**
 * @author sumudu
 * 
 * 
 */
public interface AlertTypeDAO {
	public List getAlertTypes();

	public AlertType getAlertType(int alertTypeID);

	public void saveAlertType(AlertType alertTypeID);

	public void removeAlertType(int alertTypeID);

	public List getAlertTypesOnPermission(int permissionId);
}
