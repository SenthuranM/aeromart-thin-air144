/*
 * Created on Jul 6, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.isa.thinair.alerting.core.persistence.dao;

import java.util.List;

import com.isa.thinair.alerting.api.model.AlertPriority;

/**
 * @author sumudu
 * 
 * 
 */
public interface AlertPriorityDAO {
	public List getAlertPriorites();

	public AlertPriority getAlertPriority(int priorityCode);

	public void saveAlertPriority(AlertPriority priorityCode);

	public void removeAlertPriority(int priorityCode);
}
