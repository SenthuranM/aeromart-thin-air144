package com.isa.thinair.alerting.core.service;

import com.isa.thinair.platform.api.DefaultModule;

/**
 * 
 * AlertingService service interface
 * 
 * @isa.module.service-interface module-name="alerting" description="The alerting module"
 */
public class AlertingService extends DefaultModule {
}