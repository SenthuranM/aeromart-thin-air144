package com.isa.thinair.alerting.core.persistence.dao;

import com.isa.thinair.alerting.api.dto.AlertDetailDTO;
import com.isa.thinair.alerting.api.dto.FlightPnrNotificationDTO;
import com.isa.thinair.alerting.api.dto.FlightPnrSearchNotificationDTO;
import com.isa.thinair.alerting.api.dto.QueueRequestDetailsDTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

import java.util.Date;
import java.util.List;

public interface AlertingJdbcDAO {

	public List retrieveAlertsForUser(String userId);

	public Page retrieveAlertsForSearchCriteria(AlertDetailDTO searchCriteria);

	public Page retrieveFlightPnrDetailsForSearchCriteria(FlightPnrNotificationDTO searchCriteria);

	public Page retrieveFlightPnrDetailsForViewSearchCriteria(FlightPnrNotificationDTO searchCriteria);

	public String getNextNotificationCode(String baseCode);

	public Page retrieveDetailedNotificationList(int fltNotificationId, Date notifyDate, String pnr) throws ModuleException;
	
	public Page retrieveSubmittedRequests(QueueRequestDetailsDTO searchCriteria) throws ModuleException;
	
	public Page getRequestHistory(int requestId) throws ModuleException;
	
	public Page retrieveSubmittedRequestsForAction(String userID , String queueId , String status) throws ModuleException;

	public Page retrieveFlightDetailsForSearchCriteria(FlightPnrSearchNotificationDTO searchCriteria);

	public Page retrieveMessageListForLoadCriteria(Date depDate);
}
