/**
 * 
 */
package com.isa.thinair.alerting.api.model;

import java.io.Serializable;

/**
 * @author suneth
 * @hibernate.class table = "T_QUEUE_USER" QueueUser is the entity class to represent a QueueUser model
 * 
 */
public class QueueUser implements Serializable {


	/**
	 * 
	 */
	private int queueUserId;
	private int queueId;
	private String userId;
	
	/**
	 * @return the queueUserId
	 * @hibernate.id column = "QUEUE_USER_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_QUEUE_USER"
	 */
	public int getQueueUserId() {
		return queueUserId;
	}

	/**
	 * @param queueUserId the queueUserId to set
	 */
	public void setQueueUserId(int queueUserId) {
		this.queueUserId = queueUserId;
	}

	/**
	 * @return the queueId
	 * @hibernate.property column = "QUEUE_ID" 
	 * 
	 */
	public int getQueueId() {
		return queueId;
	}

	/**
	 * @param queueId the queueId to set
	 */
	public void setQueueId(int queueId) {
		this.queueId = queueId;
	}

	/**
	 * @return the userId
	 * @hibernate.property column = "USER_ID" 
	 * 
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
}
