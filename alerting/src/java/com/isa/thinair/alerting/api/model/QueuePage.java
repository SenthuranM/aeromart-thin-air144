package com.isa.thinair.alerting.api.model;

import java.io.Serializable;

/**
 * @author sashrika
 * @hibernate.class table = "T_QUEUE_PAGE" //TBD
 *
 */
public class QueuePage implements Serializable{
	
	private int queueId;
	private String pageId;
	
	/**
	 * @return the queueId
	 * @hibernate.id column = "QUEUE_ID" generator-class = "assigned"
	 * 
	 */
	public int getQueueId() {
		return queueId;
	}
	
	/**
	 * @param queueId the queueId to set
	 */
	public void setQueueId(int queueId) {
		this.queueId = queueId;
	}

	/**
	 * @return the pageId
	 * @hibernate.id column = "PAGE_ID" generator-class = "assigned"
	 * 
	 */
	public String getPageId() {
		return pageId;
	}

	/**
	 * @param pageId the pageId to set
	 */
	public void setPageId(String pageId) {
		this.pageId = pageId;
	}

}
