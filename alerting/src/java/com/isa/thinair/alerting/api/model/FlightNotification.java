package com.isa.thinair.alerting.api.model;

import java.io.Serializable;
import java.sql.Clob;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @hibernate.class table="T_FLIGHT_NOTIFICATION"
 * 
 */
public class FlightNotification extends Persistent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7748483157712557152L;
	private long flightNotificationId;
	private String notificationCode;
	private long flightId;
	private Clob message;
	private String userId;
	private String language;

	/**
	 * @hibernate.id column = "FLIGHT_NOTIFICATION_ID" type = "java.lang.Long" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_FLIGHT_NOTIFICATION"
	 * 
	 */
	public long getFlightNotificationId() {
		return this.flightNotificationId;
	}

	public void setFlightNotificationId(long flightNotificationId) {
		this.flightNotificationId = flightNotificationId;
	}

	/**
	 * @hibernate.property column="NOTIFICATION_CODE"
	 */
	public String getNotificationCode() {
		return this.notificationCode;
	}

	public void setNotificationCode(String notificationCode) {
		this.notificationCode = notificationCode;
	}

	/**
	 * @hibernate.property column="FLIGHT_ID"
	 */
	public long getFlightId() {
		return this.flightId;
	}

	public void setFlightId(long flightId) {
		this.flightId = flightId;
	}

	/**
	 * @hibernate.property column="MESSAGE"
	 */
	public Clob getMessage() {
		return this.message;
	}

	public void setMessage(Clob message) {
		this.message = message;
	}

	/**
	 * @hibernate.property column="USER_ID"
	 */
	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @hibernate.property column="LANG"
	 */
	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

}
