package com.isa.thinair.alerting.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import com.isa.thinair.airschedules.api.dto.FlightAlertInfoDTO;

public class FlightNotificationDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5798758606068942568L;

	/** Holds the old flight information */
	private FlightAlertInfoDTO oldFlightAlertInfoDTO;

	/** Holds the new flight information */
	private FlightAlertInfoDTO newFlightAlertInfoDTO;

	/** Holds alert template id and the hashmap contains template parameters */
	private HashMap alertDetails;

	/** Holds customized SMS alert */
	private String smsMessageDesc;

	/** Holds the action for notification - Cancel, Reschedule etc */
	private String action;

	private boolean notifyBySms;

	private boolean notifyByEmail;

	private String currentNotificationCode;

	private String currentNotificationMsg;

	private String preferredLanguage;

	private String emailSubject;

	private String departureAirport;

	private String arrivalAirport;

	/**
	 * @return Returns the newFlightAlertInfoDTO.
	 */
	public FlightAlertInfoDTO getNewFlightAlertInfoDTO() {
		return newFlightAlertInfoDTO;
	}

	/**
	 * @param newFlightAlertInfoDTO
	 *            The newFlightAlertInfoDTO to set.
	 */
	public void setNewFlightAlertInfoDTO(FlightAlertInfoDTO newFlightAlertInfoDTO) {
		this.newFlightAlertInfoDTO = newFlightAlertInfoDTO;
	}

	/**
	 * @return Returns the oldFlightAlertInfoDTO.
	 */
	public FlightAlertInfoDTO getOldFlightAlertInfoDTO() {
		return oldFlightAlertInfoDTO;
	}

	/**
	 * @param oldFlightAlertInfoDTO
	 *            The oldFlightAlertInfoDTO to set.
	 */
	public void setOldFlightAlertInfoDTO(FlightAlertInfoDTO oldFlightAlertInfoDTO) {
		this.oldFlightAlertInfoDTO = oldFlightAlertInfoDTO;
	}

	/**
	 * @return Returns the alertDetails.
	 */
	public HashMap getAlertDetails() {
		return alertDetails;
	}

	/**
	 * @param alertDetails
	 *            The alertDetails to set.
	 */
	public void setAlertDetails(HashMap alertDetails) {
		this.alertDetails = alertDetails;
	}

	/**
	 * Return alert template ids
	 * 
	 * @return
	 */
	public Collection getAlertTemplateIds() {
		return new ArrayList(alertDetails.keySet());
	}

	/**
	 * 
	 * @return
	 */
	public String getSmsMessageDesc() {
		return smsMessageDesc;
	}

	/**
	 * 
	 * @param smsMessageDesc
	 */
	public void setSmsMessageDesc(String smsMessageDesc) {
		this.smsMessageDesc = smsMessageDesc;
	}

	/**
	 * 
	 * @return
	 */
	public String getAction() {
		return action;
	}

	/**
	 * 
	 * @param smsMessageDesc
	 */
	public void setAction(String action) {
		this.action = action;
	}

	public boolean isNotifyBySms() {
		return this.notifyBySms;
	}

	public void setNotifyBySms(boolean notifyBySms) {
		this.notifyBySms = notifyBySms;
	}

	public boolean isNotifyByEmail() {
		return this.notifyByEmail;
	}

	public void setNotifyByEmail(boolean notifyByEmail) {
		this.notifyByEmail = notifyByEmail;
	}

	public String getCurrentNotificationCode() {
		return this.currentNotificationCode;
	}

	public void setCurrentNotificationCode(String currentNotificationCode) {
		this.currentNotificationCode = currentNotificationCode;
	}

	public String getCurrentNotificationMsg() {
		return this.currentNotificationMsg;
	}

	public void setCurrentNotificationMsg(String currentNotificationMsg) {
		this.currentNotificationMsg = currentNotificationMsg;
	}

	public String getPreferredLanguage() {
		return preferredLanguage;
	}

	public void setPreferredLanguage(String preferredLanguage) {
		this.preferredLanguage = preferredLanguage;
	}

	public String getEmailSubject() {
		return emailSubject;
	}

	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}

	public String getDepartureAirport() {
		return departureAirport;
	}

	public void setDepartureAirport(String departureAirport) {
		this.departureAirport = departureAirport;
	}

	public String getArrivalAirport() {
		return arrivalAirport;
	}

	public void setArrivalAirport(String arrivalAirport) {
		this.arrivalAirport = arrivalAirport;
	}
}
