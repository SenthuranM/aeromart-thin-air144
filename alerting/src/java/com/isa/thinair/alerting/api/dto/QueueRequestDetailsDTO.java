/**
 * 
 */
package com.isa.thinair.alerting.api.dto;

import java.io.Serializable;
/**
 * @author suneth
 * 
 * This DTO is used to search QueueRequests
 *
 */
public class QueueRequestDetailsDTO implements Serializable{

	private static final long serialVersionUID = -6260586302689404526L;

	private String userId;
	
	private String queueId;
	
	private String applicablePageID;
	
	private String pnr;
	
	private String statusCode;
	
	private String fromDate = null;
	
	private String toDate = null;

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the queueId
	 */
	public String getQueueId() {
		return queueId;
	}

	/**
	 * @param queueId the queueId to set
	 */
	public void setQueueId(String queueId) {
		this.queueId = queueId;
	}

	/**
	 * @return the applicablePageID
	 */
	public String getApplicablePageID() {
		return applicablePageID;
	}

	/**
	 * @param applicablePageID the applicablePageID to set
	 */
	public void setApplicablePageID(String applicablePageID) {
		this.applicablePageID = applicablePageID;
	}

	/**
	 * @return the pnr
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr the pnr to set
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return the statusCode
	 */
	public String getStatusCode() {
		return statusCode;
	}

	/**
	 * @param statusCode the statusCode to set
	 */
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	/**
	 * @return the fromDate
	 */
	public String getFromDate() {
		return fromDate;
	}

	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * @return the toDate
	 */
	public String getToDate() {
		return toDate;
	}

	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	
}
