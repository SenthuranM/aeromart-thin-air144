package com.isa.thinair.alerting.api.dto;

import java.util.List;

public class RequestQueueDTO {
	private int queueId;
	private String queueName;
	private String description;	
	private List<String> pageIdList;	
	private List<String> pageNameList;
	private List<String> userNameList;
	private List<String> userIdList;	
	
	public int getQueueId() {
		return queueId;
	}
	public void setQueueId(int queueId) {
		this.queueId = queueId;
	}
	public List<String> getPageNameList() {
		return pageNameList;
	}
	public void setPageNameList(List<String> pageNameList) {
		this.pageNameList = pageNameList;
	}
	public List<String> getUserNameList() {
		return userNameList;
	}
	public void setUserNameList(List<String> userNameList) {
		this.userNameList = userNameList;
	}
	public List<String> getUserIdList() {
		return userIdList;
	}
	public void setUserIdList(List<String> userIdList) {
		this.userIdList = userIdList;
	}
	public List<String> getPageIdList() {
		return pageIdList;
	}
	public void setPageIdList(List<String> pageIdList) {
		this.pageIdList = pageIdList;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getQueueName() {
		return queueName;
	}
	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}

}