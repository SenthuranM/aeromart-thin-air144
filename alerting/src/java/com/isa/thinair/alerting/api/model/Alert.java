package com.isa.thinair.alerting.api.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @hibernate.class table="T_ALERT"
 * 
 */
public class Alert extends Persistent implements Serializable {

	private static final long serialVersionUID = -2286165608635652424L;

	/** identifier field */
	private Long alertId;

	/** nullable persistent field */
	private String content;

	/** nullable persistent field */
	private Date timestamp;

	/** nullable persistent field */
	private String status;

	/** nullable persistent field */
	private Long priorityCode;

	/** nullable persistent field */
	private Long alertTypeId;

	/** nullable persistent field */
	private Long pnrSegId;

	/** nullable persistent field */
	private Date originalDepDate;

	/** not nullable persistent field */
	private String visibleIbeOnly;

	/** not nullable persistent field */
	private String transferSelectedFlights;

	/** not nullable persistent field */
	private Set<Integer> fligthSegIds;
	
	/** not nullable presistent field */
	private String ibeActoined;

	/** full constructor */
	public Alert(String content, Date timestamp, String status, Long priorityCode, Long alertTypeId, Long pnrSegId,
			Date oroginalDepDate) {
		alert(content, timestamp, status, priorityCode, alertTypeId, pnrSegId, oroginalDepDate);
	}

	private void alert(String content, Date timestamp, String status, Long priorityCode, Long alertTypeId, Long pnrSegId,
			Date oroginalDepDate) {
		this.content = content;
		this.timestamp = timestamp;
		this.status = status;
		this.priorityCode = priorityCode;
		this.alertTypeId = alertTypeId;
		this.pnrSegId = pnrSegId;
		this.originalDepDate = oroginalDepDate;
		this.visibleIbeOnly = "N";
		this.transferSelectedFlights = "N";
		Set<Integer> fligthSegIdsEmpty = new HashSet<Integer>();
		this.fligthSegIds = fligthSegIdsEmpty;
		this.ibeActoined = "N";
	}

	/** minimum constructors */
	public Alert(String content, Integer pnrSegId) {
		alert(content, new Date(System.currentTimeMillis()), "", new Long(1), new Long(1), new Long(pnrSegId.longValue()),
				new Date(System.currentTimeMillis()));
	}

	/** default constructor */
	public Alert() {
		setDefaultValues();
	}

	/**
	 * 
	 * @hibernate.id column = "ALERT_ID" type = "java.lang.Long" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_ALERT"
	 * 
	 */
	public Long getAlertId() {
		return this.alertId;
	}

	public void setAlertId(Long alertId) {
		this.alertId = alertId;
	}

	/**
	 * @hibernate.property column="CONTENT" length="4000"
	 * 
	 */
	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @hibernate.property column="TIMESTAMP" length="7"
	 * 
	 */
	public Date getTimestamp() {
		return this.timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * @hibernate.property column="STATUS" length="15"
	 * 
	 */
	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @hibernate.property column="PRIORITY_CODE" length="10"
	 * 
	 */
	public Long getPriorityCode() {
		return this.priorityCode;
	}

	public void setPriorityCode(Long priorityCode) {
		this.priorityCode = priorityCode;
	}

	/**
	 * @hibernate.property column="ALERT_TYPE_ID" length="10"
	 * 
	 */
	public Long getAlertTypeId() {
		return this.alertTypeId;
	}

	public void setAlertTypeId(Long alertTypeId) {
		this.alertTypeId = alertTypeId;
	}

	/**
	 * @hibernate.property column="PNR_SEG_ID" length="10"
	 * 
	 */
	public Long getPnrSegId() {
		return this.pnrSegId;
	}

	public void setPnrSegId(Long pnrSegId) {
		this.pnrSegId = pnrSegId;
	}

	/**
	 * @hibernate.property column="ORIGINAL_DEP_DATE" length="7"
	 * 
	 */
	public Date getOriginalDepDate() {
		return originalDepDate;
	}

	public void setOriginalDepDate(Date originalDepDate) {
		this.originalDepDate = originalDepDate;
	}

	/**
	 * 
	 * @return the IBE visible Only flag
	 * @hibernate.property column = "VISIBLE_IBE_ONLY" length="1"
	 */
	public String getVisibleIbeOnly() {
		return visibleIbeOnly;
	}

	public void setVisibleIbeOnly(String visibleIbeOnly) {
		this.visibleIbeOnly = visibleIbeOnly;
	}

	/**
	 * 
	 * @return the transfer flight segment flag
	 * @hibernate.property column = "TRANSFER_SELECTED_FLIGHTS" length="1"
	 */
	public String getTransferSelectedFlights() {
		return transferSelectedFlights;
	}

	public void setTransferSelectedFlights(String transferSelectedFlights) {
		this.transferSelectedFlights = transferSelectedFlights;
	}

	/**
	 * @hibernate.set lazy="false" table="T_PNR_TRANSFER_FLIGHTS"
	 * @hibernate.collection-element column="FLT_SEG_ID" type="integer"
	 * @hibernate.collection-key column="ALERT_ID"
	 * 
	 * @see #fligthSegIds
	 */
	public Set<Integer> getFligthSegIds() {
		return fligthSegIds;
	}

	public void setFligthSegIds(Set<Integer> fligthSegIds) {
		this.fligthSegIds = fligthSegIds;
	}

	/**
	 * @return the ibeActoined
	 * @hibernate.property column = "IBE_ACTIONED"
	 */
	public String getIbeActoined() {
		return ibeActoined;
	}

	/**
	 * @param ibeActoined the ibeActoined to set
	 */
	public void setIbeActoined(String ibeActoined) {
		this.ibeActoined = ibeActoined;
	}

	public String toString() {
		return new ToStringBuilder(this).append("alertId", getAlertId()).toString();
	}

	public boolean equals(Object other) {
		if (!(other instanceof Alert))
			return false;
		Alert castOther = (Alert) other;
		return new EqualsBuilder().append(this.getAlertId(), castOther.getAlertId()).isEquals();
	}

	public int hashCode() {
		return new HashCodeBuilder().append(getAlertId()).toHashCode();
	}
	
	private void setDefaultValues() {
		this.visibleIbeOnly = "N";
		this.transferSelectedFlights = "N";
		this.ibeActoined = "N";
	}

}
