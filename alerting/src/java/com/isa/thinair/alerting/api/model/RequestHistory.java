/**
 * 
 */
package com.isa.thinair.alerting.api.model;

import java.io.Serializable;
import java.sql.Timestamp;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @author suneth
 * @hibernate.class table = "T_QUEUE_REQUEST_HISTORY" RequestHistory is the entity class to represent a RequestHistory model
 */
public class RequestHistory implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5239371987631263311L;
	
	private int historyId;
	
	private int requestId;
	
	private String action;
	
	private String statusChange;
	
	private String remark;
	
	private Timestamp timestamp;
	
	private String userId;

	public RequestHistory(){
		
	}
	
	public RequestHistory(int historyId,int requestId,String action,String statusChange,String remark,Timestamp timestamp,String userId){
		this.historyId=historyId;
		this.requestId=requestId;
		this.action=action;
		this.statusChange=statusChange;
		this.remark=remark;
		this.timestamp=timestamp;
		this.userId=userId;
	}
	
	/**
	 * Returns the request history Id
	 * 
	 * @return the historyId
	 * @hibernate.id column = "QUEUE_REQUEST_HISTORY_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_QUEUE_REQUEST_HISTORY"
	 * 
	 */
	public int getHistoryId() {
		return historyId;
	}

	/**
	 * Sets the request history Id
	 * 
	 * @param historyId the historyId to set
	 */
	public void setHistoryId(int historyId) {
		this.historyId = historyId;
	}

	/**
	 * Returns the request Id 
	 * 
	 * @return the requestId
	 * @hibernate.property column = "QUEUE_REQUEST_ID"
	 * 
	 */
	public int getRequestId() {
		return requestId;
	}

	/**
	 * Sets the request Id 
	 * 
	 * @param requestId the requestId to set
	 */
	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}

	/**
	 * Returns the action done
	 * 
	 * @return the action
	 * @hibernate.property column = "ACTION"
	 * 
	 */
	public String getAction() {
		return action;
	}

	/**
	 * Sets the action done
	 * 
	 * @param action the action to set
	 */
	public void setAction(String action) {
		this.action = action;
	}

	/**
	 * Returns the status change recored in the history
	 * 
	 * @return the statusChange
	 * @hibernate.property column = "STATUS_CHANGE"
	 * 
	 */
	public String getStatusChange() {
		return statusChange;
	}

	/**
	 * Sets the status change recored in the history
	 * 
	 * @param statusChange the statusChange to set
	 */
	public void setStatusChange(String statusChange) {
		this.statusChange = statusChange;
	}

	/**
	 * Returns the remark entered when a action is done to the request
	 * 
	 * @return the remark
	 * @hibernate.property column = "REMARKS"
	 * 
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * Sets the remark entered when a action is done to the request
	 * 
	 * @param remark the remark to set
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * Returns the timestamp of the history
	 * 
	 * @return the timestamp
	 * @hibernate.property column = "TIMESTAMP"
	 * 
	 */
	public Timestamp getTimestamp() {
		return timestamp;
	}

	/**
	 * Sets the timestamp of the history
	 * 
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * Returns the user Id of the user who have done the change to the request
	 * 
	 * @return the userId
	 * @hibernate.property column = "USER_ID"
	 * 
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * Sets the user Id of the user who have done the change to the request
	 * 
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	

}
