package com.isa.thinair.alerting.api.dto;

import java.io.Serializable;
import java.util.Date;

public class FlightPnrSearchNotificationDTO implements Serializable {

	private int flightId;

	private String flightNumber;

	private Date flightFromDate;

	private Date flightToDate;

	private Date depDate;

	private String pnr;

	private String route;

	private int noOfAdults;
	private int noOfChildren;
	private int noOfInfants;

	private String alertFlag;

	private String alertCreatedDate;

	private String inBoundConnections;

	private String outBoundConnections;
	
	// customize search
	private String segmentFrom;
	private String segmentTo;
	private Long specifyDelay;
	private String notificationCode;
	private String sortBy;
	private String sortByOrder;
	private Date alertCreatedFromDate;
	private Date alertCreatedToDate;
	private String daysOfOperation;
	
	private String originalFlightTime;
	
	private int totalPnrCount;

	private String searchReprotectAnciOption;

	public static final String ANY_ALERTS = "ANY";
	public static final String WITH_ALERTS = "WA";
	public static final String WITHOUT_ALERTS = "WOA";

	public static final String SEARCH_REPROTECT_ANCI_ANY = "REPROT_ANCI_ANY";
	public static final String SEARCH_REPROTECT_ANCI_FAILED = "REPROT_ANCI_FAILED";
	public static final String SEARCH_REPROTECT_ANCI_MEAL_FAILED = "REPROT_ANCI_MEAL_FAILED";
	public static final String SEARCH_REPROTECT_ANCI_SEAT_FAILED = "REPROT_ANCI_SEAT_FAILED";
	public static final String SEARCH_REPROTECT_ANCI_BAGGAGE_FAILED = "REPORT_ANCI_BAGGAGE_FAILED";

	public int getFlightId() {
		return flightId;
	}

	public void setFlightId(int flightId) {
		this.flightId = flightId;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public Date getFlightFromDate() {
		return flightFromDate;
	}

	public void setFlightFromDate(Date flightFromDate) {
		this.flightFromDate = flightFromDate;
	}

	public Date getFlightToDate() {
		return flightToDate;
	}

	public void setFlightToDate(Date flightToDate) {
		this.flightToDate = flightToDate;
	}

	public Date getDepDate() {
		return depDate;
	}

	public void setDepDate(Date depDate) {
		this.depDate = depDate;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getRoute() {
		return route;
	}

	public void setRoute(String route) {
		this.route = route;
	}

	public int getNoOfAdults() {
		return noOfAdults;
	}

	public void setNoOfAdults(int noOfAdults) {
		this.noOfAdults = noOfAdults;
	}

	public int getNoOfChildren() {
		return noOfChildren;
	}

	public void setNoOfChildren(int noOfChildren) {
		this.noOfChildren = noOfChildren;
	}

	public int getNoOfInfants() {
		return noOfInfants;
	}

	public void setNoOfInfants(int noOfInfants) {
		this.noOfInfants = noOfInfants;
	}

	public String getAlertFlag() {
		return alertFlag;
	}

	public void setAlertFlag(String alertFlag) {
		this.alertFlag = alertFlag;
	}

	public String getAlertCreatedDate() {
		return alertCreatedDate;
	}

	public void setAlertCreatedDate(String alertCreatedDate) {
		this.alertCreatedDate = alertCreatedDate;
	}

	public String getInBoundConnections() {
		return inBoundConnections;
	}

	public void setInBoundConnections(String inBoundConnections) {
		this.inBoundConnections = inBoundConnections;
	}

	public String getOutBoundConnections() {
		return outBoundConnections;
	}

	public void setOutBoundConnections(String outBoundConnections) {
		this.outBoundConnections = outBoundConnections;
	}

	public String getSegmentFrom() {
		return segmentFrom;
	}

	public void setSegmentFrom(String segmentFrom) {
		this.segmentFrom = segmentFrom;
	}

	public String getSegmentTo() {
		return segmentTo;
	}

	public void setSegmentTo(String segmentTo) {
		this.segmentTo = segmentTo;
	}

	public Long getSpecifyDelay() {
		return specifyDelay;
	}

	public void setSpecifyDelay(Long specifyDelay) {
		this.specifyDelay = specifyDelay;
	}

	public String getNotificationCode() {
		return notificationCode;
	}

	public void setNotificationCode(String notificationCode) {
		this.notificationCode = notificationCode;
	}

	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	public String getSortByOrder() {
		return sortByOrder;
	}

	public void setSortByOrder(String sortByOrder) {
		this.sortByOrder = sortByOrder;
	}

	public Date getAlertCreatedFromDate() {
		return alertCreatedFromDate;
	}

	public void setAlertCreatedFromDate(Date alertCreatedFromDate) {
		this.alertCreatedFromDate = alertCreatedFromDate;
	}

	public Date getAlertCreatedToDate() {
		return alertCreatedToDate;
	}

	public void setAlertCreatedToDate(Date alertCreatedToDate) {
		this.alertCreatedToDate = alertCreatedToDate;
	}

	public String getDaysOfOperation() {
		return daysOfOperation;
	}

	public void setDaysOfOperation(String daysOfOperation) {
		this.daysOfOperation = daysOfOperation;
	}
 
	public String getOriginalFlightTime() {
		return originalFlightTime;
	}

	public void setOriginalFlightTime(String originalFlightTime) {
		this.originalFlightTime = originalFlightTime;
	}

	public int getTotalPnrCount() {
		return totalPnrCount;
	}

	public void setTotalPnrCount(int totalPnrCount) {
		this.totalPnrCount = totalPnrCount;
	}

	public String getSearchReprotectAnciOption() {
		return searchReprotectAnciOption;
	}

	public void setSearchReprotectAnciOption(String searchReprotectAnciOption) {
		this.searchReprotectAnciOption = searchReprotectAnciOption;
	}

}
