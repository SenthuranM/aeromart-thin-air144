package com.isa.thinair.alerting.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * DTO for getting Flight Pnr Notification details
 * 
 * @author Dhanya
 */

public class FlightPnrNotificationDTO implements Serializable {

	private static final long serialVersionUID = 5833695737924456314L;

	private Date notificationtDate;

	private String flightNumber;

	private Date depDate;

	private String pnr;

	private String firstName;

	private String title;

	private int flightId;

	private int pnrSegmentId;

	private String notificationCode;

	private String searchMobileOption;

	private String searchEmailOption;

	private String searchLandPhoneOption;

	private String searchReprotectAnciOption;

	private String mobileNumberPattern;

	private String route;

	private String mobile;

	private String email;

	private String landPhone;

	private String notificationMsg;

	private String passengerName;

	private boolean isNewNotification;

	private boolean isSendSms;

	private boolean isSendEmail;

	private ArrayList notificationChannel = new ArrayList();

	private HashMap deliveryStatus = new HashMap();

	private ArrayList notifications = null;

	private Date dateFrom = null;

	private Date dateTo = null;

	private int flightNotificationId = -1;

	// Filter by Alert
	private String alert;

	private String preferredLanguage;

	private String languageOption;

	private int totalPax;
	private int noOfAdults;
	private int noOfChildren;
	private int noOfInfants;

	private Date alertCreatedFromDate;
	private Date alertCreatedToDate;
	private Date alertCreatedActualDate;

	private String inBoundFlightNo;
	private String inBoundDepartureStation;
	private String inBoundDepartureDate;

    private String outBoundFlightNo;
	private String outBoundSegmentCode;
	private String outBoundDepartureDate;

	private String segmentFrom;
	private String segmentTo;
	// Sort by Alert
	private String sortBy;
	private String sortByOrder;

	private int totalSms = 0;
	private int totalEmail = 0;
	private int totalSmsDelivered = 0;
	private int totalSmsFailed = 0;
	private int totalEmailDelivered = 0;
	private int totalEmailFailed = 0;

	private String failedEmailList;
	private String failedSmsList;
	private String segmentCode;

	public static final String SEARCH_ALL = "ALL";
	public static final String SEARCH_ALL_VALID = "ALV";
	public static final String SEARCH_WITHOUT_EMAIL = "WOE";
	public static final String SEARCH_WITHOUT_MOBILE = "WOM";
	public static final String SEARCH_WITHOUT_MOBILE_EMAIL = "WEM";
	public static final String ANY_ALERTS = "ANY";
	public static final String WITH_ALERTS = "WA";
	public static final String WITHOUT_ALERTS = "WOA";
	public static final String LANG_EXCLUDE = "ex";
	public static final String LANG_INCLUDE = "in";

	public static final String SEARCH_REPROTECT_ANCI_ANY = "REPROT_ANCI_ANY";
	public static final String SEARCH_REPROTECT_ANCI_FAILED = "REPROT_ANCI_FAILED";
	public static final String SEARCH_REPROTECT_ANCI_MEAL_FAILED = "REPROT_ANCI_MEAL_FAILED";
	public static final String SEARCH_REPROTECT_ANCI_SEAT_FAILED = "REPROT_ANCI_SEAT_FAILED";
	public static final String SEARCH_REPROTECT_ANCI_BAGGAGE_FAILED = "REPORT_ANCI_BAGGAGE_FAILED";

	public Date getNotificationtDate() {
		return this.notificationtDate;
	}

	public void setNotificationtDate(Date notificationtDate) {
		this.notificationtDate = notificationtDate;
	}

	public String getFlightNumber() {
		return this.flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getPnr() {
		return this.pnr;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle() {return this.title;}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getFirstName() {return this.firstName;}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public Date getDepDate() {
		return this.depDate;
	}

	public void setDepDate(Date depDate) {
		this.depDate = depDate;
	}

	public String getNotificationCode() {
		return this.notificationCode;
	}

	public void setNotificationCode(String notificationCode) {
		this.notificationCode = notificationCode;
	}

	public String getSearchMobileOption() {
		return this.searchMobileOption;
	}

	public void setSearchMobileOption(String searchMobileOption) {
		this.searchMobileOption = searchMobileOption;
	}

	public String getSearchEmailOption() {
		return this.searchEmailOption;
	}

	public void setSearchEmailOption(String searchEmailOption) {
		this.searchEmailOption = searchEmailOption;
	}

	public String getSearchLandPhoneOption() {
		return this.searchLandPhoneOption;
	}

	public void setSearchLandPhoneOption(String searchLandPhoneOption) {
		this.searchLandPhoneOption = searchLandPhoneOption;
	}

	public String getRoute() {
		return this.route;
	}

	public void setRoute(String route) {
		this.route = route;
	}

	public String getMobile() {
		return this.mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLandPhone() {
		return this.landPhone;
	}

	public void setLandPhone(String landPhone) {
		this.landPhone = landPhone;
	}

	public String getNotificationMsg() {
		return this.notificationMsg;
	}

	public void setNotificationMsg(String notificationMsg) {
		this.notificationMsg = notificationMsg;
	}

	public boolean isNewNotification() {
		return this.isNewNotification;
	}

	public void setIsNewNotification(boolean isNewNotification) {
		this.isNewNotification = isNewNotification;
	}

	public ArrayList getNotificationChannel() {
		return this.notificationChannel;
	}

	public void setNotificationChannel(ArrayList notificationChannel) {
		this.notificationChannel = notificationChannel;
	}

	public HashMap getDeliveryStatus() {
		return this.deliveryStatus;
	}

	public void setDeliveryStatus(HashMap deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}

	public ArrayList getNotifications() {
		return this.notifications;
	}

	public void setNotifications(ArrayList notifications) {
		this.notifications = notifications;
	}

	public int getFlightId() {
		return this.flightId;
	}

	public void setFlightId(int flightId) {
		this.flightId = flightId;
	}

	public int getPnrSegmentId() {
		return this.pnrSegmentId;
	}

	public void setPnrSegmentId(int pnrSegmentId) {
		this.pnrSegmentId = pnrSegmentId;
	}

	public String getMobileNumberPattern() {
		return this.mobileNumberPattern;
	}

	public void setMobileNumberPattern(String mobileNumberPattern) {
		this.mobileNumberPattern = mobileNumberPattern;
	}

	public boolean isSendSms() {
		return this.isSendSms;
	}

	public void setSendSms(boolean isSendSms) {
		this.isSendSms = isSendSms;
	}

	public boolean isSendEmail() {
		return this.isSendEmail;
	}

	public void setSendEmail(boolean isSendEmail) {
		this.isSendEmail = isSendEmail;
	}

	public String getPassengerName() {
		return this.passengerName;
	}

	public void setPassengerName(String passengerName) {
		this.passengerName = passengerName;
	}

	public Date getDateFrom() {
		return this.dateFrom;
	}

	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Date getDateTo() {
		return this.dateTo;
	}

	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	public int getFlightNotificationId() {
		return this.flightNotificationId;
	}

	public void setFlightNotificationId(int flightNotificationId) {
		this.flightNotificationId = flightNotificationId;
	}

	// ///////
	public int getTotalSms() {
		return this.totalSms;
	}

	public void setTotalSms(int totalSms) {
		this.totalSms = totalSms;
	}

	public int getTotalEmail() {
		return this.totalEmail;
	}

	public void setTotalEmail(int totalEmail) {
		this.totalEmail = totalEmail;
	}

	public int getTotalSmsDelivered() {
		return this.totalSmsDelivered;
	}

	public void setTotalSmsDelivered(int totalSmsDelivered) {
		this.totalSmsDelivered = totalSmsDelivered;
	}

	public int getTotalSmsFailed() {
		return this.totalSmsFailed;
	}

	public void setTotalSmsFailed(int totalSmsFailed) {
		this.totalSmsFailed = totalSmsFailed;
	}

	public int getTotalEmailDelivered() {
		return this.totalEmailDelivered;
	}

	public void setTotalEmailDelivered(int totalEmailDelivered) {
		this.totalEmailDelivered = totalEmailDelivered;
	}

	public int getTotalEmailFailed() {
		return this.totalEmailFailed;
	}

	public void setTotalEmailFailed(int totalEmailFailed) {
		this.totalEmailFailed = totalEmailFailed;
	}

	public String getFailedEmailList() {
		return failedEmailList;
	}

	public void setFailedEmailList(String failedEmailList) {
		this.failedEmailList = failedEmailList;
	}

	public String getFailedSmsList() {
		return failedSmsList;
	}

	public void setFailedSmsList(String failedSmsList) {
		this.failedSmsList = failedSmsList;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public String getAlert() {
		return alert;
	}

	public void setAlert(String alert) {
		this.alert = alert;
	}

	/**
	 * @return the preferredLanguage
	 */
	public String getPreferredLanguage() {
		return preferredLanguage;
	}

	/**
	 * @param preferredLanguage
	 *            the preferredLanguage to set
	 */
	public void setPreferredLanguage(String preferredLanguage) {
		this.preferredLanguage = preferredLanguage;
	}

	/**
	 * @return the languageOption
	 */
	public String getLanguageOption() {
		return languageOption;
	}

	/**
	 * @param languageOption
	 *            the languageOption to set
	 */
	public void setLanguageOption(String languageOption) {
		this.languageOption = languageOption;
	}

	/**
	 * @return The search re-protect anci option.
	 */
	public String getSearchReprotectAnciOption() {
		return searchReprotectAnciOption;
	}

	/**
	 * param searchReprotectAnciOption : Reprotect anci option to set.
	 */
	public void setSearchReprotectAnciOption(String searchReprotectAnciOption) {
		this.searchReprotectAnciOption = searchReprotectAnciOption;
	}

	/**
	 * @return the totalPax
	 */
	public int getTotalPax() {
		return totalPax;
	}

	/**
	 * @param totalPax
	 *            the totalPax to set
	 */
	public void setTotalPax(int totalPax) {
		this.totalPax = totalPax;
	}

	/**
	 * @return the sortBy
	 */
	public String getSortBy() {
		return sortBy;
	}

	/**
	 * @param sortBy
	 *            the sortBy to set
	 */
	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	/**
	 * @return the sortByOrder
	 */
	public String getSortByOrder() {
		return sortByOrder;
	}

	/**
	 * @param sortByOrder
	 *            the sortByOrder to set
	 */
	public void setSortByOrder(String sortByOrder) {
		this.sortByOrder = sortByOrder;
	}

	public int getNoOfAdults() {
		return noOfAdults;
	}

	public void setNoOfAdults(int noOfAdults) {
		this.noOfAdults = noOfAdults;
	}

	public int getNoOfChildren() {
		return noOfChildren;
	}

	public void setNoOfChildren(int noOfChildren) {
		this.noOfChildren = noOfChildren;
	}

	public int getNoOfInfants() {
		return noOfInfants;
	}

	public void setNoOfInfants(int noOfInfants) {
		this.noOfInfants = noOfInfants;
	}

	public Date getAlertCreatedFromDate() {
		return alertCreatedFromDate;
	}

	public void setAlertCreatedFromDate(Date alertCreatedFromDate) {
		this.alertCreatedFromDate = alertCreatedFromDate;
	}

	public Date getAlertCreatedToDate() {
		return alertCreatedToDate;
	}

	public void setAlertCreatedToDate(Date alertCreatedToDate) {
		this.alertCreatedToDate = alertCreatedToDate;
	}

	public Date getAlertCreatedActualDate() {
		return alertCreatedActualDate;
	}

	public void setAlertCreatedActualDate(Date alertCreatedActualDate) {
		this.alertCreatedActualDate = alertCreatedActualDate;
	}

	/**
	 * @return the inBoundFlightNo
	 */
	public String getInBoundFlightNo() {
		return inBoundFlightNo;
	}

	/**
	 * @param inBoundFlightNo
	 *            the inBoundFlightNo to set
	 */
	public void setInBoundFlightNo(String inBoundFlightNo) {
		this.inBoundFlightNo = inBoundFlightNo;
	}

	/**
	 * @return the inBoundDepartureStation
	 */
	public String getInBoundDepartureStation() {
		return inBoundDepartureStation;
	}

	/**
	 * @param inBoundDepartureStation
	 *            the inBoundDepartureStation to set
	 */
	public void setInBoundDepartureStation(String inBoundDepartureStation) {
		this.inBoundDepartureStation = inBoundDepartureStation;
	}

	/**
	 * @return the inBoundDepartureDate
	 */
	public String getInBoundDepartureDate() {
		return inBoundDepartureDate;
	}

	/**
	 * @param inBoundDepartureDate
	 *            the inBoundDepartureDate to set
	 */
	public void setInBoundDepartureDate(String inBoundDepartureDate) {
		this.inBoundDepartureDate = inBoundDepartureDate;
	}

	/**
	 * @return the segmentFrom
	 */
	public String getSegmentFrom() {
		return segmentFrom;
	}

	/**
	 * @param segmentFrom
	 *            the segmentFrom to set
	 */
	public void setSegmentFrom(String segmentFrom) {
		this.segmentFrom = segmentFrom;
	}

	/**
	 * @return the segmentTo
	 */
	public String getSegmentTo() {
		return segmentTo;
	}

	/**
	 * @param segmentTo
	 *            the segmentTo to set
	 */
	public void setSegmentTo(String segmentTo) {
		this.segmentTo = segmentTo;
	}

    public String getOutBoundFlightNo() {
        return outBoundFlightNo;
    }

    public void setOutBoundFlightNo(String outBoundFlightNo) {
        this.outBoundFlightNo = outBoundFlightNo;
    }

    public String getOutBoundSegmentCode() {
        return outBoundSegmentCode;
    }

    public void setOutBoundSegmentCode(String outBoundSegmentCode) {
        this.outBoundSegmentCode = outBoundSegmentCode;
    }

    public String getOutBoundDepartureDate() {
        return outBoundDepartureDate;
    }

    public void setOutBoundDepartureDate(String outBoundDepartureDate) {
        this.outBoundDepartureDate = outBoundDepartureDate;
    }
}
