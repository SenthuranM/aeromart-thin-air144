package com.isa.thinair.alerting.api.model;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @author sashrika
 * @hibernate.class table = "T_PAGE" // description goes here
 *
 */
public class ApplicablePage implements Serializable {
	
	private String pageId;
	
	private String pageName;
	
	private String description;

	/**
	 * @return the pageId
	 * @hibernate.id column = "PAGE_ID" generator-class = "native"
	 * 
	 */
	public String getPageId() {
		return pageId;
	}

	/**
	 * @param pageId the pageId to set
	 */
	public void setPageId(String pageId) {
		this.pageId = pageId;
	}

	/**
	 * @return the pageName
	 * @hibernate.property column = "PAGE_NAME" 
	 * 
	 */
	public String getPageName() {
		return pageName;
	}


	/**
	 * @param pageName the pageName to set
	 */
	public void setPageName(String pageName) {
		this.pageName = pageName;
	}

	/**
	 * @return the pageName
	 * @hibernate.property column = "PAGE_DESCRIPTION"
	 * 
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	

}
