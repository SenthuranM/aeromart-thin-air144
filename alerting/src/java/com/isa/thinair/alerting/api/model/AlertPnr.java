package com.isa.thinair.alerting.api.model;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @hibernate.class table="T_ALERT_PNR"
 * 
 */
public class AlertPnr extends Persistent implements Serializable {

	private static final long serialVersionUID = 7406798017046386634L;

	/** identifier field */
	private Long altPnrId;

	/** nullable persistent field */
	private String pnr;

	/** nullable persistent field */
	private String status;

	/** persistent field */
	private Alert alert;

	/** full constructor */
	public AlertPnr(String status, String pnr, Alert alert) {
		this.pnr = pnr;
		this.status = status;
		this.alert = alert;
	}

	/** default constructor */
	public AlertPnr() {
	}

	/** minimal constructor */
	public AlertPnr(Alert alert) {
		this.alert = alert;
	}

	/**
	 * @hibernate.id generator-class="increment" type="java.lang.Long" column="ALT_PNR_ID"
	 * 
	 */
	public Long getAltPnrId() {
		return this.altPnrId;
	}

	public void setAltPnrId(Long altPnrId) {
		this.altPnrId = altPnrId;
	}

	/**
	 * @hibernate.property column="PNR"
	 * 
	 */
	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @hibernate.property column="STATUS" length="15"
	 * 
	 */
	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @hibernate.many-to-one not-null="true"
	 * @hibernate.column name="ALERT_ID"
	 * 
	 */
	public Alert getAlert() {
		return this.alert;
	}

	public void setAlert(Alert alert) {
		this.alert = alert;
	}

	public String toString() {
		return new ToStringBuilder(this).append("altPnrId", getAltPnrId()).toString();
	}

	public boolean equals(Object other) {
		if (!(other instanceof AlertPnr))
			return false;
		AlertPnr castOther = (AlertPnr) other;
		return new EqualsBuilder().append(this.getAltPnrId(), castOther.getAltPnrId()).isEquals();
	}

	public int hashCode() {
		return new HashCodeBuilder().append(getAltPnrId()).toHashCode();
	}

}
