package com.isa.thinair.alerting.api.service;

import javax.sql.DataSource;

import com.isa.thinair.airsecurity.api.service.SecurityBD;
import com.isa.thinair.airsecurity.api.utils.AirsecurityConstants;
import com.isa.thinair.alerting.api.utils.AlertingConstants;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GenericLookupUtils;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;
import com.isa.thinair.messaging.api.utils.MessagingConstants;
import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;

public class AlertingModuleUtils {

	public static IModule getInstance() {
		return LookupServiceFactory.getInstance().getModule(AlertingConstants.MODULE_NAME);
	}

	public static DataSource getDatasource() {
		LookupService lookup = LookupServiceFactory.getInstance();
		return (DataSource) lookup.getBean(PlatformBeanUrls.Commons.DEFAULT_DATA_SOURCE_BEAN);
	}

	public static MessagingServiceBD getMessagingServiceBD() {
		return (MessagingServiceBD) lookupEJB3Service(MessagingConstants.MODULE_NAME, MessagingServiceBD.SERVICE_NAME);
	}

	private static Object lookupEJB3Service(String targetModuleName, String serviceName) {
		return GenericLookupUtils.lookupEJB3Service(targetModuleName, serviceName, getInstance().getModuleConfig(), "alerting",
				"config.dependencymap.invalid");
	}

	public static SecurityBD getSecurityBD() {
		return (SecurityBD) lookupEJB3Service(AirsecurityConstants.MODULE_NAME, SecurityBD.SERVICE_NAME);
	}
	/**
	 * Return the global configuration
	 * 
	 * @return
	 */
	public static GlobalConfig getGlobalConfig() {
		return CommonsServices.getGlobalConfig();
	}
}
