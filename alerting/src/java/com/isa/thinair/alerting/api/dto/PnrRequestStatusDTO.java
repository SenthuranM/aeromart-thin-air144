package com.isa.thinair.alerting.api.dto;

import java.io.Serializable;
import java.util.Date;

public class PnrRequestStatusDTO implements Serializable{
	private int requestId;
	private int queueId;
	private String description;
	private String status;
	private String assignedTo;
	private Date assignedDate;
	
	public PnrRequestStatusDTO(int requestId,int queueId,String description,String status,String assignedTo ,Date assignedDate){
		this.requestId=requestId;
		this.queueId=queueId;
		this.description=description;
		this.status=status;
		this.assignedTo=assignedTo;
		this.assignedDate=assignedDate;
	}
	
	public int getRequestId() {
		return requestId;
	}
	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}
	
	public int getQueueId() {
		return queueId;
	}

	public void setQueueId(int queueId) {
		this.queueId = queueId;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getAssignedTo() {
		return assignedTo;
	}
	public void setAssignedTo(String assignedTo) {
		this.assignedTo = assignedTo;
	}
	public Date getAssignedDate() {
		return assignedDate;
	}
	public void setAssignedDate(Date assignedDate) {
		this.assignedDate = assignedDate;
	}
	
}
