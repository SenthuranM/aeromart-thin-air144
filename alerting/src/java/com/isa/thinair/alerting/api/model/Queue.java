/**
 * 
 */
package com.isa.thinair.alerting.api.model;

import java.io.Serializable;
import java.util.Set;
import com.isa.thinair.commons.core.framework.Tracking;
import com.isa.thinair.commons.core.security.UserPrincipal;

/**
 * @author suneth
 * @hibernate.class table = "T_QUEUE" Queue is the entity class to represent a Queue model
 *
 */
public class Queue extends Tracking implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6647761991787126823L;
	
	private int queueId;	
	private String queueName;	
	private String description;	
	private String status;
	private Set<ApplicablePage> applicablePages;	

	/**
	 * @hibernate.set table="T_QUEUE_PAGE" cascade="none" readonly="true"
	 *                where="Exists (select * from T_PAGE where PAGE_ID = T_PAGE.PAGE_ID )"
	 * @hibernate.collection-key column="QUEUE_ID"
	 * @hibernate.collection-many-to-many class="com.isa.thinair.alerting.api.model.ApplicablePage" column="PAGE_ID"
	 * 
	 */
	public Set<ApplicablePage> getApplicablePages() {
		return applicablePages;
	}

	/**
	 * Sets the applicablePages object set of this applicablePages
	 * 
	 * @param applicablePages the applicablePages to set
	 */
	public void setApplicablePages(Set<ApplicablePage> applicablePages) {
		this.applicablePages = applicablePages;
	}
	
	
	public Queue(UserPrincipal userPrincipal){		
		setUserDetails(userPrincipal);		
	}

	public Queue(){		 // no argument constructor
		
	}

	/**
	 * @return the queueId
	 * @hibernate.id column = "QUEUE_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_QUEUE"
	 */
	public int getQueueId() {
		return queueId;
	}

	/**
	 * @param queueId the queueId to set
	 */
	public void setQueueId(int queueId) {
		this.queueId = queueId;
	}

	/**
	 * @return the queueName
	 * @hibernate.property  column = "QUEUE_NAME" 
	 * 
	 */
	public String getQueueName() {
		return queueName;
	}

	/**
	 * @param queueName the queueName to set
	 */
	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}

	/**
	 * @return the description
	 * @hibernate.property  column = "QUEUE_DESCRIPTION" 
	 * 
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * @return the status 
	 * @hibernate.property column="STATUS"  
	 * 
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}	


}
