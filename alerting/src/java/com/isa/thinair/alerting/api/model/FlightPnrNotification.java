package com.isa.thinair.alerting.api.model;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @hibernate.class table="T_FLIGHT_PNR_NOTIFICATION"
 * 
 */
public class FlightPnrNotification extends Persistent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6764245673386667705L;
	private long flightPnrNotificationId;
	private Date notificationDate;
	private long flightNotificationId;
	private long pnrSegId;
	private String notificationChannel;
	private String deliveryStatus;
	private String userId;
	private String recepients;

	/**
	 * 
	 * @hibernate.id column = "FLIGHT_PNR_NOTIFICATION_ID" type = "java.lang.Long" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_FLIGHT_PNR_NOTIFICATION"
	 * 
	 */
	public long getFlightPnrNotificationId() {
		return this.flightPnrNotificationId;
	}

	public void setFlightPnrNotificationId(long flightPnrNotificationId) {
		this.flightPnrNotificationId = flightPnrNotificationId;
	}

	/**
	 * @hibernate.property column="FLIGHT_NOTIFICATION_ID"
	 */
	public long getFlightNotificationId() {
		return this.flightNotificationId;
	}

	public void setFlightNotificationId(long flightNotificationId) {
		this.flightNotificationId = flightNotificationId;
	}

	/**
	 * @hibernate.property column="NOTIFICATION_DATE"
	 * 
	 */
	public Date getNotificationDate() {
		return this.notificationDate;
	}

	public void setNotificationDate(Date notificationDate) {
		this.notificationDate = notificationDate;
	}

	/**
	 * @hibernate.property column="PNR_SEG_ID"
	 */
	public long getPnrSegId() {
		return this.pnrSegId;
	}

	public void setPnrSegId(long pnrSegId) {
		this.pnrSegId = pnrSegId;
	}

	/**
	 * @hibernate.property column="NOTIFICATION_CHANNEL"
	 */
	public String getNotificationChannel() {
		return this.notificationChannel;
	}

	public void setNotificationChannel(String notificationChannel) {
		this.notificationChannel = notificationChannel;
	}

	/**
	 * @hibernate.property column="DELIVERY_STATUS"
	 */
	public String getDeliveryStatus() {
		return this.deliveryStatus;
	}

	public void setDeliveryStatus(String deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}

	/**
	 * @hibernate.property column="USER_ID"
	 */
	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @hibernate.property column="RECEPIENTS"
	 */
	public String getRecepients() {
		return this.recepients;
	}

	public void setRecepients(String recepients) {
		this.recepients = recepients;
	}

}
