/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:12:34
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.alerting.api.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.isa.thinair.alerting.api.dto.AlertDetailDTO;
import com.isa.thinair.alerting.api.dto.FlightNotificationDTO;
import com.isa.thinair.alerting.api.dto.FlightPnrNotificationDTO;
import com.isa.thinair.alerting.api.dto.FlightPnrSearchNotificationDTO;
import com.isa.thinair.alerting.api.dto.QueueRequestDetailsDTO;
import com.isa.thinair.alerting.api.dto.RequestQueueUser;
import com.isa.thinair.alerting.api.model.Alert;
import com.isa.thinair.alerting.api.model.AlertTemplate;
import com.isa.thinair.alerting.api.model.ApplicablePage;
import com.isa.thinair.alerting.api.model.FlightNotification;
import com.isa.thinair.alerting.api.model.FlightPnrNotification;
import com.isa.thinair.alerting.api.model.Queue;
import com.isa.thinair.alerting.api.model.QueueRequest;
import com.isa.thinair.alerting.api.model.RequestHistory;
import com.isa.thinair.alerting.api.util.AlertTemplateEnum;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * @isa.module.bd-intf id="alerting.service"
 */
public interface AlertingBD {

	public List retrieveAlerts(Collection alertIds) throws ModuleException;

	public AlertTemplate getAlertTemplate(AlertTemplateEnum templateID) throws ModuleException;

	public List retrieveAlertsForPrivileges(List privilegeIds) throws ModuleException;

	public List retrieveAlertsForUser(String userId) throws ModuleException;

	public List retrieveAlertsForPnrSegments(List pnrSegmentIds) throws ModuleException;

	public Map getAlertsForPnrSegments(Collection pnrSegmentIds) throws ModuleException;

	public Page retrieveAlertsForSearchCriteria(AlertDetailDTO searchCriteria) throws ModuleException;

	public void addAlert(Alert alert) throws ModuleException;

	public void addAlerts(Collection alertsList) throws ModuleException;

	public void updateAlert(Alert alert) throws ModuleException;

	public void updateAlerts(Collection<Alert> alerts) throws ModuleException;

	public void removeAlerts(Collection alertIds) throws ModuleException;

	public void removeAlertsOfPnrSegments(List pnrSegmentIds) throws ModuleException;

	public Collection<Integer> getPNRSegIDsOfAlerts(Collection<Integer> colAlertIDs) throws ModuleException;

	public Page retrieveFlightPnrDetailsForSearchCriteria(FlightPnrNotificationDTO searchCriteria) throws ModuleException;

	public Page retrieveFlightPnrDetailsForViewSearchCriteria(FlightPnrNotificationDTO searchCriteria) throws ModuleException;

	public void notifyReservations(int flightId, FlightNotificationDTO fltDTO, Collection colFlightPnrDTO,
			UserPrincipal userPrincipal) throws ModuleException;

	public String getNextNotificationCode(String baseCode) throws ModuleException;

	public ArrayList getNotificationsForPnr(int pnrSegId) throws ModuleException;

	public int saveFlightNotification(FlightNotification obj) throws ModuleException;

	public void saveFlightPnrNotification(FlightPnrNotification obj) throws ModuleException;

	public ArrayList getAllNotificationsForFlight(int flightId, String pnr) throws ModuleException;

	public Page retrieveDetailedNotificationList(int fltNotificationId, Date notifyDate, String pnr) throws ModuleException;

	public Integer getFlightID(String fltNum, Date depDateLocal) throws ModuleException;
	
	public Page retrieveSubmittedRequests(QueueRequestDetailsDTO searchCriteria) throws ModuleException;
	
	public void submitOrEditRequest(QueueRequest queueRequest) throws ModuleException;
	
	public QueueRequest getQueueRequest(int requestId) throws ModuleException;
	
	public void addRequestHistory(RequestHistory requestHistory) throws ModuleException;
	
	public void deleteRequest(int requestId) throws ModuleException;
	
	public ServiceResponce deleteRequestQueue(int queueId) throws ModuleException;
	
	public Page searchRequestQueue(int page, int totalper, String queueName, String applicablePage) throws ModuleException;
	
	public List<RequestQueueUser> searchUser(String userNamePart) throws ModuleException;
	
	public void updateRequestQueue(Queue queue, List<String> pageList) throws ModuleException;
	
	public void saveRequestQueue(Queue queue,List<String> pagelist,List<String> add) throws ModuleException;
	
	public void addRemoveUsers(List<String> add,List<String> remove,int queue) throws ModuleException;
	
	public Queue readRequestQueue(int queue) throws ModuleException;
	
	public List<ApplicablePage> searchPages(String pageNamePart) throws ModuleException;	
	
	public Page getRequestHistory(int requestId) throws ModuleException;
	
	public Page retrieveSubmittedRequestsForAction(String userID , String queueId , String status) throws ModuleException;	
	
	public Page getRequestStatusForPnr(int page, int maxResult,String pnrId) throws ModuleException;
	
	public Page getHistoryForRequest(int page, int maxResult,int requestId) throws ModuleException;

    public List<Integer> getFlightIDList(String fltNum, Date startDateLocal,Date endDateLocal) throws ModuleException;

	public Page retrieveFlightDetailsForSearchCriteria(FlightPnrSearchNotificationDTO searchCriteria) throws ModuleException;

	public Page retrieveMessageListForLoadCriteria(Date depDate) throws ModuleException;
}
