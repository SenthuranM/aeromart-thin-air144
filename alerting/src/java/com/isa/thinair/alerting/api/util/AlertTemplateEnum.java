package com.isa.thinair.alerting.api.util;

import java.io.Serializable;

/**
 * @author Lasantha Pambagoda
 */
public class AlertTemplateEnum implements Serializable {

	private static final long serialVersionUID = 82165498457490706L;
	private int code;

	public static final String TRANSPORT_SMS = "SMS";
	public static final String TRANSPORT_EMAIL = "EMAIL";
	public static final String TRANSPORT_ALERT = "ALERT";

	private AlertTemplateEnum(int code) {
		this.code = code;
	}

	public boolean equals(AlertTemplateEnum alertTemplate) {
		return (alertTemplate.code == this.code);
	}

	public int getCode() {
		return code;
	}

	public String getCodeDesc() {
		String codeDesc = "";
		if (getCode() == 0) {
			codeDesc = "Flight Number Change";
		} else if (getCode() == 1) {
			codeDesc = "Date Time Change";
		} else if (getCode() == 2) {
			codeDesc = "Route Change";
		} else if (getCode() == 3) {
			codeDesc = "Flight Cancellation";
		} else if (getCode() == 4) {
			codeDesc = "Day Light Saving Time Change";
		} else if (getCode() == 7) {
			codeDesc = "Flight Delayed";
		} else if (getCode() == 8) {
			codeDesc = "Delayed";
		} else if (getCode() == 9) {
			codeDesc = "Cancelled";
		} else if (getCode() == 10) {
			codeDesc = "Re-Protected";
		} else if (getCode() == 11) {
			codeDesc = "Cancelled";
		}
		return codeDesc;
	}

	public static final AlertTemplateEnum TEMPLATE_CHANGE_FLIGHT_NUMBER = new AlertTemplateEnum(0);
	public static final AlertTemplateEnum TEMPLATE_CHANGE_DATE_TIME = new AlertTemplateEnum(1);
	public static final AlertTemplateEnum TEMPLATE_CHANGE_ROUTE = new AlertTemplateEnum(2);
	public static final AlertTemplateEnum TEMPLATE_CANCELLATION = new AlertTemplateEnum(3);
	public static final AlertTemplateEnum DAY_LIGHT_SAVING_TIME_CHANGED = new AlertTemplateEnum(4);
	public static final AlertTemplateEnum TEMPLATE_AFFECTED_SEGMENT = new AlertTemplateEnum(5);
	public static final AlertTemplateEnum TEMPLATE_TRANSFER_PAX = new AlertTemplateEnum(6);
	public static final AlertTemplateEnum TEMPLATE_FLIGHT_DELAY = new AlertTemplateEnum(7);
	public static final AlertTemplateEnum TEMPLATE_FLIGHT_DELAY_MSG = new AlertTemplateEnum(8);
	public static final AlertTemplateEnum TEMPLATE_FLIGHT_CANCEL_MSG = new AlertTemplateEnum(9);
	public static final AlertTemplateEnum TEMPLATE_FLIGHT_REPROTECT_MSG = new AlertTemplateEnum(10);
	public static final AlertTemplateEnum TEMPLATE_FLIGHT_CANCEL_EMAIL = new AlertTemplateEnum(11);

	public interface TemplateParams {

		// old flight params
		public static final String OLD_FLIGHT_NO = "oldFlightNo";
		public static final String OLD_DEPARTURE_DATE = "oldDepartureDate";
		public static final String OLD_ROUTE = "oldRoute";
		public static final String OLD_LEG_DETAILS = "oldLegDetails";
		public static final String OLD_SEGMENT_DETAILS = "oldSegmentDetails";

		// new flight params
		public static final String NEW_FLIGHT_NO = "newFlightNo";
		public static final String NEW_DEPARTURE_DATE = "newDepartureDate";
		public static final String NEW_ROUTE = "newRoute";
		public static final String NEW_LEG_DETAILS = "newLegDetails";
		public static final String NEW_SEGMENT_DETAILS = "newSegmentDetails";

		// old segment params
		public static final String OLD_SEGMENT_CODE = "oldSegmentCode";
		public static final String OLD_SEGMENT_DEPARTURE = "oldSegmentDeparture";
		public static final String OLD_SEGMENT_ARRIVAL = "oldSegmentArrival";

		// new segment params
		public static final String NEW_SEGMENT_CODE = "newSegmentCode";
		public static final String NEW_SEGMENT_DEPARTURE = "newSegmentDeparture";
		public static final String NEW_SEGMENT_ARRIVAL = "newSegmentArrival";

		// common params
		public static final String PNR = "pnr";
		public static final String PHONE = "phone";
		public static final String SEGMENT = "segment";
		public static final String AFFECTED_SEGMENT = "affectedSegment";
		public static final String CANCELLED_SEGMENT = "cancelledSegment";

		// mesages
		public static final String RE_PROTECT = "REPROTECT";

		public static final String CANCELED_MEALS = "canceledMeals";
		public static final String CANCELED_SEATS = "canceledSeats";

		// immediate connecting segment params
		public static final String IMMEDIATE_CONNECTING_FLIGHT_DETAIL = "immediateConnectingFlightDetail";

	}
}
