package com.isa.thinair.alerting.api.model;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @hibernate.class table="T_ALERT_TYPE"
 * 
 */
public class AlertType extends Persistent implements Serializable {

	private static final long serialVersionUID = -5271796209950832811L;

	/** identifier field */
	private Long alertTypeId;

	/** nullable persistent field */
	private String description;

	/** nullable persistent field */
	private Long privilegeId;

	/** full constructor */
	public AlertType(Long alertTypeId, Long privilegeId, String description) {
		this.alertTypeId = alertTypeId;
		this.privilegeId = privilegeId;
		this.description = description;
	}

	/** default constructor */
	public AlertType() {
	}

	/** minimal constructor */
	public AlertType(Long alertTypeId) {
		this.alertTypeId = alertTypeId;
	}

	/**
	 * @hibernate.id generator-class="assigned" type="java.lang.Long" column="ALERT_TYPE_ID"
	 * 
	 */
	public Long getAlertTypeId() {
		return this.alertTypeId;
	}

	public void setAlertTypeId(Long alertTypeId) {
		this.alertTypeId = alertTypeId;
	}

	/**
	 * @hibernate.property column="DESCRIPTION" length="255"
	 * 
	 */
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @hibernate.property column="PRIVILEGE_ID"
	 * 
	 */
	public Long getPrivilegeId() {
		return privilegeId;
	}

	public void setPrivilegeId(Long privilegeId) {
		this.privilegeId = privilegeId;
	}

	public String toString() {
		return new ToStringBuilder(this).append("alertTypeId", getAlertTypeId()).toString();
	}

	public boolean equals(Object other) {
		if (!(other instanceof AlertType))
			return false;
		AlertType castOther = (AlertType) other;
		return new EqualsBuilder().append(this.getAlertTypeId(), castOther.getAlertTypeId()).isEquals();
	}

	public int hashCode() {
		return new HashCodeBuilder().append(getAlertTypeId()).toHashCode();
	}

}
