package com.isa.thinair.aaservices.core.bl.flightservice;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.DisplayFlightInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightInfoDetail;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightLegInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightManifestPage;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentSearchCriteria;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ModuleCriterianInfo;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAFlightManifestSearchRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAFlightManifestSearchRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.airschedules.api.criteria.FlightFlightSegmentSearchCriteria;
import com.isa.thinair.airschedules.api.dto.DisplayFlightDTO;
import com.isa.thinair.airschedules.api.dto.FlightManifestSearchDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightLeg;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.util.CommonUtil;

public class AAFlightManifestSearchBL {
	private static Log log = LogFactory.getLog(AAFlightManifestSearchBL.class);

	private AAFlightManifestSearchRQ aaFlightManifestSearchRQ;

	private AAFlightManifestSearchRS aaFlightManifestSearchRS;

	public void setAAFlightManifestSearchRQ(AAFlightManifestSearchRQ aaFlightManifestSearchRQ) {
		this.aaFlightManifestSearchRQ = aaFlightManifestSearchRQ;
	}

	private void initializeAAFlightManifestSearchRS() {
		aaFlightManifestSearchRS = new AAFlightManifestSearchRS();
		aaFlightManifestSearchRS.setResponseAttributes(new AAResponseAttributes());
	}

	public AAFlightManifestSearchRS execute() {

		initializeAAFlightManifestSearchRS();

		try {

			FlightManifestSearchDTO flightManifestSearchDTO = composeFlightManifestSearchDTO();
			Page page = AAServicesModuleUtils.getFlightBD().searchFlightsForDisplay(flightManifestSearchDTO.getCriteria(),
					flightManifestSearchDTO.getStartIndex(), flightManifestSearchDTO.getPageSize(),
					flightManifestSearchDTO.isDatesInLocal(), flightManifestSearchDTO.getFlightCriteria(),
					flightManifestSearchDTO.isFlightManifest(), flightManifestSearchDTO.isDataFromReporting(), flightManifestSearchDTO.getAgentApplicableOnds());
			// aaFlightManifestSearchRS.setPage(composePageDetail(page));
			aaFlightManifestSearchRS.setPage(composeFlightManifestPage(page));

		} catch (Exception ex) {

			log.error("Error occurred during search Flights for Manifest in given search critirea", ex);
			AAExceptionUtil.addAAErrrors(aaFlightManifestSearchRS.getResponseAttributes().getErrors(), ex);

		} finally {

			int numOfErrors = aaFlightManifestSearchRS.getResponseAttributes().getErrors().size();
			AASuccess successType = (numOfErrors == 0 ? new AASuccess() : null);
			aaFlightManifestSearchRS.getResponseAttributes().setSuccess(successType);

		}

		return aaFlightManifestSearchRS;
	}

	private FlightManifestSearchDTO composeFlightManifestSearchDTO() {
		FlightManifestSearchDTO flightManifestSearchDTO = new FlightManifestSearchDTO();

		flightManifestSearchDTO.setCarrierCode(aaFlightManifestSearchRQ.getSearchCriteria().getCarrierCode());
		flightManifestSearchDTO.setCriteria(composeCriteria());
		flightManifestSearchDTO.setDatesInLocal(aaFlightManifestSearchRQ.getSearchCriteria().isIsDatesInLocal());
		flightManifestSearchDTO.setFlightCriteria(composeFlightCriteria());
		flightManifestSearchDTO.setFlightManifest(aaFlightManifestSearchRQ.getSearchCriteria().isIsFlightManifest());
		flightManifestSearchDTO.setInterline(false);
		flightManifestSearchDTO.setPageSize(aaFlightManifestSearchRQ.getSearchCriteria().getPageSize());
		flightManifestSearchDTO.setStartIndex(aaFlightManifestSearchRQ.getSearchCriteria().getStartIndex());
		flightManifestSearchDTO.setDataFromReporting(aaFlightManifestSearchRQ.getSearchCriteria().isDataFromReporting());

		return flightManifestSearchDTO;
	}

	private List composeCriteria() {
		List criterian = new ArrayList();

		List<ModuleCriterianInfo> lccCriterianList = aaFlightManifestSearchRQ.getSearchCriteria().getValue();

		for (ModuleCriterianInfo info : lccCriterianList) {
			ModuleCriterion moduleCriterion = new ModuleCriterion();

			moduleCriterion.setCondition(info.getCondition());
			moduleCriterion.setFieldName(info.getFieldName());
			moduleCriterion.setLeaf(info.isLeaf());
			moduleCriterion.setValue(correctDateValues(info.getValue()));

			criterian.add(moduleCriterion);
		}

		return criterian;
	}

	private List correctDateValues(List valueList) {
		List ret = new ArrayList();
		for (Object object : valueList) {
			if (object instanceof XMLGregorianCalendar) {
				String strDate = object.toString();
				try {
					Calendar cal = CommonUtil.parseDate(strDate);
					ret.add(cal.getTime());
				} catch (ModuleException e) {
					ret.add(object);
				}
			} else {
				ret.add(object);
			}
		}
		return ret;
	}

	private FlightFlightSegmentSearchCriteria composeFlightCriteria() {

		FlightSegmentSearchCriteria lccCriteria = aaFlightManifestSearchRQ.getSearchCriteria().getFlightCriteria();
		if (lccCriteria != null) {
			FlightFlightSegmentSearchCriteria segmentSearchCriteria = new FlightFlightSegmentSearchCriteria();

			segmentSearchCriteria.setCheckinStatus(lccCriteria.getCheckinStatus());
			segmentSearchCriteria.setEstEndTimeZulu(lccCriteria.getEstEndTimeZulu());
			segmentSearchCriteria.setEstStartTimeZulu(lccCriteria.getEstStartTimeZulu());
			segmentSearchCriteria.setFlightNumber(lccCriteria.getFlightNumber());
			segmentSearchCriteria.setSegmentCode(lccCriteria.getSegmentCode());
			segmentSearchCriteria.setStatus(lccCriteria.getStatus());

			return segmentSearchCriteria;
		} else {
			return null;
		}
	}

	private FlightManifestPage composeFlightManifestPage(Page page) {
		FlightManifestPage flightManifestPage = new FlightManifestPage();

		flightManifestPage.setEndPosition(page.getEndPosition());
		flightManifestPage.setStartPosition(page.getStartPosition());
		flightManifestPage.setTotalNoOfRecords(page.getTotalNoOfRecords());
		flightManifestPage.setTotalNoOfRecordsInSystem(page.getTotalNoOfRecordsInSystem());
		flightManifestPage.getPageData().addAll(composeDisplayFlightInfo(page));

		return flightManifestPage;
	}

	private List<DisplayFlightInfo> composeDisplayFlightInfo(Page page) {
		List<DisplayFlightInfo> flightInfoList = new ArrayList<DisplayFlightInfo>();

		List<DisplayFlightDTO> flightDTOs = (ArrayList<DisplayFlightDTO>) page.getPageData();
		for (DisplayFlightDTO displayFlightDTO : flightDTOs) {
			DisplayFlightInfo info = new DisplayFlightInfo();

			info.setAllocated(displayFlightDTO.getAllocated());
			info.setCheckedIn(displayFlightDTO.getCheckedIn());
			info.setFlightInfo(composeFlightInfoDetail(displayFlightDTO.getFlight()));
			info.setOnHold(displayFlightDTO.getOnHold());
			info.setReservatinsExists(displayFlightDTO.getReservatinsExists());
			info.setSeatsSold(displayFlightDTO.getSeatsSold());
			info.setStndBy(displayFlightDTO.getStndBy());

			flightInfoList.add(info);
		}

		return flightInfoList;
	}

	private FlightInfoDetail composeFlightInfoDetail(Flight flight) {
		FlightInfoDetail flightInfoDetail = new FlightInfoDetail();

		flightInfoDetail.setAvailableSeatKilometers(flight.getAvailableSeatKilometers());
		flightInfoDetail.setDayNumber(flight.getDayNumber());
		flightInfoDetail.setDepartureDate(flight.getDepartureDate());
		flightInfoDetail.setDepartureDateLocal(flight.getDepartureDateLocal());
		flightInfoDetail.setDestinationAptCode(flight.getDestinationAptCode());
		flightInfoDetail.setFlightId(flight.getFlightId());
		flightInfoDetail.setFlightNumber(flight.getFlightNumber());
		flightInfoDetail.setManuallyChanged(flight.getManuallyChanged());
		flightInfoDetail.setMealNotificationAttempts(flight.getMealNotificationAttempts());
		flightInfoDetail.setMealNotificationStatus(flight.getMealNotificationStatus());
		flightInfoDetail.setModelNumber(flight.getModelNumber());
		flightInfoDetail.setOperationTypeId(flight.getOperationTypeId());
		flightInfoDetail.setOriginAptCode(flight.getOriginAptCode());
		flightInfoDetail.setOverlapingFlightId(flight.getOverlapingFlightId());
		flightInfoDetail.setScheduleId(flight.getScheduleId());
		flightInfoDetail.setStatus(flight.getStatus());
		flightInfoDetail.setTailNumber(flight.getTailNumber());
		flightInfoDetail.getFlightLegs().addAll(composeFlightLegInfo(flight.getFlightLegs()));
		flightInfoDetail.getFlightSegements().addAll(composeFlightSegementInfo(flight.getFlightSegements()));
		flightInfoDetail.getGdsIds().addAll(flight.getGdsIds());

		return flightInfoDetail;
	}

	private List<FlightLegInfo> composeFlightLegInfo(Set flightLegs) {
		List<FlightLegInfo> legInfoList = new ArrayList<FlightLegInfo>();

		if (flightLegs != null && flightLegs.size() > 0) {
			Set<FlightLeg> flightLegsSet = (Set<FlightLeg>) flightLegs;
			for (FlightLeg leg : flightLegsSet) {
				FlightLegInfo flightLegInfo = new FlightLegInfo();

				flightLegInfo.setDestination(leg.getDestination());
				flightLegInfo.setDuration(leg.getDuration());
				flightLegInfo.setDurationError(leg.getDurationError());
				flightLegInfo.setEstArrivalDayOffset(leg.getEstArrivalDayOffset());
				flightLegInfo.setEstArrivalDayOffsetLocal(leg.getEstArrivalDayOffsetLocal());
				flightLegInfo.setEstArrivalTimeLocal(leg.getEstArrivalTimeLocal());
				flightLegInfo.setEstArrivalTimeZulu(leg.getEstArrivalTimeZulu());
				flightLegInfo.setEstDepartureDayOffset(leg.getEstDepartureDayOffset());
				flightLegInfo.setEstDepartureDayOffsetLocal(leg.getEstDepartureDayOffsetLocal());
				flightLegInfo.setEstDepartureTimeLocal(leg.getEstDepartureTimeLocal());
				flightLegInfo.setEstDepartureTimeZulu(leg.getEstDepartureTimeZulu());
				flightLegInfo.setFlightId(leg.getFlightId());
				flightLegInfo.setId(leg.getId());
				flightLegInfo.setLegNumber(leg.getLegNumber());
				flightLegInfo.setModelRouteId(leg.getModelRouteId());
				flightLegInfo.setOrigin(leg.getOrigin());

				legInfoList.add(flightLegInfo);
			}
		}
		return legInfoList;
	}

	private List<FlightSegmentInfo> composeFlightSegementInfo(Set segements) {
		List<FlightSegmentInfo> segmentInfoList = new ArrayList<FlightSegmentInfo>();
		if (segements != null && segements.size() > 0) {
			Set<FlightSegement> flightSegments = (Set<FlightSegement>) segements;
			for (FlightSegement segement : flightSegments) {
				FlightSegmentInfo flightSegmentInfo = new FlightSegmentInfo();

				flightSegmentInfo.setArrivalTerminalId(segement.getArrivalTerminalId());
				flightSegmentInfo.setCheckinStatus(segement.getCheckinStatus());
				flightSegmentInfo.setDepartureTerminalId(segement.getDepartureTerminalId());
				flightSegmentInfo.setEstArrivalTimeLocal(segement.getEstTimeArrivalLocal());
				flightSegmentInfo.setEstArrivalTimeZulu(segement.getEstTimeArrivalZulu());
				flightSegmentInfo.setEstDepartureTimeLocal(segement.getEstTimeDepatureLocal());
				flightSegmentInfo.setEstDepatureTimeZulu(segement.getEstTimeDepatureZulu());
				flightSegmentInfo.setFlightId(segement.getFlightId());
				flightSegmentInfo.setFltSegId(segement.getFltSegId());
				flightSegmentInfo.setOverlapSegmentId(segement.getOverlapSegmentId());
				flightSegmentInfo.setSegmentCode(segement.getSegmentCode());
				flightSegmentInfo.setStatus(segement.getStatus());
				flightSegmentInfo.setValidFlag(segement.getValidFlag());

				segmentInfoList.add(flightSegmentInfo);
			}
		}
		return segmentInfoList;
	}
}
