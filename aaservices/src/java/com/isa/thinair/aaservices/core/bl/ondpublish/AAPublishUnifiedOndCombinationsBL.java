package com.isa.thinair.aaservices.core.bl.ondpublish;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.LanguageDescription;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OndPublishAirport;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OndPublishCurrency;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OndPublishDestination;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OndPublishOrigin;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TransitHubCodes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAOndPublishRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAOndPublishRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.commons.api.dto.ondpublish.LanguageDescriptionDTO;
import com.isa.thinair.commons.api.dto.ondpublish.OndLocationDTO;
import com.isa.thinair.commons.api.dto.ondpublish.OndPublishAirportDTO;
import com.isa.thinair.commons.api.dto.ondpublish.OndPublishCurrencyDTO;
import com.isa.thinair.commons.api.dto.ondpublish.OndPublishDestinationDTO;
import com.isa.thinair.commons.api.dto.ondpublish.OndPublishOriginDTO;
import com.isa.thinair.commons.api.dto.ondpublish.OndPublishedTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.bl.OndPublishToCachingBL;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;


public class AAPublishUnifiedOndCombinationsBL {

	private static final Log log = LogFactory.getLog(AARetrieveOwnOndCombinationsBL.class);

	private AAOndPublishRQ aaOndPublishRQ;

	public AAOndPublishRS execute() {

		// Publish data data as javascript
		log.debug("Publishing unified ond list javascript called");
		
		AAOndPublishRS aaOndPublishRS = new AAOndPublishRS();
		aaOndPublishRS.setResponseAttributes(new AAResponseAttributes());
		

		try {

			OndPublishedTO ondsPublished = transformToOndPublishedTO(aaOndPublishRQ);

			boolean cachingEnable = AppSysParamsUtil.isCachingEnableforOndPublished();
			if (cachingEnable) {
				OndPublishToCachingBL casheBL =  new OndPublishToCachingBL();
				casheBL.prepareListForCaching(ondsPublished);
			}

			AAServicesModuleUtils.getCommonMasterBD().publishOndListAsJavascript(ondsPublished);
			aaOndPublishRS.setResponseAttributes(new AAResponseAttributes());
			aaOndPublishRS.getResponseAttributes().setSuccess(new AASuccess());

		} catch (ModuleException ex) {
			log.error("Publishing Ond.js failed");
			AAExceptionUtil.addAAErrrors(aaOndPublishRS.getResponseAttributes().getErrors(), ex);
			aaOndPublishRS.getResponseAttributes().setSuccess(null);
		}

		log.debug("Publishing unified ond list javascript completed");
		return aaOndPublishRS;
	}

	public void setAaOndPublishRQ(AAOndPublishRQ aaOndPublishRQ) {
		this.aaOndPublishRQ = aaOndPublishRQ;
	}

	private OndPublishedTO transformToOndPublishedTO(AAOndPublishRQ lccOndPublishReq) {
	
		OndPublishedTO ondPublishedTO = new OndPublishedTO();

		// Transform Airports
		ondPublishedTO.setAirports(transformToAirports(lccOndPublishReq.getUnifiedAirportInfo()));
		// Transform Currencies
		ondPublishedTO.setCurrencies(transformToCurrencies(lccOndPublishReq.getUnifiedCurrencyInfo()));
		// Transform Origins/Destinations
		ondPublishedTO.setOrigins(transformToOrigins(lccOndPublishReq.getUnifiedOrigins()));

		return ondPublishedTO;
	}

	private List<LanguageDescriptionDTO> transformToLangDescriptions(List<LanguageDescription> langDescsLcc) {
		List<LanguageDescriptionDTO> langDescs = new ArrayList<LanguageDescriptionDTO>();

		LanguageDescriptionDTO langDesc;
		for (LanguageDescription langDescLcc : langDescsLcc) {
			langDesc = new LanguageDescriptionDTO();
			langDesc.setLanguageCode(langDescLcc.getLanguageCode());
			langDesc.setDescription(langDescLcc.getDescription());
			langDescs.add(langDesc);
		}
		return langDescs;
	}

	private List<OndPublishAirportDTO> transformToAirports(List<OndPublishAirport> airportsLcc) {

		List<OndPublishAirportDTO> airports = new ArrayList<OndPublishAirportDTO>();

		OndPublishAirportDTO airport;
		for (OndPublishAirport airportLcc : airportsLcc) {
			airport = new OndPublishAirportDTO();
			airport.setAirportCode(airportLcc.getAirportCode());
			airport.setCity(airportLcc.isCity());
			airport.setCityAirportCodes(airportLcc.getCityAirportCodes());
			airport.setLanguageDescriptions(transformToLangDescriptions(airportLcc.getLanguageDescriptions()));
			airports.add(airport);
		}
		return airports;

	}

	private List<OndPublishCurrencyDTO> transformToCurrencies(List<OndPublishCurrency> currenciesLcc) {

		List<OndPublishCurrencyDTO> currencies = new ArrayList<OndPublishCurrencyDTO>();

		OndPublishCurrencyDTO currency;
		for (OndPublishCurrency currencyLcc : currenciesLcc) {
			currency = new OndPublishCurrencyDTO();
			currency.setCurrencyCode(currencyLcc.getCurrencyCode());
			currency.setLanguageDescriptions(transformToLangDescriptions(currencyLcc.getLanguageDescriptions()));
			currencies.add(currency);

		}
		return currencies;

	}

	private List<OndPublishOriginDTO> transformToOrigins(List<OndPublishOrigin> originsLcc) {

		List<OndPublishOriginDTO> origins = new ArrayList<OndPublishOriginDTO>();

		OndPublishOriginDTO origin;
		for (OndPublishOrigin originLcc : originsLcc) {
			origin = new OndPublishOriginDTO();
			origin.setOndLocation(new OndLocationDTO(originLcc.getOndLocation().getLocationCode(),
					originLcc.getOndLocation().isCity()));
			origin.setDestinations(transformToDestinations(originLcc.getDestinations()));
			origins.add(origin);
		}
		return origins;
	}

	private List<OndPublishDestinationDTO> transformToDestinations(List<OndPublishDestination> destinationsLcc) {

		List<OndPublishDestinationDTO> destinations = new ArrayList<OndPublishDestinationDTO>();

		OndPublishDestinationDTO destination;
		for (OndPublishDestination destinationLcc : destinationsLcc) {
			OndLocationDTO ondLocation = new OndLocationDTO(destinationLcc.getOndLocation().getLocationCode(),
					destinationLcc.getOndLocation().isCity());
			destination = new OndPublishDestinationDTO();
			destination.setCurrencyCode(destinationLcc.getCurrency());
			destination.setMktCarrierCode(destinationLcc.getMktCarrierCode());
			destination.setRouteType(destinationLcc.getRouteType());
			destination.setOperatingCarriers(new ArrayList<String>(new HashSet<String>(destinationLcc.getOperatingCarriers())));
			destination.setOndLocation(ondLocation);
			Collection<List<String>> tmpHubCodes = new ArrayList<List<String>>();
			List<TransitHubCodes> tmpTransitHubs = destinationLcc.getTransitHubs();
			for (TransitHubCodes hubCodes : tmpTransitHubs) {
				List<String> tmpHubs = hubCodes.getHubCodes();
				tmpHubCodes.add(hubCodes.getHubCodes());
			}
			destination.setTransitHubs(tmpHubCodes);

			destinations.add(destination);
		}
		return destinations;

	}

}
