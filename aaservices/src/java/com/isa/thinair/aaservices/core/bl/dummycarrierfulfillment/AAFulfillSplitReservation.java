package com.isa.thinair.aaservices.core.bl.dummycarrierfulfillment;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AirTraveler;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TravelerInfo;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAErrorCode;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAPOS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAPayCarrierFulfullmentRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAReservation;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Class to handle the splitting of dummy reservation
 * 
 * @author malaka
 * 
 */
class AAFulfillSplitReservation extends BaseMarketingCarrierFulfillment {

	private static Log log = LogFactory.getLog(AAFulfillSplitReservation.class);

	private boolean isSplitForRemovePax;

	AAFulfillSplitReservation(AAReservation aaAirReservation, AAReservation newAAAirReservation, AAPOS aaPos) {
		super(aaAirReservation, newAAAirReservation, aaPos);
	}

	@Override
	public AAPayCarrierFulfullmentRS execute() throws WebservicesException, ModuleException {

		AAPayCarrierFulfullmentRS carrierFulfillment = new AAPayCarrierFulfullmentRS();
		carrierFulfillment.setResponseAttributes(new AAResponseAttributes());

		checkConstraints();

		try {

			if (log.isDebugEnabled()) {
				log.debug("AAFulfillSplitReservation::begin for pnr [" + aaAirReservation.getGroupPnr() + "]");
			}

			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setGroupPNR(aaAirReservation.getGroupPnr());
			Reservation oldReservation = getCarrierReservation(pnrModesDTO);

			if (oldReservation == null) {
				throw new WebservicesException(AAErrorCode.ERR_25_MAXICO_REQUIRED_THE_RESERVATION_NO_LONGER_EXIST,
						"Reservation does not exits");
			}

			// pax to be split
			TravelerInfo travelerInfo = aaAirReservation.getAirReservation().getTravelerInfo();
			List<AirTraveler> listAirTraveller = travelerInfo.getAirTraveler();
			List<Integer> pnrPaxIds = getPaxIds(oldReservation, listAirTraveller);

			ServiceResponce serviceResponse = AAServicesModuleUtils.getReservationBD().splitDummyReservation(oldReservation,
					newAAAirReservation.getGroupPnr(), pnrPaxIds, isSplitForRemovePax);
			Long version = (Long) serviceResponse.getResponseParam(CommandParamNames.VERSION);

			if (serviceResponse.isSuccess()) {

				if (log.isDebugEnabled()) {
					log.debug("AAFulfillSplitReservation::end");
				}
				carrierFulfillment.getResponseAttributes().setSuccess(new AASuccess());
				carrierFulfillment.setResVersion(version.toString()); 
			}

		} catch (ModuleException e) {
			log.error("dummy reservation AAFulfillSplitReservation failed", e);
			throw new ModuleException("aaservices.paycarrier.fulfillment.failed", e);
		}

		return carrierFulfillment;
	}

	private List<Integer> getPaxIds(Reservation reservation, List<AirTraveler> listAirTraveller) {
		List<Integer> pnrPaxIds = new ArrayList<Integer>();
		for (ReservationPax pax : (Collection<ReservationPax>) reservation.getPassengers()) {
			for (AirTraveler airTraveler : listAirTraveller) {
				if (airTraveler.getTravelerSequence().equals(pax.getPaxSequence())) {
					pnrPaxIds.add(pax.getPnrPaxId());
					break;
				}
			}
		}
		return pnrPaxIds;
	}

	@Override
	public void checkConstraints() throws WebservicesException {
		if (aaAirReservation == null || aaAirReservation.getAirReservation().getTravelerInfo() == null
				|| newAAAirReservation == null) {
			throw new WebservicesException(AAErrorCode.ERR_1_MAXICO_REQUIRED_INVALID_VALUE, "traveler info not found");
		}
	}

	/**
	 * @param isSplitForRemovePax
	 *            the isSplitForRemovePax to set
	 */
	public void setSplitForRemovePax(boolean isSplitForRemovePax) {
		this.isSplitForRemovePax = isSplitForRemovePax;
	}

}
