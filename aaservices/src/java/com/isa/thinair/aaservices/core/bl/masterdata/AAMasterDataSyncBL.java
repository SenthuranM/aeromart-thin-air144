package com.isa.thinair.aaservices.core.bl.masterdata;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.LCCStation;
import com.isa.connectivity.profiles.maxico.v1.common.dto.LCCTerritory;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAMasterDataSyncRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAMasterDataSyncRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.airmaster.api.model.Station;
import com.isa.thinair.airmaster.api.model.Territory;
import com.isa.thinair.airmaster.api.service.LocationBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.Util;

public class AAMasterDataSyncBL {

	private static Log log = LogFactory.getLog(AAMasterDataSyncBL.class);

	public AAMasterDataSyncRQ aaMasterDataSyncRQ;

	public AAMasterDataSyncBL(AAMasterDataSyncRQ aaMasterDataSyncRQ) {
		this.aaMasterDataSyncRQ = aaMasterDataSyncRQ;

	}

	public AAMasterDataSyncRS execute() {
		AAMasterDataSyncRS aaMasterDataSyncRS = new AAMasterDataSyncRS();
		aaMasterDataSyncRS.setResponseAttributes(new AAResponseAttributes());

		List<String> savedTerritoryCodes = new ArrayList<String>();
		List<String> savedStationCodes = new ArrayList<String>();

		try {

			if (aaMasterDataSyncRQ.getTerritoryList().size() > 0) {
				syncTerritories(aaMasterDataSyncRQ.getTerritoryList(), savedTerritoryCodes);
				aaMasterDataSyncRS.getUpdatedTerritoryCodes().addAll(savedTerritoryCodes);
			}

			if (aaMasterDataSyncRQ.getStationList().size() > 0) {
				syncStations(aaMasterDataSyncRQ.getStationList(), savedStationCodes);
				aaMasterDataSyncRS.getUpdatedStationCodes().addAll(savedStationCodes);
			}

			aaMasterDataSyncRS.getResponseAttributes().setSuccess(new AASuccess());

		} catch (ModuleException ex) {
			log.error("AAMasterDataSyncBL syncMasterData failed.", ex);
			AAExceptionUtil.addAAErrrors(aaMasterDataSyncRS.getResponseAttributes().getErrors(), ex);
		} catch (Exception e) {
			log.error("AAMasterDataSyncBL syncMasterData failed.", e);
			AAExceptionUtil.addAAErrrors(aaMasterDataSyncRS.getResponseAttributes().getErrors(), e);
		}

		return aaMasterDataSyncRS;
	}

	private void syncTerritories(List<LCCTerritory> territoryList, List<String> savedTerritoryCodes) throws ModuleException {
		LocationBD locationBD = AAServicesModuleUtils.getLocationServiceBD();
		for (LCCTerritory lccTerritory : territoryList) {
			Territory airTerritory = new Territory();
			airTerritory.setTerritoryCode(lccTerritory.getTerritoryCode());
			airTerritory.setDescription(lccTerritory.getDescription());
			airTerritory.setRemarks(lccTerritory.getRemarks());
			airTerritory.setStatus(lccTerritory.getStatus());
			airTerritory.setCreatedBy(lccTerritory.getCreatedBy());
			airTerritory.setCreatedDate(lccTerritory.getCreatedDate());
			airTerritory.setModifiedBy(lccTerritory.getModifiedBy());
			airTerritory.setModifiedDate(lccTerritory.getModifiedDate());
			airTerritory.setAirlineCode(lccTerritory.getAirlineCode());
			airTerritory.setSwiftCode(lccTerritory.getSwiftCode());
			airTerritory.setBankAddress(lccTerritory.getBankAddress());
			airTerritory.setAccountNumber(lccTerritory.getAccountNumber());
			airTerritory.setBank(lccTerritory.getBank());
			airTerritory.setIBANCode(lccTerritory.getIbanNo());
			airTerritory.setBeneficiaryName(lccTerritory.getBeneficiaryName());
			airTerritory.setVersion(lccTerritory.getVersion());
			airTerritory.setLccPublishStatus(Util.LCC_PUBLISHED);

			Territory oldTerritory = locationBD.getTerritory(airTerritory.getTerritoryCode());
			if (oldTerritory != null && oldTerritory.getVersion() > airTerritory.getVersion()) {
				// TO DO error
			} else {
				if (oldTerritory != null) {
					airTerritory.setVersion(oldTerritory.getVersion());
				} else {
					airTerritory.setVersion(-1);
				}

				locationBD.saveTerritory(airTerritory);
				locationBD.updateTerritoryVersion(airTerritory.getTerritoryCode(), lccTerritory.getVersion() + 1);
				savedTerritoryCodes.add(airTerritory.getTerritoryCode());
			}
		}
	}

	private void syncStations(List<LCCStation> stationList, List<String> savedStationCodes) throws ModuleException {
		LocationBD locationBD = AAServicesModuleUtils.getLocationServiceBD();
		for (LCCStation lccStation : stationList) {
			Station airStation = new Station();
			airStation.setStationCode(lccStation.getStationCode());
			airStation.setStationName(lccStation.getStationName());
			airStation.setOnlineStatus(lccStation.getOnlineStatus());
			airStation.setStatus(lccStation.getStatus());
			airStation.setRemarks(lccStation.getRemarks());
			airStation.setTerritoryCode(lccStation.getTerritoryCode());
			airStation.setCreatedBy(lccStation.getCreatedBy());
			airStation.setCreatedDate(lccStation.getCreatedDate());
			airStation.setModifiedBy(lccStation.getModifiedBy());
			airStation.setModifiedDate(lccStation.getModifiedDate());
			airStation.setCountryCode(lccStation.getCountryCode());
			airStation.setContact(lccStation.getContact());
			airStation.setTelephone(lccStation.getTelephone());
			airStation.setFax(lccStation.getFax());
			airStation.setNameChangeThresholdTime(lccStation.getThresholdTime());
			airStation.setVersion(lccStation.getVersion());
			airStation.setLccPublishStatus(Util.LCC_PUBLISHED);

			Station oldStation = locationBD.getStation(airStation.getStationCode());
			if (oldStation != null && oldStation.getVersion() > airStation.getVersion()) {
				// TO DO error
			} else {
				if (oldStation != null) {
					airStation.setVersion(oldStation.getVersion());
				} else {
					airStation.setVersion(-1);
				}

				locationBD.saveStation(airStation);
				locationBD.updateStationVersion(airStation.getStationCode(), lccStation.getVersion() + 1);

				savedStationCodes.add(lccStation.getStationCode());
			}
		}

	}

}