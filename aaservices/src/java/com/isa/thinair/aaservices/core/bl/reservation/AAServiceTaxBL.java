package com.isa.thinair.aaservices.core.bl.reservation;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.ExternalChargeType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegment;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAServiceTaxRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAServiceTaxRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.core.bl.availability.AvailabilityUtil;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.AAUtils;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxContainer;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.exception.ModuleException;

public class AAServiceTaxBL {

	private static Log log = LogFactory.getLog(AAServiceTaxBL.class);

	private AAServiceTaxRQ aaServiceTaxRQ;
	private AAServiceTaxRS aaServiceTaxRS;

	public AAServiceTaxBL(AAServiceTaxRQ aaServiceTaxRQ) {
		this.aaServiceTaxRQ = aaServiceTaxRQ;
		this.aaServiceTaxRS = new AAServiceTaxRS();
		this.aaServiceTaxRS.setResponseAttributes(new AAResponseAttributes());
	}

	public AAServiceTaxRS execute() {
		try {
			ReservationSegmentDTO reservationSegmentDTO = populateReservationSegmentDTO(aaServiceTaxRQ.getFlightSegment());
			Set<ServiceTaxContainer> serviceTaxContainers = new HashSet<ServiceTaxContainer>();
			for (ExternalChargeType externalChargeType : aaServiceTaxRQ.getTaxChargeCodes()) {
				EXTERNAL_CHARGES externalCharge = AAUtils.getExternalChargeType(externalChargeType);
				ServiceTaxContainer serviceTaxContainer = AAServicesModuleUtils.getReservationBD().getApplServiceTaxContainer(
						reservationSegmentDTO, externalCharge);

				if (serviceTaxContainer != null) {
					serviceTaxContainers.add(serviceTaxContainer);
				}
			}

			aaServiceTaxRS.getServiceTaxes().addAll(AvailabilityUtil.populateLCCServiceTaxContainers(serviceTaxContainers));

		} catch (ModuleException e) {
			log.error("ERROR", e);
			AAExceptionUtil.addAAErrrors(aaServiceTaxRS.getResponseAttributes().getErrors(), e);
		} finally {
			int numOfErrors = aaServiceTaxRS.getResponseAttributes().getErrors().size();
			AASuccess successType = (numOfErrors == 0 ? new AASuccess() : null);
			aaServiceTaxRS.getResponseAttributes().setSuccess(successType);
		}

		return aaServiceTaxRS;
	}

	private ReservationSegmentDTO populateReservationSegmentDTO(FlightSegment flightSegment) {
		ReservationSegmentDTO reservationSegmentDTO = null;

		if (flightSegment != null) {
			reservationSegmentDTO = new ReservationSegmentDTO();
			reservationSegmentDTO.setArrivalDate(flightSegment.getArrivalDateTime());
			reservationSegmentDTO.setZuluArrivalDate(flightSegment.getArrivalDateTimeZulu());
			reservationSegmentDTO.setDepartureDate(flightSegment.getDepatureDateTime());
			reservationSegmentDTO.setZuluDepartureDate(flightSegment.getDepatureDateTimeZulu());
			reservationSegmentDTO.setBookingType(flightSegment.getBookingType());
			reservationSegmentDTO.setCabinClassCode(flightSegment.getCabinClassCode());
			reservationSegmentDTO.setFlightId(flightSegment.getFlightId());
			reservationSegmentDTO.setFlightNo(flightSegment.getFlightNumber());
			reservationSegmentDTO
					.setFlightSegId(FlightRefNumberUtil.getSegmentIdFromFlightRPH(flightSegment.getFlightRefNumber()));
			reservationSegmentDTO.setSegmentCode(flightSegment.getSegmentCode());
		}

		return reservationSegmentDTO;
	}
}
