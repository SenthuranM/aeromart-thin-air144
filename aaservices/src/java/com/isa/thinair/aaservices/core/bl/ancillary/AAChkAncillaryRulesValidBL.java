package com.isa.thinair.aaservices.core.bl.ancillary;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AirTraveler;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAChkAncillaryRulesValidRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAChkAncillaryRulesValidRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.core.bl.reservation.AAModifyReservationUtil;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.PaxTypeUtils;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.seatmap.PaxSeatTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;

/**
 * @author Nilindra Fernando
 * 
 * @since Sep 21, 2010
 */
public class AAChkAncillaryRulesValidBL {

	private AAChkAncillaryRulesValidRQ aAChkAncillaryRulesValidRQ;

	public void setRequest(AAChkAncillaryRulesValidRQ aAChkAncillaryRulesValidRQ) {
		this.aAChkAncillaryRulesValidRQ = aAChkAncillaryRulesValidRQ;
	}

	public AAChkAncillaryRulesValidRS execute() {
		AAChkAncillaryRulesValidRS aaChkAncillaryRulesValidRS = new AAChkAncillaryRulesValidRS();
		aaChkAncillaryRulesValidRS.setResponseAttributes(new AAResponseAttributes());

		try {
			String groupPNR = BeanUtils.nullHandler(aAChkAncillaryRulesValidRQ.getGroupPnr());

			LCCClientPnrModesDTO lccClientPnrModesDTO = new LCCClientPnrModesDTO();
			lccClientPnrModesDTO.setGroupPNR(groupPNR);
			lccClientPnrModesDTO.setLoadFares(true);
			lccClientPnrModesDTO.setLoadSeatingInfo(true);

			long version = -1;
			if (aAChkAncillaryRulesValidRQ.getVersion() != null) {
				version = Long.parseLong(aAChkAncillaryRulesValidRQ.getVersion());
			}

			Reservation reservation = AAModifyReservationUtil.getReservation(lccClientPnrModesDTO, version, null);
			Map<Integer, Integer> parentPnrPaxIdAndInfantPnrPaxId = getParentPnrPaxIds(reservation);
			Collection<Integer> newParentPnrPaxIds = getNewParentPnrPaxIds(parentPnrPaxIdAndInfantPnrPaxId, reservation);
			Collection<Integer> oldParentPnrPaxIds = getOldParentPnrPaxIds(reservation, newParentPnrPaxIds);

			if (newParentPnrPaxIds.size() > 0) {
				Map<Integer, Collection<PaxSeatTO>> pnrPaxIdWiseSeats = getPnrPaxWiseSeats(reservation, newParentPnrPaxIds);
				Map<Integer, Collection<PaxSeatTO>> oldPnrPaxIdWiseSeats = getPnrPaxWiseSeats(reservation, oldParentPnrPaxIds);

				Map<Integer, String> flightSeatIdAndSeatPaxType = composeFlightSeatIdAndSeatPaxType(pnrPaxIdWiseSeats);
				Collection<Integer> oldParentFlightSeatIDs = new ArrayList<Integer>();
				for (Collection<PaxSeatTO> colSeat : oldPnrPaxIdWiseSeats.values()) {
					oldParentFlightSeatIDs.addAll(getFlightSeatIds(colSeat));
				}

				if (flightSeatIdAndSeatPaxType.size() > 0) {
					AAServicesModuleUtils.getSeatMapBD().checkSeatingRules(flightSeatIdAndSeatPaxType, oldParentFlightSeatIDs);
				}
			}
		} catch (Exception e) {
			AAExceptionUtil.addAAErrrors(aaChkAncillaryRulesValidRS.getResponseAttributes().getErrors(), e);
		} finally {
			int numOfErrors = aaChkAncillaryRulesValidRS.getResponseAttributes().getErrors().size();
			AASuccess successType = (numOfErrors == 0 ? new AASuccess() : null);
			aaChkAncillaryRulesValidRS.getResponseAttributes().setSuccess(successType);
		}

		return aaChkAncillaryRulesValidRS;
	}

	private Collection<Integer> getNewParentPnrPaxIds(Map<Integer, Integer> parentPnrPaxIdAndInfantPnrPaxId,
			Reservation reservation) {
		Map<Integer, Integer> srvParentInfantMap = new HashMap<Integer, Integer>();

		for (ReservationPax reservationPax : (Collection<ReservationPax>) reservation.getPassengers()) {
			if (ReservationApiUtils.isParentAfterSave(reservationPax)) {
				srvParentInfantMap.put(reservationPax.getPnrPaxId(), reservationPax.getAccompaniedPaxId());
			}
		}

		Collection<Integer> newParentPnrPaxIds = new ArrayList<Integer>();

		for (Entry<Integer, Integer> entry : parentPnrPaxIdAndInfantPnrPaxId.entrySet()) {
			Integer newParent = entry.getKey();
			Integer newInfant = entry.getValue();

			Integer srvInfant = srvParentInfantMap.get(newParent);

			if (srvInfant != null) {
				if (!srvInfant.equals(newInfant)) {
					newParentPnrPaxIds.add(newParent);
				}
			} else {
				newParentPnrPaxIds.add(newParent);
			}
		}

		return newParentPnrPaxIds;
	}

	private Collection<Integer> getOldParentPnrPaxIds(Reservation reservation, Collection<Integer> newParentPnrPaxIds) {
		Collection<Integer> oldParentPnrPaxIds = new ArrayList<Integer>();

		for (ReservationPax reservationPax : (Collection<ReservationPax>) reservation.getPassengers()) {
			if (ReservationApiUtils.isParentAfterSave(reservationPax)
					&& !newParentPnrPaxIds.contains(reservationPax.getPnrPaxId())) {
				oldParentPnrPaxIds.add(reservationPax.getPnrPaxId());
			}
		}
		return oldParentPnrPaxIds;
	}
	/**
	 * Get Parent Pnr Pax Ids
	 * 
	 * @param reservation
	 * @return
	 */
	private Map<Integer, Integer> getParentPnrPaxIds(Reservation reservation) {
		Map<Integer, Integer> parentPnrPaxIdAndInfantPnrPaxId = new HashMap<Integer, Integer>();
		Map<AirTraveler, ReservationPax> mapPassengers = new HashMap<AirTraveler, ReservationPax>();
		Map<Integer, ReservationPax> mapAdultInfant = new HashMap<Integer, ReservationPax>();

		for (AirTraveler airTraveler : aAChkAncillaryRulesValidRQ.getAirTraveler()) {
			for (ReservationPax reservationPax : (Set<ReservationPax>) reservation.getPassengers()) {
				if (PaxTypeUtils.getPaxSeq(airTraveler.getTravelerRefNumber()).compareTo(reservationPax.getPaxSequence()) == 0) {
					mapPassengers.put(airTraveler, reservationPax);
					if (ReservationApiUtils.isInfantType(reservationPax)) {
						int adultSeq = PaxTypeUtils.getParentSeq(airTraveler.getTravelerRefNumber());
						mapAdultInfant.put(adultSeq, reservationPax);
					}
					break;
				}
			}
		}

		for (Entry<AirTraveler, ReservationPax> entry : mapPassengers.entrySet()) {
			AirTraveler airTraveler = entry.getKey();
			ReservationPax reservationPax = entry.getValue();

			if (!ReservationApiUtils.isInfantType(reservationPax)) {
				Integer paxSeq = PaxTypeUtils.getPaxSeq(airTraveler.getTravelerRefNumber());
				ReservationPax attachedInfantPax = mapAdultInfant.get(paxSeq);

				if (attachedInfantPax != null) {
					parentPnrPaxIdAndInfantPnrPaxId.put(reservationPax.getPnrPaxId(), attachedInfantPax.getPnrPaxId());
				}
			}
		}

		return parentPnrPaxIdAndInfantPnrPaxId;
	}

	private Map<Integer, String> composeFlightSeatIdAndSeatPaxType(Map<Integer, Collection<PaxSeatTO>> pnrPaxIdWiseSeats) {

		Map<Integer, String> flightSeatIdAndSeatPaxType = new HashMap<Integer, String>();

		for (Entry<Integer, Collection<PaxSeatTO>> entry : pnrPaxIdWiseSeats.entrySet()) {
			Collection<PaxSeatTO> colPaxSeatTO = entry.getValue();
			Collection<Integer> flightSeatIds = getFlightSeatIds(colPaxSeatTO);

			if (flightSeatIds.size() > 0) {
				for (Integer integer : flightSeatIds) {
					flightSeatIdAndSeatPaxType.put(integer, ReservationInternalConstants.PassengerType.PARENT);
				}
			}
		}

		return flightSeatIdAndSeatPaxType;
	}

	private Collection<Integer> getFlightSeatIds(Collection<PaxSeatTO> colPaxSeatTO) {
		Collection<Integer> flightSeatIds = new HashSet<Integer>();

		if (colPaxSeatTO != null && colPaxSeatTO.size() > 0) {
			for (PaxSeatTO paxSeatTO : colPaxSeatTO) {
				flightSeatIds.add(paxSeatTO.getSelectedFlightSeatId());
			}
		}

		return flightSeatIds;
	}

	private Map<Integer, Collection<PaxSeatTO>> getPnrPaxWiseSeats(Reservation reservation, Collection<Integer> pnrPaxIds) {
		Map<Integer, Collection<PaxSeatTO>> map = new HashMap<Integer, Collection<PaxSeatTO>>();

		if (reservation.getSeats() != null && reservation.getSeats().size() > 0) {
			for (PaxSeatTO paxSeatTO : (Collection<PaxSeatTO>) reservation.getSeats()) {
				if (pnrPaxIds.contains(paxSeatTO.getPnrPaxId())) {
					Collection<PaxSeatTO> colPaxSeatTO = map.get(paxSeatTO.getPnrPaxId());

					if (colPaxSeatTO == null) {
						colPaxSeatTO = new ArrayList<PaxSeatTO>();
						colPaxSeatTO.add(paxSeatTO);

						map.put(paxSeatTO.getPnrPaxId(), colPaxSeatTO);
					} else {
						colPaxSeatTO.add(paxSeatTO);
					}
				}
			}
		}

		return map;
	}

}
