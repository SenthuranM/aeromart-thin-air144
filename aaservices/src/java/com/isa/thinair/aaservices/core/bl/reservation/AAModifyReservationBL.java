package com.isa.thinair.aaservices.core.bl.reservation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.PaxOndFlexibility;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PerPaxPriceInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PriceInfo;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAirBookModifyRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAirBookRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAErrorCode;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAModificationTypeCode;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAReadRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.aaservices.core.session.AASessionManager;
import com.isa.thinair.aaservices.core.session.AAUserSession;
import com.isa.thinair.aaservices.core.transaction.TransactionalReservationProcess;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAReservationUtil;
import com.isa.thinair.aaservices.core.util.AAUtils;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.utils.PrivilegesKeys.AlterResPrivilegesKeys;
import com.isa.thinair.airproxy.api.utils.PrivilegesKeys.MakeResPrivilegesKeys;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.webplatform.api.util.PrivilegesKeys;
import com.isa.thinair.webplatform.api.util.ReservationUtil;

/**
 * @author Nilindra Fernando
 */
public class AAModifyReservationBL {

	private static Log log = LogFactory.getLog(AAModifyReservationBL.class);

	private AAAirBookModifyRQ aaAirBookModifyRQ;

	private TransactionalReservationProcess tnxResProcess;

	public AAModifyReservationBL(AAAirBookModifyRQ aaAirBookModifyRQ, AASessionManager aaSessionManager) {
		this.aaAirBookModifyRQ = aaAirBookModifyRQ;
		String transactionId = PlatformUtiltiies
				.nullHandler((aaAirBookModifyRQ != null && aaAirBookModifyRQ.getHeaderInfo() != null) ? aaAirBookModifyRQ
						.getHeaderInfo().getTransactionIdentifier() : null);

		if (transactionId.length() > 0 && aaSessionManager != null) {
			this.tnxResProcess = (TransactionalReservationProcess) aaSessionManager.getCurrentUserTransaction(transactionId);
		}
	}

	public AAAirBookRS execute() {
		AAAirBookRS aaAirBookRS = new AAAirBookRS();
		aaAirBookRS.setResponseAttributes(new AAResponseAttributes());

		Reservation reservation = null;
		Collection<String> privilegeKeys = null;
		String pnr = null;
		String versionString;
		AAModificationTypeCode aaModificationTypeCode = aaAirBookModifyRQ.getModificationTypeCode();
		AAModificationTypeCode actualModTypeCode = aaAirBookModifyRQ.getActualModificationTypeCode();

		String originCountryOfCall = aaAirBookModifyRQ.getAaReservation().getAirReservation().getOriginCountryOfCall();
		Collection<String> subOperations;
		try {
			if (aaModificationTypeCode == null) {
				throw new WebservicesException(AAErrorCode.ERR_11_MAXICO_REQUIRED_INVALID_MODIFICATION_TYPE,
						"Modification type is invalid");
			}

			privilegeKeys = (Collection<String>) AASessionManager.getInstance().getCurrUserSessionParam(
					AAUserSession.USER_PRIVILEGES_KEYS);
			pnr = aaAirBookModifyRQ.getAaReservation().getPnr();
			versionString = aaAirBookModifyRQ.getAaReservation().getAirReservation().getVersion();

			long version = -1l;
			if (versionString != null) {
				version = Long.parseLong(versionString);
			}

			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setPnr(pnr);
			pnrModesDTO.setLoadFares(true);
			pnrModesDTO.setLoadOndChargesView(true);
			pnrModesDTO.setLoadPaxAvaBalance(true);
			pnrModesDTO.setLoadSegViewBookingTypes(true);
			pnrModesDTO.setLoadSegView(true);
			pnrModesDTO.setLoadLastUserNote(true);
			pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
			pnrModesDTO.setRecordAudit(true);
			pnrModesDTO.setLoadPaxPaymentOndBreakdownView(true);
			pnrModesDTO.setLoadEtickets(true);

			// Cancel Reservation
			if (aaModificationTypeCode == AAModificationTypeCode.MODTYPE_1_CANCEL_RESERVATION) {

				AAReservationUtil.authorize(privilegeKeys, AlterResPrivilegesKeys.CANCEL_RES);
				reservation = AAModifyReservationUtil.getReservation(pnrModesDTO, version, null);

				AAModifyReservationUtil.authorizeModification(privilegeKeys, reservation,
						AlterResPrivilegesKeys.ALT_BUFFERTIME_SEG_OR_PAX, null, actualModTypeCode);
				AAModifyReservationUtil.cancelReservation(reservation, aaAirBookModifyRQ, false);

				// load the reservation for response
				aaAirBookRS = loadReservation(pnrModesDTO);

				// Balance Payment
			} else if (aaModificationTypeCode == AAModificationTypeCode.MODTYPE_2_BALANCE_PAYMENT) {

				// Current webservices implementation accepts only on account payment and Payment by passenger credit
				// AAReservationUtil.authorizePayment(privilegeKeys, null,
				// aaAirBookModifyRQ.getAaPos().getMarketingAgentCode(), null);
				// Note : privileges are checked inside balance payment
				reservation = AAModifyReservationUtil.getReservation(pnrModesDTO, version, null);

				AAModifyReservationUtil.balancePayment(reservation, aaAirBookModifyRQ, privilegeKeys);

				// load the reservation for response
				aaAirBookRS = loadReservation(pnrModesDTO);

				// Modify contact details
			} else if (aaModificationTypeCode == AAModificationTypeCode.MODTYPE_3_MODIFY_RESERVATION_CONTACT_INFO) {

				AAReservationUtil.authorize(privilegeKeys, AlterResPrivilegesKeys.CHANGE_CONTACT_DETAILS);
				reservation = AAModifyReservationUtil.getReservation(pnrModesDTO, version, null);

				AAModifyContactInfoBL aaModifyContactInfoBL = new AAModifyContactInfoBL();
				aaModifyContactInfoBL.setRequest(aaAirBookModifyRQ);
				reservation.setOriginCountryOfCall(originCountryOfCall);
				aaModifyContactInfoBL.setReservation(reservation);

				aaModifyContactInfoBL.execute();

				// load the reservation for response
				aaAirBookRS = loadReservation(pnrModesDTO);

				// Add reservation audit data to the AA response
				Collection<ReservationAudit> resAuditCol = aaModifyContactInfoBL.getReservationAuditsCol();
				addReservationAuditForResponse(resAuditCol, aaAirBookRS,
						AuditTemplateEnum.TemplateParams.ChangedContactDetails.CONTACT_DETAILS);

				// Modify Passenger Details
			} else if (aaModificationTypeCode == AAModificationTypeCode.MODTYPE_4_MODIFY_RESERVATION_PAX_INFO) {
				// TODO:Handle these 3 actions separately and remove authorizeAny. This is ok at the moment, because we
				// check these privileges in front-end.

				AAReservationUtil.authorizeAny(privilegeKeys, AlterResPrivilegesKeys.PRIVI_NAME_CHANGE,
						AlterResPrivilegesKeys.RES_UPDATE);

				reservation = AAModifyReservationUtil.getReservation(pnrModesDTO, version, null);

//				subOperations = LccOperationsUtil.getModifyReservationOperations(reservation, aaAirBookModifyRQ.getAaReservation());

//				if (!LccOperationsUtil.isAuthorized(subOperations, privilegeKeys)) {
//					throw new WebservicesException(AAErrorCode.ERR_9_MAXICO_REQUIRED_AUTHORIZATION_ERROR, "Unauthorized Operation");
//				}

				// FIXME due the fact the reservation updates like adding usernotes and etc should not be
				// controlled by the PrivilegesKeys.AlterResPrivilegesKeys.ALT_BUFFERTIME_NAME, this check is removed.
				// Once these operations are separated, can be reinforced. At the moment this check is enforced at the
				// frontend
				// and only possible flaw would be if user action happens in buffer time boundary.
				// AAModifyReservationUtil.authorizeModification(privilegeKeys, reservation,
				// PrivilegesKeys.AlterResPrivilegesKeys.ALT_BUFFERTIME_NAME, null,
				// AAModificationTypeCode.MODTYPE_4_MODIFY_RESERVATION_PAX_INFO);

				AAModifyPaxInfoBL aaModifyPaxInfoBL = new AAModifyPaxInfoBL();
				aaModifyPaxInfoBL.setRequest(aaAirBookModifyRQ);
				reservation.setOriginCountryOfCall(originCountryOfCall);
				aaModifyPaxInfoBL.setReservation(reservation);

				aaModifyPaxInfoBL.execute();

				// load the reservation for response
				aaAirBookRS = loadReservation(pnrModesDTO);

				// Add reservation audit data to the AA response
				Collection<ReservationAudit> resAuditCol = aaModifyPaxInfoBL.getReservationAuditsCol();
				addReservationAuditForResponse(resAuditCol, aaAirBookRS,
						AuditTemplateEnum.TemplateParams.ChangedPassengerDetails.PASSENGER_DETAILS);

				// Add Segment
			} else if (aaModificationTypeCode == AAModificationTypeCode.MODTYPE_5_ADD_OND) {

				if (actualModTypeCode == AAModificationTypeCode.MODTYPE_18_MODIFY_OND) {
					AAReservationUtil.authorizeAny(privilegeKeys, AlterResPrivilegesKeys.ALT_RES_MODIFY_SEGMENT_DATE,
							AlterResPrivilegesKeys.ALT_RES_MODIFY_SEGMENT_ROUTE);
				} else {
					AAReservationUtil.authorize(privilegeKeys, AlterResPrivilegesKeys.ALT_RES_ADD_SEGMENT);
				}

				this.checkConstraintsForAddAndModifySegment();

				reservation = AAModifyReservationUtil.getReservation(pnrModesDTO, version, null);

				AAAddSegmentBL.addOND(reservation, tnxResProcess, aaAirBookModifyRQ, privilegeKeys);

				// load the reservation for response
				aaAirBookRS = loadReservation(pnrModesDTO);

				// Cancel Segment
			} else if (aaModificationTypeCode == AAModificationTypeCode.MODTYPE_7_CANCEL_OND) {

				if (actualModTypeCode == AAModificationTypeCode.MODTYPE_18_MODIFY_OND) {
					AAReservationUtil.authorizeAny(privilegeKeys, AlterResPrivilegesKeys.ALT_RES_MODIFY_SEGMENT_DATE,
							AlterResPrivilegesKeys.ALT_RES_MODIFY_SEGMENT_ROUTE);
				} else {
					AAReservationUtil.authorize(privilegeKeys, AlterResPrivilegesKeys.ALT_RES_CANCEL_SEGMENT);
				}

				reservation = AAModifyReservationUtil.getReservation(pnrModesDTO, version, null);

				AACancelSegmentBL aaCancelSegmentBL = new AACancelSegmentBL();
				aaCancelSegmentBL.cancelOND(reservation, aaAirBookModifyRQ, privilegeKeys);

				// load the reservation for response
				aaAirBookRS = loadReservation(pnrModesDTO);

				// To Retain Flexibilities
				List<PaxOndFlexibility> updatedFlexibilitiesList = aaCancelSegmentBL.getUpdatedFlexibilitiesList();
				if (updatedFlexibilitiesList != null) {
					aaAirBookRS.getAaAirReservation().getAirReservation().getUpdatedPaxOndFlexibility()
							.addAll(updatedFlexibilitiesList);
				}

				// Add Or Cancel External Segment
			} else if (aaModificationTypeCode == AAModificationTypeCode.MODTYPE_6_MODIFY_EXTERNAL_OND) {

				AAExternalSegmentBL.processOND(reservation, aaAirBookModifyRQ, privilegeKeys);

				// load the reservation for response
				aaAirBookRS = loadReservation(pnrModesDTO);

				// Split Reservation
			} else if (aaModificationTypeCode == AAModificationTypeCode.MODTYPE_8_SPLIT_PASSENGER) {

				AAReservationUtil.authorize(privilegeKeys, AlterResPrivilegesKeys.RES_SPLIT);
				reservation = AAModifyReservationUtil.getReservation(pnrModesDTO, version, null);

				AAModifyReservationUtil.authorizeModificationForSplit(privilegeKeys, reservation,
						AlterResPrivilegesKeys.ALT_BUFFERTIME_SEG_OR_PAX, null);

				AASplitReservationBL aaSplitReservationBL = new AASplitReservationBL();
				aaSplitReservationBL.setAaAirBookModifyRQ(aaAirBookModifyRQ);
				reservation.setOriginCountryOfCall(originCountryOfCall);
				aaSplitReservationBL.setReservation(reservation);
				String newPNR = aaSplitReservationBL.execute();
				pnrModesDTO.setPnr(newPNR);
				// load the reservation for response
				aaAirBookRS = loadReservation(pnrModesDTO);

				// Kill credit
			} else if (aaModificationTypeCode == AAModificationTypeCode.MODTYPE_9_KILL_CREDIT) {

				reservation = AAModifyReservationUtil.getReservation(pnrModesDTO, version, null);
				Map<String, BigDecimal> passengerTransfers = AACreditAdjustBL.killCredits(reservation);

				// load the reservation for response
				aaAirBookRS = loadReservation(pnrModesDTO);

				// append transfer amounts
				PriceInfo priceInfo = aaAirBookRS.getAaAirReservation().getAirReservation().getPriceInfo();
				List<PerPaxPriceInfo> perPaxPriceInfoList = priceInfo.getPerPaxPriceInfo();
				for (PerPaxPriceInfo perPaxPriceInfo : perPaxPriceInfoList) {

					if (passengerTransfers.containsKey(perPaxPriceInfo.getTravelerRefNumber())) {
						perPaxPriceInfo.getPassengerPrice().setTotalTransfer(
								passengerTransfers.get(perPaxPriceInfo.getTravelerRefNumber()));
					}
				}

				// Acquire credit
			} else if (aaModificationTypeCode == AAModificationTypeCode.MODTYPE_10_ACQUIRE_CREDIT) {

				reservation = AAModifyReservationUtil.getReservation(pnrModesDTO, version, null);
				AACreditAdjustBL.acquireCredits(reservation, aaAirBookModifyRQ, privilegeKeys);

				// load the reservation for response
				aaAirBookRS = loadReservation(pnrModesDTO);

				// Adjust charge
			} else if (aaModificationTypeCode == AAModificationTypeCode.MODTYPE_11_ADJUST_CHARGE) {

				reservation = AAModifyReservationUtil.getReservation(pnrModesDTO, version, null);

				AAChargeAdjustmentBL aaChargeAdjustmentBL = new AAChargeAdjustmentBL();
				aaChargeAdjustmentBL.setAaAirBookModifyRQ(aaAirBookModifyRQ);
				aaChargeAdjustmentBL.setReservation(reservation);

				aaChargeAdjustmentBL.executeGroupChargeAdjustment();

				// load the reservation for response
				aaAirBookRS = loadReservation(pnrModesDTO);

				// Extend onHold
			} else if (aaModificationTypeCode == AAModificationTypeCode.MODTYPE_12_EXTEND_ON_HOLD) {

				reservation = AAModifyReservationUtil.getReservation(pnrModesDTO, version, null);
				if (reservation.getDummyBooking() != ReservationInternalConstants.DummyBooking.YES) {
					AAReservationUtil.authorize(privilegeKeys, AlterResPrivilegesKeys.RES_EXTEND);
				}
				AAModifyReservationUtil.authorizeModification(privilegeKeys, reservation, null, null, actualModTypeCode);

				AAModifyReservationUtil.extendOnHoldReleaseTime(reservation, aaAirBookModifyRQ, privilegeKeys);

				if (reservation.getDummyBooking() == ReservationInternalConstants.DummyBooking.YES) {
					pnrModesDTO.setLoadFares(false);
					pnrModesDTO.setLoadOndChargesView(false);
					pnrModesDTO.setLoadPaxAvaBalance(false);
					pnrModesDTO.setLoadSegViewBookingTypes(false);
					pnrModesDTO.setLoadSegView(false);
					pnrModesDTO.setLoadLastUserNote(false);
					pnrModesDTO.setLoadSegViewFareCategoryTypes(false);
					pnrModesDTO.setRecordAudit(false);
					pnrModesDTO.setLoadPaxPaymentOndBreakdownView(false);
				}
				// load the reservation for response
				aaAirBookRS = loadReservationByPnrModesDTO(pnrModesDTO);

				// Transfer reservation ownership
			} else if (aaModificationTypeCode == AAModificationTypeCode.MODTYPE_14_TRANSFER_OWNERSHIP) {

				AAReservationUtil.authorize(privilegeKeys, MakeResPrivilegesKeys.RES_TRANSFER);
				reservation = AAModifyReservationUtil.getReservation(pnrModesDTO, version, null);

				AAModifyReservationUtil.transferOwnership(reservation, aaAirBookModifyRQ);

				// load the reservation for response
				aaAirBookRS = loadReservation(pnrModesDTO);

				// Remove Passenger
			} else if (aaModificationTypeCode == AAModificationTypeCode.MODTYPE_17_REMOVE_PAX) {
				reservation = AAModifyReservationUtil.getReservation(pnrModesDTO, version, null);

				if (ReservationUtil.isBulkTicketReservation(reservation)) {
					AAReservationUtil.authorize(privilegeKeys, AlterResPrivilegesKeys.RES_BULK_TKT_REMOVE_PAX);
				} else {
					AAReservationUtil.authorize(privilegeKeys, AlterResPrivilegesKeys.RES_REMOVE_PAX);
				}

				AARemovePaxBL.execute(reservation, aaAirBookModifyRQ);
				// FIXME return the newly created reservation as the response
				// load the reservation for response
				aaAirBookRS = loadReservation(pnrModesDTO);
			} else if (aaModificationTypeCode == AAModificationTypeCode.MODTYPE_19_RESERVATION_STATUS_CHANGE) {

				reservation = AAModifyReservationUtil.getReservation(pnrModesDTO, version, null);
				AAReservationStatusChangeBL.execute(reservation, aaAirBookModifyRQ);

				aaAirBookRS = loadReservation(pnrModesDTO);
			} else if (aaModificationTypeCode == AAModificationTypeCode.MODTYPE_20_SYSTEM_INITIATED_CANCEL_RESERVATION) {
				reservation = AAModifyReservationUtil.getReservation(pnrModesDTO, version, null);
				// As this is a system initiated cancel reservation, there's not need to check any privileges.
				AAModifyReservationUtil.cancelReservation(reservation, aaAirBookModifyRQ, true);
				aaAirBookRS = loadReservation(pnrModesDTO);
			} else if (aaModificationTypeCode == AAModificationTypeCode.MODTYPE_21_REVERSE_CHARGE) {

				reservation = AAModifyReservationUtil.getReservation(pnrModesDTO, version, null);

				AAChargeReverseBL aaChargeReversalBL = new AAChargeReverseBL();
				aaChargeReversalBL.setAaAirBookModifyRQ(aaAirBookModifyRQ);
				aaChargeReversalBL.setReservation(reservation);

				aaChargeReversalBL.executeChargeReversal();

				// load the reservation for response
				aaAirBookRS = loadReservation(pnrModesDTO);	
			} else {
				throw new WebservicesException(AAErrorCode.ERR_11_MAXICO_REQUIRED_INVALID_MODIFICATION_TYPE,
						"Modification type [" + aaModificationTypeCode.value() + "] not supported");
			}

		} catch (Exception ex) {
			if (aaModificationTypeCode != null) {
				log.error("ModifyReservation failed for modification type : " + aaModificationTypeCode.value(), ex);
			} else {
				log.error("ModifyReservation failed.", ex);
			}

			AAExceptionUtil.addAAErrrors(aaAirBookRS.getResponseAttributes().getErrors(), ex);
		}

		return aaAirBookRS;
	}

	private void checkConstraintsForAddAndModifySegment() throws WebservicesException, ModuleException {
		if (tnxResProcess == null) {
			throw new WebservicesException(AAErrorCode.ERR_8_MAXICO_REQUIRED_TRANSACTION_INVALID_OR_EXPIRED,
					"Transaction not found");
		}
		if (!tnxResProcess.hasFareType(aaAirBookModifyRQ.getSelectedFareTypes())) {
			throw new WebservicesException(AAErrorCode.ERR_1_MAXICO_REQUIRED_INVALID_VALUE, "Fares could not be located");
		}
	}

	private AAAirBookRS loadReservation(LCCClientPnrModesDTO pnrModesDTO) {
		AAReadRQ aaReadRQ = new AAReadRQ();
		aaReadRQ.setPnr(pnrModesDTO.getPnr());
		aaReadRQ.setHeaderInfo(AAUtils.populateHeaderInfo(null));

		if ((aaAirBookModifyRQ != null) && (aaAirBookModifyRQ.getLoadDataOptions() != null)) {
			aaAirBookModifyRQ.getLoadDataOptions().setLoadPTCPriceInfo(true);
		}
		// TODO :Currently setting the default pnr loading options. May need to optimized it by using
		// LCCReservationUtil.createLoadReservationOptions(pnrModesDTO)
		aaReadRQ.setLoadReservationOptions(AAReservationUtil.createLoadReservationOptions());
		AASearchReservationByPnrBL aaSearchReservationBL = new AASearchReservationByPnrBL();
		aaSearchReservationBL.setRequest(aaReadRQ);

		return aaSearchReservationBL.execute();
	}

	private AAAirBookRS loadReservationByPnrModesDTO(LCCClientPnrModesDTO pnrModesDTO) {
		AAReadRQ aaReadRQ = new AAReadRQ();
		aaReadRQ.setPnr(pnrModesDTO.getPnr());
		aaReadRQ.setHeaderInfo(AAUtils.populateHeaderInfo(null));

		if ((aaAirBookModifyRQ != null) && (aaAirBookModifyRQ.getLoadDataOptions() != null)) {
			aaAirBookModifyRQ.getLoadDataOptions().setLoadPTCPriceInfo(true);
		}
		// TODO :Currently setting the default pnr loading options. May need to optimized it by using
		// LCCReservationUtil.createLoadReservationOptions(pnrModesDTO)
		aaReadRQ.setLoadReservationOptions(AAReservationUtil.createLoadReservationOptions(pnrModesDTO));
		AASearchReservationByPnrBL aaSearchReservationBL = new AASearchReservationByPnrBL();
		aaSearchReservationBL.setRequest(aaReadRQ);

		return aaSearchReservationBL.execute();
	}

	/**
	 * Prepares and set reservation audit data to the AA response, to make these audit data available for LCC audits.
	 * 
	 * @param resAuditCol
	 * @param aaAirBookRS
	 */
	private void addReservationAuditForResponse(Collection<ReservationAudit> resAuditCol, AAAirBookRS aaAirBookRS,
			String contentKey) {

		List<String> auditCol = new ArrayList<String>();
		if (resAuditCol != null && resAuditCol.size() > 0) {
			for (ReservationAudit resAudit : resAuditCol) {
				StringBuilder auditText = new StringBuilder();
				auditText.append(resAudit.getContentMap().get(contentKey));
				auditCol.add(auditText.toString());
			}
		}
		aaAirBookRS.getAaAirReservation().getAirReservation().getReservationAuditList().addAll(auditCol);
	}
}
