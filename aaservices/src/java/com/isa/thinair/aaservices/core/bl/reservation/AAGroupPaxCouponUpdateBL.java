package com.isa.thinair.aaservices.core.bl.reservation;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.PaxCouponUpdateRequest;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAGroupPaxCouponUpdateRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAGroupPaxCouponUpdateRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.airproxy.api.model.reservation.dto.PassengerCouponUpdateRQ;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.auditor.api.dto.PassengerTicketCouponAuditDTO;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class AAGroupPaxCouponUpdateBL {

	private static final Log log = LogFactory.getLog(AAGroupPaxCouponUpdateBL.class);

	private AAGroupPaxCouponUpdateRQ aaGroupPaxCouponUpdateRQ;

	public AAGroupPaxCouponUpdateBL(AAGroupPaxCouponUpdateRQ aaGroupPaxCouponUpdateRQ) {
		this.aaGroupPaxCouponUpdateRQ = aaGroupPaxCouponUpdateRQ;
	}

	public AAGroupPaxCouponUpdateRS execute() {

		AAGroupPaxCouponUpdateRS aaGroupPaxCouponUpdateRS = new AAGroupPaxCouponUpdateRS();
		List<PassengerCouponUpdateRQ> groupPassengerCouponUpdateRQ = new ArrayList<PassengerCouponUpdateRQ>();

		try {

			aaGroupPaxCouponUpdateRS.setResponseAttributes(new AAResponseAttributes());

			if (log.isDebugEnabled()) {
				log.debug("Pax Coupon Update on : " + AppSysParamsUtil.getDefaultCarrierCode() + " STARTED");
			}

			List<PaxCouponUpdateRequest> aaGroupPaxCouponUpdateRequest = aaGroupPaxCouponUpdateRQ
					.getGroupPaxCouponUpdateRequest();
			for (PaxCouponUpdateRequest aaPaxCouponUpdateRequest : aaGroupPaxCouponUpdateRequest) {

				PassengerCouponUpdateRQ paxCouponUpdateRQ = new PassengerCouponUpdateRQ();
				paxCouponUpdateRQ.setCarrierCode(aaPaxCouponUpdateRequest.getCarrierCode());
				paxCouponUpdateRQ.setEticketId(aaPaxCouponUpdateRequest.getEticketId());
				paxCouponUpdateRQ.setEticketStatus(aaPaxCouponUpdateRequest.getEticketStatus());
				paxCouponUpdateRQ.setPaxStatus(aaPaxCouponUpdateRequest.getPaxStatus());
				if (aaPaxCouponUpdateRequest.getCouponAudit() != null) {
					PassengerTicketCouponAuditDTO couponAudit = new PassengerTicketCouponAuditDTO();
					couponAudit.setPassengerName(aaPaxCouponUpdateRequest.getCouponAudit().getPassengerName());
					couponAudit.setPnr(aaPaxCouponUpdateRequest.getCouponAudit().getPnr());
					couponAudit.setRemark(aaPaxCouponUpdateRequest.getCouponAudit().getRemark());
					paxCouponUpdateRQ.setCouponAudit(couponAudit);
				}

				groupPassengerCouponUpdateRQ.add(paxCouponUpdateRQ);
			}

			TrackInfoDTO trackInfo = null;

			AirproxyModuleUtils.getPassengerBD().updateGroupPassengerCoupon(groupPassengerCouponUpdateRQ, trackInfo, true);
			aaGroupPaxCouponUpdateRS.getResponseAttributes().setSuccess(new AASuccess());

			if (log.isDebugEnabled()) {
				log.debug("Pax Coupon Update on : " + AppSysParamsUtil.getDefaultCarrierCode() + " FINISHED");
			}

		} catch (Exception ex) {
			log.error("Clear segment alerts failed ", ex);
			AAExceptionUtil.addAAErrrors(aaGroupPaxCouponUpdateRS.getResponseAttributes().getErrors(), ex);
		}

		return aaGroupPaxCouponUpdateRS;
	}

}
