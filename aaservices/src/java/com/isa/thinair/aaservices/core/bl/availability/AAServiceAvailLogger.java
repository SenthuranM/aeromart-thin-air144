package com.isa.thinair.aaservices.core.bl.availability;

import java.text.SimpleDateFormat;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OriginDestinationInfoDTO;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;

public class AAServiceAvailLogger {
	final private static String AASERVICES_AVAIL_LOG = "aaservices.avail.log";
	private static Log log = LogFactory.getLog(AASERVICES_AVAIL_LOG);

	public static void logSearch(AvailableFlightSearchDTO availableSearchDTO, UserPrincipal userPrinciple) {
		try {
			String pnr = availableSearchDTO.getPnr();
			String agentCode = userPrinciple.getAgentCode();
			String userId = userPrinciple.getUserId();
			String channel = availableSearchDTO.getAppIndicator();
			String ip = userPrinciple.getIpAddress();

			int ondCount = 0;
			StringBuffer ondStr = new StringBuffer();
			if (availableSearchDTO.getOriginDestinationInfoDTOs().size() > 0) {
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				ondCount = availableSearchDTO.getOriginDestinationInfoDTOs().size();
				for (OriginDestinationInfoDTO ond : availableSearchDTO.getOriginDestinationInfoDTOs()) {
					if (ondStr.length() > 0) {
						ondStr.append("-");
					}
					ondStr.append(ond.getOrigin() + "/" + ond.getDestination() + "#"
							+ sdf.format(ond.getPreferredDateTimeStart()) + "#"
							+ CalendarUtil.daysUntil(ond.getDepartureDateTimeStart(), ond.getPreferredDateTimeStart()) + "#"
							+ CalendarUtil.daysUntil(ond.getPreferredDateTimeEnd(), ond.getDepartureDateTimeEnd()));
				}
			}

			logSearch(AppSysParamsUtil.getCarrierCode(), agentCode, userId, channel, ip, ondCount, ondStr.toString(),
					availableSearchDTO.getAdultCount(), availableSearchDTO.getChildCount(), availableSearchDTO.getInfantCount(),
					pnr);
		} catch (Exception e) {
			log.error(e);
		}
	}

	private static void logSearch(String carrier, String agentCode, String userId, String channel, String ip, int ondCount,
			String ondStr, Integer adults, Integer children, Integer infants, String pnr) {
		log.debug(createLogString(carrier, agentCode, userId, ondCount + "", ondStr, adults + "", children + "", infants + "",
				channel, ip, pnr));
	}

	private static String createLogString(String... arr) {
		StringBuffer sb = new StringBuffer();
		for (String x : arr) {
			if (sb.length() > 0) {
				sb.append(",");
			}
			sb.append(x);
		}
		return sb.toString();
	}

}
