package com.isa.thinair.aaservices.core.bl.schedule;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightScheduleDetails;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightScheduleInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightScheduleLegInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightScheduleSegmentInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FrequencyInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.RoutesRequestInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.RoutesResposeInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ScheduleOriginDestinationInfo;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAScheduleAvailRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAScheduleAvailRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.AAUtils;
import com.isa.thinair.airmaster.api.dto.CachedAirportDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.ScheduleAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.ScheduleAvailRS;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airschedules.api.dto.FlightScheduleInfoDTO;
import com.isa.thinair.airschedules.api.dto.FlightScheduleLegInfoDTO;
import com.isa.thinair.airschedules.api.dto.FlightScheduleSegmentInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class AAScheduleAvailabilitySearchBL {

	private static Log log = LogFactory.getLog(AAScheduleAvailabilitySearchBL.class);

	private AAScheduleAvailRQ aaScheduleAvailRQ;
	private AAScheduleAvailRS aaScheduleAvailRS;

	private ScheduleAvailRS scheduleAvailRS = null;

	private Date startDateTime = null;
	private Date stopDateTime = null;
	private boolean returnFlag = false;

	private void initializeAAScheduleAvailRS() {
		aaScheduleAvailRS = new AAScheduleAvailRS();
		aaScheduleAvailRS.setResponseAttributes(new AAResponseAttributes());
	}

	public void setAAScheduleAvailRQ(AAScheduleAvailRQ aaScheduleAvailRQ) {
		initializeAAScheduleAvailRS();
		this.aaScheduleAvailRQ = aaScheduleAvailRQ;
		aaScheduleAvailRS.setHeaderInfo(aaScheduleAvailRQ.getHeaderInfo());
	}

	public AAScheduleAvailRS execute() {

		try {

			logInfo("AAScheduleAvailabilitySearchBL called by LCConnect @ " + AppSysParamsUtil.getDefaultCarrierCode());

			// Populate HeaderInfo
			aaScheduleAvailRS.setHeaderInfo(AAUtils.populateHeaderInfo(null));

			// Prepare Request object
			populateAAScheduleInfo(aaScheduleAvailRQ);

		} catch (Exception ex) {
			log.error(ex);
			AAExceptionUtil.addAAErrrors(aaScheduleAvailRS.getResponseAttributes().getErrors(), ex);

		} finally {
			int numOfErrors = aaScheduleAvailRS.getResponseAttributes().getErrors().size();
			AASuccess successType = (numOfErrors == 0 ? new AASuccess() : null);
			aaScheduleAvailRS.getResponseAttributes().setSuccess(successType);
		}

		return aaScheduleAvailRS;
	}

	private void populateAAScheduleInfo(AAScheduleAvailRQ aaScheduleAvailRQ) throws ModuleException {

		List<RoutesRequestInfo> routesRequestInfo = aaScheduleAvailRQ.getRoutesRequestInfo();
		Collection<Collection<FlightScheduleInfoDTO>> colSchedules = null;
		Collection<Collection<FlightScheduleInfoDTO>> colSchedulesReturns = null;

		if (routesRequestInfo == null || routesRequestInfo.isEmpty())
			return;

		if (aaScheduleAvailRQ.getStartDateTime() == null && aaScheduleAvailRQ.getStopDateTime() == null)
			return;

		startDateTime = aaScheduleAvailRQ.getStartDateTime();
		stopDateTime = aaScheduleAvailRQ.getStopDateTime();
		returnFlag = aaScheduleAvailRQ.isReturnFlag();

		String arFrom = "";
		String arTo = "";
		String arSHJ = "";

		for (RoutesRequestInfo rouInfo : routesRequestInfo) {

			try {

				logInfo("called populateAAScheduleInfo " + rouInfo.getRouteId() + "|"
						+ rouInfo.getScheduleOriginDestinationInfo());

				List<ScheduleOriginDestinationInfo> scheduleList = rouInfo.getScheduleOriginDestinationInfo();

				Collection<FlightScheduleInfoDTO> colFlightScheduleInfoDTOs = new ArrayList<FlightScheduleInfoDTO>();

				if (scheduleList != null && !scheduleList.isEmpty()) {
					for (ScheduleOriginDestinationInfo scheduleOriginDestinationInfo : scheduleList) {

						String strFrom = scheduleOriginDestinationInfo.getOriginLocation();
						String strTo = scheduleOriginDestinationInfo.getDestinationLocation();

						ScheduleAvailRQ scheduleAvailRQ = new ScheduleAvailRQ();

						scheduleAvailRQ = createScheduleAvailRQ(strFrom, strTo, startDateTime, stopDateTime, 
								returnFlag, arFrom, arTo, arSHJ);
						// colFlightScheduleInfoDTOs =
						// AAServicesModuleUtils.getAirproxySearchAndQuoteBD().getAirlineFlightSchedules(scheduleAvailRQ);
						// scheduleAvailRS =
						// AAServicesModuleUtils.getAirproxySearchAndQuoteBD().getFlightSchedulesInfomation(scheduleAvailRQ);
						scheduleAvailRS = AAServicesModuleUtils.getAirproxySearchAndQuoteBD().getSchedulesList(scheduleAvailRQ,
								null);

						colSchedules = scheduleAvailRS.getFlightSchedules();
						colSchedulesReturns = scheduleAvailRS.getFlightSchedulesReturns();

						List<Collection> list = (List) colSchedules;
						/*
						 * getFlightSchedulesInfomation( startDateTime, stopDateTime, strFrom, strTo,
						 * aaScheduleAvailRQ.isReturnFlag());
						 */

						RoutesResposeInfo routesResposeInfo = new RoutesResposeInfo();
						routesResposeInfo.setRouteId(rouInfo.getRouteId());

						int length = list.size();
						for (int i = 0; i < length; i++) {

							// int seqNum = 1;
							Collection<Collection<FlightScheduleInfoDTO>> flightScheduleInfoDTOs = list.get(i);
							populateAllFlightDetails(flightScheduleInfoDTOs, routesResposeInfo);
						}

						aaScheduleAvailRS.getRoutesResposeInfo().add(routesResposeInfo);

						if (aaScheduleAvailRQ.isReturnFlag()) {
							if (colSchedulesReturns != null && colSchedulesReturns.size() > 0) {

								list = (List) colSchedulesReturns;

								RoutesResposeInfo routesReturnResposeInfo = new RoutesResposeInfo();
								routesReturnResposeInfo.setRouteId(rouInfo.getRouteId());

								length = list.size();
								for (int i = 0; i < length; i++) {
									// Collection<Collection<FlightScheduleInfoDTO>>flightScheduleInfoDTOs =
									// list.get(i);
									populateAllFlightDetails(list.get(i), routesReturnResposeInfo);
								}
								aaScheduleAvailRS.getRoutesReturnResposeInfo().add(routesReturnResposeInfo);
							}
						}

					}

				}

			} catch (ModuleException me) {
				log.error(me);
			}
		}

	}

	private static void populateAllFlightDetails(Collection<Collection<FlightScheduleInfoDTO>> infoDtos,
			RoutesResposeInfo routesResposeInfo) throws ModuleException {
		Map<String, CachedAirportDTO> mapAirportCodeAndCachedAirportDTO = AAServicesModuleUtils.getAirportBD()
				.getAllAirportOperatorMap();

		for (Collection<FlightScheduleInfoDTO> infoDto : infoDtos) {
			FlightScheduleDetails flightScheduleDetails = new FlightScheduleDetails();
			for (FlightScheduleInfoDTO resultInfoDTO : infoDto) {

				FlightScheduleInfo flightScheduleInfoDTO = new FlightScheduleInfo();

				flightScheduleInfoDTO.setScheduleId(resultInfoDTO.getScheduleId());
				flightScheduleInfoDTO.setFlightNumber(resultInfoDTO.getFlightNumber());
				flightScheduleInfoDTO.setStartDate(resultInfoDTO.getStartDate());
				flightScheduleInfoDTO.setStopDate(resultInfoDTO.getStopDate());
				flightScheduleInfoDTO.setStartDateLocal(resultInfoDTO.getStartDateLocal());
				flightScheduleInfoDTO.setStopDateLocal(resultInfoDTO.getStopDateLocal());
				// 6
				flightScheduleInfoDTO.setOverlapingScheduleId(resultInfoDTO.getOverlapingScheduleId());
				flightScheduleInfoDTO.setAvailableSeatKilometers(resultInfoDTO.getAvailableSeatKilometers());
				flightScheduleInfoDTO.setNumberOfDepartures(resultInfoDTO.getNumberOfDepartures());
				// 9

				flightScheduleInfoDTO.setDepartureStnCode(resultInfoDTO.getDepartureStnCode());
				flightScheduleInfoDTO.setDepartureAptName(getAirportName(mapAirportCodeAndCachedAirportDTO,
						resultInfoDTO.getDepartureStnCode()));
				flightScheduleInfoDTO.setArrivalAptName(getAirportName(mapAirportCodeAndCachedAirportDTO,
						resultInfoDTO.getArrivalStnCode()));

				flightScheduleInfoDTO.setArrivalStnCode(resultInfoDTO.getArrivalStnCode());

				flightScheduleInfoDTO.setModelNumber(resultInfoDTO.getModelNumber());
				flightScheduleInfoDTO.setOperationTypeId(resultInfoDTO.getOperationTypeId());
				flightScheduleInfoDTO.setBuildStatusCode(resultInfoDTO.getBuildStatusCode());
				flightScheduleInfoDTO.setStatusCode(resultInfoDTO.getStatusCode());
				// flightScheduleInfoDTO.setManuallyChanged(resultInfoDTO.getManuallyChanged());

				// flightScheduleInfoDTO.setLocalChange(resultInfoDTO.getLocalChange());
				// flightScheduleInfoDTO.setGdsIds(resultInfoDTO.getGdsIds());
				// flightScheduleInfoDTO.setNumberOfOpenFlights(resultInfoDTO.getNumberOfFlights());

				FrequencyInfo freq = new FrequencyInfo();

				freq.setDay0(resultInfoDTO.getFrequency().getDay0());
				freq.setDay1(resultInfoDTO.getFrequency().getDay1());
				freq.setDay2(resultInfoDTO.getFrequency().getDay2());
				freq.setDay3(resultInfoDTO.getFrequency().getDay3());
				freq.setDay4(resultInfoDTO.getFrequency().getDay4());
				freq.setDay5(resultInfoDTO.getFrequency().getDay5());
				freq.setDay6(resultInfoDTO.getFrequency().getDay6());

				FrequencyInfo freqLocal = new FrequencyInfo();

				freqLocal.setDay0(resultInfoDTO.getFrequencyLocal().getDay0());
				freqLocal.setDay1(resultInfoDTO.getFrequencyLocal().getDay1());
				freqLocal.setDay2(resultInfoDTO.getFrequencyLocal().getDay2());
				freqLocal.setDay3(resultInfoDTO.getFrequencyLocal().getDay3());
				freqLocal.setDay4(resultInfoDTO.getFrequencyLocal().getDay4());
				freqLocal.setDay5(resultInfoDTO.getFrequencyLocal().getDay5());
				freqLocal.setDay6(resultInfoDTO.getFrequencyLocal().getDay6());

				flightScheduleInfoDTO.setFrequency(freq);
				flightScheduleInfoDTO.setFrequencyLocal(freqLocal);

				Set<FlightScheduleLegInfoDTO> flightScheduleLegs = resultInfoDTO.getFlightScheduleLegs();

				// Set<FlightScheduleLegInfo> flightSet = new HashSet<FlightScheduleLegInfo>();

				for (FlightScheduleLegInfoDTO legs : flightScheduleLegs) {
					FlightScheduleLegInfo dto = new FlightScheduleLegInfo();
					dto.setId(legs.getId());
					dto.setScheduleId(legs.getScheduleId());
					dto.setLegNumber(legs.getLegNumber());
					dto.setEstDepartureTimeZulu(legs.getEstDepartureTimeZulu());
					dto.setEstArrivalTimeZulu(legs.getEstArrivalTimeZulu());

					dto.setEstDepartureTimeLocal(legs.getEstDepartureTimeLocal());
					dto.setEstArrivalTimeLocal(legs.getEstArrivalTimeLocal());
					dto.setDuration(legs.getDuration());

					dto.setEstDepartureDayOffset(legs.getEstDepartureDayOffset());
					dto.setEstArrivalDayOffset(legs.getEstArrivalDayOffset());

					dto.setEstDepartureDayOffsetLocal(legs.getEstDepartureDayOffsetLocal());
					dto.setEstArrivalTimeLocal(legs.getEstArrivalTimeLocal());

					dto.setOrigin(legs.getOrigin());
					dto.setDestination(legs.getDestination());
					dto.setModelRouteId(legs.getModelRouteId());

					flightScheduleInfoDTO.getFlightScheduleLegs().add(dto);
				}

				Collection<FlightScheduleSegmentInfoDTO> flightScheduleSegments = resultInfoDTO.getFlightScheduleSegments();
				// Set<FlightScheduleSegmentInfoDTO> segmentDTOs= new HashSet<FlightScheduleSegmentInfoDTO>();

				for (FlightScheduleSegmentInfoDTO segment : flightScheduleSegments) {
					FlightScheduleSegmentInfo dto = new FlightScheduleSegmentInfo();
					dto.setFlSchSegId(segment.getFlSchSegId());
					dto.setScheduleId(segment.getScheduleId());
					dto.setOverlapSegmentId(segment.getOverlapSegmentId());
					dto.setSegmentCode(segment.getSegmentCode());
					dto.setValidFlag(segment.isValidFlag());

					flightScheduleInfoDTO.getFlightScheduleSegments().add(dto);
				}

				flightScheduleDetails.getFlightScheduleInfo().add(flightScheduleInfoDTO);
			}
			routesResposeInfo.getFlightScheduleDetails().add(flightScheduleDetails);
		}
	}

	private static String
			getAirportName(Map<String, CachedAirportDTO> mapAirportCodeAndCachedAirportDTO, String airportCode) {

		CachedAirportDTO cachedAirportDTO = mapAirportCodeAndCachedAirportDTO.get(airportCode);
		String airportName = BeanUtils.nullHandler(cachedAirportDTO.getAirportName());

		if (airportName.length() > 0) {
			return airportName;
		} else {
			return airportCode;
		}
	}

	private static ScheduleAvailRQ createScheduleAvailRQ(String strFrom, String strTo, Date dStartDate, Date dStopDate,
			boolean roundTrip, String arFrom, String arTo, String arSHJ) {
		ScheduleAvailRQ scheduleAvailRQ = new ScheduleAvailRQ();

		scheduleAvailRQ.setOriginLocation(strFrom);
		scheduleAvailRQ.setDestLocation(strTo);
		scheduleAvailRQ.setStartDateTime(dStartDate);
		scheduleAvailRQ.setStopDateTime(dStopDate);
		scheduleAvailRQ.setRoundTrip(roundTrip);
		scheduleAvailRQ.setArFrom(arFrom);
		scheduleAvailRQ.setArTo(arTo);
		scheduleAvailRQ.setArSHJ(arSHJ);
		scheduleAvailRQ.setDefaultCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
		scheduleAvailRQ.setSystemHub(AAServicesModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.HUB_FOR_THE_SYSTEM));
		scheduleAvailRQ.setAAServiceRequest(true);

		return scheduleAvailRQ;
	}

	private static void logInfo(String logInfo) {
		if (log.isDebugEnabled())
			log.debug(logInfo);
	}

}
