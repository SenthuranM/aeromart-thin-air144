package com.isa.thinair.aaservices.core.bl.reservation;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ReservationSummary;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SearchReservatonOptions;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASearchReservationRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASearchReservationRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.AAUtils;
import com.isa.thinair.airproxy.api.dto.FlightInfoTO;
import com.isa.thinair.airproxy.api.dto.ReservationListTO;
import com.isa.thinair.airreservation.api.dto.ReservationSearchDTO;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class AASearchReservationBL {

	// Logger
	private static Log log = LogFactory.getLog(AASearchReservationBL.class);

	// Request & Response objects
	private AASearchReservationRQ aaSearchReservationRQ;
	private AASearchReservationRS aaSearchReservationRS = new AASearchReservationRS();

	private String thisAirlineCode;

	/**
	 * Set the Request object (to be used in the {@link AASearchReservationBL#execute execute} method)
	 * 
	 * @param aaSearchReservationRQ
	 */
	public void setRequest(AASearchReservationRQ aaSearchReservationRQ) {
		this.aaSearchReservationRQ = aaSearchReservationRQ;
	}

	/**
	 * Initializes the objects that will be used by the Response object
	 */
	private void initializeAASearchReservationRS() {
		aaSearchReservationRS.setResponseAttributes(new AAResponseAttributes());
		thisAirlineCode = AppSysParamsUtil.getDefaultCarrierCode();
	}

	/**
	 * Execute some logic based on the Request object and process and return the Response object
	 * 
	 * @return AASearchReservationRS
	 */
	public AASearchReservationRS execute() {

		// Initialize Return object
		initializeAASearchReservationRS();

		if (log.isDebugEnabled()) {
			log.debug("[" + thisAirlineCode + "-AAServices] has been called by LCC for reservation search");
		}

		// Prepare ReservationSearchDTO object
		ReservationSearchDTO reservationSearchDTO = populateReservationSearchDTO();

		try {
			// Search reservations
			Collection<ReservationListTO> reservations = AAServicesModuleUtils.getAirproxyReservationBD().searchReservations(
					reservationSearchDTO, null, null);

			// Populate ReservationSummary
			aaSearchReservationRS.getReservationSummary().addAll(populateReservationSummary(reservations));

			// Populate HeaderInfo
			aaSearchReservationRS.setHeaderInfo(AAUtils.populateHeaderInfo(null));

		} catch (Exception e) {
			log.error(e);
			AAExceptionUtil.addAAErrrors(aaSearchReservationRS.getResponseAttributes().getErrors(), e);
		} finally {
			/*
			 * Add Success. Success is an initialized empty object to signify that all is OK. If there are errors (from
			 * the executed previously code) then we return a null object, signifying that all is not OK. [note warnings
			 * are not considered errors]
			 */
			int numOfErrors = aaSearchReservationRS.getResponseAttributes().getErrors().size();
			AASuccess successType = (numOfErrors == 0 ? new AASuccess() : null);
			aaSearchReservationRS.getResponseAttributes().setSuccess(successType);
		}

		return aaSearchReservationRS;
	}

	/**
	 * Populate PnrModesDTO using {@link com.isa.connectivity.profiles.maxico.v1.required.dto.AASearchReservationRQ
	 * AASearchReservationRQ}
	 * 
	 * @return PnrModesDTO
	 */
	private ReservationSearchDTO populateReservationSearchDTO() {
		// No need any validations or additions as we have passed the processed data
		SearchReservatonOptions searchReservatonOptions = aaSearchReservationRQ.getSearchReservatonOptions();
		if (aaSearchReservationRQ.getSearchReservatonOptions().getCcAuthorCode() == null
				&& aaSearchReservationRQ.getSearchReservatonOptions().getCcAuthorCode() == null) {
			ReservationSearchDTO reservationSearchDTO = new ReservationSearchDTO();
			reservationSearchDTO.setSearchAll(false);
			reservationSearchDTO.setExactMatch(searchReservatonOptions.isExactMatch());
			reservationSearchDTO.setFirstName(searchReservatonOptions.getFirstName());
			reservationSearchDTO.setLastName(searchReservatonOptions.getLastName());
			reservationSearchDTO.setFromAirport(searchReservatonOptions.getFromAirport());
			reservationSearchDTO.setToAirport(searchReservatonOptions.getToAirport());
			reservationSearchDTO.setTelephoneNo(searchReservatonOptions.getTelephoneNo());
			reservationSearchDTO.setTelephoneNoForSQL(searchReservatonOptions.getTelephoneNoForSQL());
			reservationSearchDTO.setDepartureDate(searchReservatonOptions.getDepartureDate());
			reservationSearchDTO.setReturnDate(searchReservatonOptions.getReturnDate());
			reservationSearchDTO.setFlightNo(searchReservatonOptions.getFlightNo());
			reservationSearchDTO.setBookingClassType(searchReservatonOptions.getBookingClassType());
			reservationSearchDTO.setPassport(searchReservatonOptions.getPassport());
			reservationSearchDTO.setOriginChannelIds(searchReservatonOptions.getOriginChannelIds().size() > 0
					? searchReservatonOptions.getOriginChannelIds()
					: null);
			reservationSearchDTO.setETicket(searchReservatonOptions.getETicket());
			reservationSearchDTO.setPnr(searchReservatonOptions.getPnr());
			reservationSearchDTO.setInvoiceNumber(searchReservatonOptions.getInvoiceNumber());
			reservationSearchDTO.setPageNo(searchReservatonOptions.getPageNumber());
			reservationSearchDTO.setEarlyDepartureDate(searchReservatonOptions.getEarlyDepartureDate());
			reservationSearchDTO.setEmail(searchReservatonOptions.getEmail());
			reservationSearchDTO.setOwnerChannelId(searchReservatonOptions.getOwnerChannelId());
			reservationSearchDTO.setOwnerAgentCode(searchReservatonOptions.getOwnerAgentCode());
			reservationSearchDTO.setOriginAgentCode(searchReservatonOptions.getOriginAgentCode());
			reservationSearchDTO.setOriginUserId(searchReservatonOptions.getOriginUserId());
			if (searchReservatonOptions.getReservationStates() != null
					&& searchReservatonOptions.getReservationStates().size() > 0) {
				reservationSearchDTO.getReservationStates().addAll(searchReservatonOptions.getReservationStates());
			}
			reservationSearchDTO.setFilterZeroCredits(searchReservatonOptions.isFilterZeroCredits());
			reservationSearchDTO.setIncludePaxInfo(searchReservatonOptions.isIncludePaxInfo());
			reservationSearchDTO.setSearchIBEBookings(searchReservatonOptions.isSearchIBEBookings() && searchReservatonOptions.getAccessibleCarriers().contains(thisAirlineCode));

			reservationSearchDTO
					.setBookingCategories((searchReservatonOptions.getBookingCategories() != null && searchReservatonOptions
							.getBookingCategories().size() > 0) ? searchReservatonOptions.getBookingCategories() : null);

			return reservationSearchDTO;
		} else {
			// TODO
			return null;
		}
	}

	/**
	 * @return Collection<ReservationSummary>
	 */
	private Collection<ReservationSummary> populateReservationSummary(Collection<ReservationListTO> reservations) {
		Collection<ReservationSummary> reservationSummaries = new ArrayList<ReservationSummary>();
		for (ReservationListTO reservationListTO : reservations) {
			ReservationSummary reservationSummary = new ReservationSummary();
			reservationSummary.setOriginatorPnr(reservationListTO.getOriginatorPnr());
			reservationSummary.setPaxName(reservationListTO.getPaxName());
			reservationSummary.setPnrNo(reservationListTO.getPnrNo());
			reservationSummary.setPnrStatus(reservationListTO.getPnrStatus());
			reservationSummary.setReleaseTimeStamp(reservationListTO.getReleaseTimeStamp());
			reservationSummary.setRowNo(reservationListTO.getRowNo());
			reservationSummary.getFlightInfo().addAll(populateFlightInfo(reservationListTO.getFlightInfo()));
			// reservationSummary.getDryReservationSummary().setMarketingAirlineCode(reservationListTO.get)
			// TODO : we will need to store marketing carrier code in reservation table or in lcc tables.
			// Currently it is hard corded in lcc.
			reservationSummaries.add(reservationSummary);
		}
		return reservationSummaries;
	}

	/**
	 * @return Collection<ReservationSummary>
	 */
	private Collection<FlightInfo> populateFlightInfo(Collection<FlightInfoTO> flightInfoTOs) {
		Collection<FlightInfo> flightInfos = new ArrayList<FlightInfo>();
		for (FlightInfoTO flightInfoTO : flightInfoTOs) {
			FlightInfo flightInfo = new FlightInfo();
			flightInfo.setAirLine(flightInfoTO.getAirLine());
			flightInfo.setArrivalDate(flightInfoTO.getArrivalDate());
			flightInfo.setArrivalTime(flightInfoTO.getArrivalTime());
			flightInfo.setDepartureDate(flightInfoTO.getDepartureDate());
			flightInfo.setDepartureDateLong(flightInfoTO.getDepartureDateLong());
			flightInfo.setDepartureTime(flightInfoTO.getDepartureTime());
			flightInfo.setFlightNo(flightInfoTO.getFlightNo());
			flightInfo.setIsOpenReturnSegment(flightInfoTO.isOpenReturnSegment());
			flightInfo.setOrignNDest(flightInfoTO.getOrignNDest());
			flightInfo.setPaxCount(flightInfoTO.getPaxCount());
			flightInfo.setStatus(flightInfoTO.getStatus());
			flightInfo.setSubStatus(flightInfoTO.getSubStatus());
			flightInfo.setCosDescription(flightInfoTO.getCosDes());

			flightInfos.add(flightInfo);
		}
		return flightInfos;
	}

}