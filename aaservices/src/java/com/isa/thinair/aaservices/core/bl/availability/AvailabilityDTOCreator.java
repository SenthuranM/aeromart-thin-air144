package com.isa.thinair.aaservices.core.bl.availability;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AvailPreferences;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ClassOfServicePreferences;
import com.isa.connectivity.profiles.maxico.v1.common.dto.DoubleIntegerListMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.IntegerBooleanMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ModifyPreferences;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OldFareDetails;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OriginDestinationInformation;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OriginDestinationOption;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PassengerType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PassengerTypeQuantity;
import com.isa.connectivity.profiles.maxico.v1.common.dto.StringIntegerMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TravelPreferences;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TravelerInfoSummary;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAFlightAvailRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAFlightPriceRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAPOS;
import com.isa.thinair.aaservices.core.session.AASessionManager;
import com.isa.thinair.aaservices.core.util.AAUtils;
import com.isa.thinair.aaservices.core.util.TransformerUtil;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareIdDetailDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airinventory.api.dto.seatavailability.OriginDestinationInfoDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTO;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.ResUniqueIdMapper;
import com.isa.thinair.airproxy.api.utils.converters.AuxillaryHelper;
import com.isa.thinair.airproxy.api.utils.converters.AvailabilityConvertUtil;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.webplatform.api.util.PrivilegesKeys;


/**
 * @author Lasantha Pambagoda
 * @since Sep 29, 2009
 */
class AvailabilityDTOCreator {

	private List<OriginDestinationInformation> ondInfoLits = null;

	private TravelPreferences travelPreferences = null;

	private AvailPreferences availPreferences = null;

	private ClassOfServicePreferences classOfServicePreferences = null;

	private TravelerInfoSummary travelerInfoSummary = null;

	private AAPOS aaPos = null;

	private AvailableFlightSearchDTO availableSearchDTO = null;

	private String originLocation = null;

	private String destinationLocation = null;

	private int adultCount = 0;

	private int childCount = 0;

	private int infantCount = 0;

	private Date depatureDateTimeStart = null;

	private Date depatureDateTimeEnd = null;

	private Date returnDateTimeStart = null;

	private Date returnDateTimeEnd = null;

	private boolean isReturnFlight = false;

	AvailabilityDTOCreator(AAFlightAvailRQ aaFlightAvailRQ) {
		ondInfoLits = aaFlightAvailRQ.getOriginDestinationInformation();
		travelPreferences = aaFlightAvailRQ.getTravelPreferences();
		classOfServicePreferences = aaFlightAvailRQ.getClassOfServicePreferences();
		availPreferences = aaFlightAvailRQ.getAvailPreferences();
		travelerInfoSummary = aaFlightAvailRQ.getTravelerInfoSummary();
		aaPos = aaFlightAvailRQ.getAaPos();
	}

	AvailabilityDTOCreator(AAFlightPriceRQ aaFlightPriceRQ) {
		ondInfoLits = aaFlightPriceRQ.getOriginDestinationInformation();
		travelPreferences = aaFlightPriceRQ.getTravelPreferences();
		classOfServicePreferences = aaFlightPriceRQ.getClassOfServicePreferences();
		availPreferences = aaFlightPriceRQ.getAvailPreferences();
		travelerInfoSummary = aaFlightPriceRQ.getTravelerInfoSummary();
		aaPos = aaFlightPriceRQ.getAaPos();
	}

	public void createDTO() {
		availableSearchDTO = new AvailableFlightSearchDTO();
		availableSearchDTO.setModifyBooking(availPreferences.isModifyBooking());
		Map<Double, List<Integer>> flightDateFareMap = new HashMap<Double, List<Integer>>();
		List<DoubleIntegerListMap> dateFareValue = availPreferences.getFlightDateFareMap();
		List<String> pnrSegRPHs = new ArrayList<String>();
		for (int i = 0; i < dateFareValue.size(); i++) {
			Double fare = dateFareValue.get(i).getKey();
			List<Integer> dateGapList = dateFareValue.get(i).getValue();
			flightDateFareMap.put(fare, dateGapList);
		}
		availableSearchDTO.setFlightDateFareMap(flightDateFareMap);

		if (!availPreferences.isFareCalendarSearch()) {
			if (availPreferences.getQuoteOndFlexi() != null) {
				for (IntegerBooleanMap mapObj : availPreferences.getQuoteOndFlexi()) {
					availableSearchDTO.getOndQuoteFlexi().put(mapObj.getKey(), mapObj.isValue());
				}
			}

			if (availPreferences.getOndFlexiSelected() != null) {
				for (IntegerBooleanMap mapObj : availPreferences.getOndFlexiSelected()) {
					availableSearchDTO.getOndSelectedFlexi().put(mapObj.getKey(), mapObj.isValue());
				}
			}
		}

		Map<Integer, FareIdDetailDTO> flightSegFareDetails = new HashMap<Integer, FareIdDetailDTO>();
		availableSearchDTO.setFlightSegFareDetails(flightSegFareDetails);
		int ondSeq = 0;
		boolean hasOldFareDetails = true;
		boolean isRouteChange = true;
		for (OriginDestinationInformation ondInfo : ondInfoLits) {
			OriginDestinationInfoDTO ondInfoDTO = new OriginDestinationInfoDTO();
			ondInfoDTO.setOrigin(ondInfo.getOriginLocation());
			ondInfoDTO.setDestination(ondInfo.getDestinationLocation());
			ondInfoDTO.setDepartureDateTimeStart(ondInfo.getDepartureDateTimeStart());
			ondInfoDTO.setDepartureDateTimeEnd(ondInfo.getDepartureDateTimeEnd());
			ondInfoDTO.setArrivalDateTimeStart(ondInfo.getArrivalDateTimeStart());
			ondInfoDTO.setArrivalDateTimeEnd(ondInfo.getArrivalDateTimeEnd());

			if (availableSearchDTO.getOndQuoteFlexi().containsKey(ondInfo.getOndSequence())) {
				ondInfoDTO.setQuoteFlexi(availableSearchDTO.getOndQuoteFlexi().get(ondInfo.getOndSequence()));
			}

			// TODO : check with Rumesh
			Date prefDTStart = ondInfo.getPreferredDepartureDateTimeStart();
			Date prefDTEnd = ondInfo.getPreferredDepartureDateTimeEnd();

			if ((prefDTStart == null || prefDTEnd == null) && ondInfo.getPreferredDate() != null) {
				prefDTStart = CalendarUtil.getStartTimeOfDate(ondInfo.getPreferredDate());
				prefDTEnd = CalendarUtil.getEndTimeOfDate(ondInfo.getPreferredDate());
			}

			ondInfoDTO.setPreferredDateTimeStart(prefDTStart);
			ondInfoDTO.setPreferredDateTimeEnd(prefDTEnd);
			ondInfoDTO.setPreferredDate(ondInfo.getPreferredDate());
			ondInfoDTO.setFlightSegmentIds((List<Integer>) getSegmentIdList(ondInfo));
			ondInfoDTO.setPreferredClassOfService(ondInfo.getPreferredCabinClass());
			ondInfoDTO.setPreferredLogicalCabin(ondInfo.getPreferredLogicalCabin());
			ondInfoDTO.setPreferredBookingClass(ondInfo.getPreferredBookingClass());
			ondInfoDTO.setSegmentBookingClass(TransformerUtil.transformStringStringMap(ondInfo.getOndSegmentBookingClasses()));
			ondInfoDTO.setPreferredBookingType(ondInfo.getPreferredBookingType());
			ondInfoDTO.setExistingFlightSegIds(ondInfo.getExistingFlightSegIds());
			pnrSegRPHs.addAll(ondInfo.getExistingPnrSegIds());
			ondInfoDTO.setFlownOnd(ondInfo.isFlownOnd());
			ondInfoDTO.setSpecificFlightsAvailability(ondInfo.isSpecificFlightsAvailability());

			ondInfoDTO.setHasInwardCxnFlight(isHasCxnFlight(ondInfo, ondInfoDTO, true));
			ondInfoDTO.setHasOutwardCxnFlight(isHasCxnFlight(ondInfo, ondInfoDTO, false));

			ondInfoDTO.setUnTouchedOnd(ondInfo.isUnTouchedOnd());
			if (ondInfo.getUnTouchedResSegList() != null && ondInfo.getUnTouchedResSegList().size() > 0) {
				ondInfoDTO.setUnTouchedResSegList(ondInfo.getUnTouchedResSegList());
			}

			if (AppSysParamsUtil.isReQuoteOnlyRequiredONDs()
					&& getOwnOldPerPaxFareTO(ondInfo.getOldFareDetails(), null, null) != null) {

				Map<Integer, String> flightSegIdMap = getSegmentIdSegCodeMap(ondInfo);

				if (flightSegIdMap != null) {
					for (Map.Entry<Integer, String> entry : flightSegIdMap.entrySet()) {
						OldFareDetails matchingOldFare = getOwnOldPerPaxFareTO(ondInfo.getOldFareDetails(), entry.getKey(),
								entry.getValue());
						if (matchingOldFare != null) {
							FareIdDetailDTO fareIdDetailDTO = new FareIdDetailDTO();
							fareIdDetailDTO.setFareId(matchingOldFare.getOldFareId());
							fareIdDetailDTO.setFareType(matchingOldFare.getOldFareType());
							flightSegFareDetails.put(entry.getKey(), fareIdDetailDTO);
						}

					}
				}

			}

			if (ondInfo.getOldFareDetails() != null && ondInfo.getOldFareDetails().size() > 0) {
				FareTO oldPerPaxFareTO = new FareTO();
				OldFareDetails oldFareDetails = getOwnOldPerPaxFareTO(ondInfo.getOldFareDetails(), null, null);
				if (oldFareDetails != null) {
					if (oldFareDetails.getOldFareType() != null) {
						oldPerPaxFareTO.setFareType(Integer.toString(oldFareDetails.getOldFareType()));
					}
					oldPerPaxFareTO.setCarrierCode(oldFareDetails.getCarrierCode());
					oldPerPaxFareTO.setFareId(oldFareDetails.getOldFareId());
					oldPerPaxFareTO.setAmount(oldFareDetails.getOldFareAmount());
					oldPerPaxFareTO.setSegmentCode(oldFareDetails.getOndCode());
					oldPerPaxFareTO.setAppliedFlightSegId(oldFareDetails.getFlightSegId());
					oldPerPaxFareTO.setLogicalCCCode(oldFareDetails.getLogicalCCCode());
					oldPerPaxFareTO.setBulkTicketFareRule(oldFareDetails.isBulkTicketFareRule() != null 
							? oldFareDetails.isBulkTicketFareRule() 
							: false);
					

				}
				ondInfoDTO.setOldPerPaxFareTO(oldPerPaxFareTO);
			} else {
				hasOldFareDetails = false;
			}

			if ((ondInfo.getDateChangedResSegList() != null && ondInfo.getDateChangedResSegList().size() > 0)
					|| availPreferences.isFromNameChange()) {
				isRouteChange = false;
			}

			if (ondInfo.getHubTimeDetailMap() != null) {
				Map<String, Integer> hubTimeDetailMap = new HashMap<String, Integer>();
				for (StringIntegerMap hubTimeEntry : ondInfo.getHubTimeDetailMap()) {
					hubTimeDetailMap.put(hubTimeEntry.getKey(), hubTimeEntry.getValue());
				}
				ondInfoDTO.setHubTimeDetailMap(hubTimeDetailMap);
			}

			ondInfoDTO.setPreferredBundledFarePeriodId(ondInfo.getPreferredBundledFarePeriodId());

			if (ondSeq == OndSequence.OUT_BOUND) {
				originLocation = ondInfo.getOriginLocation();
				destinationLocation = ondInfo.getDestinationLocation();
				depatureDateTimeStart = ondInfo.getDepartureDateTimeStart();
				depatureDateTimeEnd = ondInfo.getDepartureDateTimeEnd();
			} else {
				returnDateTimeStart = ondInfo.getDepartureDateTimeStart();
				returnDateTimeEnd = ondInfo.getDepartureDateTimeEnd();
			}
			if (ondInfoLits.size() == 2) {
				isReturnFlight = true;
			} else if (ondInfoLits.size() == 1 && ondInfo.getOndSequence() == OndSequence.IN_BOUND) {
				availPreferences.setInboundOnd(true);
			}

			if (AppSysParamsUtil.enableCityBasesFunctionality()) {
				ondInfoDTO.setDepartureCitySearch(ondInfo.isDepartureCitySearch());
				ondInfoDTO.setArrivalCitySearch(ondInfo.isArrivalCitySearch());
			}
			
			availableSearchDTO.addOriginDestination(ondInfoDTO);
			ondSeq++;
		}

		// Setting the open return dummy segment details for availability search
		if (travelPreferences.isOpenReturn() && availableSearchDTO.getOriginDestinationInfoDTOs().size() == 1) {
			availableSearchDTO.addOriginDestination(availableSearchDTO.getOriginDestinationInfoDTOs().get(0).cloneAndSwapOnD());
		}

		// availableSearchDTO.setReturnValidityPeriodId(travelPreferences.ge);

		if (AppSysParamsUtil.isEnforceSameHigher() && hasOldFareDetails
				&& (!isRouteChange || !availPreferences.isRequoteFlightSearch())
				&& !availPreferences.isAllowOverrideSameOrHigherFareMod()) {
			availableSearchDTO.setEnforceFareCheckForModOnd(true);
		}

		for (PassengerTypeQuantity passengerTypeQuantity : travelerInfoSummary.getAirTravelerAvail().getPassengerTypeQuantity()) {
			if (passengerTypeQuantity.getPassengerType() == PassengerType.ADT) {
				adultCount = passengerTypeQuantity.getQuantity();
				availableSearchDTO.setAdultCount(adultCount);
			} else if (passengerTypeQuantity.getPassengerType() == PassengerType.CHD) {
				childCount = passengerTypeQuantity.getQuantity();
				availableSearchDTO.setChildCount(childCount);
			} else if (passengerTypeQuantity.getPassengerType() == PassengerType.INF) {
				infantCount = passengerTypeQuantity.getQuantity();
				availableSearchDTO.setInfantCount(infantCount);
			}
		}

		UserPrincipal userPrincipal = AASessionManager.getInstance().getCurrentUserPrincipal();

		
		boolean viewFlightsWithNoAvailability = AASessionManager.getUserPrivileges().contains(PrivilegesKeys.MakeResPrivilegesKeys.MAKE_RES_ALLFLIGHTS);
		String bookingType = getBookingType(travelPreferences.getBookingType());
		
		availableSearchDTO.setAvailabilityRestrictionLevel(AvailabilityConvertUtil.getAvailabilityRestrictionLevel(viewFlightsWithNoAvailability,bookingType));
		
		availableSearchDTO.setAppIndicator(availPreferences.getAppIndicator());
		int salesChannel = userPrincipal.getSalesChannel();
		if (AAUtils.getSalesChannelFromPOS(aaPos) != null) {
			salesChannel = Integer.parseInt(AAUtils.getSalesChannelFromPOS(aaPos));
		}
		availableSearchDTO.setChannelCode(salesChannel);
		if (!SalesChannelsUtil.isAppEngineWebOrMobile(availPreferences.getAppIndicator())) {
			availableSearchDTO.setPosAirport(aaPos.getAirportCode());
		}
		availableSearchDTO.setAgentCode(userPrincipal.getAgentCode());

		if (availPreferences.isIncludeHalfReturn()) {
			if (!availPreferences.isModifyBooking()) {
				availableSearchDTO.setHalfReturnFareQuote(AppSysParamsUtil.isAllowHalfReturnFaresForNewBookings());
			} else {
				if (availPreferences.isAddSegment()) {
					availableSearchDTO.setHalfReturnFareQuote(AppSysParamsUtil.isAllowHalfReturnFaresForNewBookings());
				} else {
					availableSearchDTO.setHalfReturnFareQuote(AppSysParamsUtil.isAllowHalfReturnFaresForModification());
				}
			}
		}

		if (AvailableFlightSearchDTO.JourneyType.OPEN_JAW == availableSearchDTO.getJourneyType()) {
			availableSearchDTO.setHalfReturnFareQuote(AppSysParamsUtil.isAllowHalfReturnFaresForOpenJawJourney());
		}

		availableSearchDTO.setStayOverMillis(availPreferences.getStayOverTimeInMillis());
		availableSearchDTO.setInboundFareQuote(availPreferences.isInboundOnd());
		availableSearchDTO.setQuoteFares(availPreferences.isQuoteFares() || availPreferences.isFromFareQuote());
		availableSearchDTO.setPromoCode(availPreferences.getPromoCode());
		availableSearchDTO.setBankIdentificationNo(availPreferences.getBankIdNumber());
		availableSearchDTO.setPreferredLanguage(availPreferences.getPrefLanguage());
		availableSearchDTO.setPointOfSale(availPreferences.getPointOfSale());
		availableSearchDTO.setPreserveOndOrder(availPreferences.isPreserveOndOrder());
		availableSearchDTO.setIncludeHRTFares(availPreferences.isIncludeHRTFares());

		if (availPreferences.isRequoteFlightSearch() != null) {
			availableSearchDTO.setRequoteFlightSearch(availPreferences.isRequoteFlightSearch());
		}

		if (availPreferences.getLastFareQuotedDate() != null) {
			availableSearchDTO.setLastFareQuotedDate(availPreferences.getLastFareQuotedDate());
		}

		if (availPreferences.isFQWithinValidity() != null) {
			availableSearchDTO.setFQWithinValidity(availPreferences.isFQWithinValidity());
		}

		if (availPreferences.isFQOnLastFQDate() != null) {
			availableSearchDTO.setFQOnLastFQDate(availPreferences.isFQOnLastFQDate());
		}

		if (availPreferences.isMulticitySearch() != null) {
			availableSearchDTO.setMultiCitySearch(availPreferences.isMulticitySearch());
		}

		if (availPreferences.isFromNameChange() != null) {
			availableSearchDTO.setFromNameChange(availPreferences.isFromNameChange());
		}

		// FIXME DILAN - Temporary fix until dry/interline support for fare calendar
		if (SalesChannelsUtil.isAppEngineWebOrMobile(availPreferences.getAppIndicator())
				&& !availableSearchDTO.isMultiCitySearch() && !availableSearchDTO.isRequoteFlightSearch()
				&& !availableSearchDTO.isFromNameChange()) {
			availableSearchDTO.setFareCalendarSearch(true);
		}

		// Since promotion & bundled fare available only for dry booking
		availableSearchDTO.setBundledFareApplicable(availPreferences.isBundledServiceApplicable());
		availableSearchDTO.setPromoApplicable(availPreferences.isSingleCarrier());

		// TODO:RW set point of Sale

		// FIXME DILAN 21 DEC
		// availableSearchDTO.setModifyBooking(availPreferences.isModifyBooking());

		AuxillaryHelper auxHelper = new AuxillaryHelper(AirproxyModuleUtils.getReservationAuxilliaryBD());

		if (pnrSegRPHs.size() > 0) {
			List<Integer> pnrSegIds = new ArrayList<Integer>(ResUniqueIdMapper.getPnrSegIds(pnrSegRPHs));
			Map<Integer, String> fltSegBkgClasses = auxHelper.getFltSegWiseBkgClasses(pnrSegIds);
			availableSearchDTO.setFltSegWiseExtBookingClasses(fltSegBkgClasses);
			availableSearchDTO.setFltSegWiseExtCabinClasses(auxHelper.getFltSegWiseCabinClasses(pnrSegIds));
		}

		if (availPreferences.getModifyPref() != null) {
			ModifyPreferences modifyPref = availPreferences.getModifyPref();
			availableSearchDTO.setExistingFlightSegments(getFlightSegmentList(modifyPref.getExistingSegments()));
			availableSearchDTO.setModifiedFlightSegments(getFlightSegmentList(modifyPref.getModifiedSegmentsList()));
			AvailabilityConvertUtil.setFirstAndLastArrivalDates(availableSearchDTO,
					availableSearchDTO.getExistingFlightSegments(), availableSearchDTO.getModifiedFlightSegments());
			availableSearchDTO
					.setInverseSegmentsOfModifiedFlightSegment(getFlightSegmentList(modifyPref.getInverseSegmentsList()));

			if (modifyPref.getOldFareAmount() != null && AppSysParamsUtil.isEnforceSameHigher()) {
				availableSearchDTO.setModifiedOndPaxFareAmount(modifyPref.getOldFareAmount());
				availableSearchDTO.setModifiedOndFareId(modifyPref.getOldFareId());
				availableSearchDTO.setModifiedOndFareType(modifyPref.getOldFareType());
			}
			if (availPreferences.getModifyPref().isInboundModified() != null) {
				availableSearchDTO.setInboundFareQuote(availPreferences.getModifyPref().isInboundModified());
			}

			availableSearchDTO.setInboundOutboundChanged(AvailabilityConvertUtil.isInboundOutboundChanged(availableSearchDTO));

			if (availableSearchDTO.isInboundOutboundChanged()
					|| availableSearchDTO.getInverseSegmentsOfModifiedFlightSegment() == null
					|| availableSearchDTO.getInverseSegmentsOfModifiedFlightSegment().isEmpty()) {
				availableSearchDTO.setHalfReturnFareQuote(false);
			}

			List<FlightSegment> modifiedFlightSegments = modifyPref.getModifiedSegmentsList();
			if (modifiedFlightSegments != null && !modifiedFlightSegments.isEmpty()) {
				boolean isDefaultCarrierModification = true;
				String defaultCarrier = AppSysParamsUtil.getDefaultCarrierCode();
				for (FlightSegment modifiedFlightSegment : modifiedFlightSegments) {
					if (!defaultCarrier.equalsIgnoreCase(modifiedFlightSegment.getOperatingAirline())) {
						isDefaultCarrierModification = false;
						break;
					}
				}
				if (isDefaultCarrierModification) {
					availableSearchDTO.setInverseFareID(modifyPref.getInverseFareId());
				}
			}

			if (availPreferences.isModifyBooking() && !AppSysParamsUtil.isAllowHalfReturnSegmentCombinability()
					&& modifyPref.getOldFareType() != null && availableSearchDTO.isHalfReturnFareQuote()) {
				FareRuleProxy fareRuleProxy = new FareRuleProxy();
				try {
					if (fareRuleProxy.loadFareRule(modifyPref.getOldFareType()).getReturnFlag() == 'N') {
						availableSearchDTO.setHalfReturnFareQuote(false);
					}
				} catch (Exception e) {

				}
			}
		}
		availableSearchDTO.setPnr(availPreferences.getPnr());
	}

	private static Collection<FlightSegmentDTO> getFlightSegmentList(List<FlightSegment> fltSegList) {
		List<FlightSegmentDTO> outFltSegList = null;
		if (fltSegList != null && fltSegList.size() > 0) {
			outFltSegList = new ArrayList<FlightSegmentDTO>();
			for (FlightSegment fSeg : fltSegList) {
				outFltSegList.add(populateFlightSegmentDTO(fSeg));
			}
		}
		return outFltSegList;
	}

	/**
	 * Convert FlightSegment to FlightSegmentDTO
	 * 
	 * @param flightSegment
	 * @return
	 */
	private static FlightSegmentDTO populateFlightSegmentDTO(FlightSegment flightSegment) {
		FlightSegmentDTO flightSegDTO = new FlightSegmentDTO();
		flightSegDTO.setFlightId(flightSegment.getFlightId());
		flightSegDTO.setSegmentCode(flightSegment.getSegmentCode());
		flightSegDTO.setFromAirport(flightSegment.getDepartureAirportCode());
		flightSegDTO.setToAirport(flightSegment.getArrivalAirportCode());
		flightSegDTO.setDepartureDateTime(flightSegment.getDepatureDateTime());
		flightSegDTO.setArrivalDateTime(flightSegment.getArrivalDateTime());
		flightSegDTO.setDepartureDateTimeZulu(flightSegment.getDepatureDateTimeZulu());
		flightSegDTO.setArrivalDateTimeZulu(flightSegment.getArrivalDateTimeZulu());
		flightSegDTO.setFlightNumber(flightSegment.getFlightNumber());
		flightSegDTO.setSegmentId(flightSegment.getSegmentId());
		flightSegDTO.setOperationTypeID((flightSegment.getOperationType() == 0) ? null : flightSegment.getOperationType());
		flightSegDTO.setFlightStatus(flightSegment.getFlightStatus());
		return flightSegDTO;
	}

	private String getBookingType(BookingType bookingType) {
		if (bookingType == BookingType.STANDBY) {
			return BookingClass.BookingClassType.STANDBY;
		} else if (bookingType == BookingType.OPEN_RETURN) {
			return BookingClass.BookingClassType.OPEN_RETURN;
		}
		return BookingClass.BookingClassType.NORMAL;
	}

	private Collection<Integer> getSegmentIdList(OriginDestinationInformation ondInfo) {

		Collection<Integer> segmentIdList = null;

		if (ondInfo.getOriginDestinationOptions() != null
				&& ondInfo.getOriginDestinationOptions().getOriginDestinationOption() != null
				&& ondInfo.getOriginDestinationOptions().getOriginDestinationOption().size() > 0) {
			OriginDestinationOption ondOption = ondInfo.getOriginDestinationOptions().getOriginDestinationOption().get(0);
			if (ondOption.getFlightSegment() != null && ondOption.getFlightSegment().size() > 0) {
				segmentIdList = new ArrayList<Integer>();
				for (FlightSegment segment : ondOption.getFlightSegment()) {
					segmentIdList.add(new Integer(segment.getFlightRefNumber()));
				}
			}
		}
		return segmentIdList;
	}

	private Map<Integer, String> getSegmentIdSegCodeMap(OriginDestinationInformation ondInfo) {

		Map<Integer, String> segmentIdMap = null;

		if (ondInfo.getOriginDestinationOptions() != null
				&& ondInfo.getOriginDestinationOptions().getOriginDestinationOption() != null
				&& ondInfo.getOriginDestinationOptions().getOriginDestinationOption().size() > 0) {
			OriginDestinationOption ondOption = ondInfo.getOriginDestinationOptions().getOriginDestinationOption().get(0);
			if (ondOption.getFlightSegment() != null && ondOption.getFlightSegment().size() > 0) {
				segmentIdMap = new HashMap<Integer, String>();
				for (FlightSegment segment : ondOption.getFlightSegment()) {
					segmentIdMap.put(new Integer(segment.getFlightRefNumber()), segment.getSegmentCode());
				}
			}
		}
		return segmentIdMap;
	}

	private boolean isHasCxnFlight(OriginDestinationInformation ondInfo, OriginDestinationInfoDTO ondInfoDTO, boolean isInward) {
		boolean hasCxnFlight = false;
		if (ondInfo.getOriginDestinationOptions() != null
				&& ondInfo.getOriginDestinationOptions().getOriginDestinationOption() != null
				&& ondInfo.getOriginDestinationOptions().getOriginDestinationOption().size() > 0) {
			OriginDestinationOption ondOption = ondInfo.getOriginDestinationOptions().getOriginDestinationOption().get(0);
			if (ondOption != null) {
				if (isInward) {
					if (ondOption.getInboundConnectionSegment() != null) {
						hasCxnFlight = true;
						ondInfoDTO.setInwardCxnFlightArriTime(ondOption.getInboundConnectionSegment().getArrivalDateTimeZulu());
					}
				} else {
					if (ondOption.getOutboundConnectionSegment() != null) {
						hasCxnFlight = true;
						ondInfoDTO.setOutwardCxnFlightDepTime(ondOption.getOutboundConnectionSegment().getDepatureDateTimeZulu());
					}
				}

			}
		}
		return hasCxnFlight;
	}

	private OldFareDetails getOwnOldPerPaxFareTO(List<OldFareDetails> oldFareDetails, Integer flightSegId, String segCode) {
		if (oldFareDetails != null) {
			String defaultCarrier = AppSysParamsUtil.getDefaultCarrierCode();
			String busCarrier = null;
			try {
				busCarrier = AirproxyModuleUtils.getFlightBD().getDefaultBusCarrierCode();
			} catch (Exception e) {
				// log errors
			}
			for (OldFareDetails oldFareTO : oldFareDetails) {
				if (oldFareTO != null
						&& (defaultCarrier.equals(oldFareTO.getCarrierCode()) || (busCarrier != null && busCarrier
								.equals(oldFareTO.getCarrierCode())))) {
					if ((flightSegId != null && flightSegId.equals(oldFareTO.getFlightSegId()))
							|| (segCode != null && segCode.equals(oldFareTO.getOndCode()))
							|| (flightSegId == null && segCode == null)) {
						return oldFareTO;
					}
				}
			}
		}
		return null;
	}

	public AvailableFlightSearchDTO getAvailableSearchDTO() {
		return availableSearchDTO;
	}

	public String getOriginLocation() {
		return originLocation;
	}

	public String getDestinationLocation() {
		return destinationLocation;
	}

	public int getAdultCount() {
		return adultCount;
	}

	public int getChildCount() {
		return childCount;
	}

	public int getInfantCount() {
		return infantCount;
	}

	public Date getDepatureDateTimeStart() {
		return depatureDateTimeStart;
	}

	public Date getDepatureDateTimeEnd() {
		return depatureDateTimeEnd;
	}

	public Date getReturnDateTimeStart() {
		return returnDateTimeStart;
	}

	public Date getReturnDateTimeEnd() {
		return returnDateTimeEnd;
	}

	public boolean isReturnFlight() {
		return isReturnFlight;
	}

	public boolean isInbound() {
		return availPreferences.isInboundOnd();
	}
}
