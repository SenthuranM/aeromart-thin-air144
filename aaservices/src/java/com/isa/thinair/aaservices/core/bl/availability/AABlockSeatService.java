package com.isa.thinair.aaservices.core.bl.availability;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.SegmentFareType;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AABlockSeatRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AABlockSeatRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.core.session.AASessionManager;
import com.isa.thinair.aaservices.core.transaction.TransactionalReservationProcess;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SeatDistribution;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.util.PrivilegesKeys;

public class AABlockSeatService {
	private static final Log log = LogFactory.getLog(AABlockSeatService.class);

	private TransactionalReservationProcess transactionalReservationProcess;

	private AABlockSeatRQ aaBlockSeatRQ;

	private AABlockSeatRS aaBlockSeatRS;

	public void setAABlockSeatRQ(AABlockSeatRQ aaBlockSeatRQ) {
		this.aaBlockSeatRQ = aaBlockSeatRQ;
	}

	public void setTransactionalReservationProcess(TransactionalReservationProcess transactionalReservationProcess) {
		this.transactionalReservationProcess = transactionalReservationProcess;
	}

	private void intializeAABlockSeatRS() {
		aaBlockSeatRS = new AABlockSeatRS();
		aaBlockSeatRS.setResponseAttributes(new AAResponseAttributes());
	}

	public AABlockSeatRS execute() {
		intializeAABlockSeatRS();

		try {
			// authentication for allow seat blocking
			if (AASessionManager.getUserPrivileges().contains(PrivilegesKeys.MakeResPrivilegesKeys.ALLOW_SEATBLOCKING)) {

				if (transactionalReservationProcess == null) {
					throw new ModuleException("No Transactional Reservation Process found for "
							+ aaBlockSeatRQ.getTransactionID());
				}

				List<SegmentFareType> selectedFareTypes = aaBlockSeatRQ.getSelectedFareTypes();

				// already block seat removing from the session and inventory
				Collection alreadyBlockedSeats = transactionalReservationProcess.getBlockSeats(selectedFareTypes);
				if (alreadyBlockedSeats != null) {
					AAServicesModuleUtils.getReservationBD().releaseBlockedSeats(alreadyBlockedSeats);
					transactionalReservationProcess.setBlockSeats(selectedFareTypes, null);
				}

				Collection<OndFareDTO> ondFareDTOs = transactionalReservationProcess
						.getOndFareDTOListForReservation(selectedFareTypes);

				Collection blockSeatIDs = AAServicesModuleUtils.getReservationBD().blockSeats(ondFareDTOs, null);

				if (blockSeatIDs == null || blockSeatIDs.size() == 0) {
					boolean isAlreadyBlockedSeat = false;
					isAlreadyBlockedSeat = checkAlreadyBlockedSeat(ondFareDTOs);
					if (!isAlreadyBlockedSeat) {
						throw new ModuleException("Seat Blocking failed");
					}
				} else {
					transactionalReservationProcess.setBlockSeats(selectedFareTypes, blockSeatIDs);
				}
			}

		} catch (ModuleException ex) {
			if (transactionalReservationProcess != null) {
				transactionalReservationProcess.setBlockSeats(aaBlockSeatRQ.getSelectedFareTypes(), null);
			}
			log.error("Error occurred during block seat", ex);
			AAExceptionUtil.addAAErrrors(aaBlockSeatRS.getResponseAttributes().getErrors(), ex);
		} finally {
			int numOfErrors = aaBlockSeatRS.getResponseAttributes().getErrors().size();
			AASuccess successType = (numOfErrors == 0 ? new AASuccess() : null);
			aaBlockSeatRS.getResponseAttributes().setSuccess(successType);
		}

		return aaBlockSeatRS;
	}

	private boolean checkAlreadyBlockedSeat(Collection<OndFareDTO> ondFareDTOs) {
		if (ondFareDTOs != null && ondFareDTOs.size() > 0) {
			Collection<SegmentSeatDistsDTO> segmentSeatDistsDTOs = new ArrayList<SegmentSeatDistsDTO>();
			for (OndFareDTO ondFareDTO : ondFareDTOs) {
				for (SegmentSeatDistsDTO segmentSeatDistsDTO : ondFareDTO.getSegmentSeatDistsDTO()) {
					segmentSeatDistsDTO.setBelongToInboundOnd(ondFareDTO.isInBoundOnd());
					segmentSeatDistsDTOs.add(segmentSeatDistsDTO);
					for (SeatDistribution seatDistribution : segmentSeatDistsDTO.getSeatDistribution()) {
						if (seatDistribution.isSameBookingClassAsExisting() || seatDistribution.isFlownOnd()) {
							return true;
						}
					}

				}
			}
		}
		return false;
	}
}
