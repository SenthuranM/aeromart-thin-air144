package com.isa.thinair.aaservices.core.bl.ancillary;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AirportServiceRequestMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingAirportServiceRequest;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingBaggageRequest;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingFlightSegment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingInsuranceRequest;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingMealRequest;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingSeatRequest;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingSpecialRequests;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingSpecialServiceRequest;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingSystems;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlexiQuotation;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightRefNumberRPHList;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentAirportServiceRequests;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentBaggages;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentMeals;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentSeats;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentSpecialServiceRequests;
import com.isa.connectivity.profiles.maxico.v1.common.dto.InsuranceQuotation;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ReservationSummaryLite;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SegmentSeatMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SelectedBaggage;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SelectedFlexi;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SelectedInsurance;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SelectedMeal;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SelectedSSR;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SelectedSeat;
import com.isa.connectivity.profiles.maxico.v1.common.dto.StringIntegerMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.StringStringIntegerMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.StringStringListMap;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAncillaryRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAncillaryRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASelectedAncillaryRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASelectedAncillaryRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.core.session.AASessionManager;
import com.isa.thinair.aaservices.core.session.AAUserSession;
import com.isa.thinair.aaservices.core.transaction.TransactionalReservationProcess;
import com.isa.thinair.aaservices.core.transaction.TransactionalReservationProcessBL;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.AAUtils;
import com.isa.thinair.aaservices.core.util.AncillaryUtil;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.util.AuthorizationUtil;

public class AAAncillaryServiceBL implements TransactionalReservationProcessBL {

	private static final Log log = LogFactory.getLog(AAAncillaryServiceBL.class);

	private TransactionalReservationProcess transactionalReservationProcess;

	public AAAncillaryRS getAncillaryDetail(AAAncillaryRQ aaAncillaryRQ) {
		List<BookingFlightSegment> bookingFlightSegments = aaAncillaryRQ.getBookingFlightSegment();
		BookingSpecialRequests bookingSpecialRequests = aaAncillaryRQ.getSpecialRequests();

		String pnr = bookingSpecialRequests.getPnr();
		List<Integer> pnrSegIds;
		BookingSeatRequest seatRequest = bookingSpecialRequests.getSeatRequest();
		BookingMealRequest mealRequest = bookingSpecialRequests.getMealRequest();
		BookingBaggageRequest baggageRequest = bookingSpecialRequests.getBaggageRequest();
		BookingSpecialServiceRequest specialServiceRequest = bookingSpecialRequests.getSpecialServiceRequest();
		ReservationSummaryLite reservationSummaryLite = bookingSpecialRequests.getReservationSummary();
		List<StringIntegerMap> ondWiseBundlePeriodIDs = getCarrierBundlePeriodIDs(
				bookingSpecialRequests.getOndWiseBundlePeriodIDMap());
		Map<Integer, Set<String>> flightSegIdWiseSelectedMeals = null;
		Map<Integer, Set<String>> flightSegIdWiseSelectedSSRs = null;
		String selectedLanguage = "";


		if (specialServiceRequest != null) {
			flightSegIdWiseSelectedSSRs = AncillaryUtil
					.getFlightSegIdWiseSelectedAnci(specialServiceRequest.getFlightRefWiseSelectedSSRs());
		}

		BookingInsuranceRequest insuranceRequest = bookingSpecialRequests.getInsuranceRequest();
		BookingAirportServiceRequest airportServiceRequest = bookingSpecialRequests.getAirportServiceRequest();

		AAAncillaryRS aaAncillaryRS = new AAAncillaryRS();
		aaAncillaryRS.setResponseAttributes(new AAResponseAttributes());
		aaAncillaryRS.getResponseAttributes().setSuccess(new AASuccess());

		BookingSystems appEngine = aaAncillaryRQ.getAaPos().getMarketingBookingChannel();

		try {
			Collection<String> privilegeKeys = AASessionManager.getInstance()
					.getCurrUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);

			if (mealRequest != null) {
				pnrSegIds = new ArrayList<Integer>();
				if (reservationSummaryLite != null) {
					for (ReservationSummaryLite.AirSegment segment : reservationSummaryLite.getAirSegment()) {
						pnrSegIds.add(segment.getAirSegmentId());
					}
				}

				selectedLanguage = mealRequest.getSelectedLanguage();
				// flightSegIdWiseSelectedMeals =
				// AncillaryUtil.getFlightSegIdWiseSelectedAnci(mealRequest
				// .getFlightRefWiseSelectedMeals());
				flightSegIdWiseSelectedMeals = AAServicesModuleUtils.getMealBD().getExistingMeals(pnrSegIds);
			}

			if (seatRequest != null) {
				List<BookingFlightSegment> flightSegments = getListOfRequestedFlightSegments(bookingFlightSegments,
						seatRequest.getFlightRefNumberRPHList());

				AASeatMapBL aaSeatMapBL = new AASeatMapBL();
				aaSeatMapBL.setTransactionalReservationProcess(transactionalReservationProcess);
				aaSeatMapBL.setPnr(pnr);
				selectedLanguage = bookingSpecialRequests.getSelectedLanguage();
				List<SegmentSeatMap> segmentSeatMaps = aaSeatMapBL.execute(flightSegments, selectedLanguage, appEngine);
				aaAncillaryRS.getSegmentSeatMaps().addAll(segmentSeatMaps);
			}

			if (mealRequest != null) {
				AAMealsBL aaMealsBL = new AAMealsBL();
				aaMealsBL.setTransactionalReservationProcess(transactionalReservationProcess);
				aaMealsBL.setPnr(pnr);
				List<FlightSegmentMeals> flightSegmentMeals = aaMealsBL.execute(mealRequest, bookingFlightSegments,
						selectedLanguage, flightSegIdWiseSelectedMeals, aaAncillaryRQ.getAaPos());
				aaAncillaryRS.getFlightSegmentMeals().addAll(flightSegmentMeals);
			}

			if (baggageRequest != null) {
				baggageRequest.getReservationBaggageParams()
						.setSalesChannel(Integer.valueOf(AAUtils.getSalesChannelFromPOS(aaAncillaryRQ.getAaPos())));

				AABaggagesBL aaBaggagesBL = new AABaggagesBL();
				aaBaggagesBL.setTransactionalReservationProcess(transactionalReservationProcess);
				aaBaggagesBL.setPnr(pnr);
				selectedLanguage = baggageRequest.getSelectedLanguage();
				List<FlightSegmentBaggages> flightSegmentBaggages = aaBaggagesBL.execute(baggageRequest, bookingFlightSegments,
						selectedLanguage, appEngine, bookingSpecialRequests.isRequote(), ondWiseBundlePeriodIDs);
				aaAncillaryRS.getFlightSegmentBaggages().addAll(flightSegmentBaggages);
			}

			if (specialServiceRequest != null) {

				selectedLanguage = bookingSpecialRequests.getSelectedLanguage();

				List<BookingFlightSegment> flightSegments = getListOfRequestedFlightSegments(bookingFlightSegments,
						specialServiceRequest.getFlightRefNumberRPHList());

				String salesChannel = AAUtils.getSalesChannelFromPOS(aaAncillaryRQ.getAaPos());

				AASpecialServiceRequestBL aaSpecialServiceRequestBL = new AASpecialServiceRequestBL();
				aaSpecialServiceRequestBL.setFlightSegIdWiseSelectedSSRs(flightSegIdWiseSelectedSSRs);
				aaSpecialServiceRequestBL.setHasPrivModifySSRWithinBuffer(AuthorizationUtil.hasPrivilege(privilegeKeys,
						PriviledgeConstants.PRIVI_MODIFY_SSR_WITHIN_BUFFER));

				List<FlightSegmentSpecialServiceRequests> flightSegmentSpecialServiceRequests = aaSpecialServiceRequestBL
						.execute(flightSegments, salesChannel, selectedLanguage, bookingSpecialRequests.isModifyAnci());
				aaAncillaryRS.getFlightSegmentSpecialServiceRequests().addAll(flightSegmentSpecialServiceRequests);
			}

			if (insuranceRequest != null) {
				List<InsuranceQuotation> insuranceQuotes = new ArrayList<InsuranceQuotation>();
				String marketingAirLineCode = aaAncillaryRQ.getAaPos().getMarketingAirLineCode();

				AAInsuranceBL aaInsuranceBL = new AAInsuranceBL();
				aaInsuranceBL.setBookingFlightSegments(bookingFlightSegments);
				aaInsuranceBL.setInsuranceRequest(insuranceRequest);

				insuranceQuotes = aaInsuranceBL.getInsuranceQuotes(marketingAirLineCode, appEngine);
				aaAncillaryRS.getInsuranceQuotations().addAll(insuranceQuotes);
			}

			if (airportServiceRequest != null) {
				AAAirportServiceRequestBL aaAPServiceBL = new AAAirportServiceRequestBL();
				aaAPServiceBL.setAppIndicator(airportServiceRequest.getAppIndicator());
				aaAPServiceBL.setFlightSegments(bookingFlightSegments);
				aaAPServiceBL.setServiceCategory(airportServiceRequest.getServiceCategory());
				aaAPServiceBL.setServiceCount(airportServiceRequest.getServiceCount());
				aaAPServiceBL.setSelectedLanguage(bookingSpecialRequests.getSelectedLanguage());
				aaAPServiceBL.setTransactionalReservationProcess(transactionalReservationProcess);
				aaAPServiceBL.setPnr(pnr);

				List<AirportServiceRequestMap> fltSegAPServiceRQList = aaAPServiceBL.execute();
				aaAncillaryRS.getFlightSegmentAirportServiceRequests().addAll(fltSegAPServiceRQList);
			}

		} catch (ModuleException e) {
			log.error("Error occured in getAncillaryDetail", e);
			aaAncillaryRS.getResponseAttributes().setSuccess(null);
			AAExceptionUtil.addAAErrrors(aaAncillaryRS.getResponseAttributes().getErrors(), e);
		} catch (Exception e) {
			log.error("Error occured in getAncillaryDetail", e);
			aaAncillaryRS.getResponseAttributes().setSuccess(null);
			AAExceptionUtil.addAAErrrors(aaAncillaryRS.getResponseAttributes().getErrors(), e);
		}
		return aaAncillaryRS;
	}

	private List<StringIntegerMap> getCarrierBundlePeriodIDs(List<StringStringIntegerMap> ondWiseBundlePeriodIDMap) {
		for (StringStringIntegerMap carrierBundlePeriodIDs : ondWiseBundlePeriodIDMap) {
			if (AppSysParamsUtil.getDefaultCarrierCode().equals(carrierBundlePeriodIDs.getKey())) {
				return carrierBundlePeriodIDs.getValue();
			}
		}
		return null;
	}

	public AASelectedAncillaryRS getSelectedAncillaryDetail(AASelectedAncillaryRQ aaSelectedAncillaryRQ,
			AASessionManager aaSessionManager) {
		AASelectedAncillaryRS selectedAncillaryRS = new AASelectedAncillaryRS();
		selectedAncillaryRS.setResponseAttributes(new AAResponseAttributes());
		selectedAncillaryRS.getResponseAttributes().setSuccess(new AASuccess());

		List<SelectedSeat> selectedSeats = aaSelectedAncillaryRQ.getSelectedSeats();
		List<SelectedMeal> selectedMeals = aaSelectedAncillaryRQ.getSelectedMeals();
		List<SelectedBaggage> selectedBaggages = aaSelectedAncillaryRQ.getSelectedBaggages();
		List<SelectedInsurance> selectedInsurances = aaSelectedAncillaryRQ.getSelectedInsurances();
		List<SelectedFlexi> selectedFlexis = aaSelectedAncillaryRQ.getSelectedFlexis();
		List<SelectedSSR> selectedAPS = aaSelectedAncillaryRQ.getSelectedAirportServices();
		List<SelectedSSR> selectedInflightServices = aaSelectedAncillaryRQ.getSelectedInflightServices();
		List<StringStringListMap> flightRefWiseSelectedMeals = aaSelectedAncillaryRQ.getFlightRefWiseSelectedMeals();
		boolean isModifyAnci = aaSelectedAncillaryRQ.isModifyAnci();
		// This is used when, system automaticcaly cancel ancillaries by
		// scheduler job (Auto Cancellation),
		// This should true only in scheduler job cancelling ancis, all other
		// scenarios this should be false
		boolean skipeCutoverValidation = aaSelectedAncillaryRQ.isSkipCutoverCheck();

		try {

			if (selectedInsurances != null && !selectedInsurances.isEmpty()) {
				for (SelectedInsurance selectedInsurance : selectedInsurances) {
					if (selectedInsurance != null && selectedInsurance.getInsuranceReferences() != null
							&& !selectedInsurance.getInsuranceReferences().isEmpty()) {
						AAInsuranceBL aaInsuranceBL = new AAInsuranceBL();

						List<String> insuranceReferences = selectedInsurance.getInsuranceReferences();

						for (String insuranceRef : insuranceReferences) {
							InsuranceQuotation insuranceQuotation = aaInsuranceBL.getSavedInsuranceQuote(insuranceRef);
							selectedAncillaryRS.getInsuranceQuotations().add(insuranceQuotation);
						}
					}
				}
			}

			if (selectedSeats != null && selectedSeats.size() > 0) {
				AASeatMapBL aaSeatMapBL = new AASeatMapBL();
				aaSeatMapBL.setTransactionalReservationProcess(transactionalReservationProcess);
				aaSeatMapBL.setPnr(aaSelectedAncillaryRQ.getPnr());
				List<FlightSegmentSeats> seats = aaSeatMapBL.getSelectedSeatDetails(selectedSeats);
				selectedAncillaryRS.getSeats().addAll(seats);
			}

			if (selectedMeals != null && selectedMeals.size() > 0) {
				AAMealsBL aaMealsBL = new AAMealsBL();
				aaMealsBL.setTransactionalReservationProcess(transactionalReservationProcess);
				aaMealsBL.setPnr(aaSelectedAncillaryRQ.getPnr());
				Map<Integer, Set<String>> flightSegIdWiseSelectedMeals = AncillaryUtil
						.getFlightSegIdWiseSelectedAnci(flightRefWiseSelectedMeals);
				List<FlightSegmentMeals> meals = aaMealsBL.getSelectedMealDetails(selectedMeals,
						flightSegIdWiseSelectedMeals, skipeCutoverValidation, aaSelectedAncillaryRQ.getAaPos());
				selectedAncillaryRS.getMeals().addAll(meals);
			}

			if (selectedBaggages != null && selectedBaggages.size() > 0) {
				AABaggagesBL aaBaggagesBL = new AABaggagesBL();
				aaBaggagesBL.setTransactionalReservationProcess(transactionalReservationProcess);
				aaBaggagesBL.setPnr(aaSelectedAncillaryRQ.getPnr());
				List<FlightSegmentBaggages> baggages = aaBaggagesBL.getSelectedBaggageDetails(selectedBaggages,
						skipeCutoverValidation);
				selectedAncillaryRS.getBaggages().addAll(baggages);
			}

			if (selectedFlexis != null && selectedFlexis.size() > 0) {
				AAFlexiBL aaFlexiBL = new AAFlexiBL(aaSelectedAncillaryRQ, aaSessionManager);
				List<FlexiQuotation> flexis = aaFlexiBL.getSelectedFlexiDetails(selectedFlexis);
				selectedAncillaryRS.getFlexiQuotation().addAll(flexis);
			}

			if (selectedAPS != null && selectedAPS.size() > 0) {
				AAAirportServiceRequestBL aaAirportServiceBL = new AAAirportServiceRequestBL();
				aaAirportServiceBL.setTransactionalReservationProcess(transactionalReservationProcess);
				aaAirportServiceBL.setPnr(aaSelectedAncillaryRQ.getPnr());
				List<FlightSegmentAirportServiceRequests> airportServices = aaAirportServiceBL
						.getSelectedAirportServices(selectedAPS, skipeCutoverValidation);
				selectedAncillaryRS.getAirportServices().addAll(airportServices);
			}

			if (selectedInflightServices != null && selectedInflightServices.size() > 0) {
				AASpecialServiceRequestBL aaSSRBL = new AASpecialServiceRequestBL();
				aaSSRBL.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
				aaSSRBL.setSalesChannel(AAUtils.getSalesChannelFromPOS(aaSelectedAncillaryRQ.getAaPos()));
				aaSSRBL.setModifyAnci(isModifyAnci);
				List<FlightSegmentSpecialServiceRequests> ssrList = aaSSRBL
						.getSelectedInflightServices(selectedInflightServices, skipeCutoverValidation);
				selectedAncillaryRS.getInflightServices().addAll(ssrList);
			}
		} catch (ModuleException e) {
			log.error("Error occured in getSelectedAncillaryDetail", e);
			selectedAncillaryRS.getResponseAttributes().setSuccess(null);
			AAExceptionUtil.addAAErrrors(selectedAncillaryRS.getResponseAttributes().getErrors(), e);
		} catch (Exception e) {
			log.error("Error occured in getSelectedAncillaryDetail", e);
			selectedAncillaryRS.getResponseAttributes().setSuccess(null);
			AAExceptionUtil.addAAErrrors(selectedAncillaryRS.getResponseAttributes().getErrors(), e);
		}

		return selectedAncillaryRS;
	}

	private static List<BookingFlightSegment> getListOfRequestedFlightSegments(
			List<BookingFlightSegment> bookingFlightSegments, FlightRefNumberRPHList flightRefNumberRPHList) {

		List<BookingFlightSegment> flightSegments = new ArrayList<BookingFlightSegment>();

		List<String> flightRefNums = flightRefNumberRPHList.getFlightRefNumber();
		for (BookingFlightSegment bookingFlightSegment : bookingFlightSegments) {
			if (flightRefNums.contains(bookingFlightSegment.getFlightRefNumber())) {
				flightSegments.add(bookingFlightSegment);
			}
		}

		return flightSegments;
	}

	@Override
	public void setTransactionalReservationProcess(TransactionalReservationProcess transactionalReservationProcess) {
		this.transactionalReservationProcess = transactionalReservationProcess;
	}

	
}
