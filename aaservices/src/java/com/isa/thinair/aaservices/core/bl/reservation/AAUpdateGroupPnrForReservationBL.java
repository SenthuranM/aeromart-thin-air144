package com.isa.thinair.aaservices.core.bl.reservation;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAUpdateGroupPnrRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAUpdateGroupPnrRS;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;

/**
 * Updates the originator PNR field for a given reservation
 * 
 * @author sanjaya
 * 
 */
public class AAUpdateGroupPnrForReservationBL {

	private static Log log = LogFactory.getLog(AAUpdateGroupPnrForReservationBL.class);

	/** The request */
	private AAUpdateGroupPnrRQ aaUpdateGroupPnrRQ;

	/** The response */
	private AAUpdateGroupPnrRS aAUpdateGroupPnrRS;

	// Initializes the response variable.
	private void initializeAAGenerateNewPNRRS() {
		aAUpdateGroupPnrRS = new AAUpdateGroupPnrRS();
		aAUpdateGroupPnrRS.setResponseAttributes(new AAResponseAttributes());
	}

	/**
	 * The execution logic
	 * 
	 * @return The response.
	 */
	public AAUpdateGroupPnrRS execute() {
		initializeAAGenerateNewPNRRS();

		try {

			long newVersion = AAServicesModuleUtils.getReservationBD().updateOriginatorPNRForReservation(
					aaUpdateGroupPnrRQ.getPnr());
			aAUpdateGroupPnrRS.setNewReservationVersion(newVersion);
			return this.aAUpdateGroupPnrRS;
		} catch (Exception ex) {
			log.error(ex);
			AAExceptionUtil.addAAErrrors(aAUpdateGroupPnrRS.getResponseAttributes().getErrors(), ex);
		} finally {
			int numOfErrors = aAUpdateGroupPnrRS.getResponseAttributes().getErrors().size();
			AASuccess successType = (numOfErrors == 0 ? new AASuccess() : null);
			aAUpdateGroupPnrRS.getResponseAttributes().setSuccess(successType);
		}

		return aAUpdateGroupPnrRS;
	}

	/**
	 * Sets the request object.
	 * 
	 * @param request
	 */
	public void setAAUpdateGroupPnrRQ(AAUpdateGroupPnrRQ request) {
		this.aaUpdateGroupPnrRQ = request;
	}
}
