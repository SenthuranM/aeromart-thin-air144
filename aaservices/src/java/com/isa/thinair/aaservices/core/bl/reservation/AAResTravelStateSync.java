package com.isa.thinair.aaservices.core.bl.reservation;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.TicketingInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TicketingStatus;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAErrorCode;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResTravelStateSyncRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAReservation;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;

/**
 * @author Nilindra Fernando
 * @since April 18, 2012
 */
public class AAResTravelStateSync {

	private static Log log = LogFactory.getLog(AAResTravelStateSync.class);

	private AAReservation aaAirReservation;

	public AAResTravelStateSync(AAReservation aaAirReservation) {
		this.aaAirReservation = aaAirReservation;
	}

	public AAResTravelStateSyncRS execute() {

		AAResTravelStateSyncRS aaResTravelStateSyncRS = new AAResTravelStateSyncRS();
		aaResTravelStateSyncRS.setResponseAttributes(new AAResponseAttributes());

		try {

			if (log.isDebugEnabled()) {
				log.debug("AAResTravelStateSync::begin for pnr [" + aaAirReservation.getGroupPnr() + "]");
			}

			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setGroupPNR(aaAirReservation.getGroupPnr());
			Reservation reservation = AAServicesModuleUtils.getReservationBD().getReservation(pnrModesDTO, null);

			if (reservation == null) {
				throw new WebservicesException(AAErrorCode.ERR_25_MAXICO_REQUIRED_THE_RESERVATION_NO_LONGER_EXIST,
						"Reservation does not exits");
			}

			String reservationStatus = null;
			Date onholdReleaseTimeInZulu = null;
			TicketingInfo ticketingInfo = aaAirReservation.getAirReservation().getTicketing();
			TicketingStatus ticketingStatus = ticketingInfo.getTicketingStatus();
			if (ticketingStatus == TicketingStatus.TICKETTYPE_TICKETED
					|| ticketingStatus == TicketingStatus.TICKETTYPE_PARTIAL_TICKETED) {
				reservationStatus = ReservationInternalConstants.ReservationStatus.CONFIRMED;
			} else if (ticketingStatus == TicketingStatus.TICKETTYPE_CANCELLED) {
				reservationStatus = ReservationInternalConstants.ReservationStatus.CANCEL;
			} else if (ticketingStatus == TicketingStatus.TICKETTYPE_ONHOLD_TICKETED) {
				reservationStatus = ReservationInternalConstants.ReservationStatus.ON_HOLD;
				onholdReleaseTimeInZulu = ticketingInfo.getTicketTimeLimit();
			} else {
				throw new WebservicesException(AAErrorCode.ERR_1_MAXICO_REQUIRED_INVALID_VALUE, "Invalid reservation status"
						+ ticketingStatus.toString());
			}

			if (ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(reservationStatus)) {
				if (onholdReleaseTimeInZulu == null) {
					throw new WebservicesException(AAErrorCode.ERR_1_MAXICO_REQUIRED_INVALID_VALUE, "Release time is empty"
							+ ticketingStatus.toString());
				}

				for (ReservationPax reservationPax : reservation.getPassengers()) {
					reservationPax.setStatus(ReservationInternalConstants.ReservationPaxStatus.ON_HOLD);
					reservationPax.setZuluReleaseTimeStamp(onholdReleaseTimeInZulu);
				}
			}

			AAServicesModuleUtils.getReservationBD().updateReservatonStatus(reservation, reservationStatus);

			if (log.isDebugEnabled()) {
				log.debug("AAResTravelStateSync::end");
			}

		} catch (Exception e) {
			log.error("fulfill payment fail", e);
			AAExceptionUtil.addAAErrrors(aaResTravelStateSyncRS.getResponseAttributes().getErrors(), e);
		} finally {
			int numOfErrors = aaResTravelStateSyncRS.getResponseAttributes().getErrors().size();
			AASuccess successType = (numOfErrors == 0 ? new AASuccess() : null);
			aaResTravelStateSyncRS.getResponseAttributes().setSuccess(successType);
		}

		return aaResTravelStateSyncRS;
	}

}
