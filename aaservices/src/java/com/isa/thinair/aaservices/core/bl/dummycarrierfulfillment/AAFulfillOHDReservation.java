package com.isa.thinair.aaservices.core.bl.dummycarrierfulfillment;

import java.util.Collection;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AirReservation;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OriginDestinationOption;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAirBookRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAErrorCode;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAPOS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAPayCarrierFulfullmentRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAReservation;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.aaservices.core.util.AAExtCarrierFulfillmentUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.AAServicesTransformUtil;
import com.isa.thinair.aaservices.core.util.AAUtils;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.ReservationAdminInfoTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.IReservation;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.assembler.ReservationAssembler;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Handles the dummy reservation creation for a dry/interline ONHOLD Reservation.
 * 
 * @author malaka
 * 
 */
class AAFulfillOHDReservation extends BaseMarketingCarrierFulfillment {

	private static Log log = LogFactory.getLog(AAFulfillOHDReservation.class);

	AAFulfillOHDReservation(AAReservation aaAirReservation, AAReservation newAAAirReservation, AAPOS aaPos) {
		super(aaAirReservation, newAAAirReservation, aaPos);
	}

	@Override
	public AAPayCarrierFulfullmentRS execute() throws WebservicesException, ModuleException {

		AAPayCarrierFulfullmentRS carrierFulfillment = new AAPayCarrierFulfullmentRS();
		carrierFulfillment.setResponseAttributes(new AAResponseAttributes());

		checkConstraints();

		try {

			if (log.isDebugEnabled()) {
				log.debug("AAFulfillOHDReservation::begin for pnr [" + aaAirReservation.getGroupPnr() + "]");
			}

			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setGroupPNR(aaAirReservation.getGroupPnr());
			pnrModesDTO.setLoadExternalPaxPayments(true);
			Reservation reservation = getCarrierReservation(pnrModesDTO);

			List<OriginDestinationOption> originDestinationOptions = aaAirReservation.getAirReservation().getAirItinerary()
					.getOriginDestinationOption();

			if (reservation == null) {

				AAAirBookRQ buildDummyRes = AAExtCarrierFulfillmentUtil.buildDummyRes(aaAirReservation);
				Object[] transformedBookRQInfo = new AAServicesTransformUtil.BookingRequest(buildDummyRes, null).transform();
				IReservation iReservation = (IReservation) transformedBookRQInfo[0];
				iReservation.addExternalPnrSegments(AAExtCarrierFulfillmentUtil.syncExtSegments(null, originDestinationOptions));

				iReservation.setOriginCountryOfCall(aaAirReservation.getAirReservation().getOriginCountryOfCall());
				ReservationAssembler rAssembler = (ReservationAssembler) iReservation;
				reservation = rAssembler.getDummyReservation();
				reservation.setDummyBooking(ReservationInternalConstants.DummyBooking.YES);
				reservation.setInfantPaymentSeparated(ReservationInternalConstants.InfantPaymentRecordedWithInfant.YES);

				if (aaAirReservation.getAirReservation().getStatus() != null
						&& !aaAirReservation.getAirReservation().getStatus().isEmpty()
						&& aaAirReservation.getAirReservation().getStatus()
								.equals(ReservationInternalConstants.ReservationStatus.CONFIRMED)) {
					reservation.setStatus(ReservationInternalConstants.ReservationStatus.CONFIRMED);
					for (ReservationPax reservationPax : (Collection<ReservationPax>) reservation.getPassengers()) {
						reservationPax.setStatus(ReservationInternalConstants.ReservationPaxStatus.CONFIRMED);
					}
				} else {
					// set reservation as OND
					reservation.setStatus(ReservationInternalConstants.ReservationStatus.ON_HOLD);
					for (ReservationPax reservationPax : (Collection<ReservationPax>) reservation.getPassengers()) {
						reservationPax.setStatus(ReservationInternalConstants.ReservationPaxStatus.ON_HOLD);
					}
				}

			} else {
				// updating the reservation and pax status form the opCarrier
				AAExtCarrierFulfillmentUtil.updateReservationsStatus(reservation, aaAirReservation);
			}

			ReservationAdminInfoTO adminInfoTO = AAExtCarrierFulfillmentUtil.buildReservationAdminInfoTO(aaAirReservation);
			TrackInfoDTO trackInfo = AAUtils.getTrackInfo(aaPos);

			ReservationBD reservationDelegate = AAServicesModuleUtils.getReservationBD();
			ServiceResponce responce = reservationDelegate.createPayCarrierfulfillReservation(reservation, trackInfo,
					adminInfoTO, null,null);
			Long version = (Long) responce.getResponseParam(CommandParamNames.VERSION);

			carrierFulfillment.getResponseAttributes().setSuccess(new AASuccess());
			carrierFulfillment.setResVersion(version.toString());

			if (log.isDebugEnabled()) {
				log.debug("AAFulfillOHDReservation::end");
			}

		} catch (ModuleException e) {
			log.error("dummy res AAFulfillONDReservation creation failed", e);
			throw new ModuleException("aaservices.paycarrier.fulfillment.failed", e);
		}

		return carrierFulfillment;
	}

	@Override
	public void checkConstraints() throws WebservicesException {
		AirReservation airReservation = aaAirReservation.getAirReservation();

		if ((airReservation.getAirItinerary() == null || airReservation.getAirItinerary().getOriginDestinationOption() == null || airReservation
				.getAirItinerary().getOriginDestinationOption().isEmpty())
				|| airReservation.getTravelerInfo() == null) {

			throw new WebservicesException(AAErrorCode.ERR_1_MAXICO_REQUIRED_INVALID_VALUE, "Fares could not be located");
		}

	}
}
