/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2009 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * ===============================================================================
 */
package com.isa.thinair.aaservices.core.handler.security;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashSet;
import java.util.Set;

import javax.security.auth.login.LoginException;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.required.dto.AAErrorCode;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.aaservices.core.session.AASessionManager;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.login.util.ForceLoginInvoker;
import com.sun.xml.wss.ProcessingContext;
import com.sun.xml.wss.SubjectAccessor;
import com.sun.xml.wss.XWSSProcessor;
import com.sun.xml.wss.XWSSProcessorFactory;
import com.sun.xml.wss.XWSSecurityException;

/**
 * @author Nilindra Fernando
 */
public class AAServerSecurityHandler implements SOAPHandler<SOAPMessageContext> {

	private static final Log log = LogFactory.getLog(AAServerSecurityHandler.class);

	private static final String DUMP_MESSAGES = AAServicesModuleUtils.getWebServicesConfig().getDumpMessages();

	private XWSSProcessor sprocessor = null;

	private static final String namespaceURI = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
	private static final String localPart = "Security";
	private static final String prefix = "wsse";
	private static final QName securityElementQname = new QName(namespaceURI, localPart, prefix);

	private static final String aaServicesURI = "http://www.isa.com/connectivity/profiles/maxico/v1/required/dto";
	private static final QName qAaPos = new QName(aaServicesURI, "aaPos");
	private static final QName qUname = new QName(aaServicesURI, "marketingUserId");
	private static final QName qPassword = new QName(aaServicesURI, "marketingUserPwd");
	private static final QName qUserSessionId = new QName(aaServicesURI, "userSessionId");

	public AAServerSecurityHandler() {
		InputStream in = null;

		try {
			XWSSProcessorFactory factory = XWSSProcessorFactory.newInstance();
			in = getStream();
			sprocessor = factory.createProcessorForSecurityConfiguration(getStream(), new AAServerSecurityEnvHandler());
		} catch (Exception e) {
			log.error(e);
			throw new RuntimeException(e);
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					log.error(e);
				}
			}
		}
	}

	public Set<QName> getHeaders() {

		QName securityHeader = new QName(namespaceURI, localPart, prefix);
		HashSet<QName> headers = new HashSet<QName>();
		headers.add(securityHeader);
		return headers;
	}

	public boolean handleFault(SOAPMessageContext messageContext) {
		return true;
	}

	public boolean handleMessage(SOAPMessageContext messageContext) {
		secureServer(messageContext);
		return true;
	}

	public void close(MessageContext messageContext) {
		try {
			AASessionManager.SessionOutHandler.handleSessionOut();
		} catch (Throwable ex) {
			log.error("Sessioin out handler failed", ex);
		}
	}

	private void secureServer(SOAPMessageContext messageContext) {
		Boolean outMessageIndicator = (Boolean) messageContext.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);

		SOAPMessage message = messageContext.getMessage();

		if (outMessageIndicator.booleanValue()) {
			// do nothing....
			return;
		} else {
			if (log.isDebugEnabled()) {
				log.debug("\nServer Inbound SOAP : ");
			}

			// verify the secured message.
			try {

				boolean securityHeadersIncluded = message.getSOAPPart().getEnvelope().getHeader()
						.getChildElements(securityElementQname).hasNext();

				if (securityHeadersIncluded) {
					if (log.isDebugEnabled()) {
						log.debug("security headers found in the request. allow the normal authentication to happen through callback");
					}
					ProcessingContext context = sprocessor.createProcessingContext(message);
					context.setSOAPMessage(message);
					SOAPMessage verifiedMsg = sprocessor.verifyInboundMessage(context);
					if (log.isDebugEnabled()) {
						log.debug("\nRequester Subject : " + SubjectAccessor.getRequesterSubject(context));
					}
					messageContext.setMessage(verifiedMsg);
				} else {
					if (log.isDebugEnabled()) {
						log.debug("security headers not found in the request. login the user form aaPos detail");
					}

					loginFromAAPOSDetail(message);
				}

			} catch (XWSSecurityException ex) {
				log.error(ex);
				throw new WebServiceException(ex);
			} catch (Exception ex) {
				log.error(ex);
				throw new WebServiceException(ex);
			}
		}
	}

	/**
	 * If no security headers found in the soup headers we try to authenticate using aaPos inside message body
	 * 
	 * @param message
	 * @throws SOAPException
	 * @throws LoginException
	 * @throws WebservicesException
	 */
	private void loginFromAAPOSDetail(SOAPMessage message) throws SOAPException, LoginException, WebservicesException {

		SOAPBodyElement methodElement = (SOAPBodyElement) message.getSOAPPart().getEnvelope().getBody().getChildElements().next();
		SOAPBodyElement argumentElement = (SOAPBodyElement) methodElement.getChildElements().next();

		if (!argumentElement.getChildElements(qAaPos).hasNext()) {
			log.error("No pos found for [" + methodElement.toString() + "]");
			throw new WebservicesException(AAErrorCode.ERR_58_MAXICO_REQUIRED_AA_POS_NOT_FOUND_OR_INVALID,
					"No aaPos found in the request. Unable to login to the system");
		}
		SOAPBodyElement aaPos = (SOAPBodyElement) argumentElement.getChildElements(qAaPos).next();

		if (!aaPos.getChildElements(qUname).hasNext()) {
			log.error("Username not found under pos for [" + methodElement.toString() + "]");
			throw new WebservicesException(AAErrorCode.ERR_58_MAXICO_REQUIRED_AA_POS_NOT_FOUND_OR_INVALID,
					"Username not found under pos. Unable to login to the system");

		}
		SOAPBodyElement unameElement = (SOAPBodyElement) aaPos.getChildElements(qUname).next();
		String userName = unameElement.getFirstChild().getNodeValue();

		if (!aaPos.getChildElements(qPassword).hasNext()) {
			log.error("Password not found under pos for [" + methodElement.toString() + "]");
			throw new WebservicesException(AAErrorCode.ERR_58_MAXICO_REQUIRED_AA_POS_NOT_FOUND_OR_INVALID,
					"Password not found under pos. Unable to login to the system");
		}
		SOAPBodyElement passwordElement = (SOAPBodyElement) aaPos.getChildElements(qPassword).next();
		String password = passwordElement.getFirstChild().getNodeValue();

		String sessionId = null;
		if (aaPos.getChildElements(qUserSessionId).hasNext()) {
			SOAPBodyElement sessionIdElement = (SOAPBodyElement) aaPos.getChildElements(qUserSessionId).next();
			sessionId = sessionIdElement.getFirstChild().getNodeValue();
		}

		int salesChannelId = SalesChannelsUtil.SALES_CHANNEL_LCC;

		if (log.isDebugEnabled()) {
			log.debug("Loging to the aaServices as [" + userName + "] password size [" + password.length() + "] salesChannelId ["
					+ salesChannelId + "]");
		}
		
		// TO DO remove hard coded method checking
		boolean skipAuthentication = (methodElement.getLocalName().equals("invalidateAASessionsForUser")) ? true : false;

		ForceLoginInvoker.webserviceLogin(userName, password, salesChannelId);
		AASessionManager.SessionInHandler.handleSessionIn(userName, password, salesChannelId, sessionId, skipAuthentication);
	}

	private InputStream getStream() {
		String text = "<xwss:SecurityConfiguration xmlns:xwss=\"http://java.sun.com/xml/ns/xwss/config\" dumpMessages=\""
				+ DUMP_MESSAGES + "\"> " + "<xwss:RequireUsernameToken passwordDigestRequired=\"false\"/> "
				+ "</xwss:SecurityConfiguration>";
		InputStream is = null;

		try {
			is = new ByteArrayInputStream(text.getBytes("UTF-8"));
		} catch (UnsupportedEncodingException e) {
			log.error(e);
		}

		return is;
	}
}