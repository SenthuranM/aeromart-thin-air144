package com.isa.thinair.aaservices.core.bl.masterdata;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAgreementMasterDataRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAgreementMasterDataRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAError;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.TransformerUtil;

public class AAAgreementMasterDataBL {

	private static final Log log = LogFactory.getLog(AAAgreementMasterDataBL.class);

	private AAAgreementMasterDataRQ aaAgreementMasterDataRQ;

	public AAAgreementMasterDataRS execute() {
		AAAgreementMasterDataRS aaAgreementMasterDataRS = new AAAgreementMasterDataRS();

		try {
			if (aaAgreementMasterDataRQ.isLoadFlights()) {
				aaAgreementMasterDataRS.getFlightNumbers().addAll(TransformerUtil.convertStringStringMapToLCCList(
						AAServicesModuleUtils.getCommonMasterBD().getFlightNumbers()));
			}

			if (aaAgreementMasterDataRQ.isLoadBookingClasses()) {
				aaAgreementMasterDataRS.getBookingClasses().addAll(TransformerUtil.convertStringStringMapToLCCList(
						AAServicesModuleUtils.getCommonMasterBD().getBookingClasses()));
			}

			if (aaAgreementMasterDataRQ.isLoadAgents()) {
				aaAgreementMasterDataRS.getAgents().addAll(TransformerUtil.convertStringStringMapToLCCList(
						AAServicesModuleUtils.getCommonMasterBD().getAgents()));
			}
		} catch (Exception e) {
			log.error("Error occurred while fetching master-data " , e );

			AAError aaError = new AAError();
			aaError.setDescription(e.getMessage());
			aaAgreementMasterDataRS.getResponseAttributes().getErrors().add(aaError);
			aaAgreementMasterDataRS.getResponseAttributes().setSuccess(null);
		}

		return aaAgreementMasterDataRS;
	}

	public AAAgreementMasterDataRQ getAaAgreementMasterDataRQ() {
		return aaAgreementMasterDataRQ;
	}

	public void setAaAgreementMasterDataRQ(AAAgreementMasterDataRQ aaAgreementMasterDataRQ) {
		this.aaAgreementMasterDataRQ = aaAgreementMasterDataRQ;
	}
}
