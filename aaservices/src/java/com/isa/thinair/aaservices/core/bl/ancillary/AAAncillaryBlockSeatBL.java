package com.isa.thinair.aaservices.core.bl.ancillary;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PassengerSelectedSeat;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PassengerType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SegmentSeats;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAncillaryBlockSeatRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAncillaryBlockSeatRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.airinventory.api.service.SeatMapBD;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.ServiceResponce;

public class AAAncillaryBlockSeatBL {

	private Log log = LogFactory.getLog(AAAncillaryBlockSeatBL.class);

	public AAAncillaryBlockSeatRS execute(AAAncillaryBlockSeatRQ aaBlockSeatRQ) {

		AAAncillaryBlockSeatRS blockSeatRS = new AAAncillaryBlockSeatRS();
		blockSeatRS.setResponseAttributes(new AAResponseAttributes());
		blockSeatRS.getResponseAttributes().setSuccess(new AASuccess());

		List<SegmentSeats> segmentSeats = aaBlockSeatRQ.getSegmentSeats();
		SeatMapBD seatMapBD = AAServicesModuleUtils.getSeatMapBD();

		try {
			for (SegmentSeats segmentSeat : segmentSeats) {
				FlightSegment flightSegment = segmentSeat.getFlightSegments();
				List<PassengerSelectedSeat> passengerSelectedSeats = segmentSeat.getPassengerSelectedSeat();
				Integer flightRefNumber = new Integer(flightSegment.getFlightRefNumber());

				Map<Integer, String> amSeatIdPaxTypeMap = new HashMap<Integer, String>();

				for (PassengerSelectedSeat passengerSelectedSeat : passengerSelectedSeats) {

					String seatCode = passengerSelectedSeat.getSeatNumbers();
					PassengerType passengerType = passengerSelectedSeat.getPassengerType();
					Integer flightAMSeatId = seatMapBD.getFlightAmSeatID(flightRefNumber, seatCode);

					if (passengerType.compareTo(PassengerType.ADT) == 0) {
						amSeatIdPaxTypeMap.put(flightAMSeatId, ReservationInternalConstants.PassengerType.ADULT);
					} else if (passengerType.compareTo(PassengerType.CHD) == 0) {
						amSeatIdPaxTypeMap.put(flightAMSeatId, ReservationInternalConstants.PassengerType.CHILD);
					} else if (passengerType.compareTo(PassengerType.PRT) == 0) {
						amSeatIdPaxTypeMap.put(flightAMSeatId, ReservationInternalConstants.PassengerType.PARENT);
					} else {
						throw new ModuleException("Invalid passenger type " + passengerType);
					}
				}

				ServiceResponce serviceResponce = seatMapBD.blockSeats(amSeatIdPaxTypeMap);
				if (!serviceResponce.isSuccess()) {
					blockSeatRS.getResponseAttributes().setSuccess(null);
				}
			}
		} catch (Exception e) {
			log.error("AA Seat blocking failed ", e);
		}
		return blockSeatRS;
	}
}
