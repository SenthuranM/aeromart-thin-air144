package com.isa.thinair.aaservices.core.bl.reservation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.isa.connectivity.profiles.maxico.v1.common.dto.CarrierWiseServiceTax;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ChargeAdjustment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ChargeType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaxWiseServiceTaxesMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ServiceTax;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAirBookModifyRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAErrorCode;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.aaservices.core.session.AASessionManager;
import com.isa.thinair.aaservices.core.session.AAUserSession;
import com.isa.thinair.aaservices.core.util.AAReservationUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.PaxTypeUtils;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxQuoteForTicketingRevenueResTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.utils.ChargeAdjustmentTypeUtil;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.commons.api.dto.ChargeAdjustmentTypeDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Nilindra Fernando
 */
class AAChargeAdjustmentBL {

	private AAAirBookModifyRQ aaAirBookModifyRQ;

	private Reservation reservation;

	public AAAirBookModifyRQ getAaAirBookModifyRQ() {
		return aaAirBookModifyRQ;
	}

	public void setAaAirBookModifyRQ(AAAirBookModifyRQ aaAirBookModifyRQ) {
		this.aaAirBookModifyRQ = aaAirBookModifyRQ;
	}

	public Reservation getReservation() {
		return reservation;
	}

	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}

	/**
	 * Execute some logic based on the Request object and process and return the Response object, Use
	 * executeGroupChargeAdjustment() method instead.
	 * 
	 * TODO : Remove after checking.
	 * 
	 * @return AAAirBookRS
	 * @throws WebservicesException
	 * @throws ModuleException
	 */
	@Deprecated
	void execute() throws WebservicesException, ModuleException {

		Collection privilegeKeys = (Collection) AASessionManager.getInstance().getCurrUserSessionParam(
				AAUserSession.USER_PRIVILEGES_KEYS);

		List<ChargeAdjustment> chargeAdjustments = aaAirBookModifyRQ.getAaReservation().getAirReservation()
				.getChargeAdjustments();

		for (ChargeAdjustment chargeAdjustment : chargeAdjustments) {
			List<String> elements = Arrays.asList(StringUtils.split(chargeAdjustment.getCarrierOndGroupRPH(), "|"));

			ChargeAdjustmentTypeDTO chargeAdjustmentType = ChargeAdjustmentTypeUtil.getChargeAdjustmentTypeDTO(chargeAdjustment
					.getChargeAdjustmentTypeId());

			int chargeRateId = -1;

			// derive the charge rate from the provided charge adjustment type id.
			if (chargeAdjustment.getChargeType().value().equals(ChargeType.REFUNDABLE.value())) {
				AAReservationUtil.authorize(privilegeKeys, chargeAdjustmentType.getRefundablePrivilegeId());
				chargeRateId = ChargeAdjustmentTypeUtil.getChargeRateIdForAdjustment(chargeAdjustmentType, true);
			} else if (chargeAdjustment.getChargeType().value().equals(ChargeType.NON_REFUNDABLE.value())) {
				AAReservationUtil.authorize(privilegeKeys, chargeAdjustmentType.getNonRefundablePrivilegeId());
				chargeRateId = ChargeAdjustmentTypeUtil.getChargeRateIdForAdjustment(chargeAdjustmentType, false);
			} else {
				throw new WebservicesException(AAErrorCode.ERR_49_MAXICO_REQUIRED_INVALID_CHARGE_FOUND_FOR_ADJUSTMENT,
						"Charge Type can not be empty");
			}

			// find the pnr fare id.
			for (ReservationPax reservationPax : (Set<ReservationPax>) reservation.getPassengers()) {
				Integer pnrPaxId = PaxTypeUtils.getPnrPaxId(chargeAdjustment.getTravelerRefNumber());

				if (reservationPax.getPnrPaxId().equals(pnrPaxId)) {

					if (!reservation.isInfantPaymentRecordedWithInfant() && ReservationApiUtils.isInfantType(reservationPax)) {
						throw new WebservicesException(AAErrorCode.ERR_49_MAXICO_REQUIRED_INVALID_CHARGE_FOUND_FOR_ADJUSTMENT,
								"Charge adjustments are not supported for infants");
					}

					for (ReservationPaxFare reservationPaxFare : (Set<ReservationPaxFare>) reservationPax.getPnrPaxFares()) {
						if (elements.contains(String.valueOf(reservationPaxFare.getPnrPaxFareId()))) {
							// validate charge adjustment amount
							ReservationApiUtils.isValidChargeAdjustment(reservationPaxFare, chargeAdjustment.getAmount(), null);
							AAServicesModuleUtils.getPassengerBD()
									.adjustCreditManual(
											reservation.getPnr(),
											reservationPaxFare.getPnrPaxFareId(),
											chargeRateId,
											chargeAdjustment.getAmount(),
											chargeAdjustment.getUserNote(),
											Integer.parseInt(aaAirBookModifyRQ.getAaReservation().getAirReservation()
													.getVersion()), null, null, null);
						}
					}
				}
			}
		}
	}

	/**
	 * Does group adjustments. Use this for single adjustments as well, unless there's a good reason to use the
	 * execute() method. It's kept there for reverse compatibility and reverting possibilities. Will be removed in a
	 * later merge
	 * 
	 * @throws WebservicesException
	 * @throws ModuleException
	 */
	void executeGroupChargeAdjustment() throws WebservicesException, ModuleException {

		Collection privilegeKeys = (Collection) AASessionManager.getInstance().getCurrUserSessionParam(
				AAUserSession.USER_PRIVILEGES_KEYS);

		List<ChargeAdjustment> chargeAdjustments = aaAirBookModifyRQ.getAaReservation().getAirReservation()
				.getChargeAdjustments();

		List<Integer> pnrPaxFareIDs = new ArrayList<Integer>();

		// Common values taken from the first element of the list.
		int chargeRateId = -1;
		BigDecimal amount = chargeAdjustments.get(0).getAmount();
		String userNote = chargeAdjustments.get(0).getUserNote();

		// Get the charge adjustment type object for the selected adjustment type id.
		ChargeAdjustmentTypeDTO chargeAdjustmentType = ChargeAdjustmentTypeUtil.getChargeAdjustmentTypeDTO(chargeAdjustments.get(
				0).getChargeAdjustmentTypeId());

		// Get the associated charge rate id from the charge adjustment type.
		if (chargeAdjustments.get(0).getChargeType().value().equals(ChargeType.REFUNDABLE.value())) {
			AAReservationUtil.authorize(privilegeKeys, chargeAdjustmentType.getRefundablePrivilegeId());
			chargeRateId = ChargeAdjustmentTypeUtil.getChargeRateIdForAdjustment(chargeAdjustmentType, true);
		} else if (chargeAdjustments.get(0).getChargeType().value().equals(ChargeType.NON_REFUNDABLE.value())) {
			AAReservationUtil.authorize(privilegeKeys, chargeAdjustmentType.getNonRefundablePrivilegeId());
			chargeRateId = ChargeAdjustmentTypeUtil.getChargeRateIdForAdjustment(chargeAdjustmentType, false);
		} else {
			throw new WebservicesException(AAErrorCode.ERR_49_MAXICO_REQUIRED_INVALID_CHARGE_FOUND_FOR_ADJUSTMENT,
					"Charge Type can not be empty");
		}

		for (ChargeAdjustment chargeAdjustment : chargeAdjustments) {
			List<String> elements = Arrays.asList(StringUtils.split(chargeAdjustment.getCarrierOndGroupRPH(), "|"));

			// find the pnr fare id.
			for (ReservationPax reservationPax : (Set<ReservationPax>) reservation.getPassengers()) {
				Integer pnrPaxId = PaxTypeUtils.getPnrPaxId(chargeAdjustment.getTravelerRefNumber());

				if (reservationPax.getPnrPaxId().equals(pnrPaxId)) {

					if (!reservation.isInfantPaymentRecordedWithInfant() && ReservationApiUtils.isInfantType(reservationPax)) {
						throw new WebservicesException(AAErrorCode.ERR_49_MAXICO_REQUIRED_INVALID_CHARGE_FOUND_FOR_ADJUSTMENT,
								"Charge adjustments are not supported for infants");
					}

					for (ReservationPaxFare reservationPaxFare : (Set<ReservationPaxFare>) reservationPax.getPnrPaxFares()) {
						if (elements.contains(String.valueOf(reservationPaxFare.getPnrPaxFareId()))) {
							// validate charge adjustment amount
							ReservationApiUtils.isValidChargeAdjustment(reservationPaxFare, amount, null);
							pnrPaxFareIDs.add(reservationPaxFare.getPnrPaxFareId());
						}
					}
				}
			}
		}
		
		Map<Integer, List<ExternalChgDTO>> paxCharges = AAModifyReservationQueryUtil
				.tranformPaxExternalChargeToExternalChgDTO(aaAirBookModifyRQ.getPerPaxExternalCharges());

		AAServicesModuleUtils.getPassengerBD().adjustCreditManual(reservation.getPnr(), pnrPaxFareIDs, chargeRateId, amount,
				userNote, Integer.parseInt(aaAirBookModifyRQ.getAaReservation().getAirReservation().getVersion()), null, null,
				this.getServiceTaxRS(aaAirBookModifyRQ.getCarrierWiseServiceTax()), paxCharges);
	}
	
	private ServiceTaxQuoteForTicketingRevenueResTO getServiceTaxRS(CarrierWiseServiceTax carrierWiseServiceTax){
		
		if(carrierWiseServiceTax == null){
			return null;
		}
		
		ServiceTaxQuoteForTicketingRevenueResTO serviceTaxRS = new ServiceTaxQuoteForTicketingRevenueResTO();
		Map<Integer, List<ServiceTaxTO>> paxWiseServiceTaxes = new HashMap<Integer, List<ServiceTaxTO>>();
		
		serviceTaxRS.setServiceTaxDepositeCountryCode(carrierWiseServiceTax.getServiceTaxDepositeCountryCode());
		serviceTaxRS.setServiceTaxDepositeStateCode(carrierWiseServiceTax.getServiceTaxDepositeStateCode());
		
		for(PaxWiseServiceTaxesMap paxWiseServiceTaxesMap : carrierWiseServiceTax.getPaxWiseServiceTaxes()){
			if(paxWiseServiceTaxes.get(paxWiseServiceTaxesMap.getKey()) == null){
				paxWiseServiceTaxes.put(paxWiseServiceTaxesMap.getKey(), transformServiceTax(paxWiseServiceTaxesMap.getValue()));
			}else{
				paxWiseServiceTaxes.get(paxWiseServiceTaxesMap.getKey()).addAll(transformServiceTax(paxWiseServiceTaxesMap.getValue()));
			}
		}
		
		serviceTaxRS.setPaxWiseServiceTaxes(paxWiseServiceTaxes);
		
		return serviceTaxRS;
	}
	
	private List<ServiceTaxTO> transformServiceTax(List<ServiceTax> serviceTaxes){
		
		List<ServiceTaxTO> serviceTaxTOs = new ArrayList<ServiceTaxTO>();
		
		for(ServiceTax serviceTax : serviceTaxes){
			
			ServiceTaxTO serviceTaxTO = new ServiceTaxTO();
			serviceTaxTO.setAmount(serviceTax.getAmount());
			serviceTaxTO.setCarrierCode(serviceTax.getCarrierCode());
			serviceTaxTO.setChargeCode(serviceTax.getChargeCode());
			serviceTaxTO.setChargeGroupCode(serviceTax.getChargeGroupCode());
			serviceTaxTO.setChargeRateId(serviceTax.getChargeRateId());
			serviceTaxTO.setFlightRefNumber(serviceTax.getFlightRefNumber());
			serviceTaxTO.setNonTaxableAmount(serviceTax.getNonTaxableAmount());
			serviceTaxTO.setTaxableAmount(serviceTax.getTaxableAmount());
			
			serviceTaxTOs.add(serviceTaxTO);
		}
		
		return serviceTaxTOs;
	}
	
}
