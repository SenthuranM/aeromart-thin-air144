package com.isa.thinair.aaservices.core.bl.reservation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AirTravelerEticket;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Eticket;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAExchangePassengerEticketRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AARetrievePassengerEticketRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AARetrievePassengerEticketRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAUpdatePassengerEticketRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAUpdatePassengerEticketRS;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.AAUtils;
import com.isa.thinair.aaservices.core.util.TransformerUtil;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.IFlightSegment;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.utils.SortUtil;
import com.isa.thinair.airreservation.api.dto.CreateTicketInfoDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.commons.api.dto.EticketDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.lccclient.api.util.ETicketUtil;
import com.isa.thinair.webplatform.api.util.ReservationUtil;

/**
 * @author eric
 * 
 */
public class AAPassengerEticketsBL {

	private static Log log = LogFactory.getLog(AAPassengerEticketsBL.class);

	public AAUpdatePassengerEticketRS updatePassengerTickets(AAUpdatePassengerEticketRQ aaUpdatePassengerEticketRQ) {
		AAUpdatePassengerEticketRS UpdatePassengerEticketRS = new AAUpdatePassengerEticketRS();
		UpdatePassengerEticketRS.setResponseAttributes(new AAResponseAttributes());
		try {
			AAServicesModuleUtils.getReservationBD().updatePassengerEtickets(aaUpdatePassengerEticketRQ.getPnr(),
					ReservationUtil.createTicketMap(aaUpdatePassengerEticketRQ.getAirTravelerTickets()),
					aaUpdatePassengerEticketRQ.isExchangePreviousEtickets());
			UpdatePassengerEticketRS.getResponseAttributes().setSuccess(new AASuccess());
			if (log.isDebugEnabled()) {
				log.debug("AAPassengerEticketsBL UpdatePassengerEticket(AAUpdatePassengerEticketRQ) succeded.");
			}
		} catch (ModuleException ex) {
			log.error("AAPassengerEticketsBL UpdatePassengerEticket(AAUpdatePassengerEticketRQ) failed.", ex);
			AAExceptionUtil.addAAErrrors(UpdatePassengerEticketRS.getResponseAttributes().getErrors(), ex);
		} catch (Exception e) {
			log.error("AAPassengerEticketsBL UpdatePassengerEticket(AAUpdatePassengerEticketRQ) failed.", e);
			AAExceptionUtil.addAAErrrors(UpdatePassengerEticketRS.getResponseAttributes().getErrors(), e);
		}
		return UpdatePassengerEticketRS;
	}

	public AAUpdatePassengerEticketRS ExchangePassengerTickets(AAExchangePassengerEticketRQ aaExchangePassengerEticketRQ) {
		AAUpdatePassengerEticketRS updatePassengerEticketRS = new AAUpdatePassengerEticketRS();
		updatePassengerEticketRS.setResponseAttributes(new AAResponseAttributes());
		try {
			AAServicesModuleUtils.getReservationBD().exchangePassengerEtickets(aaExchangePassengerEticketRQ.getPnr(),
					aaExchangePassengerEticketRQ.getOldPnrSegmentBookingRefs(),
					aaExchangePassengerEticketRQ.getNewPnrSegmentBookingRefs());
			updatePassengerEticketRS.getResponseAttributes().setSuccess(new AASuccess());
			if (log.isDebugEnabled()) {
				log.debug("AAPassengerEticketsBL exchangePassengerEtickets(AAExchangePassengerEticketRQ) succeded.");
			}
		} catch (ModuleException ex) {
			log.error("AAPassengerEticketsBL exchangePassengerEtickets(AAExchangePassengerEticketRQ) failed.", ex);
			AAExceptionUtil.addAAErrrors(updatePassengerEticketRS.getResponseAttributes().getErrors(), ex);
		} catch (Exception e) {
			log.error("AAPassengerEticketsBL exchangePassengerEtickets(AAExchangePassengerEticketRQ) failed.", e);
			AAExceptionUtil.addAAErrrors(updatePassengerEticketRS.getResponseAttributes().getErrors(), e);
		}
		return updatePassengerEticketRS;
	}
	
	public AAUpdatePassengerEticketRS
			ExchangeTicketsForGivenPassengers(AAExchangePassengerEticketRQ aaExchangePassengerEticketRQ) {
		AAUpdatePassengerEticketRS updatePassengerEticketRS = new AAUpdatePassengerEticketRS();
		updatePassengerEticketRS.setResponseAttributes(new AAResponseAttributes());
		try {
			AAServicesModuleUtils.getReservationBD().exchangeEticketsForGivenPassengers(aaExchangePassengerEticketRQ.getPnr(),
					aaExchangePassengerEticketRQ.getOldPnrSegmentBookingRefs(),
					aaExchangePassengerEticketRQ.getNewPnrSegmentBookingRefs(), aaExchangePassengerEticketRQ.getPaxSequences());
			updatePassengerEticketRS.getResponseAttributes().setSuccess(new AASuccess());
			if (log.isDebugEnabled()) {
				log.debug("AAPassengerEticketsBL exchangePassengerEtickets(AAExchangePassengerEticketRQ) succeded.");
			}
		} catch (ModuleException ex) {
			log.error("AAPassengerEticketsBL exchangePassengerEtickets(AAExchangePassengerEticketRQ) failed.", ex);
			AAExceptionUtil.addAAErrrors(updatePassengerEticketRS.getResponseAttributes().getErrors(), ex);
		} catch (Exception e) {
			log.error("AAPassengerEticketsBL exchangePassengerEtickets(AAExchangePassengerEticketRQ) failed.", e);
			AAExceptionUtil.addAAErrrors(updatePassengerEticketRS.getResponseAttributes().getErrors(), e);
		}
		return updatePassengerEticketRS;
	}

	public AARetrievePassengerEticketRS retrievePassengerTickets(AARetrievePassengerEticketRQ aaRetrievePassengerEticketRQ) {
		AARetrievePassengerEticketRS retrievePassengerEticketRS = new AARetrievePassengerEticketRS();
		retrievePassengerEticketRS.setResponseAttributes(new AAResponseAttributes());

		try {
			String groupPnr = aaRetrievePassengerEticketRQ.getGroupPnr();
			Reservation reservation = loadReservation(aaRetrievePassengerEticketRQ.getGroupPnr());
			List<FlightSegmentTO> flightSegmentTOs = ReservationUtil.getUnflownFlightSegmentsFromRes(reservation,
					aaRetrievePassengerEticketRQ.getAirTravelerTickets());
			SortUtil.sortFlightSegByDepZuluDate(flightSegmentTOs);
			List<Integer> lccSegmentSeqList = new ArrayList<Integer>();
			int lccSegmentSequnce = 1;
			for (IFlightSegment flightSegmentTO : flightSegmentTOs) {
				lccSegmentSeqList.add(lccSegmentSequnce++);
			}

			Collection<Integer> passengerSeqList = new ArrayList<Integer>();
			if (!aaRetrievePassengerEticketRQ.getAirTravelerTickets().isEmpty()) {
				for (AirTravelerEticket airTravelerEticket : aaRetrievePassengerEticketRQ.getAirTravelerTickets()) {
					passengerSeqList.add(airTravelerEticket.getPaxSequence());
				}
			} else {
				for (ReservationPax reservationPax : reservation.getPassengers()) {
					passengerSeqList.add(reservationPax.getPaxSequence());
				}
			}

			CreateTicketInfoDTO ticketInfoDto = ETicketUtil.createTicketingInfoDto(AAUtils
					.getTrackInfo(aaRetrievePassengerEticketRQ.getAaPos()));
			Map<Integer, Map<Integer, EticketDTO>> paxIATAEticketNumbers = AAServicesModuleUtils.getReservationBD()
					.generateIATAETicketNumbers(passengerSeqList, lccSegmentSeqList, ticketInfoDto);

			List<AirTravelerEticket> lccAirTravelerEtickets = new ArrayList<AirTravelerEticket>();

			for (Integer paxSequence : paxIATAEticketNumbers.keySet()) {
				Map<Integer, EticketDTO> paxEtickets = paxIATAEticketNumbers.get(paxSequence);
				List<Eticket> lccPaxEtickets = TransformerUtil.transform(paxEtickets, flightSegmentTOs);
				AirTravelerEticket airTravelerEticket = new AirTravelerEticket();
				airTravelerEticket.setPaxSequence(paxSequence);
				airTravelerEticket.getETickets().addAll(lccPaxEtickets);
				lccAirTravelerEtickets.add(airTravelerEticket);
			}

			retrievePassengerEticketRS.setGroupPnr(groupPnr);
			retrievePassengerEticketRS.getAirTravelerTickets().addAll(lccAirTravelerEtickets);
			retrievePassengerEticketRS.getResponseAttributes().setSuccess(new AASuccess());
			if (log.isDebugEnabled()) {
				log.debug("AAPassengerEticketsBL retrievePassengerTickets(AARetrievePassengerEticketRQ) succeded.");
			}
		} catch (ModuleException ex) {
			log.error("AAPassengerEticketsBL retrievePassengerTickets(AARetrievePassengerEticketRQ) failed.", ex);
			AAExceptionUtil.addAAErrrors(retrievePassengerEticketRS.getResponseAttributes().getErrors(), ex);
		} catch (Exception e) {
			log.error("AAPassengerEticketsBL retrievePassengerTickets(AARetrievePassengerEticketRQ) failed.", e);
			AAExceptionUtil.addAAErrrors(retrievePassengerEticketRS.getResponseAttributes().getErrors(), e);
		}
		return retrievePassengerEticketRS;
	}

	private static Reservation loadReservation(String pnr) throws ModuleException, WebservicesException {
		Reservation reservation = null;
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadOndChargesView(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);
		pnrModesDTO.setLoadSegView(true);
		reservation = AAServicesModuleUtils.getReservationBD().getReservation(pnrModesDTO, null);

		return reservation;
	}
}
