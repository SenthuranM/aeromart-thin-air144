/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2006 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.aaservices.core.bl.reservation;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AdminInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OriginDestinationOption;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OriginDestinationOptions;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaymentDetails;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TravelerFulfillment;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAirBookModifyRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAErrorCode;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAModificationTypeCode;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.aaservices.core.session.AASessionManager;
import com.isa.thinair.aaservices.core.util.AAExternalChargesUtil;
import com.isa.thinair.aaservices.core.util.AAReservationUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.AAUtils;
import com.isa.thinair.airproxy.api.dto.ReservationConstants;
import com.isa.thinair.airproxy.api.model.reservation.commons.LoyaltyPaymentInfo;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.CustomChargesTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ExternalSegmentTO;
import com.isa.thinair.airreservation.api.dto.PNRModifyPreValidationStatesTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.mediators.ReservationValidationUtils;
import com.isa.thinair.airreservation.api.model.ExternalPnrSegment;
import com.isa.thinair.airreservation.api.model.IPassenger;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.assembler.PassengerAssembler;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.webplatform.api.util.AuthorizationUtil;
import com.isa.thinair.webplatform.api.util.PrivilegesKeys;
import com.isa.thinair.webplatform.api.util.ReservationUtil;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;

/**
 * @author Nilindra Fernando
 */
public class AAModifyReservationUtil {

	private static final Log log = LogFactory.getLog(AAModifyReservationUtil.class);

	/**
	 * Cancel reservation.
	 * 
	 * @param reservation
	 * @param aaAirBookModifyRQ
	 * @param utilizeAllRefundables
	 *            TODO
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	static void cancelReservation(Reservation reservation, AAAirBookModifyRQ aaAirBookModifyRQ, boolean utilizeAllRefundables)
			throws ModuleException, WebservicesException {
		String pnr = aaAirBookModifyRQ.getAaReservation().getPnr();

		if (!ReservationInternalConstants.ReservationStatus.CANCEL.equals(reservation.getStatus())) {
			CustomChargesTO customChargesTO = AAReservationUtil.createCustomChargesTO(reservation,
					aaAirBookModifyRQ.getChargeOverride(), ReservationConstants.AlterationType.ALT_CANCEL_RES);
			
			Map<Integer, List<ExternalChgDTO>> paxCharges = AAModifyReservationQueryUtil
					.tranformPaxExternalChargeToExternalChgDTO(aaAirBookModifyRQ.getPerPaxExternalCharges());			
			
			AAServicesModuleUtils.getReservationBD().cancelReservation(pnr, customChargesTO, reservation.getVersion(), false,
					false, utilizeAllRefundables, null, false, aaAirBookModifyRQ.getUserNote(),
					GDSInternalCodes.GDSNotifyAction.NO_ACTION.getCode(), paxCharges);

			Collection<ExternalPnrSegment> colReservationExternalSegment = reservation.getExternalReservationSegment();
			Iterator itReservationExternalSegment = colReservationExternalSegment.iterator();
			Collection<ExternalSegmentTO> colExternalSegmentTO = new ArrayList<ExternalSegmentTO>();
			while (itReservationExternalSegment.hasNext()) {
				ExternalPnrSegment reservationExternalSegment = (ExternalPnrSegment) itReservationExternalSegment.next();

				// Capturing the Confirmed external segments in order to be sent to the change external segment(s) api
				if (ReservationInternalConstants.ReservationExternalSegmentStatus.CONFIRMED.equals(reservationExternalSegment
						.getStatus())) {
					ExternalSegmentTO externalSegmentTO = new ExternalSegmentTO(reservationExternalSegment);
					externalSegmentTO.setStatus(ReservationInternalConstants.ReservationExternalSegmentStatus.CANCEL);
					colExternalSegmentTO.add(externalSegmentTO);
				}
			}

			if (colExternalSegmentTO.size() > 0) {
				AAServicesModuleUtils.getSegmentBD().changeExternalSegments(pnr, colExternalSegmentTO,
						reservation.getVersion() + 1, null);
			}

		} else {
			// Reservation is already canceled
			throw new WebservicesException(AAErrorCode.ERR_14_MAXICO_REQUIRED_BOOKING_ALREADY_CANCELLED, "");
		}
	}

	/**
	 * Pay outstanding amount for a reservation.
	 * 
	 * @param reservation
	 * @param
	 * @param
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	static void balancePayment(Reservation reservation, AAAirBookModifyRQ aaAirBookModifyRQ, Collection privilegeKeys)
			throws ModuleException, WebservicesException {
		/*
		 * Balance payments for dry force confirmed bookings with carrier having only canceled segments and assuring
		 * credit b/f
		 */
		// if (ReservationInternalConstants.ReservationStatus.CANCEL.equals(reservation.getStatus())) {
		// // Already canceled
		// throw new WebservicesException(AAErrorCode.ERR_14_MAXICO_REQUIRED_BOOKING_ALREADY_CANCELLED, "");
		// }

		BigDecimal balanceAmountToPay = ReservationUtil.getTotalBalToPay(reservation);
		UserPrincipal userPrincipal = AASessionManager.getInstance().getCurrentUserPrincipal();

		Map<String, Collection<ExternalChgDTO>> externalChargesMap = AAExternalChargesUtil.getPassengerWiseExternalCharges(
				aaAirBookModifyRQ.getAaReservation().getAirReservation().getPriceInfo(), ChargeRateOperationType.MODIFY_ONLY);
		BigDecimal totalExternalCharges = AAExternalChargesUtil.getTotalExternalChargeAmount(externalChargesMap);
		balanceAmountToPay = AccelAeroCalculator.add(balanceAmountToPay, totalExternalCharges);

		// if promotion exists, allow zero payments
		if (balanceAmountToPay.doubleValue() == 0 && reservation.getPromotionId() == null) {// already paid
			throw new WebservicesException(AAErrorCode.ERR_15_MAXICO_REQUIRED_BOOKING_ALREADY_PAID, null);
		}

		// This boolean used for dry/interline bookings to skip payments. Because payments already done in MC side
		boolean isCapturePayments = aaAirBookModifyRQ.isCapturePayments();
		// extract existing paxIds
		Collection<Integer> pnrPaxIds = reservation.getPnrPaxIds();

		List<TravelerFulfillment> carrierFulfillment = aaAirBookModifyRQ.getAaReservation().getAirReservation().getFulfillment()
				.getCarrierFulfillments();
		// if promotion exists, allow zero payments
		if (carrierFulfillment.isEmpty() && reservation.getPromotionId() == null)
			throw new WebservicesException(AAErrorCode.ERR_5_MAXICO_REQUIRED_OPERATION_OR_VALUE_NOT_SUPPORTED,
					"Payment details cannot be empty");

		Map<Integer, ReservationPax> reservationPax = AAUtils.getReservationPaxSequence(reservation.getPassengers());
		IPassenger passenger = new PassengerAssembler(null);

		boolean isFirstPayment = true;
		BigDecimal travelerFulllmintsTotalToPay = AccelAeroCalculator.getDefaultBigDecimalZero();
		for (TravelerFulfillment travelerFulfillment : carrierFulfillment) {
			for (PaymentDetails paymentDetail : travelerFulfillment.getPaymentDetails()) {
				BigDecimal paymentAmount = paymentDetail.getPaymentSummary().getPaymentAmount();
				travelerFulllmintsTotalToPay = AccelAeroCalculator.add(travelerFulllmintsTotalToPay, paymentAmount);
				isFirstPayment = isFirstPayment && paymentDetail.isFirstPayment();
			}
		}
		if (Math.ceil(travelerFulllmintsTotalToPay.doubleValue()) - Math.ceil(balanceAmountToPay.doubleValue()) > 1.00
				|| travelerFulllmintsTotalToPay.doubleValue() < balanceAmountToPay.doubleValue()) {
			throw new WebservicesException(AAErrorCode.ERR_16_MAXICO_REQUIRED_INVALID_PAYMENT_AMOUNT, "Expected payment of "
					+ balanceAmountToPay + ", Received " + travelerFulllmintsTotalToPay);
		}

		for (TravelerFulfillment travelerFulfillment : carrierFulfillment) {
			PaymentAssembler reservationPayments = new PaymentAssembler();
			AAUtils.extractReservationPayments(travelerFulfillment.getPaymentDetails(), reservationPayments);
			ReservationPax traveler = reservationPax.get(travelerFulfillment.getPaxSequence());

			if (traveler == null)
				throw new WebservicesException(AAErrorCode.ERR_54_MAXICO_REQUIRED_COULDNT_MATCH_PASSENGER,
						"System error occured. Please contact administrator");

			if (isCapturePayments) // Need to authorize only if payment exists
				AAReservationUtil.authorizePayment(privilegeKeys, reservationPayments, userPrincipal.getAgentCode(), pnrPaxIds);

			// if (balanceAmountToPay.compareTo(reservationPayments.getTotalPayAmount()) != 0) {
			// throw new WebservicesException(AAErrorCode.ERR_16_MAXICO_REQUIRED_INVALID_PAYMENT_AMOUNT,
			// "Expected payment of " +
			// balanceAmountToPay + ", Received " + reservationPayments.getTotalPayAmount());
			// }
			reservationPayments.addExternalCharges(getExternalCharge(traveler, externalChargesMap));
			passenger.addPassengerPayments(traveler.getPnrPaxId(), reservationPayments);
			BigDecimal paymentConsumed = AccelAeroCalculator.getDefaultBigDecimalZero();
			// Note : we don't require to check pax is infant or not. Because travelerFulFillment will be only set for
			// adult/child from lcclient
			BigDecimal paxPaymentAmountWOExCharges = traveler.getTotalAvailableBalance().doubleValue() > 0 ? traveler
					.getTotalAvailableBalance() : AccelAeroCalculator.getDefaultBigDecimalZero();
			paymentConsumed = AccelAeroCalculator.add(paymentConsumed, paxPaymentAmountWOExCharges);
		}

		LoyaltyPaymentInfo loyaltyPaymentInfo = AAUtils.extractLoyaltyPaymentInfo(carrierFulfillment);

		AAServicesModuleUtils.getReservationBD()
				.updateReservationForPayment(reservation.getPnr(), passenger, false, false, reservation.getVersion(), null,
						false, false, isCapturePayments, false, false, isFirstPayment, loyaltyPaymentInfo, false, null);
	}

	private static Collection<ExternalChgDTO> getExternalCharge(ReservationPax traveler,
			Map<String, Collection<ExternalChgDTO>> externalChargesMap) {
		return externalChargesMap.get(traveler.getPaxSequence().toString());
	}

	/**
	 * Throws WebserviceExcetion if the user is not authorized to perform requested operation based on the time to
	 * departure/already flown constraints.
	 * 
	 * @param reservation
	 * @param bufferModPrivilege
	 * @param ctualModTypeCode
	 *            TODO
	 * 
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	static void authorizeModification(Collection privilegeKeys, Reservation reservation, String bufferModPrivilege,
			Collection modCnxPnrSegIds, AAModificationTypeCode actualModTypeCode) throws WebservicesException, ModuleException {

		PNRModifyPreValidationStatesTO statesTO = ReservationValidationUtils.getReservationOrSegPreValidationStates(reservation,
				modCnxPnrSegIds);
		UserPrincipal principal = AASessionManager.getInstance().getCurrentUserPrincipal();

		// Interline bookings modifiable or not
		if (!statesTO.isInterlinedModifiable()) {
			throw new WebservicesException(AAErrorCode.ERR_9_MAXICO_REQUIRED_AUTHORIZATION_ERROR,
					"Interlined Reservations cannot be modified once transfered");
		}

		// Reservation / Segment cancelled or not
		if (actualModTypeCode == AAModificationTypeCode.MODTYPE_7_CANCEL_OND
				|| actualModTypeCode == AAModificationTypeCode.MODTYPE_1_CANCEL_RESERVATION) {
			if (statesTO.isCancelled()
					&& !AuthorizationUtil.hasPrivilege(privilegeKeys,
							PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_ALLOW_CNX_MODIFY)) {
				throw new WebservicesException(AAErrorCode.ERR_9_MAXICO_REQUIRED_AUTHORIZATION_ERROR, "Already cancelled");
			}
		}

		if (actualModTypeCode == AAModificationTypeCode.MODTYPE_7_CANCEL_OND) {
			// Confirmed Reservation / Segment having authorization to cancel
			if (!statesTO.isCancelled()
					&& !AuthorizationUtil.hasPrivilege(privilegeKeys,
							PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_CANCEL_SEGMENT)) {
				log.warn("Authorizatin failure [userId=" + principal.getUserId() + ",failedPrivilege="
						+ PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_CANCEL_SEGMENT + "]");
				throw new WebservicesException(AAErrorCode.ERR_9_MAXICO_REQUIRED_AUTHORIZATION_ERROR,
						"Cancelled Reservations are not authorized to modify");
			}
		}

		if (actualModTypeCode == AAModificationTypeCode.MODTYPE_7_CANCEL_OND
				|| actualModTypeCode == AAModificationTypeCode.MODTYPE_18_MODIFY_OND) {
			// Restricted Segment
			if (statesTO.isRestricted()) {
				throw new WebservicesException(AAErrorCode.ERR_9_MAXICO_REQUIRED_AUTHORIZATION_ERROR,
						"Restricted Segments are not authorized to modify");
			}
		}

		// Reservation / Segment allow modifying
		if (statesTO.isFlown()
				&& !AuthorizationUtil.hasPrivilege(privilegeKeys, PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_OLD)
				&& actualModTypeCode != AAModificationTypeCode.MODTYPE_4_MODIFY_RESERVATION_PAX_INFO) {
			log.warn("Authorizatin failure [userId=" + principal.getUserId() + ",failedPrivilege="
					+ PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_OLD + "]");
			throw new WebservicesException(AAErrorCode.ERR_9_MAXICO_REQUIRED_AUTHORIZATION_ERROR,
					"Reservations having flown segment(s) are not authorized to modify");
		}

		if (bufferModPrivilege != null && !"".equals(bufferModPrivilege)) {
			if (statesTO.isInBufferTime() && !AuthorizationUtil.hasPrivilege(privilegeKeys, bufferModPrivilege)) {
				log.warn("Authorizatin failure [userId=" + principal.getUserId() + ",failedPrivilege=" + bufferModPrivilege + "]");
				throw new WebservicesException(AAErrorCode.ERR_9_MAXICO_REQUIRED_AUTHORIZATION_ERROR,
						"Not allowed within modification buffer duration");
			}
		}
	}

	/**
	 * 
	 * Throws WebserviceExcetion if the user is not authorized to perform requested operation based on the time to
	 * departure/already flown constraints. This method differ from authorizeModification due to omitting cancel segment
	 * privilege check
	 * 
	 * @param privilegeKeys
	 * @param reservation
	 * @param bufferModPrivilege
	 * @param modCnxPnrSegIds
	 * @throws WebservicesException
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	static void authorizeModificationForSplit(Collection privilegeKeys, Reservation reservation, String bufferModPrivilege,
			Collection modCnxPnrSegIds) throws WebservicesException, ModuleException {

		PNRModifyPreValidationStatesTO statesTO = ReservationValidationUtils.getReservationOrSegPreValidationStates(reservation,
				modCnxPnrSegIds);
		UserPrincipal principal = AASessionManager.getInstance().getCurrentUserPrincipal();

		// Interline bookings modifiable or not
		if (!statesTO.isInterlinedModifiable()) {
			throw new WebservicesException(AAErrorCode.ERR_9_MAXICO_REQUIRED_AUTHORIZATION_ERROR,
					"Interlined Reservations cannot be modified once transfered");
		}

		// Reservation / Segment cancelled or not
		if (statesTO.isCancelled()
				&& !AuthorizationUtil.hasPrivilege(privilegeKeys, PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_ALLOW_CNX_MODIFY)) {
			throw new WebservicesException(AAErrorCode.ERR_9_MAXICO_REQUIRED_AUTHORIZATION_ERROR, "Already cancelled");
		}

		// Restricted Segment
		if (statesTO.isRestricted()) {
			throw new WebservicesException(AAErrorCode.ERR_9_MAXICO_REQUIRED_AUTHORIZATION_ERROR,
					"Restricted Segments are not authorized to modify");
		}

		// Reservation / Segment allow modifying
		if (statesTO.isFlown()
				&& !AuthorizationUtil.hasPrivilege(privilegeKeys, PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_OLD)) {
			log.warn("Authorizatin failure [userId=" + principal.getUserId() + ",failedPrivilege="
					+ PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_OLD + "]");
			throw new WebservicesException(AAErrorCode.ERR_9_MAXICO_REQUIRED_AUTHORIZATION_ERROR,
					"Reservations having flown segment(s) are not authorized to modify");
		}

		if (bufferModPrivilege != null && !"".equals(bufferModPrivilege)) {
			if (statesTO.isInBufferTime() && !AuthorizationUtil.hasPrivilege(privilegeKeys, bufferModPrivilege)) {
				log.warn("Authorizatin failure [userId=" + principal.getUserId() + ",failedPrivilege=" + bufferModPrivilege + "]");
				throw new WebservicesException(AAErrorCode.ERR_9_MAXICO_REQUIRED_AUTHORIZATION_ERROR,
						"Not allowed within modification buffer duration");
			}
		}
	}

	/**
	 * Returns the own pnr segment id(s)
	 * 
	 * @param reservation
	 * @param lstOriDesOptions
	 * @return
	 * @throws WebservicesException
	 */
	static Collection<Collection<Integer>> getOndWiseOwnPnrSegIds(Reservation reservation,
			OriginDestinationOptions originDestinationOptions) throws WebservicesException {

		Map<Integer, Collection<Integer>> ondWiseSelectablePnrSegIdsMap = new HashMap<Integer, Collection<Integer>>();

		for (OriginDestinationOption oriDesOption : originDestinationOptions.getOriginDestinationOption()) {
			for (FlightSegment flightSegment : oriDesOption.getFlightSegment()) {

				if (AppSysParamsUtil.getDefaultCarrierCode().equals(flightSegment.getOperatingAirline())) {
					for (Iterator aaFlightSeg = reservation.getSegments().iterator(); aaFlightSeg.hasNext();) {
						ReservationSegment resSegment = (ReservationSegment) aaFlightSeg.next();

						if (resSegment.getPnrSegId() == Integer.parseInt(flightSegment.getBookingFlightRefNumber())) {

							Collection<Integer> pnrSegIds = ondWiseSelectablePnrSegIdsMap.get(resSegment.getOndGroupId());

							if (pnrSegIds == null) {
								pnrSegIds = new HashSet<Integer>();
								pnrSegIds.add(resSegment.getPnrSegId());

								ondWiseSelectablePnrSegIdsMap.put(resSegment.getOndGroupId(), pnrSegIds);
							} else {
								pnrSegIds.add(resSegment.getPnrSegId());
							}
						}
					}
				}
			}
		}

		if (ondWiseSelectablePnrSegIdsMap.size() > 0) {
			Map<Integer, Collection<Integer>> ondWiseResPnrSegIdsMap = getOndWiseResPnrSegIdsMap(reservation);
			for (Integer ondGroupId : ondWiseSelectablePnrSegIdsMap.keySet()) {
				Collection<Integer> selectedPnrSegIds = ondWiseSelectablePnrSegIdsMap.get(ondGroupId);
				Collection<Integer> resPnrSegIds = ondWiseResPnrSegIdsMap.get(ondGroupId);

				if (!selectedPnrSegIds.containsAll(resPnrSegIds)) {
					throw new WebservicesException(AAErrorCode.ERR_1_MAXICO_REQUIRED_INVALID_VALUE,
							"Invalid segment information sent");
				}
			}

			return ondWiseSelectablePnrSegIdsMap.values();
		}

		throw new WebservicesException(AAErrorCode.ERR_1_MAXICO_REQUIRED_INVALID_VALUE, "Segment information not specified");
	}

	/**
	 * Returns the ond group id wise pnr segment ids
	 * 
	 * @param reservation
	 * @return
	 */
	private static Map<Integer, Collection<Integer>> getOndWiseResPnrSegIdsMap(Reservation reservation) {
		Map<Integer, Collection<Integer>> ondWiseResPnrSegIdsMap = new HashMap<Integer, Collection<Integer>>();

		for (ReservationSegment reservationSegment : reservation.getSegments()) {

			Collection<Integer> pnrSegIds = ondWiseResPnrSegIdsMap.get(reservationSegment.getOndGroupId());

			if (pnrSegIds == null) {
				pnrSegIds = new HashSet<Integer>();
				pnrSegIds.add(reservationSegment.getPnrSegId());

				ondWiseResPnrSegIdsMap.put(reservationSegment.getOndGroupId(), pnrSegIds);
			} else {
				pnrSegIds.add(reservationSegment.getPnrSegId());
			}
		}

		return ondWiseResPnrSegIdsMap;
	}

	public static Collection<ExternalSegmentTO> getOndWiseExternalFlightSegments(
			OriginDestinationOptions originDestinationOptions, String status) throws WebservicesException {
		Collection<ExternalSegmentTO> externalFlightSegments = new ArrayList<ExternalSegmentTO>();
		Collection<String> carrierCodes = AAServicesModuleUtils.getReservationBD().getCarrierCodesByAirline(
				AppSysParamsUtil.getDefaultAirlineIdentifierCode());

		if (originDestinationOptions != null) {
			for (OriginDestinationOption oriDesOption : originDestinationOptions.getOriginDestinationOption()) {
				for (FlightSegment flightSegment : oriDesOption.getFlightSegment()) {
					if (!AppSysParamsUtil.getDefaultCarrierCode().equals(flightSegment.getOperatingAirline())
							&& !carrierCodes.contains(flightSegment.getOperatingAirline())) {

						if (flightSegment.getStatus() != null && flightSegment.getStatus().value() != null && status != null
								&& !status.equals(flightSegment.getStatus().value())) {
							continue;
						}

						ExternalSegmentTO externalSegmentTO = new ExternalSegmentTO();
						externalSegmentTO.setFlightNo(flightSegment.getFlightNumber());
						externalSegmentTO.setDepartureDate(flightSegment.getDepatureDateTime());
						externalSegmentTO.setArrivalDate(flightSegment.getArrivalDateTime());

						Date depatureDateZulu = (flightSegment.getDepatureDateTimeZulu() != null ? flightSegment
								.getDepatureDateTimeZulu() : flightSegment.getDepatureDateTime());
						Date arrivalDateTimeZulu = (flightSegment.getArrivalDateTimeZulu() != null ? flightSegment
								.getArrivalDateTimeZulu() : flightSegment.getArrivalDateTime());
						externalSegmentTO.setDepartureDateZulu(depatureDateZulu);
						externalSegmentTO.setArrivalDateZulu(arrivalDateTimeZulu);

						externalSegmentTO.setSegmentCode(flightSegment.getSegmentCode());
						externalSegmentTO.setFlightRefNumber(flightSegment.getFlightRefNumber());
						externalSegmentTO.setFltSegStatus(flightSegment.getFlightStatus());

						externalSegmentTO.setCabinClassCode(flightSegment.getCabinClassCode());
						externalSegmentTO.setLogicalCabinClassCode(flightSegment.getLogicalCabinClassCode());
						externalSegmentTO.setOperatingAirline(flightSegment.getOperatingAirline());
						externalSegmentTO.setReturnFlag(flightSegment.getReturnFlag());

						externalSegmentTO.setMarketingAgentCode(flightSegment.getMarketingAgentCode());
						externalSegmentTO.setMarketingCarrierCode(flightSegment.getMarketingCarrierCode());
						externalSegmentTO.setMarketingStationCode(flightSegment.getMarketingStationCode());
						externalSegmentTO.setStatusModifiedChannelCode(flightSegment.getStatusModifiedChannelCode());
						externalSegmentTO.setStatusModifiedDate(flightSegment.getStatusModifiedDate());
						externalSegmentTO.setSegmentSequence(flightSegment.getSegmentSequence());

						if (flightSegment.getBookingFlightRefNumber() != null) {
							externalSegmentTO.setExternalPnrSegId(Integer.valueOf(flightSegment.getBookingFlightRefNumber()));
						}
						if (status != null) {
							externalSegmentTO.setStatus(status);
						} else {
							externalSegmentTO.setStatus(flightSegment.getStatus().value());
						}
						externalSegmentTO.setSubStatus(flightSegment.getSubStatus());
						externalFlightSegments.add(externalSegmentTO);
					}
				}
			}
		}

		return externalFlightSegments;
	}

	public static Collection<ExternalSegmentTO>
			getOndWiseCanceledFlightSegments(OriginDestinationOptions originDestinationOptions) throws WebservicesException {
		Collection<ExternalSegmentTO> externalFlightSegments = new ArrayList<ExternalSegmentTO>();
		Collection<String> carrierCodes = AAServicesModuleUtils.getReservationBD().getCarrierCodesByAirline(
				AppSysParamsUtil.getDefaultAirlineIdentifierCode());

		if (originDestinationOptions != null) {
			for (OriginDestinationOption oriDesOption : originDestinationOptions.getOriginDestinationOption()) {
				for (FlightSegment flightSegment : oriDesOption.getFlightSegment()) {
					if ((ReservationInternalConstants.ReservationSegmentStatus.CANCEL
							.equals(flightSegment.getStatus().toString()))
							&& !AppSysParamsUtil.getDefaultCarrierCode().equals(flightSegment.getOperatingAirline())
							&& !carrierCodes.contains(flightSegment.getOperatingAirline())) {

						ExternalSegmentTO externalSegmentTO = new ExternalSegmentTO();
						externalSegmentTO.setFlightNo(flightSegment.getFlightNumber());
						externalSegmentTO.setDepartureDate(flightSegment.getDepatureDateTime());
						externalSegmentTO.setArrivalDate(flightSegment.getArrivalDateTime());

						Date depatureDateZulu = (flightSegment.getDepatureDateTimeZulu() != null ? flightSegment
								.getDepatureDateTimeZulu() : flightSegment.getDepatureDateTime());
						Date arrivalDateTimeZulu = (flightSegment.getArrivalDateTimeZulu() != null ? flightSegment
								.getArrivalDateTimeZulu() : flightSegment.getArrivalDateTime());
						externalSegmentTO.setDepartureDateZulu(depatureDateZulu);
						externalSegmentTO.setArrivalDateZulu(arrivalDateTimeZulu);

						externalSegmentTO.setSegmentCode(flightSegment.getSegmentCode());
						externalSegmentTO.setFlightRefNumber(flightSegment.getFlightRefNumber());
						externalSegmentTO.setFltSegStatus(flightSegment.getFlightStatus());

						externalSegmentTO.setCabinClassCode(flightSegment.getCabinClassCode());
						externalSegmentTO.setLogicalCabinClassCode(flightSegment.getLogicalCabinClassCode());
						externalSegmentTO.setOperatingAirline(flightSegment.getOperatingAirline());
						externalSegmentTO.setReturnFlag(flightSegment.getReturnFlag());

						externalSegmentTO.setMarketingAgentCode(flightSegment.getMarketingAgentCode());
						externalSegmentTO.setMarketingCarrierCode(flightSegment.getMarketingCarrierCode());
						externalSegmentTO.setMarketingStationCode(flightSegment.getMarketingStationCode());
						externalSegmentTO.setStatusModifiedChannelCode(flightSegment.getStatusModifiedChannelCode());
						externalSegmentTO.setStatusModifiedDate(flightSegment.getStatusModifiedDate());
						externalSegmentTO.setSegmentSequence(flightSegment.getSegmentSequence());
						externalSegmentTO.setSubStatus(flightSegment.getSubStatus());

						if (flightSegment.getBookingFlightRefNumber() != null) {
							externalSegmentTO.setExternalPnrSegId(Integer.valueOf(flightSegment.getBookingFlightRefNumber()));
						}
						externalSegmentTO.setStatus(flightSegment.getStatus().value());
						externalFlightSegments.add(externalSegmentTO);
					}
				}
			}
		}

		return externalFlightSegments;
	}

	/**
	 * Returns true if atleast one of the provided pnrSegId is already cancelled.
	 * 
	 * @param res
	 * @param pnrSegIds
	 * @return
	 */
	static boolean checkIfSegsAlreadyCancelled(Reservation res, Collection<Integer> pnrSegIds) {
		for (Iterator<ReservationSegment> resSegIt = res.getSegments().iterator(); resSegIt.hasNext();) {
			ReservationSegment resSeg = resSegIt.next();
			if (pnrSegIds.contains(resSeg.getPnrSegId())
					&& ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(resSeg.getStatus())) {
				return true;
			}
		}

		return false;
	}

	static boolean isOndsExists(OriginDestinationOptions airItinerary) {

		if (airItinerary != null && airItinerary.getOriginDestinationOption().size() > 0) {
			return true;
		}

		return false;
	}

	static void extendOnHoldReleaseTime(Reservation reservation, AAAirBookModifyRQ aaAirBookModifyRQ, Collection privilegeKeys)
			throws ParseException, ModuleException, WebservicesException {

		Date newReleaseDateTimeZulu = aaAirBookModifyRQ.getAaReservation().getAirReservation().getPriceInfo()
				.getOnholdReleaseTimeZulu();

		Date releaseDate = null;
		Iterator itReservationPax = reservation.getPassengers().iterator();
		while (itReservationPax.hasNext()) {
			ReservationPax reservationPax = (ReservationPax) itReservationPax.next();
			releaseDate = reservationPax.getZuluReleaseTimeStamp();
			if (releaseDate != null) {
				break;
			}
		}

		if (newReleaseDateTimeZulu.before(CalendarUtil.getCurrentSystemTimeInZulu())) {// Release time cannot be a past
																						// time
			throw new WebservicesException(
					AAErrorCode.ERR_30_MAXICO_REQUIRED_INVALID_ONHOLD_DURATION_PLEASE_CONTACT_ADMINISTRATOR,
					"Purposed Date is less than the current Date");
		}

		Collection colFlightSegmentDTO = WebplatformUtil.getConfirmedDepartureSegments(reservation.getSegmentsView());
		Date maxAllowedReleaseTimeZulu = WebplatformUtil.getMaxAllowedReleaseTimeZulu(colFlightSegmentDTO);
		boolean isConfirmedExternalSegmentExist = checkConfirmedExternalSegments(reservation.getExternalReservationSegment());

		if (reservation.getDummyBooking() == ReservationInternalConstants.DummyBooking.NO && maxAllowedReleaseTimeZulu == null
				&& !isConfirmedExternalSegmentExist) {
			throw new WebservicesException(
					AAErrorCode.ERR_30_MAXICO_REQUIRED_INVALID_ONHOLD_DURATION_PLEASE_CONTACT_ADMINISTRATOR,
					"Reservation can not extend onhold for expired segment(s)");
		}

		if (reservation.getDummyBooking() == ReservationInternalConstants.DummyBooking.NO && maxAllowedReleaseTimeZulu != null
				&& newReleaseDateTimeZulu.after(maxAllowedReleaseTimeZulu)) {
			newReleaseDateTimeZulu = maxAllowedReleaseTimeZulu; // Release time is allowed up to earliest of flight
																// closure and interline cut-over
		}

		long lDiff = BeanUtils.getIdealReleaseDate(newReleaseDateTimeZulu, releaseDate);
		int intDiff = Math.round(lDiff / (60 * 1000));

		AAServicesModuleUtils.getReservationBD().extendOnholdReservation(reservation.getPnr(), intDiff, reservation.getVersion(),
				null);
	}

	private static boolean checkConfirmedExternalSegments(Set<ExternalPnrSegment> existingExtsegments) {
		boolean isExternalSegmentExist = false;
		for (ExternalPnrSegment pnrSegment : existingExtsegments) {
			if (!ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(pnrSegment.getStatus())) {
				isExternalSegmentExist = true;
				break;
			}
		}
		return isExternalSegmentExist;
	}

	static void transferOwnership(Reservation reservation, AAAirBookModifyRQ aaAirBookModifyRQ) throws ModuleException {
		AdminInfo adminInfo = aaAirBookModifyRQ.getAaReservation().getAirReservation().getAirReservationSummary().getAdminInfo();
		String ownerAgentCode = BeanUtils.nullHandler(adminInfo.getOwnerAgentCode());
		if (ownerAgentCode.length() > 0) {
			AAServicesModuleUtils.getReservationBD().transferOwnerShip(reservation.getPnr(), ownerAgentCode,
					reservation.getVersion(), null);
		}
	}

	/**
	 * Method to get the reservation.
	 * 
	 * @param pnrModesDTO
	 * @param trackingInfoDTO
	 * @return
	 * @throws WebservicesException
	 * @throws ModuleException
	 * @throws Exception
	 */
	public static Reservation getReservation(LCCClientPnrModesDTO pnrModesDTO, long version, TrackInfoDTO trackingInfoDTO)
			throws WebservicesException, ModuleException {
		Reservation reservation = AAServicesModuleUtils.getReservationBD().getReservation(pnrModesDTO, trackingInfoDTO);
		if (reservation == null) {
			throw new WebservicesException(AAErrorCode.ERR_25_MAXICO_REQUIRED_THE_RESERVATION_NO_LONGER_EXIST,
					"No matching booking found");
		}

		if (reservation.getDummyBooking() == ReservationInternalConstants.DummyBooking.NO && reservation.getVersion() != version) {
			throw new WebservicesException(
					AAErrorCode.ERR_23_MAXICO_REQUIRED_CONCURRENT_UPDATE_OF_THE_RESERVATION_DETECTED_PLEASE_TRY_AGAIN, "");
		}

		return reservation;
	}

	static Collection<Integer> getFltSegIds(OriginDestinationOptions airItinerary) {
		Collection<Integer> newFltSegIds = new ArrayList<Integer>();
		for (OriginDestinationOption ondOption : airItinerary.getOriginDestinationOption()) {
			for (FlightSegment fltSeg : ondOption.getFlightSegment()) {
				if (AppSysParamsUtil.getDefaultCarrierCode().equals(fltSeg.getOperatingAirline())) {
					newFltSegIds.add(FlightRefNumberUtil.getSegmentIdFromFlightRPH(fltSeg.getFlightRefNumber()));
				}
			}
		}
		return newFltSegIds;
	}

}
