package com.isa.thinair.aaservices.core.bl.reservation;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.CreditOperationDetails;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAExtendORReinstateCreditRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAExtendORReinstateCreditRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.AAUtils;
import com.isa.thinair.airproxy.api.dto.PaxCreditReinstateTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class AAExtendORReinstateCreditBL {

	private static Log log = LogFactory.getLog(AAExtendORReinstateCreditBL.class);

	public static AAExtendORReinstateCreditRS execute(AAExtendORReinstateCreditRQ aaExtendORReinstateCreditRQ) {
		AAExtendORReinstateCreditRS aaExtendORReinstateCreditRS = new AAExtendORReinstateCreditRS();
		aaExtendORReinstateCreditRS.setResponseAttributes(new AAResponseAttributes());

		try {
			CreditOperationDetails creditOperationDetails = aaExtendORReinstateCreditRQ.getCreditOperationDetails();
			validateParams(creditOperationDetails);

			if (log.isDebugEnabled()) {
				log.debug("Going to execute ExtendORReinstateCredit for reservation " + creditOperationDetails.getPnr()
						+ " in carrier " + creditOperationDetails.getCarrierCode());
			}

			TrackInfoDTO trackInfo = AAUtils.getTrackInfo(aaExtendORReinstateCreditRQ.getAaPos());
			if (creditOperationDetails.getMode().equals(PaxCreditReinstateTO.EXTEND)) {
				AAServicesModuleUtils.getPassengerBD().extendCredits(creditOperationDetails.getCreditId(),
						creditOperationDetails.getExpireDate(), creditOperationDetails.getUserNote(),
						creditOperationDetails.getPnr(), creditOperationDetails.getPnrPaxId(), trackInfo);
			} else if (creditOperationDetails.getMode().equals(PaxCreditReinstateTO.RE_INSTATE)) {
				AAServicesModuleUtils.getPassengerBD().reinstateCredit(creditOperationDetails.getPnr(),
						creditOperationDetails.getPnrPaxId(), creditOperationDetails.getTxnId(),
						AccelAeroCalculator.multiply(creditOperationDetails.getAmount(), -1),
						creditOperationDetails.getExpireDate(), creditOperationDetails.getUserNote(), trackInfo);
			}
			aaExtendORReinstateCreditRS.getResponseAttributes().setSuccess(new AASuccess());

			if (log.isDebugEnabled()) {
				log.debug("Successfully executed ExtendORReinstateCredit for reservation " + creditOperationDetails.getPnr()
						+ " in carrier " + creditOperationDetails.getCarrierCode());
			}

		} catch (Exception e) {
			log.error("Error in AAExtendORReinstateCreditBL.execute", e);
			AAExceptionUtil.addAAErrrors(aaExtendORReinstateCreditRS.getResponseAttributes().getErrors(), e);
		}
		return aaExtendORReinstateCreditRS;
	}

	private static void validateParams(CreditOperationDetails creditOperationDetails) throws ModuleException {
		if (creditOperationDetails == null) {
			log.error("creditOperationDetails is null");
			throw new ModuleException("airreservations.arg.invalid.null");
		}
		if (creditOperationDetails.getMode() == null || creditOperationDetails.getExpireDate() == null
				|| creditOperationDetails.getPnr() == null || creditOperationDetails.getPnrPaxId() == 0
				|| creditOperationDetails.getTxnId() == 0) {
			log.error("Fields in creditOperationDetails is invalid. " + creditOperationDetails.getMode() + " - "
					+ creditOperationDetails.getExpireDate() + " - " + creditOperationDetails.getPnr() + " - "
					+ creditOperationDetails.getPnrPaxId() + " - " + creditOperationDetails.getTxnId());
			throw new ModuleException("airreservations.arg.invalid.null");
		}
		if (creditOperationDetails.getMode().equals(PaxCreditReinstateTO.EXTEND) && creditOperationDetails.getCreditId() == 0) {
			log.error("Credit id field in creditOperationDetails is invalid. ");
			throw new ModuleException("airreservations.arg.invalid.null");
		} else if (creditOperationDetails.getMode().equals(PaxCreditReinstateTO.RE_INSTATE)
				&& creditOperationDetails.getAmount() == null) {
			log.error("Amount fields in creditOperationDetails is null.");
			throw new ModuleException("airreservations.arg.invalid.null");
		}

	}
}
