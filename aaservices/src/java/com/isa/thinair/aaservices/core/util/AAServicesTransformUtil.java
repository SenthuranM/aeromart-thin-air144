/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005-08 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * ===============================================================================
 */
package com.isa.thinair.aaservices.core.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.Address;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AirTraveler;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ApplicablePromotionDetails;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ContactInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ExpectedBookingStates;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ExternalCharge;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ExternalChargeType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.LCCDiscountedFareDetails;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PassengerType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaxCharges;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PerPaxPriceInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SpecialReqDetails;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SpecialServiceRequest;
import com.isa.connectivity.profiles.maxico.v1.common.dto.StringIntegerMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Telephone;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAirBookRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAErrorCode;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.aaservices.core.session.AASessionManager;
import com.isa.thinair.aaservices.core.session.AAUserSession;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.PaxChargesTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.OnHoldReleaseTimeDTO;
import com.isa.thinair.airreservation.api.model.AutoCancellationInfo;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationInsurance;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.model.assembler.ReservationAssembler;
import com.isa.thinair.airreservation.api.model.assembler.SegmentSSRAssembler;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReleaseTimeUtil;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.dto.PaxAdditionalInfoDTO;
import com.isa.thinair.commons.api.dto.PaxCategoryFoidTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.promotion.api.to.ApplicablePromotionDetailsTO;
import com.isa.thinair.promotion.api.to.PromotionCriteriaTO;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;
import com.isa.thinair.wsclient.api.dto.aig.IInsuranceRequest;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceRequestAssembler;
import com.isa.thinair.wsclient.api.dto.aig.InsurePassenger;
import com.isa.thinair.wsclient.api.dto.aig.InsureSegment;

/**
 * Transforms AA [derived from OTA] data to AccelAero data.
 * 
 * @author Thejaka
 */
public class AAServicesTransformUtil {

	/**
	 * Transform booking request and returns reservation objects ready to pass to back end
	 */
	public static class BookingRequest implements IAAServiceTransformer {

		private final Log log = LogFactory.getLog(getClass());

		private final AAAirBookRQ request;

		private final Collection<OndFareDTO> ondFareDTOs;

		public BookingRequest(AAAirBookRQ request, Collection<OndFareDTO> ondFares) {
			this.request = request;
			this.ondFareDTOs = ondFares;
		}

		private AAAirBookRQ getRequest() {
			return request;
		}

		/**
		 * @return the ondFareDTOs
		 */
		public Collection<OndFareDTO> getOndFareDTOs() {
			return ondFareDTOs;
		}

		/**
		 * TODO refactor this method to return a dto rather than a array
		 * 
		 * @return new Object[]{reservation, dtReleaseTime, isOnholdReservation, splitReservation, listRPH};
		 * @throws ModuleException
		 * @throws WebservicesException
		 */
		@Override
		public Object[] transform() throws ModuleException, WebservicesException {

			ReservationAssembler reservation = new ReservationAssembler(new ArrayList(), null);
			ReservationContactInfo aaContactInfo = null;
			String aaUserNote = null;
			String aaUserNoteType = null;

			if (getRequest() != null) {
				if (getRequest().getContactInfo() != null) {
					aaContactInfo = transformToAAContactInfo(getRequest().getContactInfo(), getRequest()
							.getExpectedBookingStates(), AAUtils.isMarketingCarrier(getRequest().getAaPos()
							.getMarketingAirLineCode()));
				}
				if (getRequest().getLastUserNote() != null) {
					aaUserNote = getRequest().getLastUserNote().getNoteText();
					aaUserNoteType = getRequest().getLastUserNote().getUserNoteType();
				}

			}

			if (ondFareDTOs != null) {
				reservation.calculateTicketValidity(ondFareDTOs);
			}
			if (aaContactInfo == null) {
				aaContactInfo = prepareContactInfoTravelersInfo(getRequest().getTravelerInfo().getAirTraveler());
			}

			if (getRequest().getExpectedBookingStates() != ExpectedBookingStates.DUMMY) {
				AAUtils.enforceCarrierConfigForContactInfo(aaContactInfo, getRequest().getAppIndicator());
			}
			// set reservation contact info
			reservation.addContactInfo(aaUserNote, aaContactInfo, aaUserNoteType);

			AutoCancellationInfo autoCancellationInfo = AAReservationUtil.populateAutoCancellationInfo(request
					.getAutoCancellationInfo());
			Integer autoCnxId = null;
			if (autoCancellationInfo != null) {
				autoCancellationInfo = AAServicesModuleUtils.getReservationBD().saveAutoCancellationInfo(autoCancellationInfo,
						getRequest().getPnr(), null);
				if (autoCancellationInfo != null) {
					autoCnxId = autoCancellationInfo.getAutoCancellationId();
				}
			}

			List<String> listRPH = new ArrayList<String>();
			HashMap<String, IPayment> travelersPayments = null;
			HashMap<String, Object[]> paxRelationships = preparePaxRelationships(getRequest().getTravelerInfo().getAirTraveler());

			Date holdReleaseTimestamp = null;
			boolean isHoldBooking = false;
			boolean isPartialPayment = false;
			boolean isOnHoldBeforeConfirm = false;
			boolean isActualConfirmBooking = false; // Current implementation for dry & interline booking is to create
													// the onhold
			// in OC first and them do the balance payment. So we need to skip checking buffer time privilege for that
			// scenario
			if (getRequest().getExpectedBookingStates() == ExpectedBookingStates.ON_HOLD) {
				Date onholdReleaseTimeZulu = getRequest().getAirItineraryPricingInfo().getOnholdReleaseTimeZulu();
				holdReleaseTimestamp = (onholdReleaseTimeZulu == null ? null : onholdReleaseTimeZulu);
				isHoldBooking = true;
			} else if (getRequest().getExpectedBookingStates() == ExpectedBookingStates.DUMMY) {
				// dummy booking creation in the MC side
				Date onholdReleaseTimeZulu = getRequest().getAirItineraryPricingInfo().getOnholdReleaseTimeZulu();
				holdReleaseTimestamp = (onholdReleaseTimeZulu == null ? new Date() : onholdReleaseTimeZulu);
				isHoldBooking = true;

				// remove promotion details from dummy booking
				getRequest().getAirItineraryPricingInfo().setPromotionDetails(null);

			} else {
				isActualConfirmBooking = true;
				if (getRequest().getFulfillment() == null || getRequest().getFulfillment().getCarrierFulfillments().isEmpty()) {
					isHoldBooking = true;
					isOnHoldBeforeConfirm = true;
				} else {
					// This flow will not execute at the moment
					throw new WebservicesException(AAErrorCode.ERR_5_MAXICO_REQUIRED_OPERATION_OR_VALUE_NOT_SUPPORTED,
							"Operation not supported");

				}

				if (log.isDebugEnabled()) {
					log.debug("################ BOOK REQUEST Onhold ? " + isHoldBooking);
					log.debug("################ BOOK REQUEST partialPay ? " + isPartialPayment);
				}
				String agentCode = getAgentCode(getRequest());
				if (isHoldBooking || isPartialPayment) {
					holdReleaseTimestamp = WebplatformUtil.getReleaseTimeStamp(ReleaseTimeUtil
							.getFlightDepartureInfo(getOndFareDTOs()), (Collection) AASessionManager.getInstance()
							.getCurrUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS), true, agentCode, getRequest()
							.getAppIndicator(), ReleaseTimeUtil.getOnHoldReleaseTimeDTOFromOndFares(getOndFareDTOs()));
				} else {
					holdReleaseTimestamp = WebplatformUtil.getReleaseTimeStamp(ReleaseTimeUtil
							.getFlightDepartureInfo(getOndFareDTOs()), (Collection) AASessionManager.getInstance()
							.getCurrUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS), false, agentCode, getRequest()
							.getAppIndicator(), ReleaseTimeUtil.getOnHoldReleaseTimeDTOFromOndFares(getOndFareDTOs()));
				}				
			}			

			if (isHoldBooking && request.getPgwWiseOnholdTime() != null) {
				String agentCode = getAgentCode(getRequest());
				long bufferTime = ReleaseTimeUtil.getOnholdBufferTime(agentCode, getRequest().getAppIndicator(),
						ReleaseTimeUtil.getOnHoldReleaseTimeDTOFromOndFares(getOndFareDTOs()));
				Date pgwOnholdReleaseTimestamp = request.getPgwWiseOnholdTime();
				OnHoldReleaseTimeDTO onHoldReleaseTimeDTO = ReleaseTimeUtil.getOnHoldReleaseTimeDTOFromOndFares(getOndFareDTOs());
				if (bufferTime > 0 && onHoldReleaseTimeDTO != null && onHoldReleaseTimeDTO.getFlightDepartureDate() != null) {
					long DepartureDateTime = onHoldReleaseTimeDTO.getFlightDepartureDate().getTime();
					long systemAllowedTime = DepartureDateTime - bufferTime;
					if (systemAllowedTime < pgwOnholdReleaseTimestamp.getTime()) {
						throw new WebservicesException(AAErrorCode.ERR_7_MAXICO_REQUIRED_RESERVATION_NOT_ELIGIBLE_FOR_ONHOLD,
								null);
					} else {
						holdReleaseTimestamp = pgwOnholdReleaseTimestamp;
					}
				}
			}
			
			if (isHoldBooking) {
				if (holdReleaseTimestamp == null) {
					if (isPartialPayment) {
						throw new WebservicesException(AAErrorCode.ERR_6_MAXICO_REQUIRED_INVALID_SPLIT_OPERATION,
								"Current partially paid reservation is not eligible for hold");
					} else if (!isActualConfirmBooking) {
						throw new WebservicesException(AAErrorCode.ERR_7_MAXICO_REQUIRED_RESERVATION_NOT_ELIGIBLE_FOR_ONHOLD,
								null);
					}
				}
			}

			Map<String, Collection<ExternalChgDTO>> externalChgMap = getExternalChargeMap(ChargeRateOperationType.MAKE_ONLY);

			// Setting the PNR Zulu Release Time Stamp
			reservation.setPnrZuluReleaseTimeStamp(holdReleaseTimestamp);

			Integer nationalityCode = null;
			IPayment payment = null;
			Object[] currentPaxSequences;
			String foidNumber = null;
			PaxCategoryFoidTO paxCategoryFOIDTO = null;
			Set<String> insuranceRefNumbers = new HashSet<String>();
		

			int adultCount = 0;
			int childCount = 0;
			int infantCount = 0;

			for (AirTraveler traveler : getRequest().getTravelerInfo().getAirTraveler()) {

				listRPH.add(traveler.getTravelerRefNumber());
				currentPaxSequences = paxRelationships.get(traveler.getTravelerRefNumber());

				List<SpecialServiceRequest> ssrs = getSSRForPax(getRequest().getTravelerInfo().getSpecialReqDetails(),
						traveler.getTravelerRefNumber());
				for (SpecialServiceRequest ssr : ssrs) {
					if (ssr != null
							&& (ssr.getSsrCode().length() > 4 || !AAServicesModuleUtils.getWebServiceDAO().isValidSSR(
									ssr.getSsrCode()))) {
						throw new WebservicesException(AAErrorCode.ERR_1_MAXICO_REQUIRED_INVALID_VALUE, "SSR code ["
								+ ssr.getSsrCode() + "] is not supported.");
					}
				}

				for (PerPaxPriceInfo perPaxPriceInfo : getRequest().getAirItineraryPricingInfo().getPerPaxPriceInfo()) {
					if (perPaxPriceInfo.getTravelerRefNumber().equals(traveler.getTravelerRefNumber())) {
						for (ExternalCharge externalCharge : perPaxPriceInfo.getPassengerPrice().getExternalCharge()) {
							if (externalCharge.getType().compareTo(ExternalChargeType.INSURANCE) == 0) {
							//	InsurePassenger insurePassenger = getPopulatedInsurePassenger(traveler);
							//	insuranceRequest.addPassenger(insurePassenger);
								String [] references = externalCharge.getCode().split(",");
								insuranceRefNumbers.addAll( Arrays.asList(references));
							}
						}
					}
				}

				// pax nationality
				nationalityCode = AAUtils.getNationalityCode(traveler.getNationalityCode());

				// FIXME - Revise (Setting Pax FOID details)
				// TODO - Get Pax Category as Parameter if not set default category
				foidNumber = null;

				if (traveler.getPassengerType() != null) {
					paxCategoryFOIDTO = AAUtils.getPaxCategoryFOID(AAServiceConstants.DEFAULT_PAX_CATEGORY,
							traveler.getPassengerType());
				}

				if (getRequest().getTravelerInfo() != null) {
					foidNumber = AAUtils.getFOIDNumber(getRequest().getTravelerInfo().getAirTraveler(),
							traveler.getTravelerRefNumber());
				}

				AAUtils.validateFOIDNumber(paxCategoryFOIDTO, foidNumber);
				// End Setting FOID details

				Date psptExpiryDt = null;
				if (traveler.getFormOfIdentification().getPsptExpiry() != null) {
					psptExpiryDt = traveler.getFormOfIdentification().getPsptExpiry().toGregorianCalendar().getTime();
				}

				Date dateOfJoin = null;
				if (traveler.getFormOfIdentification().getDateOfJoin() != null) {
					dateOfJoin = traveler.getFormOfIdentification().getDateOfJoin().toGregorianCalendar().getTime();
				}

				PaxAdditionalInfoDTO paxAdditionalInfoDTO = new PaxAdditionalInfoDTO();
				paxAdditionalInfoDTO.setPassportNo(traveler.getFormOfIdentification().getFoidNumber());
				paxAdditionalInfoDTO.setPassportExpiry(psptExpiryDt);
				paxAdditionalInfoDTO.setPassportIssuedCntry(traveler.getFormOfIdentification().getPsptIssuedCntry());
				paxAdditionalInfoDTO.setEmployeeId(traveler.getFormOfIdentification().getEmployeeId());
				paxAdditionalInfoDTO.setDateOfJoin(dateOfJoin);
				paxAdditionalInfoDTO.setIdCategory(traveler.getFormOfIdentification().getIdCategory());
				paxAdditionalInfoDTO.setFfid(traveler.getFfid());
				// If it's an adult
				if (traveler.getPassengerType() != null && traveler.getPassengerType() == PassengerType.ADT) {

					if (travelersPayments != null) {
						payment = travelersPayments.get(traveler.getTravelerRefNumber());
					} else {
						payment = new PaymentAssembler();// 0 payment
					}

					// Adding the external charge
					if (externalChgMap != null) {
						payment.addExternalCharges(externalChgMap.get(traveler.getTravelerRefNumber()));
					}

					if ((Integer) currentPaxSequences[1] == -1) {// adult traveler
						reservation.addSingle(
								traveler.getPersonName().getFirstName(), // first name
								traveler.getPersonName().getSurName(), // last name
								traveler.getPersonName().getTitle(), // title
								traveler.getBirthDate() != null ? traveler.getBirthDate().toGregorianCalendar().getTime() : null, // birth
																																	// date
								nationalityCode, // nationality
								(Integer) currentPaxSequences[0], // pax sequence
								paxAdditionalInfoDTO, traveler.getPassengerCategory(), payment, new SegmentSSRAssembler(), null,
								null, null, null, CommonsConstants.INDIVIDUAL_MEMBER, null, null, null, null, null);

					} else {// parent (adult with an infant) traveler
						reservation.addParent(
								traveler.getPersonName().getFirstName(), // first name
								traveler.getPersonName().getSurName(), // last name
								traveler.getPersonName().getTitle(), // title
								traveler.getBirthDate() != null ? traveler.getBirthDate().toGregorianCalendar().getTime() : null, // birth
																																	// date
								nationalityCode, // nationality
								(Integer) currentPaxSequences[0], // pax sequence
								(Integer) currentPaxSequences[1],// infant sequece
								paxAdditionalInfoDTO, traveler.getPassengerCategory(), payment, new SegmentSSRAssembler(), null,
								null, null, null, CommonsConstants.INDIVIDUAL_MEMBER, null, null, null, null, null);
					}
					adultCount++;

					// If it's a child
				} else if (traveler.getPassengerType() != null && traveler.getPassengerType() == PassengerType.CHD) {

					if (travelersPayments != null) {
						payment = travelersPayments.get(traveler.getTravelerRefNumber());
					} else {
						payment = new PaymentAssembler();// 0 payment
					}

					// Adding the external charge
					if (externalChgMap != null) {
						payment.addExternalCharges(externalChgMap.get(traveler.getTravelerRefNumber()));
					}

					reservation.addChild(
							traveler.getPersonName().getFirstName(), // first name
							traveler.getPersonName().getSurName(), // last name
							traveler.getPersonName().getTitle(), // title
							traveler.getBirthDate() != null ? traveler.getBirthDate().toGregorianCalendar().getTime() : null, // birth
																																// date
							nationalityCode, // nationality
							(Integer) currentPaxSequences[0], // pax sequence
							paxAdditionalInfoDTO, traveler.getPassengerCategory(), payment, new SegmentSSRAssembler(), null,
							null, null, null, CommonsConstants.INDIVIDUAL_MEMBER, null, null,
							null, null, null);

					childCount++;

					// If it's an infant
				} else if (traveler.getPassengerType() != null && traveler.getPassengerType() == PassengerType.INF) {// infant
					// traveler

					Integer infAutoCnxId = (traveler.isAlertAutoCancellation()) ? autoCnxId : null;
					
					if (travelersPayments != null) {
						payment = travelersPayments.get(traveler.getTravelerRefNumber());
					} else {
						payment = new PaymentAssembler();// 0 payment
					}

					// Adding the external charge
					if (externalChgMap != null) {
						payment.addExternalCharges(externalChgMap.get(traveler.getTravelerRefNumber()));
					}

					reservation.addInfant(
							traveler.getPersonName().getFirstName(), // first name
							traveler.getPersonName().getSurName(), // last name
							traveler.getPersonName().getTitle(), // title
							traveler.getBirthDate() != null ? traveler.getBirthDate().toGregorianCalendar().getTime() : null, // birth
																																// date
							nationalityCode, // nationality
							(Integer) currentPaxSequences[0], // pax sequence
							(Integer) currentPaxSequences[1], // parent sequence
							paxAdditionalInfoDTO, traveler.getPassengerCategory(), payment, new SegmentSSRAssembler(), null, null,
							null, infAutoCnxId, CommonsConstants.INDIVIDUAL_MEMBER, null,
							null, null, null, null);
					infantCount++;
				}
			}

			reservation.setPnr(getRequest().getPnr());
			reservation.setOriginatorPnr(getRequest().getGroupPnr());

			// set total pax count
			reservation.addPaxCount(adultCount, childCount, infantCount);

			// setting the pax count

			if (insuranceRefNumbers != null && !insuranceRefNumbers.isEmpty()) {
				ReservationBD reservationBD = AAServicesModuleUtils.getReservationBD();

				for (String insuranceRefNumber : insuranceRefNumbers) {
					IInsuranceRequest insuranceRequest = new InsuranceRequestAssembler();
					ReservationInsurance reservationInsurance = reservationBD.getReservationInsurance(Integer
							.parseInt(insuranceRefNumber));
					InsureSegment insureSegment = new InsureSegment();
					insureSegment.setFromAirportCode(reservationInsurance.getOrigin());
					insureSegment.setToAirportCode(reservationInsurance.getDestination());
					insureSegment.setDepartureDate(reservationInsurance.getDateOfTravel());
					insureSegment.setArrivalDate(reservationInsurance.getDateOfReturn());
					insureSegment.setRoundTrip(reservationInsurance.getRoundTrip().equals("Y") ? true : false);

					insuranceRequest.addFlightSegment(insureSegment);
					insuranceRequest.setInsuranceId(reservationInsurance.getInsuranceId());
					insuranceRequest.setQuotedTotalPremiumAmount(reservationInsurance.getQuotedAmount());

					reservation.addInsurance(insuranceRequest);
				}
			}

			if (getRequest().getExpectedBookingStates() != ExpectedBookingStates.DUMMY) {
				// Keeping only visible field values in passenger list
				AAUtils.enforceCarrierConfigForPax(reservation.getDummyReservation().getPassengers(), getRequest()
						.getAppIndicator());

				// Set Promotion details
				if (request.getDiscountedFareDetails() != null) {
					DiscountedFareDetails discountedFareDetails = tranformLccDiscountedFareDetails(request
							.getDiscountedFareDetails());
					reservation.setFareDiscount(discountedFareDetails.getFarePercentage(), discountedFareDetails.getNotes());
					reservation.setFareDiscountInfo(discountedFareDetails);

					if (AppSysParamsUtil.enableFareDiscountCodes()) {
						reservation.setFareDiscountCode(discountedFareDetails.getCode());
					}
				}
			}

			reservation.setAutoCancellationInfo(autoCancellationInfo);

			return new Object[] { reservation, // 0
					holdReleaseTimestamp, // 1
					isHoldBooking, // 2
					isPartialPayment, // 3
					listRPH, // 4
					getOndFareDTOs(), // 5
					aaContactInfo, // 6
					isOnHoldBeforeConfirm }; // 7
		}
		/**
		 * TODO:Review
		 * @param request
		 * @return
		 */
		private String getAgentCode(AAAirBookRQ request) {
			String agentCode = null;
			if (request != null && request.getAaPos() != null) {
				agentCode = request.getAaPos().getMarketingAgentCode();
			}
			return agentCode; 			
		}

		private InsurePassenger getPopulatedInsurePassenger(AirTraveler insuredAirTraveler) {

			InsurePassenger insurePassenger = new InsurePassenger();
			if (insuredAirTraveler.getAddress() != null) {
				for (String addressLine : insuredAirTraveler.getAddress().getAddressLine()) {

					if (insurePassenger.getAddressLn1() == null) {
						insurePassenger.setAddressLn1(addressLine);
					}

					if (insurePassenger.getAddressLn2() == null) {
						insurePassenger.setAddressLn2(addressLine);
					}
				}
				insurePassenger.setCity(insuredAirTraveler.getAddress().getCityName());

				if (insuredAirTraveler.getAddress().getCountry() != null) {
					insurePassenger.setCountryOfAddress(insuredAirTraveler.getAddress().getCountry().getCountryName());
				}
			}

			insurePassenger.setEmail(insuredAirTraveler.getEmail());

			if (insuredAirTraveler.getTelephone() != null) {
				insurePassenger.setHomePhoneNumber(insuredAirTraveler.getTelephone().getPhoneNumber());
			}

			if (insuredAirTraveler.getPersonName() != null) {
				insurePassenger.setFirstName(insuredAirTraveler.getPersonName().getFirstName());
				insurePassenger.setLastName(insuredAirTraveler.getPersonName().getSurName());
			}

			insurePassenger.setDateOfBirth(CalendarUtil.asDate(insuredAirTraveler.getBirthDate()));

			return insurePassenger;
		}

		private Map<String, Collection<ExternalChgDTO>> getExternalChargeMap(int chargesAplicablity) throws WebservicesException,
				ModuleException {
			return AAExternalChargesUtil.getPassengerWiseExternalCharges(getRequest().getAirItineraryPricingInfo(),
					chargesAplicablity);
		}

		/**
		 * Prepares contact info from the first adult traveler
		 */
		private ReservationContactInfo prepareContactInfoTravelersInfo(List<AirTraveler> travelers) {
			ReservationContactInfo reservationContactInfo = new ReservationContactInfo();
			AirTraveler firstAdultTraveler = getFirstAdultPax(travelers);

			if (firstAdultTraveler.getPersonName() != null) {
				reservationContactInfo.setFirstName(firstAdultTraveler.getPersonName().getFirstName());
				reservationContactInfo.setLastName(firstAdultTraveler.getPersonName().getSurName());
			}

			String email = BeanUtils.nullHandler(firstAdultTraveler.getEmail());
			if (email.length() > 0) {
				reservationContactInfo.setEmail(email);
			}

			Telephone telephone = firstAdultTraveler.getTelephone();
			if (telephone != null) {
				String phoneNumber = "";
				String areaCode = BeanUtils.nullHandler(telephone.getAreaCode());

				if (areaCode.length() > 0) {
					phoneNumber += areaCode + AAServiceConstants.PHONE_NUMBER_SEPERATOR;
				}

				phoneNumber += telephone.getPhoneNumber();
				reservationContactInfo.setPhoneNo(phoneNumber);
			}

			Address address = firstAdultTraveler.getAddress();
			if (address != null) {
				if (address.getCountry() != null) {
					reservationContactInfo.setCountryCode(address.getCountry().getCountryCode());
				}

				reservationContactInfo.setCity(address.getCityName());

				reservationContactInfo.setStreetAddress1(CommonUtil.getValueFromNullSafeList(address.getAddressLine(), 0));
				reservationContactInfo.setStreetAddress2(CommonUtil.getValueFromNullSafeList(address.getAddressLine(), 1));
			}

			return reservationContactInfo;
		}

		/**
		 * Extracts the traveler with the least RPH ( valid adult/child traveler RPH are of the form Ai where i is a
		 * possitive integer)
		 */
		private AirTraveler getFirstAdultPax(List<AirTraveler> travelers) {
			AirTraveler firstAdultPax = null;
			for (AirTraveler traveler : travelers) {
				if (traveler.getPassengerType() != null) {
					if (traveler.getPassengerType() == PassengerType.ADT || traveler.getPassengerType() == PassengerType.CHD) {
						if (firstAdultPax == null
								|| Integer.parseInt(firstAdultPax.getTravelerRefNumber().substring(1)) > Integer
										.parseInt(traveler.getTravelerRefNumber().substring(1))) {
							firstAdultPax = traveler;
						}
					}
				}
			}
			return firstAdultPax;
		}

		/**
		 * Decodes Adult/Infant relationships and Prepares paxSequence Numbers based on travelerRPH Valid travelerRPH
		 * are Ai, Ij/Ai where i,j are mutually exclusive subsets of positive integers
		 * 
		 * @return HashMap of Object [] {paxSequence, infant/parent paxSeqence or -1, infant/parent paxRPH or null,
		 *         airTraveler}
		 */
		private HashMap<String, Object[]> preparePaxRelationships(List<AirTraveler> airTravelers) throws WebservicesException {
			HashMap<String, Object[]> paxSequencesMap = new HashMap<String, Object[]>();
			int paxSequence;
			String travelerRPH;
			String parentRPH;
			List<Integer> paxSequenceList = new ArrayList<Integer>();// for validating sequence uniqueness
			for (AirTraveler airTraveler : airTravelers) {
				if (airTraveler.getPassengerType() != null && airTraveler.getPassengerType() == PassengerType.ADT
						|| airTraveler.getPassengerType() == PassengerType.CHD) {
					travelerRPH = airTraveler.getTravelerRefNumber();
					paxSequence = Integer.parseInt(travelerRPH.substring(1));
					if (paxSequenceList.contains(new Integer(paxSequence))) {
						throw new WebservicesException(AAErrorCode.ERR_2_MAXICO_REQUIRED_INVALID_TRAVELLER_REFERENCE_NUMBER,
								"TravelerRPH [" + travelerRPH + "] is in conflict.");
					} else {
						paxSequenceList.add(new Integer(paxSequence));
					}
					paxSequencesMap.put(travelerRPH, new Object[] { paxSequence, -1, null, airTraveler });
				}
			}

			Object[] parentSeqences;
			for (AirTraveler airTraveler : airTravelers) {
				if (airTraveler.getPassengerType() != null && airTraveler.getPassengerType() == PassengerType.INF) {
					travelerRPH = airTraveler.getTravelerRefNumber();
					paxSequence = Integer.parseInt(travelerRPH.substring(1, travelerRPH.indexOf('/')));// infant
																										// sequence
					if (paxSequenceList.contains(new Integer(paxSequence))) {
						throw new WebservicesException(AAErrorCode.ERR_2_MAXICO_REQUIRED_INVALID_TRAVELLER_REFERENCE_NUMBER,
								"TravelerRPH [" + travelerRPH + "] is in conflict.");
					} else {
						paxSequenceList.add(new Integer(paxSequence));
					}

					parentRPH = travelerRPH.substring(travelerRPH.indexOf('/') + 1);
					parentSeqences = paxSequencesMap.get(parentRPH);
					if (parentSeqences == null) {
						throw new WebservicesException(AAErrorCode.ERR_2_MAXICO_REQUIRED_INVALID_TRAVELLER_REFERENCE_NUMBER,
								"Infant of RPH [" + airTraveler.getTravelerRefNumber() + "] is not linked to any adult.");
					}
					parentSeqences[1] = paxSequence;// infant sequence
					parentSeqences[2] = travelerRPH;// infant RPH
					paxSequencesMap.put(parentRPH, parentSeqences);

					paxSequencesMap.put(travelerRPH, new Object[] { paxSequence, parentSeqences[0], parentRPH, airTraveler });
				}
			}
			return paxSequencesMap;
		}

	}// end BookRequest

	private static List<SpecialServiceRequest> getSSRForPax(SpecialReqDetails specialReqDetails, String rph) {
		List<SpecialServiceRequest> ssrRequests = new ArrayList<SpecialServiceRequest>();
		if (specialReqDetails != null) {
			for (SpecialServiceRequest ssr : specialReqDetails.getSpecialServiceRequests()) {
				if (ssr.getTravelerRefNumber() != null) {
					if (ssr.getTravelerRefNumber().equals(rph)) {
						ssrRequests.add(ssr);
					}
				}
			}
		}
		return ssrRequests;
	}

	/**
	 * Transforms WS {@link ContactInfoType} to AA {@link ReservationContactInfo}
	 * 
	 * @param bookingType
	 *            : The booking type to indicate whether the reservation is on-hold,dummy etc.
	 * @param isMarketingCarrier
	 * 
	 * @return AA {@link ReservationContactInfo}
	 * @throws ModuleException
	 */
	public static ReservationContactInfo transformToAAContactInfo(ContactInfo wsContactInfo, ExpectedBookingStates bookingType,
			boolean isMarketingCarrier) throws ModuleException {
		ReservationContactInfo aaContactInfo = new ReservationContactInfo();

		// set the marketing carrier customer id if it is a IBE registered user booking
		if (bookingType != null) {
			// if this is a dummy booking we have to set the customer id.
			if (ExpectedBookingStates.DUMMY.equals(bookingType)) {
				aaContactInfo.setCustomerId(wsContactInfo.getMarketingCarrierCustomerId());
			} else {
				// if this is not a dummy booking we set the marketing customer id.
				aaContactInfo.setMarketingCarrierCustomerId(wsContactInfo.getMarketingCarrierCustomerId());
				if (isMarketingCarrier) {
					aaContactInfo.setCustomerId(wsContactInfo.getMarketingCarrierCustomerId());
				}
			}
		} else {
			aaContactInfo.setMarketingCarrierCustomerId(wsContactInfo.getMarketingCarrierCustomerId());
			if (isMarketingCarrier) {
				aaContactInfo.setCustomerId(wsContactInfo.getMarketingCarrierCustomerId());
			}
		}

		String email = BeanUtils.nullHandler(wsContactInfo.getEmail());

		if (email.length() > 0) {
			aaContactInfo.setEmail(email);
		}

		if (wsContactInfo.getTelephone() != null) {
			String telephoneNo = BeanUtils.nullHandler(wsContactInfo.getTelephone().getPhoneNumber());

			if (telephoneNo.length() > 0) {
				String phoneNumber = "";

				String areaCode = BeanUtils.nullHandler(wsContactInfo.getTelephone().getAreaCode());
				if (areaCode.length() > 0) {
					phoneNumber += areaCode + AAServiceConstants.PHONE_NUMBER_SEPERATOR;
				}

				phoneNumber += telephoneNo;
				aaContactInfo.setPhoneNo(phoneNumber);
			}
		}

		if (wsContactInfo.getMobile() != null) {
			String mobileNo = BeanUtils.nullHandler(wsContactInfo.getMobile().getPhoneNumber());

			if (mobileNo.length() > 0) {
				String phoneNumber = "";

				String areaCode = BeanUtils.nullHandler(wsContactInfo.getMobile().getAreaCode());
				if (areaCode.length() > 0) {
					phoneNumber += areaCode + AAServiceConstants.PHONE_NUMBER_SEPERATOR;
				}

				phoneNumber += mobileNo;
				aaContactInfo.setMobileNo(phoneNumber);
			}
		}

		if (wsContactInfo.getFax() != null) {
			String faxNumber = BeanUtils.nullHandler(wsContactInfo.getFax().getPhoneNumber());

			if (faxNumber.length() > 0) {
				String phoneNumber = "";

				String areaCode = BeanUtils.nullHandler(wsContactInfo.getFax().getAreaCode());
				if (areaCode.length() > 0) {
					phoneNumber += areaCode + AAServiceConstants.PHONE_NUMBER_SEPERATOR;
				}

				phoneNumber += faxNumber;
				aaContactInfo.setFax(phoneNumber);
			}
		}

		aaContactInfo.setTitle(wsContactInfo.getPersonName().getTitle());
		aaContactInfo.setFirstName(wsContactInfo.getPersonName().getFirstName());
		aaContactInfo.setLastName(wsContactInfo.getPersonName().getSurName());

		Address address = wsContactInfo.getAddress();

		if (address != null) {
			aaContactInfo.setCountryCode(address.getCountry().getCountryCode());
			aaContactInfo.setCity(address.getCityName());

			if (!address.getAddressLine().isEmpty()) {
				aaContactInfo.setStreetAddress1(address.getAddressLine().get(0));
				if (address.getAddressLine().size() > 1) {
					aaContactInfo.setStreetAddress2(address.getAddressLine().get(1));
				}

			}

			String state = BeanUtils.nullHandler(address.getState());
			if (state.length() > 0) {
				aaContactInfo.setState(state);
			}
		}

		aaContactInfo.setZipCode(BeanUtils.nullHandler(wsContactInfo.getZipCode()));

		// Nationality
		Integer nationalityCode = AAUtils.getNationalityCode(wsContactInfo.getNationalityCode());
		if (nationalityCode != null) {
			aaContactInfo.setNationalityCode(nationalityCode + "");
		}

		String prefLang = (wsContactInfo.getPreferredLanguage() == null) ? Locale.ENGLISH.toString() : wsContactInfo
				.getPreferredLanguage();
		aaContactInfo.setPreferredLanguage(prefLang);
		
		aaContactInfo.setTaxRegNo(wsContactInfo.getTaxRegNo());

		// Emergency contact info

		if (wsContactInfo.getPersonName() != null) {
			aaContactInfo.setEmgnTitle(wsContactInfo.getEmgnPersonName().getTitle());
			aaContactInfo.setEmgnFirstName(wsContactInfo.getEmgnPersonName().getFirstName());
			aaContactInfo.setEmgnLastName(wsContactInfo.getEmgnPersonName().getSurName());
		}

		if (wsContactInfo.getEmgnTelephone() != null) {
			String emgnPhoneNumber = BeanUtils.nullHandler(wsContactInfo.getEmgnTelephone().getPhoneNumber());

			if (emgnPhoneNumber.length() > 0) {
				String phoneNumber = "";

				String areaCode = BeanUtils.nullHandler(wsContactInfo.getEmgnTelephone().getAreaCode());
				if (areaCode.length() > 0) {
					phoneNumber += areaCode + AAServiceConstants.PHONE_NUMBER_SEPERATOR;
				}

				phoneNumber += emgnPhoneNumber;
				aaContactInfo.setEmgnPhoneNo(phoneNumber);
			}
		}

		aaContactInfo.setEmgnEmail(wsContactInfo.getEmgnEmail());
		aaContactInfo.setSendPromoEmail(wsContactInfo.isSendPromoEmail());

		return aaContactInfo;
	}

	public static DiscountedFareDetails tranformLccDiscountedFareDetails(LCCDiscountedFareDetails lccDiscountedFareDetails)
			throws ModuleException {
		DiscountedFareDetails discountedFareDetails = null;
		if (lccDiscountedFareDetails != null) {
			discountedFareDetails = new DiscountedFareDetails();

			if (PromotionCriteriaConstants.DiscountTypes.VALUE.equals(lccDiscountedFareDetails.getDiscountType())) {
				PromotionCriteriaTO promotionCriteriaTO = AAServicesModuleUtils.getPromotionCriteriaAdminBD()
						.findPromotionCriteria(lccDiscountedFareDetails.getPromotionId());
				if (promotionCriteriaTO.getDiscountValue() != null) {
					lccDiscountedFareDetails.setFarePercentage(promotionCriteriaTO.getDiscountValue().floatValue());
				}
			}

			discountedFareDetails.setFarePercentage(lccDiscountedFareDetails.getFarePercentage());
			discountedFareDetails.setNotes(lccDiscountedFareDetails.getNotes());
			discountedFareDetails.setCode(lccDiscountedFareDetails.getCode());
			discountedFareDetails.setDomFareDiscountPercentage(lccDiscountedFareDetails.getDomFareDiscountPercentage());
			discountedFareDetails.setDomesticDiscountApplied(lccDiscountedFareDetails.isDomesticDiscountApplied());

			if (!lccDiscountedFareDetails.getDomesticSegmentCodeList().isEmpty()) {
				discountedFareDetails.setDomesticSegmentCodeList(new ArrayList<String>(lccDiscountedFareDetails
						.getDomesticSegmentCodeList()));
			}

			discountedFareDetails.setPromotionId(lccDiscountedFareDetails.getPromotionId());
			discountedFareDetails.setPromotionType(lccDiscountedFareDetails.getPromoType());
			discountedFareDetails.setPromoCode(lccDiscountedFareDetails.getPromoCode());
			discountedFareDetails.setSystemGenerated(lccDiscountedFareDetails.isSystemGenerated());
			discountedFareDetails.setAllowSamePaxOnly(lccDiscountedFareDetails.isAllowSamePaxOnly());
			discountedFareDetails.setOriginPnr(lccDiscountedFareDetails.getOriginPnr());
			discountedFareDetails.setDiscountType(lccDiscountedFareDetails.getDiscountType());
			discountedFareDetails.setDiscountApplyTo(lccDiscountedFareDetails.getDiscountApplyTo());
			discountedFareDetails.setDiscountAs(lccDiscountedFareDetails.getDiscountAs());

			for (StringIntegerMap mapObj : lccDiscountedFareDetails.getApplicablePaxCount()) {
				discountedFareDetails.getApplicablePaxCount().put(mapObj.getKey(), mapObj.getValue());
			}

			if (!lccDiscountedFareDetails.getApplicableOnds().isEmpty()) {
				discountedFareDetails.setApplicableOnds(new HashSet<String>(lccDiscountedFareDetails.getApplicableOnds()));
			}

			if (!lccDiscountedFareDetails.getApplicableAncillaries().isEmpty()) {
				discountedFareDetails.setApplicableAncillaries(lccDiscountedFareDetails.getApplicableAncillaries());
			}
			if (!lccDiscountedFareDetails.getApplicableBINs().isEmpty()) {
				discountedFareDetails.setApplicableBINs(new HashSet<Integer>(lccDiscountedFareDetails.getApplicableBINs()));
			}

		}
		return discountedFareDetails;
	}

	public static ApplicablePromotionDetails transformApplicablePromotionDetailsTO(
			ApplicablePromotionDetailsTO applicablePromotionDetailsTO) {
		if (applicablePromotionDetailsTO != null) {
			ApplicablePromotionDetails applicablePromotionDetails = new ApplicablePromotionDetails();
			applicablePromotionDetails.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
			applicablePromotionDetails.setPromoCriteriaId(applicablePromotionDetailsTO.getPromoCriteriaId());
			applicablePromotionDetails.setPromoName(applicablePromotionDetailsTO.getPromoName());
			applicablePromotionDetails.setDescription(applicablePromotionDetailsTO.getDescription());
			applicablePromotionDetails.setPromoType(applicablePromotionDetailsTO.getPromoType());
			applicablePromotionDetails.setPromoCode(applicablePromotionDetailsTO.getPromoCode());
			applicablePromotionDetails.setSystemGenerated(applicablePromotionDetailsTO.isSystemGenerated());
			applicablePromotionDetails.setAllowSamePaxOnly(applicablePromotionDetailsTO.isAllowSamePaxOnly());
			applicablePromotionDetails.setDiscountType(applicablePromotionDetailsTO.getDiscountType());
			applicablePromotionDetails.setDiscountValue(applicablePromotionDetailsTO.getDiscountValue());
			applicablePromotionDetails.setDiscountAs(applicablePromotionDetailsTO.getDiscountAs());
			applicablePromotionDetails.setApplyTo(applicablePromotionDetailsTO.getApplyTo());
			applicablePromotionDetails.setApplicableAdultCount(applicablePromotionDetailsTO.getApplicableAdultCount());
			applicablePromotionDetails.setApplicableChildCount(applicablePromotionDetailsTO.getApplicableChildCount());
			applicablePromotionDetails.setApplicableInfantCount(applicablePromotionDetailsTO.getApplicableInfantCount());

			if (applicablePromotionDetailsTO.getApplicableAncillaries() != null) {
				applicablePromotionDetails.getApplicableAncillaries().addAll(
						applicablePromotionDetailsTO.getApplicableAncillaries());
			}

			if (applicablePromotionDetailsTO.getApplicableBINs() != null) {
				applicablePromotionDetails.getApplicableBINs().addAll(applicablePromotionDetailsTO.getApplicableBINs());
			}
			applicablePromotionDetails.getApplicableOnds().addAll(applicablePromotionDetailsTO.getApplicableOnds());
			return applicablePromotionDetails;
		}

		return null;
	}
	
	public static List<PaxChargesTO> tranformLccPaxCharges(List<PaxCharges> lccPaxCharges) {
		List<PaxChargesTO> paxChargesTOs = null;
		if (lccPaxCharges != null && !lccPaxCharges.isEmpty()) {
			paxChargesTOs = new ArrayList<PaxChargesTO>();
			for (PaxCharges lccPaxCharge : lccPaxCharges) {
				PaxChargesTO paxChargesTO = new PaxChargesTO();
				paxChargesTO.setParent(lccPaxCharge.isParent());
				paxChargesTO.setPaxSequence(lccPaxCharge.getPaxSequence());
				paxChargesTO.setPaxTypeCode(PaxTypeUtils.convertLCCPaxCodeToAAPaxCodes(lccPaxCharge.getPassengerType()));

				List<ExternalCharge> lccExternalCharges = lccPaxCharge.getExternalCharges();

				if (lccExternalCharges != null && !lccExternalCharges.isEmpty()) {
					List<LCCClientExternalChgDTO> extChgDTOList = TransformerUtil.tranformExtCharges(lccExternalCharges);
					paxChargesTO.setExtChgList(extChgDTOList);
				}

				paxChargesTOs.add(paxChargesTO);
			}
		}
		return paxChargesTOs;
	}

}