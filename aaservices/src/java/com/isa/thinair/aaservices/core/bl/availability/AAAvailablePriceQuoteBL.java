package com.isa.thinair.aaservices.core.bl.availability;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AirTraveler;
import com.isa.connectivity.profiles.maxico.v1.common.dto.IntegerIntegerMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.InterlineAirlineFareInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OndSeatAvailability;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OriginDestinationInformation;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OriginDestinationOption;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OriginDestinationOptions;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PassengerType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PerPaxPriceInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PriceInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PriceInfoType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PricingSource;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SeatAvailability;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAFlightPriceRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAFlightPriceRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.core.session.AASessionManager;
import com.isa.thinair.aaservices.core.session.AAUserSession;
import com.isa.thinair.aaservices.core.transaction.TransactionalReservationProcess;
import com.isa.thinair.aaservices.core.transaction.TransactionalReservationProcessBL;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAModificationParamUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.AAServicesTransformUtil;
import com.isa.thinair.aaservices.core.util.AAUtils;
import com.isa.thinair.aaservices.core.util.CloneUtil;
import com.isa.thinair.aaservices.core.util.PaxTypeUtils;
import com.isa.thinair.aaservices.core.util.PriceCalculationUtil;
import com.isa.thinair.aaservices.core.util.TransformerUtil;
import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareLiteDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableConnectedFlight;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableIBOBFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.ExternalFareSummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareSummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airinventory.api.dto.seatavailability.OriginDestinationInfoDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SelectedFlightDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.util.FareQuoteUtil;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxContainer;
import com.isa.thinair.airproxy.api.model.reservation.core.ModificationParamRQInfo;
import com.isa.thinair.airproxy.api.utils.converters.FareConvertUtil;
import com.isa.thinair.airproxy.api.utils.converters.OndConvertUtil;
import com.isa.thinair.airreservation.api.utils.ReleaseTimeUtil;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.LogicalCabinClassDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;

public class AAAvailablePriceQuoteBL implements TransactionalReservationProcessBL {

	private static Log log = LogFactory.getLog(AAAvailablePriceQuoteBL.class);

	private AAFlightPriceRQ aaFlightPriceRQ;

	private AAFlightPriceRS aaFlightPriceRS = null;

	private AvailabilityDTOCreator availabilityDTOCreator = null;

	private TransactionalReservationProcess transactionalReservationProcess;

	// private Date initiTime = new Date();

	@Override
	public void setTransactionalReservationProcess(TransactionalReservationProcess transactionalReservationProcess) {
		this.transactionalReservationProcess = transactionalReservationProcess;
	}

	public void setAaFlightPriceRQ(AAFlightPriceRQ aaFlightPriceRQ) {
		this.aaFlightPriceRQ = aaFlightPriceRQ;
		aaFlightPriceRS = new AAFlightPriceRS();
		aaFlightPriceRS.setResponseAttributes(new AAResponseAttributes());
		aaFlightPriceRS.setHeaderInfo(aaFlightPriceRQ.getHeaderInfo());
		aaFlightPriceRS.getResponseAttributes().setSuccess(new AASuccess());
	}

	public AAFlightPriceRS execute() {

		try {

			log.debug("AAPriceQuoteBL called by LCConnect @ " + AppSysParamsUtil.getDefaultCarrierCode());

			availabilityDTOCreator = new AvailabilityDTOCreator(aaFlightPriceRQ);
			availabilityDTOCreator.createDTO();

			AvailableFlightSearchDTO availableSearchDTO = availabilityDTOCreator.getAvailableSearchDTO();
			availableSearchDTO.setOwnSearch(false);
			availableSearchDTO.setSkipInvCheckForFlown(AppSysParamsUtil.skipInvChkForFlownInRequote());
			// setting the external booking class code
			List<InterlineAirlineFareInfo> interlineFareList = aaFlightPriceRQ.getInterlineAirlineFareInfo();
			ExternalFareSummaryDTO interlineFareSummaryDTO = null;
			if (interlineFareList != null && interlineFareList.size() > 0) {
				for (InterlineAirlineFareInfo interlineAirlineFareInfo : interlineFareList) {
					if (interlineFareSummaryDTO == null) {
						interlineFareSummaryDTO = new ExternalFareSummaryDTO();
						interlineFareSummaryDTO.setBookingClassCode(interlineAirlineFareInfo.getBookingClassCode());
						interlineFareSummaryDTO.setAdultFareAmount(interlineAirlineFareInfo.getFareAmount().doubleValue());
						interlineFareSummaryDTO.setChildFareAmount(interlineAirlineFareInfo.getFareAmount().doubleValue());
						interlineFareSummaryDTO.setInfantFareAmount(0);
					} else if (!interlineFareSummaryDTO.getBookingClassCode().equals(
							interlineAirlineFareInfo.getBookingClassCode())) {
						// TODO not supported booking class combination
						// exception
					}
				}

				OriginDestinationInfoDTO ondInfoDto = availableSearchDTO.getOrderedOriginDestinations()
						.get(OndSequence.OUT_BOUND);
				ondInfoDto.setPreferredBookingClass(interlineFareSummaryDTO.getBookingClassCode());
				ondInfoDto.setPreferredBookingType(BookingClass.BookingClassType.INTERLINE_FREESELL);

				availableSearchDTO.setExternalFareSummaryDTO(interlineFareSummaryDTO);
			}

			if (interlineFareSummaryDTO != null) {
				availableSearchDTO.setInterlineFareQuoted(true);
			}

			// set the excluded booking class list
			availableSearchDTO.setExternalBookingClasses(aaFlightPriceRQ.getBookingClassExclutions());

			Map<Integer, Boolean> ondQuoteFlexi = availableSearchDTO.getOndQuoteFlexi();

			if (!aaFlightPriceRQ.getAvailPreferences().isModifyBooking()) {
				if (availableSearchDTO.isInboundFareQuote()) {
					if (ondQuoteFlexi.get(OndSequence.IN_BOUND) != null) {
						Boolean quoteFlexi = ondQuoteFlexi.get(OndSequence.IN_BOUND);
						for (OriginDestinationInfoDTO ondInfo : availableSearchDTO.getOriginDestinationInfoDTOs()) {
							ondInfo.setQuoteFlexi(quoteFlexi);
						}
					}
				}
			}

			SelectedFlightDTO selectedFlightDTO = AAServicesModuleUtils.getReservationQueryBD().getFareQuote(availableSearchDTO,
					null);

			// if interline booking class provided inject the interline fare
			boolean injectInterlineFares = (interlineFareSummaryDTO != null);
			if (interlineFareSummaryDTO != null) {
				injectInterlineFare(selectedFlightDTO);
			}

			AppIndicatorEnum appIndicatorEnum = AppIndicatorEnum.APP_XBE;
			if (aaFlightPriceRQ.getAvailPreferences().getAppIndicator().equals(ApplicationEngine.IBE.toString())) {
				appIndicatorEnum = AppIndicatorEnum.APP_IBE;
			} else if (aaFlightPriceRQ.getAvailPreferences().getAppIndicator().equals(ApplicationEngine.ANDROID.toString())) {
				appIndicatorEnum = AppIndicatorEnum.APP_ANDROID;
			} else if (aaFlightPriceRQ.getAvailPreferences().getAppIndicator().equals(ApplicationEngine.IOS.toString())) {
				appIndicatorEnum = AppIndicatorEnum.APP_IOS;
			}
			ModificationParamRQInfo modificationParamRQInfo = new ModificationParamRQInfo();
			modificationParamRQInfo.setAppIndicator(appIndicatorEnum);
			modificationParamRQInfo.setIsRegisteredUser(false);
			List<String> reservationParams = AAModificationParamUtil.getModificationParams(modificationParamRQInfo, null, false, 0);

			Set<ServiceTaxContainer> applicableServiceTaxes = new HashSet<ServiceTaxContainer>();
			if (!availabilityDTOCreator.isInbound() && selectedFlightDTO != null) {
				ServiceTaxContainer applicableServiceTax = AAServicesModuleUtils.getReservationBD().getApplicableServiceTax(
						selectedFlightDTO.getSelectedOndFlight(OndSequence.OUT_BOUND), EXTERNAL_CHARGES.JN_OTHER);
				if (applicableServiceTax != null) {
					applicableServiceTaxes.add(applicableServiceTax);
				}
			}

			populateAAFlightPriceRS(selectedFlightDTO, injectInterlineFares, reservationParams,
					availableSearchDTO.isInboundFareQuote(), applicableServiceTaxes);

		} catch (Exception e) {
			log.error(transactionalReservationProcess.getTransactionIdentifier() + " - AAAvailablePriceQuote failed.", e);
			aaFlightPriceRS = new AAFlightPriceRS();
			aaFlightPriceRS.setResponseAttributes(new AAResponseAttributes());
			aaFlightPriceRS.setHeaderInfo(aaFlightPriceRQ.getHeaderInfo());
			AAExceptionUtil.addAAErrrors(aaFlightPriceRS.getResponseAttributes().getErrors(), e);
		}

		return aaFlightPriceRS;
	}

	private void injectInterlineFare(SelectedFlightDTO selectedFlightDTO) {

		List<InterlineAirlineFareInfo> interlineFareList = aaFlightPriceRQ.getInterlineAirlineFareInfo();
		Map<String, BigDecimal> overridePaxFareMap = new HashMap<String, BigDecimal>();
		boolean adultFareSet = false;
		for (InterlineAirlineFareInfo interlineAirlineFareInfo : interlineFareList) {
			PassengerType paxType = interlineAirlineFareInfo.getPassengerTypeQuantity().get(0).getPassengerType();
			if (!adultFareSet
					&& (paxType.value().equals(PassengerType.ADT.value()) || paxType.value().equals(PassengerType.CHD.value()))) {
				overridePaxFareMap.put(PaxTypeTO.ADULT, interlineAirlineFareInfo.getFareAmount());
				adultFareSet = true;
			} else if (paxType.value().equals(PassengerType.INF.value())) {
				overridePaxFareMap.put(PaxTypeTO.INFANT, interlineAirlineFareInfo.getFareAmount());
			}
		}

		Collection<OndFareDTO> ondFareDTOList = selectedFlightDTO.getOndFareDTOs(false);
		AvailabilityUtil.injectInterlineFaresToOndDTOs(ondFareDTOList, overridePaxFareMap);
	}

	private void injectIntlFareToFSC(FareSegChargeTO fsc) {
		List<InterlineAirlineFareInfo> interlineFareList = aaFlightPriceRQ.getInterlineAirlineFareInfo();
		BigDecimal interlineAdultChildFare = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal interlineInfantFare = AccelAeroCalculator.getDefaultBigDecimalZero();
		for (InterlineAirlineFareInfo interlineAirlineFareInfo : interlineFareList) {
			PassengerType paxType = interlineAirlineFareInfo.getPassengerTypeQuantity().get(0).getPassengerType();
			if (interlineAdultChildFare.doubleValue() == 0
					&& (paxType.value().equals(PassengerType.ADT.value()) || paxType.value().equals(PassengerType.CHD.value()))) {
				interlineAdultChildFare = interlineAirlineFareInfo.getFareAmount();
			} else if (paxType.value().equals(PassengerType.INF.value())) {
				interlineInfantFare = interlineAirlineFareInfo.getFareAmount();
			}
		}
		fsc.addOverridePaxFares(PaxTypeTO.ADULT, interlineAdultChildFare);
		fsc.addOverridePaxFares(PaxTypeTO.CHILD, interlineAdultChildFare);
		fsc.addOverridePaxFares(PaxTypeTO.INFANT, interlineInfantFare);

	}

	private void populateAAFlightPriceRS(SelectedFlightDTO selectedFlightDTO, boolean injectInterlineFares,
			List<String> reservationParams, boolean inboundFQ, Set<ServiceTaxContainer> applicableServiceTaxes)
			throws ModuleException {

		// boolean isFixedFare = false;
		BigDecimal totalFare = AccelAeroCalculator.getDefaultBigDecimalZero();

		PriceInfoType priceInfoType = new PriceInfoType();
		PriceInfoType segFarePriceInfoType = new PriceInfoType();

		AvailableFlightSearchDTO availableFlightSearchDTO = availabilityDTOCreator.getAvailableSearchDTO();

		FareRuleProxy fareRuleProxy = new FareRuleProxy();
		if (selectedFlightDTO.isSeatsAvailable()) {
			Collection<OndFareDTO> ondFareDTOList = selectedFlightDTO.getOndFareDTOs(false);
			Collection<OndFareDTO> ondSegFareDTOList = selectedFlightDTO.getOndFareDTOs(true);

			List<String> segmentCodeList = new ArrayList<String>();
			Map<Integer, Integer> actualOndSequenceMap = new HashMap<Integer, Integer>();
			for (OndFareDTO ondFareDTO : ondFareDTOList) {
				// create the fare summary list
				List<FareSummaryDTO> fareSummaryList = new ArrayList<FareSummaryDTO>();
				fareSummaryList.add(ondFareDTO.getFareSummaryDTO());
				BigDecimal totalForOnD = PriceCalculationUtil.calculateTotalFare(fareSummaryList,
						availabilityDTOCreator.getAdultCount(), availabilityDTOCreator.getChildCount(),
						availabilityDTOCreator.getInfantCount());
				totalFare = AccelAeroCalculator.add(totalFare, totalForOnD);

				for (FlightSegmentDTO segment : ondFareDTO.getSegmentsMap().values()) {
					segmentCodeList.add(segment.getSegmentCode());
				}
				actualOndSequenceMap.put(ondFareDTO.getOndSequence(), getActualOndSequence(ondFareDTO.getOndSequence()));
			}

			FareSegChargeTO fareSegChargeTO = null;
			FareSegChargeTO segFareChargeTO = null; // Holds segment or one way charges for a return fare
			if (ondFareDTOList != null) {
				AAPaxCountAssembler paxCountAssember = new AAPaxCountAssembler(this.aaFlightPriceRQ.getTravelerInfoSummary()
						.getAirTravelerAvail().getPassengerTypeQuantity());
				fareSegChargeTO = FareConvertUtil.getFareSegChargeTO(selectedFlightDTO.getFareType(), ondFareDTOList,
						paxCountAssember, actualOndSequenceMap, selectedFlightDTO.getAvailableIBOBFlightSegments(false));

				if (ondSegFareDTOList != null) {
					segFareChargeTO = FareConvertUtil.getFareSegChargeTO(selectedFlightDTO.getFareType(), ondSegFareDTOList,
							paxCountAssember, actualOndSequenceMap, selectedFlightDTO.getAvailableIBOBFlightSegments(true));
				}

				if (inboundFQ && !selectedFlightDTO.isReturnFareQuote()) {
					fareSegChargeTO.setInbound(true);
				}
				if (injectInterlineFares) {
					injectIntlFareToFSC(fareSegChargeTO);
					if (segFareChargeTO != null) {
						injectIntlFareToFSC(segFareChargeTO);
					}
				}

				TreeMap<Integer, BundledFareDTO> ondSelectedBundleFares = new TreeMap<Integer, BundledFareDTO>();
				for (AvailableIBOBFlightSegment availableIBOBFlightSegment : selectedFlightDTO.getSelectedOndFlights()) {

					int ondSequence = availableIBOBFlightSegment.getOndSequence();
					if (actualOndSequenceMap != null && actualOndSequenceMap.containsKey(ondSequence)) {
						ondSequence = actualOndSequenceMap.get(ondSequence);
					}
					ondSelectedBundleFares.put(ondSequence, availableIBOBFlightSegment.getSelectedBundledFare());

					List<FlightSegmentDTO> flightSegmentDTOs = new ArrayList<FlightSegmentDTO>();
					if (availableIBOBFlightSegment.isDirectFlight()) {
						flightSegmentDTOs = availableIBOBFlightSegment.getFlightSegmentDTOs();
					} else {
						AvailableConnectedFlight connectedFlight = (AvailableConnectedFlight) availableIBOBFlightSegment;
						List<AvailableFlightSegment> availableFlightSegments = connectedFlight.getAvailableFlightSegments();
						for (AvailableFlightSegment availableFlightSegment : availableFlightSegments) {
							flightSegmentDTOs.addAll(availableFlightSegment.getFlightSegmentDTOs());
						}
					}

					for (FlightSegmentDTO flightSegmentDTO : flightSegmentDTOs) {
						transactionalReservationProcess.addSegmentBundledFare(flightSegmentDTO.getSegmentCode(),
								availableIBOBFlightSegment.getSelectedBundledFare());
					}

				}

				// TODO remove this hack and maintain map for ondbundlefares
				if (ondSelectedBundleFares.firstKey() > 0) {
					for (int a = 0; a < ondSelectedBundleFares.firstKey(); a++) {
						ondSelectedBundleFares.put(a, null);
					}
				}

				fareSegChargeTO.setOndBundledFareDTOs(new ArrayList<BundledFareDTO>(ondSelectedBundleFares.values()));
			}
			// Removed since modification is failing -- Need to pass the
			// modification flag to skip
			// if(!aaFlightPriceRQ.isOwnAirlineFareQuote()){
			if (aaFlightPriceRQ.isAppendFareQuote()) {
				transactionalReservationProcess.addOndFareSegChargeTOList(segmentCodeList, fareSegChargeTO);
			} else {
				transactionalReservationProcess.setOndFareSegChargeTOList(segmentCodeList, fareSegChargeTO,
						new AAPaxCountAssembler(this.aaFlightPriceRQ.getTravelerInfoSummary().getAirTravelerAvail()
								.getPassengerTypeQuantity()));

			}
			// }aaFlightPriceRS.getPriceInfo().getPriceInfo().getFareType().getSurcharges()
			if (ondFareDTOList != null && !ondFareDTOList.isEmpty()) {
				PriceInfo priceInfo = getPriceInfoFromFareType(fareSegChargeTO, ondFareDTOList, false);
				PriceInfo segFarePriceInfo = null;

				fareRuleProxy.refreshTheCache(ondFareDTOList);
				fareRuleProxy.refreshTheCache(ondSegFareDTOList);
				priceInfo.getApplicableFareRule().addAll(PriceCalculationUtil.getFareRules(ondFareDTOList, fareRuleProxy));
				priceInfoType.setPriceInfo(priceInfo);

				if (ondSegFareDTOList != null && !ondSegFareDTOList.isEmpty()) {
					segFarePriceInfo = getPriceInfoFromFareType(segFareChargeTO, ondSegFareDTOList, false);
					segFarePriceInfo.getApplicableFareRule().addAll(
							PriceCalculationUtil.getFareRules(ondSegFareDTOList, fareRuleProxy));
					segFarePriceInfoType.setPriceInfo(segFarePriceInfo);
				}

				if (selectedFlightDTO.getSelectedOndFlights() != null && !selectedFlightDTO.getSelectedOndFlights().isEmpty()) {
					for (AvailableIBOBFlightSegment ondFlight : selectedFlightDTO.getSelectedOndFlights()) {

						int actualOndSequence = getActualOndSequence(ondFlight.getOndSequence());

						boolean isOndFlexiSelected = false;

						Map<Integer, Boolean> ondFlexiSelection = availableFlightSearchDTO.getOndSelectedFlexi();
						if (ondFlexiSelection != null && ondFlexiSelection.containsKey(actualOndSequence)) {
							boolean bundledFareSelected = (ondFlight.getSelectedBundledFare() != null);
							isOndFlexiSelected = !bundledFareSelected && ondFlexiSelection.get(actualOndSequence);
						}

						OndSeatAvailability ondSeatAvailability = new OndSeatAvailability();
						ondSeatAvailability.setOndSequence(actualOndSequence);
						ondSeatAvailability.getSeatAvailability().addAll(
								this.buildONDSeatAvailability(ondFlight, isOndFlexiSelected));

						priceInfo.getOndSeatAvailability().add(ondSeatAvailability);
					}

				}

				if (segFarePriceInfo != null && selectedFlightDTO != null && selectedFlightDTO.getSelectedOndFlights() != null
						&& !selectedFlightDTO.getSelectedOndFlights().isEmpty()) {
					for (int ondSequence = 0; ondSequence < selectedFlightDTO.getSelectedOndFlights().size(); ondSequence++) {

						AvailableIBOBFlightSegment selectedOndSegFlight = selectedFlightDTO.getSelectedOndSegFlight(ondSequence);
						boolean isOndFlexiSelected = false;

						Map<Integer, Boolean> ondFlexiSelection = availableFlightSearchDTO.getOndSelectedFlexi();
						if (ondFlexiSelection != null && ondFlexiSelection.containsKey(ondSequence)) {
							boolean bundledFareSelected = (selectedOndSegFlight.getSelectedBundledFare() != null);
							isOndFlexiSelected = !bundledFareSelected && ondFlexiSelection.get(ondSequence);
						}

						OndSeatAvailability ondSeatAvailability = new OndSeatAvailability();
						ondSeatAvailability.setOndSequence(ondSequence);
						ondSeatAvailability.getSeatAvailability().addAll(
								this.buildONDSeatAvailability(selectedOndSegFlight, isOndFlexiSelected));

						if (segFarePriceInfo != null) {
							segFarePriceInfo.getOndSeatAvailability().add(ondSeatAvailability);
						}

					}
				}

				// set applied promotion information
				priceInfo.setPromotionDetails(AAServicesTransformUtil.transformApplicablePromotionDetailsTO(selectedFlightDTO
						.getPromotionDetails()));
			}
		}

		if (reservationParams != null) {
			priceInfoType.getReservationParams().addAll(reservationParams);
		}

		aaFlightPriceRS.setPriceInfo(priceInfoType);
		aaFlightPriceRS.setSegFarePriceInfo(segFarePriceInfoType);
		aaFlightPriceRS.getServiceTaxes().addAll(AvailabilityUtil.populateLCCServiceTaxContainers(applicableServiceTaxes));
		Map<Integer,Integer> segGrouping = new HashMap<Integer, Integer>();
		int seq = 0;
		if (false && AppSysParamsUtil.isEnableOndSplitForBusFareQuote()) { //TODO remove false when busfare implemented for dry
			for (OriginDestinationInformation ondInfo : aaFlightPriceRQ.getOriginDestinationInformation()) {
				OriginDestinationOptions ondOption = ondInfo.getOriginDestinationOptions();
				for (OriginDestinationOption option : ondOption.getOriginDestinationOption()) {
					int i = 0;
					while (i < option.getFlightSegment().size()) {
						segGrouping.put(seq, ondInfo.getOndSequence());
						i++;
						seq++;
					}
				}
			}
		}
		if (selectedFlightDTO != null && selectedFlightDTO.getSelectedOndFlights() != null
				&& !selectedFlightDTO.getSelectedOndFlights().isEmpty()) {
			for (int ondSequence = 0; ondSequence < selectedFlightDTO.getSelectedOndFlights().size(); ondSequence++) {
				OriginDestinationInformation ondInfoRS = new OriginDestinationInformation();
				AvailableIBOBFlightSegment selOut = selectedFlightDTO.getSelectedOndFlight(ondSequence);
				OriginDestinationInformation ondInfo = aaFlightPriceRQ.getOriginDestinationInformation().get(
						selOut.getOndSequence());
				AvailabilityUtil.populateOriginDestinationInformation(ondInfoRS, ondInfo.getOriginLocation(),
						ondInfo.getDestinationLocation(), ondInfo.getDepartureDateTimeStart(), ondInfo.getDepartureDateTimeEnd());
				aaFlightPriceRS.getOriginDestinationInformation().add(ondInfoRS);
				int actualOndSequence = ondInfo.getOndSequence();
				ondInfoRS.setOndSequence(actualOndSequence);

				List<AvailableIBOBFlightSegment> selectedOndFlights = new ArrayList<AvailableIBOBFlightSegment>();
				List<AvailableIBOBFlightSegment> selectedSegFareOndFlights = new ArrayList<AvailableIBOBFlightSegment>();

				selectedOndFlights.add(selectedFlightDTO.getSelectedOndFlight(ondSequence));

				if (selectedFlightDTO.getSelectedOndSegFlight(ondSequence) != null) {
					selectedSegFareOndFlights.add(selectedFlightDTO.getSelectedOndSegFlight(ondSequence));
				}

				boolean ondFlexiSelected = false;
				if (actualOndSequence < aaFlightPriceRQ.getAvailPreferences().getOndFlexiSelected().size()) {
					ondFlexiSelected = aaFlightPriceRQ.getAvailPreferences().getOndFlexiSelected().size() > 0 ? aaFlightPriceRQ
							.getAvailPreferences().getOndFlexiSelected().get(actualOndSequence).isValue() : false;
				}

				AvailabilityUtil.populateFlightSegments(ondInfoRS, selectedOndFlights,
						selectedFlightDTO.getSelectedOndFlight(ondSequence), availabilityDTOCreator, true, false,
						ondFlexiSelected);

				if (!selectedSegFareOndFlights.isEmpty()) {
					AvailabilityUtil.populateFlightSegments(ondInfoRS, selectedSegFareOndFlights,
							selectedFlightDTO.getSelectedOndSegFlight(ondSequence), availabilityDTOCreator, true, true,
							ondFlexiSelected);
				}

			}
		}
	}

	private int getActualOndSequence(int ondSequence) {
		if (aaFlightPriceRQ.getOriginDestinationInformation().size() > ondSequence) {
			return aaFlightPriceRQ.getOriginDestinationInformation().get(ondSequence).getOndSequence();
		}
		return ondSequence;
	}

	/**
	 * @param availableFlightSegment
	 * @return
	 */
	private List<SeatAvailability> buildONDSeatAvailability(AvailableIBOBFlightSegment availableFlightSegment,
			boolean isFlexiSelected) {

		Map<String, LogicalCabinClassDTO> cachedLogicalCCMap = CommonsServices.getGlobalConfig().getAvailableLogicalCCMap();

		List<SeatAvailability> ondSeatAvailability = new ArrayList<SeatAvailability>();
		for (String availableLogicalCabinClass : availableFlightSegment.getAvailableLogicalCabinClasses()) {

			LogicalCabinClassDTO logCabinClassDTO = cachedLogicalCCMap.get(availableLogicalCabinClass);
			Collection<OndFareDTO> colOndFareDTOForLogicalCC = availableFlightSegment
					.getFareChargesSeatsSegmentsDTOs(availableLogicalCabinClass);

			if (colOndFareDTOForLogicalCC == null) {
				continue;
			}

			/*
			 * With new flexi implementation, flexi will be offered as a new logical cabin class in the front-end. But,
			 * in the back-end same logical cabin class will be used with flexi flag. So, before sending available
			 * logical CC & fares to the front-end, dummy logical cabin class will populated based on the requirement.
			 * 
			 * Here, if the flexi is defined as a FOC service for a particular logical cc, only the 'logical cc with
			 * flexi' option will be added. Otherwise separate option will be added 'logical cc with flexi'
			 */
			boolean freeFlexiEnabled = false;
			boolean hasFlexi = FareQuoteUtil.isFlexiAvailable(colOndFareDTOForLogicalCC);
			boolean isFareQuote = aaFlightPriceRQ.getAvailPreferences().isFromFareQuote();
			freeFlexiEnabled = logCabinClassDTO.isFreeFlexiEnabled();

			if (freeFlexiEnabled) {
				// Creating dummy logical cc object for flexi
				SeatAvailability seatAvailability = new SeatAvailability();
				createDummySeatAvailabilityObjectForFlexi(seatAvailability, availableFlightSegment, logCabinClassDTO,
						freeFlexiEnabled, isFareQuote, isFlexiSelected);
				ondSeatAvailability.add(seatAvailability);
			} else {
				boolean isWithFlexiSelected = false;
				String selLang = (availabilityDTOCreator.getAvailableSearchDTO() != null) ? availabilityDTOCreator
						.getAvailableSearchDTO().getPreferredLanguage() : Locale.ENGLISH.getLanguage();
				if (hasFlexi && AppSysParamsUtil.isDisplayFlexiAsSeparteBundle()) {
					// Creating dummy logical cc object for flexi
					SeatAvailability seatAvailability = new SeatAvailability();
					isWithFlexiSelected = createDummySeatAvailabilityObjectForFlexi(seatAvailability, availableFlightSegment,
							logCabinClassDTO, freeFlexiEnabled, isFareQuote, isFlexiSelected);
					if (isWithFlexiSelected) {
						seatAvailability.setFlexiRuleID(FareQuoteUtil.getAvailableFlexiID(colOndFareDTOForLogicalCC));
					}
					ondSeatAvailability.add(seatAvailability);
				}

				SeatAvailability seatAvailability = new SeatAvailability();
				seatAvailability.setLogicalCabinClass(availableLogicalCabinClass);
				seatAvailability.setLogicalCabinClassDescription(logCabinClassDTO.getDescription());
				if (!isWithFlexiSelected
						&& (availableLogicalCabinClass.equals(availableFlightSegment.getSelectedLogicalCabinClass()))
						&& (availableFlightSegment.getSelectedBundledFare() == null)) {
					seatAvailability.setSelected(true);
				}
				seatAvailability.setOndCode(availableFlightSegment.getOndCode());
				seatAvailability.setNestRank(logCabinClassDTO.getNestRank());
				seatAvailability.setComment(logCabinClassDTO.getComment());
				seatAvailability.setImageUrl(OndConvertUtil.generateLogicalCCImageUrl(logCabinClassDTO.getLogicalCCCode(), false,
						selLang));
				seatAvailability.setBundleFareDescriptionInfo(TransformerUtil.transform(logCabinClassDTO
						.getBundleFareDescriptionTemplateDTO()));
				seatAvailability.setWithFlexi(isWithFlexiSelected);
				seatAvailability.setFlexiAvailable(hasFlexi);
				ondSeatAvailability.add(seatAvailability);


			}

		}

		addBundledFaresToSeatAvailability(ondSeatAvailability, availableFlightSegment);

		if (availableFlightSegment.isFlown() && availableFlightSegment.getSelectedBundledFare() != null) {
			retainOnlyBundledServiceOption(ondSeatAvailability, availableFlightSegment.getSelectedBundledFare()
					.getBundledFarePeriodId());
		}

		return ondSeatAvailability;
	}

	private void retainOnlyBundledServiceOption(List<SeatAvailability> ondSeatAvailability, Integer bundledServicePeriodId) {
		if (bundledServicePeriodId != null) {
			Collection<SeatAvailability> retainElements = new ArrayList<SeatAvailability>();
			for (SeatAvailability seatAvailability : ondSeatAvailability) {
				if (bundledServicePeriodId.equals(seatAvailability.getBundledFarePeriodId())) {
					retainElements.add(seatAvailability);
				}
			}

			if (!retainElements.isEmpty()) {
				ondSeatAvailability.retainAll(retainElements);
				if (!ondSeatAvailability.isEmpty()) {
					ondSeatAvailability.iterator().next().setSelected(true);
				}
			}
		}
	}

	private void addBundledFaresToSeatAvailability(List<SeatAvailability> ondSeatAvailability,
			AvailableIBOBFlightSegment availableIBOBFlightSegment) {
		Map<String, List<BundledFareLiteDTO>> availableBundledFares = availableIBOBFlightSegment.getAvailableBundledFares();

		int lastRank = 0;
		if (ondSeatAvailability != null && !ondSeatAvailability.isEmpty()) {
			SeatAvailability lastElement = ondSeatAvailability.get(ondSeatAvailability.size() - 1);
			lastRank = lastElement.getNestRank();
		}

		Collection<OndFareDTO> ondFaresForLogicalCabinClass = availableIBOBFlightSegment
				.getFareChargesSeatsSegmentsDTOs(availableIBOBFlightSegment.getSelectedLogicalCabinClass());
		boolean flexiAvailable = FareQuoteUtil.isFlexiAvailable(ondFaresForLogicalCabinClass);

		if (availableBundledFares != null
				&& availableBundledFares.containsKey(availableIBOBFlightSegment.getSelectedLogicalCabinClass())
				&& !availableIBOBFlightSegment.isFlown()) {

			List<BundledFareLiteDTO> bundledFareLiteDTOs = availableBundledFares.get(availableIBOBFlightSegment
					.getSelectedLogicalCabinClass());
			if (bundledFareLiteDTOs != null && !bundledFareLiteDTOs.isEmpty()) {

				BundledFareDTO bundledFareDTO = availableIBOBFlightSegment.getSelectedBundledFare();

				for (BundledFareLiteDTO bundledFareLiteDTO : bundledFareLiteDTOs) {
					SeatAvailability seatAvailability = new SeatAvailability();
					seatAvailability.setLogicalCabinClass(availableIBOBFlightSegment.getSelectedLogicalCabinClass());
					seatAvailability.setLogicalCabinClassDescription(bundledFareLiteDTO.getBundledFareName());
					seatAvailability.setOndCode(availableIBOBFlightSegment.getOndCode());
					seatAvailability.setNestRank(++lastRank);
					seatAvailability.setComment(bundledFareLiteDTO.getDescription());
					seatAvailability.setWithFlexi(bundledFareLiteDTO.isFlexiIncluded());
					seatAvailability.setFlexiAvailable(flexiAvailable);
					seatAvailability.setBundledFarePeriodId(bundledFareLiteDTO.getBundleFarePeriodId());
					seatAvailability.setBundledFareFee(bundledFareLiteDTO.getPerPaxBundledFee());
					seatAvailability.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
					seatAvailability.setImageUrl(bundledFareLiteDTO.getImageUrl());
					seatAvailability.setBundleFareDescriptionInfo(TransformerUtil.transform(bundledFareLiteDTO
							.getBundleFareDescriptionTemplateDTO()));
					
					if (bundledFareLiteDTO.getBookingClasses() != null) {
						seatAvailability.getBookingCodes().addAll(bundledFareLiteDTO.getBookingClasses());
					}
					seatAvailability.getSegmentBookingClasses().addAll(
							TransformerUtil.convertStringStringMapToLCCList(bundledFareLiteDTO.getSegmentBookingClass()));
					seatAvailability.getBundledFareFreeServices().addAll(
							AAUtils.convertToExternalChargeTypes(bundledFareLiteDTO.getFreeServices()));

					if (bundledFareDTO != null
							&& bundledFareDTO.getBundledFarePeriodId().equals(bundledFareLiteDTO.getBundleFarePeriodId())) {
						seatAvailability.setSelected(true);
					}

					ondSeatAvailability.add(seatAvailability);
				}
			}
		} else if (availableIBOBFlightSegment.getSelectedBundledFare() != null
				&& (availableIBOBFlightSegment.isFlown() || availableIBOBFlightSegment.isUnTouchedOnd())) {
			BundledFareDTO selectedBundledFareDTO = availableIBOBFlightSegment.getSelectedBundledFare();
			SeatAvailability seatAvailability = new SeatAvailability();
			seatAvailability.setLogicalCabinClass(availableIBOBFlightSegment.getSelectedLogicalCabinClass());
			seatAvailability.setLogicalCabinClassDescription(selectedBundledFareDTO.getBundledFareName());
			seatAvailability.setOndCode(availableIBOBFlightSegment.getOndCode());
			seatAvailability.setNestRank(++lastRank);
			seatAvailability.setComment(selectedBundledFareDTO.getBundledFareName());
			seatAvailability.setFlexiAvailable(flexiAvailable);
			seatAvailability.setWithFlexi(selectedBundledFareDTO.isServiceIncluded(EXTERNAL_CHARGES.FLEXI_CHARGES.toString()));
			seatAvailability.setBundledFarePeriodId(selectedBundledFareDTO.getBundledFarePeriodId());
			seatAvailability.setBundledFareFee(selectedBundledFareDTO.getPerPaxBundledFee());
			seatAvailability.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
			seatAvailability.setImageUrl(selectedBundledFareDTO.getImageUrl());
			seatAvailability.setBundleFareDescriptionInfo(TransformerUtil.transform(selectedBundledFareDTO
					.getBundleFareDescriptionTemplateDTO()));
			
			if (selectedBundledFareDTO.getBookingClasses() != null) {
				seatAvailability.getBookingCodes().addAll(selectedBundledFareDTO.getBookingClasses());
			}
			seatAvailability.getBundledFareFreeServices().addAll(
					AAUtils.convertToExternalChargeTypes(selectedBundledFareDTO.getApplicableServiceNames()));
			seatAvailability.setSelected(true);

			ondSeatAvailability.add(seatAvailability);
		}
	}

	private boolean createDummySeatAvailabilityObjectForFlexi(SeatAvailability seatAvailability,
			AvailableIBOBFlightSegment availableFlightSegment, LogicalCabinClassDTO logCabinClassDTO, boolean freeFlexiEnabled,
			boolean isFareQuote, boolean isFlexiSelected) {

		boolean isLogicalCCWithFlexiSelected = false;
		String selLang = (availabilityDTOCreator.getAvailableSearchDTO() != null) ? availabilityDTOCreator
				.getAvailableSearchDTO().getPreferredLanguage() : Locale.ENGLISH.getLanguage();

		seatAvailability.setLogicalCabinClass(logCabinClassDTO.getLogicalCCCode());
		if (freeFlexiEnabled) {
			seatAvailability.setLogicalCabinClassDescription(logCabinClassDTO.getDescription());
		} else {
			seatAvailability.setLogicalCabinClassDescription(logCabinClassDTO.getFlexiDescription());
		}

		/*
		 * if (isFareQuote) { boolean fromFlightSearch =
		 * isSelectedFlightAvailabilitySearch(availableFlightSegment.getOndSequence(),
		 * availabilityDTOCreator.getAvailableSearchDTO().getOndLogicalCabinClassSelection());
		 * 
		 * if (!fromFlightSearch &&
		 * logCabinClassDTO.getLogicalCCCode().equals(availableFlightSegment.getSelectedLogicalCabinClass())) {
		 * isLogicalCCWithFlexiSelected = true; } else if (fromFlightSearch && freeFlexiEnabled &&
		 * logCabinClassDTO.getLogicalCCCode().equals(availableFlightSegment.getSelectedLogicalCabinClass())) {
		 * isLogicalCCWithFlexiSelected = true; } } else if (!isFareQuote && freeFlexiEnabled) {
		 * isLogicalCCWithFlexiSelected = logCabinClassDTO.getLogicalCCCode().equals(
		 * availableFlightSegment.getSelectedLogicalCabinClass()); }
		 */

		// seatAvailability.setSelected(isLogicalCCWithFlexiSelected);
		isLogicalCCWithFlexiSelected = freeFlexiEnabled || isFlexiSelected;
		seatAvailability.setSelected(freeFlexiEnabled || isFlexiSelected);
		seatAvailability.setOndCode(availableFlightSegment.getOndCode());
		seatAvailability.setNestRank(logCabinClassDTO.getNestRank());
		seatAvailability.setWithFlexi(true);
		seatAvailability.setFlexiAvailable(true);
		seatAvailability.setComment(logCabinClassDTO.getFlexiComment());
		seatAvailability
				.setImageUrl(OndConvertUtil.generateLogicalCCImageUrl(logCabinClassDTO.getLogicalCCCode(), true, selLang));

		return isLogicalCCWithFlexiSelected;
	}

	private PriceInfo getPriceInfoFromFareType(FareSegChargeTO fareSegChargeTO, Collection<OndFareDTO> ondFareDTOList,
			boolean isFixedFare) throws ModuleException {
		PriceInfo priceInfo = new PriceInfo();

		priceInfo.setPricingSource(PricingSource.PRIVATE);
		if (aaFlightPriceRQ.isOwnAirlineFareQuote()) {
			priceInfo.setFareSegmentChargeTO(AvailabilityUtil.populateFareSegChargeTO(fareSegChargeTO));
		}

		HashMap<String, PerPaxPriceInfo> perPaxPriceTemplateMap = new HashMap<String, PerPaxPriceInfo>();
		List<AirTraveler> traverList = aaFlightPriceRQ.getTravelerInfoSummary().getAirTravelerAvail().getAirTraveler();
		Map<Integer, Integer> actualOndSequence = new HashMap<Integer, Integer>();
		for (OndFareDTO ondFareDTO : ondFareDTOList) {
			actualOndSequence.put(ondFareDTO.getOndSequence(), getActualOndSequence(ondFareDTO.getOndSequence()));
		}
		for (AirTraveler airTraveler : traverList) {
			String paxType = PaxTypeUtils.convertLCCPaxCodeToAAPaxCodes(airTraveler.getPassengerType());
			if (paxType != null) {
				PerPaxPriceInfo paxPriceTemplete = perPaxPriceTemplateMap.get(paxType);
				if (paxPriceTemplete == null) {
					Date now = new Date();
					paxPriceTemplete = PriceCalculationUtil.createPerPaxPriceInfo(ondFareDTOList, paxType, isFixedFare, now,
							actualOndSequence);
					perPaxPriceTemplateMap.put(paxType, paxPriceTemplete);

					paxPriceTemplete.setTravelerRefNumber(airTraveler.getTravelerRefNumber());
					paxPriceTemplete.setPassengerType(airTraveler.getPassengerType());
					priceInfo.getPerPaxPriceInfo().add(paxPriceTemplete);
				} else {
					PerPaxPriceInfo lightClone = CloneUtil.lightClone(paxPriceTemplete);
					lightClone.setTravelerRefNumber(airTraveler.getTravelerRefNumber());
					priceInfo.getPerPaxPriceInfo().add(lightClone);
				}
			}
		}

		// set on hold information
		boolean isOnHoldRestricted = WebplatformUtil.isOnholdRestricted(ondFareDTOList);
		Date holdReleaseTimestamp = null;
		if (!isOnHoldRestricted) {
			holdReleaseTimestamp = WebplatformUtil.getReleaseTimeStamp(ReleaseTimeUtil.getFlightDepartureInfo(ondFareDTOList),
					(Collection) AASessionManager.getInstance().getCurrUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS),
					true, aaFlightPriceRQ.getAaPos().getMarketingAgentCode(),
					AAUtils.getAppIndicator(aaFlightPriceRQ.getAvailPreferences().getAppIndicator()),
					ReleaseTimeUtil.getOnHoldReleaseTimeDTOFromOndFares(ondFareDTOList));
			if (holdReleaseTimestamp != null) {
				priceInfo.setOnholdReleaseTimeZulu(holdReleaseTimestamp);
			} else {
				isOnHoldRestricted = true;
			}
		}
		priceInfo.setOnholdRestricted(isOnHoldRestricted);

		Object[] arrObj = WebplatformUtil.getRestrictedBufferTime((ReleaseTimeUtil.getFlightDepartureInfo(ondFareDTOList)));
		if (arrObj != null) {
			Date ohdBufferStart = (Date) arrObj[0];
			Date OhdBufferStop = (Date) arrObj[1];
			priceInfo.setOnholdBufferTimeZulu(ohdBufferStart);
			priceInfo.setFlightCloseTimeZulu(OhdBufferStop);
		}

		for (OndFareDTO ondFareDTO : ondFareDTOList) {
			IntegerIntegerMap mapObj = new IntegerIntegerMap();
			mapObj.setKey(getActualOndSequence(ondFareDTO.getOndSequence()));
			mapObj.setValue(ondFareDTO.getFareType());
			priceInfo.getOndFareType().add(mapObj);
		}
		priceInfo.setTotalServiceTaxAmount(fareSegChargeTO.getTotalServiceTaxAmount());
		return priceInfo;
	}

	private static boolean isSelectedFlightAvailabilitySearch(int ondSeq, Map<Integer, String> logicalCabinClassSelection) {
		boolean isAvailSearch = true;

		if (logicalCabinClassSelection != null) {
			if (logicalCabinClassSelection.containsKey(ondSeq) && logicalCabinClassSelection.get(ondSeq) != null
					&& !logicalCabinClassSelection.isEmpty()) {
				return false;
			}
		}

		return isAvailSearch;
	}

}
