package com.isa.thinair.aaservices.core.session;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class AASessionCleaningQuartzJob implements Job {

	private static Log log = LogFactory.getLog(AASessionCleaningQuartzJob.class);

	public AASessionCleaningQuartzJob() {
	}

	public void execute(JobExecutionContext context) throws JobExecutionException {

		String jobName = context.getJobDetail().getKey().getName();

		log.info("AASessionCleaningQuartzJob: " + jobName + " execution start at " + new Date());

		AASessionManager.getInstance().removeExpiredUserSessions();

		log.info("AASessionCleaningQuartzJob: " + jobName + " execution end at " + new Date());
	}
}