package com.isa.thinair.aaservices.core.bl.reservation;

import com.isa.connectivity.profiles.maxico.v1.common.dto.Address;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AdminInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AgentContactInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AirReservation;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AirReservationSummary;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AirTraveler;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AirTravelerAvail;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AirportServiceRequest;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AlertInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AppIndicator;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Balance;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BaseFare;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BookBaggageRequest;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BookMealRequest;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BookedFlightAlertInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.CarrierOndGroup;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ChargeAdjustmentTypePrivileges;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ContactInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Country;
import com.isa.connectivity.profiles.maxico.v1.common.dto.CurrencyCodeGroup;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Discount;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Eticket;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FareType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Fee;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FeeType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FormOfIdentification;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Fulfillment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.InsuranceRequest;
import com.isa.connectivity.profiles.maxico.v1.common.dto.LCCAutoCancellationInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.LCCBundledFare;
import com.isa.connectivity.profiles.maxico.v1.common.dto.LCCBundledFareFreeService;
import com.isa.connectivity.profiles.maxico.v1.common.dto.LCCPromotionInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.LCCStationContactDTO;
import com.isa.connectivity.profiles.maxico.v1.common.dto.LoadReservationOptions;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ModifcationParamRQ;
import com.isa.connectivity.profiles.maxico.v1.common.dto.NoShowRefundableInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OldFareDetails;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OriginDestinationOption;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OriginDestinationOptions;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PassengerType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PassengerTypeQuantity;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaxOndFlexibility;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaymentDetails;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PerPaxPriceInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PersonName;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PriceInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PricingSource;
import com.isa.connectivity.profiles.maxico.v1.common.dto.RefundableChargeDetail;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SeatRequest;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SegmentConnTimeInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SegmentStatus;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SpecialReqDetails;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SpecialServiceRequest;
import com.isa.connectivity.profiles.maxico.v1.common.dto.StringDecimalMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.StringRefundableDetailsMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Surcharge;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Tax;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TaxInvoice;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Telephone;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Ticket;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TicketModifiableStates;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TicketingInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TicketingStatus;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TravelerFulfillment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TravelerInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.UserNote;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAirBookRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAReadRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAReservation;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.aaservices.core.session.AASessionManager;
import com.isa.thinair.aaservices.core.session.AAUserSession;
import com.isa.thinair.aaservices.core.util.AAChargeAdjustmentTypePrivilegeUtil;
import com.isa.thinair.aaservices.core.util.AACustomerFulfillmentPopulator;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAModificationParamUtil;
import com.isa.thinair.aaservices.core.util.AAPaymentDetailsFactory;
import com.isa.thinair.aaservices.core.util.AAPnrPaxChargeHolder;
import com.isa.thinair.aaservices.core.util.AAReservationUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.AAUtils;
import com.isa.thinair.aaservices.core.util.PaxTypeUtils;
import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareTypes;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants.ChargeCodes;
import com.isa.thinair.airmaster.api.dto.CachedAirportDTO;
import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airpricing.api.dto.ChargeTO;
import com.isa.thinair.airpricing.api.dto.FareTO;
import com.isa.thinair.airpricing.api.dto.FaresAndChargesTO;
import com.isa.thinair.airpricing.api.model.Charge;
import com.isa.thinair.airpricing.api.model.FareRule;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.ModificationParamRQInfo;
import com.isa.thinair.airproxy.api.utils.FindReservationUtil;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.NameChangeUtil;
import com.isa.thinair.airreservation.api.dto.CreditInfoDTO;
import com.isa.thinair.airreservation.api.dto.PaxSSRDTO;
import com.isa.thinair.airreservation.api.dto.RefundableChargeDetailDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.StationContactDTO;
import com.isa.thinair.airreservation.api.dto.baggage.PaxBaggageTO;
import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.airreservation.api.dto.flexi.ReservationPaxOndFlexibilityDTO;
import com.isa.thinair.airreservation.api.dto.meal.PaxMealTO;
import com.isa.thinair.airreservation.api.dto.seatmap.PaxSeatTO;
import com.isa.thinair.airreservation.api.mediators.ReservationValidationUtils;
import com.isa.thinair.airreservation.api.model.AutoCancellationInfo;
import com.isa.thinair.airreservation.api.model.ExternalFlightSegment;
import com.isa.thinair.airreservation.api.model.ExternalPnrSegment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationAdminInfo;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationInsurance;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxAdditionalInfo;
import com.isa.thinair.airreservation.api.model.ReservationPaxFare;
import com.isa.thinair.airreservation.api.model.ReservationPaxFareSegment;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndCreditPromotion;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.alerting.api.model.Alert;
import com.isa.thinair.alerting.api.service.AlertingBD;
import com.isa.thinair.auditor.api.dto.ReservationModificationDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.dto.SSRInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.SSRUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.commons.core.util.XMLDataTypeUtil;
import com.isa.thinair.promotion.api.to.PromotionCriteriaTO;
import com.isa.thinair.promotion.api.to.bundledFare.BundledFareFreeServiceTO;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants.DiscountApplyTo;
import com.isa.thinair.webplatform.api.util.AuthorizationUtil;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webplatform.api.util.PrivilegesKeys;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.webplatform.core.constants.FareTypeCodes;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

public class AASearchReservationByPnrBL {

	// Logger
	private static Log log = LogFactory.getLog(AASearchReservationByPnrBL.class);

	// Request & Response objects
	private AAReadRQ aaReadRQ;
	private final AAAirBookRS airBookRS = new AAAirBookRS();

	// Information pertaining to the retrieved reservation
	private Reservation reservation;

	private AirportBD airportBD;

	private String thisAirlineCode;
	private CurrencyCodeGroup baseCurrencyCodeGroup;
	/**
	 * For any given reservation the number and type of the passengers is always the same.
	 */
	private List<PassengerTypeQuantity> paxTypeQuantityForThisRes;

	/**
	 * Set the Request object (to be used in the {@link AASearchReservationByPnrBL#execute execute} method)
	 * 
	 * @param aaReadRQ
	 */
	public void setRequest(AAReadRQ aaReadRQ) {
		this.aaReadRQ = aaReadRQ;
	}

	/**
	 * Initializes the objects that will be used by the Response object
	 */
	private void initializeAAAirBookRS() {
		airBookRS.setAaAirReservation(new AAReservation());
		airBookRS.getAaAirReservation().setAirReservation(new AirReservation());
		airBookRS.setResponseAttributes(new AAResponseAttributes());
		thisAirlineCode = AppSysParamsUtil.getDefaultCarrierCode();
		airportBD = AAServicesModuleUtils.getAirportBD();
	}

	/**
	 * Execute some logic based on the Request object and process and return the Response object
	 * 
	 * @return AAAirBookRS
	 */
	public AAAirBookRS execute() {

		// Initialize Return object
		initializeAAAirBookRS();

		if (log.isDebugEnabled()) {
			log.debug("[" + thisAirlineCode + "-AAServices] has been called by LCC, PNR = [" + aaReadRQ.getPnr() + "]");
		}

		// Prepare Request object
		LCCClientPnrModesDTO pnrModesDTO = populatePnrModesDTO();

		try {
			reservation = AAServicesModuleUtils.getReservationBD().getReservation(pnrModesDTO, null);

			// Populate HeaderInfo
			airBookRS.setHeaderInfo(AAUtils.populateHeaderInfo(null));

			// Populate AAReservation
			populateAAReservation();

			List<String> modificationParams = AAModificationParamUtil.getModificationParams(getModificationParamRQInfo(),
					ReservationApiUtils.getNonDepartedFirstSegmentDate(reservation), true, reservation.getNameChangeCount());
			if (modificationParams != null) {
				airBookRS.getModificationParams().addAll(modificationParams);
			}

			// Charge adjustment privilge id - for dynamic privilege checking
			ChargeAdjustmentTypePrivileges chargeAdjustmentTypePrivileges = AAChargeAdjustmentTypePrivilegeUtil
					.getChargeAdjustmentPrivileges();
			if (chargeAdjustmentTypePrivileges != null) {
				airBookRS.getChargeAdjustmentPrivileges().add(chargeAdjustmentTypePrivileges);
			}

		} catch (Exception e) {
			log.error("ERROR", e);
			AAExceptionUtil.addAAErrrors(airBookRS.getResponseAttributes().getErrors(), e);
		} finally {
			/*
			 * Add Success. Success is an initialized empty object to signify that all is OK. If there are errors (from
			 * the executed previously code) then we return a null object, signifying that all is not OK. [note warnings
			 * are not considered errors]
			 */
			int numOfErrors = airBookRS.getResponseAttributes().getErrors().size();
			AASuccess successType = (numOfErrors == 0 ? new AASuccess() : null);
			airBookRS.getResponseAttributes().setSuccess(successType);
		}

		return airBookRS;
	}

	/**
	 * Populate PnrModesDTO using {@link com.isa.connectivity.profiles.maxico.v1.required.dto.AAReadRQ AAReadRQ}
	 * 
	 * @return PnrModesDTO
	 */
	private LCCClientPnrModesDTO populatePnrModesDTO() {

		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(aaReadRQ.getPnr());
		pnrModesDTO.setPreferredLanguage(aaReadRQ.getSelectedLanguage());
		pnrModesDTO.setGroupPNR(null); // since we have PNR we should set
										// OriginatorPnr to empty string

		if (aaReadRQ.getAaPos() != null) {
			pnrModesDTO.setCallingAirlineCode(aaReadRQ.getAaPos().getCallingAirLineCode());
		}
		LoadReservationOptions loadReservationOptions = aaReadRQ.getLoadReservationOptions();

		// Options
		pnrModesDTO.setLoadFares(loadReservationOptions.isLoadFares());
		pnrModesDTO.setLoadSegView(loadReservationOptions.isLoadSegView());
		pnrModesDTO.setLoadSegViewBookingTypes(loadReservationOptions.isLoadSegViewBookingTypes());
		pnrModesDTO.setLoadSegViewFareCategoryTypes(loadReservationOptions.isLoadSegViewFareCategoryTypes());
		pnrModesDTO.setLoadSegViewReturnGroupId(loadReservationOptions.isLoadSegViewReturnGroupId());
		pnrModesDTO.setLoadPaxAvaBalance(loadReservationOptions.isLoadPaxAvaBalance());
		pnrModesDTO.setLoadLocalTimes(loadReservationOptions.isLoadLocalTimes());
		pnrModesDTO.setLoadLastUserNote(loadReservationOptions.isLoadLastUserNote());
		pnrModesDTO.setLoadOndChargesView(loadReservationOptions.isLoadOndChargesView());
		pnrModesDTO.setRecordAudit(loadReservationOptions.isRecordAudit());
		pnrModesDTO.setLoadSeatingInfo(loadReservationOptions.isLoadSeatingInfo());
		pnrModesDTO.setLoadMealInfo(loadReservationOptions.isLoadMealInfo());
		pnrModesDTO.setLoadBaggageInfo(loadReservationOptions.isLoadBaggageInfo());
		pnrModesDTO.setLoadSSRInfo(loadReservationOptions.isLoadSSRInfo());
		pnrModesDTO.setLoadPaxPaymentOndBreakdownView(loadReservationOptions.isLoadPaxPaymentOndBreakdownView());
		pnrModesDTO.setLoadExternalPaxPayments(loadReservationOptions.isLoadExternalPaxPayments());
		pnrModesDTO.setLoadEtickets(loadReservationOptions.isLoadEtickets());
		pnrModesDTO.setLoadOriginCountry(loadReservationOptions.isLoadOriginCountryCode());
		pnrModesDTO.setLoadRefundableTaxInfo(loadReservationOptions.isLoadRefundableChargeDetails());
		pnrModesDTO.setLoadGOQUOAmounts(loadReservationOptions.isLoadGOQUOAmounts());

		if (!loadReservationOptions.isLoadFares()) {
			log.error("==============LOAD Reservation without Fare Details : " + aaReadRQ.getPnr() + "============");
		}
		return pnrModesDTO;
	}

	/**
	 * Populate AirReservation using {@lin8k com.isa.thinair.airreservation.api.model.Reservation Reservation}
	 * 
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	private void populateAAReservation() throws ModuleException, WebservicesException {
		// Populate AAReservation
		AAReservation aaReservation = new AAReservation();
		aaReservation.setPnr(reservation.getPnr());
		aaReservation.setGroupPnr(reservation.getOriginatorPnr());
		aaReservation.setAirReservation(populateAirReservation());

		airBookRS.setAaAirReservation(aaReservation);
	}

	/**
	 * Populate AirReservation using {@link com.isa.thinair.airreservation.api.model.Reservation Reservation}
	 * 
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	private AirReservation populateAirReservation() throws ModuleException, WebservicesException {

		// This airline's base currency (this AA instance's base currency)
		baseCurrencyCodeGroup = FindReservationUtil.getDefaultCurrencyCodeGroup();

		// All Pax (Type & Quantity) for this PNR
		populatePassengerTypeQuantityForThisReservation();

		boolean onlyInfantBooking = false;
		boolean isInfantPaymentSeparated = reservation.isInfantPaymentRecordedWithInfant();
		if (reservation.getPassengers().size() > 0) {
			int noOfPax = reservation.getPassengers().size();
			int noOfInfants = 0;
			for (ReservationPax reservationPax : reservation.getPassengers()) {
				if (reservationPax.getPaxType().equals(PaxTypeTO.INFANT)) {
					noOfInfants += 1;
				}
			}

			if (noOfInfants == noOfPax) {
				onlyInfantBooking = true;
			}
		}

		// Populate AirReservation (Below setters are order dependent, do not
		// rearrange)
		AirReservation airReservation = new AirReservation();
		airReservation.setVersion(String.valueOf(reservation.getVersion()));

		// TODO verify the disable following information for dry booking. May
		// need when merging
		if (ReservationInternalConstants.DummyBooking.NO == (reservation.getDummyBooking())) {
			airReservation.setAirItinerary(populateAirItinerary());
			airReservation.setExternalAirItinerary(populateExternalAirItinerary());
			airReservation.setAirReservationSummary(populateAirReservationSummary());
			airReservation.setContactInfo(populateContactInfo());
			airReservation.setBookingCategory(reservation.getBookingCategory());
			airReservation.setOriginCountryOfCall(reservation.getOriginCountryOfCall());
		}

		airReservation.setStatus(reservation.getStatus());

		airReservation.setModifiableReservation(
				reservation.getModifiableReservation() == ReservationInternalConstants.Modifiable.YES ? true : false);

		if (!onlyInfantBooking || isInfantPaymentSeparated) {
			airReservation.setFulfillment(populateFulfillment());
		} else {
			airReservation.setFulfillment(new Fulfillment());
		}
		if (ReservationInternalConstants.DummyBooking.NO == (reservation.getDummyBooking())) {
			airReservation.setPriceInfo(populatePriceInfo());
			airReservation.setTicketing(populateTicketingInfo());
			airReservation.setTravelerInfo(populateTravelerInfo());
			airReservation.setLastUserNote(populateLastUserNote());
			airReservation.getReservationPaxOndFlexibility().addAll(populatePaxOndFlexibility());
			airReservation.getAlertInfo().addAll(populateAlertInfo());
			airReservation.getRefundableChargeDetails().addAll(populateRefundableChargeDetails());
			populateReservationCarrierGrouping(airReservation.getCarrierOndGrouping());
			airReservation.setLccAutoCancellationInfo(populateLccAutoCnxInfo(reservation.getAutoCancellationInfo()));
			airReservation.setTotalGoquoAmount(reservation.getTotalGoquoAmount());
			airReservation.getLccStationContactDTOs().addAll(populateStationContactDTOs(reservation.getStations()));
			airReservation.setLccPromoInfo(populatePromoInfo());
			airReservation.getLccBundledFares().addAll(populateBundledFareInfo());
		}
		airReservation.setNameChangeCutoffTime(populateNameChangeCutoffTime());

		airReservation.setNameChangeThresholdTimePerStation(
				NameChangeUtil.populateStationWiseNameChangeInfo(AAServicesModuleUtils.getLocationServiceBD(), reservation));
		airReservation.setNameChangeCount(reservation.getNameChangeCount());
		airReservation.setGdsId(reservation.getGdsId());
		airReservation.setExternalPos(reservation.getExternalPos());
		airReservation.setExternalRecordLocator(reservation.getExternalRecordLocator());
		airReservation.setActionedByIBE(AAServicesModuleUtils.getAirproxyReservationBD().hasActionedByIBE(reservation.getPnr()));		
		airReservation.setInfantPaymentSeparated(isInfantPaymentSeparated);
		airReservation.getNoShowRefundableInfo().addAll(populateNoShowRefundableInfo());
		airReservation.getTaxInvoicesList().addAll(populateTaxInvoiceList());
		
		return airReservation;
	}
	
	private List<TaxInvoice> populateTaxInvoiceList(){
		List<TaxInvoice> taxInvoicesList = new ArrayList<TaxInvoice>();
		if(reservation.getTaxInvoicesList() != null && !reservation.getTaxInvoicesList().isEmpty()){
			for (int i=0;i<reservation.getTaxInvoicesList().size();i++){
				TaxInvoice taxInvoice = new TaxInvoice();
				
				if (reservation.getTaxInvoicesList().get(i).getDateOfIssue() != null){
					taxInvoice.setDateOfIssue(reservation.getTaxInvoicesList().get(i).getDateOfIssue());
				} if (reservation.getTaxInvoicesList().get(i).getNonTaxableAmount() != null){
					taxInvoice.setNonTaxableAmount(reservation.getTaxInvoicesList().get(i).getNonTaxableAmount());
				} if (reservation.getTaxInvoicesList().get(i).getOriginalTaxInvoiceId() != null){
					taxInvoice.setOriginalTaxInvoiceId(reservation.getTaxInvoicesList().get(i).getOriginalTaxInvoiceId());
				} if (reservation.getTaxInvoicesList().get(i).getTaxableAmount() != null){
					taxInvoice.setTaxableAmount(reservation.getTaxInvoicesList().get(i).getTaxableAmount());
				} if (reservation.getTaxInvoicesList().get(i).getTaxInvoiceId() != null){
					taxInvoice.setTaxInvoiceId(reservation.getTaxInvoicesList().get(i).getTaxInvoiceId());
				} if (reservation.getTaxInvoicesList().get(i).getTaxRate1Amount() != null){
					taxInvoice.setTaxRate1Amount(reservation.getTaxInvoicesList().get(i).getTaxRate1Amount());
				} if (reservation.getTaxInvoicesList().get(i).getTaxRate1Id() != null){
					taxInvoice.setTaxRate1Id(reservation.getTaxInvoicesList().get(i).getTaxRate1Id());
				} if (reservation.getTaxInvoicesList().get(i).getTaxRate2Amount() != null){
					taxInvoice.setTaxRate2Amount(reservation.getTaxInvoicesList().get(i).getTaxRate2Amount());
				} if (reservation.getTaxInvoicesList().get(i).getTaxRate2Id() != null){
					taxInvoice.setTaxRate2Id(reservation.getTaxInvoicesList().get(i).getTaxRate2Id());
				} if (reservation.getTaxInvoicesList().get(i).getTaxRate3Amount() != null){
					taxInvoice.setTaxRate3Amount(reservation.getTaxInvoicesList().get(i).getTaxRate3Amount());
				} if (reservation.getTaxInvoicesList().get(i).getTaxRate3Id() != null){
					taxInvoice.setTaxRate3Id(reservation.getTaxInvoicesList().get(i).getTaxRate3Id());
				} if (reservation.getTaxInvoicesList().get(i).getTnxSeq() != null){
					taxInvoice.setTnxSeq(reservation.getTaxInvoicesList().get(i).getTnxSeq());
				} if (reservation.getTaxInvoicesList().get(i).getTotalDiscount() != null){
					taxInvoice.setTotalDiscount(reservation.getTaxInvoicesList().get(i).getTotalDiscount());
				} if (reservation.getTaxInvoicesList().get(i).getInvoiceType() != null){
					taxInvoice.setInvoiceType(reservation.getTaxInvoicesList().get(i).getInvoiceType());
				} if (reservation.getTaxInvoicesList().get(i).getCurrencyCode() != null){
					taxInvoice.setCurrencyCode(reservation.getTaxInvoicesList().get(i).getCurrencyCode());
				} if (reservation.getTaxInvoicesList().get(i).getStateCode() != null){
					taxInvoice.setStateCode(reservation.getTaxInvoicesList().get(i).getStateCode());
				} if (reservation.getTaxInvoicesList().get(i).getTaxRegistrationNumber() != null){
					taxInvoice.setTaxRegistrationNumber(reservation.getTaxInvoicesList().get(i).getTaxRegistrationNumber());
				} if (reservation.getTaxInvoicesList().get(i).getOriginalPnr() != null){
					taxInvoice.setOriginalPnr(reservation.getTaxInvoicesList().get(i).getOriginalPnr());
				} if (reservation.getTaxInvoicesList().get(i).getNameOfTheAirline() != null){
					taxInvoice.setNameOfTheAirline(reservation.getTaxInvoicesList().get(i).getNameOfTheAirline());
				} if (reservation.getTaxInvoicesList().get(i).getAirlineOfficeSTAddress1() != null){
					taxInvoice.setAirlineOfficeSTAddress1(reservation.getTaxInvoicesList().get(i).getAirlineOfficeSTAddress1());
				} if (reservation.getTaxInvoicesList().get(i).getAirlineOfficeSTAddress2() != null){
					taxInvoice.setAirlineOfficeSTAddress2(reservation.getTaxInvoicesList().get(i).getAirlineOfficeSTAddress2()); 
				} if (reservation.getTaxInvoicesList().get(i).getAirlineOfficeCity() != null){
					taxInvoice.setAirlineOfficeCity(reservation.getTaxInvoicesList().get(i).getAirlineOfficeCity());
				} if (reservation.getTaxInvoicesList().get(i).getAirlineOfficeCountryCode() != null){
					taxInvoice.setAirlineOfficeCountryCode(reservation.getTaxInvoicesList().get(i).getAirlineOfficeCountryCode());
				} if (reservation.getTaxInvoicesList().get(i).getGstinForInvoiceState() != null){
					taxInvoice.setGstinForInvoiceState(reservation.getTaxInvoicesList().get(i).getGstinForInvoiceState());
				} if (reservation.getTaxInvoicesList().get(i).getNameOfTheRecipient() != null){
					taxInvoice.setNameOfTheRecipient(reservation.getTaxInvoicesList().get(i).getNameOfTheRecipient()); 
				} if (reservation.getTaxInvoicesList().get(i).getRecipientStreetAddress1() != null){
					taxInvoice.setRecipientStreetAddress1(reservation.getTaxInvoicesList().get(i).getRecipientStreetAddress1());
				} if (reservation.getTaxInvoicesList().get(i).getRecipientStreetAddress2() != null){
					taxInvoice.setRecipientStreetAddress2(reservation.getTaxInvoicesList().get(i).getRecipientStreetAddress2()); 
				} if (reservation.getTaxInvoicesList().get(i).getRecipientCity() != null){
					taxInvoice.setRecipientCity(reservation.getTaxInvoicesList().get(i).getRecipientCity());
				} if (reservation.getTaxInvoicesList().get(i).getRecipientCountryCode() != null){
					taxInvoice.setRecipientCountryCode(reservation.getTaxInvoicesList().get(i).getRecipientCountryCode());
				} if (reservation.getTaxInvoicesList().get(i).getGstinOfTheRecipient() != null){
					taxInvoice.setGstinOfTheRecipient(reservation.getTaxInvoicesList().get(i).getGstinOfTheRecipient()); 
				} if (reservation.getTaxInvoicesList().get(i).getStateOfTheRecipient() != null){
					taxInvoice.setStateOfTheRecipient(reservation.getTaxInvoicesList().get(i).getStateOfTheRecipient()); 
				} if (reservation.getTaxInvoicesList().get(i).getStateCodeOfTheRecipient() != null){
					taxInvoice.setStateCodeOfTheRecipient(reservation.getTaxInvoicesList().get(i).getStateCodeOfTheRecipient());
				} if (reservation.getTaxInvoicesList().get(i).getAccountCodeOfService() != null){
					taxInvoice.setAccountCodeOfService(reservation.getTaxInvoicesList().get(i).getAccountCodeOfService());
				} if (reservation.getTaxInvoicesList().get(i).getDescriptionOfService() != null){
					taxInvoice.setDescriptionOfService(reservation.getTaxInvoicesList().get(i).getDescriptionOfService());
				} if (reservation.getTaxInvoicesList().get(i).getPlaceOfSupply() != null){
					taxInvoice.setPlaceOfSupply(reservation.getTaxInvoicesList().get(i).getPlaceOfSupply());
				} if (reservation.getTaxInvoicesList().get(i).getPlaceOfSupplyState() != null){
					taxInvoice.setPlaceOfSupplyState(reservation.getTaxInvoicesList().get(i).getPlaceOfSupplyState());
				} if (reservation.getTaxInvoicesList().get(i).getDigitalSignForInvoiceState() != null){
					taxInvoice.setDigitalSignForInvoiceState(reservation.getTaxInvoicesList().get(i).getDigitalSignForInvoiceState());
				} if (reservation.getTaxInvoicesList().get(i).getTotalValueOfService() != null){
					taxInvoice.setTotalValueOfService(reservation.getTaxInvoicesList().get(i).getTotalValueOfService()); 
				} if (true){
					taxInvoice.setTaxRate1Percentage(new BigDecimal(reservation.getTaxInvoicesList().get(i).getTaxRate1Percentage())); 
				} if (true){
					taxInvoice.setTaxRate2Percentage(new BigDecimal(reservation.getTaxInvoicesList().get(i).getTaxRate2Percentage())); 
				} if (true){
					taxInvoice.setTaxRate3Percentage(new BigDecimal(reservation.getTaxInvoicesList().get(i).getTaxRate3Percentage()));
				} if (reservation.getTaxInvoicesList().get(i).getTotalTaxAmount() != null){
					taxInvoice.setTotalTaxAmount(reservation.getTaxInvoicesList().get(i).getTotalTaxAmount());
				} if (reservation.getTaxInvoicesList().get(i).getDateOfIssueOriginalTaxInvoice() != null){
					taxInvoice.setDateOfIssueOriginalTaxInvoice(reservation.getTaxInvoicesList().get(i).getDateOfIssueOriginalTaxInvoice());
				}
				if (reservation.getTaxInvoicesList().get(i).getTaxInvoiceNumber() != null){
					taxInvoice.setTaxInvoiceNumber(reservation.getTaxInvoicesList().get(i).getTaxInvoiceNumber());
				}
				if (reservation.getTaxInvoicesList().get(i).getOriginalTaxInvoiceNumber() != null){
					taxInvoice.setOriginalTaxInvoiceNumber(reservation.getTaxInvoicesList().get(i).getOriginalTaxInvoiceNumber());
				}
				if (reservation.getTaxInvoicesList().get(i).getJourneyOND() != null) {
					taxInvoice.setJourneyDetails(reservation.getTaxInvoicesList().get(i).getJourneyOND());
				}
				taxInvoicesList.add(taxInvoice);
			}
		}
		return taxInvoicesList;
	}

	private LCCAutoCancellationInfo populateLccAutoCnxInfo(AutoCancellationInfo autoCancellationInfo) {
		if (autoCancellationInfo != null) {
			LCCAutoCancellationInfo lccAutoCancellationInfo = new LCCAutoCancellationInfo();
			lccAutoCancellationInfo.setCancellationId(autoCancellationInfo.getAutoCancellationId());
			lccAutoCancellationInfo.setCancellationType(autoCancellationInfo.getCancellationType());
			lccAutoCancellationInfo.setExpireOn(autoCancellationInfo.getExpireOn());
			return lccAutoCancellationInfo;
		}
		return null;
	}

	private List<LCCStationContactDTO> populateStationContactDTOs(Set<StationContactDTO> stationContacts) {
		List<LCCStationContactDTO> lccLCCStationContactDTOList = new ArrayList<LCCStationContactDTO>();
		if (stationContacts != null && stationContacts.size() > 0) {
			for (StationContactDTO stationContactDTO : stationContacts) {
				LCCStationContactDTO lccStationContactDTO = new LCCStationContactDTO();
				lccStationContactDTO.setStationCode(stationContactDTO.getStationCode());
				lccStationContactDTO.setStationName(stationContactDTO.getStationName());
				lccStationContactDTO.setStationContact(stationContactDTO.getStationContact());
				lccStationContactDTO.setStationTelephone(stationContactDTO.getStationTelephone());
				lccLCCStationContactDTOList.add(lccStationContactDTO);
			}
		}
		return lccLCCStationContactDTOList;
	}

	/**
	 * Populate the reservation's Pax Ond Flexibility information.
	 * 
	 * @throws ModuleException
	 */
	private List<PaxOndFlexibility> populatePaxOndFlexibility() throws ModuleException {
		List<PaxOndFlexibility> paxOndFlexibilityList = new ArrayList<PaxOndFlexibility>();
		Map<Integer, Collection<ReservationPaxOndFlexibilityDTO>> flexibilitiesMap = reservation.getPaxOndFlexibilities();

		if (flexibilitiesMap != null) {
			for (ReservationPax reservationPax : reservation.getPassengers()) {
				for (ReservationPaxFare reservationPaxFare : reservationPax.getPnrPaxFares()) {
					Collection<ReservationPaxOndFlexibilityDTO> paxFlexi = flexibilitiesMap
							.get(reservationPaxFare.getPnrPaxFareId());
					if (paxFlexi != null) {
						for (ReservationPaxOndFlexibilityDTO reservationPaxOndFlexibilityDTO : paxFlexi) {
							PaxOndFlexibility paxOndFlexibility = new PaxOndFlexibility();
							paxOndFlexibility.setAvailableCount(reservationPaxOndFlexibilityDTO.getAvailableCount());
							paxOndFlexibility.setCutOverBufferInMins(reservationPaxOndFlexibilityDTO.getCutOverBufferInMins());
							paxOndFlexibility.setDescription(reservationPaxOndFlexibilityDTO.getDescription());
							paxOndFlexibility.setFlexibilityTypeId(reservationPaxOndFlexibilityDTO.getFlexibilityTypeId());
							paxOndFlexibility.setFlexiRateId(reservationPaxOndFlexibilityDTO.getFlexibilityTypeId());
							paxOndFlexibility.setPpfId(reservationPaxOndFlexibilityDTO.getPpfId());
							paxOndFlexibility.setPpOndFlxId(reservationPaxOndFlexibilityDTO.getPpOndFlxId());
							paxOndFlexibility.setStatus(reservationPaxOndFlexibilityDTO.getStatus());
							paxOndFlexibility.setUtilizedCount(reservationPaxOndFlexibilityDTO.getUtilizedCount());
							paxOndFlexibility.setFlexiRuleID(reservationPaxOndFlexibilityDTO.getFlexiRuleID());
							paxOndFlexibilityList.add(paxOndFlexibility);
						}
					}
				}
			}
		}
		return paxOndFlexibilityList;
	}

	/**
	 * Populate the reservation's carrier ond grouping information.
	 * 
	 * @param carrierOndGrouping
	 * @throws ModuleException
	 */
	private void populateReservationCarrierGrouping(List<CarrierOndGroup> carrierOndGrouping) throws ModuleException {

		Map<Collection<Integer>, CarrierOndGroup> ondWisePnrPaxFareIds = new HashMap<Collection<Integer>, CarrierOndGroup>();
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		String status = null;

		for (ReservationPax reservationPax : reservation.getPassengers()) {
			for (ReservationPaxFare reservationPaxFare : reservationPax.getPnrPaxFares()) {
				Collection<Integer> pnrSegIds = new HashSet<Integer>();

				Date departure = null;
				String subStatus = null;

				for (ReservationPaxFareSegment reservationPaxFareSegment : reservationPaxFare.getPaxFareSegments()) {
					pnrSegIds.add(reservationPaxFareSegment.getSegment().getPnrSegId());

					for (ReservationSegmentDTO segmentDTO : reservationPax.getReservation().getSegmentsView()) {
						if (segmentDTO.getPnrSegId().equals(reservationPaxFareSegment.getSegment().getPnrSegId())) {
							departure = segmentDTO.getDepartureDate();
							subStatus = segmentDTO.getSubStatus();
							status = segmentDTO.getStatus();
						}
					}
				}

				if (subStatus == null || subStatus.equals("")) {// Do not show EX segments
					CarrierOndGroup carrierOndGroup = null;

					if (ondWisePnrPaxFareIds.containsKey(pnrSegIds)) {
						carrierOndGroup = ondWisePnrPaxFareIds.get(pnrSegIds);
						carrierOndGroup.setCarrierOndGroupRPH(
								carrierOndGroup.getCarrierOndGroupRPH() + "|" + reservationPaxFare.getPnrPaxFareId());

					} else {
						carrierOndGroup = new CarrierOndGroup();
						carrierOndGroup.setCarrierOndGroupRPH(String.valueOf(reservationPaxFare.getPnrPaxFareId()));

					}
					carrierOndGroup.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
					carrierOndGroup.getCarrierPnrSegIds().addAll(pnrSegIds);
					carrierOndGroup.setSegmentCode(ReservationApiUtils.getOndCode(reservationPaxFare));
					carrierOndGroup.setStatus(status);
					carrierOndGroup.setDepartureDateTime(departure == null ? null : dateFormatter.format(departure));
					carrierOndGroup.setTotalCharges(AccelAeroCalculator.add(reservationPaxFare.getTotalChargeAmount(),
							(carrierOndGroup.getTotalCharges() != null
									? carrierOndGroup.getTotalCharges()
									: AccelAeroCalculator.getDefaultBigDecimalZero())));

					ondWisePnrPaxFareIds.put(pnrSegIds, carrierOndGroup);
				}

			}
		}

		carrierOndGrouping.addAll(ondWisePnrPaxFareIds.values());
	}

	/**
	 * Populate {@link AASearchReservationByPnrBL#paxTypeQuantityForThisRes passengerTypeQuantityForThisReservation}
	 * field using {@link com.isa.thinair.airreservation.api.model.Reservation Reservation}
	 */
	private void populatePassengerTypeQuantityForThisReservation() {
		paxTypeQuantityForThisRes = new ArrayList<PassengerTypeQuantity>();

		// Adult PassengerType
		PassengerTypeQuantity adultPassengerType = new PassengerTypeQuantity();
		adultPassengerType.setPassengerType(PassengerType.ADT);
		adultPassengerType.setQuantity(reservation.getTotalPaxAdultCount());
		paxTypeQuantityForThisRes.add(adultPassengerType);

		// Child PassengerType
		PassengerTypeQuantity childPassengerType = new PassengerTypeQuantity();
		childPassengerType.setPassengerType(PassengerType.CHD);
		childPassengerType.setQuantity(reservation.getTotalPaxChildCount());
		paxTypeQuantityForThisRes.add(childPassengerType);

		// Infant PassengerType
		PassengerTypeQuantity infantPassengerType = new PassengerTypeQuantity();
		infantPassengerType.setPassengerType(PassengerType.INF);
		infantPassengerType.setQuantity(reservation.getTotalPaxInfantCount());
		paxTypeQuantityForThisRes.add(infantPassengerType);
	}

	/**
	 * Populate the AirItinerary object of AirReservation
	 * 
	 * @return OriginDestinationOptions (AirItinerary)
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	private OriginDestinationOptions populateAirItinerary() throws ModuleException {

		OriginDestinationOptions airItinerary = new OriginDestinationOptions();
		OriginDestinationOption ondOption;
		Map<Integer, OriginDestinationOption> tmpOndOptionMap = new HashMap<Integer, OriginDestinationOption>();

		if (reservation != null && reservation.getSegmentsView() != null) {
			for (ReservationSegmentDTO reservationSegmentDTO : reservation.getSegmentsView()) {

				// Pax details are the same for the entire reservation, as such
				// required info
				// on the air travelers can be retrieved from TravelerInfo
				AirTravelerAvail airTravelerAvail = null;

				// Arrival Airport & Departure Airport
				String segmentCode = reservationSegmentDTO.getSegmentCode();
				String arrivalAirportCode = segmentCode.substring(segmentCode.length() - 3);
				String departureAirportCode = segmentCode.substring(0, 3);
				String[] airportCodes = new String[] { arrivalAirportCode, departureAirportCode };
				Map<String, CachedAirportDTO> airports = AAServicesModuleUtils.getAirportBD()
						.getCachedOwnAirportMap(Arrays.asList(airportCodes));
				CachedAirportDTO airport = null;

				/* FlightSegment */
				FlightSegment flightSegment = new FlightSegment();
				flightSegment.setAirTravelerAvail(airTravelerAvail);
				flightSegment.setFlightStatus(reservationSegmentDTO.getFlightSegStatus());
				flightSegment.setSubStatus(reservationSegmentDTO.getSubStatus());

				// Arrival Info
				flightSegment.setArrivalAirportCode(arrivalAirportCode);
				Date arvDt = reservationSegmentDTO.getArrivalDate();
				flightSegment.setArrivalDateTime(arvDt);
				Date arvDtZu = reservationSegmentDTO.getZuluArrivalDate();
				flightSegment.setArrivalDateTimeZulu(arvDtZu);
				airport = airports.get(arrivalAirportCode);
				if (airport != null) {
					flightSegment.setArrivalAirportName(airport.getAirportName());
				}

				// Departure Info
				flightSegment.setDepartureAirportCode(departureAirportCode);
				Date depDt = reservationSegmentDTO.getDepartureDate();
				flightSegment.setDepatureDateTime(depDt);
				Date depDtZu = reservationSegmentDTO.getZuluDepartureDate();
				flightSegment.setDepatureDateTimeZulu(depDtZu);
				airport = airports.get(departureAirportCode);
				if (airport != null) {
					flightSegment.setDepartureAirportName(airport.getAirportName());
				}
				// Flight Ref Info
				flightSegment.setFlightNumber(reservationSegmentDTO.getFlightNo());
				// flightSegment.setFlightRefNumber(reservationSegmentDTO.getFlightSegId().toString());

				flightSegment.setFlightRefNumber(FlightRefNumberUtil.composeFlightRPH(reservationSegmentDTO));
				flightSegment.setBookingFlightRefNumber(reservationSegmentDTO.getPnrSegId().toString());

				// OperatingAirline
				flightSegment.setOperatingAirline(AppSysParamsUtil.extractCarrierCode(reservationSegmentDTO.getFlightNo()));

				if (reservationSegmentDTO.getFareTO() != null) {
					OldFareDetails oldFareDetails = new OldFareDetails();
					oldFareDetails.setOldFareAmount(reservationSegmentDTO.getFareTO().getAdultFareAmount());
					oldFareDetails.setOldFareId(reservationSegmentDTO.getFareTO().getFareId());
					oldFareDetails.setFareBasisCode(reservationSegmentDTO.getFareTO().getFareBasisCode());
					oldFareDetails.setBookingClass(reservationSegmentDTO.getFareTO().getBookingClassCode());
					oldFareDetails.setOldFareType(reservationSegmentDTO.getFareTO().getFareRuleID());
					oldFareDetails.setCarrierCode(reservationSegmentDTO.getCarrierCode());
					oldFareDetails.setOndCode(reservationSegmentDTO.getSegmentCode());
					oldFareDetails.setFlightSegId(reservationSegmentDTO.getFlightSegId());
					oldFareDetails.setLogicalCCCode(reservationSegmentDTO.getCabinClassCode());

					oldFareDetails.setOldFareType(Integer
							.parseInt(getFareType(SelectListGenerator.getFareType(reservationSegmentDTO.getPnrSegId().toString(),
									(reservationSegmentDTO.getFareTO().getFareId() + "")))));
					oldFareDetails.setBulkTicketFareRule(!ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(reservationSegmentDTO.getStatus())
							&& reservationSegmentDTO != null && reservationSegmentDTO.getFareTO() != null && reservationSegmentDTO.getFareTO().isBulkTicketFareRule());

					flightSegment.setOldFareDetails(oldFareDetails);
				}

				// OperationType
				// TODO Need to extract these as AAConstants. Otherwise how does
				// the external parties know what types it
				// returns
				// Please improve me Nili 4:46 PM 10/8/2009
				flightSegment.setOperationType(reservationSegmentDTO.getOperationTypeID());

				// Segment Code and grouping
				flightSegment.setSegmentCode(reservationSegmentDTO.getSegmentCode());
				flightSegment.setSegmentSequence(reservationSegmentDTO.getSegmentSeq());
				flightSegment.setOndGroupId(reservationSegmentDTO.getFareGroupId());
				flightSegment.setReturnFlag(reservationSegmentDTO.getReturnFlag());
				flightSegment.setAlertFlag(reservationSegmentDTO.getAlertFlag());
				flightSegment.setReturnOndGroupId(reservationSegmentDTO.getReturnOndGroupId());
				flightSegment.setJourneySeq(reservationSegmentDTO.getJourneySeq());

				// Status
				flightSegment.setStatus(SegmentStatus.fromValue(reservationSegmentDTO.getStatus()));
				flightSegment.setDisplayStatus(reservationSegmentDTO.getDisplayStatus());
				flightSegment.setCabinClassCode(reservationSegmentDTO.getCabinClassCode());
				flightSegment.setLogicalCabinClassCode(reservationSegmentDTO.getLogicalCCCode());
				flightSegment.setLastFareQuoteDateZulu(reservationSegmentDTO.getLastFareQuoteDateZulu());

				if (tmpOndOptionMap.containsKey(reservationSegmentDTO.getFareGroupId())) {
					ondOption = tmpOndOptionMap.get(reservationSegmentDTO.getFareGroupId());
				} else {
					ondOption = new OriginDestinationOption();
					airItinerary.getOriginDestinationOption().add(ondOption);

					tmpOndOptionMap.put(reservationSegmentDTO.getFareGroupId(), ondOption);
				}
				/* segment modification */
				flightSegment.setModifyByDate(reservationSegmentDTO.getFareTO().getIsAllowModifyDate());
				flightSegment.setModifyByRoute(reservationSegmentDTO.getFareTO().getIsAllowModifyRoute());

				flightSegment.setReturnFlag(reservationSegmentDTO.getReturnFlag());
				flightSegment.setBookingType(reservationSegmentDTO.getBookingType());
				/* Setting segment credential information */
				flightSegment.setMarketingAgentCode(reservationSegmentDTO.getMarketingAgentCode());
				flightSegment.setMarketingStationCode(reservationSegmentDTO.getMarketingStationCode());
				flightSegment.setMarketingCarrierCode(reservationSegmentDTO.getMarketingCarrierCode());
				flightSegment.setStatusModifiedDate(reservationSegmentDTO.getStatusModifiedDate());
				flightSegment.setStatusModifiedChannelCode(reservationSegmentDTO.getStatusModifiedChannelCode());
				flightSegment.setSubStationShortName(reservationSegmentDTO.getSubStationShortName());

				flightSegment.setBaggageAllowance(reservationSegmentDTO.getFlightSegBagAllowance());
				flightSegment.setBaggageONDGroupId(reservationSegmentDTO.getBaggageOndGroupId());

				// we do not have an interline group key for own reservation
				// loaded as dry reservations.
				// in that case we retain the fare group id as the interline
				// group key.
				flightSegment.setInterlineGroupKey(reservationSegmentDTO.getFareGroupId().toString());

				flightSegment.setGroundStationPnrSegmentID(reservationSegmentDTO.getGroundStationPnrSegmentID());
				flightSegment.setSegmentConnTimeInfo(getAirportMinMaxTransitDuration(reservationSegmentDTO));

				flightSegment.setAlertAutoCancellation(reservationSegmentDTO.isAlertAutoCancellation());
				flightSegment.setModifiedFrom(reservationSegmentDTO.getModifiedFrom());
				flightSegment.setBundledServiceId(reservationSegmentDTO.getBundledFarePeriodId());

				/* segment checking gap */
				if (airportBD != null) {
					flightSegment.setCheckInTimeGap(
							airportBD.getAirportCheckInTimeGap(getDepartureAirportCode(reservationSegmentDTO.getSegmentCode()),
									reservationSegmentDTO.getFlightNo()));
				}

				ondOption.getFlightSegment().add(flightSegment);
			}
		}
		return airItinerary;
	}

	/**
	 * assemble external pnr segments to the reservation
	 * 
	 * @return
	 */
	private OriginDestinationOptions populateExternalAirItinerary() {

		OriginDestinationOptions airItinerary = new OriginDestinationOptions();

		for (ExternalPnrSegment externalSegments : reservation.getExternalReservationSegment()) {

			ExternalFlightSegment externalFlightSegment = externalSegments.getExternalFlightSegment();
			OriginDestinationOption ondOption = new OriginDestinationOption();

			/* FlightSegment */
			FlightSegment flightSegment = new FlightSegment();

			flightSegment.setFlightStatus(externalFlightSegment.getStatus());
			flightSegment.setSubStatus(externalSegments.getSubStatus());

			String[] ondCodes = externalFlightSegment.getSegmentCode().split("/");

			// Arrival Info
			flightSegment.setArrivalAirportCode(ondCodes[1]);
			flightSegment.setArrivalDateTime(externalFlightSegment.getEstTimeArrivalLocal());
			flightSegment.setArrivalDateTimeZulu(externalFlightSegment.getEstTimeArrivalZulu());

			// Departure Info
			flightSegment.setDepartureAirportCode(ondCodes[0]);
			flightSegment.setDepatureDateTime(externalFlightSegment.getEstTimeDepatureLocal());
			flightSegment.setDepatureDateTimeZulu(externalFlightSegment.getEstTimeDepatureZulu());

			// Flight Ref Info
			flightSegment.setFlightNumber(externalFlightSegment.getFlightNumber());
			// OperatingAirline
			flightSegment.setOperatingAirline(externalFlightSegment.getExternalCarrierCode());

			flightSegment.setFlightRefNumber(FlightRefNumberUtil.composeFlightRPH(externalFlightSegment));
			flightSegment.setSegmentCode(externalFlightSegment.getSegmentCode());

			Integer externalPnrSegRef = externalSegments.getExternalPnrSegRef();
			if (externalPnrSegRef != null) {
				flightSegment.setBookingFlightRefNumber(externalPnrSegRef.toString());
			}
			flightSegment.setSegmentSequence(externalSegments.getSegmentSeq());

			// Status
			flightSegment.setStatus(SegmentStatus.fromValue(externalSegments.getStatus()));

			/** sets the segment credentials */
			flightSegment.setMarketingAgentCode(externalSegments.getMarketingAgentCode());
			flightSegment.setMarketingCarrierCode(externalSegments.getMarketingCarrierCode());
			flightSegment.setMarketingStationCode(externalSegments.getMarketingStationCode());
			flightSegment.setStatusModifiedDate(externalSegments.getStatusModifiedDate());
			flightSegment.setStatusModifiedChannelCode(externalSegments.getStatusModifiedSalesChannelCode());
			flightSegment.setCabinClassCode(externalSegments.getCabinClassCode());
			flightSegment.setLogicalCabinClassCode(externalSegments.getLogicalCabinClassCode());
			ondOption.getFlightSegment().add(flightSegment);

			// adding the external ond info to the aaReservation
			airItinerary.getOriginDestinationOption().add(ondOption);
		}

		return airItinerary;
	}

	/**
	 * Populate the AirReservationSummary object of AirReservation
	 * 
	 * @return AirReservationSummary
	 * @throws ModuleException
	 */
	private AirReservationSummary populateAirReservationSummary() throws ModuleException {

		ReservationAdminInfo resvAdminInfo = reservation.getAdminInfo();

		// AdminInfo
		AdminInfo adminInfo = new AdminInfo();
		adminInfo.setOriginAgentCode(BeanUtils.nullHandler(resvAdminInfo.getOriginAgentCode()));
		adminInfo.setOriginIpAddress(BeanUtils.nullHandler(resvAdminInfo.getOriginIPAddress()));
		adminInfo.setOriginUserID(BeanUtils.nullHandler(resvAdminInfo.getOriginUserId()));
		adminInfo.setOriginChannelID(BeanUtils.nullHandler(resvAdminInfo.getOriginChannelId().toString()));
		adminInfo.setOriginSalesTerminal(BeanUtils.nullHandler(resvAdminInfo.getOriginSalesTerminal()));
		adminInfo.setOriginCarrierCode(BeanUtils.nullHandler(resvAdminInfo.getOriginCarrierCode()));
		adminInfo.setOwnerChannelID(BeanUtils.nullHandler(resvAdminInfo.getOwnerChannelId().toString()));
		adminInfo.setLastSalesTerminal(BeanUtils.nullHandler(resvAdminInfo.getLastSalesTerminal()));
		adminInfo.setOriginCountryCode(BeanUtils.nullHandler(resvAdminInfo.getOriginCountryCode()));
		adminInfo.setOwnerAgentContactInfo(new AgentContactInfo());

		String ownerAgentCode = BeanUtils.nullHandler(resvAdminInfo.getOwnerAgentCode());

		// In an IBE booking there won't be any owner agent code thus this is to
		// cater that
		if (ownerAgentCode.length() > 0) {
			adminInfo.setOwnerAgentCode(ownerAgentCode);

			Agent ownerAgent = AAServicesModuleUtils.getTravelAgentBD().getAgent(ownerAgentCode);

			adminInfo.getOwnerAgentContactInfo().setAgentCarrier(thisAirlineCode);
			adminInfo.getOwnerAgentContactInfo().setAgentEmail(ownerAgent.getEmailId());
			adminInfo.getOwnerAgentContactInfo().setAgentName(ownerAgent.getAgentDisplayName());
			adminInfo.getOwnerAgentContactInfo().setAgentTelephone(ownerAgent.getTelephone());
		}

		// AirReservationSummary
		AirReservationSummary airReservationSummary = new AirReservationSummary();
		airReservationSummary.setAdminInfo(adminInfo);
		airReservationSummary.getPassengerTypeQuantity().addAll(paxTypeQuantityForThisRes);

		return airReservationSummary;
	}

	/**
	 * Populate the ContactInfo object of AirReservation
	 * 
	 * @return ContactInfo
	 * @throws ModuleException
	 * @throws NumberFormatException
	 */
	private ContactInfo populateContactInfo() throws NumberFormatException, ModuleException {
		ContactInfo contactInfo = new ContactInfo();
		ReservationContactInfo resvContactInfo = reservation.getContactInfo();

		// setting the marketing carrier customer id if it is a IBE registered
		// user booking
		contactInfo.setMarketingCarrierCustomerId(resvContactInfo.getMarketingCarrierCustomerId());

		Country country = new Country();
		country.setCountryCode(resvContactInfo.getCountryCode());
		country.setCountryName(""); // TODO Ignore for now (Better than making a
									// DB call)

		// Address
		Address address = new Address();
		address.getAddressLine().add(resvContactInfo.getStreetAddress1());
		address.getAddressLine().add(resvContactInfo.getStreetAddress2());
		address.setCityName(resvContactInfo.getCity());
		address.setPostalCode(""); // N/Av
		address.setCountry(country);
		address.setState(resvContactInfo.getState());
		contactInfo.setAddress(address);

		// Email & preferredLanguage
		contactInfo.setEmail(resvContactInfo.getEmail());
		// FIXME
		// contactInfo.setPreferredLanguage(resvContactInfo.getPreferredLanguage());

		// PAX Name
		PersonName personName = new PersonName();
		personName.setFirstName(resvContactInfo.getFirstName());
		personName.setMiddleName(""); // N/Av
		personName.setSurName(resvContactInfo.getLastName());
		personName.setTitle(resvContactInfo.getTitle());
		contactInfo.setPersonName(personName);

		// Phone
		Telephone telephone = new Telephone();
		telephone.setPhoneNumber(resvContactInfo.getPhoneNo());
		telephone.setAreaCode(""); // N/Av
		telephone.setCountry(country);
		contactInfo.setTelephone(telephone);

		// Fax
		Telephone fax = new Telephone();
		fax.setPhoneNumber(resvContactInfo.getFax());
		fax.setAreaCode(""); // N/Av
		fax.setCountry(country);
		contactInfo.setFax(fax);

		// Mobile
		Telephone mobile = new Telephone();
		mobile.setPhoneNumber(resvContactInfo.getMobileNo());
		mobile.setAreaCode(""); // N/Av
		mobile.setCountry(country);
		contactInfo.setMobile(mobile);

		// Nationality Code
		String nationalityCode = resvContactInfo.getNationalityCode();
		if (nationalityCode != null && nationalityCode.length() > 0) {
			contactInfo.setNationalityCode(AAUtils.getNationalityIsoCode(Integer.valueOf(nationalityCode)));
		}

		contactInfo.setZipCode(resvContactInfo.getZipCode());

		contactInfo.setPreferredLanguage(resvContactInfo.getPreferredLanguage());

		// Emergency Contact Info
		PersonName emgnPersonName = new PersonName();
		emgnPersonName.setFirstName(resvContactInfo.getEmgnFirstName());
		emgnPersonName.setMiddleName(""); // N/Av
		emgnPersonName.setSurName(resvContactInfo.getEmgnLastName());
		emgnPersonName.setTitle(resvContactInfo.getEmgnTitle());
		contactInfo.setEmgnPersonName(emgnPersonName);

		Telephone emgnPhone = new Telephone();
		emgnPhone.setPhoneNumber(resvContactInfo.getEmgnPhoneNo());
		emgnPhone.setAreaCode(""); // N/Av
		emgnPhone.setCountry(country);
		contactInfo.setEmgnTelephone(emgnPhone);

		contactInfo.setEmgnEmail(resvContactInfo.getEmgnEmail());
		
		contactInfo.setTaxRegNo(resvContactInfo.getTaxRegNo());

		return contactInfo;
	}

	/**
	 * Populate the Fulfillment object of AirReservation
	 * 
	 * Fulfillment = Payment information
	 * 
	 * @return Fulfillment
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	@SuppressWarnings("unchecked")
	private Fulfillment populateFulfillment() throws ModuleException, WebservicesException {

		Fulfillment fulfillment = new Fulfillment();
		// Get all Nominal Code Groups
		Collection<Integer> colCashNominalCodes = ReservationTnxNominalCode.getCashTypeNominalCodes();
		Collection<Integer> colVisaNominalCodes = ReservationTnxNominalCode.getVisaTypeNominalCodes();
		Collection<Integer> colMasterNominalCodes = ReservationTnxNominalCode.getMasterTypeNominalCodes();
		Collection<Integer> colAmexNominalCodes = ReservationTnxNominalCode.getAmexTypeNominalCodes();
		Collection<Integer> colDinersNominalCodes = ReservationTnxNominalCode.getDinersTypeNominalCodes();
		Collection<Integer> colGenericNominalCodes = ReservationTnxNominalCode.getGenericTypeNominalCodes();
		Collection<Integer> colCMINominalCodes = ReservationTnxNominalCode.getCMITypeNominalCodes();
		Collection<Integer> colOnAccountNominalCodes = ReservationTnxNominalCode.getOnAccountTypeNominalCodes();
		Collection<Integer> colCreditNominalCodes = ReservationTnxNominalCode.getCreditTypeNominalCodes();
		Collection<Integer> colBSPNominalCodes = ReservationTnxNominalCode.getBSPAccountTypeNominalCodes();
		Collection<Integer> colLoyalityNominalCodes = ReservationTnxNominalCode.getLoyaltyNominalCodes();
		Collection<Integer> colVoucherNominalCodes = ReservationTnxNominalCode.getVoucherNominalCodes();

		Collection<Integer> pnrPaxIdList = new ArrayList<Integer>();
		boolean isInfantPaymentRecordedWithInfant = reservation.isInfantPaymentRecordedWithInfant();
		if (reservation.getPassengers() != null) {
			for (ReservationPax reservationPax : reservation.getPassengers()) {
				if (isInfantPaymentRecordedWithInfant || !ReservationApiUtils.isInfantType(reservationPax)) {
					pnrPaxIdList.add(reservationPax.getPnrPaxId());
				}
			}
		}
		// We can load per pax credits with in the below loop. But it is
		// generating more hits to db. So, loads all pax
		// credits at once.
		Map<Integer, Collection<CreditInfoDTO>> paxCredits = AAServicesModuleUtils.getPassengerBD().getAllCredits(pnrPaxIdList);

		if (reservation.getPassengers() != null) {
			for (ReservationPax reservationPax : reservation.getPassengers()) {
				if (isInfantPaymentRecordedWithInfant || !ReservationApiUtils.isInfantType(reservationPax)) {
					TravelerFulfillment travelerFulfillment = new TravelerFulfillment();
					String travelerRef = AAReservationUtil.getTravelerRef(reservationPax);
					travelerFulfillment.setTravelerRefNumber(travelerRef);
					Map<Collection<Integer>, Collection<ReservationTnx>> payTypeAndColReservationTnx = new HashMap<Collection<Integer>, Collection<ReservationTnx>>();

					Collection<ReservationTnx> colReservationTnx = reservationPax.getPaxPaymentTnxView();
					if (colReservationTnx != null && colReservationTnx.size() > 0) {
						for (ReservationTnx reservationTnx : colReservationTnx) {
							if (colCashNominalCodes.contains(reservationTnx.getNominalCode())) {
								updateNominalMap(payTypeAndColReservationTnx, colCashNominalCodes, reservationTnx);
							} else if (colVisaNominalCodes.contains(reservationTnx.getNominalCode())) {
								updateNominalMap(payTypeAndColReservationTnx, colVisaNominalCodes, reservationTnx);
							} else if (colMasterNominalCodes.contains(reservationTnx.getNominalCode())) {
								updateNominalMap(payTypeAndColReservationTnx, colMasterNominalCodes, reservationTnx);
							} else if (colAmexNominalCodes.contains(reservationTnx.getNominalCode())) {
								updateNominalMap(payTypeAndColReservationTnx, colAmexNominalCodes, reservationTnx);
							} else if (colDinersNominalCodes.contains(reservationTnx.getNominalCode())) {
								updateNominalMap(payTypeAndColReservationTnx, colDinersNominalCodes, reservationTnx);
							} else if (colGenericNominalCodes.contains(reservationTnx.getNominalCode())) {
								updateNominalMap(payTypeAndColReservationTnx, colGenericNominalCodes, reservationTnx);
							} else if (colCMINominalCodes.contains(reservationTnx.getNominalCode())) {
								updateNominalMap(payTypeAndColReservationTnx, colCMINominalCodes, reservationTnx);
							} else if (colOnAccountNominalCodes.contains(reservationTnx.getNominalCode())) {
								updateNominalMap(payTypeAndColReservationTnx, colOnAccountNominalCodes, reservationTnx);
							} else if (colCreditNominalCodes.contains(reservationTnx.getNominalCode())) {
								updateNominalMap(payTypeAndColReservationTnx, colCreditNominalCodes, reservationTnx);
							} else if (colBSPNominalCodes.contains(reservationTnx.getNominalCode())) {
								updateNominalMap(payTypeAndColReservationTnx, colBSPNominalCodes, reservationTnx);
							} else if (colLoyalityNominalCodes.contains(reservationTnx.getNominalCode())) {
								updateNominalMap(payTypeAndColReservationTnx, colLoyalityNominalCodes, reservationTnx);
							} else if (colVoucherNominalCodes.contains(reservationTnx.getNominalCode())) {
								updateNominalMap(payTypeAndColReservationTnx, colVoucherNominalCodes, reservationTnx);
							}
						}
					}
					captureCarrierPaymentDetails(payTypeAndColReservationTnx, travelerFulfillment.getPaymentDetails());
					travelerFulfillment.getCreditDetails().addAll(AACustomerFulfillmentPopulator
							.getCustomerCreditDetails(paxCredits.get(reservationPax.getPnrPaxId())));
					fulfillment.getCarrierFulfillments().add(travelerFulfillment);
				}
			}
		}
		fulfillment.getCustomerFulfillments()
				.addAll(AACustomerFulfillmentPopulator.populateCustomerFulfillment(this.reservation, paxCredits));
		return fulfillment;
	}

	/**
	 * Populate the PriceInfo object of AirReservation
	 * 
	 * @return PriceInfo
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	@SuppressWarnings("unchecked")
	private PriceInfo populatePriceInfo() throws ModuleException, WebservicesException {
		PriceInfo priceInfo = new PriceInfo();

		// PricingSource
		// TODO Improve the pricing source
		priceInfo.setPricingSource(PricingSource.PUBLISHED);

		// FareType
		FareType reservationPrice = new FareType();

		FaresAndChargesTO faresAndChargesTO = getFareIdAndFareRuleMap();
		Map externalChargesMap = ReservationModuleUtils.getAirReservationConfig().getExternalChargesMap();
		Collection<Integer> colPnrPaxIds = this.getPayablePaxIds(reservation);
		Map<Integer, Collection<ReservationTnx>> mapBalances = AAServicesModuleUtils.getPassengerBD()
				.getAllBalanceTransactions(colPnrPaxIds);

		for (ReservationPax reservationPax : reservation.getPassengers()) {
			PerPaxPriceInfo perPaxPriceInfo = new PerPaxPriceInfo();
			perPaxPriceInfo.setTravelerRefNumber(AAReservationUtil.getTravelerRef(reservationPax));
			PassengerType passengerType = PaxTypeUtils.convertAAPaxCodeToLCCPaxCodes(reservationPax.getPaxType());
			perPaxPriceInfo.setPassengerType(passengerType);

			FareType passengerPrice = new FareType();
			AAPnrPaxChargeHolder chargeHolder = new AAPnrPaxChargeHolder();
			for (ReservationPaxFare reservationPaxFare : reservationPax.getPnrPaxFares()) {

				// Return the ond code
				String ondCode = ReservationApiUtils.getOndCode(reservationPaxFare);
				Date departureDate = ReservationApiUtils.getFirstDepartureDate(reservationPaxFare);

				if (AppSysParamsUtil.isHideStopOverEnabled() && ondCode != null && ondCode.split("/").length > 2) {
					ondCode = ReservationApiUtils.hideStopOverSeg(ondCode);
				}

				FareTO fareTO = faresAndChargesTO.getFareTOs().get(reservationPaxFare.getFareId());
				String fareMetaData = getFareMetaDesc(reservationPaxFare, fareTO);

				Charge appliedDiscountGroup = null;

				if (reservationPaxFare.getDiscountCode() != null) {
					appliedDiscountGroup = AAServicesModuleUtils.getChargeBD().getCharge(reservationPaxFare.getDiscountCode());
				}

				for (ReservationPaxOndCharge reservationPaxOndCharge : reservationPaxFare.getCharges()) {
					String chargeGroupCode = reservationPaxOndCharge.getChargeGroupCode();
					String chargeInfo = chargeGroupCode;
					// FIXME Send the local charge date instead of the zulu date
					if (ReservationInternalConstants.ChargeGroup.SUR.equals(chargeGroupCode)
							|| ReservationInternalConstants.ChargeGroup.INF.equals(chargeGroupCode)) {
						// SUR & INF
						ChargeTO chargeTO = faresAndChargesTO.getChargeTOs().get(reservationPaxOndCharge.getChargeRateId());

						Surcharge surcharge = new Surcharge();
						surcharge.setSurchargeCode(chargeTO.getChargeCode());
						surcharge.setSurchargeName(chargeTO.getChargeDescription());
						surcharge.setAmount(reservationPaxOndCharge.getAmount());
						surcharge.setApplicableDateTime(chargeHolder.getZuluChargeDate(reservationPaxOndCharge));
						surcharge.setDepartureDateTime(departureDate);
						surcharge.getApplicablePassengerType().add(passengerType);
						surcharge.setCarrierCode(thisAirlineCode);
						surcharge.setSegmentCode(ondCode);
						surcharge.setPaxFareId(reservationPaxOndCharge.getPnrPaxOndChgId().toString());

						populatePNRSegmentID(surcharge, reservationPaxFare);
						passengerPrice.getSurcharges().add(surcharge);
						reservationPrice.getSurcharges().add(surcharge);

						chargeInfo = chargeTO.getChargeCode();
					} else if (ReservationInternalConstants.ChargeGroup.TAX.equals(chargeGroupCode)) {
						// TAX
						ChargeTO chargeTO = faresAndChargesTO.getChargeTOs().get(reservationPaxOndCharge.getChargeRateId());

						Tax tax = new Tax();
						tax.setTaxCode(chargeTO.getChargeCode());
						tax.setTaxName(chargeTO.getChargeDescription());
						tax.setAmount(reservationPaxOndCharge.getAmount());
						tax.setApplicableDateTime(chargeHolder.getZuluChargeDate(reservationPaxOndCharge));
						tax.setDepartureDateTime(departureDate);
						tax.getApplicablePassengerType().add(passengerType);
						tax.setCarrierCode(thisAirlineCode);
						tax.setSegmentCode(ondCode);
						tax.setPaxFareId(reservationPaxOndCharge.getPnrPaxOndChgId().toString());

						populatePNRSegmentID(tax, reservationPaxFare);
						passengerPrice.getTaxes().add(tax);
						reservationPrice.getTaxes().add(tax);

						chargeInfo = chargeTO.getChargeCode();
					} else if (ReservationInternalConstants.ChargeGroup.FAR.equals(chargeGroupCode)) {
						// FARE
						BaseFare baseFare = new BaseFare();
						String chargeGroupDesc = AppSysParamsUtil.getChargeGroupDesc(chargeGroupCode);
						baseFare.setAmount(reservationPaxOndCharge.getAmount());
						if (reservationPax.getPaxType().equals(ReservationInternalConstants.PassengerType.INFANT)) {
							baseFare.setDescription(chargeGroupDesc);
						} else {
							baseFare.setDescription(chargeGroupDesc + fareMetaData);
							baseFare.setBookingClass(fareTO.getBookingClassCode());
						}
						baseFare.setApplicableDateTime(chargeHolder.getZuluChargeDate(reservationPaxOndCharge));
						baseFare.getApplicablePassengerType().add(passengerType);
						baseFare.getCarrierCode().add(thisAirlineCode); // FIXME
																		// :
																		// Must
																		// get
																		// and
																		// add
																		// all
																		// carriers
						baseFare.setSegmentCode(ondCode);
						baseFare.setPnrPaxFareId(reservationPaxFare.getPnrPaxFareId());
						baseFare.setPaxFareId(reservationPaxOndCharge.getPnrPaxOndChgId().toString());
						baseFare.setDepartureDateTime(departureDate);
						// sets cabin class type & fare rule for the fare
						// description
						populateSegmentRPH(baseFare, reservationPaxFare);
						populatePNRSegmentID(baseFare, reservationPaxFare);

						passengerPrice.getBaseFare().add(baseFare);
						reservationPrice.getBaseFare().add(baseFare);
					} else if (ReservationInternalConstants.ChargeGroup.DISCOUNT.equals(chargeGroupCode)) {
						ChargeTO chargeTO = faresAndChargesTO.getChargeTOs().get(reservationPaxOndCharge.getChargeRateId());
						Discount discount = new Discount();
						discount.setDiscountCode(chargeTO.getChargeCode());
						discount.setDiscountName(chargeTO.getChargeDescription());
						discount.setAmount(reservationPaxOndCharge.getAmount());
						discount.setPaxFareId(reservationPaxOndCharge.getPnrPaxOndChgId().toString());
						discount.setCarrierCode(thisAirlineCode);
						discount.setSegmentCode(ondCode);
						discount.setApplicableDateTime(chargeHolder.getZuluChargeDate(reservationPaxOndCharge));
						discount.setDepartureDateTime(departureDate);
						passengerPrice.getDiscount().add(discount);
						reservationPrice.getDiscount().add(discount);
					} else {
						ChargeTO chargeTO = null;
						chargeTO = faresAndChargesTO.getChargeTOs().get(reservationPaxOndCharge.getChargeRateId());
						if (ReservationInternalConstants.ChargeGroup.PEN.equals(chargeGroupCode) && chargeTO != null
								&& externalChargesMap.get(EXTERNAL_CHARGES.ANCI_PENALTY.toString())
										.equals(chargeTO.getChargeCode())) {
							Surcharge surcharge = new Surcharge();
							surcharge.setSurchargeCode(chargeTO.getChargeCode());
							surcharge.setSurchargeName(chargeTO.getChargeDescription());
							surcharge.setAmount(reservationPaxOndCharge.getAmount());
							surcharge.setApplicableDateTime(chargeHolder.getZuluChargeDate(reservationPaxOndCharge));
							surcharge.setDepartureDateTime(departureDate);
							surcharge.getApplicablePassengerType().add(passengerType);
							surcharge.setCarrierCode(thisAirlineCode);
							surcharge.setSegmentCode(ondCode);
							surcharge.setPaxFareId(reservationPaxOndCharge.getPnrPaxOndChgId().toString());

							populatePNRSegmentID(surcharge, reservationPaxFare);
							passengerPrice.getSurcharges().add(surcharge);
							reservationPrice.getSurcharges().add(surcharge);

						} else {
							// Other charges CNX, MOD, ADJ
							// Please note ISU, ESU will be mapped to SUR type in
							// reservation world
							chargeTO = faresAndChargesTO.getChargeTOs().get(reservationPaxOndCharge.getChargeRateId());
							Fee fee = new Fee();
							if (chargeTO != null) {
								fee.setFeeCode(chargeTO.getChargeCode());
								fee.setFeeName(chargeTO.getChargeDescription());
								chargeInfo = chargeTO.getChargeCode();
							} else {
								String chargeGroupDesc = AppSysParamsUtil.getChargeGroupDesc(chargeGroupCode);
								fee.setFeeCode(chargeGroupDesc);
								fee.setFeeName(chargeGroupDesc + fareMetaData);
							}
							fee.setAmount(reservationPaxOndCharge.getAmount());
							fee.setApplicableDateTime(chargeHolder.getZuluChargeDate(reservationPaxOndCharge));
							fee.setDepartureDateTime(departureDate);
							fee.getApplicablePassengerType().add(passengerType);
							fee.setCarrierCode(thisAirlineCode);
							fee.setSegmentCode(ondCode);
							fee.setPaxFareId(reservationPaxOndCharge.getPnrPaxOndChgId().toString());

							if (reservationPaxOndCharge.getChargeGroupCode()
									.equals(ReservationInternalConstants.ChargeGroup.ADJ)) {
								fee.setFeeType(FeeType.ADJ);
							} else if (reservationPaxOndCharge.getChargeGroupCode()
									.equals(ReservationInternalConstants.ChargeGroup.CNX)) {
								fee.setFeeType(FeeType.CNX);
							} else if (reservationPaxOndCharge.getChargeGroupCode()
									.equals(ReservationInternalConstants.ChargeGroup.MOD)) {
								fee.setFeeType(FeeType.MOD);
							} else {
								// FIXME, throw exception (Unidentified Fee) or add
								// fee type called OTH (Other) ?
							}

							passengerPrice.getFees().add(fee);
							reservationPrice.getFees().add(fee);
						}

					}

					// add discount/promotion details
					if (reservationPaxOndCharge.getDiscount() != null && reservationPaxOndCharge.getDiscount().doubleValue() != 0
							&& appliedDiscountGroup != null) {

						Discount discountTo = new Discount();
						discountTo.setAmount(reservationPaxOndCharge.getDiscount());
						discountTo.setApplicableDateTime(chargeHolder.getZuluChargeDate(reservationPaxOndCharge));
						discountTo.setCarrierCode(thisAirlineCode);
						discountTo.setDiscountCode(appliedDiscountGroup.getChargeGroupCode());
						discountTo.setChargeCode(chargeInfo);
						if (ChargeCodes.FARE_DISCOUNT.equals(reservationPaxFare.getDiscountCode())
								|| ChargeCodes.DOM_FARE_DISCOUNT.equals(reservationPaxFare.getDiscountCode())) {
							chargeInfo = appliedDiscountGroup.getChargeDescription();
						} else {
							chargeInfo = appliedDiscountGroup.getChargeDescription() + " - " + chargeInfo;
						}

						discountTo.setDiscountName(chargeInfo);
						discountTo.setSegmentCode(ondCode);
						discountTo.setDepartureDateTime(departureDate);

						passengerPrice.getDiscount().add(discountTo);
						reservationPrice.getDiscount().add(discountTo);

					}
					chargeHolder.addCharge(reservationPaxOndCharge);
				}
			}

			passengerPrice.setTotalFare(reservationPax.getTotalFare());
			passengerPrice.setCurrencyCodeGroup(baseCurrencyCodeGroup);
			passengerPrice.setTotalFees(AccelAeroCalculator.add(reservationPax.getTotalCancelCharge(),
					reservationPax.getTotalModificationCharge(), reservationPax.getTotalAdjustmentCharge()));
			passengerPrice.setTotalSurcharges(reservationPax.getTotalSurCharge());
			passengerPrice.setTotalTaxes(reservationPax.getTotalTaxCharge());
			
			if (reservation.isInfantPaymentRecordedWithInfant()) {
				passengerPrice.setTotalPrice(reservationPax.getTotalChargeAmountWhenInfantHasPayment());
			} else {
				passengerPrice.setTotalPrice(reservationPax.getTotalChargeAmount());
			}
			passengerPrice.setTotalBalance(reservationPax.getTotalAvailableBalance());
			passengerPrice.getBalance().addAll(composePaxBalances(mapBalances.get(reservationPax.getPnrPaxId())));
			passengerPrice.setTotalDiscount(reservationPax.getTotalDiscount());
			perPaxPriceInfo.setPassengerPrice(passengerPrice);

			// Fare Basis Codes (Same for all PAX in same Reservation)
			// Fare Basis code list is same for all the passengers in a
			// reservation
			Collection<String> fareBasisCodesList = new HashSet<String>();
			for (ReservationSegment reservationSegment : reservation.getSegments()) {
				String fareBasisCode = reservationSegment.getFareTO(null).getFareBasisCode();
				fareBasisCodesList.add(fareBasisCode);
			}

			perPaxPriceInfo.getFareBasisCodes().addAll(fareBasisCodesList);
			priceInfo.getPerPaxPriceInfo().add(perPaxPriceInfo);

		}

		reservationPrice.setCurrencyCodeGroup(baseCurrencyCodeGroup);

		reservationPrice.setTotalFare(reservation.getTotalTicketFare());
		reservationPrice.setTotalSurcharges(reservation.getTotalTicketSurCharge());
		reservationPrice.setTotalTaxes(reservation.getTotalTicketTaxCharge());

		reservationPrice.setTotalFees(AccelAeroCalculator.add(reservation.getTotalTicketCancelCharge(),
				reservation.getTotalTicketModificationCharge(), reservation.getTotalTicketAdjustmentCharge()));
		reservationPrice.setTotalPrice(AccelAeroCalculator.add(reservation.getTotalChargeAmounts()));
		reservationPrice.setTotalBalance(reservation.getTotalAvailableBalance());
		reservationPrice.getBalance().addAll(composePnrBalances(mapBalances.values()));
		reservationPrice.setTotalDiscount(reservation.getTotalDiscount());

		priceInfo.setFareType(reservationPrice);

		return priceInfo;
	}

	private void populateSegmentRPH(BaseFare baseFare, ReservationPaxFare reservationPaxFare) {
		if (reservationPaxFare.getPaxFareSegments() != null) {
			for (ReservationPaxFareSegment paxFareSeg : reservationPaxFare.getPaxFareSegments()) {
				if (paxFareSeg.getSegment() != null) {
					baseFare.getSegmentRPH().add(paxFareSeg.getSegment().getPnrSegId() + "");
				}
			}
		}

	}

	private void populatePNRSegmentID(BaseFare baseFare, ReservationPaxFare reservationPaxFare) {
		if (reservationPaxFare.getPaxFareSegments() != null) {
			for (ReservationPaxFareSegment paxFareSeg : reservationPaxFare.getPaxFareSegments()) {
				if (paxFareSeg.getSegment() != null) {
					baseFare.setPnrSegmentID(paxFareSeg.getSegment().getPnrSegId());
				}
			}
		}

	}

	private void populatePNRSegmentID(Surcharge surCharge, ReservationPaxFare reservationPaxFare) {
		if (reservationPaxFare.getPaxFareSegments() != null) {
			for (ReservationPaxFareSegment paxFareSeg : reservationPaxFare.getPaxFareSegments()) {
				if (paxFareSeg.getSegment() != null) {
					surCharge.setPnrSegmentID(paxFareSeg.getSegment().getPnrSegId());
				}
			}
		}

	}

	private void populatePNRSegmentID(Tax tax, ReservationPaxFare reservationPaxFare) {
		if (reservationPaxFare.getPaxFareSegments() != null) {
			for (ReservationPaxFareSegment paxFareSeg : reservationPaxFare.getPaxFareSegments()) {
				if (paxFareSeg.getSegment() != null) {
					tax.setPnrSegmentID(paxFareSeg.getSegment().getPnrSegId());
				}
			}
		}

	}

	private Collection<Balance> composePnrBalances(Collection<Collection<ReservationTnx>> values) {
		Collection<Balance> colBalance = new ArrayList<Balance>();

		if (values != null && values.size() > 0) {
			for (Collection<ReservationTnx> colReservationTnx : values) {
				colBalance.addAll(composePaxBalances(colReservationTnx));
			}
		}

		return colBalance;
	}

	private Collection<Balance> composePaxBalances(Collection<ReservationTnx> colReservationTnx) {
		Collection<Balance> colBalance = new ArrayList<Balance>();

		if (colReservationTnx != null && colReservationTnx.size() > 0) {
			for (ReservationTnx reservationTnx : colReservationTnx) {
				Balance balance = new Balance();
				balance.setAmount(reservationTnx.getAmount());
				balance.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
				balance.setApplicableDateTime(new Date()); // reservationTnx.getDateTime());

				colBalance.add(balance);
			}
		}

		return colBalance;
	}

	private Collection<Integer> getPayablePaxIds(Reservation reservation) {
		Collection<Integer> colPnrPaxIds = new ArrayList<Integer>();

		for (ReservationPax reservationPax : reservation.getPassengers()) {
			if (reservation.isInfantPaymentRecordedWithInfant() || !ReservationApiUtils.isInfantType(reservationPax)) {
				colPnrPaxIds.add(reservationPax.getPnrPaxId());
			}
		}

		return colPnrPaxIds;
	}

	private String getFareMetaDesc(ReservationPaxFare reservationPaxFare, FareTO fareTO) {

		String journeyType = "";
		String fareRuleCode = "";

		if (fareTO != null) {
			if (fareTO.isReturnFareRule()) {
				journeyType = "Return";
			} else {
				journeyType = "One Way";
			}

			fareRuleCode = BeanUtils.nullHandler(fareTO.getFareRuleCode());
		}

		if (fareRuleCode.length() == 0) {
			fareRuleCode = " Ad-Hoc ";
		}

		String fareMetaInfo = "";

		Iterator itReservationPaxFareSegment = reservationPaxFare.getPaxFareSegments().iterator();
		ReservationPaxFareSegment reservationPaxFareSegment;
		String strPaxStatus;

		// Assumption made. Only the first segment's pax status is shown as the
		// final pax status
		if (itReservationPaxFareSegment.hasNext()) {
			reservationPaxFareSegment = (ReservationPaxFareSegment) itReservationPaxFareSegment.next();

			strPaxStatus = ReservationApiUtils.getReservationPaxFareSegmentStatus(reservationPaxFareSegment.getStatus());
			fareMetaInfo = "/" + BeanUtils.nullHandler(reservationPaxFareSegment.getBookingCode()) + "/" + fareRuleCode + "/"
					+ journeyType + strPaxStatus;
		}

		return fareMetaInfo;
	}

	private FaresAndChargesTO getFareIdAndFareRuleMap() throws ModuleException {
		Collection<Integer> fareIds = new HashSet<Integer>();
		Collection<Integer> chargeRateIds = new HashSet<Integer>();
		for (ReservationPax reservationPax : reservation.getPassengers()) {
			for (ReservationPaxFare reservationPaxFare : reservationPax.getPnrPaxFares()) {
				for (ReservationPaxOndCharge reservationPaxOndCharge : reservationPaxFare.getCharges()) {
					if (reservationPaxOndCharge.getFareId() != null) {
						fareIds.add(reservationPaxOndCharge.getFareId());
					}

					if (reservationPaxOndCharge.getChargeRateId() != null) {
						chargeRateIds.add(reservationPaxOndCharge.getChargeRateId());
					}
				}
			}
		}
		Map<Integer, FareRule> fareRuleMap = AAServicesModuleUtils.getFareBD().getFareRules(fareIds);
		FaresAndChargesTO faresNCharges = AAServicesModuleUtils.getFareBD().getRefundableStatuses(fareIds, chargeRateIds, null);
		for (Integer fareID : faresNCharges.getFareTOs().keySet()) {
			FareRule fareRule = fareRuleMap.get(fareID);
			FareTO fareTO = faresNCharges.getFareTOs().get(fareID);
			fareTO.setFareRuleCode(fareRule != null ? fareRule.getFareRuleCode() : "");
			faresNCharges.getFareTOs().put(fareID, fareTO);
		}
		return faresNCharges;
	}

	/**
	 * @param mapSegView
	 * @param eTicketTOs
	 * @return
	 * @throws ModuleException
	 */
	private List<Eticket> populateETicketInfo(Map<Integer, ReservationSegmentDTO> mapSegView, Collection<EticketTO> eTicketTOs)
			throws ModuleException {
		List<Eticket> eTickets = new ArrayList<Eticket>();
		if (eTicketTOs != null && !eTicketTOs.isEmpty()) {
			for (EticketTO eticketTO : eTicketTOs) {
				Eticket eTicket = new Eticket();
				ReservationSegmentDTO segmentDTO = mapSegView.get(eticketTO.getPnrSegId());
				eTicket.setFlightSegmentRefNumber(segmentDTO.getFlightSegId().toString());
				eTicket.setPnrSegmentRefNumber(segmentDTO.getPnrSegId().toString());
				eTicket.setAirlineCode(AppSysParamsUtil.getDefaultCarrierCode());
				eTicket.setEticketNumber(eticketTO.getEticketNumber());
				eTicket.setSegmentSequence(segmentDTO.getSegmentSeq());
				eTicket.setCouponNo(eticketTO.getCouponNo());
				eTicket.setTicketStatus(eticketTO.getTicketStatus());
				eTicket.setPaxStatus(eticketTO.getPaxStatus());
				eTicket.setPnrPaxFareSegId(eticketTO.getPnrPaxFareSegId());
				eTicket.setTicketId(eticketTO.getEticketId());
				eTickets.add(eTicket);
			}
		}
		return eTickets;
	}

	/**
	 * Populate the TicketingInfo object of AirReservation
	 * 
	 * @return TicketingInfo
	 * @throws ModuleException
	 */
	private TicketingInfo populateTicketingInfo() throws ModuleException {
		String advisotryText = "";
		TicketingStatus ticketingStatus = null;
		Date ticketTimeLimit = null;

		if (ReservationInternalConstants.ReservationStatus.CONFIRMED.equals(reservation.getStatus())) {
			if (reservation.getTotalAvailableBalance().doubleValue() > 0) {
				String currencyCode = baseCurrencyCodeGroup.getCurrencyCode();
				String totalToPay = reservation.getTotalAvailableBalance() + " [" + currencyCode + "]";
				advisotryText = "Reservation is partially confirmed. Has " + totalToPay + " balance amount to pay";
				ticketingStatus = TicketingStatus.TICKETTYPE_PARTIAL_TICKETED;
			} else {
				advisotryText = "Reservation is fully paid and confirmed.";
				ticketingStatus = TicketingStatus.TICKETTYPE_TICKETED;
			}
		} else if (ReservationInternalConstants.ReservationStatus.ON_HOLD.equals(reservation.getStatus())) {
			String formattedDate = CommonUtil.getFormattedDate(reservation.getReleaseTimeStamps()[0]);
			advisotryText = "Reservation is onhold. To avoid cancellation, pay before " + formattedDate + " GMT";
			ticketingStatus = TicketingStatus.TICKETTYPE_ONHOLD_TICKETED;
			ticketTimeLimit = reservation.getReleaseTimeStamps()[0];
		} else if (ReservationInternalConstants.ReservationStatus.CANCEL.equals(reservation.getStatus())) {
			advisotryText = "Reservation is cancelled.";
			ticketingStatus = TicketingStatus.TICKETTYPE_CANCELLED;
		}

		/* TicketingInfo */
		TicketingInfo ticketingInfo = new TicketingInfo();
		ticketingInfo.setTicketingStatus(ticketingStatus);
		ticketingInfo.setTicketAdvisory(advisotryText);
		ticketingInfo.setTicketTimeLimit(ticketTimeLimit);
		ticketingInfo.setTicketType(Ticket.ETICKET);
		ticketingInfo.setTicketBookingDate(reservation.getZuluBookingTimestamp());

		Collection privilegeKeys = (Collection) AASessionManager.getInstance()
				.getCurrUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);
		boolean isAnyModifyBufferPriExist = AuthorizationUtil.hasPrivilege(privilegeKeys,
				PrivilegesKeys.AlterResPrivilegesKeys.ALT_BUFFERTIME_SEG_OR_PAX)
				|| AuthorizationUtil.hasPrivilege(privilegeKeys, PrivilegesKeys.AlterResPrivilegesKeys.ALT_BUFFERTIME_CHARGES)
				|| AuthorizationUtil.hasPrivilege(privilegeKeys, PrivilegesKeys.AlterResPrivilegesKeys.ALT_BUFFERTIME_NAME)
				|| AuthorizationUtil.hasPrivilege(privilegeKeys, PrivilegesKeys.AlterResPrivilegesKeys.ALT_BUFFERTIME_SSR)
				|| AuthorizationUtil.hasPrivilege(privilegeKeys, PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_ALLOW_FARE_BUFFER);

		Map<Integer, Date[]> modifiableDateTimes = ReservationValidationUtils.getResModifiableTillDateTime(reservation);
		TicketModifiableStates ticketModifiableStates;

		if (modifiableDateTimes != null && modifiableDateTimes.size() > 0) {

			for (Integer pnrSegId : modifiableDateTimes.keySet()) {
				Date[] dates = modifiableDateTimes.get(pnrSegId);

				ticketModifiableStates = new TicketModifiableStates();
				ticketModifiableStates.setFlightRefNumber(BeanUtils.nullHandler(pnrSegId));
				ticketModifiableStates.setTicketModifyTillBufferDateTime(dates[0]);

				// If any modify buffer privileges exists. We are saying that
				// the booking can be modifiable till the
				// flight closure
				if (isAnyModifyBufferPriExist) {
					ticketModifiableStates.setTicketModifyTillFlightClosureDateTime(dates[1]);
				} else {
					ticketModifiableStates.setTicketModifyTillFlightClosureDateTime(dates[0]);
				}

				ticketModifiableStates.setTicketCancelTillBufferDateTime(dates[2]);

				ticketingInfo.getTicketModifiableStates().add(ticketModifiableStates);
			}
		}

		return ticketingInfo;
	}

	private List<InsuranceRequest> populateInsurances() {
		List<InsuranceRequest> insReqs = new ArrayList<InsuranceRequest>();
		List<ReservationInsurance> resInsurances = reservation.getReservationInsurance();

		InsuranceRequest insReq = null;
		if (resInsurances != null && !resInsurances.isEmpty()) {

			for (ReservationInsurance resIns : resInsurances)
				if (resIns != null && resIns.getInsuranceId() != null) {
					insReq = new InsuranceRequest();
					insReq.setInsuranceRefNumber(resIns.getInsuranceId());
					insReq.setPoliceCode(resIns.getPolicyCode());
					insReq.setAmount(resIns.getAmount());
					insReq.setOrigin(resIns.getOrigin());
					insReq.setDestination(resIns.getDestination());
					insReq.setDateOfTravel(resIns.getDateOfTravel());
					insReq.setDateOfReturn(resIns.getDateOfReturn());
					insReq.setState(resIns.getState());
					insReq.setMessageId(resIns.getMessageId());
					insReq.setAlertAutoCancellation(resIns.getAutoCancellationId() != null);

					insReqs.add(insReq);
				}
		}

		return insReqs;
	}

	private Collection<SeatRequest> populateSeats(Map<Integer, String> mapPaxRef,
			Map<Integer, ReservationSegmentDTO> mapSegView) {
		List<SeatRequest> seatReq = new ArrayList<SeatRequest>();
		Collection<PaxSeatTO> seats = reservation.getSeats();
		if (seats != null) {
			for (PaxSeatTO seat : seats) {
				SeatRequest s = new SeatRequest();
				s.setSeatCharge(seat.getChgDTO().getAmount());
				s.setSeatNumber(seat.getSeatCode());
				// s.setFlightRefNumber(mapSegView.get(seat.getPnrSegId()).getFlightSegId()+"");
				s.setFlightRefNumber(FlightRefNumberUtil.composeFlightRPH(mapSegView.get(seat.getPnrSegId())));
				s.setTravelerRefNumber(mapPaxRef.get(seat.getPnrPaxId()));
				s.setAlertAutoCancellation(seat.getAutoCancellationId() != null);
				seatReq.add(s);
			}
		}
		return seatReq;
	}

	private Collection<BookMealRequest> populateMeals(Map<Integer, String> mapPaxRef,
			Map<Integer, ReservationSegmentDTO> mapSegView) {
		List<BookMealRequest> mealReq = new ArrayList<BookMealRequest>();
		Collection<PaxMealTO> meals = reservation.getMeals();
		if (meals != null) {
			for (PaxMealTO meal : meals) {
				BookMealRequest bmReq = new BookMealRequest();
				bmReq.setMealCode(meal.getMealCode());
				bmReq.setMealCategoryID(meal.getMealCategoryID());
				bmReq.setMealCharge(meal.getChgDTO().getAmount());
				String ucs = "";
				if (meal.getTranslatedMealName() == null
						|| StringUtil.getUnicode(meal.getTranslatedMealName()).equalsIgnoreCase("null")) {
					bmReq.setMealName(meal.getMealName());
				} else {
					ucs = StringUtil.getUnicode(meal.getTranslatedMealName());
					ucs = ucs.replace("^", ",");
					bmReq.setMealName(ucs);
				}
				// bmReq.setFlightRefNumber(mapSegView.get(meal.getPnrSegId()).getFlightSegId()+"");
				bmReq.setFlightRefNumber(FlightRefNumberUtil.composeFlightRPH(mapSegView.get(meal.getPnrSegId())));
				bmReq.setTravelerRefNumber(mapPaxRef.get(meal.getPnrPaxId()));
				bmReq.setAlertAutoCancellation(meal.getAutoCancellationId() != null);
				mealReq.add(bmReq);
			}
		}
		return mealReq;
	}

	private Collection<BookBaggageRequest> populateBaggages(Map<Integer, String> mapPaxRef,
			Map<Integer, ReservationSegmentDTO> mapSegView) {
		List<BookBaggageRequest> baggageReq = new ArrayList<BookBaggageRequest>();
		Collection<PaxBaggageTO> baggages = reservation.getBaggages();
		if (baggages != null) {
			for (PaxBaggageTO baggage : baggages) {
				ReservationSegmentDTO reservationSegmentDTO = mapSegView.get(baggage.getPnrSegId());
				if (ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(reservationSegmentDTO.getStatus())) {
					BookBaggageRequest bbReq = new BookBaggageRequest();
					bbReq.setBaggageName(baggage.getBaggageName());
					bbReq.setBaggageDescription(baggage.getBaggageDescription());
					bbReq.setBaggageCharge(baggage.getChgDTO().getAmount());
					bbReq.setBaggageTemplateChargeId(baggage.getBaggageChrgId());
					bbReq.setFlightRefNumber(FlightRefNumberUtil.composeFlightRPH(reservationSegmentDTO));
					bbReq.setTravelerRefNumber(mapPaxRef.get(baggage.getPnrPaxId()));
					bbReq.setAlertAutoCancellation(baggage.getAutoCancellationId() != null);
					baggageReq.add(bbReq);
				}

			}
		}
		return baggageReq;
	}

	/**
	 * Populate the TravelerInfo object of AirReservation
	 * 
	 * @return TravelerInfo
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	@SuppressWarnings("unchecked")
	private TravelerInfo populateTravelerInfo() throws ModuleException, WebservicesException {
		TravelerInfo travelerInfo = new TravelerInfo();

		List<ReservationSegmentDTO> segViewList = (List<ReservationSegmentDTO>) reservation.getSegmentsView();
		Map<Integer, ReservationSegmentDTO> mapSegView = new HashMap<Integer, ReservationSegmentDTO>();
		for (ReservationSegmentDTO segView : segViewList) {
			mapSegView.put(segView.getPnrSegId(), segView);
		}
		
		Collection<Integer> blacklistedPnrPaxId = ReservationModuleUtils.getBlacklisPAXBD().getBLackListPaxIdVsPnrPaxId(reservation.getPnr());
		List<SpecialServiceRequest> serviceReqList = new ArrayList<SpecialServiceRequest>();
		List<AirportServiceRequest> airportServiceReqList = new ArrayList<AirportServiceRequest>();
		Map<Integer, String> mapPaxRef = new HashMap<Integer, String>();
		for (ReservationPax reservationPax : reservation.getPassengers()) {

			/* AirTraveler */
			AirTraveler airTraveler = new AirTraveler();

			airTraveler.setStatus(reservationPax.getStatus());

			if (reservationPax.getPaxSSR() != null) {
				for (PaxSSRDTO paxSSR : reservationPax.getPaxSSR()) {
					if (paxSSR.getAirportCode() == null) {
						// If Airport Code is null then SSR is in-flight service
						SpecialServiceRequest ssr = new SpecialServiceRequest();
						ssr.setSsrCode(paxSSR.getSsrCode());
						ssr.setDescription(paxSSR.getSsrDesc());
						ssr.setText(paxSSR.getSsrText());
						ssr.setCharge(new BigDecimal(paxSSR.getChargeAmount()));
						ssr.setFlightRefNumber(FlightRefNumberUtil.composeFlightRPH(mapSegView.get(paxSSR.getPnrSegId())));
						ssr.setTravelerRefNumber(AAReservationUtil.getTravelerRef(reservationPax));
						ssr.setAlertAutoCancellation(paxSSR.getAutoCancellationId() != null);
						serviceReqList.add(ssr);

					} else {
						// If Airport code isn't then SSR is airport service
						AirportServiceRequest apsRequest = new AirportServiceRequest();
						apsRequest.setSsrCode(paxSSR.getSsrCode());
						apsRequest.setAirportCode(paxSSR.getAirportCode());
						apsRequest.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
						SSRInfoDTO ssrInfoDTO = SSRUtil.getSSRInfo(paxSSR.getSsrCode());
						if (ssrInfoDTO != null) {
							apsRequest.setSsrName(ssrInfoDTO.getSSRName());
						}
						apsRequest.setDescription(paxSSR.getSsrDesc());
						apsRequest.setFlightRefNumber(FlightRefNumberUtil.composeFlightRPH(mapSegView.get(paxSSR.getPnrSegId())));
						apsRequest.setTravelerRefNumber(AAReservationUtil.getTravelerRef(reservationPax));
						apsRequest.setServiceCharge(new BigDecimal(paxSSR.getChargeAmount()));
						apsRequest.setAlertAutoCancellation(paxSSR.getAutoCancellationId() != null);
						airportServiceReqList.add(apsRequest);
					}

				}
			}
			// Accompanied by Infant
			boolean accompaniedByInfant = false;
			if (reservationPax.getInfants() != null && reservationPax.getInfants().size() > 0) {
				accompaniedByInfant = true;
			}
			airTraveler.setAccompaniedByInfant(accompaniedByInfant);

			// Address
			airTraveler.setAddress(null);

			// Birth Date
			Date dtOfBirth = reservationPax.getDateOfBirth();
			airTraveler.setBirthDate(XMLDataTypeUtil.getXMLGregorianCalendar(dtOfBirth));

			// Email
			airTraveler.setEmail(null);

			// Form of Identification
			FormOfIdentification formOfIdentification = new FormOfIdentification();

			ReservationPaxAdditionalInfo paxAddnInfo = (reservationPax.getPaxAdditionalInfo() == null)
					? new ReservationPaxAdditionalInfo()
					: reservationPax.getPaxAdditionalInfo();

			formOfIdentification.setFoidNumber(paxAddnInfo.getPassportNo());
			if (paxAddnInfo.getPassportExpiry() != null) {
				formOfIdentification.setPsptExpiry(XMLDataTypeUtil.getXMLGregorianCalendar(paxAddnInfo.getPassportExpiry()));
			}
			formOfIdentification.setPsptIssuedCntry(paxAddnInfo.getPassportIssuedCntry());
			formOfIdentification.setPaxCategoryFOIDId(
					(paxAddnInfo.getAdditionalInfoId() != null) ? paxAddnInfo.getAdditionalInfoId().toString() : "");
			formOfIdentification.setPlaceOfBirth(paxAddnInfo.getPlaceOfBirth());
			formOfIdentification.setTravelDocumentType(paxAddnInfo.getTravelDocumentType());
			formOfIdentification.setVisaDocNumber(paxAddnInfo.getVisaDocNumber());
			if (paxAddnInfo.getVisaDocIssueDate() != null) {
				formOfIdentification
						.setVisaDocIssueDate(XMLDataTypeUtil.getXMLGregorianCalendar(paxAddnInfo.getVisaDocIssueDate()));
			}
			formOfIdentification.setVisaDocPlaceOfIssue(paxAddnInfo.getVisaDocPlaceOfIssue());
			formOfIdentification.setVisaApplicableCountry(paxAddnInfo.getVisaApplicableCountry());
			airTraveler.setFormOfIdentification(formOfIdentification);

			// Gender
			airTraveler.setGender(null);

			// Nationality Code
			airTraveler.setNationalityCode(AAUtils.getNationalityIsoCode(reservationPax.getNationalityCode()));

			// Pax Type
			airTraveler.setPassengerType(PaxTypeUtils.convertAAPaxCodeToLCCPaxCodes(reservationPax.getPaxType()));

			// Pax Name
			PersonName person = new PersonName();
			person.setTitle(reservationPax.getTitle());
			person.setFirstName(reservationPax.getFirstName());
			person.setMiddleName("");
			person.setSurName(reservationPax.getLastName());
			airTraveler.setPersonName(person);

			// Pax Tel
			airTraveler.setTelephone(null);

			// Pax RefNum
			String travelerRef = AAReservationUtil.getTravelerRef(reservationPax);

			airTraveler.setTravelerRefNumber(travelerRef);
			airTraveler.setTravelerSequence(reservationPax.getPaxSequence());
			airTraveler.setAccompaniedSequence(reservationPax.getAccompaniedSequence());

			// Pax Category
			airTraveler.setPassengerCategory(reservationPax.getPaxCategory());

			mapPaxRef.put(reservationPax.getPnrPaxId(), travelerRef);

			// set e ticket info
			airTraveler.getETickets().addAll(populateETicketInfo(mapSegView, reservationPax.geteTickets()));

			airTraveler.setAlertAutoCancellation(reservationPax.getAutoCancellationId() != null);

			airTraveler.setFfid(paxAddnInfo.getFfid());
			
			//set is already Blacklisted in this reservation
			if(blacklistedPnrPaxId!=null && !blacklistedPnrPaxId.isEmpty()){
				airTraveler.setBlacklisted(blacklistedPnrPaxId.contains(reservationPax.getPnrPaxId()));
			}
			travelerInfo.getAirTraveler().add(airTraveler);
		}

		SpecialReqDetails specialReqDetails = new SpecialReqDetails();
		specialReqDetails.getInsuranceRequests().addAll(populateInsurances());
		specialReqDetails.getSeatRequests().addAll(populateSeats(mapPaxRef, mapSegView));
		specialReqDetails.getMealsRequests().addAll(populateMeals(mapPaxRef, mapSegView));
		specialReqDetails.getBaggagesRequests().addAll(populateBaggages(mapPaxRef, mapSegView));
		specialReqDetails.getSpecialServiceRequests().addAll(serviceReqList);
		specialReqDetails.getAirportServiceRequests().addAll(airportServiceReqList);
		travelerInfo.setSpecialReqDetails(specialReqDetails);

		return travelerInfo;
	}

	/**
	 * Populate the UserNote object of AirReservation
	 * 
	 * @return UserNote
	 * @throws ModuleException
	 */
	private UserNote populateLastUserNote() throws ModuleException {
		UserNote lastUserNote = new UserNote();
		lastUserNote.getCarrierCode().add(thisAirlineCode);

		Collection<ReservationModificationDTO> userNotesHistory = AAServicesModuleUtils.getAuditorServiceBD().getUserNotes(
				aaReadRQ.getPnr(), false, null);

		if (userNotesHistory != null && userNotesHistory.size() > 0) {
			ReservationModificationDTO lastReservationModificationDTO = BeanUtils.getFirstElement(userNotesHistory);

			lastUserNote.setAction(null);
			lastUserNote.setNoteText(lastReservationModificationDTO.getUserNote());
			lastUserNote.setTimestamp(lastReservationModificationDTO.getZuluModificationDate());
			lastUserNote.setSystemText(lastReservationModificationDTO.getDescription());
			lastUserNote.setUsername(lastReservationModificationDTO.getUserId());
			lastUserNote.setUserNoteType(lastReservationModificationDTO.getUserNoteType());
		} else {
			lastUserNote.setAction(null);
			lastUserNote.setNoteText(reservation.getUserNote());
			lastUserNote.setTimestamp(null);
			lastUserNote.setSystemText(null);
			lastUserNote.setUsername(null);
			lastUserNote.setUserNoteType(reservation.getUserNoteType());
		}

		return lastUserNote;
	}

	private long populateNameChangeCutoffTime() throws ModuleException {
		return AppSysParamsUtil.getPassenegerNameModificationCutoffTimeInMillis();
	}

	private String populateStationWiseNameChangeInfo() throws ModuleException {
		String firstDepartingStation = "";
		if (reservation.getSegmentsView() != null) {
			Date depDate = null;
			Date prevDate = null;

			for (ReservationSegmentDTO reservationSegment : reservation.getSegmentsView()) {
				depDate = reservationSegment.getDepartureDate();
				if ((prevDate == null || (prevDate != null && (prevDate.compareTo(depDate) > 0)))
						&& ReservationInternalConstants.ReservationSegmentStatus.CONFIRMED.equals(reservationSegment.getStatus())) {

					firstDepartingStation = reservationSegment.getSegmentCode().substring(0,
							reservationSegment.getSegmentCode().indexOf("/"));
				}
				prevDate = depDate;
			}
			return AAServicesModuleUtils.getLocationServiceBD().getNameChangeThresholTimePerStation(firstDepartingStation);
		}
		return null;
	}

	/**
	 * Populate the BookedFlightAlertInfo object of AirReservation
	 * 
	 * @return BookedFlightAlertInfo
	 * @throws ModuleException
	 */
	private List<BookedFlightAlertInfo> populateAlertInfo() throws ModuleException {
		List<BookedFlightAlertInfo> bookedFlightAlertInfos = new ArrayList<BookedFlightAlertInfo>();
		AlertingBD alertingBD = AAServicesModuleUtils.getAlertingBD();
		Set<ReservationSegment> reservationSegments = reservation.getSegments();
		Collection<Integer> pnrSegIds = new ArrayList<Integer>();

		for (ReservationSegment reservationSegment : reservationSegments) {
			pnrSegIds.add(reservationSegment.getPnrSegId());
		}

		// alerts for pnrSegIds
		Map mapPnrSegIdAlerts = alertingBD.getAlertsForPnrSegments(pnrSegIds);

		Collection<Alert> alerts;
		for (Integer pnrSegId : pnrSegIds) {
			BookedFlightAlertInfo bookedFlightAlertInfo = new BookedFlightAlertInfo();
			alerts = (Collection<Alert>) mapPnrSegIdAlerts.get(new Long(pnrSegId.intValue()));
			List<AlertInfo> alertInfos = new ArrayList<AlertInfo>();
			if (alerts != null) {
				for (Alert alert : alerts) {
					AlertInfo alertInfo = new AlertInfo();
					alertInfo.setContent(alert.getContent());
					alertInfo.setAlertId(alert.getAlertId().intValue());
					alertInfo.setVisibleIBEonly(alert.getVisibleIbeOnly());
					alertInfo.setActionedByIBE(alert.getIbeActoined());
					alertInfo.setHasFlightSegments(alert.getFligthSegIds().size() == 0 ? false : true);
					alertInfos.add(alertInfo);
				}
				bookedFlightAlertInfo.setFlightRefNumber(pnrSegId.toString());
				bookedFlightAlertInfo.getAlert().addAll(alertInfos);
				bookedFlightAlertInfo.setCarrierCode(thisAirlineCode);
				bookedFlightAlertInfos.add(bookedFlightAlertInfo);
			}
		}
		return bookedFlightAlertInfos;
	}

	private void captureCarrierPaymentDetails(Map<Collection<Integer>, Collection<ReservationTnx>> payTypeAndColReservationTnx,
			List<PaymentDetails> lstPaymentDetails) throws ModuleException {

		// Check the type of each transaction then add
		for (Collection<Integer> colPayTypeNominalCodes : payTypeAndColReservationTnx.keySet()) {

			Collection<ReservationTnx> colReservationTnx = payTypeAndColReservationTnx.get(colPayTypeNominalCodes);

			/* If it's a Cash payment */
			if (colPayTypeNominalCodes.equals(ReservationTnxNominalCode.getCashTypeNominalCodes())) {

				for (ReservationTnx reservationTnx : colReservationTnx) {
					PaymentDetails paymentDetails = AAPaymentDetailsFactory.getCashPaymentDetails(reservationTnx,
							baseCurrencyCodeGroup, thisAirlineCode);
					lstPaymentDetails.add(paymentDetails);
				}

				/* If it's a On Account payment */
			} else if (colPayTypeNominalCodes.equals(ReservationTnxNominalCode.getOnAccountTypeNominalCodes())) {

				for (ReservationTnx reservationTnx : colReservationTnx) {
					PaymentDetails paymentDetails = AAPaymentDetailsFactory.getOnAccountPaymentDetails(reservationTnx,
							baseCurrencyCodeGroup, thisAirlineCode);
					lstPaymentDetails.add(paymentDetails);
				}

				/* If it's a On BSP payment */
			} else if (colPayTypeNominalCodes.equals(ReservationTnxNominalCode.getBSPAccountTypeNominalCodes())) {

				for (ReservationTnx reservationTnx : colReservationTnx) {
					PaymentDetails paymentDetails = AAPaymentDetailsFactory.getBSPAccountPaymentDetails(reservationTnx,
							baseCurrencyCodeGroup, thisAirlineCode);
					lstPaymentDetails.add(paymentDetails);
				}

				/* If it's a Credit payment */
			} else if (colPayTypeNominalCodes.equals(ReservationTnxNominalCode.getCreditTypeNominalCodes())) {

				for (ReservationTnx reservationTnx : colReservationTnx) {
					PaymentDetails paymentDetails = AAPaymentDetailsFactory.getPaxCreditPaymentDetails(reservationTnx,
							baseCurrencyCodeGroup, thisAirlineCode);
					lstPaymentDetails.add(paymentDetails);
				}

				/* If it's a Credit Card payment */
			} else if (colPayTypeNominalCodes.equals(ReservationTnxNominalCode.getVisaTypeNominalCodes())
					|| colPayTypeNominalCodes.equals(ReservationTnxNominalCode.getMasterTypeNominalCodes())
					|| colPayTypeNominalCodes.equals(ReservationTnxNominalCode.getAmexTypeNominalCodes())
					|| colPayTypeNominalCodes.equals(ReservationTnxNominalCode.getDinersTypeNominalCodes())
					|| colPayTypeNominalCodes.equals(ReservationTnxNominalCode.getGenericTypeNominalCodes())
					|| colPayTypeNominalCodes.equals(ReservationTnxNominalCode.getCMITypeNominalCodes())) {

				for (ReservationTnx reservationTnx : colReservationTnx) {
					PaymentDetails paymentDetails = AAPaymentDetailsFactory.getCreditCardPaymentDetails(reservationTnx,
							baseCurrencyCodeGroup, thisAirlineCode);
					lstPaymentDetails.add(paymentDetails);
				}
			} else if (colPayTypeNominalCodes.equals(ReservationTnxNominalCode.getLoyaltyNominalCodes())) {
				for (ReservationTnx reservationTnx : colReservationTnx) {
					PaymentDetails paymentDetails = AAPaymentDetailsFactory.getLoyaltyPaymentDetails(reservationTnx,
							baseCurrencyCodeGroup, thisAirlineCode);
					lstPaymentDetails.add(paymentDetails);
				}
			} else if (colPayTypeNominalCodes.equals(ReservationTnxNominalCode.getVoucherNominalCodes())) {

				for (ReservationTnx reservationTnx : colReservationTnx) {
					PaymentDetails paymentDetails = AAPaymentDetailsFactory.getVoucherPaymentDetails(reservationTnx,
							baseCurrencyCodeGroup, thisAirlineCode);
					lstPaymentDetails.add(paymentDetails);
				}

				/* If it's a Voucher payment */
			}
		}
	}

	private void updateNominalMap(Map<Collection<Integer>, Collection<ReservationTnx>> payTypesNominalMap,
			Collection<Integer> colNominalCodes, ReservationTnx reservationTnx) {
		Collection<ReservationTnx> colPayTypeReservationTnx;

		if (payTypesNominalMap.containsKey(colNominalCodes)) {
			colPayTypeReservationTnx = payTypesNominalMap.get(colNominalCodes);
			colPayTypeReservationTnx.add(reservationTnx);
		} else {
			colPayTypeReservationTnx = new ArrayList<ReservationTnx>();
			colPayTypeReservationTnx.add(reservationTnx);
			payTypesNominalMap.put(colNominalCodes, colPayTypeReservationTnx);
		}
	}

	private static String getDepartureAirportCode(String segmentCode) {
		String departureAirportCode = "";

		if (segmentCode != null) {
			String[] airportCodes = segmentCode.split("/");
			if (airportCodes.length > 0) {
				departureAirportCode = airportCodes[0];
			}
		}

		return departureAirportCode;
	}

	private ModificationParamRQInfo getModificationParamRQInfo() {
		ModifcationParamRQ paramRQ = aaReadRQ.getModificationParamsRQ();
		if (paramRQ != null) {
			ModificationParamRQInfo paramRQInfo = new ModificationParamRQInfo();
			if (paramRQ.getAppIndicator().equals(AppIndicator.W)) {
				paramRQInfo.setAppIndicator(AppIndicatorEnum.APP_IBE);
			} else if (paramRQ.getAppIndicator().equals(AppIndicator.C)) {
				paramRQInfo.setAppIndicator(AppIndicatorEnum.APP_XBE);
			}
			paramRQInfo.setIsRegisteredUser(paramRQ.isIsRegisteredUser());

			return paramRQInfo;
		} else {
			return null;
		}
	}

	private static SegmentConnTimeInfo getAirportMinMaxTransitDuration(ReservationSegmentDTO reservationSegmentDTO)
			throws ModuleException {

		String flightNo = reservationSegmentDTO.getFlightNo();
		String segmentCode = reservationSegmentDTO.getSegmentCode();

		SegmentConnTimeInfo segmentConnTimeInfo = null;
		boolean isGroundSegment = false;

		if (!StringUtil.isNullOrEmpty(segmentCode) && isContainGroundSegment(
				AAServicesModuleUtils.getAirportBD().getCachedOwnAirportMap(getAirportCollection(segmentCode)))) {
			isGroundSegment = true;
		}

		if (AppSysParamsUtil.isGroundServiceEnabled() && !StringUtil.isNullOrEmpty(flightNo) && !isGroundSegment) {

			String[] airportCodes = segmentCode.split("/");
			String departureAirport = "";
			String arrivalAirport = "";

			String[] minMaxTransitDurations = null;

			if (airportCodes != null && airportCodes.length >= 2) {

				segmentConnTimeInfo = new SegmentConnTimeInfo();

				Set<String> busConnectingAirports = CommonsServices.getGlobalConfig().getBusConnectingAirports();

				String airlineCarrier = flightNo.substring(0, 2);
				String busCarrier = AAServicesModuleUtils.getFlightBD().getDefaultBusCarrierCode();

				departureAirport = airportCodes[0];
				arrivalAirport = airportCodes[airportCodes.length - 1];

				// Departure airport of air segment is connecting to bus
				// segments
				if (busConnectingAirports.contains(departureAirport) && !StringUtil.isNullOrEmpty(busCarrier)) {

					minMaxTransitDurations = CommonsServices.getGlobalConfig().getMixMaxTransitDurations(departureAirport,
							busCarrier, airlineCarrier);

					segmentConnTimeInfo.setDepAirportMaxConTime(minMaxTransitDurations[1]);
					segmentConnTimeInfo.setDepAirportMinConTime(minMaxTransitDurations[0]);

				}

				// Arrival airport of air segment is connecting to bus segments
				if (busConnectingAirports.contains(arrivalAirport) && !StringUtil.isNullOrEmpty(busCarrier)) {

					minMaxTransitDurations = CommonsServices.getGlobalConfig().getMixMaxTransitDurations(arrivalAirport,
							airlineCarrier, busCarrier);

					segmentConnTimeInfo.setArriAirportMaxConTime(minMaxTransitDurations[1]);
					segmentConnTimeInfo.setArriAirportMinConTime(minMaxTransitDurations[0]);
				}

			}

		}

		return segmentConnTimeInfo;
	}

	public static Collection<String> getAirportCollection(String segmentCode) {
		Collection<String> airports = Arrays.asList(segmentCode.split("/"));
		return airports;
	}

	public static boolean isContainGroundSegment(Map<String, CachedAirportDTO> airportCodes) {
		Collection<CachedAirportDTO> airports = airportCodes.values();
		for (CachedAirportDTO airport : airports) {
			if (airport.isSurfaceSegment()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Converts refundable charge detail data from ThinAir to LCC objects.
	 */
	private List<StringRefundableDetailsMap> populateRefundableChargeDetails() {
		List<StringRefundableDetailsMap> results = new ArrayList<StringRefundableDetailsMap>();

		// If refundable charge details are null return the empty list.
		if (reservation.getRefundableChargeDetails() == null) {
			return results;
		}

		Map<Integer, String> pnrPaxIdToTravellerRefMap = getTravellerRefMap();
		for (Entry<Integer, Collection<RefundableChargeDetailDTO>> entry : reservation.getRefundableChargeDetails().entrySet()) {
			StringRefundableDetailsMap resultEntry = new StringRefundableDetailsMap();
			resultEntry.setKey(pnrPaxIdToTravellerRefMap.get(entry.getKey()));

			List<RefundableChargeDetail> lccRefundableChargeDetails = new ArrayList<RefundableChargeDetail>();
			for (RefundableChargeDetailDTO refundableChargeDTO : entry.getValue()) {
				RefundableChargeDetail detail = new RefundableChargeDetail();
				detail.setChargeType(refundableChargeDTO.getChargeType());
				detail.setPnrSegID(refundableChargeDTO.getPnrSegID());
				detail.getPnrPaxOndChgIDs().addAll(refundableChargeDTO.getPnrPaxOndChgIDs());
				detail.getPpfIDs().addAll(refundableChargeDTO.getPpfIDs());

				List<StringDecimalMap> chargeDetails = new ArrayList<StringDecimalMap>();
				for (Entry<String, BigDecimal> chargeEntry : refundableChargeDTO.getChargeAmountMap().entrySet()) {
					StringDecimalMap chargeResultEntry = new StringDecimalMap();
					chargeResultEntry.setKey(chargeEntry.getKey());
					chargeResultEntry.setValue(chargeEntry.getValue());
					chargeDetails.add(chargeResultEntry);
				}
				detail.getChargeAmountMap().addAll(chargeDetails);
				lccRefundableChargeDetails.add(detail);
			}
			resultEntry.getValue().addAll(lccRefundableChargeDetails);
			results.add(resultEntry);
		}

		return results;
	}

	/**
	 * Gets a Map of traveler references.
	 * 
	 * @return Map<Integer, String> Key -> pnrPaxID, Value -> traveler reference.
	 */
	private Map<Integer, String> getTravellerRefMap() {
		Map<Integer, String> travellerRefMap = new HashMap<Integer, String>();
		for (ReservationPax pax : reservation.getPassengers()) {
			travellerRefMap.put(pax.getPnrPaxId(), AAReservationUtil.getTravelerRef(pax));
		}
		return travellerRefMap;
	}

	private LCCPromotionInfo populatePromoInfo() throws ModuleException {
		LCCPromotionInfo lccPromotionInfo = null;
		if (reservation.getPromotionId() != null) {
			PromotionCriteriaTO promoCriteriaTO = AAServicesModuleUtils.getPromotionCriteriaAdminBD()
					.findPromotionCriteria(reservation.getPromotionId());
			if (promoCriteriaTO != null) {
				lccPromotionInfo = new LCCPromotionInfo();
				lccPromotionInfo.setPromoCodeId(promoCriteriaTO.getPromoCriteriaID());
				lccPromotionInfo.setPromoType(promoCriteriaTO.getPromotionCriteriaType());
				lccPromotionInfo.setDiscountType(promoCriteriaTO.getDiscountType());
				if (promoCriteriaTO.getApplyTO() != null && !"".equals(promoCriteriaTO.getApplyTO())) {
					for (DiscountApplyTo discAppltTo : DiscountApplyTo.values()) {
						if (discAppltTo.ordinal() == Integer.parseInt(promoCriteriaTO.getApplyTO())) {
							lccPromotionInfo.setDiscountApplyTo(discAppltTo.getName());
						}
					}
				}
				lccPromotionInfo.setDiscountValue(
						(promoCriteriaTO.getDiscountValue() != null) ? promoCriteriaTO.getDiscountValue().floatValue() : 0);
				lccPromotionInfo.setDiscountAs(promoCriteriaTO.getApplyAs());
				lccPromotionInfo.setModificationAllowed(!promoCriteriaTO.getRestrictAppliedResModification());
				lccPromotionInfo.setCancellationAllowed(!promoCriteriaTO.getRestrictAppliedResCancelation());
				lccPromotionInfo.setSplitAllowed(!promoCriteriaTO.getRestrictAppliedResSplitting());
				lccPromotionInfo.setRemovePaxAllowed(!promoCriteriaTO.getRestrictAppliedResRemovePax());
				if (promoCriteriaTO.getFreeCriteria() != null) {
					Collection<String> freeCriterias = promoCriteriaTO.getFreeCriteria().values();
					if (freeCriterias != null) {
						String freeCriteria = freeCriterias.iterator().next();
						String[] paxTypeFree = freeCriteria
								.split(PromotionCriteriaConstants.PromotionCriteriaFormatConstants.BUY_N_GET_QUANTITY_SEPERATOR);

						lccPromotionInfo.setAppliedAdults(isALLCombinationForBuyNGetFree(paxTypeFree[0])
								? reservation.getTotalPaxAdultCount()
								: Integer.parseInt(paxTypeFree[0]));
						lccPromotionInfo.setAppliedChilds(isALLCombinationForBuyNGetFree(paxTypeFree[1])
								? reservation.getTotalPaxChildCount()
								: Integer.parseInt(paxTypeFree[1]));
						lccPromotionInfo.setAppliedInfants(isALLCombinationForBuyNGetFree(paxTypeFree[2])
								? reservation.getTotalPaxInfantCount()
								: Integer.parseInt(paxTypeFree[2]));
					}
				}
				if (promoCriteriaTO.getApplicableAncillaries() != null && !promoCriteriaTO.getApplicableAncillaries().isEmpty()) {
					lccPromotionInfo.getAppliedAncis().addAll(promoCriteriaTO.getApplicableAncillaries());
				}

				if (promoCriteriaTO.getApplicableBINs() != null && !promoCriteriaTO.getApplicableBINs().isEmpty()) {
					lccPromotionInfo.getApplicableBINs().addAll(promoCriteriaTO.getApplicableBINs());
				}

				lccPromotionInfo.setSystemGenerated(promoCriteriaTO.getSystemGenerated());

				if (PromotionCriteriaConstants.DiscountApplyAsTypes.CREDIT.equals(promoCriteriaTO.getApplyAs())) {
					lccPromotionInfo.setCreditDiscountAmount(getCreditPromotionDiscount(reservation));
				}
			}
		}
		return lccPromotionInfo;
	}

	private List<LCCBundledFare> populateBundledFareInfo() throws ModuleException {

		List<LCCBundledFare> lccBundledFareDtos = new ArrayList<LCCBundledFare>();
		Map<Integer, List<Integer>> bundledFareSegmentIds = new HashMap<Integer, List<Integer>>();
		Map<Integer, List<Integer>> bundledFareJourneySeqIds = new HashMap<Integer, List<Integer>>();
		List<Integer> journeyWithoutBundleFare = new ArrayList<Integer>();

		Collection<ReservationSegmentDTO> setSegs = reservation.getSegmentsView();
		for (ReservationSegmentDTO segDto : setSegs) {
			boolean isBundleFare = false;
			if (segDto.getBundledFarePeriodId() != null
					&& !ReservationInternalConstants.ReservationStatus.CANCEL.equals(segDto.getStatus())) {

				if (!bundledFareSegmentIds.containsKey(segDto.getBundledFarePeriodId())) {
					bundledFareSegmentIds.put(segDto.getBundledFarePeriodId(), new ArrayList<Integer>());
				}
				bundledFareSegmentIds.get(segDto.getBundledFarePeriodId()).add(segDto.getPnrSegId());

				if (segDto.getBundledFarePeriodId() != null) {
					if (!bundledFareJourneySeqIds.containsKey(segDto.getBundledFarePeriodId())) {
						bundledFareJourneySeqIds.put(segDto.getBundledFarePeriodId(), new ArrayList<Integer>());
					}

					bundledFareJourneySeqIds.get(segDto.getBundledFarePeriodId()).add(segDto.getJourneySeq());
					isBundleFare = true;
				}
			}
			if (!isBundleFare) {
				journeyWithoutBundleFare.add(segDto.getJourneySeq());
			}
		}

		if (!bundledFareJourneySeqIds.isEmpty()) {
			Set<Integer> uniqueBundledFarePeriodIds = new HashSet<Integer>(bundledFareJourneySeqIds.keySet());
			if (uniqueBundledFarePeriodIds != null && !uniqueBundledFarePeriodIds.isEmpty()) {
				List<BundledFareDTO> bundledFareDTOs = AAServicesModuleUtils.getBundledFareBD()
						.getBundledFareDTOsByBundlePeriodIds(uniqueBundledFarePeriodIds);

				if (bundledFareDTOs != null) {

					TreeMap<Integer, BundledFareDTO> ondBundledFareMap = new TreeMap<Integer, BundledFareDTO>();

					Map<Integer, BundledFareDTO> bundledMap = new HashMap<Integer, BundledFareDTO>();
					for (BundledFareDTO bundledFareDTO : bundledFareDTOs) {
						bundledMap.put(bundledFareDTO.getBundledFarePeriodId(), bundledFareDTO);
					}

					for (Entry<Integer, BundledFareDTO> bundledFareEntry : bundledMap.entrySet()) {
						Integer bundledFarePeriodId = bundledFareEntry.getKey();
						List<Integer> journeySeqs = bundledFareJourneySeqIds.get(bundledFarePeriodId);

						for (Integer journeySeq : journeySeqs) {
							ondBundledFareMap.put(journeySeq, bundledFareEntry.getValue());
						}

					}

					Iterator<Integer> journeyWithoutBundleFareItr = journeyWithoutBundleFare.iterator();
					while (journeyWithoutBundleFareItr.hasNext()) {
						Integer journeySeqId = journeyWithoutBundleFareItr.next();
						ondBundledFareMap.put(journeySeqId, null);
					}

					for (Entry<Integer, BundledFareDTO> entry : ondBundledFareMap.entrySet()) {
						BundledFareDTO bundledFareDTO = entry.getValue();
						LCCBundledFare lccBundledFare = new LCCBundledFare();
						if (bundledFareDTO != null) {
							lccBundledFare.setBundledFareName(bundledFareDTO.getBundledFareName());
							lccBundledFare.setBundledFarePeriodId(bundledFareDTO.getBundledFarePeriodId());
							lccBundledFare.setPerPaxBundledFee(bundledFareDTO.getPerPaxBundledFee());
							if (bundledFareDTO.getBookingClasses() != null && !bundledFareDTO.getBookingClasses().isEmpty()) {
								lccBundledFare.getBookingClasses().addAll(bundledFareDTO.getBookingClasses());
							}

							Set<BundledFareFreeServiceTO> freeServices = bundledFareDTO.getApplicableServices();
							Set<LCCBundledFareFreeService> lccFreeServices = new HashSet<LCCBundledFareFreeService>();
							if (freeServices != null && !freeServices.isEmpty()) {
								for (BundledFareFreeServiceTO bunFareFreeServiceTO : freeServices) {
									LCCBundledFareFreeService lccBundledFareFreeService = new LCCBundledFareFreeService();
									lccBundledFareFreeService.setFreeServiceId(bunFareFreeServiceTO.getId());
									lccBundledFareFreeService.setServiceName(bunFareFreeServiceTO.getServiceName());
									lccBundledFareFreeService.setTemplateId(bunFareFreeServiceTO.getTemplateId());
									lccFreeServices.add(lccBundledFareFreeService);
								}
							}
							lccBundledFare.getFreeServices().addAll(lccFreeServices);
						}

						lccBundledFareDtos.add(lccBundledFare);
					}

				}
			}

		}

		return lccBundledFareDtos;
	}

	private List<NoShowRefundableInfo> populateNoShowRefundableInfo() throws ModuleException {
		List<NoShowRefundableInfo> noShowPaxList = new ArrayList<NoShowRefundableInfo>(); 
		NoShowRefundableInfo noShowPaxInfo;
		for(ReservationPax reservationPax : reservation.getPassengers()){
			noShowPaxInfo = new NoShowRefundableInfo();
			noShowPaxInfo.setTravellerRefNo(AAReservationUtil.getTravelerRef(reservationPax));
			noShowPaxInfo.setAmount(reservationPax.getNoShowRefundableAmount());
			noShowPaxInfo.setIsRefundable(reservationPax.isNoShowRefundable());
			noShowPaxList.add(noShowPaxInfo);
		}
		
		return noShowPaxList;
	}
	
	private static String getFareType(String fareType) {
		int i = FareTypes.OTHER_FARE;
		if (!StringUtil.isNullOrEmpty(fareType)) {
			if (FareTypeCodes.SEGMENT.equals(fareType)) {
				i = FareTypes.PER_FLIGHT_FARE;
			} else if (FareTypeCodes.CONNECTION.equals(fareType)) {
				i = FareTypes.OND_FARE;
			} else if (FareTypeCodes.RETURN.equals(fareType)) {
				i = FareTypes.RETURN_FARE;
			} else if (FareTypeCodes.HALF_RETURN.equals(fareType)) {
				i = FareTypes.HALF_RETURN_FARE;
			}
		}
		return Integer.toString(i);
	}

	private static boolean isALLCombinationForBuyNGetFree(String count) {
		return count.equals(PromotionCriteriaConstants.BUY_AND_GET_FREE_ALL) ? true : false;
	}

	public static BigDecimal getCreditPromotionDiscount(Reservation reservation) {

		BigDecimal creditPromotionDiscount = AccelAeroCalculator.getDefaultBigDecimalZero();
		for (ReservationPax reservationPax : reservation.getPassengers()) {
			for (ReservationPaxFare reservationPaxFare : reservationPax.getPnrPaxFares()) {
				for (ReservationPaxOndCreditPromotion paxOndCreditPromotion : reservationPaxFare.getChargesPromotionCredits()) {
					if (paxOndCreditPromotion.getDiscount() != null && paxOndCreditPromotion.getDiscount().doubleValue() != 0) {
						creditPromotionDiscount = AccelAeroCalculator.add(creditPromotionDiscount,
								paxOndCreditPromotion.getDiscount());
					}
				}
			}
		}
		return creditPromotionDiscount;
	}
}