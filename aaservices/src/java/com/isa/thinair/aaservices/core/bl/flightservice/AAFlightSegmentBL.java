package com.isa.thinair.aaservices.core.bl.flightservice;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.required.dto.AAFlightSegmentRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAFlightSegmentRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.airschedules.api.model.FlightSegement;

public class AAFlightSegmentBL {

	private static Log log = LogFactory.getLog(AAFlightSegmentBL.class);

	private AAFlightSegmentRQ aaFlightSegmentRQ;

	public AAFlightSegmentRS execute() {

		AAFlightSegmentRS aaFlightSegmentRS = new AAFlightSegmentRS();
		aaFlightSegmentRS.setResponseAttributes(new AAResponseAttributes());
		FlightSegement flightSegment;

		try {
			flightSegment = AAServicesModuleUtils.getFlightBD().getFlightSegmentForLocalDate(
					aaFlightSegmentRQ.getFlightNumber(), aaFlightSegmentRQ.getDepartureDateLocal());
			if (flightSegment != null) {
				aaFlightSegmentRS.setArrivalDateLocal(flightSegment.getEstTimeArrivalLocal());
				aaFlightSegmentRS.setArrivalDateZulu(flightSegment.getEstTimeArrivalZulu());
				aaFlightSegmentRS.setDepartureDateLocal(flightSegment.getEstTimeDepatureLocal());
				aaFlightSegmentRS.setDepartureDateZulu(flightSegment.getEstTimeDepatureZulu());
				aaFlightSegmentRS.setFlightSegmentId(flightSegment.getFltSegId());
				aaFlightSegmentRS.setSegmentCode(flightSegment.getSegmentCode());
				
			}
		} catch (Exception e) {
			log.error("Error occurred while fetching flight segment segment", e);
			AAExceptionUtil.addAAErrrors(aaFlightSegmentRS.getResponseAttributes().getErrors(), e);

		} finally {
			int numOfErrors = aaFlightSegmentRS.getResponseAttributes().getErrors().size();
			AASuccess successType = (numOfErrors == 0 ? new AASuccess() : null);
			aaFlightSegmentRS.getResponseAttributes().setSuccess(successType);
		}

		return aaFlightSegmentRS;
	}

	public AAFlightSegmentRQ getAaFlightSegmentRQ() {
		return aaFlightSegmentRQ;
	}

	public void setAaFlightSegmentRQ(AAFlightSegmentRQ aaFlightSegmentRQ) {
		this.aaFlightSegmentRQ = aaFlightSegmentRQ;
	}

}
