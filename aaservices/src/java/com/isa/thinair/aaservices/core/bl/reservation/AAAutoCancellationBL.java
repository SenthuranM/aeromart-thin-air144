package com.isa.thinair.aaservices.core.bl.reservation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.ExistingModificationCharges;
import com.isa.connectivity.profiles.maxico.v1.common.dto.LCCAutoCancellationInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.StringIntegerListMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.StringStringListMap;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAutoCancellationBalanceToPayRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAutoCancellationBalanceToPayRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAutoCancellationQueryRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAutoCancellationQueryRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAutoCancellationRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAutoCancellationRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAutoCancellationTimeRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAutoCancellationTimeRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAExistingMoficationChargesRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAExistingMoficationChargesRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.core.session.AASessionManager;
import com.isa.thinair.aaservices.core.session.AAUserSession;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.TransformerUtil;
import com.isa.thinair.airproxy.api.utils.PaxTypeUtils;
import com.isa.thinair.airproxy.api.utils.PrivilegesKeys;
import com.isa.thinair.airreservation.api.dto.CustomChargesTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.model.AutoCancellationInfo;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.utils.AuthorizationsUtil;
import com.isa.thinair.airreservation.api.utils.AutoCancellationUtil;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.PassengerType;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;

public class AAAutoCancellationBL {
	private static final Log log = LogFactory.getLog(AAAutoCancellationBL.class);

	@SuppressWarnings("unchecked")
	public AAAutoCancellationTimeRS calculateAutoCancellationTime(AAAutoCancellationTimeRQ aaAutoCancellationTimeRQ) {
		String pnr = aaAutoCancellationTimeRQ.getCarrierPnr();
		AAAutoCancellationTimeRS aaAutoCancellationTimeRS = new AAAutoCancellationTimeRS();
		aaAutoCancellationTimeRS.setCarrier(AppSysParamsUtil.getDefaultCarrierCode());
		aaAutoCancellationTimeRS.setResponseAttributes(new AAResponseAttributes());

		try {
			Collection<String> privilegeKeys = (Collection<String>) AASessionManager.getInstance().getCurrUserSessionParam(
					AAUserSession.USER_PRIVILEGES_KEYS);
			boolean hasBufferTimeAutoCnxPriv = AuthorizationsUtil.hasPrivilege(privilegeKeys,
					PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_ALT_RES_AUTO_CNX);

			Date firstDepartingDateTime = aaAutoCancellationTimeRQ.getFirstDepartingDateTime();
			Reservation reservation = null;

			// calculate first departing date from existing segments, if RQ doesn't pass the value
			if (firstDepartingDateTime == null && pnr != null && !aaAutoCancellationTimeRQ.isGetExistingCnxTimeOnly()) {
				reservation = AutoCancellationUtil.loadReservation(pnr);
				firstDepartingDateTime = getFirstDepartingDate(reservation.getSegmentsView());
			}

			AutoCancellationInfo autoCancellationInfo = AAServicesModuleUtils.getReservationBD().getAutoCancellationInfo(pnr,
					reservation, firstDepartingDateTime, aaAutoCancellationTimeRQ.getModifiedSegId(),
					aaAutoCancellationTimeRQ.getCancellationType(), hasBufferTimeAutoCnxPriv,
					aaAutoCancellationTimeRQ.isGetExistingCnxTimeOnly());

			if (autoCancellationInfo != null && autoCancellationInfo.getExpireOn() != null) {
				LCCAutoCancellationInfo lccAutoCnx = new LCCAutoCancellationInfo();
				lccAutoCnx.setCancellationId(autoCancellationInfo.getAutoCancellationId());
				lccAutoCnx.setCancellationType(autoCancellationInfo.getCancellationType());
				lccAutoCnx.setExpireOn(autoCancellationInfo.getExpireOn());
				lccAutoCnx.setVersion(autoCancellationInfo.getVersion());
				aaAutoCancellationTimeRS.setAutoCancellationInfo(lccAutoCnx);
			}

		} catch (ModuleException e) {
			log.error("Error in calculating auto cancellation time for PNR " + pnr, e);
			AAExceptionUtil.addAAErrrors(aaAutoCancellationTimeRS.getResponseAttributes().getErrors(), e);
		} finally {
			int numOfErrors = aaAutoCancellationTimeRS.getResponseAttributes().getErrors().size();
			AASuccess successType = (numOfErrors == 0 ? new AASuccess() : null);
			aaAutoCancellationTimeRS.getResponseAttributes().setSuccess(successType);
		}

		return aaAutoCancellationTimeRS;
	}

	public AAAutoCancellationRS saveOrUpdateAutoCancellationInfo(AAAutoCancellationRQ aaAutoCancellationRQ) {
		AAAutoCancellationRS aaAutoCancellationRS = new AAAutoCancellationRS();
		aaAutoCancellationRS.setResponseAttributes(new AAResponseAttributes());

		if (aaAutoCancellationRQ.isUpdateSchedulerStatus()) {
			if (!aaAutoCancellationRQ.getAutoCnxIds().isEmpty()) {
				try {
					AAServicesModuleUtils.getReservationBD().updateAutoCancelSchedulerStatus(
							aaAutoCancellationRQ.getAutoCnxIds(), AutoCancellationInfo.PROCESSED);
				} catch (ModuleException e) {
					log.error("Error in updating auto cancellation scheduler status", e);
				}
			}
			aaAutoCancellationRS.getResponseAttributes().setSuccess(new AASuccess());
		} else {
			AutoCancellationInfo autoCancellationInfo = AAAutoCancellationBL.populateAutoCancellation(aaAutoCancellationRQ
					.getAutoCancellationInfo());
			try {
				autoCancellationInfo = AAServicesModuleUtils.getReservationBD().saveAutoCancellationInfo(autoCancellationInfo,
						aaAutoCancellationRQ.getPnr(), null);
			} catch (ModuleException e) {
				log.error("Error in saving auto cancellation for PNR " + aaAutoCancellationRQ.getPnr(), e);
				AAExceptionUtil.addAAErrrors(aaAutoCancellationRS.getResponseAttributes().getErrors(), e);
			} finally {
				int numOfErrors = aaAutoCancellationRS.getResponseAttributes().getErrors().size();
				AASuccess successType = (numOfErrors == 0 ? new AASuccess() : null);
				aaAutoCancellationRS.getResponseAttributes().setSuccess(successType);
			}
		}
		return aaAutoCancellationRS;
	}

	public AAAutoCancellationQueryRS getExpiredReservations(AAAutoCancellationQueryRQ aaAutoCancellationQueryRQ) {
		AAAutoCancellationQueryRS aaAutoCancellationQueryRS = new AAAutoCancellationQueryRS();
		aaAutoCancellationQueryRS.setResponseAttributes(new AAResponseAttributes());

		try {
			List<Integer> autoCnxIds = AAServicesModuleUtils.getReservationBD().getExpiredCancellationIDs(
					CalendarUtil.getCurrentSystemTimeInZulu());

			if (autoCnxIds != null && !autoCnxIds.isEmpty()) {

				Map<String, Integer> autoCancellableInfo = AAServicesModuleUtils.getReservationBD().getAutoCancellableInfo(
						aaAutoCancellationQueryRQ.getMarketingCarrier(), autoCnxIds);

				aaAutoCancellationQueryRS.getAutoCnxInfo().addAll(TransformerUtil.convertMapToLCCList(autoCancellableInfo));

			}

		} catch (ModuleException me) {
			log.error("Error in retrieving auto cancellable for PNRs ", me);
			AAExceptionUtil.addAAErrrors(aaAutoCancellationQueryRS.getResponseAttributes().getErrors(), me);
		} finally {
			int numOfErrors = aaAutoCancellationQueryRS.getResponseAttributes().getErrors().size();
			AASuccess successType = (numOfErrors == 0 ? new AASuccess() : null);
			aaAutoCancellationQueryRS.getResponseAttributes().setSuccess(successType);
		}

		return aaAutoCancellationQueryRS;
	}

	public AAExistingMoficationChargesRS getExistingModificationChargesDue(
			AAExistingMoficationChargesRQ aaExistingMoficationChargesRQ) {
		AAExistingMoficationChargesRS aaExistingMoficationChargesRS = new AAExistingMoficationChargesRS();
		aaExistingMoficationChargesRS.setResponseAttributes(new AAResponseAttributes());

		try {
			List<StringIntegerListMap> intGrpWisePnrSegMap = aaExistingMoficationChargesRQ.getIntGrpWisePnrSegs();
			if (intGrpWisePnrSegMap != null && !intGrpWisePnrSegMap.isEmpty()) {
				for (StringIntegerListMap mapObj : intGrpWisePnrSegMap) {
					String interlineGroupKey = mapObj.getKey();
					List<Integer> pnrSegId = mapObj.getValue();

					Map<String, BigDecimal> paxWiseModCharge = AAServicesModuleUtils.getSegmentBD()
							.getExistingModificationChargesDue(pnrSegId);

					if (paxWiseModCharge != null && !paxWiseModCharge.isEmpty()) {
						ExistingModificationCharges modChargeDue = new ExistingModificationCharges();
						modChargeDue.setInterlineGrpKey(interlineGroupKey);
						if (paxWiseModCharge.containsKey(PassengerType.ADULT)) {
							modChargeDue.setAdultCharge(paxWiseModCharge.get(PassengerType.ADULT));
						}
						if (paxWiseModCharge.containsKey(PassengerType.CHILD)) {
							modChargeDue.setChildCharge(paxWiseModCharge.get(PassengerType.CHILD));
						}
						if (paxWiseModCharge.containsKey(PassengerType.INFANT)) {
							modChargeDue.setInfantCharge(paxWiseModCharge.get(PassengerType.INFANT));
						}
						aaExistingMoficationChargesRS.getModificationCharges().add(modChargeDue);
					}
				}
			}
		} catch (ModuleException me) {
			log.error("Error in retrieving existing modification charges due ", me);
			AAExceptionUtil.addAAErrrors(aaExistingMoficationChargesRS.getResponseAttributes().getErrors(), me);
		} finally {
			int numOfErrors = aaExistingMoficationChargesRS.getResponseAttributes().getErrors().size();
			AASuccess successType = (numOfErrors == 0 ? new AASuccess() : null);
			aaExistingMoficationChargesRS.getResponseAttributes().setSuccess(successType);
		}

		return aaExistingMoficationChargesRS;
	}

	public AAAutoCancellationBalanceToPayRS calculateAutoCancellationBalanceToPay(
			AAAutoCancellationBalanceToPayRQ aaBalanceToPayRQ) {
		AAAutoCancellationBalanceToPayRS aaBalanceToPayRS = new AAAutoCancellationBalanceToPayRS();
		aaBalanceToPayRS.setResponseAttributes(new AAResponseAttributes());

		try {
			String pnr = aaBalanceToPayRQ.getPnr();
			Long version = Long.parseLong(aaBalanceToPayRQ.getVersion());
			List<Integer> removingInfantIds = getRemovingInfantIds(aaBalanceToPayRQ.getCancelingInfants());
			Map<String, CustomChargesTO> groupWiseCustomCharges = populateGroupWiseCustomChargeMap(aaBalanceToPayRQ
					.getExistingModificationChargeDue());
			Map<String, Collection<Integer>> groupWisePnrSegMap = populateGroupWisePnrSegs(aaBalanceToPayRQ
					.getGroupWisePnrSegMap());

			BigDecimal balanceToPay = AAServicesModuleUtils.getReservationBD().calculateBalanceToPayAfterAutoCancellation(
					groupWisePnrSegMap, removingInfantIds, groupWiseCustomCharges, pnr, version);

			aaBalanceToPayRS.setBalanceToPay(balanceToPay);

		} catch (ModuleException me) {
			log.error("Error in calculating auto cancellation balance of PNR " + aaBalanceToPayRQ.getPnr() + " to pay amount ",
					me);
			AAExceptionUtil.addAAErrrors(aaBalanceToPayRS.getResponseAttributes().getErrors(), me);
		} finally {
			int numOfErrors = aaBalanceToPayRS.getResponseAttributes().getErrors().size();
			AASuccess successType = (numOfErrors == 0 ? new AASuccess() : null);
			aaBalanceToPayRS.getResponseAttributes().setSuccess(successType);
		}

		return aaBalanceToPayRS;
	}

	public static AutoCancellationInfo populateAutoCancellation(LCCAutoCancellationInfo lccAutoCancellationInfo) {
		AutoCancellationInfo autoCancellationInfo = new AutoCancellationInfo();
		autoCancellationInfo.setAutoCancellationId(lccAutoCancellationInfo.getCancellationId());
		autoCancellationInfo.setCancellationType(lccAutoCancellationInfo.getCancellationType());
		autoCancellationInfo.setExpireOn(lccAutoCancellationInfo.getExpireOn());
		autoCancellationInfo.setProcessedByScheduler(AutoCancellationInfo.NOT_PROCESSED);
		autoCancellationInfo.setVersion(lccAutoCancellationInfo.getVersion());
		return autoCancellationInfo;
	}

	private Date getFirstDepartingDate(Collection<ReservationSegmentDTO> colResSeg) {

		ReservationSegmentDTO firstDeptFltSeg = null;
		for (ReservationSegmentDTO reservationSegment : colResSeg) {
			if (!ReservationInternalConstants.ReservationSegmentStatus.CANCEL.equals(reservationSegment.getStatus())) {
				if (firstDeptFltSeg == null) {
					firstDeptFltSeg = reservationSegment;
				} else {
					if (firstDeptFltSeg.getZuluDepartureDate().after(reservationSegment.getZuluDepartureDate())) {
						firstDeptFltSeg = reservationSegment;
					}
				}
			}
		}

		if (firstDeptFltSeg != null) {
			return firstDeptFltSeg.getZuluDepartureDate();
		}

		return null;
	}

	private Map<String, Collection<Integer>> populateGroupWisePnrSegs(List<StringStringListMap> groupWisePnrSegList) {
		Map<String, Collection<Integer>> groupWisePnrSegMap = null;
		if (groupWisePnrSegList != null && !groupWisePnrSegList.isEmpty()) {
			groupWisePnrSegMap = new HashMap<String, Collection<Integer>>();
			for (StringStringListMap mapObj : groupWisePnrSegList) {
				if (mapObj.getValue() != null && !mapObj.getValue().isEmpty()) {
					if (!groupWisePnrSegMap.containsKey(mapObj.getKey())) {
						groupWisePnrSegMap.put(mapObj.getKey(), new ArrayList<Integer>());
					}

					for (String strPnrSegId : mapObj.getValue()) {
						groupWisePnrSegMap.get(mapObj.getKey()).add(Integer.parseInt(strPnrSegId));
					}
				}
			}
		}
		return groupWisePnrSegMap;
	}

	private List<Integer> getRemovingInfantIds(List<String> infantRefs) {
		List<Integer> infantIds = null;
		if (infantRefs != null && !infantRefs.isEmpty()) {
			infantIds = new ArrayList<Integer>();
			for (String infantRef : infantRefs) {
				infantIds.add(PaxTypeUtils.getPnrPaxId(infantRef));
			}
		}

		return infantIds;
	}

	private Map<String, CustomChargesTO> populateGroupWiseCustomChargeMap(
			List<ExistingModificationCharges> lccExistingCustomCharges) {
		Map<String, CustomChargesTO> groupWiseCustomChargeTOs = null;

		if (lccExistingCustomCharges != null && !lccExistingCustomCharges.isEmpty()) {
			groupWiseCustomChargeTOs = new HashMap<String, CustomChargesTO>();
			for (ExistingModificationCharges lccCustomCharge : lccExistingCustomCharges) {
				if (!groupWiseCustomChargeTOs.containsKey(lccCustomCharge.getInterlineGrpKey())) {
					CustomChargesTO customChargesTO = new CustomChargesTO();
					customChargesTO.setCompareWithExistingModificationCharges(true);
					customChargesTO.setExistingModificationChargeAdult(lccCustomCharge.getAdultCharge());
					customChargesTO.setExistingModificationChargeChild(lccCustomCharge.getChildCharge());
					customChargesTO.setExistingModificationChargeInfant(lccCustomCharge.getInfantCharge());
					groupWiseCustomChargeTOs.put(lccCustomCharge.getInterlineGrpKey(), customChargesTO);
				}
			}
		}

		return groupWiseCustomChargeTOs;
	}
}
