package com.isa.thinair.aaservices.core.bl.lms;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.LMSMember;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAMergeLoyaltymemberRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAMergeLoyaltymemberRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aircustomer.api.model.LmsMember;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class AAMergeMemberManagementBL {
	private static Log log = LogFactory.getLog(AAMergeMemberManagementBL.class);

	private AAMergeLoyaltymemberRQ aaMergeLoyaltymemberRQ;

	public AAMergeMemberManagementBL(AAMergeLoyaltymemberRQ aaMergeLoyaltymemberRQ) {
		this.aaMergeLoyaltymemberRQ = aaMergeLoyaltymemberRQ;
	}
	
	public AAMergeLoyaltymemberRS execute(){
		AAMergeLoyaltymemberRS aaMergeLoyaltymemberRS = new AAMergeLoyaltymemberRS();
		aaMergeLoyaltymemberRS.setResponseAttributes(new AAResponseAttributes());
		
		if (AppSysParamsUtil.isLMSEnabled()) {
			try {
				LmsMember lmsMember = AAServicesModuleUtils.getLmsMemberServiceBD().getLmsMember(
						aaMergeLoyaltymemberRQ.getMemberFFID());
				String validationString = AAServicesModuleUtils.getLmsMemberServiceBD().getVerificationString(
						aaMergeLoyaltymemberRQ.getMemberFFID());
				int mergeAmount = 0;

				if (validationString != null) {
					mergeAmount = AAServicesModuleUtils.getLmsMemberServiceBD().numberOfMerges(validationString,
							aaMergeLoyaltymemberRQ.getMemberFFID());
				}

				if (lmsMember != null && mergeAmount == 0) {
					// Convert to LCC LMSMember
					LMSMember lccLmsMember = new LMSMember();
					lccLmsMember.setDateOfBirth(lmsMember.getDateOfBirth());
					lccLmsMember.setEnrollmentLocation(lmsMember.getEnrollmentLocExtRef());
					lccLmsMember.setFfid(lmsMember.getFfid());
					lccLmsMember.setFirstName(lmsMember.getFirstName());
					lccLmsMember.setGenderTypeId(lmsMember.getGenderTypeId());
					lccLmsMember.setHeadOfFamily(lmsMember.getHeadOFEmailId());
					lccLmsMember.setLanguage(lmsMember.getLanguage());
					lccLmsMember.setLastName(lmsMember.getLastName());
					lccLmsMember.setLmsMemberId(lmsMember.getLmsMemberId());
					lccLmsMember.setMiddleName(lmsMember.getMiddleName());
					lccLmsMember.setMobileNumber(lmsMember.getMobileNumber());
					lccLmsMember.setNationalityCode(lmsMember.getNationalityCode());
					lccLmsMember.setPassportNum(lmsMember.getPassportNum());
					lccLmsMember.setPassword(lmsMember.getPassword());
					lccLmsMember.setPhoneNumber(lmsMember.getPhoneNumber());
					lccLmsMember.setRefereeEmail(lmsMember.getRefferedEmail());
					lccLmsMember.setResidencyCode(lmsMember.getResidencyCode());
					lccLmsMember.setSbInternalId(lmsMember.getSbInternalId());
					lccLmsMember.setMemberExternalId(lmsMember.getMemberExternalId());
					lccLmsMember.setEnrollingCarrier(lmsMember.getEnrollingCarrier());
					lccLmsMember.setEnrollingChannelExtRef(lmsMember.getEnrollingChannelExtRef());
					lccLmsMember.setEnrollingChannelIntRef(lmsMember.getEnrollingChannelIntRef());
//					lccLmsMember.setConfirmed(lmsMember.getEmailConfirmed()=='Y' ? true : false);
					aaMergeLoyaltymemberRS.setLmsMember(lccLmsMember);
				}
				aaMergeLoyaltymemberRS.getResponseAttributes().setSuccess(new AASuccess());
			} catch (ModuleException e) {
				log.error("MergeMemberManagementBL get Merge Member Account failed.", e);
				AAExceptionUtil.addAAErrrors(aaMergeLoyaltymemberRS.getResponseAttributes().getErrors(), e);
			}
		}

		return aaMergeLoyaltymemberRS;
	}

}
