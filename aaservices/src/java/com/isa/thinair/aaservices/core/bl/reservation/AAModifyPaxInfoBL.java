package com.isa.thinair.aaservices.core.bl.reservation;

import java.util.Collection;

import com.isa.connectivity.profiles.maxico.v1.common.dto.TravelerInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.UserNote;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAirBookModifyRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAErrorCode;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAModificationTypeCode;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.AAUtils;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.ServiceResponce;

class AAModifyPaxInfoBL {

	// Request & Response objects
	private AAAirBookModifyRQ aaAirBookModifyRQ;

	private Reservation reservation;
	private Collection<ReservationAudit> reservationAuditsCol;

	/**
	 * Execute some logic based on the Request object and process and return the Response object
	 * 
	 * @return AAAirBookRS
	 * @throws WebservicesException
	 * @throws ModuleException
	 */
	void execute() throws WebservicesException, ModuleException {
		if (reservation == null) {
			throw new WebservicesException(AAErrorCode.ERR_25_MAXICO_REQUIRED_THE_RESERVATION_NO_LONGER_EXIST,
					"PNR does not exist ");
		}

		if (aaAirBookModifyRQ.getModificationTypeCode() != AAModificationTypeCode.MODTYPE_4_MODIFY_RESERVATION_PAX_INFO) {
			throw new WebservicesException(AAErrorCode.ERR_11_MAXICO_REQUIRED_INVALID_MODIFICATION_TYPE,
					"Invalid Modification Type [" + aaAirBookModifyRQ.getModificationTypeCode().toString() + "] ");
		}

		TravelerInfo travelerInfo = aaAirBookModifyRQ.getAaReservation().getAirReservation().getTravelerInfo();
		AAUtils.updatePaxInfo(travelerInfo, reservation.getPassengers());

		// Map<AirTraveler, ReservationPax> map = new HashMap<AirTraveler, ReservationPax>();
		// Map<Integer, ReservationPax> mapAdultInfant = new HashMap<Integer, ReservationPax>();
		// for (AirTraveler airTraveler : travelerInfo.getAirTraveler()) {
		// for (ReservationPax reservationPax : (Set<ReservationPax>) reservation.getPassengers()) {
		// if (PaxTypeUtils.getPaxSeq(airTraveler.getTravelerRefNumber()).compareTo(reservationPax.getPaxSequence()) ==
		// 0) {
		// map.put(airTraveler, reservationPax);
		// if (ReservationApiUtils.isInfantType(reservationPax)) {
		// int adultSeq = PaxTypeUtils.getParentSeq(airTraveler.getTravelerRefNumber());
		// mapAdultInfant.put(adultSeq, reservationPax);
		// }
		// break;
		// }
		// }
		// }
		//
		// // Update ReservationPax with new info from AirTraveler
		// for (Entry<AirTraveler, ReservationPax> entry : map.entrySet()) {
		// AirTraveler airTraveler = entry.getKey();
		// ReservationPax reservationPax = entry.getValue();
		//
		// PersonName personName = airTraveler.getPersonName();
		// reservationPax.setTitle(BeanUtils.nullHandler(personName.getTitle()));
		// reservationPax.setFirstName(BeanUtils.nullHandler(personName.getFirstName()));
		// reservationPax.setLastName(BeanUtils.nullHandler(personName.getSurName()));
		//
		// Date dateOfBirth = airTraveler.getBirthDate() == null ? null :
		// XMLDataTypeUtil.getDate(airTraveler.getBirthDate());
		// reservationPax.setDateOfBirth(dateOfBirth);
		//
		// String nationalityIsoCode = airTraveler.getNationalityCode();
		// reservationPax.setNationalityCode(AAUtils.getNationalityCode(nationalityIsoCode));
		//
		// if (!ReservationApiUtils.isInfantType(reservationPax)) {
		// Integer paxSeq = PaxTypeUtils.getPaxSeq(airTraveler.getTravelerRefNumber());
		// if (reservationPax.getInfants() != null) {
		// reservationPax.getInfants().clear(); // In case we have unwanted infants attached from before
		// }
		//
		// ReservationPax attachedInfantPax = mapAdultInfant.get(paxSeq);
		//
		// if (attachedInfantPax != null) {
		// attachedInfantPax.setIndexId(reservationPax.getPaxSequence());
		// }
		// }
		//
		// //Set additional info
		// if(airTraveler.getFormOfIdentification() != null){
		// FormOfIdentification foid = airTraveler.getFormOfIdentification();
		// ReservationPaxAdditionalInfo addnInfo = reservationPax.getPaxAdditionalInfo();
		// addnInfo.setPassportNo(foid.getFoidNumber());
		// addnInfo.setPassportExpiry((foid.getPsptExpiry()!=null)?foid.getPsptExpiry().toGregorianCalendar().getTime():null);
		// addnInfo.setPassportIssuedCntry(foid.getPsptIssuedCntry());
		// reservationPax.setPaxAdditionalInfo(addnInfo);
		// }
		// }

		UserNote userNotes = aaAirBookModifyRQ.getAaReservation().getAirReservation().getLastUserNote();
		if (userNotes != null) {
			reservation.setUserNote(userNotes.getNoteText());
		}

		AAUtils.enforceCarrierConfigForPax(reservation.getPassengers(), aaAirBookModifyRQ.getAppIndicator());
		
		ServiceResponce updateReservation = AAServicesModuleUtils.getReservationBD().updateReservation(reservation, null, false,
				true, aaAirBookModifyRQ.getAllowedOperations(), aaAirBookModifyRQ.getReasonForAllowBlPax());

		// Normally all the time the status will be success or it will return an exception.
		if (!updateReservation.isSuccess()) {
			throw new WebservicesException(AAErrorCode.ERR_13_MAXICO_REQUIRED_UNDETERMINED_CAUSE_PLEASE_REPORT, null);
		}

		this.setReservationAuditsCol((Collection<ReservationAudit>) updateReservation
				.getResponseParam(CommandParamNames.RESERVATION_AUDIT_COLLECTION));
	}

	/**
	 * Set the Request object (to be used in the {@link AAModifyPaxInfoBL#execute execute} method)
	 * 
	 * @param aaReadRQ
	 */
	public void setRequest(AAAirBookModifyRQ aaAirBookModifyRQ) {
		this.aaAirBookModifyRQ = aaAirBookModifyRQ;
	}

	/**
	 * @param reservation
	 *            the reservation to set
	 */
	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}

	public Collection<ReservationAudit> getReservationAuditsCol() {
		return reservationAuditsCol;
	}

	public void setReservationAuditsCol(Collection<ReservationAudit> reservationAuditsCol) {
		this.reservationAuditsCol = reservationAuditsCol;
	}

}