package com.isa.thinair.aaservices.core.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.isa.connectivity.profiles.maxico.v1.common.dto.Baggage;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingFlightSegment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightRefNumberRPHList;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentBaggages;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentMeals;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Meal;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PassengerType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PassengerTypeQuantity;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Seat;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SeatMapCabinClass;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SeatMapColumnGroup;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SeatMapDetail;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SeatMapRow;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SeatMapRowGroup;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SeatType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SegmentSeatMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.StringIntegerMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.StringStringListMap;
import com.isa.thinair.aaservices.core.transaction.TransactionalReservationProcess;
import com.isa.thinair.airinventory.api.dto.ClassOfServiceDTO;
import com.isa.thinair.airinventory.api.dto.baggage.FlightBaggageDTO;
import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airinventory.api.dto.meal.FlightMealDTO;
import com.isa.thinair.airinventory.api.dto.seatmap.FlightSeatsDTO;
import com.isa.thinair.airinventory.api.dto.seatmap.SeatDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.IFlightSegment;
import com.isa.thinair.airproxy.api.utils.AncillaryCommonUtil;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.commons.api.dto.LogicalCabinClassDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.promotion.api.to.bundledFare.BundledFareFreeServiceTO;

public class AncillaryUtil {
	/**
	 * Return Flight Segment ID wise Class of Service map
	 * 
	 * @param flightSegment
	 * @return
	 * @throws ModuleException
	 */
	public static Map<Integer, ClassOfServiceDTO> getFlightSegmentIdWiseClassOfServices(FlightSegment flightSegment)
			throws ModuleException {
		Map<Integer, ClassOfServiceDTO> flightSegIdWiseCos = new HashMap<Integer, ClassOfServiceDTO>();

		ClassOfServiceDTO cosDTO = new ClassOfServiceDTO(flightSegment.getCabinClassCode(),
				flightSegment.getLogicalCabinClassCode(), flightSegment.getSegmentCode());

		if (flightSegment.getFlightRefNumber() != null) {
			flightSegIdWiseCos.put(new Integer(flightSegment.getFlightRefNumber()), cosDTO);
			return flightSegIdWiseCos;
		}

		Integer flightId = AAServicesModuleUtils.getFlightBD().getFlightID(flightSegment.getSegmentCode(),
				flightSegment.getFlightNumber(), flightSegment.getDepatureDateTime());

		if (flightId == null) {
			throw new ModuleException("webservices.airinventory.flight.notfound");
		}

		Collection<Integer> flightIds = new ArrayList<Integer>();
		flightIds.add(flightId);

		Collection<FlightSeatsDTO> flightSeatsDTOs = AAServicesModuleUtils.getFlightBD().getFlightSegIdsforFlight(flightIds);

		for (FlightSeatsDTO flightSeatsDTO : flightSeatsDTOs) {
			flightSegIdWiseCos.put(new Integer(flightSeatsDTO.getFlightSegmentID()), cosDTO);
		}

		return flightSegIdWiseCos;
	}

	public static List<Integer> getFlightSegmentIds(FlightSegment flightSegment) throws ModuleException {
		List<Integer> flightSegIds = new ArrayList<Integer>();

		if (flightSegment.getFlightRefNumber() != null) {
			flightSegIds.add(new Integer(flightSegment.getFlightRefNumber()));
			return flightSegIds;
		}

		Integer flightId = AAServicesModuleUtils.getFlightBD().getFlightID(flightSegment.getSegmentCode(),
				flightSegment.getFlightNumber(), flightSegment.getDepatureDateTime());

		if (flightId == null) {
			throw new ModuleException("webservices.airinventory.flight.notfound");
		}

		Collection<Integer> flightIds = new ArrayList<Integer>();
		flightIds.add(flightId);

		Collection<FlightSeatsDTO> flightSeatsDTOs = AAServicesModuleUtils.getFlightBD().getFlightSegIdsforFlight(flightIds);

		for (FlightSeatsDTO flightSeatsDTO : flightSeatsDTOs) {
			flightSegIds.add(new Integer(flightSeatsDTO.getFlightSegmentID()));
		}

		return flightSegIds;
	}

	public static List<SegmentSeatMap> transform(Map<FlightSegment, Map<Integer, FlightSeatsDTO>> flightSegmentSeatMap)
			throws ModuleException {
		List<SegmentSeatMap> segmentSeatMaps = new ArrayList<SegmentSeatMap>();

		Set<Entry<FlightSegment, Map<Integer, FlightSeatsDTO>>> flightSegmentSeatEntrySet = flightSegmentSeatMap.entrySet();
		for (Entry<FlightSegment, Map<Integer, FlightSeatsDTO>> flightSegmentSeatEntry : flightSegmentSeatEntrySet) {
			FlightSegment flightSegment = flightSegmentSeatEntry.getKey();
			Map<Integer, FlightSeatsDTO> flightSegmentIdFlightSeatsMap = flightSegmentSeatEntry.getValue();

			SegmentSeatMap segmentSeatMap = new SegmentSeatMap();
			segmentSeatMap.setFlightSegment(flightSegment);

			SeatMapDetail seatMapDetail = getSeatMapDetails(flightSegmentIdFlightSeatsMap, flightSegment, segmentSeatMap);
			segmentSeatMap.setSeatMapDetails(seatMapDetail);

			segmentSeatMaps.add(segmentSeatMap);
		}

		return segmentSeatMaps;
	}

	public static List<FlightSegmentMeals> getFlightSegmentMeals(Set<BookingFlightSegment> bkFltSegs,
			Map<Integer, List<FlightMealDTO>> mealMap) {

		List<FlightSegmentMeals> flightSegmentMeals = new ArrayList<FlightSegmentMeals>();
		Map<String, LogicalCabinClassDTO> availableLogicalCCMap = CommonsServices.getGlobalConfig().getAvailableLogicalCCMap();

		Set<Entry<Integer, List<FlightMealDTO>>> mealMapEntries = mealMap.entrySet();
		for (Entry<Integer, List<FlightMealDTO>> mealMapEntry : mealMapEntries) {

			Integer flightSegmentID = mealMapEntry.getKey();
			List<FlightMealDTO> flightMealDTOs = mealMapEntry.getValue();

			for (BookingFlightSegment bookingFlightSegment : bkFltSegs) {
				String bookingFlightSegmentId = bookingFlightSegment.getFlightRefNumber();

				if (flightSegmentID.toString().equals(bookingFlightSegmentId)) {
					FlightSegmentMeals flightSegmentMeal = new FlightSegmentMeals();
					flightSegmentMeal.setFlightSegment(bookingFlightSegment);

					String logicalCabinClassCode = bookingFlightSegment.getLogicalCabinClassCode();
					boolean isMultiMealEnabled = AppSysParamsUtil.isMultipleMealSelectionEnabled();

					// Set Logical cabin class level multi meal configuration
					if (logicalCabinClassCode != null && !"".equals(logicalCabinClassCode)) {
						LogicalCabinClassDTO logicalCCObj = availableLogicalCCMap.get(logicalCabinClassCode);
						if (logicalCCObj != null) {
							isMultiMealEnabled = isMultiMealEnabled && !logicalCCObj.isAllowSingleMealOnly();
						}
					}

					flightSegmentMeal.setMultipleMealSelectionEnabled(isMultiMealEnabled);

					for (FlightMealDTO flightMealDTO : flightMealDTOs) {

						if (flightMealDTO.getFlightSegmentID() == Integer.parseInt(bookingFlightSegmentId)) {

							Meal meal = getMealFromMealDTO(flightMealDTO);
							flightSegmentMeal.getMeal().add(meal);
						}
					}
					flightSegmentMeals.add(flightSegmentMeal);
				}
			}
		}
		return flightSegmentMeals;
	}

	public static Meal getMealFromMealDTO(FlightMealDTO flightMealDTO) {
		String mealImgPath = getMealImagePath(flightMealDTO);
		String mealThumbnailImgPath = getMealThumbnailImagePath(flightMealDTO);

		Meal meal = new Meal();
		String ucs = "";
		String catDes = "";
		meal.setMealCharge(flightMealDTO.getAmount());
		meal.setMealCode(flightMealDTO.getMealCode());
		if (flightMealDTO.getTranslatedMealTitle() == null
				|| StringUtil.getUnicode(flightMealDTO.getTranslatedMealTitle()).equalsIgnoreCase("null"))
			meal.setMealName(flightMealDTO.getMealName());
		else {
			ucs = StringUtil.getUnicode(flightMealDTO.getTranslatedMealTitle());
			ucs = ucs.replace("^", ",");
			meal.setMealName(ucs);
		}

		if (flightMealDTO.getTranslatedMealDescription() == null
				|| StringUtil.getUnicode(flightMealDTO.getTranslatedMealDescription()).equalsIgnoreCase("null"))
			meal.setMealDescription(flightMealDTO.getMealDescription());
		else {
			ucs = StringUtil.getUnicode(flightMealDTO.getTranslatedMealDescription());
			ucs = ucs.replace("^", ",");
			meal.setMealDescription(ucs);
		}

		if (flightMealDTO.getTranslatedMealCategory() == null
				|| ("null").equalsIgnoreCase(flightMealDTO.getTranslatedMealCategory())
				|| StringUtil.getUnicode(flightMealDTO.getTranslatedMealCategory()).equalsIgnoreCase("null")) {
			meal.setMealCategoryCode(flightMealDTO.getMealCategoryCode());
		} else {
			catDes = StringUtil.getUnicode(flightMealDTO.getTranslatedMealCategory());
			catDes = catDes.replace("^", ",");
			meal.setMealCategoryCode(catDes);
		}
		meal.setAllocatedMeals(flightMealDTO.getAllocatedMeal());
		meal.setAvailableMeals(flightMealDTO.getAvailableMeals());
		meal.setSoldMeals(flightMealDTO.getSoldMeals());
		meal.setMealImageLink(mealImgPath);
		meal.setMealThumbnailImageLink(mealThumbnailImgPath);
		meal.setMealCategoryID(flightMealDTO.getMealCategoryID());
		meal.setMealID(flightMealDTO.getMealId());
		meal.setMealStatus(flightMealDTO.getMealStatus());
		meal.setPopularity(flightMealDTO.getPopularity());
		return meal;
	}

	public static Collection<Meal> getMealCollectionMealDTO(Collection<FlightMealDTO> flightMealDTOs) {
		Collection<Meal> meals = new ArrayList<Meal>();
		for (FlightMealDTO mealDTO : flightMealDTOs) {
			meals.add(getMealFromMealDTO(mealDTO));
		}
		return meals;
	}

	public static List<FlightSegmentBaggages> getFlightSegmentBaggages(Set<BookingFlightSegment> bkFltSegs,
			Map<Integer, List<FlightBaggageDTO>> baggageMap) {

		List<FlightSegmentBaggages> flightSegmentBaggages = new ArrayList<FlightSegmentBaggages>();

		for (Entry<Integer, List<FlightBaggageDTO>> baggageMapEntry : baggageMap.entrySet()) {

			Integer flightSegmentID = baggageMapEntry.getKey();
			List<FlightBaggageDTO> flightBaggageDTOs = baggageMapEntry.getValue();

			for (BookingFlightSegment bookingFlightSegment : bkFltSegs) {
				String bookingFlightSegmentId = bookingFlightSegment.getFlightRefNumber();

				if (flightSegmentID.toString().equals(bookingFlightSegmentId)) {
					FlightSegmentBaggages flightSegmentBaggage = new FlightSegmentBaggages();
					flightSegmentBaggage.setFlightSegment(bookingFlightSegment);

					flightSegmentBaggage.getBaggage().addAll(transformBaggagDTOCollection(flightBaggageDTOs));

					flightSegmentBaggages.add(flightSegmentBaggage);
				}
			}
		}
		return flightSegmentBaggages;
	}

	public static Baggage transformBaggageDTO(FlightBaggageDTO flightBaggageDTO) {
		Baggage baggage = new Baggage();
		String ucs = "";
		baggage.setBaggageCharge(flightBaggageDTO.getAmount());
		baggage.setBaggageName(flightBaggageDTO.getBaggageName());
		baggage.setDefaultBaggage(flightBaggageDTO.getDefaultBaggage());
		baggage.setBaggageID(flightBaggageDTO.getBaggageId());
		baggage.setIsSelectedInactiveBaggage(flightBaggageDTO.isInactiveSelectedBaggage());
		if (flightBaggageDTO.getTranslatedBaggageDescription() == null
				|| StringUtil.getUnicode(flightBaggageDTO.getTranslatedBaggageDescription()).equalsIgnoreCase("null")) {
			baggage.setBaggageDescription(flightBaggageDTO.getBaggageDescription());
		} else {
			ucs = StringUtil.getUnicode(flightBaggageDTO.getTranslatedBaggageDescription());
			ucs = ucs.replace("^", ",");
			baggage.setBaggageDescription(ucs);
		}
		baggage.setOndGroupId(flightBaggageDTO.getOndGroupId());
		baggage.setOndChargeId(BeanUtils.nullHandler(flightBaggageDTO.getChargeId()));
		return baggage;
	}

	public static Collection<Baggage> transformBaggagDTOCollection(Collection<FlightBaggageDTO> flightBaggageDTOs) {
		Collection<Baggage> resultList = new ArrayList<Baggage>();
		for (FlightBaggageDTO flightBaggageDTO : flightBaggageDTOs) {
			resultList.add(transformBaggageDTO(flightBaggageDTO));
		}
		return resultList;
	}

	public static Map<Integer, ClassOfServiceDTO>
			getFlightSegmentIdWiseClassOfServices(Collection<BookingFlightSegment> bkFltSegs) throws NumberFormatException {
		Map<Integer, ClassOfServiceDTO> flightSegmentIdWiseCos = new HashMap<Integer, ClassOfServiceDTO>();

		for (BookingFlightSegment bookingFlightSegment : bkFltSegs) {

			ClassOfServiceDTO cosDTO = new ClassOfServiceDTO(bookingFlightSegment.getCabinClassCode(),
					bookingFlightSegment.getLogicalCabinClassCode(), bookingFlightSegment.getSegmentCode());
			flightSegmentIdWiseCos.put(new Integer(bookingFlightSegment.getFlightRefNumber()), cosDTO);
		}
		return flightSegmentIdWiseCos;
	}

	public static Collection<FlightBaggageDTO> injectBaggageONDGroupID(Collection<FlightBaggageDTO> flightBaggageDTOs,
			String ondGroupID) {
		for (FlightBaggageDTO flightBaggageDTO : flightBaggageDTOs) {
			flightBaggageDTO.setOndGroupId(ondGroupID);
		}
		return flightBaggageDTOs;
	}

	private static String getMealImagePath(FlightMealDTO flightMealDTO) {
		String strMealPath = AppSysParamsUtil.getImageUploadPath();
		Integer mealId = flightMealDTO.getMealId();

		StringBuilder mealImgPath = new StringBuilder(strMealPath);
		mealImgPath.append("meal_");
		mealImgPath.append(mealId);
		mealImgPath.append(".jpg");// dont need to add the suffix to the meals as they will be uploaded by clients

		return mealImgPath.toString();
	}

	private static String getMealThumbnailImagePath(FlightMealDTO flightMealDTO) {
		String strMealPath = AppSysParamsUtil.getImageUploadPath();
		Integer mealId = flightMealDTO.getMealId();

		StringBuilder mealImgPath = new StringBuilder(strMealPath);
		mealImgPath.append("meal_Thumbnail_");
		mealImgPath.append(mealId);
		mealImgPath.append(".jpg");// dont need to add the suffix to the meals as they will be uploaded by clients

		return mealImgPath.toString();
	}

	public static Set<BookingFlightSegment> getFlightSegmentSet(List<BookingFlightSegment> bookingFlightSegments,
			FlightRefNumberRPHList flightRefNumberRPHList) {
		Set<String> applicableFlightSegs = new HashSet<String>();
		for (String rph : flightRefNumberRPHList.getFlightRefNumber()) {
			applicableFlightSegs.add(String.valueOf(FlightRefNumberUtil.getSegmentIdFromFlightRPH(rph)));
		}

		// Create a set of flight segment ids.
		Set<BookingFlightSegment> bkFltSegs = new HashSet<BookingFlightSegment>();
		for (BookingFlightSegment bookingFlightSegment : bookingFlightSegments) {
			String flightSegId = bookingFlightSegment.getFlightRefNumber();

			if (applicableFlightSegs.contains(flightSegId)) {
				bkFltSegs.add(bookingFlightSegment);
			}
		}

		return bkFltSegs;
	}

	public static Collection<BookingFlightSegment>
			getFlightSegmentSetFromBaggage(List<FlightSegmentBaggages> baggageFLightSegments) {

		Collection<BookingFlightSegment> bkFltSegs = new ArrayList<BookingFlightSegment>();
		for (FlightSegmentBaggages baggageSegment : baggageFLightSegments) {
			bkFltSegs.add(baggageSegment.getFlightSegment());
		}

		return bkFltSegs;
	}

	public static Collection<BookingFlightSegment> getFlightSegmentSetFromMeal(List<FlightSegmentMeals> mealFLightSegments) {

		Collection<BookingFlightSegment> bkFltSegs = new ArrayList<BookingFlightSegment>();
		for (FlightSegmentMeals mealSegment : mealFLightSegments) {
			bkFltSegs.add(mealSegment.getFlightSegment());
		}

		return bkFltSegs;
	}

	public static List<FlightSegmentTO> getFlightSegmentTOs(List<BookingFlightSegment> bookingFlightSegments)
			throws ModuleException {
		List<FlightSegmentTO> flightSegmentTOs = new ArrayList<FlightSegmentTO>();

		for (BookingFlightSegment bookingFlightSegment : bookingFlightSegments) {
			FlightSegmentTO flightSegmentTO = new FlightSegmentTO();

			FlightSegement flightSegment = null;
			Integer flightSegId = null;

			if (!StringUtil.isNullOrEmpty(bookingFlightSegment.getFlightRefNumber())) {
				flightSegId = FlightRefNumberUtil.getSegmentIdFromFlightRPH(bookingFlightSegment.getFlightRefNumber());
				if (flightSegId != null) {
					flightSegment = AAServicesModuleUtils.getFlightBD().getFlightSegment(flightSegId);
				}
			}

			flightSegmentTO.setFlightNumber(bookingFlightSegment.getFlightNumber());
			flightSegmentTO.setAirportCode(bookingFlightSegment.getAirportCode());
			flightSegmentTO.setAirportType(bookingFlightSegment.getAirportType());
			flightSegmentTO.setArrivalDateTime(bookingFlightSegment.getArrivalDateTime());
			flightSegmentTO.setArrivalDateTimeZulu(bookingFlightSegment.getArrivalDateTimeZulu() == null
					? bookingFlightSegment.getArrivalDateTime()
					: bookingFlightSegment.getArrivalDateTimeZulu());
			flightSegmentTO.setDepartureDateTime(bookingFlightSegment.getDepatureDateTime());
			flightSegmentTO.setDepartureDateTimeZulu(bookingFlightSegment.getDepatureDateTimeZulu());

			if (flightSegment != null && flightSegment.getEstTimeArrivalZulu() != null) {
				flightSegmentTO.setArrivalDateTimeZulu(new Date(flightSegment.getEstTimeArrivalZulu().getTime()));
			} else {
				flightSegmentTO.setArrivalDateTimeZulu(bookingFlightSegment.getArrivalDateTimeZulu() == null
						? bookingFlightSegment.getArrivalDateTime()
						: bookingFlightSegment.getArrivalDateTimeZulu());
			}

			if (flightSegment != null && flightSegment.getEstTimeDepatureZulu() != null) {
				flightSegmentTO.setDepartureDateTimeZulu(new Date(flightSegment.getEstTimeDepatureZulu().getTime()));

			} else {
				flightSegmentTO.setDepartureDateTimeZulu(bookingFlightSegment.getDepatureDateTimeZulu() == null
						? bookingFlightSegment.getDepatureDateTime()
						: bookingFlightSegment.getDepatureDateTimeZulu());
			}

			flightSegmentTO.setFlightSegId(Integer.parseInt(bookingFlightSegment.getFlightRefNumber()));
			flightSegmentTO.setOperatingAirline(bookingFlightSegment.getOperatingAirline());
			flightSegmentTO.setOperationType(bookingFlightSegment.getOperationType());
			flightSegmentTO.setReturnFlag("Y".equals(bookingFlightSegment.getReturnFlag()) ? true : false);
			flightSegmentTO.setSegmentCode(bookingFlightSegment.getSegmentCode());
			flightSegmentTO.setCabinClassCode(bookingFlightSegment.getCabinClassCode());
			flightSegmentTO.setLogicalCabinClassCode(bookingFlightSegment.getLogicalCabinClassCode());
			flightSegmentTO.setFlightRefNumber(FlightRefNumberUtil.composeFlightRPH(flightSegmentTO));
			flightSegmentTO.setBaggageONDGroupId(bookingFlightSegment.getBaggageONDGroupId());
			flightSegmentTO.setPnrSegId(bookingFlightSegment.getBookingFlightRefNumber());
			if (bookingFlightSegment.getOndSequence() != null) {
				flightSegmentTO.setOndSequence(bookingFlightSegment.getOndSequence());
			} else {
				flightSegmentTO.setOndSequence(bookingFlightSegment.getJourneySeq());
			}

			flightSegmentTO.setAdultCount(bookingFlightSegment.getAdultCount());
			flightSegmentTO.setChildCount(bookingFlightSegment.getChildCount());
			flightSegmentTO.setInfantCount(bookingFlightSegment.getInfantCount());
			flightSegmentTO.setFareBasisCode(bookingFlightSegment.getFareBasisCode());
			flightSegmentTO
					.setFlexiID(bookingFlightSegment.getFlexiID() == null ? null : bookingFlightSegment.getFlexiID().intValue());
			flightSegmentTO.setParticipatingJourneyReturn(bookingFlightSegment.isParticipatingJourneyReturn());
			flightSegmentTO.setSectorONDCode(bookingFlightSegment.getSectorONDCode());
			flightSegmentTO.setInverseSegmentDeparture(bookingFlightSegment.getInverseSegmentDeparture());
			flightSegmentTO.setBookingClass(bookingFlightSegment.getBookingClass());

			flightSegmentTOs.add(flightSegmentTO);
		}

		return flightSegmentTOs;
	}

	public static Map<String, Integer> convertLCCListToMap(List<StringIntegerMap> mapList) {
		Map<String, Integer> map = new HashMap<String, Integer>();

		for (StringIntegerMap stringIntegerMap : mapList) {
			map.put(stringIntegerMap.getKey(), stringIntegerMap.getValue());
		}

		return map;
	}

	public static AppIndicatorEnum getAppIndicator(String app) {
		AppIndicatorEnum appIndicator = AppIndicatorEnum.APP_XBE;
		if (AppIndicatorEnum.APP_XBE.toString().equals(app)) {
			appIndicator = AppIndicatorEnum.APP_XBE;
		} else if (AppIndicatorEnum.APP_IBE.toString().equals(app)) {
			appIndicator = AppIndicatorEnum.APP_IBE;
		} else if (AppIndicatorEnum.APP_WS.toString().equals(app)) {
			appIndicator = AppIndicatorEnum.APP_WS;
		} else if (AppIndicatorEnum.APP_ANDROID.toString().equals(app)) {
			appIndicator = AppIndicatorEnum.APP_ANDROID;
		} else if (AppIndicatorEnum.APP_IOS.toString().equals(app)) {
			appIndicator = AppIndicatorEnum.APP_IOS;
		}

		return appIndicator;
	}

	public static Map<Integer, Set<String>> getFlightSegIdWiseSelectedAnci(List<StringStringListMap> selectedAnciList) {
		Map<Integer, Set<String>> flightSegIdWiseMap = null;

		if (selectedAnciList != null) {
			flightSegIdWiseMap = new HashMap<Integer, Set<String>>();
			for (StringStringListMap mapObj : selectedAnciList) {
				Integer flightSegId = FlightRefNumberUtil.getSegmentIdFromFlightRPH(mapObj.getKey());
				if (flightSegIdWiseMap.get(flightSegId) == null) {
					flightSegIdWiseMap.put(flightSegId, new HashSet<String>());
				}

				if (mapObj.getValue() != null) {
					flightSegIdWiseMap.get(flightSegId).addAll(mapObj.getValue());
				}

			}
		}

		return flightSegIdWiseMap;
	}

	/**
	 * Construct the seat map details.
	 * 
	 * @param flightSegmentIdFlightSeatsMap
	 * @param flightSegment
	 * @return
	 */
	private static SeatMapDetail getSeatMapDetails(Map<Integer, FlightSeatsDTO> flightSegmentIdFlightSeatsMap,
			FlightSegment flightSegment, SegmentSeatMap owningSegSeatMap) {
		SeatMapDetail seatMapDetail = new SeatMapDetail();

		for (Entry<Integer, FlightSeatsDTO> flightSeatsEntry : flightSegmentIdFlightSeatsMap.entrySet()) {
			FlightSeatsDTO flightSeatsDTO = flightSeatsEntry.getValue();

			owningSegSeatMap.setIsAnciOffer(flightSeatsDTO.isAnciOffer());
			owningSegSeatMap.setOfferName(flightSeatsDTO.getAnciOfferName());
			owningSegSeatMap.setOfferDescription(flightSeatsDTO.getAnciOfferDescription());
			owningSegSeatMap.setOfferedTemplateID(flightSeatsDTO.getOfferedTemplateID());

			// Seats in the flightSegment.
			Collection<SeatDTO> seats = flightSeatsDTO.getSeats();

			// Gets the populated list of all the cabin classes.
			List<SeatMapCabinClass> cabinClass = getCabinClassList(seats, flightSegment);

			seatMapDetail.getCabinClass().addAll(cabinClass);
		}
		return seatMapDetail;
	}

	private static List<SeatMapCabinClass> getCabinClassList(Collection<SeatDTO> seats, FlightSegment flightSegment) {

		List<SeatMapCabinClass> cabinClassList = new ArrayList<SeatMapCabinClass>();

		// A map of SeatDTOs and the relevant cabin class code.
		Map<String, List<SeatDTO>> cabinClassSeatMap = AncillaryCommonUtil.getCabinClassSeatMap(seats);
		for (Entry<String, List<SeatDTO>> cabinClassSeatMapEntry : cabinClassSeatMap.entrySet()) {

			// The cabin class code.
			String cabinClassCode = cabinClassSeatMapEntry.getKey();

			// The list of all the Seats in the cabin.
			List<SeatDTO> cabinSeats = cabinClassSeatMapEntry.getValue();

			// Get the list of all the air row groups in cabin class.
			List<SeatMapRowGroup> airRowGroups = getSeatMapRowGroupSeats(cabinSeats, flightSegment);

			SeatMapCabinClass cabinClass = new SeatMapCabinClass();
			cabinClass.setCabinType(cabinClassCode);
			cabinClass.getAirRowGroups().addAll(airRowGroups);

			cabinClassList.add(cabinClass);
		}
		return cabinClassList;
	}

	private static List<SeatMapRowGroup> getSeatMapRowGroupSeats(List<SeatDTO> cabinClassSeatList, FlightSegment flightSegment) {
		List<SeatMapRowGroup> airRowGroups = new ArrayList<SeatMapRowGroup>();

		// A map of SeatDTOs and the relevant cabin class code.
		Map<String, List<SeatDTO>> rowGroupSeatMap = AncillaryCommonUtil.getRowGroupSeatMap(cabinClassSeatList);
		for (Entry<String, List<SeatDTO>> rowGroupSeatMapEntry : rowGroupSeatMap.entrySet()) {

			// The list of all the Seats in the row group.
			List<SeatDTO> rowGroupSeats = rowGroupSeatMapEntry.getValue();

			List<SeatMapColumnGroup> SeatMapColumnGroups = getSeatMapColumnGroupSeats(rowGroupSeats, flightSegment);

			SeatMapRowGroup airRowGroup = new SeatMapRowGroup();
			airRowGroup.setRowGroupId(rowGroupSeatMapEntry.getKey());
			airRowGroup.getAirColumnGroups().addAll(SeatMapColumnGroups);

			airRowGroups.add(airRowGroup);
		}
		return airRowGroups;
	}

	private static List<SeatMapColumnGroup> getSeatMapColumnGroupSeats(List<SeatDTO> rowGroupSeats, FlightSegment flightSegment) {
		List<SeatMapColumnGroup> SeatMapColumnGroups = new ArrayList<SeatMapColumnGroup>();

		// A map of SeatDTOs and the relevant column group.
		Map<String, List<SeatDTO>> columnGroupSeatMap = AncillaryCommonUtil.getColumnGroupSeatMap(rowGroupSeats);
		for (Entry<String, List<SeatDTO>> columnGroupSeatMapEntry : columnGroupSeatMap.entrySet()) {

			int columnNumber = Integer.parseInt(columnGroupSeatMapEntry.getKey());
			boolean isOddColumn = (columnNumber % 2 != 0) ? true : false;

			// The list of all the Seats in the column group.
			List<SeatDTO> columnGroupSeats = columnGroupSeatMapEntry.getValue();

			List<SeatMapRow> airRows = getAirRowsSeats(columnGroupSeats, flightSegment, isOddColumn);

			SeatMapColumnGroup SeatMapColumnGroup = new SeatMapColumnGroup();
			SeatMapColumnGroup.setColumnGroupId(columnGroupSeatMapEntry.getKey());
			SeatMapColumnGroup.getAirRows().addAll(airRows);

			SeatMapColumnGroups.add(SeatMapColumnGroup);
		}
		return SeatMapColumnGroups;
	}

	private static List<SeatMapRow> getAirRowsSeats(List<SeatDTO> columnGroupSeats, FlightSegment flightSegment,
			boolean isOddColumn) {
		List<SeatMapRow> airRows = new ArrayList<SeatMapRow>();

		Map<String, List<SeatDTO>> rowSeatsMap = AncillaryCommonUtil.getRowSeatMap(columnGroupSeats);
		for (Entry<String, List<SeatDTO>> rowSeatsMapEntry : rowSeatsMap.entrySet()) {
			int rowNumber = Integer.parseInt(rowSeatsMapEntry.getKey());
			List<SeatDTO> rowSeats = rowSeatsMapEntry.getValue();

			SeatMapRow airRow = new SeatMapRow();
			airRow.setRowNumber(rowNumber);

			boolean isOddRow = (rowNumber % 2 != 0) ? true : false;

			boolean isInfantBooked = false;
			int vacantSeats = 0;
			int seatsOpenForReservation = 0;

			List<Seat> airSeats = new ArrayList<Seat>();
			for (SeatDTO seatDTO : rowSeats) {

				Seat airSeat = getPopulatedAirSeat(seatDTO);
				airSeats.add(airSeat);

				String paxType = seatDTO.getPaxType();
				if (paxType != null && paxType.equalsIgnoreCase(PaxTypeTO.PARENT)) {
					isInfantBooked = true;
				}

				if (seatDTO.getStatus().equals("VAC")) {
					vacantSeats = vacantSeats + 1;
				}

				if (seatDTO.getStatus().equals("VAC") || seatDTO.getStatus().equals("ACQ") || seatDTO.getStatus().equals("BLK")
						|| seatDTO.getStatus().equals("RES")) {
					seatsOpenForReservation = seatsOpenForReservation + 1;
				}
			}
			List<PassengerTypeQuantity> passengerTypeQuantities = new ArrayList<PassengerTypeQuantity>();
			if (vacantSeats > 0) {
				airRow.setSeatsVacant(true);

				PassengerTypeQuantity passengerTypeQuantity = new PassengerTypeQuantity();
				passengerTypeQuantity.setPassengerType(PassengerType.ADT);
				passengerTypeQuantity.setQuantity(vacantSeats);

				passengerTypeQuantities.add(passengerTypeQuantity);

				passengerTypeQuantity = new PassengerTypeQuantity();
				passengerTypeQuantity.setPassengerType(PassengerType.CHD);
				passengerTypeQuantity.setQuantity(vacantSeats);

				passengerTypeQuantities.add(passengerTypeQuantity);

				if (!isInfantBooked) {
					if ((isOddColumn && isOddRow) || !isOddColumn) {
						passengerTypeQuantity = new PassengerTypeQuantity();
						passengerTypeQuantity.setPassengerType(PassengerType.INF);
						passengerTypeQuantity.setQuantity(1);
					} else {
						passengerTypeQuantity = new PassengerTypeQuantity();
						passengerTypeQuantity.setPassengerType(PassengerType.INF);
						passengerTypeQuantity.setQuantity(0);
					}

					passengerTypeQuantities.add(passengerTypeQuantity);
				} else {
					passengerTypeQuantity = new PassengerTypeQuantity();
					passengerTypeQuantity.setPassengerType(PassengerType.INF);
					passengerTypeQuantity.setQuantity(0);

					passengerTypeQuantities.add(passengerTypeQuantity);
				}
			}

			airRow.getPassengerTypeQuantity().addAll(passengerTypeQuantities);
			airRow.getAirSeats().addAll(airSeats);

			airRows.add(airRow);
		}

		return airRows;
	}

	private static Seat getPopulatedAirSeat(SeatDTO seatDTO) {
		Seat airSeat = new Seat();
		airSeat.setSeatAvailability(seatDTO.getStatus());
		airSeat.setSeatCharge(seatDTO.getChargeAmount());
		airSeat.setSeatNumber(seatDTO.getSeatCode());
		airSeat.setBookedPassengerType(getPassengerType(seatDTO.getPaxType()));
		airSeat.setSeatType(getSeatType(seatDTO.getSeatType()));
		airSeat.setSeatLocationType(seatDTO.getSeatLocationType());
		airSeat.setSeatMessage(seatDTO.getSeatMessage());
		airSeat.setLogicalCabinClass(seatDTO.getLogicalCabinClassCode());

		if (seatDTO.getSeatType() != null && seatDTO.getSeatType().equalsIgnoreCase("EXIT")) {
			airSeat.getNotAllowedPassengerType().add(PassengerType.INF);
			airSeat.getNotAllowedPassengerType().add(PassengerType.CHD);
		}
		return airSeat;
	}

	private static SeatType getSeatType(String seatType) {
		if (seatType == null) {
			return null;
		}
		if (seatType.equalsIgnoreCase("EXIT")) {
			return SeatType.EXIT;
		} else {
			return null;
		}
	}

	private static PassengerType getPassengerType(String paxType) {
		if (paxType == null) {
			return null;
		}
		if (paxType.equalsIgnoreCase(PaxTypeTO.ADULT)) {
			return PassengerType.ADT;
		} else if (paxType.equalsIgnoreCase(PaxTypeTO.CHILD)) {
			return PassengerType.CHD;
		} else if (paxType.equalsIgnoreCase(PaxTypeTO.PARENT)) {
			return PassengerType.PRT;
		} else if (paxType.equalsIgnoreCase(PaxTypeTO.INFANT)) {
			return PassengerType.INF;
		} else {
			return null;
		}
	}

	public static IFlightSegment getFlightSegmentTO(Integer flightSegId) throws ModuleException {
		FlightSegement flightSegment = AAServicesModuleUtils.getFlightBD().getFlightSegment(flightSegId);

		FlightSegmentTO flightSegmentTO = new FlightSegmentTO();
		flightSegmentTO.setFlightSegId(flightSegment.getFltSegId());
		flightSegmentTO.setDepartureDateTime(flightSegment.getEstTimeDepatureLocal());
		flightSegmentTO.setDepartureDateTimeZulu(flightSegment.getEstTimeDepatureZulu());
		flightSegmentTO.setArrivalDateTime(flightSegment.getEstTimeArrivalLocal());
		flightSegmentTO.setArrivalDateTimeZulu(flightSegment.getEstTimeArrivalZulu());

		// FIXME
		flightSegmentTO.setReturnFlag(false);
		flightSegmentTO.setSegmentCode(flightSegment.getSegmentCode());

		return flightSegmentTO;

	}

	public static Map<String, Integer> getSegmentBundledFareServiceAvailability(
			TransactionalReservationProcess transactionalReservationProcess, String pnr, String serviceName)
			throws ModuleException {

		Map<String, BundledFareDTO> segmentBundledFares = getSegmentCodeWiseBundledFares(transactionalReservationProcess, pnr,
				null);
		Map<String, Integer> segmentBundledFareServiceAvailability = null;

		if (segmentBundledFares != null) {
			segmentBundledFareServiceAvailability = new HashMap<String, Integer>();
			for (Entry<String, BundledFareDTO> bundledFareEntry : segmentBundledFares.entrySet()) {
				String segmentCode = bundledFareEntry.getKey();
				BundledFareDTO bundledFareDTO = bundledFareEntry.getValue();

				if (bundledFareDTO != null
						&& (!segmentBundledFareServiceAvailability.containsKey(segmentCode)
								|| segmentBundledFareServiceAvailability.get(segmentCode) == null)
						&& bundledFareDTO.isServiceIncluded(serviceName)) {
					segmentBundledFareServiceAvailability.put(segmentCode, bundledFareDTO.getBundledFarePeriodId());
				}
			}
		}

		return segmentBundledFareServiceAvailability;
	}

	public static Map<String, Boolean> getOndBundledFareServiceMultipleSelection(Map<String, BundledFareDTO> segmentBundledFares,
			String serviceName) {
		Map<String, Boolean> segmentBundledFareServiceTemplates = null;

		if (segmentBundledFares != null) {
			segmentBundledFareServiceTemplates = new HashMap<String, Boolean>();
			for (Entry<String, BundledFareDTO> bundledFareEntry : segmentBundledFares.entrySet()) {
				String segmentCode = bundledFareEntry.getKey();
				BundledFareDTO bundledFareDTO = bundledFareEntry.getValue();

				if (!segmentBundledFareServiceTemplates.containsKey(segmentCode) && bundledFareDTO != null) {
					BundledFareFreeServiceTO freeServiceTo = bundledFareDTO.getApplicableService(serviceName);
					if (freeServiceTo != null) {
						segmentBundledFareServiceTemplates.put(segmentCode, freeServiceTo.isAllowMultipleSelect());
					}
				}
			}
		}

		return segmentBundledFareServiceTemplates;
	}

	public static Map<String, Integer> getOndBundledFareServiceTemplateIds(Map<String, BundledFareDTO> segmentBundledFares,
			String serviceName) {
		Map<String, Integer> segmentBundledFareServiceTemplates = null;

		if (segmentBundledFares != null) {
			segmentBundledFareServiceTemplates = new HashMap<String, Integer>();
			for (Entry<String, BundledFareDTO> bundledFareEntry : segmentBundledFares.entrySet()) {
				String segmentCode = bundledFareEntry.getKey();
				BundledFareDTO bundledFareDTO = bundledFareEntry.getValue();

				if (!segmentBundledFareServiceTemplates.containsKey(segmentCode) && bundledFareDTO != null) {
					segmentBundledFareServiceTemplates.put(segmentCode,
							bundledFareDTO.getApplicableServiceTemplateId(serviceName));
				}
			}
		}

		return segmentBundledFareServiceTemplates;
	}

	public static Map<String, Integer> getOndBundledFareServiceTemplateIds(
			TransactionalReservationProcess transactionalReservationProcess, String pnr,
			Map<String, Integer> ondWiseBundlePeriodIDMap, String serviceName) throws ModuleException {

		Map<String, BundledFareDTO> segmentBundledFares = getSegmentCodeWiseBundledFares(transactionalReservationProcess, pnr,
				ondWiseBundlePeriodIDMap);
		Map<String, Integer> segmentBundledFareServiceTemplates = null;

		if (segmentBundledFares != null) {
			segmentBundledFareServiceTemplates = new HashMap<String, Integer>();
			for (Entry<String, BundledFareDTO> bundledFareEntry : segmentBundledFares.entrySet()) {
				String segmentCode = bundledFareEntry.getKey();
				BundledFareDTO bundledFareDTO = bundledFareEntry.getValue();

				if (!segmentBundledFareServiceTemplates.containsKey(segmentCode) && bundledFareDTO != null) {
					segmentBundledFareServiceTemplates.put(segmentCode,
							bundledFareDTO.getApplicableServiceTemplateId(serviceName));
				}
			}
		}

		return segmentBundledFareServiceTemplates;
	}

	public static boolean isOverrideServiceCharge(Map<String, Integer> segmentBundledFareServiceInclusion, String segmentCode,
			List<BundledFareDTO> bundledFareDTOs, Long ssrId) {
		if (segmentBundledFareServiceInclusion != null && segmentBundledFareServiceInclusion.containsKey(segmentCode)
				&& segmentBundledFareServiceInclusion.get(segmentCode) != null) {
			Integer selectedBundledServicePeriodId = segmentBundledFareServiceInclusion.get(segmentCode);
			for (BundledFareDTO bundledFareDTO : bundledFareDTOs) {
				if (bundledFareDTO.getBundledFarePeriodId().equals(selectedBundledServicePeriodId)) {
					Set<Integer> freeApsIds = bundledFareDTO.getApplicableApsList();
					if (freeApsIds != null && freeApsIds.contains(ssrId.intValue())) {
						return true;
					}
				}
			}
		}

		return false;
	}

	public static Map<String, BundledFareDTO> getSegmentCodeWiseBundledFares(
			TransactionalReservationProcess transactionalReservationProcess, String pnr,
			Map<String, Integer> ondWiseBundlePeriodIDMap) throws ModuleException {
		Map<String, BundledFareDTO> segmentCodeWiseBundledFares = null;

		if (transactionalReservationProcess != null) {
			segmentCodeWiseBundledFares = transactionalReservationProcess.getSegmentBundledFareMap();
		} else if (ondWiseBundlePeriodIDMap != null && !ondWiseBundlePeriodIDMap.isEmpty()) {

			List<BundledFareDTO> bundledFareDTOs = AAServicesModuleUtils.getBundledFareBD()
					.getBundledFareDTOsByBundlePeriodIds(new ArrayList<Integer>(ondWiseBundlePeriodIDMap.values()));
			Map<Integer, BundledFareDTO> bundledFareMap = new HashMap<Integer, BundledFareDTO>();
			for (BundledFareDTO bundledFareDTO : bundledFareDTOs) {
				bundledFareMap.put(bundledFareDTO.getBundledFarePeriodId(), bundledFareDTO);
			}

			segmentCodeWiseBundledFares = new HashMap<String, BundledFareDTO>();
			for (Entry<String, Integer> segmentEntry : ondWiseBundlePeriodIDMap.entrySet()) {
				String segmentCode = segmentEntry.getKey();
				Integer bundledFarePeriodId = segmentEntry.getValue();

				segmentCodeWiseBundledFares.put(segmentCode, bundledFareMap.get(bundledFarePeriodId));
			}
		} else if (pnr != null && !"".equals(pnr)) {
			Map<String, Integer> segmentBundledFarePeriodIds = AAServicesModuleUtils.getReservationQueryBD()
					.getSegmentBundledFarePeriodIds(pnr);
			Collection<Integer> colBundledFarePeriodIds = segmentBundledFarePeriodIds.values();
			if (colBundledFarePeriodIds != null && !colBundledFarePeriodIds.isEmpty()) {
				Set<Integer> selectedBundledFarePeriodIds = new HashSet<Integer>();
				for (Integer bundledFarePeriodId : colBundledFarePeriodIds) {
					if (bundledFarePeriodId != null) {
						selectedBundledFarePeriodIds.add(bundledFarePeriodId);
					}
				}

				if (!selectedBundledFarePeriodIds.isEmpty()) {
					List<BundledFareDTO> bundledFareDTOs = AAServicesModuleUtils.getBundledFareBD()
							.getBundledFareDTOsByBundlePeriodIds(selectedBundledFarePeriodIds);

					Map<Integer, BundledFareDTO> bundledFareMap = new HashMap<Integer, BundledFareDTO>();
					for (BundledFareDTO bundledFareDTO : bundledFareDTOs) {
						bundledFareMap.put(bundledFareDTO.getBundledFarePeriodId(), bundledFareDTO);
					}

					segmentCodeWiseBundledFares = new HashMap<String, BundledFareDTO>();

					for (Entry<String, Integer> segmentEntry : segmentBundledFarePeriodIds.entrySet()) {
						String segmentCode = segmentEntry.getKey();
						Integer bundledFarePeriodId = segmentEntry.getValue();

						segmentCodeWiseBundledFares.put(segmentCode, bundledFareMap.get(bundledFarePeriodId));
					}
				}

			}

		}

		return segmentCodeWiseBundledFares;
	}

	public static boolean isAtleastOneBundledFareTemplateExists(TransactionalReservationProcess transactionalReservationProcess,
			String pnr, Map<String, Integer> ondWiseBundlePeriodIDMap) throws ModuleException {

		if (transactionalReservationProcess != null) {
			return transactionalReservationProcess.isAtleastOneBundledFareSelected();
		} else if (ondWiseBundlePeriodIDMap != null && !ondWiseBundlePeriodIDMap.isEmpty()) {
			return true;
		} else if (pnr != null && !"".equals(pnr)) {
			Map<String, Integer> segmentBundledFarePeriodIds = AAServicesModuleUtils.getReservationQueryBD()
					.getSegmentBundledFarePeriodIds(pnr);
			if (segmentBundledFarePeriodIds != null && !segmentBundledFarePeriodIds.isEmpty()) {
				for (Integer bundledFarePeriodId : segmentBundledFarePeriodIds.values()) {
					if (bundledFarePeriodId != null && bundledFarePeriodId > 0) {
						return true;
					}
				}
			}
		}

		return false;
	}
}
