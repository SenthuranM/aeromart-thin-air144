package com.isa.thinair.aaservices.core.bl.flightservice;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightLoadReportDataInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightLoadReportSearchCriteria;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAFlightLoadReportDataRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAFlightLoadReportDataRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.airproxy.api.utils.ReportingUtil;
import com.isa.thinair.airschedules.api.dto.FlightLoadReportDataDTO;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;

public class AAFlightLoadReportServiceBL {

	private static final Log log = LogFactory.getLog(AAFlightLoadReportServiceBL.class);

	private AAFlightLoadReportDataRQ aaFlightLoadReportDataRQ;

	private AAFlightLoadReportDataRS aaFlightLoadReportDataRS;

	public void setAAFlightLoadReportDataRQ(AAFlightLoadReportDataRQ aaFlightLoadReportDataRQ) {
		this.aaFlightLoadReportDataRQ = aaFlightLoadReportDataRQ;
	}

	private void intializeAAFlightLoadReportDataRS() {
		aaFlightLoadReportDataRS = new AAFlightLoadReportDataRS();
		aaFlightLoadReportDataRS.setResponseAttributes(new AAResponseAttributes());
	}

	public AAFlightLoadReportDataRS execute() {
		intializeAAFlightLoadReportDataRS();

		try {
			ReportsSearchCriteria criteria = composeSearchCriteria(aaFlightLoadReportDataRQ.getCriteria());

			List<FlightLoadReportDataDTO> reportDTOList = ReportingUtil.composeFlightLoadReportDataDTOs(AAServicesModuleUtils
					.getDataExtractionBD().getFlightLoad(criteria));

			List<FlightLoadReportDataInfo> aaReportInfoList = composeAAReportInfoList(reportDTOList);

			aaFlightLoadReportDataRS.getResultList().addAll(aaReportInfoList);

		} catch (Exception ex) {
			log.error("Error occurred during getting flight load report data in given search critirea", ex);
			AAExceptionUtil.addAAErrrors(aaFlightLoadReportDataRS.getResponseAttributes().getErrors(), ex);
		} finally {
			int numOfErrors = aaFlightLoadReportDataRS.getResponseAttributes().getErrors().size();
			AASuccess successType = (numOfErrors == 0 ? new AASuccess() : null);
			aaFlightLoadReportDataRS.getResponseAttributes().setSuccess(successType);
		}

		return aaFlightLoadReportDataRS;
	}

	private List<FlightLoadReportDataInfo> composeAAReportInfoList(List<FlightLoadReportDataDTO> reportDTOList) {
		List<FlightLoadReportDataInfo> infoList = new ArrayList<FlightLoadReportDataInfo>();

		for (FlightLoadReportDataDTO dto : reportDTOList) {
			FlightLoadReportDataInfo info = new FlightLoadReportDataInfo();

			info.setConnChild(dto.getConnChild());
			info.setConnectingFlightNumber(dto.getConnectingFlightNumber());
			info.setConnectingSegment(dto.getConnectingSegment());
			info.setConnFemale(dto.getConnFemale());
			info.setConnInfant(dto.getConnInfant());
			info.setConnMale(dto.getConnMale());
			info.setConnOther(dto.getConnOther());
			info.setConnTotal(dto.getConnTotal());
			info.setOriginChild(dto.getOriginChild());
			info.setOriginDate(dto.getOriginDate());
			info.setOriginFemale(dto.getOriginFemale());
			info.setOriginFlightnumber(dto.getOriginFlightnumber());
			info.setOriginInfant(dto.getOriginInfant());
			info.setOriginMale(info.getOriginMale());
			info.setOriginOther(dto.getOriginOther());
			info.setOriginSegment(info.getOriginSegment());
			info.setOriginTotal(dto.getOriginTotal());

			infoList.add(info);
		}

		return infoList;
	}

	private ReportsSearchCriteria composeSearchCriteria(FlightLoadReportSearchCriteria info) {
		ReportsSearchCriteria criteria = new ReportsSearchCriteria();

		criteria.setConTime(info.getConTime());
		criteria.setDateRangeFrom(info.getDateRangeFrom());
		criteria.setDateRangeTo(info.getDateRangeTo());
		criteria.setFlightNumber(info.getFlightNumber());
		criteria.setFrom(info.getFrom());
		criteria.setOperationType(info.getOperationType());
		criteria.setStatus(info.getStatus());
		criteria.setTo(info.getTo());

		return criteria;
	}
}
