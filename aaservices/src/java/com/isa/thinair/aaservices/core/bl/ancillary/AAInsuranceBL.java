package com.isa.thinair.aaservices.core.bl.ancillary;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AirTraveler;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingFlightSegment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingInsuranceRequest;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingSystems;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.InsuranceExternalContentMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.InsuranceQuotation;
import com.isa.connectivity.profiles.maxico.v1.common.dto.InsuredAirTraveler;
import com.isa.connectivity.profiles.maxico.v1.common.dto.InsuredContactInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.InsuredJourneyDetails;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.AncillaryUtil;
import com.isa.thinair.airproxy.api.model.reservation.ancillary.LCCInsuredContactInfoDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.utils.AnciOfferUtil;
import com.isa.thinair.airreservation.api.model.ReservationInsurance;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.INSURANCE_STATES;
import com.isa.thinair.commons.api.constants.CommonsConstants.INSURANCEPROVIDER;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.promotion.api.model.AnciOfferType;
import com.isa.thinair.promotion.api.to.anciOffer.AnciOfferCriteriaTO;
import com.isa.thinair.webplatform.api.v2.ancillary.AncillaryDTOUtil;
import com.isa.thinair.wsclient.api.dto.aig.IInsuranceRequest;
import com.isa.thinair.wsclient.api.dto.aig.InsurableFlightSegment;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceRequestAssembler;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceResponse;
import com.isa.thinair.wsclient.api.dto.aig.InsurePassenger;
import com.isa.thinair.wsclient.api.dto.aig.InsureSegment;

class AAInsuranceBL {

	private static final Log log = LogFactory.getLog(AAInsuranceBL.class);

	private List<BookingFlightSegment> bookingFlightSegments;

	private BookingInsuranceRequest insuranceRequest;

	public List<InsuranceQuotation> getInsuranceQuotes(String marketingAirLineCode, BookingSystems appEngine)
			throws ModuleException {

		List<InsuranceQuotation> insuranceQuotes = new ArrayList<InsuranceQuotation>();

		try {
			if (AppSysParamsUtil.isShowTravelInsurance()) {
				log.debug(" Start - Insurace quote reqest for carrier  [" + marketingAirLineCode + "]");

				IInsuranceRequest insuranceRequest = populateInsuranceRequest();

				List<InsuranceResponse> insuranceResponses = AAServicesModuleUtils.getInsuranceClientBD().quoteInsurancePolicy(
						insuranceRequest);

				insuranceQuotes = transformAndPersist(insuranceResponses, marketingAirLineCode, appEngine);
				log.debug(" End - Insurace quote reqest for carrier  [" + marketingAirLineCode + "]");
			}

		} catch (ModuleException e) {
			log.error("Module exception caught.", e);
		} catch (Exception e) {
			log.error("Generic exception caught.", e);
		}

		return insuranceQuotes;
	}

	// HELPER FUNCTIONS
	private IInsuranceRequest populateInsuranceRequest() {

		IInsuranceRequest iRequest = new InsuranceRequestAssembler();

		iRequest.addFlightSegment(getPopulatedInsureSegment());
		iRequest.setLccInsuranceContactInfo(populateLCCInsuredContactInfoDTO());
		iRequest.setFlightSegments(createFlightSegmentList());

		for (InsuredAirTraveler insuredAirTraveler : insuranceRequest.getAirTravelers()) {
			InsurePassenger insurePassenger = getPopulatedPassenger(insuredAirTraveler);
			iRequest.addPassenger(insurePassenger);
		}

		return iRequest;
	}

	private InsureSegment getPopulatedInsureSegment() {

		InsureSegment insureSegment = new InsureSegment();

		InsuredJourneyDetails insuredJourneyDetails = insuranceRequest.getInsuredJourneyDetails();
		insureSegment.setFromAirportCode(insuredJourneyDetails.getJourneyStartAirportCode());
		insureSegment.setToAirportCode(insuredJourneyDetails.getJourneyEndAirportCode());
		insureSegment.setDepartureDate(insuredJourneyDetails.getJourneyStartDate());
		insureSegment.setDepartureDateOffset(insuredJourneyDetails.getJourneyStartDateOffset());
		insureSegment.setArrivalDate(insuredJourneyDetails.getJourneyEndDate());
		insureSegment.setArrivalDateOffset(insuredJourneyDetails.getJourneyEndDateOffset());
		insureSegment.setRoundTrip(insuredJourneyDetails.isRoundTrip());
		insureSegment.setSalesChanelCode(insuranceRequest.getSalesChannel());
		addExtraFlightSegmentDetails(insureSegment);

		return insureSegment;
	}

	private InsurePassenger getPopulatedPassenger(AirTraveler insuredAirTraveler) {

		InsurePassenger insurePassenger = new InsurePassenger();
		for (String addressLine : insuredAirTraveler.getAddress().getAddressLine()) {

			if (insurePassenger.getAddressLn1() == null) {
				insurePassenger.setAddressLn1(addressLine);
			}

			if (insurePassenger.getAddressLn2() == null) {
				insurePassenger.setAddressLn2(addressLine);
			}
		}

		insurePassenger.setCity(insuredAirTraveler.getAddress().getCityName());
		insurePassenger.setEmail(insuredAirTraveler.getEmail());
		insurePassenger.setCountryOfAddress(insuredAirTraveler.getAddress().getCountry().getCountryName());
		insurePassenger.setHomePhoneNumber(insuredAirTraveler.getTelephone().getPhoneNumber());
		insurePassenger.setFirstName(insuredAirTraveler.getPersonName().getFirstName());
		insurePassenger.setLastName(insuredAirTraveler.getPersonName().getSurName());
		insurePassenger.setDateOfBirth(CalendarUtil.asDate(insuredAirTraveler.getBirthDate()));

		return insurePassenger;
	}

	private List<InsurableFlightSegment> createFlightSegmentList() {
		List<InsurableFlightSegment> flightSegments = new ArrayList<InsurableFlightSegment>();

		for (FlightSegment flightSegmentTO : insuranceRequest.getFlightSegments()) {
			InsurableFlightSegment flightSegment = new InsurableFlightSegment();
			flightSegment.setDepartureStationCode(flightSegmentTO.getSegmentCode().substring(0, 3));
			flightSegment.setDepartureDateTimeLocal(flightSegmentTO.getDepatureDateTime());
			flightSegment.setDepartureDateTimeZulu(flightSegmentTO.getDepatureDateTimeZulu());
			flightSegment.setArrivalStationCode(flightSegmentTO.getSegmentCode().substring(
					flightSegmentTO.getSegmentCode().length() - 3, flightSegmentTO.getSegmentCode().length()));
			flightSegment.setFlightNo(flightSegmentTO.getFlightNumber());
			flightSegments.add(flightSegment);
		}

		return flightSegments;

	}

	@Deprecated
	private InsureSegment addExtraFlightSegmentDetails(InsureSegment insSegment) {
		if (AppSysParamsUtil.getInsuranceProvidersList().contains(INSURANCEPROVIDER.TUNE)) {
			for (BookingFlightSegment flightsegment : bookingFlightSegments) {
				if (!"TRUE".equalsIgnoreCase(flightsegment.getReturnFlag())) {
					insSegment.setDepartureFlightNo(flightsegment.getFlightNumber());

				} else {
					insSegment.setArrivalFlightNo(flightsegment.getFlightNumber());
				}

			}
		}
		return insSegment;
	}

	private LCCInsuredContactInfoDTO populateLCCInsuredContactInfoDTO() {
		LCCInsuredContactInfoDTO contactInfo = new LCCInsuredContactInfoDTO();

		InsuredContactInfo insuredContactInfo = insuranceRequest.getInsuredContactInfoDTO();
		contactInfo.setContactPerson(insuredContactInfo.getContactPerson());
		contactInfo.setAddress1(insuredContactInfo.getAddress1());
		contactInfo.setAddress2(insuredContactInfo.getAddress2());
		contactInfo.setAddress3(insuredContactInfo.getAddress3());
		contactInfo.setMobilePhoneNum(insuredContactInfo.getMobilePhoneNum());
		contactInfo.setHomePhoneNum(insuredContactInfo.getHomePhoneNum());
		contactInfo.setCity(insuredContactInfo.getCity());
		contactInfo.setCountry(insuredContactInfo.getCountry());
		contactInfo.setEmailAddress(insuredContactInfo.getEmailAddress());
		contactInfo.setPrefLanguage(insuredContactInfo.getPrefLanguage());
		return contactInfo;
	}

	private List<InsuranceQuotation> transformAndPersist(List<InsuranceResponse> insuranceResponses, String marketingAirLineCode,
			BookingSystems appEngine) throws ModuleException {

		List<InsuranceQuotation> insuranceQuotes = new ArrayList<InsuranceQuotation>();

		InsuranceQuotation insuranceQuotation = null;

		for (InsuranceResponse insuranceResponse : insuranceResponses) {
			if (log.isDebugEnabled()) {
				log.debug("Insurance qoute retrieved, ErrorCode : " + insuranceResponse.getErrorCode());
				log.debug("Insurance qoute retrieved, ErrorMessage : " + insuranceResponse.getErrorMessage());
				log.debug("Insurance qoute retrieved, MessageId : " + insuranceResponse.getMessageId());
				log.debug("Insurance qoute retrieved, CurrencyCode : " + insuranceResponse.getQuotedCurrencyCode());
				log.debug("Insurance qoute retrieved, TotalPremiumAmount : " + insuranceResponse.getQuotedTotalPremiumAmount());
			}

			if (insuranceResponse.getErrorCode().equals("0") && insuranceResponse.getErrorMessage().equals("OK")) {

				insuranceQuotation = new InsuranceQuotation();

				insuranceQuotation.setPolicyCode(insuranceResponse.getPolicyCode());
				insuranceQuotation.setQuotedCurrencyCode(insuranceResponse.getQuotedCurrencyCode());
				insuranceQuotation.setQuotedTotalPremiumAmount(insuranceResponse.getQuotedTotalPremiumAmount());
				insuranceQuotation.setTotalPremiumAmount(insuranceResponse.getTotalPremiumAmount());
				insuranceQuotation.setOperatingAirline(AppSysParamsUtil.getDefaultCarrierCode());
				insuranceQuotation.setSuccess(true);

				insuranceQuotation.setSsrFeeCode(insuranceResponse.getSsrFeeCode());
				insuranceQuotation.setPlanCode(insuranceResponse.getPlanCode());

				insuranceQuotation.setRank(insuranceResponse.getInsProductConfig().getRank());
				insuranceQuotation.setGroupID(insuranceResponse.getInsProductConfig().getGroupID());
				insuranceQuotation.setSubGroupID(insuranceResponse.getInsProductConfig().getSubGroupID());
				insuranceQuotation.setExternalContent(insuranceResponse.getInsProductConfig().isExternalContent());
				insuranceQuotation.setPopup(insuranceResponse.getInsProductConfig().isPopup());
				insuranceQuotation.setDisplayName(insuranceResponse.getInsProductConfig().getDisplayName());
				insuranceQuotation.setIsEuropianCountry(insuranceResponse.isEuropianCountry());
				insuranceQuotation.getInsuranceExternalContentMap().addAll(getInsuranceExternalContentForDisplay(insuranceResponse.getInsuranceExternalContent()));
				
				
				Integer insruanceId = persistReservationInsurance(insuranceResponse, marketingAirLineCode);

				insuranceQuotation.setInsuranceRefNumber(insruanceId);

				AnciOfferCriteriaTO applicableAnciOffer = null;

				if (SalesChannelsUtil.isBookingSystemWebOrMobile(appEngine)) {
					applicableAnciOffer = getApplicableAnciOffer();
				}

				if (applicableAnciOffer != null) {
					String selectedLanguage = insuranceRequest.getSelectedLanguage();
					insuranceQuotation.setAnciOffer(true);
					insuranceQuotation.setAnciOfferName(applicableAnciOffer.getNameTranslations().get(selectedLanguage));
					insuranceQuotation.setAnciOfferDescription(applicableAnciOffer.getDescriptionTranslations().get(
							selectedLanguage));
				}

				insuranceQuotes.add(insuranceQuotation);

			}

		}

		return insuranceQuotes;
	}

	private Integer persistReservationInsurance(InsuranceResponse insuranceResponse, String marketingAirLineCode) {
		Integer insuranceId = null;

		ReservationInsurance reservationInsurance = new ReservationInsurance();

		reservationInsurance.setQuoteTime(new Date());
		reservationInsurance.setState(INSURANCE_STATES.QO.toString());

		reservationInsurance.setPolicyCode(insuranceResponse.getPolicyCode());
		reservationInsurance.setMessageId(insuranceResponse.getMessageId());

		reservationInsurance.setQuotedCurrencyCode(insuranceResponse.getQuotedCurrencyCode());

		reservationInsurance.setAmount(insuranceResponse.getTotalPremiumAmount());
		reservationInsurance.setQuotedAmount(insuranceResponse.getQuotedTotalPremiumAmount());

		reservationInsurance.setTaxAmount(insuranceResponse.getTaxAmount());
		reservationInsurance.setQuotedTaxAmout(insuranceResponse.getQuotedTaxAmout());

		reservationInsurance.setNetAmount(insuranceResponse.getNetAmount());
		reservationInsurance.setQuotedNetAmount(insuranceResponse.getQuotedNetAmount());

		reservationInsurance.setSsrFeeCode(insuranceResponse.getSsrFeeCode());
		reservationInsurance.setPlanCode(insuranceResponse.getPlanCode());

		reservationInsurance.setInsuranceProductConfigID(insuranceResponse.getInsProductConfig().getInsuranceProductConfigID());

		InsuredJourneyDetails insuredJourneyDetails = insuranceRequest.getInsuredJourneyDetails();

		reservationInsurance.setRoundTrip(insuredJourneyDetails.isRoundTrip() ? "Y" : "N");
		reservationInsurance.setTotalPaxCount(insuranceRequest.getAirTravelers().size());
		reservationInsurance.setDateOfTravel(insuredJourneyDetails.getJourneyStartDate());
		reservationInsurance.setDateOfReturn(insuredJourneyDetails.getJourneyEndDate());
		reservationInsurance.setOrigin(insuredJourneyDetails.getJourneyStartAirportCode());
		reservationInsurance.setDestination(insuredJourneyDetails.getJourneyEndAirportCode());
		reservationInsurance.setMarketingCarrier(marketingAirLineCode);
		reservationInsurance.setInsuranceType(1);
		reservationInsurance.setSalesChannel(insuranceRequest.getSalesChannel());

		reservationInsurance.setStartDateOffset(insuredJourneyDetails.getJourneyStartDateOffset());
		reservationInsurance.setEndDateOffset(insuredJourneyDetails.getJourneyEndDateOffset());

		insuranceId = AAServicesModuleUtils.getReservationBD().saveReservationInsurance(reservationInsurance);

		if (log.isInfoEnabled()) {
			log.debug("Insurance quote saved successfully, insurance id saved : " + insuranceId);
		}

		return insuranceId;

	}

	InsuranceQuotation getSavedInsuranceQuote(String insuranceID) {
		InsuranceQuotation insuranceQuotation = new InsuranceQuotation();
		try {

			ReservationInsurance insuranceResponse = AAServicesModuleUtils.getReservationBD().getReservationInsurance(
					Integer.parseInt(insuranceID));
			insuranceQuotation.setPolicyCode(insuranceResponse.getPolicyCode());
			insuranceQuotation.setQuotedCurrencyCode(insuranceResponse.getQuotedCurrencyCode());
			insuranceQuotation.setQuotedTotalPremiumAmount(insuranceResponse.getQuotedAmount());
			insuranceQuotation.setTotalPremiumAmount(insuranceResponse.getAmount());
			insuranceQuotation.setOperatingAirline(AppSysParamsUtil.getDefaultCarrierCode());
			insuranceQuotation.setSuccess(true);
			insuranceQuotation.setInsuranceRefNumber(insuranceResponse.getInsuranceId());

		} catch (Exception e) {
			log.error("Generic exception caught.", e);
		}
		return insuranceQuotation;
	}

	private AnciOfferCriteriaTO getApplicableAnciOffer() throws ModuleException {
		List<FlightSegmentTO> flightSegmentTOs = AncillaryUtil.getFlightSegmentTOs(bookingFlightSegments);
		Object[] inboundOutboundList = AncillaryDTOUtil.getOutboundInboundList(flightSegmentTOs);

		List<FlightSegmentTO> outboundFlightSegmentTOs = (List<FlightSegmentTO>) inboundOutboundList[0];
		List<FlightSegmentTO> inboundFlightSegmentTOs = (List<FlightSegmentTO>) inboundOutboundList[1];

		FlightSegmentTO firstDepartureSeg = outboundFlightSegmentTOs.iterator().hasNext() ? outboundFlightSegmentTOs.iterator()
				.next() : null;
		FlightSegmentTO firstArrivalSeg = inboundFlightSegmentTOs.iterator().hasNext() ? inboundFlightSegmentTOs.iterator()
				.next() : null;

		Collection<AnciOfferCriteriaTO> availableOutboundOffers = AAServicesModuleUtils.getAnciOfferBD()
				.getAllAvailableAnciOffers(AnciOfferUtil.createAnciOfferSearch(firstDepartureSeg, AnciOfferType.INSURANCE));

		if (firstArrivalSeg != null) {
			Collection<AnciOfferCriteriaTO> availableInboundOffers = AAServicesModuleUtils.getAnciOfferBD()
					.getAllAvailableAnciOffers(AnciOfferUtil.createAnciOfferSearch(firstArrivalSeg, AnciOfferType.INSURANCE));
			if (availableInboundOffers.isEmpty()) {
				return null;
			} else {
				availableInboundOffers.retainAll(availableOutboundOffers);
				return availableInboundOffers.isEmpty() ? null : availableInboundOffers.iterator().next();
			}
		} else {
			return availableOutboundOffers.isEmpty() ? null : availableOutboundOffers.iterator().next();
		}
	}

	public void setBookingFlightSegments(List<BookingFlightSegment> bookingFlightSegments) {
		this.bookingFlightSegments = bookingFlightSegments;
	}

	public void setInsuranceRequest(BookingInsuranceRequest insuranceRequest) {
		this.insuranceRequest = insuranceRequest;
	}
	
	private List<InsuranceExternalContentMap> getInsuranceExternalContentForDisplay(Map<String, String> insuranceExternalContent){
		List<InsuranceExternalContentMap> insuranceExternalContentMap = new ArrayList<InsuranceExternalContentMap>();
		for (Map.Entry<String, String> entry : insuranceExternalContent.entrySet()) {
			InsuranceExternalContentMap externalContentForDisplay =  new InsuranceExternalContentMap();
			externalContentForDisplay.setKey(entry.getKey());
			externalContentForDisplay.setValue(entry.getValue());
			insuranceExternalContentMap.add(externalContentForDisplay);
		}		
		return insuranceExternalContentMap;		
	}

}
