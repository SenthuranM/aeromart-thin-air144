package com.isa.thinair.aaservices.core.bl.reservation;

import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAirBookModifyRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAirBookRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAErrorCode;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAModificationTypeCode;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.aaservices.core.session.AASessionManager;
import com.isa.thinair.aaservices.core.session.AAUserSession;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAReservationUtil;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airproxy.api.utils.PrivilegesKeys.AlterResPrivilegesKeys;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.util.ReservationUtil;

/**
 * This class checkes the privileges in the each operation carrier side for special set of privileges which are not
 * restrictive at the moment At the moment following set of privileges are not most restrictive
 * 
 * CANCEL_SEGMENT ALLOW_MODIFY_DATE ALLOW_MODIFY_ROUTE ADD_SEGMENT GROUND_SERVICE_ENABLED
 * 
 * For thoes operations we need to check that all participating carrier are satisfing the privileges before doing the
 * actual opreration in each carrier
 * 
 * @author malaka
 * 
 */
public class AAAlterResevationAuthorizationCheckBL {

	private AAAirBookModifyRQ aaAirBookModifyRQ;

	private static Log log = LogFactory.getLog(AAAlterResevationAuthorizationCheckBL.class);

	public AAAlterResevationAuthorizationCheckBL(AAAirBookModifyRQ aaAirBookModifyRQ) {
		this.aaAirBookModifyRQ = aaAirBookModifyRQ;
	}

	public AAAirBookRS execute() {

		AAAirBookRS aaAirBookRS = new AAAirBookRS();
		aaAirBookRS.setResponseAttributes(new AAResponseAttributes());

		Collection<String> privilegeKeys = null;

		AAModificationTypeCode aaModificationTypeCode;

		try {

			aaModificationTypeCode = aaAirBookModifyRQ.getModificationTypeCode();

			privilegeKeys = (Collection<String>) AASessionManager.getInstance().getCurrUserSessionParam(
					AAUserSession.USER_PRIVILEGES_KEYS);

			if (aaModificationTypeCode == null) {
				throw new WebservicesException(AAErrorCode.ERR_11_MAXICO_REQUIRED_INVALID_MODIFICATION_TYPE,
						"Modification type is invalid");
			}

			if (aaModificationTypeCode == AAModificationTypeCode.MODTYPE_AUTH_1_CHECK_CANCEL_OND) {
				AAReservationUtil.authorize(privilegeKeys, AlterResPrivilegesKeys.ALT_RES_CANCEL_SEGMENT);
				
			} else if (aaModificationTypeCode == AAModificationTypeCode.MODTYPE_AUTH_2_CHECK_MODIFY_OND) {
				AAReservationUtil.authorizeAny(privilegeKeys, AlterResPrivilegesKeys.ALT_RES_MODIFY_SEGMENT_DATE,AlterResPrivilegesKeys.ALT_RES_MODIFY_SEGMENT_ROUTE);

			} else if (aaModificationTypeCode == AAModificationTypeCode.MODTYPE_AUTH_3_CHECK_ADD_OND) {
				AAReservationUtil.authorize(privilegeKeys, AlterResPrivilegesKeys.ALT_RES_ADD_SEGMENT);

			} else if (aaModificationTypeCode == AAModificationTypeCode.MODTYPE_AUTH_5_CHECK_MODIFY_EXPIRED_RESERVATIONS) {
				Reservation reservation = null;
				
				String pnr = aaAirBookModifyRQ.getAaReservation().getPnr();
				
				String versionString = aaAirBookModifyRQ.getAaReservation().getAirReservation().getVersion();
				
				LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
				pnrModesDTO.setPnr(pnr);
				pnrModesDTO.setLoadFares(true);
				pnrModesDTO.setLoadOndChargesView(true);
				pnrModesDTO.setLoadPaxAvaBalance(true);
				pnrModesDTO.setLoadSegViewBookingTypes(true);
				pnrModesDTO.setLoadSegView(true);
				pnrModesDTO.setLoadLastUserNote(true);
				pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
				pnrModesDTO.setRecordAudit(true);
				pnrModesDTO.setLoadPaxPaymentOndBreakdownView(true);
				
				versionString = aaAirBookModifyRQ.getAaReservation().getAirReservation().getVersion();
				
				long version = -1l;
				if (versionString != null) {
					version = Long.parseLong(versionString);
				}
				reservation = AAModifyReservationUtil.getReservation(pnrModesDTO, version, null);
				
				if (ReservationUtil.isBulkTicketReservation(reservation)) {
					AAReservationUtil.authorize(privilegeKeys, AlterResPrivilegesKeys.RES_BULK_TKT_REMOVE_PAX);
				} else {
					AAReservationUtil.authorize(privilegeKeys, AlterResPrivilegesKeys.RES_REMOVE_PAX);
				}
				
				AAModifyReservationUtil.authorizeModificationForSplit(privilegeKeys, reservation,
						AlterResPrivilegesKeys.ALT_BUFFERTIME_SEG_OR_PAX, null);
				
			}
			
			aaAirBookRS.getResponseAttributes().setSuccess(new AASuccess());

		} catch (ModuleException ex) {
			log.error("Splitting Reservation failed.", ex);
			AAExceptionUtil.addAAErrrors(aaAirBookRS.getResponseAttributes().getErrors(), ex);
		} catch (WebservicesException ex) {
			log.error("modifyResQuery(AAAirBookModifyRQ) failed.", ex);
			AAExceptionUtil.addAAErrrors(aaAirBookRS.getResponseAttributes().getErrors(), ex);
		} 

		return aaAirBookRS;
	}

}
