package com.isa.thinair.aaservices.core.bl.reservation;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAirBookModifyRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAirBookRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAErrorCode;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAModificationTypeCode;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.aaservices.core.session.AASessionManager;
import com.isa.thinair.aaservices.core.transaction.TransactionalReservationProcess;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * @author Nilindra Fernando
 * @since 6:51 PM 11/13/2009
 */
public class AAModifyReservationQueryBL {

	private static Log log = LogFactory.getLog(AAModifyReservationQueryBL.class);

	private AAAirBookModifyRQ aaAirBookModifyRQ;

	private TransactionalReservationProcess tnxResProcess;

	public AAModifyReservationQueryBL(AAAirBookModifyRQ aaAirBookModifyRQ, AASessionManager aaSessionManager) {
		this.aaAirBookModifyRQ = aaAirBookModifyRQ;
		String transactionId = PlatformUtiltiies
				.nullHandler((aaAirBookModifyRQ != null && aaAirBookModifyRQ.getHeaderInfo() != null) ? aaAirBookModifyRQ
						.getHeaderInfo().getTransactionIdentifier() : null);

		if (transactionId.length() > 0 && aaSessionManager != null) {
			this.tnxResProcess = (TransactionalReservationProcess) aaSessionManager.getCurrentUserTransaction(transactionId);
		}
	}

	public AAAirBookRS execute() {
		AAAirBookRS aaAirBookRS = new AAAirBookRS();
		aaAirBookRS.setResponseAttributes(new AAResponseAttributes());

		Reservation reservation = null;
		String pnr = null;
		String version;
		AAModificationTypeCode aaModificationTypeCode;

		try {
			aaModificationTypeCode = aaAirBookModifyRQ.getModificationTypeCode();

			if (aaModificationTypeCode == null) {
				throw new WebservicesException(AAErrorCode.ERR_11_MAXICO_REQUIRED_INVALID_MODIFICATION_TYPE,
						"Modification type is invalid");
			}

			pnr = aaAirBookModifyRQ.getAaReservation().getPnr();
			version = aaAirBookModifyRQ.getAaReservation().getAirReservation().getVersion();

			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setPnr(pnr);
			pnrModesDTO.setLoadFares(true);
			pnrModesDTO.setLoadOndChargesView(true);
			pnrModesDTO.setLoadPaxAvaBalance(true);
			pnrModesDTO.setLoadSegViewBookingTypes(true);
			pnrModesDTO.setLoadSegView(true);
			pnrModesDTO.setLoadLastUserNote(true);
			pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
			pnrModesDTO.setRecordAudit(true);

			if (aaModificationTypeCode == AAModificationTypeCode.MODTYPE_BALANCES_1_FOR_ADD_OND) {
				this.checkAddAndModifySegmentConstraints();
				try {
					reservation = getReservation(pnrModesDTO);
				} catch (Exception ex) {
					// TODO: Temp fix. Handle this properly.
					reservation = null;
				}
				aaAirBookRS = AAModifyReservationQueryUtil.getBalancesForAddOND(reservation, Long.parseLong(version),
						aaAirBookModifyRQ,
						tnxResProcess.getOndFareDTOListForReservation(aaAirBookModifyRQ.getSelectedFareTypes()));

			} else if (aaModificationTypeCode == AAModificationTypeCode.MODTYPE_BALANCES_3_FOR_CANCEL_OND) {
				reservation = AAModifyReservationUtil.getReservation(pnrModesDTO, Long.parseLong(version), null);
				aaAirBookRS = AAModifyReservationQueryUtil.getBalancesForCancelOND(reservation, aaAirBookModifyRQ);

			} else if (aaModificationTypeCode == AAModificationTypeCode.MODTYPE_BALANCES_4_FOR_CANCEL_RESERVATION) {
				reservation = AAModifyReservationUtil.getReservation(pnrModesDTO, Long.parseLong(version), null);
				aaAirBookRS = AAModifyReservationQueryUtil.getBalancesForCancelRes(reservation, aaAirBookModifyRQ);

			} else if (aaModificationTypeCode == AAModificationTypeCode.MODTYPE_13_LAST_ALLOWED_RELEASE_TIME) {
				reservation = AAModifyReservationUtil.getReservation(pnrModesDTO, Long.parseLong(version), null);
				aaAirBookRS = AAModifyReservationQueryUtil.getLastAllowedReleaseTime(reservation);

			} else if (aaModificationTypeCode == AAModificationTypeCode.MODTYPE_16_ADD_INFANT) {
				reservation = AAModifyReservationUtil.getReservation(pnrModesDTO, Long.parseLong(version), null);
				aaAirBookRS = AAModifyReservationQueryUtil.getBalanceForAddInfant(reservation, aaAirBookModifyRQ);
			} else {
				throw new WebservicesException(AAErrorCode.ERR_11_MAXICO_REQUIRED_INVALID_MODIFICATION_TYPE,
						"Modification type [" + aaModificationTypeCode.toString() + "] not supported");
			}
		} catch (Exception ex) {
			log.error("modifyResQuery(AAAirBookModifyRQ) failed.", ex);
			AAExceptionUtil.addAAErrrors(aaAirBookRS.getResponseAttributes().getErrors(), ex);
		}

		return aaAirBookRS;
	}

	private Reservation getReservation(LCCClientPnrModesDTO pnrModesDTO) throws ModuleException {
		Reservation reservation = null;

		try {
			reservation = AAServicesModuleUtils.getReservationBD().getReservation(pnrModesDTO, null);
		} catch (ModuleException e) {
			if (!BeanUtils.nullHandler(e.getExceptionCode()).equals("airreservations.arg.noLongerExist")) {
				throw e;
			}
		}

		return reservation;
	}

	private void checkAddAndModifySegmentConstraints() throws WebservicesException, ModuleException {
		if (tnxResProcess == null) {
			throw new WebservicesException(AAErrorCode.ERR_8_MAXICO_REQUIRED_TRANSACTION_INVALID_OR_EXPIRED,
					"Transaction not found");
		}
		if (!tnxResProcess.hasFareType(aaAirBookModifyRQ.getSelectedFareTypes())) {
			throw new WebservicesException(AAErrorCode.ERR_1_MAXICO_REQUIRED_INVALID_VALUE, "Fares could not be located");
		}
	}
}
