package com.isa.thinair.aaservices.core.bl.reservation;

import java.util.ArrayList;
import java.util.List;

import com.isa.connectivity.profiles.maxico.v1.common.dto.GroundSegment;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAGroundSegmentRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAGroundSegmentRS;
import com.isa.thinair.commons.core.util.SubStationUtil;

public class AALoadGroundSegmentBL {

	private AAGroundSegmentRQ aaGroundSegmentRQ;

	public AAGroundSegmentRS execute() {
		AAGroundSegmentRS aaGroundSegmentRS = new AAGroundSegmentRS();
		List[] arrList = SubStationUtil.getGroundSegmentCombo(aaGroundSegmentRQ.getSelectedOnd(),
				aaGroundSegmentRQ.isAddGroundSegment());
		pupolateGroundSegmentsObj(arrList, aaGroundSegmentRS);
		return aaGroundSegmentRS;
	}

	private static void pupolateGroundSegmentsObj(List[] arrList, AAGroundSegmentRS aaGroundSegmentRS) {
		List<GroundSegment> groundStationsFrom = new ArrayList<GroundSegment>();
		List<GroundSegment> groundStationsTo = new ArrayList<GroundSegment>();

		if (arrList != null && arrList.length == 2) {
			List<String[]> availableGroundStationsFrom = arrList[0];
			List<String[]> availableGroundStationsTo = arrList[1];

			for (String[] groundStationFrom : availableGroundStationsFrom) {
				GroundSegment groundSegment = new GroundSegment();
				groundSegment.setAirportCode(groundStationFrom[0]);
				groundSegment.setAirportName(groundStationFrom[1]);
				groundStationsFrom.add(groundSegment);
			}

			for (String[] groundStationTo : availableGroundStationsTo) {
				GroundSegment groundSegment = new GroundSegment();
				groundSegment.setAirportCode(groundStationTo[0]);
				groundSegment.setAirportName(groundStationTo[1]);
				groundStationsTo.add(groundSegment);
			}

		}
		aaGroundSegmentRS.getGroundStationsFrom().addAll(groundStationsFrom);
		aaGroundSegmentRS.getGroundStationsTo().addAll(groundStationsTo);
	}

	public AAGroundSegmentRQ getAaGroundSegmentRQ() {
		return aaGroundSegmentRQ;
	}

	public void setAaGroundSegmentRQ(AAGroundSegmentRQ aaGroundSegmentRQ) {
		this.aaGroundSegmentRQ = aaGroundSegmentRQ;
	}

}
