package com.isa.thinair.aaservices.core.bl.ticketing;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.SegmentETicketInfo;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAETicketInfoRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAETicketInfoRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.airreservation.api.dto.ResSegmentEticketInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

public class AAEticketInfoBL {
	private static final Log log = LogFactory.getLog(AAEticketInfoBL.class);

	private AAETicketInfoRQ aaETicketInfoRQ;

	private AAETicketInfoRS aaETicketInfoRS;

	public void setAAETicketInfoRQ(AAETicketInfoRQ aaETicketInfoRQ) {
		this.aaETicketInfoRQ = aaETicketInfoRQ;
	}

	private void initializeAAETicketInfoRS() {
		aaETicketInfoRS = new AAETicketInfoRS();
		aaETicketInfoRS.setResponseAttributes(new AAResponseAttributes());
	}

	public AAETicketInfoRS execute() {
		initializeAAETicketInfoRS();

		try {
			if (aaETicketInfoRQ != null && aaETicketInfoRQ.getLccUniqueIDs().size() > 0) {
				List<ResSegmentEticketInfoDTO> eticketInfoDTOs = AAServicesModuleUtils.getSegmentBD().getEticketInformation(
						aaETicketInfoRQ.getLccUniqueIDs(), null);
				composeAASegmentETicketInfo(eticketInfoDTOs);
			}
		} catch (ModuleException ex) {
			log.error("Error occurred during block seat", ex);
			AAExceptionUtil.addAAErrrors(aaETicketInfoRS.getResponseAttributes().getErrors(), ex);
		} finally {
			int numOfErrors = aaETicketInfoRS.getResponseAttributes().getErrors().size();
			AASuccess successType = (numOfErrors == 0 ? new AASuccess() : null);
			aaETicketInfoRS.getResponseAttributes().setSuccess(successType);
		}

		return aaETicketInfoRS;
	}

	private void composeAASegmentETicketInfo(List<ResSegmentEticketInfoDTO> eticketInfoDTOs) {
		for (ResSegmentEticketInfoDTO eticketInfoDTO : eticketInfoDTOs) {
			SegmentETicketInfo segETicketInfo = new SegmentETicketInfo();

			segETicketInfo.setDepartureDateZulu(eticketInfoDTO.getDepartureDateZulu());
			segETicketInfo.setETicketNumber(eticketInfoDTO.geteTicketNumber());
			segETicketInfo.setFlightNumber(eticketInfoDTO.getFlightNumber());
			segETicketInfo.setLccUniqueID(eticketInfoDTO.getLccUniqueID());
			segETicketInfo.setPaxSequence(eticketInfoDTO.getPaxSequence());
			segETicketInfo.setSegmentCode(eticketInfoDTO.getSegmentCode());

			aaETicketInfoRS.getETicketInfo().add(segETicketInfo);
		}
	}
}
