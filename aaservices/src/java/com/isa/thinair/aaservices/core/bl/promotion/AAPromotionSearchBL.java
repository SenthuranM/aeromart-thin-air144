package com.isa.thinair.aaservices.core.bl.promotion;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.ApplicablePromotionDetails;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAError;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAPromoSelectionCriteriaRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAPromoSelectionCriteriaRS;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.TransformerUtil;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.promotion.api.to.ApplicablePromotionDetailsTO;
import com.isa.thinair.promotion.api.to.PromoSelectionCriteria;

public class AAPromotionSearchBL {

	private static Log log = LogFactory.getLog(AAPromotionSearchBL.class);

	private AAPromoSelectionCriteriaRQ aaPromoSelectionCriteriaRQ;

	public AAPromoSelectionCriteriaRS execute() {
		AAPromoSelectionCriteriaRS aaPromoSelectionCriteriaRS = new AAPromoSelectionCriteriaRS();
		try {
			PromoSelectionCriteria promoSelectionCriteria = TransformerUtil
					.transformFromPromotionSelectionCriteria(aaPromoSelectionCriteriaRQ.getPromoSelectionCriteria());
			ApplicablePromotionDetailsTO pickApplicablePromotions = AAServicesModuleUtils.getAirproxySearchAndQuoteBD()
					.pickApplicablePromotions(promoSelectionCriteria, SYSTEM.AA, null);
			
			ApplicablePromotionDetails aaApplicablePromotionDetails = TransformerUtil
					.transformFromApplicablePromotionDetailsTO(pickApplicablePromotions);
			aaPromoSelectionCriteriaRS.setApplicablePromotionDetails(aaApplicablePromotionDetails);
		} catch (ModuleException e) {
			log.error("Error occured while searching promotions" + e);
			AAError aaError = new AAError();
			aaError.setDescription(e.getMessage());
			aaPromoSelectionCriteriaRS.getResponseAttributes().getErrors().add(aaError);
			aaPromoSelectionCriteriaRS.getResponseAttributes().setSuccess(null);
		}
		return aaPromoSelectionCriteriaRS;
	}

	public AAPromoSelectionCriteriaRQ getAaPromoSelectionCriteriaRQ() {
		return aaPromoSelectionCriteriaRQ;
	}

	public void setAaPromoSelectionCriteriaRQ(AAPromoSelectionCriteriaRQ aaPromoSelectionCriteriaRQ) {
		this.aaPromoSelectionCriteriaRQ = aaPromoSelectionCriteriaRQ;
	}
}