package com.isa.thinair.aaservices.core.bl.ChangeFare;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.FareSegment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.IntegerStringMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PriceInfo;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAChangeFareRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAChangeFareRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.core.bl.availability.AAPaxCountAssembler;
import com.isa.thinair.aaservices.core.session.AASessionManager;
import com.isa.thinair.aaservices.core.transaction.TransactionalReservationProcess;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.AAUtils;
import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.ChangeFaresDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentsFareDTO;
import com.isa.thinair.airproxy.api.model.reservation.availability.ChangeFareRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.assembler.IPaxCountAssembler;
import com.isa.thinair.airproxy.api.utils.assembler.PaxCountAssembler;
import com.isa.thinair.airproxy.api.utils.converters.AuxillaryHelper;
import com.isa.thinair.airproxy.api.utils.converters.AvailabilityConvertUtil;
import com.isa.thinair.airproxy.api.utils.converters.FareConvertUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class AAChangeFareBL {

	private final Log logger = LogFactory.getLog(AAChangeFareBL.class);

	private AAChangeFareRQ aaChangeFareRQ;
	private TransactionalReservationProcess transactionalReservationProcess;

	public void setParams(AAChangeFareRQ aaChangeFareRQ) {
		this.aaChangeFareRQ = aaChangeFareRQ;
	}

	public void setTransactionalReservationProcess(TransactionalReservationProcess transactionalReservationProcess) {
		this.transactionalReservationProcess = transactionalReservationProcess;
	}

	public AAChangeFareRS execute() {
		AAChangeFareRS aaRecalculateChargesRS = new AAChangeFareRS();
		aaRecalculateChargesRS.setResponseAttributes(new AAResponseAttributes());
		try {

			// Getting existing fare changers

			Collection<OndFareDTO> ondFareDTOs = transactionalReservationProcess.getOndFareDTOListForReservation(
					aaChangeFareRQ.getSelectedFareTypes(), false);

			// ChangeFaresDTO changeFaresDTO = null;
			UserPrincipal userPrincipal = AASessionManager.getInstance().getCurrentUserPrincipal();
			AuxillaryHelper auxHelper = new AuxillaryHelper(AAServicesModuleUtils.getReservationAuxillaryBD());

			ChangeFareRQ fareAvailRQ = new ChangeFareRQ(AAChangeFareUtils.populateBaseAvailRQ(aaChangeFareRQ, "allfares"));
			populateAdditionalChangeFareInfo(fareAvailRQ, aaChangeFareRQ);

			AvailableFlightSearchDTO availableFlightSearchDTO = AvailabilityConvertUtil.getAvailableFlightSearchDTO(fareAvailRQ,
					userPrincipal, true, auxHelper);
			availableFlightSearchDTO.setSkipInvCheckForFlown(AppSysParamsUtil.skipInvChkForFlownInRequote());
			// FIXME DILAN 21 DEC
			// FareQuoteUtil.setHalfReturnFareQuoteOption(availableFlightSearchDTO, fareAvailRQ.getAvailPreferences()
			// .isModifyBooking());
			ChangeFaresDTO changeFaresDTO = new ChangeFaresDTO();
			changeFaresDTO.setPosAirport(fareAvailRQ.getPosAirport());
			IPaxCountAssembler paxAssm = new PaxCountAssembler(fareAvailRQ.getTravelerInfoSummary()
					.getPassengerTypeQuantityList());
			changeFaresDTO.setInfantSeats(paxAssm.getInfantCount());
			availableFlightSearchDTO.setOwnSearch(false);
			if (fareAvailRQ.getChangedFareSegments() != null && fareAvailRQ.getChangedFareSegments().size() > 0) {
				changeFaresDTO.setExistingFareChargesSeatsSegmentsDTOs(ondFareDTOs);
				for (SegmentsFareDTO chgFareSeg : fareAvailRQ.getChangedFareSegments()) {
					changeFaresDTO.addChangedFaresSeatsSegmentsDTO(chgFareSeg);
				}

				changeFaresDTO = AirproxyModuleUtils.getFlightInventoryResBD().recalculateChargesForFareChange(changeFaresDTO,
						availableFlightSearchDTO);
			} else {
				changeFaresDTO.setExistingFareChargesSeatsSegmentsDTOs(ondFareDTOs);
				changeFaresDTO.setNewFareChargesSeatsSegmentsDTOs(ondFareDTOs);

				if (ondFareDTOs != null) {
					for (OndFareDTO ondFareDTO : ondFareDTOs) {
						changeFaresDTO.setChangedFareType(ondFareDTO.getFareType());
						break;
					}
				}
			}

			FlightPriceRS flightPriceRS = AvailabilityConvertUtil.getFlightRS(changeFaresDTO, fareAvailRQ,
					userPrincipal.getAgentCode());

			aaRecalculateChargesRS.getOriginDestinationInformation().addAll(
					AAChangeFareUtils.populateOndInfoList(flightPriceRS.getOriginDestinationInformationList()));
			PriceInfo priceInfo = AAChangeFareUtils.populatePriceInfo(flightPriceRS.getSelectedPriceFlightInfo());
			aaRecalculateChargesRS.setPriceInfo(priceInfo);
			aaRecalculateChargesRS.setHeaderInfo(AAUtils.populateHeaderInfo(flightPriceRS.getTransactionIdentifier()));
			aaRecalculateChargesRS.setOpenReturnOptionsTO(AAChangeFareUtils.populateOpenReturnOptions(flightPriceRS
					.getOpenReturnOptionsTO()));

			Collection<OndFareDTO> ondFareDTOList = changeFaresDTO.getNewFareChargesSeatsSegmentsDTOs();
			AAPaxCountAssembler paxCAsm = new AAPaxCountAssembler(aaChangeFareRQ.getTravelerInfoSummary().getAirTravelerAvail()
					.getPassengerTypeQuantity());
			FareSegChargeTO fareSegChargeTO = null;
			List<String> segmentCodeList = new ArrayList<String>();
			Map<Integer, Integer> actualOndSequence = new HashMap<Integer, Integer>();
			for (OndFareDTO ondFareDTO : ondFareDTOList) {
				actualOndSequence.put(ondFareDTO.getOndSequence(), getActualOndSequence(ondFareDTO.getOndSequence()));
			}
			if (ondFareDTOList != null) {
				fareSegChargeTO = FareConvertUtil.getFareSegChargeTO(changeFaresDTO.getChangedFareType(), ondFareDTOList,
						new AAPaxCountAssembler(aaChangeFareRQ.getTravelerInfoSummary().getAirTravelerAvail()
								.getPassengerTypeQuantity()), null);
				if (availableFlightSearchDTO.isInboundFareQuote() && !availableFlightSearchDTO.isReturnFlag()) {
					fareSegChargeTO.setInbound(true);
				}

				Map<Integer, BundledFareDTO> ondSelectedBundledServices = changeFaresDTO.getOndSelectedBundledFares();
				Map<Integer, BundledFareDTO> allBundledServiceSelection = new TreeMap<Integer, BundledFareDTO>();

				for (OndFareDTO ondFareDTO : ondFareDTOList) {
					for (FlightSegmentDTO segment : ondFareDTO.getSegmentsMap().values()) {
						segmentCodeList.add(segment.getSegmentCode());

						if (ondSelectedBundledServices != null) {
							transactionalReservationProcess.addSegmentBundledFare(segment.getSegmentCode(),
									ondSelectedBundledServices.get(ondFareDTO.getOndSequence()));
						}
					}

					// Set Selected Bundled service
					if (ondSelectedBundledServices != null) {
						allBundledServiceSelection.put(ondFareDTO.getOndSequence(),
								ondSelectedBundledServices.get(ondFareDTO.getOndSequence()));
					}
				}

				fareSegChargeTO.setOndBundledFareDTOs(new ArrayList<BundledFareDTO>(allBundledServiceSelection.values()));
			}

			int count = 0;
			for (String segCode : segmentCodeList) {
				List<String> segList = new ArrayList<String>();
				segList.add(segCode);
				if (count == 0) {
					transactionalReservationProcess.setOndFareSegChargeTOList(segList, fareSegChargeTO, paxCAsm);
				} else {
					transactionalReservationProcess.addOndFareSegChargeTOList(segList, fareSegChargeTO);
				}
				count++;
			}
		} catch (ModuleException ex) {
			AAExceptionUtil.addAAErrrors(aaRecalculateChargesRS.getResponseAttributes().getErrors(), ex);
		} catch (Exception e) {
			logger.error("Error Occured in AAChangeFareBL ", e);
		} finally {
			int numOfErrors = aaRecalculateChargesRS.getResponseAttributes().getErrors().size();
			AASuccess successType = (numOfErrors == 0 ? new AASuccess() : null);
			aaRecalculateChargesRS.getResponseAttributes().setSuccess(successType);
		}

		return aaRecalculateChargesRS;
	}

	private int getActualOndSequence(int ondSequence) {
		if (aaChangeFareRQ.getOriginDestinationInformation().size() > ondSequence) {
			return aaChangeFareRQ.getOriginDestinationInformation().get(ondSequence).getOndSequence();
		}
		return ondSequence;
	}

	private void populateAdditionalChangeFareInfo(ChangeFareRQ fareAvailRQ, AAChangeFareRQ aaChangeFareRQ2) {
		fareAvailRQ.setPosAirport(aaChangeFareRQ2.getPosAirport());
		List<SegmentsFareDTO> changedFareSegments = new ArrayList<SegmentsFareDTO>();
		for (FareSegment fareSegment : aaChangeFareRQ2.getChangedFareSegments()) {
			SegmentsFareDTO fareSegDTO = new SegmentsFareDTO();
			fareSegDTO.setCabinClassCode(fareSegment.getCabinClass());
			fareSegDTO.setFareSummaryDTO(AAChangeFareUtils.populateFareSummaryDTO(fareSegment.getFareSummary()));
			fareSegDTO.setFareType(fareSegment.getFareType());
			fareSegDTO.setFixedQuotaSeats(fareSegment.isFixedQuotaSeats());
			fareSegDTO.setInbound(fareSegment.isInbound());
			fareSegDTO.setLogicalCabinClassCode(fareSegment.getLogicalCabinClass());
			fareSegDTO.setOndCode(fareSegment.getOndCode());
			fareSegDTO.setOndSequence(fareSegment.getOndSequence());
			fareSegDTO.getSeatDistributions().addAll(
					AAChangeFareUtils.getSeatDustributionList(fareSegment.getSeatDistributions()));
			for (IntegerStringMap segmentRPHMap : fareSegment.getSegmentsMap()) {
				fareSegDTO.getSegmentRPHMap().put(segmentRPHMap.getKey(), segmentRPHMap.getValue());
			}
			changedFareSegments.add(fareSegDTO);
		}
		fareAvailRQ.setChangedFareSegments(changedFareSegments);
	}
}
