package com.isa.thinair.aaservices.core.bl.ChangeFare;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AirTravelerAvail;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AvailPreferences;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BaseFare;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ExternalCharge;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FareCharacteristics;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FareRule;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FareSummary;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Fee;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlexiInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.IntegerBooleanMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.IntegerIntegerMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.LogicalCabinSeatAvailability;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ModifyPreferences;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ONDExternalCharge;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OldFareDetails;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OndSeatAvailability;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OpenReturnOptions;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OriginDestinationInformation;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OriginDestinationOption;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OriginDestinationOptions;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PassengerTypeQuantity;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PerPaxPriceInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PriceInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SeatAvailability;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Surcharge;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Tax;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAFlightAvailRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAPOS;
import com.isa.thinair.aaservices.core.util.AAServicesTransformUtil;
import com.isa.thinair.aaservices.core.util.AAUtils;
import com.isa.thinair.aaservices.core.util.PaxTypeUtils;
import com.isa.thinair.aaservices.core.util.TransformerUtil;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareSummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndSequence;
import com.isa.thinair.airinventory.api.dto.seatavailability.SeatDistribution;
import com.isa.thinair.airproxy.api.model.reservation.availability.BaseAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FareAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FlightPriceRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.AvailPreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.BaseFareTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ExternalChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareRuleDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareTypeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FeeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlexiInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.LogicalCabinClassInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ONDExternalChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OndClassOfServiceSummeryTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OpenReturnOptionsTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PassengerTypeQuantityTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PerPaxPriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PriceInfoTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.SurchargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TaxTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TravelPreferencesTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.TravelerInfoSummaryTO;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.StringUtil;

class AAChangeFareUtils {

	/**
	 * convert FlightAvail to FlightAvail
	 * 
	 * @param flightAvail
	 * @param param
	 * @return
	 */
	static BaseAvailRQ populateBaseAvailRQ(AAFlightAvailRQ flightAvail, String param) {
		BaseAvailRQ baseAvailRQ = new BaseAvailRQ();
		if ("flight".equals(param)) {
			baseAvailRQ = new FlightAvailRQ();
		} else if ("price".equals(param)) {
			baseAvailRQ = new FlightPriceRQ();
		} else if ("allfares".equals(param)) {
			baseAvailRQ = new FareAvailRQ();
		}

		// populate AvailPreferencesTO
		AvailPreferencesTO availTO = baseAvailRQ.getAvailPreferences();
		if (flightAvail.getAvailPreferences() != null) {
			AvailPreferences availPref = flightAvail.getAvailPreferences();
			availTO.setRestrictionLevel(availPref.getRestrictionLevel());
			availTO.setBookingPaxType(availPref.getBookingPaxType());
			availTO.setFareCategoryType(availPref.getFareCategoryType());
			availTO.setHalfReturnFareQuote(availPref.isIncludeHalfReturn());
			availTO.setStayOverTimeInMillis(availPref.getStayOverTimeInMillis());
			availTO.setInboundFareModified(availPref.isInboundOnd());
			availTO.setModifyBooking(availPref.isModifyBooking());
			availTO.setRequoteFlightSearch(availPref.isRequoteFlightSearch());
			availTO.setAllowOverrideSameOrHigherFareMod(availPref.isAllowOverrideSameOrHigherFareMod());

			if (availPref.getQuoteOndFlexi() != null) {
				for (IntegerBooleanMap mapObj : availPref.getQuoteOndFlexi()) {
					availTO.setQuoteOndFlexi(mapObj.getKey(), mapObj.isValue());
				}
			}

			availTO.setTravelAgentCode(availPref.getTravelAgentCode());

			if (availPref.getAppIndicator() == null) {
				availTO.setAppIndicator(ApplicationEngine.XBE);
			} else {
				availTO.setAppIndicator(ApplicationEngine.valueOf(availPref.getAppIndicator()));
			}

			availTO.setPromoCode(availPref.getPromoCode());
			availTO.setBankIdentificationNumber(availPref.getBankIdNumber());
			availTO.setPromoApplicable(availPref.isSingleCarrier());
			availTO.setBundledFareApplicable(availPref.isSingleCarrier());
			availTO.setPreserveOndOrder(availPref.isPreserveOndOrder());

			if (availPref.getModifyPref() != null) {
				ModifyPreferences modifyPref = availPref.getModifyPref();
				availTO.setFlightsPerOndRestriction(modifyPref.getFlightsPerOndRestriction());
				availTO.setOldFareAmount(modifyPref.getOldFareAmount());
				availTO.setOldFareID(modifyPref.getOldFareId());
				availTO.setOldFareType(modifyPref.getOldFareType());

				Collection<FlightSegmentDTO> existinfSegList = new ArrayList<FlightSegmentDTO>();
				if (modifyPref.getExistingSegments() != null) {
					for (FlightSegment fSeg : modifyPref.getExistingSegments()) {
						existinfSegList.add(populateFlightSegmentDTO(fSeg));
					}
				}

				Collection<FlightSegmentDTO> modifiedSegList = new ArrayList<FlightSegmentDTO>();
				if (modifyPref.getModifiedSegmentsList() != null) {
					for (FlightSegment fSeg : modifyPref.getModifiedSegmentsList()) {
						modifiedSegList.add(populateFlightSegmentDTO(fSeg));
					}
				}

				Collection<FlightSegmentDTO> inverseSegList = new ArrayList<FlightSegmentDTO>();
				if (modifyPref.getInverseSegmentsList() != null) {
					for (FlightSegment fSeg : modifyPref.getInverseSegmentsList()) {
						inverseSegList.add(populateFlightSegmentDTO(fSeg));
					}
				}

				availTO.setExistingSegmentsList((existinfSegList.size() == 0) ? null : existinfSegList);
				availTO.setModifiedSegmentsList((modifiedSegList.size() == 0) ? null : modifiedSegList);
				availTO.setInverseSegmentsList((inverseSegList.size() == 0) ? null : inverseSegList);
				availTO.setInverseFareID(modifyPref.getInverseFareId());

				if (availPref.getModifyPref().isInboundModified() != null) {
					availTO.setInboundFareModified(availPref.getModifyPref().isInboundModified());
				}
			}
		}

		// populate TravelPreferencesTO
		TravelPreferencesTO travelPrefTO = baseAvailRQ.getTravelPreferences();
		travelPrefTO.setBookingType(flightAvail.getTravelPreferences().getBookingType().toString());

		// TODO: BC and other params passing should be catered
		// travelPrefTO.setBookingClassCode(flightAvail.getBookingClassCode());
		// travelPrefTO.setOpenReturnConfirm(flightAvail.isOpenReturnConfirm());

		// populate TravelerInfoSummaryTO
		TravelerInfoSummaryTO travelerInfoTO = baseAvailRQ.getTravelerInfoSummary();

		if (flightAvail.getTravelerInfoSummary() != null && flightAvail.getTravelerInfoSummary().getAirTravelerAvail() != null) {
			AirTravelerAvail airTravelerAvail = flightAvail.getTravelerInfoSummary().getAirTravelerAvail();
			if (airTravelerAvail.getPassengerTypeQuantity() != null) {
				for (PassengerTypeQuantity paxTypeQty : airTravelerAvail.getPassengerTypeQuantity()) {
					PassengerTypeQuantityTO paxTypeQtyTO = new PassengerTypeQuantityTO();
					paxTypeQtyTO.setPassengerType(PaxTypeUtils.convertLCCPaxCodeToAAPaxCodes(paxTypeQty.getPassengerType()));
					paxTypeQtyTO.setQuantity(paxTypeQty.getQuantity());
					travelerInfoTO.getPassengerTypeQuantityList().add(paxTypeQtyTO);
				}
			}
		}

		// populate OnDInformationList
		Collection<OriginDestinationInformationTO> ondInfoList = baseAvailRQ.getOriginDestinationInformationList();
		if (flightAvail.getOriginDestinationInformation() != null) {
			for (OriginDestinationInformation ondInfo : flightAvail.getOriginDestinationInformation()) {
				OriginDestinationInformationTO ondInfoTO = new OriginDestinationInformationTO();
				ondInfoTO.setDepartureDateTimeStart(ondInfo.getDepartureDateTimeStart());
				ondInfoTO.setDepartureDateTimeEnd(ondInfo.getDepartureDateTimeEnd());
				ondInfoTO.setOrigin(ondInfo.getOriginLocation());
				ondInfoTO.setDestination(ondInfo.getDestinationLocation());
				ondInfoTO.setPreferredDate(ondInfo.getPreferredDate());
				ondInfoTO.setPreferredClassOfService(ondInfo.getPreferredCabinClass());
				ondInfoTO.setPreferredLogicalCabin(ondInfo.getPreferredLogicalCabin());
				ondInfoTO.setPreferredBookingType(ondInfo.getPreferredBookingType());
				ondInfoTO.setPreferredBookingClass(ondInfo.getPreferredBookingClass());
				ondInfoTO.setPreferredBundleFarePeriodId(ondInfo.getPreferredBundledFarePeriodId());
				ondInfoTO.setPreferredBundleFarePeriodId(ondInfo.getPreferredBundledFarePeriodId());
				ondInfoTO.setReturnFlag(ondInfo.isReturnFlag());
				ondInfoTO.getExistingFlightSegIds().addAll(ondInfo.getExistingFlightSegIds());
				ondInfoTO.getExistingPnrSegRPHs().addAll(ondInfo.getExistingPnrSegIds());
				ondInfoTO.setSpecificFlightsAvailability(ondInfo.isSpecificFlightsAvailability());
				if (ondInfo.getDateChangedResSegList() != null && ondInfo.getDateChangedResSegList().size() > 0) {
					ondInfoTO.setDateChangedResSegList(ondInfo.getDateChangedResSegList());
				}

				if (ondInfo.getOldFareDetails() != null && ondInfo.getOldFareDetails().size() > 0) {
					List<FareTO> oldPerPaxFareTOs = new ArrayList<FareTO>();
					OldFareDetails oldFareDetails = getOwnOldPerPaxFareTO(ondInfo.getOldFareDetails(), null, null);
					if (oldFareDetails != null) {
						FareTO oldPerPaxFareTO = new FareTO();
						if (oldFareDetails.getOldFareType() != null) {
							oldPerPaxFareTO.setFareType(Integer.toString(oldFareDetails.getOldFareType()));
						}
						oldPerPaxFareTO.setCarrierCode(oldFareDetails.getCarrierCode());
						oldPerPaxFareTO.setFareId(oldFareDetails.getOldFareId());
						oldPerPaxFareTO.setAmount(oldFareDetails.getOldFareAmount());
						oldPerPaxFareTO.setSegmentCode(oldFareDetails.getOndCode());
						oldPerPaxFareTO.setAppliedFlightSegId(oldFareDetails.getFlightSegId());
						oldPerPaxFareTO.setLogicalCCCode(oldFareDetails.getLogicalCCCode());
						oldPerPaxFareTO.setBulkTicketFareRule( oldFareDetails.isBulkTicketFareRule() != null 
								? oldFareDetails.isBulkTicketFareRule() 
								: false);
						oldPerPaxFareTOs.add(oldPerPaxFareTO);
					}
					ondInfoTO.setOldPerPaxFareTOList(oldPerPaxFareTOs);
				}

				if (ondInfo.getOriginDestinationOptions() != null
						&& ondInfo.getOriginDestinationOptions().getOriginDestinationOption() != null) {
					for (OriginDestinationOption ondOption : ondInfo.getOriginDestinationOptions().getOriginDestinationOption()) {
						OriginDestinationOptionTO ondOptionTo = new OriginDestinationOptionTO();
						ondInfoTO.getOrignDestinationOptions().add(ondOptionTo);
						if (ondOption.getFlightSegment() != null) {
							ondOptionTo.getFlightSegmentList().addAll(tranformSegments(ondOption.getFlightSegment()));
						}
					}
				}

				ondInfoList.add(ondInfoTO);
			}
		}

		return baseAvailRQ;
	}

	/**
	 * Convert FlightSegment to FlightSegmentDTO
	 * 
	 * @param flightSegment
	 * @return
	 */
	private static FlightSegmentDTO populateFlightSegmentDTO(FlightSegment flightSegment) {
		FlightSegmentDTO flightSegDTO = new FlightSegmentDTO();
		flightSegDTO.setFlightId(flightSegment.getFlightId());
		flightSegDTO.setSegmentCode(flightSegment.getSegmentCode());
		flightSegDTO.setFromAirport(flightSegment.getDepartureAirportCode());
		flightSegDTO.setToAirport(flightSegment.getArrivalAirportCode());
		flightSegDTO.setDepartureDateTime(flightSegment.getDepatureDateTime());
		flightSegDTO.setArrivalDateTime(flightSegment.getArrivalDateTime());
		flightSegDTO.setDepartureDateTimeZulu(flightSegment.getDepatureDateTimeZulu());
		flightSegDTO.setArrivalDateTimeZulu(flightSegment.getArrivalDateTimeZulu());
		flightSegDTO.setFlightNumber(flightSegment.getFlightNumber());
		flightSegDTO.setSegmentId(flightSegment.getSegmentId());
		flightSegDTO.setOperationTypeID((flightSegment.getOperationType() == 0) ? null : flightSegment.getOperationType());
		flightSegDTO.setFlightStatus(flightSegment.getFlightStatus());
		return flightSegDTO;
	}

	static UserPrincipal populateUserPrincipal(AAPOS aaPOS) {
		String strSalesChCode = AAUtils.getSalesChannelFromPOS(aaPOS);
		int salesChCode = 0;

		if (strSalesChCode != null && !"".equals(strSalesChCode)) {
			salesChCode = (new Integer(strSalesChCode)).intValue();
		}

		return (UserPrincipal) UserPrincipal.createIdentity(null, salesChCode, aaPOS.getMarketingAgentCode(),
				aaPOS.getAirportCode(), null, null, null, null, null, null, null, null, null, null, null, null, null);
	}

	static OpenReturnOptions populateOpenReturnOptions(OpenReturnOptionsTO openReturnOptionsTO) {
		OpenReturnOptions openReturnOptions = null;
		if (openReturnOptionsTO != null) {
			openReturnOptions = new OpenReturnOptions();
			openReturnOptions.setConfirmExpiryDate(openReturnOptionsTO.getConfirmExpiryDate());
			openReturnOptions.setTravelExpiryDate(openReturnOptionsTO.getTravelExpiryDate());
			openReturnOptions.setAirportCode(openReturnOptionsTO.getAirportCode());
		}
		return openReturnOptions;
	}

	// populate PriceInfo to PriceInfoTO
	static PriceInfo populatePriceInfo(PriceInfoTO priceInfoTO) {
		PriceInfo priceInfo = new PriceInfo();
		boolean populate = true;
		Date now = CalendarUtil.getCurrentSystemTimeInZulu();

		FareTypeTO fareTypeTO = priceInfoTO.getFareTypeTO();

		if (fareTypeTO.getPerPaxPriceInfo() != null) {
			int count = 0;
			for (PerPaxPriceInfoTO perPaxTO : fareTypeTO.getPerPaxPriceInfo()) {
				PerPaxPriceInfo perPaxPriceInfo = new PerPaxPriceInfo();
				perPaxPriceInfo.setPassengerType(PaxTypeUtils.convertAAPaxCodeToLCCPaxCodes(perPaxTO.getPassengerType()));
				perPaxPriceInfo.setTravelerRefNumber(new Integer(count++).toString());
				perPaxPriceInfo.setPassengerPrice(populateFareType(perPaxTO.getPassengerPrice(), now));
				priceInfo.getPerPaxPriceInfo().add(perPaxPriceInfo);
			}
		}

		if (populate) {
			com.isa.connectivity.profiles.maxico.v1.common.dto.FareType fareType = populateFareType(fareTypeTO, now);
			priceInfo.setFareType(fareType);
			priceInfo.setOnholdRestricted(fareTypeTO.isOnHoldRestricted());
			priceInfo.setOnholdBufferTimeZulu(fareTypeTO.getOnHoldStartTime());
			priceInfo.setOnholdReleaseTimeZulu(fareTypeTO.getOnHoldReleaseTimeStamp());
			priceInfo.setFlightCloseTimeZulu(fareTypeTO.getOnHoldStopTime());
			priceInfo.getApplicableFareRule().addAll(populateFareRuleList(fareTypeTO.getApplicableFareRules()));
			priceInfo.setPromotionDetails(AAServicesTransformUtil.transformApplicablePromotionDetailsTO(fareTypeTO
					.getPromotionTO()));
			if (fareTypeTO.getOndWiseFareType() != null) {
				for (Entry<Integer, Integer> fareTypeEntry : fareTypeTO.getOndWiseFareType().entrySet()) {
					IntegerIntegerMap ondFareTypeObj = new IntegerIntegerMap();
					ondFareTypeObj.setKey(fareTypeEntry.getKey());
					ondFareTypeObj.setValue(fareTypeEntry.getValue());
					priceInfo.getOndFareType().add(ondFareTypeObj);
				}
			}

			if (priceInfoTO.getAvailableLogicalCCList() != null && !priceInfoTO.getAvailableLogicalCCList().isEmpty()) {
				List<OndClassOfServiceSummeryTO> ondClassOfServiceSummeryTOs = priceInfoTO.getAvailableLogicalCCList();
				List<OndSeatAvailability> ondSeatAvailabilities = priceInfo.getOndSeatAvailability();
				for (OndClassOfServiceSummeryTO ondClassOfServiceSummeryTO : ondClassOfServiceSummeryTOs) {
					OndSeatAvailability ondSeatAvailability = new OndSeatAvailability();

					int ondSequence = ondClassOfServiceSummeryTO.getSequence();
					ondSeatAvailability.setOndSequence(ondSequence);

					List<LogicalCabinClassInfoTO> availableLogicalCCList = ondClassOfServiceSummeryTO.getAvailableLogicalCCList();
					if (availableLogicalCCList != null && !availableLogicalCCList.isEmpty()) {
						for (LogicalCabinClassInfoTO logicalCabinClassInfoTO : availableLogicalCCList) {
							SeatAvailability seatAvailability = new SeatAvailability();
							if (logicalCabinClassInfoTO.getBookingClasses() != null) {
								seatAvailability.getBookingCodes().addAll(logicalCabinClassInfoTO.getBookingClasses());
							}
							seatAvailability.setBundledFarePeriodId(logicalCabinClassInfoTO.getBundledFarePeriodId());
							seatAvailability.setBundledFareFee(logicalCabinClassInfoTO.getBundledFareFee());
							seatAvailability.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
							seatAvailability.setImageUrl(logicalCabinClassInfoTO.getImageUrl());
							seatAvailability.setBundleFareDescriptionInfo(TransformerUtil.transform(logicalCabinClassInfoTO
									.getBundleFareDescriptionTemplateDTO()));
							seatAvailability.setComment(logicalCabinClassInfoTO.getComment());
							if (logicalCabinClassInfoTO.getFlexiRuleID() != null) {
								seatAvailability.setFlexiRuleID(logicalCabinClassInfoTO.getFlexiRuleID());
							}
							seatAvailability.setLogicalCabinClass(logicalCabinClassInfoTO.getLogicalCCCode());
							seatAvailability.setLogicalCabinClassDescription(logicalCabinClassInfoTO.getLogicalCCDesc());
							seatAvailability.setNestRank(logicalCabinClassInfoTO.getRank());
							seatAvailability.setSelected(logicalCabinClassInfoTO.isSelected());
							seatAvailability.setFlexiAvailable(logicalCabinClassInfoTO.isFlexiAvailable());
							seatAvailability.setWithFlexi(logicalCabinClassInfoTO.isWithFlexi());

							seatAvailability.setJourneyOndCode(ondClassOfServiceSummeryTO.getOndCode());

							ondSeatAvailability.getSeatAvailability().add(seatAvailability);
						}
					}

					ondSeatAvailabilities.add(ondSeatAvailability);
				}
			}

			populate = false;
		}

		return priceInfo;
	}

	private static com.isa.connectivity.profiles.maxico.v1.common.dto.FareType populateFareType(FareTypeTO fareTypeTO, Date date) {
		com.isa.connectivity.profiles.maxico.v1.common.dto.FareType fareType = new com.isa.connectivity.profiles.maxico.v1.common.dto.FareType();
		fareType.setTotalFare(fareTypeTO.getTotalBaseFare());
		fareType.setTotalTaxes(fareTypeTO.getTotalTaxes());
		fareType.setTotalSurcharges(fareTypeTO.getTotalSurcharges());
		fareType.setTotalFees(fareTypeTO.getTotalFees());
		fareType.setTotalPrice(fareTypeTO.getTotalPrice());
		fareType.setTotalPriceWithInfant(fareTypeTO.getTotalPriceWithInfant());
		// FIXME OND seq is harcoded
		fareType.setTotalOutBoundExternalCharges(fareTypeTO.getOndExternalCharge(OndSequence.OUT_BOUND)
				.getTotalOndExternalCharges());
		if (fareTypeTO.hasOndExternalCharge(OndSequence.IN_BOUND)) {
			// FIXME OND seq is harcoded
			fareType.setTotalInBoundExternalCharges(fareTypeTO.getOndExternalCharge(OndSequence.IN_BOUND)
					.getTotalOndExternalCharges());
		}

		fareType.getBaseFare().addAll(populateBaseFares(fareTypeTO.getBaseFares(), date));
		fareType.getTaxes().addAll(populateTaxes(fareTypeTO.getTaxes(), date));
		fareType.getSurcharges().addAll(populateSurcharges(fareTypeTO.getSurcharges(), date));
		fareType.getFees().addAll(populateFees(fareTypeTO.getFees(), date));

		for (ONDExternalChargeTO ondExternalChargeTO : fareTypeTO.getOndExternalCharges()) {
			fareType.getOndExternalCharges().addAll(
					populateOnDExternalCharge(ondExternalChargeTO.getExternalCharges(), date,
							ondExternalChargeTO.getTotalOndExternalCharges(), ondExternalChargeTO.getOndSequence()));
		}

		return fareType;
	}

	private static List<ExternalCharge> populateExternalCharge(List<ExternalChargeTO> externalChargeTOList, Date date) {
		List<ExternalCharge> externalCharges = null;
		if (externalChargeTOList != null) {
			externalCharges = new ArrayList<ExternalCharge>();
			for (ExternalChargeTO externalChargeTO : externalChargeTOList) {
				ExternalCharge externalCharge = new ExternalCharge();
				externalCharge.setAmount(externalChargeTO.getAmount());
				externalCharge.setCarrierCode(externalChargeTO.getCarrierCode());
				externalCharge.setSegmentCode(externalChargeTO.getSegmentCode());
				externalCharge.setCode(externalChargeTO.getSurchargeCode());
				externalCharge.setName(externalChargeTO.getSurchargeName());
				externalCharge.setApplicableDateTime(date);
				for (String paxType : externalChargeTO.getApplicablePassengerTypeCode()) {
					externalCharge.getApplicablePassengerType().add(PaxTypeUtils.convertAAPaxCodeToLCCPaxCodes(paxType));
				}
				Collection<FlexiInfo> flexiInfoList = new ArrayList<FlexiInfo>();
				for (FlexiInfoTO flexiInfoTO : externalChargeTO.getAdditionalDetails()) {
					FlexiInfo flexiInfo = new FlexiInfo();
					flexiInfo.setFlexiRateId(flexiInfoTO.getFlexiRateId());
					flexiInfo.setAvailableCount(flexiInfoTO.getAvailableCount());
					flexiInfo.setCutOverBufferInMins(flexiInfoTO.getCutOverBufferInMins());
					flexiInfo.setFlexibilityTypeId(flexiInfoTO.getFlexibilityTypeId());
					flexiInfoList.add(flexiInfo);
				}
				externalCharge.getAdditionalDetails().addAll(flexiInfoList);
				externalCharge.setType(AAUtils.getExternalChargeType(externalChargeTO.getType()));
				externalCharges.add(externalCharge);
			}
		}
		return externalCharges;
	}

	private static List<ONDExternalCharge> populateOnDExternalCharge(List<ExternalChargeTO> externalChargeTOList, Date date,
			BigDecimal totalOnDExternalCharges, int ondSequence) {
		List<ExternalCharge> externalCharges = null;
		List<ONDExternalCharge> ondExternalCharges = null;
		if (externalChargeTOList != null) {
			externalCharges = new ArrayList<ExternalCharge>();
			ondExternalCharges = new ArrayList<ONDExternalCharge>();
			for (ExternalChargeTO externalChargeTO : externalChargeTOList) {
				ExternalCharge externalCharge = new ExternalCharge();
				externalCharge.setAmount(externalChargeTO.getAmount());
				externalCharge.setCarrierCode(externalChargeTO.getCarrierCode());
				externalCharge.setSegmentCode(externalChargeTO.getSegmentCode());
				externalCharge.setCode(externalChargeTO.getSurchargeCode());
				externalCharge.setName(externalChargeTO.getSurchargeName());
				externalCharge.setApplicableDateTime(date);
				for (String paxType : externalChargeTO.getApplicablePassengerTypeCode()) {
					externalCharge.getApplicablePassengerType().add(PaxTypeUtils.convertAAPaxCodeToLCCPaxCodes(paxType));
				}
				Collection<FlexiInfo> flexiInfoList = new ArrayList<FlexiInfo>();
				for (FlexiInfoTO flexiInfoTO : externalChargeTO.getAdditionalDetails()) {
					FlexiInfo flexiInfo = new FlexiInfo();
					flexiInfo.setFlexiRateId(flexiInfoTO.getFlexiRateId());
					flexiInfo.setAvailableCount(flexiInfoTO.getAvailableCount());
					flexiInfo.setCutOverBufferInMins(flexiInfoTO.getCutOverBufferInMins());
					flexiInfo.setFlexibilityTypeId(flexiInfoTO.getFlexibilityTypeId());
					flexiInfoList.add(flexiInfo);
				}
				externalCharge.getAdditionalDetails().addAll(flexiInfoList);
				externalCharge.setType(AAUtils.getExternalChargeType(externalChargeTO.getType()));
				externalCharges.add(externalCharge);
			}
			ONDExternalCharge ondExternalCharge = new ONDExternalCharge();
			ondExternalCharge.setOndSequence(ondSequence);
			ondExternalCharge.getExternalCharges().addAll(externalCharges);
			ondExternalCharge.setTotalExternalCharges(totalOnDExternalCharges);
			ondExternalCharges.add(ondExternalCharge);
		}
		return ondExternalCharges;
	}

	private static List<BaseFare> populateBaseFares(List<BaseFareTO> baseFareDTOList, Date date) {
		List<BaseFare> baseFares = new ArrayList<BaseFare>();
		if (baseFareDTOList != null) {
			for (BaseFareTO baseFareTO : baseFareDTOList) {
				BaseFare baseFare = new BaseFare();
				baseFare.setAmount(baseFareTO.getAmount());
				baseFare.getCarrierCode().addAll(baseFareTO.getCarrierCode());
				baseFare.setSegmentCode(baseFareTO.getSegmentCode());
				baseFare.getSegmentRPH().addAll(baseFareTO.getSegmentRPHList());
				baseFare.setOndSequence(baseFareTO.getOndSequence());
				baseFare.setApplicableDateTime(date);
				if (baseFareTO.getApplicablePassengerTypeCode() != null) {
					for (String paxCode : baseFareTO.getApplicablePassengerTypeCode()) {
						baseFare.getApplicablePassengerType().add(PaxTypeUtils.convertAAPaxCodeToLCCPaxCodes(paxCode));
					}
				}
				baseFares.add(baseFare);
			}
		}
		return baseFares;
	}

	private static List<Tax> populateTaxes(List<TaxTO> taxDTOList, Date date) {
		List<Tax> taxes = new ArrayList<Tax>();
		if (taxDTOList != null) {
			for (TaxTO taxTO : taxDTOList) {
				Tax tax = new Tax();
				tax.setAmount(taxTO.getAmount());
				tax.setCarrierCode(taxTO.getCarrierCode());
				tax.setSegmentCode(taxTO.getSegmentCode());
				tax.setTaxName(taxTO.getTaxName());
				tax.setTaxCode(taxTO.getTaxCode());
				tax.setOndSequence(taxTO.getOndSequence());
				tax.setApplicableDateTime(date);
				tax.setOndSequence(taxTO.getOndSequence());
				if (taxTO.getApplicablePassengerTypeCode() != null) {
					for (String paxCode : taxTO.getApplicablePassengerTypeCode()) {
						tax.getApplicablePassengerType().add(PaxTypeUtils.convertAAPaxCodeToLCCPaxCodes(paxCode));
					}
				}
				taxes.add(tax);
			}
		}
		return taxes;
	}

	private static List<Surcharge> populateSurcharges(List<SurchargeTO> surchargeDTOList, Date date) {
		List<Surcharge> surcharges = new ArrayList<Surcharge>();
		if (surchargeDTOList != null) {
			for (SurchargeTO surchargeTO : surchargeDTOList) {
				Surcharge surcharge = new Surcharge();
				surcharge.setAmount(surchargeTO.getAmount());
				surcharge.setCarrierCode(surchargeTO.getCarrierCode());
				surcharge.setSegmentCode(surchargeTO.getSegmentCode());
				surcharge.setSurchargeCode(surchargeTO.getSurchargeCode());
				surcharge.setApplicableDateTime(date);
				surcharge.setOndSequence(surchargeTO.getOndSequence());

				if (surchargeTO.getApplicablePassengerTypeCode() != null) {
					for (String paxCode : surchargeTO.getApplicablePassengerTypeCode()) {
						surcharge.getApplicablePassengerType().add(PaxTypeUtils.convertAAPaxCodeToLCCPaxCodes(paxCode));
					}
				}
				surcharges.add(surcharge);
			}
		}
		return surcharges;
	}

	private static List<Fee> populateFees(List<FeeTO> feeDTOList, Date date) {
		List<Fee> fees = new ArrayList<Fee>();
		if (feeDTOList != null) {
			for (FeeTO feeTO : feeDTOList) {
				Fee fee = new Fee();
				fee.setAmount(feeTO.getAmount());
				fee.setCarrierCode(feeTO.getCarrierCode());
				fee.setSegmentCode(feeTO.getSegmentCode());
				fee.setFeeCode(feeTO.getFeeCode());
				fee.setApplicableDateTime(date);
				if (feeTO.getApplicablePassengerTypeCode() != null) {
					for (String paxCode : feeTO.getApplicablePassengerTypeCode()) {
						fee.getApplicablePassengerType().add(PaxTypeUtils.convertAAPaxCodeToLCCPaxCodes(paxCode));
					}
				}
				fees.add(fee);
			}
		}
		return fees;
	}

	private static List<FareRule> populateFareRuleList(List<FareRuleDTO> fareRuleDTOs) {
		List<FareRule> fareRules = new ArrayList<FareRule>();
		if (fareRuleDTOs != null) {
			for (FareRuleDTO fareRuleDTO : fareRuleDTOs) {
				FareRule fareRule = new FareRule();
				fareRule.setComments(fareRuleDTO.getComments());
				fareRule.setSegmentCode(fareRuleDTO.getOrignNDest());
				fareRule.setDescription(fareRuleDTO.getDescription());
				fareRule.setFareRuleCode(fareRuleDTO.getFareRuleCode());
				fareRule.setFareBasisCode(fareRuleDTO.getFareBasisCode());
				fareRule.setFareCategoryCode(fareRuleDTO.getFareCategoryCode());
				fareRule.setCabinClassCode(fareRuleDTO.getCabinClassCode());
				fareRule.setBookingClassCode(fareRuleDTO.getBookingClassCode());
				fareRule.setFareRuleId(fareRuleDTO.getFareRuleId());
				fareRule.setInterlineSegmentCode(fareRuleDTO.getOrignNDest());
				fareRule.setOperatingCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
				fareRule.setOndSequence(fareRuleDTO.getOndSequence());

				FareCharacteristics adFareCharacteristics = new FareCharacteristics();
				adFareCharacteristics.setFareAmount(fareRuleDTO.getAdultFareAmount());
				adFareCharacteristics.setApplicable(fareRuleDTO.isAdultFareApplicable());
				adFareCharacteristics.setRefundable(fareRuleDTO.isAdultFareRefundable());

				fareRule.setAdultFareCharacteristics(adFareCharacteristics);

				FareCharacteristics chFareCharacteristics = new FareCharacteristics();
				chFareCharacteristics.setFareType(fareRuleDTO.getChildFareType());
				chFareCharacteristics.setFareAmount(fareRuleDTO.getChildFareAmount());
				chFareCharacteristics.setApplicable(fareRuleDTO.isChildFareApplicable());
				chFareCharacteristics.setRefundable(fareRuleDTO.isChildFareRefundable());

				fareRule.setChildFareCharacteristics(chFareCharacteristics);

				FareCharacteristics inFareCharacteristics = new FareCharacteristics();
				inFareCharacteristics.setFareType(fareRuleDTO.getInfantFareType());
				inFareCharacteristics.setFareAmount(fareRuleDTO.getInfantFareAmount());
				inFareCharacteristics.setApplicable(fareRuleDTO.isInfantFareApplicable());
				inFareCharacteristics.setRefundable(fareRuleDTO.isInfantFareRefundable());

				fareRule.setInfantFareCharacteristics(inFareCharacteristics);

				fareRules.add(fareRule);
			}
		}
		return fareRules;
	}

	static List<OriginDestinationInformation> populateOndInfoList(Collection<OriginDestinationInformationTO> ondListTO) {
		List<OriginDestinationInformation> ondList = new ArrayList<OriginDestinationInformation>();
		if (ondListTO != null) {
			for (OriginDestinationInformationTO odInfoTo : ondListTO) {
				OriginDestinationInformation odInfo = new OriginDestinationInformation();
				odInfo.setOriginLocation(odInfoTo.getOrigin());
				odInfo.setDestinationLocation(odInfoTo.getDestination());
				odInfo.setDepartureDateTimeStart(odInfoTo.getDepartureDateTimeStart());
				odInfo.setDepartureDateTimeEnd(odInfoTo.getDepartureDateTimeEnd());
				odInfo.setPreferredDate(odInfoTo.getPreferredDate());
				odInfo.setReturnFlag(odInfoTo.isReturnFlag());
				odInfo.setOriginDestinationOptions(populateOriginDestinationOptions(odInfoTo.getOrignDestinationOptions()));
				ondList.add(odInfo);
			}
		}

		return ondList;
	}

	private static OriginDestinationOptions populateOriginDestinationOptions(List<OriginDestinationOptionTO> ondInfoDTOList) {
		OriginDestinationOptions ondOptions = new OriginDestinationOptions();

		if (ondInfoDTOList != null) {
			for (OriginDestinationOptionTO ondTO : ondInfoDTOList) {
				OriginDestinationOption ondOption = new OriginDestinationOption();

				if (ondTO.getFlightSegmentList() != null) {
					for (FlightSegmentTO fSegTo : ondTO.getFlightSegmentList()) {
						ondOption.getFlightSegment().add(populateFlightSegmentFromTO(fSegTo));
					}
				}
				ondOption.setSelectedOption(ondTO.isSelected());
				ondOption.setSystemType(AAUtils.transformSystem(ondTO.getSystem()));
				ondOption.setSeatAvailable(ondTO.isSeatAvailable());

				// FIXME:RW refactor this
				// if (ondTO.getFlightFareSummaryTO() != null) {
				// ondOption.setAdultCharges(ondTO.getFlightFareSummaryTO().getBaseFareAmount());
				// ondOption.setSeatAvailable(ondTO.getFlightFareSummaryTO().isSeatAvailable());
				// if (ondTO.getFlightFareSummaryTO().getTotalPrice()
				// .compareTo(ondTO.getFlightFareSummaryTO().getBaseFareAmount()) == 1) {
				// ondOption.setAdultFare(ondTO.getFlightFareSummaryTO().getTotalPrice()
				// .subtract(ondTO.getFlightFareSummaryTO().getBaseFareAmount()));
				// } else {
				// ondOption.setAdultFare(ondTO.getFlightFareSummaryTO().getBaseFareAmount());
				// }
				// }

				ondOptions.getOriginDestinationOption().add(ondOption);
			}
		}

		return ondOptions;
	}

	/*
	 * ############################### Common methods used in util ####################################
	 */

	// Convert FareSummary to FareSummaryDTO
	public static FareSummaryDTO populateFareSummaryDTO(FareSummary fareSummary) {
		FareSummaryDTO fareSummaryDTO = new FareSummaryDTO();
		fareSummaryDTO.setFareId(fareSummary.getFareId());
		fareSummaryDTO.setAdultFare((fareSummary.getFareAmount() != null) ? fareSummary.getFareAmount().doubleValue() : 0);
		fareSummaryDTO.setFarePercentage(fareSummary.getFarePercentage());
		fareSummaryDTO.setBookingClassCode(StringUtil.getNotNullString(fareSummary.getBookingClassCode()));
		fareSummaryDTO.setFareRuleID(fareSummary.getFareRuleID());
		fareSummaryDTO.setFareCategoryCode(StringUtil.getNotNullString(fareSummary.getFareCategoryCode()));
		fareSummaryDTO.setFareRuleCode(StringUtil.getNotNullString(fareSummary.getFareRuleCode()));
		fareSummaryDTO.setFareBasisCode(StringUtil.getNotNullString(fareSummary.getFareBasisCode()));
		fareSummaryDTO.setFareRuleComment(StringUtil.getNotNullString(fareSummary.getFareRuleComment()));
		fareSummaryDTO.setFareCommonAllSegments(fareSummary.isFareCommonAllSegments());
		fareSummaryDTO.setChildFareType(StringUtil.getNotNullString(fareSummary.getChildFareType()));
		fareSummaryDTO.setChildFare((fareSummary.getChildFare() != null) ? fareSummary.getChildFare().doubleValue() : 0);
		fareSummaryDTO.setInfantFareType(StringUtil.getNotNullString(fareSummary.getInfantFareType()));
		fareSummaryDTO.setInfantFare((fareSummary.getInfantFare() != null) ? fareSummary.getInfantFare().doubleValue() : 0);
		fareSummaryDTO.getVisibleChannels().addAll(fareSummary.getVisibleChannels());
		fareSummaryDTO.getVisibleChannelNames().addAll(fareSummary.getVisibleChannelNames());
		fareSummaryDTO.getVisibleAgents().addAll(fareSummary.getVisibleAgents());
		fareSummaryDTO.setAdultApplicability(fareSummary.isAdultApplicability());
		fareSummaryDTO.setChildApplicability(fareSummary.isChildApplicability());
		fareSummaryDTO.setInfantApplicability(fareSummary.isInfantApplicability());
		fareSummaryDTO.setAdultFareRefundable(fareSummary.isAdultFareRefundable());
		fareSummaryDTO.setChildFareRefundable(fareSummary.isChildFareRefundable());
		fareSummaryDTO.setInfantFareRefundable(fareSummary.isInfantFareRefundable());
		fareSummaryDTO.setOndCode(fareSummary.getOndCode());

		return fareSummaryDTO;
	}

	public static List<SeatDistribution> getSeatDustributionList(
			List<com.isa.connectivity.profiles.maxico.v1.common.dto.SeatDistribution> seatDistList) {
		List<SeatDistribution> seatDisList = new ArrayList<SeatDistribution>();
		for (com.isa.connectivity.profiles.maxico.v1.common.dto.SeatDistribution seatDist : seatDistList) {
			SeatDistribution seatDistDTO = new SeatDistribution();
			seatDistDTO.setBookingClassCode(seatDist.getBookingClassCode());
			seatDistDTO.setNoOfSeats(seatDist.getNoOfSeats());
			seatDistDTO.setNestedSeats(seatDist.isNestedSeats());
			seatDistDTO.setBookingClassType(seatDist.getBookingClassType());
			seatDistDTO.setOnholdRestricted(seatDist.isOnholdRestricted());
			seatDisList.add(seatDistDTO);
		}
		return seatDisList;
	}

	private static FlightSegment populateFlightSegmentFromTO(FlightSegmentTO fSegTO) {
		FlightSegment fSeg = new FlightSegment();
		fSeg.setSegmentCode(fSegTO.getSegmentCode());
		fSeg.setDepatureDateTime(fSegTO.getDepartureDateTime());
		fSeg.setDepatureDateTimeZulu(fSegTO.getDepartureDateTimeZulu());
		fSeg.setArrivalDateTime(fSegTO.getArrivalDateTime());
		fSeg.setArrivalDateTimeZulu(fSegTO.getArrivalDateTimeZulu());
		fSeg.setFlightNumber(fSegTO.getFlightNumber());
		fSeg.setOperatingAirline(fSegTO.getOperatingAirline());
		fSeg.setOperationType(fSegTO.getOperationType());
		fSeg.setFlightRefNumber(fSegTO.getFlightRefNumber());
		fSeg.setFlightId(new Integer(fSegTO.getFlightId()));
		fSeg.setCabinClassCode(fSegTO.getCabinClassCode());
		fSeg.setSubJourneyGroup(fSegTO.getSubJourneyGroup());

		AirTravelerAvail airTravelerAvail = new AirTravelerAvail();

		PassengerTypeQuantity adPax = new PassengerTypeQuantity();
		adPax.setPassengerType(PaxTypeUtils.convertAAPaxCodeToLCCPaxCodes(PaxTypeTO.ADULT));
		adPax.setQuantity(fSegTO.getAdultCount());

		PassengerTypeQuantity chPax = new PassengerTypeQuantity();
		chPax.setPassengerType(PaxTypeUtils.convertAAPaxCodeToLCCPaxCodes(PaxTypeTO.CHILD));
		chPax.setQuantity(fSegTO.getChildCount());

		PassengerTypeQuantity inPax = new PassengerTypeQuantity();
		inPax.setPassengerType(PaxTypeUtils.convertAAPaxCodeToLCCPaxCodes(PaxTypeTO.INFANT));
		inPax.setQuantity(fSegTO.getInfantCount());

		airTravelerAvail.getPassengerTypeQuantity().add(adPax);
		airTravelerAvail.getPassengerTypeQuantity().add(chPax);
		airTravelerAvail.getPassengerTypeQuantity().add(inPax);
		fSeg.setAirTravelerAvail(airTravelerAvail);

		for (String logicalCC : fSegTO.getAvailableAdultCountMap().keySet()) {
			LogicalCabinSeatAvailability logicCCSeatAvail = new LogicalCabinSeatAvailability();
			logicCCSeatAvail.setLogicalCabinClass(logicalCC);
			PassengerTypeQuantity paxQty = new PassengerTypeQuantity();
			paxQty.setPassengerType(PaxTypeUtils.convertAAPaxCodeToLCCPaxCodes(PaxTypeTO.ADULT));
			paxQty.setQuantity(fSegTO.getAvailableAdultCountMap().get(logicalCC));
			logicCCSeatAvail.getPassengerTypeQuantity().add(paxQty);

			if (fSegTO.getAvailableInfantCountMap().containsKey(logicalCC)) {
				paxQty = new PassengerTypeQuantity();
				paxQty.setPassengerType(PaxTypeUtils.convertAAPaxCodeToLCCPaxCodes(PaxTypeTO.INFANT));
				paxQty.setQuantity(fSegTO.getAvailableInfantCountMap().get(logicalCC));
			}
			fSeg.getLogicalCabinWiseSeatAvailability().add(logicCCSeatAvail);
		}
		fSeg.setSubStationShortName(fSegTO.getSubStationShortName());
		fSeg.setSeatAvailCount(fSegTO.getSeatAvailCount());
		return fSeg;
	}

	public static List<FlightSegmentTO> tranformSegments(List<FlightSegment> inboundFlightSegments) {
		if (inboundFlightSegments == null) {
			return null;
		}

		List<FlightSegmentTO> fltSegList = new ArrayList<FlightSegmentTO>();
		for (FlightSegment fltSeg : inboundFlightSegments) {
			FlightSegmentTO fltSegTO = new FlightSegmentTO();
			fltSegTO.setSegmentCode(fltSeg.getSegmentCode());
			fltSegTO.setFlightNumber(fltSeg.getFlightNumber());
			fltSegTO.setDepartureDateTime(fltSeg.getDepatureDateTime());
			fltSegTO.setArrivalDateTime(fltSeg.getArrivalDateTime());
			fltSegTO.setDepartureDateTimeZulu(fltSegTO.getDepartureDateTimeZulu());
			fltSegTO.setArrivalDateTimeZulu(fltSeg.getArrivalDateTimeZulu());
			fltSegTO.setFlightId(fltSeg.getFlightId());
			fltSegTO.setFlightRefNumber(fltSeg.getFlightRefNumber());
			fltSegList.add(fltSegTO);
		}
		return fltSegList;
	}

	private static OldFareDetails getOwnOldPerPaxFareTO(List<OldFareDetails> oldFareDetails, Integer flightSegId, String segCode) {
		if (oldFareDetails != null) {
			String defaultCarrier = AppSysParamsUtil.getDefaultCarrierCode();
			String busCarrier = null;
			try {
				busCarrier = AirproxyModuleUtils.getFlightBD().getDefaultBusCarrierCode();
			} catch (Exception e) {
				// log errors
			}
			for (OldFareDetails oldFareTO : oldFareDetails) {
				if (oldFareTO != null
						&& (defaultCarrier.equals(oldFareTO.getCarrierCode()) || (busCarrier != null && busCarrier
								.equals(oldFareTO.getCarrierCode())))) {
					if ((flightSegId != null && flightSegId.equals(oldFareTO.getFlightSegId()))
							|| (segCode != null && segCode.equals(oldFareTO.getOndCode()))
							|| (flightSegId == null && segCode == null)) {
						return oldFareTO;
					}
				}
			}
		}
		return null;
	}
	/*
	 * ############################ End of Common methods Used in Util #############################
	 */
}
