package com.isa.thinair.aaservices.core.util;

import com.isa.connectivity.profiles.maxico.v1.common.dto.PerPaxPriceInfo;

public class CloneUtil {

	public static PerPaxPriceInfo lightClone(PerPaxPriceInfo perPaxPriceInfo) {
		PerPaxPriceInfo clone = new PerPaxPriceInfo();
		clone.setPassengerPrice(perPaxPriceInfo.getPassengerPrice());
		clone.setPassengerType(perPaxPriceInfo.getPassengerType());
		clone.setTravelerRefNumber(perPaxPriceInfo.getTravelerRefNumber());
		clone.getFareBasisCodes().addAll(perPaxPriceInfo.getFareBasisCodes());
		return clone;
	}
}
