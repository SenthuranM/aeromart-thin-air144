package com.isa.thinair.aaservices.core.transaction;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.isa.thinair.aaservices.core.session.AASessionManager;

public class AATxnCleaningQuartzJob implements Job {

	private static Log log = LogFactory.getLog(AATxnCleaningQuartzJob.class);

	public AATxnCleaningQuartzJob() {
	}

	public void execute(JobExecutionContext context) throws JobExecutionException {

		String jobName = context.getJobDetail().getKey().getName();

		log.info("AATxnCleaningQuartzJob: " + jobName + " execution start at " + new Date());

		AASessionManager.getInstance().removeExpiredUserTransactions();

		log.info("AATxnCleaningQuartzJob: " + jobName + " execution end at " + new Date());
	}
}