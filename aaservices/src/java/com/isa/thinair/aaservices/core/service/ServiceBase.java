package com.isa.thinair.aaservices.core.service;

import com.isa.thinair.aaservices.core.session.AASessionManager;

/**
 * @author Nilindra Fernando
 */
class ServiceBase {

	// @Resource
	// private WebServiceContext wsContext;
	//
	//
	// public HttpSession getSession() {
	// MessageContext mc = wsContext.getMessageContext();
	// HttpSession session = ((HttpServletRequest) mc.get(MessageContext.SERVLET_REQUEST)).getSession();
	// return session;
	// }

	public AASessionManager getAASession() {
		return AASessionManager.getInstance();
	}

}
