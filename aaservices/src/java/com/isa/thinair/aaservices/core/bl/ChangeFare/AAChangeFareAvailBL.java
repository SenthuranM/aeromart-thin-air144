package com.isa.thinair.aaservices.core.bl.ChangeFare;

import java.math.BigInteger;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.BCStatus;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BCType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FareRuleFare;
import com.isa.connectivity.profiles.maxico.v1.common.dto.InvFareAlloc;
import com.isa.connectivity.profiles.maxico.v1.common.dto.InvFareType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SegmentInvFare;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAFareAvailRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAFareAvailRS;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.aaservices.core.session.AASessionManager;
import com.isa.thinair.aaservices.core.session.AAUserSession;
import com.isa.thinair.aaservices.core.util.AAReservationUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.airinventory.api.dto.seatavailability.AllFaresDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSearchDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareFilteringCriteria;
import com.isa.thinair.airinventory.api.dto.seatavailability.OriginDestinationInfoDTO;
import com.isa.thinair.airinventory.api.util.FareQuoteUtil;
import com.isa.thinair.airproxy.api.model.reservation.availability.FareAvailRQ;
import com.isa.thinair.airproxy.api.model.reservation.availability.FareAvailRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareRuleFareDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.InvFareAllocTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.InvFareTypeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationOptionTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.SegmentInvFareTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.PrivilegesKeys.MakeResPrivilegesKeys;
import com.isa.thinair.airproxy.api.utils.converters.AuxillaryHelper;
import com.isa.thinair.airproxy.api.utils.converters.AvailabilityConvertUtil;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;

/**
 * 
 * @author rumesh
 * 
 */

public class AAChangeFareAvailBL {
	private static Log log = LogFactory.getLog(AAChangeFareAvailBL.class);

	private AAFareAvailRQ aaFareAvailRQ;

	public void setRequestParams(AAFareAvailRQ aaChangeFareAvailRQ) {
		if ((aaChangeFareAvailRQ.getOutboundFlightSegments() == null
				|| aaChangeFareAvailRQ.getOutboundFlightSegments().size() == 0)
				&& (aaChangeFareAvailRQ.getInboundFlightSegments() != null
						&& aaChangeFareAvailRQ.getInboundFlightSegments().size() > 0)) {
			aaChangeFareAvailRQ.getOutboundFlightSegments().addAll(aaChangeFareAvailRQ.getInboundFlightSegments());
			aaChangeFareAvailRQ.getInboundFlightSegments().clear();

			String selOBFltSeg = aaChangeFareAvailRQ.getSelectedOBFlightRefNumber();
			String selIBFltSeg = aaChangeFareAvailRQ.getSelectedIBFlightRefNumber();

			if ((selOBFltSeg == null || selOBFltSeg.equals("")) && (selIBFltSeg != null || !selIBFltSeg.equals(""))) {
				aaChangeFareAvailRQ.setSelectedOBFlightRefNumber(selIBFltSeg);
				aaChangeFareAvailRQ.setSelectedIBFlightRefNumber(null);
			}
		}
		AuxillaryHelper auxHelper = new AuxillaryHelper(AAServicesModuleUtils.getReservationAuxillaryBD());
		Collection privilegeKeys = (Collection) AASessionManager.getInstance()
				.getCurrUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);
		UserPrincipal userPrincipal = AASessionManager.getInstance().getCurrentUserPrincipal();
		String travelAgent = userPrincipal.getAgentCode();

		if (isAuhtorized(privilegeKeys, MakeResPrivilegesKeys.PRIVI_OVERRIDE_AGENT_FARE_ONLY)) {
			aaChangeFareAvailRQ.getAvailPreferences().setTravelAgentCode(travelAgent);
			aaChangeFareAvailRQ.setExcludeNonLowestPublicFares(true);
			aaChangeFareAvailRQ
					.setSalesChannelCode(SalesChannelsUtil.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_TRAVELAGNET_KEY));
		}
		if (isAuhtorized(privilegeKeys, MakeResPrivilegesKeys.PRIVI_OVERRIDE_VISIBLE_FARE)) {
			aaChangeFareAvailRQ.getAvailPreferences().setTravelAgentCode(travelAgent);
			aaChangeFareAvailRQ.setExcludeNonLowestPublicFares(false);
			aaChangeFareAvailRQ
					.setSalesChannelCode(SalesChannelsUtil.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_TRAVELAGNET_KEY));
		}
		if (isAuhtorized(privilegeKeys, MakeResPrivilegesKeys.PRIVI_NAME_CHANGE_FARE_ANY)) {
			aaChangeFareAvailRQ.getAvailPreferences().setTravelAgentCode(null);
			aaChangeFareAvailRQ.setSalesChannelCode(-1);
			aaChangeFareAvailRQ.setExcludeNonLowestPublicFares(false);
		} else {
			if (aaChangeFareAvailRQ.getTravelAgentCode() != null && !aaChangeFareAvailRQ.getTravelAgentCode().equals("")) {
				aaChangeFareAvailRQ.getAvailPreferences().setTravelAgentCode(aaChangeFareAvailRQ.getTravelAgentCode());
			}
		}

		this.aaFareAvailRQ = aaChangeFareAvailRQ;
	}

	public AAFareAvailRS execute() {

		AAFareAvailRS aaFareAvailRS = new AAFareAvailRS();
		try {
			FareAvailRQ fareAvailRQ = new FareAvailRQ(AAChangeFareUtils.populateBaseAvailRQ(aaFareAvailRQ, "allfares"));
			populateAdditionalFareInfo(fareAvailRQ, aaFareAvailRQ);
			AuxillaryHelper auxHelper = new AuxillaryHelper(AAServicesModuleUtils.getReservationAuxillaryBD());
			UserPrincipal userPrincipal = AAChangeFareUtils.populateUserPrincipal(aaFareAvailRQ.getAaPos());
			AvailableFlightSearchDTO availableFlightSearchDTO = AvailabilityConvertUtil.getAvailableFlightSearchDTO(fareAvailRQ,
					userPrincipal, true, auxHelper);
			// NOT enabled cause it will show all the channel fares which might not be desirable for dry
			// availableFlightSearchDTO.setChannelCode(fareAvailRQ.getSalesChannelCode());

			if (AppSysParamsUtil.isSubJourneyDetectionEnabled()
					&& availableFlightSearchDTO.getOriginDestinationInfoDTOs().size() > 2
					&& (fareAvailRQ.getSelectedIBFltRefNo() != null || fareAvailRQ.getSelectedOBFltRefNo() != null)) {

				List<AvailableFlightSearchDTO> availableFlightSearchDTOList = ReservationApiUtils
						.getAvailableSubJourneyCollection(availableFlightSearchDTO);

				if (availableFlightSearchDTOList != null && availableFlightSearchDTOList.size() > 1) {
					for (AvailableFlightSearchDTO availFlightSearchDTO : availableFlightSearchDTOList) {
						for (OriginDestinationInfoDTO ondInfoDTO : availFlightSearchDTO.getOriginDestinationInfoDTOs()) {
							if (ondInfoDTO.getFlightSegmentIds()
									.contains(FlightRefNumberUtil.getSegmentIdFromFlightRPH(fareAvailRQ.getSelectedIBFltRefNo()))
									|| ondInfoDTO.getFlightSegmentIds().contains(
											FlightRefNumberUtil.getSegmentIdFromFlightRPH(fareAvailRQ.getSelectedOBFltRefNo()))) {
								availableFlightSearchDTO = availFlightSearchDTO;
								break;
							}
						}
					}

					// only flights associated with the selected flights journey will be considered
					if (availableFlightSearchDTO != null && availableFlightSearchDTO.getOriginDestinationInfoDTOs().size() >= 1) {

						Iterator<OriginDestinationInformationTO> ondItr = fareAvailRQ.getOriginDestinationInformationList()
								.iterator();
						while (ondItr.hasNext()) {
							OriginDestinationInformationTO ondInfo = ondItr.next();
							boolean isSameGroup = false;
							for (OriginDestinationInfoDTO ondInfoDTO : availableFlightSearchDTO.getOriginDestinationInfoDTOs()) {
								if (ondInfo.getOrignDestinationOptions() != null) {
									for (OriginDestinationOptionTO ondOptTO : ondInfo.getOrignDestinationOptions()) {
										if (ondOptTO.getFlightSegmentList() != null) {
											for (FlightSegmentTO fltSegTO : ondOptTO.getFlightSegmentList()) {
												if (ondInfoDTO.getFlightSegmentIds().contains(FlightRefNumberUtil
														.getSegmentIdFromFlightRPH(fltSegTO.getFlightRefNumber()))) {
													isSameGroup = true;
													break;
												}
											}
										}
									}
								}
							}

							if (!isSameGroup) {
								ondItr.remove();
							}
						}
					}
				}

			}

			availableFlightSearchDTO.setSkipInvCheckForFlown(AppSysParamsUtil.skipInvChkForFlownInRequote());
			FareQuoteUtil.setHalfReturnFareQuoteOption(availableFlightSearchDTO,
					(fareAvailRQ.getAvailPreferences().isModifyBooking()
							|| fareAvailRQ.getAvailPreferences().isRequoteFlightSearch()));
			AllFaresDTO allFaresDTO = AvailabilityConvertUtil.getAllFaresDTO(fareAvailRQ, AAServicesModuleUtils.getFlightBD());
			FareFilteringCriteria filteringCriteria = AvailabilityConvertUtil.getFareFilteringCriteria(fareAvailRQ,
					userPrincipal);
			allFaresDTO = AAServicesModuleUtils.getFlightInventoryResBD().getAllBucketFareCombinationsForSegment(allFaresDTO,
					filteringCriteria, availableFlightSearchDTO);

			FareAvailRS fareAvailRS = AvailabilityConvertUtil.getFareAvailRS(allFaresDTO, availableFlightSearchDTO);
			populateFares(aaFareAvailRS, fareAvailRS);
			// aaChangeFareAvailRS.setAllFares(AAChangeFareUtils.populateAllFares(allFaresDTO));
		} catch (ModuleException e) {
			log.error("ERROR in AAChangeFareAvailBL: ", e);
		}

		return aaFareAvailRS;
	}

	private void populateAdditionalFareInfo(FareAvailRQ fareAvailRQ, AAFareAvailRQ aaFareAvailRQ2) {
		fareAvailRQ.setExcludeNonLowestPublicFares(aaFareAvailRQ2.isExcludeNonLowestPublicFares());
		fareAvailRQ.setExcludeOnewayPublicFaresFromReturnJourneys(aaFareAvailRQ2.isExcludeOnewayPublicFaresFromReturnJourneys());
		fareAvailRQ.setInboundFlightSegments(AAChangeFareUtils.tranformSegments(aaFareAvailRQ2.getInboundFlightSegments()));
		fareAvailRQ.setOutboundFlightSegments(AAChangeFareUtils.tranformSegments(aaFareAvailRQ2.getOutboundFlightSegments()));
		fareAvailRQ.setSalesChannelCode(aaFareAvailRQ2.getSalesChannelCode());
		fareAvailRQ.setSelectedIBFltRefNo(aaFareAvailRQ2.getSelectedIBFlightRefNumber());
		fareAvailRQ.setSelectedOBFltRefNo(aaFareAvailRQ2.getSelectedOBFlightRefNumber());
	}

	private void populateFares(AAFareAvailRS aaFareAvailRS, FareAvailRS fareAvailRS) {
		for (SegmentInvFareTO segInvFareTO : fareAvailRS.getSegmentInvFares()) {
			SegmentInvFare segmentInvFare = new SegmentInvFare();
			segmentInvFare.setFlightRefNumber(segInvFareTO.getFlightRefNumber());

			for (InvFareTypeTO invFareTypeTO : segInvFareTO.getInvetoryFareTypes()) {
				InvFareType invFareType = new InvFareType();
				invFareType.setFareOndCode(invFareTypeTO.getFareOndCode());
				invFareType.setFareType(invFareTypeTO.getFareType());
				for (InvFareAllocTO resInvFareAlloc : invFareTypeTO.getInvFareAllocations()) {
					InvFareAlloc lccInvFareAlloc = new InvFareAlloc();
					lccInvFareAlloc.setAllocatedSeats(resInvFareAlloc.getAllocatedSeats());
					lccInvFareAlloc.setAvailableMaxSeats(resInvFareAlloc.getAvailableMaxSeats());
					lccInvFareAlloc.setAvailableSeats(resInvFareAlloc.getAvailableSeats());
					lccInvFareAlloc.setBcInvStatus(BCStatus.valueOf(resInvFareAlloc.getBcInvStatus())); // TODO verify
					lccInvFareAlloc.setBcType(BCType.valueOf(resInvFareAlloc.getBcType().toString())); // TODO verify
					lccInvFareAlloc.setBookingClass(resInvFareAlloc.getBookingCode());
					lccInvFareAlloc.setCabinClass(resInvFareAlloc.getCcCode());
					lccInvFareAlloc.setLogicalCabinClass(resInvFareAlloc.getLogicalCabinClass());
					lccInvFareAlloc.setNestRank(
							resInvFareAlloc.getNestRank() == null ? null : BigInteger.valueOf(resInvFareAlloc.getNestRank()));
					lccInvFareAlloc.setOnHoldRestricted("Y".equals(resInvFareAlloc.getOnholdRestricted()));
					lccInvFareAlloc.setOnholdSeats(resInvFareAlloc.getOnholdSeats());
					lccInvFareAlloc.setSoldSeats(resInvFareAlloc.getSoldSeats());
					lccInvFareAlloc.setActualSeatsOnhold(resInvFareAlloc.getActualSeatsOnHold());
					lccInvFareAlloc.setActualsSeatsSold(resInvFareAlloc.getActualSeatsSold());
					lccInvFareAlloc.setAvailableNestedSeats(resInvFareAlloc.getAvailableNestedSeats());
					lccInvFareAlloc.setCommonToAllSegments(resInvFareAlloc.isCommonAllSegments());

					for (FareRuleFareDTO resFareRuleFare : resInvFareAlloc.getFareRuleFares()) {
						FareRuleFare lccFareRuleFare = new FareRuleFare();
						lccFareRuleFare.setAdultFareAmount(resFareRuleFare.getAdultFareAmount());
						lccFareRuleFare.setAdultFareApplicable(resFareRuleFare.isAdultFareApplicable());
						lccFareRuleFare.setBookingClass(resFareRuleFare.getBookingClassCode());
						lccFareRuleFare.setChildFareAmount(resFareRuleFare.getChildFareAmount());
						lccFareRuleFare.setChildFareApplicable(resFareRuleFare.isChildFareApplicable());
						lccFareRuleFare.setFareBasisCode(resFareRuleFare.getFareBasisCode());
						lccFareRuleFare.setFareId(resFareRuleFare.getFareId());
						lccFareRuleFare.setFareRuleCode(resFareRuleFare.getFareRuleCode());
						lccFareRuleFare.setInfantFareAmount(resFareRuleFare.getInfantFareAmount());
						lccFareRuleFare.setInfantFareApplicable(resFareRuleFare.isAdultFareApplicable());
						if (resFareRuleFare.getVisibleAgents() != null) {
							lccFareRuleFare.getVisibleAgents().addAll(resFareRuleFare.getVisibleAgents());
						}
						if (resFareRuleFare.getVisibleChannelNames() != null) {
							lccFareRuleFare.getVisibleChannelNames().addAll(resFareRuleFare.getVisibleChannelNames());
						}
						lccInvFareAlloc.getFareRuleFares().add(lccFareRuleFare);
					}
					invFareType.getInvFareAllocs().add(lccInvFareAlloc);
				}
				segmentInvFare.getInventoryFareTypes().add(invFareType);
			}

			aaFareAvailRS.getSegmentInvFares().add(segmentInvFare);
		}
	}

	private static boolean isAuhtorized(Collection privilegeKeys, String privilige) {
		boolean isAuthorized = false;
		try {
			AAReservationUtil.authorize(privilegeKeys, privilige);
			isAuthorized = true;
		} catch (WebservicesException e) {
			isAuthorized = false;
		}

		return isAuthorized;
	}
}
