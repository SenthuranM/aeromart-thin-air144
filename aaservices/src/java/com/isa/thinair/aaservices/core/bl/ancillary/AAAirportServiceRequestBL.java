package com.isa.thinair.aaservices.core.bl.ancillary;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AirportServiceKey;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AirportServiceRequest;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AirportServiceRequestMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingFlightSegment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentAirportServiceRequests;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SelectedSSR;
import com.isa.connectivity.profiles.maxico.v1.common.dto.StringIntegerMap;
import com.isa.thinair.aaservices.core.transaction.TransactionalReservationProcess;
import com.isa.thinair.aaservices.core.transaction.TransactionalReservationProcessBL;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.AncillaryUtil;
import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airmaster.api.dto.AirportServiceDTO;
import com.isa.thinair.airmaster.api.dto.AirportServiceKeyTO;
import com.isa.thinair.airmaster.api.service.AirmasterUtils;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.utils.AncillaryCommonUtil;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;

/**
 * 
 * @author rumesh
 * 
 */

public class AAAirportServiceRequestBL implements TransactionalReservationProcessBL {
	private static final Log log = LogFactory.getLog(AAAirportServiceRequestBL.class);

	private String appIndicator;
	private int serviceCategory;
	private String selectedLanguage;
	private List<StringIntegerMap> serviceCount;
	private List<BookingFlightSegment> flightSegments;
	private TransactionalReservationProcess transactionalReservationProcess;
	private String pnr;

	public List<AirportServiceRequestMap> execute() throws ModuleException {
		if (log.isDebugEnabled()) {
			log.debug("execute called.");
		}

		List<AirportServiceRequestMap> airportServiceRequestMap = new ArrayList<AirportServiceRequestMap>();

		if (AppSysParamsUtil.isAirportServicesEnabled()) {
			Map<AirportServiceKeyTO, FlightSegmentTO> airportWiseMap = AncillaryCommonUtil.populateAiportWiseFltSegments(
					AncillaryUtil.getFlightSegmentTOs(flightSegments), AncillaryUtil.convertLCCListToMap(serviceCount), false);

			Map<String, Integer> segmentBundledFareServiceInclusion = AncillaryUtil.getSegmentBundledFareServiceAvailability(
					transactionalReservationProcess, pnr, EXTERNAL_CHARGES.AIRPORT_SERVICE.toString());

			Map<AirportServiceKeyTO, List<AirportServiceDTO>> airportWiseResults = AAServicesModuleUtils.getSsrServiceBD()
					.getAvailableAirportServices(airportWiseMap, AncillaryUtil.getAppIndicator(appIndicator), serviceCategory,
							selectedLanguage, segmentBundledFareServiceInclusion, false);

			for (BookingFlightSegment bkFltSeg : flightSegments) {
				for (Entry<AirportServiceKeyTO, List<AirportServiceDTO>> entry : airportWiseResults.entrySet()) {
					AirportServiceKeyTO keyTO = entry.getKey();

					if (entry.getValue().size() > 0) {
						if (bkFltSeg.getSegmentCode().equals(keyTO.getSegmentCode())
								&& bkFltSeg.getDepatureDateTimeZulu().getTime() == keyTO.getDepartureDateTime()) {
							List<AirportServiceRequest> airportServices = new ArrayList<AirportServiceRequest>();

							for (AirportServiceDTO airportServiceDTO : entry.getValue()) {
								airportServices.add(populateAirportServiceRequest(airportServiceDTO));
							}

							FlightSegmentAirportServiceRequests fltSegAPServiceRQ = new FlightSegmentAirportServiceRequests();
							fltSegAPServiceRQ.setFlightSegment(bkFltSeg);
							fltSegAPServiceRQ.getAirportServices().addAll(airportServices);

							AirportServiceRequestMap mapObj = new AirportServiceRequestMap();
							mapObj.setKey(getAirportServiceKey(keyTO));
							mapObj.setValue(fltSegAPServiceRQ);
							airportServiceRequestMap.add(mapObj);
						}
					}
				}
			}
		}

		return airportServiceRequestMap;
	}

	public List<FlightSegmentAirportServiceRequests> getSelectedAirportServices(List<SelectedSSR> selectedAPS,
			boolean skipCutoverValidation) throws ModuleException {
		List<FlightSegmentAirportServiceRequests> fltSegAPSList = new ArrayList<FlightSegmentAirportServiceRequests>();

		Map<Integer, List<String>> airportFltSegIdMap = getAirportFltSegIdMap(selectedAPS);
		Map<String, List<AirportServiceDTO>> selectedAPSMap = AAServicesModuleUtils.getSsrServiceBD()
				.getAvailableAirportServices(airportFltSegIdMap, skipCutoverValidation);

		Map<String, Integer> segmentBundledFareServiceInclusion = AncillaryUtil.getSegmentBundledFareServiceAvailability(
				transactionalReservationProcess, pnr, EXTERNAL_CHARGES.AIRPORT_SERVICE.toString());

		List<BundledFareDTO> bundledFareDTOs = new ArrayList<BundledFareDTO>();
		if (segmentBundledFareServiceInclusion != null && segmentBundledFareServiceInclusion.values() != null
				&& !segmentBundledFareServiceInclusion.values().isEmpty()) {
			bundledFareDTOs = AirmasterUtils.getBundledFareBD().getBundledFareDTOsByBundlePeriodIds(
					new HashSet<Integer>(segmentBundledFareServiceInclusion.values()));
		}

		for (Entry<String, List<AirportServiceDTO>> entry : selectedAPSMap.entrySet()) {
			String[] keyArr = entry.getKey().split("\\" + AirportServiceDTO.SEPERATOR);
			List<AirportServiceDTO> apsDTO = entry.getValue();
			for (AirportServiceDTO airportServiceDTO : apsDTO) {
				for (SelectedSSR selectedAirportService : selectedAPS) {
					if (selectedAirportService.getSsrCode().contains(airportServiceDTO.getSsrCode())
							&& selectedAirportService.getFlightSegment().getFlightRefNumber().equals(keyArr[1])) {

						BookingFlightSegment flightSegment = selectedAirportService.getFlightSegment();

						if (AncillaryUtil.isOverrideServiceCharge(segmentBundledFareServiceInclusion,
								flightSegment.getSegmentCode(), bundledFareDTOs, airportServiceDTO.getSsrID())) {
							airportServiceDTO.setAdultAmount("0");
							airportServiceDTO.setChildAmount("0");
							airportServiceDTO.setInfantAmount("0");
							airportServiceDTO.setReservationAmount("0");
						}

						FlightSegmentAirportServiceRequests fltSegAPS = new FlightSegmentAirportServiceRequests();
						fltSegAPS.setFlightSegment(selectedAirportService.getFlightSegment());
						fltSegAPS.getAirportServices().add(populateAirportServiceRequest(airportServiceDTO));

						fltSegAPSList.add(fltSegAPS);
					}

				}

			}
		}

		return fltSegAPSList;
	}

	private AirportServiceKey getAirportServiceKey(AirportServiceKeyTO airportServiceKeyTO) {
		AirportServiceKey airportServiceKey = new AirportServiceKey();
		airportServiceKey.setAirport(airportServiceKeyTO.getAirport());
		airportServiceKey.setAirportType(airportServiceKeyTO.getAirportType());
		airportServiceKey.setDepartureDateTime(airportServiceKeyTO.getDepartureDateTime());
		airportServiceKey.setSegmentCode(airportServiceKeyTO.getSegmentCode());

		return airportServiceKey;
	}

	private Map<Integer, List<String>> getAirportFltSegIdMap(List<SelectedSSR> selectedAPS) {
		Map<Integer, List<String>> airportFltSegIdMap = new HashMap<Integer, List<String>>();
		for (SelectedSSR selectedAirportService : selectedAPS) {
			BookingFlightSegment fltSeg = selectedAirportService.getFlightSegment();
			Integer fltRef = new Integer(fltSeg.getFlightRefNumber());
			List<String> airportList = null;
			if (airportFltSegIdMap.get(fltRef) != null) {
				airportList = airportFltSegIdMap.get(fltRef);
			} else {
				airportList = new ArrayList<String>();
				airportFltSegIdMap.put(fltRef, airportList);
			}
			airportList.add(fltSeg.getAirportCode());
		}

		return airportFltSegIdMap;
	}

	private AirportServiceRequest populateAirportServiceRequest(AirportServiceDTO airportServiceDTO) {
		AirportServiceRequest airportServiceRequest = new AirportServiceRequest();
		airportServiceRequest.setSsrCode(airportServiceDTO.getSsrCode());
		airportServiceRequest.setSsrName(airportServiceDTO.getSsrName());
		airportServiceRequest.setApplicabilityType(airportServiceDTO.getApplicabilityType());
		airportServiceRequest.setAdultAmount(new BigDecimal(airportServiceDTO.getAdultAmount()));
		airportServiceRequest.setChildAmount(new BigDecimal(airportServiceDTO.getChildAmount()));
		airportServiceRequest.setInfantAmount(new BigDecimal(airportServiceDTO.getInfantAmount()));
		airportServiceRequest.setReservationAmount(new BigDecimal(airportServiceDTO.getReservationAmount()));
		airportServiceRequest.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
		airportServiceRequest.setAirportCode(airportServiceDTO.getAirport());
		airportServiceRequest.setStatus(airportServiceDTO.getStatus());
		airportServiceRequest.setSsrImagePath(airportServiceDTO.getSsrImagePath());
		airportServiceRequest.setSsrThumbnailImagePath(airportServiceDTO.getSsrThumbnailImagePath());

		if (airportServiceDTO.getDecriptionSelectedLanguage() == null
				|| StringUtil.getUnicode(airportServiceDTO.getDecriptionSelectedLanguage()).equalsIgnoreCase("null"))
			airportServiceRequest.setDescription(airportServiceDTO.getDescription());
		else {
			// TODO check whether this can be eliminated
			String ucs = StringUtil.getUnicode(airportServiceDTO.getDecriptionSelectedLanguage());
			ucs = ucs.replace("^", ",");
			airportServiceRequest.setDescription(ucs);
		}

		return airportServiceRequest;
	}

	/**
	 * @return the appIndicator
	 */
	public String getAppIndicator() {
		return appIndicator;
	}

	/**
	 * @param appIndicator
	 *            the appIndicator to set
	 */
	public void setAppIndicator(String appIndicator) {
		this.appIndicator = appIndicator;
	}

	/**
	 * @return the serviceCategory
	 */
	public int getServiceCategory() {
		return serviceCategory;
	}

	/**
	 * @param serviceCategory
	 *            the serviceCategory to set
	 */
	public void setServiceCategory(int serviceCategory) {
		this.serviceCategory = serviceCategory;
	}

	/**
	 * @return the serviceCount
	 */
	public List<StringIntegerMap> getServiceCount() {
		return serviceCount;
	}

	/**
	 * @param serviceCount
	 *            the serviceCount to set
	 */
	public void setServiceCount(List<StringIntegerMap> serviceCount) {
		this.serviceCount = serviceCount;
	}

	/**
	 * @return the flightSegments
	 */
	public List<BookingFlightSegment> getFlightSegments() {
		return flightSegments;
	}

	/**
	 * @param flightSegments
	 *            the flightSegments to set
	 */
	public void setFlightSegments(List<BookingFlightSegment> flightSegments) {
		this.flightSegments = flightSegments;
	}

	/**
	 * @return the selectedLanguage
	 */
	public String getSelectedLanguage() {
		return selectedLanguage;
	}

	/**
	 * @param selectedLanguage
	 *            the selectedLanguage to set
	 */
	public void setSelectedLanguage(String selectedLanguage) {
		this.selectedLanguage = selectedLanguage;
	}

	@Override
	public void setTransactionalReservationProcess(TransactionalReservationProcess transactionalReservationProcess) {
		this.transactionalReservationProcess = transactionalReservationProcess;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}
}
