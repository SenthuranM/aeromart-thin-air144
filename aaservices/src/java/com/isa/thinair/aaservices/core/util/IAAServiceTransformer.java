package com.isa.thinair.aaservices.core.util;

import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * 
 * @author Mehdi Raza
 */
interface IAAServiceTransformer {
	public Object[] transform() throws ModuleException, WebservicesException;
}
