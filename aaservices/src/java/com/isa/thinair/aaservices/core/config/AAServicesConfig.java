/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2009 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * ===============================================================================
 */
package com.isa.thinair.aaservices.core.config;

import com.isa.thinair.platform.api.DefaultModuleConfig;

/**
 * @author Nilindra Fernando
 */
public class AAServicesConfig extends DefaultModuleConfig {

	private long userSessTimeoutInMills;

	private long txnTimeoutInMillis;

	private String dumpMessages;

	private long userSessionCleaningGapInMillis;

	private long txnCleaningGapInMillis;

	public long getUserSessTimeoutInMills() {
		return userSessTimeoutInMills;
	}

	public void setUserSessTimeoutInMills(long userSessTimeoutInMills) {
		this.userSessTimeoutInMills = userSessTimeoutInMills;
	}

	public long getTxnTimeoutInMillis() {
		return txnTimeoutInMillis;
	}

	public void setTxnTimeoutInMillis(long txnTimeoutInMillis) {
		this.txnTimeoutInMillis = txnTimeoutInMillis;
	}

	public String getDumpMessages() {
		return dumpMessages;
	}

	public void setDumpMessages(String dumpMessages) {
		this.dumpMessages = dumpMessages;
	}

	public long getUserSessionCleaningGapInMillis() {
		return userSessionCleaningGapInMillis;
	}

	public void setUserSessionCleaningGapInMillis(long userSessionCleaningGapInMillis) {
		this.userSessionCleaningGapInMillis = userSessionCleaningGapInMillis;
	}

	public long getTxnCleaningGapInMillis() {
		return txnCleaningGapInMillis;
	}

	public void setTxnCleaningGapInMillis(long txnCleaningGapInMillis) {
		this.txnCleaningGapInMillis = txnCleaningGapInMillis;
	}
}
