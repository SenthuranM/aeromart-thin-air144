package com.isa.thinair.aaservices.core.bl.reservation;

import java.util.Collection;

import com.isa.connectivity.profiles.maxico.v1.common.dto.ContactInfo;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAirBookModifyRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAErrorCode;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAModificationTypeCode;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.AAUtils;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.ServiceResponce;

class AAModifyContactInfoBL {

	// Request & Response objects
	private AAAirBookModifyRQ aaAirBookModifyRQ;

	private Reservation reservation;
	private Collection<ReservationAudit> reservationAuditsCol;

	/**
	 * Execute some logic based on the Request object and process and return the Response object
	 * 
	 * @return AAAirBookRS
	 * @throws WebservicesException
	 * @throws ModuleException
	 */
	void execute() throws WebservicesException, ModuleException {
		if (reservation == null) {
			throw new WebservicesException(AAErrorCode.ERR_25_MAXICO_REQUIRED_THE_RESERVATION_NO_LONGER_EXIST,
					"PNR does not exist");
		}

		if (aaAirBookModifyRQ.getModificationTypeCode() != AAModificationTypeCode.MODTYPE_3_MODIFY_RESERVATION_CONTACT_INFO) {
			throw new WebservicesException(AAErrorCode.ERR_11_MAXICO_REQUIRED_INVALID_MODIFICATION_TYPE,
					"Invalid Modification Type [" + aaAirBookModifyRQ.getModificationTypeCode().toString() + "] ");
		}

		ReservationContactInfo reservationContactInfo = reservation.getContactInfo();

		// Update fields in reservation's contact info
		ContactInfo contactInfo = aaAirBookModifyRQ.getAaReservation().getAirReservation().getContactInfo();
		AAUtils.updateReservationContactInfo(contactInfo, reservationContactInfo);

		AAUtils.enforceCarrierConfigForContactInfo(reservationContactInfo, aaAirBookModifyRQ.getAppIndicator());

		// Update reservation contact info
		ServiceResponce updateContactInfo = AAServicesModuleUtils.getReservationBD().updateContactInfo(reservation.getPnr(),
				reservation.getVersion(), reservationContactInfo, null);

		// Normally all the time the status will be success or it will return an exception.
		if (!updateContactInfo.isSuccess()) {
			throw new WebservicesException(AAErrorCode.ERR_13_MAXICO_REQUIRED_UNDETERMINED_CAUSE_PLEASE_REPORT, null);
		}
		this.setReservationAuditsCol((Collection<ReservationAudit>) updateContactInfo
				.getResponseParam(CommandParamNames.RESERVATION_AUDIT_COLLECTION));
	}

	/**
	 * Set the Request object (to be used in the {@link AAModifyContactInfoBL#execute execute} method)
	 * 
	 * @param aaReadRQ
	 */
	public void setRequest(AAAirBookModifyRQ aaAirBookModifyRQ) {
		this.aaAirBookModifyRQ = aaAirBookModifyRQ;
	}

	/**
	 * @param reservation
	 *            the reservation to set
	 */
	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}

	public Collection<ReservationAudit> getReservationAuditsCol() {
		return reservationAuditsCol;
	}

	public void setReservationAuditsCol(Collection<ReservationAudit> reservationAuditsCol) {
		this.reservationAuditsCol = reservationAuditsCol;
	}

}