package com.isa.thinair.aaservices.core.bl.reservation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.connectivity.profiles.maxico.v1.common.dto.PerPaxPriceInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PriceInfo;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAirBookModifyRQ;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.aaservices.core.session.AASessionManager;
import com.isa.thinair.aaservices.core.util.AAReservationUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.PaxTypeUtils;
import com.isa.thinair.airreservation.api.model.IPassenger;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.assembler.PassengerAssembler;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

class AACreditAdjustBL {

	static Map<String, BigDecimal> killCredits(Reservation reservation) throws ModuleException {
		Map<Integer, ReservationPax> creditsPassengerMap = new HashMap<Integer, ReservationPax>();
		for (ReservationPax reservationPax : (Set<ReservationPax>) reservation.getPassengers()) {

			if (!ReservationApiUtils.isInfantType(reservationPax)) {

				// If it's having credit(s) only we have to kill the credits.
				// Otherwise we will be killing functionality
				if (reservationPax.getTotalAvailableBalance().doubleValue() < 0) {
					creditsPassengerMap.put(reservationPax.getPnrPaxId(), reservationPax);
				}
			}
		}

		Map<String, BigDecimal> passengerTransfers = new HashMap<String, BigDecimal>();
		if (creditsPassengerMap.size() > 0) {
			String agentCode = AASessionManager.getInstance().getCurrentUserPrincipal().getAgentCode();
			IPassenger iPassenger = new PassengerAssembler(null);
			BigDecimal totalKillCredits = AccelAeroCalculator.getDefaultBigDecimalZero();

			for (Integer pnrPaxId : creditsPassengerMap.keySet()) {
				ReservationPax reservationPax = creditsPassengerMap.get(pnrPaxId);
				BigDecimal totalPaxBalance = reservationPax.getTotalAvailableBalance().negate();
				passengerTransfers.put(PaxTypeUtils.travelerReference(reservationPax), totalPaxBalance);

				IPayment iPayment = new PaymentAssembler();
				iPayment.addAgentCreditPayment(agentCode, totalPaxBalance, null, null, null, null, null, null, null);
				iPassenger.addPassengerPayments(reservationPax.getPnrPaxId(), iPayment);

				totalKillCredits = AccelAeroCalculator.add(totalKillCredits, totalPaxBalance);
			}

			AAServicesModuleUtils.getPassengerBD().passengerRefund(
					reservation.getPnr(),
					iPassenger,
					"Interline booking refunded for credit transfer [" + totalKillCredits + " "
									+ AppSysParamsUtil.getBaseCurrency() + "] ", true, reservation.getVersion(), null, false, null, null, false);
		}

		return passengerTransfers;
	}

	static void acquireCredits(Reservation reservation, AAAirBookModifyRQ aaAirBookModifyRQ,
			Collection<String> privilegeKeys) throws ModuleException, WebservicesException {
		String agentCode = AASessionManager.getInstance().getCurrentUserPrincipal().getAgentCode();
		PriceInfo priceInfo = aaAirBookModifyRQ.getAaReservation().getAirReservation().getPriceInfo();
		List<PerPaxPriceInfo> perPaxPriceInfoList = priceInfo.getPerPaxPriceInfo();

		Map<Integer, BigDecimal> acquireCreditsMap = new HashMap<Integer, BigDecimal>();

		for (ReservationPax reservationPax : (Set<ReservationPax>) reservation.getPassengers()) {

			if (!ReservationApiUtils.isInfantType(reservationPax)) {
				String paxRefNumber = PaxTypeUtils.travelerReference(reservationPax);

				for (PerPaxPriceInfo perPaxPriceInfo : perPaxPriceInfoList) {
					if (perPaxPriceInfo.getTravelerRefNumber().equals(paxRefNumber)) {
						BigDecimal paymentAmount = perPaxPriceInfo.getPassengerPrice().getTotalTransfer();

						// If we are to acquire only we will be acquiring the credits
						if (paymentAmount != null && paymentAmount.doubleValue() > 0) {
							acquireCreditsMap.put(reservationPax.getPnrPaxId(), paymentAmount);
						}

						break;
					}
				}
			}
		}

		if (acquireCreditsMap.size() > 0) {
			IPassenger passenger = new PassengerAssembler(null);

			for (Integer pnrPaxId : acquireCreditsMap.keySet()) {
				BigDecimal paymentAmount = acquireCreditsMap.get(pnrPaxId);

				PaymentAssembler paxPayment = new PaymentAssembler();
				paxPayment.addAgentCreditPayment(agentCode, paymentAmount, null, null, null, null, null, null, null);

				Collection<Integer> pnrPaxIds = new ArrayList<Integer>();
				pnrPaxIds.add(pnrPaxId);

				AAReservationUtil.authorizePayment(privilegeKeys, paxPayment, agentCode, pnrPaxIds);

				passenger.addPassengerPayments(pnrPaxId, paxPayment);
			}

			AAServicesModuleUtils.getReservationBD().updateReservationForPayment(reservation.getPnr(), passenger, false, true,
					reservation.getVersion(), null, false, true, true, false, false, false, null, false, null);
		}
	}
}
