package com.isa.thinair.aaservices.core.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AncillaryType;
import com.isa.thinair.aaservices.core.session.AASessionManager;
import com.isa.thinair.airproxy.api.model.reservation.core.ModificationParamRQInfo;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.webplatform.api.util.PrivilegesKeys;
import com.isa.thinair.webplatform.api.v2.modify.ModifcationParamTypes;

public class AAModificationParamUtil {

	public static List<String> getModificationParams(ModificationParamRQInfo paramRQInfo, Date nextDepartingSegmentsTime, boolean isInEditMode, int nameChangeCount) {
		if (paramRQInfo != null) {
			if (SalesChannelsUtil.isAppIndicatorWebOrMobile(paramRQInfo.getAppIndicator())) {
				if (paramRQInfo.getIsRegisteredUser()) {
					return getParamsForIBERegUser(nextDepartingSegmentsTime, isInEditMode);
				} else {
					return getParamsForIBEUnRegUser(nextDepartingSegmentsTime, isInEditMode, nameChangeCount);
				}
			} else if (paramRQInfo.getAppIndicator().equals(AppIndicatorEnum.APP_XBE)) {
				// TODO : implement the XBE modification parameters/privileges
				return getParamsForXBEUser();
			}
		}

		return new ArrayList<String>();
	}

	private static List<String> getParamsForIBERegUser(Date nextDepartingSegmentsTime, boolean isInEditMode) {
		List<String> allowedModifications = new ArrayList<String>();

		if (AppSysParamsUtil.isAllowIBECancellationServicesReservation()) {
			allowedModifications.add(ModifcationParamTypes.CANCEL_ALLOW);
		}
		if (AppSysParamsUtil.isAllowModifySegmentDateIBE()) {
			allowedModifications.add(ModifcationParamTypes.ALLOW_MODIFY_DATE);
		}
		if (AppSysParamsUtil.isAllowModifySegmentRouteIBE()) {
			allowedModifications.add(ModifcationParamTypes.ALLOW_MODIFY_ROUTE);
		}
		if (AppSysParamsUtil.isRegisterdUserCanAddServices() && AppSysParamsUtil.isIBEAddSeat()) {
			allowedModifications.add(ModifcationParamTypes.ADD_SEAT);
		}
		//TODO add parameter for enable edit service if needed
		if (AppSysParamsUtil.isRegisterdUserCanAddServices() && AppSysParamsUtil.isIBEEditSeat()) {
			allowedModifications.add(ModifcationParamTypes.EDIT_SEAT);
		}
		if (AppSysParamsUtil.isRegisterdUserCanAddServices() && AppSysParamsUtil.isIBEAddMeal()) {
			allowedModifications.add(ModifcationParamTypes.ADD_MEAL);
		}
		if (AppSysParamsUtil.isRegisterdUserCanAddServices() && AppSysParamsUtil.isIBEEditMeal()) {
			allowedModifications.add(ModifcationParamTypes.EDIT_MEAL);
		}
		if (AppSysParamsUtil.isRegisterdUserCanAddServices() && AppSysParamsUtil.isIBEAddBaggage()) {
			allowedModifications.add(ModifcationParamTypes.ADD_BAGGAGE);
		}
		if (AppSysParamsUtil.isRegisterdUserCanAddServices() && AppSysParamsUtil.isIBEEditBaggage()
				&& AAUtils.isEnableServiceIBE(AncillaryType.BAGGAGE, nextDepartingSegmentsTime, isInEditMode)) {
			allowedModifications.add(ModifcationParamTypes.EDIT_BAGGAGE);
		}
		if (AppSysParamsUtil.isRegisterdUserCanAddServices() && AppSysParamsUtil.isIBEAddInsurance()) {
			allowedModifications.add(ModifcationParamTypes.ADD_INSURANCE);
		}
		if (AppSysParamsUtil.isRegisterdUserCanAddServices() && AppSysParamsUtil.isIBEEditInsurance()) {
			allowedModifications.add(ModifcationParamTypes.EDIT_INSURANCE);
		}
		if (AppSysParamsUtil.isRegisterdUserCanAddServices() && AppSysParamsUtil.isMultipleMealSelectionEnabled()) {
			allowedModifications.add(ModifcationParamTypes.MULTI_MEAL_ENABLED);
		}
		if (AppSysParamsUtil.isRegisterdUserCanAddServices() && AppSysParamsUtil.isIBEAddAirportServices()) {
			allowedModifications.add(ModifcationParamTypes.ADD_AIRPORT_SERVICE);
		}
		if (AppSysParamsUtil.isRegisterdUserCanAddServices() && AppSysParamsUtil.isIBEEditAirportServices()) {
			allowedModifications.add(ModifcationParamTypes.EDIT_AIRPORT_SERVICE);
		}
		if (AppSysParamsUtil.isRegisterdUserCanAddServices() && AppSysParamsUtil.isIBEAddSSR()) {
			allowedModifications.add(ModifcationParamTypes.ADD_SSR);
		}
		if (AppSysParamsUtil.isRegisterdUserCanAddServices() && AppSysParamsUtil.isIBEEditSSR()) {
			allowedModifications.add(ModifcationParamTypes.EDIT_SSR);
		}
		
		if(AppSysParamsUtil.isBlacklistpassengerCheckEnabled()){
			allowedModifications.add(ModifcationParamTypes.ENABLE_BLACKLIST_PAX);
		}

		return allowedModifications;
	}

	private static List<String> getParamsForIBEUnRegUser(Date nextDepartingSegmentsTime, boolean isInEditMode, int nameChangeCount) {
		List<String> allowedModifications = new ArrayList<String>();

		if (AppSysParamsUtil.isAllowIBECancellationServicesReservation()) {
			allowedModifications.add(ModifcationParamTypes.CANCEL_ALLOW);
		}
		if (AppSysParamsUtil.isUnregisterdUserCanMofidyReservation() && AppSysParamsUtil.isAllowModifySegmentDateIBE()) {
			allowedModifications.add(ModifcationParamTypes.ALLOW_MODIFY_DATE);
		}
		if (AppSysParamsUtil.isUnregisterdUserCanMofidyReservation() && AppSysParamsUtil.isAllowModifySegmentRouteIBE()) {
			allowedModifications.add(ModifcationParamTypes.ALLOW_MODIFY_ROUTE);
		}
		if (AppSysParamsUtil.isUnregisterdUserCanAddServices() && AppSysParamsUtil.isIBEAddSeat()) {
			allowedModifications.add(ModifcationParamTypes.ADD_SEAT);
		}
		if (AppSysParamsUtil.isUnregisterdUserCanAddServices() && AppSysParamsUtil.isIBEEditSeat()) {
			allowedModifications.add(ModifcationParamTypes.EDIT_SEAT);
		}
		if (AppSysParamsUtil.isUnregisterdUserCanAddServices() && AppSysParamsUtil.isIBEAddMeal()) {
			allowedModifications.add(ModifcationParamTypes.ADD_MEAL);
		}
		if (AppSysParamsUtil.isUnregisterdUserCanAddServices() && AppSysParamsUtil.isIBEEditMeal()) {
			allowedModifications.add(ModifcationParamTypes.EDIT_MEAL);
		}
		if (AppSysParamsUtil.isUnregisterdUserCanAddServices() && AppSysParamsUtil.isIBEAddBaggage()) {
			allowedModifications.add(ModifcationParamTypes.ADD_BAGGAGE);
		}
		if (AppSysParamsUtil.isUnregisterdUserCanAddServices() && AppSysParamsUtil.isIBEEditBaggage()
				&& AAUtils.isEnableServiceIBE(AncillaryType.BAGGAGE, nextDepartingSegmentsTime, isInEditMode)) {
			allowedModifications.add(ModifcationParamTypes.EDIT_BAGGAGE);
		}
		if (AppSysParamsUtil.isUnregisterdUserCanAddServices() && AppSysParamsUtil.isIBEAddInsurance()) {
			allowedModifications.add(ModifcationParamTypes.ADD_INSURANCE);
		}
		if (AppSysParamsUtil.isUnregisterdUserCanAddServices() && AppSysParamsUtil.isIBEEditInsurance()) {
			allowedModifications.add(ModifcationParamTypes.EDIT_INSURANCE);
		}
		if (AppSysParamsUtil.isUnregisterdUserCanAddServices() && AppSysParamsUtil.isMultipleMealSelectionEnabled()) {
			allowedModifications.add(ModifcationParamTypes.MULTI_MEAL_ENABLED);
		}
		if (AppSysParamsUtil.isUnregisterdUserCanCancelSegment() && AppSysParamsUtil.isAllowIBECancellationServicesReservation()) {
			allowedModifications.add(ModifcationParamTypes.CANCEL_SEGMENT);
		}
		if (AppSysParamsUtil.isUnregisterdUserCanAddServices() && AppSysParamsUtil.isIBEAddAirportServices()) {
			allowedModifications.add(ModifcationParamTypes.ADD_AIRPORT_SERVICE);
		}
		if (AppSysParamsUtil.isUnregisterdUserCanAddServices() && AppSysParamsUtil.isIBEEditAirportServices()) {
			allowedModifications.add(ModifcationParamTypes.EDIT_AIRPORT_SERVICE);
		}
		if (AppSysParamsUtil.isUnregisterdUserCanAddServices() && AppSysParamsUtil.isIBEAddSSR()) {
			allowedModifications.add(ModifcationParamTypes.ADD_SSR);
		}
		if (AppSysParamsUtil.isUnregisterdUserCanAddServices() && AppSysParamsUtil.isIBEEditSSR()) {
			allowedModifications.add(ModifcationParamTypes.EDIT_SSR);
		}
		if (AppSysParamsUtil.isEnableFareRuleLevelNCC(ApplicationEngine.IBE)
				&& nameChangeCount < AppSysParamsUtil.getNameChangeMaxCount(ApplicationEngine.IBE)) {
			allowedModifications.add(ModifcationParamTypes.FARE_RULE_NCC_ENABLED);
		}
		if (AppSysParamsUtil.isEnableFareRuleLevelNCCForFlown(ApplicationEngine.IBE)
				&& nameChangeCount < AppSysParamsUtil.getNameChangeMaxCount(ApplicationEngine.IBE)) {
			allowedModifications.add(ModifcationParamTypes.FARE_RULE_NCC_FOR_FLOWN_ENABLED);
		}
		
		if(AppSysParamsUtil.isBlacklistpassengerCheckEnabled()){
			allowedModifications.add(ModifcationParamTypes.ENABLE_BLACKLIST_PAX);
		}
		return allowedModifications;
	}

	private static List<String> getParamsForXBEUser() {
		List<String> allowedModifications = new ArrayList<String>();

		Collection<String> userPriviliges = AASessionManager.getUserPrivileges();

		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_MODIFY_SEGMENT_DATE)) {
			allowedModifications.add(ModifcationParamTypes.ALLOW_MODIFY_DATE);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_MODIFY_SEGMENT_ROUTE)) {
			allowedModifications.add(ModifcationParamTypes.ALLOW_MODIFY_ROUTE);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.ALT_BUFFERTIME_SEG_OR_PAX)) {
			allowedModifications.add(ModifcationParamTypes.BUFFER_TIME_MODIFICATION);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_CANCEL_SEGMENT)) {
			allowedModifications.add(ModifcationParamTypes.CANCEL_SEGMENT);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_ADD_SEGMENT)) {
			allowedModifications.add(ModifcationParamTypes.ADD_SEGMENT);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_OLD)) {
			allowedModifications.add(ModifcationParamTypes.ALLOW_MODIFY_FLOWN);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_ALLOW_CNX_MODIFY)) {
			allowedModifications.add(ModifcationParamTypes.ALLOW_MODIFY_CNX_RESERVATION);
		}
		if (userPriviliges.contains(PrivilegesKeys.MakeResPrivilegesKeys.RES_TRANSFER)) {
			allowedModifications.add(ModifcationParamTypes.TRANSFER_OWNERSHIP);
		}
		if (userPriviliges.contains(PrivilegesKeys.MakeResPrivilegesKeys.RES_ANYCARRIER_TRANSFER)) {
			allowedModifications.add(ModifcationParamTypes.TRANSFER_OWNERSHIP_TO_ANY_CARRIER);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.RES_ADD_INFANT)) {
			allowedModifications.add(ModifcationParamTypes.ADD_INFANT);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.CANCEL_RES)) {
			allowedModifications.add(ModifcationParamTypes.CANCEL_ALLOW);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_VIEW_CNX_ITEN)) {
			allowedModifications.add(ModifcationParamTypes.VIEW_CNX_ITEN);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.ALT_BUFFERTIME_SEG_OR_PAX)) {
			allowedModifications.add(ModifcationParamTypes.ALLOW_MODFIFY_BUFFERTIME_SEG_OR_PAX);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.RES_BULK_TKT_SPLIT)) {
			allowedModifications.add(ModifcationParamTypes.SPLIT_BULK_RESERVATION);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.RES_SPLIT)) {
			allowedModifications.add(ModifcationParamTypes.SPLIT_RESERVATION);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.RES_EXTEND)) {
			allowedModifications.add(ModifcationParamTypes.EXTEND_ONHOLD);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.RES_REMOVE_PAX)) {
			allowedModifications.add(ModifcationParamTypes.REMOVE_PAX);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.RES_BULK_TKT_REMOVE_PAX)) {
			allowedModifications.add(ModifcationParamTypes.BULK_TKT_REMOVE_PAX);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.MULTIPLE_CHARGE_ADJUSTMENT)) {
			allowedModifications.add(ModifcationParamTypes.MULTIPLE_CHARGE_ADJUSTMENT);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.RES_UPDATE)) {
			allowedModifications.add(ModifcationParamTypes.ADD_USER_NOTES);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.VIEW_ITN_WITH_CHARGES)) {
			allowedModifications.add(ModifcationParamTypes.VIEW_ITN_WITH_CHARGES);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.VIEW_ITN_WITH_PAYMENT_INFO)) {
			allowedModifications.add(ModifcationParamTypes.VIEW_ITN_WITH_PAYMENT_INFO);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.VIEW_ITN_WITHOUT_PAX_CHARGES)) {
			allowedModifications.add(ModifcationParamTypes.VIEW_ITN_WITHOUT_PAX_CHARGES);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_NAME_CHANGE)) {
			allowedModifications.add(ModifcationParamTypes.ALLOW_NAME_CHANGE);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_TBA_NAME_CHANGE)) {
			allowedModifications.add(ModifcationParamTypes.ALLOW_TBA_NAME_CHANGE);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.ALT_BUFFERTIME_NAME)) {
			allowedModifications.add(ModifcationParamTypes.ALLOW_BUFFERTIME_NAME_CHANGE);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.CHANGE_CONTACT_DETAILS)) {
			allowedModifications.add(ModifcationParamTypes.ALLOW_MODIFY_CONTACT_DETAILS);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.VIEW_RES_HISTORY)) {
			allowedModifications.add(ModifcationParamTypes.VIEW_RESERVATION_HISTORY);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.VIEW_RES_HISTORY_OTHERS)) {
			allowedModifications.add(ModifcationParamTypes.VIEW_RESERVATION_HISTORY_OTHER_CARRIERS);
		}
		if (userPriviliges.contains(PrivilegesKeys.MakeResPrivilegesKeys.ADD_USER_NOTE)) {
			allowedModifications.add(ModifcationParamTypes.ADD_USR_NOTE);
		}
		if (userPriviliges.contains(PrivilegesKeys.MakeResPrivilegesKeys.CLASSIFY_USER_NOTE)) {
			allowedModifications.add(ModifcationParamTypes.CLASSIFY_USR_NOTE);
		}
		/* Payment related modification privileges */
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_NO_PAY)) {
			allowedModifications.add(ModifcationParamTypes.PARTIAL_PAYMENT_MODIFY);
		}

		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_OHD_CONF)) {
			allowedModifications.add(ModifcationParamTypes.MODIFY_ONHOLD_CONFIM);
		}
		
		if (userPriviliges.contains(PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_CASH_PAYMNETS)) {
			allowedModifications.add(ModifcationParamTypes.CASH_PAYMENTS);
		}
		if (userPriviliges.contains(PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_CREDIT_CARD_PAYMNETS)) {
			allowedModifications.add(ModifcationParamTypes.CREDIT_CARD_PAYMENTS);
		}
		if (userPriviliges.contains(PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_OFFLINE_PAYMNETS)) {
			allowedModifications.add(ModifcationParamTypes.OFFLINE_PAYMENTS);
		}
		if (userPriviliges.contains(PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_VOUCHER_PAYMNETS)) {
			allowedModifications.add(ModifcationParamTypes.VOUCHER_PAYMENTS);
		}
		if (userPriviliges.contains(PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_ONACCOUNT_PAYMNETS)) {
			allowedModifications.add(ModifcationParamTypes.ON_ACCOUNT_PAYMENTS);
		}

		if (userPriviliges.contains(PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_MULTIPLE_AGENT_ONACCOUNT_PAYMNETS)) {
			allowedModifications.add(ModifcationParamTypes.MULTIPLE_AGENT_ON_ACCOUNT_PAYMENTS);
		}

		if (userPriviliges.contains(PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_BSP_PAYMNETS)) {
			allowedModifications.add(ModifcationParamTypes.BSP_PAYMENTS);
		}
		if (userPriviliges.contains(PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_ONACCOUNT_PAYMNETS_ANY)) {
			allowedModifications.add(ModifcationParamTypes.ON_ACCOUNT_PAYMENTS_ANY);
		}
		if (userPriviliges.contains(PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_ONACCOUNT_PAYMNETS_REPORTING)) {
			allowedModifications.add(ModifcationParamTypes.ON_ACCOUNT_PAYMENTS_REPORTING);
		}
		if (userPriviliges.contains(PrivilegesKeys.MakeResPrivilegesKeys.RES_ACCEPT_NO_PAY)) {
			allowedModifications.add(ModifcationParamTypes.FORCE_CONFIRM);
		}
		if (userPriviliges.contains(PrivilegesKeys.MakeResPrivilegesKeys.LOAD_PROFILE)) {
			allowedModifications.add(ModifcationParamTypes.LOAD_PROFILE);
		}
		if (AppSysParamsUtil.isAllowCapturePayRef()) {
			allowedModifications.add(ModifcationParamTypes.ALLOW_CAPTURE_PAY_REFERENCE);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_APPLY_CREDIT)) {
			allowedModifications.add(ModifcationParamTypes.ALLOW_NAMED_CREDIT_TRANSFER);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_APPLY_CREDIT_ANY)) {
			allowedModifications.add(ModifcationParamTypes.ALLOW_ANY_CREDIT_TRANSFER);
		}
		/* charges related info */
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.PAX_ADJUST_CREDIT)) {
			allowedModifications.add(ModifcationParamTypes.ADJUST_CHARGES);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.PAX_ADJUST_CREDIT_REFUND)) {
			allowedModifications.add(ModifcationParamTypes.REFUNDABLE_CHARGE_ADJUST);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.PAX_ADJUST_CREDIT_EXP_DATE_CHANGE)) {
			allowedModifications.add(ModifcationParamTypes.ALLOW_PAX_CREDIT_EXP_DATE_CHANGE);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.PAX_ADJUST_CREDIT_REINSTATE)) {
			allowedModifications.add(ModifcationParamTypes.ALLOW_REINSTATE_PAX_CREDITS);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.ADJUST_OTHER_CARRIER_CHARGES)) {
			allowedModifications.add(ModifcationParamTypes.ADJUST_OTHER_CARRIER_CHARGES);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.PAX_ADJUST_CREDIT_NONREFUND)) {
			allowedModifications.add(ModifcationParamTypes.NON_REFFUNDABLE_CHARGE_ADJUST);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.ALLOW_REFUND_NO_CREDIT)) {
			allowedModifications.add(ModifcationParamTypes.ALLOW_REFUND_NO_CREDIT);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.ALLOW_CASH_REFUND)) {
			allowedModifications.add(ModifcationParamTypes.ALLOW_CASH_REFUND);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.ALLOW_ON_ACC_REFUND)) {
			allowedModifications.add(ModifcationParamTypes.ALLOW_ON_ACCOUNT_REFUND);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.ALLOW_BSP_REFUND)) {
			allowedModifications.add(ModifcationParamTypes.ALLOW_BSP_REFUND);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.ALLOW_ON_ACCOUNT_RPT_REFUND)) {
			allowedModifications.add(ModifcationParamTypes.ALLOW_ON_ACCOUNT_RPT_REFUND);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.ALLOW_ANY_CARRIER_REFUND)) {
			allowedModifications.add(ModifcationParamTypes.ALLOW_ANY_CARRIER_REFUND);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_PAX_REFUND_CCARD)) {
			allowedModifications.add(ModifcationParamTypes.ALLOW_CREDIT_CARD_REFUND);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_PAX_REFUND_CCARD_ANY)) {
			allowedModifications.add(ModifcationParamTypes.ALLOW_ANY_CREDIT_CARD_REFUND);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.ALT_BUFFERTIME_CHARGES)) {
			allowedModifications.add(ModifcationParamTypes.BUFFERTIME_MOD_ACCOUNT_ALLOWED);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_EXT_PAY_VIEW)) {
			allowedModifications.add(ModifcationParamTypes.VIEW_EXTERNAL_PAYMENTS);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_VIEW_CREDIT)) {
			allowedModifications.add(ModifcationParamTypes.VIEW_CREDIT);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.PAX_REFUND)) {
			allowedModifications.add(ModifcationParamTypes.ALLOW_REFUND);
		}
		if (AppSysParamsUtil.isAllowPercentageWiseModificationCharges()) {
			allowedModifications.add(ModifcationParamTypes.SHOW_CHARGE_TYPE_OVERRIDE);

		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.OVERRIDE_CHARGES)) {
			allowedModifications.add(ModifcationParamTypes.HAS_OVERRIDE_CNX_MOD_CHARGE);
		}
		/* charges related inf0 */
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.ALLOW_LCC_ANY_OND_MODIFY)) {
			allowedModifications.add(ModifcationParamTypes.ALLOW_LCC_ANY_OND_MODIFY);
		}

		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.PRINT_ITN_FOR_PARTIAL_PAYMENTS)) {
			allowedModifications.add(ModifcationParamTypes.PRINT_ITN_FOR_PARTIAL_PAYMENTS);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.ALT_RES_TRANSFER_SEGMENT)) {
			allowedModifications.add(ModifcationParamTypes.TRANSFER_ALERT);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.CLEAR_ALERT)) {
			allowedModifications.add(ModifcationParamTypes.CLEAR_ALERT);
		}

		if (AppSysParamsUtil.isGroundServiceEnabled()) {
			allowedModifications.add(ModifcationParamTypes.GROUND_SERVICE_ENABLED);
		}
		
		if (AppSysParamsUtil.restrictModificationForCancelledOrTotallyFlownBookings()) {
			allowedModifications.add(ModifcationParamTypes.RESTRICT_MODIFICATION_CNX_FLOWN_BOOKINGS);
		}

		/* create reservation params */
		if (userPriviliges.contains(PrivilegesKeys.MakeResPrivilegesKeys.PRIVI_NAME_CHANGE_FARE_ANY)) {
			allowedModifications.add(ModifcationParamTypes.CHANGE_FARE_ANY);
		}
		if (userPriviliges.contains(PrivilegesKeys.MakeResPrivilegesKeys.PRIVI_OVERRIDE_AGENT_FARE_ONLY)) {
			allowedModifications.add(ModifcationParamTypes.OVERRIDE_AGENT_FARE_ONLY);
		}
		if (userPriviliges.contains(PrivilegesKeys.MakeResPrivilegesKeys.PRIVI_VIEW_PUBLIC_ONEWAY_FOR_RETURNS)) {
			allowedModifications.add(ModifcationParamTypes.VIEW_PUBLIC_ONEWAY_FOR_RETURNS);
		}
		
		if (userPriviliges.contains(PrivilegesKeys.MakeResPrivilegesKeys.PRIVI_OVERRIDE_VISIBLE_FARE)) {
			allowedModifications.add(ModifcationParamTypes.OVERRIDE_VISIBLE_FARE);
		}
		if (userPriviliges.contains(PrivilegesKeys.MakeResPrivilegesKeys.RES_MAKE_ONHOLD)) {
			allowedModifications.add(ModifcationParamTypes.ALLOW_ON_HOLD);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.RES_ALLOW_BUFFERTIME_OHD)) {
			allowedModifications.add(ModifcationParamTypes.ALLOW_ON_HOLD_BUFFER_TIME);
		}
		if (userPriviliges.contains(PrivilegesKeys.MakeResPrivilegesKeys.ALLOW_SEATBLOCKING)) {
			allowedModifications.add(ModifcationParamTypes.ALLOW_BLOCK_SEATS);
		}

		if (AppSysParamsUtil.isMultipleMealSelectionEnabled()) {
			allowedModifications.add(ModifcationParamTypes.MULTI_MEAL_ENABLED);
		}

		if (AppSysParamsUtil.isEnableTransactionGranularity()) {
			allowedModifications.add(ModifcationParamTypes.ENABLE_TRANSACTION_GRANULARITY_TRACKING);
		}

		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_ONHOLD)) {
			allowedModifications.add(ModifcationParamTypes.PRIVI_ONHOLD);
		}

		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_MODIFY_PASSENGER_COUPON_STATUS)) {
			allowedModifications.add(ModifcationParamTypes.CHANGE_PASSENGER_COUPON_STATUS);
		}

		if (AppSysParamsUtil.isBaggageMandatory()) {
			allowedModifications.add(ModifcationParamTypes.BAGGAGE_MANDATORY_ENABLED);
		}
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_OVERRIDE_SAME_FARE_MODIFICATION)) {
			allowedModifications.add(ModifcationParamTypes.OVERRIDE_SAME_FARE_MODIFICATION);
		}

		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_MODIFY_NAME_WITHIN_THRESHOLD)) {
			allowedModifications.add(ModifcationParamTypes.MODIFY_PAX_NAME_WITHIN_THRESHOLD_TIME);
		}
		if (AppSysParamsUtil.applyExtendedNameModificationFunctionality()) {
			if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_MODIFY_EXTENDED_NAME_WITHIN_BUFFER)) {
				allowedModifications.add(ModifcationParamTypes.MODIFY_EXTENDED_NAME_WITHIN_BUFFER);
			}

			if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_MODIFY_NAME_CREDITUTILIZED_PNR)) {
				allowedModifications.add(ModifcationParamTypes.MODIFY_CREDIT_UTILIZED_PAX_NAME);
			}

			if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_MODIFY_NAME_WITH_DEPARTED_SEGMENTS)) {
				allowedModifications.add(ModifcationParamTypes.MODIFY_PAX_NAME_HAVING_DEPARTED_SEGMENTS);
			}

			if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_MODIFY_EXTENDED_NAME)) {
				allowedModifications.add(ModifcationParamTypes.MODIFY_EXTENDED_NAME);
			}
		}
		
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_MODIFY_USER_NOTE_WITH_DEPARTED_SEGMENTS)) {
			allowedModifications.add(ModifcationParamTypes.MODIFY_USER_NOTE_HAVING_DEPARTED_SEGMENTS);
		}

		if (AppSysParamsUtil.applyExtendedNameModificationFunctionality()
				&& userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_MODIFY_NAME_WITHIN_CUTOFF)) {
			allowedModifications.add(ModifcationParamTypes.MODIFY_PAX_NAME_WITHIN_NAME_CHANGE_CUTOFF);
		}

		if (userPriviliges.contains(PrivilegesKeys.MakeResPrivilegesKeys.ALLOW_CSV_DETAIL_UPLOAD)) {
			allowedModifications.add(ModifcationParamTypes.ALLOW_CSV_DETAIL_UPLOAD);
		}

		if (userPriviliges.contains(PrivilegesKeys.MakeResPrivilegesKeys.ALLOW_CREATE_USER_ALERT)) {
			allowedModifications.add(ModifcationParamTypes.ALLOW_CREATE_USER_ALERT);
		}		
		
		if (AppSysParamsUtil.isEnableFareRuleLevelNCC(ApplicationEngine.XBE)) {
			allowedModifications.add(ModifcationParamTypes.FARE_RULE_NCC_ENABLED);
		}
		
		if (AppSysParamsUtil.isEnableFareRuleLevelNCCForFlown(ApplicationEngine.XBE)) {
			allowedModifications.add(ModifcationParamTypes.FARE_RULE_NCC_FOR_FLOWN_ENABLED);
		}
		
		if (AppSysParamsUtil.isDuplicateNameCheckEnabled()) {
			allowedModifications.add(ModifcationParamTypes.DUPLICATE_NAME_CHECK_ENABLED);
		}

		if(AppSysParamsUtil.isBlacklistpassengerCheckEnabled()){
			allowedModifications.add(ModifcationParamTypes.ENABLE_BLACKLIST_PAX);
		}
		
		if (userPriviliges.contains(PrivilegesKeys.MakeResPrivilegesKeys.PRIVI_DUPLI_NAME_SKIP)) {
			allowedModifications.add(ModifcationParamTypes.DUPLICATE_NAME_SKIP);
		}
		
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_DUPLI_NAME_SKIP)) {
			allowedModifications.add(ModifcationParamTypes.DUPLICATE_NAME_SKIP_MODIFY);
		}
		
		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_ONHOlD_NAME_CHANGE)) {
			allowedModifications.add(ModifcationParamTypes.ONHOLD_NAME_CHANGE);
		}

		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.ALT_BUFFERTIME_NAME)) {
			allowedModifications.add(ModifcationParamTypes.ALLOW_BUFFERTIME_NAME_CHANGE);
		}

		if (AppSysParamsUtil.isEnforceSameHigher()
				&& userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_ALLOW_OVERRIDE_SAME_OR_HIGHER_FARE)) {
			allowedModifications.add(ModifcationParamTypes.ALLOW_OVERRIDE_SAME_OR_HIGHER_FARE);
		}

		if (userPriviliges.contains(PrivilegesKeys.AlterResPrivilegesKeys.PRIVI_OVERRIDE_REQUOTE_NAME_CHANGE)) {
			allowedModifications.add(ModifcationParamTypes.ALLOW_OVERRIDE_REQUOTE_NAME_CHANGE);
		}

		return allowedModifications;
	}

}
