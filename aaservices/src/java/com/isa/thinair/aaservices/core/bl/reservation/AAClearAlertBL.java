package com.isa.thinair.aaservices.core.bl.reservation;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AlertActionCode;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BookedFlightAlertInfo;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAClearAlertRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAClearAlertRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.airreservation.api.service.SegmentBD;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class AAClearAlertBL {

	private static final Log log = LogFactory.getLog(AAClearAlertBL.class);

	// Request & Response objects
	private AAClearAlertRQ aaClearAlertRQ;

	public void setAAClearAlertRQ(AAClearAlertRQ aaClearAlertRQ) {
		this.aaClearAlertRQ = aaClearAlertRQ;
	}

	public AAClearAlertRS execute() {

		AAClearAlertRS aaClearAlertRS = new AAClearAlertRS();

		try {

			aaClearAlertRS.setResponseAttributes(new AAResponseAttributes());

			if (log.isDebugEnabled()) {
				log.debug("Clearing segment alerts on : " + AppSysParamsUtil.getDefaultCarrierCode() + " STARTED");
			}

			String actionTaken = this.getAlertAction(aaClearAlertRQ.getAlertAction());
			String actionText = aaClearAlertRQ.getAlertText();
			Collection<BookedFlightAlertInfo> bookedFlightAlertInfos = aaClearAlertRQ.getAlertInfo();

			Map<Integer, String> mapPnrSegActions = new HashMap<Integer, String>();

			for (BookedFlightAlertInfo bookedFlightAlertInfo : bookedFlightAlertInfos) {
				mapPnrSegActions.put(new Integer(bookedFlightAlertInfo.getFlightRefNumber()), " Action Taken : " + actionTaken
						+ " Action Text : " + actionText);
			}

			SegmentBD segmentBD = ReservationModuleUtils.getSegmentBD();

			segmentBD.clearSegmentAlerts(mapPnrSegActions);

			aaClearAlertRS.getResponseAttributes().setSuccess(new AASuccess());

			if (log.isDebugEnabled()) {
				log.debug("Returning success response for clear segment alerts");
			}

		} catch (Exception ex) {
			log.error("Clear segment alerts failed ", ex);
			AAExceptionUtil.addAAErrrors(aaClearAlertRS.getResponseAttributes().getErrors(), ex);
		}
		return aaClearAlertRS;
	}

	private String getAlertAction(AlertActionCode alertActionCode) {
		switch (alertActionCode) {
		case ALERT_1_OTHER:
			return "Other";
		case ALERT_2_EMAIL:
			return "Email";
		case ALERT_3_FAX:
			return "Fax";
		case ALERT_4_TELEPHONE:
			return "Telephone";
		default:
			return "";
		}
	}
}
