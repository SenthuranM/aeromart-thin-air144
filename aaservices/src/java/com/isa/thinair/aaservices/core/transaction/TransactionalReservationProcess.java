package com.isa.thinair.aaservices.core.transaction;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.isa.connectivity.profiles.maxico.v1.common.dto.SegmentFareType;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.BookingClassAlloc;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndRebuildCriteria;
import com.isa.thinair.airinventory.api.util.InventoryAPIUtil;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OndFareSegChargeTO;
import com.isa.thinair.airproxy.api.utils.assembler.IPaxCountAssembler;
import com.isa.thinair.airproxy.api.utils.converters.AvailabilityConvertUtil;
import com.isa.thinair.commons.api.exception.ModuleException;

public class TransactionalReservationProcess {

	private Map<String, List<FareSegChargeTO>> ondFareSegChargeMap = new HashMap<String, List<FareSegChargeTO>>();

	private IPaxCountAssembler paxCountAssembler = null;

	private String transactionIdentifier = null;

	private long lastAccessed;
	private Map<String, BundledFareDTO> segmentBundledFareMap;

	public Set<FareSegChargeTO> getFSCSet(List<SegmentFareType> segmentFareTypeList) {
		Set<FareSegChargeTO> fscSet = new HashSet<FareSegChargeTO>();
		for (SegmentFareType fst : segmentFareTypeList) {
			if (ondFareSegChargeMap.containsKey(fst.getSegmentCode())) {
				fscSet.addAll(ondFareSegChargeMap.get(fst.getSegmentCode()));
			}
		}
		return fscSet;
	}

	public Collection<OndFareDTO> getOndFareDTOListForReservation(List<SegmentFareType> segmentFareTypeList,
			boolean overrideInboundOndFlag) throws ModuleException {
		updateLastAccessed();
		List<OndFareDTO> ondFareDTOList = null;

		if (segmentFareTypeList != null) {
			ondFareDTOList = new ArrayList<OndFareDTO>();
			Set<FareSegChargeTO> fscSet = getFSCSet(segmentFareTypeList);
			for (FareSegChargeTO fsc : fscSet) {
				// TODO : set correct booking type for lcc bookings
				OndRebuildCriteria ondRebuildCriteria = AvailabilityConvertUtil.getOndRebuildCriteria(paxCountAssembler, fsc,
						getBookingType(fsc), false);
				Collection<OndFareDTO> collFares = AAServicesModuleUtils.getReservationQueryBD().recreateFareSegCharges(
						ondRebuildCriteria);

				for (OndFareDTO ondFareDTO : collFares) {
					if (overrideInboundOndFlag) {
						ondFareDTO.setInBoundOnd(fsc.isInbound() || ondFareDTO.isInBoundOnd());
					}
					setSelectedBundledFare(ondFareDTO);
					ondFareDTOList.add(ondFareDTO);
				}
			}

			Collections.sort(ondFareDTOList);
		}
		return ondFareDTOList;
	}

	private void setSelectedBundledFare(OndFareDTO ondFareDTO) {
		if (segmentBundledFareMap != null && !segmentBundledFareMap.isEmpty()) {
			for (Entry<String, BundledFareDTO> bundledFareEntry : segmentBundledFareMap.entrySet()) {
				String segmentCode = bundledFareEntry.getKey();
				BundledFareDTO bundledFareDTO = bundledFareEntry.getValue();

				Collection<FlightSegmentDTO> colFlightSegmentDTOs = ondFareDTO.getSegmentsMap().values();
				for (FlightSegmentDTO flightSegmentDTO : colFlightSegmentDTOs) {
					if (!InventoryAPIUtil.isBusSegment(flightSegmentDTO) && flightSegmentDTO.getSegmentCode().equals(segmentCode)
							&& bundledFareDTO != null) {
						ondFareDTO.setSelectedBundledFarePeriodId(bundledFareDTO.getBundledFarePeriodId());
						return;
					}
				}
			}
		}
	}

	public String getBookingType(FareSegChargeTO fsc) {
		if (fsc.getOndFareSegChargeTOs() != null) {
			for (OndFareSegChargeTO ondFareSegChg : fsc.getOndFareSegChargeTOs()) {
				if (ondFareSegChg.getFsegBCAlloc() != null) {
					for (Entry<Integer, LinkedList<BookingClassAlloc>> bcAllocEntry : ondFareSegChg.getFsegBCAlloc().entrySet()) {
						LinkedList<BookingClassAlloc> bcAllocList = bcAllocEntry.getValue();
						if (bcAllocList != null) {
							for (BookingClassAlloc bookingClassAlloc : bcAllocList) {
								if (bookingClassAlloc != null) {
									return bookingClassAlloc.getBookingClassType();
								}
							}
						}
					}
				}
			}
		}
		return null;
	}

	public Collection<OndFareDTO> getOndFareDTOListForReservation(List<SegmentFareType> segmentFareTypeList)
			throws ModuleException {
		return getOndFareDTOListForReservation(segmentFareTypeList, true);
	}

	public String getTransactionIdentifier() {
		updateLastAccessed();
		return transactionIdentifier;
	}

	public void setTransactionIdentifier(String transactionIdentifier) {
		updateLastAccessed();
		this.transactionIdentifier = transactionIdentifier;
	}

	public boolean hasFareType(List<SegmentFareType> segmentFareTypeList) {
		updateLastAccessed();
		boolean hasFareType = true;
		for (SegmentFareType fst : segmentFareTypeList) {
			if (!ondFareSegChargeMap.containsKey(fst.getSegmentCode())) {
				hasFareType = false;
				break;
			} else {
				hasFareType = true;
				break;
			}
		}
		return hasFareType;
	}

	public void addOndFareSegChargeTOList(List<String> segmentCodeList, FareSegChargeTO fareSegChargeTO) {
		updateLastAccessed();
		for (String segmentCode : segmentCodeList) {
			if (ondFareSegChargeMap.containsKey(segmentCode)) {
				ondFareSegChargeMap.get(segmentCode).add(fareSegChargeTO);
			} else {
				List<FareSegChargeTO> fareSegCharges = new ArrayList<FareSegChargeTO>();
				fareSegCharges.add(fareSegChargeTO);
				ondFareSegChargeMap.put(segmentCode, fareSegCharges);
			}
		}
	}

	public void setOndFareSegChargeTOList(List<String> segmentCodeList, FareSegChargeTO fareSegChargeTO,
			IPaxCountAssembler paxCAsm) {
		updateLastAccessed();
		clearOndFareSegSelection();
		addOndFareSegChargeTOList(segmentCodeList, fareSegChargeTO);
		paxCountAssembler = paxCAsm;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Collection getBlockSeats(List<SegmentFareType> segmentFareTypeList) {
		updateLastAccessed();
		Collection blockSeatIds = null;
		if (segmentFareTypeList != null) {

			Set<FareSegChargeTO> fscSet = getFSCSet(segmentFareTypeList);
			for (FareSegChargeTO fsc : fscSet) {
				if (fsc.getBlockSeatIds() != null) {
					if (blockSeatIds == null)
						blockSeatIds = new HashSet();
					blockSeatIds.addAll(fsc.getBlockSeatIds());
				}
			}
		}
		return blockSeatIds;
	}

	/**
	 * When releasing block seats we don't want seg wise of fare wise block ids. We just need to get the block seat ids
	 * for all segments.
	 * 
	 * @return block seat ids
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Collection getBlockSeats() {
		Collection blockSeatIds = new ArrayList();
		if (ondFareSegChargeMap != null && !ondFareSegChargeMap.isEmpty()) {
			for (String ond : ondFareSegChargeMap.keySet()) {
				if (ondFareSegChargeMap.get(ond) != null && ondFareSegChargeMap.get(ond).size() > 0) {
					for (FareSegChargeTO fareSegChargeTO : ondFareSegChargeMap.get(ond)) {
						if (fareSegChargeTO.getBlockSeatIds() != null) {
							blockSeatIds.addAll(fareSegChargeTO.getBlockSeatIds());
						}
					}
				}
			}
		}
		return blockSeatIds;
	}

	/**
	 * After releasing all blocked seats we need to nullify block seat ids in the session. This method is used for that
	 * purpose
	 * 
	 * @return block seat ids
	 */
	public void removeBlockSeats() {
		if (ondFareSegChargeMap != null && !ondFareSegChargeMap.isEmpty()) {
			for (String ond : ondFareSegChargeMap.keySet()) {
				if (ondFareSegChargeMap.get(ond) != null && ondFareSegChargeMap.get(ond).size() > 0) {
					for (FareSegChargeTO fareSegChargeTO : ondFareSegChargeMap.get(ond)) {
						fareSegChargeTO.setBlockSeatIds(null);
					}
				}
			}
		}
	}

	@SuppressWarnings("rawtypes")
	public void setBlockSeats(List<SegmentFareType> segmentFareTypeList, Collection blockSeats) {
		updateLastAccessed();
		if (segmentFareTypeList != null) {
			Set<FareSegChargeTO> fscSet = getFSCSet(segmentFareTypeList);

			for (FareSegChargeTO fsc : fscSet) {
				fsc.setBlockSeatIds(blockSeats);
			}
		}
	}

	public void updateLastAccessed() {
		lastAccessed = System.currentTimeMillis();
	}

	public long getLastAccessedTimestamp() {
		return lastAccessed;
	}

	public boolean qualifyForExpiration(long sessionTimeoutDuration) {
		boolean isExpired = false;
		if (System.currentTimeMillis() - getLastAccessedTimestamp() > sessionTimeoutDuration) {
			paxCountAssembler = null;
			transactionIdentifier = null;
			isExpired = true;
		}
		return isExpired;
	}

	public IPaxCountAssembler getPaxCountAssembler() {
		return paxCountAssembler;
	}

	public Map<String, BundledFareDTO> getSegmentBundledFareMap() {
		if (segmentBundledFareMap == null) {
			segmentBundledFareMap = new HashMap<String, BundledFareDTO>();
		}
		return segmentBundledFareMap;
	}

	public void setSegmentBundledFareMap(Map<String, BundledFareDTO> segmentBundledFareMap) {
		this.segmentBundledFareMap = segmentBundledFareMap;
	}

	public void addSegmentBundledFare(String segmentCode, BundledFareDTO bundledFareDTO) {
		getSegmentBundledFareMap().put(segmentCode, bundledFareDTO);
	}

	public boolean isAtleastOneBundledFareSelected() {
		if (segmentBundledFareMap != null && !segmentBundledFareMap.isEmpty()) {
			Collection<BundledFareDTO> bundledFareDTOs = segmentBundledFareMap.values();
			if (bundledFareDTOs != null && !bundledFareDTOs.isEmpty()) {
				for (BundledFareDTO bundledFareDTO : bundledFareDTOs) {
					if (bundledFareDTO != null) {
						return true;
					}
				}
			}
		}

		return false;
	}

	private void clearOndFareSegSelection() {
		if (ondFareSegChargeMap != null && !ondFareSegChargeMap.isEmpty()) {
			for (String ond : ondFareSegChargeMap.keySet()) {
				ondFareSegChargeMap.put(ond, new ArrayList<FareSegChargeTO>());
			}
		}
	}

}
