package com.isa.thinair.aaservices.core.bl.dummycarrierfulfillment;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AirReservation;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAErrorCode;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAPOS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAPayCarrierFulfullmentRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAReservation;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.aaservices.core.util.AAExtCarrierFulfillmentUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.ExternalSegmentTO;
import com.isa.thinair.airreservation.api.model.ExternalFlightSegment;
import com.isa.thinair.airreservation.api.model.ExternalPnrSegment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Handle changers to the dummy reservations when cancel reservation
 * 
 * @author malaka
 * 
 */
class AAFulfillCancelReservation extends BaseMarketingCarrierFulfillment {

	private static Log log = LogFactory.getLog(AAFulfillCancelReservation.class);

	AAFulfillCancelReservation(AAReservation aaAirReservation, AAReservation newAAAirReservation, AAPOS aaPos) {
		super(aaAirReservation, newAAAirReservation, aaPos);
	}

	@Override
	public AAPayCarrierFulfullmentRS execute() throws WebservicesException, ModuleException {

		checkConstraints();

		AAPayCarrierFulfullmentRS carrierFulfillment = new AAPayCarrierFulfullmentRS();
		carrierFulfillment.setResponseAttributes(new AAResponseAttributes());

		try {

			if (log.isDebugEnabled()) {
				log.debug("AAFulfillCancelReservation::begin for pnr [" + aaAirReservation.getGroupPnr() + "]");
			}

			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setGroupPNR(aaAirReservation.getGroupPnr());
			Reservation reservation = getCarrierReservation(pnrModesDTO);

			if (reservation == null || reservation.getExternalReservationSegment() == null) {
				throw new WebservicesException(AAErrorCode.ERR_25_MAXICO_REQUIRED_THE_RESERVATION_NO_LONGER_EXIST,
						"Reservation does not exits");
			}

			// updating the reservation and pax status form the opCarrier
			AAExtCarrierFulfillmentUtil.updateReservationsStatus(reservation, aaAirReservation);

			Collection<ExternalSegmentTO> colExternalSegmentTO = new ArrayList<ExternalSegmentTO>();
			for (ExternalPnrSegment externalSegment : (Collection<ExternalPnrSegment>) reservation
					.getExternalReservationSegment()) {
				ExternalSegmentTO externalSegmentTO = new ExternalSegmentTO();
				ExternalFlightSegment externalFlightSegment = externalSegment.getExternalFlightSegment();
				externalSegmentTO.setFlightNo(externalFlightSegment.getFlightNumber());
				externalSegmentTO.setDepartureDate(externalFlightSegment.getEstTimeDepatureLocal());
				externalSegmentTO.setArrivalDate(externalFlightSegment.getEstTimeArrivalLocal());
				externalSegmentTO.setSegmentCode(externalFlightSegment.getSegmentCode());
				externalSegmentTO.setSegmentSequence(externalSegment.getSegmentSeq());
				externalSegmentTO.setCabinClassCode(externalSegment.getCabinClassCode());
				externalSegmentTO.setLogicalCabinClassCode(externalSegment.getLogicalCabinClassCode());
				externalSegmentTO.setStatus(ReservationInternalConstants.ReservationExternalSegmentStatus.CANCEL);
				externalSegmentTO.setSubStatus(externalSegment.getSubStatus());

				colExternalSegmentTO.add(externalSegmentTO);
			}

			// CNX the external segments linking to dummy reservation
			ServiceResponce responce = AAServicesModuleUtils.getSegmentBD().changeExternalSegmentsForDummyReservation(
					reservation, colExternalSegmentTO);
			Long version = (Long) responce.getResponseParam(CommandParamNames.VERSION);

			if (log.isDebugEnabled()) {
				log.debug("AAFulfillCancelReservation::end");
			}

			carrierFulfillment.getResponseAttributes().setSuccess(new AASuccess());
			carrierFulfillment.setResVersion(version.toString());
		} catch (ModuleException e) {
			log.error("fulfill AAFulfillCancelReservation fail", e);
			throw new ModuleException("aaservices.paycarrier.fulfillment.failed", e);
		}

		return carrierFulfillment;
	}

	@Override
	public void checkConstraints() throws WebservicesException {
		AirReservation airReservation = aaAirReservation.getAirReservation();

		if ((airReservation == null || !ReservationInternalConstants.ReservationStatus.CANCEL.equals(airReservation.getStatus()))) {
			log.error("airReservation is : " + airReservation == null ? "null" : ("not null and reservation status : "
					+ airReservation.getStatus() + " for reservation " + aaAirReservation.getGroupPnr()));
			throw new WebservicesException(AAErrorCode.ERR_1_MAXICO_REQUIRED_INVALID_VALUE, "invalide value in request");
		}

	}

}
