package com.isa.thinair.aaservices.core.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.isa.connectivity.profiles.maxico.v1.common.dto.BaseFare;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ExternalCharge;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ExternalChargeType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FareCharacteristics;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FareRule;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FareType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Fee;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FeeType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlexiInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ONDExternalCharge;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PerPaxPriceInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Surcharge;
import com.isa.connectivity.profiles.maxico.v1.common.dto.Tax;
import com.isa.thinair.aaservices.core.bl.availability.FareRuleProxy;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareSummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.airpricing.api.criteria.PricingConstants.ChargeGroups;
import com.isa.thinair.airpricing.api.dto.FlexiRuleDTO;
import com.isa.thinair.airpricing.api.dto.FlexiRuleFlexibilityDTO;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airpricing.api.model.Charge;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class PriceCalculationUtil {

	public static BigDecimal calculateTotalFare(List<FareSummaryDTO> fareSummaryList, int adultCount, int childCount,
			int infantCount) {

		BigDecimal totalFare = AccelAeroCalculator.getDefaultBigDecimalZero();

		for (FareSummaryDTO fareSummary : fareSummaryList) {

			BigDecimal totalAdultFare = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal totalChildFare = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal totalInfantFare = AccelAeroCalculator.getDefaultBigDecimalZero();
			if (fareSummary.getFareAmount(PaxTypeTO.ADULT) != null)
				totalAdultFare = AccelAeroCalculator.multiply(new BigDecimal(fareSummary.getFareAmount(PaxTypeTO.ADULT)),
						new BigDecimal(adultCount));
			if (fareSummary.getFareAmount(PaxTypeTO.CHILD) != null)
				totalChildFare = AccelAeroCalculator.multiply(new BigDecimal(fareSummary.getFareAmount(PaxTypeTO.CHILD)),
						new BigDecimal(childCount));
			if (fareSummary.getFareAmount(PaxTypeTO.INFANT) != null)
				totalInfantFare = AccelAeroCalculator.multiply(new BigDecimal(fareSummary.getFareAmount(PaxTypeTO.INFANT)),
						new BigDecimal(infantCount));
			totalFare = AccelAeroCalculator.add(totalFare, totalAdultFare, totalChildFare, totalInfantFare);
		}

		return totalFare;
	}

	public static PerPaxPriceInfo createPerPaxPriceInfo(Collection<OndFareDTO> ondFareDTOList, String paxType,
			boolean isFixedFare, Date quartedDate, Map<Integer, Integer> actualOndSequence) throws ModuleException {

		PerPaxPriceInfo paxPriceInfo = new PerPaxPriceInfo();
		paxPriceInfo.setPassengerPrice(new FareType());

		BigDecimal totalPaxFare = AccelAeroCalculator.getDefaultBigDecimalZero();
		for (OndFareDTO ondFareDTO : ondFareDTOList) {
			populatePaxTaxSurchargeList(paxPriceInfo, ondFareDTO, paxType, isFixedFare, quartedDate, actualOndSequence);

			FareSummaryDTO fareSummary = ondFareDTO.getFareSummaryDTO();
			if (fareSummary.getFareAmount(paxType) != null) {

				BigDecimal ondFare = new BigDecimal(fareSummary.getFareAmount(paxType));
				BaseFare baseFare = new BaseFare();
				baseFare.setAmount(ondFare);
				baseFare.setSegmentCode(ondFareDTO.getOndCode());
				baseFare.getCarrierCode().add(AppSysParamsUtil.getDefaultCarrierCode());
				baseFare.setApplicableDateTime(quartedDate);
				baseFare.getApplicablePassengerType().add(PaxTypeUtils.convertAAPaxCodeToLCCPaxCodes(paxType));
				baseFare.setOndSequence(actualOndSequence.get(ondFareDTO.getOndSequence()));
				for (Object segmentId : ondFareDTO.getSegmentsMap().keySet()) {
					baseFare.getSegmentRPH().add(segmentId.toString());
				}
				paxPriceInfo.getPassengerPrice().getBaseFare().add(baseFare);

				totalPaxFare = AccelAeroCalculator.add(totalPaxFare, ondFare);
			}
		}

		FareType perPaxFareType = paxPriceInfo.getPassengerPrice();
		perPaxFareType.setTotalFare(totalPaxFare);
		perPaxFareType.setTotalPrice(AccelAeroCalculator.add(perPaxFareType.getTotalFare(), perPaxFareType.getTotalSurcharges(),
				perPaxFareType.getTotalTaxes(), perPaxFareType.getTotalFees(), perPaxFareType.getTotalONDExternalCharges()));

		return paxPriceInfo;
	}

	private static void populatePaxTaxSurchargeList(PerPaxPriceInfo paxPriceInfo, OndFareDTO ondFareDTO, String paxType,
			boolean isFixedFare, Date initiTime, Map<Integer, Integer> actualOndSequence) throws ModuleException {

		// Ond code
		String ondCode = ondFareDTO.getOndCode(false);

		String carrierCode = AppSysParamsUtil.getDefaultCarrierCode();

		// All Charges
		@SuppressWarnings("unchecked")
		Collection<QuotedChargeDTO> chargesList = ondFareDTO.getAllCharges();

		// All Flexi Charges
		@SuppressWarnings("unchecked")
		Collection<FlexiRuleDTO> flexiChargesList = ondFareDTO.getAllFlexiCharges();

		HashMap<String, Fee> paxFeeMap = new HashMap<String, Fee>();
		BigDecimal totalFees = paxPriceInfo.getPassengerPrice().getTotalFees();
		BigDecimal paxFeeTotal = totalFees != null ? totalFees : AccelAeroCalculator.getDefaultBigDecimalZero();

		HashMap<String, Tax> paxTaxMap = new HashMap<String, Tax>();
		BigDecimal totalTaxes = paxPriceInfo.getPassengerPrice().getTotalTaxes();
		BigDecimal paxTaxTotal = totalTaxes != null ? totalTaxes : AccelAeroCalculator.getDefaultBigDecimalZero();

		HashMap<String, Surcharge> paxSurchargeMap = new HashMap<String, Surcharge>();
		BigDecimal totalSurcharges = paxPriceInfo.getPassengerPrice().getTotalSurcharges();
		BigDecimal paxSurchargeTotal = totalSurcharges != null ? totalSurcharges : AccelAeroCalculator.getDefaultBigDecimalZero();

		HashMap<String, ExternalCharge> outBoundPaxFlexiChargeMap = new HashMap<String, ExternalCharge>();
		BigDecimal totalOutBoundPaxFlexiCharges = paxPriceInfo.getPassengerPrice().getTotalOutBoundExternalCharges();
		BigDecimal outBoundPaxFlexiChargeTotal = totalOutBoundPaxFlexiCharges != null
				? totalOutBoundPaxFlexiCharges
				: AccelAeroCalculator.getDefaultBigDecimalZero();

		HashMap<String, ExternalCharge> inBoundPaxFlexiChargeMap = new HashMap<String, ExternalCharge>();
		BigDecimal totalInBoundPaxFlexiCharges = paxPriceInfo.getPassengerPrice().getTotalInBoundExternalCharges();
		BigDecimal inBoundPaxFlexiChargeTotal = totalInBoundPaxFlexiCharges != null
				? totalInBoundPaxFlexiCharges
				: AccelAeroCalculator.getDefaultBigDecimalZero();

		if (chargesList != null && chargesList.size() > 0) {
			for (QuotedChargeDTO quotedChargeDTO : chargesList) {

				if (paxType.compareTo(PaxTypeTO.ADULT) == 0) {
					if (quotedChargeDTO.getApplicableTo() != Charge.APPLICABLE_TO_ADULT_CHILD
							&& quotedChargeDTO.getApplicableTo() != Charge.APPLICABLE_TO_ADULT_INFANT
							&& quotedChargeDTO.getApplicableTo() != Charge.APPLICABLE_TO_ADULT_ONLY
							&& quotedChargeDTO.getApplicableTo() != Charge.APPLICABLE_TO_ALL) {
						continue;
					}
				} else if (paxType.compareTo(PaxTypeTO.CHILD) == 0) {
					if (quotedChargeDTO.getApplicableTo() != Charge.APPLICABLE_TO_ADULT_CHILD
							&& quotedChargeDTO.getApplicableTo() != Charge.APPLICABLE_TO_CHILD_ONLY
							&& quotedChargeDTO.getApplicableTo() != Charge.APPLICABLE_TO_ALL) {
						continue;
					}
				} else if (paxType.compareTo(PaxTypeTO.INFANT) == 0) {
					if (quotedChargeDTO.getApplicableTo() != Charge.APPLICABLE_TO_ADULT_INFANT
							&& quotedChargeDTO.getApplicableTo() != Charge.APPLICABLE_TO_INFANT_ONLY
							&& quotedChargeDTO.getApplicableTo() != Charge.APPLICABLE_TO_ALL) {
						continue;
					}
				}

				if (quotedChargeDTO.getChargeGroupCode().compareTo(ChargeGroups.TAX) == 0) {
					BigDecimal paxTax = getPaxEffectiveCharge(quotedChargeDTO, paxType);
					Tax tax = paxTaxMap.get(quotedChargeDTO.getChargeCode());
					if (tax == null) {
						tax = new Tax();
						tax.setAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
						tax.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
						tax.setSegmentCode(ondCode);
						tax.setTaxCode(quotedChargeDTO.getChargeCode());
						tax.setTaxName(quotedChargeDTO.getChargeDescription());
						tax.setApplicableDateTime(initiTime);
						tax.getApplicablePassengerType().add(PaxTypeUtils.convertAAPaxCodeToLCCPaxCodes(paxType));
						tax.setOndSequence(actualOndSequence.get(ondFareDTO.getOndSequence()));
						paxTaxMap.put(quotedChargeDTO.getChargeCode(), tax);
					}
					tax.setAmount(AccelAeroCalculator.add(tax.getAmount(), paxTax));
					paxTaxTotal = AccelAeroCalculator.add(paxTaxTotal, paxTax);
				} else if (quotedChargeDTO.getChargeGroupCode().compareTo(ChargeGroups.SURCHARGE) == 0
						|| quotedChargeDTO.getChargeGroupCode().compareTo(ChargeGroups.INFANT_SURCHARGE) == 0) {
					BigDecimal paxSurcharge = getPaxEffectiveCharge(quotedChargeDTO, paxType);
					Surcharge surcharge = paxSurchargeMap.get(quotedChargeDTO.getChargeCode());
					if (surcharge == null) {
						surcharge = new Surcharge();
						surcharge.setAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
						surcharge.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
						surcharge.setSegmentCode(ondCode);
						surcharge.setSurchargeCode(quotedChargeDTO.getChargeCode());
						surcharge.setSurchargeName(quotedChargeDTO.getChargeDescription());
						surcharge.setApplicableDateTime(initiTime);
						surcharge.getApplicablePassengerType().add(PaxTypeUtils.convertAAPaxCodeToLCCPaxCodes(paxType));
						surcharge.setOndSequence(actualOndSequence.get(ondFareDTO.getOndSequence()));
						paxSurchargeMap.put(quotedChargeDTO.getChargeCode(), surcharge);
					}
					surcharge.setAmount(AccelAeroCalculator.add(surcharge.getAmount(), paxSurcharge));
					paxSurchargeTotal = AccelAeroCalculator.add(paxSurchargeTotal, paxSurcharge);
				} else {
					// Add to fees
					BigDecimal paxFee = getPaxEffectiveCharge(quotedChargeDTO, paxType);
					Fee fee = paxFeeMap.get(quotedChargeDTO.getChargeCode());
					if (fee == null) {
						fee = new Fee();
						fee.setAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
						fee.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
						fee.setSegmentCode(ondCode);
						fee.setFeeCode(quotedChargeDTO.getChargeCode());
						fee.setFeeName(quotedChargeDTO.getChargeDescription());
						fee.setApplicableDateTime(initiTime);
						fee.getApplicablePassengerType().add(PaxTypeUtils.convertAAPaxCodeToLCCPaxCodes(paxType));
						fee.setOndSequence(actualOndSequence.get(ondFareDTO.getOndSequence()));

						if (quotedChargeDTO.getChargeGroupCode().equals(ReservationInternalConstants.ChargeGroup.ADJ)) {
							fee.setFeeType(FeeType.ADJ);
						} else if (quotedChargeDTO.getChargeGroupCode().equals(ReservationInternalConstants.ChargeGroup.CNX)) {
							fee.setFeeType(FeeType.CNX);
						} else if (quotedChargeDTO.getChargeGroupCode().equals(ReservationInternalConstants.ChargeGroup.MOD)) {
							fee.setFeeType(FeeType.MOD);
						} else {
							// FIXME, throw exception (Unidentified Fee) or add fee type called OTH (Other) ?
						}

						paxFeeMap.put(quotedChargeDTO.getChargeCode(), fee);
					}
					fee.setAmount(AccelAeroCalculator.add(fee.getAmount(), paxFee));
					paxSurchargeTotal = AccelAeroCalculator.add(paxSurchargeTotal, paxFee);
				}

			}
		}

		BigDecimal totalONDPaxFlexiChargeTotal = paxPriceInfo.getPassengerPrice().getTotalONDExternalCharges();
		totalONDPaxFlexiChargeTotal = totalONDPaxFlexiChargeTotal != null ? totalONDPaxFlexiChargeTotal : AccelAeroCalculator
				.getDefaultBigDecimalZero();

		BigDecimal ondPaxFlexiChargeTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
		List<ExternalCharge> flexiExternalCharges = new ArrayList<ExternalCharge>();

		if (flexiChargesList != null) {
			for (FlexiRuleDTO quotedFlexiChargeDTO : flexiChargesList) {
				ExternalCharge externalCharge = createExternalChargeTO(carrierCode, ondCode, initiTime, quotedFlexiChargeDTO,
						paxType);

				flexiExternalCharges.add(externalCharge);
				ondPaxFlexiChargeTotal = AccelAeroCalculator.add(ondPaxFlexiChargeTotal, externalCharge.getAmount());
				totalONDPaxFlexiChargeTotal = AccelAeroCalculator.add(totalONDPaxFlexiChargeTotal, externalCharge.getAmount());

			}
		}

		ONDExternalCharge ondExternalCharge = new ONDExternalCharge();
		ondExternalCharge.setOndSequence(actualOndSequence.get(ondFareDTO.getOndSequence()));
		ondExternalCharge.getExternalCharges().addAll(flexiExternalCharges);
		ondExternalCharge.setTotalExternalCharges(ondPaxFlexiChargeTotal);

		paxPriceInfo.getPassengerPrice().getOndExternalCharges().add(ondExternalCharge);
		paxPriceInfo.getPassengerPrice().setTotalONDExternalCharges(totalONDPaxFlexiChargeTotal);

		paxPriceInfo.getPassengerPrice().getFees().addAll(paxFeeMap.values());
		paxPriceInfo.getPassengerPrice().setTotalFees(paxFeeTotal);

		paxPriceInfo.getPassengerPrice().getTaxes().addAll(paxTaxMap.values());
		paxPriceInfo.getPassengerPrice().setTotalTaxes(paxTaxTotal);

		paxPriceInfo.getPassengerPrice().getSurcharges().addAll(paxSurchargeMap.values());
		paxPriceInfo.getPassengerPrice().setTotalSurcharges(paxSurchargeTotal);

		paxPriceInfo.getPassengerPrice().getOutBoundExternalCharges().addAll(outBoundPaxFlexiChargeMap.values());
		paxPriceInfo.getPassengerPrice().setTotalOutBoundExternalCharges(outBoundPaxFlexiChargeTotal);

		paxPriceInfo.getPassengerPrice().getInBoundExternalCharges().addAll(inBoundPaxFlexiChargeMap.values());
		paxPriceInfo.getPassengerPrice().setTotalInBoundExternalCharges(inBoundPaxFlexiChargeTotal);
	}

	private static BigDecimal getPaxEffectiveCharge(QuotedChargeDTO quotedChargeDTO, String paxType) {
		BigDecimal chargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		// Zaki : This is to fix an issue caused by retrieving certain charges which are saved
		// as percentages within the database. These charges once multiplied give double values
		// with improper precisions. One example is TAX code [GT] which once retrieved gives
		// [12.4500000000001] rather than [12.45].
		if (quotedChargeDTO.getEffectiveChargeAmount(paxType) != null) {
			chargeAmount = AccelAeroCalculator.parseBigDecimal(quotedChargeDTO.getEffectiveChargeAmount(paxType));
		}

		return chargeAmount;
	}

	private static ExternalCharge createExternalChargeTO(String carrierCode, String ondCode, Date date,
			FlexiRuleDTO flexiRuleDTO, String paxType) {
		Collection<FlexiInfo> flexiInfos = new ArrayList<FlexiInfo>();
		for (Iterator<FlexiRuleFlexibilityDTO> flexiRuleIterator = flexiRuleDTO.getAvailableFlexibilities().iterator(); flexiRuleIterator
				.hasNext();) {
			FlexiRuleFlexibilityDTO flexiRuleFlexibilityDTO = flexiRuleIterator.next();
			FlexiInfo flexiInfo = new FlexiInfo();
			flexiInfo.setFlexiRateId(flexiRuleDTO.getFlexiRuleRateId());
			flexiInfo.setAvailableCount(flexiRuleFlexibilityDTO.getAvailableCount());
			flexiInfo.setCutOverBufferInMins(flexiRuleFlexibilityDTO.getCutOverBufferInMins());
			flexiInfo.setFlexibilityTypeId(flexiRuleFlexibilityDTO.getFlexibilityTypeId());
			flexiInfos.add(flexiInfo);
		}
		ExternalCharge externalCharge = new ExternalCharge();
		externalCharge.setAmount(AccelAeroCalculator.parseBigDecimal(flexiRuleDTO.getEffectiveChargeAmount(paxType)));
		externalCharge.setCarrierCode(carrierCode);
		externalCharge.setSegmentCode(ondCode);
		externalCharge.setCode(flexiRuleDTO.getChargeCode());
		externalCharge.setName(flexiRuleDTO.getDescription());
		externalCharge.setApplicableDateTime(date);
		externalCharge.getApplicablePassengerType().add(PaxTypeUtils.convertAAPaxCodeToLCCPaxCodes(paxType));
		externalCharge.setType(ExternalChargeType.FLEXI_CHARGES);
		externalCharge.getAdditionalDetails().addAll(flexiInfos);
		// externalCharge.setComments(flexiRuleDTO.getRuleComments());
		return externalCharge;
	}

	public static Collection<FareRule> getFareRules(Collection<OndFareDTO> ondFareDTOList, FareRuleProxy fareRuleProxy)
			throws ModuleException {
		Collection<FareRule> fareRules = new ArrayList<FareRule>();
		Map<String, String> bcToCoSMap = new HashMap<String, String>();
		FareRule tmpFareRule = null;
		for (OndFareDTO ondFareDTO : ondFareDTOList) {
			fillBcToCoSMap(bcToCoSMap, ondFareDTO.getSegmentSeatDistsDTO());
			tmpFareRule = populateFareRule(ondFareDTO.getFareSummaryDTO(), bcToCoSMap, fareRuleProxy);
			tmpFareRule.setOndSequence(ondFareDTO.getOndSequence());
			try {
				tmpFareRule.setSegmentCode(ondFareDTO.getOndCode());
			} catch (ModuleException e) {
			}
			fareRules.add(tmpFareRule);
		}
		return fareRules;
	}

	private static void fillBcToCoSMap(Map<String, String> bcToCoSMap, Collection<SegmentSeatDistsDTO> seatDistColl) {

		for (SegmentSeatDistsDTO seatDist : seatDistColl) {
			if (!bcToCoSMap.containsKey(seatDist.getBookingCode())) {
				bcToCoSMap.put(seatDist.getBookingCode(), seatDist.getCabinClassCode());
			}
		}
	}

	public static FareRule populateFareRule(FareSummaryDTO fareSummaryDTO, Map<String, String> bcToCoSMap,
			FareRuleProxy fareRuleProxy) throws ModuleException {

		FareRule fareRule = new FareRule();
		fareRule.setOperatingCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
		fareRule.setCabinClassCode(bcToCoSMap.get(fareSummaryDTO.getBookingClassCode()));
		fareRule.setBookingClassCode(fareSummaryDTO.getBookingClassCode());
		fareRule.setFareRuleId(fareSummaryDTO.getFareRuleID());
		fareRule.setComments(fareSummaryDTO.getFareRuleComment());
		fareRule.setFareRuleCode(fareSummaryDTO.getFareRuleCode());
		fareRule.setDescription(fareRuleProxy.getFareRule(fareSummaryDTO.getFareRuleID()).getFareRuleDescription());
		fareRule.setFareBasisCode(fareSummaryDTO.getFareBasisCode());
		fareRule.setFareCategoryCode(fareSummaryDTO.getFareCategoryCode());
		fareRule.setSegmentCode(fareSummaryDTO.getOndCode());

		FareCharacteristics adultFare = new FareCharacteristics();
		adultFare.setApplicable(fareSummaryDTO.isAdultApplicability());
		adultFare.setRefundable(fareSummaryDTO.isAdultFareRefundable());
		adultFare.setFareAmount(new BigDecimal(fareSummaryDTO.getFareAmount(PaxTypeTO.ADULT)));
		fareRule.setAdultFareCharacteristics(adultFare);

		FareCharacteristics childFare = new FareCharacteristics();
		childFare.setApplicable(fareSummaryDTO.isChildApplicability());
		childFare.setRefundable(fareSummaryDTO.isChildFareRefundable());
		childFare.setFareAmount(new BigDecimal(fareSummaryDTO.getFareAmount(PaxTypeTO.CHILD)));
		childFare.setFareType(fareSummaryDTO.getChildFareType());
		fareRule.setChildFareCharacteristics(childFare);

		FareCharacteristics infantFare = new FareCharacteristics();
		infantFare.setApplicable(fareSummaryDTO.isInfantApplicability());
		infantFare.setRefundable(fareSummaryDTO.isInfantFareRefundable());
		infantFare.setFareAmount(new BigDecimal(fareSummaryDTO.getFareAmount(PaxTypeTO.INFANT)));
		infantFare.setFareType(fareSummaryDTO.getInfantFareType());
		fareRule.setInfantFareCharacteristics(infantFare);
		return fareRule;
	}

}
