package com.isa.thinair.aaservices.core.bl.dummycarrierfulfillment;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AirReservation;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ContactInfo;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAErrorCode;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAPOS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAPayCarrierFulfullmentRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAReservation;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.AAUtils;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Handle modify contact detail changers for dummy reservation
 * 
 * @author malaka
 * 
 */
class AAFulfillModifyContact extends BaseMarketingCarrierFulfillment {

	private static Log log = LogFactory.getLog(AAFulfillModifyContact.class);

	AAFulfillModifyContact(AAReservation aaAirReservation, AAReservation newAAAirReservation, AAPOS aaPos) {
		super(aaAirReservation, newAAAirReservation, aaPos);
	}

	@Override
	public AAPayCarrierFulfullmentRS execute() throws WebservicesException, ModuleException {

		AAPayCarrierFulfullmentRS carrierFulfillment = new AAPayCarrierFulfullmentRS();
		carrierFulfillment.setResponseAttributes(new AAResponseAttributes());

		checkConstraints();

		try {

			if (log.isDebugEnabled()) {
				log.debug("AAFulfillModifyContact::begin for pnr [" + aaAirReservation.getGroupPnr() + "]");
			}

			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setGroupPNR(aaAirReservation.getGroupPnr());
			Reservation reservation = getCarrierReservation(pnrModesDTO);

			if (reservation == null) {
				throw new WebservicesException(AAErrorCode.ERR_25_MAXICO_REQUIRED_THE_RESERVATION_NO_LONGER_EXIST,
						"Reservation does not exits");
			}

			ReservationContactInfo reservationContactInfo = reservation.getContactInfo();
			// Update fields in reservation's contact info
			ContactInfo contactInfo = aaAirReservation.getAirReservation().getContactInfo();
			AAUtils.updateReservationContactInfo(contactInfo, reservationContactInfo);

			TrackInfoDTO trackInfo = AAUtils.getTrackInfo(aaPos);

			// Update reservation contact info
			ServiceResponce updateContactInfo = AAServicesModuleUtils.getReservationBD().updateDummyReservationContactInfo(
					reservation.getPnr(), reservation.getVersion(), reservationContactInfo, trackInfo);
			Long version = (Long) updateContactInfo.getResponseParam(CommandParamNames.VERSION);

			if (updateContactInfo.isSuccess()) {

				if (log.isDebugEnabled()) {
					log.debug("AAFulfillModifyContact::end");
				}

				carrierFulfillment.getResponseAttributes().setSuccess(new AASuccess());
				carrierFulfillment.setResVersion(version.toString());
			}

		} catch (ModuleException e) {
			log.error("dummy reservation AAFulfillModifyContact failed", e);
			throw new ModuleException("aaservices.paycarrier.fulfillment.failed", e);
		}

		return carrierFulfillment;
	}

	@Override
	public void checkConstraints() throws WebservicesException {
		AirReservation airReservation = aaAirReservation.getAirReservation();

		if (airReservation.getContactInfo() == null) {
			throw new WebservicesException(AAErrorCode.ERR_1_MAXICO_REQUIRED_INVALID_VALUE, "Contact detail not found");
		}

	}

}
