package com.isa.thinair.aaservices.core.bl.alerting;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AlertDetail;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAddAlertRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAddAlertsRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.alerting.api.model.Alert;
import com.isa.thinair.commons.core.util.CalendarUtil;

public class AAAddAlertsBL {
	private static Log log = LogFactory.getLog(AAAddAlertsBL.class);
	private AAAddAlertsRQ aaAddAlertsRQ;
	private AAAddAlertRS aaAddAlertsRS;

	public void setRequestParams(AAAddAlertsRQ aaAddAlertRQ) {
		this.aaAddAlertsRQ = aaAddAlertRQ;
	}

	// Initializes the response variable.
	private void initializeAAAddAlertsRS() {
		aaAddAlertsRS = new AAAddAlertRS();
		aaAddAlertsRS.setResponseAttributes(new AAResponseAttributes());
	}

	public AAAddAlertRS execute() {
		initializeAAAddAlertsRS();
		try {
			if (aaAddAlertsRQ.getAlerts() != null) {
				List<Alert> alertList = new ArrayList<Alert>();
				for (AlertDetail alertDTO : aaAddAlertsRQ.getAlerts()) {
					Alert alert = new Alert();
					alert.setAlertTypeId(alertDTO.getAlertTypeId());
					alert.setContent(alertDTO.getContent());
					alert.setPnrSegId(alertDTO.getPnrSegId());
					alert.setPriorityCode(alertDTO.getPriorityCode());
					alert.setOriginalDepDate(alertDTO.getDepDate());
					alert.setTimestamp(CalendarUtil.getCurrentZuluDateTime());
					alertList.add(alert);
					AAServicesModuleUtils.getAlertingBD().addAlerts(alertList);
				}

			}
		} catch (Exception ex) {
			log.error("Error occurred during retrieval of alerts for given search critirea", ex);
			AAExceptionUtil.addAAErrrors(aaAddAlertsRS.getResponseAttributes().getErrors(), ex);

		} finally {
			int numOfErrors = aaAddAlertsRS.getResponseAttributes().getErrors().size();
			AASuccess successType = (numOfErrors == 0 ? new AASuccess() : null);
			aaAddAlertsRS.getResponseAttributes().setSuccess(successType);
		}

		return aaAddAlertsRS;
	}
}
