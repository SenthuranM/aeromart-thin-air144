package com.isa.thinair.aaservices.core.bl.ancillary;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.StringStringListMap;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAirBookRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAnciModifyRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAReadRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.thinair.aaservices.core.bl.reservation.AAAutoCancellationBL;
import com.isa.thinair.aaservices.core.bl.reservation.AASearchReservationByPnrBL;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAReservationUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.AAUtils;
import com.isa.thinair.aaservices.core.util.TransformerUtil;
import com.isa.thinair.airproxy.api.model.reservation.core.CommonAncillaryModifyAssembler;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class AAAnciModificationBL {

	private static Log log = LogFactory.getLog(AAAnciModificationBL.class);

	private AAAnciModifyRQ aaAnciModifyRQ;

	public AAAirBookRS execute() {
		AAAirBookRS aaAirBookRS = new AAAirBookRS();
		aaAirBookRS.setResponseAttributes(new AAResponseAttributes());

		log.debug("Anci modification in aaservices in " + AppSysParamsUtil.getDefaultCarrierCode() + " "
				+ aaAnciModifyRQ.getCommonAnciModifyAssembler().getPnr());

		CommonAncillaryModifyAssembler commonAncillaryModifyAssembler = TransformerUtil.transformAnciAssembler(aaAnciModifyRQ
				.getCommonAnciModifyAssembler());
		commonAncillaryModifyAssembler.setSkipCutoverValidation(aaAnciModifyRQ.isSkipCutoverCheck());
		aaAnciModifyRQ.getHeaderInfo();

		TrackInfoDTO trackInfoDTO = null;
		if (aaAnciModifyRQ.getTrackingInfo() == null) {
			trackInfoDTO = AAUtils.getTrackInfo(aaAnciModifyRQ.getAaPos());
		} else {
			trackInfoDTO = TransformerUtil.transformTrackInfo(aaAnciModifyRQ.getTrackingInfo());
		}

		String pnr = commonAncillaryModifyAssembler.getPnr();

		Map<String, Set<String>> flightSegIdWiseSelectedMeals = getFlightRefWiseSelectedMeals(aaAnciModifyRQ
				.getCommonAnciModifyAssembler().getFlightRefWiseSelectedMeals());

		if (aaAnciModifyRQ.getAutoCancellationInfo() != null) {
			commonAncillaryModifyAssembler.setAutoCancellationInfo(AAAutoCancellationBL.populateAutoCancellation(aaAnciModifyRQ
					.getAutoCancellationInfo()));
			commonAncillaryModifyAssembler.setAutoCnxEnabled(true);
			commonAncillaryModifyAssembler.setSkipOwnAutoCnxCalculation(true);
		}

		try {
			AAServicesModuleUtils.getAirproxyReservationBD().updateAncillary(commonAncillaryModifyAssembler,
					aaAnciModifyRQ.isFraudCheckEnbaled(), null, false, flightSegIdWiseSelectedMeals, trackInfoDTO);

			AAReadRQ aaReadRQ = new AAReadRQ();
			aaReadRQ.setPnr(pnr);
			aaReadRQ.setHeaderInfo(AAUtils.populateHeaderInfo(null));
			aaReadRQ.setLoadReservationOptions(AAReservationUtil.createLoadReservationOptions());

			AASearchReservationByPnrBL aaSearchReservationByPnrBL = new AASearchReservationByPnrBL();
			aaSearchReservationByPnrBL.setRequest(aaReadRQ);

			aaAirBookRS = aaSearchReservationByPnrBL.execute();
		} catch (Exception e) {
			log.error("ERROR in Anci Modification ", e);
			AAExceptionUtil.addAAErrrors(aaAirBookRS.getResponseAttributes().getErrors(), e);
		}

		return aaAirBookRS;
	}

	public void setAAAnciModifyRQ(AAAnciModifyRQ aaAnciModifyRQ) {
		this.aaAnciModifyRQ = aaAnciModifyRQ;
	}

	private static Map<String, Set<String>> getFlightRefWiseSelectedMeals(List<StringStringListMap> selectedMeals) {
		Map<String, Set<String>> flightSegIdWiseMap = null;

		if (selectedMeals != null) {
			flightSegIdWiseMap = new HashMap<String, Set<String>>();
			for (StringStringListMap mapObj : selectedMeals) {
				if (flightSegIdWiseMap.get(mapObj.getKey()) == null) {
					flightSegIdWiseMap.put(mapObj.getKey(), new HashSet<String>());
				}

				if (mapObj.getValue() != null) {
					flightSegIdWiseMap.get(mapObj.getKey()).addAll(mapObj.getValue());
				}

			}
		}

		return flightSegIdWiseMap;
	}
}
