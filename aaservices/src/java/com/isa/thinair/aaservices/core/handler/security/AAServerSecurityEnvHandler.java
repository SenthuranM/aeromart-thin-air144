/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2009 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * ===============================================================================
 */
package com.isa.thinair.aaservices.core.handler.security;

import java.io.IOException;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.aaservices.core.session.AASessionManager;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.login.util.ForceLoginInvoker;
import com.sun.xml.wss.impl.callback.PasswordValidationCallback;
import com.sun.xml.wss.impl.callback.TimestampValidationCallback;

/**
 * @author Nilindra Fernando
 */
class AAServerSecurityEnvHandler implements CallbackHandler {

	private static final Log log = LogFactory.getLog(AAServerSecurityEnvHandler.class);

	public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {
		for (int i = 0; i < callbacks.length; i++) {
			if (callbacks[i] instanceof PasswordValidationCallback) {
				PasswordValidationCallback cb = (PasswordValidationCallback) callbacks[i];
				if (cb.getRequest() instanceof PasswordValidationCallback.PlainTextPasswordRequest) {
					cb.setValidator(new PlainTextPasswordValidator());
				} else {
					throw new UnsupportedCallbackException(null, "Unsupported Callback Type Encountered");
				}
			} else if (callbacks[i] instanceof TimestampValidationCallback) {
				TimestampValidationCallback cb = (TimestampValidationCallback) callbacks[i];
				cb.setValidator(new DefaultTimestampValidator());
			} else {
				throw new UnsupportedCallbackException(null, "Unsupported Callback Type Encountered");
			}
		}
	}

	private class PlainTextPasswordValidator implements PasswordValidationCallback.PasswordValidator {
		public boolean validate(PasswordValidationCallback.Request request)
				throws PasswordValidationCallback.PasswordValidationException {
			PasswordValidationCallback.PlainTextPasswordRequest plainTextRequest = (PasswordValidationCallback.PlainTextPasswordRequest) request;

			// TODO Assumped currently only LCC connect will access the LCC
			// This needs to be improved with proper support for multiple channels
			int salesChannelId = SalesChannelsUtil.SALES_CHANNEL_LCC;

			try {
				String userName = plainTextRequest.getUsername();
				String password = plainTextRequest.getPassword();
				ForceLoginInvoker.webserviceLogin(userName, password, salesChannelId);
				// FIXME pass the client session id
				AASessionManager.SessionInHandler.handleSessionIn(userName, password, salesChannelId, null, false);
				return true;
			} catch (Exception e) {
				log.error("Error ", e);
				return false;
			}
		}
	}

	private class DefaultTimestampValidator implements TimestampValidationCallback.TimestampValidator {
		public void validate(TimestampValidationCallback.Request request)
				throws TimestampValidationCallback.TimestampValidationException {
			// TODO Currently Not Validating the TimeStamps
		}
	}
}