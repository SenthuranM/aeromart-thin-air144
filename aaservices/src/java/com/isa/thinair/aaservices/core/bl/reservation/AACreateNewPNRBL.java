package com.isa.thinair.aaservices.core.bl.reservation;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.required.dto.AAGenerateNewPNRRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;

/**
 * 
 * @author sanjaya
 * 
 */
public class AACreateNewPNRBL {

	/* Logger */
	private static Log log = LogFactory.getLog(AACreateNewPNRBL.class);

	/* New PNR Response */
	private AAGenerateNewPNRRS aAGenerateNewPNRRS;

	// Initializes the response variable.
	private void initializeAAGenerateNewPNRRS() {
		aAGenerateNewPNRRS = new AAGenerateNewPNRRS();
		aAGenerateNewPNRRS.setResponseAttributes(new AAResponseAttributes());
	}

	/**
	 * @return The new PNR Response
	 */
	public AAGenerateNewPNRRS excute() {

		initializeAAGenerateNewPNRRS();

		try {
			aAGenerateNewPNRRS.setNewPNR(AAServicesModuleUtils.getReservationBD().getNewPNR(false, null));
			return this.aAGenerateNewPNRRS;

		} catch (Exception ex) {
			log.error(ex);
			AAExceptionUtil.addAAErrrors(aAGenerateNewPNRRS.getResponseAttributes().getErrors(), ex);

		} finally {
			int numOfErrors = aAGenerateNewPNRRS.getResponseAttributes().getErrors().size();
			AASuccess successType = (numOfErrors == 0 ? new AASuccess() : null);
			aAGenerateNewPNRRS.getResponseAttributes().setSuccess(successType);
		}

		return aAGenerateNewPNRRS;
	}

}
