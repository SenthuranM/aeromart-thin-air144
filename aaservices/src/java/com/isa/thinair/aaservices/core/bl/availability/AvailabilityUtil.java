package com.isa.thinair.aaservices.core.bl.availability;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AdditionalFareInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AirTravelerAvail;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingClassAllocation;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FareSegmentChargeTO;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FareTypeCategory;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentBCAllocation;
import com.isa.connectivity.profiles.maxico.v1.common.dto.InterlineAirlineFareInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.LCCServiceTaxContainer;
import com.isa.connectivity.profiles.maxico.v1.common.dto.LogicalCabinSeatAvailability;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OndFareSegmentChargeTO;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OriginDestinationInformation;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OriginDestinationOption;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OriginDestinationOptions;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PassengerType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PassengerTypeQuantity;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PerPaxJourneyFare;
import com.isa.connectivity.profiles.maxico.v1.common.dto.StringIntegerMap;
import com.isa.thinair.aaservices.core.session.AASessionManager;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.AAUtils;
import com.isa.thinair.aaservices.core.util.PaxTypeUtils;
import com.isa.thinair.aaservices.core.util.TransformerUtil;
import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareLiteDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableBCAllocationSummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableConnectedFlight;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableIBOBFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.BookingClassAlloc;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareSummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareTypes;
import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.util.FareQuoteUtil;
import com.isa.thinair.airpricing.api.dto.FlexiRuleDTO;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.OndFareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ServiceTaxContainer;
import com.isa.thinair.airproxy.api.utils.converters.OndConvertUtil;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.dto.LogicalCabinClassDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.webplatform.api.util.PrivilegeUtil;

public class AvailabilityUtil {

	static void populateOriginDestinationInformation(OriginDestinationInformation ondInfo, String originLocation,
			String destinationLocation, Date depatureDateTimeStart, Date depatureDateTimeEnd) {

		ondInfo.setOriginDestinationOptions(new OriginDestinationOptions());
		ondInfo.setOriginDestinationSegFareOptions(new OriginDestinationOptions());
		ondInfo.setOriginLocation(originLocation);
		ondInfo.setDestinationLocation(destinationLocation);
		ondInfo.setDepartureDateTimeStart(depatureDateTimeStart);
		ondInfo.setDepartureDateTimeEnd(depatureDateTimeEnd);
	}

	static void populateFlightSegments(OriginDestinationInformation ondInfo,
			Collection<AvailableIBOBFlightSegment> availableFlightSegments, AvailableIBOBFlightSegment selectedMinimumFareFlight,
			AvailabilityDTOCreator availabilityDTOCretor, boolean isFromFareQuote, boolean setToSegFareOption,
			boolean ondFlexiSelected) throws ModuleException {

		Map<String, LogicalCabinClassDTO> cachedLogicalCCMap = CommonsServices.getGlobalConfig().getAvailableLogicalCCMap();
		for (AvailableIBOBFlightSegment availableFlight : availableFlightSegments) {

			boolean isSelectedFlight = false;

			if (availableFlight.isDirectFlight()) {
				AvailableFlightSegment availableFlightSegment = (AvailableFlightSegment) availableFlight;

				// minimum selected flight found
				if (selectedMinimumFareFlight != null && availableFlightSegment.equals(selectedMinimumFareFlight)) {
					availableFlightSegment = (AvailableFlightSegment) selectedMinimumFareFlight;
					isSelectedFlight = true;
				}

				List<AvailableIBOBFlightSegment> flights = new ArrayList<AvailableIBOBFlightSegment>();
				flights.add(availableFlightSegment);

				OriginDestinationOption ondOption = new OriginDestinationOption();
				populateFlightSegment(ondOption, availableFlightSegment, availabilityDTOCretor);
				// Set available interline booking classes
				Collection<AvailableFlightSegment> segmentsList = new ArrayList<AvailableFlightSegment>();
				segmentsList.add(availableFlightSegment);
				List<InterlineAirlineFareInfo> interlineAirlineFareInfo = populateInterlineFareInfo(segmentsList,
						cachedLogicalCCMap);
				ondOption.getAvailableInterlineAirlineFare().addAll(interlineAirlineFareInfo);

				// Seat Availability
				ondOption.setSeatAvailable(true);
				if (!availableFlightSegment.getAvailableLogicalCabinClasses().isEmpty()) {

					for (String availableLogicalCabinClass : availableFlightSegment.getAvailableLogicalCabinClasses()) {
						LogicalCabinClassDTO logicalCabinClassDTO = cachedLogicalCCMap.get(availableLogicalCabinClass);
						populateAirlineFareInfo(ondOption, flights, availabilityDTOCretor, logicalCabinClassDTO, isSelectedFlight,
								isFromFareQuote, ondFlexiSelected);
					}
				}

				OriginDestinationOptions ondOptions = setToSegFareOption
						? ondInfo.getOriginDestinationSegFareOptions()
						: ondInfo.getOriginDestinationOptions();
				ondOptions.getOriginDestinationOption().add(ondOption);

				ondInfo.setPreferredCabinClass(availableFlightSegment.getCabinClassCode());
				ondInfo.setPreferredLogicalCabin(availableFlightSegment.getSelectedLogicalCabinClass());

			} else {
				AvailableConnectedFlight availableConnectedFlight = (AvailableConnectedFlight) availableFlight;

				if (selectedMinimumFareFlight != null && availableConnectedFlight.equals(selectedMinimumFareFlight)) {
					availableConnectedFlight = (AvailableConnectedFlight) selectedMinimumFareFlight;
					isSelectedFlight = true;
				}

				OriginDestinationOption ondOption = new OriginDestinationOption();
				Collection<AvailableFlightSegment> segmentsForConnection = availableConnectedFlight.getAvailableFlightSegments();

				List<AvailableIBOBFlightSegment> flights = new ArrayList<AvailableIBOBFlightSegment>();

				for (AvailableFlightSegment availableFlightSegment : segmentsForConnection) {
					populateFlightSegment(ondOption, availableFlightSegment, availabilityDTOCretor);
					ondInfo.setPreferredCabinClass(availableFlightSegment.getCabinClassCode());
					ondInfo.setPreferredLogicalCabin(availableFlightSegment.getSelectedLogicalCabinClass());
				}

				List<InterlineAirlineFareInfo> interlineAirlineFareInfo = populateInterlineFareInfo(segmentsForConnection,
						cachedLogicalCCMap);
				ondOption.getAvailableInterlineAirlineFare().addAll(interlineAirlineFareInfo);

				if (!availableConnectedFlight.getAvailableLogicalCabinClasses().isEmpty()) {

					flights.add(availableConnectedFlight);

					for (String availableLogicalCabinClass : availableConnectedFlight.getAvailableLogicalCabinClasses()) {
						LogicalCabinClassDTO logicalCabinClassDTO = cachedLogicalCCMap.get(availableLogicalCabinClass);
						populateAirlineFareInfo(ondOption, flights, availabilityDTOCretor, logicalCabinClassDTO, isSelectedFlight,
								isFromFareQuote, ondFlexiSelected);
					}
				}
				// Seat Availability
				ondOption.setSeatAvailable(true);

				OriginDestinationOptions ondOptions = setToSegFareOption
						? ondInfo.getOriginDestinationSegFareOptions()
						: ondInfo.getOriginDestinationOptions();
				ondOptions.getOriginDestinationOption().add(ondOption);
			}

		}
	}

	/**
	 * @param ondOption
	 * @param availabilityDTOCretor
	 * @param isSelectedFlight
	 * @param isFromFareQuote
	 * @throws ModuleException
	 */
	private static void populateAirlineFareInfo(OriginDestinationOption ondOption,
			List<AvailableIBOBFlightSegment> availableFlights, AvailabilityDTOCreator availabilityDTOCretor,
			LogicalCabinClassDTO logicalCabinClassDTO, boolean isSelectedFlight, boolean isFromFareQuote,
			boolean ondFlexiSelected) throws ModuleException {
		if (!availabilityDTOCretor.getAvailableSearchDTO().isSearchOnlyInterlineFlights()) {
			String selectedLanguage = availabilityDTOCretor.getAvailableSearchDTO().getPreferredLanguage();
			for (AvailableIBOBFlightSegment availableFlight : availableFlights) {

				Collection<OndFareDTO> colOndFareDTOForLogicalCC = availableFlight
						.getFareChargesSeatsSegmentsDTOs(logicalCabinClassDTO.getLogicalCCCode());

				if (colOndFareDTOForLogicalCC == null) {
					continue;
				}

				FareTypeCategory fareTypeCategory = getFareTypeCategory(colOndFareDTOForLogicalCC);

				/*
				 * With new flexi implementation, flexi will be offered as a new logical cabin class in the front-end.
				 * But, in the back-end same logical cabin class will be used with a flexi flag. So, before sending
				 * available logical CC & fares to the front-end, dummy logical cabin class will populated based on the
				 * requirement.
				 * 
				 * Here, if the flexi is defined as a FOC service for a particular logical cc, only the 'logical cc with
				 * flexi' fare details will be added. Otherwise separate fare details will be added for 'logical cc with
				 * flexi'
				 */
				boolean freeFlexiEnabled = logicalCabinClassDTO.isFreeFlexiEnabled();
				boolean hasFlexi = FareQuoteUtil.isFlexiAvailable(colOndFareDTOForLogicalCC);

				FareSummaryDTO fareDTO = getFareSummaryDTO(availableFlight, logicalCabinClassDTO.getLogicalCCCode());

				BigDecimal totalFare = BigDecimal.valueOf(fareDTO.getTotalFare(availabilityDTOCretor.getAdultCount(),
						availabilityDTOCretor.getChildCount(), availabilityDTOCretor.getInfantCount()));

				BigDecimal adultFare = BigDecimal.valueOf(fareDTO.getAdultFare());

				BigDecimal adultChargesAndTaxTotal = BigDecimal.ZERO;
				BigDecimal totalChargesAndTax = BigDecimal.ZERO;
				BigDecimal totalAdultFareWithChargesAndWithoutServiceTaxes = new BigDecimal(
						fareDTO.getRatioAdjustedTotalPriceWithoutServiceTaxes(1, 0, 0));
				adultChargesAndTaxTotal = AccelAeroCalculator.subtract(totalAdultFareWithChargesAndWithoutServiceTaxes, adultFare);
				BigDecimal serviceTaxAmountForAdult = AccelAeroCalculator.subtract(new BigDecimal(fareDTO.
								getRatioAdjustedTotalPrice(1, 0, 0)) , totalAdultFareWithChargesAndWithoutServiceTaxes);

				BigDecimal totalFareWithCharges = new BigDecimal(
						fareDTO.getRatioAdjustedTotalPrice(availabilityDTOCretor.getAdultCount(),
								availabilityDTOCretor.getChildCount(), availabilityDTOCretor.getInfantCount()));
				totalChargesAndTax = AccelAeroCalculator.subtract(totalFareWithCharges, totalFare);
				// }
				if (freeFlexiEnabled) {
					createDummyFareSummaryObjectForFlexi(ondOption, fareTypeCategory, availableFlight, adultFare, totalFare,
							adultChargesAndTaxTotal, totalChargesAndTax, logicalCabinClassDTO, freeFlexiEnabled, isSelectedFlight,
							isFromFareQuote, fareDTO, ondFlexiSelected, selectedLanguage);
				} else {
					if (hasFlexi && AppSysParamsUtil.isDisplayFlexiAsSeparteBundle()) {
						createDummyFareSummaryObjectForFlexi(ondOption, fareTypeCategory, availableFlight, adultFare, totalFare,
								adultChargesAndTaxTotal, totalChargesAndTax, logicalCabinClassDTO, freeFlexiEnabled,
								isSelectedFlight, isFromFareQuote, fareDTO, ondFlexiSelected, selectedLanguage);
					}

					AdditionalFareInfo fareInfo = getFareInfoDetail(ondOption.getAdditionalFareInfo(), fareTypeCategory,
							logicalCabinClassDTO.getLogicalCCCode(), false);

					if (fareInfo == null) {
						fareInfo = new AdditionalFareInfo();
						fareInfo.setFareTypeCategory(fareTypeCategory);
						fareInfo.setLogicalCabinClass(logicalCabinClassDTO.getLogicalCCCode());
						fareInfo.setLogicalCabinClassDescription(logicalCabinClassDTO.getDescription());
						fareInfo.setComment(logicalCabinClassDTO.getComment());

						if (isSelectedFlight
								&& logicalCabinClassDTO.getLogicalCCCode().equals(availableFlight.getSelectedLogicalCabinClass())
								&& availableFlight.getSelectedBundledFare() == null && !ondFlexiSelected) {
							fareInfo.setSelected(true);
						} else {
							fareInfo.setSelected(false);
						}

						fareInfo.setNestRank(logicalCabinClassDTO.getNestRank());
						fareInfo.setSeatAvailable(availableFlight.isSeatsAvailable(logicalCabinClassDTO.getLogicalCCCode()));
						fareInfo.setAdultFare(adultFare);
						fareInfo.setServiceTaxAmountForAdult(serviceTaxAmountForAdult);
						fareInfo.setServiceTaxPercentageForFares(OndConvertUtil.getServiceTaxPercentageForFares(availableFlight));
						fareInfo.setTotalFare(totalFare);
						fareInfo.setAdultCharges(adultChargesAndTaxTotal);
						fareInfo.setTotalCharges(totalChargesAndTax);
						fareInfo.setFlexiAvailable(hasFlexi);
						fareInfo.setWithFlexi(false);
						fareInfo.setIsFreeFlexi(false);
						fareInfo.setImageUrl(OndConvertUtil.generateLogicalCCImageUrl(logicalCabinClassDTO.getLogicalCCCode(),
								false, selectedLanguage));
						fareInfo.setBundleFareDescriptionInfo(TransformerUtil.transform(logicalCabinClassDTO.getBundleFareDescriptionTemplateDTO()));
						
						if (((FareSummaryDTO) fareDTO).getVisibleChannelNames() != null
								|| (((FareSummaryDTO) fareDTO).getVisibleChannelNames()).size() > 0) {
							fareInfo.setVisibleChannelName(
									(String) ((List) ((FareSummaryDTO) fareDTO).getVisibleChannelNames()).get(0));
						}
						if (!availableFlight.getMinimumFareByPriceOnd().isEmpty()) {
							fareInfo.getMinimumFareByPriceOndType().addAll(TransformerUtil
									.transformFarePriceOndToStringDecimalMap(availableFlight.getMinimumFareByPriceOnd()));
						}
						ondOption.getAdditionalFareInfo().add(fareInfo);
					} else {
						fareInfo.setAdultFare(AccelAeroCalculator.add(adultFare, fareInfo.getAdultFare()));
						fareInfo.setTotalFare(AccelAeroCalculator.add(totalChargesAndTax, fareInfo.getTotalCharges()));
						fareInfo.setAdultCharges(AccelAeroCalculator.add(adultChargesAndTaxTotal, fareInfo.getAdultCharges()));
						if (((FareSummaryDTO) fareDTO).getVisibleChannelNames() != null
								|| (((FareSummaryDTO) fareDTO).getVisibleChannelNames()).size() > 0) {
							fareInfo.setVisibleChannelName(
									(String) ((List) ((FareSummaryDTO) fareDTO).getVisibleChannelNames()).get(0));
						}
						if (!availableFlight.getMinimumFareByPriceOnd().isEmpty()) {
							fareInfo.getMinimumFareByPriceOndType().addAll(TransformerUtil
									.transformFarePriceOndToStringDecimalMap(availableFlight.getMinimumFareByPriceOnd()));
						}
					}

					// Booking class wise allocations
					int numberOfBCWiseAvailableSeats = fareDTO.getNoOfAvailableSeats();

					// Flights seat allocation (in case of connections first
					// flights availability will be taken)
					int numberOfAllocatedAvailableSeats = availableFlight.getFlightSegmentDTOs().get(0).getAvailableAdultCount()
							.get(logicalCabinClassDTO.getLogicalCCCode());

					/*
					 * If Booking class wise allocation is less than allocated available seats in the flight it is taken
					 * otherwise, actual allocated number of seats are taken. (lesser of the two)
					 */
					int numberOfAvailableSeats = numberOfAllocatedAvailableSeats < numberOfBCWiseAvailableSeats
							? numberOfAllocatedAvailableSeats
							: numberOfBCWiseAvailableSeats;
					fareInfo.setAvailableNoOfSeats(numberOfAvailableSeats);
				}

				addBundledFaresToLogicalCabinClasses(ondOption, availableFlight, logicalCabinClassDTO, fareTypeCategory, fareDTO,
						availabilityDTOCretor);

				if (availableFlight.isFlown() && availableFlight.getSelectedBundledFare() != null) {
					retainOnlyBundledServiceOption(ondOption.getAdditionalFareInfo(),
							availableFlight.getSelectedBundledFare().getBundledFarePeriodId());
				}
			}
		}
	}

	private static void retainOnlyBundledServiceOption(List<AdditionalFareInfo> additionalFareInfos, Integer bundledServicePeriodId) {
		if (bundledServicePeriodId != null) {
			Collection<AdditionalFareInfo> retainElements = new ArrayList<AdditionalFareInfo>();
			for (AdditionalFareInfo fareInfo : additionalFareInfos) {
				if (bundledServicePeriodId.equals(fareInfo.getBundledFarePeriodId())) {
					retainElements.add(fareInfo);
				}
			}

			if (!retainElements.isEmpty()) {
				additionalFareInfos.retainAll(retainElements);
				if (!additionalFareInfos.isEmpty()) {
					additionalFareInfos.iterator().next().setSelected(true);
				}
			}
		}
	}

	private static void addBundledFaresToLogicalCabinClasses(OriginDestinationOption ondOption,
			AvailableIBOBFlightSegment availableIBOBFlightSegment, LogicalCabinClassDTO logicalCabinClassDTO,
			FareTypeCategory fareTypeCategory, FareSummaryDTO fareDTO, AvailabilityDTOCreator availabilityDTOCretor)
			throws ModuleException {
		Map<String, List<BundledFareLiteDTO>> availableBundledFares = availableIBOBFlightSegment.getAvailableBundledFares();

		List<AdditionalFareInfo> additionalFareInfos = ondOption.getAdditionalFareInfo();

		int lastRank = 0;
		if (additionalFareInfos != null && !additionalFareInfos.isEmpty()) {
			AdditionalFareInfo lastElement = additionalFareInfos.get(additionalFareInfos.size() - 1);
			lastRank = lastElement.getNestRank();
		}
		String logicalCabinclass = logicalCabinClassDTO.getLogicalCCCode();
		BigDecimal availableFlightTotalFlexi = calculateTotalFlexiCharge(availableIBOBFlightSegment, logicalCabinclass);

		if (availableBundledFares != null
				&& availableBundledFares.containsKey(availableIBOBFlightSegment.getSelectedLogicalCabinClass())
				&& !availableIBOBFlightSegment.isFlown()) {

			List<BundledFareLiteDTO> bundledFareLiteDTOs = availableBundledFares.get(logicalCabinclass);

			if (bundledFareLiteDTOs != null && !bundledFareLiteDTOs.isEmpty()) {

				BundledFareDTO bundledFareDTO = availableIBOBFlightSegment.getSelectedBundledFare();

				for (BundledFareLiteDTO bundledFareLiteDTO : bundledFareLiteDTOs) {
					if (bundledFareLiteDTO.getBookingClasses() == null || bundledFareLiteDTO.getBookingClasses().isEmpty()
							|| (!bundledFareLiteDTO.getBookingClasses().isEmpty()
									&& bundledFareLiteDTO.getBookingClasses().contains(fareDTO.getBookingClassCode()))) {
						AdditionalFareInfo fareInfo = transformToAdditionalFareInfo(fareDTO, bundledFareLiteDTO, bundledFareDTO,
								availableIBOBFlightSegment, logicalCabinClassDTO, fareTypeCategory, lastRank,
								availabilityDTOCretor, availableFlightTotalFlexi);

						ondOption.getAdditionalFareInfo().add(fareInfo);
					}

				}
			}

		} else if (availableIBOBFlightSegment.isFlown() && availableIBOBFlightSegment.getSelectedBundledFare() != null) {
			BundledFareDTO selectedBundledFareDTO = availableIBOBFlightSegment.getSelectedBundledFare();

			BundledFareLiteDTO bundledFareLiteDTO = AAServicesModuleUtils.getBundledFareBD().getBundledFareLiteDTO(
					selectedBundledFareDTO.getBundledFarePeriodId(),
					availabilityDTOCretor.getAvailableSearchDTO().getPreferredLanguage());

			if (bundledFareLiteDTO.getBookingClasses() == null || !bundledFareLiteDTO.getBookingClasses().isEmpty()
					|| bundledFareLiteDTO.getBookingClasses().contains(fareDTO.getBookingClassCode())) {

				AdditionalFareInfo fareInfo = transformToAdditionalFareInfo(fareDTO, bundledFareLiteDTO, selectedBundledFareDTO,
						availableIBOBFlightSegment, logicalCabinClassDTO, fareTypeCategory, lastRank, availabilityDTOCretor,
						availableFlightTotalFlexi);

				ondOption.getAdditionalFareInfo().add(fareInfo);
			}
		}
	}

	private static AdditionalFareInfo transformToAdditionalFareInfo(FareSummaryDTO fareDTO, BundledFareLiteDTO bundledFareLiteDTO,
			BundledFareDTO bundledFareDTO, AvailableIBOBFlightSegment availableIBOBFlightSegment,
			LogicalCabinClassDTO logicalCabinClassDTO, FareTypeCategory fareTypeCategory, int lastRank,
			AvailabilityDTOCreator availabilityDTOCretor, BigDecimal availableFlightTotalFlexi) {
		AdditionalFareInfo fareInfo = new AdditionalFareInfo();

		BigDecimal adultFare = BigDecimal.valueOf(fareDTO.getAdultFare());
		BigDecimal totalFare = BigDecimal.valueOf(fareDTO.getTotalFare(availabilityDTOCretor.getAdultCount(),
				availabilityDTOCretor.getChildCount(), availabilityDTOCretor.getInfantCount()));

		BigDecimal chargesAndTaxTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalCharges = AccelAeroCalculator.getDefaultBigDecimalZero();

		// if (availableIBOBFlightSegment.getSelectedFlightDTO() != null) {
		// List<Double> chargesCol =
		// availableIBOBFlightSegment.getTotalCharges(logicalCabinClassDTO.getLogicalCCCode());
		// if (chargesCol != null && !chargesCol.isEmpty()) {
		// chargesAndTaxTotal = AccelAeroCalculator.parseBigDecimal(chargesCol.get(0));
		// totalCharges = AccelAeroCalculator.parseBigDecimal(chargesCol.get(0) * availabilityDTOCretor.getAdultCount()
		// + chargesCol.get(1) * availabilityDTOCretor.getInfantCount() + chargesCol.get(2)
		// * availabilityDTOCretor.getChildCount());
		// }
		BigDecimal totalAdultFareWithChargesAndWithoutServiceTaxes = new BigDecimal(
				fareDTO.getRatioAdjustedTotalPriceWithoutServiceTaxes(1, 0, 0));
		chargesAndTaxTotal = AccelAeroCalculator.subtract(totalAdultFareWithChargesAndWithoutServiceTaxes, adultFare);
		BigDecimal serviceTaxAmountForAdult = AccelAeroCalculator.subtract(new BigDecimal(fareDTO.
						getRatioAdjustedTotalPrice(1, 0, 0)) ,totalAdultFareWithChargesAndWithoutServiceTaxes);
		serviceTaxAmountForAdult = AccelAeroCalculator.add(serviceTaxAmountForAdult,
				AccelAeroCalculator.parseBigDecimal(OndConvertUtil.
						getServiceTaxAmountForFare(bundledFareLiteDTO.getPerPaxBundledFee(), availableIBOBFlightSegment)));


		BigDecimal totalFareWithCharges = new BigDecimal(fareDTO.getRatioAdjustedTotalPrice(availabilityDTOCretor.getAdultCount(),
				availabilityDTOCretor.getChildCount(), availabilityDTOCretor.getInfantCount()));
		totalCharges = AccelAeroCalculator.subtract(totalFareWithCharges, totalFare);
		// }

		fareInfo.setFareTypeCategory(fareTypeCategory);
		fareInfo.setLogicalCabinClass(logicalCabinClassDTO.getLogicalCCCode());
		fareInfo.setLogicalCabinClassDescription(bundledFareLiteDTO.getBundledFareName());
		fareInfo.setComment(bundledFareLiteDTO.getDescription());
		fareInfo.setBundledFarePeriodId(bundledFareLiteDTO.getBundleFarePeriodId());
		fareInfo.setBundledFareFee(bundledFareLiteDTO.getPerPaxBundledFee());
		fareInfo.setServiceTaxAmountForAdult(serviceTaxAmountForAdult);
		fareInfo.setServiceTaxPercentageForFares(OndConvertUtil.getServiceTaxPercentageForFares(availableIBOBFlightSegment));
		fareInfo.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
				
		fareInfo.setImageUrl(bundledFareLiteDTO.getImageUrl());
		fareInfo.setBundleFareDescriptionInfo(TransformerUtil.transform(bundledFareLiteDTO.getBundleFareDescriptionTemplateDTO()));		
		fareInfo.getBundledFareFreeServices().addAll(AAUtils.convertToExternalChargeTypes(bundledFareLiteDTO.getFreeServices()));
		if (bundledFareLiteDTO.getBookingClasses() != null) {
			fareInfo.getBookingCodes().addAll(bundledFareLiteDTO.getBookingClasses());
		}
		fareInfo.getSegmentBookingClasses()
				.addAll(TransformerUtil.convertStringStringMapToLCCList(bundledFareLiteDTO.getSegmentBookingClass()));
		fareInfo.setNestRank(++lastRank);
		fareInfo.setSeatAvailable(availableIBOBFlightSegment.isSeatsAvailable(logicalCabinClassDTO.getLogicalCCCode()));
		fareInfo.setAdultFare(adultFare);
		fareInfo.setAdultCharges(chargesAndTaxTotal);
		fareInfo.setTotalFare(totalFare);
		fareInfo.setTotalCharges(totalCharges);
		fareInfo.setWithFlexi(bundledFareLiteDTO.isFlexiIncluded());

		if (bundledFareLiteDTO.isFlexiIncluded()) {
			fareInfo.setFlexiCharge(AccelAeroCalculator.getDefaultBigDecimalZero());
		} else {
			fareInfo.setFlexiCharge(availableFlightTotalFlexi);
		}

		fareInfo.setIsFreeFlexi(false);
		if (((FareSummaryDTO) fareDTO).getVisibleChannelNames() != null
				|| (((FareSummaryDTO) fareDTO).getVisibleChannelNames()).size() > 0) {
			fareInfo.setVisibleChannelName((String) ((List) ((FareSummaryDTO) fareDTO).getVisibleChannelNames()).get(0));
		}

		// Booking class wise allocations
		int numberOfBCWiseAvailableSeats = fareDTO.getNoOfAvailableSeats();

		// Flights seat allocation (in case of connections first flights availability will be taken)
		int numberOfAllocatedAvailableSeats = availableIBOBFlightSegment.getFlightSegmentDTOs().get(0).getAvailableAdultCount()
				.get(logicalCabinClassDTO.getLogicalCCCode());

		/*
		 * If Booking class wise allocation is less than allocated available seats in the flight it is taken otherwise,
		 * actual allocated number of seats are taken. (lesser of the two)
		 */
		int numberOfAvailableSeats = numberOfAllocatedAvailableSeats < numberOfBCWiseAvailableSeats
				? numberOfAllocatedAvailableSeats
				: numberOfBCWiseAvailableSeats;
		fareInfo.setAvailableNoOfSeats(numberOfAvailableSeats);

		if (bundledFareDTO != null && bundledFareDTO.getBundledFarePeriodId().equals(bundledFareLiteDTO.getBundleFarePeriodId())) {
			fareInfo.setSelected(true);
		}
		if (availableIBOBFlightSegment.getMinimumFareByPriceOnd() != null
				&& !availableIBOBFlightSegment.getMinimumFareByPriceOnd().isEmpty()) {
			fareInfo.getMinimumFareByPriceOndType().addAll(TransformerUtil
					.transformFarePriceOndToStringDecimalMap(availableIBOBFlightSegment.getMinimumFareByPriceOnd()));
		}
		return fareInfo;
	}

	private static FareSummaryDTO getFareSummaryDTO(AvailableIBOBFlightSegment availableFlight, String logicalCabinClass) {
		FareSummaryDTO fareSummary = null;
		if (!availableFlight.isDirectFlight()) {
			for (AvailableFlightSegment availableFlightSegment : ((AvailableConnectedFlight) availableFlight)
					.getAvailableFlightSegments()) {
				if (availableFlightSegment.getFare(logicalCabinClass) != null) {
					FareSummaryDTO fareSummaryDTO = availableFlightSegment.getFare(logicalCabinClass);
					if (fareSummary == null) {
						fareSummary = fareSummaryDTO;
					} else {
						fareSummary.setAdultFare(fareSummary.getAdultFare() + fareSummaryDTO.getAdultFare());
						fareSummary.setChildFare(fareSummary.getAdultFare() + fareSummaryDTO.getAdultFare());
						fareSummary.setInfantFare(fareSummary.getAdultFare() + fareSummaryDTO.getAdultFare());

						if (!availableFlightSegment.isBusSegment()) {
							fareSummary.setBookingClassCode(fareSummaryDTO.getBookingClassCode());
						}
					}
				}
			}
		} else {
			fareSummary = availableFlight.getFare(logicalCabinClass);
		}
		return fareSummary;
	}

	private static void createDummyFareSummaryObjectForFlexi(OriginDestinationOption ondOption, FareTypeCategory fareCategory,
			AvailableIBOBFlightSegment availableFlight, BigDecimal adultFare, BigDecimal totalFare, BigDecimal adultChargesTotal,
			BigDecimal totalCharges, LogicalCabinClassDTO logicalCabinClassDTO, boolean freeFlexiEnabled,
			boolean isSelectedFlight, boolean isFromFareQuote, FareSummaryDTO fareDTO, boolean ondFlexiSelected,
			String preferredLanguage) {
		boolean isLogicalCCWithFlexiSelected = false;

		BigDecimal availableFlightTotalFlexi = calculateTotalFlexiCharge(availableFlight,
				logicalCabinClassDTO.getLogicalCCCode());
		Integer flexiRuleID = getFlexiRuleID(availableFlight, logicalCabinClassDTO.getLogicalCCCode());

		AdditionalFareInfo fareInfo = getFareInfoDetail(ondOption.getAdditionalFareInfo(), fareCategory,
				logicalCabinClassDTO.getLogicalCCCode(), true);

		if (fareInfo == null) {
			fareInfo = new AdditionalFareInfo();
			fareInfo.setIsFreeFlexi(freeFlexiEnabled);
			fareInfo.setFareTypeCategory(fareCategory);
			fareInfo.setLogicalCabinClass(logicalCabinClassDTO.getLogicalCCCode());
			if (freeFlexiEnabled) {
				fareInfo.setLogicalCabinClassDescription(logicalCabinClassDTO.getDescription());
			} else {
				fareInfo.setLogicalCabinClassDescription(logicalCabinClassDTO.getFlexiDescription());
			}
			fareInfo.setComment(logicalCabinClassDTO.getFlexiComment());

			// if (isSelectedFlight && isFromFareQuote
			// && logicalCabinClassDTO.getLogicalCCCode().equals(availableFlight.getSelectedLogicalCabinClass())) {
			// isLogicalCCWithFlexiSelected = true;
			// } else if (isSelectedFlight && !isFromFareQuote && freeFlexiEnabled
			// && logicalCabinClassDTO.getLogicalCCCode().equals(availableFlight.getSelectedLogicalCabinClass())) {
			// isLogicalCCWithFlexiSelected = true;
			// }

			if (freeFlexiEnabled || ondFlexiSelected) {
				isLogicalCCWithFlexiSelected = (availableFlight.getSelectedBundledFare() == null);
			}

			fareInfo.setSelected(isLogicalCCWithFlexiSelected);
			fareInfo.setNestRank(logicalCabinClassDTO.getNestRank());
			fareInfo.setSeatAvailable(availableFlight.isSeatsAvailable(logicalCabinClassDTO.getLogicalCCCode()));
			fareInfo.setAdultFare(adultFare);
			fareInfo.setAdultCharges(adultChargesTotal);
			fareInfo.setTotalFare(totalFare);
			fareInfo.setTotalCharges(totalCharges);
			fareInfo.setFlexiCharge(availableFlightTotalFlexi);
			fareInfo.setWithFlexi(true);
			fareInfo.setImageUrl(
					OndConvertUtil.generateLogicalCCImageUrl(logicalCabinClassDTO.getLogicalCCCode(), true, preferredLanguage));
			if (((FareSummaryDTO) fareDTO).getVisibleChannelNames() != null
					|| (((FareSummaryDTO) fareDTO).getVisibleChannelNames()).size() > 0) {
				fareInfo.setVisibleChannelName((String) ((List) ((FareSummaryDTO) fareDTO).getVisibleChannelNames()).get(0));
			}
			if (!availableFlight.getMinimumFareByPriceOnd().isEmpty()) {
				fareInfo.getMinimumFareByPriceOndType().addAll(
						TransformerUtil.transformFarePriceOndToStringDecimalMap(availableFlight.getMinimumFareByPriceOnd()));
			}
			ondOption.getAdditionalFareInfo().add(fareInfo);
		} else {
			fareInfo.setAdultFare(AccelAeroCalculator.add(adultFare, fareInfo.getAdultFare()));
			fareInfo.setTotalFare(AccelAeroCalculator.add(totalFare, fareInfo.getTotalFare()));
			fareInfo.setAdultCharges(AccelAeroCalculator.add(adultChargesTotal, fareInfo.getAdultCharges()));
			if (((FareSummaryDTO) fareDTO).getVisibleChannelNames() != null
					|| (((FareSummaryDTO) fareDTO).getVisibleChannelNames()).size() > 0) {
				fareInfo.setVisibleChannelName((String) ((List) ((FareSummaryDTO) fareDTO).getVisibleChannelNames()).get(0));
			}
			if (!availableFlight.getMinimumFareByPriceOnd().isEmpty()) {
				fareInfo.getMinimumFareByPriceOndType().addAll(
						TransformerUtil.transformFarePriceOndToStringDecimalMap(availableFlight.getMinimumFareByPriceOnd()));
			}
		}

		// Booking class wise allocations
		int numberOfBCWiseAvailableSeats = fareDTO.getNoOfAvailableSeats();

		// Flights seat allocation (in case of connections first flights availability will be taken)
		int numberOfAllocatedAvailableSeats = availableFlight.getFlightSegmentDTOs().get(0).getAvailableAdultCount()
				.get(logicalCabinClassDTO.getLogicalCCCode());

		/*
		 * If Booking class wise allocation is less than allocated available seats in the flight it is taken otherwise,
		 * actual allocated number of seats are taken. (lesser of the two)
		 */
		int numberOfAvailableSeats = numberOfAllocatedAvailableSeats < numberOfBCWiseAvailableSeats
				? numberOfAllocatedAvailableSeats
				: numberOfBCWiseAvailableSeats;
		fareInfo.setAvailableNoOfSeats(numberOfAvailableSeats);
		fareInfo.setFlexiRuleID(flexiRuleID);
	}

	/**
	 * @param additionalFareInfo
	 * @param fareCategory
	 * @param logicalCabinClass
	 * @param withFlexi
	 * @return
	 */
	private static AdditionalFareInfo getFareInfoDetail(List<AdditionalFareInfo> additionalFareInfo,
			FareTypeCategory fareCategory, String logicalCabinClass, boolean withFlexi) {

		for (AdditionalFareInfo fareInfo : additionalFareInfo) {
			if (fareInfo.getFareTypeCategory().equals(fareCategory) && fareInfo.getLogicalCabinClass().equals(logicalCabinClass)
					&& fareInfo.isWithFlexi() == withFlexi) {
				return fareInfo;
			}
		}
		return null;
	}

	private static BigDecimal calculateTotalFlexiCharge(AvailableIBOBFlightSegment availableFlight, String logicalCabinClass) {
		BigDecimal flexiTotal = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (availableFlight.getFare(logicalCabinClass) != null) {
			flexiTotal = getTotalFlexiCharges(availableFlight.getEffectiveFlexiCharges(logicalCabinClass));
		} else if (!availableFlight.isDirectFlight()) {
			for (AvailableFlightSegment availableFlightSegment : ((AvailableConnectedFlight) availableFlight)
					.getAvailableFlightSegments()) {
				if (availableFlightSegment.getFare(logicalCabinClass) != null) {
					flexiTotal = AccelAeroCalculator.add(flexiTotal,
							getTotalFlexiCharges(availableFlightSegment.getEffectiveFlexiCharges(logicalCabinClass)));
				}
			}
		}

		return flexiTotal;
	}

	private static Integer getFlexiRuleID(AvailableIBOBFlightSegment availableFlight, String logicalCabinClass) {
		// Flexi rules are one per segment.
		Integer firstFlexiRuleID = null;

		Collection<FlexiRuleDTO> flexiCharges = availableFlight.getEffectiveFlexiCharges(logicalCabinClass);

		if (flexiCharges != null && !flexiCharges.isEmpty()) {
			firstFlexiRuleID = flexiCharges.iterator().next().getFlexiRuleId();
		}
		return firstFlexiRuleID;
	}

	private static BigDecimal getTotalFlexiCharges(Collection<FlexiRuleDTO> colFlexiRuleDTOs) {
		BigDecimal flexiTotal = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (colFlexiRuleDTOs != null) {
			for (FlexiRuleDTO flexiRuleDTO : colFlexiRuleDTOs) {
				Double flexiVal = flexiRuleDTO.getEffectiveChargeAmount(PaxTypeTO.ADULT);
				if (flexiVal != null) {
					flexiTotal = AccelAeroCalculator.add(flexiTotal, AccelAeroCalculator.parseBigDecimal(flexiVal));
				}
			}
		}

		return flexiTotal;
	}

	/**
	 * @param colOndFareDTOForLogicalCC
	 * @return
	 */
	private static FareTypeCategory getFareTypeCategory(Collection<OndFareDTO> colOndFareDTOForLogicalCC) {
		if (colOndFareDTOForLogicalCC != null && !colOndFareDTOForLogicalCC.isEmpty()) {
			int fareType = colOndFareDTOForLogicalCC.iterator().next().getFareType();
			if (fareType == FareTypes.HALF_RETURN_FARE) {
				return FareTypeCategory.HALFRT;
			} else {
				return FareTypeCategory.ONEWAY;
			}
		}
		return null;
	}

	/**
	 * @param availableFlightSegments
	 * @param availabilityDTOCretor
	 * @return
	 */
	private static List<InterlineAirlineFareInfo> populateInterlineFareInfo(
			Collection<AvailableFlightSegment> availableFlightSegments, Map<String, LogicalCabinClassDTO> cachedLogicalCCMap) {

		List<Map<String, List<String>>> flightWiseExternalBookingClasses = new ArrayList<Map<String, List<String>>>();

		for (AvailableFlightSegment availableFlightSegment : availableFlightSegments) {

			Map<String, List<String>> externalBookingClasses = new HashMap<String, List<String>>();

			if (availableFlightSegment.getFilteredBucketsDTOForSegmentFares() != null) {
				for (String logicalCabinClass : availableFlightSegment.getFilteredBucketsDTOForSegmentFares()
						.getExternalBookingClassAvailability()) {

					Collection<AvailableBCAllocationSummaryDTO> externalBCAvailability = availableFlightSegment
							.getFilteredBucketsDTOForSegmentFares().getExternalBookingClassAvailability(logicalCabinClass);

					externalBookingClasses.put(logicalCabinClass, new ArrayList<String>());

					if (externalBCAvailability != null) {
						for (AvailableBCAllocationSummaryDTO bcAllocSummaryDTO : externalBCAvailability) {
							externalBookingClasses.get(logicalCabinClass).add(bcAllocSummaryDTO.getBookingCode());
						}
					}
				}
			}
			if (!externalBookingClasses.isEmpty()) {
				flightWiseExternalBookingClasses.add(externalBookingClasses);
			}
		}
		Map<String, List<String>> filteredExternalBookingClasses = getFilteredExternalBookingClassAvailability(
				flightWiseExternalBookingClasses);
		List<InterlineAirlineFareInfo> interlineAirlineFareInfo = createInterlineFareInfo(filteredExternalBookingClasses,
				cachedLogicalCCMap);
		setAdultCharges(interlineAirlineFareInfo, availableFlightSegments);

		return interlineAirlineFareInfo;
	}

	/**
	 * @param interlineAirlineFareInfoList
	 * @param availableFlightSegments
	 */
	private static void setAdultCharges(List<InterlineAirlineFareInfo> interlineAirlineFareInfoList,
			Collection<AvailableFlightSegment> availableFlightSegments) {

		for (InterlineAirlineFareInfo interlineAirlineFareInfo : interlineAirlineFareInfoList) {
			BigDecimal totalAdultCharge = BigDecimal.ZERO;
			for (AvailableFlightSegment availableFlightSegment : availableFlightSegments) {
				List<Double> charges = availableFlightSegment.getChargesForInterlineBookingClass(
						interlineAirlineFareInfo.getLogicalCabinClass(), interlineAirlineFareInfo.getBookingClassCode());
				if (charges != null && !charges.isEmpty()) {
					totalAdultCharge = AccelAeroCalculator.add(totalAdultCharge, BigDecimal.valueOf(charges.get(0)));
				}
			}
			interlineAirlineFareInfo.setChargeAmount(totalAdultCharge);
		}
	}

	/**
	 * @param flightWiseExternalBookingClasses
	 * @return
	 */
	private static Map<String, List<String>>
			getFilteredExternalBookingClassAvailability(List<Map<String, List<String>>> flightWiseExternalBookingClasses) {
		Map<String, List<String>> filteredExternalBookingClasses = new HashMap<String, List<String>>();
		for (Map<String, List<String>> externalBookingClasses : flightWiseExternalBookingClasses) {
			for (String logicalCabinClass : externalBookingClasses.keySet()) {
				if (filteredExternalBookingClasses.get(logicalCabinClass) == null) {
					filteredExternalBookingClasses.put(logicalCabinClass, new ArrayList<String>());
					filteredExternalBookingClasses.get(logicalCabinClass).addAll(externalBookingClasses.get(logicalCabinClass));
				} else {
					filteredExternalBookingClasses.get(logicalCabinClass)
							.retainAll(externalBookingClasses.get(logicalCabinClass));
				}
			}
		}
		return filteredExternalBookingClasses;
	}

	/**
	 * @param filteredExternalBookingClasses
	 * @param availabilityDTOCretor
	 * @param cachedLogicalCCMap
	 * @return
	 */
	private static List<InterlineAirlineFareInfo> createInterlineFareInfo(
			Map<String, List<String>> filteredExternalBookingClasses, Map<String, LogicalCabinClassDTO> cachedLogicalCCMap) {

		List<InterlineAirlineFareInfo> interlineAirlineFareInfo = new ArrayList<InterlineAirlineFareInfo>();
		for (String logicalCabinClass : filteredExternalBookingClasses.keySet()) {
			LogicalCabinClassDTO logicalCabinClassDTO = cachedLogicalCCMap.get(logicalCabinClass);
			for (String bookingClass : filteredExternalBookingClasses.get(logicalCabinClass)) {

				InterlineAirlineFareInfo ilFareInfo = new InterlineAirlineFareInfo();

				ilFareInfo.setLogicalCabinClass(logicalCabinClass);
				ilFareInfo.setLogicalCabinClassDescription(logicalCabinClassDTO.getDescription());
				ilFareInfo.setLogicalCabinClassRank(logicalCabinClassDTO.getNestRank());
				ilFareInfo.setLogicalCabinClassComment(logicalCabinClassDTO.getComment());
				ilFareInfo.setBookingClassCode(bookingClass);
				ilFareInfo.setCurrencyCode(AppSysParamsUtil.getBaseCurrency());

				interlineAirlineFareInfo.add(ilFareInfo);
			}
		}
		return interlineAirlineFareInfo;
	}

	private static FlightSegment createFlightSegment(FlightSegmentDTO flightSegmentDTO, int adultsAvailable, int infantsAvailable,
			String cabinClassCode, String appIndicator, int subJourneyGroup, boolean isInboundFlight, String flightType)
			throws ModuleException {

		FlightSegment flightSegment = new FlightSegment();

		flightSegment.setSegmentCode(flightSegmentDTO.getSegmentCode());
		flightSegment.setDepatureDateTime(flightSegmentDTO.getDepartureDateTime());
		flightSegment.setDepatureDateTimeZulu(flightSegmentDTO.getDepartureDateTimeZulu());
		flightSegment.setArrivalDateTime(flightSegmentDTO.getArrivalDateTime());
		flightSegment.setArrivalDateTimeZulu(flightSegmentDTO.getArrivalDateTimeZulu());

		flightSegment.setFlightRefNumber(flightSegmentDTO.getSegmentId().toString());
		flightSegment.setFlightId(flightSegmentDTO.getFlightId());
		flightSegment.setFlightNumber(flightSegmentDTO.getFlightNumber());
		flightSegment.setOperationType(flightSegmentDTO.getOperationTypeID());
		flightSegment.setOperatingAirline(flightSegmentDTO.getCarrierCode());
		flightSegment.setReturnFlag(isInboundFlight
				? ReservationInternalConstants.ReturnTypes.RETURN_FLAG_TRUE
				: ReservationInternalConstants.ReturnTypes.RETURN_FLAG_FALSE);

		flightSegment.setFltModelDesc(flightSegmentDTO.getFlightModelDescription());
		flightSegment.setFlightDuration(flightSegmentDTO.getFlightDuration());

		flightSegment.setAirTravelerAvail(new AirTravelerAvail());
		PassengerTypeQuantity adultsQuantity = new PassengerTypeQuantity();
		adultsQuantity.setPassengerType(PassengerType.ADT);
		adultsQuantity.setQuantity(adultsAvailable);
		flightSegment.getAirTravelerAvail().getPassengerTypeQuantity().add(adultsQuantity);

		PassengerTypeQuantity infantQuantity = new PassengerTypeQuantity();
		infantQuantity.setPassengerType(PassengerType.INF);
		infantQuantity.setQuantity(infantsAvailable);
		flightSegment.getAirTravelerAvail().getPassengerTypeQuantity().add(infantQuantity);
		flightSegment.setSubJourneyGroup(subJourneyGroup);

		flightSegment.setFlightType(flightType);

		for (String logicalCC : flightSegmentDTO.getAvailableAdultCount().keySet()) {
			LogicalCabinSeatAvailability logicCCSeatAvail = new LogicalCabinSeatAvailability();
			logicCCSeatAvail.setLogicalCabinClass(logicalCC);
			PassengerTypeQuantity paxQty = new PassengerTypeQuantity();
			paxQty.setPassengerType(PaxTypeUtils.convertAAPaxCodeToLCCPaxCodes(PaxTypeTO.ADULT));
			paxQty.setQuantity(flightSegmentDTO.getAvailableAdultCount().get(logicalCC));
			logicCCSeatAvail.getPassengerTypeQuantity().add(paxQty);

			if (flightSegmentDTO.getAvailableInfantCount().containsKey(logicalCC)) {
				paxQty = new PassengerTypeQuantity();
				paxQty.setPassengerType(PaxTypeUtils.convertAAPaxCodeToLCCPaxCodes(PaxTypeTO.INFANT));
				paxQty.setQuantity(flightSegmentDTO.getAvailableInfantCount().get(logicalCC));
			}
			flightSegment.getLogicalCabinWiseSeatAvailability().add(logicCCSeatAvail);
		}

		if (ApplicationEngine.XBE.toString().equals(appIndicator)) {
			Set<String> privileges = (Set<String>) AASessionManager.getUserPrivileges();
			boolean viewActualAvail = PrivilegeUtil.hasPrivilege(privileges, PriviledgeConstants.ALLOW_VIEW_AVAILABLESEATS);
			if (!viewActualAvail) {
				String adultStr = adultsAvailable + "";
				String infStr = adultsAvailable + "";
				if (adultsAvailable > AppSysParamsUtil.getShowSeatCount()) {
					adultStr = AppSysParamsUtil.getShowSeatCount() + "+";
				}
				if (infantsAvailable > AppSysParamsUtil.getShowSeatCount()) {
					infStr = AppSysParamsUtil.getShowSeatCount() + "+";
				}
				flightSegment.setSeatAvailCount(adultStr + "/" + infStr);
			}
		}

		// required in multi-city flight search to show the available seat count cabin wise
		if (flightSegmentDTO.getAvailableAdultCount() != null && flightSegmentDTO.getAvailableAdultCount().size() > 0) {
			for (String logicalCC : flightSegmentDTO.getAvailableAdultCount().keySet()) {
				StringIntegerMap strIntObj = new StringIntegerMap();
				strIntObj.setKey(logicalCC);
				strIntObj.setValue(flightSegmentDTO.getAvailableAdultCount().get(logicalCC));
				flightSegment.getAvailableAdultCountMap().add(strIntObj);

			}
		}

		if (flightSegmentDTO.getAvailableInfantCount() != null && flightSegmentDTO.getAvailableInfantCount().size() > 0) {
			for (String logicalCC : flightSegmentDTO.getAvailableInfantCount().keySet()) {
				StringIntegerMap strIntObj = new StringIntegerMap();
				strIntObj.setKey(logicalCC);
				strIntObj.setValue(flightSegmentDTO.getAvailableInfantCount().get(logicalCC));
				flightSegment.getAvailableInfantCountMap().add(strIntObj);

			}
		}

		flightSegment.setCabinClassCode(cabinClassCode);

		return flightSegment;
	}

	private static void populateFlightSegment(OriginDestinationOption ondOption, AvailableFlightSegment availableFlightSegment,
			AvailabilityDTOCreator availabilityDTOCreator) throws ModuleException {

		FlightSegmentDTO flightSegmentDTO = availableFlightSegment.getFlightSegmentDTO();
		String cabinClass = availableFlightSegment.getCabinClassCode();
		FlightSegment flightSegment = createFlightSegment(flightSegmentDTO,
				availableFlightSegment.getFlightSegmentDTO().getTotalAvailableAdultCount(),
				availableFlightSegment.getFlightSegmentDTO().getTotalAvailableInfantCount(), cabinClass,
				availabilityDTOCreator.getAvailableSearchDTO().getAppIndicator(), availableFlightSegment.getSubJourneyGroup(),
				availableFlightSegment.isInboundFlightSegment(), availableFlightSegment.getFlightSegmentDTO().getFlightType());

		ondOption.getFlightSegment().add(flightSegment);
	}

	static FareSegmentChargeTO populateFareSegChargeTO(FareSegChargeTO fareSegChargeTO) {
		if (fareSegChargeTO == null) {
			return null;
		}
		FareSegmentChargeTO fsct = new FareSegmentChargeTO();

		for (OndFareSegChargeTO ondFareSegChargeTO : fareSegChargeTO.getOndFareSegChargeTOs()) {
			fsct.getOndFareSegmentChargeTOs().add(transformFareSegChargeTO(ondFareSegChargeTO));
		}

		for (String paxT : fareSegChargeTO.getTotalJourneyFare().keySet()) {
			PerPaxJourneyFare paxTotalJourneyFare = new PerPaxJourneyFare();
			PassengerType paxType = PaxTypeUtils.convertAAPaxCodeToLCCPaxCodes(paxT);
			paxTotalJourneyFare.setFareAmount(fareSegChargeTO.getTotalJourneyFare().get(paxT));
			paxTotalJourneyFare.setPaxType(paxType);
			fsct.getPerPaxTotalJourneyFare().add(paxTotalJourneyFare);
		}
		return fsct;
	}

	private static OndFareSegmentChargeTO transformFareSegChargeTO(OndFareSegChargeTO ondFareSegChargeTO) {
		OndFareSegmentChargeTO rsFSC = new OndFareSegmentChargeTO();
		rsFSC.setFareID(ondFareSegChargeTO.getFareID());
		rsFSC.setFareBasisCode(ondFareSegChargeTO.getFareBasisCode());
		rsFSC.setFarePercentage(ondFareSegChargeTO.getFarePercentage());
		rsFSC.setFareTypeId(ondFareSegChargeTO.getFareType());
		rsFSC.setIsInbound(ondFareSegChargeTO.isInbound());
		rsFSC.setIsModifyInbound(ondFareSegChargeTO.isModifyInbound());
		rsFSC.setIsOpenReturn(ondFareSegChargeTO.isOpenReturn());
		rsFSC.setBusSegmentExists(ondFareSegChargeTO.isBusSegmentExists());
		for (Integer fltSegId : ondFareSegChargeTO.getFsegBCAlloc().keySet()) {
			FlightSegmentBCAllocation fltSegBCAlloc = new FlightSegmentBCAllocation();
			fltSegBCAlloc.setFlightSegId(fltSegId);
			for (BookingClassAlloc bcAl : ondFareSegChargeTO.getFsegBCAlloc().get(fltSegId)) {
				BookingClassAllocation bcAlloc = new BookingClassAllocation();
				bcAlloc.setBcCode(bcAl.getBookingClassCode());
				// bcAlloc.setCcCode(bcAl.getCcCode()); //FIXME
				bcAlloc.setNoOfAdults(bcAl.getNoOfAdults());
				fltSegBCAlloc.getBcAllocation().add(bcAlloc);
				// set cabin class details
				fltSegBCAlloc.setCabinClass(bcAl.getCabinClassCode());
				fltSegBCAlloc.setLogicalCabinClass(bcAl.getLogicaCabinClassCode());
			}
			rsFSC.getFsegBCAlloc().add(fltSegBCAlloc);
		}

		rsFSC.getChargeRateIds().addAll(ondFareSegChargeTO.getChargeRateIds());

		rsFSC.getFlexiChargeIds().addAll(ondFareSegChargeTO.getFlexiChargeIds());
		return rsFSC;
	}

	public static void injectInterlineFaresToOndDTOs(Collection<OndFareDTO> ondFareDTOList,
			Map<String, BigDecimal> overridePaxFareMap) {
		if (ondFareDTOList != null && ondFareDTOList.size() > 0 && overridePaxFareMap.size() > 0) {
			BigDecimal ondCount = new BigDecimal(ondFareDTOList.size());

			BigDecimal interlineAdultChildFarePerOnD = AccelAeroCalculator.divide(overridePaxFareMap.get(PaxTypeTO.ADULT),
					ondCount);
			BigDecimal interlineInfantFarePerOnD = AccelAeroCalculator.divide(overridePaxFareMap.get(PaxTypeTO.INFANT), ondCount);
			BigDecimal interlineAdultChildFareForLastOnD = AccelAeroCalculator.subtract(overridePaxFareMap.get(PaxTypeTO.ADULT),
					AccelAeroCalculator.multiply(interlineAdultChildFarePerOnD, new BigDecimal(ondFareDTOList.size() - 1)));
			BigDecimal interlineInfantFareForOnD = AccelAeroCalculator.subtract(overridePaxFareMap.get(PaxTypeTO.INFANT),
					AccelAeroCalculator.multiply(interlineInfantFarePerOnD, new BigDecimal(ondFareDTOList.size() - 1)));

			Object[] ondFareArr = ondFareDTOList.toArray();

			for (int i = 0; i < ondFareDTOList.size(); i++) {
				OndFareDTO ondFareDTO = (OndFareDTO) ondFareArr[i];
				if (i == ondFareDTOList.size() - 1) {
					ondFareDTO.getFareSummaryDTO().setAdultFare(interlineAdultChildFareForLastOnD.doubleValue());
					ondFareDTO.getFareSummaryDTO().setInfantFare(interlineInfantFareForOnD.doubleValue());
					recalculatePrecentageCharges(ondFareDTO);
				} else {
					ondFareDTO.getFareSummaryDTO().setAdultFare(interlineAdultChildFarePerOnD.doubleValue());
					ondFareDTO.getFareSummaryDTO().setInfantFare(interlineInfantFarePerOnD.doubleValue());
					recalculatePrecentageCharges(ondFareDTO);
				}
			}
		}
	}

	public static List<LCCServiceTaxContainer> populateLCCServiceTaxContainers(Set<ServiceTaxContainer> serviceTaxCOntainers) {
		List<LCCServiceTaxContainer> lccServiceTaxContainers = new ArrayList<LCCServiceTaxContainer>();
		for (ServiceTaxContainer serviceTaxContainer : serviceTaxCOntainers) {
			LCCServiceTaxContainer lccServiceTaxContainer = new LCCServiceTaxContainer();
			lccServiceTaxContainer.setTaxApplicable(serviceTaxContainer.isTaxApplicable());
			lccServiceTaxContainer.setTaxRatio(serviceTaxContainer.getTaxRatio());
			lccServiceTaxContainer.setTaxChargeCode(AAUtils.getExternalChargeType(serviceTaxContainer.getExternalCharge()));
			lccServiceTaxContainer.setTaxApplyingFlightRefNumber(serviceTaxContainer.getTaxApplyingFlightRefNumber());
			for (EXTERNAL_CHARGES taxableExtChg : serviceTaxContainer.getTaxableExternalCharges()) {
				lccServiceTaxContainer.getTaxableExternalCharges().add(AAUtils.getExternalChargeType(taxableExtChg));
			}
			lccServiceTaxContainers.add(lccServiceTaxContainer);
		}
		return lccServiceTaxContainers;
	}

	private static void recalculatePrecentageCharges(OndFareDTO ondFareDTO) {

		// All Charges
		Collection<QuotedChargeDTO> chargesList = ondFareDTO.getAllCharges();
		if (chargesList != null && chargesList.size() > 0) {
			for (QuotedChargeDTO quotedChargeDTO : chargesList) {
				if (quotedChargeDTO.isChargeValueInPercentage()) {
					setReCulatedPrecentageCharge(ondFareDTO, quotedChargeDTO, PaxTypeTO.ADULT);
					setReCulatedPrecentageCharge(ondFareDTO, quotedChargeDTO, PaxTypeTO.CHILD);
					setReCulatedPrecentageCharge(ondFareDTO, quotedChargeDTO, PaxTypeTO.INFANT);
				}
			}
		}
	}

	private static void setReCulatedPrecentageCharge(OndFareDTO ondFareDTO, QuotedChargeDTO quotedChargeDTO, String paxType) {

		double paxFare = ondFareDTO.getFareSummaryDTO().getFareAmount(paxType);
		Double paxNewCharge = AccelAeroCalculator
				.multiply(new BigDecimal(paxFare), new BigDecimal(quotedChargeDTO.getChargeValuePercentage())).doubleValue();
		paxNewCharge = AccelAeroCalculator.divide(BigDecimal.valueOf(paxNewCharge), new BigDecimal(100)).doubleValue();

		if (quotedChargeDTO.isChargeRateRoundingApplicable()) {
			paxNewCharge = AccelAeroRounderPolicy.getRoundedValue(BigDecimal.valueOf(paxNewCharge),
					quotedChargeDTO.getBoundryValue(), quotedChargeDTO.getBreakPoint()).doubleValue();
		}

		quotedChargeDTO.setEffectiveChargeAmount(paxType, paxNewCharge);
	}

}
