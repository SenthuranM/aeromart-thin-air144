package com.isa.thinair.aaservices.core.bl.reservation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.DiscountCharge;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PassengerTypeQuantity;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaxDiscountDetails;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AADiscountRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AADiscountRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.core.session.AASessionManager;
import com.isa.thinair.aaservices.core.transaction.TransactionalReservationProcess;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.AAServicesTransformUtil;
import com.isa.thinair.aaservices.core.util.PaxTypeUtils;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PassengerTypeQuantityTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PaxDiscountDetailTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.QuotedFareRebuildDTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ReservationDiscountDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.DiscountRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.DiscountRQ.DISCOUNT_METHOD;
import com.isa.thinair.airproxy.api.utils.assembler.PaxCountAssembler;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * @author M.Rikaz
 */
public class AACalculateDiscountBL {
	private static Log log = LogFactory.getLog(AACalculateDiscountBL.class);

	private AADiscountRQ aaDiscountCalcRQ;

	private TransactionalReservationProcess tnxResProcess;

	public AACalculateDiscountBL(AADiscountRQ aaAirRequoteBalanceRQ, AASessionManager aaSessionManager) {
		this.aaDiscountCalcRQ = aaAirRequoteBalanceRQ;
		String transactionId = PlatformUtiltiies.nullHandler((aaAirRequoteBalanceRQ != null && aaAirRequoteBalanceRQ
				.getHeaderInfo() != null) ? aaAirRequoteBalanceRQ.getHeaderInfo().getTransactionIdentifier() : null);

		if (transactionId.length() > 0 && aaSessionManager != null) {
			this.tnxResProcess = (TransactionalReservationProcess) aaSessionManager.getCurrentUserTransaction(transactionId);
		}
	}

	public AADiscountRS execute() {
		AADiscountRS aaDiscountCalcRS = new AADiscountRS();
		aaDiscountCalcRS.setTotalDiscount(AccelAeroCalculator.getDefaultBigDecimalZero());
		aaDiscountCalcRS.setResponseAttributes(new AAResponseAttributes());

		List<FareSegChargeTO> fareSegChargeTOs = new ArrayList<FareSegChargeTO>();

		try {
			DiscountRQ promotionRQ = new DiscountRQ(DISCOUNT_METHOD.getEnum(aaDiscountCalcRQ.getActionType()));

			promotionRQ.setValidateCriteria(aaDiscountCalcRQ.isValidateCriteria());
			promotionRQ.setCalculateTotalOnly(aaDiscountCalcRQ.isCalculateTotalOnly());
			DiscountedFareDetails discountedFareDetails = null;
			if (aaDiscountCalcRQ.getDiscountedFareDetails() != null) {
				discountedFareDetails = AAServicesTransformUtil.tranformLccDiscountedFareDetails(aaDiscountCalcRQ
						.getDiscountedFareDetails());
				promotionRQ.setDiscountInfoDTO(discountedFareDetails);
			}

			Collection<PassengerTypeQuantityTO> paxQtyList = null;
			if (aaDiscountCalcRQ.getPassengerTypeQuantity() != null && !aaDiscountCalcRQ.getPassengerTypeQuantity().isEmpty()) {
				paxQtyList = new ArrayList<PassengerTypeQuantityTO>();
				for (PassengerTypeQuantity paxTypeQty : aaDiscountCalcRQ.getPassengerTypeQuantity()) {
					PassengerTypeQuantityTO paxTypeQtyTO = new PassengerTypeQuantityTO();
					paxTypeQtyTO.setPassengerType(PaxTypeUtils.convertLCCPaxCodeToAAPaxCodes(paxTypeQty.getPassengerType()));
					paxTypeQtyTO.setQuantity(paxTypeQty.getQuantity());
					paxQtyList.add(paxTypeQtyTO);
				}
			}

			if (aaDiscountCalcRQ.getPaxCharges() != null) {
				promotionRQ.setPaxChargesList(AAServicesTransformUtil.tranformLccPaxCharges(aaDiscountCalcRQ.getPaxCharges()));
			}

			promotionRQ.setPaxQtyList(paxQtyList);

			if (aaDiscountCalcRQ.getSelectedFareTypes() != null && aaDiscountCalcRQ.getSelectedFareTypes().size() > 0) {
				fareSegChargeTOs.addAll(tnxResProcess.getFSCSet(aaDiscountCalcRQ.getSelectedFareTypes()));

				PaxCountAssembler paxCountAssembler = new PaxCountAssembler(tnxResProcess.getPaxCountAssembler().getAdultCount(),
						tnxResProcess.getPaxCountAssembler().getChildCount(), tnxResProcess.getPaxCountAssembler()
								.getInfantCount());

				String bookingType = null;
				if (fareSegChargeTOs != null) {
					bookingType = tnxResProcess.getBookingType(fareSegChargeTOs.get(0));
				}

				QuotedFareRebuildDTO fareInfo = new QuotedFareRebuildDTO(fareSegChargeTOs, paxCountAssembler,
						discountedFareDetails, bookingType);

				promotionRQ.setQuotedFareRebuildDTO(fareInfo);

			}

			ReservationDiscountDTO resDiscountDTO = AAServicesModuleUtils.getReservationBD().calculateDiscount(promotionRQ, null);
			aaDiscountCalcRS.setTotalDiscount(resDiscountDTO.getTotalDiscount());
			aaDiscountCalcRS.setDiscountCode(resDiscountDTO.getDiscountCode());
			BigDecimal discountPercentage = AccelAeroCalculator.getDefaultBigDecimalZero();
			if (resDiscountDTO.getDiscountPercentage() > 0) {
				discountPercentage = new BigDecimal(resDiscountDTO.getDiscountPercentage());
				aaDiscountCalcRS.setDiscountPercentage(discountPercentage);
			}
			aaDiscountCalcRS.setPromoPaxCount(resDiscountDTO.getPromoPaxCount());
			aaDiscountCalcRS.setPromotionId(resDiscountDTO.getPromotionId());

			Map<Integer, PaxDiscountDetailTO> paxDiscountDetails = resDiscountDTO.getPaxDiscountDetails();

			List<PaxDiscountDetails> lccPaxDiscountDetails = null;
			if (paxDiscountDetails != null && !paxDiscountDetails.isEmpty()) {
				lccPaxDiscountDetails = new ArrayList<PaxDiscountDetails>();
				for (Integer paxSequence : paxDiscountDetails.keySet()) {
					PaxDiscountDetailTO paxDiscountDetailTO = paxDiscountDetails.get(paxSequence);

					PaxDiscountDetails paxDiscountDetail = new PaxDiscountDetails();
					paxDiscountDetail.setPaxSequence(paxDiscountDetailTO.getPaxSequence());
					paxDiscountDetail.setPaxType(paxDiscountDetailTO.getPaxType());

					List<DiscountCharge> lccPaxDiscountCharges = convertToDiscountCharge(paxDiscountDetailTO
							.getPaxDiscountChargeTOs());
					if (lccPaxDiscountCharges != null && !lccPaxDiscountCharges.isEmpty()) {
						paxDiscountDetail.getPaxDiscountCharges().addAll(lccPaxDiscountCharges);
					}

					List<DiscountCharge> lccInfantDiscountCharges = convertToDiscountCharge(paxDiscountDetailTO
							.getInfantDiscountChargeTOs());
					if (lccInfantDiscountCharges != null && !lccInfantDiscountCharges.isEmpty()) {
						paxDiscountDetail.getInfantDiscountCharges().addAll(lccInfantDiscountCharges);
					}

					lccPaxDiscountDetails.add(paxDiscountDetail);

				}
				aaDiscountCalcRS.getPaxDiscountDetails().addAll(lccPaxDiscountDetails);
			}

		} catch (Exception exp) {
			log.error("calculatePromotionDiscount(aaAirRequoteBalanceRQ) failed.", exp);
			AAExceptionUtil.addAAErrrors(aaDiscountCalcRS.getResponseAttributes().getErrors(), exp);
		} finally {
			int numOfErrors = aaDiscountCalcRS.getResponseAttributes().getErrors().size();
			AASuccess successType = (numOfErrors == 0 ? new AASuccess() : null);
			aaDiscountCalcRS.getResponseAttributes().setSuccess(successType);
		}
		return aaDiscountCalcRS;
	}

	private List<DiscountCharge> convertToDiscountCharge(List<DiscountChargeTO> paxDiscountChargeTOs) {
		List<DiscountCharge> lccDiscountCharges = null;
		if (paxDiscountChargeTOs != null && !paxDiscountChargeTOs.isEmpty()) {
			lccDiscountCharges = new ArrayList<DiscountCharge>();
			for (DiscountChargeTO discChargeTO : paxDiscountChargeTOs) {
				DiscountCharge lccDiscountCharge = new DiscountCharge();
				lccDiscountCharge.setChargeCode(discChargeTO.getChargeCode());
				lccDiscountCharge.setChargeGroupCode(discChargeTO.getChargeGroupCode());
				if (discChargeTO.getRateId() != null) {
					lccDiscountCharge.setRateId(discChargeTO.getRateId());
				}
				lccDiscountCharge.setDiscountAmount(discChargeTO.getDiscountAmount());
				lccDiscountCharge.setSegmentCode(discChargeTO.getSegmentCode());
				lccDiscountCharge.getFlightSegIds().addAll(discChargeTO.getFlightSegmentIds());			
				lccDiscountCharge.setExternalChargeCode(discChargeTO.getExternalChargeCode());
				lccDiscountCharges.add(lccDiscountCharge);
			}

		}

		return lccDiscountCharges;
	}

}
