/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.aaservices.core.bl.reservation;

import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import com.isa.connectivity.profiles.maxico.v1.common.dto.PassengerRefund;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaymentDetails;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAirBookRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAErrorCode;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAPassengerRefundRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAPassengerRefundRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAReadRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAReservationUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.AAUtils;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.model.IPassenger;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.assembler.PassengerAssembler;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.core.remoting.ejb.ReservationServiceBean;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Business class to communicate with the {@link ReservationServiceBean} which wraps the passenger refund logic.
 * 
 * @author Abheek Das Gupta
 * 
 */
public class AAPassengerRefundBL {

	private Logger logger = Logger.getLogger(AAPassengerRefundBL.class);

	public AAPassengerRefundRS execute(AAPassengerRefundRQ aaPassengerRefundRQ) {
		AAPassengerRefundRS aaPassengerRefundRS = new AAPassengerRefundRS();
		aaPassengerRefundRS.setResponseAttributes(new AAResponseAttributes());

		try {
			IPassenger iPassenger = new PassengerAssembler(null);

			String userNotes = null;
			List<String> preferredRefundOrder = null;
			List<Long> pnrPaxOndChgIds = null;

			List<PassengerRefund> paxRefunds = aaPassengerRefundRQ.getRefund();

			if (paxRefunds != null && !paxRefunds.isEmpty()) {
				String groupPnr = aaPassengerRefundRQ.getGroupPnr();
				Reservation reservation = getReservation(groupPnr);

				// Single transaction refunds will only send transaction
				// granuality info
				userNotes = paxRefunds.get(0).getPaymentDetails().get(0).getRemarks();
				preferredRefundOrder = paxRefunds.get(0).getPreferredRefundOrder();
				pnrPaxOndChgIds = paxRefunds.get(0).getPnrPaxOndChgIds();

				// Each passenger refund request denotes a passenger refund
				// request which can contain mulitple payment refunds
				for (PassengerRefund paxRefund : paxRefunds) {

					Integer pnrPaxId = null;

					// Determine the PNR_PAX_ID for the given traveler reference
					// number.
					String travelerRefNumber = paxRefund.getTravelerRefNumber();
					for (ReservationPax reservationPax : (Set<ReservationPax>) reservation.getPassengers()) {
						if (travelerRefNumber.indexOf(reservationPax.getPnrPaxId().toString()) >= 0) {
							pnrPaxId = reservationPax.getPnrPaxId();
							break;
						}
					}

					if (pnrPaxId == null) {
						throw new WebservicesException(
								AAErrorCode.ERR_2_MAXICO_REQUIRED_INVALID_TRAVELLER_REFERENCE_NUMBER,
								"Could not find a traveler " + "with the given traveler reference number ("
										+ travelerRefNumber + ") " + "for the PNR : " + groupPnr);
					}

					List<PaymentDetails> paymentDetailsList = paxRefund.getPaymentDetails();
					PaymentAssembler reservationPayments = new PaymentAssembler();
					AAUtils.updatePaxTransactionIds(paymentDetailsList, pnrPaxId);
					AAUtils.extractReservationPayments(paymentDetailsList, reservationPayments);
					iPassenger.addPassengerPayments(pnrPaxId, reservationPayments);

				}

				ServiceResponce serviceResponce = AAServicesModuleUtils.getPassengerBD().groupPassengerRefund(groupPnr,
						iPassenger, userNotes, false, reservation.getVersion(),
						AAUtils.getTrackInfo(aaPassengerRefundRQ.getAaPos()), false, preferredRefundOrder,
						pnrPaxOndChgIds, false, false);
				// ServiceResponce serviceResponce =
				// AAServicesModuleUtils.getPassengerBD().passengerRefund(groupPnr,
				// iPassenger, originalPaymentTnxId,
				// paymentDetails.getRemarks(), false, reservation.getVersion(),
				// AAUtils.getTrackInfo(aaPassengerRefundRQ.getAaPos()), false,
				// preferredRefundOrder, pnrPaxOndChgIds);
				if (serviceResponce.isSuccess()) {
					aaPassengerRefundRS.getResponseAttributes().setSuccess(new AASuccess());
					AAAirBookRS aaReservation = loadReservation(groupPnr);
					aaPassengerRefundRS.setAaAirReservation(aaReservation.getAaAirReservation());
				} else {
					throw new WebservicesException(AAErrorCode.ERR_13_MAXICO_REQUIRED_UNDETERMINED_CAUSE_PLEASE_REPORT,
							"Passenger refund failed with error code : " + serviceResponce.getResponseCode());
				}
			}

		} catch (Exception e) {
			logger.error("ERROR", e);
			AAExceptionUtil.addAAErrrors(aaPassengerRefundRS.getResponseAttributes().getErrors(), e);
		}

		return aaPassengerRefundRS;
	}

	private Reservation getReservation(String groupPnr) throws ModuleException {
		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(groupPnr);
		pnrModesDTO.setLoadFares(true);
		pnrModesDTO.setLoadOndChargesView(true);
		pnrModesDTO.setLoadPaxAvaBalance(true);
		pnrModesDTO.setLoadSegViewBookingTypes(true);
		pnrModesDTO.setLoadSegView(true);
		pnrModesDTO.setLoadLastUserNote(true);
		pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
		pnrModesDTO.setRecordAudit(false);

		Reservation reservation = AAServicesModuleUtils.getReservationBD().getReservation(pnrModesDTO, null);
		return reservation;
	}

	private AAAirBookRS loadReservation(String pnr) {
		AAReadRQ aaReadRQ = new AAReadRQ();
		aaReadRQ.setPnr(pnr);
		aaReadRQ.setHeaderInfo(AAUtils.populateHeaderInfo(null));
		aaReadRQ.setLoadReservationOptions(AAReservationUtil.createLoadReservationOptions());
		AASearchReservationByPnrBL aaSearchReservationBL = new AASearchReservationByPnrBL();
		aaSearchReservationBL.setRequest(aaReadRQ);
		return aaSearchReservationBL.execute();
	}
}
