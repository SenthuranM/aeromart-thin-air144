package com.isa.thinair.aaservices.core.util;

import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.airsecurity.api.model.Privilege;
import com.isa.thinair.airsecurity.api.service.SecurityBD;
import com.isa.thinair.commons.api.exception.ModuleException;

public class SecurityUtil {
	private static Map<String, Privilege> privilegeMap = new HashMap<>();

	private static SecurityBD getSecurityBD() {
		return AAServicesModuleUtils.getSecurityBD();
	}

	public static Privilege getPrivilege(String privilegeId) throws ModuleException {
		if (!privilegeMap.keySet().contains(privilegeId)) {
			Privilege p = getSecurityBD().getPrivilege(privilegeId);
			if (p != null) {
				privilegeMap.put(privilegeId, p);
			}
		}
		return privilegeMap.get(privilegeId);
	}
}
