package com.isa.thinair.aaservices.core.util;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airreservation.api.model.ReservationPaxOndCharge;

/**
 * Pax Wise Charge Holder to fetch the reflective charge dates
 * 
 * @author Dilan Anuruddha
 * 
 */
public class AAPnrPaxChargeHolder {

	private Map<String, Map<BigDecimal, Set<ReservationPaxOndCharge>>> chargeMap = new HashMap<String, Map<BigDecimal, Set<ReservationPaxOndCharge>>>();

	public void addCharge(ReservationPaxOndCharge reservationPaxOndCharge) {
		if (!chargeMap.containsKey(reservationPaxOndCharge.getChargeGroupCode())) {
			chargeMap.put(reservationPaxOndCharge.getChargeGroupCode(), new HashMap<BigDecimal, Set<ReservationPaxOndCharge>>());
		}
		if (!chargeMap.get(reservationPaxOndCharge.getChargeGroupCode()).containsKey(reservationPaxOndCharge.getAmount())) {
			chargeMap.get(reservationPaxOndCharge.getChargeGroupCode()).put(reservationPaxOndCharge.getAmount(),
					new HashSet<ReservationPaxOndCharge>());
		}

		chargeMap.get(reservationPaxOndCharge.getChargeGroupCode()).get(reservationPaxOndCharge.getAmount())
				.add(reservationPaxOndCharge);
	}

	/**
	 * Based on the charge group code and reflective charge value (opposite sign value) of the ReservationPaxOndCharge
	 * passed the method, will return the the charge date of the already added matching charge if any. Else it will
	 * return the passed charge date
	 * 
	 * Reason for using this in AAServices is to negate the effect of individual charge conversion for refunded charges
	 * (due to modification or cancellation) which could result balance to pay or credit if the LCC exchange rate
	 * changes in between modification charge date and first charge date.
	 * 
	 * @param reservationPaxOndCharge
	 * @return
	 */
	public Date getZuluChargeDate(ReservationPaxOndCharge reservationPaxOndCharge) {
		if (chargeMap.containsKey(reservationPaxOndCharge.getChargeGroupCode())
				&& chargeMap.get(reservationPaxOndCharge.getChargeGroupCode()).containsKey(
						reservationPaxOndCharge.getAmount().negate())) {
			Set<ReservationPaxOndCharge> chargeSet = chargeMap.get(reservationPaxOndCharge.getChargeGroupCode()).get(
					reservationPaxOndCharge.getAmount().negate());
			if (chargeSet.size() > 0) {
				ReservationPaxOndCharge chg = chargeSet.iterator().next();
				chargeSet.remove(chg);
				return chg.getZuluChargeDate();
			} else {
				return reservationPaxOndCharge.getZuluChargeDate();
			}
		}
		return reservationPaxOndCharge.getZuluChargeDate();
	}
}
