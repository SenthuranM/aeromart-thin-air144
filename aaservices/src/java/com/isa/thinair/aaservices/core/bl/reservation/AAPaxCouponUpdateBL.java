package com.isa.thinair.aaservices.core.bl.reservation;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.PaxCouponUpdateRequest;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAPaxCouponUpdateRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAPaxCouponUpdateRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.airproxy.api.model.reservation.dto.PassengerCouponUpdateRQ;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.auditor.api.dto.PassengerTicketCouponAuditDTO;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class AAPaxCouponUpdateBL {
	private static final Log log = LogFactory.getLog(AAPaxCouponUpdateBL.class);

	private AAPaxCouponUpdateRQ aaPaxCouponUpdateRQ;

	public void setAAPaxCouponUpdateRQ(AAPaxCouponUpdateRQ aaPaxCouponUpdateRQ) {
		this.aaPaxCouponUpdateRQ = aaPaxCouponUpdateRQ;
	}

	public AAPaxCouponUpdateRS execute() {

		AAPaxCouponUpdateRS aaPaxCouponUpdateRS = new AAPaxCouponUpdateRS();

		try {

			aaPaxCouponUpdateRS.setResponseAttributes(new AAResponseAttributes());

			if (log.isDebugEnabled()) {
				log.debug("Pax Coupon Update on : " + AppSysParamsUtil.getDefaultCarrierCode() + " STARTED");
			}

			PaxCouponUpdateRequest aaPaxCouponUpdateRequest = aaPaxCouponUpdateRQ.getPaxCouponUpdateRequest();
			PassengerCouponUpdateRQ paxCouponUpdateRQ = new PassengerCouponUpdateRQ();
			paxCouponUpdateRQ.setCarrierCode(aaPaxCouponUpdateRequest.getCarrierCode());
			paxCouponUpdateRQ.setEticketId(aaPaxCouponUpdateRequest.getEticketId());
			paxCouponUpdateRQ.setEticketStatus(aaPaxCouponUpdateRequest.getEticketStatus());
			paxCouponUpdateRQ.setPaxStatus(aaPaxCouponUpdateRequest.getPaxStatus());
			if (aaPaxCouponUpdateRequest.getCouponAudit() != null) {
				PassengerTicketCouponAuditDTO couponAudit = new PassengerTicketCouponAuditDTO();
				couponAudit.setPassengerName(aaPaxCouponUpdateRequest.getCouponAudit().getPassengerName());
				couponAudit.setPnr(aaPaxCouponUpdateRequest.getCouponAudit().getPnr());
				couponAudit.setRemark(aaPaxCouponUpdateRequest.getCouponAudit().getRemark());
				paxCouponUpdateRQ.setCouponAudit(couponAudit);
			}
			TrackInfoDTO trackInfo = null;
			//TODO: get allowExceptions through lcc
			AirproxyModuleUtils.getPassengerBD().updatePassengerCoupon(paxCouponUpdateRQ, trackInfo, true);

			aaPaxCouponUpdateRS.getResponseAttributes().setSuccess(new AASuccess());

			if (log.isDebugEnabled()) {
				log.debug("Pax Coupon Update on : " + AppSysParamsUtil.getDefaultCarrierCode() + " FINISHED");
			}

		} catch (Exception ex) {
			log.error("Clear segment alerts failed ", ex);
			AAExceptionUtil.addAAErrrors(aaPaxCouponUpdateRS.getResponseAttributes().getErrors(), ex);
		}
		return aaPaxCouponUpdateRS;
	}
}
