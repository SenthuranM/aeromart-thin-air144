package com.isa.thinair.aaservices.core.bl.reservation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.isa.connectivity.profiles.maxico.v1.common.dto.ChargerOverride;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaxOndFlexibility;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAirBookModifyRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAErrorCode;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAModificationTypeCode;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.aaservices.core.util.AAReservationUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.TransformerUtil;
import com.isa.thinair.airproxy.api.dto.ReservationConstants;
import com.isa.thinair.airreservation.api.dto.CustomChargesTO;
import com.isa.thinair.airreservation.api.dto.ExternalSegmentTO;
import com.isa.thinair.airreservation.api.dto.flexi.ReservationPaxOndFlexibilityDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.webplatform.api.util.PrivilegesKeys;

/**
 * @author Nilindra Fernando
 */
class AACancelSegmentBL {

	private Map<Integer, Collection<ReservationPaxOndFlexibilityDTO>> updatedFlexibilitiesMap = null;

	/**
	 * Cancels the given OND
	 * 
	 * @param reservation
	 * @param aaAirBookModifyRQ
	 * @param privilegeKeys
	 * @throws WebservicesException
	 * @throws ModuleException
	 */
	void cancelOND(Reservation reservation, AAAirBookModifyRQ aaAirBookModifyRQ, Collection privilegeKeys)
			throws WebservicesException, ModuleException {

		String pnr = aaAirBookModifyRQ.getAaReservation().getPnr();
		long version = Long.parseLong(aaAirBookModifyRQ.getAaReservation().getAirReservation().getVersion());
		Collection<Collection<Integer>> ondWisePnrSegIds = AAModifyReservationUtil.getOndWiseOwnPnrSegIds(reservation,
				aaAirBookModifyRQ.getAaReservation().getAirReservation().getAirItinerary());

		int ondCount = ondWisePnrSegIds.size();

		boolean isNewOndExists = false;
		Collection<Integer> newFltSegIds = null;
		if (aaAirBookModifyRQ.getNewAAReservation() != null) {
			isNewOndExists = AAModifyReservationUtil.isOndsExists(aaAirBookModifyRQ.getNewAAReservation().getAirReservation()
					.getAirItinerary());
			if (ondCount == 1) {
				newFltSegIds = AAModifyReservationUtil.getFltSegIds(aaAirBookModifyRQ.getNewAAReservation().getAirReservation()
						.getAirItinerary());
			}
		}

		AAModificationTypeCode actualModTypeCode = aaAirBookModifyRQ.getActualModificationTypeCode();

		for (Collection<Integer> pnrSegIds : ondWisePnrSegIds) {
			AAModifyReservationUtil.authorizeModification(privilegeKeys, reservation,
					PrivilegesKeys.AlterResPrivilegesKeys.ALT_BUFFERTIME_SEG_OR_PAX, pnrSegIds, actualModTypeCode);

			if (AAModifyReservationUtil.checkIfSegsAlreadyCancelled(reservation, pnrSegIds)) {
				throw new WebservicesException(AAErrorCode.ERR_35_MAXICO_REQUIRED_SEGMENT_IS_ALREADY_CANCELLED_BY_ANOTHER_USER,
						"");
			}
		}

		ChargerOverride chargeOverride = aaAirBookModifyRQ.getChargeOverride();
		AAReservationUtil.recalculateChargeOverride(chargeOverride, ondCount);

		ServiceResponce serviceResponce;
		for (Collection<Integer> pnrSegIds : ondWisePnrSegIds) {
			if (isNewOndExists) {
				CustomChargesTO customChargesTO = TransformerUtil.transformCustomCharges(chargeOverride,
						ReservationConstants.AlterationType.ALT_MODIFY_OND);
				serviceResponce = AAServicesModuleUtils.getSegmentBD().cancelSegmentsApplyingModCharge(pnr, pnrSegIds,
						newFltSegIds, customChargesTO, version, true, null,aaAirBookModifyRQ.getUserNote(), aaAirBookModifyRQ.isHasExternalPayments());
			} else {
				CustomChargesTO customChargesTO = TransformerUtil.transformCustomCharges(chargeOverride,
						ReservationConstants.AlterationType.ALT_CANCEL_OND);
				serviceResponce = AAServicesModuleUtils.getSegmentBD().cancelSegments(pnr, pnrSegIds, customChargesTO, version,
						true, null, aaAirBookModifyRQ.isHasExternalPayments(), aaAirBookModifyRQ.getUserNote(), true);
			}

			updatedFlexibilitiesMap = (Map<Integer, Collection<ReservationPaxOndFlexibilityDTO>>) serviceResponce
					.getResponseParam(CommandParamNames.UPDATED_PAX_FLEXIBILITIES_MAP);
			version = ((Long) serviceResponce.getResponseParam(CommandParamNames.VERSION));

		}

		Collection<ExternalSegmentTO> ondWiseExternalFlightSegments = AAModifyReservationUtil.getOndWiseExternalFlightSegments(
				aaAirBookModifyRQ.getAaReservation().getAirReservation().getAirItinerary(),
				ReservationInternalConstants.ReservationExternalSegmentStatus.CANCEL);

		if (ondWiseExternalFlightSegments != null && ondWiseExternalFlightSegments.size() > 0) {
			// Collection<ExternalSegmentTO> colExternalSegmentTO = new ArrayList<ExternalSegmentTO>();
			// for (FlightSegment flightSegment : ondWiseExternalFlightSegments) {
			// ExternalSegmentTO externalSegmentTO = new ExternalSegmentTO();
			// externalSegmentTO.setFlightNo(flightSegment.getFlightNumber());
			// externalSegmentTO.setDepartureDate(flightSegment.getDepatureDateTime());
			// externalSegmentTO.setArrivalDate(flightSegment.getArrivalDateTime());
			// externalSegmentTO.setSegmentCode(flightSegment.getSegmentCode());
			// externalSegmentTO.setCabinClassCode(flightSegment.getTravelPreferences().getCabinClassCode());
			// externalSegmentTO.setStatus(ReservationInternalConstants.ReservationExternalSegmentStatus.CANCEL);
			//
			// colExternalSegmentTO.add(externalSegmentTO);
			// }

			Reservation tmpReservation = AAReservationUtil.getReservation(pnr, null, false, false, false, false, false, false,
					false);
			AAServicesModuleUtils.getSegmentBD().changeExternalSegments(pnr, ondWiseExternalFlightSegments,
					tmpReservation.getVersion(), null);
		}
	}

	public Map<Integer, Collection<ReservationPaxOndFlexibilityDTO>> getUpdatedFlexibilitiesMap() {
		return updatedFlexibilitiesMap;
	}

	public List<PaxOndFlexibility> getUpdatedFlexibilitiesList() {
		List<PaxOndFlexibility> updatedFlexibilitiesList = new ArrayList<PaxOndFlexibility>();
		if (updatedFlexibilitiesMap != null) {
			Iterator entries = updatedFlexibilitiesMap.entrySet().iterator();
			while (entries.hasNext()) {
				Entry entry = (Entry) entries.next();
				Integer pnrPaxId = (Integer) entry.getKey();
				List<ReservationPaxOndFlexibilityDTO> paxFlexi = (List<ReservationPaxOndFlexibilityDTO>) entry.getValue();
				for (ReservationPaxOndFlexibilityDTO reservationPaxOndFlexibilityDTO : paxFlexi) {
					PaxOndFlexibility paxOndFlexibility = new PaxOndFlexibility();
					paxOndFlexibility.setAvailableCount(reservationPaxOndFlexibilityDTO.getAvailableCount());
					paxOndFlexibility.setCutOverBufferInMins(reservationPaxOndFlexibilityDTO.getCutOverBufferInMins());
					paxOndFlexibility.setDescription(reservationPaxOndFlexibilityDTO.getDescription());
					paxOndFlexibility.setFlexibilityTypeId(reservationPaxOndFlexibilityDTO.getFlexibilityTypeId());
					paxOndFlexibility.setFlexiRateId(reservationPaxOndFlexibilityDTO.getFlexibilityTypeId());
					paxOndFlexibility.setPpfId(reservationPaxOndFlexibilityDTO.getPpfId());
					paxOndFlexibility.setPpOndFlxId(reservationPaxOndFlexibilityDTO.getPpOndFlxId());
					paxOndFlexibility.setStatus(reservationPaxOndFlexibilityDTO.getStatus());
					paxOndFlexibility.setUtilizedCount(reservationPaxOndFlexibilityDTO.getUtilizedCount());
					paxOndFlexibility.setPnrPaxId(pnrPaxId);
					updatedFlexibilitiesList.add(paxOndFlexibility);
				}
			}
		}
		return updatedFlexibilitiesList;
	}

	public void setUpdatedFlexibilitiesMap(Map<Integer, Collection<ReservationPaxOndFlexibilityDTO>> updatedFlexibilitiesMap) {
		this.updatedFlexibilitiesMap = updatedFlexibilitiesMap;
	}
}
