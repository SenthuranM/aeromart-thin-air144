package com.isa.thinair.aaservices.core.bl.paxcredit;

import java.util.ArrayList;
import java.util.Collection;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AirTraveler;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingFlightSegment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ContactInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.CustomerCredit;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PassengerType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PassengerTypeQuantity;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PersonName;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ReservationPassengerDetailInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SegmentStatus;
import com.isa.thinair.aaservices.core.util.PaxTypeUtils;
import com.isa.thinair.airreservation.api.dto.PaxCreditDTO;
import com.isa.thinair.airreservation.api.dto.ReservationExternalSegmentTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaxDTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaxDetailsDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * Util to transform ReservationPassengerDetailInfo to ReservationPaxDTO and vis versa
 * 
 * @author mekanayake
 * 
 */
class PaxCreditUtil {

	public static ReservationPassengerDetailInfo populateReservationPassengerDetailInfo(ReservationPaxDTO dto) {

		ReservationPassengerDetailInfo detailInfo = new ReservationPassengerDetailInfo();
		detailInfo.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
		detailInfo.setPnr(dto.getPnr());
		detailInfo.setPnrDate(dto.getPnrDate());
		detailInfo.setStatus(dto.getStatus());

		// populating pax count
		detailInfo.getPassengerTypeQuantity().add(populatePassengerTypeQuantity(PassengerType.ADT, dto.getAdultCount()));
		detailInfo.getPassengerTypeQuantity().add(populatePassengerTypeQuantity(PassengerType.CHD, dto.getChildCount()));
		detailInfo.getPassengerTypeQuantity().add(populatePassengerTypeQuantity(PassengerType.INF, dto.getInfantCount()));
		// populating contact info.
		detailInfo.setContactInfo(populateContactInfo(dto.getReservationContactInfo()));
		// populating passenger info
		detailInfo.getPassengers().addAll(populatePassengers(dto.getPassengers()));
		// populating segment info
		detailInfo.getFlightSegments().addAll(populateFlightSegments(dto.getSegments()));
		// populating external segments info
		detailInfo.getExternalflightSegments().addAll(populateExternalFlightSegments(dto.getColExternalSegments()));

		return detailInfo;
	}

	private static ContactInfo populateContactInfo(ReservationContactInfo resContactInfo) {
		ContactInfo contactInfo = new ContactInfo();
		PersonName name = new PersonName();
		name.setFirstName(resContactInfo.getFirstName());
		name.setSurName(resContactInfo.getLastName());
		contactInfo.setPersonName(name);
		return contactInfo;
	}

	private static Collection<AirTraveler> populatePassengers(Collection<ReservationPaxDetailsDTO> paxCol) {
		Collection<AirTraveler> airTravelers = new ArrayList<AirTraveler>();
		if (paxCol != null && !paxCol.isEmpty()) {
			for (ReservationPaxDetailsDTO paxDetailsDTO : paxCol) {
				AirTraveler airTraveler = new AirTraveler();
				airTraveler.setPnrPaxId(paxDetailsDTO.getPnrPaxId());
				airTraveler.setPassengerType(PaxTypeUtils.convertAAPaxCodeToLCCPaxCodes(paxDetailsDTO.getPaxType()));
				airTraveler.setCredit(paxDetailsDTO.getCredit());
				// Note : we need to have two separate values for credit amount and base amount. There is not difference
				// at this point. But we may convert the credit amount from credit residing carrier -> OC -> MC
				// So this is used to trac the original amount and based on this utilizing credit percentage will
				// decided
				airTraveler.setResidingCarrierCredit(paxDetailsDTO.getCredit());
				PersonName name = new PersonName();
				name.setFirstName(paxDetailsDTO.getFirstName());
				name.setSurName(paxDetailsDTO.getLastName());
				name.setTitle(paxDetailsDTO.getTitle());
				airTraveler.setPersonName(name);
				airTravelers.add(airTraveler);
			}
		}
		return airTravelers;
	}

	private static PassengerTypeQuantity populatePassengerTypeQuantity(PassengerType paxType, int count) {
		PassengerTypeQuantity passengerTypeQuantity = new PassengerTypeQuantity();
		passengerTypeQuantity.setPassengerType(paxType);
		passengerTypeQuantity.setQuantity(count);
		return passengerTypeQuantity;
	}

	private static Collection<BookingFlightSegment> populateFlightSegments(Collection<ReservationSegmentDTO> segments) {
		Collection<BookingFlightSegment> bookingFlightSegments = new ArrayList<BookingFlightSegment>();
		if (segments != null && !segments.isEmpty()) {
			for (ReservationSegmentDTO segmentDTO : segments) {
				BookingFlightSegment flightSegment = new BookingFlightSegment();
				flightSegment.setFlightNumber(segmentDTO.getFlightNo());
				flightSegment.setSegmentCode(segmentDTO.getSegmentCode());
				flightSegment.setOperatingAirline(segmentDTO.getCarrierCode());
				flightSegment.setDepatureDateTime(segmentDTO.getDepartureDate());
				flightSegment.setDepatureDateTimeZulu(segmentDTO.getZuluDepartureDate());
				flightSegment.setArrivalDateTime(segmentDTO.getArrivalDate());
				flightSegment.setArrivalDateTimeZulu(segmentDTO.getZuluArrivalDate());
				bookingFlightSegments.add(flightSegment);
			}
		}
		return bookingFlightSegments;
	}

	private static Collection<BookingFlightSegment> populateExternalFlightSegments(
			Collection<ReservationExternalSegmentTO> colExternalSegments) {
		Collection<BookingFlightSegment> exSegments = new ArrayList<BookingFlightSegment>();
		if (colExternalSegments != null && !colExternalSegments.isEmpty()) {
			for (ReservationExternalSegmentTO externalSegmentTO : colExternalSegments) {
				BookingFlightSegment flightSegment = new BookingFlightSegment();
				flightSegment.setFlightNumber(externalSegmentTO.getFlightNo());
				flightSegment.setStatus(SegmentStatus.valueOf(externalSegmentTO.getStatus()));
				flightSegment.setSegmentCode(externalSegmentTO.getSegmentCode());
				flightSegment.setDepatureDateTime(externalSegmentTO.getDepartureDate());
				flightSegment.setSegmentSequence(externalSegmentTO.getSegmentSeq());
				flightSegment.setArrivalDateTime(externalSegmentTO.getArrivalDate());
				exSegments.add(flightSegment);
			}
		}
		return exSegments;
	}

	static Collection<CustomerCredit> populateCustomerCredit(Collection<PaxCreditDTO> paxCreditList) {
		Collection<CustomerCredit> customerCreditList = new ArrayList<CustomerCredit>();
		for (PaxCreditDTO paxCreditDTO : paxCreditList) {
			CustomerCredit customerCredit = new CustomerCredit();
			customerCredit.setPnr(paxCreditDTO.getPnr());
			customerCredit.setFirstName(paxCreditDTO.getFirstName());
			customerCredit.setLastName(paxCreditDTO.getLastName());
			customerCredit.setPaxSequence(paxCreditDTO.getPaxSequence());
			customerCredit.setAmount(paxCreditDTO.getBalance());
			customerCredit.setExpireDate(paxCreditDTO.getDateOfExpiry());
			customerCreditList.add(customerCredit);
		}
		return customerCreditList;
	}

}
