package com.isa.thinair.aaservices.core.bl.dummycarrierfulfillment;

import com.isa.connectivity.profiles.maxico.v1.required.dto.AAPOS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAPayCarrierFulfullmentRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAReservation;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.commons.api.exception.ModuleException;

abstract class BaseMarketingCarrierFulfillment {

	protected AAReservation aaAirReservation;

	protected AAReservation newAAAirReservation;

	protected AAPOS aaPos;

	BaseMarketingCarrierFulfillment(AAReservation aaAirReservation, AAReservation newAAAirReservation, AAPOS aaPos) {
		super();
		this.aaAirReservation = aaAirReservation;
		this.newAAAirReservation = newAAAirReservation;
		this.aaPos = aaPos;
	}

	public abstract AAPayCarrierFulfullmentRS execute() throws WebservicesException, ModuleException;

	public abstract void checkConstraints() throws WebservicesException;

	Reservation getCarrierReservation(LCCClientPnrModesDTO pnrModesDTO) throws ModuleException {
		// Try to load the reservation by originator pnr
		return AAServicesModuleUtils.getReservationBD().getReservation(pnrModesDTO, null);
	}

}
