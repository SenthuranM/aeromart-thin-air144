
package com.isa.thinair.aaservices.core.bl.reservation;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AirTraveler;
import com.isa.connectivity.profiles.maxico.v1.common.dto.HeaderInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TravelerInfo;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AACheckBlacklistPaxRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AACheckBlacklistPaxRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.airproxy.api.model.reservation.commons.BlacklisPaxReservationTO;
import com.isa.thinair.airreservation.api.model.BlacklistPAX;
import com.isa.thinair.airreservation.api.utils.BlacklistPAXUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.lccclient.api.util.LCCClientApiUtils;
import com.isa.thinair.lccclient.api.util.LCCUtils;


public class AABlacklistPaxCheckBL {

	/**
	 * @author subash
	 * **/
	
	private static Log log = LogFactory.getLog(AABlacklistPaxCheckBL.class);
	
	AACheckBlacklistPaxRQ aaCheckBlacklistPaxRQ;
		
	public AABlacklistPaxCheckBL(AACheckBlacklistPaxRQ aaCheckBlacklistPaxRQ){
		this.aaCheckBlacklistPaxRQ = aaCheckBlacklistPaxRQ;
	}
	
	public AACheckBlacklistPaxRS execute() {
		AACheckBlacklistPaxRS aaCheckBlacklistPaxRS = new AACheckBlacklistPaxRS();
		
		try{
			
			boolean onlyFirstElement = aaCheckBlacklistPaxRQ.isOnlyFirstElement();
			List<BlacklisPaxReservationTO> blpaxToList = createToList(aaCheckBlacklistPaxRQ.getTravelerInfo().getAirTraveler()); 
			List<BlacklistPAX> blPaxList = AAServicesModuleUtils.getBlacklistPAXBD().getBalcklistedPaxReservation(blpaxToList,onlyFirstElement);
				TravelerInfo travellerInfo = new TravelerInfo();
				AAResponseAttributes aaResAttr = new AAResponseAttributes();
				aaCheckBlacklistPaxRS.setResponseAttributes(aaResAttr);
				aaCheckBlacklistPaxRS.setTravelerInfo(travellerInfo);
				aaCheckBlacklistPaxRS.getTravelerInfo().getAirTraveler().addAll(createAirTravelerList(blPaxList));
				
		}catch(Exception e){
			log.error("EEROR @ AABlacklistPaxCheckBL execute"+ e.getLocalizedMessage());
			AAResponseAttributes aaresponseattr = new AAResponseAttributes();
			aaCheckBlacklistPaxRS.setResponseAttributes(aaresponseattr);
			AAExceptionUtil.addAAErrrors(aaCheckBlacklistPaxRS.getResponseAttributes().getErrors(), e);			
		
		}finally{
			if(aaCheckBlacklistPaxRS.getResponseAttributes() ==null){
				AAResponseAttributes aaresponseattr = new AAResponseAttributes();
				aaCheckBlacklistPaxRS.setResponseAttributes(aaresponseattr);
			}
			if(aaCheckBlacklistPaxRS.getResponseAttributes().getErrors().isEmpty()){
				aaCheckBlacklistPaxRS.getResponseAttributes().setSuccess(new AASuccess());
			}
		}
		
		
		return aaCheckBlacklistPaxRS;
	}

	private List<BlacklisPaxReservationTO> createToList(List<AirTraveler> airTraveler) throws ModuleException {
		List<BlacklisPaxReservationTO> blpaxToList = new ArrayList<BlacklisPaxReservationTO>();
		
		for(AirTraveler airtravelarPax : airTraveler){
			BlacklisPaxReservationTO blPaxTo = new BlacklisPaxReservationTO();
			blPaxTo.setFullName(airtravelarPax.getPersonName().getFirstName()+BlacklistPAXUtil.PAX_NAME_SEPARATOR+ airtravelarPax.getPersonName().getSurName());
			if(!StringUtil.isNullOrEmpty(airtravelarPax.getNationalityCode())){
				blPaxTo.setNationalityCode(LCCUtils.getNationalityCode(airtravelarPax.getNationalityCode()));			
			}
			blPaxTo.setNationality(airtravelarPax.getNationalityName());
			if(airtravelarPax.getBirthDate()!=null){
				blPaxTo.setDateOfBirth(CalendarUtil.asDate(airtravelarPax.getBirthDate()));
			}
			if(airtravelarPax.getFormOfIdentification()!=null){
				blPaxTo.setPassportNo(airtravelarPax.getFormOfIdentification().getFoidNumber());
			}
			blpaxToList.add(blPaxTo);
		}
		
		return blpaxToList;
	}	
	
	private List<AirTraveler> createAirTravelerList(List<BlacklistPAX> blPaxList){
		
		List<AirTraveler> airTraveler = new ArrayList<AirTraveler>();
		for(BlacklistPAX bpax  : blPaxList){
			AirTraveler at = new AirTraveler();			
			at.setPersonName(LCCClientApiUtils.transform("", bpax.getFullName(), "")); // set Full name to Airtravllers firstname
			airTraveler.add(at);
		}
		
		return airTraveler;		
	}
	
}