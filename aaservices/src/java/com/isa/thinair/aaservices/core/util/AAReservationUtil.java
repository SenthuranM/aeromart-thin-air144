/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2006 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.aaservices.core.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.ApplicableBasis;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ChargerOverride;
import com.isa.connectivity.profiles.maxico.v1.common.dto.LCCAutoCancellationInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.LoadReservationOptions;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAErrorCode;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.aaservices.core.session.AASessionManager;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.SeatDistribution;
import com.isa.thinair.airinventory.api.dto.seatavailability.SegmentSeatDistsDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.AgentCreditInfo;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.dto.CashPaymentInfo;
import com.isa.thinair.airreservation.api.dto.CustomChargesTO;
import com.isa.thinair.airreservation.api.dto.PaxCreditInfo;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.dto.VoucherPaymentInfo;
import com.isa.thinair.airreservation.api.model.AutoCancellationInfo;
import com.isa.thinair.airreservation.api.model.IPayment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.webplatform.api.util.AuthorizationUtil;
import com.isa.thinair.webplatform.api.util.PrivilegesKeys;
import com.isa.thinair.webplatform.api.util.ReservationUtil;

public class AAReservationUtil {

	private static final Log log = LogFactory.getLog(AAReservationUtil.class);

	/**
	 * Returns the Reservation
	 * 
	 * @param pnr
	 * @param trackInfoDTO
	 * @param loadFares
	 * @param loadLastUserNote
	 * @param loadLocalTimes
	 * @param loadOndChargesView
	 * @param loadPaxAvaBalance
	 * @param loadSegView
	 * @param loadSegViewBookingTypes
	 * @return
	 * @throws ModuleException
	 */
	public static Reservation getReservation(String pnr, TrackInfoDTO trackInfoDTO, boolean loadFares, boolean loadLastUserNote,
			boolean loadLocalTimes, boolean loadOndChargesView, boolean loadPaxAvaBalance, boolean loadSegView,
			boolean loadSegViewBookingTypes) throws ModuleException {

		LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
		pnrModesDTO.setPnr(pnr);
		pnrModesDTO.setLoadFares(loadFares);
		pnrModesDTO.setLoadLastUserNote(loadLastUserNote);
		pnrModesDTO.setLoadLocalTimes(loadLocalTimes);
		pnrModesDTO.setLoadOndChargesView(loadOndChargesView);
		pnrModesDTO.setLoadPaxAvaBalance(loadPaxAvaBalance);
		pnrModesDTO.setLoadSegViewBookingTypes(loadSegViewBookingTypes);

		if (pnrModesDTO.isLoadOndChargesView()) {
			pnrModesDTO.setLoadSegView(true);
		} else {
			pnrModesDTO.setLoadSegView(loadSegView);
		}

		return AAServicesModuleUtils.getReservationBD().getReservation(pnrModesDTO, trackInfoDTO);
	}

	/**
	 * Method to authorize a functionality. If not authorized, throw WebservicesException.
	 * 
	 * @param privilegeKey
	 * @throws WebservicesException
	 */
	public static void authorize(Collection<String> privilegeKeys, String... privilegeKey) throws WebservicesException {
		for (int i = 0; i < privilegeKey.length; i++) {
			if (!AuthorizationUtil.hasPrivileges(privilegeKeys, privilegeKey[i])) {
				UserPrincipal principal = AASessionManager.getInstance().getCurrentUserPrincipal();
				log.warn("Authorizatin failure [userId=" + principal.getUserId() + ",failedPrivilege=" + privilegeKey[i] + "]");

				throw new WebservicesException(AAErrorCode.ERR_9_MAXICO_REQUIRED_AUTHORIZATION_ERROR, "Unauthorized Operation");
			}
		}
	}

	/**
	 * Will authorize the operation if one of any privileges is allowed
	 * 
	 * @param privilegeKeys
	 * @param privilegeKey
	 * @throws WebservicesException
	 */
	public static void authorizeAny(Collection<String> privilegeKeys, String... privilegeKey) throws WebservicesException {
		boolean authorized = false;
		for (int i = 0; i < privilegeKey.length; i++) {
			if (AuthorizationUtil.hasPrivileges(privilegeKeys, privilegeKey[i])) {
				authorized = true;
				break;

			}
		}
		if (!authorized) {
			throw new WebservicesException(AAErrorCode.ERR_9_MAXICO_REQUIRED_AUTHORIZATION_ERROR, "Unauthorized Operation");
		}
	}

	/**
	 * Authorizes specified mode of payment. I f payment mode is not authorized to use, then throw WebservicesException.
	 * 
	 * @param privilegeKeys
	 * @param payments
	 * @param requestingAgencyCode
	 * @throws WebservicesException
	 */
	public static void authorizePayment(Collection<String> privilegeKeys, IPayment payments, String requestingAgencyCode,
			Collection<Integer> existingPnrPaxIds) throws WebservicesException {
		if (payments != null && ((PaymentAssembler) payments).getPayments() != null
				&& ((PaymentAssembler) payments).getPayments().size() > 0) {
			for (Object paymentObj : ((PaymentAssembler) payments).getPayments()) {
				if (paymentObj instanceof AgentCreditInfo) {
					if (!((AgentCreditInfo) paymentObj).getAgentCode().equals(requestingAgencyCode)) {
						authorize(privilegeKeys, PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_ONACCOUNT_PAYMNETS_ANY);
					}
					authorize(privilegeKeys, PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_ONACCOUNT_PAYMNETS);
				} else if (paymentObj instanceof CashPaymentInfo) {
					authorize(privilegeKeys, PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_CASH_PAYMNETS);
				} else if (paymentObj instanceof CardPaymentInfo) {
					authorize(privilegeKeys, PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_CREDIT_CARD_PAYMNETS);
				} else if (paymentObj instanceof PaxCreditInfo) {
					if (existingPnrPaxIds != null) {
						// TODO - Authorize any credit transfer
					}
					authorize(privilegeKeys, PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_CREDIT_PAYMNETS);
				} else if (paymentObj instanceof VoucherPaymentInfo) {
					authorize(privilegeKeys, PrivilegesKeys.MakeResPrivilegesKeys.ACCEPT_VOUCHER_PAYMNETS);
				}
			}
		}
	}

	/**
	 * loads a travel agent
	 * 
	 * @param agentCode
	 * @return
	 * @throws ModuleException
	 */
	static Agent loadTravelAgent(String agentCode) throws ModuleException {
		Collection<String> agentCodes = new ArrayList<String>();
		agentCodes.add(agentCode);
		Collection<Agent> colAgents = AAServicesModuleUtils.getTravelAgentBD().getAgents(agentCodes);
		return (Agent) BeanUtils.getFirstElement(colAgents);
	}

	/**
	 * Is On Hold Allowed or not
	 * 
	 * @param ondFareDTOCol
	 * @return
	 */
	public static boolean isHoldAllowed(Collection<OndFareDTO> ondFareDTOCol) {
		boolean isOnholdAllowed = true;
		outer: for (Iterator collFaresIt = ondFareDTOCol.iterator(); collFaresIt.hasNext();) {
			OndFareDTO ondFareDTO = (OndFareDTO) collFaresIt.next();
			for (Iterator segIt = ondFareDTO.getSegmentSeatDistsDTO().iterator(); segIt.hasNext();) {
				SegmentSeatDistsDTO segSeatDistsDTO = (SegmentSeatDistsDTO) segIt.next();
				for (Iterator seatsIt = segSeatDistsDTO.getSeatDistribution().iterator(); seatsIt.hasNext();) {
					if (((SeatDistribution) seatsIt.next()).isOnholdRestricted()) {
						isOnholdAllowed = false;
						break outer;
					}
				}
			}
		}
		return isOnholdAllowed;
	}

	public static String getTravelerRef(ReservationPax reservationPax) {
		String travelerRef = null;
		if (reservationPax != null) {
			if (reservationPax.getPaxType().equals(PaxTypeTO.ADULT) || reservationPax.getPaxType().equals(PaxTypeTO.CHILD)
					|| (reservationPax.getPaxType().equals(PaxTypeTO.INFANT) && reservationPax.getParent() != null)) {
				travelerRef = PaxTypeUtils.travelerReference(reservationPax);
			} else {
				travelerRef = "I" + reservationPax.getPaxSequence() + "$" + reservationPax.getPnrPaxId();
			}
		}

		return travelerRef;
	}

	public static CustomChargesTO createCustomChargesTO(Reservation reservation, ChargerOverride chargeOverride,
			String alterationType) throws ModuleException {
		int ondCount = ReservationUtil.getUniqueFareIds(reservation);
		// divide the override value among OND if it's the TOT
		AAReservationUtil.recalculateChargeOverride(chargeOverride, ondCount);
		CustomChargesTO customChargesTO = TransformerUtil.transformCustomCharges(chargeOverride, alterationType);
		return customChargesTO;
	}

	public static void recalculateChargeOverride(ChargerOverride chargeOverride, int ondCount) {

		if (chargeOverride != null) {

			if (chargeOverride.getApplicableBasis() != null && chargeOverride.getApplicableBasis().equals(ApplicableBasis.TOT)
					&& chargeOverride.getCustomAdultChargeValue() != null) {
				chargeOverride.setCustomAdultChargeValue(AccelAeroCalculator.divide(chargeOverride.getCustomAdultChargeValue(),
						ondCount));
			}

			if (chargeOverride.getApplicableBasis() != null && chargeOverride.getApplicableBasis().equals(ApplicableBasis.TOT)
					&& chargeOverride.getCustomChildChargeValue() != null) {
				chargeOverride.setCustomChildChargeValue(AccelAeroCalculator.divide(chargeOverride.getCustomChildChargeValue(),
						ondCount));
			}

			if (chargeOverride.getApplicableBasis() != null && chargeOverride.getApplicableBasis().equals(ApplicableBasis.TOT)
					&& chargeOverride.getCustomInfantChargeValue() != null) {
				chargeOverride.setCustomInfantChargeValue(AccelAeroCalculator.divide(chargeOverride.getCustomInfantChargeValue(),
						ondCount));
			}
		}
	}

	/*
	 * Creates LoadReservationOptions Object from reservation search. We have the same method in LCC client.
	 */
	public static LoadReservationOptions createLoadReservationOptions() {
		// Comments extracted from Zaki's notes
		// If you are doing any modification please check it need to be applied in the
		// Lcconnect/lcc-engine-maxico/src/main/java/com/isa/lcc/maxico/core/util/CommonUtil.java
		// class as well.
		LoadReservationOptions loadReservationOptions = new LoadReservationOptions();
		loadReservationOptions.setLoadFares(true); // Loads ReservationPax into Reservation object
		loadReservationOptions.setLoadLocalTimes(true); // For performance I think it's better we convert ourselves at
														// the end
		loadReservationOptions.setLoadSegView(true); // Loads ReservationSegmentDTO into Reservation object
		loadReservationOptions.setLoadOndChargesView(true); // If this is true, then [loadFares] is set to true in code
		loadReservationOptions.setLoadSegViewReturnGroupId(true); // Loads the return group id if needed.
																	// "Use only for open return bookings"
		loadReservationOptions.setLoadSegViewBookingTypes(true); // Loads booking types
		loadReservationOptions.setLoadSegViewFareCategoryTypes(true); // If true then loadSegViewBookingTypes is set to
																		// true
		loadReservationOptions.setLoadPaxAvaBalance(true); // Zaki [Feb.11.2009] : Always keep true
		loadReservationOptions.setLoadSeatingInfo(true); // Loads Collection<PaxSeatTO> into Reservation object
		loadReservationOptions.setLoadMealInfo(true); // Cause we don't have meals in AAAirBookRS
		loadReservationOptions.setLoadBaggageInfo(true);
		loadReservationOptions.setLoadSSRInfo(true);
		loadReservationOptions.setRecordAudit(true); // I belive we should always make an audit, unless LCC will do it
														// for us.
		loadReservationOptions.setLoadLastUserNote(true); // -- BIZ DECISION = ALWAYS SET TO TRUE --
		loadReservationOptions.setLoadPaxPaymentOndBreakdownView(true); // loads the pax payment breakdown for payments
		loadReservationOptions.setLoadExternalPaxPayments(true);
		loadReservationOptions.setLoadEtickets(true);
		loadReservationOptions.setLoadOriginCountryCode(false);
		loadReservationOptions.setLoadGOQUOAmounts(false);
		loadReservationOptions.setLoadRefundableChargeDetails(false);
		return loadReservationOptions;
	}

	public static LoadReservationOptions createLoadReservationOptions(LCCClientPnrModesDTO lccClientPnrModesDTO) {
		LoadReservationOptions loadReservationOptions = new LoadReservationOptions();
		loadReservationOptions.setLoadFares(lccClientPnrModesDTO.isLoadFares());
		loadReservationOptions.setLoadLocalTimes(lccClientPnrModesDTO.isLoadLocalTimes());
		loadReservationOptions.setLoadSegView(lccClientPnrModesDTO.isLoadSegView());
		loadReservationOptions.setLoadOndChargesView(lccClientPnrModesDTO.isLoadOndChargesView());
		loadReservationOptions.setLoadSegViewReturnGroupId(lccClientPnrModesDTO.isLoadSegViewReturnGroupId());
		loadReservationOptions.setLoadSegViewBookingTypes(lccClientPnrModesDTO.isLoadSegViewBookingTypes());
		loadReservationOptions.setLoadSegViewFareCategoryTypes(lccClientPnrModesDTO.isLoadSegViewFareCategoryTypes());
		loadReservationOptions.setLoadPaxAvaBalance(lccClientPnrModesDTO.isLoadPaxAvaBalance());
		loadReservationOptions.setLoadSeatingInfo(lccClientPnrModesDTO.isLoadSeatingInfo());
		loadReservationOptions.setLoadMealInfo(lccClientPnrModesDTO.isLoadMealInfo());
		loadReservationOptions.setLoadBaggageInfo(lccClientPnrModesDTO.isLoadBaggageInfo());
		loadReservationOptions.setLoadSSRInfo(lccClientPnrModesDTO.isLoadSSRInfo());
		loadReservationOptions.setRecordAudit(lccClientPnrModesDTO.isRecordAudit());
		loadReservationOptions.setLoadLastUserNote(lccClientPnrModesDTO.isLoadLastUserNote());
		loadReservationOptions.setLoadPaxPaymentOndBreakdownView(lccClientPnrModesDTO.isLoadPaxPaymentOndBreakdownView());
		loadReservationOptions.setLoadExternalPaxPayments(lccClientPnrModesDTO.isLoadExternalPaxPayments());
		loadReservationOptions.setLoadEtickets(lccClientPnrModesDTO.isLoadEtickets());
		loadReservationOptions.setLoadOriginCountryCode(lccClientPnrModesDTO.isLoadOriginCountry());
		loadReservationOptions.setLoadGOQUOAmounts(lccClientPnrModesDTO.isLoadGOQUOAmounts());
		loadReservationOptions.setLoadRefundableChargeDetails(lccClientPnrModesDTO.isLoadRefundableTaxInfo());

		return loadReservationOptions;
	}

	public static AutoCancellationInfo populateAutoCancellationInfo(LCCAutoCancellationInfo lccAutoCancellationInfo) {
		AutoCancellationInfo autoCancellationInfo = null;
		if (lccAutoCancellationInfo != null) {
			autoCancellationInfo = new AutoCancellationInfo();
			autoCancellationInfo.setAutoCancellationId(lccAutoCancellationInfo.getCancellationId());
			autoCancellationInfo.setExpireOn(lccAutoCancellationInfo.getExpireOn());
			autoCancellationInfo.setCancellationType(lccAutoCancellationInfo.getCancellationType());
			autoCancellationInfo.setProcessedByScheduler(AutoCancellationInfo.NOT_PROCESSED);
			autoCancellationInfo.setVersion(lccAutoCancellationInfo.getVersion());
		}
		return autoCancellationInfo;
	}

}
