package com.isa.thinair.aaservices.core.service;

import java.util.Date;

import javax.jws.HandlerChain;
import javax.jws.WebMethod;
import javax.jws.WebService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.required.dto.*;
import com.isa.connectivity.profiles.maxico.v1.required.services.MaxicoRequiredWS;
import com.isa.thinair.aaservices.core.bl.ChangeFare.AAChangeFareAvailBL;
import com.isa.thinair.aaservices.core.bl.ChangeFare.AAChangeFareBL;
import com.isa.thinair.aaservices.core.bl.alerting.AAAddAlertsBL;
import com.isa.thinair.aaservices.core.bl.alerting.AARemoveAlertsBL;
import com.isa.thinair.aaservices.core.bl.alerting.AARetrieveAlertsBL;
import com.isa.thinair.aaservices.core.bl.ancillary.AAAnciModificationBL;
import com.isa.thinair.aaservices.core.bl.ancillary.AAAncillaryAvailabilityBL;
import com.isa.thinair.aaservices.core.bl.ancillary.AAAncillaryBlockSeatBL;
import com.isa.thinair.aaservices.core.bl.ancillary.AAAncillaryReleaseSeatBL;
import com.isa.thinair.aaservices.core.bl.ancillary.AAAncillaryServiceBL;
import com.isa.thinair.aaservices.core.bl.ancillary.AAChkAncillaryRulesValidBL;
import com.isa.thinair.aaservices.core.bl.availability.AAAvailabilitySearchBL;
import com.isa.thinair.aaservices.core.bl.availability.AAAvailablePriceQuoteBL;
import com.isa.thinair.aaservices.core.bl.availability.AABlockSeatRemoveService;
import com.isa.thinair.aaservices.core.bl.availability.AABlockSeatService;
import com.isa.thinair.aaservices.core.bl.dummycarrierfulfillment.AAPayCarrierFulfillmentBL;
import com.isa.thinair.aaservices.core.bl.flightservice.AAFLightManifestDataService;
import com.isa.thinair.aaservices.core.bl.flightservice.AAFlightLoadReportServiceBL;
import com.isa.thinair.aaservices.core.bl.flightservice.AAFlightManifestSearchBL;
import com.isa.thinair.aaservices.core.bl.flightservice.AAFlightSegmentBL;
import com.isa.thinair.aaservices.core.bl.flightservice.FlightSegmentInfoBL;
import com.isa.thinair.aaservices.core.bl.lms.AALoyaltyRedeemBL;
import com.isa.thinair.aaservices.core.bl.lms.AAMergeMemberManagementBL;
import com.isa.thinair.aaservices.core.bl.masterdata.AAAgreementMasterDataBL;
import com.isa.thinair.aaservices.core.bl.masterdata.AAMasterDataSyncBL;
import com.isa.thinair.aaservices.core.bl.ondpublish.AAPublishUnifiedOndCombinationsBL;
import com.isa.thinair.aaservices.core.bl.ondpublish.AARetrieveOwnOndCombinationsBL;
import com.isa.thinair.aaservices.core.bl.onhold.AAOnholdAvailabilityBL;
import com.isa.thinair.aaservices.core.bl.paxcredit.AAPaxCreditBL;
import com.isa.thinair.aaservices.core.bl.payment.AAPaymentBL;
import com.isa.thinair.aaservices.core.bl.promotion.AAPromotionSearchBL;
import com.isa.thinair.aaservices.core.bl.reservation.AAAddInfantBL;
import com.isa.thinair.aaservices.core.bl.reservation.AAAddUserNoteBL;
import com.isa.thinair.aaservices.core.bl.reservation.AAAlterResevationAuthorizationCheckBL;
import com.isa.thinair.aaservices.core.bl.reservation.AAAutoCancellationBL;
import com.isa.thinair.aaservices.core.bl.reservation.AABlacklistPaxCheckBL;
import com.isa.thinair.aaservices.core.bl.reservation.AACalculateDiscountBL;
import com.isa.thinair.aaservices.core.bl.reservation.AAClearAlertBL;
import com.isa.thinair.aaservices.core.bl.reservation.AACreateNewPNRBL;
import com.isa.thinair.aaservices.core.bl.reservation.AACreateReservationBL;
import com.isa.thinair.aaservices.core.bl.reservation.AACustomerNameUpdateBL;
import com.isa.thinair.aaservices.core.bl.reservation.AADuplicateNameCheckBL;
import com.isa.thinair.aaservices.core.bl.reservation.AAExtendORReinstateCreditBL;
import com.isa.thinair.aaservices.core.bl.reservation.AAGroupPaxCouponUpdateBL;
import com.isa.thinair.aaservices.core.bl.reservation.AALoadGroundSegmentBL;
import com.isa.thinair.aaservices.core.bl.reservation.AALoadPaxContactConfigBL;
import com.isa.thinair.aaservices.core.bl.reservation.AAModifyReservationBL;
import com.isa.thinair.aaservices.core.bl.reservation.AAModifyReservationQueryBL;
import com.isa.thinair.aaservices.core.bl.reservation.AAPassengerEticketsBL;
import com.isa.thinair.aaservices.core.bl.reservation.AAPassengerRefundBL;
import com.isa.thinair.aaservices.core.bl.reservation.AAPaxCouponUpdateBL;
import com.isa.thinair.aaservices.core.bl.reservation.AARequoteModifySegmentsBL;
import com.isa.thinair.aaservices.core.bl.reservation.AARequoteReservationQueryBL;
import com.isa.thinair.aaservices.core.bl.reservation.AAResTravelStateSync;
import com.isa.thinair.aaservices.core.bl.reservation.AAReservationAuditBL;
import com.isa.thinair.aaservices.core.bl.reservation.AARetrieveChargeAdjustmentTypesBL;
import com.isa.thinair.aaservices.core.bl.reservation.AASearchReservationBL;
import com.isa.thinair.aaservices.core.bl.reservation.AASearchReservationByPnrBL;
import com.isa.thinair.aaservices.core.bl.reservation.AASelfReprotectFlightsBL;
import com.isa.thinair.aaservices.core.bl.reservation.AAServiceTaxBL;
import com.isa.thinair.aaservices.core.bl.reservation.AAServiceTaxQuoteBL;
import com.isa.thinair.aaservices.core.bl.reservation.AATransferSegmentsBL;
import com.isa.thinair.aaservices.core.bl.reservation.AAUpdateGroupPnrForReservationBL;
import com.isa.thinair.aaservices.core.bl.schedule.AAScheduleAvailabilitySearchBL;
import com.isa.thinair.aaservices.core.bl.ticketing.AAEticketInfoBL;
import com.isa.thinair.aaservices.core.bl.useragent.AAInvalidateUserSessionBL;
import com.isa.thinair.aaservices.core.bl.useragent.AAUserInfoSyncBL;
import com.isa.thinair.aaservices.core.transaction.TransactionalReservationProcess;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

@WebService(targetNamespace = MaxicoRequiredWS.targetNamespace, endpointInterface = MaxicoRequiredWS.endpointInterface)
@HandlerChain(file = "/WEB-INF/handlers.xml")
public class MaxicoRequired extends ServiceBase implements MaxicoRequiredWS {

	private static final Log log = LogFactory.getLog(MaxicoRequired.class);
	
	@Override
	public AAPingRS ping(AAPingRQ aaPingRQ) {

		if (log.isDebugEnabled()) {
			log.debug("ping required webservice called on carrier " + AppSysParamsUtil.getDefaultCarrierCode());
		}

		AAPingRS aaPingRS = new AAPingRS();

		aaPingRS.setTimestamp(new Date());
		aaPingRS.setToken("Received : " + aaPingRQ.getToken());

		if (log.isDebugEnabled()) {
			log.debug("ping required webservice returning response from carrier " + AppSysParamsUtil.getDefaultCarrierCode());
		}

		return aaPingRS;
	}

	@Override
	public AAFlightAvailRS searchAvailableFlights(AAFlightAvailRQ aaFlightAvailRQ) {
		AAAvailabilitySearchBL availSearch = new AAAvailabilitySearchBL();
		availSearch.setTransactionalReservationProcess(getTransactionalReservationProcess(aaFlightAvailRQ.getHeaderInfo()
				.getTransactionIdentifier(), aaFlightAvailRQ.getAaPos().isActualUserIntegration()));
		availSearch.setAaFlightAvailRQ(aaFlightAvailRQ);
		return availSearch.execute();
	}

	@Override
	public AAScheduleAvailRS getScheduleAvailability(AAScheduleAvailRQ aaScheduleAvailRQ) {
		AAScheduleAvailabilitySearchBL aaScheduleAvailabilitySearchBL = new AAScheduleAvailabilitySearchBL();
		aaScheduleAvailabilitySearchBL.setAAScheduleAvailRQ(aaScheduleAvailRQ);
		return aaScheduleAvailabilitySearchBL.execute();
	}

	@Override
	public AAFlightPriceRS quoteFlightPrice(AAFlightPriceRQ aaFlightPriceRQ) {
		AAAvailablePriceQuoteBL priceQuote = new AAAvailablePriceQuoteBL();
		priceQuote.setTransactionalReservationProcess(getTransactionalReservationProcess(aaFlightPriceRQ.getHeaderInfo()
				.getTransactionIdentifier(), aaFlightPriceRQ.getAaPos().isActualUserIntegration()));
		priceQuote.setAaFlightPriceRQ(aaFlightPriceRQ);
		return priceQuote.execute();
	}

	@Override
	public AASearchReservationRS searchReservations(AASearchReservationRQ aaSearchReservationRQ) {
		AASearchReservationBL aaSearchReservationBL = new AASearchReservationBL();
		aaSearchReservationBL.setRequest(aaSearchReservationRQ);
		return aaSearchReservationBL.execute();
	}

	@Override
	public AAAirBookRS searchReservationByPNR(AAReadRQ aaReadRQ) {
		AASearchReservationByPnrBL aaSearchReservationBL = new AASearchReservationByPnrBL();
		aaSearchReservationBL.setRequest(aaReadRQ);
		return aaSearchReservationBL.execute();
	}

	@Override
	public AAAirBookRS book(AAAirBookRQ aaAirBookRQ) {
		AACreateReservationBL createReservationBL = new AACreateReservationBL(aaAirBookRQ, this.getAASession());
		return createReservationBL.execute();
	}

	@Override
	public AAPaymentRS makePayment(AAPaymentRQ aaPaymentRQ) {
		AAPaymentBL aaPaymentBL = new AAPaymentBL(aaPaymentRQ);
		return aaPaymentBL.makePayment();
	}

	@Override
	public AAPaymentRS reversePayment(AAPaymentRQ aaPaymentRQ) {
		AAPaymentBL aaPaymentBL = new AAPaymentBL(aaPaymentRQ);
		return aaPaymentBL.reversePayment();
	}

	/**
	 * WS method responsible for creating the dummy booking in the MC for payable report creation
	 */
	@Override
	public AAPayCarrierFulfullmentRS fulFillPayCarrier(AAPayCarrierFulfullmentRQ aaPayCarrierFulfullmentRQ) {
		AAPayCarrierFulfillmentBL aaCreateDummyReservationBL = new AAPayCarrierFulfillmentBL(aaPayCarrierFulfullmentRQ);
		return aaCreateDummyReservationBL.execute();
	}

	@Override
	public AAAirBookRS modifyReservation(AAAirBookModifyRQ aaAirBookModifyRQ) {
		AAModifyReservationBL aaModifyReservationBL = new AAModifyReservationBL(aaAirBookModifyRQ, this.getAASession());
		return aaModifyReservationBL.execute();
	}

	@Override
	public AAAirBookRS modifyResQuery(AAAirBookModifyRQ aaAirBookModifyRQ) {
		AAModifyReservationQueryBL aaModifyReservationQueryBL = new AAModifyReservationQueryBL(aaAirBookModifyRQ,
				this.getAASession());
		return aaModifyReservationQueryBL.execute();
	}

	@Override
	public AAAirRequoteBalanceRS requoteBalanceQuery(AAAirRequoteBalanceRQ aaAirRequoteBalanceRQ) {
		AARequoteReservationQueryBL aaRequoteReservationQueryBL = new AARequoteReservationQueryBL(aaAirRequoteBalanceRQ,
				this.getAASession());
		return aaRequoteReservationQueryBL.execute();
	}

	@Override
	public AARequoteModifySegmentsRS requoteModifySegments(AARequoteModifySegmentsRQ aaRequoteModifySegmentsRQ) {
		AARequoteModifySegmentsBL aaRequoteModifySegmentsBL = new AARequoteModifySegmentsBL(aaRequoteModifySegmentsRQ,
				this.getAASession());
		return aaRequoteModifySegmentsBL.execute();
	}

	public AAAirBookRS alterResAuthorizationCheck(AAAirBookModifyRQ aaAirBookModifyRQ) {
		AAAlterResevationAuthorizationCheckBL authorizationCheckBL = new AAAlterResevationAuthorizationCheckBL(aaAirBookModifyRQ);
		return authorizationCheckBL.execute();
	}

	@Override
	public AAClearAlertRS clearSegmentAlerts(AAClearAlertRQ aaClearAlertRQ) {
		if (log.isDebugEnabled()) {
			log.debug("clearSegmentAlerts required webservice called on carrier " + AppSysParamsUtil.getDefaultCarrierCode());
		}

		AAClearAlertBL aaClearAlertBL = new AAClearAlertBL();
		aaClearAlertBL.setAAClearAlertRQ(aaClearAlertRQ);
		AAClearAlertRS aaClearAlertRS = aaClearAlertBL.execute();

		if (log.isDebugEnabled()) {
			log.debug("clearSegmentAlerts required webservice returning response from carrier "
					+ AppSysParamsUtil.getDefaultCarrierCode());
		}
		return aaClearAlertRS;
	}

	@Override
	public AAResAuditRS getReservationAudit(AAResAuditReadRQ aaResAuditReadRQ) {
		AAReservationAuditBL aaReservationAuditBL = new AAReservationAuditBL();
		aaReservationAuditBL.setAaResAuditReadRQ(aaResAuditReadRQ);
		return aaReservationAuditBL.execute();
	}

	@Override
	public AATransferSegmentsRS transferSegments(AATransferSegmentsRQ aaTransferSegmentsRQ) {
		AATransferSegmentsBL aaTransferSegmentBL = new AATransferSegmentsBL();
		aaTransferSegmentBL.setTransferSegmentsRQ(aaTransferSegmentsRQ);
		return aaTransferSegmentBL.execute();
	}

	@Override
	public AAAncillaryAvailabilityRS getAvailableAncillaries(AAAncillaryAvailabilityRQ aaAncillaryAvailabilityRQ) {
		if (log.isDebugEnabled()) {
			log.debug("getAvailableAncillaries required webservice called on carrier " + AppSysParamsUtil.getDefaultCarrierCode());
		}

		AAAncillaryAvailabilityBL aaAncillaryAvailabilityBL = new AAAncillaryAvailabilityBL();
		aaAncillaryAvailabilityBL.setAAAncillaryAvailabilityRQ(aaAncillaryAvailabilityRQ);
		AAAncillaryAvailabilityRS aaAncillaryAvailabilityRS = aaAncillaryAvailabilityBL.execute();

		if (log.isDebugEnabled()) {
			log.debug("getAvailableAncillaries required webservice returning response from carrier "
					+ AppSysParamsUtil.getDefaultCarrierCode());
		}
		return aaAncillaryAvailabilityRS;
	}

	@Override
	public AAAncillaryRS getAncillaryDetail(AAAncillaryRQ aaAncillaryRQ) {
		if (log.isDebugEnabled()) {
			log.debug("getAncillaryDetail required webservice called on carrier " + AppSysParamsUtil.getDefaultCarrierCode());
		}

		AAAncillaryServiceBL aaAncillaryServiceBL = new AAAncillaryServiceBL();
		aaAncillaryServiceBL.setTransactionalReservationProcess(getTransactionalReservationProcess(aaAncillaryRQ.getHeaderInfo()
				.getTransactionIdentifier(), aaAncillaryRQ.getAaPos().isActualUserIntegration()));
		AAAncillaryRS aaAncillaryRS = aaAncillaryServiceBL.getAncillaryDetail(aaAncillaryRQ);

		if (log.isDebugEnabled()) {
			log.debug("getAncillaryDetail required webservice returning response from carrier "
					+ AppSysParamsUtil.getDefaultCarrierCode());
		}
		return aaAncillaryRS;
	}

	@Override
	public AAAncillaryBlockSeatRS blockSeats(AAAncillaryBlockSeatRQ aaBlockSeatRQ) {
		if (log.isDebugEnabled()) {
			log.debug("blockSeats required webservice called on carrier " + AppSysParamsUtil.getDefaultCarrierCode());
		}

		AAAncillaryBlockSeatBL aaAncillaryBlockSeatBL = new AAAncillaryBlockSeatBL();
		AAAncillaryBlockSeatRS aaBlockSeatRS = aaAncillaryBlockSeatBL.execute(aaBlockSeatRQ);

		if (log.isDebugEnabled()) {
			log.debug("blockSeats required webservice returning response from carrier "
					+ AppSysParamsUtil.getDefaultCarrierCode());
		}
		return aaBlockSeatRS;
	}

	@Override
	public AAAncillaryReleaseSeatRS releaseSeats(AAAncillaryReleaseSeatRQ releaseSeatRQ) {
		if (log.isDebugEnabled()) {
			log.debug("releaseSeats required webservice called on carrier " + AppSysParamsUtil.getDefaultCarrierCode());
		}

		AAAncillaryReleaseSeatBL aaAncillaryReleaseSeatBL = new AAAncillaryReleaseSeatBL();
		AAAncillaryReleaseSeatRS releaseSeatRS = aaAncillaryReleaseSeatBL.execute(releaseSeatRQ);

		if (log.isDebugEnabled()) {
			log.debug("releaseSeats required webservice returning response from carrier "
					+ AppSysParamsUtil.getDefaultCarrierCode());
		}
		return releaseSeatRS;
	}

	@Override
	public AAAirBookRS addInfants(AAAirBookModifyRQ aaAirBookModifyRQ) {

		if (log.isDebugEnabled()) {
			log.debug("add Infants required webservice called on carrier " + AppSysParamsUtil.getDefaultCarrierCode());
		}
		AAAddInfantBL aaAddInfantBL = new AAAddInfantBL();
		aaAddInfantBL.setAaAirBookModifyRQ(aaAirBookModifyRQ);
		return aaAddInfantBL.execute();
	}

	@Override
	public AASelectedAncillaryRS getSelectedAncillaryDetail(AASelectedAncillaryRQ aaSelectedAncillaryRQ) {
		AAAncillaryServiceBL aaAncillaryServiceBL = new AAAncillaryServiceBL();
		aaAncillaryServiceBL.setTransactionalReservationProcess(getTransactionalReservationProcess(aaSelectedAncillaryRQ
				.getHeaderInfo().getTransactionIdentifier(), aaSelectedAncillaryRQ.getAaPos().isActualUserIntegration()));
		return aaAncillaryServiceBL.getSelectedAncillaryDetail(aaSelectedAncillaryRQ, this.getAASession());
	}

	@Override
	public AAPassengerRefundRS refundPassenger(AAPassengerRefundRQ aaPassengerRefundRQ) {
		if (log.isDebugEnabled()) {
			log.debug("refundPassenger required webservice called on carrier " + AppSysParamsUtil.getDefaultCarrierCode());
		}

		AAPassengerRefundBL aaPassengerRefundBL = new AAPassengerRefundBL();
		return aaPassengerRefundBL.execute(aaPassengerRefundRQ);
	}

	@Override
	public AAChkAncillaryRulesValidRS validateAncillaryRules(AAChkAncillaryRulesValidRQ AAChkAncillaryRulesValidRQ) {
		if (log.isDebugEnabled()) {
			log.debug("validateAncillaryRules required webservice called on carrier " + AppSysParamsUtil.getDefaultCarrierCode());
		}

		AAChkAncillaryRulesValidBL aaChkAncillaryRulesValidBL = new AAChkAncillaryRulesValidBL();
		aaChkAncillaryRulesValidBL.setRequest(AAChkAncillaryRulesValidRQ);

		return aaChkAncillaryRulesValidBL.execute();
	}

	@Override
	public AAPaxCreditSearchRS getPaxCreditForOtherCarrier(AAPaxCreditSearchRQ aaPaxCreditSearchRQ) {
		if (log.isDebugEnabled()) {
			log.debug(" getPaxCreditForOtherCarrier required webservice called on carrier "
					+ AppSysParamsUtil.getDefaultCarrierCode());
		}

		AAPaxCreditBL aaPaxCreditBL = new AAPaxCreditBL();
		return aaPaxCreditBL.getPaxCreditForOtherCarrier(aaPaxCreditSearchRQ);
	}

	@Override
	public AAPaxCreditSearchByPnrListRS
			getCustomerCreditForOtherCarrier(AAPaxCreditSearchByPnrListRQ aaPaxCreditSearchByPnrListRQ) {
		if (log.isDebugEnabled()) {
			log.debug(" getPaxCreditForOtherCarrier (by PNR List) required webservice called on carrier "
					+ AppSysParamsUtil.getDefaultCarrierCode());
		}

		AAPaxCreditBL aaPaxCreditBL = new AAPaxCreditBL();
		return aaPaxCreditBL.getPaxCreditForOtherCarrier(aaPaxCreditSearchByPnrListRQ);

	}

	@Override
	public AAPaxContactConfigRS loadPaxContactConfig(AAPaxContactConfigRQ aaContactConfigRQ) {
		if (log.isDebugEnabled()) {
			log.debug(" loadPaxContactConfig required webservice called on carrier " + AppSysParamsUtil.getDefaultCarrierCode());
		}
		AALoadPaxContactConfigBL aaLoadContactConfigBL = new AALoadPaxContactConfigBL();
		aaLoadContactConfigBL.setRequestParams(aaContactConfigRQ);
		return aaLoadContactConfigBL.excute();
	}

	@Override
	public AAGenerateNewPNRRS createNewPNR(AABaseTypeRQ aaBaseTypeRQ) {
		if (log.isDebugEnabled()) {
			log.debug(" createNewPNR required webservice called on carrier " + AppSysParamsUtil.getDefaultCarrierCode());
		}
		AACreateNewPNRBL aaCreateNewPNRBL = new AACreateNewPNRBL();
		return aaCreateNewPNRBL.excute();

	}

	@Override
	public AAAirBookRS modifyAncillary(AAAnciModifyRQ aaAnciModifyRQ) {
		if (log.isDebugEnabled()) {
			log.debug(" ancillary modification required web service called on carrier "
					+ AppSysParamsUtil.getDefaultCarrierCode());
		}
		AAAnciModificationBL aaAnciModificationBL = new AAAnciModificationBL();
		aaAnciModificationBL.setAAAnciModifyRQ(aaAnciModifyRQ);

		return aaAnciModificationBL.execute();
	}

	@Override
	public AAOndCombinationsRS getOndCombinations(AAOndCombinationsRQ aaOndCombinationsRQ) {
		if (log.isDebugEnabled()) {
			log.debug("getOndCombinations required webservice called on carrier " + AppSysParamsUtil.getDefaultCarrierCode());
		}

		AARetrieveOwnOndCombinationsBL aaRetrieveOwnOndCombinationsBL = new AARetrieveOwnOndCombinationsBL();
		aaRetrieveOwnOndCombinationsBL.setAaOndCombinationsRQ(aaOndCombinationsRQ);
		return aaRetrieveOwnOndCombinationsBL.execute();

	}

	@Override
	public AAOndPublishRS publishUnifiedOndCombinations(AAOndPublishRQ aaOndPublishRQ) {
		if (log.isDebugEnabled()) {
			log.debug("publishUnifiedOndCombinations required webservice called on carrier "
					+ AppSysParamsUtil.getDefaultCarrierCode());
		}

		AAPublishUnifiedOndCombinationsBL aaPublishUnifiedOndCombinationsBL = new AAPublishUnifiedOndCombinationsBL();
		aaPublishUnifiedOndCombinationsBL.setAaOndPublishRQ(aaOndPublishRQ);
		return aaPublishUnifiedOndCombinationsBL.execute();

	}

	@Override
	public AAAlertRetrievalRS retrieveAlerts(AAAlertRetrievalRQ aaAlertRetrievalRQ) {
		if (log.isDebugEnabled()) {
			log.debug("retrieveAlerts required webservice called on carrier " + AppSysParamsUtil.getDefaultCarrierCode());
		}
		AARetrieveAlertsBL aaRetrieveAlertsBL = new AARetrieveAlertsBL();
		aaRetrieveAlertsBL.setRequestParams(aaAlertRetrievalRQ);
		return aaRetrieveAlertsBL.excute();
	}

	@Override
	public AARemoveAlertsRS removeAlerts(AARemoveAlertsRQ aaRemoveAlertsRQ) {
		if (log.isDebugEnabled()) {
			log.debug("removeAlerts required webservice called on carrier " + AppSysParamsUtil.getDefaultCarrierCode());
		}
		AARemoveAlertsBL aaRemoveAlertsBL = new AARemoveAlertsBL();
		aaRemoveAlertsBL.setRequestParams(aaRemoveAlertsRQ);
		return aaRemoveAlertsBL.excute();
	}

	public AAAddAlertRS addAlerts(AAAddAlertsRQ aaAddAlertRQ) {
		AAAddAlertsBL aaAddAlertsBL = new AAAddAlertsBL();
		aaAddAlertsBL.setRequestParams(aaAddAlertRQ);
		return aaAddAlertsBL.execute();
	}

	@Override
	public AAFareAvailRS loadAvailChangeFare(AAFareAvailRQ aaChangeFareAvailRQ) {
		AAChangeFareAvailBL aaChangeFareAvailBL = new AAChangeFareAvailBL();
		aaChangeFareAvailBL.setRequestParams(aaChangeFareAvailRQ);

		return aaChangeFareAvailBL.execute();
	}

	@Override
	public AAChangeFareRS changeFare(AAChangeFareRQ aaRecalculateChargesRQ) {
		AAChangeFareBL aaChangeFareBL = new AAChangeFareBL();
		aaChangeFareBL.setParams(aaRecalculateChargesRQ);
		aaChangeFareBL.setTransactionalReservationProcess(getTransactionalReservationProcess(aaRecalculateChargesRQ
				.getHeaderInfo().getTransactionIdentifier(), aaRecalculateChargesRQ.getAaPos().isActualUserIntegration()));
		return aaChangeFareBL.execute();
	}

	@Override
	public AAFlightManifestSearchRS searchFlightsForManifest(AAFlightManifestSearchRQ aaFlightManifestSearchRQ) {
		if (log.isDebugEnabled()) {
			log.debug("flight manifest search required webservice called on carrier " + AppSysParamsUtil.getDefaultCarrierCode());
		}
		AAFlightManifestSearchBL aaFlightManifestSearchBL = new AAFlightManifestSearchBL();
		aaFlightManifestSearchBL.setAAFlightManifestSearchRQ(aaFlightManifestSearchRQ);

		return aaFlightManifestSearchBL.execute();
	}

	@Override
	public AAFlightManifestDataRS getFlightManifestData(AAFlightManifestDataRQ aaFlightManifestDataRQ) {
		if (log.isDebugEnabled()) {
			log.debug("flight manifest data required webservice called on carrier " + AppSysParamsUtil.getDefaultCarrierCode());
		}
		AAFLightManifestDataService aafLightManifestDataService = new AAFLightManifestDataService();
		aafLightManifestDataService.setAAFlightManifestDataRQ(aaFlightManifestDataRQ);

		return aafLightManifestDataService.execute();
	}

	@Override
	public AAFlightLoadReportDataRS getFlightLoadReportData(AAFlightLoadReportDataRQ aaFlightLoadReportDataRQ) {
		if (log.isDebugEnabled()) {
			log.debug("flight manifest data required webservice called on carrier " + AppSysParamsUtil.getDefaultCarrierCode());
		}
		AAFlightLoadReportServiceBL aaFlightLoadReportServiceBL = new AAFlightLoadReportServiceBL();
		aaFlightLoadReportServiceBL.setAAFlightLoadReportDataRQ(aaFlightLoadReportDataRQ);

		return aaFlightLoadReportServiceBL.execute();
	}

	@Override
	public AAGroundSegmentRS getGroundSegmentData(AAGroundSegmentRQ aaGroundSegmentRQ) {
		if (log.isDebugEnabled()) {
			log.debug("Ground Segment data required webservice called on carrier " + AppSysParamsUtil.getDefaultCarrierCode());
		}
		AALoadGroundSegmentBL aaLoadGroundSegmentBL = new AALoadGroundSegmentBL();
		aaLoadGroundSegmentBL.setAaGroundSegmentRQ(aaGroundSegmentRQ);

		return aaLoadGroundSegmentBL.execute();
	}

	@Override
	public AABlockSeatRS blockSeat(AABlockSeatRQ aaBlockSeatRQ) {
		if (log.isDebugEnabled()) {
			log.debug("Block Seat webservice called on carrier " + AppSysParamsUtil.getDefaultCarrierCode());
		}
		AABlockSeatService aaBlockSeatService = new AABlockSeatService();
		aaBlockSeatService.setTransactionalReservationProcess(getTransactionalReservationProcess(
				aaBlockSeatRQ.getTransactionID(), false));
		aaBlockSeatService.setAABlockSeatRQ(aaBlockSeatRQ);

		return aaBlockSeatService.execute();
	}

	@Override
	public AABlockSeatRS removeBlockSeat(AABlockSeatRQ aaBlockSeatRQ) {
		if (log.isDebugEnabled()) {
			log.debug("Block Seat Remove webservice called on carrier " + AppSysParamsUtil.getDefaultCarrierCode());
		}
		AABlockSeatRemoveService aaBlockSeatRemoveService = new AABlockSeatRemoveService();
		aaBlockSeatRemoveService.setTransactionalReservationProcess(getTransactionalReservationProcess(
				aaBlockSeatRQ.getTransactionID(), false));
		aaBlockSeatRemoveService.setAABlockSeatRQ(aaBlockSeatRQ);

		return aaBlockSeatRemoveService.execute();
	}

	@Override
	public AAUpdateGroupPnrRS updateGroupPnrForReservation(AAUpdateGroupPnrRQ aAUpdateGroupPnrRQ) {
		if (log.isDebugEnabled()) {
			log.debug("Update Group PNR For Reservation web service called on carrier "
					+ AppSysParamsUtil.getDefaultCarrierCode());
		}

		AAUpdateGroupPnrForReservationBL updateGroupPnrBL = new AAUpdateGroupPnrForReservationBL();
		updateGroupPnrBL.setAAUpdateGroupPnrRQ(aAUpdateGroupPnrRQ);
		return updateGroupPnrBL.execute();
	}

	@Override
	public AAInvalidateAASessionsForUserRS invalidateAASessionsForUser(
			AAInvalidateAASessionsForUserRQ aaInvalidateAASessionsForUserRQ) {
		if (log.isDebugEnabled()) {
			log.debug("Invalidate AASession for User web service method called on carrier : "
					+ AppSysParamsUtil.getDefaultCarrierCode());
		}

		AAInvalidateUserSessionBL invalidateUserSessionBL = new AAInvalidateUserSessionBL();
		invalidateUserSessionBL.setRequest(aaInvalidateAASessionsForUserRQ);
		return invalidateUserSessionBL.execute();
	}

	@Override
	public AAOnholdAvailabilityRS checkOnholdAvailability(AAOnholdAvailabilityRQ aaOnholdAvailabilityRQ) {
		if (log.isDebugEnabled()) {
			log.debug("check IBE onhold availability web service method called on carrier : "
					+ AppSysParamsUtil.getDefaultCarrierCode());
		}

		AAOnholdAvailabilityBL aaOnholdAvailabilityBL = new AAOnholdAvailabilityBL();
		aaOnholdAvailabilityBL.setRequest(aaOnholdAvailabilityRQ);
		return aaOnholdAvailabilityBL.execute();
	}

	@Override
	public AARetrieveChargeAdjustmentTypesRS retrieveChargeAdjustmentTypes(AABaseTypeRQ aaBaseTypeRQ) {

		if (log.isDebugEnabled()) {
			log.debug("Retrieve Charge Adjustment Types called on carrier : " + AppSysParamsUtil.getDefaultCarrierCode());
		}

		AARetrieveChargeAdjustmentTypesBL retrieveChargeAdjustmentTypesBL = new AARetrieveChargeAdjustmentTypesBL();
		return retrieveChargeAdjustmentTypesBL.excute();
	}

	@Override
	public AAPaymentCheckRS checkPayments(AAPaymentCheckRQ paymentCheckRQ) {
		return AAPaymentBL.checkReservationPayments(paymentCheckRQ);
	}

	@Override
	public AAExtendORReinstateCreditRS extendORReinstateCredit(AAExtendORReinstateCreditRQ aaExtendORReinstateCreditRQ) {
		return AAExtendORReinstateCreditBL.execute(aaExtendORReinstateCreditRQ);
	}

	@Override
	public AAUpdatePassengerEticketRS updatePassengerEtickets(AAUpdatePassengerEticketRQ aaUpdatePassengerEticketRQ) {
		AAPassengerEticketsBL aaPassengerEticketsBL = new AAPassengerEticketsBL();
		return aaPassengerEticketsBL.updatePassengerTickets(aaUpdatePassengerEticketRQ);
	}

	@Override
	public AAUpdatePassengerEticketRS exchangePassengerEtickets(AAExchangePassengerEticketRQ aaExchangePassengerEticketRQ) {
		AAPassengerEticketsBL aaPassengerEticketsBL = new AAPassengerEticketsBL();
		return aaPassengerEticketsBL.ExchangePassengerTickets(aaExchangePassengerEticketRQ);
	}
	
	@Override
	public AAUpdatePassengerEticketRS exchangeEticketsForGivenPassengers(AAExchangePassengerEticketRQ aaExchangePassengerEticketRQ) {
		AAPassengerEticketsBL aaPassengerEticketsBL = new AAPassengerEticketsBL();
		return aaPassengerEticketsBL.ExchangeTicketsForGivenPassengers(aaExchangePassengerEticketRQ);
	}

	public AAETicketInfoRS getETicketInformation(AAETicketInfoRQ aaETicketInfoRQ) {
		if (log.isDebugEnabled()) {
			log.debug("get ETicket Info web service method called on carrier : " + AppSysParamsUtil.getDefaultCarrierCode());
		}

		AAEticketInfoBL aaEticketInfoBL = new AAEticketInfoBL();
		aaEticketInfoBL.setAAETicketInfoRQ(aaETicketInfoRQ);
		return aaEticketInfoBL.execute();
	}

	@Override
	public AARetrievePassengerEticketRS retrievePassengerEtickets(AARetrievePassengerEticketRQ aaRetrievePassengerEticketRQ) {
		AAPassengerEticketsBL aaPassengerEticketsBL = new AAPassengerEticketsBL();
		return aaPassengerEticketsBL.retrievePassengerTickets(aaRetrievePassengerEticketRQ);
	}

	private TransactionalReservationProcess getTransactionalReservationProcess(String transactionId, boolean isActualUser) {
		transactionId = PlatformUtiltiies.nullHandler(transactionId);

		if (transactionId.length() > 0) {
			TransactionalReservationProcess transactionalReservationProcess = (TransactionalReservationProcess) getAASession()
					.getCurrentUserTransaction(transactionId);

			if (transactionalReservationProcess == null) {
				transactionalReservationProcess = new TransactionalReservationProcess();
				transactionalReservationProcess.setTransactionIdentifier(transactionId);
				getAASession().addCurrentUserTransaction(transactionId, transactionalReservationProcess, isActualUser);
			}

			return transactionalReservationProcess;
		}

		return null;
	}

	public AAResTravelStateSyncRS syncResTravelStates(AAResTravelStateSyncRQ aaResTravelStateSyncRQ) {
		if (log.isDebugEnabled()) {
			log.debug("syncResTravelStates method called on carrier : " + AppSysParamsUtil.getDefaultCarrierCode());
		}

		AAResTravelStateSync aaResTravelStateSync = new AAResTravelStateSync(aaResTravelStateSyncRQ.getSourceAirReservation());
		return aaResTravelStateSync.execute();
	}

	@Override
	public AAAutoCancellationTimeRS calculateAutoCancellationTime(AAAutoCancellationTimeRQ aaAutoCancellationTimeRQ) {
		if (log.isDebugEnabled()) {
			log.debug("calculateAutoCancellationTime on carrier " + AppSysParamsUtil.getDefaultCarrierCode());
		}
		AAAutoCancellationBL aaAutoCancellationBL = new AAAutoCancellationBL();
		return aaAutoCancellationBL.calculateAutoCancellationTime(aaAutoCancellationTimeRQ);
	}

	@Override
	public AAAutoCancellationRS saveOrUpdateAutoCancellation(AAAutoCancellationRQ aaAutoCancellationRQ) {
		if (log.isDebugEnabled()) {
			log.debug("save or update Auto Cancellation Info on carrier " + AppSysParamsUtil.getDefaultCarrierCode());
		}
		AAAutoCancellationBL aaAutoCancellationBL = new AAAutoCancellationBL();
		return aaAutoCancellationBL.saveOrUpdateAutoCancellationInfo(aaAutoCancellationRQ);
	}

	@Override
	public AAAutoCancellationQueryRS getAutoCancellableReservations(AAAutoCancellationQueryRQ aaAutoCancellationQueryRQ) {
		if (log.isDebugEnabled()) {
			log.debug("get Auto Cancellable Reservations on carrier " + AppSysParamsUtil.getDefaultCarrierCode());
		}
		AAAutoCancellationBL aaAutoCancellationBL = new AAAutoCancellationBL();
		return aaAutoCancellationBL.getExpiredReservations(aaAutoCancellationQueryRQ);
	}

	@Override
	public AAExistingMoficationChargesRS getExistingModificationCharges(
			AAExistingMoficationChargesRQ aaExistingMoficationChargesRQ) {
		if (log.isDebugEnabled()) {
			log.debug("get Existing Modification Charges Due on carrier " + AppSysParamsUtil.getDefaultCarrierCode());
		}
		AAAutoCancellationBL aaAutoCancellationBL = new AAAutoCancellationBL();
		return aaAutoCancellationBL.getExistingModificationChargesDue(aaExistingMoficationChargesRQ);
	}

	@Override
	public AAAutoCancellationBalanceToPayRS calculateAutoCancellationBalanceToPay(
			AAAutoCancellationBalanceToPayRQ aaBalanceToPayRQ) {
		if (log.isDebugEnabled()) {
			log.debug("get Auto Cancellation Balance To Pay Amount of PNR " + aaBalanceToPayRQ.getPnr() + " on carrier "
					+ AppSysParamsUtil.getDefaultCarrierCode());
		}
		AAAutoCancellationBL aaAutoCancellationBL = new AAAutoCancellationBL();
		return aaAutoCancellationBL.calculateAutoCancellationBalanceToPay(aaBalanceToPayRQ);
	}

	@Override
	public AAAnciTaxApplicabilityRS getAnciTaxApplicability(AAAnciTaxApplicabilityRQ aaAnciTaxApplicabilityRQ) {
		if (log.isDebugEnabled()) {
			log.debug("get tax applicability for ancillaries on carrier " + AppSysParamsUtil.getDefaultCarrierCode());
		}
		AAAncillaryAvailabilityBL anciBL = new AAAncillaryAvailabilityBL();
		return anciBL.getAnciTaxApplicability(aaAnciTaxApplicabilityRQ);
	}

	@Override
	public AAAgreementMasterDataRS getAgreementMasterData(AAAgreementMasterDataRQ agreementMasterDataRQ) {
		if (log.isDebugEnabled()) {
			log.debug("get master data on carrier " + AppSysParamsUtil.getDefaultCarrierCode());
		}
		AAAgreementMasterDataBL masterDataBL = new AAAgreementMasterDataBL();
		masterDataBL.setAaAgreementMasterDataRQ(agreementMasterDataRQ);
		return masterDataBL.execute();
	}

	@Override
	public AASegmentInfoRS getFlightSegmentInfo(AASegmentInfoRQ aaSegmentInfoRQ) {
		if (log.isDebugEnabled()) {
			log.debug("get flight segment data on carrier " + AppSysParamsUtil.getDefaultCarrierCode());
		}
		FlightSegmentInfoBL flightSegmentInfoBL = new FlightSegmentInfoBL();
		flightSegmentInfoBL.setAaSegmentInfoRQ(aaSegmentInfoRQ);
		return flightSegmentInfoBL.execute();
	}
	
	@Override
	public AAFlightSegmentRS getFlightSegment(AAFlightSegmentRQ aaFlightSegmentRQ) {
		if (log.isDebugEnabled()) {
			log.debug("get flight segment data on carrier " + AppSysParamsUtil.getDefaultCarrierCode());
		}
		AAFlightSegmentBL aaFlightSegmentBL = new AAFlightSegmentBL();
		aaFlightSegmentBL.setAaFlightSegmentRQ(aaFlightSegmentRQ);
		return aaFlightSegmentBL.execute();
	}

	@Override
	public AAPaxCouponUpdateRS updatePaxCoupon(AAPaxCouponUpdateRQ aaPaxCouponUpdateRQ) {
		AAPaxCouponUpdateBL paxCouponUpdateBL = new AAPaxCouponUpdateBL();
		paxCouponUpdateBL.setAAPaxCouponUpdateRQ(aaPaxCouponUpdateRQ);
		return paxCouponUpdateBL.execute();
	}
	
	@Override
	public AAGroupPaxCouponUpdateRS updateGroupPaxCoupon(AAGroupPaxCouponUpdateRQ aaGroupPaxCouponUpdateRQ){	
		AAGroupPaxCouponUpdateBL groupPaxCoupneBL = new AAGroupPaxCouponUpdateBL(aaGroupPaxCouponUpdateRQ);
		return groupPaxCoupneBL.execute();
	}

	@Override
	public AAPromoSelectionCriteriaRS pickApplicablePromotions(AAPromoSelectionCriteriaRQ aaPromoSelectionCriteriaRQ) {
		if (log.isDebugEnabled()) {
			log.debug("Pick available promotions " + AppSysParamsUtil.getDefaultCarrierCode());
		}
		AAPromotionSearchBL promotionSearch = new AAPromotionSearchBL();
		promotionSearch.setAaPromoSelectionCriteriaRQ(aaPromoSelectionCriteriaRQ);
		return promotionSearch.execute();
	}

	@Override
	@WebMethod
	public AAServiceTaxRS getApplicableServiceTaxes(AAServiceTaxRQ aaServiceTaxRQ) {
		if (log.isDebugEnabled()) {
			log.debug("Get Applicable Service Taxes " + AppSysParamsUtil.getDefaultCarrierCode());
		}

		AAServiceTaxBL aaServiceTaxBL = new AAServiceTaxBL(aaServiceTaxRQ);
		return aaServiceTaxBL.execute();
	}

	@Override
	public AAMasterDataSyncRS syncMasterData(AAMasterDataSyncRQ aaMasterDataSyncRQ) {
		if (log.isDebugEnabled()) {
			log.debug("Sync master data on carrier :  " + AppSysParamsUtil.getDefaultCarrierCode());
		}

		AAMasterDataSyncBL aaMasterDataSyncBL = new AAMasterDataSyncBL(aaMasterDataSyncRQ);
		return aaMasterDataSyncBL.execute();
	}

	@Override
	public AAUserInfoSyncRS syncUserInfo(AAUserInfoSyncRQ aaUserInfoSyncRQ) {
		if (log.isDebugEnabled()) {
			log.debug("Sync user info on carrier :  " + AppSysParamsUtil.getDefaultCarrierCode());
		}

		AAUserInfoSyncBL aaUserInfoSyncBL = new AAUserInfoSyncBL(aaUserInfoSyncRQ);
		return aaUserInfoSyncBL.execute();
	}

	@Override
	public AAMergeLoyaltymemberRS getMergeLoyaltyMember(AAMergeLoyaltymemberRQ aaMergeLoyaltymemberRQ) {
		if (log.isDebugEnabled()) {
			log.debug("get merge loyalty member on carrier :  " + AppSysParamsUtil.getDefaultCarrierCode());
		}

		AAMergeMemberManagementBL aaMergeMemberManagementBL = new AAMergeMemberManagementBL(aaMergeLoyaltymemberRQ);
		return aaMergeMemberManagementBL.execute();
	}
	
	@Override
	public AADiscountRS calculateDiscount(AADiscountRQ aaDiscountRQ) {
		AACalculateDiscountBL aaCalculateDiscountBL = new AACalculateDiscountBL(aaDiscountRQ, this.getAASession());
		return aaCalculateDiscountBL.execute();
	}

		@Override
	public AALoyaltyRedeemRS getLoyaltyRedeemableAmounts(AALoyaltyRedeemRQ aaLoyaltyRedeemRQ) {
		if (log.isDebugEnabled()) {
			log.debug("getLoyaltyRedeemableAmounts on carrier :  " + AppSysParamsUtil.getDefaultCarrierCode());
		}

		AALoyaltyRedeemBL aaLoyaltyRedeemBL = new AALoyaltyRedeemBL(aaLoyaltyRedeemRQ);
		aaLoyaltyRedeemBL.setTransactionalReservationProcess(getTransactionalReservationProcess(aaLoyaltyRedeemRQ.getHeaderInfo()
				.getTransactionIdentifier(), aaLoyaltyRedeemRQ.getAaPos().isActualUserIntegration()));
		return aaLoyaltyRedeemBL.execute();
	}

	@Override
	public AADuplicateNameCheckRS duplicateNameCheck(AADuplicateNameCheckRQ aaDuplicateNameCheckRQ) {
		if (log.isDebugEnabled()) {
			log.debug("check duplicate names on carrier :  " + AppSysParamsUtil.getDefaultCarrierCode());
		}
		
		return (AADuplicateNameCheckRS) new AADuplicateNameCheckBL(aaDuplicateNameCheckRQ).execute();
		
	}

	@Override
	public AAUserNoteRS addUserNote(AAUserNoteRQ aaUserNoteRQ) {
		if (log.isDebugEnabled()) {
			log.debug("Add UserNote  on carrier :  " + AppSysParamsUtil.getDefaultCarrierCode());
		}
		AAAddUserNoteBL aaAddUserNote = new AAAddUserNoteBL(aaUserNoteRQ);
		return aaAddUserNote.execute();

	}

	@Override
	public AACustomerNameUpdateRS customerNameUpdate(AACustomerNameUpdateRQ aaCustomerNameUpdateRQ) {
		if (log.isDebugEnabled()) {
			log.debug("check duplicate names on carrier :  " + AppSysParamsUtil.getDefaultCarrierCode());
		}

		return (AACustomerNameUpdateRS) new AACustomerNameUpdateBL(aaCustomerNameUpdateRQ).execute();
	}
	
	@Override
	public AAServiceTaxQuoteForTktRevRS quoteServiceTaxForTicketingRevenue(
			AAServiceTaxQuoteForTktRevRQ aaServiceTaxQuoteForTktRevRQ) {
		if (log.isDebugEnabled()) {
			log.debug("Service Tax quote on carrier :  " + AppSysParamsUtil.getDefaultCarrierCode());
		}
		return (AAServiceTaxQuoteForTktRevRS) new AAServiceTaxQuoteBL(aaServiceTaxQuoteForTktRevRQ, this.getAASession())
				.execute();
	}

	@Override
	public AACheckBlacklistPaxRS getBalcklistedPaxReservation(AACheckBlacklistPaxRQ aaCheckBLacklistPax) {
		if (log.isDebugEnabled()) {
			log.debug("getBalcklistedPaxReservation on carrier :  " + AppSysParamsUtil.getDefaultCarrierCode());
		}
		AABlacklistPaxCheckBL aaBlacklistPaxCheckBL = new AABlacklistPaxCheckBL(aaCheckBLacklistPax);		
		return aaBlacklistPaxCheckBL.execute();
	}

	@Override
	public AASelfReprotectFlightsRS getSelfReprotectFlights(AASelfReprotectFlightsRQ aaSelfReprotectFlightsRQ){
		if (log.isDebugEnabled()) {
			log.debug("Getting Self Re-protect flights from :  " + AppSysParamsUtil.getDefaultCarrierCode());
		}
		
		AASelfReprotectFlightsBL aaSelfReprotectFlights = new AASelfReprotectFlightsBL(aaSelfReprotectFlightsRQ);
		return aaSelfReprotectFlights.execute();
		
	}
}