package com.isa.thinair.aaservices.core.bl.reservation;

import java.util.ArrayList;
import java.util.Collection;

import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAirBookModifyRQ;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.airreservation.api.dto.ExternalSegmentTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * External Segment business related functionalities
 * 
 * @author Nilindra Fernando
 */
class AAExternalSegmentBL {

	static void processOND(Reservation reservation, AAAirBookModifyRQ aaAirBookModifyRQ, Collection privilegeKeys)
			throws ModuleException, WebservicesException {

		String pnr = aaAirBookModifyRQ.getAaReservation().getPnr();
		long version = Long.parseLong(aaAirBookModifyRQ.getAaReservation().getAirReservation().getVersion());

		Collection<ExternalSegmentTO> colExternalSegmentTO = new ArrayList<ExternalSegmentTO>();

		if (aaAirBookModifyRQ.getNewAAReservation() != null
				&& aaAirBookModifyRQ.getNewAAReservation().getAirReservation().getAirItinerary() != null) {
			Collection<ExternalSegmentTO> newOndWiseExternalFlightSegments = AAModifyReservationUtil
					.getOndWiseExternalFlightSegments(aaAirBookModifyRQ.getNewAAReservation().getAirReservation()
							.getAirItinerary(), ReservationInternalConstants.ReservationExternalSegmentStatus.CONFIRMED);
			colExternalSegmentTO.addAll(newOndWiseExternalFlightSegments);

			// if (newOndWiseExternalFlightSegments != null && newOndWiseExternalFlightSegments.size() > 0) {
			// for (FlightSegment flightSegment : newOndWiseExternalFlightSegments) {
			// ExternalSegmentTO externalSegmentTO = new ExternalSegmentTO();
			// externalSegmentTO.setFlightNo(flightSegment.getFlightNumber());
			// externalSegmentTO.setDepartureDate(flightSegment.getDepatureDateTime());
			// externalSegmentTO.setArrivalDate(flightSegment.getArrivalDateTime());
			// externalSegmentTO.setSegmentCode(flightSegment.getSegmentCode());
			// externalSegmentTO.setCabinClassCode(flightSegment.getTravelPreferences().getCabinClassCode());
			// externalSegmentTO.setStatus(ReservationInternalConstants.ReservationExternalSegmentStatus.CONFIRMED);
			//
			// colExternalSegmentTO.add(externalSegmentTO);
			// }
			// }
		}

		if (aaAirBookModifyRQ.getAaReservation() != null
				&& aaAirBookModifyRQ.getAaReservation().getAirReservation().getExistingAirItinerary() != null) {
			Collection<ExternalSegmentTO> existingAllOndWiseExternalFlightSegments = AAModifyReservationUtil
					.getOndWiseExternalFlightSegments(aaAirBookModifyRQ.getAaReservation().getAirReservation()
							.getExistingAirItinerary(), null);
			colExternalSegmentTO.addAll(existingAllOndWiseExternalFlightSegments);
		}

		if (aaAirBookModifyRQ.getAaReservation() != null
				&& aaAirBookModifyRQ.getAaReservation().getAirReservation().getAirItinerary() != null) {
			Collection<ExternalSegmentTO> oldOndWiseExternalFlightSegments = AAModifyReservationUtil
					.getOndWiseExternalFlightSegments(aaAirBookModifyRQ.getAaReservation().getAirReservation().getAirItinerary(),
							ReservationInternalConstants.ReservationExternalSegmentStatus.CANCEL);
			colExternalSegmentTO.addAll(oldOndWiseExternalFlightSegments);

			// if (oldOndWiseExternalFlightSegments != null && oldOndWiseExternalFlightSegments.size() > 0) {
			// for (FlightSegment flightSegment : oldOndWiseExternalFlightSegments) {
			// ExternalSegmentTO externalSegmentTO = new ExternalSegmentTO();
			// externalSegmentTO.setFlightNo(flightSegment.getFlightNumber());
			// externalSegmentTO.setDepartureDate(flightSegment.getDepatureDateTime());
			// externalSegmentTO.setArrivalDate(flightSegment.getArrivalDateTime());
			// externalSegmentTO.setSegmentCode(flightSegment.getSegmentCode());
			// externalSegmentTO.setCabinClassCode(flightSegment.getTravelPreferences().getCabinClassCode());
			// externalSegmentTO.setStatus(ReservationInternalConstants.ReservationExternalSegmentStatus.CANCEL);
			//
			// colExternalSegmentTO.add(externalSegmentTO);
			// }
			// }
		}

		if (colExternalSegmentTO.size() > 0) {
			AAServicesModuleUtils.getSegmentBD().changeExternalSegments(pnr, colExternalSegmentTO, version, null);
		}

	}

}
