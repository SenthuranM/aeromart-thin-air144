package com.isa.thinair.aaservices.core.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.ChargeGroupCode;
import com.isa.connectivity.profiles.maxico.v1.common.dto.CommonCreditCardPayment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.CurrencyCodeGroup;
import com.isa.connectivity.profiles.maxico.v1.common.dto.DirectBill;
import com.isa.connectivity.profiles.maxico.v1.common.dto.NominalType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OndPaymentBreakdownDetails;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaymentCurrencyAmount;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaymentDetails;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaymentSummary;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TravelerCreditPayment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TravelerVoucherPayment;
import com.isa.thinair.airproxy.api.utils.FindReservationUtil;
import com.isa.thinair.airreservation.api.dto.CardPaymentInfo;
import com.isa.thinair.airreservation.api.model.ReservationPaxExtTnx;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndPayment;
import com.isa.thinair.airreservation.api.model.ReservationTnx;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.invoicing.api.util.NominalCodeConverter;

/**
 * Factory method to create PaymentDetails for {@link ReservationTnx} and {@link ReservationPaxExtTnx} entries.
 * 
 * @author mekanayake
 * @author sanjaya
 * 
 */
public class AAPaymentDetailsFactory {

	/** Executor airline code */
	private static String thisAirlineCode = AppSysParamsUtil.getDefaultCarrierCode();

	// Logger
	private static Log log = LogFactory.getLog(AAPaymentDetailsFactory.class);

	private static final String CARRIER_SEPERATOR = "|";

	public static final double PER_PAX_ERROR_MARGIN = 0.09;

	/**
	 * construct Cash payment detail
	 * 
	 * @param reservationTnx
	 * @param codeGroup
	 * @param opCarrier
	 *            TODO
	 * @return
	 */
	public static PaymentDetails getCashPaymentDetails(ReservationTnx reservationTnx, CurrencyCodeGroup codeGroup,
			String opCarrier) {

		PaymentDetails paymentDetails = getBasicPaymentDetail(reservationTnx, codeGroup, opCarrier);
		paymentDetails.setCash(true);

		paymentDetails.setDirectBill(getAgentDetails(reservationTnx.getAgentCode()));

		return paymentDetails;
	}

	/**
	 * construct Cash payment detail for @{link ReservationPaxExtTnx}
	 * 
	 * @param reservationPaxExtTnx
	 * @param codeGroup
	 * @param opCarrier
	 *            : The operating carrier
	 * @return
	 */
	static PaymentDetails getCashPaymentDetails(ReservationPaxExtTnx reservationPaxExtTnx, CurrencyCodeGroup codeGroup,
			String opCarrier) {

		PaymentDetails paymentDetails = getBasicPaymentDetail(reservationPaxExtTnx, codeGroup, opCarrier);
		paymentDetails.setCash(true);

		return paymentDetails;
	}

	/**
	 * construct onAccount payment detail
	 * 
	 * @param reservationTnx
	 * @param codeGroup
	 * @param opCarrier
	 *            TODO
	 * @return
	 */
	public static PaymentDetails getOnAccountPaymentDetails(ReservationTnx reservationTnx, CurrencyCodeGroup codeGroup,
			String opCarrier) {
		PaymentDetails paymentDetails = getBasicPaymentDetail(reservationTnx, codeGroup, opCarrier);
		paymentDetails.setDirectBill(getAgentDetails(reservationTnx.getAgentCode()));

		return paymentDetails;

	}

	/**
	 * construct onAccount payment detail
	 * 
	 * @param reservationTnx
	 * @param codeGroup
	 * @param opCarrier
	 *            TODO
	 * @return
	 */
	public static PaymentDetails getBSPAccountPaymentDetails(ReservationTnx reservationTnx, CurrencyCodeGroup codeGroup,
			String opCarrier) {
		PaymentDetails paymentDetails = getBasicPaymentDetail(reservationTnx, codeGroup, opCarrier);

		paymentDetails.setDirectBill(getAgentDetails(reservationTnx.getAgentCode()));

		return paymentDetails;

	}

	/**
	 * construct onAccount payment detail for external payment record
	 * 
	 * @param reservationTnx
	 * @param codeGroup
	 * @param opCarrier
	 *            TODO
	 * @return
	 */
	static PaymentDetails getOnAccountPaymentDetails(ReservationPaxExtTnx reservationPaxExtTnx, CurrencyCodeGroup codeGroup,
			String opCarrier) {
		PaymentDetails paymentDetails = getBasicPaymentDetail(reservationPaxExtTnx, codeGroup, opCarrier);

		paymentDetails.setDirectBill(getAgentDetails(reservationPaxExtTnx.getAgentCode()));

		return paymentDetails;

	}

	/**
	 * construct onAccount payment detail for external payment record
	 * 
	 * @param reservationTnx
	 * @param codeGroup
	 * @param opCarrier
	 *            TODO
	 * @return
	 */
	public static PaymentDetails getBSPAccountPaymentDetails(ReservationPaxExtTnx reservationPaxExtTnx,
			CurrencyCodeGroup codeGroup, String opCarrier) {
		PaymentDetails paymentDetails = getBasicPaymentDetail(reservationPaxExtTnx, codeGroup, opCarrier);

		paymentDetails.setDirectBill(getAgentDetails(reservationPaxExtTnx.getAgentCode()));

		return paymentDetails;

	}

	/**
	 * construct pax credit payment detail
	 * 
	 * @param reservationTnx
	 * @param codeGroup
	 * @param opCarrier
	 *            TODO
	 * @return
	 */
	public static PaymentDetails getPaxCreditPaymentDetails(ReservationTnx reservationTnx, CurrencyCodeGroup codeGroup,
			String opCarrier) {

		PaymentDetails paymentDetails = getBasicPaymentDetail(reservationTnx, codeGroup, opCarrier);
		TravelerCreditPayment travelerCreditPayment = new TravelerCreditPayment();
		travelerCreditPayment.setDebitTravelerRefNumber(null);
		if (reservationTnx.getNominalCode().equals(5)) {
			travelerCreditPayment.setDescription("PREVIOUS CREDIT CF");
		} else if (reservationTnx.getNominalCode().equals(6)) {
			travelerCreditPayment.setDescription("PREVIOUS CREDIT BF");
		}
		paymentDetails.setTravelerCreditPayment(travelerCreditPayment);

		return paymentDetails;

	}

	/**
	 * construct pax credit payment detail for @{link ReservationPaxExtTnx}
	 * 
	 * @param reservationTnx
	 * @param codeGroup
	 * @param opCarrier
	 *            TODO
	 * @return
	 */
	static PaymentDetails getPaxCreditPaymentDetails(ReservationPaxExtTnx reservationPaxExtTnx, CurrencyCodeGroup codeGroup,
			String opCarrier) {

		PaymentDetails paymentDetails = getBasicPaymentDetail(reservationPaxExtTnx, codeGroup, opCarrier);
		TravelerCreditPayment travelerCreditPayment = new TravelerCreditPayment();
		travelerCreditPayment.setDebitTravelerRefNumber(null);
		if (reservationPaxExtTnx.getNominalCode().equals(5)) {
			travelerCreditPayment.setDescription("PREVIOUS CREDIT CF");
		} else if (reservationPaxExtTnx.getNominalCode().equals(6)) {
			travelerCreditPayment.setDescription("PREVIOUS CREDIT BF");
		}
		paymentDetails.setTravelerCreditPayment(travelerCreditPayment);

		return paymentDetails;

	}

	/**
	 * construct credit card payment detail
	 * 
	 * @param reservationTnx
	 * @param codeGroup
	 * @param opCarrier
	 *            TODO
	 * @return
	 * @throws ModuleException
	 */
	public static PaymentDetails getCreditCardPaymentDetails(ReservationTnx reservationTnx, CurrencyCodeGroup codeGroup,
			String opCarrier) throws ModuleException {

		PaymentDetails paymentDetails = getBasicPaymentDetail(reservationTnx, codeGroup, opCarrier);
		CommonCreditCardPayment paymentCard = new CommonCreditCardPayment();
		paymentCard.setCardType(AAUtils.getPaymentCardType(reservationTnx.getNominalCode()));

		fillCreditCardInfo(paymentCard, reservationTnx);
		paymentDetails.setPaymentCard(paymentCard);
		return paymentDetails;

	}

	/**
	 * construct credit card payment detail for @{link ReservationPaxExtTnx}
	 * 
	 * @param reservationTnx
	 * @param codeGroup
	 * @param opCarrier
	 *            TODO
	 * @return
	 * @throws ModuleException
	 */
	static PaymentDetails getCreditCardPaymentDetails(ReservationPaxExtTnx reservationPaxExtTnx, CurrencyCodeGroup codeGroup,
			String opCarrier) throws ModuleException {

		PaymentDetails paymentDetails = getBasicPaymentDetail(reservationPaxExtTnx, codeGroup, opCarrier);
		CommonCreditCardPayment paymentCard = new CommonCreditCardPayment();
		paymentCard.setCardType(AAUtils.getPaymentCardType(reservationPaxExtTnx.getNominalCode()));

		fillCreditCardInfo(paymentCard, reservationPaxExtTnx);
		paymentDetails.setPaymentCard(paymentCard);

		return paymentDetails;
	}

	public static PaymentDetails getLoyaltyPaymentDetails(ReservationTnx reservationTnx, CurrencyCodeGroup codeGroup,
			String opCarrier) {
		PaymentDetails paymentDetails = getBasicPaymentDetail(reservationTnx, codeGroup, opCarrier);
		paymentDetails.setDirectBill(getAgentDetails(reservationTnx.getAgentCode()));
		paymentDetails.setLoyaltyFFID(reservationTnx.getExternalReference());
		setPaymentRefundableFlag(paymentDetails);
		return paymentDetails;

	}

	// creates PaymentDetails for pax-tnx record.
	private static PaymentDetails getBasicPaymentDetail(ReservationTnx tnx, CurrencyCodeGroup codeGroup, String opCarrier) {

		PaymentDetails paymentDetails = new PaymentDetails();
		paymentDetails.setPaymentSummary(new PaymentSummary());
		paymentDetails.getPaymentSummary().setPaymentAmount(tnx.getAmount().negate());
		paymentDetails.getPaymentSummary().setApplicableDateTime(tnx.getDateTime());
		paymentDetails.getPaymentSummary().setCurrencyCodeGroup(codeGroup);
		paymentDetails.getPaymentSummary().setPaymentCarrier(tnx.getPaymentCarrier());
		paymentDetails.setCarrierCode(opCarrier);
		paymentDetails.setDummyPayment(tnx.getDummyPayment());
		paymentDetails.setNorminalCode(tnx.getNominalCode());
		paymentDetails.setUniqueTnxId(tnx.getLccUniqueId());
		paymentDetails.setSalesChannelCode(tnx.getSalesChannelCode());
		paymentDetails.setUserId(tnx.getUserId());
		paymentDetails.setAgentCode(tnx.getAgentCode());
		paymentDetails.setRemarks(tnx.getRemarksRemovingUnwantedCharacters());
		paymentDetails.setPaymentCurrencyAmount(getPaymentCurrncyAmount(tnx.getPayCurrencyAmount().negate(),
				tnx.getPayCurrencyCode()));
		paymentDetails.setOriginalPaymentRefNumber(generateCarrierViseValue(tnx.getPaymentCarrier(), tnx.getTnxId().toString()));
		paymentDetails.setCarrierVisePayments(generateCarrierViseValue(tnx.getPaymentCarrier(), tnx.getAmount().negate()
				.toString()));
		paymentDetails.setOriginalPaymentTnxDateTime(tnx.getDateTime());
		if (tnx.getOriginalPaymentTnxID() != null) {
			paymentDetails.setOriginalPaymentRefNumber(generateCarrierViseValue(tnx.getPaymentCarrier(), tnx
					.getOriginalPaymentTnxID().toString()));
		}

		if (AppSysParamsUtil.isEnableTransactionGranularity()) {
			Collection<ReservationPaxOndPayment> reservationPaxOndPayments = tnx.getReservationPaxTnxOndBreakdown();
			if (reservationPaxOndPayments != null && !reservationPaxOndPayments.isEmpty()) {
				for (ReservationPaxOndPayment ondPayment : reservationPaxOndPayments) {
					paymentDetails.getOndPaymetBreakdowns().add(getOndPaymetBreakdowns(ondPayment));
				}
			}
		}
		paymentDetails.setFirstPayment(CommonsConstants.YES.equals(tnx.getFirstPayment()));
		return paymentDetails;
	}

	// creates the basic payment details for external tnx record.
	private static PaymentDetails getBasicPaymentDetail(ReservationPaxExtTnx eTnx, CurrencyCodeGroup codeGroup, String opCarrier) {

		PaymentDetails paymentDetails = new PaymentDetails();
		paymentDetails.setPaymentSummary(new PaymentSummary());
		BigDecimal paymentAmount = eTnx.getAmount().negate();
		BigDecimal paymentCurrencyAmount = eTnx.getAmountPayCur().negate();
		paymentDetails.getPaymentSummary().setPaymentAmount(paymentAmount);
		paymentDetails.getPaymentSummary().setApplicableDateTime(eTnx.getDateTime());
		paymentDetails.getPaymentSummary().setCurrencyCodeGroup(codeGroup);

		// for external tnx records found in this should be payments made by this carrier.
		paymentDetails.getPaymentSummary().setPaymentCarrier(thisAirlineCode);

		paymentDetails.setCarrierCode(opCarrier);
		paymentDetails.setNorminalCode(eTnx.getNominalCode());
		paymentDetails.setUniqueTnxId(eTnx.getLccUniqueId());
		paymentDetails.setSalesChannelCode(eTnx.getSalesChannelCode());
		paymentDetails.setUserId(eTnx.getUserId());
		paymentDetails.setAgentCode(eTnx.getAgentCode());
		paymentDetails.setRemarks(eTnx.getRemarks());
		paymentDetails.setPaymentCurrencyAmount(getPaymentCurrncyAmount(paymentCurrencyAmount, eTnx.getPayCurrencyCode()));
		paymentDetails.setOriginalPaymentRefNumber(generateCarrierViseValue(eTnx.getExternalCarrierCode(), eTnx
				.getPaxExternalTnxId().toString()));
		paymentDetails.setOriginalPaymentTnxDateTime(eTnx.getDateTime());
		paymentDetails.setCarrierVisePayments(generateCarrierViseValue(eTnx.getExternalCarrierCode(), paymentAmount.toString()));

		if (eTnx.getOriginalExtPaymentTnxID() != null) {
			paymentDetails.setOriginalPaymentTnxRefNumber(generateCarrierViseValue(eTnx.getExternalCarrierCode(), eTnx
					.getOriginalExtPaymentTnxID().toString()));
		}

		paymentDetails.setFirstPayment(CommonsConstants.YES.equals(eTnx.getFirstPayment()));

		return paymentDetails;
	}

	/**
	 * Generate carrier Wise value <carrier>=<value> Ex : G9=1000
	 * 
	 * @param carrier
	 * @param value
	 * @return
	 */
	private static String generateCarrierViseValue(String carrier, String value) {
		StringBuilder sb = new StringBuilder();
		sb.append(carrier).append("=").append(value);
		return sb.toString();
	}

	private static OndPaymentBreakdownDetails getOndPaymetBreakdowns(ReservationPaxOndPayment ondPayment) {
		OndPaymentBreakdownDetails details = new OndPaymentBreakdownDetails();
		details.setAmount(ondPayment.getAmount());
		details.setCharger(ChargeGroupCode.valueOf(ondPayment.getChargeGroupCode().toUpperCase()));
		details.setNorminalCode(ondPayment.getNominalCode());
		details.setRemarks(ondPayment.getRemarks());
		/**
		 * do we need the ondPayment breakdown payment amount as well ? Need to revise the payment breakdown recording
		 * if we want the payment breakdown in payment currency as well
		 */

		return details;
	}

	private static PaymentCurrencyAmount getPaymentCurrncyAmount(BigDecimal payAmount, String payCurrencyCode) {

		PaymentCurrencyAmount payCurrencyAmount = new PaymentCurrencyAmount();
		payCurrencyAmount.setPaymentCurrencyAmount(payAmount);
		CurrencyCodeGroup currencyCodeGroup = new CurrencyCodeGroup();
		currencyCodeGroup.setCurrencyCode(payCurrencyCode);
		currencyCodeGroup.setDecimalPlaces(AppSysParamsUtil.getNumberOfDecimalPoints());
		payCurrencyAmount.setCurrencyCodeGroup(currencyCodeGroup);

		return payCurrencyAmount;
	}

	public static PaymentDetails createPaymentDetails(ReservationTnx reservationTnx, String airlineCode,
			CurrencyCodeGroup codeGroup, boolean isCash) throws ModuleException {
		PaymentDetails paymentDetails = new PaymentDetails();
		paymentDetails.setPaymentSummary(new PaymentSummary());
		paymentDetails.setCash(true);
		if (codeGroup == null)
			codeGroup = FindReservationUtil.getDefaultCurrencyCodeGroup();
		paymentDetails.getPaymentSummary().setCurrencyCodeGroup(codeGroup);
		paymentDetails.getPaymentSummary().setPaymentAmount(reservationTnx.getAmount().negate());
		paymentDetails.setCarrierCode(airlineCode);
		paymentDetails.getPaymentSummary().setApplicableDateTime(reservationTnx.getDateTime());
		paymentDetails.getPaymentSummary().setPaymentCarrier(reservationTnx.getPaymentCarrier());
		paymentDetails.setCash(isCash);
		return paymentDetails;
	}

	public static PaymentDetails createPaymentDetails(Date applicableDataTime, BigDecimal amount, String currencyCode,
			String airlineCode) throws ModuleException {
		PaymentDetails paymentDetails = new PaymentDetails();
		paymentDetails.setPaymentSummary(new PaymentSummary());
		paymentDetails.getPaymentSummary().setApplicableDateTime(applicableDataTime);
		paymentDetails.getPaymentSummary().setPaymentAmount(amount);
		paymentDetails.setCarrierCode(airlineCode);
		CurrencyCodeGroup codeGroup = FindReservationUtil.getDefaultCurrencyCodeGroup();
		if (currencyCode == null || currencyCode.equals(""))
			codeGroup.setCurrencyCode(currencyCode);
		paymentDetails.getPaymentSummary().setCurrencyCodeGroup(codeGroup);
		return paymentDetails;
	}

	private static DirectBill getAgentDetails(String agentCode) {
		DirectBill agentDetails = new DirectBill();
		agentDetails.setAgentCode(agentCode);

		if (StringUtils.isNotEmpty(agentCode)) {
			try {
				Agent agent = AAReservationUtil.loadTravelAgent(agentCode);
				if (agent != null) {
					agentDetails.setAgentName(agent.getAgentName());
					agentDetails.setAgentTypeCode(agent.getAgentTypeCode());
				}
			} catch (Exception ex) {
				// safe to ignore. we just couldn't get the agent's name.
				log.warn("Error occurred while retrieving agent information for agent code : " + agentCode);
			}

		}

		return agentDetails;
	}

	/**
	 * Fills the credit card information for a given {@link ReservationTnx}
	 * 
	 * @param ccPayment
	 * @param paxTnx
	 * @throws ModuleException
	 */
	private static void fillCreditCardInfo(CommonCreditCardPayment ccPayment, ReservationTnx paxTnx) throws ModuleException {

		if (paxTnx != null && ReservationTnxNominalCode.getCreditCardNominalCodes().contains(paxTnx.getNominalCode())) {
			CardPaymentInfo cardPaymentInfo = null;
			Collection<Integer> txnIds = new ArrayList<Integer>();
			txnIds.add(paxTnx.getTnxId());

			Map<Integer, CardPaymentInfo> mapCardPayInfo = AAServicesModuleUtils.getReservationBD().getCreditCardInfoFromRESPCD(
					txnIds, true);
			if (mapCardPayInfo != null && mapCardPayInfo.containsKey(paxTnx.getTnxId())) {
				cardPaymentInfo = mapCardPayInfo.get(paxTnx.getTnxId());
				if (cardPaymentInfo != null) {
					ccPayment.setNo(cardPaymentInfo.getNo());
					ccPayment.setFirst4Digits(cardPaymentInfo.getNoFirstDigits());
					ccPayment.setLast4Digits(cardPaymentInfo.getNoLastDigits());
					ccPayment.setEDate(cardPaymentInfo.getEDate());
					ccPayment.setAuthorizationId(cardPaymentInfo.getAuthorizationId());
					ccPayment.setCardHolderName(cardPaymentInfo.getName());
					ccPayment.setPaymentBrokerId(cardPaymentInfo.getPaymentBrokerRefNo());
				} else {
					log.error(String.format("Could not find a credit card infomation entry for tnx-id: %d ", paxTnx.getTnxId()));
					throw new ModuleException("cc.information.not,found.for.payment");
				}
			} else {
				log.error(String.format("Could not find a credit card infomation entry for tnx-id: %d ", paxTnx.getTnxId()));
			}
		}
	}

	/**
	 * Fills the credit card information for a given {@link ReservationPaxExtTnx}
	 * 
	 * TODO : incomplete method.
	 * 
	 * @param ccPayment
	 * @param paxExtTnx
	 * @throws ModuleException
	 */
	private static void fillCreditCardInfo(CommonCreditCardPayment ccPayment, ReservationPaxExtTnx paxExtTnx)
			throws ModuleException {
		if (paxExtTnx != null && ReservationTnxNominalCode.getCreditCardNominalCodes().contains(paxExtTnx.getNominalCode())) {
			CardPaymentInfo cardPaymentInfo = null;
			Collection<Integer> txnIds = new ArrayList<Integer>();
			txnIds.add(paxExtTnx.getPaxExternalTnxId());

			Map<Integer, CardPaymentInfo> mapCardPayInfo = AAServicesModuleUtils.getReservationBD().getCreditCardInfoFromRESPCD(
					txnIds, false);
			if (mapCardPayInfo != null && mapCardPayInfo.containsKey(paxExtTnx.getPaxExternalTnxId())) {
				cardPaymentInfo = mapCardPayInfo.get(paxExtTnx.getPaxExternalTnxId());
				if (cardPaymentInfo != null) {
					ccPayment.setNo(cardPaymentInfo.getNo());
					ccPayment.setEDate(cardPaymentInfo.getEDate());
					ccPayment.setFirst4Digits(cardPaymentInfo.getNoLastDigits());
					ccPayment.setLast4Digits(cardPaymentInfo.getNoLastDigits());
					ccPayment.setAuthorizationId(cardPaymentInfo.getAuthorizationId());
					ccPayment.setCardHolderName(cardPaymentInfo.getName());
					ccPayment.setPaymentBrokerId(cardPaymentInfo.getPaymentBrokerRefNo());
				} else {
					log.error(String.format("Could not find a credit card infomation entry for tnx-id: %d ",
							paxExtTnx.getPaxExternalTnxId()));
					throw new ModuleException("cc.information.not,found.for.payment");
				}
			} else {
				log.error(String.format("Could not find a credit card infomation entry for tnx-id: %d ",
						paxExtTnx.getPaxExternalTnxId()));
			}
		}
	}

	/**
	 * Add the amounts in a ReservationTnx object to the list of PaymentDetails IF the LCC unique key for ReservationTnx
	 * object appears in any of the items in PaymentDetails list.
	 * 
	 * @param tnx
	 *            : The tnx record.
	 * @param paymentDetailList
	 *            : The PaymentDetails list.
	 * @return : true if the payment was added. false otherwise.
	 */
	static boolean addTransactionToExisitingPaymentDetail(ReservationTnx tnx, List<PaymentDetails> paymentDetailList) {
		boolean transactionAddedToExisitingPayment = false;

		if (tnx != null && paymentDetailList != null) {
			for (PaymentDetails paymentDetail : paymentDetailList) {

				if (paymentDetail != null && StringUtils.isNotEmpty(paymentDetail.getUniqueTnxId())
						&& StringUtils.isNotEmpty(tnx.getLccUniqueId())) {
					if (paymentDetail.getUniqueTnxId().equals(tnx.getLccUniqueId())
							&& ReservationTnxNominalCode.VOUCHER_PAYMENT.getCode() != tnx.getNominalCode()) {

						// add the 2 amounts together and set as the new payment detail amount.
						BigDecimal amount = paymentDetail.getPaymentSummary().getPaymentAmount();
						BigDecimal newamount = AccelAeroCalculator.add(amount, tnx.getAmount().negate());
						paymentDetail.getPaymentSummary().setPaymentAmount(newamount);

						// add the 2 payment amounts together and set as the new payment detail amount.
						BigDecimal amountPaycur = paymentDetail.getPaymentCurrencyAmount().getPaymentCurrencyAmount();
						BigDecimal newAmountPayCur = AccelAeroCalculator.add(amountPaycur, tnx.getPayCurrencyAmount().negate());
						paymentDetail.getPaymentCurrencyAmount().setPaymentCurrencyAmount(newAmountPayCur);

						// adding the new OriginalPaymetRef to existing one
						String originalPaymentRefNumber = paymentDetail.getOriginalPaymentRefNumber();
						String newPaymentRefNumber = generateCarrierViseValue(tnx.getPaymentCarrier(), tnx.getTnxId().toString());
						paymentDetail.setOriginalPaymentRefNumber(new StringBuilder(originalPaymentRefNumber)
								.append(CARRIER_SEPERATOR).append(newPaymentRefNumber).toString());

						// adding carrier vise payments
						String existingCarrierPayment = paymentDetail.getCarrierVisePayments();
						String newCarrierPayment = generateCarrierViseValue(tnx.getPaymentCarrier(), tnx.getAmount().negate()
								.toEngineeringString());
						paymentDetail.setCarrierVisePayments(new StringBuffer(existingCarrierPayment).append(CARRIER_SEPERATOR)
								.append(newCarrierPayment).toString());

						transactionAddedToExisitingPayment = true;
						break;
					}
				}
			}
		}

		return transactionAddedToExisitingPayment;
	}

	/**
	 * Add the amounts in a ReservationPaxExtTnx object to the list of PaymentDetails IF the LCC unique key for
	 * ReservationPaxExtTnx object appears in any of the items in PaymentDetails list.
	 * 
	 * @param extTnx
	 *            : The external tnx record.
	 * @param paymentDetailList
	 *            : The PaymentDetails list.
	 * @return : true if the payment was added. false otherwise.
	 */
	static boolean addExtTransactionToExisitingPaymentDetail(ReservationPaxExtTnx extTnx, List<PaymentDetails> paymentDetailList) {
		boolean transactionAddedToExisitingPayment = false;
		List<String> tobeRmPaymentFromDisplay = new ArrayList<String>();

		if (extTnx != null && paymentDetailList != null) {
			for (PaymentDetails paymentDetail : paymentDetailList) {
				if (paymentDetail != null && StringUtils.isNotEmpty(paymentDetail.getUniqueTnxId())
						&& StringUtils.isNotEmpty(extTnx.getLccUniqueId())
						&& isSameVoucherPaymentToMerge(extTnx, paymentDetail)) {
					if (paymentDetail.getUniqueTnxId().equals(extTnx.getLccUniqueId())) {
						BigDecimal paymentAmount = extTnx.getAmount().negate();
						BigDecimal paymentCurrencyAmount = extTnx.getAmountPayCur().negate();
						// add the 2 amounts together and set as the new payment detail amount.
						BigDecimal amount = paymentDetail.getPaymentSummary().getPaymentAmount();
						BigDecimal newamount = AccelAeroCalculator.add(amount, paymentAmount);
						paymentDetail.getPaymentSummary().setPaymentAmount(newamount);

						if (newamount.doubleValue() < PER_PAX_ERROR_MARGIN) {
							tobeRmPaymentFromDisplay.add(paymentDetail.getUniqueTnxId());
							transactionAddedToExisitingPayment = true;
							break;
						}

						// add the 2 payment amounts together and set as the new payment detail amount.
						BigDecimal amountPaycur = paymentDetail.getPaymentCurrencyAmount().getPaymentCurrencyAmount();
						BigDecimal newAmountPayCur = AccelAeroCalculator.add(amountPaycur, paymentCurrencyAmount);
						paymentDetail.getPaymentCurrencyAmount().setPaymentCurrencyAmount(newAmountPayCur);

						// adding the new OriginalPaymetRef to existing one
						String originalPaymentRefNumber = paymentDetail.getOriginalPaymentRefNumber();
						String newPaymentRefNumber = generateCarrierViseValue(extTnx.getExternalCarrierCode(),
								extTnx.getPaxExternalTnxId().toString());
						StringBuilder sb = new StringBuilder(originalPaymentRefNumber).append(CARRIER_SEPERATOR)
								.append(newPaymentRefNumber);
						paymentDetail.setOriginalPaymentRefNumber(sb.toString());

						// adding carrier vise payments
						String existingCarrierPayment = paymentDetail.getCarrierVisePayments();
						String newCarrierPayment = generateCarrierViseValue(extTnx.getExternalCarrierCode(),
								paymentAmount.toString());
						paymentDetail.setCarrierVisePayments(new StringBuffer(existingCarrierPayment).append(CARRIER_SEPERATOR)
								.append(newCarrierPayment).toString());

						transactionAddedToExisitingPayment = true;
						break;
					}

				}
			}
		}

		if (tobeRmPaymentFromDisplay.size() > 0) {
			Iterator<PaymentDetails> payIte = paymentDetailList.iterator();
			while (payIte.hasNext()) {
				PaymentDetails tmpPaymentDetails = payIte.next();
				if (tobeRmPaymentFromDisplay.contains(tmpPaymentDetails.getUniqueTnxId())) {
					payIte.remove();
				}
			}
		}

		return transactionAddedToExisitingPayment;
	}
	
	private static void setPaymentRefundableFlag(PaymentDetails paymentDetails) {
		Collection<Integer> colNonRefundablePaymentCodes = ReservationTnxNominalCode.getNonRefundableTypePaymentCodes();
		if (paymentDetails != null && colNonRefundablePaymentCodes != null && !colNonRefundablePaymentCodes.isEmpty()
				&& colNonRefundablePaymentCodes.contains(paymentDetails.getNorminalCode())) {
			paymentDetails.setNonRefundable(true);
		}
	}

	public static PaymentDetails getVoucherPaymentDetails(ReservationTnx reservationTnx, CurrencyCodeGroup codeGroup,
			String opCarrier) {
		PaymentDetails paymentDetails = getBasicPaymentDetail(reservationTnx, codeGroup, opCarrier);
		paymentDetails.setDirectBill(getAgentDetails(reservationTnx.getAgentCode()));
		TravelerVoucherPayment voucherPayment = new TravelerVoucherPayment();
		voucherPayment.setVoucherID(reservationTnx.getExternalReference());
		voucherPayment.setVoucherRedeemAmount(reservationTnx.getPayCurrencyAmount());
		voucherPayment.setVoucherID(reservationTnx.getExternalReference());
		paymentDetails.setVoucherPayment(voucherPayment);

		return paymentDetails;
	}


	public static PaymentDetails getVoucherPaymentDetails(ReservationPaxExtTnx reservationPaxExtTnx, CurrencyCodeGroup codeGroup,
			String opCarrier) {
		PaymentDetails paymentDetails = getBasicPaymentDetail(reservationPaxExtTnx, codeGroup, opCarrier);
		paymentDetails.setDirectBill(getAgentDetails(reservationPaxExtTnx.getAgentCode()));
		TravelerVoucherPayment voucherPayment = new TravelerVoucherPayment();
		voucherPayment.setVoucherRedeemAmount(reservationPaxExtTnx.getAmountPayCur());
		voucherPayment.setVoucherID(reservationPaxExtTnx.getExternalReference());
		paymentDetails.setVoucherPayment(voucherPayment);

		return paymentDetails;
	}
	
	private static boolean isSameVoucherPaymentToMerge(ReservationPaxExtTnx extTnx, PaymentDetails paymentDetail) {
		if (ReservationTnxNominalCode.VOUCHER_PAYMENT.getCode() != extTnx.getNominalCode()) {
			return true;
		} else if (ReservationTnxNominalCode.VOUCHER_PAYMENT.getCode() == extTnx.getNominalCode()
				&& StringUtils.isNotEmpty(extTnx.getExternalReference()) && paymentDetail.getVoucherPayment() != null
				&& extTnx.getExternalReference().equals(paymentDetail.getVoucherPayment().getVoucherID())) {
			return true;
		}
		return false;
	}

}
