package com.isa.thinair.aaservices.core.bl.reservation;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AirReservation;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AirTraveler;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TravelerInfo;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAirBookModifyRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAReservation;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.PaxTypeUtils;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Business class to handle splitting passengers from a reservation.
 * 
 * @author agupta
 */
class AASplitReservationBL {

	private static final Log log = LogFactory.getLog(AASplitReservationBL.class);

	// Request & Response objects
	private AAAirBookModifyRQ aaAirBookModifyRQ;

	private Reservation reservation;

	public AAAirBookModifyRQ getAaAirBookModifyRQ() {
		return aaAirBookModifyRQ;
	}

	public void setAaAirBookModifyRQ(AAAirBookModifyRQ aaAirBookModifyRQ) {
		this.aaAirBookModifyRQ = aaAirBookModifyRQ;
	}

	public Reservation getReservation() {
		return reservation;
	}

	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}

	/**
	 * The main business logic encapsulate function, which is used to delegate the split functionality to the
	 * ReservationBD class.
	 * 
	 * @return
	 * @throws WebservicesException
	 * @throws ModuleException
	 */
	String execute() throws WebservicesException, ModuleException {

		if (log.isDebugEnabled()) {
			log.debug("execute method called.");
		}

		AAReservation aaReservation = aaAirBookModifyRQ.getAaReservation();
		AirReservation airReservation = aaReservation.getAirReservation();
		TravelerInfo travelerInfo = airReservation.getTravelerInfo();

		List<AirTraveler> listAirTraveller = travelerInfo.getAirTraveler();
		List<Integer> pnrPaxIds = getPaxIds(listAirTraveller);

		String pnr = aaAirBookModifyRQ.getAaReservation().getPnr();
		String newOriginatorPNR = aaAirBookModifyRQ.getAaReservation().getGroupPnr();
		long version = reservation.getVersion();

		if (log.isDebugEnabled()) {
			log.debug("Calling ReservationBD.splitReservation");
			log.debug("Parameters being passed : ");
			log.debug("PNR for the concerned reservation : " + pnr);
			log.debug("PaxIds to be split : " + pnrPaxIds);
			log.debug("Reservation version : " + version);
			log.debug("newOriginatorPNR : " + newOriginatorPNR);
			log.debug("newCarrierPNR : " + newOriginatorPNR);
		}

		// Calling the reservation service bean for splitting the reservation.
		ServiceResponce serviceResponse = AAServicesModuleUtils.getReservationBD().splitReservation(pnr, pnrPaxIds, version,
				newOriginatorPNR, newOriginatorPNR, null, null, null);

		if (log.isDebugEnabled()) {
			log.debug("ServiceResponce returned successfuly : " + serviceResponse.isSuccess());
		}

		if (serviceResponse != null && serviceResponse.isSuccess()) {
			return (String) serviceResponse.getResponseParam(CommandParamNames.PNR);
		} else {
			throw new ModuleException("airreservations.arg.invalidSplit");
		}
	}

	/**
	 * Method to get the paxIds from the list of AirTraveller
	 * 
	 * @param listAirTraveller
	 * @return
	 */
	private List<Integer> getPaxIds(List<AirTraveler> listAirTraveller) {
		List<Integer> pnrPaxIds = new ArrayList<Integer>();

		String travelerReferenceNumber = null;
		for (AirTraveler airTraveller : listAirTraveller) {
			travelerReferenceNumber = BeanUtils.nullHandler(airTraveller.getTravelerRefNumber());
			if (travelerReferenceNumber.length() > 0) {
				pnrPaxIds.add(PaxTypeUtils.getPnrPaxId(travelerReferenceNumber));
			}
		}
		return pnrPaxIds;
	}
}
