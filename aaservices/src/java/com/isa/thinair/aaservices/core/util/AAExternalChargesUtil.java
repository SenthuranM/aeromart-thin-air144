package com.isa.thinair.aaservices.core.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import com.isa.connectivity.profiles.maxico.v1.common.dto.ExternalCharge;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlexiInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PerPaxPriceInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PriceInfo;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAErrorCode;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.SMExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ServiceTaxExtCharges;
import com.isa.thinair.airreservation.api.dto.ServiceTaxExtChgDTO;
import com.isa.thinair.airreservation.api.dto.baggage.BaggageExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.flexi.FlexiExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.flexi.ReservationPaxOndFlexibilityDTO;
import com.isa.thinair.airreservation.api.dto.meal.MealExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ssr.SSRExternalChargeDTO;
import com.isa.thinair.airreservation.api.model.ReservationPaxOndFlexibility;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.SSRUtil;
import com.isa.thinair.commons.core.util.StringUtil;

/**
 * @author Nilindra Fernando
 */
public class AAExternalChargesUtil {

	public static BigDecimal getTotalExternalChargeAmount(Map<String, Collection<ExternalChgDTO>> externalChargesMap) {
		BigDecimal amount = AccelAeroCalculator.getDefaultBigDecimalZero();

		if (externalChargesMap != null && externalChargesMap.size() > 0) {

			for (Collection<ExternalChgDTO> colExternalChgDTO : externalChargesMap.values()) {

				if (colExternalChgDTO != null && colExternalChgDTO.size() > 0) {
					for (ExternalChgDTO externalChgDTO : colExternalChgDTO) {
						if (externalChgDTO != null) {
							amount = AccelAeroCalculator.add(amount, externalChgDTO.getAmount());
						}
					}
				}
			}
		}

		return amount;
	}

	public static Map<String, Collection<ExternalChgDTO>> getPassengerWiseExternalCharges(PriceInfo priceInfo,
			int chargesAplicablity) throws WebservicesException, ModuleException {

		Map<String, Collection<ExternalChgDTO>> extAAExternalChgDTOMap = new HashMap<String, Collection<ExternalChgDTO>>();
		Map<String, Collection<ExternalCharge>> extChargesMap = new HashMap<String, Collection<ExternalCharge>>();
		Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new HashSet<EXTERNAL_CHARGES>();

		if (priceInfo != null) {
			if (priceInfo.getPerPaxPriceInfo().size() > 0) {
				for (PerPaxPriceInfo perPaxPriceInfo : priceInfo.getPerPaxPriceInfo()) {
					extChargesMap.put(perPaxPriceInfo.getTravelerRefNumber(), perPaxPriceInfo.getPassengerPrice()
							.getExternalCharge());

					colEXTERNAL_CHARGES.addAll(deriveExternalCharge(perPaxPriceInfo.getPassengerPrice().getExternalCharge()));
				}
			}
		}

		if (colEXTERNAL_CHARGES.size() > 0) {

			Map extExternalChgDTOMap = AAServicesModuleUtils.getReservationBD().getQuotedExternalCharges(colEXTERNAL_CHARGES,
					null, chargesAplicablity);

			for (String travellerRefNo : extChargesMap.keySet()) {
				Collection<ExternalCharge> colExternalCharge = extChargesMap.get(travellerRefNo);

				Collection<ExternalCharge> updatedExternalCharges = new ArrayList<ExternalCharge>();
				for (ExternalCharge externalCharge : colExternalCharge) {
					updatedExternalCharges.add(externalCharge);
				}
				extAAExternalChgDTOMap.put(travellerRefNo,
						transformToAAExternalCharges(extExternalChgDTOMap, updatedExternalCharges));
			}
		}

		return extAAExternalChgDTOMap;
	}

	private static Collection<ExternalChgDTO> transformToAAExternalCharges(Map extExternalChgDTOMap,
			Collection<ExternalCharge> colExternalCharge) throws WebservicesException {

		Collection<ExternalChgDTO> colExternalChgDTO = new ArrayList<ExternalChgDTO>();

		for (ExternalCharge externalCharge : colExternalCharge) {
			List<ExternalCharge> oneExternalCharge = new ArrayList<ExternalCharge>();
			oneExternalCharge.add(externalCharge);

			EXTERNAL_CHARGES extEnum = BeanUtils.getFirstElement(deriveExternalCharge(oneExternalCharge));
			ExternalChgDTO externalChgDTO = (ExternalChgDTO) extExternalChgDTOMap.get(extEnum);

			externalChgDTO = (ExternalChgDTO) externalChgDTO.clone();
			if (externalCharge.getJourneyType() != null) {
				externalChgDTO.setApplicableChargeRate(externalCharge.getJourneyType());
			}
			
			externalChgDTO.setOperationMode(externalCharge.getOperationMode());
			
			externalChgDTO.setAmountConsumedForPayment(true);
			externalChgDTO.setAmount(externalCharge.getAmount());
			if (!StringUtil.isNullOrEmpty(externalCharge.getFlightRefNumber())) {
				externalChgDTO.setFlightSegId(FlightRefNumberUtil.getSegmentIdFromFlightRPH(externalCharge.getFlightRefNumber()));
			}

			if (externalChgDTO instanceof SMExternalChgDTO) {				
				((SMExternalChgDTO) externalChgDTO).setSeatNumber(externalCharge.getCode());
			} else if (externalChgDTO instanceof MealExternalChgDTO) {				
				((MealExternalChgDTO) externalChgDTO).setMealCode(externalCharge.getCode());
				((MealExternalChgDTO) externalChgDTO).setAnciOffer(externalCharge.isIsAnciOffer());
				((MealExternalChgDTO) externalChgDTO).setOfferedTemplateID(externalCharge.getOfferedAnciTemplateID());
			} else if (externalChgDTO instanceof BaggageExternalChgDTO) {				
				((BaggageExternalChgDTO) externalChgDTO).setBaggageCode(externalCharge.getCode());
				((BaggageExternalChgDTO) externalChgDTO).setOndBaggageChargeGroupId(externalCharge.getOndBaggageChargeGroupId());
				((BaggageExternalChgDTO) externalChgDTO).setOndBaggageGroupId(externalCharge.getOndBaggageGroupId());
			} else if (externalChgDTO instanceof FlexiExternalChgDTO) {				
				((FlexiExternalChgDTO) externalChgDTO).setChargeCode(externalCharge.getCode());

				if (externalCharge.getAdditionalDetails() != null && externalCharge.getAdditionalDetails().size() > 0) {
					for (FlexiInfo flexibility : externalCharge.getAdditionalDetails()) {
						ReservationPaxOndFlexibilityDTO flexi = new ReservationPaxOndFlexibilityDTO();
						flexi.setAvailableCount(flexibility.getAvailableCount());
						flexi.setStatus(ReservationPaxOndFlexibility.STATUS_ACTIVE);
						flexi.setUtilizedCount(0);
						flexi.setFlexiRateId(flexibility.getFlexiRateId());
						flexi.setFlexibilityTypeId(flexibility.getFlexibilityTypeId());
						((FlexiExternalChgDTO) externalChgDTO).addToReservationFlexibilitiesList(flexi);
					}
				}
			} else if (externalChgDTO instanceof SSRExternalChargeDTO) {
				String ssrCode = externalCharge.getCode();
				int ssrID = SSRUtil.getSSRId(ssrCode);
				((SSRExternalChargeDTO) externalChgDTO).setSSRId(ssrID);
				((SSRExternalChargeDTO) externalChgDTO).setFlightRPH(externalCharge.getFlightRefNumber());
				((SSRExternalChargeDTO) externalChgDTO).setChargeCode(ssrCode);
				((SSRExternalChargeDTO) externalChgDTO).setAirportCode(externalCharge.getAirportCode());
				((SSRExternalChargeDTO) externalChgDTO).setApplyOn(externalCharge.getApplyOn());
				((SSRExternalChargeDTO) externalChgDTO).setSSRText(externalCharge.getUserNote());
				((SSRExternalChargeDTO) externalChgDTO).setSegmentSequece(externalCharge.getSegmentSequence());
			} else if (externalChgDTO instanceof ServiceTaxExtChgDTO) {
				((ServiceTaxExtChgDTO) externalChgDTO).setFlightRefNumber(externalCharge.getFlightRefNumber());
			} else if (externalChgDTO instanceof ServiceTaxExtCharges) {
				if (!StringUtil.isNullOrEmpty(externalCharge.getCode())) {
					externalChgDTO = (ServiceTaxExtChgDTO) ((ServiceTaxExtCharges) extExternalChgDTOMap
							.get(externalChgDTO.getExternalChargesEnum())).getRespectiveServiceChgDTO(externalCharge.getCode());
					if (externalChgDTO != null) {
						externalChgDTO = (ServiceTaxExtChgDTO) externalChgDTO.clone();
						((ServiceTaxExtChgDTO) externalChgDTO).setFlightRefNumber(externalCharge.getFlightRefNumber());
						((ServiceTaxExtChgDTO) externalChgDTO).setChargeCode(externalCharge.getCode());
						((ServiceTaxExtChgDTO) externalChgDTO).setTicketingRevenue(externalCharge.isTicketingRevenue());
						((ServiceTaxExtChgDTO) externalChgDTO).setTaxableAmount(externalCharge.getTaxableAmount());
						((ServiceTaxExtChgDTO) externalChgDTO).setNonTaxableAmount(externalCharge.getNonTaxableAmount());
						((ServiceTaxExtChgDTO) externalChgDTO).setAmount(externalCharge.getAmount());
						((ServiceTaxExtChgDTO) externalChgDTO)
								.setTaxDepositCountryCode(externalCharge.getTaxDepositCountryCode());
						((ServiceTaxExtChgDTO) externalChgDTO).setTaxDepositStateCode(externalCharge.getTaxDepositStateCode());
					}

				}
			}

			colExternalChgDTO.add(externalChgDTO);
		}

		return colExternalChgDTO;
	}

	private static Collection<EXTERNAL_CHARGES> deriveExternalCharge(List<ExternalCharge> colExternalCharge)
			throws WebservicesException {
		Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new ArrayList<EXTERNAL_CHARGES>();

		if (colExternalCharge != null && colExternalCharge.size() > 0) {
			for (ExternalCharge externalCharge : colExternalCharge) {
				if (BeanUtils.nullHandler(externalCharge.getType()).equals(
						ReservationInternalConstants.EXTERNAL_CHARGES.CREDIT_CARD.toString())) {
					colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.CREDIT_CARD);
				} else if (BeanUtils.nullHandler(externalCharge.getType()).equals(
						ReservationInternalConstants.EXTERNAL_CHARGES.HANDLING_CHARGE.toString())) {
					colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.HANDLING_CHARGE);
				} else if (BeanUtils.nullHandler(externalCharge.getType()).equals(
						ReservationInternalConstants.EXTERNAL_CHARGES.MEAL.toString())) {
					colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.MEAL);
				} else if (BeanUtils.nullHandler(externalCharge.getType()).equals(
						ReservationInternalConstants.EXTERNAL_CHARGES.BAGGAGE.toString())) {
					colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.BAGGAGE);
				} else if (BeanUtils.nullHandler(externalCharge.getType()).equals(
						ReservationInternalConstants.EXTERNAL_CHARGES.SEAT_MAP.toString())) {
					colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.SEAT_MAP);
				} else if (BeanUtils.nullHandler(externalCharge.getType()).equals(
						ReservationInternalConstants.EXTERNAL_CHARGES.INFLIGHT_SERVICES.toString())) {
					colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.INFLIGHT_SERVICES);
				} else if (BeanUtils.nullHandler(externalCharge.getType()).equals(
						ReservationInternalConstants.EXTERNAL_CHARGES.INSURANCE.toString())) {
					colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.INSURANCE);
				} else if (BeanUtils.nullHandler(externalCharge.getType()).equals(
						ReservationInternalConstants.EXTERNAL_CHARGES.FLEXI_CHARGES.toString())) {
					colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.FLEXI_CHARGES);
				} else if (BeanUtils.nullHandler(externalCharge.getType()).equals(
						ReservationInternalConstants.EXTERNAL_CHARGES.AIRPORT_SERVICE.toString())) {
					colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.AIRPORT_SERVICE);
				} else if (BeanUtils.nullHandler(externalCharge.getType()).equals(
						ReservationInternalConstants.EXTERNAL_CHARGES.PROMOTION_REWARD.toString())) {
					colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.PROMOTION_REWARD);
				} else if (BeanUtils.nullHandler(externalCharge.getType()).equals(
						ReservationInternalConstants.EXTERNAL_CHARGES.ADDITIONAL_SEAT_CHARGE.toString())) {
					colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.ADDITIONAL_SEAT_CHARGE);
				} else if (BeanUtils.nullHandler(externalCharge.getType()).equals(
						ReservationInternalConstants.EXTERNAL_CHARGES.JN_ANCI.toString())) {
					colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.JN_ANCI);
				} else if (BeanUtils.nullHandler(externalCharge.getType()).equals(
						ReservationInternalConstants.EXTERNAL_CHARGES.JN_OTHER.toString())) {
					colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.JN_OTHER);
				} else if (BeanUtils.nullHandler(externalCharge.getType())
						.equals(ReservationInternalConstants.EXTERNAL_CHARGES.SERVICE_TAX.toString())) {
					colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.SERVICE_TAX);
				} else if (BeanUtils.nullHandler(externalCharge.getType())
						.equals(ReservationInternalConstants.EXTERNAL_CHARGES.BSP_FEE.toString())) {
					colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.BSP_FEE);
				}
				else {
					throw new WebservicesException(AAErrorCode.ERR_1_MAXICO_REQUIRED_INVALID_VALUE,
							"Invalid external charge, charge code [" + externalCharge.getType() + "] ");
				}
			}
		}

		return colEXTERNAL_CHARGES;
	}

}
