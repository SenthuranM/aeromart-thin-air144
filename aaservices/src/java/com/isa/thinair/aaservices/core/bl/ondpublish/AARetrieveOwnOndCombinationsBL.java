package com.isa.thinair.aaservices.core.bl.ondpublish;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.LanguageDescription;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OndLocation;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OndPublishAirport;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OndPublishCurrency;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OndPublishDestination;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OndPublishOrigin;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TransitHubCodes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAOndCombinationsRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAOndCombinationsRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.commons.api.dto.ondpublish.LanguageDescriptionDTO;
import com.isa.thinair.commons.api.dto.ondpublish.OndPublishAirportDTO;
import com.isa.thinair.commons.api.dto.ondpublish.OndPublishCurrencyDTO;
import com.isa.thinair.commons.api.dto.ondpublish.OndPublishDestinationDTO;
import com.isa.thinair.commons.api.dto.ondpublish.OndPublishOriginDTO;
import com.isa.thinair.commons.api.dto.ondpublish.OndPublishedTO;
import com.isa.thinair.commons.api.exception.ModuleException;

public class AARetrieveOwnOndCombinationsBL {

	private static final Log log = LogFactory.getLog(AARetrieveOwnOndCombinationsBL.class);

	private AAOndCombinationsRQ aaOndCombinationsRQ;

	public AAOndCombinationsRS execute() {

		// Generate list of own airline onds
		AAOndCombinationsRS aaOndCombinationsRS = null;

		if (log.isDebugEnabled())
			log.debug("RetrieveOwnOndCombinations starts for " + aaOndCombinationsRQ.getAirlineCode());

		try {

			OndPublishedTO generatedOnds = AAServicesModuleUtils.getCommonMasterBD().getOndListForSystem();

			if (generatedOnds != null && generatedOnds.getOrigins() != null && generatedOnds.getOrigins().size() > 0) {
				aaOndCombinationsRS = transformToLccResponse(generatedOnds);
			} else {
				aaOndCombinationsRS = new AAOndCombinationsRS();
			}
			aaOndCombinationsRS.setResponseAttributes(new AAResponseAttributes());
			aaOndCombinationsRS.getResponseAttributes().setSuccess(new AASuccess());

		} catch (ModuleException ex) {
			log.error("Generating Ond combinations failed for:" + aaOndCombinationsRQ.getAirlineCode());
			aaOndCombinationsRS = new AAOndCombinationsRS();
			aaOndCombinationsRS.setResponseAttributes(new AAResponseAttributes());
			AAExceptionUtil.addAAErrrors(aaOndCombinationsRS.getResponseAttributes().getErrors(), ex);
			aaOndCombinationsRS.getResponseAttributes().setSuccess(null);
		}

		if (log.isDebugEnabled())
			log.debug("RetrieveOwnOndCombinations ends");

		return aaOndCombinationsRS;
	}

	private AAOndCombinationsRS transformToLccResponse(OndPublishedTO publishedOnds) {

		AAOndCombinationsRS aaOndCombinationsRS = new AAOndCombinationsRS();

		// Transform Airports
		aaOndCombinationsRS.getOndPublishAirports().addAll(transformToLccAirports(publishedOnds.getAirports()));
		// Transform Currencies
		aaOndCombinationsRS.getOndPublishCurrencies().addAll(transformToLccCurrencies(publishedOnds.getCurrencies()));
		// Transform Origins/Destinations
		aaOndCombinationsRS.getOndPublishOrigins().addAll(transformToLccOrigins(publishedOnds.getOrigins()));

		return aaOndCombinationsRS;

	}

	private List<LanguageDescription> transformToLccLangDescriptions(List<LanguageDescriptionDTO> languageDescriptions) {

		List<LanguageDescription> langDescsLcc = new ArrayList<LanguageDescription>();

		LanguageDescription langDescLcc;
		for (LanguageDescriptionDTO langDesc : languageDescriptions) {
			langDescLcc = new LanguageDescription();
			langDescLcc.setLanguageCode(langDesc.getLanguageCode());
			langDescLcc.setDescription(langDesc.getDescription());
			langDescsLcc.add(langDescLcc);
		}

		return langDescsLcc;

	}

	private List<OndPublishAirport> transformToLccAirports(List<OndPublishAirportDTO> airports) {

		List<OndPublishAirport> airportsLcc = new ArrayList<OndPublishAirport>();

		OndPublishAirport airportLcc;
		for (OndPublishAirportDTO airport : airports) {
			airportLcc = new OndPublishAirport();
			airportLcc.setAirportCode(airport.getAirportCode());
			airportLcc.getCityAirportCodes().addAll(airport.getCityAirportCodes());
			airportLcc.setCity(airport.isCity());
			airportLcc.getLanguageDescriptions().addAll(transformToLccLangDescriptions(airport.getLanguageDescriptions()));
			airportsLcc.add(airportLcc);
		}
		return airportsLcc;

	}

	private List<OndPublishCurrency> transformToLccCurrencies(List<OndPublishCurrencyDTO> currencies) {

		List<OndPublishCurrency> currenciesLcc = new ArrayList<OndPublishCurrency>();

		OndPublishCurrency currencyLcc;
		for (OndPublishCurrencyDTO currency : currencies) {
			currencyLcc = new OndPublishCurrency();
			currencyLcc.setCurrencyCode(currency.getCurrencyCode());
			currencyLcc.getLanguageDescriptions().addAll(transformToLccLangDescriptions(currency.getLanguageDescriptions()));
			currenciesLcc.add(currencyLcc);
		}
		return currenciesLcc;

	}

	private List<OndPublishOrigin> transformToLccOrigins(List<OndPublishOriginDTO> origins) {

		List<OndPublishOrigin> originsLcc = new ArrayList<OndPublishOrigin>();

		OndPublishOrigin originLcc;
		for (OndPublishOriginDTO origin : origins) {
			originLcc = new OndPublishOrigin();
			OndLocation ondLocation = new OndLocation();
			
			ondLocation.setLocationCode(origin.getOndLocation().getOndLocationCode());
			ondLocation.setCity(origin.getOndLocation().getCity());
			
			originLcc.setOndLocation(ondLocation);
			originLcc.getDestinations().addAll(transformToLccDestinations(origin.getDestinations()));
			originsLcc.add(originLcc);
		}
		return originsLcc;

	}

	private List<OndPublishDestination> transformToLccDestinations(List<OndPublishDestinationDTO> destinations) {

		List<OndPublishDestination> destinationsLcc = new ArrayList<OndPublishDestination>();

		OndPublishDestination destinationLcc;
		for (OndPublishDestinationDTO destination : destinations) {
			destinationLcc = new OndPublishDestination();
			OndLocation ondLocation = new OndLocation();
			
			ondLocation.setLocationCode(destination.getOndLocation().getOndLocationCode());
			ondLocation.setCity(destination.getOndLocation().getCity());
			
			destinationLcc.setOndLocation(ondLocation);
			destinationLcc.setCurrency(destination.getCurrencyCode());
			destinationLcc.setMktCarrierCode(destination.getMktCarrierCode());
			destinationLcc.setRouteType(destination.getRouteType());
			destinationLcc.getOperatingCarriers().addAll(destination.getOperatingCarriers());

			List<TransitHubCodes> tempTransitHubs = new ArrayList<TransitHubCodes>();
			TransitHubCodes transitHubs;
			for (List<String> hubCodes : destination.getTransitHubs()) {
				transitHubs = new TransitHubCodes();
				transitHubs.getHubCodes().addAll(hubCodes);
				tempTransitHubs.add(transitHubs);
			}
			destinationLcc.getTransitHubs().addAll(tempTransitHubs);

			destinationsLcc.add(destinationLcc);
		}
		return destinationsLcc;

	}
	
	private OndLocation getOndLocation(String locationCode, Boolean city) {
		OndLocation ondLocation = new OndLocation();
		ondLocation.setCity(city);
		ondLocation.setLocationCode(locationCode);
		return ondLocation;
	}

	public void setAaOndCombinationsRQ(AAOndCombinationsRQ aaOndCombinationsRQ) {
		this.aaOndCombinationsRQ = aaOndCombinationsRQ;
	}

}
