package com.isa.thinair.aaservices.core.session;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Holds user data during user session.
 * 
 * @author Nasly
 */
public class AAUserSession implements Serializable {

	private static final long serialVersionUID = -2679211186634370240L;
	/** User Session data keys */
	static final String USER_PRINCIPAL = "userPrincipal";
	public static final String USER_PRIVILEGES_KEYS = "userPrivKeys";
	static final String TXN_IDS_SESSION_KEY = "$_USER_TXN_IDS";

	/** Holds all the user related data */
	private Map<String, Object> userData = Collections.synchronizedMap(new HashMap<String, Object>());

	/** For expiration purposes */
	private long lastAccessed;

	private int status = 0;

	public AAUserSession() {
		updateLastAccessed();
	}

	/**
	 * Add parameter to the UserSession
	 * 
	 * @param key
	 * @param value
	 */
	void addParameter(String key, Object value) {
		userData.put(key, value);
		updateLastAccessed();
	}

	/**
	 * Get parameter from the UserSession
	 * 
	 * @param key
	 * @return
	 */
	Object getParameter(String key) {
		updateLastAccessed();
		return userData.get(key);
	}

	/**
	 * Remove parameter from the UserSession
	 * 
	 * @param key
	 * @return
	 */
	Object removeParameter(String key) {
		updateLastAccessed();
		return userData.remove(key);
	}

	/**
	 * Sets the last accessed time
	 */
	private void updateLastAccessed() {
		if (getStatus() == STATUS_EXPIRED) {
			throw new RuntimeException("User Session Expired");
		}
		lastAccessed = System.currentTimeMillis();
	}

	/**
	 * Returns last accessed time in millis
	 * 
	 * @return
	 */
	public long getLastAccessedTimestamp() {
		return lastAccessed;
	}

	/**
	 * Returns session status.
	 * 
	 * @return
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * Sets session status.
	 * 
	 * @param status
	 */
	private void setStatus(int status) {
		this.status = status;
	}

	/**
	 * Expire session if timed out.
	 * 
	 * @param sessionTimeoutDuration
	 */
	boolean qualifyForExpiration(long sessionTimeoutDuration) {
		boolean isExpired = false;
		if (System.currentTimeMillis() - getLastAccessedTimestamp() > sessionTimeoutDuration) {
			setStatus(STATUS_EXPIRED);
			userData.clear();
			isExpired = true;
		}
		return isExpired;
	}

	private static int STATUS_EXPIRED = -1;
}
