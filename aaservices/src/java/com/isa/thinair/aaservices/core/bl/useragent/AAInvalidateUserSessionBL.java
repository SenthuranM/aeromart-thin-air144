package com.isa.thinair.aaservices.core.bl.useragent;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.required.dto.AAInvalidateAASessionsForUserRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAInvalidateAASessionsForUserRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.core.session.AASessionManager;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * Invalidates the aaservices session for the given user.
 * 
 * @author sanjaya
 */
public class AAInvalidateUserSessionBL {

	/** Class logger */
	private static final Log log = LogFactory.getLog(AAInvalidateUserSessionBL.class);

	/** The request object */
	private AAInvalidateAASessionsForUserRQ invalidateAASessionsForUserRQ;

	/**
	 * @param request
	 *            The request object to set.
	 */
	public void setRequest(AAInvalidateAASessionsForUserRQ request) {
		this.invalidateAASessionsForUserRQ = request;
	}

	// Initializes the response variable.
	private AAInvalidateAASessionsForUserRS initializeAAInvalidateAASessionsForUserRS() {
		AAInvalidateAASessionsForUserRS invalidateAASessionsForUserRS = new AAInvalidateAASessionsForUserRS();
		invalidateAASessionsForUserRS.setResponseAttributes(new AAResponseAttributes());
		return invalidateAASessionsForUserRS;
	}

	/**
	 * Removes the AASession object for the provided user id.
	 * 
	 * @return
	 */
	public AAInvalidateAASessionsForUserRS execute() {

		AAInvalidateAASessionsForUserRS response = initializeAAInvalidateAASessionsForUserRS();

		try {

			if (invalidateAASessionsForUserRQ == null) {
				throw new ModuleException("The request object is empty for removing AASession");
			}

			String userId = invalidateAASessionsForUserRQ.getUserId();
			String sessionId = invalidateAASessionsForUserRQ.getAaPos().getUserSessionId();
			Object removedSession = AASessionManager.getInstance().removeUserSession(userId, sessionId);

			if (removedSession != null) {
				if (log.isDebugEnabled()) {
					log.debug(String.format("Removed AASession for user id[%s],  session id [%s] in carrier [%s]", userId, sessionId,
							AppSysParamsUtil.getDefaultCarrierCode()));
				}
			} else {
				if (log.isDebugEnabled()) {
					log.debug(String.format("No AASession found for user id[%s],  session id [%s] in carrier [%s] for removal",
							userId, sessionId, AppSysParamsUtil.getDefaultCarrierCode()));
				}
			}

		} catch (Exception ex) {
			log.error(ex);
			AAExceptionUtil.addAAErrrors(response.getResponseAttributes().getErrors(), ex);
		} finally {
			int numOfErrors = response.getResponseAttributes().getErrors().size();
			AASuccess successType = (numOfErrors == 0 ? new AASuccess() : null);
			response.getResponseAttributes().setSuccess(successType);
		}

		return response;
	}
}
