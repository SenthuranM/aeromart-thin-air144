package com.isa.thinair.aaservices.core.util;

import java.util.ArrayList;
import java.util.List;

import com.isa.connectivity.profiles.maxico.v1.common.dto.ChargeAdjustmentTypePrivilege;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ChargeAdjustmentTypePrivileges;
import com.isa.thinair.aaservices.core.session.AASessionManager;
import com.isa.thinair.commons.api.dto.ChargeAdjustmentTypeDTO;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;

/**
 * A Utility class for charge adjustment privilege related functionalities in aaservices.
 * 
 * @author sanjaya
 * 
 */
public class AAChargeAdjustmentTypePrivilegeUtil {

	/**
	 * Gets the charge adjustment privileges for this carrier.
	 * 
	 * @return : the charge adjustment privileges.
	 */
	public static ChargeAdjustmentTypePrivileges getChargeAdjustmentPrivileges() {

		ChargeAdjustmentTypePrivileges chargeAdjustmentPrivileges = new ChargeAdjustmentTypePrivileges();
		chargeAdjustmentPrivileges.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
		List<ChargeAdjustmentTypePrivilege> privilegeList = new ArrayList<ChargeAdjustmentTypePrivilege>();

		List<ChargeAdjustmentTypeDTO> chargeAdjustmentTypes = CommonsServices.getGlobalConfig().getChargeAdjustmentTypes();

		for (ChargeAdjustmentTypeDTO chargeAdjustmentType : chargeAdjustmentTypes) {

			ChargeAdjustmentTypePrivilege chargeAdjustmentTypePrivilege = new ChargeAdjustmentTypePrivilege();
			chargeAdjustmentTypePrivilege.setChargeAdjustmentTypeId(chargeAdjustmentType.getChargeAdjustmentTypeId());

			if (AASessionManager.getUserPrivileges().contains(chargeAdjustmentType.getRefundablePrivilegeId())) {
				chargeAdjustmentTypePrivilege.setRefundAllowed(true);
			} else {
				chargeAdjustmentTypePrivilege.setRefundAllowed(false);
			}

			if (AASessionManager.getUserPrivileges().contains(chargeAdjustmentType.getNonRefundablePrivilegeId())) {
				chargeAdjustmentTypePrivilege.setNonRefundAllowed(true);
			} else {
				chargeAdjustmentTypePrivilege.setNonRefundAllowed(false);
			}
			
			privilegeList.add(chargeAdjustmentTypePrivilege);
		}

		chargeAdjustmentPrivileges.getChargeAdjustmentPrivilegeList().addAll(privilegeList);
		return chargeAdjustmentPrivileges;
	}
}