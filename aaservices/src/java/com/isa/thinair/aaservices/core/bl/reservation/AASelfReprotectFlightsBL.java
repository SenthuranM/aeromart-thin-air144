/**
 * 
 */
package com.isa.thinair.aaservices.core.bl.reservation;

import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegment;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASelfReprotectFlightsRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASelfReprotectFlightsRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.airproxy.api.dto.SelfReprotectFlightDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author suneth
 *
 */
public class AASelfReprotectFlightsBL {

	private static Log log = LogFactory.getLog(AASelfReprotectFlightsBL.class);

	AASelfReprotectFlightsRQ aaSelfReprotectFlightsRQ;

	public AASelfReprotectFlightsBL(AASelfReprotectFlightsRQ aaSelfReprotectFlightsRQ) {
		this.aaSelfReprotectFlightsRQ = aaSelfReprotectFlightsRQ;
	}

	public AASelfReprotectFlightsRS execute() {

		AASelfReprotectFlightsRS aaSelfReprotectFlightsRS = new AASelfReprotectFlightsRS();

		try {

			Collection<SelfReprotectFlightDTO> flights = AAServicesModuleUtils.getReservationBD().getSelfReprotectFlights(
					aaSelfReprotectFlightsRQ.getAlertId());

			for (SelfReprotectFlightDTO selfReprotectFlightDTO : flights) {

				FlightSegment flightSegment = new FlightSegment();

				flightSegment.setFlightNumber(selfReprotectFlightDTO.getFlightNumber());
				flightSegment.setArrivalDateTime(selfReprotectFlightDTO.getArrivalDate());
				flightSegment.setDepatureDateTime(selfReprotectFlightDTO.getDepartureDate());
				flightSegment.setFlightId(selfReprotectFlightDTO.getFlightId());
				flightSegment.setFlightRefNumber(selfReprotectFlightDTO.getFltSegId());
				flightSegment.setSegmentCode(selfReprotectFlightDTO.getSegmentCode());

				aaSelfReprotectFlightsRS.getFlightSegments().add(flightSegment);

			}

			aaSelfReprotectFlightsRS.setResponseAttributes(new AAResponseAttributes());
			aaSelfReprotectFlightsRS.getResponseAttributes().setSuccess(new AASuccess());

		} catch (ModuleException e) {

			log.error("Getting flights for self reprotection falid for interline.", e);
			AAExceptionUtil.addAAErrrors(aaSelfReprotectFlightsRS.getResponseAttributes().getErrors(), e);
			e.printStackTrace();
		}

		return aaSelfReprotectFlightsRS;

	}

}
