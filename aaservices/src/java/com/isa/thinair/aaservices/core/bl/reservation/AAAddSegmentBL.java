package com.isa.thinair.aaservices.core.bl.reservation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.connectivity.profiles.maxico.v1.common.dto.ExpectedBookingStates;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ExternalCharge;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ExternalChargeType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.InsuranceRequest;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OriginDestinationOption;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaxOndFlexibility;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PerPaxPriceInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PriceInfo;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAirBookModifyRQ;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.aaservices.core.transaction.TransactionalReservationProcess;
import com.isa.thinair.aaservices.core.util.AAExternalChargesUtil;
import com.isa.thinair.aaservices.core.util.AAReservationUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.AAUtils;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ExternalSegmentTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.dto.flexi.ReservationPaxOndFlexibilityDTO;
import com.isa.thinair.airreservation.api.model.ISegment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationInsurance;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.model.assembler.SegmentAssembler;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.ReleaseTimeUtil;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.constants.CommonsConstants.ChargeRateOperationType;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.webplatform.api.util.AddModifySurfaceSegmentValidations;
import com.isa.thinair.webplatform.api.util.ReservationUtil;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;
import com.isa.thinair.wsclient.api.dto.aig.IInsuranceRequest;
import com.isa.thinair.wsclient.api.dto.aig.InsuranceRequestAssembler;
import com.isa.thinair.wsclient.api.dto.aig.InsurePassenger;
import com.isa.thinair.wsclient.api.dto.aig.InsureSegment;

/**
 * @author Nilindra Fernando
 */
class AAAddSegmentBL {

	static void addOND(Reservation reservation, TransactionalReservationProcess tnxResProcess,
			AAAirBookModifyRQ aaAirBookModifyRQ, Collection privilegeKeys) throws WebservicesException, ModuleException {

		String pnr = aaAirBookModifyRQ.getAaReservation().getPnr();
		long version = -1L;
		if (reservation.getDummyBooking() == ReservationInternalConstants.DummyBooking.NO) {
			version = Long.parseLong(aaAirBookModifyRQ.getAaReservation().getAirReservation().getVersion());
		} else {
			version = reservation.getVersion();
		}
		Collection<OndFareDTO> colOndFareDTOs = tnxResProcess.getOndFareDTOListForReservation(aaAirBookModifyRQ
				.getSelectedFareTypes());

		TrackInfoDTO trackInfo = AAUtils.getTrackInfo(aaAirBookModifyRQ.getAaPos());
		Collection blockedSeatsCol = tnxResProcess.getBlockSeats(aaAirBookModifyRQ.getSelectedFareTypes());
		if (blockedSeatsCol == null) {
			blockedSeatsCol = AAServicesModuleUtils.getReservationBD().blockSeats(colOndFareDTOs, null);
		}

		Collection colReservationFlights = WebplatformUtil.getConfirmedDepartureSegments(reservation.getSegmentsView());
		Collection colNewFlightSegmentDTOs = ReleaseTimeUtil.getFlightDepartureInfo(colOndFareDTOs);
		Collection colAccumilatedSegments = new ArrayList();
		colAccumilatedSegments.addAll(colReservationFlights);
		colAccumilatedSegments.addAll(colNewFlightSegmentDTOs);

		String marketingAgentCode = aaAirBookModifyRQ.getAaPos().getMarketingAgentCode();

		Date newReleaseTimestamp = WebplatformUtil.getReleaseTimeStamp(colAccumilatedSegments, privilegeKeys, true,
				marketingAgentCode, aaAirBookModifyRQ.getAppIndicator(),
				ReleaseTimeUtil.getOnHoldReleaseTimeDTOFromOndFares(colOndFareDTOs));

		Integer intPaymentMode = ReservationInternalConstants.ReservationPaymentModes.TRIGGER_LEAVE_STATES_AS_IT_IS_BUT_NO_OWNERSHIP_CHANGE;

		if (aaAirBookModifyRQ.getExpectedBookingStates() != null
				&& aaAirBookModifyRQ.getExpectedBookingStates() == ExpectedBookingStates.FORCED_CONFIRMED) {
			intPaymentMode = ReservationInternalConstants.ReservationPaymentModes.TRIGGER_PNR_FORCE_CONFIRMED_BUT_NO_OWNERSHIP_CHANGE;
		}

		Map<Integer, String> groundStationBySegmentMap = new LinkedHashMap<Integer, String>();
		Map<Integer, Integer> groundSementFlightByAirFlightMap = new LinkedHashMap<Integer, Integer>();
		try {
			AddModifySurfaceSegmentValidations.mapStationsBySegments(aaAirBookModifyRQ.getNewAAReservation().getAirReservation()
					.getAirItinerary().getOriginDestinationOption(), groundStationBySegmentMap, groundSementFlightByAirFlightMap);
		} catch (Exception ex) {
			// Do nothing
		}

		Map<Integer, String> flightSegIdWiseBaggageOndGrpId = getFlightSegIdWiseBaggageOndGrpIds(aaAirBookModifyRQ
				.getNewAAReservation().getAirReservation().getAirItinerary().getOriginDestinationOption());
		ISegment addedResSegs = ReservationUtil.prepareNewResSegments(reservation, colOndFareDTOs, groundStationBySegmentMap,
				groundSementFlightByAirFlightMap, flightSegIdWiseBaggageOndGrpId);
		addedResSegs.overrideZuluReleaseTimeStamp(newReleaseTimestamp);

		addedResSegs.setAutoCancellationInfo(AAReservationUtil.populateAutoCancellationInfo(aaAirBookModifyRQ
				.getAutoCancellationInfo()));

		addSegmentSSeqToExtCharges(((SegmentAssembler) addedResSegs).getSegmentObjectsMap(), aaAirBookModifyRQ.getAaReservation()
				.getAirReservation().getPriceInfo().getPerPaxPriceInfo());

		addPayments(reservation, addedResSegs, aaAirBookModifyRQ.getAaReservation().getAirReservation().getPriceInfo());
		Map<Integer, List<ReservationPaxOndFlexibilityDTO>> ondFlexibilitiesMap = transformPaxOndFlexibilityDTO(aaAirBookModifyRQ
				.getAaReservation().getAirReservation().getUpdatedPaxOndFlexibility());

		populateInsurance(addedResSegs, aaAirBookModifyRQ.getAaReservation().getAirReservation().getTravelerInfo()
				.getSpecialReqDetails().getInsuranceRequests(), reservation);

		addedResSegs.setUserNote(aaAirBookModifyRQ.getUserNote());
		AAServicesModuleUtils.getSegmentBD().addSegments(reservation.getPnr(), addedResSegs, blockedSeatsCol, intPaymentMode,
				version, trackInfo, false, false, aaAirBookModifyRQ.getSelectedPnrSegID(), ondFlexibilitiesMap, false,
				aaAirBookModifyRQ.isHasExternalPayments(), true);

		Collection<ExternalSegmentTO> newOndWiseExternalFlightSegments = AAModifyReservationUtil
				.getOndWiseExternalFlightSegments(aaAirBookModifyRQ.getNewAAReservation().getAirReservation().getAirItinerary(),
						ReservationInternalConstants.ReservationExternalSegmentStatus.CONFIRMED);

		if (newOndWiseExternalFlightSegments.size() > 0) {
			Reservation tmpReservation = AAReservationUtil.getReservation(pnr, null, false, false, false, false, false, false,
					false);
			AAServicesModuleUtils.getSegmentBD().changeExternalSegments(pnr, newOndWiseExternalFlightSegments,
					tmpReservation.getVersion(), null);
		}
	}

	private static Map<Integer, String> getFlightSegIdWiseBaggageOndGrpIds(List<OriginDestinationOption> originDestinationOption) {
		Map<Integer, String> flightSegIdWiseBaggageOndGrpId = new HashMap<Integer, String>();

		for (OriginDestinationOption originDestinationOptionType : originDestinationOption) {
			for (FlightSegment bookFlightSegment : originDestinationOptionType.getFlightSegment()) {
				Integer extFlightSegRef = FlightRefNumberUtil.getSegmentIdFromFlightRPH(bookFlightSegment.getFlightRefNumber());
				flightSegIdWiseBaggageOndGrpId.put(extFlightSegRef, bookFlightSegment.getBaggageONDGroupId());
			}
		}

		return flightSegIdWiseBaggageOndGrpId;
	}

	private static void addSegmentSSeqToExtCharges(Map segmentMap, List<PerPaxPriceInfo> perPaxPriceInfoList) {
		if (perPaxPriceInfoList != null) {
			for (PerPaxPriceInfo perPaxPriceInfo : perPaxPriceInfoList) {
				if (perPaxPriceInfo.getPassengerPrice() != null) {
					for (ExternalCharge externalCharge : perPaxPriceInfo.getPassengerPrice().getExternalCharge()) {
						if (externalCharge.getType().equals(ExternalChargeType.INFLIGHT_SERVICES)
								|| externalCharge.getType().equals(ExternalChargeType.AIRPORT_SERVICE)) {
							if (externalCharge.getFlightRefNumber() != null && !externalCharge.getFlightRefNumber().equals("")
									&& segmentMap.get(new Integer(externalCharge.getFlightRefNumber())) != null) {
								ReservationSegment reservationSegment = (ReservationSegment) segmentMap.get(new Integer(
										externalCharge.getFlightRefNumber()));
								externalCharge.setSegmentSequence(reservationSegment.getSegmentSeq());
							}
						}
					}
				}
			}
		}
	}

	private static void populateInsurance(ISegment iSegment, List<InsuranceRequest> insuranceRequests, Reservation reservation) {
		IInsuranceRequest insurance = new InsuranceRequestAssembler();

		ReservationBD reservationBD = AAServicesModuleUtils.getReservationBD();

		if (insuranceRequests != null && !insuranceRequests.isEmpty()) {

			for (InsuranceRequest insuranceRequest : insuranceRequests) {

				ReservationInsurance reservationInsurance = reservationBD.getReservationInsurance(new Integer(insuranceRequest
						.getInsuranceRefNumber()));
				InsureSegment insSegment = new InsureSegment();
				insSegment.setFromAirportCode(reservationInsurance.getOrigin());
				insSegment.setToAirportCode(reservationInsurance.getDestination());
				insSegment.setDepartureDate(reservationInsurance.getDateOfTravel());
				insSegment.setArrivalDate(reservationInsurance.getDateOfReturn());
				insSegment.setRoundTrip(reservationInsurance.getRoundTrip().equals("Y") ? true : false);
				insSegment.setSalesChanelCode(SalesChannelsUtil.SALES_CHANNEL_AGENT);

				insurance.addFlightSegment(insSegment);
				insurance.setInsuranceId(reservationInsurance.getInsuranceId());

				insurance.setSsrFeeCode(insuranceRequest.getSsrFeeCode());
				insurance.setPlanCode(insuranceRequest.getPlanCode());

				BigDecimal amount = reservationInsurance.getQuotedAmount();
				insurance.setQuotedTotalPremiumAmount(amount);

				BigDecimal insPaxWise[] = AccelAeroCalculator.roundAndSplit(amount, reservation.getTotalPaxAdultCount()
						+ reservation.getTotalPaxChildCount());
				int i = 0;
				for (ReservationPax pax : reservation.getPassengers()) {
					if (!PaxTypeTO.INFANT.equals(pax.getPaxType())) {
						InsurePassenger insurePassenger = new InsurePassenger();

						// adding AIG Pax info
						insurePassenger.setFirstName(pax.getFirstName());
						insurePassenger.setLastName(pax.getLastName());
						insurePassenger.setDateOfBirth(pax.getDateOfBirth());
						ReservationContactInfo contactInfo = reservation.getContactInfo();
						// Adding AIG Contact Info
						insurePassenger.setAddressLn1(contactInfo.getStreetAddress1());
						insurePassenger.setAddressLn2(contactInfo.getStreetAddress2());
						insurePassenger.setCity(contactInfo.getCity());
						insurePassenger.setEmail(contactInfo.getEmail());
						insurePassenger.setCountryOfAddress(contactInfo.getCountryCode());
						insurePassenger.setHomePhoneNumber(contactInfo.getPhoneNo());

						insurance.addPassenger(insurePassenger);
						insurance.addInsuranceCharge(pax.getPaxSequence(), insPaxWise[i++]);
					}
				}

				iSegment.addInsurance(insurance);
			}

		}

	}

	/**
	 * Add dummy payments with external charges
	 * 
	 * @param reservation
	 * @param addedResSegs
	 * @param priceInfo
	 * @throws ModuleException
	 * @throws WebservicesException
	 */
	private static void addPayments(Reservation reservation, ISegment addedResSegs, PriceInfo priceInfo) throws ModuleException,
			WebservicesException {
		// Add zero payments for passengers not having payment

		Map<String, Collection<ExternalChgDTO>> externalChargesMap = AAExternalChargesUtil.getPassengerWiseExternalCharges(
				priceInfo, ChargeRateOperationType.MODIFY_ONLY);

		for (Object element : reservation.getPassengers()) {
			ReservationPax pnrPax = (ReservationPax) element;
			// Payment should always sent for the adults/children only
			if (!ReservationApiUtils.isInfantType(pnrPax)) {
				String paxSeq = pnrPax.getPaxSequence() + "";
				PaymentAssembler paymentAsm = new PaymentAssembler();
				paymentAsm.addExternalCharges(externalChargesMap.get(paxSeq));
				addedResSegs.addPassengerPayments(pnrPax.getPnrPaxId(), paymentAsm);
			}
		}
	}

	private static Map<Integer, List<ReservationPaxOndFlexibilityDTO>> transformPaxOndFlexibilityDTO(
			List<PaxOndFlexibility> paxOndFlexibilityList) {
		Map<Integer, List<ReservationPaxOndFlexibilityDTO>> ondFlexibilitiesMap = new HashMap<Integer, List<ReservationPaxOndFlexibilityDTO>>();
		List<ReservationPaxOndFlexibilityDTO> reservationPaxOndFlexibilityDTOs = null;

		Set<Integer> pnrpaxIdList = null;
		if (paxOndFlexibilityList != null) {
			pnrpaxIdList = new HashSet<Integer>();
			for (PaxOndFlexibility reservationPaxOndFlexibility : paxOndFlexibilityList) {
				pnrpaxIdList.add(reservationPaxOndFlexibility.getPnrPaxId());
			}
		}

		if (pnrpaxIdList != null) {
			for (int pnrPaxId : pnrpaxIdList) {
				reservationPaxOndFlexibilityDTOs = new ArrayList<ReservationPaxOndFlexibilityDTO>();
				for (PaxOndFlexibility reservationPaxOndFlexibility : paxOndFlexibilityList) {
					if (pnrPaxId == reservationPaxOndFlexibility.getPnrPaxId()) {
						ReservationPaxOndFlexibilityDTO paxOndFlexibilityDTO = new ReservationPaxOndFlexibilityDTO();
						paxOndFlexibilityDTO.setAvailableCount(reservationPaxOndFlexibility.getAvailableCount());
						paxOndFlexibilityDTO.setCutOverBufferInMins(reservationPaxOndFlexibility.getCutOverBufferInMins());
						paxOndFlexibilityDTO.setDescription(reservationPaxOndFlexibility.getDescription());
						paxOndFlexibilityDTO.setFlexibilityTypeId(reservationPaxOndFlexibility.getFlexibilityTypeId());
						paxOndFlexibilityDTO.setFlexiRateId(reservationPaxOndFlexibility.getFlexibilityTypeId());
						paxOndFlexibilityDTO.setPpfId(reservationPaxOndFlexibility.getPpfId());
						paxOndFlexibilityDTO.setPpOndFlxId(reservationPaxOndFlexibility.getPpOndFlxId());
						paxOndFlexibilityDTO.setStatus(reservationPaxOndFlexibility.getStatus());
						paxOndFlexibilityDTO.setUtilizedCount(reservationPaxOndFlexibility.getUtilizedCount());
						reservationPaxOndFlexibilityDTOs.add(paxOndFlexibilityDTO);
					}
				}
				ondFlexibilitiesMap.put(pnrPaxId, reservationPaxOndFlexibilityDTOs);
			}
		}
		return ondFlexibilitiesMap;
	}

}
