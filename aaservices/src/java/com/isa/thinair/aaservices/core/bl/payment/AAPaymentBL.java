package com.isa.thinair.aaservices.core.bl.payment;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.CommonCreditCardPayment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaymentDetails;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TravelerCreditPayment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.TravelerFulfillment;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAPaymentCheckRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAPaymentCheckRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAPaymentRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAPaymentRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.AAServicesTransformUtil;
import com.isa.thinair.aaservices.core.util.AAUtils;
import com.isa.thinair.aaservices.core.util.TransformerUtil;
import com.isa.thinair.airreservation.api.dto.PaymentInfo;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.PaymentType;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.TnxCreditPayment;
import com.isa.thinair.airreservation.api.model.assembler.PaymentAssembler;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.dto.VoucherDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGIdentificationParamsDTO;
import com.isa.thinair.paymentbroker.api.util.TnxModeEnum;
import com.isa.thinair.platform.api.ServiceResponce;

public class AAPaymentBL {

	private static Log log = LogFactory.getLog(AAPaymentBL.class);

	private AAPaymentRQ aaPaymentRQ;

	public AAPaymentBL(AAPaymentRQ aaPaymentRQ) {
		this.aaPaymentRQ = aaPaymentRQ;
	}

	public AAPaymentRS makePayment() {

		AAPaymentRS aaPaymentRS = new AAPaymentRS();
		aaPaymentRS.setResponseAttributes(new AAResponseAttributes());
		aaPaymentRS.setHeaderInfo(aaPaymentRQ.getHeaderInfo());
		TrackInfoDTO trackInfo = AAUtils.getTrackInfo(aaPaymentRQ.getAaPos());

		try {
			List<TravelerFulfillment> travelerFulfillments = aaPaymentRQ.getFulfillment();
			ReservationContactInfo contactInfo = AAServicesTransformUtil.transformToAAContactInfo(aaPaymentRQ.getContactInfo(),
					null, AAUtils.isMarketingCarrier(aaPaymentRQ.getAaPos().getMarketingAirLineCode()));
			if (travelerFulfillments.size() == 0)
				throw new ModuleException("aaservices.fulfillments.shouldbe.filled");

			PaymentAssembler ipaymentPassenger = new PaymentAssembler();
			for (TravelerFulfillment travelerFulfillment : travelerFulfillments) {
				for (PaymentDetails paymentDetails : travelerFulfillment.getPaymentDetails()) {
					if (paymentDetails.getTravelerCreditPayment() != null) {
						TravelerCreditPayment travelerCreditPayment = paymentDetails.getTravelerCreditPayment();
						// This means a credit payment. First need to CF credit on residing carrier, then pass the BF
						// pax paymetns back to lcc.
						// Those credit BF payments need to be linked with pax for recording
						ipaymentPassenger.addCreditPayment(paymentDetails.getPaymentSummary().getPaymentAmount(),
								travelerCreditPayment.getDebitTravelerRefNumber(), paymentDetails.getPaymentSummary()
										.getPaymentCarrier(), null, paymentDetails.getUniqueTnxId(), travelerFulfillment
										.getPaxSequence(), travelerCreditPayment.getEDate(), travelerCreditPayment.getBasePnr(),
								null, travelerCreditPayment.getUtilizingPnr(), null);
					} else if (paymentDetails.getDirectBill() != null) {

						// PaymentUtil.getOnAccPayCurrencyDTO(selectedCurrencyCode, loggedInAgencyCode,
						// agentCodeForOnAccPayment, paymentTimestamp)
						PayCurrencyDTO payCurrencyDTO = null; // FIXME for pay currency code
						ipaymentPassenger.addAgentCreditPayment(paymentDetails.getDirectBill().getAgentCode(), paymentDetails
								.getPaymentSummary().getPaymentAmount(), paymentDetails.getPaymentSummary().getPaymentCarrier(),
								payCurrencyDTO, paymentDetails.getUniqueTnxId(), paymentDetails.getPaymentSummary()
										.getPaymentMethod(), null);
						// This is an OA payment
					} else if (paymentDetails.isCash()) {
						throw new ModuleException("aaservices.cash.payment.notsupported.through.lcc");
						// This is a cash payment. Nothing to do. Actually this block should not execute. This will be
						// filtered in LCC itself
					} else if (paymentDetails.getPaymentCard() != null) {
						// This is a credit card payment
						CommonCreditCardPayment cardPayment = paymentDetails.getPaymentCard();

						// PayCurrencyDTO payCurrencyDTO =
						// PaymentUtil.getPaymentCurrencyForCardPay(cardPayment.getPaymentCurrencyCode(),
						// cardPayment.getIpgId(), new Date());
						PayCurrencyDTO payCurrencyDTO = null; // FIXME for pay currency code

						ipaymentPassenger.addCardPayment(PaymentType.getPaymentType(cardPayment.getCardType()), cardPayment
								.getEDate(), cardPayment.getNo(), cardPayment.getName(), cardPayment.getAddress(), cardPayment
								.getSecurityCode(), paymentDetails.getPaymentSummary().getPaymentAmount(), SalesChannelsUtil
								.isAppIndicatorWebOrMobile(cardPayment.getAppIndicator())
								? AppIndicatorEnum.APP_IBE
								: AppIndicatorEnum.APP_XBE, cardPayment.getTnxMode().intValue() == TnxModeEnum.MAIL_TP_ORDER
								.getCode() ? TnxModeEnum.MAIL_TP_ORDER : TnxModeEnum.SECURE_3D,
								(cardPayment.getOldCCRecordId() != null) ? -1 : cardPayment.getOldCCRecordId(),
								new IPGIdentificationParamsDTO(cardPayment.getIpgId(), cardPayment.getPaymentCurrencyCode()),
								paymentDetails.getPaymentSummary().getPaymentCarrier(), payCurrencyDTO, paymentDetails
										.getUniqueTnxId(), cardPayment.getAuthorizationId(), cardPayment.getPaymentBrokerId(),
								null);
					} else if (paymentDetails.getVoucherPayment() != null) {
						PayCurrencyDTO payCurrencyDTO = null;
						VoucherDTO voucherDTO = new VoucherDTO();
						voucherDTO.setVoucherId(paymentDetails.getVoucherPayment().getVoucherID());
						voucherDTO.setAmountInBase(paymentDetails.getVoucherPayment().getVoucherAmount().toString());
						voucherDTO.setRedeemdAmount(paymentDetails.getVoucherPayment().getVoucherRedeemAmount().toString());
						ipaymentPassenger.addVoucherPayment(voucherDTO, paymentDetails.getPaymentSummary().getPaymentAmount(),
								paymentDetails.getPaymentSummary().getPaymentCarrier(), payCurrencyDTO,
								paymentDetails.getUniqueTnxId(), null);
					}
				}
			}
			// Note ; temp payment id map is set to null. This wont be an issue till we are not using operating carrier
			// payment gateways
			ServiceResponce response = AAServicesModuleUtils.getReservationBD().makePayment(trackInfo, ipaymentPassenger,
					aaPaymentRQ.getPnr(), contactInfo, new ArrayList<Integer>());

			Map<Integer, Collection<TnxCreditPayment>> paxCreditPayments = (Map<Integer, Collection<TnxCreditPayment>>) response
					.getResponseParam(CommandParamNames.PAX_CREDIT_PAYMENTS);
			Collection<PaymentInfo> paymentInfos = (Collection<PaymentInfo>) response
					.getResponseParam(CommandParamNames.PAYMENT_INFO);

			// Since we return form other module we need updated list.
			ipaymentPassenger.getPayments().clear();
			ipaymentPassenger.getPayments().addAll(paymentInfos);

			for (Integer paxSequence : paxCreditPayments.keySet()) {
				TravelerFulfillment tempTravelerFulfillment = null;
				for (TravelerFulfillment travelerFulfillment : travelerFulfillments) {
					if (travelerFulfillment.getPaxSequence() == paxSequence.intValue()) {
						tempTravelerFulfillment = travelerFulfillment;
						break;
					}
				}
				// tempTravelerFulfillment should not be null here.
				tempTravelerFulfillment.getPaymentDetails().clear();
				tempTravelerFulfillment.getPaymentDetails().addAll(TransformerUtil.transform(paxCreditPayments.get(paxSequence)));
				aaPaymentRS.getTravelerFulfillments().add(tempTravelerFulfillment);
			}
			aaPaymentRS.getResponseAttributes().setSuccess(new AASuccess());
		} catch (Exception e) {
			log.error("makePayment(AAPaymentRQ) failed.", e);
			AAExceptionUtil.addAAErrrors(aaPaymentRS.getResponseAttributes().getErrors(), e);
		}
		return aaPaymentRS;
	}

	public AAPaymentRS reversePayment() {
		AAPaymentRS aaPaymentRS = new AAPaymentRS();
		aaPaymentRS.setResponseAttributes(new AAResponseAttributes());
		TrackInfoDTO trackInfo = AAUtils.getTrackInfo(aaPaymentRQ.getAaPos());

		try {
			List<TravelerFulfillment> travelerFulfillments = aaPaymentRQ.getFulfillment();
			ReservationContactInfo contactInfo = AAServicesTransformUtil.transformToAAContactInfo(aaPaymentRQ.getContactInfo(),
					null, AAUtils.isMarketingCarrier(aaPaymentRQ.getAaPos().getMarketingAirLineCode()));
			PaymentAssembler ipaymentPassenger = new PaymentAssembler();
			if (travelerFulfillments.size() == 0)
				throw new ModuleException("aaservices.fulfillments.shouldbe.filled");

			for (TravelerFulfillment travelerFulfillment : travelerFulfillments) {
				for (PaymentDetails paymentDetails : travelerFulfillment.getPaymentDetails()) {
					if (paymentDetails.getTravelerCreditPayment() != null) {
						// This means a credit payment. First need to CF credit on residing carrier, then pass the BF
						// pax paymetns back to lcc.
						// Those credit BF payments need to be linked with pax for recording
						TravelerCreditPayment travelerCreditPayment = paymentDetails.getTravelerCreditPayment();
						ipaymentPassenger.addCreditPayment(travelerCreditPayment.getCarrierBaseAmount(), travelerCreditPayment
								.getDebitTravelerRefNumber(), paymentDetails.getPaymentSummary().getPaymentCarrier(), null,
								paymentDetails.getUniqueTnxId(), travelerFulfillment.getPaxSequence(), travelerCreditPayment
										.getEDate(), travelerCreditPayment.getBasePnr(), travelerCreditPayment.getPaxCreditId(),
								travelerCreditPayment.getUtilizingPnr(), null);
					} else if (paymentDetails.getDirectBill() != null) {
						// FIXME for paycurrency

						ipaymentPassenger.addAgentCreditPayment(paymentDetails.getDirectBill().getAgentCode(), paymentDetails
								.getPaymentSummary().getPaymentAmount(), paymentDetails.getPaymentSummary().getPaymentCarrier(),
								null, paymentDetails.getUniqueTnxId(), null, null, null, null);
						// This is an OA payment
					} else if (paymentDetails.isCash()) {
						throw new ModuleException("aaservices.cash.payment.notsupported.through.lcc");
						// This is a cash payment. Nothing to do. Actually this block should not execute. This will be
						// filtered in LCC itself
					}
				}
			}
			AAServicesModuleUtils.getReservationBD().refundPayment(trackInfo, ipaymentPassenger, aaPaymentRQ.getPnr(),
					contactInfo, new ArrayList<Integer>());
			aaPaymentRS.getResponseAttributes().setSuccess(new AASuccess());
		} catch (Exception e) {
			log.error("reverse Payment(AAPaymentRQ) failed.", e);
			AAExceptionUtil.addAAErrrors(aaPaymentRS.getResponseAttributes().getErrors(), e);
		}
		return aaPaymentRS;
	}

	public static AAPaymentCheckRS checkReservationPayments(AAPaymentCheckRQ paymentCheckRQ) {
		AAPaymentCheckRS aaPaymentCheckRS = new AAPaymentCheckRS();
		aaPaymentCheckRS.setResponseAttributes(new AAResponseAttributes());

		try {
			boolean hasPayment = AAServicesModuleUtils.getReservationBD().hasPayments(paymentCheckRQ.getGroupPnr());
			aaPaymentCheckRS.setHasPayments(hasPayment);
			aaPaymentCheckRS.getResponseAttributes().setSuccess(new AASuccess());
		} catch (Exception e) {
			log.error("Payment check failed.", e);
			AAExceptionUtil.addAAErrrors(aaPaymentCheckRS.getResponseAttributes().getErrors(), e);
		}
		return aaPaymentCheckRS;
	}
}
