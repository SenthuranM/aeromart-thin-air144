package com.isa.thinair.aaservices.core.bl.dummycarrierfulfillment;

import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AirReservation;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AirTraveler;
import com.isa.connectivity.profiles.maxico.v1.common.dto.LCCAutoCancellationInfo;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAErrorCode;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAPOS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAPayCarrierFulfullmentRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAReservation;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.aaservices.core.bl.reservation.AAAutoCancellationBL;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.PaxTypeUtils;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.model.AutoCancellationInfo;
import com.isa.thinair.airreservation.api.model.IPassenger;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.assembler.PassengerAssembler;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Adds a infant to dummy reservation
 * 
 * @author malaka
 * 
 */
class AAFulfillAddInfant extends BaseMarketingCarrierFulfillment {

	private static Log log = LogFactory.getLog(AAFulfillAddInfant.class);

	AAFulfillAddInfant(AAReservation aaAirReservation, AAReservation newAAAirReservation, AAPOS aaPos) {
		super(aaAirReservation, newAAAirReservation, aaPos);
	}

	@Override
	public AAPayCarrierFulfullmentRS execute() throws WebservicesException, ModuleException {

		AAPayCarrierFulfullmentRS carrierFulfillment = new AAPayCarrierFulfullmentRS();
		carrierFulfillment.setResponseAttributes(new AAResponseAttributes());

		checkConstraints();

		try {

			if (log.isDebugEnabled()) {
				log.debug("AAFulfillAddInfant::begin for pnr [" + aaAirReservation.getGroupPnr() + "]");
			}

			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setGroupPNR(aaAirReservation.getGroupPnr());
			Reservation reservation = getCarrierReservation(pnrModesDTO);

			if (reservation == null) {
				throw new WebservicesException(AAErrorCode.ERR_25_MAXICO_REQUIRED_THE_RESERVATION_NO_LONGER_EXIST,
						"Reservation does not exits");
			}

			List<AirTraveler> airTravelers = aaAirReservation.getAirReservation().getTravelerInfo().getAirTraveler();

			LCCAutoCancellationInfo lccAutoCnxInfo = aaAirReservation.getAirReservation().getLccAutoCancellationInfo();
			Integer autoCnxId = null;
			if (lccAutoCnxInfo != null) {
				AutoCancellationInfo autoCancellationInfo = AAAutoCancellationBL.populateAutoCancellation(lccAutoCnxInfo);
				autoCancellationInfo = AAServicesModuleUtils.getReservationBD().saveAutoCancellationInfo(autoCancellationInfo,
						aaAirReservation.getGroupPnr(), null);
				autoCnxId = autoCancellationInfo.getAutoCancellationId();
			}

			Integer adultPaxSeq = null;
			Integer infantPaxSequence = null;
			AirTraveler infantTraveler = null;
			String foidNumber = null;
			Date foidExpiry = null;
			String foidIssuedCntry = null;
			String employeeId = null;
			Date dateOfJoin = null;
			String idCategory = null;

			IPassenger iPassenger = new PassengerAssembler(null);

			for (AirTraveler airTraveler : airTravelers) {
				if (airTraveler.isAccompaniedByInfant()) {
					adultPaxSeq = PaxTypeUtils.getPaxSeq(airTraveler.getTravelerRefNumber());
				} else {
					infantPaxSequence = getInfantPaxSequence(airTraveler.getTravelerRefNumber());
					infantTraveler = airTraveler;
				}
			}

			if (infantTraveler != null) {
				if (infantTraveler.getFormOfIdentification() != null) {
					foidNumber = infantTraveler.getFormOfIdentification().getFoidNumber();
					employeeId = infantTraveler.getFormOfIdentification().getEmployeeId();
				}
			}

			// Gets the nationality code from nationality ISO code
			Integer intnationality = null;
			if (infantTraveler.getNationalityCode() != null) {
				int nationalityCode = AAServicesModuleUtils.getCommonMasterBD()
						.getNationality(infantTraveler.getNationalityCode()).getNationalityCode();
				intnationality = new Integer(nationalityCode);
			}

			if (infantTraveler.getFormOfIdentification() != null) {
				foidExpiry = infantTraveler.getFormOfIdentification().getPsptExpiry() != null ? infantTraveler
						.getFormOfIdentification().getPsptExpiry().toGregorianCalendar().getTime() : null;
				foidIssuedCntry = infantTraveler.getFormOfIdentification().getPsptIssuedCntry();
				dateOfJoin = infantTraveler.getFormOfIdentification().getDateOfJoin() != null ? infantTraveler
						.getFormOfIdentification().getDateOfJoin().toGregorianCalendar().getTime() : null;
				idCategory = infantTraveler.getFormOfIdentification().getIdCategory();
			}

			// Add infant to PassengerAssembler
			iPassenger.addInfant(infantTraveler.getPersonName().getFirstName(), infantTraveler.getPersonName().getSurName(),
					null, infantTraveler.getBirthDate() != null
							? infantTraveler.getBirthDate().toGregorianCalendar().getTime()
							: null, intnationality, infantPaxSequence, adultPaxSeq, foidNumber, foidExpiry, foidIssuedCntry,
					employeeId, dateOfJoin, idCategory, null, infantTraveler.getPassengerCategory(), null, autoCnxId, null, "",
					"", "", "", null);

			ServiceResponce response = AAServicesModuleUtils.getPassengerBD().addInfantForDummyReservation(reservation,
					iPassenger);
			Long version = (Long) response.getResponseParam(CommandParamNames.VERSION);

			if (response.isSuccess()) {

				if (log.isDebugEnabled()) {
					log.debug("AAFulfillModifyContact::end");
				}

				carrierFulfillment.getResponseAttributes().setSuccess(new AASuccess());
				carrierFulfillment.setResVersion(version.toString());
			}

		} catch (ModuleException e) {
			log.error("dummy reservation AAFulfillAddInfant failed", e);
			throw new ModuleException("aaservices.paycarrier.fulfillment.failed", e);
		}

		return carrierFulfillment;
	}

	@Override
	public void checkConstraints() throws WebservicesException {
		AirReservation airReservation = aaAirReservation.getAirReservation();

		if (airReservation.getTravelerInfo() == null) {
			throw new WebservicesException(AAErrorCode.ERR_1_MAXICO_REQUIRED_INVALID_VALUE, "traveler info not found");
		}
	}

	/**
	 * Extract the infant sequence number from infant traveler ref number
	 * 
	 * @param travellerRefNum
	 * @return
	 */
	private Integer getInfantPaxSequence(String travellerRefNum) {
		if (travellerRefNum.indexOf("$") > 0) {
			return PaxTypeUtils.getPaxSeq(travellerRefNum);
		} else {
			String infSeq = travellerRefNum.substring(1, travellerRefNum.indexOf("/"));
			return new Integer(infSeq);
		}
	}
}
