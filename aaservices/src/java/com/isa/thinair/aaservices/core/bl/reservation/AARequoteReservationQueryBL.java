package com.isa.thinair.aaservices.core.bl.reservation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.BaseRequoteBalances;
import com.isa.connectivity.profiles.maxico.v1.common.dto.CarrierProductCodeWiseAmountDue;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ClientExternalChg;
import com.isa.connectivity.profiles.maxico.v1.common.dto.CurrencyCodeGroup;
import com.isa.connectivity.profiles.maxico.v1.common.dto.IntegerDecimalMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.IntegerExtChgMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaxRequoteReservationBalances;
import com.isa.connectivity.profiles.maxico.v1.common.dto.RequoteBalances;
import com.isa.connectivity.profiles.maxico.v1.common.dto.RequoteReservationBalances;
import com.isa.connectivity.profiles.maxico.v1.common.dto.StringDecimalMap;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAirRequoteBalanceRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAirRequoteBalanceRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.core.session.AASessionManager;
import com.isa.thinair.aaservices.core.transaction.TransactionalReservationProcess;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.TransformerUtil;
import com.isa.thinair.airproxy.api.dto.ReservationConstants;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.QuotedFareRebuildDTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPassengerSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientSegmentSummaryTO;
import com.isa.thinair.airproxy.api.model.reservation.core.RequoteBalanceQueryRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.ReservationBalanceTO;
import com.isa.thinair.airproxy.api.utils.assembler.PaxCountAssembler;
import com.isa.thinair.airreservation.api.dto.CustomChargesTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ServiceTaxExtChgDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * @author Jagath Kumara
 */
public class AARequoteReservationQueryBL {
	private static Log log = LogFactory.getLog(AARequoteReservationQueryBL.class);

	private AAAirRequoteBalanceRQ aaAirRequoteBalanceRQ;

	private TransactionalReservationProcess tnxResProcess;

	public AARequoteReservationQueryBL(AAAirRequoteBalanceRQ aaAirRequoteBalanceRQ, AASessionManager aaSessionManager) {
		this.aaAirRequoteBalanceRQ = aaAirRequoteBalanceRQ;
		String transactionId = PlatformUtiltiies.nullHandler((aaAirRequoteBalanceRQ != null && aaAirRequoteBalanceRQ
				.getHeaderInfo() != null) ? aaAirRequoteBalanceRQ.getHeaderInfo().getTransactionIdentifier() : null);

		if (transactionId.length() > 0 && aaSessionManager != null) {
			this.tnxResProcess = (TransactionalReservationProcess) aaSessionManager.getCurrentUserTransaction(transactionId);
		}
	}

	public AAAirRequoteBalanceRS execute() {
		AAAirRequoteBalanceRS aaAirRequoteBalanceRS = new AAAirRequoteBalanceRS();
		aaAirRequoteBalanceRS.setResponseAttributes(new AAResponseAttributes());

		RequoteBalanceQueryRQ balanceQueryRQ = new RequoteBalanceQueryRQ();
		composeBalanceQueryTO(balanceQueryRQ);
		List<FareSegChargeTO> fareSegChargeTOs = new ArrayList<FareSegChargeTO>();

		if (aaAirRequoteBalanceRQ.getSelectedFareTypes() != null && aaAirRequoteBalanceRQ.getSelectedFareTypes().size() > 0) {
			fareSegChargeTOs.addAll(tnxResProcess.getFSCSet(aaAirRequoteBalanceRQ.getSelectedFareTypes()));

			PaxCountAssembler paxCountAssembler = new PaxCountAssembler(tnxResProcess.getPaxCountAssembler().getAdultCount(),
					tnxResProcess.getPaxCountAssembler().getChildCount(), tnxResProcess.getPaxCountAssembler().getInfantCount());

			// TO DO add discounted fare details
			String bookingType = null;
			if (fareSegChargeTOs != null) {
				bookingType = tnxResProcess.getBookingType(fareSegChargeTOs.get(0));
			}

			QuotedFareRebuildDTO fareInfo = new QuotedFareRebuildDTO(fareSegChargeTOs, paxCountAssembler, null,
					bookingType);
			balanceQueryRQ.setFareInfo(fareInfo);
		}

		try {

			ReservationBalanceTO reservationBalanceTO = AAServicesModuleUtils.getAirproxyReservationBD()
					.getRequoteBalanceSummary(balanceQueryRQ, null);
			String alterationType = "";
			aaAirRequoteBalanceRS = composeAAAirRequoteBalanceRS(alterationType, reservationBalanceTO);
		} catch (Exception exp) {
			log.error("requoteBalanceQuery(aaAirRequoteBalanceRQ) failed.", exp);
			AAExceptionUtil.addAAErrrors(aaAirRequoteBalanceRS.getResponseAttributes().getErrors(), exp);
		}
		return aaAirRequoteBalanceRS;
	}

	private void composeBalanceQueryTO(RequoteBalanceQueryRQ balanceQueryTO) {
		balanceQueryTO.setGroupPnr(aaAirRequoteBalanceRQ.getGroupPnr());
		balanceQueryTO.setPnr(aaAirRequoteBalanceRQ.getGroupPnr());
		if (aaAirRequoteBalanceRQ.getExcludedSegFarePnrSegIds() != null
				&& aaAirRequoteBalanceRQ.getExcludedSegFarePnrSegIds().size() > 0) {
			balanceQueryTO.setExcludedSegFarePnrSegIds(aaAirRequoteBalanceRQ.getExcludedSegFarePnrSegIds());
		}
		balanceQueryTO.setFQWithinValidity(aaAirRequoteBalanceRQ.isFqWithinValidity());
		balanceQueryTO.setLastFareQuoteDate(aaAirRequoteBalanceRQ.getLastFareQuotedDate());
		balanceQueryTO.getRemovedSegmentIds().addAll(aaAirRequoteBalanceRQ.getRemovedSegIds());
		balanceQueryTO.setSameFlightModification(aaAirRequoteBalanceRQ.isSameFlightModification());
		balanceQueryTO.setVersion(aaAirRequoteBalanceRQ.getVersion());
		balanceQueryTO.setRemainingLoyaltyPoints(aaAirRequoteBalanceRQ.getRemainingLoyaltyPoints());

		balanceQueryTO.setOldFareIdByFltSegIdMap(TransformerUtil.transformIntegerIntegerMap(aaAirRequoteBalanceRQ
				.getOldFareIdFltSegIdMap()));
		balanceQueryTO.setOndFareTypeByFareIdMap(TransformerUtil.transformIntegerStringMap(aaAirRequoteBalanceRQ
				.getOndFareTypeFareIdMap()));

		if (aaAirRequoteBalanceRQ.getRequoteSegmentMap() != null && aaAirRequoteBalanceRQ.getRequoteSegmentMap().size() > 0) {
			balanceQueryTO.setRequoteSegmentMap(TransformerUtil.transformStringStringMap(aaAirRequoteBalanceRQ
					.getRequoteSegmentMap()));
		}
		balanceQueryTO.setPaxExtChgMap(TransformerUtil.composePerPaxExternalChargeMap(aaAirRequoteBalanceRQ.getPaxExtChgMap()));
		CustomChargesTO customChargesTO = TransformerUtil.transformCustomCharges(aaAirRequoteBalanceRQ.getChargeOverride(),
				ReservationConstants.AlterationType.ALT_REQUOTE);
		balanceQueryTO.setCustomCharges(customChargesTO);
		balanceQueryTO.setApplyModificationCharge(aaAirRequoteBalanceRQ.isCnxWithModCharge());
		balanceQueryTO.setNameChangedPaxMap(TransformerUtil.transformIntegerPaxNameDTO(aaAirRequoteBalanceRQ
				.getNameChangedPaxMap()));
		balanceQueryTO.setDuplicateNameCheck(aaAirRequoteBalanceRQ.isSkipDuplicateNameCheck());
	}

	private AAAirRequoteBalanceRS composeAAAirRequoteBalanceRS(String alterationType, ReservationBalanceTO reservationBalanceTO)
			throws ModuleException {

		AAAirRequoteBalanceRS aaAirRequoteBalanceRS = new AAAirRequoteBalanceRS();
		aaAirRequoteBalanceRS.setResponseAttributes(new AAResponseAttributes());

		RequoteReservationBalances requoteReservationBalances = prepareRequoteReservationBalances(alterationType,
				reservationBalanceTO);

		aaAirRequoteBalanceRS.setRequoteReservationBalances(requoteReservationBalances);
		aaAirRequoteBalanceRS.setGroupPnr(aaAirRequoteBalanceRQ.getGroupPnr());
		aaAirRequoteBalanceRS.getResponseAttributes().setSuccess(new AASuccess());

		return aaAirRequoteBalanceRS;
	}

	private RequoteReservationBalances prepareRequoteReservationBalances(String alterationType,
			ReservationBalanceTO reservationBalanceTO) {
		RequoteReservationBalances requoteReservationBalances = new RequoteReservationBalances();

		RequoteBalances requoteBalances = composeRequoteBalances(reservationBalanceTO);
		requoteReservationBalances.setRequoteBalances(requoteBalances);

		CurrencyCodeGroup currencyCodeGroup = new CurrencyCodeGroup();
		currencyCodeGroup.setCurrencyCode(AppSysParamsUtil.getBaseCurrency());
		currencyCodeGroup.setDecimalPlaces(2);

		requoteBalances.setCurrencyCodeGroup(currencyCodeGroup);

		for (LCCClientPassengerSummaryTO lccClientPassengerSummaryTO : reservationBalanceTO.getPassengerSummaryList()) {
			PaxRequoteReservationBalances paxRequoteReservationBalances = new PaxRequoteReservationBalances();

			paxRequoteReservationBalances.setPaxRefNumber(lccClientPassengerSummaryTO.getTravelerRefNumber());
			paxRequoteReservationBalances.setPaxName(lccClientPassengerSummaryTO.getPaxName());
			paxRequoteReservationBalances.setPassengerType(TransformerUtil.transformLCCPaxTypeCode(lccClientPassengerSummaryTO
					.getPaxType()));
			if (lccClientPassengerSummaryTO.hasInfant()) {
				paxRequoteReservationBalances.setPaxInfantName(lccClientPassengerSummaryTO.getInfantName());
				paxRequoteReservationBalances.setInfantRefNumber(lccClientPassengerSummaryTO.getAccompaniedTravellerRef());
			} else {
				paxRequoteReservationBalances.setPaxInfantName("");
				paxRequoteReservationBalances.setInfantRefNumber("");
			}

			RequoteBalances paxRequoteBalances = composePaxRequoteBalances(lccClientPassengerSummaryTO.getSegmentSummary());
			paxRequoteBalances.setTotalAmountDue(lccClientPassengerSummaryTO.getTotalAmountDue());
			paxRequoteBalances.setTotalCreditAmount(lccClientPassengerSummaryTO.getTotalCreditAmount());
			paxRequoteBalances.setTotalCnxCharge(lccClientPassengerSummaryTO.getTotalCnxCharge());
			paxRequoteBalances.setTotalModCharge(lccClientPassengerSummaryTO.getTotalModCharge());
			paxRequoteBalances.setTotalPrice(lccClientPassengerSummaryTO.getTotalPrice());
			paxRequoteBalances.setCurrencyCodeGroup(currencyCodeGroup);
			paxRequoteBalances.setTotalPaidAmount(lccClientPassengerSummaryTO.getTotalPaidAmount());

			if (lccClientPassengerSummaryTO.getCarrierProductCodeWiseAmountDue() != null
					&& lccClientPassengerSummaryTO.getCarrierProductCodeWiseAmountDue().size() > 0) {
				List<CarrierProductCodeWiseAmountDue> carrierProductCodeWiseAmountDueList = new ArrayList<CarrierProductCodeWiseAmountDue>();
				for (String carrierCode : lccClientPassengerSummaryTO.getCarrierProductCodeWiseAmountDue().keySet()) {
					List<StringDecimalMap> paxProduceDueAmounts = TransformerUtil
							.populateStringDecimalMap(lccClientPassengerSummaryTO.getCarrierProductCodeWiseAmountDue().get(
									carrierCode));
					CarrierProductCodeWiseAmountDue carrierProductCodeWiseAmountDue = new CarrierProductCodeWiseAmountDue();
					carrierProductCodeWiseAmountDue.setKey(carrierCode);
					carrierProductCodeWiseAmountDue.getValue().addAll(paxProduceDueAmounts);
					carrierProductCodeWiseAmountDueList.add(carrierProductCodeWiseAmountDue);
				}
				paxRequoteBalances.getCarrierProductCodeWiseAmountDue().addAll(carrierProductCodeWiseAmountDueList);
			}

			paxRequoteReservationBalances.setRequoteBalances(paxRequoteBalances);

			requoteReservationBalances.getPaxRequoteReservationBalances().add(paxRequoteReservationBalances);
		}

		requoteReservationBalances.setRequoteBalances(requoteBalances);

		return requoteReservationBalances;
	}

	private RequoteBalances composePaxRequoteBalances(LCCClientSegmentSummaryTO segmentSummary) {
		RequoteBalances paxRequoteBalances = new RequoteBalances();
		composeBaseRequoteBalances(paxRequoteBalances, segmentSummary);
		paxRequoteBalances.setTotalRefundableAmount(segmentSummary.getCurrentRefunds());
		paxRequoteBalances.setTotalNonRefundableAmount(segmentSummary.getCurrentNonRefunds());
		paxRequoteBalances.setInboundExtCharge(segmentSummary.getInBoundExternalCharge());
		paxRequoteBalances.setOutboundExtCharge(segmentSummary.getOutBoundExternalCharge());

		return paxRequoteBalances;
	}

	private RequoteBalances composeRequoteBalances(ReservationBalanceTO reservationBalanceTO) {
		RequoteBalances requoteBalances = new RequoteBalances();
		composeBaseRequoteBalances(requoteBalances, reservationBalanceTO.getSegmentSummary());
		requoteBalances.setTotalAmountDue(reservationBalanceTO.getTotalAmountDue());
		requoteBalances.setTotalCreditAmount(reservationBalanceTO.getTotalCreditAmount());
		requoteBalances.setTotalCnxCharge(reservationBalanceTO.getTotalCnxCharge());
		requoteBalances.setTotalModCharge(reservationBalanceTO.getTotalModCharge());
		requoteBalances.setTotalPrice(reservationBalanceTO.getTotalPrice());
		requoteBalances.setTotalPaidAmount(reservationBalanceTO.getTotalPaidAmount());
		requoteBalances.setTotalRefundableAmount(reservationBalanceTO.getSegmentSummary().getCurrentRefunds());
		requoteBalances.setTotalNonRefundableAmount(reservationBalanceTO.getSegmentSummary().getCurrentNonRefunds());

		List<IntegerDecimalMap> paxWiseDueAmountList = new ArrayList<IntegerDecimalMap>();
		for (Entry<Integer, BigDecimal> chargeEntry : reservationBalanceTO.getPaxWiseAdjustmentAmountMap().entrySet()) {
			IntegerDecimalMap chargeResultEntry = new IntegerDecimalMap();
			chargeResultEntry.setKey(chargeEntry.getKey());
			chargeResultEntry.setValue(chargeEntry.getValue());
			paxWiseDueAmountList.add(chargeResultEntry);
		}

		requoteBalances.getPaxWiseAdjustmentAmount().addAll(paxWiseDueAmountList);

		if (reservationBalanceTO.getPaxEffectiveTax() != null) {
			for (Entry<Integer, List<ExternalChgDTO>> paxEntry : reservationBalanceTO.getPaxEffectiveTax().entrySet()) {
				Integer paxSeq = paxEntry.getKey();
				List<ExternalChgDTO> externalChgDTOs = paxEntry.getValue();

				IntegerExtChgMap integerExtChgMap = new IntegerExtChgMap();
				integerExtChgMap.setKey(paxSeq);
				integerExtChgMap.getValue().addAll(transformExternalCharges(externalChgDTOs));
				requoteBalances.getPaxEffectiveTaxes().add(integerExtChgMap);
			}
		}

		return requoteBalances;
	}

	private void composeBaseRequoteBalances(RequoteBalances tmpRequoteBalances, LCCClientSegmentSummaryTO segmentSummary) {
		BaseRequoteBalances existingOndBalances = new BaseRequoteBalances();
		existingOndBalances.setFareAmount(segmentSummary.getCurrentFareAmount());
		existingOndBalances.setTaxAmount(segmentSummary.getCurrentTaxAmount());
		existingOndBalances.setSurAmount(segmentSummary.getCurrentSurchargeAmount());
		existingOndBalances.setAdjAmount(segmentSummary.getCurrentAdjAmount());
		existingOndBalances.setModAmount(segmentSummary.getCurrentModAmount());
		existingOndBalances.setCnxAmount(segmentSummary.getCurrentCnxAmount());
		existingOndBalances.setDiscountAmount(segmentSummary.getCurrentDiscount());
		existingOndBalances.setSeatAmount(segmentSummary.getCurrentSeatAmount());
		existingOndBalances.setMealAmount(segmentSummary.getCurrentMealAmount());
		existingOndBalances.setBaggageAmount(segmentSummary.getCurrentBaggageAmount());
		existingOndBalances.setInsuranceAmount(segmentSummary.getCurrentInsuranceAmount());
		existingOndBalances.setSsrAmount(segmentSummary.getCurrentSSRAmount());
		existingOndBalances.setPenaltyAmount(segmentSummary.getCurrentModificationPenatly());
		existingOndBalances.setExtraFeeAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
		existingOndBalances.setTotalPrice(segmentSummary.getCurrentTotalPrice());

		tmpRequoteBalances.setExistingOndBalances(existingOndBalances);

		BaseRequoteBalances requoteOndBalances = new BaseRequoteBalances();
		requoteOndBalances.setFareAmount(segmentSummary.getNewFareAmount());
		requoteOndBalances.setTaxAmount(segmentSummary.getNewTaxAmount());
		requoteOndBalances.setSurAmount(segmentSummary.getNewSurchargeAmount());
		requoteOndBalances.setAdjAmount(segmentSummary.getNewAdjAmount());
		requoteOndBalances.setModAmount(segmentSummary.getNewModAmount());
		requoteOndBalances.setCnxAmount(segmentSummary.getNewCnxAmount());
		requoteOndBalances.setDiscountAmount(segmentSummary.getNewDiscount());
		requoteOndBalances.setSeatAmount(segmentSummary.getNewSeatAmount());
		requoteOndBalances.setMealAmount(segmentSummary.getNewMealAmount());
		requoteOndBalances.setBaggageAmount(segmentSummary.getNewBaggageAmount());
		requoteOndBalances.setInsuranceAmount(segmentSummary.getNewInsuranceAmount());
		requoteOndBalances.setSsrAmount(segmentSummary.getNewSSRAmount());
		requoteOndBalances.setPenaltyAmount(segmentSummary.getModificationPenalty());
		requoteOndBalances.setExtraFeeAmount(segmentSummary.getNewExtraFeeAmount());
		requoteOndBalances.setTotalPrice(segmentSummary.getNewTotalPrice());

		tmpRequoteBalances.setRequoteOndBalances(requoteOndBalances);
	}

	private static List<ClientExternalChg> transformExternalCharges(List<ExternalChgDTO> aaExternalCharges) {
		List<ClientExternalChg> lccClientExternalChgs = new ArrayList<ClientExternalChg>();
		if (aaExternalCharges != null) {
			for (ExternalChgDTO externalChgDTO : aaExternalCharges) {
				ClientExternalChg clientExternalChg = new ClientExternalChg();
				clientExternalChg.setAmount(externalChgDTO.getAmount());
				clientExternalChg.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
				clientExternalChg.setCode(externalChgDTO.getChargeCode());
				clientExternalChg.setExternalCharges(externalChgDTO.getExternalChargesEnum().toString());
				if (externalChgDTO instanceof ServiceTaxExtChgDTO) {
					clientExternalChg.setFlightRefNumber(((ServiceTaxExtChgDTO) externalChgDTO).getFlightRefNumber());
				}
				lccClientExternalChgs.add(clientExternalChg);
			}
		}
		return lccClientExternalChgs;
	}

}
