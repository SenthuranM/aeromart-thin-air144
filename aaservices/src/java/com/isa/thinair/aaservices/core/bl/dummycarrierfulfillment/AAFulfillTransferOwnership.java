package com.isa.thinair.aaservices.core.bl.dummycarrierfulfillment;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AirReservation;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAErrorCode;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAPOS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAPayCarrierFulfullmentRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAReservation;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.AAUtils;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * Class to handle the transfer ownership for dummy reservations
 * 
 * @author malaka
 * 
 */
class AAFulfillTransferOwnership extends BaseMarketingCarrierFulfillment {

	private static Log log = LogFactory.getLog(AAFulfillTransferOwnership.class);

	AAFulfillTransferOwnership(AAReservation aaAirReservation, AAReservation newAAAirReservation, AAPOS aaPos) {
		super(aaAirReservation, newAAAirReservation, aaPos);
	}

	@Override
	public AAPayCarrierFulfullmentRS execute() throws WebservicesException, ModuleException {

		AAPayCarrierFulfullmentRS carrierFulfillment = new AAPayCarrierFulfullmentRS();
		carrierFulfillment.setResponseAttributes(new AAResponseAttributes());

		try {

			if (log.isDebugEnabled()) {
				log.debug("AAFulfillTransferOwnership::begin for pnr [" + aaAirReservation.getGroupPnr() + "]");
			}

			checkConstraints();
			TrackInfoDTO trackInfo = AAUtils.getTrackInfo(aaPos);
			String ownerAgentCode = aaAirReservation.getAirReservation().getAirReservationSummary().getAdminInfo()
					.getOwnerAgentCode();
			String groupPnr = aaAirReservation.getGroupPnr();
			// Update reservation contact info
			ServiceResponce transfer = AAServicesModuleUtils.getReservationBD().transferOwnerShipForDummyRes(groupPnr,
					ownerAgentCode, trackInfo);
			Long version = (Long) transfer.getResponseParam(CommandParamNames.VERSION);

			if (transfer.isSuccess()) {
				if (log.isDebugEnabled()) {
					log.debug("AAFulfillTransferOwnership::end");
				}
				carrierFulfillment.getResponseAttributes().setSuccess(new AASuccess());
				carrierFulfillment.setResVersion(version.toString());
			}

		} catch (ModuleException e) {
			log.error("dummy reservation AAFulfillTransferOwnership failed", e);
			throw new ModuleException("aaservices.paycarrier.fulfillment.failed", e);
		}

		return carrierFulfillment;
	}

	@Override
	public void checkConstraints() throws WebservicesException {
		AirReservation airReservation = aaAirReservation.getAirReservation();
		if (airReservation.getAirReservationSummary().getAdminInfo() == null) {
			throw new WebservicesException(AAErrorCode.ERR_1_MAXICO_REQUIRED_INVALID_VALUE, "Fares could not be located");
		}
	}
}
