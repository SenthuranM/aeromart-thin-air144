package com.isa.thinair.aaservices.core.bl.reservation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AirTraveler;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AirportServiceRequest;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingSystems;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ExpectedBookingStates;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.OriginDestinationOption;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PassengerType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SpecialReqDetails;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SpecialServiceRequest;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAirBookRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAirBookRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAErrorCode;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAReadRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.aaservices.core.session.AASessionManager;
import com.isa.thinair.aaservices.core.session.AAUserSession;
import com.isa.thinair.aaservices.core.transaction.TransactionalReservationProcess;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAExtCarrierFulfillmentUtil;
import com.isa.thinair.aaservices.core.util.AAReservationUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.AAServicesTransformUtil;
import com.isa.thinair.aaservices.core.util.AAUtils;
import com.isa.thinair.aaservices.core.util.PaxTypeUtils;
import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airinventory.api.util.TempBlockSeatUtil;
import com.isa.thinair.airmaster.api.dto.AirportServiceDTO;
import com.isa.thinair.airmaster.api.service.SsrBD;
import com.isa.thinair.airpricing.api.model.SSRCharge;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.ReservationAssemblerConvertUtil;
import com.isa.thinair.airreservation.api.dto.CustomChargesTO;
import com.isa.thinair.airreservation.api.dto.ReservationAdminInfoTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.IReservation;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationContactInfo;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxSegmentSSR;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.model.assembler.ReservationAssembler;
import com.isa.thinair.airreservation.api.model.assembler.SegmentSSRAssembler;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.CommandParamNames;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.dto.SSRInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SSRUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.gdsservices.api.util.GDSInternalCodes;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.webplatform.api.util.AddModifySurfaceSegmentValidations;

/**
 * @author Nilindra Fernando
 */
public class AACreateReservationBL {

	private static Log log = LogFactory.getLog(AACreateReservationBL.class);

	private final AAAirBookRQ aaAirBookRQ;

	private TransactionalReservationProcess tnxResProcess;

	public AACreateReservationBL(AAAirBookRQ aaAirBookRQ, AASessionManager aaSessionManager) {
		this.aaAirBookRQ = aaAirBookRQ;
		String transactionId = PlatformUtiltiies
				.nullHandler((aaAirBookRQ != null) ? aaAirBookRQ.getHeaderInfo().getTransactionIdentifier() : null);

		if (transactionId.length() > 0 && aaSessionManager != null) {
			if (log.isDebugEnabled()) {
				log.debug("Current request session id " + aaAirBookRQ.getAaPos().getUserSessionId());
			}
			this.tnxResProcess = aaSessionManager.getCurrentUserTransaction(transactionId);
		}
	}

	public AAAirBookRS execute() {
		AAAirBookRS aaAirBookRS = new AAAirBookRS();
		aaAirBookRS.setResponseAttributes(new AAResponseAttributes());

		try {
			checkConstraints();

			if (log.isDebugEnabled()) {
				log.debug("Going to create " + AppSysParamsUtil.getDefaultCarrierCode()
						+ " carrier booking for the reservation : " + aaAirBookRQ.getGroupPnr());
			}

			Object[] transformedBookRQInfo = new AAServicesTransformUtil.BookingRequest(aaAirBookRQ,
					tnxResProcess.getOndFareDTOListForReservation(aaAirBookRQ.getSelectedFareTypes())).transform();

			IReservation iReservation = (IReservation) transformedBookRQInfo[0];
			iReservation.setReasonForAllowBlPaxRes(aaAirBookRQ.getReasonForAllowBlPax());
			boolean onholdReservation = (Boolean) transformedBookRQInfo[2];
			ReservationContactInfo aaContactInfo = (ReservationContactInfo) transformedBookRQInfo[6];
			boolean isForceConfirmed = aaAirBookRQ.getExpectedBookingStates() == ExpectedBookingStates.FORCED_CONFIRMED;
			boolean isOnHoldBeforeConfirm = (Boolean) transformedBookRQInfo[7];

			iReservation.setOriginCountryOfCall(aaAirBookRQ.getOriginCountryOfCall());
			Collection<String> privilegeKeys = (Collection<String>) AASessionManager.getInstance()
					.getCurrUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);
			UserPrincipal userPrincipal = AASessionManager.getInstance().getCurrentUserPrincipal();
			Reservation dummyReservation = ((ReservationAssembler) iReservation).getDummyReservation();

			if (!onholdReservation) {
				for (Iterator<ReservationPax> resPaxIt = dummyReservation.getPassengers().iterator(); resPaxIt.hasNext();) {
					ReservationPax pax = resPaxIt.next();
					Collection<Integer> pnrPaxIdCol = new ArrayList<Integer>();
					pnrPaxIdCol.add(pax.getPnrPaxId());

					AAReservationUtil.authorizePayment(privilegeKeys, pax.getPayment(), userPrincipal.getAgentCode(),
							pnrPaxIdCol);
				}
			}

			ReservationBD reservationDelegate = AAServicesModuleUtils.getReservationBD();
			Collection<OndFareDTO> ondFareDto = (Collection<OndFareDTO>) transformedBookRQInfo[5];

			if (onholdReservation && !isOnHoldBeforeConfirm) {
				if (ondFareDto != null) {
					if (!AAReservationUtil.isHoldAllowed(ondFareDto)) {
						log.error("Hold not allowed. Need to be ticketed immediately");
						throw new WebservicesException(AAErrorCode.ERR_7_MAXICO_REQUIRED_RESERVATION_NOT_ELIGIBLE_FOR_ONHOLD,
								"Hold not allowed. Need to be ticketed immediately");
					}
				}
			}

			iReservation.addOndFareDTOs(ondFareDto);

			TrackInfoDTO trackInfo = AAUtils.getTrackInfo(aaAirBookRQ.getAaPos());

			extractExtSegsAndAmmendForReservation(iReservation, aaAirBookRQ, trackInfo, userPrincipal);

			Map<Integer, String> groundStationBySegmentMap = new LinkedHashMap<Integer, String>();
			Map<Integer, Integer> groundSementFlightByAirFlightMap = new LinkedHashMap<Integer, Integer>();

			// add reservation segments
			AddModifySurfaceSegmentValidations.mapStationsBySegments(aaAirBookRQ.getAirItinerary().getOriginDestinationOption(),
					groundStationBySegmentMap, groundSementFlightByAirFlightMap);
			Map<Integer, String> flightSegIdWiseBaggageOndGrpId = getFlightSegIdWiseBaggageOndGrpIds(aaAirBookRQ);
			ReservationAssemblerConvertUtil.populateOndSegments(iReservation, ondFareDto, groundStationBySegmentMap,
					groundSementFlightByAirFlightMap, 1, 1, 1, 1, flightSegIdWiseBaggageOndGrpId, null, null);

			// Set Marketing credentials and transfer if required
			this.setMarketingCredentials(iReservation, aaContactInfo);

			// adding SSR to the reservation
			this.updateReservationWithSSR(iReservation);

			Collection collBlockIDs = tnxResProcess.getBlockSeats(aaAirBookRQ.getSelectedFareTypes());

			if (TempBlockSeatUtil.isExpired(collBlockIDs)) {
				reservationDelegate.releaseBlockedSeats(collBlockIDs, true);
				collBlockIDs = null;
			}

			ServiceResponce serviceResponse = reservationDelegate.createAAServiceReservationMacro(iReservation, collBlockIDs,
					ondFareDto, isForceConfirmed, onholdReservation, aaAirBookRQ.isCapturePayments(),
					aaAirBookRQ.getLastUserNote().getNoteText(), trackInfo);
			String pnr = (String) serviceResponse.getResponseParam(CommandParamNames.PNR);
			Long pnrVersion = (Long) serviceResponse.getResponseParam(CommandParamNames.VERSION);

			if (log.isDebugEnabled()) {
				log.debug(AppSysParamsUtil.getDefaultCarrierCode() + " carrier booking is successfully"
						+ "created for the reservation : " + pnr);
			}

			AAReadRQ aaReadRQ = new AAReadRQ();
			aaReadRQ.setPnr(pnr);
			aaReadRQ.setHeaderInfo(AAUtils.populateHeaderInfo(aaAirBookRQ.getHeaderInfo().getTransactionIdentifier()));
			// Set the default values. TODO : need to verify and remove unwanted
			aaReadRQ.setLoadReservationOptions(AAReservationUtil.createLoadReservationOptions());

			AASearchReservationByPnrBL aaSearchReservationBL = new AASearchReservationByPnrBL();
			aaSearchReservationBL.setRequest(aaReadRQ);

			aaAirBookRS = aaSearchReservationBL.execute();

			// Why we need to cancel the reservation ? Because if we throw an error in aaAirBookRS, lcc can NOT identify
			// whether reservation is created or not. But at this point reservation is SURELY created but retrieval is
			// failed. You may be able to load the onhold reservation and do the balance payment. But you has to check
			// the manifest to confirm whether reservation is created or not. For the safe side we cancel the
			// reservation.

			if (aaAirBookRS.getResponseAttributes().getSuccess() == null) {
				// CustomChargesTO set to null to follow the default behavior
				reservationDelegate.cancelReservation(pnr, new CustomChargesTO(), pnrVersion, false, false, true, trackInfo,
						false, aaAirBookRQ.getLastUserNote().getNoteText(), GDSInternalCodes.GDSNotifyAction.NO_ACTION.getCode(), null);
				if (log.isDebugEnabled()) {
					log.debug("Successfully cancelled " + AppSysParamsUtil.getDefaultCarrierCode() + " carrier booking "
							+ " for the reservation : " + pnr);
				}
			} else {
				// Just do the logging part. Can introduce else if, but this is a much clean way
				if (log.isDebugEnabled()) {
					log.debug("Successfully loaded " + AppSysParamsUtil.getDefaultCarrierCode() + " carrier booking "
							+ " for the reservation : " + pnr);
				}
			}

		} catch (Exception e) {
			log.error("book(AAAirBookRQ) failed.", e);
			AAExceptionUtil.addAAErrrors(aaAirBookRS.getResponseAttributes().getErrors(), e);
		}

		return aaAirBookRS;
	}

	private Map<Integer, String> getFlightSegIdWiseBaggageOndGrpIds(AAAirBookRQ aaAirBookRQ) {

		Map<Integer, String> flightSegIdWiseBaggageOndGrpId = new HashMap<Integer, String>();
		List<OriginDestinationOption> originDestinationOptions = aaAirBookRQ.getAirItinerary().getOriginDestinationOption();

		if (originDestinationOptions != null && !originDestinationOptions.isEmpty()) {
			for (OriginDestinationOption originDestinationOptionType : originDestinationOptions) {
				for (FlightSegment bookFlightSegment : originDestinationOptionType.getFlightSegment()) {
					Integer extFlightSegRef = FlightRefNumberUtil
							.getSegmentIdFromFlightRPH(bookFlightSegment.getFlightRefNumber());
					flightSegIdWiseBaggageOndGrpId.put(extFlightSegRef, bookFlightSegment.getBaggageONDGroupId());
				}
			}
		}

		return flightSegIdWiseBaggageOndGrpId;
	}

	private void updateReservationWithSSR(IReservation iReservation) throws ModuleException {
		Reservation reservation = ((ReservationAssembler) iReservation).getDummyReservation();
		Set<ReservationPax> reservationPaxs = reservation.getPassengers();
		for (ReservationPax reservationPax : reservationPaxs) {
			SegmentSSRAssembler ssrAssembler = getSegmentSSRAssemblerForPAX(reservation, reservationPax.getPaxSequence(),
					aaAirBookRQ.getTravelerInfo().getSpecialReqDetails());
			iReservation.getPaxSegmentSSRDTOMaps().put(reservationPax.getPaxSequence(), ssrAssembler.getNewSegmentSSRDTOMap());
		}
	}

	private SegmentSSRAssembler getSegmentSSRAssemblerForPAX(Reservation reservation, Integer paxSequence,
			SpecialReqDetails specialReqDetails) throws ModuleException {

		SegmentSSRAssembler ssrAssembler = new SegmentSSRAssembler();
		SsrBD ssrBD = AAServicesModuleUtils.getSsrServiceBD();
		Collection<ReservationSegment> segments = reservation.getSegments();

		/*
		 * Add in-flight services to SegmentSSRAssembler
		 */
		for (SpecialServiceRequest specialServiceRequest : specialReqDetails.getSpecialServiceRequests()) {
			Integer segmentSequence = null;

			segmentBlock: for (ReservationSegment reservationSegment : segments) {
				Integer fltSegId = FlightRefNumberUtil.getSegmentIdFromFlightRPH(specialServiceRequest.getFlightRefNumber());
				if (fltSegId.equals(reservationSegment.getFlightSegId())) {
					segmentSequence = reservationSegment.getSegmentSeq();

					if (paxSequence.equals(getPaxSequence(specialServiceRequest.getTravelerRefNumber()))
							&& specialServiceRequest.getSsrCode() != null) {

						SSRInfoDTO ssr = SSRUtil.getSSRInfo(SSRUtil.getSSRId(specialServiceRequest.getSsrCode()));
						SSRCharge ssrCharge = AAServicesModuleUtils.getChargeBD().getSSRChargesBySSRId(ssr.getSSRId());

						ssrAssembler.addPaxSegmentSSR(segmentSequence, ssr.getSSRId(), specialServiceRequest.getText(), 0,
								ssrCharge.getAdultAmount(), ssr.getSSRCode(), ssr.getSSRDescription(), null, null,
								ReservationPaxSegmentSSR.APPLY_ON_SEGMENT, null, null, null, null);
					}
					break segmentBlock;
				}
			}
		}

		Map<String, List<AirportServiceDTO>> availableAirportServices = getAirportWiseAirportServiceMap(
				specialReqDetails.getAirportServiceRequests());

		/*
		 * Add airport services to SegmentSSRAssembler
		 */
		for (AirportServiceRequest airportServiceRequest : specialReqDetails.getAirportServiceRequests()) {
			Integer segmentSequence = null;

			segmentBlock: for (ReservationSegment reservationSegment : segments) {
				Integer fltSegId = FlightRefNumberUtil.getSegmentIdFromFlightRPH(airportServiceRequest.getFlightRefNumber());
				if (fltSegId.equals(reservationSegment.getFlightSegId())) {
					segmentSequence = reservationSegment.getSegmentSeq();

					if (paxSequence.equals(getPaxSequence(airportServiceRequest.getTravelerRefNumber()))
							&& airportServiceRequest.getSsrCode() != null) {

						SSRInfoDTO ssr = SSRUtil.getSSRInfo(SSRUtil.getSSRId(airportServiceRequest.getSsrCode()));

						BigDecimal charge = AccelAeroCalculator.getDefaultBigDecimalZero();
						if (!isAirportServiceOfferedFreeOfCharge(fltSegId, ssr.getSSRId())) {
							List<AirportServiceDTO> apsList = availableAirportServices.get(airportServiceRequest.getAirportCode()
									+ AirportServiceDTO.SEPERATOR + reservationSegment.getFlightSegId());
							charge = getApplicableAirportCharge(reservation, apsList, ssr.getSSRCode(), paxSequence);
						}

						ssrAssembler.addPaxSegmentSSR(segmentSequence, ssr.getSSRId(), null, 0, charge, ssr.getSSRCode(),
								ssr.getSSRDescription(), null, airportServiceRequest.getAirportCode(),
								airportServiceRequest.getApplyOn(), null, null, null, null);

					}
					break segmentBlock;
				}
			}
		}

		return ssrAssembler;
	}

	private boolean isAirportServiceOfferedFreeOfCharge(Integer fltSegId, Integer apsId) {
		if (fltSegId != null) {
			Map<String, BundledFareDTO> segmentBundledFare = tnxResProcess.getSegmentBundledFareMap();
			for (OriginDestinationOption ondOption : aaAirBookRQ.getAirItinerary().getOriginDestinationOption()) {
				List<FlightSegment> flightSegments = ondOption.getFlightSegment();
				if (!flightSegments.isEmpty()) {
					for (FlightSegment fltSeg : flightSegments) {
						if (fltSegId.equals(FlightRefNumberUtil.getSegmentIdFromFlightRPH(fltSeg.getFlightRefNumber()))) {
							String fltSegCode = fltSeg.getSegmentCode();
							if (segmentBundledFare != null && segmentBundledFare.containsKey(fltSegCode)) {
								BundledFareDTO bundledFareDTO = segmentBundledFare.get(fltSegCode);
								if (bundledFareDTO != null) {
									Set<Integer> freeServices = bundledFareDTO.getApplicableApsList();
									if (freeServices != null && freeServices.contains(apsId)) {
										return true;
									}
								}
							}
						}
					}
				}
			}
		}

		return false;
	}

	private BigDecimal getApplicableAirportCharge(Reservation reservation, List<AirportServiceDTO> airportServiceDTOs,
			String ssrCode, Integer paxSequence) {
		BigDecimal charge = AccelAeroCalculator.getDefaultBigDecimalZero();
		for (AirportServiceDTO airportServiceDTO : airportServiceDTOs) {
			if (airportServiceDTO.getSsrCode().equals(ssrCode)) {
				if (AirportServiceDTO.APPLICABILITY_TYPE_PAX.equals(airportServiceDTO.getApplicabilityType())) {
					String paxType = getPaxType(aaAirBookRQ.getTravelerInfo().getAirTraveler(), paxSequence);

					if (PaxTypeTO.PARENT.equals(paxType)) {
						charge = AccelAeroCalculator.add(new BigDecimal(airportServiceDTO.getAdultAmount()),
								new BigDecimal(airportServiceDTO.getInfantAmount()));
					} else if (PaxTypeTO.ADULT.equals(paxType)) {
						charge = new BigDecimal(airportServiceDTO.getAdultAmount());
					} else if (PaxTypeTO.CHILD.equals(paxType)) {
						charge = new BigDecimal(airportServiceDTO.getChildAmount());
					}

				} else if (AirportServiceDTO.APPLICABILITY_TYPE_RESERVATION.equals(airportServiceDTO.getApplicabilityType())) {
					charge = AccelAeroCalculator.divide(new BigDecimal(airportServiceDTO.getReservationAmount()),
							reservation.getTotalPaxAdultCount() + reservation.getTotalPaxChildCount());
				}
			}
		}

		return charge;
	}

	private Map<String, List<AirportServiceDTO>> getAirportWiseAirportServiceMap(List<AirportServiceRequest> apsRequests)
			throws ModuleException {
		Map<Integer, List<String>> fltSegAirportMap = new HashMap<Integer, List<String>>();
		for (AirportServiceRequest airportServiceRequest : apsRequests) {
			List<String> airportList = null;
			Integer fltRef = FlightRefNumberUtil.getSegmentIdFromFlightRPH(airportServiceRequest.getFlightRefNumber());
			if (fltSegAirportMap.get(fltRef) != null) {
				airportList = fltSegAirportMap.get(fltRef);
			} else {
				airportList = new ArrayList<String>();
				fltSegAirportMap.put(fltRef, airportList);
			}

			airportList.add(airportServiceRequest.getAirportCode());
		}

		return AAServicesModuleUtils.getSsrServiceBD().getAvailableAirportServices(fltSegAirportMap, false);
	}

	private String getPaxType(List<AirTraveler> airTravelerList, Integer paxSeq) {
		String paxType = null;

		for (AirTraveler airTraveler : airTravelerList) {
			if (paxSeq.intValue() == airTraveler.getTravelerSequence().intValue()) {
				if (PassengerType.ADT.equals(airTraveler.getPassengerType()) && airTraveler.isAccompaniedByInfant()) {
					paxType = PaxTypeTO.PARENT;
				} else {
					paxType = PaxTypeUtils.convertLCCPaxCodeToAAPaxCodes(airTraveler.getPassengerType());
				}
				break;
			}
		}

		return paxType;
	}

	private Integer getPaxSequence(String travelerRefNumber) {
		return PaxTypeUtils.getPaxSeq(travelerRefNumber);
	}

	private void setMarketingCredentials(IReservation iReservation, ReservationContactInfo aaContactInfo) {
		ReservationAdminInfoTO reservationAdminInfoTO = null;

		if (aaAirBookRQ.getAaPos().getMarketingBookingChannel() != null) {
			if (SalesChannelsUtil.isBookingSystemWebOrMobile(aaAirBookRQ.getAaPos().getMarketingBookingChannel())) {
				if (reservationAdminInfoTO == null) {
					reservationAdminInfoTO = new ReservationAdminInfoTO();
				}

				reservationAdminInfoTO.setOverrideOriginAgentCode(true);
				reservationAdminInfoTO.setOriginAgentCode(null);

				reservationAdminInfoTO.setOverrideOriginUserId(true);
				reservationAdminInfoTO.setOriginUserId(null);

				reservationAdminInfoTO.setOverrideOwnerAgentCode(true);
				reservationAdminInfoTO.setOwnerAgentCode(null);

				reservationAdminInfoTO.setOverrideOriginChannelId(true);

				reservationAdminInfoTO.setOverrideOwnerChannelId(true);

				if (aaAirBookRQ.getAaPos().getMarketingBookingChannel() == BookingSystems.AARES_IBE) {
					reservationAdminInfoTO.setOriginChannelId(ReservationInternalConstants.SalesChannel.WEB);
					reservationAdminInfoTO.setOwnerChannelId(ReservationInternalConstants.SalesChannel.WEB);
				} else if (aaAirBookRQ.getAaPos().getMarketingBookingChannel() == BookingSystems.AARES_IOS) {
					reservationAdminInfoTO.setOriginChannelId(ReservationInternalConstants.SalesChannel.IOS);
					reservationAdminInfoTO.setOwnerChannelId(ReservationInternalConstants.SalesChannel.IOS);
				} else if (aaAirBookRQ.getAaPos().getMarketingBookingChannel() == BookingSystems.AARES_ANDROID) {
					reservationAdminInfoTO.setOriginChannelId(ReservationInternalConstants.SalesChannel.ANDROID);
					reservationAdminInfoTO.setOwnerChannelId(ReservationInternalConstants.SalesChannel.ANDROID);
				}

			} else if (aaAirBookRQ.getAaPos().getMarketingBookingChannel() == BookingSystems.AARES_XBE) {
				// Setting the owner agent code
				String ownerAgentCode = BeanUtils.nullHandler(aaAirBookRQ.getAaPos().getMarketingAgentCode());
				String marketingAirLineCode = BeanUtils.nullHandler(aaAirBookRQ.getAaPos().getMarketingAirLineCode());

				if (ownerAgentCode.length() > 0 && marketingAirLineCode.equals(AppSysParamsUtil.getDefaultCarrierCode())) {
					String ownerUserId = BeanUtils.nullHandler(aaAirBookRQ.getAaPos().getMarketingUserId());

					if (reservationAdminInfoTO == null) {
						reservationAdminInfoTO = new ReservationAdminInfoTO();
					}

					reservationAdminInfoTO.setOverrideOriginAgentCode(true);
					reservationAdminInfoTO.setOriginAgentCode(ownerAgentCode);

					reservationAdminInfoTO.setOverrideOriginUserId(true);
					reservationAdminInfoTO.setOriginUserId(ownerUserId);

					reservationAdminInfoTO.setOverrideOwnerAgentCode(true);
					reservationAdminInfoTO.setOwnerAgentCode(ownerAgentCode);

					reservationAdminInfoTO.setOverrideOwnerChannelId(true);
					reservationAdminInfoTO.setOwnerChannelId(ReservationInternalConstants.SalesChannel.TRAVEL_AGENT);
				}
			}
		}

		// transfer ownership of the reservation if required
		if (!BeanUtils.nullHandler(aaAirBookRQ.getOwnerAgentCode()).isEmpty()) {
			String ownerAgentCode = aaAirBookRQ.getOwnerAgentCode();
			if (ownerAgentCode.contains("|")) {
				String[] agentCodeArray = ownerAgentCode.split("\\|");
				if (agentCodeArray.length == 2) {
					ownerAgentCode = agentCodeArray[1];
				} else {
					ownerAgentCode = null;
				}
			}

			if (reservationAdminInfoTO == null) {
				reservationAdminInfoTO = new ReservationAdminInfoTO();
			}

			reservationAdminInfoTO.setOriginAgentCode(BeanUtils.nullHandler(ownerAgentCode));
			reservationAdminInfoTO.setOwnerChannelId(ReservationInternalConstants.SalesChannel.TRAVEL_AGENT);
		}

		String originAgentCode = BeanUtils.nullHandler(aaAirBookRQ.getAaPos().getOriginAgentCode());
		String originUserId = BeanUtils.nullHandler(aaAirBookRQ.getAaPos().getOriginUserId());

		if (!originAgentCode.isEmpty() && !originAgentCode.equals(reservationAdminInfoTO.getOriginAgentCode())) {
			reservationAdminInfoTO.setOverrideOriginAgentCode(true);
			reservationAdminInfoTO.setOriginAgentCode(originAgentCode);
		}

		if (!originUserId.isEmpty() && !originUserId.equals(reservationAdminInfoTO.getOriginUserId())) {
			reservationAdminInfoTO.setOverrideOriginUserId(true);
			reservationAdminInfoTO.setOriginUserId(originUserId);
		}

		if (reservationAdminInfoTO != null) {
			iReservation.setReservationAdminInfoTO(reservationAdminInfoTO);
		}
	}

	private void checkConstraints() throws WebservicesException, ModuleException {

		if (tnxResProcess == null) {
			String transactionId = PlatformUtiltiies
					.nullHandler((aaAirBookRQ != null) ? aaAirBookRQ.getHeaderInfo().getTransactionIdentifier() : null);
			log.error(" TransactionalReservationProcess is null for the pnr : " + aaAirBookRQ.getGroupPnr() + ", txnId : "
					+ transactionId);
			throw new WebservicesException(AAErrorCode.ERR_8_MAXICO_REQUIRED_TRANSACTION_INVALID_OR_EXPIRED,
					"Transaction not found");
		}
		if (!tnxResProcess.hasFareType(aaAirBookRQ.getSelectedFareTypes())) {
			log.error(" Saved fare type in TransactionalReservationProcess is not same in book request for the pnr : "
					+ aaAirBookRQ.getGroupPnr());
			throw new WebservicesException(AAErrorCode.ERR_1_MAXICO_REQUIRED_INVALID_VALUE, "Fares could not be located");
		}
	}

	/**
	 * Extract the external segments and ammend to the reservation
	 * 
	 * @param iReservation
	 * @param aaairBookRQ
	 * @return
	 * @throws ModuleException
	 */
	private void extractExtSegsAndAmmendForReservation(IReservation iReservation, AAAirBookRQ aaairBookRQ,
			TrackInfoDTO trackInfoDTO, UserPrincipal userPrincipal) throws ModuleException {

		List<OriginDestinationOption> originDestinationOptions = aaairBookRQ.getAirItinerary().getOriginDestinationOption();
		AAExtCarrierFulfillmentUtil.injectCredentialInfomationToFlightSegment(originDestinationOptions, trackInfoDTO,
				userPrincipal);
		if ((aaairBookRQ.getAirItinerary() != null && originDestinationOptions != null && originDestinationOptions.size() > 0)) {
			iReservation.addExternalPnrSegments(AAExtCarrierFulfillmentUtil.syncExtSegments(null, originDestinationOptions));
		}
	}

}
