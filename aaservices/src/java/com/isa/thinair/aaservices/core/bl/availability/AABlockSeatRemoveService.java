package com.isa.thinair.aaservices.core.bl.availability;

import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.required.dto.AABlockSeatRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AABlockSeatRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.core.session.AASessionManager;
import com.isa.thinair.aaservices.core.transaction.TransactionalReservationProcess;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.webplatform.api.util.PrivilegesKeys;

public class AABlockSeatRemoveService {
	private static final Log log = LogFactory.getLog(AABlockSeatRemoveService.class);

	private TransactionalReservationProcess transactionalReservationProcess;

	private AABlockSeatRQ aaBlockSeatRQ;

	private AABlockSeatRS aaBlockSeatRS;

	public void setAABlockSeatRQ(AABlockSeatRQ aaBlockSeatRQ) {
		this.aaBlockSeatRQ = aaBlockSeatRQ;
	}

	public void setTransactionalReservationProcess(TransactionalReservationProcess transactionalReservationProcess) {
		this.transactionalReservationProcess = transactionalReservationProcess;
	}

	private void intializeAABlockSeatRS() {
		aaBlockSeatRS = new AABlockSeatRS();
		aaBlockSeatRS.setResponseAttributes(new AAResponseAttributes());
	}

	public AABlockSeatRS execute() {
		intializeAABlockSeatRS();

		try {
			// authentication for allow seat blocking
			if (!AASessionManager.getUserPrivileges().contains(PrivilegesKeys.MakeResPrivilegesKeys.ALLOW_SEATBLOCKING)) {
				throw new ModuleException("User doesn't have the seat blocking privilege");
			}

			if (transactionalReservationProcess == null) {
				throw new ModuleException("No Transactional Reservation Process found for " + aaBlockSeatRQ.getTransactionID());
			}

			UserPrincipal userPrincipal = AASessionManager.getInstance().getCurrentUserPrincipal();
			// already block seat removal from the session and inventory
			Collection alreadyBlockedSeats = transactionalReservationProcess.getBlockSeats();
			if (alreadyBlockedSeats != null) {
				AAServicesModuleUtils.getReservationBD().releaseBlockedSeats(alreadyBlockedSeats);
			}
			transactionalReservationProcess.removeBlockSeats();

		} catch (Exception ex) {
			if (transactionalReservationProcess != null) {
				transactionalReservationProcess.removeBlockSeats();
			}
			log.error("Error occurred during block seat", ex);
			AAExceptionUtil.addAAErrrors(aaBlockSeatRS.getResponseAttributes().getErrors(), ex);
		} finally {
			int numOfErrors = aaBlockSeatRS.getResponseAttributes().getErrors().size();
			AASuccess successType = (numOfErrors == 0 ? new AASuccess() : null);
			aaBlockSeatRS.getResponseAttributes().setSuccess(successType);
		}

		return aaBlockSeatRS;
	}
}
