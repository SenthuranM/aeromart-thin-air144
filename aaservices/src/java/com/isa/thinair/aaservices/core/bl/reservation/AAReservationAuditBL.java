package com.isa.thinair.aaservices.core.bl.reservation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.UserNote;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResAuditRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResAuditReadRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.AAUtils;
import com.isa.thinair.auditor.api.dto.ReservationModificationDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

public class AAReservationAuditBL {

	private AAResAuditReadRQ aaResAuditReadRQ;
	private AAResAuditRS aaResAuditRS;
	private Collection<ReservationModificationDTO> reservationHistory;
	private Collection<ReservationModificationDTO> userNotesHistory;

	// Logger
	private static Log log = LogFactory.getLog(AAReservationAuditBL.class);

	public AAResAuditReadRQ getAaResAuditReadRQ() {
		return aaResAuditReadRQ;
	}

	public void setAaResAuditReadRQ(AAResAuditReadRQ aaResAuditReadRQ) {
		this.aaResAuditReadRQ = aaResAuditReadRQ;
	}

	/**
	 * Initializes the objects that will be used by the Response object
	 */
	private void initializeAAResAuditRS() {
		aaResAuditRS = new AAResAuditRS();
		aaResAuditRS.setResponseAttributes(new AAResponseAttributes());
	}

	/**
	 * This method will return the reservation history for the airline itself.
	 * 
	 * @return AAResAuditRS
	 * @throws WebservicesException
	 * @throws ModuleException
	 */
	public AAResAuditRS execute() {
		initializeAAResAuditRS();
		String pnr = getAaResAuditReadRQ().getPnr();
		if (log.isDebugEnabled()) {
			log.debug("AAServices for Reservation Audit has been called by LCC, PNR = [" + pnr + "]");
		}
		try {
			if (getAaResAuditReadRQ().isLoadHistory()) {
				reservationHistory = AAServicesModuleUtils.getAuditorServiceBD().getHistory(pnr);
				List<UserNote> reservationHistoryList = new ArrayList<UserNote>();
				populateReservationHistory(reservationHistoryList);
				aaResAuditRS.setPnr(pnr);
				aaResAuditRS.getReservationHistory().addAll(reservationHistoryList);
				// Populate HeaderInfo
				aaResAuditRS.setHeaderInfo(AAUtils.populateHeaderInfo(null));

			} else if (getAaResAuditReadRQ().isLoadUserNotes()) {
				userNotesHistory = AAServicesModuleUtils.getAuditorServiceBD().getUserNotes(pnr,
						getAaResAuditReadRQ().isClassifyUN(), aaResAuditReadRQ.getAaPos().getMarketingAgentCode());
				aaResAuditRS.setPnr(pnr);
				List<UserNote> userNotesHistoryList = new ArrayList<UserNote>();
				populateReservationNotes(userNotesHistoryList);
				aaResAuditRS.getReservationNotes().addAll(userNotesHistoryList);
				// Populate HeaderInfo
				aaResAuditRS.setHeaderInfo(AAUtils.populateHeaderInfo(null));
			}
		} catch (Exception e) {
			log.error(e);
			AAExceptionUtil.addAAErrrors(aaResAuditRS.getResponseAttributes().getErrors(), e);
		} finally {
			/*
			 * Add Success. Success is an initialized empty object to signify that all is OK. If there are errors (from
			 * the executed previously code) then we return a null object, signifying that all is not OK. [note warnings
			 * are not considered errors]
			 */
			int numOfErrors = aaResAuditRS.getResponseAttributes().getErrors().size();
			AASuccess successType = (numOfErrors == 0 ? new AASuccess() : null);
			aaResAuditRS.getResponseAttributes().setSuccess(successType);
		}
		return aaResAuditRS;
	}

	private void populateReservationHistory(List<UserNote> reservationHistory) {
		for (ReservationModificationDTO userNoteDTO : this.reservationHistory) {
			UserNote userNote = new UserNote();
			userNote.setTimestamp(userNoteDTO.getZuluModificationDate());
			userNote.setNoteText(userNoteDTO.getUserNote());
			userNote.setSystemText(userNoteDTO.getDescription());
			userNote.setAction(userNoteDTO.getModificationTypeDesc());
			userNote.setUsername(userNoteDTO.getUserId());
			userNote.getCarrierCode().add(AppSysParamsUtil.getDefaultCarrierCode());
			reservationHistory.add(userNote);
		}

	}

	private void populateReservationNotes(List<UserNote> reservationNotes) {

		for (ReservationModificationDTO userNoteDTO : userNotesHistory) {
			UserNote userNote = new UserNote();
			userNote.setTimestamp(userNoteDTO.getZuluModificationDate());
			userNote.setNoteText(userNoteDTO.getUserNote());
			userNote.setSystemText(userNoteDTO.getDescription());
			userNote.setAction(userNoteDTO.getModificationTypeDesc());
			userNote.setUsername(userNoteDTO.getUserId());
			userNote.getCarrierCode().add(AppSysParamsUtil.getDefaultCarrierCode());
			reservationNotes.add(userNote);
		}
	}

}
