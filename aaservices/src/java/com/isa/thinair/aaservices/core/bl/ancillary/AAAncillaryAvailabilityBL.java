package com.isa.thinair.aaservices.core.bl.ancillary;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AncillaryAvailability;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AncillaryType;
import com.isa.connectivity.profiles.maxico.v1.common.dto.AppIndicator;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingFlightSegment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentAncillaryAvailabilityStatus;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ReservationBaggageParams;
import com.isa.connectivity.profiles.maxico.v1.common.dto.StringStringMap;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAnciTaxApplicabilityRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAnciTaxApplicabilityRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAncillaryAvailabilityRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAncillaryAvailabilityRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAError;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.aaservices.core.session.AASessionManager;
import com.isa.thinair.aaservices.core.session.AAUserSession;
import com.isa.thinair.aaservices.core.util.AAReservationUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.AAUtils;
import com.isa.thinair.aaservices.core.util.AncillaryUtil;
import com.isa.thinair.airinventory.api.dto.ClassOfServiceDTO;
import com.isa.thinair.airinventory.api.dto.baggage.BaggageRq;
import com.isa.thinair.airinventory.api.dto.baggage.FlightBaggageDTO;
import com.isa.thinair.airinventory.api.dto.meal.FlightMealDTO;
import com.isa.thinair.airinventory.api.dto.seatmap.FlightSeatsDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.util.AirinventoryCustomConstants.ChargeCodes;
import com.isa.thinair.airmaster.api.dto.AirportServiceDTO;
import com.isa.thinair.airmaster.api.dto.AirportServiceKeyTO;
import com.isa.thinair.airmaster.api.dto.SpecialServiceRequestDTO;
import com.isa.thinair.airmaster.api.model.SSRCategory;
import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.airmaster.api.service.SsrBD;
import com.isa.thinair.airproxy.api.model.reservation.availability.AnciAvailabilityRS;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.IFlightSegment;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.airproxy.api.utils.AncillaryCommonUtil;
import com.isa.thinair.airproxy.api.utils.FlightRefNumberUtil;
import com.isa.thinair.airproxy.api.utils.PrivilegesKeys.AlterResPrivilegesKeys;
import com.isa.thinair.airproxy.api.utils.PrivilegesKeys.MakeResPrivilegesKeys;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.utils.BaggageTimeWatcher;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.dto.LogicalCabinClassDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.webplatform.api.util.AuthorizationUtil;
import com.isa.thinair.webplatform.api.v2.util.DateUtil;

public class AAAncillaryAvailabilityBL {

	private AAAncillaryAvailabilityRQ aaAncillaryAvailabilityRQ;

	private Log log = LogFactory.getLog(AAAncillaryAvailabilityBL.class);

	public void setAAAncillaryAvailabilityRQ(AAAncillaryAvailabilityRQ aaAncillaryAvailabilityRQ) {
		this.aaAncillaryAvailabilityRQ = aaAncillaryAvailabilityRQ;
	}

	public AAAncillaryAvailabilityRS execute() {

		AAAncillaryAvailabilityRS aaAncillaryAvailabilityRS = new AAAncillaryAvailabilityRS();
		aaAncillaryAvailabilityRS.setResponseAttributes(new AAResponseAttributes());
		aaAncillaryAvailabilityRS.getResponseAttributes().setSuccess(new AASuccess());

		Map<String, LogicalCabinClassDTO> cachedLogicalCCMap = CommonsServices.getGlobalConfig()
				.getAvailableLogicalCCMap();

		try {

			if (aaAncillaryAvailabilityRQ != null && aaAncillaryAvailabilityRQ.getFlightSegment() != null
					&& aaAncillaryAvailabilityRQ.getFlightSegment().size() > 0) {
				for (BookingFlightSegment flightSegment : aaAncillaryAvailabilityRQ.getFlightSegment()) {
					flightSegment.setSurfaceSegment(
							AirproxyModuleUtils.getAirportBD().isBusSegment(flightSegment.getSegmentCode()));
				}
			}

			boolean isBaggageAnciExists = isBaggageAnciExists(aaAncillaryAvailabilityRQ.getAncillaryType());

			if (!isBaggageAnciExists) {
				processForNonBaggageAnci(aaAncillaryAvailabilityRS, cachedLogicalCCMap);
			} else {
				processBaggageAnci(aaAncillaryAvailabilityRS, cachedLogicalCCMap);
			}

			if (AppIndicator.W.equals(aaAncillaryAvailabilityRQ.getAppIndicator())) {
				aaAncillaryAvailabilityRS
						.setApplyPenaltyForModification(!AppSysParamsUtil.isRefundIBEAnciModifyCredit());
			}

		} catch (Exception e) {
			log.error("ERROR in AA Anci Availability Search", e);
			AAError aaError = new AAError();
			aaError.setDescription(e.getMessage());
			aaAncillaryAvailabilityRS.getResponseAttributes().getErrors().add(aaError);
			aaAncillaryAvailabilityRS.getResponseAttributes().setSuccess(null);
		}
		return aaAncillaryAvailabilityRS;
	}

	public AAAnciTaxApplicabilityRS getAnciTaxApplicability(AAAnciTaxApplicabilityRQ aaAnciTaxApplicabilityRQ) {
		AAAnciTaxApplicabilityRS aaAnciTaxApplicabilityRS = new AAAnciTaxApplicabilityRS();
		aaAnciTaxApplicabilityRS.setResponseAttributes(new AAResponseAttributes());
		aaAnciTaxApplicabilityRS.getResponseAttributes().setSuccess(new AASuccess());

		try {
			EXTERNAL_CHARGES extChg = AAUtils.getExternalChargeType(aaAnciTaxApplicabilityRQ.getExternalCharge());

			AnciAvailabilityRS anciAvailabilityRS = AAServicesModuleUtils.getAirproxySearchAndQuoteBD()
					.getTaxApplicabilityForAncillaries(AppSysParamsUtil.getDefaultCarrierCode(),
							aaAnciTaxApplicabilityRQ.getSegmentCode(), extChg, null);

			aaAnciTaxApplicabilityRS.setJnTaxApplicable(anciAvailabilityRS.isJnTaxApplicable());
			aaAnciTaxApplicabilityRS.setTaxRatio(anciAvailabilityRS.getTaxRatio());

		} catch (Exception e) {
			log.error("ERROR in AA Anci Tax Applicability", e);
			AAError aaError = new AAError();
			aaError.setDescription(e.getMessage());
			aaAnciTaxApplicabilityRS.getResponseAttributes().getErrors().add(aaError);
			aaAnciTaxApplicabilityRS.getResponseAttributes().setSuccess(null);
		}

		return aaAnciTaxApplicabilityRS;
	}

	private void processBaggageAnci(AAAncillaryAvailabilityRS aaAncillaryAvailabilityRS,
			Map<String, LogicalCabinClassDTO> cachedLogicalCCMap) throws ModuleException {

		if (!AppSysParamsUtil.isONDBaggaeEnabled()) {
			boolean isAvailable = true;
			for (BookingFlightSegment flightSegment : aaAncillaryAvailabilityRQ.getFlightSegment()) {
				if (AppSysParamsUtil.getDefaultCarrierCode().equals(flightSegment.getOperatingAirline())) {
					FlightSegmentAncillaryAvailabilityStatus flightSegmentAncillaryAvailabilityStatus = new FlightSegmentAncillaryAvailabilityStatus();
					flightSegmentAncillaryAvailabilityStatus.setFlightSegments(flightSegment);
					AncillaryAvailability ancillaryAvailability = null;
					if (flightSegment.getDepatureDateTimeZulu() != null && CalendarUtil.getCurrentSystemTimeInZulu()
							.compareTo(flightSegment.getDepatureDateTimeZulu()) >= 0) {
						isAvailable = false;
					} else {
						ancillaryAvailability = populateAncillaryAvailability(flightSegment, AncillaryType.BAGGAGE,
								aaAncillaryAvailabilityRQ.getAppIndicator());
					}

					isAvailable = isAvailable && ancillaryAvailability.isAvailability();

					flightSegmentAncillaryAvailabilityStatus.getAncillaryAvailability().add(ancillaryAvailability);

					aaAncillaryAvailabilityRS.getAncillaryAvailabilityStatus()
							.add(flightSegmentAncillaryAvailabilityStatus);

					checkPrivilegesAndBufferTimes(aaAncillaryAvailabilityRS, aaAncillaryAvailabilityRQ.isModifyAnci(),
							aaAncillaryAvailabilityRQ.getAppIndicator());
				} else {
					FlightSegmentAncillaryAvailabilityStatus flightSegmentAncillaryAvailabilityStatus = new FlightSegmentAncillaryAvailabilityStatus();
					flightSegmentAncillaryAvailabilityStatus.setFlightSegments(flightSegment);

					AncillaryAvailability ancillaryAvailability = new AncillaryAvailability();
					ancillaryAvailability.setAncillary(AncillaryType.BAGGAGE);
					if (flightSegment.getDepatureDateTimeZulu() != null && CalendarUtil.getCurrentSystemTimeInZulu()
							.compareTo(flightSegment.getDepatureDateTimeZulu()) >= 0) {
						ancillaryAvailability.setAvailability(false);
					} else {
						ancillaryAvailability.setAvailability(isAvailable);
					}

					flightSegmentAncillaryAvailabilityStatus.getAncillaryAvailability().add(ancillaryAvailability);

					aaAncillaryAvailabilityRS.getAncillaryAvailabilityStatus()
							.add(flightSegmentAncillaryAvailabilityStatus);
				}
			}
			// check OND baggage
		} else {
			getONDWiseBaggagesAvailability(aaAncillaryAvailabilityRS.getAncillaryAvailabilityStatus());
		}
	}

	private void processForNonBaggageAnci(AAAncillaryAvailabilityRS aaAncillaryAvailabilityRS,
			Map<String, LogicalCabinClassDTO> cachedLogicalCCMap) throws ModuleException {
		String departureSegCode = aaAncillaryAvailabilityRQ.getDepartureSegmentCode();
		for (BookingFlightSegment flightSegment : aaAncillaryAvailabilityRQ.getFlightSegment()) {
			FlightSegmentAncillaryAvailabilityStatus flightSegmentAncillaryAvailabilityStatus = new FlightSegmentAncillaryAvailabilityStatus();
			flightSegment.setSsrInventoryCheck(AppSysParamsUtil.isInventoryCheckForSSREnabled());

			String logicalCabinClass = flightSegment.getLogicalCabinClassCode();
			if (logicalCabinClass != null && !"".equals(logicalCabinClass)) {
				LogicalCabinClassDTO logicalCabinClassDTO = cachedLogicalCCMap.get(logicalCabinClass);
				flightSegment.setFreeSeatEnabled(logicalCabinClassDTO.isFreeSeatEnabled());
				flightSegment.setFreeMealEnabled(logicalCabinClassDTO.isAllowSingleMealOnly());
			}

			flightSegmentAncillaryAvailabilityStatus.setFlightSegments(flightSegment);

			for (AncillaryType ancillaryType : aaAncillaryAvailabilityRQ.getAncillaryType()) {
				if (ancillaryType.compareTo(AncillaryType.AIRPORT_SERVICE) == 0) {
					continue;
				}

				AncillaryAvailability ancillaryAvailability = populateAncillaryAvailability(flightSegment,
						ancillaryType, aaAncillaryAvailabilityRQ.getAppIndicator());

				flightSegmentAncillaryAvailabilityStatus.getAncillaryAvailability().add(ancillaryAvailability);
			}

			aaAncillaryAvailabilityRS.getAncillaryAvailabilityStatus().add(flightSegmentAncillaryAvailabilityStatus);

			checkPrivilegesAndBufferTimes(aaAncillaryAvailabilityRS, aaAncillaryAvailabilityRQ.isModifyAnci(),
					aaAncillaryAvailabilityRQ.getAppIndicator());
		}

		for (AncillaryType ancillaryType : aaAncillaryAvailabilityRQ.getAncillaryType()) {
			if (ancillaryType.equals(AncillaryType.AIRPORT_SERVICE)) {
				getSegmentWiseAirportServiceAvailablity(aaAncillaryAvailabilityRS.getAncillaryAvailabilityStatus());
			}
		}

		if (aaAncillaryAvailabilityRQ.isCalculateAnciJNTax() && AppSysParamsUtil.isJNTaxApplicableForAnci()) {
			boolean isJNTaxApplicableForAnci = AirproxyModuleUtils.getChargeBD()
					.isValidChargeExists(ChargeCodes.JN_ANCI_TAX, departureSegCode);
			aaAncillaryAvailabilityRS.setJnTaxApplicable(isJNTaxApplicableForAnci);
			if (isJNTaxApplicableForAnci) {
				Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new ArrayList<EXTERNAL_CHARGES>();
				colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.JN_ANCI);

				ExternalChgDTO externalChgDTO = BeanUtils.getFirstElement(AAServicesModuleUtils.getReservationBD()
						.getQuotedExternalCharges(colEXTERNAL_CHARGES, null, null).values());
				aaAncillaryAvailabilityRS.setTaxRatio(externalChgDTO.getRatioValue());
			}
		}
	}

	private boolean isBaggageAnciExists(List<AncillaryType> ancillaryTypes) throws ModuleException {
		boolean isBaggageExists = false;
		boolean isOtherAnciExists = false;

		for (AncillaryType ancillaryType : ancillaryTypes) {
			if (AncillaryType.BAGGAGE == ancillaryType) {
				isBaggageExists = true;
			} else {
				isOtherAnciExists = true;
			}
		}

		if (isBaggageExists && isOtherAnciExists) {
			throw new ModuleException("aaservices.baggage.anci.should.be.single.request");
		}

		return isBaggageExists;
	}

	private void getONDWiseBaggagesAvailability(
			List<FlightSegmentAncillaryAvailabilityStatus> ancillaryAvailabilityStatusList) throws ModuleException {

		List<BookingFlightSegment> flightSegments = aaAncillaryAvailabilityRQ.getFlightSegment();
		List<FlightSegmentTO> flightSegmentTOs = AncillaryUtil.getFlightSegmentTOs(flightSegments);
		Map<String, String> bookingClassMapping = new HashMap<String, String>();
		Map<String, String> classesOfServiceMapping = new HashMap<String, String>();
		BaggageRq baggageRq = new BaggageRq();
		boolean isEnabled = true;

		ReservationBaggageParams baggageParams = aaAncillaryAvailabilityRQ.getReservationBaggageParams();
		String salesChannel = AAUtils.getSalesChannelFromPOS(aaAncillaryAvailabilityRQ.getAaPos());
		baggageParams.setSalesChannel(Integer.valueOf(salesChannel));

		for (StringStringMap entry : baggageParams.getBookingClass()) {
			bookingClassMapping.put(entry.getKey(), entry.getValue());
		}

		for (StringStringMap entry : baggageParams.getClassesOfService()) {
			classesOfServiceMapping.put(entry.getKey(), entry.getValue());
		}

		baggageRq.setFlightSegmentTOs(flightSegmentTOs);
		baggageRq.setAgent(baggageParams.getAgent());
		baggageRq.setBookingClasses(bookingClassMapping);
		baggageRq.setClassesOfService(classesOfServiceMapping);
		baggageRq.setRequote(aaAncillaryAvailabilityRQ.isRequote());
		baggageRq.setCheckCutoverTime(true);
		baggageRq.setSalesChannel(baggageParams.getSalesChannel());

		Map<Integer, List<FlightBaggageDTO>> baggageMap = AirproxyModuleUtils.getBaggageBusinessDelegate()
				.getBaggage(baggageRq);

		AncillaryCommonUtil.handleBufferTimes(baggageMap, flightSegmentTOs, aaAncillaryAvailabilityRQ.isModifyAnci(),
				baggageParams.isAllowUntilFinalCutOver());

		boolean isBaggageAvailable = AncillaryCommonUtil.isBaggageAvailable(baggageMap);

		for (BookingFlightSegment bookingFlightSegment : flightSegments) {
			FlightSegmentAncillaryAvailabilityStatus flightSegmentAncillaryAvailabilityStatus = new FlightSegmentAncillaryAvailabilityStatus();
			flightSegmentAncillaryAvailabilityStatus.setFlightSegments(bookingFlightSegment);

			FlightSegmentTO flightSegmentTO = getSelectedFlightSegmentTO(flightSegmentTOs,
					bookingFlightSegment.getFlightRefNumber());

			AncillaryAvailability ancillaryAvailability = new AncillaryAvailability();
			ancillaryAvailability.setAncillary(AncillaryType.BAGGAGE);
			ancillaryAvailability.setAvailability(isBaggageAvailable);
			ancillaryAvailability.setEnabled(isEnabled);

			if (flightSegmentTO != null) {
				if (flightSegmentTO.getDepartureDateTimeZulu() != null && CalendarUtil.getCurrentSystemTimeInZulu()
						.compareTo(flightSegmentTO.getDepartureDateTimeZulu()) >= 0) {
					ancillaryAvailability.setAvailability(false);
				} else {
					List<FlightBaggageDTO> baggageDTOs = baggageMap.get(flightSegmentTO.getFlightSegId());
					if (baggageDTOs == null || baggageDTOs.isEmpty()) {
						ancillaryAvailability.setAvailability(false);
					}
				}
				flightSegmentAncillaryAvailabilityStatus.getAncillaryAvailability().add(ancillaryAvailability);
			}

			ancillaryAvailabilityStatusList.add(flightSegmentAncillaryAvailabilityStatus);
		}
	}

	private void getSegmentWiseAirportServiceAvailablity(
			List<FlightSegmentAncillaryAvailabilityStatus> ancillaryAvailabilityStatusList) throws ModuleException {

		List<BookingFlightSegment> flightSegments = aaAncillaryAvailabilityRQ.getFlightSegment();
		List<FlightSegmentTO> flightSegmentTOs = AncillaryUtil.getFlightSegmentTOs(flightSegments);

		Map<AirportServiceKeyTO, FlightSegmentTO> airportWiseMap = AncillaryCommonUtil.populateAiportWiseFltSegments(
				flightSegmentTOs,
				AncillaryUtil.convertLCCListToMap(aaAncillaryAvailabilityRQ.getAirportServiceCountMap()), false);
		Map<AirportServiceKeyTO, List<AirportServiceDTO>> airportServiceMap = AAServicesModuleUtils.getSsrServiceBD()
				.getAvailableAirportServices(airportWiseMap,
						AncillaryUtil.getAppIndicator(aaAncillaryAvailabilityRQ.getAppIndicator().toString()),
						SSRCategory.ID_AIRPORT_SERVICE, null, null, false);

		for (FlightSegmentAncillaryAvailabilityStatus ancillaryAvailabilityStatus : ancillaryAvailabilityStatusList) {
			BookingFlightSegment bookingFltSeg = ancillaryAvailabilityStatus.getFlightSegments();

			AncillaryAvailability ancillaryAvailability = new AncillaryAvailability();
			ancillaryAvailability.setAncillary(AncillaryType.AIRPORT_SERVICE);

			if (AppSysParamsUtil.isAirportServicesEnabled()) {
				ancillaryAvailability.setEnabled(true);

				for (Entry<AirportServiceKeyTO, List<AirportServiceDTO>> apEntry : airportServiceMap.entrySet()) {

					AirportServiceKeyTO keyTO = apEntry.getKey();
					FlightSegmentTO flightSegmentTO = getSelectedFlightSegmentTO(flightSegmentTOs,
							bookingFltSeg.getFlightRefNumber());

					if (flightSegmentTO != null) {
						if (bookingFltSeg.getSegmentCode().equals(keyTO.getSegmentCode()) && flightSegmentTO
								.getDepartureDateTimeZulu().getTime() == keyTO.getDepartureDateTime()) {

							if (apEntry.getValue().size() > 0) {
								// set airport code to support airport services
								bookingFltSeg.setAirportCode(keyTO.getAirport());
								ancillaryAvailability.setAvailability(true);
							}
						}
					}
				}
			} else {
				ancillaryAvailability.setEnabled(false);
				ancillaryAvailability.setAvailability(false);
			}

			ancillaryAvailabilityStatus.getAncillaryAvailability().add(ancillaryAvailability);

		}
	}

	@SuppressWarnings("unchecked")
	private AncillaryAvailability populateAncillaryAvailability(BookingFlightSegment flightSegment,
			AncillaryType ancillaryType, AppIndicator app) throws ModuleException {

		AncillaryAvailability ancillaryAvailability = new AncillaryAvailability();
		ancillaryAvailability.setAncillary(ancillaryType);

		if (flightSegment.getDepatureDateTimeZulu() != null
				&& CalendarUtil.getCurrentSystemTimeInZulu().compareTo(flightSegment.getDepatureDateTimeZulu()) >= 0) {
			ancillaryAvailability.setAvailability(false);
			return ancillaryAvailability;
		}

		Collection privilegeKeys = (Collection) AASessionManager.getInstance()
				.getCurrUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);

		Map<Integer, ClassOfServiceDTO> flightSegmentIdWiseCos = getFlightSegmentIdWiseMap(flightSegment);

		// TODO - set ancillaryAvailability.setEnabled() based on app-params
		ancillaryAvailability.setEnabled(true);

		if (ancillaryType.compareTo(AncillaryType.SEAT_MAP) == 0 && AppSysParamsUtil.isShowSeatMap()
				&& !flightSegment.isForInsAvailability()) {
			Map<Integer, FlightSeatsDTO> seatMap = AAServicesModuleUtils.getSeatMapBD()
					.getFlightSeats(flightSegmentIdWiseCos, null);
			ancillaryAvailability.setAvailability(AncillaryCommonUtil.isSeatAvailable(seatMap));

		} else if (ancillaryType.compareTo(AncillaryType.INSURANCE) == 0 && AppSysParamsUtil.isShowTravelInsurance()) {

			try {
				if (app == AppIndicator.C) {
					if (aaAncillaryAvailabilityRQ.isOnHold()) {
						AAReservationUtil.authorize(privilegeKeys, MakeResPrivilegesKeys.PRIVI_INSURANCE_OHD);
					} else {
						AAReservationUtil.authorize(privilegeKeys, MakeResPrivilegesKeys.PRIVI_INSURANCE);
					}
				}
				// T_AIG_COUNTRY - check origin, if present then send true
				CommonMasterBD commonMasterBD = AAServicesModuleUtils.getCommonMasterBD();
				ancillaryAvailability.setAvailability(
						commonMasterBD.isAIGCountryCodeActive(flightSegment.getSegmentCode().split("/")[0]));

			} catch (WebservicesException e) {
				ancillaryAvailability.setAvailability(false);
			}
		} else if (ancillaryType.compareTo(AncillaryType.MEALS) == 0 && AppSysParamsUtil.isShowMeals()
				&& !flightSegment.isForInsAvailability()) {

			Map<Integer, Set<String>> selectedMeals = AncillaryUtil
					.getFlightSegIdWiseSelectedAnci(aaAncillaryAvailabilityRQ.getFlightRefWiseSelectedMeals());

			int salesChannelCode = Integer.valueOf(AAUtils.getSalesChannelFromPOS(aaAncillaryAvailabilityRQ.getAaPos()));

			Map<Integer, List<FlightMealDTO>> mealMap = AAServicesModuleUtils.getMealBD()
					.getMeals(flightSegmentIdWiseCos, selectedMeals, false, null, salesChannelCode);
			ancillaryAvailability.setAvailability(AncillaryCommonUtil.isMealAvailable(mealMap, selectedMeals));

		} else if (ancillaryType.compareTo(AncillaryType.FLEXI) == 0 && AppSysParamsUtil.isFlexiEnabledInAnci()) {
			ancillaryAvailability.setAvailability(true);

		} else if (ancillaryType.compareTo(AncillaryType.BAGGAGE) == 0 && AppSysParamsUtil.isShowBaggages()) {

			try {
				if (app == AppIndicator.C) {
					AAReservationUtil.authorize(privilegeKeys, MakeResPrivilegesKeys.PRIVI_BAGGAGE_SHOW);
				}

				Map<Integer, List<FlightBaggageDTO>> baggageMap = AAServicesModuleUtils.getBaggageBD()
						.getBaggages(flightSegmentIdWiseCos, false, false);
				ancillaryAvailability.setAvailability(AncillaryCommonUtil.isBaggageAvailable(baggageMap));
			} catch (Exception ex) {
				ancillaryAvailability.setAvailability(false);
			}

		} else if (ancillaryType.compareTo(AncillaryType.SSR) == 0 && AppSysParamsUtil.isInFlightServicesEnabled()
				&& !flightSegment.isForInsAvailability()) {
			try {
				if (app == AppIndicator.C) {
					AAReservationUtil.authorize(privilegeKeys, MakeResPrivilegesKeys.PRIVI_SSR_ADD);
				}

				Map<Integer, Set<String>> selectedSSRs = AncillaryUtil
						.getFlightSegIdWiseSelectedAnci(aaAncillaryAvailabilityRQ.getFlightRefWiseSelectedSSRs());

				SsrBD ssrDelegate = AAServicesModuleUtils.getSsrServiceBD();
				Set<String> ondCodes = getOndCodeList(flightSegment.getSegmentCode());

				String salesChannel = AAUtils.getSalesChannelFromPOS(aaAncillaryAvailabilityRQ.getAaPos());

				List<SpecialServiceRequestDTO> specialServiceRequestDTOs = ssrDelegate.getActiveSSRsByFltSegIds(
						flightSegmentIdWiseCos, SSRCategory.ID_INFLIGHT_SERVICE.toString(), salesChannel,
						AppSysParamsUtil.isInventoryCheckForSSREnabled(), null,
						aaAncillaryAvailabilityRQ.isModifyAnci(), false, AuthorizationUtil.hasPrivilege(privilegeKeys,
								PriviledgeConstants.PRIVI_MODIFY_SSR_WITHIN_BUFFER), false);

				if (specialServiceRequestDTOs.size() > 0) {

					for (SpecialServiceRequestDTO specialServiceRequestDTO : specialServiceRequestDTOs) {

						if (ondCodes.contains(specialServiceRequestDTO.getOndCode())
								|| specialServiceRequestDTO.getOndCode().equals("***/***")) {
							if (AppSysParamsUtil.isInventoryCheckForSSREnabled()
									&& !aaAncillaryAvailabilityRQ.isModifyAnci()) {
								if (specialServiceRequestDTO.getAvailableQty() > 0) {
									ancillaryAvailability.setAvailability(true);
									break;
								} else if ((specialServiceRequestDTO.getAvailableQty().intValue() == 0
										|| "N".equals(specialServiceRequestDTO.getStatus()))
										&& isSSRAlreadyAdded(selectedSSRs,
												FlightRefNumberUtil
														.getSegmentIdFromFlightRPH(flightSegment.getFlightRefNumber()),
												specialServiceRequestDTO.getSsrCode())) {
									ancillaryAvailability.setAvailability(true);
									break;
								}
							} else {
								ancillaryAvailability.setAvailability(true);
								break;
							}

						}
					}
				} else {
					ancillaryAvailability.setAvailability(false);
				}
			} catch (Exception ex) {
				ancillaryAvailability.setAvailability(false);
			}

		} else {
			ancillaryAvailability.setAvailability(false);
		}

		return ancillaryAvailability;
	}

	private void checkPrivilegesAndBufferTimes(AAAncillaryAvailabilityRS aaAncillaryAvailabilityRS,
			boolean isModifyAnci, AppIndicator appIndicator) {
		for (FlightSegmentAncillaryAvailabilityStatus anciAvailabilityStatus : aaAncillaryAvailabilityRS
				.getAncillaryAvailabilityStatus()) {
			BookingFlightSegment flightSegment = anciAvailabilityStatus.getFlightSegments();
			Date departureDate = flightSegment.getDepatureDateTime();
			String airportCode = flightSegment.getSegmentCode().split("/")[0];
			Date depatureDateZulu = null;

			boolean isStandBy = isStandby(flightSegment);
			try {
				depatureDateZulu = DateUtil.getZuluDateTime(departureDate, airportCode);
			} catch (ModuleException e) {
				depatureDateZulu = departureDate;
			}
			for (AncillaryAvailability anciAvailability : anciAvailabilityStatus.getAncillaryAvailability()) {
				if (anciAvailability != null) {
					if (appIndicator == AppIndicator.C) {
						anciAvailability.setAvailability(anciAvailability.isAvailability() && isEnableServiceXBE(
								anciAvailability.getAncillary(), depatureDateZulu, isModifyAnci, isStandBy));
					} else if (appIndicator == AppIndicator.W) {
						anciAvailability.setAvailability(anciAvailability.isAvailability() && AAUtils
								.isEnableServiceIBE(anciAvailability.getAncillary(), depatureDateZulu, isModifyAnci));
					}
				}
			}
		}
	}

	private boolean isEnableServiceXBE(AncillaryType anciType, Date departDate, boolean isInEditMode,
			boolean isStandBy) {

		Collection privilegeKeys = (Collection) AASessionManager.getInstance()
				.getCurrUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);

		if (isInEditMode && anciType == AncillaryType.SEAT_MAP
				&& !isAuhtorized(privilegeKeys, AlterResPrivilegesKeys.PRIVI_SEAT_UPDATE)) {
			return false;
		}
		if (isInEditMode && anciType == AncillaryType.SSR
				&& !isAuhtorized(privilegeKeys, AlterResPrivilegesKeys.PRIVI_SSR_CHANGE)) {
			return false;
		}

		if (anciType == AncillaryType.INSURANCE) {
			if (aaAncillaryAvailabilityRQ.isOnHold()) {
				return isAuhtorized(privilegeKeys, MakeResPrivilegesKeys.PRIVI_INSURANCE_OHD);
			} else {
				return isAuhtorized(privilegeKeys, MakeResPrivilegesKeys.PRIVI_INSURANCE);
			}
		}
		// Cutover validation is done in backend
		if (anciType == AncillaryType.MEALS) {
			return true;
		}

		if (anciType == AncillaryType.FLEXI) {
			return true;
		}

		long diffMils = 3600000;
		long lngBufferTime = 0;
		Calendar currentTime = Calendar.getInstance();
		long modificationStopBuffer = AppSysParamsUtil.getInternationalBufferDurationInMillis();

		if (anciType == AncillaryType.SEAT_MAP) {
			diffMils = AppSysParamsUtil.getXBESeatmapStopCutoverInMillis();
		} else if (anciType == AncillaryType.MEALS) {
			diffMils = AppSysParamsUtil.getTimeInMillis(AppSysParamsUtil.getMealCutOverTime());
		} else if (anciType == AncillaryType.BAGGAGE) {
			diffMils = BaggageTimeWatcher.getBaggageCutOverTimeForXBEInMillis(isInEditMode,
					isAuhtorized(privilegeKeys, MakeResPrivilegesKeys.ALLOW_BAGGAGES_TILL_FINAL_CUT_OVER), isStandBy);
		} else if (anciType == AncillaryType.SSR) {
			diffMils = AppSysParamsUtil.getSSRCutOverTimeInMillis();
		} else if (anciType == AncillaryType.INSURANCE) {
			diffMils = lngBufferTime;
		}

		if (isInEditMode) {
			if (anciType.equals(AncillaryType.BAGGAGE) && AppSysParamsUtil.isBaggageMandatory()) {
				if (departDate.getTime() - diffMils < currentTime.getTimeInMillis()) {
					return false;
				} else {
					return true;
				}
			} else if ((departDate.getTime() - modificationStopBuffer) < currentTime.getTimeInMillis()) {
				if (anciType == AncillaryType.SSR
						&& isAuhtorized(privilegeKeys, AlterResPrivilegesKeys.ALT_BUFFERTIME_SSR)) {
					return true;
				} else if (anciType == AncillaryType.SEAT_MAP
						&& isAuhtorized(privilegeKeys, MakeResPrivilegesKeys.PRIVI_ADD_SEATS_IN_CUTOVER)) {
					return true;
				} else {
					return false;
				}
			} else {
				if (anciType == AncillaryType.SSR && isAuhtorized(privilegeKeys, MakeResPrivilegesKeys.PRIVI_SSR_ADD)) {
					return true;
				} else if (anciType == AncillaryType.SEAT_MAP) {
					return true;
				} else if (anciType == AncillaryType.MEALS) {
					return true;
				} else if (anciType == AncillaryType.BAGGAGE) {
					return true;
				} else {
					return false;
				}
			}
		} else if (departDate.getTime() - diffMils < currentTime.getTimeInMillis()) {
			if (anciType == AncillaryType.SEAT_MAP
					&& isAuhtorized(privilegeKeys, MakeResPrivilegesKeys.PRIVI_ADD_SEATS_IN_CUTOVER)) {
				return true;
			} else {
				return false;
			}
		} else {
			if (anciType == AncillaryType.SSR && isAuhtorized(privilegeKeys, MakeResPrivilegesKeys.PRIVI_SSR_ADD)) {
				return true;
			} else if (anciType == AncillaryType.SEAT_MAP) {
				return true;
			} else if (anciType == AncillaryType.MEALS) {
				return true;
			} else if (anciType == AncillaryType.BAGGAGE) {
				return true;
			} else {
				return false;
			}
		}
	}

	private boolean isAuhtorized(Collection privilegeKeys, String privilige) {
		boolean isAuthorized = false;
		try {
			AAReservationUtil.authorize(privilegeKeys, privilige);
			isAuthorized = true;
		} catch (WebservicesException e) {
			isAuthorized = false;
		}

		return isAuthorized;
	}

	private Map<Integer, ClassOfServiceDTO> getFlightSegmentIdWiseMap(BookingFlightSegment flightSegment)
			throws ModuleException {
		Map<Integer, ClassOfServiceDTO> flightSegmentIds = new HashMap<Integer, ClassOfServiceDTO>();

		ClassOfServiceDTO cosDTO = new ClassOfServiceDTO(flightSegment.getCabinClassCode(),
				flightSegment.getLogicalCabinClassCode(), flightSegment.getSegmentCode());

		if (flightSegment.getFlightRefNumber() != null) {
			flightSegmentIds.put(new Integer(flightSegment.getFlightRefNumber()), cosDTO);
			return flightSegmentIds;
		}

		Integer flightId = AAServicesModuleUtils.getFlightBD().getFlightID(flightSegment.getSegmentCode(),
				flightSegment.getFlightNumber(), flightSegment.getDepatureDateTime());

		if (flightId == null) {
			throw new ModuleException("webservices.airinventory.flight.notfound");
		}

		Collection<Integer> flightIds = new ArrayList<Integer>();
		flightIds.add(flightId);

		Collection<FlightSeatsDTO> flightSeatsDTOs = AAServicesModuleUtils.getFlightBD()
				.getFlightSegIdsforFlight(flightIds);

		for (FlightSeatsDTO flightSeatsDTO : flightSeatsDTOs) {
			flightSegmentIds.put(new Integer(flightSeatsDTO.getFlightSegmentID()), cosDTO);
		}

		return flightSegmentIds;
	}

	private Set<String> getOndCodeList(String segmentCode) {
		Set<String> ondCodeList = new HashSet<String>();
		Set<String> segments = getMultiLegSegList(segmentCode);

		for (String segment : segments) {
			String[] ondArray = segment.split("/");
			ondCodeList.add(segment);
			ondCodeList.add(ondArray[0] + "/***");
			ondCodeList.add("***/" + ondArray[1]);
		}

		return ondCodeList;
	}

	private static Set<String> getMultiLegSegList(String segmentCode) {
		Set<String> segments = new HashSet<String>();
		String ssArr[] = segmentCode.split("/");
		if (ssArr.length == 2) {
			segments.add(ssArr[0] + "/" + ssArr[1]);
		} else {
			for (int i = 0; i < ssArr.length - 2; i++) {
				String ori = ssArr[i + 0];
				String con = ssArr[i + 1];
				String des = ssArr[i + 2];

				segments.add(ori + "/" + con);
				segments.add(con + "/" + des);
			}
		}

		return segments;
	}

	private FlightSegmentTO getSelectedFlightSegmentTO(List<FlightSegmentTO> flightSegmentTOs, String flightRefNumber) {

		Iterator<FlightSegmentTO> flightSegmentTOsItr = flightSegmentTOs.iterator();

		while (flightSegmentTOsItr.hasNext()) {
			FlightSegmentTO flightSegmentTO = flightSegmentTOsItr.next();
			if (BeanUtils.nullHandler(flightSegmentTO.getFlightSegId()).equals(flightRefNumber)) {
				return flightSegmentTO;
			}

		}

		return null;
	}

	private boolean isSSRAlreadyAdded(Map<Integer, Set<String>> fltSegIdWiseSSRs, Integer flightSegId, String ssrCode) {

		if (fltSegIdWiseSSRs != null) {
			for (Entry<Integer, Set<String>> entry : fltSegIdWiseSSRs.entrySet()) {
				if (flightSegId != null && (flightSegId.intValue() == entry.getKey().intValue())
						&& entry.getValue() != null && entry.getValue().contains(ssrCode)) {
					return true;
				}
			}
		}

		return false;
	}

	private boolean isStandby(BookingFlightSegment flightSegment) {
		boolean isStandBy = true;
		if (flightSegment != null) {
			if (!BookingClass.BookingClassType.STANDBY.equals(flightSegment.getBookingType())) {
				isStandBy = false;
			}
		}
		return isStandBy;
	}

}