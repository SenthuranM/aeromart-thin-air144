package com.isa.thinair.aaservices.core.bl.flightservice;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingFlightSegment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.LCCSegmentInfo;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAError;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASegmentInfoRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASegmentInfoRS;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;

public class FlightSegmentInfoBL {

	private static Log log = LogFactory.getLog(FlightSegmentInfoBL.class);

	private AASegmentInfoRQ aaSegmentInfoRQ;

	public AASegmentInfoRS execute() {

		AASegmentInfoRS aaSegmentInfoRS = new AASegmentInfoRS();

		List<BookingFlightSegment> flightSegments;
		List<Integer> flightSegIds;
		int flightSegId;

		Map<Integer, BigDecimal> loadFactors;
		Map<Integer, Set<Integer>> interceptingSegs;
		Map<Integer, BigDecimal> baggageWeight;

		LCCSegmentInfo lccSegmentInfo;

		try {
			flightSegments = aaSegmentInfoRQ.getFlightSegment();
			flightSegIds = new ArrayList<Integer>();
			for (BookingFlightSegment bookingFlightSegment : flightSegments) {
				flightSegIds.add(Integer.parseInt(bookingFlightSegment.getFlightRefNumber()));
			}

			loadFactors = AAServicesModuleUtils.getFlightInventoryBD().getFlightLoadFactor(flightSegIds);
			interceptingSegs = AAServicesModuleUtils.getFlightInventoryBD().getInterceptingFlightSegmentIds(flightSegIds);
			baggageWeight = AAServicesModuleUtils.getBaggageBD().getTotalBaggageWeight(interceptingSegs);


			for (BookingFlightSegment bookingFlightSegment : flightSegments) {
				flightSegId = Integer.parseInt(bookingFlightSegment.getFlightRefNumber());
				lccSegmentInfo = new LCCSegmentInfo();
				lccSegmentInfo.setFlightSegment(bookingFlightSegment);
				lccSegmentInfo.setSeatFactor(loadFactors.get(flightSegId));
				lccSegmentInfo.setTotalWeight(baggageWeight.get(flightSegId));
				aaSegmentInfoRS.getSegmentInfo().add(lccSegmentInfo);
			}
		} catch (Exception e) {
			log.error("Error occurred while fetching flight segment info " , e );

			AAError aaError = new AAError();
			aaError.setDescription(e.getMessage());
			aaSegmentInfoRS.getResponseAttributes().getErrors().add(aaError);
			aaSegmentInfoRS.getResponseAttributes().setSuccess(null);
		}

		return aaSegmentInfoRS;
	}

	public AASegmentInfoRQ getAaSegmentInfoRQ() {
		return aaSegmentInfoRQ;
	}

	public void setAaSegmentInfoRQ(AASegmentInfoRQ aaSegmentInfoRQ) {
		this.aaSegmentInfoRQ = aaSegmentInfoRQ;
	}
}
