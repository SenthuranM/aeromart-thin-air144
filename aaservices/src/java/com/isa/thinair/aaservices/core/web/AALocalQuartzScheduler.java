package com.isa.thinair.aaservices.core.web;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.Scheduler;
import org.quartz.SchedulerFactory;
import org.quartz.impl.JobDetailImpl;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.triggers.CronTriggerImpl;

import com.isa.thinair.aaservices.core.session.AASessionCleaningQuartzJob;
import com.isa.thinair.aaservices.core.transaction.AATxnCleaningQuartzJob;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;

class AALocalQuartzScheduler {

	private static Log log = LogFactory.getLog(AALocalQuartzScheduler.class);

	private Scheduler sched;

	public AALocalQuartzScheduler() throws Exception {
		log.info("------- Initializing AA LocalQuartzScheduler-------------------");

		SchedulerFactory sf = new StdSchedulerFactory();
		sched = sf.getScheduler();

		log.info("------- Initialization Complete AA LocalQuartzScheduler--------");
	}

	void scheduleAndStartScheduler() throws Exception {
		log.info("------- Scheduling Jobs AA----------------");

		long sessCleaningIntervalInMins = AAServicesModuleUtils.getWebServicesConfig().getUserSessionCleaningGapInMillis()
				/ (1000 * 60);

		JobDetailImpl job1 = new JobDetailImpl("jobAASessionCleaning", "groupLocal", AASessionCleaningQuartzJob.class);
		CronTriggerImpl trigger1 = new CronTriggerImpl("triggerAASessionCleaning", "groupLocal", "jobAASessionCleaning", "groupLocal",
				"0 0/" + sessCleaningIntervalInMins + " * * * ?");
		Date ft1 = sched.scheduleJob(job1, trigger1);

		log.info(job1.getKey().getName() + " has been scheduled to run at: " + ft1 + " and repeat based on expression: "
				+ trigger1.getCronExpression());

		long txnCleaningIntervalInMins = AAServicesModuleUtils.getWebServicesConfig().getTxnCleaningGapInMillis() / (1000 * 60);

		JobDetailImpl job2 = new JobDetailImpl("jobAATxnCleaning", "groupLocal", AATxnCleaningQuartzJob.class);
		CronTriggerImpl trigger2 = new CronTriggerImpl("triggerAATxnCleaning", "groupLocal", "jobAATxnCleaning", "groupLocal", "0 0/"
				+ txnCleaningIntervalInMins + " * * * ?");
		Date ft2 = sched.scheduleJob(job2, trigger2);
		log.info(job2.getKey().getName() + " has been scheduled to run at: " + ft2 + " and repeat based on expression: "
				+ trigger2.getCronExpression());

		log.info("------- Starting Scheduler ----------------");
		sched.start();
		log.info("------- Started Scheduler -----------------");

	}

	
}
