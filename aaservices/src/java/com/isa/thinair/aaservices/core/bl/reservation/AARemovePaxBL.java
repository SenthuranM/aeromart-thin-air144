package com.isa.thinair.aaservices.core.bl.reservation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.isa.connectivity.profiles.maxico.v1.common.dto.AirTraveler;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAAirBookModifyRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAErrorCode;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.aaservices.core.util.AAReservationUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.PaxTypeUtils;
import com.isa.thinair.airproxy.api.dto.ReservationConstants;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.CustomChargesTO;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.dto.ExternalSegmentTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.airreservation.api.model.ExternalPnrSegment;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.service.PassengerBD;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.platform.api.ServiceResponce;

class AARemovePaxBL {

	private static Logger logger = Logger.getLogger(AARemovePaxBL.class);

	@SuppressWarnings("unchecked")
	static void execute(Reservation reservation, AAAirBookModifyRQ aaAirBookModifyRQ) throws Exception {

		try {
			if (reservation != null) {
				List<AirTraveler> airTravelers = aaAirBookModifyRQ.getAaReservation().getAirReservation().getTravelerInfo()
						.getAirTraveler();
				List<Integer> pnrPaxIds = getPaxIdsForRemoval(airTravelers);

				if (reservation.getPassengers().size() <= pnrPaxIds.size()) {
					throw new WebservicesException(AAErrorCode.ERR_56_MAXICO_REQUIRED_INVALID_PASSENGER_SIZE_TO_REMOVE,
							"Cannot remove all passengers from reservation PNR: " + reservation.getPnr());
				}

				TrackInfoDTO trackInfoDTO = new TrackInfoDTO();
				trackInfoDTO.setCarrierCode(aaAirBookModifyRQ.getAaPos().getMarketingAirLineCode());
				trackInfoDTO.setIpAddress(aaAirBookModifyRQ.getAaPos().getMarketingIPAddress());

				PassengerBD passengerDelegate = AAServicesModuleUtils.getPassengerBD();
				String newPnr = aaAirBookModifyRQ.getAaReservation().getGroupPnr();

				CustomChargesTO customChargesTO = AAReservationUtil.createCustomChargesTO(reservation,
						aaAirBookModifyRQ.getChargeOverride(), ReservationConstants.AlterationType.ALT_CANCEL_RES);
								
				Map<Integer, List<ExternalChgDTO>> paxCharges = AAModifyReservationQueryUtil
						.tranformPaxExternalChargeToExternalChgDTO(aaAirBookModifyRQ.getPerPaxExternalCharges());

				ServiceResponce serviceResponce = passengerDelegate.removePassengers(reservation.getPnr(), newPnr, pnrPaxIds,
						customChargesTO, reservation.getVersion(), trackInfoDTO, null, paxCharges);
				if (!serviceResponce.isSuccess()) {
					throw new WebservicesException(AAErrorCode.ERR_34_MAXICO_REQUIRED_REMOVE_PASSENGER_FAILED, "Response Code ["
							+ serviceResponce.getResponseCode() + "] ");
				} else {

					LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
					pnrModesDTO.setPnr(newPnr);
					pnrModesDTO.setLoadFares(true);
					pnrModesDTO.setLoadOndChargesView(true);
					pnrModesDTO.setLoadPaxAvaBalance(true);
					pnrModesDTO.setLoadSegViewBookingTypes(true);
					pnrModesDTO.setLoadSegView(true);
					pnrModesDTO.setLoadLastUserNote(true);
					pnrModesDTO.setLoadSegViewFareCategoryTypes(true);
					pnrModesDTO.setRecordAudit(true);

					reservation = AAServicesModuleUtils.getReservationBD().getReservation(pnrModesDTO, null);
					Collection<ExternalPnrSegment> colReservationExternalSegment = reservation.getExternalReservationSegment();
					Iterator<ExternalPnrSegment> itReservationExternalSegment = colReservationExternalSegment.iterator();
					Collection<ExternalSegmentTO> colExternalSegmentTO = new ArrayList<ExternalSegmentTO>();

					while (itReservationExternalSegment.hasNext()) {
						ExternalPnrSegment reservationExternalSegment = itReservationExternalSegment.next();

						// Capturing the Confirmed external segments in order to be sent to the change external
						// segment(s) api
						if (ReservationInternalConstants.ReservationExternalSegmentStatus.CONFIRMED
								.equals(reservationExternalSegment.getStatus())) {
							ExternalSegmentTO externalSegmentTO = new ExternalSegmentTO(reservationExternalSegment);
							externalSegmentTO.setStatus(ReservationInternalConstants.ReservationExternalSegmentStatus.CANCEL);
							colExternalSegmentTO.add(externalSegmentTO);
						}
					}

					if (colExternalSegmentTO.size() > 0) {
						AAServicesModuleUtils.getSegmentBD().changeExternalSegments(newPnr, colExternalSegmentTO,
								reservation.getVersion(), null);
					}
				}
			}
		} catch (Exception e) {
			logger.error("Error in AARemovePaxBL.execute", e);
			throw e;
		}
	}

	private static List<Integer> getPaxIdsForRemoval(List<AirTraveler> airTravelers) {
		List<Integer> pnrPaxIds = new ArrayList<Integer>();
		for (AirTraveler airTraveler : airTravelers) {
			pnrPaxIds.add(PaxTypeUtils.getPnrPaxId(airTraveler.getTravelerRefNumber()));
		}

		return pnrPaxIds;
	}
}
