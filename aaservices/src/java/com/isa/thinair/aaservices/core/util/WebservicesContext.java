/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005-08 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * ===============================================================================
 */
package com.isa.thinair.aaservices.core.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * For propagating data across different method calls during a service execution.
 * 
 * @author Mohamed Nasly
 * @author thejaka
 */
public class WebservicesContext {

	private Map<String, Object> wsContexParams = new HashMap<String, Object>();

	/**
	 * Add parameter to the WebserviceContext
	 * 
	 * @param key
	 * @param value
	 */
	public void setParameter(String key, Object value) {
		wsContexParams.put(key, value);
	}

	/**
	 * Get parameter from the WebserviceContext
	 * 
	 * @param key
	 * @return
	 */
	public Object getParameter(String key) {
		return wsContexParams.get(key);
	}

	/**
	 * Remove parameter from the WebserviceContext
	 * 
	 * @param key
	 * @return Parameter being removed
	 */
	public Object removeParameter(String key) {
		return wsContexParams.remove(key);
	}

	/**
	 * Removes all the parameters from the Webservices context.
	 */
	public void removeAllParameters() {
		wsContexParams.clear();
	}

	/**
	 * Get all parameter keys from the WebservicesContext
	 * 
	 * @return
	 */
	public Iterator<String> getAllParamsKeysIterator() {
		return wsContexParams.keySet().iterator();
	}
}
