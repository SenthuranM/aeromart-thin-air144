package com.isa.thinair.aaservices.core.bl.ancillary;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.Baggage;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingBaggageRequest;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingFlightSegment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.BookingSystems;
import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegmentBaggages;
import com.isa.connectivity.profiles.maxico.v1.common.dto.ReservationBaggageParams;
import com.isa.connectivity.profiles.maxico.v1.common.dto.SelectedBaggage;
import com.isa.connectivity.profiles.maxico.v1.common.dto.StringIntegerMap;
import com.isa.connectivity.profiles.maxico.v1.common.dto.StringStringMap;
import com.isa.thinair.aaservices.core.transaction.TransactionalReservationProcess;
import com.isa.thinair.aaservices.core.transaction.TransactionalReservationProcessBL;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.AncillaryUtil;
import com.isa.thinair.airinventory.api.dto.ClassOfServiceDTO;
import com.isa.thinair.airinventory.api.dto.baggage.BaggageRq;
import com.isa.thinair.airinventory.api.dto.baggage.FlightBaggageDTO;
import com.isa.thinair.airinventory.api.dto.bundledfare.BundledFareDTO;
import com.isa.thinair.airinventory.api.util.AirInventoryModuleUtils;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.utils.AnciOfferUtil;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.constants.CommonsConstants.SeatFactorConditionApplyFor;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.promotion.api.model.AnciOfferType;
import com.isa.thinair.promotion.api.to.anciOffer.AnciOfferCriteriaSearchTO;
import com.isa.thinair.promotion.api.to.anciOffer.AnciOfferCriteriaTO;

class AABaggagesBL implements TransactionalReservationProcessBL {

	private static final Log log = LogFactory.getLog(AAMealsBL.class);

	private TransactionalReservationProcess transactionalReservationProcess;

	private String pnr;

	List<FlightSegmentBaggages> execute(BookingBaggageRequest baggageRequest, List<BookingFlightSegment> bookingFlightSegments,
			String selectedLangauge, BookingSystems appEngine, boolean isRequote, List<StringIntegerMap> ondWiseBundlePeriodIDs)
			throws ModuleException {

		BaggageRq baggageRq;

		if (log.isDebugEnabled()) {
			log.debug("execute method called");
		}

		List<FlightSegmentBaggages> flightSegmentBaggages = new ArrayList<FlightSegmentBaggages>();

		Set<BookingFlightSegment> bkFltSegs = AncillaryUtil.getFlightSegmentSet(bookingFlightSegments,
				baggageRequest.getFlightRefNumberRPHList());

		List<FlightSegmentTO> flightSegmentTOs = AncillaryUtil.getFlightSegmentTOs(BeanUtils.toList(bkFltSegs));
		Map<Integer, ClassOfServiceDTO> flightSegIdWiseCos = AncillaryUtil.getFlightSegmentIdWiseClassOfServices(bkFltSegs);
		Map<String, Integer> ondWiseBundlePeriodIDMap = getOndWiseBundlePeriodIDs(ondWiseBundlePeriodIDs);
		Map<Integer, List<FlightBaggageDTO>> baggageMap = null;
		// TODO handle the scenario where the requested language is not available
		if (selectedLangauge != null && !selectedLangauge.trim().equals("") && !selectedLangauge.trim().equalsIgnoreCase("en")) {
			if (AppSysParamsUtil.isONDBaggaeEnabled()) {
				Map<String, String> bookingClassMapping = new HashMap<String, String>();
				Map<String, String> classesOfServiceMapping = new HashMap<String, String>();
				ReservationBaggageParams baggageParams = baggageRequest.getReservationBaggageParams();
				for (StringStringMap entry : baggageParams.getBookingClass()) {
					bookingClassMapping.put(entry.getKey(), entry.getValue());
				}
				for (StringStringMap entry : baggageParams.getClassesOfService()) {
					classesOfServiceMapping.put(entry.getKey(), entry.getValue());
				}

				baggageRq = new BaggageRq();
				baggageRq.setFlightSegmentTOs(flightSegmentTOs);
				baggageRq.setRequote(isRequote);
				baggageRq.setCheckCutoverTime(true);
				baggageRq.setAgent(baggageRequest.getReservationBaggageParams().getAgent());
				baggageRq.setBookingClasses(bookingClassMapping);
				baggageRq.setClassesOfService(classesOfServiceMapping);
				baggageRq.setSalesChannel(baggageRequest.getReservationBaggageParams().getSalesChannel());

				baggageMap = AAServicesModuleUtils.getBaggageBD().getBaggage(baggageRq);
			} else {
				baggageMap = AAServicesModuleUtils.getBaggageBD().getBaggagesWithTranslations(flightSegIdWiseCos,
						selectedLangauge);
				baggageMap = injectExternalSegments(baggageMap, flightSegmentTOs);
			}
		} else {
			if (AppSysParamsUtil.isONDBaggaeEnabled()) {
				Map<String, String> bookingClassMapping = new HashMap<String, String>();
				Map<String, String> classesOfServiceMapping = new HashMap<String, String>();
				ReservationBaggageParams baggageParams = baggageRequest.getReservationBaggageParams();
				for (StringStringMap entry : baggageParams.getBookingClass()) {
					bookingClassMapping.put(entry.getKey(), entry.getValue());
				}
				for (StringStringMap entry : baggageParams.getClassesOfService()) {
					classesOfServiceMapping.put(entry.getKey(), entry.getValue());
				}

				baggageRq = new BaggageRq();
				baggageRq.setFlightSegmentTOs(flightSegmentTOs);
				baggageRq.setRequote(isRequote);
				baggageRq.setCheckCutoverTime(true);
				baggageRq.setAgent(baggageRequest.getReservationBaggageParams().getAgent());
				baggageRq.setBookingClasses(bookingClassMapping);
				baggageRq.setClassesOfService(classesOfServiceMapping);
				baggageRq.setSalesChannel(baggageRequest.getReservationBaggageParams().getSalesChannel());
				baggageMap = AAServicesModuleUtils.getBaggageBD().getBaggage(baggageRq);
			} else {
				baggageMap = AAServicesModuleUtils.getBaggageBD().getBaggages(flightSegIdWiseCos, false, false);
				baggageMap = injectExternalSegments(baggageMap, flightSegmentTOs);
			}
		}

		List<FlightSegmentBaggages> flightSegmentBaggage = AncillaryUtil.getFlightSegmentBaggages(bkFltSegs, baggageMap);

		flightSegmentBaggages.addAll(flightSegmentBaggage);

		setBaggageOffers(flightSegmentBaggages, selectedLangauge, flightSegmentTOs, appEngine, ondWiseBundlePeriodIDMap);
		return flightSegmentBaggages;
	}

	private Map<String, Integer> getOndWiseBundlePeriodIDs(List<StringIntegerMap> ondWiseBundlePeriodIDs) {
		Map<String, Integer> ondWiseBundlePeriodIDMap = new HashMap<String, Integer>();
		if (ondWiseBundlePeriodIDs != null) {
			for (StringIntegerMap ondWiseBundlePeriodIDEntry : ondWiseBundlePeriodIDs) {
				ondWiseBundlePeriodIDMap.put(ondWiseBundlePeriodIDEntry.getKey(), ondWiseBundlePeriodIDEntry.getValue());
			}
		}
		return ondWiseBundlePeriodIDMap;
	}

	private void setBaggageOffers(List<FlightSegmentBaggages> flightSegmentBaggages, String selectedLanguage,
			List<FlightSegmentTO> flightSegmentTOs, BookingSystems appEngine, Map<String, Integer> ondWiseBundlePeriodIDMap) throws ModuleException {

		Map<String, Integer> ondBundledFareBaggegeTemplates = AncillaryUtil.getOndBundledFareServiceTemplateIds(
				transactionalReservationProcess, pnr, ondWiseBundlePeriodIDMap, EXTERNAL_CHARGES.BAGGAGE.toString());
		boolean isBundledFareSelected = AncillaryUtil.isAtleastOneBundledFareTemplateExists(transactionalReservationProcess, pnr,
				ondWiseBundlePeriodIDMap);
		if (ondWiseBundlePeriodIDMap != null && !ondWiseBundlePeriodIDMap.isEmpty()) {
			isBundledFareSelected = true;
		}
		Collection<BookingFlightSegment> bookingFlightSegments = AncillaryUtil
				.getFlightSegmentSetFromBaggage(flightSegmentBaggages);

		Map<Integer, Map<Integer, BigDecimal>> ondWiseSeatFactorCriteria = getONDWiseSeatFactorCriteria(flightSegmentTOs);

		for (FlightSegmentBaggages segmentBaggage : flightSegmentBaggages) {
			BookingFlightSegment bookingFlightSegment = segmentBaggage.getFlightSegment();
			if (isBundledFareSelected) {
				if (ondBundledFareBaggegeTemplates != null
						&& ondBundledFareBaggegeTemplates.containsKey(bookingFlightSegment.getSegmentCode())
						&& ondBundledFareBaggegeTemplates.get(bookingFlightSegment.getSegmentCode()) != null
						&& !segmentBaggage.getBaggage().isEmpty()) {

					Integer bundledFareTemplateId = ondBundledFareBaggegeTemplates.get(bookingFlightSegment.getSegmentCode());

					List<FlightBaggageDTO> bundledFareBaggages = AAServicesModuleUtils.getBaggageBD().getBaggageFromTemplate(
							bundledFareTemplateId, selectedLanguage, bookingFlightSegment.getCabinClassCode());
					
					Map<Integer, BigDecimal> seatFactorCriterias = ondWiseSeatFactorCriteria
							.get(segmentBaggage.getFlightSegment().getOndSequence());
					
					removeInvalidBaggagesFromBundle(bundledFareBaggages, seatFactorCriterias);
					AncillaryUtil.injectBaggageONDGroupID(bundledFareBaggages,
							getBaggageONDGroupIDFromBaggage(segmentBaggage.getBaggage()));

					segmentBaggage.getBaggage().clear();
					segmentBaggage.getBaggage().addAll(AncillaryUtil.transformBaggagDTOCollection(bundledFareBaggages));

				}

			} else if (appEngine != null && (SalesChannelsUtil.isBookingSystemWebOrMobile(appEngine))) {
				setAnciOffers(segmentBaggage, bookingFlightSegments, selectedLanguage);
			}
		}

	}
	
	private Map<Integer, Map<Integer, BigDecimal>> getONDWiseSeatFactorCriteria(List<FlightSegmentTO> flightSegmentTOs) {
		Map<Integer, List<FlightSegmentTO>> ondWiseFlightSegments = new HashMap<>();
		Map<Integer, Map<Integer, BigDecimal>> ondWiseSeatFactorCriteria = new HashMap<>();
		for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
			List<FlightSegmentTO> flightSegments = ondWiseFlightSegments.get(flightSegmentTO.getOndSequence());
			if (flightSegments == null) {
				flightSegments = new ArrayList<>();
				ondWiseFlightSegments.put(flightSegmentTO.getOndSequence(), flightSegments);
			}
			flightSegments.add(flightSegmentTO);
		}
		
		for(Map.Entry<Integer, List<FlightSegmentTO>> entry : ondWiseFlightSegments.entrySet()) {
			ondWiseSeatFactorCriteria.put(entry.getKey(), getSeatFactorCriteria(entry.getValue()));
		}
		
		return ondWiseSeatFactorCriteria;
	}

	private Map<Integer, BigDecimal> getSeatFactorCriteria(List<FlightSegmentTO> flightSegmentTOs) {

		Map<Integer, BigDecimal> seatFactorCriterias = new HashMap<Integer, BigDecimal>();
		Map<Integer, BigDecimal> loadFactors = null;
		List<Integer> flightSegIds = new ArrayList<Integer>();
		for (FlightSegmentTO flightSeg : flightSegmentTOs) {
			flightSegIds.add(flightSeg.getFlightSegId());
		}
		loadFactors = AirInventoryModuleUtils.getLocalFlightInventoryBD().getFlightLoadFactor(flightSegIds);

		Integer flightSegmentWithMostRestrictiveSeatFactor = Collections
				.max(loadFactors.entrySet(), new Comparator<Map.Entry<Integer, BigDecimal>>() {
					public int compare(Map.Entry<Integer, BigDecimal> e1, Map.Entry<Integer, BigDecimal> e2) {
						return e1.getValue().compareTo(e2.getValue());
					}
				}).getKey();

		FlightSegmentTO firstSeg = getPreferedFlightSegmentBaggageRq(flightSegmentTOs,
				SeatFactorConditionApplyFor.APPLY_FOR_FIRST_SEGMENT.getCondition());
		FlightSegmentTO lastSeg = getPreferedFlightSegmentBaggageRq(flightSegmentTOs,
				SeatFactorConditionApplyFor.APPLY_FOR_LAST_SEGMENT.getCondition());

		seatFactorCriterias.put(SeatFactorConditionApplyFor.APPLY_FOR_MOTST_RESTRICTIVE.getCondition(),
				loadFactors.get(flightSegmentWithMostRestrictiveSeatFactor));
		seatFactorCriterias.put(SeatFactorConditionApplyFor.APPLY_FOR_FIRST_SEGMENT.getCondition(),
				loadFactors.get(firstSeg.getFlightSegId()));
		seatFactorCriterias.put(SeatFactorConditionApplyFor.APPLY_FOR_LAST_SEGMENT.getCondition(),
				loadFactors.get(lastSeg.getFlightSegId()));
		return seatFactorCriterias;

	}

	private static FlightSegmentTO getPreferedFlightSegmentBaggageRq(List<FlightSegmentTO> flightSegments, int condition) {

		if (SeatFactorConditionApplyFor.APPLY_FOR_FIRST_SEGMENT.getCondition() == condition) {
			Collections.sort(flightSegments, new Comparator<FlightSegmentTO>() {
				public int compare(FlightSegmentTO o1, FlightSegmentTO o2) {
					return o1.getDepartureDateTimeZulu().compareTo(o2.getDepartureDateTimeZulu());
				}
			});
			return flightSegments.get(0);
		} else if (SeatFactorConditionApplyFor.APPLY_FOR_LAST_SEGMENT.getCondition() == condition) {
			Collections.sort(flightSegments, new Comparator<FlightSegmentTO>() {
				public int compare(FlightSegmentTO o1, FlightSegmentTO o2) {
					return o2.getDepartureDateTimeZulu().compareTo(o1.getDepartureDateTimeZulu());
				}
			});
			return flightSegments.get(0);
		}

		return null;
	}

	private static void removeInvalidBaggagesFromBundle(List<FlightBaggageDTO> bundledFareBaggages,
			Map<Integer, BigDecimal> seatFactorCriterias) {
		List<FlightBaggageDTO> invalidBaggages = new ArrayList<FlightBaggageDTO>();
		for (FlightBaggageDTO bundledFareBaggage : bundledFareBaggages) {
			BigDecimal seatFactorCondition = seatFactorCriterias.get(bundledFareBaggage.getSeatFactorConditionApplyFor());
			if (seatFactorCondition != null && seatFactorCondition.intValue() > bundledFareBaggage.getSeatFactor()) {
				invalidBaggages.add(bundledFareBaggage);
			}
		}

		if (!invalidBaggages.isEmpty()) {
			bundledFareBaggages.removeAll(invalidBaggages);
		}
	}

	/**
	 * Retrieves applicable anci offer criteria and set the offer details if any offers are available, replaces the
	 * selected baggage prices with the offer prices.
	 * 
	 * @param segmentBaggages
	 *            Selected baggage details.
	 * @param selectedLanguage
	 *            Selected language.
	 * @throws ModuleException
	 *             If any of the underlying operations fail.
	 */
	@Deprecated
	private void setAnciOffers(List<FlightSegmentBaggages> segmentBaggages, String selectedLanguage) throws ModuleException {
		for (FlightSegmentBaggages segmentBaggage : segmentBaggages) {
			AnciOfferCriteriaSearchTO anciOfferSearchCriteria = AnciOfferUtil.createAnciOfferSearch(
					segmentBaggage.getFlightSegment(), AnciOfferType.BAGGAGE,
					AncillaryUtil.getFlightSegmentSetFromBaggage(segmentBaggages));

			AnciOfferCriteriaTO availableAnciOffer = AAServicesModuleUtils.getAnciOfferBD()
					.getAvailableAnciOffer(anciOfferSearchCriteria);

			if (availableAnciOffer != null) {
				List<FlightBaggageDTO> offerBaggages = AAServicesModuleUtils.getBaggageBD().getBaggageFromTemplate(
						availableAnciOffer.getTemplateID(), selectedLanguage,
						segmentBaggage.getFlightSegment().getCabinClassCode());

				AncillaryUtil.injectBaggageONDGroupID(offerBaggages,
						getBaggageONDGroupIDFromBaggage(segmentBaggage.getBaggage()));

				segmentBaggage.setIsAnciOffer(true);
				segmentBaggage.setOfferName(availableAnciOffer.getNameTranslations().get(selectedLanguage));
				segmentBaggage.setOfferDescription(availableAnciOffer.getDescriptionTranslations().get(selectedLanguage));
				segmentBaggage.setOfferedTemplateID(availableAnciOffer.getTemplateID());

				segmentBaggage.getBaggage().clear();
				segmentBaggage.getBaggage().addAll(AncillaryUtil.transformBaggagDTOCollection(offerBaggages));
			}
		}
	}

	/**
	 * Retrieves applicable anci offer criteria and set the offer details if any offers are available, replaces the
	 * selected baggage prices with the offer prices.
	 * 
	 * @param segmentBaggage
	 *            Selected segment baggage details.
	 * @param selectedLanguage
	 *            Selected language.
	 * @throws ModuleException
	 *             If any of the underlying operations fail.
	 */
	private void setAnciOffers(FlightSegmentBaggages segmentBaggage, Collection<BookingFlightSegment> bookingFlightSegments,
			String selectedLanguage) throws ModuleException {
		AnciOfferCriteriaSearchTO anciOfferSearchCriteria = AnciOfferUtil.createAnciOfferSearch(segmentBaggage.getFlightSegment(),
				AnciOfferType.BAGGAGE, bookingFlightSegments);

		AnciOfferCriteriaTO availableAnciOffer = AAServicesModuleUtils.getAnciOfferBD()
				.getAvailableAnciOffer(anciOfferSearchCriteria);

		if (availableAnciOffer != null) {
			List<FlightBaggageDTO> offerBaggages = AAServicesModuleUtils.getBaggageBD().getBaggageFromTemplate(
					availableAnciOffer.getTemplateID(), selectedLanguage, segmentBaggage.getFlightSegment().getCabinClassCode());

			AncillaryUtil.injectBaggageONDGroupID(offerBaggages, getBaggageONDGroupIDFromBaggage(segmentBaggage.getBaggage()));

			segmentBaggage.setIsAnciOffer(true);
			segmentBaggage.setOfferName(availableAnciOffer.getNameTranslations().get(selectedLanguage));
			segmentBaggage.setOfferDescription(availableAnciOffer.getDescriptionTranslations().get(selectedLanguage));
			segmentBaggage.setOfferedTemplateID(availableAnciOffer.getTemplateID());

			segmentBaggage.getBaggage().clear();
			segmentBaggage.getBaggage().addAll(AncillaryUtil.transformBaggagDTOCollection(offerBaggages));
		}
	}

	private Map<Integer, List<FlightBaggageDTO>> injectExternalSegments(Map<Integer, List<FlightBaggageDTO>> baggageMap,
			List<FlightSegmentTO> flightSegmentTOs) throws ModuleException {

		if (isExternalSegmentsExists(flightSegmentTOs)) {
			List<FlightBaggageDTO> firstFlightBaggageDTO = BeanUtils.getFirstElement(baggageMap.values());
			Map<Integer, List<FlightBaggageDTO>> newExternalFlightBaggages = new HashMap<Integer, List<FlightBaggageDTO>>();

			for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
				if (!baggageMap.keySet().contains(new Integer(flightSegmentTO.getFlightSegId()))) {
					newExternalFlightBaggages.put(new Integer(flightSegmentTO.getFlightSegId()), firstFlightBaggageDTO);
				}
			}

			baggageMap.putAll(newExternalFlightBaggages);

			int baggageOndGrpId = getBaggageOndGrpId(flightSegmentTOs);
			for (List<FlightBaggageDTO> lstFlightBaggageDTO : baggageMap.values()) {
				for (FlightBaggageDTO flightBaggageDTO : lstFlightBaggageDTO) {
					flightBaggageDTO.setOndGroupId(BeanUtils.nullHandler(baggageOndGrpId));
				}
			}
		}

		return baggageMap;
	}

	private int getBaggageOndGrpId(List<FlightSegmentTO> flightSegmentTOs) throws ModuleException {
		Collection<String> baggageOndGrpIds = new HashSet<String>();
		for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
			String baggageOndGrpId = BeanUtils.nullHandler(flightSegmentTO.getBaggageONDGroupId());
			if (baggageOndGrpId.length() > 0) {
				baggageOndGrpIds.add(baggageOndGrpId);
			}
		}

		if (baggageOndGrpIds.size() > 1) {
			throw new ModuleException("aaservices.baggage.invalid.baggage.ond.group.id");
		}

		if (baggageOndGrpIds.size() == 0) {
			return AAServicesModuleUtils.getBaggageBD().getNextBaggageOndGroupId();
		} else {
			return new Integer(BeanUtils.getFirstElement(baggageOndGrpIds));
		}
	}

	private boolean isExternalSegmentsExists(List<FlightSegmentTO> flightSegmentTOs) {
		for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
			if (!flightSegmentTO.getOperatingAirline().equals(AppSysParamsUtil.getDefaultCarrierCode())) {
				return true;
			}
		}

		return false;
	}

	List<FlightSegmentBaggages> getSelectedBaggageDetails(List<SelectedBaggage> selectedBaggages, boolean skipCutoverValidation)
			throws ModuleException {

		List<FlightSegmentTO> applicableSegs;
		List<FlightSegmentBaggages> flightSegmentBaggagesList = new ArrayList<FlightSegmentBaggages>();
		Set<Integer> templateIds = new HashSet<Integer>();

		Map<Integer, List<FlightBaggageDTO>> baggageMap = new HashMap<Integer, List<FlightBaggageDTO>>();
		Map<Integer, List<FlightBaggageDTO>> tempBaggageMap = null;

		Map<Integer, Integer> segmentWiseCountAdd = new HashMap<Integer, Integer>();
		Map<Integer, Integer> segmentWiseCountRem = new HashMap<Integer, Integer>();

		if (AppSysParamsUtil.isONDBaggaeEnabled()) {

			List<FlightSegmentTO> flightSegmentTOs = AncillaryUtil
					.getFlightSegmentTOs(getFlightSegmentTOWithExistingOndGroupPreserved(selectedBaggages));

			for (SelectedBaggage baggage : selectedBaggages) {
				applicableSegs = new ArrayList<FlightSegmentTO>();
				templateIds = new HashSet<Integer>();
				templateIds.addAll(baggage.getBaggageTemplateId());

				lvl_flight_seg: for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
					if (flightSegmentTO.getFlightSegId().toString().equals(baggage.getFlightSegment().getFlightRefNumber())) {
						applicableSegs.add(flightSegmentTO);
						break;
					}

					// for (BookingFlightSegment bookingFlightSegment : baggage.getExternalFlightSegments()) {
					// if
					// (flightSegmentTO.getFlightSegId().toString().equals(bookingFlightSegment.getFlightRefNumber())) {
					// applicableSegs.add(flightSegmentTO);
					// break;
					// }
					// }
				}

				for (int templateId : templateIds) {
					tempBaggageMap = AAServicesModuleUtils.getBaggageBD().getBaggage(applicableSegs, templateId);
					for (int segId : tempBaggageMap.keySet()) {
						if (!baggageMap.containsKey(segId)) {
							baggageMap.put(segId, new ArrayList<FlightBaggageDTO>());
						}

						baggageMap.get(segId).addAll(tempBaggageMap.get(segId));
					}
				}
			}

			Object[] arr = getSegmentWiseCount(baggageMap,
					AncillaryUtil.getFlightSegmentTOs(getFlightSegmentTOWithRemoveBaggages(selectedBaggages)));
			segmentWiseCountAdd = (Map<Integer, Integer>) arr[0];
			segmentWiseCountRem = (Map<Integer, Integer>) arr[1];
		} else {
			Map<Integer, ClassOfServiceDTO> flightSegIdWiseCos = getFlightSegmentIdWiseCos(selectedBaggages);
			baggageMap = AAServicesModuleUtils.getBaggageBD().getBaggages(flightSegIdWiseCos, false, skipCutoverValidation);
		}

		Map<Integer, Integer> segWiseOfferTemplateIDs = getFlightSegmentWiseTemplateIDs(selectedBaggages);

		for (Entry<Integer, List<FlightBaggageDTO>> segmentEntry : baggageMap.entrySet()) {

			Integer segmentId = segmentEntry.getKey();
			List<FlightBaggageDTO> flightBaggageDTOs = segmentEntry.getValue();
			String cabinClassCode = flightBaggageDTOs != null ? flightBaggageDTOs.iterator().next().getCabinClassCode() : null;

			List<FlightBaggageDTO> offerBaggages = AAServicesModuleUtils.getBaggageBD()
					.getBaggageFromTemplate(segWiseOfferTemplateIDs.get(segmentId), Locale.ENGLISH.getLanguage(), null);

			AncillaryUtil.injectBaggageONDGroupID(offerBaggages, getBaggageONDGroupIDFromDTO(flightBaggageDTOs));

			if (!offerBaggages.isEmpty()) {
				flightBaggageDTOs = offerBaggages;
			}

			for (FlightBaggageDTO flightBaggageDTO : flightBaggageDTOs) {
				for (SelectedBaggage selectedBaggage : selectedBaggages) {
					if (selectedBaggage.getBaggageName().contains(flightBaggageDTO.getBaggageName())
							&& selectedBaggage.getFlightSegment().getFlightRefNumber().equals(segmentId.toString())) {

						Baggage baggage = new Baggage();

						if (AppSysParamsUtil.isONDBaggaeEnabled()) {

							if (selectedBaggage.getFlightSegment().getBaggageONDGroupId() != null) {
								baggage.setBaggageCharge(AccelAeroCalculator.divide(flightBaggageDTO.getAmount(),
										segmentWiseCountAdd.get(segmentId)));
							} else {
								baggage.setBaggageCharge(AccelAeroCalculator.divide(flightBaggageDTO.getAmount(),
										segmentWiseCountRem.get(segmentId)));
							}
						} else {
							baggage.setBaggageCharge(flightBaggageDTO.getAmount());
						}

						baggage.setBaggageName(flightBaggageDTO.getBaggageName());
						baggage.setOndGroupId(flightBaggageDTO.getOndGroupId());
						baggage.setOndChargeId(BeanUtils.nullHandler(flightBaggageDTO.getChargeId()));

						FlightSegmentBaggages flightSegmentBaggages = new FlightSegmentBaggages();
						flightSegmentBaggages.setFlightSegment(selectedBaggage.getFlightSegment());
						flightSegmentBaggages.getBaggage().add(baggage);

						if (segWiseOfferTemplateIDs.get(segmentId) != null) {
							flightSegmentBaggages.setIsAnciOffer(true);
							flightSegmentBaggages.setOfferedTemplateID(segWiseOfferTemplateIDs.get(segmentId));
						}

						flightSegmentBaggagesList.add(flightSegmentBaggages);
					}
				}
			}
		}

		return flightSegmentBaggagesList;

	}

	private String getBaggageONDGroupIDFromDTO(Collection<FlightBaggageDTO> flightBaggageDTOs) {
		String groupID = null;

		for (FlightBaggageDTO flightBaggageDTO : flightBaggageDTOs) {
			groupID = flightBaggageDTO.getOndGroupId();
			break;
		}

		return groupID;
	}

	private String getBaggageONDGroupIDFromBaggage(Collection<Baggage> flightBaggageDTOs) {
		String groupID = null;

		for (Baggage flightBaggageDTO : flightBaggageDTOs) {
			groupID = flightBaggageDTO.getOndGroupId();
			break;
		}

		return groupID;
	}

	private Map<Integer, FlightSegmentTO> getFltSegIdFltSegments(List<FlightSegmentTO> list, boolean isAdd) {
		Map<Integer, FlightSegmentTO> map = new HashMap<Integer, FlightSegmentTO>();
		for (FlightSegmentTO fltSeg : list) {
			if (isAdd && fltSeg.getBaggageONDGroupId() != null) {
				map.put(fltSeg.getFlightSegId(), fltSeg);
			} else if (!isAdd && fltSeg.getBaggageONDGroupId() == null) {
				map.put(fltSeg.getFlightSegId(), fltSeg);
			}
		}
		return map;
	}

	private Object[] getSegmentWiseCount(Map<Integer, List<FlightBaggageDTO>> baggageMap,
			List<FlightSegmentTO> flightSegmentTOs) {

		String defaultCarrierCode = AppSysParamsUtil.getDefaultCarrierCode();
		List<FlightSegmentTO> flightSegmentTOsTemp = new ArrayList<FlightSegmentTO>();
		for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
			if (defaultCarrierCode.equals(flightSegmentTO.getOperatingAirline())) {
				flightSegmentTOsTemp.add(flightSegmentTO);
			}
		}
		flightSegmentTOs = flightSegmentTOsTemp;
		Collections.sort(flightSegmentTOs);

		Map<String, Map<String, FlightSegmentTO>> flightSegsBaggageAdd = new HashMap<String, Map<String, FlightSegmentTO>>();
		Map<String, Integer> segCodeWiseCount = new HashMap<String, Integer>();

		for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
			if (flightSegmentTO.getBaggageONDGroupId() != null) {
				if (!flightSegsBaggageAdd.containsKey(flightSegmentTO.getBaggageONDGroupId())) {
					flightSegsBaggageAdd.put(flightSegmentTO.getBaggageONDGroupId(), new HashMap<String, FlightSegmentTO>());
				}
				flightSegsBaggageAdd.get(flightSegmentTO.getBaggageONDGroupId()).put(flightSegmentTO.getSegmentCode(),
						flightSegmentTO);
			}
		}

		Map<Integer, Integer> fltSegIdWiseCount1 = new HashMap<Integer, Integer>();
		Map<Integer, Integer> fltSegIdWiseCount2 = new HashMap<Integer, Integer>();
		for (String baggageOndGrpId : flightSegsBaggageAdd.keySet()) {
			for (String segCode : flightSegsBaggageAdd.get(baggageOndGrpId).keySet()) {
				fltSegIdWiseCount1.put(flightSegsBaggageAdd.get(baggageOndGrpId).get(segCode).getFlightSegId(),
						flightSegsBaggageAdd.get(baggageOndGrpId).size());
				segCodeWiseCount.put(flightSegsBaggageAdd.get(baggageOndGrpId).get(segCode).getSegmentCode(),
						flightSegsBaggageAdd.get(baggageOndGrpId).size());
			}
		}

		for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
			if (flightSegmentTO.getBaggageONDGroupId() == null) {
				if (segCodeWiseCount.containsKey(flightSegmentTO.getSegmentCode())) {
					fltSegIdWiseCount2.put(flightSegmentTO.getFlightSegId(),
							segCodeWiseCount.get(flightSegmentTO.getSegmentCode()));
				} else {
					fltSegIdWiseCount2.put(flightSegmentTO.getFlightSegId(), 1);
				}
			}
		}

		return new Object[] { fltSegIdWiseCount1, fltSegIdWiseCount2 };
	}

	private String getONDBaggageGroupID(Integer segmentID, List<FlightSegmentTO> flightSegmentTOs) {
		for (FlightSegmentTO seg : flightSegmentTOs) {
			if (seg.getFlightSegId().equals(segmentID)) {
				return seg.getBaggageONDGroupId();
			}
		}
		return null;
	}

	private Map<Integer, String> getFltSegIdWiseOperatingCarrier(List<FlightSegmentTO> flightSegmentTOs) {
		Map<Integer, String> fltSegIdWiseOperatingCarrier = new HashMap<Integer, String>();

		for (FlightSegmentTO flightSegmentTO : flightSegmentTOs) {
			fltSegIdWiseOperatingCarrier.put(flightSegmentTO.getFlightSegId(), flightSegmentTO.getOperatingAirline());
		}

		return fltSegIdWiseOperatingCarrier;
	}

	private List<BookingFlightSegment> getFlightSegmentTOWithExistingOndGroupPreserved(List<SelectedBaggage> selectedBaggages) {
		return getFlightSegmentTOs(selectedBaggages, true);
	}

	private List<BookingFlightSegment> getFlightSegmentTOWithRemoveBaggages(List<SelectedBaggage> selectedBaggages) {
		return getFlightSegmentTOs(selectedBaggages, false);
	}

	private List<BookingFlightSegment> getFlightSegmentTOs(List<SelectedBaggage> selectedBaggages,
			boolean mergeAndPreserveOndBaggageId) {
		Map<String, BookingFlightSegment> sameBookingFlightSegment = new HashMap<String, BookingFlightSegment>();

		for (SelectedBaggage selectedBaggage : selectedBaggages) {
			BookingFlightSegment bkgFlightSeg = selectedBaggage.getFlightSegment();

			String key = composeUniqueKey(bkgFlightSeg, mergeAndPreserveOndBaggageId);
			if (!sameBookingFlightSegment.containsKey(key)) {
				sameBookingFlightSegment.put(key, bkgFlightSeg);
			} else {
				if (!hasBaggageOndGroupId(sameBookingFlightSegment.get(key)) && hasBaggageOndGroupId(bkgFlightSeg)) {
					sameBookingFlightSegment.put(key, bkgFlightSeg);
				}
			}

			if (selectedBaggage.getExternalFlightSegments().size() > 0) {
				for (BookingFlightSegment externalBookingFlightSegment : selectedBaggage.getExternalFlightSegments()) {
					key = composeUniqueKey(externalBookingFlightSegment, mergeAndPreserveOndBaggageId);
					if (!sameBookingFlightSegment.containsKey(key)) {
						sameBookingFlightSegment.put(key, externalBookingFlightSegment);
					} else {
						if (!hasBaggageOndGroupId(sameBookingFlightSegment.get(key))
								&& hasBaggageOndGroupId(externalBookingFlightSegment)) {
							sameBookingFlightSegment.put(key, externalBookingFlightSegment);
						}
					}
				}
			}
		}

		return new ArrayList<BookingFlightSegment>(sameBookingFlightSegment.values());
	}

	private boolean hasBaggageOndGroupId(BookingFlightSegment bookingFlightSegment) {
		return (bookingFlightSegment != null && bookingFlightSegment.getBaggageONDGroupId() != null
				&& !"".equals(bookingFlightSegment.getBaggageONDGroupId()));
	}

	private String composeUniqueKey(BookingFlightSegment bookingFlightSegment, boolean mergeAndPreserveOndBaggageId) {
		if (mergeAndPreserveOndBaggageId) {
			return BeanUtils.nullHandler(bookingFlightSegment.getOperatingAirline())
					+ BeanUtils.nullHandler(bookingFlightSegment.getFlightRefNumber());
		} else {
			return BeanUtils.nullHandler(bookingFlightSegment.getOperatingAirline())
					+ BeanUtils.nullHandler(bookingFlightSegment.getFlightRefNumber())
					+ BeanUtils.nullHandler(bookingFlightSegment.getBaggageONDGroupId());
		}
	}

	private Map<Integer, ClassOfServiceDTO> getFlightSegmentIdWiseCos(List<SelectedBaggage> selectedBaggages) {
		Map<Integer, ClassOfServiceDTO> flightSegIdWiseCos = new HashMap<Integer, ClassOfServiceDTO>();

		for (SelectedBaggage selectedBaggage : selectedBaggages) {
			BookingFlightSegment bkgFlightSeg = selectedBaggage.getFlightSegment();
			ClassOfServiceDTO cosDto = new ClassOfServiceDTO(bkgFlightSeg.getCabinClassCode(),
					bkgFlightSeg.getLogicalCabinClassCode(), bkgFlightSeg.getSegmentCode());
			flightSegIdWiseCos.put(new Integer(bkgFlightSeg.getFlightRefNumber()), cosDto);
		}

		return flightSegIdWiseCos;
	}

	private Map<Integer, Integer> getFlightSegmentWiseTemplateIDs(List<SelectedBaggage> selectedBaggages) throws ModuleException {
		Map<Integer, Integer> flightSegWiseTemplateIDs = new HashMap<Integer, Integer>();
		boolean isBundledFareSelected = false;

		Map<String, BundledFareDTO> segmentBundledFareMap = AncillaryUtil
				.getSegmentCodeWiseBundledFares(transactionalReservationProcess, pnr, null);
		if (segmentBundledFareMap != null && !segmentBundledFareMap.isEmpty()) {
			for (SelectedBaggage selectedBaggage : selectedBaggages) {
				BookingFlightSegment bkgFlightSeg = selectedBaggage.getFlightSegment();
				if (segmentBundledFareMap.containsKey(bkgFlightSeg.getSegmentCode())) {
					isBundledFareSelected = true;
					BundledFareDTO bundledFareDTO = segmentBundledFareMap.get(bkgFlightSeg.getSegmentCode());
					if (bundledFareDTO != null) {
						flightSegWiseTemplateIDs.put(new Integer(bkgFlightSeg.getFlightRefNumber()),
								bundledFareDTO.getApplicableServiceTemplateId(EXTERNAL_CHARGES.BAGGAGE.toString()));
					}
				}
			}
		}

		// If bundled fare is not selected, then check for any applied anci offers
		if (!isBundledFareSelected) {
			for (SelectedBaggage selectedBaggage : selectedBaggages) {
				BookingFlightSegment bkgFlightSeg = selectedBaggage.getFlightSegment();

				flightSegWiseTemplateIDs.put(new Integer(bkgFlightSeg.getFlightRefNumber()),
						selectedBaggage.getOfferedAnciTemplateID());
			}
		}

		return flightSegWiseTemplateIDs;
	}

	@Override
	public void setTransactionalReservationProcess(TransactionalReservationProcess transactionalReservationProcess) {
		this.transactionalReservationProcess = transactionalReservationProcess;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}
}
