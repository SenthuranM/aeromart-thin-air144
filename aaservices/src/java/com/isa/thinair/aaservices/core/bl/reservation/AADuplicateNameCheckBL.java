/**
 * 
 */
package com.isa.thinair.aaservices.core.bl.reservation;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightSegment;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaxNameInfo;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AADuplicateNameCheckRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AADuplicateNameCheckRS;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.airproxy.api.model.reservation.commons.FlightSegmentTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.ProxyConstants.SYSTEM;
import com.isa.thinair.airproxy.api.model.reservation.core.DuplicateValidatorAssembler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientReservationPax;
import com.isa.thinair.commons.api.exception.ModuleException;


/**
 * @author suneth
 *
 */
public class AADuplicateNameCheckBL {
	
	private static Log log = LogFactory.getLog(AADuplicateNameCheckBL.class);

	private AADuplicateNameCheckRQ aaDuplicateNameCheckRQ;

	public AADuplicateNameCheckBL(AADuplicateNameCheckRQ aaDuplicateNameCheckRQ){

		this.aaDuplicateNameCheckRQ = aaDuplicateNameCheckRQ;
		
	}
	
	public AADuplicateNameCheckRS execute(){
		
		boolean hasDuplicate = false;
		List<FlightSegmentTO> flightSegmentTOs = new ArrayList<FlightSegmentTO>();
		List<LCCClientReservationPax> paxList = new ArrayList<LCCClientReservationPax>();
		
		for(FlightSegment fltSeg : aaDuplicateNameCheckRQ.getFlightSegment()){
			
			FlightSegmentTO fltSegTO = new FlightSegmentTO();
			fltSegTO.setFlightRefNumber(fltSeg.getFlightRefNumber());
			fltSegTO.setFlightNumber(fltSeg.getFlightNumber());
			fltSegTO.setOperatingAirline(fltSeg.getOperatingAirline());
			
			flightSegmentTOs.add(fltSegTO);
			
		}
		
		for(PaxNameInfo paxNameInfo : aaDuplicateNameCheckRQ.getReservationPax()){
			
			LCCClientReservationPax resPax = new LCCClientReservationPax();
			resPax.setTitle(paxNameInfo.getTitle());
			resPax.setFirstName(paxNameInfo.getFirstName());
			resPax.setLastName(paxNameInfo.getLastName());
			resPax.setPaxType(paxNameInfo.getPaxReference());
			
			paxList.add(resPax);
			
		}
		
		DuplicateValidatorAssembler dva = new DuplicateValidatorAssembler();
		dva.setTargetSystem(SYSTEM.getEnum("AA"));
		dva.setFlightSegments(flightSegmentTOs);
		dva.setPaxList(paxList);
		
		try {
			hasDuplicate = AAServicesModuleUtils.getAirproxyReservationBD().checkForDuplicates(dva, null, null);
		} catch (ModuleException e) {
			log.error("AADuplicateNameCheckBL.execute => ", e);
		}
		
		AADuplicateNameCheckRS aaDuplicateNameCheckRS = new AADuplicateNameCheckRS();

		aaDuplicateNameCheckRS.setHasDuplicates(hasDuplicate);
		
		return aaDuplicateNameCheckRS;
		
	}

}
