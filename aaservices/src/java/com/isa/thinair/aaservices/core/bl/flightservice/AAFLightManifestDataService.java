package com.isa.thinair.aaservices.core.bl.flightservice;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.FlightManifestOptionsInfo;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAFlightManifestDataRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAFlightManifestDataRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.core.session.AASessionManager;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.airreservation.api.dto.flightmanifest.FlightManifestOptionsDTO;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;

public class AAFLightManifestDataService {
	private static final Log log = LogFactory.getLog(AAFLightManifestDataService.class);

	private AAFlightManifestDataRQ aaFlightManifestDataRQ;

	private AAFlightManifestDataRS aaFlightManifestDataRS;

	public void setAAFlightManifestDataRQ(AAFlightManifestDataRQ aaFlightManifestDataRQ) {
		this.aaFlightManifestDataRQ = aaFlightManifestDataRQ;
	}

	private void intilaizeAAFlightManifestDataRS() {
		aaFlightManifestDataRS = new AAFlightManifestDataRS();
		aaFlightManifestDataRS.setResponseAttributes(new AAResponseAttributes());
	}

	public AAFlightManifestDataRS execute() {
		intilaizeAAFlightManifestDataRS();

		try {
			FlightManifestOptionsDTO flightManifestOptionsDTO = composeFlightManifestOptions(aaFlightManifestDataRQ
					.getManifestOptions());

			String responseHTML = null;
			boolean emailResponse = false;
			boolean emailSuccess = false;
			if (!flightManifestOptionsDTO.isEmail()) {
				responseHTML = AAServicesModuleUtils.getReservationQueryBD().viewFlightManifest(flightManifestOptionsDTO);
			} else {
				emailResponse = true;
				Boolean responseEmail = AAServicesModuleUtils.getReservationQueryBD().sendFlightManifestAsEmail(
						flightManifestOptionsDTO);
				if (responseEmail != null) {
					emailSuccess = responseEmail;
				}
			}
			aaFlightManifestDataRS.setResponseHTML(responseHTML);
			aaFlightManifestDataRS.setEmailResponse(emailResponse);
			aaFlightManifestDataRS.setEmailSuccess(emailSuccess);

		} catch (Exception ex) {
			log.error("Error occurred during flight manifest data in given options ", ex);
			AAExceptionUtil.addAAErrrors(aaFlightManifestDataRS.getResponseAttributes().getErrors(), ex);
		} finally {
			int numOfErrors = aaFlightManifestDataRS.getResponseAttributes().getErrors().size();
			AASuccess successType = (numOfErrors == 0 ? new AASuccess() : null);
			aaFlightManifestDataRS.getResponseAttributes().setSuccess(successType);
		}

		return aaFlightManifestDataRS;
	}

	private FlightManifestOptionsDTO composeFlightManifestOptions(FlightManifestOptionsInfo info) {
		FlightManifestOptionsDTO dto = new FlightManifestOptionsDTO();

		dto.setCabinClass(info.getCabinClass());
		dto.setLogicalCabinClass(info.getLogicalCabinClass());
		dto.setCheckInwardConnections(info.isCheckInwardConnections());
		dto.setCheckOutwardConnections(info.isCheckOutwardConnections());
		dto.setConfirmedReservation(info.isConfirmedReservation());
		dto.setEmail(info.isIsEmail());
		dto.setEmailId(info.getEmailId());
		dto.setFilterByCabinClass(info.isFilterByCabinClass());
		dto.setFilterByLogicalCabinClass(info.isFilterByLogicalCabinClass());
		dto.setFlightDate(info.getFlightDate());
		dto.setFlightId(info.getFlightId());
		dto.setFlightNo(info.getFlightNo());
		dto.setInwardBoardingAirport(info.getInwardBoardingAirport());
		dto.setInwardConnectionOrigin(info.getInwardConnectionOrigin());
		dto.setInwardFlightNo(info.getInwardFlightNo());
		dto.setMaxInwardConnectionTime(info.getMaxInwardConnectionTime());
		dto.setMaxOutwardConnectionTime(info.getMaxOutwardConnectionTime());
		dto.setOutwardBoardingAirport(info.getOutwardConnectionDestination());
		dto.setOutwardConnectionDestination(info.getOutwardConnectionDestination());
		dto.setOutwardFlightNo(info.getOutwardFlightNo());
		dto.setRoute(info.getRoute());
		dto.setShowAirportServices(info.isShowAirportServices());
		dto.setShowCreditCardDetails(info.isShowCreditCardDetails());
		dto.setShowMeal(info.isShowMeal());
		dto.setShowPaxIndentification(info.isShowPaxIndentification());
		dto.setShowSeatMap(info.isShowSeatMap());
		dto.setShowSsr(info.isShowSsr());
		dto.setSortOption(info.getSortOption());
		dto.setSummaryMode(info.getSummaryMode());
		dto.setSummaryPosition(info.getSummaryPosition());
		dto.setViewOption(info.getViewOption());
		dto.setLoadByPnr(info.isLoadByPnr());
		dto.setUserPrincipal(AASessionManager.getInstance().getCurrentUserPrincipal());
		dto.setSearchTriggerdByDifferentCarrier(info.isSearchTriggerdByDifferentCarrier());
		dto.setDataSourceMode(info.isDataFromReporting() ? ReportsSearchCriteria.DATASOURCE_TYPE_RPT
				: ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
		dto.setPrivilegedToShowAllBookings(AASessionManager.getUserPrivileges().contains(
				PriviledgeConstants.ALLOW_SHOW_ALL_BOOKINGS_IN_FLIGHT_MANIFEST));
		return dto;
	}
}
