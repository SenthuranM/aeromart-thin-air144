package com.isa.thinair.aaservices.core.bl.reservation;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAUserNoteRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAUserNoteRS;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.TransformerUtil;
import com.isa.thinair.airproxy.api.model.reservation.commons.UserNoteTO;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * @author priyantha
 */
public class AAAddUserNoteBL {

	private static Log log = LogFactory.getLog(AAAddUserNoteBL.class);

	private final AAUserNoteRQ aaUserNoteRQ;

	public AAAddUserNoteBL(AAUserNoteRQ aaUserNoteRQ) {
		this.aaUserNoteRQ = aaUserNoteRQ;

	}

	public AAUserNoteRS execute() {
		AAUserNoteRS aaUserNoteRS = new AAUserNoteRS();
		aaUserNoteRS.setResponseAttributes(new AAResponseAttributes());
		try {

			if (log.isDebugEnabled()) {
				log.debug("Going to add user note to " + AppSysParamsUtil.getDefaultCarrierCode() + "pnr   : "
						+ aaUserNoteRQ.getUserNote().getPnr());
			}

			TrackInfoDTO trackInfoDTO = TransformerUtil.transform(aaUserNoteRQ.getTrackInfo());
			UserNoteTO userNoteTO = TransformerUtil.transform(aaUserNoteRQ.getUserNote());
			AAServicesModuleUtils.getReservationQueryBD().addUserNote(userNoteTO, trackInfoDTO);
			aaUserNoteRS.setUserNote(aaUserNoteRQ.getUserNote());

			aaUserNoteRS.getResponseAttributes().setSuccess(new AASuccess());

		} catch (Exception e) {
			log.error("AddUserNote(AAUserNoteRQ) failed.", e);
			AAExceptionUtil.addAAErrrors(aaUserNoteRS.getResponseAttributes().getErrors(), e);
		}

		return aaUserNoteRS;
	}
}
