package com.isa.thinair.aaservices.core.session;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.required.dto.AAWarningCode;
import com.isa.thinair.aaservices.api.exception.WebservicesException;
import com.isa.thinair.aaservices.core.transaction.TransactionalReservationProcess;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.WebservicesContext;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserDST;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.login.util.DatabaseUtil;
import com.isa.thinair.webplatform.api.util.PrivilegeUtil;

/**
 * Manages user sessions
 * 
 * @author Nasly
 */
public class AASessionManager {

	private static final Log log = LogFactory.getLog(AASessionManager.class);

	private static final Log txnLogger = LogFactory.getLog("aaservices.txn.log");

	private static AASessionManager instance = null;

	private Map<String, Object> userSessions = Collections.synchronizedMap(new HashMap<String, Object>());

	private Object lock = new Object();

	/** UserPrincipal bound to current thread */
	private static ThreadLocal<UserPrincipal> threadLocalUserPrincipal = new ThreadLocal<UserPrincipal>();

	/**
	 * WebserviceContext bound to current thread Used for passing parameters
	 * across different methods during current thread processing
	 */
	private static ThreadLocal<WebservicesContext> threadLocalWSContext = new ThreadLocal<WebservicesContext>();

	public static AASessionManager getInstance() {
		if (instance == null) {
			synchronized (AASessionManager.class) {
				if (instance == null) {
					instance = new AASessionManager();
				}
			}
		}
		return instance;
	}

	/**
	 * Get user session; null if no session exists for user yet
	 * 
	 * @param userID
	 * @return AAUserSession
	 */
	private AAUserSession getUserSession(String userID, String sessionID) {
		return (AAUserSession) userSessions.get(getUserKey(userID, sessionID));
	}

	private String getUserKey(String userID, String sessionID) {
		if (sessionID == null) {
			return userID;
		} else {
			return userID + "_" + sessionID;
		}
	}

	/**
	 * Create and initialize a new user session
	 * 
	 * @param userID
	 * @param password
	 * @return
	 * @throws WebservicesException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private AAUserSession createNewUserSession(String userID, String password, int salesChannelId, String sessionID)
			throws WebservicesException {
		AAUserSession aaUserSession = new AAUserSession();
		Collection privilegeKeys = new HashSet();
		try {
			/** Initialize User Data */
			User user = AAServicesModuleUtils.getSecurityBD().getUser(userID);
			Agent agent = AAServicesModuleUtils.getTravelAgentBD().getAgent(user.getAgentCode());

			// Calling from login module
			Collection<UserDST> colUserDST = DatabaseUtil.getAgentLocalTime(agent.getStationCode());
			Collection userCarriers = DatabaseUtil.getCarrierCodes(userID);
			// Setting salesChannel agents and loginType to -1
			UserPrincipal userPrincipal = (UserPrincipal) UserPrincipal.createIdentity(userID, salesChannelId,
					user.getAgentCode(), agent.getStationCode(), null, userID, colUserDST, password, userCarriers,
					user.getDefaultCarrierCode(), user.getAirlineCode(), agent.getAgentTypeCode(),
					agent.getCurrencyCode(), null, null, null, sessionID);

			// Storing data in user session
			aaUserSession.addParameter(AAUserSession.USER_PRINCIPAL, userPrincipal);

			privilegeKeys = PrivilegeUtil.getXBEAndAdminUserPrivileges(user, userPrincipal).keySet();
			aaUserSession.addParameter(AAUserSession.USER_PRIVILEGES_KEYS, privilegeKeys);

			userSessions.put(getUserKey(userID, sessionID), aaUserSession);
		} catch (ModuleException me) {
			log.error("User session initialization failed", me);
			throw new WebservicesException(me);
		}

		if (log.isDebugEnabled()) {
			log.debug("AASessionManager:: created new user session [userId=" + userID + "]");
		}

		return aaUserSession;
	}

	/**
	 * Set current UserPrincipal
	 * 
	 * @param currentUserPrincipal
	 */
	public void setCurrentUserPrincipal(UserPrincipal currentUserPrincipal) {
		threadLocalUserPrincipal.set(currentUserPrincipal);
	}

	/**
	 * Returns current UserPrincipal
	 * 
	 * @return
	 */
	public UserPrincipal getCurrentUserPrincipal() {
		return threadLocalUserPrincipal.get();
	}

	/**
	 * Unset current UserPrincipal NOTE: MUST make sure to call this before each
	 * service thread completes
	 */
	private void unsetCurrentUserPrincipal() {
		threadLocalUserPrincipal.set(null);
	}

	/**
	 * Set current WebservicesContext
	 * 
	 * @param currentWebservicesContext
	 */
	public void setCurrentWSContext(WebservicesContext currentWebservicesContext) {
		threadLocalWSContext.set(currentWebservicesContext);
	}

	/**
	 * Returns current WebservicesContext
	 * 
	 * @return
	 */
	public WebservicesContext getCurrentWSContext() {
		return threadLocalWSContext.get();
	}

	/**
	 * Unset current WebservicesContext NOTE: MUST make sure to call this before
	 * each service thread completes
	 */
	private void unsetCurrentWSContext() {
		threadLocalWSContext.set(null);
	}

	/**
	 * Returns parameter value from UserSession
	 * 
	 * @param userId
	 * @param key
	 * @return
	 */

	private AAUserSession getCurrentUserSession() {
		String userId = null;
		String sessionId = null;
		if (threadLocalUserPrincipal.get() != null) {
			userId = threadLocalUserPrincipal.get().getUserId();
			sessionId = threadLocalUserPrincipal.get().getSessionId();
		} else {
			// TODO handle
		}
		AAUserSession aaUserSession = getUserSession(userId, sessionId);
		return aaUserSession;
	}

	public <T> T getCurrUserSessionParam(String key) {
		Object paramValue = null;
		AAUserSession aaUserSession = getCurrentUserSession();
		if (aaUserSession != null) {
			paramValue = aaUserSession.getParameter(key);
		} else {
			// TODO handle
		}
		return (T) paramValue;
	}

	/**
	 * Add parameter in UserSession
	 * 
	 * @param userId
	 * @param key
	 * @param value
	 */
	private void setCurrUserSessionParam(String key, Object value) {
		AAUserSession aaUserSession = getCurrentUserSession();
		if (aaUserSession != null) {
			aaUserSession.addParameter(key, value);
		} else {
			// TODO
		}
	}

	@SuppressWarnings("unchecked")
	public void addCurrentUserTransaction(String tnxId, TransactionalReservationProcess value, boolean isActualUser) {
		Set<String> tnxIdSet = (Set<String>) getCurrUserSessionParam(AAUserSession.TXN_IDS_SESSION_KEY);
		if (tnxIdSet == null) {
			tnxIdSet = Collections.synchronizedSet(new HashSet<String>());
			setCurrUserSessionParam(AAUserSession.TXN_IDS_SESSION_KEY, tnxIdSet);
		} else if (isActualUser) {
			for (String key : tnxIdSet) {
				removeCurrUserSessionParam(key);
			}
			tnxIdSet.clear();
		}
		if (txnLogger.isDebugEnabled()) {
			UserPrincipal up = getCurrentUserPrincipal();
			txnLogger.debug("User:" + (up != null ? up.getUserId() : "null") + ",tnxIdSetSize:" + tnxIdSet.size());
		}
		tnxIdSet.add(tnxId);
		setCurrUserSessionParam(tnxId, value);
	}

	public TransactionalReservationProcess getCurrentUserTransaction(String tnxId) {
		TransactionalReservationProcess tnxPros = (TransactionalReservationProcess) getCurrUserSessionParam(tnxId);
		if (tnxPros != null) {
			tnxPros.updateLastAccessed();
		}
		return tnxPros;
	}

	/**
	 * Remove parameter value from UserSession
	 * 
	 * @param userId
	 * @param key
	 * @return
	 */
	private Object removeCurrUserSessionParam(String key) {
		Object paramValue = null;
		AAUserSession aaUserSession = getCurrentUserSession();
		if (aaUserSession != null) {
			paramValue = aaUserSession.removeParameter(key);
		}
		return paramValue;
	}

	/**
	 * Returns all the privileges the current user has.
	 * 
	 * @return
	 */

	@SuppressWarnings("unchecked")
	public static Collection<String> getUserPrivileges() {
		return (Collection<String>) getInstance().getCurrUserSessionParam(AAUserSession.USER_PRIVILEGES_KEYS);
	}

	/**
	 * Removes expired user sessions TODO - test
	 */
	@SuppressWarnings("rawtypes")
	void removeExpiredUserSessions() {
		log.debug("Begin user sessions expiration [Timestamp=" + new Date() + "]");
		Collection<String> expiredSessionIDs = new ArrayList<String>();
		AAUserSession session = null;
		String sessionID = null;
		synchronized (userSessions) {
			for (Iterator sessionKeysIt = userSessions.keySet().iterator(); sessionKeysIt.hasNext();) {
				sessionID = (String) sessionKeysIt.next();
				session = (AAUserSession) userSessions.get(sessionID);
				if (session.qualifyForExpiration(
						AAServicesModuleUtils.getWebServicesConfig().getUserSessTimeoutInMills())) {
					expiredSessionIDs.add(sessionID);
				}
			}
			if (expiredSessionIDs.size() > 0) {
				for (String id : expiredSessionIDs) {
					if (log.isDebugEnabled()) {
						log.debug("Removing expired session [SessionID=" + id + "]");
					}
					userSessions.remove(id);
				}
			}
		}

		log.debug("End user sessions expiration [Timestamp=" + new Date() + "]");
	}

	public void removeExpiredUserTransactions() {
		log.debug("Begin user transaction expiration [Timestamp=" + new Date() + "]");
		AAUserSession session = null;
		String sessionID = null;
		for (Iterator<String> sessionKeysIt = userSessions.keySet().iterator(); sessionKeysIt.hasNext();) {
			sessionID = sessionKeysIt.next();
			session = (AAUserSession) userSessions.get(sessionID);
			removeUserWiseTransactions(sessionID, session);
		}

		log.debug("End user transaction expiration [Timestamp=" + new Date() + "]");
	}

	@SuppressWarnings("unchecked")
	private void removeUserWiseTransactions(String sessionID, AAUserSession session) {
		Long startTimestamp = System.currentTimeMillis();
		int countAtStart = 0;
		int clearedCount = 0;
		int finalCount = 0;
		try {
			synchronized (lock) {
				Set<String> tnxIdSet = (Set<String>) session.getParameter(AAUserSession.TXN_IDS_SESSION_KEY);
				if (tnxIdSet != null && tnxIdSet.size() > 0) {
					tnxIdSet = new HashSet<String>(tnxIdSet);
					countAtStart = tnxIdSet.size();
					if (log.isInfoEnabled()) {
						log.info("###AASERV### Begin AAService tnx cleanup [SessionID=" + sessionID + ", Timestamp="
								+ CommonsConstants.DATE_FORMAT_FOR_LOGGING.format(new Date(System.currentTimeMillis()))
								+ ", TotalTnxs=" + tnxIdSet.size() + "]");
					}

					Set<String> tnxIdSetRemoved = new HashSet<String>();
					for (String tnxId : tnxIdSet) {
						TransactionalReservationProcess tnxPro = (TransactionalReservationProcess) session
								.getParameter(tnxId);
						if (tnxPro.qualifyForExpiration(
								AAServicesModuleUtils.getWebServicesConfig().getTxnTimeoutInMillis())) {
							session.removeParameter(tnxId);
							if (log.isInfoEnabled()) {
								log.info("###AASERV### Removing expired tnxs for user sessionID : " + sessionID
										+ " [TnxId=" + tnxId + ", lastAccessBefore(secs)="
										+ (System.currentTimeMillis() - tnxPro.getLastAccessedTimestamp()) / 1000
										+ "]");
							}
							tnxIdSetRemoved.add(tnxId);
						} else {
							if (log.isDebugEnabled()) {
								log.debug("###AASERV### Non expired tnxs for user sessionID : " + sessionID + " [TnxId="
										+ tnxId + ", lastAccessBefore(secs)="
										+ (System.currentTimeMillis() - tnxPro.getLastAccessedTimestamp()) / 1000
										+ "]");
							}
						}

					}
					clearedCount = tnxIdSetRemoved.size();
					((Set<String>) session.getParameter(AAUserSession.TXN_IDS_SESSION_KEY)).removeAll(tnxIdSetRemoved);
					finalCount = ((Set<String>) session.getParameter(AAUserSession.TXN_IDS_SESSION_KEY)).size();
				} else {
					if (log.isInfoEnabled()) {
						log.info("###AASERV### Tnx set is empty [SessionID=" + sessionID + ", Timestamp="
								+ CommonsConstants.DATE_FORMAT_FOR_LOGGING
										.format(new Date(System.currentTimeMillis())));
					}
				}
			}
		} finally {
			log.info("###AASERV### End AAService tnx cleanup [SessionID=" + sessionID + ", Time Spent (seconds)="
					+ (System.currentTimeMillis() - startTimestamp) / 1000 + ", Timestamp="
					+ CommonsConstants.DATE_FORMAT_FOR_LOGGING.format(new Date(System.currentTimeMillis()))
					+ ", Start Count = " + countAtStart + ", New Count = " + finalCount + ", Cleared count = "
					+ clearedCount + "]");
		}

	}

	/**
	 * Removes the user session for the provided user id.
	 * 
	 * @param userId
	 *            : The user id of the user whose AASession object to be
	 *            removed.
	 * @param sessionId
	 * 
	 * @return : The removed session object if there was a matching session.
	 *         <tt>null<tt> otherwise.
	 */
	public Object removeUserSession(String userId, String sessionId) {
		Object removedObj = null;
		if (StringUtils.isNotEmpty(userId)) {
			synchronized (userSessions) {
				removedObj = userSessions.remove(getUserKey(userId, sessionId));
			}
		}

		return removedObj;
	}

	/**
	 * Handle user session initialization before service call.
	 * 
	 * @author Nasly
	 */
	public static class SessionInHandler {

		/**
		 * Initialize UserSession, if not already exists.
		 * 
		 * @param userId
		 * @param password
		 * @param salesChannelId
		 * @param skipAuthentication
		 * @throws WebservicesException
		 */
		public static void handleSessionIn(String userId, String password, int salesChannelId, String sessionID,
				boolean skipAuthentication) throws WebservicesException {
			log.debug("SessionInHandler::handleSessionIn called");
			AAUserSession aaUserSession = null;
			if (!skipAuthentication && !isAuthenticatedInSession(userId, password, salesChannelId, sessionID)) {
				aaUserSession = AASessionManager.getInstance().createNewUserSession(userId, password, salesChannelId,
						sessionID);
			} else {
				aaUserSession = AASessionManager.getInstance().getUserSession(userId, sessionID);
			}
			// Bind the current UserPrincipal to the current thread
			AASessionManager.getInstance()
					.setCurrentUserPrincipal((UserPrincipal) aaUserSession.getParameter(AAUserSession.USER_PRINCIPAL));
		}

		/**
		 * Checks if User is already authenticated. Throws WebservicesException
		 * if user is already authenticated and UserSession credentials does not
		 * match with request credentials.
		 * 
		 * @param userName
		 * @param password
		 * @param salesChannelId
		 * @return
		 * @throws WebservicesException
		 */
		private static boolean isAuthenticatedInSession(String userName, String password, int salesChannelId,
				String sessionId) throws WebservicesException {
			boolean authenticated = false;
			AAUserSession aaUserSession = AASessionManager.getInstance().getUserSession(userName, sessionId);
			if (aaUserSession != null) {
				UserPrincipal userPrincipal = (UserPrincipal) aaUserSession.getParameter(AAUserSession.USER_PRINCIPAL);

				if (userName.equals(userPrincipal.getUserId()) && password.equals(userPrincipal.getPassword())
						&& salesChannelId == userPrincipal.getSalesChannel()) {
					authenticated = true;
				} else {
					log.error("Authenticated username/password does not match with supplied credentials " + "[userId="
							+ userName + ", sessionId=" + sessionId + "]");
					throw new WebservicesException(AAWarningCode.EWT_1_MAXICO_REQUIRED_AUTHENTICATION_ERROR,
							"Authenticated username/password does not match with supplied credentials " + "[userId="
									+ userName + ", sessionId=" + sessionId + "]");
				}
			}
			return authenticated;
		}
	}

	/**
	 * Handle user session after service call.
	 * 
	 * @author Nasly
	 */
	public static class SessionOutHandler {

		public static void handleSessionOut() {
			log.debug("SessionOutHandler::handleSessionOut called");
			try {
				// Unset the UserPrincipal from the current thread
				AASessionManager.getInstance().unsetCurrentUserPrincipal();
			} catch (Exception ex) {
				log.error("Failure in SessionOutHandler", ex);
			}

			try {
				// Unset the WebservicesContext from the current thread
				AASessionManager.getInstance().unsetCurrentWSContext();
			} catch (Exception ex) {
				log.error("Failure in SessionOutHandler", ex);
			}
		}
	}

}
