package com.isa.thinair.aaservices.core.bl.lms;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.connectivity.profiles.maxico.v1.common.dto.DiscountInfo;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaxProductAmounts;
import com.isa.connectivity.profiles.maxico.v1.common.dto.PaxProductPoints;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AALoyaltyRedeemRQ;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AALoyaltyRedeemRS;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAResponseAttributes;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AASuccess;
import com.isa.thinair.aaservices.core.transaction.TransactionalReservationProcess;
import com.isa.thinair.aaservices.core.transaction.TransactionalReservationProcessBL;
import com.isa.thinair.aaservices.core.util.AAExceptionUtil;
import com.isa.thinair.aaservices.core.util.AAServicesModuleUtils;
import com.isa.thinair.aaservices.core.util.AAServicesTransformUtil;
import com.isa.thinair.aaservices.core.util.TransformerUtil;
import com.isa.thinair.aircustomer.api.model.LmsMember;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airproxy.api.model.reservation.commons.DiscountedFareDetails;
import com.isa.thinair.airproxy.api.model.reservation.commons.FareSegChargeTO;
import com.isa.thinair.airproxy.api.model.reservation.core.DiscountRQ;
import com.isa.thinair.airproxy.api.model.reservation.core.DiscountRQ.DISCOUNT_METHOD;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientExternalChgDTO;
import com.isa.thinair.airproxy.api.utils.AirProxyReservationUtil;
import com.isa.thinair.airproxy.api.utils.AirproxyModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.ibe.core.service.ModuleServiceLocator;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.to.lms.RedeemCalculateReq;
import com.isa.thinair.promotion.api.to.lms.RedeemLoyaltyPointsReq;
import com.isa.thinair.promotion.api.utils.ResponceCodes;
import com.isa.thinair.webplatform.api.util.WebplatformUtil;

public class AALoyaltyRedeemBL implements TransactionalReservationProcessBL {

	private static Log log = LogFactory.getLog(AALoyaltyRedeemBL.class);

	private AALoyaltyRedeemRQ aaLoyaltyRedeemRQ;

	private AALoyaltyRedeemRS aaLoyaltyRedeemRS;

	private TransactionalReservationProcess transactionalReservationProcess;

	public AALoyaltyRedeemBL(AALoyaltyRedeemRQ aaLoyaltyRedeemRQ) {
		this.aaLoyaltyRedeemRQ = aaLoyaltyRedeemRQ;
		aaLoyaltyRedeemRS = new AALoyaltyRedeemRS();
		aaLoyaltyRedeemRS.setResponseAttributes(new AAResponseAttributes());
		aaLoyaltyRedeemRS.setHeaderInfo(aaLoyaltyRedeemRQ.getHeaderInfo());
		aaLoyaltyRedeemRS.getResponseAttributes().setSuccess(new AASuccess());
	}

	@SuppressWarnings("unchecked")
	public AALoyaltyRedeemRS execute() {

		try {

		List<FareSegChargeTO> fareSegChargeTOs = new ArrayList<FareSegChargeTO>();

			CurrencyExchangeRate currencyExchangeRate = AirproxyModuleUtils.getCommonMasterBD().getExchangeRate(
					AppSysParamsUtil.getLoyaltyManagedCurrencyCode(), CalendarUtil.getCurrentSystemTimeInZulu());

			BigDecimal pointToCurrencyConversionRate = AppSysParamsUtil.getLoyaltyPointsToCurrencyConversionRate();

			RedeemCalculateReq redeemCalculateReqTo = new RedeemCalculateReq();

			Double remainingPoints = null;
			redeemCalculateReqTo.setMemberAccountId(aaLoyaltyRedeemRQ.getMemberFFID());
			if (aaLoyaltyRedeemRQ.getRemainingPoint() != null) {
				remainingPoints = aaLoyaltyRedeemRQ.getRemainingPoint();
			}

			if (aaLoyaltyRedeemRQ.getRedeemRequestAmount() != null) {
				redeemCalculateReqTo.setRequestedLoyaltyPointsAmount(aaLoyaltyRedeemRQ.getRedeemRequestAmount());
			}

			redeemCalculateReqTo.setMemberAvailablePoints(remainingPoints);

			String carrierCode = AppSysParamsUtil.getDefaultCarrierCode();

			redeemCalculateReqTo.setLoyaltyPointsToCurrencyConversionRate(pointToCurrencyConversionRate);
			redeemCalculateReqTo.setCurrencyToLoyaltyPointsConversionRate(AppSysParamsUtil
					.getCurrencyToLoyaltyPointsConversionRate());
			redeemCalculateReqTo.setCurrencyExRate(currencyExchangeRate);
			redeemCalculateReqTo.setMemberAvailablePoints(remainingPoints);
			redeemCalculateReqTo.setIssueRewardIds(aaLoyaltyRedeemRQ.isIssueRewardIds());			
			redeemCalculateReqTo.setDiscountRQ(composeDiscountRQ(aaLoyaltyRedeemRQ.getDiscountInfo()));
			redeemCalculateReqTo.setFlexiQuote(aaLoyaltyRedeemRQ.isFlexiQuote());

			if (aaLoyaltyRedeemRQ.isIssueRewardIds()) {
				boolean syncSuccess = checkAndSyncLMSAccount(aaLoyaltyRedeemRQ.getAaPos().getMarketingAirLineCode(),
						aaLoyaltyRedeemRQ.getMemberFFID());
				if (!syncSuccess) {
					return aaLoyaltyRedeemRS;
				}
			}

			Map<String, TreeMap<String, List<LCCClientExternalChgDTO>>> paxCarrierExternalCharges = TransformerUtil
					.composePaxExternalChargeMap(aaLoyaltyRedeemRQ.getPaxExtChgMap());
			redeemCalculateReqTo.setPaxCarrierExternalCharges(paxCarrierExternalCharges);

			if (aaLoyaltyRedeemRQ.getSelectedFareTypes() != null && aaLoyaltyRedeemRQ.getSelectedFareTypes().size() > 0) {
				fareSegChargeTOs.addAll(transactionalReservationProcess.getFSCSet(aaLoyaltyRedeemRQ.getSelectedFareTypes()));

				if (aaLoyaltyRedeemRQ.getPerPaxProductDueAmounts() != null
						&& aaLoyaltyRedeemRQ.getPerPaxProductDueAmounts().size() > 0) {
					redeemCalculateReqTo.setOndFareDTOs(null);
					redeemCalculateReqTo.setPaxCarrierExternalCharges(null);

					Map<Integer, Map<String, Map<String, BigDecimal>>> paxCarrierWiseProductDueAmounts = new HashMap<Integer, Map<String, Map<String, BigDecimal>>>();
					for (PaxProductAmounts paxProductAmounts : aaLoyaltyRedeemRQ.getPerPaxProductDueAmounts()) {
						Map<String, Map<String, BigDecimal>> carrierProductDueAmounts = new HashMap<String, Map<String, BigDecimal>>();
						carrierProductDueAmounts.put(carrierCode,
								TransformerUtil.transformStringDecimalMap(paxProductAmounts.getValue()));
						paxCarrierWiseProductDueAmounts.put(paxProductAmounts.getKey(), carrierProductDueAmounts);
					}
					redeemCalculateReqTo.setPaxCarrierProductDueAmount(paxCarrierWiseProductDueAmounts);
				} else {
					Collection<OndFareDTO> colOndFareDTOs = transactionalReservationProcess
							.getOndFareDTOListForReservation(aaLoyaltyRedeemRQ.getSelectedFareTypes());
					redeemCalculateReqTo.setOndFareDTOs(colOndFareDTOs);
				}

			} else if (aaLoyaltyRedeemRQ.isPayForOHD()) {// User trying to pay for OHD reservation from IBE
				redeemCalculateReqTo.setPaxCarrierProductDueAmount(ModuleServiceLocator.getAirproxyReservationBD()
						.getPaxProduDueAmount(aaLoyaltyRedeemRQ.getOriginatorPnr(), false, true, remainingPoints));
				redeemCalculateReqTo.setOndFareDTOs(null);
				redeemCalculateReqTo.setPaxCarrierExternalCharges(null);
			}

			ServiceResponce serviceResponce = AAServicesModuleUtils.getLoyaltyManagementBD()
					.calculatePaxLoyaltyRedeemableAmounts(redeemCalculateReqTo);

			if (serviceResponce.isSuccess()) {
				Map<Integer, Map<String, BigDecimal>> paxWiseProductAmounts = (Map<Integer, Map<String, BigDecimal>>) serviceResponce
						.getResponseParam(ResponceCodes.ResponseParams.PAX_PRODUCT_AMOUNT);

				Map<Integer, Map<String, Double>> paxWiseProductPoints = (Map<Integer, Map<String, Double>>) serviceResponce
						.getResponseParam(ResponceCodes.ResponseParams.PAX_PRODUCT_POINTS);

				Double availablePoint = (Double) serviceResponce.getResponseParam(ResponceCodes.ResponseParams.AVAILABLE_POINTS);

				BigDecimal availablePointAmount = (BigDecimal) serviceResponce
						.getResponseParam(ResponceCodes.ResponseParams.AVAILABLE_POINTS_AMOUNT);

				if (aaLoyaltyRedeemRQ.isIssueRewardIds()) {
					Map<String, Double> productTotalRedeemPoints = WebplatformUtil
							.getProductTotalRedeemPoints(paxWiseProductPoints);
					RedeemLoyaltyPointsReq redeemLoyaltyPointsReq = new RedeemLoyaltyPointsReq();
					redeemLoyaltyPointsReq.setMemberAccountId(aaLoyaltyRedeemRQ.getMemberFFID());
					redeemLoyaltyPointsReq.setProductPoints(productTotalRedeemPoints);
					redeemLoyaltyPointsReq.setAppIndicator(AppIndicatorEnum.fromValue(aaLoyaltyRedeemRQ.getAppIndicator()));
					redeemLoyaltyPointsReq.setLoyaltyPointsToCurrencyConversionRate(AppSysParamsUtil
							.getLoyaltyPointsToCurrencyConversionRate());
					redeemLoyaltyPointsReq.setPnr(aaLoyaltyRedeemRQ.getOriginatorPnr());
					redeemLoyaltyPointsReq.setMemberEnrollingCarrier(aaLoyaltyRedeemRQ.getMemberEnrollingCarrier());
					redeemLoyaltyPointsReq.setMemberExternalId(aaLoyaltyRedeemRQ.getMemberExternalId());
					
					serviceResponce = ModuleServiceLocator.getLoyaltyManagementBD().issueLoyaltyRewards(redeemLoyaltyPointsReq);

				if (serviceResponce.isSuccess()) {
						String[] loyaltyRewardIds = (String[]) serviceResponce
								.getResponseParam(ResponceCodes.ResponseParams.LOYALTY_REWARD_IDS);
						BigDecimal loyaltyPayAmount = AirProxyReservationUtil.getTotalRedeemedPoints(paxWiseProductAmounts);
						aaLoyaltyRedeemRS.setLoyaltyPayAmount(loyaltyPayAmount);

						List<String> rewardIdList = new ArrayList<String>();
						for (String rewardId : loyaltyRewardIds) {
							rewardIdList.add(rewardId);
					}
						aaLoyaltyRedeemRS.getRewardIds().addAll(rewardIdList);
					}
				}

				Collection<PaxProductPoints> paxProductPoints = new ArrayList<PaxProductPoints>();
				for (Integer paxSeq : paxWiseProductPoints.keySet()) {
					PaxProductPoints perPaxProductPoints = new PaxProductPoints();
					perPaxProductPoints.setKey(paxSeq);
					perPaxProductPoints.getValue().addAll(
							TransformerUtil.populateStringDoubleMap(paxWiseProductPoints.get(paxSeq)));
					remainingPoints = calculateRemainingPoints(remainingPoints, availablePoint, paxWiseProductPoints.get(paxSeq));
					paxProductPoints.add(perPaxProductPoints);
				}
				aaLoyaltyRedeemRS.getPerPaxProductPoints().addAll(paxProductPoints);

				Collection<PaxProductAmounts> paxProductAmounts = new ArrayList<PaxProductAmounts>();
				for (Integer paxSeq : paxWiseProductAmounts.keySet()) {
					PaxProductAmounts perPaxProductAmounts = new PaxProductAmounts();
					perPaxProductAmounts.setKey(paxSeq);
					perPaxProductAmounts.getValue().addAll(
							TransformerUtil.populateStringDecimalMap(paxWiseProductAmounts.get(paxSeq)));
					paxProductAmounts.add(perPaxProductAmounts);
				}
				aaLoyaltyRedeemRS.getPerPaxProductAmounts().addAll(paxProductAmounts);
				aaLoyaltyRedeemRS.setAvailablePoint(availablePoint);
				aaLoyaltyRedeemRS.setAvailablePointAmount(availablePointAmount);
				aaLoyaltyRedeemRS.setRemainingPoint(remainingPoints);
			}
			

		} catch (ModuleException ex) {
			AAExceptionUtil.addAAErrrors(aaLoyaltyRedeemRS.getResponseAttributes().getErrors(), ex);
		} catch (Exception e) {
			log.error("Error Occured in AALoyaltyRedeemBL ", e);
		}

		return aaLoyaltyRedeemRS;
	}

	private boolean checkAndSyncLMSAccount(String marketingAirLineCode, String memberFFID) {
		try {
			LmsMember lmsMember = AAServicesModuleUtils.getLmsMemberServiceBD().getLmsMember(memberFFID);

			if (lmsMember == null) {
				return AAServicesModuleUtils.getLmsMemberServiceBD().syncLmsMember(marketingAirLineCode, memberFFID);
			} else {
				return true;
			}

		} catch (ModuleException e) {
			log.error("AALoyaltyRedeemBL get LMS Member Account failed.", e);
		}

		return false;
	}

	private Double calculateRemainingPoints(Double remainingPoints, Double availablePoint, Map<String, Double> productPointMap) {
		if (remainingPoints == null) {
			remainingPoints = availablePoint;
		}

		for (Double productPoint : productPointMap.values()) {
			remainingPoints = remainingPoints - productPoint;
		}

		return remainingPoints;

	}

	@Override
	public void setTransactionalReservationProcess(TransactionalReservationProcess transactionalReservationProcess) {
		this.transactionalReservationProcess = transactionalReservationProcess;
	}

	private BigDecimal getPointsValueByBaseCurrency(double pointsValue, BigDecimal pointToCurrencyConversionRate,
			CurrencyExchangeRate currencyExchangeRate) {
		BigDecimal loyaltyCurrencyValue = AccelAeroCalculator
				.multiply(new BigDecimal(pointsValue), pointToCurrencyConversionRate);

		BigDecimal baseCurrencyAmount = AccelAeroCalculator.multiply(loyaltyCurrencyValue,
				currencyExchangeRate.getExrateCurToBaseNumber());
		return AccelAeroCalculator.scaleDefaultRoundingDown(baseCurrencyAmount);
	}
	
	private DiscountRQ composeDiscountRQ(DiscountInfo discountInfo) throws ModuleException {
		DiscountRQ promotionRQ = null;
		if (discountInfo != null) {
			promotionRQ = new DiscountRQ(DISCOUNT_METHOD.getEnum(discountInfo.getActionType()));

			promotionRQ.setCalculateTotalOnly(false);
			DiscountedFareDetails discountedFareDetails = null;
			if (discountInfo.getDiscountedFareDetails() != null) {
				discountedFareDetails = AAServicesTransformUtil.tranformLccDiscountedFareDetails(discountInfo
						.getDiscountedFareDetails());
				promotionRQ.setDiscountInfoDTO(discountedFareDetails);
			}
			if (discountInfo.getPaxCharges() != null) {
				promotionRQ.setPaxChargesList(AAServicesTransformUtil.tranformLccPaxCharges(discountInfo.getPaxCharges()));
			}

		}
		return promotionRQ;
	}

}
