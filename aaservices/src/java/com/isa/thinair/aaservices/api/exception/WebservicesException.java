package com.isa.thinair.aaservices.api.exception;

import java.util.HashMap;
import java.util.Map;

import com.isa.connectivity.profiles.maxico.v1.required.dto.AAErrorCode;
import com.isa.connectivity.profiles.maxico.v1.required.dto.AAWarningCode;

/**
 * Exception for propagating exceptional situations & warnings in web services internal implementations to the main
 * service implementation class
 * 
 * @author Nilindra Fernando
 */
public class WebservicesException extends Exception {

	private static final long serialVersionUID = -6571651954258094065L;

	private Map<AAErrorCode, String> aaErrorCodes = new HashMap<AAErrorCode, String>();

	private Map<AAWarningCode, String> aaWarningCodes = new HashMap<AAWarningCode, String>();

	public WebservicesException(Exception cause) {
		super(cause);
	}

	public WebservicesException(AAErrorCode aaErrorCode, String contextInfo) {
		aaErrorCodes.put(aaErrorCode, contextInfo);
	}

	public WebservicesException(AAWarningCode aaErrorCode, String contextInfo) {
		aaWarningCodes.put(aaErrorCode, contextInfo);
	}

	public Map<AAErrorCode, String> getAAErrorCodes() {
		return aaErrorCodes;
	}

}
