package com.isa.thinair.aaservices.client;


import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashSet;
import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import com.sun.xml.wss.ProcessingContext;
import com.sun.xml.wss.XWSSProcessor;
import com.sun.xml.wss.XWSSProcessorFactory;
import com.sun.xml.wss.XWSSecurityException;

/**
 * @author Nilindra Fernando
 */
public class ClientSecurityHandler implements SOAPHandler<SOAPMessageContext> {
   
    private XWSSProcessor cprocessor = null;

    public ClientSecurityHandler(String userName, String password) {
        InputStream in = null;
    	try {
            XWSSProcessorFactory factory = XWSSProcessorFactory.newInstance();
            in = getStream();
            cprocessor = factory.createProcessorForSecurityConfiguration(in, new ClientSecurityEnvHandler(userName, password));
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
        	if (in != null) {
        		try {
					in.close();
				} catch (IOException e) {
				}
        	}
        }
    }
   
    public Set<QName> getHeaders() {
         QName securityHeader = new QName("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd", "Security", "wsse");
         HashSet<QName> headers = new HashSet<QName>();
         headers.add(securityHeader);
         return headers;
    } 

    public boolean handleFault(SOAPMessageContext messageContext) {
        return true;
    }

    public boolean handleMessage(SOAPMessageContext messageContext) {
    	secureClient(messageContext);
    	return true;
    }
    
    public void close(MessageContext messageContext) {}
   
    private SOAPMessage createFaultResponse(XWSSecurityException ex) {
        // TODO: add code here
        return null;
    }
    
    private void secureClient(SOAPMessageContext messageContext) {
        Boolean outMessageIndicator = (Boolean) messageContext.get (MessageContext.MESSAGE_OUTBOUND_PROPERTY);
        SOAPMessage message = messageContext.getMessage();
        
        if (outMessageIndicator.booleanValue()) {
            System.out.println("\n Client Outbound SOAP:");
            ProcessingContext context;
            try {
                context = cprocessor.createProcessingContext(message);
                context.setSOAPMessage(message);
                SOAPMessage secureMsg = cprocessor.secureOutboundMessage(context);
                //secureMsg.writeTo(System.out);
                messageContext.setMessage(secureMsg);
            } catch (XWSSecurityException ex) {
                ex.printStackTrace();
                throw new RuntimeException(ex);
            } catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            } 
            return;
        } else {
            System.out.println("\n Client Inbound SOAP:");
            //do nothing
            return;
        }     
    }
    
    private InputStream getStream() {
		String text = "<xwss:SecurityConfiguration xmlns:xwss=\"http://java.sun.com/xml/ns/xwss/config\" dumpMessages=\"true\"> "
				+ "<xwss:UsernameToken digestPassword=\"false\"/> "
				+ "</xwss:SecurityConfiguration>";
		InputStream is = null;

		try {
			is = new ByteArrayInputStream(text.getBytes("UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		return is;
	}
}

