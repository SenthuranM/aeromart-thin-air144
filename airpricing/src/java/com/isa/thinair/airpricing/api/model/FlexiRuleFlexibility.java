package com.isa.thinair.airpricing.api.model;

import com.isa.thinair.commons.core.framework.Tracking;

public class FlexiRuleFlexibility extends Tracking {

	private static final long serialVersionUID = 7013214680483512131L;

	private int flexiRuleFlexibilityId;

	private int flexiRuleId;

	private int flexiRuleFlexiTypeId;

	private int allowedCount;

	private long cutOverMins;

	private String status;

	/**
	 * @return Returns the flexiRuleFlexibilityId.
	 * @hibernate.id column = "FLEXI_RULE_FLEXIBILITY_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_FLEXI_RULE_FLEXIBILITY"
	 */
	public int getFlexiRuleFlexibilityId() {
		return flexiRuleFlexibilityId;
	}

	/**
	 * @param flexiRuleFlexibilityId
	 *            The flexiRuleFlexibilityId to set.
	 */
	public void setFlexiRuleFlexibilityId(int flexiRuleFlexibilityId) {
		this.flexiRuleFlexibilityId = flexiRuleFlexibilityId;
	}

	/**
	 * returns the flexiRuleId
	 * 
	 * @return Returns the flexiRuleId.
	 * 
	 * @hibernate.property column = "FLEXI_RULE_ID"
	 * 
	 */
	public int getFlexiRuleId() {
		return flexiRuleId;
	}

	/**
	 * @param flexiRuleId
	 *            The flexiRuleId to set.
	 */
	public void setFlexiRuleId(int flexiRuleId) {
		this.flexiRuleId = flexiRuleId;
	}

	/**
	 * returns the flexiRuleFlexiTypeId
	 * 
	 * @return Returns the flexiRuleFlexiTypeId.
	 * 
	 * @hibernate.property column = "FLEXI_RULE_FLEXI_TYPE_ID"
	 * 
	 */
	public int getFlexiRuleFlexiTypeId() {
		return flexiRuleFlexiTypeId;
	}

	/**
	 * @param flexiRuleFlexiTypeId
	 *            The flexiRuleFlexiTypeId to set.
	 */
	public void setFlexiRuleFlexiTypeId(int flexiRuleFlexiTypeId) {
		this.flexiRuleFlexiTypeId = flexiRuleFlexiTypeId;
	}

	/**
	 * returns the allowedCount
	 * 
	 * @return Returns the allowedCount.
	 * 
	 * @hibernate.property column = "ALLOWED_COUNT"
	 * 
	 */
	public int getAllowedCount() {
		return allowedCount;
	}

	/**
	 * @param allowedCount
	 *            The allowedCount to set.
	 */
	public void setAllowedCount(int allowedCount) {
		this.allowedCount = allowedCount;
	}

	/**
	 * returns the cutOverMins
	 * 
	 * @return Returns the cutOverMins.
	 * 
	 * @hibernate.property column = "CUT_OVER_MINS"
	 * 
	 */
	public long getCutOverMins() {
		return cutOverMins;
	}

	/**
	 * @param cutOverMins
	 *            The cutOverMins to set.
	 */
	public void setCutOverMins(long cutOverMins) {
		this.cutOverMins = cutOverMins;
	}

	/**
	 * returns the status
	 * 
	 * @return Returns the status.
	 * 
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * sets the status
	 * 
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}
}
