package com.isa.thinair.airpricing.api.model;

import java.io.Serializable;

/**
 * @hibernate.class table = "T_FARE_RULE_COMMENT"
 */
public class FareRuleComment implements Serializable {


	private static final long serialVersionUID = 1L;

	private Integer commentID;

	private String languageCode;

	private String comments;

	private FareRule fareRule;

	/**
	 * @return Returns the commentID.
	 * @hibernate.id column = "FARE_RULE_COMMENT_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_FARE_RULE_COMMENT"
	 */

	public Integer getCommentID() {
		return commentID;
	}

	public void setCommentID(Integer commentID) {
		this.commentID = commentID;
	}

	/**
	 * @return languageCode
	 * @hibernate.property column = "LANGUAGE_CODE"
	 */

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	/**
	 * @return comments
	 * @hibernate.property column = "RULES_COMMENTS" sql-type="CLOB"
	 */

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * @return the fareRule
	 * @hibernate.many-to-one column="FARE_RULE_ID" class="com.isa.thinair.airpricing.api.model.FareRule"
	 */
	public FareRule getFareRule() {
		return fareRule;
	}

	public void setFareRule(FareRule fareRule) {
		this.fareRule = fareRule;
	}

}
