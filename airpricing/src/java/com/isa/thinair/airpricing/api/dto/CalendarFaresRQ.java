package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airproxy.api.model.reservation.availability.CalendarSearchRQ;

public class CalendarFaresRQ implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private CalendarSearchRQ searchRQ = null;
	private Map<Integer, Set<String>> ondWiseOndCodes = null;

	public CalendarFaresRQ(CalendarSearchRQ searchRQ, Map<Integer, Set<String>> ondWiseOndCodes) {
		this.searchRQ = searchRQ;
		this.ondWiseOndCodes = ondWiseOndCodes;
	}

	public CalendarSearchRQ getSearchRQ() {
		return searchRQ;
	}

	public Set<String> getOndCodesFor(Integer ondSequence) {
		return ondWiseOndCodes.get(ondSequence);
	}
}
