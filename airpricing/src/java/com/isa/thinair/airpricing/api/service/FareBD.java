/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 *
 * Created on Jul 13, 2005 17:09:10
 *
 *
 * ===============================================================================
 */
package com.isa.thinair.airpricing.api.service;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.seatavailability.FareSummaryDTO;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airpricing.api.criteria.FareSearchCriteria;
import com.isa.thinair.airpricing.api.dto.CalendarFaresRQ;
import com.isa.thinair.airpricing.api.dto.CalendarFaresRS;
import com.isa.thinair.airpricing.api.dto.ChargeTO;
import com.isa.thinair.airpricing.api.dto.FareDTO;
import com.isa.thinair.airpricing.api.dto.FaresAndChargesTO;
import com.isa.thinair.airpricing.api.model.Charge;
import com.isa.thinair.airpricing.api.model.Fare;
import com.isa.thinair.airpricing.api.model.FareRule;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Byorn
 */
public interface FareBD {

	public static final String SERVICE_NAME = "FareService";

	/** BD Interface to save a Fare **/
	public void saveFare(FareDTO fareDTO) throws ModuleException;

	/** BD Interface to update a Fare **/
	public Integer updateFare(FareDTO fareDTO, boolean skipSplitFare) throws ModuleException;

	/**
	 * Updates the non critical FareRule details without any other operation(splitting etc.). Currently updates Agent
	 * Instructions and Comments.
	 * 
	 * @param fareDTO
	 * @throws ModuleException
	 */
	public void updateNonCriticalFareDetails(FareDTO fareDTO) throws ModuleException;

	/** BD Interface to get a Fare for fareID **/
	public FareDTO getFare(int fareID) throws ModuleException;

	public Collection<FareDTO> getFares(Collection<Integer> fareIds) throws ModuleException;

	/** BD Interface to get a Fares **/
	public Page getFares(FareSearchCriteria fareSearchCriteria, int startIndex, int pageSize, List<String> orderBy)
			throws ModuleException;

	/** BD Interface to update a list of fares **/
	public void updateFares(Collection<Fare> fares) throws ModuleException;

	/** Filters effective fares for set of booking codes constrained by ondCode, depatureDate **/
	public Collection<Fare> getEffectiveFares(Collection<String> bookingCodes, String ondCode, Date depatureDate)
			throws ModuleException;

	/** The method returns a hashmap keyed by bookingcode having no of effective agents against each booking code **/
	public HashMap<String, Integer> getEffectiveAgentsCount(Collection<String> bookingCodes, String ondCode, Date depatureDate)
			throws ModuleException;

	/**
	 * Return fare and charges states for air reservation purposes
	 * 
	 * @param fareIds
	 *            collection of fare Ids
	 * @param chargeRateIds
	 *            collection of charge rate ids
	 * @param prefferedLanguage
	 *            TODO
	 * @param prefferedLanguage
	 * @return object of FaresAndChargesTO
	 * @throws ModuleException
	 */
	public FaresAndChargesTO getRefundableStatuses(Collection<Integer> fareIds, Collection<Integer> chargeRateIds,
			String prefferedLanguage) throws ModuleException;

	/**
	 * Returns the fare rule id and FareSummaryDTO
	 * 
	 * @param fareRuleIds
	 * @return
	 * @throws ModuleException
	 */
	public Map<Integer, FareSummaryDTO> getPaxDetails(Collection<Integer> fareRuleIds) throws ModuleException;

	public HashMap<String, List<String>> getAgentsLinkedToBookingClass(Collection<String> bookingCodesCollection)
			throws ModuleException;

	public Collection<String> getBookingCodesLinkedToAgent(String agentCode) throws ModuleException;

	public Collection<Charge> getActiveCharges(Collection<String> colChargeGrpCodes) throws ModuleException;

	/**
	 * Return fare rule attached with specified fares.
	 * 
	 * @param fareIds
	 * @return
	 */
	public HashMap<Integer, FareRule> getFareRules(Collection<Integer> fareIds) throws ModuleException;

	public void reCalcBaseFareForLocalCurr(Date date) throws ModuleException;

	public void updateStatusForFaresInLocalCurr(Collection<CurrencyExchangeRate> currExchangeList, String status)
			throws ModuleException;

	public Collection<FareRule> getFareRules(String fareBasisCode) throws ModuleException;

	public Map<Integer, ChargeTO> getChageRateById(Collection<Integer> chargeRateIds) throws ModuleException;

	public Collection<Object[]> getFare(FareDTO fareDTO) throws ModuleException;

	/** Interface method to Check if FareRule, Booking Class, OND Combination Exists for the time period **/
	public boolean isFareLegal(String fareRuleCode, String bookingCode, String ONDCode, Date efctFrom, Date efctTo,
			Date salesEfctFrom, Date salesEfctTo, Date returnFromDate, Date returnToDate, boolean returnFlag)
			throws ModuleException;

	public Map<Integer, FareSummaryDTO> getLocalFares(Collection<Integer> fareIds) throws ModuleException;

	public Map<Integer, String> getMasterFaresForOnD(String ondCode) throws ModuleException;

	public boolean isMasterFareAlreayLinked(Integer masterFareId) throws ModuleException;

	public boolean isMasterFareRefCodeAlreayUsed(String masterFareRefCode) throws ModuleException;

	public Collection<Fare> getLinkFares(Integer masterFareID) throws ModuleException;

	public void fareUpdateOnExchangeRateChange() throws ModuleException;

	public boolean isModifyToSameFareEnabled(Integer fareId) throws ModuleException;

	public Map<Integer, Integer> getSameReturnGroupSegDetails(Collection<Integer> dateChangedResSegList) throws ModuleException;

	public CalendarFaresRS getApplicableFares(CalendarFaresRQ faresRQ) throws ModuleException;
}
