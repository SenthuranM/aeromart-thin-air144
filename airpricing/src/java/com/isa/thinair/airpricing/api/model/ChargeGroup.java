/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:13:03
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airpricing.api.model;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * 
 * @author : Code Generation Tool
 * @version : Ver 1.0.0
 * 
 * @hibernate.class table = "T_CHARGE_GROUP"
 * 
 *                  ChargeGroup is the entity class to repesent a ChargeGroup model
 * 
 */
public class ChargeGroup extends Persistent {

	private static final long serialVersionUID = -9092281813423318546L;

	private String chargeGroupCode;

	private String chargeGroupDesc;

	/**
	 * @return Returns the chargeCategoryCode.
	 * @hibernate.id column = "CHARGE_GROUP_CODE" generator-class = "assigned"
	 * 
	 */
	public String getChargeGroupCode() {
		return chargeGroupCode;
	}

	public void setChargeGroupCode(String chargeGroupCode) {
		this.chargeGroupCode = chargeGroupCode;
	}

	/**
	 * returns the chargeGroupDesc
	 * 
	 * @return Returns the chargeGroupDesc.
	 * 
	 * @hibernate.property column = "CHARGE_GROUP_DESC"
	 */
	public String getDescription() {
		return chargeGroupDesc;
	}

	/**
	 * sets the chargeGroupDesc
	 * 
	 * @param chargeGroupDesc
	 *            The chargeGroupDesc to set.
	 */
	public void setDescription(String chargeGroupDesc) {
		this.chargeGroupDesc = chargeGroupDesc;
	}

}
