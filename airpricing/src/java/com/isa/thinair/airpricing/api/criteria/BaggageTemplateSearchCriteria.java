/**
 * 	mano
	May 6, 2011 
	2011
 */
package com.isa.thinair.airpricing.api.criteria;

import java.io.Serializable;

/**
 * @author mano
 * 
 */
public class BaggageTemplateSearchCriteria implements Serializable {

	private static final long serialVersionUID = -8792296121618294720L;

	private String cabinClass;

	private String status;

	private String templateCode;

	/**
	 * @return the cabinClass
	 */
	public String getCabinClass() {
		return cabinClass;
	}

	/**
	 * @param cabinClass
	 *            the cabinClass to set
	 */
	public void setCabinClass(String cabinClass) {
		this.cabinClass = cabinClass;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the templateCode
	 */
	public String getTemplateCode() {
		return templateCode;
	}

	/**
	 * @param templateCode
	 *            the templateCode to set
	 */
	public void setTemplateCode(String templateCode) {
		this.templateCode = templateCode;
	}

}
