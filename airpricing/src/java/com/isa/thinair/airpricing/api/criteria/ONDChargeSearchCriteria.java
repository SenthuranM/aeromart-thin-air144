/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:16:12
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airpricing.api.criteria;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author Byorn
 * 
 */

public class ONDChargeSearchCriteria implements Serializable {

	private static final long serialVersionUID = -6008387515661131657L;

	private String originAirportCode;

	private String destinationAirportCode;

	private List<String> viaAirportCodes;

	private String chargeCategory;
	private String posStation;

	private boolean exactAirportsCombinationOnly;

	private Date effectiveFromDate;

	private Date effectiveToDate;

	/**
	 * @return Returns the destinationAirportCode.
	 */
	public String getDestinationAirportCode() {
		return destinationAirportCode;
	}

	/**
	 * @param destinationAirportCode
	 *            The destinationAirportCode to set.
	 */
	public void setDestinationAirportCode(String destinationAirportCode) {
		this.destinationAirportCode = destinationAirportCode;
	}

	/**
	 * @return Returns the effectiveFromDate.
	 */
	public Date getEffectiveFromDate() {
		return effectiveFromDate;
	}

	/**
	 * @param effectiveFromDate
	 *            The effectiveFromDate to set.
	 */
	public void setEffectiveFromDate(Date effectiveFromDate) {
		this.effectiveFromDate = effectiveFromDate;
	}

	/**
	 * @return Returns the effectiveToDate.
	 */
	public Date getEffectiveToDate() {
		return effectiveToDate;
	}

	/**
	 * @param effectiveToDate
	 *            The effectiveToDate to set.
	 */
	public void setEffectiveToDate(Date effectiveToDate) {
		this.effectiveToDate = effectiveToDate;
	}

	/**
	 * @return Returns the exactAirportsCombinationOnly.
	 */
	public boolean isExactAirportsCombinationOnly() {
		return exactAirportsCombinationOnly;
	}

	/**
	 * @param exactAirportsCombinationOnly
	 *            The exactAirportsCombinationOnly to set.
	 */
	public void setExactAirportsCombinationOnly(boolean exactAirportsCombinationOnly) {
		this.exactAirportsCombinationOnly = exactAirportsCombinationOnly;
	}

	/**
	 * @return Returns the originAirportCode.
	 */
	public String getOriginAirportCode() {
		return originAirportCode;
	}

	/**
	 * @param originAirportCode
	 *            The originAirportCode to set.
	 */
	public void setOriginAirportCode(String originAirportCode) {
		this.originAirportCode = originAirportCode;
	}

	/**
	 * @return Returns the viaAirportCodes.
	 */
	public List<String> getViaAirportCodes() {
		return viaAirportCodes;
	}

	/**
	 * @param viaAirportCodes
	 *            The viaAirportCodes to set.
	 */
	public void setViaAirportCodes(List<String> viaAirportCodes) {
		this.viaAirportCodes = viaAirportCodes;
	}

	/**
	 * @return Returns the posStation.
	 */
	public String getPosStation() {
		return posStation;
	}

	/**
	 * @param posStation
	 *            The posStation to set.
	 */
	public void setPosStation(String posStation) {
		this.posStation = posStation;
	}

	/**
	 * @return Returns the chargeCategory.
	 */
	public String getChargeCategory() {
		return chargeCategory;
	}

	/**
	 * @param chargeCategory
	 *            The chargeCategory to set.
	 */
	public void setChargeCategory(String chargeCategory) {
		this.chargeCategory = chargeCategory;
	}

}
