package com.isa.thinair.airpricing.api.model;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.isa.thinair.commons.core.framework.Persistent;
/**
 * 
 * @author chamila
 * 
 * @hibernate.class table = "T_OND_FARE_PRORATION"
 *
 */
public class Proration extends Persistent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4647953883714596594L;
	private Integer prorationId;
	private String segmentCode;
	private Integer proration;	
	private Fare fare;

	/**
	 * returns the prorationId
	 * 
	 * @return Returns the prorationId.
	 * 
	 * @hibernate.id column = "OND_FARE_PRORATION_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_OND_FARE_PRORATION"
	 */
	public Integer getProrationId() {
		return prorationId;
	}

	/**
	 * Set proration id
	 * 
	 * @param prorationId
	 */
	public void setProrationId(Integer prorationId) {
		this.prorationId = prorationId;
	}

	/**
	 * @hibernate.property column = "SEGMENT_CODE"
	 * @return return the segment code
	 */
	public String getSegmentCode() {
		return segmentCode;
	}

	/**
	 * Sets the segment code
	 * 
	 * @param segmentCode
	 */
	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	/**
	 * @hibernate.property column = "PRORATION"
	 * @return
	 */
	public Integer getProration() {
		return proration;
	}

	/**
	 * 
	 * @param proration
	 */
	public void setProration(Integer proration) {
		this.proration = proration;
	}


	/**
	 * @return the fare
	 * @hibernate.many-to-one column="FARE_ID"
	 *                        class="com.isa.thinair.airpricing.api.model.Fare"
	 */
	public Fare getFare() {
		return fare;
	}

	/**
	 * 
	 * @param fare
	 */
	public void setFare(Fare fare) {
		this.fare = fare;
	}
	
	public int hashCode() {
		return new HashCodeBuilder().append(getProrationId()).toHashCode();
	}

}
