package com.isa.thinair.airpricing.api.service;

import javax.sql.DataSource;

import com.isa.thinair.airinventory.api.service.BaggageBusinessDelegate;
import com.isa.thinair.airinventory.api.service.BookingClassBD;
import com.isa.thinair.airinventory.api.service.MealBD;
import com.isa.thinair.airinventory.api.service.SeatMapBD;
import com.isa.thinair.airinventory.api.utils.AirinventoryConstants;
import com.isa.thinair.airmaster.api.service.AircraftBD;
import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.airmaster.api.utils.AirmasterConstants;
import com.isa.thinair.airmaster.core.persistence.dao.CurrencyDAO;
import com.isa.thinair.airpricing.api.utils.AirpricingConstants;
import com.isa.thinair.airpricing.core.config.AirpricingConfig;
import com.isa.thinair.airtravelagents.api.service.TravelAgentFinanceBD;
import com.isa.thinair.airtravelagents.api.utils.AirtravelagentsConstants;
import com.isa.thinair.auditor.api.service.AuditorBD;
import com.isa.thinair.auditor.api.utils.AuditorConstants;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GenericLookupUtils;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;
import com.isa.thinair.messaging.api.utils.MessagingConstants;
import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.IServiceDelegate;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;
import com.isa.thinair.platform.core.controller.ModuleFramework;

/**
 * 
 * @author byorn
 * 
 */
public class AirpricingUtils {

	private static final String CURRENCY_DAO_PROXY = "CurrencyDAOProxy";

	/**
	 * 
	 * IModule
	 * 
	 * @return
	 */
	public static IModule getInstance() {
		return LookupServiceFactory.getInstance().getModule("airpricing");

	}

	/**
	 * Returns air pricing module data source
	 * 
	 * @return
	 */
	public static DataSource getDatasource() {
		LookupService lookup = LookupServiceFactory.getInstance();
		return (DataSource) lookup.getBean(PlatformBeanUrls.Commons.DEFAULT_DATA_SOURCE_BEAN);
	}

	/**
	 * For Looking the BookingClass BD IServiceDelegate
	 * 
	 * @param targetModuleName
	 * @param BDKeyWithoutLocality
	 * @return
	 */
	private static IServiceDelegate lookupServiceBD(String targetModuleName, String BDKeyWithoutLocality) {
		return GenericLookupUtils.lookupEJB2ServiceBD(targetModuleName, BDKeyWithoutLocality, getAirpricingConfig(),
				"airpricing", "airpricing.config.dependencymap.invalid");
	}

	private static Object lookupEJB3Service(String targetModuleName, String serviceName) {
		return GenericLookupUtils.lookupEJB3Service(targetModuleName, serviceName, getAirpricingConfig(), "airpricing",
				"airpricing.config.dependencymap.invalid");
	}

	/**
	 * Will return airpricing config AirpricingConfig
	 * 
	 * @return
	 */
	public static AirpricingConfig getAirpricingConfig() {
		return (AirpricingConfig) LookupServiceFactory.getInstance().getModuleConfig("airpricing");

	}

	public static AuditorBD getAuditorBD() {

		return (AuditorBD) lookupServiceBD(AuditorConstants.MODULE_NAME, AuditorConstants.BDKeys.AUDITOR_SERVICE);

	}

	public static SeatMapBD getSeatMapBD() {

		return (SeatMapBD) lookupEJB3Service(AirinventoryConstants.MODULE_NAME, SeatMapBD.SERVICE_NAME);

	}

	public static MealBD getMealBD() {

		return (MealBD) lookupEJB3Service(AirinventoryConstants.MODULE_NAME, MealBD.SERVICE_NAME);

	}

	public static AircraftBD getAirCraftBD() {
		return (AircraftBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, AircraftBD.SERVICE_NAME);
	}

	public static BookingClassBD getBookingClassBD() {
		return (BookingClassBD) lookupEJB3Service(AirinventoryConstants.MODULE_NAME, BookingClassBD.SERVICE_NAME);
	}

	/**
	 * Return the global confirguration
	 * 
	 * @return
	 */
	public static GlobalConfig getGlobalConfig() {
		return CommonsServices.getGlobalConfig();
	}

	/**
	 * Return transparent location class delegate
	 * 
	 * @return
	 */
	public static CommonMasterBD getCommonMasterBD() {
		return (CommonMasterBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, CommonMasterBD.SERVICE_NAME);
	}

	public static CurrencyDAO getCurrencyDAO() {
		return (CurrencyDAO) ModuleFramework.getInstance().getBean(AirmasterConstants.MODULE_NAME, CURRENCY_DAO_PROXY);
	}

	public static ChargeBD getChargeBD() {
		return (ChargeBD) lookupEJB3Service(AirpricingConstants.MODULE_NAME, ChargeBD.SERVICE_NAME);
	}

	public static FareBD getFareBD() {
		return (FareBD) lookupEJB3Service(AirpricingConstants.MODULE_NAME, FareBD.SERVICE_NAME);
	}

	public static BaggageBusinessDelegate getBaggageBD() {

		return (BaggageBusinessDelegate) lookupEJB3Service(AirinventoryConstants.MODULE_NAME,
				BaggageBusinessDelegate.SERVICE_NAME);

	}
	
	public static TravelAgentFinanceBD lookupTravelAgentFinanceBD() {
		return (TravelAgentFinanceBD) lookupEJB3Service(AirtravelagentsConstants.MODULE_NAME, TravelAgentFinanceBD.SERVICE_NAME);
	}

	public static MessagingServiceBD getMessagingServiceBD() {
		return (MessagingServiceBD) lookupEJB3Service(MessagingConstants.MODULE_NAME, MessagingServiceBD.SERVICE_NAME);
	}
}
