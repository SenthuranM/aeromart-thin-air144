package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;

public class FlexiRuleFlexibilityDTO implements Serializable {

	private static final long serialVersionUID = 2994786863275462621L;

	private String segmentCode;

	private int flexibilityTypeId;

	private String flexibilityDescription;

	private int availableCount;

	private int utilizedCount;

	private long cutOverBufferInMins;

	private int flexiRuleId;

	public int getFlexibilityTypeId() {
		return this.flexibilityTypeId;
	}

	public void setFlexibilityTypeId(int flexibilityTypeId) {
		this.flexibilityTypeId = flexibilityTypeId;
	}

	public int getFlexiRuleId() {
		return this.flexiRuleId;
	}

	public void setFlexiRuleId(int flexiRuleId) {
		this.flexiRuleId = flexiRuleId;
	}

	public String getFlexibilityDescription() {
		return this.flexibilityDescription;
	}

	public void setFlexibilityDescription(String flexibilityDescription) {
		this.flexibilityDescription = flexibilityDescription;
	}

	public int getAvailableCount() {
		return this.availableCount;
	}

	public void setAvailableCount(int availableCount) {
		this.availableCount = availableCount;
	}

	public int getUtilizedCount() {
		return this.utilizedCount;
	}

	public void setUtilizedCount(int utilizedCount) {
		this.utilizedCount = utilizedCount;
	}

	public long getCutOverBufferInMins() {
		return this.cutOverBufferInMins;
	}

	public void setCutOverBufferInMins(long cutOverBufferInMins) {
		this.cutOverBufferInMins = cutOverBufferInMins;
	}

	public FlexiRuleFlexibilityDTO clone() {
		FlexiRuleFlexibilityDTO clone = new FlexiRuleFlexibilityDTO();
		clone.setFlexibilityTypeId(this.getFlexibilityTypeId());
		clone.setFlexibilityDescription(this.getFlexibilityDescription());
		clone.setAvailableCount(this.getAvailableCount());
		clone.setUtilizedCount(this.getUtilizedCount());
		clone.setCutOverBufferInMins(this.getCutOverBufferInMins());
		clone.setFlexiRuleId(this.getFlexiRuleId());
		return clone;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

}
