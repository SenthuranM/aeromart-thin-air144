/**
 * 	mano
	May 6, 2011 
	2011
 */
package com.isa.thinair.airpricing.api.model;

import java.util.Set;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * @author mano
 * @hibernate.class table = "BG_T_BAGGAGE_TEMPLATE"
 */
public class BaggageTemplate extends Tracking {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8640519447247047547L;
	
	private int templateId;

	private String description;

	private Set<BaggageCharge> baggageCharges;

	private String status;

	// private String cabinClassCode;

	private String templateCode;
	
	private String chargeLocalCurrencyCode;

	/**
	 * 
	 * 
	 * @return Returns the templateId.
	 * @hibernate.id column = "BAGGAGE_TEMPLATE_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "BG_S_BAGGAGE_TEMPLATE"
	 * 
	 */
	public int getTemplateId() {
		return templateId;
	}

	/**
	 * @param templateId
	 *            the templateId to set
	 */
	public void setTemplateId(int templateId) {
		this.templateId = templateId;
	}

	/**
	 * @return the description
	 * @hibernate.property column = "BAGGAGE_DESC"
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @hibernate.set lazy="false" cascade="all" order-by="BAGGAGE_ID"
	 * @hibernate.collection-key column="BAGGAGE_TEMPLATE_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airpricing.api.model.BaggageCharge"
	 * 
	 * @return Set of BaggageCharge objects
	 */
	public Set<BaggageCharge> getBaggageCharges() {
		return baggageCharges;
	}

	/**
	 * @param baggageCharges
	 *            the baggageCharges to set
	 */
	public void setBaggageCharges(Set<BaggageCharge> baggageCharges) {
		this.baggageCharges = baggageCharges;
	}

	/**
	 * @return the status
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the templateCode
	 * @hibernate.property column = "BAGGAGE_TEMPLATE_CODE"
	 */
	public String getTemplateCode() {
		return templateCode;
	}

	/**
	 * @param templateCode
	 *            the templateCode to set
	 */
	public void setTemplateCode(String templateCode) {
		this.templateCode = templateCode;
	}

	public int hashCode() {
		return new HashCodeBuilder().append(getTemplateCode()).toHashCode();

	}
	
	/**
	 * @return the chargeLocalCurrencyCode
	 * @hibernate.property column = "LOCAL_CURRENCY_CODE"
	 */
	public String getChargeLocalCurrencyCode() {
		return chargeLocalCurrencyCode;
	}

	public void setChargeLocalCurrencyCode(String chargeLocalCurrency) {
		this.chargeLocalCurrencyCode = chargeLocalCurrency;
	}
}
