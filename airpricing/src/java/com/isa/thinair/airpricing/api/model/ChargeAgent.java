/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on May 28, 2009 10:00:33
 * 
 * ===============================================================================
 */
package com.isa.thinair.airpricing.api.model;

import java.io.Serializable;

/**
 * @author Manjula
 * 
 * @hibernate.class table = "T_CHARGE_AGENT"
 */
public class ChargeAgent implements Serializable {

	private static final long serialVersionUID = 5012354443147523773L;

	private int chargeAgentId;

	private String agentCode;

	private Charge charge;

	private String includeOrExclude;

	/**
	 * @hibernate.id column = "charge_agent_id" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_CHARGE_AGENT"
	 * @return
	 */
	public int getChargeAgentId() {
		return chargeAgentId;
	}

	public void setChargeAgentId(int chargeAgentId) {
		this.chargeAgentId = chargeAgentId;
	}

	/**
	 * @hibernate.property column = "agent_code"
	 * @return
	 */
	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * @hibernate.property column = "apply_status"
	 * @return
	 */
	public String getIncludeOrExclude() {
		return includeOrExclude;
	}

	public void setIncludeOrExclude(String includeOrExclude) {
		this.includeOrExclude = includeOrExclude;
	}

	/**
	 * @return Returns the charge.
	 * @hibernate.many-to-one column="CHARGE_CODE" class="com.isa.thinair.airpricing.api.model.Charge"
	 */
	public Charge getCharge() {
		return charge;
	}

	/**
	 * @param charge
	 *            The charge to set.
	 */
	public void setCharge(Charge charge) {
		this.charge = charge;
	}
}
