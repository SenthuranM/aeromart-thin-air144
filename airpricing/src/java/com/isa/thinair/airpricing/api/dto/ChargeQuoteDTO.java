package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;

public class ChargeQuoteDTO implements Serializable {

	private static final long serialVersionUID = -5308772274091840583L;
	private String chargeCode;
	private String chargeGroup;
	private String chargeCategory;
	private int chargeRateId;
	private boolean isChargeInBoolean;
	private double chargeValue;
	private String ondCode;

	public String getChargeCategory() {
		return chargeCategory;
	}

	public void setChargeCategory(String chargeCategory) {
		this.chargeCategory = chargeCategory;
	}

	public String getChargeCode() {
		return chargeCode;
	}

	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

	public String getChargeGroup() {
		return chargeGroup;
	}

	public void setChargeGroup(String chargeGroup) {
		this.chargeGroup = chargeGroup;
	}

	public int getChargeRateId() {
		return chargeRateId;
	}

	public void setChargeRateId(int chargeRateId) {
		this.chargeRateId = chargeRateId;
	}

	public double getChargeValue() {
		return chargeValue;
	}

	public void setChargeValue(double chargeValue) {
		this.chargeValue = chargeValue;
	}

	public boolean isChargeInBoolean() {
		return isChargeInBoolean;
	}

	public void setChargeInBoolean(boolean isChargeInBoolean) {
		this.isChargeInBoolean = isChargeInBoolean;
	}

	public String getOndCode() {
		return ondCode;
	}

	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}

}
