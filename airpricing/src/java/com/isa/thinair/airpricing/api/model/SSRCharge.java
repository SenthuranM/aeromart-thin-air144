package com.isa.thinair.airpricing.api.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * 
 * @author : M.Rikaz
 * @hibernate.class table = "T_SSR_CHARGE"
 */
public class SSRCharge extends Persistent {
	private static final long serialVersionUID = 1L;
	public static final String CHARGE_CODE_HALA_MAAS_FAMILY = "MAASFAMILY";
	public static final String CHARGE_CODE_HALA_MAAS_SINGLE = "MAASSINGLE";
	public static final String CHARGE_CODE_HALA_LOUNGE_FIRST_CLASS = "LOUNGEFIR";
	public static final String CHARGE_CODE_HALA_LOUNGE_BUSINESS_CLASS = "LOUNGEBIS";
	
	private Integer chargeId;

	private Integer maxAdultPax;

	private Integer minAdultPax;

	private Integer maxChildPax;

	private Integer minChildPax;

	private Integer maxInfantPax;

	private Integer minInfantPax;

	private BigDecimal chargeAmount;

	private Date effectiveFrom;

	private Date effectiveTo;

	private String status;

	private String description;

	private Integer maxTotalPax;

	private Integer minTotalPax;

	private String commissionIn;

	private BigDecimal commissionValue;

	private Integer ssrId;

	private String ssrChargeCode;

	private String airportCode;

	private String applicablityType;

	private BigDecimal adultAmount;

	private BigDecimal childAmount;

	private BigDecimal infantAmount;

	private BigDecimal reservationAmount;

	private Set<SSRChargesCOS> ssrChargeCOS;

	private String chargeLocalCurrencyCode;

	private BigDecimal localCurrChargeAmount;

	private BigDecimal localCurrAdultAmount;

	private BigDecimal localCurrChildAmount;

	private BigDecimal localCurrInfantAmount;

	private BigDecimal localCurrReservationAmount;

	/**
	 * @return Returns the chargeId.
	 * 
	 * @hibernate.id column="SSR_CHARGE_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_SSR_CHARGE"
	 */

	public Integer getChargeId() {
		return chargeId;
	}

	public void setChargeId(Integer chargeId) {
		this.chargeId = chargeId;
	}

	/**
	 * @return Returns the maxAdultPax.
	 * 
	 * @hibernate.property column = "MAX_ADT_PAX"
	 */
	public Integer getMaxAdultPax() {
		return maxAdultPax;
	}

	public void setMaxAdultPax(Integer maxAdultPax) {
		this.maxAdultPax = maxAdultPax;
	}

	/**
	 * @return Returns the minAdultPax.
	 * 
	 * @hibernate.property column = "MIN_ADT_PAX"
	 */
	public Integer getMinAdultPax() {
		return minAdultPax;
	}

	public void setMinAdultPax(Integer minAdultPax) {
		this.minAdultPax = minAdultPax;
	}

	/**
	 * @return Returns the maxChildPax.
	 * 
	 * @hibernate.property column = "MAX_CHD_PAX"
	 */
	public Integer getMaxChildPax() {
		return maxChildPax;
	}

	public void setMaxChildPax(Integer maxChildPax) {
		this.maxChildPax = maxChildPax;
	}

	/**
	 * @return Returns the minChildPax.
	 * 
	 * @hibernate.property column = "MIN_CHD_PAX"
	 */
	public Integer getMinChildPax() {
		return minChildPax;
	}

	public void setMinChildPax(Integer minChildPax) {
		this.minChildPax = minChildPax;
	}

	/**
	 * @return Returns the maxInfantPax.
	 * 
	 * @hibernate.property column = "MAX_INF_PAX"
	 */
	public Integer getMaxInfantPax() {
		return maxInfantPax;
	}

	public void setMaxInfantPax(Integer maxInfantPax) {
		this.maxInfantPax = maxInfantPax;
	}

	/**
	 * @return Returns the minInfantPax.
	 * 
	 * @hibernate.property column = "MIN_INF_PAX"
	 */
	public Integer getMinInfantPax() {
		return minInfantPax;
	}

	public void setMinInfantPax(Integer minInfantPax) {
		this.minInfantPax = minInfantPax;
	}

	/**
	 * @return Returns the chargeAmount.
	 * 
	 * @hibernate.property column = "CHARGE_AMOUNT"
	 */
	public BigDecimal getChargeAmount() {
		return chargeAmount;
	}

	public void setChargeAmount(BigDecimal chargeAmount) {
		this.chargeAmount = chargeAmount;
	}

	/**
	 * @return Returns the effectiveFrom.
	 * 
	 * @hibernate.property column = "EFFECTIVE_FROM"
	 */
	public Date getEffectiveFrom() {
		return effectiveFrom;
	}

	public void setEffectiveFrom(Date effectiveFrom) {
		this.effectiveFrom = effectiveFrom;
	}

	/**
	 * @return Returns the effectiveTo.
	 * 
	 * @hibernate.property column = "EFFECTIVE_TO"
	 */
	public Date getEffectiveTo() {
		return effectiveTo;
	}

	public void setEffectiveTo(Date effectiveTo) {
		this.effectiveTo = effectiveTo;
	}

	/**
	 * @return Returns the status.
	 * 
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return Returns the description.
	 * 
	 * @hibernate.property column = "DESCRIPTION"
	 */
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return Returns the maxTotalPax.
	 * 
	 * @hibernate.property column = "MAX_TOT_PAX"
	 */
	public Integer getMaxTotalPax() {
		return maxTotalPax;
	}

	public void setMaxTotalPax(Integer maxTotalPax) {
		this.maxTotalPax = maxTotalPax;
	}

	/**
	 * @return Returns the minTotalPax.
	 * 
	 * @hibernate.property column = "MIN_TOT_PAX"
	 */
	public Integer getMinTotalPax() {
		return minTotalPax;
	}

	public void setMinTotalPax(Integer minTotalPax) {
		this.minTotalPax = minTotalPax;
	}

	/**
	 * @return Returns the commissionIn.
	 * 
	 * @hibernate.property column = "COMMISSION_IN"
	 */
	public String getCommissionIn() {
		return commissionIn;
	}

	public void setCommissionIn(String commissionIn) {
		this.commissionIn = commissionIn;
	}

	/**
	 * @return Returns the commissionValue.
	 * 
	 * @hibernate.property column = "COMMISSION_VALUE"
	 */
	public BigDecimal getCommissionValue() {
		return commissionValue;
	}

	public void setCommissionValue(BigDecimal commissionValue) {
		this.commissionValue = commissionValue;
	}

	/**
	 * @return Returns the .
	 * 
	 * @hibernate.property column = "SSR_CHARGE_CODE"
	 */
	public String getSsrChargeCode() {
		return ssrChargeCode;
	}

	public void setSsrChargeCode(String ssrChargeCode) {
		this.ssrChargeCode = ssrChargeCode;
	}

	/**
	 * @return Returns the airportCode.
	 * 
	 * @hibernate.property column = "AIRPORT_CODE"
	 */
	public String getAirportCode() {
		return airportCode;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	/**
	 * @return Returns the applicablityType.
	 * 
	 * @hibernate.property column = "APPLICABLITY_TYPE"
	 */
	public String getApplicablityType() {
		return applicablityType;
	}

	public void setApplicablityType(String applicablityType) {
		this.applicablityType = applicablityType;
	}

	/**
	 * @return Returns the adultAmount.
	 * 
	 * @hibernate.property column = "ADULT_AMOUNT"
	 */
	public BigDecimal getAdultAmount() {
		return adultAmount;
	}

	public void setAdultAmount(BigDecimal adultAmount) {
		this.adultAmount = adultAmount;
	}

	/**
	 * @return Returns the childAmount.
	 * 
	 * @hibernate.property column = "CHILD_AMOUNT"
	 */
	public BigDecimal getChildAmount() {
		return childAmount;
	}

	public void setChildAmount(BigDecimal childAmount) {
		this.childAmount = childAmount;
	}

	/**
	 * @return Returns the infantAmount.
	 * 
	 * @hibernate.property column = "INFANT_AMOUNT"
	 */
	public BigDecimal getInfantAmount() {
		return infantAmount;
	}

	public void setInfantAmount(BigDecimal infantAmount) {
		this.infantAmount = infantAmount;
	}

	/**
	 * @return Returns the reservationAmount.
	 * 
	 * @hibernate.property column = "RESERVATION_AMOUNT"
	 */
	public BigDecimal getReservationAmount() {
		return reservationAmount;
	}

	public void setReservationAmount(BigDecimal reservationAmount) {
		this.reservationAmount = reservationAmount;
	}

	/**
	 * @return Returns the ssrId.
	 * 
	 * @hibernate.property column = "SSR_ID"
	 */
	public Integer getSsrId() {
		return ssrId;
	}

	public void setSsrId(Integer ssrId) {
		this.ssrId = ssrId;
	}

	/**
	 * @return the ssrChargeCOS
	 */
	public Set<SSRChargesCOS> getSsrChargeCOS() {
		return ssrChargeCOS;
	}

	/**
	 * @param ssrChargeCOS
	 *            the ssrChargeCOS to set
	 * @hibernate.set lazy="false" cascade="all" inverse="true"
	 * @hibernate.collection-key column="SSR_CHARGE_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airpricing.api.model.SSRChargesCOS"
	 */
	public void setSsrChargeCOS(Set<SSRChargesCOS> ssrChargeCOS) {
		this.ssrChargeCOS = ssrChargeCOS;
	}

	/**
	 * @return Returns the chargeLocalCurrencyCode.
	 * 
	 * @hibernate.property column = "LOCAL_CURRENCY_CODE"
	 */
	public String getchargeLocalCurrencyCode() {
		return chargeLocalCurrencyCode;
	}

	public void setchargeLocalCurrencyCode(String chargeLocalCurrency) {
		this.chargeLocalCurrencyCode = chargeLocalCurrency;
	}

	/**
	 * @return Returns the localCurrChargeAmount.
	 * 
	 * @hibernate.property column = "LOCAL_CURR_CHARGE_AMOUNT"
	 */
	public BigDecimal getLocalCurrChargeAmount() {
		return localCurrChargeAmount;
	}

	public void setLocalCurrChargeAmount(BigDecimal localCurrChargeAmount) {
		this.localCurrChargeAmount = localCurrChargeAmount;
	}

	/**
	 * @return Returns the localCurrAdultAmount.
	 * 
	 * @hibernate.property column = "LOCAL_CURR_ADULT_AMOUNT"
	 */
	public BigDecimal getLocalCurrAdultAmount() {
		return localCurrAdultAmount;
	}

	public void setLocalCurrAdultAmount(BigDecimal localCurrAdultAmount) {
		this.localCurrAdultAmount = localCurrAdultAmount;
	}

	/**
	 * @return Returns the localCurrChildAmount.
	 * 
	 * @hibernate.property column = "LOCAL_CURR_CHILD_AMOUNT"
	 */
	public BigDecimal getLocalCurrChildAmount() {
		return localCurrChildAmount;
	}

	public void setLocalCurrChildAmount(BigDecimal localCurrChildAmount) {
		this.localCurrChildAmount = localCurrChildAmount;
	}

	/**
	 * @return Returns the localCurrInfantAmount.
	 * 
	 * @hibernate.property column = "LOCAL_CURR_INFANT_AMOUNT"
	 */
	public BigDecimal getLocalCurrInfantAmount() {
		return localCurrInfantAmount;
	}

	public void setLocalCurrInfantAmount(BigDecimal localCurrInfantAmount) {
		this.localCurrInfantAmount = localCurrInfantAmount;
	}

	/**
	 * @return Returns the localCurrReservationAmount.
	 * 
	 * @hibernate.property column = "LOCAL_CURR_RESERVATION_AMOUNT"
	 */
	public BigDecimal getLocalCurrReservationAmount() {
		return localCurrReservationAmount;
	}

	public void setLocalCurrReservationAmount(BigDecimal localCurrReservationAmount) {
		this.localCurrReservationAmount = localCurrReservationAmount;
	}
}
