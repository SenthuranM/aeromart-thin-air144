/**
 * 
 */
package com.isa.thinair.airpricing.api.model;

import java.util.Set;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * 
 * @hibernate.class table = "ML_T_MEAL_TEMPLATE" MealTemplate is the entity class to repesent a MealTemplate
 * 
 */
public class MealTemplate extends Tracking {

	private static final long serialVersionUID = 8051213353235434168L;
	
	private int templateId;

	private String templateCode;

	private String description;

	private Set<MealCharge> mealCharges;

	private String status;
	
	private String chargeLocalCurrencyCode;
	
	private Set<MultiSelectCategoryRestriction> categoryRestrictions;
	// private String cabinClassCode;

	/**
	 * 
	 * @return
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * 
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	public int hashCode() {
		return new HashCodeBuilder().append(getTemplateCode()).toHashCode();
	}

	/**
	 * @return Returns the templateId.
	 * @hibernate.id column = "MEAL_TEMPLATE_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "ML_S_MEAL_TEMPLATE"
	 */
	public int getTemplateId() {
		return templateId;
	}

	/**
	 * @param templateId
	 *            The templateId to set.
	 */
	public void setTemplateId(int templateId) {
		this.templateId = templateId;
	}

	/**
	 * @return Returns the description.
	 * 
	 * @hibernate.property column = "MEAL_TEMPLATE_DESC"
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            The description to set.
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return Returns the templateCode.
	 * 
	 * @hibernate.property column = "MEAL_TEMPLATE_CODE"
	 */
	public String getTemplateCode() {
		return templateCode;
	}

	/**
	 * @param templateCode
	 *            The templateCode to set.
	 */
	public void setTemplateCode(String templateCode) {
		this.templateCode = templateCode;
	}

	/**
	 * @hibernate.set lazy="false" cascade="all" order-by="MEAL_ID"
	 * @hibernate.collection-key column="MEAL_TEMPLATE_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airpricing.api.model.MealCharge"
	 * 
	 * @return Set of MealCharge objects
	 */
	public Set<MealCharge> getMealCharges() {
		return mealCharges;
	}

	/**
	 * 
	 * @param Set
	 *            of MealCharge objects
	 */
	public void setMealCharges(Set<MealCharge> mealCharges) {
		this.mealCharges = mealCharges;
	}
	
	/**
	 * @return the chargeLocalCurrencyCode
	 * @hibernate.property column = "LOCAL_CURRENCY_CODE"
	 */
	public String getChargeLocalCurrencyCode() {
		return chargeLocalCurrencyCode;
	}

	public void setChargeLocalCurrencyCode(String chargeLocalCurrency) {
		this.chargeLocalCurrencyCode = chargeLocalCurrency;
	}

	/**
	 * @hibernate.set lazy="false" cascade="all-delete-orphan" inverse="true" orphanRemoval="true"
	 * @hibernate.collection-key column="MEAL_TEMPLATE_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airpricing.api.model.MultiSelectCategoryRestriction"
	 * 
	 * @return Set of Multi Select Meal Category Restrictions
	 */
	public Set<MultiSelectCategoryRestriction> getCategoryRestrictions() {
		return categoryRestrictions;
	}

	/**
	 * @param categoryRestrictions the categoryRestrictions to set
	 */
	public void setCategoryRestrictions(Set<MultiSelectCategoryRestriction> categoryRestrictions) {
		this.categoryRestrictions = categoryRestrictions;
	}

	/*
	 * /**
	 * 
	 * @return
	 * o
	 * @hibernate.property column = "CABIN_CLASS_CODE"
	 */
	/*
	 * public String getCabinClassCode() { return cabinClassCode; }
	 * 
	 * public void setCabinClassCode(String cabinClassCode) { this.cabinClassCode = cabinClassCode; }
	 */
	
	
}
