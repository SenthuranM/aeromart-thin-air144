package com.isa.thinair.airpricing.api.model;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * @author Dhanushka
 * 
 * @hibernate.class table = "T_FARE_RULE_PAX"
 */
public class FareRulePax extends Tracking {

	private static final long serialVersionUID = 7013214680483512131L;

	public static final String APPLY_YES = "Y";

	public static final String APPLY_NO = "N";

	private Long farePaxID;

	/** Holds the Fare Rule */
	private FareRule fareRule;

	private String paxType;

	private String applyFare = APPLY_NO;

	private String applyModCharge = APPLY_NO;

	private String applyCNXCharge = APPLY_NO;

	private String refundableFare = APPLY_NO;

	private double noShowChrge;

	private String noShowChgBasis;

	private Double noShowChgBreakPoint;

	private Double noShowChgBoundary;

	private Double noShowChrgeInLocal = 0.0d;

	/**
	 * @return the farePaxID
	 * @hibernate.id column = "FR_PAX_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_FARE_RULE_PAX"
	 */
	public Long getFarePaxID() {
		return farePaxID;
	}

	/**
	 * @param farePaxID
	 *            the farePaxID to set
	 */
	public void setFarePaxID(Long farePaxID) {
		this.farePaxID = farePaxID;
	}

	/**
	 * @return the paxType
	 * @hibernate.property column = "PAX_TYPE_CODE"
	 */
	public String getPaxType() {
		return paxType;
	}

	/**
	 * @param paxType
	 *            the paxType to set
	 */
	public void setPaxType(String paxType) {
		this.paxType = paxType;
	}

	/**
	 * @return the fareRule
	 * @hibernate.many-to-one column="FARE_RULE_ID" class="com.isa.thinair.airpricing.api.model.FareRule"
	 */
	public FareRule getFareRule() {
		return fareRule;
	}

	/**
	 * @param fareRule
	 *            the fareRule to set
	 */
	public void setFareRule(FareRule fareRule) {
		this.fareRule = fareRule;
	}

	/**
	 * Overrides hashcode to support lazy loading
	 */
	public int hashCode() {
		return new HashCodeBuilder().append(getFarePaxID()).toHashCode();
	}

	/**
	 * Overides the equals
	 * 
	 * @param o
	 * @return
	 */
	public boolean equals(Object o) {
		// If it's a same class instance
		if (o instanceof FareRulePax) {
			FareRulePax castObject = (FareRulePax) o;
			if (castObject.getFarePaxID() == null || this.getFarePaxID() == null) {
				return false;
			} else if (castObject.getFarePaxID() == this.getFarePaxID()) {
				return true;
			} else {
				return false;
			}
		}
		// If it's not
		else {
			return false;
		}
	}

	/**
	 * @param applyCNXCharge
	 *            The applyCNXCharge to set.
	 */
	private void setApplyCNXCharge(String applyCNXCharge) {
		this.applyCNXCharge = applyCNXCharge;
	}

	/**
	 * @param applyCNXCharge
	 *            The applyCNXCharge to set.
	 */
	public void setApplyCNXCharges(boolean applyCNXCharge) {
		if (applyCNXCharge) {
			this.setApplyCNXCharge(APPLY_YES);
		} else {
			this.setApplyCNXCharge(APPLY_NO);
		}
	}

	/**
	 * @param applyFare
	 *            The applyFare to set.
	 */
	private void setApplyFare(String applyFare) {
		this.applyFare = applyFare;
	}

	/**
	 * @param applyFare
	 *            The applyFare to set.
	 */
	public void setApplyFares(boolean applyFare) {
		if (applyFare) {
			this.setApplyFare(APPLY_YES);
		} else {
			this.setApplyFare(APPLY_NO);
		}
	}

	/**
	 * @param showModCharge
	 *            The showModCharge to set.
	 */
	private void setApplyModCharge(String applyModCharge) {
		this.applyModCharge = applyModCharge;
	}

	/**
	 * @param showModCharge
	 *            The showModCharge to set.
	 */
	public void setApplyModCharges(boolean applyModCharge) {
		if (applyModCharge) {
			this.setApplyModCharge(APPLY_YES);
		} else {
			this.setApplyModCharge(APPLY_NO);
		}
	}

	/**
	 * @param refundableFare
	 *            The refundableFare to set.
	 */
	private void setRefundableFare(String refundableFare) {
		this.refundableFare = refundableFare;
	}

	/**
	 * @param refundableFare
	 *            The refundableFare to set.
	 */
	public void setRefundableFares(boolean refundableFare) {
		if (refundableFare) {
			this.setRefundableFare(APPLY_YES);
		} else {
			this.setRefundableFare(APPLY_NO);
		}
	}

	/**
	 * @return Returns the applyCNXCharge.
	 * @hibernate.property column = "APPLY_CNX_CHARGE"
	 */
	private String getApplyCNXCharge() {
		return applyCNXCharge;
	}

	/**
	 * Apply cancellation charges
	 * 
	 * @return
	 */
	public boolean getApplyCNXCharges() {
		if (APPLY_YES.equals(getApplyCNXCharge())) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @return Returns the applyFare.
	 * @hibernate.property column = "APPLY_FARE"
	 */
	private String getApplyFare() {
		return applyFare;
	}

	/**
	 * Apply fares
	 * 
	 * @return
	 */
	public boolean getApplyFares() {
		if (APPLY_YES.equals(getApplyFare())) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @return Returns the showModCharge.
	 * @hibernate.property column = "APPLY_MOD_CHARGE"
	 */
	private String getApplyModCharge() {
		return applyModCharge;
	}

	/**
	 * Apply modification charges
	 * 
	 * @return
	 */
	public boolean getApplyModCharges() {
		if (APPLY_YES.equals(getApplyModCharge())) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @return Returns the refundableFare.
	 * @hibernate.property column = "REFUNDABLE_FLAG"
	 */
	private String getRefundableFare() {
		return refundableFare;
	}

	/**
	 * Return refundable fares
	 * 
	 * @return
	 */
	public boolean getRefundableFares() {
		if (APPLY_YES.equals(getRefundableFare())) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @return Returns the noShowChrge.
	 * @hibernate.property column = "NO_SHOW_CHARGE"
	 */
	public double getNoShowChrge() {
		return noShowChrge;
	}

	/**
	 * @param noShowChrge
	 *            The noShowChrge to set.
	 */
	public void setNoShowChrge(double noShowChrge) {
		this.noShowChrge = noShowChrge;
	}

	/**
	 * @return the noShowChgBasis
	 * @hibernate.property column = "NO_SHOW_CHG_BASIS"
	 */
	public String getNoShowChgBasis() {
		return noShowChgBasis;
	}

	/**
	 * @param noShowChgBasis
	 *            the noShowChgBasis to set
	 */
	public void setNoShowChgBasis(String noShowChgBasis) {
		this.noShowChgBasis = noShowChgBasis;
	}

	/**
	 * @return the noShowChgBreakPoint
	 * @hibernate.property column = "NO_SHOW_CHG_BREAKPOINT"
	 */
	public Double getNoShowChgBreakPoint() {
		return noShowChgBreakPoint;
	}

	/**
	 * @param noShowChgBreakPoint
	 *            the noShowChgBreakPoint to set
	 */
	public void setNoShowChgBreakPoint(Double noShowChgBreakPoint) {
		this.noShowChgBreakPoint = noShowChgBreakPoint;
	}

	/**
	 * @return the noShowChgBoundary
	 * @hibernate.property column = "NO_SHOW_CHG_BOUNDARY"
	 */
	public Double getNoShowChgBoundary() {
		return noShowChgBoundary;
	}

	/**
	 * @param noShowChgBoundary
	 *            the noShowChgBoundary to set
	 */
	public void setNoShowChgBoundary(Double noShowChgBoundary) {
		this.noShowChgBoundary = noShowChgBoundary;
	}

	/**
	 * @return the noShowChrgeInLocal
	 * @hibernate.property column = "NO_SHOW_CHARGE_IN_LOCAL"
	 */
	public Double getNoShowChrgeInLocal() {
		return noShowChrgeInLocal;
	}

	public void setNoShowChrgeInLocal(Double noShowChrgeInLocal) {
		this.noShowChrgeInLocal = noShowChrgeInLocal;
	}

}
