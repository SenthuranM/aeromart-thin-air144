/**
 * 
 */
package com.isa.thinair.airpricing.api.criteria;

import java.io.Serializable;

/**
 * @author indika
 * 
 */
public class ChargeTemplateSearchCriteria implements Serializable {

	private static final long serialVersionUID = -5338663891108446684L;

	private String modelNo;

	private String templateCode;

	private Integer templateId;

	/**
	 * @return Returns the modelNo.
	 */
	public String getModelNo() {
		return modelNo;
	}

	/**
	 * @param modelNo
	 *            The modelNo to set.
	 */
	public void setModelNo(String modelNo) {
		this.modelNo = modelNo;
	}

	/**
	 * @return Returns the templateCode.
	 */
	public String getTemplateCode() {
		return templateCode;
	}

	/**
	 * @param templateCode
	 *            The templateCode to set.
	 */
	public void setTemplateCode(String templateCode) {
		this.templateCode = templateCode;
	}

	public Integer getTemplateId() {
		return templateId;
	}

	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}

}
