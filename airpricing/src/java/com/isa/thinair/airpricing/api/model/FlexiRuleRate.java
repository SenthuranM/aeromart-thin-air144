package com.isa.thinair.airpricing.api.model;

import java.util.Date;

import com.isa.thinair.commons.core.framework.Tracking;

public class FlexiRuleRate extends Tracking {

	private static final long serialVersionUID = 7013214680483512131L;

	private int flexiRuleRateId;

	private int flexiRuleId;

	private Date depDateFrom;

	private Date depDateTo;

	private Date salesDateFrom;

	private Date salesDateTo;

	private double adultCharge;

	private String adultChargeType;

	private int adultPercentageBasisId;

	private double childCharge;

	private String childChargeType;

	private int childPercentageBasisId;

	private double infantCharge;

	private String infantChargeType;

	private int infantPercentageBasisId;

	/**
	 * @return Returns the flexiRuleRateId.
	 * @hibernate.id column = "FLEXI_RULE_RATE_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_FLEXI_RULE_RATE"
	 */
	public int getFlexiRuleRateId() {
		return flexiRuleRateId;
	}

	/**
	 * @param flexiRuleId
	 *            The flexiRuleId to set.
	 */
	public void setFlexiRuleRateId(int flexiRuleRateId) {
		this.flexiRuleRateId = flexiRuleRateId;
	}

	/**
	 * returns the flexiRuleId
	 * 
	 * @return Returns the flexiRuleId.
	 * 
	 * @hibernate.property column = "FLEXI_RULE_ID"
	 * 
	 */
	public int getFlexiRuleId() {
		return flexiRuleId;
	}

	/**
	 * @param flexiRuleId
	 *            The flexiRuleId to set.
	 */
	public void setFlexiRuleId(int flexiRuleId) {
		this.flexiRuleId = flexiRuleId;
	}

	/**
	 * returns the depDateFrom
	 * 
	 * @return Returns the depDateFrom.
	 * 
	 * @hibernate.property column = "DEP_DATE_FROM"
	 */
	public Date getDepDateFrom() {
		return depDateFrom;
	}

	/**
	 * sets the depDateFrom
	 * 
	 * @param depDateFrom
	 *            The depDateFrom to set.
	 */
	public void setDepDateFrom(Date depDateFrom) {
		this.depDateFrom = depDateFrom;
	}

	/**
	 * returns the depDateTo
	 * 
	 * @return Returns the depDateTo.
	 * 
	 * @hibernate.property column = "DEP_DATE_TO"
	 */
	public Date getDepDateTo() {
		return depDateTo;
	}

	/**
	 * sets the depDateTo
	 * 
	 * @param depDateTo
	 *            The depDateTo to set.
	 */
	public void setDepDateTo(Date depDateTo) {
		this.depDateTo = depDateTo;
	}

	/**
	 * @hibernate.property column = "SALES_DATE_FROM"
	 * @return the salesDateFrom
	 */
	public Date getSalesDateFrom() {
		return salesDateFrom;
	}

	/**
	 * @param salesDateFrom
	 *            the salesDateFrom to set
	 */
	public void setSalesDateFrom(Date salesDateFrom) {
		this.salesDateFrom = salesDateFrom;
	}

	/**
	 * @hibernate.property column = "SALES_DATE_TO"
	 * @return the salesDateTo
	 */
	public Date getSalesDateTo() {
		return salesDateTo;
	}

	/**
	 * @param salesDateTo
	 *            the salesDateTo to set
	 */
	public void setSalesDateTo(Date salesDateTo) {
		this.salesDateTo = salesDateTo;
	}

	/**
	 * @return the adultCharge
	 * @hibernate.property column = "ADULT_CHARGE"
	 */
	public double getAdultCharge() {
		return adultCharge;
	}

	/**
	 * @param adultCharge
	 *            the adultCharge to set
	 */
	public void setAdultCharge(double adultCharge) {
		this.adultCharge = adultCharge;
	}

	/**
	 * @return the adultChargeType
	 * @hibernate.property column = "ADULT_CHARGE_TYPE"
	 */
	public String getAdultChargeType() {
		return adultChargeType;
	}

	/**
	 * @param adultChargeType
	 *            the adultChargeType to set
	 */
	public void setAdultChargeType(String adultChargeType) {
		this.adultChargeType = adultChargeType;
	}

	/**
	 * returns the adultPercentageBasisId
	 * 
	 * @return Returns the adultPercentageBasisId.
	 * 
	 * @hibernate.property column = "ADULT_PERCENTAGE_BASIS_ID"
	 * 
	 */
	public int getAdultPercentageBasisId() {
		return adultPercentageBasisId;
	}

	/**
	 * @param adultPercentageBasisId
	 *            The adultPercentageBasisId to set.
	 */
	public void setAdultPercentageBasisId(int adultPercentageBasisId) {
		this.adultPercentageBasisId = adultPercentageBasisId;
	}

	/**
	 * @return the childCharge
	 * @hibernate.property column = "CHILD_CHARGE"
	 */
	public double getChildCharge() {
		return childCharge;
	}

	/**
	 * @param childCharge
	 *            the childCharge to set
	 */
	public void setChildCharge(double childCharge) {
		this.childCharge = childCharge;
	}

	/**
	 * @return the childChargeType
	 * @hibernate.property column = "CHILD_CHARGE_TYPE"
	 */
	public String getChildChargeType() {
		return childChargeType;
	}

	/**
	 * @param childChargeType
	 *            the childChargeType to set
	 */
	public void setChildChargeType(String childChargeType) {
		this.childChargeType = childChargeType;
	}

	/**
	 * returns the childPercentageBasisId
	 * 
	 * @return Returns the childPercentageBasisId.
	 * 
	 * @hibernate.property column = "CHILD_PERCENTAGE_BASIS_ID"
	 * 
	 */
	public int getChildPercentageBasisId() {
		return childPercentageBasisId;
	}

	/**
	 * @param childPercentageBasisId
	 *            The childPercentageBasisId to set.
	 */
	public void setChildPercentageBasisId(int childPercentageBasisId) {
		this.childPercentageBasisId = childPercentageBasisId;
	}

	/**
	 * @return the infantCharge
	 * @hibernate.property column = "INFANT_CHARGE"
	 */
	public double getInfantCharge() {
		return infantCharge;
	}

	/**
	 * @param infantCharge
	 *            the infantCharge to set
	 */
	public void setInfantCharge(double infantCharge) {
		this.infantCharge = infantCharge;
	}

	/**
	 * @return the infantChargeType
	 * @hibernate.property column = "INFANT_CHARGE_TYPE"
	 */
	public String getInfantChargeType() {
		return infantChargeType;
	}

	/**
	 * @param infantChargeType
	 *            the infantChargeType to set
	 */
	public void setInfantChargeType(String infantChargeType) {
		this.infantChargeType = infantChargeType;
	}

	/**
	 * returns the infantPercentageBasisId
	 * 
	 * @return Returns the infantPercentageBasisId.
	 * 
	 * @hibernate.property column = "INFANT_PERCENTAGE_BASIS_ID"
	 * 
	 */
	public int getInfantPercentageBasisId() {
		return infantPercentageBasisId;
	}

	/**
	 * @param infantPercentageBasisId
	 *            The infantPercentageBasisId to set.
	 */
	public void setInfantPercentageBasisId(int infantPercentageBasisId) {
		this.infantPercentageBasisId = infantPercentageBasisId;
	}

}
