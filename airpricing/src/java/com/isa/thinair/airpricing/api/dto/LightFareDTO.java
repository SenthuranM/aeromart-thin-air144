package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import com.isa.thinair.airpricing.api.model.Fare;
import com.isa.thinair.airproxy.api.utils.assembler.IPaxCountAssembler;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class LightFareDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private long fareId;
	private String bookingCode;
	private boolean standardBC;
	private Integer nestRank;
	private BigDecimal fareAmount;
	private Date effectiveFrom;
	private Date effectiveTo;
	private String ondCode;

	private String childFareType;
	private BigDecimal childFare;

	private String infantFareType;
	private BigDecimal infantFare;
	private LightFareRuleDTO fareRule;

	private String logicalCabinClass;

	public long getFareId() {
		return fareId;
	}

	public void setFareId(long fareId) {
		this.fareId = fareId;
	}

	public String getBookingCode() {
		return bookingCode;
	}

	public void setBookingCode(String bookingCode) {
		this.bookingCode = bookingCode;
	}

	public boolean isStandardBC() {
		return standardBC;
	}

	public void setStandardBC(boolean standardBC) {
		this.standardBC = standardBC;
	}

	public Integer getNestRank() {
		return nestRank;
	}

	public void setNestRank(Integer nestRank) {
		this.nestRank = nestRank;
	}

	public BigDecimal getFareAmount() {
		return fareAmount;
	}

	public void setFareAmount(BigDecimal fareAmount) {
		this.fareAmount = fareAmount;
	}

	public Date getEffectiveFrom() {
		return effectiveFrom;
	}

	public void setEffectiveFrom(Date effectiveFrom) {
		this.effectiveFrom = effectiveFrom;
	}

	public Date getEffectiveTo() {
		return effectiveTo;
	}

	public void setEffectiveTo(Date effectiveTo) {
		this.effectiveTo = effectiveTo;
	}

	public String getOndCode() {
		return ondCode;
	}

	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}

	public void setChildFare(String fareType, BigDecimal fareAmount) {
		this.childFare = fareAmount;
		this.childFareType = fareType;
	}

	public void setInfantFare(String fareType, BigDecimal fareAmount) {
		this.infantFare = fareAmount;
		this.infantFareType = fareType;
	}

	public BigDecimal getChildFare() {
		if (Fare.VALUE_PERCENTAGE_FLAG_V.equals(childFareType)) {
			return childFare;
		} else {
			return AccelAeroCalculator.calculatePercentage(fareAmount, childFare.floatValue(), RoundingMode.DOWN);
		}
	}

	public BigDecimal getInfantFare() {
		if (Fare.VALUE_PERCENTAGE_FLAG_V.equals(infantFareType)) {
			return infantFare;
		} else {
			return AccelAeroCalculator.calculatePercentage(fareAmount, infantFare.floatValue(), RoundingMode.DOWN);
		}
	}

	public LightFareRuleDTO getFareRule() {
		return fareRule;
	}

	public void setFareRule(LightFareRuleDTO fareRule) {
		this.fareRule = fareRule;
	}

	public void setLogicalCabinClass(String logicalCabinClass) {
		this.logicalCabinClass = logicalCabinClass;
	}

	public String getLogicalCabinClass() {
		return this.logicalCabinClass;
	}

	public BigDecimal getTotalFare(IPaxCountAssembler paxAssm) {
		return AccelAeroCalculator.add(AccelAeroCalculator.multiply(getFareAmount(), paxAssm.getAdultCount()),
				AccelAeroCalculator.multiply(getChildFare(), paxAssm.getChildCount()),
				AccelAeroCalculator.multiply(getInfantFare(), paxAssm.getInfantCount()));
	}

	public boolean isWithinOutboundValidity(Date date) {
		if (date != null && effectiveFrom.compareTo(date) <= 0 && effectiveTo.compareTo(date) >= 0) {
			return true;
		}
		return false;
	}

}
