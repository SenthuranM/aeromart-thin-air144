package com.isa.thinair.airpricing.api.service;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import com.isa.thinair.airpricing.api.criteria.FlexiRuleSearchCriteria;
import com.isa.thinair.airpricing.api.dto.FlexiQuoteCriteria;
import com.isa.thinair.airpricing.api.dto.FlexiRuleDTO;
import com.isa.thinair.airpricing.api.model.FlexiRule;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

public interface AirPricingBD {

	public static final String SERVICE_NAME = "AirPricingService";

	// Flexi Charge Rules
	public void createFlexiRule(FlexiRule flexiRule) throws ModuleException;

	public void updateFlexiRule(FlexiRule flexiRule) throws ModuleException;

	public void deleteFlexiRule(int flexiRuleId) throws ModuleException;

	public Page getFlexiRules(FlexiRuleSearchCriteria flexiRuleSearchCriteria, int startIndex, int pageSize, List<String> orderbyfields)
			throws ModuleException;

	public HashMap<String, HashMap<String, FlexiRuleDTO>> quoteFlexiCharges(Collection<FlexiQuoteCriteria> colCriteria)
			throws ModuleException;

	/**
	 * This method recreate the flexi chargers for a given flexiIds
	 * 
	 * @param flexiIdCollection
	 * @param criteria
	 * @return
	 * @throws ModuleException
	 */
	public HashMap<String, FlexiRuleDTO> rebuildFlexiCharges(Collection<Integer> flexiIdCollection, FlexiQuoteCriteria criteria)
			throws ModuleException;

	public List<FlexiRuleDTO> requoteFlexiCharges(Collection<Integer> flexiRuleRateIds) throws ModuleException;

}
