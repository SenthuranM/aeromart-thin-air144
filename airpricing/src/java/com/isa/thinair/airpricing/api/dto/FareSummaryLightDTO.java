package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;

import com.isa.thinair.airpricing.api.model.FareRule;

public class FareSummaryLightDTO implements Serializable, Comparable<FareSummaryLightDTO> {

	private static final long serialVersionUID = 2292724096904655509L;

	private int fareId;

	private double fareAmount;

	private double childFareAmount;

	private double infantFareAmount;

	private String childFareType;

	private String infantFareType;

	private int fareRuleId;

	private String returnFlag;

	public double getFareAmount() {
		return fareAmount;
	}

	public void setFareAmount(double fareAmount) {
		this.fareAmount = fareAmount;
	}

	public int getFareId() {
		return fareId;
	}

	public void setFareId(int fareId) {
		this.fareId = fareId;
	}

	public int getFareRuleId() {
		return fareRuleId;
	}

	public void setFareRuleId(int fareRuleId) {
		this.fareRuleId = fareRuleId;
	}

	/**
	 * @return the childFareAmount
	 */
	public double getChildFareAmount() {
		return childFareAmount;
	}

	/**
	 * @param childFareAmount
	 *            the childFareAmount to set
	 */
	public void setChildFareAmount(double childFareAmount) {
		this.childFareAmount = childFareAmount;
	}

	/**
	 * @return the infantFareAmount
	 */
	public double getInfantFareAmount() {
		return infantFareAmount;
	}

	/**
	 * @param infantFareAmount
	 *            the infantFareAmount to set
	 */
	public void setInfantFareAmount(double infantFareAmount) {
		this.infantFareAmount = infantFareAmount;
	}

	/**
	 * @return the childFareType
	 */
	public String getChildFareType() {
		return childFareType;
	}

	/**
	 * @param childFareType
	 *            the childFareType to set
	 */
	public void setChildFareType(String childFareType) {
		this.childFareType = childFareType;
	}

	/**
	 * @return the infantFareType
	 */
	public String getInfantFareType() {
		return infantFareType;
	}

	/**
	 * @param infantFareType
	 *            the infantFareType to set
	 */
	public void setInfantFareType(String infantFareType) {
		this.infantFareType = infantFareType;
	}

	public String getReturnFlag() {
		return returnFlag;
	}

	public void setReturnFlag(String returnFlag) {
		this.returnFlag = returnFlag;
	}

	//Sorting according to Journey type
	@Override
	public int compareTo(FareSummaryLightDTO fareSummaryLightDTO) {
		if ("N".equals(fareSummaryLightDTO.getReturnFlag())) {
			if ("N".equals(this.returnFlag)) {
				if (fareSummaryLightDTO.getFareAmount() > this.getFareAmount()) {
					return -1;
				} else {
					return 1;
				}
			}
		} else if ("Y".equals(fareSummaryLightDTO.getReturnFlag())) {
			if ("N".equals(this.returnFlag)) {
				return -1;
			} else if ("Y".equals(this.returnFlag)) {
				if (fareSummaryLightDTO.getFareAmount() > this.getFareAmount()) {
					return -1;
				} else {
					return 1;
				}
			}
		}
		return 1;
	}

}
