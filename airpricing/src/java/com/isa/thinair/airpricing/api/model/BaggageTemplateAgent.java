package com.isa.thinair.airpricing.api.model;

import java.io.Serializable;

import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * @hibernate.class table = "BG_T_OND_TEMPLATE_AGENT"
 */
public class BaggageTemplateAgent implements Serializable {

	private Integer id;
	private ONDBaggageTemplate baggageTemplate;
	private String agentCode;

	/**
	 * @hibernate.id column = "OND_TEMPLATE_AGENT_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "BG_S_OND_TEMPLATE_AGENT"
	 */
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @hibernate.many-to-one column="OND_CHARGE_TEMPLATE_ID" class="com.isa.thinair.airpricing.api.model.ONDBaggageTemplate"
	 */
	public ONDBaggageTemplate getBaggageTemplate() {
		return baggageTemplate;
	}

	public void setBaggageTemplate(ONDBaggageTemplate baggageTemplate) {
		this.baggageTemplate = baggageTemplate;
	}

	/**
	 * @hibernate.property column = "AGENT_CODE"
	 */
	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		BaggageTemplateAgent agent = (BaggageTemplateAgent) o;

		if (agentCode != null ? !agentCode.equals(agent.agentCode) : agent.agentCode != null) return false;

		return true;
	}

	public int hashCode() {
		return new HashCodeBuilder().append(getAgentCode()).toHashCode();
	}
}
