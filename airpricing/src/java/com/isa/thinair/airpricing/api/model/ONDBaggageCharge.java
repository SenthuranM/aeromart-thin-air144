/**
 * 
 */
package com.isa.thinair.airpricing.api.model;

import java.math.BigDecimal;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @author kasun
 * @hibernate.class table = "BG_T_OND_BAGGAGE_CHARGE"
 */
public class ONDBaggageCharge extends Persistent {

	/**
	 * 
	 */
	// TODO generate proper serialVersionUID
	private static final long serialVersionUID = 1L;
	
	private Integer chargeId;

	private ONDBaggageTemplate template;

	private Integer baggageId;

	private BigDecimal amount;

	private Integer allocatedPieces;

	private String status;

	private String baggageName;

	private String cabinClass;

	private String logicalCCCode;

	private String defaultBaggageFlag;

	private Double seatFactor;

	private BigDecimal totalWeightLimit;
	
	private Integer seatFactorLimitationApplyFor;
	
	private BigDecimal localCurrencyAmount;
	/**
	 * @param chargeId
	 *            the chargeId to set
	 */
	public void setChargeId(Integer chargeId) {
		this.chargeId = chargeId;
	}

	/**
	 * @hibernate.id column = "OND_BAGGAGE_CHARGE_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "BG_S_OND_BAGG_CHARGE_TEMPLATE"
	 * 
	 * @return Returns the chargeId.
	 */
	public Integer getChargeId() {
		return chargeId;
	}

	/**
	 * @param template the template to set
	 */
	public void setTemplate(ONDBaggageTemplate template) {
		this.template = template;
	}

	/**
	 * @return Returns the templateCode.
	 * @hibernate.many-to-one column = "OND_CHARGE_TEMPLATE_ID" class=
	 *                        "com.isa.thinair.airpricing.api.model.ONDBaggageTemplate"
	 */
	public ONDBaggageTemplate getTemplate() {
		return template;
	}

	/**
	 * @param baggageId the baggageId to set
	 */
	public void setBaggageId(Integer baggageId) {
		this.baggageId = baggageId;
	}

	/**
	 * @return the baggageId
	 * @hibernate.property column = "BAGGAGE_ID"
	 */
	public Integer getBaggageId() {
		return baggageId;
	}

	/**
	 * @param amount
	 *            the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return the amount
	 * @hibernate.property column = "AMOUNT"
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param allocatedPieces
	 *            the allocatedPieces to set
	 */
	public void setAllocatedPieces(Integer allocatedPieces) {
		this.allocatedPieces = allocatedPieces;
	}

	/**
	 * @return the allocatedPieces
	 * @hibernate.property column = "ALLOCATED_PIECES"
	 */
	public Integer getAllocatedPieces() {
		return allocatedPieces;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the status
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param baggageName the baggageName to set
	 */
	public void setBaggageName(String baggageName) {
		this.baggageName = baggageName;
	}

	/**
	 * @return the baggageName
	 */
	public String getBaggageName() {
		return baggageName;
	}

	/**
	 * @param cabinClass the cabinClass to set
	 */
	public void setCabinClass(String cabinClass) {
		this.cabinClass = cabinClass;
	}

	/**
	 * @return the cabinClass
	 * @hibernate.property column = "CABIN_CLASS_CODE"
	 */
	public String getCabinClass() {
		return cabinClass;
	}

	/**
	 * @param logicalCCCode
	 *            the logicalCCCode to set
	 */
	public void setLogicalCCCode(String logicalCCCode) {
		this.logicalCCCode = logicalCCCode;
	}

	/***
	 * @return
	 * @hibernate.property column = "LOGICAL_CABIN_CLASS_CODE"
	 */
	public String getLogicalCCCode() {
		return logicalCCCode;
	}

	/**
	 * @param defaultBaggageFlag the defaultBaggageFlag to set
	 */
	public void setDefaultBaggageFlag(String defaultBaggageFlag) {
		this.defaultBaggageFlag = defaultBaggageFlag;
	}

	/**
	 * @return the defaultBaggageFlag
	 * @hibernate.property column = "IS_DEFAULT"
	 */
	public String getDefaultBaggageFlag() {
		return defaultBaggageFlag;
	}

	/**
	 * @param seatFactor the seatFactor to set
	 */
	public void setSeatFactor(Double seatFactor) {
		this.seatFactor = seatFactor;
	}

	/**
	 * @return the defaultBaggageFlag
	 * @hibernate.property column = "SEAT_FACTOR"
	 */
	public Double getSeatFactor() {
		return seatFactor;
	}

	public void setTotalWeightLimit(BigDecimal totalWeightLimit) {
		this.totalWeightLimit = totalWeightLimit;
	}

	/**
	 * @hibernate.property column = "TOTAL_WEIGHT_LIMIT"
	 */
	public BigDecimal getTotalWeightLimit() {
		return totalWeightLimit;
	}

	/**
	 * @hibernate.property column = "SEAT_FACTOR_COND_APPLY_FOR"
	 */
	public Integer getSeatFactorLimitationApplyFor() {
		return seatFactorLimitationApplyFor;
	}

	public void setSeatFactorLimitationApplyFor(Integer seatFactorLimitationApplyFor) {
		this.seatFactorLimitationApplyFor = seatFactorLimitationApplyFor;
	}
	
	/**
	 * @return the localCurrencyAmount
	 * @hibernate.property column = "LOCAL_CURR_AMOUNT"
	 */
	public BigDecimal getLocalCurrencyAmount() {
		return localCurrencyAmount;
	}

	public void setLocalCurrencyAmount(BigDecimal localCurrencyAmount) {
		this.localCurrencyAmount = localCurrencyAmount;
	}
}
