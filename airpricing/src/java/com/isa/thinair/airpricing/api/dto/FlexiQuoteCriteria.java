package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import com.isa.thinair.airinventory.api.dto.seatavailability.FareSummaryDTO;
import com.isa.thinair.commons.core.util.CalendarUtil;

/**
 * Criteria for quoting flexi charges for an OND.
 * 
 * @author Dhanya
 */
public class FlexiQuoteCriteria implements Serializable {

	private static final long serialVersionUID = 1L;

	private int fareRuleId;

	Date departureDate;

	Date departureZuluDate;

	String ondCode;

	private boolean offerFlexiFreeOfCharge;

	private FareSummaryDTO fareSummaryDTO;

	public FlexiQuoteCriteria() {
	}

	public FlexiQuoteCriteria(FareSummaryDTO fareSummaryDTO, Date departureDate, Date departureZuluDate, String ondCode,
			boolean offerFlexiFreeOfCharge) {
		setFareSummaryDTO(fareSummaryDTO);
		setFareRuleID(fareSummaryDTO.getFareRuleID());
		setDepartureDate(departureDate);
		setDepartureZuluDate(departureZuluDate);
		setOndCode(ondCode);
		setOfferFlexiFreeOfCharge(offerFlexiFreeOfCharge);
	}

	public int getFareRuleId() {
		return this.fareRuleId;
	}

	public void setFareRuleID(int fareRuleId) {
		this.fareRuleId = fareRuleId;
	}

	public String getOndCode() {
		return this.ondCode;
	}

	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}

	/**
	 * @return Returns the fareSummaryDTO.
	 */
	public FareSummaryDTO getFareSummaryDTO() {
		return this.fareSummaryDTO;
	}

	/**
	 * @param fareSummaryDTO
	 *            The fareSummaryDTO to set.
	 */
	public void setFareSummaryDTO(FareSummaryDTO fareSummaryDTO) {
		this.fareSummaryDTO = fareSummaryDTO;
	}

	public java.sql.Date getDepartureDate() {
		return new java.sql.Date(departureDate.getTime());
	}

	public void setDepartureDate(Date depatureDateTime) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(depatureDateTime);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		this.departureDate = cal.getTime();
	}

	public long getDepartureTimeGap() {
		return departureZuluDate.getTime() - CalendarUtil.getCurrentSystemTimeInZulu().getTime();
	}

	public void setDepartureZuluDate(Date departureZuluDate) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(departureZuluDate);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		this.departureZuluDate = cal.getTime();
	}

	public boolean isOfferFlexiFreeOfCharge() {
		return offerFlexiFreeOfCharge;
	}

	public void setOfferFlexiFreeOfCharge(boolean offerFlexiFreeOfCharge) {
		this.offerFlexiFreeOfCharge = offerFlexiFreeOfCharge;
	}

}
