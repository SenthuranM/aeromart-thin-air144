package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.exception.ModuleException;

public class BaggageTemplateTo implements Serializable {

	private Integer templateId;
	private String templateCode;
	private String description;
	private String status;
	private Date fromDate;
	private Date toDate;
	private List<String> onds;
	private Map<String, String> flights;
	private Map<String, String> agents;
	private Map<String, String> bookingClasses;
	private Map<String, String> salesChannels;
	
	public Integer getTemplateId() {
		return templateId;
	}

	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}

	public String getTemplateCode() {
		return templateCode;
	}

	public void setTemplateCode(String templateCode) {
		this.templateCode = templateCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public List<String> getOnds() {
		return onds;
	}

	public void setOnds(List<String> onds) {
		this.onds = onds;
	}

	public Map<String, String> getFlights() {
		return flights;
	}

	public void setFlights(Map<String, String> flights) {
		this.flights = flights;
	}

	public Map<String, String> getAgents() {
		return agents;
	}

	public void setAgents(Map<String, String> agents) {
		this.agents = agents;
	}

	public Map<String, String> getBookingClasses() {
		return bookingClasses;
	}

	public void setBookingClasses(Map<String, String> bookingClasses) {
		this.bookingClasses = bookingClasses;
	}

	
	public Map<String, String> getSalesChannels() {
		return salesChannels;
	}

	public void setSalesChannels(Map<String, String> salesChannels) {
		this.salesChannels = salesChannels;
	}

	public void validateRules() throws ModuleException {

		// ond validations
		String origin = null;
		String destination = null;
		String[] ondPoints;
		int i;

		if (onds != null ) {
			for (String ond : onds) {
				ondPoints = ond.split("/");
				i = 0;
				for (String s : ondPoints) {
					if (i == 0)
						origin = ondPoints[0].trim();
					if (i + 1 == ondPoints.length)
						destination = ondPoints[i].trim();
					i++;
				}


				// Only ALL/ALL is allowed. ALL/SHJ/ALL not allowed.
				if (origin.equals("All") && destination.equals("All")) {
					if (!ond.equals("All/All")) {
						throw new ModuleException("airinventory.logic.ondbaggage.departure.arrival.mismatch");
					}
				}

				// Only ALL/XXX or XXX/ALL is allowed. XXX/YYY/ALL is not allowed.
				if (origin.equals("All") || destination.equals("All")) {
					if (ondPoints.length > 2) {
						throw new ModuleException("airinventory.logic.ondbaggage.partial.ond.all.not.supported");
					}
				}
			}
		}

		if (flights != null) {
			if (flights.size() > 1 && flights.containsKey(CommonsConstants.ALL_DESIGNATOR_STRING)) {
				throw new ModuleException("airinventory.logic.baggage.template.conflict.all.flight");
			}
		}

		if (agents != null) {
			if (agents.size() > 1 && agents.containsKey(CommonsConstants.ALL_DESIGNATOR_STRING)) {
				throw new ModuleException("airinventory.logic.baggage.template.conflict.all.agents");
			}
		}

		if (bookingClasses != null) {
			if (bookingClasses.size() > 1 && bookingClasses.containsKey(CommonsConstants.ALL_DESIGNATOR_STRING)) {
				throw new ModuleException("airinventory.logic.baggage.template.conflict.all.bookingclasses");
			}
		}
	}


}
