package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

public class AvailableSSRChargeDTO implements Serializable {

	private static final long serialVersionUID = 3791306282364983488L;
	/**
	 * Map<ssrChargeCode, Collection<ssrChargeDTO>>
	 */
	private Map<String, Collection<SSRChargeDTO>> sSRChargeDTOMap;

	public Map<String, Collection<SSRChargeDTO>> getSSRChargeDTOMap() {
		return this.sSRChargeDTOMap;
	}

	public void setSSRChargeDTOMap(Map<String, Collection<SSRChargeDTO>> chargeDTOMap) {
		this.sSRChargeDTOMap = chargeDTOMap;
	}

}
