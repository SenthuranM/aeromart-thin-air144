package com.isa.thinair.airpricing.api.model;

import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Tracking;

/** 
 * @author manoji
 * @hibernate.class table = "T_MCO"
 */
public class MCO extends Tracking{
	
	private Integer mcoId;
	private String mcoNumber;
	private Integer pnrPaxId;
	private Integer mcoServiceId;
	private String flightNo;
	private Date flightDate;
	private BigDecimal amount;
	private BigDecimal amountInLocal;
	private String currency;
	private String status;
	private String email;
	private String remarks;
	private String ond;
	
	public static final String FLOWN = "F";
	public static final String VOID = "V";	
	
	/**
	 * @return
	 * @hibernate.id column = "MCO_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_MCO"
	 */	
	public Integer getMcoId() {
		return mcoId;
	}
	public void setMcoId(Integer mcoId) {
		this.mcoId = mcoId;
	}
	
	/**
	 * @return
	 * @hibernate.property column = "MCO_NUMBER"
	 */
	public String getMcoNumber() {
		return mcoNumber;
	}
	public void setMcoNumber(String mcoNumber) {
		this.mcoNumber = mcoNumber;
	}
	
	/**
	 * @return
	 * @hibernate.property column = "PNR_PAX_ID"
	 */
	public Integer getPnrPaxId() {
		return pnrPaxId;
	}
	public void setPnrPaxId(Integer pnrPaxId) {
		this.pnrPaxId = pnrPaxId;
	}
	
	/**
	 * @return
	 * @hibernate.property column = "FLIGHT_NO"
	 */
	public String getFlightNo() {
		return flightNo;
	}
	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}
	
	/**
	 * @return
	 * @hibernate.property column = "FLIGHT_DATE"
	 */
	public Date getFlightDate() {
		return flightDate;
	}
	public void setFlightDate(Date flightDate) {
		this.flightDate = flightDate;
	}
	
	/**
	 * @return
	 * @hibernate.property column = "AMOUNT"
	 */
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
	/**
	 * @return
	 * @hibernate.property column = "AMOUNT_IN_LOCAL"
	 */
	public BigDecimal getAmountInLocal() {
		return amountInLocal;
	}
	public void setAmountInLocal(BigDecimal amountInLocal) {
		this.amountInLocal = amountInLocal;
	}
	
	/**
	 * @return
	 * @hibernate.property column = "CURRENCY_CODE"
	 */
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	/**
	 * @return
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	/**
	 * @return
	 * @hibernate.property column = "EMAIL"
	 */
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	/**
	 * @return
	 * @hibernate.property column = "REMARKS"
	 */
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	/**
	 * @return
	 * @hibernate.property column = "MCO_SERVICE_ID"
	 */
	public Integer getMcoServiceId() {
		return mcoServiceId;
	}
	public void setMcoServiceId(Integer mcoServiceId) {
		this.mcoServiceId = mcoServiceId;
	}
	
	/**
	 * @return
	 * @hibernate.property column = "OND_CODE"
	 */
	public String getOnd() {
		return ond;
	}
	public void setOnd(String ond) {
		this.ond = ond;
	}
	
}



