/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:20:59
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airpricing.api.model;

import java.util.Collection;
import java.util.Date;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * 
 * @author : Code Generation Tool
 * @version : Ver 1.0.0
 * 
 * @hibernate.class table = "T_OND_FARE"
 * 
 *                  OriginDestinationFare is the entity class to repesent a OriginDestinationFare model
 * 
 */
public class Fare extends Persistent {

	private static final long serialVersionUID = -583139352247969284L;

	public static final String STATUS_ACTIVE = "ACT";

	public static final String STATUS_INACTIVE = "INA";

	public static final String VALUE_PERCENTAGE_FLAG_V = "V";

	public static final String VALUE_PERCENTAGE_FLAG_P = "P";
	
	public static final String FARE_DEF_TYPE_MASTER = "MASTER";
	public static final String FARE_DEF_TYPE_NORMAL = "NORMAL";
	public static final String FARE_DEF_TYPE_LINK = "LINK";

	public static final String MODIFY_TO_SAME_FARE_YES = "Y";
	public static final String MODIFY_TO_SAME_FARE_NO = "N";

	private Integer fareId;

	private Date effectiveFromDate;

	private Date effectiveToDate;

	private double fareAmount;

	private String status;

	private String ondCode;

	private Integer fareRuleId;

	private String bookingCode;

	private String childFareType;

	private double childFare;

	private String infantFareType;

	private double infantFare;

	private Date salesEffectiveFrom;

	private Date salesEffectiveTo;

	private Double fareAmountInLocal;

	private Double childFareInLocal;

	private Double infantFareInLocal;

	private String localCurrencyCode;

	private boolean localCurrencySelected = false;

	private Date returnEffectiveFromDate;

	private Date returnEffectiveToDate;

	// this is to differentiate how fares added
	private boolean isUploaded;
	
	private String fareDefType = FARE_DEF_TYPE_NORMAL;
	
	private Integer masterFareID;
	
	private String masterFareRefCode;
	
	private Integer masterFarePercentage;
	
	private Collection<Proration> prorations;

	private String modifyToSameFare = MODIFY_TO_SAME_FARE_NO;

	/**
	 * returns the fareId
	 * 
	 * @return Returns the fareId.
	 * 
	 * @hibernate.id column = "FARE_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_OND_FARE"
	 */
	public Integer getFareId() {
		return fareId;
	}

	/**
	 * sets the fareId
	 * 
	 * @param fareId
	 *            The fareId to set.
	 */
	public void setFareId(Integer fareId) {
		this.fareId = fareId;
	}

	/**
	 * returns the effectiveFromDate
	 * 
	 * @return Returns the effectiveFromDate.
	 * 
	 * @hibernate.property column = "EFFECTIVE_FROM_DATE"
	 */
	public Date getEffectiveFromDate() {
		return effectiveFromDate;
	}

	/**
	 * sets the effectiveFromDate
	 * 
	 * @param effectiveFromDate
	 *            The effectiveFromDate to set.
	 */
	public void setEffectiveFromDate(Date effectiveFromDate) {
		this.effectiveFromDate = effectiveFromDate;
	}

	/**
	 * returns the effectiveToDate
	 * 
	 * @return Returns the effectiveToDate.
	 * 
	 * @hibernate.property column = "EFFECTIVE_TO_DATE"
	 */
	public Date getEffectiveToDate() {
		return effectiveToDate;
	}

	/**
	 * sets the effectiveToDate
	 * 
	 * @param effectiveToDate
	 *            The effectiveToDate to set.
	 */
	public void setEffectiveToDate(Date effectiveToDate) {
		this.effectiveToDate = effectiveToDate;
	}

	/**
	 * returns the status
	 * 
	 * @return Returns the status.
	 * 
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * sets the status
	 * 
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @hibernate.property column = "OND_CODE"
	 * 
	 */
	public String getOndCode() {
		return ondCode;
	}

	/**
	 * 
	 */
	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}

	/**
	 * @return Returns the bookingCode.
	 * @hibernate.property column = "BOOKING_CODE"
	 */
	public String getBookingCode() {
		return bookingCode;
	}

	/**
	 * @param bookingCode
	 *            The bookingCode to set.
	 */
	public void setBookingCode(String bookingCode) {
		this.bookingCode = bookingCode;
	}

	/**
	 * @return Returns the fareAmount.
	 * @hibernate.property column = "FARE_AMOUNT"
	 */
	public double getFareAmount() {
		return fareAmount;
	}

	/**
	 * @param fareAmount
	 *            The fareAmount to set.
	 */
	public void setFareAmount(double fareAmount) {
		this.fareAmount = fareAmount;
	}

	/**
	 * @return Returns the fareRuleId.
	 * @hibernate.property column = "FARE_RULE_ID"
	 */
	public Integer getFareRuleId() {
		return fareRuleId;
	}

	/**
	 * @param fareRuleId
	 *            The fareRuleId to set.
	 */
	public void setFareRuleId(Integer fareRuleId) {
		this.fareRuleId = fareRuleId;
	}

	/**
	 * @return the childFare
	 * @hibernate.property column = "CHILD_FARE"
	 */
	public double getChildFare() {
		return childFare;
	}

	/**
	 * @param childFare
	 *            the childFare to set
	 */
	public void setChildFare(double childFare) {
		this.childFare = childFare;
	}

	/**
	 * @return the childFareType
	 * @hibernate.property column = "CHILD_FARE_TYPE"
	 */
	public String getChildFareType() {
		return childFareType;
	}

	/**
	 * @param childFareType
	 *            the childFareType to set
	 */
	public void setChildFareType(String childFareType) {
		this.childFareType = childFareType;
	}

	/**
	 * @return the infantFare
	 * @hibernate.property column = "INFANT_FARE"
	 */
	public double getInfantFare() {
		return infantFare;
	}

	/**
	 * @param infantFare
	 *            the infantFare to set
	 */
	public void setInfantFare(double infantFare) {
		this.infantFare = infantFare;
	}

	/**
	 * @return the infantFareType
	 * @hibernate.property column = "INFANT_FARE_TYPE"
	 */
	public String getInfantFareType() {
		return infantFareType;
	}

	/**
	 * @param infantFareType
	 *            the infantFareType to set
	 */
	public void setInfantFareType(String infantFareType) {
		this.infantFareType = infantFareType;
	}

	/**
	 * @hibernate.property column = "SALES_VALID_FROM"
	 * @return the salesEffectiveFrom
	 */
	public Date getSalesEffectiveFrom() {
		return salesEffectiveFrom;
	}

	/**
	 * @param salesEffectiveFrom
	 *            the salesEffectiveFrom to set
	 */
	public void setSalesEffectiveFrom(Date salesEffectiveFrom) {
		this.salesEffectiveFrom = salesEffectiveFrom;
	}

	/**
	 * @hibernate.property column = "SALES_VALID_TO"
	 * @return the salesEffectiveTo
	 */
	public Date getSalesEffectiveTo() {
		return salesEffectiveTo;
	}

	/**
	 * @param salesEffectiveTo
	 *            the salesEffectiveTo to set
	 */
	public void setSalesEffectiveTo(Date salesEffectiveTo) {
		this.salesEffectiveTo = salesEffectiveTo;
	}

	/**
	 * @return Returns the childFareInLocal.
	 * @hibernate.property column = "CHILD_FARE_IN_LOCAL"
	 */
	public Double getChildFareInLocal() {
		return childFareInLocal;
	}

	/**
	 * @param childFareInLocal
	 *            The childFareInLocal to set.
	 */
	public void setChildFareInLocal(Double childFareInLocal) {
		this.childFareInLocal = childFareInLocal;
	}

	/**
	 * @return Returns the fareAmountInLocal.
	 * @hibernate.property column = "FARE_AMOUNT_IN_LOCAL"
	 */
	public Double getFareAmountInLocal() {
		return fareAmountInLocal;
	}

	/**
	 * @param fareAmountInLocal
	 *            The fareAmountInLocal to set.
	 */
	public void setFareAmountInLocal(Double fareAmountInLocal) {
		this.fareAmountInLocal = fareAmountInLocal;
	}

	/**
	 * @return Returns the infantFareInLocal.
	 * @hibernate.property column = "INFANT_FARE_IN_LOCAL"
	 */
	public Double getInfantFareInLocal() {
		return infantFareInLocal;
	}

	/**
	 * @param infantFareInLocal
	 *            The infantFareInLocal to set.
	 */
	public void setInfantFareInLocal(Double infantFareInLocal) {
		this.infantFareInLocal = infantFareInLocal;
	}

	/**
	 * @return Returns the localCurrencyCode.
	 * @hibernate.property column = "CURRENCY_CODE"
	 */
	public String getLocalCurrencyCode() {
		return localCurrencyCode;
	}

	/**
	 * @param localCurrencyCode
	 *            The localCurrencyCode to set.
	 */
	public void setLocalCurrencyCode(String localCurrencyCode) {
		this.localCurrencyCode = localCurrencyCode;
	}

	/**
	 * @return Returns true if local Currency Selected
	 */
	public boolean isLocalCurrencySelected() {
		return localCurrencySelected;
	}

	/**
	 * @param localCurrencySelected
	 *            The boolean to set if local currency is selected.
	 */
	public void setLocalCurrencySelected(boolean localCurrencySelected) {
		this.localCurrencySelected = localCurrencySelected;
	}

	/**
	 * returns the returnEffectiveFromDate
	 * 
	 * @return Returns the returnEffectiveFromDate.
	 * 
	 * @hibernate.property column = "RT_EFFECTIVE_FROM_DATE"
	 */
	public Date getReturnEffectiveFromDate() {
		return returnEffectiveFromDate;
	}

	/**
	 * sets the returnEffectiveFromDate
	 * 
	 * @param returnEffectiveFromDate
	 *            The returnEffectiveFromDate to set.
	 */
	public void setReturnEffectiveFromDate(Date returnEffectiveFromDate) {
		this.returnEffectiveFromDate = returnEffectiveFromDate;
	}

	/**
	 * returns the returnEffectiveToDate
	 * 
	 * @return Returns the returnEffectiveToDate.
	 * 
	 * @hibernate.property column = "RT_EFFECTIVE_TO_DATE"
	 */
	public Date getReturnEffectiveToDate() {
		return returnEffectiveToDate;
	}

	/**
	 * sets the returnEffectiveToDate
	 * 
	 * @param returnEffectiveToDate
	 *            The returnEffectiveToDate to set.
	 */
	public void setReturnEffectiveToDate(Date returnEffectiveToDate) {
		this.returnEffectiveToDate = returnEffectiveToDate;
	}

	public boolean isUploaded() {
		return isUploaded;
	}

	public void setUploaded(boolean isUploaded) {
		this.isUploaded = isUploaded;
	}

	/**
	 * @return Returns the masterFareStatus.
	 * @hibernate.property column = "FARE_DEF_TYPE"
	 */
	public String getFareDefType() {
		return fareDefType;
	}

	/**
	 * @param masterFareStatus
	 *            The masterFareStatus to set.
	 */
	public void setFareDefType(String fareDefType) {
		this.fareDefType = fareDefType;
	}

	/**
	 * @return Returns the masterFareID.
	 * @hibernate.property column = "MASTER_FARE_ID"
	 */
	public Integer getMasterFareID() {
		return masterFareID;
	}

	/**
	 * @param masterFareID
	 *            The masterFareID to set.
	 */
	public void setMasterFareID(Integer masterFareID) {
		this.masterFareID = masterFareID;
	}

	/**
	 * @return Returns the masterFareRefCode.
	 * @hibernate.property column = "MASTER_FARE_REF_CODE"
	 */
	public String getMasterFareRefCode() {
		return masterFareRefCode;
	}

	/**
	 * @param masterFareRefCode
	 *            The masterFareRefCode to set.
	 */
	public void setMasterFareRefCode(String masterFareRefCode) {
		this.masterFareRefCode = masterFareRefCode;
	}

	/**
	 * @return Returns the masterFarePercentage.
	 * @hibernate.property column = "MASTER_FARE_PERCENTAGE"
	 */
	public Integer getMasterFarePercentage() {
		return masterFarePercentage;
	}

	/**
	 * @param masterFarePercentage
	 *            The masterFarePercentage to set.
	 */
	public void setMasterFarePercentage(Integer masterFarePercentage) {
		this.masterFarePercentage = masterFarePercentage;
	}

	/**
	 * @return Returns the modifyToSameFare.
	 * @hibernate.property column = "MODIFY_TO_SAME_FARE"
	 */
	public String getModifyToSameFare() {
		return modifyToSameFare;
	}

	/**
	 * @param modifyToSameFare
	 *            The modifyToSameFare to set.
	 */
	public void setModifyToSameFare(String modifyToSameFare) {
		this.modifyToSameFare = modifyToSameFare;
	}
	
	/**
	 * @return Returns the prorations as Collection of Proration objects
	 * 
	 * @hibernate.set lazy="false" cascade="all-delete-orphan" inverse="true"
	 * 
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airpricing.api.model.Proration"
	 * 
	 * @hibernate.collection-key column="FARE_ID"
	 */
 
	public Collection<Proration> getProrations() {
		return prorations;
	}
	
	/**
	 * 
	 * @param prorations
	 * 				The prorations to set
	 */
	public void setProrations(Collection<Proration> prorations) {
		this.prorations = prorations;
	}
	
	public int hashCode() {
		return new HashCodeBuilder().append(getFareId()).toHashCode();
	}

}
