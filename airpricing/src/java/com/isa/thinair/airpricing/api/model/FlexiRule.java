package com.isa.thinair.airpricing.api.model;

import java.util.Set;

import com.isa.thinair.commons.core.framework.Tracking;

public class FlexiRule extends Tracking {

	private static final long serialVersionUID = 7013214680483512131L;

	private int flexiRuleId;

	private String flexiCode;

	private String description;

	private String status;

	private String chargeCode;

	private String journeyType;

	private Set<FlexiRuleRate> flexiRuleRates;

	private Set<FlexiRuleFlexibility> flexiRuleFlexibilities;

	/**
	 * @return Returns the flexiRuleId.
	 * @hibernate.id column = "FLEXI_RULE_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_FLEXI_RULE"
	 */
	public int getFlexiRuleId() {
		return flexiRuleId;
	}

	/**
	 * @param flexiRuleId
	 *            The flexiRuleId to set.
	 */
	public void setFlexiRuleId(int flexiRuleId) {
		this.flexiRuleId = flexiRuleId;
	}

	/**
	 * returns the flexiCode
	 * 
	 * @return Returns the flexiCode.
	 * @hibernate.id column = "FLEXI_CODE" generator-class = "assigned"
	 */
	public String getFlexiCode() {
		return flexiCode;
	}

	/**
	 * sets the flexiCode
	 * 
	 * @param flexiCode
	 *            The flexiCode to set.
	 */
	public void setFlexiCode(String flexiCode) {
		this.flexiCode = flexiCode;
	}

	/**
	 * returns the description
	 * 
	 * @return Returns the description.
	 * 
	 * @hibernate.property column = "DESCRIPTION"
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * sets the description
	 * 
	 * @param description
	 *            The description to set.
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * returns the chargeCode
	 * 
	 * @return Returns the chargeCode.
	 * @hibernate.id column = "CHARGE_CODE" generator-class = "assigned"
	 */
	public String getChargeCode() {
		return chargeCode;
	}

	/**
	 * sets the chargeCode
	 * 
	 * @param chargeCode
	 *            The chargeCode to set.
	 */
	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

	/**
	 * returns the status
	 * 
	 * @return Returns the status.
	 * 
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * sets the status
	 * 
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return Returns the journeyType.
	 * @hibernate.property column = "JOURNEY_TYPE"
	 */
	public String getJourneyType() {
		return journeyType;
	}

	/**
	 * @param journeyType
	 *            The journeyType to set.
	 */
	public void setJourneyType(String journeyType) {
		this.journeyType = journeyType;
	}

	/**
	 * returns the flexi rule rates related with this flexi rule
	 * 
	 * @hibernate.set lazy="false" cascade="all"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airpricing.api.model.FlexiRuleRate"
	 * @hibernate.collection-key column="FLEXI_RULE_ID"
	 */
	public Set<FlexiRuleRate> getFlexiRuleRates() {
		return flexiRuleRates;
	}

	public void setFlexiRuleRates(Set<FlexiRuleRate> flexiRuleRates) {
		this.flexiRuleRates = flexiRuleRates;
	}

	/**
	 * returns the flexi rule flexibilities related with this flexi rule
	 * 
	 * @hibernate.set lazy="false" cascade="all"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airpricing.api.model.FlexiRuleFlexibility"
	 * @hibernate.collection-key column="FLEXI_RULE_ID"
	 */
	public Set<FlexiRuleFlexibility> getFlexiRuleFlexibilities() {
		return flexiRuleFlexibilities;
	}

	public void setFlexiRuleFlexibilities(Set<FlexiRuleFlexibility> flexiRuleFlexibilities) {
		this.flexiRuleFlexibilities = flexiRuleFlexibilities;
	}
}
