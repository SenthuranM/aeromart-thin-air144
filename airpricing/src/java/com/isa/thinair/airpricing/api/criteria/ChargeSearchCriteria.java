/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:16:12
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airpricing.api.criteria;

import java.io.Serializable;
import java.util.Date;

public class ChargeSearchCriteria implements Serializable {

	private static final long serialVersionUID = -2355969965388743921L;
	private String chargeCode;
	private String chargeGroupCode;
	private String chargeCategoryCode;
	private String chargeStatus;
	private Date fromDate;
	private Date toDate;
	private Date posStation;
	private boolean onlyChargesWithoutRate;
	private String countryCode;
	private Integer stateId;
	private String interState;
	private String ticketingServiceTax;

	/**
	 * @return Returns the chargeCategoryCode.
	 */
	public String getChargeCategoryCode() {
		return chargeCategoryCode;
	}

	/**
	 * @param chargeCategoryCode
	 *            The chargeCategoryCode to set.
	 */
	public void setChargeCategoryCode(String chargeCategoryCode) {
		this.chargeCategoryCode = chargeCategoryCode;
	}

	/**
	 * @return Returns the chargeGroupCode.
	 */
	public String getChargeGroupCode() {
		return chargeGroupCode;
	}

	/**
	 * @param chargeGroupCode
	 *            The chargeGroupCode to set.
	 */
	public void setChargeGroupCode(String chargeGroupCode) {
		this.chargeGroupCode = chargeGroupCode;
	}

	/**
	 * @return the chargeStatus
	 */
	public String getChargeStatus() {
		return chargeStatus;
	}

	/**
	 * @param chargeStatus the chargeStatus to set
	 */
	public void setChargeStatus(String chargeStatus) {
		this.chargeStatus = chargeStatus;
	}
	
	/**
	 * @return Returns the fromDate.
	 */
	public Date getFromDate() {
		return fromDate;
	}

	/**
	 * @param fromDate
	 *            The fromDate to set.
	 */
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * @return Returns the toDate.
	 */
	public Date getToDate() {
		return toDate;
	}

	/**
	 * @param toDate
	 *            The toDate to set.
	 */
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	/**
	 * @return Returns the chargeCode.
	 */
	public String getChargeCode() {
		return chargeCode;
	}

	/**
	 * @param chargeCode
	 *            The chargeCode to set.
	 */
	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

	/**
	 * @return Returns the onlyChargesWithoutRate.
	 */
	public boolean isOnlyChargesWithoutRate() {
		return onlyChargesWithoutRate;
	}

	/**
	 * @param onlyChargesWithoutRate
	 *            The onlyChargesWithoutRate to set.
	 */
	public void setOnlyChargesWithoutRate(boolean onlyChargesWithoutRate) {
		this.onlyChargesWithoutRate = onlyChargesWithoutRate;
	}

	public Date getPosStation() {
		return posStation;
	}

	public void setPosStation(Date posStation) {
		this.posStation = posStation;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public Integer getStateId() {
		return stateId;
	}

	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}

	public String getInterState() {
		return interState;
	}

	public void setInterState(String interState) {
		this.interState = interState;
	}

	public String getTicketingServiceTax() {
		return ticketingServiceTax;
	}

	public void setTicketingServiceTax(String ticketingServiceTax) {
		this.ticketingServiceTax = ticketingServiceTax;
	}
}
