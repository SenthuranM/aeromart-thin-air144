package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Nilindra Fernando
 */
public class FareRuleFeeTO implements Serializable, Comparable<FareRuleFeeTO> {

	private static final long serialVersionUID = -4436128443873904171L;

	private Long fareRuleFeeId;

	private Integer fareRuleId;

	private String chargeType;

	private String chargeAmountType;

	private BigDecimal chargeAmount;

	private BigDecimal minimumPerChargeAmt;

	private BigDecimal maximumPerChargeAmt;

	private String compareAgainst;

	private String timeType;

	private String timeValue;

	private String status;

	private Date triggerDate;

	/**
	 * @return the fareRuleFeeId
	 */
	public Long getFareRuleFeeId() {
		return fareRuleFeeId;
	}

	/**
	 * @param fareRuleFeeId
	 *            the fareRuleFeeId to set
	 */
	public void setFareRuleFeeId(Long fareRuleFeeId) {
		this.fareRuleFeeId = fareRuleFeeId;
	}

	/**
	 * @return the fareRuleId
	 */
	public Integer getFareRuleId() {
		return fareRuleId;
	}

	/**
	 * @param fareRuleId
	 *            the fareRuleId to set
	 */
	public void setFareRuleId(Integer fareRuleId) {
		this.fareRuleId = fareRuleId;
	}

	/**
	 * @return the chargeType
	 */
	public String getChargeType() {
		return chargeType;
	}

	/**
	 * @param chargeType
	 *            the chargeType to set
	 */
	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}

	/**
	 * @return the chargeAmountType
	 */
	public String getChargeAmountType() {
		return chargeAmountType;
	}

	/**
	 * @param chargeAmountType
	 *            the chargeAmountType to set
	 */
	public void setChargeAmountType(String chargeAmountType) {
		this.chargeAmountType = chargeAmountType;
	}

	/**
	 * @return the chargeAmount
	 */
	public BigDecimal getChargeAmount() {
		return chargeAmount;
	}

	/**
	 * @param chargeAmount
	 *            the chargeAmount to set
	 */
	public void setChargeAmount(BigDecimal chargeAmount) {
		this.chargeAmount = chargeAmount;
	}

	/**
	 * @return the minimumPerChargeAmt
	 */
	public BigDecimal getMinimumPerChargeAmt() {
		return minimumPerChargeAmt;
	}

	/**
	 * @param minimumPerChargeAmt
	 *            the minimumPerChargeAmt to set
	 */
	public void setMinimumPerChargeAmt(BigDecimal minimumPerChargeAmt) {
		this.minimumPerChargeAmt = minimumPerChargeAmt;
	}

	/**
	 * @return the maximumPerChargeAmt
	 */
	public BigDecimal getMaximumPerChargeAmt() {
		return maximumPerChargeAmt;
	}

	/**
	 * @param maximumPerChargeAmt
	 *            the maximumPerChargeAmt to set
	 */
	public void setMaximumPerChargeAmt(BigDecimal maximumPerChargeAmt) {
		this.maximumPerChargeAmt = maximumPerChargeAmt;
	}

	/**
	 * @return the compareAgainst
	 */
	public String getCompareAgainst() {
		return compareAgainst;
	}

	/**
	 * @param compareAgainst
	 *            the compareAgainst to set
	 */
	public void setCompareAgainst(String compareAgainst) {
		this.compareAgainst = compareAgainst;
	}

	/**
	 * @return the timeType
	 */
	public String getTimeType() {
		return timeType;
	}

	/**
	 * @param timeType
	 *            the timeType to set
	 */
	public void setTimeType(String timeType) {
		this.timeType = timeType;
	}

	/**
	 * @return the timeValue
	 */
	public String getTimeValue() {
		return timeValue;
	}

	/**
	 * @param timeValue
	 *            the timeValue to set
	 */
	public void setTimeValue(String timeValue) {
		this.timeValue = timeValue;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the triggerDate
	 */
	public Date getTriggerDate() {
		return triggerDate;
	}

	/**
	 * @param triggerDate
	 *            the triggerDate to set
	 */
	public void setTriggerDate(Date triggerDate) {
		this.triggerDate = triggerDate;
	}

	@Override
	public int compareTo(FareRuleFeeTO o) {
		return this.getTriggerDate().compareTo(o.getTriggerDate());
	}
}
