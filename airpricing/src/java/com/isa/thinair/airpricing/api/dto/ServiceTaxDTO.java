package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class ServiceTaxDTO implements Serializable {
	
	private static final long serialVersionUID = -872752184195676821L;
	
	private BigDecimal amount = AccelAeroCalculator.getDefaultBigDecimalZero();
	
	private String chargeCode;
	
	private int chargeRateId;
	
	private String chargeGroupCode;
	
	private String carrierCode;
	
	private String flightRefNumber;
	
	private BigDecimal taxableAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
	
	private BigDecimal nonTaxableAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

	public BigDecimal getAmount() {
		return amount;
	}

	public String getChargeCode() {
		return chargeCode;
	}

	public String getChargeGroupCode() {
		return chargeGroupCode;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public String getFlightRefNumber() {
		return flightRefNumber;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

	public void setChargeGroupCode(String chargeGroupCode) {
		this.chargeGroupCode = chargeGroupCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public void setFlightRefNumber(String flightRefNumber) {
		this.flightRefNumber = flightRefNumber;
	}

	public BigDecimal getTaxableAmount() {
		return taxableAmount;
	}

	public BigDecimal getNonTaxableAmount() {
		return nonTaxableAmount;
	}

	public void setTaxableAmount(BigDecimal taxableAmount) {
		this.taxableAmount = taxableAmount;
	}

	public void setNonTaxableAmount(BigDecimal nonTaxableAmount) {
		this.nonTaxableAmount = nonTaxableAmount;
	}

	public int getChargeRateId() {
		return chargeRateId;
	}

	public void setChargeRateId(int chargeRateId) {
		this.chargeRateId = chargeRateId;
	}
}
