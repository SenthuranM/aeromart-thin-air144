package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class CnxModPenChargeDTO implements Serializable {
	
	private static final long serialVersionUID = -872722184195676821L;
	
	private BigDecimal amount;
	
	private String chargeGroupCode;
	
	private String chargeCode;

	public BigDecimal getAmount() {
		return amount;
	}

	public String getChargeGroupCode() {
		return chargeGroupCode;
	}

	public String getChargeCode() {
		return chargeCode;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public void setChargeGroupCode(String chargeGroupCode) {
		this.chargeGroupCode = chargeGroupCode;
	}

	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}
}
