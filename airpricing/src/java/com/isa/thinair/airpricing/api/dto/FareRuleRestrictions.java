package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class FareRuleRestrictions implements Serializable {

	private static final long serialVersionUID = 6895811139229264931L;
	private String ondCode;
	private boolean returnFlag;
	private Date outboundDepatureDateTime;
	private Date inboundDepatureDateTime;
	private Date bookingDate;
	private List<Integer> visibileChannels;
	private List<String> agentsList;

	public List<String> getAgentsList() {
		return agentsList;
	}

	public void setAgentsList(List<String> agentsList) {
		this.agentsList = agentsList;
	}

	public Date getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}

	public Date getInboundDepatureDateTime() {
		return inboundDepatureDateTime;
	}

	public void setInboundDepatureDateTime(Date inboundDepatureDateTime) {
		this.inboundDepatureDateTime = inboundDepatureDateTime;
	}

	public String getOndCode() {
		return ondCode;
	}

	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}

	public Date getOutboundDepatureDateTime() {
		return outboundDepatureDateTime;
	}

	public void setOutboundDepatureDateTime(Date outboundDepatureDateTime) {
		this.outboundDepatureDateTime = outboundDepatureDateTime;
	}

	public boolean isReturnFlag() {
		return returnFlag;
	}

	public void setReturnFlag(boolean returnFlag) {
		this.returnFlag = returnFlag;
	}

	public List<Integer> getVisibileChannels() {
		return visibileChannels;
	}

	public void setVisibileChannels(List<Integer> visibileChannels) {
		this.visibileChannels = visibileChannels;
	}
}
