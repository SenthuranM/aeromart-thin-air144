package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class ServiceTaxQuoteForTicketingRevenueRQ implements Serializable {
	
	private static final long serialVersionUID = -872782184195676821L;
	
	private Map<String, List<SimplifiedChargeDTO>> paxTypeWiseCharges;
	
	private Map<SimplifiedPaxDTO, List<SimplifiedChargeDTO>> paxWiseExternalCharges;
	
	private List<SimplifiedFlightSegmentDTO> simplifiedFlightSegmentDTOs;
	
	private String paxState;
	
	private String paxCountryCode;
	
	private boolean paxTaxRegistered;
	
	private int channelCode = -1;

	public Map<String, List<SimplifiedChargeDTO>> getPaxTypeWiseCharges() {
		return paxTypeWiseCharges;
	}

	public String getPaxState() {
		return paxState;
	}

	public boolean isPaxTaxRegistered() {
		return paxTaxRegistered;
	}

	public void setPaxTypeWiseCharges(Map<String, List<SimplifiedChargeDTO>> paxTypeWiseCharges) {
		this.paxTypeWiseCharges = paxTypeWiseCharges;
	}

	public void setPaxState(String paxState) {
		this.paxState = paxState;
	}

	public void setPaxTaxRegistered(boolean paxTaxRegistered) {
		this.paxTaxRegistered = paxTaxRegistered;
	}

	public int getChannelCode() {
		return channelCode;
	}

	public void setChannelCode(int channelCode) {
		this.channelCode = channelCode;
	}

	public List<SimplifiedFlightSegmentDTO> getSimplifiedFlightSegmentDTOs() {
		return simplifiedFlightSegmentDTOs;
	}

	public void setSimplifiedFlightSegmentDTOs(List<SimplifiedFlightSegmentDTO> simplifiedFlightSegmentDTOs) {
		this.simplifiedFlightSegmentDTOs = simplifiedFlightSegmentDTOs;
	}

	public String getPaxCountryCode() {
		return paxCountryCode;
	}

	public void setPaxCountryCode(String paxCountryCode) {
		this.paxCountryCode = paxCountryCode;
	}

	public Map<SimplifiedPaxDTO, List<SimplifiedChargeDTO>> getPaxWiseExternalCharges() {
		return paxWiseExternalCharges;
	}

	public void setPaxWiseExternalCharges(Map<SimplifiedPaxDTO, List<SimplifiedChargeDTO>> paxWiseExternalCharges) {
		this.paxWiseExternalCharges = paxWiseExternalCharges;
	}
	
}
