package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;

/**
 * DTO used when creating a new charge
 * 
 * @author Byorn
 */
public class ChargeTO implements Serializable {

	private static final long serialVersionUID = 8832609644813329211L;
	private int chargeRateId;
	private String chargeCode;
	private String chargeDescription;
	private boolean refundable;
	private boolean refundableOnlyForMOD;
	private String airportTaxCategory;
	private boolean refundableWhenExchange;
	private boolean bundleFareFee;

	/**
	 * @return Returns the chargeRateId.
	 */
	public int getChargeRateId() {
		return chargeRateId;
	}

	/**
	 * @param chargeRateId
	 *            The chargeRateId to set.
	 */
	public void setChargeRateId(int chargeRateId) {
		this.chargeRateId = chargeRateId;
	}

	/**
	 * @return Returns the refundable.
	 */
	public boolean isRefundable() {
		return refundable;
	}

	/**
	 * @param refundable
	 *            The refundable to set.
	 */
	public void setRefundable(boolean refundable) {
		this.refundable = refundable;
	}

	/**
	 * @return Returns the chargeDescription.
	 */
	public String getChargeDescription() {
		return chargeDescription;
	}

	/**
	 * @param chargeDescription
	 *            The chargeDescription to set.
	 */
	public void setChargeDescription(String chargeDescription) {
		this.chargeDescription = chargeDescription;
	}

	/**
	 * @return Returns the chargeCode.
	 */
	public String getChargeCode() {
		return chargeCode;
	}

	/**
	 * @param chargeCode
	 *            The chargeCode to set.
	 */
	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

	public void setAirportTaxCategory(String airportTaxCategory) {
		this.airportTaxCategory = airportTaxCategory;
	}

	public String getAirportTaxCategory() {
		return airportTaxCategory;
	}

	public boolean isAirportTax() {
		return (airportTaxCategory != null);
	}

	public boolean isRefundableOnlyForMOD() {
		return refundableOnlyForMOD;
	}

	public void setRefundableOnlyForMOD(boolean refundableOnlyForMOD) {
		this.refundableOnlyForMOD = refundableOnlyForMOD;
	}

	public boolean isRefundableWhenExchange() {
		return refundableWhenExchange;
	}

	public void setRefundableWhenExchange(boolean refundableWhenExchange) {
		this.refundableWhenExchange = refundableWhenExchange;
	}

	public boolean isBundleFareFee() {
		return bundleFareFee;
	}

	public void setBundleFareFee(boolean bundleFareFee) {
		this.bundleFareFee = bundleFareFee;
	}

}
