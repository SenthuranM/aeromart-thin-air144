/**
 * 
 */
package com.isa.thinair.airpricing.api.criteria;

import java.io.Serializable;

/**
 * @author kasun
 *
 */
public class ONDCodeSearchCriteria implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String status;

	private String ondCode;

	private String templateCodeID;
	private String agentCode;

	private String bookingClass;

	private String flightNumber;
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param ondCode the ondCode to set
	 */
	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}

	/**
	 * @return the ondCode
	 */
	public String getOndCode() {
		return ondCode;
	}

	public String getTemplateCodeID() {
		return templateCodeID;
	}

	public void setTemplateCodeID(String templateCodeID) {
		this.templateCodeID = templateCodeID;
	}
	
	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getBookingClass() {
		return bookingClass;
	}

	public void setBookingClass(String bookingClass) {
		this.bookingClass = bookingClass;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}
}
