package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;
import java.util.Collection;

/** Transfer Object to store agent vise Fare rules **/
public class AgentFareRulesDTO implements Serializable {

	private static final long serialVersionUID = -5458016497582911648L;
	private String agentCode;
	private String agentName;
	private Collection<FareRuleSummaryDTO> fareRulesSummary;
	private Collection<FareRuleSummaryDTO> adhocFareRulesSummary;

	/**
	 * @return Returns the adhocFareRulesSummary.
	 */
	public Collection<FareRuleSummaryDTO> getAdhocFareRulesSummary() {
		return adhocFareRulesSummary;
	}

	/**
	 * @param adhocFareRulesSummary
	 *            The adhocFareRulesSummary to set.
	 */
	public void setAdhocFareRulesSummary(Collection<FareRuleSummaryDTO> adhocFareRulesSummary) {
		this.adhocFareRulesSummary = adhocFareRulesSummary;
	}

	/**
	 * @return Returns the agentCode.
	 */
	public String getAgentCode() {
		return agentCode;
	}

	/**
	 * @param agentCode
	 *            The agentCode to set.
	 */
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * @return Returns the agentName.
	 */
	public String getAgentName() {
		return agentName;
	}

	/**
	 * @param agentName
	 *            The agentName to set.
	 */
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	/**
	 * @return Returns the fareRulesSummary.
	 */
	public Collection<FareRuleSummaryDTO> getFareRulesSummary() {
		return fareRulesSummary;
	}

	/**
	 * @param fareRulesSummary
	 *            The fareRulesSummary to set.
	 */
	public void setFareRulesSummary(Collection<FareRuleSummaryDTO> fareRulesSummary) {
		this.fareRulesSummary = fareRulesSummary;
	}

}
