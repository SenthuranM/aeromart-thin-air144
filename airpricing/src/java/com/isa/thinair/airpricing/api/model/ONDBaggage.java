package com.isa.thinair.airpricing.api.model;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @author kasun
 * 
 * @hibernate.class table = "BG_T_OND_BAGGAGE"
 */
public class ONDBaggage extends Persistent {

	private static final long serialVersionUID = 1L;

	private String ondCode;

	private String origin;

	private String destination;

	private String status;

	/**
	 * @hibernate.id column = "OND_BAGGAGE_OND_CODE" generator-class = "assigned"
	 */
	public String getOndCode() {
		return ondCode;
	}

	/**
	 * @param ondCode
	 *            the ondCode to set
	 */
	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}

	/**
	 * @return the origin
	 * @hibernate.property column = "ORIGIN"
	 */
	public String getOrigin() {
		return origin;
	}

	/**
	 * @param origin
	 *            the origin to set
	 */
	public void setOrigin(String origin) {
		this.origin = origin;
	}

	/**
	 * @return the destination
	 * @hibernate.property column = "DESTINATION"
	 */
	public String getDestination() {
		return destination;
	}

	/**
	 * @param destination
	 *            the destination to set
	 */
	public void setDestination(String destination) {
		this.destination = destination;
	}

	/**
	 * @return the status
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

}
