package com.isa.thinair.airpricing.api.model;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * 
 * @hibernate.class table = "ML_T_MEAL_TEMPL_RESTRICT_CAT" MealTemplate multi select categories
 * 
 */
public class MultiSelectCategoryRestriction extends Persistent {

	private static final long serialVersionUID = 5253015299702881762L;

	private Integer multiSelectRestrictionCatId;

	private MealTemplate template;

	private Integer mealCategoryId;

	/**
	 * @return Returns the multiSelectRestrictionCatId.
	 * @hibernate.id column = "MEAL_TEMPL_RESTRICT_CAT_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "ML_S_MEAL_TEMPL_RESTRICT_CAT"
	 */
	public Integer getMultiSelectRestrictionCatId() {
		return multiSelectRestrictionCatId;
	}

	public void setMultiSelectRestrictionCatId(Integer multiSelectRestrictionCatId) {
		this.multiSelectRestrictionCatId = multiSelectRestrictionCatId;
	}

	/**
	 * @return Returns the template.
	 * @hibernate.many-to-one column = "MEAL_TEMPLATE_ID" class= "com.isa.thinair.airpricing.api.model.MealTemplate"
	 */
	public MealTemplate getTemplateId() {
		return template;
	}

	/**
	 * @param template
	 *            the template to set
	 */
	public void setTemplateId(MealTemplate template) {
		this.template = template;
	}

	/**
	 * 
	 * @return
	 * @hibernate.property column = "MEAL_CATEGORY_ID"
	 */
	public Integer getMealCategoryId() {
		return mealCategoryId;
	}

	/**
	 * @param mealCategoryId
	 *            the mealCategoryId to set
	 */
	public void setMealCategoryId(Integer mealCategoryId) {
		this.mealCategoryId = mealCategoryId;
	}

	public int hashCode() {
		return new HashCodeBuilder().append(getMultiSelectRestrictionCatId()).toHashCode();
	}

}
