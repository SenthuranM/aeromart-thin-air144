package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class SimplifiedChargeDTO implements Serializable {
	
	private static final long serialVersionUID = -872802184195676821L;
	
	private BigDecimal amount;
	
	private String chargeCode;
	
	private String chargeGroupCode;
	
	private String carrierCode;
	
	private Integer ondSequence;
	
	private String segmentCode;
	
	private String logicalCCCode;
	
	private String flightRefNumber;
	
	private Integer segmentSequence;

	public BigDecimal getAmount() {
		return amount;
	}

	public String getChargeCode() {
		return chargeCode;
	}

	public String getChargeGroupCode() {
		return chargeGroupCode;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public String getLogicalCCCode() {
		return logicalCCCode;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

	public void setChargeGroupCode(String chargeGroupCode) {
		this.chargeGroupCode = chargeGroupCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public void setLogicalCCCode(String logicalCCCode) {
		this.logicalCCCode = logicalCCCode;
	}

	public String getFlightRefNumber() {
		return flightRefNumber;
	}

	public void setFlightRefNumber(String flightRefNumber) {
		this.flightRefNumber = flightRefNumber;
	}

	public Integer getOndSequence() {
		return ondSequence;
	}

	public Integer getSegmentSequence() {
		return segmentSequence;
	}

	public void setOndSequence(Integer ondSequence) {
		this.ondSequence = ondSequence;
	}

	public void setSegmentSequence(Integer segmentSequence) {
		this.segmentSequence = segmentSequence;
	}


}
