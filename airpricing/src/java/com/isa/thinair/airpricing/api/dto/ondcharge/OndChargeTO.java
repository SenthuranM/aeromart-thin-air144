/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:20:29
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airpricing.api.dto.ondcharge;

import java.io.Serializable;
import java.util.List;

public class OndChargeTO implements Serializable {

	private static final long serialVersionUID = 7468294168252255502L;

	public static int REFUNDABLE_CHARGE = 1;

	public static int NONE_REFUNDABLE_CHARGE = 0;

	private String chargeCode;

	private String chargeDescription;

	private String chargeCategoryValue;

	private int appliesTo;

	private int refundableChargeFlag;

	private String chargeCategoryCode;

	private String chargeGroupCode;

	private List<OndChargeRateTO> chargeRates;

	/**
	 * @return Returns the chargeCategoryCode.
	 */
	public String getChargeCategoryCode() {
		return chargeCategoryCode;
	}

	/**
	 * @param chargeCategoryCode
	 *            The chargeCategoryCode to set.
	 */
	public void setChargeCategoryCode(String chargeCategoryCode) {
		this.chargeCategoryCode = chargeCategoryCode;
	}

	/**
	 * @return Returns the chargeCategoryValue.
	 */
	public String getChargeCategoryValue() {
		return chargeCategoryValue;
	}

	/**
	 * @param chargeCategoryValue
	 *            The chargeCategoryValue to set.
	 */
	public void setChargeCategoryValue(String chargeCategoryValue) {
		this.chargeCategoryValue = chargeCategoryValue;
	}

	/**
	 * @return Returns the chargeCode.
	 */
	public String getChargeCode() {
		return chargeCode;
	}

	/**
	 * @param chargeCode
	 *            The chargeCode to set.
	 */
	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

	/**
	 * @return Returns the chargeDescription.
	 */
	public String getChargeDescription() {
		return chargeDescription;
	}

	/**
	 * @param chargeDescription
	 *            The chargeDescription to set.
	 */
	public void setChargeDescription(String chargeDescription) {
		this.chargeDescription = chargeDescription;
	}

	/**
	 * @return Returns the chargeGroupCode.
	 */
	public String getChargeGroupCode() {
		return chargeGroupCode;
	}

	/**
	 * @param chargeGroupCode
	 *            The chargeGroupCode to set.
	 */
	public void setChargeGroupCode(String chargeGroupCode) {
		this.chargeGroupCode = chargeGroupCode;
	}

	/**
	 * @return Returns the refundableChargeFlag.
	 */
	public int getRefundableChargeFlag() {
		return refundableChargeFlag;
	}

	/**
	 * @param refundableChargeFlag
	 *            The refundableChargeFlag to set.
	 */
	public void setRefundableChargeFlag(int refundableChargeFlag) {
		this.refundableChargeFlag = refundableChargeFlag;
	}

	/**
	 * @return Returns the chargeRates.
	 */
	public List<OndChargeRateTO> getChargeRates() {
		return chargeRates;
	}

	/**
	 * @param chargeRates
	 *            The chargeRates to set.
	 */
	public void setChargeRates(List<OndChargeRateTO> chargeRates) {
		this.chargeRates = chargeRates;
	}

	/**
	 * @return Returns the appliesTo.
	 */
	public int getAppliesTo() {
		return appliesTo;
	}

	/**
	 * @param appliesTo
	 *            The appliesTo to set.
	 */
	public void setAppliesTo(int appliesTo) {
		this.appliesTo = appliesTo;
	}
}
