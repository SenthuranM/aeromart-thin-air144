/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2012 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 * 
 * ===============================================================================
 */

package com.isa.thinair.airpricing.api.model;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @author Harshana
 * 
 * @since 1.0
 * @hibernate.class table = "T_RPT_CHARGE_GROUP"
 */
public class ReportChargeGroup extends Persistent {

	private static final long serialVersionUID = 583096494184650160L;

	private int reportChargeGroupID;

	private String reportChargeGroupCode;

	private String description;

	/**
	 * @return the reportChargeGroupID
	 * @hibernate.id column = "RPT_CHARGE_GROUP_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_RPT_CHARGE_GROUP"
	 */
	public int getReportChargeGroupID() {
		return reportChargeGroupID;
	}

	/**
	 * @param reportChargeGroupID
	 *            the reportChargeGroupID to set
	 */
	public void setReportChargeGroupID(int reportChargeGroupID) {
		this.reportChargeGroupID = reportChargeGroupID;
	}

	/**
	 * @return the reportChargeGroupCode
	 * @hibernate.property column = "RPT_CHARGE_GROUP_CODE"
	 */
	public String getReportChargeGroupCode() {
		return reportChargeGroupCode;
	}

	/**
	 * @param reportChargeGroupCode
	 *            the reportChargeGroupCode to set
	 */
	public void setReportChargeGroupCode(String reportChargeGroupCode) {
		this.reportChargeGroupCode = reportChargeGroupCode;
	}

	/**
	 * @return the description
	 * @hibernate.property column = "DESCRIPTION"
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

}
