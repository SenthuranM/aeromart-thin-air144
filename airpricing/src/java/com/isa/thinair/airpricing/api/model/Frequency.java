/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:17:16
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airpricing.api.model;

import java.io.Serializable;

/**
 * class to represent Frequency in the model
 * 
 * @author Byorn
 */
public abstract class Frequency implements Serializable {

	private static final long serialVersionUID = -2502716872372837222L;

	public abstract int getDay0();

	public abstract void setDay0(int day0);

	public abstract int getDay1();

	public abstract void setDay1(int day1);

	public abstract int getDay2();

	public abstract void setDay2(int day2);

	public abstract int getDay3();

	public abstract void setDay3(int day3);

	public abstract int getDay4();

	public abstract void setDay4(int day4);

	public abstract int getDay5();

	public abstract void setDay5(int day5);

	public abstract int getDay6();

	public abstract void setDay6(int day6);

}