package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.isa.thinair.airpricing.api.model.DefaultAnciTemplateRoute;

public class DefaultAnciTemplateTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private int defaultAnciTemplateId;

	private String ancillaryType;

	private int anciTemplateId;

	private String status;
	
	private String anciTemplateCode;

	private String airCraftModelnumber;

	private String ondString;
	
	private List<DefaultAnciTemplateRoute> assignedOnds;
	
	private String createdBy;

	private Date createdDate;

	private String modifiedBy;

	private Date modifiedDate;
	
	private long version = -1;
	
	private String templateName;

	/**
	 * @return the defaultAnciTemplateId
	 */
	public int getDefaultAnciTemplateId() {
		return defaultAnciTemplateId;
	}

	/**
	 * @param defaultAnciTemplateId the defaultAnciTemplateId to set
	 */
	public void setDefaultAnciTemplateId(int defaultAnciTemplateId) {
		this.defaultAnciTemplateId = defaultAnciTemplateId;
	}

	/**
	 * @return the ancillaryType
	 */
	public String getAncillaryType() {
		return ancillaryType;
	}

	/**
	 * @param ancillaryType the ancillaryType to set
	 */
	public void setAncillaryType(String ancillaryType) {
		this.ancillaryType = ancillaryType;
	}

	/**
	 * @return the anciTemplateId
	 */
	public int getAnciTemplateId() {
		return anciTemplateId;
	}

	/**
	 * @param anciTemplateId the anciTemplateId to set
	 */
	public void setAnciTemplateId(int anciTemplateId) {
		this.anciTemplateId = anciTemplateId;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the airCraftModelnumber
	 */
	public String getAirCraftModelnumber() {
		return airCraftModelnumber;
	}

	/**
	 * @param airCraftModelnumber the airCraftModelnumber to set
	 */
	public void setAirCraftModelnumber(String airCraftModelnumber) {
		this.airCraftModelnumber = airCraftModelnumber;
	}

	/**
	 * @return the ondString
	 */
	public String getOndString() {
		return ondString;
	}

	/**
	 * @param ondString the ondString to set
	 */
	public void setOndString(String ondString) {
		this.ondString = ondString;
	}
	
	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the modifiedDate
	 */
	public Date getModifiedDate() {
		return modifiedDate;
	}

	/**
	 * @param modifiedDate the modifiedDate to set
	 */
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	/**
	 * @return the version
	 */
	public long getVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(long version) {
		this.version = version;
	}

	/**
	 * @return the templateName
	 */
	public String getTemplateName() {
		return templateName;
	}

	/**
	 * @param templateName the templateName to set
	 */
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}
	
	/**
	 * @return the assignedOnds
	 */
	public List<DefaultAnciTemplateRoute> getAssignedOnds() {
		return assignedOnds;
	}

	/**
	 * @param assignedOnds the assignedOnds to set
	 */
	public void setAssignedOnds(List<DefaultAnciTemplateRoute> assignedOnds) {
		this.assignedOnds = assignedOnds;
	}
	
	public void addOND(DefaultAnciTemplateRoute ond){
		if(this.assignedOnds == null){
			this.assignedOnds = new ArrayList<>();
		}
		getAssignedOnds().add(ond);
	}

	/**
	 * @return the anciTemplateCode
	 */
	public String getAnciTemplateCode() {
		return anciTemplateCode;
	}

	/**
	 * @param anciTemplateCode the anciTemplateCode to set
	 */
	public void setAnciTemplateCode(String anciTemplateCode) {
		this.anciTemplateCode = anciTemplateCode;
	}

	
	
}
