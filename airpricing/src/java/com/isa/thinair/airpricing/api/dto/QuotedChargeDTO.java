package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * DTO for carrying quoted charge for an ond + pos + date combination
 * 
 * @author MN
 */
public class QuotedChargeDTO implements Serializable, Cloneable {

	private static final long serialVersionUID = -7462220818280974592L;

	private String chargeCode;

	private String chargeDescription;

	private String chargeGroupCode;

	private String chargeCategoryCode;

	private int applicableTo;

	private int chargeRateId;

	private boolean isChargeValueInPercentage;

	private double chargeValuePercentage;

	private double valueInLocalCurrency;

	private String localCurrencyCode;

	private BigDecimal chargeValueMin;

	private BigDecimal chargeValueMax;

	private String chargeBasis;

	private Map<String, Double> effectiveChargeAmountsMap;

	private Date timepsatmp;

	private String segApplicable;

	private String journeyType;

	private Date effectiveFromDate;

	private Date effectiveToDate;

	/** this charge rate specific break point used for rounding using {@link AccelAeroRounderPolicy} */
	private BigDecimal breakPoint;

	/** this charge rate specific boundary value used for rounding using {@link AccelAeroRounderPolicy} */
	private BigDecimal boundryValue;

	// since charge rates can defined journey wise, this is filled from charge rate .
	private int chargeRateJourneyType;

	private String reportingChargeGroupCode;

	private int minFlightSearchGap;

	private int maxFlightSearchGap;

	private Collection<Integer> discountApplicablePAXSequence;

	/** Holds the calculated discount amount PAX Sequence wise */
	private Map<Integer, Double> paxWiseDiscountAmountsMap;
	
	private Map<Integer, Double> paxWiseBalanceDueAdj;
	
	private boolean isRefundable;

	private boolean bundledFareCharge;

	private String cabinClassCode;

	public int getMaxFlightSearchGap() {
		return maxFlightSearchGap;
	}

	public void setMaxFlightSearchGap(int maxFlightSearchGap) {
		this.maxFlightSearchGap = maxFlightSearchGap;
	}

	public int getMinFlightSearchGap() {
		return minFlightSearchGap;
	}

	public void setMinFlightSearchGap(int minFlightSearchGap) {
		this.minFlightSearchGap = minFlightSearchGap;
	}

	private QuotedChargeDTO(Date timepsatmp) {
		this.timepsatmp = new Date(timepsatmp.getTime());
	}

	public QuotedChargeDTO() {
		this.timepsatmp = new Date();
	}

	/*
	 * Set inside SelectedFlightDTO true if a particular charge is applicable for both outbound and inbound journey
	 */
	private boolean appliesToOutAndInJourney = false;

	public static interface PaxTypeCharge {
		public static final String RESERVATION_CHARGE = "RC";
	}

	public String getChargeCategoryCode() {
		return chargeCategoryCode;
	}

	public void setChargeCategoryCode(String chargeCategoryCode) {
		this.chargeCategoryCode = chargeCategoryCode;
	}

	public String getChargeDescription() {
		return chargeDescription;
	}

	public void setChargeDescription(String chargeDescription) {
		this.chargeDescription = chargeDescription;
	}

	public String getChargeCode() {
		return chargeCode;
	}

	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

	public String getChargeGroupCode() {
		return chargeGroupCode;
	}

	public void setChargeGroupCode(String chargeGroupCode) {
		this.chargeGroupCode = chargeGroupCode;
	}

	public int getChargeRateId() {
		return chargeRateId;
	}

	public void setChargeRateId(int chargeRateId) {
		this.chargeRateId = chargeRateId;
	}

	public double getChargeValuePercentage() {
		return chargeValuePercentage;
	}

	public void setChargeValuePercentage(double chargeValuePercentage) {
		this.chargeValuePercentage = chargeValuePercentage;
	}

	public BigDecimal getChargeValueMin() {
		return chargeValueMin;
	}

	public void setChargeValueMin(BigDecimal chargeValueMin) {
		this.chargeValueMin = chargeValueMin;
	}

	public BigDecimal getChargeValueMax() {
		return chargeValueMax;
	}

	public void setChargeValueMax(BigDecimal chargeValueMax) {
		this.chargeValueMax = chargeValueMax;
	}

	public String getChargeBasis() {
		return chargeBasis;
	}

	public boolean isRefundable() {
		return isRefundable;
	}

	public void setRefundable(boolean isRefundable) {
		this.isRefundable = isRefundable;
	}

	public void setChargeBasis(String chargeBasis) {
		this.chargeBasis = chargeBasis;
	}

	public boolean isChargeValueInPercentage() {
		return isChargeValueInPercentage;
	}

	public void setChargeValueInPercentage(boolean isChargeValueInPercentage) {
		this.isChargeValueInPercentage = isChargeValueInPercentage;
	}

	public void setEffectiveChargeAmount(String paxType, Double effectiveChargAmount) {
		if (this.effectiveChargeAmountsMap == null) {
			this.effectiveChargeAmountsMap = new HashMap<String, Double>();
		}
		if (effectiveChargAmount != null) {
			if (AppSysParamsUtil.isRoundupChargesWhenCalculatedInBaseCurrency()) {
				effectiveChargAmount = new BigDecimal(effectiveChargAmount).setScale(0, RoundingMode.UP).doubleValue();
			} else {
				effectiveChargAmount = Double.parseDouble(AccelAeroCalculator.parseBigDecimal(effectiveChargAmount.doubleValue())
						.toString());
			}
		}
		this.effectiveChargeAmountsMap.put(paxType, effectiveChargAmount);
	}

	public Double getEffectiveChargeAmount(String paxType) {
		if (this.effectiveChargeAmountsMap == null || !this.effectiveChargeAmountsMap.containsKey(paxType)) {
			throw new ModuleRuntimeException("module.runtime.error");
		}
		return (Double) this.effectiveChargeAmountsMap.get(paxType);
	}

	public Map<String, Double> getEffectiveChargeAmountMap() {
		return this.effectiveChargeAmountsMap;
	}

	public double getValueInLocalCurrency() {
		return valueInLocalCurrency;
	}

	public void setValueInLocalCurrency(double valueInLocalCurrency) {
		this.valueInLocalCurrency = valueInLocalCurrency;
	}

	public boolean isAppliesToOutAndInJourney() {
		return appliesToOutAndInJourney;
	}

	public void setAppliesToOutAndInJourney(boolean appliesToOutAndInJourney) {
		this.appliesToOutAndInJourney = appliesToOutAndInJourney;
	}

	public Date getTimepsatmp() {
		return timepsatmp;
	}

	public void setTimepsatmp(Date timepsatmp) {
		this.timepsatmp = timepsatmp;
	}

	public String getSegApplicable() {
		return segApplicable;
	}

	public void setSegApplicable(String segApplicable) {
		this.segApplicable = segApplicable;
	}

	public String getJourneyType() {
		return journeyType;
	}

	public void setJourneyType(String journeyType) {
		this.journeyType = journeyType;
	}

	public Date getEffectiveFromDate() {
		return effectiveFromDate;
	}

	public void setEffectiveFromDate(Date effectiveFromDate) {
		this.effectiveFromDate = effectiveFromDate;
	}

	public Date getEffectiveToDate() {
		return effectiveToDate;
	}

	public void setEffectiveToDate(Date effectiveToDate) {
		this.effectiveToDate = effectiveToDate;
	}

	public int getChargeRateJourneyType() {
		return chargeRateJourneyType;
	}

	public void setChargeRateJourneyType(int chargeRateJourneyType) {
		this.chargeRateJourneyType = chargeRateJourneyType;
	}

	/**
	 * @return : true if both boundary value and break point are both available in for the charge rate record.
	 */
	public boolean isChargeRateRoundingApplicable() {
		if (this.getBoundryValue() != null && this.getBoundryValue().compareTo(BigDecimal.ZERO) > 0
				&& this.getBreakPoint() != null && this.getBreakPoint().compareTo(BigDecimal.ZERO) > 0) {
			return true;
		}
		return false;
	}

	/**
	 * clone.
	 */
	@Override
	public QuotedChargeDTO clone() {
		QuotedChargeDTO clone = new QuotedChargeDTO(this.getTimepsatmp());
		clone.setApplicableTo(this.getApplicableTo());
		clone.setChargeCategoryCode(this.getChargeCategoryCode());
		clone.setChargeCode(this.getChargeCode());
		clone.setChargeDescription(this.getChargeDescription());
		clone.setChargeGroupCode(this.getChargeGroupCode());
		clone.setChargeRateId(this.getChargeRateId());
		clone.setChargeValueInPercentage(this.isChargeValueInPercentage());
		clone.setChargeValuePercentage(this.getChargeValuePercentage());
		clone.setAppliesToOutAndInJourney(this.isAppliesToOutAndInJourney());
		clone.setChargeValueMin(this.getChargeValueMin());
		clone.setChargeValueMax(this.getChargeValueMax());
		clone.setChargeBasis(this.getChargeBasis());
		clone.setLocalCurrencyCode(this.getLocalCurrencyCode());
		clone.setValueInLocalCurrency(this.getValueInLocalCurrency());
		clone.setReportingChargeGroupCode(this.getReportingChargeGroupCode());
		// Conditioned cloning since some flows doesn't set Pax Wise Charge Amounts.
		if (this.isPaxWiseEffectiveChgAmountsExist()) {
			clone.setEffectiveChargeAmount(PaxTypeTO.ADULT, this.getEffectiveChargeAmount(PaxTypeTO.ADULT));
			clone.setEffectiveChargeAmount(PaxTypeTO.CHILD, this.getEffectiveChargeAmount(PaxTypeTO.CHILD));
			clone.setEffectiveChargeAmount(PaxTypeTO.INFANT, this.getEffectiveChargeAmount(PaxTypeTO.INFANT));
		}

		clone.setSegApplicable(this.segApplicable);
		clone.setRefundable(this.isRefundable);
		clone.setJourneyType(this.journeyType);
		clone.setEffectiveFromDate(this.effectiveFromDate);
		clone.setEffectiveToDate(this.effectiveToDate);
		clone.setChargeRateJourneyType(this.chargeRateJourneyType);
		clone.setBoundryValue(this.getBoundryValue());
		clone.setBreakPoint(this.getBreakPoint());
		clone.setMinFlightSearchGap(this.minFlightSearchGap);
		clone.setMaxFlightSearchGap(this.maxFlightSearchGap);
		clone.setBundledFareCharge(this.bundledFareCharge);
		clone.setCabinClassCode(this.getCabinClassCode());
		return clone;
	}

	/**
	 * Is Passenger wise effective charge amounts exist
	 * 
	 * @return
	 */
	private boolean isPaxWiseEffectiveChgAmountsExist() {
		if (this.effectiveChargeAmountsMap != null && this.effectiveChargeAmountsMap.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @return Returns the applicableTo.
	 */
	public int getApplicableTo() {
		return applicableTo;
	}

	/**
	 * @param applicableTo
	 *            The applicableTo to set.
	 */
	public void setApplicableTo(int applicableTo) {
		this.applicableTo = applicableTo;
	}

	/**
	 * @return : The break point.
	 */
	public BigDecimal getBreakPoint() {
		return breakPoint;
	}

	/**
	 * @param breakPoint
	 *            : The break point to set.
	 */
	public void setBreakPoint(BigDecimal breakPoint) {
		this.breakPoint = breakPoint;
	}

	/**
	 * @return : The boundary value.
	 */
	public BigDecimal getBoundryValue() {
		return boundryValue;
	}

	/**
	 * @param boundryValue
	 *            : The boundary value to set.
	 */
	public void setBoundryValue(BigDecimal boundryValue) {
		this.boundryValue = boundryValue;
	}

	public String getLocalCurrencyCode() {
		return localCurrencyCode;
	}

	public void setLocalCurrencyCode(String localCurrencyCode) {
		this.localCurrencyCode = localCurrencyCode;
	}

	/**
	 * @return the reportingChargeGroupCode
	 */
	public String getReportingChargeGroupCode() {
		return reportingChargeGroupCode;
	}

	/**
	 * @param reportingChargeGroupCode
	 *            the reportingChargeGroupCode to set
	 */
	public void setReportingChargeGroupCode(String reportingChargeGroupCode) {
		this.reportingChargeGroupCode = reportingChargeGroupCode;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("QuotedChargeDTO [ ");
		sb.append(" chargeRateId = " + chargeRateId);
		sb.append(" applicableTo = " + applicableTo);
		sb.append(" chargeBasis	 = " + chargeBasis);
		sb.append(" chargeCategoryCode =" + chargeCategoryCode);
		sb.append(" chargeCode = " + chargeCode);
		sb.append(" chargeGroupCode	= " + chargeGroupCode);
		sb.append(" chargeValuePercentage	= " + chargeValuePercentage);
		sb.append(" effectiveChargeAmountsMap = " + effectiveChargeAmountsMap);
		sb.append(" isChargeValueInPercentage = " + isChargeValueInPercentage);
		sb.append(" breakPoint = " + breakPoint);
		sb.append(" boundryValue = " + boundryValue);
		sb.append(" bundledFareCharge = " + bundledFareCharge);
		sb.append("]");
		return sb.toString();
	}

	public Collection<Integer> getDiscountApplicablePAXSequence() {
		return discountApplicablePAXSequence;
	}

	public void setDiscountApplicablePAXSequence(Collection<Integer> discountApplicablePAXSequence) {
		this.discountApplicablePAXSequence = discountApplicablePAXSequence;
	}

	public void addDiscountApplicablePAXSequence(Integer paxSequence) {
		if (this.discountApplicablePAXSequence == null)
			this.discountApplicablePAXSequence = new ArrayList<Integer>();

		this.discountApplicablePAXSequence.add(paxSequence);

	}

	public boolean isDiscountApplicable(Integer paxSequence) {
		if (discountApplicablePAXSequence != null && !discountApplicablePAXSequence.isEmpty() && paxSequence != null) {
			if (discountApplicablePAXSequence.contains(paxSequence)) {
				return true;
			}
		}
		return false;
	}

	public void setPaxWiseDiscountAmount(Integer paxSequence, Double effectiveChargAmount) {
		if (this.paxWiseDiscountAmountsMap == null) {
			this.paxWiseDiscountAmountsMap = new HashMap<Integer, Double>();
		}
		if (effectiveChargAmount != null) {
			effectiveChargAmount = Double.parseDouble(AccelAeroCalculator.parseBigDecimal(effectiveChargAmount.doubleValue())
					.toString());
		}
		this.paxWiseDiscountAmountsMap.put(paxSequence, effectiveChargAmount);
	}

	public Double getPaxWiseDiscountAmount(Integer paxSequence) {
		if (this.paxWiseDiscountAmountsMap == null || !this.paxWiseDiscountAmountsMap.containsKey(paxSequence)) {
			return new Double(0);
		}
		return (Double) this.paxWiseDiscountAmountsMap.get(paxSequence);
	}

	public Map<Integer, Double> getPaxWiseDiscountAmountsMap() {
		return this.paxWiseDiscountAmountsMap;
	}	

	public Map<Integer, Double> getPaxWiseBalanceDueAdj() {
		return paxWiseBalanceDueAdj;
	}

	public void setPaxWiseBalanceDueAdj(Integer pnrPaxId, Double balanceDueAdjAmount) {
		if (this.paxWiseBalanceDueAdj == null) {
			this.paxWiseBalanceDueAdj = new HashMap<Integer, Double>();
		}
		if (balanceDueAdjAmount != null) {
			balanceDueAdjAmount = Double.parseDouble(AccelAeroCalculator.parseBigDecimal(balanceDueAdjAmount.doubleValue())
					.toString());
		}
		this.paxWiseBalanceDueAdj.put(pnrPaxId, balanceDueAdjAmount);
	}

	public Double getPaxWiseBalanceDueAdjAmount(Integer pnrPaxId) {
		if (this.paxWiseBalanceDueAdj == null || !this.paxWiseBalanceDueAdj.containsKey(pnrPaxId)) {
			return new Double(0);
		}
		return (Double) this.paxWiseBalanceDueAdj.get(pnrPaxId);
	}

	public boolean isBundledFareCharge() {
		return bundledFareCharge;
	}

	public void setBundledFareCharge(boolean bundledFareCharge) {
		this.bundledFareCharge = bundledFareCharge;
	}

	public String getCabinClassCode() {
		return cabinClassCode;
	}

	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}
}
