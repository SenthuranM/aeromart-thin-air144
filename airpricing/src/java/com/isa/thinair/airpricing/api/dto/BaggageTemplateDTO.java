package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;
import java.util.Date;

public class BaggageTemplateDTO implements Serializable {

	/**
	 * @author subash
	 */
	private static final long serialVersionUID = 1L;
	private String description;
	private String templateCode;
	private Integer templateId;
	private long version;
	private String createdBy;
	private Date createdDate;
	private String status;
	private String baggageCharges;
	private String localCurrCode;
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getTemplateCode() {
		return templateCode;
	}
	public void setTemplateCode(String templateCode) {
		this.templateCode = templateCode;
	}
	public Integer getTemplateId() {
		return templateId;
	}
	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}
	public long getVersion() {
		return version;
	}
	public void setVersion(long version) {
		this.version = version;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getBaggageCharges() {
		return baggageCharges;
	}
	public void setBaggageCharges(String baggageCharges) {
		this.baggageCharges = baggageCharges;
	}
	public String getLocalCurrCode() {
		return localCurrCode;
	}
	public void setLocalCurrCode(String localCurrCode) {
		this.localCurrCode = localCurrCode;
	}
		

	

}
