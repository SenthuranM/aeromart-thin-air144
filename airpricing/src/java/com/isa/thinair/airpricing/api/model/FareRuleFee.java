/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:16:12
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airpricing.api.model;

import java.math.BigDecimal;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @hibernate.class table = "T_FARE_RULE_FEE"
 */
public class FareRuleFee extends Persistent {

	private static final long serialVersionUID = 6141852604444719470L;

	private Long fareRuleFeeId;

	private String chargeType;

	private String chargeAmountType;

	private BigDecimal chargeAmount;

	private BigDecimal minimumPerChargeAmt;

	private BigDecimal maximumPerChargeAmt;

	private String compareAgainst;

	private String timeType;

	private String timeValue;

	private String status;

	private FareRule fareRule;

	private BigDecimal chargeAmountInLocal = BigDecimal.ZERO;

	public static interface CHARGE_AMOUNT_TYPES {
		public static final String CHARGE_TYPE_V = "V";
		public static final String CHARGE_TYPE_PF = "PF";
		public static final String CHARGE_TYPE_PFS = "PFS";
	}

	public static interface CHARGE_TYPES {
		public static final String MODIFICATION = "MOD";
		public static final String CANCELLATION = "CNX";
		public static final String NAME_CHANGE = "NCC";
	}

	public static interface COMPARE_AGAINTS {
		public static final String BEFORE_DEPARTURE = "BEFDEP";
		public static final String AFTER_DEPARTURE = "AFTDEP";
	}

	public static interface TIME_TYPE {
		public static final String MINUTES = "MIN";
		public static final String HOURS = "HRS";
		public static final String DAYS = "DAY";
		public static final String WEEK = "WEK";
		public static final String MONTH = "MON";
	}

	public static interface STATES {
		public static final String ACTIVE = "ACT";
		public static final String INACTIVE = "INA";
	}

	public FareRuleFee() {

	}

	/**
	 * @return the fareRuleFeeId
	 * @hibernate.id column = "FARE_RULE_FEE_ID" generator-class = "native"
	 * @hibernate.generator-param name="sequence" value="S_FARE_RULE_FEE"
	 */
	public Long getFareRuleFeeId() {
		return fareRuleFeeId;
	}

	/**
	 * @param fareRuleFeeId
	 *            the fareRuleFeeId to set
	 */
	public void setFareRuleFeeId(Long fareRuleFeeId) {
		this.fareRuleFeeId = fareRuleFeeId;
	}

	/**
	 * @return the chargeType
	 * @hibernate.property column = "CHARGE_TYPE"
	 */
	public String getChargeType() {
		return chargeType;
	}

	/**
	 * @param chargeType
	 *            the chargeType to set
	 */
	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}

	/**
	 * @return the chargeAmountType
	 * @hibernate.property column = "CHARGE_AMOUNT_TYPE"
	 */
	public String getChargeAmountType() {
		return chargeAmountType;
	}

	/**
	 * @param chargeAmountType
	 *            the chargeAmountType to set
	 */
	public void setChargeAmountType(String chargeAmountType) {
		this.chargeAmountType = chargeAmountType;
	}

	/**
	 * @return the chargeAmount
	 * @hibernate.property column = "CHARGE_AMOUNT"
	 */
	public BigDecimal getChargeAmount() {
		return chargeAmount;
	}

	/**
	 * @param chargeAmount
	 *            the chargeAmount to set
	 */
	public void setChargeAmount(BigDecimal chargeAmount) {
		this.chargeAmount = chargeAmount;
	}

	/**
	 * @return the minimumPerChargeAmt
	 * @hibernate.property column = "MINIMUM_PERT_CHARGE_AMOUNT"
	 */
	public BigDecimal getMinimumPerChargeAmt() {
		return minimumPerChargeAmt;
	}

	/**
	 * @param minimumPerChargeAmt
	 *            the minimumPerChargeAmt to set
	 */
	public void setMinimumPerChargeAmt(BigDecimal minimumPerChargeAmt) {
		this.minimumPerChargeAmt = minimumPerChargeAmt;
	}

	/**
	 * @return the maximumPerChargeAmt
	 * @hibernate.property column = "MAXIMUM_PERT_CHARGE_AMOUNT"
	 */
	public BigDecimal getMaximumPerChargeAmt() {
		return maximumPerChargeAmt;
	}

	/**
	 * @param maximumPerChargeAmt
	 *            the maximumPerChargeAmt to set
	 */
	public void setMaximumPerChargeAmt(BigDecimal maximumPerChargeAmt) {
		this.maximumPerChargeAmt = maximumPerChargeAmt;
	}

	/**
	 * @return the compareAgainst
	 * @hibernate.property column = "COMPARE_AGAINST"
	 */
	public String getCompareAgainst() {
		return compareAgainst;
	}

	/**
	 * @param compareAgainst
	 *            the compareAgainst to set
	 */
	public void setCompareAgainst(String compareAgainst) {
		this.compareAgainst = compareAgainst;
	}

	/**
	 * @return the timeType
	 * @hibernate.property column = "TIME_TYPE"
	 */
	public String getTimeType() {
		return timeType;
	}

	/**
	 * @param timeType
	 *            the timeType to set
	 */
	public void setTimeType(String timeType) {
		this.timeType = timeType;
	}

	/**
	 * @return the timeValue
	 * @hibernate.property column = "TIME_VALUE"
	 */
	public String getTimeValue() {
		return timeValue;
	}

	/**
	 * @param timeValue
	 *            the timeValue to set
	 */
	public void setTimeValue(String timeValue) {
		this.timeValue = timeValue;
	}

	/**
	 * @return the status
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the fareRule
	 * @hibernate.many-to-one column="FARE_RULE_ID" class="com.isa.thinair.airpricing.api.model.FareRule"
	 */
	public FareRule getFareRule() {
		return fareRule;
	}

	/**
	 * @param fareRule
	 *            the fareRule to set
	 */
	public void setFareRule(FareRule fareRule) {
		this.fareRule = fareRule;
	}

	/**
	 * Overides the equals
	 * 
	 * @param o
	 * @return
	 */
	public boolean equals(Object o) {
		// If it's a same class instance
		if (o instanceof FareRuleFee) {
			FareRuleFee castObject = (FareRuleFee) o;
			if (castObject.getFareRuleFeeId() == null || this.getFareRuleFeeId() == null) {
				return false;
			} else if (castObject.getFareRuleFeeId().intValue() == this.getFareRuleFeeId().intValue()) {
				return true;
			} else {
				return false;
			}
		}
		// If it's not
		else {
			return false;
		}
	}

	/**
	 * Overrides hashcode to support lazy loading
	 */
	public int hashCode() {
		return new HashCodeBuilder().append(getFareRuleFeeId()).toHashCode();
	}

	/**
	 * @return the chargeAmountInLocal
	 * @hibernate.property column = "CHARGE_AMOUNT_IN_LOCAL"
	 */
	public BigDecimal getChargeAmountInLocal() {
		return chargeAmountInLocal;
	}

	public void setChargeAmountInLocal(BigDecimal chargeAmountInLocal) {
		this.chargeAmountInLocal = chargeAmountInLocal;
	}

}
