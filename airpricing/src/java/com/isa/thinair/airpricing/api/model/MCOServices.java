package com.isa.thinair.airpricing.api.model;

import com.isa.thinair.commons.core.framework.Persistent;

/** 
 * @author manoji
 * @hibernate.class table = "T_MCO_SERVICE"
 */
public class MCOServices extends Persistent{
	
	private Integer mcoServiceId;
	private String serviceName;
	private String serviceDescription;
	private String status;
	
	public static final String STATUS_ACTIVE = "ACT";
	public static final String STATUS_INACTIVE = "INA";	
	/**
	 * @return
	 * @hibernate.id column = "MCO_SERVICE_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_MCO_SERVICE"
	 */
	public Integer getMcoServiceId() {
		return mcoServiceId;
	}
	public void setMcoServiceId(Integer mcoServiceId) {
		this.mcoServiceId = mcoServiceId;
	}
	
	/**
	 * @return
	 * @hibernate.property column = "SERVICE_NAME"
	 */
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	
	/**
	 * @return
	 * @hibernate.property column = "MCO_SERVICE_DESCRIPTION"
	 */
	public String getServiceDescription() {
		return serviceDescription;
	}
	public void setServiceDescription(String serviceDescription) {
		this.serviceDescription = serviceDescription;
	}
	
	/**
	 * @return
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
