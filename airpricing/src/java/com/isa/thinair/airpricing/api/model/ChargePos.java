/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:12:33
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airpricing.api.model;

import java.io.Serializable;

/**
 * @author Byorn
 * 
 * @hibernate.class table = "T_CHARGE_POS"
 */
public class ChargePos implements Serializable {

	private static final long serialVersionUID = -8496147861664045814L;

	private int chargePosId;

	private String pos;

	private Charge charge;

	private String includeOrExclude;

	/**
	 * @hibernate.id column = "cp_id" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_CHARGE_POS"
	 * @return
	 */
	public int getChargePosId() {
		return chargePosId;
	}

	public void setChargePosId(int chargePosId) {
		this.chargePosId = chargePosId;
	}

	/**
	 * @hibernate.property column = "pos_code"
	 * @return
	 */
	public String getPos() {
		return pos;
	}

	public void setPos(String pos) {
		this.pos = pos;
	}

	/**
	 * @hibernate.property column = "apply_status"
	 * @return
	 */
	public String getIncludeOrExclude() {
		return includeOrExclude;
	}

	public void setIncludeOrExclude(String includeOrExclude) {
		this.includeOrExclude = includeOrExclude;
	}

	/**
	 * @return Returns the charge.
	 * @hibernate.many-to-one column="CHARGE_CODE" class="com.isa.thinair.airpricing.api.model.Charge"
	 */
	public Charge getCharge() {
		return charge;
	}

	/**
	 * @param charge
	 *            The charge to set.
	 */
	public void setCharge(Charge charge) {
		this.charge = charge;
	}
}
