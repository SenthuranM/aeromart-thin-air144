/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:20:29
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airpricing.api.model;

import java.util.Set;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * 
 * @author : Code Generation Tool
 * @version : Ver 1.0.0
 * 
 * @hibernate.class table = "T_OND"
 * 
 *                  OriginDestination is the entity class to repesent a OriginDestination model
 * 
 */
public class OriginDestination extends Persistent {

	private static final long serialVersionUID = -7825529219793401578L;

	private String ondCode;

	private String originAirportCode;

	private String destinationAirportCode;

	private Set<Charge> charges;

	/**
	 * returns the ondId
	 * 
	 * @return Returns the ondId.
	 * 
	 * @hibernate.id column = "OND_CODE" generator-class = "assigned"
	 */
	public String getONDCode() {
		return ondCode;
	}

	public void setONDCode(String ondCode) {
		this.ondCode = ondCode;
	}

	/**
	 * returns the description
	 * 
	 * @return Returns the description.
	 * 
	 * @hibernate.property column = "DESTINATION"
	 */
	public String getDestinationAirportCode() {
		return destinationAirportCode;
	}

	public void setDestinationAirportCode(String destinationAirportCode) {
		this.destinationAirportCode = destinationAirportCode;
	}

	/**
	 * returns the description
	 * 
	 * @return Returns the description.
	 * 
	 * @hibernate.property column = "ORIGIN"
	 */

	public String getOriginAirportCode() {
		return originAirportCode;
	}

	public void setOriginAirportCode(String originAirportCode) {
		this.originAirportCode = originAirportCode;
	}

	/**
	 * @hibernate.set role="permissions" table="T_OND_CHARGE" cascade="all" readonly="true"
	 * @hibernate.collection-key column="OND_CODE"
	 * @hibernate.collection-many-to-many class="com.isa.thinair.airpricing.api.model.Charge" column="CHARGE_CODE"
	 * @return Returns all the Charges for this group as a java.util.Set
	 */
	public Set<Charge> getCharges() {
		return charges;
	}

	/**
	 * @param charges
	 *            The charges to set.
	 */
	public void setCharges(Set<Charge> charges) {
		this.charges = charges;
	}

}
