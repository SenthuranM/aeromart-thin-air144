/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:12:33
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airpricing.api.model;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.isa.thinair.commons.api.dto.ChargeApplicabilityTO;
import com.isa.thinair.commons.core.framework.Tracking;

/**
 * 
 * @author : Code Generation Tool
 * @version : Ver 1.0.0
 * 
 * @hibernate.class table = "T_CHARGE"
 * 
 *                  Charge is the entity class to represent the Charge model
 * 
 */
public class Charge extends Tracking {

	private static final long serialVersionUID = -6247517697402538311L;
	/** Valid statuses */
	public static String STATUS_ACTIVE = "ACT";
	public static String STATUS_INACTIVE = "INA";

	public static String EXCLUABLE = "Y";
	public static String NOT_EXCLUABLE = "N";

	/** Passenger Applicability */
	public static final int APPLICABLE_TO_ALL = ChargeApplicabilityTO.APPLICABLE_TO_ALL;
	// Applicable for any passenger type
	public static final int APPLICABLE_TO_ADULT_ONLY = ChargeApplicabilityTO.APPLICABLE_TO_ADULT_ONLY;
	// Applicable for adult passenger type only
	public static final int APPLICABLE_TO_INFANT_ONLY = ChargeApplicabilityTO.APPLICABLE_TO_INFANT_ONLY;
	// Applicable for infant passenger type only
	public static final int APPLICABLE_TO_CHILD_ONLY = ChargeApplicabilityTO.APPLICABLE_TO_CHILD_ONLY;
	// Applicable for child passenger type only
	public static final int APPLICABLE_TO_ADULT_INFANT = ChargeApplicabilityTO.APPLICABLE_TO_ADULT_INFANT;
	// Applicable for both adult and infant passenger type only
	public static final int APPLICABLE_TO_ADULT_CHILD = ChargeApplicabilityTO.APPLICABLE_TO_ADULT_CHILD;
	// Applicable for both adult and child passenger type only
	public static final int APPLICABLE_TO_RESERVATION = ChargeApplicabilityTO.APPLICABLE_TO_RESERVATION;
	// Applicable for the reservation
	public static final int APPLICABLE_TO_OND = ChargeApplicabilityTO.APPLICABLE_TO_OND;
	// Applicable for the OND

	/** OND applicability */
	public static final String APPLY_TO_ALL_ONDs = "A";
	// Applicable for any segment of the journey being charge quoted
	public static final String APPLY_TO_FIRST_OND_ONLY = "F";
	// Applicable for the first segment of the journey being charge quoted
	public static final String APPLY_TO_LAST_OND_ONLY = "L";
	// Applicable for the last segment of the journey being charge quoted

	/** Journey type */
	public static final String JOURNEY_TYPE_ALL = "A";
	// Applicable for any OND
	public static final String JOURNEY_TYPE_EXCLUDE_SHORT_TRANSIT = "E";
	// Apply for all ONDs, excluding transit ONDs with transit gap < min transit duration
	// public static final String JOURNEY_TYPE_SHORT_TRANSIT_ONLY = "T"; //Apply only for ONDs with transit gap < min
	// transit duration

	/** Airport tax Category **/
	public static final String AIRPORT_TAX_CAT_DEP = "DEP";
	public static final String AIRPORT_TAX_CAT_ARR = "ARR";
	public static final String AIRPORT_TAX_CAT_TRANSIT = "TRANSIT";

	public static final String CHARGE_CATEGORY_GLOBAL = "GLOBAL";
	public static final String CHARGE_CATEGORY_POS = "POS";
	public static final String CHARGE_CATEGORY_OND = "OND";
	public static final String CHARGE_CATEGORY_COOKIE_BASED = "COOKIE_BASED";
	public static final String CHARGE_CATEGORY_SERVICE_TAX = "SERVICE_TAX";

	public static int REFUNDABLE_CHARGE = 1;

	public static int NONE_REFUNDABLE_CHARGE = 0;

	private String chargeCode;

	private String chargeDescription;

	private String chargeCategoryValue;

	private int appliesTo;

	private int refundableChargeFlag;

	private String refundableOnlyforModifications;

	private String status;

	private String chargeCategoryCode;

	private String chargeGroupCode;

	private Set<ChargeRate> chargeRates;

	private Set<String> ondCodes;

	private char departureOrArival;

	private Integer reportingChargeGroupId;

	private String ondApplicability;

	private String journeyType;

	private String chargeAgentFilter;

	private String surRevenue;

	private String surRevenuePerc;

	private Collection<ChargePos> posCharges;

	private Collection<ChargeAgent> agentCharges;

	private char hasInwardTransit;

	private char hasOutwardTransit;

	private Long minTransitTime;

	private Long maxTransitTime;

	private char hasTransit;

	private String airportTaxCategory;

	private int channelId;

	private String excludable;

	private Integer minFlightSearchGap;

	private Integer maxFlightSearchGap;

	private String refundableWhenExchange;

	private String bundledFareCharge;

	private String countryCode;

	private String interState;

	private Integer stateId;

	private String chargeCodeFilter;

	private Collection<ChargeApplicableCharges> applicableCharges;

	private String invoiceTaxCategory;
	
	private String ticketingServiceTax;

	/**
	 * returns the chargeCode
	 * 
	 * @return Returns the chargeCode.
	 * @hibernate.id column = "CHARGE_CODE" generator-class = "assigned"
	 */
	public String getChargeCode() {
		return chargeCode;
	}

	/**
	 * sets the chargeCode
	 * 
	 * @param chargeCode
	 *            The chargeCode to set.
	 */
	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

	/**
	 * returns the chargeDescription
	 * 
	 * @return Returns the chargeDescription.
	 * 
	 * @hibernate.property column = "CHARGE_DESCRIPTION"
	 */
	public String getChargeDescription() {
		return chargeDescription;
	}

	/**
	 * sets the chargeDescription
	 * 
	 * @param chargeDescription
	 *            The chargeDescription to set.
	 */
	public void setChargeDescription(String chargeDescription) {
		this.chargeDescription = chargeDescription;
	}

	/**
	 * returns the chargeCategoryValue
	 * 
	 * @return Returns the chargeCategoryValue.
	 * 
	 * @hibernate.property column = "CHARGE_CATEGORY_VALUE"
	 */
	public String getChargeCategoryValue() {
		return chargeCategoryValue;
	}

	/**
	 * sets the chargeCategoryValue
	 * 
	 * @param chargeCategoryValue
	 *            The chargeCategoryValue to set.
	 */
	public void setChargeCategoryValue(String chargeCategoryValue) {
		this.chargeCategoryValue = chargeCategoryValue;
	}

	/**
	 * returns the refundableChargeFlag
	 * 
	 * @return Returns the refundableChargeFlag.
	 * 
	 * @hibernate.property column = "REFUNDABLE_CHARGE_FLAG"
	 */
	public int getRefundableChargeFlag() {
		return refundableChargeFlag;
	}

	/**
	 * sets the refundableChargeFlag
	 * 
	 * @param refundableChargeFlag
	 *            The refundableChargeFlag to set.
	 */
	public void setRefundableChargeFlag(int refundableChargeFlag) {
		this.refundableChargeFlag = refundableChargeFlag;
	}

	/**
	 * returns the status
	 * 
	 * @return Returns the status.
	 * 
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * sets the status
	 * 
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * returns the chargeCategoryCode
	 * 
	 * @hibernate.property column = "CHARGE_CATEGORY_CODE"
	 */
	public String getChargeCategoryCode() {
		return chargeCategoryCode;
	}

	public void setChargeCategoryCode(String chargeCategoryCode) {
		this.chargeCategoryCode = chargeCategoryCode;
	}

	/**
	 * returns the chargeGroupCde
	 * 
	 * 
	 * 
	 * @hibernate.property column = "CHARGE_GROUP_CODE"
	 */
	public String getChargeGroupCode() {
		return chargeGroupCode;
	}

	public void setChargeGroupCode(String chargeGroupCode) {
		this.chargeGroupCode = chargeGroupCode;
	}

	/**
	 * returns the charge rates related with this Charge
	 * 
	 * @hibernate.set lazy="false" cascade="all"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airpricing.api.model.ChargeRate"
	 * @hibernate.collection-key column="CHARGE_CODE"
	 */
	public Set<ChargeRate> getChargeRates() {
		return chargeRates;
	}

	public void setChargeRates(Set<ChargeRate> chargeRates) {
		this.chargeRates = chargeRates;
	}

	/**
	 * @hibernate.set table="T_OND_CHARGE" cascade="all"
	 * @hibernate.collection-key column="CHARGE_CODE"
	 * @hibernate.collection-element type="string" column="OND_CODE"
	 */
	public Set<String> getOndCodes() {
		return ondCodes;
	}

	public void setOndCodes(Set<String> ondCodes) {
		this.ondCodes = ondCodes;
	}

	/**
	 * @hibernate.property column = "ARR_DEP" TODO Rename database column
	 */
	public char getDepartureOrArival() {
		return departureOrArival;
	}

	public void setDepartureOrArival(char departureOrArival) {
		this.departureOrArival = departureOrArival;
	}

	/**
	 * @return Returns the reporting Group id
	 * @hibernate.property column = "RPT_CHARGE_GROUP_ID" TODO Rename database column
	 */
	public Integer getReportingChargeGroupId() {

		return reportingChargeGroupId;
	}

	public void setReportingChargeGroupId(Integer reportingChargeGroupId) {
		this.reportingChargeGroupId = reportingChargeGroupId;
	}

	/**
	 * @return the ondApplicability
	 * @hibernate.property column = "SEG_APPLICABLE"
	 */
	public String getOndApplicability() {
		return ondApplicability;
	}

	/**
	 * @param ondApplicability
	 *            the segApplicability to set
	 */
	public void setOndApplicability(String ondApplicability) {
		this.ondApplicability = ondApplicability;
	}

	/**
	 * @return Returns the journeyType.
	 * @hibernate.property column = "JOURNEY_TYPE"
	 */
	public String getJourneyType() {
		return journeyType;
	}

	/**
	 * @param journeyType
	 *            The journeyType to set.
	 */
	public void setJourneyType(String journeyType) {
		this.journeyType = journeyType;
	}

	/**
	 * @return Returns the appliesTo.
	 * @hibernate.property column = "APPLIES_TO"
	 */
	public int getAppliesTo() {
		return appliesTo;
	}

	/**
	 * @param appliesTo
	 *            The appliesTo to set.
	 */
	public void setAppliesTo(int appliesTo) {
		this.appliesTo = appliesTo;
	}

	/**
	 * returns the charge rates related with this Charge
	 * 
	 * @hibernate.set lazy="false" cascade="all" order-by="CP_ID" inverse="true"
	 * @hibernate.collection-key column="CHARGE_CODE"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airpricing.api.model.ChargePos"
	 */
	public Collection<ChargePos> getPosCharges() {
		return posCharges;
	}

	private void setPosCharges(Collection<ChargePos> posCharges) {
		this.posCharges = posCharges;
	}

	/**
	 * Adds pos charges
	 * 
	 * @param chargePos
	 */
	public void addPosCharges(ChargePos chargePos) {
		if (this.getPosCharges() == null) {
			this.setPosCharges(new HashSet<ChargePos>());
		}

		chargePos.setCharge(this);
		this.getPosCharges().add(chargePos);
	}

	/**
	 * returns the charge rates related with this Charge
	 * 
	 * @hibernate.set lazy="false" cascade="all" order-by="charge_agent_id" inverse="true"
	 * @hibernate.collection-key column="CHARGE_CODE"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airpricing.api.model.ChargeAgent"
	 */
	public Collection<ChargeAgent> getAgentCharges() {
		return agentCharges;
	}

	/**
	 * @param ondCharges
	 *            the ondCharges to set
	 */
	public void setAgentCharges(Collection<ChargeAgent> agentCharges) {
		this.agentCharges = agentCharges;
	}

	/**
	 * Adds agent charges
	 * 
	 * @param chargePos
	 */
	public void addAgentCharges(ChargeAgent chargeAgent) {
		if (this.getAgentCharges() == null) {
			this.setAgentCharges(new HashSet<ChargeAgent>());
		}

		chargeAgent.setCharge(this);
		this.getAgentCharges().add(chargeAgent);
	}

	/**
	 * @return Returns the appliesTo.
	 * @hibernate.property column = "CHARGE_AGENT_FILTER"
	 */
	public String getChargeAgentFilter() {
		return chargeAgentFilter;
	}

	/**
	 * @param chargeAgentFilter
	 *            the chargeAgentFilter to set
	 */
	public void setChargeAgentFilter(String chargeAgentFilter) {
		this.chargeAgentFilter = chargeAgentFilter;
	}

	/**
	 * @return Returns the appliesTo.
	 * @hibernate.property column = "SUR_REVENUE"
	 */
	public String getSurRevenue() {
		return surRevenue;
	}

	/**
	 * @param surRevenue
	 *            the surRevenue to set
	 */
	public void setSurRevenue(String surRevenue) {
		this.surRevenue = surRevenue;
	}

	/**
	 * @return Returns the appliesTo.
	 * @hibernate.property column = "SUR_REVENUE_PERC"
	 */
	public String getSurRevenuePerc() {
		return surRevenuePerc;
	}

	/**
	 * @param surRevenuePerc
	 *            the surRevenuePerc to set
	 */
	public void setSurRevenuePerc(String surRevenuePerc) {
		this.surRevenuePerc = surRevenuePerc;
	}

	/**
	 * 
	 * @return the hasInwardTransit
	 * @hibernate.property column = "IF_HAS_INWARD_TRANSIT"
	 */
	public char getHasInwardTransit() {
		return hasInwardTransit;
	}

	/**
	 * @param hasInwardTransit
	 *            the hasInwardTransit to set
	 */
	public void setHasInwardTransit(char hasInwardTransit) {
		this.hasInwardTransit = hasInwardTransit;
	}

	/**
	 * 
	 * @return the hasOutwardTransit
	 * @hibernate.property column = "IF_HAS_OUTWARD_TRANSIT"
	 */
	public char getHasOutwardTransit() {
		return hasOutwardTransit;
	}

	/**
	 * @param hasOutwardTransit
	 *            the hasOutwardTransit to set
	 */
	public void setHasOutwardTransit(char hasOutwardTransit) {
		this.hasOutwardTransit = hasOutwardTransit;
	}

	/**
	 * 
	 * @return the maxTransitTime
	 * @hibernate.property column = "TRANSIT_QUAL_MAX_DURATION "
	 */
	public Long getMaxTransitTime() {
		return maxTransitTime;
	}

	/**
	 * @param maxTransitTime
	 *            the maxTransitTime to set
	 */
	public void setMaxTransitTime(Long maxTransitTime) {
		this.maxTransitTime = maxTransitTime;
	}

	/**
	 * 
	 * @return the minTransitTime
	 * @hibernate.property column = "TRANSIT_QUAL_MIN_DURATION"
	 */
	public Long getMinTransitTime() {
		return minTransitTime;
	}

	/**
	 * @param minTransitTime
	 *            the minTransitTime to set
	 */
	public void setMinTransitTime(Long minTransitTime) {
		this.minTransitTime = minTransitTime;
	}

	/**
	 * 
	 * @return the hasTransit
	 * @hibernate.property column = "IF_HAS_TRANSIT"
	 */
	public char getHasTransit() {
		return hasTransit;
	}

	/**
	 * @param hasTransit
	 *            the hasTransit to set
	 */
	public void setHasTransit(char hasTransit) {
		this.hasTransit = hasTransit;
	}

	/**
	 * 
	 * @hibernate.property column = "AIRPORT_TAX_CATEGORY"
	 */
	public String getAirportTaxCategory() {
		return airportTaxCategory;
	}

	public void setAirportTaxCategory(String airportTaxCategory) {
		this.airportTaxCategory = airportTaxCategory;
	}

	/**
	 * @return Returns the channelId.
	 * @hibernate.property column = "CHANNEL_ID"
	 */
	public int getChannelId() {
		return channelId;
	}

	/**
	 * @param channelId
	 *            the channelId to set
	 */
	public void setChannelId(int channelId) {
		this.channelId = channelId;
	}

	/**
	 * @return Returns the refundableOnlyforModifications.
	 * @hibernate.property column = "REFUNDABLE_ONLY_FOR_MOD"
	 */
	public String getRefundableOnlyforModifications() {
		return refundableOnlyforModifications;
	}

	/**
	 * @param refundableOnlyforModifications
	 *            the refundableOnlyforModifications to set
	 */
	public void setRefundableOnlyforModifications(String refundableOnlyforModifications) {
		this.refundableOnlyforModifications = refundableOnlyforModifications;
	}

	/**
	 * @return the excludable
	 * @hibernate.property column = "EXCLUDABLE"
	 */
	public String getExcludable() {
		return excludable;
	}

	/**
	 * @param excludable
	 *            the excludable to set
	 */
	public void setExcludable(String excludable) {
		this.excludable = excludable;
	}

	/**
	 * @return
	 * @hibernate.property column = "MIN_FLIGHT_SEARCH_GAP"
	 * 
	 */
	public Integer getMinFlightSearchGap() {
		return minFlightSearchGap;
	}

	public void setMinFlightSearchGap(Integer minFlightSearchGap) {
		this.minFlightSearchGap = minFlightSearchGap;
	}

	/**
	 * @return the maxFlightSearchGap
	 * @hibernate.property column = "MAX_FLIGHT_SEARCH_GAP"
	 */
	public Integer getMaxFlightSearchGap() {
		return maxFlightSearchGap;
	}

	/**
	 * @param maxFlightSearchGap
	 *            the maxFlightSearchGap to set
	 */
	public void setMaxFlightSearchGap(Integer maxFlightSearchGap) {
		this.maxFlightSearchGap = maxFlightSearchGap;
	}

	/**
	 * 
	 * @return the hasTransit
	 * @hibernate.property column = "REFUNDABLE_WHEN_EXCHANGE"
	 */
	public String getRefundableWhenExchange() {
		return refundableWhenExchange;
	}

	/**
	 * @param hasTransit
	 *            the hasTransit to set
	 */
	public void setRefundableWhenExchange(String refundableWhenExchange) {
		this.refundableWhenExchange = refundableWhenExchange;
	}

	/**
	 * @return the bundledFareCharge
	 * @hibernate.property column = "BUNDLED_FARE_CHARGE"
	 */
	public String getBundledFareCharge() {
		return bundledFareCharge;
	}

	/**
	 * @param bundledFareCharge
	 *            the bundledFareCharge to set
	 */
	public void setBundledFareCharge(String bundledFareCharge) {
		this.bundledFareCharge = bundledFareCharge;
	}

	/**
	 * @return the interState
	 * @hibernate.property column = "INTER_STATE"
	 */
	public String getInterState() {
		return interState;
	}

	/**
	 * @param interState
	 *            the interState to set
	 */
	public void setInterState(String interState) {
		this.interState = interState;
	}

	/**
	 * @return the country code
	 * @hibernate.property column = "COUNTRY_CODE"
	 */
	public String getCountryCode() {
		return countryCode;
	}

	/**
	 * @param country
	 *            code the country code to set
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	/**
	 * @return the state id
	 * @hibernate.property column = "STATE_ID"
	 */
	public Integer getStateId() {
		return stateId;
	}

	/**
	 * @param state
	 *            id the state id code to set
	 */
	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}

	/**
	 * @return the Charge Code Filter
	 * @hibernate.property column = "CHARGE_CODE_FILTER"
	 */
	public String getChargeCodeFilter() {
		return chargeCodeFilter;
	}

	/**
	 * @param Charge
	 *            Code Filter the Charge Code Filter to set
	 */
	public void setChargeCodeFilter(String chargeCodeFilter) {
		this.chargeCodeFilter = chargeCodeFilter;
	}

	/**
	 * returns the excluded charges related with this Charge
	 * 
	 * @hibernate.set lazy="false" cascade="all-delete-orphan" order-by="CHARGE_APPLICABLE_CHARGES_ID" inverse="true"
	 * @hibernate.collection-key column="CHARGE_CODE"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airpricing.api.model.ChargeApplicableCharges"
	 */
	public Collection<ChargeApplicableCharges> getApplicableCharges() {
		return applicableCharges;
	}

	public void setApplicableCharges(Collection<ChargeApplicableCharges> excludedCharges) {
		this.applicableCharges = excludedCharges;
	}

	/**
	 * Adds applicable charges
	 * 
	 * @param ChargeApplicableCharges
	 */
	public void addApplicableCharges(ChargeApplicableCharges applicableCharge) {
		if (this.getApplicableCharges() == null) {
			this.setApplicableCharges(new HashSet<ChargeApplicableCharges>());
		}
		this.getApplicableCharges().add(applicableCharge);
	}

	/**
	 * @return the Charge Code Filter
	 * @hibernate.property column = "INVOICE_TAX_CATEGORY"
	 */
	public String getInvoiceTaxCategory() {
		return invoiceTaxCategory;
	}

	public void setInvoiceTaxCategory(String invoiceTaxCategory) {
		this.invoiceTaxCategory = invoiceTaxCategory;
	}

	/**
	 * @return the ticketingServiceTax
	 * @hibernate.property column = "TICKETING_SERVICE_TAX"
	 */
	public String getTicketingServiceTax() {
		return ticketingServiceTax;
	}

	/**
	 * @param ticketingServiceTax
	 * the ticketingServiceTax to set
	 */
	public void setTicketingServiceTax(String ticketingServiceTax) {
		this.ticketingServiceTax = ticketingServiceTax;
	}
}
