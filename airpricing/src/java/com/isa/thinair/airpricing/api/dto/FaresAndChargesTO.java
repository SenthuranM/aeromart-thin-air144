package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class FaresAndChargesTO implements Serializable {

	private static final long serialVersionUID = -8905014671999347198L;
	private HashMap<Integer, FareTO> fareTOs;
	private HashMap<Integer, ChargeTO> chargeTOs;
	private Map<Integer, Collection<FareRuleFeeTO>> fareRuleFeeTOs;

	/**
	 * @return Returns the chargeTOs.
	 */
	public HashMap<Integer, ChargeTO> getChargeTOs() {
		return chargeTOs;
	}

	/**
	 * @param chargeTOs
	 *            The chargeTOs to set.
	 */
	public void setChargeTOs(HashMap<Integer, ChargeTO> chargeTOs) {
		this.chargeTOs = chargeTOs;
	}

	/**
	 * @return Returns the fareTOs.
	 */
	public HashMap<Integer, FareTO> getFareTOs() {
		return fareTOs;
	}

	/**
	 * @param fareTOs
	 *            The fareTOs to set.
	 */
	public void setFareTOs(HashMap<Integer, FareTO> fareTOs) {
		this.fareTOs = fareTOs;
	}

	/**
	 * @return the fareRuleFeeTOs
	 */
	public Map<Integer, Collection<FareRuleFeeTO>> getFareRuleFeeTOs() {
		return fareRuleFeeTOs;
	}

	/**
	 * @param fareRuleFeeTOs
	 *            the fareRuleFeeTOs to set
	 */
	public void setFareRuleFeeTOs(Map<Integer, Collection<FareRuleFeeTO>> fareRuleFeeTOs) {
		this.fareRuleFeeTOs = fareRuleFeeTOs;
	}

	public ChargeTO getChargeTO(Integer chargeRateId) {
		if (getChargeTOs() != null && getChargeTOs().containsKey(chargeRateId)) {
			return getChargeTOs().get(chargeRateId);
		}
		return null;
	}

	public FareTO getFareTO(Integer fareId) {
		if (getFareTOs() != null && getFareTOs().containsKey(fareId)) {
			return getFareTOs().get(fareId);
		}
		return null;
	}

}
