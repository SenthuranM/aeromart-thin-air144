/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:20:29
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airpricing.api.dto.ondcharge;

import java.io.Serializable;
import java.util.Date;

public class OndChargeRateTO implements Serializable {

	private static final long serialVersionUID = 3962183035371958827L;

	public static final String CHARGE_BASIS_V = "V";

	public static final String CHARGE_BASIS_PF = "PF";

	public static final String CHARGE_BASIS_PTF = "PTF";

	private int chargeRateCode;

	private Date chargeEffectiveFromDate;

	private Date chargeEffectiveToDate;

	private String valuePercentageFlag;

	private Double chargeValuePercentage;

	/**
	 * @return Returns the chargeEffectiveFromDate.
	 */
	public Date getChargeEffectiveFromDate() {
		return chargeEffectiveFromDate;
	}

	/**
	 * @param chargeEffectiveFromDate
	 *            The chargeEffectiveFromDate to set.
	 */
	public void setChargeEffectiveFromDate(Date chargeEffectiveFromDate) {
		this.chargeEffectiveFromDate = chargeEffectiveFromDate;
	}

	/**
	 * @return Returns the chargeEffectiveToDate.
	 */
	public Date getChargeEffectiveToDate() {
		return chargeEffectiveToDate;
	}

	/**
	 * @param chargeEffectiveToDate
	 *            The chargeEffectiveToDate to set.
	 */
	public void setChargeEffectiveToDate(Date chargeEffectiveToDate) {
		this.chargeEffectiveToDate = chargeEffectiveToDate;
	}

	/**
	 * @return Returns the chargeRateCode.
	 */
	public int getChargeRateCode() {
		return chargeRateCode;
	}

	/**
	 * @param chargeRateCode
	 *            The chargeRateCode to set.
	 */
	public void setChargeRateCode(int chargeRateCode) {
		this.chargeRateCode = chargeRateCode;
	}

	/**
	 * @return Returns the chargeValuePercentage.
	 */
	public Double getChargeValuePercentage() {
		return chargeValuePercentage;
	}

	/**
	 * @param chargeValuePercentage
	 *            The chargeValuePercentage to set.
	 */
	public void setChargeValuePercentage(Double chargeValuePercentage) {
		this.chargeValuePercentage = chargeValuePercentage;
	}

	/**
	 * @return Returns the valuePercentageFlag.
	 */
	public String getValuePercentageFlag() {
		return valuePercentageFlag;
	}

	/**
	 * @param valuePercentageFlag
	 *            The valuePercentageFlag to set.
	 */
	public void setValuePercentageFlag(String valuePercentageFlag) {
		this.valuePercentageFlag = valuePercentageFlag;
	}

}
