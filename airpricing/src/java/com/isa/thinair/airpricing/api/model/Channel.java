package com.isa.thinair.airpricing.api.model;

import java.io.Serializable;

/**
 * @author MN
 * @hibernate.class table = "T_SALES_CHANNEL"
 */
public class Channel implements Serializable {

	private static final long serialVersionUID = 8891049698311413503L;

	private int channelId;

	private String channelDescription;

	/**
	 * @hibernate.id column = "SALES_CHANNEL_CODE" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_SALES_CHANNEL"
	 */
	public int getChannelId() {
		return channelId;
	}

	public void setChannelId(int channelId) {
		this.channelId = channelId;
	}

	/**
	 * @hibernate.property column = "DESCRIPTION"
	 */
	public String getChannelDescription() {
		return channelDescription;
	}

	public void setChannelDescription(String channelDescription) {
		this.channelDescription = channelDescription;
	}
}
