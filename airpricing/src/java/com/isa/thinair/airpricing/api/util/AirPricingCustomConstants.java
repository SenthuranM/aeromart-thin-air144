package com.isa.thinair.airpricing.api.util;

/**
 * Pricing module constants for the module clients to refer.
 */
public interface AirPricingCustomConstants {

	/** Denotes the passenger type at booking creation time */
	public static interface BookingPaxType {
		/** Denotes any passenger */
		public static String ANY = "A";

		/** Denotes local passenger */
		public static String LOCAL = "L";

		/** Denotes foreign passenger */
		public static String FOREIGN = "F";
	}

	/** Denotes the fare category type */
	public static interface FareCategoryType {
		/** Denotes Any fare category - a fare category not belonging to any specific type */
		public static String ANY = "A";

		/** Denotes normal (unrestricted) fare category */
		public static String NORMAL = "N";

		/** Denotes restricted fare category - a categority to which restricts apply for modifications */
		public static String RESTRICTED = "R";
	}

	/** Holds Flexi Charge percentage basis */
	public static enum FLEXI_CHARGE_PERCENTAGE_BASIS {
		FARE_ONLY, FARE_AND_SURCHARGE, TOTAL_PRICE
	};

	/** Holds Flexi Rule Flexibility type */
	public static enum FLEXI_RULE_FLEXIBILITY_TYPE {
		MODIFY_OND, CANCEL_OND
	};

	/**
	 * Holds Modification \ Cancellation charge types
	 * 
	 */
	public enum ChargeTypes {

		ABSOLUTE_VALUE("V"),

		PERCENTAGE_OF_FARE_PF("PF"),

		PERCENTAGE_OF_FARE_AND_SUR_PFS("PFS"),
		
		PERCENTAGE_OF_TOTAL_FARE_PTF("PTF");

		private String chargeType;

		private ChargeTypes(String chargeType) {
			this.chargeType = chargeType;
		}

		public String getChargeTypes() {
			return chargeType;
		}

	}
	
	public static final String STATUS_ACTIVE = "ACT";
	public static final String STATUS_INACTIVE = "INA";
}
