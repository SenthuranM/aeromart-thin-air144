/**
 * 
 */
package com.isa.thinair.airpricing.api.model;

import java.util.Date;
import java.util.Set;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * @author kasun
 * @hibernate.class table = "BG_T_OND_CHARGE_TEMPLATE"
 */
public class ONDBaggageTemplate extends Tracking {

	private static final long serialVersionUID = 1345353535535L;
	
	private int templateId;

	private String description;

	private String status;

	private String templateCode;

	private Set<ONDBaggageCharge> baggageCharges;

	private Date fromDate;

	private Date toDate;

	private String chargeLocalCurrencyCode;

	private Set<ONDBaggageChargeTemplate> onds;

	private Set<BaggageTemplateFlight> flights;

	private Set<BaggageTemplateAgent> agents;

	private Set<BaggageTemplateBookingClass> bookingClasses;

	private Set<BaggageTemplateSalesChannel> salesChannels;

	/**
	 * @param templateId
	 *            the templateId to set
	 */
	public void setTemplateId(int templateId) {
		this.templateId = templateId;
	}

	/**
	 * @return Returns the templateId.
	 * @hibernate.id column = "OND_CHARGE_TEMPLATE_ID" generator-class =
	 *               "native"
	 * @hibernate.generator-param name = "sequence" value =
	 *                            "BG_S_OND_CHARGE_TEMPLATE"
	 */
	public int getTemplateId() {
		return templateId;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the description
	 * @hibernate.property column = "DESCRIPTION"
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the status
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param templateCode
	 *            the templateCode to set
	 */
	public void setTemplateCode(String templateCode) {
		this.templateCode = templateCode;
	}

	/**
	 * @return the templateCode
	 * @hibernate.property column = "CHARGE_TEMPLATE_CODE"
	 */
	public String getTemplateCode() {
		return templateCode;
	}

	/**
	 * @param baggageCharges
	 *            the baggageCharges to set
	 */
	public void setBaggageCharges(Set<ONDBaggageCharge> baggageCharges) {
		this.baggageCharges = baggageCharges;
	}

	/**
	 * @hibernate.set lazy="false" cascade="all" order-by="BAGGAGE_ID"
	 * @hibernate.collection-key column="OND_CHARGE_TEMPLATE_ID"
	 * @hibernate.collection-one-to-many 
	 *                                   class="com.isa.thinair.airpricing.api.model.ONDBaggageCharge"
	 * 
	 * @return Set of BaggageCharge objects
	 */
	public Set<ONDBaggageCharge> getBaggageCharges() {
		return baggageCharges;
	}

	public int hashCode() {
		return new HashCodeBuilder().append(getTemplateCode()).toHashCode();
	}

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ONDBaggageTemplate other = (ONDBaggageTemplate) obj;
		if (templateCode == null) {
			if (other.templateCode != null)
				return false;
		} else if (!templateCode.equals(other.templateCode))
			return false;
		if (templateId != other.templateId)
			return false;
		return true;
	}

	/**
	 * @return the from date
	 * @hibernate.property column = "FROM_DATE"
	 */
	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * @return the toDate
	 * @hibernate.property column = "END_DATE"
	 */
	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	/**
	 * @hibernate.set cascade="all-delete-orphan" inverse="true" lazy="false"
	 * @hibernate.collection-key column="OND_CHARGE_TEMPLATE_ID"
	 * @hibernate.collection-one-to-many 
	 *                                   class="com.isa.thinair.airpricing.api.model.ONDBaggageChargeTemplate"
	 */
	public Set<ONDBaggageChargeTemplate> getOnds() {
		return onds;
	}

	public void setOnds(Set<ONDBaggageChargeTemplate> onds) {
		this.onds = onds;
	}

	/**
	 * @hibernate.set cascade="all-delete-orphan" inverse="true" lazy="false"
	 * @hibernate.collection-key column="OND_CHARGE_TEMPLATE_ID"
	 * @hibernate.collection-one-to-many 
	 *                                   class="com.isa.thinair.airpricing.api.model.BaggageTemplateFlight"
	 */
	public Set<BaggageTemplateFlight> getFlights() {
		return flights;
	}

	public void setFlights(Set<BaggageTemplateFlight> flights) {
		this.flights = flights;
	}

	/**
	 * @hibernate.set cascade="all-delete-orphan" inverse="true" lazy="false"
	 * @hibernate.collection-key column="OND_CHARGE_TEMPLATE_ID"
	 * @hibernate.collection-one-to-many 
	 *                                   class="com.isa.thinair.airpricing.api.model.BaggageTemplateAgent"
	 */
	public Set<BaggageTemplateAgent> getAgents() {
		return agents;
	}

	public void setAgents(Set<BaggageTemplateAgent> agents) {
		this.agents = agents;
	}

	/**
	 * @hibernate.set cascade="all-delete-orphan" inverse="true" lazy="false"
	 * @hibernate.collection-key column="OND_CHARGE_TEMPLATE_ID"
	 * @hibernate.collection-one-to-many 
	 *                                   class="com.isa.thinair.airpricing.api.model.BaggageTemplateBookingClass"
	 */
	public Set<BaggageTemplateBookingClass> getBookingClasses() {
		return bookingClasses;
	}

	public void setBookingClasses(
			Set<BaggageTemplateBookingClass> bookingClasses) {
		this.bookingClasses = bookingClasses;
	}

	/**
	 * @hibernate.set cascade="all-delete-orphan" inverse="true" lazy="false"
	 * @hibernate.collection-key column="OND_CHARGE_TEMPLATE_ID"
	 * @hibernate.collection-one-to-many 
	 *                                   class="com.isa.thinair.airpricing.api.model.BaggageTemplateSalesChannel"
	 */
	public Set<BaggageTemplateSalesChannel> getSalesChannels() {
		return salesChannels;
	}

	public void setSalesChannels(Set<BaggageTemplateSalesChannel> salesChannels) {
		this.salesChannels = salesChannels;
	}
	
	/**
	 * @return the chargeLocalCurrencyCode
	 * @hibernate.property column = "LOCAL_CURRENCY_CODE"
	 */
	public String getChargeLocalCurrencyCode() {
		return chargeLocalCurrencyCode;
	}

	public void setChargeLocalCurrencyCode(String chargeLocalCurrency) {
		this.chargeLocalCurrencyCode = chargeLocalCurrency;
	}

	
	

}
