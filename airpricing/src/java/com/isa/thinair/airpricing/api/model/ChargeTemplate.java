/**
 * 
 */
package com.isa.thinair.airpricing.api.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Set;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * @author indika
 * 
 * @hibernate.class table = "SM_T_AM_CHARGE_TEMPLATE"
 * 
 */
public class ChargeTemplate extends Tracking implements Serializable {

	private static final long serialVersionUID = 3567323191583462583L;
	
	private int templateId;

	private String modelNo;

	private String templateCode;

	private String description;

	private BigDecimal defaultChargeAmount;

	private String userNote;

	private Set<SeatCharge> seatCharges;

	private String status;
	
	private String chargeLocalCurrencyCode;

	/**
	 * 
	 * @return
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * 
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	public int hashCode() {
		return new HashCodeBuilder().append(getTemplateCode()).toHashCode();
	}

	/**
	 * @return Returns the templateId.
	 * @hibernate.id column = "AMC_TEMPLATE_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "SM_S_AM_CHARGE_TEMPLATE"
	 */
	public int getTemplateId() {
		return templateId;
	}

	/**
	 * @param templateId
	 *            The templateId to set.
	 */
	public void setTemplateId(int templateId) {
		this.templateId = templateId;
	}

	/**
	 * @return Returns the defaultChargeAmount.
	 * 
	 * @hibernate.property column = "DEFAULT_CHARGE_AMOUNT"
	 */
	public BigDecimal getDefaultChargeAmount() {
		return defaultChargeAmount;
	}

	/**
	 * @param defaultChargeAmount
	 *            The defaultChargeAmount to set.
	 */
	public void setDefaultChargeAmount(BigDecimal defaultChargeAmount) {
		this.defaultChargeAmount = defaultChargeAmount;
	}

	/**
	 * @return Returns the description.
	 * 
	 * @hibernate.property column = "DESCRIPTION"
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            The description to set.
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return Returns the modelNo.
	 * 
	 * @hibernate.property column = "MODEL_NUMBER"
	 */
	public String getModelNo() {
		return modelNo;
	}

	/**
	 * @param modelNo
	 *            The modelNo to set.
	 */
	public void setModelNo(String modelNo) {
		this.modelNo = modelNo;
	}

	/**
	 * @return Returns the templateCode.
	 * 
	 * @hibernate.property column = "AMC_TEMPLATE_CODE"
	 */
	public String getTemplateCode() {
		return templateCode;
	}

	/**
	 * @param templateCode
	 *            The templateCode to set.
	 */
	public void setTemplateCode(String templateCode) {
		this.templateCode = templateCode;
	}

	/**
	 * @return Returns the userNote.
	 * 
	 * @hibernate.property column = "USER_NOTES"
	 */
	public String getUserNote() {
		return userNote;
	}

	/**
	 * @param userNote
	 *            The userNote to set.
	 */
	public void setUserNote(String userNote) {
		this.userNote = userNote;
	}

	/**
	 * @hibernate.set lazy="false" cascade="all" order-by="AM_SEAT_ID"
	 * @hibernate.collection-key column="AMC_TEMPLATE_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airpricing.api.model.SeatCharge"
	 * 
	 * @return Set of SeatCharge objects
	 */
	public Set<SeatCharge> getSeatCharges() {
		return seatCharges;
	}

	/**
	 * 
	 * @param Set
	 *            of SeatCharge objects
	 */
	public void setSeatCharges(Set<SeatCharge> seatCharges) {
		this.seatCharges = seatCharges;
	}
	
	/**
	 * @return the chargeLocalCurrencyCode
	 * @hibernate.property column = "LOCAL_CURRENCY_CODE"
	 */
	public String getChargeLocalCurrencyCode() {
		return chargeLocalCurrencyCode;
	}

	public void setChargeLocalCurrencyCode(String chargeLocalCurrency) {
		this.chargeLocalCurrencyCode = chargeLocalCurrency;
	}
}
