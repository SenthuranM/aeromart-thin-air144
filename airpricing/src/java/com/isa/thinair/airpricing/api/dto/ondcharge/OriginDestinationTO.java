/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:20:29
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airpricing.api.dto.ondcharge;

import java.io.Serializable;
import java.util.List;

public class OriginDestinationTO implements Serializable {

	private static final long serialVersionUID = -586263535976895398L;

	private String ondCode;

	private String originAirportCode;

	private String destinationAirportCode;

	private List<OndChargeTO> charges;

	/**
	 * @return Returns the charges.
	 */
	public List<OndChargeTO> getCharges() {
		return charges;
	}

	/**
	 * @param charges
	 *            The charges to set.
	 */
	public void setCharges(List<OndChargeTO> charges) {
		this.charges = charges;
	}

	/**
	 * @return Returns the destinationAirportCode.
	 */
	public String getDestinationAirportCode() {
		return destinationAirportCode;
	}

	/**
	 * @param destinationAirportCode
	 *            The destinationAirportCode to set.
	 */
	public void setDestinationAirportCode(String destinationAirportCode) {
		this.destinationAirportCode = destinationAirportCode;
	}

	/**
	 * @return Returns the ondCode.
	 */
	public String getOndCode() {
		return ondCode;
	}

	/**
	 * @param ondCode
	 *            The ondCode to set.
	 */
	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}

	/**
	 * @return Returns the originAirportCode.
	 */
	public String getOriginAirportCode() {
		return originAirportCode;
	}

	/**
	 * @param originAirportCode
	 *            The originAirportCode to set.
	 */
	public void setOriginAirportCode(String originAirportCode) {
		this.originAirportCode = originAirportCode;
	}

}
