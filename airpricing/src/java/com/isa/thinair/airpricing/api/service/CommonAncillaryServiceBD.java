package com.isa.thinair.airpricing.api.service;

import java.util.Map;

import com.isa.thinair.airpricing.api.criteria.DefaultAnciTemplSearchCriteria;
import com.isa.thinair.airpricing.api.dto.DefaultAnciTemplateTO;
import com.isa.thinair.airpricing.api.model.DefaultAnciTemplate;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

public interface CommonAncillaryServiceBD {

	public static final String SERVICE_NAME = "CommonAncillaryService";

	public Page<DefaultAnciTemplate> getAllDefaultAnciTemplate() throws ModuleException;

	public void saveDefaultAnciTemplate(DefaultAnciTemplate template) throws ModuleException;

	public Page<DefaultAnciTemplateTO> searchDefAnciTemplate(DefaultAnciTemplSearchCriteria criteria, int startRec, int noRecs)
			throws ModuleException;

	public void checkDuplicateDefaultAnciTemplateForSameRoute(DefaultAnciTemplateTO templateTO) throws ModuleException;

	public void checkTemplateAttachedToRouteWiseDefAnciTempl(DefaultAnciTemplate.ANCI_TEMPLATES anciType, int templateId)
			throws ModuleException;
	
	public Map<String,Integer> getDefaultAnciTemplateForFlight(String ondCode, String flightModel)throws ModuleException;

	public void deleteDefaultAnciTemplate(DefaultAnciTemplate template) throws ModuleException;
}
