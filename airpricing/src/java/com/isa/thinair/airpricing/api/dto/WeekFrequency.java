package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;
import java.util.Calendar;

public class WeekFrequency implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean[] weekFreq = null;

	public WeekFrequency(boolean sun, boolean mon, boolean tue, boolean wed, boolean thu, boolean fri, boolean sat) {

		weekFreq = new boolean[7];
		weekFreq[Calendar.SUNDAY - 1] = sun;
		weekFreq[Calendar.MONDAY - 1] = mon;
		weekFreq[Calendar.TUESDAY - 1] = tue;
		weekFreq[Calendar.WEDNESDAY - 1] = wed;
		weekFreq[Calendar.THURSDAY - 1] = thu;
		weekFreq[Calendar.FRIDAY - 1] = fri;
		weekFreq[Calendar.SATURDAY - 1] = sat;
	}

	public boolean isValid(Calendar date) {
		return weekFreq[date.get(Calendar.DAY_OF_WEEK) - 1];
	}
}