/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:09:10
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airpricing.api.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airpricing.api.criteria.AgentFareRuleSearchCriteria;
import com.isa.thinair.airpricing.api.dto.AgentFareRulesDTO;
import com.isa.thinair.airpricing.api.model.Channel;
import com.isa.thinair.airpricing.api.model.FareRule;
import com.isa.thinair.airreservation.api.dto.OnHoldReleaseTimeDTO;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Byorn
 */
public interface FareRuleBD {

	public static final String SERVICE_NAME = "FareRuleService";

	/**
	 * Will save non critical fields like description, comment etc. Will avoid splitting. Any additions or subtractions
	 * to non critical fields than need to be saved will have to be passed on and then set from the BL level.
	 * 
	 * 
	 * /** BD Interface to get a FareRule
	 **/
	public FareRule getFareRule(int fareRuleID) throws ModuleException;

	public FareRule getFareRule(String fareRuleCodeD) throws ModuleException;

	/** BD Interface method to a page of Agent vise Master Fare Rules **/
	public Collection<AgentFareRulesDTO> getAgentFareRules(AgentFareRuleSearchCriteria agentFareRuleSearchCriteria,
			List<String> orderBy) throws ModuleException;

	/** BD Interface method to get a list of channels **/
	public Collection<Channel> getChannels(Collection<Integer> channelIds) throws ModuleException;

	public void createFareRule(FareRule fareRule, boolean splitFares) throws ModuleException;

	public Page getFareRules(List<ModuleCriterion> criteria, int startIndex, int pageSize, List<String> orderBy)
			throws ModuleException;

	public void deleteFareRule(int fareRuleID) throws ModuleException;

	/** BD Interface method to get all sales channels **/
	public Collection<Channel> getAllSalesChannels() throws ModuleException;

	public void updateFareRule(FareRule fareRule) throws ModuleException;

	public Map<Integer, FareRule> getFareRulesMap(Collection<Integer> fareRuleIds) throws ModuleException;

	public FareRule getFareRuleForFareID(Integer fareId) throws ModuleException;

	public Collection<OnHoldReleaseTimeDTO> getOHDRelTimeDTOForFareRuleId(Integer fareRuleId) throws ModuleException;

	public Collection<Integer> getFareIdsForFareRule(Integer fareRuleId) throws ModuleException;

}
