package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import com.isa.thinair.airpricing.api.util.AirPricingCustomConstants;
import com.isa.thinair.commons.api.dto.FareCategoryTO;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

/**
 * DTO Used when Creating a New Fare
 * 
 * @author Byorn
 */
public class FareTO implements Serializable {

	private static final long serialVersionUID = -872712184195676821L;

	private int fareId;

	private int fareRuleID;

	private String fareCategoryCode;

	private String fareRulesComments;

	private String fareRulesCommentsInSelected;

	private String fareRuleCode;

	private String fareBasisCode;

	private String bookingClassCode;

	private String ondCode;
	
	private String currencyCode;

	private boolean ignoreFareValueDecimals;

	private String agentCommissionType;

	private BigDecimal agentCommissionAmount;

	private FareCategoryTO fareCategoryTO = new FareCategoryTO();

	private BigDecimal adultFareAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal childFareRatio = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal infantFareRatio = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal adultCancellationCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal childCancellationCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal infantCancellationCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal adultModificationCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal childModificationCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal infantModificationCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal adultNoShoreCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal childNoShoreCharge = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal infantNoShoreCharge = AccelAeroCalculator.getDefaultBigDecimalZero();

	private boolean isAdultFareRefundable;
	private boolean isChildFareRefundable;
	private boolean isInfantFareRefundable;

	private boolean isAdultApplyFare;
	private boolean isChildApplyFare;
	private boolean isInfantApplyFare;

	private boolean isAdultApplyModChg;
	private boolean isChildApplyModChg;
	private boolean isInfantApplyModChg;

	private boolean isAdultApplyCnxChg;
	private boolean isChildApplyCnxChg;
	private boolean isInfantApplyCnxChg;

	private String childFareType;
	private String infantFareType;

	private long internationalModifyBufferTimeInMillis;
	private long domesticModifyBufferTimeInMillis;

	private long maximumStayOver;
	private long maximumStayOverMonths;

	private long minimumStayOver;
	private long minimumStayOverMonths;

	private long openReturnConfirmPeriod;
	private long openReturnConfirmPeriodMonths;

	private boolean printExpiryDate;

	private boolean isReturnFareRule;

	private FareChargesTO fareChargesTO;

	/* segment modification */
	private boolean isAllowModifyDate;

	private boolean isAllowModifyRoute;

	private Integer fareDiscountMin;

	private Integer fareDiscountMax;

	/* no show charge type */
	private String adultNoShowChargeBasis;
	private String childNoShowChargeBasis;
	private String infantNoShowChargeBasis;

	private BigDecimal adultNoShowChargeBreakPoint = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal childNoShowChargeBreakPoint = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal infantNoShowChargeBreakPoint = AccelAeroCalculator.getDefaultBigDecimalZero();

	private BigDecimal adultNoShowChargeBoundary = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal childNoShowChargeBoundary = AccelAeroCalculator.getDefaultBigDecimalZero();
	private BigDecimal infantNoShowChargeBoundary = AccelAeroCalculator.getDefaultBigDecimalZero();

	private String agentInstructions;

	private boolean noonDayFeePolicyEnabled;

	private boolean isModifyToSameFare;

	private BigDecimal nameChangeChargeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
	
	private boolean bulkTicketFareRule;

	/**
	 * @return Returns the fareCategoryCode.
	 */
	public String getFareCategoryCode() {
		return fareCategoryCode;
	}

	/**
	 * @param fareCategoryCode
	 *            The fareCategoryCode to set.
	 */
	public void setFareCategoryCode(String fareCategoryCode) {
		this.fareCategoryCode = fareCategoryCode;
	}

	/**
	 * @return Returns the fareRulesComments.
	 */
	public String getFareRulesComments() {
		return fareRulesComments;
	}

	/**
	 * @param fareRulesComments
	 *            The fareRulesComments to set.
	 */
	public void setFareRulesComments(String fareRulesComments) {
		this.fareRulesComments = fareRulesComments;
	}

	/**
	 * @return Returns the fareCategoryTO.
	 */
	public FareCategoryTO getFareCategoryTO() {
		return fareCategoryTO;
	}

	/**
	 * @param fareCategoryTO
	 *            The fareCategoryTO to set.
	 */
	public void setFareCategoryTO(FareCategoryTO fareCategoryTO) {
		this.fareCategoryTO = fareCategoryTO;
	}

	/**
	 * Returns if the fare is restricted
	 * 
	 * @return
	 */
	public boolean isFareRestricted() {
		if (AirPricingCustomConstants.FareCategoryType.RESTRICTED.equals(fareCategoryCode)) {
			return true;
		}

		return false;
	}

	/**
	 * @return Returns the adultCancellationCharge.
	 */
	public BigDecimal getAdultCancellationCharge() {
		return adultCancellationCharge;
	}

	/**
	 * @param adultCancellationCharge
	 *            The adultCancellationCharge to set.
	 */
	public void setAdultCancellationCharge(BigDecimal adultCancellationCharge) {
		this.adultCancellationCharge = adultCancellationCharge;
	}

	/**
	 * @return Returns the adultModificationCharge.
	 */
	public BigDecimal getAdultModificationCharge() {
		return adultModificationCharge;
	}

	/**
	 * @param adultModificationCharge
	 *            The adultModificationCharge to set.
	 */
	public void setAdultModificationCharge(BigDecimal adultModificationCharge) {
		this.adultModificationCharge = adultModificationCharge;
	}

	/**
	 * @return Returns the childCancellationCharge.
	 */
	public BigDecimal getChildCancellationCharge() {
		return childCancellationCharge;
	}

	/**
	 * @param childCancellationCharge
	 *            The childCancellationCharge to set.
	 */
	public void setChildCancellationCharge(BigDecimal childCancellationCharge) {
		this.childCancellationCharge = childCancellationCharge;
	}

	/**
	 * @return Returns the childModificationCharge.
	 */
	public BigDecimal getChildModificationCharge() {
		return childModificationCharge;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	/**
	 * @param childModificationCharge
	 *            The childModificationCharge to set.
	 */
	public void setChildModificationCharge(BigDecimal childModificationCharge) {
		this.childModificationCharge = childModificationCharge;
	}

	/**
	 * @return Returns the infantCancellationCharge.
	 */
	public BigDecimal getInfantCancellationCharge() {
		return infantCancellationCharge;
	}

	/**
	 * @param infantCancellationCharge
	 *            The infantCancellationCharge to set.
	 */
	public void setInfantCancellationCharge(BigDecimal infantCancellationCharge) {
		this.infantCancellationCharge = infantCancellationCharge;
	}

	/**
	 * @return Returns the infantModificationCharge.
	 */
	public BigDecimal getInfantModificationCharge() {
		return infantModificationCharge;
	}

	/**
	 * @param infantModificationCharge
	 *            The infantModificationCharge to set.
	 */
	public void setInfantModificationCharge(BigDecimal infantModificationCharge) {
		this.infantModificationCharge = infantModificationCharge;
	}

	/**
	 * @return Returns the isAdultFareRefundable.
	 */
	public boolean isAdultFareRefundable() {
		return isAdultFareRefundable;
	}

	/**
	 * @param isAdultFareRefundable
	 *            The isAdultFareRefundable to set.
	 */
	public void setAdultFareRefundable(boolean isAdultFareRefundable) {
		this.isAdultFareRefundable = isAdultFareRefundable;
	}

	/**
	 * @return Returns the isChildFareRefundable.
	 */
	public boolean isChildFareRefundable() {
		return isChildFareRefundable;
	}

	/**
	 * @param isChildFareRefundable
	 *            The isChildFareRefundable to set.
	 */
	public void setChildFareRefundable(boolean isChildFareRefundable) {
		this.isChildFareRefundable = isChildFareRefundable;
	}

	/**
	 * @return Returns the isInfantFareRefundable.
	 */
	public boolean isInfantFareRefundable() {
		return isInfantFareRefundable;
	}

	/**
	 * @param isInfantFareRefundable
	 *            The isInfantFareRefundable to set.
	 */
	public void setInfantFareRefundable(boolean isInfantFareRefundable) {
		this.isInfantFareRefundable = isInfantFareRefundable;
	}

	/**
	 * @return Returns the fareId.
	 */
	public int getFareId() {
		return fareId;
	}

	/**
	 * @param fareId
	 *            The fareId to set.
	 */
	public void setFareId(int fareId) {
		this.fareId = fareId;
	}

	/**
	 * @return Returns the isAdultApplyCnxChg.
	 */
	public boolean isAdultApplyCnxChg() {
		return isAdultApplyCnxChg;
	}

	/**
	 * @param isAdultApplyCnxChg
	 *            The isAdultApplyCnxChg to set.
	 */
	public void setAdultApplyCnxChg(boolean isAdultApplyCnxChg) {
		this.isAdultApplyCnxChg = isAdultApplyCnxChg;
	}

	/**
	 * @return Returns the isAdultApplyFare.
	 */
	public boolean isAdultApplyFare() {
		return isAdultApplyFare;
	}

	/**
	 * @param isAdultApplyFare
	 *            The isAdultApplyFare to set.
	 */
	public void setAdultApplyFare(boolean isAdultApplyFare) {
		this.isAdultApplyFare = isAdultApplyFare;
	}

	/**
	 * @return Returns the isAdultApplyModChg.
	 */
	public boolean isAdultApplyModChg() {
		return isAdultApplyModChg;
	}

	/**
	 * @param isAdultApplyModChg
	 *            The isAdultApplyModChg to set.
	 */
	public void setAdultApplyModChg(boolean isAdultApplyModChg) {
		this.isAdultApplyModChg = isAdultApplyModChg;
	}

	/**
	 * @return Returns the isChildApplyCnxChg.
	 */
	public boolean isChildApplyCnxChg() {
		return isChildApplyCnxChg;
	}

	/**
	 * @param isChildApplyCnxChg
	 *            The isChildApplyCnxChg to set.
	 */
	public void setChildApplyCnxChg(boolean isChildApplyCnxChg) {
		this.isChildApplyCnxChg = isChildApplyCnxChg;
	}

	/**
	 * @return Returns the isChildApplyFare.
	 */
	public boolean isChildApplyFare() {
		return isChildApplyFare;
	}

	/**
	 * @param isChildApplyFare
	 *            The isChildApplyFare to set.
	 */
	public void setChildApplyFare(boolean isChildApplyFare) {
		this.isChildApplyFare = isChildApplyFare;
	}

	/**
	 * @return Returns the isChildApplyModChg.
	 */
	public boolean isChildApplyModChg() {
		return isChildApplyModChg;
	}

	/**
	 * @param isChildApplyModChg
	 *            The isChildApplyModChg to set.
	 */
	public void setChildApplyModChg(boolean isChildApplyModChg) {
		this.isChildApplyModChg = isChildApplyModChg;
	}

	/**
	 * @return Returns the isInfantApplyCnxChg.
	 */
	public boolean isInfantApplyCnxChg() {
		return isInfantApplyCnxChg;
	}

	/**
	 * @param isInfantApplyCnxChg
	 *            The isInfantApplyCnxChg to set.
	 */
	public void setInfantApplyCnxChg(boolean isInfantApplyCnxChg) {
		this.isInfantApplyCnxChg = isInfantApplyCnxChg;
	}

	/**
	 * @return Returns the isInfantApplyFare.
	 */
	public boolean isInfantApplyFare() {
		return isInfantApplyFare;
	}

	/**
	 * @param isInfantApplyFare
	 *            The isInfantApplyFare to set.
	 */
	public void setInfantApplyFare(boolean isInfantApplyFare) {
		this.isInfantApplyFare = isInfantApplyFare;
	}

	/**
	 * @return Returns the isInfantApplyModChg.
	 */
	public boolean isInfantApplyModChg() {
		return isInfantApplyModChg;
	}

	/**
	 * @param isInfantApplyModChg
	 *            The isInfantApplyModChg to set.
	 */
	public void setInfantApplyModChg(boolean isInfantApplyModChg) {
		this.isInfantApplyModChg = isInfantApplyModChg;
	}

	/**
	 * @return Returns the adultFareAmount.
	 */
	public BigDecimal getAdultFareAmount() {
		return adultFareAmount;
	}

	/**
	 * @param adultFareAmount
	 *            The adultFareAmount to set.
	 */
	public void setAdultFareAmount(BigDecimal adultFareAmount) {
		this.adultFareAmount = adultFareAmount;
	}

	/**
	 * @return Returns the childFareRatio.
	 */
	public BigDecimal getChildFareRatio() {
		return childFareRatio;
	}

	/**
	 * @param childFareRatio
	 *            The childFareRatio to set.
	 */
	public void setChildFareRatio(BigDecimal childFareRatio) {
		this.childFareRatio = childFareRatio;
	}

	/**
	 * @return Returns the childFareType.
	 */
	public String getChildFareType() {
		return childFareType;
	}

	/**
	 * @param childFareType
	 *            The childFareType to set.
	 */
	public void setChildFareType(String childFareType) {
		this.childFareType = childFareType;
	}

	/**
	 * @return Returns the infantFareRatio.
	 */
	public BigDecimal getInfantFareRatio() {
		return infantFareRatio;
	}

	/**
	 * @param infantFareRatio
	 *            The infantFareRatio to set.
	 */
	public void setInfantFareRatio(BigDecimal infantFareRatio) {
		this.infantFareRatio = infantFareRatio;
	}

	/**
	 * @return Returns the infantFareType.
	 */
	public String getInfantFareType() {
		return infantFareType;
	}

	/**
	 * @param infantFareType
	 *            The infantFareType to set.
	 */
	public void setInfantFareType(String infantFareType) {
		this.infantFareType = infantFareType;
	}

	/**
	 * @return Returns the fareBasisCode.
	 */
	public String getFareBasisCode() {
		return fareBasisCode;
	}

	/**
	 * @param fareBasisCode
	 *            The fareBasisCode to set.
	 */
	public void setFareBasisCode(String fareBasisCode) {
		this.fareBasisCode = fareBasisCode;
	}

	/**
	 * @return Returns the fareRuleCode.
	 */
	public String getFareRuleCode() {
		return fareRuleCode;
	}

	/**
	 * @param fareRuleCode
	 *            The fareRuleCode to set.
	 */
	public void setFareRuleCode(String fareRuleCode) {
		this.fareRuleCode = fareRuleCode;
	}

	public int getFareRuleID() {
		return fareRuleID;
	}

	public void setFareRuleID(int fareRuleID) {
		this.fareRuleID = fareRuleID;
	}

	/**
	 * @return Returns the internationalModifyBufferTimeInMillis.
	 */
	public long getInternationalModifyBufferTimeInMillis() {
		return internationalModifyBufferTimeInMillis;
	}

	/**
	 * @param internationalModifyBufferTimeInMillis
	 *            The internationalModifyBufferTimeInMillis to set.
	 */
	public void setInternationalModifyBufferTimeInMillis(long internationalModifyBufferTimeInMillis) {
		this.internationalModifyBufferTimeInMillis = internationalModifyBufferTimeInMillis;
	}

	/**
	 * @return the domesticModifyBufferTimeInMillis
	 */
	public long getDomesticModifyBufferTimeInMillis() {
		return domesticModifyBufferTimeInMillis;
	}

	/**
	 * @param domesticModifyBufferTimeInMillis
	 *            the domesticModifyBufferTimeInMillis to set
	 */
	public void setDomesticModifyBufferTimeInMillis(long domesticModifyBufferTimeInMillis) {
		this.domesticModifyBufferTimeInMillis = domesticModifyBufferTimeInMillis;
	}

	/**
	 * @return Returns the maximumStayOver.
	 */
	public long getMaximumStayOver() {
		return maximumStayOver;
	}

	/**
	 * @param maximumStayOver
	 *            The maximumStayOver to set.
	 */
	public void setMaximumStayOver(long maximumStayOver) {
		this.maximumStayOver = maximumStayOver;
	}

	/**
	 * @return Returns the printExpiryDate.
	 */
	public boolean isPrintExpiryDate() {
		return printExpiryDate;
	}

	/**
	 * @param printExpiryDate
	 *            The printExpiryDate to set.
	 */
	public void setPrintExpiryDate(boolean printExpiryDate) {
		this.printExpiryDate = printExpiryDate;
	}

	/**
	 * @return the adultNoShoreCharge
	 */
	public BigDecimal getAdultNoShoreCharge() {
		return adultNoShoreCharge;
	}

	/**
	 * @param adultNoShoreCharge
	 *            the adultNoShoreCharge to set
	 */
	public void setAdultNoShoreCharge(BigDecimal adultNoShoreCharge) {
		this.adultNoShoreCharge = adultNoShoreCharge;
	}

	/**
	 * @return the childNoShoreCharge
	 */
	public BigDecimal getChildNoShoreCharge() {
		return childNoShoreCharge;
	}

	/**
	 * @param childNoShoreCharge
	 *            the childNoShoreCharge to set
	 */
	public void setChildNoShoreCharge(BigDecimal childNoShoreCharge) {
		this.childNoShoreCharge = childNoShoreCharge;
	}

	/**
	 * @return the infantNoShoreCharge
	 */
	public BigDecimal getInfantNoShoreCharge() {
		return infantNoShoreCharge;
	}

	/**
	 * @param infantNoShoreCharge
	 *            the infantNoShoreCharge to set
	 */
	public void setInfantNoShoreCharge(BigDecimal infantNoShoreCharge) {
		this.infantNoShoreCharge = infantNoShoreCharge;
	}

	/**
	 * @return the isReturnFareRule
	 */
	public boolean isReturnFareRule() {
		return isReturnFareRule;
	}

	/**
	 * @param isReturnFareRule
	 *            the isReturnFareRule to set
	 */
	public void setReturnFareRule(boolean isReturnFareRule) {
		this.isReturnFareRule = isReturnFareRule;
	}

	/**
	 * @return the minimumStayOver
	 */
	public long getMinimumStayOver() {
		return minimumStayOver;
	}

	/**
	 * @param minimumStayOver
	 *            the minimumStayOver to set
	 */
	public void setMinimumStayOver(long minimumStayOver) {
		this.minimumStayOver = minimumStayOver;
	}

	/**
	 * @return the openReturnConfirmPeriod
	 */
	public long getOpenReturnConfirmPeriod() {
		return openReturnConfirmPeriod;
	}

	/**
	 * @param openReturnConfirmPeriod
	 *            the openReturnConfirmPeriod to set
	 */
	public void setOpenReturnConfirmPeriod(long openReturnConfirmPeriod) {
		this.openReturnConfirmPeriod = openReturnConfirmPeriod;
	}

	/**
	 * @return the minimumStayOverMonths
	 */
	public long getMinimumStayOverMonths() {
		return minimumStayOverMonths;
	}

	/**
	 * @param minimumStayOverMonths
	 *            the minimumStayOverMonths to set
	 */
	public void setMinimumStayOverMonths(long minimumStayOverMonths) {
		this.minimumStayOverMonths = minimumStayOverMonths;
	}

	/**
	 * @return the maximumStayOverMonths
	 */
	public long getMaximumStayOverMonths() {
		return maximumStayOverMonths;
	}

	/**
	 * @param maximumStayOverMonths
	 *            the maximumStayOverMonths to set
	 */
	public void setMaximumStayOverMonths(long maximumStayOverMonths) {
		this.maximumStayOverMonths = maximumStayOverMonths;
	}

	/**
	 * @return the openReturnConfirmPeriodMonths
	 */
	public long getOpenReturnConfirmPeriodMonths() {
		return openReturnConfirmPeriodMonths;
	}

	/**
	 * @param openReturnConfirmPeriodMonths
	 *            the openReturnConfirmPeriodMonths to set
	 */
	public void setOpenReturnConfirmPeriodMonths(long openReturnConfirmPeriodMonths) {
		this.openReturnConfirmPeriodMonths = openReturnConfirmPeriodMonths;
	}

	/**
	 * @return the bookingClassCode
	 */
	public String getBookingClassCode() {
		return bookingClassCode;
	}

	/**
	 * @param bookingClassCode
	 *            the bookingClassCode to set
	 */
	public void setBookingClassCode(String bookingClassCode) {
		this.bookingClassCode = bookingClassCode;
	}

	/**
	 * @return the ondCode
	 */
	public String getOndCode() {
		return ondCode;
	}

	/**
	 * @param ondCode
	 *            the ondCode to set
	 */
	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}

	/**
	 * @return the fareChargesTO
	 */
	public FareChargesTO getFareChargesTO() {
		return fareChargesTO;
	}

	/**
	 * @param fareChargesTO
	 *            the fareChargesTO to set
	 */
	public void setFareChargesTO(FareChargesTO fareChargesTO) {
		this.fareChargesTO = fareChargesTO;
	}

	/* segment modification */
	/**
	 * @return the allowModifyDate
	 */
	public boolean getIsAllowModifyDate() {
		return isAllowModifyDate;
	}

	/**
	 * @param allowModifyDate
	 *            the allowModifyDate to set
	 */
	public void setIsAllowModifyDate(boolean isAllowModifyDate) {
		this.isAllowModifyDate = isAllowModifyDate;
	}

	/**
	 * @return the allowModifyRoute
	 */
	public boolean getIsAllowModifyRoute() {
		return isAllowModifyRoute;
	}

	/**
	 * @param allowModifyRoute
	 *            the allowModifyRoute to set
	 */
	public void setIsAllowModifyRoute(boolean isAllowModifyRoute) {
		this.isAllowModifyRoute = isAllowModifyRoute;
	}

	public Integer getFareDiscountMin() {
		return fareDiscountMin;
	}

	public void setFareDiscountMin(Integer fareDiscountMin) {
		this.fareDiscountMin = fareDiscountMin;
	}

	public Integer getFareDiscountMax() {
		return fareDiscountMax;
	}

	public void setFareDiscountMax(Integer fareDiscountMax) {
		this.fareDiscountMax = fareDiscountMax;
	}

	/**
	 * @return the noShowChargeBasis
	 */
	public String getAdultNoShowChargeBasis() {
		return adultNoShowChargeBasis;
	}

	/**
	 * @param noShowChargeBasis
	 *            the noShowChargeBasis to set
	 */
	public void setAdultNoShowChargeBasis(String adultNoShowChargeBasis) {
		this.adultNoShowChargeBasis = adultNoShowChargeBasis;
	}

	/**
	 * @return the noShowChargeBreakPoint
	 */
	public BigDecimal getAdultNoShowChargeBreakPoint() {
		return adultNoShowChargeBreakPoint;
	}

	/**
	 * @param noShowChargeBreakPoint
	 *            the noShowChargeBreakPoint to set
	 */
	public void setAdultNoShowChargeBreakPoint(BigDecimal adultNoShowChargeBreakPoint) {
		this.adultNoShowChargeBreakPoint = adultNoShowChargeBreakPoint;
	}

	/**
	 * @return the noShowChargeBoundary
	 */
	public BigDecimal getAdultNoShowChargeBoundary() {
		return adultNoShowChargeBoundary;
	}

	/**
	 * @param noShowChargeBoundary
	 *            the noShowChargeBoundary to set
	 */
	public void setAdultNoShowChargeBoundary(BigDecimal adultNoShowChargeBoundary) {
		this.adultNoShowChargeBoundary = adultNoShowChargeBoundary;
	}

	/**
	 * @return the childNoShowChargeBasis
	 */
	public String getChildNoShowChargeBasis() {
		return childNoShowChargeBasis;
	}

	/**
	 * @param childNoShowChargeBasis
	 *            the childNoShowChargeBasis to set
	 */
	public void setChildNoShowChargeBasis(String childNoShowChargeBasis) {
		this.childNoShowChargeBasis = childNoShowChargeBasis;
	}

	/**
	 * @return the childNoShowChargeBreakPoint
	 */
	public BigDecimal getChildNoShowChargeBreakPoint() {
		return childNoShowChargeBreakPoint;
	}

	/**
	 * @param childNoShowChargeBreakPoint
	 *            the childNoShowChargeBreakPoint to set
	 */
	public void setChildNoShowChargeBreakPoint(BigDecimal childNoShowChargeBreakPoint) {
		this.childNoShowChargeBreakPoint = childNoShowChargeBreakPoint;
	}

	/**
	 * @return the childNoShowChargeBoundary
	 */
	public BigDecimal getChildNoShowChargeBoundary() {
		return childNoShowChargeBoundary;
	}

	/**
	 * @param childNoShowChargeBoundary
	 *            the childNoShowChargeBoundary to set
	 */
	public void setChildNoShowChargeBoundary(BigDecimal childNoShowChargeBoundary) {
		this.childNoShowChargeBoundary = childNoShowChargeBoundary;
	}

	/**
	 * @return the infantNoShowChargeBasis
	 */
	public String getInfantNoShowChargeBasis() {
		return infantNoShowChargeBasis;
	}

	/**
	 * @param infantNoShowChargeBasis
	 *            the infantNoShowChargeBasis to set
	 */
	public void setInfantNoShowChargeBasis(String infantNoShowChargeBasis) {
		this.infantNoShowChargeBasis = infantNoShowChargeBasis;
	}

	/**
	 * @return the infantNoShowChargeBreakPoint
	 */
	public BigDecimal getInfantNoShowChargeBreakPoint() {
		return infantNoShowChargeBreakPoint;
	}

	/**
	 * @param infantNoShowChargeBreakPoint
	 *            the infantNoShowChargeBreakPoint to set
	 */
	public void setInfantNoShowChargeBreakPoint(BigDecimal infantNoShowChargeBreakPoint) {
		this.infantNoShowChargeBreakPoint = infantNoShowChargeBreakPoint;
	}

	/**
	 * @return the infantNoShowChargeBoundary
	 */
	public BigDecimal getInfantNoShowChargeBoundary() {
		return infantNoShowChargeBoundary;
	}

	/**
	 * @param infantNoShowChargeBoundary
	 *            the infantNoShowChargeBoundary to set
	 */
	public void setInfantNoShowChargeBoundary(BigDecimal infantNoShowChargeBoundary) {
		this.infantNoShowChargeBoundary = infantNoShowChargeBoundary;
	}

	/**
	 * @return the agentInstructions
	 */
	public String getAgentInstructions() {
		return agentInstructions;
	}

	/**
	 * @param agentInstructions
	 *            the agentInstructions to set
	 */
	public void setAgentInstructions(String agentInstructions) {
		this.agentInstructions = agentInstructions;
	}

	public void setNoonDayFeePolicyEnabled(boolean noonDayFeePolicyEnabled) {
		this.noonDayFeePolicyEnabled = noonDayFeePolicyEnabled;
	}

	public boolean isNoonDayFeePolicyEnabled() {
		return noonDayFeePolicyEnabled;
	}

	public String getAgentCommissionType() {
		return agentCommissionType;
	}

	public void setAgentCommissionType(String agentCommissionType) {
		this.agentCommissionType = agentCommissionType;
	}

	public BigDecimal getAgentCommissionAmount() {
		return agentCommissionAmount;
	}

	public void setAgentCommissionAmount(BigDecimal agentCommissionAmount) {
		this.agentCommissionAmount = agentCommissionAmount;
	}

	public boolean isIgnoreFareValueDecimals() {
		return ignoreFareValueDecimals;
	}

	public void setIgnoreFareValueDecimals(boolean ignoreFareValueDecimals) {
		this.ignoreFareValueDecimals = ignoreFareValueDecimals;
	}

	/**
	 * @return the isModifyToSameFare
	 */
	public boolean isModifyToSameFare() {
		return isModifyToSameFare;
	}

	/**
	 * @param isModifyToSameFare
	 *            the isModifyToSameFare to set
	 */
	public void setModifyToSameFare(boolean isModifyToSameFare) {
		this.isModifyToSameFare = isModifyToSameFare;
	}

	public BigDecimal getNameChangeChargeAmount() {
		return nameChangeChargeAmount;
	}

	public void setNameChangeChargeAmount(BigDecimal nameChangeChargeAmount) {
		this.nameChangeChargeAmount = nameChangeChargeAmount;
	}

	public String getFareRulesCommentsInSelected() {
		return fareRulesCommentsInSelected;
	}

	public void setFareRulesCommentsInSelected(String fareRulesCommentsInSelected) {
		this.fareRulesCommentsInSelected = fareRulesCommentsInSelected;
	}

	public boolean isBulkTicketFareRule() {
		return bulkTicketFareRule;
	}

	public void setBulkTicketFareRule(boolean bulkTicketFareRule) {
		this.bulkTicketFareRule = bulkTicketFareRule;
	}
}
