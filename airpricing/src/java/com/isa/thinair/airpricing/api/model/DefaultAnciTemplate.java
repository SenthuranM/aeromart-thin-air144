package com.isa.thinair.airpricing.api.model;

import java.util.Set;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * 
 * @hibernate.class table = "T_DEFAULT_ANCI_TEMPLATE"
 * 
 */


public class DefaultAnciTemplate extends Tracking {

	private static final long serialVersionUID = -6019636872193742142L;

	private int defaultAnciTemplateId;

	private String ancillaryType;

	private int anciTemplateId;

	private String status;

	private String airCraftModelnumber;

	private Set<DefaultAnciTemplateRoute> assignedONDs;
	
	
	public static final String STATUS_ACTIVE = "ACT";
	public static final String STATUS_INACTIVE = "INA";
		
	public enum ANCI_TEMPLATES {
		MEAL, SEAT, BAGGAGE;
	}
	
	public int hashCode() {
		return new HashCodeBuilder().append(getDefaultAnciTemplateId()).toHashCode();
	}

	/**
	 * @return the defaultAnciTemplateId
	 * @hibernate.id column = "DEFAULT_ANCI_TEMPLATE_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_DEFAULT_ANCI_TEMPLATE"
	 */
	public int getDefaultAnciTemplateId() {
		return defaultAnciTemplateId;
	}

	/**
	 * @param defaultAnciTemplateId
	 *            the defaultAnciTemplateId to set
	 */
	public void setDefaultAnciTemplateId(int defaultAnciTemplateId) {
		this.defaultAnciTemplateId = defaultAnciTemplateId;
	}

	/**
	 * @return the ancillaryType
	 * @hibernate.property column = "ANCI_TYPE"
	 */
	public String getAncillaryType() {
		return ancillaryType;
	}

	/**
	 * @param ancillaryType
	 *            the ancillaryType to set
	 */
	public void setAncillaryType(String ancillaryType) {
		this.ancillaryType = ancillaryType;
	}

	/**
	 * @return the anciTemplateId
	 * @hibernate.property column = "ANCI_TEMPLATE_ID"
	 */
	public int getAnciTemplateId() {
		return anciTemplateId;
	}

	/**
	 * @param anciTemplateId
	 *            the anciTemplateId to set
	 */
	public void setAnciTemplateId(int anciTemplateId) {
		this.anciTemplateId = anciTemplateId;
	}

	/**
	 * @return the status
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the airCraftModelnumber
	 * @hibernate.property column = "MODEL_NUMBER"
	 */
	public String getAirCraftModelnumber() {
		return airCraftModelnumber;
	}

	/**
	 * @param airCraftModelnumber
	 *            the airCraftModelnumber to set
	 */
	public void setAirCraftModelnumber(String airCraftModelnumber) {
		this.airCraftModelnumber = airCraftModelnumber;
	}

	/**
	 * 
	 * 
	 * @hibernate.set lazy="false" cascade="all" order-by="OND_CODE" inverse="true"
	 * @hibernate.collection-key column="DEFAULT_ANCI_TEMPLATE_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.airpricing.api.model.DefaultAnciTemplateRoute"
	 * 
	 * @return the assignedONDs
	 */
	public Set<DefaultAnciTemplateRoute> getAssignedONDs() {
		return assignedONDs;
	}

	/**
	 * @param assignedONDs
	 *            the assignedONDs to set
	 */
	public void setAssignedONDs(Set<DefaultAnciTemplateRoute> assignedRoutes) {
		this.assignedONDs = assignedRoutes;
	}
}
