package com.isa.thinair.airpricing.api.model;

import java.io.Serializable;

/**
 * 
 * @hibernate.class table = "T_SSR_CHARGE_COS"
 * 
 */
public class SSRChargesCOS implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8850616511647064490L;

	private Integer ssrChargeCOSId;

	private SSRCharge ssrCharge;

	private String cabinClassCode;

	private String logicalCCCode;

	/**
	 * @return the ssrChargeCOSId
	 * 
	 * @hibernate.id column="SSR_CHARGE_COS_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_SSR_CHARGE_COS"
	 */
	public Integer getSsrChargeCOSId() {
		return ssrChargeCOSId;
	}

	/**
	 * @param ssrChargeCOSId
	 *            the ssrChargeCOSId to set
	 */
	public void setSsrChargeCOSId(Integer ssrChargeCOSId) {
		this.ssrChargeCOSId = ssrChargeCOSId;
	}

	/**
	 * @return the cabinClassCode
	 * 
	 * @hibernate.property column = "CABIN_CLASS_CODE"
	 */
	public String getCabinClassCode() {
		return cabinClassCode;
	}

	/**
	 * @param cabinClassCode
	 *            the cabinClassCode to set
	 */
	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	/**
	 * @return the logicalCCCode
	 * 
	 * @hibernate.property column = "LOGICAL_CABIN_CLASS_CODE"
	 */
	public String getLogicalCCCode() {
		return logicalCCCode;
	}

	/**
	 * @param logicalCCCode
	 *            the logicalCCCode to set
	 */
	public void setLogicalCCCode(String logicalCCCode) {
		this.logicalCCCode = logicalCCCode;
	}

	/**
	 * @return the ssrCharge
	 * @hibernate.many-to-one column="SSR_CHARGE_ID" class="com.isa.thinair.airpricing.api.model.SSRCharge"
	 */
	public SSRCharge getSsrCharge() {
		return ssrCharge;
	}

	/**
	 * @param ssrCharge
	 *            the ssrCharge to set
	 */
	public void setSsrCharge(SSRCharge ssrCharge) {
		this.ssrCharge = ssrCharge;
	}
}