package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class PaxSSRChargeDTO implements Serializable {

	private static final long serialVersionUID = -1867021564461422417L;
	private Integer sSRChargeId;
	private String ssrCode;
	private String oNDCode;
	private BigDecimal amount;

	public Integer getSSRChargeId() {
		return sSRChargeId;
	}

	public void setSSRChargeId(Integer ssrChargeId) {
		this.sSRChargeId = ssrChargeId;
	}

	public String getSsrCode() {
		return ssrCode;
	}

	public void setSsrCode(String ssrCode) {
		this.ssrCode = ssrCode;
	}

	public String getONDCode() {
		return oNDCode;
	}

	public void setONDCode(String code) {
		oNDCode = code;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

}
