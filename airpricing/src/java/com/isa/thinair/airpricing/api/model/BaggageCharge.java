/**
 * 	mano
	May 9, 2011 
	2011
 */
package com.isa.thinair.airpricing.api.model;

import java.math.BigDecimal;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * @author mano
 * @hibernate.class table = "BG_T_BAGGAGE_CHARGE"
 */
public class BaggageCharge extends Persistent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8718956196375999768L;
	
	private Integer chargeId;

	private BaggageTemplate template;

	private Integer baggageId;

	private BigDecimal amount;

	private Integer allocatedPieces;

	private String status;

	private String baggageName;

	private String cabinClass;

	private String logicalCCCode;

	private String defaultBaggageFlag;
	
	private BigDecimal localCurrencyAmount;
	
	/**
	 * @hibernate.id column = "BAGGAGE_CHARGE_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "BG_S_BAGGAGE_CHARGE"
	 * 
	 * @return Returns the chargeId.
	 */
	public Integer getChargeId() {
		return chargeId;
	}

	/**
	 * @param chargeId
	 *            the chargeId to set
	 */
	public void setChargeId(Integer chargeId) {
		this.chargeId = chargeId;
	}

	/**
	 * @return Returns the templateCode.
	 * @hibernate.many-to-one column = "BAGGAGE_TEMPLATE_ID" class=
	 *                        "com.isa.thinair.airpricing.api.model.BaggageTemplate"
	 */
	public BaggageTemplate getTemplate() {
		return template;
	}

	/**
	 * @param template
	 *            the template to set
	 */
	public void setTemplate(BaggageTemplate template) {
		this.template = template;
	}

	/**
	 * @return the baggageId
	 * @hibernate.property column = "BAGGAGE_ID"
	 */
	public Integer getBaggageId() {
		return baggageId;
	}

	/**
	 * @param baggageId
	 *            the baggageId to set
	 */
	public void setBaggageId(Integer baggageId) {
		this.baggageId = baggageId;
	}

	/**
	 * @return the amount
	 * @hibernate.property column = "AMOUNT"
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount
	 *            the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return the allocatedPieces
	 * @hibernate.property column = "ALLOCATED_PIECES"
	 */
	public Integer getAllocatedPieces() {
		return allocatedPieces;
	}

	/**
	 * @param allocatedPieces
	 *            the allocatedPieces to set
	 */
	public void setAllocatedPieces(Integer allocatedPieces) {
		this.allocatedPieces = allocatedPieces;
	}

	/**
	 * @return the status
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the baggageName
	 */
	public String getBaggageName() {
		return baggageName;
	}

	/**
	 * @param baggageName
	 *            the baggageName to set
	 */
	public void setBaggageName(String baggageName) {
		this.baggageName = baggageName;
	}

	/**
	 * @return the cabinClass
	 * @hibernate.property column = "CABIN_CLASS_CODE"
	 */
	public String getCabinClass() {
		return cabinClass;
	}

	/**
	 * @param cabinClass
	 *            the cabinClass to set
	 */
	public void setCabinClass(String cabinClass) {
		this.cabinClass = cabinClass;
	}

	/**
	 * @return the defaultBaggageFlag
	 * @hibernate.property column = "IS_DEFAULT"
	 */
	public String getDefaultBaggageFlag() {
		return defaultBaggageFlag;
	}

	/**
	 * @param defaultBaggageFlag
	 *            to set default baggage for the template
	 */
	public void setDefaultBaggageFlag(String defaultBaggageFlag) {
		this.defaultBaggageFlag = defaultBaggageFlag;
	}

	/***
	 * 
	 * @return
	 * @hibernate.property column = "LOGICAL_CABIN_CLASS_CODE"
	 */
	public String getLogicalCCCode() {
		return logicalCCCode;
	}

	public void setLogicalCCCode(String logicalCCCode) {
		this.logicalCCCode = logicalCCCode;
	}
	
	/**
	 * @return the localCurrencyAmount
	 * @hibernate.property column = "LOCAL_CURR_AMOUNT"
	 */
	public BigDecimal getLocalCurrencyAmount() {
		return localCurrencyAmount;
	}

	public void setLocalCurrencyAmount(BigDecimal localCurrencyAmount) {
		this.localCurrencyAmount = localCurrencyAmount;
	}

}
