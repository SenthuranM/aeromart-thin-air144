package com.isa.thinair.airpricing.api.model;

import java.io.Serializable;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * @hibernate.class table = "T_PAYMENT_GATEWAY_WISE_CHARGES"
 */
public class PaymentGatewayWiseCharges extends Tracking implements Serializable {
	
	private static final long serialVersionUID = -6790538999298260934L;

	private Integer chargeId;
	private int paymentGatewayId;
	private Date chargeEffectiveFromDate;
	private Date chargeEffectiveToDate;
	private String valuePercentageFlag;
	private float chargeValuePercentage;
	private float valueInDefaultCurrency;
	private String status;
	private Date createdDate;
	private PaymentGatewayWiseCharges oldPaymentGatewayWiseCharges;
	
	/**
	 * @return the paymentGatewayId 
	 * @hibernate.property column = "PAYMENT_GATEWAY_ID"
	 */
	public int getPaymentGatewayId() {
		return paymentGatewayId;
	}
	/**
	 * @param paymentGatewayId the paymentGatewayId to set
	 */
	public void setPaymentGatewayId(int paymentGatewayId) {
		this.paymentGatewayId = paymentGatewayId;
	}
	/**
	 * @return the chargeEffectiveFromDate
	 * @hibernate.property column = "CHARGE_EFFECTIVE_FROM_DATE"
	 */
	public Date getChargeEffectiveFromDate() {
		return chargeEffectiveFromDate;
	}
	/**
	 * @param chargeEffectiveFromDate the chargeEffectiveFromDate to set
	 */
	public void setChargeEffectiveFromDate(Date chargeEffectiveFromDate) {
		this.chargeEffectiveFromDate = chargeEffectiveFromDate;
	}
	/**
	 * @return the chargeEffectiveToDate
	 * @hibernate.property column = "CHARGE_EFFECTIVE_TO_DATE"
	 */
	public Date getChargeEffectiveToDate() {
		return chargeEffectiveToDate;
	}
	/**
	 * @param chargeEffectiveToDate the chargeEffectiveToDate to set
	 */
	public void setChargeEffectiveToDate(Date chargeEffectiveToDate) {
		this.chargeEffectiveToDate = chargeEffectiveToDate;
	}
	/**
	 * @return the valuePercentageFlag
	 * @hibernate.property column = "VALUE_PERCENTAGE_FLAG"
	 */
	public String getValuePercentageFlag() {
		return valuePercentageFlag;
	}
	/**
	 * @param valuePercentageFlag the valuePercentageFlag to set
	 */
	public void setValuePercentageFlag(String valuePercentageFlag) {
		this.valuePercentageFlag = valuePercentageFlag;
	}
	/**
	 * @return the chargeValuePercentage
	 * @hibernate.property column = "CHARGE_VALUE_PERCENTAGE"
	 */
	public float getChargeValuePercentage() {
		return chargeValuePercentage;
	}
	/**
	 * @param chargeValuePercentage the chargeValuePercentage to set
	 */
	public void setChargeValuePercentage(float chargeValuePercentage) {
		this.chargeValuePercentage = chargeValuePercentage;
	}
	/**
	 * @return the valueInDefaultCurrency
	 * @hibernate.property column = "VALUE_IN_DEFAULT_CURRENCY"
	 */
	public float getValueInDefaultCurrency() {
		return valueInDefaultCurrency;
	}
	/**
	 * @param valueInDefaultCurrency the valueInDefaultCurrency to set
	 */
	public void setValueInDefaultCurrency(float valueInDefaultCurrency) {
		this.valueInDefaultCurrency = valueInDefaultCurrency;
	}
	/**
	 * @return the status
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the createdDate
	 * @hibernate.property column = "CREATED_DATE"
	 */
	public Date getCreatedDate() {
		return createdDate;
	}
	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	/**
	 * @return the chargeId
	 * @hibernate.id column = "PAYMENT_GATEWAY_WISE_CHARGE_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_PAYMENT_GATEWAY_WISE_CHARGES"
	 */
	public Integer getChargeId() {
		return chargeId;
	}
	/**
	 * @param chargeId the chargeId to set
	 */
	public void setChargeId(Integer chargeId) {
		this.chargeId = chargeId;
	}
	public PaymentGatewayWiseCharges getOldPaymentGatewayWiseCharges() {
		return oldPaymentGatewayWiseCharges;
	}
	public void setOldPaymentGatewayWiseCharges(PaymentGatewayWiseCharges oldPaymentGatewayWiseCharges) {
		this.oldPaymentGatewayWiseCharges = oldPaymentGatewayWiseCharges;
	}
	
}
