package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class TransitInfoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<Long> transitDurationList;

	private Set<String> transitAirports;

	private Map<Date, String> airportMap;

	private Map<Date, Long> durationMap;

	private Map<Date, Boolean> shortTransitQualMap;

	private boolean qualifyForShortTransit;

	public List<Long> getTransitDurationList() {
		return transitDurationList;
	}

	public List<Long> getTransitDurationList(Date departureDateZulu) {

		List<Long> transitList = null;
		if (durationMap != null && durationMap.containsKey(departureDateZulu)) {
			transitList = new ArrayList<Long>();
			transitList.add(durationMap.get(departureDateZulu));
		}
		return transitList;
	}

	public Set<String> getTransitAirports() {
		return transitAirports;
	}

	public Set<String> getTransitAirports(Date departureDateZulu) {

		Set<String> airportSet = null;
		if (airportMap != null && airportMap.containsKey(departureDateZulu)) {
			airportSet = new HashSet<String>();
			airportSet.add(airportMap.get(departureDateZulu));
		}
		return airportSet;
	}

	public Set<String> getDepArrExcludeAirports(String fromAP, String toAP) {
		Set<String> airportSet = new HashSet<String>();
		if (transitAirports != null && transitAirports.contains(fromAP)) {
			airportSet.add(fromAP);
		}
		if (transitAirports != null && transitAirports.contains(toAP)) {
			airportSet.add(toAP);
		}
		return airportSet;
	}

	public void addTransit(Date departureDate, String transitAirportCode, Long duration) {
		if (transitDurationList == null) {
			transitDurationList = new ArrayList<Long>();
			transitAirports = new HashSet<String>();
			airportMap = new HashMap<Date, String>();
			durationMap = new HashMap<Date, Long>();
		}
		transitDurationList.add(duration);
		transitAirports.add(transitAirportCode);
		airportMap.put(departureDate, transitAirportCode);
		durationMap.put(departureDate, duration);
	}

	public void setQualifyForShortTransit(Date departureDate, boolean qualifyForShortTransit) {
		if (shortTransitQualMap == null) {
			shortTransitQualMap = new HashMap<Date, Boolean>();
		}
		this.qualifyForShortTransit = (qualifyForShortTransit || this.qualifyForShortTransit);
		this.shortTransitQualMap.put(departureDate, qualifyForShortTransit);
	}

	public boolean isQualifyForShortTransit(Date departureDate) {
		if (shortTransitQualMap != null && shortTransitQualMap.containsKey(departureDate)) {
			return shortTransitQualMap.get(departureDate);
		}
		return false;
	}

	public boolean isQualifyForShortTransit() {
		return qualifyForShortTransit;
	}
}
