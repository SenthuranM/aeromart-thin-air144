package com.isa.thinair.airpricing.api.model;

import java.io.Serializable;

import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * @hibernate.class table = "BG_T_OND_TEMPLATE_BC"
 */
public class BaggageTemplateBookingClass implements Serializable {

	private Integer id;
	private ONDBaggageTemplate baggageTemplate;
	private String bookingClass;

	/**
	 * @hibernate.id column = "OND_TEMPLATE_BC_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "BG_S_OND_TEMPLATE_BC"
	 */
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @hibernate.many-to-one column="OND_CHARGE_TEMPLATE_ID" class="com.isa.thinair.airpricing.api.model.ONDBaggageTemplate"
	 */
	public ONDBaggageTemplate getBaggageTemplate() {
		return baggageTemplate;
	}

	public void setBaggageTemplate(ONDBaggageTemplate baggageTemplate) {
		this.baggageTemplate = baggageTemplate;
	}

	/**
 	 * @hibernate.property column = "BOOKING_CODE"
	 */
	public String getBookingClass() {
		return bookingClass;
	}

	public void setBookingClass(String bookingClass) {
		this.bookingClass = bookingClass;
	}

	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		BaggageTemplateBookingClass that = (BaggageTemplateBookingClass) o;

		if (bookingClass != null ? !bookingClass.equals(that.bookingClass) : that.bookingClass != null) return false;

		return true;
	}

	public int hashCode() {
		return new HashCodeBuilder().append(getBookingClass()).toHashCode();
	}
}
