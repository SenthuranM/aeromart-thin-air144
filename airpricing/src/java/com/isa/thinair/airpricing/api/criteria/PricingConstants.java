package com.isa.thinair.airpricing.api.criteria;

public interface PricingConstants {

	public static interface ChargeGroups {
		public static String CANCELLATION_CHARGE = "CNX";
		public static String MODIFICATION_CHARGE = "MOD";
		public static String THIRDPARTY_SURCHARGES = "ESU";
		public static String INFANT_SURCHARGE = "INF";
		public static String ADJUSTMENT_CHARGE = "ADJ";
		public static String SURCHARGE = "SUR";
		public static String TAX = "TAX";
		public static String FARE = "FAR";
		public static String DISCOUNT = "DIS";
	}
}
