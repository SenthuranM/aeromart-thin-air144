/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:16:12
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;

/**
 * @author Byorn
 * 
 */
public class FareQuoteDTO implements Serializable {

	private static final long serialVersionUID = -6492822699582096155L;
	private String bookingCode;
	private int fareId;
	private double fareAmount;

	public String getBookingCode() {
		return bookingCode;
	}

	public void setBookingCode(String bookingCode) {
		this.bookingCode = bookingCode;
	}

	public double getFareAmount() {
		return fareAmount;
	}

	public void setFareAmount(double fareAmount) {
		this.fareAmount = fareAmount;
	}

	public int getFareId() {
		return fareId;
	}

	public void setFareId(int fareId) {
		this.fareId = fareId;
	}

}
