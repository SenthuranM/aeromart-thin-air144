package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;
import java.util.Date;

public class CCChargeTO implements Serializable {

	private static final long serialVersionUID = 8832608595813329211L;

	private int creditCardWiseChargeId;
	private int pgwId;
	private Date effectiveFromDate;
	private Date effectiveToDate;
	private String valuePercentageFlag;
	private float chargeValuePercentage;
	private float valueInDefaultCurrency;
	private String status;
	private String pgwName;
	private String pgwDescription;

	public int getCreditCardWiseChargeId() {
		return creditCardWiseChargeId;
	}

	public void setCreditCardWiseChargeId(int creditCardWiseChargeId) {
		this.creditCardWiseChargeId = creditCardWiseChargeId;
	}

	public int getPgwId() {
		return pgwId;
	}

	public void setPgwId(int pgwId) {
		this.pgwId = pgwId;
	}

	public Date getEffectiveFromDate() {
		return effectiveFromDate;
	}

	public void setEffectiveFromDate(Date effectiveFromDate) {
		this.effectiveFromDate = effectiveFromDate;
	}

	public Date getEffectiveToDate() {
		return effectiveToDate;
	}

	public void setEffectiveToDate(Date effectiveToDate) {
		this.effectiveToDate = effectiveToDate;
	}

	public String getValuePercentageFlag() {
		return valuePercentageFlag;
	}

	public void setValuePercentageFlag(String valuePercentageFlag) {
		this.valuePercentageFlag = valuePercentageFlag;
	}

	public float getChargeValuePercentage() {
		return chargeValuePercentage;
	}

	public void setChargeValuePercentage(float chargeValuePercentage) {
		this.chargeValuePercentage = chargeValuePercentage;
	}

	public float getValueInDefaultCurrency() {
		return valueInDefaultCurrency;
	}

	public void setValueInDefaultCurrency(float valueInDefaultCurrency) {
		this.valueInDefaultCurrency = valueInDefaultCurrency;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPgwName() {
		return pgwName;
	}

	public void setPgwName(String pgwName) {
		this.pgwName = pgwName;
	}

	public String getPgwDescription() {
		return pgwDescription;
	}

	public void setPgwDescription(String pgwDescription) {
		this.pgwDescription = pgwDescription;
	}

}
