package com.isa.thinair.airpricing.api.util;

/**
* @author subash 
* 
*/

public class SplitUtil {
	public static String[] getSplittedCabinClassAndLogicalCabinClass(String cabinClassAndLogicalCabinClass) {
		String[] returnCabinClassAndLogicalCabinClass = new String[2];
		String[] splittedCabinClassAndLogicalCabinClass = cabinClassAndLogicalCabinClass
				.split(DelimeterConstants.CABIN_CLASS_LOGICAL_CABIN_CLASS_SEPERATOR);
		if (splittedCabinClassAndLogicalCabinClass[0].trim().equalsIgnoreCase(splittedCabinClassAndLogicalCabinClass[1].trim())) {
			returnCabinClassAndLogicalCabinClass[0] = splittedCabinClassAndLogicalCabinClass[0].trim();
		} else {
			returnCabinClassAndLogicalCabinClass[1] = splittedCabinClassAndLogicalCabinClass[0].trim().split(
					"\\" + DelimeterConstants.LOGICAL_CABIN_CLASS_APPENDER)[0];
		}
		return returnCabinClassAndLogicalCabinClass;
	}
	
}
