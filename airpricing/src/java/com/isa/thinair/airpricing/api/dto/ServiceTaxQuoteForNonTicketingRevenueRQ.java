package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;
import java.util.List;

public class ServiceTaxQuoteForNonTicketingRevenueRQ implements Serializable {
	
	private static final long serialVersionUID = -872762184195676821L;
	
	private List<PaxCnxModPenChargeDTO> paxCnxModPenCharges;
	
	private String paxState;
	
	private String paxCountryCode;
	
	private String modifyingAgentStation;
	
	private boolean paxTaxRegistered;
	
	private int channelCode = -1;

	public List<PaxCnxModPenChargeDTO> getPaxCnxModPenCharges() {
		return paxCnxModPenCharges;
	}

	public String getPaxState() {
		return paxState;
	}

	public boolean isPaxTaxRegistered() {
		return paxTaxRegistered;
	}

	public void setPaxCnxModPenCharges(List<PaxCnxModPenChargeDTO> paxCnxModPenCharges) {
		this.paxCnxModPenCharges = paxCnxModPenCharges;
	}

	public void setPaxState(String paxState) {
		this.paxState = paxState;
	}

	public void setPaxTaxRegistered(boolean paxTaxRegistered) {
		this.paxTaxRegistered = paxTaxRegistered;
	}

	public String getPaxCountryCode() {
		return paxCountryCode;
	}

	public void setPaxCountryCode(String paxCountryCode) {
		this.paxCountryCode = paxCountryCode;
	}

	public int getChannelCode() {
		return channelCode;
	}

	public void setChannelCode(int channelCode) {
		this.channelCode = channelCode;
	}

	public String getModifyingAgentStation() {
		return modifyingAgentStation;
	}

	public void setModifyingAgentStation(String modifyingAgentStation) {
		this.modifyingAgentStation = modifyingAgentStation;
	}	
}
