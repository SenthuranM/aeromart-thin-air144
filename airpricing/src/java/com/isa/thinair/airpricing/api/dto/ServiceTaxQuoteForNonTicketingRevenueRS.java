package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;
import java.util.Map;

public class ServiceTaxQuoteForNonTicketingRevenueRS extends ServiceTaxQuoteRS implements Serializable {

	private static final long serialVersionUID = -872772184195676821L;

	private Map<Integer, CnxModPenServiceTaxDTO> paxWiseCnxModPenServiceTaxes;

	public Map<Integer, CnxModPenServiceTaxDTO> getPaxWiseCnxModPenServiceTaxes() {
		return paxWiseCnxModPenServiceTaxes;
	}

	public void setPaxWiseCnxModPenServiceTaxes(Map<Integer, CnxModPenServiceTaxDTO> paxWiseCnxModPenServiceTaxes) {
		this.paxWiseCnxModPenServiceTaxes = paxWiseCnxModPenServiceTaxes;
	}
}
