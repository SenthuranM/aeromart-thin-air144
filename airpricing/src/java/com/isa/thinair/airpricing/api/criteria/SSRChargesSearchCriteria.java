package com.isa.thinair.airpricing.api.criteria;

import java.io.Serializable;
import java.util.Collection;

/**
 * @author M.Rikaz
 * 
 */
public class SSRChargesSearchCriteria implements Serializable {

	private static final long serialVersionUID = 8377494969335238721L;

	private String chargeCode;

	private String ssrCode;

	private String category;

	private String status;

	private Collection<String> adminVisibleSSRId;

	public Collection<String> getAdminVisibleSSRId() {
		return adminVisibleSSRId;
	}

	public void setAdminVisibleSSRId(Collection<String> adminVisibleSSRId) {
		this.adminVisibleSSRId = adminVisibleSSRId;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	public String getChargeCode() {
		return chargeCode;
	}

	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

	public String getSsrCode() {
		return ssrCode;
	}

	public void setSsrCode(String ssrCode) {
		this.ssrCode = ssrCode;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

}
