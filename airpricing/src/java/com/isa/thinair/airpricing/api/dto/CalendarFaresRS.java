package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalendarFaresRS implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Map<Integer, List<LightFareDTO>> ondFareMap = null;

	public void addOndFares(Integer ondSequence, List<LightFareDTO> fares) {
		if (fares != null) {
			getFaresForOnd(ondSequence).addAll(fares);
		}
	}

	public Map<Integer, List<LightFareDTO>> getOndFareMap() {
		if (ondFareMap == null) {
			ondFareMap = new HashMap<Integer, List<LightFareDTO>>();
		}
		return ondFareMap;
	}

	public List<LightFareDTO> getFaresForOnd(Integer ondSequence) {
		if (getOndFareMap().get(ondSequence) == null) {
			getOndFareMap().put(ondSequence, new ArrayList<LightFareDTO>());
		}
		return getOndFareMap().get(ondSequence);
	}
}
