package com.isa.thinair.airpricing.api.model;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Type-safe enumeration.
 */
@SuppressWarnings("rawtypes")
public final class Status implements Comparable {

	public static final Status active = new Status("ACTIVE");
	public static final Status inactive = new Status("INACTIVE");

	public String toString() {
		return fName;
	}

	public int compareTo(Object aObject) {
		return fOrdinal - ((Status) aObject).fOrdinal;
	}

	/**
	 * Convert a String into its corresponding RockBand object, if possible.
	 * 
	 * @param aText
	 *            possibly-null text which may map to a RockBand.
	 * @return null if aText is null, else try to match to a known RockBand.
	 * @exception IllegalArgumentException
	 *                if it cannot be matched to a known enumeration element.
	 */
	public static Status valueOf(String aText) {
		if (aText == null)
			return null;
		Iterator valuesIter = VALUES.iterator();
		Status item = null;
		while (valuesIter.hasNext()) {
			item = (Status) valuesIter.next();
			if (aText.equals(item.toString())) {
				return item;
			}
		}
		throw new IllegalArgumentException("Cannot parse into Status object:" + aText);
	}

	// / PRIVATE ///
	private final String fName;

	private static int fNextOrdinal = 1;
	private final int fOrdinal = fNextOrdinal++;

	private Status(String aName) {
		fName = aName;
	}

	private static final Status[] fValues = { active, inactive };
	/**
	 * Allows user to iterate over all elements of the enumeration.
	 */
	public static final List VALUES = Collections.unmodifiableList(Arrays.asList(fValues));

}
