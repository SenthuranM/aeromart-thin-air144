package com.isa.thinair.airpricing.api.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airpricing.api.dto.BaggageTemplateTo;
import com.isa.thinair.airpricing.api.model.BaggageTemplateAgent;
import com.isa.thinair.airpricing.api.model.BaggageTemplateBookingClass;
import com.isa.thinair.airpricing.api.model.BaggageTemplateFlight;
import com.isa.thinair.airpricing.api.model.BaggageTemplateSalesChannel;
import com.isa.thinair.airpricing.api.model.ONDBaggageChargeTemplate;
import com.isa.thinair.airpricing.api.model.ONDBaggageTemplate;

public class BaggageUtil {
	private BaggageUtil() {
	}

	public static void mergeToBaggageTemplate(BaggageTemplateTo baggageTemplateTo,
	                                          ONDBaggageTemplate baggageTemplate) {

		ONDBaggageChargeTemplate ondTemplate;
		List<ONDBaggageChargeTemplate> ondsToRemove = new ArrayList<ONDBaggageChargeTemplate>();
		List<BaggageTemplateFlight> flightsToRemove = new ArrayList<BaggageTemplateFlight>();
		List<BaggageTemplateAgent> agentsToRemove = new ArrayList<BaggageTemplateAgent>();
		List<BaggageTemplateBookingClass> bookingClassesToRemove = new ArrayList<BaggageTemplateBookingClass>();
		List<BaggageTemplateSalesChannel> salesChannelsToRemove = new ArrayList<BaggageTemplateSalesChannel>();
		
		BaggageTemplateFlight baggageTemplateFlight;
		BaggageTemplateBookingClass baggageTemplateBookingClass;
		BaggageTemplateAgent baggageTemplateAgent;

		// prepare onds
		Set<ONDBaggageChargeTemplate> onds = baggageTemplate.getOnds();
		if (onds == null) {
			onds = new HashSet<ONDBaggageChargeTemplate>();
			baggageTemplate.setOnds(onds);
		}

		if (baggageTemplateTo.getOnds() == null) {
			baggageTemplateTo.setOnds(new ArrayList<String>());
		}

		Collection<String> newOnds = baggageTemplateTo.getOnds();
		for (ONDBaggageChargeTemplate tempOndTemplate : onds) {
			if (!newOnds.contains(tempOndTemplate.getOndCode())) {
				ondsToRemove.add(tempOndTemplate);
			}
		}
		onds.removeAll(ondsToRemove);

		for (String ondCode : newOnds) {
			ondTemplate = new ONDBaggageChargeTemplate();
			ondTemplate.setOndCode(ondCode);
			ondTemplate.setOndBaggageTemplate(baggageTemplate);

			if (!onds.contains(ondTemplate)) {
				onds.add(ondTemplate);
			}
		}



		// prepare flights
		Set<BaggageTemplateFlight> flights = baggageTemplate.getFlights();
		if (flights == null) {
			flights = new HashSet<BaggageTemplateFlight>();
			baggageTemplate.setFlights(flights);
		}

		if (baggageTemplateTo.getFlights() == null) {
			baggageTemplateTo.setFlights(new HashMap<String, String>());
		}

		Set<String> newFlights = baggageTemplateTo.getFlights().keySet();
		for (BaggageTemplateFlight tempFlight : flights) {
			if (!newFlights.contains(tempFlight.getFlightNumber())) {
				flightsToRemove.add(tempFlight);
			}
		}
		flights.removeAll(flightsToRemove);

		for (String flight : newFlights) {
			baggageTemplateFlight = new BaggageTemplateFlight();
			baggageTemplateFlight.setBaggageTemplate(baggageTemplate);
			baggageTemplateFlight.setFlightNumber(flight);
			if (!flights.contains(flight)) {
				flights.add(baggageTemplateFlight);
			}
		}



		// prepare agents
		Set<BaggageTemplateAgent> agents = baggageTemplate.getAgents();
		if (agents == null) {
			agents = new HashSet<BaggageTemplateAgent>();
			baggageTemplate.setAgents(agents);
		}

		if (baggageTemplateTo.getAgents() == null) {
			baggageTemplateTo.setAgents(new HashMap<String, String>());
		}
		Set<String> newAgents = baggageTemplateTo.getAgents().keySet();

		for (BaggageTemplateAgent tempAgent : agents) {
			if (!newAgents.contains(tempAgent.getAgentCode())) {
				agentsToRemove.add(tempAgent);
			}
		}
		agents.removeAll(agentsToRemove);

		for (String agent : newAgents) {
			baggageTemplateAgent = new BaggageTemplateAgent();
			baggageTemplateAgent.setBaggageTemplate(baggageTemplate);
			baggageTemplateAgent.setAgentCode(agent);
			if (!agents.contains(agent)) {
				agents.add(baggageTemplateAgent);
			}
		}

		// prepare booking classes
		Set<BaggageTemplateBookingClass> bookingClasses = baggageTemplate.getBookingClasses();
		if (bookingClasses == null) {
			bookingClasses = new HashSet<BaggageTemplateBookingClass>();
			baggageTemplate.setBookingClasses(bookingClasses);
		}

		if (baggageTemplateTo.getBookingClasses() == null) {
			baggageTemplateTo.setBookingClasses(new HashMap<String, String>());
		}
		Set<String> newBookingClasses = baggageTemplateTo.getBookingClasses().keySet();

		for (BaggageTemplateBookingClass tempBookingClasses : bookingClasses) {
			if (!newBookingClasses.contains(tempBookingClasses.getBookingClass())) {
				bookingClassesToRemove.add(tempBookingClasses);
			}
		}
		bookingClasses.removeAll(bookingClassesToRemove);

		for (String bookingClass : newBookingClasses) {
			baggageTemplateBookingClass = new BaggageTemplateBookingClass();
			baggageTemplateBookingClass.setBaggageTemplate(baggageTemplate);
			baggageTemplateBookingClass.setBookingClass(bookingClass);
			if (!bookingClasses.contains(bookingClass)) {
				bookingClasses.add(baggageTemplateBookingClass);
			}
		}
		
		//prepare sales channels
		Set<BaggageTemplateSalesChannel> salesChannels=baggageTemplate.getSalesChannels();
		
		if(salesChannels == null){
			salesChannels = new HashSet<BaggageTemplateSalesChannel>();
			baggageTemplate.setSalesChannels(salesChannels);
		}
		
		if(baggageTemplateTo.getSalesChannels() == null){
			baggageTemplateTo.setSalesChannels(new HashMap<String,String>());
		}
		Set<String> newSalesChannels = baggageTemplateTo.getSalesChannels().keySet();
		
		for(BaggageTemplateSalesChannel tempSalesChannel: salesChannels){
			if(!newSalesChannels.contains(tempSalesChannel.getSalesChannelCode())){
				salesChannelsToRemove.add(tempSalesChannel);
			}
		}
		salesChannels.removeAll(salesChannelsToRemove);
		
		for(String salesChannel:newSalesChannels){
			if(!salesChannels.contains(salesChannel)){
				BaggageTemplateSalesChannel baggageTemplateSalesChannel=new BaggageTemplateSalesChannel();
				baggageTemplateSalesChannel.setBaggageTemplate(baggageTemplate);
				baggageTemplateSalesChannel.setSalesChannelCode(salesChannel);
				salesChannels.add(baggageTemplateSalesChannel);
			}
		}

	}


	public static BaggageTemplateTo toBaggageTemplateTo(ONDBaggageTemplate baggageTemplate) {
		BaggageTemplateTo to = new BaggageTemplateTo();

		to.setTemplateId(baggageTemplate.getTemplateId());
		to.setTemplateCode(baggageTemplate.getTemplateCode());
		to.setDescription(baggageTemplate.getDescription());
		to.setStatus(baggageTemplate.getStatus());
		to.setFromDate(baggageTemplate.getFromDate());
		to.setToDate(baggageTemplate.getToDate());

		List<String> onds = new ArrayList<String>();
		for (ONDBaggageChargeTemplate ond : baggageTemplate.getOnds()) {
			onds.add(ond.getOndCode());
		}
		to.setOnds(onds);

		Map<String, String> flights = new HashMap<String, String>();
		for (BaggageTemplateFlight flight : baggageTemplate.getFlights()) {
			flights.put(flight.getFlightNumber(), null);
		}
		to.setFlights(flights);

		Map<String, String> agents = new HashMap<String, String>();
		for (BaggageTemplateAgent agent : baggageTemplate.getAgents()) {
			agents.put(agent.getAgentCode(), null);
		}
		to.setAgents(agents);

		Map<String, String> bookingClasses = new HashMap<String, String>();
		for (BaggageTemplateBookingClass bookingClass : baggageTemplate.getBookingClasses()) {
			bookingClasses.put(bookingClass.getBookingClass(), null);
		}
		to.setBookingClasses(bookingClasses);

		Map<String,String> salesChannels=new HashMap<String,String>();
		for(BaggageTemplateSalesChannel salesChannel:baggageTemplate.getSalesChannels()){
			salesChannels.put(salesChannel.getSalesChannelCode(), null);
		}
		to.setSalesChannels(salesChannels);

		return to;
	}
}
