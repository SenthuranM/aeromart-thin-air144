package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;

public class LightFareRuleDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private long fareRuleId;
	private Integer advanceBookingDays;
	private WeekFrequency departureWeekFrequency;
	private HourValidity departureHourValidity;
	private Long loadFactorMin;
	private Long loadFactorMax;

	public long getFareRuleId() {
		return fareRuleId;
	}

	public void setFareRuleId(long fareRuleId) {
		this.fareRuleId = fareRuleId;
	}

	public Integer getAdvanceBookingDays() {
		return advanceBookingDays;
	}

	public void setAdvanceBookingDays(Integer advanceBookingDays) {
		this.advanceBookingDays = advanceBookingDays;
	}

	public WeekFrequency getDepartureWeekFrequency() {
		return departureWeekFrequency;
	}

	public void setDepartureWeekFrequency(WeekFrequency departureWeekFrequency) {
		this.departureWeekFrequency = departureWeekFrequency;
	}

	public HourValidity getDepartureHourValidity() {
		return departureHourValidity;
	}

	public void setDepartureHourValidity(HourValidity departureHourValidity) {
		this.departureHourValidity = departureHourValidity;
	}

	public Long getLoadFactorMin() {
		return loadFactorMin;
	}

	public void setLoadFactorMin(Long loadFactorMin) {
		this.loadFactorMin = loadFactorMin;
	}

	public Long getLoadFactorMax() {
		return loadFactorMax;
	}

	public void setLoadFactorMax(Long loadFactorMax) {
		this.loadFactorMax = loadFactorMax;
	}

}
