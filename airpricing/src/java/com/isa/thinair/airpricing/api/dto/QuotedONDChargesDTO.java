package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.seatavailability.ChargeQuoteUtils;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareSummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.OndFareDTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;

public class QuotedONDChargesDTO implements Serializable {

	private static final long serialVersionUID = 6583662323270539242L;

	public void setOndWiseQuotedChargesMaps(HashMap<String, HashMap<String, QuotedChargeDTO>> ondWiseQuotedChargesMap) {
		this.ondWiseQuotedChargesMaps = ondWiseQuotedChargesMap;
	}

	public void setOndFaresMap(HashMap<String, FareSummaryDTO> ondFaresMap) {
		this.ondFaresMap = ondFaresMap;
	}

	private Collection<QuotedChargeDTO> getEffectiveInfantCharges(String ondCode, Collection<String> chargeGroups,
			Map<String, Double> totalSurcharges) {
		if (effectiveChargesMap.get(ondCode) == null) {
			if (ondWiseQuotedChargesMaps != null && ondFaresMap != null) {
				FareSummaryDTO ondFareSummaryDTO = (FareSummaryDTO) ondFaresMap.get(ondCode);

				FareSummaryDTO tmpFareSummaryDTO = null;
				Double infantTotalJourneyFare = null;
				for (Iterator<FareSummaryDTO> ondFaresMapIt = ondFaresMap.values().iterator(); ondFaresMapIt.hasNext();) {
					tmpFareSummaryDTO = (FareSummaryDTO) ondFaresMapIt.next();
					if (tmpFareSummaryDTO != null) {
						if (infantTotalJourneyFare == null) {
							infantTotalJourneyFare = tmpFareSummaryDTO.getFareAmount(PaxTypeTO.INFANT);
						} else {
							infantTotalJourneyFare += tmpFareSummaryDTO.getFareAmount(PaxTypeTO.INFANT);
						}
					}
				}

				Map<String, Double> totalFareMap = null;
				if (infantTotalJourneyFare != null) {
					totalFareMap = new HashMap<String, Double>();
					totalFareMap.put(PaxTypeTO.CHILD, 0d);
					totalFareMap.put(PaxTypeTO.ADULT, 0d);
					totalFareMap.put(PaxTypeTO.INFANT, infantTotalJourneyFare);
				}

				// Since we are doing a infant quote we don't need the number of adults and no of children
				effectiveChargesMap.put(ondCode, ChargeQuoteUtils.getChargesWithEffectiveChargeValues(
						getApplicableCharges(ondCode, chargeGroups), ondFareSummaryDTO, 0, 0, totalFareMap, totalSurcharges));
			}
		}
		return (Collection<QuotedChargeDTO>) effectiveChargesMap.get(ondCode);
	}

	private Collection<QuotedChargeDTO> getApplicableCharges(String ondCode, Collection<String> chargeGroups) {
		Collection<QuotedChargeDTO> charges = ((HashMap<String, QuotedChargeDTO>) ondWiseQuotedChargesMaps.get(ondCode)).values();
		List<QuotedChargeDTO> filteredCharges = null;
		if (charges != null && chargeGroups != null) {
			filteredCharges = new ArrayList<QuotedChargeDTO>();
			Iterator<QuotedChargeDTO> chargesIt = charges.iterator();
			while (chargesIt.hasNext()) {
				QuotedChargeDTO quotedCharge = (QuotedChargeDTO) chargesIt.next();
				if (chargeGroups.contains(quotedCharge.getChargeGroupCode())) {
					filteredCharges.add(quotedCharge.clone());
				}
			}
		}
		return filteredCharges;
	}

	public String getOndCode(Integer fareId) {
		return (String) fareIdOndCodeMap.get(fareId);
	}

	public void addFareIdOndCode(Integer fareId, String ondCode) {
		this.fareIdOndCodeMap.put(fareId, ondCode);
	}

	/**
	 * Updates Charges in OndFareDTOs
	 * 
	 * @param ondFareDTOCollection
	 * @param bcChgGroupMap
	 * @return OndFareDTO collection with charges set
	 * @throws ModuleException
	 */
	public Collection<OndFareDTO> setInfantCharges(Collection<OndFareDTO> ondFareDTOCollection,
			Map<String, Collection<String>> bcChgGroupMap) throws ModuleException {
		Iterator<OndFareDTO> ondFareDTOsIt = ondFareDTOCollection.iterator();
		Map<String, Double> totalSurcharges = new HashMap<String, Double>();
		totalSurcharges.put(PaxTypeTO.ADULT, new Double(0));
		totalSurcharges.put(PaxTypeTO.CHILD, new Double(0));
		totalSurcharges.put(PaxTypeTO.INFANT, new Double(0));
		while (ondFareDTOsIt.hasNext()) {
			OndFareDTO ondFareDTO = (OndFareDTO) ondFareDTOsIt.next();
			if (!ondFareDTO.isFlown()) {
				String ondCode = null;
				if (ondFareDTO.getOndCode() != null && !ondFareDTO.getOndCode().equals("")) {
					ondCode = ondFareDTO.getOndCode();
				} else {
					ondCode = getOndCode(new Integer(ondFareDTO.getFareId()));
				}
				Collection<QuotedChargeDTO> charges = getEffectiveInfantCharges(ondCode,
						bcChgGroupMap.get(ondFareDTO.getBCForChargeQuote()), null);

				Map<String, Double> currSurcharges = ChargeQuoteUtils.getTotalSurcharges(charges);

				totalSurcharges.put(PaxTypeTO.ADULT, totalSurcharges.get(PaxTypeTO.ADULT) + currSurcharges.get(PaxTypeTO.ADULT));
				totalSurcharges.put(PaxTypeTO.CHILD, totalSurcharges.get(PaxTypeTO.CHILD) + currSurcharges.get(PaxTypeTO.CHILD));
				totalSurcharges.put(PaxTypeTO.INFANT,
						totalSurcharges.get(PaxTypeTO.INFANT) + currSurcharges.get(PaxTypeTO.INFANT));

				effectiveChargesMap.put(ondCode, null);
			}
		}

		ondFareDTOsIt = ondFareDTOCollection.iterator();
		while (ondFareDTOsIt.hasNext()) {
			OndFareDTO ondFareDTO = (OndFareDTO) ondFareDTOsIt.next();
			if (!ondFareDTO.isFlown()) {
				String ondCode = null;
				if (ondFareDTO.getOndCode() != null && !ondFareDTO.getOndCode().equals("")) {
					ondCode = ondFareDTO.getOndCode();
				} else {
					ondCode = getOndCode(new Integer(ondFareDTO.getFareId()));
				}
				ondFareDTO.setAllCharges(getEffectiveInfantCharges(ondCode, bcChgGroupMap.get(ondFareDTO.getBCForChargeQuote()),
						totalSurcharges));
			}
		}
		return ondFareDTOCollection;
	}

	private HashMap<String, HashMap<String, QuotedChargeDTO>> ondWiseQuotedChargesMaps = null;

	private HashMap<String, FareSummaryDTO> ondFaresMap = null;

	private HashMap<String, Collection<QuotedChargeDTO>> effectiveChargesMap = new HashMap<String, Collection<QuotedChargeDTO>>();

	private HashMap<Integer, String> fareIdOndCodeMap = new HashMap<Integer, String>();

}
