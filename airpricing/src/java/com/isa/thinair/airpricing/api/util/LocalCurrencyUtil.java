package com.isa.thinair.airpricing.api.util;

import java.math.BigDecimal;

import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;

/**
 * @author subash
 *
 */
public class LocalCurrencyUtil {

	public static BigDecimal amountInBaseCurrency(String localCurrencyCode, BigDecimal amount) throws ModuleException {
		try {
			if (localCurrencyCode.equals(AppSysParamsUtil.getBaseCurrency())) {
				return amount;
			}

			ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
			CurrencyExchangeRate currencyExchangeRate = exchangeRateProxy.getCurrencyExchangeRate(localCurrencyCode);
			Currency currency = currencyExchangeRate.getCurrency();

			BigDecimal totVal;
			BigDecimal boundaryValue = currency.getBoundryValue();
			BigDecimal breakPointValue = currency.getBreakPoint();
			BigDecimal exRate = currencyExchangeRate.getExrateCurToBaseNumber();

			if (AppSysParamsUtil.isCurrencyRoundupEnabled()) {

				if (boundaryValue != null && boundaryValue.doubleValue() != 0 && breakPointValue != null) {
					totVal = AccelAeroRounderPolicy.getRoundedValue(AccelAeroCalculator.multiply(amount, exRate), boundaryValue,
							breakPointValue);
				} else {
					totVal = AccelAeroCalculator.multiplyDefaultScale(amount, exRate);
				}
			} else {
				totVal = AccelAeroCalculator.multiplyDefaultScale(amount, exRate);
			}

			return totVal;
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

}
