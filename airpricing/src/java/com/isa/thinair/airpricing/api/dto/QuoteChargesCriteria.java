package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.dto.seatavailability.FareSummaryDTO;
import com.isa.thinair.airpricing.api.criteria.PricingConstants.ChargeGroups;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.core.util.CalendarUtil;

/**
 * Criteria for quoting charges for an OND.
 * 
 * @author Nasly
 */
public class QuoteChargesCriteria implements Serializable {

	private static final long serialVersionUID = 2244279874165179416L;

	private static final String ANY_AIRPORT_CODE_MATCHER = "***";

	/**
	 * OND position identifier (with respect to the journey being quoted) ONDs are based on the fare quoted
	 */

	/**
	 * first OND of multi-OND journey
	 */
	public static final String FIRST_OND = "F";
	/**
	 * last OND of mulit-OND journey
	 */
	public static final String LAST_OND = "L";
	/**
	 * For a single OND journey, OND positioning could be considered to be first as well as last
	 */
	public static final String FIRST_AND_LAST_OND = "FL";
	/**
	 * Any OND inbetween first and last ONDs
	 */
	public static final String INBETWEEN_OND = "I";

	public static final String INCLUDE = "I";

	public static final String EXCLUDE = "E";

	public static final String CODE_FOR_INCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT = "I";
	public static final String CODE_FOR_EXCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT = "E";
	public static final String CODE_FOR_NEGLECTING_ANY_FORM_OF_TRANSIT = "N";
	
	public static final String CODE_FOR_INCLUDE_INTER_STATE_TAX = "Y";	
	public static final String CODE_FOR_EXCLUDE_INTER_STATE_TAX = "N";
	
	public static final String CODE_FOR_INCLUDE_TICKETING_SERVICE_TAX = "Y";	
	public static final String CODE_FOR_EXCLUDE_TICKETING_SERVICE_TAX = "N";
	
	// public static final String CODE_FOR_DISCARD_ANY_FORM_OF_TRANSIT = "D";
	// public static final String CODE_FOR_APPLY_IF_HAS_CORRESPONING_FORM_OF_TRANSIT = "A";

	private String ond;

	private String posAirport;

	private Date depatureDate;

	private String[] chargeCatCodes;

	private String[] chargeGroups = { ChargeGroups.TAX, ChargeGroups.SURCHARGE, ChargeGroups.INFANT_SURCHARGE };

	private FareSummaryDTO fareSummaryDTO;

	private String ondPosition;

	private String agentCode;

	/** Whether the OND qualifies for short transit or not */
	boolean qualifyForShortTransit;

	/** Whether ond for which charge quoted for has inward onds which falls under transit criteria */
	boolean hasInwardTransit;

	/** Whether ond for which charge quoted for has transits within the ond charge quoted for itself */
	boolean hasTransitWithinOnd;

	/** Whether ond for which charge quoted for has outward onds which falls under transit criteria */
	boolean hasOutwardTransit;

	/** Gap between inward ond and ond being charge quoted */
	private long inwardTransitGap;

	private Collection<Long> ondTransitDurList;

	/** Gap between outward ond and ond being charge quoted */
	private long outwardTransitGap;

	/** Map for store total journey fare */
	private Map<String, Double> totalJourneyFare;

	/* Parameters for variance quote */
	private boolean isVarianceQuote;

	private Date minDepartureDate;

	private Date maxDepartureDate;

	private Collection<String> ondCodes;

	private String cabinClassCode;

	private Set<String> transitAirports;

	private Set<String> excludeAPlist;

	private Date chargeQuoteDate;

	private int channelId;

	/** Holds list of charges that should exclude from charge quote by privilege users */
	private List<String> excludedCharges;

	private Integer lastSearchGapInDays = null;

	private Set<String> bundledFareChargeCodes;
	
	private boolean isServiceTaxQuote;
	
	private String serviceTaxPlaceOfSupplyStateCode;
	
	private String serviceTaxPlaceOfSupplyCountryCode;
	
	private String serviceTaxDepositeStateCode;
	
	private String serviceTaxDepositeCountryCode;
	
	private boolean isServiceTaxQuoteForNonTktRev;

	public QuoteChargesCriteria() {
	}

	// public QuoteChargesCriteria(String ond, String posString, Date effectiveDate, String[] chargeCatCodes, String
	// agentString) {
	// setOnd(ond);
	// setPosAirport(posString);
	// setDepatureDate(effectiveDate);
	// setChargeCatCodes(chargeCatCodes);
	// setAgentCode(agentString);
	// }

	public String[] getChargeCatCodes() {
		return chargeCatCodes;
	}

	public void setChargeCatCodes(String[] chargeCatCodes) {
		this.chargeCatCodes = chargeCatCodes;
	}

	public Date getDepatureDate() {
		return depatureDate;
	}

	public void setDepatureDate(Date depatureDate) {
		this.depatureDate = depatureDate;
	}

	public String getOnd() {
		return ond;
	}

	public void setOnd(String ond) {
		this.ond = ond;
	}

	public String getPosAirport() {
		return posAirport;
	}

	public void setPosAirport(String posAirport) {
		this.posAirport = posAirport;
	}

	/**
	 * @return Returns the fareSummaryDTO.
	 */
	public FareSummaryDTO getFareSummaryDTO() {
		return fareSummaryDTO;
	}

	/**
	 * @param fareSummaryDTO
	 *            The fareSummaryDTO to set.
	 */
	public void setFareSummaryDTO(FareSummaryDTO fareSummaryDTO) {
		this.fareSummaryDTO = fareSummaryDTO;
	}

	public String[] getChargeGroups() {
		return chargeGroups;
	}

	public void setChargeGroups(String[] chargeGroups) {
		this.chargeGroups = chargeGroups;
	}

	public String getOndCodeForAnyDepAirport() {
		String ondCodeForAnyDepAirport = "";
		if (getOnd() != null) {
			ondCodeForAnyDepAirport = ANY_AIRPORT_CODE_MATCHER + getOnd().substring(getOnd().indexOf("/"));
		}
		return ondCodeForAnyDepAirport;
	}

	public String getOndCodeForAnyArrAirport() {
		String ondCodeForAnyArrAirport = "";
		if (getOnd() != null) {
			ondCodeForAnyArrAirport = getOnd().substring(0, getOnd().length() - 3) + ANY_AIRPORT_CODE_MATCHER;
		}
		return ondCodeForAnyArrAirport;
	}

	/**
	 * @return the ondPosition
	 */
	public String getOndPosition() {
		return ondPosition;
	}

	/**
	 * @param ondPosition
	 *            the segPosition to set
	 */
	public void setOndPosition(String ondPosition) {
		this.ondPosition = ondPosition;
	}

	public boolean isQualifyForShortTransit() {
		return qualifyForShortTransit;
	}

	public void setQualifyForShortTransit(boolean qualifyForShortTransit) {
		this.qualifyForShortTransit = qualifyForShortTransit;
	}

	/**
	 * @return the agentCode
	 */
	public String getAgentCode() {
		return agentCode;
	}

	/**
	 * @param agentCode
	 *            the agentCode to set
	 */
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * @return the hasInwardTransit
	 */
	public boolean isHasInwardTransit() {
		return hasInwardTransit;
	}

	/**
	 * @param hasInwardTransit
	 *            the hasInwardTransit to set
	 */
	public void setHasInwardTransit(boolean hasInwardTransit) {
		this.hasInwardTransit = hasInwardTransit;
	}

	/**
	 * @return the hasTransitWithinOnd
	 */
	public boolean isHasTransitWithinOnd() {
		return hasTransitWithinOnd;
	}

	/**
	 * @param hasTransitWithinOnd
	 *            the hasTransitWithinOnd to set
	 */
	private void setHasTransitWithinOnd(boolean hasTransitWithinOnd) {
		this.hasTransitWithinOnd = hasTransitWithinOnd;
	}

	/**
	 * @return the hasOutwardTransit
	 */
	public boolean isHasOutwardTransit() {
		return hasOutwardTransit;
	}

	/**
	 * @param hasOutwardTransit
	 *            the hasOutwardTransit to set
	 */
	public void setHasOutwardTransit(boolean hasOutwardTransit) {
		this.hasOutwardTransit = hasOutwardTransit;
	}

	/**
	 * @return the inwardTransitGap
	 */
	public long getInwardTransitGap() {
		return inwardTransitGap;
	}

	/**
	 * @param inwardTransitGap
	 *            the inwardTransitGap to set
	 */
	public void setInwardTransitGap(long inwardTransitGap) {
		this.inwardTransitGap = inwardTransitGap;
	}

	/**
	 * @return the outwardTransitGap
	 */
	public long getOutwardTransitGap() {
		return outwardTransitGap;
	}

	/**
	 * @param outwardTransitGap
	 *            the outwardTransitGap to set
	 */
	public void setOutwardTransitGap(long outwardTransitGap) {
		this.outwardTransitGap = outwardTransitGap;
	}

	/**
	 * @return the totalJourneyFare
	 */
	public Map<String, Double> getTotalJourneyFare() {
		return totalJourneyFare;
	}

	/**
	 * @param totalJourneyFare
	 *            the totalJourneyFare to set
	 */
	public void setTotalJourneyFare(Map<String, Double> totalJourneyFare) {
		this.totalJourneyFare = totalJourneyFare;
	}

	public boolean isVarianceQuote() {
		return this.isVarianceQuote;
	}

	public void setVarianceQuote(boolean isVarianceQuote) {
		this.isVarianceQuote = isVarianceQuote;
	}

	public Date getMinDepartureDate() {
		return this.minDepartureDate;
	}

	public void setMinDepartureDate(Date minDepartureDate) {
		this.minDepartureDate = minDepartureDate;
	}

	public Date getMaxDepartureDate() {
		return this.maxDepartureDate;
	}

	public void setMaxDepartureDate(Date maxDepartureDate) {
		this.maxDepartureDate = maxDepartureDate;
	}

	public Collection<String> getOndCodes() {
		if (this.ondCodes == null)
			return new ArrayList<String>();
		else
			return this.ondCodes;
	}

	public void setOndCodes(Collection<String> ondCodes) {
		this.ondCodes = ondCodes;
	}

	/**
	 * @return the ondTransitDurList
	 */
	public Collection<Long> getOndTransitDurList() {
		return ondTransitDurList;
	}

	/**
	 * @param ondTransitDurList
	 *            the ondTransitDurList to set
	 */
	public void setOndTransitDurList(Collection<Long> ondTransitDurList) {
		this.ondTransitDurList = ondTransitDurList;
		if (ondTransitDurList != null && ondTransitDurList.size() > 0) {
			this.setHasTransitWithinOnd(true);
		}
	}

	public String getCabinClassCode() {
		return cabinClassCode;
	}

	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	public void setTransitAiports(Set<String> transitAirports, Set<String> excludeAPlist) {
		this.transitAirports = transitAirports;
		this.excludeAPlist = excludeAPlist;
	}

	public Set<String> getTransitAirports() {
		return transitAirports;
	}

	public Set<String> getWildCardTransitAirports() {
		Set<String> wildCardAirports = null;
		if (hasTransitAirports()) {
			wildCardAirports = new HashSet<String>();
			for (String airport : getTransitAirports()) {
				wildCardAirports.add(ANY_AIRPORT_CODE_MATCHER + "/" + airport + "/" + ANY_AIRPORT_CODE_MATCHER);
			}
		}
		return wildCardAirports;
	}

	public boolean hasTransitAirports() {
		return (getTransitAirports() != null && getTransitAirports().size() > 0);
	}

	public Set<String> getExcludeArrDepTaxAirports() {
		return excludeAPlist;
	}

	public boolean hasExcludeArrDepTaxAirports() {
		return (getExcludeArrDepTaxAirports() != null && getExcludeArrDepTaxAirports().size() > 0);
	}

	public Date getChargeQuoteDate() {
		if (this.chargeQuoteDate == null) {
			return CalendarUtil.getCurrentSystemTimeInZulu();
		}
		return this.chargeQuoteDate;
	}

	public void setChargeQuoteDate(Date chargeQuoteDate) {
		this.chargeQuoteDate = chargeQuoteDate;
	}

	public int getChannelId() {
		return channelId;
	}

	public void setChannelId(int channelId) {
		this.channelId = channelId;
	}

	public List<String> getExcludedCharges() {
		return excludedCharges;
	}

	public void setExcludedCharges(List<String> excludedCharges) {
		this.excludedCharges = excludedCharges;
	}

	/**
	 * For debug purposes.
	 */
	public StringBuffer getSummary() {
		StringBuffer summary = new StringBuffer("--------------Quoting charges for criteria------------------\n");
		summary.append("OND code			= " + getOnd() + "," + getOndCodeForAnyDepAirport() + "," + getOndCodeForAnyArrAirport()
				+ "\n\r");
		summary.append("POS Airport			= " + getPosAirport() + "\n\r");
		summary.append("Date				= " + getDepatureDate() + "\n\r");
		summary.append("Charge groups		= " + getChargeGroups() + "\n\r");
		summary.append("Charge categories	= " + getChargeCatCodes() + "\n\r");
		summary.append("Ond Position		= " + getOndPosition() + "\n\r");
		summary.append("Agent Code			= " + getAgentCode() + "\n\r");
		summary.append("Short transit		= " + isQualifyForShortTransit() + "\n\r");
		summary.append("hasInwardTransit	= " + isHasInwardTransit() + "\n\r");
		summary.append("hasTransitWithinOnd	= " + isHasTransitWithinOnd() + "\n\r");
		summary.append("hasOutwardTransit	= " + isHasOutwardTransit() + "\n\r");
		summary.append("inwardTransitGap	= " + getInwardTransitGap() + "\n\r");
		summary.append("ondTransitDurList	= " + getOndTransitDurList() + "\n\r");
		summary.append("outwardTransitGap	= " + getOutwardTransitGap() + "\n\r");
		summary.append("Fare Id				= " + (getFareSummaryDTO() != null ? getFareSummaryDTO().getFareId() : "") + "\n\r");
		summary.append("Adult  Fare amount	= "
				+ (getFareSummaryDTO() != null ? getFareSummaryDTO().getFareAmount(PaxTypeTO.ADULT) : "") + "\n\r");
		summary.append("Child  Fare amount	= "
				+ (getFareSummaryDTO() != null ? getFareSummaryDTO().getFareAmount(PaxTypeTO.CHILD) : "") + "\n\r");
		summary.append("Infant Fare amount	= "
				+ (getFareSummaryDTO() != null ? getFareSummaryDTO().getFareAmount(PaxTypeTO.INFANT) : "") + "\n\r");
		summary.append("charge Quote Date	= " + getChargeQuoteDate().toString() + "\n\r");
		if (getExcludedCharges() != null) {
			summary.append("Excluded charges    = " + getExcludedCharges() + "\n\r");
		}
		return summary;
	}

	/**
	 * @return the applyCookieBasedCharges
	 */
	public boolean isApplyCookieBasedCharges() {
		return lastSearchGapInDays != null;
	}

	public void setLastSearchGapInDays(Integer lastSearchGapInDays) {
		this.lastSearchGapInDays = lastSearchGapInDays;
	}

	/**
	 * @return the lastSearchGapInDays
	 */
	public Integer getLastSearchGapInDays() {
		return lastSearchGapInDays;
	}

	public Set<String> getBundledFareChargeCodes() {
		if (this.bundledFareChargeCodes == null) {
			this.bundledFareChargeCodes = new HashSet<String>();
		}
		return bundledFareChargeCodes;
	}

	public void setBundledFareChargeCodes(Set<String> bundledFareChargeCodes) {
		this.bundledFareChargeCodes = bundledFareChargeCodes;
	}

	public boolean isServiceTaxQuote() {
		return isServiceTaxQuote;
	}

	public void setServiceTaxQuote(boolean isServiceTaxQuote) {
		this.isServiceTaxQuote = isServiceTaxQuote;
	}

	public String getServiceTaxPlaceOfSupplyStateCode() {
		return serviceTaxPlaceOfSupplyStateCode;
	}

	public String getServiceTaxDepositeStateCode() {
		return serviceTaxDepositeStateCode;
	}

	public void setServiceTaxPlaceOfSupplyStateCode(String serviceTaxPlaceOfSupplyStateCode) {
		this.serviceTaxPlaceOfSupplyStateCode = serviceTaxPlaceOfSupplyStateCode;
	}

	public void setServiceTaxDepositeStateCode(String serviceTaxDepositeStateCode) {
		this.serviceTaxDepositeStateCode = serviceTaxDepositeStateCode;
	}

	public String getServiceTaxPlaceOfSupplyCountryCode() {
		return serviceTaxPlaceOfSupplyCountryCode;
	}

	public void setServiceTaxPlaceOfSupplyCountryCode(String serviceTaxPlaceOfSupplyCountryCode) {
		this.serviceTaxPlaceOfSupplyCountryCode = serviceTaxPlaceOfSupplyCountryCode;
	}

	public String getServiceTaxDepositeCountryCode() {
		return serviceTaxDepositeCountryCode;
	}

	public void setServiceTaxDepositeCountryCode(String serviceTaxDepositeCountryCode) {
		this.serviceTaxDepositeCountryCode = serviceTaxDepositeCountryCode;
	}

	public boolean isServiceTaxQuoteForNonTktRev() {
		return isServiceTaxQuoteForNonTktRev;
	}

	public void setServiceTaxQuoteForNonTktRev(boolean isServiceTaxQuoteForNonTktRev) {
		this.isServiceTaxQuoteForNonTktRev = isServiceTaxQuoteForNonTktRev;
	}
}
