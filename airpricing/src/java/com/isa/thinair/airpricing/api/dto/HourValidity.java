package com.isa.thinair.airpricing.api.dto;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

public class HourValidity implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Date from;
	private Date to;

	public HourValidity(Date from, Date to) {
		this.from = from;
		this.to = to;
	}

	public boolean isValid(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.set(1970, Calendar.JANUARY, 1);
		Date d = c.getTime();
		if (d.getTime() >= from.getTime() && d.getTime() <= to.getTime()) {
			return true;
		}
		return false;
	}
}