/**
 * 
 */
package com.isa.thinair.airpricing.api.model;

import java.io.Serializable;
import java.math.BigDecimal;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.isa.thinair.commons.core.framework.Tracking;

/**
 * @author indika
 * 
 * @hibernate.class table = "SM_T_AM_SEAT_CHARGE"
 * 
 */
public class SeatCharge extends Tracking implements Serializable {

	private static final long serialVersionUID = -1052899819175574195L;
	
	private Integer chargeId;

	// private String templateCode;

	private Integer seatId;

	private BigDecimal chargeAmount;

	private String status;

	private ChargeTemplate template;
	
	private BigDecimal localCurrencyAmount;
	
	public enum SeatChargeStatus {
		ACTIVE("ACT"), INACTIVE("INA");
		String status;

		private SeatChargeStatus(String status) {
			this.status = status;
		}

		public String getStatusCode() {
			return this.status;
		}
	}

	public int hashCode() {
		return new HashCodeBuilder().append(getChargeId()).toHashCode();
	}

	/**
	 * 
	 * @hibernate.property column = "CHARGE_AMOUNT"
	 * 
	 * @return Returns the chargeAmount.
	 */
	public BigDecimal getChargeAmount() {
		return chargeAmount;
	}

	/**
	 * @param chargeAmount
	 *            The chargeAmount to set.
	 */
	public void setChargeAmount(BigDecimal chargeAmount) {
		this.chargeAmount = chargeAmount;
	}

	/**
	 * @hibernate.id column = "AMS_CHARGE_ID" generator-class = "native"
	 * 
	 * @hibernate.generator-param name = "sequence" value = "SM_S_AM_SEAT_CHARGE"
	 * 
	 * @return Returns the chargeId.
	 */
	public Integer getChargeId() {
		return chargeId;
	}

	/**
	 * @param chargeId
	 *            The chargeId to set.
	 */
	public void setChargeId(Integer chargeId) {
		this.chargeId = chargeId;
	}

	/**
	 * 
	 * @hibernate.property column = "AM_SEAT_ID"
	 * 
	 * @return Returns the seatId.
	 */
	public Integer getSeatId() {
		return seatId;
	}

	/**
	 * @param seatId
	 *            The seatId to set.
	 */
	public void setSeatId(Integer seatId) {
		this.seatId = seatId;
	}

	/**
	 * 
	 * @hibernate.property column = "STATUS"
	 * 
	 * @return Returns the status.
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/*
	 * 
	 * @hibernate.property column = "AMC_TEMPLATE_CODE"
	 * 
	 * @return Returns the templateCode.
	 */
	/*
	 * public String getTemplateCode() { return templateCode; }
	 * 
	 * /**
	 * 
	 * @param templateCode The templateCode to set.
	 */
	/*
	 * public void setTemplateCode(String templateCode) { this.templateCode = templateCode; }
	 */

	/**
	 * @return Returns the templateCode.
	 * @hibernate.many-to-one column = "AMC_TEMPLATE_ID" class= "com.isa.thinair.airpricing.api.model.ChargeTemplate"
	 */
	public ChargeTemplate getTemplate() {
		return template;
	}

	/**
	 * @param templateId
	 *            The templateId to set.
	 */
	public void setTemplate(ChargeTemplate templateId) {
		this.template = templateId;
	}
	
	/**
	 * @return the localCurrencyAmount
	 * @hibernate.property column = "LOCAL_CURR_AMOUNT"
	 */
	public BigDecimal getLocalCurrencyAmount() {
		return localCurrencyAmount;
	}

	public void setLocalCurrencyAmount(BigDecimal localCurrencyAmount) {
		this.localCurrencyAmount = localCurrencyAmount;
	}

}
