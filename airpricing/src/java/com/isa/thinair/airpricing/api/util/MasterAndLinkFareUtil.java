package com.isa.thinair.airpricing.api.util;

import com.isa.thinair.airpricing.api.model.Fare;
import com.isa.thinair.airpricing.api.service.FareBD;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;

public class MasterAndLinkFareUtil {

	public static boolean isEligibleToUpdate(String fareID, String ondCode, boolean isMasterFareUpdate, boolean isLinkFareUpdate,
			String masterFareRefCode, FareBD fareBD) throws ModuleException {
		boolean isEligibleToUpdate = false;

		if (fareID != null && !fareID.equals("")) {
			Fare existingFare = fareBD.getFare(Integer.parseInt(fareID)).getFare();

			String existingFareDefType = existingFare.getFareDefType();

			if (existingFareDefType.equals(Fare.FARE_DEF_TYPE_NORMAL)) {
				isEligibleToUpdate = true;
			} else {
				if (existingFare.getFareDefType().equals(Fare.FARE_DEF_TYPE_MASTER) && !isMasterFareUpdate && isLinkFareUpdate) {
					boolean isMasterFareAlreayLinked = fareBD.isMasterFareAlreayLinked(existingFare.getFareId());
					if (isMasterFareAlreayLinked) {
						throw new ModuleException("airpricing.fare.master.already.linked");
					} else {
						isEligibleToUpdate = true;
					}
				} else if (existingFare.getFareDefType().equals(Fare.FARE_DEF_TYPE_MASTER) && isMasterFareUpdate) {
					if (BeanUtils.nullHandler(masterFareRefCode).length() > 0
							&& !existingFare.getMasterFareRefCode().equals(masterFareRefCode)) {
						if (fareBD.isMasterFareRefCodeAlreayUsed(masterFareRefCode)) {
							throw new ModuleException("airpricing.fare.master.fareref.already.use");
						} else {
							isEligibleToUpdate = true;
						}
					} else {
						isEligibleToUpdate = true;
					}
				} else if (existingFare.getFareDefType().equals(Fare.FARE_DEF_TYPE_LINK) && isLinkFareUpdate) {
					String existingOndCode = existingFare.getOndCode();
					if (!existingOndCode.equals(BeanUtils.nullHandler(ondCode))) {
						throw new ModuleException("airpricing.fare.link.ondchange");
					} else {
						isEligibleToUpdate = true;
					}
				} else {
					isEligibleToUpdate = true;
				}
			}
		} else {
			if (isMasterFareUpdate) {
				if (BeanUtils.nullHandler(masterFareRefCode).length() > 0
						&& fareBD.isMasterFareRefCodeAlreayUsed(masterFareRefCode)) {
					throw new ModuleException("airpricing.fare.master.fareref.already.use");
				} else {
					isEligibleToUpdate = true;
				}
			} else {
				isEligibleToUpdate = true;
			}
		}

		return isEligibleToUpdate;
	}

	public static void updateFieldsOfLinkFare(Fare fare, Fare masterFare) {
		fare.setChildFareType(masterFare.getChildFareType());
		fare.setEffectiveFromDate(masterFare.getEffectiveFromDate());
		fare.setEffectiveToDate(masterFare.getEffectiveToDate());
		fare.setFareDefType(Fare.FARE_DEF_TYPE_LINK);
		fare.setInfantFareType(masterFare.getInfantFareType());
		fare.setLocalCurrencyCode(masterFare.getLocalCurrencyCode());
		fare.setLocalCurrencySelected(masterFare.isLocalCurrencySelected());
		fare.setMasterFareRefCode(null);
		fare.setOndCode(masterFare.getOndCode());
		fare.setReturnEffectiveFromDate(masterFare.getReturnEffectiveFromDate());
		fare.setReturnEffectiveToDate(masterFare.getReturnEffectiveToDate());
		fare.setSalesEffectiveFrom(masterFare.getSalesEffectiveFrom());
		fare.setSalesEffectiveTo(masterFare.getSalesEffectiveTo());
		fare.setMasterFareID(masterFare.getFareId());
		fare.setStatus(masterFare.getStatus());
		fare.setUploaded(false);
		fare.setModifyToSameFare(masterFare.getModifyToSameFare());
	}

	public static void adjustChargesAndUpdateForLinkFare(Fare fare, Fare masterFare) {

		int linkedMasterPercentage = fare.getMasterFarePercentage();

		double masterFareAmount = masterFare.getFareAmount();
		double masterChildAmount = masterFare.getChildFare();
		double masterInfantAmount = masterFare.getInfantFare();
		String masterChildFareType = masterFare.getChildFareType();
		String masterInfantFareType = masterFare.getInfantFareType();

		double linkFareAmount = Double.parseDouble(AccelAeroCalculator.calculatePercentage(masterFareAmount,
				linkedMasterPercentage).toString());
		double linkChildAmount = 0.0;
		if (masterChildFareType.equals(Fare.VALUE_PERCENTAGE_FLAG_V)) {
			linkChildAmount = Double.parseDouble(AccelAeroCalculator.calculatePercentage(masterChildAmount,
					linkedMasterPercentage).toString());
		} else {
			linkChildAmount = masterChildAmount;
		}
		double linkInfantAmount = 0.0;
		if (masterInfantFareType.equals(Fare.VALUE_PERCENTAGE_FLAG_V)) {
			linkInfantAmount = Double.parseDouble(AccelAeroCalculator.calculatePercentage(masterInfantAmount,
					linkedMasterPercentage).toString());
		} else {
			linkInfantAmount = masterInfantAmount;
		}

		Double masterFareAmountInLocal = masterFare.getFareAmountInLocal();
		Double masterChildAmountInLocal = masterFare.getChildFareInLocal();
		Double masterInfantFareInLocal = masterFare.getInfantFareInLocal();

		Double linkFareAmountInLocal = null;
		Double linkChildAmountInLocal = null;
		Double linkInfantAmountInLocal = null;

		if (masterFareAmountInLocal != null) {
			linkFareAmountInLocal = Double.parseDouble(AccelAeroCalculator.calculatePercentage(masterFareAmountInLocal,
					linkedMasterPercentage).toString());
		}
		if (masterChildAmountInLocal != null && masterChildFareType.equals(Fare.VALUE_PERCENTAGE_FLAG_V)) {
			linkChildAmountInLocal = Double.parseDouble(AccelAeroCalculator.calculatePercentage(masterChildAmountInLocal,
					linkedMasterPercentage).toString());
		} else {
			linkChildAmountInLocal = masterChildAmountInLocal;
		}

		if (masterInfantFareInLocal != null && masterInfantFareType.equals(Fare.VALUE_PERCENTAGE_FLAG_V)) {
			linkInfantAmountInLocal = Double.parseDouble(AccelAeroCalculator.calculatePercentage(masterInfantFareInLocal,
					linkedMasterPercentage).toString());
		} else {
			linkInfantAmountInLocal = masterInfantFareInLocal;
		}

		fare.setFareAmount(linkFareAmount);
		fare.setFareAmountInLocal(linkFareAmountInLocal);
		fare.setChildFare(linkChildAmount);
		fare.setChildFareInLocal(linkChildAmountInLocal);
		fare.setInfantFare(linkInfantAmount);
		fare.setInfantFareInLocal(linkInfantAmountInLocal);

	}
}
