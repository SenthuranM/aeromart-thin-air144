/**
 * 
 */
package com.isa.thinair.airpricing.api.criteria;

import java.io.Serializable;

public class MealTemplateSearchCriteria implements Serializable {

	private static final long serialVersionUID = 2867874837589432582L;

	private String cabinClass;

	private String templateCode;

	private String status;

	/**
	 * @return Returns the status.
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return Returns the cabinClass.
	 */
	public String getCabinClass() {
		return cabinClass;
	}

	/**
	 * @param cabinClass
	 *            The cabinClass to set.
	 */
	public void setCabinClass(String cabinClass) {
		this.cabinClass = cabinClass;
	}

	/**
	 * @return Returns the templateCode.
	 */
	public String getTemplateCode() {
		return templateCode;
	}

	/**
	 * @param templateCode
	 *            The templateCode to set.
	 */
	public void setTemplateCode(String templateCode) {
		this.templateCode = templateCode;
	}

}
