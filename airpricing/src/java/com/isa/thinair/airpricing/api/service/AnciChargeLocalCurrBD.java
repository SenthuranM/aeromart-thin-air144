package com.isa.thinair.airpricing.api.service;

import com.isa.thinair.commons.api.exception.ModuleException;

public interface AnciChargeLocalCurrBD {

	
	public static final String SERVICE_NAME = "AnciChargeLocalCurrService";
	
	public void baseCurrAmountUpdateOnLocalCurrExchangeRtUpdate() throws ModuleException;
	
}
