/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:16:12
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airpricing.api.model;

public class InboundDepartureFrequency extends Frequency {

	private static final long serialVersionUID = 5845073083451399022L;

	private int day0;

	private int day1;

	private int day2;

	private int day3;

	private int day4;

	private int day5;

	private int day6;

	/**
	 * returns the validDaysOfWeekA0
	 * 
	 * @return Returns the validDaysOfWeekA0.
	 * 
	 * @hibernate.property column = "VALID_DAYS_OF_WEEK_A0"
	 */
	public int getDay0() {
		// TODO Auto-generated method stub
		return day0;
	}

	public void setDay0(int day0) {
		this.day0 = day0;

	}

	/**
	 * returns the validDaysOfWeekA0
	 * 
	 * @return Returns the validDaysOfWeekA0.
	 * 
	 * @hibernate.property column = "VALID_DAYS_OF_WEEK_A1"
	 */

	public int getDay1() {
		return day1;
	}

	public void setDay1(int day1) {
		this.day1 = day1;

	}

	/**
	 * returns the validDaysOfWeekA0
	 * 
	 * @return Returns the validDaysOfWeekA0.
	 * 
	 * @hibernate.property column = "VALID_DAYS_OF_WEEK_A2"
	 */

	public int getDay2() {
		// TODO Auto-generated method stub
		return day2;
	}

	public void setDay2(int day2) {
		this.day2 = day2;

	}

	/**
	 * returns the validDaysOfWeekA0
	 * 
	 * @return Returns the validDaysOfWeekA0.
	 * 
	 * @hibernate.property column = "VALID_DAYS_OF_WEEK_A3"
	 */

	public int getDay3() {
		// TODO Auto-generated method stub
		return day3;
	}

	public void setDay3(int day3) {
		this.day3 = day3;

	}

	/**
	 * returns the validDaysOfWeekA0
	 * 
	 * @return Returns the validDaysOfWeekA0.
	 * 
	 * @hibernate.property column = "VALID_DAYS_OF_WEEK_A4"
	 */

	public int getDay4() {
		// TODO Auto-generated method stub
		return day4;
	}

	public void setDay4(int day4) {
		this.day4 = day4;

	}

	/**
	 * returns the validDaysOfWeekA0
	 * 
	 * @return Returns the validDaysOfWeekA0.
	 * 
	 * @hibernate.property column = "VALID_DAYS_OF_WEEK_A5"
	 */

	public int getDay5() {
		// TODO Auto-generated method stub
		return day5;
	}

	public void setDay5(int day5) {
		this.day5 = day5;

	}

	/**
	 * returns the validDaysOfWeekA0
	 * 
	 * @return Returns the validDaysOfWeekA0.
	 * 
	 * @hibernate.property column = "VALID_DAYS_OF_WEEK_A6"
	 */

	public int getDay6() {
		// TODO Auto-generated method stub
		return day6;
	}

	public void setDay6(int day6) {
		this.day6 = day6;

	}
}
