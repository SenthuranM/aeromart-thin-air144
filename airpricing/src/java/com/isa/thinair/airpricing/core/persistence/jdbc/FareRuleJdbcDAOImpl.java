package com.isa.thinair.airpricing.core.persistence.jdbc;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airinventory.api.dto.seatavailability.FareSummaryDTO;
import com.isa.thinair.airpricing.api.criteria.AgentFareRuleSearchCriteria;
import com.isa.thinair.airpricing.api.dto.AgentFareRulesDTO;
import com.isa.thinair.airpricing.api.dto.FareChargesTO;
import com.isa.thinair.airpricing.api.dto.FareRuleFeeTO;
import com.isa.thinair.airpricing.api.dto.FareTO;
import com.isa.thinair.airpricing.core.persistence.dao.FareRuleJdbcDAO;
import com.isa.thinair.airpricing.core.persistence.jdbc.farerulejdbcdao.AgentAdhocFareRuleResultsExtractor;
import com.isa.thinair.airpricing.core.persistence.jdbc.farerulejdbcdao.AgentFareRulePreparedStatementCreator;
import com.isa.thinair.airpricing.core.persistence.jdbc.farerulejdbcdao.AgentFareRuleResultsExtractor;
import com.isa.thinair.airpricing.core.util.PricingUtils;
import com.isa.thinair.airreservation.api.dto.OnHoldReleaseTimeDTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

/**
 * BHive: FareRule module
 * 
 * @author BYORN
 */
public class FareRuleJdbcDAOImpl implements FareRuleJdbcDAO {

	private static final String SQL_AGENT_FR = "agentfr";

	private static final String SQL_AGENT_ADHOC = "agentadhoc";

	/** data souce to get system params */
	private DataSource dataSource;

	/** properties to store queries for combos * */
	private Properties queryString;

	private final Log log = LogFactory.getLog(getClass());

	/**
	 * Method to Get Agent vise Master Fare Rules.
	 */
	@Override
	public Collection<AgentFareRulesDTO> getAgentFareRules(AgentFareRuleSearchCriteria agentFareRuleSearchCriteria,
			List<String> orderByList) throws CommonsDataAccessException {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());

		String sqlAgentfr = "";
		String sqlAgentAdhoc = "";

		sqlAgentfr = queryString.getProperty(SQL_AGENT_FR);
		sqlAgentAdhoc = queryString.getProperty(SQL_AGENT_ADHOC);

		log.debug("Agent wise FareRules sql:-" + sqlAgentfr);
		log.debug("Agent wise Adhoc FareRules sql:-" + sqlAgentAdhoc);

		AgentFareRuleResultsExtractor agentFareRuleResultsExtractor = new AgentFareRuleResultsExtractor();

		try {

			/** List of Agent Master Fare Rules * */
			jdbcTemplate.query(new AgentFareRulePreparedStatementCreator(agentFareRuleSearchCriteria, sqlAgentfr),
					agentFareRuleResultsExtractor);
			List<AgentFareRulesDTO> listAgentFareRules = agentFareRuleResultsExtractor.getList();

			/** List of Agent Adhoc Fare Rules * */
			AgentAdhocFareRuleResultsExtractor agentAdhocFareRuleResultsExtractor1 = new AgentAdhocFareRuleResultsExtractor(
					listAgentFareRules);
			jdbcTemplate.query(new AgentFareRulePreparedStatementCreator(agentFareRuleSearchCriteria, sqlAgentAdhoc),
					agentAdhocFareRuleResultsExtractor1);
			List<AgentFareRulesDTO> finalList = agentAdhocFareRuleResultsExtractor1.getList();

			if (log.isDebugEnabled()) {
				log.debug("Get Agent wise Fare Rules for search criteria" + agentFareRuleSearchCriteria.toString());
			}

			return finalList;

		} catch (Exception e) {
			log.error("FAILED : - Get Agent wise Fare Rules for search criteria" + agentFareRuleSearchCriteria.toString());
			throw new CommonsDataAccessException(e, "airpricing.exec.error", "airpricing.desc");
		}

	}

	/**
	 * Return refundable fares
	 * 
	 * @param fareIds
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<FareTO> getRefundableFares(Collection<Integer> fareIds, String prefferedLanguage) {
		// If no fare ids return null
		if (fareIds == null || fareIds.size() == 0) {
			return null;
		}

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		String inStr = PricingUtils.constructINString(fareIds);
		prefferedLanguage = (prefferedLanguage != null && !prefferedLanguage.isEmpty()) ? prefferedLanguage : "en";
		String outFareRuleComment = (AppSysParamsUtil.isEnableFareRuleComments() ? "d.rules_comments ," : "b.rules_comments ,");
		String fareRuleCommentCriteria = (AppSysParamsUtil.isEnableFareRuleComments()
				? "AND b.fare_rule_id=d.fare_rule_id(+) AND d.language_code(+) = '" + prefferedLanguage + "'"
				: "");
		
		String sql = " SELECT a.fare_id, a.fare_amount, a.child_fare_type, a.infant_fare_type, a.child_fare, a.infant_fare, "
				+ " a.BOOKING_CODE, a.OND_CODE, a.currency_code, b.modify_buffer_time, b.domestic_modify_buffer_time, "
				+ " b.fare_cat_id, b.cancellation_charge_amount, b.modification_charge_amount, b.namechange_charge_amount, b.fare_rule_code, b.fare_rule_code , b.fare_rule_id,"
				+ outFareRuleComment
				+ " b.fare_basis_code, b.minimum_stay_over_mins, b.maximum_stay_over_mins, b.openrt_conf_period_mins, "
				+ " b.minimum_stay_over_months, b.maximum_stay_over_months, b.openrt_conf_stay_over_months, b.print_expire_date, "
				+ " b.fare_discount_min, b.fare_discount_max, "
				+ " b.RETURN_FLAG, c.pax_type_code, c.refundable_flag, c.apply_cnx_charge, c.apply_mod_charge, c.apply_fare, "
				+ " c.no_show_charge, c.no_show_chg_basis , c.no_show_chg_breakpoint, c.no_show_chg_boundary"
				+ ", b.modification_charge_type,b.cancellation_charge_type, b.namechange_charge_type, b.min_modification_value,b.max_modification_value,b.min_cancellation_value, b.max_cancellation_value, "
				+ " b.min_namechange_value, b.max_namechange_value,b.modify_date_change, b.modify_route_change, b.FEE_TWL_HRS_CUTOVER_APPLIED, a.MODIFY_TO_SAME_FARE, "
				+ " b.agent_commission_type, b.agent_commission_amount, b.bulk_ticket_flag "
				+ " FROM T_OND_FARE a, T_FARE_RULE b, T_FARE_RULE_PAX c "
				+ (AppSysParamsUtil.isEnableFareRuleComments() ? ", T_FARE_RULE_COMMENT d " : "")
				+ " WHERE a.fare_rule_id = b.fare_rule_id AND b.fare_rule_id = c.fare_rule_id " + " AND a.fare_id in (" + inStr
				+ ")" + fareRuleCommentCriteria;

		Collection<FareTO> fareTOs = (Collection<FareTO>) jdbcTemplate.query(sql, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<FareTO> fareTOs = new HashSet<FareTO>();
				Map<Integer, FareTO> fareIdAndFareTOs = new HashMap<Integer, FareTO>();
				FareTO fareTO;

				int fareId;
				int fareRuleID;
				String childFareType;
				String infantFareType;
				BigDecimal adultFareAmount;
				BigDecimal childFareRatio;
				BigDecimal infantFareRatio;
				BigDecimal noShoreChargeAmount;
				String fareCategoryCode;
				String fareRuleComments;
				String paxType;
				String fareRuleCode;
				String fareBasisCode;
				String bookingClassCode;
				String ondCode;
				String currencyCode;
				BigDecimal baseCNXCharge;
				BigDecimal baseMODCharge;
				BigDecimal nameChangeChargeAmount;
				boolean isFareRefundable;
				boolean isApplyCNXCharge;
				boolean isApplyMODCharge;
				boolean isApplyFare;
				long internationalModifyBufferInMillis;
				long domesticModifyBufferTimeInMillis;
				long minimumStayOver;
				long minimumStayOverMonths;
				long maxumumStayOver;
				long maximumStayOverMonths;
				long openReturnConfirmPeriod;
				long openReturnConfirmPeriodMonths;
				boolean isPrintExpiryDate;
				boolean isModifyToSameFare;
				/* segment modification */
				String allowModifyDate;
				String allowModifyRoute;

				String modificationChargeType;
				String cancellationChargeType;
				String nameChangeChargeType;

				BigDecimal minModificationAmount;
				BigDecimal maximumModificationAmount;
				BigDecimal minCancellationAmount;
				BigDecimal maximumCancellationAmount;
				BigDecimal minNameChangeAmount;
				BigDecimal maxNameChangeAmount;

				Integer fareDiscountMin = null;
				Integer fareDiscountMax = null;

				String noShowChargeBasis;
				BigDecimal noShowChargeBreakPoint;
				BigDecimal noShowChargeBoundary;

				String agentCommissionType;
				BigDecimal agentCommissionAmount;

				boolean is12NoonDayFeePolicyEanbled = false;
				if (rs != null) {
					while (rs.next()) {
						fareId = rs.getInt("fare_id");
						fareRuleID = rs.getInt("fare_rule_id");

						childFareType = PlatformUtiltiies.nullHandler(rs.getString("child_fare_type"));
						infantFareType = PlatformUtiltiies.nullHandler(rs.getString("infant_fare_type"));

						adultFareAmount = rs.getBigDecimal("fare_amount");
						childFareRatio = rs.getBigDecimal("child_fare");
						infantFareRatio = rs.getBigDecimal("infant_fare");

						fareCategoryCode = PlatformUtiltiies.nullHandler(rs.getString("fare_cat_id"));
						if (AppSysParamsUtil.isEnableFareRuleComments()) {
							fareRuleComments = StringUtil.getUnicode(StringUtil.clobToString(rs.getClob("rules_comments")));
						} else {
							fareRuleComments = PlatformUtiltiies.nullHandler(rs.getString("rules_comments"));
						}
						paxType = PlatformUtiltiies.nullHandler(rs.getString("pax_type_code"));
						fareRuleCode = PlatformUtiltiies.nullHandler(rs.getString("fare_rule_code"));
						fareBasisCode = PlatformUtiltiies.nullHandler(rs.getString("fare_basis_code"));

						bookingClassCode = PlatformUtiltiies.nullHandler(rs.getString("BOOKING_CODE"));
						ondCode = PlatformUtiltiies.nullHandler(rs.getString("OND_CODE"));
						currencyCode = rs.getString("currency_code");

						baseCNXCharge = rs.getBigDecimal("cancellation_charge_amount");
						baseMODCharge = rs.getBigDecimal("modification_charge_amount");

						isFareRefundable = PlatformUtiltiies.nullHandler(rs.getString("refundable_flag")).equals("Y")
								? true
								: false;
						isApplyCNXCharge = PlatformUtiltiies.nullHandler(rs.getString("apply_cnx_charge")).equals("Y")
								? true
								: false;
						isApplyMODCharge = PlatformUtiltiies.nullHandler(rs.getString("apply_mod_charge")).equals("Y")
								? true
								: false;
						isApplyFare = PlatformUtiltiies.nullHandler(rs.getString("apply_fare")).equals("Y") ? true : false;
						isModifyToSameFare = PlatformUtiltiies.nullHandler(rs.getString("MODIFY_TO_SAME_FARE")).equals("Y")
								? true
								: false;
						internationalModifyBufferInMillis = AppSysParamsUtil.getTimeInMillis(PlatformUtiltiies.nullHandler(rs
								.getString("modify_buffer_time")));

						domesticModifyBufferTimeInMillis = AppSysParamsUtil.getTimeInMillis(PlatformUtiltiies.nullHandler(rs
								.getString("domestic_modify_buffer_time")));

						minimumStayOver = rs.getLong("minimum_stay_over_mins");
						maxumumStayOver = rs.getLong("maximum_stay_over_mins");
						openReturnConfirmPeriod = rs.getLong("openrt_conf_period_mins");

						minimumStayOverMonths = rs.getLong("minimum_stay_over_months");
						maximumStayOverMonths = rs.getLong("maximum_stay_over_months");
						openReturnConfirmPeriodMonths = rs.getLong("openrt_conf_stay_over_months");

						isPrintExpiryDate = PlatformUtiltiies.nullHandler(rs.getString("print_expire_date")).equals("Y")
								? true
								: false;
						noShoreChargeAmount = rs.getBigDecimal("no_show_charge");

						modificationChargeType = rs.getString("modification_charge_type");// != null &&
																							// !rs.getString("modification_charge_type").equals("")
																							// ?
																							// rs.getString("modification_charge_type")
																							// : "V";
						cancellationChargeType = rs.getString("cancellation_charge_type");

						minModificationAmount = rs.getBigDecimal("min_modification_value");
						maximumModificationAmount = rs.getBigDecimal("max_modification_value");
						minCancellationAmount = rs.getBigDecimal("min_cancellation_value");
						maximumCancellationAmount = rs.getBigDecimal("max_cancellation_value");

						nameChangeChargeAmount = rs.getBigDecimal("namechange_charge_amount");
						nameChangeChargeType = rs.getString("namechange_charge_type");
						minNameChangeAmount = rs.getBigDecimal("min_namechange_value");
						maxNameChangeAmount = rs.getBigDecimal("max_namechange_value");

						allowModifyDate = rs.getString("modify_date_change");
						allowModifyRoute = rs.getString("modify_route_change");
						if (rs.getString("fare_discount_min") != null && rs.getString("fare_discount_max") != null) {
							fareDiscountMin = rs.getInt("fare_discount_min");
							fareDiscountMax = rs.getInt("fare_discount_max");
						} else {
							fareDiscountMin = null;
							fareDiscountMax = null;
						}

						noShowChargeBasis = rs.getString("no_show_chg_basis");
						noShowChargeBreakPoint = rs.getBigDecimal("no_show_chg_breakpoint");
						noShowChargeBoundary = rs.getBigDecimal("no_show_chg_boundary");
						is12NoonDayFeePolicyEanbled = "Y".equals(rs.getString("FEE_TWL_HRS_CUTOVER_APPLIED")) ? true : false;

						agentCommissionType = rs.getString("agent_commission_type");
						agentCommissionAmount = rs.getBigDecimal("agent_commission_amount");

						if (fareIdAndFareTOs.containsKey(fareId)) {
							fareTO = (FareTO) fareIdAndFareTOs.get(fareId);
						} else {
							fareTO = new FareTO();
							fareTO.setFareId(fareId);
							fareTO.setFareRuleID(fareRuleID);
							fareTO.setFareCategoryCode(fareCategoryCode);
							fareTO.setFareRuleCode(fareRuleCode);
							fareTO.setFareBasisCode(fareBasisCode);
							if (AppSysParamsUtil.isEnableFareRuleComments()) {
								fareTO.setFareRulesCommentsInSelected(fareRuleComments);
							} else {
								fareTO.setFareRulesComments(fareRuleComments);
							}
							fareTO.setBookingClassCode(bookingClassCode);
							fareTO.setOndCode(ondCode);
							fareTO.setCurrencyCode(currencyCode);
							fareTO.setInternationalModifyBufferTimeInMillis(internationalModifyBufferInMillis);
							fareTO.setDomesticModifyBufferTimeInMillis(domesticModifyBufferTimeInMillis);
							fareTO.setMinimumStayOver(minimumStayOver);
							fareTO.setMaximumStayOver(maxumumStayOver);
							fareTO.setOpenReturnConfirmPeriod(openReturnConfirmPeriod);
							fareTO.setMinimumStayOverMonths(minimumStayOverMonths);
							fareTO.setMaximumStayOverMonths(maximumStayOverMonths);
							fareTO.setOpenReturnConfirmPeriodMonths(openReturnConfirmPeriodMonths);
							fareTO.setPrintExpiryDate(isPrintExpiryDate);
							fareTO.setReturnFareRule(rs.getString("RETURN_FLAG").equals("Y") ? true : false);
							fareTO.setFareDiscountMin(fareDiscountMin);
							fareTO.setFareDiscountMax(fareDiscountMax);
							fareTO.setNoonDayFeePolicyEnabled(is12NoonDayFeePolicyEanbled);
							fareTO.setModifyToSameFare(isModifyToSameFare);
							fareTO.setAgentCommissionType(agentCommissionType);
							fareTO.setAgentCommissionAmount(agentCommissionAmount);
							fareTO.setNameChangeChargeAmount(nameChangeChargeAmount);
							fareTO.setBulkTicketFareRule(rs.getString("bulk_ticket_flag").equals("Y") ? true : false);

							/* segment modification */
							if (allowModifyDate.equalsIgnoreCase("Y")) {
								fareTO.setIsAllowModifyDate(true);
							} else if (allowModifyDate.equalsIgnoreCase("N")) {
								fareTO.setIsAllowModifyDate(false);
							}
							if (allowModifyRoute.equalsIgnoreCase("Y")) {
								fareTO.setIsAllowModifyRoute(true);
							} else if (allowModifyRoute.equalsIgnoreCase("N")) {
								fareTO.setIsAllowModifyRoute(false);
							}

							FareChargesTO fareChargesTO = new FareChargesTO();
							fareChargesTO.setModificationChargeType(modificationChargeType);
							fareChargesTO.setCancellationChargeType(cancellationChargeType);
							fareChargesTO.setMinModificationAmount(minModificationAmount);
							fareChargesTO.setMaximumModificationAmount(maximumModificationAmount);
							fareChargesTO.setMinCancellationAmount(minCancellationAmount);
							fareChargesTO.setMaximumCancellationAmount(maximumCancellationAmount);
							fareChargesTO.setNameChangeChargeType(nameChangeChargeType);
							fareChargesTO.setMinNameChangeChargeAmount(minNameChangeAmount);
							fareChargesTO.setMaxNameChangeChargeAmount(maxNameChangeAmount);

							fareTO.setFareChargesTO(fareChargesTO);

							fareIdAndFareTOs.put(fareId, fareTO);
						}

						// If it's an adult
						if (PaxTypeTO.ADULT.equals(paxType)) {
							fareTO.setAdultFareRefundable(isFareRefundable);
							fareTO.setAdultApplyFare(isApplyFare);
							fareTO.setAdultApplyModChg(isApplyMODCharge);
							fareTO.setAdultApplyCnxChg(isApplyCNXCharge);
							fareTO.setAdultFareAmount(adultFareAmount);
							fareTO.setAdultNoShoreCharge(noShoreChargeAmount);
							fareTO.setAdultNoShowChargeBasis(noShowChargeBasis);
							fareTO.setAdultNoShowChargeBreakPoint(noShowChargeBreakPoint);
							fareTO.setAdultNoShowChargeBoundary(noShowChargeBoundary);

							// Cancellation charge
							if (isApplyCNXCharge) {
								fareTO.setAdultCancellationCharge(baseCNXCharge);
							} else {
								fareTO.setAdultCancellationCharge(AccelAeroCalculator.getDefaultBigDecimalZero());
							}

							// Modification charge
							if (isApplyMODCharge) {
								fareTO.setAdultModificationCharge(baseMODCharge);
							} else {
								fareTO.setAdultModificationCharge(AccelAeroCalculator.getDefaultBigDecimalZero());
							}
							// If it's a child
						} else if (PaxTypeTO.CHILD.equals(paxType)) {
							fareTO.setChildFareRefundable(isFareRefundable);
							fareTO.setChildApplyFare(isApplyFare);
							fareTO.setChildApplyModChg(isApplyMODCharge);
							fareTO.setChildApplyCnxChg(isApplyCNXCharge);
							fareTO.setChildFareRatio(childFareRatio);
							fareTO.setChildFareType(childFareType);
							fareTO.setChildNoShoreCharge(noShoreChargeAmount);
							fareTO.setChildNoShowChargeBasis(noShowChargeBasis);
							fareTO.setChildNoShowChargeBreakPoint(noShowChargeBreakPoint);
							fareTO.setChildNoShowChargeBoundary(noShowChargeBoundary);
							// Cancellation charge
							if (isApplyCNXCharge) {
								fareTO.setChildCancellationCharge(baseCNXCharge);
							} else {
								fareTO.setChildCancellationCharge(AccelAeroCalculator.getDefaultBigDecimalZero());
							}

							// Modification charge
							if (isApplyMODCharge) {
								fareTO.setChildModificationCharge(baseMODCharge);
							} else {
								fareTO.setChildModificationCharge(AccelAeroCalculator.getDefaultBigDecimalZero());
							}
							// If it's an infant
						} else if (PaxTypeTO.INFANT.equals(paxType)) {
							fareTO.setInfantFareRefundable(isFareRefundable);
							fareTO.setInfantApplyFare(isApplyFare);
							fareTO.setInfantApplyModChg(isApplyMODCharge);
							fareTO.setInfantApplyCnxChg(isApplyCNXCharge);
							fareTO.setInfantFareRatio(infantFareRatio);
							fareTO.setInfantFareType(infantFareType);
							fareTO.setInfantNoShoreCharge(noShoreChargeAmount);
							fareTO.setInfantNoShowChargeBasis(noShowChargeBasis);
							fareTO.setInfantNoShowChargeBreakPoint(noShowChargeBreakPoint);
							fareTO.setInfantNoShowChargeBoundary(noShowChargeBoundary);
							// Cancellation charge
							if (isApplyCNXCharge) {
								fareTO.setInfantCancellationCharge(baseCNXCharge);
							} else {
								fareTO.setInfantCancellationCharge(AccelAeroCalculator.getDefaultBigDecimalZero());
							}

							// Modification charge
							if (isApplyMODCharge) {
								fareTO.setInfantModificationCharge(baseMODCharge);
							} else {
								fareTO.setInfantModificationCharge(AccelAeroCalculator.getDefaultBigDecimalZero());
							}
						} else {
							throw new CommonsDataAccessException("airpricing.logic.paxTypeInfoNotExist");
						}

						fareTOs.add(fareTO);
					}
				}
				return fareTOs;
			}
		});
		return fareTOs;
	}

	/**
	 * Return refundable fares
	 * 
	 * @param fareRuleIds
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Map<Integer, FareSummaryDTO> getPaxDetails(Collection<Integer> fareRuleIds) {
		log.debug("Inside getPaxDetails");
		if (fareRuleIds == null || fareRuleIds.size() == 0) {
			return null;
		}

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		String inStr = PricingUtils.constructINString(fareRuleIds);

		// Final SQL
		String sql = "SELECT FRP.FARE_RULE_ID FARE_RULE_ID, FRP.APPLY_FARE APPLY_FARE, FRP.PAX_TYPE_CODE PAX_TYPE_CODE,"
				+ " FRP.REFUNDABLE_FLAG REFUNDABLE_FLAG" + " FROM  T_FARE_RULE_PAX FRP" + " WHERE FRP.FARE_RULE_ID IN (" + inStr
				+ ")" + " ORDER BY FRP.FARE_RULE_ID ASC";

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql);
		log.debug(" SQL Parameter            : " + inStr);
		log.debug("############################################");

		Map<Integer, FareSummaryDTO> mapFareRuleIdAndSummary = (Map<Integer, FareSummaryDTO>) jdbcTemplate.query(sql,
				new ResultSetExtractor() {

					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						FareSummaryDTO fareSummaryDTO;
						Map<Integer, FareSummaryDTO> mapFareRuleIdAndSummary = new HashMap<Integer, FareSummaryDTO>();
						int fareRuleID;
						String paxType;

						if (rs != null) {
							while (rs.next()) {
								fareRuleID = rs.getInt("FARE_RULE_ID");
								paxType = rs.getString("PAX_TYPE_CODE");

								if (mapFareRuleIdAndSummary.containsKey(fareRuleID)) {
									fareSummaryDTO = (FareSummaryDTO) mapFareRuleIdAndSummary.get(fareRuleID);

									if (paxType.equalsIgnoreCase(PaxTypeTO.ADULT)) {
										fareSummaryDTO.setAdultApplicability(BeanUtils.nullHandler(rs.getString("APPLY_FARE"))
												.equalsIgnoreCase("Y") ? true : false);
										fareSummaryDTO.setAdultFareRefundable(BeanUtils.nullHandler(
												rs.getString("REFUNDABLE_FLAG")).equalsIgnoreCase("Y") ? true : false);
									} else if (paxType.equalsIgnoreCase(PaxTypeTO.CHILD)) {
										fareSummaryDTO.setChildApplicability(BeanUtils.nullHandler(rs.getString("APPLY_FARE"))
												.equalsIgnoreCase("Y") ? true : false);
										fareSummaryDTO.setChildFareRefundable(BeanUtils.nullHandler(
												rs.getString("REFUNDABLE_FLAG")).equalsIgnoreCase("Y") ? true : false);

									} else if (paxType.equalsIgnoreCase(PaxTypeTO.INFANT)) {
										fareSummaryDTO.setInfantApplicability(BeanUtils.nullHandler(rs.getString("APPLY_FARE"))
												.equalsIgnoreCase("Y") ? true : false);
										fareSummaryDTO.setInfantFareRefundable(BeanUtils.nullHandler(
												rs.getString("REFUNDABLE_FLAG")).equalsIgnoreCase("Y") ? true : false);
									}
								} else {
									fareSummaryDTO = new FareSummaryDTO();
									fareSummaryDTO.setFareRuleID(fareRuleID);

									if (paxType.equalsIgnoreCase(PaxTypeTO.ADULT)) {
										fareSummaryDTO.setAdultApplicability(BeanUtils.nullHandler(rs.getString("APPLY_FARE"))
												.equalsIgnoreCase("Y") ? true : false);

										fareSummaryDTO.setAdultFareRefundable(BeanUtils.nullHandler(
												rs.getString("REFUNDABLE_FLAG")).equalsIgnoreCase("Y") ? true : false);

									} else if (paxType.equalsIgnoreCase(PaxTypeTO.CHILD)) {
										fareSummaryDTO.setChildApplicability(BeanUtils.nullHandler(rs.getString("APPLY_FARE"))
												.equalsIgnoreCase("Y") ? true : false);

										fareSummaryDTO.setChildFareRefundable(BeanUtils.nullHandler(
												rs.getString("REFUNDABLE_FLAG")).equalsIgnoreCase("Y") ? true : false);

									} else if (paxType.equalsIgnoreCase(PaxTypeTO.INFANT)) {
										fareSummaryDTO.setInfantApplicability(BeanUtils.nullHandler(rs.getString("APPLY_FARE"))
												.equalsIgnoreCase("Y") ? true : false);

										fareSummaryDTO.setInfantFareRefundable(BeanUtils.nullHandler(
												rs.getString("REFUNDABLE_FLAG")).equalsIgnoreCase("Y") ? true : false);
									}

									mapFareRuleIdAndSummary.put(fareRuleID, fareSummaryDTO);
								}
							}
						}

						return mapFareRuleIdAndSummary;
					}
				});

		log.debug("Exit getPaxDetails");
		return mapFareRuleIdAndSummary;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<FareRuleFeeTO> getFareRuleFees(Collection<Integer> fareRuleIds) {
		// If no fare ids return null
		if (fareRuleIds == null || fareRuleIds.size() == 0) {
			return null;
		}

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		String inStr = PricingUtils.constructINString(fareRuleIds);

		String sql = " SELECT FEE.FARE_RULE_FEE_ID, FEE.FARE_RULE_ID, FEE.CHARGE_TYPE, FEE.CHARGE_AMOUNT_TYPE, "
				+ " FEE.CHARGE_AMOUNT, FEE.MINIMUM_PERT_CHARGE_AMOUNT, FEE.MAXIMUM_PERT_CHARGE_AMOUNT, FEE.COMPARE_AGAINST, "
				+ " FEE.TIME_TYPE, FEE.TIME_VALUE, FEE.STATUS FROM T_FARE_RULE_FEE FEE WHERE FEE.STATUS = 'ACT' "
				+ " AND FEE.FARE_RULE_ID IN (" + inStr + ") ORDER BY FEE.CHARGE_TYPE, FEE.FARE_RULE_FEE_ID ";

		Collection<FareRuleFeeTO> fareRuleFeeTOs = (Collection<FareRuleFeeTO>) jdbcTemplate.query(sql, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<FareRuleFeeTO> fareRuleFeeTOs = new ArrayList<FareRuleFeeTO>();

				if (rs != null) {
					while (rs.next()) {
						FareRuleFeeTO fareRuleFeeTO = new FareRuleFeeTO();
						fareRuleFeeTO.setFareRuleFeeId(rs.getLong("FARE_RULE_FEE_ID"));
						fareRuleFeeTO.setFareRuleId(rs.getInt("FARE_RULE_ID"));
						fareRuleFeeTO.setChargeType(BeanUtils.nullHandler(rs.getString("CHARGE_TYPE")));
						fareRuleFeeTO.setChargeAmountType(BeanUtils.nullHandler(rs.getString("CHARGE_AMOUNT_TYPE")));
						fareRuleFeeTO.setChargeAmount(rs.getBigDecimal("CHARGE_AMOUNT"));

						fareRuleFeeTO.setMinimumPerChargeAmt(rs.getBigDecimal("MINIMUM_PERT_CHARGE_AMOUNT"));
						fareRuleFeeTO.setMaximumPerChargeAmt(rs.getBigDecimal("MAXIMUM_PERT_CHARGE_AMOUNT"));
						fareRuleFeeTO.setCompareAgainst(BeanUtils.nullHandler(rs.getString("COMPARE_AGAINST")));
						fareRuleFeeTO.setTimeType(BeanUtils.nullHandler(rs.getString("TIME_TYPE")));
						fareRuleFeeTO.setTimeValue(BeanUtils.nullHandler(rs.getString("TIME_VALUE")));
						fareRuleFeeTO.setStatus(BeanUtils.nullHandler(rs.getString("STATUS")));

						fareRuleFeeTOs.add(fareRuleFeeTO);
					}
				}
				return fareRuleFeeTOs;
			}
		});
		return fareRuleFeeTOs;
	}

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public Properties getQueryString() {
		return queryString;
	}

	public void setQueryString(Properties queryString) {
		this.queryString = queryString;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<Integer, FareSummaryDTO> getLocalFares(Collection<Integer> fareIds) {
		if (fareIds == null || fareIds.size() == 0) {
			return null;
		}

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		String inStr = PricingUtils.constructINString(fareIds);

		String sql = " SELECT FARE_ID, FARE_AMOUNT_IN_LOCAL, CHILD_FARE_IN_LOCAL,  CHILD_FARE_TYPE, INFANT_FARE_IN_LOCAL, INFANT_FARE_TYPE"
				+ " , CURRENCY_CODE FROM T_OND_FARE WHERE FARE_ID IN ( " + inStr + " )";

		Map<Integer, FareSummaryDTO> mapFareRuleIdAndSummary = (Map<Integer, FareSummaryDTO>) jdbcTemplate.query(sql,
				new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						FareSummaryDTO fareSummaryDTO;
						Map<Integer, FareSummaryDTO> mapFareRuleIdAndSummary = new HashMap<Integer, FareSummaryDTO>();
						Integer fareRuleID;

						if (rs != null) {
							while (rs.next()) {
								fareRuleID = rs.getInt("FARE_ID");

								if (!mapFareRuleIdAndSummary.containsKey(fareRuleID)) {
									mapFareRuleIdAndSummary.put(fareRuleID, new FareSummaryDTO());
								}
								fareSummaryDTO = mapFareRuleIdAndSummary.get(fareRuleID);
								fareSummaryDTO.setAdultFare(rs.getDouble("FARE_AMOUNT_IN_LOCAL"));
								fareSummaryDTO.setChildFare(rs.getDouble("CHILD_FARE_IN_LOCAL"));
								fareSummaryDTO.setChildFareType(rs.getString("CHILD_FARE_TYPE"));
								fareSummaryDTO.setInfantFare(rs.getDouble("INFANT_FARE_IN_LOCAL"));
								fareSummaryDTO.setInfantFareType(rs.getString("INFANT_FARE_TYPE"));
								fareSummaryDTO.setCurrenyCode(rs.getString("CURRENCY_CODE"));

							}
						}
						return mapFareRuleIdAndSummary;
					}
				});
		return mapFareRuleIdAndSummary;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<String> getFareRuleCodeNotInFareRule(String currencyCode) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());

		String sql = "SELECT fare_rule_code FROM t_fare_rule WHERE fare_rule_code NOT IN "
				+ " (SELECT DISTINCT fare_rule_code FROM t_fare_rule WHERE fare_rule_code IS NOT NULL) AND currency_code='"
				+ currencyCode + "' AND currency_code IS NOT NULL";

		Collection<String> fareRuleCodeColl = (Collection<String>) jdbcTemplate.query(sql, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<String> fareRuleCodeColl = new ArrayList<String>();

				if (rs != null) {
					while (rs.next()) {

						fareRuleCodeColl.add(rs.getString("fare_rule_code"));

					}
				}
				return fareRuleCodeColl;
			}
		});
		return fareRuleCodeColl;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<OnHoldReleaseTimeDTO> getOHDRelTimeDTO(Integer fareRuleId) {
		if (fareRuleId == null) {
			return null;
		}

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());

		String sql = " SELECT OHD.ONHOLD_RELEASE_TIME_ID, OHD.START_CUTOVER, OHD.END_CUTOVER, OHD.RELEASE_TIME, OHD.REL_TIME_WRT, OHD.MODULE_CODE, OHD.VERSION "
				+ " FROM T_ONHOLD_RELEASE_TIME OHD where OHD.FARE_RULE_ID = " + fareRuleId;

		Collection<OnHoldReleaseTimeDTO> ohdRelTimeDTOs = (Collection<OnHoldReleaseTimeDTO>) jdbcTemplate.query(sql,
				new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

						Collection<OnHoldReleaseTimeDTO> colOnholdReleaseTimes = new ArrayList<OnHoldReleaseTimeDTO>();
						if (rs != null) {
							while (rs.next()) {
								OnHoldReleaseTimeDTO onholdReleaseTimeDTO = new OnHoldReleaseTimeDTO();
								onholdReleaseTimeDTO.setReleaseTimeId(rs.getInt("ONHOLD_RELEASE_TIME_ID"));
								onholdReleaseTimeDTO.setEndCutoverTime(rs.getLong("END_CUTOVER"));
								onholdReleaseTimeDTO.setStartCutoverTime(rs.getLong("START_CUTOVER"));
								onholdReleaseTimeDTO.setModuleCode(rs.getString("MODULE_CODE"));
								onholdReleaseTimeDTO.setReleaseTime(rs.getLong("RELEASE_TIME"));
								onholdReleaseTimeDTO.setReleaseTimeWithRelativeTo(rs.getString("REL_TIME_WRT"));
								onholdReleaseTimeDTO.setVersion(rs.getLong("VERSION"));
								colOnholdReleaseTimes.add(onholdReleaseTimeDTO);
							}
						}
						return colOnholdReleaseTimes;
					}
				});
		return ohdRelTimeDTOs;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Integer> getFareIdsForFareRule(Integer fareRuleId) {
		if (fareRuleId == null) {
			return null;
		}

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());

		String sql = " SELECT OND.FARE_ID " + " FROM T_OND_FARE OND where OND.FARE_RULE_ID = " + fareRuleId;

		Collection<Integer> fareIds = (Collection<Integer>) jdbcTemplate.query(sql, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				Collection<Integer> tmpFareIds = new ArrayList<Integer>();
				if (rs != null) {
					while (rs.next()) {
						tmpFareIds.add(rs.getInt("FARE_ID"));
					}
				}
				return tmpFareIds;
			}
		});
		return fareIds;
	}
}
