package com.isa.thinair.airpricing.core.persistence.dao;

import java.util.Collection;

import com.isa.thinair.airpricing.api.dto.SSRChargeDTO;
import com.isa.thinair.airpricing.core.persistence.jdbc.ssrchargejdbcdao.SSRChargeSearchCriteria;
import com.isa.thinair.commons.api.exception.ModuleException;

public interface SSRChargeJdbcDAO {
	/**
	 * search SSR charges
	 * 
	 * @return Collection<SSRChargeDTO>
	 * @throws ModuleException
	 */
	public Collection<SSRChargeDTO> getAvailableSSRChargeDTOs(SSRChargeSearchCriteria ssrChargeSearchCriteria)
			throws ModuleException;
}
