package com.isa.thinair.airpricing.core.persistence.dao;

import java.util.List;

import com.isa.thinair.airpricing.api.criteria.FlexiRuleSearchCriteria;
import com.isa.thinair.airpricing.api.model.FlexiRule;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * BHive Commons: FlexiRule module
 * 
 * @author Dhanya
 */

public interface FlexiRuleDAO {

	public void saveFlexiRule(FlexiRule flexiRule);

	public void removeFlexiRule(int flexiRuleId);

	public Page getFlexiRules(FlexiRuleSearchCriteria flexiRuleSearchCriteria, int startIndex, int pageSize, List<String> orderbyfields)
			throws ModuleException;
}