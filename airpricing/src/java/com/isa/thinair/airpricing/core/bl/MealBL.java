/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:12:34
 * 
 * $Id$
 * 
 * ===============================================================================
 */

package com.isa.thinair.airpricing.core.bl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.meal.FlightMealDTO;
import com.isa.thinair.airinventory.api.service.MealBD;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.model.Meal;
import com.isa.thinair.airmaster.api.model.MealCategory;
import com.isa.thinair.airpricing.api.dto.MealTemplateDTO;
import com.isa.thinair.airpricing.api.model.MealCharge;
import com.isa.thinair.airpricing.api.model.MealTemplate;
import com.isa.thinair.airpricing.api.model.MultiSelectCategoryRestriction;
import com.isa.thinair.airpricing.api.service.AirpricingUtils;
import com.isa.thinair.airpricing.api.util.AirPricingCustomConstants;
import com.isa.thinair.airpricing.api.util.LocalCurrencyUtil;
import com.isa.thinair.airpricing.api.util.SplitUtil;
import com.isa.thinair.airpricing.core.audit.AuditAirpricing;
import com.isa.thinair.airpricing.core.persistence.dao.ChargeDAO;
import com.isa.thinair.airpricing.core.persistence.dao.ChargeJdbcDAO;
import com.isa.thinair.airpricing.core.util.InternalConstants;
import com.isa.thinair.airpricing.core.util.PricingUtils;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.webplatform.api.util.Constants;

/**
 * Class to implement the bussiness logic related to Airpricing
 * 
 * 
 */

public class MealBL {
	private final Log log = LogFactory.getLog(MealBL.class);

	private ChargeDAO chargedao;

	private ChargeJdbcDAO chargejdbcdao;

	public MealBL() {

		chargedao = (ChargeDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.CHARGE_DAO);

		chargejdbcdao = getChargeJdbcDAO();

	}

	@SuppressWarnings("unused")
	public void saveTemplate(MealTemplate mealTemplate, Integer chargeid) throws ModuleException {
		try {

			chargedao.saveMealTemplate(mealTemplate);

			// remove calling from create
			if (mealTemplate.getVersion() != 0) {
				Collection<FlightMealDTO> colFltMls = chargejdbcdao.getSegmentMealsForMealTemplate(mealTemplate.getTemplateId());
				MealBD oMealBD = AirpricingUtils.getMealBD();
				Collection<FlightMealDTO> colSegs = new HashSet<FlightMealDTO>();

				for (FlightMealDTO fltMlDTO : colFltMls) {
					fltMlDTO.setTemplateId(mealTemplate.getTemplateId());
					colSegs.add(fltMlDTO);
				}

				ServiceResponce sr = oMealBD.assignFlightMealCharges(colSegs, chargeid, true);
			}

		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	private ChargeJdbcDAO getChargeJdbcDAO() {
		return (ChargeJdbcDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.CHARGE_JDBC_DAO);
	}

	public MealTemplate createTemplate(MealTemplateDTO mealTmpDto) throws ModuleException {
		try {
			MealTemplate mealTemplate = new MealTemplate();
			mealTemplate.setDescription(mealTmpDto.getDescription().trim());
			mealTemplate.setTemplateCode(mealTmpDto.getTemplateCode().toUpperCase().trim());
			if (mealTmpDto.getTemplateId() != null) {
				mealTemplate.setTemplateId(mealTmpDto.getTemplateId());
			}
			mealTemplate.setVersion(mealTmpDto.getVersion());
			mealTemplate.setCreatedBy(mealTmpDto.getCreatedBy());
			mealTemplate.setCreatedDate(mealTmpDto.getCreatedDate());

			if (mealTmpDto.getStatus() != null) {
				mealTemplate.setStatus(mealTmpDto.getStatus());
			} else {
				mealTemplate.setStatus(Meal.STATUS_INACTIVE);
			}
			mealTemplate.setVersion(mealTmpDto.getVersion());
			Set<MealCharge> mlCharges = new HashSet<MealCharge>();
			if (mealTmpDto.getMealCharges() != null && !mealTmpDto.getMealCharges().equals("")) {
				String[] arrChages = mealTmpDto.getMealCharges().split("~");
				String[] strChages = null;
				for (int i = 0; i < arrChages.length; i++) {
					if (arrChages[i] != null && !arrChages[i].equals("")) {
						strChages = arrChages[i].split(Constants.STRING_SEPARATOR);
						MealCharge mealChg = new MealCharge();
						mealChg.setMealName(strChages[0]);
						if (strChages[1] != null && !strChages[1].trim().equals("")) {
							mealChg.setChargeId(new Integer(strChages[1]));
						}
						mealChg.setTemplateId(mealTemplate);
						mealChg.setMealId(new Integer(strChages[3]));
						mealChg.setLocalCurrencyAmount(new BigDecimal(strChages[4]));
						mealChg.setAmount(LocalCurrencyUtil.amountInBaseCurrency(mealTmpDto.getLocalCurrCode(),
								mealChg.getLocalCurrencyAmount()));
						mealChg.setAllocatedMeal(new Integer(strChages[5]));
						mealChg.setVersion(new Long(strChages[6]));
						mealChg.setStatus(strChages[7]);
						String[] splittedCabinClassAndLogicalCabinClass = SplitUtil
								.getSplittedCabinClassAndLogicalCabinClass(strChages[8]);
						mealChg.setLogicalCCCode(splittedCabinClassAndLogicalCabinClass[1]);
						mealChg.setCabinClass(splittedCabinClassAndLogicalCabinClass[0]);
						if (strChages[9] != null && !strChages[9].trim().equals("")) {
							mealChg.setPopularity(new Integer(strChages[9]));
						}						
						mlCharges.add(mealChg);
					}
				}
			}
			mealTemplate.setMealCharges(mlCharges);
			mealTemplate.setChargeLocalCurrencyCode(mealTmpDto.getLocalCurrCode());
			
			Set<MultiSelectCategoryRestriction> categoryRestrictions =new HashSet<MultiSelectCategoryRestriction>();
			if (mealTmpDto.getMultiRestrictions() != null && !mealTmpDto.getMultiRestrictions().equals("")) {
				String[] arrSelectedCat = mealTmpDto.getMultiRestrictions().split("~");

				for (String selectedCategories : arrSelectedCat) {

					if (selectedCategories != null && !selectedCategories.equals("")) {
						String[] arrRestCat = selectedCategories.split(Constants.STRING_SEPARATOR);

						MultiSelectCategoryRestriction restriction = new MultiSelectCategoryRestriction();
						restriction.setMealCategoryId(Integer.valueOf(arrRestCat[0]));

						if (arrRestCat[1] != "-1") {
							restriction.setMultiSelectRestrictionCatId(Integer.valueOf(arrRestCat[1]));
						}

						// FIXME: hibernate.hbm constraint on model should be fine tuned, due to the time constraint
						// committing this
						// fix as update of Meal Template to work as expected
						// if (arrRestCat[2] != "-1") {
						// restriction.setVersion(Long.valueOf(arrRestCat[2]));
						// }
						restriction.setTemplateId(mealTemplate);

						categoryRestrictions.add(restriction);

					}
				}
			}
			
			// FIXME: hibernate.hbm constraint on model should be fine tuned, due to the time constraint committing this
			// fix as update/delete of Meal Template to work as expected
			if (mealTemplate.getTemplateId() > 0) {
				chargedao.removeMealTemplatesCategoryRestrictions(mealTemplate.getTemplateId());
			}
			
			mealTemplate.setCategoryRestrictions(categoryRestrictions);
			return mealTemplate;
		} catch (ModuleException e) {
			log.error("ERROR-Auto Updating Charges upon Exchnage Rate Change Failed");
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	public void updateMealChargeBaseCurrAmountOnLocalCurrExchangeRtUpdate(CurrencyExchangeRate currencyExchangeRate,
			UserPrincipal userPrincipal) throws ModuleException {

		try {
			ArrayList<MealTemplate> affectedMealTMPList;
			BigDecimal newChargeAmount;
			BigDecimal exRate, boundaryValue, breakPointValue;

			affectedMealTMPList = (ArrayList<MealTemplate>) chargedao.getAffectedMealTemplates(currencyExchangeRate.getCurrency()
					.getCurrencyCode());
			exRate = currencyExchangeRate.getExrateCurToBaseNumber();
			boundaryValue = currencyExchangeRate.getCurrency().getBoundryValue();
			breakPointValue = currencyExchangeRate.getCurrency().getBreakPoint();
			for (MealTemplate mealTemp : affectedMealTMPList) {
				for (MealCharge mealChrg : mealTemp.getMealCharges()) {
					if (!mealChrg.getStatus().equals(AirPricingCustomConstants.STATUS_ACTIVE)) {
						continue;
					}
					newChargeAmount = AccelAeroRounderPolicy.getRoundedValue(
							AccelAeroCalculator.multiply(mealChrg.getLocalCurrencyAmount(), exRate), boundaryValue,
							breakPointValue);
					mealChrg.setAmount(newChargeAmount);
				}
			}
			updateMealTemplate(affectedMealTMPList, userPrincipal);

		} catch (ModuleException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	private void updateMealTemplate(ArrayList<MealTemplate> mealTMPList, UserPrincipal userPrincipal) throws ModuleException {
		Iterator<MealTemplate> iterator = mealTMPList.iterator();
		try {
			while (iterator.hasNext()) {
				MealTemplate mealTemplate = iterator.next();
				mealTemplate = (MealTemplate) PricingUtils.setUserDetails(mealTemplate, userPrincipal);
				chargedao.saveMealTemplate(mealTemplate);
				AuditAirpricing.doAuditMealAddOrModifyTemplate(mealTemplate, userPrincipal);
			}
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}

	}	
}
