/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:12:34
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airpricing.core.persistence.dao;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airpricing.api.criteria.FareSearchCriteria;
import com.isa.thinair.airpricing.api.dto.LightFareDTO;
import com.isa.thinair.airpricing.core.criteria.ApplicableFareSearchCriteria;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * BHive Commons: FareRule module
 * 
 * @author Byorn
 */
public interface FareJdbcDAO {

	public Page getFares(FareSearchCriteria fareSearchCriteria, int startIndex, int pageSize, List<String> orderBy)
			throws CommonsDataAccessException;

	public HashMap<String, List<String>> getAgentsLinkedToBookingClass(Collection<String> bookingCodesCollection)
			throws ModuleException;

	public Collection<String> getBookingCodesLinkedToAgent(String agentCode);

	public boolean isFareAssignedForPnr(Integer fareId);

	public boolean isFareRuleAssociatedWithPnr(Integer fareRuleId);

	public boolean isModifyToSameFareEnabled(Integer fareId);

	public Map<Integer, Integer> getSameReturnGroupSegDetails(Collection<Integer> dateChangedResSegList);

	public List<LightFareDTO> getApplicableFares(ApplicableFareSearchCriteria searchCriteria);

}
