/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 *
 * Created on Jul 13, 2005 17:12:34
 *
 * $Id$
 *
 * ===============================================================================
 */
package com.isa.thinair.airpricing.core.persistence.dao;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airpricing.api.criteria.BaggageTemplateSearchCriteria;
import com.isa.thinair.airpricing.api.criteria.ChargeSearchCriteria;
import com.isa.thinair.airpricing.api.criteria.ChargeTemplateSearchCriteria;
import com.isa.thinair.airpricing.api.criteria.MealTemplateSearchCriteria;
import com.isa.thinair.airpricing.api.criteria.ONDChargeSearchCriteria;
import com.isa.thinair.airpricing.api.criteria.ONDCodeSearchCriteria;
import com.isa.thinair.airpricing.api.criteria.SSRChargesSearchCriteria;
import com.isa.thinair.airpricing.api.dto.ChargeTO;
import com.isa.thinair.airpricing.api.model.BaggageCharge;
import com.isa.thinair.airpricing.api.model.BaggageTemplate;
import com.isa.thinair.airpricing.api.model.Charge;
import com.isa.thinair.airpricing.api.model.ChargeRate;
import com.isa.thinair.airpricing.api.model.ChargeTemplate;
import com.isa.thinair.airpricing.api.model.MCO;
import com.isa.thinair.airpricing.api.model.MCOServices;
import com.isa.thinair.airpricing.api.model.MealCharge;
import com.isa.thinair.airpricing.api.model.MealTemplate;
import com.isa.thinair.airpricing.api.model.ONDBaggage;
import com.isa.thinair.airpricing.api.model.ONDBaggageChargeTemplate;
import com.isa.thinair.airpricing.api.model.ONDBaggageTemplate;
import com.isa.thinair.airpricing.api.model.ReportChargeGroup;
import com.isa.thinair.airpricing.api.model.SSRCharge;
import com.isa.thinair.airpricing.api.model.SeatCharge;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * BHive Commons: Charge module
 * 
 * ChargeDAO is business delegate interface for the charge service apis
 * 
 */

public interface ChargeDAO {

	/** Interface method to getCharges...with paging and criteria **/
	public Page getCharges(ChargeSearchCriteria chargeSearchCriteria, int startIndex, int pageSize, List<String> orderbyfields);

	/** Interface method to get a charge for a charge code **/
	public Charge getCharge(String ChargeCode);

	/** Interface method to get a charge for a chargeSearchCriteria **/
	public Charge getCharge(ChargeSearchCriteria chargeSearchCriteria);

	/** Interface method to save a charge **/
	public void saveCharge(Charge charge);

	/** Interface method when removing a charge **/
	public void removeCharge(String ChargeCode);

	/** Interface method when removing a charge **/
	public void removeCharge(String ChargeCode, long version);

	/** Interface method when removing a charge **/
	public void deleteCharge(Charge charge);

	/** Interface method to get a Page of ONDChareges passing a criteria **/
	public Page getONDCharges(ONDChargeSearchCriteria criteria, int startIndex, int pageSize, List<String> orderBy);

	/** Interface method to get Global Charges **/
	public Collection<Charge> getGlobalCharges();

	/** Interface method to get a list of ChargeTO's **/
	public List<ChargeTO> getRefundableCharges(Collection<Integer> chargeRateIds);

	/**
	 * Return active charges based on charge group codes
	 * 
	 * @param colChargeGrpCodes
	 * @return
	 */
	public Collection<Charge> getActiveCharges(Collection<String> colChargeGrpCodes);

	/**
	 * Get ChargeTemplate
	 * 
	 * @param templateId
	 * @return ChargeTemplate
	 */
	public ChargeTemplate getChargeTemplate(int templateId);

	/**
	 * Get ChargeTemplates based on criteria
	 * 
	 * @param criteria
	 * @param startRec
	 * @param noRecs
	 * @return Page
	 * @throws CommonsDataAccessException
	 */
	public Page<ChargeTemplate> getSeatCharges(ChargeTemplateSearchCriteria criteria, int startRec, int noRecs);

	/**
	 * Save Charge Template
	 * 
	 * @param chargeTemplate
	 */
	public void saveTemplate(ChargeTemplate chargeTemplate);

	public ChargeTemplate getTemplateId(String templateCode);

	/**
	 * 
	 * @param criteria
	 * @return
	 * @throws CommonsDataAccessException
	 */
	public Collection<ChargeTemplate> getChargeTemplates(ChargeTemplateSearchCriteria criteria);

	/**
	 * 
	 * @param templateId
	 * @throws ModuleException
	 */
	public void deleteTemplate(ChargeTemplate chargeTemplate);

	/**
	 * 
	 * @param colAMSChaegeIDs
	 * @return
	 * @throws ModuleException
	 */
	public Collection<SeatCharge> getSeatCharges(Collection<Integer> colAMSChaegeIDs);

	public Page<ChargeTemplate> getChargeTemplate(ChargeTemplateSearchCriteria criteria, int startRec, int noRecs);

	/**
	 * Removes the charges pos(s) for the given charge code
	 * 
	 * @param chargeCode
	 */
	public void removeChargePos(String chargeCode);

	/**
	 * Removes the charges ond & Pos agents(s) for the given charge code
	 * 
	 * @param chargeCode
	 */
	public void removeChargeAgents(String chargeCode);

	/**
	 * Removes the charges ond & Pos agents(s) for the given charge code
	 * 
	 * @param chargeCode
	 */
	public void removeApplicableCharges(String chargeCode);

	/**
	 * Save Meal Template
	 * 
	 * @param mealTemplate
	 */
	public void saveMealTemplate(MealTemplate mealTemplate);

	public Page<MealTemplate> getMealTemplates(MealTemplateSearchCriteria criteria, int startRec, int noRecs);

	/**
	 * 
	 * @param MealTemplate
	 *            the MealTemplate
	 * @throws ModuleException
	 */
	public void deleteMealTemplate(MealTemplate mealTemplate);

	public Collection<MealTemplate> getMealTemplates(Integer mealId);

	public MealTemplate getMealTemplate(int templateID) throws CommonsDataAccessException;

	public Collection<MealCharge> getMealCharges(Collection<Integer> colMealChargeIDs) throws CommonsDataAccessException;

	public Collection<ChargeRate> getChargeRatesWithoutLocalCurrency(Date effectiveDate, String baseCurrency);

	public Map<Integer, ChargeTO> getChageRateById(Collection<Integer> chargeRateIds);

	public BaggageTemplate getBaggageTemplate(int templateID) throws CommonsDataAccessException;

	public ONDBaggageTemplate getONDBaggageTemplate(int ondTemplateID) throws CommonsDataAccessException;

	public Page<BaggageTemplate> getBaggageTemplates(BaggageTemplateSearchCriteria criteria, int startRec, int noRecs);

	public void saveBaggageTemplate(BaggageTemplate mealTemplate);

	public void deleteBaggageTemplate(BaggageTemplate baggageTemplate);

	public Page getONDBaggageTemplates(BaggageTemplateSearchCriteria criteria, int startRec, int noRecs);

	public void saveONDBaggageTemplate(ONDBaggageTemplate baggageTemplate);

	public void deleteONDBaggageTemplate(ONDBaggageTemplate baggageTemplate);

	public boolean isActiveONDCodeExistForBaggageTemplate(int templateId, Date fromDate, Date toDate);

	public Page getONDCodes(ONDCodeSearchCriteria criteria, int startRec, int noRecs);

	public Page searchBaggageTemplates(ONDCodeSearchCriteria criteria, int startRec, int noRecs);

	public void saveONDCodes(ONDBaggageChargeTemplate baggageChargeTemplate);

	public void deleteONDCodes(ONDBaggageChargeTemplate baggageChargeTemplate);

	public boolean isONDCodeExist(String ondCode, int templateId);

	public ONDBaggage saveONDBaggage(ONDBaggage ondBaggage);

	public Collection<BaggageTemplate> getBaggageTemplatesCollect(Integer baggageId) throws ModuleException;

	public Collection<BaggageCharge> getBaggageCharges(Collection<Integer> colBaggageChargeIDs) throws CommonsDataAccessException;

	public Page<SSRCharge> getSSRCharges(SSRChargesSearchCriteria criteria, int startRec, int noRecs);

	public SSRCharge getSSRCharges(int chargeId) throws CommonsDataAccessException;

	public SSRCharge getSSRChargesBySSRId(int ssrId) throws CommonsDataAccessException;

	public void saveSSRCharges(SSRCharge ssrCharge) throws ModuleException;
	
	public void saveSSRChargeswithoutDeletingCos(SSRCharge ssrCharge) throws ModuleException;

	public void deleteSSRCharges(SSRCharge ssrCharge);

	public boolean checkSSRChargeAlreadyDefined(int ssrId, String ssrChargeCode, int chargeId, ArrayList<String> ccList,
			ArrayList<String> lccList) throws CommonsDataAccessException;

	// JIRA 6929
	public void saveReportChargeGroup(ReportChargeGroup reportChargeGroup) throws ModuleException;

	public void deleteReportChargeGroup(ReportChargeGroup reportChargeGroup) throws ModuleException;

	public ReportChargeGroup getReportChargeGroupByRptChargeGroupCode(String reportChargeGroupCode)
			throws CommonsDataAccessException;

	public List<Charge> getApplicableChargesForUpdateOnExchangeRateChange(String currencyCode);

	public List<Charge> getExcludableCharges() throws CommonsDataAccessException;

	public List<MCOServices> getMCOServices() throws ModuleException;

	public void saveMCO(MCO mco) throws ModuleException;

	public HashMap<String, Object> getMCORecord(String mcoNumber, String agentCode) throws CommonsDataAccessException;

	public String getNextMCONumberSQID() throws ModuleException;

	public MCO getMCO(String mcoNumber) throws ModuleException;

	public List<HashMap<String, Object>> getMCORecords(String mcoNumber, Integer pnrPaxID) throws CommonsDataAccessException;

	public boolean hasMCORecords(Integer pnrPaxID) throws CommonsDataAccessException;

	public List<String> getAllActiveServiceTaxChargeCodes() throws CommonsDataAccessException;

	public Collection<Charge> getCharges(ChargeSearchCriteria chargeSearchCriteria) throws CommonsDataAccessException;

	public Map<String,Boolean> getChargeRefundabilityMap(List<String> chargeCodes) throws ModuleException;
	
	public Boolean isEnableLMSCreditBlock(int tempTransactionId) throws CommonsDataAccessException;
	public List<ONDBaggageTemplate> getAffectedONDBaggageTemplates(String currencyCode) throws ModuleException;
	public List<MealTemplate> getAffectedMealTemplates(String currencyCode) throws ModuleException;
	public List<BaggageTemplate> getAffectedBaggageTemplates(String currencyCode) throws ModuleException;
	public List<SSRCharge> getAffectedSSRCharges(String currencyCode) throws ModuleException;
	public List<ChargeTemplate> getAffectedChargeTemplates(String currencyCode) throws ModuleException;
	
	public void removeMealTemplatesCategoryRestrictions(Integer mealTemplateId);
}
