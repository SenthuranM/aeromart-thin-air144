package com.isa.thinair.airpricing.core.persistence.jdbc;

import java.util.Collection;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.JdbcTemplate;

import com.isa.thinair.airpricing.api.dto.SSRChargeDTO;
import com.isa.thinair.airpricing.core.persistence.dao.SSRChargeJdbcDAO;
import com.isa.thinair.airpricing.core.persistence.jdbc.ssrchargejdbcdao.SSRChargeResultSetExtractor;
import com.isa.thinair.airpricing.core.persistence.jdbc.ssrchargejdbcdao.SSRChargeSearchCriteria;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseJdbcDAOSupport;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.CalendarUtil;

public class SSRChargeJdbcDAOImpl extends PlatformBaseJdbcDAOSupport implements SSRChargeJdbcDAO {
	private Log log = LogFactory.getLog(getClass());

	@SuppressWarnings("unchecked")
	@Override
	public Collection<SSRChargeDTO> getAvailableSSRChargeDTOs(SSRChargeSearchCriteria ssrChargeSearchCriteria)
			throws ModuleException {
		Collection<SSRChargeDTO> availableSSRChargeDTOs = null;
		String sql = null;
		String ondCodesInStr = null;
		String effectiveDate = CalendarUtil.formatDate(new Date(), "dd-MMM-yyyy");
		log.debug("getSSRChargeDTOs");

		ondCodesInStr = getSqlInValuesStr(ssrChargeSearchCriteria.getONDPatterns());

		sql = createSSRChargesQuery(ondCodesInStr, ssrChargeSearchCriteria.getAppIndicator());

		if (log.isDebugEnabled()) {
			log.debug(sql);
		}

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		Object[] o = { CommonsConstants.STATUS_ACTIVE, CommonsConstants.STATUS_ACTIVE, CommonsConstants.STATUS_ACTIVE,
				effectiveDate, effectiveDate, "Y" };

		availableSSRChargeDTOs = (Collection<SSRChargeDTO>) jdbcTemplate.query(sql, o, new SSRChargeResultSetExtractor());

		return availableSSRChargeDTOs;

	}

	private static String createSSRChargesQuery(String ondCodesInStr, AppIndicatorEnum appIndicator) {
		String sql = "SELECT inf.ssr_id,inf.ssr_code,inf.ssr_sub_cat_id,"
				+ "chg.ssr_charge_id,chg.ssr_charge_code,chg.description,chg.max_tot_pax,"
				+ "chg.min_tot_pax,chg.max_adt_pax,chg.min_adt_pax,chg.max_chd_pax,"
				+ "chg.min_chd_pax,chg.max_inf_pax,chg.min_inf_pax,chg.charge_amount,scc.cabin_class_code,ond.ond_code,"
				+ "con.apply_adt,con.apply_chd,con.apply_inf,con.apply_segment,con.apply_arrival,con.apply_departure,"
				+ "con.apply_transit,con.min_transit_time_in_mins,con.max_transit_time_in_mins "
				+ "FROM t_ssr_applicability_containts con, t_ssr_applicable_ond ond, t_ssr_category cat,"
				+ "t_ssr_sub_category scat, t_ssr_charge chg, t_ssr_charge_cos scc, t_ssr_info inf "
				+ "WHERE con.ssr_app_con_id = ond.ssr_app_con_id AND inf.ssr_sub_cat_id = scat.ssr_sub_cat_id "
				+ "AND scat.ssr_cat_id = cat.ssr_cat_id AND inf.ssr_id = con.ssr_id AND inf.ssr_id = chg.ssr_id "
				+ "AND chg.ssr_charge_id = scc.ssr_charge_id "
				+ "AND cat.status = ? AND chg.status = ? AND inf.status = ? AND chg.effective_from <=	 to_date(?) "
				+ "AND chg.effective_to >= to_date(?) AND ond.ond_code in (" + ondCodesInStr + ") ";

		if (AppIndicatorEnum.APP_XBE.equals(appIndicator)) {
			sql += "AND inf.show_in_xbe = ? ";
		} else {
			sql += "AND inf.show_in_ibe = ? ";
		}

		sql += "ORDER BY ond.ond_code,inf.ssr_code,chg.ssr_charge_code ";

		return sql;
	}
}
