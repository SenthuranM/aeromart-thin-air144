package com.isa.thinair.airpricing.core.persistence.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airpricing.api.criteria.ONDChargeSearchCriteria;
import com.isa.thinair.airpricing.core.persistence.dao.ONDChargeJdbcDAO;
import com.isa.thinair.airpricing.core.persistence.jdbc.ondchargejdbcdao.OndChargesResultsExtractor;
import com.isa.thinair.airpricing.core.util.PricingUtils;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.core.framework.PlatformBaseJdbcDAOSupport;

/**
 * @author BYORN
 */
public class ONDChargeJdbcDAOImpl extends PlatformBaseJdbcDAOSupport implements ONDChargeJdbcDAO {

	private static final String SQL_OND_CHARGE = "OND_CHARGES";

	private Log log = LogFactory.getLog(ONDChargeJdbcDAOImpl.class);

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Page getONDCharges(ONDChargeSearchCriteria criteria, int startIndex, int pageSize, List<String> orderBy) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());

		// need to get the ond_code and passing it to the exctracter because
		// the extractor has to filter out all the ondcodes that apear as eg: CMB/DOH, CMB/***, ***/DOH will take as one
		// ond code.
		String ondCode = PricingUtils.constructViaPointSearch(criteria.getOriginAirportCode(),
				criteria.getDestinationAirportCode(), criteria.getViaAirportCodes(), criteria.isExactAirportsCombinationOnly());

		String sqlOnd = "select OND_CODE from T_OND where OND_CODE like '" + ondCode + "'";
		Collection ondCodes = (Collection) new JdbcTemplate(getDataSource()).query(sqlOnd, new ResultSetExtractor() {
			Collection<String> ondCodes = new ArrayList<String>();

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					ondCodes.add(rs.getString(1));
				}
				return ondCodes;
			}

		});

		try {

			Collection finalList = new ArrayList();
			Iterator iterator = ondCodes.iterator();
			while (iterator.hasNext()) {
				String oCode = iterator.next().toString();
				String sql = constructSql(criteria, oCode);
				List list = (List) jdbcTemplate.query(sql, new OndChargesResultsExtractor(oCode));
				finalList.addAll(list);
				
				if (log.isDebugEnabled()) {
					log.debug("Get Page of getONDCharges" + criteria.toString());
					log.debug(sql);
				}
			}
			return new Page(0, 1, 1, finalList);
		} catch (Exception e) {
			log.error("Get Page of FgetONDCharges");
			throw new CommonsDataAccessException(e, "airpricing.exec.error", "airpricing.desc");
		}
	}

	private String constructSql(ONDChargeSearchCriteria criteria, String ondCode) {

		String departureCharge = criteria.getOriginAirportCode() + "/***";
		String arrivalCharge = "***/" + criteria.getDestinationAirportCode();

		String sql = getQuery(SQL_OND_CHARGE);

		String sqlCondition = "";

		if (criteria.getOriginAirportCode() != null && criteria.getDestinationAirportCode() != null) {

			sqlCondition = " AND ( " + " oc.ond_code like '" + ondCode + "'" + " or oc.ond_code like '" + departureCharge + "'"
					+ " or oc.ond_code like '" + arrivalCharge + "'" + " or oc.ond_code is null )";

		}

		if (criteria.getPosStation() != null) {
			sqlCondition = sqlCondition + " AND (c.charge_category_value is null or c.charge_category_value like '"
					+ criteria.getPosStation() + "')";
		} else {
			sqlCondition = sqlCondition + " AND (c.charge_category_value is null) ";
		}

		if (criteria.getEffectiveFromDate() != null && criteria.getEffectiveToDate() == null) {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
			String effectiveFromDate = simpleDateFormat.format(criteria.getEffectiveFromDate());
			String sqlDateSearch = " AND ( cr.charge_effective_to_date>=" + "'" + effectiveFromDate + "')";

			sqlCondition = sqlCondition + sqlDateSearch;

		}

		if (criteria.getEffectiveFromDate() == null && criteria.getEffectiveToDate() != null) {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
			String effectiveToDate = simpleDateFormat.format(criteria.getEffectiveToDate());
			String sqlDateSearch = " AND  (cr.charge_effective_from_date <= " + "'" + effectiveToDate + "')";

			sqlCondition = sqlCondition + sqlDateSearch;
		}

		if (criteria.getEffectiveFromDate() != null && criteria.getEffectiveToDate() != null) {

			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
			String effectiveToDate = simpleDateFormat.format(criteria.getEffectiveToDate());
			String effectiveFromDate = simpleDateFormat.format(criteria.getEffectiveFromDate());

			String sqlDateSearch = " AND ((cr.charge_effective_from_date>=" + "'" + effectiveFromDate + "'"
					+ " AND  cr.charge_effective_to_date>=" + "'" + effectiveToDate + "'"
					+ " AND cr.charge_effective_from_date<=" + "'" + effectiveToDate + "')" + " or "
					+ " (cr.charge_effective_from_date<=" + "'" + effectiveFromDate + "'" + " AND cr.charge_effective_to_date<="
					+ "'" + effectiveToDate + "'" + " AND cr.charge_effective_to_date>=" + "'" + effectiveFromDate + "')"
					+ " or " + " (cr.charge_effective_from_date<=" + "'" + effectiveFromDate + "'"
					+ " and cr.charge_effective_to_date>=" + "'" + effectiveToDate + "')" + " or "
					+ " (cr.charge_effective_from_date>=" + "'" + effectiveFromDate + "'" + " and cr.charge_effective_to_date<="
					+ "'" + effectiveToDate + "'))";

			sqlCondition = sqlCondition + sqlDateSearch;
		}

		String query = setStringToPlaceHolderIndex(sql, 0, sqlCondition);
		return query;
		// String finalQuery = setStringToPlaceHolderIndex(query,1,sqlSpecialCondition);
		// String finalQueryCount = setStringToPlaceHolderIndex(queryCount,1,sqlSpecialCondition);
		// return new String[]{finalQuery, finalQueryCount};
	}

}
