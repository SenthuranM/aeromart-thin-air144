package com.isa.thinair.airpricing.core.persistence.hibernate;

import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airpricing.api.model.Proration;
import com.isa.thinair.airpricing.core.persistence.dao.FareProrationDAO;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;

/**
 * FareProrationDAOImpl is the FareProrationDAO implementatiion for hibernate
 * @author chamila
 * @isa.module.dao-impl dao-name="FareProrationDAO"
 */
public class FareProrationDAOImpl extends PlatformBaseHibernateDaoSupport
		implements FareProrationDAO {

	private final Log log = LogFactory.getLog(FareProrationDAOImpl.class);

	@Override
	/**
	 * This method is used ot delete proration collection
	 */
	public void deleteAllProrations(Collection<Proration> prorations) {
		deleteAll(prorations);
		if (log.isDebugEnabled()) {
			log.debug("Finished delete collection");
		}

	}

}
