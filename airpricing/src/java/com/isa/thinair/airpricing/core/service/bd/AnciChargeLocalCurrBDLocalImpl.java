package com.isa.thinair.airpricing.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.airpricing.api.service.AnciChargeLocalCurrBD;

/**
 * @author subash
 */

@Local
public interface AnciChargeLocalCurrBDLocalImpl extends AnciChargeLocalCurrBD {

}
