package com.isa.thinair.airpricing.core.persistence.jdbc.ssrchargejdbcdao;

import java.util.Collection;

import com.isa.thinair.commons.core.util.AppIndicatorEnum;

public class SSRChargeSearchCriteria {
	private Collection<String> oNDPatterns;
	private AppIndicatorEnum appIndicator;

	public Collection<String> getONDPatterns() {
		return this.oNDPatterns;
	}

	public void setONDPatterns(Collection<String> patterns) {
		this.oNDPatterns = patterns;
	}

	public AppIndicatorEnum getAppIndicator() {
		return this.appIndicator;
	}

	public void setAppIndicator(AppIndicatorEnum appIndicator) {
		this.appIndicator = appIndicator;
	}

}
