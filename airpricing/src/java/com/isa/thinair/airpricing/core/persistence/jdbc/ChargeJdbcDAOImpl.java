package com.isa.thinair.airpricing.core.persistence.jdbc;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airinventory.api.dto.baggage.FlightBaggageDTO;
import com.isa.thinair.airinventory.api.dto.meal.FlightMealDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareSummaryDTO;
import com.isa.thinair.airpricing.api.dto.QuoteChargesCriteria;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airpricing.api.dto.QuotedONDChargesDTO;
import com.isa.thinair.airpricing.api.model.BaggageTemplateAgent;
import com.isa.thinair.airpricing.api.model.BaggageTemplateBookingClass;
import com.isa.thinair.airpricing.api.model.BaggageTemplateFlight;
import com.isa.thinair.airpricing.api.model.Charge;
import com.isa.thinair.airpricing.api.model.ChargeRate;
import com.isa.thinair.airpricing.api.model.ONDBaggageChargeTemplate;
import com.isa.thinair.airpricing.api.model.ONDBaggageTemplate;
import com.isa.thinair.airpricing.api.service.AirpricingUtils;
import com.isa.thinair.airpricing.core.persistence.dao.ChargeJdbcDAO;
import com.isa.thinair.airpricing.core.persistence.dao.FareDAO;
import com.isa.thinair.airpricing.core.persistence.jdbc.chargejdbcdao.ChargeQuoteForGroupsResultSetExtractor;
import com.isa.thinair.airpricing.core.persistence.jdbc.chargejdbcdao.ChargeQuotePreparedStmtCreator;
import com.isa.thinair.airpricing.core.persistence.jdbc.chargejdbcdao.ChargeQuoteResultSetExtractor;
import com.isa.thinair.airpricing.core.util.InternalConstants;
import com.isa.thinair.airpricing.core.util.PricingUtils;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.framework.PlatformBaseJdbcDAOSupport;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.commons.core.util.Util;

/**
 * Spring JDBC calls for charges retrieval.
 * 
 * @author Nasly
 */
public class ChargeJdbcDAOImpl extends PlatformBaseJdbcDAOSupport implements ChargeJdbcDAO {

	private final Log log = LogFactory.getLog(getClass());

	/** Query IDs */
	private static final String CHARGE_QUOTE_FOR_OND = "CHARGE_QUOTE_FOR_OND";
	private static final String CHAREG_QUOTE_FOR_GROUPS = "CHARGE_QUOTE_FOR_GROUPS";
	private static final String CHARGE_GROUPS = "CHARGE_GROUPS";
	private static final String CHARGE_RATE = "CHARGE_RATE";
	private static final String REPORTING_CHARGE_GROUP_CODE = "REPORTING_CHARGE_GROUP_CODE";

	@SuppressWarnings("unchecked")
	@Override
	public HashMap<String, HashMap<String, QuotedChargeDTO>> quoteCharges(Collection<QuoteChargesCriteria> criteriaCollection)
			throws ModuleException {
		HashMap<String, HashMap<String, QuotedChargeDTO>> ondChargesMap = new HashMap<String, HashMap<String, QuotedChargeDTO>>();
		

		boolean isChannelWebAndroidOrIOS =false;
		Iterator<QuoteChargesCriteria> criteriaCollectionIt = criteriaCollection.iterator();
		while (criteriaCollectionIt.hasNext()) {
			JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
			QuoteChargesCriteria criteria = criteriaCollectionIt.next();

			isChannelWebAndroidOrIOS = SalesChannelsUtil.isSalesChannelWebOrMobile(criteria.getChannelId());

			String matchChargeCatCodesSQLPart = "";
			if ((criteria.getChargeCatCodes() != null) && (criteria.getChargeCatCodes().length > 0)) {
				matchChargeCatCodesSQLPart = "AND c.charge_category_code IN ("
						+ getSqlInPlaceholders(criteria.getChargeCatCodes()) + ") ";
			}

			String matchChargeGroupCodesSQLPart = "";
			if (criteria.getChargeGroups() != null && criteria.getChargeGroups().length > 0) {
				matchChargeGroupCodesSQLPart = "AND c.charge_group_code IN (" + getSqlInPlaceholders(criteria.getChargeGroups())
						+ ")";
			}
	
			String matchOndPositionSQLPart = " ";
			String transitChargesSQLPart = " ";
			String matchPosStationsSQLPart = " ";
			String matchAgentStationsSQLPart = " ";
			String transitSQLPart = " ";
			String transitOndCodePart = "";
			String excludeArrDeptTaxesPart = "";
			String excludeCookieBasedChargesSQLPart = " ";			
			String serviceTaxQuoteSQLPart = " ";
			
			if (!criteria.isServiceTaxQuote()) {
				
				if (!criteria.isVarianceQuote()) {
					matchOndPositionSQLPart = " AND ( c.SEG_APPLICABLE = ? ) ";
					if (criteria.getOndPosition() != null) {
						if (criteria.getOndPosition().equals(QuoteChargesCriteria.FIRST_AND_LAST_OND)) {
							matchOndPositionSQLPart = " AND (c.SEG_APPLICABLE = ? OR c.SEG_APPLICABLE = ? OR c.SEG_APPLICABLE = ?) ";
						} else if (!criteria.getOndPosition().equals(QuoteChargesCriteria.INBETWEEN_OND)) {
							matchOndPositionSQLPart = " AND (c.SEG_APPLICABLE = ? OR c.SEG_APPLICABLE = ?) ";
						}
					}
				}
				
				if (criteria.isQualifyForShortTransit()) {
					transitChargesSQLPart = " AND ( c.JOURNEY_TYPE <> ? ) ";
				}
				
				if (criteria.getPosAirport() != null) {
					matchPosStationsSQLPart = " (c.charge_code in (select charge_code from t_charge_pos where pos_code='"
							+ criteria.getPosAirport() + "' and apply_status = '" + QuoteChargesCriteria.INCLUDE
							+ "') and c.charge_category_value='" + QuoteChargesCriteria.INCLUDE + "') "
							+ " OR (c.charge_code not in (select charge_code from t_charge_pos where pos_code='"
							+ criteria.getPosAirport() + "' and apply_status = '" + QuoteChargesCriteria.EXCLUDE
							+ "') and c.charge_category_value='" + QuoteChargesCriteria.EXCLUDE + "') OR ";
				} else {
					// AARESAA-11826
					if (isChannelWebAndroidOrIOS) {
						matchPosStationsSQLPart = "c.charge_category_value ='" + QuoteChargesCriteria.EXCLUDE + "' OR ";
					} else {
						// Ideally this should never happen
						throw new ModuleException("airPricing.invalid.chargequote", "airpricing.desc");
					}
				}	
				
				if (criteria.getAgentCode() != null && !criteria.getAgentCode().equals("")) {
					matchAgentStationsSQLPart = " (c.charge_agent_filter = 'I' and (c.charge_code in (select charge_code from t_charge_agent "
							+ " where agent_code='"
							+ criteria.getAgentCode()
							+ "' and apply_status = '"
							+ QuoteChargesCriteria.INCLUDE
							+ "')"
							+ " OR c.charge_code in (select charge_code from t_charge_agent "
							+ " where agent_code='"
							+ criteria.getAgentCode()
							+ "' and apply_status = '"
							+ QuoteChargesCriteria.EXCLUDE
							+ "'))) "
							+ " OR (c.charge_agent_filter = 'E' and (c.charge_code in (select charge_code from t_charge_agent "
							+ " where agent_code='"
							+ criteria.getAgentCode()
							+ "' and apply_status = '"
							+ QuoteChargesCriteria.INCLUDE
							+ "')"
							+ " OR c.charge_code not in (select charge_code from t_charge_agent "
							+ " where agent_code='"
							+ criteria.getAgentCode() + "' and apply_status = '" + QuoteChargesCriteria.EXCLUDE + "'))) OR ";
				} else {
					// AARESAA-11826
					if (isChannelWebAndroidOrIOS) {
						matchAgentStationsSQLPart = "c.charge_agent_filter ='" + QuoteChargesCriteria.EXCLUDE + "' OR ";
					} else {
						// Ideally this should never happen
						throw new ModuleException("airPricing.invalid.chargequote", "airpricing.desc");
					}
				}
				
				if (!criteria.isHasInwardTransit() && !criteria.isHasOutwardTransit() && !criteria.isHasTransitWithinOnd()) { // no_tran
					transitSQLPart = " AND (c.IF_HAS_INWARD_TRANSIT <> ? AND c.IF_HAS_OUTWARD_TRANSIT <> ? AND c.IF_HAS_TRANSIT <> ?)"; // get
																																		// all
																																		// the
																																		// charges
																																		// not
																																		// define
																																		// include
				} else {

					// transitSQLPart =
					// " AND ((c.IF_HAS_INWARD_TRANSIT not in ( ?, ? ) AND c.IF_HAS_OUTWARD_TRANSIT not in ( ?, ? ) AND c.IF_HAS_TRANSIT not in ( ?, ? )) ";
					// // getting
					// other
					// charges
					// not
					// define
					// include/exclude
					// transit

					if (criteria.isHasInwardTransit() && !criteria.isHasOutwardTransit() && !criteria.isHasTransitWithinOnd()) { // inw_tran
						transitSQLPart = " AND ( c.IF_HAS_INWARD_TRANSIT not in ( ?, ? ) ";

						transitSQLPart += " OR ((((c.IF_HAS_INWARD_TRANSIT = ? ) AND ? BETWEEN c.TRANSIT_QUAL_MIN_DURATION AND c.TRANSIT_QUAL_MAX_DURATION) " // Handle
																																								// the
																																								// Include
																																								// scenario
								+ " OR ((c.IF_HAS_INWARD_TRANSIT = ? ) AND ? NOT BETWEEN c.TRANSIT_QUAL_MIN_DURATION AND c.TRANSIT_QUAL_MAX_DURATION))" // Handle
																																						// the
																																						// Exclude
																																						// scenario
								+ " AND c.IF_HAS_OUTWARD_TRANSIT <> ? AND c.IF_HAS_TRANSIT <> ?))";
					} else if (!criteria.isHasInwardTransit() && criteria.isHasOutwardTransit() && !criteria.isHasTransitWithinOnd()) {// out_tran
						transitSQLPart = " AND ( c.IF_HAS_OUTWARD_TRANSIT not in ( ?, ? ) ";

						transitSQLPart += " OR ((((c.IF_HAS_OUTWARD_TRANSIT = ? ) AND ? BETWEEN c.TRANSIT_QUAL_MIN_DURATION AND c.TRANSIT_QUAL_MAX_DURATION) "
								+ " OR ((c.IF_HAS_OUTWARD_TRANSIT = ? ) AND ? NOT BETWEEN c.TRANSIT_QUAL_MIN_DURATION AND c.TRANSIT_QUAL_MAX_DURATION))" // Handle
																																							// the
																																							// Exclude
																																							// scenario
								+ " AND  c.IF_HAS_INWARD_TRANSIT <> ? AND c.IF_HAS_TRANSIT <> ?))";
					} else if (!criteria.isHasInwardTransit() && !criteria.isHasOutwardTransit() && criteria.isHasTransitWithinOnd()) {// ond_tran
						transitSQLPart = " AND (c.IF_HAS_TRANSIT not in ( ?, ? ) ";

						Iterator<Long> ondTranDurIt = criteria.getOndTransitDurList().iterator();
						for (int count = 1; ondTranDurIt.hasNext(); count++) {
							ondTranDurIt.next();
							if (count == 1) {
								transitSQLPart += "OR ((((c.IF_HAS_TRANSIT = ? ) AND (";
							}

							if (count == criteria.getOndTransitDurList().size()) {
								transitSQLPart += "( ? BETWEEN c.TRANSIT_QUAL_MIN_DURATION AND c.TRANSIT_QUAL_MAX_DURATION))) ";
							} else {
								transitSQLPart += "( ? BETWEEN c.TRANSIT_QUAL_MIN_DURATION AND c.TRANSIT_QUAL_MAX_DURATION) OR ";
							}
						}
						Iterator<Long> ondTranDurIt1 = criteria.getOndTransitDurList().iterator();
						for (int count1 = 1; ondTranDurIt1.hasNext(); count1++) { // Handle the Exclude scenario
							ondTranDurIt1.next();
							if (count1 == 1) {
								transitSQLPart += "OR ((c.IF_HAS_TRANSIT = ? ) AND (";
							}

							if (count1 == criteria.getOndTransitDurList().size()) {
								transitSQLPart += "( ? NOT BETWEEN c.TRANSIT_QUAL_MIN_DURATION AND c.TRANSIT_QUAL_MAX_DURATION)))) ";
							} else {
								transitSQLPart += "( ? NOT BETWEEN c.TRANSIT_QUAL_MIN_DURATION AND c.TRANSIT_QUAL_MAX_DURATION) OR ";
							}
						}

						transitSQLPart += " AND  c.IF_HAS_INWARD_TRANSIT <> ? AND c.IF_HAS_OUTWARD_TRANSIT <> ? ))";
					} else if (criteria.isHasInwardTransit() && criteria.isHasOutwardTransit() && !criteria.isHasTransitWithinOnd()) {// inw_tran
																																		// &
																																		// out_tran
						transitSQLPart = " AND ((c.IF_HAS_INWARD_TRANSIT not in ( ?, ? ) "
								+ "AND c.IF_HAS_OUTWARD_TRANSIT not in ( ?, ? ) ) ";

						transitSQLPart += " OR ((((c.IF_HAS_INWARD_TRANSIT = ? ) AND ? BETWEEN c.TRANSIT_QUAL_MIN_DURATION AND c.TRANSIT_QUAL_MAX_DURATION) "
								+ " OR ((c.IF_HAS_INWARD_TRANSIT = ? ) AND ? NOT BETWEEN c.TRANSIT_QUAL_MIN_DURATION AND c.TRANSIT_QUAL_MAX_DURATION))" // Handle
																																						// the
																																						// Exclude
																																						// scenario
								+ " AND (((c.IF_HAS_OUTWARD_TRANSIT = ? ) AND ? BETWEEN c.TRANSIT_QUAL_MIN_DURATION AND c.TRANSIT_QUAL_MAX_DURATION) "
								+ " OR ((c.IF_HAS_OUTWARD_TRANSIT = ? ) AND ? NOT BETWEEN c.TRANSIT_QUAL_MIN_DURATION AND c.TRANSIT_QUAL_MAX_DURATION))" // Handle
																																							// the
																																							// Exclude
																																							// scenario
								+ " AND c.IF_HAS_TRANSIT <> ?))";
					} else if (criteria.isHasInwardTransit() && !criteria.isHasOutwardTransit() && criteria.isHasTransitWithinOnd()) {// inw_tran
																																		// &
																																		// ond_tran
						transitSQLPart = " AND ((c.IF_HAS_INWARD_TRANSIT not in ( ?, ? ) "
								+ " AND c.IF_HAS_TRANSIT not in ( ?, ? )) ";

						transitSQLPart += " OR ((((c.IF_HAS_INWARD_TRANSIT = ? ) AND ? BETWEEN c.TRANSIT_QUAL_MIN_DURATION AND c.TRANSIT_QUAL_MAX_DURATION) "
								+ " OR ((c.IF_HAS_INWARD_TRANSIT = ? ) AND ? NOT BETWEEN c.TRANSIT_QUAL_MIN_DURATION AND c.TRANSIT_QUAL_MAX_DURATION))"; // Handle
																																							// the
																																							// Exclude
																																							// scenario
						Iterator<Long> ondTranDurIt = criteria.getOndTransitDurList().iterator();
						for (int count = 1; ondTranDurIt.hasNext(); count++) {
							ondTranDurIt.next();
							if (count == 1) {
								transitSQLPart += "AND (((c.IF_HAS_TRANSIT = ? ) AND (";
							}

							if (count == criteria.getOndTransitDurList().size()) {
								transitSQLPart += "( ? BETWEEN c.TRANSIT_QUAL_MIN_DURATION AND c.TRANSIT_QUAL_MAX_DURATION))) ";
							} else {
								transitSQLPart += "( ? BETWEEN c.TRANSIT_QUAL_MIN_DURATION AND c.TRANSIT_QUAL_MAX_DURATION) OR ";
							}
						}

						Iterator<Long> ondTranDurIt1 = criteria.getOndTransitDurList().iterator();
						for (int count1 = 1; ondTranDurIt1.hasNext(); count1++) {
							ondTranDurIt1.next();
							if (count1 == 1) {
								transitSQLPart += "OR ((c.IF_HAS_TRANSIT = ? ) AND (";
							}

							if (count1 == criteria.getOndTransitDurList().size()) {
								transitSQLPart += "( ? NOT BETWEEN c.TRANSIT_QUAL_MIN_DURATION AND c.TRANSIT_QUAL_MAX_DURATION)))) ";
							} else {
								transitSQLPart += "( ? NOT BETWEEN c.TRANSIT_QUAL_MIN_DURATION AND c.TRANSIT_QUAL_MAX_DURATION) OR ";
							}
						}
						transitSQLPart += " AND c.IF_HAS_OUTWARD_TRANSIT <> ? ))";
					} else if (!criteria.isHasInwardTransit() && criteria.isHasOutwardTransit() && criteria.isHasTransitWithinOnd()) {// out_tran
																																		// &
																																		// ond_tran
						transitSQLPart = " AND (( c.IF_HAS_OUTWARD_TRANSIT not in ( ?, ? ) AND c.IF_HAS_TRANSIT not in ( ?, ? )) ";

						transitSQLPart += " OR ((((c.IF_HAS_OUTWARD_TRANSIT = ? ) AND ? BETWEEN c.TRANSIT_QUAL_MIN_DURATION AND c.TRANSIT_QUAL_MAX_DURATION) "
								+ " OR ((c.IF_HAS_OUTWARD_TRANSIT = ? ) AND ? NOT BETWEEN c.TRANSIT_QUAL_MIN_DURATION AND c.TRANSIT_QUAL_MAX_DURATION))"; // Handle
																																							// the
																																							// Exclude
																																							// scenario
						Iterator<Long> ondTranDurIt = criteria.getOndTransitDurList().iterator();
						for (int count = 1; ondTranDurIt.hasNext(); count++) {
							ondTranDurIt.next();
							if (count == 1) {
								transitSQLPart += "AND (((c.IF_HAS_TRANSIT = ? ) AND (";
							}

							if (count == criteria.getOndTransitDurList().size()) {
								transitSQLPart += "( ? BETWEEN c.TRANSIT_QUAL_MIN_DURATION AND c.TRANSIT_QUAL_MAX_DURATION))) ";
							} else {
								transitSQLPart += "( ? BETWEEN c.TRANSIT_QUAL_MIN_DURATION AND c.TRANSIT_QUAL_MAX_DURATION) OR ";
							}
						}

						Iterator<Long> ondTranDurIt1 = criteria.getOndTransitDurList().iterator();
						for (int count1 = 1; ondTranDurIt1.hasNext(); count1++) {
							ondTranDurIt1.next();
							if (count1 == 1) {
								transitSQLPart += "OR ((c.IF_HAS_TRANSIT = ? ) AND (";
							}

							if (count1 == criteria.getOndTransitDurList().size()) {
								transitSQLPart += "( ? NOT BETWEEN c.TRANSIT_QUAL_MIN_DURATION AND c.TRANSIT_QUAL_MAX_DURATION)))) ";
							} else {
								transitSQLPart += "( ? NOT BETWEEN c.TRANSIT_QUAL_MIN_DURATION AND c.TRANSIT_QUAL_MAX_DURATION) OR ";
							}
						}

						transitSQLPart += " AND  c.IF_HAS_INWARD_TRANSIT <> ? ))";
					} else if (criteria.isHasInwardTransit() && criteria.isHasOutwardTransit() && criteria.isHasTransitWithinOnd()) { // inw_tran,
																																		// out_tran
																																		// &
																																		// ond_tran
						transitSQLPart = " AND ((c.IF_HAS_INWARD_TRANSIT not in ( ?, ? ) "
								+ "AND c.IF_HAS_OUTWARD_TRANSIT not in ( ?, ? ) AND c.IF_HAS_TRANSIT not in ( ?, ? )) ";

						transitSQLPart += " OR ((((c.IF_HAS_INWARD_TRANSIT = ? ) AND ? BETWEEN c.TRANSIT_QUAL_MIN_DURATION AND c.TRANSIT_QUAL_MAX_DURATION) "
								+ " OR ((c.IF_HAS_INWARD_TRANSIT = ? ) AND ? NOT BETWEEN c.TRANSIT_QUAL_MIN_DURATION AND c.TRANSIT_QUAL_MAX_DURATION))"; // Handle
																																							// the
																																							// Exclude
																																							// scenario
						Iterator<Long> ondTranDurIt = criteria.getOndTransitDurList().iterator();
						for (int count = 1; ondTranDurIt.hasNext(); count++) {
							ondTranDurIt.next();
							if (count == 1) {
								transitSQLPart += "AND (((c.IF_HAS_TRANSIT = ? ) AND (";
							}

							if (count == criteria.getOndTransitDurList().size()) {
								transitSQLPart += "( ? BETWEEN c.TRANSIT_QUAL_MIN_DURATION AND c.TRANSIT_QUAL_MAX_DURATION))) ";
							} else {
								transitSQLPart += "( ? BETWEEN c.TRANSIT_QUAL_MIN_DURATION AND c.TRANSIT_QUAL_MAX_DURATION) OR ";
							}
						}

						Iterator<Long> ondTranDurIt1 = criteria.getOndTransitDurList().iterator();
						for (int count1 = 1; ondTranDurIt1.hasNext(); count1++) {
							ondTranDurIt1.next();
							if (count1 == 1) {
								transitSQLPart += "OR ((c.IF_HAS_TRANSIT = ? ) AND (";
							}

							if (count1 == criteria.getOndTransitDurList().size()) {
								transitSQLPart += "( ? NOT BETWEEN c.TRANSIT_QUAL_MIN_DURATION AND c.TRANSIT_QUAL_MAX_DURATION)))) ";
							} else {
								transitSQLPart += "( ? NOT BETWEEN c.TRANSIT_QUAL_MIN_DURATION AND c.TRANSIT_QUAL_MAX_DURATION) OR ";
							}
						}
						transitSQLPart += " AND (((c.IF_HAS_OUTWARD_TRANSIT = ? ) AND ? BETWEEN c.TRANSIT_QUAL_MIN_DURATION AND c.TRANSIT_QUAL_MAX_DURATION) "
								+ " OR ((c.IF_HAS_OUTWARD_TRANSIT = ? ) AND ? NOT BETWEEN c.TRANSIT_QUAL_MIN_DURATION AND c.TRANSIT_QUAL_MAX_DURATION)))"; // Handle
																																							// the
																																							// Exclude
																																							// scenario
					}
				}
				
				if (criteria.hasTransitAirports()) {
					transitOndCodePart = " OR ( c.AIRPORT_TAX_CATEGORY = 'TRANSIT' AND ( oc.ond_code in (";
					Iterator<String> airports = criteria.getWildCardTransitAirports().iterator();
					int transitAirportCount = 0;
					while (airports.hasNext()) {
						airports.next();
						if (transitAirportCount > 0) {
							transitOndCodePart += ",";
						}
						transitOndCodePart += " ? ";
						transitAirportCount++;
					}
					transitOndCodePart += " ) ) )";
				}
				if (criteria.hasExcludeArrDepTaxAirports()) {
					Iterator<String> airports = criteria.getExcludeArrDepTaxAirports().iterator();
					while (airports.hasNext()) {
						airports.next();
						excludeArrDeptTaxesPart += " AND ( c.AIRPORT_TAX_CATEGORY IS NULL OR "
								+ "(NOT ( c.AIRPORT_TAX_CATEGORY = 'DEP'  AND oc.ond_code LIKE ? ) AND "
								+ "NOT ( c.AIRPORT_TAX_CATEGORY = 'ARR' AND oc.ond_code LIKE ? )) ) ";
					}
				}	
				
				if (AppSysParamsUtil.isCookieBasedSurchargesApplyForIBE()) {
					if (isChannelWebAndroidOrIOS && criteria.isApplyCookieBasedCharges()) {
						excludeCookieBasedChargesSQLPart += "  AND ((c.charge_category_code = 'COOKIE_BASED' AND "
								+ "(c.MIN_FLIGHT_SEARCH_GAP <= " + criteria.getLastSearchGapInDays()
								+ " AND c.MAX_FLIGHT_SEARCH_GAP >= " + criteria.getLastSearchGapInDays()
								+ ")) OR c.charge_category_code <> 'COOKIE_BASED' ) ";
					} else {
						excludeCookieBasedChargesSQLPart = "AND c.charge_category_code <> 'COOKIE_BASED'";
					}
				} else {
					excludeCookieBasedChargesSQLPart = "AND c.charge_category_code <> 'COOKIE_BASED'";
				}
				
				serviceTaxQuoteSQLPart = " AND c.charge_category_code <> 'SERVICE_TAX' ";

			} else {
				serviceTaxQuoteSQLPart += " AND c.charge_category_code = 'SERVICE_TAX' ";
				if (criteria.getServiceTaxDepositeCountryCode() != null) {
					serviceTaxQuoteSQLPart += " AND c.COUNTRY_CODE = '" + criteria.getServiceTaxDepositeCountryCode() + "' ";
					if (criteria.getServiceTaxDepositeStateCode() != null) {
						if (!criteria.isServiceTaxQuoteForNonTktRev()) {
							serviceTaxQuoteSQLPart += " AND c.TICKETING_SERVICE_TAX = '" + QuoteChargesCriteria.CODE_FOR_INCLUDE_TICKETING_SERVICE_TAX + "' ";
							if (!criteria.getServiceTaxDepositeCountryCode().equals(
									criteria.getServiceTaxPlaceOfSupplyCountryCode())
									|| (criteria.getServiceTaxDepositeStateCode().equals(criteria
											.getServiceTaxPlaceOfSupplyStateCode()))) {
								serviceTaxQuoteSQLPart += " and ((s.state_code = '" + criteria.getServiceTaxDepositeStateCode()
										+ "' and s.country_code = '" + criteria.getServiceTaxDepositeCountryCode()
										+ "' and c.inter_state = '" + QuoteChargesCriteria.CODE_FOR_EXCLUDE_INTER_STATE_TAX
										+ "') OR c.state_id is null) ";
							} else {
								if (criteria.getServiceTaxPlaceOfSupplyStateCode() != null)
									serviceTaxQuoteSQLPart += " and (s.state_code = '"
											+ criteria.getServiceTaxPlaceOfSupplyStateCode() + "' and s.country_code = '"
											+ criteria.getServiceTaxDepositeCountryCode() + "' and c.inter_state = '"
											+ QuoteChargesCriteria.CODE_FOR_INCLUDE_INTER_STATE_TAX + "') ";
							}
						} else {
							serviceTaxQuoteSQLPart += " AND c.TICKETING_SERVICE_TAX = '" + QuoteChargesCriteria.CODE_FOR_EXCLUDE_TICKETING_SERVICE_TAX + "' ";
							if (!criteria.getServiceTaxDepositeCountryCode().equals(
									criteria.getServiceTaxPlaceOfSupplyCountryCode())) {
								serviceTaxQuoteSQLPart += " and (s.state_code = '" + criteria.getServiceTaxDepositeStateCode()
										+ "' and s.country_code = '" + criteria.getServiceTaxDepositeCountryCode()
										+ "' and c.inter_state = '" + QuoteChargesCriteria.CODE_FOR_INCLUDE_INTER_STATE_TAX
										+ "') ";
							} else if (criteria.getServiceTaxDepositeStateCode().equals(
									criteria.getServiceTaxPlaceOfSupplyStateCode())) {
								serviceTaxQuoteSQLPart += " and ((s.state_code = '" + criteria.getServiceTaxDepositeStateCode()
										+ "' and s.country_code = '" + criteria.getServiceTaxDepositeCountryCode()
										+ "' and c.inter_state = '" + QuoteChargesCriteria.CODE_FOR_EXCLUDE_INTER_STATE_TAX
										+ "') OR c.state_id is null) ";
							} else {
								serviceTaxQuoteSQLPart += " and (s.state_code = '"
										+ criteria.getServiceTaxPlaceOfSupplyStateCode() + "' and s.country_code = '"
										+ criteria.getServiceTaxDepositeCountryCode() + "' and c.inter_state = '"
										+ QuoteChargesCriteria.CODE_FOR_INCLUDE_INTER_STATE_TAX + "') ";
							}
						}
					}
				}
			}


			String matchingDepartureDateRangeSQLPart = " ";
			if (!criteria.isVarianceQuote()) {
				matchingDepartureDateRangeSQLPart = " AND ? BETWEEN cr.charge_effective_from_date AND cr.charge_effective_to_date ";
			} else {
				matchingDepartureDateRangeSQLPart = "  AND( (? between cr.charge_effective_from_date And cr.charge_effective_to_date AND "
						+ " ? between cr.charge_effective_from_date And cr.charge_effective_to_date) "
						+ " OR  (? between cr.charge_effective_from_date And cr.charge_effective_to_date AND "
						+ " ? > cr.charge_effective_to_date) "
						+ " OR (? < cr.charge_effective_from_date  AND "
						+ " ? between cr.charge_effective_from_date And cr.charge_effective_to_date) "
						+ " OR  (cr.charge_effective_from_date between ? AND ? AND "
						+ "  cr.charge_effective_to_date between ? AND ?)) ";
			}

			matchingDepartureDateRangeSQLPart += " AND ? BETWEEN cr.sales_valid_from AND cr.sales_valid_to ";

			String channelSpecificCharges = ") ";
			if (criteria.getChannelId() != -1) {
				channelSpecificCharges = " OR c.channel_id = " + criteria.getChannelId() + ") ";
			}

			String excludedCharges = "";
			if (criteria.getExcludedCharges() != null && !criteria.getExcludedCharges().isEmpty()) {
				excludedCharges = " AND c.charge_code NOT IN(" + Util.buildStringInClauseContent(criteria.getExcludedCharges())
						+ ") ";
			}

			StringBuilder bundledFaresExclude = new StringBuilder("AND (c.bundled_fare_charge='N'");
			if (criteria.getBundledFareChargeCodes() != null && !criteria.getBundledFareChargeCodes().isEmpty()) {
				bundledFaresExclude.append("  OR c.charge_code IN ( "
						+ Util.buildStringInClauseContent(criteria.getBundledFareChargeCodes()) + ") ");
			}
			bundledFaresExclude.append(") ");

			String query = getQuery(CHARGE_QUOTE_FOR_OND, new String[] { matchChargeCatCodesSQLPart,
					matchChargeGroupCodesSQLPart, matchOndPositionSQLPart, transitChargesSQLPart, matchPosStationsSQLPart,
					matchAgentStationsSQLPart, transitSQLPart, matchingDepartureDateRangeSQLPart, transitOndCodePart,
					excludeArrDeptTaxesPart, channelSpecificCharges, excludedCharges, excludeCookieBasedChargesSQLPart,
					bundledFaresExclude.toString(), serviceTaxQuoteSQLPart });

			// get ond code from ondFare if fareId is set
			if ((criteria.getOnd() == null || criteria.getOnd().equals("")) && (criteria.getFareSummaryDTO() != null)) {
				criteria.setOnd(getFareDAO().getFare(criteria.getFareSummaryDTO().getFareId()).getOndCode());
			}

			if (criteria.getCabinClassCode() == null && criteria.getFareSummaryDTO() != null) {
				// TODO : ideally at this point we must have cabin class set if not throw an exception.
				// Did this for mahan changes and add this to avoid breaking other flows
				criteria.setCabinClassCode(AirpricingUtils.getBookingClassBD().getCabinClassForBookingClass(
						criteria.getFareSummaryDTO().getBookingClassCode()));
			}

			if (log.isDebugEnabled())
				log.debug(criteria.getSummary());

			ondChargesMap.put(criteria.getOnd(), (HashMap<String, QuotedChargeDTO>) jdbcTemplate.query(
					new ChargeQuotePreparedStmtCreator(query, criteria), new ChargeQuoteResultSetExtractor(criteria)));
		}
		return ondChargesMap;
	}

	@SuppressWarnings("unchecked")
	@Override
	public HashMap<String, QuotedChargeDTO> rebuildQuoteCharges(Collection<Integer> chargeRateIds, QuoteChargesCriteria criteria) {
		log.debug("rebuildQuoteCharges");

		String inStr = Util.buildIntegerInClauseContent(chargeRateIds);

		String sql = "SELECT C.CHARGE_CODE, "
				+ "  C.CHARGE_DESCRIPTION , "
				+ "  C.CHARGE_GROUP_CODE, "
				+ " C.MIN_FLIGHT_SEARCH_GAP, "
				+ " C.MAX_FLIGHT_SEARCH_GAP, "
				+ "  C.CHARGE_CATEGORY_CODE, "
				+ "  C.APPLIES_TO, "
				+ "  R.CHARGE_RATE_ID, "
				+ "  R.VALUE_PERCENTAGE_FLAG, "
				+ "  R.VALUE_IN_LOCAL_CURRENCY, R.CURRENCY_CODE AS LOCAL_CURRENCY_CODE, "
				+ "  R.CHARGE_VALUE_PERCENTAGE, "
				+ "  R.CHARGE_VALUE_MAX, "
				+ "  R.CHARGE_VALUE_MIN, "
				+ "  R.CHARGE_BASIS, "
				+ "  C.SEG_APPLICABLE, C.REFUNDABLE_CHARGE_FLAG, C.JOURNEY_TYPE, R.CHARGE_EFFECTIVE_FROM_DATE, R.CHARGE_EFFECTIVE_TO_DATE, "
				+ "  R.BREAK_POINT, R.BOUNDRY_VALUE, RCG.RPT_CHARGE_GROUP_CODE, C.bundled_fare_charge as bundled_fare_charge " +

				"FROM T_CHARGE_RATE R, " + "  T_CHARGE C, T_RPT_CHARGE_GROUP RCG " + "WHERE R.CHARGE_CODE   = C.CHARGE_CODE "
				+ "AND C.RPT_CHARGE_GROUP_ID = RCG.RPT_CHARGE_GROUP_ID(+)" + "AND R.CHARGE_RATE_ID IN (" + inStr + ")";

		if (log.isDebugEnabled()) {
			log.debug("rebuildQuoteCharges sql [" + sql + "]");
		}

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		HashMap<String, QuotedChargeDTO> quotedChargeMap = (HashMap<String, QuotedChargeDTO>) jdbcTemplate.query(sql,
				new ChargeQuoteResultSetExtractor(criteria));

		return quotedChargeMap;
	}

	@Override
	public QuotedONDChargesDTO quoteONDCharges(Collection<QuoteChargesCriteria> criteriaCollection) throws ModuleException {
		QuotedONDChargesDTO quotedOndCharges = new QuotedONDChargesDTO();
		quotedOndCharges.setOndWiseQuotedChargesMaps(quoteCharges(criteriaCollection));
		HashMap<String, FareSummaryDTO> ondFaresMap = new HashMap<String, FareSummaryDTO>();
		Iterator<QuoteChargesCriteria> criteriaCollectionIt = criteriaCollection.iterator();
		while (criteriaCollectionIt.hasNext()) {
			QuoteChargesCriteria criteria = criteriaCollectionIt.next();

			// FIXME Fare Per Pax Type ///DONE -Indika
			ondFaresMap.put(criteria.getOnd(), criteria.getFareSummaryDTO());
			quotedOndCharges.addFareIdOndCode(criteria.getFareSummaryDTO().getFareId(), criteria.getOnd());
		}
		quotedOndCharges.setOndFaresMap(ondFaresMap);

		return quotedOndCharges;
	}

	@SuppressWarnings("unchecked")
	@Override
	public HashMap<String, QuotedChargeDTO> quoteUniqueChargeForGroups(Collection<String> chargeGroups) {
		log.debug("quoteUniqueChargeForGroups");
		if (chargeGroups == null || chargeGroups.size() == 0) {
			throw new CommonsDataAccessException("airpricing.technical.error");
		}

		String query = getQuery(CHAREG_QUOTE_FOR_GROUPS);
		String sql = setStringToPlaceHolderIndex(query, 0, PricingUtils.constructINString(chargeGroups));
		if (log.isDebugEnabled()) {
			log.debug(sql);
		}
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		Object[] o = { ChargeRate.Status_Active, new Date() };
		return (HashMap<String, QuotedChargeDTO>) jdbcTemplate.query(sql, o, new ChargeQuoteForGroupsResultSetExtractor());
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Integer> getSegmentIdsForChargeTemplate(int templateId) {

		String query = "SELECT stfas.flt_seg_id  FROM sm_t_flight_am_seat stfas,  sm_t_am_seat_charge stasc, t_flight_segment fs,"
				+ " t_flight f where stfas.AMS_CHARGE_ID = stasc.AMS_CHARGE_ID and stasc.AMC_TEMPLATE_ID = "
				+ templateId
				+ " and fs.flight_id = f.flight_id and f.status <> 'CNX' and fs.status='OPN' "
				+ "and fs.EST_TIME_DEPARTURE_ZULU > sysdate and fs.flt_seg_id = stfas.flt_seg_id GROUP BY stfas.flt_seg_id";
		log.debug(query);

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		return (Collection<Integer>) jdbcTemplate.query(query, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<Integer> colSegIds = new ArrayList<Integer>();

				if (rs != null) {
					while (rs.next()) {
						colSegIds.add(new Integer(BeanUtils.nullHandler(rs.getInt("flt_seg_id"))));
					}
				}

				return colSegIds;
			}
		});
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<FlightMealDTO> getSegmentMealsForMealTemplate(int templateId) {
		String query = "SELECT a.flt_seg_id, a.cabin_class_code,a.logical_cabin_class_code FROM ML_T_FLIGHT_MEAL_CHARGE a"
				+ " where MEAL_CHARGE_ID in(select MEAL_CHARGE_ID from ML_T_MEAL_CHARGE "
				+ " where MEAL_TEMPLATE_ID IN ("
				+ templateId
				+ ")) and a.status = 'ACT' and " 
				+ " a.flt_seg_id in(select flt_seg_id from t_flight_segment"
				+ " where status='OPN' and EST_TIME_DEPARTURE_ZULU>sysdate)"
				+ " GROUP BY a.flt_seg_id, a.cabin_class_code, a.logical_cabin_class_code";
		log.debug(query);

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		return (Collection<FlightMealDTO>) jdbcTemplate.query(query, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<FlightMealDTO> colFltMls = new ArrayList<FlightMealDTO>();

				if (rs != null) {
					while (rs.next()) {
						FlightMealDTO fltMlDto = new FlightMealDTO();
						fltMlDto.setFlightSegmentID(new Integer(BeanUtils.nullHandler(rs.getInt("flt_seg_id"))));
						fltMlDto.setCabinClassCode(BeanUtils.nullHandler(rs.getString("cabin_class_code")));
						fltMlDto.setLogicalCCCode(BeanUtils.nullHandler(rs.getString("logical_cabin_class_code")));
						colFltMls.add(fltMlDto);
					}
				}

				return colFltMls;
			}
		});
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<FlightBaggageDTO> getSegmentBaggagesForBaggageTemplate(int templateId) {
		String query = "SELECT a.flt_seg_id, a.cabin_class_code "
				+ " FROM BG_T_FLT_SEG_BAGGAGE_CHARGE a "
				+ " where a.BAGGAGE_CHARGE_ID in(select BAGGAGE_CHARGE_ID from BG_T_BAGGAGE_CHARGE where BAGGAGE_TEMPLATE_ID IN ("
				+ templateId
				+ "))and " // TODO
				+ " a.flt_seg_id in(select flt_seg_id from t_flight_segment"
				+ " where status='OPN' and EST_TIME_DEPARTURE_ZULU>sysdate)" + " GROUP BY a.flt_seg_id, a.cabin_class_code";
		log.debug(query);

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		return (Collection<FlightBaggageDTO>) jdbcTemplate.query(query, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<FlightBaggageDTO> colFltBgs = new ArrayList<FlightBaggageDTO>();

				if (rs != null) {
					while (rs.next()) {
						FlightBaggageDTO fltBGDto = new FlightBaggageDTO();
						fltBGDto.setFlightSegmentID(new Integer(BeanUtils.nullHandler(rs.getInt("flt_seg_id"))));
						fltBGDto.setCabinClassCode(BeanUtils.nullHandler(rs.getString("cabin_class_code")));
						colFltBgs.add(fltBGDto);
					}
				}

				return colFltBgs;
			}
		});
	}

	private FareDAO getFareDAO() {
		return (FareDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FARE_DAO);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, String> getChargeGroups() {
		String query = getQuery(CHARGE_GROUPS);
		log.debug(query);

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		return (Map<String, String>) jdbcTemplate.query(query, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<String, String> mapGroups = new HashMap<String, String>();

				if (rs != null) {
					while (rs.next()) {
						mapGroups.put(BeanUtils.nullHandler(rs.getString("charge_group_code")),
								BeanUtils.nullHandler(rs.getString("charge_group_desc")));
					}
				}

				return mapGroups;
			}
		});
	}

	/**
	 * Return Active charge with rate applicable for the specified effective date.
	 */
	@Override
	public QuotedChargeDTO getCharge(String chargeCode, Date effectiveDate, FareSummaryDTO fareSummaryDTO,
			Integer operationalMode, boolean checkSalesValidity) {
		String query = getQuery(CHARGE_RATE);
		Object[] params;

		// If any effective date specified
		if (effectiveDate != null) {
			String sqlCondition = " AND ? BETWEEN cr.charge_effective_from_date AND cr.charge_effective_to_date AND cr.applies_to = ? ";
			// to filter the rate by operational mood , make=1 , modify=2 , default=0;
			if (operationalMode == null) {
				operationalMode = 0;
			}

			if (checkSalesValidity) {
				sqlCondition += " AND ? BETWEEN cr.sales_valid_from AND cr.sales_valid_to ";
				params = new Object[] { chargeCode, Charge.STATUS_ACTIVE, ChargeRate.Status_Active, effectiveDate,
						operationalMode.intValue(), effectiveDate };
			} else {
				params = new Object[] { chargeCode, Charge.STATUS_ACTIVE, ChargeRate.Status_Active, effectiveDate,
						operationalMode.intValue() };
			}
			query = setStringToPlaceHolderIndex(query, 0, sqlCondition);

		} else {
			String sqlCondition = " AND cr.applies_to = ? ";
			// to filter the rate by operational mood , make=1 , modify=2 , default=0;
			if (operationalMode == null) {
				operationalMode = 0;
			}
			params = new Object[] { chargeCode, Charge.STATUS_ACTIVE, ChargeRate.Status_Active, operationalMode.intValue() };
			query = setStringToPlaceHolderIndex(query, 0, sqlCondition);
		}

		log.debug(query);
		final FareSummaryDTO fFareSummaryDTO = fareSummaryDTO;
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		return (QuotedChargeDTO) jdbcTemplate.query(query, params, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				QuotedChargeDTO quotedChargeDTO = null;

				if (rs != null) {
					if (rs.next()) {
						quotedChargeDTO = new QuotedChargeDTO();
						String chargeCode = rs.getString("charge_code");
						quotedChargeDTO.setChargeCode(chargeCode);
						quotedChargeDTO.setChargeDescription(rs.getString("charge_description"));
						quotedChargeDTO.setChargeGroupCode(rs.getString("charge_group_code"));
						quotedChargeDTO.setChargeCategoryCode(rs.getString("charge_category_code"));
						// JIRA 6929
						quotedChargeDTO.setReportingChargeGroupCode(rs.getString("rpt_charge_group_code"));
						quotedChargeDTO.setApplicableTo(rs.getInt("applies_to"));

						quotedChargeDTO.setChargeRateId(rs.getInt("charge_rate_id"));
						String isChargePercentage = rs.getString("value_percentage_flag");// should remove when removing
																							// charge value percentage
						String chargeBasis = rs.getString("charge_basis");
						quotedChargeDTO.setValueInLocalCurrency(rs.getDouble("value_in_local_currency"));
						quotedChargeDTO.setLocalCurrencyCode(rs.getString("local_currency_code"));
						/**
						 * Represents the percentage when charge is defined as a percentage otherwise represents the
						 * charge value.
						 */
						BigDecimal valueOrPercentage = rs.getBigDecimal("charge_value_percentage");

						quotedChargeDTO.setChargeRateJourneyType(rs.getInt("RATE_JOURNEY_TYPE"));

						quotedChargeDTO.setChargeValueInPercentage(isChargePercentage != null
								&& isChargePercentage.equals(ChargeRate.CHARGE_BASIS_V) ? false : true);

						quotedChargeDTO.setChargeValuePercentage(valueOrPercentage.doubleValue());

						BigDecimal effectiveChargeAmount = null;
						// Added new two fields to make upper and lower bound to the charge. If calculated value exceeds
						// the boundary it will get those values
						quotedChargeDTO.setChargeValueMin(rs.getBigDecimal("charge_value_min"));
						quotedChargeDTO.setChargeValueMax(rs.getBigDecimal("charge_value_max"));
						// Added new field for calculate the charge. Charge will be calculate base on this value
						quotedChargeDTO.setChargeBasis(chargeBasis);

						quotedChargeDTO.setBreakPoint(rs.getBigDecimal("break_point"));
						quotedChargeDTO.setBoundryValue(rs.getBigDecimal("boundry_value"));
						quotedChargeDTO.setCabinClassCode(rs.getString("cabin_class_code"));

						if (!isChargePercentage.equals(ChargeRate.CHARGE_BASIS_V)) {
							if (fFareSummaryDTO != null) {
								setEffectiveAmount(quotedChargeDTO, PaxTypeTO.ADULT, valueOrPercentage,
										fFareSummaryDTO.getFareAmount(PaxTypeTO.ADULT));
								setEffectiveAmount(quotedChargeDTO, PaxTypeTO.CHILD, valueOrPercentage,
										fFareSummaryDTO.getFareAmount(PaxTypeTO.CHILD));
								setEffectiveAmount(quotedChargeDTO, PaxTypeTO.INFANT, valueOrPercentage,
										fFareSummaryDTO.getFareAmount(PaxTypeTO.INFANT));
								setEffectiveAmount(quotedChargeDTO, QuotedChargeDTO.PaxTypeCharge.RESERVATION_CHARGE,
										valueOrPercentage, fFareSummaryDTO.getFareAmount(PaxTypeTO.ADULT));
							}
						} else {
							effectiveChargeAmount = rs.getBigDecimal("charge_value_percentage");
							quotedChargeDTO.setEffectiveChargeAmount(PaxTypeTO.ADULT, effectiveChargeAmount.doubleValue());
							quotedChargeDTO.setEffectiveChargeAmount(PaxTypeTO.CHILD, effectiveChargeAmount.doubleValue());
							quotedChargeDTO.setEffectiveChargeAmount(PaxTypeTO.INFANT, effectiveChargeAmount.doubleValue());
							quotedChargeDTO.setEffectiveChargeAmount(QuotedChargeDTO.PaxTypeCharge.RESERVATION_CHARGE,
									effectiveChargeAmount.doubleValue());
						}
					}
				}

				return quotedChargeDTO;
			}

			private void setEffectiveAmount(QuotedChargeDTO quotedChargeDTO, String paxType, BigDecimal valueOrPercentage,
					Double fareAmount) {
				BigDecimal bgChargeAmount = AccelAeroCalculator.divide(valueOrPercentage, new BigDecimal(100));

				boolean chargeRateLevelRoundingEnabled = quotedChargeDTO.isChargeRateRoundingApplicable();

				String strRoundEnable = AirpricingUtils.getGlobalConfig().getBizParam(SystemParamKeys.CHARGE_ROUND_ENABLE);
				// the global rounding should happen only when the individual rounding is not configured.
				if (!chargeRateLevelRoundingEnabled && strRoundEnable != null && strRoundEnable.equals("Y")) {
					bgChargeAmount = AccelAeroRounderPolicy.getRoundedValue(bgChargeAmount, new BigDecimal(AirpricingUtils
							.getGlobalConfig().getBizParam(SystemParamKeys.CHARGE_ROUND_ROUND)), new BigDecimal(AirpricingUtils
							.getGlobalConfig().getBizParam(SystemParamKeys.CHARGE_ROUND_BREAKPOINT)));
				}
				Double calculatedEffectiveVal = AccelAeroCalculator.multiply(AccelAeroCalculator.parseBigDecimal(fareAmount),
						bgChargeAmount).doubleValue();

				if (chargeRateLevelRoundingEnabled) {
					calculatedEffectiveVal = AccelAeroRounderPolicy.getRoundedValue(BigDecimal.valueOf(calculatedEffectiveVal),
							quotedChargeDTO.getBoundryValue(), quotedChargeDTO.getBreakPoint()).doubleValue();
				}

				BigDecimal defineChargeValueMin = quotedChargeDTO.getChargeValueMin();
				BigDecimal defineChargeValueMax = quotedChargeDTO.getChargeValueMax();
				Double effectiveChargeAmount = calculatedEffectiveVal;
				if ((defineChargeValueMin != null) && (calculatedEffectiveVal < defineChargeValueMin.doubleValue())) {
					effectiveChargeAmount = defineChargeValueMin.doubleValue();
				} else if ((defineChargeValueMax != null) && (calculatedEffectiveVal > defineChargeValueMax.doubleValue())) {
					effectiveChargeAmount = defineChargeValueMax.doubleValue();
				}
				quotedChargeDTO.setEffectiveChargeAmount(paxType, effectiveChargeAmount);
			}
		});
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<QuotedChargeDTO> getCharges(String chargeCode, Date effectiveDate, FareSummaryDTO fareSummaryDTO,
			Integer operationalMode) {
		String query = getQuery(CHARGE_RATE);
		Object[] params;

		// If any effective date specified
		if (effectiveDate != null) {
			String sqlCondition = " AND ? BETWEEN cr.charge_effective_from_date AND cr.charge_effective_to_date AND cr.applies_to = ? ";
			// to filter the rate by operational mood , make=1 , modify=2 , default=0;
			if (operationalMode == null) {
				operationalMode = 0;
			}
			params = new Object[] { chargeCode, Charge.STATUS_ACTIVE, ChargeRate.Status_Active, effectiveDate,
					operationalMode.intValue() };
			query = setStringToPlaceHolderIndex(query, 0, sqlCondition);

		} else {
			String sqlCondition = " AND cr.applies_to = ? ";
			// to filter the rate by operational mood , make=1 , modify=2 , default=0;
			if (operationalMode == null) {
				operationalMode = 0;
			}
			params = new Object[] { chargeCode, Charge.STATUS_ACTIVE, ChargeRate.Status_Active, operationalMode.intValue() };
			query = setStringToPlaceHolderIndex(query, 0, sqlCondition);
		}

		log.debug(query);
		final FareSummaryDTO fFareSummaryDTO = fareSummaryDTO;
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		return (List<QuotedChargeDTO>) jdbcTemplate.query(query, params, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<QuotedChargeDTO> quotedChargeDTOs = new ArrayList<QuotedChargeDTO>();
				QuotedChargeDTO quotedChargeDTO = null;

				if (rs != null) {
					while (rs.next()) {
						quotedChargeDTO = new QuotedChargeDTO();
						String chargeCode = rs.getString("charge_code");
						quotedChargeDTO.setChargeCode(chargeCode);
						quotedChargeDTO.setChargeDescription(rs.getString("charge_description"));
						quotedChargeDTO.setChargeGroupCode(rs.getString("charge_group_code"));
						quotedChargeDTO.setChargeCategoryCode(rs.getString("charge_category_code"));
						// JIRA 6929
						quotedChargeDTO.setReportingChargeGroupCode(rs.getString("rpt_charge_group_code"));
						quotedChargeDTO.setApplicableTo(rs.getInt("applies_to"));

						quotedChargeDTO.setChargeRateId(rs.getInt("charge_rate_id"));
						String isChargePercentage = rs.getString("value_percentage_flag");// should remove when removing
																							// charge value percentage
						String chargeBasis = rs.getString("charge_basis");
						quotedChargeDTO.setValueInLocalCurrency(rs.getDouble("value_in_local_currency"));
						quotedChargeDTO.setLocalCurrencyCode(rs.getString("local_currency_code"));
						/**
						 * Represents the percentage when charge is defined as a percentage otherwise represents the
						 * charge value.
						 */
						BigDecimal valueOrPercentage = rs.getBigDecimal("charge_value_percentage");

						quotedChargeDTO.setChargeValueInPercentage(isChargePercentage != null
								&& isChargePercentage.equals(ChargeRate.CHARGE_BASIS_V) ? false : true);

						quotedChargeDTO.setChargeValuePercentage(valueOrPercentage.doubleValue());

						BigDecimal effectiveChargeAmount = null;

						quotedChargeDTO.setChargeRateJourneyType(rs.getInt("RATE_JOURNEY_TYPE"));
						// Added new two fields to make upper and lower bound to the charge. If calculated value exceeds
						// the boundary it will get those values
						quotedChargeDTO.setChargeValueMin(rs.getBigDecimal("charge_value_min"));
						quotedChargeDTO.setChargeValueMax(rs.getBigDecimal("charge_value_max"));
						// Added new field for calculate the charge. Charge will be calculate base on this value
						quotedChargeDTO.setChargeBasis(chargeBasis);

						quotedChargeDTO.setBreakPoint(rs.getBigDecimal("break_point"));
						quotedChargeDTO.setBoundryValue(rs.getBigDecimal("boundry_value"));
						quotedChargeDTO.setCabinClassCode(rs.getString("cabin_class_code"));

						if (!isChargePercentage.equals(ChargeRate.CHARGE_BASIS_V)) {
							if (fFareSummaryDTO != null) {
								setEffectiveAmount(quotedChargeDTO, PaxTypeTO.ADULT, valueOrPercentage,
										fFareSummaryDTO.getFareAmount(PaxTypeTO.ADULT));
								setEffectiveAmount(quotedChargeDTO, PaxTypeTO.CHILD, valueOrPercentage,
										fFareSummaryDTO.getFareAmount(PaxTypeTO.CHILD));
								setEffectiveAmount(quotedChargeDTO, PaxTypeTO.INFANT, valueOrPercentage,
										fFareSummaryDTO.getFareAmount(PaxTypeTO.INFANT));
								setEffectiveAmount(quotedChargeDTO, QuotedChargeDTO.PaxTypeCharge.RESERVATION_CHARGE,
										valueOrPercentage, fFareSummaryDTO.getFareAmount(PaxTypeTO.ADULT));
							}
						} else {
							effectiveChargeAmount = rs.getBigDecimal("charge_value_percentage");
							quotedChargeDTO.setEffectiveChargeAmount(PaxTypeTO.ADULT, effectiveChargeAmount.doubleValue());
							quotedChargeDTO.setEffectiveChargeAmount(PaxTypeTO.CHILD, effectiveChargeAmount.doubleValue());
							quotedChargeDTO.setEffectiveChargeAmount(PaxTypeTO.INFANT, effectiveChargeAmount.doubleValue());
							quotedChargeDTO.setEffectiveChargeAmount(QuotedChargeDTO.PaxTypeCharge.RESERVATION_CHARGE,
									effectiveChargeAmount.doubleValue());
						}
						quotedChargeDTOs.add(quotedChargeDTO);
					}
				}

				return quotedChargeDTOs;
			}

			private void setEffectiveAmount(QuotedChargeDTO quotedChargeDTO, String paxType, BigDecimal valueOrPercentage,
					Double fareAmount) {
				BigDecimal bgChargeAmount = AccelAeroCalculator.divide(valueOrPercentage, new BigDecimal(100));

				boolean chargeRateLevelRoundingEnabled = quotedChargeDTO.isChargeRateRoundingApplicable();
				String strRoundEnable = AirpricingUtils.getGlobalConfig().getBizParam(SystemParamKeys.CHARGE_ROUND_ENABLE);
				// the global rounding should happen only when the individual rounding is not configured.
				if (!chargeRateLevelRoundingEnabled && strRoundEnable != null && strRoundEnable.equals("Y")) {
					bgChargeAmount = AccelAeroRounderPolicy.getRoundedValue(bgChargeAmount, new BigDecimal(AirpricingUtils
							.getGlobalConfig().getBizParam(SystemParamKeys.CHARGE_ROUND_ROUND)), new BigDecimal(AirpricingUtils
							.getGlobalConfig().getBizParam(SystemParamKeys.CHARGE_ROUND_BREAKPOINT)));
				}
				Double calculatedEffectiveVal = AccelAeroCalculator.multiply(AccelAeroCalculator.parseBigDecimal(fareAmount),
						bgChargeAmount).doubleValue();

				if (chargeRateLevelRoundingEnabled) {
					calculatedEffectiveVal = AccelAeroRounderPolicy.getRoundedValue(BigDecimal.valueOf(calculatedEffectiveVal),
							quotedChargeDTO.getBoundryValue(), quotedChargeDTO.getBreakPoint()).doubleValue();
				}

				BigDecimal defineChargeValueMin = quotedChargeDTO.getChargeValueMin();
				BigDecimal defineChargeValueMax = quotedChargeDTO.getChargeValueMax();
				Double effectiveChargeAmount = calculatedEffectiveVal;
				if ((defineChargeValueMin != null) && (calculatedEffectiveVal < defineChargeValueMin.doubleValue())) {
					effectiveChargeAmount = defineChargeValueMin.doubleValue();
				} else if ((defineChargeValueMax != null) && (calculatedEffectiveVal > defineChargeValueMax.doubleValue())) {
					effectiveChargeAmount = defineChargeValueMax.doubleValue();
				}
				quotedChargeDTO.setEffectiveChargeAmount(paxType, effectiveChargeAmount);
			}
		});
	}

	// JIRA 6929
	public String getReportingChargeGroupCodeByChargeRateID(Integer chargeRateId) {
		String query = getQuery(REPORTING_CHARGE_GROUP_CODE);
		Object[] params = new Object[] { chargeRateId };
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		return (String) jdbcTemplate.query(query, params, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				String reportingChargeGroupCode = null;

				if (rs != null) {
					if (rs.next()) {
						reportingChargeGroupCode = BeanUtils.nullHandler(rs.getString("rpt_charge_group_code"));
					}
				}

				return reportingChargeGroupCode;
			}
		});
	}

	public boolean isChargeAssignedForPnr(Integer chargeRateId) {
		StringBuilder sql = new StringBuilder();

		sql.append("SELECT count(*) as cnt FROM t_pnr_pax_ond_charges WHERE charge_rate_id=" + chargeRateId);

		JdbcTemplate template = new JdbcTemplate(getDataSource());

		Integer recs = (Integer) template.query(sql.toString(), new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					return new Integer(rs.getInt("cnt"));
				}
				return null;

			}
		});

		if (recs != null && recs.intValue() > 0) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean isValidChargeExists(String chargeCode, String ondCode) throws CommonsDataAccessException {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT COUNT(DISTINCT c.charge_code) CNT FROM t_charge c, t_ond_charge oc ");
		sql.append("WHERE c.charge_code=oc.charge_code AND c.charge_code=? ");
		sql.append("AND c.status=? AND oc.ond_code=? ");

		Object[] params = new Object[] { chargeCode, Charge.STATUS_ACTIVE, getSegmentCode(ondCode) };

		Integer recs = (Integer) new JdbcTemplate(getDataSource()).query(sql.toString(), params, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					return new Integer(rs.getInt("CNT"));
				}
				return null;

			}
		});

		if (recs != null && recs.intValue() > 0) {
			return true;
		} else {
			return false;
		}

	}

	public boolean overlapsBaggageTemplatesFlights(ONDBaggageTemplate baggageTemplate) {
		boolean isOverlapped = false;
		int overlappedTemplates = 0;
		Collection<BaggageTemplateFlight> flights = baggageTemplate.getFlights();
		Collection<String> flightNos;

		if (flights != null && !flights.isEmpty()) {

			flightNos = new ArrayList<String>();
			for (BaggageTemplateFlight flight : flights) {
				flightNos.add(flight.getFlightNumber());
			}

			StringBuilder queryContent = new StringBuilder();
			queryContent
					.append("   SELECT COUNT(1) OVERLAP_COUNT  ")
					.append("   FROM BG_T_OND_CHARGE_TEMPLATE BT, BG_T_OND_TEMPLATE_FLIGHT BTF ")
					.append("   WHERE BT.STATUS = 'ACT' AND (BTF.FLIGHT_NUMBER = '" + CommonsConstants.ALL_DESIGNATOR_STRING
							+ "' OR    ");

			if (flights.contains(CommonsConstants.ALL_DESIGNATOR_STRING)) {
				queryContent.append(" BTF.FLIGHT_NUMBER LIKE '%' ) ");
			} else {
				queryContent.append(" BTF.FLIGHT_NUMBER IN ( " + Util.buildStringInClauseContent(flightNos) + " ) ) ");
			}

			queryContent.append("   AND ( BT.FROM_DATE <= ? AND BT.END_DATE >= ? )    ")
					.append("       AND BT.OND_CHARGE_TEMPLATE_ID = BTF.OND_CHARGE_TEMPLATE_ID    ")
					.append("       AND BT.OND_CHARGE_TEMPLATE_ID != ?    ");

			JdbcTemplate template = new JdbcTemplate(getDataSource());
			overlappedTemplates = (Integer) template.query(queryContent.toString(), new Object[] { baggageTemplate.getToDate(),
					baggageTemplate.getFromDate(), baggageTemplate.getTemplateId() }, new ResultSetExtractor() {
				public Object extractData(ResultSet resultSet) throws SQLException, DataAccessException {
					if (resultSet.next()) {
						return resultSet.getInt("OVERLAP_COUNT");
					} else {
						return 0;
					}
				}
			});

			isOverlapped = overlappedTemplates > 0;
		}

		return isOverlapped;
	}

	public boolean overlapsBaggageTemplatesAgents(ONDBaggageTemplate baggageTemplate) {
		boolean isOverlapped = false;
		int overlappedTemplates = 0;
		Collection<BaggageTemplateAgent> agents = baggageTemplate.getAgents();
		Collection<String> agentCodes;

		if (agents != null && !agents.isEmpty()) {

			agentCodes = new ArrayList<String>();
			for (BaggageTemplateAgent agent : agents) {
				agentCodes.add(agent.getAgentCode());
			}

			StringBuilder queryContent = new StringBuilder();
			queryContent
					.append("   SELECT COUNT(1) OVERLAP_COUNT  ")
					.append("   FROM BG_T_OND_CHARGE_TEMPLATE BT, BG_T_OND_TEMPLATE_AGENT BTA ")
					.append("   WHERE BT.STATUS = 'ACT' AND (BTA.AGENT_CODE = '" + CommonsConstants.ALL_DESIGNATOR_STRING
							+ "' OR    ");

			if (agents.contains(CommonsConstants.ALL_DESIGNATOR_STRING)) {
				queryContent.append(" BTA.AGENT_CODE LIKE '%' ) ");
			} else {
				queryContent.append(" BTA.AGENT_CODE IN ( " + Util.buildStringInClauseContent(agentCodes) + " ) ) ");
			}

			queryContent.append("       AND ( BT.FROM_DATE <= ? AND BT.END_DATE >= ? )    ")
					.append("       AND BT.OND_CHARGE_TEMPLATE_ID = BTA.OND_CHARGE_TEMPLATE_ID    ")
					.append("       AND BT.OND_CHARGE_TEMPLATE_ID != ?    ");

			JdbcTemplate template = new JdbcTemplate(getDataSource());
			overlappedTemplates = (Integer) template.query(queryContent.toString(), new Object[] { baggageTemplate.getToDate(),
					baggageTemplate.getFromDate(), baggageTemplate.getTemplateId() }, new ResultSetExtractor() {
				public Object extractData(ResultSet resultSet) throws SQLException, DataAccessException {
					if (resultSet.next()) {
						return resultSet.getInt("OVERLAP_COUNT");
					} else {
						return 0;
					}
				}
			});

			isOverlapped = overlappedTemplates > 0;
		}

		return isOverlapped;
	}

	public boolean overlapsBaggageTemplatesBookingClasses(ONDBaggageTemplate baggageTemplate) {

		boolean isOverlapped = false;
		int overlappedTemplates = 0;
		Collection<BaggageTemplateBookingClass> bcs = baggageTemplate.getBookingClasses();
		Collection<String> bookingClassCodes;

		if (bcs != null && !bcs.isEmpty()) {
			bookingClassCodes = new ArrayList<String>();
			for (BaggageTemplateBookingClass bookingClass : bcs) {
				bookingClassCodes.add(bookingClass.getBookingClass());
			}

			StringBuilder queryContent = new StringBuilder();
			queryContent
					.append("   SELECT COUNT(1) OVERLAP_COUNT  ")
					.append("   FROM BG_T_OND_CHARGE_TEMPLATE BT, BG_T_OND_TEMPLATE_BC BTBC ")
					.append("   WHERE BT.STATUS = 'ACT' AND (BTBC.BOOKING_CODE = '" + CommonsConstants.ALL_DESIGNATOR_STRING
							+ "' OR    ");

			if (bcs.contains(CommonsConstants.ALL_DESIGNATOR_STRING)) {
				queryContent.append(" BTBC.BOOKING_CODE LIKE '%' ) ");
			} else {
				queryContent.append(" BTBC.BOOKING_CODE IN ( " + Util.buildStringInClauseContent(bookingClassCodes) + " ) ) ");
			}

			queryContent.append("       AND ( BT.FROM_DATE <= ? AND BT.END_DATE >= ? )    ")
					.append("       AND BT.OND_CHARGE_TEMPLATE_ID = BTBC.OND_CHARGE_TEMPLATE_ID    ")
					.append("       AND BT.OND_CHARGE_TEMPLATE_ID != ?    ");

			JdbcTemplate template = new JdbcTemplate(getDataSource());
			overlappedTemplates = (Integer) template.query(queryContent.toString(), new Object[] { baggageTemplate.getToDate(),
					baggageTemplate.getFromDate(), baggageTemplate.getTemplateId() }, new ResultSetExtractor() {
				public Object extractData(ResultSet resultSet) throws SQLException, DataAccessException {
					if (resultSet.next()) {
						return resultSet.getInt("OVERLAP_COUNT");
					} else {
						return 0;
					}
				}
			});

			isOverlapped = overlappedTemplates > 0;
		}

		return isOverlapped;
	}

	@Override
	public Map<Integer, Map<CommonsConstants.BaggageCriterion, List<String>>> getOverlappedTemplates(
			ONDBaggageTemplate baggageTemplate) {

		Map<Integer, Map<CommonsConstants.BaggageCriterion, List<String>>> overlappedTemplates = null;
		Object[] args = new Object[3];

		List<String> flightNumbers = new ArrayList<String>();
		if (baggageTemplate.getFlights() != null && !baggageTemplate.getFlights().isEmpty()) {
			for (BaggageTemplateFlight flight : baggageTemplate.getFlights()) {
				flightNumbers.add(flight.getFlightNumber());
			}
		}

		// TODO -- add sales channel once merge is done
		// List<String> salesChannels = new ArrayList<String>();
		// if(baggageTemplate != null && !baggageTemplate .isEmpty()) {
		// for (BaggageTemplate : baggageTemplate) {
		// salesChannels.add();
		// }
		// }

		List<String> agents = new ArrayList<String>();
		if (baggageTemplate.getAgents() != null && !baggageTemplate.getAgents().isEmpty()) {
			for (BaggageTemplateAgent agent : baggageTemplate.getAgents()) {
				agents.add(agent.getAgentCode());
			}
		}

		List<String> bookingClasses = new ArrayList<String>();
		if (baggageTemplate.getBookingClasses() != null && !baggageTemplate.getBookingClasses().isEmpty()) {
			for (BaggageTemplateBookingClass bookingClass : baggageTemplate.getBookingClasses()) {
				bookingClasses.add(bookingClass.getBookingClass());
			}
		}

		List<String> onds = new ArrayList<String>();
		if (baggageTemplate.getOnds() != null && !baggageTemplate.getOnds().isEmpty()) {
			for (ONDBaggageChargeTemplate ond : baggageTemplate.getOnds()) {
				onds.add(ond.getOndCode());
			}
		}

		StringBuilder queryContent = new StringBuilder();
		queryContent.append(" SELECT b.ond_charge_template_id, b.param, b.criteria FROM ( ");

		if (!bookingClasses.isEmpty()) {
			queryContent.append("      SELECT bt.ond_charge_template_id, bt.from_date, bt.end_date , bt.status, ")
					.append("            b.BOOKING_CODE param, 'BOOKING_CLASS' criteria ")
					.append("      FROM bg_t_ond_charge_template bt, bg_t_ond_template_bc b ")
					.append("      WHERE bt.ond_charge_template_id = b.ond_charge_template_id ")
					.append("      AND " + Util.getReplaceStringForIN("b.BOOKING_CODE", bookingClasses));
		} else {
			queryContent.append("      SELECT bt.ond_charge_template_id, bt.from_date, bt.end_date, bt.status,  ")
					.append("            NULL param, 'BOOKING_CLASS' criteria ")
					.append("      FROM bg_t_ond_charge_template bt, bg_t_ond_template_bc b ")
					.append("      WHERE bt.ond_charge_template_id = b.ond_charge_template_id (+) ")
					.append("      GROUP BY bt.ond_charge_template_id, bt.from_date, bt.end_date, bt.status  ")
					.append("      HAVING COUNT( b.ond_template_bc_id ) = 0             ");
		}

		queryContent.append("  UNION ");

		// if (!salesChannels.isEmpty()) {
		// queryContent
		// .append("      SELECT bt.ond_charge_template_id, bt.from_date, bt.end_date, bt.status, ")
		// .append("            c.sales_channel param, 'SALES_CHANNEL' criteria ")
		// .append("      FROM bg_t_ond_charge_template bt, bg_t_ond_template_channel c ")
		// .append("      WHERE bt.ond_charge_template_id = c.ond_charge_template_id ")
		// .append("            AND c.sales_channel IN (" + Util.buildStringInClauseContent(salesChannels) + ") ");
		// } else {
		// queryContent
		// .append("      SELECT bt.ond_charge_template_id, bt.from_date, bt.end_date, bt.status,  ")
		// .append("            NULL param, 'SALES_CHANNEL' criteria ")
		// .append("      FROM bg_t_ond_charge_template bt, bg_t_ond_template_channel c ")
		// .append("      WHERE bt.ond_charge_template_id = c.ond_charge_template_id (+) ")
		// .append("      GROUP BY bt.ond_charge_template_id, bt.from_date, bt.end_date, bt.status  ")
		// .append("      HAVING COUNT( c.ond_template_channel_id ) = 0  ")
		// }

		// queryContent.append("  UNION ");

		if (!flightNumbers.isEmpty()) {
			queryContent.append("      SELECT bt.ond_charge_template_id, bt.from_date, bt.end_date, bt.status, ")
					.append("            f.flight_number param, 'FLIGHT_NUMBER' criteria ")
					.append("      FROM bg_t_ond_charge_template bt, bg_t_ond_template_flight f ")
					.append("      WHERE bt.ond_charge_template_id = f.ond_charge_template_id ")
					.append("             AND " + Util.getReplaceStringForIN("f.FLIGHT_NUMBER", flightNumbers));

		} else {
			queryContent.append("      SELECT bt.ond_charge_template_id, bt.from_date, bt.end_date, bt.status,  ")
					.append("            NULL param, 'FLIGHT_NUMBER' criteria ")
					.append("      FROM bg_t_ond_charge_template bt, bg_t_ond_template_flight f ")
					.append("      WHERE bt.ond_charge_template_id = f.ond_charge_template_id (+) ")
					.append("      GROUP BY bt.ond_charge_template_id, bt.from_date, bt.end_date, bt.status  ")
					.append("      HAVING COUNT( f.ond_template_flight_id ) = 0              ");
		}

		queryContent.append("  UNION ");

		if (!agents.isEmpty()) {
			queryContent.append("      SELECT bt.ond_charge_template_id, bt.from_date, bt.end_date , bt.status, ")
					.append("            a.AGENT_CODE param, 'AGENT' criteria ")
					.append("      FROM bg_t_ond_charge_template bt, bg_t_ond_template_agent a ")
					.append("      WHERE bt.ond_charge_template_id = a.ond_charge_template_id ")
					.append("            AND " + Util.getReplaceStringForIN("A.AGENT_CODE", agents));
		} else {
			queryContent.append("      SELECT bt.ond_charge_template_id, bt.from_date, bt.end_date, bt.status,  ")
					.append("            NULL param, 'AGENT' criteria ")
					.append("      FROM bg_t_ond_charge_template bt, bg_t_ond_template_agent a ")
					.append("      WHERE bt.ond_charge_template_id = a.ond_charge_template_id (+) ")
					.append("      GROUP BY bt.ond_charge_template_id, bt.from_date, bt.end_date, bt.status  ")
					.append("      HAVING COUNT( A.ond_template_agent_id ) = 0              ");
		}

		queryContent.append("  UNION ");

		if (!onds.isEmpty()) {
			queryContent.append("      SELECT bt.ond_charge_template_id, bt.from_date, bt.end_date, bt.status, ")
					.append("            o.ond_baggage_ond_code param, 'OND' criteria ")
					.append("      FROM bg_t_ond_charge_template bt, bg_t_ond_bagg_charge_template o ")
					.append("      WHERE bt.ond_charge_template_id = o.ond_charge_template_id ")
					.append("             AND " + Util.getReplaceStringForIN("o.ond_baggage_ond_code", onds));
		} else {
			queryContent.append("      SELECT bt.ond_charge_template_id, bt.from_date, bt.end_date, bt.status,  ")
					.append("            NULL param, 'OND' criteria ")
					.append("      FROM bg_t_ond_charge_template bt, bg_t_ond_bagg_charge_template bo  ")
					.append("      WHERE bt.ond_charge_template_id = bo.ond_charge_template_id (+)  ")
					.append("      GROUP BY bt.ond_charge_template_id, bt.from_date, bt.end_date, bt.status ")
					.append("      HAVING COUNT( bo.ond_bagg_charge_template_id ) = 0  ");
		}

		queryContent.append("  ) b ").append(" WHERE b.from_date <= ? AND b.end_date >= ? and b.status = 'ACT' ");

		args[0] = baggageTemplate.getToDate();
		args[1] = baggageTemplate.getFromDate();

		if (baggageTemplate.getTemplateId() != 0) {
			queryContent.append("  AND b.ond_charge_template_id != ? ");
			args[2] = baggageTemplate.getTemplateId();
		}

		JdbcTemplate template = new JdbcTemplate(getDataSource());
		overlappedTemplates = (Map<Integer, Map<CommonsConstants.BaggageCriterion, List<String>>>) template.query(
				queryContent.toString(), args, new ResultSetExtractor() {
					public Object extractData(ResultSet resultSet) throws SQLException, DataAccessException {
						int templateId;
						CommonsConstants.BaggageCriterion criterion;
						String param;

						Map<Integer, Map<CommonsConstants.BaggageCriterion, List<String>>> templates = new HashMap<Integer, Map<CommonsConstants.BaggageCriterion, List<String>>>();

						while (resultSet.next()) {
							templateId = resultSet.getInt("ond_charge_template_id");
							criterion = CommonsConstants.BaggageCriterion.valueOf(resultSet.getString("criteria"));
							param = resultSet.getString("param");

							if (!templates.containsKey(templateId)) {
								templates.put(templateId, new HashMap<CommonsConstants.BaggageCriterion, List<String>>());
							}

							if (!templates.get(templateId).containsKey(criterion)) {
								templates.get(templateId).put(criterion, new ArrayList<String>());
							}

							if (param != null) {
								templates.get(templateId).get(criterion).add(param);
							}
						}

						return templates;
					}
				});
		return overlappedTemplates;
	}

	private static String getSegmentCode(String ondCode) {
		String segCode = "";
		String ssArr[] = ondCode.split("/");
		int elements = ssArr.length;
		if (elements == 2) {
			segCode = ondCode;
		} else if (elements > 2) {
			for (int i = 1; i <= elements; i++) {
				if (i == elements) {
					segCode += "***";
				} else {
					segCode += ssArr[i - 1] + "/";
				}
			}
		} else {
			segCode += ssArr[0] + "/***";
		}

		return segCode;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, List<String>> getServiceTaxWiseExcludableChargeCodes(Collection<String> quotedServiceTaxCodesForFirstOnd,
			HashSet<String> reservationChargeCodes) {
		String query = "select cac.charge_code, cac.applicable_charge_code, cac.apply_status from T_CHARGE_APPLICABLE_CHARGES cac where cac.charge_code in ("
				+ getSqlInValuesStr(quotedServiceTaxCodesForFirstOnd)
				+ ") and cac.applicable_charge_code in ("
				+ getSqlInValuesStr(reservationChargeCodes) + ")";
		log.debug(query);

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		return (Map<String, List<String>>) jdbcTemplate.query(query, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Map<String, List<String>> serviceTaxWiseExcludableChargeCodes = new HashMap<String, List<String>>();

				if (rs != null) {
					while (rs.next()) {
						if (QuoteChargesCriteria.EXCLUDE.equals(BeanUtils.nullHandler(rs.getString("apply_status")))) {
							List<String> excludableChargeCodes;
							String chargeCode = BeanUtils.nullHandler(rs.getString("charge_code"));
							if (serviceTaxWiseExcludableChargeCodes.get(chargeCode) != null) {
								excludableChargeCodes = serviceTaxWiseExcludableChargeCodes.get(chargeCode);
							} else {
								excludableChargeCodes = new ArrayList<String>();
							}
							excludableChargeCodes.add(BeanUtils.nullHandler(rs.getString("applicable_charge_code")));
							if (serviceTaxWiseExcludableChargeCodes.get(chargeCode) == null) {
								serviceTaxWiseExcludableChargeCodes.put(chargeCode, excludableChargeCodes);
							}
						}
					}
				}
				return serviceTaxWiseExcludableChargeCodes;
			}
		});
	}
}