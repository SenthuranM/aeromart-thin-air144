/**
 * 
 */
package com.isa.thinair.airpricing.core.bl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.baggage.FlightBaggageDTO;
import com.isa.thinair.airinventory.api.service.BaggageBusinessDelegate;
import com.isa.thinair.airmaster.api.model.Baggage;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airpricing.api.dto.BaggageTemplateDTO;
import com.isa.thinair.airpricing.api.model.BaggageCharge;
import com.isa.thinair.airpricing.api.model.BaggageTemplate;
import com.isa.thinair.airpricing.api.service.AirpricingUtils;
import com.isa.thinair.airpricing.api.util.AirPricingCustomConstants;
import com.isa.thinair.airpricing.api.util.LocalCurrencyUtil;
import com.isa.thinair.airpricing.api.util.SplitUtil;
import com.isa.thinair.airpricing.core.audit.AuditAirpricing;
import com.isa.thinair.airpricing.core.persistence.dao.ChargeDAO;
import com.isa.thinair.airpricing.core.persistence.dao.ChargeJdbcDAO;
import com.isa.thinair.airpricing.core.util.InternalConstants;
import com.isa.thinair.airpricing.core.util.PricingUtils;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.webplatform.api.util.Constants;

/**
 * @author mano
 * 
 */
public class BaggageBusinessLayer {

	private final Log log = LogFactory.getLog(BaggageBusinessLayer.class);

	private ChargeDAO chargedao;

	private ChargeJdbcDAO chargejdbcdao;

	public BaggageBusinessLayer() {

		chargedao = (ChargeDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.CHARGE_DAO);

		chargejdbcdao = getChargeJdbcDAO();

	}

	@SuppressWarnings("unused")
	public void saveTemplate(BaggageTemplate baggageTemplate, Integer chargeid) throws ModuleException {
		try {

			chargedao.saveBaggageTemplate(baggageTemplate);

			// remove calling from create
			if (baggageTemplate.getVersion() != 0) {
				Collection<FlightBaggageDTO> colFltBgs = chargejdbcdao.getSegmentBaggagesForBaggageTemplate(baggageTemplate
						.getTemplateId());
				BaggageBusinessDelegate oBaggageBD = AirpricingUtils.getBaggageBD();
				Collection<FlightBaggageDTO> colSegs = new HashSet<FlightBaggageDTO>();

				for (FlightBaggageDTO fltBgDTO : colFltBgs) {
					fltBgDTO.setTemplateId(baggageTemplate.getTemplateId());
					colSegs.add(fltBgDTO);
				}

				ServiceResponce sr = oBaggageBD.assignFlightBaggageCharges(colSegs, chargeid, true, null);
			}

		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	private ChargeJdbcDAO getChargeJdbcDAO() {
		return (ChargeJdbcDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.CHARGE_JDBC_DAO);
	}

	public BaggageTemplate createTemplate(BaggageTemplateDTO bgDTO) throws ModuleException {
		try {
			BaggageTemplate baggageTemplate = new BaggageTemplate();
			baggageTemplate.setDescription(bgDTO.getDescription().trim());
			baggageTemplate.setTemplateCode(bgDTO.getTemplateCode().toUpperCase().trim());

			if (bgDTO.getTemplateId() != null) {
				baggageTemplate.setTemplateId(bgDTO.getTemplateId());
			}
			baggageTemplate.setVersion(bgDTO.getVersion());
			baggageTemplate.setCreatedBy(bgDTO.getCreatedBy());
			baggageTemplate.setCreatedDate(bgDTO.getCreatedDate());

			if (bgDTO.getStatus() != null) {
				baggageTemplate.setStatus(bgDTO.getStatus());
			} else {
				baggageTemplate.setStatus(Baggage.STATUS_INACTIVE);
			}
			Set<BaggageCharge> bgCharges = new HashSet<BaggageCharge>();

			if (bgDTO.getBaggageCharges() != null && !bgDTO.getBaggageCharges().equals("")) {
				String[] arrChages = bgDTO.getBaggageCharges().split("~");
				String[] strChages = null;
				Map<String, Boolean> defaultBGGStatus = new HashMap<String, Boolean>();
				for (int i = 0; i < arrChages.length; i++) {
					if (arrChages[i] != null && !arrChages[i].equals("")) {
						strChages = arrChages[i].split(Constants.STRING_SEPARATOR);
						BaggageCharge baggageChg = new BaggageCharge();
						baggageChg.setBaggageName(strChages[1]);
						if (strChages[2] != null && !strChages[2].trim().equals("")) {
							baggageChg.setChargeId(new Integer(strChages[2]));
						}
						baggageChg.setTemplate(baggageTemplate);
						if (strChages[4] != null && !strChages[4].trim().equals("")) {
							baggageChg.setBaggageId(new Integer(strChages[4]));
						}

						baggageChg.setAmount(LocalCurrencyUtil.amountInBaseCurrency(bgDTO.getLocalCurrCode(), new BigDecimal(
								strChages[5])));// change to local currency

						baggageChg.setLocalCurrencyAmount(new BigDecimal(strChages[5]));
						if (strChages[6] != null && !strChages[6].trim().equals("")) {
							baggageChg.setAllocatedPieces(new Integer(strChages[6]));
						}
						baggageChg.setVersion(new Long(strChages[7]));
						baggageChg.setStatus(strChages[8]);
						String[] splittedCabinClassAndLogicalCabinClass = SplitUtil
								.getSplittedCabinClassAndLogicalCabinClass(strChages[9]);
						baggageChg.setLogicalCCCode(splittedCabinClassAndLogicalCabinClass[1]);
						baggageChg.setCabinClass(splittedCabinClassAndLogicalCabinClass[0]);
						if (strChages[10] != null
								&& strChages[10].equalsIgnoreCase("Y")
								&& (defaultBGGStatus.isEmpty()
										|| defaultBGGStatus.get(splittedCabinClassAndLogicalCabinClass[1]) == null || !defaultBGGStatus
											.get(splittedCabinClassAndLogicalCabinClass[1]))) {
							defaultBGGStatus.put(splittedCabinClassAndLogicalCabinClass[1], true);
							baggageChg.setDefaultBaggageFlag("Y");
						} else {
							baggageChg.setDefaultBaggageFlag("N");
						}

						bgCharges.add(baggageChg);
					}
				}
			}
			baggageTemplate.setBaggageCharges(bgCharges);
			baggageTemplate.setChargeLocalCurrencyCode(bgDTO.getLocalCurrCode());
			return baggageTemplate;
		} catch (ModuleException e) {
			log.error("ERROR-Auto Updating Charges upon Exchnage Rate Change Failed");
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	public void updateBaggageChargeBaseCurrAmountOnLocalCurrExchangeRtUpdate(CurrencyExchangeRate currencyExchangeRate,
			UserPrincipal userPrincipal) throws ModuleException {

		try {
			ArrayList<BaggageTemplate> affectedBgTMPList;
			BigDecimal newChargeAmount;
			BigDecimal exRate, boundaryValue, breakPointValue;

			affectedBgTMPList = (ArrayList<BaggageTemplate>) chargedao.getAffectedBaggageTemplates(currencyExchangeRate
					.getCurrency().getCurrencyCode());
			exRate = currencyExchangeRate.getExrateCurToBaseNumber();
			boundaryValue = currencyExchangeRate.getCurrency().getBoundryValue();
			breakPointValue = currencyExchangeRate.getCurrency().getBreakPoint();
			for (BaggageTemplate bgTmp : affectedBgTMPList) {
				for (BaggageCharge bgChrg : bgTmp.getBaggageCharges()) {
					if (!bgChrg.getStatus().equals(AirPricingCustomConstants.STATUS_ACTIVE)) {
						continue;
					}
					newChargeAmount = AccelAeroRounderPolicy
							.getRoundedValue(AccelAeroCalculator.multiply(bgChrg.getLocalCurrencyAmount(), exRate),
									boundaryValue, breakPointValue);
					bgChrg.setAmount(newChargeAmount);
				}
			}
			updateBaggageTemplate(affectedBgTMPList, userPrincipal);

		} catch (ModuleException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	private void updateBaggageTemplate(ArrayList<BaggageTemplate> bgTMPList, UserPrincipal userPrincipal) throws ModuleException {
		Iterator<BaggageTemplate> iterator = bgTMPList.iterator();
		try {
			while (iterator.hasNext()) {
				BaggageTemplate bgTemplate = iterator.next();
				bgTemplate = (BaggageTemplate) PricingUtils.setUserDetails(bgTemplate, userPrincipal);
				chargedao.saveBaggageTemplate(bgTemplate);
				AuditAirpricing.doAuditBaggageAddOrModifyTemplate(bgTemplate, userPrincipal);

			}
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}

	}	

}
