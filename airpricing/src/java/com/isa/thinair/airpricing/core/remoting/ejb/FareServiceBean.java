/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 *
 * Created on Jul 13, 2005 17:29:42
 *
 * $Id$
 *
 * ===============================================================================
 */
package com.isa.thinair.airpricing.core.remoting.ejb;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airinventory.api.dto.seatavailability.FareSummaryDTO;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airpricing.api.criteria.FareSearchCriteria;
import com.isa.thinair.airpricing.api.dto.CalendarFaresRQ;
import com.isa.thinair.airpricing.api.dto.CalendarFaresRS;
import com.isa.thinair.airpricing.api.dto.ChargeTO;
import com.isa.thinair.airpricing.api.dto.FareDTO;
import com.isa.thinair.airpricing.api.dto.FaresAndChargesTO;
import com.isa.thinair.airpricing.api.dto.LightFareDTO;
import com.isa.thinair.airpricing.api.model.Charge;
import com.isa.thinair.airpricing.api.model.Fare;
import com.isa.thinair.airpricing.api.model.FareRule;
import com.isa.thinair.airpricing.api.model.FareRulePax;
import com.isa.thinair.airpricing.api.model.FareRulePaxFees;
import com.isa.thinair.airpricing.api.service.AirpricingUtils;
import com.isa.thinair.airpricing.api.service.FareBD;
import com.isa.thinair.airpricing.core.audit.AuditAirpricing;
import com.isa.thinair.airpricing.core.bl.FareBL;
import com.isa.thinair.airpricing.core.criteria.ApplicableFareSearchCriteria;
import com.isa.thinair.airpricing.core.persistence.dao.FareDAO;
import com.isa.thinair.airpricing.core.persistence.dao.FareJdbcDAO;
import com.isa.thinair.airpricing.core.service.bd.FareBDImpl;
import com.isa.thinair.airpricing.core.service.bd.FareBDLocalImpl;
import com.isa.thinair.airpricing.core.util.InternalConstants;
import com.isa.thinair.airproxy.api.model.reservation.availability.CalendarSearchRQ;
import com.isa.thinair.airproxy.api.model.reservation.commons.OriginDestinationInformationTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.PassengerTypeQuantityTO;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;

/**
 * @author Byorn
 */
@Stateless
@RemoteBinding(jndiBinding = "FareService.remote")
@LocalBinding(jndiBinding = "FareService.local")
@RolesAllowed("user")
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class FareServiceBean extends PlatformBaseSessionBean implements FareBDImpl, FareBDLocalImpl {

	private final Log log = LogFactory.getLog(getClass());

	/**
	 * Create Master FareRule void
	 * 
	 * @param fareDTO
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void saveFare(FareDTO fareDTO) throws ModuleException {
		try {
			if (fareDTO.getFareRule() != null) {
				setUserDetails(fareDTO.getFareRule());
				if (fareDTO.getFareRule().getPaxDetails() != null) {
					setUserDetailsForCollection(fareDTO.getFareRule().getPaxDetails());
				}
			}

			new FareBL().saveFare(fareDTO, getUserPrincipal());
		} catch (ModuleException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		} catch (Exception e1) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException("airpricing.technical.error", "airpricing.desc");
		}

	}

	/**
	 * Update Master FareRule void
	 * 
	 * @param fareDTO
	 * 
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Integer updateFare(FareDTO fareDTO, boolean skipSplitFare) throws ModuleException {
		try {
			if (fareDTO.getFareRule() != null) {
				setUserDetails(fareDTO.getFareRule());
				if (fareDTO.getFareRule().getPaxDetails() != null) {
					setUserDetailsForCollection(fareDTO.getFareRule().getPaxDetails());
				}
			}
			FareBL fareBL = new FareBL();

			fareBL.updateFare(fareDTO, getUserPrincipal(), skipSplitFare, false);
			if (fareDTO.getFare() != null && fareDTO.getFare().getFareDefType() != null
					&& fareDTO.getFare().getFareDefType().equals(Fare.FARE_DEF_TYPE_MASTER)) {
				fareBL.updateLinkFares(fareDTO.getFare(), fareDTO.getFare().getFareId(), getUserPrincipal(), false);
			}

			return fareDTO.getFare().getFareId();
		} catch (ModuleException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		} catch (Exception e1) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e1, "airpricing.technical.error", "airpricing.desc");
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void updateNonCriticalFareDetails(FareDTO fareDTO) throws ModuleException {
		try {
			new FareBL().updateNonCriticalFareDetails(fareDTO);
		} catch (ModuleException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		} catch (Exception e1) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException("airpricing.technical.error", "airpricing.desc");
		}
	}

	/**
	 * Update collection of fares void
	 * 
	 * @param fares
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void updateFares(Collection<Fare> fares) throws ModuleException {
		try {
			// auditing each fare
			Iterator<Fare> iteratorFares = fares.iterator();
			while (iteratorFares.hasNext()) {
				Fare fare = iteratorFares.next();
				AuditAirpricing.doAudit(fare, getUserPrincipal());
			}
			new FareBL().updateFares(fares, getUserPrincipal(), false, false);
		} catch (ModuleException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		} catch (Exception e1) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException("airpricing.technical.error", "airpricing.desc");
		}
	}

	/**
	 * get Fare Fare
	 * 
	 * @param fareID
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public FareDTO getFare(int fareID) throws ModuleException {
		return new FareBL().getFare(fareID);
	}

	@Override
	public Collection<FareDTO> getFares(Collection<Integer> fareIds) throws ModuleException {
		return new FareBL().getFares(fareIds);
	}

	/**
	 * 
	 * Page
	 * 
	 * @param fareSearchCriteria
	 * @param startIndex
	 * @param pageSize
	 * @param orderBy
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Page getFares(FareSearchCriteria fareSearchCriteria, int startIndex, int pageSize, List<String> orderBy)
			throws ModuleException {
		try {
			return getFareJdbcDAO().getFares(fareSearchCriteria, startIndex, pageSize, orderBy);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}

	}

	/**
	 * 
	 * Collection
	 * 
	 * @param bookingCodes
	 * @param ondCode
	 * @param depatureDate
	 * @return
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Collection<Fare> getEffectiveFares(Collection<String> bookingCodes, String ondCode, Date depatureDate)
			throws ModuleException {
		try {
			return getFareDAO().getEffectiveFares(bookingCodes, ondCode, depatureDate);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpriciing.desc");
		}

	}

	/**
	 * get Effective Agents Count HashMap
	 * 
	 * @param bookingCodes
	 * @param ondCode
	 * @param depatureDate
	 * @return
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public HashMap<String, Integer> getEffectiveAgentsCount(Collection<String> bookingCodes, String ondCode, Date depatureDate)
			throws ModuleException {
		try {
			return new FareBL().getEffectiveAgentsCount(bookingCodes, ondCode, depatureDate);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpriciing.desc");
		}

	}

	/**
	 * 
	 * @param agentCode
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public HashMap<String, List<String>> getAgentsLinkedToBookingClass(Collection<String> bookingCodesCollection)
			throws ModuleException {
		return getFareJdbcDAO().getAgentsLinkedToBookingClass(bookingCodesCollection);
	}

	/**
	 * 
	 * @param agentCode
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Collection<String> getBookingCodesLinkedToAgent(String agentCode) throws ModuleException {
		return getFareJdbcDAO().getBookingCodesLinkedToAgent(agentCode);
	}

	/**
	 * Return refundable states
	 * 
	 * @param fareIds
	 * @param chargeRateIds
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public FaresAndChargesTO getRefundableStatuses(Collection<Integer> fareIds, Collection<Integer> chargeRateIds,
			String prefferedLanguage) throws ModuleException {
		return new FareBL().getRefundableStatuses(fareIds, chargeRateIds, prefferedLanguage);
	}

	/**
	 * Return ChargeRateById
	 * 
	 * @param chargeRateIds
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<Integer, ChargeTO> getChageRateById(Collection<Integer> chargeRateIds) throws ModuleException {
		return new FareBL().getChageRateById(chargeRateIds);
	}

	/**
	 * Returns the fare rule id and summary mapping
	 * 
	 * @param fareRuleIds
	 *            Collection of FareRuleIDs
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<Integer, FareSummaryDTO> getPaxDetails(Collection<Integer> fareRuleIds) throws ModuleException {
		try {
			return new FareBL().getPaxDetails(fareRuleIds);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	/**
	 * 
	 * FareJdbcDAO
	 * 
	 * @return
	 */
	private FareJdbcDAO getFareJdbcDAO() throws ModuleException {
		return (FareJdbcDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.MANAGE_FARES);
	}

	/**
	 * 
	 * FareDAO
	 * 
	 * @return
	 * @throws ModuleException
	 */
	private FareDAO getFareDAO() throws ModuleException {
		return (FareDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FARE_DAO);
	}

	/**
	 * Returns active charges
	 * 
	 * @param colChargeGrpCodes
	 * @return
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Collection<Charge> getActiveCharges(Collection<String> colChargeGrpCodes) {
		return new FareBL().getActiveCharges(colChargeGrpCodes);
	}

	/**
	 * Returns fare rules linked to provided fares.
	 * 
	 * @param fareIds
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public HashMap<Integer, FareRule> getFareRules(Collection<Integer> fareIds) throws ModuleException {
		return getFareDAO().getFareRules(fareIds);
	}

	/**
	 * Return fare rules for a given fare basis code
	 * 
	 * @param fareBasisCode
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<FareRule> getFareRules(String fareBasisCode) throws ModuleException {
		return new FareBL().getFareRules(fareBasisCode);
	}

	/**
	 * Set the user details permission for the FareRule Passenger Details
	 * 
	 * @param paxDetails
	 * @return
	 * @throws ModuleException
	 */
	private void setUserDetailsForCollection(Collection<FareRulePax> paxDetails) throws ModuleException {
		Iterator<FareRulePax> Itr = paxDetails.iterator();
		FareRulePax oMFPax;

		while (Itr.hasNext()) {
			oMFPax = Itr.next();
			setUserDetails(oMFPax);
		}
	}

	/**
	 * Re-calculate Base fare, when conversion rates changed.
	 * 
	 * @param toDate
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void reCalcBaseFareForLocalCurr(Date date) throws ModuleException {

		FareBD fareBD = AirpricingUtils.getFareBD();

		Collection<CurrencyExchangeRate> currExchangeList = null;

		try {

			currExchangeList = AirpricingUtils.getCommonMasterBD().getAllUpdatedCurrFares(date);
			new FareBL().reCalcBaseFareForLocalCurr(currExchangeList);
			fareBD.updateStatusForFaresInLocalCurr(currExchangeList, CommonsConstants.CreditLimitUpdateStatus.SUCCESS.code());
		} catch (ModuleException ex) {
			log.error("ModuleException :: reCalcBaseFareForLocalCurr ", ex);
			fareBD.updateStatusForFaresInLocalCurr(currExchangeList, CommonsConstants.CreditLimitUpdateStatus.FAILED.code());

			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error("CommonsDataAccessException :: reCalcBaseFareForLocalCurr ", cdaex);
			fareBD.updateStatusForFaresInLocalCurr(currExchangeList, CommonsConstants.CreditLimitUpdateStatus.FAILED.code());

			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void updateStatusForFaresInLocalCurr(Collection<CurrencyExchangeRate> currExchangeList, String status)
			throws ModuleException {
		try {
			new FareBL().updateStatusForFaresInLocalCurr(currExchangeList, status);
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::updateStatusForFaresInLocalCurr ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::updateStatusForFaresInLocalCurr ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	private void setUserDetailsForPaxFeeCollection(Collection<FareRulePaxFees> paxFees) throws ModuleException {
		Iterator<FareRulePaxFees> Itr = paxFees.iterator();
		FareRulePaxFees fareRulePaxFees;

		while (Itr.hasNext()) {
			fareRulePaxFees = Itr.next();
			setUserDetails(fareRulePaxFees);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<Object[]> getFare(FareDTO fareDTO) throws ModuleException {

		String fareRuleCode = fareDTO.getFareRuleCode();
		Date effectiveFromDate = fareDTO.getFare().getEffectiveFromDate();
		Date effectiveToDate = fareDTO.getFare().getEffectiveToDate();
		Date salesEffectiveFromDate = fareDTO.getFare().getSalesEffectiveFrom();
		Date salesEffectiveToDate = fareDTO.getFare().getSalesEffectiveTo();

		Date returnEffectiveFromDate = fareDTO.getFare().getReturnEffectiveFromDate();
		Date returnEffectiveToDate = fareDTO.getFare().getReturnEffectiveToDate();

		String bookingCode = fareDTO.getFare().getBookingCode();
		String ondCode = fareDTO.getFare().getOndCode();
		return getFareDAO().getFare(fareRuleCode, bookingCode, ondCode, effectiveFromDate, effectiveToDate,
				salesEffectiveFromDate, salesEffectiveToDate, returnEffectiveFromDate, returnEffectiveToDate);
	}

	/**
	 * Return whether the fare is valid or not
	 * 
	 * @param fareRuleCode
	 * @param bookingCode
	 * @param ONDCode
	 * @param efctFrom
	 * @param efctTo
	 * @param salesEfctFrom
	 * @param salesEfctTo
	 * @param returnFromDate
	 * @param returnToDate
	 * 
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean isFareLegal(String fareRuleCode, String bookingCode, String ONDCode, Date efctFrom, Date efctTo,
			Date salesEfctFrom, Date salesEfctTo, Date returnFromDate, Date returnToDate, boolean returnFlag)
			throws ModuleException {
		return getFareDAO().isFareLegal(fareRuleCode, bookingCode, ONDCode, efctFrom, efctTo, salesEfctFrom, salesEfctTo,
				returnFromDate, returnToDate, returnFlag);
	}

	/**
	 * Returns the fare rule id and summary mapping
	 * 
	 * @param fareRuleIds
	 *            Collection of FareRuleIDs
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<Integer, FareSummaryDTO> getLocalFares(Collection<Integer> fareIds) throws ModuleException {
		try {
			return new FareBL().getLocalFares(fareIds);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<Integer, String> getMasterFaresForOnD(String ondCode) throws ModuleException {
		try {
			return new FareBL().getMasterFaresForOnD(ondCode);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean isMasterFareAlreayLinked(Integer masterFareId) throws ModuleException {
		try {
			return new FareBL().isMasterFareAlreadyLinked(masterFareId);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean isMasterFareRefCodeAlreayUsed(String masterFareRefCode) throws ModuleException {
		try {
			return new FareBL().isMasterFareRefCodeAlreayUsed(masterFareRefCode);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Collection<Fare> getLinkFares(Integer masterFareID) throws ModuleException {
		try {
			return new FareBL().getLinkFares(masterFareID);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void fareUpdateOnExchangeRateChange() throws ModuleException {
		try {
			new FareBL().fareUpdateOnExchangeRateChange(getUserPrincipal());
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	/**
	 * 
	 * @param fareId
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean isModifyToSameFareEnabled(Integer fareId) throws ModuleException {
		return getFareJdbcDAO().isModifyToSameFareEnabled(fareId);
	}

	/**
	 * 
	 * @param fareId
	 * @return
	 * @throws ModuleException
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<Integer, Integer> getSameReturnGroupSegDetails(Collection<Integer> dateChangedResSegList) throws ModuleException {
		return getFareJdbcDAO().getSameReturnGroupSegDetails(dateChangedResSegList);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public CalendarFaresRS getApplicableFares(CalendarFaresRQ faresRQ) throws ModuleException {

		CalendarSearchRQ searchRQ = faresRQ.getSearchRQ();
		ApplicableFareSearchCriteria criteria = new ApplicableFareSearchCriteria();
		criteria.setBookingType(searchRQ.getTravelPreferences().getBookingType());
		criteria.setAgentCode(searchRQ.getAvailPreferences().getTravelAgentCode());
		Set<Integer> salesChannels = new HashSet<Integer>();
		salesChannels.add(SalesChannelsUtil.SALES_CHANNEL_PUBLIC);
		if (ApplicationEngine.IBE == searchRQ.getAvailPreferences().getAppIndicator()) {
			salesChannels.add(SalesChannelsUtil.SALES_CHANNEL_WEB);
		} else if (ApplicationEngine.IBE == searchRQ.getAvailPreferences().getAppIndicator()) {
			salesChannels.add(SalesChannelsUtil.SALES_CHANNEL_AGENT);
		}
		criteria.setSalesChannels(salesChannels);
		Set<String> paxTypes = new HashSet<String>();
		for (PassengerTypeQuantityTO paxTypeQty : searchRQ.getTravelerInfoSummary().getPassengerTypeQuantityList()) {
			paxTypes.add(paxTypeQty.getPassengerType());
		}
		criteria.setPaxTypes(paxTypes);

		CalendarFaresRS calendarFares = new CalendarFaresRS();
		int ondSequence = 0;
		for (OriginDestinationInformationTO ondTO : searchRQ.getOrderedOriginDestinationInformationList()) {
			criteria.setFrom(ondTO.getDepartureDateTimeStart());
			criteria.setTo(ondTO.getDepartureDateTimeEnd());
			criteria.setOndCodes(faresRQ.getOndCodesFor(ondSequence));
			List<LightFareDTO> fares = null;
			if (faresRQ.getOndCodesFor(ondSequence) != null && faresRQ.getOndCodesFor(ondSequence).size() > 0) {
				fares = getFareJdbcDAO().getApplicableFares(criteria);
			}
			calendarFares.addOndFares(ondSequence++, fares);
		}

		return calendarFares;
	}
}
