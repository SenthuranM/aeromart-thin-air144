package com.isa.thinair.airpricing.core.persistence.dao;

import java.util.List;

import com.isa.thinair.airpricing.api.criteria.ONDChargeSearchCriteria;
import com.isa.thinair.commons.api.dto.Page;

public interface ONDChargeJdbcDAO {

	/**
	 * Filter charges and matched specified OND and POS
	 * 
	 * returns a list of OriginDestinationDTO
	 * 
	 */
	/** Interface method to get a Page of ONDChareges passing a criteria **/
	public Page getONDCharges(ONDChargeSearchCriteria criteria, int startIndex, int pageSize, List<String> orderBy);

}
