/*
 * Created on 05-Jun-2005
 *
 */
package com.isa.thinair.airpricing.core.service;

import com.isa.thinair.platform.api.DefaultModule;

/**
 * @author Byorn
 * 
 *         AirpricingModule's service interface
 * @isa.module.service-interface module-name="airpricing" description="The pricing module"
 */
public class AirpricingService extends DefaultModule {
}
