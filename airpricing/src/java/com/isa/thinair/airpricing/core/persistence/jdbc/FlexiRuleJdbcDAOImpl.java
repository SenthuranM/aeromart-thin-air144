package com.isa.thinair.airpricing.core.persistence.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airpricing.api.dto.FlexiQuoteCriteria;
import com.isa.thinair.airpricing.api.dto.FlexiRuleDTO;
import com.isa.thinair.airpricing.api.dto.FlexiRuleFlexibilityDTO;
import com.isa.thinair.airpricing.core.persistence.dao.FlexiRuleJdbcDAO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.core.framework.PlatformBaseJdbcDAOSupport;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.Util;

/**
 * BHive: FlexiRule module
 * 
 * @author Dhanya
 */

public class FlexiRuleJdbcDAOImpl extends PlatformBaseJdbcDAOSupport implements FlexiRuleJdbcDAO {

	private Log log = LogFactory.getLog(FlexiRuleJdbcDAOImpl.class);

	/** Query IDs */
	private static final String FLEXI_QUOTE_FOR_OND = "FLEXI_QUOTE_FOR_OND";

	// private static final String REBUILF_FLEXI_QUOTE_FOR_OND = "REBUILF_FLEXI_QUOTE_FOR_OND";

	/**
	 * method to get Flexi Rules with paging and criteria
	 * 
	 * @return Page
	 * @param FlexiRuleSearchCriteria
	 * @param startIndex
	 * @param pageSize
	 * @param orderbyfields
	 */
	@SuppressWarnings("unchecked")
	public HashMap<String, HashMap<String, FlexiRuleDTO>> quoteFlexiCharges(Collection<FlexiQuoteCriteria> colFlexiQuoteCriteria) {

		HashMap<String, HashMap<String, FlexiRuleDTO>> ondChargesMap = new HashMap<String, HashMap<String, FlexiRuleDTO>>();

		String query = getQuery(FLEXI_QUOTE_FOR_OND, new String[] {});
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		Iterator<FlexiQuoteCriteria> iter = colFlexiQuoteCriteria.iterator();
		FlexiQuoteCriteria flexiQuoteCriteria;
		while (iter.hasNext()) {
			flexiQuoteCriteria = iter.next();
			ondChargesMap.put(flexiQuoteCriteria.getOndCode(), (HashMap<String, FlexiRuleDTO>) jdbcTemplate.query(
					new FlexiQuotePreparedStmtCreator(query, flexiQuoteCriteria), new FlexiQuoteResultSetExtractor(
							flexiQuoteCriteria)));
		}

		return ondChargesMap;
	}

	/**
	 * This method rebuild the FlexiRuleDTOs for a given flexiRuleId col
	 */
	@SuppressWarnings("unchecked")
	@Override
	public HashMap<String, FlexiRuleDTO> rebuildFlexiCharges(Collection<Integer> flexiIds, FlexiQuoteCriteria criteria) {

		HashMap<String, FlexiRuleDTO> ondFlexiChargesMap = new HashMap<String, FlexiRuleDTO>();

		log.debug("rebuildFlexiCharges");
		String inStr = Util.buildIntegerInClauseContent(flexiIds);

		String sql = "SELECT FLXRULE.FLEXI_RULE_ID, " + "  FLXRULE.FLEXI_CODE, " + "  FLXRULE.DESCRIPTION, "
				+ "  FLXRULE.JOURNEY_TYPE, " + "  FLXRULE.CHARGE_CODE, " + "  FLXRATE.FLEXI_RULE_RATE_ID, "
				+ "  FLXRATE.ADULT_CHARGE, " + "  FLXRATE.ADULT_CHARGE_TYPE, " + "  FLXRATE.ADULT_PERCENTAGE_BASIS_ID, "
				+ "  FLXRATE.CHILD_CHARGE, " + "  FLXRATE.CHILD_CHARGE_TYPE, " + "  FLXRATE.CHILD_PERCENTAGE_BASIS_ID, "
				+ "  FLXRATE.INFANT_CHARGE, " + "  FLXRATE.INFANT_CHARGE_TYPE, " + "  FLXRATE.INFANT_PERCENTAGE_BASIS_ID, "
				+ "  FLXF.FLEXI_RULE_FLEXI_TYPE_ID, " + "  FLXF.ALLOWED_COUNT, " + "  FLXF.CUT_OVER_MINS, "
				+ "  FLXFT.DESCRIPTION AS FLEXIBILITY_DESC, " + "  FLXRULE.RULE_COMMENTS " + "FROM T_FLEXI_RULE FLXRULE, "
				+ "  T_FLEXI_RULE_RATE FLXRATE, " + "  T_FLEXI_RULE_FLEXIBILITY FLXF, " + "  T_FLEXI_RULE_FLEXI_TYPE FLXFT "
				+ "WHERE FLXRULE.FLEXI_RULE_ID       = FLXRATE.FLEXI_RULE_ID "
				+ "AND FLXRULE.FLEXI_RULE_ID         = FLXF.FLEXI_RULE_ID "
				+ "AND FLXF.FLEXI_RULE_FLEXI_TYPE_ID = FLXFT.FLEXI_RULE_FLEXI_TYPE_ID " + "AND FLXRULE.FLEXI_RULE_ID        IN("
				+ inStr + ")";

		if (log.isDebugEnabled()) {
			log.debug("rebuildFlexiCharges sql [" + sql + "]");
		}

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		ondFlexiChargesMap = (HashMap<String, FlexiRuleDTO>) jdbcTemplate.query(sql, new FlexiQuoteResultSetExtractor(criteria));

		return ondFlexiChargesMap;
	}

	public class FlexiQuotePreparedStmtCreator implements PreparedStatementCreator {

		private String queryString;

		private FlexiQuoteCriteria _criteria;

		public FlexiQuotePreparedStmtCreator(String query, FlexiQuoteCriteria flexiQuoteCriteria) {
			this.queryString = query;
			this._criteria = flexiQuoteCriteria;
		}

		public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
			PreparedStatement ps = con.prepareStatement(queryString);

			int index = 0;
			ps.setDate(++index, _criteria.getDepartureDate());
			ps.setDate(++index, new java.sql.Date(CalendarUtil.getCurrentSystemTimeInZulu().getTime()));
			ps.setLong(++index, _criteria.getDepartureTimeGap());
			ps.setInt(++index, _criteria.getFareRuleId());
			return ps;
		}

	}

	public class FlexiQuoteResultSetExtractor implements ResultSetExtractor {

		private FlexiQuoteCriteria _criteria;

		public FlexiQuoteResultSetExtractor(FlexiQuoteCriteria flexiQuoteCriteria) {
			this._criteria = flexiQuoteCriteria;
		}

		public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
			HashMap<String, FlexiRuleDTO> flexiRuleDTOs = new HashMap<String, FlexiRuleDTO>();
			String flexiCode = null;
			FlexiRuleDTO flexiRuleDTO;

			while (rs.next()) {
				flexiCode = rs.getString("FLEXI_CODE");
				if (flexiRuleDTOs.containsKey(flexiCode))
					flexiRuleDTO = (FlexiRuleDTO) flexiRuleDTOs.get(flexiCode);
				else {
					flexiRuleDTO = new FlexiRuleDTO();

					flexiRuleDTO.setFareRuleId(_criteria.getFareRuleId());
					flexiRuleDTO.setFlexiRuleId(rs.getInt("FLEXI_RULE_ID"));
					flexiRuleDTO.setFlexiCode(flexiCode);
					flexiRuleDTO.setDescription(rs.getString("DESCRIPTION"));
					flexiRuleDTO.setChargeCode(rs.getString("CHARGE_CODE"));
					flexiRuleDTO.setJourneyType(rs.getString("JOURNEY_TYPE"));
					flexiRuleDTO.setFlexiRuleRateId(rs.getInt("FLEXI_RULE_RATE_ID"));
					// Adult flexi rate
					flexiRuleDTO.addCharge(PaxTypeTO.ADULT,
							_criteria.isOfferFlexiFreeOfCharge() ? 0 : rs.getDouble("ADULT_CHARGE"));
					flexiRuleDTO.addChargeType(PaxTypeTO.ADULT, rs.getString("ADULT_CHARGE_TYPE"));
					flexiRuleDTO.addPercentageBasis(PaxTypeTO.ADULT, rs.getString("ADULT_PERCENTAGE_BASIS_ID"));
					// Child flexi rate
					flexiRuleDTO.addCharge(PaxTypeTO.CHILD,
							_criteria.isOfferFlexiFreeOfCharge() ? 0 : rs.getDouble("CHILD_CHARGE"));
					flexiRuleDTO.addChargeType(PaxTypeTO.CHILD, rs.getString("CHILD_CHARGE_TYPE"));
					flexiRuleDTO.addPercentageBasis(PaxTypeTO.CHILD, rs.getString("CHILD_PERCENTAGE_BASIS_ID"));
					// Infant flexi rate
					flexiRuleDTO.addCharge(PaxTypeTO.INFANT,
							_criteria.isOfferFlexiFreeOfCharge() ? 0 : rs.getDouble("INFANT_CHARGE"));
					flexiRuleDTO.addChargeType(PaxTypeTO.INFANT, rs.getString("INFANT_CHARGE_TYPE"));
					flexiRuleDTO.addPercentageBasis(PaxTypeTO.INFANT, rs.getString("INFANT_PERCENTAGE_BASIS_ID"));
					flexiRuleDTO.setRuleComments(rs.getString("RULE_COMMENTS"));
				}

				FlexiRuleFlexibilityDTO flexibilityDTO = new FlexiRuleFlexibilityDTO();
				flexibilityDTO.setFlexibilityTypeId(rs.getInt("FLEXI_RULE_FLEXI_TYPE_ID"));
				flexibilityDTO.setAvailableCount(rs.getInt("ALLOWED_COUNT"));
				flexibilityDTO.setCutOverBufferInMins(rs.getLong("CUT_OVER_MINS"));
				flexibilityDTO.setFlexibilityDescription(rs.getString("FLEXIBILITY_DESC"));
				flexiRuleDTO.addToFlexibilitiesList(flexibilityDTO);

				flexiRuleDTOs.put(flexiRuleDTO.getFlexiCode(), flexiRuleDTO);
			}
			return flexiRuleDTOs;
		}

	}

	/**
	 * This method requote flexi charges (FlexiRuleDTOs) for a given flexiRuleRateIds
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<FlexiRuleDTO> requoteFlexiCharges(Collection<Integer> flexiRuleRateIds) {

		List<FlexiRuleDTO> ondFlexiCharges = new ArrayList<FlexiRuleDTO>();

		log.debug("rebuildFlexiCharges");
		String inStr = Util.buildIntegerInClauseContent(flexiRuleRateIds);

		String sql = "SELECT FLXRATE.FLEXI_RULE_RATE_ID, " + "  FLXRATE.ADULT_CHARGE, " + "  FLXRATE.ADULT_CHARGE_TYPE, "
				+ "  FLXRATE.ADULT_PERCENTAGE_BASIS_ID, " + "  FLXRATE.CHILD_CHARGE, " + "  FLXRATE.CHILD_CHARGE_TYPE, "
				+ "  FLXRATE.CHILD_PERCENTAGE_BASIS_ID, " + "  FLXRATE.INFANT_CHARGE, " + "  FLXRATE.INFANT_CHARGE_TYPE, "
				+ "  FLXRATE.INFANT_PERCENTAGE_BASIS_ID " + " FROM T_FLEXI_RULE_RATE FLXRATE "
				+ "WHERE FLXRATE.FLEXI_RULE_RATE_ID IN(" + inStr + ")";

		if (log.isDebugEnabled()) {
			log.debug("rebuildFlexiCharges sql [" + sql + "]");
		}

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		ondFlexiCharges = (ArrayList<FlexiRuleDTO>) jdbcTemplate.query(sql, new FlexiRequoteResultSetExtractor());

		return ondFlexiCharges;
	}

	public class FlexiRequoteResultSetExtractor implements ResultSetExtractor {

		public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
			List<FlexiRuleDTO> flexiRuleDTOs = new ArrayList<FlexiRuleDTO>();
			FlexiRuleDTO flexiRuleDTO;

			while (rs.next()) {
				flexiRuleDTO = new FlexiRuleDTO();
				flexiRuleDTO.setFlexiRuleRateId(rs.getInt("FLEXI_RULE_RATE_ID"));
				// Adult flexi rate
				flexiRuleDTO.addCharge(PaxTypeTO.ADULT, rs.getDouble("ADULT_CHARGE"));
				flexiRuleDTO.addChargeType(PaxTypeTO.ADULT, rs.getString("ADULT_CHARGE_TYPE"));
				flexiRuleDTO.addPercentageBasis(PaxTypeTO.ADULT, rs.getString("ADULT_PERCENTAGE_BASIS_ID"));
				// Child flexi rate
				flexiRuleDTO.addCharge(PaxTypeTO.CHILD, rs.getDouble("CHILD_CHARGE"));
				flexiRuleDTO.addChargeType(PaxTypeTO.CHILD, rs.getString("CHILD_CHARGE_TYPE"));
				flexiRuleDTO.addPercentageBasis(PaxTypeTO.CHILD, rs.getString("CHILD_PERCENTAGE_BASIS_ID"));
				// Infant flexi rate
				flexiRuleDTO.addCharge(PaxTypeTO.INFANT, rs.getDouble("INFANT_CHARGE"));
				flexiRuleDTO.addChargeType(PaxTypeTO.INFANT, rs.getString("INFANT_CHARGE_TYPE"));
				flexiRuleDTO.addPercentageBasis(PaxTypeTO.INFANT, rs.getString("INFANT_PERCENTAGE_BASIS_ID"));

				flexiRuleDTOs.add(flexiRuleDTO);
			}
			return flexiRuleDTOs;
		}
	}

}
