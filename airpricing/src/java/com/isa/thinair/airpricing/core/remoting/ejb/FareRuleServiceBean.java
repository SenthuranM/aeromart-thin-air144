/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:29:42
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airpricing.core.remoting.ejb;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.ejb.TransactionTimeout;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airpricing.api.criteria.AgentFareRuleSearchCriteria;
import com.isa.thinair.airpricing.api.dto.AgentFareRulesDTO;
import com.isa.thinair.airpricing.api.model.Channel;
import com.isa.thinair.airpricing.api.model.FareRule;
import com.isa.thinair.airpricing.api.model.FareRulePax;
import com.isa.thinair.airpricing.api.model.FareRulePaxFees;
import com.isa.thinair.airpricing.api.service.AirpricingUtils;
import com.isa.thinair.airpricing.core.audit.AuditAirpricing;
import com.isa.thinair.airpricing.core.bl.FareRuleBL;
import com.isa.thinair.airpricing.core.persistence.dao.FareRuleDAO;
import com.isa.thinair.airpricing.core.persistence.dao.FareRuleJdbcDAO;
import com.isa.thinair.airpricing.core.service.bd.FareRuleBDImpl;
import com.isa.thinair.airpricing.core.service.bd.FareRuleBDLocalImpl;
import com.isa.thinair.airpricing.core.util.InternalConstants;
import com.isa.thinair.airreservation.api.dto.OnHoldReleaseTimeDTO;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;

/**
 * @author Byorn
 */
@Stateless
@RemoteBinding(jndiBinding = "FareRuleService.remote")
@LocalBinding(jndiBinding = "FareRuleService.local")
@RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class FareRuleServiceBean extends PlatformBaseSessionBean implements FareRuleBDImpl, FareRuleBDLocalImpl {

	/**
	 * Create a fare rule
	 */
	@Override
	@TransactionTimeout(1200)
	public void createFareRule(FareRule fareRule, boolean splitFares) throws ModuleException {
		try {
			setUserDetails(fareRule);
			if (fareRule.getPaxDetails() != null) {
				setUserDetailsForCollection(fareRule.getPaxDetails());
			}
			/*
			 * if (fareRule.getFees() != null) { setUserDetailsForPaxFeeCollection(fareRule.getPaxFees()); }
			 */
			AuditAirpricing.doAudit(fareRule, getUserPrincipal());
			new FareRuleBL().saveFareRule(fareRule, getUserPrincipal(), splitFares);
		} catch (ModuleException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		} catch (Exception e1) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException("airpricing.technical.error", "airpricing.desc");
		}
	}

	private void setUserDetailsForCollection(Collection<FareRulePax> paxDetails) throws ModuleException {
		Iterator<FareRulePax> Itr = paxDetails.iterator();
		FareRulePax oMFPax;

		while (Itr.hasNext()) {
			oMFPax = (FareRulePax) Itr.next();
			setUserDetails(oMFPax);
		}
	}

	private void setUserDetailsForCollectionsForFare(Collection<FareRulePax> paxDetails) throws ModuleException {
		Iterator<FareRulePax> Itr = paxDetails.iterator();
		FareRulePax oMFPax;

		while (Itr.hasNext()) {
			oMFPax = (FareRulePax) Itr.next();
			setUserDetails(oMFPax);
		}
	}

	private void setUserDetailsForPaxFeeCollection(Collection<FareRulePaxFees> paxFees) throws ModuleException {
		Iterator<FareRulePaxFees> Itr = paxFees.iterator();
		FareRulePaxFees fareRulePaxFees;

		while (Itr.hasNext()) {
			fareRulePaxFees = (FareRulePaxFees) Itr.next();
			setUserDetails(fareRulePaxFees);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void updateFareRule(FareRule fareRule) throws ModuleException {
		try {
			setUserDetails(fareRule);
			if (fareRule.getPaxDetails() != null) {
				setUserDetailsForCollectionsForFare(fareRule.getPaxDetails());
			}
			AuditAirpricing.doAudit(fareRule, getUserPrincipal());
			new FareRuleBL().saveFareRule(fareRule, getUserPrincipal(), true);
		} catch (ModuleException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		} catch (Exception e1) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException("airpricing.technical.error", "airpricing.desc");
		}
	}

	/**
	 * getFareRule FareRule
	 * 
	 * @param fareRuleId
	 * @return
	 * @throws ModuleException
	 * 
	 */
	public FareRule getFareRule(int fareRuleId) throws ModuleException {
		try {
			return lookupFareRuleDAO().getFareRule(fareRuleId);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	/**
	 * getFareRule FareRule
	 * 
	 * @param fareRuleCode
	 * @return
	 * @throws ModuleException
	 * 
	 */
	public FareRule getFareRule(String fareRuleCode) throws ModuleException {
		try {
			return lookupFareRuleDAO().getFareRule(fareRuleCode);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	public Map<Integer, FareRule> getFareRulesMap(Collection<Integer> fareRuleIds) throws ModuleException {
		try {
			return lookupFareRuleDAO().getFareRulesMap(fareRuleIds);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}

	}

	public Page getFareRules(List<ModuleCriterion> criteria, int startIndex, int pageSize, List<String> orderBy)
			throws ModuleException {
		try {
			return lookupFareRuleDAO().getFareRules(criteria, startIndex, pageSize, orderBy);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	/**
	 * Method to get a Page of Agents Master Fare Rule Page
	 * 
	 * @param criteriaList
	 * @param startIndex
	 * @param pageSize
	 * @param orderBy
	 * @return
	 * @throws ModuleException
	 * 
	 */
	public Collection<AgentFareRulesDTO> getAgentFareRules(AgentFareRuleSearchCriteria agentFareRuleSearchCriteria,
			List<String> orderBy) throws ModuleException {
		try {
			return lookupFareRuleJDBCDAO().getAgentFareRules(agentFareRuleSearchCriteria, orderBy);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	/**
	 *
	 */
	public Collection<Channel> getChannels(Collection<Integer> channelIds) throws ModuleException {
		try {
			return lookupFareRuleDAO().getChannels(channelIds);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	/**
	 * Method to Lookup the FareRuleDAO FareRuleDAO
	 * 
	 * @return
	 */
	private FareRuleDAO lookupFareRuleDAO() {

		FareRuleDAO dao = null;
		try {
			dao = (FareRuleDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FARE_RULE_DAO);
		} catch (Exception ex) {
			System.out.println("ERROR IN LOOKUP");
		}
		return dao;
	}

	/**
	 * Method to Lookup the FareRuleJdbcDAO FareRuleJdbcDAO
	 * 
	 * @return
	 */
	private FareRuleJdbcDAO lookupFareRuleJDBCDAO() {

		FareRuleJdbcDAO dao = null;
		try {
			dao = (FareRuleJdbcDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.AGENT_FARE_RULE);
		} catch (Exception ex) {
			System.out.println("ERROR IN LOOKUP");
		}
		return dao;
	}

	public void deleteFareRule(int fareRuleID) throws ModuleException {
		try {
			// AuditAirpricing.doAudit(mfrCode, AuditAirpricing.FARE_RULE,getUserPrincipal());
			lookupFareRuleDAO().deleteFareRule(fareRuleID);
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		} catch (Exception e1) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException("airpricing.technical.error", "airpricing.desc");
		}
	}

	/**
	 *
	 */
	public Collection<Channel> getAllSalesChannels() throws ModuleException {
		try {
			return lookupFareRuleDAO().getAllSalesChannels();
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	public FareRule getFareRuleForFareID(Integer fareId) throws ModuleException {

		try {
			return lookupFareRuleDAO().getFareRuleForFareID(fareId);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	public Collection<OnHoldReleaseTimeDTO> getOHDRelTimeDTOForFareRuleId(Integer fareRuleId) throws ModuleException {
		try {
			return lookupFareRuleJDBCDAO().getOHDRelTimeDTO(fareRuleId);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	public Collection<Integer> getFareIdsForFareRule(Integer fareRuleId) throws ModuleException {
		try {
			return lookupFareRuleJDBCDAO().getFareIdsForFareRule(fareRuleId);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

}
