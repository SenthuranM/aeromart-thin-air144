package com.isa.thinair.airpricing.core.persistence.jdbc.farerulejdbcdao;

/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airpricing.api.dto.AgentFareRulesDTO;
import com.isa.thinair.airpricing.api.dto.FareRuleSummaryDTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;

/**
 * Class that JdbcTemplate will use.
 * 
 * @author Byorn
 * 
 */
public class AgentFareRuleResultsExtractor implements ResultSetExtractor {

	private static final String AGENT_CODE = "agent_code";

	private static final String AGENT_NAME = "agent_name";

	private static final String FARE_RULE_CODE = "fare_rule_code";

	private static final String FARE_RULE_ID = "fare_rule_id";

	private static final String DESCRIPTION = "fare_rule_description";

	private static final String FARE_BASIS_CODE = "fare_basis_code";

	private static final String STATUS = "status";

	private static final String FARE_ID = "fare_id";

	private List<AgentFareRulesDTO> list;

	/**
	 * Will return the List of Records extracted from the resultsSet List
	 * 
	 * @return
	 */
	public List<AgentFareRulesDTO> getList() {
		return list;
	}

	/**
	 * Method that implements the ResultSetExtractor interface. This method is internally used by the JdbcTemplate.
	 * Method is used for accessing the resultSet and populating the DTO and adding it to the results LIst.
	 */
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

		list = new ArrayList<AgentFareRulesDTO>();

		AgentFareRulesDTO agentFareRulesDTO = null;
		FareRuleSummaryDTO fareRuleSummarryDTO = null;

		while (rs.next()) {
			/** Check for first Record * */

			if (agentFareRulesDTO == null) {
				agentFareRulesDTO = new AgentFareRulesDTO();
				agentFareRulesDTO.setAgentCode(BeanUtils.nullHandler(rs.getString(AGENT_CODE)));
				agentFareRulesDTO.setAgentName(BeanUtils.nullHandler(rs.getString(AGENT_NAME)));
				fareRuleSummarryDTO = new FareRuleSummaryDTO();
				fareRuleSummarryDTO.setFareRuleCode(BeanUtils.nullHandler(rs.getString(FARE_RULE_CODE)));
				fareRuleSummarryDTO.setFareRuleId(rs.getInt(FARE_RULE_ID));
				fareRuleSummarryDTO.setDescription(BeanUtils.nullHandler(rs.getString(DESCRIPTION)));
				fareRuleSummarryDTO.setFareBasisCode(BeanUtils.nullHandler(rs.getString(FARE_BASIS_CODE)));
				fareRuleSummarryDTO.setStatus(BeanUtils.nullHandler(rs.getString(STATUS)));

				if (rs.getObject(FARE_ID) == null) {
					fareRuleSummarryDTO.setLinkedToNonExpiredFare(false);
				} else {
					fareRuleSummarryDTO.setLinkedToNonExpiredFare(true);
				}

				/** Add the farerule to the agent code * */
				List<FareRuleSummaryDTO> fareRules = new ArrayList<FareRuleSummaryDTO>();
				fareRules.add(fareRuleSummarryDTO);
				agentFareRulesDTO.setFareRulesSummary(fareRules);

				/** add the agent dto to the list * */
				list.add(agentFareRulesDTO);
				continue;
			}

			/**
			 * Check if another record exist for the same agent i.e if agentcode has many fare rule codes.
			 */
			String newagentCode = BeanUtils.nullHandler(rs.getString(AGENT_CODE));
			String fareRuleCode = BeanUtils.nullHandler(rs.getString(FARE_RULE_CODE));

			AgentFareRulesDTO agentFareRulesDTO2 = agentFareRulesDTO;

			if (agentFareRulesDTO2.getAgentCode().equals(newagentCode)) {
				list.remove(list.lastIndexOf(agentFareRulesDTO));

				agentFareRulesDTO2.setAgentCode(BeanUtils.nullHandler(rs.getString(AGENT_CODE)));
				agentFareRulesDTO2.setAgentName(BeanUtils.nullHandler(rs.getString(AGENT_NAME)));
				FareRuleSummaryDTO fareRuleSummaryDTO2 = new FareRuleSummaryDTO();
				fareRuleSummaryDTO2.setFareRuleCode(BeanUtils.nullHandler(rs.getString(FARE_RULE_CODE)));
				fareRuleSummaryDTO2.setFareRuleId(rs.getInt(FARE_RULE_ID));
				fareRuleSummaryDTO2.setDescription(BeanUtils.nullHandler(rs.getString(DESCRIPTION)));
				fareRuleSummaryDTO2.setFareBasisCode(BeanUtils.nullHandler(rs.getString(FARE_BASIS_CODE)));
				fareRuleSummaryDTO2.setStatus(BeanUtils.nullHandler(rs.getString(STATUS)));
				if (rs.getObject(FARE_ID) == null) {
					fareRuleSummaryDTO2.setLinkedToNonExpiredFare(false);
				} else {
					fareRuleSummaryDTO2.setLinkedToNonExpiredFare(true);
				}

				Collection<FareRuleSummaryDTO> fareRules = agentFareRulesDTO2.getFareRulesSummary();

				if (isNewFareRule(fareRuleCode, fareRules)) {
					fareRules.add(fareRuleSummaryDTO2);
				}

				agentFareRulesDTO2.setFareRulesSummary(fareRules);

				list.add(agentFareRulesDTO2);
				continue;
			}

			/** if it is a new Agent Code then add it to the list * */
			agentFareRulesDTO = new AgentFareRulesDTO();
			agentFareRulesDTO.setAgentCode(BeanUtils.nullHandler(rs.getString(AGENT_CODE)));
			agentFareRulesDTO.setAgentName(BeanUtils.nullHandler(rs.getString(AGENT_NAME)));
			FareRuleSummaryDTO fareRuleSummarryDTO3 = new FareRuleSummaryDTO();
			fareRuleSummarryDTO3.setFareRuleCode(BeanUtils.nullHandler(rs.getString(FARE_RULE_CODE)));
			fareRuleSummarryDTO3.setFareRuleId(rs.getInt(FARE_RULE_ID));
			fareRuleSummarryDTO3.setDescription(BeanUtils.nullHandler(rs.getString(DESCRIPTION)));
			fareRuleSummarryDTO3.setFareBasisCode(BeanUtils.nullHandler(rs.getString(FARE_BASIS_CODE)));
			fareRuleSummarryDTO3.setStatus(BeanUtils.nullHandler(rs.getString(STATUS)));

			if (rs.getObject(FARE_ID) == null) {
				fareRuleSummarryDTO3.setLinkedToNonExpiredFare(false);
			} else {
				fareRuleSummarryDTO3.setLinkedToNonExpiredFare(true);
			}

			/** Add the farerule to the agent code * */
			List<FareRuleSummaryDTO> fareRules1 = new ArrayList<FareRuleSummaryDTO>();
			fareRules1.add(fareRuleSummarryDTO3);
			agentFareRulesDTO.setFareRulesSummary(fareRules1);

			/** add the agent dto to the list * */
			list.add(agentFareRulesDTO);

		}// end of while

		return list;
	}

	private boolean isNewFareRule(String fareRuleCode, Collection<FareRuleSummaryDTO> fareRules) {
		String frCode = BeanUtils.nullHandler(fareRuleCode);
		Iterator<FareRuleSummaryDTO> iterator = fareRules.iterator();
		boolean status = true;
		while (iterator.hasNext()) {
			FareRuleSummaryDTO fareRuleSummaryDTO = (FareRuleSummaryDTO) iterator.next();
			if (frCode.equals(BeanUtils.nullHandler(fareRuleSummaryDTO.getFareRuleCode()))) {
				status = false;
			}

		}

		return status;

	}
}
