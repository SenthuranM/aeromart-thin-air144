package com.isa.thinair.airpricing.core.persistence.dao;

import java.util.Collection;

import com.isa.thinair.airpricing.api.model.Proration;

/**
 * 
 * @author chamila
 * 
 */
public interface FareProrationDAO {
	/**
	 * Used to delete proration collection
	 * @param removeList
	 */
	void deleteAllProrations(Collection<Proration> removeList);

}
