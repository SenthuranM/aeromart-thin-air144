package com.isa.thinair.airpricing.core.util;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airpricing.api.model.FareRule;
import com.isa.thinair.airpricing.api.model.FareRuleFee;
import com.isa.thinair.airpricing.api.model.FareRulePax;
import com.isa.thinair.airpricing.api.model.InboundDepartureFrequency;
import com.isa.thinair.airpricing.api.model.OutboundDepartureFrequency;
import com.isa.thinair.airpricing.api.service.AirpricingUtils;
import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.Tracking;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;

/**
 * 
 * @author Byorn
 * 
 */
public class PricingUtils {

	public PricingUtils() {
	}

	
	public static void validateFareRule(FareRule fareRule) throws ModuleException {
		Set<Integer> visibileSalesChannels = fareRule.getVisibleChannelIds();
		if (visibileSalesChannels.contains(new Integer(SalesChannelsUtil
				.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_TRAVELAGNET_KEY)))
				&& (fareRule.getVisibileAgentCodes() == null || fareRule.getVisibileAgentCodes().size() == 0)) {
			throw new ModuleException("airpricing.visibagent.none");
		}
	}

	
	public static FareRule clearAgentsListIfVisibilityIsNotAgent(FareRule fareRule) {
		Set<Integer> visibileSalesChannels = fareRule.getVisibleChannelIds();
		if (!visibileSalesChannels.contains(new Integer(SalesChannelsUtil
				.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_TRAVELAGNET_KEY)))) {
			fareRule.setVisibileAgentCodes(null);
		}
		return fareRule;
	}
	
	public static FareRule clearPOSListIfVisibilityNotWeb(FareRule fareRule){
		Set<Integer> visibileSalesChannels = fareRule.getVisibleChannelIds();
		if (visibileSalesChannels!=null && !visibileSalesChannels.contains(new Integer(SalesChannelsUtil
				.getSalesChannelCode(SalesChannelsUtil.SALES_CHANNEL_WEB_KEY)))) {
			fareRule.setPointOfSale(null);
			fareRule.setApplicableToAllCountries(Boolean.FALSE);
		}
		return fareRule;
	}

	/**
	 * constructViaPointSearch String
	 * 
	 * @param originAirportCode
	 * @param destinationAirportCode
	 * @param viapoints
	 * @param isExactCombination
	 * @return
	 */
	public static String constructViaPointSearch(String originAirportCode, String destinationAirportCode, List<String> viapoints,
			boolean isExactCombination) {
		String strViapoints = "";

		if (originAirportCode != null && destinationAirportCode != null && viapoints != null) {

			Iterator<String> iterator = viapoints.iterator();
			strViapoints = originAirportCode;
			while (iterator.hasNext()) {
				String viapoint = iterator.next().toString();
				if (viapoint.equals("")) {
					strViapoints = strViapoints + "/___";
					continue;
				}
				strViapoints = strViapoints + "/" + viapoint;
			}

			if (isExactCombination) {
				strViapoints = strViapoints + "/" + destinationAirportCode;
			} else {
				strViapoints = strViapoints + "/%" + destinationAirportCode;
			}
		}

		if (originAirportCode != null && destinationAirportCode != null && viapoints == null) {
			if (isExactCombination) {
				strViapoints = originAirportCode + "/" + destinationAirportCode;
			} else {
				strViapoints = originAirportCode + "/%" + destinationAirportCode;
			}

		}
		return strViapoints;
	}

	/**
	 * Construct IN string for HQL or SQL String
	 * 
	 * @param codes
	 * @return 'AAA', 'BBB', 'CCC'
	 */
	@SuppressWarnings("rawtypes")
	public static String constructINString(Collection codes) {
		String inStr = "";
		Iterator iter = codes.iterator();
		while (iter.hasNext()) {

			inStr = inStr.equals("") ? "'" + iter.next().toString() + "'" : inStr + "," + "'" + iter.next().toString() + "'";

		}
		return inStr;
	}

	@SuppressWarnings("rawtypes")
	public static String constructINStringForInts(Collection codes) {
		String inStr = "";
		Iterator iter = codes.iterator();
		while (iter.hasNext()) {

			inStr = inStr.equals("") ? iter.next().toString() : inStr + "," + iter.next().toString();

		}
		return inStr;
	}

	public static Frequency convertToPlatFormsFrequency(com.isa.thinair.airpricing.api.model.Frequency frequency) {
		Frequency platFrequency = new Frequency();
		platFrequency.setDay0(frequency.getDay0() == 1 ? true : false);
		platFrequency.setDay1(frequency.getDay1() == 1 ? true : false);
		platFrequency.setDay2(frequency.getDay2() == 1 ? true : false);
		platFrequency.setDay3(frequency.getDay3() == 1 ? true : false);
		platFrequency.setDay4(frequency.getDay4() == 1 ? true : false);
		platFrequency.setDay5(frequency.getDay5() == 1 ? true : false);
		platFrequency.setDay6(frequency.getDay6() == 1 ? true : false);
		return platFrequency;

	}

	public static com.isa.thinair.airpricing.api.model.Frequency convertToPricingFrequency(Frequency platfrequency,
			boolean isInboudFrequency) {
		// com.isa.thinair.airpricing.api.model.Frequency fq = new com.isa.thinair.airpricing.api.model.Frequency();
		if (isInboudFrequency) {
			InboundDepartureFrequency inboundDepartureFrequency = new InboundDepartureFrequency();
			inboundDepartureFrequency.setDay0(platfrequency.getDay0() == true ? 1 : 0);
			inboundDepartureFrequency.setDay1(platfrequency.getDay1() == true ? 1 : 0);
			inboundDepartureFrequency.setDay2(platfrequency.getDay2() == true ? 1 : 0);
			inboundDepartureFrequency.setDay3(platfrequency.getDay3() == true ? 1 : 0);
			inboundDepartureFrequency.setDay4(platfrequency.getDay4() == true ? 1 : 0);
			inboundDepartureFrequency.setDay5(platfrequency.getDay5() == true ? 1 : 0);
			inboundDepartureFrequency.setDay6(platfrequency.getDay6() == true ? 1 : 0);
			return inboundDepartureFrequency;
		} else {
			OutboundDepartureFrequency outboundDepartureFrequency = new OutboundDepartureFrequency();
			outboundDepartureFrequency.setDay0(platfrequency.getDay0() == true ? 1 : 0);
			outboundDepartureFrequency.setDay1(platfrequency.getDay1() == true ? 1 : 0);
			outboundDepartureFrequency.setDay2(platfrequency.getDay2() == true ? 1 : 0);
			outboundDepartureFrequency.setDay3(platfrequency.getDay3() == true ? 1 : 0);
			outboundDepartureFrequency.setDay4(platfrequency.getDay4() == true ? 1 : 0);
			outboundDepartureFrequency.setDay5(platfrequency.getDay5() == true ? 1 : 0);
			outboundDepartureFrequency.setDay6(platfrequency.getDay6() == true ? 1 : 0);
			return outboundDepartureFrequency;
		}

	}

	public static String[] getAdvBkDDHHMM(int AdvBkTimeInMins) {
		String[] advBkDDHHMM = { "0", "0", "0" };
		int days = AdvBkTimeInMins / (24 * 60);
		int hours = (AdvBkTimeInMins % (24 * 60)) / 60;
		int mins = (AdvBkTimeInMins % (24 * 60)) % 60;

		advBkDDHHMM[0] = (days == 0) ? "0" : String.valueOf(days);
		advBkDDHHMM[1] = (hours == 0) ? "0" : String.valueOf(hours);
		advBkDDHHMM[2] = (mins == 0) ? "0" : String.valueOf(mins);

		return advBkDDHHMM;
	}
	
	public static String[] getStayOverDDHHMM(Long stayOverTimeInMins) {
		String[] stayOverDDHHMM = { "0", "0", "0" };
		int days = (int) (stayOverTimeInMins / (24 * 60));
		int hours = (int) ((stayOverTimeInMins % (24 * 60)) / 60);
		int mins = (int) ((stayOverTimeInMins % (24 * 60)) % 60);

		stayOverDDHHMM[0] = (days == 0) ? "0" : String.valueOf(days);
		stayOverDDHHMM[1] = (hours == 0) ? "0" : String.valueOf(hours);
		stayOverDDHHMM[2] = (mins == 0) ? "0" : String.valueOf(mins);

		return stayOverDDHHMM;
	}

	/**
	 * setFareRuleChargesInSelectedCurr method update the FareRule charges with the latest exchange rate when fare rule
	 * is not defined in BASE currency
	 * */
	public static void setFareRuleChargesInSelectedCurr(FareRule fareRule) throws ModuleException {

		String baseCurrency = AppSysParamsUtil.getBaseCurrency();
		String selectedCurrency = fareRule.getLocalCurrencyCode();
		if (selectedCurrency != null && !selectedCurrency.equals(baseCurrency)) {

			BigDecimal modAmountInLocal = new BigDecimal(fareRule.getModChargeAmountInLocal());
			BigDecimal cnxAmountInLocal = new BigDecimal(fareRule.getCnxChargeAmountInLocal());

			BigDecimal modAmountInBase = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal cnxAmountInBase = AccelAeroCalculator.getDefaultBigDecimalZero();

			CurrencyExchangeRate currExRate = AirpricingUtils.getCommonMasterBD().getExchangeRate(
					fareRule.getLocalCurrencyCode(), new Date());

			if (currExRate != null) {
				BigDecimal exchangeRate = currExRate.getExrateBaseToCurNumber();

				if (fareRule.getModificationChargeType() != null && fareRule.getModificationChargeType().equalsIgnoreCase("V")) {
					modAmountInBase = AccelAeroRounderPolicy.convertAndRound(modAmountInLocal, exchangeRate, null, null);
					fareRule.setModificationChargeAmount(modAmountInBase.doubleValue());
				}

				if (fareRule.getCancellationChargeType() != null && fareRule.getCancellationChargeType().equalsIgnoreCase("V")) {
					cnxAmountInBase = AccelAeroRounderPolicy.convertAndRound(cnxAmountInLocal, exchangeRate, null, null);
					fareRule.setCancellationChargeAmount(cnxAmountInBase.doubleValue());
				}

				String agentCommissionType = fareRule.getAgentCommissionType();
				if (agentCommissionType != null && agentCommissionType.equals(FareRule.CHARGE_TYPE_V)) {
					Double comInLocal = fareRule.getAgentCommissionAmountInLocal();
					if (comInLocal != null) {
						BigDecimal commissionInBase = AccelAeroRounderPolicy.convertAndRound(
								BigDecimal.valueOf(fareRule.getAgentCommissionAmountInLocal()), exchangeRate, null, null);
						fareRule.setAgentCommissionAmount(commissionInBase.doubleValue());
					}
				}

				Collection<FareRulePax> paxDetailsColl = fareRule.getPaxDetails();

				if (paxDetailsColl != null && paxDetailsColl.size() > 0) {
					Iterator<FareRulePax> paxDetailsItr = paxDetailsColl.iterator();

					while (paxDetailsItr.hasNext()) {
						FareRulePax fareRulePax = paxDetailsItr.next();

						BigDecimal noShowChargeInLocal = new BigDecimal(fareRulePax.getNoShowChrgeInLocal());
						BigDecimal noShowChargeInBase = AccelAeroCalculator.getDefaultBigDecimalZero();

						if (fareRulePax.getNoShowChgBasis().equalsIgnoreCase("V")) {
							noShowChargeInBase = AccelAeroRounderPolicy.convertAndRound(noShowChargeInLocal, exchangeRate, null,
									null);
							fareRulePax.setNoShowChrge(noShowChargeInBase.doubleValue());
						}
					}
				}

				Collection<FareRuleFee> feesColl = fareRule.getFees();
				if (feesColl != null && feesColl.size() > 0) {
					Iterator<FareRuleFee> feesItr = feesColl.iterator();
					while (feesItr.hasNext()) {
						FareRuleFee fareRulefee = feesItr.next();

						BigDecimal feeAmountInLocal = fareRulefee.getChargeAmountInLocal();

						BigDecimal feeAmountInBase = AccelAeroCalculator.getDefaultBigDecimalZero();

						if (fareRulefee.getChargeAmountType() != null && fareRulefee.getChargeAmountType().equalsIgnoreCase("V")) {
							feeAmountInBase = AccelAeroRounderPolicy.convertAndRound(feeAmountInLocal, exchangeRate, null, null);
							fareRulefee.setChargeAmount(feeAmountInBase);
						}
					}
				}

			}

		}
	}

	public static Tracking setUserDetails(Tracking tracking, UserPrincipal userPrincipal) {
		  if (tracking.getVersion() < 0) { // save operation
				tracking.setCreatedBy(userPrincipal.getUserId() == null ? "" : userPrincipal.getUserId());
				tracking.setCreatedDate(new Date());

				if (tracking.getModifiedBy() == null) {
					tracking.setModifiedBy(userPrincipal.getUserId() == null ? "" : userPrincipal.getUserId());
					tracking.setModifiedDate(new Date());
				}
			} else { // update operation
				tracking.setModifiedBy(userPrincipal.getUserId() == null ? "" : userPrincipal.getUserId());
				tracking.setModifiedDate(new Date());
			}		
		  
		 
		return tracking;
	}
}
