/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:12:34
 * 
 * 
 * ===============================================================================
 */

package com.isa.thinair.airpricing.core.persistence.hibernate;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airmaster.api.model.Baggage;
import com.isa.thinair.airpricing.api.criteria.BaggageTemplateSearchCriteria;
import com.isa.thinair.airpricing.api.criteria.ChargeSearchCriteria;
import com.isa.thinair.airpricing.api.criteria.ChargeTemplateSearchCriteria;
import com.isa.thinair.airpricing.api.criteria.MealTemplateSearchCriteria;
import com.isa.thinair.airpricing.api.criteria.ONDChargeSearchCriteria;
import com.isa.thinair.airpricing.api.criteria.ONDCodeSearchCriteria;
import com.isa.thinair.airpricing.api.criteria.SSRChargesSearchCriteria;
import com.isa.thinair.airpricing.api.dto.BaggageTemplateTo;
import com.isa.thinair.airpricing.api.dto.ChargeTO;
import com.isa.thinair.airpricing.api.model.BaggageCharge;
import com.isa.thinair.airpricing.api.model.BaggageTemplate;
import com.isa.thinair.airpricing.api.model.Charge;
import com.isa.thinair.airpricing.api.model.ChargeAgent;
import com.isa.thinair.airpricing.api.model.ChargeApplicableCharges;
import com.isa.thinair.airpricing.api.model.ChargePos;
import com.isa.thinair.airpricing.api.model.ChargeRate;
import com.isa.thinair.airpricing.api.model.ChargeTemplate;
import com.isa.thinair.airpricing.api.model.MCO;
import com.isa.thinair.airpricing.api.model.MCOServices;
import com.isa.thinair.airpricing.api.model.MealCharge;
import com.isa.thinair.airpricing.api.model.MealTemplate;
import com.isa.thinair.airpricing.api.model.ONDBaggage;
import com.isa.thinair.airpricing.api.model.ONDBaggageChargeTemplate;
import com.isa.thinair.airpricing.api.model.ONDBaggageTemplate;
import com.isa.thinair.airpricing.api.model.ReportChargeGroup;
import com.isa.thinair.airpricing.api.model.SSRCharge;
import com.isa.thinair.airpricing.api.model.SSRChargesCOS;
import com.isa.thinair.airpricing.api.model.SeatCharge;
import com.isa.thinair.airpricing.api.service.AirpricingUtils;
import com.isa.thinair.airpricing.api.util.AirPricingCustomConstants;
import com.isa.thinair.airpricing.api.util.BaggageUtil;
import com.isa.thinair.airpricing.core.persistence.dao.ChargeDAO;
import com.isa.thinair.airpricing.core.util.PricingUtils;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.gdsservices.api.model.EticketInfoTO;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;

/**
 * BHive: Charge module
 * 
 * ChargeDAOImpl is the ChargetDAO implementatiion for hibernate
 * 
 * @isa.module.dao-impl dao-name="ChargeDAO"
 */

public class ChargeDAOImpl extends PlatformBaseHibernateDaoSupport implements ChargeDAO {

	private final Log log = LogFactory.getLog(ChargeDAOImpl.class);

	private DataSource getDatasource() {
		LookupService lookup = LookupServiceFactory.getInstance();
		return (DataSource) lookup.getBean(PlatformBeanUrls.Commons.DEFAULT_DATA_SOURCE_BEAN);
	}

	/**
	 * method to get a Charge for id
	 */
	@Override
	public Charge getCharge(String id) {

		if (log.isDebugEnabled()) {
			log.debug("getCharge for id" + id);
		}

		return (Charge) get(Charge.class, id);
	}

	/**
	 * method to save a charge
	 */
	@Override
	public void saveCharge(Charge charge) {
		if (log.isDebugEnabled()) {
			log.debug("saveCharge for id" + charge.getChargeCode());
		}
		hibernateSaveOrUpdate(charge);

	}

	/**
	 * method to remove a Charge for a given chargeCode
	 */
	@Override
	public void removeCharge(String ChargeCode) {

		if (log.isDebugEnabled()) {
			log.debug("removeCharge for id" + ChargeCode);
		}
		Object charge = load(Charge.class, ChargeCode);
		delete(charge);

	}

	/**
	 * method to get Charges with paging and criteria using HQL joins.
	 * 
	 * @return Page
	 * @param ChargeSearchCriteria
	 * @param startIndex
	 * @param pageSize
	 * @param orderbyfields
	 */
	@Override
	public Page getCharges(ChargeSearchCriteria chargeSearchCriteria, int startIndex, int pageSize, List<String> orderbyfields) {

		/**
		 * getHQLForCharges method will return a String array of 3 values s[0] = The Main SQL with the generated where
		 * clause s[1] = The SQL for extracting the count for the selected criteria s[3] = will return ("y" or "n")
		 * checks if a search criteria for is necessary for "chargerate effective-from and effective-to dates" note:
		 * This is because from the web page if the user does not wish to search by any date
		 */
		String s[] = getHQLForCharges(chargeSearchCriteria, orderbyfields);
		String hql = s[0];
		String hqlCount = s[1];
		String hasDateSearch = s[2];

		/** -------------------------- **/

		Query query = getSession().createQuery(hql).setFirstResult(startIndex).setMaxResults(pageSize);

		Query queryCount = getSession().createQuery(hqlCount);

		try {
			if (chargeSearchCriteria.getChargeCode() != null) {
				query.setParameter("chargecode", chargeSearchCriteria.getChargeCode());
				queryCount.setParameter("chargecode", chargeSearchCriteria.getChargeCode());
			}

			if (chargeSearchCriteria.getChargeCategoryCode() != null) {
				query.setParameter("chargecategorycode", chargeSearchCriteria.getChargeCategoryCode());
				queryCount.setParameter("chargecategorycode", chargeSearchCriteria.getChargeCategoryCode());
			}

			if (chargeSearchCriteria.getChargeGroupCode() != null) {
				query.setParameter("chargegroupcode", chargeSearchCriteria.getChargeGroupCode());
				queryCount.setParameter("chargegroupcode", chargeSearchCriteria.getChargeGroupCode());
			}

			if (chargeSearchCriteria.getChargeStatus() != null && !chargeSearchCriteria.getChargeStatus().equals("All")) {
				query.setParameter("status", chargeSearchCriteria.getChargeStatus());
				queryCount.setParameter("status", chargeSearchCriteria.getChargeStatus());
			}

			if (hasDateSearch.equals("y")) {
				if (chargeSearchCriteria.getFromDate() != null && chargeSearchCriteria.getToDate() == null) {
					query.setParameter("chargeeffectivefromdate", chargeSearchCriteria.getFromDate());
					queryCount.setParameter("chargeeffectivefromdate", chargeSearchCriteria.getFromDate());
				}

				if (chargeSearchCriteria.getFromDate() == null && chargeSearchCriteria.getToDate() != null) {
					query.setParameter("chargeeffectivetodate", chargeSearchCriteria.getToDate());
					queryCount.setParameter("chargeeffectivetodate", chargeSearchCriteria.getToDate());
				}

				if (chargeSearchCriteria.getFromDate() != null && chargeSearchCriteria.getToDate() != null) {
					query.setParameter("chargeeffectivefromdate", chargeSearchCriteria.getFromDate());
					query.setParameter("chargeeffectivetodate", chargeSearchCriteria.getToDate());
					queryCount.setParameter("chargeeffectivefromdate", chargeSearchCriteria.getFromDate());
					queryCount.setParameter("chargeeffectivetodate", chargeSearchCriteria.getToDate());
				}

			}

		} catch (Exception e) {

			throw new CommonsDataAccessException(e, null);
		}
		log.debug("Page getCharges with criteria");

		/** The duplicate records will be filtered out **/
		/** HIBERNATE WONT return distinct records when using distinct keyword with left outer join **/
		// Set distinctCount = new HashSet(queryCount.list());

		// Set distinctResults = new HashSet(query.list());

		return new Page(queryCount.list().size(), startIndex, startIndex + pageSize, query.list());
	}

	/**
	 * DAOImpl Method to Load all the Charges Related to an OND
	 * 
	 * @param ONDChargeSearchCriteria
	 * @param startIndex
	 * @param pageSize
	 * @param orderBy
	 * @return Page
	 */
	@Override
	public Page getONDCharges(ONDChargeSearchCriteria criteria, int startIndex, int pageSize, List<String> orderBy) {

		log.debug("START: Page getCharges with criteria");
		String s[] = getHQLForONDCharges(criteria, orderBy);
		String hql = s[0];
		String hqlCount = s[1];
		String hasDateSearch = s[2];
		String ondCode = s[3];

		if (log.isDebugEnabled()) {
			log.debug("SQL : " + hql);
		}

		Query query = getSession().createQuery(hql).setFirstResult(startIndex).setMaxResults(pageSize);

		Query queryCount = getSession().createQuery(hqlCount);

		try {
			if (criteria.getOriginAirportCode() != null && criteria.getDestinationAirportCode() != null) {
				query.setParameter("ondcode", ondCode);
				queryCount.setParameter("ondcode", ondCode);
			}

			if (criteria.getChargeCategory() != null) {
				query.setParameter("chargeCategory", criteria.getChargeCategory());
				queryCount.setParameter("chargeCategory", criteria.getChargeCategory());
			}

			if (criteria.getPosStation() != null) {
				query.setParameter("pos", criteria.getPosStation());
				queryCount.setParameter("pos", criteria.getPosStation());
			}

			if (hasDateSearch.equals("y")) {
				if (criteria.getEffectiveFromDate() != null && criteria.getEffectiveToDate() == null) {
					query.setParameter("chargeeffectivefromdate", criteria.getEffectiveFromDate());
					queryCount.setParameter("chargeeffectivefromdate", criteria.getEffectiveFromDate());
				}

				if (criteria.getEffectiveFromDate() == null && criteria.getEffectiveToDate() != null) {
					query.setParameter("chargeeffectivetodate", criteria.getEffectiveToDate());
					queryCount.setParameter("chargeeffectivetodate", criteria.getEffectiveToDate());
				}

				if (criteria.getEffectiveFromDate() != null && criteria.getEffectiveToDate() != null) {
					query.setParameter("chargeeffectivefromdate", criteria.getEffectiveFromDate());
					query.setParameter("chargeeffectivetodate", criteria.getEffectiveToDate());
					queryCount.setParameter("chargeeffectivefromdate", criteria.getEffectiveFromDate());
					queryCount.setParameter("chargeeffectivetodate", criteria.getEffectiveToDate());
				}

			}

		} catch (Exception e) {

			throw new CommonsDataAccessException(e, null);
		}

		log.debug("EXIT Page getCharges with criteria");
		return new Page(queryCount.list().size(), startIndex, startIndex + pageSize, query.list());

	}

	/**
	 * Method will return (1) the Main HQL with the Where statement (2) the HQL for extracting the total count (3) a
	 * boolean "y/n" to see if date search has been selected String[]
	 * 
	 * @param chargeSearchCriteria
	 * @param orderbyfields
	 * @return
	 */
	private String[] getHQLForCharges(ChargeSearchCriteria chargeSearchCriteria, List<String> orderbyfields) {
		String hqlCount = "select distinct charge.chargeCode from Charge charge left join charge.chargeRates chargerates";

		String hql = "select distinct charge from Charge charge left join charge.chargeRates chargerates";

		String criteriaHql = "";// where chargerates.chargeCode=charge.chargeCode ";

		String hasDateSearch = "y";
		/** --> NOTE: if user leaves from-date and to-date null, i.e no search selection for dates **/

		/** If Charge Code has been selected in the search criteria **/
		if (chargeSearchCriteria.getChargeCode() != null) {
			if (criteriaHql.equals("")) {
				criteriaHql = criteriaHql + " where charge.chargeCode=:chargecode";
			} else {
				criteriaHql = criteriaHql + " and charge.chargeCode=:chargecode";
			}
		}

		/** If Charge Category Code has been selected in the search criteria **/
		if (chargeSearchCriteria.getChargeCategoryCode() != null) {
			if (criteriaHql.equals("")) {
				criteriaHql = criteriaHql + " where charge.chargeCategoryCode=:chargecategorycode";
			} else {
				criteriaHql = criteriaHql + " and charge.chargeCategoryCode=:chargecategorycode";
			}
		}

		/** If Charge Group Code has been selected in the search criteria **/
		if (chargeSearchCriteria.getChargeGroupCode() != null) {
			if (criteriaHql.equals("")) {
				criteriaHql = criteriaHql + " where charge.chargeGroupCode=:chargegroupcode";
			} else {
				criteriaHql = criteriaHql + " and charge.chargeGroupCode=:chargegroupcode";
			}
		}

		/** If Charge Status has been selected in the search criteria **/
		if (chargeSearchCriteria.getChargeStatus() != null) {
			if (!chargeSearchCriteria.getChargeStatus().equals("All")) {
				if (criteriaHql.equals("")) {
					criteriaHql = criteriaHql + " where charge.status=:status";
				} else {
					criteriaHql = criteriaHql + " and charge.status=:status";
				}
			}
		} else {
			if (criteriaHql.equals("")) {
				criteriaHql = " where charge.status='ACT'";
			} else {
				criteriaHql = " and charge.status='ACT'";
			}
		}

		/** if either one date is selected for searching it will be compared with the other date's current time **/
		if (chargeSearchCriteria.getFromDate() != null && chargeSearchCriteria.getToDate() != null) {
			if (criteriaHql.equals("")) {
				criteriaHql = criteriaHql
						+ " where ((chargerates.chargeEffectiveFromDate>=:chargeeffectivefromdate and "
						+ " chargerates.chargeEffectiveToDate>=:chargeeffectivetodate and chargerates.chargeEffectiveFromDate<=:chargeeffectivetodate) or "
						+ " (chargerates.chargeEffectiveFromDate<=:chargeeffectivefromdate and "
						+ " chargerates.chargeEffectiveToDate<=:chargeeffectivetodate and chargerates.chargeEffectiveToDate>=:chargeeffectivefromdate) or"
						+ " (chargerates.chargeEffectiveFromDate<=:chargeeffectivefromdate and "
						+ " chargerates.chargeEffectiveToDate>=:chargeeffectivetodate) or"
						+ " (chargerates.chargeEffectiveFromDate>=:chargeeffectivefromdate and "
						+ " chargerates.chargeEffectiveToDate<=:chargeeffectivetodate)) ";
			} else {
				criteriaHql = criteriaHql
						+ " and ((chargerates.chargeEffectiveFromDate>=:chargeeffectivefromdate and "
						+ " chargerates.chargeEffectiveToDate>=:chargeeffectivetodate and chargerates.chargeEffectiveFromDate<=:chargeeffectivetodate) or "
						+ " (chargerates.chargeEffectiveFromDate<=:chargeeffectivefromdate and "
						+ " chargerates.chargeEffectiveToDate<=:chargeeffectivetodate and chargerates.chargeEffectiveToDate>=:chargeeffectivefromdate) or"
						+ " (chargerates.chargeEffectiveFromDate<=:chargeeffectivefromdate and "
						+ " chargerates.chargeEffectiveToDate>=:chargeeffectivetodate) or"
						+ " (chargerates.chargeEffectiveFromDate>=:chargeeffectivefromdate and "
						+ " chargerates.chargeEffectiveToDate<=:chargeeffectivetodate)) ";
			}
		}

		if (chargeSearchCriteria.getFromDate() != null && chargeSearchCriteria.getToDate() == null) {
			if (criteriaHql.equals("")) {
				criteriaHql = criteriaHql + " where ((chargerates.chargeEffectiveFromDate>=:chargeeffectivefromdate or "
						+ " chargerates.chargeEffectiveToDate>=:chargeeffectivefromdate)) ";
			} else {
				criteriaHql = criteriaHql + " and ((chargerates.chargeEffectiveFromDate>=:chargeeffectivefromdate or "
						+ " chargerates.chargeEffectiveToDate>=:chargeeffectivefromdate)) ";
			}
		}

		if (chargeSearchCriteria.getFromDate() == null && chargeSearchCriteria.getToDate() != null) {
			if (criteriaHql.equals("")) {
				criteriaHql = criteriaHql + " where ((chargerates.chargeEffectiveFromDate<=:chargeeffectivetodate or "
						+ " chargerates.chargeEffectiveToDate<=:chargeeffectivetodate)) ";
			} else {
				criteriaHql = criteriaHql + " and ((chargerates.chargeEffectiveFromDate<=:chargeeffectivetodate or "
						+ " chargerates.chargeEffectiveToDate<=:chargeeffectivetodate)) ";
			}
		}

		if (chargeSearchCriteria.getFromDate() == null && chargeSearchCriteria.getToDate() == null) {
			hasDateSearch = "n";
			if (chargeSearchCriteria.isOnlyChargesWithoutRate()) {
				if (criteriaHql.equals("")) {
					criteriaHql = criteriaHql + " where chargerates.chargeCode is null";
				} else {
					criteriaHql = criteriaHql + " and chargerates.chargeCode is null";
				}
			}
		}

		if (!AppSysParamsUtil.isCookieBasedSurchargesApplyForIBE()) {
			if (criteriaHql.equals("")) {
				criteriaHql = criteriaHql + " where charge.chargeCategoryCode <> 'COOKIE_BASED' ";
			} else {
				criteriaHql = criteriaHql + " and charge.chargeCategoryCode <> 'COOKIE_BASED' ";
			}
		}
		hql = hql + criteriaHql;
		hqlCount = hqlCount + criteriaHql;
		/** check for order by list **/

		if (orderbyfields != null) {
			String orderbyHql = "";
			Iterator<String> iter = orderbyfields.iterator();
			while (iter.hasNext()) {
				if (orderbyHql.equals("")) {
					orderbyHql = " order by " + iter.next().toString();
				} else {
					orderbyHql = orderbyHql + ", " + iter.next().toString();
				}
			}
			hql = hql + " " + orderbyHql + " asc";
		}

		return new String[] { hql, hqlCount, hasDateSearch };
	}

	/**
	 * Method will return (1) the Main HQL with the Where statement (2) the HQL for extracting the total count (3) a
	 * boolean "y/n" to see if date search has been selected
	 * 
	 * String[]
	 * 
	 * @param criteria
	 * @param orderbyfields
	 * @return
	 */
	private String[] getHQLForONDCharges(ONDChargeSearchCriteria criteria, List<String> orderbyfields) {
		String hql = "SELECT DISTINCT ond from OriginDestination ond join ond.charges charge left join charge.chargeRates chargerates";
		String hqlCount = "SELECT DISTINCT ond.ONDCode from OriginDestination ond join ond.charges charge left join charge.chargeRates chargerates";

		// String hql = "SELECT DISTINCT ond from OriginDestination ond join ond.charges charge";
		// String hqlCount="SELECT DISTINCT ond.ONDCode from OriginDestination ond join ond.charges charge";

		String criteriaHql = "";
		String hasDateSearch = "y";
		String ondCode = null;

		if (criteria.getOriginAirportCode() != null && criteria.getDestinationAirportCode() != null) {

			ondCode = PricingUtils.constructViaPointSearch(criteria.getOriginAirportCode(), criteria.getDestinationAirportCode(),
					criteria.getViaAirportCodes(), criteria.isExactAirportsCombinationOnly());

			if (criteriaHql.equals("")) {
				criteriaHql = criteriaHql + " where ond.ONDCode like :ondcode";
			} else {
				criteriaHql = criteriaHql + " and ond.ONDCode like :ondcode";
			}

		}

		/**
		 * charge category value "POS" is selected or chargeCategoryValue is selected all the POS OND charges will be
		 * shown *
		 */

		if (criteria.getChargeCategory() != null) {

			if (criteriaHql.equals("")) {
				criteriaHql = criteriaHql + " where charge.chargeCategory like :chargecategory OR charge.chargecategory is null ";
			} else {
				criteriaHql = criteriaHql + " and charge.chargeCategory like :chargecategory OR charge.chargecategory is null ";
			}

		}

		// filtering pos
		if (criteria.getPosStation() != null) {
			if (criteriaHql.equals("")) {
				criteriaHql = criteriaHql + " where charge.chargeCategoryValue like :pos OR charge.chargeCategoryValue is null ";
			} else {
				criteriaHql = criteriaHql + " and charge.chargeCategoryValue like :pos OR charge.chargeCategoryValue is null ";
			}
		} else {
			if (criteriaHql.equals("")) {
				criteriaHql = criteriaHql + " where charge.chargeCategoryValue is null ";
			} else {
				criteriaHql = criteriaHql + " and  charge.chargeCategoryValue is null ";
			}
		}

		/** if either one date is selected for searching it will be compared with the other date's current time **/
		if (criteria.getEffectiveFromDate() != null && criteria.getEffectiveToDate() != null) {
			if (criteriaHql.equals("")) {
				criteriaHql = criteriaHql
						+ " where ((chargerates.chargeEffectiveFromDate>=:chargeeffectivefromdate and "
						+ " chargerates.chargeEffectiveToDate>=:chargeeffectivetodate and chargerates.chargeEffectiveFromDate<=:chargeeffectivetodate) or "
						+ " (chargerates.chargeEffectiveFromDate<=:chargeeffectivefromdate and "
						+ " chargerates.chargeEffectiveToDate<=:chargeeffectivetodate and chargerates.chargeEffectiveToDate>=:chargeeffectivefromdate) or"
						+ " (chargerates.chargeEffectiveFromDate<=:chargeeffectivefromdate and "
						+ " chargerates.chargeEffectiveToDate>=:chargeeffectivetodate) or"
						+ " (chargerates.chargeEffectiveFromDate>=:chargeeffectivefromdate and "
						+ " chargerates.chargeEffectiveToDate<=:chargeeffectivetodate)) ";
			} else {
				criteriaHql = criteriaHql
						+ " and ((chargerates.chargeEffectiveFromDate>=:chargeeffectivefromdate and "
						+ " chargerates.chargeEffectiveToDate>=:chargeeffectivetodate and chargerates.chargeEffectiveFromDate<=:chargeeffectivetodate) or "
						+ " (chargerates.chargeEffectiveFromDate<=:chargeeffectivefromdate and "
						+ " chargerates.chargeEffectiveToDate<=:chargeeffectivetodate and chargerates.chargeEffectiveToDate>=:chargeeffectivefromdate) or"
						+ " (chargerates.chargeEffectiveFromDate<=:chargeeffectivefromdate and "
						+ " chargerates.chargeEffectiveToDate>=:chargeeffectivetodate) or"
						+ " (chargerates.chargeEffectiveFromDate>=:chargeeffectivefromdate and "
						+ " chargerates.chargeEffectiveToDate<=:chargeeffectivetodate)) ";
			}
		}

		if (criteria.getEffectiveFromDate() != null && criteria.getEffectiveToDate() == null) {
			if (criteriaHql.equals("")) {
				criteriaHql = criteriaHql + " where ((chargerates.chargeEffectiveFromDate>=:chargeeffectivefromdate or "
						+ " chargerates.chargeEffectiveToDate>=:chargeeffectivefromdate)) ";
			} else {
				criteriaHql = criteriaHql + " and ((chargerates.chargeEffectiveFromDate>=:chargeeffectivefromdate or "
						+ " chargerates.chargeEffectiveToDate>=:chargeeffectivefromdate)) ";
			}
		}

		if (criteria.getEffectiveFromDate() == null && criteria.getEffectiveToDate() != null) {
			if (criteriaHql.equals("")) {
				criteriaHql = criteriaHql + " where ((chargerates.chargeEffectiveFromDate<=:chargeeffectivetodate or "
						+ " chargerates.chargeEffectiveToDate<=:chargeeffectivetodate)) ";
			} else {
				criteriaHql = criteriaHql + " and ((chargerates.chargeEffectiveFromDate<=:chargeeffectivetodate or "
						+ " chargerates.chargeEffectiveToDate<=:chargeeffectivetodate)) ";
			}
		}

		if (criteria.getEffectiveFromDate() == null && criteria.getEffectiveToDate() == null) {
			hasDateSearch = "n";
		}

		/** Merging the "search hql" part with the "main hql" **/
		hql = hql + criteriaHql;
		hqlCount = hqlCount + criteriaHql;

		/** check for order by list and merge it to the hql **/
		if (orderbyfields != null) {
			String orderbyHql = "";
			Iterator<String> iter = orderbyfields.iterator();
			while (iter.hasNext()) {
				if (orderbyHql.equals("")) {
					orderbyHql = " order by " + iter.next().toString();
				} else {
					orderbyHql = orderbyHql + ", " + iter.next().toString();
				}
			}
			hql = hql + " " + orderbyHql + " asc";
		}
		System.out.println("HQL:" + hql);
		System.out.println("HQLCOUNT:" + hqlCount);
		return new String[] { hql, hqlCount, hasDateSearch, ondCode };
	}

	/**
	 * Method to get Global Charges
	 */
	@Override
	public Collection<Charge> getGlobalCharges() {
		String globalString = (String) AirpricingUtils.getAirpricingConfig().getChargeCategoriesMap().get("GLOBAL");

		if (globalString == null) {
			globalString = "%";
		}

		String hql = "select charge from Charge charge where charge.chargeCategoryCode like '" + globalString + "'";
		return find(hql, Charge.class);

	}

	@Override
	@SuppressWarnings("rawtypes")
	public Map<Integer, ChargeTO> getChageRateById(Collection<Integer> chargeRateIds) {
		if (chargeRateIds == null || chargeRateIds.size() == 0) {
			return null;
		}

		Map<Integer, ChargeTO> map = new TreeMap<Integer, ChargeTO>();
		String inStr = PricingUtils.constructINString(chargeRateIds);

		String hql = "FROM ChargeRate chargerate  where " + "chargerate.chargeRateCode in (" + inStr + ")";

		List l = find(hql, ChargeRate.class);
		Iterator iter = l.iterator();

		while (iter.hasNext()) {
			Object obj = iter.next();
			ChargeRate chargeRate = (ChargeRate) obj;
			int chargeRateId = chargeRate.getChargeRateCode();

			if (chargeRateIds.contains(new Integer(chargeRateId))) {
				ChargeTO chargeTO = new ChargeTO();
				chargeTO.setChargeRateId(chargeRateId);
				chargeTO.setChargeCode(chargeRate.getChargeCode());

				map.put(chargeRateId, chargeTO);
			}
		}

		return map;
	}

	/**
	 * Impl method that excepts a list of chargeRate Ids and will return a list of ChargeTOs
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<ChargeTO> getRefundableCharges(Collection<Integer> chargeRateIds) {

		if (chargeRateIds == null || chargeRateIds.size() == 0) {
			return null;
		}

		List<ChargeTO> chargeTos = new ArrayList<ChargeTO>();

		String inStr = PricingUtils.constructINString(chargeRateIds);
		String hql = "select chargerate, charge from ChargeRate chargerate, Charge charge where "
				+ " chargerate.chargeCode=charge.chargeCode and chargerate.chargeRateCode in ( :chargeRateIds )";

		Query query = getSession().createQuery(hql);
		query.setParameterList("chargeRateIds", chargeRateIds, StandardBasicTypes.INTEGER);
		List<Object[]> l = query.list();

		Iterator<Object[]> iter = l.iterator();

		while (iter.hasNext()) {
			Object[] objects = iter.next();
			ChargeRate chargeRate = (ChargeRate) objects[0];
			Charge charge = (Charge) objects[1];
			int chargeRateId = chargeRate.getChargeRateCode();

			if (chargeRateIds.contains(new Integer(chargeRateId))) {
				ChargeTO chargeTO = new ChargeTO();
				chargeTO.setChargeRateId(chargeRateId);
				chargeTO.setChargeCode(charge.getChargeCode());
				chargeTO.setChargeDescription(charge.getChargeDescription());
				chargeTO.setAirportTaxCategory(charge.getAirportTaxCategory());
				chargeTO.setBundleFareFee("Y".equals(charge.getBundledFareCharge()));

				if (charge.getRefundableChargeFlag() == Charge.REFUNDABLE_CHARGE) {
					chargeTO.setRefundable(true);
				} else {
					chargeTO.setRefundable(false);
				}

				if ("Y".equals(charge.getRefundableOnlyforModifications())) {
					chargeTO.setRefundableOnlyForMOD(true);
				} else {
					chargeTO.setRefundableOnlyForMOD(false);
				}

				if ("Y".equals(charge.getRefundableWhenExchange())) {
					chargeTO.setRefundableWhenExchange(true);
				} else {
					chargeTO.setRefundableWhenExchange(false);
				}

				chargeTos.add(chargeTO);
			}
		}

		return chargeTos;
	}

	@Override
	public void deleteCharge(Charge charge) {
		delete(charge);

	}

	@Override
	public void removeCharge(String ChargeCode, long version) {
		Charge charge = (Charge) get(Charge.class, ChargeCode);
		if (charge == null) {
			throw new CommonsDataAccessException("module.hibernate.objectnotfound");
		} else {
			if (version < charge.getVersion()) {
				throw new CommonsDataAccessException("module.update.delete.failed");

			} else {
				delete(charge);
			}
		}

	}

	@Override
	public Collection<Charge> getActiveCharges(Collection<String> colChargeGrpCodes) {
		if (colChargeGrpCodes == null || colChargeGrpCodes.size() == 0) {
			String hql = "select charge from Charge charge left join charge.chargeRates chargerates where charge.status = '"
					+ Charge.STATUS_ACTIVE + "' and chargerates.status = '" + ChargeRate.Status_Active + "' ";
			return find(hql, Charge.class);
		} else {
			String hql = "select charge from Charge charge left join charge.chargeRates chargerates where charge.status = '"
					+ Charge.STATUS_ACTIVE + "' and chargerates.status = '" + ChargeRate.Status_Active
					+ "' and charge.chargeGroupCode in (" + Util.buildStringInClauseContent(colChargeGrpCodes) + ") ";
			return find(hql, Charge.class);
		}
	}

	@Override
	public Collection<SeatCharge> getSeatCharges(Collection<Integer> colAMSChaegeIDs) {
		if (colAMSChaegeIDs != null && colAMSChaegeIDs.size() > 0) {
			String hql = "select seatcharge from SeatCharge seatcharge where seatcharge.chargeId in ("
					+ Util.buildIntegerInClauseContent(colAMSChaegeIDs) + ") ";
			return find(hql, SeatCharge.class);
		} else {
			throw new CommonsDataAccessException("module.hibernate.objectnotfound");
		}
	}

	@Override
	public ChargeTemplate getChargeTemplate(int templateId) {
		return (ChargeTemplate) get(ChargeTemplate.class, templateId);
	}

	@Override
	public void saveTemplate(ChargeTemplate chargeTemplate) {
		hibernateSaveOrUpdate(chargeTemplate);
	}

	@Override
	public ChargeTemplate getTemplateId(String templateCode) {
		String hql = " SELECT ct FROM ChargeTemplate ct WHERE ct.templateCode =:templateCode";
		Query query = getSession().createQuery(hql);
		return (ChargeTemplate) query.setString("templateCode", templateCode).uniqueResult();
	}

	@Override
	public Page<ChargeTemplate> getSeatCharges(ChargeTemplateSearchCriteria criteria, int startRec, int noRecs) {
		String s[] = constructHql(criteria);
		String hql = s[0];
		String hqlCount = s[1];

		Query qResults = getSession().createQuery(hql).setFirstResult(startRec).setMaxResults(noRecs);
		Query qCount = getSession().createQuery(hqlCount);

		int count = ((Long) qCount.uniqueResult()).intValue();
		@SuppressWarnings("unchecked")
		Collection<ChargeTemplate> colList = qResults.list();
		return new Page<ChargeTemplate>(count, startRec, startRec + noRecs - 1, colList);
	}

	@Override
	@SuppressWarnings("unchecked")
	public Page<ChargeTemplate> getChargeTemplate(ChargeTemplateSearchCriteria criteria, int startRec, int noRecs) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		String sqlCount = "select count(*) as cnt from (select act.amc_template_id FROM SM_T_AM_CHARGE_TEMPLATE act ";
		String sql = "select * from (select act.amc_template_id, act.amc_template_code, act.model_number, act.description, act.default_charge_amount,";
		sql = sql + " act.status,act.version,act.user_notes,act.local_currency_code,ROWNUM r FROM SM_T_AM_CHARGE_TEMPLATE act ";
		String sqlWhere = " where ";
		if (criteria.getModelNo() != null) {
			if (!sqlWhere.equals(" where ")) {
				sqlWhere += " and ";
			}
			sqlWhere += " model_number = '" + criteria.getModelNo() + "' ";
		}

		if (criteria.getTemplateCode() != null) {
			if (!sqlWhere.equals(" where ")) {
				sqlWhere += " and ";
			}
			sqlWhere += " amc_template_code ='" + criteria.getTemplateCode() + "' ";
		}

		if (!sqlWhere.equals(" where ")) {
			sql = sql + sqlWhere;
			sqlCount = sqlCount + sqlWhere;
		}
		sql += ") where r >= " + startRec + " and r < " + startRec + noRecs;
		sqlCount += ")";

		Collection<ChargeTemplate> colList = (Collection<ChargeTemplate>) jdbcTemplate.query(sql, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<ChargeTemplate> chargeList = new ArrayList<ChargeTemplate>();
				while (rs.next()) {
					ChargeTemplate chargeTemplate = new ChargeTemplate();
					chargeTemplate.setDescription(rs.getString("DESCRIPTION"));
					chargeTemplate.setTemplateId(rs.getInt("AMC_TEMPLATE_ID"));
					chargeTemplate.setTemplateCode(rs.getString("AMC_TEMPLATE_CODE"));
					chargeTemplate.setModelNo(rs.getString("MODEL_NUMBER"));
					chargeTemplate.setDefaultChargeAmount(new BigDecimal(rs.getInt("DEFAULT_CHARGE_AMOUNT")));
					chargeTemplate.setStatus(rs.getString("STATUS"));
					chargeTemplate.setUserNote(rs.getString("USER_NOTES"));
					chargeTemplate.setVersion(rs.getInt("VERSION"));
					chargeTemplate.setChargeLocalCurrencyCode(rs.getString("LOCAL_CURRENCY_CODE"));
					chargeList.add(chargeTemplate);
				}
				return chargeList;
			}
		});

		Integer count = (Integer) jdbcTemplate.query(sqlCount, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					return new Integer(rs.getInt("cnt"));
				}
				return null;
			}
		});
		return new Page<ChargeTemplate>(count, startRec, startRec + noRecs - 1, colList);
	}

	@Override
	public Collection<ChargeTemplate> getChargeTemplates(ChargeTemplateSearchCriteria criteria) {
		String s[] = constructHql(criteria);
		return find(s[0], ChargeTemplate.class);
	}

	private String[] constructHql(ChargeTemplateSearchCriteria criteria) {
		String hql = " select chargeTemplate from ChargeTemplate chargeTemplate ";
		String hqlCount = " select count(*) from ChargeTemplate chargeTemplate ";
		String hqlWhere = " WHERE 1=1 ";

		if (criteria.getModelNo() != null) {
			hqlWhere += "AND chargeTemplate.modelNo = '" + criteria.getModelNo() + "' ";
		}

		if (criteria.getTemplateCode() != null) {
			hqlWhere += "AND chargeTemplate.templateCode ='" + criteria.getTemplateCode() + "' ";
		}

		if (criteria.getTemplateId() != null) {
			hqlWhere += "AND chargeTemplate.templateId ='" + criteria.getTemplateId() + "' ";
		}
		String completeHql = hql + hqlWhere;
		String completeHqlCount = hqlCount + hqlWhere;

		return new String[] { completeHql, completeHqlCount };
	}

	@Override
	public void deleteTemplate(ChargeTemplate chargeTemplate) {
		delete(chargeTemplate);
	}

	/**
	 * method to remove a ChargePos for a given chargeCode
	 */
	@Override
	public void removeChargePos(String chargeCode) {
		String hql = "select chargePos from ChargePos chargePos where " + " chargePos.charge.chargeCode = '" + chargeCode + "' ";
		Collection<ChargePos> chargePoss = find(hql, ChargePos.class);

		if (chargePoss.size() > 0) {
			deleteAll(chargePoss);
		}
	}

	/**
	 * method to remove a ChargePos for a given chargeCode
	 */
	@Override
	public void removeChargeAgents(String chargeCode) {
		String hql = "select chargeAgent from ChargeAgent chargeAgent where " + " chargeAgent.charge.chargeCode = '" + chargeCode
				+ "' ";
		Collection<ChargeAgent> chargeAgentList = find(hql, ChargeAgent.class);

		if (chargeAgentList.size() > 0) {
			deleteAll(chargeAgentList);
		}
	}

	@Override
	public void deleteMealTemplate(MealTemplate mealTemplate) {
		delete(mealTemplate);
	}

	@Override
	public Page<MealTemplate> getMealTemplates(MealTemplateSearchCriteria criteria, int startRec, int noRecs) {
		String s[] = constructMealHql(criteria);
		String hql = s[0];
		String hqlCount = s[1];

		Query qResults = getSession().createQuery(hql).setFirstResult(startRec).setMaxResults(noRecs);
		Query qCount = getSession().createQuery(hqlCount);

		int count = ((Long) qCount.uniqueResult()).intValue();
		@SuppressWarnings("unchecked")
		Collection<MealTemplate> colList = qResults.list();
		return new Page<MealTemplate>(count, startRec, startRec + noRecs - 1, colList);
	}

	@Override
	public void saveMealTemplate(MealTemplate mealTemplate) {
		hibernateSaveOrUpdate(mealTemplate);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private String[] constructMealHql(MealTemplateSearchCriteria criteria) {
		String completeHql = null;
		String completeHqlCount = null;
		Collection colCriteria = new ArrayList();

		String hql = " select mealTemplate from MealTemplate mealTemplate ";
		String hqlCount = " select count(*) from MealTemplate mealTemplate ";
		String hqlWhere = " ";

		if (criteria.getTemplateCode() != null) {
			String templateCode = "mealTemplate.templateId ='" + criteria.getTemplateCode() + "' ";
			colCriteria.add(templateCode);
		}

		if (criteria.getStatus() != null) {
			String stat = criteria.getStatus();
			String status = null;
			if (stat.equals("ALL")) {
				status = "mealTemplate.status like '%' ";
			} else {
				status = "mealTemplate.status ='" + criteria.getStatus() + "' ";
			}
			colCriteria.add(status);
		}

		if (colCriteria.size() > 0) {
			hqlWhere = " WHERE ";
			Iterator Itr = colCriteria.iterator();
			while (Itr.hasNext()) {
				hqlWhere += (String) Itr.next();
				if (Itr.hasNext()) {
					hqlWhere += " AND ";
				}
			}
		}
		hqlWhere += " order by mealTemplate.templateId ";
		completeHql = hql + hqlWhere;
		completeHqlCount = hqlCount + hqlWhere;

		return new String[] { completeHql, completeHqlCount };
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private String[] constructBaggageHql(BaggageTemplateSearchCriteria criteria) {
		String completeHql = null;
		String completeHqlCount = null;
		Collection colCriteria = new ArrayList();

		String hql = " select baggageTemplate from BaggageTemplate baggageTemplate ";
		String hqlCount = " select count(*) from BaggageTemplate baggageTemplate ";
		String hqlWhere = " ";

		if (criteria.getTemplateCode() != null) {
			String templateCode = "baggageTemplate.templateId ='" + criteria.getTemplateCode() + "' ";
			colCriteria.add(templateCode);
		}

		if (criteria.getStatus() != null) {
			String stat = criteria.getStatus();
			String status = null;
			if (stat.equals("ALL")) {
				status = "baggageTemplate.status like '%' ";
			} else {
				status = "baggageTemplate.status ='" + criteria.getStatus() + "' ";
			}
			colCriteria.add(status);
		}

		if (colCriteria.size() > 0) {
			hqlWhere = " WHERE ";
			Iterator Itr = colCriteria.iterator();
			while (Itr.hasNext()) {
				hqlWhere += (String) Itr.next();
				if (Itr.hasNext()) {
					hqlWhere += " AND ";
				}
			}
		}
		hqlWhere += " order by baggageTemplate.templateId ";
		completeHql = hql + hqlWhere;
		completeHqlCount = hqlCount + hqlWhere;

		return new String[] { completeHql, completeHqlCount };
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private String[] constructONDBaggageHql(BaggageTemplateSearchCriteria criteria) {
		String completeHql = null;
		String completeHqlCount = null;
		Collection colCriteria = new ArrayList();

		String hql = " select baggageTemplate from ONDBaggageTemplate baggageTemplate ";
		String hqlCount = " select count(*) from ONDBaggageTemplate baggageTemplate ";
		String hqlWhere = " ";

		if (criteria.getTemplateCode() != null) {
			String templateCode = "baggageTemplate.templateId ='" + criteria.getTemplateCode() + "' ";
			colCriteria.add(templateCode);
		}

		if (criteria.getStatus() != null) {
			String stat = criteria.getStatus();
			String status = null;
			if (stat.equals("ALL")) {
				status = "baggageTemplate.status like '%' ";
			} else {
				status = "baggageTemplate.status ='" + criteria.getStatus() + "' ";
			}
			colCriteria.add(status);
		}

		if (colCriteria.size() > 0) {
			hqlWhere = " WHERE ";
			Iterator Itr = colCriteria.iterator();
			while (Itr.hasNext()) {
				hqlWhere += (String) Itr.next();
				if (Itr.hasNext()) {
					hqlWhere += " AND ";
				}
			}
		}
		hqlWhere += " order by baggageTemplate.templateId ";
		completeHql = hql + hqlWhere;
		completeHqlCount = hqlCount + hqlWhere;

		return new String[] { completeHql, completeHqlCount };
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private String[] constructONDCodeHql(ONDCodeSearchCriteria criteria) {
		String completeHql = null;
		String completeHqlCount = null;
		Collection colCriteria = new ArrayList();

		String hql = "select baggageChargeTemplate  from ONDBaggageChargeTemplate baggageChargeTemplate inner join fetch baggageChargeTemplate.ondBaggageTemplate baggageTemplate ";

		String hqlCount = "select count(*) from ONDBaggageChargeTemplate baggageChargeTemplate";

		String hqlWhere = " ";
		if (criteria.getOndCode() != null) {
			String ondCode = "baggageChargeTemplate.ondCode ='" + criteria.getOndCode() + "' ";
			colCriteria.add(ondCode);
		}

		if (criteria.getTemplateCodeID() != null) {
			String templateCode = criteria.getTemplateCodeID();
			String status = null;
			if (templateCode.equals("ALL")) {
				templateCode = "baggageChargeTemplate.ondBaggageTemplate.templateId like '%' ";
			} else {
				templateCode = "baggageChargeTemplate.ondBaggageTemplate.templateId ='" + criteria.getTemplateCodeID() + "' ";
			}
			colCriteria.add(templateCode);
		}

		/*
		 * if (criteria.getStatus() != null) { String stat = criteria.getStatus(); String status = null; if
		 * (stat.equals("ALL")) { status = "baggageChargeTemplate.ondCodes.status like '%' "; } else { status =
		 * "baggageChargeTemplate.ondCodes.status ='" + criteria.getStatus() + "' "; } colCriteria.add(status); }
		 */

		if (colCriteria.size() > 0) {
			hqlWhere = " WHERE ";
			Iterator Itr = colCriteria.iterator();
			while (Itr.hasNext()) {
				hqlWhere += (String) Itr.next();
				if (Itr.hasNext()) {
					hqlWhere += " AND ";
				}
			}
		}
		hqlWhere += " order by baggageChargeTemplate.ondCode ";
		completeHql = hql + hqlWhere;
		completeHqlCount = hqlCount + hqlWhere;

		return new String[] { completeHql, completeHqlCount };
	}

	@Override
	@SuppressWarnings("unchecked")
	public Collection<MealTemplate> getMealTemplates(Integer mealId) {
		String hql = " select mealTemplate from MealTemplate mealTemplate, MealCharge mlChg "
				+ " where mealTemplate.templateId    = mlChg.templateId " + " and mlChg.mealId = " + mealId;
		Query qResults = getSession().createQuery(hql);
		return qResults.list();
	}

	@Override
	public MealTemplate getMealTemplate(int templateID) {
		return (MealTemplate) get(MealTemplate.class, templateID);
	}

	@Override
	public Collection<MealCharge> getMealCharges(Collection<Integer> colMealChargeIDs) throws CommonsDataAccessException {
		if (colMealChargeIDs != null && colMealChargeIDs.size() > 0) {
			String hql = "select mealcharge from MealCharge mealcharge where mealcharge.chargeId in ("
					+ Util.buildIntegerInClauseContent(colMealChargeIDs) + ") ";
			return find(hql, MealCharge.class);
		} else {
			throw new CommonsDataAccessException("module.hibernate.objectnotfound");
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public Collection<ChargeRate> getChargeRatesWithoutLocalCurrency(Date effectiveDate, String baseCurrency) {

		String hql = "SELECT chargeRate FROM ChargeRate as chargeRate "
				+ " WHERE :effDate BETWEEN chargeRate.chargeEffectiveFromDate AND " + " chargeRate.chargeEffectiveToDate AND "
				+ " chargeRate.currencyCode IS NOT NULL AND " + " chargeRate.currencyCode != '" + baseCurrency + "'";

		Query hQuery = getSession().createQuery(hql);

		hQuery.setTimestamp("effDate", effectiveDate);

		List<ChargeRate> resultsList = hQuery.list();

		return resultsList;

	}

	@Override
	@SuppressWarnings("unchecked")
	public Collection<BaggageTemplate> getBaggageTemplatesCollect(Integer baggageId) {
		String hql = " select baggageTemplate from BaggageTemplate baggageTemplate, BaggageCharge bgCharg "
				+ " where baggageTemplate.templateId = bgCharg.template.templateId " + " and bgCharg.baggageId = " + baggageId;
		Query qResults = getSession().createQuery(hql);
		return qResults.list();
	}

	@Override
	public BaggageTemplate getBaggageTemplate(int templateID) {
		return (BaggageTemplate) get(BaggageTemplate.class, templateID);
	}

	@Override
	public ONDBaggageTemplate getONDBaggageTemplate(int ondTemplateID) throws CommonsDataAccessException {
		return (ONDBaggageTemplate) get(ONDBaggageTemplate.class, ondTemplateID);
	}

	@Override
	public Page<BaggageTemplate> getBaggageTemplates(BaggageTemplateSearchCriteria criteria, int startRec, int noRecs) {
		String s[] = constructBaggageHql(criteria);
		String hql = s[0];
		String hqlCount = s[1];

		Query qResults = getSession().createQuery(hql).setFirstResult(startRec).setMaxResults(noRecs);
		Query qCount = getSession().createQuery(hqlCount);

		int count = ((Long) qCount.uniqueResult()).intValue();
		@SuppressWarnings("unchecked")
		Collection<BaggageTemplate> colList = qResults.list();
		return new Page<BaggageTemplate>(count, startRec, startRec + noRecs - 1, colList);
	}

	@Override
	public void saveBaggageTemplate(BaggageTemplate baggageTemplate) {
		hibernateSaveOrUpdate(baggageTemplate);
	}

	@Override
	public void deleteBaggageTemplate(BaggageTemplate baggageTemplate) {
		delete(baggageTemplate);
	}

	@Override
	@SuppressWarnings("rawtypes")
	public Page getONDBaggageTemplates(BaggageTemplateSearchCriteria criteria, int startRec, int noRecs) {
		String s[] = constructONDBaggageHql(criteria);
		String hql = s[0];
		String hqlCount = s[1];

		Query qResults = getSession().createQuery(hql).setFirstResult(startRec).setMaxResults(noRecs);
		Query qCount = getSession().createQuery(hqlCount);

		int count = ((Long) qCount.uniqueResult()).intValue();
		Collection colList = qResults.list();
		return new Page(count, startRec, startRec + noRecs - 1, colList);
	}

	@Override
	public void saveONDBaggageTemplate(ONDBaggageTemplate baggageTemplate) {
		hibernateSaveOrUpdate(baggageTemplate);
	}

	@Override
	public void deleteONDBaggageTemplate(ONDBaggageTemplate baggageTemplate) {
		delete(baggageTemplate);
	}

	@Override
	public boolean isActiveONDCodeExistForBaggageTemplate(int templateId, Date fromDate, Date toDate) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());

		String sql = "SELECT COUNT(1) AS cnt FROM bg_t_ond_bagg_charge_template obct, "
				+ "bg_t_ond_bagg_charge_template obct2, bg_t_ond_charge_template oct "
				+ "WHERE obct.ond_charge_template_id = ? AND obct2.ond_charge_template_id != ? "
				+ "AND obct.ond_baggage_ond_code = obct2.ond_baggage_ond_code "
				+ "AND oct.ond_charge_template_id = obct2.ond_charge_template_id AND oct.status = '" + Baggage.STATUS_ACTIVE
				+ "'";

		List<Object> listParams = new ArrayList<Object>();
		listParams.add(templateId);
		listParams.add(templateId);

		if (fromDate != null && toDate != null) {
			String sqlCondition = " and ( ? between oct.from_date and oct.end_date OR ? between oct.from_date and oct.end_date "
					+ "OR ( ? < oct.from_date AND ? > oct.end_date ))";
			sql += sqlCondition;

			listParams.add(fromDate);
			listParams.add(toDate);
			listParams.add(fromDate);
			listParams.add(toDate);
		}

		Integer count = (Integer) jdbcTemplate.query(sql, listParams.toArray(), new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					return new Integer(rs.getInt("cnt"));
				}
				return null;

			}
		});

		return (count > 0);
	}

	@Override
	@SuppressWarnings("rawtypes")
	public Page getONDCodes(ONDCodeSearchCriteria criteria, int startRec, int noRecs) {
		String s[] = constructONDCodeHql(criteria);
		String hql = s[0];
		String hqlCount = s[1];

		Query qResults = getSession().createQuery(hql).setFirstResult(startRec).setMaxResults(noRecs);
		Query qCount = getSession().createQuery(hqlCount);

		int count = ((Long) qCount.uniqueResult()).intValue();
		Collection colList = qResults.list();
		return new Page(count, startRec, startRec + noRecs - 1, colList);
	}

	public Page searchBaggageTemplates(ONDCodeSearchCriteria criteria, int startRec, int noRecs) {
		List<BaggageTemplateTo> templates = new ArrayList<BaggageTemplateTo>();

		Query countQry;
		Query dataQry;

		String countHql = " select count ( distinct t ) ";
		String dataHql = " select distinct t ";
		String commonHql = " from ONDBaggageTemplate t ";
		String orderHql = "order by t.templateId ASC ";

		commonHql += " left join t.flights f left join t.agents a left join t.bookingClasses b left join t.onds o left join t.salesChannels s ";
		commonHql += "      where t.status = 'ACT' and t.toDate >= trunc(sysdate) ";

		if (criteria.getAgentCode() != null) {
			commonHql += " and  a.agentCode = :agent  ";
		}
		if (criteria.getBookingClass() != null) {
			commonHql += " and b.bookingClass = :bookingClass  ";
		}
		if (criteria.getFlightNumber() != null) {
			commonHql += " and f.flightNumber = :flightNo ";
		}
		if (criteria.getOndCode() != null) {
			commonHql += " and o.ondCode = :ondCode ";
		}

		countQry = getSession().createQuery(countHql + commonHql);
		dataQry = getSession().createQuery(dataHql + commonHql + orderHql).setFirstResult(startRec).setMaxResults(noRecs);

		if (criteria.getAgentCode() != null) {
			countQry.setParameter("agent", criteria.getAgentCode());
			dataQry.setParameter("agent", criteria.getAgentCode());
		}
		if (criteria.getBookingClass() != null) {
			countQry.setParameter("bookingClass", criteria.getBookingClass());
			dataQry.setParameter("bookingClass", criteria.getBookingClass());
		}
		if (criteria.getFlightNumber() != null) {
			countQry.setParameter("flightNo", criteria.getFlightNumber());
			dataQry.setParameter("flightNo", criteria.getFlightNumber());
		}
		if (criteria.getOndCode() != null) {
			countQry.setParameter("ondCode", criteria.getOndCode());
			dataQry.setParameter("ondCode", criteria.getOndCode());
		}

		int count = ((Long) countQry.uniqueResult()).intValue();
		List<ONDBaggageTemplate> data = dataQry.list();

		for (ONDBaggageTemplate template : data) {
			templates.add(BaggageUtil.toBaggageTemplateTo(template));
		}

		return new Page(count, startRec, startRec + noRecs - 1, templates);
	}

	@Override
	public void saveONDCodes(ONDBaggageChargeTemplate baggageChargeTemplate) {
		hibernateSaveOrUpdate(baggageChargeTemplate);
	}

	@Override
	public void deleteONDCodes(ONDBaggageChargeTemplate baggageChargeTemplate) {
		delete(baggageChargeTemplate);
	}

	@Override
	public boolean isONDCodeExist(String ondCode, int templateId) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());

		List<Date> dates = getONDChargeTemplateDateRange(jdbcTemplate, templateId);

		String sql = "select count(1) as cnt from bg_t_ond_bagg_charge_template obct, bg_t_ond_charge_template oct "
				+ "where obct.ond_charge_template_id = oct.ond_charge_template_id and obct.ond_baggage_ond_code like ? and oct.status = '"
				+ Baggage.STATUS_ACTIVE + "' and oct.ond_charge_template_id != ? ";

		List<Object> listParams = new ArrayList<Object>();
		listParams.add(ondCode);
		listParams.add(templateId);

		if (dates.size() > 0) {
			String sqlCondition = " and ( ? between oct.from_date and oct.end_date OR ? between oct.from_date and oct.end_date "
					+ "OR ( ? < oct.from_date AND ? > oct.end_date ))";
			sql += sqlCondition;

			listParams.add(dates.get(0));
			listParams.add(dates.get(1));
			listParams.add(dates.get(0));
			listParams.add(dates.get(1));

		}
		Integer count = (Integer) jdbcTemplate.query(sql, listParams.toArray(), new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					return new Integer(rs.getInt("cnt"));
				}
				return null;

			}
		});

		if (count > 0) {
			return true;
		}
		return false;

	}

	private List<Date> getONDChargeTemplateDateRange(JdbcTemplate jdbcTemplate, int templateId) {

		String sql = "SELECT oct.from_date, oct.end_date FROM bg_t_ond_charge_template oct WHERE oct.ond_charge_template_id ='"
				+ templateId + "'";

		@SuppressWarnings("unchecked")
		List<Date> dates = (List<Date>) jdbcTemplate.query(sql, new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<Date> dateList = new ArrayList<Date>();
				if (rs != null) {
					while (rs.next()) {
						if (rs.getDate("from_Date") != null && rs.getDate("end_Date") != null) {
							dateList.add(rs.getDate("from_Date"));
							dateList.add(rs.getDate("end_Date"));
						}
					}
				}
				return dateList;
			}
		});

		return dates;
	}

	@Override
	public ONDBaggage saveONDBaggage(ONDBaggage ondBaggage) {
		hibernateSaveOrUpdate(ondBaggage);
		return ondBaggage;
	}

	@Override
	public Collection<BaggageCharge> getBaggageCharges(Collection<Integer> colBaggageChargeIDs) throws CommonsDataAccessException {
		if (colBaggageChargeIDs != null && colBaggageChargeIDs.size() > 0) {
			String hql = "select baggagecharge from BaggageCharge baggagecharge where baggagecharge.chargeId in ("
					+ Util.buildIntegerInClauseContent(colBaggageChargeIDs) + ") ";
			return find(hql, BaggageCharge.class);
		} else {
			throw new CommonsDataAccessException("module.hibernate.objectnotfound");
		}
	}

	/* SSR Charges */

	@Override
	public Page<SSRCharge> getSSRCharges(SSRChargesSearchCriteria criteria, int startRec, int noRecs) {
		String s[] = constructSSRChargesHql(criteria);
		String hql = s[0];
		String hqlCount = s[1];

		Query qResults = getSession().createQuery(hql).setFirstResult(startRec).setMaxResults(noRecs);
		Query qCount = getSession().createQuery(hqlCount);

		int count = ((Long) qCount.uniqueResult()).intValue();
		@SuppressWarnings("unchecked")
		Collection<SSRCharge> colList = qResults.list();
		return new Page<SSRCharge>(count, startRec, startRec + noRecs - 1, colList);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private String[] constructSSRChargesHql(SSRChargesSearchCriteria criteria) {
		String completeHql = null;
		String completeHqlCount = null;
		Collection colCriteria = new ArrayList();

		String hql = " select ssrCharge from SSRCharge ssrCharge where ";
		String hqlCount = " select count(*) from SSRCharge ssrCharge where ";

		String hqlWhere = " ";

		if (criteria.getChargeCode() != null) {
			String chargeCode = null;
			if (criteria.getChargeCode().equals("ALL")) {
				chargeCode = "ssrCharge.ssrChargeCode like '%' ";
			} else {
				chargeCode = "ssrCharge.ssrChargeCode like '%" + criteria.getChargeCode() + "%'";
			}
			colCriteria.add(chargeCode);
		}

		if (criteria.getSsrCode() != null && !criteria.getSsrCode().equals("ALL")) {
			String ssrCode = "ssrCharge.ssrId ='" + criteria.getSsrCode() + "' ";
			colCriteria.add(ssrCode);
		}

		if (criteria.getStatus() != null) {
			String stat = criteria.getStatus();
			String status = null;
			if (stat.equals("ALL")) {
				status = "ssrCharge.status like '%' ";
			} else {
				status = "ssrCharge.status ='" + criteria.getStatus() + "' ";
			}
			colCriteria.add(status);
		}

		Collection<String> visibleSSRIds = criteria.getAdminVisibleSSRId();
		if (visibleSSRIds != null && !visibleSSRIds.isEmpty() && visibleSSRIds.size() > 0) {
			StringBuilder filterNonAirportServ = new StringBuilder();
			filterNonAirportServ.append("ssrCharge.ssrId IN (");
			filterNonAirportServ.append(Util.buildStringInClauseContent(visibleSSRIds));
			filterNonAirportServ.append(") ");

			colCriteria.add(filterNonAirportServ.toString());
		}

		if (colCriteria.size() > 0) {
			Iterator Itr = colCriteria.iterator();
			while (Itr.hasNext()) {
				hqlWhere += (String) Itr.next();
				if (Itr.hasNext()) {
					hqlWhere += " AND ";
				}
			}
		}
		hqlWhere += " order by ssrCharge.chargeId ";
		completeHql = hql + hqlWhere;
		completeHqlCount = hqlCount + hqlWhere;

		return new String[] { completeHql, completeHqlCount };
	}

	@Override
	public SSRCharge getSSRCharges(int chargeId) {
		return (SSRCharge) get(SSRCharge.class, chargeId);
	}

	@Override
	@SuppressWarnings("rawtypes")
	public SSRCharge getSSRChargesBySSRId(int ssrId) throws CommonsDataAccessException {
		SSRCharge ssrCharge = null;

		String hql = "select ssrCharge from SSRCharge ssrCharge where ssrCharge.ssrId=" + ssrId;

		List<SSRCharge> chargeList = find(hql, SSRCharge.class);

		if (chargeList != null && chargeList.size() > 0) {
			ssrCharge = chargeList.get(0);
		}

		return ssrCharge;
	}

	@Override
	@SuppressWarnings("unchecked")
	public void saveSSRCharges(SSRCharge ssrCharge) throws ModuleException {
		if (ssrCharge.getVersion() != -1) {
			List<SSRChargesCOS> ssrList = find(
					"from SSRChargesCOS ssrChargeCOS where ssrChargeCOS.ssrCharge.chargeId = ?", ssrCharge.getChargeId(), SSRChargesCOS.class);
			for (SSRChargesCOS ssrChargeCOSList : ssrList) {
				int ssrChargeCOSId = ssrChargeCOSList.getSsrChargeCOSId();
				Object ssrChargeCOS = load(SSRChargesCOS.class, ssrChargeCOSId);
				delete(ssrChargeCOS);
			}
		}
		hibernateSaveOrUpdate(ssrCharge);
	}
	
	@Override
	public void saveSSRChargeswithoutDeletingCos(SSRCharge ssrCharge) throws ModuleException {
		hibernateSaveOrUpdate(ssrCharge);
	}

	@Override
	public void deleteSSRCharges(SSRCharge ssrCharge) {
		delete(ssrCharge);
	}

	@Override
	public boolean checkSSRChargeAlreadyDefined(int ssrId, String ssrChargeCode, int chargeId, ArrayList<String> ccList,
			ArrayList<String> lccList) throws CommonsDataAccessException {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		if (ssrChargeCode != null) {
			String sql = "select count(*) as cnt from t_ssr_charge sc, t_ssr_charge_cos scc where sc.ssr_id = " + ssrId
					+ " and sc.ssr_charge_id = scc.ssr_charge_id and (";
			if (ccList.size() > 0) {
				sql = sql + " scc.cabin_class_code IN (" + Util.buildStringInClauseContent(ccList)
						+ ") or scc.logical_cabin_class_code in ";
				sql = sql + " (select logical_cabin_class_code from t_logical_cabin_class where cabin_class_code IN ("
						+ Util.buildStringInClauseContent(ccList) + " )) or ";
			}
			if (lccList.size() > 0) {
				sql = sql + " scc.logical_cabin_class_code IN (" + Util.buildStringInClauseContent(lccList)
						+ ") or scc.cabin_class_code IN ( ";
				sql = sql + " (select cabin_class_code from t_logical_cabin_class where logical_cabin_class_code IN ("
						+ Util.buildStringInClauseContent(lccList) + "))) or ";
			}
			sql = sql + " sc.ssr_charge_code like '%" + ssrChargeCode + "%')";
			Integer count = (Integer) jdbcTemplate.query(sql, new ResultSetExtractor() {

				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					while (rs.next()) {
						return new Integer(rs.getInt("cnt"));
					}
					return null;

				}
			});

			if (count > 0) {
				return true;
			}
			return false;
		} else {
			String sql = "SELECT count(*) as cnt FROM t_ssr_charge sc, t_ssr_charge_cos scc WHERE sc.ssr_id = " + ssrId;

			if (lccList.size() > 0) {
				sql = sql + " and scc.logical_cabin_class_code IN (" + Util.buildStringInClauseContent(lccList) + ") ";
			}

			if (ccList.size() > 0) {
				sql = sql + " and scc.cabin_class_code IN (" + Util.buildStringInClauseContent(ccList)
						+ ") AND scc.ssr_charge_id = sc.ssr_charge_id ";
			}
			sql = sql + " and scc.ssr_charge_id = sc.ssr_charge_id and sc.ssr_charge_id not in (" + chargeId + ")";

			Integer count = (Integer) jdbcTemplate.query(sql, new ResultSetExtractor() {

				@Override
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					while (rs.next()) {
						return new Integer(rs.getInt("cnt"));
					}
					return null;

				}
			});

			if (count > 0) {
				return true;
			}
			return false;
		}
	}

	// JIRA-6929
	@Override
	public void saveReportChargeGroup(ReportChargeGroup reportChargeGroup) throws ModuleException {
		hibernateSaveOrUpdate(reportChargeGroup);
	}

	@Override
	public void deleteReportChargeGroup(ReportChargeGroup reportChargeGroup) throws ModuleException {
		delete(reportChargeGroup);
	}

	@Override
	@SuppressWarnings("rawtypes")
	public ReportChargeGroup getReportChargeGroupByRptChargeGroupCode(String reportChargeGroupCode)
			throws CommonsDataAccessException {
		ReportChargeGroup reportChargeGroup = null;
		String hql = "select reportChargeGroup from ReportChargeGroup reportChargeGroup where reportChargeGroup.reportChargeGroupCode ='"
				+ reportChargeGroupCode + "'";

		List<ReportChargeGroup> reportChargeGroupList = find(hql, ReportChargeGroup.class);

		if (reportChargeGroupList != null && reportChargeGroupList.size() > 0) {
			reportChargeGroup = reportChargeGroupList.get(0);
		}
		return reportChargeGroup;
	}

	public List<Charge> getApplicableChargesForUpdateOnExchangeRateChange(String currencyCode) {
		String hql = "select distinct charge from Charge charge left join charge.chargeRates chargerates where charge.status = '"
				+ Charge.STATUS_ACTIVE + "' and chargerates.status = '" + ChargeRate.Status_Active
				+ "' and chargerates.chargeEffectiveToDate >=:effectiveDate and chargerates.currencyCode=:currencyCode";
		Query q = getSession().createQuery(hql);
		q.setParameter("currencyCode", currencyCode);
		q.setTimestamp("effectiveDate", new Date());
		return q.list();
	}

	@Override
	public List<Charge> getExcludableCharges() throws CommonsDataAccessException {
		String hql = "SELECT charge FROM Charge charge WHERE charge.excludable =:isExcludable and charge.status =:status";
		Query q = getSession().createQuery(hql);
		q.setParameter("isExcludable", Charge.EXCLUABLE);
		q.setParameter("status", Charge.STATUS_ACTIVE);
		return q.list();
	}

	@Override
	public List<MCOServices> getMCOServices() throws ModuleException {
		String hql = "SELECT mcoService FROM MCOServices mcoService WHERE mcoService.status =:status";
		Query q = getSession().createQuery(hql);
		q.setParameter("status", MCOServices.STATUS_ACTIVE);
		return q.list();
	}

	@Override
	public void saveMCO(MCO mco) throws ModuleException {
		hibernateSaveOrUpdate(mco);
	}

	@Override
	public HashMap<String, Object> getMCORecord(String mcoNumber, String agentCode) throws CommonsDataAccessException {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		StringBuilder sqlBuilder = new StringBuilder();

		sqlBuilder.append(" SELECT m.created_date AS issued_date,");
		sqlBuilder.append(" pp.pnr AS pnr,");
		sqlBuilder.append(" m.mco_number as mco_number,");
		sqlBuilder.append(" ag.agent_name AS agent_name,");
		sqlBuilder.append(" pp.title as title,");
		sqlBuilder.append(" pp.first_name as first_name,");
		sqlBuilder.append(" pp.last_name as last_name,");
		sqlBuilder.append(" ms.service_name as service_name,");
		sqlBuilder.append(" m.amount_in_local as amount,");
		sqlBuilder.append(" m.currency_code as currency_code, ");
		sqlBuilder.append(" m.flight_no as flight_no,");
		sqlBuilder.append(" m.flight_date as flight_date,");
		sqlBuilder.append(" m.email as email,");
		sqlBuilder.append(" m.remarks as remarks,");
		sqlBuilder.append(" m.status as status,  ");
		sqlBuilder.append(" m.mco_service_id as service_id  ");
		sqlBuilder.append(" FROM T_MCO m,");
		sqlBuilder.append(" T_MCO_SERVICE ms,");
		sqlBuilder.append(" T_PNR_PASSENGER pp,");
		sqlBuilder.append(" T_AGENT ag,");
		sqlBuilder.append(" T_USER us");
		sqlBuilder.append(" WHERE m.mco_number       = '" + mcoNumber + "'");
		sqlBuilder.append(" AND m.mco_service_id = ms.mco_service_id ");
		sqlBuilder.append(" AND m.pnr_pax_id     = pp.pnr_pax_id ");
		sqlBuilder.append(" AND m.created_by = us.user_id ");
		sqlBuilder.append(" AND ag.agent_code = us.agent_code ");
		if (agentCode != null) {
			sqlBuilder.append(" AND ag.agent_code = '" + agentCode + "' ");
		}

		@SuppressWarnings("unchecked")
		HashMap<String, Object> paramMap = (HashMap<String, Object>) jdbcTemplate.query(sqlBuilder.toString(),
				new ResultSetExtractor() {
					@Override
					public Object extractData(ResultSet mcoResultSet) throws SQLException, DataAccessException {
						HashMap<String, Object> dataMap = new HashMap<>();
						if (mcoResultSet != null && mcoResultSet.next()) {
							dataMap.put("issuedDate", mcoResultSet.getDate("issued_date"));
							dataMap.put("pnr", mcoResultSet.getString("pnr"));
							dataMap.put("mcoNumber", mcoResultSet.getString("mco_number"));
							dataMap.put("paidAgent", mcoResultSet.getString("agent_name"));
							dataMap.put("title", mcoResultSet.getString("title"));
							dataMap.put("firstName", mcoResultSet.getString("first_name"));
							dataMap.put("lastName", mcoResultSet.getString("last_name"));
							dataMap.put("service", mcoResultSet.getString("service_name"));
							dataMap.put("amount", mcoResultSet.getBigDecimal("amount"));
							dataMap.put("currencyCode", mcoResultSet.getString("currency_code"));
							dataMap.put("fltNum", mcoResultSet.getString("flight_no"));
							dataMap.put("fltDeparture", mcoResultSet.getDate("flight_date"));
							dataMap.put("remarks", StringUtils.defaultString(mcoResultSet.getString("remarks")));
							dataMap.put("email", StringUtils.defaultString(mcoResultSet.getString("email")));
							dataMap.put("status", mcoResultSet.getString("status"));
							dataMap.put("serviceId", mcoResultSet.getString("service_id"));
						}
						return dataMap;
					}
				});

		return paramMap;
	}

	public List<HashMap<String, Object>> getMCORecords(String mcoNumber, Integer pnrPaxID) throws CommonsDataAccessException {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());
		StringBuilder sqlBuilder = new StringBuilder();

		sqlBuilder.append(" SELECT m.created_date AS issued_date,");
		sqlBuilder.append(" m.mco_number as mco_number,");
		sqlBuilder.append(" ms.service_name as service_name,");
		sqlBuilder.append(" m.amount_in_local as amount,");
		sqlBuilder.append(" m.currency_code as currency_code, ");
		sqlBuilder.append(" m.flight_no as flight_no,");
		sqlBuilder.append(" m.flight_date as flight_date,");
		sqlBuilder.append(" m.status as status,");
		sqlBuilder.append(" m.remarks as remarks  ");
		sqlBuilder.append(" FROM T_MCO m,");
		sqlBuilder.append(" T_MCO_SERVICE ms");
		sqlBuilder.append(" WHERE m.mco_service_id = ms.mco_service_id ");

		if (mcoNumber != null && !mcoNumber.isEmpty()) {
			sqlBuilder.append(" AND m.mco_number       = '" + mcoNumber + "'");
		}

		if (pnrPaxID != null) {
			sqlBuilder.append(" AND m.pnr_pax_id     = " + pnrPaxID);
		}

		@SuppressWarnings("unchecked")
		List<HashMap<String, Object>> mcoDataList = (List<HashMap<String, Object>>) jdbcTemplate.query(sqlBuilder.toString(),
				new ResultSetExtractor() {
					@Override
					public Object extractData(ResultSet mcoResultSet) throws SQLException, DataAccessException {
						List<HashMap<String, Object>> mcoDataList = new ArrayList<>();
						if (mcoResultSet != null) {
							while (mcoResultSet.next()) {
								HashMap<String, Object> dataMap = new HashMap<>();
								dataMap.put("issuedDate", mcoResultSet.getDate("issued_date"));
								dataMap.put("mcoNumber", mcoResultSet.getString("mco_number"));
								dataMap.put("service", mcoResultSet.getString("service_name"));
								dataMap.put("amount", mcoResultSet.getString("amount"));
								dataMap.put("currencyCode", mcoResultSet.getString("currency_code"));
								dataMap.put("flightNumber", mcoResultSet.getString("flight_no"));
								dataMap.put("remarks", mcoResultSet.getString("remarks"));
								if (MCO.VOID.equals(mcoResultSet.getString("status"))) {
									dataMap.put("status", "VOID");
								} else if (MCO.FLOWN.equals(mcoResultSet.getString("status"))) {
									dataMap.put("status", "FLOWN");
								}

								mcoDataList.add(dataMap);
							}
						}
						return mcoDataList;
					}
				});

		return mcoDataList;
	}

	public String getNextMCONumberSQID() throws ModuleException {

		log.debug("Inside getNextMCONumberSQID");
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		String sql = " SELECT S_MCO_NUMBER.NEXTVAL MCO_NUMBER FROM DUAL ";

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql);
		log.debug("############################################");

		String mcoNo = (String) jt.query(sql, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				String mcoNo = null;

				if (rs != null) {
					if (rs.next()) {
						mcoNo = BeanUtils.nullHandler(rs.getString("MCO_NUMBER"));
					}
				}

				return mcoNo;
			}
		});

		log.debug("Exit getNextMCONumberSQID");
		return mcoNo;

	}

	public MCO getMCO(String mcoNumber) throws ModuleException {
		List<MCO> mcoList = null;
		MCO mco = null;
		mcoList = find("select mco from MCO as mco where mco.mcoNumber = ? and mcoNumber.status = ? ", new Object[] { mcoNumber,
				MCO.FLOWN }, MCO.class);

		if (mcoList.size() > 0) {
			mco = mcoList.get(0);
		}
		return mco;
	}

	@SuppressWarnings("unchecked")
	public boolean hasMCORecords(Integer pnrPaxID) throws CommonsDataAccessException {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDatasource());

		boolean hasMCO = false;

		if (pnrPaxID != null) {
			StringBuilder sqlBuilder = new StringBuilder();

			sqlBuilder.append(" SELECT COUNT(*) AS mco_count ");
			sqlBuilder.append(" FROM T_MCO m ");
			sqlBuilder.append(" WHERE m.pnr_pax_id     = " + pnrPaxID);

			hasMCO = (boolean) jdbcTemplate.query(sqlBuilder.toString(), new ResultSetExtractor() {
				@Override
				public Object extractData(ResultSet mcoResultSet) throws SQLException, DataAccessException {
					boolean hasMCO = false;
					if (mcoResultSet != null && mcoResultSet.next()) {
						hasMCO = mcoResultSet.getInt("mco_count") > 0;
					}
					return hasMCO;
				}
			});
		}
		return hasMCO;

	}
	
	@Override
	public Map<String, Boolean> getChargeRefundabilityMap(List<String> chargeCodes) throws ModuleException {

		StringBuilder sql = new StringBuilder();

		sql.append("SELECT CHARGE_CODE , REFUNDABLE_CHARGE_FLAG ");
		sql.append(" FROM T_CHARGE WHERE");
		sql.append(" CHARGE_CODE IN  (" + Util.buildStringInClauseContent(chargeCodes) + ")");

		JdbcTemplate template = new JdbcTemplate(getDatasource());

		Map<String, Boolean> chargeRefundabilityMap = (Map<String, Boolean>) template.query(sql.toString(),
				new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

						Map<String, Boolean> chargeRefundabilityMap = new HashMap<String, Boolean>();

						Collection<EticketInfoTO> eTicketList = new ArrayList<EticketInfoTO>();
						while (rs.next()) {

							Boolean isChageRefundable = null;
							String chargeCode = rs.getString("CHARGE_CODE");
							Integer refundableFlag = rs.getInt("REFUNDABLE_CHARGE_FLAG");

							if (refundableFlag != null) {
								isChageRefundable = refundableFlag.intValue() == 1;
							} else {
								isChageRefundable = false;
							}

							chargeRefundabilityMap.put(chargeCode, isChageRefundable);

						}
						return chargeRefundabilityMap;
					}
				});

		return chargeRefundabilityMap;

	}

	@SuppressWarnings("unchecked")
	public List<String> getAllActiveServiceTaxChargeCodes() throws CommonsDataAccessException {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		// Final SQL
		String sql = "SELECT DISTINCT CHARGE_CODE FROM T_CHARGE WHERE STATUS ='ACT' AND CHARGE_CATEGORY_CODE='SERVICE_TAX'";

		List<String> chargeCodes = (List<String>) jt.query(sql, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<String> chargeCodes = new ArrayList<String>();

				if (rs != null) {
					while (rs.next()) {
						chargeCodes.add(rs.getString("CHARGE_CODE"));
					}
				}

				return chargeCodes;
			}
		});

		log.debug("Exit getAllActiveServiceTaxChargeCodes");
		return chargeCodes;
	}

	@Override
	public void removeApplicableCharges(String chargeCode) {
		String hql = "select chargeApplicableCharges from ChargeApplicableCharges chargeApplicableCharges where "
				+ " chargeApplicableCharges.chargeCode = '" + chargeCode + "' ";
		Collection<ChargeApplicableCharges> applicableCharges = find(hql, ChargeApplicableCharges.class);

		if (applicableCharges.size() > 0) {
			deleteAll(applicableCharges);
		}

	}

	@Override
	public Charge getCharge(ChargeSearchCriteria chargeSearchCriteria) {

		Charge charge = null;
		List<Charge> chargeList = null;
		Criteria chargeCriteria = getSession().createCriteria(Charge.class);

		try {
			if (chargeSearchCriteria.getCountryCode() != null) {
				if ("".equals(chargeSearchCriteria.getCountryCode())) {
					chargeCriteria.add(Restrictions.eq("countryCode", null));
				} else {
					chargeCriteria.add(Restrictions.eq("countryCode", chargeSearchCriteria.getCountryCode()));
				}
			}

			if (chargeSearchCriteria.getStateId() != null) {
				chargeCriteria.add(Restrictions.eq("stateId", chargeSearchCriteria.getStateId()));
			} else {
				chargeCriteria.add(Restrictions.eq("stateId", null));
			}

			if (chargeSearchCriteria.getInterState() != null) {
				if ("".equals(chargeSearchCriteria.getInterState())) {
					chargeCriteria.add(Restrictions.eq("interState", "N"));
				} else {
					chargeCriteria.add(Restrictions.eq("interState", chargeSearchCriteria.getInterState()));
				}
			}
			
			if (chargeSearchCriteria.getTicketingServiceTax() != null) {
				if ("".equals(chargeSearchCriteria.getTicketingServiceTax())) {
					chargeCriteria.add(Restrictions.eq("ticketingServiceTax", "N"));
				} else {
					chargeCriteria.add(Restrictions.eq("ticketingServiceTax", chargeSearchCriteria.getTicketingServiceTax()));
				}
			}

			chargeList = chargeCriteria.list();

		} catch (Exception e) {
			throw new CommonsDataAccessException(e, "module.hibernate.objectnotfound");
		}
		if (chargeList != null && chargeList.size() > 0) {
			charge = chargeList.get(0);
		}

		return charge;
	}

	@Override
	public Collection<Charge> getCharges(ChargeSearchCriteria chargeSearchCriteria) {

		String s[] = getHQLForCharges(chargeSearchCriteria, null);
		String hql = s[0];
		// String hqlCount = s[1];
		String hasDateSearch = s[2];

		Query query = getSession().createQuery(hql);

		try {
			if (chargeSearchCriteria.getChargeCode() != null) {
				query.setParameter("chargecode", chargeSearchCriteria.getChargeCode());
			}

			if (chargeSearchCriteria.getChargeCategoryCode() != null) {
				query.setParameter("chargecategorycode", chargeSearchCriteria.getChargeCategoryCode());
			}

			if (chargeSearchCriteria.getChargeGroupCode() != null) {
				query.setParameter("chargegroupcode", chargeSearchCriteria.getChargeGroupCode());
			}

			if (chargeSearchCriteria.getChargeStatus() != null && !chargeSearchCriteria.getChargeStatus().equals("All")) {
				query.setParameter("status", chargeSearchCriteria.getChargeStatus());
			}

			if (hasDateSearch.equals("y")) {
				if (chargeSearchCriteria.getFromDate() != null && chargeSearchCriteria.getToDate() == null) {
					query.setParameter("chargeeffectivefromdate", chargeSearchCriteria.getFromDate());
				}

				if (chargeSearchCriteria.getFromDate() == null && chargeSearchCriteria.getToDate() != null) {
					query.setParameter("chargeeffectivetodate", chargeSearchCriteria.getToDate());
				}

				if (chargeSearchCriteria.getFromDate() != null && chargeSearchCriteria.getToDate() != null) {
					query.setParameter("chargeeffectivefromdate", chargeSearchCriteria.getFromDate());
					query.setParameter("chargeeffectivetodate", chargeSearchCriteria.getToDate());
				}

			}

		} catch (Exception e) {

			throw new CommonsDataAccessException(e, null);
		}

		return query.list();
	}
	
	
	@SuppressWarnings("unchecked")
	public Boolean isEnableLMSCreditBlock(int tempTransactionId) throws CommonsDataAccessException {

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		StringBuilder sbSql = new StringBuilder();

		sbSql.append("SELECT pgw.ENABLED_LMS_CREDIT_BLOCK");
		sbSql.append(" FROM T_CCARD_PAYMENT_STATUS cstatus");
		sbSql.append(" INNER JOIN T_PAYMENT_GATEWAY_CURRENCY gtwcurrency");
		sbSql.append(" ON cstatus.GATEWAY_NAME = gtwcurrency.PAYMENT_GATEWAY_CURRENCY_CODE");
		sbSql.append(" INNER JOIN T_PAYMENT_GATEWAY pgw");
		sbSql.append(" ON pgw.PAYMENT_GATEWAY_ID = gtwcurrency.PAYMENT_GATEWAY_ID");
		sbSql.append(" WHERE cstatus.TPT_ID ='" + tempTransactionId + "'");

		Boolean isEnabledLMSCreditBlock = (Boolean) jt.query(sbSql.toString(), new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Boolean isEnabledLMSCreditBlock = false;

				if (rs != null) {
					while (rs.next()) {
						isEnabledLMSCreditBlock = ("Y").equals(rs.getString("ENABLED_LMS_CREDIT_BLOCK"));
					}
				}

				return isEnabledLMSCreditBlock;
			}
		});

		return isEnabledLMSCreditBlock;
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<ONDBaggageTemplate> getAffectedONDBaggageTemplates(String currencyCode) throws ModuleException {
		String hql = "select distinct OndBaggageTemp from ONDBaggageTemplate OndBaggageTemp left join OndBaggageTemp.baggageCharges ondBgChrgs where OndBaggageTemp.status = '"
				+ AirPricingCustomConstants.STATUS_ACTIVE + "' and ondBgChrgs.status = '" + AirPricingCustomConstants.STATUS_ACTIVE
				+ "' and OndBaggageTemp.toDate >=:effectiveDate and OndBaggageTemp.chargeLocalCurrencyCode=:currencyCode";
		Query q = getSession().createQuery(hql);
		q.setParameter("currencyCode", currencyCode);
		q.setTimestamp("effectiveDate", new Date());
		return q.list();		
	}

	@Override
	public List<MealTemplate> getAffectedMealTemplates(String currencyCode) throws ModuleException {
		String hql = "select distinct mealTemp from MealTemplate mealTemp left join mealTemp.mealCharges mealChrgs where mealTemp.status = '"
				+ AirPricingCustomConstants.STATUS_ACTIVE + "' and mealChrgs.status = '" + AirPricingCustomConstants.STATUS_ACTIVE
				+ "' and mealTemp.chargeLocalCurrencyCode=:currencyCode";
		Query q = getSession().createQuery(hql);
		q.setParameter("currencyCode", currencyCode);
		return q.list();
		
	}

	@Override
	public List<BaggageTemplate> getAffectedBaggageTemplates(String currencyCode) throws ModuleException {
		String hql = "select distinct baggageTemp from BaggageTemplate baggageTemp left join baggageTemp.baggageCharges bgChrgs where baggageTemp.status = '"
				+ AirPricingCustomConstants.STATUS_ACTIVE + "' and bgChrgs.status = '" + AirPricingCustomConstants.STATUS_ACTIVE
				+ "' and baggageTemp.chargeLocalCurrencyCode=:currencyCode";
		Query q = getSession().createQuery(hql);
		q.setParameter("currencyCode", currencyCode);
		return q.list();
		
	}

	@Override
	public List<SSRCharge> getAffectedSSRCharges(String currencyCode) throws ModuleException {
		String hql = "select distinct ssrCharge from SSRCharge ssrCharge where ssrCharge.status = '"		
				+ AirPricingCustomConstants.STATUS_ACTIVE    
				+ "' and ssrCharge.chargeLocalCurrencyCode=:currencyCode";
		Query q = getSession().createQuery(hql);
		q.setParameter("currencyCode", currencyCode);
		return q.list();
		
	}

	@Override
	public List<ChargeTemplate> getAffectedChargeTemplates(String currencyCode) throws ModuleException {
		String hql = "select distinct chargeTemp from ChargeTemplate chargeTemp left join chargeTemp.seatCharges seatChrgs where chargeTemp.status = '"
				+ AirPricingCustomConstants.STATUS_ACTIVE + "' and seatChrgs.status = '" + AirPricingCustomConstants.STATUS_ACTIVE
				+ "' and chargeTemp.chargeLocalCurrencyCode=:currencyCode";
		Query q = getSession().createQuery(hql);
		q.setParameter("currencyCode", currencyCode);
		return q.list();
		
	}
	
	public void removeMealTemplatesCategoryRestrictions(Integer mealTemplateId) {
		Query q = getSession()
				.createQuery("DELETE MultiSelectCategoryRestriction m WHERE m.templateId.templateId = :mealTemplateId");
		q.setParameter("mealTemplateId", mealTemplateId);
		q.executeUpdate();
	}
}
