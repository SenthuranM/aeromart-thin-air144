package com.isa.thinair.airpricing.core.persistence.jdbc.chargejdbcdao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airpricing.api.model.ChargeRate;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;

/**
 * @author MN, Byorn
 */
public class ChargeQuoteForGroupsResultSetExtractor implements ResultSetExtractor {

	@Override
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

		HashMap<String, QuotedChargeDTO> quotedChargeDTOs = new HashMap<String, QuotedChargeDTO>();
		String newchargeCode = null;
		while (rs.next()) {

			QuotedChargeDTO quotedChargeDTO = new QuotedChargeDTO();
			String chargeCode = rs.getString("charge_code");

			if (newchargeCode != null && newchargeCode.equals(chargeCode)) {

				throw new CommonsDataAccessException("airpricing.chargequoteforgroup.resultset.error");

			}

			newchargeCode = rs.getString("charge_code");
			quotedChargeDTO.setChargeCode(chargeCode);
			quotedChargeDTO.setChargeDescription(rs.getString("charge_description"));
			quotedChargeDTO.setChargeGroupCode(rs.getString("charge_group_code"));
			quotedChargeDTO.setChargeCategoryCode(rs.getString("charge_category_code"));
			// quotedChargeDTO.setAppliesToInfants(rs.getBoolean("applies_to_infants"));
			quotedChargeDTO.setChargeRateId(rs.getInt("charge_rate_id"));
			String chargeBasis = rs.getString("charge_basis");
			// double chargeValue = rs.getDouble("value_in_local_currency");
			quotedChargeDTO.setValueInLocalCurrency(rs.getDouble("value_in_local_currency"));
			quotedChargeDTO.setLocalCurrencyCode(rs.getString("local_currency_code"));
			quotedChargeDTO.setBreakPoint(rs.getBigDecimal("break_point"));
			quotedChargeDTO.setBoundryValue(rs.getBigDecimal("boundry_value"));
			double chargePercentage = rs.getDouble("charge_value_percentage");

			quotedChargeDTO.setChargeValueInPercentage(chargeBasis != null && chargeBasis.equals(ChargeRate.CHARGE_BASIS_V)
					? false
					: true);

			quotedChargeDTO.setChargeValuePercentage(chargePercentage);

			String isBundledFare = rs.getString("bundled_fare_charge");
			quotedChargeDTO.setBundledFareCharge(isBundledFare != null && "Y".equals(isBundledFare));

			// TODO check effective charge need to be set?

			quotedChargeDTOs.put(quotedChargeDTO.getChargeGroupCode(), quotedChargeDTO);
		}
		return quotedChargeDTOs;
	}
}
