package com.isa.thinair.airpricing.core.persistence.hibernate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airpricing.api.criteria.DefaultAnciTemplSearchCriteria;
import com.isa.thinair.airpricing.api.dto.DefaultAnciTemplateTO;
import com.isa.thinair.airpricing.api.model.DefaultAnciTemplate;
import com.isa.thinair.airpricing.api.model.DefaultAnciTemplateRoute;
import com.isa.thinair.airpricing.core.persistence.dao.CommonAncillaryDAO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.commons.core.util.StringUtil;

/**
 * 
 * @isa.module.dao-impl dao-name="CommonAncillaryDAO"
 * 
 *                      CommonAncillaryDAOImpl is the CommonAncillaryDAOImpl implementation for hibernate
 * 
 */

public class CommonAncillaryDAOImpl extends PlatformBaseHibernateDaoSupport implements CommonAncillaryDAO {

	private final String COMMA_SPILLITTER = ",";
	private static Log log = LogFactory.getLog(CommonAncillaryDAOImpl.class);

	@Override
	public Page<DefaultAnciTemplate> getdefaultAnciTemaplates(int startRec, int noRecs) {

		searchDefAnciTemplate(null, 0, 10);

		String hql = "from DefaultAnciTemplate";
		String hqlCount = "select count(*) from DefaultAnciTemplate";

		Query qResults = getSession().createQuery(hql).setFirstResult(startRec).setMaxResults(noRecs);
		Query qCount = getSession().createQuery(hqlCount);

		int count = ((Long) qCount.uniqueResult()).intValue();
		@SuppressWarnings("unchecked")
		Collection<DefaultAnciTemplate> colList = qResults.list();
		return new Page<DefaultAnciTemplate>(count, startRec, startRec + noRecs - 1, colList);
	}

	@Override
	public void saveDefaultAnciTemplate(DefaultAnciTemplate template) {
		if (template.getVersion() != -1) {
			List<DefaultAnciTemplateRoute> templateRoutes = find(
					"from DefaultAnciTemplateRoute templRt where templRt.template.defaultAnciTemplateId = ?",
					template.getDefaultAnciTemplateId(), DefaultAnciTemplateRoute.class);
			deleteAll(templateRoutes);
		}
		hibernateSaveOrUpdate(template);

	}

	@Override
	public void deleteDefaultAnciTemplate(DefaultAnciTemplate template) {
		Object anciTemplate = load(DefaultAnciTemplate.class, template.getDefaultAnciTemplateId());
		delete(anciTemplate);
	}

	@Override
	public Page<DefaultAnciTemplateTO> searchDefAnciTemplate(DefaultAnciTemplSearchCriteria criteria, int startRec, int noRecs) {

		Collection<DefaultAnciTemplateTO> colList = getDefAnciTemplTOList(criteria, startRec, noRecs);

		return new Page<DefaultAnciTemplateTO>(colList.size(), startRec, startRec + noRecs - 1, colList);
	}

	private Collection<DefaultAnciTemplateTO> getDefAnciTemplTOList(DefaultAnciTemplSearchCriteria criteria, int startRec,
			int noRecs) {

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		Collection paramList = new ArrayList<>();
		String sql = getSearchSqlQuery(criteria, paramList);

		Collection<DefaultAnciTemplateTO> templList = (List<DefaultAnciTemplateTO>) jt.query(sql, paramList.toArray(),
				new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

						Map templMap = new LinkedHashMap();
						Set ondIdset = new HashSet();
						Collection templateCol = new ArrayList();
						DefaultAnciTemplateTO template;
						DefaultAnciTemplateRoute ond;

						List<DefaultAnciTemplateTO> templateList = new ArrayList<DefaultAnciTemplateTO>();
						if (rs != null) {

							while (rs.next()) {

								int defAnciTemplId = rs.getInt("DEF_ANCI_TEMPL_ID");
								int ondId = rs.getInt("DEF_ANCI_OND_ID");

								if (templMap.containsKey(defAnciTemplId)) {

									template = (DefaultAnciTemplateTO) templMap.get(defAnciTemplId);

									if (!ondIdset.contains(ondId)) {
										ond = getOND(rs);

										template.addOND(ond);
										ondIdset.add(ondId);
									}
								} else {

									template = getDefAnciTemplate(defAnciTemplId, rs, true);

									ond = getOND(rs);
									template.addOND(ond);
									ondIdset.add(ondId);

									templMap.put(defAnciTemplId, template);
								}
							}
						}
						templateCol = templMap.values();
						return new ArrayList(templateCol);
					}

				});

		return templList;
	}

	private static String getSearchSqlQuery(DefaultAnciTemplSearchCriteria criteria, Collection paramList) {

		String sqWhereClause = createWhereClause(criteria, paramList);

		String sqlUnion = " UNION ";
		String sql = "";

		String sqlSEAT = " SELECT def_templ.DEFAULT_ANCI_TEMPLATE_ID  AS DEF_ANCI_TEMPL_ID,  "
				+ " def_templ.ANCI_TYPE                      AS ANCI_TYPE,  "
				+ " def_templ.ANCI_TEMPLATE_ID               AS ANCI_TEMPLID,  "
				+ " def_templ.STATUS                         AS DEF_ANCI_TEMPL_STATUS,  "
				+ " def_templ.MODEL_NUMBER                   AS AC_MODEL,  "
				+ " def_templ.CREATED_BY                     AS CREATED_BY,  "
				+ " def_templ.CREATED_DATE                   AS CREATED_DATE,  "
				+ " def_templ.MODIFIED_BY                    AS MODIFIED_BY,  "
				+ " def_templ.MODIFIED_DATE                  AS MODIFIED_DATE,  "
				+ " def_templ.VERSION                        AS VERSION,  "
				+ " templ_ond.DEFAULT_ANCI_TEMPLATE_ROUTE_ID AS DEF_ANCI_OND_ID,  "
				+ " templ_ond.OND_CODE                       AS DEF_ANCI_OND_CODE,  "
				+ " templ_ond.VERSION                        AS DEF_ANDI_OND_VERSION,  "
				+ " seat_templ.amc_template_code             AS TEMPL_CODE  " + " FROM t_default_anci_template def_templ  "
				+ " LEFT OUTER JOIN t_default_anci_template_route templ_ond  "
				+ " ON def_templ.default_anci_template_id =templ_ond.default_anci_template_id  "
				+ " LEFT OUTER JOIN sm_t_am_charge_template seat_templ  "
				+ " ON def_templ.anci_template_id = seat_templ.amc_template_id  " + " WHERE def_templ.anci_type     = 'SEAT'  ";

		String sqlMEAL = " SELECT def_templ.DEFAULT_ANCI_TEMPLATE_ID  AS DEF_ANCI_TEMPL_ID,  "
				+ " def_templ.ANCI_TYPE                      AS ANCI_TYPE,  "
				+ " def_templ.ANCI_TEMPLATE_ID               AS ANCI_TEMPLID,  "
				+ " def_templ.STATUS                         AS DEF_ANCI_TEMPL_STATUS,  "
				+ " def_templ.MODEL_NUMBER                   AS AC_MODEL,  "
				+ " def_templ.CREATED_BY                     AS CREATED_BY,  "
				+ " def_templ.CREATED_DATE                   AS CREATED_DATE,  "
				+ " def_templ.MODIFIED_BY                    AS MODIFIED_BY,  "
				+ " def_templ.MODIFIED_DATE                  AS MODIFIED_DATE,  "
				+ "  def_templ.VERSION                        AS VERSION,  "
				+ "  templ_ond.DEFAULT_ANCI_TEMPLATE_ROUTE_ID AS DEF_ANCI_OND_ID,  "
				+ "  templ_ond.OND_CODE                       AS DEF_ANCI_OND_CODE,  "
				+ "  templ_ond.VERSION                        AS DEF_ANDI_OND_VERSION,  "
				+ "  meal_templ.meal_template_code            AS TEMPL_CODE  " + " FROM t_default_anci_template def_templ  "
				+ " LEFT OUTER JOIN t_default_anci_template_route templ_ond  "
				+ " ON def_templ.default_anci_template_id =templ_ond.default_anci_template_id  "
				+ " LEFT OUTER JOIN ml_t_meal_template meal_templ  "
				+ " ON def_templ.anci_template_id = meal_templ.meal_template_id  " + " WHERE def_templ.anci_type     = 'MEAL'  ";

		String sqLBAGGAGE = " SELECT def_templ.DEFAULT_ANCI_TEMPLATE_ID  AS DEF_ANCI_TEMPL_ID,  "
				+ "  def_templ.ANCI_TYPE                      AS ANCI_TYPE,  "
				+ "  def_templ.ANCI_TEMPLATE_ID               AS ANCI_TEMPLID,  "
				+ "  def_templ.STATUS                         AS DEF_ANCI_TEMPL_STATUS,  "
				+ "  def_templ.MODEL_NUMBER                   AS AC_MODEL,  "
				+ "  def_templ.CREATED_BY                     AS CREATED_BY,  "
				+ "  def_templ.CREATED_DATE                   AS CREATED_DATE,  "
				+ "  def_templ.MODIFIED_BY                    AS MODIFIED_BY,  "
				+ "  def_templ.MODIFIED_DATE                  AS MODIFIED_DATE,  "
				+ "  def_templ.VERSION                        AS VERSION,  "
				+ "  templ_ond.DEFAULT_ANCI_TEMPLATE_ROUTE_ID AS DEF_ANCI_OND_ID,  "
				+ "  templ_ond.OND_CODE                       AS DEF_ANCI_OND_CODE,  "
				+ "  templ_ond.VERSION                        AS DEF_ANDI_OND_VERSION,  "
				+ "  bg_templ.baggage_template_code           AS TEMPL_CODE  " + "	FROM t_default_anci_template def_templ  "
				+ "	LEFT OUTER JOIN t_default_anci_template_route templ_ond  "
				+ "	ON def_templ.default_anci_template_id =templ_ond.default_anci_template_id  "
				+ "	LEFT OUTER JOIN bg_t_baggage_template bg_templ  "
				+ "	ON def_templ.anci_template_id = bg_templ.baggage_template_id  "
				+ "	WHERE def_templ.anci_type     = 'BAGGAGE'  ";

		if (criteria != null) {
			if (DefaultAnciTemplate.ANCI_TEMPLATES.BAGGAGE.toString().equals(criteria.getAnciType())) {
				sql = sqLBAGGAGE;
			} else if (DefaultAnciTemplate.ANCI_TEMPLATES.MEAL.toString().equals(criteria.getAnciType())) {
				sql = sqlMEAL;
			} else if (DefaultAnciTemplate.ANCI_TEMPLATES.SEAT.toString().equals(criteria.getAnciType())) {
				sql = sqlSEAT;
			} else {
				sql = sqLBAGGAGE + sqlUnion + sqlMEAL + sqlUnion + sqlSEAT ;
			}
		} else {
			sql = sqLBAGGAGE + sqlUnion + sqlMEAL + sqlUnion + sqlSEAT ;
		}

		StringBuilder sb = new StringBuilder();
		sb.append(" Select * from  (   ").append(sql).append(" )  tmpTable  ").append(sqWhereClause);

		return sb.toString();
	}

	private static String createWhereClause(DefaultAnciTemplSearchCriteria criteria, Collection paramList) {
		final String ondCodeMatch = " tmpTable.DEF_ANCI_OND_CODE like ? ";
		final String statusMactch = " tmpTable.DEF_ANCI_TEMPL_STATUS = ? ";

		List<String> sqlStrList = new ArrayList<>();
		StringBuilder sb = new StringBuilder();

		if (isNull(criteria)) {
			return sb.toString();
		}

		if (!isNull(criteria.getStatus())) {
			sqlStrList.add(statusMactch);
			paramList.add(criteria.getStatus());
		}

		if (!isNull(criteria.getOrigin()) && !isNull(criteria.getDepature())) {
			sqlStrList.add(ondCodeMatch);
			paramList.add(criteria.getOrigin().concat("%").concat(criteria.getDepature()));
		} else if (!isNull(criteria.getOrigin())) {
			sqlStrList.add(ondCodeMatch);
			paramList.add(criteria.getOrigin().concat("%"));
		} else if (!isNull(criteria.getDepature())) {
			sqlStrList.add(ondCodeMatch);
			paramList.add("%".concat(criteria.getDepature()));
		}

		boolean whereIncluded = false;
		for (String sqlStr : sqlStrList) {
			if (!whereIncluded) {
				sb.append("  where  ").append(sqlStr);
				whereIncluded = true;
			} else {
				sb.append("  and  ").append(sqlStr);
			}
		}
		return sb.toString();
	}

	private static boolean isNull(Object obj) {
		if (obj == null || "".equals(obj.toString())) {
			return true;
		} else {
			return false;
		}
	}

	private DefaultAnciTemplateRoute getOND(ResultSet rs) throws SQLException {

		DefaultAnciTemplateRoute ond = new DefaultAnciTemplateRoute();

		ond.setDefaultAnciTemplateRouteId(rs.getInt("DEF_ANCI_OND_ID"));
		ond.setOndCode(BeanUtils.nullHandler(rs.getString("DEF_ANCI_OND_CODE")));
		ond.setVersion(rs.getInt("DEF_ANDI_OND_VERSION"));

		return ond;
	}

	private DefaultAnciTemplateTO getDefAnciTemplate(int defAnciTemplId, ResultSet rs, boolean b) throws SQLException {
		DefaultAnciTemplateTO template = new DefaultAnciTemplateTO();

		template.setDefaultAnciTemplateId(rs.getInt("DEF_ANCI_TEMPL_ID"));
		template.setAncillaryType(BeanUtils.nullHandler(rs.getString("ANCI_TYPE")));
		template.setAnciTemplateId(rs.getInt("ANCI_TEMPLID"));
		template.setStatus(BeanUtils.nullHandler(rs.getString("DEF_ANCI_TEMPL_STATUS")));
		template.setAirCraftModelnumber(BeanUtils.nullHandler(rs.getString("AC_MODEL")));
		template.setCreatedBy(BeanUtils.nullHandler(rs.getString("CREATED_BY")));
		template.setCreatedDate(rs.getTimestamp("CREATED_DATE"));
		template.setModifiedBy(BeanUtils.nullHandler(rs.getString("MODIFIED_BY")));
		template.setModifiedDate(rs.getTimestamp("MODIFIED_DATE"));
		template.setVersion(rs.getInt("VERSION"));
		template.setAnciTemplateCode(BeanUtils.nullHandler(rs.getString("TEMPL_CODE")));

		return template;
	}

	@Override
	public boolean checkDuplicateDefaultAnciTemplateForSameRoute(DefaultAnciTemplateTO templateTO) {

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		Collection paramList = new ArrayList<>();

		paramList.add(templateTO.getAncillaryType());
		
		String sql = "  SELECT COUNT(*) AS ROW_COUNT  " + "  FROM t_default_anci_template def_templ  "
				+ "  JOIN t_default_anci_template_route def_templ_route  "
				+ "  ON def_templ.default_anci_template_id = def_templ_route.default_anci_template_id  "
				+ "  WHERE def_templ.anci_type             =?  " + "  AND def_templ_route.ond_code         IN  "
				+ getONDSQLStr(templateTO.getOndString());

		if (templateTO.getVersion() >= 0 && templateTO.getDefaultAnciTemplateId() > 0) {
			sql = sql + "  and def_templ.default_anci_template_id != ?  ";
			paramList.add(templateTO.getDefaultAnciTemplateId());
		}

		Integer count = (Integer) jt.query(sql, paramList.toArray(), new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					return new Integer(rs.getInt("ROW_COUNT"));
				}
				return null;

			}
		});

		return count > 0;
	}

	private String getONDSQLStr(String ondString) {

		String[] onDCodes = ondString.split(COMMA_SPILLITTER);
		StringBuilder sb = new StringBuilder();
		sb.append("( ");

		for (String ondCode : onDCodes) {
			sb.append("'").append(ondCode).append("',");
		}
		sb.deleteCharAt(sb.length() - 1);
		sb.append(" )");
		return sb.toString();
	}

	@Override
	public boolean checkTemplateAttachedToRouteWiseDefAnciTempl(DefaultAnciTemplate.ANCI_TEMPLATES anciType, int templateId) {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		Collection paramList = new ArrayList<>();

		paramList.add(anciType.toString());
		paramList.add(templateId);
		String tblName, clmName;

		if (DefaultAnciTemplate.ANCI_TEMPLATES.MEAL.equals(anciType)) {

			tblName = "  ml_t_meal_template  ";
			clmName = "meal_template_id  ";

		} else if (DefaultAnciTemplate.ANCI_TEMPLATES.SEAT.equals(anciType)) {

			tblName = "  sm_t_am_charge_template  ";
			clmName = "amc_template_id  ";

		} else {

			tblName = "  bg_t_baggage_template  ";
			clmName = "baggage_template_id  ";
		}

		String sql = "  SELECT count(*) as ROW_COUNT  " + "  FROM t_default_anci_template dt " + "  JOIN " + tblName
				+ " templTbl  " + "  ON dt.anci_template_id = templTbl." + clmName + "  WHERE dt.status        ='ACT'  "
				+ "  AND dt.anci_type       = ?  " + "  AND dt.anci_template_id= ?  ";

		Integer count = (Integer) jt.query(sql, paramList.toArray(), new ResultSetExtractor() {
			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					return new Integer(rs.getInt("ROW_COUNT"));
				}
				return null;

			}
		});

		return count > 0;
	}

	@Override
	public Map<String, Integer> getDefaultAnciTemplateForFlight(String ondCode, String flightModel) {

		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		String ondCodeCriteria = null;
		if (!StringUtil.isNullOrEmpty(ondCode)) {
			String[] segments = ondCode.split("/");
			String originAnyDestination = segments[0] + "/***";
			String anyOriginDestination = "***/" + segments[segments.length - 1];
			ondCodeCriteria = " (rt.ond_code = '" + ondCode + "' OR rt.ond_code = '" + originAnyDestination
					+ "' OR rt.ond_code = '" + anyOriginDestination + "') ";

		}

		StringBuilder sqlSb = new StringBuilder();
		sqlSb.append("SELECT tm.anci_type as ANCI_TYPE,tm.anci_template_id as ANCI_TEMPL_ID ");
		sqlSb.append("FROM t_default_anci_template tm JOIN t_default_anci_template_route rt ");
		sqlSb.append("ON tm.default_anci_template_id = rt.default_anci_template_id ");
		sqlSb.append("WHERE " + ondCodeCriteria + " ");
		sqlSb.append("AND tm.status = '" + DefaultAnciTemplate.STATUS_ACTIVE + "'  ");
		sqlSb.append("AND ((tm.anci_type ='SEAT' ");
		sqlSb.append("AND tm.model_number = '" + flightModel + "')  ");
		sqlSb.append("OR (tm.anci_type IN ('MEAL')))   ");

		Map<String, Integer> anciTypeTemplIdMap = (Map<String, Integer>) jt.query(sqlSb.toString(), new ResultSetExtractor() {
			public Map extractData(ResultSet rs) throws SQLException, DataAccessException {

				Map<String, Integer> map = new HashMap<>();

				if (rs != null) {

					while (rs.next()) {

						map.put(rs.getString("ANCI_TYPE"), rs.getInt("ANCI_TEMPL_ID"));

					}
				}
				return map;
			}
		});

		return anciTypeTemplIdMap;
	}

}
