/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:12:34
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airpricing.core.persistence.dao;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.seatavailability.FareSummaryDTO;
import com.isa.thinair.airpricing.api.criteria.AgentFareRuleSearchCriteria;
import com.isa.thinair.airpricing.api.dto.AgentFareRulesDTO;
import com.isa.thinair.airpricing.api.dto.FareRuleFeeTO;
import com.isa.thinair.airpricing.api.dto.FareTO;
import com.isa.thinair.airreservation.api.dto.OnHoldReleaseTimeDTO;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;

/**
 * BHive Commons: FareRule module
 * 
 * @author Byorn
 */
public interface FareRuleJdbcDAO {

	public Collection<AgentFareRulesDTO> getAgentFareRules(AgentFareRuleSearchCriteria agentFareRuleSearchCriteria,
			List<String> orderBy) throws CommonsDataAccessException;

	/**
	 * Interface method that excepts fareIds and will return a list of FareTOs
	 * 
	 * @param prefferedLanguage
	 **/
	public Collection<FareTO> getRefundableFares(Collection<Integer> fareIds, String prefferedLanguage);

	/** Interface method that excepts fareRuleIds and will return a list of FareSummaryDTOs **/
	public Map<Integer, FareSummaryDTO> getPaxDetails(Collection<Integer> fareRuleIds);

	/** temporarily include in fareRule - should be moved to Fare jdbc dao **/
	public Map<Integer, FareSummaryDTO> getLocalFares(Collection<Integer> fareIds);

	public Collection<FareRuleFeeTO> getFareRuleFees(Collection<Integer> fareRuleIds);

	public Collection<String> getFareRuleCodeNotInFareRule(String currencyCode);

	public Collection<OnHoldReleaseTimeDTO> getOHDRelTimeDTO(Integer fareRuleId);

	public Collection<Integer> getFareIdsForFareRule(Integer fareRuleId);
}
