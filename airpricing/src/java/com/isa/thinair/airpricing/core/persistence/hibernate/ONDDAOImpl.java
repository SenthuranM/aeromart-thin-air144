/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:12:34
 * 
 * $Id$
 * 
 * ===============================================================================
 */

package com.isa.thinair.airpricing.core.persistence.hibernate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airpricing.api.model.OriginDestination;
import com.isa.thinair.airpricing.core.persistence.dao.ONDDAO;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;

/**
 * BHive: Charge module
 * 
 * @author Byorn
 * 
 *         ONDDAOImpl is the ONDDAO implementatiion for hibernate
 * @isa.module.dao-impl dao-name="ONDDAO"
 */

public class ONDDAOImpl extends PlatformBaseHibernateDaoSupport implements ONDDAO {

	private Log log = LogFactory.getLog(getClass());

	/**
	 * The Following method will give a list of ONDS for a list of ondCodes. This method is used to distinquish any OND
	 * Codes that are not in the Master OND Table.
	 * 
	 * @return List
	 * @param List
	 *            of ONDCodes that will be in the IN part in the HQL
	 */
	public List<String> getONDsForCodes(Set<String> inList) {

		String inQuery = "";
		final int INLIST_LIMIT = 800;
		final int listSize = inList.size();
		List<String> ondCodeList = new ArrayList<String>(inList);
		for (int i = 0; i < listSize; i += INLIST_LIMIT) {
			if (inQuery.length() > 0) {
				inQuery += " OR ";
			}
			inQuery += " ond.ONDCode IN " + "(" + getInQuery(ondCodeList.subList(i, Math.min(listSize, i + INLIST_LIMIT))) + ")";
		}
		String strHql = "Select ond.ONDCode from OriginDestination as ond where " + inQuery;
		log.debug("Get List of ONDs for Codes");
		return find(strHql, String.class);

	}

	private static <T> String getInQuery(List<T> ondCodeList) {
		if (ondCodeList == null)
			return null;
		String inQuery = "";
		ListIterator<String> itr = (ListIterator<String>) ondCodeList.listIterator();
		while (itr.hasNext()) {
			inQuery = inQuery.equals("") ? "'" + itr.next().toString() + "'" : inQuery + "," + "'" + itr.next().toString() + "'";
		}
		return inQuery;
	}

	/**
	 * Saves a new OrginDestination.
	 * 
	 * @return void
	 */
	public void saveOND(OriginDestination originDestination) {
		if (log.isDebugEnabled()) {
			log.debug("Saving OND" + originDestination.getONDCode());
		}
		hibernateSaveOrUpdate(originDestination);
	}

	/**
	 * get an OriginDesitination for the ondCode
	 */
	public OriginDestination getOND(String ondCode) {
		if (log.isDebugEnabled()) {
			log.debug("get OND" + ondCode);
		}
		return (OriginDestination) get(OriginDestination.class, ondCode);

	}

}
