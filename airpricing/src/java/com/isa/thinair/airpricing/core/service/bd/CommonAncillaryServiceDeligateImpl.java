package com.isa.thinair.airpricing.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.airpricing.api.service.CommonAncillaryServiceBD;

@Remote
public interface CommonAncillaryServiceDeligateImpl extends CommonAncillaryServiceBD {

}
