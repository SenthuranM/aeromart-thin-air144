package com.isa.thinair.airpricing.core.util;

import com.isa.thinair.airpricing.api.model.ChargeRate;

public class SplitChargeBySalesUtil {

	public static void copyChargeRateAttributes(ChargeRate fromChargeRate, ChargeRate toChargeRate) {
		toChargeRate.setChargeCode(fromChargeRate.getChargeCode());
		toChargeRate.setChargeEffectiveFromDate(fromChargeRate.getChargeEffectiveFromDate());
		toChargeRate.setChargeEffectiveToDate(fromChargeRate.getChargeEffectiveToDate());
		toChargeRate.setChargeValuePercentage(fromChargeRate.getChargeValuePercentage());
		toChargeRate.setValueInLocalCurrency(fromChargeRate.getValueInLocalCurrency());
		toChargeRate.setStatus(fromChargeRate.getStatus());
		toChargeRate.setChargeBasis(fromChargeRate.getChargeBasis());
		toChargeRate.setChargeValueMin(fromChargeRate.getChargeValueMin());
		toChargeRate.setChargeValueMax(fromChargeRate.getChargeValueMax());
		toChargeRate.setOperationType(fromChargeRate.getOperationType());
		toChargeRate.setJourneyType(fromChargeRate.getJourneyType());
		toChargeRate.setCabinClassCode(fromChargeRate.getCabinClassCode());
		toChargeRate.setBreakPoint(fromChargeRate.getBreakPoint());
		toChargeRate.setBoundryValue(fromChargeRate.getBoundryValue());
		toChargeRate.setVersion(fromChargeRate.getVersion());
		toChargeRate.setChargeRateCode(fromChargeRate.getChargeRateCode());
		toChargeRate.setCurrencyCode(fromChargeRate.getCurrencyCode());
		toChargeRate.setSalesFromDate(fromChargeRate.getSalesFromDate());
		toChargeRate.setSaleslToDate(fromChargeRate.getSaleslToDate());
		toChargeRate.setValuePercentageFlag(fromChargeRate.getValuePercentageFlag());
	}
}
