package com.isa.thinair.airpricing.core.persistence.hibernate;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.FloatType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;

import com.isa.thinair.airpricing.api.dto.CCChargeTO;
import com.isa.thinair.airpricing.api.model.PaymentGatewayWiseCharges;
import com.isa.thinair.airpricing.core.persistence.dao.PaymentGatewayWiseChargesDAO;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformHibernateDaoSupport;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.Util;


/**
 * BHive: Charge module
 * 
 * @author Tharindu
 * 
 *         PaymentGatewayWiseChargesDAOImpl is the PaymentGatewayWiseChargesDAO implementatiion for hibernate
 * @isa.module.dao-impl dao-name="PaymentGatewayWiseChargesDAO"
 */
public class PaymentGatewayWiseChargesDAOImpl extends PlatformHibernateDaoSupport implements
		PaymentGatewayWiseChargesDAO {
	
	private Log log = LogFactory.getLog(getClass());

	@Override
	public PaymentGatewayWiseCharges getPaymentGatewayWiseCharges(int gatewayId) {
		if (log.isDebugEnabled()) {
			log.debug("get PaymentGatewayWiseCharges for PGW " + gatewayId);
		}
		PaymentGatewayWiseCharges charge = null;
		// apply date range to check validity
		List<PaymentGatewayWiseCharges> list = find(" from PaymentGatewayWiseCharges p where p.status = 'ACT' AND p.paymentGatewayId = ?", gatewayId, PaymentGatewayWiseCharges.class);

		if (list.size() != 0) {
			charge = list.get(0);
			Date now = CalendarUtil.getCurrentSystemTimeInZulu();
			if(!(charge.getChargeEffectiveFromDate().before(now) && charge.getChargeEffectiveToDate().after(now))){
				charge = null;
			}
		}
		return charge;

	}

	@Override
	public PaymentGatewayWiseCharges getUniquePaymentGatewayWiseCharge(int chargeId) {
		if (log.isDebugEnabled()) {
			log.debug("get PaymentGatewayWiseCharges with chargeId " + chargeId);
		}
		PaymentGatewayWiseCharges charge = null;
		// apply date range to check validity
		List<PaymentGatewayWiseCharges> list = find(" from PaymentGatewayWiseCharges p where  p.chargeId = ?", chargeId, PaymentGatewayWiseCharges.class);

		if (list.size() != 0) {
			charge = (PaymentGatewayWiseCharges) list.get(0);
		}
		return charge;

	}
	
	@Override
	public void savePaymentGatewayWiseCharges(PaymentGatewayWiseCharges paymentGatewayWiseCharges) throws ModuleException {
		if (log.isDebugEnabled()) {
			log.debug("Saving paymentGatewayWiseCharges for PGW " + paymentGatewayWiseCharges.getPaymentGatewayId());
		}
		hibernateSaveOrUpdate(paymentGatewayWiseCharges);

	}

	@Override
	public void delete(PaymentGatewayWiseCharges paymentGatewayWiseCharges){
		if (log.isDebugEnabled()) {
			log.debug("Deleting paymentGatewayWiseCharges for PGW " + paymentGatewayWiseCharges.getPaymentGatewayId());
		}
		delete(paymentGatewayWiseCharges);
	}
	
	@Override
	public Page<CCChargeTO> getPaymentGatewayWiseCharges(int startrec, int numofrecs, List<ModuleCriterion> criteria) {
		String hql = "";
		String hqlCount = "";
		final int fromRowNum = startrec;
		final int toRowNum = startrec + numofrecs;

		hql = "SELECT * FROM ( SELECT m.*, rownum r FROM ( Select paymentGatewayWiseCharges.PAYMENT_GATEWAY_WISE_CHARGE_ID AS creditCardWiseChargeId, paymentGatewayWiseCharges.PAYMENT_GATEWAY_ID AS pgwId, paymentGatewayWiseCharges.CHARGE_EFFECTIVE_FROM_DATE AS effectiveFromDate, paymentGatewayWiseCharges.CHARGE_EFFECTIVE_TO_DATE AS effectiveToDate, paymentGatewayWiseCharges.VALUE_PERCENTAGE_FLAG AS valuePercentageFlag, paymentGatewayWiseCharges.CHARGE_VALUE_PERCENTAGE AS chargeValuePercentage, paymentGatewayWiseCharges.VALUE_IN_DEFAULT_CURRENCY AS valueInDefaultCurrency, paymentGatewayWiseCharges.STATUS AS status, pgw.PROVIER_NAME AS pgwName, pgw.DESCRIPTION AS pgwDescription from t_payment_gateway_wise_charges paymentGatewayWiseCharges, t_payment_gateway pgw ";
		hqlCount = "Select count(*) from t_payment_gateway_wise_charges paymentGatewayWiseCharges, t_payment_gateway pgw ";
		
		String hqlWhere = " where ";
		if(criteria != null){
			for (ModuleCriterion cc : criteria) {
				if(cc.getFieldName().equals("pgwId")){
					cc.setFieldName("paymentGatewayWiseCharges.PAYMENT_GATEWAY_ID");
				} 
				if(cc.getFieldName().equals("ccwcStatus")){
					cc.setFieldName("paymentGatewayWiseCharges.STATUS");
				}
				if (hqlWhere.equals(" where ")) {
					if (cc.getFieldName().equals("effectiveDate")) {
						java.sql.Date sqlDate = (java.sql.Date) cc.getValue().get(0);
						hqlWhere += "paymentGatewayWiseCharges.CHARGE_EFFECTIVE_FROM_DATE <= to_date(" + CalendarUtil.formatForSQL_toDate(sqlDate) +") AND paymentGatewayWiseCharges.CHARGE_EFFECTIVE_TO_DATE >= to_date(" + CalendarUtil.formatForSQL_toDate(sqlDate)+ ") ";

					} else {
						hqlWhere += cc.getFieldName() + " " + cc.getCondition() + " "
								+ Util.buildStringInClauseContent(cc.getValue());
					}
				} else {
					hqlWhere += " AND ";
					hqlWhere += cc.getFieldName() + " " + cc.getCondition() + " "
								+ Util.buildStringInClauseContent(cc.getValue());
					
				}
			}
		}
		
		if (!hqlWhere.equals(" where ")) {
			hql = hql + hqlWhere + "AND paymentGatewayWiseCharges.PAYMENT_GATEWAY_ID = pgw.PAYMENT_GATEWAY_ID AND pgw.STATUS = 'ACT' ) m ) where r > " + fromRowNum + " and r < "  + toRowNum;
			hqlCount = hqlCount + hqlWhere + "AND paymentGatewayWiseCharges.PAYMENT_GATEWAY_ID = pgw.PAYMENT_GATEWAY_ID AND pgw.STATUS = 'ACT' ";
		} else {
			hql = hql + hqlWhere + "paymentGatewayWiseCharges.PAYMENT_GATEWAY_ID = pgw.PAYMENT_GATEWAY_ID AND pgw.STATUS = 'ACT' ) m ) where r > " + fromRowNum + " and r < "  + toRowNum;
			hqlCount = hqlCount + hqlWhere + "paymentGatewayWiseCharges.PAYMENT_GATEWAY_ID = pgw.PAYMENT_GATEWAY_ID AND pgw.STATUS = 'ACT' ";
		}

		SQLQuery qResults = getSession().createSQLQuery(hql);
		qResults.addScalar("creditCardWiseChargeId",IntegerType.INSTANCE);
		qResults.addScalar("pgwId", IntegerType.INSTANCE);
		qResults.addScalar("effectiveFromDate", TimestampType.INSTANCE);
		qResults.addScalar("effectiveToDate", TimestampType.INSTANCE);
		qResults.addScalar("valuePercentageFlag", StringType.INSTANCE);
		qResults.addScalar("chargeValuePercentage", FloatType.INSTANCE);
		qResults.addScalar("valueInDefaultCurrency", FloatType.INSTANCE);
		qResults.addScalar("status", StringType.INSTANCE);
		qResults.addScalar("pgwName", StringType.INSTANCE);
		qResults.addScalar("pgwDescription", StringType.INSTANCE);
		qResults.setResultTransformer(Transformers.aliasToBean(CCChargeTO.class));
		
		SQLQuery qCount = getSession().createSQLQuery(hqlCount);
		
		Collection<CCChargeTO> ccChargeList = qResults.list();
		int count = Integer.parseInt(qCount.uniqueResult().toString());

		return new Page<CCChargeTO>(count, startrec, startrec + numofrecs - 1, ccChargeList);
		
	}
}
