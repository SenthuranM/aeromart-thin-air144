/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airpricing.core.persistence.jdbc.farerulejdbcdao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.springframework.jdbc.core.PreparedStatementCreator;

import com.isa.thinair.airpricing.api.criteria.AgentFareRuleSearchCriteria;

/**
 * @author Byorn
 * 
 */
public class AgentFareRulePreparedStatementCreator implements PreparedStatementCreator {

	private String paramAgentCode;

	private String sql;

	/**
	 * Constructor
	 * 
	 * @param agentFareRuleSearchCriteria
	 * @param sql
	 */
	public AgentFareRulePreparedStatementCreator(AgentFareRuleSearchCriteria agentFareRuleSearchCriteria, String sql) {
		this.paramAgentCode = agentFareRuleSearchCriteria.getAgentCode() == null ? "%" : agentFareRuleSearchCriteria
				.getAgentCode();
		this.sql = sql;

	}

	/**
	 * Method that will attach the parameter to the sql and will parse the preparedStatement
	 * 
	 * @param Connection
	 * @return PreparedStatement
	 */
	public PreparedStatement createPreparedStatement(Connection con) throws SQLException {

		PreparedStatement pstat = con.prepareStatement(sql);
		pstat.setString(1, paramAgentCode);
		return pstat;

	}

}
