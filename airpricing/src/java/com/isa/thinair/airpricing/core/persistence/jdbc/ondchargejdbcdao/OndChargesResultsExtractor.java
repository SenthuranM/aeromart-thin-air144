package com.isa.thinair.airpricing.core.persistence.jdbc.ondchargejdbcdao;

/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airpricing.api.dto.ondcharge.OndChargeRateTO;
import com.isa.thinair.airpricing.api.dto.ondcharge.OndChargeTO;
import com.isa.thinair.airpricing.api.dto.ondcharge.OriginDestinationTO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;

/**
 * Class that JdbcTemplate will use.
 * 
 * @author Byorn
 * 
 */
public class OndChargesResultsExtractor implements ResultSetExtractor {

	private static final String OND_CODE = "ond_code";
	private static final String CHARGE_CODE = "charge_code";
	private static final String CHARGE_DESCRIPTION = "charge_description";
	private static final String CHARGE_GROUP_CODE = "charge_group_code";
	private static final String CHARGE_CATEGORY_CODE = "charge_category_code";
	private static final String CHARGE_CATEGORY_VALUE = "charge_category_value";
	private static final String APPLIES_TO = "applies_to";
	private static final String REFUNDABLE_CHARGE_FLAG = "refundable_charge_flag";
	private static final String CHARGE_RATE_ID = "charge_rate_id";
	private static final String EFFECTIVE_FROM = "charge_effective_from_date";
	private static final String EFFECTIVE_TO = "charge_effective_to_date";
	private static final String CHARGE_BASIS = "charge_basis";
	private static final String VALUE_PERCENTAGE = "charge_value_percentage";

	private static List<OriginDestinationTO> list;

	private String ondCode;

	public OndChargesResultsExtractor(String ondCode) {
		this.ondCode = ondCode;
	}

	/**
	 * Will return the List of Records extracted from the resultsSet List
	 * 
	 * @return
	 */
	public static List<OriginDestinationTO> getList() {
		return list;
	}

	/**
	 * Method that implements the ResultSetExtractor interface. This method is internally used by the JdbcTemplate.
	 * Method is used for accessing the resultSet and populating the DTO and adding it to the results LIst.
	 */
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

		list = new ArrayList<OriginDestinationTO>();

		OriginDestinationTO originDestinationDTO = null;
		OndChargeTO ondChargeTO = null;
		OndChargeRateTO ondChargeRateTO = null;

		while (rs.next()) {
			/** Check for first Record **/
			if (originDestinationDTO == null) {
				originDestinationDTO = new OriginDestinationTO();
				originDestinationDTO.setOndCode(rs.getString(OND_CODE) != null ? this.ondCode : BeanUtils.nullHandler(rs
						.getString(OND_CODE)));
				List<OndChargeTO> charges = new ArrayList<OndChargeTO>();

				if (ondChargeTO == null) {
					ondChargeTO = new OndChargeTO();
					ondChargeTO.setAppliesTo(rs.getInt(APPLIES_TO));
					ondChargeTO.setChargeCategoryCode(BeanUtils.nullHandler(rs.getString(CHARGE_CATEGORY_CODE)));
					ondChargeTO.setChargeCategoryValue(BeanUtils.nullHandler(rs.getString(CHARGE_CATEGORY_VALUE)));
					ondChargeTO.setChargeCode(BeanUtils.nullHandler(rs.getString(CHARGE_CODE)));
					ondChargeTO.setChargeDescription(BeanUtils.nullHandler(rs.getString(CHARGE_DESCRIPTION)));
					ondChargeTO.setChargeGroupCode(BeanUtils.nullHandler(rs.getString(CHARGE_GROUP_CODE)));
					ondChargeTO.setRefundableChargeFlag(rs.getInt(REFUNDABLE_CHARGE_FLAG));

					List<OndChargeRateTO> chargeRates = new ArrayList<OndChargeRateTO>();
					if (ondChargeRateTO == null) {
						ondChargeRateTO = new OndChargeRateTO();
						ondChargeRateTO.setChargeEffectiveFromDate(rs.getDate(EFFECTIVE_FROM));
						ondChargeRateTO.setChargeEffectiveToDate(rs.getDate(EFFECTIVE_TO));
						ondChargeRateTO.setChargeRateCode(rs.getInt(CHARGE_RATE_ID));
						ondChargeRateTO.setChargeValuePercentage(new Double(rs.getDouble(VALUE_PERCENTAGE)));
						ondChargeRateTO.setValuePercentageFlag(BeanUtils.nullHandler(rs.getString(CHARGE_BASIS)));
					}
					chargeRates.add(ondChargeRateTO);
					ondChargeTO.setChargeRates(chargeRates);
					charges.add(ondChargeTO);
				}
				originDestinationDTO.setCharges(charges);
				list.add(originDestinationDTO);
				continue;
			}
			/****** Finished Entering the First Record ****/

			String newOndCode = rs.getString(OND_CODE) != null ? this.ondCode : BeanUtils.nullHandler(rs.getString(OND_CODE));

			// check if the newOndCode has 'All' attached to it.
			// if so i will take it as the same ond code. i.e CMB/SHJ = CMB/ALL = ALL/SHJ

			/** Next record is for the same OND **/
			if (newOndCode.equals(originDestinationDTO.getOndCode())) {
				list.remove(list.lastIndexOf(originDestinationDTO));
				String newChargeCode = rs.getString(CHARGE_CODE);
				List<OndChargeTO> existingCharges = originDestinationDTO.getCharges();
				/** Same Charge code or New Charge Code **/
				if (newChargeCode.equals(ondChargeTO.getChargeCode())) {
					/** Same Charge Code **/
					existingCharges.remove(existingCharges.lastIndexOf(ondChargeTO));
					List<OndChargeRateTO> existingChargeRates = ondChargeTO.getChargeRates();
					int newChargeRateId = rs.getInt(CHARGE_RATE_ID);
					if (newChargeRateId == ondChargeRateTO.getChargeRateCode()) {
						// code should never come here.
						// throw new RuntimeException();
					} else {
						OndChargeRateTO ondChargeRateTO1 = new OndChargeRateTO();
						ondChargeRateTO1.setChargeEffectiveFromDate(rs.getDate(EFFECTIVE_FROM));
						ondChargeRateTO1.setChargeEffectiveToDate(rs.getDate(EFFECTIVE_TO));
						ondChargeRateTO1.setChargeRateCode(rs.getInt(CHARGE_RATE_ID));
						ondChargeRateTO1.setChargeValuePercentage(new Double(rs.getDouble(VALUE_PERCENTAGE)));
						ondChargeRateTO1.setValuePercentageFlag(BeanUtils.nullHandler(rs.getString(CHARGE_BASIS)));
						existingChargeRates.add(ondChargeRateTO1);
					}
					ondChargeTO.setChargeRates(existingChargeRates);
					existingCharges.add(ondChargeTO);
					originDestinationDTO.setCharges(existingCharges);
					list.add(originDestinationDTO);
					continue;
				} else {
					/** New Charge Code **/
					ondChargeTO = new OndChargeTO();
					ondChargeTO.setAppliesTo(rs.getInt(APPLIES_TO));
					ondChargeTO.setChargeCategoryCode(BeanUtils.nullHandler(rs.getString(CHARGE_CATEGORY_CODE)));
					ondChargeTO.setChargeCategoryValue(BeanUtils.nullHandler(rs.getString(CHARGE_CATEGORY_VALUE)));
					ondChargeTO.setChargeCode(BeanUtils.nullHandler(rs.getString(CHARGE_CODE)));
					ondChargeTO.setChargeDescription(BeanUtils.nullHandler(rs.getString(CHARGE_DESCRIPTION)));
					ondChargeTO.setChargeGroupCode(BeanUtils.nullHandler(rs.getString(CHARGE_GROUP_CODE)));
					ondChargeTO.setRefundableChargeFlag(rs.getInt(REFUNDABLE_CHARGE_FLAG));
					List<OndChargeRateTO> chargeRates1 = new ArrayList<OndChargeRateTO>();

					OndChargeRateTO ondChargeRateTO1 = new OndChargeRateTO();
					ondChargeRateTO1.setChargeEffectiveFromDate(rs.getDate(EFFECTIVE_FROM));
					ondChargeRateTO1.setChargeEffectiveToDate(rs.getDate(EFFECTIVE_TO));
					ondChargeRateTO1.setChargeRateCode(rs.getInt(CHARGE_RATE_ID));
					ondChargeRateTO1.setChargeValuePercentage(new Double(rs.getDouble(VALUE_PERCENTAGE)));
					ondChargeRateTO1.setValuePercentageFlag(BeanUtils.nullHandler(rs.getString(CHARGE_BASIS)));
					chargeRates1.add(ondChargeRateTO1);
					ondChargeTO.setChargeRates(chargeRates1);
					existingCharges.add(ondChargeTO);
					originDestinationDTO.setCharges(existingCharges);
					list.add(originDestinationDTO);
					continue;
				}
			}
			// new ond
			else {

				originDestinationDTO = new OriginDestinationTO();
				originDestinationDTO.setOndCode(rs.getString(OND_CODE) != null ? this.ondCode : BeanUtils.nullHandler(rs
						.getString(OND_CODE)));
				List<OndChargeTO> charges = new ArrayList<OndChargeTO>();

				ondChargeTO = new OndChargeTO();
				ondChargeTO.setAppliesTo(rs.getInt(APPLIES_TO));
				ondChargeTO.setChargeCategoryCode(BeanUtils.nullHandler(rs.getString(CHARGE_CATEGORY_CODE)));
				ondChargeTO.setChargeCategoryValue(BeanUtils.nullHandler(rs.getString(CHARGE_CATEGORY_VALUE)));
				ondChargeTO.setChargeCode(rs.getString(CHARGE_CODE));
				ondChargeTO.setChargeDescription(rs.getString(CHARGE_DESCRIPTION));
				ondChargeTO.setChargeGroupCode(rs.getString(CHARGE_GROUP_CODE));
				ondChargeTO.setRefundableChargeFlag(rs.getInt(REFUNDABLE_CHARGE_FLAG));

				List<OndChargeRateTO> chargeRates = new ArrayList<OndChargeRateTO>();

				ondChargeRateTO = new OndChargeRateTO();
				ondChargeRateTO.setChargeEffectiveFromDate(rs.getDate(EFFECTIVE_FROM));
				ondChargeRateTO.setChargeEffectiveToDate(rs.getDate(EFFECTIVE_TO));
				ondChargeRateTO.setChargeRateCode(rs.getInt(CHARGE_RATE_ID));
				ondChargeRateTO.setChargeValuePercentage(new Double(rs.getDouble(VALUE_PERCENTAGE)));
				ondChargeRateTO.setValuePercentageFlag(rs.getString(CHARGE_BASIS));

				chargeRates.add(ondChargeRateTO);
				ondChargeTO.setChargeRates(chargeRates);
				charges.add(ondChargeTO);

				originDestinationDTO.setCharges(charges);
				list.add(originDestinationDTO);
				continue;

			}

		}// end of while

		// if ond was selected but does not have charges but only global charges, the global charges will be shown with
		// that ondcode.
		if (list.size() == 1) {
			OriginDestinationTO destinationTO = (OriginDestinationTO) list.get(0);
			if (destinationTO.getOndCode().equals("")) {
				destinationTO.setOndCode(ondCode);
				list.clear();
				list.add(destinationTO);
			}
		}
		return list;
	}
}