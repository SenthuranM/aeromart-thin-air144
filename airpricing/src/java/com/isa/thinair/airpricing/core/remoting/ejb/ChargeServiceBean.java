/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:29:42
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airpricing.core.remoting.ejb;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.ejb.TransactionTimeout;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airinventory.api.dto.seatavailability.FareSummaryDTO;
import com.isa.thinair.airinventory.api.dto.seatmap.SeatDTO;
import com.isa.thinair.airmaster.api.model.Baggage;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.model.Meal;
import com.isa.thinair.airmaster.core.audit.AuditAirMaster;
import com.isa.thinair.airpricing.api.criteria.BaggageTemplateSearchCriteria;
import com.isa.thinair.airpricing.api.criteria.ChargeSearchCriteria;
import com.isa.thinair.airpricing.api.criteria.ChargeTemplateSearchCriteria;
import com.isa.thinair.airpricing.api.criteria.MealTemplateSearchCriteria;
import com.isa.thinair.airpricing.api.criteria.ONDChargeSearchCriteria;
import com.isa.thinair.airpricing.api.criteria.ONDCodeSearchCriteria;
import com.isa.thinair.airpricing.api.criteria.SSRChargesSearchCriteria;
import com.isa.thinair.airpricing.api.dto.BaggageTemplateDTO;
import com.isa.thinair.airpricing.api.dto.BaggageTemplateTo;
import com.isa.thinair.airpricing.api.dto.CCChargeTO;
import com.isa.thinair.airpricing.api.dto.MealTemplateDTO;
import com.isa.thinair.airpricing.api.dto.ONDBaggageTemplateDTO;
import com.isa.thinair.airpricing.api.dto.QuoteChargesCriteria;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airpricing.api.dto.QuotedONDChargesDTO;
import com.isa.thinair.airpricing.api.dto.SSRChargeDTO;
import com.isa.thinair.airpricing.api.dto.SSRChargeSearchDTO;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForNonTicketingRevenueRQ;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForNonTicketingRevenueRS;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForTicketingRevenueRQ;
import com.isa.thinair.airpricing.api.dto.ServiceTaxQuoteForTicketingRevenueRS;
import com.isa.thinair.airpricing.api.model.BaggageCharge;
import com.isa.thinair.airpricing.api.model.BaggageTemplate;
import com.isa.thinair.airpricing.api.model.Charge;
import com.isa.thinair.airpricing.api.model.ChargeTemplate;
import com.isa.thinair.airpricing.api.model.MCO;
import com.isa.thinair.airpricing.api.model.MCOServices;
import com.isa.thinair.airpricing.api.model.MealCharge;
import com.isa.thinair.airpricing.api.model.MealTemplate;
import com.isa.thinair.airpricing.api.model.ONDBaggageChargeTemplate;
import com.isa.thinair.airpricing.api.model.ONDBaggageTemplate;
import com.isa.thinair.airpricing.api.model.PaymentGatewayWiseCharges;
import com.isa.thinair.airpricing.api.model.ReportChargeGroup;
import com.isa.thinair.airpricing.api.model.SSRCharge;
import com.isa.thinair.airpricing.api.model.SeatCharge;
import com.isa.thinair.airpricing.api.service.AirpricingUtils;
import com.isa.thinair.airpricing.api.service.ChargeBD;
import com.isa.thinair.airpricing.api.util.BaggageUtil;
import com.isa.thinair.airpricing.core.audit.AuditAirpricing;
import com.isa.thinair.airpricing.core.bl.BaggageBl;
import com.isa.thinair.airpricing.core.bl.BaggageBusinessLayer;
import com.isa.thinair.airpricing.core.bl.ChargeBL;
import com.isa.thinair.airpricing.core.bl.IBaggageBl;
import com.isa.thinair.airpricing.core.bl.MealBL;
import com.isa.thinair.airpricing.core.bl.ONDBaggageBL;
import com.isa.thinair.airpricing.core.bl.ONDBaggageChargeTemplateBL;
import com.isa.thinair.airpricing.core.bl.SSRChargeBL;
import com.isa.thinair.airpricing.core.persistence.dao.ChargeDAO;
import com.isa.thinair.airpricing.core.persistence.dao.ChargeJdbcDAO;
import com.isa.thinair.airpricing.core.persistence.dao.ONDChargeJdbcDAO;
import com.isa.thinair.airpricing.core.persistence.dao.ONDDAO;
import com.isa.thinair.airpricing.core.persistence.dao.PaymentGatewayWiseChargesDAO;
import com.isa.thinair.airpricing.core.service.bd.ChargeBDImpl;
import com.isa.thinair.airpricing.core.service.bd.ChargeBDLocalImpl;
import com.isa.thinair.airpricing.core.util.InternalConstants;
import com.isa.thinair.airreservation.api.dto.PaymentReferenceTO;
import com.isa.thinair.airtravelagents.api.util.AirTravelAgentConstants;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.dto.PayCurrencyDTO;
import com.isa.thinair.commons.api.dto.SSRChargesDTO;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.MessagesUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants.ProductType;

/**
 * 
 * @author Byorn
 */
@Stateless
@RemoteBinding(jndiBinding = "ChargeService.remote")
@LocalBinding(jndiBinding = "ChargeService.local")
@RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class ChargeServiceBean extends PlatformBaseSessionBean implements ChargeBDImpl, ChargeBDLocalImpl {

	private static ChargeBL chargeBL = null;
	private final Log log = LogFactory.getLog(getClass());

	/**
	 * @param onds
	 * @param posAirport
	 * @param depatureDate
	 * @return
	 * @throws ModuleException
	 */
	@Override
	public HashMap<String, HashMap<String, QuotedChargeDTO>> quoteCharges(Collection<QuoteChargesCriteria> criteriaCollection)
			throws ModuleException {
		try {
			return getChargeJdbcDAO().quoteCharges(criteriaCollection);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	/**
	 * rebuild the quoted charges from charge rate ids. Used to rebuild the OndFareDTOs
	 * 
	 * @throws ModuleException
	 */
	@Override
	public HashMap<String, QuotedChargeDTO> rebuildQuoteCharges(Collection<Integer> chargeRateIds, QuoteChargesCriteria criteria)
			throws ModuleException {

		try {
			return getChargeJdbcDAO().rebuildQuoteCharges(chargeRateIds, criteria);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}

	}

	/**
	 * 
	 * @param criteriaCollection
	 * @return
	 * @throws ModuleException
	 */
	@Override
	public QuotedONDChargesDTO quoteONDCharges(Collection<QuoteChargesCriteria> criteriaCollection) throws ModuleException {
		try {
			return getChargeJdbcDAO().quoteONDCharges(criteriaCollection);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	/**
	 * Page
	 * 
	 * @param chargeSearchCriteria
	 * @param startindex
	 * @param pagesize
	 * @param orderbyList
	 * @return
	 * @throws ModuleException
	 */
	@Override
	public Page getCharges(ChargeSearchCriteria chargeSearchCriteria, int startindex, int pagesize, List<String> orderbyList)
			throws ModuleException {
		try {
			return getChargeDAO().getCharges(chargeSearchCriteria, startindex, pagesize, orderbyList);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	public Charge getCharge(String chargeCode) throws ModuleException {
		try {
			return getChargeDAO().getCharge(chargeCode);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	/**
	 * void
	 * 
	 * @param charge
	 * @throws ModuleException
	 */
	@Override
	public void createCharge(Charge charge) throws ModuleException {
		try {
			AuditAirpricing.doAudit(charge, getUserPrincipal());
			new ChargeBL().createCharge(charge);
		} catch (CommonsDataAccessException accessException) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(accessException.getExceptionCode(), "airpricing.desc");
		} catch (ModuleException me) {
			this.sessionContext.setRollbackOnly();
			throw me;
		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, "airpricing.desc");
		}

	}

	/**
	 * void
	 * 
	 * @param charge
	 * @throws ModuleException
	 */
	@Override
	public void updateCharge(Charge charge) throws ModuleException {
		try {
			AuditAirpricing.doAudit(charge, getUserPrincipal());
			new ChargeBL().createCharge(charge);
		} catch (CommonsDataAccessException accessException) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(accessException.getExceptionCode(), "airpricing.desc");
		} catch (ModuleException me) {
			this.sessionContext.setRollbackOnly();
			throw me;
		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, "airpricing.technical.error");
		}
	}

	@Override
	public Page getONDCharges(ONDChargeSearchCriteria criteria, int startIndex, int pageSize, List<String> orderbyList)
			throws ModuleException {
		try {
			return getONDChargeJdbcDAO().getONDCharges(criteria, startIndex, pageSize, orderbyList);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	public Collection<Charge> getGlobalCharges() throws ModuleException {
		try {
			return getChargeDAO().getGlobalCharges();

		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	public HashMap<String, QuotedChargeDTO> quoteUniqueChargeForGroups(Collection<String> chargeGroups) throws ModuleException {

		try {
			return getChargeJdbcDAO().quoteUniqueChargeForGroups(chargeGroups);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}

	}

	private ONDChargeJdbcDAO getONDChargeJdbcDAO() {
		return (ONDChargeJdbcDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.OND_CHARGE_JDBC_DAO);
	}

	private ChargeDAO getChargeDAO() {
		ChargeDAO dao = null;
		try {
			dao = (ChargeDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.CHARGE_DAO);
		} catch (Exception ex) {
			System.out.println("ERROR IN LOOKUP");
		}
		return dao;
	}

	private PaymentGatewayWiseChargesDAO getPaymentGatewayWiseChargesDAO() {
		PaymentGatewayWiseChargesDAO dao = null;
		try {
			dao = (PaymentGatewayWiseChargesDAO) AirpricingUtils.getInstance().getLocalBean(
					InternalConstants.DAOProxyNames.PAYMENTGATEWAY_WISE_CHARGE_DAO);
		} catch (Exception ex) {
			System.out.println("ERROR IN LOOKUP");
		}
		return dao;
	}

	private ChargeJdbcDAO getChargeJdbcDAO() {
		return (ChargeJdbcDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.CHARGE_JDBC_DAO);
	}

	private ONDDAO getOndDao() {
		return (ONDDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.OND_DAO);
	}

	/**
	 * void
	 * 
	 * @param chargeCode
	 * @throws ModuleException
	 */
	@Override
	public void deleteCharge(String chargeCode) throws ModuleException {
		try {
			AuditAirpricing.doAudit(chargeCode, AuditAirpricing.CHARGE, getUserPrincipal());
			getChargeDAO().removeCharge(chargeCode);
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	/**
	 * void
	 * 
	 * @param charge
	 * @throws ModuleException
	 */
	@Override
	public void deleteCharge(Charge charge) throws ModuleException {
		try {
			AuditAirpricing.doAudit(charge.getChargeCode(), AuditAirpricing.CHARGE, getUserPrincipal());
			getChargeDAO().deleteCharge(charge);
		}

		catch (CommonsDataAccessException accessException) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(accessException.getExceptionCode(), "airpricing.desc");
		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, "airpricing.desc");
		}

	}

	/**
	 * void
	 * 
	 * @param charge
	 * @throws ModuleException
	 */
	@Override
	public void deleteCharge(String chargeCode, long version) throws ModuleException {
		try {
			AuditAirpricing.doAudit(chargeCode, AuditAirpricing.CHARGE, getUserPrincipal());
			getChargeDAO().removeCharge(chargeCode, version);
		}

		catch (CommonsDataAccessException accessException) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(accessException.getExceptionCode(), "airpricing.desc");
		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, "airpricing.desc");
		}

	}

	/**
	 * Return charge groups codes
	 * 
	 * @throws ModuleException
	 */
	@Override
	public Map<String, String> getChargeGroups() throws ModuleException {
		try {
			return getChargeJdbcDAO().getChargeGroups();
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	/**
	 * Return Active charge with rate applicable for the specified effective date.
	 * 
	 * @param chargeCode
	 * @param effectiveDate
	 * @param fareSummaryDTO
	 *            basis for charge value when charge is specified as a percentage
	 * @return
	 * @throws ModuleException
	 */
	@Override
	public QuotedChargeDTO getCharge(String chargeCode, Date effectiveDate, FareSummaryDTO fareSummaryDTO,
			Integer operationalMode, boolean checkSalesValidity) throws ModuleException {
		try {
			return getChargeJdbcDAO().getCharge(chargeCode, effectiveDate, fareSummaryDTO, operationalMode, checkSalesValidity);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	public List<QuotedChargeDTO> getCharges(String chargeCode, Date effectiveDate, FareSummaryDTO fareSummaryDTO,
			Integer operationalMode) throws ModuleException {
		try {
			return getChargeJdbcDAO().getCharges(chargeCode, effectiveDate, fareSummaryDTO, operationalMode);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	/**
	 * Get ChargeTemplate
	 * 
	 * @param templateId
	 * @return ChargeTemplate
	 */
	@Override
	public ChargeTemplate getChargeTemplate(int templateId) throws ModuleException {
		try {
			return getChargeDAO().getChargeTemplate(templateId);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	/**
	 * Get ChargeTemplates for criteria
	 * 
	 * @param criteria
	 * @param startRec
	 * @param noRecs
	 * @return
	 * @throws ModuleException
	 */
	@Override
	public Page<ChargeTemplate> getChargeTemplates(ChargeTemplateSearchCriteria criteria, int startRec, int noRecs)
			throws ModuleException {
		try {
			return getChargeDAO().getChargeTemplate(criteria, startRec, noRecs);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	public Page<ChargeTemplate> getSeatCharges(ChargeTemplateSearchCriteria criteria, int startRec, int noRecs)
			throws ModuleException {
		try {
			return getChargeDAO().getSeatCharges(criteria, startRec, noRecs);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void saveSeatChargeTemplate(ChargeTemplate chargeTemplate, Collection<SeatDTO> seatChargeDTOs, boolean isDefAmountChanged) throws ModuleException {
		try {
			chargeTemplate = (ChargeTemplate) setUserDetails(chargeTemplate);
			AuditAirpricing.doAuditSeatMapAddOrModifyTemplate(chargeTemplate, getUserPrincipal());
			
			getChargeBL().saveSeatChargeTemplate(chargeTemplate, seatChargeDTOs,isDefAmountChanged, getUserPrincipal());
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	/**
	 * Save Charge Template
	 * 
	 * @param chargeTemplate
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void saveTemplate(ChargeTemplate chargeTemplate, Integer flitghtSegID, String modelNo) throws ModuleException {
		try {
			chargeTemplate = (ChargeTemplate) setUserDetails(chargeTemplate);
			AuditAirpricing.doAuditSeatMapAddOrModifyTemplate(chargeTemplate, getUserPrincipal());
			
			getChargeBL().saveTemplate(chargeTemplate, flitghtSegID, modelNo);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	public ChargeTemplate getTemplateId(String templateCode) {
		return getChargeBL().getTemplateId(templateCode);
	}

	/**
	 * Save Charge Template
	 * 
	 * @param chargeTemplate
	 */
	@Override
	public Collection<ChargeTemplate> getChargeTemplates(ChargeTemplateSearchCriteria criteria) throws ModuleException {
		try {
			return getChargeDAO().getChargeTemplates(criteria);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	/**
	 * 
	 * @param templateId
	 * @throws ModuleException
	 */
	@Override
	public void deleteTemplate(int ichargeTemplate) throws ModuleException {
		try {
			ChargeTemplate chargeTemplate = getChargeDAO().getChargeTemplate(ichargeTemplate);
			AuditAirpricing.doAuditSeatMapDeleteTemplate(chargeTemplate.getTemplateCode(), getUserPrincipal());
			getChargeDAO().deleteTemplate(chargeTemplate);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	/**
	 * 
	 * @param colAMSChaegeIDs
	 * @return
	 * @throws ModuleException
	 */
	@Override
	public Collection<SeatCharge> getSeatCharges(Collection<Integer> colAMSChaegeIDs) throws ModuleException {
		try {
			return getChargeDAO().getSeatCharges(colAMSChaegeIDs);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	/**
	 * 
	 * @param srcTemplateID
	 * @param newTemplateCode
	 * @param seatCharges
	 * @return
	 * @throws ModuleException
	 */
	@Override
	public ChargeTemplate copyChargeTemplate(int srcTemplateID, String newTemplateCode, Collection<SeatCharge> seatCharges)
			throws ModuleException {
		try {
			return getChargeBL().copyChargeTemplate(srcTemplateID, newTemplateCode, seatCharges);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	private ChargeBL getChargeBL() {
		if (chargeBL == null) {
			chargeBL = new ChargeBL();
		}
		return chargeBL;
	}

	@Override
	public Page<MealTemplate> getMealTemplates(MealTemplateSearchCriteria criteria, int startRec, int noRecs)
			throws ModuleException {
		try {
			return getChargeDAO().getMealTemplates(criteria, startRec, noRecs);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@TransactionTimeout(10000)
	public void saveMealTemplate(MealTemplateDTO mealTmpDto, Integer newChargeId) throws ModuleException {
		try {
			MealBL mealBL = new MealBL();
			MealTemplate mealTemplate = mealBL.createTemplate(mealTmpDto);
			saveMealTemplate(mealTemplate, newChargeId);

		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void saveMealTemplate(MealTemplate mealTemplate, Integer newChargeId) throws ModuleException {
		try {
			mealTemplate = (MealTemplate) setUserDetails(mealTemplate);
			AuditAirpricing.doAuditMealAddOrModifyTemplate(mealTemplate, getUserPrincipal());
			new MealBL().saveTemplate(mealTemplate, newChargeId);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	public void deleteMealTemplate(MealTemplateDTO mealTmpDTO) throws ModuleException {
		try {
			MealBL mealBL = new MealBL();
			MealTemplate mealTemplate = mealBL.createTemplate(mealTmpDTO);
			AuditAirpricing.doAuditMealDeleteTemplate(mealTemplate.getTemplateCode(), getUserPrincipal());
			getChargeDAO().deleteMealTemplate(mealTemplate);
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	public Collection<MealTemplate> getMealTemplates(Integer mealId) throws ModuleException {
		try {
			return getChargeDAO().getMealTemplates(mealId);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	public MealTemplate getMealTemplate(int templateID) throws ModuleException {
		try {
			return getChargeDAO().getMealTemplate(templateID);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	public Collection<MealCharge> getMealCharges(Collection<Integer> soldMeals) throws ModuleException {
		try {
			return getChargeDAO().getMealCharges(soldMeals);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	/**
	 * Re-calculate charges, when conversion rates changed.
	 * 
	 * @param toDate
	 * @throws ModuleException
	 */
	@Override
	public void reCalcChargesInLocalCurr(Date date) throws ModuleException {

		ChargeBD chargeBD = AirpricingUtils.getChargeBD();
		Collection<CurrencyExchangeRate> currExchangeList = null;

		try {

			currExchangeList = AirpricingUtils.getCommonMasterBD().getAllUpdatedCurrForCharges(date);
			new ChargeBL().reCalcChargesInLocalCurr(currExchangeList);
			chargeBD.updateStatusForChargesInLocalCurr(currExchangeList, CommonsConstants.CreditLimitUpdateStatus.SUCCESS.code());

		} catch (ModuleException ex) {
			log.error("ModuleException :: reCalcChargesInLocalCurr ", ex);
			chargeBD.updateStatusForChargesInLocalCurr(currExchangeList, CommonsConstants.CreditLimitUpdateStatus.FAILED.code());

			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error("CommonsDataAccessException :: reCalcChargesInLocalCurr ", cdaex);
			chargeBD.updateStatusForChargesInLocalCurr(currExchangeList, CommonsConstants.CreditLimitUpdateStatus.FAILED.code());

			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void updateStatusForChargesInLocalCurr(Collection<CurrencyExchangeRate> currExchangeList, String status)
			throws ModuleException {
		try {
			new ChargeBL().updateStatusForChargesInLocalCurr(currExchangeList, status);
		} catch (ModuleException ex) {
			log.error(" ((o)) ModuleException::updateStatusForChargesInLocalCurr ", ex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(ex, ex.getExceptionCode(), ex.getModuleCode());
		} catch (CommonsDataAccessException cdaex) {
			log.error(" ((o)) CommonsDataAccessException::updateStatusForChargesInLocalCurr ", cdaex);
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		}
	}

	@Override
	public HashMap<String, Collection<SSRChargeDTO>> getSSRCharges(SSRChargeSearchDTO ssrChargeSearchDTO) throws ModuleException {
		HashMap<String, Collection<SSRChargeDTO>> ssrCharges = SSRChargeBL.getSSRCharges(ssrChargeSearchDTO);
		return ssrCharges;
	}

	@Override
	public Collection<BaggageTemplate> getBaggageTemplatesCollect(Integer baggageId) throws ModuleException {
		try {
			return getChargeDAO().getBaggageTemplatesCollect(baggageId);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	public BaggageTemplate getBaggageTemplate(int templateID) throws ModuleException {
		try {
			return getChargeDAO().getBaggageTemplate(templateID);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	public Collection<BaggageCharge> getBaggageCharges(Collection<Integer> soldBaggages) throws ModuleException {
		try {
			return getChargeDAO().getBaggageCharges(soldBaggages);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	/**
	 * Save newOnds
	 * 
	 * @param newOnds
	 */
	@Override
	public void saveONDs(Set<String> newOnds) throws ModuleException {
		try {
			getChargeBL().saveNewONDs(newOnds);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	public Page<BaggageTemplate> getBaggageTemplates(BaggageTemplateSearchCriteria criteria, int startRec, int noRecs)
			throws ModuleException {
		try {
			return getChargeDAO().getBaggageTemplates(criteria, startRec, noRecs);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	public boolean isONDsExists(Set<String> ondList) throws ModuleException {

		try {
			if (ondList != null && ondList.size() > 0) {
				for (String ond : ondList) {
					if (getOndDao().getOND(ond) == null) {
						return false;
					}
				}
			}
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}

		return true;
	}

	@Override
	public Page getONDBaggageTemplates(BaggageTemplateSearchCriteria criteria, int startRec, int noRecs) throws ModuleException {
		try {
			return getChargeDAO().getONDBaggageTemplates(criteria, startRec, noRecs);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	public ONDBaggageTemplate getONDBaggageTemplate(int templateId) throws ModuleException {
		try {
			return getChargeDAO().getONDBaggageTemplate(templateId);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	public Page getONDCodes(ONDCodeSearchCriteria criteria, int startRec, int noRecs) throws ModuleException {
		try {
			return getChargeDAO().getONDCodes(criteria, startRec, noRecs);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	public Page searchBaggageTemplates(ONDCodeSearchCriteria criteria, int startRec, int noRecs) throws ModuleException {
		try {
			return getChargeDAO().searchBaggageTemplates(criteria, startRec, noRecs);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	/* SSR Charges */
	@Override
	public Page<SSRCharge> getSSRCharges(SSRChargesSearchCriteria criteria, int startRec, int noRecs) throws ModuleException {
		try {
			return getChargeDAO().getSSRCharges(criteria, startRec, noRecs);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void saveBaggageTemplate(BaggageTemplateDTO baggageTemplateDTO, Integer newChargeId) throws ModuleException {
		try {
			BaggageBusinessLayer bgBL = new BaggageBusinessLayer();
			BaggageTemplate baggageTemplate = bgBL.createTemplate(baggageTemplateDTO);
			saveBaggageTemplate(baggageTemplate, newChargeId);

		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	public void saveBaggageTemplate(BaggageTemplate baggageTemplate, Integer newChargeId) throws ModuleException {
		try {
			baggageTemplate = (BaggageTemplate) setUserDetails(baggageTemplate);
			AuditAirpricing.doAuditBaggageAddOrModifyTemplate(baggageTemplate, getUserPrincipal());
			new BaggageBusinessLayer().saveTemplate(baggageTemplate, newChargeId);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void saveONDBaggageTemplate(ONDBaggageTemplateDTO baggageTemplateDTO, Integer newChargeId) throws ModuleException {
		try {
			ONDBaggageBL oNDBaggageBL = new ONDBaggageBL();
			ONDBaggageTemplate baggageTemplate = oNDBaggageBL.createTemplate(baggageTemplateDTO);
			baggageTemplate = (ONDBaggageTemplate) setUserDetails(baggageTemplate);
			
			AuditAirpricing.doAuditONDBaggageAddOrModifyTemplate(baggageTemplate, getUserPrincipal());
			if (baggageTemplate.getVersion() >= 0) {
				IBaggageBl baggageBl = new BaggageBl();
				if (Baggage.STATUS_ACTIVE.equalsIgnoreCase(baggageTemplate.getStatus())) {
					baggageBl.validateBaggageRules(baggageTemplate);
				}
			} 
			oNDBaggageBL.saveTemplate(baggageTemplate, newChargeId);
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean isActiveONDCodeExistForBaggageTemplate(int templateId, Date fromDate, Date toDate) throws ModuleException {
		try {
			return getChargeDAO().isActiveONDCodeExistForBaggageTemplate(templateId, fromDate, toDate);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void saveONDCodes(ONDBaggageChargeTemplate baggageChargeTemplate, Integer newChargeId) throws ModuleException {
		try {

			if (baggageChargeTemplate.getVersion() < 0) {

				AuditAirpricing.doAuditONDCodeAdd(baggageChargeTemplate.getOndBaggageTemplate().getTemplateCode(),
						baggageChargeTemplate.getOndCode(), getUserPrincipal());
			} else {

				AuditAirpricing.doAuditONDCodeModify(baggageChargeTemplate.getOndBaggageTemplate().getTemplateCode(),
						baggageChargeTemplate.getOndCode(), getUserPrincipal());
			}

			ChargeBD chargeBD = AirpricingUtils.getChargeBD();
			if (chargeBD.isONDCodeExist(baggageChargeTemplate.getOndCode(), baggageChargeTemplate.getOndBaggageTemplate()
					.getTemplateId())) {
				this.sessionContext.setRollbackOnly();
				throw new ModuleException("module.duplicate.key", MessagesUtil.getMessage("module.duplicate.key"));
			}

			new ONDBaggageChargeTemplateBL().saveTemplate(baggageChargeTemplate, newChargeId);
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@TransactionTimeout(1200)
	public void saveBaggageTemplate(BaggageTemplateTo baggageTemplateTo) throws ModuleException {

		ONDBaggageTemplate baggageTemplate = null;
		if (baggageTemplateTo.getTemplateId() != null) {
			baggageTemplate = getChargeDAO().getONDBaggageTemplate(baggageTemplateTo.getTemplateId());
		} else {
			baggageTemplate = new ONDBaggageTemplate();
		}

		BaggageUtil.mergeToBaggageTemplate(baggageTemplateTo, baggageTemplate);

		IBaggageBl baggageBl = new BaggageBl();
		try {
			baggageBl.validateBaggageRules(baggageTemplate);
			baggageBl.saveBaggage(baggageTemplate);
		} catch (ModuleException e) {
			sessionContext.setRollbackOnly();
			throw e;
		}

	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean isONDCodeExist(String ondCode, int templateId) throws ModuleException {
		try {
			return getChargeDAO().isONDCodeExist(ondCode, templateId);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	public void saveSSRCharges(SSRChargesDTO ssrChargesDTO, String cos) throws ModuleException {

		SSRCharge ssrCharge = null;
		if (ssrChargesDTO.getChargeId() != null && ssrChargesDTO.getChargeId() > 0)
			ssrCharge = getSSRCharges(ssrChargesDTO.getChargeId());

		ssrCharge = SSRChargeBL.getSSRCharge(ssrCharge, ssrChargesDTO, cos);
		try {
			getChargeDAO().saveSSRCharges(ssrCharge);
			AuditAirpricing.doAuditAddModifySSRCharge(ssrCharge, getUserPrincipal());
			
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	public SSRCharge getSSRCharges(int chargeId) throws ModuleException {
		try {
			return getChargeDAO().getSSRCharges(chargeId);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	public SSRCharge getSSRChargesBySSRId(int ssrId) throws ModuleException {
		try {
			return getChargeDAO().getSSRChargesBySSRId(ssrId);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	public void deleteSSRCharges(SSRCharge ssrCharge) throws ModuleException {
		try {
			AuditAirpricing.doAuditDeleteSSRCharge(ssrCharge.getSsrChargeCode(), getUserPrincipal());
			getChargeDAO().deleteSSRCharges(ssrCharge);
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	public void deleteBaggageTemplate(BaggageTemplateDTO baggageTemplateDTO) throws ModuleException {
		try {
			BaggageBusinessLayer bgBL = new BaggageBusinessLayer();
			BaggageTemplate baggageTemplate = bgBL.createTemplate(baggageTemplateDTO);
			AuditAirpricing.doAuditBaggageDeleteTemplate(baggageTemplate.getTemplateCode(), getUserPrincipal());
			getChargeDAO().deleteBaggageTemplate(baggageTemplate);
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	// ONDBaggageTemplateDTO
			public
			void deleteONDBaggageTemplate(ONDBaggageTemplateDTO baggageTemplateDTO) throws ModuleException {
		try {
			ONDBaggageBL oNDBaggageBL = new ONDBaggageBL();
			ONDBaggageTemplate baggageTemplate = oNDBaggageBL.createTemplate(baggageTemplateDTO);

			AuditAirpricing.doAuditONDBaggageDeleteTemplate(baggageTemplate.getTemplateCode(), getUserPrincipal());
			getChargeDAO().deleteONDBaggageTemplate(baggageTemplate);
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	public void deleteONDCodes(ONDBaggageChargeTemplate baggageChargeTemplate) throws ModuleException {
		try {

			/*
			 * AuditAirpricing.doAuditONDCodesDelete(baggageChargeTemplate.getOndBaggageTemplate().getTemplateCode(),
			 * baggageChargeTemplate.getOndCodes().getOndCode(), getUserPrincipal());
			 */
			AuditAirpricing.doAuditONDCodesDelete(baggageChargeTemplate.getOndBaggageTemplate().getTemplateCode(),
					baggageChargeTemplate.getOndCode(), getUserPrincipal());
			getChargeDAO().deleteONDCodes(baggageChargeTemplate);
		} catch (CommonsDataAccessException e) {
			this.sessionContext.setRollbackOnly();
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	public boolean checkSSRChargeAlreadyDefined(int ssrId, String ssrChargeCode, int chargeId, ArrayList<String> ccList,
			ArrayList<String> lccList) throws ModuleException {
		try {
			return getChargeDAO().checkSSRChargeAlreadyDefined(ssrId, ssrChargeCode, chargeId, ccList, lccList);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	public String getReportingChargeGroupCodeByChargeRateID(Integer chargeRateId) throws ModuleException {
		try {
			return getChargeJdbcDAO().getReportingChargeGroupCodeByChargeRateID(chargeRateId);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	public void saveReportChargeGroup(ReportChargeGroup reportChargeGroup) throws ModuleException {
		try {
			getChargeDAO().saveReportChargeGroup(reportChargeGroup);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	public void deleteReportChargeGroup(ReportChargeGroup reportChargeGroup) throws ModuleException {
		try {
			getChargeDAO().deleteReportChargeGroup(reportChargeGroup);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	public ReportChargeGroup getReportChargeGroupByRptChargeGroupCode(String reportChargeGroupCode) throws ModuleException {
		try {
			return getChargeDAO().getReportChargeGroupByRptChargeGroupCode(reportChargeGroupCode);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void chargeUpdateOnExchangeRateChange() throws ModuleException {
		try {
			new ChargeBL().chargeUpdateOnExchangeRateChange(getUserPrincipal());
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<Charge> getExcludableCharges() throws ModuleException {
		try {
			return getChargeDAO().getExcludableCharges();
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean isValidChargeExclusion(List<String> excludingCharges) throws ModuleException {
		try {
			return getChargeBL().isValidChargeExclusion(excludingCharges);
		} catch (ModuleException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	public boolean isValidChargeExists(String chargeCode, String ondCode) throws ModuleException {
		try {
			return getChargeJdbcDAO().isValidChargeExists(chargeCode, PlatformUtiltiies.nullHandler(ondCode));
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	public Map<Integer, Map<CommonsConstants.BaggageCriterion, List<String>>> getOverlappedTemplates(
			ONDBaggageTemplate baggageTemplate) {
		return getChargeJdbcDAO().getOverlappedTemplates(baggageTemplate);

	}

	@Override
	public PaymentGatewayWiseCharges getPaymentGatewayWiseCharge(int paymentGatewayId) {
		return getPaymentGatewayWiseChargesDAO().getPaymentGatewayWiseCharges(paymentGatewayId);
	}

	@Override
	public PaymentGatewayWiseCharges getUniquePaymentGatewayWiseCharge(int chargeId) {
		return getPaymentGatewayWiseChargesDAO().getUniquePaymentGatewayWiseCharge(chargeId);
	}

	@Override
	public Page<CCChargeTO> getPaymentGatewayWiseCharge(int paymentGatewayId, int i, List<ModuleCriterion> criterion) {
		return getPaymentGatewayWiseChargesDAO().getPaymentGatewayWiseCharges(paymentGatewayId, i, criterion);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void savePaymentGatewayWiseCharges(PaymentGatewayWiseCharges paymentGatewayWiseCharges) throws ModuleException {

		try {
			paymentGatewayWiseCharges = (PaymentGatewayWiseCharges) setUserDetails(paymentGatewayWiseCharges);
			if (paymentGatewayWiseCharges.getVersion() > 0) {
				PaymentGatewayWiseCharges oldPaymentGatewayWiseCharges = getUniquePaymentGatewayWiseCharge(paymentGatewayWiseCharges
						.getChargeId());
				paymentGatewayWiseCharges.setOldPaymentGatewayWiseCharges(oldPaymentGatewayWiseCharges);
			}
			getPaymentGatewayWiseChargesDAO().savePaymentGatewayWiseCharges(paymentGatewayWiseCharges);
			AuditAirMaster.doAudit(paymentGatewayWiseCharges, getUserPrincipal());
		} catch (Exception ex) {
			log.error("Retrieving parameters failed", ex);
			if (ex instanceof CommonsDataAccessException) {
				throw new ModuleException(ex, ((CommonsDataAccessException) ex).getExceptionCode(), "airpricing.desc");
			} else if (ex instanceof ModuleException) {
				throw (ModuleException) ex;
			} else {
				throw new ModuleException(ex, "module.genericexception", "airpricing.desc");
			}
		}

	}

	@Override
	public List<MCOServices> getMCOServices() throws ModuleException {
		try {
			return getChargeDAO().getMCOServices();
		} catch (ModuleException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	public void saveMCO(MCO mco) throws ModuleException {
		try {
			getChargeDAO().saveMCO(mco);
		} catch (ModuleException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}

	}

	@Override
	public String getPrintMCOText(String mcoNumber, String agentCode) throws ModuleException {
		try {
			return new ChargeBL().getPrintMCOText(mcoNumber, agentCode);
		} catch (ModuleException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	public String getNextMCOSQID() throws ModuleException {
		return new ChargeBL().getGeneratedMcoNumber();
	}
	
	@Override
	public void emailMCO(String mcoNumber, String agentCode) throws ModuleException {
		try {
			new ChargeBL().emailMCO(mcoNumber, agentCode);
		} catch (ModuleException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	public void issueMCO(MCO mco, PayCurrencyDTO payCurrencyDTO, PaymentReferenceTO paymentReferenceTO) throws ModuleException {
		try {
			saveMCO(mco);
			AirpricingUtils.lookupTravelAgentFinanceBD().recordExternalProductSale(getUserPrincipal().getAgentCode(),
					mco.getAmount(), mco.getMcoNumber(), null, payCurrencyDTO, paymentReferenceTO, ProductType.MCO, null);
		} catch (CommonsDataAccessException cdaex) {
			this.sessionContext.setRollbackOnly();
			log.error("recordDebit  failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			log.error("recordDebit  failed.", e);
			throw new ModuleException(e, AirTravelAgentConstants.MODULE_NAME);
		}
	}

	@Override
	public void voidMCO(MCO mco, PayCurrencyDTO payCurrencyDTO, PaymentReferenceTO paymentReferenceTO) throws ModuleException {
		try {
			saveMCO(mco);
			AirpricingUtils.lookupTravelAgentFinanceBD().recordExternalProductRefund(getUserPrincipal().getAgentCode(),
					mco.getAmount(), mco.getMcoNumber(), null, payCurrencyDTO, paymentReferenceTO, ProductType.MCO);
		} catch (CommonsDataAccessException cdaex) {
			this.sessionContext.setRollbackOnly();
			log.error("recordDebit  failed.", cdaex);
			throw new ModuleException(cdaex, cdaex.getExceptionCode(), cdaex.getModuleCode());
		} catch (Exception e) {
			this.sessionContext.setRollbackOnly();
			log.error("recordDebit  failed.", e);
			throw new ModuleException(e, AirTravelAgentConstants.MODULE_NAME);
		}
	}

	public MCO getMCO(String mcoNumber) throws ModuleException {
		return getChargeDAO().getMCO(mcoNumber);
	}
	
	@Override
	public HashMap<String, Object> getMCORecord(String mcoNumber, String agentCode) throws CommonsDataAccessException {
		return getChargeDAO().getMCORecord(mcoNumber, agentCode);
	}

	@Override
	public List<HashMap<String, Object>> getMCORecords(String mcoNumber, Integer pnrPaxID) throws ModuleException {
		try {
			return getChargeDAO().getMCORecords(mcoNumber, pnrPaxID);
		} catch (Exception ex) {
			throw new ModuleException(ex, ((CommonsDataAccessException) ex).getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	public boolean hasMCORecords(Integer pnrPaxID) throws ModuleException {
		try {
			return getChargeDAO().hasMCORecords(pnrPaxID);
		} catch (Exception ex) {
			throw new ModuleException(ex, ((CommonsDataAccessException) ex).getExceptionCode(), "airpricing.desc");
		}
	}
	
	@Override
	public ServiceTaxQuoteForTicketingRevenueRS quoteServiceTaxForTicketingRevenue(
			ServiceTaxQuoteForTicketingRevenueRQ serviceTaxQuoteForTktRevRQ) throws ModuleException {
		try {
			return getChargeBL().quoteServiceTaxForTicketingRevenue(serviceTaxQuoteForTktRevRQ);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}
	
	@Override
	public ServiceTaxQuoteForNonTicketingRevenueRS quoteServiceTaxForNonTicketingRevenue(
			ServiceTaxQuoteForNonTicketingRevenueRQ serviceTaxQuoteForNonTktRevRQ) throws ModuleException {
		try {
			return getChargeBL().quoteServiceTaxForNonTicketingRevenue(serviceTaxQuoteForNonTktRevRQ);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	public List<String> getAllActiveServiceTaxChargeCodes() throws ModuleException {
		try {
			return getChargeDAO().getAllActiveServiceTaxChargeCodes();
		} catch (Exception ex) {
			throw new ModuleException(ex, ((CommonsDataAccessException) ex).getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	public Charge getCharge(ChargeSearchCriteria chargeSearchCriteria) throws ModuleException {
		try {
			return getChargeDAO().getCharge(chargeSearchCriteria);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}
	
	@Override
	public Collection<Charge> getCharges(ChargeSearchCriteria chargeSearchCriteria) throws ModuleException {
		try {
			return getChargeDAO().getCharges(chargeSearchCriteria);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	public Map<String, Boolean> getChargeCodeWiseRefundability(List<String> chargeCodes) throws ModuleException {
		try {
			return getChargeDAO().getChargeRefundabilityMap(chargeCodes);
		} catch (Exception ex) {
			throw new ModuleException(ex, ((CommonsDataAccessException) ex).getExceptionCode(), "Charge code refundability map");
		}
	}
	
	@Override
	public Boolean isEnableLMSCreditBlock(int tempTransactionId) {
		return getChargeDAO().isEnableLMSCreditBlock(tempTransactionId);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void updateMealTemplates(Meal meal) throws ModuleException {		
		
		Integer mealID = meal.getMealId();
		Collection<MealTemplate> mealTemplates = getMealTemplates(mealID);
		Integer newMealChargeId = null;
		
		for (MealTemplate mealTemplate : mealTemplates) {
			
			Collection<MealCharge> mealCharges = mealTemplate.getMealCharges();
			Set<MealCharge> updatedCharge = new HashSet<MealCharge>();
			
			for (MealCharge mealCharge : mealCharges) {
				
				if (mealID.equals(mealCharge.getMealId())) {
					
					mealCharge.setStatus(meal.getStatus());
					newMealChargeId = mealCharge.getChargeId();
					
				}
				
				updatedCharge.add(mealCharge);
			}
			
			mealTemplate.setMealCharges(updatedCharge);
			saveMealTemplate(mealTemplate, newMealChargeId);
		}
		
		
	}
}
