package com.isa.thinair.airpricing.core.remoting.ejb;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.airpricing.api.criteria.FlexiRuleSearchCriteria;
import com.isa.thinair.airpricing.api.dto.FlexiQuoteCriteria;
import com.isa.thinair.airpricing.api.dto.FlexiRuleDTO;
import com.isa.thinair.airpricing.api.model.FlexiRule;
import com.isa.thinair.airpricing.api.service.AirpricingUtils;
import com.isa.thinair.airpricing.core.persistence.dao.FlexiRuleDAO;
import com.isa.thinair.airpricing.core.persistence.dao.FlexiRuleJdbcDAO;
import com.isa.thinair.airpricing.core.service.bd.AirPricingBDImpl;
import com.isa.thinair.airpricing.core.service.bd.AirPricingBDLocalImpl;
import com.isa.thinair.airpricing.core.util.InternalConstants;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;

/**
 * @author Dhanya
 */
@Stateless
@RemoteBinding(jndiBinding = "AirPricingService.remote")
@LocalBinding(jndiBinding = "AirPricingService.local")
@RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class AirPricingServiceBean extends PlatformBaseSessionBean implements AirPricingBDImpl, AirPricingBDLocalImpl {

	/**
	 * void
	 * 
	 * @param flexiRule
	 * @throws ModuleException
	 */
	public void createFlexiRule(FlexiRule flexiRule) throws ModuleException {
		try {
			getFlexiRuleDAO().saveFlexiRule(flexiRule);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	/**
	 * void
	 * 
	 * @param flexiRule
	 * @throws ModuleException
	 */
	public void updateFlexiRule(FlexiRule flexiRule) throws ModuleException {
		try {
			getFlexiRuleDAO().saveFlexiRule(flexiRule);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	/**
	 * void
	 * 
	 * @param flexiRuldId
	 * @throws ModuleException
	 */
	public void deleteFlexiRule(int flexiRuleId) throws ModuleException {
		try {
			getFlexiRuleDAO().removeFlexiRule(flexiRuleId);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	public Page getFlexiRules(FlexiRuleSearchCriteria flexiRuleSearchCriteria, int startIndex, int pageSize, List<String> orderbyfields)
			throws ModuleException {
		try {
			return getFlexiRuleDAO().getFlexiRules(flexiRuleSearchCriteria, startIndex, pageSize, orderbyfields);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public HashMap<String, HashMap<String, FlexiRuleDTO>>  quoteFlexiCharges(Collection<FlexiQuoteCriteria> colCriteria) throws ModuleException {
		try {
			HashMap<String, HashMap<String, FlexiRuleDTO>>  ondFlexiCharges = getFlexiRuleJdbcDAO().quoteFlexiCharges(colCriteria);
			return ondFlexiCharges;
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	@Override
	public HashMap<String, FlexiRuleDTO> rebuildFlexiCharges(Collection<Integer> flexiIdCollection, FlexiQuoteCriteria criteria) throws ModuleException {
		try {
			return getFlexiRuleJdbcDAO().rebuildFlexiCharges(flexiIdCollection, criteria);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	public List<FlexiRuleDTO> requoteFlexiCharges(Collection<Integer> flexiRuleRateIds) throws ModuleException {
		try {
			return getFlexiRuleJdbcDAO().requoteFlexiCharges(flexiRuleRateIds);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	private FlexiRuleDAO getFlexiRuleDAO() {
		FlexiRuleDAO dao = null;
		try {
			dao = (FlexiRuleDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FLEXI_RULE_DAO);
		} catch (Exception ex) {
			System.out.println("ERROR IN LOOKUP");
		}
		return dao;
	}

	private FlexiRuleJdbcDAO getFlexiRuleJdbcDAO() {
		return (FlexiRuleJdbcDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FLEXI_RULE_JDBC_DAO);
	}

}
