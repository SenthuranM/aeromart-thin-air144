/**
 * 
 */
package com.isa.thinair.airpricing.core.bl;

import com.isa.thinair.airpricing.api.model.ONDBaggageChargeTemplate;
import com.isa.thinair.airpricing.api.service.AirpricingUtils;
import com.isa.thinair.airpricing.core.persistence.dao.ChargeDAO;
import com.isa.thinair.airpricing.core.persistence.dao.ChargeJdbcDAO;
import com.isa.thinair.airpricing.core.util.InternalConstants;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author kasun
 *
 */
public class ONDBaggageChargeTemplateBL {

	private ChargeDAO chargedao;

	private ChargeJdbcDAO chargejdbcdao;

	public ONDBaggageChargeTemplateBL() {

		chargedao = (ChargeDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.CHARGE_DAO);

		chargejdbcdao = getChargeJdbcDAO();

	}

	public void saveTemplate(ONDBaggageChargeTemplate baggageChargeTemplate, Integer chargeid) throws ModuleException {
		try {

			chargedao.saveONDCodes(baggageChargeTemplate);

		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	private ChargeJdbcDAO getChargeJdbcDAO() {
		return (ChargeJdbcDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.CHARGE_JDBC_DAO);
	}

}
