package com.isa.thinair.airpricing.core.persistence.jdbc.ondchargejdbcdao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airpricing.api.criteria.ONDChargeSearchCriteria;

/**
 * 
 * @author Byorn Class for exctracting the Count of Agents for Master FareRules
 */
public class OndChargesCount {

	private DataSource ds;
	private String queryString;
	private ONDChargeSearchCriteria ondChargesSearchCriteria;

	/**
	 * Constructor
	 * 
	 * @param criteria
	 * @param sql
	 * @param ds
	 */
	public OndChargesCount(ONDChargeSearchCriteria criteria, String sql, DataSource ds) {
		this.ds = ds;
		this.queryString = sql;
		this.ondChargesSearchCriteria = criteria;
	}

	/**
	 * Method that uses the JdbcTemplate with prepared statement creator and extractor int
	 * 
	 * @return
	 */
	public int getCountofCharges() {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(ds);
		Integer count = (Integer) jdbcTemplate.query(new ChargePrepStatCreator(ondChargesSearchCriteria, queryString),
				new RetrieveFaresCount());
		return count.intValue();
	}

	/**
	 * Class that passes the criteria to the prepared statement.
	 * 
	 * @author Byorn
	 * 
	 */
	public class ChargePrepStatCreator implements PreparedStatementCreator {

		private String queryString;
		@SuppressWarnings("unused")
		private ONDChargeSearchCriteria criteria;

		public ChargePrepStatCreator(ONDChargeSearchCriteria criteria, String sql) {
			this.criteria = criteria;
			this.queryString = sql;
		}

		public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
			PreparedStatement prepStmt = con.prepareStatement(queryString);
			// prepStmt.setString(1, criteria.get==null? "%":criteria.getAgentCode());
			return prepStmt;
		}

	}

	/**
	 * Class that is used inside JdbcTemplate Will access a single colummn and row, to get the count value.
	 * 
	 * @author Byorn
	 * 
	 */
	public class RetrieveFaresCount implements ResultSetExtractor {

		public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
			Integer count = null;
			while (rs.next()) {
				count = new Integer(rs.getInt(1));
			}
			return count;

		}
	}
}
