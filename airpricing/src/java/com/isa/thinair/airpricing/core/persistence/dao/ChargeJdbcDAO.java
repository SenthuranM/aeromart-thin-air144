package com.isa.thinair.airpricing.core.persistence.dao;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airinventory.api.dto.baggage.FlightBaggageDTO;
import com.isa.thinair.airinventory.api.dto.meal.FlightMealDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.FareSummaryDTO;
import com.isa.thinair.airpricing.api.dto.QuoteChargesCriteria;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airpricing.api.dto.QuotedONDChargesDTO;
import com.isa.thinair.airpricing.api.model.ONDBaggageTemplate;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;

public interface ChargeJdbcDAO {

	/**
	 * Filter charges which have an effective charge for the given depature date and matched specified OND and POS
	 * 
	 * @param criteriaCollection
	 * @return HashMap contains ChargeQuoteDTOs keyed by ondCode
	 */
	public HashMap<String, HashMap<String, QuotedChargeDTO>> quoteCharges(Collection<QuoteChargesCriteria> criteriaCollection)
			throws ModuleException;

	/**
	 * Rebuild the quoted charge form chargeRateIds
	 * 
	 * @param chargeRateIds
	 * @return
	 */
	public HashMap<String, QuotedChargeDTO> rebuildQuoteCharges(Collection<Integer> chargeRateIds, QuoteChargesCriteria criteria);

	public HashMap<String, QuotedChargeDTO> quoteUniqueChargeForGroups(Collection<String> chargeGroups);

	public QuotedONDChargesDTO quoteONDCharges(Collection<QuoteChargesCriteria> criteriaCollection) throws ModuleException;

	/**
	 * Return charge groups
	 * 
	 * @return
	 */
	public Map<String, String> getChargeGroups();

	/**
	 * Return Active charge charge with rate applicable for the specified effective date.
	 * 
	 * @param chargeCode
	 * @param effectiveDate
	 * @param fareSummaryDTO
	 *            basis for charge value when charge is specified as a percentage
	 * @return
	 */
	public QuotedChargeDTO getCharge(String chargeCode, Date effectiveDate, FareSummaryDTO fareSummaryDTO,
			Integer operationalMode, boolean checkSalesValidity);

	/**
	 * @param chargeCode
	 * @param effectiveDate
	 * @param fareSummaryDTO
	 * @param operationlaMode
	 * @return
	 */
	public List<QuotedChargeDTO> getCharges(String chargeCode, Date effectiveDate, FareSummaryDTO fareSummaryDTO,
			Integer operationalMode);

	public Collection<Integer> getSegmentIdsForChargeTemplate(int templateId);

	public Collection<FlightMealDTO> getSegmentMealsForMealTemplate(int templateId);

	public Collection<FlightBaggageDTO> getSegmentBaggagesForBaggageTemplate(int templateId);

	// JIRA 6929
	public String getReportingChargeGroupCodeByChargeRateID(Integer chargeRateId);

	public boolean isChargeAssignedForPnr(Integer chargeRateId);

	public boolean isValidChargeExists(String chargeCode, String ondCode) throws CommonsDataAccessException;
	
	public boolean overlapsBaggageTemplatesFlights(ONDBaggageTemplate baggageTemplate);

	public boolean overlapsBaggageTemplatesAgents(ONDBaggageTemplate baggageTemplate);

	public boolean overlapsBaggageTemplatesBookingClasses(ONDBaggageTemplate baggageTemplate);

	public Map<Integer, Map<CommonsConstants.BaggageCriterion, List<String>>> getOverlappedTemplates(ONDBaggageTemplate baggageTemplate);
	
	public Map<String, List<String>> getServiceTaxWiseExcludableChargeCodes(
			Collection<String> quotedServiceTaxCodesForFirstOnd, HashSet<String> reservationChargeCodes);

	}