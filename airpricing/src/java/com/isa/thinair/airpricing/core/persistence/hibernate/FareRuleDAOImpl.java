/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:12:34
 * 
 * 
 * ===============================================================================
 */

package com.isa.thinair.airpricing.core.persistence.hibernate;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.SQLQuery;

import com.isa.thinair.airpricing.api.model.Channel;
import com.isa.thinair.airpricing.api.model.Fare;
import com.isa.thinair.airpricing.api.model.FareRule;
import com.isa.thinair.airpricing.core.persistence.dao.FareRuleDAO;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;

/**
 * BHive: FareRule module
 * 
 * @author BYORN
 * 
 *         FareRuleDAOImpl is the FareRule implementatiion for hibernate
 * @isa.module.dao-impl dao-name="FareRuleDAO"
 */

public class FareRuleDAOImpl extends PlatformBaseHibernateDaoSupport implements FareRuleDAO {

	private Log log = LogFactory.getLog(FareRuleDAOImpl.class);

	/**
	 * Method to get Page of Fare Rules.
	 */
	public Page getFareRules(List<ModuleCriterion> criteriaList, int startIndex, int pageSize, List<String> orderBy) {
		log.debug("getFareRules");
		return getPagedData(criteriaList, startIndex, pageSize, FareRule.class, orderBy);
	}

	@SuppressWarnings("unchecked")
	public Collection<FareRule> getFareRuleByBasisCode(String fareBasisCode) {
		return getSession().createQuery("from FareRule where fareBasisCode = :fareBasisCode")
				.setParameter("fareBasisCode", fareBasisCode).list();
	}

	/**
     * 
     */
	@SuppressWarnings("unchecked")
	public Collection<Channel> getChannels(Collection<Integer> channelIds) {
		log.debug("getting Channels");
		String hql = "select c from Channel c where " + prepareSqlINString("c.channelId", channelIds, "");
		return getSession().createQuery(hql).list();
	}

	/**
	 * 
	 * String
	 * 
	 * @param columnName
	 * @param values
	 * @param suffix
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	private String prepareSqlINString(String columnName, Collection values, String suffix) {
		if (values.size() == 0)
			return "";
		StringBuffer inStrBuffer = new StringBuffer(columnName + " IN ( ");
		Object[] valuesArr = values.toArray();
		if (valuesArr[0] instanceof Integer) {
			for (int i = 0; i < valuesArr.length; i++) {
				inStrBuffer.append(((Integer) valuesArr[i]).toString() + ",");
			}
		} else if (valuesArr[0] instanceof String) {
			for (int i = 0; i < valuesArr.length; i++) {
				inStrBuffer.append("'" + (String) valuesArr[i] + "',");
			}
		}
		String inStr = inStrBuffer.toString();
		return inStr.substring(0, inStr.lastIndexOf(',')).concat(")").concat(suffix);
	}

	/**
	 * DAO Method when saving a FareRule
	 */
	public void saveFareRule(FareRule fareRule) {
		log.debug("saveFareRule");
		hibernateSaveOrUpdate(fareRule);

	}

	/**
	 * DAO Method to delete a Fare Rule
	 */
	public void deleteFareRule(Integer fareRuleId) {
		log.debug("delete Fare Rule");
		Object o = load(FareRule.class, fareRuleId);
		delete(o);
	}

	/**
	 * Method to get a FareRule for fareRuleCode
	 */
	// TODO Remove
	@SuppressWarnings("rawtypes")
	public FareRule getFareRule(String fareRuleCode) {
		log.debug("get Fare Rule");
		String hql = "select farerule from FareRule as farerule where  farerule.fareRuleCode=:farerulecode";

		List list = getSession().createQuery(hql).setParameter("farerulecode", fareRuleCode).list();

		if (list.size() == 0) {
			return null;
		}

		if (list.size() == 1) {
			return (FareRule) list.get(0);
		}

		throw new CommonsDataAccessException("pric.data.integrity.error", "airpricing.desc");

	}

	@SuppressWarnings("unchecked")
	public List<FareRule> getFareRules(String fareRuleCode) {
		log.debug("get Fare Rules for fareRuleCode");
		String hql = "select farerule from FareRule as farerule where  farerule.fareRuleCode=:farerulecode";

		try {
			return getSession().createQuery(hql).setParameter("farerulecode", fareRuleCode).list();
		} catch (Exception e) {
			throw new CommonsDataAccessException(e, null);
		}

	}

	public void inactiveFareForFareRule(int fareRuleId) {
		log.debug("inactiveFares");

		String hqlUpdate = "update Fare set status =:status where fareRuleId =:fareruleid";
		try {
			getSession().createQuery(hqlUpdate).setString("status", Fare.STATUS_INACTIVE).setInteger("fareruleid", fareRuleId)
					.executeUpdate();

		} catch (Exception e) {
			throw new CommonsDataAccessException(e, null);
		}

	}

	/**
	 * getFareRule for fareRuleID
	 */
	public FareRule getFareRule(int fareRuleID) {
		return (FareRule) get(FareRule.class, new Integer(fareRuleID));
	}

	/**
     * 
     */
	@SuppressWarnings("unchecked")
	public Collection<Channel> getAllSalesChannels() {
		log.debug("getting Channels");
		String hql = "select c from Channel c ";
		return getSession().createQuery(hql).list();
	}

	@Override
	public Map<Integer, FareRule> getFareRulesMap(Collection<Integer> fareRuleIds) {

		String hql = "select farerule from FareRule as farerule where farerule.fareRuleId in ("
				+ BeanUtils.constructINStringForInts(fareRuleIds) + ")";

		Map<Integer, FareRule> mapFareRule = new HashMap<Integer, FareRule>();
		List<FareRule> listFareRule = getSession().createQuery(hql).list();

		for (FareRule fareRule : listFareRule) {
			mapFareRule.put(fareRule.getFareRuleId(), fareRule);
		}

		return mapFareRule;
	}

	@Override
	public List<FareRule> getApplicableFareRulesForExchangeRateChange(String currencyCode) {
		String hql = "Select fr from FareRule fr where fr.localCurrencyCode=:currencyCode";
		Query q = getSession().createQuery(hql);
		q.setParameter("currencyCode", currencyCode);
		return q.list();
	}

	@Override
	public FareRule getFareRuleForFareID(Integer fareId) {

		List<FareRule> fareRules = null;

		StringBuilder queryContent = new StringBuilder();
		queryContent.append("select {fr.*} from t_fare_rule fr, t_ond_fare ondf ");
		queryContent.append("where ondf.fare_id =:fareId ");
		queryContent.append("and  ondf.fare_rule_id = fr.fare_rule_id ");

		SQLQuery query = getSession().createSQLQuery(queryContent.toString());
		query.setParameter("fareId", fareId);
		query.addEntity("fr", FareRule.class);

		fareRules = query.list();

		if (fareRules != null && !fareRules.isEmpty()) {
			return fareRules.get(0);
		} else {
			return null;
		}
	}

	// private void migrateFareRuleComments() {
	// String hql = "FROM FareRuleComment";
	// Query q = getSession().createQuery(hql);
	// Collection<FareRuleComment> fareRuleComments = q.list();
	// for (FareRuleComment fareRuleComment : fareRuleComments) {
	// String hexComment = StringUtil.convertToHex(fareRuleComment.getComments());
	// fareRuleComment.setComments(hexComment);
	// }
	// hibernateSaveOrUpdateAll(fareRuleComments);
	// }
}
