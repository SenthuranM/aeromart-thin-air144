package com.isa.thinair.airpricing.core.bl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airinventory.api.dto.seatavailability.FlightSegmentDTO;
import com.isa.thinair.airinventory.api.util.InventoryAPIUtil;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airpricing.api.dto.SSRChargeDTO;
import com.isa.thinair.airpricing.api.dto.SSRChargeSearchDTO;
import com.isa.thinair.airpricing.api.model.SSRCharge;
import com.isa.thinair.airpricing.api.model.SSRChargesCOS;
import com.isa.thinair.airpricing.api.service.AirpricingUtils;
import com.isa.thinair.airpricing.api.util.AirPricingCustomConstants;
import com.isa.thinair.airpricing.api.util.LocalCurrencyUtil;
import com.isa.thinair.airpricing.api.util.SplitUtil;
import com.isa.thinair.airpricing.core.audit.AuditAirpricing;
import com.isa.thinair.airpricing.core.persistence.dao.ChargeDAO;
import com.isa.thinair.airpricing.core.persistence.dao.SSRChargeJdbcDAO;
import com.isa.thinair.airpricing.core.persistence.jdbc.ssrchargejdbcdao.SSRChargeSearchCriteria;
import com.isa.thinair.airpricing.core.util.InternalConstants;
import com.isa.thinair.commons.api.dto.KeyValue;
import com.isa.thinair.commons.api.dto.SSRCategoryDTO;
import com.isa.thinair.commons.api.dto.SSRChargesDTO;
import com.isa.thinair.commons.api.dto.SSRInfoDTO;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AccelAeroRounderPolicy;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.webplatform.api.util.Constants;

public class SSRChargeBL {
	private enum ApplyPattern {
		DEPARTURE, ARRIVAL, TRANSIT
	}

	private final static Log log = LogFactory.getLog(SSRChargeBL.class);

	ChargeDAO chargedao;

	/**
	 * 
	 * @param ssrChargeSearchDTO
	 * @return Map<airportCode,Map<ssrChargeCode, SSRChargeDTO>>
	 * @throws ModuleException
	 */
	public static HashMap<String, Collection<SSRChargeDTO>> getSSRCharges(SSRChargeSearchDTO ssrChargeSearchDTO)
			throws ModuleException {
		Collection<SSRChargeDTO> availableSSRChargeDTOs = getAvailableSSRChargeMap(ssrChargeSearchDTO);
		HashMap<String, Map<String, SSRChargeDTO>> airportSSRChargeMap = getAirportSSRChargeMap(availableSSRChargeDTOs,
				ssrChargeSearchDTO);

		HashMap<String, Collection<SSRChargeDTO>> newAirportSSRChargeMap = getSSRPaxChargeDetails(airportSSRChargeMap,
				ssrChargeSearchDTO);

		return newAirportSSRChargeMap;

	}

	private static HashMap<String, Map<String, SSRChargeDTO>> getAirportSSRChargeMap(
			Collection<SSRChargeDTO> availableSSRChargeDTOs, SSRChargeSearchDTO ssrChargeSearchDTO) {
		HashMap<String, Map<String, SSRChargeDTO>> airportSSRChargeMap = new HashMap<String, Map<String, SSRChargeDTO>>();
		Map<String, KeyValue> transitAirportMap = getTransitAirports(ssrChargeSearchDTO);
		Map<String, Boolean> applicableAirportMap = CommonsServices.getGlobalConfig().getSSRApplicableAirportsMap();

		for (Iterator<SSRChargeDTO> iterator = availableSSRChargeDTOs.iterator(); iterator.hasNext();) {
			SSRChargeDTO chargeDTO = (SSRChargeDTO) iterator.next();

			String ondCode = chargeDTO.getONDCode();
			KeyValue airportONDPattern = getAirportONDPattern(ondCode);

			if (isBookingAllowed(chargeDTO, ssrChargeSearchDTO) && airportONDPattern != null) {
				String airportCode = (String) airportONDPattern.getKey();
				ApplyPattern ondPatternType = (ApplyPattern) airportONDPattern.getValue();

				if (chargeDTO.isApplyForSegment()
						|| applicableAirportMap.get(chargeDTO.getSSRId().toString() + airportCode) != null) {
					Map<String, SSRChargeDTO> ssrChargeDTOMap = airportSSRChargeMap.get(airportCode);

					if (ssrChargeDTOMap == null) {
						ssrChargeDTOMap = new HashMap<String, SSRChargeDTO>();
						airportSSRChargeMap.put(airportCode, ssrChargeDTOMap);
					}

					SSRChargeDTO newChargeDTO = ssrChargeDTOMap.get(chargeDTO.getSSRChargeCode());

					if (newChargeDTO == null) {
						newChargeDTO = createChargeDTO(chargeDTO);
						ssrChargeDTOMap.put(chargeDTO.getSSRChargeCode(), newChargeDTO);
					}

					if (!chargeDTO.isApplyForSegment()) {
						if (ondPatternType == ApplyPattern.DEPARTURE) {
							newChargeDTO.setApplyForDeparture(chargeDTO.isApplyForDeparture());
						} else if (ondPatternType == ApplyPattern.ARRIVAL) {
							newChargeDTO.setApplyForArrival(chargeDTO.isApplyForArrival());
						} else if (ondPatternType == ApplyPattern.TRANSIT) {
							KeyValue transitAirport = transitAirportMap.get(airportCode);

							if (transitAirport != null) {
								if (newChargeDTO.getMinimumTransitTime().compareTo(((Integer) transitAirport.getValue())) <= 0) {
									newChargeDTO.setApplyForTransit(chargeDTO.isApplyForTransit());
								}
							}

						}
					}
				}
			}
		}

		return filterApplicableAirports(airportSSRChargeMap);
	}

	private static HashMap<String, Map<String, SSRChargeDTO>> filterApplicableAirports(
			HashMap<String, Map<String, SSRChargeDTO>> airportSSRChargeMap) {
		Collection<Map<String, SSRChargeDTO>> ssrChargeMaps = airportSSRChargeMap.values();

		for (Iterator<Map<String, SSRChargeDTO>> itSSRChargeMaps = ssrChargeMaps.iterator(); itSSRChargeMaps.hasNext();) {
			Map<String, SSRChargeDTO> ssrChargeMap = (Map<String, SSRChargeDTO>) itSSRChargeMaps.next();
			Collection<SSRChargeDTO> ssrChargeDTOs = ssrChargeMap.values();
			Collection<SSRChargeDTO> invalidChargeDTOs = new ArrayList<SSRChargeDTO>();

			for (Iterator<SSRChargeDTO> iterator = ssrChargeDTOs.iterator(); iterator.hasNext();) {
				SSRChargeDTO chargeDTO = (SSRChargeDTO) iterator.next();

				if ((!chargeDTO.isApplyForAdult() && !chargeDTO.isApplyForChild() && !chargeDTO.isApplyForInfant())
						|| (!chargeDTO.isApplyForDeparture() && !chargeDTO.isApplyForArrival() && !chargeDTO.isApplyForTransit())) {
					invalidChargeDTOs.add(chargeDTO);
				}

			}

			ssrChargeDTOs.removeAll(invalidChargeDTOs);
		}

		return airportSSRChargeMap;
	}

	private static boolean isBookingAllowed(SSRChargeDTO chargeDTO, SSRChargeSearchDTO ssrChargeSearchDTO) {
		Map<Integer, SSRInfoDTO> ssrInfoMap = CommonsServices.getGlobalConfig().getSSRInfoByIdMap();
		Map<Integer, SSRCategoryDTO> ssrCategoryMap = CommonsServices.getGlobalConfig().getSsrCategoryMap();
		Integer ssrCategoryId = ssrInfoMap.get(chargeDTO.getSSRId()).getSSRCategoryId();
		SSRCategoryDTO ssrCategory = ssrCategoryMap.get(ssrCategoryId);

		Date depDate = InventoryAPIUtil.getFirstDepartureDate(ssrChargeSearchDTO.getFlightSegments());

		boolean isAllowed = CalendarUtil.isInEndBuffer(depDate, null, ssrCategory.getSSRBookingCutoverTime());

		return isAllowed;
	}

	private static Map<String, KeyValue> getTransitAirports(SSRChargeSearchDTO ssrChargeSearchDTO) {
		Collection<FlightSegmentDTO> flightSegmentDTOs = ssrChargeSearchDTO.getFlightSegments();
		Map<String, KeyValue> transitAirportMap = new HashMap<String, KeyValue>();
		FlightSegmentDTO prevFlightSegmentDTO = null;

		if (flightSegmentDTOs.size() > 1) {
			for (Iterator<FlightSegmentDTO> iterator = flightSegmentDTOs.iterator(); iterator.hasNext();) {
				FlightSegmentDTO flightSegmentDTO = (FlightSegmentDTO) iterator.next();
				String segmentCode = flightSegmentDTO.getSegmentCode();

				String depAirport = segmentCode.substring(0, 3);

				if (prevFlightSegmentDTO != null && prevFlightSegmentDTO.getSegmentCode().substring(4).equals(depAirport)) {
					// int transitTimeHrs =
					// CalendarUtil.getTimeDifferenceInHours(prevFlightSegmentDTO.getArrivalDateTimeZulu(),
					// flightSegmentDTO.getDepartureDateTimeZulu());
					int transitTimeMins = CalendarUtil.getTimeDifferenceInMinutes(prevFlightSegmentDTO.getArrivalDateTimeZulu(),
							flightSegmentDTO.getDepartureDateTimeZulu());
					KeyValue transitAirport = new KeyValue(depAirport, transitTimeMins);

					transitAirportMap.put(depAirport, transitAirport);
				}

				prevFlightSegmentDTO = flightSegmentDTO;

			}
		}

		return transitAirportMap;
	}

	private static SSRChargeDTO createChargeDTO(SSRChargeDTO chargeDTO) {
		SSRChargeDTO newChargeDTO;

		try {
			newChargeDTO = chargeDTO.clone();
			newChargeDTO.setApplyForDeparture(false);
			newChargeDTO.setApplyForArrival(false);
			newChargeDTO.setApplyForTransit(false);

		} catch (CloneNotSupportedException e) {
			throw new RuntimeException(e);
		}

		return newChargeDTO;
	}

	private static void validateSSRChargesSearchParams(SSRChargeSearchDTO ssrChargeSearchDTO) throws ModuleException {
		if (ssrChargeSearchDTO.getAppIndicator() == null) {
			throw new ModuleException("airpricing.ssrcharges.search.applicator.null");
		}

		if (ssrChargeSearchDTO.getFlightSegments() == null || ssrChargeSearchDTO.getFlightSegments().isEmpty()) {
			throw new ModuleException("airpricing.ssrcharges.search.segment.null");
		}

	}

	private static SSRChargeJdbcDAO getSSRChargeJdbcDAO() {
		return (SSRChargeJdbcDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.SSR_CHARGE_JDBC_DAO);
	}

	private static Collection<SSRChargeDTO> getAvailableSSRChargeMap(SSRChargeSearchDTO ssrChargeSearchDTO)
			throws ModuleException {
		Collection<SSRChargeDTO> availableSSRChargeDTOs = null;

		validateSSRChargesSearchParams(ssrChargeSearchDTO);

		Collection<String> ondPatterns = getONDPatterns(ssrChargeSearchDTO);
		SSRChargeSearchCriteria ssrChargeSearchCriteria = new SSRChargeSearchCriteria();

		ssrChargeSearchCriteria.setAppIndicator(ssrChargeSearchDTO.getAppIndicator());
		ssrChargeSearchCriteria.setONDPatterns(ondPatterns);

		availableSSRChargeDTOs = getSSRChargeJdbcDAO().getAvailableSSRChargeDTOs(ssrChargeSearchCriteria);

		return availableSSRChargeDTOs;

	}

	private static Collection<String> getONDPatterns(SSRChargeSearchDTO ssrChargeSearchDTO) {
		Collection<String> segmentCodes = getSegmentCodes(ssrChargeSearchDTO);
		String origin = ssrChargeSearchDTO.getFromAirpot();
		String destination = ssrChargeSearchDTO.getToAirpot();

		return getONDPatterns(segmentCodes, origin, destination);
	}

	private static Collection<String> getONDPatterns(Collection<String> segmentCodes, String origin, String destination) {
		Collection<String> ondPatterns = new ArrayList<String>();
		String preAirport = null;

		for (Iterator<String> iterator = segmentCodes.iterator(); iterator.hasNext();) {
			String segCode = (String) iterator.next();
			String fromSegApt = segCode.substring(0, 3);
			String toSegApt = segCode.substring(4);

			if (preAirport == null) {
				preAirport = fromSegApt;
				ondPatterns.add(fromSegApt + "/***");
			}

			if (toSegApt.equals(origin) || toSegApt.equals(destination)) {
				ondPatterns.add("***/" + toSegApt);
				preAirport = null;
			} else {
				ondPatterns.add("***/" + toSegApt + "/***");
			}
		}

		return ondPatterns;
	}

	private static KeyValue getAirportONDPattern(String ondPattern) {
		String depApt = ondPattern.substring(0, 3);
		String arrApt = ondPattern.substring(4, 7);
		ApplyPattern ondPatternType = null;
		String machingApt = null;
		KeyValue airpotONDType = null;

		if (!"***".equals(depApt)) {
			machingApt = depApt;
			ondPatternType = ApplyPattern.DEPARTURE;
		} else if (!"***".equals(arrApt)) {
			machingApt = arrApt;

			if (ondPattern.length() == 7) {
				ondPatternType = ApplyPattern.ARRIVAL;
			} else {
				ondPatternType = ApplyPattern.TRANSIT;
			}
		}

		if (machingApt != null) {
			airpotONDType = new KeyValue(machingApt, ondPatternType);
		}

		return airpotONDType;
	}

	private static Collection<String> getSegmentCodes(SSRChargeSearchDTO ssrChargeSearchDTO) {
		Collection<FlightSegmentDTO> flightSegmentDTOs = ssrChargeSearchDTO.getFlightSegments();
		Set<String> segmentCodes = new HashSet<String>();

		for (Iterator<FlightSegmentDTO> iterator = flightSegmentDTOs.iterator(); iterator.hasNext();) {
			FlightSegmentDTO flightSegmentDTO = (FlightSegmentDTO) iterator.next();

			segmentCodes.add(flightSegmentDTO.getSegmentCode());
		}

		return segmentCodes;
	}

	private static HashMap<String, Collection<SSRChargeDTO>> getSSRPaxChargeDetails(
			HashMap<String, Map<String, SSRChargeDTO>> airportSSRChargeMap, SSRChargeSearchDTO ssrChargeSearchDTO) {
		Set<String> airportKeys = airportSSRChargeMap.keySet();
		HashMap<String, Collection<SSRChargeDTO>> newAirportSSRChargeMap = new HashMap<String, Collection<SSRChargeDTO>>();

		for (Iterator<String> itAirportKeys = airportKeys.iterator(); itAirportKeys.hasNext();) {
			String airport = (String) itAirportKeys.next();
			Map<String, SSRChargeDTO> chargesMap = airportSSRChargeMap.get(airport);

			SSRChargeDTO massFamilyCharge = chargesMap.get(SSRCharge.CHARGE_CODE_HALA_MAAS_FAMILY);
			SSRChargeDTO massSingleCharge = chargesMap.get(SSRCharge.CHARGE_CODE_HALA_MAAS_SINGLE);
			SSRChargeDTO loungeFirstCharge = chargesMap.get(SSRCharge.CHARGE_CODE_HALA_LOUNGE_FIRST_CLASS);
			SSRChargeDTO loungeBusiCharge = chargesMap.get(SSRCharge.CHARGE_CODE_HALA_LOUNGE_BUSINESS_CLASS);

			addMASSCharges(airport, newAirportSSRChargeMap, ssrChargeSearchDTO, massFamilyCharge, massSingleCharge);
			addLoungeCharges(airport, newAirportSSRChargeMap, ssrChargeSearchDTO, loungeFirstCharge, loungeBusiCharge);
		}

		return newAirportSSRChargeMap;
	}

	private static void addMASSCharges(String airport, HashMap<String, Collection<SSRChargeDTO>> newAirportSSRChargeMap,
			SSRChargeSearchDTO ssrChargeSearchDTO, SSRChargeDTO massFamilyCharge, SSRChargeDTO massSingleCharge) {
		// Collection<FlightSegmentDTO> flightSegments = ssrChargeSearchDTO.getFlightSegments();

		boolean isFamilyPackageAdded = addMASSFamilyCharges(airport, newAirportSSRChargeMap, ssrChargeSearchDTO, massFamilyCharge);

		if (!isFamilyPackageAdded) {
			addMASSSingleCharges(airport, newAirportSSRChargeMap, ssrChargeSearchDTO, massSingleCharge);
		}

	}

	private static boolean addMASSSingleCharges(String airport, HashMap<String, Collection<SSRChargeDTO>> newAirportSSRChargeMap,
			SSRChargeSearchDTO ssrChargeSearchDTO, SSRChargeDTO massSingleCharge) {
		int adultCount = ssrChargeSearchDTO.getAdultCount();
		int childCount = ssrChargeSearchDTO.getChildCount();
		int infantCount = ssrChargeSearchDTO.getInfantCount();

		if (massSingleCharge == null) {
			return false;
		}

		Collection<SSRChargeDTO> ssrChargeDTOs = newAirportSSRChargeMap.get(airport);

		if (ssrChargeDTOs == null) {
			ssrChargeDTOs = new HashSet<SSRChargeDTO>();
			newAirportSSRChargeMap.put(airport, ssrChargeDTOs);
		}

		BigDecimal[] adultCharges = new BigDecimal[adultCount];
		BigDecimal[] childCharges = new BigDecimal[childCount];
		BigDecimal[] infantCharges = new BigDecimal[infantCount];

		for (int i = 0; i < adultCount; i++) {
			adultCharges[i] = massSingleCharge.getChargeAmount();
		}

		for (int i = 0; i < childCount; i++) {
			childCharges[i] = massSingleCharge.getChargeAmount();
		}

		for (int i = 0; i < infantCount; i++) {
			infantCharges[i] = massSingleCharge.getChargeAmount();
		}

		massSingleCharge.setAdultCharges(adultCharges);
		massSingleCharge.setChildCharges(childCharges);
		massSingleCharge.setInfantCharges(infantCharges);

		ssrChargeDTOs.add(massSingleCharge);

		return true;

	}

	private static boolean addMASSFamilyCharges(String airport, HashMap<String, Collection<SSRChargeDTO>> newAirportSSRChargeMap,
			SSRChargeSearchDTO ssrChargeSearchDTO, SSRChargeDTO massFamilyCharge) {
		int adultCount = ssrChargeSearchDTO.getAdultCount();
		int childCount = ssrChargeSearchDTO.getChildCount();
		int infantCount = ssrChargeSearchDTO.getInfantCount();

		if (massFamilyCharge == null) {
			return false;
		}

		Integer familyMaxTotalPax = massFamilyCharge.getMaximumTotalPax();
		Integer familyMinTotalPax = massFamilyCharge.getMinimumTotalPax();

		Integer familyMaxAdultPax = massFamilyCharge.getMaximumAdultPax();
		Integer familyMinAdultPax = massFamilyCharge.getMinimumAdultPax();

		Integer familyMaxChildPax = massFamilyCharge.getMaximumChildPax();
		Integer familyMinChildPax = massFamilyCharge.getMinimumChildPax();

		// Integer familyMaxInfantPax = massFamilyCharge.getMaximumInfantPax();
		// Integer familyMinInfantPax = massFamilyCharge.getMinimumInfantPax();

		int totalCount = adultCount + childCount;// + infantCount; // REFACTORED infant should not be counted.

		boolean isFamilyPackageAdded = false;

		if (totalCount <= familyMaxTotalPax && totalCount >= familyMinTotalPax) {
			if (adultCount <= familyMaxAdultPax && adultCount >= familyMinAdultPax) {
				if (childCount <= familyMaxChildPax && childCount >= familyMinChildPax) {
					// if (infantCount <= familyMaxInfantPax && infantCount >= familyMinInfantPax) {
					Collection<SSRChargeDTO> ssrChargeDTOs = newAirportSSRChargeMap.get(airport);

					if (ssrChargeDTOs == null) {
						ssrChargeDTOs = new HashSet<SSRChargeDTO>();
						newAirportSSRChargeMap.put(airport, ssrChargeDTOs);
					}

					isFamilyPackageAdded = true;

					BigDecimal[] familyAdultCharges = new BigDecimal[adultCount];
					BigDecimal[] familyChildCharges = new BigDecimal[childCount];
					BigDecimal[] familyInfantCharges = new BigDecimal[infantCount];

					BigDecimal[] paxAmounts = AccelAeroCalculator.roundAndSplit(massFamilyCharge.getChargeAmount(), totalCount);

					for (int i = 0; i < adultCount; i++) {
						familyAdultCharges[i] = paxAmounts[i];
					}

					for (int i = 0; i < childCount; i++) {
						familyChildCharges[i] = paxAmounts[i + adultCount];
					}

					// for (int i = 0; i < infantCount; i++) {
					// familyInfantCharges[i] = paxAmounts[i + adultCount + childCount];
					// }

					massFamilyCharge.setAdultCharges(familyAdultCharges);
					massFamilyCharge.setChildCharges(familyChildCharges);
					massFamilyCharge.setInfantCharges(familyInfantCharges);

					ssrChargeDTOs.add(massFamilyCharge);
				}
				// }
			}
		}

		return isFamilyPackageAdded;

	}

	private static void addLoungeCharges(String airport, HashMap<String, Collection<SSRChargeDTO>> newAirportSSRChargeMap,
			SSRChargeSearchDTO ssrChargeSearchDTO, SSRChargeDTO loungeFirstCharge, SSRChargeDTO loungeBusiCharge) {
		addLoungeFirstClassCharges(airport, newAirportSSRChargeMap, ssrChargeSearchDTO, loungeFirstCharge);
		addLoungeBusinessClassCharges(airport, newAirportSSRChargeMap, ssrChargeSearchDTO, loungeBusiCharge);
	}

	private static void addLoungeBusinessClassCharges(String airport,
			HashMap<String, Collection<SSRChargeDTO>> newAirportSSRChargeMap, SSRChargeSearchDTO ssrChargeSearchDTO,
			SSRChargeDTO loungeBusiCharge) {
		if (loungeBusiCharge != null) {
			// Collection<FlightSegmentDTO> flightSegments =
			// ssrChargeSearchDTO.getFlightSegments();

			int adultCount = ssrChargeSearchDTO.getAdultCount();
			int childCount = ssrChargeSearchDTO.getChildCount();
			int infantCount = ssrChargeSearchDTO.getInfantCount();

			// Integer maxTotalPax = loungeBusiCharge.getMaximumTotalPax();
			// Integer minTotalPax = loungeBusiCharge.getMinimumTotalPax();
			//
			// Integer maxAdultPax = loungeBusiCharge.getMaximumAdultPax();
			// Integer minAdultPax = loungeBusiCharge.getMinimumAdultPax();
			//
			// Integer maxChildPax = loungeBusiCharge.getMaximumChildPax();
			// Integer minChildPax = loungeBusiCharge.getMinimumChildPax();
			//
			// Integer maxInfantPax = loungeBusiCharge.getMaximumInfantPax();
			// Integer minInfantPax = loungeBusiCharge.getMinimumInfantPax();

			BigDecimal[] adultCharges = new BigDecimal[adultCount];
			BigDecimal[] childCharges = new BigDecimal[childCount];
			BigDecimal[] infantCharges = new BigDecimal[infantCount];

			for (int i = 0; i < adultCount; i++) {
				adultCharges[i] = loungeBusiCharge.getChargeAmount();
			}

			for (int i = 0; i < childCount; i++) {
				childCharges[i] = loungeBusiCharge.getChargeAmount();
			}

			for (int i = 0; i < infantCount; i++) {
				infantCharges[i] = loungeBusiCharge.getChargeAmount();
			}

			Collection<SSRChargeDTO> ssrChargeDTOs = newAirportSSRChargeMap.get(airport);

			if (ssrChargeDTOs == null) {
				ssrChargeDTOs = new HashSet<SSRChargeDTO>();
				newAirportSSRChargeMap.put(airport, ssrChargeDTOs);
			}

			loungeBusiCharge.setAdultCharges(adultCharges);
			loungeBusiCharge.setChildCharges(childCharges);
			loungeBusiCharge.setInfantCharges(infantCharges);

			ssrChargeDTOs.add(loungeBusiCharge);
		}
	}

	private static void addLoungeFirstClassCharges(String airport,
			HashMap<String, Collection<SSRChargeDTO>> newAirportSSRChargeMap, SSRChargeSearchDTO ssrChargeSearchDTO,
			SSRChargeDTO loungeFirstCharge) {
		if (loungeFirstCharge != null) {
			// Collection<FlightSegmentDTO> flightSegments =
			// ssrChargeSearchDTO.getFlightSegments();

			int adultCount = ssrChargeSearchDTO.getAdultCount();
			int childCount = ssrChargeSearchDTO.getChildCount();
			int infantCount = ssrChargeSearchDTO.getInfantCount();

			// Integer maxTotalPax = loungeFirstCharge.getMaximumTotalPax();
			// Integer minTotalPax = loungeFirstCharge.getMinimumTotalPax();
			//
			// Integer maxAdultPax = loungeFirstCharge.getMaximumAdultPax();
			// Integer minAdultPax = loungeFirstCharge.getMinimumAdultPax();
			//
			// Integer maxChildPax = loungeFirstCharge.getMaximumChildPax();
			// Integer minChildPax = loungeFirstCharge.getMinimumChildPax();
			//
			// Integer maxInfantPax = loungeFirstCharge.getMaximumInfantPax();
			// Integer minInfantPax = loungeFirstCharge.getMinimumInfantPax();

			BigDecimal[] adultCharges = new BigDecimal[adultCount];
			BigDecimal[] childCharges = new BigDecimal[childCount];
			BigDecimal[] infantCharges = new BigDecimal[infantCount];

			for (int i = 0; i < adultCount; i++) {
				adultCharges[i] = loungeFirstCharge.getChargeAmount();
			}

			for (int i = 0; i < childCount; i++) {
				childCharges[i] = loungeFirstCharge.getChargeAmount();
			}

			for (int i = 0; i < infantCount; i++) {
				infantCharges[i] = loungeFirstCharge.getChargeAmount();
			}

			Collection<SSRChargeDTO> ssrChargeDTOs = newAirportSSRChargeMap.get(airport);

			if (ssrChargeDTOs == null) {
				ssrChargeDTOs = new HashSet<SSRChargeDTO>();
				newAirportSSRChargeMap.put(airport, ssrChargeDTOs);
			}

			loungeFirstCharge.setAdultCharges(adultCharges);
			loungeFirstCharge.setChildCharges(childCharges);
			loungeFirstCharge.setInfantCharges(infantCharges);

			ssrChargeDTOs.add(loungeFirstCharge);
		}
	}

	public static SSRCharge getSSRCharge(SSRCharge ssrCharge, SSRChargesDTO ssrChargesDTO, String cos) throws ModuleException {
		try {
			// if (ssrChargesDTO.getChargeId() != null && ssrChargesDTO.getChargeId() > 0) {
			if (ssrCharge != null) {
				// ssrCharge = ModuleServiceLocator.getChargeBD().getSSRCharges(ssrChargesDTO.getChargeId());
				ssrCharge.setChargeId(ssrChargesDTO.getChargeId());
			} else {
				ssrCharge = new SSRCharge();

				ssrCharge.setMaxAdultPax(0);
				ssrCharge.setMinAdultPax(0);
				ssrCharge.setMaxChildPax(0);
				ssrCharge.setMinChildPax(0);
				ssrCharge.setMaxInfantPax(0);
				ssrCharge.setMinInfantPax(0);
				ssrCharge.setMaxTotalPax(0);
				ssrCharge.setMinTotalPax(0);
				ssrCharge.setCommissionIn("V");
				ssrCharge.setCommissionValue(new BigDecimal(0));
				ssrCharge.setLocalCurrChargeAmount(new BigDecimal(0));
				ssrCharge.setChargeAmount(new BigDecimal(0));
				ssrCharge.setAirportCode("***");

			}

			String[] cosList = cos.split(Constants.STRING_SEPARATOR);
			Set<SSRChargesCOS> ssrChargeCOSList = new HashSet<SSRChargesCOS>();
			for (String element : cosList) {
				SSRChargesCOS ssrChargeCOS = new SSRChargesCOS();
				String[] splittedCabinClassAndLogicalCabinClass = SplitUtil.getSplittedCabinClassAndLogicalCabinClass(element);
				ssrChargeCOS.setSsrCharge(ssrCharge);
				if (splittedCabinClassAndLogicalCabinClass[0] != null) {
					ssrChargeCOS.setCabinClassCode(splittedCabinClassAndLogicalCabinClass[0]);
				} else {
					ssrChargeCOS.setLogicalCCCode(splittedCabinClassAndLogicalCabinClass[1]);
				}
				ssrChargeCOSList.add(ssrChargeCOS);
			}
			ssrCharge.setSsrChargeCOS(ssrChargeCOSList);

			Date currentSysDate = new Date();
			GregorianCalendar calendar1 = new GregorianCalendar();
			calendar1.setTime(currentSysDate);
			// calendar1.add(GregorianCalendar.MONTH,currentSysDate.);

			GregorianCalendar calendar2 = new GregorianCalendar();
			calendar2.setTime(currentSysDate);
			calendar2.add(GregorianCalendar.MONTH, (calendar1.get(GregorianCalendar.MONTH) + 3));

			ssrCharge.setEffectiveFrom(calendar1.getTime());
			ssrCharge.setEffectiveTo(calendar2.getTime());

			ssrCharge.setStatus(ssrChargesDTO.getStatus());
			ssrCharge.setDescription(ssrChargesDTO.getDescription());
			ssrCharge.setVersion(ssrChargesDTO.getVersion());
			ssrCharge.setSsrId(ssrChargesDTO.getSsrId());
			ssrCharge.setSsrChargeCode(ssrChargesDTO.getSsrChargeCode());

			String applyBy = ssrChargesDTO.getApplicablityType();

			ssrCharge.setLocalCurrAdultAmount(ssrChargesDTO.getLocalCurrAdultAmount());
			ssrCharge.setAdultAmount(LocalCurrencyUtil.amountInBaseCurrency(ssrChargesDTO.getChargeLocalCurrencyCode(),
					ssrChargesDTO.getLocalCurrAdultAmount()));

			ssrCharge.setLocalCurrChildAmount(ssrChargesDTO.getLocalCurrChildAmount());
			ssrCharge.setChildAmount(LocalCurrencyUtil.amountInBaseCurrency(ssrChargesDTO.getChargeLocalCurrencyCode(),
					ssrChargesDTO.getLocalCurrChildAmount()));

			ssrCharge.setLocalCurrInfantAmount(ssrChargesDTO.getLocalCurrInfantAmount());
			ssrCharge.setInfantAmount(LocalCurrencyUtil.amountInBaseCurrency(ssrChargesDTO.getChargeLocalCurrencyCode(),
					ssrChargesDTO.getLocalCurrInfantAmount()));

			ssrCharge.setLocalCurrReservationAmount(ssrChargesDTO.getLocalCurrReservationAmount());
			ssrCharge.setReservationAmount(LocalCurrencyUtil.amountInBaseCurrency(ssrChargesDTO.getChargeLocalCurrencyCode(),
					ssrChargesDTO.getLocalCurrReservationAmount()));

			ssrCharge.setApplicablityType(applyBy);
			ssrCharge.setchargeLocalCurrencyCode(ssrChargesDTO.getChargeLocalCurrencyCode());

			return ssrCharge;
		} catch (CommonsDataAccessException e) {
			e.printStackTrace();
			log.error("ERROR-Auto Updating Charges upon Exchnage Rate Change Failed");
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}

	}

	public void updateSSRChargeBaseCurrAmountOnLocalCurrExchangeRtUpdate(CurrencyExchangeRate currencyExchangeRate,
			UserPrincipal userPrincipal) throws ModuleException {
		chargedao = (ChargeDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.CHARGE_DAO);

		try {

			ArrayList<SSRCharge> affectedSSRList;
			BigDecimal exRate, boundaryValue, breakPointValue;

			affectedSSRList = (ArrayList<SSRCharge>) chargedao.getAffectedSSRCharges(currencyExchangeRate.getCurrency()
					.getCurrencyCode());
			exRate = currencyExchangeRate.getExrateCurToBaseNumber();
			boundaryValue = currencyExchangeRate.getCurrency().getBoundryValue();
			breakPointValue = currencyExchangeRate.getCurrency().getBreakPoint();
			for (SSRCharge ssrChrg : affectedSSRList) {
				if (!ssrChrg.getStatus().equals(AirPricingCustomConstants.STATUS_ACTIVE)) {
					continue;
				}
				ssrChrg.setAdultAmount(AccelAeroRounderPolicy.getRoundedValue(
						AccelAeroCalculator.multiply(ssrChrg.getLocalCurrAdultAmount(), exRate), boundaryValue, breakPointValue));
				ssrChrg.setChildAmount(AccelAeroRounderPolicy.getRoundedValue(
						AccelAeroCalculator.multiply(ssrChrg.getLocalCurrChildAmount(), exRate), boundaryValue, breakPointValue));
				ssrChrg.setInfantAmount(AccelAeroRounderPolicy.getRoundedValue(
						AccelAeroCalculator.multiply(ssrChrg.getLocalCurrInfantAmount(), exRate), boundaryValue, breakPointValue));
				ssrChrg.setChargeAmount(AccelAeroRounderPolicy.getRoundedValue(
						AccelAeroCalculator.multiply(ssrChrg.getLocalCurrChargeAmount(), exRate), boundaryValue, breakPointValue));
				ssrChrg.setReservationAmount(AccelAeroRounderPolicy.getRoundedValue(
						AccelAeroCalculator.multiply(ssrChrg.getLocalCurrReservationAmount(), exRate), boundaryValue,
						breakPointValue));
			}
			updateSSRCharge(affectedSSRList, userPrincipal);

		} catch (ModuleException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	private void updateSSRCharge(ArrayList<SSRCharge> ssrList, UserPrincipal userPrincipal) throws ModuleException {
		Iterator<SSRCharge> iterator = ssrList.iterator();
		try {
			while (iterator.hasNext()) {
				SSRCharge ssrCharge = iterator.next();
				chargedao.saveSSRChargeswithoutDeletingCos(ssrCharge);
				AuditAirpricing.doAuditAddModifySSRCharge(ssrCharge, userPrincipal); 
			}
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}

	}

}
