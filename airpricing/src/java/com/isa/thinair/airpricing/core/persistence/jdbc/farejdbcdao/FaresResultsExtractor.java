package com.isa.thinair.airpricing.core.persistence.jdbc.farejdbcdao;

/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airpricing.api.dto.ManageFareDTO;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * Class that JdbcTemplate will use.
 * 
 * @author Byorn
 * 
 */
public class FaresResultsExtractor implements ResultSetExtractor {

	private static final String FARE_ID = "fare_id";
	private static final String FARE_RULE_ID = "fare_rule_id";
	private static final String FARE_BASIS_CODE = "fare_basis_code";
	private static final String EFFECTIVE_FROM_DATE = "effective_from_date";
	private static final String EFFECTIVE_TO_DATE = "effective_to_date";
	private static final String FARE_AMOUNT = "fare_amount";
	private static final String BOOKING_CODE = "booking_code";
	private static final String CABIN_CLASS_CODE = "cabin_class_code";
	private static final String LOGICAL_CABIN_CLASS_CODE = "logical_cabin_class_code";
	private static final String RETURN_FLAG = "return_flag";
	private static final String FIXED_FLAG = "fixed_flag";
	private static final String STANDARD_CODE = "standard_code";
	private static final String OND_CODE = "ond_code";
	private static final String SALES_CHANNEL_CODE = "sales_channel_code";
	private static final String STATUS = "status";
	private static final String SALES_EFFECTIVE_FROM = "sales_valid_from";
	private static final String SALES_EFFECTIVE_TO = "sales_valid_to";
	private static final String RT_EFFECTIVE_FROM_DATE = "rt_effective_from_date";
	private static final String RT_EFFECTIVE_TO_DATE = "rt_effective_to_date";
	private static final String FARE_DEF_TYPE = "fare_def_type";
	private static final String MASTER_FARE_ID = "master_fare_id";
	private static final String MASTER_FARE_PERCENTAGE = "master_fare_percentage";
	private static final String MASTER_FARE_REF_CODE = "master_fare_ref_code";
	private static final String MODIFY_TO_SAME_FARE = "modify_to_same_fare";
	private static final String PAX_CATEGORY_CODE = "pax_category_code";
	private static final String CURRENCY_CODE = "currency_code";

	// private static final String REFUNDABLE_FLAG="refundable_flag";
	private static final String VERSION = "version";
	private static List<ManageFareDTO> list;

	/**
	 * Will return the List of Records extracted from the resultsSet List
	 * 
	 * @return
	 */
	public static List<ManageFareDTO> getList() {
		return list;
	}

	/**
	 * Method that implements the ResultSetExtractor interface. This method is internally used by the JdbcTemplate.
	 * Method is used for accessing the resultSet and populating the DTO and adding it to the results LIst.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

		list = new ArrayList<ManageFareDTO>();

		ManageFareDTO manageFareDTO = null;

		while (rs.next()) {
			/** Check for first Record **/
			if (manageFareDTO == null) {
				manageFareDTO = new ManageFareDTO();
				manageFareDTO.setFareId(rs.getInt(FARE_ID));
				manageFareDTO.setFareRuleId(rs.getInt(FARE_RULE_ID));
				manageFareDTO.setFareBasisCode(rs.getString(FARE_BASIS_CODE));
				manageFareDTO.setValidFrom(rs.getDate(EFFECTIVE_FROM_DATE));
				manageFareDTO.setValidTo(rs.getDate(EFFECTIVE_TO_DATE));
				manageFareDTO.setFareAmount(rs.getDouble(FARE_AMOUNT));
				manageFareDTO.setBookingClass(rs.getString(BOOKING_CODE).toString());
				manageFareDTO.setStandardCode(rs.getString(STANDARD_CODE).toString());

				if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
					manageFareDTO.setClassOfService(rs.getString(LOGICAL_CABIN_CLASS_CODE).toString());
				} else {
					manageFareDTO.setClassOfService(rs.getString(CABIN_CLASS_CODE).toString());
				}

				manageFareDTO.setReturnFlag(rs.getString(RETURN_FLAG).toString());
				manageFareDTO.setFixedAllocation(rs.getString(FIXED_FLAG).toString());
				manageFareDTO.setOndCode(rs.getString(OND_CODE).toString());
				manageFareDTO.setSalesValidFrom(rs.getDate(SALES_EFFECTIVE_FROM));
				manageFareDTO.setSalesValidTo(rs.getDate(SALES_EFFECTIVE_TO));
				manageFareDTO.setReturnValidFrom(rs.getDate(RT_EFFECTIVE_FROM_DATE));
				manageFareDTO.setReturnValidTo(rs.getDate(RT_EFFECTIVE_TO_DATE));
				Set<Integer> channels = new HashSet();
				channels.add(rs.getInt(SALES_CHANNEL_CODE));
				manageFareDTO.setChannelVisibilities(channels);
				manageFareDTO.setStatus(rs.getString(STATUS));
				// manageFareDTO.setRefundableFlag(rs.getString(REFUNDABLE_FLAG));
				manageFareDTO.setVersion(rs.getInt(VERSION));
				manageFareDTO.setMasterFareID(rs.getInt(MASTER_FARE_ID));
				manageFareDTO.setMasterFarePercentage(rs.getInt(MASTER_FARE_PERCENTAGE));
				manageFareDTO.setMasterFareStatus(rs.getString(FARE_DEF_TYPE));
				manageFareDTO.setMasterFareRefCode(rs.getString(MASTER_FARE_REF_CODE));
				manageFareDTO.setModToSameFare(rs.getString(MODIFY_TO_SAME_FARE));
				manageFareDTO.setPaxCategoryCode(rs.getString(PAX_CATEGORY_CODE));
				manageFareDTO.setCurrencyCode(rs.getString(CURRENCY_CODE));
				list.add(manageFareDTO);
				continue;
			}

			int newFareID = rs.getInt(FARE_ID);
			int prevFareID = manageFareDTO.getFareId();
			if (newFareID == prevFareID) {
				ManageFareDTO manageFareDTO2 = (ManageFareDTO) list.get(list.lastIndexOf(manageFareDTO));
				list.remove(list.lastIndexOf(manageFareDTO));
				Set<Integer> channels = manageFareDTO.getChannelVisibilities();
				channels.add(rs.getInt(SALES_CHANNEL_CODE));
				list.add(manageFareDTO2);
				continue;
			}

			/** if code comes here its a new fareID and not the first record **/
			manageFareDTO = new ManageFareDTO();
			manageFareDTO.setFareId(rs.getInt(FARE_ID));
			manageFareDTO.setFareRuleId(rs.getInt(FARE_RULE_ID));
			manageFareDTO.setFareBasisCode(rs.getString(FARE_BASIS_CODE));
			manageFareDTO.setValidFrom(rs.getDate(EFFECTIVE_FROM_DATE));
			manageFareDTO.setValidTo(rs.getDate(EFFECTIVE_TO_DATE));
			manageFareDTO.setFareAmount(rs.getDouble(FARE_AMOUNT));
			manageFareDTO.setBookingClass(rs.getString(BOOKING_CODE).toString());
			manageFareDTO.setStandardCode(rs.getString(STANDARD_CODE).toString());
			if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
				manageFareDTO.setClassOfService(rs.getString(LOGICAL_CABIN_CLASS_CODE).toString());
			} else {
				manageFareDTO.setClassOfService(rs.getString(CABIN_CLASS_CODE).toString());
			}
			manageFareDTO.setReturnFlag(rs.getString(RETURN_FLAG).toString());
			manageFareDTO.setFixedAllocation(rs.getString(FIXED_FLAG).toString());
			manageFareDTO.setOndCode(rs.getString(OND_CODE).toString());
			manageFareDTO.setSalesValidFrom(rs.getDate(SALES_EFFECTIVE_FROM));
			manageFareDTO.setSalesValidTo(rs.getDate(SALES_EFFECTIVE_TO));
			manageFareDTO.setReturnValidFrom(rs.getDate(RT_EFFECTIVE_FROM_DATE));
			manageFareDTO.setReturnValidTo(rs.getDate(RT_EFFECTIVE_TO_DATE));
			manageFareDTO.setMasterFareID(rs.getInt(MASTER_FARE_ID));
			manageFareDTO.setMasterFarePercentage(rs.getInt(MASTER_FARE_PERCENTAGE));
			manageFareDTO.setMasterFareStatus(rs.getString(FARE_DEF_TYPE));
			manageFareDTO.setMasterFareRefCode(rs.getString(MASTER_FARE_REF_CODE));
			manageFareDTO.setModToSameFare(rs.getString(MODIFY_TO_SAME_FARE));
			manageFareDTO.setPaxCategoryCode(rs.getString(PAX_CATEGORY_CODE));
			manageFareDTO.setCurrencyCode(rs.getString(CURRENCY_CODE));

			Set<Integer> channels = new HashSet<Integer>();
			channels.add(rs.getInt(SALES_CHANNEL_CODE));
			manageFareDTO.setChannelVisibilities(channels);
			manageFareDTO.setStatus(rs.getString(STATUS));
			// manageFareDTO.setRefundableFlag(rs.getString(REFUNDABLE_FLAG));
			manageFareDTO.setVersion(rs.getInt(VERSION));
			list.add(manageFareDTO);

		}
		return manageFareDTO;
	}
}