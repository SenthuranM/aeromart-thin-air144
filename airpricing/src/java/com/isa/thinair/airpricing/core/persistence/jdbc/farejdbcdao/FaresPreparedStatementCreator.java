/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airpricing.core.persistence.jdbc.farejdbcdao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.springframework.jdbc.core.PreparedStatementCreator;

import com.isa.thinair.airpricing.api.criteria.FareSearchCriteria;

/**
 * @author Byorn
 * 
 */
public class FaresPreparedStatementCreator implements PreparedStatementCreator {

	private String sql;
	private int paramToIndex;
	private int paramFromIndex;

	/**
	 * Constructor
	 * 
	 * @param agentFareRuleSearchCriteria
	 * @param sql
	 */
	public FaresPreparedStatementCreator(FareSearchCriteria fareSearchCriteria, String sql, int fromIndex, int toIndex) {

		this.sql = sql;
		this.paramToIndex = toIndex;
		this.paramFromIndex = fromIndex;

	}

	/**
	 * Method that will attach the parameter to the sql and will parse the preparedStatement
	 * 
	 * @param Connection
	 * @return PreparedStatement
	 */
	public PreparedStatement createPreparedStatement(Connection con) throws SQLException {

		PreparedStatement pstat = con.prepareStatement(sql);
		pstat.setInt(1, paramFromIndex);
		pstat.setInt(2, paramToIndex);

		return pstat;

	}

}
