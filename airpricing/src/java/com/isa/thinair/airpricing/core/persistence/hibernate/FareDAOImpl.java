/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:12:34
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.airpricing.core.persistence.hibernate;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;

import com.isa.thinair.airpricing.api.model.Fare;
import com.isa.thinair.airpricing.api.model.FareRule;
import com.isa.thinair.airpricing.core.persistence.dao.FareDAO;
import com.isa.thinair.airpricing.core.util.PricingUtils;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;

/**
 * BHive: FareRule module
 * 
 * @author BYORN
 * 
 *         FareDAOImpl is the FareDAO implementatiion for hibernate
 * @isa.module.dao-impl dao-name="FareDAO"
 */

public class FareDAOImpl extends PlatformBaseHibernateDaoSupport implements FareDAO {

	private final Log log = LogFactory.getLog(FareDAOImpl.class);

	/**
	 * Method to Save a Fare.
	 */
	@Override
	public void saveFare(Fare fare) {
		if (log.isDebugEnabled()) {
			log.debug("saving fare for fareid" + fare.getFareId());
		}
		hibernateSaveOrUpdate(fare);

	}

	@Override
	public void saveOrUpdateFare(Collection<Fare> fares) {
		hibernateSaveOrUpdateAll(fares);
		if (log.isDebugEnabled()) {
			log.debug("Finished saveOrUpdate");
		}
	}

	/**
	 * Method to delete a Fare.
	 */
	@Override
	public void deleteFare(int fareId) {
		if (log.isDebugEnabled()) {
			log.debug("deleteing fare for fareid" + fareId);
		}
		delete(new Integer(fareId));

	}

	/**
	 * Method will return true if fareRule, bookingCode and OndCode combination exists.
	 */
	@Override
	public boolean isFareLegal(String fareRuleCode, String bookingCode, String ONDCode, Date efctFrom, Date efctTo,
			Date salesEfctFrom, Date salesEfctTo, Date returnFromDate, Date returnToDate, boolean returnFlag) {

		String Hql = "";
		int count = 0;

		Hql = "select count(distinct fare.fareId) from Fare fare, FareRule farerule "
				+ " where fare.fareRuleId=farerule.fareRuleId "
				+ " and farerule.fareRuleCode=:fareRuleCode "
				+ " and fare.ondCode=:ondcode and fare.bookingCode=:bookingcode "

				// /////////////////////
				// ////////////////////
				+ " and ((fare.salesEffectiveFrom>=:saleseffectivefrom and "
				+ " fare.salesEffectiveTo>=:saleseffectiveto and fare.salesEffectiveFrom<=:saleseffectiveto) or "
				+ " (fare.salesEffectiveFrom<=:saleseffectivefrom and "
				+ " fare.salesEffectiveTo<=:saleseffectiveto and fare.salesEffectiveTo>=:saleseffectivefrom) or"
				+ " (fare.salesEffectiveFrom<=:saleseffectivefrom and "
				+ " fare.salesEffectiveTo>=:saleseffectiveto) or"
				+ " (fare.salesEffectiveFrom>=:saleseffectivefrom and "
				+ " fare.salesEffectiveTo<=:saleseffectiveto)) "

				// CHeck for Out Bound Dates
				+ " AND  ((fare.effectiveFromDate>=:effectivefromdate and "
				+ " fare.effectiveToDate>=:effectivetodate and fare.effectiveFromDate<=:effectivetodate) or "
				+ " (fare.effectiveFromDate<=:effectivefromdate and "
				+ " fare.effectiveToDate<=:effectivetodate and fare.effectiveToDate>=:effectivefromdate) or"
				+ " (fare.effectiveFromDate<=:effectivefromdate and " + " fare.effectiveToDate>=:effectivetodate) or"
				+ " (fare.effectiveFromDate>=:effectivefromdate and " + " fare.effectiveToDate<=:effectivetodate)) ";

		if (returnFlag && returnFromDate != null && returnToDate != null) {
			// Check For In bound Date
			Hql += " AND  ((fare.returnEffectiveFromDate>=:returnFromDate and "
					+ " fare.returnEffectiveToDate>=:returnToDate and " + "	fare.returnEffectiveFromDate<=:returnToDate ) "
					+ " OR (fare.returnEffectiveFromDate<=:returnFromDate and	"
					+ "	fare.returnEffectiveToDate<=:returnToDate and " + "	fare.returnEffectiveToDate>=:returnFromDate )"
					+ "	OR (fare.returnEffectiveFromDate<=:returnFromDate and  " + "	fare.returnEffectiveToDate>=:returnToDate)"
					+ "	OR (fare.returnEffectiveFromDate>=:returnFromDate and  " + "	fare.returnEffectiveToDate<=:returnToDate)"
					+ "	)	";

			count = ((Long) getSession().createQuery(Hql).setParameter("fareRuleCode", fareRuleCode)
					.setParameter("bookingcode", bookingCode).setParameter("ondcode", ONDCode)
					.setParameter("effectivefromdate", efctFrom).setParameter("effectivetodate", efctTo)
					.setParameter("saleseffectivefrom", salesEfctFrom).setParameter("saleseffectiveto", salesEfctTo)
					.setParameter("returnFromDate", returnFromDate).setParameter("returnToDate", returnToDate).uniqueResult())
					.intValue();
		} else {

			// System.out.println(Hql);
			count = ((Long) getSession().createQuery(Hql).setParameter("fareRuleCode", fareRuleCode)
					.setParameter("bookingcode", bookingCode).setParameter("ondcode", ONDCode)
					.setParameter("effectivefromdate", efctFrom).setParameter("effectivetodate", efctTo)
					.setParameter("saleseffectivefrom", salesEfctFrom).setParameter("saleseffectiveto", salesEfctTo)
					.uniqueResult()).intValue();
		}

		return count == 0;
	}

	/**
	 * method to getFare
	 * 
	 * @param fareID
	 * @return Fare
	 */
	@Override
	public Fare getFare(int fareID) {
		log.debug("getting fare for fareid" + fareID);
		return (Fare) get(Fare.class, new Integer(fareID));

	}

	@Override
	public Map<Integer, Fare> getFares(Collection<Integer> fareIds) {
		Map<Integer, Fare> fareMap = new HashMap<Integer, Fare>();
		String hql = "SELECT fare FROM Fare fare WHERE fare.fareId in ( :fareIds )";
		Query query = getSession().createQuery(hql);
		query.setParameterList("fareIds", fareIds);
		@SuppressWarnings("unchecked")
		Collection<Fare> fares = query.list();
		for (Fare fare : fares) {
			fareMap.put(fare.getFareId(), fare);
		}
		return fareMap;
	}

	/**
	 * Method will return true if fareRule, bookingCode and OndCode combination exists elsewhere except for that
	 * particular fareId
	 */
	@Override
	public boolean isFareLegal(String fareRuleCode, String bookingCode, String ONDCode, Integer fareID, Date efctFrom,
			Date efctTo, Date salesEfctFrom, Date salesEfctTo, Date returnFromDate, Date returnToDate, boolean returnFlag) {

		String Hql = "";
		int count = 0;

		Hql = "select count(distinct fare.fareId) from Fare fare, FareRule farerule "
				+ " where fare.fareRuleId=farerule.fareRuleId and "
				+ " farerule.fareRuleCode=:fareRuleCode and fare.ondCode=:ondcode and fare.bookingCode=:bookingcode and fare.fareId !=:fareid "

				// Check for Effective Selective Date
				+ " AND ((fare.salesEffectiveFrom>=:saleseffectivefrom and "
				+ " fare.salesEffectiveTo>=:saleseffectiveto and fare.salesEffectiveFrom<=:saleseffectiveto) or "
				+ " (fare.salesEffectiveFrom<=:saleseffectivefrom and "
				+ " fare.salesEffectiveTo<=:saleseffectiveto and fare.salesEffectiveTo>=:saleseffectivefrom) or"
				+ " (fare.salesEffectiveFrom<=:saleseffectivefrom and "
				+ " fare.salesEffectiveTo>=:saleseffectiveto) or"
				+ " (fare.salesEffectiveFrom>=:saleseffectivefrom and "
				+ " fare.salesEffectiveTo<=:saleseffectiveto)) "

				// CHECK For OUT Bound Date
				+ " AND ((fare.effectiveFromDate>=:effectivefromdate and "
				+ " fare.effectiveToDate>=:effectivetodate and fare.effectiveFromDate<=:effectivetodate) or "
				+ " (fare.effectiveFromDate<=:effectivefromdate and "
				+ " fare.effectiveToDate<=:effectivetodate and fare.effectiveToDate>=:effectivefromdate) or"
				+ " (fare.effectiveFromDate<=:effectivefromdate and " + " fare.effectiveToDate>=:effectivetodate) or"
				+ " (fare.effectiveFromDate>=:effectivefromdate and " + " fare.effectiveToDate<=:effectivetodate)) ";

		if (returnFlag && returnFromDate != null && returnToDate != null) {
			// Check For In bound Date
			Hql += " AND  ((fare.returnEffectiveFromDate>=:returnFromDate and "
					+ " fare.returnEffectiveToDate>=:returnToDate and " + "	fare.returnEffectiveFromDate<=:returnToDate ) "
					+ " OR (fare.returnEffectiveFromDate<=:returnFromDate and	"
					+ "	fare.returnEffectiveToDate<=:returnToDate and " + "	fare.returnEffectiveToDate>=:returnFromDate )"
					+ "	OR (fare.returnEffectiveFromDate<=:returnFromDate and  " + "	fare.returnEffectiveToDate>=:returnToDate)"
					+ "	OR (fare.returnEffectiveFromDate>=:returnFromDate and  " + "	fare.returnEffectiveToDate<=:returnToDate)"
					+ "	)	";

			count = ((Long) getSession().createQuery(Hql).setParameter("fareRuleCode", fareRuleCode)
					.setParameter("bookingcode", bookingCode).setParameter("ondcode", ONDCode).setParameter("fareid", fareID)
					.setParameter("effectivefromdate", efctFrom).setParameter("effectivetodate", efctTo)
					.setParameter("saleseffectivefrom", salesEfctFrom).setParameter("saleseffectiveto", salesEfctTo)
					.setParameter("returnFromDate", returnFromDate).setParameter("returnToDate", returnToDate).uniqueResult())
					.intValue();
		} else {

			// System.out.println(Hql);

			count = ((Long) getSession().createQuery(Hql).setParameter("fareRuleCode", fareRuleCode)
					.setParameter("bookingcode", bookingCode).setParameter("ondcode", ONDCode).setParameter("fareid", fareID)
					.setParameter("effectivefromdate", efctFrom).setParameter("effectivetodate", efctTo)
					.setParameter("saleseffectivefrom", salesEfctFrom).setParameter("saleseffectiveto", salesEfctTo)
					.uniqueResult()).intValue();
		}

		return count == 0;

	}

	/**
	 * Method will return list of fares if fareRule, bookingCode and OndCode combination exists.
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<Object[]> getFare(String fareRuleCode, String bookingCode, String ONDCode, Date efctFrom, Date efctTo,
			Date salesEfctFrom, Date salesEfctTo, Date returnFromDate, Date returnToDate) {

		String Hql = "";
		List<Object[]> list = null;

		Hql = "select fare.fareId, fare.version from Fare fare, FareRule farerule "
				+ " where fare.fareRuleId=farerule.fareRuleId "
				+ " and farerule.fareRuleCode=:farerulecode "
				+ " and fare.ondCode=:ondcode and fare.bookingCode=:bookingcode "

				// /////////////////////
				// ////////////////////
				+ " and ((fare.salesEffectiveFrom>=:saleseffectivefrom and "
				+ " fare.salesEffectiveTo>=:saleseffectiveto and fare.salesEffectiveFrom<=:saleseffectiveto) or "
				+ " (fare.salesEffectiveFrom<=:saleseffectivefrom and "
				+ " fare.salesEffectiveTo<=:saleseffectiveto and fare.salesEffectiveTo>=:saleseffectivefrom) or"
				+ " (fare.salesEffectiveFrom<=:saleseffectivefrom and "
				+ " fare.salesEffectiveTo>=:saleseffectiveto) or"
				+ " (fare.salesEffectiveFrom>=:saleseffectivefrom and "
				+ " fare.salesEffectiveTo<=:saleseffectiveto)) "

				// CHeck for Out Bound Dates
				+ " AND  ((fare.effectiveFromDate>=:effectivefromdate and "
				+ " fare.effectiveToDate>=:effectivetodate and fare.effectiveFromDate<=:effectivetodate) or "
				+ " (fare.effectiveFromDate<=:effectivefromdate and "
				+ " fare.effectiveToDate<=:effectivetodate and fare.effectiveToDate>=:effectivefromdate) or"
				+ " (fare.effectiveFromDate<=:effectivefromdate and " + " fare.effectiveToDate>=:effectivetodate) or"
				+ " (fare.effectiveFromDate>=:effectivefromdate and " + " fare.effectiveToDate<=:effectivetodate)) ";

		if (returnFromDate != null && returnToDate != null) {
			// Check For In bound Date
			Hql += " AND  ((fare.returnEffectiveFromDate>=:returnFromDate and "
					+ " fare.returnEffectiveToDate>=:returnToDate and " + "	fare.returnEffectiveFromDate<=:returnToDate ) "
					+ " OR (fare.returnEffectiveFromDate<=:returnFromDate and	"
					+ "	fare.returnEffectiveToDate<=:returnToDate and " + "	fare.returnEffectiveToDate>=:returnFromDate )"
					+ "	OR (fare.returnEffectiveFromDate<=:returnFromDate and  " + "	fare.returnEffectiveToDate>=:returnToDate)"
					+ "	OR (fare.returnEffectiveFromDate>=:returnFromDate and  " + "	fare.returnEffectiveToDate<=:returnToDate)"
					+ "	)	";

			list = getSession().createQuery(Hql).setParameter("farerulecode", fareRuleCode)
					.setParameter("bookingcode", bookingCode).setParameter("ondcode", ONDCode)
					.setParameter("effectivefromdate", efctFrom).setParameter("effectivetodate", efctTo)
					.setParameter("saleseffectivefrom", salesEfctFrom).setParameter("saleseffectiveto", salesEfctTo)
					.setParameter("returnFromDate", returnFromDate).setParameter("returnToDate", returnToDate).list();
		} else {

			// System.out.println(Hql);
			list = getSession().createQuery(Hql).setParameter("farerulecode", fareRuleCode)
					.setParameter("bookingcode", bookingCode).setParameter("ondcode", ONDCode)
					.setParameter("effectivefromdate", efctFrom).setParameter("effectivetodate", efctTo)
					.setParameter("saleseffectivefrom", salesEfctFrom).setParameter("saleseffectiveto", salesEfctTo).list();
		}
		return list;
	}

	/**
	 * Method will return a collection of fare objects.
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Fare> getEffectiveFares(Collection<String> bookingCodes, String ondCode, Date depatureDate) {

		String inStr = PricingUtils.constructINString(bookingCodes);

		String strHql = "Select fare from Fare as fare where fare.ondCode=:ondcode and :departuredate between fare.effectiveFromDate and fare.effectiveToDate and fare.bookingCode in ("
				+ inStr + ") order by fare.bookingCode, fare.fareAmount";

		if (log.isDebugEnabled()) {
			log.debug("getting effective fares ondCode : " + ondCode + " departure date:" + depatureDate.toString());
		}

		return getSession().createQuery(strHql).setParameter("ondcode", ondCode).setParameter("departuredate", depatureDate)
				.list();

	}

	/**
	 * Method will return a collection of fare objects.
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Collection<Object[]> getEffectiveFaresWithFareRule(Collection<String> bookingCodes, String ondCode, Date depatureDate) {

		String inStr = PricingUtils.constructINString(bookingCodes);
		String strHql = "Select fare.bookingCode, farerule from Fare fare, FareRule farerule where fare.fareRuleId=farerule.fareRuleId  and fare.ondCode=:ondcode and :departuredate between fare.effectiveFromDate and fare.effectiveToDate and fare.bookingCode in ("
				+ inStr + ") order by fare.bookingCode, fare.fareAmount";

		if (log.isDebugEnabled()) {
			log.debug("getting effective fares ondCode : " + ondCode + " departure date:" + depatureDate.toString());
		}

		return getSession().createQuery(strHql).setParameter("ondcode", ondCode).setParameter("departuredate", depatureDate)
				.list();

	}

	/**
	 * Return fare rules linked to specified fares.
	 * 
	 * @param fareIds
	 * @return
	 */
	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public HashMap<Integer, FareRule> getFareRules(Collection<Integer> fareIds) {
		HashMap fareRulesMap = new HashMap();

		String hql = "SELECT f.fareId, fr FROM Fare f, FareRule fr WHERE f.fareRuleId = fr.fareRuleId "
				+ " AND f.fareId in ( :fareIds )";

		Query q = getSession().createQuery(hql);
		q.setParameterList("fareIds", fareIds);
		Collection<Object[]> fareRulesCol = q.list();

		Iterator fareRuleIt = fareRulesCol.iterator();
		while (fareRuleIt.hasNext()) {
			Object[] result = (Object[]) fareRuleIt.next();
			fareRulesMap.put(result[0], result[1]);
		}

		return fareRulesMap;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Collection<Fare> getFaresWithLocalCurrency(Date effectiveDate) {

		String hql = "SELECT fare FROM Fare as fare " + " WHERE :effDate BETWEEN fare.effectiveFromDate AND "
				+ " fare.effectiveToDate AND fare.fareAmountInLocal IS NOT NULL ";

		Query hQuery = getSession().createQuery(hql);

		hQuery.setTimestamp("effDate", effectiveDate);

		List<Fare> resultsList = hQuery.list();

		return resultsList;

	}

	/**
	 * Return fare rules linked to specified fares.
	 * 
	 * @param fareIds
	 * @return
	 */
	@Override
	@SuppressWarnings("unchecked")
	public HashMap<Integer, String> getMasterFares(String ondCode) {
		HashMap<Integer, String> fareInfo = new HashMap<Integer, String>();

		String hql = "SELECT f.fareId,f.masterFareRefCode FROM Fare f " + " WHERE f.ondCode=:ondcode "
				+ " AND f.fareDefType=:faredeftype";

		Query q = getSession().createQuery(hql);
		q.setParameter("ondcode", ondCode);
		q.setParameter("faredeftype", Fare.FARE_DEF_TYPE_MASTER);
		Collection<Object[]> faresList = q.list();

		Iterator<Object[]> fare = faresList.iterator();
		while (fare.hasNext()) {
			Object[] result = fare.next();
			fareInfo.put((Integer) result[0], (String) result[1]);
		}

		return fareInfo;
	}

	@Override
	@SuppressWarnings("rawtypes")
	public boolean isMasterFareLinked(Integer masterFareID) {
		String hql = "SELECT f.fareId FROM Fare f " + " WHERE f.masterFareID=:masterfareid " + " AND f.fareDefType=:faredeftype";

		Query q = getSession().createQuery(hql);
		q.setParameter("masterfareid", masterFareID);
		q.setParameter("faredeftype", Fare.FARE_DEF_TYPE_LINK);
		Collection faresList = q.list();

		if (faresList != null && faresList.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	@SuppressWarnings("rawtypes")
	public boolean isMasterFareRefCodeAlreayUsed(String masterFareRefCode) {
		String hql = "SELECT f.masterFareRefCode FROM Fare f " + " WHERE f.masterFareRefCode=:masterfarerefcode "
				+ " AND f.fareDefType=:faredeftype";

		Query q = getSession().createQuery(hql);
		q.setParameter("masterfarerefcode", masterFareRefCode);
		q.setParameter("faredeftype", Fare.FARE_DEF_TYPE_MASTER);
		Collection faresList = q.list();

		if (faresList != null && faresList.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public Collection<Fare> getLinkFares(Integer masterFareID) {
		String hql = "Select fare from Fare as fare where fare.masterFareID=:masterfareid and fare.fareDefType=:faredeftype";

		Query q = getSession().createQuery(hql);
		q.setParameter("masterfareid", masterFareID);
		q.setParameter("faredeftype", Fare.FARE_DEF_TYPE_LINK);
		Collection<Fare> faresList = q.list();

		return faresList;
	}

	@Override
	public List<Fare> getApplicableFaresForUpdateOnExchangeRateChange(String currencyCode) {
		String hql = "Select fare from Fare as fare where fare.salesEffectiveTo >=:currentDate and fare.localCurrencyCode=:currencyCode";
		Query q = getSession().createQuery(hql);
		q.setParameter("currencyCode", currencyCode);
		q.setTimestamp("currentDate", new Date());
		return q.list();
	}

	@SuppressWarnings("unchecked")
	public Collection<Fare> getFaresByFareRuleId(Integer fareRuleId) {

		String hql = "FROM Fare  WHERE fareRuleId =:fareruleid";
		Query hQuery = getSession().createQuery(hql);
		hQuery.setInteger("fareruleid", fareRuleId);
		Collection<Fare> resultsList = hQuery.list();
		return resultsList;

	}
}
