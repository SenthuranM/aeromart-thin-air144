package com.isa.thinair.airpricing.core.persistence.dao;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import com.isa.thinair.airpricing.api.dto.FlexiQuoteCriteria;
import com.isa.thinair.airpricing.api.dto.FlexiRuleDTO;

/**
 * BHive Commons: FlexiRule module
 * 
 * @author Dhanya
 */

public interface FlexiRuleJdbcDAO {

	/**
	 * Flexi quote for a given colCriteria
	 * 
	 * @param colCriteria
	 * @return
	 */
	public HashMap<String, HashMap<String, FlexiRuleDTO>> quoteFlexiCharges(Collection<FlexiQuoteCriteria> colCriteria);

	/**
	 * rebuild felix for a given flexi ids
	 * 
	 * @param flexiIdCollection
	 * @param criteria
	 * @return
	 */
	public HashMap<String, FlexiRuleDTO> rebuildFlexiCharges(Collection<Integer> flexiIdCollection, FlexiQuoteCriteria criteria);

	public List<FlexiRuleDTO> requoteFlexiCharges(Collection<Integer> flexiRuleRateIds);
}
