package com.isa.thinair.airpricing.core.persistence.jdbc.ssrchargejdbcdao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airpricing.api.dto.SSRChargeDTO;
import com.isa.thinair.commons.api.constants.CommonsConstants;

public class SSRChargeResultSetExtractor implements ResultSetExtractor {

	@Override
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
		Collection<SSRChargeDTO> ssrChargeDTOs = new ArrayList<SSRChargeDTO>();

		while (rs.next()) {
			String ondCode = rs.getString("ond_code");
			String ssrChargeCode = rs.getString("ssr_charge_code");

			SSRChargeDTO ssrChargeDTO = new SSRChargeDTO();

			ssrChargeDTO.setSSRChargeId(rs.getInt("ssr_charge_id"));
			ssrChargeDTO.setSSRChargeCode(ssrChargeCode);
			ssrChargeDTO.setONDCode(ondCode);

			ssrChargeDTO.setSSRId(rs.getInt("ssr_id"));
			ssrChargeDTO.setSSRCode(rs.getString("ssr_code"));
			ssrChargeDTO.setSSRSubCategoryId(rs.getInt("ssr_sub_cat_id"));
			ssrChargeDTO.setDescription(rs.getString("description"));
			ssrChargeDTO.setChargeAmount(rs.getBigDecimal("charge_amount"));

			ssrChargeDTO.setApplyForAdult(CommonsConstants.YES.equals(rs.getString("apply_adt")));
			ssrChargeDTO.setApplyForChild(CommonsConstants.YES.equals(rs.getString("apply_chd")));
			ssrChargeDTO.setApplyForInfant(CommonsConstants.YES.equals(rs.getString("apply_inf")));
			ssrChargeDTO.setApplyForArrival(CommonsConstants.YES.equals(rs.getString("apply_arrival")));
			ssrChargeDTO.setApplyForDeparture(CommonsConstants.YES.equals(rs.getString("apply_departure")));
			ssrChargeDTO.setApplyForTransit(CommonsConstants.YES.equals(rs.getString("apply_transit")));
			ssrChargeDTO.setApplyForSegment(CommonsConstants.YES.equals(rs.getString("apply_segment")));
			ssrChargeDTO.setMinimumTransitTime(rs.getInt("min_transit_time_in_mins"));
			ssrChargeDTO.setMaximumTransitTime(rs.getInt("max_transit_time_in_mins"));

			ssrChargeDTO.setClassOfService(rs.getString("cabin_class_code"));

			ssrChargeDTO.setMinimumTotalPax(rs.getInt("min_tot_pax"));
			ssrChargeDTO.setMaximumTotalPax(rs.getInt("max_tot_pax"));
			ssrChargeDTO.setMinimumAdultPax(rs.getInt("min_adt_pax"));
			ssrChargeDTO.setMaximumAdultPax(rs.getInt("max_adt_pax"));
			ssrChargeDTO.setMinimumChildPax(rs.getInt("min_chd_pax"));
			ssrChargeDTO.setMaximumChildPax(rs.getInt("max_chd_pax"));
			ssrChargeDTO.setMinimumInfantPax(rs.getInt("min_inf_pax"));
			ssrChargeDTO.setMaximumInfantPax(rs.getInt("max_inf_pax"));

			ssrChargeDTOs.add(ssrChargeDTO);
		}

		return ssrChargeDTOs;
	}
}
