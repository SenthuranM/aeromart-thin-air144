package com.isa.thinair.airpricing.core.persistence.dao;

import java.util.Map;

import com.isa.thinair.airpricing.api.criteria.DefaultAnciTemplSearchCriteria;
import com.isa.thinair.airpricing.api.dto.DefaultAnciTemplateTO;
import com.isa.thinair.airpricing.api.model.DefaultAnciTemplate;
import com.isa.thinair.commons.api.dto.Page;

public interface CommonAncillaryDAO {

	public Page<DefaultAnciTemplate> getdefaultAnciTemaplates(int startRec, int noRecs);

	public void saveDefaultAnciTemplate(DefaultAnciTemplate template);

	public void deleteDefaultAnciTemplate(DefaultAnciTemplate template);

	public Page<DefaultAnciTemplateTO> searchDefAnciTemplate(DefaultAnciTemplSearchCriteria criteria, int startRec, int noRecs);

	public boolean checkDuplicateDefaultAnciTemplateForSameRoute(DefaultAnciTemplateTO templateTO);

	public boolean checkTemplateAttachedToRouteWiseDefAnciTempl(DefaultAnciTemplate.ANCI_TEMPLATES anciType, int templateId);

	public Map<String,Integer> getDefaultAnciTemplateForFlight(String ondCode, String flightModel);
}