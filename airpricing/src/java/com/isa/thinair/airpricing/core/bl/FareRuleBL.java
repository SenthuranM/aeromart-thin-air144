package com.isa.thinair.airpricing.core.bl;

import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.isa.thinair.airpricing.api.model.Fare;
import com.isa.thinair.airpricing.api.model.FareRule;
import com.isa.thinair.airpricing.api.model.FareRulePax;
import com.isa.thinair.airpricing.api.service.AirpricingUtils;
import com.isa.thinair.airpricing.core.persistence.dao.FareDAO;
import com.isa.thinair.airpricing.core.persistence.dao.FareJdbcDAO;
import com.isa.thinair.airpricing.core.persistence.dao.FareRuleDAO;
import com.isa.thinair.airpricing.core.util.InternalConstants;
import com.isa.thinair.airpricing.core.util.PricingUtils;
import com.isa.thinair.airpricing.core.util.SplitFareBySalesUtil;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;

public class FareRuleBL {

	private FareRuleDAO fareRuleDAO;

	private FareDAO faredao;

	private FareJdbcDAO fareJdbcDAO;

	public FareRuleBL() {
		fareRuleDAO = (FareRuleDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FARE_RULE_DAO);
		faredao = (FareDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FARE_DAO);
		fareJdbcDAO = (FareJdbcDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.MANAGE_FARES);
	}

	public void saveFareRule(FareRule fareRule, UserPrincipal userPrincipal, boolean splitFares) throws ModuleException {

		PricingUtils.validateFareRule(fareRule);

		boolean updateMode = fareRule.getVersion() >= 0 ? true : false;

		fareRule = PricingUtils.clearAgentsListIfVisibilityIsNotAgent(fareRule);
		fareRule = PricingUtils.clearPOSListIfVisibilityNotWeb(fareRule);
		if (updateMode) {
			FareRule fareRuleUpdate = fareRuleDAO.getFareRule(fareRule.getFareRuleId());
			String createdBy = fareRuleUpdate.getCreatedBy();
			Date createdDate = fareRuleUpdate.getCreatedDate();
			fareRule.setCreatedBy(createdBy);
			fareRule.setCreatedDate(createdDate);
			if (fareRule.getPaxDetails() != null) {
				for (Object paxObj : fareRule.getPaxDetails()) {
					((FareRulePax) paxObj).setCreatedBy(createdBy);
					((FareRulePax) paxObj).setCreatedDate(createdDate);
				}
			}
		}

		if (fareRule.isLocalCurrencySelected()) {
			PricingUtils.setFareRuleChargesInSelectedCurr(fareRule);
		}

		try {
			if (updateMode && splitFares) {
				updateAssociatedFareDetails(fareRule.getFareRuleCode(), userPrincipal);
			}
			fareRuleDAO.saveFareRule(fareRule);
		} catch (CommonsDataAccessException e) {
			throw new ModuleException(e, e.getExceptionCode(), "airpricing.desc");
		}
	}

	private void updateAssociatedFareDetails(String fareRuleCode, UserPrincipal userPrincipal) throws ModuleException {

		boolean fareSplitEnabled = AppSysParamsUtil.isSplitFareSalesValidityEnabled();

		if (fareSplitEnabled) {
			List<FareRule> fareRulesList = fareRuleDAO.getFareRules(fareRuleCode);

			Iterator<FareRule> fareRulesItr = fareRulesList.iterator();

			while (fareRulesItr.hasNext()) {
				FareRule fareRule = fareRulesItr.next();
				Integer fareRuleId = fareRule.getFareRuleId();

				Collection<Fare> faresCollect = faredao.getFaresByFareRuleId(fareRuleId);

				if (faresCollect != null && faresCollect.size() > 0) {

					Iterator<Fare> faresItr = faresCollect.iterator();
					Date currentDate = new Date();
					while (faresItr.hasNext()) {
						Fare fare = faresItr.next();
						boolean fareAssignedForPnr = fareJdbcDAO.isFareAssignedForPnr(fare.getFareId());

						boolean isSalesEffectiveNotExpired = CalendarUtil.isGreaterThan(fare.getSalesEffectiveTo(), new Date());

						boolean isSalesEndToDay = CalendarUtil.isSameDay(currentDate, fare.getSalesEffectiveTo());

						if (fareAssignedForPnr && isSalesEffectiveNotExpired && !isSalesEndToDay) {

							FareRule newFareRule = SplitFareBySalesUtil.copyExistingFareRuleAsAdhoc(fareRuleId);

							SplitFareBySalesUtil.splitFares(fare.getFareId(), newFareRule.getFareRuleId(), currentDate, null,
									true, userPrincipal);

						} else if (fareAssignedForPnr && (isSalesEndToDay || !isSalesEffectiveNotExpired)) {
							FareRule newFareRule = SplitFareBySalesUtil.copyExistingFareRuleAsAdhoc(fareRuleId);

							SplitFareBySalesUtil.splitFares(fare.getFareId(), newFareRule.getFareRuleId(), currentDate, null,
									false, userPrincipal);

						}

					}

				}

			}
		}

	}
}
