package com.isa.thinair.airpricing.core.bl;

import com.isa.thinair.airpricing.api.criteria.ONDCodeSearchCriteria;
import com.isa.thinair.airpricing.api.dto.BaggageTemplateTo;
import com.isa.thinair.airpricing.api.model.ONDBaggageTemplate;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

public interface IBaggageBl {

	public void saveBaggage(ONDBaggageTemplate baggage);

	public Page<BaggageTemplateTo> searchBaggage(ONDCodeSearchCriteria criteria, int startRec, int noOfRecs)
			throws ModuleException;

	public void validateBaggageRules(ONDBaggageTemplate baggageTemplate) throws ModuleException;
}
