package com.isa.thinair.airpricing.core.persistence.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airpricing.api.criteria.FareSearchCriteria;
import com.isa.thinair.airpricing.api.dto.HourValidity;
import com.isa.thinair.airpricing.api.dto.LightFareDTO;
import com.isa.thinair.airpricing.api.dto.LightFareRuleDTO;
import com.isa.thinair.airpricing.api.dto.WeekFrequency;
import com.isa.thinair.airpricing.api.model.Fare;
import com.isa.thinair.airpricing.core.criteria.ApplicableFareSearchCriteria;
import com.isa.thinair.airpricing.core.persistence.dao.FareJdbcDAO;
import com.isa.thinair.airpricing.core.persistence.jdbc.farejdbcdao.FaresCount;
import com.isa.thinair.airpricing.core.persistence.jdbc.farejdbcdao.FaresPreparedStatementCreator;
import com.isa.thinair.airpricing.core.persistence.jdbc.farejdbcdao.FaresResultsExtractor;
import com.isa.thinair.airpricing.core.util.PricingUtils;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseJdbcDAOSupport;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.Util;

/**
 * @author BYORN
 */
public class FareJdbcDAOImpl extends PlatformBaseJdbcDAOSupport implements FareJdbcDAO {

	private static final String SQL_FARE = "fare";

	private static final String PAGE_ORDERBY = "pageorderby";

	private static final String SQL_COUNT = "countoffares";

	private static final String SQL_COUNT_ORDERBY = "countorderby";

	private static final String GET_AGENTS_LINKED_TO_BC = "GET_AGENTS_LINKED_TO_BC";

	private static final String SQL_SAME_RETURNGROUP_DETAILS = "samereturngrouppnrsegflightseg";

	private static final String GET_BOOKING_CODES_LINKED_TO_AGENT = "GET_BOOKING_CODES_LINKED_TO_AGENT";

	/** properties to store queries for combos * */
	private Properties queryString;

	private Log log = LogFactory.getLog(getClass());

	/**
	 * method to getFares
	 */
	@SuppressWarnings({ "static-access", "rawtypes" })
	public Page getFares(FareSearchCriteria fareSearchCriteria, int startIndex, int pageSize, List<String> orderBy)
			throws CommonsDataAccessException {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		FaresResultsExtractor extractor = new FaresResultsExtractor();

		int fromIndex = startIndex + 1;
		int toIndex = startIndex + pageSize;
		String sqlArray[] = constructSql(fareSearchCriteria);

		String sql = sqlArray[0];
		String countSql = sqlArray[1];
		try {
			jdbcTemplate.query(new FaresPreparedStatementCreator(fareSearchCriteria, sql, fromIndex, toIndex), extractor);

			List list = extractor.getList();

			FaresCount faresCount = new FaresCount(fareSearchCriteria, countSql, getDataSource());

			if (log.isDebugEnabled()) {
				log.debug("Get Page of Fares for criteria" + fareSearchCriteria.toString());
				log.debug(sql);
				log.debug(countSql);
			}
			return new Page(faresCount.getCountofFares(), startIndex, toIndex, list);
		} catch (Exception e) {
			log.error("Get Page of Fares for criteria Failed", e);
			throw new CommonsDataAccessException(e, "airpricing.exec.error", "airpricing.desc");
		}

	}

	/**
	 * constructSql String
	 * 
	 * @param fareSearchCriteria
	 * @return
	 */
	public String[] constructSql(FareSearchCriteria fareSearchCriteria) {
		String sql = getQuery(SQL_FARE);
		String sqlCount = getQuery(SQL_COUNT);
		String sqlCountOrderby = getQuery(SQL_COUNT_ORDERBY);

		String sortParam = " t_ond_fare.fare_id asc ";
		if (fareSearchCriteria.getSortingField() != null && fareSearchCriteria.getSortingOder() != null) {
			sortParam = fareSearchCriteria.getSortingField() + " " + fareSearchCriteria.getSortingOder();
		}

		if (fareSearchCriteria.getFareRuleCode() != null) {

			{
				String sqlFareRuleCode = " and (t_fare_rule.fare_rule_code like " + "'" + fareSearchCriteria.getFareRuleCode()
						+ "%')";
				sql = sql + sqlFareRuleCode;
				sqlCount = sqlCount + sqlFareRuleCode;
			}
		}

		if (fareSearchCriteria.getBookingClassCode() != null) {

			{
				String sqlFareBasisCode = " and (t_booking_class.booking_code like " + "'"
						+ fareSearchCriteria.getBookingClassCode() + "%')";
				sql = sql + sqlFareBasisCode;
				sqlCount = sqlCount + sqlFareBasisCode;
			}
		}

		if (fareSearchCriteria.getCabinClassCode() != null) {
			String sqlCabinClassCode = " and (t_booking_class.cabin_class_code = " + "'" + fareSearchCriteria.getCabinClassCode()
					+ "')";
			sql = sql + sqlCabinClassCode;
			sqlCount = sqlCount + sqlCabinClassCode;
		}

		if (fareSearchCriteria.getLogicalCabinClass() != null) {
			String sqlLogicalCCCode = " and (t_booking_class.logical_cabin_class_code = " + "'"
					+ fareSearchCriteria.getLogicalCabinClass() + "')";
			sql = sql + sqlLogicalCCCode;
			sqlCount = sqlCount + sqlLogicalCCCode;
		}

		if (fareSearchCriteria.getFareBasisCode() != null) {
			String sqlFareBasisCode = " and (t_fare_rule.fare_basis_code = " + "'" + fareSearchCriteria.getFareBasisCode() + "')";
			sql = sql + sqlFareBasisCode;
			sqlCount = sqlCount + sqlFareBasisCode;
		}

		if (fareSearchCriteria.getStatus() != null) {
			String sqlStatus = " and (t_ond_fare.status = " + "'" + fareSearchCriteria.getStatus() + "') ";
			sql = sql + sqlStatus;
			sqlCount = sqlCount + sqlStatus;
		}

		if (fareSearchCriteria.getOriginAirportCode() != null && fareSearchCriteria.getDestinationAirportCode() != null) {
			String ondCode = PricingUtils.constructViaPointSearch(fareSearchCriteria.getOriginAirportCode(),
					fareSearchCriteria.getDestinationAirportCode(), fareSearchCriteria.getViaAirportCodes(),
					fareSearchCriteria.isExactCombination());
			String sqlOndCode = " and (t_ond_fare.ond_code like " + "'" + ondCode + "')";
			sql = sql + sqlOndCode;
			sqlCount = sqlCount + sqlOndCode;
		}
		String columnName_DateFrom = "t_ond_fare.effective_from_date";
		String columnName_DateTo = "t_ond_fare.effective_to_date";
		if (fareSearchCriteria.isSearchForSalesPeriod()) {
			columnName_DateFrom = "t_ond_fare.sales_valid_from";
			columnName_DateTo = "t_ond_fare.sales_valid_to";
		}

		if (fareSearchCriteria.getFromDate() != null && fareSearchCriteria.getToDate() == null) {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
			String effectiveFromDate = simpleDateFormat.format(fareSearchCriteria.getFromDate());
			String sqlDateSearch = " AND ((" + columnName_DateFrom + ">=" + "'" + effectiveFromDate + "'" + " OR  "
					+ columnName_DateTo + ">=" + "'" + effectiveFromDate + "'))";
			sql = sql + sqlDateSearch;
			sqlCount = sqlCount + sqlDateSearch;

		}

		if (fareSearchCriteria.getFromDate() == null && fareSearchCriteria.getToDate() != null) {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
			String effectiveToDate = simpleDateFormat.format(fareSearchCriteria.getToDate());
			String sqlDateSearch = " AND ((" + columnName_DateFrom + "<=" + "'" + effectiveToDate + "'" + " OR  "
					+ columnName_DateTo + "<=" + "'" + effectiveToDate + "'))";

			sql = sql + sqlDateSearch;
			sqlCount = sqlCount + sqlDateSearch;
		}

		if (fareSearchCriteria.getFromDate() != null && fareSearchCriteria.getToDate() != null) {

			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
			String effectiveToDate = simpleDateFormat.format(fareSearchCriteria.getToDate());
			String effectiveFromDate = simpleDateFormat.format(fareSearchCriteria.getFromDate());

			String sqlDateSearch = " AND ((" + columnName_DateFrom + ">=" + "'" + effectiveFromDate + "'" + " AND  "
					+ columnName_DateTo + ">=" + "'" + effectiveToDate + "'" + " AND " + columnName_DateFrom + "<=" + "'"
					+ effectiveToDate + "')" + " or " + " (" + columnName_DateFrom + "<=" + "'" + effectiveFromDate + "'"
					+ " AND " + columnName_DateTo + "<=" + "'" + effectiveToDate + "'" + " AND " + columnName_DateTo + ">=" + "'"
					+ effectiveFromDate + "')" + " or " + " (" + columnName_DateFrom + "<=" + "'" + effectiveFromDate + "'"
					+ " and " + columnName_DateTo + ">=" + "'" + effectiveToDate + "')" + " or " + " (" + columnName_DateFrom
					+ ">=" + "'" + effectiveFromDate + "'" + " and " + columnName_DateTo + "<=" + "'" + effectiveToDate + "'))";
			sql = sql + sqlDateSearch;
			sqlCount = sqlCount + sqlDateSearch;
		}
		String mainSql = sql + " " + " order by " + sortParam + " ))t  where t.seq between ? and ? )) order by " + sortParam;
		String countSql = sqlCount + " " + sqlCountOrderby;

		return new String[] { mainSql, countSql };
	}

	@SuppressWarnings("unchecked")
	public HashMap<String, List<String>> getAgentsLinkedToBookingClass(Collection<String> bookingCodesCollection)
			throws ModuleException {
		final String query = getQuery(GET_AGENTS_LINKED_TO_BC, new String[] { getSqlInValuesStr(bookingCodesCollection) });
		JdbcTemplate templete = new JdbcTemplate(getDataSource());
		return (HashMap<String, List<String>>) templete.query(query, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				HashMap<String, List<String>> agentsLinkedToBCMap = new HashMap<String, List<String>>();
				while (rs.next()) {
					String bookingCode = rs.getString("booking_code");
					String agentCode = rs.getString("agent_code");
					if ((agentsLinkedToBCMap.size() == 0) || !agentsLinkedToBCMap.containsKey(bookingCode)) {
						agentsLinkedToBCMap.put(bookingCode, new ArrayList<String>());
					}
					((List<String>) agentsLinkedToBCMap.get(bookingCode)).add(agentCode);
				}
				return agentsLinkedToBCMap;
			}
		});
	}

	@SuppressWarnings("unchecked")
	public Collection<String> getBookingCodesLinkedToAgent(String agentCode) {
		final String query = getQuery(GET_BOOKING_CODES_LINKED_TO_AGENT);
		JdbcTemplate templete = new JdbcTemplate(getDataSource());
		return (Collection<String>) templete.query(query, new String[] { agentCode }, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<String> bookingCodesLinkedToAgent = new ArrayList<String>();
				while (rs.next()) {
					bookingCodesLinkedToAgent.add(rs.getString("booking_code"));

				}
				return bookingCodesLinkedToAgent;
			}
		});
	}

	/**
	 * 
	 * Properties
	 * 
	 * @return
	 */
	public Properties getQueryString() {
		return queryString;
	}

	public void setQueryString(Properties queryString) {
		this.queryString = queryString;
	}

	public boolean isFareAssignedForPnr(Integer fareId) {
		StringBuilder sql = new StringBuilder();

		sql.append("SELECT count(*) as cnt FROM t_pnr_pax_fare WHERE fare_id=" + fareId);

		JdbcTemplate template = new JdbcTemplate(getDataSource());

		Integer recs = (Integer) template.query(sql.toString(), new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					return new Integer(rs.getInt("cnt"));
				}
				return null;

			}
		});

		if (recs != null && recs.intValue() > 0) {
			return true;
		} else {
			return false;
		}

	}

	public boolean isFareRuleAssociatedWithPnr(Integer fareRuleId) {
		StringBuilder sql = new StringBuilder();

		sql.append("SELECT COUNT(*) AS cnt FROM t_pnr_pax_fare ppf,t_ond_fare f WHERE ppf.fare_id =f.fare_id AND f.fare_rule_id="
				+ fareRuleId);

		JdbcTemplate template = new JdbcTemplate(getDataSource());

		Integer recs = (Integer) template.query(sql.toString(), new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					return new Integer(rs.getInt("cnt"));
				}
				return null;

			}
		});

		if (recs != null && recs.intValue() > 0) {
			return true;
		} else {
			return false;
		}

	}

	public boolean isModifyToSameFareEnabled(Integer fareId) {
		StringBuilder sql = new StringBuilder();

		sql.append("SELECT ONDF.MODIFY_TO_SAME_FARE FROM T_OND_FARE ONDF WHERE ONDF.FARE_ID =" + fareId);

		JdbcTemplate template = new JdbcTemplate(getDataSource());

		String modToSameFare = (String) template.query(sql.toString(), new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					return new String(rs.getString("MODIFY_TO_SAME_FARE"));
				}
				return null;

			}
		});

		if (modToSameFare != null && modToSameFare.equals(Fare.MODIFY_TO_SAME_FARE_YES)) {
			return true;
		} else {
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	public Map<Integer, Integer> getSameReturnGroupSegDetails(Collection<Integer> dateChangedResSegList) {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());
		String strPnrSegSql = "SELECT * FROM T_PNR_SEGMENT PS  WHERE " + " PS.STATUS = 'CNF' "
				+ " AND (PS.PNR, PS.RETURN_GROUP_ID) IN (SELECT PS1.PNR,PS1.RETURN_GROUP_ID "
				+ " FROM T_PNR_SEGMENT PS1 WHERE PS1.PNR_SEG_ID IN(" + Util.buildIntegerInClauseContent(dateChangedResSegList)
				+ "))  ";

		Map<Integer, Integer> pnrSegIdWiseFltSegId = (Map<Integer, Integer>) jt.query(strPnrSegSql, new Object[] {},
				new ResultSetExtractor() {
					Map<Integer, Integer> pnrSegIdWiseFltSegId = new HashMap<Integer, Integer>();

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						while (rs.next()) {
							Integer pnrSegId = rs.getInt("pnr_seg_id");
							Integer fltSegId = rs.getInt("flt_seg_id");
							pnrSegIdWiseFltSegId.put(pnrSegId, fltSegId);

						}
						return pnrSegIdWiseFltSegId;
					}

				});
		return pnrSegIdWiseFltSegId;
	}

	@Override
	public List<LightFareDTO> getApplicableFares(ApplicableFareSearchCriteria searchCriteria) {
		JdbcTemplate jt = new JdbcTemplate(ReservationModuleUtils.getDatasource());

		String fareQuery = "SELECT fare.fare_id, fare.booking_code, fare.OND_CODE, fare.FARE_AMOUNT, fare.CHILD_FARE, "
				+ " fare.CHILD_FARE_TYPE, fare.INFANT_FARE, fare.INFANT_FARE_TYPE, fare.EFFECTIVE_FROM_DATE, "
				+ " fare.EFFECTIVE_TO_DATE, bc.STANDARD_CODE AS standard_flag, bc.NEST_RANK, bc.LOGICAL_CABIN_CLASS_CODE,"
				+ " farerule.ADVANCE_BOOKING_DAYS, "
				+ " farerule.FARE_RULE_ID, farerule.LOAD_FACTOR_MIN, farerule.LOAD_FACTOR_MAX, farerule.DEPARTURE_TIME_FROM, "
				+ " farerule.DEPARTURE_TIME_TO, farerule.VALID_DAYS_OF_WEEK_D0, farerule.VALID_DAYS_OF_WEEK_D1, "
				+ " farerule.VALID_DAYS_OF_WEEK_D2, farerule.VALID_DAYS_OF_WEEK_D3, farerule.VALID_DAYS_OF_WEEK_D4, "
				+ " farerule.VALID_DAYS_OF_WEEK_D5, farerule.VALID_DAYS_OF_WEEK_D6  "
				+ " FROM T_OND_FARE fare, T_FARE_RULE farerule, T_BOOKING_CLASS bc, T_FARE_VISIBILITY farevisibility, "
				+ " T_FARE_AGENT fareagent  WHERE fare.BOOKING_CODE = bc.BOOKING_CODE  "
				+ " AND fare.FARE_RULE_ID   = farerule.FARE_RULE_ID  AND  "
				+ Util.getReplaceStringForIN("fare.ond_code", searchCriteria.getOndCodes())
				+ " AND sysdate BETWEEN fare.SALES_VALID_FROM AND fare.SALES_VALID_TO  " + " AND (to_date(  "
				+ CalendarUtil.formatForSQL_toDate(searchCriteria.getFromDate()) + ") < fare.EFFECTIVE_TO_DATE   "
				+ " OR to_date( " + CalendarUtil.formatForSQL_toDate(searchCriteria.getToDate())
				+ ")   > fare.EFFECTIVE_FROM_DATE)   "
				+ " AND fare.status = 'ACT'  AND bc.ALLOCATION_TYPE IN ( 'CON', 'SEG', 'COM')   "
				+ " AND bc.BC_TYPE IN (?)  AND bc.status = 'ACT'  AND farerule.STATUS = 'ACT'  "
				+ " AND farerule.RETURN_FLAG = 'N'  ";
		if (searchCriteria.getPaxTypes().contains(PaxTypeTO.ADULT)) {
			fareQuery += " AND farerule.FARE_RULE_ID IN "
					+ " (SELECT TF.FARE_RULE_ID FROM T_FARE_RULE_PAX TF WHERE (TF.PAX_TYPE_CODE='AD') AND TF.APPLY_FARE = 'Y' ) ";
		}
		if (searchCriteria.getPaxTypes().contains(PaxTypeTO.CHILD)) {
			fareQuery += " AND farerule.FARE_RULE_ID IN "
					+ " (SELECT TF.FARE_RULE_ID FROM T_FARE_RULE_PAX TF WHERE (TF.PAX_TYPE_CODE='CH') AND TF.APPLY_FARE = 'Y' ) ";
		}
		if (searchCriteria.getPaxTypes().contains(PaxTypeTO.INFANT)) {
			fareQuery += " AND farerule.FARE_RULE_ID IN "
					+ " (SELECT TF.FARE_RULE_ID FROM T_FARE_RULE_PAX TF WHERE (TF.PAX_TYPE_CODE='IN') AND TF.APPLY_FARE = 'Y' ) ";
		}

		fareQuery += " AND farevisibility.FARE_RULE_ID = farerule.FARE_RULE_ID  " + " AND "
				+ getSalesChannelCondition(searchCriteria.getSalesChannels())
				+ " AND farerule.FARE_RULE_ID = fareagent.FARE_RULE_ID (+)  ";

		if (searchCriteria.getAgentCode() == null) {
			fareQuery += " AND (fareagent.AGENT_CODE IS NULL  )  ";
		} else {
			fareQuery += " AND (fareagent.AGENT_CODE IS NULL  OR fareagent.AGENT_CODE = '" + searchCriteria.getAgentCode()
					+ "')  ";
		}
		fareQuery += " ORDER BY bc.STANDARD_CODE, bc.NEST_RANK ASC, fare.FARE_AMOUNT ASC ";
		@SuppressWarnings("unchecked")
		List<LightFareDTO> fares = (List<LightFareDTO>) jt.query(fareQuery, new Object[] { searchCriteria.getBookingType() },
				new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						List<LightFareDTO> list = new ArrayList<LightFareDTO>();
						Map<Long, LightFareRuleDTO> fareRuleMap = new HashMap<Long, LightFareRuleDTO>();
						while (rs.next()) {
							LightFareDTO fare = new LightFareDTO();
							fare.setFareId(rs.getLong("fare_id"));
							fare.setOndCode(rs.getString("ond_code"));
							fare.setBookingCode(rs.getString("booking_code"));
							fare.setFareAmount(rs.getBigDecimal("fare_amount"));
							fare.setChildFare(rs.getString("child_fare_type"), rs.getBigDecimal("child_fare"));
							fare.setInfantFare(rs.getString("infant_fare_type"), rs.getBigDecimal("infant_fare"));
							fare.setEffectiveFrom(rs.getDate("effective_from_date"));
							fare.setEffectiveTo(rs.getDate("effective_to_date"));
							fare.setStandardBC("Y".equals(rs.getString("standard_flag")));
							fare.setLogicalCabinClass(rs.getString("logical_cabin_class_code"));
							if (fare.isStandardBC()) {
								fare.setNestRank(rs.getInt("nest_rank"));
							}
							Long fareRuleId = rs.getLong("fare_rule_id");
							LightFareRuleDTO fareRule = null;
							if (fareRuleMap.containsKey(fareRuleId)) {
								fareRule = fareRuleMap.get(fareRuleId);
							} else {
								fareRule = new LightFareRuleDTO();
								fareRule.setFareRuleId(fareRuleId);
								if (rs.getString("advance_booking_days") != null) {
									fareRule.setAdvanceBookingDays(rs.getInt("advance_booking_days"));
								}
								HourValidity departureHourValidity = new HourValidity(rs.getTimestamp("departure_time_from"), rs
										.getTimestamp("departure_time_to"));
								fareRule.setDepartureHourValidity(departureHourValidity);
								WeekFrequency departureWeekFrequency = new WeekFrequency(rs.getBoolean("valid_days_of_week_d0"),
										rs.getBoolean("valid_days_of_week_d1"), rs.getBoolean("valid_days_of_week_d2"), rs
												.getBoolean("valid_days_of_week_d3"), rs.getBoolean("valid_days_of_week_d4"), rs
												.getBoolean("valid_days_of_week_d5"), rs.getBoolean("valid_days_of_week_d6"));
								fareRule.setDepartureWeekFrequency(departureWeekFrequency);

								fareRule.setLoadFactorMin(rs.getLong("load_factor_min"));
								if (rs.wasNull()) {
									fareRule.setLoadFactorMin(null);
								}

								fareRule.setLoadFactorMax(rs.getLong("load_factor_max"));
								if (rs.wasNull()) {
									fareRule.setLoadFactorMax(null);
								}
							}
							fare.setFareRule(fareRule);
							list.add(fare);
						}
						return list;
					}

				});
		return fares;
	}

	private String getSalesChannelCondition(Set<Integer> salesChannels) {
		String query = "";
		if (salesChannels != null && salesChannels.size() > 0) {
			for (Integer sc : salesChannels) {
				if (!"".equals(query)) {
					query += " OR ";
				}
				query += (" farevisibility.sales_channel_code   = " + sc.intValue() + " ");
			}
			query = "(" + query + " ) ";
		} else {
			query = " farevisibility.sales_channel_code = 1 ";
		}
		return query;
	}
}
