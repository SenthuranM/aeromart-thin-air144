package com.isa.thinair.airpricing.core.persistence.jdbc.chargejdbcdao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Iterator;

import org.springframework.jdbc.core.PreparedStatementCreator;

import com.isa.thinair.airpricing.api.dto.QuoteChargesCriteria;
import com.isa.thinair.airpricing.api.model.Charge;
import com.isa.thinair.airpricing.api.model.ChargeRate;

/**
 * @author Nasly
 */
public class ChargeQuotePreparedStmtCreator implements PreparedStatementCreator {

	private QuoteChargesCriteria _criteria;

	private String _query;

	public ChargeQuotePreparedStmtCreator(String query, QuoteChargesCriteria criteria) {
		_criteria = criteria;
		_query = query;
	}

	public PreparedStatement createPreparedStatement(Connection conn) throws SQLException {
		PreparedStatement ps = conn.prepareStatement(_query);
		int index = 0;
		ps.setString(++index, Charge.STATUS_ACTIVE);
		if (_criteria.getChargeCatCodes() != null && _criteria.getChargeCatCodes().length > 0) {
			for (int i = 0; i < _criteria.getChargeCatCodes().length; i++) {
				ps.setString(++index, _criteria.getChargeCatCodes()[i]);
			}
		}

		if (_criteria.getChargeGroups() != null && _criteria.getChargeGroups().length > 0) {
			for (int i = 0; i < _criteria.getChargeGroups().length; ++i) {
				ps.setString(++index, _criteria.getChargeGroups()[i]);
			}
		}
		
//      AARESAA-11826 - POS charges getting applied to IBE
//		if (_criteria.getPosAirport() == null || _criteria.getPosAirport().equals(""))
//			ps.setString(++index, "%");
//
//		if (_criteria.getAgentCode() == null || _criteria.getAgentCode().equals(""))
//			ps.setString(++index, "%");

		// else
		// ps.setString(++index, _criteria.getPosAirport());
		if (!_criteria.isVarianceQuote())
			ps.setTimestamp(++index, new java.sql.Timestamp(_criteria.getDepatureDate().getTime()));
		else {
			ps.setTimestamp(++index, new java.sql.Timestamp(_criteria.getMinDepartureDate().getTime())); 
			ps.setTimestamp(++index, new java.sql.Timestamp(_criteria.getMaxDepartureDate().getTime()));
			ps.setTimestamp(++index, new java.sql.Timestamp(_criteria.getMinDepartureDate().getTime()));
			ps.setTimestamp(++index, new java.sql.Timestamp(_criteria.getMaxDepartureDate().getTime()));
			ps.setTimestamp(++index, new java.sql.Timestamp(_criteria.getMinDepartureDate().getTime()));
			ps.setTimestamp(++index, new java.sql.Timestamp(_criteria.getMaxDepartureDate().getTime()));
			ps.setTimestamp(++index, new java.sql.Timestamp(_criteria.getMinDepartureDate().getTime()));
			ps.setTimestamp(++index, new java.sql.Timestamp(_criteria.getMaxDepartureDate().getTime()));
			ps.setTimestamp(++index, new java.sql.Timestamp(_criteria.getMinDepartureDate().getTime()));
			ps.setTimestamp(++index, new java.sql.Timestamp(_criteria.getMaxDepartureDate().getTime()));
		}

		ps.setTimestamp(++index, new java.sql.Timestamp(_criteria.getChargeQuoteDate().getTime()));
		ps.setString(++index, ChargeRate.Status_Active);
		ps.setString(++index, _criteria.getOnd());
		// ps.setString(++index, _criteria.getOndForDepartureCharge());
		// ps.setString(++index, _criteria.getOndForArrivalCharge());

		ps.setString(++index, _criteria.getOndCodeForAnyArrAirport());
		ps.setString(++index, _criteria.getOndCodeForAnyDepAirport());

		if(!_criteria.isServiceTaxQuote()) {
			if (_criteria.hasTransitAirports()) {
				Iterator<String> airports = _criteria.getWildCardTransitAirports().iterator();
				while (airports.hasNext()) {
					ps.setString(++index, airports.next());
				}
	
			}
			if (_criteria.hasExcludeArrDepTaxAirports()) {
				for (String transitAP : _criteria.getExcludeArrDepTaxAirports()) {
					ps.setString(++index, transitAP + "/%"); // departure
					ps.setString(++index, "%/" + transitAP); // arrival
				}
			}
	
			if (!_criteria.isVarianceQuote()) {
				ps.setString(++index, Charge.APPLY_TO_ALL_ONDs);
				if (_criteria.getOndPosition() != null) {
					if (_criteria.getOndPosition().equals(QuoteChargesCriteria.FIRST_AND_LAST_OND)) {
						ps.setString(++index, Charge.APPLY_TO_FIRST_OND_ONLY);
						ps.setString(++index, Charge.APPLY_TO_LAST_OND_ONLY);
					} else if (_criteria.getOndPosition().equals(QuoteChargesCriteria.FIRST_OND)) {
						ps.setString(++index, Charge.APPLY_TO_FIRST_OND_ONLY);
					} else if (_criteria.getOndPosition().equals(QuoteChargesCriteria.LAST_OND)) {
						ps.setString(++index, Charge.APPLY_TO_LAST_OND_ONLY);
					}
				}
			}
	
			if (_criteria.isQualifyForShortTransit()) {
				ps.setString(++index, Charge.JOURNEY_TYPE_EXCLUDE_SHORT_TRANSIT);
			}
	
			if (!_criteria.isHasInwardTransit() && !_criteria.isHasOutwardTransit() && !_criteria.isHasTransitWithinOnd()) { // no_tran
				ps.setString(++index, QuoteChargesCriteria.CODE_FOR_INCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT); // getting
																													// all
																													// the
																													// charges
																													// not
																													// define
																													// include
				ps.setString(++index, QuoteChargesCriteria.CODE_FOR_INCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
				ps.setString(++index, QuoteChargesCriteria.CODE_FOR_INCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
			} else {
				// getting
				// other
				// charges
				// not
				// define
				// include/exclude
				// transit
				// ps.setString(++index, QuoteChargesCriteria.CODE_FOR_INCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
				// ps.setString(++index, QuoteChargesCriteria.CODE_FOR_EXCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
				// ps.setString(++index, QuoteChargesCriteria.CODE_FOR_INCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
				// ps.setString(++index, QuoteChargesCriteria.CODE_FOR_EXCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
				// ps.setString(++index, QuoteChargesCriteria.CODE_FOR_INCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
				// ps.setString(++index, QuoteChargesCriteria.CODE_FOR_EXCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
	
				if (_criteria.isHasInwardTransit() && !_criteria.isHasOutwardTransit() && !_criteria.isHasTransitWithinOnd()) { // inw_tran
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_INCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_EXCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
	
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_INCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
					ps.setLong(++index, _criteria.getInwardTransitGap());
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_EXCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
					ps.setLong(++index, _criteria.getInwardTransitGap());
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_INCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_INCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
				} else if (!_criteria.isHasInwardTransit() && _criteria.isHasOutwardTransit() && !_criteria.isHasTransitWithinOnd()) { // out_tran
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_INCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_EXCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
	
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_INCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
					ps.setLong(++index, _criteria.getOutwardTransitGap());
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_EXCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
					ps.setLong(++index, _criteria.getOutwardTransitGap());
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_INCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_INCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
				} else if (!_criteria.isHasInwardTransit() && !_criteria.isHasOutwardTransit() && _criteria.isHasTransitWithinOnd()) { // ond_tran
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_INCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_EXCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
	
					Iterator<Long> ondTranDurIt = _criteria.getOndTransitDurList().iterator();
					for (int count = 1; ondTranDurIt.hasNext(); count++) {
						if (count == 1) {
							ps.setString(++index, QuoteChargesCriteria.CODE_FOR_INCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
						}
						ps.setLong(++index, (Long) ondTranDurIt.next());
					}
	
					Iterator<Long> ondTranDurIt1 = _criteria.getOndTransitDurList().iterator();
					for (int count1 = 1; ondTranDurIt1.hasNext(); count1++) {
						if (count1 == 1) {
							ps.setString(++index, QuoteChargesCriteria.CODE_FOR_EXCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
						}
						ps.setLong(++index, (Long) ondTranDurIt1.next());
					}
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_INCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_INCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
				} else if (_criteria.isHasInwardTransit() && _criteria.isHasOutwardTransit() && !_criteria.isHasTransitWithinOnd()) { // inw_tran
																																		// &
																																		// out_tran
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_INCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_EXCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_INCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_EXCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
	
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_INCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
					ps.setLong(++index, _criteria.getInwardTransitGap());
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_EXCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
					ps.setLong(++index, _criteria.getInwardTransitGap());
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_INCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
					ps.setLong(++index, _criteria.getOutwardTransitGap());
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_EXCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
					ps.setLong(++index, _criteria.getOutwardTransitGap());
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_INCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
				} else if (_criteria.isHasInwardTransit() && !_criteria.isHasOutwardTransit() && _criteria.isHasTransitWithinOnd()) { // inw_tran
																																		// &
																																		// ond_tran
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_INCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_EXCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_INCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_EXCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
	
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_INCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
					ps.setLong(++index, _criteria.getInwardTransitGap());
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_EXCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
					ps.setLong(++index, _criteria.getInwardTransitGap());
					Iterator<Long> ondTranDurIt = _criteria.getOndTransitDurList().iterator();
					for (int count = 1; ondTranDurIt.hasNext(); count++) {
						if (count == 1) {
							ps.setString(++index, QuoteChargesCriteria.CODE_FOR_INCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
						}
						ps.setLong(++index, (Long) ondTranDurIt.next());
					}
	
					Iterator<Long> ondTranDurIt1 = _criteria.getOndTransitDurList().iterator();
					for (int count1 = 1; ondTranDurIt1.hasNext(); count1++) {
						if (count1 == 1) {
							ps.setString(++index, QuoteChargesCriteria.CODE_FOR_EXCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
						}
						ps.setLong(++index, (Long) ondTranDurIt1.next());
					}
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_INCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
				} else if (!_criteria.isHasInwardTransit() && _criteria.isHasOutwardTransit() && _criteria.isHasTransitWithinOnd()) { // out_tran
																																		// &
																																		// ond_tran
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_INCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_EXCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_INCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_EXCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
	
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_INCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
					ps.setLong(++index, _criteria.getOutwardTransitGap());
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_EXCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
					ps.setLong(++index, _criteria.getOutwardTransitGap());
					Iterator<Long> ondTranDurIt = _criteria.getOndTransitDurList().iterator();
					for (int count = 1; ondTranDurIt.hasNext(); count++) {
						if (count == 1) {
							ps.setString(++index, QuoteChargesCriteria.CODE_FOR_INCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
						}
						ps.setLong(++index, (Long) ondTranDurIt.next());
					}
					Iterator<Long> ondTranDurIt1 = _criteria.getOndTransitDurList().iterator();
					for (int count1 = 1; ondTranDurIt1.hasNext(); count1++) {
						if (count1 == 1) {
							ps.setString(++index, QuoteChargesCriteria.CODE_FOR_EXCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
						}
						ps.setLong(++index, (Long) ondTranDurIt1.next());
					}
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_INCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
				} else if (_criteria.isHasInwardTransit() && _criteria.isHasOutwardTransit() && _criteria.isHasTransitWithinOnd()) { // inw_tran,
																																		// out_tran
																																		// &
																																		// ond_tran
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_INCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_EXCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_INCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_EXCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_INCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_EXCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
	
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_INCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
					ps.setLong(++index, _criteria.getInwardTransitGap());
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_EXCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
					ps.setLong(++index, _criteria.getInwardTransitGap());
					Iterator<Long> ondTranDurIt = _criteria.getOndTransitDurList().iterator();
					for (int count = 1; ondTranDurIt.hasNext(); count++) {
						if (count == 1) {
							ps.setString(++index, QuoteChargesCriteria.CODE_FOR_INCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
						}
						ps.setLong(++index, (Long) ondTranDurIt.next());
					}
					Iterator<Long> ondTranDurIt1 = _criteria.getOndTransitDurList().iterator();
					for (int count1 = 1; ondTranDurIt1.hasNext(); count1++) {
						if (count1 == 1) {
							ps.setString(++index, QuoteChargesCriteria.CODE_FOR_EXCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
						}
						ps.setLong(++index, (Long) ondTranDurIt1.next());
					}
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_INCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
					ps.setLong(++index, _criteria.getOutwardTransitGap());
					ps.setString(++index, QuoteChargesCriteria.CODE_FOR_EXCLUDE_IF_HAS_CORRESPONING_FORM_OF_TRANSIT);
					ps.setLong(++index, _criteria.getOutwardTransitGap());
				}
			}
		}
		ps.setString(++index, _criteria.getCabinClassCode());
		return ps;
	}

}
