/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 13, 2005 17:12:34
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airpricing.core.persistence.dao;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.isa.thinair.airpricing.api.model.Channel;
import com.isa.thinair.airpricing.api.model.FareRule;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;

/**
 * BHive Commons: FareRule module
 * 
 * @author Byorn
 */
public interface FareRuleDAO {

	/**
	 * save, getFareRules with Paging, and deletion of FareRules
	 */
	public void saveFareRule(FareRule fareRule);

	public Page getFareRules(List<ModuleCriterion> criteriaList, int startIndex, int pageSize, List<String> orderBy);

	public FareRule getFareRule(String fareRuleCode);

	public Collection<FareRule> getFareRuleByBasisCode(String fareBasisCode);

	public List<FareRule> getFareRules(String fareRuleCode);

	public FareRule getFareRule(int fareRuleID);

	public void deleteFareRule(Integer fareRuleId);

	public void inactiveFareForFareRule(int fareRuleId);

	/** Interface Method to get a list of Channels **/
	public Collection<Channel> getChannels(Collection<Integer> channelIds);

	/** Interface Method to get a list of Channels **/
	public Collection<Channel> getAllSalesChannels();

	public Map<Integer, FareRule> getFareRulesMap(Collection<Integer> fareRuleIds);

	public List<FareRule> getApplicableFareRulesForExchangeRateChange(String currencyCode);

	public FareRule getFareRuleForFareID(Integer fareId);

}
