package com.isa.thinair.airpricing.core.util;

import java.io.Serializable;
import java.util.Comparator;

import com.isa.thinair.airpricing.api.dto.AgentFareRulesDTO;

@SuppressWarnings("rawtypes")
public class AgentFaresComparator implements Comparator, Serializable {

	private static final long serialVersionUID = -6093809269243359908L;

	public int compare(Object o1, Object o2) {

		if (!(o1 instanceof AgentFareRulesDTO))
			throw new ClassCastException();
		if (!(o2 instanceof AgentFareRulesDTO))
			throw new ClassCastException();

		String agentCode1 = ((AgentFareRulesDTO) o1).getAgentCode();
		String agentCode2 = ((AgentFareRulesDTO) o2).getAgentCode();

		return agentCode1.compareTo(agentCode2);
		// if(((AgentFareRulesDTO)o1)
		// < ((MyClass)o2).data)
		// return -1;
		// if(((MyClass)o1).data
		// > ((MyClass)o2).data)
		// return 1;
		// else return 0;

	}

}
