/*
 * Created on 05-Jun-2005
 *
 */
package com.isa.thinair.airpricing.core.config;

import java.util.Map;

import com.isa.thinair.platform.api.DefaultModuleConfig;

/**
 * @author byorn
 * @isa.module.config-bean
 */
public class AirpricingConfig extends DefaultModuleConfig {
	private Map chargeCategoriesMap;
	private Map flexiChargePercentageBasisMap;
	private Map flexiruleFlexibilityTypeMap;

	public AirpricingConfig() {
		super();

	}

	/**
	 * @return Returns the chargeCategoriesMap.
	 */
	public Map getChargeCategoriesMap() {
		return chargeCategoriesMap;
	}

	/**
	 * @param chargeCategoriesMap
	 *            The chargeCategoriesMap to set.
	 */
	public void setChargeCategoriesMap(Map chargeCategoriesMap) {
		this.chargeCategoriesMap = chargeCategoriesMap;
	}

	/**
	 * @return Returns the flexiChargePercentageBasisMap.
	 */
	public Map getFlexiChargePercentageBasisMap() {
		return flexiChargePercentageBasisMap;
	}

	/**
	 * @param flexiChargePercentageBasisMap
	 *            The flexiChargePercentageBasisMap to set.
	 */
	public void setFlexiChargePercentageBasisMap(Map flexiChargePercentageBasisMap) {
		this.flexiChargePercentageBasisMap = flexiChargePercentageBasisMap;
	}

	/**
	 * @return Returns the flexiruleFlexibilityTypeMap.
	 */
	public Map getFlexiruleFlexibilityTypeMap() {
		return flexiruleFlexibilityTypeMap;
	}

	/**
	 * @param flexiruleFlexibilityTypeMap
	 *            The flexiruleFlexibilityTypeMap to set.
	 */
	public void setFlexiruleFlexibilityTypeMap(Map flexiruleFlexibilityTypeMap) {
		this.flexiruleFlexibilityTypeMap = flexiruleFlexibilityTypeMap;
	}

}
