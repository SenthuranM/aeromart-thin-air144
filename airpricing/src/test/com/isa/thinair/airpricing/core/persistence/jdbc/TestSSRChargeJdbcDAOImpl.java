package com.isa.thinair.airpricing.core.persistence.jdbc;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import junit.framework.TestCase;

import com.isa.thinair.airpricing.api.dto.SSRChargeDTO;
import com.isa.thinair.airpricing.api.dto.SSRChargeSearchDTO;
import com.isa.thinair.airpricing.api.service.AirpricingUtils;
import com.isa.thinair.airpricing.core.persistence.dao.SSRChargeJdbcDAO;
import com.isa.thinair.airpricing.core.persistence.jdbc.ssrchargejdbcdao.SSRChargeSearchCriteria;
import com.isa.thinair.airpricing.core.util.InternalConstants;
import com.isa.thinair.commons.core.util.AppIndicatorEnum;
import com.isa.thinair.commons.core.util.CommonsTestcase;

public class TestSSRChargeJdbcDAOImpl extends CommonsTestcase {
	SSRChargeJdbcDAO ssrChargeJdbcDAO = null;
	
	protected void setUp() throws Exception {
		super.setUp();
		ssrChargeJdbcDAO = getSSRChargeJdbcDAO();
	}
	
	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	public void testGetSSRChargeDTOs() throws Exception {

		SSRChargeSearchCriteria ssrChargeSearchCriteria = new SSRChargeSearchCriteria();
		Collection<String> ondPatterns = new ArrayList<String>();
		
		ondPatterns.add("SHJ/***");
		ondPatterns.add("***/CMB");
		
		ssrChargeSearchCriteria.setAppIndicator(AppIndicatorEnum.APP_XBE);
		ssrChargeSearchCriteria.setONDPatterns(ondPatterns);
		
		Collection<SSRChargeDTO> ssrChargesMap = ssrChargeJdbcDAO.getAvailableSSRChargeDTOs(ssrChargeSearchCriteria);

		for (Iterator iterator = ssrChargesMap.iterator(); iterator.hasNext();) {
				SSRChargeDTO chargeDTO = (SSRChargeDTO) iterator.next();
				System.out.println("SSRCode:" + chargeDTO.getSSRCode());
				System.out.println("SSRChargeCode:" + chargeDTO.getSSRChargeCode());
				System.out.println("ONDCode:" + chargeDTO.getONDCode());
				System.out.println("MaximumPaxCount:" + chargeDTO.getMaximumTotalPax());
				System.out.println("");
		}

	}

	private SSRChargeJdbcDAO getSSRChargeJdbcDAO() {
		return (SSRChargeJdbcDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.SSR_CHARGE_JDBC_DAO);
	}
}
