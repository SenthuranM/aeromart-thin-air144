/*
 * ============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2005 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, 
 * with or without modification, is not permitted without 
 * prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jun 7, 2005
 * 
 * TestCountryDAOImpl.java
 * 
 * ============================================================================
 */
package com.isa.thinair.airpricing.core.remoting.ejb.fares;

import java.util.ArrayList;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.Iterator;

import com.isa.thinair.airpricing.api.criteria.FareSearchCriteria;
import com.isa.thinair.airpricing.api.dto.ManageFareDTO;
import com.isa.thinair.airpricing.api.model.Fare;
import com.isa.thinair.airpricing.core.bl.FareBL;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Byorn
 */
public class TestFareValidations extends TestBaseFaresTest {
	
	
	/**
     * When Updating a Fare, and Editing it with an Existing Date Range for OND, BC and FR combination
     * void
     * @throws Exception
	 */
    public void testIsFareLegal() throws Exception
	{
		Collection fares=new ArrayList();
		FareSearchCriteria fareSearchCriteria = new FareSearchCriteria();
		Page p = fareJdbcDAO.getFares(fareSearchCriteria,1,9,null);
		Collection c = p.getPageData();
		
        assertTrue("Number of Records Retrieved was not 9", 9==c.size());
        
        Iterator iter = c.iterator();
		int fareid=0;
		int i=0;
		while(iter.hasNext())
		{
		
			ManageFareDTO manageFareDTO = (ManageFareDTO)iter.next();
			fareid = manageFareDTO.getFareId();
			Fare fare = new Fare();
			fare.setFareId(manageFareDTO.getFareId());
		
            /** another fareid has the same date range, ond code, bc and fr combination ,..error should occur **/
            GregorianCalendar dateValidFrom = new GregorianCalendar(2000,GregorianCalendar.JULY,27);
            GregorianCalendar dateValidTo = new GregorianCalendar(2000,GregorianCalendar.OCTOBER,27);
            
            fare.setEffectiveFromDate(dateValidFrom.getTime());
			fare.setEffectiveToDate(dateValidTo.getTime());
			/****************************************************************************************************/
            
            fare.setFareAmount(234.22);
			fare.setStatus(manageFareDTO.getStatus());
			fare.setOndCode(manageFareDTO.getOndCode());
			fare.setBookingCode(manageFareDTO.getBookingClass());
			fare.setFareRuleId(manageFareDTO.getFareRuleId());
			
			
			
			fare.setVersion(manageFareDTO.getVersion());
			fares.add(fare);
			
        }
		
		try{
        getFareService().updateFares(fares);
        }
        catch(ModuleException e)
        {
            assertTrue("Module Exception Code was not airpricing.fare.illegal", e.getExceptionCode().equals("airpricing.fare.illegal"));
                        
        }
		
		
	}
	

   
    


	
	
	
    

  

		
}
	