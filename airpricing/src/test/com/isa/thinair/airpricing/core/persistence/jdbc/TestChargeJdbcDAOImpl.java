package com.isa.thinair.airpricing.core.persistence.jdbc;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.isa.thinair.airpricing.api.criteria.PricingConstants;
import com.isa.thinair.airpricing.api.dto.QuoteChargesCriteria;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airpricing.api.dto.QuotedONDChargesDTO;
import com.isa.thinair.airpricing.api.service.AirpricingUtils;
import com.isa.thinair.airpricing.core.persistence.dao.ChargeJdbcDAO;
import com.isa.thinair.airpricing.core.util.InternalConstants;
import com.isa.thinair.platform.api.util.PlatformTestCase;

public class TestChargeJdbcDAOImpl extends PlatformTestCase {

	
	public void testQuoteChargesForGroups()throws Exception{
		Collection chargeGroups = new ArrayList();
		chargeGroups.add(PricingConstants.ChargeGroups.ADJUSTMENT_CHARGE);
		chargeGroups.add(PricingConstants.ChargeGroups.INFANT_SURCHARGE);
		chargeGroups.add(PricingConstants.ChargeGroups.TAX);
		chargeGroups.add("SUR");
		HashMap hashMap = getChargeJdbcDAO().quoteUniqueChargeForGroups(chargeGroups);
		Iterator iter = hashMap.keySet().iterator();
		while (iter.hasNext()) {
			String chargeGroupCode = (String) iter.next();
			System.out.println(chargeGroupCode);
			QuotedChargeDTO chargeDTO = (QuotedChargeDTO) hashMap.get(chargeGroupCode);
			System.out.println("Charge Code : " + chargeDTO.getChargeCode()
			+ "\n charge rate id" + chargeDTO.getChargeRateId()		
			);
			
		}
	}
	
//    public void testQuoteCharges() {
//        List criteriaCollection = new ArrayList();
//        
//        QuoteChargesCriteria criteria = new QuoteChargesCriteria();
//        Calendar departureDate = new GregorianCalendar();
//        departureDate.set(2006, Calendar.JANUARY, 20);
//        criteria.setOnd("CMB/SHJ");
//        criteria.setPosAirport(null);
//        criteria.setDepatureDate(departureDate.getTime());
//        criteria.setChargeCatCodes(null);//all charge categories
//        criteria.setFareAmount(new Double(200));
//        criteriaCollection.add(criteria);
//        
//        
//        QuoteChargesCriteria criteria1 = new QuoteChargesCriteria();
//        Calendar departureDate1 = new GregorianCalendar();
//        departureDate1.set(2006, Calendar.JANUARY, 20);
//        criteria1.setOnd("SHJ/DOH");
//        criteria1.setPosAirport(null);
//        criteria1.setDepatureDate(departureDate1.getTime());
//        criteria1.setChargeCatCodes(null);//all charge categories
//        criteria1.setFareAmount(new Double(150));
//        criteriaCollection.add(criteria1);
//
//        QuotedONDChargesDTO quotedCharges = getChargeJdbcDAO().quoteONDCharges(
//                criteriaCollection);
//        
//        
//        assertNotNull(quotedCharges);
//        Iterator cmbshjChargesIt = quotedCharges.getEffectiveCharges("CMB/SHJ").iterator();
//        while (cmbshjChargesIt.hasNext()){
//            QuotedChargeDTO chargeSummaryDTO = (QuotedChargeDTO)cmbshjChargesIt.next();
//            System.out.println("chargeRateId = "+chargeSummaryDTO.getChargeRateId()+
//                                " chargeValue = "+chargeSummaryDTO.getChargeValue() +
//                                (chargeSummaryDTO.isAppliesToInfants()?" adultAndInfantCharge":" adultOnlyCharge"));
//        }
//        double [] cmbshjTotalCharges = quotedCharges.getTotalCharges("CMB/SHJ");
//        System.out.println("total adult  charges for CMB/SHJ   = " + cmbshjTotalCharges[0]);
//        System.out.println("total infant charges for CMB/SHJ   = " + cmbshjTotalCharges[1]);
//        
//        Iterator shjdohChargesIt = quotedCharges.getEffectiveCharges("SHJ/DOH").iterator();
//        while (shjdohChargesIt.hasNext()){
//            QuotedChargeDTO chargeSummaryDTO = (QuotedChargeDTO)shjdohChargesIt.next();
//            System.out.println("chargeRateId = "+chargeSummaryDTO.getChargeRateId()+
//                                " chargeValue = "+chargeSummaryDTO.getChargeValue() +
//                                (chargeSummaryDTO.isAppliesToInfants()?" adultAndInfantCharge":" adultOnlyCharge"));
//        }
//        double [] shjdohTotalCharges = quotedCharges.getTotalCharges("SHJ/DOH");
//        System.out.println("total adult  charges for SHJ/DOH    = " + shjdohTotalCharges[0]);
//        System.out.println("total infant charges for SHJ/DOH   = " + shjdohTotalCharges[1]);
//        
//        Iterator allOndsChargesIt = quotedCharges.getUnifiedCharges().iterator();
//        while (allOndsChargesIt.hasNext()){
//            QuotedChargeDTO chargeSummaryDTO = (QuotedChargeDTO)allOndsChargesIt.next();
//            System.out.println("chargeRateId = "+chargeSummaryDTO.getChargeRateId()+
//                                " chargeValue = "+chargeSummaryDTO.getChargeValue() +
//                                (chargeSummaryDTO.isAppliesToInfants()?" adultAndInfantCharge":" adultOnlyCharge"));
//        }
//        double [] totalCharges2 = quotedCharges.getTotalCharges();
//        System.out.println("total adult  charges for all ONDs    = " + totalCharges2[0]);
//        System.out.println("total infant charges for all ONDs   = " + totalCharges2[1]);
//        
//    }

    private ChargeJdbcDAO getChargeJdbcDAO() {
         return (ChargeJdbcDAO) AirpricingUtils.getInstance().getLocalBean(
                InternalConstants.DAOProxyNames.CHARGE_JDBC_DAO);
    }
}
