/*
 * ============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2005 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, 
 * with or without modification, is not permitted without 
 * prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jun 7, 2005
 * 
 * TestCountryDAOImpl.java
 * 
 * ============================================================================
 */
package com.isa.thinair.airpricing.core.bl;

import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.isa.thinair.airpricing.api.model.Charge;
import com.isa.thinair.airpricing.api.model.ChargeRate;
import com.isa.thinair.airpricing.api.service.AirpricingUtils;
import com.isa.thinair.airpricing.core.persistence.dao.ChargeDAO;
import com.isa.thinair.airpricing.core.util.InternalConstants;
import com.isa.thinair.platform.api.util.PlatformTestCase;

/**
 * @author Byorn
 */
public class TestChargeBL extends PlatformTestCase {
		
	public TestChargeBL() {
		super();
	}

	protected void setUp() throws Exception{
		super.setUp();
	}

	/**
	 * Test Method that will check for new OND Codes and will save it to the master table and then will save the charge.
	 * void
	 * @throws Exception
	 */
	public void testSaveChargeWithNewOND() throws Exception
	{
		Set ondCodes = new HashSet();
		ondCodes.add("FRA/HKG");
		ondCodes.add("UUU/BAH");
			
		Charge obj = new Charge();
		//obj.setOndCodes(ondCodes);
		obj.setChargeCode("AB4");
		obj.setChargeCategoryCode("Global");
		obj.setChargeGroupCode("Tax");
				
		obj.setChargeCategoryValue("aa");
		obj.setChargeDescription("charge description");
        //FIXME
		//obj.setAppliesToInfants(obj.APPLIES_TO_INFANTS_YES);
		obj.setRefundableChargeFlag(obj.NONE_REFUNDABLE_CHARGE);
		obj.setStatus(obj.STATUS_ACTIVE);
		
		ChargeRate cr = new ChargeRate();
		cr.setChargeEffectiveFromDate(new Date());
		cr.setChargeEffectiveToDate(new Date());
        //FIXME
		//cr.setChargeValuePercentage(new Integer(12));
		
		cr.setValuePercentageFlag("V");
		
		Set s = new HashSet();
		s.add(cr);
		
		obj.setChargeRates(s);
		
		ChargeBL chargeBL = new ChargeBL();
		chargeBL.createCharge(obj);

		
		
		getChargeDAO().removeCharge("AB4");
		
	}
 
	/** Test Method to Get OND Codes for a list of ONDCodes. **/
	public void testUpdateChargeWithChangedOND() throws Exception
	{
//		Set ondCodes = new HashSet();
//		ondCodes.add("FRA/HKG");
//		ondCodes.add("UUU/BAH");
//			
		Charge obj = getChargeDAO().getCharge("AB3");
		Set ondCodes = obj.getOndCodes();
		Iterator iter = ondCodes.iterator();
		if (ondCodes.contains("FRA/HKG"))
		{
			ondCodes.remove("FRA/HKG");
			ondCodes.add("FRA/AA1");
		}
		
		//obj.setOndCodes(ondCodes);
				
		obj.setChargeDescription("changed description");
			
		
		
		
		ChargeBL chargeBL = new ChargeBL();
		chargeBL.updateCharge(obj);

		Set s = getChargeDAO().getCharge("AB3").getOndCodes();
		assertTrue("Record was Not Found", s.contains("FRA/AA1"));
		
	}

	
	/**
	  * 
	  * private method to lookup the ChargeDAO
	  * @return ChargeDAO
	  */
	 private ChargeDAO getChargeDAO(){
		  return (ChargeDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.CHARGE_DAO);
	 }
   
}
		
	