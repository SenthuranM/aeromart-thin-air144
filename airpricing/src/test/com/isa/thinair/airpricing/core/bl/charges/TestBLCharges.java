/*
 * ============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2005 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, 
 * with or without modification, is not permitted without 
 * prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jun 7, 2005
 * 
 * TestCountryDAOImpl.java
 * 
 * ============================================================================
 */
package com.isa.thinair.airpricing.core.bl.charges;

import com.isa.thinair.airpricing.api.service.AirpricingUtils;
import com.isa.thinair.airpricing.api.service.ChargeBD;
import com.isa.thinair.airpricing.api.service.FareBD;
import com.isa.thinair.airpricing.core.persistence.dao.ChargeDAO;
import com.isa.thinair.airpricing.core.persistence.dao.FareDAO;
import com.isa.thinair.airpricing.core.persistence.dao.FareJdbcDAO;
import com.isa.thinair.airpricing.core.persistence.dao.FareRuleDAO;
import com.isa.thinair.airpricing.core.persistence.hibernate.ChargeDAOImpl;
import com.isa.thinair.airpricing.core.util.InternalConstants;
import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;

import com.isa.thinair.platform.api.util.PlatformTestCase;

/**
 * @author Byorn
 */
public class TestBLCharges extends PlatformTestCase {
	
	
	
	public ChargeDAO chargeDAO;  
	
	
	
	public TestBLCharges() {
		super();
		chargeDAO = (ChargeDAO)AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.CHARGE_DAO);
//		fareDAO = (FareDAO)AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FARE_DAO);
//		fareJdbcDAO = (FareJdbcDAO)AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.MANAGE_FARES);		
	}
    
    
	public void testGetONDCharges() throws Exception
	{
		
		//Page page = chargeDAO.getONDCharges(criteria, countTestCases(), countTestCases(), orderBy)
	}
	
	public ChargeBD getChargeService()
    { 
        LookupService lookup = LookupServiceFactory.getInstance();
        IModule airpricingModule = lookup.getModule("airpricing");
        System.out.println("Lookup successful...");
        ChargeBD delegate = null;
        try {
            delegate = (ChargeBD)airpricingModule.getServiceBD("charge.service.remote");
            System.out.println("Delegate creation successful...");
            return delegate;
            
        } catch (Exception e) {
            System.out.println("Exception in creating delegate");
            e.printStackTrace();
        }
        return delegate;
    }
	
		
}
	