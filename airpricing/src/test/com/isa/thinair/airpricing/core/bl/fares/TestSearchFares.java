/*
 * ============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2005 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, 
 * with or without modification, is not permitted without 
 * prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jun 7, 2005
 * 
 * TestCountryDAOImpl.java
 * 
 * ============================================================================
 */
package com.isa.thinair.airpricing.core.bl.fares;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import com.isa.thinair.airpricing.api.criteria.FareSearchCriteria;
import com.isa.thinair.airpricing.api.dto.ManageFareDTO;
import com.isa.thinair.airpricing.core.bl.FareBL;
import com.isa.thinair.airpricing.core.remoting.ejb.fares.TestBaseFaresTest;
import com.isa.thinair.airpricing.core.util.BeanUtils;
import com.isa.thinair.commons.api.dto.Page;

/**
 * @author Byorn
 */
public class TestSearchFares extends TestBaseFaresTest {
	
	
	
	

    
    public void testGetFares() throws Exception
    {
        
       
        Collection fares=new ArrayList();
        
        // SCENARIO 1: - ALL CRITERIAS
        FareSearchCriteria fareSearchCriteria = new FareSearchCriteria();
        fareSearchCriteria.setOriginAirportCode("AT2");
        fareSearchCriteria.setOriginAirportCode("LXR");
        Page p = fareJdbcDAO.getFares(fareSearchCriteria,1,9,null);
        assertSchenario(1, p.getPageData());
        
//        // SCENARIO 2: - ORIGIN AIRPORT CODE AND DESTINATION AIRPORT CODE GIVEN
//        FareSearchCriteria fareSearchCriteria2 = new FareSearchCriteria();
//        fareSearchCriteria2.setOriginAirportCode("AA1");
//        fareSearchCriteria2.setDestinationAirportCode("AA2");
//        fareSearchCriteria2.setExactCombination(true);
//        p = fareJdbcDAO.getFares(fareSearchCriteria2,1,9,null);
//        assertSchenario(2, p.getPageData());
//        /*************************************/
//        
//        // SCHENARIO 3: - CABIN CLASS CODE IS SET TO 'I'
//        FareSearchCriteria fareSearchCriteria3 = new FareSearchCriteria();
//        fareSearchCriteria3.setOriginAirportCode("AA1");
//        fareSearchCriteria3.setDestinationAirportCode("AA2");
//        fareSearchCriteria3.setExactCombination(true);
//        fareSearchCriteria3.setCabinClassCode("J");
//        p = fareJdbcDAO.getFares(fareSearchCriteria3,1,9,null);
//        assertSchenario(3, p.getPageData());
//        
//        // SCHENARIO 4: - STATUS IS SET TO INA
//        FareSearchCriteria fareSearchCriteria4 = new FareSearchCriteria();
//        fareSearchCriteria4.setOriginAirportCode("AA1");
//        fareSearchCriteria4.setDestinationAirportCode("AA2");
//        fareSearchCriteria4.setStatus("INA");
//        
//        p = fareJdbcDAO.getFares(fareSearchCriteria4,1,9,null);
//        assertSchenario(4, p.getPageData());
//        
//        // SCHENARIO 5: - FARES EFFECTIVE FROM 1ST MARCH 2000 EFFECTIVE TO 1 JULY 2000
//        FareSearchCriteria fareSearchCriteria5 = new FareSearchCriteria();
//        fareSearchCriteria5.setOriginAirportCode("AA1");
//        fareSearchCriteria5.setDestinationAirportCode("AA2");
//        
//        fareSearchCriteria5.setFromDate(new GregorianCalendar(2000, GregorianCalendar.MARCH, 1).getTime());
//        fareSearchCriteria5.setToDate(new GregorianCalendar(2000, GregorianCalendar.JULY, 1).getTime());
//        
//        p = fareJdbcDAO.getFares(fareSearchCriteria5,1,9,null);
//        assertSchenario(5, p.getPageData());
//
//
//        // SCHENARIO 6: - FARES EFFECTIVE FROM 1ST MARCH 2000 EFFECTIVE TO NOT GIVEN
//        FareSearchCriteria fareSearchCriteria6 = new FareSearchCriteria();
//        fareSearchCriteria6.setOriginAirportCode("AA1");
//        fareSearchCriteria6.setDestinationAirportCode("AA2");
//        
//        fareSearchCriteria6.setFromDate(new GregorianCalendar(2000, GregorianCalendar.MARCH, 1).getTime());
//        p = fareJdbcDAO.getFares(fareSearchCriteria6,1,9,null);
//        assertSchenario(6, p.getPageData());
//
//        // SCHENARIO 7: - FARES EFFECTIVE FROM NOT GIVEN AND  EFFECTIVE TO 1 JULY 2000
//        FareSearchCriteria fareSearchCriteria7 = new FareSearchCriteria();
//        fareSearchCriteria7.setOriginAirportCode("AA1");
//        fareSearchCriteria7.setDestinationAirportCode("AA2");
//        
//        fareSearchCriteria7.setFromDate(new GregorianCalendar(2000, GregorianCalendar.JULY, 1).getTime());
//        p = fareJdbcDAO.getFares(fareSearchCriteria7,1,9,null);
//        assertSchenario(7, p.getPageData());
//
//        // SCHENARIO 8: - Viapoint AA2 All
//        FareSearchCriteria fareSearchCriteria8 = new FareSearchCriteria();
//        fareSearchCriteria8.setOriginAirportCode("AA1");
//        fareSearchCriteria8.setDestinationAirportCode("AA3");
//        List viaAirportCodes = new ArrayList();
//        viaAirportCodes.add("AA2");
//        fareSearchCriteria8.setViaAirportCodes(viaAirportCodes);
//        fareSearchCriteria8.setShowAllONDCombinations(true);
//        p = fareJdbcDAO.getFares(fareSearchCriteria8,1,9,null);
//        assertSchenario(8, p.getPageData());
//        
//        // SCHENARIO 9: - Viapoint AA2 Exact
//        FareSearchCriteria fareSearchCriteria9 = new FareSearchCriteria();
//        fareSearchCriteria9.setOriginAirportCode("AA1");
//        fareSearchCriteria9.setDestinationAirportCode("AA3");
//        List viaAirportCodes1 = new ArrayList();
//        fareSearchCriteria9.setExactCombination(true);
//        viaAirportCodes1.add("AA2");
//        fareSearchCriteria9.setViaAirportCodes(viaAirportCodes1);
//        p = fareJdbcDAO.getFares(fareSearchCriteria9,1,9,null);
//        assertSchenario(9, p.getPageData());

        
    }

  
    /**
     * void
     * @param scenario
     * @param c
     * @throws Exception
     */
    /**
     * void
     * @param scenario
     * @param c
     * @throws Exception
     */
    private void assertSchenario(int scenario, Collection c) throws Exception {
        GregorianCalendar compareFrom = new GregorianCalendar(2000,
                GregorianCalendar.MARCH, 1);
        GregorianCalendar compareTo = new GregorianCalendar(2000,
                GregorianCalendar.JULY, 1);

        List classOfServices = new ArrayList();
        List ondCodes = new ArrayList();
        Vector vectorDates = new Vector();
        List dates = null;
        List statuses = new ArrayList();
        /** populating the data to sets..for ease of testing assertion * */
        Iterator iterator = c.iterator();
        while (iterator.hasNext()) {

            ManageFareDTO manageFareDTO = (ManageFareDTO) iterator.next();
            classOfServices.add(BeanUtils.nullHandler(manageFareDTO
                    .getClassOfService()));
            ondCodes.add(BeanUtils.nullHandler(manageFareDTO.getOndCode()));

            dates = new ArrayList();

            Date dateFrom = manageFareDTO.getValidFrom();
            /** effective From date * */
            int fromDay = dateFrom.getDate();
            int fromMonth = dateFrom.getMonth();
            int fromYear = dateFrom.getYear() + 1900;
            GregorianCalendar dateEffectiveFrom = new GregorianCalendar(
                    fromYear, fromMonth, fromDay);

            Date dateTo = manageFareDTO.getValidTo();
            /** effective To date * */
            int toDay = dateTo.getDate();
            int toMonth = dateTo.getMonth();
            int toYear = dateTo.getYear() + 1900;
            GregorianCalendar dateEffectiveTo = new GregorianCalendar(toYear,
                    toMonth, toDay);

            dates.add(dateEffectiveFrom);
            dates.add(dateEffectiveTo);

            vectorDates.add(dates);

            statuses.add(BeanUtils.nullHandler(manageFareDTO.getStatus()));

        }

       
        switch (scenario) {

            // no criteria
            case 1: {
                assertTrue(scenario + "No Records Found",
                c.size() > 0);
                break;
            }

            // ond code selected to AA1/AA2 with exact combination
            case 2: {
                assertTrue(scenario + "Ond Code was not found in data" + scenario,
                        ondCodes.contains("AA1/AA2"));

                Iterator iterOndCodes = ondCodes.iterator();
                while (iterOndCodes.hasNext()) {
                    String s = (String) iterOndCodes.next();
                    assertTrue(scenario + "Invalid OndCode was found ", s
                            .equals("AA1/AA2"));
                }

                break;
            }

            // cabin class code give as 'I'
            case 3: {
                assertTrue(scenario + "No Records with Cabin Class Code 'J' Schenario: -"
                        + scenario, classOfServices.contains("J"));
                Iterator iterClassOfService = classOfServices.iterator();
                while (iterClassOfService.hasNext()) {
                    String s = (String) iterClassOfService.next();
                    assertTrue(scenario + "Invalid cabin class code found", s.equals("J"));
                }
                break;
            }

            // status set to INA
            case 4: {
                assertTrue(scenario + "No Records with Status  'INA' : -" + scenario,
                        statuses.contains("INA"));
                Iterator iterStatuses = statuses.iterator();
                while (iterStatuses.hasNext()) {
                    String s = (String) iterStatuses.next();
                    assertTrue(scenario + "Inapropriate Status value found ", s
                            .equals("INA"));
                }
                break;
            }

            // fares effect from 1ST MARCH 2000 to 1 JULY 2000
            case 5: {

                for (int i = 0; i < vectorDates.size(); i++) {

                    List range = (List) vectorDates.get(i);
                    GregorianCalendar dateEffectiveFrom = (GregorianCalendar) range
                            .get(0);
                    //System.out.println(dateEffectiveFrom.getTime().toString());
                    GregorianCalendar dateEffectiveTo = (GregorianCalendar) range
                            .get(1);
                    //System.out.println(dateEffectiveTo.getTime().toString());

                    //System.out.println(compareFrom.getTime().toLocaleString());
                    //System.out.println(compareTo.getTime().toLocaleString());

                    boolean notOk = true;

                    if (dateEffectiveFrom.after(compareFrom)
                            && dateEffectiveFrom.before(compareTo)
                            && dateEffectiveTo.after(compareTo)) {
                        notOk = false;
                    }

                    if (dateEffectiveFrom.before(compareFrom)
                            && dateEffectiveTo.after(compareFrom)
                            && dateEffectiveTo.before(compareTo)) {
                        notOk = false;
                    }

                    if (dateEffectiveFrom.before(compareFrom)
                            && dateEffectiveTo.after(compareTo)) {
                        notOk = false;
                    }

                    if (dateEffectiveFrom.after(compareFrom)
                            && dateEffectiveTo.before(compareTo)) {
                        notOk = false;
                    }

                    if (notOk) {
                        throw new Exception(
                                scenario + "date result out of range: Effective From"
                                        + dateEffectiveFrom.getTime()
                                                .toString() + " Effective To"
                                        + dateEffectiveTo.getTime().toString());

                    }
                }
                break;

            }

            case 6: {

                for (int i = 0; i < vectorDates.size(); i++) {

                    List range = (List) vectorDates.get(i);
                    GregorianCalendar dateEffectiveFrom = (GregorianCalendar) range
                            .get(0);
                    //System.out.println(dateEffectiveFrom.getTime().toString());
                    GregorianCalendar dateEffectiveTo = (GregorianCalendar) range
                            .get(1);
                    //System.out.println(dateEffectiveTo.getTime().toString());

                    //System.out.println(compareFrom.getTime().toLocaleString());
                    //System.out.println(compareTo.getTime().toLocaleString());

                    boolean notOk = true;

                    if (dateEffectiveFrom.after(compareFrom)
                            || dateEffectiveTo.after(compareFrom)) {

                        notOk = false;
                    }

                    if (notOk) {
                        throw new Exception(
                                scenario + "date result out of range: Effective From"
                                        + dateEffectiveFrom.getTime()
                                                .toString() + " Effective To"
                                        + dateEffectiveTo.getTime().toString());
                    }

                }
                break;
            }

            case 7: {

                for (int i = 0; i < vectorDates.size(); i++) {

                    List range = (List) vectorDates.get(i);
                    GregorianCalendar dateEffectiveFrom = (GregorianCalendar) range
                            .get(0);
                   // System.out.println(dateEffectiveFrom.getTime().toString());
                    GregorianCalendar dateEffectiveTo = (GregorianCalendar) range
                            .get(1);
                   // System.out.println(dateEffectiveTo.getTime().toString());

                   // System.out.println(compareFrom.getTime().toLocaleString());
                   // System.out.println(compareTo.getTime().toLocaleString());

                    boolean notOk = true;

                    if (dateEffectiveTo.before(compareTo)
                            || dateEffectiveFrom.before(compareTo)) {
                        notOk = false;
                    }

                    if (notOk) {
                        throw new Exception(
                                scenario +  "date result out of range: Effective From"
                                        + dateEffectiveFrom.getTime()
                                                .toString() + " Effective To"
                                        + dateEffectiveTo.getTime().toString());

                    }
                }
                break;

            }

            // via AA2 all combinations
            case 8: {
                               
                assertTrue(scenario + "No Records with OND Code AA1/AA2/AA3: -"
                        + scenario, ondCodes.contains("AA1/AA2/AA3"));
               
                assertTrue(scenario + "No Records with OND Code AA1/AA2/AA4/AA3: -"
                        + scenario, ondCodes.contains("AA1/AA2/AA4/AA3"));
               
                
                Iterator iterOndCodes = ondCodes.iterator();
                while (iterOndCodes.hasNext()) {
                    String s = (String) iterOndCodes.next();
                    if (!(s.equals("AA1/AA2/AA3") || s.equals("AA1/AA2/AA4/AA3")))
                    {  throw new Exception(
                            scenario + "Invalid OND Code found" + s); } 
                                                
                        
                }
                
                break;

            }

            // via AA2 exact combinations
            case 9: {

               
                assertTrue(scenario + "No Records with OND Code AA1/AA2/AA3: -"
                        + scenario, ondCodes.contains("AA1/AA2/AA3"));
                
                Iterator iterOndCodes = ondCodes.iterator();
                while (iterOndCodes.hasNext()) {
                    String s = (String) iterOndCodes.next();
                    if (!(s.equals("AA1/AA2/AA3")))
                    {  throw new Exception(
                            scenario + "Invalid OND Code found" + s); } 
                                                
                        
                }
                
                break;

            }

            // default assertion
            default: {
                // printFareDetails(c, "Fare Details Default Scenario");
                assertTrue(
                        "No Records Found - (No search Criteria) Schenario : -"
                                + scenario, c.size() > 0);
                break;
            }

        }

    }

    private void printFareDetails(Collection d, String schenario) throws Exception
    {
        Collection c = d;
        System.out.println("***PRINTING FARE SEARCH RESULTS***" + schenario.toUpperCase());
   
        
        Iterator iter = c.iterator();
        int fareid=0;
        while(iter.hasNext())
        {
            ManageFareDTO manageFareDTO = (ManageFareDTO)iter.next();
            fareid = manageFareDTO.getFareId();
            System.out.println(manageFareDTO.toString());
            
            
            assertNotNull(manageFareDTO); 
        }
    
        System.out.println("*** END OF PRINTING FARE SEARCH RESULTS****");
        
    }
		
}
	