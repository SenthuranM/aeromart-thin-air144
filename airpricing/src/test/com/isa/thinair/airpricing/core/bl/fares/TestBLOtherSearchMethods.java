/*
 * ============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2005 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, 
 * with or without modification, is not permitted without 
 * prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jun 7, 2005
 * 
 * TestCountryDAOImpl.java
 * 
 * ============================================================================
 */
package com.isa.thinair.airpricing.core.bl.fares;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.isa.thinair.airinventory.api.service.BookingClassBD;
import com.isa.thinair.airpricing.api.service.AirpricingUtils;
import com.isa.thinair.airpricing.core.bl.FareBL;
import com.isa.thinair.airpricing.core.remoting.ejb.fares.TestBaseFaresTest;

/**
 * @author Byorn
 */
public class TestBLOtherSearchMethods extends TestBaseFaresTest {
	
	
	


    /**
     * Test to Get Booking class from BD
     * void
     * @throws Exception
     */
    public void testGetBookingClsss()throws Exception
    {
        //BookingClassBD bd = (BookingClassBD)AirpricingUtils.lookupServiceBD("airinventory","bookingclass.service");
        BookingClassBD bd = null;
        assertNotNull("No record", bd.getBookingClass("T1"));
    }

    
  public void testGetEffectiveAgentsCount() throws Exception
  {
      
      Collection bookingCodes = new ArrayList();
      bookingCodes.add("AA1");
      bookingCodes.add("AA2");
      bookingCodes.add("AA3");
          
      String ondCode = "AA1/AA2";
      
      GregorianCalendar departureDate = new GregorianCalendar(2000,Calendar.MARCH,3);
      HashMap hashMap  = new FareBL().getEffectiveAgentsCount(bookingCodes,ondCode,departureDate.getTime());
      Set s = hashMap.keySet();
      assertTrue("Invalid Booking Code found", s.contains("AA1"));
        assertTrue("Invalid Booking Code found", s.contains("AA2"));
        assertTrue("Invalid Booking Code found", s.contains("AA3"));
        Iterator sIter = s.iterator();
        
        while(sIter.hasNext())
        {
            String bookingCode = (String) sIter.next();
            if(!(bookingCode.equals("AA1") || bookingCode.equals("AA2")||bookingCode.equals("AA3")))
            {
                throw new Exception("invalid booking code found");
            }
        }
        
        Integer agentCountForAA1 = (Integer)hashMap.get("AA1");
        Integer agentCountForAA2 = (Integer)hashMap.get("AA2");
        Integer agentCountForAA3 = (Integer)hashMap.get("AA3");
        
        assertEquals("agent count for booking code AA1 is wrong", agentCountForAA1, new Integer(1));
        assertEquals("agent count for booking code AA2 is wrong", agentCountForAA2, new Integer(1));
        assertEquals("agent count for booking code AA3 is wrong", agentCountForAA3, new Integer(1));
        
       
    }
  
      public void testGetRefundable() throws Exception{
  
              Collection c = new ArrayList();
              //fare ids
              Map m = new HashMap();
              m.put(new Integer(19001),null);
              m.put(new Integer(19002),null);
              //chargeids
              Map m1 = new HashMap();
              m1.put(new String("AA1"), null);
              m1.put(new String("AA2"), null);
              //m1.put(new String("AA3"), null);
              //m1.put(new String("AA4"), null);
              
              c.add(m);
              c.add(m1);
              
              //Collection c1 = new FareBL().getRefundable(c);
              Collection c1 = null;
              
              Iterator iterator = c1.iterator();
              
              Map hashMap = (Map) iterator.next();
              Map hashMap1 = (Map) iterator.next();
              System.out.println(hashMap.keySet().toString());
              System.out.println(hashMap.entrySet().toString());
              System.out.println(hashMap1.keySet().toString());
              System.out.println(hashMap1.entrySet().toString());
      }
}
	