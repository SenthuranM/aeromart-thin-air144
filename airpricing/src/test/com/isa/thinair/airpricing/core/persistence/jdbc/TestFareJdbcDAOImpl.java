/*
 * ============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2005 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, 
 * with or without modification, is not permitted without 
 * prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jun 7, 2005
 * 
 * TestCountryDAOImpl.java
 * 
 * ============================================================================
 */
package com.isa.thinair.airpricing.core.persistence.jdbc;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import com.isa.thinair.airpricing.api.criteria.FareSearchCriteria;
import com.isa.thinair.airpricing.api.dto.ManageFareDTO;
import com.isa.thinair.airpricing.api.service.AirpricingUtils;
import com.isa.thinair.airpricing.core.persistence.dao.FareJdbcDAO;
import com.isa.thinair.airpricing.core.util.InternalConstants;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.util.PlatformTestCase;

/**
 * @author Byorn
 *
 */
public class TestFareJdbcDAOImpl extends PlatformTestCase {
	
	protected void setUp() throws Exception{
		super.setUp();
	}

	
	/**
	 * Test Method to Save and Delete a  charge.
	 * void
	 * @throws Exception
	 */
	public void TestGetFares() throws Exception
	{
		FareSearchCriteria criteria = new FareSearchCriteria();
		
	//	criteria.setOriginAirportCode("CMB");
	//	criteria.setDestinationAirportCode("SHJ");
	//	criteria.setExactCombination(false);
		//criteria.setFareBasisCode("test");
     //   criteria.setCabinClassCode("I");		
		Page p = getFareJdbcDAO().getFares(criteria,0,100, null);				
		assertTrue("No Records", p.getPageData().size()>0);
		Collection c = p.getPageData();
		Iterator i = c.iterator();
		
		while(i.hasNext())
		{
			ManageFareDTO manageFareDTO = (ManageFareDTO)i.next();
			
			System.out.println("fare id :" + manageFareDTO.getFareId() + 
					        "\n booking class :" + manageFareDTO.getBookingClass() + 
					        "\n class of service :" + manageFareDTO.getClassOfService() + 
					        "\n get fare amount :" + manageFareDTO.getFareAmount() + 
					        "\n\n valid from :" + manageFareDTO.getValidFrom() + 
					        "\n\n valid to :" + manageFareDTO.getValidTo() + 
					
					        "\n channel visibilities :" + manageFareDTO.getChannelVisibilities() + 
					        "\n ondCode :" + manageFareDTO.getOndCode() + 
					        "\n standard code" + manageFareDTO.getStandardCode() + 
					        "\n status :" + manageFareDTO.getStatus() + 
					
					        "\n\n fare basis code :" + manageFareDTO.getFareBasisCode() + 
					        
					        "\n version : " + manageFareDTO.getVersion()
					        
			); 
		}
		System.out.println("Number of records:" + p.getPageData().size());
		System.out.println("Total Count of Records:" + p.getTotalNoOfRecords());
	}
   	   
	//TODO: write test with proper test data
    public void TestGetAgentsLinkedToBookingClasses() throws ModuleException, Exception{
        Collection bookingCodes = new ArrayList();
        bookingCodes.add("SFIF1");
        bookingCodes.add("NFIF1");
        HashMap result = getFareJdbcDAO().getAgentsLinkedToBookingClass(bookingCodes);
        assertNotNull(result);
    }
    
    public void testGetAgentBookingCodes () throws Exception{
        getFareJdbcDAO().getBookingCodesLinkedToAgent("%");
    }
	/**
	 * 
	 * FareJdbcDAO
	 * @return
	 */
	private FareJdbcDAO getFareJdbcDAO() throws Exception
	{
	   
	 	return (FareJdbcDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.MANAGE_FARES);	  
	}
   
	
	
	
}
		
	