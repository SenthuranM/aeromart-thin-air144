/*
 * ============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2005 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, 
 * with or without modification, is not permitted without 
 * prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jun 7, 2005
 * 
 * TestCountryDAOImpl.java
 * 
 * ============================================================================
 */
package com.isa.thinair.airpricing.core.persistence.jdbc;

import java.util.Collection;
import java.util.Iterator;

import com.isa.thinair.airpricing.api.criteria.AgentFareRuleSearchCriteria;
import com.isa.thinair.airpricing.api.dto.AgentFareRulesDTO;
import com.isa.thinair.airpricing.api.dto.FareRuleSummaryDTO;
import com.isa.thinair.airpricing.api.service.AirpricingUtils;
import com.isa.thinair.airpricing.core.persistence.dao.FareRuleJdbcDAO;
import com.isa.thinair.airpricing.core.util.BeanUtils;
import com.isa.thinair.airpricing.core.util.InternalConstants;
import com.isa.thinair.commons.api.dto.Page;

import com.isa.thinair.platform.api.util.PlatformTestCase;

/**
 * @author Byorn
 *
 */
public class TestFareRuleJdbcDAOImpl extends PlatformTestCase {
	
	protected void setUp() throws Exception{
		super.setUp();
	}

	
	/**
	 * Test Method to Save and Delete a  charge.
	 * void
	 * @throws Exception
	 */
	public void testGetAgentFareRules() throws Exception
	{
		AgentFareRuleSearchCriteria agentFareRuleSearchCriteria = new AgentFareRuleSearchCriteria();
	
		Page p=null;
		try{
		 p = getFareRuleJdbcDAO().getAgentFareRules(agentFareRuleSearchCriteria,0,100,null);
		}
		catch(Exception e){e.printStackTrace();}
		
		
		Collection l = p.getPageData();
		Iterator iter = l.iterator();
		while(iter.hasNext())
		{
			AgentFareRulesDTO agentFareRulesDTO = (AgentFareRulesDTO)iter.next();
			System.out.println("Agent Code : " + agentFareRulesDTO.getAgentCode() + 
					"\n Agent Name :" + agentFareRulesDTO.getAgentName());  
			System.out.println("\n MasterFAreRules :");
			
			Collection mfrlist = agentFareRulesDTO.getMasterFareRulesSummary();
			if(mfrlist!=null)
            {
            Iterator mfrlistiterator = mfrlist.iterator();
			
            while(mfrlistiterator.hasNext())
			{
				
				FareRuleSummaryDTO masterFareRules = (FareRuleSummaryDTO)mfrlistiterator.next();
				System.out.println("\n"  + BeanUtils.nullHandler(masterFareRules.getMasterFareRuleCode()) + " " + BeanUtils.nullHandler(masterFareRules.getFareRuleId()));
			}
            }
			
			System.out.println("\n AdhocFAreRules :");
			Collection ahoclist = agentFareRulesDTO.getAdhocFareRulesSummary();
			if (ahoclist!=null)
			{Iterator ahoclistiterator = ahoclist.iterator();
			while(ahoclistiterator.hasNext())
			{
				FareRuleSummaryDTO fareRules = (FareRuleSummaryDTO)ahoclistiterator.next();
				System.out.println("\n"  + BeanUtils.nullHandler(fareRules.getMasterFareRuleCode()) + " " + BeanUtils.nullHandler(fareRules.getFareRuleId()));
			}
			}
		}
		
		
		
		assertTrue("No Records", p.getPageData().size()>0);
		System.out.println(p.getPageData().size());
		System.out.println("Total Count of Records:" + p.getTotalNoOfRecords());
	}
   	   
   
	private FareRuleJdbcDAO getFareRuleJdbcDAO(){
	   
	 	return (FareRuleJdbcDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.AGENT_MASTER_FARE_RULE);
	}
   
	
	
	
}
		
	