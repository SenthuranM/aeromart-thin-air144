/*
 * ============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2005 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, 
 * with or without modification, is not permitted without 
 * prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jun 7, 2005
 * 
 * TestCountryDAOImpl.java
 * 
 * ============================================================================
 */
package com.isa.thinair.airpricing.core.persistence.hibernate;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import junit.framework.TestCase;

import com.isa.thinair.airpricing.api.criteria.ChargeTemplateSearchCriteria;
import com.isa.thinair.airpricing.api.criteria.ONDChargeSearchCriteria;
import com.isa.thinair.airpricing.api.model.Charge;
import com.isa.thinair.airpricing.api.model.ChargeRate;
import com.isa.thinair.airpricing.api.model.ChargeTemplate;
import com.isa.thinair.airpricing.api.model.OriginDestination;
import com.isa.thinair.airpricing.api.model.SeatCharge;
import com.isa.thinair.airpricing.api.service.AirpricingUtils;
import com.isa.thinair.airpricing.core.persistence.dao.ChargeDAO;
import com.isa.thinair.airpricing.core.util.InternalConstants;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.core.controller.ModuleFramework;


/**
 * @author Byorn
 */
public class TestChargeDAOImpl extends TestCase {
		
	public TestChargeDAOImpl() {
		super();
	}

	protected void setUp() throws Exception{
		super.setUp();
	}

 
    
//    public  void testGetGlobalCharges() throws Exception
//    {
//        ChargeDAO dao = (ChargeDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.CHARGE_DAO);
//        Collection collection = dao.getGlobalCharges();
//        assertNotNull("collection was null", collection);
//        assertTrue("NO records found", collection.size()>0);
//        
//        
//    }
	
  /**
   * Test DATA: { 
   *                 OND Codes :-- ['AA1/AA2' , 'AA1/AA2/AA3']
   *                 Charge Rated Date Effective 1) From 1-Jan-2000 --> 3-Mar-2000
   *                                             2) From 3-Mar-2000 --> 6-Jun-2000       
   *                
   *            } 
   * 
   * Test Method to getONDs specifiing a search criteria
   * void
   * @throws Exception
   */

 /* public void testGetOND() throws Exception
  {
	   ChargeDAO dao = (ChargeDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.CHARGE_DAO);
	   ONDChargeSearchCriteria searchCriteria = new ONDChargeSearchCriteria();
	   
	   searchCriteria.setExactAirportsCombinationOnly(true);
	   //  searchCriteria.setOriginAirportCode("AA1");
	   // searchCriteria.setDestinationAirportCode("DOH");
	   GregorianCalendar g1 = new GregorianCalendar(2000, Calendar.MARCH, 1);
	   Date d1 = g1.getTime();
	   searchCriteria.setEffectiveFromDate(d1);
	   searchCriteria.setEffectiveToDate(d1);
	   
	   //List viapoint = new ArrayList();
	   //viapoint.add("AA2");
	   //searchCriteria.setViaAirportCodes(viapoint);
	   
	   List orderbylist = new ArrayList();
	   orderbylist.add("ond.ONDCode");
	   
	   Page p = dao.getONDCharges(searchCriteria, 0,100,orderbylist);
	   Collection l = p.getPageData();
	   Iterator iter = l.iterator();
	   assertTrue("Num of records", l.size()>0);
       
       System.out.println("PAGE TOTAL NUM OF RECORDS :" + p.getTotalNoOfRecords());
	   
	   while(iter.hasNext())
	   {
		   OriginDestination originDestination = (OriginDestination) iter.next();
		   System.out.println("OND CODE: " + originDestination.getONDCode());
		   System.out.println("\n origin: " + originDestination.getOriginAirportCode());
		   System.out.println("\n destination: " + originDestination.getDestinationAirportCode());
		   System.out.println("\n ******Charges For the OND*******");
		   
		   Set s = originDestination.getCharges();
		   Iterator iter1 = s.iterator();
		   while(iter1.hasNext())
		   {
			   Charge c = (Charge)iter1.next();
			   System.out.println("\n Charge Code" + c.getChargeCode());
			   System.out.println("\n Charge description" + c.getChargeDescription());
               //FIXME
			   //System.out.println("\n Applies to infants" + c.getAppliesToInfants());
			   System.out.println("\n charge categorycode" + c.getChargeCategoryCode());
			   System.out.println("\n charge groupcode" + c.getChargeGroupCode());
			   System.out.println("\n ******Charges Rate sFor the Charge*******");
			   Set s1 = c.getChargeRates();
			   Iterator iter2 = s1.iterator();
			   while(iter2.hasNext())
			   {
				   ChargeRate cr = (ChargeRate)iter2.next();  
				   System.out.println("\n chargeRate code " + cr.getChargeRateCode());
				   System.out.println("\n charge effective from date :" + cr.getChargeEffectiveFromDate().toString());
				   System.out.println("\n charge effective to date :" + cr.getChargeEffectiveToDate().toString());
				   System.out.println("\n value in local currency :" + cr.getValueInLocalCurrency());
				   System.out.println("\n value in Status :" + cr.getStatus());
			   }
		   }
	   }
  }*/
    
    
    public void testGetOND() throws Exception
      {
        /*ChargeTemplateSearchCriteria oCriteria = new ChargeTemplateSearchCriteria();
        oCriteria.setModelNo("A320-211");
        oCriteria.setTemplateCode("ABC");
        */
        
        
        ChargeTemplate template = new ChargeTemplate();
        template.setModelNo("A320-211");
        template.setDescription("TestXXX");
        template.setTemplateCode("test");
        
        SeatCharge seatcharge1 = new SeatCharge();
        SeatCharge seatcharge2 = new SeatCharge();
        
        seatcharge1.setStatus("ACT");
        seatcharge2.setStatus("INA");
        seatcharge1.setSeatId(1001);
        seatcharge2.setSeatId(1006);
        seatcharge1.setTemplateId(template);
        seatcharge2.setTemplateId(template);
        
        Collection col = new ArrayList();
        col.add(seatcharge1);
        col.add(seatcharge2);
        
        Set set = new HashSet();
        set.addAll(col);
        
        template.setSeatCharges(set);
        try{
            getChargeDAO().saveTemplate(template);
        }catch(Exception e){
            e.printStackTrace();
        }
        
        System.out.println("Done----------------");
        //LookupService lookup = LookupServiceFactory.getInstance();
        
        //ChargeDAO dao = (ChargeDAO) lookup.getBean("isa:base://modules/airmaster?id=ChargeDAOProxy");
        
        /*Page oCol = getChargeDAO().getChargeTemplates(oCriteria,1,10);
        //Page oCol = dao.getChargeTemplates(oCriteria,1,10);
        
        System.out.println("+++++++++++++++END++++++++");
        System.out.println("+++++++++++++++END++++++++"+oCol.getTotalNoOfRecords());*/
      }

//  public void testGetOND() throws Exception
//  {
//	   ChargeDAO dao = (ChargeDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.CHARGE_DAO);
//	   ONDChargeSearchCriteria searchCriteria = new ONDChargeSearchCriteria();
//	   
//	   searchCriteria.setExactAirportsCombinationOnly(true);
//	   //  searchCriteria.setOriginAirportCode("AA1");
//	   // searchCriteria.setDestinationAirportCode("DOH");
//	   GregorianCalendar g1 = new GregorianCalendar(2000, Calendar.MARCH, 1);
//	   Date d1 = g1.getTime();
//	   searchCriteria.setEffectiveFromDate(d1);
//	   //searchCriteria.setEffectiveToDate(d1);
//	   
//	   //List viapoint = new ArrayList();
//	   //viapoint.add("AA2");
//	   //searchCriteria.setViaAirportCodes(viapoint);
//	   
//	   List orderbylist = new ArrayList();
//	   orderbylist.add("chargerates.chargeEffectiveFromDate");
//	   
//	   Page p = dao.getONDCharges(searchCriteria, 0,100,orderbylist);
//	   Collection l = p.getPageData();
//	   Iterator iter = l.iterator();
//	   assertTrue("Num of records", l.size()>0);
//       
//       System.out.println("PAGE TOTAL NUM OF RECORDS :" + p.getTotalNoOfRecords());
//	   
//	   while(iter.hasNext())
//	   {
//		   OriginDestination originDestination = (OriginDestination) iter.next();
//		   System.out.println("OND CODE: " + originDestination.getONDCode());
//		   System.out.println("\n origin: " + originDestination.getOriginAirportCode());
//		   System.out.println("\n destination: " + originDestination.getDestinationAirportCode());
//		   System.out.println("\n ******Charges For the OND*******");
//		   
//		   Set s = originDestination.getCharges();
//		   Iterator iter1 = s.iterator();
//		   while(iter1.hasNext())
//		   {
//			   Charge c = (Charge)iter1.next();
//			   System.out.println("\n Charge Code" + c.getChargeCode());
//			   System.out.println("\n Charge description" + c.getChargeDescription());
//			   System.out.println("\n Applies to infants" + c.getAppliesToInfants());
//			   System.out.println("\n charge categorycode" + c.getChargeCategoryCode());
//			   System.out.println("\n charge groupcode" + c.getChargeGroupCode());
//			   System.out.println("\n ******Charges Rate sFor the Charge*******");
//			   Set s1 = c.getChargeRates();
//			   Iterator iter2 = s1.iterator();
//			   while(iter2.hasNext())
//			   {
//				   ChargeRate cr = (ChargeRate)iter2.next();  
//				   System.out.println("\n chargeRate code " + cr.getChargeRateCode());
//				   System.out.println("\n charge effective from date :" + cr.getChargeEffectiveFromDate().toString());
//				   System.out.println("\n charge effective to date :" + cr.getChargeEffectiveToDate().toString());
//				   System.out.println("\n value in local currency :" + cr.getValueInLocalCurrency());
//				   System.out.println("\n value in Status :" + cr.getStatus());
//			   }
//		   }
//	   }
//  }

//	
//	
//  /**
//   * Test Method to Save and Delete a  charge.
//   * void
//   * @throws Exception
//   */
//  	public void testSaveCharge() throws Exception
//	{
//		ChargeDAO dao = (ChargeDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.CHARGE_DAO);
//		assertNotNull(dao);
//		
//		Charge obj = new Charge();
//		obj.setChargeCode("AA4");
//		obj.setChargeCategoryCode("Global");
//		obj.setChargeGroupCode("Tax");
//				
//		obj.setChargeCategoryValue("aa");
//		obj.setChargeDescription("charge description");
//		obj.setAppliesToInfants(obj.APPLIES_TO_INFANTS_YES);
//		obj.setRefundableChargeFlag(obj.NONE_REFUNDABLE_CHARGE);
//		obj.setStatus(obj.STATUS_ACTIVE);
//		
//		ChargeRate cr = new ChargeRate();
//		cr.setChargeEffectiveFromDate(new Date());
//		cr.setChargeEffectiveToDate(new Date());
//		cr.setChargeValuePercentage(new Integer(12));
//		
//		cr.setValuePercentageFlag("V");
//		
//		Set s = new HashSet();
//		s.add(cr);
//		
//		obj.setChargeRates(s);
//		
//		dao.saveCharge(obj);
//
//		dao.removeCharge("AA4");
//		assertNull(dao.getCharge("AA4"));
//			
//		
//	}
//
//   /**
//    * Test to get All the charges for a criteria and a orderby list.
//    * Note : chargeRate.effectiveFromDate and ...ToDate are compared.
//    */
//   public void testChargesPassingCriteria() throws Exception {   
//	  ChargeSearchCriteria chargeSearchCriteria = new ChargeSearchCriteria();
//
//	  GregorianCalendar fromDate = new GregorianCalendar(2000, GregorianCalendar.JANUARY, 10);
//	  GregorianCalendar toDate = new GregorianCalendar(2000, GregorianCalendar.JUNE, 1);
//
//	  chargeSearchCriteria.setOnlyChargesWithoutRate(false);
//	 chargeSearchCriteria.setFromDate(fromDate.getTime());
//	  chargeSearchCriteria.setToDate(toDate.getTime());
//
//	  List orderbyList = new ArrayList();
//	  
//	  //** have to give the column names in the follwoing way otherwise error will occur *
//	  orderbyList.add("charge.chargeCode");
//	  //orderbyList.add("charge.chargeGroupCode");
//	  Page p  = getChargeDAO().getCharges(chargeSearchCriteria,0,20, orderbyList);
//	  Collection l = p.getPageData();
//	  assertTrue("No Records", l.size()>0);
//      Iterator iter = l.iterator();
//	  System.out.println("Total number of Records" + p.getTotalNoOfRecords());
//	  int num=0;
//	  while(iter.hasNext())
//	  {
//		 Charge charge = (Charge)iter.next(); 
//		 System.out.println(charge.getChargeCode());
//		Collection crs = charge.getChargeRates();
//		 Iterator itercrs = crs.iterator();
//		 while(itercrs.hasNext())
//		 {
//			 ChargeRate chargeRate = (ChargeRate) itercrs.next();
//			 System.out.println("chrge effec from: " + chargeRate.getChargeEffectiveFromDate() +  "\n chrge effec to: " + chargeRate.getChargeEffectiveToDate());
//		 }
//		 //System.out.print(charge.getOndCodes().toString());
//		 num++;
//	  }
//	  System.out.println("Total number of Records" + p.getTotalNoOfRecords());
//	  System.out.println("page number:" + num);
//	     
//   }
//   
   
   /**
    * 
    * private method to lookup the ChargeDAO
    * @return ChargeDAO
    */
   private ChargeDAO getChargeDAO(){
	   return (ChargeDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.CHARGE_DAO);
   }
}
		
	