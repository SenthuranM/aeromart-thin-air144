/*
 * ============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2005 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, 
 * with or without modification, is not permitted without 
 * prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jun 7, 2005
 * 
 * TestCountryDAOImpl.java
 * 
 * ============================================================================
 */
package com.isa.thinair.airpricing.core.persistence.hibernate;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.isa.thinair.airpricing.api.model.OriginDestination;
import com.isa.thinair.airpricing.api.service.AirpricingUtils;

import com.isa.thinair.airpricing.core.persistence.dao.ONDDAO;
import com.isa.thinair.airpricing.core.util.InternalConstants;

import com.isa.thinair.platform.api.util.PlatformTestCase;

/**
 * @author Byorn
 */
public class TestONDDAOImpl extends PlatformTestCase {
		
	public TestONDDAOImpl() {
		super();
	}

	protected void setUp() throws Exception{
		super.setUp();
	}

	/** Test Method to Get OND Codes for a list of ONDCodes. **/
	public void testGetONDsForCodes() throws Exception
	{
		Set ondCodes = new HashSet();
		ondCodes.add("SHJ/CMB");
		ondCodes.add("VVV/AAA");
		List list = getONDDAO().getONDsForCodes(ondCodes);
		Iterator iterator = list.iterator();
		while(iterator.hasNext())
		{
			System.out.println(iterator.next().toString());	
		}
		
	}
 
	/** Test Method to SAVE an OND to the table **/
	public void testSaveOND() throws Exception
	{
		OriginDestination originDestination = new OriginDestination();
		originDestination.setONDCode("AA3/AA4");
		getONDDAO().saveOND(originDestination);
	}
      
   /**
    * 
    * private method to lookup the ONDDAO
    * @return ONDDAO
    */
   private ONDDAO getONDDAO(){
	   return (ONDDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.OND_DAO);
   }
}
		
	