/*
 * ============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2005 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, 
 * with or without modification, is not permitted without 
 * prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jun 7, 2005
 * 
 * TestCountryDAOImpl.java
 * 
 * ============================================================================
 */
package com.isa.thinair.airpricing.core.persistence.hibernate;


import java.util.ArrayList;




import java.util.List;



import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;

import com.isa.thinair.platform.core.controller.ModuleFramework;


import com.isa.thinair.airpricing.api.model.HandlingCharge;
import com.isa.thinair.airpricing.api.service.AirpricingUtils;

import com.isa.thinair.airpricing.core.persistence.dao.HandlingChargeDAO;

import com.isa.thinair.airpricing.core.util.InternalConstants;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;




import junit.framework.TestCase;

/**
 * @author Byorn
 *
 */

public class TestHandlingChargeDAOImpl extends TestCase {

	HandlingCharge handlingCharge = null;
	
	
	public TestHandlingChargeDAOImpl() {
		super();
	}

	public TestHandlingChargeDAOImpl(String arg0) {
		super(arg0);
	}
	
	protected void setUp(){
		//initialize framework
		ModuleFramework.startup();
	}

  
	/**
	 * test method for saving a handlingCharge
	 */
	public void testSaveHandlingcharge() throws Exception
	{
		
		HandlingChargeDAO dao = (HandlingChargeDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.HANDLINGCHARGE_DAO);
				
		assertNotNull(dao);
		
		HandlingCharge obj = new HandlingCharge();
		
		//obj.setHandlingChargId(13);
		obj.setHandlingChargeAmount(12.12);
		obj.setAirportCode("AA2");
		obj.setStatus(obj.STATUS_ACTIVE);
		obj.setValidSegmentCode("AA1/AA2");
				
		dao.saveHandlingCharge(obj);
		//dao.removeHandlingCharge(13);
		//assertNull(dao.getHandlingCharge(13));
			
		
	}
	
	/**
     * test method to get a list of HandlingCharges for a airport code
     */
      public void testGetHandlingchargesForAirportCode() throws Exception
	{
		
    	HandlingChargeDAO dao = (HandlingChargeDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.HANDLINGCHARGE_DAO);
		assertNotNull(dao.getHandlingCharges("AA1"));
			
		
	}
    /**
     * test method to get a list of HandlingCharges. Criteria pased.
     */
   public void testGetHandlingchargesCriteriaWithPaging() throws Exception
   {
	
	   LookupService lookup = LookupServiceFactory.getInstance();
		
		HandlingChargeDAO dao = (HandlingChargeDAO) lookup.getBean("isa:base://modules/airpricing?id=HandlingChargeDAOProxy");
		
			List criteria = new ArrayList();
			ModuleCriterion md = new ModuleCriterion();
			md.setCondition(md.CONDITION_EQUALS);
			md.setFieldName("airportCode");
			List valueList = new ArrayList();
			valueList.add("AA1");
			md.setValue(valueList);
			criteria.add(md);
			
			
			Page p = dao.getHandlingCharges(criteria, 0, 10, null);
			assertTrue("No Records", p.getPageData().size()>0);
	
	   
   }
   
	
   
	
	
	
}
		
	