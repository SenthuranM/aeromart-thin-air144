/*
 * ============================================================================
 * JKCS Software License, Version 1.0
 *
 * Copyright (c) 2005 The John Keells Computer Services.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, 
 * with or without modification, is not permitted without 
 * prior approval from JKCS.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jun 7, 2005
 * 
 * TestCountryDAOImpl.java
 * 
 * ============================================================================
 */
package com.isa.thinair.airpricing.core.persistence.hibernate;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import com.isa.thinair.airpricing.api.model.Fare;
import com.isa.thinair.airpricing.api.model.FareRule;
import com.isa.thinair.airpricing.api.service.AirpricingUtils;
import com.isa.thinair.airpricing.core.persistence.dao.FareDAO;
import com.isa.thinair.airpricing.core.util.InternalConstants;
import com.isa.thinair.platform.api.util.PlatformTestCase;
/**
 * @author Byorn
 */
public class TestFareDAOImpl extends PlatformTestCase {
		
	public TestFareDAOImpl() {
		super();
	}

	protected void setUp() throws Exception{
		super.setUp();
	}
	
	/**
	 * Test DAO method to save a fare.
	 * void
	 * @throws Exception
	 */

//	public void testSaveFare() throws Exception
//	{
//		Fare fare = new Fare();
//		fare.setFareId(9002);
//        fare.setFareAmount(123.22);
//		fare.setBookingCode("AA1");
//		fare.setEffectiveFromDate(new Date());
//		fare.setEffectiveToDate(new Date());
//		fare.setFareRuleId(9003);
//		fare.setOndCode("AA1/AA2");
//		fare.setStatus(fare.STATUS_ACTIVE);
//			
//		getFareDAO().saveFare(fare);
//        assertEquals(9002,getFareDAO().getFare(9002).getFareId());
//        getFareDAO().deleteFare(9002);
//        assertNull("Fare was not deleted properly",getFareDAO().getFare(9002));
//	}
// 
//	
//	/**
//	 * 
//	 */
//	public void testGetEffectiveFares() throws Exception
//	{
//		Collection bookingCodes = new ArrayList();
//		bookingCodes.add("AA1");
//		//bookingCodes.add("AT2");
//		
//		String ondCode = "AA1/AA2";
//		
//		GregorianCalendar departureDate = new GregorianCalendar(2005,Calendar.JANUARY, 1);
//		Collection list = getFareDAO().getEffectiveFares(bookingCodes,ondCode,departureDate.getTime());
//		assertTrue("No records found", list.size()>0);
//		
//	}
//	
//	/**
//	 * 
//	 */
//	public void testGetEffectiveFareswithFareRule() throws Exception
//	{
//		Collection bookingCodes = new ArrayList();
//		bookingCodes.add("AT1");
//		bookingCodes.add("AT2");
//		bookingCodes.add("eeeee");
//		bookingCodes.add("PTS2");
//		
//		
//		String ondCode = "CMB/SHJ";
//		
//		GregorianCalendar departureDate = new GregorianCalendar(2005,Calendar.NOVEMBER,11);
//		Collection list1 = getFareDAO().getEffectiveFaresWithFareRule(bookingCodes,ondCode,departureDate.getTime());
//		Iterator iterator = list1.iterator();
//		
//		while(iterator.hasNext())
//		{
//		
//			Object[] pair = (Object[]) iterator.next();
//			System.out.println(pair[0].toString());
//			FareRule fareRule = (FareRule) pair[1];
//			System.out.println(fareRule.toString());
//			//List list = (List) iterator.next();
//			//String s = (String) list.get(0);
//			//FareRule fareRule = (FareRule) list.get(1);
//			//System.out.println(iterator.next().toString());
//			//FareRule fareRule = (FareRule) iterator.next();
//			//System.out.println(fareRule.toString());
//		}
//		System.out.println(list1.size());
//		assertTrue("No records found", list1.size()>0);
//		
//	}
	/**
	 * Test DAO method to check if fr bc and ond combintation exists
	 * void
	 * @throws Exception
	 */
//	public void testFRBCONDCominationExists() throws Exception
//	{
//		System.out.println(getFareDAO().frbcondCombinationExists("MFR0","PTS1","CMB/SHJ"));
//		//assertTrue("Combination Does not exists",getFareDAO().frbcondCombinationExists(9000,"PTS1","CMB/SHJ")==true);
//	}

	public void testSaveFare() throws Exception
	{
		Fare fare = new Fare();
		fare.setFareId(9002);
        fare.setFareAmount(123.22);
		fare.setBookingCode("AA1");
		fare.setEffectiveFromDate(new Date());
		fare.setEffectiveToDate(new Date());
		fare.setFareRuleId(9003);
		fare.setOndCode("AA1/AA2");
		fare.setStatus(fare.STATUS_ACTIVE);
			
		getFareDAO().saveFare(fare);
        assertEquals(9002,getFareDAO().getFare(9002).getFareId());
        getFareDAO().deleteFare(9002);
        assertNull("Fare was not deleted properly",getFareDAO().getFare(9002));
	}
 
	
	/**
	 * 
	 */
	public void testGetEffectiveFares() throws Exception
	{
		Collection bookingCodes = new ArrayList();
		bookingCodes.add("AA1");
		//bookingCodes.add("AT2");
		
		String ondCode = "AA1/AA2";
		
		GregorianCalendar departureDate = new GregorianCalendar(2005,Calendar.JANUARY, 1);
		Collection list = getFareDAO().getEffectiveFares(bookingCodes,ondCode,departureDate.getTime());
		assertTrue("No records found", list.size()>0);
		
	}
	
	/**
	 * 
	 */
	public void testGetEffectiveFareswithFareRule() throws Exception
	{
		Collection bookingCodes = new ArrayList();
		bookingCodes.add("AT1");
		bookingCodes.add("AT2");
		bookingCodes.add("eeeee");
		bookingCodes.add("PTS2");
		
		
		String ondCode = "CMB/SHJ";
		
		GregorianCalendar departureDate = new GregorianCalendar(2005,Calendar.NOVEMBER,11);
		Collection list1 = getFareDAO().getEffectiveFaresWithFareRule(bookingCodes,ondCode,departureDate.getTime());
		Iterator iterator = list1.iterator();
		
		while(iterator.hasNext())
		{
		
			Object[] pair = (Object[]) iterator.next();
			System.out.println(pair[0].toString());
			FareRule fareRule = (FareRule) pair[1];
			System.out.println(fareRule.toString());
			//List list = (List) iterator.next();
			//String s = (String) list.get(0);
			//FareRule fareRule = (FareRule) list.get(1);
			//System.out.println(iterator.next().toString());
			//FareRule fareRule = (FareRule) iterator.next();
			//System.out.println(fareRule.toString());
		}
		System.out.println(list1.size());
		assertTrue("No records found", list1.size()>0);
		
	}
//	/**
//	 * Test DAO method to check if fr bc and ond combintation exists
//	 * void
//	 * @throws Exception
//	 */
//	public void testFRBCONDCominationExists() throws Exception
//	{
//		System.out.println(getFareDAO().frbcondCombinationExists("MFR0","PTS1","CMB/SHJ"));
//		//assertTrue("Combination Does not exists",getFareDAO().frbcondCombinationExists(9000,"PTS1","CMB/SHJ")==true);
//	}

   
   /**
    * 
    * private method to lookup the FareDAO
    * @return FareDAO
    */
   private FareDAO getFareDAO(){
	   return (FareDAO) AirpricingUtils.getInstance().getLocalBean(InternalConstants.DAOProxyNames.FARE_DAO);
   }
}
		
	