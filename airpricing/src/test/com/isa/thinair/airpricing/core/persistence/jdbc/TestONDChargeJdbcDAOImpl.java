package com.isa.thinair.airpricing.core.persistence.jdbc;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import com.isa.thinair.airpricing.api.criteria.ONDChargeSearchCriteria;
import com.isa.thinair.airpricing.api.dto.ondcharge.OndChargeRateTO;
import com.isa.thinair.airpricing.api.dto.ondcharge.OndChargeTO;
import com.isa.thinair.airpricing.api.dto.ondcharge.OriginDestinationTO;
import com.isa.thinair.airpricing.api.service.AirpricingUtils;
import com.isa.thinair.airpricing.core.persistence.dao.ONDChargeJdbcDAO;
import com.isa.thinair.airpricing.core.util.InternalConstants;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.platform.api.util.PlatformTestCase;

public class TestONDChargeJdbcDAOImpl extends PlatformTestCase {

    public void testGetONDCharges() {
      
    	  ONDChargeJdbcDAO dao = getONDChargeJdbcDAO();
    	  ONDChargeSearchCriteria searchCriteria = new ONDChargeSearchCriteria();
    	   
    	  searchCriteria.setExactAirportsCombinationOnly(true);
    	  searchCriteria.setOriginAirportCode("SHJ");
    	  searchCriteria.setDestinationAirportCode("CMB");
    	  GregorianCalendar g1 = new GregorianCalendar(2005, Calendar.MARCH, 1);
    	  Date d1 = g1.getTime();
    	  searchCriteria.setEffectiveFromDate(d1);
    	 // searchCriteria.setEffectiveToDate(d1);
    	   
    	   //List viapoint = new ArrayList();
    	   //viapoint.add("AA2");
    	   //searchCriteria.setViaAirportCodes(viapoint);
    	   
    	   //List orderbylist = new ArrayList();
    	   //orderbylist.add("ond.ONDCode");
    	   
    	   Page p = dao.getONDCharges(searchCriteria, 1,2,null);
    	   Collection l = p.getPageData();
    	   Iterator iter = l.iterator();
    	   assertTrue("Num of records", l.size()>0);
           System.out.println("num or onds:" + l.size());
           System.out.println("PAGE TOTAL NUM OF RECORDS :" + p.getTotalNoOfRecords());
    	   
    	   while(iter.hasNext())
    	   {
    		   OriginDestinationTO originDestination = (OriginDestinationTO) iter.next();
    		   System.out.println("OND CODE: " + originDestination.getOndCode());
    		   System.out.println("\n origin: " + originDestination.getOriginAirportCode());
    		   System.out.println("\n destination: " + originDestination.getDestinationAirportCode());
    		   System.out.println("\n ******Charges For the OND*******");
    		   
    		   List s = originDestination.getCharges();
    		   Iterator iter1 = s.iterator();
    		   while(iter1.hasNext())
    		   {
    			   OndChargeTO c = (OndChargeTO)iter1.next();
    			   System.out.println("\n Charge Code" + c.getChargeCode());
    			   System.out.println("\n Charge description" + c.getChargeDescription());
    			   //System.out.println("\n Applies to infants" + c.getAppliesToInfants());
    			   System.out.println("\n charge categorycode" + c.getChargeCategoryCode());
    			   System.out.println("\n charge groupcode" + c.getChargeGroupCode());
    			   System.out.println("\n ******Charges Rate sFor the Charge*******");
    			   List s1 = c.getChargeRates();
    			   Iterator iter2 = s1.iterator();
    			   while(iter2.hasNext())
    			   {
    				   OndChargeRateTO cr = (OndChargeRateTO)iter2.next();  
    				   System.out.println("\n chargeRate code " + cr.getChargeRateCode());
    				   System.out.println("\n charge effective from date :" + cr.getChargeEffectiveFromDate());
    				   System.out.println("\n charge effective to date :" + cr.getChargeEffectiveToDate());
    				   
    			   }
    		   }
    	   }
    }

    private ONDChargeJdbcDAO getONDChargeJdbcDAO() {
         return (ONDChargeJdbcDAO) AirpricingUtils.getInstance().getLocalBean(
                InternalConstants.DAOProxyNames.OND_CHARGE_JDBC_DAO);
    }
}
