/**
 * 
 */
package com.isa.thinair.messaging.core.util;

/**
 * @author indika
 * 
 */
public class SMSUtil {
	/**
	 * Simple replace is used if the charcters get too many need to write reg express URLENCODER didnot work
	 * 
	 * @param str
	 * @return
	 */
	public static String getEncodesStr(String str) {
		str = str.replace("&", "%26");
		str = str.replace("+", "%2B");
		str = str.replace("%", "%25");
		str = str.replace("#", "%23");
		str = str.replace("=", "%0A");
		str = str.replace(" ", "%20");
		return str;
	}
}
