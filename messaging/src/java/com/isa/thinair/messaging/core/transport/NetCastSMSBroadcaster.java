package com.isa.thinair.messaging.core.transport;

import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.messaging.api.model.SmsMessage;
import com.isa.thinair.messaging.api.service.MessagingModuleUtils;
import com.isa.thinair.messaging.api.utils.MessagingConstants;
import com.isa.thinair.messaging.core.config.MessagingModuleConfig;
import com.isa.thinair.messaging.core.config.SmsConfig;
import com.isa.thinair.messaging.core.util.SMSAuditor;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.wsclient.api.dto.netcast.NetCastRequest;

public class NetCastSMSBroadcaster extends SMSAuditor implements MessageBroadcaster {

	private final Log log = LogFactory.getLog(getClass());

	private final String SMS_CONFIG_NAME = "netcast_sms";

	@Override
	public void sendMessages(List messageList) {
		MessagingModuleConfig messModConfig = (MessagingModuleConfig) LookupServiceFactory.getInstance().getModuleConfig(
				MessagingConstants.MODULE_NAME);
		SmsConfig smsConfig = (SmsConfig) messModConfig.getSmsConfigurationMap().get(SMS_CONFIG_NAME);

		if (messageList != null) {
			for (Iterator iterator = messageList.iterator(); iterator.hasNext();) {
				SmsMessage smsMessage = (SmsMessage) iterator.next();
				// Send SMS
				sendSMS(smsConfig, smsMessage);
			}
		}
	}

	private void sendSMS(SmsConfig smsConfig, SmsMessage smsMessage) {

		Throwable exToAudit = null;

		if (log.isDebugEnabled()) {
			log.info("[NetCastSMSBroadcaster] Compile SMS Details");
		}

		NetCastRequest request = new NetCastRequest();
		request.setMessage(smsMessage.getBody());
		request.setMobile(smsMessage.getRecipient().replace("-", ""));
		request.setNetCastId(smsConfig.getSenderID());
		request.setMask(new String());

		try {
			log.info("[NetCastSMSBroadcaster] Sendign SMSss to :" + smsMessage.getRecipient() + "   message:"
					+ smsMessage.getBody());
			String resultCode = MessagingModuleUtils.getWSClientBD().sendNetCastSMSMessage(request);

			log.info("[NetCastSMSBroadcaster] SMS Status : " + resultCode + " : " + responseDescription(resultCode));

		} catch (Exception e) {
			exToAudit = e;
			log.error("[NetCastSMSBroadcaster]Fatal transport error: ", e);
		} finally {
			// Perform Auditing
			this.recordReservationAudit(smsMessage.getAuditInfo(), exToAudit, true, smsMessage.getRecipient());
			if (smsMessage.getAuditInfo() != null) {
				smsMessage.getAuditInfo().clear();
			}
			this.updateObjectInfo(smsMessage.getObjectInfo(), exToAudit, true);
		}
	}

	private String responseDescription(String responseCode) {
		String description = "SMS Sent";

		if (responseCode == null) {
			return description;
		} else if (responseCode.equals("RETEMP01")) {
			description = "Netcast ID is empty";
		} else if (responseCode.equals("RETEMP02")) {
			description = "Mobile Number is empty";
		} else if (responseCode.equals("RETEMP03")) {
			description = "Message is empty";
		} else if (responseCode.equals("RETGMS01")) {
			description = "Pending/Queued";
		} else if (responseCode.equals("RETGMS03")) {
			description = "SMS Sending Failed";
		} else if (responseCode.equals("RETGMS04")) {
			description = "Invalid Transaction Reference Number";
		} else if (responseCode.equals("RETVAL01")) {
			description = "Unauthorized IP address";
		} else if (responseCode.equals("RETVAL02")) {
			description = "Unauthorized Netcast ID";
		} else if (responseCode.equals("RETVAL03")) {
			description = "Invalid Mobile Number";
		} else if (responseCode.equals("RETVAL04")) {
			description = "Unrecognized Mobile Number";
		} else if (responseCode.equals("RETVAL05")) {
			description = "Message contains illegal characters";
		} else if (responseCode.equals("RETVAL07")) {
			description = "Unauthorized Custom Mask";
		} else if (responseCode.equals("RETEMP05")) {
			description = "Transaction Reference Number is empty";
		} else if (responseCode.equals("RETEMP06")) {
			description = "Date is empty";
		}

		return description;
	}
}
