package com.isa.thinair.messaging.core.transport;

import java.net.URLEncoder;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HostConfiguration;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.messaging.api.model.SmsMessage;
import com.isa.thinair.messaging.api.utils.MessagingConstants;
import com.isa.thinair.messaging.core.config.MessagingModuleConfig;
import com.isa.thinair.messaging.core.config.SmsConfig;
import com.isa.thinair.messaging.core.util.SMSAuditor;
import com.isa.thinair.platform.api.LookupServiceFactory;

/**
 * Magfa SMS Gateway Integration Implementation
 * 
 * @author Janaka Padukka
 * 
 */
public class MagfaSMSBroadcaster extends SMSAuditor implements MessageBroadcaster {

	private final Log log = LogFactory.getLog(getClass());
	private final String SMS_CONFIG_NAME = "magfa_sms";
	private NameValuePair[] parameters = new NameValuePair[7];

	// Statuses
	private final String SENT_SUCCESS = "success";
	private final String SENT_FAILED = "failed";

	private final static String SMS_ACTION = "Enqueue";

	@Override
	public void sendMessages(List messageList) {
		MessagingModuleConfig messModConfig = (MessagingModuleConfig) LookupServiceFactory.getInstance()
				.getModuleConfig(MessagingConstants.MODULE_NAME);
		SmsConfig smsConfig = (SmsConfig) messModConfig.getSmsConfigurationMap().get(SMS_CONFIG_NAME);

		setCommonParams(smsConfig);

		HttpClient client = new HttpClient();

		// If proxy enabled go through proxy
		if (smsConfig.isProxyEnable()) {
			HostConfiguration hcon = new HostConfiguration();
			hcon.setProxy(smsConfig.getProxyServer(), smsConfig.getProxyPort());
			client.setHostConfiguration(hcon);
		}

		if (messageList != null) {
			for (Iterator iterator = messageList.iterator(); iterator.hasNext();) {
				SmsMessage smsMessage = (SmsMessage) iterator.next();
				// Send SMS
				sendSMS(messModConfig, client, smsConfig, smsMessage);
			}
		}

	}

	/**
	 * Individual SMS Sender
	 * 
	 * @param msgConfig
	 * @param client
	 * @param smsConfig
	 * @param smsMessage
	 */
	private void sendSMS(MessagingModuleConfig msgConfig, HttpClient client, SmsConfig smsConfig, SmsMessage smsMessage) {

		String responseString = null;
		String[] responseStatus;
		String errorMessage = "";
		boolean success = true;
		Throwable xceptionToAudit = null;

		GetMethod method = new GetMethod(smsConfig.getSmsServer());

		// Setting message specific data
		setMessageParams(smsMessage);

		// Setting query string by NameValuePair for automatic URL encoding
		method.setQueryString(parameters);

		// Provide custom retry handler is necessary
		method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(3, false));

		try {

			log.info("[MagfaSMSBroadcaster] Sending SMS ... [recipient=" + smsMessage.getRecipient() + ",smsText="
					+ smsMessage.getBody() + "]");
			// Execute the method.
			int statusCode = client.executeMethod(method);

			if (statusCode != HttpStatus.SC_OK) {
				log.error("[MagfaSMSBroadcaster] Method failed: " + method.getStatusLine());
			}

			// Read the response body.
			byte[] responseBody = method.getResponseBody();
			responseString = new String(responseBody);

			responseStatus = parseResponse(responseString);

			if (SENT_SUCCESS.equals(responseStatus[0])) {
				if (log.isInfoEnabled()) {
					log.info("[MagfaSMSBroadcaster] SMS successfully sent. [recipient=" + smsMessage.getRecipient()
							+ ",smsText=" + smsMessage.getBody() + ",messageId=" + responseStatus[1] + "]");
				}
			} else {
				success = false;
				if (log.isErrorEnabled()) {
					log.error("[MagfaSMSBroadcaster] SMS sending failed. [recipient=" + smsMessage.getRecipient()
							+ ",smsText=" + ",errorMessage=" + responseStatus[1] + "]");
				}
				errorMessage = "Error sending SMS ";
				sendErrorAlert(errorMessage, msgConfig, errorMessage + " : " + responseStatus[1]);
			}

		} catch (Exception e) {
			success = false;
			xceptionToAudit = e;
			log.error("[MagfaSMSBroadcaster] SMS sending failed. [recipient=" + smsMessage.getRecipient() + ",smsText="
					+ smsMessage.getBody() + "]", e);
			errorMessage = "Exception in Sending SMS";
			sendErrorAlert(errorMessage, msgConfig, msgConfig + " : " + e);
		} finally {
			// Performs auditing
			this.recordReservationAudit(smsMessage.getAuditInfo(), xceptionToAudit, success, smsMessage.getRecipient());
			if (smsMessage.getAuditInfo() != null)
				smsMessage.getAuditInfo().clear();
			this.updateObjectInfo(smsMessage.getObjectInfo(), xceptionToAudit, success);
			method.releaseConnection();
		}
	}

	/**
	 * Sets formatted message specific parameters to be included to query string
	 * 
	 * @param smsMessage
	 */
	private void setMessageParams(SmsMessage smsMessage) {
		String smsText = smsMessage.getBody();
		try {
			String unescapedHtml = StringEscapeUtils.unescapeHtml(smsText);
			if (!smsText.equals(unescapedHtml)) {
				if (unescapedHtml.length() > 160) {
					unescapedHtml = unescapedHtml.substring(0, 159);
				}
				smsText = URLEncoder.encode(unescapedHtml, "UTF-8");
			} else if (smsText.length() > 160) {
				smsText = smsText.substring(0, 159);
			}
		} catch(Exception e) {
			if (log.isErrorEnabled()) {
				log.error("SMS escaping error happened ");
			}			
		}
		parameters[5].setValue(buildMobileNumber(smsMessage.getRecipient()));
		parameters[6].setValue(smsText);
	}

	/**
	 * Sets common SMS gateway parameter to be included in query string
	 * 
	 * @param smsConfig
	 */
	private void setCommonParams(SmsConfig smsConfig) {

		parameters[0] = new NameValuePair("service", SMS_ACTION);
		parameters[1] = new NameValuePair("domain", smsConfig.getSmsGateway());
		parameters[2] = new NameValuePair("username", smsConfig.getSmsUserName());
		parameters[3] = new NameValuePair("password", smsConfig.getSmsUserPassword());
		parameters[4] = new NameValuePair("from", smsConfig.getSenderID());
		parameters[5] = new NameValuePair("to", null);
		parameters[6] = new NameValuePair("message", null);

	}

	/**
	 * Formats mobile number
	 * 
	 * @param mobileNo
	 * @return
	 */
	private String buildMobileNumber(String mobileNo) {
		// Appends + to the start of the mobile number
		mobileNo = mobileNo.replace("-", "");
		return "+".concat(mobileNo);
	}

	/**
	 * Parses response.
	 * 
	 * @param responseCode
	 * @return
	 */
	private String[] parseResponse(String responseCode) {

		String[] result = { SENT_FAILED, "No response from SMS gateway" };

		String output;

		if (responseCode != null && responseCode.length() > 0) {

			// Assumption: Error codes are always <= 2 in length successful messages 
			// sends an output which is not parsable to an int.
			// Hence setting anything more than 2 in length to success

			int input = 0;
			if (responseCode.length() <= 2) {
				input = Integer.parseInt(responseCode);
			}

			switch (input) {
			case 1:
				output = "recipientNumber is Null or not valid";
				break;
			case 2:
				output = "SenderNumber is Null or not valid";
				break;
			case 3:
				output = "Encoding parameter is not valid";
				break;
			case 4:
				output = "Mclass parameter is not valid";
				break;
			case 6:
				output = "UDH is not valid";
				break;
			case 10:
				output = "Priority is not valid";
				break;
			case 13:
				output = "Message length is null";
				break;
			case 14:
				output = "You are out of credit";
				break;
			case 15:
				output = "The server had a problem when your message was sent. Resend messages";
				break;
			case 16:
				output = "The account is inactive";
				break;
			case 17:
				output = "Account has expired";
				break;
			case 18:
				output = "Username or password is not valid";
				break;
			case 19:
				output = "The request is not valid";
				break;
			case 20:
				output = "The service that you want is not valid";
				break;
			case 23:
				output = "Due to high traffic, the server is not ready to receive new messages";
				break;
			case 24:
				output = "messageId is not valid";
				break;
			case 25:
				output = "Service name is not valid";
				break;
			default:
				if (responseCode.length() > 2) {
					output = responseCode;
					result[0] = SENT_SUCCESS;
				} else {
					output = "Undefined error";
				}
				break;
			}
			result[1] = output;
		}

		return result;
	}
}
