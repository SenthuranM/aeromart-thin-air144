/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005-08 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 06, 2005 15:28:39
 * 
 * ===============================================================================
 */
package com.isa.thinair.messaging.core.processor;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airreservation.api.dto.ReservationPaxDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.CommonsBusinessException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.TemplateEngine;
import com.isa.thinair.messaging.api.dto.LogDetailDTO;
import com.isa.thinair.messaging.api.model.EmailMessage;
import com.isa.thinair.messaging.api.model.LANMessage;
import com.isa.thinair.messaging.api.model.Message;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.SmsMessage;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.messaging.api.service.MessagingModuleUtils;
import com.isa.thinair.messaging.core.config.MessagingModuleConfig;
import com.isa.thinair.messaging.core.config.TopicConfig;
import com.isa.thinair.messaging.core.transport.MessageBroadcasterFactory;
import com.isa.thinair.platform.api.LookupServiceFactory;

/**
 * @author sumudu
 */
public class MessageManager {

	private final Log log = LogFactory.getLog(getClass());

	private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS");

	private static TemplateEngine engine = new TemplateEngine();

	private static final String EMAIL_CONCATENATOR = ",";

	public void processMessages(List profiles) {

		Iterator ite = profiles.iterator();
		MessageProfile profile;

		// if the it comes in a combination
		List<EmailMessage> emailMessagesList = new ArrayList<EmailMessage>();
		List<SmsMessage> smsMessagesList = new ArrayList<SmsMessage>();

		StringWriter w = null;
		ByteArrayInputStream inputStream = null;

		Object objMessage = null;
		SmsMessage smsMessage = null;
		EmailMessage emailMessage = null;

		HashMap topicParam = null;
		List usersList = null;
		Topic topic = null;
		String topicName = null;
		TopicConfig topicConfig = null;
		String templateFileName = null;
		String localeSpecificTemplateFileName = null;
		String tranport = null;

		StringBuffer htmlEmailContent = new StringBuffer();

		while (ite.hasNext()) {
			objMessage = ite.next();

			if (objMessage instanceof EmailMessage) {
				emailMessage = (EmailMessage) objMessage;
				emailMessagesList.add(emailMessage);

			} else if (objMessage instanceof SmsMessage) {
				smsMessage = (SmsMessage) objMessage;
				smsMessagesList.add(smsMessage);
			} else if (objMessage instanceof LANMessage) {
				// just in case
			} else {

				/***************************************************/

				try {
					// get details from profile
					profile = (MessageProfile) objMessage;

					// get users(message receivers)
					usersList = profile.getUserMessagings();
					topic = profile.getTopic();
					topicName = topic.getTopicName();
					topicParam = topic.getTopicParams();

					if (!topicParam.containsKey("commonTemplatingDTO")) {
						topicParam.put("commonTemplatingDTO", AppSysParamsUtil.composeCommonTemplatingDTO(null));
					}

					if (log.isDebugEnabled()) {
						log.debug("Processing message for topic name = " + topicName);
					}

					topicConfig = (TopicConfig) ((MessagingModuleConfig) MessagingModuleUtils.getInstance().getModuleConfig())
							.getTopicConfigurationMap().get(topicName);

					if (topicConfig == null || topicConfig.equals("")) {
						throw new CommonsBusinessException("messaging.invalid.topicname", "messaging.desc");
					}

					// Find the tranport type(LAN/Email/SMS)
					tranport = topicConfig.getDefaultTransportType();
					templateFileName = topicConfig.getTemplateFileName();

					localeSpecificTemplateFileName = templateFileName;

					w = new StringWriter();

					StringWriter templateWriter = new StringWriter();
					ByteArrayInputStream templateInputStream = null;

					if ((topic.getLocale() != null)
							&& !topicName.equals(ReservationInternalConstants.PnrTemplateNames.INTERLINE_ITINERARY_EMAIL)) {
						String prefLang = (topic.getLocale().toString()).toUpperCase();
						if (templateFileName.indexOf(".") > 0) {
							localeSpecificTemplateFileName = templateFileName.substring(0, templateFileName.indexOf(".")) + "_"
									+ prefLang + templateFileName.substring(templateFileName.indexOf("."));
						} else {
							localeSpecificTemplateFileName = templateFileName + "_" + prefLang;
						}
					}
					engine.writeTemplate(topicParam, localeSpecificTemplateFileName, templateWriter);
					byte templateArr[] = templateWriter.toString().getBytes(Message.CONTENT_BYTE_ARRAY_TYPE);
					templateInputStream = new ByteArrayInputStream(templateArr);

					boolean isUserEdited = false;
					// check for user edited content or not
					if (topicParam.get("userEdited") != null && ((Boolean) topicParam.get("userEdited"))) {
						isUserEdited = true;
						w.write((String) topicParam.get("message"));

					} else {
						w = templateWriter;
					}

					if (log.isDebugEnabled()) {
						log.debug("Content read from template :: " + w.toString());
					}
					// Convert to input stream
					byte arr[] = w.toString().getBytes(Message.CONTENT_BYTE_ARRAY_TYPE);
					inputStream = new ByteArrayInputStream(arr);

					// parse the message
					MessageParser parser = new MessageParser();

					if (tranport.equalsIgnoreCase("EMAIL")) {

						emailMessage = new EmailMessage();

						if (isUserEdited) {
							EmailMessage e = new EmailMessage();
							parser.parse(templateInputStream, e);
							emailMessage.setSubject(e.getSubject());
							emailMessage.setBody(w.toString());
						} else {
							parser.parse(inputStream, emailMessage);
						}

						emailMessagesList = addEmailMessage(emailMessage, usersList, templateFileName, topic, topicConfig,
								topicName, topicParam, emailMessage.getBody().getBytes(Message.CONTENT_BYTE_ARRAY_TYPE),
								emailMessagesList);

						htmlEmailContent.append(new String(emailMessage.getBody().getBytes(Message.CONTENT_BYTE_ARRAY_TYPE)));

					} else if (tranport.equalsIgnoreCase("SMS")) {

						smsMessage = new SmsMessage();

						if (isUserEdited) {
							SmsMessage s = new SmsMessage();
							parser.parse(templateInputStream, s);
							smsMessage.setSubject(s.getSubject());
							smsMessage.setBody(w.toString());
						} else {
							parser.parse(inputStream, smsMessage);
						}

						smsMessagesList = addSMSMessage(smsMessage, usersList, smsMessagesList, topic);

					} else if (tranport.equalsIgnoreCase("LAN")) {
						// [TODO:implement]
						// LANMessage lanMsg = (LANMessage) message;
					}
				} catch (Exception e) {
					log.error("Preparing message failed", e);
				} finally {
					if (topicParam != null)
						topicParam.clear();

					if (w != null) {
						try {
							w.close();
						} catch (IOException e) {
							log.error("Closing message template file stream failed", e);
						}
					}
					if (inputStream != null) {
						try {
							inputStream.close();
						} catch (IOException e) {
							log.error("Closing message template file stream failed", e);
						}
					}
				}

				/****************************************************/

			}

		}

		if (emailMessagesList.size() > 0) {
			MessageBroadcasterFactory.getBroadcaster("EMAIL").sendMessages(emailMessagesList);
			emailMessagesList.clear();
		}
		if (smsMessagesList.size() > 0) {
			MessageBroadcasterFactory.getBroadcaster("SMS").sendMessages(smsMessagesList);
			smsMessagesList.clear();
		}

	}

	public void recordMessage(LogDetailDTO logDetailDTO) {

		StringWriter w = new StringWriter();
		PrintStream p = null; // declare a print stream object
		FileOutputStream out = null; // declare a file output object

		try {
			TopicConfig topicConfig = (TopicConfig) ((MessagingModuleConfig) LookupServiceFactory.getInstance().getModuleConfig(
					"messaging")).getTopicConfigurationMap().get(logDetailDTO.getTemplateName());

			if (topicConfig == null || topicConfig.equals("")) {
				throw new CommonsBusinessException("messaging.invalid.topicname", "messaging.desc");
			}

			// fill the mail template
			TemplateEngine engine = new TemplateEngine();
			String fileName = topicConfig.getTemplateFileName();
			String localeSpecificFileName = fileName;

			// message locale is NOT the default locale
			if ((logDetailDTO.getLocale() != null)
					&& (!logDetailDTO.getLocale().getLanguage().equals(Locale.getDefault().getLanguage()))) {

				if (fileName.indexOf(".") > 0) {
					localeSpecificFileName = fileName.substring(0, fileName.indexOf(".")) + "_"
							+ logDetailDTO.getLocale().getLanguage().trim().toUpperCase()
							+ fileName.substring(fileName.indexOf("."));

				} else {
					localeSpecificFileName = fileName + "_" + logDetailDTO.getLocale().getLanguage().trim();
				}
			}

			engine.writeTemplate(logDetailDTO.getTemplateParams(), localeSpecificFileName, w);

			String directoryPath = logDetailDTO.getSaveFileName().substring(0, logDetailDTO.getSaveFileName().lastIndexOf("/"));

			// Create directory
			File newDir = new File(directoryPath);
			newDir.mkdirs();

			// Create a new file output stream
			// connected to file
			out = new FileOutputStream(logDetailDTO.getSaveFileName());

			// Connect print stream to the output stream
			p = new PrintStream(out);

			p.println(w.toString());

		} catch (Exception e) {
			log.error("Preparing message failed", e);
		} finally {
			try {
				if (p != null)
					p.close();
				if (out != null)
					out.close();
				w.close();
			} catch (IOException e) {
				log.error("Closing message template file stream failed", e);
			}
		}
	}

	private String getUsersFromList(List usersList) {
		String addList = "";
		for (int r = 0; r < usersList.size(); r++) {
			UserMessaging u = (UserMessaging) usersList.get(r);
			addList += u.getToAddres();
			if (!(r == usersList.size() - 1))
				addList += EMAIL_CONCATENATOR;
		}
		return addList;
	}

	private List<EmailMessage> addEmailMessage(EmailMessage message, List usersList, String templateFileName, Topic topic,
			TopicConfig topicConfig, String topicName, HashMap topicParam, byte[] arr, List<EmailMessage> emailMessagesList) {

		String bodyAttachmentName = null;
		OutputStream os = null;

		if (topic.isAttachMessageBody()) {
			Date date = new Date(System.currentTimeMillis());

			String pnr = "";
			if (topic.getTopicName().equals("itinerary_email")) {
				if (topicParam != null && topicParam.containsKey("reservationPaxDTO")) {
					ReservationPaxDTO paxDTO = (ReservationPaxDTO) topicParam.get("reservationPaxDTO");
					if (paxDTO != null) {
						pnr = paxDTO.getPnr();
						if (log.isDebugEnabled()) {
							log.debug("Preparing itinerary [pnr=" + pnr + "]");
						}
					}
				}
			}

			String templateName = templateFileName.substring(0, templateFileName.indexOf("."));
			if (templateName.lastIndexOf("/") > 0) {
				templateName = templateName.substring(templateName.lastIndexOf("/") + 1);
			}

			bodyAttachmentName = templateName + "_" + pnr + "_" + dateFormat.format(date) + ".htm";

			// added By Sandun---------------------------------------START----------------------------
			if (topicConfig.getAttachmentDefaultContentType() != null
					&& topicConfig.getAttachmentDefaultContentType().equalsIgnoreCase(Message.ATTACHMENT_TYPE_PDF)) {
				bodyAttachmentName = templateName + "_" + pnr + "_" + dateFormat.format(date) + ".pdf";
				arr = PDFComposer.xhtmlPdfConversion(arr);
				message.setAttachmentType(Message.ATTACHMENT_TYPE_PDF);
			}// Added By Sandun end ----------------END--------------------------------------------------
		}

		// EmailMessage msg = (EmailMessage) message;

		message.setToAddress(getUsersFromList(usersList));
		message.setFromAddress(topicConfig.getDefaultFromAddress());
		message.setMailServerConfigurationName(topic.getMailServerConfigurationName() != null ? topic
				.getMailServerConfigurationName() : topicConfig.getDefaultMailServerConfigurationName());
		message.setContentType(topic.getContentType() != null ? topic.getContentType() : topicConfig.getDefaultContentType());
		message.setAttachmentFileNames(topic.getAttachmentFileNames());
		if (topic.isAttachMessageBody()) {
			message.setAttachBody(topic.isAttachMessageBody());
			message.setBodyAttachmentName(bodyAttachmentName);
			message.setMsgBodyByteArray(arr);
			message.setMsgBodyByteArrayType(Message.CONTENT_BYTE_ARRAY_TYPE);
		}
		message.setAuditInfo(topic.getAuditInfo());
		message.setObjectInfo(topic.getObjectInfo());

		emailMessagesList.add(message);
		return emailMessagesList;

	}

	private List<SmsMessage> addSMSMessage(SmsMessage message, List usersList, List<SmsMessage> smsMessagesList, Topic topic) {
		message.setRecipient(getUsersFromList(usersList));
		message.setAuditInfo(topic.getAuditInfo());
		message.setObjectInfo(topic.getObjectInfo());

		smsMessagesList.add(message);
		return smsMessagesList;
	}
}
