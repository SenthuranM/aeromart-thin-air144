package com.isa.thinair.messaging.core.service.bd;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseServiceDelegate;
import com.isa.thinair.messaging.api.model.PNRGOVMessageData;
import com.isa.thinair.messaging.api.service.MessagingModuleUtils;

public class SavePnrGovMessageViaMDB extends PlatformBaseServiceDelegate {
	private final Log log = LogFactory.getLog(getClass());

	private static final java.lang.String DESTINATION_JNDI_NAME = "queue/pnrGovMsgQueue";
	private static final java.lang.String CONNECTION_FACTORY_JNDI_NAME = "ConnectionFactory";

	public void saveMessageViaMDB(PNRGOVMessageData messageData) {
		try {
			sendMessage(messageData, MessagingModuleUtils.getInstance().getModuleConfig().getJndiProperties(),
					DESTINATION_JNDI_NAME, CONNECTION_FACTORY_JNDI_NAME);
		} catch (ModuleException moduleException) {
			log.error("Sending message via jms failed", moduleException);
		}
	}
}
