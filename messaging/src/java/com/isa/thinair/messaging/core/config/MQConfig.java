package com.isa.thinair.messaging.core.config;

import java.io.Serializable;

public class MQConfig implements Serializable {

	private static final long serialVersionUID = 1L;

	private String pnrGovMQHostName;
	
	private int pnrGovMQListeningPort;
	
	private String pnrGovMQChannel;
	
	private String pnrGovMQManager;
	
	private String pnrGovMQName;

	public String getPnrGovMQHostName() {
		return pnrGovMQHostName;
	}

	public void setPnrGovMQHostName(String pnrGovMQHostName) {
		this.pnrGovMQHostName = pnrGovMQHostName;
	}

	public int getPnrGovMQListeningPort() {
		return pnrGovMQListeningPort;
	}

	public void setPnrGovMQListeningPort(int pnrGovMQListeningPort) {
		this.pnrGovMQListeningPort = pnrGovMQListeningPort;
	}

	public String getPnrGovMQChannel() {
		return pnrGovMQChannel;
	}

	public void setPnrGovMQChannel(String pnrGovMQChannel) {
		this.pnrGovMQChannel = pnrGovMQChannel;
	}

	public String getPnrGovMQManager() {
		return pnrGovMQManager;
	}

	public void setPnrGovMQManager(String pnrGovMQManager) {
		this.pnrGovMQManager = pnrGovMQManager;
	}

	public String getPnrGovMQName() {
		return pnrGovMQName;
	}

	public void setPnrGovMQName(String pnrGovMQName) {
		this.pnrGovMQName = pnrGovMQName;
	}
	
}
