package com.isa.thinair.messaging.core.persistence.dao;

import java.util.List;

import com.isa.thinair.messaging.api.dto.UpdateEmailStatusInfoDTO;
import com.isa.thinair.messaging.api.model.EmailMessage;

public interface MessagingJdbcDAO {

	public void saveEmail(EmailMessage message);

	public List loadEmails();

	public void deleteEmail(Integer id);

	public void updatePaxSegmentSSREmailSentStatus(UpdateEmailStatusInfoDTO updateInfoDTO);
	
	public boolean isMessageExists(Integer id);

}
