/**
 * 
 */
package com.isa.thinair.messaging.core.transport;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HostConfiguration;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.messaging.api.model.SmsMessage;
import com.isa.thinair.messaging.api.utils.MessagingConstants;
import com.isa.thinair.messaging.core.config.MessagingModuleConfig;
import com.isa.thinair.messaging.core.config.SmsConfig;
import com.isa.thinair.messaging.core.util.SMSAuditor;
import com.isa.thinair.messaging.core.util.SMSUtil;
import com.isa.thinair.platform.api.LookupServiceFactory;

/**
 * @author nafly
 * 
 */
public class AxonSMSBroadcaster extends SMSAuditor implements MessageBroadcaster {

	private final Log log = LogFactory.getLog(getClass());
	private final String SMS_CONFIG_NAME = "axon_sms";
	private String url = null;

	@Override
	public void sendMessages(List messageList) {
		MessagingModuleConfig messModConfig = (MessagingModuleConfig) LookupServiceFactory.getInstance().getModuleConfig(
				MessagingConstants.MODULE_NAME);
		SmsConfig smsConfig = (SmsConfig) messModConfig.getSmsConfigurationMap().get(SMS_CONFIG_NAME);

		url = buildGetRequest(smsConfig);

		if (messageList != null) {
			for (Iterator iterator = messageList.iterator(); iterator.hasNext();) {
				SmsMessage smsMessage = (SmsMessage) iterator.next();
				// Send SMS
				sendSMS(smsConfig, smsMessage);
			}

		}
	}

	private String buildGetRequest(SmsConfig smsConfig) {
		StringBuilder sb = new StringBuilder();

		sb.append(smsConfig.getSmsServer());
		sb.append("?apikey=" + smsConfig.getApiKey());
		sb.append("&msgtype=" + smsConfig.getSmsMode());
		sb.append("&sid=" + SMSUtil.getEncodesStr(smsConfig.getSenderID()));
		sb.append("&dlr=" + smsConfig.getSmsMode()); // do we need delivery report or no
		return sb.toString();
	}

	private void sendSMS(SmsConfig smsConfig, SmsMessage smsMessage) {
		String smsUrl = null;
		// Create an instance of HttpClient.
		HttpClient client = new HttpClient();
		Throwable xceptionToAudit = null;

		// If proxy enabled go through proxy
		if (smsConfig.isProxyEnable()) {
			HostConfiguration hcon = new HostConfiguration();
			hcon.setProxy(smsConfig.getProxyServer(), smsConfig.getProxyPort());
			client.setHostConfiguration(hcon);
		}

		smsUrl = url + getMessage(smsMessage);
		if (log.isDebugEnabled()) {
			log.debug("[AxonSMSBroadcaster] Compile SMS URL :" + smsUrl);
		}

		// Create a method instance.
		GetMethod method = new GetMethod(smsUrl);

		// Provide custom retry handler is necessary
		method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(3, false));

		try {
			log.info("[AxonSMSBroadcaster] Sendign SMSss to :" + smsMessage.getRecipient() + "   message:" + smsMessage.getBody());
			// Execute the method.
			int statusCode = client.executeMethod(method);

			if (statusCode != HttpStatus.SC_OK) {
				log.info("[AxonSMSBroadcaster] Method failed: " + method.getStatusLine());
			}

			// Read the response body.
			byte[] responseBody = method.getResponseBody();

			// Deal with the response.
			// Use caution: ensure correct character encoding and is not binary data
			log.info("[AxonSMSBroadcaster] SMS Sent" + new String(responseBody));

		} catch (HttpException e) {
			xceptionToAudit = e;
			log.error("[AxonSMSBroadcaster]Fatal protocol violation: ", e);
		} catch (IOException e) {
			xceptionToAudit = e;
			log.error("[AxonSMSBroadcaster]Fatal transport error: ", e);
		} catch (Exception e) {
			xceptionToAudit = e;
			log.error("[AxonSMSBroadcaster]Fatal transport error: ", e);
		} finally {
			// Perform Auditing
			this.recordReservationAudit(smsMessage.getAuditInfo(), xceptionToAudit, true, smsMessage.getRecipient());
			if (smsMessage.getAuditInfo() != null)
				smsMessage.getAuditInfo().clear();
			this.updateObjectInfo(smsMessage.getObjectInfo(), xceptionToAudit, true);

			// Release the connection.
			method.releaseConnection();
		}
	}

	private String getMessage(SmsMessage smsMessage) {
		StringBuilder sb = new StringBuilder();
		String smsText = smsMessage.getBody();
		if (smsText.length() > 160) {
			smsText = smsText.substring(0, 159);
		}
		smsText = SMSUtil.getEncodesStr(smsText);
		sb.append("&mobiles=" + buildMobileNumber(smsMessage.getRecipient()));
		sb.append("&msg=" + smsText);
		return sb.toString();
	}

	private String buildMobileNumber(String mobileNo) {
		return mobileNo.replaceAll("-", "");
	}
}
