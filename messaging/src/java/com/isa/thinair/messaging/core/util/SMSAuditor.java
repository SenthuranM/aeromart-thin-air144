/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * 
 * ===============================================================================
 */
package com.isa.thinair.messaging.core.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.alerting.api.model.FlightPnrNotification;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.service.AuditorBD;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.messaging.api.model.EmailMessage;
import com.isa.thinair.messaging.api.service.MessagingModuleUtils;
import com.isa.thinair.messaging.core.config.MessagingModuleConfig;
import com.isa.thinair.messaging.core.config.TopicConfig;
import com.isa.thinair.messaging.core.transport.EmailBroadcaster;

/**
 * Class to Retrieve The SMS BroadCaster
 * 
 * @author Thushara
 * 
 */
public class SMSAuditor {

	private final Log log = LogFactory.getLog(getClass());

	public void sendErrorAlert(String strSubject, MessagingModuleConfig msgConfig, String message) {

		EmailMessage msg = null;
		TopicConfig topicConfig = null;
		String emailAddress = null;
		List<EmailMessage> messagesList = new ArrayList<EmailMessage>();

		topicConfig = (TopicConfig) ((MessagingModuleConfig) MessagingModuleUtils.getInstance().getModuleConfig())
				.getTopicConfigurationMap().get("sms_failures");

		Properties props = msgConfig.getSmsFailList();
		Enumeration<Object> enumeration = props.elements();
		while (enumeration.hasMoreElements()) {
			msg = new EmailMessage();
			emailAddress = (String) enumeration.nextElement();
			msg.setToAddress(emailAddress);
			msg.setFromAddress(topicConfig.getDefaultFromAddress());
			msg.setContentType(topicConfig.getDefaultContentType());
			msg.setSubject(strSubject);
			msg.setBody("<HTML><BODY>" + message + "</BODY></HTML>");
			msg.setMailServerConfigurationName(topicConfig.getDefaultMailServerConfigurationName());
			messagesList.add(msg);
		}

		EmailBroadcaster emailBroadcaster = new EmailBroadcaster();
		emailBroadcaster.sendMessages(messagesList);

	}

	/**
	 * (1) Will Check Type of ReservationAudit (2) If exception is null, status is success else false and will get root
	 * cause!
	 * 
	 * @param auditInfo
	 * @param e
	 */
	public void recordReservationAudit(Collection auditInfo, Throwable e, boolean success, String toAddress) {
		log.debug("|||Going to Record ReservationAudit");

		try {
			if (auditInfo != null && auditInfo.size() > 0) {
				AuditorBD auditorBD = MessagingModuleUtils.getAuditorBD();
				Collection colReservationAudit = new ArrayList();
				ReservationAudit reservationAudit;
				Object element;

				for (Iterator itObject = auditInfo.iterator(); itObject.hasNext();) {
					element = (Object) itObject.next();

					// If auditInfo is type of ReservationAudit Only proceed..!
					if (element != null && element instanceof ReservationAudit) {
						reservationAudit = (ReservationAudit) element;

						if (AuditTemplateEnum.NOTIFY_PNR.getCode().equals(reservationAudit.getModificationType())) {
							// Which means SMS successfully sent
							if (e == null && success) {
								reservationAudit.addContentMap(
										AuditTemplateEnum.TemplateParams.EmailFlightAlterations.SENT_STATUS,
										"<b>Notification sent successfully</b>");
								reservationAudit.addContentMap(
										AuditTemplateEnum.TemplateParams.EmailFlightAlterations.STATUS_REASONS, "Address: "
												+ toAddress);
							}
							// Which means SMS failure
							else {
								reservationAudit.addContentMap(
										AuditTemplateEnum.TemplateParams.EmailFlightAlterations.SENT_STATUS,
										"<b>Notification sending Failed</b>");
								reservationAudit.addContentMap(
										AuditTemplateEnum.TemplateParams.EmailFlightAlterations.STATUS_REASONS, "Address: "
												+ toAddress + " " + getRootCauseDetails(e));

								log.error(reservationAudit.getPnr(), e);
							}
						}

						colReservationAudit.add(element);
					}
					// FIXME Other objects are yet to be implemented
					else {
						log.debug("|||FIXME! Auditing Other objects are yet to be implemented");
					}
				}

				// Saves the audit collection
				auditorBD.audit(colReservationAudit);
			} else {
				log.debug("|||AuditInfo Collection Not Found!");
			}
		} catch (Throwable recordReservationAuditException) {
			log.error("Error in Auditing In Messaging Module", recordReservationAuditException);
		}
	}

	/**
	 * Returns Root cause information
	 * 
	 * @param e
	 * @return
	 */
	private String getRootCauseDetails(Throwable e) {
		String genericErrorMsg = "General Failure";
		if (e == null) {
			return genericErrorMsg;
		}

		boolean hasCause = true;
		Throwable t = e;

		while (hasCause) {
			if (t.getCause() == null) {
				hasCause = false;
				genericErrorMsg = t.getMessage();

			} else {
				t = e.getCause();
			}
		}

		String message = genericErrorMsg.replaceAll("\\s*", "");
		try {
			return formatString(message);
		} catch (Exception e1) {

			log.error("Error in Formatting Error message for Auditing", e1);
			return message;
		}

	}

	/**
	 * Format String
	 * 
	 * @param msg
	 * @return
	 */
	public String formatString(String msg) {
		String strMsg = "";
		Iterator iterator = splitThisString(msg, 55).iterator();

		while (iterator.hasNext()) {
			strMsg += iterator.next() + "<br>";
		}
		return strMsg;
	}

	/**
	 * 
	 * @param strToSplit
	 * @param limit
	 * @return
	 */
	public static Collection splitThisString(String strToSplit, int limit) {
		Collection<String> results = new ArrayList<String>();
		if (strToSplit.length() >= limit) {
			String partOne = (String) strToSplit.substring(0, limit);
			String partTwo = strToSplit.substring(limit);

			results.add(partOne);
			if (partTwo.length() > limit) {
				results.addAll(splitThisString(partTwo, limit));
			} else {
				results.add(partTwo);
			}
		} else {
			results.add(strToSplit);
		}
		return results;
	}

	/**
	 * (1) Will Check Type of Object (2) If exception is null, status is success else false!
	 * 
	 * @param objectInfo
	 * @param e
	 */
	public void updateObjectInfo(Collection objectInfo, Throwable e, boolean success) {
		log.debug("|||Going to Record ReservationAudit");
		try {
			if (objectInfo != null && objectInfo.size() > 0) {
				Object element;

				for (Iterator itObject = objectInfo.iterator(); itObject.hasNext();) {
					element = (Object) itObject.next();

					// If objectInfo is type of FlightPnrNotification Only proceed..!
					if (element != null && element instanceof FlightPnrNotification) {
						FlightPnrNotification objFltPnr = (FlightPnrNotification) element;

						// Which means SMS successfully sent
						if (e == null && success) {
							objFltPnr.setDeliveryStatus("D"); // SMS Delivered successfully
						}
						// Which means SMS failure
						else {
							objFltPnr.setDeliveryStatus("F"); // Sending SMS Failed
						}
						// Saves the object collection
						MessagingModuleUtils.getAlertingBD().saveFlightPnrNotification(objFltPnr);
					}
					// FIXME Other objects are yet to be implemented
					else {
						log.debug("|||FIXME! Other objects are yet to be implemented");
					}
				}

			} else {
				log.debug("|||ObjectInfo Collection Not Found!");
			}
		} catch (Throwable ex) {
			log.error("Error in updating object In Messaging Module", ex);
		}
	}

}
