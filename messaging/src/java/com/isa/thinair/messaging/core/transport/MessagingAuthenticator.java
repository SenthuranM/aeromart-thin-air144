package com.isa.thinair.messaging.core.transport;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

public class MessagingAuthenticator extends Authenticator {
	private String userName;
	private String password;

	public MessagingAuthenticator(String userName, String password) {
		this.userName = userName;
		this.password = password;
	}

	public PasswordAuthentication getPasswordAuthentication() {
		return new PasswordAuthentication(userName, password);
	}
}
