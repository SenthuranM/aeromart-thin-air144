package com.isa.thinair.messaging.core.config;

/**
 * @author Nasly
 */
public class TopicConfig {

	private String topicName;

	private String defaultFromAddress;

	private String defaultTransportType;

	private String defaultContentType;

	private String templateFileName;

	private String defaultMailServerConfigurationName;

	private String attachmentDefaultContentType;

	public String getDefaultFromAddress() {
		return defaultFromAddress;
	}

	public void setDefaultFromAddress(String defaultFromAddress) {
		this.defaultFromAddress = defaultFromAddress;
	}

	public String getDefaultTransportType() {
		return defaultTransportType;
	}

	public void setDefaultTransportType(String defaultTransportType) {
		this.defaultTransportType = defaultTransportType;
	}

	public String getDefaultContentType() {
		return defaultContentType;
	}

	public void setDefaultContentType(String defaultContentType) {
		this.defaultContentType = defaultContentType;
	}

	public String getTemplateFileName() {
		return templateFileName;
	}

	public void setTemplateFileName(String templateFileName) {
		this.templateFileName = templateFileName;
	}

	public String getTopicName() {
		return topicName;
	}

	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}

	public String getDefaultMailServerConfigurationName() {
		return defaultMailServerConfigurationName;
	}

	public void setDefaultMailServerConfigurationName(String defaultMailServerConfigurationName) {
		this.defaultMailServerConfigurationName = defaultMailServerConfigurationName;
	}

	public String getAttachmentDefaultContentType() {
		return attachmentDefaultContentType;
	}

	public void setAttachmentDefaultContentType(String attachmentDefaultContentType) {
		this.attachmentDefaultContentType = attachmentDefaultContentType;
	}

}
