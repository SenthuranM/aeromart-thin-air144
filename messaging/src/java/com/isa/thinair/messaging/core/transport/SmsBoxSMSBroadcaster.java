package com.isa.thinair.messaging.core.transport;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HostConfiguration;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.isa.thinair.messaging.api.model.SmsMessage;
import com.isa.thinair.messaging.api.utils.MessagingConstants;
import com.isa.thinair.messaging.core.config.MessagingModuleConfig;
import com.isa.thinair.messaging.core.config.SmsConfig;
import com.isa.thinair.messaging.core.util.SMSAuditor;
import com.isa.thinair.platform.api.LookupServiceFactory;

/**
 * SMSBOX SMS Gateway Integration Implementation
 * 
 * @author Dilshan
 * 
 */
public class SmsBoxSMSBroadcaster extends SMSAuditor implements MessageBroadcaster {

	private final Log log = LogFactory.getLog(getClass());
	private final String SMS_CONFIG_NAME = "sms_box";
	private NameValuePair[] parameters = new NameValuePair[9];

	private final static String SUCCESS = "success";
	private final static String MESSAGE = "message";
	private final static String MESSAGE_ID = "messageId";
	private final static String SUCCESS_TRUE = "true";
	private final static String SUCCESS_FALSE = "false";

	@Override
	public void sendMessages(List messageList) {
		MessagingModuleConfig messModConfig = (MessagingModuleConfig) LookupServiceFactory.getInstance().getModuleConfig(
				MessagingConstants.MODULE_NAME);
		SmsConfig smsConfig = (SmsConfig) messModConfig.getSmsConfigurationMap().get(SMS_CONFIG_NAME);

		setCommonParams(smsConfig);

		HttpClient client = new HttpClient();

		// If proxy enabled go through proxy
		if (smsConfig.isProxyEnable()) {
			HostConfiguration hcon = new HostConfiguration();
			hcon.setProxy(smsConfig.getProxyServer(), smsConfig.getProxyPort());
			client.setHostConfiguration(hcon);
		}

		if (messageList != null) {
			for (Iterator iterator = messageList.iterator(); iterator.hasNext();) {
				SmsMessage smsMessage = (SmsMessage) iterator.next();
				// Send SMS
				sendSMS(messModConfig, client, smsConfig, smsMessage);
			}
		}

	}

	/**
	 * Individual SMS Sender
	 * 
	 * @param msgConfig
	 * @param client
	 * @param smsConfig
	 * @param smsMessage
	 */
	private void sendSMS(MessagingModuleConfig msgConfig, HttpClient client, SmsConfig smsConfig, SmsMessage smsMessage) {

		String responseString = null;
		Map<String, String> responseStatus;
		String errorMessage = "";
		boolean success = true;
		Throwable xceptionToAudit = null;

		GetMethod method = new GetMethod(smsConfig.getSmsServer());

		// Setting message specific data
		setMessageParams(smsMessage);

		// Setting query string by NameValuePair for automatic URL encoding
		method.setQueryString(parameters);

		// Provide custom retry handler is necessary
		method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(3, false));

		try {

			log.info("[SmsBoxSMSBroadcaster] Sending SMS ... [recipient=" + smsMessage.getRecipient() + ",smsText="
					+ smsMessage.getBody() + "]");
			// Execute the method.
			int statusCode = client.executeMethod(method);

			if (statusCode != HttpStatus.SC_OK) {
				log.info("[SmsBoxSMSBroadcaster] Method failed: " + method.getStatusLine());
			}

			// Read the response body.
			byte[] responseBody = method.getResponseBody();
			responseString = new String(responseBody);
			log.info("[SmsBoxSMSBroadcaster]responseString :  " + responseString);
			if (statusCode == HttpStatus.SC_OK) {
				responseStatus = parseResponse(responseString);
				if (responseStatus.get(SUCCESS).equals(SUCCESS_TRUE)) {
					if (log.isInfoEnabled()) {
						log.info("[SmsBoxSMSBroadcaster] SMS successfully sent. [recipient=" + smsMessage.getRecipient()
								+ ",smsText=" + smsMessage.getBody() + ",messageId=" + responseStatus.get(MESSAGE_ID) + "]");
					}
				} else {
					success = false;
					if (log.isErrorEnabled()) {
						log.error("[SmsBoxSMSBroadcaster] SMS sending failed. [recipient=" + smsMessage.getRecipient()
								+ ",smsText=" + smsMessage.getBody() + ",errorMessage=" + responseStatus.get(MESSAGE) + "]");
					}
					errorMessage = "Error sending SMS ";
					sendErrorAlert(errorMessage, msgConfig, errorMessage + " : " + responseStatus.get(MESSAGE));
				}
			} else {
				success = false;
				if (log.isErrorEnabled()) {
					log.error("[SmsBoxSMSBroadcaster] SMS sending failed. [recipient=" + smsMessage.getRecipient() + ",smsText="
							+ smsMessage.getBody() + ",errorMessage=" + "HTTP  " + statusCode + "]");
				}
				errorMessage = "Error sending SMS ";
				sendErrorAlert(errorMessage, msgConfig, errorMessage + " : HTTP " + statusCode);
			}

		} catch (Exception e) {
			success = false;
			xceptionToAudit = e;
			log.error("[SmsBoxSMSBroadcaster] SMS sending failed. [recipient=" + smsMessage.getRecipient() + ",smsText="
					+ smsMessage.getBody() + "]", e);
			errorMessage = "Exception in Sending SMS";
			sendErrorAlert(errorMessage, msgConfig, msgConfig + " : " + e);
		} finally {
			// Performs auditing
			this.recordReservationAudit(smsMessage.getAuditInfo(), xceptionToAudit, success, smsMessage.getRecipient());
			if (smsMessage.getAuditInfo() != null)
				smsMessage.getAuditInfo().clear();
			this.updateObjectInfo(smsMessage.getObjectInfo(), xceptionToAudit, success);
			method.releaseConnection();
		}
	}

	/**
	 * Sets formatted message specific parameters to be included to query string
	 * 
	 * @param smsMessage
	 */
	private void setMessageParams(SmsMessage smsMessage) {
		String smsText = smsMessage.getBody();
		if (smsText.length() > 160) {
			smsText = smsText.substring(0, 159);
		}
		parameters[4].setValue(smsText);
		parameters[5].setValue(buildMobileNumber(smsMessage.getRecipient()));

	}

	/**
	 * Sets common SMS gateway parameter to be included in query string
	 * 
	 * @param smsConfig
	 */
	private void setCommonParams(SmsConfig smsConfig) {

		parameters[0] = new NameValuePair("username", smsConfig.getSmsUserName());
		parameters[1] = new NameValuePair("password", smsConfig.getSmsUserPassword());
		parameters[2] = new NameValuePair("customerId", Integer.toString(smsConfig.getCustomerID()));
		parameters[3] = new NameValuePair("senderText", smsConfig.getSenderID());
		parameters[4] = new NameValuePair("messageBody", null);
		parameters[5] = new NameValuePair("recipientNumbers", null);
		parameters[6] = new NameValuePair("defdate", null);
		parameters[7] = new NameValuePair("isBlink", smsConfig.getIsBlink());
		parameters[8] = new NameValuePair("isFlash", smsConfig.getIsFlash());

	}

	/**
	 * Formats mobile number
	 * 
	 * @param mobileNo
	 * @return
	 */
	private String buildMobileNumber(String mobileNo) {
		return mobileNo = mobileNo.replace("-", "");
	}

	/**
	 * Parses response.
	 * 
	 * @param responseCode
	 * @return
	 */
	private Map<String, String> parseResponse(String responseCode) {
		Map<String, String> resultMap = new HashMap<String, String>();

		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;
		try {
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(new InputSource(new ByteArrayInputStream(responseCode.getBytes("utf-8"))));
			doc.getDocumentElement().normalize();
			log.info("Root element :" + doc.getDocumentElement().getNodeName());
			Element element = doc.getDocumentElement();
			String result = null;
			String message = null;
			String messageId = null;
			NodeList nList = element.getChildNodes();
			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					if (eElement.getNodeName().equals("Result")) {
						result = eElement.getTextContent();
					} else if (eElement.getNodeName().equals("Message")) {
						message = eElement.getTextContent();
					} else if (eElement.getNodeName().equals("messageId")) {
						messageId = eElement.getTextContent();
					}
				}
			}

			if (result != null && result.equals(SUCCESS_FALSE)) {
				resultMap.put(SUCCESS, SUCCESS_FALSE);
				resultMap.put(MESSAGE, message);
			} else {
				resultMap.put(SUCCESS, SUCCESS_TRUE);
				resultMap.put(MESSAGE, messageId);
			}

		} catch (ParserConfigurationException e) {
			log.error("[SmsBoxSMSBroadcaster] xml response parse failed. " + responseCode);
		} catch (SAXException | IOException e) {
			log.error("[SmsBoxSMSBroadcaster] xml response parse failed because of SAXException or IOException . " + responseCode);
		}

		return resultMap;
	}
}
