package com.isa.thinair.messaging.core.util;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.messaging.api.model.EmailMessage;

public class EmailUtil {

	private final static Log log = LogFactory.getLog(EmailUtil.class);

	private static final String KEY_VALUE_SEPARATOR = "#";

	private static final String KEY_VALUE_PAIR_SEPARATOR = "|";

	private static final String ATTACHMENT_SEPARATOR = ",";

	private static final String KEY_CONTENT_TYPE = "CONTENT_TYPE";

	private static final String KEY_FROM_ADDRESS = "FROM_ADDRESS";

	private static final String KEY_TO_ADDRESS = "TO_ADDRESS";

	private static final String KEY_SUBJECT = "SUBJECT";

	private static final String KEY_MAIL_SERVER_CONFIG_NAME = "MAIL_SERVER_CONFIG_NAME";

	private static final String KEY_ATTACHMENT_NAMES = "ATTACHMENT_NAMES";

	public static void deleteMail(String id) throws ModuleException {
		// TODO Auto-generated method stub

	}

	public static String getMessageParam(EmailMessage message) {

		StringBuffer sb = new StringBuffer();
		String messageParam = null;

		// content type
		sb.append(KEY_CONTENT_TYPE);
		sb.append(KEY_VALUE_SEPARATOR);
		sb.append(message.getContentType());

		sb.append(KEY_VALUE_PAIR_SEPARATOR);

		// from address
		sb.append(KEY_FROM_ADDRESS);
		sb.append(KEY_VALUE_SEPARATOR);
		sb.append(message.getFromAddress());

		sb.append(KEY_VALUE_PAIR_SEPARATOR);

		// to address
		sb.append(KEY_TO_ADDRESS);
		sb.append(KEY_VALUE_SEPARATOR);
		sb.append(message.getToAddress());

		sb.append(KEY_VALUE_PAIR_SEPARATOR);

		// subject
		sb.append(KEY_SUBJECT);
		sb.append(KEY_VALUE_SEPARATOR);
		sb.append(message.getSubject());

		sb.append(KEY_VALUE_PAIR_SEPARATOR);

		// mail server config name
		sb.append(KEY_MAIL_SERVER_CONFIG_NAME);
		sb.append(KEY_VALUE_SEPARATOR);
		sb.append(message.getMailServerConfigurationName());

		sb.append(KEY_VALUE_PAIR_SEPARATOR);

		// attachment file names
		sb.append(KEY_ATTACHMENT_NAMES);
		sb.append(KEY_VALUE_SEPARATOR);
		sb.append(getAttachmentFileNames(message.getAttachmentFileNames()));

		sb.append(KEY_VALUE_PAIR_SEPARATOR);

		// arrByte = sb.toString().getBytes();
		messageParam = sb.toString();
		return messageParam;
	}

	public static String getMessageBody(EmailMessage message) {

		StringBuffer sb = new StringBuffer();
		String messageBody = null;

		// body
		sb.append(message.getBody());

		messageBody = sb.toString();
		return messageBody;
	}

	public static EmailMessage getEmailMessage(int id, String param, String content) {
		EmailMessage emailMessage = new EmailMessage();
		String strMessage = param;

		String[] arrKeyValuePairs = StringUtils.split(strMessage, KEY_VALUE_PAIR_SEPARATOR);
		Map mapData = getDataMap(arrKeyValuePairs);

		String[] arrAttachmentFileNames = null;
		// id
		emailMessage.setId(String.valueOf(id));

		// content type
		emailMessage.setContentType((String) mapData.get(KEY_CONTENT_TYPE));

		// from address
		emailMessage.setFromAddress((String) mapData.get(KEY_FROM_ADDRESS));

		// to address
		emailMessage.setToAddress((String) mapData.get(KEY_TO_ADDRESS));

		// subject
		emailMessage.setSubject((String) mapData.get(KEY_SUBJECT));

		// mail server config name
		emailMessage.setMailServerConfigurationName((String) mapData.get(KEY_MAIL_SERVER_CONFIG_NAME));

		// attachment file names
		arrAttachmentFileNames = StringUtils.split((String) mapData.get(KEY_ATTACHMENT_NAMES), ATTACHMENT_SEPARATOR);
		emailMessage.setAttachmentFileNames(getAttachmentFileNames(arrAttachmentFileNames));

		// set body
		emailMessage.setBody(content);

		return emailMessage;
	}

	private static Collection getAttachmentFileNames(String[] arrAttachmentFileNames) {
		Collection<String> colAttachmentFileNames = new ArrayList<String>();

		if (arrAttachmentFileNames != null) {
			int len = arrAttachmentFileNames.length;

			for (int i = 0; i < len; i++) {
				colAttachmentFileNames.add(arrAttachmentFileNames[i]);
			}
		}

		return colAttachmentFileNames;
	}

	private static Map getDataMap(String[] arrKeyValuePairs) {

		Map<String, String> mapData = new HashMap<String, String>();

		for (int i = 0; i < arrKeyValuePairs.length; i++) {
			String[] arrKeyValue = StringUtils.split(arrKeyValuePairs[i], KEY_VALUE_SEPARATOR);
			String key = arrKeyValue[0];
			String value = "";

			if (arrKeyValue.length != 1 && arrKeyValue[1] != null) {
				value = arrKeyValue[1];
			}
			mapData.put(key, value);
		}

		return mapData;
	}

	private static String getAttachmentFileNames(Collection attachmentFileNames) {
		StringBuffer sb = new StringBuffer();

		if (attachmentFileNames != null) {
			for (Iterator iter = attachmentFileNames.iterator(); iter.hasNext();) {
				String attName = (String) iter.next();

				sb.append(attName);

				if (iter.hasNext()) {
					sb.append(",");
				}
			}
		}
		return sb.toString();
	}

	public static EmailMessage getEmailMessageNew(int id, String param, byte[] content) {
		EmailMessage emailMessage = new EmailMessage();
		String strMessage = param;

		String[] arrKeyValuePairs = StringUtils.split(strMessage, KEY_VALUE_PAIR_SEPARATOR);
		Map mapData = getDataMap(arrKeyValuePairs);

		String[] arrAttachmentFileNames = null;
		// id
		emailMessage.setId(String.valueOf(id));

		// content type
		emailMessage.setContentType((String) mapData.get(KEY_CONTENT_TYPE));

		// from address
		emailMessage.setFromAddress((String) mapData.get(KEY_FROM_ADDRESS));

		// to address
		emailMessage.setToAddress((String) mapData.get(KEY_TO_ADDRESS));

		// subject
		emailMessage.setSubject((String) mapData.get(KEY_SUBJECT));

		// mail server config name
		emailMessage.setMailServerConfigurationName((String) mapData.get(KEY_MAIL_SERVER_CONFIG_NAME));

		// attachment file names
		arrAttachmentFileNames = StringUtils.split((String) mapData.get(KEY_ATTACHMENT_NAMES), ATTACHMENT_SEPARATOR);
		emailMessage.setAttachmentFileNames(getAttachmentFileNames(arrAttachmentFileNames));

		// set body
		try {
			emailMessage.setBody(new String(content, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			log.error("Error converting the message content" + e.getMessage());
			emailMessage = new EmailMessage();
		}

		return emailMessage;
	}

}
