package com.isa.thinair.messaging.core.persistence.util;

import com.isa.thinair.messaging.api.dto.UpdateEmailStatusInfoDTO;

/**
 * Prepares SQL statement which related to updating status of a particular table when an email message is sent.
 * 
 * @author lalanthi
 */
public class MessageStatusUpdateStatementCreater {

	public String getPaxSegmentSSREmailUpdateQuery(UpdateEmailStatusInfoDTO updateInfoDTO) {
		StringBuilder sb = new StringBuilder();
		sb.append("update T_PNR_PAX_SEGMENT_SSR SS set SS.email_status='" + updateInfoDTO.getStatus()
					+ "',SS.NO_OF_ATTEMPTS= decode(SS.NO_OF_ATTEMPTS, null,1,SS.NO_OF_ATTEMPTS+1) where SS.ppss_id in(" 
					+ updateInfoDTO.getPpssId() + ") ");

		return sb.toString();
	}


}
