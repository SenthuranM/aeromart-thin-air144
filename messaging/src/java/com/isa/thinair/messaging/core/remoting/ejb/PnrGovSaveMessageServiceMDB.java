package com.isa.thinair.messaging.core.remoting.ejb;

import java.io.File;
import java.io.IOException;

import javax.annotation.security.RunAs;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.login.util.ForceLoginInvoker;
import com.isa.thinair.messaging.api.model.PNRGOVMessageData;
import com.isa.thinair.messaging.api.service.MessagingModuleUtils;
import com.isa.thinair.messaging.core.config.MessagingModuleConfig;

@MessageDriven(name = "PnrGovSaveMessageServiceMDB", activationConfig = {
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "queue/pnrGovMsgQueue"),
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
		@ActivationConfigProperty(propertyName = "transactionTimeout", propertyValue = "3600") })
@RunAs("user")
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
public class PnrGovSaveMessageServiceMDB implements MessageListener {
	private static final long serialVersionUID = -6583759698965963350L;
	private final Log logger = LogFactory.getLog(getClass());

	@Override
	public void onMessage(Message message) {
		try {
			MessagingModuleConfig messagingModuleConfig = MessagingModuleUtils.getMessagingModuleConfig();
			if (messagingModuleConfig.isInvokeCredentials()) {
				ForceLoginInvoker.login(messagingModuleConfig.getEjbAccessUsername(),
						messagingModuleConfig.getEjbAccessPassword());
			}

			if (logger.isDebugEnabled()) {
				logger.debug("Entered PnrGovSaveMessageServiceMDB.onMessage()");
			}

			ObjectMessage objMessage = (ObjectMessage) message;
			PNRGOVMessageData pnrGovMessageData = (PNRGOVMessageData) objMessage.getObject();
			String countryCode = pnrGovMessageData.getCountryCode();
			if (countryCode == null || countryCode.equals("")) {
				throw new ModuleException("FILE PATH CANNOT EXTRACT WITHOUT COUNTRYCODE.");
			}
			writeTextFile(pnrGovMessageData.getFileName(), pnrGovMessageData.getMessageContent(), countryCode,
					messagingModuleConfig.getPnrGovFileSavePath());

		} catch (JMSException ex) {
			logger.error("Exception In PnrGovSaveMessageServiceMDB.onMessage()" + ex.getMessage());
		} catch (Exception exception) {
			logger.error("Exception in PnrGovSaveMessageServiceMDB.onMessage() " + exception.getMessage(), exception);
		} finally {
			if (MessagingModuleUtils.getMessagingModuleConfig().isInvokeCredentials()) {
				ForceLoginInvoker.close();
			}
		}
	}

	private void writeTextFile(String fileName, String content, String countryCode, String fileSavePath) throws IOException {

		StringBuilder localFilePath = new StringBuilder();
		localFilePath.append(fileSavePath).append(File.separatorChar).append(countryCode);
		File folderCheck = new File(localFilePath.toString());

		if (folderCheck.exists() && folderCheck.canWrite()) {
			
			localFilePath.append(File.separatorChar).append(fileName);

			File file = new File(localFilePath.toString());
			FileUtils.writeStringToFile(file, content);
		} else {
			throw new IOException(fileName + ":PNRGOV file writing failed at " + fileSavePath);
		}
	}

}
