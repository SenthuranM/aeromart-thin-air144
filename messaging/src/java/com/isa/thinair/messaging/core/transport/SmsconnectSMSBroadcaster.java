package com.isa.thinair.messaging.core.transport;

import java.io.StringReader;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HostConfiguration;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.isa.thinair.messaging.api.model.SmsMessage;
import com.isa.thinair.messaging.api.utils.MessagingConstants;
import com.isa.thinair.messaging.core.config.MessagingModuleConfig;
import com.isa.thinair.messaging.core.config.SmsConfig;
import com.isa.thinair.messaging.core.util.SMSAuditor;
import com.isa.thinair.platform.api.LookupServiceFactory;

/**
 * SMSConnect SMS Gateway Integration Implementation
 * 
 * @author Janaka Padukka
 * 
 */
public class SmsconnectSMSBroadcaster extends SMSAuditor implements MessageBroadcaster {

	private final Log log = LogFactory.getLog(getClass());
	private final String SMS_CONFIG_NAME = "smsconnect_sms";
	private NameValuePair[] parameters = new NameValuePair[5];

	// Status codes
	private final int SENT = 0;

	@Override
	public void sendMessages(List messageList) {
		// TODO Auto-generated method stub
		MessagingModuleConfig messModConfig = (MessagingModuleConfig) LookupServiceFactory.getInstance()
				.getModuleConfig(MessagingConstants.MODULE_NAME);
		SmsConfig smsConfig = (SmsConfig) messModConfig.getSmsConfigurationMap().get(SMS_CONFIG_NAME);

		setCommonParams(smsConfig);

		HttpClient client = new HttpClient();

		// If proxy enabled go through proxy
		if (smsConfig.isProxyEnable()) {
			HostConfiguration hcon = new HostConfiguration();
			hcon.setProxy(smsConfig.getProxyServer(), smsConfig.getProxyPort());
			client.setHostConfiguration(hcon);
		}

		if (messageList != null) {
			for (Iterator iterator = messageList.iterator(); iterator.hasNext();) {
				SmsMessage smsMessage = (SmsMessage) iterator.next();
				// Send SMS
				sendSMS(messModConfig, client, smsConfig, smsMessage);
			}
		}

	}

	/**
	 * Individual SMS Sender
	 * 
	 * @param msgConfig
	 * @param client
	 * @param smsConfig
	 * @param smsMessage
	 */
	private void sendSMS(MessagingModuleConfig msgConfig, HttpClient client, SmsConfig smsConfig, SmsMessage smsMessage) {

		String responseString = null;
		int responseCode = -1;
		String errorMessage = "";
		String strError = "";
		boolean success = true;
		Throwable xceptionToAudit = null;

		GetMethod method = new GetMethod(smsConfig.getSmsServer());

		// Setting message specific data
		setMessageParams(smsMessage);

		// Setting query string by NameValuePair for automatic URL encoding
		method.setQueryString(parameters);

		// Provide custom retry handler is necessary
		method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(3, false));

		try {

			log.info("[SmsconnectSMSBroadcaster] Sending SMS ... [recipient=" + smsMessage.getRecipient() + ",smsText="
					+ smsMessage.getBody() + "]");
			// Execute the method.
			int statusCode = client.executeMethod(method);

			if (statusCode != HttpStatus.SC_OK) {
				log.error("[SmsconnectSMSBroadcaster] Method failed: " + method.getStatusLine());
			}

			// Read the response body.
			byte[] responseBody = method.getResponseBody();
			responseString = new String(responseBody);

			responseCode = parseResponse(responseString);

			if (responseCode == SENT) {
				if (log.isInfoEnabled()) {
					log.info("[SmsconnectSMSBroadcaster] SMS successfully sent. [recipient="
							+ smsMessage.getRecipient() + ",smsText=" + smsMessage.getBody() + "]");
				}
			} else {
				// As error scenarios's are not confirmed yet, all status codes
				// other than 0 is considered to be errorneous.
				success = false;
				if (log.isErrorEnabled()) {
					log.error("[SmsconnectSMSBroadcaster] SMS successfully sent. [recipient="
							+ smsMessage.getRecipient() + ",smsText=" + smsMessage.getBody() + "]");
				}
				strError = "SmsconnectSMSBroadcaster common error";
				errorMessage = "Error sending SMS ";
				sendErrorAlert(errorMessage, msgConfig, errorMessage + " : " + strError);
			}

		} catch (Exception e) {
			xceptionToAudit = e;
			log.error("[SmsconnectSMSBroadcaster] SMS sending failed. [recipient=" + smsMessage.getRecipient()
					+ ",smsText=" + smsMessage.getBody() + "]", e);
			errorMessage = "Exception in Sending SMS";
			sendErrorAlert(errorMessage, msgConfig, msgConfig + " : " + e);
		} finally {
			// Performs auditing
			this.recordReservationAudit(smsMessage.getAuditInfo(), xceptionToAudit, success, smsMessage.getRecipient());
			if (smsMessage.getAuditInfo() != null)
				smsMessage.getAuditInfo().clear();
			this.updateObjectInfo(smsMessage.getObjectInfo(), xceptionToAudit, success);
			method.releaseConnection();
		}
	}

	/**
	 * Sets formatted message specific parameters to be included to query string
	 * 
	 * @param smsMessage
	 */
	private void setMessageParams(SmsMessage smsMessage) {
		String smsText = smsMessage.getBody();
		if (smsText.length() > 160) {
			smsText = smsText.substring(0, 159);
		}
		parameters[3].setValue(buildMobileNumber(smsMessage.getRecipient()));
		parameters[4].setValue(smsText);
	}

	/**
	 * Sets common SMS gateway parameter to be included to query string
	 * 
	 * @param smsConfig
	 */
	private void setCommonParams(SmsConfig smsConfig) {

		parameters[0] = new NameValuePair("login", smsConfig.getSmsUserName());
		parameters[1] = new NameValuePair("password", smsConfig.getSmsUserPassword());
		parameters[2] = new NameValuePair("oadc", smsConfig.getSenderID());
		parameters[3] = new NameValuePair("msisdn_to", null);
		parameters[4] = new NameValuePair("body", null);

	}

	/**
	 * Formats mobile number
	 * 
	 * @param mobileNo
	 * @return
	 */
	private String buildMobileNumber(String mobileNo) {
		// Appends + to the start of the mobile number
		mobileNo = mobileNo.replace("-", "");
		return "+".concat(mobileNo);
	}

	/**
	 * Parses output XML string to extract response
	 * 
	 * @param response
	 * @return
	 */
	private int parseResponse(String response) {

		int statusCode = -1;

		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(response));

			Document doc = db.parse(is);
			NodeList nodes = doc.getElementsByTagName("statuscode");
			Element element = (Element) nodes.item(0);

			String statusCodeString = element.getTextContent();

			if (statusCodeString != null) {
				statusCode = Integer.parseInt(statusCodeString.trim());
			}
		} catch (Exception ex) {

			if (log.isErrorEnabled()) {
				log.error("[SmsconnectSMSBroadcaster] Parsing response failed.", ex);
			}
		}

		return statusCode;

	}

}
