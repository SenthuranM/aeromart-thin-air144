package com.isa.thinair.messaging.core.config;

import java.io.Serializable;

/**
 * @author Nasly
 */
public class MailServerConfig implements Serializable {

	private static final long serialVersionUID = 21111111L;

	private String hostAddress;

	private String userName;

	private String password;

	private String smtpPartialSending;

	private String protocol;

	private String manualPnlSend;

	private String connectiontimeout;

	private String iotimeout;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getHostAddress() {
		return hostAddress;
	}

	public void setHostAddress(String hostAddress) {
		this.hostAddress = hostAddress;
	}

	public String getSmtpPartialSending() {
		return smtpPartialSending;
	}

	public void setSmtpPartialSending(String smtpPartialSending) {
		this.smtpPartialSending = smtpPartialSending;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getManualPnlSend() {
		return manualPnlSend;
	}

	public void setManualPnlSend(String manualPnlSend) {
		this.manualPnlSend = manualPnlSend;
	}

	public String getConnectiontimeout() {
		return connectiontimeout;
	}

	public void setConnectiontimeout(String connectiontimeout) {
		this.connectiontimeout = connectiontimeout;
	}

	public String getIotimeout() {
		return iotimeout;
	}

	public void setIotimeout(String iotimeout) {
		this.iotimeout = iotimeout;
	}

}
