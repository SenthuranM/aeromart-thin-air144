/*
 * Created on 05-Jun-2005
 *
 */
package com.isa.thinair.messaging.core.config;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import com.isa.thinair.platform.api.DefaultModuleConfig;

/**
 * @isa.module.config-bean
 * @author Nasly
 */
public class MessagingModuleConfig extends DefaultModuleConfig {

	private Map topicConfigurationMap;

	private Map mailServersConfigurationMap;
	
	private Map externalSitaLocationConfigurationMap;

	private Map pNRGOVConfigMap;

	private String maxAttempts;

	private String smsGateway;

	public String getSmsGateway() {
		return smsGateway;
	}

	public void setSmsGateway(String smsGateway) {
		this.smsGateway = smsGateway;
	}

	private boolean sendBCC;

	private Collection bccAddresses;

	private Properties smsFailList;

	private Map smsConfigurationMap;

	private String ejbAccessUsername;
	private String ejbAccessPassword;
	private boolean invokeCredentials;
	private String pnrGovFileSavePath;

	public Map getSmsConfigurationMap() {
		return smsConfigurationMap;
	}

	public void setSmsConfigurationMap(Map smsConfigurationMap) {
		this.smsConfigurationMap = smsConfigurationMap;
	}

	public Map getTopicConfigurationMap() {
		return topicConfigurationMap;
	}

	public void setTopicConfigurationMap(Map topicConfigurationMap) {
		this.topicConfigurationMap = topicConfigurationMap;
	}

	public Map getMailServersConfigurationMap() {
		return mailServersConfigurationMap;
	}

	public void setMailServersConfigurationMap(Map mailServersConfigurationMap) {
		this.mailServersConfigurationMap = mailServersConfigurationMap;
	}

	public Collection getMailServerConfigs(String configurationCode) {
		Collection mailServerCofigs = new ArrayList();
		Iterator mailserverConfigsIt = getMailServersConfigurationMap().keySet().iterator();
		while (mailserverConfigsIt.hasNext()) {
			String key = (String) mailserverConfigsIt.next();
			if (key.startsWith(configurationCode)) {
				mailServerCofigs.add(getMailServersConfigurationMap().get(key));
			}
		}
		return mailServerCofigs;
	}

	public boolean isSendBCC() {
		return sendBCC;
	}

	public void setSendBCC(boolean sendBCC) {
		this.sendBCC = sendBCC;
	}

	public Collection getBccAddresses() {
		return bccAddresses;
	}

	public void setBccAddresses(Collection bccAddresses) {
		this.bccAddresses = bccAddresses;
	}

	public String getMaxAttempts() {
		return maxAttempts;
	}

	public void setMaxAttempts(String maxAttempts) {
		this.maxAttempts = maxAttempts;
	}

	/**
	 * @return Returns the smsFailList.
	 */
	public Properties getSmsFailList() {
		return smsFailList;
	}

	/**
	 * @param smsFailList
	 *            The smsFailList to set.
	 */
	public void setSmsFailList(Properties smsFailList) {
		this.smsFailList = smsFailList;
	}

	/**
	 * @return the ejbAccessUsername
	 */
	public String getEjbAccessUsername() {
		return ejbAccessUsername;
	}

	/**
	 * @param ejbAccessUsername
	 *            the ejbAccessUsername to set
	 */
	public void setEjbAccessUsername(String ejbAccessUsername) {
		this.ejbAccessUsername = ejbAccessUsername;
	}

	/**
	 * @return the ejbAccessPassword
	 */
	public String getEjbAccessPassword() {
		return ejbAccessPassword;
	}

	/**
	 * @param ejbAccessPassword
	 *            the ejbAccessPassword to set
	 */
	public void setEjbAccessPassword(String ejbAccessPassword) {
		this.ejbAccessPassword = ejbAccessPassword;
	}

	/**
	 * @return the invokeCredentials
	 */
	public boolean isInvokeCredentials() {
		return invokeCredentials;
	}

	/**
	 * @param invokeCredentials
	 *            the invokeCredentials to set
	 */
	public void setInvokeCredentials(boolean invokeCredentials) {
		this.invokeCredentials = invokeCredentials;
	}

	public String getPnrGovFileSavePath() {
		return pnrGovFileSavePath;
	}

	public void setPnrGovFileSavePath(String pnrGovFileSavePath) {
		this.pnrGovFileSavePath = pnrGovFileSavePath;
	}

	public Map getExternalSitaLocationConfigurationMap() {
		return externalSitaLocationConfigurationMap;
	}

	public void setExternalSitaLocationConfigurationMap(Map externalSitaLocationConfigurationMap) {
		this.externalSitaLocationConfigurationMap = externalSitaLocationConfigurationMap;
	}

	public Map getpNRGOVConfigMap() {
		return pNRGOVConfigMap;
	}

	public void setpNRGOVConfigMap(Map pNRGOVConfigMap) {
		this.pNRGOVConfigMap = pNRGOVConfigMap;
	}
}
