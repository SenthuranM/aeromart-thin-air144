/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Juil 4, 2005
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.messaging.core.remoting.ejb;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.interceptor.Interceptors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.annotation.ejb.Clustered;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.security.SecurityDomain;

import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.framework.PlatformBaseSessionBean;
import com.isa.thinair.commons.core.interceptor.Perf4JEJB3Interceptor;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.messaging.api.dto.LogDetailDTO;
import com.isa.thinair.messaging.api.dto.SimpleEmailDTO;
import com.isa.thinair.messaging.api.model.EmailMessage;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.PNRGOVMessageData;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.messaging.api.service.MessagingModuleUtils;
import com.isa.thinair.messaging.api.util.MessagingCustomConstants;
import com.isa.thinair.messaging.core.config.MailServerConfig;
import com.isa.thinair.messaging.core.config.MessagingModuleConfig;
import com.isa.thinair.messaging.core.persistence.dao.MessagingJdbcDAO;
import com.isa.thinair.messaging.core.processor.MessageManager;
import com.isa.thinair.messaging.core.service.bd.MessagingServiceRemote;
import com.isa.thinair.messaging.core.service.bd.MessagingServiceLocal;
import com.isa.thinair.messaging.core.service.bd.SavePnrGovMessageViaMDB;
import com.isa.thinair.messaging.core.service.bd.SendMessageViaMDB;
import com.isa.thinair.messaging.core.transport.EmailBroadcaster;
import com.isa.thinair.messaging.core.transport.MQMessageHandler;
import com.isa.thinair.messaging.core.transport.MessageBroadcasterFactory;
import com.isa.thinair.messaging.core.util.MessagingInternalConstants;

/**
 * @author sumudu
 */
@Stateless
@RemoteBinding(jndiBinding = "MessageServices.remote")
@LocalBinding(jndiBinding = "MessageServices.local")
@RolesAllowed("user")
@TransactionAttribute(TransactionAttributeType.REQUIRED)
@SecurityDomain(value = "ISALogin", unauthenticatedPrincipal = "nobody")
@Interceptors(Perf4JEJB3Interceptor.class)
@Clustered
public class MessageServicesBean extends PlatformBaseSessionBean implements MessagingServiceRemote,
		MessagingServiceLocal {

	private static final long serialVersionUID = 4043042233531319460L;

	private final Log log = LogFactory.getLog(getClass());

	public void recordMessage(LogDetailDTO logDetailDTO) throws ModuleException {
		getMessageManager().recordMessage(logDetailDTO);
	}

	public void saveEmail(EmailMessage message) throws ModuleException {
		try {
			getMessagingJdbcDAO().saveEmail(message);
		} catch (CommonsDataAccessException e) {
			log.error("saveEmail failed.", e);
			throw new ModuleException(e.getExceptionCode(), "messaging.desc");
		}
	}

	/**
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public List loadEmails() throws ModuleException {
		try {
			return getMessagingJdbcDAO().loadEmails();
		} catch (CommonsDataAccessException e) {
			log.error("loadEmails failed.", e);
			throw new ModuleException(e.getExceptionCode(), "messaging.desc");
		}
	}

	/**
	 * 
	 * @param configurationCode
	 * @return
	 */
	public Collection getMailServerConfigs(String configurationCode) {
		return ((MessagingModuleConfig) MessagingModuleUtils.getInstance().getModuleConfig())
				.getMailServerConfigs(configurationCode);
	}

	/**
	 * Returns the Default Mail Server
	 * 
	 * @return
	 */
	public MailServerConfig getDefaultMailServer() {
		Collection colMailServerConfig = ((MessagingModuleConfig) MessagingModuleUtils.getInstance().getModuleConfig())
				.getMailServerConfigs(MessagingCustomConstants.DEFAULT_MAIL_SERVER_KEY);

		Iterator itColMailServerConfig = colMailServerConfig.iterator();
		MailServerConfig mailServerConfig = null;

		if (itColMailServerConfig.hasNext()) {
			mailServerConfig = (MailServerConfig) itColMailServerConfig.next();
		}

		return mailServerConfig;
	}

	private MessageManager getMessageManager() throws EJBException {
		MessageManager manager = new MessageManager();
		return manager;
	}

	private MessagingJdbcDAO getMessagingJdbcDAO() {
		return (MessagingJdbcDAO) MessagingModuleUtils.getInstance().getLocalBean(
				MessagingInternalConstants.DAOProxyNames.MESSAGING_JDBC_DAO);
	}

	public void sendMessage(MessageProfile impl) {
		new SendMessageViaMDB().sendMessageViaMDB(impl);
	}

	public void sendMessages(List profileList) {
		new SendMessageViaMDB().sendMessagesViaMDB(profileList);
	}

	@Override
	public void sendSimpleEmail(SimpleEmailDTO simpleEmailDTO) throws ModuleException {
		if (log.isDebugEnabled())
			log.debug("Messaging Service : sendSimpleEmail()");

		String strCarrier = CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.CARRIER_NAME);
		UserMessaging userMessaging = new UserMessaging();
		userMessaging.setToAddres(simpleEmailDTO.getToEmailAddress());
		userMessaging.setFirstName(simpleEmailDTO.getFromAddress());

		List<UserMessaging> messages = new ArrayList<UserMessaging>();
		messages.add(userMessaging);

		// parameter list
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("subject", simpleEmailDTO.getSubject());
		map.put("content", simpleEmailDTO.getContent());
		map.put("carrier", strCarrier);

		// Topic
		Topic topic = new Topic();
		topic.setTopicParams(map);
		topic.setTopicName("simple_email_sender");
		// topic.setAttachMessageBody(false);

		// User Profile
		MessageProfile messageProfile = new MessageProfile();
		messageProfile.setUserMessagings(messages);
		messageProfile.setTopic(topic);
		sendMessage(messageProfile);
	}

	public void sendSimpleTextEmail(SimpleEmailDTO simpleEmailDTO) throws ModuleException {
		((EmailBroadcaster) MessageBroadcasterFactory.getBroadcaster("EMAIL")).sendSimpleTextEmail(simpleEmailDTO);
	}

	@Override
	public void saveAndPushPNRGOVMessage(String message, String fileName, String countryCode) throws ModuleException {
		PNRGOVMessageData messageData = new PNRGOVMessageData();
		messageData.setFileName(fileName);
		messageData.setMessageContent(message);
		messageData.setCountryCode(countryCode);
		
		new SavePnrGovMessageViaMDB().saveMessageViaMDB(messageData);
		
		MQMessageHandler handler = new MQMessageHandler(countryCode);
		int numberOfPushTries = 0;
		
		
		try {
			while (numberOfPushTries < 5) {
				numberOfPushTries++;
				try {
					handler.pushMessage(message, countryCode);
					break;
				} catch (ModuleException ex) {
					if (numberOfPushTries < 5) {
						continue;
					} else {
						log.info(ex);
						throw new ModuleException("PNRGOV MESSAGE PUSH FAILED.");
					}
				}
			}
		} finally {
			handler.closeConnections();
		}
		
	}
}
