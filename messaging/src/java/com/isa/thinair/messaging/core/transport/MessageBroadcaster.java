/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jun 30, 2005
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.messaging.core.transport;

import java.util.List;

/**
 * @author sumudu
 * 
 *         MessageBroadcaster is the tmplaplate for creating broadcasters
 */
public interface MessageBroadcaster {

	public void sendMessages(List messageList);

}
