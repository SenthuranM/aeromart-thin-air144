/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005-08 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 06, 2005 15:28:39
 * 
 * ===============================================================================
 */
package com.isa.thinair.messaging.core.transport;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.model.DashboardMessage;
import com.isa.thinair.airmaster.api.model.SSRCategory;
import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.airmaster.api.to.DashboardMsgTO;
import com.isa.thinair.airreservation.api.dto.ancilaryreminder.FlightSegReminderNotificationDTO;
import com.isa.thinair.airreservation.api.model.ReservationSegmentNotificationEvent;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airschedules.api.dto.FlightMealNotifyStatusDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.alerting.api.model.FlightPnrNotification;
import com.isa.thinair.auditor.api.dto.AncillaryReminderAuditDTO;
import com.isa.thinair.auditor.api.dto.OnlineCheckInReminderAuditDTO;
import com.isa.thinair.auditor.api.model.AgentAudit;
import com.isa.thinair.auditor.api.model.ReservationAudit;
import com.isa.thinair.auditor.api.service.AuditorBD;
import com.isa.thinair.auditor.api.util.AuditTemplateEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.FTPServerProperty;
import com.isa.thinair.commons.core.util.FTPUtil;
import com.isa.thinair.messaging.api.dto.SimpleEmailDTO;
import com.isa.thinair.messaging.api.dto.UpdateEmailStatusInfoDTO;
import com.isa.thinair.messaging.api.model.EmailMessage;
import com.isa.thinair.messaging.api.model.Message;
import com.isa.thinair.messaging.api.service.MessagingModuleUtils;
import com.isa.thinair.messaging.api.util.MessagingCustomConstants;
import com.isa.thinair.messaging.api.utils.MessagingConstants;
import com.isa.thinair.messaging.core.config.MailServerConfig;
import com.isa.thinair.messaging.core.config.MessagingModuleConfig;
import com.isa.thinair.messaging.core.persistence.dao.MessagingJdbcDAO;
import com.isa.thinair.messaging.core.util.MessagingInternalConstants;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformConstants;
import com.isa.thinair.scheduler.api.service.SchedulerBD;

/**
 * @author Nasly
 */
public class EmailBroadcaster implements MessageBroadcaster {

	private final Log log = LogFactory.getLog(getClass());

	private static String SMTP_PROTOCOL = "mail.transport.protocol";

	private static String SMTP_HOST = "mail.smtp.host";

	private static String SMTP_PARTIAL_SENDING = "mail.smtp.sendpartial";

	private static String SMTP_AUTHENITCATE = "mail.smtp.auth";

	// Refer - http://java.sun.com/products/javamail/javadocs/com/sun/mail/smtp/package-summary.html
	private static String SMTP_CONNECTION_TIMEOUT = "mail.smtp.connectiontimeout";

	private static String SMTP_IO_TIMEOUT = "mail.smtp.timeout";

	private final MessagingJdbcDAO messgingJdbcDAO;

	private FTPUtil ftpUtil = null;

	private FTPServerProperty ftpServerProperty = null;

	public EmailBroadcaster() {
		this.messgingJdbcDAO = (MessagingJdbcDAO) MessagingModuleUtils.getInstance().getLocalBean(
				MessagingInternalConstants.DAOProxyNames.MESSAGING_JDBC_DAO);
		if (this.ftpUtil == null) {
			this.ftpUtil = new FTPUtil();
		}

		this.ftpUtil.setPassiveMode(CommonsServices.getGlobalConfig().isFtpEnablePassiveMode());

		if (this.ftpServerProperty == null) {
			this.ftpServerProperty = new FTPServerProperty();
		}

		// Code to FTP
		this.ftpServerProperty.setServerName(CommonsServices.getGlobalConfig().getFtpServerName());
		if (CommonsServices.getGlobalConfig().getFtpServerUserName() == null) {
			this.ftpServerProperty.setUserName("");
		} else {
			this.ftpServerProperty.setUserName(CommonsServices.getGlobalConfig().getFtpServerUserName());
		}
		this.ftpServerProperty.setUserName(CommonsServices.getGlobalConfig().getFtpServerUserName());

		if (CommonsServices.getGlobalConfig().getFtpServerPassword() == null) {
			this.ftpServerProperty.setPassword("");
		} else {
			this.ftpServerProperty.setPassword(CommonsServices.getGlobalConfig().getFtpServerPassword());
		}
	}

	@Override
	public void sendMessages(List messagesList) {

		EmailMessage message = null;
		Throwable exceptionToAudit = null;
		Throwable lastExceptionToAudit = null;
		boolean success = false;
		Properties props = null;

		Session session = null;
		MimeMessage emailMessage = null;
		MimeBodyPart messageBodyPart = null;

		Long beforeCallingSend = null;

		Multipart multipart = null;
		MimeBodyPart messageBodyPartAttach = null;
		DataSource source = null;

		Iterator iterator = messagesList.iterator();
		String attachmentFileName = null;
		String filePath = null;
		boolean downloaded = false;

		int sentOrInvalidCount = 0;
		int messagesListCount = messagesList.size();

		while (iterator.hasNext()) {
			success = false;
			exceptionToAudit = null;
			try {
				message = (EmailMessage) iterator.next();
				InternetAddress[] addresses = InternetAddress.parse(message.getToAddress());

				// Default Properties for the Email Message
				MessagingModuleConfig moduleConfig = (MessagingModuleConfig) LookupServiceFactory.getInstance().getModuleConfig(
						MessagingConstants.MODULE_NAME);

				props = new Properties();

				MailServerConfig mailServerConfig = (MailServerConfig) moduleConfig.getMailServersConfigurationMap().get(
						message.getMailServerConfigurationName());

				props.put(SMTP_PROTOCOL, mailServerConfig.getProtocol());
				props.put(SMTP_HOST, mailServerConfig.getHostAddress());
				props.put(SMTP_CONNECTION_TIMEOUT, mailServerConfig.getConnectiontimeout());
				props.put(SMTP_IO_TIMEOUT, mailServerConfig.getIotimeout());
				props.put(SMTP_PARTIAL_SENDING, mailServerConfig.getSmtpPartialSending());
				
				//log for trace the issue. Can remove after that
				log.error("At EmailBroadCaster: MailServerConfigurationName =" + message.getMailServerConfigurationName()
						+ ", SMTP_HOST=" + mailServerConfig.getHostAddress());

				Authenticator authenticator = null;
				if (!("").equals(mailServerConfig.getUserName()) && !("").equals(mailServerConfig.getPassword())) {
					props.put(SMTP_AUTHENITCATE, "true");
					authenticator = new MessagingAuthenticator(mailServerConfig.getUserName(), mailServerConfig.getPassword());
					log.error("At EmailBroadCaster: set authenticator: UserName =" + mailServerConfig.getUserName()
							+ ", Password=" + mailServerConfig.getPassword());
				}

				try {
					session = Session.getDefaultInstance(props, authenticator);
					log.error("At EmailBroadCaster: get default session successed");
				} catch (SecurityException e) {
					session = Session.getInstance(props, authenticator);
					log.error("At EmailBroadCaster: get default session failed. Get new session");
				}

				// javax.mail.Message emailMessage = new MimeMessage(session);
				emailMessage = new MimeMessage(session);
				emailMessage.setFrom(new InternetAddress(message.getFromAddress()));
				InternetAddress[] replyAddress = InternetAddress.parse(message.getFromAddress());
				emailMessage.setRecipients(javax.mail.Message.RecipientType.TO, addresses);

				// Sending a BCC if configured
				if (moduleConfig.isSendBCC()) {
					if (moduleConfig.getBccAddresses() != null && moduleConfig.getBccAddresses().size() > 0) {
						String bccAddresses = "";
						for (Iterator it = moduleConfig.getBccAddresses().iterator(); it.hasNext();) {
							if ("".equals(bccAddresses)) {
								bccAddresses = (String) it.next();
							} else {
								bccAddresses += ";" + (String) it.next();
							}
						}
						emailMessage.setRecipients(javax.mail.Message.RecipientType.BCC, InternetAddress.parse(bccAddresses));
					}
				}

				emailMessage.setReplyTo(replyAddress);
				emailMessage.setSubject(message.getSubject());

				// create the message part
				messageBodyPart = new MimeBodyPart();
				messageBodyPart.setContent(message.getBody(), Message.CONTENT_TYPE_HTML);

				multipart = new MimeMultipart();
				multipart.addBodyPart(messageBodyPart);

				// Attach caller prepared files loading them via FTP
				if (message.getAttachmentFileNames() != null && message.getAttachmentFileNames().size() > 0) {

					Iterator iterMail = message.getAttachmentFileNames().iterator();

					while (iterMail.hasNext()) {

						attachmentFileName = (String) iterMail.next();
						filePath = PlatformConstants.getAbsAttachmentsPath() + "/" + attachmentFileName;
						
						boolean isFileExist = new File(filePath).exists();
						
						// If file exists in local location, we do not downlaod it from FTP
	                    if(!isFileExist){	        
	                    	
						downloaded = ftpUtil.downloadAttachment(attachmentFileName, ftpServerProperty);
						if ((message.getId() != null) && (!downloaded)) {
							throw new Exception("Cannot Download from FTP Server");
						}
						if ((message.getId() == null) && (!downloaded)) {
							boolean uploaded = ftpUtil.uploadAttachment(attachmentFileName, message.getBody().getBytes("UTF-8"),
									ftpServerProperty);
							if (!uploaded) {
								throw new Exception("Cannot Upload to FTP Server");
							}
						}
						// End Code to FTP						
	                    }

						messageBodyPartAttach = new MimeBodyPart();
						source = new FileDataSource(new File(filePath));

						messageBodyPartAttach.setDataHandler(new DataHandler(source));
						messageBodyPartAttach.setFileName(attachmentFileName);

						multipart.addBodyPart(messageBodyPartAttach);
					}
				}

				// Message body as an attachment
				if (message.isAttachBody()) {
					attachmentFileName = BeanUtils.nullHandler(message.getBodyAttachmentName());

					if (message.getAttachmentType() != null
							&& Message.ATTACHMENT_TYPE_PDF.compareTo(message.getAttachmentType()) == 0) {
						if (message.getMsgBodyByteArray() != null) {
							source = new ByteArrayDataSource(message.getMsgBodyByteArray(), "application/pdf");
							messageBodyPartAttach = new MimeBodyPart();
							messageBodyPartAttach.setDataHandler(new DataHandler(source));
							messageBodyPartAttach.setFileName(attachmentFileName);
							multipart.addBodyPart(messageBodyPartAttach);
						}
					} else {
						source = new ByteArrayDataSource(message.getMsgBodyByteArray(), "text/html");
						messageBodyPartAttach = new MimeBodyPart();
						messageBodyPartAttach.setDataHandler(new DataHandler(source));
						messageBodyPartAttach.setFileName(attachmentFileName);
						multipart.addBodyPart(messageBodyPartAttach);
					}
				}

				emailMessage.setContent(multipart);
				emailMessage.saveChanges();
				if (log.isDebugEnabled()) {
					beforeCallingSend = new Date().getTime();
					log.debug("Before calling email send");
				}
				Transport.send(emailMessage);
				if (log.isDebugEnabled()) {
					log.debug("After calling email send. [TimeSpentInMillis=" + (new Date().getTime() - beforeCallingSend) + "]");
				}
				success = true;
				sentOrInvalidCount++;

				if (log.isDebugEnabled()) {
					log.debug("Sent email to the server." + "\ntoAddress(es)=" + message.getToAddress() + "\nSubject="
							+ message.getSubject());
				}
				if (log.isDebugEnabled()) {
					log.debug("Emailed mesage [content = " + message.getBody() + "]");
				}

			} catch (AddressException e) {
				sentOrInvalidCount++; // Discard invalid email addresses
				exceptionToAudit = e;
				log.error("Sending email failed due AddressException." + "\ntoAddress(es)=" + message.getToAddress()
						+ "\nSubject=" + message.getSubject() + "\nContent=" + message.getBody(), e);
			} catch (MessagingException e) {
				exceptionToAudit = e;
				lastExceptionToAudit = e;
				log.error("Sending email failed due MessagingException." + "\ntoAddress(es)=" + message.getToAddress()
						+ "\nSubject=" + message.getSubject() + "\nContent=" + message.getBody(), e);
			} catch (Exception e) {
				exceptionToAudit = e;
				lastExceptionToAudit = e;
				log.error(
						"Sending email failed due MessagingException." + "\ntoAddress(es)=" + message != null
								&& message.getToAddress() != null ? message.getToAddress() : "" + "\nSubject=" + message != null
								&& message.getSubject() != null ? message.getSubject() : "" + "\nContent=" + message != null
								&& message.getBody() != null ? message.getBody() : "", e);
			} finally {
				if (log.isDebugEnabled()) {
					log.debug("From finally block :: timestamp " + new Date());
				}
				if (success) {
					if (message.getId() != null) {
						Integer id = new Integer(message.getId());
						log.error("Check message existance to delete in T_MESSAGES_AUDIT for message ID: " + id);
						if (messgingJdbcDAO.isMessageExists(id)) {
							log.error("Delete T_MESSAGES_AUDIT message ID: " + id);
							messgingJdbcDAO.deleteEmail(id);
						}
						log.error("Delete message in T_MESSAGES_AUDIT completed for message ID: " + id);
					}
				} else {
					if (log.isDebugEnabled()) {
						log.debug("Save T_MESSAGES_AUDIT message ID: " + "\ntoAddress(es)=" + message.getToAddress()
								+ "\nSubject=" + message.getSubject() + "\nContent=" + message.getBody());
					}
					messgingJdbcDAO.saveEmail(message);
				}

				// Performs auditing
				this.recordReservationAudit(message.getAuditInfo(), exceptionToAudit, message.getToAddress());

				// Perform Auditing for agent emails
				this.recordAgentAudit(message.getAuditInfo(), exceptionToAudit, message.getToAddress(), message.getBody());

				this.updateObjectInfo(message.getObjectInfo(), exceptionToAudit);

			}
		}
		if (message.getAuditInfo() != null && message.getAuditInfo().iterator().hasNext()) {
			Object obj = message.getAuditInfo().iterator().next();
			if (obj != null && obj instanceof AncillaryReminderAuditDTO) {
				this.recordFlightSegmentStatus(message.getAuditInfo(), lastExceptionToAudit, sentOrInvalidCount,
						messagesListCount);
			} else if (obj != null && obj instanceof OnlineCheckInReminderAuditDTO) {
				this.recordFlightSegmentStatus(message.getAuditInfo(), lastExceptionToAudit, sentOrInvalidCount,
						messagesListCount);
			}
		}
	}

	/**
	 * (1) Will Check Type of ReservationAudit (2) If exception is null, status is success else false and will get root
	 * cause!
	 * 
	 * @param auditInfo
	 * @param e
	 */
	private void recordReservationAudit(Collection auditInfo, Throwable e, String toAddress) {
		log.debug("|||Going to Record ReservationAudit");

		try {
			if (auditInfo != null && auditInfo.size() > 0) {
				AuditorBD auditorBD = MessagingModuleUtils.getAuditorBD();
				ReservationBD reservationBD = MessagingModuleUtils.getReservationBD();
				Collection colReservationAudit = new ArrayList();
				ReservationAudit reservationAudit;
				AncillaryReminderAuditDTO ancillaryReminderAuditDTO;
				OnlineCheckInReminderAuditDTO onlineCheckInReminderAuditDTO;
				Collection reservationSegmentNotificationEventCol;
				Object element;

				for (Iterator itObject = auditInfo.iterator(); itObject.hasNext();) {
					element = itObject.next();

					// If auditInfo is type of ReservationAudit Only proceed..!
					if (element != null && element instanceof ReservationAudit) {
						reservationAudit = (ReservationAudit) element;

						// For the Itinerary
						if (AuditTemplateEnum.EMAIL_ITINERARY.getCode().equals(reservationAudit.getModificationType())) {
							// Which means email successfully sent
							if (e == null) {
								reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.EmailItinerary.SENT_STATUS,
										"<b>Itinerary sent successfully</b>");
								reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.EmailItinerary.STATUS_REASONS,
										"Address: " + toAddress);
							}
							// Which means email failure
							else {
								reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.EmailItinerary.SENT_STATUS,
										"<b>Itinerary sending Failed</b>");
								reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.EmailItinerary.STATUS_REASONS,
										"Address: " + toAddress + " " + getRootCauseDetails(e));

								log.error(reservationAudit.getPnr(), e);
							}
							// For the Flight Alteration Notifications
						} else if (AuditTemplateEnum.EMAIL_FLIGHT_ALTERATION.getCode().equals(
								reservationAudit.getModificationType())) {
							// Which means email successfully sent
							if (e == null) {
								reservationAudit.addContentMap(
										AuditTemplateEnum.TemplateParams.EmailFlightAlterations.SENT_STATUS,
										"<b>Notification sent successfully</b>");
								reservationAudit.addContentMap(
										AuditTemplateEnum.TemplateParams.EmailFlightAlterations.STATUS_REASONS, "Address: "
												+ toAddress);
							}
							// Which means email failure
							else {
								reservationAudit.addContentMap(
										AuditTemplateEnum.TemplateParams.EmailFlightAlterations.SENT_STATUS,
										"<b>Notification sending Failed</b>");
								reservationAudit.addContentMap(
										AuditTemplateEnum.TemplateParams.EmailFlightAlterations.STATUS_REASONS, "Address: "
												+ toAddress + " " + getRootCauseDetails(e));

								log.error(reservationAudit.getPnr(), e);
							}
						} else if (AuditTemplateEnum.NOTIFY_PNR.getCode().equals(reservationAudit.getModificationType())) {
							// Which means email successfully sent
							if (e == null) {
								reservationAudit.addContentMap(
										AuditTemplateEnum.TemplateParams.EmailFlightAlterations.SENT_STATUS,
										"<b>Notification sent successfully</b>");
								reservationAudit.addContentMap(
										AuditTemplateEnum.TemplateParams.EmailFlightAlterations.STATUS_REASONS, "Address: "
												+ toAddress);
							}
							// Which means email failure
							else {
								reservationAudit.addContentMap(
										AuditTemplateEnum.TemplateParams.EmailFlightAlterations.SENT_STATUS,
										"<b>Notification sending Failed</b>");
								reservationAudit.addContentMap(
										AuditTemplateEnum.TemplateParams.EmailFlightAlterations.STATUS_REASONS, "Address: "
												+ toAddress + " " + getRootCauseDetails(e));

								log.error(reservationAudit.getPnr(), e);
							}
						} else if (AuditTemplateEnum.ONHOLD_EMAIL.getCode().equals(reservationAudit.getModificationType())
								|| AuditTemplateEnum.OHD_PAY_REMINDER.getCode().equals(reservationAudit.getModificationType())) {
							reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.OnholdAlert.EMAIL_ADDRESS, toAddress);
							if (e == null) {
								reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.OnholdAlert.FAILURE_REASONS,
										"NONE");
								reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.OnholdAlert.SENT_STATUS, "SENT");
								reservationBD.updateOnHoldAlert(reservationAudit.getPnr(),
										ReservationInternalConstants.OnholdAlertStatus.SENT);

							} else {
								// Which means email failure
								reservationAudit
										.addContentMap(AuditTemplateEnum.TemplateParams.OnholdAlert.SENT_STATUS, "FAILED");
								reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.OnholdAlert.FAILURE_REASONS,
										getRootCauseDetails(e));

								log.error(reservationAudit.getPnr(), e);
								reservationBD.updateOnHoldAlert(reservationAudit.getPnr(),
										ReservationInternalConstants.OnholdAlertStatus.FALIED);

							}

						} else if (AuditTemplateEnum.RESERVATION_MODIFIED_EMAIL_AGENT.getCode().equals(
								reservationAudit.getModificationType())) {
							reservationAudit.addContentMap(
									AuditTemplateEnum.TemplateParams.ReservationModifyEmailAgent.EMAIL_ADDRESS, toAddress);
							if (e == null) {
								reservationAudit.addContentMap(
										AuditTemplateEnum.TemplateParams.ReservationModifyEmailAgent.FAILURE_REASONS, "NONE");
								reservationAudit.addContentMap(
										AuditTemplateEnum.TemplateParams.ReservationModifyEmailAgent.SENT_STATUS, "SENT");

							} else {
								reservationAudit.addContentMap(
										AuditTemplateEnum.TemplateParams.ReservationModifyEmailAgent.SENT_STATUS, "FAILED");
								reservationAudit.addContentMap(
										AuditTemplateEnum.TemplateParams.ReservationModifyEmailAgent.FAILURE_REASONS,
										getRootCauseDetails(e));

								log.error(reservationAudit.getPnr(), e);

							}

						}

						colReservationAudit.add(element);
					} else if (element != null && element instanceof AncillaryReminderAuditDTO) { // //CHECK FOR
																									// ANCILLARY
																									// REMINDER
						ancillaryReminderAuditDTO = (AncillaryReminderAuditDTO) element;
						reservationSegmentNotificationEventCol = new ArrayList();
						reservationAudit = new ReservationAudit();
						reservationAudit.setUserId(ancillaryReminderAuditDTO.getUserId());
						reservationAudit.setDisplayName(MessagingModuleUtils.getSecurityBD()
								.getUserBasicDetails(reservationAudit.getUserId()).getDisplayName());

						for (FlightSegReminderNotificationDTO segReminderNotifyDTO : ancillaryReminderAuditDTO
								.getFlightSegNotifyDTOList()) {

							ReservationSegmentNotificationEvent rsne = new ReservationSegmentNotificationEvent();
							reservationSegmentNotificationEventCol.add(rsne);

							rsne.setPnrSegmentId(segReminderNotifyDTO.getSegmentId());
							rsne.setPnrSegmentMsgEventId(segReminderNotifyDTO.getPnrSegNotifyId());
							rsne.setNotificationType(ReservationInternalConstants.NotificationType.ANCI_REMINDER);
							rsne.setTimeStamp(CalendarUtil.getCurrentSystemTimeInZulu());

							reservationAudit.setPnr(ancillaryReminderAuditDTO.getPnr());
							reservationAudit.setZuluModificationDate(CalendarUtil.getCurrentSystemTimeInZulu());
							reservationAudit.setModificationType("EMLREM");
							reservationAudit.setDescription("Ancillary Reminder Audit Data");

							reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.EmailReminder.EMAIL_ADDRESS,
									toAddress);

							// Which means email successfully sent
							if (e == null) {
								rsne.setNotifyStatus(ReservationInternalConstants.PnrSegNotifyEvent.SENT);
								reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.EmailReminder.SENT_STATUS, "<b>"
										+ ReservationInternalConstants.PnrSegNotifyEvent.SENT + "</b>");
								reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.EmailReminder.FAILURE_REASONS,
										"NONE");
							} else { // Which means email failure
								rsne.setFailureReason(getRootCauseDetails(e));
								if (e instanceof AddressException) {
									rsne.setNotifyStatus(ReservationInternalConstants.PnrSegNotifyEvent.INVALID);
									reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.EmailReminder.SENT_STATUS,
											"<b>" + ReservationInternalConstants.PnrSegNotifyEvent.INVALID + "</b>");
									reservationAudit.addContentMap(
											AuditTemplateEnum.TemplateParams.EmailReminder.FAILURE_REASONS, "Address: "
													+ toAddress + " " + getRootCauseDetails(e));
								} else {
									rsne.setNotifyStatus(ReservationInternalConstants.PnrSegNotifyEvent.FAILED);
									reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.EmailReminder.SENT_STATUS,
											"<b>" + ReservationInternalConstants.PnrSegNotifyEvent.FAILED + "</b>");
									reservationAudit.addContentMap(
											AuditTemplateEnum.TemplateParams.EmailReminder.FAILURE_REASONS, "Address: "
													+ toAddress + " " + getRootCauseDetails(e));
								}
							}
							if (log.isInfoEnabled()) {
								log.info("PNR Anci reminder emailing status [ToAddress=" + toAddress + ", PnrSegId="
										+ segReminderNotifyDTO.getSegmentId() + ", SendStatus=" + rsne.getNotifyStatus()
										+ ", JobName=" + ancillaryReminderAuditDTO.getQrtzJobName() + "]");
							}
						}
						if (reservationSegmentNotificationEventCol.size() > 0) {
							colReservationAudit.add(reservationAudit);
							reservationBD.updatePnrSegmentNotifyStatus(reservationSegmentNotificationEventCol);
						} else {
							log.warn("PNR Anci reminder emailing status - empty flight list [JobName="
									+ ancillaryReminderAuditDTO.getQrtzJobName() + "]");
						}
					} else if (element != null && element instanceof OnlineCheckInReminderAuditDTO) {

						onlineCheckInReminderAuditDTO = (OnlineCheckInReminderAuditDTO) element;
						reservationSegmentNotificationEventCol = new ArrayList();
						reservationAudit = new ReservationAudit();
						reservationAudit.setUserId(onlineCheckInReminderAuditDTO.getUserId());
						reservationAudit.setDisplayName(MessagingModuleUtils.getSecurityBD()
								.getUserBasicDetails(reservationAudit.getUserId()).getDisplayName());

						ReservationSegmentNotificationEvent rsne = new ReservationSegmentNotificationEvent();
						reservationSegmentNotificationEventCol.add(rsne);

						rsne.setPnrSegmentId(onlineCheckInReminderAuditDTO.getPnrSegId());
						rsne.setPnrSegmentMsgEventId(onlineCheckInReminderAuditDTO.getPnrSegNotificationEventId());
						rsne.setNotificationType(ReservationInternalConstants.NotificationType.ONLINE_CHECKIN_REMINDER);
						rsne.setTimeStamp(CalendarUtil.getCurrentSystemTimeInZulu());

						reservationAudit.setPnr(onlineCheckInReminderAuditDTO.getPnr());
						reservationAudit.setZuluModificationDate(CalendarUtil.getCurrentSystemTimeInZulu());
						reservationAudit.setModificationType("EMLOCR");
						reservationAudit.setDescription("Online CheckIn Reminder Audit Data");

						reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.EmailReminder.EMAIL_ADDRESS, toAddress);

						// Which means email successfully sent
						if (e == null) {
							rsne.setNotifyStatus(ReservationInternalConstants.PnrSegNotifyEvent.SENT);
							reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.EmailReminder.SENT_STATUS, "<b>"
									+ ReservationInternalConstants.PnrSegNotifyEvent.SENT + "</b>");
							reservationAudit
									.addContentMap(AuditTemplateEnum.TemplateParams.EmailReminder.FAILURE_REASONS, "NONE");
						} else { // Which means email failure
							rsne.setFailureReason(getRootCauseDetails(e));
							if (e instanceof AddressException) {
								rsne.setNotifyStatus(ReservationInternalConstants.PnrSegNotifyEvent.INVALID);
								reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.EmailReminder.SENT_STATUS, "<b>"
										+ ReservationInternalConstants.PnrSegNotifyEvent.INVALID + "</b>");
								reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.EmailReminder.FAILURE_REASONS,
										"Address: " + toAddress + " " + getRootCauseDetails(e));
							} else {
								rsne.setNotifyStatus(ReservationInternalConstants.PnrSegNotifyEvent.FAILED);
								reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.EmailReminder.SENT_STATUS, "<b>"
										+ ReservationInternalConstants.PnrSegNotifyEvent.FAILED + "</b>");
								reservationAudit.addContentMap(AuditTemplateEnum.TemplateParams.EmailReminder.FAILURE_REASONS,
										"Address: " + toAddress + " " + getRootCauseDetails(e));
							}
						}
						if (log.isInfoEnabled()) {
							log.info("PNR Online Checkin reminder emailing status [ToAddress="
									+ toAddress
									+ ", Pnr="
									+ onlineCheckInReminderAuditDTO.getPnr()
									+ ", SendStatus="
									+ reservationAudit.getContentMap().get(
											AuditTemplateEnum.TemplateParams.EmailReminder.SENT_STATUS) + ", JobName="
									+ onlineCheckInReminderAuditDTO.getQrtzJobName() + "]");
						}

						colReservationAudit.add(reservationAudit);
						reservationBD.updatePnrSegmentNotifyStatus(reservationSegmentNotificationEventCol);

					} else { // FIXME Other objects are yet to be implemented
						log.error("|||FIXME! Auditing Other objects are yet to be implemented");
					}
				}

				if (colReservationAudit.size() > 0) {
					// Saves the audit collection
					auditorBD.audit(colReservationAudit);
				}
			} else {
				log.debug("|||AuditInfo Collection Not Found!");
			}
		} catch (Throwable recordReservationAuditException) {
			log.error("Error in Auditing In Messaging Module", recordReservationAuditException);
		}
	}

	/**
	 * 
	 * @param auditInfo
	 * @param e
	 * @param toAddress
	 */
	private void recordAgentAudit(Collection auditInfo, Throwable e, String toAddress, String messageBody) {

		try {

			if (auditInfo != null && !auditInfo.isEmpty()) {
				AuditorBD auditorBD = MessagingModuleUtils.getAuditorBD();

				Collection<AgentAudit> colAgentAudit = new ArrayList<AgentAudit>();
				AgentAudit agentAudit = null;
				for (Object object : auditInfo) {
					if (object instanceof AgentAudit) {
						agentAudit = (AgentAudit) object;

						// BANK GUARANTEE EXPIRY NOTIFIER EMAIL
						if (AuditTemplateEnum.BANK_GUR_EXP_EMAIL.getCode().equals(agentAudit.getTemplateCode())) {

							// No failure
							if (e == null) {
								agentAudit.addContentMap(
										AuditTemplateEnum.TemplateParams.BankGuaranteeExpireEmail.FAILURE_REASONS, "NONE");
								agentAudit.addContentMap(AuditTemplateEnum.TemplateParams.BankGuaranteeExpireEmail.SENT_STATUS,
										"SENT");

							} else {
								// Email Failed
								agentAudit.addContentMap(AuditTemplateEnum.TemplateParams.BankGuaranteeExpireEmail.SENT_STATUS,
										"FAILED");
								agentAudit.addContentMap(
										AuditTemplateEnum.TemplateParams.BankGuaranteeExpireEmail.FAILURE_REASONS,
										getRootCauseDetails(e));

								if (log.isErrorEnabled()) {
									log.error("Bank Guarantee Expiry Notification Email Failed for Agent : "
											+ agentAudit.getAgentCode() + " Agent Email Address is : " + toAddress);
								}

							}

							colAgentAudit.add(agentAudit);
						} else if (AuditTemplateEnum.CREDIT_LIMIT_UTILIZATION_EMAIL.getCode()
								.equals(agentAudit.getTemplateCode())
								|| AuditTemplateEnum.BSP_CREDIT_LIMIT_UTILIZATION_EMAIL.getCode().equals(
										agentAudit.getTemplateCode())) {

							// No failure
							if (e == null) {
								agentAudit.addContentMap(
										AuditTemplateEnum.TemplateParams.CreditLimitUtilizationEmail.FAILURE_REASONS, "NONE");
								agentAudit.addContentMap(
										AuditTemplateEnum.TemplateParams.CreditLimitUtilizationEmail.SENT_STATUS, "SENT");

							} else {
								// Email Failed
								agentAudit.addContentMap(
										AuditTemplateEnum.TemplateParams.CreditLimitUtilizationEmail.SENT_STATUS, "FAILED");
								agentAudit.addContentMap(
										AuditTemplateEnum.TemplateParams.CreditLimitUtilizationEmail.FAILURE_REASONS,
										getRootCauseDetails(e));

								if (log.isErrorEnabled()) {

									String errorLog = "Bank Guarantee Expiry Notification Email Failed for Agent : ";
									if (AuditTemplateEnum.CREDIT_LIMIT_UTILIZATION_EMAIL.getCode().equals(
											agentAudit.getTemplateCode())) {
										errorLog = "Agent Credit Limit Utilization Notification Email Failed for Agent : ";
									} else if (AuditTemplateEnum.BSP_CREDIT_LIMIT_UTILIZATION_EMAIL.getCode().equals(
											agentAudit.getTemplateCode())) {
										errorLog = "Agent BSP Credit Limit Utilization Notification Email Failed for Agent : ";
									}

									log.error(errorLog + agentAudit.getAgentCode() + " Agent Email Address is : " + toAddress);
								}

							}

							colAgentAudit.add(agentAudit);
						}
					}
				}

				if (!colAgentAudit.isEmpty()) {
					createDashBoardMessages(colAgentAudit, messageBody);
					auditorBD.audit(colAgentAudit);

					log.info("Agent Audit done for Agent Notifier email message");
				}

			} else {
				if (log.isDebugEnabled()) {
					log.debug("Agent AuditInfo Collection Not Found or Empty!");
				}
			}

		} catch (Throwable recordAgentAuditException) {
			if (log.isErrorEnabled()) {
				log.error("Error in Auditing In Messaging Module", recordAgentAuditException);
			}
		}
	}

	/**
	 * Create and save the dashboard messages
	 * 
	 * @param agentCollection
	 * @param message
	 * @throws ModuleException
	 */
	private void createDashBoardMessages(Collection agentCollection, String message) throws ModuleException {
		DashboardMsgTO dashboardMsgTO = null;
		AgentAudit agentAudit = null;
		CommonMasterBD commonMasterBD = MessagingModuleUtils.getCommonServiceBD();
		Date currentDate = new Date();
		List<DashboardMsgTO> msgTOs = new ArrayList<DashboardMsgTO>();

		for (Object object : agentCollection) {
			if (object instanceof AgentAudit) {
				dashboardMsgTO = new DashboardMsgTO();
				agentAudit = (AgentAudit) object;
				// Add days to expire to get the to date
				Date toDate = null;

				if (agentAudit.getDaysPriorToExpire() != null) {
					toDate = CalendarUtil.add(currentDate, 0, 0, agentAudit.getDaysPriorToExpire(), 0, 0, 0);
				} else {
					toDate = CalendarUtil.add(currentDate, 0, 0, agentAudit.getDefaultMessageValidity(), 0, 0, 0);
				}

				dashboardMsgTO.setAgentInclusionStatus(DashboardMessage.APPLY_STATUS_Y);
				dashboardMsgTO.setFromDate(CalendarUtil.formatDate(currentDate, CalendarUtil.PATTERN1));
				dashboardMsgTO.setMessageContent(message);
				dashboardMsgTO.setMessageType(DashboardMessage.ACCLEARO_MSG);
				dashboardMsgTO.setSelectedAgents(agentAudit.getAgentCode());
				dashboardMsgTO.setStatus(DashboardMessage.MSG_ACTIVE);
				dashboardMsgTO.setToDate(CalendarUtil.formatDate(toDate, CalendarUtil.PATTERN1));

				dashboardMsgTO.setPriority(DashboardMessage.ACCLEARO_PRIORITY);

				dashboardMsgTO.setAgentTypeInclusionStatus("N");
				dashboardMsgTO.setUserInclusionStatus("N");

				msgTOs.add(dashboardMsgTO);

			}
		}

		if (!msgTOs.isEmpty()) {
			commonMasterBD.saveDashboardMsg(msgTOs);
		}
	}

	/**
	 * Returns Root cause information
	 * 
	 * @param e
	 * @return
	 */
	private String getRootCauseDetails(Throwable e) {
		String genericErrorMsg = "General Failure";
		if (e == null) {
			return genericErrorMsg;
		}

		boolean hasCause = true;
		Throwable t = e;

		while (hasCause) {
			if (t.getCause() == null) {
				hasCause = false;
				genericErrorMsg = t.getMessage();

			} else {
				t = e.getCause();
			}
		}

		String message = genericErrorMsg.replaceAll("\\s*", "");
		try {
			return formatString(message);
		} catch (Exception e1) {
			log.error("Error in Formatting Error message for Auditing", e1);
			return message;
		}

	}

	/**
	 * Format String
	 * 
	 * @param msg
	 * @return
	 */
	public String formatString(String msg) {
		String strMsg = "";
		Iterator iterator = splitThisString(msg, 55).iterator();

		while (iterator.hasNext()) {
			strMsg += iterator.next() + "<br>";
		}
		return strMsg;
	}

	/**
	 * 
	 * @param strToSplit
	 * @param limit
	 * @return
	 */
	public static Collection splitThisString(String strToSplit, int limit) {
		Collection<String> results = new ArrayList<String>();
		if (strToSplit.length() >= limit) {
			String partOne = strToSplit.substring(0, limit);
			String partTwo = strToSplit.substring(limit);

			results.add(partOne);
			if (partTwo.length() > limit) {
				results.addAll(splitThisString(partTwo, limit));
			} else {
				results.add(partTwo);
			}
		} else {
			results.add(strToSplit);
		}
		return results;
	}

	/**
	 * (1) Will Check Type of Object (2) If exception is null, status is success else false!
	 * 
	 * @param objectInfo
	 * @param e
	 */
	private void updateObjectInfo(Collection objectInfo, Throwable e) {
		log.debug("|||Going to Record ReservationAudit");

		try {
			if (objectInfo != null && objectInfo.size() > 0) {
				Object element;

				for (Iterator itObject = objectInfo.iterator(); itObject.hasNext();) {
					element = itObject.next();

					// If objectInfo is type of FlightPnrNotification Only proceed..!
					if (element != null && element instanceof FlightPnrNotification) {
						FlightPnrNotification objFltPnr = (FlightPnrNotification) element;

						// Which means Email successfully sent
						if (e == null) {
							objFltPnr.setDeliveryStatus("D"); // SMS Delivered successfully
						}
						// Which means Email failure
						else {
							objFltPnr.setDeliveryStatus("F"); // Sending SMS Failed
						}
						// Saves the object collection
						MessagingModuleUtils.getAlertingBD().saveFlightPnrNotification(objFltPnr);
					} else if (element != null && element instanceof FlightMealNotifyStatusDTO) {
						FlightMealNotifyStatusDTO flightMealNotifyStatusDTO = (FlightMealNotifyStatusDTO) element;
						if (e == null) {
							flightMealNotifyStatusDTO.setStatus(Flight.MEAL_NOTIFICATION_SENT);
						} else {
							flightMealNotifyStatusDTO.setStatus(Flight.MEAL_NOTIFICATION_FAILED);
						}
						MessagingModuleUtils.getFlightBD().updateFlightMealNotifyStatus(flightMealNotifyStatusDTO);

					} else if (element != null && element instanceof UpdateEmailStatusInfoDTO) {
						UpdateEmailStatusInfoDTO updateInfoDTO = (UpdateEmailStatusInfoDTO) element;
						if (e == null) {
							updateInfoDTO.setStatus(SSRCategory.NOTIFICATION_EMAIL_SENT);
						} else {
							updateInfoDTO.setStatus(SSRCategory.NOTIFICATION_EMAIL_NOT_SENT);
						}
						messgingJdbcDAO.updatePaxSegmentSSREmailSentStatus(updateInfoDTO);
					} else { // FIXME Other objects are yet to be implemented
						log.error("|||FIXME! Other objects are yet to be implemented");
					}
				}

			} else {
				log.debug("|||ObjectInfo Collection Not Found!");
			}
		} catch (Throwable ex) {
			log.error("Error in updating object In Messaging Module", ex);
		}
	}

	private void recordFlightSegmentStatus(Collection auditInfo, Throwable e, int emailCount, int pnrsCount) {
		try {
			if (auditInfo != null && auditInfo.size() > 0) {
				FlightBD flightBD = MessagingModuleUtils.getFlightBD();
				SchedulerBD schedulerBD = MessagingModuleUtils.getSchedulerBD();

				AncillaryReminderAuditDTO reminderAuditDTO;
				OnlineCheckInReminderAuditDTO onlineCheckinReminderAuditDTO;
				Object element;

				for (Iterator itObject = auditInfo.iterator(); itObject.hasNext();) {
					element = itObject.next();
					if (element instanceof AncillaryReminderAuditDTO) {
						reminderAuditDTO = (AncillaryReminderAuditDTO) element;
						String jobName = reminderAuditDTO.getQrtzJobName();
						String jobGroupName = reminderAuditDTO.getQrtzJobGroupName();
						Integer flightSegId = reminderAuditDTO.getFlightSegmentId();
						Integer flightSegNotificationId = reminderAuditDTO.getFlightSegNotificationId();

						String status = null;
						if (pnrsCount > emailCount || (e != null && !(e instanceof AddressException))) {
							status = "FAILED";
							flightBD.updateFlightSegmentNotifyStatus(flightSegId, flightSegNotificationId, status,
									getRootCauseDetails(e), emailCount, null);
						} else {
							status = "SENT";
							flightBD.updateFlightSegmentNotifyStatus(flightSegId, flightSegNotificationId, status, "",
									emailCount, null);
							try {
								// REMOVE THE JOB FROM QRTZ TABLE
								if (log.isInfoEnabled()) {
									log.info("Removing execution completed anci reminder job. [JobName=" + jobName
											+ ", JobGroupName=" + jobGroupName + "]");
								}
								schedulerBD.removeJob(jobName, jobGroupName);
							} catch (Exception exception) {
								log.error("Exception in removing anci reminder quartz job [JobName=" + jobName
										+ ", JobGroupName=" + jobGroupName + "]", exception);
							}
						}
						if (log.isInfoEnabled()) {
							log.info("Before calling flight update flight notify status [FltSegId="
									+ reminderAuditDTO.getFlightSegmentId() + ", flightSegNotificationId="
									+ flightSegNotificationId + ", JobName=" + jobName + ", JobGroupName=" + jobGroupName
									+ ", pnrsCount=" + pnrsCount + ", sentOrInvalidEmailCount=" + emailCount
									+ ", flightSegNotifyStatus=" + status + "]");
						}
					} else if (element instanceof OnlineCheckInReminderAuditDTO) {

						onlineCheckinReminderAuditDTO = (OnlineCheckInReminderAuditDTO) element;
						String jobName = onlineCheckinReminderAuditDTO.getQrtzJobName();
						String jobGroupName = onlineCheckinReminderAuditDTO.getQrtzJobGroupName();
						Integer flightSegId = onlineCheckinReminderAuditDTO.getFlightSegmentId();
						Integer flightSegNotificationId = onlineCheckinReminderAuditDTO.getFlightSegNotificationId();

						String status = null;
						if (pnrsCount > emailCount || (e != null && !(e instanceof AddressException))) {
							status = "FAILED";
							flightBD.updateFlightSegmentNotifyStatus(flightSegId, flightSegNotificationId, status,
									getRootCauseDetails(e), emailCount, null);
						} else {
							status = "SENT";
							flightBD.updateFlightSegmentNotifyStatus(flightSegId, flightSegNotificationId, status, "",
									emailCount, ReservationInternalConstants.NotificationType.ONLINE_CHECKIN_REMINDER);
							try {
								// REMOVE THE JOB FROM QRTZ TABLE
								if (log.isInfoEnabled()) {
									log.info("Removing execution completed online checkin reminder job. [JobName=" + jobName
											+ ", JobGroupName=" + jobGroupName + "]");
								}
								schedulerBD.removeJob(jobName, jobGroupName);
							} catch (Exception exception) {
								log.error("Exception in removing online checkin reminder quartz job [JobName=" + jobName
										+ ", JobGroupName=" + jobGroupName + "]", exception);
							}
						}
						if (log.isInfoEnabled()) {
							log.info("Before calling flight update flight notify status [FltSegId="
									+ onlineCheckinReminderAuditDTO.getFlightSegmentId() + ", flightSegNotificationId="
									+ flightSegNotificationId + ", JobName=" + jobName + ", JobGroupName=" + jobGroupName
									+ ", pnrsCount=" + pnrsCount + ", sentOrInvalidEmailCount=" + emailCount
									+ ", flightSegNotifyStatus=" + status + "]");
						}

					} else { // FIXME Other objects are yet to be implemented
						log.error("|||FIXME! Other objects are yet to be implemented");
					}
				}
			} else {
				log.debug("|||ObjectInfo Collection Not Found!");
			}
		} catch (Throwable ex) {
			log.error("Error in updating flight notification status", ex);
		}
	}

	public void sendSimpleTextEmail(SimpleEmailDTO simpleEmailDTO) {
		try {
			MessagingModuleConfig moduleConfig = (MessagingModuleConfig) LookupServiceFactory.getInstance().getModuleConfig(
					MessagingConstants.MODULE_NAME);

			Properties props = new Properties();

			MailServerConfig mailServerConfig = (MailServerConfig) moduleConfig.getMailServersConfigurationMap().get(
					MessagingCustomConstants.DEFAULT_MAIL_SERVER_KEY);

			props.put(SMTP_HOST, mailServerConfig.getHostAddress());

			Authenticator authenticator = null;
			if (!("").equals(mailServerConfig.getUserName()) && !("").equals(mailServerConfig.getPassword())) {
				props.put(SMTP_AUTHENITCATE, "true");
				authenticator = new MessagingAuthenticator(mailServerConfig.getUserName(), mailServerConfig.getPassword());
			}

			Session mailsession = Session.getDefaultInstance(props, authenticator);
			javax.mail.Message msg = new MimeMessage(mailsession);
			msg.setRecipient(javax.mail.Message.RecipientType.TO, new InternetAddress(simpleEmailDTO.getToEmailAddress()));
			msg.setSubject(simpleEmailDTO.getSubject());
			msg.setFrom(new InternetAddress(simpleEmailDTO.getFromAddress()));
			msg.setText(simpleEmailDTO.getContent());
			Transport.send(msg);
			log.debug("EMail Sent Successfully.");
		} catch (Exception e) {
			log.error(e);
		}
	}
}
