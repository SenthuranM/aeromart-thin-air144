package com.isa.thinair.messaging.core.config;

import java.io.Serializable;

public class ExternalSitaConfig implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String sitaAddress;
	
	private String sitaLocation;
	
	private String msgType;

	public String getSitaAddress() {
		return sitaAddress;
	}

	public void setSitaAddress(String sitaAddress) {
		this.sitaAddress = sitaAddress;
	}

	public String getSitaLocation() {
		return sitaLocation;
	}

	public void setSitaLocation(String sitaLocation) {
		this.sitaLocation = sitaLocation;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}
}
