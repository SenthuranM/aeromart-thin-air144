/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.messaging.core.remoting.ejb;

import java.util.ArrayList;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.login.util.ForceLoginInvoker;
import com.isa.thinair.messaging.api.service.MessagingModuleUtils;
import com.isa.thinair.messaging.core.config.MessagingModuleConfig;
import com.isa.thinair.messaging.core.processor.MessageManager;

/**
 * 
 * @author Srikantha
 */
@MessageDriven(name = "MessagingServiceMDB", activationConfig = {
		@ActivationConfigProperty(propertyName = "destination", propertyValue = "queue/isaQueue"),
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
		@ActivationConfigProperty(propertyName = "transactionTimeout", propertyValue = "3600") })
public class MessagingServiceMDB implements MessageListener {
	private static final long serialVersionUID = -6583759698965963589L;
	private final Log logger = LogFactory.getLog(getClass());

	public void onMessage(Message message) {
		try {
			MessagingModuleConfig messagingModuleConfig = MessagingModuleUtils.getMessagingModuleConfig();
			if (messagingModuleConfig.isInvokeCredentials()) {
				ForceLoginInvoker.login(messagingModuleConfig.getEjbAccessUsername(),
						messagingModuleConfig.getEjbAccessPassword());
			}

			if (logger.isDebugEnabled()) {
				logger.debug("Entered MessagingServiceMDB.onMessage()");
			}
			ObjectMessage objMessage = (ObjectMessage) message;
			ArrayList msgProfiles = (ArrayList) objMessage.getObject();
			MessageManager manager = new MessageManager();
			manager.processMessages(msgProfiles);
			if (logger.isDebugEnabled()) {
				logger.debug("Entered MessagingServiceMDB.onMessage()");
			}
		} catch (JMSException jmsException) {
			logger.error("Exception in MessagingServiceMDB:onMessage() " + jmsException.getMessage(), jmsException);
		} catch (Exception exception) {
			logger.error("Exception in MessagingServiceMDB:onMessage() " + exception.getMessage(), exception);
		} finally {
			if (MessagingModuleUtils.getMessagingModuleConfig().isInvokeCredentials()) {
				ForceLoginInvoker.close();
			}
		}
	}
}
