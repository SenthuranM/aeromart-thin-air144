package com.isa.thinair.messaging.core.processor;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.PD4MLBridge;

/**
 * @author Nilindra Fernando
 */
public class PDFComposer {

	private static final Log log = LogFactory.getLog(PDFComposer.class);

	public static byte[] xhtmlPdfConversion(byte[] arr) {
		GlobalConfig globalConfig = CommonsServices.getGlobalConfig();
		if (globalConfig.getUseProxy()) {
			System.setProperty("http.proxyHost", globalConfig.getHttpProxy());
			System.setProperty("http.proxyPort",
					String.valueOf(globalConfig.getHttpPort()));
		}

		try {
			return PD4MLBridge.getInstance().render(arr);
		} catch (Throwable e) {
			log.error("PDFComposer ", e);
		}

		return null;
	}

}
