package com.isa.thinair.messaging.core.config;

import java.io.Serializable;

/**
 * 
 * @author Thushara
 * 
 */
public class SmsConfig implements Serializable {

	private static final long serialVersionUID = 4735492802837340187L;

	private String smsServer;

	private String smsDBName;

	private String smsDBPassword;

	private String smsUserName;

	private String smsUserPassword;

	private int smsMode;

	private String smsGateway;

	private boolean proxyEnable;

	private String proxyServer;

	private int proxyPort;

	private String ipconfig;

	private String senderID;

	private String apiKey;
	
	// Specific for SMSBOX Integration
	private int customerID;
	
	private String isBlink;
	
	private String isFlash;

	/**
	 * @return the senderID
	 */
	public String getSenderID() {
		return senderID;
	}

	/**
	 * @param senderID
	 *            the senderID to set
	 */
	public void setSenderID(String senderID) {
		this.senderID = senderID;
	}

	public String getIpconfig() {
		return ipconfig;
	}

	public void setIpconfig(String ipconfig) {
		this.ipconfig = ipconfig;
	}

	public String getSmsServer() {
		return smsServer;
	}

	public void setSmsServer(String smsServer) {
		this.smsServer = smsServer;
	}

	public String getSmsDBName() {
		return smsDBName;
	}

	public void setSmsDBName(String smsDBName) {
		this.smsDBName = smsDBName;
	}

	public String getSmsDBPassword() {
		return smsDBPassword;
	}

	public void setSmsDBPassword(String smsDBPassword) {
		this.smsDBPassword = smsDBPassword;
	}

	public String getSmsUserName() {
		return smsUserName;
	}

	public void setSmsUserName(String smsUserName) {
		this.smsUserName = smsUserName;
	}

	public String getSmsUserPassword() {
		return smsUserPassword;
	}

	public void setSmsUserPassword(String smsUserPassword) {
		this.smsUserPassword = smsUserPassword;
	}

	public int getSmsMode() {
		return smsMode;
	}

	public void setSmsMode(int smsMode) {
		this.smsMode = smsMode;
	}

	public String getSmsGateway() {
		return smsGateway;
	}

	public void setSmsGateway(String smsGateway) {
		this.smsGateway = smsGateway;
	}

	public boolean isProxyEnable() {
		return proxyEnable;
	}

	public void setProxyEnable(boolean proxyEnable) {
		this.proxyEnable = proxyEnable;
	}

	public String getProxyServer() {
		return proxyServer;
	}

	public void setProxyServer(String proxyServer) {
		this.proxyServer = proxyServer;
	}

	public int getProxyPort() {
		return proxyPort;
	}

	public void setProxyPort(int proxyPort) {
		this.proxyPort = proxyPort;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public int getCustomerID() {
		return customerID;
	}

	public void setCustomerID(int customerID) {
		this.customerID = customerID;
	}

	public String getIsBlink() {
		return isBlink;
	}

	public void setIsBlink(String isBlink) {
		this.isBlink = isBlink;
	}

	public String getIsFlash() {
		return isFlash;
	}

	public void setIsFlash(String isFlash) {
		this.isFlash = isFlash;
	}	

}
