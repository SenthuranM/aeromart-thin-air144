/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 06, 2005 15:28:39
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.messaging.core.service;

import com.isa.thinair.platform.api.DefaultModule;

/**
 * @author Nasly
 * @isa.module.service-interface module-name="messaging" description="module responsible for handling emails and alerts"
 */
public class MessagingService extends DefaultModule {
}
