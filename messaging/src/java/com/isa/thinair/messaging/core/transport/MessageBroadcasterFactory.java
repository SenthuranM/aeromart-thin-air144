/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jun 30, 2005
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.messaging.core.transport;

import com.isa.thinair.messaging.core.util.SMSBroadCasterFactory;

/**
 * @author sumudu
 * 
 *         MessageBroadcasterFactory is the factory creating broadcasters
 * 
 */
public abstract class MessageBroadcasterFactory {
	/**
	 * 
	 * @param type
	 * @return MessageBroadcaster
	 */
	public static MessageBroadcaster getBroadcaster(String type) {
		MessageBroadcaster broadcaster = null;
		String messageType = type.toUpperCase();

		if (messageType.equals("EMAIL")) {
			broadcaster = new EmailBroadcaster();
		}

		if (messageType.equals("LAN")) {
			broadcaster = new LANBroadcaster();
		}

		if (messageType.equals("SMS")) {
			broadcaster = SMSBroadCasterFactory.getSMSBroadcaster();
		}

		return broadcaster;
	}
}
