/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 06, 2005 15:28:39
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.messaging.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.messaging.api.service.MessagingServiceBD;

/**
 * @author Nasly
 */
@Local
public interface MessagingServiceLocal extends MessagingServiceBD {

}
