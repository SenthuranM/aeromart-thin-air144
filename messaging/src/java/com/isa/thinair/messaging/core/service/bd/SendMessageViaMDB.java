/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.messaging.core.service.bd;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseServiceDelegate;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.service.MessagingModuleUtils;

/**
 * @author Srikantha
 * 
 */
public class SendMessageViaMDB extends PlatformBaseServiceDelegate {
	private final Log log = LogFactory.getLog(getClass());

	private static final java.lang.String DESTINATION_JNDI_NAME = "queue/isaQueue";
	private static final java.lang.String CONNECTION_FACTORY_JNDI_NAME = "ConnectionFactory";

	public void sendMessageViaMDB(MessageProfile impl) {
		ArrayList<MessageProfile> msgList = new ArrayList<MessageProfile>();
		msgList.add(impl);

		try {
			sendMessage(msgList, MessagingModuleUtils.getInstance().getModuleConfig().getJndiProperties(), DESTINATION_JNDI_NAME,
					CONNECTION_FACTORY_JNDI_NAME);
		} catch (ModuleException moduleException) {
			log.error("Sending message via jms failed", moduleException);
		}
	}

	public void sendMessagesViaMDB(List profileList) {
		try {
			sendMessage((ArrayList) profileList, MessagingModuleUtils.getInstance().getModuleConfig().getJndiProperties(),
					DESTINATION_JNDI_NAME, CONNECTION_FACTORY_JNDI_NAME);
		} catch (ModuleException moduleException) {
			log.error("Sending message via jms failed", moduleException);
		}
	}
}
