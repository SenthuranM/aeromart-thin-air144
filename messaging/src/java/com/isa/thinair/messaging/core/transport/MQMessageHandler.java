package com.isa.thinair.messaging.core.transport;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ibm.mq.MQC;
import com.ibm.mq.MQEnvironment;
import com.ibm.mq.MQException;
import com.ibm.mq.MQMessage;
import com.ibm.mq.MQPutMessageOptions;
import com.ibm.mq.MQQueue;
import com.ibm.mq.MQQueueManager;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.messaging.api.utils.MessagingConstants;
import com.isa.thinair.messaging.core.config.MQConfig;
import com.isa.thinair.messaging.core.config.MessagingModuleConfig;
import com.isa.thinair.platform.api.LookupServiceFactory;

public class MQMessageHandler {
	private final Log log = LogFactory.getLog(getClass());

	private MQQueueManager queueManager;

	private MQQueue queue;

	public MQMessageHandler(String countryCode) throws ModuleException {
		MessagingModuleConfig moduleConfig = (MessagingModuleConfig) LookupServiceFactory.getInstance().getModuleConfig(
				MessagingConstants.MODULE_NAME);
		if (moduleConfig.getpNRGOVConfigMap().containsKey(countryCode)){
			MQConfig mqConfig = (MQConfig) moduleConfig.getpNRGOVConfigMap().get(countryCode);
			MQEnvironment.hostname = mqConfig.getPnrGovMQHostName();
			MQEnvironment.port = mqConfig.getPnrGovMQListeningPort();
			MQEnvironment.channel = mqConfig.getPnrGovMQChannel();
			MQEnvironment.properties.put(MQC.TRANSPORT_PROPERTY, MQC.TRANSPORT_MQSERIES);
		} else {
			log.error("MQ country code does not exist.");
			throw new ModuleException("MQ country code does not exist.");
		}

	}

	public void pushMessage(String message, String countryCode) throws ModuleException {
		initializeMQAccess(countryCode);
		MQPutMessageOptions pmo = new MQPutMessageOptions();

		try {
			MQMessage mqMessage = new MQMessage();

			mqMessage.writeString(message);
			queue.put(mqMessage, pmo);
		} catch (Throwable e) {
			log.error("MQPUSH error occured . " + e);
			throw new ModuleException(e, "Websphere MQ push error.");
		}
	}

	public void closeConnections() throws ModuleException {
		try {
			queue.close();
			queueManager.disconnect();
		} catch (MQException e) {
			log.error("WebsphereMQ error occurred.", e);
			throw new ModuleException(e,"Websphere MQ Close connection Error");
		}

	}
	
	private void initializeMQAccess(String countryCode) throws ModuleException {
		MessagingModuleConfig moduleConfig = (MessagingModuleConfig) LookupServiceFactory.getInstance().getModuleConfig(
				MessagingConstants.MODULE_NAME);
		Map<String, MQConfig> mqConfigMap = (Map<String, MQConfig>) moduleConfig.getpNRGOVConfigMap();
		if (mqConfigMap != null && !mqConfigMap.isEmpty() && mqConfigMap.containsKey(countryCode)) {
			MQConfig mqConfig = mqConfigMap.get(countryCode);
			String mqName = mqConfig.getPnrGovMQName();
			int openOptions = MQC.MQOO_OUTPUT;
			try {
				queueManager = new MQQueueManager(mqConfig.getPnrGovMQManager());
				queue = queueManager.accessQueue(mqName, openOptions);
			} catch (MQException e) {
				throw new ModuleException("WEBSPHERE MQ ERROR OCCURED. " + e.getMessage());
			}
		} else {
			throw new ModuleException("MQ NAME ERROR");
		}
	}

}
