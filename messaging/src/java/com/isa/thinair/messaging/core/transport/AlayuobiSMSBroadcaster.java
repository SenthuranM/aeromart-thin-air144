package com.isa.thinair.messaging.core.transport;

import java.util.Iterator;
import java.util.List;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HostConfiguration;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.messaging.api.model.SmsMessage;
import com.isa.thinair.messaging.api.utils.MessagingConstants;
import com.isa.thinair.messaging.core.config.MessagingModuleConfig;
import com.isa.thinair.messaging.core.config.SmsConfig;
import com.isa.thinair.messaging.core.util.SMSAuditor;
import com.isa.thinair.platform.api.LookupServiceFactory;

public class AlayuobiSMSBroadcaster extends SMSAuditor implements MessageBroadcaster {

	private final Log log = LogFactory.getLog(getClass());

	private final String NOT_SENT = "0000";
	private final String INVALID_SERVER = "0005";
	private final String USER_NOT_PROVIDED = "0010";
	private final String PASSWD_NOT_PROVIDED = "0011";
	private final String INVAID_LOGIN = "00";
	private final String INSUFFICIENT_CREDIT = "0020";
	private final String INVALID_SENDER_ID = "0030";
	private final String MOBILENO_NOT_PROVIDED = "0040";
	private final String INVLID_MOBILE = "0041";
	private final String NETWORK_NOT_SUPPORTED = "0042"; // both are for net work not
															// supported
	private final String INVALID_MESSAGE = "0050";
	private final String INVALID_QUANTITY = "0060";
	private final String NETWORK_NOT_SUPPORTED2 = "0066"; // both are for net work not
															// supported
	private final String SMS_CONFIG_NAME = "alayoubi_sms";

	public void sendMessages(List messageList) {

		SmsMessage smsMessage = null;
		Throwable xceptionToAudit = null;
		boolean success = true;

		MessagingModuleConfig messModConfig = (MessagingModuleConfig) LookupServiceFactory.getInstance().getModuleConfig(
				MessagingConstants.MODULE_NAME);
		SmsConfig smsConfig = (SmsConfig) messModConfig.getSmsConfigurationMap().get(SMS_CONFIG_NAME);
		HttpClient client = new HttpClient();

		// Dn't need in prod as we dn't have a proxy
		if (smsConfig.isProxyEnable()) {
			HostConfiguration hcon = new HostConfiguration();
			hcon.setProxy(smsConfig.getProxyServer(), smsConfig.getProxyPort());
			client.setHostConfiguration(hcon);
		}

		if (messageList != null) {

			Iterator iter = messageList.iterator();

			String server = smsConfig.getSmsServer();
			String smsUserName = smsConfig.getSmsUserName();
			String smsPassword = smsConfig.getSmsUserPassword();
			String smsIp = smsConfig.getIpconfig();

			StringBuilder sb = null;
			GetMethod method = null;

			String smsText = null;
			String recipient = null;
			String smsHeader = null;
			String errorMessage = "";

			while (iter.hasNext()) {

				sb = new StringBuilder();
				sb.append(server);
				sb.append("?username=" + getEncodesStr(smsUserName));
				sb.append("&password=" + getEncodesStr(smsPassword));

				try {
					smsMessage = (SmsMessage) iter.next();
					smsText = getEncodesStr(smsMessage.getBody());
					recipient = getEncodesStr(smsMessage.getRecipient());
					smsHeader = getEncodesStr(smsMessage.getSubject());
					recipient = recipient.replace("-", "");

					if (smsText.length() > 160)
						smsText = smsText.substring(0, 159);

					sb.append("&mno=" + recipient);
					sb.append("&msg=" + smsText);
					sb.append("&Sid=" + getEncodesStr("Air Arabia"));
					sb.append("&fl=0&mt=0");
					sb.append("&ipcl=" + smsIp);

					// only supports get method
					method = new GetMethod(sb.toString());
					// Provide custom retry handler is necessary
					method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(0, false));
					String msg;

					int statusCode = client.executeMethod(method);

					if (statusCode != HttpStatus.SC_OK) {
						System.err.println("Method failed: " + method.getStatusLine());
					}

					// Read the response body.
					byte[] responseBody = method.getResponseBody();
					msg = new String(responseBody);
					if (msg.length() > 4) {
						if (log.isInfoEnabled()) {
							log.info("SMS successfully sent. [message id=" + msg + ",recipient=" + recipient + ",smsText="
									+ smsText + "]");
						}
					} else {
						success = false;
						String strError = "";
						if (msg != null) {
							if (msg.equals(NOT_SENT)) {
								strError = "Not sent";
							} else if (msg.equals(INVALID_SERVER)) {
								strError = "Invalid server";
							} else if (msg.equals(USER_NOT_PROVIDED)) {
								strError = "No user name";
							} else if (msg.equals(PASSWD_NOT_PROVIDED)) {
								strError = "no password";
							} else if (msg.equals(INVAID_LOGIN)) {
								strError = "invalid user name password";
							} else if (msg.equals(INSUFFICIENT_CREDIT)) {
								strError = "insufficient credit";
							} else if (msg.equals(INVALID_SENDER_ID)) {
								strError = "invalid sender id";
							} else if (msg.equals(MOBILENO_NOT_PROVIDED)) {
								strError = "no mobile no";
							} else if (msg.equals(INVLID_MOBILE)) {
								strError = "invlid mobile number";
							} else if (msg.equals(NETWORK_NOT_SUPPORTED) || msg.equals(NETWORK_NOT_SUPPORTED2)) {
								strError = "network is not supported";
							} else if (msg.equals(INVALID_MESSAGE)) {
								strError = "invalid message";
							} else if (msg.equals(INVALID_QUANTITY)) {
								strError = "invalid quantity";
							}
						}

						if (log.isErrorEnabled()) {
							log.error("Sending SMS failed. [error= " + strError + ",recipient=" + recipient + ",smsText="
									+ smsText + "]");
						}
						errorMessage = "Error sending SMS ";
						sendErrorAlert(errorMessage, messModConfig, errorMessage + " : " + strError);
					}

				} catch (Exception e) {
					xceptionToAudit = e;
					log.error("Sending SMS failed. [recipient=" + recipient + ",smsText=" + smsText + "]", e);
					errorMessage = "Exception in Sending SMS";
					sendErrorAlert(errorMessage, messModConfig, messModConfig + " : " + e);
				} finally {
					// Performs auditing
					this.recordReservationAudit(smsMessage.getAuditInfo(), xceptionToAudit, success, smsMessage.getRecipient());
					if (smsMessage.getAuditInfo() != null)
						smsMessage.getAuditInfo().clear();
					this.updateObjectInfo(smsMessage.getObjectInfo(), xceptionToAudit, success);
					method.releaseConnection();
				}
			}

		}
	}

	/**
	 * Simple replace is used if the charcters get too many need to write reg express URLENCODER didnot work
	 * 
	 * @param str
	 * @return
	 */
	private String getEncodesStr(String str) {
		str = str.replace("&", "%26");
		str = str.replace("+", "%2B");
		str = str.replace("%", "%25");
		str = str.replace("#", "%23");
		str = str.replace("=", "%0A");
		str = str.replace(" ", "%20");
		return str;
	}
}
