/*
 * Created on Jun 29, 2005
 *
 */
package com.isa.thinair.messaging.api.model;

import java.io.Serializable;
import java.util.Collection;

/**
 * @author Nasly
 */
public class EmailMessage implements Message, Serializable {

	private static final long serialVersionUID = 9152620244551955354L;

	private String subject;

	private String body;

	private String fromAddress;

	private String toAddress;

	private String id;

	private String mailServerConfigurationName;

	private String contentType;

	private Collection attachmentFileNames;

	private boolean attachBody;

	private String bodyAttachmentName;

	private byte[] msgBodyByteArray;

	private String msgBodyByteArrayType;

	private Collection auditInfo;

	private Collection objectInfo;

	private String attachmentType;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSubject() {
		return subject;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getFromAddress() {
		return fromAddress;
	}

	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	public String getToAddress() {
		return toAddress;
	}

	public void setToAddress(String toAddress) {
		this.toAddress = toAddress;
	}

	public String getMailServerConfigurationName() {
		return mailServerConfigurationName;
	}

	public void setMailServerConfigurationName(String mailServerConfigurationName) {
		this.mailServerConfigurationName = mailServerConfigurationName;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public Collection getAttachmentFileNames() {
		return attachmentFileNames;
	}

	public void setAttachmentFileNames(Collection attachmentFileNames) {
		this.attachmentFileNames = attachmentFileNames;
	}

	/**
	 * @return Returns the auditInfo.
	 */
	public Collection getAuditInfo() {
		return auditInfo;
	}

	/**
	 * @param auditInfo
	 *            The auditInfo to set.
	 */
	public void setAuditInfo(Collection auditInfo) {
		this.auditInfo = auditInfo;
	}

	public boolean isAttachBody() {
		return attachBody;
	}

	public void setAttachBody(boolean attachBody) {
		this.attachBody = attachBody;
	}

	public String getBodyAttachmentName() {
		return bodyAttachmentName;
	}

	public void setBodyAttachmentName(String bodyAttachmentName) {
		this.bodyAttachmentName = bodyAttachmentName;
	}

	public byte[] getMsgBodyByteArray() {
		return msgBodyByteArray;
	}

	public void setMsgBodyByteArray(byte[] msgBodyByteArray) {
		this.msgBodyByteArray = msgBodyByteArray;
	}

	public String getMsgBodyByteArrayType() {
		return msgBodyByteArrayType;
	}

	public void setMsgBodyByteArrayType(String msgBodyByteArrayType) {
		this.msgBodyByteArrayType = msgBodyByteArrayType;
	}

	/**
	 * @return Returns the objectInfo.
	 */
	public Collection getObjectInfo() {
		return objectInfo;
	}

	/**
	 * @param objectInfo
	 *            The objectInfo to set.
	 */
	public void setObjectInfo(Collection objectInfo) {
		this.objectInfo = objectInfo;
	}

	/**
	 * @return the attachmentType
	 */
	public String getAttachmentType() {
		return attachmentType;
	}

	/**
	 * @param attachmentType
	 *            the attachmentType to set
	 */
	public void setAttachmentType(String attachmentType) {
		this.attachmentType = attachmentType;
	}

}
