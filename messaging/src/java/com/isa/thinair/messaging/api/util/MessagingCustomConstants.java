package com.isa.thinair.messaging.api.util;

public abstract class MessagingCustomConstants {
	public static final String MAIL_SERVER_CONFIGS_CODE_PNL_ADL_TRANSMISSION = "mailserver_pnladl";
	public static final String MAIL_SERVER_CONFIGS_CODE_PAL_CAL_TRANSMISSION = "mailserver_palcal";
	public static final String DEFAULT_MAIL_SERVER_KEY = "default";
	public static final String RESON_SMS_BROADCASTER = "RESON8";
	public static final String ALAYOUBI_SMS_BROADCASTER = "ALAYOUBI";
	public static final String VECTRAMIND_SMS_BROADCASTER = "VECTRAMIND";
	public static final String LESMS_BROADCASTER = "LESMS";
	public static final String SMSCONNECT_SMS_BROADCASTER = "SMSCONNECT";
	public static final String MAGFA_SMS_BROADCASTER = "MAGFA";
	public static final String NETCAST_SMS_BROADCASTER = "NETCAST";
	public static final String AXON_SMS_BROADCASTER = "AXON";
	public static final String SYRIATEL_SMS_BROADCASTER = "SYRIATEL";
	public static final String DHIRAAGU_SMS_BROADCASTER = "DIRAAGU";
	public static final String TEST_EXT_SITA = "testExternalSita";
	public static final String SMSBOX_SMS_BROADCASTER = "SMSBOX";
	
}
