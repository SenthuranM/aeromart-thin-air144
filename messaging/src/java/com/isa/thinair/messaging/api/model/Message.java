/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 06, 2005 15:28:39
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.messaging.api.model;

/**
 * @author Nasly
 * 
 */
public interface Message {

	public static final String CONTENT_TYPE_HTML = "text/html; charset=UTF-8;";

	public static final String CONTENT_TYPE_TEXT = "text/plain";

	public static final String CONTENT_BYTE_ARRAY_TYPE = "UTF-8";

	public static final String ATTACHMENT_TYPE_PDF = "PDF";

	public static final String ATTACHMENT_TYPE_HTML = "HTML";

	public String getSubject();

	public String getBody();

	public void setBody(String body);

	public void setSubject(String subject);
}
