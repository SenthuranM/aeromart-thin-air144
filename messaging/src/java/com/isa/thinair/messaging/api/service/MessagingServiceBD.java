/*
 * Created on Jun 29, 2005
 *
 */
package com.isa.thinair.messaging.api.service;

import java.util.Collection;
import java.util.List;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.messaging.api.dto.LogDetailDTO;
import com.isa.thinair.messaging.api.dto.SimpleEmailDTO;
import com.isa.thinair.messaging.api.model.EmailMessage;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.core.config.MailServerConfig;

/**
 * Business delegate for messaging module
 * 
 * @author Nasly
 */
public interface MessagingServiceBD {

	public static final String SERVICE_NAME = "MessageServices";

	public void sendMessage(MessageProfile messageProfile);

	public void sendMessages(List profilesList);

	public void recordMessage(LogDetailDTO logDetailDTO) throws ModuleException;

	public Collection getMailServerConfigs(String configurationCode) throws ModuleException;

	public MailServerConfig getDefaultMailServer() throws ModuleException;

	public void saveEmail(EmailMessage message) throws ModuleException;

	public List loadEmails() throws ModuleException;

	public void sendSimpleEmail(SimpleEmailDTO simpleEmailDTO) throws ModuleException;

	public void sendSimpleTextEmail(SimpleEmailDTO simpleEmailDTO) throws ModuleException;
	
	public void saveAndPushPNRGOVMessage(String message, String fileName, String countryCode) throws ModuleException;
}
