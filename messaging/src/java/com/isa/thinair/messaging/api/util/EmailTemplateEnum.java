package com.isa.thinair.messaging.api.util;

import java.io.Serializable;

/**
 * @author Lasantha Pambagoda
 */
public class EmailTemplateEnum implements Serializable {

	private static final long serialVersionUID = -143120665178743333L;
	public static final String GENERAL_CANCEL_RESCHEDULE_TEMPLATE = "change_flight";

}
