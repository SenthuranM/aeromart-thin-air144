package com.isa.thinair.messaging.api.dto;

import java.io.Serializable;

public class SimpleEmailDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String toEmailAddress;

	private String subject;

	private String content;

	private String fromAddress;

	public String getToEmailAddress() {
		return toEmailAddress;
	}

	public String getSubject() {
		return subject;
	}

	public String getContent() {
		return content;
	}

	public String getFromAddress() {
		return fromAddress;
	}

	public void setToEmailAddress(String toEmailAddress) {
		this.toEmailAddress = toEmailAddress;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

}
