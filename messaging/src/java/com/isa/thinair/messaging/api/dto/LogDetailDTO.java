/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 06, 2005 15:28:39
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.messaging.api.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Locale;

/**
 * @author Thejaka
 */
public class LogDetailDTO implements Serializable {

	private static final long serialVersionUID = 6466071649065864438L;

	private String templateName;

	private HashMap templateParams;

	private String contentType = null;

	private Locale locale;

	private String saveFileName;

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	public String getSaveFileName() {
		return saveFileName;
	}

	public void setSaveFileName(String saveFileName) {
		this.saveFileName = saveFileName;
	}

	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public HashMap getTemplateParams() {
		return templateParams;
	}

	public void setTemplateParams(HashMap templateParams) {
		this.templateParams = templateParams;
	}

}
