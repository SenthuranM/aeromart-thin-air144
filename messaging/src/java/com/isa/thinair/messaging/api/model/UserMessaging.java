/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 06, 2005 15:28:39
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.messaging.api.model;

import java.io.Serializable;

public class UserMessaging implements Serializable {

	private static final long serialVersionUID = 5519420161395951103L;
	private String userID;
	private String firstName;
	private String lastName;
	private String toAddres;

	public UserMessaging (){}
	
	public UserMessaging (String address){
		this.toAddres = address;
	}
	
	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLasttName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getToAddres() {
		return toAddres;
	}

	public void setToAddres(String toAddres) {
		this.toAddres = toAddres;
	}

}
