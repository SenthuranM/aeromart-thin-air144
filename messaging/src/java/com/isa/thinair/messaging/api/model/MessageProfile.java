/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 06, 2005 15:28:39
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.messaging.api.model;

import java.io.Serializable;
import java.util.List;

/**
 * @author Nasly
 * 
 */
public class MessageProfile implements Serializable {

	private static final long serialVersionUID = 3474930041225748079L;
	private List userMessagings;
	private Topic topic;

	public List getUserMessagings() {
		return userMessagings;
	}

	public void setUserMessagings(List userMessagings) {
		this.userMessagings = userMessagings;
	}

	public void setTopic(Topic topic) {
		this.topic = topic;
	}

	public Topic getTopic() {
		return topic;
	}
}
