package com.isa.thinair.messaging.api.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.airmaster.api.utils.AirmasterConstants;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.utils.AirreservationConstants;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.airschedules.api.utils.AirschedulesConstants;
import com.isa.thinair.airsecurity.api.service.SecurityBD;
import com.isa.thinair.airsecurity.api.utils.AirsecurityConstants;
import com.isa.thinair.alerting.api.service.AlertingBD;
import com.isa.thinair.alerting.api.utils.AlertingConstants;
import com.isa.thinair.auditor.api.service.AuditorBD;
import com.isa.thinair.auditor.api.utils.AuditorConstants;
import com.isa.thinair.commons.core.util.GenericLookupUtils;
import com.isa.thinair.login.util.ForceLoginInvoker;
import com.isa.thinair.messaging.api.utils.MessagingConstants;
import com.isa.thinair.messaging.core.config.MessagingModuleConfig;
import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.IServiceDelegate;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.core.bean.UnifiedBeanFactory;
import com.isa.thinair.scheduler.api.service.SchedulerBD;
import com.isa.thinair.scheduler.api.utils.SchedulerConstants;
import com.isa.thinair.wsclient.api.service.WSClientBD;
import com.isa.thinair.wsclient.api.utils.WsclientConstants;

public class MessagingModuleUtils {

	/** Hold the logger instance */
	private static final Log log = LogFactory.getLog(MessagingModuleUtils.class);

	public static IModule getInstance() {
		return LookupServiceFactory.getInstance().getModule(MessagingConstants.MODULE_NAME);
	}

	public static MessagingModuleConfig getMessagingModuleConfig() {
		return (MessagingModuleConfig) getInstance().getModuleConfig();
	}

	/**
	 * Returns transparent service delegate
	 * 
	 * @param targetModuleName
	 * @param BDKeyWithoutLocality
	 * @return
	 */
	private static IServiceDelegate lookupServiceBD(String targetModuleName, String BDKeyWithoutLocality) {
		return GenericLookupUtils.lookupEJB2ServiceBD(targetModuleName, BDKeyWithoutLocality, getMessagingModuleConfig(),
				"messaging", "module.dependencymap.invalid");
	}

	private static Object lookupEJB3Service(String targetModuleName, String serviceName) {
		return GenericLookupUtils.lookupEJB3Service(targetModuleName, serviceName, getMessagingModuleConfig(), "messaging",
				"module.dependencymap.invalid");
	}

	/**
	 * Return transparent auditor delegate
	 * 
	 * @return
	 */
	public static AuditorBD getAuditorBD() {
		return (AuditorBD) MessagingModuleUtils.lookupServiceBD(AuditorConstants.MODULE_NAME,
				AuditorConstants.BDKeys.AUDITOR_SERVICE);
	}

	/**
	 * Return alerting delegate
	 * 
	 * @return
	 */
	public static AlertingBD getAlertingBD() {
		return (AlertingBD) MessagingModuleUtils.lookupServiceBD(AlertingConstants.MODULE_NAME,
				AlertingConstants.BDKeys.ALERTING_SERVICE);
	}

	public static FlightBD getFlightBD() {
		return (FlightBD) lookupEJB3Service(AirschedulesConstants.MODULE_NAME, FlightBD.SERVICE_NAME);
	}

	public static ReservationBD getReservationBD() {
		return (ReservationBD) lookupEJB3Service(AirreservationConstants.MODULE_NAME, ReservationBD.SERVICE_NAME);
	}

	public static SchedulerBD getSchedulerBD() {
		return (SchedulerBD) lookupEJB3Service(SchedulerConstants.MODULE_NAME, SchedulerBD.SERVICE_NAME);

	}
	
	public static SecurityBD getSecurityBD() {
		return (SecurityBD) lookupEJB3Service(AirsecurityConstants.MODULE_NAME, SecurityBD.SERVICE_NAME);
	}

	/**
	 * Gets a Common Master Service from Air Master
	 * 
	 * @return CommonMasterBD the common master delegate
	 */
	public final static CommonMasterBD getCommonServiceBD() {
		return (CommonMasterBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, CommonMasterBD.SERVICE_NAME);
	}

	public static WSClientBD getWSClientBD() {
		if (getMessagingModuleConfig().isInvokeCredentials()) {
			ForceLoginInvoker.login(getMessagingModuleConfig().getEjbAccessUsername(), getMessagingModuleConfig()
					.getEjbAccessPassword());
		}
		return (WSClientBD) lookupEJB3Service(WsclientConstants.MODULE_NAME, WSClientBD.SERVICE_NAME);
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T getBeanByName(String beanName) {
		return (T)LookupServiceFactory.getInstance().getBean(
				UnifiedBeanFactory.URI_SCHEME_PLUS_SLASHES + "modules/messaging?id=" + beanName);
	}

}
