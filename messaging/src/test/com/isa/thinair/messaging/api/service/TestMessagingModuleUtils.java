package com.isa.thinair.messaging.api.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;
import com.isa.thinair.messaging.core.processor.ResInfo;
import com.isa.thinair.platform.api.IServiceDelegate;
import com.isa.thinair.platform.api.util.PlatformTestCase;
import com.isa.thinair.samplemodule.api.model.SampleOjbect;
import com.isa.thinair.samplemodule.api.service.SampleServiceBD;

public class TestMessagingModuleUtils extends PlatformTestCase{
	protected void setUp() throws Exception{
		super.setUp();
	}
	
	public void testGetBD() throws ModuleException {	
//		MessagingServiceBD bd = (MessagingServiceBD)MessagingModuleUtils.getInstance()
//		.getBD("messaging","messaging.bdimpl");
//		assertNotNull(bd);
//		bd.sendMessages(getMessageProfiles());
		SampleServiceBD bd = (SampleServiceBD)MessagingModuleUtils.getInstance().lookupServiceBD("samplemodule","sample.service");
		SampleOjbect sampleObject = new SampleOjbect();
		sampleObject.setId("212121");
		sampleObject.setMessage("testtestest");
		bd.saveOrUpdate(sampleObject);
		bd.delete(sampleObject.getId());
	}
	
	private List getMessageProfiles(){
		MessageProfile profile = new MessageProfile();
		UserMessaging user=new UserMessaging();
		user.setFirstName("Sumudu");
		user.setLastName("Wickramaratne");
		user.setToAddres("nasly@jkcsworld.com");
		List messageList= new ArrayList();
		messageList.add(user);
		profile.setUserMessagings(messageList);
		
		Topic topic= new Topic();
		HashMap map = new HashMap();
		
		map.put("User", user);
		
		ResInfo resInfo = new ResInfo();
		resInfo.setDepatureDate(new Date());
		resInfo.setPNR("SS12345");
		map.put("ResInfo", resInfo);
		
		topic.setTopicParams(map);
		topic.setTopicName("sample_topic");
		profile.setTopic(topic);
		
		List list= new ArrayList();
		list.add(profile);
		return list;
	}
}
