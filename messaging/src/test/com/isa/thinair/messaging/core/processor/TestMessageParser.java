/*
 * Created on 28-Jun-2005
 *
 */
package com.isa.thinair.messaging.core.processor;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Date;
import java.util.HashMap;

import junit.framework.TestCase;

import com.isa.thinair.commons.core.util.TemplateEngine;
import com.isa.thinair.messaging.api.model.EmailMessage;
import com.isa.thinair.messaging.api.model.Message;
import com.isa.thinair.messaging.core.processor.MessageParser;

/**
 * @author Nasly
 *
 */
public class TestMessageParser extends TestCase{
	public void testTemplating() throws Exception{
		TemplateEngine templateEngine = new TemplateEngine();
		HashMap map = new HashMap();
		User user = new User();
		user.setFirstName("Nasly");
		map.put("User", user);
		ResInfo resInfo = new ResInfo();
		resInfo.setDepatureDate(new Date());
		resInfo.setPNR("SS12345");
		map.put("ResInfo", resInfo);
		StringWriter sw = new StringWriter(5000);
		Writer w = new BufferedWriter(sw);
		templateEngine.writeTemplate(map, "simple_message.xml.vm",w);
		ByteArrayInputStream bis = new ByteArrayInputStream(sw.toString().getBytes("ISO-8859-1"));
		MessageParser parser = new MessageParser();
		Message msg = new EmailMessage(); 
		parser.parse(bis, msg);
		assertNotNull(msg.getBody());
	}
	
	public void testParse() throws FileNotFoundException{
		File f= new File("c:/aaconfig/templates/simple_message.xml.vm");
		FileInputStream fileIn = new FileInputStream(f);
		MessageParser parser = new MessageParser();
		Message msg = new EmailMessage();
		parser.parse(fileIn, msg);
		assertNotNull(msg.getBody());
		assertNotNull(msg.getSubject());
	}
}

