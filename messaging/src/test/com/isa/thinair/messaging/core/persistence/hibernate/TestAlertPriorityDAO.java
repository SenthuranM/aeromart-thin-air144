/*
 * Created on Jul 6, 2005
 *
 *
 */
package com.isa.thinair.messaging.core.persistence.hibernate;

import java.util.List;

import junit.framework.TestCase;


import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.core.controller.ModuleFramework;
import com.isa.thinair.messaging.core.persistence.dao.AlertPriorityDAO;
import com.isa.thinair.messaging.core.persistence.hibernate.*;
import com.isa.thinair.messaging.api.model.*;

/**
 * @author sumudu
 *
 *
 */
public class TestAlertPriorityDAO extends TestCase{
	
	AlertPriorityDAO alertPriorityDao;
	
	public TestAlertPriorityDAO() {
		super();
	}
	protected void setUp() throws Exception {
		ModuleFramework.startup();
		LookupService lookup = LookupServiceFactory.getInstance();
		alertPriorityDao = (AlertPriorityDAO) lookup.getBean("isa:base://modules/messaging?id=alertPriorityDAOProxy");
		
	}
	
	protected void tearDown() throws Exception {
		//alertPriorityDao = null;
		
	}
	public static void main(String[] args) {
		junit.textui.TestRunner.run(TestAlertPriorityDAO.class);
	}
	
	public List testGetAlertTypes(){
		return alertPriorityDao.getAlertPriorites();
	}
	public AlertPriority testGetAlertType(){
		int alertTypeID=2;
		return alertPriorityDao.getAlertPriority(alertTypeID);
	}
	
	public void testSaveAlertPriority(){
		assertNotNull(alertPriorityDao);
		AlertPriority alertPriority = new AlertPriority();
		alertPriority.setDescription("High Priority");
		alertPriority.setPriorityCode(new Long(3));
		alertPriority.setPriorityLevel(new Integer(2));
		alertPriorityDao.saveAlertPriority(alertPriority);
		//assertTrue("primary key assigned", alertPriority.!= null);
	}
	public void testRemoveAlertPriority(){
		int priorityCode=2;
		alertPriorityDao.removeAlertPriority(priorityCode);
		}
		
	
	
	}

			
