/*
 * Created on Jul 6, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.isa.thinair.messaging.core.persistence.hibernate;

import java.util.List;

import junit.framework.TestCase;


import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.core.controller.ModuleFramework;
import com.isa.thinair.messaging.core.persistence.dao.AlertDAO;
import com.isa.thinair.messaging.core.persistence.dao.AlertTypeDAO;
import com.isa.thinair.messaging.core.persistence.hibernate.*;
import com.isa.thinair.messaging.api.model.*;

/**
 * @author sumudu
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class TestAlertDAO extends TestCase {
	Alert alert;
	AlertDAO alertDao;
	protected void setUp() throws Exception {
		ModuleFramework.startup();
		LookupService lookup = LookupServiceFactory.getInstance();
		alertDao = (AlertDAO) lookup.getBean("isa:base://modules/messaging?id=alertDAOProxy");
	;
		
	}
	
	protected void tearDown() throws Exception {
		alertDao = null;
		
	}
	public static void main(String[] args) {
		junit.textui.TestRunner.run(AlertTypeDAO.class);
	}
	
	public List testGetAlerts(){
		return alertDao.getAlerts();
	}
	public Alert testGetAlert(){
		String alertTypeID="";
		return alertDao.getAlert(alertTypeID);
	}
	
	public void testSaveAlert(){
		alert = new Alert();
		alert.setContent("hi");
		alert.setPriorityCode(new Long(1));
		alert.setAlertTypeId(new Long(1));
		//alert.setPNRs("Level 1");
		alert.setStatus("Attended");
		alertDao.saveAlert(alert);
	}
	/*public void testRemoveAlert(){
		alertDao.removeAlert("aaa","aaa");
		}*/
}
