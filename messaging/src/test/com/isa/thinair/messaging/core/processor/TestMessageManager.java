/*
 * Created on Jul 4, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.isa.thinair.messaging.core.processor;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import junit.framework.TestCase;

import com.isa.thinair.airreservation.api.dto.PNLADLLogDTO;
import com.isa.thinair.messaging.api.dto.LogDetailDTO;
import  com.isa.thinair.messaging.api.model.*;
import  com.isa.thinair.messaging.core.transport.EmailBroadcaster;
import com.isa.thinair.platform.api.constants.PlatformConstants;
import com.isa.thinair.platform.api.util.PlatformTestCase;

import com.isa.thinair.scheduledservices.api.util.ScheduleServicesConstants;


/**
 * @author sumudu
 *
 */
public class TestMessageManager extends PlatformTestCase {
	
	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public static void main(String[] args) {
		junit.textui.TestRunner.run(TestMessageManager.class);
	}
	
	public void TestProcessMessages() throws Exception {
		MessageProfile profile= new MessageProfile();
		
		UserMessaging user=new UserMessaging();
		user.setFirstName("Sumudu");
		user.setLastName("Wickramaratne");
		user.setToAddres("nasly@jkcsworld.com");
		List messageList= new ArrayList();
		messageList.add(user);
		profile.setUserMessagings(messageList);
		
		Topic topic= new Topic();
		HashMap map = new HashMap();
		
		map.put("User", user);
		
		ResInfo resInfo = new ResInfo();
		resInfo.setDepatureDate(new Date());
		resInfo.setPNR("SS12345");
		map.put("ResInfo", resInfo);
		
		topic.setTopicParams(map);
		topic.setTopicName("sample_topic");
		profile.setTopic(topic);
		
		List list= new ArrayList();
		list.add(profile);
		
		MessageManager manager = new MessageManager();
		manager.processMessages(list);	
	}
	
}
