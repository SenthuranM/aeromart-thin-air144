/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 06, 2005 15:28:39
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.messaging.core.persistence.hibernate;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Collection;

import com.isa.thinair.messaging.api.service.MessagingModuleUtils;
import com.isa.thinair.messaging.api.model.Alert;
import com.isa.thinair.messaging.core.persistence.dao.MessagingDAO;
import com.isa.thinair.messaging.core.util.MessagingInternalConstants;
import com.isa.thinair.platform.api.util.PlatformTestCase;

public class TestMessagingDAO extends PlatformTestCase {
	
	protected void setUp() throws Exception {
		super.setUp();	
	}
	
	protected void tearDown() throws Exception {
	}
	
	public void testRetrieveAlerts() {
		List alertIds = new ArrayList();
        alertIds.add(new Integer(1));
        alertIds.add(new Integer(15));
        alertIds.add(new Integer(25));
		List alerts = getMessagingDAO().retrieveAlerts(alertIds);
		assertNotNull(alerts);
	}
    
    public void testRetrieveAlertsForPrivileges() {
        List privileges = new ArrayList();
        privileges.add("ALERT.PNR.ACT");
        List alerts = getMessagingDAO().retrieveAlertsForPrivileges(privileges);
        assertNotNull(alerts);
    }
    
    public void testRetrieveAlertsForPnrSegments() {
        List pnrSegIds = new ArrayList();
        pnrSegIds.add(new Integer(9630));
        pnrSegIds.add(new Integer(9643));
        pnrSegIds.add(new Integer(9638));
        List alerts = getMessagingDAO().retrieveAlertsForPnrSegments(pnrSegIds);
        assertNotNull(alerts);
    }
    
	public void testAddAlert() {
		Alert alert = new Alert();
		alert.setContent("Test by Messaging module");
		alert.setPriorityCode(new Long(1));
		alert.setStatus("open");
		alert.setAlertTypeId(new Long(1));
		alert.setTimestamp(new Date(System.currentTimeMillis()));
        alert.setPnrSegId(new Long(1));
		getMessagingDAO().addAlert(alert);
	}
	
	public void testUpdateAlertStatus() {
		getMessagingDAO().updateAlertStatus(new Long(1), "testing");
	}
	
	public void testUpdateAlert() {
		Alert alert = new Alert();
        alert.setAlertId(new Long(1));
		alert.setAlertTypeId(new Long(1));
		alert.setContent("Updated Content");
		alert.setPriorityCode(new Long(1));
		alert.setStatus("open");
        alert.setPnrSegId(new Long(1));     
        alert.setTimestamp(new Date(System.currentTimeMillis()));        
        alert.setVersion(Long.parseLong("2"));        
		getMessagingDAO().updateAlert(alert);
	}
    
    public void testRemoveAlerts() {
        List alertIds = new ArrayList();
        alertIds.add(new Integer(40));
        alertIds.add(new Integer(17));
        getMessagingDAO().removeAlerts(alertIds);
    } 

    public void testRemoveAlertsOfPnrSegments() {
        List pnrSegIds = new ArrayList();
        pnrSegIds.add(new Integer(9644));
        pnrSegIds.add(new Integer(9623));
        getMessagingDAO().removeAlertsOfPnrSegments(pnrSegIds);
    } 
    
	private MessagingDAO getMessagingDAO() {
        return (MessagingDAO) MessagingModuleUtils.getInstance().getLocalBean(
                MessagingInternalConstants.DAOProxyNames.MESSAGING_DAO);        
	}
}
