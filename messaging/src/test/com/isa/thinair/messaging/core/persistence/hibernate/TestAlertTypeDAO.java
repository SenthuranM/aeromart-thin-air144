/*
 * Created on Jul 6, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.isa.thinair.messaging.core.persistence.hibernate;

import java.util.List;

import junit.framework.TestCase;


import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.core.controller.ModuleFramework;
import com.isa.thinair.messaging.core.persistence.dao.AlertTypeDAO;
import com.isa.thinair.messaging.core.persistence.hibernate.*;
import com.isa.thinair.messaging.api.model.*;


/**
 * @author sumudu
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class TestAlertTypeDAO extends TestCase {
	AlertType alertType;
	AlertTypeDAO alertTypeDao;
	protected void setUp() throws Exception {
		ModuleFramework.startup();
		LookupService lookup = LookupServiceFactory.getInstance();
		alertTypeDao = (AlertTypeDAO) lookup.getBean("isa:base://modules/messaging?id=alertTypeDAOProxy");
	
		
	}
	
	protected void tearDown() throws Exception {
		alertTypeDao = null;
		
	}
	public static void main(String[] args) {
		junit.textui.TestRunner.run(AlertTypeDAO.class);
	}
	
	public List testGetAlertTypes(){
		return alertTypeDao.getAlertTypes();
	}
	public AlertType testGetAlertType(){
		int alertTypeID=1;
		return alertTypeDao.getAlertType(alertTypeID);
	}
	
	public void testSaveAlertType(){
		alertType = new AlertType();
		alertType.setAlertTypeId(new Long(3));
		alertType.setDescription("Reservation Cancel");
		alertType.setPrivilegeId(new Long(12345));
		//alertType.setPrevilegeId(1);
		alertTypeDao.saveAlertType(alertType);
	}
	public void testRemoveAlertType(){
		int alertTypeID=2;
		alertTypeDao.removeAlertType(alertTypeID);
		}
}
