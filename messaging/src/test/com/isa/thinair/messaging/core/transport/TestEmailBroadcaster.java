/*
 * Created on Jun 29, 2005
 *
 */
package com.isa.thinair.messaging.core.transport;

import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import com.isa.thinair.messaging.api.model.EmailMessage;
import com.isa.thinair.platform.api.util.PlatformTestCase;

/**
 * @author Nasly
 *
 */
public class TestEmailBroadcaster extends PlatformTestCase {
	
	protected void setUp() throws Exception{
		super.setUp();
	}
	
	public void testSendMessages() throws AddressException, MessagingException{
		EmailMessage msg = new EmailMessage();
		msg.setFromAddress("sumudupw@jkcsworld.com");
		msg.setToAddress("sumudupw@jkcsworld.com");
		msg.setSubject("Test mail");
		msg.setBody("<HTML><BODY><B>This is a test</B></BODY></HTML>");
		EmailBroadcaster emailBroadcaster = new EmailBroadcaster();
		List messagesList = new ArrayList();
		messagesList.add(msg);
		emailBroadcaster.sendMessages(messagesList);
	}
}
