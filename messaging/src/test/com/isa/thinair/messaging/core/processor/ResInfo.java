/*
 * Created on 28-Jun-2005
 *
 */
package com.isa.thinair.messaging.core.processor;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Nasly
 *
 */
public class ResInfo implements Serializable {
	private Date depatureDate;
	private String PNR;
	
	
	public Date getDepatureDate() {
		return depatureDate;
	}
	public void setDepatureDate(Date depatureDate) {
		this.depatureDate = depatureDate;
	}
	public String getPNR() {
		return PNR;
	}
	public void setPNR(String pnr) {
		PNR = pnr;
	}
}
