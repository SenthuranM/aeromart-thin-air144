/*
 * Created on Jul 4, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.isa.thinair.messaging.core.remoting.ejb;

import java.util.List;

import com.isa.thinair.login.util.ForceLoginInvoker;
import com.isa.thinair.messaging.api.service.MessagingModuleUtils;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;
import com.isa.thinair.messaging.api.utils.MessagingConstants;
import com.isa.thinair.platform.api.util.PlatformTestCase;

/**
 * @author sumudu
 *
 */
public class TestMessageServiceRemote extends PlatformTestCase {
	protected void setUp() throws Exception {
		super.setUp();
	}
    
    public void testSendMessages() throws Exception {
        System.out.println("Calling LoginModule...");
        //ForceLoginInvoker.login("SYSTEM", "password");
        
        System.out.println("exec testcase");
        MessagingServiceBD messagingBD = getMessagingService();
        System.out.println("BD created");
        List messageList =  messagingBD.loadEmails();
        System.out.println("mail loaded");
        System.out.println("Sending mails");
        messagingBD.sendMessages(messageList);
        System.out.println("Finished");
    }
	
    /*public void testSendMessage(){
        System.out.println("=============================");
        System.out.println("Running the MessagingMDB test...");
        MessageProfile profile= new MessageProfile();
        
        UserMessaging user=new UserMessaging();
        user.setFirstName("Srikanth");
        user.setLastName("Kanagaratnam");
        user.setToAddres("srikanth@jkcsworld.com");
        List messageList= new ArrayList();
        messageList.add(user);
        profile.setUserMessagings(messageList);

        Topic topic= new Topic();
        HashMap map = new HashMap();
        map.put("user", user);
        
        topic.setTopicParams(map);
        topic.setTopicName("dst_change");
        topic.setContentType(EmailMessage.CONTENT_TYPE_HTML);//current impl only support html
        topic.setMailServerConfigurationName("mailserver_sita.1");
        profile.setTopic(topic);
        
        ArrayList list= new ArrayList();
        list.add(profile);
              
        
        System.out.println("Calling the MessagingMDB service...");
        getMessagingService().sendMessages(list);  
        System.out.println("=============================");
        
    } */   
 /*   
	public void testSendMessages(){
		MessageProfile profile= new MessageProfile();
		
		UserMessaging user=new UserMessaging();
		user.setFirstName("Sumudu");
		user.setLastName("Wickramaratne");
		user.setToAddres("nasly@jkcsworld.com");
		List messageList= new ArrayList();
		messageList.add(user);
		profile.setUserMessagings(messageList);
		
		Topic topic= new Topic();
		HashMap map = new HashMap();
		
		map.put("User", user);
		
		ResInfo resInfo = new ResInfo();
		resInfo.setDepatureDate(new Date());
		resInfo.setPNR("SS12345");
		map.put("ResInfo", resInfo);
		
		topic.setTopicParams(map);
		topic.setTopicName("sample_topic");
		profile.setTopic(topic);
		
		List list= new ArrayList();
		list.add(profile);
		getMessagingService().sendMessages(list);
	}
    
    public void testRetrieveAlerts() throws Exception {
        List alertIds = new ArrayList();
        alertIds.add(new Integer("1"));
        alertIds.add(new Integer("15"));
        alertIds.add(new Integer("25"));
        List alerts = getMessagingService().retrieveAlerts(alertIds);
        assertNotNull(alerts);
    }
    
    public void testRetrieveAlertsForPrivileges() throws Exception {
        List privileges = new ArrayList();
        privileges.add("ALERT.PNR.ACT");
        List alerts = getMessagingService().retrieveAlertsForPrivileges(privileges);
        assertNotNull(alerts);
        assertEquals(3, alerts.size());         
    }
    
    public void testRetrieveAlertsForPnrSegments() throws Exception {
        List pnrSegIds = new ArrayList();
        pnrSegIds.add(new Integer(9630));
        pnrSegIds.add(new Integer(9643));
        pnrSegIds.add(new Integer(9638));
        List alerts = getMessagingService().retrieveAlertsForPnrSegments(pnrSegIds);
        assertNotNull(alerts);
    }
    
    public void testRetrieveAlertsForUser() throws Exception {
        List list = getMessagingService().retrieveAlertsForUser("Janaki");
        assertNotNull(list);
        assertEquals(2, list.size());
    }    
    
    public void testAddAlert() throws Exception {
        Alert alert = new Alert();
        alert.setContent("Test by Messaging module rem");
        alert.setPriorityCode(new Long(1));
        alert.setStatus("open");
        alert.setAlertTypeId(new Long(1));
        alert.setTimestamp(new Date(System.currentTimeMillis()));
        alert.setPnrSegId(new Long(1));
        getMessagingService().addAlert(alert);
    }
    
    public void testUpdateAlert() throws Exception {
        Alert alert = new Alert();
        alert.setAlertId(new Long(10));
        alert.setAlertTypeId(new Long(1));
        alert.setContent("Updated Content rem");
        alert.setPriorityCode(new Long(1));
        alert.setStatus("open");
        alert.setPnrSegId(new Long(1));     
        alert.setTimestamp(new Date(System.currentTimeMillis()));        
        alert.setVersion(Long.parseLong("1"));        
        getMessagingService().updateAlert(alert);
    }
    
    public void testRetrieveAlertsForSearchCriteria() throws Exception {
        AlertDetailDTO criteria = new AlertDetailDTO();
        //GregorianCalendar fromDateG = new GregorianCalendar(2005, 10, 28, 0, 0);
        //GregorianCalendar toDateG = new GregorianCalendar(2005, 11, 25, 0, 0);
        //GregorianCalendar depDateG = new GregorianCalendar(2006, 00, 16, 0, 0);
        
        Date fromDateG = CalanderUtil.getParsedTime("01/11/2005", "dd/MM/yyyy");
        Date toDateG = CalanderUtil.getParsedTime("30/11/2005", "dd/MM/yyyy");
        Date depDateG = CalanderUtil.getParsedTime("16/01/2006", "dd/MM/yyyy");
        
        criteria.setFromDate(fromDateG);
        criteria.setToDate(toDateG);
        criteria.setDepDate(depDateG);
        Collection col = getMessagingService().retrieveAlertsForSearchCriteria(criteria);     
        assertNotNull(col);    
        assertEquals(4, col.size());        
    }  
    
    public void testRemoveAlerts() throws Exception {
        List lst = new ArrayList();
        lst.add(new Integer("10"));
        lst.add(new Integer("42"));
        getMessagingService().removeAlerts(lst);
    } 
    
    public void testRemoveAlertsOfPnrSegments() throws Exception {
        List pnrSegIds = new ArrayList();
        pnrSegIds.add(new Integer(9643));
        pnrSegIds.add(new Integer(9632));
        getMessagingService().removeAlertsOfPnrSegments(pnrSegIds);
    }    
 */   
	private MessagingServiceBD getMessagingService(){
		return (MessagingServiceBD) MessagingModuleUtils.getInstance().lookupServiceBDSpecifyingFullBDKey(MessagingConstants.MODULE_NAME, MessagingConstants.FullBDKeys.MESSAGING_SERVICE_REMOTE);
                
	}
}
