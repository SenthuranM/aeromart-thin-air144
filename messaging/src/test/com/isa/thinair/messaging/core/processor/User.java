/*
 * Created on 28-Jun-2005
 *
 */
package com.isa.thinair.messaging.core.processor;

/**
 * @author Nasly
 *
 */
public class User {
	private String firstName;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
}
