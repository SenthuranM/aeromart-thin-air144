/*
 * ==============================================================================
 * ISA Software License, Version Ver 1.0.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * Created on Jul 06, 2005 15:28:39
 * 
 * $Id$
 * 
 * ===============================================================================
 */
package com.isa.thinair.messaging.core.persistence.jdbc;

import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Collection;
import java.util.GregorianCalendar;

import com.isa.thinair.commons.core.util.CalanderUtil;
import com.isa.thinair.messaging.api.dto.AlertDetailDTO;
import com.isa.thinair.messaging.api.service.MessagingModuleUtils;
import com.isa.thinair.messaging.core.persistence.dao.MessagingJdbcDAO;
import com.isa.thinair.messaging.core.util.MessagingInternalConstants;
import com.isa.thinair.platform.api.util.PlatformTestCase;

public class TestMessagingJdbcDAO extends PlatformTestCase {
	
	protected void setUp() throws Exception {
		super.setUp();	
	}
	
	protected void tearDown() throws Exception {
	}
	
    public void testRetrieveAlertsForUser() {
        List alerts = getMessagingJdbcDAO().retrieveAlertsForUser("USR001");
        assertNotNull(alerts);
        assertEquals(4, alerts.size());
    }   
	
    public void testRetrieveAlertsForSearchCriteria() throws ParseException {
        AlertDetailDTO criteria = new AlertDetailDTO();
        
        /*GregorianCalendar fromDateG = new GregorianCalendar(2005, 10, 29, 0, 0);
        GregorianCalendar toDateG = new GregorianCalendar(2005, 10, 30, 0, 0);
        GregorianCalendar depDateG = new GregorianCalendar(2006, 00, 16, 0, 0);*/
        
        Date fromDateG = CalanderUtil.getParsedTime("01/11/2005", "dd/MM/yyyy");
        Date toDateG = CalanderUtil.getParsedTime("30/11/2005", "dd/MM/yyyy");
        Date depDateG = CalanderUtil.getParsedTime("16/01/2006", "dd/MM/yyyy");
        
        //criteria.setFromDate(fromDateG);
        //criteria.setToDate(toDateG);
        criteria.setDepDate(depDateG);
        //criteria.setTotalRecs(3);
//        Collection col = getMessagingJdbcDAO().retrieveAlertsForSearchCriteria(criteria);
//        assertNotNull(col);
//        assertEquals(2, col.size());        
    }
    
	private MessagingJdbcDAO getMessagingJdbcDAO(){
		return (MessagingJdbcDAO) MessagingModuleUtils.getInstance().getLocalBean(
                MessagingInternalConstants.DAOProxyNames.MESSAGING_JDBC_DAO);
	}

}
