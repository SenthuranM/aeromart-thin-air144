package com.isa.thinair.invoicing.api.model.bsp.records;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicRecord;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

/**
 * @author primal
 * @date Mar 12, 2014
 */
public class X92_Coupon_Status_History_Record extends BasicRecord {

	public X92_Coupon_Status_History_Record(DISH_FILE_TYPE fileType) throws BSPException {
		super(fileType, DISH_RECORD_TYPE.X92);
	}

	public void setTICKET_NUMBER(String ticketNumber) throws BSPException {
		dataContainer.setValue(1, ticketNumber);
	}

	public void setRTA_TIMESTAMP(Date ticketChangeTimeStamp) throws BSPException {
		dataContainer.setValue(2, new SimpleDateFormat("yyyyMMdd").format(ticketChangeTimeStamp));
	}

	public void setREC_TYPE(String recordType) throws BSPException {
		dataContainer.setValue(3, recordType);
	}

	public void setSEQUENCE_INDIC(BigDecimal sequenceIndicator) throws BSPException {
		dataContainer.setValue(4, sequenceIndicator, 0);
	}

	public void setCSH(BigDecimal ticketCouponNumber) throws BSPException {
		dataContainer.setValue(5, ticketCouponNumber, 0);
	}

	public void setTPS(String HistoricalCouponStatus) throws BSPException {
		dataContainer.setValue(6, HistoricalCouponStatus);
	}

	public void setDATE_TIME(Date changeDateTime) throws BSPException {
		dataContainer.setValue(7, new SimpleDateFormat("yyyyMMddHHmmss").format(changeDateTime));
	}

	public void setTPC(String historicalPsudoCity) throws BSPException {
		dataContainer.setValue(8, historicalPsudoCity);
	}

	public void setTHL(String agentIdentifier) throws BSPException {
		dataContainer.setValue(9, agentIdentifier);
	}

	public void setTHA(String transActionCode) throws BSPException {
		dataContainer.setValue(10, transActionCode);
	}

	public void setTAG(String historicalAgentID) throws BSPException {
		dataContainer.setValue(11, historicalAgentID);
	}

	public void setTAN(String historicalAgencyNumber) throws BSPException {
		dataContainer.setValue(12, historicalAgencyNumber);
	}

	public void setTPT(String partition) throws BSPException {
		dataContainer.setValue(13, partition);
	}

	public void setSSQ(String secondarySequenceNumber) throws BSPException {
		dataContainer.setValue(14, secondarySequenceNumber);
	}

	public void setTTH(String changeIndicator) throws BSPException {
		dataContainer.setValue(15, changeIndicator);
	}

	public void setAP4(String transActionCode) throws BSPException {
		dataContainer.setValue(16, transActionCode);
	}

	public void setOPN(String oldPNRNumber) throws BSPException {
		dataContainer.setValue(17, oldPNRNumber);
	}

	public void setTCJ(String exchangeTicketNumber) throws BSPException {
		dataContainer.setValue(18, exchangeTicketNumber);
	}

	public void setCNJ(String numberOfExchangeTickets) throws BSPException {
		dataContainer.setValue(19, numberOfExchangeTickets);
	}

	public void setCHG_STATUS(String changeTodayStatus) throws BSPException {
		dataContainer.setValue(20, changeTodayStatus);
	}

}
