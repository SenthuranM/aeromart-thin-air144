package com.isa.thinair.invoicing.api.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * BSPFailedTnxDTO is the entity class to represent BSP Dish Data
 * 
 * @hibernate.class table = "T_BSP_FILE_DATA_LOG"
 */
public class DishFileTnxLogData extends Persistent implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer logID;
	
	private Long primaryETicket;
	
	private Long secondaryEticket;
	
	private String paxName;
	
	private String pnr;
	
	private Date dateOfTnx;
	
	private String tnxType;
	
	private Integer tnxID;
	
	private String lccUniqueID;
	
	private Integer paxSequence;
	
	private BSPFileLog bspFileLog;
		
	private Integer HOTFileId;
	
	private String createdBy;
	
	private Date createdDate;
	
	private String modifiedby;
	
	private Date modifiedDate;
	
	private String reconciled;
	
	private BigDecimal processedAmount;
	
	private String processedCurrencyCode;
	
	private String emds;

	private String externalReference;
	
	private String productType = "P";

	/**
	 * @return Returns the logID.
	 * 
	 * @hibernate.id column="BSP_FILE_DATA_LOG_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_BSP_FILE_DATA_LOG"
	 */
	public Integer getLogID() {
		return logID;
	}

	public void setLogID(Integer logID) {
		this.logID = logID;
	}

	/**
	 * @return Returns the primaryETicket.
	 * @hibernate.property column = "PRIMARY_E_TICKET"
	 */
	public Long getPrimaryETicket() {
		return primaryETicket;
	}

	/**
	 * @param primaryETicket
	 *            The primaryETicket to set.
	 */
	public void setPrimaryETicket(Long primaryETicket) {
		this.primaryETicket = primaryETicket;
	}

	/**
	 * @return Returns the secondaryEticket.
	 * @hibernate.property column = "SECONDARY_E_TICKET"
	 */
	public Long getSecondaryEticket() {
		return secondaryEticket;
	}

	/**
	 * @param secondaryEticket
	 *            The secondaryEticket to set.
	 */
	public void setSecondaryEticket(Long secondaryEticket) {
		this.secondaryEticket = secondaryEticket;
	}

	/**
	 * @return Returns the paxName.
	 * @hibernate.property column = "PAX_NAME"
	 */
	public String getPaxName() {
		return paxName;
	}

	/**
	 * @param paxName
	 *            The paxName to set.
	 */
	public void setPaxName(String paxName) {
		this.paxName = paxName;
	}

	/**
	 * @return Returns the pnr.
	 * @hibernate.property column = "PNR"
	 */
	public String getPnr() {
		return pnr;
	}

	/**
	 * @param pnr
	 *            The pnr to set.
	 */
	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return Returns the dateOfTnx.
	 * @hibernate.property column = "TNX_DATE"
	 */
	public Date getDateOfTnx() {
		return dateOfTnx;
	}

	/**
	 * @param dateOfTnx
	 *            The dateOfTnx to set.
	 */
	public void setDateOfTnx(Date dateOfTnx) {
		this.dateOfTnx = dateOfTnx;
	}

	/**
	 * @return Returns the tnxType.
	 * @hibernate.property column = "TNX_TYPE"
	 */
	public String getTnxType() {
		return tnxType;
	}

	/**
	 * @param tnxType
	 *            The tnxType to set.
	 */
	public void setTnxType(String tnxType) {
		this.tnxType = tnxType;
	}

	/**
	 * @return Returns the tnxID.
	 * @hibernate.property column = "TNX_ID"
	 */
	public Integer getTnxID() {
		return tnxID;
	}

	/**
	 * @param tnxID
	 *            The tnxID to set.
	 */
	public void setTnxID(Integer tnxID) {
		this.tnxID = tnxID;
	}

	/**
	 * @return Returns the lccUniqueID.
	 * @hibernate.property column = "LCC_UNIQUE_TXN_ID"
	 */
	public String getLccUniqueID() {
		return lccUniqueID;
	}

	/**
	 * @param lccUniqueID
	 *            The lccUniqueID to set.
	 */
	public void setLccUniqueID(String lccUniqueID) {
		this.lccUniqueID = lccUniqueID;
	}

	/**
	 * @return Returns the paxSequence.
	 * @hibernate.property column = "PAX_SEQUENCE"
	 */
	public Integer getPaxSequence() {
		return paxSequence;
	}

	/**
	 * @param paxSequence
	 *            The paxSequence to set.
	 */
	public void setPaxSequence(Integer paxSequence) {
		this.paxSequence = paxSequence;
	}

	/**
	 * @return Returns the createdBy.
	 * @hibernate.property column = "CREATED_BY"
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            The createdBy to set.
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return Returns the createdDate.
	 * @hibernate.property column = "CREATED_DATE"
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate
	 *            The createdDate to set.
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return Returns the modifiedby.
	 * @hibernate.property column = "MODIFIED_BY"
	 */
	public String getModifiedby() {
		return modifiedby;
	}

	/**
	 * @param modifiedby
	 *            The modifiedby to set.
	 */
	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}

	/**
	 * @return Returns the modifiedDate.
	 * @hibernate.property column = "MODIFIED_DATE"
	 */
	public Date getModifiedDate() {
		return modifiedDate;
	}

	/**
	 * @param modifiedDate
	 *            The modifiedDate to set.
	 */
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	
	

	/**
	 * @hibernate.many-to-one column="BSP_FILE_LOG_ID" class="com.isa.thinair.invoicing.api.model.BSPFileLog"
	 * @return Returns the bspFileLog.
	 */
	public BSPFileLog getBspFileLog() {
		return bspFileLog;
	}

	public void setBspFileLog(BSPFileLog bspFileDataLog) {
		this.bspFileLog = bspFileDataLog;
	}

	/**
	 * @hibernate.property column = "BSP_HOT_FILE_LOG_ID"
	 */
	public Integer getHOTFileId() {
		return HOTFileId;
	}

	public void setHOTFileId(Integer hOTFileId) {
		HOTFileId = hOTFileId;
	}

	/**
	 * @hibernate.property column = "RECONCILED"
	 */
	public String getReconciled() {
		return reconciled;
	}

	public void setReconciled(String reconciled) {
		this.reconciled = reconciled;
	}

	/**
	 * @hibernate.property column = "PROCESSED_AMOUNT"
	 */
	public BigDecimal getProcessedAmount() {
		return processedAmount;
	}

	public void setProcessedAmount(BigDecimal processedAmount) {
		this.processedAmount = processedAmount;
	}

	/**
	 * @hibernate.property column = "PROCESSED_CURRENCY"
	 */
	public String getProcessedCurrencyCode() {
		return processedCurrencyCode;
	}

	public void setProcessedCurrencyCode(String currencyCode) {
		this.processedCurrencyCode = currencyCode;
	}

	/**
	 * @hibernate.property column = "EMDS"
	 */
	public String getEmds() {
		return emds;
	}

	public void setEmds(String emds) {
		this.emds = emds;
	}

	/**
	 * @hibernate.property column = "EXT_REFERENCE"
	 */	
	public String getExternalReference() {
		return externalReference;
	}

	public void setExternalReference(String externalReference) {
		this.externalReference = externalReference;
	}

	/**
	 * @hibernate.property column = "PRODUCT_TYPE"
	 */
	public String getProductType() {
		return productType;
	}

	/**
	 * @param productType the productType to set
	 */
	public void setProductType(String productType) {
		this.productType = productType;
	}
	
	
	
	
}
