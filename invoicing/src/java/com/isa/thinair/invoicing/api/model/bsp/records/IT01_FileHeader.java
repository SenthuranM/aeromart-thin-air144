package com.isa.thinair.invoicing.api.model.bsp.records;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicRecord;
import com.isa.thinair.invoicing.api.model.bsp.EnumTypes.OPERATION_STATUS;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

public class IT01_FileHeader extends BasicRecord {

	public IT01_FileHeader(DISH_FILE_TYPE fileType) throws BSPException {
		super(fileType, DISH_RECORD_TYPE.IT01);
	}

	public void setSysProvRptPeriodEnddate(Date date) throws BSPException {
		dataContainer.setValue(2, date);
	}

	public void setReportingSystemId(String id) throws BSPException {
		dataContainer.setValue(3, id);
	}

	public void setHandbookRevNo(Long revNo) throws BSPException {
		dataContainer.setValue(4, revNo);
	}

	public void setTestProdStatus(OPERATION_STATUS status) throws BSPException {
		dataContainer.setValue(5, status.toString());
	}

	public void setProcessingDate(Date date) throws BSPException {
		dataContainer.setValue(6, date);
	}

	public void setProcessingTime(Date time) throws BSPException {
		dataContainer.setValue(7, new SimpleDateFormat("HHMM").format(time));
	}

	public void setCountryCode(String countryCode) throws BSPException {
		dataContainer.setValue(8, countryCode);
	}

	public void setFileType(String fileType) throws BSPException {
		dataContainer.setValue(10, fileType);
	}

	public void setFileSeqNo(String fileSeqNo) throws BSPException {
		dataContainer.setValue(11, fileSeqNo);
	}
}
