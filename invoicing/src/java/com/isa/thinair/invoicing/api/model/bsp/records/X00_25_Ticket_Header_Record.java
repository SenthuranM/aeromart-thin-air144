package com.isa.thinair.invoicing.api.model.bsp.records;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicRecord;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

/**
 * @author primal
 * @date Mar 7, 2014
 * 
 */
public class X00_25_Ticket_Header_Record extends BasicRecord {

	public X00_25_Ticket_Header_Record(DISH_FILE_TYPE fileType) throws BSPException {
		super(fileType, DISH_RECORD_TYPE.X00_25);

	}

	public void setPaxType(String paxTpye) throws BSPException {
		dataContainer.setValue(1, paxTpye);
	}

	public void setPaxSurName(String paxSurname) throws BSPException {
		dataContainer.setValue(2, paxSurname);
	}

	public void setPaxFirstName(String paxFirstName) throws BSPException {
		dataContainer.setValue(3, paxFirstName);
	}

}
