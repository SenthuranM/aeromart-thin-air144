package com.isa.thinair.invoicing.api.model.bsp;

import java.math.BigDecimal;

public class ETicketFOP {

	private String fopDescription;

	private String paymentMethod;

	private String ccVendor;

	private BigDecimal tnxAmount;

	public String getFopDescription() {
		return fopDescription;
	}

	public void setFopDescription(String fopDescription) {
		this.fopDescription = fopDescription;
		this.paymentMethod = FOPNominalCodes.valueOf(fopDescription).paymentMethod;
		this.ccVendor = FOPNominalCodes.valueOf(fopDescription).ccCode;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getCcVendor() {
		return ccVendor;
	}

	public void setCcVendor(String ccVendor) {
		this.ccVendor = ccVendor;
	}

	public BigDecimal getTnxAmount() {
		return tnxAmount;
	}

	public void setTnxAmount(BigDecimal tnxAmount) {
		this.tnxAmount = tnxAmount.negate();
	}

}

enum FOPNominalCodes {
	CARD_PAYMENT_VISA("CC", "VI"), CARD_PAYMENT_MASTER("CC", "CA"), CARD_PAYMENT_DINERS("CC", "DC"), CARD_PAYMENT_CMI("MS", ""), CARD_PAYMENT_PAYPAL(
			"MS", ""), CARD_PAYMENT_GENERIC("MA", ""), CASH_PAYMENT("CA", ""), ONACCOUNT_PAYMENT("MA", "");

	String paymentMethod;
	String ccCode;

	FOPNominalCodes(String method, String cccode) {
		paymentMethod = method;
		ccCode = cccode;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public String getCcCode() {
		return ccCode;
	}
}