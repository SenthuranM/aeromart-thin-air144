package com.isa.thinair.invoicing.api.model.bsp;

public class ReportFileTypes {

	public enum FileTypes {
		RET, HOT, ETLR, EXCEPTION
	};
}
