package com.isa.thinair.invoicing.api.model;

/**
 * BSPWeblinkAccount is the entity class to represent BSP Weblink SFTP Credentials
 * 
 * @hibernate.class table = "T_BSP_WEBLINK_SFTP_DETAILS"
 */
public class BSPWeblinkAccount {

	private Integer bspSftpId;

	private String countryCode;

	private String host;

	private String username;

	private String password;
	

	/**
	 * @return Returns the bspSftpId.
	 * 
	 * @hibernate.id column="BSP_SFTP_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_BSP_WEBLINK_SFTP_DETAILS"
	 */
	public Integer getBspSftpId() {
		return bspSftpId;
	}

	public void setBspSftpId(Integer bspSftpId) {
		this.bspSftpId = bspSftpId;
	}
	

	/**
	 * @return Returns the countryCode.
	 * @hibernate.property column = "COUNTRY_CODE"
	 */
	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	

	/**
	 * @return Returns the host.
	 * @hibernate.property column = "HOST"
	 */
	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}
	

	/**
	 * @return Returns the username.
	 * @hibernate.property column = "USER_NAME"
	 */
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	

	/**
	 * @return Returns the password.
	 * @hibernate.property column = "PASSWORD"
	 */
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
