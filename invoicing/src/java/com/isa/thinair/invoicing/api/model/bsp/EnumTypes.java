package com.isa.thinair.invoicing.api.model.bsp;

public class EnumTypes {
	public enum NetReportingIndicator {

		NR("NR"), NA("");

		private String str;

		NetReportingIndicator(String str) {
			this.str = str;
		}

		public String toString() {
			return this.str;
		}
	}

	public enum FormOfPaymentType {
		/** cash */
		CA,
		/** credit card - CCAX0000 */
		CC,
		/** credit to cash */
		CM,
		/** exchange */
		EX,
		/** misc - MSCC, MSCA */
		MS,
		/** void */
		VD;
	}

	public enum CouponUseIndicator {
		/** Boarding pass only */
		B,
		/** Value coupon issued */
		F,
		/** Non-value coupon between two valid coupons (interrupt of journey) */
		S,
		/**
		 * Void (unused) coupon (before beginning or after end of journey, on single or conjunction tickets), or a
		 * non-printed OPATB, MDnn , or unused MCOM coupons
		 */
		V;
	}

	public enum ConjuctionTktIndicator {
		CNJ
	}

	public enum OPERATION_STATUS {
		PROD, TEST, TST1, TST2;
	}

	public enum TicketingMode {
		/**
		 * Where the ticket was issued directly from an airline's or System Provider's computer system
		 */
		DIRECT("/"),
		/**
		 * Where the ticket was issued by a third party or agent's own computer system from an interface record supplied
		 * by the airline's or System Provider's computer system
		 */
		THIRD_PARTY("X"),
		/**
		 * Where the ticket was issued by an agent system without an airline or System Provider computer system
		 * interface.
		 */
		AGENT("A");

		private String str;

		TicketingMode(String str) {
			this.str = str;
		}

		@Override
		public String toString() {
			return this.str;
		}
	}

	public enum ApprovedLocationType {
		RETAIL_AGENCY("A"), ELECTRONIC_DELIVRY_NTW("E"), SATELLITE_TKT_PRINTER("S");

		private String str;

		ApprovedLocationType(String str) {
			this.str = str;
		}

		@Override
		public String toString() {
			return this.str;
		}
	}

	public enum DataInputStatusIndicator {
		/**
		 * Fully automated data and manually captured cancellation penalty/refund charge and waiver code of cancellation
		 * penalty
		 */
		A,
		/** Fully automated (no agent entered data is included in the Refund transaction) */
		F,
		/** Mixed automated data and data manually captured */
		L,
		/** Manual data capture - no automated data */
		M
	}

	public enum CommissionType {
		STANDARD(""), CCP("CCP"), XLP("XLP"), G("G"), N("N"), PSC("PSC"), CCPPSC("CCPPSC");

		private String str;

		CommissionType(String str) {
			this.str = str;
		}

		@Override
		public String toString() {
			return this.str;
		}
	}

	public enum StatisticalCodeType {
		Domestic("D"), International("I"), BLANK("");

		private String type;

		StatisticalCodeType(String type) {
			this.type = type;
		}

		public String getType() {
			return type;
		}
	}
}
