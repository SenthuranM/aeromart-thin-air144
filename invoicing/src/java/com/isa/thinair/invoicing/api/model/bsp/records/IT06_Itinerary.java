package com.isa.thinair.invoicing.api.model.bsp.records;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicDataType;
import com.isa.thinair.invoicing.api.model.bsp.BasicRecord;
import com.isa.thinair.invoicing.api.model.bsp.ElementFactory.DE_STATUS;
import com.isa.thinair.invoicing.api.model.bsp.ElementFactory.DISH_ELEMENT;
import com.isa.thinair.invoicing.api.model.bsp.ElementMetaData;
import com.isa.thinair.invoicing.api.model.bsp.FlightCoupon;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

public class IT06_Itinerary extends BasicRecord implements DishTransactional {

	private static Log log = LogFactory.getLog(IT06_Itinerary.class);

	private final static int MAX_FLIGHT_COUPON_SETS = 3;

	private int currentFlightCouponSetCount = 0;

	public IT06_Itinerary(DISH_FILE_TYPE fileType) throws BSPException {
		super(fileType, DISH_RECORD_TYPE.IT06);
	}

	public IT06_Itinerary(DISH_FILE_TYPE fileType, DISH_RECORD_TYPE recordType) throws BSPException {
		super(fileType, recordType);
	}

	public void setTransactionNo(Long tnxNo) throws BSPException {
		dataContainer.setValue(2, tnxNo);
	}

	public void addFlightCoupon(FlightCoupon flightCoupon) throws BSPException {
		if (MAX_FLIGHT_COUPON_SETS <= currentFlightCouponSetCount) {
			throw new RuntimeException("Max number of flight coupons allowed exceeded");
		}
		int offset = currentFlightCouponSetCount * 16;
		dataContainer.setValue(3 + offset, flightCoupon.getOriginAirportCode());
		dataContainer.setValue(4 + offset, flightCoupon.getDestinationAirportCode());
		dataContainer.setValue(5 + offset, flightCoupon.getFrequentFlyerReference());
		dataContainer.setValue(6 + offset, flightCoupon.getCarrier());
		dataContainer.setValue(8 + offset, flightCoupon.getReservationBookingDesignatior());
		dataContainer.setValue(9 + offset, flightCoupon.getFlightDate());
		dataContainer.setValue(10 + offset, flightCoupon.getNotValidBeforeDate());
		dataContainer.setValue(11 + offset, flightCoupon.getNotValidAfterDate());
		dataContainer.setValue(12 + offset, flightCoupon.getFareBasis());
		dataContainer.setValue(13 + offset, flightCoupon.getFlightNumber());
		dataContainer.setTime(14 + offset, flightCoupon.getFlightDepartureTime());
		dataContainer.setValue(15 + offset, flightCoupon.getFreeBaggageAllowance());
		dataContainer.setValue(16 + offset, flightCoupon.getFlightBookingStatus());
		dataContainer.setValue(17 + offset, flightCoupon.getSegmentIdentifier());
		dataContainer.setValue(18 + offset, flightCoupon.getStopOverCode());
		currentFlightCouponSetCount++;
	}

	@Override
	public boolean isValidDISHFormat() {
		boolean isSecondFlightCouponExists = false;
		boolean isThirdFlightCouponExists = false;

		boolean isValidDishFormat = true;
		int i = 0;
		for (BasicDataType basicDataType : dataContainer.getElementList()) {
			ElementMetaData metaData = dataContainer.getMetaDataList().get(i++);

			if (i < 18 || i >= 50) {
				if (metaData.getStatus() == DE_STATUS.M && metaData.getElement() != DISH_ELEMENT.RESD) {
					if (basicDataType.getData() == null) {
						log.error("mandatory rules violated " + "BasicDataType - " + basicDataType + ", MetaData - " + metaData);
						isValidDishFormat = false;
						break;
					}
				}
			} else if (i >= 18 && i < 34) {
				if (basicDataType.getData() != null) {
					isSecondFlightCouponExists = true;
				}
			} else if (i >= 34 && i < 50) {
				if (basicDataType.getData() != null) {
					isThirdFlightCouponExists = true;
				}
			}
		}
		if (isSecondFlightCouponExists) {
			for (int j = 18; j < 34; ++j) {
				BasicDataType basicDataType = dataContainer.getElementList().get(j);
				ElementMetaData metaData = dataContainer.getMetaDataList().get(j);
				if (metaData.getStatus() == DE_STATUS.M && metaData.getElement() != DISH_ELEMENT.RESD) {
					if (basicDataType.getData() == null) {
						log.error("mandatory rules violated " + "BasicDataType - " + basicDataType + ", MetaData - " + metaData);
						isValidDishFormat = false;
						break;
					}
				}
			}
		}

		if (isThirdFlightCouponExists) {
			for (int j = 34; j < 50; ++j) {
				BasicDataType basicDataType = dataContainer.getElementList().get(j);
				ElementMetaData metaData = dataContainer.getMetaDataList().get(j);
				if (metaData.getStatus() == DE_STATUS.M && metaData.getElement() != DISH_ELEMENT.RESD) {
					if (basicDataType.getData() == null) {
						log.error("mandatory rules violated " + "BasicDataType - " + basicDataType + ", MetaData - " + metaData);
						isValidDishFormat = false;
						break;
					}
				}
			}
		}

		return isValidDishFormat;
	}
}
