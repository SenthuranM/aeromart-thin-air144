package com.isa.thinair.invoicing.api.model.bsp.records;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicRecord;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

/**
 * @author primal
 * @date Mar 12, 2014
 */
public class XA0_Extend_Alternate_Coupon_Flight_History extends BasicRecord {

	public XA0_Extend_Alternate_Coupon_Flight_History(DISH_FILE_TYPE fileType) throws BSPException {
		super(fileType, DISH_RECORD_TYPE.XA0);
	}

	public void setTICKET_NUMBER(String ticketNumber) throws BSPException {
		dataContainer.setValue(1, ticketNumber);
	}

	public void setRTA_TIMESTAMP(Date ticketChangeTimeStamp) throws BSPException {
		dataContainer.setValue(2, new SimpleDateFormat("yyyyMMdd").format(ticketChangeTimeStamp));
	}

	public void setREC_TYPE(String recordType) throws BSPException {
		dataContainer.setValue(3, recordType);
	}

	public void setSEQUENCE_INDIC(String sequenceIndicator) throws BSPException {
		dataContainer.setValue(4, sequenceIndicator);
	}

	public void setSEN(String ticketCouponNumber) throws BSPException {
		dataContainer.setValue(5, ticketCouponNumber);
	}

	public void setSECOND_SEQ_NBR(String secondrySequenceNumber) throws BSPException {
		dataContainer.setValue(6, secondrySequenceNumber);
	}

	public void setUNIT_OF_MEASURE(String unitOfMesure) throws BSPException {
		dataContainer.setValue(7, unitOfMesure);
	}

	public void setNBR_OF_BAGS(String numberOfBages) throws BSPException {
		dataContainer.setValue(8, numberOfBages);
	}

	public void setCHK_BAG_WEIGHT(String uncheckedBaggageWeight) throws BSPException {
		dataContainer.setValue(9, uncheckedBaggageWeight);
	}

	public void setUNCHK_BAG_WEIGHT(String uncheckedBaggageWeight) throws BSPException {
		dataContainer.setValue(10, uncheckedBaggageWeight);
	}

	public void setHEAD_OF_POOL(String headOfPoolBaggegeEticketNumber) throws BSPException {
		dataContainer.setValue(11, headOfPoolBaggegeEticketNumber);
	}

	public void setNBR_OF_BAG_TAGS(String numberOfBaggageTagsOccurance) throws BSPException {
		dataContainer.setValue(12, numberOfBaggageTagsOccurance);
	}

	public void setBAG_TAG_INFO(String baggageTagInfo) throws BSPException {
		dataContainer.setValue(13, baggageTagInfo);
	}

	public void setDEST(String eticketDestination) throws BSPException {
		dataContainer.setValue(14, eticketDestination);
	}

	public void setBAG_TAG_CRR(String baggageTagCarrier) throws BSPException {
		dataContainer.setValue(15, baggageTagCarrier);
	}

	public void setSTART_BAG_TAG_NBR(String startingBaggageTagNumber) throws BSPException {
		dataContainer.setValue(16, startingBaggageTagNumber);
	}

	public void setNBR_CONSEC_NBRS(String numberOfConsecativeBaggageTags) throws BSPException {
		dataContainer.setValue(17, numberOfConsecativeBaggageTags);
	}

}
