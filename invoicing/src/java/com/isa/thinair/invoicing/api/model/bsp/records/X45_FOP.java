package com.isa.thinair.invoicing.api.model.bsp.records;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicRecord;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

/**
 * @author primal
 * @date Mar 10, 2014
 */
public class X45_FOP extends BasicRecord {

	public X45_FOP(DISH_FILE_TYPE fileType) throws BSPException {
		super(fileType, DISH_RECORD_TYPE.X45);
	}

	public void setTICKET_NUMBER(String ticketNumber) throws BSPException {
		dataContainer.setValue(1, ticketNumber);
	}

	public void setRTA_TIMESTAMP(Date ticketChangeTimeStamp) throws BSPException {
		dataContainer.setValue(2, new SimpleDateFormat("yyyyMMdd").format(ticketChangeTimeStamp));
	}

	public void setREC_TYPE(String recordType) throws BSPException {
		dataContainer.setValue(3, recordType);
	}

	public void setSEQUENCE_INDIC(String sequenceIndicator) throws BSPException {
		dataContainer.setValue(4, sequenceIndicator);
	}

	public void setFOP_TYPE(String fopType) throws BSPException {
		dataContainer.setValue(5, fopType);
	}

	public void setFOP_AMT(BigDecimal fopAmount) throws BSPException {
		dataContainer.setValue(6, fopAmount.toString());
	}

	public void setCC_VENDOR(String ccVendor) throws BSPException {
		dataContainer.setValue(7, ccVendor);
	}

	public void setCC_NUM(String ccNumber) throws BSPException {
		dataContainer.setValue(8, ccNumber);
	}

	public void setCC_EXP_DATE(String ccExpireDate) throws BSPException {
		dataContainer.setValue(9, ccExpireDate);
	}

	public void setAUTH(String auth) throws BSPException {
		dataContainer.setValue(10, auth);
	}

	public void setAPPROVAL_SOURCE(String approvalSequence) throws BSPException {
		dataContainer.setValue(11, approvalSequence);
	}

	public void setAUTH_AMT(String authAmount) throws BSPException {
		dataContainer.setValue(12, authAmount);
	}

	public void setADDR_VERIF_CODE(String addressVirificationCode) throws BSPException {
		dataContainer.setValue(13, addressVirificationCode);
	}

	public void set1ST_CC_NUM_EXT_PAY(String firstCCnumberExtPay) throws BSPException {
		dataContainer.setValue(14, firstCCnumberExtPay);
	}

	public void setCC_CORP_CONTRACT(String ccCropContract) throws BSPException {
		dataContainer.setValue(15, ccCropContract);
	}
}
