package com.isa.thinair.invoicing.api.model.bsp.records;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicDataType;
import com.isa.thinair.invoicing.api.model.bsp.BasicRecord;
import com.isa.thinair.invoicing.api.model.bsp.ElementFactory.DE_STATUS;
import com.isa.thinair.invoicing.api.model.bsp.ElementFactory.DISH_ELEMENT;
import com.isa.thinair.invoicing.api.model.bsp.ElementMetaData;
import com.isa.thinair.invoicing.api.model.bsp.FormOfPayment;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

public class IT08_FormOfPayment extends BasicRecord implements DishTransactional {
	private static Log log = LogFactory.getLog(IT08_FormOfPayment.class);
	private final static int MAX_FORM_OF_PAY_SETS = 2;
	// private final static int MAX_FORM_OF_PAY_CC_SETS = 2;

	private int currentFormOfPayCount = 0;

	// private int currentFormOfPayCCCount = 0;

	public IT08_FormOfPayment(DISH_FILE_TYPE fileType) throws BSPException {
		super(fileType, DISH_RECORD_TYPE.IT08);
	}

	public void setTransactionNo(Long id) throws BSPException {
		dataContainer.setValue(2, id);
	}

	public void addFormOfPayment(FormOfPayment fop) throws BSPException {
		if (MAX_FORM_OF_PAY_SETS <= currentFormOfPayCount) {
			throw new RuntimeException("Max number of form of payment allowed exceeded");
		}
		int offset = currentFormOfPayCount * 13;
		dataContainer.setValue(3 + offset, fop.getAccountNo());
		dataContainer.setValue(4 + offset, fop.getPaymentAmount(), fop.getDecimalPlaces());
		dataContainer.setValue(5 + offset, fop.getApprovalCode());
		dataContainer.setValue(6 + offset, fop.getCurrencyType());
		dataContainer.setValue(7 + offset, fop.getExtendedPaymentCode());
		dataContainer.setValue(8 + offset, fop.getFormOfPaymentType());
		dataContainer.setValue(9 + offset, fop.getExpiryDate());
		dataContainer.setValue(10 + offset, fop.getCustomerFileReference());
		dataContainer.setValue(11 + offset, fop.getCreditCardCorporateContract());
		dataContainer.setValue(12 + offset, fop.getAddressVerificationCode());
		dataContainer.setValue(13 + offset, fop.getSourceOfApprovalCode());
		dataContainer.setValue(14 + offset, fop.getTransactionInformation());
		dataContainer.setValue(15 + offset, fop.getAuthorisedAmount());
		currentFormOfPayCount++;
	}

	// only in DISH 20.3
	// public void addCardVerificationValueResult(String cardVerificationValueResult){
	// if(MAX_FORM_OF_PAY_CC_SETS <= currentFormOfPayCCCount){
	// throw new RuntimeException("Max number of card verification values results allowed exceeded");
	// }
	// int offset = currentFormOfPayCCCount;
	// dataContainer.setValue(29 + offset, cardVerificationValueResult);
	// currentFormOfPayCCCount++;
	// }
	@Override
	public boolean isValidDISHFormat() {
		boolean isSecondFormOfPaymentExists = false;

		boolean isValidDishFormat = true;
		int i = 0;
		for (BasicDataType basicDataType : dataContainer.getElementList()) {
			ElementMetaData metaData = dataContainer.getMetaDataList().get(i++);

			if (i < 15 || i >= 28) {
				if (metaData.getStatus() == DE_STATUS.M && metaData.getElement() != DISH_ELEMENT.RESD) {
					if (basicDataType.getData() == null) {
						log.error("mandatory rules violated " + "BasicDataType - " + basicDataType + ", MetaData - " + metaData);
						isValidDishFormat = false;
						break;
					}
				}
			} else if (i >= 15 && i < 28) {
				if (basicDataType.getData() != null) {
					isSecondFormOfPaymentExists = true;
				}
			}
		}
		if (isSecondFormOfPaymentExists) {
			for (int j = 18; j < 34; ++j) {
				BasicDataType basicDataType = dataContainer.getElementList().get(j);
				ElementMetaData metaData = dataContainer.getMetaDataList().get(j);
				if (metaData.getStatus() == DE_STATUS.M && metaData.getElement() != DISH_ELEMENT.RESD) {
					if (basicDataType.getData() == null) {
						log.error("mandatory rules violated " + "BasicDataType - " + basicDataType + ", MetaData - " + metaData);
						isValidDishFormat = false;
						break;
					}
				}
			}
		}

		return isValidDishFormat;
	}
}
