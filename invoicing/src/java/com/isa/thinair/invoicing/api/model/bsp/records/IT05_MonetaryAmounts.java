package com.isa.thinair.invoicing.api.model.bsp.records;

import java.math.BigDecimal;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicRecord;
import com.isa.thinair.invoicing.api.model.bsp.EnumTypes.CommissionType;
import com.isa.thinair.invoicing.api.model.bsp.EnumTypes.NetReportingIndicator;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

public class IT05_MonetaryAmounts extends BasicRecord implements DishTransactional {
	private final static int MAX_TAX_SETS = 6;
	private final static int MAX_COMISSION_SETS = 3;

	private int currentTaxSetCount = 0;
	private int currentComissionSetCount = 0;

	public IT05_MonetaryAmounts(DISH_FILE_TYPE fileType) throws BSPException {
		super(fileType, DISH_RECORD_TYPE.IT05);
	}

	public void setTransactionNo(Long tnxNo) throws BSPException {
		dataContainer.setValue(2, tnxNo);
	}

	public void setAmountEnteredByAgent(BigDecimal amountEnteredByAgent, int decimalPlaces) throws BSPException {
		dataContainer.setValue(3, amountEnteredByAgent, decimalPlaces);
	}

	public void addTaxFee(String taxFeeType, BigDecimal taxFeeAmount, int decimalPlaces) throws BSPException {
		if (MAX_TAX_SETS <= currentTaxSetCount) {
			throw new RuntimeException("Max number of taxes allowed exceeded");
		}
		if (currentTaxSetCount < 3) {
			int offset = currentTaxSetCount * 2;
			dataContainer.setValue(5 + offset, taxFeeType);
			dataContainer.setValue(6 + offset, taxFeeAmount, decimalPlaces);
		} else {
			int offset = (currentTaxSetCount - 3) * 2;
			dataContainer.setValue(15 + offset, taxFeeType);
			dataContainer.setValue(16 + offset, taxFeeAmount, decimalPlaces);
		}
		currentTaxSetCount++;
	}

	public void setTicketAmount(BigDecimal ticketAmount, int decimalPlaces) throws BSPException {
		dataContainer.setValue(11, ticketAmount, decimalPlaces);
	}

	public void setCurrencyType(String currencyCode, int decimalPlaces) throws BSPException {
		if (decimalPlaces > 9) {
			throw new RuntimeException("Invalid no of decimals :" + decimalPlaces);
		}
		dataContainer.setValue(12, currencyCode + decimalPlaces);
	}

	public void setTaxOnCommission(BigDecimal taxOnCommission, int decimalPlaces) throws BSPException {
		dataContainer.setValue(13, taxOnCommission, decimalPlaces);
	}

	public void addCommission(CommissionType type, BigDecimal rate, BigDecimal amount, int decimalPlaces) throws BSPException {
		if (MAX_COMISSION_SETS <= currentComissionSetCount) {
			throw new RuntimeException("Max number of commission allowed exceeded");
		}
		int offset = currentComissionSetCount * 3;
		dataContainer.setValue(21 + offset, type.toString());
		dataContainer.setValue(22 + offset, rate);
		dataContainer.setValue(23 + offset, amount, decimalPlaces);
		currentComissionSetCount++;
	}

	public void setNetReportingIndicator(NetReportingIndicator netReportingIndicator) throws BSPException {
		dataContainer.setValue(30, netReportingIndicator.toString());
	}

	public void setAmountPaidByCustomer(BigDecimal amountPaidByCustomer, int decimalPlaces) throws BSPException {
		dataContainer.setValue(31, amountPaidByCustomer, decimalPlaces);
	}

	public void setTaxOnCommissionType(String taxOnCommissionType) throws BSPException {
		dataContainer.setValue(32, taxOnCommissionType);
	}

}
