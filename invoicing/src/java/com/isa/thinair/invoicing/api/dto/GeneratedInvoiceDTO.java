/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.invoicing.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.airtravelagents.api.util.AirTravelAgentConstants;

/**
 * 
 * @author : Byorn
 * 
 */

public class GeneratedInvoiceDTO implements Serializable {

	private static final long serialVersionUID = 2345678971225748079L;
	private String invoiceNumber;

	private Date invoiceDate;

	private BigDecimal invoiceAmount;

	private BigDecimal invoiceAmountLocal;

	private String agentCode;

	private String agentName;

	private String billingEmailAddress;

	private String accountCode;

	private String transferedStatus;

	private String emailsSent;

	private int numOfMailsSent;

	private String reportToGSA;

	private BigDecimal settledAmount;

	private int billingPeriod;

	private String currencyCode;

	private SearchInvoicesDTO searchInvoicesDTO;

	private boolean inlocalCurrency = true;

	private Integer pnr;

	private String prefReportFmt;
	
	private String entityName;
	
	private String entityCode;

	public String getPrefReportFmt() {
		return prefReportFmt;
	}

	public void setPrefReportFmt(String prefReportFmt) {
		this.prefReportFmt = prefReportFmt;
	}

	public boolean isInlocalCurrency() {
		return inlocalCurrency;
	}

	public void setInlocalCurrency(boolean inlocalCurrency) {
		this.inlocalCurrency = inlocalCurrency;
	}

	/**
	 * @return Returns the searchInvoicesDTO.
	 */
	public SearchInvoicesDTO getSearchInvoicesDTO() {
		return searchInvoicesDTO;
	}

	/**
	 * @param searchInvoicesDTO
	 *            The searchInvoicesDTO to set.
	 */
	public void setSearchInvoicesDTO(SearchInvoicesDTO searchInvoicesDTO) {
		this.searchInvoicesDTO = searchInvoicesDTO;
	}

	/**
	 * @return Returns the accountCode.
	 */
	public String getAccountCode() {
		return accountCode;
	}

	/**
	 * @param accountCode
	 *            The accountCode to set.
	 */
	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}

	/**
	 * @return Returns the agentCode.
	 */
	public String getAgentCode() {
		return agentCode;
	}

	/**
	 * @param agentCode
	 *            The agentCode to set.
	 */
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * @return Returns the invoiceAmount.
	 */
	public BigDecimal getInvoiceAmount() {
		return invoiceAmount;
	}

	/**
	 * @param invoiceAmount
	 *            The invoiceAmount to set.
	 */
	public void setInvoiceAmount(BigDecimal invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}

	/**
	 * @return Returns the invoiceDate.
	 */
	public Date getInvoiceDate() {
		return invoiceDate;
	}

	/**
	 * @param invoiceDate
	 *            The invoiceDate to set.
	 */
	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	/**
	 * @return Returns the invoiceNumber.
	 */
	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	/**
	 * @param invoiceNumber
	 *            The invoiceNumber to set.
	 */
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	/**
	 * @return Returns the agentName.
	 */
	public String getAgentName() {
		return agentName;
	}

	/**
	 * @param agentName
	 *            The agentName to set.
	 */
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	/**
	 * @return Returns the transferedStatus.
	 */
	public String getTransferedStatus() {
		return transferedStatus;
	}

	/**
	 * @param transferedStatus
	 *            The transferedStatus to set.
	 */
	public void setTransferedStatus(String transferedStatus) {
		this.transferedStatus = transferedStatus;
	}

	/**
	 * @return Returns the billingEmailAddress.
	 */
	public String getBillingEmailAddress() {
		return billingEmailAddress;
	}

	/**
	 * @param billingEmailAddress
	 *            The billingEmailAddress to set.
	 */
	public void setBillingEmailAddress(String billingEmailAddress) {
		this.billingEmailAddress = billingEmailAddress;
	}

	/**
	 * @return Returns the emailsSent.
	 */
	public String getEmailsSent() {
		if (emailsSent != null) {
			if (emailsSent.equals(AirTravelAgentConstants.EmailStatus.SENT)) {
				return "SENT-" + getNumOfMailsSent();
			}
			if (emailsSent.equals(AirTravelAgentConstants.EmailStatus.PROCESSING)) {
				return "SENDING";
			}
		}
		return "NONE";
	}

	/**
	 * @param emailsSent
	 *            The emailsSent to set.
	 */
	public void setEmailsSent(String emailsSent) {
		this.emailsSent = emailsSent;
	}

	/**
	 * @return Returns the numOfMailsSent.
	 */
	public int getNumOfMailsSent() {
		return numOfMailsSent;
	}

	/**
	 * @param numOfMailsSent
	 *            The numOfMailsSent to set.
	 */
	public void setNumOfMailsSent(int numOfMailsSent) {
		this.numOfMailsSent = numOfMailsSent;
	}

	/**
	 * @return the reportToGSA
	 */
	public String getReportToGSA() {
		return reportToGSA;
	}

	/**
	 * @param reportToGSA
	 *            the reportToGSA to set
	 */
	public void setReportToGSA(String reportToGSA) {
		this.reportToGSA = reportToGSA;
	}

	/**
	 * @return Returns the billingPeriod.
	 */
	public int getBillingPeriod() {
		return billingPeriod;
	}

	/**
	 * @param billingPeriod
	 *            The billingPeriod to set.
	 */
	public void setBillingPeriod(int billingPeriod) {
		this.billingPeriod = billingPeriod;
	}

	/**
	 * @return Returns the settledAmount.
	 */
	public BigDecimal getSettledAmount() {
		return settledAmount;
	}

	/**
	 * @param settledAmount
	 *            The settledAmount to set.
	 */
	public void setSettledAmount(BigDecimal settledAmount) {
		this.settledAmount = settledAmount;
	}

	/**
	 * Gets the Agent Currency Code
	 * 
	 * @return currencyCode
	 */
	public String getCurrencyCode() {
		return currencyCode;
	}

	/**
	 * Sets the Agent Currency Code
	 * 
	 * @param currencyCode
	 *            the currencyCode
	 */
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public BigDecimal getInvoiceAmountLocal() {
		return invoiceAmountLocal;
	}

	public void setInvoiceAmountLocal(BigDecimal invoiceAmountLocal) {
		this.invoiceAmountLocal = invoiceAmountLocal;
	}

	/**
	 * Gets the pnr
	 * 
	 * @return currencyCode
	 */
	public Integer getPnr() {
		return pnr;
	}

	/**
	 * Sets the pnr
	 * 
	 * @param pnr
	 *            the currencyCode
	 */
	public void setPnr(Integer pnr) {
		this.pnr = pnr;
	}

	/**
	 * @return the entityName
	 */
	public String getEntityName() {
		return entityName;
	}

	/**
	 * @param entityName the entityName to set
	 */
	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	/**
	 * @return the entityCode
	 */
	public String getEntityCode() {
		return entityCode;
	}

	/**
	 * @param entityCode the entityCode to set
	 */
	public void setEntityCode(String entityCode) {
		this.entityCode = entityCode;
	}
}