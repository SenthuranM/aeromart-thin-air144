package com.isa.thinair.invoicing.api.model.bsp.records;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicRecord;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

/**
 * @author primal
 * @date Mar 12, 2014
 */
public class X80_Alternative_Flight_History_Record extends BasicRecord {

	public X80_Alternative_Flight_History_Record(DISH_FILE_TYPE fileType) throws BSPException {
		super(fileType, DISH_RECORD_TYPE.X80);
	}

	public void setTICKET_NUMBER(String ticketNumber) throws BSPException {
		dataContainer.setValue(1, ticketNumber);
	}

	public void setRTA_TIMESTAMP(Date ticketChangeTimeStamp) throws BSPException {
		dataContainer.setValue(2, new SimpleDateFormat("yyyyMMdd").format(ticketChangeTimeStamp));
	}

	public void setREC_TYPE(String recordType) throws BSPException {
		dataContainer.setValue(3, recordType);
	}

	public void setSEQUENCE_INDIC(String sequenceIndicator) throws BSPException {
		dataContainer.setValue(4, sequenceIndicator);
	}

	public void setATC(String alternateTicketCouponNumber) throws BSPException {
		dataContainer.setValue(5, alternateTicketCouponNumber);
	}

	public void setCT2(String alternateDepatureCity) throws BSPException {
		dataContainer.setValue(6, alternateDepatureCity);
	}

	public void setHCC(String alternateCarrierCode) throws BSPException {
		dataContainer.setValue(7, alternateCarrierCode);
	}

	public void setHDT(String alternateDepatureDate) throws BSPException {
		dataContainer.setValue(8, alternateDepatureDate);
	}

	public void setHDC(String alternateDistinationAireport) throws BSPException {
		dataContainer.setValue(9, alternateDistinationAireport);
	}

	public void setFLT(String alternateFlightNumber) throws BSPException {
		dataContainer.setValue(10, alternateFlightNumber);
	}

	public void setCLA(String alternateBookingClass) throws BSPException {
		dataContainer.setValue(11, alternateBookingClass);
	}

	public void setAVI(String voluntaryInvoluntaryIndicator) throws BSPException {
		dataContainer.setValue(12, voluntaryInvoluntaryIndicator);
	}

	public void setFBN(String alternativeFareBasisCode) throws BSPException {
		dataContainer.setValue(13, alternativeFareBasisCode);
	}

}
