package com.isa.thinair.invoicing.api.model.bsp.filetypes;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.records.X00_25_Ticket_Header_Record;
import com.isa.thinair.invoicing.api.model.bsp.records.X00_30_Ticket_Header_Record;
import com.isa.thinair.invoicing.api.model.bsp.records.X00_50_Ticket_Header_Record;
import com.isa.thinair.invoicing.api.model.bsp.records.X00_90_Ticket_Header_Record;
import com.isa.thinair.invoicing.api.model.bsp.records.X00_Mutation_Ticket_Header_Record;
import com.isa.thinair.invoicing.api.model.bsp.records.X00_Ticket_Header_Record;
import com.isa.thinair.invoicing.api.model.bsp.records.X34_Mutation_Fare_Record;
import com.isa.thinair.invoicing.api.model.bsp.records.X36_Ticket_Tax;
import com.isa.thinair.invoicing.api.model.bsp.records.X40_Commission_Data;
import com.isa.thinair.invoicing.api.model.bsp.records.X45_FOP;
import com.isa.thinair.invoicing.api.model.bsp.records.X60_Coupon;
import com.isa.thinair.invoicing.api.model.bsp.records.X70_Exchange_Records;
import com.isa.thinair.invoicing.api.model.bsp.records.X74_Recent_Exchange_Record;
import com.isa.thinair.invoicing.api.model.bsp.records.X80_Alternative_Flight_History_Record;
import com.isa.thinair.invoicing.api.model.bsp.records.X92_Coupon_Status_History_Record;
import com.isa.thinair.invoicing.api.model.bsp.records.X94_Revalidation_Record;
import com.isa.thinair.invoicing.api.model.bsp.records.XA0_Extend_Alternate_Coupon_Flight_History;

/**
 * @author primal
 * @date Apr 02, 2014
 */
public class ETLR extends BasicFileType {

	public ETLR() {
		super(DISH_FILE_TYPE.ETLR);
	}

	public void addTransactionHeader(X00_Ticket_Header_Record transactionHeader) throws BSPException {
		setTransactionId(transactionHeader);
		addElement(transactionHeader);
	}

	public void addTransactionHeader(X00_Mutation_Ticket_Header_Record transactionHeader) throws BSPException {
		addElement(transactionHeader);
	}

	public void addTransactionHeader(X00_25_Ticket_Header_Record transactionHeader) throws BSPException {
		addElement(transactionHeader);
	}

	public void addTransactionHeader(X00_30_Ticket_Header_Record transactionHeader) throws BSPException {
		addElement(transactionHeader);
	}

	public void addTransactionHeader(X00_50_Ticket_Header_Record transactionHeader) throws BSPException {
		addElement(transactionHeader);
	}

	public void addTransactionHeader(X00_90_Ticket_Header_Record transactionHeader) throws BSPException {
		addElement(transactionHeader);
	}

	public void addTransactionHeader(X34_Mutation_Fare_Record transactionHeader) throws BSPException {
		addElement(transactionHeader);
	}

	public void addTransactionHeader(X36_Ticket_Tax transactionHeader) throws BSPException {
		addElement(transactionHeader);
	}

	public void addTransactionHeader(X40_Commission_Data transactionHeader) throws BSPException {
		addElement(transactionHeader);
	}

	public void addTransactionHeader(X45_FOP transactionHeader) throws BSPException {
		addElement(transactionHeader);
	}

	public void addTransactionHeader(X60_Coupon transactionHeader) throws BSPException {
		addElement(transactionHeader);
	}

	public void addTransactionHeader(X70_Exchange_Records transactionHeader) throws BSPException {
		addElement(transactionHeader);
	}

	public void addTransactionHeader(X74_Recent_Exchange_Record transactionHeader) throws BSPException {
		addElement(transactionHeader);
	}

	public void addTransactionHeader(X80_Alternative_Flight_History_Record transactionHeader) throws BSPException {
		addElement(transactionHeader);
	}

	public void addTransactionHeader(X92_Coupon_Status_History_Record transactionHeader) throws BSPException {
		addElement(transactionHeader);
	}

	public void addTransactionHeader(X94_Revalidation_Record transactionHeader) throws BSPException {
		addElement(transactionHeader);
	}

	public void addTransactionHeader(XA0_Extend_Alternate_Coupon_Flight_History transactionHeader) throws BSPException {
		addElement(transactionHeader);
	}
}
