/**
 * 
 */
package com.isa.thinair.invoicing.api.model.bsp.records;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicRecord;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

/**
 * @author primal
 * @date Mar 10, 2014
 */
public class X36_Ticket_Tax extends BasicRecord {

	public X36_Ticket_Tax(DISH_FILE_TYPE fileType) throws BSPException {
		super(fileType, DISH_RECORD_TYPE.X36);
	}

	public void setTICKET_NUMBER(String ticketNumber) throws BSPException {
		dataContainer.setValue(1, ticketNumber);
	}

	public void setRTA_TIMESTAMP(Date ticketChangeTimeStamp) throws BSPException {
		dataContainer.setValue(2, new SimpleDateFormat("yyyyMMdd").format(ticketChangeTimeStamp));
	}

	public void setREC_TYPE(String recordType) throws BSPException {
		dataContainer.setValue(3, recordType);
	}

	// public void setSEQUENCE_INDIC(String sequenceIndicator) throws BSPException {
	// dataContainer.setValue(4, sequenceIndicator);
	// }
	//
	// public void setTAX_CUR_CODE(String taxCurrencyCode) throws BSPException {
	// dataContainer.setValue(5, taxCurrencyCode);
	// }
	//
	// public void setTAX_ISO_COUNTRY_CODE(String taxCountryCode) throws BSPException {
	// dataContainer.setValue(6, taxCountryCode);
	// }
	//
	// public void setTAX_ISO_TAX_CODE(String taxISOCode) throws BSPException {
	// dataContainer.setValue(7, taxISOCode);
	// }
	//
	// public void setTAX_AMOUNT(String taxAmount) throws BSPException {
	// dataContainer.setValue(8, taxAmount);
	// }

	public void addTAXData(int index, String taxCurrencyCode, String taxCountryCode, String taxISOCode, String taxAmount)
			throws BSPException {
		int position = index * 5;
		dataContainer.setValue(position + 4, Integer.toString(index + 1));
		dataContainer.setValue(position + 5, taxCurrencyCode);
		dataContainer.setValue(position + 6, taxCountryCode);
		dataContainer.setValue(position + 7, taxISOCode);
		dataContainer.setValue(position + 8, taxAmount);

	}

}
