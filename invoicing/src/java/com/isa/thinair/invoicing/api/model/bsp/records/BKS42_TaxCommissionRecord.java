package com.isa.thinair.invoicing.api.model.bsp.records;

import java.util.Date;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicBKRecord;
import com.isa.thinair.invoicing.api.model.bsp.ElementFactory.FMT_ID;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

public class BKS42_TaxCommissionRecord extends BasicBKRecord implements DishTransactional {

	public BKS42_TaxCommissionRecord(DISH_FILE_TYPE fileType) throws BSPException {
		super(fileType, DISH_RECORD_TYPE.BKS39, "BKS");
		// setTransactionCode(fileType);
		// setFormatIdentifier(fileType.getFormatId());
	}

	public void setTaxCommitionType(String type) throws BSPException {
		dataContainer.setValue(8, type);
	}

	public void setTaxCommitionAmount(Long amount) throws BSPException {
		dataContainer.setValue(10, amount);
	}

	public void setEffecCommitionRate(Long amount) throws BSPException {
		dataContainer.setValue(15, amount);
	}

	public void setEffecCommitionAmount(Long amount) throws BSPException {
		dataContainer.setValue(16, amount);
	}

	public void setAgentNumericCode(long identifier) throws BSPException {
		dataContainer.setValue(12, identifier);
	}

	public void setDocumentAmount(long amount) throws BSPException {
		dataContainer.setValue(15, amount);
	}

	public void setCurrencyType(String currType) throws BSPException {
		dataContainer.setValue(19, currType);
	}

	public void setReservedSpace(String resSpace) throws BSPException {
		dataContainer.setValue(9, resSpace);
		dataContainer.setValue(12, resSpace);
		dataContainer.setValue(15, resSpace);
		dataContainer.setValue(18, resSpace);
		dataContainer.setValue(20, resSpace);
	}

	public void setDateOfIssue(Date dateOfIssue) throws BSPException {
		dataContainer.setValue(12, dateOfIssue);
	}

	public void setTransactionCode(DISH_FILE_TYPE transactionCode) throws BSPException {
		dataContainer.setValue(15, transactionCode.toString());
	}

	public void setFormatIdentifier(FMT_ID formatIdentifier) throws BSPException {
		dataContainer.setValue(15, formatIdentifier.toString());
	}

	@Override
	public void setTransactionNo(Long tnxNo) throws BSPException {
		dataContainer.setValue(5, tnxNo);

	}

}
