package com.isa.thinair.invoicing.api.model.bsp.records;

import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicBKRecord;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

public class BAR64_DocumentAmounts extends BasicBKRecord implements DishTransactional {

	public BAR64_DocumentAmounts(DISH_FILE_TYPE fileType) throws BSPException {
		super(fileType, DISH_RECORD_TYPE.BAR64, "BAR", 64l);
		// setTransactionCode(fileType);
		// setFormatIdentifier(fileType.getFormatId());
	}

	@Override
	public void setTransactionNo(Long tnxNo) throws BSPException {
		dataContainer.setValue(2, tnxNo);

	}

	public void setStandardNumericQulaifier(String numQualifier) throws BSPException {
		dataContainer.setValue(3, numQualifier);
	}

	public void setRemitanceEndingDate(Date endingDate) throws BSPException {
		dataContainer.setValue(4, endingDate);
	}

	public void setTrasactionNo(long transactionNo) throws BSPException {
		dataContainer.setValue(5, transactionNo);
	}

	public void setDocumentNumber(String documentNo) throws BSPException {
		dataContainer.setValue(6, documentNo);
	}

	public void setCheckDigit(Long digit) throws BSPException {
		dataContainer.setValue(7, digit);
	}

	public void setFare(String fare) throws BSPException {
		dataContainer.setValue(8, fare);
	}

	public void setTicketingModeIndicator(String ticketingMode) throws BSPException {
		dataContainer.setValue(9, ticketingMode);
	}

	public void setEquiFarePaid(String fare) throws BSPException {
		dataContainer.setValue(10, fare);
	}

	public void setTax1(BigDecimal tax) throws BSPException {
		dataContainer.setValue(11, tax);
	}

	public void setTax2(BigDecimal tax) throws BSPException {
		dataContainer.setValue(12, tax);
	}

	public void setTax3(BigDecimal tax) throws BSPException {
		dataContainer.setValue(13, tax);
	}

	public void setTotal(BigDecimal total) throws BSPException {
		dataContainer.setValue(14, total);
	}

	public void setNeuSystemIdentifier(String neuIdentifier) throws BSPException {
		dataContainer.setValue(15, neuIdentifier);
	}

	public void setserviceProviderIdentifier(String serviceIdentifier) throws BSPException {
		dataContainer.setValue(16, serviceIdentifier);
	}

	public void setFareCalculationModeIdentifier(String fareCalculationModeIdentifier) throws BSPException {
		dataContainer.setValue(17, fareCalculationModeIdentifier);
	}

	public void setBookingAgentIdentifier(String bookingAgentIdentifier) throws BSPException {
		dataContainer.setValue(18, bookingAgentIdentifier);
	}

	public void setBookingLocationNumber(String bookingLocation) throws BSPException {
		dataContainer.setValue(19, bookingLocation);
	}

	public void setBookingEntryOutletType(String outletType) throws BSPException {
		dataContainer.setValue(20, outletType);
	}

	public void setFareCalculationPriceIndicator(String priceIndicator) throws BSPException {
		dataContainer.setValue(21, priceIndicator);
	}

	public void setReservedSpace(String resSpace) throws BSPException {
		dataContainer.setValue(22, resSpace);
	}

}
