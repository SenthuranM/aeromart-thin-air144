package com.isa.thinair.invoicing.api.model.bsp;

import java.util.Date;

public class BSPSegmentInfoDTO {

	private String pnr;

	private int pnrPaxID;

	private String origin;

	private String destination;

	private String carrierCode;

	private String flightNumber;

	private Date departureDateZulu;

	private Date departureDate;

	private String segmentCode;

	private long eTicketNumber;

	private int segmentIdentifier;

	private String freeBaggageAllowance;

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public int getPnrPaxID() {
		return pnrPaxID;
	}

	public void setPnrPaxID(int pnrPaxID) {
		this.pnrPaxID = pnrPaxID;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public Date getDepartureDateZulu() {
		return departureDateZulu;
	}

	public void setDepartureDateZulu(Date departureDateZulu) {
		this.departureDateZulu = departureDateZulu;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public long geteTicketNumber() {
		return eTicketNumber;
	}

	public void seteTicketNumber(long eTicketNumber) {
		this.eTicketNumber = eTicketNumber;
	}

	public int getSegmentIdentifier() {
		return segmentIdentifier;
	}

	public void setSegmentIdentifier(int segmentIdentifier) {
		this.segmentIdentifier = segmentIdentifier;
	}

	public String getFreeBaggageAllowance() {
		return freeBaggageAllowance;
	}

	public void setFreeBaggageAllowance(String freeBaggageAllowance) {
		this.freeBaggageAllowance = freeBaggageAllowance;
	}

}
