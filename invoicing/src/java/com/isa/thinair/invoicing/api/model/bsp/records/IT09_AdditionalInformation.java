package com.isa.thinair.invoicing.api.model.bsp.records;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicRecord;
import com.isa.thinair.invoicing.api.model.bsp.EnumTypes.FormOfPaymentType;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

public class IT09_AdditionalInformation extends BasicRecord implements DishTransactional {
	private final static int MAX_FORM_OF_PAY_SETS = 2;

	private int currentFormOfPayCount = 0;

	public IT09_AdditionalInformation(DISH_FILE_TYPE fileType) throws BSPException {
		super(fileType, DISH_RECORD_TYPE.IT09);
	}

	public void setTransactionNo(Long tnxId) throws BSPException {
		dataContainer.setValue(2, tnxId);
	}

	public void setEndorsements(String endorsements) throws BSPException {
		dataContainer.setValue(3, endorsements);
	}

	public void addFormOfPayment(FormOfPaymentType formOfPayment) throws BSPException {
		if (MAX_FORM_OF_PAY_SETS <= currentFormOfPayCount) {
			throw new RuntimeException("Max number of form of payment allowed exceeded");
		}
		int offset = currentFormOfPayCount;
		dataContainer.setValue(4 + offset, formOfPayment.toString());
		currentFormOfPayCount++;
	}

	public void addFormOfPayment(String formOfPayment) throws BSPException {
		if (MAX_FORM_OF_PAY_SETS <= currentFormOfPayCount) {
			throw new RuntimeException("Max number of form of payment allowed exceeded");
		}
		int offset = currentFormOfPayCount;
		dataContainer.setValue(4 + offset, formOfPayment);
		currentFormOfPayCount++;
	}
}
