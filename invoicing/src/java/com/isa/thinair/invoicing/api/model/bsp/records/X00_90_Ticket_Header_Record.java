package com.isa.thinair.invoicing.api.model.bsp.records;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicRecord;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

/**
 * @author primal
 * @date Mar 10, 2014
 */
public class X00_90_Ticket_Header_Record extends BasicRecord {

	public X00_90_Ticket_Header_Record(DISH_FILE_TYPE fileType) throws BSPException {
		super(fileType, DISH_RECORD_TYPE.X00_90);
	}

	public void setPLATING_CARRIER(String ownerCode) throws BSPException {
		dataContainer.setValue(1, ownerCode);
	}

	public void setDATE_TIME(Date issuedDateTime) throws BSPException {
		dataContainer.setValue(2, new SimpleDateFormat("yyyyMMddhhmmss").format(issuedDateTime));
	}

}
