package com.isa.thinair.invoicing.api.model.bsp.records;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicRecord;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

/**
 * @author primal
 * @date Mar 10, 2014
 */
public class X70_Exchange_Records extends BasicRecord {

	public X70_Exchange_Records(DISH_FILE_TYPE fileType) throws BSPException {
		super(fileType, DISH_RECORD_TYPE.X70);
	}

	public void setTICKET_NUMBER(String ticketNumber) throws BSPException {
		dataContainer.setValue(1, ticketNumber);
	}

	public void setRTA_TIMESTAMP(Date ticketChangeTimeStamp) throws BSPException {
		dataContainer.setValue(2, new SimpleDateFormat("yyyyMMdd").format(ticketChangeTimeStamp));
	}

	public void setREC_TYPE(String recordType) throws BSPException {
		dataContainer.setValue(3, recordType);
	}

	public void setSEQUENCE_INDIC(String sequenceIndicator) throws BSPException {
		dataContainer.setValue(4, sequenceIndicator);
	}

	public void setETK(String exchangeTicketNumber) throws BSPException {
		dataContainer.setValue(5, exchangeTicketNumber);
	}

	public void setEXCH_TYPE(String exchangeType) throws BSPException {
		dataContainer.setValue(6, exchangeType);
	}

	public void setORIG_FOP(String orginOfPayment) throws BSPException {
		dataContainer.setValue(7, orginOfPayment);
	}

	public void setADDL_COLL_CURR(String additionalCollectionCurrencyCode) throws BSPException {
		dataContainer.setValue(8, additionalCollectionCurrencyCode);
	}

	public void setADDL_COLL_AMT(String additionalCollectionAmount) throws BSPException {
		dataContainer.setValue(9, additionalCollectionAmount);
	}

	public void setADDL_COLL_TOTCURR(String additionalCollectionTotalCurrencyCode) throws BSPException {
		dataContainer.setValue(10, additionalCollectionTotalCurrencyCode);
	}

	public void setADDL_COLL_TOTAMT(String additionalCollectionTotalAmount) throws BSPException {
		dataContainer.setValue(11, additionalCollectionTotalAmount);
	}

	public void setADMIN_FEE_AMT(String adminFeeAmount) throws BSPException {
		dataContainer.setValue(12, adminFeeAmount);
	}

	public void setTTX(String ttx) throws BSPException {
		dataContainer.setValue(13, ttx);
	}

	public void setEXCHANGE_INFO(String exchangeInfo) throws BSPException {
		dataContainer.setValue(14, exchangeInfo);
	}

}
