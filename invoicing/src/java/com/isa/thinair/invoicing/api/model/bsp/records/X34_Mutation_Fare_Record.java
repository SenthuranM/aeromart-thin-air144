package com.isa.thinair.invoicing.api.model.bsp.records;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicRecord;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

/**
 * @author primal
 * @date Mar 10, 2014
 */
public class X34_Mutation_Fare_Record extends BasicRecord {

	public X34_Mutation_Fare_Record(DISH_FILE_TYPE fileType) throws BSPException {
		super(fileType, DISH_RECORD_TYPE.X34);
	}

	public void setTICKET_NUMBER(String ticketNumber) throws BSPException {
		dataContainer.setValue(1, ticketNumber);
	}

	public void setRTA_TIMESTAMP(Date ticketChangeTimeStamp) throws BSPException {
		dataContainer.setValue(2, new SimpleDateFormat("yyyyMMdd").format(ticketChangeTimeStamp));
	}

	public void setREC_TYPE(String recordType) throws BSPException {
		dataContainer.setValue(3, recordType);
	}

	public void setSEQENCE_INDIC(String sequenceIndicator) throws BSPException {
		dataContainer.setValue(4, sequenceIndicator);
	}

	public void setFARE_LADDER(String fareLadder) throws BSPException {
		dataContainer.setValue(5, fareLadder);
	}
}
