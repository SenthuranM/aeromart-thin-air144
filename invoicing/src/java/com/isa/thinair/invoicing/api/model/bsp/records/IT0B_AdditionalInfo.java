package com.isa.thinair.invoicing.api.model.bsp.records;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicRecord;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

public class IT0B_AdditionalInfo extends BasicRecord implements DishTransactional {
	public IT0B_AdditionalInfo(DISH_FILE_TYPE fileType) throws BSPException {
		super(fileType, DISH_RECORD_TYPE.IT0B);
	}

	public void setTransactionNo(Long tnxId) throws BSPException {
		dataContainer.setValue(2, tnxId);
	}

	public void setOptionalAgencyAirlineInfo(String optionalAgencyAirlineInfo) throws BSPException {
		dataContainer.setValue(3, optionalAgencyAirlineInfo);
	}
}
