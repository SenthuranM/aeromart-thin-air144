package com.isa.thinair.invoicing.api.model.bsp.records;

import java.util.Date;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicBKRecord;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

public class BKF81_FareCalculationRecord extends BasicBKRecord implements DishTransactional {

	public BKF81_FareCalculationRecord(DISH_FILE_TYPE fileType) throws BSPException {
		super(fileType, DISH_RECORD_TYPE.BKF81, "BKF", 81l);
		// setTransactionCode(fileType);
		// setFormatIdentifier(fileType.getFormatId());
	}

	@Override
	public void setTransactionNo(Long tnxNo) throws BSPException {
		dataContainer.setValue(2, tnxNo);

	}

	public void setStandardNumericQulaifier(String numQualifier) throws BSPException {
		dataContainer.setValue(3, numQualifier);
	}

	public void setRemitanceEndingDate(Date endingDate) throws BSPException {
		dataContainer.setValue(4, endingDate);
	}

	public void setTrasactionNo(long transactionNo) throws BSPException {
		dataContainer.setValue(5, transactionNo);
	}

	public void setDocumentNumber(String documentNo) throws BSPException {
		dataContainer.setValue(6, documentNo);
	}

	public void setCheckDigit(Long digit) throws BSPException {
		dataContainer.setValue(7, digit);
	}

	public void setReservedSpace(String data) throws BSPException {
		dataContainer.setValue(11, data);
		dataContainer.setValue(8, data);
	}

	public void setFareCalculatiuonSeqNo(Long no) throws BSPException {
		dataContainer.setValue(9, no);

	}

	public void setFareCalArea(String calAre) throws BSPException {
		dataContainer.setValue(10, calAre);

	}

	public void setPrintLineText(String printText) throws BSPException {
		dataContainer.setValue(11, printText);
	}
}