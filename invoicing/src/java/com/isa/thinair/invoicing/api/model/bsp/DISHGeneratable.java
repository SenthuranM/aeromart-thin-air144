package com.isa.thinair.invoicing.api.model.bsp;

public interface DISHGeneratable {
	public String getDISHFmtData() throws BSPException;

	public boolean isValidDISHFormat();
}
