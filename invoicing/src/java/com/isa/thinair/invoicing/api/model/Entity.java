package com.isa.thinair.invoicing.api.model;

import java.io.Serializable;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * 
 * @hibernate.class table = "T_ENTITY"
 */
public class Entity extends Persistent implements Serializable {
	
	private static final long serialVersionUID = 8569325475869854581L;
	                                             
	private String entityCode;
	
	private String entityName;
	
	private String status;
	
	private Integer displayOrder;

	/**
	 * @return the entityCode
	 * @hibernate.id column = "ENTITY_CODE" generator-class = "assigned"
	 */
	public String getEntityCode() {
		return entityCode;
	}

	/**
	 * @param entityCode the entityCode to set
	 */
	public void setEntityCode(String entityCode) {
		this.entityCode = entityCode;
	}

	/**
	 * @return the entityName
	 * @hibernate.property column = "ENTITY_NAME"
	 */
	public String getEntityName() {
		return entityName;
	}

	/**
	 * @param entityName the entityName to set
	 */
	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	/**
	 * @return the status
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the displayOrder
	 * @hibernate.property column = "DISPLAY_ORDER"
	 */
	public Integer getDisplayOrder() {
		return displayOrder;
	}

	/**
	 * @param displayOrder the displayOrder to set
	 */
	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}
}
