package com.isa.thinair.invoicing.api.model.bsp.records;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicRecord;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

public class IT0A_MiscDocInfo extends BasicRecord implements DishTransactional {
	public IT0A_MiscDocInfo(DISH_FILE_TYPE fileType) throws BSPException {
		super(fileType, DISH_RECORD_TYPE.IT0A);
	}

	public void setTransactionNo(Long id) throws BSPException {
		dataContainer.setValue(2, id);
	}
	public void setReasonForIssuanceCode(String rfic) throws BSPException {
		dataContainer.setValue(8, rfic);
	}
}
