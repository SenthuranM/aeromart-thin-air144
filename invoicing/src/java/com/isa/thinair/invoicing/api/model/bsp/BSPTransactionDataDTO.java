package com.isa.thinair.invoicing.api.model.bsp;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.Pair;
import com.isa.thinair.invoicing.api.model.EMDSTransaction;

public class BSPTransactionDataDTO {
	public enum TransactionType {
		TKTT("Payment"), RFND("Refund"), CANX("Cancel"), EMDA("Emda"), EMDS("Emds");

		private String description;

		TransactionType(String description) {
			this.description = description;
		}

		public String getDescription() {
			return description;
		}
	};

	public enum TaxCodes {
		TX1, TX2, TX3
	}

	private Pair<Long, List<BSPSegmentInfoDTO>> firstSetOfSegments;

	private Pair<Long, List<BSPSegmentInfoDTO>> secondSetOfSegments;

	private BigDecimal currencyExchangeRate;

	private String paymentDesc;

	private String ticketingAirlineCode;

	private BigDecimal agentCommission;

	private String paxName;

	private String longPaxName;

	private String title;

	private String pnr;

	private BigDecimal totalFare;

	private BigDecimal payCurrencyAmount;

	private String payCurrencyCode;

	private BigDecimal baseCurrencyAmount;

	private Date dateOfIssue;

	private Map<String, BigDecimal> taxCollection;

	private TransactionType tnxType;

	private String[] fareCalculationAreas;

	private Date originalPaymentTnxDate;

	private Integer aaTnxID;

	private String aaLCCUniqueID;

	private Integer paxSequnce;

	private String aaAgentCode;
	
	private boolean isOriginalPaymentFirstPayment;
	
	private EMDSTransaction emdsTransaction;
	
	private EMDSTransaction originalEMDSTransaction;
	
	private Long iataAgentCode;

	private ArrayList<BSPTransactionTaxData> tnxOndTaxInfoCollection;

	private String paxSpecificData;

	private String externalReference;
	
	private String productType = "P";

	public Pair<Long, List<BSPSegmentInfoDTO>> getFirstSetOfSegments() {
		return firstSetOfSegments;
	}

	public void setFirstSetOfSegments(Pair<Long, List<BSPSegmentInfoDTO>> firstSetOfSegments) {
		this.firstSetOfSegments = firstSetOfSegments;
	}

	public Pair<Long, List<BSPSegmentInfoDTO>> getSecondSetOfSegments() {
		return secondSetOfSegments;
	}

	public void setSecondSetOfSegments(Pair<Long, List<BSPSegmentInfoDTO>> secondSetOfSegments) {
		this.secondSetOfSegments = secondSetOfSegments;
	}

	public BigDecimal getCurrencyExchangeRate() {
		return currencyExchangeRate;
	}

	public void setCurrencyExchangeRate(BigDecimal currencyExchangeRate) {
		this.currencyExchangeRate = currencyExchangeRate;
	}

	public String getTicketingAirlineCode() {
		return ticketingAirlineCode;
	}

	public void setTicketingAirlineCode(String ticketingAirlineCode) {
		this.ticketingAirlineCode = ticketingAirlineCode;
	}

	public BigDecimal getAgentCommission() {
		return agentCommission;
	}

	public void setAgentCommission(BigDecimal agentCommission) {
		this.agentCommission = agentCommission;
	}

	public String getPaxName() {
		return paxName;
	}

	public void setPaxName(String paxName) {
		this.paxName = (paxName.length() > 16) ? paxName.substring(0, 16) : paxName;
	}

	public BigDecimal getTotalFare() {
		return totalFare;
	}

	public void setTotalFare(BigDecimal totalFare) {
		this.totalFare = totalFare;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public BigDecimal getPayCurrencyAmount() {
		return new BigDecimal(AccelAeroCalculator.formatAsDecimal(payCurrencyAmount));
	}

	public void setPayCurrencyAmount(BigDecimal payCurrencyAmount) {
		this.payCurrencyAmount = payCurrencyAmount;
	}

	public String getPayCurrencyCode() {
		return payCurrencyCode;
	}

	public void setPayCurrencyCode(String payCurrencyCode) {
		this.payCurrencyCode = payCurrencyCode;
	}

	public BigDecimal getBaseCurrencyAmount() {
		return baseCurrencyAmount;
	}

	public void setBaseCurrencyAmount(BigDecimal baseCurrencyAmount) {
		this.baseCurrencyAmount = baseCurrencyAmount;
	}

	public Date getDateOfIssue() {
		return dateOfIssue;
	}

	public void setDateOfIssue(Date dateOfIssue) {
		this.dateOfIssue = dateOfIssue;
	}

	public Map<String, BigDecimal> getTaxCollection() {
		return taxCollection;
	}

	public void setTaxCollection(Map<String, BigDecimal> taxCollection) {
		this.taxCollection = taxCollection;
	}

	public TransactionType getTnxType() {
		return tnxType;
	}

	public void setTnxType(TransactionType tnxType) {
		this.tnxType = tnxType;
	}

	public String[] getFareCalculationAreas() {
		return fareCalculationAreas;
	}

	public void setFareCalculationAreas(String[] fareCalculationAreas) {
		this.fareCalculationAreas = fareCalculationAreas;
	}

	public Date getOriginalPaymentTnxDate() {
		return originalPaymentTnxDate;
	}

	public void setOriginalPaymentTnxDate(Date originalPaymentTnxDate) {
		this.originalPaymentTnxDate = originalPaymentTnxDate;
	}

	public Integer getAaTnxID() {
		return aaTnxID;
	}

	public void setAaTnxID(Integer aaTnxID) {
		this.aaTnxID = aaTnxID;
	}

	public String getAaLCCUniqueID() {
		return aaLCCUniqueID;
	}

	public void setAaLCCUniqueID(String aaLCCUniqueID) {
		this.aaLCCUniqueID = aaLCCUniqueID;
	}

	public Integer getPaxSequnce() {
		return paxSequnce;
	}

	public void setPaxSequnce(Integer paxSequnce) {
		this.paxSequnce = paxSequnce;
	}

	public String getAaAgentCode() {
		return aaAgentCode;
	}

	public void setAaAgentCode(String aaAgentCode) {
		this.aaAgentCode = aaAgentCode;
	}

	public boolean isOriginalPaymentFirstPayment() {
		return isOriginalPaymentFirstPayment;
	}

	public void setOriginalPaymentFirstPayment(boolean isOriginalPaymentFirstPayment) {
		this.isOriginalPaymentFirstPayment = isOriginalPaymentFirstPayment;
	}
	
	public EMDSTransaction getEmdsTransaction() {
		return emdsTransaction;
	}

	public void setEmdsTransaction(EMDSTransaction emdsTransaction) {
		this.emdsTransaction = emdsTransaction;
	}

	public EMDSTransaction getOriginalEMDSTransaction() {
		return originalEMDSTransaction;
	}

	public void setOriginalEMDSTransaction(EMDSTransaction originalEMDSTransaction) {
		this.originalEMDSTransaction = originalEMDSTransaction;
	}

	public Long getIataAgentCode() {
		return iataAgentCode;
	}

	public void setIataAgentCode(Long iataAgentCode) {
		this.iataAgentCode = iataAgentCode;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPaymentDesc() {
		return paymentDesc;
	}

	public void setPaymentDesc(String paymentDesc) {
		this.paymentDesc = paymentDesc;
	}

	public String getLongPaxName() {
		return longPaxName;
	}

	public void setLongPaxName(String longPaxName) {
		this.longPaxName = longPaxName;
	}

	public ArrayList<BSPTransactionTaxData> getTnxOndTaxInfoCollection() {
		return tnxOndTaxInfoCollection;
	}

	public void setTnxOndTaxInfoCollection(ArrayList<BSPTransactionTaxData> tnxOndTaxInfoCollection) {
		this.tnxOndTaxInfoCollection = tnxOndTaxInfoCollection;
	}

	public String getPaxSpecificData() {
		return paxSpecificData;
	}

	public void setPaxSpecificData(String paxSpecificData) {
		this.paxSpecificData = paxSpecificData;
	}

	public String getExternalReference() {
		return externalReference;
	}

	public void setExternalReference(String externalReference) {
		this.externalReference = externalReference;
	}

	/**
	 * @return the productType
	 */
	public String getProductType() {
		return productType;
	}

	/**
	 * @param productType the productType to set
	 */
	public void setProductType(String productType) {
		this.productType = productType;
	}
	
	
}
