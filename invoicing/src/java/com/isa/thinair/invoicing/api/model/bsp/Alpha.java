package com.isa.thinair.invoicing.api.model.bsp;

public class Alpha extends BasicDataType{
	public Alpha(int startPosition, int maxLength, DATA_F field) {
		super(startPosition, maxLength, field);
	}

	public Alpha(int maxLength) {
		super(maxLength);
	}

	@Override
	public void setData(String str) throws BSPException {
		setData(str, false);
	}
	
	@Override
	public void setData(String str, boolean trim) throws BSPException {
		//TODO do validation of alpha
		super.setData(str, trim);
	}


}
