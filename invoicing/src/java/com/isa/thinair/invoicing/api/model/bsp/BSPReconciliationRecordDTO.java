/**
 * 
 */
package com.isa.thinair.invoicing.api.model.bsp;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Janaka Padukka
 * 
 */
public class BSPReconciliationRecordDTO implements Comparable<BSPReconciliationRecordDTO>, Serializable {
	
	private static final long serialVersionUID = 1L;

	private Date transactionDate;

	private Date dpcProcesssedDate;

	private Long primaryEticketNo;

	private Long secondaryEticketNo;

	private String pnr;

	private String agentCode;

	private String iataAgentCode;

	private String paxName;

	private String submitted;

	private String reconciled;

	private BigDecimal paymentAmount;

	private BigDecimal refundAmount;
	
	private BigDecimal processedPaymentAmount;
	
	private BigDecimal processedRefundAmount;

	private String currency;
	
	private String processedCurrency;

	private String transactionType;

	private String lccUniqueTnxID;

	private Integer tnxID;

	private int paxSequence;

	private String country;
	
	private String externalReference;

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public Date getDpcProcesssedDate() {
		return dpcProcesssedDate;
	}

	public void setDpcProcesssedDate(Date dpcProcesssedDate) {
		this.dpcProcesssedDate = dpcProcesssedDate;
	}

	public Long getPrimaryEticketNo() {
		return primaryEticketNo;
	}

	public void setPrimaryEticketNo(Long primaryEticketNo) {
		this.primaryEticketNo = primaryEticketNo;
	}

	public Long getSecondaryEticketNo() {
		return secondaryEticketNo;
	}

	public void setSecondaryEticketNo(Long secondaryEticketNo) {
		this.secondaryEticketNo = secondaryEticketNo;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getIataAgentCode() {
		return iataAgentCode;
	}

	public void setIataAgentCode(String iataAgentCode) {
		this.iataAgentCode = iataAgentCode;
	}

	public String getPaxName() {
		return paxName;
	}

	public void setPaxName(String paxName) {
		this.paxName = paxName;
	}

	public String getSubmitted() {
		return submitted;
	}

	public void setSubmitted(String submitted) {
		this.submitted = submitted;
	}

	public String getReconciled() {
		return reconciled;
	}

	public void setReconciled(String reconciled) {
		this.reconciled = reconciled;
	}

	public BigDecimal getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(BigDecimal paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public BigDecimal getRefundAmount() {
		return refundAmount;
	}

	public void setRefundAmount(BigDecimal refundAmount) {
		this.refundAmount = refundAmount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getLccUniqueTnxID() {
		return lccUniqueTnxID;
	}

	public void setLccUniqueTnxID(String lccUniqueTnxID) {
		this.lccUniqueTnxID = lccUniqueTnxID;
	}

	public Integer getTnxID() {
		return tnxID;
	}

	public void setTnxID(Integer tnxID) {
		this.tnxID = tnxID;
	}

	public BigDecimal getProcessedPaymentAmount() {
		return processedPaymentAmount;
	}

	public void setProcessedPaymentAmount(BigDecimal processedPaymentAmount) {
		this.processedPaymentAmount = processedPaymentAmount;
	}

	public BigDecimal getProcessedRefundAmount() {
		return processedRefundAmount;
	}

	public void setProcessedRefundAmount(BigDecimal processedRefundAmount) {
		this.processedRefundAmount = processedRefundAmount;
	}
	
	public String getProcessedCurrency() {
		return processedCurrency;
	}

	public void setProcessedCurrency(String processedCurrency) {
		this.processedCurrency = processedCurrency;
	}

	@Override
	public int compareTo(BSPReconciliationRecordDTO o) {
		return this.transactionDate.compareTo(o.getTransactionDate());
	}

	public int getPaxSequence() {
		return paxSequence;
	}

	public void setPaxSequence(int paxSequence) {
		this.paxSequence = paxSequence;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getExternalReference() {
		return externalReference;
	}

	public void setExternalReference(String externalReference) {
		this.externalReference = externalReference;
	}
	
	
}
