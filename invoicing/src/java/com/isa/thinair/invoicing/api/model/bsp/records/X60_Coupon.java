package com.isa.thinair.invoicing.api.model.bsp.records;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicRecord;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

/**
 * @author primal
 * @date Mar 10, 2014
 */
public class X60_Coupon extends BasicRecord {

	public X60_Coupon(DISH_FILE_TYPE fileType) throws BSPException {
		super(fileType, DISH_RECORD_TYPE.X60);
	}

	public void setTICKET_NUMBER(String ticketNumber) throws BSPException {
		dataContainer.setValue(1, ticketNumber);
	}

	public void setRTA_TIMESTAMP(Date ticketChangeTimeStamp) throws BSPException {
		dataContainer.setValue(2, new SimpleDateFormat("yyyyMMdd").format(ticketChangeTimeStamp));
	}

	public void setREC_TYPE(String recordType) throws BSPException {
		dataContainer.setValue(3, recordType);
	}

	public void setSEQUENCE_INDIC(BigDecimal sequenceIndicator) throws BSPException {
		dataContainer.setValue(4, sequenceIndicator, 0);
	}

	public void setTICKET_NUM(BigDecimal ticketNumber) throws BSPException {
		dataContainer.setValue(5, ticketNumber);
	}

	public void setFLIGHT_INDICS(String flightIndicator) throws BSPException {
		dataContainer.setValue(6, flightIndicator);
	}

	public void setSTAT_CHG_DATE(Date statChgDate) throws BSPException {
		dataContainer.setValue(7, new SimpleDateFormat("yyyyMMdd").format(statChgDate));
	}

	public void setEDIFACT_COUPON_NUM(BigDecimal edifactCoupanNumber) throws BSPException {
		dataContainer.setValue(8, edifactCoupanNumber, 0);
	}

	public void setCOUPON_STATUS(String couponStatus) throws BSPException {
		dataContainer.setValue(9, couponStatus);
	}

	public void setEXCH_TKT_NUM(String exchangeTicketNumber) throws BSPException {
		dataContainer.setValue(10, exchangeTicketNumber);
	}

	public void setCOUPON_VALUE(BigDecimal couponValue) throws BSPException {
		dataContainer.setValue(11, couponValue.toString());
	}

	public void setDEP_DATE(Date depDate) throws BSPException {
		dataContainer.setValue(12, new SimpleDateFormat("yyyyMMdd").format(depDate));
	}

	public void setDEP_TIMEIn24hrs(Date depTime) throws BSPException {
		dataContainer.setValue(13, new SimpleDateFormat("HHmm ").format(depTime));
	}

	public void setSTOPOVER_CODE(String stopOverCode) throws BSPException {
		dataContainer.setValue(14, stopOverCode);
	}

	public void setSEGMENT_STATUS(String segmentCode) throws BSPException {
		dataContainer.setValue(15, segmentCode);
	}

	public void setSAC_CODE(String segmentAuthorizationCode) throws BSPException {
		dataContainer.setValue(16, segmentAuthorizationCode);
	}

	public void setNVB_CENT(String nvbCent) throws BSPException {
		dataContainer.setValue(17, nvbCent);
	}

	public void setNVB_DATE(Date notValiedBeforeDate) throws BSPException {
		if (notValiedBeforeDate != null) {
			dataContainer.setValue(18, new SimpleDateFormat("yyMMdd").format(notValiedBeforeDate));
		} else {
			dataContainer.setValue(20, "");
		}
	}

	public void setNVA_CENT(String nvaCent) throws BSPException {
		dataContainer.setValue(19, nvaCent);
	}

	public void setNVA_DATE(Date notValiedAfterDate) throws BSPException {
		if (notValiedAfterDate != null) {
			dataContainer.setValue(20, new SimpleDateFormat("yyMMdd").format(notValiedAfterDate));
		} else {
			dataContainer.setValue(20, "");
		}
	}

	public void setORIG_AIRPORT(String originAirPort) throws BSPException {
		dataContainer.setValue(21, originAirPort);
	}

	public void setDEST_AIRPORT(String destinationAirPort) throws BSPException {
		dataContainer.setValue(22, destinationAirPort);
	}

	public void setMKTING_AIRLINE(String mktingAirLine) throws BSPException {
		dataContainer.setValue(23, mktingAirLine);
	}

	public void setCARRIER(String carrier) throws BSPException {
		dataContainer.setValue(24, carrier);
	}

	public void setFLIGHT_NUM(String flightNumber) throws BSPException {
		dataContainer.setValue(25, flightNumber);
	}

	public void setCLASS_OF_SERVICE(String classOfService) throws BSPException {
		dataContainer.setValue(26, classOfService);
	}

	public void setFQTV_AIRLINE(String ferquentFlyerAirlineCode) throws BSPException {
		dataContainer.setValue(27, ferquentFlyerAirlineCode);
	}

	public void setFQTV_NUMBER(String freqFlyerNumber) throws BSPException {
		dataContainer.setValue(28, freqFlyerNumber);
	}

	public void setBBB_DATE(String dateOfCouponLift) throws BSPException {
		dataContainer.setValue(29, dateOfCouponLift);
	}

	public void setFARE_BASIS_CODE(String fareBasisCode) throws BSPException {
		dataContainer.setValue(30, fareBasisCode);
	}

	public void setVOL_INVOL_INDIC(String voluntaryInvoluntaryIndicator) throws BSPException {
		dataContainer.setValue(31, voluntaryInvoluntaryIndicator);
	}

	public void setBAGGAGE_RESTRICT_CD(String baggageRestiction) throws BSPException {
		dataContainer.setValue(32, baggageRestiction);
	}

	public void setBAGGAGE_UNIT(String BaggageUnit) throws BSPException {
		dataContainer.setValue(33, BaggageUnit);
	}

}
