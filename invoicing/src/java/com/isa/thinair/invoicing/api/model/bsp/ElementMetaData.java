package com.isa.thinair.invoicing.api.model.bsp;

import com.isa.thinair.invoicing.api.model.bsp.ElementFactory.DE_STATUS;
import com.isa.thinair.invoicing.api.model.bsp.ElementFactory.DISH_ELEMENT;
import com.isa.thinair.invoicing.api.model.bsp.ElementFactory.FMT_ID;

public class ElementMetaData {
	private int length;
	private int startPos;
	private DISH_ELEMENT element;
	private DE_STATUS status = DE_STATUS.M;
	private FMT_ID rl = FMT_ID.S;

	public ElementMetaData(int startPos, int length, DISH_ELEMENT element) {
		this.length = length;
		this.startPos = startPos;
		this.element = element;
	}

	public ElementMetaData(int startPos, int length, DISH_ELEMENT element, DE_STATUS status) {
		this.length = length;
		this.startPos = startPos;
		this.element = element;
		this.status = status;
	}

	public ElementMetaData(int startPos, int length, DISH_ELEMENT element, FMT_ID rl) {
		this.length = length;
		this.startPos = startPos;
		this.element = element;
		this.rl = rl;
	}

	public ElementMetaData(int startPos, int length, DISH_ELEMENT element, FMT_ID rl, DE_STATUS status) {
		this.length = length;
		this.startPos = startPos;
		this.element = element;
		this.rl = rl;
		this.status = status;
	}

	@Override
	public ElementMetaData clone() {
		ElementMetaData md = new ElementMetaData(this.startPos, this.length, this.element, this.rl, this.status);
		return md;
	}

	public ElementMetaData clone(DE_STATUS status) {
		ElementMetaData md = new ElementMetaData(this.startPos, this.length, this.element, this.rl, status);
		return md;
	}

	public void setStatus(DE_STATUS status) {
		this.status = status;
	}

	public DISH_ELEMENT getElement() {
		return element;
	}

	public int getLength() {
		return length;
	}

	public DE_STATUS getStatus() {
		return status;
	}

	public int getStartPos() {
		return startPos;
	}

	@Override
	public String toString() {
		return element.toString() + "," + length + "," + startPos + "," + status + "," + rl.toString();
	}
}