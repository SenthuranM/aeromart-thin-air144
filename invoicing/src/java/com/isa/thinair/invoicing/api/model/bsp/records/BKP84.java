package com.isa.thinair.invoicing.api.model.bsp.records;

import java.math.BigDecimal;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicHOTFileRecordParser;
import com.isa.thinair.invoicing.api.model.bsp.utils.BSPApiUtils;

/**
 * @author Janaka Padukka
 * 
 */
public class BKP84 extends BasicHOTFileRecordParser {

	private BigDecimal REMT;

	private String CUTP;

	public BKP84(String recordString, String recordType) throws BSPException {
		super(recordString, recordType);
		parseRecord();
	}

	public void parseRecord() throws BSPException {

		try {
			this.CUTP = this.recordString.substring(132, 135);

			int numOfDecimalPlaces = Integer.parseInt(this.recordString.substring(135, 136));

			String parsedUnsignedAmountString = this.recordString.substring(97, 107)
					+ BSPApiUtils.getSignedFieldValueReplacement(this.recordString.charAt(107));

			String decimalPlaceIncludedAmountString = new StringBuilder(parsedUnsignedAmountString).insert(
					parsedUnsignedAmountString.length() - numOfDecimalPlaces, ".").toString();

			this.REMT = new BigDecimal(decimalPlaceIncludedAmountString);
		} catch (Exception e) {
			throw new BSPException("Invalid data format", e);
		}

	}

	public BigDecimal getREMT() {
		return REMT;
	}

	public String getCUTP() {
		return CUTP;
	}
}
