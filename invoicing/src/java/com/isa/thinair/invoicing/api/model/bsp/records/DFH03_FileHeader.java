package com.isa.thinair.invoicing.api.model.bsp.records;

import java.util.Date;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicRecord;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

public class DFH03_FileHeader extends BasicRecord {

	public DFH03_FileHeader(DISH_FILE_TYPE fileType) throws BSPException {
		super(fileType, DISH_RECORD_TYPE.DFH03, "BOH");
	}

	public void setSequenceNumber(Long segNo) throws BSPException {
		dataContainer.setValue(2, segNo);
	}

	public void setStandardNumQulifier(Long numQualifire) throws BSPException {
		dataContainer.setValue(3, numQualifire);
	}

	public void setAgentNumericCode(Long code) throws BSPException {
		dataContainer.setValue(4, code);
	}

	public void setRemitannceEndDate(Date date) throws BSPException {
		dataContainer.setValue(5, date);
	}

	public void setCurrencyType(String currency) throws BSPException {
		dataContainer.setValue(7, currency);
	}

	public void setMultyLocationIDentifier(String identifier) throws BSPException {
		dataContainer.setValue(8, identifier);
	}

	public void setReserveSpace(String reserveSpace) throws BSPException {
		dataContainer.setValue(6, reserveSpace);
		dataContainer.setValue(9, reserveSpace);
	}
}
