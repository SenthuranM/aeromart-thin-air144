package com.isa.thinair.invoicing.api.model.bsp;

import java.util.Date;

public class FlightCoupon {
	private String originAirportCode;
	private String destinationAirportCode;
	private String frequentFlyerReference;
	private String carrier;
	private String reservationBookingDesignatior;
	private Date flightDate;
	private Date notValidBeforeDate;
	private Date notValidAfterDate;
	private String fareBasis;
	private String flightNumber;
	private Date flightDepartureTime;
	private String freeBaggageAllowance;
	private String flightBookingStatus;
	private Long segmentIdentifier;
	private String stopOverCode;

	public String getOriginAirportCode() {
		return originAirportCode;
	}

	public String getDestinationAirportCode() {
		return destinationAirportCode;
	}

	public String getFrequentFlyerReference() {
		return frequentFlyerReference;
	}

	public String getCarrier() {
		return carrier;
	}

	public String getReservationBookingDesignatior() {
		return reservationBookingDesignatior;
	}

	public Date getFlightDate() {
		return flightDate;
	}

	public Date getNotValidBeforeDate() {
		return notValidBeforeDate;
	}

	public Date getNotValidAfterDate() {
		return notValidAfterDate;
	}

	public String getFareBasis() {
		return fareBasis;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public Date getFlightDepartureTime() {
		return flightDepartureTime;
	}

	public String getFreeBaggageAllowance() {
		return freeBaggageAllowance;
	}

	public String getFlightBookingStatus() {
		return flightBookingStatus;
	}

	public Long getSegmentIdentifier() {
		return segmentIdentifier;
	}

	public String getStopOverCode() {
		return stopOverCode;
	}

	public void setOriginAirportCode(String originAirportCode) {
		this.originAirportCode = originAirportCode;
	}

	public void setDestinationAirportCode(String destinationAirportCode) {
		this.destinationAirportCode = destinationAirportCode;
	}

	public void setFrequentFlyerReference(String frequentFlyerReference) {
		this.frequentFlyerReference = frequentFlyerReference;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	/**
	 * Set the booking class e.g. Y, W, Z
	 * 
	 * @param reservationBookingDesignatior
	 */
	public void setReservationBookingDesignatior(String reservationBookingDesignatior) {
		this.reservationBookingDesignatior = reservationBookingDesignatior;
	}

	public void setFlightDate(Date flightDate) {
		this.flightDate = flightDate;
	}

	public void setNotValidBeforeDate(Date notValidBeforeDate) {
		this.notValidBeforeDate = notValidBeforeDate;
	}

	public void setNotValidAfterDate(Date notValidAfterDate) {
		this.notValidAfterDate = notValidAfterDate;
	}

	public void setFareBasis(String fareBasis) {
		this.fareBasis = fareBasis;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public void setFlightDepartureTime(Date flightDepartureTime) {
		this.flightDepartureTime = flightDepartureTime;
	}

	public void setFreeBaggageAllowance(String freeBaggageAllowance) {
		this.freeBaggageAllowance = freeBaggageAllowance;
	}

	public void setFlightBookingStatus(String flightBookingStatus) {
		this.flightBookingStatus = flightBookingStatus;
	}

	public void setSegmentIdentifier(Long segmentIdentifier) {
		this.segmentIdentifier = segmentIdentifier;
	}

	public void setStopOverCode(String stopOverCode) {
		this.stopOverCode = stopOverCode;
	}

}
