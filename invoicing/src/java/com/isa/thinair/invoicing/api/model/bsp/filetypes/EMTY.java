package com.isa.thinair.invoicing.api.model.bsp.filetypes;

import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;

public class EMTY extends BasicFileType {

	public EMTY() {
		super(DISH_FILE_TYPE.EMTY);
	}

}
