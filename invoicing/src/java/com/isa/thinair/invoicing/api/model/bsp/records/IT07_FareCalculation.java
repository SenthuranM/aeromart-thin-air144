package com.isa.thinair.invoicing.api.model.bsp.records;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicRecord;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

public class IT07_FareCalculation extends BasicRecord implements DishTransactional {

	private final static int MAX_FARE_CALC_SETS = 2;
	private final static int MAX_FARE_CALC_AREA_LENGTH = 87;

	private int currentfareCalcAreaCount = 0;

	public IT07_FareCalculation(DISH_FILE_TYPE fileType) throws BSPException {
		super(fileType, DISH_RECORD_TYPE.IT07);
	}

	public void setTransactionNo(Long tnxNo) throws BSPException {
		dataContainer.setValue(2, tnxNo);
	}

	public void addFareCalculationArea(String fareCalculationArea, int sequence) throws BSPException {
		if (MAX_FARE_CALC_SETS <= currentfareCalcAreaCount) {
			throw new RuntimeException("Max number of fare calculation areas allowed exceeded");
		}
		int offset = currentfareCalcAreaCount * 2;
		if (fareCalculationArea.length() > MAX_FARE_CALC_AREA_LENGTH) {
			fareCalculationArea = fareCalculationArea.substring(0, 87);
		}
		dataContainer.setValue(3 + offset, fareCalculationArea);
		dataContainer.setValue(4 + offset, Integer.toString(sequence));
		currentfareCalcAreaCount++;
	}

	public void setFareCalculationModeIndicator(String fareCalculationModeIndicator) throws BSPException {
		dataContainer.setValue(7, fareCalculationModeIndicator);
	}
	 //available from DISH 20.3
	public void setFareCalculationPricingIndicator(String fareCalculationPricingIndicator) throws BSPException {
		dataContainer.setValue(8, fareCalculationPricingIndicator);
	}

}
