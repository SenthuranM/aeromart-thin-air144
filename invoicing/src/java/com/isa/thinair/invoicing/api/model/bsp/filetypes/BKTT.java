package com.isa.thinair.invoicing.api.model.bsp.filetypes;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.records.BAR64_DocumentAmounts;
import com.isa.thinair.invoicing.api.model.bsp.records.BAR65_AdditionalPassengerInformation;
import com.isa.thinair.invoicing.api.model.bsp.records.BAR66_AdditionalPaymentRecordInformation;
import com.isa.thinair.invoicing.api.model.bsp.records.BKF81_FareCalculationRecord;
import com.isa.thinair.invoicing.api.model.bsp.records.BKI63_ItineraryDataSegment;
import com.isa.thinair.invoicing.api.model.bsp.records.BKP83_ElectronicTransaction;
import com.isa.thinair.invoicing.api.model.bsp.records.BKP84_FormOfPayment;
import com.isa.thinair.invoicing.api.model.bsp.records.BKS24_TicketIdentification;
import com.isa.thinair.invoicing.api.model.bsp.records.BKS30_DocumentAmount;
import com.isa.thinair.invoicing.api.model.bsp.records.BKS39_CommissionRecord;
import com.isa.thinair.invoicing.api.model.bsp.records.BKS42_TaxCommissionRecord;
import com.isa.thinair.invoicing.api.model.bsp.records.BKS45_TicketInformation;
import com.isa.thinair.invoicing.api.model.bsp.records.BKS46_SalesTransactions;
import com.isa.thinair.invoicing.api.model.bsp.records.BKT06_TransactionHeader;
import com.isa.thinair.invoicing.api.model.bsp.records.BMD75_EDocumentCouponDetails;
import com.isa.thinair.invoicing.api.model.bsp.records.BMD76_EDocumentCouponRemarks;
import com.isa.thinair.invoicing.api.model.bsp.records.BMP70_MiscellaneousDocumentInformation;
import com.isa.thinair.invoicing.api.model.bsp.records.BMP71_MiscellaneousAdditionalInformationRecord;
import com.isa.thinair.invoicing.api.model.bsp.records.BMP72_MiscellaneousDocumnetAmountInLetters;
import com.isa.thinair.invoicing.api.model.bsp.records.BMP73_OptionalAirlineInformation;
import com.isa.thinair.invoicing.api.model.bsp.records.BMP74_AdditionalPrintLinesRecord;
import com.isa.thinair.invoicing.api.model.bsp.records.BMP77_CouponAdditionPrintLines;
import com.isa.thinair.invoicing.api.model.bsp.records.BMP78_TicketAdviceSponsorInformation;

public class BKTT extends BasicFileType {

	public BKTT() {
		super(DISH_FILE_TYPE.BKTT);
	}

	public void addTransactionHeader(BKT06_TransactionHeader transactionHeader) throws BSPException {
		setTransactionId(transactionHeader);
		addElement(transactionHeader);
	}

	public void addTicketIdentification(BKS24_TicketIdentification ticketIdentification) throws BSPException {
		setTransactionId(ticketIdentification);
		addElement(ticketIdentification);
	}

	public void addDocumentAmount(BKS30_DocumentAmount documentAmount) throws BSPException {
		setTransactionId(documentAmount);
		addElement(documentAmount);
	}

	public void addCommissionRecord(BKS39_CommissionRecord commissionRecord) throws BSPException {
		setTransactionId(commissionRecord);
		addElement(commissionRecord);
	}

	public void addTaxCommissionRecord(BKS42_TaxCommissionRecord taxCommitionRecord) throws BSPException {
		setTransactionId(taxCommitionRecord);
		addElement(taxCommitionRecord);
	}

	public void addTicketInformation(BKS45_TicketInformation ticketInformation) throws BSPException {
		setTransactionId(ticketInformation);
		addElement(ticketInformation);
	}

	public void addSalesTransactions(BKS46_SalesTransactions salesTransaction) throws BSPException {
		setTransactionId(salesTransaction);
		addElement(salesTransaction);
	}

	public void addItineraryDataSegment(BKI63_ItineraryDataSegment itineraryDataSegment) throws BSPException {
		setTransactionId(itineraryDataSegment);
		addElement(itineraryDataSegment);
	}

	public void addDocumentAmounts(BAR64_DocumentAmounts documentAmount) throws BSPException {
		setTransactionId(documentAmount);
		addElement(documentAmount);
	}

	public void addAdditionPassengerInfo(BAR65_AdditionalPassengerInformation additionPassengerInfo) throws BSPException {
		setTransactionId(additionPassengerInfo);
		addElement(additionPassengerInfo);
	}

	public void addAdditionPaymentRecordInfo(BAR66_AdditionalPaymentRecordInformation additionPaymentRecordInfo)
			throws BSPException {
		setTransactionId(additionPaymentRecordInfo);
		addElement(additionPaymentRecordInfo);
	}

	public void addMiscellaneousDocumentInformation(BMP70_MiscellaneousDocumentInformation miscellaneousDocumentInformation)
			throws BSPException {
		setTransactionId(miscellaneousDocumentInformation);
		addElement(miscellaneousDocumentInformation);
	}

	public void addMiscellaneousAdditionalInformationRecord(
			BMP71_MiscellaneousAdditionalInformationRecord miscellaneousAdditionalInformationRecord) throws BSPException {
		setTransactionId(miscellaneousAdditionalInformationRecord);
		addElement(miscellaneousAdditionalInformationRecord);
	}

	public void addMiscellaneousDocumnetAmountInLetters(
			BMP72_MiscellaneousDocumnetAmountInLetters miscellaneousDocumnetAmountInLetters) throws BSPException {
		setTransactionId(miscellaneousDocumnetAmountInLetters);
		addElement(miscellaneousDocumnetAmountInLetters);
	}

	public void addOptionalAirlineInformation(BMP73_OptionalAirlineInformation optionalAirlineInformation) throws BSPException {
		setTransactionId(optionalAirlineInformation);
		addElement(optionalAirlineInformation);
	}

	public void addAdditionalPrintLinesRecord(BMP74_AdditionalPrintLinesRecord additionalPrintLinesRecord) throws BSPException {
		setTransactionId(additionalPrintLinesRecord);
		addElement(additionalPrintLinesRecord);
	}

	public void addEDocumentCouponDetails(BMD75_EDocumentCouponDetails eDocumentCouponDetails) throws BSPException {
		setTransactionId(eDocumentCouponDetails);
		addElement(eDocumentCouponDetails);
	}

	public void addEDocumentCouponRemarks(BMD76_EDocumentCouponRemarks eDocumentCouponRemarks) throws BSPException {
		setTransactionId(eDocumentCouponRemarks);
		addElement(eDocumentCouponRemarks);
	}

	public void addCouponAdditionPrintLines(BMP77_CouponAdditionPrintLines couponAdditionPrintLines) throws BSPException {
		setTransactionId(couponAdditionPrintLines);
		addElement(couponAdditionPrintLines);
	}

	public void addTicketAdviceSponsorInformation(BMP78_TicketAdviceSponsorInformation ticketAdviceSponsorInformation)
			throws BSPException {
		setTransactionId(ticketAdviceSponsorInformation);
		addElement(ticketAdviceSponsorInformation);
	}

	public void addFareCalculationRecord(BKF81_FareCalculationRecord fareCalculationRecord) throws BSPException {
		setTransactionId(fareCalculationRecord);
		addElement(fareCalculationRecord);
	}

	public void addElectronicTransaction(BKP83_ElectronicTransaction electronicTransaction) throws BSPException {
		setTransactionId(electronicTransaction);
		addElement(electronicTransaction);
	}

	public void addFormOfPayment(BKP84_FormOfPayment formOfPayment) throws BSPException {
		setTransactionId(formOfPayment);
		addElement(formOfPayment);
	}
}
