/**
 * 
 */
package com.isa.thinair.invoicing.api.model.bsp.records;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicRecord;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

/**
 * @author primal
 * @date Mar 10, 2014
 */
public class X74_Recent_Exchange_Record extends BasicRecord {

	public X74_Recent_Exchange_Record(DISH_FILE_TYPE fileType) throws BSPException {
		super(fileType, DISH_RECORD_TYPE.X74);
	}

	public void setTICKET_NUMBER(String ticketNumber) throws BSPException {
		dataContainer.setValue(1, ticketNumber);
	}

	public void setRTA_TIMESTAMP(Date ticketChangeTimeStamp) throws BSPException {
		dataContainer.setValue(2, new SimpleDateFormat("yyyyMMdd").format(ticketChangeTimeStamp));
	}

	public void setREC_TYPE(String recordType) throws BSPException {
		dataContainer.setValue(3, recordType);
	}

	public void setSEQUENCE_INDIC(String sequenceIndicator) throws BSPException {
		dataContainer.setValue(4, sequenceIndicator);
	}

	public void setMOST_REC_EXCH_TKT(String mostRecentExchangeTicket) throws BSPException {
		dataContainer.setValue(5, mostRecentExchangeTicket);
	}

}
