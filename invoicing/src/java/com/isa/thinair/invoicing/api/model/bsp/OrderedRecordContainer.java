package com.isa.thinair.invoicing.api.model.bsp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.invoicing.api.model.bsp.ElementFactory.FMT_ID;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

public class OrderedRecordContainer implements DISHGeneratable {

	private static Log log = LogFactory.getLog(OrderedRecordContainer.class);

	List<BasicRecord> records = new ArrayList<BasicRecord>();
	private DISH_FILE_TYPE dishFileType;
	private FMT_ID formatId;
	private Map<DISH_RECORD_TYPE, Integer> recordCountMap = new HashMap<RecordFactory.DISH_RECORD_TYPE, Integer>();

	public OrderedRecordContainer(DISH_FILE_TYPE dishFileType) {
		this.dishFileType = dishFileType;
		this.formatId = dishFileType.getFormatId();
	}

	public void addElement(BasicRecord element) throws BSPException {
		if (formatId != null
				&& ((formatId != FMT_ID.NA && !element.recordType.isSupportedFmt(formatId)) || formatId == FMT_ID.NA
						&& !element.recordType.isSupportedFmt(FMT_ID.S))) {
			throw new BSPException("This record type " + element.recordType + " does not support format :" + formatId);
		}
		DISH_RECORD_TYPE precedingType = RecordFactory.getPrecedingRecordType(element.recordType);

		RecordMetaData recordMetaData = RecordFactory.getInstance().getRecordMetaData(dishFileType, element.recordType);
		if (!recordCountMap.containsKey(element.recordType)) {
			recordCountMap.put(element.recordType, 0);
		}
		if (recordMetaData.getMax() != RecordMetaData.UNLIMITED
				&& recordCountMap.get(element.recordType) >= recordMetaData.getMax()) {
			throw new BSPException("Invalid number of records added for record type :" + element.recordType);
		}

		int i = 0;
		if (precedingType != null) {
			for (BasicRecord record : records) {
				i++;
				if (precedingType == record.recordType) {
					break;
				}
			}
		}

		/*
		 * In case of repetitive record types of the precedingType or current type we have to skip over them
		 */
		while (records.size() > i
				&& records.get(i) != null
				&& (records.get(i).recordType == element.recordType || (precedingType != null && records.get(i).recordType == precedingType))) {
			i++;
		}

		records.add(i, element);
		recordCountMap.put(element.recordType, recordCountMap.get(element.recordType) + 1);
	}

	@Override
	public String getDISHFmtData() {
		StringBuilder sb = new StringBuilder();
		// int i = 1;
		for (BasicRecord record : records) {
			if (sb.length() > 0) {
				sb.append("\n");
			}
			sb.append(adjustRecordLengths(record.getDISHFmtData()));
			// sb.append((i++)+":"+adjustRecordLengths(record.getDISHFmtData()));
		}
		return sb.toString();
	}

	public String adjustRecordLengths(String record) {
		StringBuilder sb = new StringBuilder();
		if (record.length() > 256) {
			int count = record.length() / 256;
			int startIndex = 0;
			for (int i = 0; i < count + 1; i++) {
				if (i == count) {
					sb.append(record.substring(startIndex, record.length()));
				} else {
					sb.append(record.substring(startIndex, 256 * (i + 1)) + "\n");
				}
				startIndex += 256;
			}
		} else {
			sb.append(record);
		}
		return sb.toString();
	}

	@Override
	public boolean isValidDISHFormat() {
		boolean isValidDishFormat = true;
		for (BasicRecord basicRecord : records) {
			if (!basicRecord.isValidDISHFormat()) {
				log.error("BasicRecord error " + basicRecord.recordType.toString());
				isValidDishFormat = false;
				break;
			}
		}
		return isValidDishFormat;
	}

	public void removeElement(DISH_RECORD_TYPE recordType) throws BSPException {
		if (recordCountMap.get(recordType) == 1) {
			BasicRecord toRm = null;
			for (BasicRecord record : records) {
				if (record.recordType == recordType) {
					toRm = record;
					break;
				}
			}
			records.remove(toRm);
			recordCountMap.put(recordType, 0);
		} else if (recordCountMap.get(recordType) > 1) {
			throw new BSPException("Cannot set null value, because there are multiple entries of specified record type "
					+ recordType);
		}
	}

	public int getNoOfRecords() {
		return records.size();
	}
}
