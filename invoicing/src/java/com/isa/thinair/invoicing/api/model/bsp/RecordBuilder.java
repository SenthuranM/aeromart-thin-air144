package com.isa.thinair.invoicing.api.model.bsp;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

public class RecordBuilder {
	private Map<DISH_RECORD_TYPE, RecordMetaData> recordMap = new HashMap<RecordFactory.DISH_RECORD_TYPE, RecordMetaData>();

	public RecordBuilder() {
		recordMap = new HashMap<RecordFactory.DISH_RECORD_TYPE, RecordMetaData>();
	}

	public void addElementBuilder(DISH_RECORD_TYPE recordType, ElementBuilder elementBuilder, int min, int max) {

		RecordMetaData recordM = new RecordMetaData(recordType, elementBuilder, min, max);
		recordMap.put(recordType, recordM);
	}

	public RecordMetaData getRecordMetaData(DISH_RECORD_TYPE recordType) {
		return recordMap.get(recordType);
	}

	public ElementBuilder getElementBuilder(DISH_RECORD_TYPE recordType) {
		return recordMap.get(recordType).getElementBuilder();
	}

	public Collection<RecordMetaData> getRecordMetaDataList() {
		return recordMap.values();
	}
}