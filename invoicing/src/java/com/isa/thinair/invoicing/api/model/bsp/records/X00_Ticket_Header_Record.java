/**
 * 
 */
package com.isa.thinair.invoicing.api.model.bsp.records;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicRecord;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

/**
 * @author primal
 * @date Mar 7, 2014
 */

public class X00_Ticket_Header_Record extends BasicRecord implements DishTransactional {

	public X00_Ticket_Header_Record(DISH_FILE_TYPE fileType) throws BSPException {
		super(fileType, DISH_RECORD_TYPE.X00);
	}

	public void setTicketNumber(String ticketNumber) throws BSPException {
		dataContainer.setValue(1, ticketNumber);
	}

	public void setRTATimeStamp(Date dateTicketChange) throws BSPException {
		dataContainer.setValue(2, new SimpleDateFormat("yyyyMMdd").format(dateTicketChange));
	}

	public void setRecordType(String recType) throws BSPException {
		dataContainer.setValue(3, recType);
	}

	@Override
	public void setTransactionNo(Long tnxNo) throws BSPException {
		// TODO Auto-generated method stub

	}

}
