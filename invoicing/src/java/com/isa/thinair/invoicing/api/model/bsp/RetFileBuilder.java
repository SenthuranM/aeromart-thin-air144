package com.isa.thinair.invoicing.api.model.bsp;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.invoicing.api.model.bsp.EnumTypes.OPERATION_STATUS;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.filetypes.BasicFileType;
import com.isa.thinair.invoicing.api.model.bsp.filetypes.CANX;
import com.isa.thinair.invoicing.api.model.bsp.filetypes.EMDA;
import com.isa.thinair.invoicing.api.model.bsp.filetypes.EMDS;
import com.isa.thinair.invoicing.api.model.bsp.filetypes.EMTY;
import com.isa.thinair.invoicing.api.model.bsp.filetypes.RFND;
import com.isa.thinair.invoicing.api.model.bsp.filetypes.TKTT;
import com.isa.thinair.invoicing.api.model.bsp.records.IT01_FileHeader;
import com.isa.thinair.invoicing.api.model.bsp.records.IT0Z_FileTrailer;

public class RetFileBuilder implements DISHGeneratable {

	private final Long HANDBOOK_REV_NO = new Long(AppSysParamsUtil.getBSPRETFileVersion());
	private final String REPORTING_SYS_ID = "WEBL";
	private final String FILE_FORMAT = "ew";
	private final OPERATION_STATUS OP_STAT = (OPERATION_STATUS.PROD.toString().equals(
			AppSysParamsUtil.getBSPOperationStatus()) ? OPERATION_STATUS.PROD : OPERATION_STATUS.TEST);
	private final String WEBLINK = "WEBLINK";
	private List<DISHStructured> files = new ArrayList<DISHStructured>();
	private long tranxId = 1L;
	private String countryCode;
	private Date processingDate;
	private Date reportingPeriodEndDate;
	private Long revVersion;

	public RetFileBuilder(String countryCode, Date processingDate, Date reportingPeriodEndDate, Long revVersion) {
		this.countryCode = countryCode;
		this.processingDate = processingDate;
		this.reportingPeriodEndDate = reportingPeriodEndDate;
		this.revVersion = revVersion;
	}

	private void addElement(DISHStructured structuredItem) {
		structuredItem.setTrnxId(tranxId++);
		files.add(structuredItem);
	}

	private IT01_FileHeader getFileHeader(DISH_FILE_TYPE fileType) throws BSPException {
		IT01_FileHeader it01 = new IT01_FileHeader(fileType);
		it01.setReportingSystemId(REPORTING_SYS_ID);
		it01.setHandbookRevNo(revVersion);
		it01.setTestProdStatus(OP_STAT);
		it01.setCountryCode(countryCode);
		it01.setProcessingTime(processingDate);
		it01.setProcessingDate(processingDate);
		it01.setSysProvRptPeriodEnddate(reportingPeriodEndDate);
		return it01;
	}

	private IT0Z_FileTrailer getFileTrailer(DISH_FILE_TYPE fileType) throws BSPException {
		IT0Z_FileTrailer it0Z = new IT0Z_FileTrailer(fileType);
		// adding 1 due to the trailer record
		it0Z.setReportRecordCounter(getRecordCount() + 1);
		return it0Z;
	}

	public TKTT getNewTKTTInstance() {
		TKTT tktt = new TKTT();
		addElement(tktt);
		return tktt;
	}

	public EMDA getNewEMDAInstance() {
		EMDA emda = new EMDA();
		addElement(emda);
		return emda;
	}

	public RFND getNewRFNDInstance() {
		RFND rfnd = new RFND();
		addElement(rfnd);
		return rfnd;
	}

	public CANX getNewCANXInstance() {
		CANX canx = new CANX();
		addElement(canx);
		return canx;
	}

	public EMDS getNewEMDSInstance() {
		EMDS emds = new EMDS();
		addElement(emds);
		return emds;
	}

	private EMTY getNewEmptyInstance() {
		EMTY emty = new EMTY();
		addElement(emty);
		return emty;
	}

	@Override
	public String getDISHFmtData() throws BSPException {
		StringBuffer sb = new StringBuffer();
		int i = 1;
		if (files.size() > 0) {
			for (DISHStructured element : files) {
				if (i == 1) {
					IT01_FileHeader fh = getFileHeader(element.getFileType());
					element.setFileHeader(fh);
				} else {
					sb.append("\n");
				}
				if (i == files.size()) {
					IT0Z_FileTrailer ft = getFileTrailer(element.getFileType());
					element.setFileTrailer(ft);
				}
				sb.append(element.getDISHFmtData());
				i++;
			}
		} else if (files.size() == 0) {
			DISHStructured element = getNewEmptyInstance();
			IT01_FileHeader fh = getFileHeader(element.getFileType());
			element.setFileHeader(fh);
			IT0Z_FileTrailer ft = getFileTrailer(element.getFileType());
			element.setFileTrailer(ft);
			sb.append(element.getDISHFmtData());
		}

		return sb.toString();
	}

	@Override
	public boolean isValidDISHFormat() {
		// TODO Auto-generated method stub
		return false;
	}

	public String getRETFileName(int airlineCode, int seqNo) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		return this.countryCode + FILE_FORMAT + WEBLINK + "_" + sdf.format(this.processingDate) + "_" + airlineCode
				+ (airlineCode % 7) + "_" + String.format("%03d", seqNo);
	}

	public List<DISHStructured> getFiles() {
		return files;
	}

	public void setFiles(List<DISHStructured> files) {
		this.files = files;
	}

	public String getCountryCode() {
		return countryCode;
	}

	private int getRecordCount() {
		int recordCount = 0;
		for (DISHStructured file : files) {
			BasicFileType basicFileType = (BasicFileType) file;
			recordCount = recordCount + basicFileType.getNoOfRecords();
		}
		return recordCount;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((countryCode == null) ? 0 : countryCode.hashCode());
		result = prime * result + ((processingDate == null) ? 0 : processingDate.hashCode());
		result = prime * result + ((reportingPeriodEndDate == null) ? 0 : reportingPeriodEndDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetFileBuilder other = (RetFileBuilder) obj;
		if (countryCode == null) {
			if (other.countryCode != null)
				return false;
		} else if (!countryCode.equals(other.countryCode))
			return false;
		if (processingDate == null) {
			if (other.processingDate != null)
				return false;
		} else if (!processingDate.equals(other.processingDate))
			return false;
		if (reportingPeriodEndDate == null) {
			if (other.reportingPeriodEndDate != null)
				return false;
		} else if (!reportingPeriodEndDate.equals(other.reportingPeriodEndDate))
			return false;
		return true;
	}
}
