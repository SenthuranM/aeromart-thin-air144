package com.isa.thinair.invoicing.api.model.bsp;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class ETLRRec {

	private String eTicketNumber;

	private int eTicketVersion;

	private boolean isInFinalEticketStatus;

	private boolean allVoid;

	private Date modifiedDate;

	private Date createdDate;

	private String paxTypeCode;

	private String firstName;

	private String lastName;

	private String paxID;

	private String returnFlag;

	private String pnr;

	private String paymentCurrencyCode;

	private BigDecimal totalSurchargeAmount;

	private BigDecimal taxAmount;

	private BigDecimal fareAmount;

	private String agentTypeCode;

	private String agentCountaryCode;

	private String agentID;

	private String carrierCode;

	private List<ETicketCoupon> eTicketCoupon;

	private Collection<ETicketCouponStatus> eTicketCouponStatus;

	private List<ETicketFOP> eTicketFop;

	private HashMap<String, ETicketCoupon> lastUpdatedCouponData;

	public String geteTicketNumber() {
		return eTicketNumber;
	}

	public void seteTicketNumber(String eTicketNumber) {
		this.eTicketNumber = eTicketNumber;
	}

	public int geteTicketVersion() {
		return eTicketVersion;
	}

	public void seteTicketVersion(int eTicketVersion) {
		this.eTicketVersion = eTicketVersion;
	}

	public boolean getIsInFinalEticketStatus() {
		return isInFinalEticketStatus;
	}

	public void setIsInFinalEticketStatus(boolean isInFinalEticketStatus) {
		this.isInFinalEticketStatus = isInFinalEticketStatus;
	}

	public boolean isAllVoid() {
		return allVoid;
	}

	public void setAllVoid(boolean allVoid) {
		this.allVoid = allVoid;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getPaxTypeCode() {
		return paxTypeCode;
	}

	public void setPaxTypeCode(String paxTypeCode) {
		this.paxTypeCode = paxTypeCode;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPaxID() {
		return paxID;
	}

	public void setPaxID(String paxID) {
		this.paxID = paxID;
	}

	public String getReturnFlag() {
		return returnFlag;
	}

	public void setReturnFlag(String returnFlag) {
		this.returnFlag = returnFlag;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getPaymentCurrencyCode() {
		return paymentCurrencyCode;
	}

	public void setPaymentCurrencyCode(String paymentCurrencyCode) {
		this.paymentCurrencyCode = paymentCurrencyCode;
	}

	public BigDecimal getTotalSurchargeAmount() {
		return totalSurchargeAmount;
	}

	public void setTotalSurchargeAmount(BigDecimal totalSurchargeAmount) {
		this.totalSurchargeAmount = totalSurchargeAmount;
	}

	public BigDecimal getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(BigDecimal taxAmount) {
		this.taxAmount = taxAmount;
	}

	public BigDecimal getFareAmount() {
		return fareAmount;
	}

	public void setFareAmount(BigDecimal fareAmount) {
		this.fareAmount = fareAmount;
	}

	public String getAgentTypeCode() {
		return agentTypeCode;
	}

	public void setAgentTypeCode(String agentTypeCode) {
		this.agentTypeCode = agentTypeCode;
	}

	public String getAgentCountaryCode() {
		return agentCountaryCode;
	}

	public void setAgentCountaryCode(String agentCountaryCode) {
		this.agentCountaryCode = agentCountaryCode;
	}

	public String getAgentID() {
		return agentID;
	}

	public void setAgentID(String agentID) {
		this.agentID = agentID;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public List<ETicketCoupon> geteTicketCoupon() {
		return eTicketCoupon;
	}

	public void seteTicketCoupon(List<ETicketCoupon> eTicketCoupon) {
		this.eTicketCoupon = eTicketCoupon;
	}

	public Collection<ETicketCouponStatus> geteTicketCouponStatus() {
		return eTicketCouponStatus;
	}

	public void seteTicketCouponStatus(Collection<ETicketCouponStatus> eTicketCouponStatus) {
		this.eTicketCouponStatus = eTicketCouponStatus;
	}

	public List<ETicketFOP> geteTicketFop() {
		return eTicketFop;
	}

	public void seteTicketFop(List<ETicketFOP> eTicketFop) {
		this.eTicketFop = eTicketFop;
	}

	public HashMap<String, ETicketCoupon> getLastUpdatedCouponData() {
		return lastUpdatedCouponData;
	}

	public void setLastUpdatedCouponData(HashMap<String, ETicketCoupon> lastUpdatedCouponData) {
		this.lastUpdatedCouponData = lastUpdatedCouponData;
	}

}