package com.isa.thinair.invoicing.api.model.bsp.records;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicRecord;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

public class IT0Z_FileTrailer extends BasicRecord {
	public IT0Z_FileTrailer(DISH_FILE_TYPE fileType) throws BSPException {
		super(fileType, DISH_RECORD_TYPE.IT0Z);
	}

	public void setReportRecordCounter(int recordCount) throws BSPException {
		dataContainer.setValue(2, Integer.toString(recordCount));
	}
}
