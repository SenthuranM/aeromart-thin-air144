/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.invoicing.api.dto;

import java.io.Serializable;

/**
 * @author Byorn
 * 
 */
public class GenerateInvoiceOption implements Serializable {

	/**
	     * 
	 	*/
	private static final long serialVersionUID = -3456442404140544250L;

	private int code;

	public GenerateInvoiceOption(int code) {
		this.code = code;
	}

	public String toString() {
		return String.valueOf(this.code);
	}

	public int getCode() {
		return code;
	}

	public static final GenerateInvoiceOption IF_EXISTS_ROLLBACK_AND_OVERIDE_ALL = new GenerateInvoiceOption(1);

	public static final GenerateInvoiceOption IF_EXISTS_SKIP_AND_PROCEED = new GenerateInvoiceOption(2);

	public static final GenerateInvoiceOption IF_EXISTS_DO_NOT_PROCEED = new GenerateInvoiceOption(3);

}