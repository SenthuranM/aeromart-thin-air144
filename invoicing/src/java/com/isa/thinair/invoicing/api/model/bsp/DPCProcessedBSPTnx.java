/**
 * 
 */
package com.isa.thinair.invoicing.api.model.bsp;

import java.math.BigDecimal;

/**
 * @author Janaka Padukka
 * 
 */
public class DPCProcessedBSPTnx {

	private String pnr;

	private Long primaryEticket;

	private Long secondaryEticket;

	private String tranactionType;
	
	private BigDecimal processedAmount;
	
	private String processedCurrencyCode;

	public DPCProcessedBSPTnx(Long primaryEticket, Long secondaryEticket, String pnr, String tranactionType) {
		this.primaryEticket = primaryEticket;
		this.secondaryEticket = secondaryEticket;
		this.pnr = pnr;
		this.tranactionType = tranactionType;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getTranactionType() {
		return tranactionType;
	}

	public void setTranactionType(String tranactionType) {
		this.tranactionType = tranactionType;
	}

	public Long getPrimaryEticket() {
		return primaryEticket;
	}

	public void setPrimaryEticket(Long primaryEticket) {
		this.primaryEticket = primaryEticket;
	}

	public Long getSecondaryEticket() {
		return secondaryEticket;
	}

	public void setSecondaryEticket(Long secondaryEticket) {
		this.secondaryEticket = secondaryEticket;
	}

	public BigDecimal getProcessedAmount() {
		return processedAmount;
	}

	public void setProcessedAmount(BigDecimal processedAmount) {
		this.processedAmount = processedAmount;
	}

	public String getProcessedCurrencyCode() {
		return processedCurrencyCode;
	}

	public void setProcessedCurrencyCode(String currencyCode) {
		this.processedCurrencyCode = currencyCode;
	}
}
