package com.isa.thinair.invoicing.api.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import org.apache.commons.lang.builder.HashCodeBuilder;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * BSPFileLog is the entity class to represent BSP File Log
 * 
 * @hibernate.class table = "T_BSP_FILE_LOG"
 */
public class BSPFileLog extends Persistent implements Serializable {

	private Integer logID;

	private String fileName;

	private Integer fileSequence;

	private String countryCode;

	private String createdBy;

	private Date createdDate;

	private String modifiedby;

	private Date modifiedDate;
	
	private String RETFileStatus;
	
	private Set<DishFileTnxLogData> fileTransactionLogs;
	
	public enum RET_FILE_STATUS {
		CREATED, UPLOADED, UPLOAD_FAILED
	};

	/**
	 * @return Returns the logID.
	 * 
	 * @hibernate.id column="BSP_FILE_LOG_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_BSP_FILE_LOG"
	 */
	public Integer getLogID() {
		return logID;
	}

	public void setLogID(Integer logID) {
		this.logID = logID;
	}

	/**
	 * @return Returns the fileName.
	 * @hibernate.property column = "FILE_NAME"
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @param fileName
	 *            The fileName to set.
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * @return Returns the fileSequence.
	 * @hibernate.property column = "FILE_SEQUENCE"
	 */
	public Integer getFileSequence() {
		return fileSequence;
	}

	/**
	 * @param fileSequence
	 *            The fileSequence to set.
	 */
	public void setFileSequence(Integer fileSequence) {
		this.fileSequence = fileSequence;
	}

	/**
	 * @return Returns the countryCode.
	 * @hibernate.property column = "COUNTRY_CODE"
	 */
	public String getCountryCode() {
		return countryCode;
	}

	/**
	 * @param countryCode
	 *            The countryCode to set.
	 */
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	/**
	 * @return Returns the createdBy.
	 * @hibernate.property column = "CREATED_BY"
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            The createdBy to set.
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return Returns the createdDate.
	 * @hibernate.property column = "CREATED_DATE"
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate
	 *            The createdDate to set.
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return Returns the modifiedby.
	 * @hibernate.property column = "MODIFIED_BY"
	 */
	public String getModifiedby() {
		return modifiedby;
	}

	/**
	 * @param modifiedby
	 *            The modifiedby to set.
	 */
	public void setModifiedby(String modifiedby) {
		this.modifiedby = modifiedby;
	}

	/**
	 * @return Returns the modifiedDate.
	 * @hibernate.property column = "MODIFIED_DATE"
	 */
	public Date getModifiedDate() {
		return modifiedDate;
	}

	/**
	 * @param modifiedDate
	 *            The modifiedDate to set.
	 */
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	/**
	 * @return Returns the RETtFileStatus.
	 * @hibernate.property column = "RET_FILE_STATUS"
	 */
	public String getRETFileStatus() {
		return RETFileStatus;
	}

	public void setRETFileStatus(String rETFileStatus) {
		RETFileStatus = rETFileStatus;
	}
	
	/**
	 * @return Returns the fileTransactionLogs.
	 * @hibernate.set lazy="true" cascade="all" inverse="true"
	 * @hibernate.collection-key column="BSP_FILE_LOG_ID"
	 * @hibernate.collection-one-to-many class="com.isa.thinair.invoicing.api.model.DishFileTnxLogData"
	 */
	public Set<DishFileTnxLogData> getFileTransactionLogs() {
		return fileTransactionLogs;
	}

	public void setFileTransactionLogs(Set<DishFileTnxLogData> fileTransactionLogs) {
		this.fileTransactionLogs = fileTransactionLogs;
	}
	
	public int hashCode() {
		return new HashCodeBuilder().append(getLogID()).toHashCode();
	}
	
}
