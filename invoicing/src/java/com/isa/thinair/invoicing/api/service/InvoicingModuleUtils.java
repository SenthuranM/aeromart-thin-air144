/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.invoicing.api.service;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.airmaster.api.utils.AirmasterConstants;
import com.isa.thinair.airproxy.api.service.AirproxySegmentBD;
import com.isa.thinair.airproxy.api.utils.AirproxyConstants;
import com.isa.thinair.airreservation.api.service.PassengerBD;
import com.isa.thinair.airreservation.api.service.ReservationAuxilliaryBD;
import com.isa.thinair.airreservation.api.service.SegmentBD;
import com.isa.thinair.airreservation.api.utils.AirreservationConstants;
import com.isa.thinair.airtravelagents.api.service.TravelAgentBD;
import com.isa.thinair.airtravelagents.api.service.TravelAgentFinanceBD;
import com.isa.thinair.airtravelagents.api.utils.AirtravelagentsConstants;
import com.isa.thinair.auditor.api.service.AuditorBD;
import com.isa.thinair.auditor.api.utils.AuditorConstants;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.ExternalDAOSystemsConfig;
import com.isa.thinair.commons.core.util.GenericLookupUtils;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.XDBConnectionUtil;
import com.isa.thinair.invoicing.api.utils.InvoicingConstants;
import com.isa.thinair.invoicing.core.config.InvoicingConfig;
import com.isa.thinair.invoicing.core.persistence.dao.BSPLinkDAO;
import com.isa.thinair.invoicing.core.persistence.dao.BSPLinkJdbcDAO;
import com.isa.thinair.invoicing.core.persistence.dao.InvoiceDAO;
import com.isa.thinair.invoicing.core.persistence.dao.InvoiceJdbcDAO;
import com.isa.thinair.invoicing.core.persistence.dao.TransferInvoiceDAO;
import com.isa.thinair.invoicing.core.persistence.dao.TransferInvoiceJdbcDAO;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;
import com.isa.thinair.messaging.api.utils.MessagingConstants;
import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.IServiceDelegate;
import com.isa.thinair.platform.api.LookupService;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.reporting.api.service.DataExtractionBD;
import com.isa.thinair.reporting.api.utils.ReportingConstants;
import com.isa.thinair.reportingframework.api.service.ReportingFrameworkBD;
import com.isa.thinair.reportingframework.api.utils.ReportingframeworkConstants;

/**
 * @author Byorn
 * 
 */
public class InvoicingModuleUtils {

	public static final Log log = LogFactory.getLog(InvoicingModuleUtils.class);

	public static String TRANSFER_INVOICE_DAO = "transferinvoiceDAOProxy";

	public static String TRANSFER_INVOICE_JDBC_DAO = "transferinvoiceJdbcDAO";

	public static String INVOICEDAO = "invoiceDAO";

	public static String INVOICE_JDBC_DAO = "invoiceJdbcDAO";

	public static String BSPLINK_JDBC_DAO = "bspLinkJdbcDAO";

	public static String BSPLINK_DAO = "bspLinkDAO";

	public static DataSource getDatasource() {
		LookupService lookup = LookupServiceFactory.getInstance();
		return (DataSource) lookup.getBean(PlatformBeanUrls.Commons.DEFAULT_DATA_SOURCE_BEAN);
	}

	public static DataSource getDataSource(String type) {

		String dataSourceBean = null;
		if (AppSysParamsUtil.isReportingDbAvailable() && ReportsSearchCriteria.DATASOURCE_TYPE_RPT.equals(type)) {
			dataSourceBean = PlatformBeanUrls.Commons.REPORT_DATA_SOURCE_BEAN;
		} else {
			dataSourceBean = PlatformBeanUrls.Commons.DEFAULT_DATA_SOURCE_BEAN;
		}

		LookupService lookup = LookupServiceFactory.getInstance();
		return (DataSource) lookup.getBean(dataSourceBean);
	}

	public static TransferInvoiceDAO getTransferInvoiceDAO() {
		return (TransferInvoiceDAO) getInstance().getLocalBean(TRANSFER_INVOICE_DAO);
	}

	/**
	 * Returns the IModule interface
	 * 
	 * @return the IModule interface
	 */
	public static IModule getInstance() {
		return LookupServiceFactory.getInstance().getModule(InvoicingConstants.MODULE_NAME);
	}

	/**
	 * 
	 * @return
	 */
	public static TransferInvoiceJdbcDAO getTransferInvoiceJdbcDAO() {
		return (TransferInvoiceJdbcDAO) getInstance().getLocalBean(TRANSFER_INVOICE_JDBC_DAO);
	}

	public static BSPLinkJdbcDAO getBSPLinkJdbcDao() {
		return (BSPLinkJdbcDAO) getInstance().getLocalBean(BSPLINK_JDBC_DAO);
	}

	public static BSPLinkDAO getBSPLinkDao() {
		return (BSPLinkDAO) getDAO(BSPLINK_DAO);
	}

	/**
	 * Returns the external accounting system DAO
	 * 
	 * @return
	 */

	public static ExternalDAOSystemsConfig getExternalAccoutingSystemsDAOImplConfig() {

		String uri = "isa:base://modules/invoicing?id=externalDAOSystemsImplConfig";
		LookupService lookup = LookupServiceFactory.getInstance();
		return (ExternalDAOSystemsConfig) lookup.getBean(uri);

	}

	public static DataExtractionBD lookupDataExtractionBD() {
		return (DataExtractionBD) lookupEJB3Service(ReportingConstants.MODULE_NAME, DataExtractionBD.SERVICE_NAME);
	}

	public static ReportingFrameworkBD lookupReportingFrameworkBD() {
		return (ReportingFrameworkBD) lookupServiceBD(ReportingframeworkConstants.MODULE_NAME,
				ReportingframeworkConstants.BDKeys.REPORTINGFRAMEWORK_SERVICE);
	}

	public static PassengerBD getPassengerBD() {
		return (PassengerBD) lookupEJB3Service(AirreservationConstants.MODULE_NAME, PassengerBD.SERVICE_NAME);
	}

	public static SegmentBD getSegmentBD() {
		return (SegmentBD) lookupEJB3Service(AirreservationConstants.MODULE_NAME, SegmentBD.SERVICE_NAME);
	}

	public static MessagingServiceBD lookupMessagingBD() {
		return (MessagingServiceBD) lookupEJB3Service(MessagingConstants.MODULE_NAME, MessagingServiceBD.SERVICE_NAME);
	}

	public static InvoicingBD lookupInvoicingBD() {
		return (InvoicingBD) lookupEJB3Service(InvoicingConstants.MODULE_NAME, InvoicingBD.SERVICE_NAME);
	}

	public static CommonMasterBD getAirmasterBD() {
		return (CommonMasterBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, CommonMasterBD.SERVICE_NAME);
	}

	public static ReservationAuxilliaryBD getReservationAuxilliaryBD() {
		return (ReservationAuxilliaryBD) lookupEJB3Service(AirreservationConstants.MODULE_NAME,
				ReservationAuxilliaryBD.SERVICE_NAME);
	}

	public static TravelAgentBD lookupTravelAgentBD() {
		return (TravelAgentBD) lookupEJB3Service(AirtravelagentsConstants.MODULE_NAME, TravelAgentBD.SERVICE_NAME);
	}

	public static TravelAgentFinanceBD lookupTravelAgentFinanceBD() {
		return (TravelAgentFinanceBD) lookupEJB3Service(AirtravelagentsConstants.MODULE_NAME, TravelAgentFinanceBD.SERVICE_NAME);
	}

	public static AuditorBD getAuditorBD() {
		return (AuditorBD) lookupServiceBD(AuditorConstants.MODULE_NAME, AuditorConstants.BDKeys.AUDITOR_SERVICE);
	}

	public static AirproxySegmentBD getAirproxySegmentBD() {
		return (AirproxySegmentBD) lookupEJB3Service(AirproxyConstants.MODULE_NAME, AirproxySegmentBD.SERVICE_NAME);
	}

	/**
	 * Private to get InvoiceDAO.
	 * 
	 * @return InvoiceDAO.
	 */
	public static InvoiceDAO getInvoiceDAO() {
		return (InvoiceDAO) getDAO(INVOICEDAO);
	}

	/**
	 * 
	 * @return
	 */
	public static InvoiceJdbcDAO getInvoiceJdbcDAO() {
		return (InvoiceJdbcDAO) getInstance().getLocalBean(INVOICE_JDBC_DAO);
	}

	/**
	 * Gets the dao
	 * 
	 * @param module
	 * @param daoModule
	 * @return
	 */
	public static Object getDAO(String daoModule) {
		String beanUri = null;

		beanUri = "isa:base://modules/invoicing?id=" + daoModule + "Proxy";

		LookupService lookupService = LookupServiceFactory.getInstance();
		return lookupService.getBean(beanUri);
	}

	public static InvoicingConfig getInvoicingConfig() {
		return (InvoicingConfig) getInstance().getModuleConfig();
	}

	private static IServiceDelegate lookupServiceBD(String targetModuleName, String BDKeyWithoutLocality) {
		return GenericLookupUtils.lookupEJB2ServiceBD(targetModuleName, BDKeyWithoutLocality, getInvoicingConfig(),
				InvoicingConstants.MODULE_NAME, "module.dependencymap.invalid");
	}

	private static Object lookupEJB3Service(String targetModuleName, String serviceName) {
		return GenericLookupUtils.lookupEJB3Service(targetModuleName, serviceName, getInvoicingConfig(),
				InvoicingConstants.MODULE_NAME, "module.dependencymap.invalid");
	}

	public static GlobalConfig getGlobalConfig() {
		return (GlobalConfig) LookupServiceFactory.getInstance().getBean(PlatformBeanUrls.Commons.GLOBAL_CONFIG_BEAN);
	}

	public static XDBConnectionUtil getExternalDBConnectionUtil() {
		LookupService lookup = LookupServiceFactory.getInstance();
		return (XDBConnectionUtil) lookup.getBean("isa:base://modules?id=myXDBDataSourceUtil");
	}

}
