package com.isa.thinair.invoicing.api.model.bsp;

import java.math.BigDecimal;

public class Numeric extends BasicDataType {
	public Numeric(int startPosition, int maxLength, DATA_F field) {
		super(startPosition, maxLength, field);
	}

	public Numeric(int maxLength) {
		super(maxLength);
	}

	public void setDate(String data) throws BSPException {
		setData(data, false);
	}

	public void setDate(String data, boolean trim) throws BSPException {
		// TODO validate number
		super.setData(data, trim);
	}

	public void setData(Long lng) throws BSPException {
		setData(lng, false);
	}

	public void setData(BigDecimal decimal) throws BSPException {
		setData(decimal, false);
	}

	public void setData(BigDecimal decimal, int decimalPlaces) throws BSPException {
		setData(decimal, decimalPlaces, false);
	}

	public void setData(Long lng, boolean trim) throws BSPException {
		setData(lng.toString(), trim);
	}

	public void setData(BigDecimal decimal, boolean trim) throws BSPException {
		setData(decimal.toString().replace(".", ""), trim);
	}

	private String getZeros(int i) {
		StringBuffer bf = new StringBuffer();
		while (i > 0) {
			bf.append("0");
			i--;
		}
		return bf.toString();
	}

	public void setData(BigDecimal decimal, int decimalPlaces, boolean trim) throws BSPException {
		String str = decimal.toString();
		String arr[] = str.split("\\.");
		if (arr.length > 1) {
			String dec = arr[1];
			if (dec.length() < decimalPlaces) {
				dec = dec + getZeros(decimalPlaces - dec.length());
			} else if (dec.length() > decimalPlaces) {
				dec = dec.substring(0, decimalPlaces);
			}
			str = arr[0] + dec;
		}
		setData(str, trim);
	}
}
