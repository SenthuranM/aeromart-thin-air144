package com.isa.thinair.invoicing.api.model.bsp.records;

import java.util.Date;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicBKRecord;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

public class BMP77_CouponAdditionPrintLines extends BasicBKRecord implements DishTransactional {

	public BMP77_CouponAdditionPrintLines(DISH_FILE_TYPE fileType) throws BSPException {
		super(fileType, DISH_RECORD_TYPE.BMP77, "BMP", 77l);
		// setTransactionCode(fileType);
		// setFormatIdentifier(fileType.getFormatId());
	}

	@Override
	public void setTransactionNo(Long tnxNo) throws BSPException {
		dataContainer.setValue(2, tnxNo);

	}

	public void setStandardNumericQulaifier(String numQualifier) throws BSPException {
		dataContainer.setValue(3, numQualifier);
	}

	public void setRemitanceEndingDate(Date endingDate) throws BSPException {
		dataContainer.setValue(4, endingDate);
	}

	public void setTrasactionNo(long transactionNo) throws BSPException {
		dataContainer.setValue(5, transactionNo);
	}

	public void setDocumentNumber(String documentNo) throws BSPException {
		dataContainer.setValue(6, documentNo);
	}

	public void setCheckDigit(Long digit) throws BSPException {
		dataContainer.setValue(7, digit);
	}

	public void setReservedSpace(String data) throws BSPException {
		dataContainer.setValue(8, data);
		dataContainer.setValue(12, data);
	}

	public void setCouponNumber(String couponNum) throws BSPException {
		dataContainer.setValue(9, couponNum);
	}

	public void setPrintLineIdentifier(String printIdentifier) throws BSPException {
		dataContainer.setValue(10, printIdentifier);
	}

	public void setPrintLineText(String printText) throws BSPException {
		dataContainer.setValue(11, printText);
	}
}