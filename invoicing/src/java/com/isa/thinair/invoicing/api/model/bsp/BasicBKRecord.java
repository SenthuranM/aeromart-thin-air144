package com.isa.thinair.invoicing.api.model.bsp;

import java.util.Date;

import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

public class BasicBKRecord extends BasicRecord {

	public BasicBKRecord(DISH_FILE_TYPE fileType, DISH_RECORD_TYPE recordType) throws BSPException {
		super(fileType, recordType);
	}

	public BasicBKRecord(DISH_FILE_TYPE fileType, DISH_RECORD_TYPE recordType, String name) throws BSPException {
		super(fileType, recordType, name);
	}

	public BasicBKRecord(DISH_FILE_TYPE fileType, DISH_RECORD_TYPE recordType, String name, Long id) throws BSPException {
		super(fileType, recordType, name, id);
	}

	public void setSequenceNo(Long tnxNo) throws BSPException {
		dataContainer.setValue(2, tnxNo);

	}

	public void setStandardNumericQulaifier(String numQualifier) throws BSPException {
		dataContainer.setValue(3, numQualifier);
	}

	public void setIsssueDate(Date issueDate) throws BSPException {
		dataContainer.setValue(4, issueDate);
	}

	public void setDocumentNumber(String documentNo) throws BSPException {
		dataContainer.setValue(6, documentNo);
	}

	public void setCheckDigit(Long digit) throws BSPException {
		dataContainer.setValue(7, digit);
	}

	public void setTransactionNo(Long tnxNo) throws BSPException {
		dataContainer.setValue(5, tnxNo);

	}
}