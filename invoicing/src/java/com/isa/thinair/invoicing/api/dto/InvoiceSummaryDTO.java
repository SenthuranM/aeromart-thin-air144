/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.invoicing.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;

import com.isa.thinair.invoicing.api.model.Invoice;

/**
 * @author Chamindap
 * 
 */
public class InvoiceSummaryDTO implements Serializable {

	private static final long serialVersionUID = 6166301374922121336L;
	private Invoice invoice;
	private String agentAddress;
	private String paymentTerms;
	private String accountNo;
	private String accountName;
	private String bankNameAddress;
	private String agentName;
	private String agentEmail;
	private String reportToGSA;
	private BigDecimal invoiceResrvationAmount;
	private BigDecimal invoiceExternalAmount;
	private Map<String, BigDecimal> invoiceExternalProductAmounts;
	private Map<String, BigDecimal> invoiceExternalProductAmountsLocal;
	private String currencyCode;
	private BigDecimal fareAmount;
	private BigDecimal taxAmount;
	private BigDecimal modifyAmount;
	private BigDecimal surchrageAmount;
	private BigDecimal fareAmountLocal;
	private BigDecimal taxAmountLocal;
	private BigDecimal modifyAmountLocal;
	private BigDecimal surchrageAmountLocal;
	private BigDecimal invoiceResrvationAmountLocal;
	private BigDecimal invoiceExternalAmountLocal;

	/**
	 * 
	 * @return
	 */
	public BigDecimal getInvoiceResrvationAmountLocal() {
		return invoiceResrvationAmountLocal;
	}

	/**
	 * 
	 * @param invoiceResrvationAmountLocal
	 */
	public void setInvoiceResrvationAmountLocal(BigDecimal invoiceResrvationAmountLocal) {
		this.invoiceResrvationAmountLocal = invoiceResrvationAmountLocal;
	}

	/**
	 * 
	 * @return
	 */
	public BigDecimal getInvoiceExternalAmountLocal() {
		return invoiceExternalAmountLocal;
	}

	/**
	 * 
	 * @param invoiceExternalAmountLocal
	 */
	public void setInvoiceExternalAmountLocal(BigDecimal invoiceExternalAmountLocal) {
		this.invoiceExternalAmountLocal = invoiceExternalAmountLocal;
	}

	/**
	 * Gets the invoice Fare Amount
	 * 
	 * @return fareAmount
	 */
	public BigDecimal getFareAmount() {
		return fareAmount;
	}

	/**
	 * Sets the fare Amount
	 * 
	 * @param fareAmount
	 *            the fareAmount
	 */
	public void setFareAmount(BigDecimal fareAmount) {
		this.fareAmount = fareAmount;
	}

	/**
	 * Gets the Invoice Tax Amount
	 * 
	 * @return taxAmount
	 */
	public BigDecimal getTaxAmount() {
		return taxAmount;
	}

	/**
	 * Sets the Tax Amount
	 * 
	 * @param taxAmount
	 *            taxAmount
	 */
	public void setTaxAmount(BigDecimal taxAmount) {
		this.taxAmount = taxAmount;
	}

	/**
	 * Gets the Modify Amount
	 * 
	 * @return modifyAmount the modifyAmount
	 */
	public BigDecimal getModifyAmount() {
		return modifyAmount;
	}

	/**
	 * Sets the Modify Amount
	 * 
	 * @param modifyAmount
	 *            the modifyAmount
	 */
	public void setModifyAmount(BigDecimal modifyAmount) {
		this.modifyAmount = modifyAmount;
	}

	/**
	 * Gets the Surcharge Amount
	 * 
	 * @return surchrageAmount
	 */
	public BigDecimal getSurchrageAmount() {
		return surchrageAmount;
	}

	/**
	 * Sets the Surcharge Amount
	 * 
	 * @param surchrageAmount
	 *            the surchrageAmount
	 */
	public void setSurchrageAmount(BigDecimal surchrageAmount) {
		this.surchrageAmount = surchrageAmount;
	}

	/**
	 * @return Returns the agentEmail.
	 */
	public String getAgentEmail() {
		return agentEmail;
	}

	/**
	 * @param agentEmail
	 *            The agentEmail to set.
	 */
	public void setAgentEmail(String agentEmail) {
		this.agentEmail = agentEmail;
	}

	/**
	 * The constructor.
	 */
	public InvoiceSummaryDTO() {
		super();
	}

	/**
	 * @return Returns the accountName.
	 */
	public String getAccountName() {
		return accountName;
	}

	/**
	 * @param accountName
	 *            The accountName to set.
	 */
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	/**
	 * @return Returns the accountNo.
	 */
	public String getAccountNo() {
		return accountNo;
	}

	/**
	 * @param accountNo
	 *            The accountNo to set.
	 */
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	/**
	 * @return Returns the agentAddress.
	 */
	public String getAgentAddress() {
		return agentAddress;
	}

	/**
	 * @param agentAddress
	 *            The agentAddress to set.
	 */
	public void setAgentAddress(String agentAddress) {
		this.agentAddress = agentAddress;
	}

	/**
	 * @return Returns the bankNameAddress.
	 */
	public String getBankNameAddress() {
		return bankNameAddress;
	}

	/**
	 * @param bankNameAddress
	 *            The bankNameAddress to set.
	 */
	public void setBankNameAddress(String bankNameAddress) {
		this.bankNameAddress = bankNameAddress;
	}

	/**
	 * @return Returns the invoice.
	 */
	public Invoice getInvoice() {
		return invoice;
	}

	/**
	 * @param invoice
	 *            The invoice to set.
	 */
	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

	/**
	 * @return Returns the paymentTerms.
	 */
	public String getPaymentTerms() {
		return paymentTerms;
	}

	/**
	 * @param paymentTerms
	 *            The paymentTerms to set.
	 */
	public void setPaymentTerms(String paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	/**
	 * @return Returns the agentName.
	 */
	public String getAgentName() {
		return agentName;
	}

	/**
	 * @param agentName
	 *            The agentName to set.
	 */
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	/**
	 * @return the reportToGSA
	 */
	public String getReportToGSA() {
		return reportToGSA;
	}

	/**
	 * @param reportToGSA
	 *            the reportToGSA to set
	 */
	public void setReportToGSA(String reportToGSA) {
		this.reportToGSA = reportToGSA;
	}

	/**
	 * returns the invoiceResrvationAmount
	 * 
	 * @return Returns the invoiceResrvationAmount.
	 */
	public BigDecimal getInvoiceResrvationAmount() {
		return invoiceResrvationAmount;
	}

	/**
	 * sets the invoiceResrvationAmount
	 * 
	 * @param invoiceResrvationAmount
	 *            The Resrevations invoice Amount to set.
	 */
	public void setInvoiceResrvationAmount(BigDecimal invoiceResrvationAmount) {
		this.invoiceResrvationAmount = invoiceResrvationAmount;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public BigDecimal getFareAmountLocal() {
		return fareAmountLocal;
	}

	public void setFareAmountLocal(BigDecimal fareAmountLocal) {
		this.fareAmountLocal = fareAmountLocal;
	}

	public BigDecimal getTaxAmountLocal() {
		return taxAmountLocal;
	}

	public void setTaxAmountLocal(BigDecimal taxAmountLocal) {
		this.taxAmountLocal = taxAmountLocal;
	}

	public BigDecimal getModifyAmountLocal() {
		return modifyAmountLocal;
	}

	public void setModifyAmountLocal(BigDecimal modifyAmountLocal) {
		this.modifyAmountLocal = modifyAmountLocal;
	}

	public BigDecimal getSurchrageAmountLocal() {
		return surchrageAmountLocal;
	}

	public void setSurchrageAmountLocal(BigDecimal surchrageAmountLocal) {
		this.surchrageAmountLocal = surchrageAmountLocal;
	}

	public Map<String, BigDecimal> getInvoiceExternalProductAmounts() {
		return invoiceExternalProductAmounts;
	}

	public void setInvoiceExternalProductAmounts(Map<String, BigDecimal> invoiceExternalProductAmounts) {
		this.invoiceExternalProductAmounts = invoiceExternalProductAmounts;
	}

	public Map<String, BigDecimal> getInvoiceExternalProductAmountsLocal() {
		return invoiceExternalProductAmountsLocal;
	}

	public void setInvoiceExternalProductAmountsLocal(Map<String, BigDecimal> invoiceExternalProductAmountsLocal) {
		this.invoiceExternalProductAmountsLocal = invoiceExternalProductAmountsLocal;
	}

	public BigDecimal getInvoiceExternalAmount() {
		return invoiceExternalAmount;
	}

	public void setInvoiceExternalAmount(BigDecimal invoiceExternalAmount) {
		this.invoiceExternalAmount = invoiceExternalAmount;
	}

}