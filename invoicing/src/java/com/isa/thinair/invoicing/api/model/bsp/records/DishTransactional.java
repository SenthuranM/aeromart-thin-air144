package com.isa.thinair.invoicing.api.model.bsp.records;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;

public interface DishTransactional {
	public void setTransactionNo(Long tnxNo) throws BSPException;
}
