package com.isa.thinair.invoicing.api.model.bsp.records;

import java.math.BigDecimal;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicRecord;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

public class IT0Y_AgencyData extends BasicRecord implements DishTransactional {
	private final static int MAX_CC_REMARKS_SET = 3;
	private final static int MAX_REBATE_RECORD_SET = 2;

	private int currentCreditCardRmkCount = 0;
	private int currentrebateRecordCount = 0;

	public IT0Y_AgencyData(DISH_FILE_TYPE fileType) throws BSPException{
		super(fileType, DISH_RECORD_TYPE.IT0Y);
	}

	public void setTransactionNo(Long tnxNo) throws BSPException{
		dataContainer.setValue(2, tnxNo);
	}

	public void setAgentUse(String agentUse) throws BSPException{
		dataContainer.setValue(3, agentUse);
	}

	public void setReceivingOrderCode(String receivingOrderCode) throws BSPException{
		dataContainer.setValue(4, receivingOrderCode);
	}

	public void setSectionCode(String sectionCode) throws BSPException{
		dataContainer.setValue(5, sectionCode);
	}

	public void setSalesStaffCode(String salesStaffCode) throws BSPException{
		dataContainer.setValue(6, salesStaffCode);
	}

	public void setRoutingCode(String routingCode) throws BSPException{
		dataContainer.setValue(7, routingCode);
	}

	public void setCustomerCode(String customerCode) throws BSPException{
		dataContainer.setValue(8, customerCode);
	}

	public void setPayingCode(String payingCode) throws BSPException{
		dataContainer.setValue(9, payingCode);
	}

	public void setFreeCode(String freeCode) throws BSPException{
		dataContainer.setValue(10, freeCode);
	}

	public void setPackageTourCode(String packageTourCode) throws BSPException{
		dataContainer.setValue(11, packageTourCode);
	}

	public void addRebateCode(String rebateCode) throws BSPException{
		if (MAX_REBATE_RECORD_SET <= currentrebateRecordCount) {
			throw new RuntimeException("Max number of rebate codes allowed exceeded");
		}
		int offset = currentrebateRecordCount;
		dataContainer.setValue(12 + offset, rebateCode);
		currentrebateRecordCount++;
	}

	public void setPricingCode(String pricingCode) throws BSPException{
		dataContainer.setValue(14, pricingCode);
	}

	public void setCreditCardOriganisationCode(String creditCardOriganisationCode) throws BSPException{
		dataContainer.setValue(16, creditCardOriganisationCode);
	}

	public void setCreditCardExpenseNumber(String creditCardExpenseNumber) throws BSPException{
		dataContainer.setValue(17, creditCardExpenseNumber);
	}

	public void addCreditCardRemarks(String creditCardRemarks) throws BSPException{
		if (MAX_CC_REMARKS_SET <= currentCreditCardRmkCount) {
			throw new RuntimeException("Max number of credit card remarks allowed exceeded");
		}
		int offset = currentCreditCardRmkCount;
		dataContainer.setValue(18 + offset, creditCardRemarks);
		currentCreditCardRmkCount++;
	}

	public void setCreditCardHoldersName(String creditCardHoldersName) throws BSPException{
		dataContainer.setValue(21, creditCardHoldersName);
	}

	public void setSignedForAmount(BigDecimal signedForAmount) throws BSPException{
		dataContainer.setValue(22, signedForAmount);
	}

	public void setCurrencyType(String currencyCode, int noOfDecimals) throws BSPException{
		if (noOfDecimals > 9) {
			throw new RuntimeException("Invalid no of decimals :" + noOfDecimals);
		}
		dataContainer.setValue(23, currencyCode + noOfDecimals);
	}
}
