/**
 * 
 */
package com.isa.thinair.invoicing.api.model.bsp;

/**
 * @author Janaka Padukka
 * 
 */
public class BasicHOTFileRecordParser {

	protected String recordString;

	private String recordType;
	
	//Common record properties
	private String SMSG;
	
	private String SQNR;
	
	private String STNQ;
	
	
	public BasicHOTFileRecordParser(String recordString, String recordType) throws BSPException{
		this.recordString = recordString;
		this.recordType = recordType;
		parseRecord();	
	}

	public void parseRecord() throws BSPException {
		this.SMSG = this.recordString.substring(0, 3);
		this.SQNR = this.recordString.substring(3, 11);
		this.STNQ = this.recordString.substring(11, 13);
	}

	public String getRecordString() {
		return recordString;
	}

	public String getRecordType() {
		return recordType;
	}

	public String getSMSG() {
		return SMSG;
	}

	public String getSQNR() {
		return SQNR;
	}

	public String getSTNQ() {
		return STNQ;
	}
}
