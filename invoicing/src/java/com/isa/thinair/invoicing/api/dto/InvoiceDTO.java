/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.invoicing.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * 
 * @author : Byorn
 * 
 */

public class InvoiceDTO extends Persistent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3453406627478618505L;

	private String invoiceNumber;

	private Date invoiceDate;

	private Date inviocePeriodFrom;

	private Date invoicePeriodTo;

	private BigDecimal invoiceAmount;

	private String agentCode;

	private String accountCode;

	private String processStatus;

	private BigDecimal invoiceAmountLocal;

	private String currencyCode;

	private String accountingSystemName;
	
	private String entityName;

	/**
	 * @return Returns the accountCode.
	 */
	public String getAccountCode() {
		return accountCode;
	}

	/**
	 * @param accountCode
	 *            The accountCode to set.
	 */
	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}

	/**
	 * @return Returns the agentCode.
	 */
	public String getAgentCode() {
		return agentCode;
	}

	/**
	 * @param agentCode
	 *            The agentCode to set.
	 */
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	/**
	 * @return Returns the inviocePeriodFrom.
	 */
	public Date getInviocePeriodFrom() {
		return inviocePeriodFrom;
	}

	/**
	 * @param inviocePeriodFrom
	 *            The inviocePeriodFrom to set.
	 */
	public void setInviocePeriodFrom(Date inviocePeriodFrom) {
		this.inviocePeriodFrom = inviocePeriodFrom;
	}

	/**
	 * @return Returns the invoiceAmount.
	 */
	public BigDecimal getInvoiceAmount() {
		return invoiceAmount;
	}

	/**
	 * @param invoiceAmount
	 *            The invoiceAmount to set.
	 */
	public void setInvoiceAmount(BigDecimal invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}

	/**
	 * @return Returns the invoiceDate.
	 */
	public Date getInvoiceDate() {
		return invoiceDate;
	}

	/**
	 * @param invoiceDate
	 *            The invoiceDate to set.
	 */
	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	/**
	 * @return Returns the invoiceNumber.
	 */
	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	/**
	 * @param invoiceNumber
	 *            The invoiceNumber to set.
	 */
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	/**
	 * @return Returns the invoicePeriodTo.
	 */
	public Date getInvoicePeriodTo() {
		return invoicePeriodTo;
	}

	/**
	 * @param invoicePeriodTo
	 *            The invoicePeriodTo to set.
	 */
	public void setInvoicePeriodTo(Date invoicePeriodTo) {
		this.invoicePeriodTo = invoicePeriodTo;
	}

	/**
	 * @return Returns the processStatus.
	 */
	public String getProcessStatus() {
		return processStatus;
	}

	/**
	 * @param processStatus
	 *            The processStatus to set.
	 */
	public void setProcessStatus(String processStatus) {
		this.processStatus = processStatus;
	}

	public BigDecimal getInvoiceAmountLocal() {
		return invoiceAmountLocal;
	}

	public void setInvoiceAmountLocal(BigDecimal invoiceAmountLocal) {
		this.invoiceAmountLocal = invoiceAmountLocal;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getAccountingSystemName() {
		return accountingSystemName;
	}

	public void setAccountingSystemName(String accountingSystemName) {
		this.accountingSystemName = accountingSystemName;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

}