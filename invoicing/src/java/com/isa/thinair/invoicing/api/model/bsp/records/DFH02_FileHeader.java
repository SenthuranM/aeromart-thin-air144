package com.isa.thinair.invoicing.api.model.bsp.records;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicRecord;
import com.isa.thinair.invoicing.api.model.bsp.EnumTypes.OPERATION_STATUS;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

public class DFH02_FileHeader extends BasicRecord {

	public DFH02_FileHeader(DISH_FILE_TYPE fileType) throws BSPException {
		super(fileType, DISH_RECORD_TYPE.DFH02, "BCH");
	}

	public void setSequenceNumber(Long segNo) throws BSPException {
		dataContainer.setValue(2, segNo);
	}

	public void setStandardNumQulifier(Long numQualifire) throws BSPException {
		dataContainer.setValue(3, numQualifire);
	}

	public void setBSPIdentifier(String bspIden) throws BSPException {
		dataContainer.setValue(4, bspIden);
	}

	public void setAirlineCode(String airlineCode) throws BSPException {
		dataContainer.setValue(5, airlineCode);
	}

	public void setHandbookRevNo(Long revNo) throws BSPException {
		dataContainer.setValue(6, revNo);
	}

	public void setTestProdStatus(OPERATION_STATUS status) throws BSPException {
		dataContainer.setValue(7, status.toString());
	}

	public void setProcessingDate(Date date) throws BSPException {
		dataContainer.setValue(8, date);
	}

	public void setProcessingTime(Date time) throws BSPException {
		dataContainer.setValue(9, new SimpleDateFormat("HHMM").format(time));
	}

	public void setCountryCode(String countryCode) throws BSPException {
		dataContainer.setValue(10, countryCode);
	}

	public void setFileSeqNo(String fileSqNum) throws BSPException {
		dataContainer.setValue(11, fileSqNum);
	}

	public void setReserveSpace(String reserveSpace) throws BSPException {
		dataContainer.setValue(12, reserveSpace);
	}
}
