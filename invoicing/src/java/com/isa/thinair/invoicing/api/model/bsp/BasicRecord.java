package com.isa.thinair.invoicing.api.model.bsp;

import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

public class BasicRecord implements DISHGeneratable {

	protected OrderedElementContainer dataContainer = null;
	protected DISH_RECORD_TYPE recordType = null;

	public BasicRecord(DISH_FILE_TYPE fileType, DISH_RECORD_TYPE recordType) throws BSPException {
		this.recordType = recordType;
		dataContainer = RecordFactory.getInstance().getOrderedElementContainer(fileType, recordType);
		dataContainer.setValue(1, recordType.recordId());
	}

	public BasicRecord(DISH_FILE_TYPE fileType, DISH_RECORD_TYPE recordType, String name) throws BSPException {
		this.recordType = recordType;
		dataContainer = RecordFactory.getInstance().getOrderedElementContainer(fileType, recordType);
		dataContainer.setValue(1, name);
	}

	public BasicRecord(DISH_FILE_TYPE fileType, DISH_RECORD_TYPE recordType, String name, Long numQulifier) throws BSPException {
		this.recordType = recordType;
		dataContainer = RecordFactory.getInstance().getOrderedElementContainer(fileType, recordType);
		dataContainer.setValue(1, name);
		dataContainer.setValue(3, numQulifier);
	}

	@Override
	public String getDISHFmtData() {
		return dataContainer.getDISHFmtData();
	}

	@Override
	public boolean isValidDISHFormat() {
		return dataContainer.isValidDISHFormat();
	}

}