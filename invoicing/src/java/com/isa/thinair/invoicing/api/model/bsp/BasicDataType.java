package com.isa.thinair.invoicing.api.model.bsp;

import java.util.Locale;

public abstract class BasicDataType implements DISHGeneratable {
	protected int length;
	protected int startPosition;
	protected DATA_F dataField = null;
	protected String data = null;

	public enum DATA_F {
		MANDATORY, CONDITIONAL
	};

	public BasicDataType(int maxLength) {
		this.length = maxLength;
	}

	public BasicDataType(int startPosition, int maxLength, DATA_F field) {
		this.length = maxLength;
		this.startPosition = startPosition;
		this.dataField = field;
	}

	public void setData(String data, boolean trim) throws BSPException {
		if (data != null) {
			if (!trim && data.length() > length) {
				throw new BSPException("Invalid length for data " + data + " [actual:" + data.length() + " expected: " + length
						+ "]");
			} else if (trim && data.length() > length) {
				data = data.substring(0, length + 1);
			}

			this.data = data;
		}
	}

	public void setData(String data) throws BSPException {
		setData(data, false);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		if (this instanceof Numeric) {
			sb.append("Numeric[");
		} else if (this instanceof AlphaNumeric) {
			sb.append("AlphaNumeric[");
		} else if (this instanceof Alpha) {
			sb.append("Alpha[");
		} else {
			sb.append("Undefined[");
		}
		sb.append(data + "," + length + "," + startPosition + "," + (dataField == null ? "null" : dataField.toString()) + "]");
		return sb.toString();
	}

	@Override
	public String getDISHFmtData() {
		StringBuilder sb = new StringBuilder();
		String fmtData = data;
		if (fmtData == null) {
			fmtData = "";
		}
		fmtData = fmtData.toUpperCase(Locale.ENGLISH);
		String appender = "";
		boolean numeric = false;
		if (this instanceof Numeric) {
			appender = "0";
			numeric = true;
		} else {
			appender = " ";
		}
		if (fmtData.length() < length) {
			if (!numeric) {
				sb.append(fmtData);
			}
			for (int i = 0; i < (length - fmtData.length()); i++) {
				sb.append(appender);
			}
			if (numeric) {
				sb.append(fmtData);
			}
		} else {
			sb.append(fmtData);
		}
		return sb.toString();
	}

	@Override
	public boolean isValidDISHFormat() {
		return true;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getStartPosition() {
		return startPosition;
	}

	public void setStartPosition(int startPosition) {
		this.startPosition = startPosition;
	}

	public DATA_F getDataField() {
		return dataField;
	}

	public void setDataField(DATA_F dataField) {
		this.dataField = dataField;
	}

	public String getData() {
		return data;
	}
}
