package com.isa.thinair.invoicing.api.model.bsp;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.isa.thinair.invoicing.api.model.bsp.BSPTransactionDataDTO.TransactionType;
import com.isa.thinair.invoicing.api.model.bsp.EnumTypes.OPERATION_STATUS;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.filetypes.BKTT;
import com.isa.thinair.invoicing.api.model.bsp.filetypes.BasicFileType;
import com.isa.thinair.invoicing.api.model.bsp.filetypes.CANX;
import com.isa.thinair.invoicing.api.model.bsp.filetypes.EMTHOT;
import com.isa.thinair.invoicing.api.model.bsp.filetypes.RFND;
import com.isa.thinair.invoicing.api.model.bsp.records.BCT95_BillingAnalysisCurrencyType;
import com.isa.thinair.invoicing.api.model.bsp.records.BFT99_FileTotalCurrencyType;
import com.isa.thinair.invoicing.api.model.bsp.records.BOT93_TransactionCurrencyType;
import com.isa.thinair.invoicing.api.model.bsp.records.BOT94_CurrencyType;
import com.isa.thinair.invoicing.api.model.bsp.records.DFH01_FileHeader;
import com.isa.thinair.invoicing.api.model.bsp.records.DFH02_FileHeader;
import com.isa.thinair.invoicing.api.model.bsp.records.DFH03_FileHeader;

public class HotFileBuilder implements DISHGeneratable {

	private final Long HANDBOOK_REV_NO = 201L;
	private final String REPORTING_SYS_ID = "WEBL";
	private final String FILE_FORMAT = "ew";
	private final OPERATION_STATUS OP_STAT = OPERATION_STATUS.PROD;
	private final String WEBLINK = "WEBLINK";
	private List<DISHStructured> files = new ArrayList<DISHStructured>();
	private long tranxId = 1L;
	private String countryCode;
	private Date processingDate;
	private Date reportingPeriodEndDate;
	private Long revVersion;

	public HotFileBuilder(String countryCode, Date processingDate, Date reportingPeriodEndDate, Long revVersion) {
		this.countryCode = countryCode;
		this.processingDate = processingDate;
		this.reportingPeriodEndDate = reportingPeriodEndDate;
		this.revVersion = revVersion;
	}

	private void addElement(DISHStructured structuredItem) {
		structuredItem.setTrnxId(tranxId++);
		files.add(structuredItem);
	}

	private DFH01_FileHeader getFileHeader(DISH_FILE_TYPE fileType) throws BSPException {
		DFH01_FileHeader it01 = new DFH01_FileHeader(fileType);
		it01.setSequenceNumber(00000001l);
		it01.setStandardNumQulifier(01l);
		it01.setBSPIdentifier("SYR");
		it01.setAirlineCode("070");
		it01.setHandbookRevNo(203l);
		it01.setTestProdStatus(OP_STAT);
		it01.setCountryCode(countryCode);
		it01.setProcessingTime(processingDate);
		it01.setProcessingDate(processingDate);
		// TODO Please check this and uncomment
		//it01.setFileSeqNo(000001l);
		it01.setReserveSpace(new StringBuilder(90).toString());
		return it01;
	}

	private DFH02_FileHeader getFileHeader2(DISH_FILE_TYPE fileType) throws BSPException {
		DFH02_FileHeader it01 = new DFH02_FileHeader(fileType);
		it01.setSequenceNumber(00000002l);
		it01.setStandardNumQulifier(02l);
		it01.setAirlineCode("070");
		it01.setHandbookRevNo(revVersion);
		it01.setTestProdStatus(OP_STAT);
		it01.setCountryCode(countryCode);
		it01.setProcessingTime(processingDate);
		it01.setProcessingDate(processingDate);
		it01.setReserveSpace("");
		return it01;
	}

	private DFH03_FileHeader getFileHeader3(DISH_FILE_TYPE fileType) throws BSPException {
		DFH03_FileHeader it01 = new DFH03_FileHeader(fileType);
		it01.setSequenceNumber(00000003l);
		it01.setStandardNumQulifier(03l);
		it01.setAgentNumericCode(000000002l);
		it01.setRemitannceEndDate(reportingPeriodEndDate);
		it01.setCurrencyType("SYP0");

		it01.setReserveSpace("");
		return it01;
	}

	private BOT93_TransactionCurrencyType getBOT93FileTrailer(DISH_FILE_TYPE fileType) throws BSPException {
		BOT93_TransactionCurrencyType bot93 = new BOT93_TransactionCurrencyType(fileType);

		// adding 1 due to the trailer record
		// bot93.setCurrencyType("SYP0");
		return bot93;
	}

	private BOT94_CurrencyType getBOT94FileTrailer(DISH_FILE_TYPE fileType) throws BSPException {
		BOT94_CurrencyType bot94 = new BOT94_CurrencyType(fileType);

		// adding 1 due to the trailer record
		// bot93.setCurrencyType("SYP0");
		return bot94;
	}

	private BCT95_BillingAnalysisCurrencyType getBCT95FileTrailer(DISH_FILE_TYPE fileType) throws BSPException {
		BCT95_BillingAnalysisCurrencyType bct95 = new BCT95_BillingAnalysisCurrencyType(fileType);

		// adding 1 due to the trailer record
		// bot93.setCurrencyType("SYP0");
		return bct95;
	}

	private BFT99_FileTotalCurrencyType getBFT99FileTrailer(DISH_FILE_TYPE fileType) throws BSPException {
		BFT99_FileTotalCurrencyType bft99 = new BFT99_FileTotalCurrencyType(fileType);

		// adding 1 due to the trailer record
		// bot93.setCurrencyType("SYP0");
		return bft99;
	}

	public BKTT getNewBKTTInstance() {
		BKTT tktt = new BKTT();
		addElement(tktt);
		return tktt;
	}

	public RFND getNewRFNDInstance() {
		RFND rfnd = new RFND();
		addElement(rfnd);
		return rfnd;
	}

	public CANX getNewCANXInstance() {
		CANX canx = new CANX();
		addElement(canx);
		return canx;
	}

	private EMTHOT getNewEmptyInstance() {
		EMTHOT emty = new EMTHOT();
		addElement(emty);
		return emty;
	}

	@Override
	public String getDISHFmtData() throws BSPException {
		return null;
	}

	public String getDISHFmtData(HotFileHeaderFooterDataDTO data) throws BSPException {
		StringBuffer sb = new StringBuffer();
		int i = 1;
		if (files.size() > 0) {
			for (DISHStructured element : files) {
				if (i == 1) {
					DISHStructured elementHeader = new BKTT();

					DFH01_FileHeader fh = getFileHeader(element.getFileType());

					elementHeader.setDFHFileHeader(fh);
					DFH02_FileHeader fh2 = getFileHeader2(element.getFileType());
					elementHeader.setDFH2FileHeader(fh2);
					DFH03_FileHeader fh3 = getFileHeader3(element.getFileType());
					elementHeader.setDFH3FileHeader(fh3);
					sb.append(elementHeader.getDISHFmtData());
					sb.append("\n");
				} else {
					sb.append("\n");
				}
				if (i == files.size()) {
					BOT93_TransactionCurrencyType ft = getBOT93FileTrailer(element.getFileType());
					ft.setTransactionNo(data.getSequenceNo());
					ft.setCurrencyType(data.getPayCurrencyCode());
					ft.setTranactionCode(TransactionType.TKTT.name());
					element.setBOT93FileHeader(ft);
					// element.setBOT93FileHeader(ft);

					BOT94_CurrencyType ft94 = getBOT94FileTrailer(element.getFileType());
					ft94.setTransactionNo(data.getSequenceNo());
					ft94.setCurrencyType(data.getPayCurrencyCode());
					ft94.setGrossValueAmount(data.getTotalFare());
					ft94.setTaxFeeAmount(data.getTotalTaxAmount());
					element.setBOT94FileHeader(ft94);
					// element.setBOT94FileHeader(ft94);

					BCT95_BillingAnalysisCurrencyType bct95 = getBCT95FileTrailer(element.getFileType());
					bct95.setTransactionNo(data.getSequenceNo());
					bct95.setGrossValueAmount(data.getTotalFare());
					bct95.setTaxFeeAmount(data.getTotalTaxAmount());
					bct95.setCurrencyType(data.getPayCurrencyCode());
					element.setBCT95FileHeader(bct95);

					BFT99_FileTotalCurrencyType bft99 = getBFT99FileTrailer(element.getFileType());
					bft99.setTransactionNo(data.getSequenceNo());
					bft99.setGrossValueAmount(data.getTotalFare());
					bft99.setTaxFeeAmount(data.getTotalTaxAmount());
					bft99.setCurrencyType(data.getPayCurrencyCode());
					element.setBFT99FileHeader(bft99);
				}
				sb.append(element.getDISHFmtData());
				i++;
			}
		} else if (files.size() == 0) {
			DISHStructured element = getNewEmptyInstance();
			DFH01_FileHeader fh = getFileHeader(element.getFileType());
			element.setDFHFileHeader(fh);
			DFH02_FileHeader fh2 = getFileHeader2(element.getFileType());
			element.setDFH2FileHeader(fh2);
			DFH03_FileHeader fh3 = getFileHeader3(element.getFileType());
			element.setDFH3FileHeader(fh3);

			BOT93_TransactionCurrencyType ft = getBOT93FileTrailer(element.getFileType());
			element.setBOT93FileHeader(ft);
			sb.append(element.getDISHFmtData());
		}

		return sb.toString();
	}

	@Override
	public boolean isValidDISHFormat() {
		// TODO Auto-generated method stub
		return false;
	}

	public String getRETFileName(int airlineCode, int seqNo) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		return this.countryCode + FILE_FORMAT + WEBLINK + "_" + sdf.format(this.processingDate) + "_" + airlineCode
				+ (airlineCode % 7) + "_" + String.format("%03d", seqNo);
	}

	public List<DISHStructured> getFiles() {
		return files;
	}

	public void setFiles(List<DISHStructured> files) {
		this.files = files;
	}

	public String getCountryCode() {
		return countryCode;
	}

	private int getRecordCount() {
		int recordCount = 0;
		for (DISHStructured file : files) {
			BasicFileType basicFileType = (BasicFileType) file;
			recordCount = recordCount + basicFileType.getNoOfRecords();
		}
		return recordCount;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((countryCode == null) ? 0 : countryCode.hashCode());
		result = prime * result + ((processingDate == null) ? 0 : processingDate.hashCode());
		result = prime * result + ((reportingPeriodEndDate == null) ? 0 : reportingPeriodEndDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HotFileBuilder other = (HotFileBuilder) obj;
		if (countryCode == null) {
			if (other.countryCode != null)
				return false;
		} else if (!countryCode.equals(other.countryCode))
			return false;
		if (processingDate == null) {
			if (other.processingDate != null)
				return false;
		} else if (!processingDate.equals(other.processingDate))
			return false;
		if (reportingPeriodEndDate == null) {
			if (other.reportingPeriodEndDate != null)
				return false;
		} else if (!reportingPeriodEndDate.equals(other.reportingPeriodEndDate))
			return false;
		return true;
	}
}
