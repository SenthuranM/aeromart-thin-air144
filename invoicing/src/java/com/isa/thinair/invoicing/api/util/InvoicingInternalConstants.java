/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 *
 * Use is subjected to license terms.
 *
 * ===============================================================================
 */
package com.isa.thinair.invoicing.api.util;

/**
 * To keep track of AirTravelAgent Internal Constants
 * 
 * @author Byorn John
 * @since 1.0
 */
public class InvoicingInternalConstants {

	/** Denotes Transfer Status's to Interface Tables */
	public static interface TransferStatus {
		/** Denotes credit transaction type */
		public static final String TRANSFERED_SUCCESSFULY = "Y";

		/** Denotes debit transaction type */
		public static final String NOT_TRANSFERED = "N";

		public static final String IN_PROGRESS = "P";

	}

	public static interface InvoiceAttachment {

		public static final String OPTION_VALUE = "PDF";

		public static final String ID = "UC_REPM_008";

		public static final String INVOICE_DETAIL_JASPER_TEMPLATE = "InvoiceDetailViewReport.jasper";

		public static final String INVOICE_DETAIL_BASIC_JASPER_TEMPLATE = "InvoiceDetailReport.jasper";

		public static final String INVOICE_SUMMARY_JASPER_TEMPLATE = "InvoiceSummaryAttachment.jasper";

		public static final String INVOICE_SUMMARY_JASPER_BASIC_TEMPLATE = "InvoiceSummaryAttachmentBasic.jasper";

		public static final String INVOICE_DETAIL_BY_CURRENCY_JASPER_TEMPLATE = "InvoiceDetailReportByCurrency.jasper";

		public static final String INVOICE_SUMMARY_BY_CURRENCY_JASPER_TEMPLATE = "InvoiceSummaryAttachmentByCurrency.jasper";

		public static final String INVOICE_SUMMARY_BY_TRANSACTION_JASPER_TEMPLATE = "InvoiceSummaryReceiptAttachment.jasper";

	}

	public static interface EmailStatus {

		public static final String NOT_SENT = "NO";
		public static final String SENT = "YES";
		public static final String PROCESSING = "PRO";

	}

	/** Denotes Invoices's Status's to Interface Tables */
	public static interface InvoiceStatus {
		/** Denotes credit transaction type */
		public static final String INVOICE_SETTLED = "SETTLED";

		/** Denotes debit transaction type */
		public static final String INVOICE_UNSETTLED = "UNSETTLED";
	}

	/** Denotes Agents payment Methods */
	public static interface AgentPaymentMethods {
		public static final String ON_ACCOUNT = "OA";

	}

	public static interface BSPErrorLogStatus {
		public static final String ERROR = "ERROR";
		public static final String RESUBMIT = "RESUBMIT";
		public static final String SUBMITTED = "SUBMITTED";
	}
	
	public static interface Entity {
		public static final String ENTITY_RESERVATION = "E_RES";
	}

}
