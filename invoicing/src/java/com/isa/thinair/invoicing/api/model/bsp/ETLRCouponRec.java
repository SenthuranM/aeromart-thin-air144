package com.isa.thinair.invoicing.api.model.bsp;

import java.math.BigDecimal;
import java.util.Date;

public class ETLRCouponRec {

	private String eTicketNumber;

	private int eTicketVersion;

	private Date modifiedDate;

	private Date createdDate;

	private String paxTypeCode;

	private String firstName;

	private String lastName;

	private String paxID;

	private String couponNumber;

	private String couponStatus;

	private String segmentCode;

	private Date depatureDate;

	private String returnFlag;

	private String pnr;

	private String paymentCurrencyCode;

	private BigDecimal totalSurchargeAmount;

	private BigDecimal taxAmount;

	private BigDecimal fareAmount;

	private String agentTypeCode;

	private String agentCountaryCode;

	private String agentID;

	private String flightNumber;

	private String classofService;

	public String geteTicketNumber() {
		return eTicketNumber;
	}

	public void seteTicketNumber(String eTicketNumber) {
		this.eTicketNumber = eTicketNumber;
	}

	public int geteTicketVersion() {
		return eTicketVersion;
	}

	public void seteTicketVersion(int eTicketVersion) {
		this.eTicketVersion = eTicketVersion;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getPaxTypeCode() {
		return paxTypeCode;
	}

	public void setPaxTypeCode(String paxTypeCode) {
		this.paxTypeCode = paxTypeCode;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPaxID() {
		return paxID;
	}

	public void setPaxID(String paxID) {
		this.paxID = paxID;
	}

	public String getCouponNumber() {
		return couponNumber;
	}

	public void setCouponNumber(String couponNumber) {
		this.couponNumber = couponNumber;
	}

	public String getCouponStatus() {
		return couponStatus;
	}

	public void setCouponStatus(String couponStatus) {
		this.couponStatus = couponStatus;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public Date getDepatureDate() {
		return depatureDate;
	}

	public void setDepatureDate(Date depatureDate) {
		this.depatureDate = depatureDate;
	}

	public String getReturnFlag() {
		return returnFlag;
	}

	public void setReturnFlag(String returnFlag) {
		this.returnFlag = returnFlag;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public String getPaymentCurrencyCode() {
		return paymentCurrencyCode;
	}

	public void setPaymentCurrencyCode(String paymentCurrencyCode) {
		this.paymentCurrencyCode = paymentCurrencyCode;
	}

	public BigDecimal getTotalSurchargeAmount() {
		return totalSurchargeAmount;
	}

	public void setTotalSurchargeAmount(BigDecimal totalSurchargeAmount) {
		this.totalSurchargeAmount = totalSurchargeAmount;
	}

	public BigDecimal getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(BigDecimal taxAmount) {
		this.taxAmount = taxAmount;
	}

	public BigDecimal getFareAmount() {
		return fareAmount;
	}

	public void setFareAmount(BigDecimal fareAmount) {
		this.fareAmount = fareAmount;
	}

	public String getAgentTypeCode() {
		return agentTypeCode;
	}

	public void setAgentTypeCode(String agentTypeCode) {
		this.agentTypeCode = agentTypeCode;
	}

	public String getAgentCountaryCode() {
		return agentCountaryCode;
	}

	public void setAgentCountaryCode(String agentCountaryCode) {
		this.agentCountaryCode = agentCountaryCode;
	}

	public String getAgentID() {
		return agentID;
	}

	public void setAgentID(String agentID) {
		this.agentID = agentID;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getClassofService() {
		return classofService;
	}

	public void setClassofService(String classofService) {
		this.classofService = classofService;
	}

}