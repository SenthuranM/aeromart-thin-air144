package com.isa.thinair.invoicing.api.model.bsp.filetypes;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicRecord;
import com.isa.thinair.invoicing.api.model.bsp.DISHStructured;
import com.isa.thinair.invoicing.api.model.bsp.OrderedRecordContainer;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.records.BCT95_BillingAnalysisCurrencyType;
import com.isa.thinair.invoicing.api.model.bsp.records.BFT99_FileTotalCurrencyType;
import com.isa.thinair.invoicing.api.model.bsp.records.BKT06_TransactionHeader;
import com.isa.thinair.invoicing.api.model.bsp.records.BOT93_TransactionCurrencyType;
import com.isa.thinair.invoicing.api.model.bsp.records.BOT94_CurrencyType;
import com.isa.thinair.invoicing.api.model.bsp.records.DFH01_FileHeader;
import com.isa.thinair.invoicing.api.model.bsp.records.DFH02_FileHeader;
import com.isa.thinair.invoicing.api.model.bsp.records.DFH03_FileHeader;
import com.isa.thinair.invoicing.api.model.bsp.records.DishTransactional;
import com.isa.thinair.invoicing.api.model.bsp.records.IT01_FileHeader;
import com.isa.thinair.invoicing.api.model.bsp.records.IT02_BasicTransaction;
import com.isa.thinair.invoicing.api.model.bsp.records.IT0Z_FileTrailer;

abstract public class BasicFileType implements DISHStructured {

	private Long tnxId;
	private DISH_FILE_TYPE dishFileType;
	private OrderedRecordContainer orc = null;

	public BasicFileType(DISH_FILE_TYPE dishFileType) {
		super();
		this.dishFileType = dishFileType;
		orc = new OrderedRecordContainer(dishFileType);
	}

	public Object createNewRecord(Class<? extends BasicRecord> recordClass) throws BSPException {
		try {
			Constructor<?> matchingConstructor = null;
			for (Constructor<?> constructor : recordClass.getConstructors()) {
				if (constructor.getParameterTypes().length == 1 && constructor.getParameterTypes()[0].isEnum()
						&& constructor.getParameterTypes()[0].getName().equals(DISH_FILE_TYPE.class.getName())) {
					matchingConstructor = constructor;
					break;
				}
			}
			Object obj = matchingConstructor.newInstance(getFileType());
			Method targetMethod = null;
			for (Method method : this.getClass().getMethods()) {
				if (method.getParameterTypes().length == 1) {
					if (method.getParameterTypes()[0].getName().equals(matchingConstructor.getDeclaringClass().getName())) {
						targetMethod = method;
						break;
					}
				}
			}
			targetMethod.invoke(this, obj);
			return obj;
		} catch (IllegalArgumentException e) {
			throw new BSPException("Illegal argument ", e);
		} catch (SecurityException e) {
			throw new BSPException("Security violated ", e);
		} catch (InstantiationException e) {
			throw new BSPException("Instantiation failed ", e);
		} catch (IllegalAccessException e) {
			throw new BSPException("Illegal access ", e);
		} catch (InvocationTargetException e) {
			throw new BSPException("Invocation target error", e);
		}
	}

	protected void setTransactionId(DishTransactional tnxObj) throws BSPException {
		tnxObj.setTransactionNo(this.tnxId);
	}

	protected void addElement(BasicRecord element) throws BSPException {
		if (element != null) {
			orc.addElement(element);
		}

	}

	protected void removeElement(DISH_RECORD_TYPE recordType) throws BSPException {
		orc.removeElement(recordType);
	}

	public DISH_FILE_TYPE getFileType() {
		return dishFileType;
	}

	@Override
	public void setTrnxId(Long tnxId) {
		this.tnxId = tnxId;
	}

	public void setFileHeader(IT01_FileHeader fileHeader) throws BSPException {
		if (fileHeader == null) {
			removeElement(DISH_RECORD_TYPE.IT01);
		} else {
			addElement(fileHeader);
		}
	}

	public void setDFHFileHeader(DFH01_FileHeader fileHeader) throws BSPException {
		if (fileHeader == null) {
			removeElement(DISH_RECORD_TYPE.DFH01);
		} else {
			addElement(fileHeader);
		}
	}

	public void setDFH2FileHeader(DFH02_FileHeader fileHeader) throws BSPException {
		if (fileHeader == null) {
			removeElement(DISH_RECORD_TYPE.DFH02);
		} else {
			addElement(fileHeader);
		}
	}

	public void setDFH3FileHeader(DFH03_FileHeader fileHeader) throws BSPException {
		if (fileHeader == null) {
			removeElement(DISH_RECORD_TYPE.DFH03);
		} else {
			addElement(fileHeader);
		}
	}

	public void setBasicTnx(IT02_BasicTransaction basicTnx) throws BSPException {
		setTransactionId(basicTnx);
		addElement(basicTnx);
	}

	public void setTransactionHeader(BKT06_TransactionHeader transHeader) throws BSPException {
		setTransactionId(transHeader);
		addElement(transHeader);
	}

	public void setFileTrailer(IT0Z_FileTrailer fileTrailer) throws BSPException {
		if (fileTrailer == null) {
			removeElement(DISH_RECORD_TYPE.IT0Z);
		} else {
			addElement(fileTrailer);
		}
	}

	public void setBOT93FileHeader(BOT93_TransactionCurrencyType bot93Trailer) throws BSPException {
		if (bot93Trailer == null) {
			removeElement(DISH_RECORD_TYPE.BOT93);
		} else {
			addElement(bot93Trailer);
		}
	}

	public void setBOT94FileHeader(BOT94_CurrencyType bot93Trailer) throws BSPException {
		if (bot93Trailer == null) {
			removeElement(DISH_RECORD_TYPE.BOT94);
		} else {
			addElement(bot93Trailer);
		}
	}

	public void setBCT95FileHeader(BCT95_BillingAnalysisCurrencyType bct95Trailer) throws BSPException {
		if (bct95Trailer == null) {
			removeElement(DISH_RECORD_TYPE.BCT95);
		} else {
			addElement(bct95Trailer);
		}
	}

	public void setBFT99FileHeader(BFT99_FileTotalCurrencyType bft99Trailer) throws BSPException {
		if (bft99Trailer == null) {
			removeElement(DISH_RECORD_TYPE.BFT99);
		} else {
			addElement(bft99Trailer);
		}
	}

	@Override
	public String getDISHFmtData() {
		return orc.getDISHFmtData();
	}

	@Override
	public boolean isValidDISHFormat() {
		return orc.isValidDISHFormat();
	}

	public int getNoOfRecords() {
		return orc.getNoOfRecords();
	}
}