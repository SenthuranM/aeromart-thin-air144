package com.isa.thinair.invoicing.api.model.bsp.records;

import java.util.Date;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicBKRecord;
import com.isa.thinair.invoicing.api.model.bsp.ElementFactory.FMT_ID;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

public class BKS30_DocumentAmount extends BasicBKRecord implements DishTransactional {

	public BKS30_DocumentAmount(DISH_FILE_TYPE fileType) throws BSPException {
		super(fileType, DISH_RECORD_TYPE.BKS30, "BKS", 30l);
		// setTransactionCode(fileType);
		// setFormatIdentifier(fileType.getFormatId());
	}

	public void setCommitionAmount(Long amount) throws BSPException {
		dataContainer.setValue(8, amount);
	}

	public void setNetFareAmount(Long amount) throws BSPException {
		dataContainer.setValue(9, amount);
	}

	public void setTaxFeeType(String feeType) throws BSPException {
		dataContainer.setValue(11, feeType);
	}

	public void setTaxFeeAmount(Long amount) throws BSPException {
		dataContainer.setValue(12, amount);

	}

	public void setTaxFeeType1(String feeType) throws BSPException {

		dataContainer.setValue(13, feeType);
	}

	public void setTaxFeeAmount1(Long amount) throws BSPException {

		dataContainer.setValue(14, amount);
	}

	public void setAgentNumericCode(long identifier) throws BSPException {
		dataContainer.setValue(12, identifier);
	}

	public void setDocumentAmount(long amount) throws BSPException {
		dataContainer.setValue(15, amount);
	}

	public void setReservedSpace(String resSpace) throws BSPException {
		dataContainer.setValue(10, resSpace);
		dataContainer.setValue(17, resSpace);
		dataContainer.setValue(19, resSpace);
	}

	public void setDateOfIssue(Date dateOfIssue) throws BSPException {
		dataContainer.setValue(12, dateOfIssue);
	}

	public void setTransactionCode(DISH_FILE_TYPE transactionCode) throws BSPException {
		dataContainer.setValue(15, transactionCode.toString());
	}

	public void setFormatIdentifier(FMT_ID formatIdentifier) throws BSPException {
		dataContainer.setValue(15, formatIdentifier.toString());
	}

	public void setCurrencyType(String code) throws BSPException {
		dataContainer.setValue(18, code);
	}

	@Override
	public void setTransactionNo(Long tnxNo) throws BSPException {
		dataContainer.setValue(5, tnxNo);

	}

}
