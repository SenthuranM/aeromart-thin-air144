package com.isa.thinair.invoicing.api.model.bsp.records;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicRecord;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

/**
 * @author primal
 * @date Mar 10, 2014
 */
public class X00_50_Ticket_Header_Record extends BasicRecord {

	public X00_50_Ticket_Header_Record(DISH_FILE_TYPE fileType) throws BSPException {
		super(fileType, DISH_RECORD_TYPE.X00_50);
	}

	public void setDOC_CURR_CODE(String dcumentCurrencyCode) throws BSPException {
		dataContainer.setValue(1, dcumentCurrencyCode);
	}

	public void setDOC_AMT(String dcumentTicketedAmount) throws BSPException {
		dataContainer.setValue(2, dcumentTicketedAmount);
	}

}
