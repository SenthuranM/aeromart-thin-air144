package com.isa.thinair.invoicing.api.model.bsp;

import java.util.Date;

public class ETicketCoupon {

	private String couponNumber;

	private String couponStatus;

	private String segmentCode;

	private Date depatureDate;

	private String flightNumber;

	private String classofService;

	public String getCouponNumber() {
		return couponNumber;
	}

	public void setCouponNumber(String couponNumber) {
		this.couponNumber = couponNumber;
	}

	public String getCouponStatus() {
		return couponStatus;
	}

	public void setCouponStatus(String couponStatus) {
		this.couponStatus = couponStatus;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public Date getDepatureDate() {
		return depatureDate;
	}

	public void setDepatureDate(Date depatureDate) {
		this.depatureDate = depatureDate;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getClassofService() {
		return classofService;
	}

	public void setClassofService(String classofService) {
		this.classofService = classofService;
	}

}