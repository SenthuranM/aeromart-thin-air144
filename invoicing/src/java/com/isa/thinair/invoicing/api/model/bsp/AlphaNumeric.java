package com.isa.thinair.invoicing.api.model.bsp;

public class AlphaNumeric extends BasicDataType{
	public AlphaNumeric(int startPosition, int maxLength, DATA_F field) {
		super(startPosition, maxLength, field);
	}

	public AlphaNumeric(int maxLength) {
		super(maxLength);
	}

	@Override
	public void setData(String str) throws BSPException{
		setData(str, false);
	}
	
	@Override
	public void setData(String str, boolean trim) throws BSPException{
		// TODO validating alphanumeric
		super.setData(str, trim);
	}

}
