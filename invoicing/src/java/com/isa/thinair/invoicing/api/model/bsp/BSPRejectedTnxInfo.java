package com.isa.thinair.invoicing.api.model.bsp;

import java.io.Serializable;

public class BSPRejectedTnxInfo implements Serializable {
	private Integer recordID;
	
	private Integer tnxNumber;
	
	private Integer agentIATACode;
	
	private Long eTicketNumber;
	
	private String description;
	
	private String errorType;
	
	private String code;

	public Integer getRecordID() {
		return recordID;
	}

	public void setRecordID(Integer recordID) {
		this.recordID = recordID;
	}

	public Integer getTnxNumber() {
		return tnxNumber;
	}

	public void setTnxNumber(Integer tnxNumber) {
		this.tnxNumber = tnxNumber;
	}

	public Integer getAgentIATACode() {
		return agentIATACode;
	}

	public void setAgentIATACode(Integer agentCode) {
		this.agentIATACode = agentCode;
	}

	public Long geteTicketNumber() {
		return eTicketNumber;
	}

	public void seteTicketNumber(Long eTicketNumber) {
		this.eTicketNumber = eTicketNumber;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getErrorType() {
		return errorType;
	}

	public void setErrorType(String errorType) {
		this.errorType = errorType;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
}
