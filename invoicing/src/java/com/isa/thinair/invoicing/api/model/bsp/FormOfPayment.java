package com.isa.thinair.invoicing.api.model.bsp;

import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.invoicing.api.model.bsp.EnumTypes.FormOfPaymentType;

public class FormOfPayment {
	private String accountNo;
	private BigDecimal paymentAmount;
	private int decimalPlaces;
	private String approvalCode;
	private String currencyType;
	private String extendedPaymentCode;
	private FormOfPaymentType formOfPaymentType;
	private Date expiryDate;
	private String customerFileReference;
	private String creditCardCorporateContract;
	private String addressVerificationCode;
	private String sourceOfApprovalCode;
	private String transactionInformation;
	private String authorisedAmount;

	public FormOfPayment(String currencyCode, int decimalPlaces) throws BSPException {
		setCurrencyType(currencyCode, decimalPlaces);
		setDecimalPlaces(decimalPlaces);
	}

	public String getAccountNo() {
		return accountNo;
	}

	public BigDecimal getPaymentAmount() {
		return paymentAmount;
	}

	public String getApprovalCode() {
		return approvalCode;
	}

	public String getCurrencyType() {
		return currencyType;
	}

	public String getExtendedPaymentCode() {
		return extendedPaymentCode;
	}

	public String getFormOfPaymentType() {
		return formOfPaymentType.toString();
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public String getCustomerFileReference() {
		return customerFileReference;
	}

	public String getCreditCardCorporateContract() {
		return creditCardCorporateContract;
	}

	public String getAddressVerificationCode() {
		return addressVerificationCode;
	}

	public String getSourceOfApprovalCode() {
		return sourceOfApprovalCode;
	}

	public String getTransactionInformation() {
		return transactionInformation;
	}

	public String getAuthorisedAmount() {
		return authorisedAmount;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public void setPaymentAmount(BigDecimal paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public void setApprovalCode(String approvalCode) {
		this.approvalCode = approvalCode;
	}

	private void setCurrencyType(String currencyCode, int noOfDecimals) throws BSPException {
		if (noOfDecimals > 9) {
			throw new BSPException("Invalid no of decimals :" + noOfDecimals);
		}
		this.currencyType = currencyCode + noOfDecimals;
	}

	public void setExtendedPaymentCode(String extendedPaymentCode) {
		this.extendedPaymentCode = extendedPaymentCode;
	}

	public void setFormOfPaymentType(FormOfPaymentType formOfPaymentType) {
		this.formOfPaymentType = formOfPaymentType;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public void setCustomerFileReference(String customerFileReference) {
		this.customerFileReference = customerFileReference;
	}

	public void setCreditCardCorporateContract(String creditCardCorporateContract) {
		this.creditCardCorporateContract = creditCardCorporateContract;
	}

	public void setAddressVerificationCode(String addressVerificationCode) {
		this.addressVerificationCode = addressVerificationCode;
	}

	public void setSourceOfApprovalCode(String sourceOfApprovalCode) {
		this.sourceOfApprovalCode = sourceOfApprovalCode;
	}

	public void setTransactionInformation(String transactionInformation) {
		this.transactionInformation = transactionInformation;
	}

	public void setAuthorisedAmount(String authorisedAmount) {
		this.authorisedAmount = authorisedAmount;
	}

	private void setDecimalPlaces(int decimalPlaces) {
		this.decimalPlaces = decimalPlaces;
	}

	public int getDecimalPlaces() {
		return decimalPlaces;
	}

}
