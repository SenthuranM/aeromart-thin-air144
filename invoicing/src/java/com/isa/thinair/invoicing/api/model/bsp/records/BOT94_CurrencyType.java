package com.isa.thinair.invoicing.api.model.bsp.records;

import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicRecord;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

public class BOT94_CurrencyType extends BasicRecord implements DishTransactional {

	public BOT94_CurrencyType(DISH_FILE_TYPE fileType) throws BSPException {
		super(fileType, DISH_RECORD_TYPE.BOT94, "BOT", 94l);
		// setTransactionCode(fileType);
		// setFormatIdentifier(fileType.getFormatId());
	}

	@Override
	public void setTransactionNo(Long tnxNo) throws BSPException {
		dataContainer.setValue(2, tnxNo);

	}

	public void setStandardNumericQulaifier(String numQualifier) throws BSPException {
		dataContainer.setValue(3, numQualifier);
	}

	public void setAgentNumericCode(String code) throws BSPException {
		dataContainer.setValue(4, code);
	}

	public void setRemitanceEndingDate(Date date) throws BSPException {
		dataContainer.setValue(5, date);
	}

	public void setReservedSpace(String data) throws BSPException {
		dataContainer.setValue(6, data);
		dataContainer.setValue(12, data);
		dataContainer.setValue(15, data);

	}

	public void setGrossValueAmount(BigDecimal amount) throws BSPException {
		dataContainer.setValue(7, amount);
	}

	public void setRemittanceAmount(BigDecimal value) throws BSPException {
		dataContainer.setValue(8, value);
	}

	public void setTotalCommitionAmount(BigDecimal value) throws BSPException {
		dataContainer.setValue(9, value);
	}

	public void setTaxFeeAmount(BigDecimal value) throws BSPException {
		dataContainer.setValue(10, value);
	}

	public void setTotalLateReportingPenalty(BigDecimal value) throws BSPException {
		dataContainer.setValue(11, value);
	}

	public void setTaxComitionAmount(BigDecimal amount) throws BSPException {
		dataContainer.setValue(13, amount);
	}

	public void setCurrencyType(String type) throws BSPException {
		dataContainer.setValue(14, type);
	}

}