/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.invoicing.api.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.isa.thinair.commons.core.framework.Persistent;

public class BSPResultsDTO extends Persistent implements Serializable {

	private String invoiceNumber;

	private Date salesPeriodFrom;

	private Date salesPeriodTo;

	private BigDecimal paymentAmount;

	private BigDecimal paymentLocalAmount;

	private String agentCode;

	private String agentIATACode;

	private String paymentCurrency;

	private String country;

	private String processStatus;

	private int salesID;

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public Date getSalesPeriodFrom() {
		return salesPeriodFrom;
	}

	public void setSalesPeriodFrom(Date salesPeriodFrom) {
		this.salesPeriodFrom = salesPeriodFrom;
	}

	public Date getSalesPeriodTo() {
		return salesPeriodTo;
	}

	public void setSalesPeriodTo(Date salesPeriodTo) {
		this.salesPeriodTo = salesPeriodTo;
	}

	public BigDecimal getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(BigDecimal paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public BigDecimal getPaymentLocalAmount() {
		return paymentLocalAmount;
	}

	public void setPaymentLocalAmount(BigDecimal paymentLocalAmount) {
		this.paymentLocalAmount = paymentLocalAmount;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getAgentIATACode() {
		return agentIATACode;
	}

	public void setAgentIATACode(String agentIATACode) {
		this.agentIATACode = agentIATACode;
	}

	public String getPaymentCurrency() {
		return paymentCurrency;
	}

	public void setPaymentCurrency(String paymentCurrency) {
		this.paymentCurrency = paymentCurrency;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getProcessStatus() {
		return processStatus;
	}

	public void setProcessStatus(String processStatus) {
		this.processStatus = processStatus;
	}

	public int getSalesID() {
		return salesID;
	}

	public void setSalesID(int salesID) {
		this.salesID = salesID;
	}
}