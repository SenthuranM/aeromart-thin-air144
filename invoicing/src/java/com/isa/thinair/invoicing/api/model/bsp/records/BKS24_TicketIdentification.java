package com.isa.thinair.invoicing.api.model.bsp.records;

import java.util.Date;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicBKRecord;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

public class BKS24_TicketIdentification extends BasicBKRecord implements DishTransactional {

	public BKS24_TicketIdentification(DISH_FILE_TYPE fileType) throws BSPException {
		super(fileType, DISH_RECORD_TYPE.BKS24, "BKS", 24l);
		// setTransactionCode(fileType);
		// setFormatIdentifier(fileType.getFormatId());
	}

	public void setTransmitionCotrolNo(String num) throws BSPException {
		dataContainer.setValue(8, num);
	}

	public void setChgeckDigit(Long digit) throws BSPException {
		dataContainer.setValue(9, digit);
	}

	public void setAgentNumericCode(long identifier) throws BSPException {
		dataContainer.setValue(12, identifier);
	}

	public void setReservedSpace(String resSpace) throws BSPException {
		dataContainer.setValue(13, resSpace);
		dataContainer.setValue(17, resSpace);
		dataContainer.setValue(19, resSpace);
	}

	public void setDateOfIssue(Date dateOfIssue) throws BSPException {
		dataContainer.setValue(12, dateOfIssue);
	}

	public void setTransactionCode(DISH_FILE_TYPE transactionCode) throws BSPException {
		dataContainer.setValue(15, transactionCode.toString());
	}

	public void setOrginDesinationCItyCode(String cityCodes) throws BSPException {
		dataContainer.setValue(16, cityCodes);
	}

}
