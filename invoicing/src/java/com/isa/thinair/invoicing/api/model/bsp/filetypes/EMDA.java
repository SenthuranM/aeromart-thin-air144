package com.isa.thinair.invoicing.api.model.bsp.filetypes;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.records.BKT06_TransactionHeader;
import com.isa.thinair.invoicing.api.model.bsp.records.IT02_BasicTransaction;
import com.isa.thinair.invoicing.api.model.bsp.records.IT02_BasicTransaction_CNJ;
import com.isa.thinair.invoicing.api.model.bsp.records.IT04_AdditionalSaleInfo;
import com.isa.thinair.invoicing.api.model.bsp.records.IT05_MonetaryAmounts;
import com.isa.thinair.invoicing.api.model.bsp.records.IT06_Itinerary;
import com.isa.thinair.invoicing.api.model.bsp.records.IT06_Itinerary_CNJ;
import com.isa.thinair.invoicing.api.model.bsp.records.IT07_FareCalculation;
import com.isa.thinair.invoicing.api.model.bsp.records.IT08_FormOfPayment;
import com.isa.thinair.invoicing.api.model.bsp.records.IT09_AdditionalInformation;
import com.isa.thinair.invoicing.api.model.bsp.records.IT0B_AdditionalInfo;
import com.isa.thinair.invoicing.api.model.bsp.records.IT0S_StockControlNumbers;
import com.isa.thinair.invoicing.api.model.bsp.records.IT0Y_AgencyData;
import com.isa.thinair.invoicing.api.model.bsp.records.X00_25_Ticket_Header_Record;
import com.isa.thinair.invoicing.api.model.bsp.records.X00_30_Ticket_Header_Record;
import com.isa.thinair.invoicing.api.model.bsp.records.X00_50_Ticket_Header_Record;
import com.isa.thinair.invoicing.api.model.bsp.records.X00_90_Ticket_Header_Record;
import com.isa.thinair.invoicing.api.model.bsp.records.X00_Mutation_Ticket_Header_Record;
import com.isa.thinair.invoicing.api.model.bsp.records.X00_Ticket_Header_Record;
import com.isa.thinair.invoicing.api.model.bsp.records.X34_Mutation_Fare_Record;
import com.isa.thinair.invoicing.api.model.bsp.records.X36_Ticket_Tax;
import com.isa.thinair.invoicing.api.model.bsp.records.X40_Commission_Data;
import com.isa.thinair.invoicing.api.model.bsp.records.X45_FOP;
import com.isa.thinair.invoicing.api.model.bsp.records.X60_Coupon;
import com.isa.thinair.invoicing.api.model.bsp.records.X70_Exchange_Records;
import com.isa.thinair.invoicing.api.model.bsp.records.X74_Recent_Exchange_Record;
import com.isa.thinair.invoicing.api.model.bsp.records.X80_Alternative_Flight_History_Record;
import com.isa.thinair.invoicing.api.model.bsp.records.X92_Coupon_Status_History_Record;
import com.isa.thinair.invoicing.api.model.bsp.records.X94_Revalidation_Record;
import com.isa.thinair.invoicing.api.model.bsp.records.XA0_Extend_Alternate_Coupon_Flight_History;

public class EMDA extends BasicFileType {

	public EMDA() {
		super(DISH_FILE_TYPE.EMDA);
	}

	public void addStockControlNos(IT0S_StockControlNumbers stockControlNos) throws BSPException {
		setTransactionId(stockControlNos);
		addElement(stockControlNos);
	}

	public void setAdditionalSalesInfo(IT04_AdditionalSaleInfo additionalSalesInfo) throws BSPException {
		setTransactionId(additionalSalesInfo);
		addElement(additionalSalesInfo);
	}

	public void addMonetaryAmts(IT05_MonetaryAmounts monetaryAmts) throws BSPException {
		setTransactionId(monetaryAmts);
		addElement(monetaryAmts);
	}

	public void addItinerary(IT06_Itinerary itinerary) throws BSPException {
		setTransactionId(itinerary);
		addElement(itinerary);
	}

	public void setBasicTnxCNJ(IT02_BasicTransaction_CNJ basicTransaction_CNJ) throws BSPException {
		setTransactionId(basicTransaction_CNJ);
		addElement(basicTransaction_CNJ);
	}

	public void setBasicTnx(IT02_BasicTransaction basicTransaction) throws BSPException {
		setTransactionId(basicTransaction);
		addElement(basicTransaction);
	}

	public void addItinerary_CNJ(IT06_Itinerary_CNJ itineraryCNJ) throws BSPException {
		setTransactionId(itineraryCNJ);
		addElement(itineraryCNJ);
	}

	public void addFareCalc(IT07_FareCalculation fareCalc) throws BSPException {
		setTransactionId(fareCalc);
		addElement(fareCalc);
	}

	public void addFormOfPayment(IT08_FormOfPayment formOfPayment) throws BSPException {
		setTransactionId(formOfPayment);
		addElement(formOfPayment);
	}

	public void addAgencyData(IT0Y_AgencyData agencyData) throws BSPException {
		setTransactionId(agencyData);
		addElement(agencyData);
	}

	public void setAdditionalInfo(IT09_AdditionalInformation additionalInfo) throws BSPException {
		setTransactionId(additionalInfo);
		addElement(additionalInfo);
	}

	public void addAdditionalInfo(IT0B_AdditionalInfo additionalInfo) throws BSPException {
		setTransactionId(additionalInfo);
		addElement(additionalInfo);
	}

	public void addTransactionHeader(BKT06_TransactionHeader transactionHeader) throws BSPException {
		setTransactionId(transactionHeader);
		addElement(transactionHeader);
	}

	public void addTransactionHeader(X00_Ticket_Header_Record transactionHeader) throws BSPException {
		setTransactionId(transactionHeader);
		addElement(transactionHeader);
	}

	public void addTransactionHeader(X00_Mutation_Ticket_Header_Record transactionHeader) throws BSPException {
		addElement(transactionHeader);
	}

	public void addTransactionHeader(X00_25_Ticket_Header_Record transactionHeader) throws BSPException {
		addElement(transactionHeader);
	}

	public void addTransactionHeader(X00_30_Ticket_Header_Record transactionHeader) throws BSPException {
		addElement(transactionHeader);
	}

	public void addTransactionHeader(X00_50_Ticket_Header_Record transactionHeader) throws BSPException {
		addElement(transactionHeader);
	}

	public void addTransactionHeader(X00_90_Ticket_Header_Record transactionHeader) throws BSPException {
		addElement(transactionHeader);
	}

	public void addTransactionHeader(X34_Mutation_Fare_Record transactionHeader) throws BSPException {
		addElement(transactionHeader);
	}

	public void addTransactionHeader(X36_Ticket_Tax transactionHeader) throws BSPException {
		addElement(transactionHeader);
	}

	public void addTransactionHeader(X40_Commission_Data transactionHeader) throws BSPException {
		addElement(transactionHeader);
	}

	public void addTransactionHeader(X45_FOP transactionHeader) throws BSPException {
		addElement(transactionHeader);
	}

	public void addTransactionHeader(X60_Coupon transactionHeader) throws BSPException {
		addElement(transactionHeader);
	}

	public void addTransactionHeader(X70_Exchange_Records transactionHeader) throws BSPException {
		addElement(transactionHeader);
	}

	public void addTransactionHeader(X74_Recent_Exchange_Record transactionHeader) throws BSPException {
		addElement(transactionHeader);
	}

	public void addTransactionHeader(X80_Alternative_Flight_History_Record transactionHeader) throws BSPException {
		addElement(transactionHeader);
	}

	public void addTransactionHeader(X92_Coupon_Status_History_Record transactionHeader) throws BSPException {
		addElement(transactionHeader);
	}

	public void addTransactionHeader(X94_Revalidation_Record transactionHeader) throws BSPException {
		addElement(transactionHeader);
	}

	public void addTransactionHeader(XA0_Extend_Alternate_Coupon_Flight_History transactionHeader) throws BSPException {
		addElement(transactionHeader);
	}

}
