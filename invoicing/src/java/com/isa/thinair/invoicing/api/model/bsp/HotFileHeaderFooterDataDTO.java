package com.isa.thinair.invoicing.api.model.bsp;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import com.isa.thinair.invoicing.api.model.bsp.BSPTransactionDataDTO.TransactionType;

public class HotFileHeaderFooterDataDTO {

	private String ticketingAirlineCode;

	private BigDecimal agentCommission;

	private String paxName;

	private String pnr;

	private BigDecimal totalFare = new BigDecimal("0");

	private BigDecimal payCurrencyAmount = new BigDecimal("0");

	private BigDecimal totalTaxAmount = new BigDecimal("0");

	private String payCurrencyCode;

	private Date dateOfIssue;

	private Map<String, BigDecimal> taxCollection;

	private TransactionType tnxType;

	private Date originalPaymentTnxDate;

	private Date remitancePeriodEndDate;

	private BigDecimal remittanceAmount = new BigDecimal("0");

	private Integer aaTnxID;

	private String aaLCCUniqueID;

	private Integer paxSequnce;

	private String aaAgentCode;

	private Long iataAgentCode;

	private Long sequenceNo;

	public String getTicketingAirlineCode() {
		return ticketingAirlineCode;
	}

	public void setTicketingAirlineCode(String ticketingAirlineCode) {
		this.ticketingAirlineCode = ticketingAirlineCode;
	}

	public BigDecimal getAgentCommission() {
		return agentCommission;
	}

	public void setAgentCommission(BigDecimal agentCommission) {
		this.agentCommission = agentCommission;
	}

	public String getPaxName() {
		return paxName;
	}

	public void setPaxName(String paxName) {
		this.paxName = paxName;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public BigDecimal getTotalFare() {
		return totalFare;
	}

	public void setTotalFare(BigDecimal totalFare) {
		this.totalFare = totalFare;
	}

	public BigDecimal getPayCurrencyAmount() {
		return payCurrencyAmount;
	}

	public void setPayCurrencyAmount(BigDecimal payCurrencyAmount) {
		this.payCurrencyAmount = payCurrencyAmount;
	}

	public String getPayCurrencyCode() {
		return payCurrencyCode;
	}

	public void setPayCurrencyCode(String payCurrencyCode) {
		this.payCurrencyCode = payCurrencyCode;
	}

	public Date getDateOfIssue() {
		return dateOfIssue;
	}

	public void setDateOfIssue(Date dateOfIssue) {
		this.dateOfIssue = dateOfIssue;
	}

	public Map<String, BigDecimal> getTaxCollection() {
		return taxCollection;
	}

	public void setTaxCollection(Map<String, BigDecimal> taxCollection) {
		this.taxCollection = taxCollection;
	}

	public TransactionType getTnxType() {
		return tnxType;
	}

	public void setTnxType(TransactionType tnxType) {
		this.tnxType = tnxType;
	}

	public Date getOriginalPaymentTnxDate() {
		return originalPaymentTnxDate;
	}

	public void setOriginalPaymentTnxDate(Date originalPaymentTnxDate) {
		this.originalPaymentTnxDate = originalPaymentTnxDate;
	}

	public Integer getAaTnxID() {
		return aaTnxID;
	}

	public void setAaTnxID(Integer aaTnxID) {
		this.aaTnxID = aaTnxID;
	}

	public String getAaLCCUniqueID() {
		return aaLCCUniqueID;
	}

	public void setAaLCCUniqueID(String aaLCCUniqueID) {
		this.aaLCCUniqueID = aaLCCUniqueID;
	}

	public Integer getPaxSequnce() {
		return paxSequnce;
	}

	public void setPaxSequnce(Integer paxSequnce) {
		this.paxSequnce = paxSequnce;
	}

	public String getAaAgentCode() {
		return aaAgentCode;
	}

	public void setAaAgentCode(String aaAgentCode) {
		this.aaAgentCode = aaAgentCode;
	}

	public Long getIataAgentCode() {
		return iataAgentCode;
	}

	public void setIataAgentCode(Long iataAgentCode) {
		this.iataAgentCode = iataAgentCode;
	}

	public Long getSequenceNo() {
		return sequenceNo;
	}

	public void setSequenceNo(Long sequenceNo) {
		this.sequenceNo = sequenceNo;
	}

	public Date getRemitancePeriodEndDate() {
		return remitancePeriodEndDate;
	}

	public void setRemitancePeriodEndDate(Date remitancePeriodEndDate) {
		this.remitancePeriodEndDate = remitancePeriodEndDate;
	}

	public BigDecimal getRemittanceAmount() {
		return remittanceAmount;
	}

	public void setRemittanceAmount(BigDecimal remittanceAmount) {
		this.remittanceAmount = remittanceAmount;
	}

	public BigDecimal getTotalTaxAmount() {
		return totalTaxAmount;
	}

	public void setTotalTaxAmount(BigDecimal totalTaxAmount) {
		this.totalTaxAmount = totalTaxAmount;
	}
}
