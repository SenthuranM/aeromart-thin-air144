/**
 * 
 */
package com.isa.thinair.invoicing.api.model.bsp;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Janaka Padukka
 *
 */
public class BSPReconciliationStatus {
	
	private Integer tnxID;
	
	private String lccUniqueTnxID;
	
	private String reconciled;
	
	private Date dpcProcessedDate;
	
	private Long primaryEticket;
	
	private Long secondaryEticket;
	
	private BigDecimal processedAmount;
	
	private String processedCurrency;
	
	private String RETFileStatus;
	
	private int paxSequence;
	
	private String externalReference;

	public Integer getTnxID() {
		return tnxID;
	}

	public void setTnxID(Integer tnxID) {
		this.tnxID = tnxID;
	}

	public String getLccUniqueTnxID() {
		return lccUniqueTnxID;
	}

	public void setLccUniqueTnxID(String lccUniqueTnxID) {
		this.lccUniqueTnxID = lccUniqueTnxID;
	}

	public String getReconciled() {
		return reconciled;
	}

	public void setReconciled(String reconciled) {
		this.reconciled = reconciled;
	}

	public Date getDpcProcessedDate() {
		return dpcProcessedDate;
	}

	public void setDpcProcessedDate(Date dpcProcessedDate) {
		this.dpcProcessedDate = dpcProcessedDate;
	}

	public Long getPrimaryEticket() {
		return primaryEticket;
	}

	public void setPrimaryEticket(Long primaryEticket) {
		this.primaryEticket = primaryEticket;
	}

	public Long getSecondaryEticket() {
		return secondaryEticket;
	}

	public void setSecondaryEticket(Long secondaryEticket) {
		this.secondaryEticket = secondaryEticket;
	}

	public BigDecimal getProcessedAmount() {
		return processedAmount;
	}

	public void setProcessedAmount(BigDecimal processedAmount) {
		this.processedAmount = processedAmount;
	}

	public String getProcessedCurrency() {
		return processedCurrency;
	}

	public void setProcessedCurrency(String processedCurrencyCode) {
		this.processedCurrency = processedCurrencyCode;
	}

	public String getRETFileStatus() {
		return RETFileStatus;
	}

	public void setRETFileStatus(String rETFileStatus) {
		RETFileStatus = rETFileStatus;
	}

	public int getPaxSequence() {
		return paxSequence;
	}

	public void setPaxSequence(int paxSequence) {
		this.paxSequence = paxSequence;
	}

	public String getExternalReference() {
		return externalReference;
	}

	public void setExternalReference(String externalReference) {
		this.externalReference = externalReference;
	}

	
}
