/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.invoicing.api.dto;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

/**
 * @author Byorn Used to Select Agents for Transfering(ONLY) to the Interface Tables, Agents Invoices will be transfered
 *         and also invoices will be emailed to the coresponding agents billing email address.
 * 
 */
public class SearchInvoicesDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7097010441600754450L;
	private int year;
	private int month;
	private int invoicePeriod;
	private boolean filterZeroInvoices = true;

	private int yearTo;
	private int monthTo;
	private int invoicePeriodTo;
	private boolean byTrasaction;
	private Date invoiceDateFrom;
	private Date invoiceDateTo;
	private Collection<String> inoviceNumbers;
	private String entity;

	/**
	 * NOTE: IF AGENT CODES IS NULL OR EMPTY MEANS ALL THE AGENTS WILL BE TAKEN to sql.
	 */
	private Collection<String> agentCodes;

	/**
	 * @return Returns the agentCodes. <String Codes in ArrayList>
	 */
	public Collection<String> getAgentCodes() {
		return agentCodes;
	}

	/**
	 * @param agentCodes
	 *            The agentCodes to set.
	 */
	public void setAgentCodes(Collection<String> agentCodes) {
		this.agentCodes = agentCodes;
	}

	/**
	 * @return Returns the invoicePeriod.
	 */
	public int getInvoicePeriod() {
		return invoicePeriod;
	}

	/**
	 * @param invoicePeriod
	 *            The invoicePeriod to set.
	 */
	public void setInvoicePeriod(int invoicePeriod) {
		this.invoicePeriod = invoicePeriod;
	}

	/**
	 * @return Returns the month.
	 */
	public int getMonth() {
		return month;
	}

	/**
	 * @param month
	 *            The month to set.
	 */
	public void setMonth(int month) {
		this.month = month;
	}

	/**
	 * @return Returns the year.
	 */
	public int getYear() {
		return year;
	}

	/**
	 * @param year
	 *            The year to set.
	 */
	public void setYear(int year) {
		this.year = year;
	}

	/**
	 * @return Returns the filterZeroInvoices.
	 */
	public boolean isFilterZeroInvoices() {
		return filterZeroInvoices;
	}

	/**
	 * @param filterZeroInvoices
	 *            The filterZeroInvoices to set.
	 */
	public void setFilterZeroInvoices(boolean filterZeroInvoices) {
		this.filterZeroInvoices = filterZeroInvoices;
	}

	/**
	 * @return Returns the invoicePeriodTo.
	 */
	public int getInvoicePeriodTo() {
		return invoicePeriodTo;
	}

	/**
	 * @param invoicePeriodTo
	 *            The invoicePeriodTo to set.
	 */
	public void setInvoicePeriodTo(int invoicePeriodTo) {
		this.invoicePeriodTo = invoicePeriodTo;
	}

	/**
	 * @return Returns the monthTo.
	 */
	public int getMonthTo() {
		return monthTo;
	}

	/**
	 * @param monthTo
	 *            The monthTo to set.
	 */
	public void setMonthTo(int monthTo) {
		this.monthTo = monthTo;
	}

	/**
	 * @return Returns the yearTo.
	 */
	public int getYearTo() {
		return yearTo;
	}

	/**
	 * @param yearTo
	 *            The yearTo to set.
	 */
	public void setYearTo(int yearTo) {
		this.yearTo = yearTo;
	}

	/**
	 * 
	 * @return
	 */
	public String getInvoiceNumberFormat() {

		String yearPart = "";

		Integer intYr = new Integer(this.year);
		String s = intYr.toString();
		yearPart = (s.substring(s.length() - 2, s.length()));

		if (this.month > 9) {
			return yearPart + this.month + "0" + this.invoicePeriod + "%";
		}
		return yearPart + "0" + this.month + "0" + this.invoicePeriod + "%";

	}

	/**
	 * @return Returns the isByTrasaction.
	 */
	public boolean isByTrasaction() {
		return byTrasaction;
	}

	/**
	 * @param isByTrasaction
	 *            The isByTrasaction to set.
	 */
	public void setByTrasaction(boolean byTrasaction) {
		this.byTrasaction = byTrasaction;
	}

	public Date getInvoiceDateTo() {
		return invoiceDateTo;
	}

	public void setInvoiceDateTo(Date invoiceDateTo) {
		this.invoiceDateTo = invoiceDateTo;
	}

	public Date getInvoiceDateFrom() {
		return invoiceDateFrom;
	}

	public void setInvoiceDateFrom(Date invoiceDateFrom) {
		this.invoiceDateFrom = invoiceDateFrom;
	}

	public Collection<String> getInoviceNumbers() {
		return inoviceNumbers;
	}

	public void setInoviceNumbers(Collection<String> inoviceNumbers) {
		this.inoviceNumbers = inoviceNumbers;
	}

	public String getEntity() {
		return entity;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}
}