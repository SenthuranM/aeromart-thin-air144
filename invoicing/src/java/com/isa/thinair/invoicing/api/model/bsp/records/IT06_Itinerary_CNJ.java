package com.isa.thinair.invoicing.api.model.bsp.records;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

public class IT06_Itinerary_CNJ extends IT06_Itinerary {

	public IT06_Itinerary_CNJ(DISH_FILE_TYPE fileType) throws BSPException {
		super(fileType, DISH_RECORD_TYPE.IT06CNJ);
	}

}
