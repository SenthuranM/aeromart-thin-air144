package com.isa.thinair.invoicing.api.model.bsp.records;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicRecord;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

public class BKT06_TransactionHeader extends BasicRecord implements DishTransactional {

	public BKT06_TransactionHeader(DISH_FILE_TYPE fileType) throws BSPException {
		super(fileType, DISH_RECORD_TYPE.BKT06, "BKT", 06l);
		// setTransactionCode(fileType);
		// setFormatIdentifier(fileType.getFormatId());
	}

	public BKT06_TransactionHeader(DISH_FILE_TYPE fileType, DISH_RECORD_TYPE recordType) throws BSPException {
		super(fileType, recordType);
		// setTransactionCode(fileType);
		// setFormatIdentifier(fileType.getFormatId());
	}

	public void setSequesnceNo(Long id) throws BSPException {
		dataContainer.setValue(2, id);
	}

	public void setStandardNumberQalifier(Long id) throws BSPException {
		dataContainer.setValue(3, id);
	}

	public void setVendorISOCountryCode(String countryCode) throws BSPException {
		dataContainer.setValue(4, countryCode);
	}

	public void setVendorIdentification(String vendorIdentification) throws BSPException {
		dataContainer.setValue(5, vendorIdentification);
	}

	public void setTransactionNo(Long transNo) throws BSPException {
		dataContainer.setValue(6, transNo);
	}

	public void setTransactionalRefNo(Long transRefNo) throws BSPException {
		dataContainer.setValue(7, transRefNo);
	}

	public void setNetReportingIndicator(String indicator) throws BSPException {
		dataContainer.setValue(8, indicator);
	}

	public void setTransactionRecordCounter(Long no) throws BSPException {
		dataContainer.setValue(8, no);
	}

	public void setFormatIdentifier(String identifer) throws BSPException {
		dataContainer.setValue(10, identifer);
	}

	public void setAuditStatusIndicator(String indicator) throws BSPException {
		dataContainer.setValue(11, indicator);
	}

	public void setAirlineCodeNumber(String no) throws BSPException {
		dataContainer.setValue(12, no);
	}

	public void setCommercialAgreementReference(String ref) throws BSPException {
		dataContainer.setValue(13, ref);
	}

	public void setCustomerFileReference(String ref) throws BSPException {
		dataContainer.setValue(14, ref);
	}

	public void setCLientIdentification(String identify) throws BSPException {
		dataContainer.setValue(15, identify);
	}

	public void setReportingSysyIdenfifier(String identify) throws BSPException {
		dataContainer.setValue(16, identify);
	}

	public void setApproveLocationNumericCode(Long numCode) throws BSPException {
		dataContainer.setValue(17, numCode);
		dataContainer.setValue(19, numCode);
		dataContainer.setValue(21, numCode);
	}

	public void setReserverdSpace(String identify) throws BSPException {
		dataContainer.setValue(24, identify);
	}

}
