package com.isa.thinair.invoicing.api.model.bsp.records;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicRecord;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

/**
 * @author primal
 * @date Mar 7, 2014
 */
public class X00_Mutation_Ticket_Header_Record extends BasicRecord {

	public X00_Mutation_Ticket_Header_Record(DISH_FILE_TYPE fileType) throws BSPException {
		super(fileType, DISH_RECORD_TYPE.X00M);
	}

	public void setIsNewTicketIndication(String isNew) throws BSPException {
		dataContainer.setValue(1, isNew);
	}

	public void setPurgeIndication(String purgeIndication) throws BSPException {
		dataContainer.setValue(2, purgeIndication);
	}

	public void setVoidIndication(String voidIndication) throws BSPException {
		dataContainer.setValue(3, voidIndication);
	}

}
