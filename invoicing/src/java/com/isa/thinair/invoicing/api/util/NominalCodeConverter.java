package com.isa.thinair.invoicing.api.util;

public class NominalCodeConverter {

	private NominalCodeConverter() {
	}


	public static String toAgentTnxNominalCode(String paxTnxNominalCode) {
		String nominalCode = null;

		if (paxTnxNominalCode.equals("19")) {
			nominalCode = "19";
		}
		else if (paxTnxNominalCode.equals("25")) {
			nominalCode = "20";
		}
		if (paxTnxNominalCode.equals("36")) {
			nominalCode = "21";
		}
		if (paxTnxNominalCode.equals("37")) {
			nominalCode = "22";
		}

		return nominalCode;
	}

}
