package com.isa.thinair.invoicing.api.model.bsp;

import java.util.ArrayList;
import java.util.List;

import com.isa.thinair.invoicing.api.model.bsp.ElementFactory.DE_STATUS;
import com.isa.thinair.invoicing.api.model.bsp.ElementFactory.DISH_ELEMENT;
import com.isa.thinair.invoicing.api.model.bsp.ElementFactory.FMT_ID;


public class ElementBuilder {
	private int currentPosition;
	private List<ElementMetaData> list = null;
	private DE_STATUS defaultStatus = null;
	private FMT_ID defaultRecordLength = null;

	public ElementBuilder(DE_STATUS status) {
		this.currentPosition = 1;
		this.list = new ArrayList<ElementMetaData>();
		this.defaultStatus = status;
		this.defaultRecordLength = FMT_ID.S;
	}

	public ElementBuilder(FMT_ID recordLength) {
		this.currentPosition = 1;
		this.list = new ArrayList<ElementMetaData>();
		this.defaultStatus = DE_STATUS.M;
		this.defaultRecordLength = recordLength;
	}

	public ElementBuilder(FMT_ID recordLength, DE_STATUS status) {
		this.currentPosition = 1;
		this.list = new ArrayList<ElementMetaData>();
		this.defaultStatus = status;
		this.defaultRecordLength = recordLength;
	}

	public ElementBuilder() {
		this.currentPosition = 1;
		this.list = new ArrayList<ElementMetaData>();
		this.defaultStatus = DE_STATUS.M;
		this.defaultRecordLength = FMT_ID.S;
	}

	private ElementBuilder(List<ElementMetaData> list) {
		this.list = list;
	}

	public void setDefaultStatus(DE_STATUS status) {
		this.defaultStatus = status;
	}

	public void setRecordLength(FMT_ID recordLength) {
		this.defaultRecordLength = recordLength;
	}

	public void addElement(DISH_ELEMENT element, int length) {
		addElement(element, defaultRecordLength, length);
	}

	public void addElement(DISH_ELEMENT element, FMT_ID recordLength, int length) {
		list.add(new ElementMetaData(currentPosition, length, element, recordLength, defaultStatus));
		currentPosition += length;
	}

	public void addElement(DISH_ELEMENT element, DE_STATUS status, int length) {
		addElement(element, defaultRecordLength, status, length);
	}

	public void addElement(DISH_ELEMENT element, FMT_ID recordLength, DE_STATUS status, int length) {
		list.add(new ElementMetaData(currentPosition, length, element, recordLength, status));
		currentPosition += length;
	}

	public void addElement(DISH_ELEMENT element, FMT_ID recordLength) {
		addElement(element, recordLength, element.getDtLength());
	}

	public void addElement(DISH_ELEMENT element) {
		addElement(element, defaultStatus, element.getDtLength());
	}

	public void addElement(DISH_ELEMENT element, DE_STATUS status) {
		addElement(element, status, element.getDtLength());
	}

	public void addElement(DISH_ELEMENT element, FMT_ID recordLength, DE_STATUS status) {
		addElement(element, recordLength, status, element.getDtLength());
	}

	/**
	 * Add elements to the same position
	 * 
	 * @param elements
	 */
	public void addElements(DISH_ELEMENT... elements) {
		for (DISH_ELEMENT element : elements) {
			list.add(new ElementMetaData(currentPosition, element.getDtLength(), element, defaultRecordLength, defaultStatus));
			currentPosition += element.getDtLength();
			break;
		}
	}

	/**
	 * Add elements to the same position
	 * 
	 * @param elements
	 */
	public void addElements(DE_STATUS status, DISH_ELEMENT... elements) {
		for (DISH_ELEMENT element : elements) {
			list.add(new ElementMetaData(currentPosition, element.getDtLength(), element, defaultRecordLength, status));
			currentPosition += element.getDtLength();
			break;
		}
	}

	/**
	 * Add elements to the same position
	 * 
	 * @param elements
	 */
	public void addElements(FMT_ID recordLength, DE_STATUS status, DISH_ELEMENT... elements) {
		for (DISH_ELEMENT element : elements) {
			list.add(new ElementMetaData(currentPosition, element.getDtLength(), element, recordLength, status));
			currentPosition += element.getDtLength();
			break;
		}
	}

	public List<ElementMetaData> getMetaDataList() {
		return list;
	}

	public void changeStatus(int index, DE_STATUS status) throws BSPException {
		if (this.list.size() < index) {
			throw new BSPException("Invalid index " + index);
		}
		this.list.get(index - 1).setStatus(status);
	}

	public ElementBuilder clone(DE_STATUS status) {
		List<ElementMetaData> clonedList = new ArrayList<ElementMetaData>();
		for (ElementMetaData md : this.list) {
			clonedList.add(md.clone(status));
		}
		return new ElementBuilder(clonedList);
	}

	@Override
	public ElementBuilder clone() {
		List<ElementMetaData> clonedList = new ArrayList<ElementMetaData>();
		for (ElementMetaData md : this.list) {
			clonedList.add(md.clone());
		}
		return new ElementBuilder(clonedList);
	}
}