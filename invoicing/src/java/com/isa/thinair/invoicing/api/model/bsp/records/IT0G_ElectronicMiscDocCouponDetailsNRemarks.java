package com.isa.thinair.invoicing.api.model.bsp.records;

import java.math.BigDecimal;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicRecord;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

public class IT0G_ElectronicMiscDocCouponDetailsNRemarks extends BasicRecord implements DishTransactional {
	public IT0G_ElectronicMiscDocCouponDetailsNRemarks(DISH_FILE_TYPE fileType) throws BSPException {
		super(fileType, DISH_RECORD_TYPE.IT0G);
	}

	public void setTransactionNo(Long tnxNo) throws BSPException {
		dataContainer.setValue(2, tnxNo);
	}

	public void setEmdCouponNumber(Long emdCouponNumber) throws BSPException {
		dataContainer.setValue(3, emdCouponNumber);
	}

	public void setEmdCouponValue(BigDecimal emdCouponValue) throws BSPException {
		dataContainer.setValue(4, emdCouponValue);
	}

	public void setEmdCouponCurrency(String emdCouponCurrency, int decimalPlaces) throws BSPException {
		
		if (decimalPlaces > 9) {
			throw new RuntimeException("Invalid no of decimals :" + decimalPlaces);
		}
		dataContainer.setValue(5, emdCouponCurrency + decimalPlaces);
	}

	public void setEmdRelatedTicketNumber(String emdRelatedTicketNumber) throws BSPException {
		dataContainer.setValue(6, emdRelatedTicketNumber);
	}

	public void setEmdRelatedCouponNumber(Long emdRelatedCouponNumber) throws BSPException {
		dataContainer.setValue(7, emdRelatedCouponNumber);
	}

	public void setEmdReasonForIssuanceSubCode(String emdReasonForIssuanceSubCode) throws BSPException {
		dataContainer.setValue(8, emdReasonForIssuanceSubCode);
	}

	public void setEmdFeeOwnerAirlineDesignator(String emdFeeOwnerAirlineDesignator) throws BSPException {
		dataContainer.setValue(9, emdFeeOwnerAirlineDesignator);
	}

	public void setEmdExcessBaggageOverAllowanceQualifier(String emdExcessBaggageOverAllowanceQualifier) throws BSPException {
		dataContainer.setValue(10, emdExcessBaggageOverAllowanceQualifier);
	}

	public void setEmdExcessBaggageRate(String emdExcessBaggageRate) throws BSPException {
		dataContainer.setValue(11, emdExcessBaggageRate);
	}

	public void setEmdExcessBaggageTotalExcess(String emdExcessBaggageTotalExcess) throws BSPException {
		dataContainer.setValue(12, emdExcessBaggageTotalExcess);
	}

	public void setEmdRemarks(String emdRemarks) throws BSPException {
		dataContainer.setValue(13, emdRemarks);
	}

	public void setEmdConsumedAtIssuanceIndicator(String emdConsumedAtIssuanceIndicator) throws BSPException {
		dataContainer.setValue(14, emdConsumedAtIssuanceIndicator);
	}

	public void setEmdExcessBaggageCurrencyType(String emdExcessBaggageCurrencyType) throws BSPException {
		dataContainer.setValue(15, emdExcessBaggageCurrencyType);
	}

}
