package com.isa.thinair.invoicing.api.model.bsp.records;

import java.math.BigDecimal;

import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicRecord;
import com.isa.thinair.invoicing.api.model.bsp.EnumTypes.TicketingMode;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.utils.BSPApiUtils;

public class IT04_AdditionalSaleInfo extends BasicRecord implements DishTransactional {
	private final static int MAX_TAX_SETS = 3;
	private int currentTaxSetCount = 0;

	public IT04_AdditionalSaleInfo(DISH_FILE_TYPE fileType) throws BSPException {
		super(fileType, DISH_RECORD_TYPE.IT04);
	}

	public void setTransactionNo(Long tnxNo) throws BSPException {
		dataContainer.setValue(2, tnxNo);
	}

	public void setPnrReference(String pnrReference) throws BSPException {
		dataContainer.setValue(3, pnrReference);
	}

	public void setTrueOriginDestCityCodes(String trueOriginDestCityCodes) throws BSPException {
		dataContainer.setValue(4, trueOriginDestCityCodes);
	}

	public void setTicketingModeIndicator(TicketingMode ticketingModeIndicator) throws BSPException {
		dataContainer.setValue(6, ticketingModeIndicator.toString());
	}

	public void setOriginalIssueInformation(String originalIssueInformation) throws BSPException {
		dataContainer.setValue(7, originalIssueInformation);
	}

	public void setTourCode(String tourCode) throws BSPException {
		dataContainer.setValue(8, tourCode);
	}

	public void setFare(String fare) throws BSPException {
		try {
			dataContainer.setValue(9, fare);
		} catch (NumberFormatException ex) {
			throw new BSPException("Error : invalid fare " + fare);
		}
	}

	public void setEquivalentFarePaid(String equivalentFarePaid) throws BSPException {
		dataContainer.setValue(10, equivalentFarePaid);
	}

	public void addTax(String tax) throws BSPException {
		if (MAX_TAX_SETS <= currentTaxSetCount) {
			throw new RuntimeException("Max number of taxes allowed exceeded");
		}
		int offset = currentTaxSetCount;
		dataContainer.setValue(11 + offset, tax);
		currentTaxSetCount++;
	}

	public void setTotal(String total) throws BSPException {
		try {
			BigDecimal bdTotal = new BigDecimal(total);
			String value = AccelAeroCalculator.formatAsDecimal(bdTotal).replace(".", "");
			if (value.length() > 11) {
				dataContainer.setValue(14, new BigDecimal("0").toString());// to do check how to display the total with
																			// the length
				// more than 11
			} else {
				dataContainer.setValue(14, value);
			}

		} catch (NumberFormatException ex) {
			throw new BSPException("Error : invalid total " + total);
		}
	}

	public void setTotal(String currencyCode, String total) throws BSPException {
		dataContainer.setValue(14, total);
	}

	public void setNeutralTktSystemId(String neutralTktSystemId) throws BSPException {
		dataContainer.setValue(15, neutralTktSystemId);
	}

	public void setServicingAirlineId(String servicingAirlineId) throws BSPException {
		dataContainer.setValue(16, servicingAirlineId + BSPApiUtils.calculateMod7CheckBit(servicingAirlineId));
	}

	public void setClientIdentification(String clientIdentification) throws BSPException {
		dataContainer.setValue(17, clientIdentification);
	}

	public void setBookingAgentId(String bookingAgentId) throws BSPException {
		dataContainer.setValue(18, bookingAgentId);
	}

	public void setPassengerSpecificData(String passengerSpecificData) throws BSPException {
		dataContainer.setValue(19, passengerSpecificData);
		;
	}

	public void setValidationLocationCode(Long validationLocationCode) throws BSPException {
		dataContainer.setValue(20, validationLocationCode);
	}

	public void setBookingAgency(String bookingAgency) throws BSPException {
		dataContainer.setValue(21, bookingAgency);
	}

	public void setBookingEntityOutlet(String bookingEntityOutlet) throws BSPException {
		dataContainer.setValue(22, bookingEntityOutlet);
	}

}
