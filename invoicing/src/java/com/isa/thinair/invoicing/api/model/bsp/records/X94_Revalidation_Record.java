package com.isa.thinair.invoicing.api.model.bsp.records;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicRecord;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

/**
 * @author primal
 * @date Mar 12, 2014
 */
public class X94_Revalidation_Record extends BasicRecord {

	public X94_Revalidation_Record(DISH_FILE_TYPE fileType) throws BSPException {
		super(fileType, DISH_RECORD_TYPE.X94);
	}

	public void setTICKET_NUMBER(String ticketNumber) throws BSPException {
		dataContainer.setValue(1, ticketNumber);
	}

	public void setRTA_TIMESTAMP(Date ticketChangeTimeStamp) throws BSPException {
		dataContainer.setValue(2, new SimpleDateFormat("yyyyMMdd").format(ticketChangeTimeStamp));
	}

	public void setREC_TYPE(String recordType) throws BSPException {
		dataContainer.setValue(3, recordType);
	}

	public void setSEQUENCE_INDIC(String sequenceIndicator) throws BSPException {
		dataContainer.setValue(4, sequenceIndicator);
	}

	public void setRSN(String ticketCouponNumber) throws BSPException {
		dataContainer.setValue(5, ticketCouponNumber);
	}

	public void setRSS(String secondarySequenceNumber) throws BSPException {
		dataContainer.setValue(6, secondarySequenceNumber);
	}

	public void setORIG_MK_CRR_CD(String originMarketingCarrierCode) throws BSPException {
		dataContainer.setValue(7, originMarketingCarrierCode);
	}

	public void setORIG_MK_DEP_TIME(Date depatureTime) throws BSPException {
		dataContainer.setValue(8, new SimpleDateFormat("HHmm ").format(depatureTime));
	}

	public void setORIG_MK_DEP_DATE(Date depatureDate) throws BSPException {
		dataContainer.setValue(9, new SimpleDateFormat("yyyyMMdd").format(depatureDate));
	}

	public void setORIG_MK_FLT_NBR(String originalMarketingFlightNumber) throws BSPException {
		dataContainer.setValue(10, originalMarketingFlightNumber);
	}

	public void setORIG_MK_COS(String originalMarketingClassofService) throws BSPException {
		dataContainer.setValue(11, originalMarketingClassofService);
	}

	public void setNEW_MK_CRR_CD(String newMarketingCarrierCode) throws BSPException {
		dataContainer.setValue(12, newMarketingCarrierCode);
	}

	public void setNEW_MK_DEP_TIME(Date newDepatureTime) throws BSPException {
		dataContainer.setValue(13, new SimpleDateFormat("HHMM ").format(newDepatureTime));
	}

	public void setNEW_MK_DEP_DATE(Date newDepatureDate) throws BSPException {
		dataContainer.setValue(14, new SimpleDateFormat("yyyymmdd").format(newDepatureDate));
	}

	public void setNEW_MK_FLT_NBR(String newMarketingFlightNumber) throws BSPException {
		dataContainer.setValue(15, newMarketingFlightNumber);
	}

	public void setNEW_MK_COS(String newMarketingClassofService) throws BSPException {
		dataContainer.setValue(16, newMarketingClassofService);
	}

	public void setORIG_OP_CRR_CD(String originalOperatingCarrierCode) throws BSPException {
		dataContainer.setValue(17, originalOperatingCarrierCode);
	}

	public void setORIG_OP_FLT_NBR(String originalOperatingFlightNumber) throws BSPException {
		dataContainer.setValue(18, originalOperatingFlightNumber);
	}

	public void setORIG_OP_COS(String originalOperatingClassofService) throws BSPException {
		dataContainer.setValue(19, originalOperatingClassofService);
	}

	public void setORIG_OP_CTL_CRR(String originalcarrierInControle) throws BSPException {
		dataContainer.setValue(20, originalcarrierInControle);
	}

	public void setNEW_OP_CRR_CD(String newOperatingCarrierCode) throws BSPException {
		dataContainer.setValue(21, newOperatingCarrierCode);
	}

	public void setNEW_OP_FLT_NBR(String newOperatingFlightNumber) throws BSPException {
		dataContainer.setValue(22, newOperatingFlightNumber);
	}

	public void setNEW_OP_COS(String newOperatingClassofService) throws BSPException {
		dataContainer.setValue(23, newOperatingClassofService);
	}

	public void setNEW_OP_CTL_CRR(String newcarrierInControle) throws BSPException {
		dataContainer.setValue(24, newcarrierInControle);
	}

}
