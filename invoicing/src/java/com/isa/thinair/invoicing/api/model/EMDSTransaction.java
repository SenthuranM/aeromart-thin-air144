/**
 * 
 */
package com.isa.thinair.invoicing.api.model;

import java.io.Serializable;
import java.math.BigDecimal;

import com.isa.thinair.commons.core.framework.Persistent;

/**
 * EMDSTransaction is the entity class to represent EMDS transaction records
 * generated at RET file generation
 * 
 * @hibernate.class table = "T_EMDS_TRANSACTION"
 */
public class EMDSTransaction extends Persistent implements Serializable {

	private Integer emdsID;
	private String emdsTicketNo;
	private String lccUniqueID;
	private Integer tnxID;
	private String requestForIssuanceCode;
	private String requestForIssuanceSubCode;
	private BigDecimal amount;
	private String currency;
	private String status;
	private Integer paxSequence;
	private String externalReference;

	public enum RFIC {
		A, B, C, D, E, F, G
	};

	public enum RFISC {

		UPGRADES("0BJ", "Upgrades - Air Transportation(A)", "Reservation Upgrade");
		private RFISC(String code, String name, String remark) {
			this.code = code;
			this.name = name;
			this.remark = remark;
		}

		private final String code;
		private final String name;
		private final String remark;

		public String getCode() {
			return code;
		}

		public String getName() {
			return name;
		}

		public String getRemark() {
			return remark.toUpperCase();
		}

	}
	
	public enum STATUS {
		I,R,F
	};

	/**
	 * @return Returns the emdsID.
	 * 
	 * @hibernate.id column="EMDS_ID" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "S_EMDS_TRANSACTION"
	 */
	public Integer getEmdsID() {
		return emdsID;
	}

	public void setEmdsID(Integer emdsID) {
		this.emdsID = emdsID;
	}

	/**
	 * @return Returns the emdsTicketNo.
	 * @hibernate.property column = "EMDS_TICKET_NO"
	 */
	public String getEmdsTicketNo() {
		return emdsTicketNo;
	}

	public void setEmdsTicketNo(String emdsTicketNo) {
		this.emdsTicketNo = emdsTicketNo;
	}

	/**
	 * @return Returns the lccUniqueID.
	 * @hibernate.property column = "LCC_UNIQUE_TXN_ID"
	 */
	public String getLccUniqueID() {
		return lccUniqueID;
	}

	public void setLccUniqueID(String lccUniqueID) {
		this.lccUniqueID = lccUniqueID;
	}

	/**
	 * @return Returns the tnxID.
	 * @hibernate.property column = "TXN_ID"
	 */
	public Integer getTnxID() {
		return tnxID;
	}

	public void setTnxID(Integer tnxID) {
		this.tnxID = tnxID;
	}

	/**
	 * @return Returns the requestForIssuanceCode.
	 * @hibernate.property column = "RFIC"
	 */
	public String getRequestForIssuanceCode() {
		return requestForIssuanceCode;
	}

	public void setRequestForIssuanceCode(String requestForIssuanceCode) {
		this.requestForIssuanceCode = requestForIssuanceCode;
	}

	/**
	 * @return Returns the requestForIssuanceSubCode.
	 * @hibernate.property column = "RFISC"
	 */
	public String getRequestForIssuanceSubCode() {
		return requestForIssuanceSubCode;
	}

	public void setRequestForIssuanceSubCode(String requestForIssuanceSubCode) {
		this.requestForIssuanceSubCode = requestForIssuanceSubCode;
	}

	/**
	 * @return Returns the amount.
	 * @hibernate.property column = "AMOUNT"
	 */
	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return Returns the currency.
	 * @hibernate.property column = "CURRENCY_CODE"
	 */
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return Returns the status.
	 * @hibernate.property column = "STATUS"
	 */
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return Returns the paxSequence.
	 * @hibernate.property column = "PAX_SEQUENCE"
	 */
	public Integer getPaxSequence() {
		return paxSequence;
	}

	public void setPaxSequence(Integer paxSequence) {
		this.paxSequence = paxSequence;
	}

	/**
	 * @return Returns the externalReference.
	 * @hibernate.property column = "EXT_REFERENCE"
	 */
	public String getExternalReference() {
		return externalReference;
	}

	public void setExternalReference(String externalReference) {
		this.externalReference = externalReference;
	}
	
	
}
