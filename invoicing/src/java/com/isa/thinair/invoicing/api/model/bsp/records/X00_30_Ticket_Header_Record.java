/**
 * 
 */
package com.isa.thinair.invoicing.api.model.bsp.records;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicRecord;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_RECORD_TYPE;

/**
 * @author primal
 * @date Mar 7, 2014
 */
public class X00_30_Ticket_Header_Record extends BasicRecord {

	public X00_30_Ticket_Header_Record(DISH_FILE_TYPE fileType) throws BSPException {
		super(fileType, DISH_RECORD_TYPE.X00_30);
	}

	public void setNUM_FLT_SEGS(int numberOfFlightSegments) throws BSPException {
		dataContainer.setValue(1, Integer.toString(numberOfFlightSegments));
	}

	public void setDOCUMENT_TYPE(String doctype) throws BSPException {
		dataContainer.setValue(2, doctype);
	}

	public void setTKT_CREATE_DATE(Date ticketCreatedDate) throws BSPException {
		dataContainer.setValue(3, new SimpleDateFormat("yyyyMMdd").format(ticketCreatedDate));
	}

	public void set1ST_TKT_NUMBER(String firstTicketNumber) throws BSPException {
		dataContainer.setValue(4, firstTicketNumber);
	}

	public void setLAST_TKT_NUMBER(String lastTicketNumber) throws BSPException {
		dataContainer.setValue(5, lastTicketNumber);
	}

	public void setNUM_OF_BOOKLETS(int numberOfTicket) throws BSPException {
		dataContainer.setValue(6, Integer.toString(numberOfTicket));
	}

	public void setTOUR_CODE(String ticketTourCode) throws BSPException {
		dataContainer.setValue(7, ticketTourCode);
	}

	public void setTKTNG_MODE_INDIC(String ticketModeIndicator) throws BSPException {
		dataContainer.setValue(8, ticketModeIndicator);
	}

	public void setINTL_SALES_INDIC(String internationalSalesIndicator) throws BSPException {
		dataContainer.setValue(9, internationalSalesIndicator);
	}

	public void setINTL_DOM_IND(String internationalDomesticFlightIndicator) throws BSPException {
		dataContainer.setValue(10, internationalDomesticFlightIndicator);
	}

	public void setSELF_SALE_INDIC(String selfSalesIndicator) throws BSPException {
		dataContainer.setValue(11, selfSalesIndicator);
	}

	public void setNET_RPTG_INDIC(String reportingIndicator) throws BSPException {
		dataContainer.setValue(12, reportingIndicator);
	}

	public void setTKTING_DATE(Date ticketIssuedDate) throws BSPException {
		dataContainer.setValue(13, new SimpleDateFormat("yyyyMMdd").format(ticketIssuedDate));
	}

	public void setORIG_AIRPORT(String originAirPort) throws BSPException {
		dataContainer.setValue(14, originAirPort);
	}

	public void setDEST_AIRPORT(String depatureAirPort) throws BSPException {
		dataContainer.setValue(15, depatureAirPort);
	}

	public void setRES_SYSTEM(String airLinePlating) throws BSPException {
		dataContainer.setValue(16, airLinePlating);
	}

	public void setSYMB_PNR_ADDR(String symbolicPNR) throws BSPException {
		dataContainer.setValue(17, symbolicPNR);
	}

	public void setRES_SYSTM_OAL(String otherAirLinePlating) throws BSPException {
		dataContainer.setValue(18, otherAirLinePlating);
	}

	public void setSYMB_PNR_ADDR_OAL(String otherAirlineSymbolicPNR) throws BSPException {
		dataContainer.setValue(19, otherAirlineSymbolicPNR);
	}

	public void setINVOICE_NUM(String invoiceNumber) throws BSPException {
		dataContainer.setValue(20, invoiceNumber);
	}

	public void setCLIENT_ACCT_CODE(String clientAccountCode) throws BSPException {
		dataContainer.setValue(21, clientAccountCode);
	}

	public void setPRICING_INDIC(String pricingIndicator) throws BSPException {
		dataContainer.setValue(22, pricingIndicator);
	}

	public void setCURRENCY_CODE(String currencyCodeOfTotalFare) throws BSPException {
		dataContainer.setValue(23, currencyCodeOfTotalFare);
	}

	public void setTOTAL_FARE(String totalFare) throws BSPException {
		dataContainer.setValue(24, totalFare);
	}

	public void setBF_CURR_CD(String currencyCodeOfBaseFare) throws BSPException {
		dataContainer.setValue(25, currencyCodeOfBaseFare);
	}

	public void setBF_FARE(String baseFare) throws BSPException {
		dataContainer.setValue(26, baseFare);
	}

	public void setEF_CURR_CD(String currencyCodeOfEquivalentFare) throws BSPException {
		dataContainer.setValue(27, currencyCodeOfEquivalentFare);
	}

	public void setEF_FARE(String equivalentFare) throws BSPException {
		dataContainer.setValue(28, equivalentFare);
	}

	public void setBOOKING_IATA(String bookingIATA) throws BSPException {
		dataContainer.setValue(29, bookingIATA);
	}

	public void setSYSTEM_PROVIDER(String systemProvider) throws BSPException {
		dataContainer.setValue(30, systemProvider);
	}

	public void setDELIV_SYSTEM(String deliveringSystem) throws BSPException {
		dataContainer.setValue(31, deliveringSystem);
	}

	public void setDELIV_CITY(String deliveringCity) throws BSPException {
		dataContainer.setValue(32, deliveringCity);
	}

	public void setAGY_LOCALE(String agencyLocal) throws BSPException {
		dataContainer.setValue(33, agencyLocal);
	}

	public void setRES_SYS_ID(String resSystemID) throws BSPException {
		dataContainer.setValue(34, resSystemID);
	}

	public void setPSEUDO_CITY(String psudoCity) throws BSPException {
		dataContainer.setValue(35, psudoCity);
	}

	public void setORIG_SYS(String orginatingSystem) throws BSPException {
		dataContainer.setValue(36, orginatingSystem);
	}

	public void setAGENT_TYPE(String agentType) throws BSPException {
		dataContainer.setValue(37, agentType);
	}

	public void setTKTD_COUNTRY(String ticketedCountry) throws BSPException {
		dataContainer.setValue(38, ticketedCountry);
	}

	public void setTKTD_AGENT(String agentID) throws BSPException {
		dataContainer.setValue(39, agentID);
	}

	public void setNET_CURR(String currencyCode) throws BSPException {
		dataContainer.setValue(40, currencyCode);
	}

	public void setNET_AMNT(String netFareAmount) throws BSPException {
		dataContainer.setValue(41, netFareAmount);
	}

	public void setBBR_CURREXCH_CODE(String bankersBuyingRate) throws BSPException {
		dataContainer.setValue(42, bankersBuyingRate);
	}

	public void setNOTICE_REQUIRED(String noticeRequired) throws BSPException {
		dataContainer.setValue(43, noticeRequired);
	}

	public void setTL3(String countaryCodeFromPTK) throws BSPException {
		dataContainer.setValue(44, countaryCodeFromPTK);
	}

	public void setCURRENCY_EXCH_AMT(String currencyExchangeAmount) throws BSPException {
		dataContainer.setValue(45, currencyExchangeAmount);
	}

	public void setCERTIFICATE_NUMBER(String certificateNumber) throws BSPException {
		dataContainer.setValue(46, certificateNumber);
	}

	public void setENDORSE_RESTRICT(String EndorseResrict) throws BSPException {
		dataContainer.setValue(47, EndorseResrict);
	}

}
