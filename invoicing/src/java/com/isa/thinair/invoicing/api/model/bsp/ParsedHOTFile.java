/**
 * 
 */
package com.isa.thinair.invoicing.api.model.bsp;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Janaka Padukka
 * 
 */
public class ParsedHOTFile {

	// HOT File meta data
	private Date processedDate;
	
	private String airlineIdentifier;
	
	private String bspCountryCode;

	// Transactions
	private Map<String,List<DPCProcessedBSPTnx>> transactions = new HashMap<String, List<DPCProcessedBSPTnx>>();

	public Date getProcessedDate() {
		return processedDate;
	}

	public void setProcessedDate(Date processedDate) {
		this.processedDate = processedDate;
	}

	public Map<String,List<DPCProcessedBSPTnx>> getTransactions() {
		return transactions;
	}

	public void setTransactions(Map<String,List<DPCProcessedBSPTnx>> transactions) {
		this.transactions = transactions;
	}

	public void addTransaction(DPCProcessedBSPTnx tnx) {
		if (!transactions.containsKey(tnx.getTranactionType() + tnx.getPrimaryEticket())) {
			transactions.put(tnx.getTranactionType() + tnx.getPrimaryEticket(), new ArrayList<DPCProcessedBSPTnx>());
		}
		transactions.get(tnx.getTranactionType() + tnx.getPrimaryEticket()).add(tnx);
	}

	public String getAirlineIdentifier() {
		return airlineIdentifier;
	}

	public void setAirlineIdentifier(String airlineIdentifier) {
		this.airlineIdentifier = airlineIdentifier;
	}

	public String getBspCountryCode() {
		return bspCountryCode;
	}

	public void setBspCountryCode(String bspCountryCode) {
		this.bspCountryCode = bspCountryCode;
	}
}
