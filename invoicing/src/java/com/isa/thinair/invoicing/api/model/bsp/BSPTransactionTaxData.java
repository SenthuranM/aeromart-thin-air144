package com.isa.thinair.invoicing.api.model.bsp;


public class BSPTransactionTaxData {

	private String taxCode;

	private String amount;

	private String currancyCode;

	private String ondCode;

	private String countryCode;

	private String depatureAirportLocalCurrency;

	public String getTaxCode() {
		return taxCode;
	}

	public void setTaxCode(String taxCode) {
		this.taxCode = taxCode;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getCurrancyCode() {
		return currancyCode;
	}

	public void setCurrancyCode(String currancyCode) {
		this.currancyCode = currancyCode;
	}

	public String getOndCode() {
		return ondCode;
	}

	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
		// this.setCountryCode(CommonsServices.getGlobalConfig().getAirportInfo(ondCode.substring(0, 3),
		// false)[3].toString());
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getDepatureAirportLocalCurrency() {
		return depatureAirportLocalCurrency;
	}

	public void setDepatureAirportLocalCurrency(String depatureAirportLocalCurrency) {
		this.depatureAirportLocalCurrency = depatureAirportLocalCurrency;
	}

}
