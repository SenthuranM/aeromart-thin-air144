/**
 * 
 */
package com.isa.thinair.invoicing.api.model.bsp.records;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BasicHOTFileRecordParser;

/**
 * @author Janaka Padukka
 * 
 */
public class BFH01 extends BasicHOTFileRecordParser {

	private String BSPI;

	private String TACN;

	private String REVN;

	private String TPST;

	// Date time both specified
	private Date PRDA;

	private String ISOC;

	private String FSQN;

	/**
	 * @param recordString
	 * @throws BSPException
	 */
	public BFH01(String recordString, String recordType) throws BSPException {
		super(recordString, recordType);
		parseRecord();
	}

	public void parseRecord() throws BSPException {
		// Only required fields are parsed
		DateFormat df = new SimpleDateFormat("yyMMddHHmm");

		this.BSPI = this.recordString.substring(13, 16);
		this.TACN = this.recordString.substring(16, 19).trim();
		this.REVN = this.recordString.substring(19, 22).trim();
		this.TPST = this.recordString.substring(22, 26);
		try {
			this.PRDA = df.parse(this.recordString.substring(26, 36));
		} catch (ParseException e) {
			throw new BSPException("Invalid date format", e);
		}
		this.ISOC = this.recordString.substring(36, 38);
		this.FSQN = this.recordString.substring(38, 44).trim();

	}

	public String getBSPI() {
		return BSPI;
	}

	public String getTACN() {
		return TACN;
	}

	public String getREVN() {
		return REVN;
	}

	public String getTPST() {
		return TPST;
	}

	public Date getPRDA() {
		return PRDA;
	}

	public String getISOC() {
		return ISOC;
	}

	public String getFSQN() {
		return FSQN;
	}
}
