package com.isa.thinair.invoicing.core.service;

import com.isa.thinair.platform.api.DefaultModule;

/**
 * 
 * InvoicingService's service interface
 * 
 * @isa.module.service-interface module-name="invoicing" description="The invoicing module"
 */
public class InvoicingService extends DefaultModule {
}