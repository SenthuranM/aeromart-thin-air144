/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.invoicing.core.service.bd;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseServiceDelegate;
import com.isa.thinair.invoicing.api.dto.GeneratedInvoiceDTO;
import com.isa.thinair.invoicing.api.service.InvoicingModuleUtils;

/**
 * @author Byorn
 * 
 */
public class SendInvoicesViaMDB extends PlatformBaseServiceDelegate {
	private final Log log = LogFactory.getLog(getClass());

	private static final java.lang.String DESTINATION_JNDI_NAME = "queue/invoiceQueue";
	private static final java.lang.String CONNECTION_FACTORY_JNDI_NAME = "ConnectionFactory";

	/**
	 * 
	 * @param generatedInvoicesDTOs
	 */
	public void sendEmailsViaMDB(GeneratedInvoiceDTO generatedInvoicesDTO) {
		try {
			if (generatedInvoicesDTO != null) {
				// modified to access MDB one by one so if one fails others should not get affected
				sendMessage(generatedInvoicesDTO, InvoicingModuleUtils.getInstance().getModuleConfig().getJndiProperties(),
						DESTINATION_JNDI_NAME, CONNECTION_FACTORY_JNDI_NAME);

			}

		} catch (ModuleException moduleException) {
			log.error("Sending message via jms failed", moduleException);
		}
	}

	/**
	 * 
	 * @param invoices
	 */
	public void transferViaMDB(Collection invoices) {
		try {
			sendMessage((ArrayList) invoices, InvoicingModuleUtils.getInstance().getModuleConfig().getJndiProperties(),
					DESTINATION_JNDI_NAME, CONNECTION_FACTORY_JNDI_NAME);
		} catch (ModuleException moduleException) {
			log.error("Sending message via jms failed", moduleException);
		}
	}

}
