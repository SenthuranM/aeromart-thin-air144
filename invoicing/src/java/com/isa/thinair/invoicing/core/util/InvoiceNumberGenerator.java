/**
 * 
 */
package com.isa.thinair.invoicing.core.util;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.invoicing.api.model.Invoice;
import com.isa.thinair.invoicing.api.service.InvoicingModuleUtils;

/**
 * @author BYORN.
 * 
 */
public class InvoiceNumberGenerator extends HibernateDaoSupport {

	private static final String DEFAULT = "DEFAULT";

	private static final Log log = LogFactory.getLog(InvoiceNumberGenerator.class);

	/**
	 * @author BYORN. 26/06/2006
	 * 
	 *         I have reused the same functionalty which chaminda wrote. Only difference is that I have separed the
	 *         logic into a separte class(THIS). and
	 * 
	 *         This method is for saveOrUpdateAll which is provided in hibernate.
	 * 
	 * 
	 *         Reason: In prev functionaliy saving an invoice takes a very (VERY) long time in invoice generation.
	 *         Simply because it goes through a loop hole of generating invoice number and saving for EACH AND EVERY
	 *         AGENT.
	 * 
	 *         This improvisation will reduce the time taken.
	 * 
	 *         I have reussed the number generation logic only...but will generate numbers for a collection of invoices
	 *         and will return them for the time being, so that getHibernateTemplate.saveOrUpdteAll(Invoice Collection)
	 *         can work.
	 * 
	 * 
	 */

	/**
	 * Method to set invoice number rule. FORMAT - YYMMBB9999 Where, YY = Year MM = Month BB = Billing period 9999 =
	 * Sequence Number. <Iterate all the given invoices> <first decide the max invoice number> <once take, keep
	 * incrementing it and attach it to the invoice> <return the invoices with there invoice_numbers for the
	 * saveOrUpdateAll method for hibernate.
	 * 
	 * @return constructed invoice number.
	 * @throws ModuleException
	 */

	/**
	 * Method will determine the default format for the client Depend on format invoice no generation will be differ ,
	 * This new Method being introduced as a part of jira :AARESAA-2687
	 */
	public Collection<Invoice> getInvoiceNos(Date invoiceDate, int billingPeriod, Collection<Invoice> invoices,
			String invoiceNoFormat) throws ModuleException {

		/*
		 * [AARESAA-2687]YY0000000000 --------------------> G9MDEFAULT --------------> G9
		 */

		Collection<Invoice> invoiceCollection;

		if (!DEFAULT.equalsIgnoreCase(invoiceNoFormat)) {
			invoiceCollection = setInvoiceNos(invoiceDate, billingPeriod, invoices, invoiceNoFormat);
		} else {
			invoiceCollection = setInvoiceNos(invoiceDate, billingPeriod, invoices);
		}

		return invoiceCollection;
	}

	@SuppressWarnings("deprecation")
	public Collection<Invoice> setInvoiceNos(Date invoiceDate, int billingPeriod, Collection<Invoice> invoices) {

		boolean getLastMaxInvoiceNumber = true;
		String invoiceNumber = null;
		String year = null;
		String month = null;
		String bilp = null;

		int max = 0;

		String invoiceSeq = null;

		year = Integer.toString(invoiceDate.getYear()).substring(1);

		if (invoiceDate.getMonth() < 9) {
			month = "0" + Integer.toString(invoiceDate.getMonth() + 1);
		} else {
			month = Integer.toString(invoiceDate.getMonth() + 1);
		}

		bilp = "0" + Integer.toString(billingPeriod);

		Iterator invoicesIterator = invoices.iterator();

		while (invoicesIterator.hasNext()) {

			Invoice invoice = (Invoice) invoicesIterator.next();

			if (getLastMaxInvoiceNumber) {

				String invNumPatern = year + month + bilp;
				String maxInvNum = InvoicingModuleUtils.getInvoiceJdbcDAO().getMaximumInvoiceNumber(invNumPatern);
				if (maxInvNum == null) {
					max = 1;
				} else {
					max = Integer.parseInt(maxInvNum.substring(6));
					max = max + 1;
				}

			} else {
				max = max + 1;
			}

			getLastMaxInvoiceNumber = false;

			invoiceSeq = Integer.toString(max);

			if (invoiceSeq.length() == 1) {
				invoiceSeq = "00000" + invoiceSeq;
			} else if (invoiceSeq.length() == 2) {
				invoiceSeq = "0000" + invoiceSeq;
			}

			else if (invoiceSeq.length() == 3) {
				invoiceSeq = "000" + invoiceSeq;
			} else if (invoiceSeq.length() == 4) {
				invoiceSeq = "00" + invoiceSeq;
			} else if (invoiceSeq.length() == 5) {
				invoiceSeq = "0" + invoiceSeq;
			}

			invoiceNumber = year.concat(month).concat(bilp).concat(invoiceSeq);
			invoice.setInvoiceNumber(invoiceNumber);
		}

		return invoices;
	}

	/**
	 * Format The Invoice No to requested format :
	 * 
	 * @param invoiceDate
	 * @param invoices
	 *            AARESAA-2687
	 * @return invoices with invoice numbers.
	 * @throws ModuleException
	 */
	public Collection<Invoice> setInvoiceNos(Date invoiceDate, int billingPeriod, Collection<Invoice> invoices,
			String invoiceNoFormat) throws ModuleException {

		// YY0000000000 --------------------> G9M -- AARESAA-2687
		String invoiceNumber = null;
		String invNoFormatWihoutPrefix = null;
		String prefixFormat = "00";
		String prefix = "";
		DecimalFormat prefixDecimalFormat = new DecimalFormat(prefixFormat);
		long max = 0;
		// TODO error throw for unknown pattern

		if (invoiceNoFormat.contains("YY")) {
			prefix = Integer.toString(CalendarUtil.getCalendar(invoiceDate).get(Calendar.YEAR)).substring(2);

		}
		if (invoiceNoFormat.contains("MM")) {
			prefix = prefix + prefixDecimalFormat.format(CalendarUtil.getCalendar(invoiceDate).get(Calendar.MONTH) + 1);

		}
		if (invoiceNoFormat.contains("BB")) {
			prefix = prefix + prefixDecimalFormat.format(billingPeriod);
		}

		invNoFormatWihoutPrefix = invoiceNoFormat.substring(prefix.length());
		try {

			String maxInvNum = InvoicingModuleUtils.getInvoiceJdbcDAO().getMaximumInvoiceNumber(prefix);

			if (maxInvNum != null && !"".equals(maxInvNum)) {
				max = Long.parseLong(maxInvNum.substring(prefix.length()));
			}

			DecimalFormat invoiceformat = new DecimalFormat(invNoFormatWihoutPrefix);

			for (Iterator invIterator = invoices.iterator(); invIterator.hasNext();) {
				Invoice invoice = (Invoice) invIterator.next();
				max++;
				invoiceNumber = invoiceformat.format(max);
				if (!(invoiceNumber.length() > invNoFormatWihoutPrefix.length())) {

					invoice.setInvoiceNumber(prefix + invoiceNumber);

				} else {
					// TODO instead STring pass a msg key
					throw new ModuleException("INVOICE NO EXCEEDS The limit , Please Increase it");
				}

			}

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Error Genarating INVOICE NO's" + e.getMessage());
			}
			throw new ModuleException("Error Genarating INVOICE NO's");
		}

		return invoices;
	}

}
