/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.invoicing.core.persistence.dao;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;

import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.core.util.XDBConnectionUtil;
import com.isa.thinair.invoicing.api.dto.InvoiceSummaryDTO;
import com.isa.thinair.invoicing.api.dto.SearchInvoicesDTO;
import com.isa.thinair.invoicing.api.dto.SearchInvoicesReceiptsDTO;
import com.isa.thinair.invoicing.core.util.InvoiceNumberGenerator;

/**
 * @author Byorn
 * 
 */
public interface InvoiceJdbcDAO {

	/**
	 * Returns a Page Object with all the Generated Invoices(T_INVOICE) for the period
	 * 
	 * @param invoicesDTO
	 * @param startIndex
	 * @param maxRecs
	 * @return PAGE-->GeneratedInvoicesDTO
	 */
	public Page getGeneratedInvoices(SearchInvoicesDTO invoicesDTO, int startIndex, int maxRecs);

	/**
	 * From t_pax_txn_breakdown_summary for NM codes SALE AND REFUND <If agentCodes == null will not filter will return
	 * record for all agents>
	 * 
	 * @param tnxDateFrom
	 * @param tnxDateTo
	 * @return Collection of AgentTotalSalesDTO
	 */
	public Collection getAgentTotalSalesForInvoicingPayCurr(Date tnxDateFrom, Date tnxDateTo, Collection agentCodes);

	/**
	 * From T_AGENT_TRANSACTION for NM codes SALE AND REFUND <If agentCodes == null will not filter will return record
	 * for all agents>
	 * 
	 * @param tnxDateFrom
	 * @param tnxDateTo
	 * @return Collection of AgentTotalSalesDTO
	 */
	public Collection getAgentTotalSalesForInvoicing(Date tnxDateFrom, Date tnxDateTo, Collection agentCodes);
	
	/**
	 * 
	 * @param tnxDateFrom
	 * @param tnxDateTo
	 * @param agentCodes
	 * @return Collection of AgentTotalSalesDTO
	 */
	public Collection getAgentTotalExternalSalesForInvoicing(Date tnxDateFrom, Date tnxDateTo, Collection agentCodes);

	/**
	 * Returns the Max Invoice Number. <Used for next number generation>
	 * 
	 * @see InvoiceNumberGenerator Class
	 * @return
	 * 
	 *         Accepst a patern like '060501' 'YYMMPe'
	 */
	public String getMaximumInvoiceNumber(String invNumberPatern);

	/**
	 * 
	 * Gets the count of invoice for the period/agentcodes.
	 * 
	 * @param invoicesDTO
	 * @return
	 */
	public int getCountOfInvoices(SearchInvoicesDTO invoicesDTO);

	/**
	 * 
	 * @return
	 */
	public Collection getAgentsWithoutTransactions(SearchInvoicesDTO invoicesDTO);

	/**
	 * 
	 * @param invoicesDTO
	 * @return
	 */
	public BigDecimal getInvoiceTotal(SearchInvoicesDTO invoicesDTO);

	/**
	 * 
	 * @param invoicesDTO
	 * @param system 
	 * @param accountingSystem 
	 * @return
	 */
	public BigDecimal getInvoiceTotalinExternalDB(SearchInvoicesDTO invoicesDTO, XDBConnectionUtil accountingSystem, String system);

	public boolean invoiceTransferInProgress(SearchInvoicesDTO invoicesDTO);

	public HashMap getAgentCommissionMap();

	public HashMap getAgentFareMap(Date fromDate, Date toDate);

	public InvoiceSummaryDTO getInvoiceBreakDownForAgent(String agentGSACode, String year, String month, int billingPeriod,
			InvoiceSummaryDTO invoiceSummDTO);

	public InvoiceSummaryDTO getInvoiceBreakDownForAgent(String agentGSACode, String pnr, InvoiceSummaryDTO invoiceSummDTO);

	public Collection getAgentPNRSalesForInvoicing(SearchInvoicesReceiptsDTO searchInvoicesReceiptsDTO);

	public Collection getAgentSalesForRefundInvoicing(Collection<Integer> txnIds);

}
