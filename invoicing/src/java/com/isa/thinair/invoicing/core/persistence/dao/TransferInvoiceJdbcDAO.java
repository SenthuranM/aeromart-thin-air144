/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.invoicing.core.persistence.dao;

import java.util.Collection;
import java.util.Date;

import com.isa.thinair.commons.core.util.XDBConnectionUtil;
import com.isa.thinair.invoicing.api.dto.SearchInvoicesDTO;

/**
 * @author Byorn
 * 
 */
public interface TransferInvoiceJdbcDAO {

	/**
	 * 
	 * @param invoicesDTO
	 * @param system
	 * @param accountingSystem
	 * @return
	 */
	public double getInvoiceTotalinExternalDB(SearchInvoicesDTO invoicesDTO, XDBConnectionUtil accountingSystem, String system);

	/**
	 * Returns all the Invoices for the period without paging.
	 * 
	 * @param invoicesDTO
	 * @return PAGE-->GeneratedInvoicesDTO
	 */
	public Collection getInvoices(SearchInvoicesDTO invoicesDTO);

	public Object[] getAutoInvoicesForPeriod(Date invoiceDate);

}
