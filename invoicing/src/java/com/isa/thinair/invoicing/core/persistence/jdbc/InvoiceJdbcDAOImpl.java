package com.isa.thinair.invoicing.core.persistence.jdbc;

/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import javax.sql.DataSource;

import com.isa.thinair.invoicing.api.service.InvoicingModuleUtils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airreservation.api.dto.AgentTotalSaledDTO;
import com.isa.thinair.airtravelagents.api.service.AirTravelagentsModuleUtils;
import com.isa.thinair.airtravelagents.api.util.AirTravelAgentConstants;
import com.isa.thinair.airtravelagents.api.util.BLUtil;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.core.framework.PlatformBaseJdbcDAOSupport;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.commons.core.util.XDBConnectionUtil;
import com.isa.thinair.invoicing.api.dto.GeneratedInvoiceDTO;
import com.isa.thinair.invoicing.api.dto.InvoiceSummaryDTO;
import com.isa.thinair.invoicing.api.dto.SearchInvoicesDTO;
import com.isa.thinair.invoicing.api.dto.SearchInvoicesReceiptsDTO;
import com.isa.thinair.invoicing.api.util.InvoicingInternalConstants;
import com.isa.thinair.invoicing.core.persistence.dao.InvoiceJdbcDAO;

/**
 * Invoice JDBC impl
 * 
 * @author Byorn
 * 
 */
public class InvoiceJdbcDAOImpl extends PlatformBaseJdbcDAOSupport implements InvoiceJdbcDAO {

	private Log log = LogFactory.getLog(getClass());

	// Query Ids
	private static final String GENERATED_INVOICES = "GENERATED_INVOICES";
	private static final String COUNT_OF_GEN_INVOICES = "COUNT_OF_GEN_INVOICES";
	private static final String GENERATED_INVOICES_BYTRANSACTION = "GENERATED_INVOICES_BYTRANSACTION";
	private static final String AGENT_TOTAL_SALES = "AGENT_TOTAL_SALES";
	private static final String AGENT_TOTAL_EXTERNAL_SALES = "AGENT_TOTAL_EXTERNAL_SALES";
	private static final String ZERO_INVOICES = "ZERO_INVOICES";
	private static final String GENERATED_INVOICES_FOR_RANGE = "GENERATED_INVOICES_FOR_RANGE";
	private static final String COUNT_OF_GEN_INVOICES_FOR_RANGE = "COUNT_OF_GEN_INVOICES_FOR_RANGE";
	private static final String AGENT_TOTAL_SALES_PAYCUR = "AGENT_TOTAL_SALES_PAYCUR";
	private static final String AGENT_TOTAL_SALES_FOR_INVOICE_RECEIPTS = "AGENT_TOTAL_SALES_FOR_INVOICE_RECEIPTS";

	
	/**
	 * @param agentCodes
	 *            <If agentCodes is NULL, will return for all the agents>
	 * @param startIndex
	 * @param maxRecs
	 * @return <Page object containing GeneratedInvoicesDTO's>
	 */
	public Page getGeneratedInvoices(SearchInvoicesDTO invoicesDTO, int startIndex, int maxRecs) {

		String agentsFilteringSql = "";
		String filterOutZeroInvoicesSql = "";
		// construct the start date of the period
		Date periodStartDate = BLUtil.getFromDate(invoicesDTO);

		Integer periodEndDateInt = invoicesDTO.getYearTo();
		Date periodEndDate = null;
		Page page = null;
		Object params2[] = new Object[5];
		Object params3[] = new Object[5];

		if (periodEndDateInt != 0) {
			periodEndDate = BLUtil.getPeriodToDate(invoicesDTO);
		}

		DataSource ds = AirTravelagentsModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		int rowNumFrom = startIndex;
		int rowNumTo = startIndex + maxRecs - 1;

		Object params[] = { periodStartDate, new Integer(rowNumFrom), new Integer(rowNumTo), invoicesDTO.getEntity() };

		if (periodEndDateInt != 0) {
			params2[0] = periodStartDate;
			params2[1] = periodEndDate;
			params2[2] = new Integer(rowNumFrom);
			params2[3] = new Integer(rowNumTo);
			params2[4] = invoicesDTO.getEntity();
		}
		if (invoicesDTO.isByTrasaction()) {
			Date invoiceDateFrm = invoicesDTO.getInvoiceDateFrom();
			Date invoiceDateTo = invoicesDTO.getInvoiceDateTo();
			params3[0] = invoiceDateFrm;
			params3[1] = invoiceDateTo;
			params3[2] = new Integer(rowNumFrom);
			params3[3] = new Integer(rowNumTo);
			params3[4] = invoicesDTO.getEntity();
		}
		// gets "" or "agent_code in ('AG1','AG2')
		agentsFilteringSql = BLUtil.getSQlForFilteringAgents(invoicesDTO.getAgentCodes(), "agent_code");
		filterOutZeroInvoicesSql = BLUtil.getSqlForFilteringZeroInvoice(invoicesDTO.isFilterZeroInvoices(), "invoice_amount");

		String query = null;
		if (periodEndDateInt != 0) {
			query = getQuery(GENERATED_INVOICES_FOR_RANGE);
		} else {
			if (invoicesDTO.isByTrasaction()) {
				query = getQuery(GENERATED_INVOICES_BYTRANSACTION);
			} else {
				query = getQuery(GENERATED_INVOICES);
			}
		}
		String sql1 = setStringToPlaceHolderIndex(query, 0, agentsFilteringSql);
		final String sql = setStringToPlaceHolderIndex(sql1, 1, filterOutZeroInvoicesSql);
		if (log.isDebugEnabled()) {
			log.debug(sql);
		}

		Object col = null;
		try {
			if (periodEndDateInt != 0) {
				col = templete.query(sql, params2, new ResultSetExtractor() {

					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Collection colInvoiceDTOs = new ArrayList();
						while (rs.next()) {

							GeneratedInvoiceDTO invoiceDTO = new GeneratedInvoiceDTO();
							invoiceDTO.setInvoiceNumber(rs.getString("invoice_number"));
							invoiceDTO.setInvoiceDate(rs.getDate("invoice_date"));
							invoiceDTO.setAgentCode(rs.getString("agent_code"));
							invoiceDTO.setAgentName(rs.getString("agent_name"));
							invoiceDTO.setAccountCode(rs.getString("account_code"));
							invoiceDTO.setInvoiceAmountLocal(rs.getBigDecimal("invoice_amount_local"));
							invoiceDTO.setInvoiceAmount(rs.getBigDecimal("invoice_amount"));
							invoiceDTO.setTransferedStatus(rs.getString("transfer_status"));
							invoiceDTO.setBillingEmailAddress(rs.getString("billing_email"));
							invoiceDTO.setEmailsSent(rs.getString("email_status"));
							invoiceDTO.setNumOfMailsSent(rs.getInt("emails_sent"));
							invoiceDTO.setSettledAmount(rs.getBigDecimal("settled_amount"));
							invoiceDTO.setBillingPeriod(rs.getInt("billing_period"));
							invoiceDTO.setCurrencyCode(rs.getString("currency_code"));
							invoiceDTO.setEntityName(rs.getString("entity_name"));
							invoiceDTO.setEntityCode(rs.getString("entity_code"));
							colInvoiceDTOs.add(invoiceDTO);
						}
						return colInvoiceDTOs;
					}

				});
			} else {
				if (invoicesDTO.isByTrasaction()) {
					col = templete.query(sql, params3, new ResultSetExtractor() {

						public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
							Collection colInvoiceDTOs = new ArrayList();
							while (rs.next()) {

								GeneratedInvoiceDTO invoiceDTO = new GeneratedInvoiceDTO();
								invoiceDTO.setInvoiceNumber(rs.getString("invoice_number"));
								invoiceDTO.setInvoiceDate(new Date(rs.getTimestamp("invoice_date").getTime()));
								invoiceDTO.setAgentCode(rs.getString("agent_code"));
								invoiceDTO.setAgentName(rs.getString("agent_name"));
								invoiceDTO.setAccountCode(rs.getString("account_code"));
								invoiceDTO.setInvoiceAmountLocal(rs.getBigDecimal("invoice_amount_local"));
								invoiceDTO.setInvoiceAmount(rs.getBigDecimal("invoice_amount"));
								invoiceDTO.setTransferedStatus(rs.getString("transfer_status"));
								invoiceDTO.setBillingEmailAddress(rs.getString("billing_email"));
								invoiceDTO.setEmailsSent(rs.getString("email_status"));
								invoiceDTO.setNumOfMailsSent(rs.getInt("emails_sent"));
								invoiceDTO.setCurrencyCode(rs.getString("currency_code"));
								invoiceDTO.setPnr(rs.getInt("pnr"));
								invoiceDTO.setEntityName(rs.getString("entity_name"));
								invoiceDTO.setEntityCode(rs.getString("entity_code"));
								colInvoiceDTOs.add(invoiceDTO);
							}
							return colInvoiceDTOs;
						}

					});

				} else {
					col = templete.query(sql, params, new ResultSetExtractor() {

						public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
							Collection colInvoiceDTOs = new ArrayList();
							while (rs.next()) {

								GeneratedInvoiceDTO invoiceDTO = new GeneratedInvoiceDTO();
								invoiceDTO.setInvoiceNumber(rs.getString("invoice_number"));
								invoiceDTO.setInvoiceDate(rs.getDate("invoice_date"));
								invoiceDTO.setAgentCode(rs.getString("agent_code"));
								invoiceDTO.setAgentName(rs.getString("agent_name"));
								invoiceDTO.setAccountCode(rs.getString("account_code"));
								invoiceDTO.setInvoiceAmountLocal(rs.getBigDecimal("invoice_amount_local"));
								invoiceDTO.setInvoiceAmount(rs.getBigDecimal("invoice_amount"));
								invoiceDTO.setTransferedStatus(rs.getString("transfer_status"));
								invoiceDTO.setBillingEmailAddress(rs.getString("billing_email"));
								invoiceDTO.setEmailsSent(rs.getString("email_status"));
								invoiceDTO.setNumOfMailsSent(rs.getInt("emails_sent"));
								invoiceDTO.setCurrencyCode(rs.getString("currency_code"));
								invoiceDTO.setEntityName(rs.getString("entity_name"));
								invoiceDTO.setEntityCode(rs.getString("entity_code"));
								colInvoiceDTOs.add(invoiceDTO);
							}
							return colInvoiceDTOs;
						}

					});
				}
			}
		} catch (DataAccessException e) {
			log.error(e);
		}

		if (periodEndDateInt != 0) {
			page = new Page(getCountForPeriod(invoicesDTO), rowNumFrom, rowNumTo, (Collection) col);
		} else {
			page = new Page(getCount(invoicesDTO), rowNumFrom, rowNumTo, (Collection) col);
		}
		return page;
	}

	/**
	 * @param periodStartDate
	 * @param agentCodes
	 * @return
	 */
	private int getCount(SearchInvoicesDTO invoicesDTO) {

		Date periodStartDate = BLUtil.getFromDate(invoicesDTO);
		String agentsFilteringSql = BLUtil.getSQlForFilteringAgents(invoicesDTO.getAgentCodes(), "agent_code");
		String filterOutZeroInvoices = BLUtil.getSqlForFilteringZeroInvoice(invoicesDTO.isFilterZeroInvoices(), "invoice_amount");
		DataSource ds = AirTravelagentsModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		Object params[] = { periodStartDate };

		String query = getQuery(COUNT_OF_GEN_INVOICES);
		String sql1 = setStringToPlaceHolderIndex(query, 0, agentsFilteringSql);

		final String sql = setStringToPlaceHolderIndex(sql1, 1, filterOutZeroInvoices);

		Object intObj = templete.query(sql, params, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Integer count = null;
				while (rs.next()) {
					count = new Integer(rs.getInt(1));

				}
				return count;

			}

		});

		// Page page = new Page();
		return ((Integer) intObj).intValue();

	}

	/**
	 * @param periodStartDate
	 * @param agentCodes
	 * @return
	 */
	private int getCountForPeriod(SearchInvoicesDTO invoicesDTO) {

		Date periodStartDate = BLUtil.getFromDate(invoicesDTO);
		String agentsFilteringSql = BLUtil.getSQlForFilteringAgents(invoicesDTO.getAgentCodes(), "agent_code");
		String filterOutZeroInvoices = BLUtil.getSqlForFilteringZeroInvoice(invoicesDTO.isFilterZeroInvoices(), "invoice_amount");
		DataSource ds = AirTravelagentsModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		Date periodEndDate = BLUtil.getPeriodToDate(invoicesDTO);
		;

		Object params[] = { periodStartDate, periodEndDate };

		String query = getQuery(COUNT_OF_GEN_INVOICES_FOR_RANGE);
		String sql1 = setStringToPlaceHolderIndex(query, 0, agentsFilteringSql);

		final String sql = setStringToPlaceHolderIndex(sql1, 1, filterOutZeroInvoices);

		Object intObj = templete.query(sql, params, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Integer count = null;
				while (rs.next()) {
					count = new Integer(rs.getInt(1));

				}
				return count;

			}

		});

		// Page page = new Page();
		return ((Integer) intObj).intValue();

	}

	/**
	 * InvoiceDAO: getAgentTotalSalesForInvoicing(Date tnxDateFrom, Date tnxDateTo)
	 * 
	 * @param tnxDateFrom
	 * @param tnxDateTo
	 * @return Collection of AgentTotatalSalesDTO 's.
	 */
	public Collection getAgentTotalSalesForInvoicing(Date tnxDateFrom, Date tnxDateTo, Collection agentCodes) {

		DataSource ds = AirTravelagentsModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);
		Object params[] = null; // {tnxDateFrom, tnxDateTo};
		List<Integer> nominalCodes = new ArrayList<>();

		nominalCodes.add(1);
		nominalCodes.add(7);
		nominalCodes.add(10);
		nominalCodes.add(11);
		nominalCodes.add(19);
		nominalCodes.add(20);
		

		String filterForAgents = BLUtil.getSQlForFilteringAgents(agentCodes, "a.agent_code");
		String agenTotalSqlesSql = getQuery(AGENT_TOTAL_SALES);
		agenTotalSqlesSql = agenTotalSqlesSql.replaceFirst("[?]", BLUtil.getOracleToDate(tnxDateFrom));
		agenTotalSqlesSql = agenTotalSqlesSql.replaceFirst("[?]", BLUtil.getOracleToDate(tnxDateTo));
		String sql = setStringToPlaceHolderIndex(agenTotalSqlesSql, 0, Util.buildIntegerInClauseContent(nominalCodes));
		sql = setStringToPlaceHolderIndex(sql, 1, filterForAgents);
		Object objcol = null;
		try {
			objcol = template.query(sql, params, new ResultSetExtractor() {

				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

					Collection agentTotalSales = new ArrayList();
					while (rs.next()) {
						AgentTotalSaledDTO agentTotalSaledDTO = new AgentTotalSaledDTO();
						agentTotalSaledDTO.setTotalSales(rs.getBigDecimal("sum"));
						agentTotalSaledDTO.setAgentCode(rs.getString("agent_code"));
						agentTotalSaledDTO.setAccountId(rs.getString("account_code"));
						// agentTotalSaledDTO.setAgentCommissionCode(rs.getString("agent_commission_code"));
						agentTotalSaledDTO.setCurrencyCode(rs.getString("currency_code"));
						// agentTotalSaledDTO.setBaseExchangeRate(rs.getBigDecimal("base_exchange_rate"));
						agentTotalSaledDTO.setTotalSalesLocal(rs.getBigDecimal("sumlocal"));
						agentTotalSaledDTO.setEntityCode(rs.getString("entity_code"));
						agentTotalSales.add(agentTotalSaledDTO);
					}
					return agentTotalSales;
				}

			});
		} catch (DataAccessException e) {
			throw new CommonsDataAccessException(e, "airtravelagent.jdbc.error", AirTravelAgentConstants.MODULE_NAME);
		}
		return (Collection) objcol;
	}
	
	/**
	 * InvoiceDAO: getAgentTotalSalesForInvoicing(Date tnxDateFrom, Date tnxDateTo)
	 * 
	 * @param tnxDateFrom
	 * @param tnxDateTo
	 * @return Collection of AgentTotatalSalesDTO 's.
	 */
	public Collection getAgentTotalExternalSalesForInvoicing(Date tnxDateFrom, Date tnxDateTo, Collection agentCodes) {

		DataSource ds = AirTravelagentsModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);
		Object params[] = null; // {tnxDateFrom, tnxDateTo};
		List<Integer> nominalCodes = new ArrayList<>();

		nominalCodes.add(1);
		nominalCodes.add(7);
		nominalCodes.add(10);
		nominalCodes.add(11);
		nominalCodes.add(19);
		nominalCodes.add(20);
		
		String filterForAgents = BLUtil.getSQlForFilteringAgents(agentCodes, "a.agent_code");
		String agenTotalSqlesSql = getQuery(AGENT_TOTAL_EXTERNAL_SALES);
		agenTotalSqlesSql = agenTotalSqlesSql.replaceFirst("[?]", BLUtil.getOracleToDate(tnxDateFrom));
		agenTotalSqlesSql = agenTotalSqlesSql.replaceFirst("[?]", BLUtil.getOracleToDate(tnxDateTo));
		String sql = setStringToPlaceHolderIndex(agenTotalSqlesSql, 0, Util.buildIntegerInClauseContent(nominalCodes));
		sql = setStringToPlaceHolderIndex(sql, 1, filterForAgents);
		sql = setStringToPlaceHolderIndex(sql, 2, InvoicingInternalConstants.Entity.ENTITY_RESERVATION);
		Object objcol = null;
		try {
			objcol = template.query(sql, params, new ResultSetExtractor() {

				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

					Collection agentTotalSales = new ArrayList();
					while (rs.next()) {
						AgentTotalSaledDTO agentTotalSaledDTO = new AgentTotalSaledDTO();
						agentTotalSaledDTO.setTotalSales(rs.getBigDecimal("sum"));
						agentTotalSaledDTO.setAgentCode(rs.getString("agent_code"));
						agentTotalSaledDTO.setAccountId(rs.getString("account_code"));
						// agentTotalSaledDTO.setAgentCommissionCode(rs.getString("agent_commission_code"));
						agentTotalSaledDTO.setCurrencyCode(rs.getString("currency_code"));
						// agentTotalSaledDTO.setBaseExchangeRate(rs.getBigDecimal("base_exchange_rate"));
						agentTotalSaledDTO.setTotalSalesLocal(rs.getBigDecimal("sumlocal"));
						agentTotalSaledDTO.setEntityCode(rs.getString("entity_code"));
						agentTotalSales.add(agentTotalSaledDTO);
					}
					return agentTotalSales;
				}

			});
		} catch (DataAccessException e) {
			throw new CommonsDataAccessException(e, "airtravelagent.jdbc.error", AirTravelAgentConstants.MODULE_NAME);
		}
		return (Collection) objcol;
	}

	/**
	 * InvoiceDAO: getAgentTotalSalesForInvoicing(Date tnxDateFrom, Date tnxDateTo)
	 * 
	 * @param tnxDateFrom
	 * @param tnxDateTo
	 * @return Collection of AgentTotatalSalesDTO 's.
	 */
	public Collection getAgentTotalSalesForInvoicingPayCurr(Date tnxDateFrom, Date tnxDateTo, Collection agentCodes) {

		DataSource ds = AirTravelagentsModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);

		Object params[] = null; // {tnxDateFrom,tnxDateTo,tnxDateFrom, tnxDateTo};

		String filterForAgents = BLUtil.getSQlForFilteringAgents(agentCodes, "A.agent_code");
		String agenTotalSqlesSql = getQuery(AGENT_TOTAL_SALES_PAYCUR);
		agenTotalSqlesSql = agenTotalSqlesSql.replaceFirst("[?]", AppSysParamsUtil.getDefaultCarrierCode()); // filter
																												// tnx
																												// values
																												// from
																												// own
																												// airline
																												// (see
																												// the
																												// sql
																												// as
																												// well)
		agenTotalSqlesSql = agenTotalSqlesSql.replaceFirst("[?]", BLUtil.getOracleToDate(tnxDateFrom));
		agenTotalSqlesSql = agenTotalSqlesSql.replaceFirst("[?]", BLUtil.getOracleToDate(tnxDateTo));
		agenTotalSqlesSql = agenTotalSqlesSql.replaceFirst("[?]", BLUtil.getOracleToDate(tnxDateFrom));
		agenTotalSqlesSql = agenTotalSqlesSql.replaceFirst("[?]", BLUtil.getOracleToDate(tnxDateTo));
		final String sql = setStringToPlaceHolderIndex(agenTotalSqlesSql, 0, filterForAgents);

		Object objcol = null;
		try {
			objcol = template.query(sql, params, new ResultSetExtractor() {

				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

					Collection agentTotalSales = new ArrayList();
					while (rs.next()) {
						AgentTotalSaledDTO agentTotalSaledDTO = new AgentTotalSaledDTO();
						agentTotalSaledDTO.setTotalSales(rs.getBigDecimal("sum"));
						agentTotalSaledDTO.setTotalSalesLocal(rs.getBigDecimal("sum_local"));
						agentTotalSaledDTO.setAgentCode(rs.getString("agent_code"));
						agentTotalSaledDTO.setAccountId(rs.getString("account_code"));
						// agentTotalSaledDTO.setAgentCommissionCode(rs.getString("agent_commission_code"));
						agentTotalSaledDTO.setCurrencyCode(rs.getString("currency_code"));
						// agentTotalSaledDTO.setBaseExchangeRate(rs.getBigDecimal("base_exchange_rate"));
						// since the query returns reservation transactions
						agentTotalSaledDTO.setEntityCode(InvoicingInternalConstants.Entity.ENTITY_RESERVATION);
						agentTotalSales.add(agentTotalSaledDTO);
					}
					return agentTotalSales;
				}

			});
		} catch (DataAccessException e) {
			throw new CommonsDataAccessException(e, "airtravelagent.jdbc.error", AirTravelAgentConstants.MODULE_NAME);
		}
		return (Collection) objcol;
	}

	/**
	 * @see com.isa.thinair.invoicing.core.persistance.dao.InvoiceJdbcDAO #getMaximumInvoiceNumber()
	 */
	public String getMaximumInvoiceNumber(String invNumberPatern) {
		DataSource ds = AirTravelagentsModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);

		String sql = " SELECT max(invoice_number) as maxnum FROM t_invoice " + " WHERE invoice_number like '" + invNumberPatern
				+ "%'";

		String maxinvNum = null;
		try {
			maxinvNum = (String) template.query(sql, new ResultSetExtractor() {

				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

					String maxNumber = "";
					while (rs.next()) {
						maxNumber = rs.getString("maxnum");
					}
					return maxNumber;

				}

			});
		} catch (DataAccessException e) {
			throw new CommonsDataAccessException(e, "airtravelagent.jdbc.error", AirTravelAgentConstants.MODULE_NAME);
		}

		return maxinvNum;
	}

	public int getCountOfInvoices(SearchInvoicesDTO invoicesDTO) {

		DataSource ds = AirTravelagentsModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);
		Date periodFrom = BLUtil.getFromDate(invoicesDTO);
		Object params[] = { periodFrom };

		String sql = " SELECT count(i.invoice_number) as cnt FROM t_invoice i where " + " trunc(i.invioce_period_from)=? ";

		Object count = null;

		try {
			count = template.query(sql, params, new ResultSetExtractor() {

				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					Integer count = null;

					while (rs.next()) {
						count = new Integer(rs.getInt("cnt"));
					}
					return count;

				}

			});
		} catch (DataAccessException e) {
			throw new CommonsDataAccessException(e, "airtravelagent.jdbc.error", AirTravelAgentConstants.MODULE_NAME);
		}
		return ((Integer) count).intValue();

	}

	/**
	 * @see com.isa.thinair.invoicing.core.persistance.dao.InvoiceJdbcDAO#getOnAccountAgentCodes()
	 */
	public Collection getAgentsWithoutTransactions(SearchInvoicesDTO invoicesDTO) {
		String sql = getQuery(ZERO_INVOICES);
		DataSource ds = AirTravelagentsModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);

		Date tnxFromDate = BLUtil.getFromDate(invoicesDTO);
		Date tnxToDate = BLUtil.getToDate(invoicesDTO);
		Object params[] = { InvoicingInternalConstants.AgentPaymentMethods.ON_ACCOUNT, tnxFromDate, tnxToDate };
		Object col = null;

		try {

			col = template.query(sql, params, new ResultSetExtractor() {
				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

					Collection agentTotalSales = new ArrayList();
					while (rs.next()) {
						AgentTotalSaledDTO agentTotalSaledDTO = new AgentTotalSaledDTO();
						agentTotalSaledDTO.setAgentCode(rs.getString("agent_code"));
						agentTotalSaledDTO.setAccountId(rs.getString("account_code"));
						agentTotalSales.add(agentTotalSaledDTO);
					}
					return agentTotalSales;
				}

			});

		} catch (DataAccessException e) {
			log.error(e);
			throw new CommonsDataAccessException(e, "airtravelagent.jdbc.error", AirTravelAgentConstants.MODULE_NAME);
		}

		return (Collection) col;

	}

	/**
	 * 
	 * @param invoicesDTO
	 * @return
	 */
	public BigDecimal getInvoiceTotal(SearchInvoicesDTO invoicesDTO) {

		DataSource ds = AirTravelagentsModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		String fromD = sdf.format(BLUtil.getFromDate(invoicesDTO));
		String toD = sdf.format(BLUtil.getToDate(invoicesDTO));

		String sql = " SELECT sum(invoice_amount) as amt FROM t_invoice " + " t where t.invioce_period_from = to_date('" + fromD
				+ " 00:00:00', 'dd-mon-yyyy hh24:mi:ss') " + " and t.invoice_period_to = to_date('" + toD
				+ "  23:59:59', 'dd-mon-yyyy hh24:mi:ss') ";

		// String sql = " SELECT sum(invoice_amount) as amt"
		// + " FROM t_invoice where invoice_number like '"
		// + invoicesDTO.getInvoiceNumberFormat() + "'";

		Object count = null;

		try {
			count = template.query(sql, new ResultSetExtractor() {

				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					BigDecimal count = null;

					while (rs.next()) {
						count = rs.getBigDecimal("amt");
					}
					return count;

				}

			});
		} catch (DataAccessException e) {
			throw new CommonsDataAccessException(e, "airtravelagent.jdbc.error", AirTravelAgentConstants.MODULE_NAME);
		}
		return (BigDecimal) count;

	}

	/**
	 * see DAO Interface
	 * 
	 * @param invoicesDTO
	 * @return
	 */
	public boolean invoiceTransferInProgress(SearchInvoicesDTO invoicesDTO) {

		DataSource ds = AirTravelagentsModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		String fromD = sdf.format(BLUtil.getFromDate(invoicesDTO));
		String toD = sdf.format(BLUtil.getToDate(invoicesDTO));

		/**
		 * String sql = " SELECT count(invoice_number) as cnt" + " FROM t_invoice where invoice_number like '" +
		 * invoicesDTO.getInvoiceNumberFormat() + "'" + " and transfer_status= '" +
		 * InvoicingInternalConstants.TransferStatus.IN_PROGRESS + "'" ;
		 */
		String sql = " SELECT count(invoice_number) as cnt" + " FROM t_invoice t where t.invioce_period_from = to_date('" + fromD
				+ " 00:00:00', 'dd-mon-yyyy hh24:mi:ss') " + " and t.invoice_period_to = to_date('" + toD
				+ "  23:59:59', 'dd-mon-yyyy hh24:mi:ss') " + " and transfer_status= '"
				+ InvoicingInternalConstants.TransferStatus.IN_PROGRESS + "'";

		Object count = null;

		try {
			count = template.query(sql, new ResultSetExtractor() {

				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					Integer count = null;

					while (rs.next()) {
						count = new Integer(rs.getInt("cnt"));
					}
					return count;

				}

			});
		} catch (DataAccessException e) {
			throw new CommonsDataAccessException(e, "airtravelagent.jdbc.error", AirTravelAgentConstants.MODULE_NAME);
		}
		int cnt = ((Integer) count).intValue();
		if (cnt > 0) {
			return true;
		}
		return false;
	}

	/**
	 * Method used to find the invoice total from corresponding external system
	 * @param invoiceCollection
	 * @param accountingSystem
	 * @param system
	 * @return totAmount
	 */
	public BigDecimal
			getInvoiceTotalinExternalDB(SearchInvoicesDTO invoicesDTO, XDBConnectionUtil accountingSystem, String system) {
		BigDecimal totAmount = new BigDecimal(-1);
		Connection connection = accountingSystem.getExternalConnection();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		String selectStatment = "";
		// select query is different for two external systems because of column name dissimilarity
		if (AirTravelAgentConstants.X_EXTERNAL_SYSTEM.equalsIgnoreCase(system)) {
			selectStatment = "select sum(total_invoice_amount) as amt from INT_T_INVOICE where INVOICE_NUMBER like " + "'"
					+ invoicesDTO.getInvoiceNumberFormat() + "'";
		} else if (AirTravelAgentConstants.X3_EXTERNAL_SYSTEM.equalsIgnoreCase(system)) {
			selectStatment = "select sum(invoice_amount) as amt from INT_T_INVOICE where INVOICE_NUMBER like " + "'"
					+ invoicesDTO.getInvoiceNumberFormat() + "'";
		}
		PreparedStatement selectStmt = null;
		// create the prepared statements
		try {
			selectStmt = connection.prepareStatement(selectStatment);
		} catch (SQLException e) {
			log.error("Error when getting prepared statment" + e);
			throw new CommonsDataAccessException(e, "airtravelagent.prepstatment.error", AirTravelAgentConstants.MODULE_NAME);
		}
		try {
			ResultSet res = selectStmt.executeQuery();
			// Should check whether amounts can be taken as bigdecimal
			while (res.next()) {
				totAmount = res.getBigDecimal("amt");
			}
		} catch (Exception e) {
			throw new CommonsDataAccessException(e, "exdb.getconnection.retrieval.failed", AirTravelAgentConstants.MODULE_NAME);
		}
		return totAmount;
	}

	public HashMap<String, Double> getAgentCommissionMap() {

		DataSource ds = AirTravelagentsModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);
		Date d = new Date();
		String sql = " SELECT a.agent_commission_code, " + " a.commission " + " FROM t_agent_commission_rate a "
				+ " WHERE a.status='ACT' " + " and ? between a.effective_from_date and a.effective_to_date";

		Object params[] = { d };
		return (HashMap<String, Double>) template.query(sql, params, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				HashMap<String, Double> map = new HashMap<String, Double>();
				while (rs.next()) {
					map.put(rs.getString("agent_commission_code"), rs.getDouble("commission"));

				}
				return map;

			}

		});
	}

	public HashMap<String, BigDecimal> getAgentFareMap(Date fromDate, Date toDate) {
		DataSource ds = AirTravelagentsModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		String fromD = sdf.format(fromDate);
		String toD = sdf.format(toDate);

		StringBuilder sql = new StringBuilder();
		sql.append("	select agent_code, sum(total_fare) as totfare from (");
		sql.append("	select distinct r.pnr, r.total_fare, t.agent_code from ");
		sql.append("	(select distinct r.pnr, first_value(t.txn_id) over (partition by r.pnr order by t.tnx_date) first_txn_id");
		sql.append("		from t_reservation r, t_pnr_passenger p, t_pax_transaction t");
		sql.append("	where r.pnr = p.pnr");
		sql.append("	and p.pnr_pax_id = t.pnr_pax_id");
		sql.append("	and t.nominal_code in (15,16,17,18,19,28,30)");
		sql.append("	AND P.PNR IN(SELECT P1.pnr FROM t_pax_transaction T1,t_pnr_passenger P1");
		sql.append("	WHERE P1.pnr_pax_id=T1.pnr_pax_id");
		sql.append("	AND nominal_code = 19 ");// AND agent_code in('GSA15', 'SHJ64')");
		sql.append("	AND (T1.payment_carrier_code is null or T1.payment_carrier_code='"
				+ AppSysParamsUtil.getDefaultCarrierCode() + "') ");// AND agent_code in('GSA15', 'SHJ64')");
		sql.append("	AND tnx_date");
		sql.append("	between to_date('" + fromD + " 00:00:00', 'dd-mon-yyyy hh24:mi:ss')");
		sql.append("	and to_date('" + toD + " 23:59:59', 'dd-mon-yyyy hh24:mi:ss'))");
		sql.append("	)txn,");
		sql.append("	t_reservation r, t_pnr_passenger p, t_pax_transaction t");
		sql.append("	where");
		sql.append("	txn.pnr = r.pnr");
		sql.append("	and t.txn_id = txn.first_txn_id");
		sql.append("	and t.tnx_date between to_date('" + fromD + " 00:00:00', 'dd-mon-yyyy hh24:mi:ss')");
		sql.append("	and to_date('" + toD + " 23:59:59', 'dd-mon-yyyy hh24:mi:ss')");
		sql.append("	and r.pnr = p.pnr");
		sql.append("	and p.pnr_pax_id = t.pnr_pax_id ");
		sql.append("	and t.nominal_code = 19");
		sql.append("	and (t.payment_carrier_code is null or t.payment_carrier_code='" + AppSysParamsUtil.getDefaultCarrierCode()
				+ "') ");
		sql.append("	and exists (select 'x' from t_pnr_passenger p1,t_pax_transaction t1");
		sql.append("	where p1.pnr_pax_id=t1.pnr_pax_id and (t1.payment_carrier_code is null or t1.payment_carrier_code = '"
				+ AppSysParamsUtil.getDefaultCarrierCode() + "') and  p1.pnr=r.pnr group by p1.pnr having sum(t1.amount)<=0)");
		sql.append("	)");
		sql.append("	group by agent_code");
		if (log.isDebugEnabled()) {

			log.debug(sql.toString());

		}
		return (HashMap<String, BigDecimal>) template.query(sql.toString(), new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				HashMap<String, BigDecimal> map = new HashMap<String, BigDecimal>();
				while (rs.next()) {
					map.put(rs.getString("agent_code"), rs.getBigDecimal("totfare"));

				}
				return map;

			}

		});
	}

	public InvoiceSummaryDTO getInvoiceBreakDownForAgent(String agentGSACode, String year, String month, int billingPeriod,
			InvoiceSummaryDTO invoiceSummDTO) {
		final InvoiceSummaryDTO invoiceSummaryDTO = invoiceSummDTO;
		Date firstday = null;
		Date lastday = null;

		GregorianCalendar gc = new GregorianCalendar();

		gc.set(Calendar.YEAR, Integer.parseInt(year));
		gc.set(Calendar.MONTH, (Integer.parseInt(month) - 1));
		gc.set(Calendar.DATE, 1);
		if (billingPeriod == 1) {
			firstday = gc.getTime();
			gc.set(Calendar.DATE, 15);
			lastday = gc.getTime();

		} else {
			gc.set(Calendar.DATE, 16);
			firstday = gc.getTime();
			gc.set(Calendar.DATE, gc.getActualMaximum(Calendar.DAY_OF_MONTH));
			lastday = gc.getTime();
		}

		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
		String obj[] = { formatter.format(firstday), formatter.format(lastday), formatter.format(firstday),
				formatter.format(lastday) };
		DataSource ds = AirTravelagentsModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);
		String queryString = "SELECT agent_code, (-1 *sum(total_fare_amount)) as total_fare, (-1 *sum (total_fare_amount_paycur)) as total_fare_amount_paycurr,"
				+ "(-1 * sum(total_tax_amount)) as total_tax, (-1 * sum(total_tax_amount_paycur)) as total_tax_paycurr, "
				+ "(-1 * sum(total_sur_amount)) as total_sur, (-1 * sum(total_sur_amount_paycur)) as total_sur_paycurr, "
				+ "(-1 * sum(total_mod_amount)) as total_mod, (-1 * sum(total_mod_amount_paycur)) as total_mod_paycurr "
				+ " FROM ("
				// Normal Sales
				+ " select b.agent_code, total_fare_amount, total_fare_amount_paycur, total_tax_amount, total_tax_amount_paycur, total_sur_amount,"
				+ " total_sur_amount_paycur, total_mod_amount, total_mod_amount_paycur"
				+ " from t_pax_txn_breakdown_summary  brsum, t_pax_transaction  pax, t_agent  b "
				+ " where brsum.pax_txn_id = pax.txn_id	 "
				+ " and   pax.agent_code = b.agent_code "
				+ " and   b.agent_code = '"
				+ agentGSACode
				+ "'"
				+ " and   pax.nominal_code in (19,25)"
				+ " and   (pax.PAYMENT_CARRIER_CODE is null or pax.PAYMENT_CARRIER_CODE='"
				+ AppSysParamsUtil.getDefaultCarrierCode()
				+ "')"
				+ " and	  trunc(brsum.timestamp) >= ? and  trunc(brsum.timestamp)<= ? "
				// External sales - TODO we don't have the breakdown information for external tnxs.
				+ " UNION ALL "
				+ " select b.agent_code, p.amount as total_fare_amount, p.amount_paycur as total_fare_amount_paycur, 0 as total_tax_amount,"
				+ " 0 as total_tax_amount_paycur, 0 as total_sur_amount, 0 as total_sur_amount_paycur,"
				+ " 0 as total_mod_amount, 0 as total_mod_amount_paycur "
				+ " from t_pax_ext_carrier_transactions p, t_agent b "
				+ " where "
				+ " b.agent_code = '"
				+ agentGSACode
				+ "'"
				+ " and   p.nominal_code in (19,25) "
				+ " and   b.agent_code = p.agent_code "
				+ " and	  trunc(p.TXN_TIMESTAMP) >= ? and  trunc(p.TXN_TIMESTAMP)<= ? "
				+ ") group by agent_code";

		templete.query(queryString, obj, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					invoiceSummaryDTO.setFareAmountLocal(new BigDecimal(rs.getString("total_fare_amount_paycurr")));
					invoiceSummaryDTO.setTaxAmountLocal(new BigDecimal(rs.getString("total_tax_paycurr")));
					invoiceSummaryDTO.setSurchrageAmountLocal(new BigDecimal(rs.getString("total_sur_paycurr")));
					invoiceSummaryDTO.setModifyAmountLocal(new BigDecimal(rs.getString("total_mod_paycurr")));
					invoiceSummaryDTO.setFareAmount(new BigDecimal(rs.getString("total_fare")));
					invoiceSummaryDTO.setTaxAmount(new BigDecimal(rs.getString("total_tax")));
					invoiceSummaryDTO.setModifyAmount(new BigDecimal(rs.getString("total_mod")));
					invoiceSummaryDTO.setSurchrageAmount(new BigDecimal(rs.getString("total_sur")));
				}
				return invoiceSummaryDTO;
			}

		});

		return invoiceSummaryDTO;
	}

	public InvoiceSummaryDTO getInvoiceBreakDownForAgent(String agentGSACode, String pnr, InvoiceSummaryDTO invoiceSummDTO) {
		final InvoiceSummaryDTO invoiceSummaryDTO = invoiceSummDTO;

		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");

		DataSource ds = AirTravelagentsModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);
		String queryString = "SELECT agent_code, (-1 *sum(total_fare_amount)) as total_fare, (-1 *sum (total_fare_amount_paycur)) as total_fare_amount_paycurr,"
				+ "(-1 * sum(total_tax_amount)) as total_tax, (-1 * sum(total_tax_amount_paycur)) as total_tax_paycurr, "
				+ "(-1 * sum(total_sur_amount)) as total_sur, (-1 * sum(total_sur_amount_paycur)) as total_sur_paycurr, "
				+ "(-1 * sum(total_mod_amount)) as total_mod, (-1 * sum(total_mod_amount_paycur)) as total_mod_paycurr "

				/*
				 * + " FROM t_pax_txn_breakdown_summary  brsum, t_pax_transaction  pax, t_agent  b, t_pnr_passenger pp "
				 * + "where brsum.pax_txn_id = pax.txn_id	 " + " and   pax.agent_code = b.agent_code " +
				 * " and   b.agent_code = '" + agentGSACode +"'" + " and pp.pnr = '" + pnr + "' " +
				 * " and pp.pnr_pax_id = pax.pnr_pax_id " + " and pax.txn_id = brsum.pax_txn_id " +
				 * " and   pax.nominal_code in (19,25) " + " group by b.agent_code " ;
				 */

				+ " FROM ("
				// Normal Sales
				+ " select b.agent_code, total_fare_amount, total_fare_amount_paycur, total_tax_amount, total_tax_amount_paycur, total_sur_amount,"
				+ " total_sur_amount_paycur, total_mod_amount, total_mod_amount_paycur"
				+ " from t_pax_txn_breakdown_summary  brsum, t_pax_transaction  pax, t_agent  b, t_pnr_passenger pp "
				+ " where brsum.pax_txn_id = pax.txn_id	 " + " and   pax.agent_code = b.agent_code " + " and   b.agent_code = '"
				+ agentGSACode
				+ "'"
				+ " and pp.pnr = '"
				+ pnr
				+ "' "
				+ " and pp.pnr_pax_id = pax.pnr_pax_id "
				+ " and pax.txn_id = brsum.pax_txn_id "
				+ " and (pax.payment_carrier_code is null "
				+ " or pax.payment_carrier_code = '"
				+ AppSysParamsUtil.getDefaultCarrierCode()
				+ "') "
				+ " and   pax.nominal_code in (19,25)"
				// External Sales (Interline + Dry). Note : as of now we do not have the breakdown summary for values in
				// external tnx records table.
				+ " UNION ALL "
				+ " select b.agent_code, p.amount as total_fare_amount, p.amount_paycur as total_fare_amount_paycur, 0 as total_tax_amount,"
				+ " 0 as total_tax_amount_paycur, 0 as total_sur_amount, 0 as total_sur_amount_paycur,"
				+ " 0 as total_mod_amount, 0 as total_mod_amount_paycur "
				+ " from t_pax_ext_carrier_transactions p, t_agent b, t_pnr_passenger pp "
				+ " where p.pnr_pax_id = pp.pnr_pax_id "
				+ " and b.agent_code = '"
				+ agentGSACode
				+ "'"
				+ " and pp.pnr = '"
				+ pnr
				+ "' " + " and b.agent_code =  p.agent_code " + " and p.nominal_code in (19,25) " + ") group by agent_code";

		templete.query(queryString, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				while (rs.next()) {
					invoiceSummaryDTO.setFareAmountLocal(new BigDecimal(rs.getString("total_fare_amount_paycurr")));
					invoiceSummaryDTO.setTaxAmountLocal(new BigDecimal(rs.getString("total_tax_paycurr")));
					invoiceSummaryDTO.setSurchrageAmountLocal(new BigDecimal(rs.getString("total_sur_paycurr")));
					invoiceSummaryDTO.setModifyAmountLocal(new BigDecimal(rs.getString("total_mod_paycurr")));
					invoiceSummaryDTO.setFareAmount(new BigDecimal(rs.getString("total_fare")));
					invoiceSummaryDTO.setTaxAmount(new BigDecimal(rs.getString("total_tax")));
					invoiceSummaryDTO.setModifyAmount(new BigDecimal(rs.getString("total_mod")));
					invoiceSummaryDTO.setSurchrageAmount(new BigDecimal(rs.getString("total_sur")));
				}
				return invoiceSummaryDTO;
			}

		});

		return invoiceSummaryDTO;
	}

	/**
	 * Sum of Agent transactions for a On Account refund
	 */
	public Collection getAgentSalesForRefundInvoicing(Collection<Integer> txnIds) {

		DataSource ds = AirTravelagentsModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);

		StringBuilder sqlBuilder = new StringBuilder("");
		sqlBuilder
				.append("select sum(tr.amount) as sum, sum(tr.amount_local) as sumlocal, tr.agent_code, a.account_code , tr.pnr, ");
		sqlBuilder.append(" a.currency_code from T_AGENT_TRANSACTION  tr, T_AGENT a , T_CURRENCY cur ");
		sqlBuilder.append(" where   tr.agent_code = a.agent_code and");
		sqlBuilder.append(" a.currency_code=cur.currency_code and");
		sqlBuilder.append(" tr.NOMINAL_CODE in (1,7, 10, 11) and");
		sqlBuilder.append(" tr.txn_id in (" + Util.constructINStringForInts(txnIds) + ") ");
		sqlBuilder.append(" group by tr.agent_code, a.account_code, a.currency_code, tr.pnr");

		Object objcol = null;
		try {
			objcol = template.query(sqlBuilder.toString(), new ResultSetExtractor() {

				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

					Collection agentTotalSales = new ArrayList();
					while (rs.next()) {
						AgentTotalSaledDTO agentTotalSaledDTO = new AgentTotalSaledDTO();
						agentTotalSaledDTO.setTotalSales(rs.getBigDecimal("sum"));
						agentTotalSaledDTO.setAgentCode(rs.getString("agent_code"));
						agentTotalSaledDTO.setAccountId(rs.getString("account_code"));
						agentTotalSaledDTO.setCurrencyCode(rs.getString("currency_code"));
						agentTotalSaledDTO.setTotalSalesLocal(rs.getBigDecimal("sumlocal"));
						agentTotalSaledDTO.setPnr(rs.getString("pnr"));
						agentTotalSales.add(agentTotalSaledDTO);
					}
					return agentTotalSales;
				}

			});
		} catch (DataAccessException e) {
			throw new CommonsDataAccessException(e, "airtravelagent.jdbc.error", AirTravelAgentConstants.MODULE_NAME);
		}
		return (Collection) objcol;

	}

	/**
	 * sum of Agent transactions for a given PNR. SQL is such away that alway return one result set while making a
	 * reservation.
	 */
	public Collection getAgentPNRSalesForInvoicing(SearchInvoicesReceiptsDTO searchInvoicesReceiptsDTO) {
		DataSource ds = AirTravelagentsModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);
		String strTnxIDs = Util.constructINStringForInts(searchInvoicesReceiptsDTO.getColTnxIds());
		String sql = getQuery(AGENT_TOTAL_SALES_FOR_INVOICE_RECEIPTS);
		// Object params[]= {strTnxIDs, searchInvoicesReceiptsDTO.getAgentCode()};
		String f1 = setStringToPlaceHolderIndex(sql, 0, strTnxIDs);
		String f2 = setStringToPlaceHolderIndex(f1, 1, "'" + searchInvoicesReceiptsDTO.getAgentCode() + "'");

		Object objcol = null;
		try {
			objcol = template.query(f2, new ResultSetExtractor() {

				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

					Collection agentTotalSales = new ArrayList();
					while (rs.next()) {
						AgentTotalSaledDTO agentTotalSaledDTO = new AgentTotalSaledDTO();
						agentTotalSaledDTO.setTotalSales(rs.getBigDecimal("sum"));
						agentTotalSaledDTO.setAgentCode(rs.getString("agent_code"));
						agentTotalSaledDTO.setAccountId(rs.getString("account_code"));
						agentTotalSaledDTO.setCurrencyCode(rs.getString("currency_code"));
						agentTotalSaledDTO.setTotalSalesLocal(rs.getBigDecimal("sumlocal"));
						agentTotalSaledDTO.setPnr(rs.getString("pnr"));
						agentTotalSaledDTO.setEntityCode(rs.getString("entity_code"));
						agentTotalSales.add(agentTotalSaledDTO);
					}
					return agentTotalSales;
				}

			});
		} catch (DataAccessException e) {
			throw new CommonsDataAccessException(e, "airtravelagent.jdbc.error", AirTravelAgentConstants.MODULE_NAME);
		}
		return (Collection) objcol;
	}
}
