/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.invoicing.core.persistence.hibernate;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airtravelagents.api.util.AirTravelAgentConstants;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.XDBConnectionUtil;
import com.isa.thinair.invoicing.api.dto.InvoiceDTO;
import com.isa.thinair.invoicing.core.persistence.dao.X3InvoiceTransferDAO;

/**
 * @author
 * @isa.module.dao-impl dao-name="x3InvoiceTransferDAO"
 */
public class X3InvoiceTransferDAOImpl extends PlatformBaseHibernateDaoSupport implements X3InvoiceTransferDAO {
	private static Log log = LogFactory.getLog(X3InvoiceTransferDAOImpl.class);

	/**
	 * Method used to transfer invoice data to external system for X3 SERVER Interface table : INT_T_INVOICE
	 * 
	 * @param invoiceCollection
	 * @param accountingSystem
	 * @throws SQLException
	 * @throws CommonsDataAccessException
	 */
	public void insertInvoicesToInterfaceTable(Collection invoiceCollection, XDBConnectionUtil accountingSystem) {

		if (invoiceCollection == null || invoiceCollection.size() == 0) {
			throw new CommonsDataAccessException("airtravelagent.param.nullempty", AirTravelAgentConstants.MODULE_NAME);
		}
		if (accountingSystem != null) {
			Connection connection = accountingSystem.getExternalConnection();
			Iterator it = invoiceCollection.iterator();
			// common sql queries
			String insertStatment = "insert into  INT_T_INVOICE  values (?,?,?,?,?,?,?,?)";
			if (AppSysParamsUtil.isDataTransferToSageEnabled()) {
				insertStatment = "insert into  INT_T_INVOICE (INVOICE_NUMBER,INVOICE_DATE,INVIOCE_PERIOD_FROM,INVIOCE_PERIOD_TO,AGENT_CODE,TOTAL_INVOICE_AMOUNT,INVOICE_AMOUNT_LOCAL,INVOICE_CURRENCY_CODE) values (?,?,?,?,?,?,?,?)";
			}
			String selectStatment = "select * from INT_T_INVOICE where INVOICE_NUMBER=?";
			String deleteStatement = "delete from  INT_T_INVOICE where INVOICE_NUMBER=?";
			PreparedStatement insertStmt = null;
			PreparedStatement selectStmt = null;
			PreparedStatement deleteStmt = null;

			// create the prepared statements
			try {
				insertStmt = connection.prepareStatement(insertStatment);
				selectStmt = connection.prepareStatement(selectStatment);
				deleteStmt = connection.prepareStatement(deleteStatement);
				connection.setAutoCommit(false);
			} catch (SQLException e) {
				log.error("Error when getting prepared statment" + e);
				throw new CommonsDataAccessException(e, "airtravelagent.prepstatment.error", AirTravelAgentConstants.MODULE_NAME);
			}
			try {
				// Iterate through the Invoice List
				while (it.hasNext()) {
					InvoiceDTO invoice = (InvoiceDTO) it.next();
					// Delete If already In Interface Table
					selectStmt.setString(1, String.valueOf(invoice.getInvoiceNumber()));
					ResultSet res = selectStmt.executeQuery();
					int count = 0;
					while (res.next()) {
						count++;
						deleteStmt.setString(1, res.getString("INVOICE_NUMBER"));

						deleteStmt.executeUpdate();
					}
					// end of select and delete
					// set params to insert statment
					settingParamsToInsertStmt(insertStmt, invoice);
				}
			} catch (Exception e) {
				log.error("failed whent transfering", e);
				XDBConnectionUtil.rollback(connection);
				throw new CommonsDataAccessException(e, "airtravelagent.invoicetransfer.error",
						AirTravelAgentConstants.MODULE_NAME);
			}
			XDBConnectionUtil.commit(connection);
		} else {
			log.error("Accounting system couldn't find. Please configure it");
			throw new CommonsDataAccessException("airtravelagent.param.nullempty", AirTravelAgentConstants.MODULE_NAME);
		}
	}

	/**
	 * Method used to set parameters to insert statement and execute the same.
	 * 
	 * @param insertStmt
	 * @param invoice
	 */
	private void settingParamsToInsertStmt(PreparedStatement insertStmt, InvoiceDTO invoice) {
		Date id = invoice.getInvoiceDate();
		java.sql.Date invoiceDate = new java.sql.Date(id.getTime());
		try {
			insertStmt.setString(1, invoice.getInvoiceNumber());
			insertStmt.setDate(2, invoiceDate);
			insertStmt.setDate(3, new java.sql.Date(invoice.getInviocePeriodFrom().getTime()));
			insertStmt.setDate(4, new java.sql.Date(invoice.getInvoicePeriodTo().getTime()));
			insertStmt.setString(5, invoice.getAgentCode());
			insertStmt.setDouble(6, invoice.getInvoiceAmount().doubleValue());
			insertStmt.setDouble(7, invoice.getInvoiceAmountLocal().doubleValue());
			insertStmt.setString(8, invoice.getCurrencyCode());
			insertStmt.executeUpdate();
		} catch (SQLException e) {
			log.error("failed to executeUpdate in settingParamsToInsertStmt() method in X3InvoiceTransferDAOImpl class", e);
		}
	}
}
