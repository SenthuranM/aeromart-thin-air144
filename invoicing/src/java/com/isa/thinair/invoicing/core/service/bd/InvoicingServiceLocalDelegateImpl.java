package com.isa.thinair.invoicing.core.service.bd;

import javax.ejb.Local;

import com.isa.thinair.invoicing.api.service.InvoicingBD;

@Local
public interface InvoicingServiceLocalDelegateImpl extends InvoicingBD {

}