/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.invoicing.core.bl;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airreservation.api.dto.AgentTotalSaledDTO;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.util.AirTravelAgentConstants;
import com.isa.thinair.airtravelagents.api.util.BLUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.FTPServerProperty;
import com.isa.thinair.commons.core.util.FTPUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.invoicing.api.dto.SearchInvoicesReceiptsDTO;
import com.isa.thinair.invoicing.api.model.Invoice;
import com.isa.thinair.invoicing.api.service.InvoicingModuleUtils;
import com.isa.thinair.invoicing.api.util.InvoicingInternalConstants;
import com.isa.thinair.invoicing.core.persistence.dao.InvoiceDAO;
import com.isa.thinair.invoicing.core.persistence.dao.InvoiceJdbcDAO;
import com.isa.thinair.invoicing.core.util.InvoiceNumberGenerator;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.platform.api.constants.PlatformConstants;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.reportingframework.api.service.ReportingFrameworkBD;

/**
 * @author SanjeewaF Main Jira :AARESAA-2363 Invoice Generation - Invoices to be generated for each payment & Refund [ON
 *         ACCOUNT]
 */
public class InvoiceReceiptsGeneratorBL {

	private static final Log log = LogFactory.getLog(InvoiceGeneratorBL.class);

	private static GlobalConfig globalConfig = InvoicingModuleUtils.getGlobalConfig();

	// strings needed for reports
	public static final String REP_HDN_LIVE = "hdnLive";

	public static final String REP_SET_LIVE = "reqHdnLive";

	public static final String REP_RPT_START_DAY = "reqStartDay";

	public static final String REP_LIVE = "LIVE";

	public static final String REP_OFFLINE = "OFFLINE";

	public static final String REPORT_HTML = "HTML";

	public static final String REPORT_REF = "WebReport1";

	public static final String REP_CARRIER_NAME = "CARRIER";

	/**
	 * Generate Invoices for Transaction NOTE: Invoices Will be generated for OnAccount Transactions. AARESAA-2363
	 * 
	 * @param searchInvoicesReceiptsDTO
	 * @throws ModuleException
	 */
	public Collection<Invoice> generateInvoicesForPayments(SearchInvoicesReceiptsDTO searchInvoicesReceiptsDTO)
			throws ModuleException {
		if (log.isDebugEnabled()) {
			log.debug("=============Starting  to Generate Invoices=============================");
		}

		InvoiceJdbcDAO invoiceJdbcDAO = InvoicingModuleUtils.getInvoiceJdbcDAO();
		Collection agentTotalSalesDTOs = invoiceJdbcDAO.getAgentPNRSalesForInvoicing(searchInvoicesReceiptsDTO);
		Collection<Invoice> invoices = createInvoices(agentTotalSalesDTOs, searchInvoicesReceiptsDTO);

		return invoices;
	}

	/**
	 * Method will create invoices with generated invoice no's
	 * 
	 * @param agentTotalSalesDTOs
	 * @param searchInvoicesDTO
	 * @return
	 * @throws ModuleException
	 */
	private Collection<Invoice> createInvoices(Collection agentTotalSalesDTOs, SearchInvoicesReceiptsDTO searchInvoicesDTO)
			throws ModuleException {

		boolean blnPayCurr = false;
		Date txnDate = searchInvoicesDTO.getTxnDate();

		if ("Y".equalsIgnoreCase(InvoicingModuleUtils.getGlobalConfig().getBizParam(
				SystemParamKeys.STORE_PAYMENTS_IN_AGENTS_CURRENCY))
				&& "Y".equalsIgnoreCase(InvoicingModuleUtils.getGlobalConfig().getBizParam(
						SystemParamKeys.INVOICE_IN_AGENTS_CURRENCY))) {
			blnPayCurr = true;
		}

		InvoiceDAO invoiceDAO = InvoicingModuleUtils.getInvoiceDAO();
		Iterator agentTotalSalesIterator;
		String includeCommission = InvoicingModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.AGENT_COMMISSION);
		String baseCurrency = InvoicingModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.BASE_CURRENCY);
		InvoiceJdbcDAO invoiceJdbcDAO = InvoicingModuleUtils.getInvoiceJdbcDAO();
		String invoiceNoFormat = InvoicingModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.INVOICE_NO_FORMAT);

		HashMap<String, Double> commissionCodeValueMap = null;
		HashMap<String, BigDecimal> agentCodeFareMap = null;
		if ("Y".equals(includeCommission)) {
			commissionCodeValueMap = getAgentCommissionCodeValueMap();
			agentCodeFareMap = getAgentCodeFareMap(txnDate, txnDate);
		}

		agentTotalSalesIterator = agentTotalSalesDTOs.iterator();

		Collection<Invoice> invoicesCollection = new ArrayList<Invoice>();

		// iterate throught the agent total sales from Agent Transaction Table
		if (log.isDebugEnabled()) {
			log.debug("========Iterating the AgentTotalSalesDTOs and Preparing the Invoice Collections ================");
		}

		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());
		while (agentTotalSalesIterator.hasNext()) {

			Invoice invoice = new Invoice();
			AgentTotalSaledDTO agentTotalSaledDTO = (AgentTotalSaledDTO) agentTotalSalesIterator.next();

			invoice.setAgentCode(agentTotalSaledDTO.getAgentCode());
			invoice.setInviocePeriodFrom(txnDate);
			invoice.setInvoicePeriodTo(txnDate);
			invoice.setPnr(agentTotalSaledDTO.getPnr());
			invoice.setEntityCode(agentTotalSaledDTO.getEntityCode());

			if ("Y".equals(includeCommission) && agentTotalSaledDTO.getTotalSales().doubleValue() > 0) {
				BigDecimal fareAmount = agentCodeFareMap.get(invoice.getAgentCode());
				Double rate = commissionCodeValueMap.get(agentTotalSaledDTO.getAgentCommissionCode());
				BigDecimal commission = AccelAeroCalculator.getDefaultBigDecimalZero();
				if (fareAmount != null && rate != null) {
					commission = AccelAeroCalculator.multiply(fareAmount,
							AccelAeroCalculator.parseBigDecimal(rate.doubleValue() / 100));
					BigDecimal invoiceAmt = AccelAeroCalculator.subtract(agentTotalSaledDTO.getTotalSales(), commission);
					invoice.setInvoiceAmount(invoiceAmt);
					try {
						invoice.setInvoiceAmountLocal(BLUtil.getAmountInLocal(invoiceAmt, agentTotalSaledDTO.getCurrencyCode(),
								exchangeRateProxy));
					} catch (ModuleException e) {
						invoice.setInvoiceAmountLocal(invoiceAmt);
						agentTotalSaledDTO.setCurrencyCode(baseCurrency);
					}
					invoice.setTotalFareAmount(fareAmount);
				} else {
					invoice.setInvoiceAmount(agentTotalSaledDTO.getTotalSales());
					if (blnPayCurr) {
						invoice.setInvoiceAmountLocal(agentTotalSaledDTO.getTotalSalesLocal());
					} else {
						invoice.setInvoiceAmountLocal(BLUtil.getAmountInLocal(agentTotalSaledDTO.getTotalSales(),
								agentTotalSaledDTO.getCurrencyCode(), exchangeRateProxy));
					}

					invoice.setTotalFareAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
				}

				invoice.setCommissionAmount(commission);

			} else {
				invoice.setInvoiceAmount(agentTotalSaledDTO.getTotalSales());
				if (blnPayCurr) {
					invoice.setInvoiceAmountLocal(agentTotalSaledDTO.getTotalSalesLocal());
				} else {
					try {
						invoice.setInvoiceAmountLocal(BLUtil.getAmountInLocal(agentTotalSaledDTO.getTotalSales(),
								agentTotalSaledDTO.getCurrencyCode(), exchangeRateProxy));
					} catch (ModuleException e) {
						invoice.setInvoiceAmountLocal(agentTotalSaledDTO.getTotalSales());
						agentTotalSaledDTO.setCurrencyCode(baseCurrency);
					}
				}

			}

			invoice.setBillingPeriod(searchInvoicesDTO.getInvoicePeriod());

			invoice.setTransferTimestamp(new Timestamp(new Date().getTime()));
			// keeping the current date as the invoice date.
			invoice.setInvoiceDate(txnDate);

			// if invoice amount is zero keep it as a settle invoice, else its
			// an unsettled invoice
			if (invoice.getInvoiceAmount().compareTo(BigDecimal.ZERO) == 0) {
				invoice.setStatus(InvoicingInternalConstants.InvoiceStatus.INVOICE_SETTLED);
			} else {
				invoice.setStatus(InvoicingInternalConstants.InvoiceStatus.INVOICE_UNSETTLED);
			}

			invoice.setTransferStatus(InvoicingInternalConstants.TransferStatus.NOT_TRANSFERED);
			invoice.setMailStatus(AirTravelAgentConstants.EmailStatus.NOT_SENT);
			invoice.setNumOfTimesMailSent(new Integer(0));

			invoicesCollection.add(invoice);

		}

		Collection<Invoice> invoices = new InvoiceNumberGenerator().getInvoiceNos(txnDate, searchInvoicesDTO.getInvoicePeriod(),
				invoicesCollection, invoiceNoFormat);

		// save the positive and zero invoices.

		invoiceDAO.saveInvoices(invoices);

		log.info("=======Invoice Generation and Saving to Tables is a Success  ============");
		return invoices;
	}

	private HashMap<String, Double> getAgentCommissionCodeValueMap() {
		InvoiceJdbcDAO invoiceJdbcDAO = InvoicingModuleUtils.getInvoiceJdbcDAO();
		return invoiceJdbcDAO.getAgentCommissionMap();

	}

	private HashMap<String, BigDecimal> getAgentCodeFareMap(Date fromDate, Date toDate) {
		InvoiceJdbcDAO invoiceJdbcDAO = InvoicingModuleUtils.getInvoiceJdbcDAO();
		return invoiceJdbcDAO.getAgentFareMap(fromDate, toDate);

	}

	/**
	 * Create Invoice attchment using pdf report
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	public static void setReportAttachment(String detailFileName, Invoice invoice, Agent agent) throws ModuleException {

		ReportingFrameworkBD reportingFrameworkBD = InvoicingModuleUtils.lookupReportingFrameworkBD();

		Date firstday = null;
		Date lastday = null;
		ResultSet resultSet = null;
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
		GregorianCalendar calendar = CalendarUtil.getCalendar(invoice.getInvoiceDate());
		String year = Integer.toString(calendar.get(Calendar.YEAR));
		String month = Integer.toString(calendar.get(Calendar.MONTH) + 1);
		String value = "PDF";
		String agentCode = invoice.getAgentCode();
		String invoiceNo = invoice.getInvoiceNumber();
		String invoiceDate = formatter.format(invoice.getInvoiceDate());
		String id = "UC_REPM_008";
		String reportTemplateDetails = "InvoiceDetailReceiptViewReport.jasper";
		String accountCode = agent.getAccountCode();
		boolean blnPayCurr = false;

		if ("Y".equalsIgnoreCase(globalConfig.getBizParam(SystemParamKeys.STORE_PAYMENTS_IN_AGENTS_CURRENCY))
				&& "Y".equalsIgnoreCase(globalConfig.getBizParam(SystemParamKeys.INVOICE_IN_AGENTS_CURRENCY))) {
			blnPayCurr = true;
		}

		// String agentName = agent.getAgentName();

		String strLogo = AppSysParamsUtil.getReportLogo(false);// globalConfig.getBizParam(SystemParamKeys.REPORT_LOGO);
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String strBase = globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY);
		String strlive = REP_HDN_LIVE;
		String strAgentCurr = agent.getCurrencyCode();

		try {
			Map<String, Object> parameters = new HashMap<String, Object>();
			Map<String, String> parameterSummary = new HashMap<String, String>();
			ReportsSearchCriteria search = new ReportsSearchCriteria();
			ReportsSearchCriteria searchSummary = new ReportsSearchCriteria();
			search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);

			search.setReportType(ReportsSearchCriteria.PNR_INVOICE_DETAILS);
			parameters.put("AGENT_CODE", agentCode);
			parameters.put("BOOKED_DATE", invoiceDate);
			parameters.put("ACCOUNT_CODE", agentCode);
			parameters.put("AGENT_NAME", agent.getAgentDisplayName());
			parameters.put("INVOICE_NO", invoiceNo);
			parameters.put("ID", id);
			parameters.put("AMOUNT", AccelAeroCalculator.formatAsDecimal(invoice.getInvoiceAmount()));
			parameters.put("AMOUNT_LOCAL", invoice.getInvoiceAmountLocal().toString());
			parameters.put("AMT_1", "(" + strBase + ")");
			parameters.put("AMT_2", strAgentCurr);
			parameters.put(REP_CARRIER_NAME, strCarrier);
			parameters.put("PNR", invoice.getPnr());

			GregorianCalendar gc = new GregorianCalendar();

			gc.set(Calendar.YEAR, Integer.parseInt(year));
			gc.set(Calendar.MONTH, (Integer.parseInt(month) - 1));
			gc.set(Calendar.DATE, 1);
			firstday = gc.getTime();
			gc.add(Calendar.DATE, gc.getActualMaximum(Calendar.DAY_OF_MONTH) - 1);
			lastday = gc.getTime();

			search.setInvoiceNumber(invoice.getInvoiceNumber());

			reportingFrameworkBD.storeJasperTemplateRefs("WebReport1", getReportTemplate(reportTemplateDetails));

			resultSet = InvoicingModuleUtils.lookupDataExtractionBD().getPnrInvoiceReceipt(search);

			String reportsRootDir = PlatformConstants.getConfigRootAbsPath() + "/templates/reports/" + strLogo;

			parameters.put("INVOICE_PERIOD", Integer.toString(invoice.getBillingPeriod()));
			parameters.put("OPTION", value);

			if (value.trim().equals("PDF")) {
				parameters.put("IMG", reportsRootDir);
				String fileName = detailFileName;

				byte[] byteStream = reportingFrameworkBD.createPDFReport("WebReport1", parameters, resultSet);

				FTPServerProperty serverProperty = new FTPServerProperty();
				FTPUtil ftpUtil = new FTPUtil();

				ftpUtil.setPassiveMode(globalConfig.isFtpEnablePassiveMode());
				// Code to FTP
				serverProperty.setServerName(globalConfig.getFtpServerName());
				if (globalConfig.getFtpServerUserName() == null) {
					serverProperty.setUserName("");
				} else {
					serverProperty.setUserName(globalConfig.getFtpServerUserName());
				}
				serverProperty.setUserName(globalConfig.getFtpServerUserName());
				if (globalConfig.getFtpServerPassword() == null) {
					serverProperty.setPassword("");
				} else {
					serverProperty.setPassword(globalConfig.getFtpServerPassword());
				}
				ftpUtil.uploadAttachment(fileName, byteStream, serverProperty);
				// End Code to FTP

			}
		} catch (Exception e) {
			log.error("Error in creating invoice summary and detail report" + e.getMessage());
			throw new ModuleException("Error in creating invoice summary and detail report" + e.getMessage());

		}
	}

	/**
	 * Gets the reports file path.
	 * 
	 * @param fileName
	 * @return
	 */
	public static String getReportTemplate(String fileName) {
		String reportsRootDir = null;

		reportsRootDir = PlatformConstants.getConfigRootAbsPath() + "/templates/reports/" + fileName;

		return reportsRootDir;
	}

	/**
	 * Invoices That generate while performing ON Account Refunds
	 * 
	 * @param agenTotalSaledDTOs
	 * @param searchInvoicesReceiptsDTO
	 * @return default serviceResponce
	 * @throws ModuleException
	 */
	public ServiceResponce generateRefundInvoices(Collection<AgentTotalSaledDTO> agenTotalSaledDTOs,
			SearchInvoicesReceiptsDTO searchInvoicesReceiptsDTO) throws ModuleException {

		ServiceResponce serviceResponce = new DefaultServiceResponse(true);

		if (log.isDebugEnabled()) {
			log.debug("=============Starting  to Generate Invoices=============================");
		}
		Collection<Invoice> invoices = createInvoices(agenTotalSaledDTOs, searchInvoicesReceiptsDTO);

		return serviceResponce;
	}
}