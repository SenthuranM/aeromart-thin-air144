package com.isa.thinair.invoicing.core.bl.bsp;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.model.Country;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.util.CurrencyProxy;
import com.isa.thinair.airtravelagents.api.util.AirTravelAgentConstants;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.SFTPUtil;
import com.isa.thinair.invoicing.api.dto.BSPResultsDTO;
import com.isa.thinair.invoicing.api.dto.SearchInvoicesDTO;
import com.isa.thinair.invoicing.api.model.BSPFileLog;
import com.isa.thinair.invoicing.api.model.BSPWeblinkAccount;
import com.isa.thinair.invoicing.api.model.EMDSTransaction;
import com.isa.thinair.invoicing.api.model.bsp.BSPAgentDataDTO;
import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BSPSegmentInfoDTO;
import com.isa.thinair.invoicing.api.model.bsp.BSPTransactionDataDTO;
import com.isa.thinair.invoicing.api.model.bsp.BSPTransactionDataDTO.TaxCodes;
import com.isa.thinair.invoicing.api.model.bsp.BSPTransactionDataDTO.TransactionType;
import com.isa.thinair.invoicing.api.model.bsp.DISHStructured;
import com.isa.thinair.invoicing.api.model.bsp.EnumTypes.ApprovedLocationType;
import com.isa.thinair.invoicing.api.model.bsp.EnumTypes.ConjuctionTktIndicator;
import com.isa.thinair.invoicing.api.model.bsp.EnumTypes.CouponUseIndicator;
import com.isa.thinair.invoicing.api.model.bsp.EnumTypes.DataInputStatusIndicator;
import com.isa.thinair.invoicing.api.model.bsp.EnumTypes.FormOfPaymentType;
import com.isa.thinair.invoicing.api.model.bsp.EnumTypes.NetReportingIndicator;
import com.isa.thinair.invoicing.api.model.bsp.EnumTypes.OPERATION_STATUS;
import com.isa.thinair.invoicing.api.model.bsp.EnumTypes.StatisticalCodeType;
import com.isa.thinair.invoicing.api.model.bsp.EnumTypes.TicketingMode;
import com.isa.thinair.invoicing.api.model.bsp.FlightCoupon;
import com.isa.thinair.invoicing.api.model.bsp.FormOfPayment;
import com.isa.thinair.invoicing.api.model.bsp.RetFileBuilder;
import com.isa.thinair.invoicing.api.model.bsp.filetypes.BasicFileType;
import com.isa.thinair.invoicing.api.model.bsp.filetypes.EMDS;
import com.isa.thinair.invoicing.api.model.bsp.filetypes.RFND;
import com.isa.thinair.invoicing.api.model.bsp.filetypes.TKTT;
import com.isa.thinair.invoicing.api.model.bsp.records.IT02_BasicTransaction;
import com.isa.thinair.invoicing.api.model.bsp.records.IT02_BasicTransaction_CNJ;
import com.isa.thinair.invoicing.api.model.bsp.records.IT03_RelatedTicketInformation;
import com.isa.thinair.invoicing.api.model.bsp.records.IT04_AdditionalSaleInfo;
import com.isa.thinair.invoicing.api.model.bsp.records.IT05_MonetaryAmounts;
import com.isa.thinair.invoicing.api.model.bsp.records.IT06_Itinerary;
import com.isa.thinair.invoicing.api.model.bsp.records.IT06_Itinerary_CNJ;
import com.isa.thinair.invoicing.api.model.bsp.records.IT07_FareCalculation;
import com.isa.thinair.invoicing.api.model.bsp.records.IT08_FormOfPayment;
import com.isa.thinair.invoicing.api.model.bsp.records.IT09_AdditionalInformation;
import com.isa.thinair.invoicing.api.model.bsp.records.IT0A_MiscDocInfo;
import com.isa.thinair.invoicing.api.model.bsp.records.IT0G_ElectronicMiscDocCouponDetailsNRemarks;
import com.isa.thinair.invoicing.api.service.InvoicingModuleUtils;
import com.isa.thinair.invoicing.api.util.InvoicingInternalConstants;
import com.isa.thinair.invoicing.core.persistence.dao.BSPLinkJdbcDAO;
import com.isa.thinair.invoicing.core.service.bd.TransferBSPsToQueue;
import com.isa.thinair.invoicing.core.util.BSPCoreUtils;
import com.isa.thinair.platform.api.constants.PlatformConstants;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;

public class BSPPublishBL {

	private static Log log = LogFactory.getLog(BSPPublishBL.class);

	private SFTPPool sftpPool;

	public void publishDailySales(Date rptPeriodStartDate, Date rptPeriodEndDate, UserPrincipal userPrincipal,
			boolean processErrorsOnly, String specifiedCountryCode,
			String agentCode) throws ModuleException {

		try {
			Collection<Country> countryList = null;
			if (AppSysParamsUtil.getBSPOperationStatus().equals(OPERATION_STATUS.TEST.toString())) {
				countryList = new ArrayList<>();
				Country country = new Country();
				country.setCountryCode(AppSysParamsUtil.getBSPTestCountryCode());
				countryList.add(country);
			} else {
				if (specifiedCountryCode != null) {
					Country country = InvoicingModuleUtils.getAirmasterBD().getBSPCountryByCode(specifiedCountryCode);
					if (country == null) {
						throw new ModuleException("Invalid country code");
					}
					countryList = new ArrayList<>();
					countryList.add(country);
				} else {
					countryList = InvoicingModuleUtils.getAirmasterBD().getBSPCountries();
				}

			}
			List<RetFileBuilder> rfbList = new ArrayList<RetFileBuilder>();
			Map<RetFileBuilder, List<BSPTransactionDataDTO>> loggingDataMap = new HashMap<RetFileBuilder, List<BSPTransactionDataDTO>>();
			CurrencyProxy currencyProxy = new CurrencyProxy(InvoicingModuleUtils.getAirmasterBD());
			
			for (Country country : countryList) {
				String countryCode = country.getCountryCode();

				Collection<BSPAgentDataDTO> bspEnabledAgents = null;
				if (agentCode != null) {
					bspEnabledAgents = new ArrayList<>();
					BSPAgentDataDTO agent = InvoicingModuleUtils.getBSPLinkJdbcDao().getBSPAgent(countryCode, agentCode);
					if (agent != null) {
						bspEnabledAgents.add(agent);
					}
				} else {
					bspEnabledAgents = InvoicingModuleUtils.getBSPLinkJdbcDao().getBSPEnabledAgents(countryCode);

				}

				RetFileBuilder rfb = new RetFileBuilder(countryCode, new Date(), rptPeriodEndDate,
						new Long(AppSysParamsUtil.getBSPRETFileVersion()));
				if (loggingDataMap.get(rfb) == null) {
					loggingDataMap.put(rfb, new ArrayList<BSPTransactionDataDTO>());
				}
	
				for (BSPAgentDataDTO agent : bspEnabledAgents) {
					List<BSPTransactionDataDTO> bspTnxList = BSPCoreUtils.composeBSPTransactionDataDTOs(countryCode,
							agent.getAgentCode(), rptPeriodStartDate, rptPeriodEndDate, userPrincipal, processErrorsOnly);

					for (BSPTransactionDataDTO bspTnx : bspTnxList) {
						Currency currency = currencyProxy.getCurrency(bspTnx.getPayCurrencyCode());
						int payCurrDecimalPlaces = currency.getDecimalPlaces();
						if (bspTnx.getTnxType() == TransactionType.TKTT) {
							TKTT tktt = rfb.getNewTKTTInstance();

							IT02_BasicTransaction it02 = (IT02_BasicTransaction) tktt
									.createNewRecord(IT02_BasicTransaction.class);
							it02.setAgentCode(new Long(agent.getIataCode()));
							it02.setDateOfIssue(bspTnx.getDateOfIssue());
							it02.setPassengerName(bspTnx.getPaxName());
							it02.setTicketingAirlineCodeNo(bspTnx.getTicketingAirlineCode());
							it02.setTicketDocumentNo(bspTnx.getFirstSetOfSegments().getLeft() + "");
							it02.setApprovedLocationType(ApprovedLocationType.RETAIL_AGENCY);
							it02.setIsoCountryCode(countryCode);
							it02.setStatisticalCode(StatisticalCodeType.International);

							IT04_AdditionalSaleInfo it04 = (IT04_AdditionalSaleInfo) tktt
									.createNewRecord(IT04_AdditionalSaleInfo.class);
							it04.setFare(adjustDecimalPlaces(
									AccelAeroCalculator.multiply(bspTnx.getTotalFare(), bspTnx.getCurrencyExchangeRate()),
									payCurrDecimalPlaces));
							it04.setTicketingModeIndicator(TicketingMode.DIRECT);
							it04.setPnrReference(bspTnx.getPnr());
							it04.setTotal(adjustDecimalPlaces(bspTnx.getPayCurrencyAmount(), payCurrDecimalPlaces));

							IT05_MonetaryAmounts it05 = (IT05_MonetaryAmounts) tktt.createNewRecord(IT05_MonetaryAmounts.class);
							// it05.setAmountEnteredByAgent(bspTnx.getPayCurrencyAmount());
							// it05.setAmountPaidByCustomer(bspTnx.getPayCurrencyAmount());
							it05.setAmountEnteredByAgent(AccelAeroCalculator.getDefaultBigDecimalZero(), payCurrDecimalPlaces);
							it05.setAmountPaidByCustomer(AccelAeroCalculator.getDefaultBigDecimalZero(), payCurrDecimalPlaces);
							it05.setCurrencyType(bspTnx.getPayCurrencyCode(), payCurrDecimalPlaces);
							it05.setNetReportingIndicator(NetReportingIndicator.NA);
							it05.setTicketAmount(bspTnx.getPayCurrencyAmount(), payCurrDecimalPlaces);
							if (bspTnx.getTaxCollection().get(TaxCodes.TX1.toString()) != null)
								it05.addTaxFee("TX1",
										AccelAeroCalculator.scaleDefaultRoundingDown(AccelAeroCalculator.multiply(
												bspTnx.getTaxCollection().get(TaxCodes.TX1.toString()),
												bspTnx.getCurrencyExchangeRate())),
										payCurrDecimalPlaces);
							if (bspTnx.getTaxCollection().get(TaxCodes.TX2.toString()) != null)
								it05.addTaxFee("TX2",
										AccelAeroCalculator.scaleDefaultRoundingDown(AccelAeroCalculator.multiply(
												bspTnx.getTaxCollection().get(TaxCodes.TX2.toString()),
												bspTnx.getCurrencyExchangeRate())),
										payCurrDecimalPlaces);
							if (bspTnx.getTaxCollection().get(TaxCodes.TX3.toString()) != null && !bspTnx.getTaxCollection()
									.get(TaxCodes.TX3.toString()).equals(AccelAeroCalculator.getDefaultBigDecimalZero()))
								it05.addTaxFee("TX3",
										AccelAeroCalculator.scaleDefaultRoundingDown(AccelAeroCalculator.multiply(
												bspTnx.getTaxCollection().get(TaxCodes.TX3.toString()),
												bspTnx.getCurrencyExchangeRate())),
										payCurrDecimalPlaces);
							// if (bspTnx.getAgentCommission() != null) {
							// it05.addCommission(CommissionType.STANDARD, new BigDecimal(0L), new BigDecimal(
							// AccelAeroCalculator.formatAsDecimal(bspTnx.getAgentCommission())));
							// }

							IT06_Itinerary it06 = (IT06_Itinerary) tktt.createNewRecord(IT06_Itinerary.class);
							int couponsUsed = 1;
							CouponUseIndicator[] coupons = { CouponUseIndicator.V, CouponUseIndicator.V, CouponUseIndicator.V,
									CouponUseIndicator.V };

							for (BSPSegmentInfoDTO resSeg : bspTnx.getFirstSetOfSegments().getRight()) {

								FlightCoupon flightCoupon = new FlightCoupon();
								coupons[couponsUsed - 1] = CouponUseIndicator.F;
								flightCoupon.setOriginAirportCode(resSeg.getOrigin());
								flightCoupon.setDestinationAirportCode(resSeg.getDestination());
								flightCoupon.setCarrier(resSeg.getCarrierCode());
								flightCoupon.setFareBasis(AppSysParamsUtil.getBSPDefaultFareBasisCode());
								String flightNo = resSeg.getFlightNumber().substring(2);
								flightCoupon.setFlightNumber(flightNo);
								flightCoupon.setFlightDate(resSeg.getDepartureDate());
								flightCoupon.setFlightDepartureTime(resSeg.getDepartureDate());
								flightCoupon.setReservationBookingDesignatior("Y");
								flightCoupon.setSegmentIdentifier(new Long(resSeg.getSegmentIdentifier()));
								if (couponsUsed == 4) {
									IT06_Itinerary it06Second = (IT06_Itinerary) tktt.createNewRecord(IT06_Itinerary.class);
									it06Second.addFlightCoupon(flightCoupon);
								} else {
									it06.addFlightCoupon(flightCoupon);
								}
								couponsUsed++;
							}

							it02.setCouponUseIndicator(coupons);

							if (bspTnx.getSecondSetOfSegments() != null) {
								IT02_BasicTransaction_CNJ it02CNJ = (IT02_BasicTransaction_CNJ) tktt
										.createNewRecord(IT02_BasicTransaction_CNJ.class);

								it02CNJ.setAgentCode(new Long(agent.getIataCode()));
								it02CNJ.setDateOfIssue(bspTnx.getDateOfIssue());
								it02CNJ.setPassengerName(bspTnx.getPaxName());
								it02CNJ.setTicketingAirlineCodeNo(bspTnx.getTicketingAirlineCode());
								it02CNJ.setTicketDocumentNo(bspTnx.getSecondSetOfSegments().getLeft() + "");
								it02CNJ.setApprovedLocationType(ApprovedLocationType.RETAIL_AGENCY);
								it02CNJ.setIsoCountryCode(countryCode);
								it02CNJ.setStatisticalCode(StatisticalCodeType.International);

								it02CNJ.setConjunctionTicketIndicator(ConjuctionTktIndicator.CNJ);

								IT06_Itinerary_CNJ it06CNJ = (IT06_Itinerary_CNJ) tktt.createNewRecord(IT06_Itinerary_CNJ.class);
								int couponsUsedCNJ = 1;
								CouponUseIndicator[] couponsCNJ = { CouponUseIndicator.V, CouponUseIndicator.V,
										CouponUseIndicator.V, CouponUseIndicator.V };

								for (BSPSegmentInfoDTO resSeg : bspTnx.getSecondSetOfSegments().getRight()) {

									FlightCoupon flightCoupon = new FlightCoupon();
									couponsCNJ[couponsUsedCNJ - 1] = CouponUseIndicator.F;
									flightCoupon.setOriginAirportCode(resSeg.getOrigin());
									flightCoupon.setDestinationAirportCode(resSeg.getDestination());
									flightCoupon.setCarrier(resSeg.getCarrierCode());
									flightCoupon.setFareBasis(AppSysParamsUtil.getBSPDefaultFareBasisCode());
									String flightNo = resSeg.getFlightNumber().substring(2);
									flightCoupon.setFlightNumber(flightNo);
									flightCoupon.setFlightDate(resSeg.getDepartureDate());
									flightCoupon.setFlightDepartureTime(resSeg.getDepartureDate());
									flightCoupon.setReservationBookingDesignatior("Y");
									flightCoupon.setSegmentIdentifier(new Long(resSeg.getSegmentIdentifier()));
									if (couponsUsedCNJ == 4) {
										IT06_Itinerary_CNJ it06SecondCNJ = (IT06_Itinerary_CNJ) tktt
												.createNewRecord(IT06_Itinerary_CNJ.class);
										it06SecondCNJ.addFlightCoupon(flightCoupon);
									} else {
										it06CNJ.addFlightCoupon(flightCoupon);
									}
									couponsUsedCNJ++;

								}
								it02CNJ.setCouponUseIndicator(couponsCNJ);
							}

							IT07_FareCalculation it07 = (IT07_FareCalculation) tktt.createNewRecord(IT07_FareCalculation.class);
							it07.setFareCalculationModeIndicator("0");
							it07.setFareCalculationPricingIndicator("0");

							String[] fareCalcAreas = bspTnx.getFareCalculationAreas();
							if (fareCalcAreas == null || fareCalcAreas[0] == null) {
								throw new BSPException("Fare Calculation Area is missing");
							} else {
								if (fareCalcAreas[1] == null) {
									it07.addFareCalculationArea(fareCalcAreas[0], 1);
								} else {
									it07.addFareCalculationArea(fareCalcAreas[0], 1);
									if (fareCalcAreas[2] == null) {
										it07.addFareCalculationArea(fareCalcAreas[1], 2);
									} else {
										it07.addFareCalculationArea(fareCalcAreas[1], 2);
										if (fareCalcAreas[3] == null) {
											IT07_FareCalculation it07Sec = (IT07_FareCalculation) tktt
													.createNewRecord(IT07_FareCalculation.class);
											it07Sec.setFareCalculationModeIndicator("0");
											it07Sec.setFareCalculationPricingIndicator("0");
											it07Sec.addFareCalculationArea(fareCalcAreas[2], 3);
										} else {
											IT07_FareCalculation it07Sec = (IT07_FareCalculation) tktt
													.createNewRecord(IT07_FareCalculation.class);
											it07Sec.setFareCalculationModeIndicator("0");
											it07Sec.setFareCalculationPricingIndicator("0");

											it07Sec.addFareCalculationArea(fareCalcAreas[2], 3);
											it07Sec.addFareCalculationArea(fareCalcAreas[3], 4);
										}
									}
								}
							}

							IT08_FormOfPayment it08 = (IT08_FormOfPayment) tktt.createNewRecord(IT08_FormOfPayment.class);
							FormOfPayment fop = new FormOfPayment(bspTnx.getPayCurrencyCode(), payCurrDecimalPlaces);
							fop.setPaymentAmount(bspTnx.getPayCurrencyAmount());
							fop.setFormOfPaymentType(FormOfPaymentType.CA);
							fop.setCustomerFileReference(bspTnx.getPnr());
							it08.addFormOfPayment(fop);

							IT09_AdditionalInformation it09 = (IT09_AdditionalInformation) tktt
									.createNewRecord(IT09_AdditionalInformation.class);
							it09.addFormOfPayment(FormOfPaymentType.CA);
							it09.addFormOfPayment(FormOfPaymentType.CA);
						} else if (bspTnx.getTnxType() == TransactionType.EMDS) {
							// EMDS
							EMDS emds = rfb.getNewEMDSInstance();
							IT02_BasicTransaction it02 = (IT02_BasicTransaction) emds
									.createNewRecord(IT02_BasicTransaction.class);
							it02.setAgentCode(new Long(agent.getIataCode()));
							it02.setDateOfIssue(bspTnx.getDateOfIssue());
							it02.setPassengerName(bspTnx.getPaxName());
							it02.setTicketingAirlineCodeNo(bspTnx.getTicketingAirlineCode());
							it02.setTicketDocumentNo(bspTnx.getEmdsTransaction().getEmdsTicketNo());
							it02.setApprovedLocationType(ApprovedLocationType.RETAIL_AGENCY);
							it02.setIsoCountryCode(countryCode);
							it02.setStatisticalCode(StatisticalCodeType.International);
							CouponUseIndicator[] coupons = { CouponUseIndicator.V, CouponUseIndicator.V, CouponUseIndicator.V,
									CouponUseIndicator.V };
							it02.setCouponUseIndicator(coupons);

							IT04_AdditionalSaleInfo it04 = (IT04_AdditionalSaleInfo) emds
									.createNewRecord(IT04_AdditionalSaleInfo.class);
							it04.setFare(adjustDecimalPlaces(bspTnx.getPayCurrencyAmount(), payCurrDecimalPlaces));
							it04.setTicketingModeIndicator(TicketingMode.DIRECT);
							it04.setPnrReference(bspTnx.getPnr());
							it04.setTotal(adjustDecimalPlaces(bspTnx.getPayCurrencyAmount(), payCurrDecimalPlaces));
							it04.setServicingAirlineId(bspTnx.getTicketingAirlineCode());

							IT05_MonetaryAmounts it05 = (IT05_MonetaryAmounts) emds.createNewRecord(IT05_MonetaryAmounts.class);
							it05.setAmountEnteredByAgent(AccelAeroCalculator.getDefaultBigDecimalZero(), payCurrDecimalPlaces);
							it05.setAmountPaidByCustomer(AccelAeroCalculator.getDefaultBigDecimalZero(), payCurrDecimalPlaces);
							it05.setCurrencyType(bspTnx.getPayCurrencyCode(), payCurrDecimalPlaces);
							it05.setNetReportingIndicator(NetReportingIndicator.NA);
							it05.setTicketAmount(bspTnx.getPayCurrencyAmount(), payCurrDecimalPlaces);

							IT0G_ElectronicMiscDocCouponDetailsNRemarks it0g = (IT0G_ElectronicMiscDocCouponDetailsNRemarks) emds
									.createNewRecord(IT0G_ElectronicMiscDocCouponDetailsNRemarks.class);
							it0g.setEmdCouponNumber(1L);
							it0g.setEmdCouponValue(bspTnx.getPayCurrencyAmount());
							it0g.setEmdCouponCurrency(bspTnx.getPayCurrencyCode(), payCurrDecimalPlaces);
							it0g.setEmdReasonForIssuanceSubCode(bspTnx.getEmdsTransaction().getRequestForIssuanceSubCode());
							it0g.setEmdFeeOwnerAirlineDesignator(countryCode);
							it0g.setEmdConsumedAtIssuanceIndicator(CommonsConstants.YES);
							it0g.setEmdRemarks(EMDSTransaction.RFISC.UPGRADES.getRemark());

							IT08_FormOfPayment it08 = (IT08_FormOfPayment) emds.createNewRecord(IT08_FormOfPayment.class);
							FormOfPayment fop = new FormOfPayment(bspTnx.getPayCurrencyCode(), payCurrDecimalPlaces);
							fop.setPaymentAmount(bspTnx.getPayCurrencyAmount());
							fop.setFormOfPaymentType(FormOfPaymentType.CA);
							fop.setCustomerFileReference(bspTnx.getPnr());
							it08.addFormOfPayment(fop);

							IT09_AdditionalInformation it09 = (IT09_AdditionalInformation) emds
									.createNewRecord(IT09_AdditionalInformation.class);
							it09.addFormOfPayment(FormOfPaymentType.CA);
							it09.addFormOfPayment(FormOfPaymentType.CA);

							IT0A_MiscDocInfo it0A = (IT0A_MiscDocInfo) emds.createNewRecord(IT0A_MiscDocInfo.class);
							it0A.setReasonForIssuanceCode(EMDSTransaction.RFIC.A.toString());

						} else {

							boolean isFirstPaymentRefund = (bspTnx.getOriginalEMDSTransaction() == null ? true : false);
							RFND rfnd = rfb.getNewRFNDInstance();
							IT02_BasicTransaction it02 = (IT02_BasicTransaction) rfnd
									.createNewRecord(IT02_BasicTransaction.class);
							it02.setAgentCode(new Long(agent.getIataCode()));
							it02.setDateOfIssue(bspTnx.getDateOfIssue());
							it02.setPassengerName(bspTnx.getPaxName());
							it02.setTicketingAirlineCodeNo(bspTnx.getTicketingAirlineCode());

							if (isFirstPaymentRefund) {
								it02.setTicketDocumentNo(bspTnx.getFirstSetOfSegments().getLeft() + "");
							} else {
								it02.setTicketDocumentNo(bspTnx.getOriginalEMDSTransaction().getEmdsTicketNo());
							}

							// it02.setApprovedLocationType(ApprovedLocationType.RETAIL_AGENCY);
							it02.setIsoCountryCode(countryCode);
							it02.setStatisticalCode(StatisticalCodeType.International);
							it02.setDataInputStatusIndicator(DataInputStatusIndicator.M);
							// TODO set the correct indicator

							IT03_RelatedTicketInformation it03 = (IT03_RelatedTicketInformation) rfnd
									.createNewRecord(IT03_RelatedTicketInformation.class);

							if (isFirstPaymentRefund) {
								int[] couponNoIdentifiers = new int[4];
								for (int i = 0; i < bspTnx.getFirstSetOfSegments().getRight().size(); ++i) {
									couponNoIdentifiers[i] = i + 1;
								}
								StringBuffer sbCouponNoID = new StringBuffer();
								for (int j : couponNoIdentifiers) {
									sbCouponNoID.append("" + j + "");
								}
								it03.addTicketCoupon(sbCouponNoID.toString(),
										bspTnx.getFirstSetOfSegments().getLeft().toString());
								if (bspTnx.getSecondSetOfSegments() != null) {
									couponNoIdentifiers = new int[4];
									for (int i = 0; i < bspTnx.getSecondSetOfSegments().getRight().size(); ++i) {
										couponNoIdentifiers[i] = i + 1;
									}
									sbCouponNoID = new StringBuffer();
									for (int j : couponNoIdentifiers) {
										sbCouponNoID.append("" + j + "");
									}
									it03.addTicketCoupon(sbCouponNoID.toString(),
											bspTnx.getSecondSetOfSegments().getLeft().toString());
								}
							} else {
								// Defaulting first coupon for EMDS refund
								it03.addTicketCoupon("1000", bspTnx.getOriginalEMDSTransaction().getEmdsTicketNo());
							}
							it03.setDateOfIssueRelatedDoc(bspTnx.getOriginalPaymentTnxDate());

							IT05_MonetaryAmounts it05 = (IT05_MonetaryAmounts) rfnd.createNewRecord(IT05_MonetaryAmounts.class);
							// it05.setAmountEnteredByAgent(bspTnx.getPayCurrencyAmount().negate());
							// it05.setAmountPaidByCustomer(bspTnx.getPayCurrencyAmount().negate());
							it05.setAmountEnteredByAgent(AccelAeroCalculator.getDefaultBigDecimalZero(), payCurrDecimalPlaces);
							it05.setAmountPaidByCustomer(AccelAeroCalculator.getDefaultBigDecimalZero(), payCurrDecimalPlaces);
							it05.setCurrencyType(bspTnx.getPayCurrencyCode(), payCurrDecimalPlaces);
							it05.setNetReportingIndicator(NetReportingIndicator.NA);
							it05.setTicketAmount(bspTnx.getPayCurrencyAmount().negate(), payCurrDecimalPlaces);

							IT08_FormOfPayment it08 = (IT08_FormOfPayment) rfnd.createNewRecord(IT08_FormOfPayment.class);
							FormOfPayment fop = new FormOfPayment(bspTnx.getPayCurrencyCode(), payCurrDecimalPlaces);
							fop.setPaymentAmount(bspTnx.getPayCurrencyAmount().negate());
							fop.setFormOfPaymentType(FormOfPaymentType.CA);
							fop.setCustomerFileReference(bspTnx.getPnr());
							it08.addFormOfPayment(fop);
						}
						loggingDataMap.get(rfb).add(bspTnx);
					}
				}
				if (rfb.getFiles() != null && rfb.getFiles().size() > 0) {
					boolean isErrorInFileType = false;
					for (DISHStructured dishStructured : rfb.getFiles()) {
						BasicFileType fileType = (BasicFileType) dishStructured;
						if (!fileType.isValidDISHFormat()) {
							log.error("Error in Dish Format : FileType - " + fileType.getFileType().toString() + " for country "
									+ rfb.getCountryCode());
							isErrorInFileType = true;
							break;
						}
					}
					if (!isErrorInFileType) {
						rfbList.add(rfb);
					}
				} else {
					rfbList.add(rfb);
				}
			}

			// writing text files and logging
			processRFBList(rfbList, loggingDataMap, userPrincipal);
		} catch (BSPException bspExp) {
			log.error("Error occured while publishing RET ", bspExp);
			throw new ModuleException("Error occured while generating RET ", bspExp);
		} finally {
			if (sftpPool != null) {
				sftpPool.shutdown();
			}
		}
	}

	private void processRFBList(List<RetFileBuilder> rfbList, Map<RetFileBuilder, List<BSPTransactionDataDTO>> loggingDataMap,
			UserPrincipal userPrincipal) throws ModuleException, BSPException {

		List<String> failedFileList = new ArrayList<String>();

		Exception lastException = null;
		for (RetFileBuilder rfb : rfbList) {
			int fileSeq = BSPCoreUtils.getDishFileSeqence(rfb.getCountryCode(), new Date());
			String dishFileName = rfb.getRETFileName(Integer.parseInt(AppSysParamsUtil.getBSPTicketingAirlineCode()), fileSeq);

			boolean fileCreationSuccess = false;
			boolean fileLoggingSuccess = false;
			try {
				log.info("Started Writing File: " + dishFileName);
				writeTextFile(dishFileName, rfb.getDISHFmtData());
				log.info("Finished Writing File: " + dishFileName);
				fileCreationSuccess = true;
				BSPCoreUtils.recordBSPFileLog(rfb.getCountryCode(), fileSeq, userPrincipal, dishFileName,
						BSPFileLog.RET_FILE_STATUS.CREATED, loggingDataMap.get(rfb));
				fileLoggingSuccess = true;

			} catch (Exception e) {
				log.error("Failed in generation RET file:" + dishFileName, e);
				failedFileList.add(dishFileName);
				lastException = e;
			} finally {
				if (fileCreationSuccess && !fileLoggingSuccess) {
					File createdDishFile = new File(PlatformConstants.getAbsBSPUploadPath() + File.separatorChar + dishFileName);
					createdDishFile.delete();
				}
				if (!fileCreationSuccess || (fileCreationSuccess && !fileLoggingSuccess)) {
					if (!fileLoggingSuccess) {
						InvoicingModuleUtils.getBSPLinkDao().deleteBSPFileLog(dishFileName);
					}
					BSPCoreUtils.logTransactionsForResubmission(loggingDataMap.get(rfb), rfb.getCountryCode(),
							"Submission failed", userPrincipal.getUserId());
				}

			}
		}
		if (!failedFileList.isEmpty()) {
			Throwable throwable = new Throwable(failedFileList.toString(), lastException);
			throw new ModuleException(throwable, "invoicing.bsp.ret.file.genration.failed");
		}
	}

	public void uploadRetFiles() throws ModuleException {
		// Get all created ret files which are in CREATED/ UPLOAD failed status
		List<BSPFileLog> uploadPendingFilesList = InvoicingModuleUtils.getBSPLinkDao().getUploadPendingRetFiles();

		List<String> uploadFailedFileList = new ArrayList<String>();
		Exception lastException = null;

		if (AppSysParamsUtil.isBSPEnhancedUserActivated()) {
			if (uploadPendingFilesList != null && !uploadPendingFilesList.isEmpty()) {
				List<BSPWeblinkAccount> sftpAccounts = InvoicingModuleUtils.getBSPLinkDao().getBSPSftpAccounts();
				try {
					sftpPool = new SFTPPool(getBSPSftpMap(sftpAccounts));
					for (BSPFileLog bspFile : uploadPendingFilesList) {
						boolean uploadStatus = true;
						try {
							uploadFile(sftpPool.getSFTPUtil(bspFile.getCountryCode()), bspFile.getFileName());
						} catch (Exception e) {
							log.error("Failed in uploading RET file:" + bspFile.getFileName(), e);
							uploadStatus = false;
							uploadFailedFileList.add(bspFile.getFileName());
							lastException = e;
						} finally {
							bspFile.setRETFileStatus((uploadStatus == true)
									? BSPFileLog.RET_FILE_STATUS.UPLOADED.toString()
									: BSPFileLog.RET_FILE_STATUS.UPLOAD_FAILED.toString());
							InvoicingModuleUtils.getBSPLinkDao().saveOrUpdateBSPFileLog(bspFile);
						}
					}
				} finally {
					if (sftpPool != null) {
						sftpPool.shutdown();
					}
				}
			}

			if (!uploadFailedFileList.isEmpty()) {
				Throwable throwable = new Throwable(uploadFailedFileList.toString(), lastException);
				throw new ModuleException(throwable, "invoicing.bsp.ret.file.upload.failed");
			}
		}
	}

	public void uploadReportFiles() throws ModuleException {
		// Get all created ret files which are in CREATED/ UPLOAD failed status
		List<BSPFileLog> uploadPendingFilesList = InvoicingModuleUtils.getBSPLinkDao().getUploadPendingRetFiles();

		if (AppSysParamsUtil.isBSPEnhancedUserActivated()) {
			if (uploadPendingFilesList != null && !uploadPendingFilesList.isEmpty()) {
				List<BSPWeblinkAccount> sftpAccounts = InvoicingModuleUtils.getBSPLinkDao().getBSPSftpAccounts();
				sftpPool = new SFTPPool(getBSPSftpMap(sftpAccounts));
				for (BSPFileLog bspFile : uploadPendingFilesList) {
					boolean uploadStatus = true;
					try {
						uploadFile(sftpPool.getSFTPUtil(bspFile.getCountryCode()), bspFile.getFileName());
					} catch (Exception e) {
						log.error("SFTP file upload failed for RET file:" + bspFile.getFileName(), e);
						uploadStatus = false;
						throw new ModuleException("SFTP file upload failed for RET file:" + bspFile.getFileName(), e);
					} finally {
						bspFile.setRETFileStatus((uploadStatus == true)
								? BSPFileLog.RET_FILE_STATUS.UPLOADED.toString()
								: BSPFileLog.RET_FILE_STATUS.UPLOAD_FAILED.toString());
						InvoicingModuleUtils.getBSPLinkDao().saveOrUpdateBSPFileLog(bspFile);
					}
				}
				sftpPool.shutdown();
			}
		}
	}

	class SFTPPool {
		private Map<String, BSPWeblinkAccount> ftpConfig = new HashMap<String, BSPWeblinkAccount>();
		private Map<String, SFTPUtil> sftpMap = new HashMap<String, SFTPUtil>();

		SFTPPool(Map<String, BSPWeblinkAccount> ftpConfig) {
			if (ftpConfig != null) {
				this.ftpConfig = ftpConfig;
			}
		}

		SFTPUtil getSFTPUtil(String countryCode) {
			if (!sftpMap.containsKey(countryCode)) {
				if (ftpConfig.containsKey(countryCode)) {
					BSPWeblinkAccount sftpAccount = ftpConfig.get(countryCode);
					SFTPUtil sftpUtil = new SFTPUtil(sftpAccount.getHost(), sftpAccount.getUsername(), sftpAccount.getPassword());
					GlobalConfig globalConfig = InvoicingModuleUtils.getGlobalConfig();
					if (globalConfig.getUseProxy()) {
						sftpUtil.setHttpProxy(globalConfig.getHttpProxy(), globalConfig.getHttpPort());
					}
					sftpMap.put(countryCode, sftpUtil);
				}
			}

			if (sftpMap.containsKey(countryCode)) {
				return sftpMap.get(countryCode);
			}
			return null;
		}

		void shutdown() {
			for (SFTPUtil sftpUtil : sftpMap.values()) {
				sftpUtil.closeAndLogout();
			}
			sftpMap = new HashMap<String, SFTPUtil>();
		}
	}

	private void writeTextFile(String fileName, String content) throws IOException {

		File folderCheck = new File(PlatformConstants.getAbsBSPUploadPath());

		if (folderCheck.canWrite()) {
			StringBuilder localFilePath = new StringBuilder();
			localFilePath.append(PlatformConstants.getAbsBSPUploadPath()).append(File.separatorChar);
			localFilePath.append(fileName);

			File file = new File(localFilePath.toString());
			FileUtils.writeStringToFile(file, content);
		} else {
			throw new IOException(fileName + ":RET file writing failed due to limited permissions");
		}
	}

	private void uploadFile(SFTPUtil sftp, String fileName) throws SftpException, JSchException {
		if (sftp != null) {
			StringBuilder localFilePath = new StringBuilder();
			localFilePath.append(PlatformConstants.getAbsBSPUploadPath()).append(File.separatorChar);
			localFilePath.append(fileName);
			sftp.uploadFile(localFilePath.toString(), fileName);
		} else {
			log.error("FATAL! NO BSP SFTP CONFIG IS AVAILABLE TO UPLOAD FILE " + fileName);
		}
	}

	private String adjustDecimalPlaces(BigDecimal decimal, int decimalPlaces) {
		String str = decimal.toString();
		String arr[] = str.split("\\.");
		if (arr.length > 1) {
			String dec = arr[1];
			if (dec.length() < decimalPlaces) {
				dec = dec + getZeros(decimalPlaces - dec.length());
			} else if (dec.length() > decimalPlaces) {
				dec = dec.substring(0, decimalPlaces);
			}
			str = arr[0] + dec;
		}
		return str;
	}

	private String getZeros(int i) {
		StringBuffer bf = new StringBuffer();
		while (i > 0) {
			bf.append("0");
			i--;
		}
		return bf.toString();
	}

	private Map<String, BSPWeblinkAccount> getBSPSftpMap(List<BSPWeblinkAccount> sftpAccounts) {
		Map<String, BSPWeblinkAccount> sftpCredentialsMap = new HashMap<String, BSPWeblinkAccount>();

		for (BSPWeblinkAccount sftpAccount : sftpAccounts) {
			sftpCredentialsMap.put(sftpAccount.getCountryCode(), sftpAccount);
		}

		return sftpCredentialsMap;
	}
	
	/**
	 * Method used to transfer BSP sales summary to queue
	 * 
	 * @param transferBSPsDTO
	 * @throws ModuleException
	 */
	public void transferBSPsToQueue(SearchInvoicesDTO transferBSPsDTO) throws ModuleException {
		
		String transferToExternal = InvoicingModuleUtils.getInvoicingConfig()
				.getTransferToSqlServer();
		if (transferToExternal == null ||
				!(AirTravelAgentConstants.TRANSFER_TO_EXTERNAL_SYSTEM .equalsIgnoreCase(transferToExternal))) { 
			throw new ModuleException("BSP.exdb not configured"); 
		}
		
		BSPLinkJdbcDAO bspJdbcDAO = InvoicingModuleUtils.getBSPLinkJdbcDao();
		Date startDate = transferBSPsDTO.getInvoiceDateFrom();
		Date endDate = transferBSPsDTO.getInvoiceDateTo();

		Collection bSPsales = null;

		// retrieve BSPs for all agents.

		try {
			bSPsales = bspJdbcDAO.getBSPsToTransfer(startDate,endDate);
		} catch (CommonsDataAccessException e) {
			log.error("Error in getting BSP for Period " + startDate+ " Enddate "+ endDate, e);
		}

		/* if there were no bsp then throw exception to the user.
		 if not, insert the retrieved data to the internal summary table. */
		if (!(bSPsales == null || bSPsales.size() == 0)) {
			
			bspJdbcDAO.insertBSPsToInternalSummaryTable(startDate,endDate,bSPsales,
							InvoicingInternalConstants.TransferStatus.IN_PROGRESS);
			
					new TransferBSPsToQueue().transferToQueue(bSPsales);
		} else {
			throw new ModuleException("BSP not found", AirTravelAgentConstants.MODULE_NAME);
		}
	}
}
