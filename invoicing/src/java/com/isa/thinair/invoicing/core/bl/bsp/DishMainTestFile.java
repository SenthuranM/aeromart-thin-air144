package com.isa.thinair.invoicing.core.bl.bsp;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.ETLRFileBuilder;
import com.isa.thinair.invoicing.api.model.bsp.ElementFactory;
import com.isa.thinair.invoicing.api.model.bsp.ElementFactory.DE_STATUS;
import com.isa.thinair.invoicing.api.model.bsp.ElementFactory.DISH_ELEMENT;
import com.isa.thinair.invoicing.api.model.bsp.ElementMetaData;
import com.isa.thinair.invoicing.api.model.bsp.EnumTypes.ApprovedLocationType;
import com.isa.thinair.invoicing.api.model.bsp.EnumTypes.CommissionType;
import com.isa.thinair.invoicing.api.model.bsp.EnumTypes.CouponUseIndicator;
import com.isa.thinair.invoicing.api.model.bsp.EnumTypes.FormOfPaymentType;
import com.isa.thinair.invoicing.api.model.bsp.EnumTypes.NetReportingIndicator;
import com.isa.thinair.invoicing.api.model.bsp.EnumTypes.TicketingMode;
import com.isa.thinair.invoicing.api.model.bsp.FlightCoupon;
import com.isa.thinair.invoicing.api.model.bsp.FormOfPayment;
import com.isa.thinair.invoicing.api.model.bsp.RecordBuilder;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory;
import com.isa.thinair.invoicing.api.model.bsp.RecordFactory.DISH_FILE_TYPE;
import com.isa.thinair.invoicing.api.model.bsp.RecordMetaData;
import com.isa.thinair.invoicing.api.model.bsp.RetFileBuilder;
import com.isa.thinair.invoicing.api.model.bsp.filetypes.CANX;
import com.isa.thinair.invoicing.api.model.bsp.filetypes.ETLR;
import com.isa.thinair.invoicing.api.model.bsp.filetypes.TKTT;
import com.isa.thinair.invoicing.api.model.bsp.records.IT02_BasicTransaction;
import com.isa.thinair.invoicing.api.model.bsp.records.IT04_AdditionalSaleInfo;
import com.isa.thinair.invoicing.api.model.bsp.records.IT05_MonetaryAmounts;
import com.isa.thinair.invoicing.api.model.bsp.records.IT06_Itinerary;
import com.isa.thinair.invoicing.api.model.bsp.records.IT07_FareCalculation;
import com.isa.thinair.invoicing.api.model.bsp.records.IT08_FormOfPayment;
import com.isa.thinair.invoicing.api.model.bsp.records.IT09_AdditionalInformation;
import com.isa.thinair.invoicing.api.model.bsp.records.X00_25_Ticket_Header_Record;
import com.isa.thinair.invoicing.api.model.bsp.records.X00_30_Ticket_Header_Record;
import com.isa.thinair.invoicing.api.model.bsp.records.X00_50_Ticket_Header_Record;
import com.isa.thinair.invoicing.api.model.bsp.records.X00_90_Ticket_Header_Record;
import com.isa.thinair.invoicing.api.model.bsp.records.X00_Mutation_Ticket_Header_Record;
import com.isa.thinair.invoicing.api.model.bsp.records.X00_Ticket_Header_Record;
import com.isa.thinair.invoicing.api.model.bsp.records.X34_Mutation_Fare_Record;
import com.isa.thinair.invoicing.api.model.bsp.records.X36_Ticket_Tax;
import com.isa.thinair.invoicing.api.model.bsp.records.X40_Commission_Data;
import com.isa.thinair.invoicing.api.model.bsp.records.X45_FOP;
import com.isa.thinair.invoicing.api.model.bsp.records.X60_Coupon;
import com.isa.thinair.invoicing.api.model.bsp.records.X70_Exchange_Records;
import com.isa.thinair.invoicing.api.model.bsp.records.X74_Recent_Exchange_Record;
import com.isa.thinair.invoicing.api.model.bsp.records.X80_Alternative_Flight_History_Record;
import com.isa.thinair.invoicing.api.model.bsp.records.X92_Coupon_Status_History_Record;
import com.isa.thinair.invoicing.api.model.bsp.records.X94_Revalidation_Record;
import com.isa.thinair.invoicing.api.model.bsp.records.XA0_Extend_Alternate_Coupon_Flight_History;
import com.isa.thinair.invoicing.core.bl.RapidReportFiles.RETFileGenerationBL;

public class DishMainTestFile {
	public static void main(String args[]) throws BSPException {
		// printMandatoryElements(DISH_FILE_TYPE.CANX);
		// printGeneration();
		printETLR();
	}

	public static void printGeneration() throws BSPException {
		System.out.println("DISH gen started......");
		RetFileBuilder retBuilder = new RetFileBuilder("WW", new Date(), new Date(), 201L);
		TKTT tktt = retBuilder.getNewTKTTInstance();

		IT02_BasicTransaction it02 = (IT02_BasicTransaction) tktt.createNewRecord(IT02_BasicTransaction.class);
		it02.setAgentCode(99999999L);
		it02.setDateOfIssue(new Date());
		it02.setPassengerName("DILAN ANURUDDHA");
		it02.setTicketingAirlineCodeNo("986");
		it02.setTicketDocumentNo("9862400000000");
		it02.setCouponUseIndicator(new CouponUseIndicator[] { CouponUseIndicator.F, CouponUseIndicator.V, CouponUseIndicator.V,
				CouponUseIndicator.V });
		it02.setApprovedLocationType(ApprovedLocationType.RETAIL_AGENCY);
		it02.setIsoCountryCode("WW");

		IT04_AdditionalSaleInfo it04 = (IT04_AdditionalSaleInfo) tktt.createNewRecord(IT04_AdditionalSaleInfo.class);
		// it04.setFare(new BigDecimal("14300"), 2);
		it04.setTicketingModeIndicator(TicketingMode.DIRECT);
		it04.setPnrReference("SEQK6R");

		IT05_MonetaryAmounts it05 = (IT05_MonetaryAmounts) tktt.createNewRecord(IT05_MonetaryAmounts.class);
		it05.setAmountEnteredByAgent(new BigDecimal("777.77"), 2);
		it05.setAmountPaidByCustomer(new BigDecimal("888.88"), 2);
		it05.setCurrencyType("LKR", 2);
		it05.setNetReportingIndicator(NetReportingIndicator.NR);
		it05.setTicketAmount(new BigDecimal("222.22"), 2);
		it05.addTaxFee("TX1", new BigDecimal("11.11"), 2);
		it05.addTaxFee("TX2", new BigDecimal("22.22"), 2);
		it05.addTaxFee("XT", new BigDecimal("33.33"), 2);
		it05.addCommission(CommissionType.STANDARD, new BigDecimal("55"), new BigDecimal("66"), 2);

		IT06_Itinerary it06 = (IT06_Itinerary) tktt.createNewRecord(IT06_Itinerary.class);
		FlightCoupon flightCoupon = new FlightCoupon();
		flightCoupon.setOriginAirportCode("SHJ");
		flightCoupon.setDestinationAirportCode("CMB");
		flightCoupon.setCarrier("G9");
		flightCoupon.setFareBasis("YOW");
		flightCoupon.setFlightNumber("0505");
		flightCoupon.setFlightDate(new Date());
		flightCoupon.setFlightDepartureTime(new Date());
		flightCoupon.setReservationBookingDesignatior("Y");
		flightCoupon.setSegmentIdentifier(1L);
		it06.addFlightCoupon(flightCoupon);

		IT07_FareCalculation it07 = (IT07_FareCalculation) tktt.createNewRecord(IT07_FareCalculation.class);
		it07.setFareCalculationModeIndicator("0");
		it07.addFareCalculationArea("GVA XB YMQ 71.50 RVYDM XB GVA 71.50 RVYDM NUC143.00 END ROE1.00000 YQ30.00 JD11.24", 1);
		it07.addFareCalculationArea("QV3.64 GS12.12", 2);

		IT08_FormOfPayment it08 = (IT08_FormOfPayment) tktt.createNewRecord(IT08_FormOfPayment.class);
		FormOfPayment fop = new FormOfPayment("LKR", 2);
		fop.setPaymentAmount(new BigDecimal("200.00"));
		fop.setFormOfPaymentType(FormOfPaymentType.CA);
		fop.setCustomerFileReference("SEQK6R");
		it08.addFormOfPayment(fop);

		IT09_AdditionalInformation it09 = (IT09_AdditionalInformation) tktt.createNewRecord(IT09_AdditionalInformation.class);

		CANX canx = retBuilder.getNewCANXInstance();
		IT02_BasicTransaction cnxIt02 = (IT02_BasicTransaction) canx.createNewRecord(IT02_BasicTransaction.class);
		cnxIt02.setAgentCode(99999999L);
		cnxIt02.setDateOfIssue(new Date());
		cnxIt02.setPassengerName("DILAN Karanachcharighe Anuruddha");
		cnxIt02.setTicketingAirlineCodeNo("986");
		cnxIt02.setTicketDocumentNo("9862400000000");
		cnxIt02.setCouponUseIndicator(new CouponUseIndicator[] { CouponUseIndicator.V, CouponUseIndicator.V,
				CouponUseIndicator.V, CouponUseIndicator.V });

		System.out.println("------------------------------------------------------------------------------");
		System.out.println(retBuilder.getDISHFmtData());
		System.out.println("------------------------------------------------------------------------------");
		System.out.println("DISH gen ended........");
	}

	public static void printETLR() throws BSPException {
		RETFileGenerationBL retFileBl = new RETFileGenerationBL();

		System.out.println("ETLR gen started......");
		for (int i = 0; i < 3; i++) {
			ETLRFileBuilder retBuilder = new ETLRFileBuilder("WW", new Date(), new Date(), 201L);
			ETLR tktt = retBuilder.getNewETLRInstance();

			X00_Ticket_Header_Record x00 = (X00_Ticket_Header_Record) tktt.createNewRecord(X00_Ticket_Header_Record.class);
			x00.setTicketNumber("98621122");
			x00.setRTATimeStamp(new Date());
			x00.setRecordType("23");

			X00_Mutation_Ticket_Header_Record x00MT = (X00_Mutation_Ticket_Header_Record) tktt
					.createNewRecord(X00_Mutation_Ticket_Header_Record.class);
			x00MT.setIsNewTicketIndication("Y");
			x00MT.setPurgeIndication("P");
			x00MT.setVoidIndication("Y");

			X00_25_Ticket_Header_Record x00_25 = (X00_25_Ticket_Header_Record) tktt
					.createNewRecord(X00_25_Ticket_Header_Record.class);
			x00_25.setPaxType("ADT");
			x00_25.setPaxFirstName("primal");
			x00_25.setPaxSurName("Suaris");

			X00_30_Ticket_Header_Record X00_30 = (X00_30_Ticket_Header_Record) tktt
					.createNewRecord(X00_30_Ticket_Header_Record.class);
			X00_30.setNUM_FLT_SEGS(4);
			X00_30.setDOCUMENT_TYPE("T");
			X00_30.setTKT_CREATE_DATE(new Date());
			X00_30.set1ST_TKT_NUMBER("1321242134");
			X00_30.setLAST_TKT_NUMBER("1321242136");
			X00_30.setNUM_OF_BOOKLETS(23);
			X00_30.setTOUR_CODE("FDSDF");
			X00_30.setTKTNG_MODE_INDIC("Y");
			X00_30.setINTL_SALES_INDIC("SITO");
			X00_30.setINTL_DOM_IND("D");
			X00_30.setSELF_SALE_INDIC("");
			X00_30.setNET_RPTG_INDIC("N");
			X00_30.setTKTING_DATE(new Date());
			X00_30.setORIG_AIRPORT("SHJ");
			X00_30.setDEST_AIRPORT("CMB");
			X00_30.setRES_SYSTEM("G9");
			X00_30.setSYMB_PNR_ADDR("1098765432");
			X00_30.setRES_SYSTM_OAL("");
			X00_30.setSYMB_PNR_ADDR_OAL("");
			X00_30.setINVOICE_NUM("");
			X00_30.setCLIENT_ACCT_CODE("");
			X00_30.setPRICING_INDIC("'2'");
			X00_30.setCURRENCY_CODE("AED");
			X00_30.setTOTAL_FARE("9276.09");
			X00_30.setBF_CURR_CD("AED");
			X00_30.setBF_FARE("9134.09");
			X00_30.setEF_CURR_CD("AED");
			X00_30.setEF_FARE("9134.09");
			X00_30.setBOOKING_IATA("");
			X00_30.setSYSTEM_PROVIDER("5235");
			X00_30.setDELIV_SYSTEM("");
			X00_30.setDELIV_CITY("SHJ");
			X00_30.setAGY_LOCALE("");
			X00_30.setRES_SYS_ID("");
			X00_30.setPSEUDO_CITY("");
			X00_30.setORIG_SYS("");
			X00_30.setAGENT_TYPE("T");
			X00_30.setTKTD_COUNTRY("SHJ");
			X00_30.setTKTD_AGENT("9834887");
			X00_30.setNET_CURR("AED");
			X00_30.setNET_AMNT("876378.00");
			X00_30.setBBR_CURREXCH_CODE("");
			X00_30.setTL3("");
			X00_30.setCURRENCY_EXCH_AMT("");
			X00_30.setCERTIFICATE_NUMBER("");
			X00_30.setENDORSE_RESTRICT("");

			X00_50_Ticket_Header_Record X00_50 = (X00_50_Ticket_Header_Record) tktt
					.createNewRecord(X00_50_Ticket_Header_Record.class);
			X00_50.setDOC_CURR_CODE("AED");
			X00_50.setDOC_AMT("86754623.07");

			X00_90_Ticket_Header_Record X00_90 = (X00_90_Ticket_Header_Record) tktt
					.createNewRecord(X00_90_Ticket_Header_Record.class);
			X00_90.setPLATING_CARRIER("G9");
			X00_90.setDATE_TIME(new Date());

			X34_Mutation_Fare_Record X34 = (X34_Mutation_Fare_Record) tktt.createNewRecord(X34_Mutation_Fare_Record.class);
			X34.setTICKET_NUMBER("98621122");
			X34.setRTA_TIMESTAMP(new Date());
			X34.setREC_TYPE("34");
			X34.setSEQENCE_INDIC("1231");
			X34.setFARE_LADDER("");

			X36_Ticket_Tax X36 = (X36_Ticket_Tax) tktt.createNewRecord(X36_Ticket_Tax.class);
			X36.setTICKET_NUMBER("98621122");
			X36.setRTA_TIMESTAMP(new Date());
			// X36.setREC_TYPE("36");
			// X36.setSEQUENCE_INDIC("0001");
			// X36.setTAX_CUR_CODE("AED");
			// X36.setTAX_ISO_COUNTRY_CODE("AE");
			// X36.setTAX_ISO_TAX_CODE("VAT");
			// X36.setTAX_AMOUNT("213.09");

			X40_Commission_Data X40 = (X40_Commission_Data) tktt.createNewRecord(X40_Commission_Data.class);
			X40.setTICKET_NUMBER("98621122");
			X40.setRTA_TIMESTAMP(new Date());
			X40.setREC_TYPE("36");
			X40.setSEQUENCE_INDIC("0001");
			X40.setCOMMISS_SEQ_NUM("0096");
			X40.setCOMMISS_RATE("23.2");
			X40.setCOMMISS_AMT("23245.34");
			X40.setCOMMISS_CURR_CODE("AED");
			X40.setCOMMISS_TAX_INDIC("");
			X40.setCOMMISS_TYPE("");

			X45_FOP X45 = (X45_FOP) tktt.createNewRecord(X45_FOP.class);
			X45.setTICKET_NUMBER("98621122");
			X45.setRTA_TIMESTAMP(new Date());
			X45.setREC_TYPE("36");
			X45.setSEQUENCE_INDIC("0001");
			X45.setFOP_TYPE("MS");
			X45.setFOP_AMT(new BigDecimal("2143.76"));
			X45.setCC_VENDOR("MAS");
			X45.setCC_NUM("5534999564137435");
			X45.setCC_EXP_DATE("2014-03");
			X45.setAUTH("234");
			X45.setAPPROVAL_SOURCE("MAS");
			X45.setAUTH_AMT("32423.45");
			X45.setADDR_VERIF_CODE("001");
			X45.set1ST_CC_NUM_EXT_PAY("");
			X45.setCC_CORP_CONTRACT("");

			X60_Coupon X60 = (X60_Coupon) tktt.createNewRecord(X60_Coupon.class);
			X60.setTICKET_NUMBER("98621122");
			X60.setRTA_TIMESTAMP(new Date());
			X60.setREC_TYPE("60");
			X60.setSEQUENCE_INDIC(new BigDecimal(1));
			X60.setTICKET_NUM(new BigDecimal(101));
			X60.setFLIGHT_INDICS(" ");
			X60.setSTAT_CHG_DATE(new Date());
			X60.setEDIFACT_COUPON_NUM(new BigDecimal(1));
			X60.setCOUPON_STATUS("A");
			X60.setEXCH_TKT_NUM("");
			X60.setCOUPON_VALUE(new BigDecimal(2134214.23));
			X60.setDEP_DATE(new Date());
			X60.setDEP_TIMEIn24hrs(new Date());
			X60.setSTOPOVER_CODE("");
			X60.setSEGMENT_STATUS("OK");
			X60.setSAC_CODE("");
			X60.setNVB_CENT("");
			X60.setNVB_DATE(new Date());
			X60.setNVA_CENT("");
			X60.setNVA_DATE(new Date());
			X60.setORIG_AIRPORT("SHJ");
			X60.setDEST_AIRPORT("CMB");
			X60.setMKTING_AIRLINE("30");
			X60.setCARRIER("G9");
			X60.setFLIGHT_NUM("MA370");
			X60.setCLASS_OF_SERVICE("E");
			X60.setFQTV_AIRLINE("");
			X60.setFQTV_NUMBER("");
			X60.setBBB_DATE(new SimpleDateFormat("yyyyMMdd").format(new Date()));
			X60.setFARE_BASIS_CODE("");
			X60.setVOL_INVOL_INDIC("");
			X60.setBAGGAGE_RESTRICT_CD("");
			X60.setBAGGAGE_UNIT("");

			// X60_Coupon X60_2 = (X60_Coupon) tktt.createNewRecord(X60_Coupon.class);
			// X60_2.setTICKET_NUMBER("98621122");
			// X60_2.setRTA_TIMESTAMP(new Date());
			// X60_2.setREC_TYPE("60");
			// X60_2.setSEQUENCE_INDIC("0001");
			// X60_2.setTICKET_NUM("0100");
			// X60_2.setFLIGHT_INDICS(" ");
			// X60_2.setSTAT_CHG_DATE(new Date());
			// X60_2.setEDIFACT_COUPON_NUM("0001");
			// X60_2.setCOUPON_STATUS("A");
			// X60_2.setEXCH_TKT_NUM("");
			// X60_2.setCOUPON_VALUE("2134214.23");
			// X60_2.setDEP_DATE(new Date());
			// X60_2.setDEP_TIMEIn24hrs(new Date());
			// X60_2.setSTOPOVER_CODE("");
			// X60_2.setSEGMENT_STATUS("OK");
			// X60_2.setSAC_CODE("");
			// X60_2.setNVB_CENT("");
			// X60_2.setNVB_DATE(new Date());
			// X60_2.setNVA_CENT("");
			// X60_2.setNVA_DATE(new Date());
			// X60_2.setORIG_AIRPORT("SHJ");
			// X60_2.setDEST_AIRPORT("CMB");
			// X60_2.setMKTING_AIRLINE("30");
			// X60_2.setCARRIER("G9");
			// X60_2.setFLIGHT_NUM("MA370");
			// X60_2.setCLASS_OF_SERVICE("E");
			// X60_2.setFQTV_AIRLINE("");
			// X60_2.setFQTV_NUMBER("");
			// X60_2.setBBB_DATE(new SimpleDateFormat("yyyyMMdd").format(new Date()));
			// X60_2.setFARE_BASIS_CODE("");
			// X60_2.setVOL_INVOL_INDIC("");
			// X60_2.setBAGGAGE_RESTRICT_CD("");
			// X60_2.setBAGGAGE_UNIT("");

			X70_Exchange_Records X70 = (X70_Exchange_Records) tktt.createNewRecord(X70_Exchange_Records.class);
			X70.setTICKET_NUMBER("98621122");
			X70.setRTA_TIMESTAMP(new Date());
			X70.setREC_TYPE("60");
			X70.setSEQUENCE_INDIC("0001");
			X70.setETK("");
			X70.setEXCH_TYPE("");
			X70.setORIG_FOP("");
			X70.setADDL_COLL_CURR("AED");
			X70.setADDL_COLL_AMT("234325.54");
			X70.setADDL_COLL_TOTCURR("AED");
			X70.setADDL_COLL_TOTAMT("32445.23");
			X70.setADMIN_FEE_AMT("");
			X70.setTTX("");
			X70.setEXCHANGE_INFO("");

			X74_Recent_Exchange_Record X74 = (X74_Recent_Exchange_Record) tktt.createNewRecord(X74_Recent_Exchange_Record.class);
			X74.setTICKET_NUMBER("98621122");
			X74.setRTA_TIMESTAMP(new Date());
			X74.setREC_TYPE("60");
			X74.setSEQUENCE_INDIC("0001");
			X74.setMOST_REC_EXCH_TKT("");

			X80_Alternative_Flight_History_Record X80 = (X80_Alternative_Flight_History_Record) tktt
					.createNewRecord(X80_Alternative_Flight_History_Record.class);
			X80.setTICKET_NUMBER("98621122");
			X80.setRTA_TIMESTAMP(new Date());
			X80.setREC_TYPE("60");
			X80.setSEQUENCE_INDIC("0001");
			X80.setATC("");
			X80.setCT2("");
			X80.setHCC("");
			X80.setHDT("");
			X80.setHDC("");
			X80.setFLT("");
			X80.setCLA("");
			X80.setAVI("");
			X80.setFBN("");

			X92_Coupon_Status_History_Record X92 = (X92_Coupon_Status_History_Record) tktt
					.createNewRecord(X92_Coupon_Status_History_Record.class);
			X92.setTICKET_NUMBER("98621122");
			X92.setRTA_TIMESTAMP(new Date());
			X92.setREC_TYPE("60");
			X92.setSEQUENCE_INDIC(new BigDecimal(1));
			X92.setCSH(BigDecimal.ONE);
			X92.setTPS("");
			X92.setDATE_TIME(new Date());
			X92.setTPC("");
			X92.setTHL("");
			X92.setTHA("");
			X92.setTAG("");
			X92.setTAN("");
			X92.setTPT("");
			X92.setSSQ("");
			X92.setTTH("");
			X92.setAP4("");
			X92.setOPN("");
			X92.setTCJ("");
			X92.setCNJ("");
			X92.setCHG_STATUS("");

			X94_Revalidation_Record X94 = (X94_Revalidation_Record) tktt.createNewRecord(X94_Revalidation_Record.class);
			X94.setTICKET_NUMBER("98621122");
			X94.setRTA_TIMESTAMP(new Date());
			X94.setREC_TYPE("94");
			X94.setSEQUENCE_INDIC("0001");
			X94.setRSN("");
			X94.setRSS("");
			X94.setORIG_MK_CRR_CD("");
			X94.setORIG_MK_DEP_TIME(new Date());
			X94.setORIG_MK_DEP_DATE(new Date());
			X94.setORIG_MK_FLT_NBR("");
			X94.setORIG_MK_COS("");
			X94.setNEW_MK_CRR_CD("");
			X94.setNEW_MK_DEP_TIME(new Date());
			X94.setNEW_MK_DEP_DATE(new Date());
			X94.setNEW_MK_FLT_NBR("");
			X94.setNEW_OP_COS("");
			X94.setORIG_OP_CRR_CD("");
			X94.setORIG_OP_FLT_NBR("");
			X94.setORIG_OP_COS("");
			X94.setORIG_OP_CTL_CRR("");
			X94.setNEW_OP_CRR_CD("");
			X94.setNEW_OP_FLT_NBR("");
			X94.setNEW_OP_COS("");
			X94.setNEW_OP_CTL_CRR("");

			XA0_Extend_Alternate_Coupon_Flight_History XA0 = (XA0_Extend_Alternate_Coupon_Flight_History) tktt
					.createNewRecord(XA0_Extend_Alternate_Coupon_Flight_History.class);
			XA0.setTICKET_NUMBER("98621122");
			XA0.setRTA_TIMESTAMP(new Date());
			XA0.setREC_TYPE("60");
			XA0.setSEQUENCE_INDIC("0001");
			XA0.setSEN("");
			XA0.setSECOND_SEQ_NBR("");
			XA0.setUNIT_OF_MEASURE("");
			XA0.setNBR_OF_BAGS("");
			XA0.setCHK_BAG_WEIGHT("");
			XA0.setUNCHK_BAG_WEIGHT("");
			XA0.setHEAD_OF_POOL("");
			XA0.setNBR_OF_BAG_TAGS("");
			XA0.setBAG_TAG_INFO("");
			XA0.setDEST("");
			XA0.setBAG_TAG_CRR("");
			XA0.setSTART_BAG_TAG_NBR("");
			XA0.setNBR_CONSEC_NBRS("");

			System.out.println("------------------------------------------------------------------------------");
			System.out.println(retBuilder.getDISHFmtData());
			System.out.println("------------------------------------------------------------------------------");
		}
		System.out.println("ETLR gen end......");
	}

	public static void printMandatoryElements(DISH_FILE_TYPE fileType) throws BSPException {
		RecordBuilder recordBuilder = RecordFactory.getInstance().getRecordBuilder(fileType);
		Collection<RecordMetaData> recordList = recordBuilder.getRecordMetaDataList();
		System.out.println(fileType + ":");
		Set<DISH_ELEMENT> dishElements = new HashSet<ElementFactory.DISH_ELEMENT>();
		for (RecordMetaData recordMetaData : recordList) {
			if (recordMetaData.getMin() > 0) {
				for (ElementMetaData elementData : recordMetaData.getElementBuilder().getMetaDataList()) {
					if (elementData.getStatus() == DE_STATUS.M) {
						dishElements.add(elementData.getElement());
						System.out.println(recordMetaData.getRecordType() + " " + elementData.getElement().getDescription());
					}
				}
			}
		}
		System.out.println("\n" + fileType + " Unique elements:");
		for (DISH_ELEMENT element : dishElements) {
			System.out.println(element.getDescription());
		}
	}
}
