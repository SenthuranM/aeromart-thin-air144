package com.isa.thinair.invoicing.core.persistence.jdbc;

/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airtravelagents.api.util.AirTravelAgentConstants;
import com.isa.thinair.airtravelagents.api.util.BLUtil;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.core.framework.PlatformBaseJdbcDAOSupport;
import com.isa.thinair.commons.core.util.XDBConnectionUtil;
import com.isa.thinair.invoicing.api.dto.GeneratedInvoiceDTO;
import com.isa.thinair.invoicing.api.dto.SearchInvoicesDTO;
import com.isa.thinair.invoicing.api.service.InvoicingModuleUtils;
import com.isa.thinair.invoicing.core.persistence.dao.TransferInvoiceJdbcDAO;

/**
 * @author Byorn
 * 
 */
public class TransferInvoiceJdbcDAOImpl extends PlatformBaseJdbcDAOSupport implements TransferInvoiceJdbcDAO {

	private Log log = LogFactory.getLog(getClass());

	// Query Ids

	private static final String GENERATED_INVOICES_WITHOUT_PAGING = "GENERATED_INVOICES_WITHOUT_PAGING";
	private static final String GENERATED_INVOICES_BY_TRANSACTION_WITHOUT_PAGING = "GENERATED_INVOICES_BY_TRANSACTION_WITHOUT_PAGING";
	private static final String AUTO_INVOICES = "AUTO_INVOICES";

	/**
	 * Method used to find the total sum of invoice amount in external system.
	 * This method is common for X and X3
	 * @param invoicesDTO
	 * @param accountingSystem
	 * @param system
	 * @throws CommonsDataAccessException
	 */
	public double getInvoiceTotalinExternalDB(
			SearchInvoicesDTO invoicesDTO,XDBConnectionUtil accountingSystem, String system) {
		double totAmount = -1;
		String selectStatment = "";
		Connection connection = accountingSystem.getExternalConnection();
		//select query is different for two external systems because of column name dissimilarity
		if(AirTravelAgentConstants.X_EXTERNAL_SYSTEM.equalsIgnoreCase(system)){
			selectStatment = "select sum(total_invoice_amount) as amt from INT_T_INVOICE where INVOICE_NUMBER like " + "'"
				+ invoicesDTO.getInvoiceNumberFormat() + "'";
		}else if(AirTravelAgentConstants.X3_EXTERNAL_SYSTEM.equalsIgnoreCase(system)){
			selectStatment = "select sum(invoice_amount) as amt from INT_T_INVOICE where INVOICE_NUMBER like " + "'"
					+ invoicesDTO.getInvoiceNumberFormat() + "'";
		}
		PreparedStatement selectStmt = null;
		// create the prepared statements
		try {
			selectStmt = connection.prepareStatement(selectStatment);
		} catch (SQLException e) {
			log.error("Error when getting prepared statment" + e);
			throw new CommonsDataAccessException(e, "airtravelagent.prepstatment.error", AirTravelAgentConstants.MODULE_NAME);
		}
		try {
			ResultSet res = selectStmt.executeQuery();
			while (res.next()) {
				totAmount = res.getDouble("amt");
			}
		} catch (Exception e) {
			throw new CommonsDataAccessException(e, "exdb.getconnection.retrieval.failed", AirTravelAgentConstants.MODULE_NAME);
		}
		return totAmount;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Collection<GeneratedInvoiceDTO> getInvoices(SearchInvoicesDTO invoicesDTO) {
		String agentsFilteringSql = "";
		String invoiceFilteringSql = null;
		String filterZeroInvoiceSql = "";
		Object params1[] = new Object[1];
		Object params2[] = new Object[2];
		String query = null;

		boolean isByTransaction = invoicesDTO.isByTrasaction();
		if (isByTransaction) {
			invoiceFilteringSql = BLUtil.getSQlForFilteringAgents(invoicesDTO.getInoviceNumbers(), "i.invoice_number");
			params1[0] = invoicesDTO.getEntity();
		} else {
			// construct the start date of the period
			Date periodStartDate = BLUtil.getFromDate(invoicesDTO);
			params2[0] = periodStartDate;
			params2[1] = invoicesDTO.getEntity();
			agentsFilteringSql = BLUtil.getSQlForFilteringAgents(invoicesDTO.getAgentCodes(), "i.agent_code");
		}
		DataSource ds = InvoicingModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		String sql1 = null;

		filterZeroInvoiceSql = BLUtil.getSqlForFilteringZeroInvoice(invoicesDTO.isFilterZeroInvoices(), "i.invoice_amount");

		if (isByTransaction) {
			query = getQuery(GENERATED_INVOICES_BY_TRANSACTION_WITHOUT_PAGING);
			sql1 = setStringToPlaceHolderIndex(query, 0, invoiceFilteringSql);
		} else {
			query = getQuery(GENERATED_INVOICES_WITHOUT_PAGING);
			sql1 = setStringToPlaceHolderIndex(query, 0, agentsFilteringSql);
		}

		final String sql = setStringToPlaceHolderIndex(sql1, 1, filterZeroInvoiceSql);

		Object col = null;
		try {
			if (isByTransaction) {
				col = templete.query(sql,params1, new ResultSetExtractor() {

					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Collection<GeneratedInvoiceDTO> colInvoiceDTOs = new ArrayList<GeneratedInvoiceDTO>();
						while (rs.next()) {

							GeneratedInvoiceDTO invoiceDTO = new GeneratedInvoiceDTO();
							invoiceDTO.setInvoiceNumber(rs.getString("invoice_number"));
							invoiceDTO.setInvoiceDate(new Date(rs.getTimestamp("invoice_date").getTime()));
							invoiceDTO.setAgentCode(rs.getString("agent_code"));
							invoiceDTO.setAgentName(rs.getString("agent_name"));
							invoiceDTO.setPrefReportFmt(rs.getString("pref_rpt_format"));
							invoiceDTO.setAccountCode(rs.getString("account_code"));
							invoiceDTO.setInvoiceAmount(rs.getBigDecimal("invoice_amount"));
							invoiceDTO.setTransferedStatus(rs.getString("transfer_status"));
							invoiceDTO.setBillingEmailAddress(rs.getString("billing_email"));
							invoiceDTO.setReportToGSA(rs.getString("report_to_gsa"));
							invoiceDTO.setInvoiceAmountLocal(rs.getBigDecimal("invoice_amount_local"));
							invoiceDTO.setCurrencyCode(rs.getString("currency_code"));
							invoiceDTO.setBillingPeriod(rs.getInt("billing_period"));
							invoiceDTO.setPnr(rs.getInt("pnr"));
							invoiceDTO.setEntityName(rs.getString("entity_name"));
							invoiceDTO.setEntityCode(rs.getString("entity_code"));
							colInvoiceDTOs.add(invoiceDTO);
						}
						return colInvoiceDTOs;
					}

				});
			} else {
				col = templete.query(sql, params2, new ResultSetExtractor() {

					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						Collection<GeneratedInvoiceDTO> colInvoiceDTOs = new ArrayList<GeneratedInvoiceDTO>();
						while (rs.next()) {

							GeneratedInvoiceDTO invoiceDTO = new GeneratedInvoiceDTO();
							invoiceDTO.setInvoiceNumber(rs.getString("invoice_number"));
							invoiceDTO.setInvoiceDate(rs.getDate("invoice_date"));
							invoiceDTO.setAgentCode(rs.getString("agent_code"));
							invoiceDTO.setAgentName(rs.getString("agent_name"));
							invoiceDTO.setPrefReportFmt(rs.getString("pref_rpt_format"));
							invoiceDTO.setAccountCode(rs.getString("account_code"));
							invoiceDTO.setInvoiceAmount(rs.getBigDecimal("invoice_amount"));
							invoiceDTO.setTransferedStatus(rs.getString("transfer_status"));
							invoiceDTO.setBillingEmailAddress(rs.getString("billing_email"));
							invoiceDTO.setReportToGSA(rs.getString("report_to_gsa"));
							invoiceDTO.setInvoiceAmountLocal(rs.getBigDecimal("invoice_amount_local"));
							invoiceDTO.setCurrencyCode(rs.getString("currency_code"));
							invoiceDTO.setBillingPeriod(rs.getInt("billing_period"));
							invoiceDTO.setEntityName(rs.getString("entity_name"));
							invoiceDTO.setEntityCode(rs.getString("entity_code"));
							colInvoiceDTOs.add(invoiceDTO);
						}
						return colInvoiceDTOs;
					}

				});
			}
		} catch (DataAccessException e) {
			log.error(e);
			throw new CommonsDataAccessException(e, "airtravelagent.jdbc.error", AirTravelAgentConstants.MODULE_NAME);
		}
		return (Collection<GeneratedInvoiceDTO>) col;
	}

	/**
	 * 
	 * @param likeInvoiceNumber
	 *            [yymmpp]
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Object[] getAutoInvoicesForPeriod(Date invoiceDate) {

		DataSource ds = InvoicingModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");

		String invoicedate = sdf.format(invoiceDate);

		String sql = getQuery(AUTO_INVOICES);

		String sqlQuery = setStringToPlaceHolderIndex(sql, 0, invoicedate);

		Object obj[] = null;
		try {
			obj = (Object[]) templete.query(sqlQuery, new ResultSetExtractor() {

				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					SearchInvoicesDTO sid = new SearchInvoicesDTO();
					sid.setYearTo(0);
					Collection<GeneratedInvoiceDTO> colInvoiceDTOs = new ArrayList<GeneratedInvoiceDTO>();
					Collection<String> invoiceNumbers = new ArrayList<String>();
					while (rs.next()) {

						GeneratedInvoiceDTO invoiceDTO = new GeneratedInvoiceDTO();
						invoiceDTO.setInvoiceNumber(rs.getString("invoice_number"));
						invoiceDTO.setInvoiceDate(rs.getDate("invoice_date"));
						invoiceDTO.setAgentCode(rs.getString("agent_code"));
						invoiceDTO.setAgentName(rs.getString("agent_name"));
						invoiceDTO.setPrefReportFmt(rs.getString("pref_rpt_format"));
						invoiceDTO.setAccountCode(rs.getString("account_code"));
						invoiceDTO.setInvoiceAmount(rs.getBigDecimal("invoice_amount"));
						invoiceDTO.setTransferedStatus(rs.getString("transfer_status"));
						invoiceDTO.setBillingEmailAddress(rs.getString("billing_email"));
						invoiceDTO.setReportToGSA(rs.getString("report_to_gsa"));
						invoiceDTO.setInvoiceAmountLocal(rs.getBigDecimal("invoice_amount_local"));
						invoiceDTO.setCurrencyCode(rs.getString("currency_code"));
						invoiceDTO.setEntityName(rs.getString("entity_name"));
						invoiceDTO.setEntityCode(rs.getString("entity_code"));
						invoiceDTO.setSearchInvoicesDTO(sid);
						colInvoiceDTOs.add(invoiceDTO);
						invoiceNumbers.add(invoiceDTO.getInvoiceNumber());
					}
					return new Object[] { invoiceNumbers, colInvoiceDTOs };
				}

			});
		} catch (DataAccessException e) {
			log.error(e);
			throw new CommonsDataAccessException(e, "airtravelagent.jdbc.error", AirTravelAgentConstants.MODULE_NAME);
		}
		return obj;
	}
}
