/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.invoicing.core.persistence.dao;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.sql.RowSet;

import com.isa.thinair.invoicing.api.dto.GeneratedInvoiceDTO;
import com.isa.thinair.invoicing.api.dto.InvoiceDTO;
import com.isa.thinair.invoicing.api.dto.InvoiceSummaryDTO;
import com.isa.thinair.invoicing.api.model.Invoice;
import com.isa.thinair.invoicing.api.model.InvoiceSettlement;

/**
 * @author Chamindap
 * 
 */
public interface InvoiceDAO {

	/**
	 * Save the invoice.
	 * 
	 * @param invoice
	 *            the invoice.
	 */
	public void saveInvoice(Invoice invoice);

	/**
	 * Save the invoices
	 * 
	 * @param invoices
	 * @return witll return a HashMap of AgentCodes with there invoiceNumber
	 */
	public void saveInvoices(Collection invoices);

	/**
	 * Remove the invoice.
	 * 
	 * @param invoiceNo
	 *            the invoice No.
	 */
	public void removeInvoice(String invoiceNo);

	/**
	 * Gets the invoice object for given invoice number.
	 * 
	 * @param invoiceNumber
	 *            the invoice number.
	 * @return the Invoice object.
	 */
	public Invoice getInvoice(String invoiceNumber);

	/**
	 * Gets all unsettled invoices for an agent.
	 * 
	 * @param agentCode
	 *            the agent code.
	 * @return a collection of unsettled invoices.
	 */
	public Collection getUnsettledInvoicesForAgent(String agentCode);

	/**
	 * Adds InvoiceSettlement.
	 * 
	 * @param settlement
	 *            InvoiceSettlement.
	 */
	public void addInvoiceSettlement(InvoiceSettlement settlement);

	/**
	 * Update the invoice number
	 * 
	 * @param invoiceNumber
	 *            the invoiceNumber
	 * @param amount
	 *            the amount.
	 */
	public void updateInvoiceSettlement(String invoiceNumber, BigDecimal amount);

	/**
	 * Gets invoice for agent.
	 * 
	 * @param critiria
	 *            the search critiria.
	 * @return search invoiceSummaryDTO.
	 */
	public InvoiceSummaryDTO getInvoiceForAgent(String invoiceNo, String pnr);

	/**
	 * Gets invoice for agent.
	 * 
	 * @param critiria
	 *            the search critiria.
	 * @return search invoiceSummaryDTO.
	 */
	public InvoiceSummaryDTO getInvoiceForAgent(String agentGSACode, String year, String month, int billingPeriod,String entity);

	/**
	 * Gets invoices for agent.
	 * 
	 * @param critiria
	 *            the search critiria.
	 * @return GeneratedInvoiceDTO.
	 */
	public Collection<GeneratedInvoiceDTO> getGenaratedInvoicesForAgent(String agentGSACode, String dateFrom, String dateTo);

	public void insertInvoicesToInterfaceTable(Collection invoiceCollection);

	public void insertInvoicesToInterfaceTable(Collection invoiceCollection, boolean payCurr);

	public void updateInvoiceStatus(Date from, Collection agentCodes, String transferStatus);

	/**
	 * Returns a collection of Invoice Objects for agent codes. <T_INVOICE TABLE>
	 * 
	 * @param fromDate
	 * @param agentCodes
	 * @return
	 */
	public Collection getInvoicesForAgents(Date fromDate, Collection agentCodes);

	public Invoice getReservationInvoicePerAgent(String agentGSACode, String year, String month, int billingPeriod);

	public Invoice getReservationInvoicePerAgent(String agentGSACode, String pnr);

	public List<InvoiceDTO> getExternalInvoicePerAgent(String agentGSACode, String year, String month, int billingPeriod,
			String entity);

	public List<InvoiceDTO> getExternalInvoicePerAgent(String agentGSACode, String pnr);

	public RowSet getInvoiceExportData(Collection<String> invoiceIDList);
}
