/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.invoicing.core.persistence.hibernate;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.isa.thinair.airtravelagents.api.util.AirTravelAgentConstants;
import com.isa.thinair.airtravelagents.api.util.BLUtil;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.invoicing.api.dto.InvoiceDTO;
import com.isa.thinair.invoicing.api.service.InvoicingModuleUtils;
import com.isa.thinair.invoicing.api.utils.InvoicingConstants;
import com.isa.thinair.invoicing.core.persistence.dao.TransferInvoiceDAO;

/**
 * @author Chamindap
 * @isa.module.dao-impl dao-name="transferinvoiceDAO"
 */
public class TransferInvoiceDAOImpl extends HibernateDaoSupport implements TransferInvoiceDAO {

	private Log log = LogFactory.getLog(this.getClass());

	/**
	 * InvoiceDAO: updateInvoiceStatus(Date from, Collection agentCodes, boolean transferStatus)
	 * 
	 * @param from
	 * @param agentCodes
	 * @param transferStatus
	 */
	public void updateInvoiceStatus(Date from, Collection agentCodes, String transferStatus) {

		DataSource ds = InvoicingModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		String sql = "update T_INVOICE set TRANSFER_STATUS=? where trunc(INVIOCE_PERIOD_FROM)=? "
				+ BLUtil.getSQlForFilteringAgents(agentCodes, "AGENT_CODE");

		Object params[] = { transferStatus, from };
		templete.update(sql, params);

	}

	public void updateEmailsStatus(String status, Collection invoiceNumbers) {

		try {

			DataSource ds = InvoicingModuleUtils.getDatasource();
			JdbcTemplate templete = new JdbcTemplate(ds);
			Object params[] = { status };

			String invoiceNumberswithINClause = Util.getReplaceStringForIN("INVOICE_NUMBER", invoiceNumbers);

			String sqlUpdate = "UPDATE T_INVOICE SET EMAIL_STATUS =?  WHERE " + invoiceNumberswithINClause;

			if (status.equals(AirTravelAgentConstants.EmailStatus.SENT)) {

				sqlUpdate = "UPDATE T_INVOICE SET EMAIL_STATUS =? , EMAILS_SENT = EMAILS_SENT + 1  WHERE "
						+ invoiceNumberswithINClause;
			}
			templete.update(sqlUpdate, params);

		} catch (Exception e) {
			throw new CommonsDataAccessException(e, null);
		}

	}

	public void updateInvoiceTransferStatus(String status, Collection invoiceNumbers) {

		/*
		 * String inStr = PlatformUtiltiies .constructINStringForCharactors(invoiceNumbers);
		 * 
		 * Collection invoics = (Collection) find( "from Invoice invoice where invoice.invoiceNumber in (" + inStr +
		 * ")"); Iterator iterator = invoics.iterator(); while (iterator.hasNext()) { Invoice invoice = (Invoice)
		 * iterator.next(); invoice.setTransferStatus(status);
		 * 
		 * } // invoice.setMailStatus(status); saveOrUpdateAll(invoics);
		 */

		// removing calling and setting method and doing a batch update

		try {

			String hqlUpdate = "update Invoice set transferStatus =:transferStatus " + "where "
					+ "invoiceNumber in (:invoiceNumbers)";

			getSession().createQuery(hqlUpdate).setParameter("transferStatus", status)
					.setParameterList("invoiceNumbers", invoiceNumbers).executeUpdate();

		} catch (Exception e) {
			throw new CommonsDataAccessException(e, null);
		}
	}

	/**
	 * Implmentation for getInvoicesForAgents(Date fromDate, Collection agentCodes) in InvoiceDAO Returns Collection of
	 * InvoiceDTO Objects for a give fromDate and agentCode FYI : Invoice will have <from date> AND <to date> i.e 1 - 15
	 * or 16 - 31
	 */
	@SuppressWarnings("rawtypes")
	public Collection getInvoicesForAgents(Date fromDate, Collection agentCodes, String system) {
		/*
		 * if (fromDate == null) { throw new CommonsDataAccessException("airtravelagent.param.nullempty",
		 * InvoicingConstants.MODULE_NAME); }
		 */

		Object params[] = { fromDate };
		DataSource ds = InvoicingModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		String queryString = " select  i.invoice_number, i.invoice_date, i.agent_code, a.account_code, "
				+ " i.invioce_period_from, i.invoice_period_to, " + " i.invoice_amount,i.invoice_amount_local, "
				+ " a.currency_code from t_invoice i, t_agent a where i.agent_code = a.agent_code "
				+ " and i.invioce_period_from= ? " + BLUtil.getSQlForFilteringAgents(agentCodes, "i.agent_code");
		@SuppressWarnings("unchecked")
		Object col = templete.query(queryString, params, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<InvoiceDTO> colInvoiceDTOs = new ArrayList<InvoiceDTO>();
				while (rs.next()) {

					InvoiceDTO invoiceDTO = new InvoiceDTO();
					invoiceDTO.setInvoiceNumber(rs.getString("invoice_number"));
					invoiceDTO.setInvoiceDate(rs.getDate("invoice_date"));
					invoiceDTO.setAgentCode(rs.getString("agent_code"));
					invoiceDTO.setAccountCode(rs.getString("account_code"));
					invoiceDTO.setInviocePeriodFrom(rs.getDate("invioce_period_from"));
					invoiceDTO.setInvoicePeriodTo(rs.getDate("invoice_period_to"));
					invoiceDTO.setInvoiceAmount(rs.getBigDecimal("invoice_amount"));
					invoiceDTO.setInvoiceAmountLocal(rs.getBigDecimal("invoice_amount_local") == null ? new BigDecimal(0) : rs
							.getBigDecimal("invoice_amount_local"));
					invoiceDTO.setCurrencyCode(StringUtil.getNotNullString(rs.getString("currency_code")));
					invoiceDTO.setAccountingSystemName(system);
					colInvoiceDTOs.add(invoiceDTO);
				}
				return colInvoiceDTOs;
			}

		});

		return (Collection) col;
	}
}
