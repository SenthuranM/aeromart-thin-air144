/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.invoicing.core.persistence.hibernate;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import javax.sql.DataSource;
import javax.sql.RowSet;

import oracle.jdbc.rowset.OracleCachedRowSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airtravelagents.api.util.BLUtil;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.core.framework.PlatformHibernateDaoSupport;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.commons.core.util.XDBConnectionUtil;
import com.isa.thinair.invoicing.api.dto.GeneratedInvoiceDTO;
import com.isa.thinair.invoicing.api.dto.InvoiceDTO;
import com.isa.thinair.invoicing.api.dto.InvoiceSummaryDTO;
import com.isa.thinair.invoicing.api.model.Invoice;
import com.isa.thinair.invoicing.api.model.InvoiceSettlement;
import com.isa.thinair.invoicing.api.service.InvoicingModuleUtils;
import com.isa.thinair.invoicing.api.utils.InvoicingConstants;
import com.isa.thinair.invoicing.core.persistence.dao.InvoiceDAO;

/**
 * @author Chamindap
 * @isa.module.dao-impl dao-name="invoiceDAO"
 * 
 */
public class InvoiceDAOImpl extends PlatformHibernateDaoSupport implements InvoiceDAO {

	private Log log = LogFactory.getLog(this.getClass());
	private static final String INVOICE_STATUS_SETTLED = "SETTLED";

	private static final String INVOICE_STATUS_UNSETTLED = "UNSETTLED";

	/**
	 * Implementation of InvoiceDAO:insertInvoicesToInterfaceTable(Collection invoiceCollection) Accepts InvoiceDTO
	 * collection and inserts to SQL SERVER Interface table : I_T_INVOICE
	 */
	public void insertInvoicesToInterfaceTable(Collection invoiceCollection) {

		if (invoiceCollection == null || invoiceCollection.size() == 0) {
			throw new CommonsDataAccessException("airtravelagent.param.nullempty", InvoicingConstants.MODULE_NAME);
		}

		Connection connection = InvoicingModuleUtils.getExternalDBConnectionUtil().getExternalConnection();

		Iterator it = invoiceCollection.iterator();

		// common sql queries

		String insertStatment = "insert into  INT_T_INVOICE  values (?,?,?,?,?,?,?,?)";
		String selectStatment = "select * from INT_T_INVOICE where INVOICE_NUMBER=?";
		String deleteStatement = "delete from  INT_T_INVOICE where INVOICE_NUMBER=?";

		PreparedStatement insertStmt = null;
		PreparedStatement selectStmt = null;
		PreparedStatement deleteStmt = null;

		// create the prepared statements
		try {
			insertStmt = connection.prepareStatement(insertStatment);

			selectStmt = connection.prepareStatement(selectStatment);

			deleteStmt = connection.prepareStatement(deleteStatement);

			connection.setAutoCommit(false);
		} catch (SQLException e) {
			log.error("Error when getting prepared statment" + e);
			throw new CommonsDataAccessException(e, "airtravelagent.prepstatment.error", InvoicingConstants.MODULE_NAME);

		}

		try {

			// Iterate through the Invoice List
			while (it.hasNext()) {

				InvoiceDTO invoice = (InvoiceDTO) it.next();

				Date id = invoice.getInvoiceDate();
				java.sql.Date invoiceDate = new java.sql.Date(id.getTime());

				// Delete If already In Interface Table

				selectStmt.setString(1, String.valueOf(invoice.getInvoiceNumber()));

				ResultSet res = selectStmt.executeQuery();

				int count = 0;
				while (res.next()) {
					count++;
					deleteStmt.setString(1, res.getString("INVOICE_NUMBER"));

					deleteStmt.executeUpdate();
				}
				// end of select and delete

				// set params to insert statment
				insertStmt.setString(1, invoice.getInvoiceNumber());
				insertStmt.setDate(2, invoiceDate);
				insertStmt.setDate(3, new java.sql.Date(invoice.getInviocePeriodFrom().getTime()));
				insertStmt.setDate(4, new java.sql.Date(invoice.getInvoicePeriodTo().getTime()));
				insertStmt.setString(5, invoice.getAgentCode());
				insertStmt.setString(6, invoice.getAccountCode());
				insertStmt.setDouble(7, invoice.getInvoiceAmount().doubleValue());
				insertStmt.setInt(8, 0);

				insertStmt.executeUpdate();

			}
		} catch (Exception e) {
			log.error("failed whent transfering", e);
			XDBConnectionUtil.rollback(connection);
			throw new CommonsDataAccessException(e, "airtravelagent.invoicetransfer.error", InvoicingConstants.MODULE_NAME);
		}
		XDBConnectionUtil.commit(connection);

	}

	public void insertInvoicesToInterfaceTable(Collection invoiceCollection, boolean payCurr) {
		// TODO -change according to need of the external tables
		if (invoiceCollection == null || invoiceCollection.size() == 0) {
			throw new CommonsDataAccessException("airtravelagent.param.nullempty", InvoicingConstants.MODULE_NAME);
		}

		Connection connection = InvoicingModuleUtils.getExternalDBConnectionUtil().getExternalConnection();

		Iterator it = invoiceCollection.iterator();

		// common sql queries

		String insertStatment = "insert into  INT_T_INVOICE  values (?,?,?,?,?,?,?,?)";
		String selectStatment = "select * from INT_T_INVOICE where INVOICE_NUMBER=?";
		String deleteStatement = "delete from  INT_T_INVOICE where INVOICE_NUMBER=?";

		PreparedStatement insertStmt = null;
		PreparedStatement selectStmt = null;
		PreparedStatement deleteStmt = null;

		// create the prepared statements
		try {
			insertStmt = connection.prepareStatement(insertStatment);

			selectStmt = connection.prepareStatement(selectStatment);

			deleteStmt = connection.prepareStatement(deleteStatement);

			connection.setAutoCommit(false);
		} catch (SQLException e) {
			log.error("Error when getting prepared statment" + e);
			throw new CommonsDataAccessException(e, "airtravelagent.prepstatment.error", InvoicingConstants.MODULE_NAME);

		}

		try {

			// Iterate through the Invoice List
			while (it.hasNext()) {

				InvoiceDTO invoice = (InvoiceDTO) it.next();

				Date id = invoice.getInvoiceDate();
				java.sql.Date invoiceDate = new java.sql.Date(id.getTime());

				// Delete If already In Interface Table

				selectStmt.setString(1, String.valueOf(invoice.getInvoiceNumber()));

				ResultSet res = selectStmt.executeQuery();

				int count = 0;
				while (res.next()) {
					count++;
					deleteStmt.setString(1, res.getString("INVOICE_NUMBER"));

					deleteStmt.executeUpdate();
				}
				// end of select and delete

				// set params to insert statment
				insertStmt.setString(1, invoice.getInvoiceNumber());
				insertStmt.setDate(2, invoiceDate);
				insertStmt.setDate(3, new java.sql.Date(invoice.getInviocePeriodFrom().getTime()));
				insertStmt.setDate(4, new java.sql.Date(invoice.getInvoicePeriodTo().getTime()));
				insertStmt.setString(5, invoice.getAgentCode());
				insertStmt.setString(6, invoice.getAccountCode());
				insertStmt.setDouble(7, invoice.getInvoiceAmount().doubleValue());
				insertStmt.setInt(8, 0);

				insertStmt.executeUpdate();

			}
		} catch (Exception e) {
			log.error("failed whent transfering", e);
			XDBConnectionUtil.rollback(connection);
			throw new CommonsDataAccessException(e, "airtravelagent.invoicetransfer.error", InvoicingConstants.MODULE_NAME);
		}
		XDBConnectionUtil.commit(connection);

	}

	/**
	 * Implementation for getInvoicesForAgents(Date fromDate, Collection agentCodes) in InvoiceDAO Returns Collection of
	 * InvoiceDTO Objects for a give fromDate and agentCode FYI : Invoice will have <from date> AND <to date> i.e 1 - 15
	 * or 16 - 31
	 */
	public Collection getInvoicesForAgents(Date fromDate, Collection agentCodes) {
		if (fromDate == null) {
			throw new CommonsDataAccessException("airtravelagent.param.nullempty", InvoicingConstants.MODULE_NAME);
		}

		Object params[] = { fromDate };
		DataSource ds = InvoicingModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		String queryString = " select  i.invoice_number, i.invoice_date, i.agent_code, a.account_code, "
				+ " i.invioce_period_from, i.invoice_period_to, " + " i.invoice_amount "
				+ " from t_invoice i, t_agent a where i.agent_code = a.agent_code " + " and i.invioce_period_from= ? "
				+ BLUtil.getSQlForFilteringAgents(agentCodes, "i.agent_code");

		Object col = templete.query(queryString, params, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<InvoiceDTO> colInvoiceDTOs = new ArrayList<InvoiceDTO>();
				while (rs.next()) {

					InvoiceDTO invoiceDTO = new InvoiceDTO();
					invoiceDTO.setInvoiceNumber(rs.getString("invoice_number"));
					invoiceDTO.setInvoiceDate(rs.getDate("invoice_date"));
					invoiceDTO.setAgentCode(rs.getString("agent_code"));
					invoiceDTO.setAccountCode(rs.getString("account_code"));
					invoiceDTO.setInviocePeriodFrom(rs.getDate("invioce_period_from"));
					invoiceDTO.setInvoicePeriodTo(rs.getDate("invoice_period_to"));
					invoiceDTO.setInvoiceAmount(rs.getBigDecimal("invoice_amount"));
					colInvoiceDTOs.add(invoiceDTO);
				}
				return colInvoiceDTOs;
			}

		});

		return (Collection) col;

	}

	/**
	 * The constructor.
	 */
	public InvoiceDAOImpl() {
		super();
	}

	/**
	 * Gets all unsettled invoices for an agent.
	 * 
	 * @param agentCode
	 *            the agent code.
	 * @return a collection of unsettled invoices.
	 * @see com.isa.thinair.airtravelagents.core.persistence.dao
	 *      .TravelAgentFinanceDAO#getUnsettledInvoicesForAgent(String)
	 */
	public Collection getUnsettledInvoicesForAgent(String agentCode) {
		Collection unsettledInvoices = null;

		unsettledInvoices = find(
				"select invoice from " + "Invoice as invoice where invoice.status = 'UNSETTLED' "
						+ "and invoice.agentCode=? order by invoice.invoiceDate desc", agentCode, Invoice.class);
		
		// Due to Xdoclet doesn't support many to one lazy loading
		Iterator iter = unsettledInvoices.iterator();
		while (iter.hasNext()) {
			 ((Invoice) iter.next()).getEntity().getEntityName();
		}

		return unsettledInvoices;
	}

	/**
	 * Save the invoice.
	 * 
	 * @param invoice
	 *            the invoice.
	 * @see com.isa.thinair.invoicing.core.persistance.dao.InvoiceDAO#saveInvoice(Invoice)
	 */
	public void saveInvoice(Invoice invoice) {
		String invoiceNumber = null;

		invoiceNumber = getInvoiceNo(invoice.getInvoicePeriodTo(), invoice.getBillingPeriod());
		invoice.setInvoiceNumber(invoiceNumber);

		try {
			hibernateSave(invoice);
		} catch (Exception e) {
		}
	}

	/**
	 * Save the invoices.
	 * 
	 * @param invoice
	 *            the invoice.
	 * @see com.isa.thinair.invoicing.core.persistance.dao.InvoiceDAO#saveInvoice(Invoice)
	 */
	public void saveInvoices(Collection invoices) {
		hibernateSaveOrUpdateAll(invoices);
	}

	/**
	 * Remove the invoice.
	 * 
	 * @param invoiceNo
	 *            the invoice No.
	 * @see com.isa.thinair.invoicing.core.persistance.dao.InvoiceDAO #removeInvoice(java.lang.String)
	 */
	public void removeInvoice(String invoiceNo) {
		Object agent = load(Invoice.class, invoiceNo);
		delete(agent);
	}

	/**
	 * Gets the invoice object for given invoice number.
	 * 
	 * @param invoiceNumber
	 *            the invoice number.
	 * @return the Invoice object.
	 * @see com.isa.thinair.invoicing.core.persistance.dao.InvoiceDAO #getInvoice(java.lang.String)
	 */
	public Invoice getInvoice(String invoiceNumber) {
		Invoice invoice = null;
		List invoices = null;

		invoices = find("select invoice from Invoice " + "as invoice where invoice.invoiceNumber=?",
				invoiceNumber, Invoice.class);

		if (invoices.size() != 0) {
			invoice = (Invoice) invoices.get(0);
		}
		return invoice;
	}

	/**
	 * Adds InvoiceSettlement.
	 * 
	 * @param settlement
	 *            InvoiceSettlement.
	 * @see com.isa.thinair.invoicing.core.persistance.dao.InvoiceDAO
	 *      #addInvoiceSettlement(com.isa.thinair.invoicing.api.model.InvoiceSettlement)
	 */
	public void addInvoiceSettlement(InvoiceSettlement settlement) {
		// settlement.setEntryType(InvoiceSettlement.ENTRY_TYPE_CR); // removed
		// by kasun, because it is handled at BL
		hibernateSave(settlement);
	}

	public Collection getNominalcodeForInvoiceAgent() {

		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);
		// String queryString ="select AGENT_CODE from T_AGENT";

		final Collection<String> nominalCodes = new ArrayList<String>();
		String desc[] = { "ACC_SALE", "ACC_CREDIT", "ACC_DEBIT", "ACC_REFUND", "EXT_ACC_SALE", "EXT_ACC_REFUND" };
		for (int j = 0; j < desc.length; j++) {

			String queryString = "select NOMINAL_CODE  from t_agent_transaction_nc tnc where tnc.DESCRIPTION=? ";
			Object params[] = { desc[j] };

			templete.query(queryString, params, new ResultSetExtractor() {

				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

					while (rs.next()) {
						String agentCode = rs.getString("NOMINAL_CODE");
						nominalCodes.add(agentCode);

					}

					return null;
				}
			});
		}
		return nominalCodes;

	}

	public String getAccountCode(String agentCode) {

		DataSource ds = InvoicingModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		Object params[] = { agentCode };
		String queryString = "select ACCOUNT_CODE from T_AGENT where AGENT_CODE=? ";
		final Collection<String> finalList = new ArrayList<String>();
		templete.query(queryString, params, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				while (rs.next()) {

					String acode = rs.getString("ACCOUNT_CODE");
					if (acode == null | acode == "") {
						acode = "-1";// todo
					}

					finalList.add(acode);
				}
				return null;
			}

		});

		return (String) (((ArrayList) finalList).get(0));

	}

	/**
	 * Adds InvoiceSettlement.
	 * 
	 * @param invoiceNumber
	 *            the invoice number.
	 * @param invoiceAmount
	 *            the invoice amount.
	 * @see com.isa.thinair.invoicing.core.persistance.dao.InvoiceDAO #addInvoiceSettlement(java.lang.String,
	 *      java.lang.Float)
	 */
	public void addInvoiceSettlement(String invoiceNumber, BigDecimal reverseAmount, String reason) {
		InvoiceSettlement settlement = null;

		settlement = new InvoiceSettlement();

		settlement.setAmountPaid(reverseAmount);
		settlement.setCpCode(null);
		settlement.setEntryType(InvoiceSettlement.ENTRY_TYPE_CR);
		settlement.setInvoiceNo(invoiceNumber);
		settlement.setSettlementDate(new GregorianCalendar().getTime());
		settlement.setReason(reason);

		hibernateSave(settlement);
	}

	/**
	 * Update the invoice number
	 * 
	 * @param invoiceNumber
	 *            the invoiceNumber
	 * @param amount
	 *            the amount.
	 * @see com.isa.thinair.invoicing.core.persistance.dao.InvoiceDAO #updateInvoiceSettlement(java.lang.String,
	 *      BigDecimal)
	 */
	public void updateInvoiceSettlement(String invoiceNumber, BigDecimal amount) {
		Invoice invoice = null;
		BigDecimal settledAmount;
		BigDecimal invoiceAmount;
		long settledAmountLong;
		long invoiceAmountLong;
		String status = null;

		invoice = getInvoice(invoiceNumber);

		if (invoice != null) {
			status = invoice.getStatus();
			settledAmount = invoice.getSettledAmount().abs();
			invoiceAmount = invoice.getInvoiceAmount().abs();
			settledAmount = AccelAeroCalculator.add(settledAmount, amount.abs());
			settledAmountLong = (int) settledAmount.intValue();
			invoiceAmountLong = (int) invoiceAmount.intValue();

			if ((settledAmount.compareTo(invoiceAmount) == 0) || (settledAmountLong == invoiceAmountLong)) {
				status = INVOICE_STATUS_SETTLED;// "SETTLED";

			} else if (settledAmount.compareTo(invoiceAmount) == -1) {
				status = INVOICE_STATUS_UNSETTLED;// "UNSETTLED";
			}
			// to handle negative invoice // by kasun
			if (invoice.getInvoiceAmount().compareTo(BigDecimal.ZERO) == -1) {
				invoice.setSettledAmount(settledAmount.negate());
			} else {
				invoice.setSettledAmount(settledAmount);
			}
			invoice.setStatus(status);
			hibernateSaveOrUpdate(invoice);
		} else {
			// TD for messages exceptions
		}
	}

	/**
	 * Gets invoice for agent.
	 * 
	 * @param critiria
	 *            the search critiria.
	 * @return search invoice.
	 * @see com.isa.thinair.invoicing.core.persistance.dao.InvoiceDAO #getInvoiceForAgent(java.util.List)
	 */
	public InvoiceSummaryDTO getInvoiceForAgent(String agentGSACode, String year, String month, int billingPeriod,String entity) {
		InvoiceSummaryDTO invoiceSummaryDTO = new InvoiceSummaryDTO();
		List list = null;
		Date firstday = null;
		Date lastday = null;

		GregorianCalendar gc = new GregorianCalendar();

		gc.set(Calendar.YEAR, Integer.parseInt(year));
		gc.set(Calendar.MONTH, (Integer.parseInt(month) - 1));
		gc.set(Calendar.DATE, 1);
		if (AppSysParamsUtil.isWeeklyInvoiceEnabled()) {
			firstday = BLUtil.getInvoceDate(Integer.parseInt(year), (Integer.parseInt(month)), billingPeriod);
			Calendar last = Calendar.getInstance();
			last.setTime(firstday);
			last.add(Calendar.DATE, 6);
			last.add(Calendar.HOUR, 23);
			last.add(Calendar.MINUTE, 59);
			last.add(Calendar.SECOND, 59);
			lastday = last.getTime();
		} else {
			if (billingPeriod == 1) {
				firstday = gc.getTime();
				gc.set(Calendar.DATE, 15);
				lastday = gc.getTime();

			} else {
				gc.set(Calendar.DATE, 16);
				firstday = gc.getTime();
				gc.set(Calendar.DATE, gc.getActualMaximum(Calendar.DAY_OF_MONTH));
				lastday = gc.getTime();
			}
		}

		Object obj[] = { CalendarUtil.getStartTimeOfDate(firstday), CalendarUtil.getEndTimeOfDate(lastday), agentGSACode,entity};
		list = find(
				"select invoice from Invoice as invoice where invoice.billingPeriod=" + billingPeriod
						+ " and invoice.inviocePeriodFrom >= ? and invoice.invoicePeriodTo <= ? " + " and invoice.agentCode= ? "
								+ " and invoice.entityCode= ?",
				obj, Invoice.class);

		if (list.size() != 0) {
			Invoice inv = (Invoice) list.get(0);
			invoiceSummaryDTO.setInvoice(inv);
		}
		return invoiceSummaryDTO;
	}

	/**
	 * Gets invoice for agent.
	 * 
	 * @param critiria
	 *            the search critiria.
	 * @return search invoice.
	 * @see com.isa.thinair.invoicing.core.persistance.dao.InvoiceDAO #getInvoiceForAgent(java.util.List)
	 */
	public InvoiceSummaryDTO getInvoiceForAgent(String invoiceNo, String pnr) {
		InvoiceSummaryDTO invoiceSummaryDTO = new InvoiceSummaryDTO();
		List list = null;

		list = find(
				"select invoice from Invoice as invoice where invoice.invoiceNumber='" + invoiceNo + "' and "
						+ " invoice.pnr = '" + pnr + "'", Invoice.class);

		if (list.size() != 0) {
			Invoice inv = (Invoice) list.get(0);
			invoiceSummaryDTO.setInvoice(inv);
		}
		return invoiceSummaryDTO;
	}

	/**
	 * Gets invoice for agent.
	 * 
	 * @param critiria
	 *            the search critiria.
	 * @return search invoice.
	 * @see com.isa.thinair.invoicing.core.persistance.dao.InvoiceDAO #getInvoiceForAgent(java.util.List)
	 */
	public Collection<GeneratedInvoiceDTO> getGenaratedInvoicesForAgent(String agentGSACode, String dateFrom, String dateTo) {
		Object lstInvoceDTOs = null;

		DataSource ds = InvoicingModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		String strQuery = "select  invoice.invoice_number, invoice.invoice_date, invoice.pnr, invoice.invoice_amount_local from t_invoice invoice "
				+ "where invoice.invoice_date between to_date("
				+ dateFrom
				+ ")  and to_date("
				+ dateTo
				+ ") "
				+ " and invoice.agent_code='" + agentGSACode + "'";

		lstInvoceDTOs = templete.query(strQuery, new ResultSetExtractor() {
			GeneratedInvoiceDTO genInv = null;

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<GeneratedInvoiceDTO> colGenInvoices = new ArrayList();
				while (rs.next()) {
					GeneratedInvoiceDTO genInv = new GeneratedInvoiceDTO();
					genInv.setInvoiceNumber(rs.getString("invoice_number"));
					genInv.setInvoiceDate(new Date(rs.getTimestamp("invoice_date").getTime()));
					genInv.setPnr(rs.getInt("pnr"));
					genInv.setInvoiceAmount(rs.getBigDecimal("invoice_amount_local"));
					colGenInvoices.add(genInv);
				}
				return colGenInvoices;
			}
		});

		return (Collection) lstInvoceDTOs;
	}

	/**
	 * Method to set invoice number rule. FORMAT - YYMMBB9999 Where, YY = Year MM = Month BB = Billing period 9999 =
	 * Sequence Number.
	 * 
	 * @return constructed invoice number.
	 */
	private String getInvoiceNo(Date invoiceDate, int billingPeriod) {
		String invoiceNumber = null;
		String year = null;
		String month = null;
		String bilp = null;
		List invoiceList = null;
		int max = 0;
		int temp = 0;
		String invoiceSeq = null;
		Calendar inCal = new GregorianCalendar();
		inCal.setTime(invoiceDate);

		year = Integer.toString(inCal.get(Calendar.YEAR)).substring(2);

		if (inCal.get(Calendar.MONTH) < 9) {
			month = "0" + Integer.toString(inCal.get(Calendar.MONTH) + 1);
		} else {
			month = Integer.toString(inCal.get(Calendar.MONTH) + 1);
		}

		bilp = "0" + Integer.toString(billingPeriod);

		invoiceList = find("select invoice.invoiceNumber" + " from Invoice as invoice", String.class);

		Iterator iter = invoiceList.iterator();
		while (iter.hasNext()) {
			String element = (String) iter.next();
			temp = Integer.parseInt(element.substring(6));
			if (temp > max) {
				max = temp;
			}
		}

		max = max + 1;
		invoiceSeq = Integer.toString(max);

		if (invoiceSeq.length() == 1) {
			invoiceSeq = "00000" + invoiceSeq;
		} else if (invoiceSeq.length() == 2) {
			invoiceSeq = "0000" + invoiceSeq;
		}

		else if (invoiceSeq.length() == 3) {
			invoiceSeq = "000" + invoiceSeq;
		} else if (invoiceSeq.length() == 4) {
			invoiceSeq = "00" + invoiceSeq;
		} else if (invoiceSeq.length() == 5) {
			invoiceSeq = "0" + invoiceSeq;
		}

		invoiceNumber = year.concat(month).concat(bilp).concat(invoiceSeq);

		return invoiceNumber;
	}

	/**
	 * InvoiceDAO: updateInvoiceStatus(Date from, Collection agentCodes, boolean transferStatus)
	 * 
	 * @param from
	 * @param agentCodes
	 * @param transferStatus
	 */
	public void updateInvoiceStatus(Date from, Collection agentCodes, String transferStatus) {

		DataSource ds = InvoicingModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);
		String sql = "update T_INVOICE set TRANSFER_STATUS=? where trunc(INVIOCE_PERIOD_FROM)=? "
				+ BLUtil.getSQlForFilteringAgents(agentCodes, "AGENT_CODE");

		Object params[] = { transferStatus, from };
		templete.update(sql, params);

	}

	public Invoice getReservationInvoicePerAgent(String agentGSACode, String year, String month, int billingPeriod) {

		DataSource ds = InvoicingModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);
		final Invoice inv = new Invoice();

		Date firstday = null;
		Date lastday = null;

		firstday = getCalculatedDate(year, month, billingPeriod, "FIRST");

		lastday = getCalculatedDate(year, month, billingPeriod, "LAST");
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");

		final Collection agentCodes = new ArrayList();
		Object params[] = { formatter.format(firstday), formatter.format(lastday), agentGSACode };

		String strQuery = "select nvl(sum(tr.amount),0) as sum, nvl(sum(tr.amount_local),0) as sum_local, tr.agent_code, a.account_code "
				+ " from T_AGENT_TRANSACTION  tr, T_AGENT a "
				+ " where   tr.agent_code = a.agent_code "
				+ " and   tr.NOMINAL_CODE in (1,7) "
				+ " and trunc(tr.TNX_DATE) >= ? and trunc(tr.TNX_DATE) <= ? "
				+ " and tr.agent_code = ?"
				+ " and(tr.sales_channel_code is null or tr.sales_channel_code <> 11) "
				+ "group by tr.agent_code, a.account_code";

		templete.query(strQuery, params, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				while (rs.next()) {
					String agentCode = rs.getString("AGENT_CODE");
					inv.setAgentCode(agentCode);
					inv.setInvoiceAmount(rs.getBigDecimal("SUM"));
					inv.setInvoiceAmountLocal(rs.getBigDecimal("SUM_LOCAL"));
					String accountCode = rs.getString("ACCOUNT_CODE");
					inv.setAccountCode(accountCode);
				}

				return agentCodes;
			}
		});

		return inv;

	}

	public Invoice getReservationInvoicePerAgent(String agentGSACode, String pnr) {

		DataSource ds = InvoicingModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);
		final Invoice inv = new Invoice();
		final Collection agentCodes = new ArrayList();

		String strQuery = "select nvl(sum(tr.amount),0) as sum, nvl(sum(tr.amount_local),0) as sum_local, tr.agent_code, a.account_code "
				+ " from T_AGENT_TRANSACTION  tr, T_AGENT a "
				+ " where   tr.agent_code = a.agent_code "
				+ " and   tr.NOMINAL_CODE in (1,7) "
				+ " and tr.pnr = '"
				+ pnr
				+ "' "
				+ " and tr.agent_code = '"
				+ agentGSACode
				+ "' "
				+ " and(tr.sales_channel_code is null or tr.sales_channel_code <> 11) "
				+ "group by tr.agent_code, a.account_code";

		templete.query(strQuery, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {

				while (rs.next()) {
					String agentCode = rs.getString("AGENT_CODE");
					inv.setAgentCode(agentCode);
					inv.setInvoiceAmount(rs.getBigDecimal("SUM"));
					inv.setInvoiceAmountLocal(rs.getBigDecimal("SUM_LOCAL"));
					String accountCode = rs.getString("ACCOUNT_CODE");
					inv.setAccountCode(accountCode);
				}

				return agentCodes;
			}
		});

		return inv;

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<InvoiceDTO> getExternalInvoicePerAgent(String agentGSACode, String year, String month, int billingPeriod,
			String entity) {
		DataSource ds = InvoicingModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);

		Date firstday = null;
		Date lastday = null;

		firstday = getCalculatedDate(year, month, billingPeriod, "FIRST");
		lastday = getCalculatedDate(year, month, billingPeriod, "LAST");
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");

		final List<InvoiceDTO> invoiceDtos = new ArrayList<InvoiceDTO>();

		Object params[] = { formatter.format(firstday), formatter.format(lastday), agentGSACode,entity };

		String strQuery = "select nvl(sum(tr.amount),0) as sum, nvl(sum(tr.amount_local),0) as sum_local,tr.agent_code, a.account_code , e.ENTITY_NAME"
				+ " from T_AGENT_TRANSACTION  tr, T_AGENT a , T_PRODUCT p ,T_ENTITY e"
				+ " where   tr.agent_code = a.agent_code "
				+ " AND tr.PRODUCT_TYPE =p.PRODUCT_TYPE "
				+ " AND p.ENTITY_CODE = e.ENTITY_CODE "
				+ " and tr.NOMINAL_CODE in (1,7,19,20,10,11)"
				+ " and trunc(tr.TNX_DATE) >= ? and trunc(tr.TNX_DATE) <= ? "
				+ " and tr.agent_code = ? "
				+ " and (e.ENTITY_CODE = ? OR tr.invoice_separated ='N')" 
				+ " group by e.ENTITY_NAME,tr.agent_code, a.account_code,e.DISPLAY_ORDER "
				+ " order by e.DISPLAY_ORDER ";

		templete.query(strQuery, params, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				InvoiceDTO invoiceDTO = null;
				while (rs.next()) {

					invoiceDTO = new InvoiceDTO();
					invoiceDTO.setAgentCode(rs.getString("AGENT_CODE"));
					invoiceDTO.setInvoiceAmount(rs.getBigDecimal("SUM"));
					invoiceDTO.setInvoiceAmountLocal(rs.getBigDecimal("SUM_LOCAL"));
					invoiceDTO.setAccountCode(rs.getString("ACCOUNT_CODE"));
					invoiceDTO.setEntityName(rs.getString("ENTITY_NAME"));
					invoiceDtos.add(invoiceDTO);

				}

				return invoiceDtos;
			}
		});

		return invoiceDtos;

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<InvoiceDTO> getExternalInvoicePerAgent(String agentGSACode, String pnr) {
		DataSource ds = InvoicingModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);
		
		final List<InvoiceDTO> invoiceDTOs = new ArrayList<InvoiceDTO>();

		String strQuery = "select nvl(sum(tr.amount),0) as sum, nvl(sum(tr.amount_local),0) as sum_local,tr.agent_code, a.account_code,e.ENTITY_NAME"
				+ " from T_AGENT_TRANSACTION  tr, T_AGENT a, T_PRODUCT p,T_ENTITY e "
				+ " where   tr.agent_code = a.agent_code "
				+ " AND tr.PRODUCT_TYPE =p.PRODUCT_TYPE"
				+ " AND  p.ENTITY_CODE = e.ENTITY_CODE "
				+ " and   tr.NOMINAL_CODE IN(1,7,19,20,10,11)"
				+ " and tr.pnr = '"
				+ pnr
				+ "'  "
				+ " and tr.agent_code = '" + agentGSACode + "' "
				+ " group by e.ENTITY_NAME,tr.agent_code, a.account_code,e.DISPLAY_ORDER "
				+ " order by e.DISPLAY_ORDER ";

		templete.query(strQuery, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				InvoiceDTO invoiceDTO = null;
				while (rs.next()) {
					invoiceDTO = new InvoiceDTO();
					invoiceDTO.setAgentCode(rs.getString("AGENT_CODE"));
					invoiceDTO.setInvoiceAmount(rs.getBigDecimal("SUM"));
					invoiceDTO.setInvoiceAmountLocal(rs.getBigDecimal("SUM_LOCAL"));
					invoiceDTO.setAccountCode(rs.getString("ACCOUNT_CODE"));
					invoiceDTO.setEntityName(rs.getString("ENTITY_NAME"));

					invoiceDTOs.add(invoiceDTO);

				}

				return invoiceDTOs;
			}
		});

		return invoiceDTOs;

	}


	private Date getCalculatedDate(String year, String month, int period, String strDay) {

		GregorianCalendar gc = new GregorianCalendar();

		gc.set(Calendar.YEAR, Integer.parseInt(year));
		gc.set(Calendar.MONTH, (Integer.parseInt(month) - 1));

		if (period == 1) {
			if (strDay.equals("FIRST")) {
				gc.set(Calendar.DATE, 1);
			} else {
				gc.set(Calendar.DATE, 15);
			}

		} else {
			if (strDay.equals("FIRST")) {
				gc.set(Calendar.DATE, 16);
			} else {
				gc.set(Calendar.DAY_OF_MONTH, gc.getActualMaximum(Calendar.DAY_OF_MONTH));
			}

		}
		return gc.getTime();

	}

	/**
	 * Retrives Invoice Export data for Each Invoice.
	 */
	public RowSet getInvoiceExportData(Collection<String> invoiceIDList) {
		DataSource ds = ReservationModuleUtils.getDatasource();
		JdbcTemplate template = new JdbcTemplate(ds);
		List<String> paramsList = new ArrayList<String>();
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT distinct a.invoice_number, a.invoice_date, a.agent_code, ");
		sb.append("   a.invoice_amount, a.pnr, agent.ACCOUNT_CODE as INCOME_CODE ");
		sb.append("   ,actual_p.payment_mode  ,ptn.description as TRX_TYPE_NOMINAL");
		sb.append(" FROM t_invoice a, T_AGENT agent,T_ACTUAL_PAYMENT_METHOD actual_p, ");
		sb.append(" t_pax_transaction pt,  T_PAX_TRNX_NOMINAL_CODE ptn, ");
		sb.append(" T_PNR_PASSENGER pnr_p ");
		sb.append(" where a.agent_code = agent.AGENT_CODE ");
		sb.append("	AND actual_p.payment_mode_id (+)= pt.payment_mode_id  ");
		sb.append("	AND a.pnr = pnr_p.pnr ");
		sb.append("	AND (pt.PAYMENT_CARRIER_CODE is null or pt.PAYMENT_CARRIER_CODE='" + AppSysParamsUtil.getDefaultCarrierCode()
				+ "') ");
		sb.append("	AND ptn.nominal_code = pt.nominal_code ");
		sb.append("	AND pt.pnr_pax_id = pnr_p.pnr_pax_id ");
		sb.append(" AND pt.nominal_code IN (");
		sb.append(Util.buildIntegerInClauseContent(ReservationTnxNominalCode.getOnAccountTypeNominalCodes()));
		sb.append(" ) AND ");

		int count = 0;
		for (Iterator<String> iterator = invoiceIDList.iterator(); iterator.hasNext();) {
			String invoiceNumber = iterator.next();
			paramsList.add(invoiceNumber);
			if (count == 0) {
				sb.append(" ( ");
			}
			sb.append("	a.invoice_number = ? ");
			if (iterator.hasNext()) {
				sb.append(" OR ");
			} else {
				sb.append(" ) ");
			}
			count++;
		}
		sb.append("	order by a.invoice_number ");

		try {
			final OracleCachedRowSet rowSet = new OracleCachedRowSet();
			template.query(sb.toString(), paramsList.toArray(), new ResultSetExtractor() {

				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					if (rs != null) {
						rowSet.populate(rs);
					}
					return null;
				}
			});
			return rowSet;
		} catch (SQLException e) {
			log.error("Error getInvoiceExportData " + e);
			throw new CommonsDataAccessException(e, "module.unidentified.error", InvoicingConstants.MODULE_NAME);
		}

	}

	private String combineAndBracketNominalCodes(List<Integer> nominalCodes) {
		String codes = null;
		if (nominalCodes != null && nominalCodes.size() > 0) {
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append("(");
			for (int code : nominalCodes) {
				stringBuilder.append(code);
				stringBuilder.append(",");
			}
			codes = stringBuilder.toString();
			codes = codes.substring(0, codes.length() - 1); // remove redundant comma
			codes += ")";
		}
		return codes;
	}

	

}
