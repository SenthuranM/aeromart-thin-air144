/**
 * 
 */
package com.isa.thinair.invoicing.core.bl.bsp;

import java.util.Stack;

import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.DPCProcessedBSPTnx;
import com.isa.thinair.invoicing.api.model.bsp.ParsedHOTFile;
import com.isa.thinair.invoicing.api.model.bsp.records.BFH01;
import com.isa.thinair.invoicing.api.model.bsp.records.BKP84;
import com.isa.thinair.invoicing.api.model.bsp.records.BKS24;

/**
 * @author Janaka Padukka
 * 
 */
public class HOTFileParser {

	public enum HOT_RECORD_TYPE {
		BFH01, BKS24, BKP84;
	}

	public ParsedHOTFile parse(String hotFileContent) throws BSPException {

		ParsedHOTFile parsedHOTFile = new ParsedHOTFile();
		// Parses hotfile content and extract transactions
		String[] hotFileLines = hotFileContent.split("\n");

		String startRecordType = getRecordType(hotFileLines[0]);

		if (HOT_RECORD_TYPE.BFH01.toString().equals(startRecordType)) {
			BFH01 headerRecord = new BFH01(hotFileLines[0], startRecordType);
			parsedHOTFile.setAirlineIdentifier(headerRecord.getTACN());
			parsedHOTFile.setProcessedDate(headerRecord.getPRDA());
			parsedHOTFile.setBspCountryCode(headerRecord.getISOC());
		} else {
			throw new BSPException("BFH01 Record not found at start!!!");
		}

		try {
			Stack<DPCProcessedBSPTnx> processedTransactionStack = new Stack<DPCProcessedBSPTnx>();
			for (int i = 1; i < hotFileLines.length; i++) {
				String recordType = getRecordType(hotFileLines[i]);
				// Identify Record type
				if (HOT_RECORD_TYPE.BKS24.toString().equals(recordType)) {
					BKS24 bks24Record = new BKS24(hotFileLines[i], recordType);
					DPCProcessedBSPTnx tnx = new DPCProcessedBSPTnx(bks24Record.getTDNR(), null, bks24Record.getPNRR(),
							bks24Record.getTRNC());
					processedTransactionStack.push(tnx);
				} else if (HOT_RECORD_TYPE.BKP84.toString().equals(recordType)) {
					DPCProcessedBSPTnx tnx = processedTransactionStack.pop();
					BKP84 bkp84Record = new BKP84(hotFileLines[i], recordType);
					tnx.setProcessedAmount(bkp84Record.getREMT());
					tnx.setProcessedCurrencyCode(bkp84Record.getCUTP());
					parsedHOTFile.addTransaction(tnx);
				} else {
					continue;
				}
			}
		} catch (Exception e) {
			throw new BSPException("Trannsaction record processing failed", e);
		}

		return parsedHOTFile;
	}

	private String getRecordType(String recordString) {
		return (recordString.substring(0, 3) + recordString.substring(11, 13));
	}

}
