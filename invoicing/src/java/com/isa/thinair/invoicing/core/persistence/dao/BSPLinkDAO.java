package com.isa.thinair.invoicing.core.persistence.dao;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.invoicing.api.model.BSPFailedTnx;
import com.isa.thinair.invoicing.api.model.BSPFileLog;
import com.isa.thinair.invoicing.api.model.BSPHOTFileLog;
import com.isa.thinair.invoicing.api.model.BSPWeblinkAccount;
import com.isa.thinair.invoicing.api.model.DishFileTnxLogData;
import com.isa.thinair.invoicing.api.model.EMDSTransaction;
import com.isa.thinair.invoicing.api.model.bsp.DPCProcessedBSPTnx;

public interface BSPLinkDAO {

	public void saveOrUpdateBSPFailedTransactions(Collection<BSPFailedTnx> bspFailedTnxDTOs);

	public Collection getFailedOWnTransactions(Collection<Integer> transactionIDs);

	public Collection getFailedDryInterlineTransactions(Collection<String> transactionIDs);

	public BSPFailedTnx getFailedOwnTnx(Integer tnxID, int paxSequence);

	public BSPFailedTnx getFailedExternalTnx(String lccUniqueID, int paxSequence);

	public void saveOrUpdateDishFileTnxLogData(Collection<DishFileTnxLogData> logs);

	public Page getBSPReports(int startrec, int numofrecs) throws ModuleException;

	public Page getBSPReports(int startrec, int numofrecs, List criteria) throws ModuleException;

	public List getBSPReports();

	public BSPFailedTnx getChangingBSPFailedTnx(String bspTxnLogID);

	public DishFileTnxLogData getBSPDishFileLogData(Long eticketNumber);

	public Integer getLastFileSequence(String countryCode, Date date) throws ModuleException;

	public void saveOrUpdateBSPFileLog(BSPFileLog bspFileLog);
	
	public void saveOrUpdateBSPFileLogs(Collection<BSPFileLog> bspFileLogs);
	
	public List<BSPFileLog> getUploadPendingRetFiles();
	
	public Page getBSPHOTFileRecords(int startrec, int numofrecs, List criteria) throws ModuleException;
	
	public BSPHOTFileLog getHOTFileByName(String HOTFileName);
	
	public void saveOrUpdateBSPHOTFile(BSPHOTFileLog hotFile);
	
	public void deleteBSPHOTFile(Integer hotFileLogId);
	
	public List<DishFileTnxLogData> getMatchingTransactions(List<DPCProcessedBSPTnx> transactions);
	
	public void saveOrUpdateEMDSTransaction(EMDSTransaction emdsTransaction);
	
	public EMDSTransaction getEMDSTransction(Integer tnxID, String lccUniqueID, Integer paxSequence, String extReference);
	
	public void deleteBSPFileLog(String fileLogName);
	
	public List<BSPWeblinkAccount> getBSPSftpAccounts();
	
}
