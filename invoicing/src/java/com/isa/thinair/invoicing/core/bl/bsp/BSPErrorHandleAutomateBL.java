package com.isa.thinair.invoicing.core.bl.bsp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.SFTPUtil;
import com.isa.thinair.invoicing.api.model.BSPFailedTnx;
import com.isa.thinair.invoicing.api.model.bsp.BSPRejectedTnxInfo;
import com.isa.thinair.invoicing.api.service.InvoicingModuleUtils;
import com.isa.thinair.invoicing.core.util.BSPCoreUtils;
import com.isa.thinair.platform.api.constants.PlatformConstants;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;

public class BSPErrorHandleAutomateBL {

	private SFTPUtil sftp;

	public void executeAutomate(UserPrincipal userPrincipal) throws ModuleException {
		sftp = new SFTPUtil(AppSysParamsUtil.getBSPSFTPHost(), AppSysParamsUtil.getBSPSFTPUserName(),
				AppSysParamsUtil.getBSPSFTPPassword());
		GlobalConfig globalConfig = InvoicingModuleUtils.getGlobalConfig();
		if (globalConfig.getUseProxy()) {
			sftp.setHttpProxy(globalConfig.getHttpProxy(), globalConfig.getHttpPort());
		}
		String fileName = ""; // TODO : prepare the file name
		try {
			downloadFile(fileName);
		} catch (SftpException e) {
			throw new ModuleException(""); // TODO
		} catch (JSchException e) {
			throw new ModuleException(""); // TODO
		}
		String countryCode = "A7"; // TODO
		List<String> errors = getErrorsFromFile(fileName);
		processAndRecordErrors(countryCode, errors, userPrincipal);
	}

	private void downloadFile(String fileName) throws SftpException, JSchException {
		StringBuilder localFilePath = new StringBuilder();
		localFilePath.append(PlatformConstants.getAbsBSPDownloadPath()).append(File.separatorChar);
		localFilePath.append(fileName);
		sftp.downloadFile(localFilePath.toString(), fileName);
	}

	private List<String> getErrorsFromFile(String fileName) throws ModuleException {
		ArrayList<String> fileContent = new ArrayList<String>();

		try {
			BufferedReader inReader = new BufferedReader(new FileReader(fileName));
			String line = inReader.readLine();
			while (line != null && !line.equals("EOF")) {
				fileContent.add(line);
				line = inReader.readLine();
			}
			inReader.close();
		} catch (Exception exp) {
			throw new ModuleException("BSP Error File Reading failed");
		}

		return fileContent;
	}

	private void processAndRecordErrors(String countryCode, List<String> errors, UserPrincipal userPrincipal) {
		List<BSPRejectedTnxInfo> rejectedTnxs = BSPCoreUtils.parseBSPErrors(errors);

		List<BSPFailedTnx> bspFailedTnxs = new ArrayList<BSPFailedTnx>();
		Date date = new Date();
		for (BSPRejectedTnxInfo rejTnx : rejectedTnxs) {
			BSPFailedTnx failedTnx = BSPCoreUtils.composeErrorDTO(rejTnx, userPrincipal, date, countryCode);
			bspFailedTnxs.add(failedTnx);
		}

		BSPCoreUtils.recordBSPFailedErrors(bspFailedTnxs);
	}
}
