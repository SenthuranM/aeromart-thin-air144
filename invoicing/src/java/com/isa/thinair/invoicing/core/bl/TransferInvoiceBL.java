/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.invoicing.core.bl;

import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airtravelagents.api.util.AirTravelAgentConstants;
import com.isa.thinair.airtravelagents.api.util.BLUtil;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.bl.ExternalAccountingSystem;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.commons.core.util.XDBConnectionUtil;
import com.isa.thinair.invoicing.api.dto.GeneratedInvoiceDTO;
import com.isa.thinair.invoicing.api.dto.SearchInvoicesDTO;
import com.isa.thinair.invoicing.api.service.InvoicingModuleUtils;
import com.isa.thinair.invoicing.api.util.InvoicingInternalConstants;
import com.isa.thinair.invoicing.core.persistence.dao.TransferInvoiceDAO;
import com.isa.thinair.invoicing.core.persistence.dao.TransferInvoiceJdbcDAO;
import com.isa.thinair.invoicing.core.service.bd.SendInvoicesViaMDB;

/**
 * @author Byorn
 * 
 */
public class TransferInvoiceBL {

	private static final Log log = LogFactory.getLog(TransferInvoiceBL.class);

	public static String INVOICEDAO = "invoiceDAO";

	/**
	 * Method Called when user selects all the agents for the period.
	 * 
	 * @param searchInvoicesDTO
	 * @throws ModuleException
	 */
	public void sendInvoiceMail(Collection<GeneratedInvoiceDTO> generatedInvoices) throws ModuleException {

		// create attachment and send mail via mdb,
		// note: mdb will call a service bean which will call InvoiceBL().sendInvoiceMail(Collection
		// generatedInviocesDTOs);
		if (generatedInvoices != null) {
			for (GeneratedInvoiceDTO generatedInvoiceDTO : generatedInvoices) {
				new SendInvoicesViaMDB().sendEmailsViaMDB(generatedInvoiceDTO);
			}
		}
	}

	public Collection<GeneratedInvoiceDTO> getGeneratedInvoices(SearchInvoicesDTO searchInvoicesDTO) throws ModuleException {

		TransferInvoiceJdbcDAO invoiceJdbcDAO = InvoicingModuleUtils.getTransferInvoiceJdbcDAO();
		boolean isByTransaction = searchInvoicesDTO.isByTrasaction();
		Date periodStartDate = null;
		Date periodEndDate = null;
		if (!isByTransaction) {
			// construct the start date of the period
			periodStartDate = BLUtil.getFromDate(searchInvoicesDTO);
			// construct the end date of the period
			periodEndDate = BLUtil.getToDate(searchInvoicesDTO);
		} else {
			periodStartDate = searchInvoicesDTO.getInvoiceDateFrom();
			periodEndDate = searchInvoicesDTO.getInvoiceDateTo();
		}
		Collection<GeneratedInvoiceDTO> generatedInvoices = invoiceJdbcDAO.getInvoices(searchInvoicesDTO);

		if (generatedInvoices == null || generatedInvoices.size() == 0) {
			throw new ModuleException("airtravelagent.param.nullempty", AirTravelAgentConstants.MODULE_NAME);
		}

		// iterator for all the agents invoices to collect the invoice numbers to show as processed
		Iterator<GeneratedInvoiceDTO> genInvoiceIterator = generatedInvoices.iterator();

		Iterator<String> agentIter = null;
		String strAgCode = null;

		while (genInvoiceIterator.hasNext()) {
			GeneratedInvoiceDTO generatedInvoiceDTO = (GeneratedInvoiceDTO) genInvoiceIterator.next();
			generatedInvoiceDTO.setSearchInvoicesDTO(searchInvoicesDTO);
		}
		return generatedInvoices;
	}

	/**
	 * Method used to transfer invoice to queue by fetching data using DAO method.
	 * 
	 * @param transferInvoicesDTO
	 * @throws ModuleException
	 */
	public void transferToInterfaceTables(SearchInvoicesDTO transferInvoicesDTO) throws ModuleException {

		// if invoice period is 1 then day is 1st, if period is 2 then day is
		// 16th
		String transferToExternal = InvoicingModuleUtils.getInvoicingConfig().getTransferToSqlServer();
		if (transferToExternal == null
				|| !(AirTravelAgentConstants.TRANSFER_TO_EXTERNAL_SYSTEM.equalsIgnoreCase(transferToExternal))) {
			throw new ModuleException("airtravelagent.exdb.notconfigured");
		}
		// Reading accounting systems from invoice config
		List<String> accountingSystemName = InvoicingModuleUtils.getInvoicingConfig().getInvoiceTransferAccountingSystemList();
		try {
			for (String system : accountingSystemName) {
				if (StringUtil.isNullOrEmpty(system)) {
					log.error("Accounting system couldn't find. Please configure it");
					throw new ModuleException("module.dependencymap.invalid");
				} else {
					ExternalAccountingSystem accountingSystem = (ExternalAccountingSystem) BLUtil
							.getExternalAccoutingSystemsImplConfig().getAccountingSystemMap().get(system);

					TransferInvoiceJdbcDAO invoiceJdbcDAO = InvoicingModuleUtils.getTransferInvoiceJdbcDAO();
					// passing accounting system (holding connection data) to below method
					if (invoiceJdbcDAO.getInvoiceTotalinExternalDB(transferInvoicesDTO, (XDBConnectionUtil) accountingSystem,
							system) > 0) {
						throw new ModuleException("airtravelagent.invoicetransfer.invoiceexist");
					}
					int fromday = transferInvoicesDTO.getInvoicePeriod() == 1 ? 1 : 16;
					// construct the start date of the period
					Date periodStartDate = new GregorianCalendar(transferInvoicesDTO.getYear(),
							transferInvoicesDTO.getMonth() - 1, fromday).getTime();
					if (AppSysParamsUtil.isWeeklyInvoiceEnabled()) {
						SearchInvoicesDTO searchinvoicesDTO = new SearchInvoicesDTO();
						searchinvoicesDTO.setYear(transferInvoicesDTO.getYear());
						searchinvoicesDTO.setMonth(transferInvoicesDTO.getMonth());
						searchinvoicesDTO.setInvoicePeriod(transferInvoicesDTO.getInvoicePeriod());
						periodStartDate = BLUtil.getFromDate(searchinvoicesDTO);
					}
					Collection invoices = null;
					TransferInvoiceDAO invoiceDAO = InvoicingModuleUtils.getTransferInvoiceDAO();
					// retrive invoices for all or selected agents.
					try {
						invoices = invoiceDAO.getInvoicesForAgents(periodStartDate, transferInvoicesDTO.getAgentCodes(), system);
					} catch (CommonsDataAccessException e) {
						log.error("Error in getting invoices for Period " + periodStartDate, e);
					}
					// if there were no invoices generated then throw exception
					// to the user.
					// if not insert the date to the interface tables.
					if (!(invoices == null || invoices.size() == 0)) {
						invoiceDAO.updateInvoiceStatus(periodStartDate, transferInvoicesDTO.getAgentCodes(),
								InvoicingInternalConstants.TransferStatus.IN_PROGRESS);
						new SendInvoicesViaMDB().transferViaMDB(invoices);
					} else {
						throw new ModuleException("airtravelagent.invoices.notfound", AirTravelAgentConstants.MODULE_NAME);
					}
				}
			}
		} catch (Exception ex) {
			log.error("Transfer of invoice to external system failed", ex);
		}

	}

	/**
	 * Method called when user selects agents for each page.
	 * 
	 * @param generatedInvoicesDTOs
	 * @throws ModuleException
	 */
	public boolean sendInvoiceMail(GeneratedInvoiceDTO generatedInvoicesDTO) throws ModuleException {

		if (generatedInvoicesDTO == null) {
			throw new ModuleException("airtravelagent.param.nullempty", "invoicing");
		}
		TransferInvoiceEmail airTravelAgentEmail = new TransferInvoiceEmail();
		return airTravelAgentEmail.emailInvoice(generatedInvoicesDTO);
	}

}
