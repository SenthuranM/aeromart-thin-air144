package com.isa.thinair.invoicing.core.service.bd;

import javax.ejb.Remote;

import com.isa.thinair.invoicing.api.service.InvoicingBD;

@Remote
public interface InvoicingServiceDelegateImpl extends InvoicingBD {

}