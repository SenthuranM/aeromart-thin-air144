/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.invoicing.core.persistence.dao;

import java.util.Collection;

import com.isa.thinair.commons.core.util.XDBConnectionUtil;

public interface ExternalInvoiceTransferDAO {
	public void insertInvoicesToInterfaceTable(Collection invoiceCollection, XDBConnectionUtil accountingSystem);
}
