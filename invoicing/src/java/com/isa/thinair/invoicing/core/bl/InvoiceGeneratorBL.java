/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.invoicing.core.bl;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airreservation.api.dto.AgentTotalSaledDTO;
import com.isa.thinair.airtravelagents.api.service.TravelAgentBD;
import com.isa.thinair.airtravelagents.api.util.AirTravelAgentConstants;
import com.isa.thinair.airtravelagents.api.util.BLUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.framework.DefaultServiceResponse;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.invoicing.api.dto.GenerateInvoiceOption;
import com.isa.thinair.invoicing.api.dto.SearchInvoicesDTO;
import com.isa.thinair.invoicing.api.model.Invoice;
import com.isa.thinair.invoicing.api.model.InvoiceSettlement;
import com.isa.thinair.invoicing.api.service.InvoicingModuleUtils;
import com.isa.thinair.invoicing.api.util.InvoicingInternalConstants;
import com.isa.thinair.invoicing.api.utils.InvoicingConstants;
import com.isa.thinair.invoicing.core.persistence.dao.InvoiceDAO;
import com.isa.thinair.invoicing.core.persistence.dao.InvoiceJdbcDAO;
import com.isa.thinair.invoicing.core.util.InvoiceNumberGenerator;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * @author Byorn
 * 
 */
public class InvoiceGeneratorBL {

	private static final Log log = LogFactory.getLog(InvoiceGeneratorBL.class);

	/**
	 * Generate Invoices for selected period NOTE: Invoices Will be generated for Agents who have OnAccount
	 * Transactions.
	 * 
	 * @param searchInvoicesDTO
	 * @throws ModuleException
	 */
	public ServiceResponce generateInvoices(SearchInvoicesDTO searchInvoicesDTO, GenerateInvoiceOption generateInvoiceOption)
			throws ModuleException {

		log.info("============= Starting to Generate Invoices =============================");

		boolean blnPayCurr = false;

		InvoiceJdbcDAO invoiceJdbcDAO = InvoicingModuleUtils.getInvoiceJdbcDAO();
		TravelAgentBD agentBD = InvoicingModuleUtils.lookupTravelAgentBD();

		// first check if any invoices are generated for this period.
		// take necessary action.
		if (invoiceJdbcDAO.getCountOfInvoices(searchInvoicesDTO) > 0) {
			throw new ModuleException("airtravelagent.invoicegenerate.invoiceexist");

		}

		// For iterating throught the collection return from Agent Transaction
		// table.

		// construct the start date of the period
		Date periodStartDate = BLUtil.getFromDate(searchInvoicesDTO);
		// construct the end date of the period
		Date periodEndDate = BLUtil.getToDate(searchInvoicesDTO);

		Collection<String> srcCol = searchInvoicesDTO.getAgentCodes();

		if (srcCol == null || srcCol.isEmpty()) {
			srcCol = new ArrayList<String>();

			if (log.isDebugEnabled()) {
				log.debug("========Going to retrieve agent collection ================" + new Date());
			}
			Collection agCol = agentBD.getAllOwnAirlineAgents();
			Collection<String> agCol2 = new ArrayList(agCol);

			if (log.isDebugEnabled()) {
				log.debug("========Retrieved agent collection ================" + new Date());
			}

			for (String string : agCol2) {
				srcCol.add(string);
			}
		}
		searchInvoicesDTO.setAgentCodes(srcCol);

		if ("Y".equalsIgnoreCase(InvoicingModuleUtils.getGlobalConfig().getBizParam(
				SystemParamKeys.STORE_PAYMENTS_IN_AGENTS_CURRENCY))
				&& "Y".equalsIgnoreCase(InvoicingModuleUtils.getGlobalConfig().getBizParam(
						SystemParamKeys.INVOICE_IN_AGENTS_CURRENCY))) {
			blnPayCurr = true;
		}

		DefaultServiceResponse response = new DefaultServiceResponse(true);

		Collection agentTotalSalesDTOs = null;
		
		Collection agentTotalExternalSalesDTOs = null;

		// retrieve ALL / for a group of Agents The Agent Total Sales from the
		// Agent Transaction Table.
		if (log.isDebugEnabled()) {
			log.debug("========Retrieving Agent Total Sales For the Period================");
		}
		if (blnPayCurr) {
			// returns only reservation transactions
			agentTotalSalesDTOs = invoiceJdbcDAO.getAgentTotalSalesForInvoicingPayCurr(periodStartDate, periodEndDate, srcCol);
			// returns external transactions
			agentTotalExternalSalesDTOs = invoiceJdbcDAO.getAgentTotalExternalSalesForInvoicing(periodStartDate, periodEndDate, srcCol);
			if (agentTotalExternalSalesDTOs != null && agentTotalExternalSalesDTOs.size() != 0){
				agentTotalSalesDTOs.addAll(agentTotalExternalSalesDTOs);
			}
		} else {
			agentTotalSalesDTOs = invoiceJdbcDAO.getAgentTotalSalesForInvoicing(periodStartDate, periodEndDate, srcCol);
		}

		// if no records throw exception.
		if (agentTotalSalesDTOs == null || agentTotalSalesDTOs.size() == 0) {
			throw new ModuleException("airtravelagent.agtotsales.notfound", InvoicingConstants.MODULE_NAME);
		}
		if (blnPayCurr) {
			if (agentTotalSalesDTOs != null && agentTotalSalesDTOs.size() > 0) {
				createInvoices(agentTotalSalesDTOs, blnPayCurr, periodStartDate, periodEndDate, searchInvoicesDTO);
			}

		} else {
			createInvoices(agentTotalSalesDTOs, blnPayCurr, periodStartDate, periodEndDate, searchInvoicesDTO);
		}

		return response;
	}

	private void createInvoices(Collection agentTotalSalesDTOs, boolean blnPayCurr, Date periodStartDate, Date periodEndDate,
			SearchInvoicesDTO searchInvoicesDTO) throws ModuleException {

		InvoiceDAO invoiceDAO = InvoicingModuleUtils.getInvoiceDAO();
		Iterator agentTotalSalesIterator;
		String includeCommission = InvoicingModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.AGENT_COMMISSION);
		String baseCurrency = InvoicingModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.BASE_CURRENCY);
		InvoiceJdbcDAO invoiceJdbcDAO = InvoicingModuleUtils.getInvoiceJdbcDAO();
		String invoiceNoFormat = InvoicingModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.INVOICE_NO_FORMAT);

		agentTotalSalesIterator = agentTotalSalesDTOs.iterator();

		// keep a collection of

		// (3) invoices for agents without transactions
		// (1) zero and positive invoices
		Collection<Invoice> invoicesCollection = new ArrayList<Invoice>();
		Collection<Invoice> invoicesWithRefunds = new ArrayList<Invoice>();
		Collection<Invoice> invoicesWithoutTnx = new ArrayList<Invoice>();
		ExchangeRateProxy exchangeRateProxy = new ExchangeRateProxy(CalendarUtil.getCurrentSystemTimeInZulu());

		boolean agentCommissionEnabled = AppSysParamsUtil.isAgentCommmissionEnabled();

		// iterate throught the agent total sales from Agent Transaction Table
		if (log.isDebugEnabled()) {
			log.debug("========Iterating the AgentTotalSalesDTOs and Preparing the Invoice Collections ================");
		}
		while (agentTotalSalesIterator.hasNext()) {

			Invoice invoice = new Invoice();
			AgentTotalSaledDTO agentTotalSaledDTO = (AgentTotalSaledDTO) agentTotalSalesIterator.next();

			invoice.setAgentCode(agentTotalSaledDTO.getAgentCode());
			invoice.setInviocePeriodFrom(periodStartDate);
			invoice.setInvoicePeriodTo(periodEndDate);
			invoice.setEntityCode(agentTotalSaledDTO.getEntityCode());

			if (agentCommissionEnabled) {
				String agentCode = agentTotalSaledDTO.getAgentCode();
				if (agentCode != null && !agentCode.isEmpty()) {
					BigDecimal offeredCommissions = InvoicingModuleUtils.lookupTravelAgentFinanceBD()
							.getTotalOfferedAgentCommissions(agentCode, periodStartDate, periodEndDate);
					invoice.setCommissionAmount(offeredCommissions);
				}
			}

			if (blnPayCurr) {
				invoice.setInvoiceAmountLocal(agentTotalSaledDTO.getTotalSalesLocal());
			} else {
				try {
					invoice.setInvoiceAmountLocal(BLUtil.getAmountInLocal(agentTotalSaledDTO.getTotalSales(),
							agentTotalSaledDTO.getCurrencyCode(), exchangeRateProxy));
				} catch (ModuleException e) {
					invoice.setInvoiceAmountLocal(agentTotalSaledDTO.getTotalSales());
					agentTotalSaledDTO.setCurrencyCode(baseCurrency);
				}
			}
			invoice.setInvoiceAmount(agentTotalSaledDTO.getTotalSales());

			invoice.setBillingPeriod(searchInvoicesDTO.getInvoicePeriod());

			invoice.setTransferTimestamp(new Timestamp(new Date().getTime()));
			// keeping the current date as the invoice date.
			invoice.setInvoiceDate(periodEndDate);

			// if invoice amount is zero keep it as a settle invoice, else its
			// an unsettled invoice
			if (invoice.getInvoiceAmount().compareTo(BigDecimal.ZERO) == 0) {
				invoice.setStatus(InvoicingInternalConstants.InvoiceStatus.INVOICE_SETTLED);
			} else {
				invoice.setStatus(InvoicingInternalConstants.InvoiceStatus.INVOICE_UNSETTLED);
			}

			invoice.setTransferStatus(InvoicingInternalConstants.TransferStatus.NOT_TRANSFERED);
			invoice.setMailStatus(AirTravelAgentConstants.EmailStatus.NOT_SENT);
			invoice.setNumOfTimesMailSent(new Integer(0));

			// if the agent has a refund then put the invoice to the negative
			// invioces collection else put it in the positive
			// and zero invoices collection
			if (agentTotalSaledDTO.getTotalSales().doubleValue() < 0) {

				invoicesWithRefunds.add(invoice);
				continue;
			}
			invoicesCollection.add(invoice);

		}

		log.info("========Finished preparing invoice collections===========");

		// generate invoice numbers for the invoices.
		if (log.isDebugEnabled()) {
			log.debug("===  Generating Invoice Numbers for Agents with Zero and Positive Values ");
		}

		Collection<Invoice> invoices = new InvoiceNumberGenerator().getInvoiceNos(periodEndDate,
				searchInvoicesDTO.getInvoicePeriod(), invoicesCollection, invoiceNoFormat);

		// save the positive and zero invoices.

		invoiceDAO.saveInvoices(invoices);

		// generate invoice numbers for the negative invoices.
		if (log.isDebugEnabled()) {
			log.debug("=== Generating Invoice Numbers for Negative Invoices=====");
		}

		Collection<Invoice> negInvoices = new InvoiceNumberGenerator().getInvoiceNos(periodEndDate,
				searchInvoicesDTO.getInvoicePeriod(), invoicesWithRefunds, invoiceNoFormat);

		// save the negative invoices

		invoiceDAO.saveInvoices(negInvoices);

		// check if for all the onaccount agents invoices were generated.
		// i.e Agents Without Transactions done for the period..create a zero
		// invoice.
		// if not then generate a invoice with zero value.
		// note: this is done only if generated invoices for the entire period is selected.
		// for selected agents this wont apply.

		if (searchInvoicesDTO.getAgentCodes() == null) {
			Collection agentsWithoutTnx = invoiceJdbcDAO.getAgentsWithoutTransactions(searchInvoicesDTO);
			log.debug("=======Successfully fetched Agents Without Transactions========");
			Iterator agentsWithoutTnxIterator = agentsWithoutTnx.iterator();
			while (agentsWithoutTnxIterator.hasNext()) {
				Invoice invoice = new Invoice();
				AgentTotalSaledDTO agentTotalSaledDTO = (AgentTotalSaledDTO) agentsWithoutTnxIterator.next();
				invoice.setAgentCode(agentTotalSaledDTO.getAgentCode());
				invoice.setInviocePeriodFrom(periodStartDate);
				invoice.setInvoicePeriodTo(periodEndDate);
				invoice.setInvoiceAmount(AccelAeroCalculator.getDefaultBigDecimalZero());
				invoice.setBillingPeriod(searchInvoicesDTO.getInvoicePeriod());
				invoice.setTransferTimestamp(new Timestamp(new Date().getTime()));
				// keeping the current date as the invoice date.
				invoice.setInvoiceDate(periodEndDate);
				// if invoice amount is zero keep it as a settle invoice, else
				// its
				// an unsettled invoice
				invoice.setStatus(InvoicingInternalConstants.InvoiceStatus.INVOICE_SETTLED);
				invoice.setTransferStatus(InvoicingInternalConstants.TransferStatus.NOT_TRANSFERED);
				invoice.setMailStatus(AirTravelAgentConstants.EmailStatus.NOT_SENT);
				invoice.setNumOfTimesMailSent(new Integer(0));
				invoicesWithoutTnx.add(invoice);
			}

			// generate invoice numbers for the zero invoices.
			log.debug("=== Generating Invoice Numbers for Zero Invoices=====");
			Collection zeroInvoices = new InvoiceNumberGenerator().setInvoiceNos(periodEndDate,
					searchInvoicesDTO.getInvoicePeriod(), invoicesWithoutTnx);

			// save the zero
			invoiceDAO.saveInvoices(zeroInvoices);
		}
		// settle invoices(negative invoices)/(with refunds)
		Iterator invoicesWithRefundsIterator = negInvoices.iterator();

		// iterate throught the invoices that have refunds and settle them.
		log.info("=======Going to make Settlements for Negative Invoices ============");
		while (invoicesWithRefundsIterator.hasNext()) {

			Invoice invoice = (Invoice) invoicesWithRefundsIterator.next();

			InvoiceSettlement invoiceSettlement = new InvoiceSettlement();
			invoiceSettlement.setAmountPaid(invoice.getInvoiceAmount().abs());

			invoiceSettlement.setInvoiceNo(invoice.getInvoiceNumber());

			invoiceSettlement.setSettlementDate(new Date());
			invoiceSettlement.setCpCode("INV");
			ArrayList setledInvoice = new ArrayList();
			setledInvoice.add(invoiceSettlement);

			/**
			 * @TDOD: Need to modifiy the below method to work for a collection of agent codes
			 */
			new InvoiceBL().settleInvoice(invoice.getAgentCode(), setledInvoice, false);

		}
		log.info("=======Going to make Settlements for Negative Invoices ============");

	}
}
