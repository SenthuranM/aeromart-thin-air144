package com.isa.thinair.invoicing.core.persistence.hibernate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;

import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.framework.PlatformBaseHibernateDaoSupport;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.invoicing.api.model.BSPFailedTnx;
import com.isa.thinair.invoicing.api.model.BSPFileLog;
import com.isa.thinair.invoicing.api.model.BSPHOTFileLog;
import com.isa.thinair.invoicing.api.model.BSPWeblinkAccount;
import com.isa.thinair.invoicing.api.model.DishFileTnxLogData;
import com.isa.thinair.invoicing.api.model.EMDSTransaction;
import com.isa.thinair.invoicing.api.model.bsp.BSPTransactionDataDTO.TransactionType;
import com.isa.thinair.invoicing.api.model.bsp.DPCProcessedBSPTnx;
import com.isa.thinair.invoicing.api.util.InvoicingInternalConstants.BSPErrorLogStatus;
import com.isa.thinair.invoicing.core.persistence.dao.BSPLinkDAO;

/**
 * @isa.module.dao-impl dao-name="bspLinkDAO"
 */
public class BSPLinkDAOImpl extends PlatformBaseHibernateDaoSupport implements BSPLinkDAO {

	private static final Log log = LogFactory.getLog(BSPLinkDAOImpl.class);

	public void saveOrUpdateBSPFailedTransactions(Collection<BSPFailedTnx> bspFailedTnxDTOs) {
		if (log.isDebugEnabled()) {
			log.debug("Saving/Updating bsp failed transaxctions ");
		}
		hibernateSaveOrUpdateAll(bspFailedTnxDTOs);
	}

	public Collection getFailedOWnTransactions(Collection<Integer> transactionIDs) {
		return find(
				"from BSPFailedTnx ft where ft.tnxID in (" + Util.buildIntegerInClauseContent(transactionIDs) + ")", BSPFailedTnx.class);
	}

	public Collection getFailedDryInterlineTransactions(Collection<String> transactionIDs) {
		return find(
				"from BSPFailedTnx ft where ft.lccUniqueID in (" + Util.buildStringInClauseContent(transactionIDs) + ")", BSPFailedTnx.class);
	}

	public Collection getResubmitTransactions(String agentCode) throws ModuleException {
		return find("from BSPFailedTnx ft where ft.status = ? and ft.agentCode = ?",
				new Object[] { BSPErrorLogStatus.RESUBMIT.toString(), agentCode }, BSPFailedTnx.class);
	}

	public BSPFailedTnx getFailedOwnTnx(Integer tnxID, int paxSequence) {
		String hql = "from BSPFailedTnx ft where ft.tnxID = ? and ft.paxSequence = ?";
		return (BSPFailedTnx) find(hql, new Object[] { tnxID, paxSequence }, BSPFailedTnx.class).get(0);
	}

	public BSPFailedTnx getFailedExternalTnx(String lccUniqueID, int paxSequence) {
		String hql = "from BSPFailedTnx ft where ft.lccUniqueID = ? and ft.paxSequence = ?";
		return (BSPFailedTnx) find(hql, new Object[] { lccUniqueID, paxSequence }, BSPFailedTnx.class).get(0);
	}

	public void saveOrUpdateDishFileTnxLogData(Collection<DishFileTnxLogData> logs) {
		if (log.isDebugEnabled()) {
			log.debug("Saving/Updating DishFileTnxLogData- Start");
		}
		hibernateSaveOrUpdateAll(logs);
		
		if (log.isDebugEnabled()) {
			log.debug("Saving/Updating DishFileTnxLogData- End");
		}
	}

	public DishFileTnxLogData getBSPDishFileLogData(Long eticketNumber) {
		return (DishFileTnxLogData) find(
				"from DishFileTnxLogData lg where lg.primaryETicket = ? or lg.secondaryEticket = ?",
				new Object[] { eticketNumber, eticketNumber }, DishFileTnxLogData.class).get(0);
	}

	public Page getBSPReports(int startrec, int numofrecs) throws ModuleException {
		List ob = new ArrayList();
		ob.add("bspReportId");
		return getPagedData(null, startrec, numofrecs, BSPFailedTnx.class, ob);
	}

	public Page getBSPReports(int startrec, int numofrecs, List criteria) throws ModuleException {
		return getPagedData(criteria, startrec, numofrecs, BSPFailedTnx.class, null);
	}

	public List getBSPReports() {
		return find("from BSPFailedTnx", BSPFailedTnx.class);
	}

	public BSPFailedTnx getChangingBSPFailedTnx(String bspTxnLogID) {
		BSPFailedTnx bspFailedTnx = null;
		List bspFailedTnxes = null;

		bspFailedTnxes = find(
				"select bspFailedTnx from BSPFailedTnx " + "as bspFailedTnx where bspFailedTnx.bspTxnLogID=?", bspTxnLogID, BSPFailedTnx.class);

		if (bspFailedTnxes.size() != 0) {
			bspFailedTnx = (BSPFailedTnx) bspFailedTnxes.get(0);
		}
		return bspFailedTnx;
	}

	public Integer getLastFileSequence(String countryCode, Date date) throws ModuleException {
		Integer lastFileSequence = null;
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd");
		Date minDate = null;
		try {
			minDate = fmt.parse(fmt.format(date));
		} catch (ParseException e) {
			throw new ModuleException("Invalid Date");
		}
		Date maxDate = new Date(minDate.getTime() + TimeUnit.DAYS.toMillis(1));
		String hql = "from BSPFileLog where countryCode = ? and createdDate between ? and ? ";
		List<BSPFileLog> fileLogs = find(hql,
				new Object[] { countryCode, minDate, maxDate }, BSPFileLog.class);
		if (fileLogs != null && fileLogs.size() > 0) {
			Collections.sort(fileLogs, new Comparator<BSPFileLog>() {
				public int compare(BSPFileLog o1, BSPFileLog o2) {
					return o2.getFileSequence().compareTo(o1.getFileSequence());
				}
			});
			lastFileSequence = fileLogs.get(0).getFileSequence();
		}
		return lastFileSequence;
	}

	public void saveOrUpdateBSPFileLog(BSPFileLog bspFileLog) {
		if (log.isDebugEnabled()) {
			log.debug("Saving/Updating bsp failed transaxction : " + bspFileLog.getFileSequence() + " "
					+ bspFileLog.getFileName());
		}
		hibernateSaveOrUpdate(bspFileLog);
	}
	
	public void saveOrUpdateBSPFileLogs(Collection<BSPFileLog> bspFileLogs) {

		if (log.isDebugEnabled()) {
			log.debug("Saving/Updating BSPFileLogs ");
		}
		hibernateSaveOrUpdateAll(bspFileLogs);
	}
	
	@SuppressWarnings("unchecked")
	public List<BSPFileLog> getUploadPendingRetFiles() {

		List<String> uploadEligibleStatuses = new ArrayList<String>();
		uploadEligibleStatuses.add(BSPFileLog.RET_FILE_STATUS.CREATED.toString());
		uploadEligibleStatuses.add(BSPFileLog.RET_FILE_STATUS.UPLOAD_FAILED.toString());

		Query query = getSession().createQuery("from BSPFileLog where ret_file_status in (:eligibleStatuses)")
				.setParameterList("eligibleStatuses", uploadEligibleStatuses);

		return query.list();
	}


	@Override
	public Page getBSPHOTFileRecords(int startrec, int numofrecs, List criteria) throws ModuleException {
		List ob = new ArrayList();
		ob.add("logID");
		return getPagedData(criteria, startrec, numofrecs, BSPHOTFileLog.class, ob);
	}
	
	@Override
	public BSPHOTFileLog getHOTFileByName(String HOTFileName) {
		String hql = "from BSPHOTFileLog hot where hot.fileName = ?";
		return find(hql, new Object[] { HOTFileName }, BSPHOTFileLog.class).get(0);
	}

	@Override
	public void saveOrUpdateBSPHOTFile(BSPHOTFileLog hotFile) {		
		if (log.isDebugEnabled()) {
			log.debug("Saving/Updating HOT File : " + hotFile.getFileName());
		}
		hibernateSaveOrUpdate(hotFile);		
	}

	@Override
	public void deleteBSPHOTFile(Integer hotFileLogId) {
		BSPHOTFileLog hotFile = (BSPHOTFileLog) get(BSPHOTFileLog.class, hotFileLogId);

		if (log.isDebugEnabled()) {
			log.debug("Deleting HOT File : " + hotFile.getFileName());
		}
		delete(hotFile);
	}


	@Override
	public List<DishFileTnxLogData> getMatchingTransactions(List<DPCProcessedBSPTnx> transactions) {

		StringBuffer sbHqlBuilder = new StringBuffer();

		int numOfTnx = transactions.size();
		sbHqlBuilder.append("from DishFileTnxLogData where (");
		for (int i = 0; i < numOfTnx; i++) {
			DPCProcessedBSPTnx tnx = transactions.get(i);

			if (TransactionType.EMDS.toString().equals(tnx.getTranactionType())) {
				sbHqlBuilder.append("(emds ='");
			} else if (TransactionType.RFND.toString().equals(tnx.getTranactionType())) {
				sbHqlBuilder.append("((emds ='");
				sbHqlBuilder.append(tnx.getPrimaryEticket());
				sbHqlBuilder.append("' or primaryETicket ='");
			} else {
				sbHqlBuilder.append("(primaryETicket ='");	
			}

			sbHqlBuilder.append(tnx.getPrimaryEticket());
			
			if (TransactionType.RFND.toString().equals(tnx.getTranactionType())) {
				sbHqlBuilder.append("') and tnxType = '");
			} else {
				sbHqlBuilder.append("' and tnxType = '");
			}
			
			sbHqlBuilder.append(tnx.getTranactionType());

			if (tnx.getSecondaryEticket() != null) {
				sbHqlBuilder.append("' and secondaryEticket = '");
				sbHqlBuilder.append(tnx.getSecondaryEticket());
			}
			sbHqlBuilder.append("') ");
			if (i != numOfTnx - 1) {
				sbHqlBuilder.append(" or ");
			}
		}
		sbHqlBuilder.append(" )");

		return find(sbHqlBuilder.toString(), DishFileTnxLogData.class);
	}

	@Override
	public void saveOrUpdateEMDSTransaction(EMDSTransaction emdsTransaction) {
		if (log.isDebugEnabled()) {
			log.debug("Saving/Updating EMDS Transacton: " + emdsTransaction.getEmdsTicketNo());
		}
		hibernateSaveOrUpdate(emdsTransaction);

	}

	@Override
	public EMDSTransaction getEMDSTransction(Integer tnxID, String lccUniqueID, Integer paxSequence, String extReference) {

		String hql = null;
		Object[] params = null;

		if (extReference != null) {
			hql = "from EMDSTransaction emds where emds.externalReference = ?";
			params = new Object[] { extReference };
		} else if (lccUniqueID != null) {
			hql = "from EMDSTransaction emds where emds.lccUniqueID = ? and emds.paxSequence = ?";
			params = new Object[] { lccUniqueID, paxSequence };
		} else if (tnxID != null) {
			hql = "from EMDSTransaction emds where emds.tnxID = ? and emds.paxSequence = ?";
			params = new Object[] { tnxID, paxSequence };
		}

		List<EMDSTransaction> tnxList = find(hql, params, EMDSTransaction.class);

		EMDSTransaction returnTnx = null;
		if (tnxList != null && tnxList.size() > 0) {
			returnTnx = (EMDSTransaction) tnxList.get(0);
		}

		return returnTnx;
	}

	@Override
	public void deleteBSPFileLog(String fileLogName) {
		List fileLogList = null;

		fileLogList = find(
				"from BSPFileLog where fileName = '" + fileLogName + "' order by createdDate desc", BSPFileLog.class);

		if (fileLogList != null && !fileLogList.isEmpty()) {
			BSPFileLog fileLog = (BSPFileLog) fileLogList.get(0);
			if (log.isDebugEnabled()) {
				log.debug("Deleting BSP File log : " + fileLog.getFileName());
			}
			delete(fileLog);
		}
	}	
	
	@Override
	public List<BSPWeblinkAccount> getBSPSftpAccounts () {
		StringBuffer buffer = new StringBuffer();
		
		buffer.append("from BSPWeblinkAccount");
		
		return find(buffer.toString(), BSPWeblinkAccount.class);
	}
}
