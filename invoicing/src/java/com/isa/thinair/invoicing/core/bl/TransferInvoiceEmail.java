/**
 * 
 */
package com.isa.thinair.invoicing.core.bl;

import java.io.File;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.model.Territory;
import com.isa.thinair.airreservation.api.utils.ReservationModuleUtils;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.service.AirTravelagentsModuleUtils;
import com.isa.thinair.airtravelagents.api.service.TravelAgentBD;
import com.isa.thinair.airtravelagents.api.util.BLUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.AccelAeroClients;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.FTPServerProperty;
import com.isa.thinair.commons.core.util.FTPUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.invoicing.api.dto.GeneratedInvoiceDTO;
import com.isa.thinair.invoicing.api.dto.InvoiceSummaryDTO;
import com.isa.thinair.invoicing.api.service.InvoicingModuleUtils;
import com.isa.thinair.invoicing.api.util.InvoicingInternalConstants;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;
import com.isa.thinair.platform.api.constants.PlatformConstants;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.reporting.api.service.DataExtractionBD;
import com.isa.thinair.reportingframework.api.service.ReportingFrameworkBD;

/**
 * @author isa11
 * 
 */
public class TransferInvoiceEmail {

	private static final Log log = LogFactory.getLog(TransferInvoiceEmail.class);
	MessagingServiceBD messagingServiceBD = null;

	public TransferInvoiceEmail() {
		messagingServiceBD = InvoicingModuleUtils.lookupMessagingBD();

	}

	/**
	 * Will create attachment and will email the envoice via mdb
	 * 
	 * @param generatedInvoiceDTO
	 * @return
	 * @throws ModuleException
	 */
	public boolean emailInvoice(GeneratedInvoiceDTO generatedInvoiceDTO) throws ModuleException {
		try {
			MessageProfile messageProfile = prepareMessageProfile(generatedInvoiceDTO);
			if (messageProfile != null && messagingServiceBD != null) {
				messagingServiceBD.sendMessage(messageProfile);
				return true;
			} else {
				return false;
			}
		} catch (Throwable e) {
			log.error(
					"Error When preparing message profile and messaging in emailInvoice(GeneratedInvoiceDTO generatedInvoiceDTO)",
					e);
			return false;
		}

	}

	/**
	 * Construct a Message Profile, and Uploads the Invoice SUmmary and Detail
	 * to FTP
	 * 
	 * @param generatedInvoiceDTO
	 * @return
	 * @throws ModuleException
	 */
	public MessageProfile prepareMessageProfile(GeneratedInvoiceDTO generatedInvoiceDTO) throws ModuleException {
		try {

			String agentName = generatedInvoiceDTO.getAgentName();
			String billingEmaillAddress = generatedInvoiceDTO.getBillingEmailAddress();

			String fileNameSummary = null;
			String summaryPath = null;

			String fileExtNameSummary = null;
			String extPath = null;

			if (billingEmaillAddress == null || billingEmaillAddress.equals("")) {
				return null;
			}
			String fileName = generatedInvoiceDTO.getAgentName();
			fileName = fileName + "_" + BLUtil.getFormattedDateForInvoicing() + ".pdf";
			String path = PlatformConstants.getAbsAttachmentsPath() + "/" + fileName;

			fileExtNameSummary = generatedInvoiceDTO.getAgentName() + "_Holidays";
			fileExtNameSummary = fileExtNameSummary + "_" + BLUtil.getFormattedDateForInvoicing() + ".pdf";
			extPath = PlatformConstants.getAbsAttachmentsPath() + "/" + fileExtNameSummary;

			if ((generatedInvoiceDTO.getSearchInvoicesDTO() != null)
					&& (generatedInvoiceDTO.getSearchInvoicesDTO().getYearTo() == 0)) {

				fileNameSummary = generatedInvoiceDTO.getAgentName() + "_Summary";
				fileNameSummary = fileNameSummary + "_" + BLUtil.getFormattedDateForInvoicing() + ".pdf";
				summaryPath = PlatformConstants.getAbsAttachmentsPath() + "/" + fileNameSummary;
			}

			HashMap<String, Object> map = new HashMap<String, Object>();
			Topic topic = new Topic();

			UserMessaging usermsg = new UserMessaging();

			List<UserMessaging> messageList = new ArrayList<UserMessaging>();
			usermsg.setToAddres(billingEmaillAddress);
			usermsg.setFirstName(agentName);
			usermsg.setLastName("");
			messageList.add(usermsg);

			MessageProfile msgProfile = new MessageProfile();
			msgProfile.setUserMessagings(messageList);

			String strCarrier = CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.CARRIER_NAME);

			map.put("user", usermsg);
			map.put("airline", strCarrier);

			if (!"".equals(
					CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.INVOICE_EMAIL_REPLY_ADDRESS))) {
				String invoiceEmails = getInvoiceEmailString(
						CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.INVOICE_EMAIL_REPLY_ADDRESS));

				map.put("notice",
						" ----------------------------------------------- <br> "
								+ " This E-mail is system generated. <br> "
								+ " Please do not reply to the sender's address <br> "
								+ " For any queries please reply to : <br>" + invoiceEmails + "<br> "
								+ " -----------------------------------------------<br>");
			}
			if (AppSysParamsUtil.isTransactInvoiceEnable()) {
				prepareReportsAttachmentForTransaction(generatedInvoiceDTO, fileName, fileNameSummary,
						fileExtNameSummary);
			} else if ((generatedInvoiceDTO.getSearchInvoicesDTO() != null)
					&& (generatedInvoiceDTO.getSearchInvoicesDTO().getYearTo() == 0)) {
				// upload invoice detail and invoice summary to ftp
				prepareReportsAttachment(generatedInvoiceDTO, fileName, fileNameSummary, fileExtNameSummary);// harsha
																												// summ

			} else if ((generatedInvoiceDTO.getSearchInvoicesDTO() != null)
					&& (generatedInvoiceDTO.getSearchInvoicesDTO().getYearTo() != 0)) {
				prepareIndividualReportsAttachment(generatedInvoiceDTO, fileName);// det
			}

			File attachment = new File(path);
			if (attachment.exists()) {
				topic.addAttachmentFileName(fileName);
			}

			File extAttachment = new File(extPath);
			if (extAttachment.exists()) {
				topic.addAttachmentFileName(fileExtNameSummary);
			}

			if ((generatedInvoiceDTO.getSearchInvoicesDTO() != null)
					&& (generatedInvoiceDTO.getSearchInvoicesDTO().getYearTo() == 0)) {

				File attachmentSummary = new File(summaryPath);
				if (attachmentSummary.exists()) {
					topic.addAttachmentFileName(fileNameSummary);
				}
			}

			topic.setTopicParams(map);
			if ((generatedInvoiceDTO.getSearchInvoicesDTO() != null)
					&& (generatedInvoiceDTO.getSearchInvoicesDTO().getYearTo() == 0)) {
				topic.setTopicName("invoice");
			} else if ((generatedInvoiceDTO.getSearchInvoicesDTO() != null)
					&& (generatedInvoiceDTO.getSearchInvoicesDTO().getYearTo() != 0)) {
				topic.setTopicName("outstanding_balance_rpt_email");
			}

			msgProfile.setTopic(topic);

			return msgProfile;

		} catch (Exception e) {
			log.error("############## Error In Preparing Msg Profile #######", e);

			return null;
		}

	}

	/**
	 * get invoice reply email Address
	 * 
	 * @param replyEmailAddrs
	 * @return
	 */
	private String getInvoiceEmailString(String replyEmailAddrs) {

		String[] sArr = replyEmailAddrs.split(",");
		String str = "";
		if (sArr != null && sArr.length > 1) {
			str = sArr[0] + " or " + sArr[1];
		} else {
			str = sArr[0];
		}

		return str;
	}

	/**
	 * Uploading the PDF, and Details to FTP
	 * 
	 * @param generatedInvoiceDTO
	 */
	private void prepareIndividualReportsAttachment(GeneratedInvoiceDTO generatedInvoiceDTO, String fileName)
			throws ModuleException {

		ReportingFrameworkBD reportingFrameworkBD = InvoicingModuleUtils.lookupReportingFrameworkBD();

		DataExtractionBD dataExtractionBD = (DataExtractionBD) InvoicingModuleUtils.lookupDataExtractionBD();

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

		ResultSet resultSet = null;

		String agentCode = generatedInvoiceDTO.getAgentCode();
		Collection<String> agentcol = new ArrayList<String>();

		GlobalConfig globalConfig = CommonsServices.getGlobalConfig();

		String id = " SC_FACM_007";
		String reportTemplate = null;
		String strLogo = AppSysParamsUtil.getReportLogo(false);// globalConfig.getBizParam(SystemParamKeys.REPORT_LOGO);
		String image = "../../images/" + AppSysParamsUtil.getReportLogo(true);
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation 				System";
		String strBase = globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY);
		String strAgentCurr = generatedInvoiceDTO.getCurrencyCode();
		String strEntity = generatedInvoiceDTO.getEntityName();
		String entityCode = generatedInvoiceDTO.getEntityCode();

		Date periodStartDate = BLUtil.getFromDate(generatedInvoiceDTO.getSearchInvoicesDTO());
		Date periodEndDate = BLUtil.getPeriodToDate(generatedInvoiceDTO.getSearchInvoicesDTO());

		String fromDate = dateFormat.format(periodStartDate);
		String toDate = dateFormat.format(periodEndDate);

		try {

			ReportsSearchCriteria search = new ReportsSearchCriteria();

			if (agentCode != null && !agentCode.equals("")) {
				agentcol.add(agentCode);
				search.setAgents(agentcol);
			}

			if (generatedInvoiceDTO.getSearchInvoicesDTO().isFilterZeroInvoices()) {
				search.setExcludeZeroBalance(true);
			}

			search.setDateRangeFrom(BLUtil.convertDate(fromDate));
			search.setDateRangeTo(BLUtil.convertDate(toDate));
			search.setEntity(entityCode);

			reportTemplate = "EmailOutstandingBalanceDetail.jasper";

			reportingFrameworkBD.storeJasperTemplateRefs("WebReport1", BLUtil.getReportTemplate(reportTemplate));

			resultSet = dataExtractionBD.getEmailOutstandingBalanceDetailData(search);

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("FROM_DATE", fromDate);
			parameters.put("TO_DATE", toDate);
			parameters.put("ID", id);
			parameters.put("AMT_1", "(" + strBase + ")");
			parameters.put("AMT_2", strAgentCurr);
			parameters.put("CARRIER", strCarrier);
			parameters.put("INVOICE_ENTITY", strEntity);

			image = BLUtil.getReportTemplate(strLogo);
			parameters.put("IMG", image);
			byte[] byteStream = reportingFrameworkBD.createPDFReport("WebReport1", parameters, resultSet);

			FTPServerProperty serverProperty = new FTPServerProperty();
			FTPUtil ftpUtil = new FTPUtil();

			ftpUtil.setPassiveMode(globalConfig.isFtpEnablePassiveMode());
			// Code to FTP
			serverProperty.setServerName(globalConfig.getFtpServerName());
			if (globalConfig.getFtpServerUserName() == null) {
				serverProperty.setUserName("");
			} else {
				serverProperty.setUserName(globalConfig.getFtpServerUserName());
			}
			serverProperty.setUserName(globalConfig.getFtpServerUserName());
			if (globalConfig.getFtpServerPassword() == null) {
				serverProperty.setPassword("");
			} else {
				serverProperty.setPassword(globalConfig.getFtpServerPassword());
			}
			ftpUtil.uploadAttachment(fileName, byteStream, serverProperty);

		} catch (Exception e) {
			log.error("Error in creating invoice summary and detail report", e);
			throw new ModuleException(e, "Error in creating invoice summary and detail report" + e.getMessage());
		}

	}

	/**
	 * Uploading the PDF, and Details to FTP
	 * 
	 * @param generatedInvoiceDTO
	 */
	private void prepareReportsAttachment(GeneratedInvoiceDTO generatedInvoiceDTO, String fileName,
			String fileNameSummary, String fileNameExternal) throws ModuleException {

		Date firstday = null;
		Date lastday = null;
		GlobalConfig globalConfig = CommonsServices.getGlobalConfig();
		String invoiceNo = generatedInvoiceDTO.getInvoiceNumber();
		String year;
		String month;
		String billingPeriod;
		String strEntity;
		String entityCode;

		if ("DEFAULT".equalsIgnoreCase(globalConfig.getBizParam(SystemParamKeys.INVOICE_NO_FORMAT))) {

			year = 20 + invoiceNo.substring(0, 2);
			month = invoiceNo.substring(2, 4);
			billingPeriod = invoiceNo.substring(5, 6);
		} else {
			Date invDate = generatedInvoiceDTO.getInvoiceDate();// LOGIC
																// :INvoice
																// should
																// generated
																// during the
			// period(i.e same Month)
			year = Integer.toString(CalendarUtil.getCalendar(invDate).get(Calendar.YEAR));
			month = (Integer.toString(CalendarUtil.getCalendar(invDate).get(Calendar.MONTH) + 1));
			billingPeriod = Integer.toString(generatedInvoiceDTO.getBillingPeriod());
		}

		String value = InvoicingInternalConstants.InvoiceAttachment.OPTION_VALUE;
		String agentCode = generatedInvoiceDTO.getAgentCode();
		String agentName = generatedInvoiceDTO.getAgentName();
		String agentCurrencyCode = generatedInvoiceDTO.getCurrencyCode();
		String id = InvoicingInternalConstants.InvoiceAttachment.ID;
		String prefReportFmt = generatedInvoiceDTO.getPrefReportFmt();
		String addStatement = AppSysParamsUtil.getAdditionalSentenceOnInvoice();
		strEntity = generatedInvoiceDTO.getEntityName();
		entityCode = generatedInvoiceDTO.getEntityCode();

		// Load territory for agent to update bank details
		TravelAgentBD agentBD = InvoicingModuleUtils.lookupTravelAgentBD();
		Agent agent = agentBD.getAgent(agentCode);
		Territory territory = InvoicingModuleUtils.getAirmasterBD().getTerritory(agent.getTerritoryCode());

		String paymentTerms = globalConfig.getBizParam(SystemParamKeys.PAYMENT_TERMS);
		String accountNumber = getFirstValueIfNotNullAndNotEmpty(territory.getAccountNumber(),
				AppSysParamsUtil.getSettlementBankAccountNumber());

		String bankName = getFirstValueIfNotNullAndNotEmpty(territory.getBank(),
				AppSysParamsUtil.getSettlementBankName());
		String bankAddress = getFirstValueIfNotNullAndNotEmpty(territory.getBankAddress(),
				AppSysParamsUtil.getSettlementBankAddress());
		String baseCurrency = globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY);
		String swiftCode = getFirstValueIfNotNullAndNotEmpty(territory.getSwiftCode(),
				AppSysParamsUtil.getSettlementBankSwiftCode());
		String IBANNo = getFirstValueIfNotNullAndNotEmpty(territory.getIBANCode(),
				AppSysParamsUtil.getSettlementBankIBANNo());
		String beneficiaryName = getFirstValueIfNotNullAndNotEmpty(territory.getBeneficiaryName(),
				AppSysParamsUtil.getSettlementBankBeneficiaryName());

		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String strPayCurr = "N";
		boolean blnPayCurr = false;

		if ("Y".equalsIgnoreCase(globalConfig.getBizParam(SystemParamKeys.STORE_PAYMENTS_IN_AGENTS_CURRENCY))
				&& "Y".equalsIgnoreCase(globalConfig.getBizParam(SystemParamKeys.INVOICE_IN_AGENTS_CURRENCY))) {
			strPayCurr = "Y";
			blnPayCurr = true;
		}

		/**
		 * Urgent Requirement for Mihin Lanka: Quick Fix
		 * 
		 * @todo: Please Refactor Hard Coded Stuff
		 */
		if (AccelAeroClients.MIHIN_LANKA.equals(globalConfig.getBizParam(SystemParamKeys.ACCELAERO_CLIENT_IDENTIFIER))
				&& generatedInvoiceDTO.getReportToGSA().equals("Y")) {

			paymentTerms = "Please contact your GSA for payment terms";
			accountNumber = "";
			bankName = "";
			bankAddress = "";
			baseCurrency = "";
			swiftCode = "";

		}

		String period = "";
		try {
			Map<String, Object> parametersForInvoiceDetails = new HashMap<String, Object>();
			Map<String, Object> parametersForInvoiceSummary = new HashMap<String, Object>();

			ReportsSearchCriteria search = new ReportsSearchCriteria();
			ReportsSearchCriteria searchSummary = new ReportsSearchCriteria();

			// ReportsHTMLGenerator.getReportTemplate(reportTemplateDetails));
			search.setAgentCode(agentCode);
			search.setYear(year);
			search.setMonth(month);
			searchSummary.setYear(year);
			searchSummary.setMonth(month);
			search.setPaymentSource("INTERNAL");
			search.setReportType(ReportsSearchCriteria.AGENT_INVOICE_DETAILS);
			search.setEntity(entityCode);

			// TO Display External Holoday Data
			search.setShowExternal("Y");

			/** AARESAA:3128 - Lalanthi */
			if (globalConfig.getPetroFacAgentsMap().containsKey(agentCode)) {
				parametersForInvoiceDetails.put("SHOW_PAYREF", "Y");
				parametersForInvoiceDetails.put("PAYREF", "Payment Ref");
			} else {
				parametersForInvoiceDetails.put("SHOW_PAYREF", "N");
				// parametersForInvoiceDetails.put("PAYREF", "");
			}

			if (billingPeriod != null && !billingPeriod.equals("")) {
				search.setBillingPeriod(Integer.parseInt(billingPeriod));
				searchSummary.setBillingPeriod(Integer.parseInt(billingPeriod));

				period = year + "/" + month + " Period ";
				if (billingPeriod.equals("1")) {
					period += "1";
				} else {
					period += "2";
				}
			}
			GregorianCalendar gc = new GregorianCalendar();

			gc.set(Calendar.YEAR, Integer.parseInt(year));
			gc.set(Calendar.MONTH, (Integer.parseInt(month) - 1));
			if (billingPeriod.equals("1")) {
				gc.set(Calendar.DATE, 1);
				firstday = gc.getTime();
				gc.set(Calendar.DATE, 15);
				lastday = gc.getTime();
			} else {
				gc.set(Calendar.DATE, 16);
				firstday = gc.getTime();
				gc.set(Calendar.DATE, gc.getActualMaximum(Calendar.DAY_OF_MONTH));
				lastday = gc.getTime();
			}

			SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");

			searchSummary.setAgentCode(agentCode);
			searchSummary.setDateRangeFrom(formatter.format(firstday));
			searchSummary.setDateRangeTo(formatter.format(lastday));
			searchSummary.setEntity(entityCode);

			String reportsLogoImage = PlatformConstants.getConfigRootAbsPath() + "/templates/reports/"
					+ AppSysParamsUtil.getReportLogo(false);

			parametersForInvoiceDetails.put("INVOICE_PERIOD", period);
			parametersForInvoiceDetails.put("OPTION", value);
			parametersForInvoiceDetails.put("INVOICE_ENTITY", strEntity);

			if (blnPayCurr) {
				parametersForInvoiceDetails.put("AMT_2", "(" + agentCurrencyCode + ")");
				// parametersForInvoiceSummary.put("AMT_2","("+agentCurrencyCode+")");
			} else {
				parametersForInvoiceDetails.put("AMT_1", "(" + baseCurrency + ")");
				// parametersForInvoiceSummary.put("AMT_1","("+baseCurrency+")");
			}

			// parametersForInvoiceDetails.put("AMT_2","("+agentCurrencyCode+")");
			parametersForInvoiceSummary.put("AMT_2", "(" + agentCurrencyCode + ")");
			// parametersForInvoiceDetails.put("AMT_1","("+baseCurrency+")");
			parametersForInvoiceSummary.put("AMT_1", "(" + baseCurrency + ")");

			String sameBaseCur = "N";
			if (baseCurrency != null && agentCurrencyCode != null) {
				if (baseCurrency.equalsIgnoreCase(agentCurrencyCode)) {
					sameBaseCur = "Y";
				}
			}

			String showPaymentBreakDown = globalConfig.getBizParam(SystemParamKeys.SHOW_BREAKDOWN);
			String showPaymentCurrWithBreakDown = "N";
			if (strPayCurr.equalsIgnoreCase("Y") && showPaymentBreakDown.equalsIgnoreCase("Y")) {
				showPaymentCurrWithBreakDown = "Y";
				showPaymentBreakDown = "N";
			}

			if (value.trim().equals("PDF")) {
				parametersForInvoiceDetails.put("IMG", reportsLogoImage);

				FTPServerProperty serverProperty = new FTPServerProperty();

				// Code to FTP
				String ftpServerName = CommonsServices.getGlobalConfig().getFtpServerName();
				String ftpServerUserName = CommonsServices.getGlobalConfig().getFtpServerUserName();
				String ftpServerPassword = CommonsServices.getGlobalConfig().getFtpServerPassword();

				serverProperty.setServerName(ftpServerName);
				serverProperty.setUserName(ftpServerUserName);
				serverProperty.setPassword(ftpServerPassword);

				parametersForInvoiceDetails.put("AGENT_CODE", agentCode);
				parametersForInvoiceDetails.put("AGENT_NAME", agentName);
				parametersForInvoiceDetails.put("INVOICE_NO", invoiceNo);
				parametersForInvoiceDetails.put("ID", id);
				parametersForInvoiceDetails.put("AMT_2", "(" + agentCurrencyCode + ")");
				parametersForInvoiceDetails.put("CARRIER", strCarrier);
				parametersForInvoiceDetails.put("SHOW_BREAKDOWN", showPaymentBreakDown);
				parametersForInvoiceDetails.put("LOCALE", prefReportFmt);
				parametersForInvoiceDetails.put("SAME_PAYMENT_CUR", sameBaseCur);
				parametersForInvoiceDetails.put("SHOW_PAY_CUR", strPayCurr);
				parametersForInvoiceDetails.put("SHOW_PAYCUR_BREKDWN", showPaymentCurrWithBreakDown);
				parametersForInvoiceDetails.put("SHOW_EXTERNAL", "Y");
				parametersForInvoiceDetails.put("BASE_CURRENCY", baseCurrency);

				uploadDeatilFile(serverProperty, fileName, blnPayCurr, parametersForInvoiceDetails, search,
						generatedInvoiceDTO.isInlocalCurrency());
				// End Code to FTP

				parametersForInvoiceDetails.put("IMG", reportsLogoImage);
				// String fileNameSummary = (String)
				// request.getAttribute("fileNameSummary");
				// String pathSummary =
				// PlatformConstants.getAbsAttachmentsPath()+"/"+fileNameSummary;

				parametersForInvoiceSummary.put("AGENT_CODE", agentCode);
				parametersForInvoiceSummary.put("PAYMENT_TERM", paymentTerms);
				parametersForInvoiceSummary.put("ACCOUNT_NO", accountNumber);
				parametersForInvoiceSummary.put("BANK_NAME", bankName);
				parametersForInvoiceSummary.put("BANK_ADDRESS", bankAddress);
				parametersForInvoiceSummary.put("SWIFT_CODE", swiftCode);
				parametersForInvoiceSummary.put("IMG", reportsLogoImage);
				parametersForInvoiceSummary.put("AMT_2", "(" + agentCurrencyCode + ")");
				parametersForInvoiceSummary.put("CARRIER", strCarrier);
				parametersForInvoiceSummary.put("LOCALE", prefReportFmt);
				parametersForInvoiceSummary.put("BENEFICIARY_NAME", beneficiaryName);
				parametersForInvoiceSummary.put("IBAN_NO", IBANNo);
				parametersForInvoiceSummary.put("INCLUDE_SENTENCE", addStatement);
				parametersForInvoiceSummary.put("SAME_PAYMENT_CUR", sameBaseCur);

				parametersForInvoiceSummary.put("SHOW_PAY_CUR", strPayCurr);
				parametersForInvoiceSummary.put("SHOW_PAYCUR_BREKDWN", showPaymentCurrWithBreakDown);
				parametersForInvoiceSummary.put("SHOW_EXTERNAL", "Y");
				parametersForInvoiceSummary.put("INVOICE_ENTITY", strEntity);

				uploadSummaryFile(serverProperty, fileNameSummary, blnPayCurr, parametersForInvoiceSummary,
						searchSummary, generatedInvoiceDTO.isInlocalCurrency());

				// End Code to FTP
			}
		} catch (Exception e) {
			log.error("Error in creating invoice summary and detail report", e);
			throw new ModuleException(e, "Error in creating invoice summary and detail report" + e.getMessage());
		}

	}

	/**
	 * This method will upload the invoice summary File
	 * 
	 * @param serverProperty
	 * @param fileNameSummary
	 * @param blnPayCurr
	 * @param parametersForInvoiceSummary
	 * @param searchSummary
	 */
	private void uploadSummaryFile(FTPServerProperty serverProperty, String fileNameSummary, boolean blnPayCurr,
			Map<String, Object> parametersForInvoiceSummary, ReportsSearchCriteria searchSummary, boolean inLocalCurr)
			throws ModuleException {

		FTPUtil ftpUtil = new FTPUtil();
		ReportingFrameworkBD reportingFrameworkBD = InvoicingModuleUtils.lookupReportingFrameworkBD();
		DataExtractionBD dataExtractionBD = (DataExtractionBD) InvoicingModuleUtils.lookupDataExtractionBD();
		ResultSet resultSetSummary = null;

		if (AppSysParamsUtil.isTransactInvoiceEnable()) {
			resultSetSummary = dataExtractionBD.getInvoiceSummaryAttachmentByTransaction(searchSummary);
		} else if (blnPayCurr) {
			resultSetSummary = dataExtractionBD.getInvoiceSummaryAttachmentByCurrency(searchSummary);
		} else {
			resultSetSummary = dataExtractionBD.getInvoiceSummaryAttachment(searchSummary);
		}

		if (AppSysParamsUtil.isTransactInvoiceEnable()) {
			reportingFrameworkBD.storeJasperTemplateRefs("WebReport1", BLUtil.getReportTemplate(
					InvoicingInternalConstants.InvoiceAttachment.INVOICE_SUMMARY_BY_TRANSACTION_JASPER_TEMPLATE));
		} else if (blnPayCurr) {
			reportingFrameworkBD.storeJasperTemplateRefs("WebReport1", BLUtil.getReportTemplate(
					InvoicingInternalConstants.InvoiceAttachment.INVOICE_SUMMARY_BY_CURRENCY_JASPER_TEMPLATE));
			if (!inLocalCurr) { // generatedInvoiceDTO.isInlocalCurrency()
				reportingFrameworkBD.storeJasperTemplateRefs("WebReport1", BLUtil.getInvoiceSummarryReportTemplate());
			}
		} else {
			reportingFrameworkBD.storeJasperTemplateRefs("WebReport1", BLUtil.getReportTemplate(
					InvoicingInternalConstants.InvoiceAttachment.INVOICE_SUMMARY_JASPER_BASIC_TEMPLATE));

		}

		String includeCommission = AirTravelagentsModuleUtils.getGlobalConfig()
				.getBizParam(SystemParamKeys.AGENT_COMMISSION);

		if (!AppSysParamsUtil.isTransactInvoiceEnable()) {
			if ("Y".equals(includeCommission)) {
				// commissionCodeValueMap = getAgentCommissionCodeValueMap();
				Double commission_amount = null;
				Double totalFare = null;

				Double rate = new Double(0);

				try {
					while (resultSetSummary.next()) {
						// by default in db is zero
						totalFare = resultSetSummary.getDouble("total_fare_amount");
						commission_amount = resultSetSummary.getDouble("commission_amount");
						if (totalFare != null && commission_amount != null && totalFare.doubleValue() > 0) {
							rate = (commission_amount * 100) / totalFare;
						}
						// expecting only one value;
						resultSetSummary.beforeFirst();
						break;
					}
				} catch (SQLException e) {
					log.error("Error in creating invoice summary and detail report", e);
					throw new ModuleException(e,
							"Error in creating invoice summary and detail report" + e.getMessage());
				}

				parametersForInvoiceSummary.put("TOTAL_FARE_DES", "Total Fare Collection");
				parametersForInvoiceSummary.put("TOTAL_FARE", totalFare);
				if (totalFare.doubleValue() > 0) {
					parametersForInvoiceSummary.put("COM_AMT", commission_amount);
					parametersForInvoiceSummary.put("COMMISION", "Agent Commission applied " + rate.toString() + "%");
				}

			}
		}
		byte[] byteStreamSummary = reportingFrameworkBD.createPDFReport("WebReport1", parametersForInvoiceSummary,
				resultSetSummary);

		log.info("PDF file generated : " + fileNameSummary + "File size: " + byteStreamSummary.length);

		if (byteStreamSummary.length == 0) {
			log.info("A file with zero size generated for below paramerters");
			for (Map.Entry<String, Object> entry : parametersForInvoiceSummary.entrySet()) {
				String key = null;
				String value = null;

				key = entry.getKey();
				if (entry.getValue() != null) {
					value = entry.getValue().toString();
				} else {
					value = "null";
				}
				log.info(key + ":" + value);
			}
			if (resultSetSummary != null) {
				log.info("Result set is not empty");
			} else {
				log.info("Result set is null");
			}
		}

		// Code to FTP
		ftpUtil.setPassiveMode(CommonsServices.getGlobalConfig().isFtpEnablePassiveMode());
		ftpUtil.uploadAttachment(fileNameSummary, byteStreamSummary, serverProperty);
	}

	/**
	 * This method will upload the invoice Details File
	 * 
	 * @param serverProperty
	 * @param fileNameSummary
	 * @param blnPayCurr
	 * @param parametersForInvoiceSummary
	 * @param searchSummary
	 */
	private void uploadDeatilFile(FTPServerProperty serverProperty, String fileName, boolean blnPayCurr,
			Map<String, Object> parametersForInvoiceDetails, ReportsSearchCriteria search, boolean inLocalCurr)
			throws ModuleException {

		FTPUtil ftpUtil = new FTPUtil();
		ReportingFrameworkBD reportingFrameworkBD = InvoicingModuleUtils.lookupReportingFrameworkBD();
		DataExtractionBD dataExtractionBD = (DataExtractionBD) InvoicingModuleUtils.lookupDataExtractionBD();
		ResultSet resultSetForDetails = null;

		if (blnPayCurr) {
			reportingFrameworkBD.storeJasperTemplateRefs("WebReport3",
					BLUtil.getReportTemplate("InvoiceDetailReportTemplat.jasper"));

			if (!inLocalCurr) {
				reportingFrameworkBD.storeJasperTemplateRefs("WebReport3", BLUtil.getInvoiceReportTemplate());
			}
		} else {
			reportingFrameworkBD.storeJasperTemplateRefs("WebReport3", BLUtil.getReportTemplate(
					InvoicingInternalConstants.InvoiceAttachment.INVOICE_DETAIL_BASIC_JASPER_TEMPLATE));

		}

		if (blnPayCurr) {
			resultSetForDetails = dataExtractionBD.getInvoiceSummaryDataByCurrency(search);
		} else {
			resultSetForDetails = dataExtractionBD.getInvoiceSummaryData(search);
		}

		byte[] byteStreamWithExternal = reportingFrameworkBD.createPDFReport("WebReport3", parametersForInvoiceDetails,
				resultSetForDetails);

		log.info("PDF file generated : " + fileName + "File size: " + byteStreamWithExternal.length);

		if (byteStreamWithExternal.length == 0) {
			log.info("A file with zero size generated for below paramerters");
			for (Map.Entry<String, Object> entry : parametersForInvoiceDetails.entrySet()) {
				String key = null;
				String value = null;

				key = entry.getKey();
				if (entry.getValue() != null) {
					value = entry.getValue().toString();
				} else {
					value = "null";
				}
				log.info(key + ":" + value);
			}
			if (resultSetForDetails != null) {
				log.info("Result set is not empty");
			} else {
				log.info("Result set is null");
			}
		}

		ftpUtil.setPassiveMode(CommonsServices.getGlobalConfig().isFtpEnablePassiveMode());
		ftpUtil.uploadAttachment(fileName, byteStreamWithExternal, serverProperty);

	}

	private void prepareReportsAttachmentForTransaction(GeneratedInvoiceDTO generatedInvoiceDTO, String fileName,
			String fileNameSummary, String fileExtNameSummary) throws ModuleException {
		Date dateFrom = null;
		Date dateTo = null;
		Date invoiceDate = null;
		String strFrmDate = null;
		String strToDate = null;
		String strInvoiceDate = null;
		GlobalConfig globalConfig = CommonsServices.getGlobalConfig();
		String invoiceNo = generatedInvoiceDTO.getInvoiceNumber();
		String year;
		String month;
		String billingPeriod;

		if ("DEFAULT".equalsIgnoreCase(globalConfig.getBizParam(SystemParamKeys.INVOICE_NO_FORMAT))) {

			year = 20 + invoiceNo.substring(0, 2);
			month = invoiceNo.substring(2, 4);
			billingPeriod = invoiceNo.substring(5, 6);
		} else {
			Date invDate = generatedInvoiceDTO.getInvoiceDate();// LOGIC
																// :INvoice
																// should
																// generated
																// during the
			// period(i.e same Month)
			year = Integer.toString(CalendarUtil.getCalendar(invDate).get(Calendar.YEAR));
			month = (Integer.toString(CalendarUtil.getCalendar(invDate).get(Calendar.MONTH) + 1));
			billingPeriod = Integer.toString(generatedInvoiceDTO.getBillingPeriod());
		}

		NumberFormat numFormat = NumberFormat.getInstance();
		numFormat.setMinimumFractionDigits(2);

		String value = InvoicingInternalConstants.InvoiceAttachment.OPTION_VALUE;
		String agentCode = generatedInvoiceDTO.getAgentCode();
		String agentName = generatedInvoiceDTO.getAgentName();
		String agentCurrencyCode = generatedInvoiceDTO.getCurrencyCode();
		String id = InvoicingInternalConstants.InvoiceAttachment.ID;
		String pnr = generatedInvoiceDTO.getPnr().toString();
		String addStatement = AppSysParamsUtil.getAdditionalSentenceOnInvoice();

		// Load territory for agent to update bank details
		TravelAgentBD agentBD = InvoicingModuleUtils.lookupTravelAgentBD();
		Agent agent = agentBD.getAgent(agentCode);
		Territory territory = InvoicingModuleUtils.getAirmasterBD().getTerritory(agent.getTerritoryCode());

		String paymentTerms = globalConfig.getBizParam(SystemParamKeys.PAYMENT_TERMS);
		String accountNumber = getFirstValueIfNotNullAndNotEmpty(territory.getAccountNumber(),
				AppSysParamsUtil.getSettlementBankAccountNumber());
		String bankName = getFirstValueIfNotNullAndNotEmpty(territory.getBank(),
				AppSysParamsUtil.getSettlementBankName());
		String bankAddress = getFirstValueIfNotNullAndNotEmpty(territory.getBankAddress(),
				AppSysParamsUtil.getSettlementBankAddress());
		String baseCurrency = globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY);
		String swiftCode = getFirstValueIfNotNullAndNotEmpty(territory.getSwiftCode(),
				AppSysParamsUtil.getSettlementBankSwiftCode());
		String IBANNo = getFirstValueIfNotNullAndNotEmpty(territory.getIBANCode(),
				AppSysParamsUtil.getSettlementBankIBANNo());
		String beneficiaryName = getFirstValueIfNotNullAndNotEmpty(territory.getBeneficiaryName(),
				AppSysParamsUtil.getSettlementBankBeneficiaryName());

		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		boolean blnPayCurr = false;

		// if("Y".equalsIgnoreCase(globalConfig.getBizParam(SystemParamKeys.STORE_PAYMENTS_IN_AGENTS_CURRENCY))
		// &&
		// "Y".equalsIgnoreCase(globalConfig.getBizParam(SystemParamKeys.INVOICE_IN_AGENTS_CURRENCY))
		// && getRecordStatus(year, month, billingPeriod)){
		// blnPayCurr = true;
		// }
		ReportsSearchCriteria searchTmpSummary = new ReportsSearchCriteria();
		ResultSet resultSetSummary = null;
		searchTmpSummary.setAgentCode(agentCode);
		searchTmpSummary.setInvoiceNumber(invoiceNo);
		DataExtractionBD dataExtractionBD = (DataExtractionBD) InvoicingModuleUtils.lookupDataExtractionBD();
		resultSetSummary = dataExtractionBD.getInvoiceSummaryAttachmentByTransaction(searchTmpSummary);

		boolean paymentBreakdown = false;
		if ("Y".equals(CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.SHOW_BREAKDOWN))) {
			paymentBreakdown = true;
		}

		BigDecimal invFare = BigDecimal.ZERO;
		BigDecimal invTax = BigDecimal.ZERO;
		BigDecimal invModify = BigDecimal.ZERO;
		BigDecimal invSurCharge = BigDecimal.ZERO;
		String strBreakdown = "N";
		if (paymentBreakdown) {
			strBreakdown = "Y";
			InvoiceSummaryDTO invoiceSummaryDTO = null;
			invoiceSummaryDTO = (InvoiceSummaryDTO) ReservationModuleUtils.getInvoicingBD().searchInvoiceForAgent(//TODO pass entity
					agentCode, "", "", 1, paymentBreakdown, invoiceNo, pnr.length() == 0 ? null : pnr,null);// no
																										// need
																										// to
																										// send
																										// year,month
																										// &
																										// billing
																										// period

			if (invoiceSummaryDTO != null) {
				invFare = (invoiceSummaryDTO.getFareAmount() == null) ? BigDecimal.ZERO
						: invoiceSummaryDTO.getFareAmount();
				invTax = (invoiceSummaryDTO.getTaxAmount() == null) ? BigDecimal.ZERO
						: invoiceSummaryDTO.getTaxAmount();
				invModify = (invoiceSummaryDTO.getModifyAmount() == null) ? BigDecimal.ZERO
						: invoiceSummaryDTO.getModifyAmount();
				invSurCharge = (invoiceSummaryDTO.getSurchrageAmount() == null) ? BigDecimal.ZERO
						: invoiceSummaryDTO.getSurchrageAmount();
			}
		}

		Map<String, Object> parametersForInvoiceSummary = new HashMap<String, Object>();
		ReportsSearchCriteria searchSummary = new ReportsSearchCriteria();

		searchSummary.setYear(year);
		searchSummary.setMonth(month);

		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		searchSummary.setAgentCode(agentCode);
		invoiceDate = generatedInvoiceDTO.getInvoiceDate();

		if (invoiceDate != null) {
			strInvoiceDate = formatter.format(invoiceDate);
			searchSummary.setDateRangeFrom(strInvoiceDate);
		}

		String reportsLogoImage = PlatformConstants.getConfigRootAbsPath() + "/templates/reports/"
				+ AppSysParamsUtil.getReportLogo(false);

		if (blnPayCurr) {
			parametersForInvoiceSummary.put("AMT_1", "(" + agentCurrencyCode + ")");
		} else {
			parametersForInvoiceSummary.put("AMT_1", "(" + baseCurrency + ")");
		}

		if (value.trim().equals("PDF")) {
			FTPServerProperty serverProperty = new FTPServerProperty();

			// Code to FTP
			String ftpServerName = CommonsServices.getGlobalConfig().getFtpServerName();
			String ftpServerUserName = CommonsServices.getGlobalConfig().getFtpServerUserName();
			String ftpServerPassword = CommonsServices.getGlobalConfig().getFtpServerPassword();

			serverProperty.setServerName(ftpServerName);
			serverProperty.setUserName(ftpServerUserName);
			serverProperty.setPassword(ftpServerPassword);
			parametersForInvoiceSummary.put("AGENT_CODE", agentCode);
			parametersForInvoiceSummary.put("PAYMENT_TERM", paymentTerms);
			parametersForInvoiceSummary.put("ACCOUNT_NO", accountNumber);
			parametersForInvoiceSummary.put("BANK_NAME", bankName);
			parametersForInvoiceSummary.put("BANK_ADDRESS", bankAddress);
			parametersForInvoiceSummary.put("SWIFT_CODE", swiftCode);
			parametersForInvoiceSummary.put("IMG", reportsLogoImage);
			parametersForInvoiceSummary.put("AMT_1", "(" + agentCurrencyCode + ")");
			parametersForInvoiceSummary.put("CARRIER", strCarrier);
			parametersForInvoiceSummary.put("SHOW_BREAKDOWN", strBreakdown);
			parametersForInvoiceSummary.put("INV_FARE", numFormat.format(invFare).toString());
			parametersForInvoiceSummary.put("INV_TAX", numFormat.format(invTax).toString());
			parametersForInvoiceSummary.put("INV_MOD", numFormat.format(invModify).toString());
			parametersForInvoiceSummary.put("INV_SURCHARGE", numFormat.format(invSurCharge).toString());
			parametersForInvoiceSummary.put("BENEFICIARY_NAME", beneficiaryName);
			parametersForInvoiceSummary.put("IBAN_NO", IBANNo);
			parametersForInvoiceSummary.put("INCLUDE_SENTENCE", addStatement);

			uploadSummaryFile(serverProperty, fileNameSummary, blnPayCurr, parametersForInvoiceSummary, searchSummary,
					generatedInvoiceDTO.isInlocalCurrency());
		}
	}

	private String getFirstValueIfNotNullAndNotEmpty(String first, String second) {
		if (first == null || first.isEmpty()) {
			return second;
		}
		return first;
	}
}
