/**
 * 
 */
package com.isa.thinair.invoicing.core.bl.bsp;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.util.CurrencyProxy;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.invoicing.api.model.BSPHOTFileLog;
import com.isa.thinair.invoicing.api.model.DishFileTnxLogData;
import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BSPReconciliationRecordDTO;
import com.isa.thinair.invoicing.api.model.bsp.BSPTnx;
import com.isa.thinair.invoicing.api.model.bsp.BSPTransactionDataDTO.TransactionType;
import com.isa.thinair.invoicing.api.model.bsp.DPCProcessedBSPTnx;
import com.isa.thinair.invoicing.api.model.bsp.ParsedHOTFile;
import com.isa.thinair.invoicing.api.service.InvoicingModuleUtils;
import com.isa.thinair.invoicing.core.util.BSPCoreUtils;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;

/**
 * @author Janaka Padukka
 * 
 */
public class BSPReconciliationBL {

	private static Log log = LogFactory.getLog(BSPReconciliationBL.class);

	public void parseHOTFile(String hotFileContent, String processingUser, String hotFileName) throws ModuleException {

		// Reads hotfile and extract transactions
		HOTFileParser parser = new HOTFileParser();
		ParsedHOTFile parsedHOTFile;
		CurrencyProxy currencyProxy = new CurrencyProxy(InvoicingModuleUtils.getAirmasterBD());
		try {

			if (log.isDebugEnabled()) {
				log.debug("Hot file parsing - Start");
			}

			parsedHOTFile = parser.parse(hotFileContent);

			if (log.isDebugEnabled()) {
				log.debug("Hot file parsing - Completed");
			}
		} catch (BSPException e) {
			log.error(e.getMessage());
			e.printStackTrace();
			throw new ModuleException("um.bsp.hotfile.parsing.failed", e);
		}

		Map<String, List<DPCProcessedBSPTnx>> parsedTransactionsMap = parsedHOTFile.getTransactions();
		if (parsedTransactionsMap != null && !parsedTransactionsMap.isEmpty()) {

			List<DPCProcessedBSPTnx> parsedTransctions = new ArrayList<DPCProcessedBSPTnx>();
			Set<String> keySet = parsedTransactionsMap.keySet();
			
			for (String key : keySet) {
				List<DPCProcessedBSPTnx> tempTransactions = parsedTransactionsMap.get(key);
				if (tempTransactions.size() > 0) {
					parsedTransctions.addAll(tempTransactions);
				}
			}
			
			int parsedTransactionCount = parsedTransctions.size();

			List<List<DPCProcessedBSPTnx>> listOfSublists = Util.sliceListToListOfLists(parsedTransctions, 500);

			BSPHOTFileLog processedHOTFile = InvoicingModuleUtils.getBSPLinkDao().getHOTFileByName(hotFileName);
			int matchedTnxCount = 0;
			for (List<DPCProcessedBSPTnx> subList : listOfSublists) {

				List<DishFileTnxLogData> matchedTransactions = InvoicingModuleUtils.getBSPLinkDao()
						.getMatchingTransactions(subList);

				if (matchedTransactions != null && !matchedTransactions.isEmpty()) {

					matchedTnxCount = matchedTnxCount + matchedTransactions.size();

					Integer hotFileId = processedHOTFile.getLogID();
					for (DishFileTnxLogData matchedTransaction : matchedTransactions) {

						String txnType = matchedTransaction.getTnxType();
						String eticketId = null;
						if ((TransactionType.EMDS).toString().equals(txnType)) {
							eticketId = matchedTransaction.getEmds();
						} else if ((TransactionType.RFND).toString().equals(txnType)
								&& matchedTransaction.getEmds() != null && !matchedTransaction.getEmds().equals("")) {
							eticketId = matchedTransaction.getEmds();
						} else {
							eticketId = matchedTransaction.getPrimaryETicket() + "";
						}

						List<DPCProcessedBSPTnx> parsedTnxList = parsedTransactionsMap.get(matchedTransaction.getTnxType()
								+ eticketId);
						DPCProcessedBSPTnx parsedTnx = null;
						if (parsedTnxList.size() > 1 || (TransactionType.RFND).toString().equals(txnType)) {
							Integer txnId = null;
							String lccUniqueTxnID = null;
							if (matchedTransaction.getLccUniqueID() != null) {
								lccUniqueTxnID = matchedTransaction.getLccUniqueID();
							} else {
								txnId = matchedTransaction.getTnxID();
							}
							List<BSPTnx> originalTransactionList = InvoicingModuleUtils.getBSPLinkJdbcDao()
									.getBSPTransactionForHotFile(txnId, lccUniqueTxnID,
											ReservationTnxNominalCode.getBSPAccountTypeNominalCodes());

							BSPTnx originalTransaction = null;
							if (!originalTransactionList.isEmpty()) {
								originalTransaction = originalTransactionList.get(0);
							}							
							if (originalTransaction == null) {
								log.error("Cannot Find BSP Transaction [ Primary ET/EMDS:"
										+ matchedTransaction.getPrimaryETicket() + ", PNR:" + matchedTransaction.getPnr()
										+ ", Tnx Type:" + matchedTransaction.getTnxType() + " ]");
								continue;
							}
							
							Currency currency = currencyProxy.getCurrency(originalTransaction.getPayCurrencyCode());
							BigDecimal submittedAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
							if (matchedTransaction.getTnxType().equals(TransactionType.RFND.toString())) {
								submittedAmount = adjustDecimalPlaces(
										new BigDecimal(AccelAeroCalculator.formatAsDecimal(originalTransaction
												.getPayCurrencyAmount())), currency.getDecimalPlaces());
							} else {
								submittedAmount = adjustDecimalPlaces(
										new BigDecimal(AccelAeroCalculator.formatAsDecimal(originalTransaction
												.getPayCurrencyAmount().negate())), currency.getDecimalPlaces());
							}

							for (DPCProcessedBSPTnx parsedTnxTmp : parsedTnxList) {
								if (parsedTnxTmp.getProcessedAmount().equals(submittedAmount)) {
									parsedTnx = parsedTnxTmp;
									break;
								}
							}
							
							if (parsedTnx == null) {
								continue;			//RFND Transactions can occur outside the HOT File Transactions date range
							}
						} else if (!parsedTnxList.isEmpty()) {
							parsedTnx = parsedTnxList.get(0);
						} else {
							continue;
						}
						
						if (parsedTnx == null) {
							log.error("Mismatching transaction [ Primary ET/EMDS:" + matchedTransaction.getPrimaryETicket()
									+ ", PNR:" + matchedTransaction.getPnr() + ", Tnx Type:" + matchedTransaction.getTnxType()
									+ " ]");
							throw new ModuleException("um.bsp.hotfile.parsing.failed");
						}

						matchedTransaction.setProcessedAmount(parsedTnx.getProcessedAmount());
						matchedTransaction.setProcessedCurrencyCode(parsedTnx.getProcessedCurrencyCode());
						matchedTransaction.setReconciled(CommonsConstants.YES);
						matchedTransaction.setHOTFileId(hotFileId);
					}

					InvoicingModuleUtils.getBSPLinkDao().saveOrUpdateDishFileTnxLogData(matchedTransactions);
					
					if (log.isInfoEnabled()) {
						log.info("BSP HOT File-  number of matchedTnxCount: " + matchedTnxCount + ", parsedTransactionCount: "
								+ parsedTransactionCount);
					}

				}
			}
			

			if (log.isInfoEnabled()) {
				log.info("BSP HOT File number of matched transactions : " + matchedTnxCount);
			}

			if (matchedTnxCount != 0) {
				if (parsedTransactionCount != matchedTnxCount) {
					log.warn("HOT File ID:" + hotFileName + ":[Parsed Tnx Count:" + parsedTransactionCount
							+ ", Matching Tnx Count:" + matchedTnxCount + "]");
				}

				processedHOTFile.setFileStatus(BSPHOTFileLog.HOT_FILE_STATUS.PROCESSED.toString());
				processedHOTFile.setProcessedBy(processingUser);
				processedHOTFile.setProcessedOn(new Date());
				processedHOTFile.setDpcProcessedDate(parsedHOTFile.getProcessedDate());
				InvoicingModuleUtils.getBSPLinkDao().saveOrUpdateBSPHOTFile(processedHOTFile);
			} else {
				throw new ModuleException("um.bsp.hotfile.processing.no.matching.tnxs");
			}

		} else {
			throw new ModuleException("um.bsp.hotfile.processing.zero.tnxs ");
		}
	}

	public List<BSPReconciliationRecordDTO> getBSPReconciliationReportData(ReportsSearchCriteria criteria,
			UserPrincipal userPrincipal) throws ModuleException {
		return BSPCoreUtils.getBSPTransactionsForReconciliation(criteria, userPrincipal);
	}
	
	private BigDecimal adjustDecimalPlaces(BigDecimal decimal, int decimalPlaces) {
		String str = decimal.toString();
		String arr[] = str.split("\\.");
		if (arr.length > 1) {
			String dec = arr[1];
			if (dec.length() < decimalPlaces) {
				dec = dec + getZeros(decimalPlaces - dec.length());
			} else if (dec.length() > decimalPlaces) {
				dec = dec.substring(0, decimalPlaces);
			}
			str = arr[0] + "." + dec;
		}
		return new BigDecimal(str);
	}
	
	private String getZeros(int i) {
		StringBuffer bf = new StringBuffer();
		while (i > 0) {
			bf.append("0");
			i--;
		}
		return bf.toString();
	}

}
