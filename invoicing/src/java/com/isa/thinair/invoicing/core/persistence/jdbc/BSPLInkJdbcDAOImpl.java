package com.isa.thinair.invoicing.core.persistence.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.TicketGeneratorCriteriaType;
import com.isa.thinair.airtravelagents.api.model.AgentTransactionNominalCode;
import com.isa.thinair.airtravelagents.api.util.AirTravelAgentConstants;
import com.isa.thinair.airtravelagents.api.util.BLUtil;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.exception.CommonsDataAccessException;
import com.isa.thinair.commons.core.framework.PlatformBaseJdbcDAOSupport;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.commons.core.util.XDBConnectionUtil;
import com.isa.thinair.invoicing.api.dto.BSPResultsDTO;
import com.isa.thinair.invoicing.api.model.bsp.BSPAgentDataDTO;
import com.isa.thinair.invoicing.api.model.bsp.BSPReconciliationStatus;
import com.isa.thinair.invoicing.api.model.bsp.BSPSegmentInfoDTO;
import com.isa.thinair.invoicing.api.model.bsp.BSPTnx;
import com.isa.thinair.invoicing.api.model.bsp.BSPTransactionTaxData;
import com.isa.thinair.invoicing.api.model.bsp.ETLRCoupon;
import com.isa.thinair.invoicing.api.model.bsp.ETLRCouponRec;
import com.isa.thinair.invoicing.api.model.bsp.ETicketCouponStatus;
import com.isa.thinair.invoicing.api.model.bsp.ETicketFOP;
import com.isa.thinair.invoicing.api.service.InvoicingModuleUtils;
import com.isa.thinair.invoicing.api.utils.InvoicingConstants;
import com.isa.thinair.invoicing.api.util.InvoicingInternalConstants.BSPErrorLogStatus;
import com.isa.thinair.invoicing.api.util.NominalCodeConverter;
import com.isa.thinair.invoicing.core.persistence.dao.BSPLinkJdbcDAO;

public class BSPLInkJdbcDAOImpl extends PlatformBaseJdbcDAOSupport implements BSPLinkJdbcDAO {
	private Log log = LogFactory.getLog(BSPLInkJdbcDAOImpl.class);

	/**
	 * Query Ids
	 */
	private static final String AGENT_BSP_ALL_TNX = "AGENT_BSP_ALL_TNX";

	private static final String EXTERNAL_AGENT_BSP_TNX = "EXTERNAL_AGENT_BSP_TNX";

	private static final String BSP_EXTERNAL_AGENT_ERROR_FIXED_TNX = "BSP_EXTERNAL_AGENT_ERROR_FIXED_TNX";

	private static final String AGENT_BSP_TNX_BY_PRODUCT = "AGENT_BSP_TNX_BY_PRODUCT";

	private static final String AGENT_ALL_TNX = "AGENT_ALL_TNX";

	private static final String BSP_ENABLED_AGENTS_FOR_COUNTRY = "BSP_ENABLED_AGENTS_FOR_COUNTRY";
	
	private static final String SINGLE_BSP_ENABLED_AGENT_FOR_COUNTRY = "SINGLE_BSP_ENABLED_AGENT_FOR_COUNTRY";

	private static final String RET_FILE_GENERATION_AGENTS = "RET_FILE_GENERATION_AGENTS";

	private static final String BSP_TNX_SEGMENT_INFO = "BSP_TNX_SEGMENT_INFO";

	private static final String BSP_ORIGINAL_TNX_FOR_LCC = "BSP_ORIGINAL_TNX_FOR_LCC";

	private static final String BSP_ORIGINAL_TNX_FOR_OWN = "BSP_ORIGINAL_TNX_FOR_OWN";

	private static final String BSP_ERROR_FIXED_AGENT_TNX = "BSP_ERROR_FIXED_AGENT_TNX";

	private static final String BSP_PAX_TNX_TAX_DATA = "BSP_PAX_TNX_TAX_DATA";

	private static final String AGENT_CODE_FOR_IATA_NUMBER = "AGENT_CODE_FOR_IATA_NUMBER";

	private static final String GET_AGENTS_BY_TYPE = "GET_AGENTS_BY_TYPE";

	private static final String E_TICKET_LIFT_DATA = "E_TICKET_LIFT_DATA";

	private static final String E_TICKET_COUPONS_STATUS = "E_TICKET_COUPONS_STATUS";

	private static final String E_TICKET_COUPON = "E_TICKET_COUPON";

	private static final String E_TICKET_FOP = "E_TICKET_FOP";

	private static final String BSP_OWN_TXN_DETAILS_FOR_HOT_FILE_PROCESS = "BSP_OWN_TXN_DETAILS_FOR_HOT_FILE_PROCESS";

	private static final String BSP_LCC_TXN_DETAILS_FOR_HOT_FILE_PROCESS = "BSP_LCC_TXN_DETAILS_FOR_HOT_FILE_PROCESS";

	/*
	 * Other Constants
	 */
	private static final String SELECT_ALL_COUNTRY = "All";

	@Override
	@SuppressWarnings("unchecked")
	public List<BSPTnx> getAgentAllBSPTransactions(String agentCode, Date fromDate, Date toDate,
			Collection<Integer> nomincalCodes, String dataSourceType) {
		return getAllBSPTransactions(agentCode, fromDate, toDate, true, nomincalCodes, dataSourceType, null);

	}

	public List<BSPTnx> getExternalAgentBSPTransactions(String agentCode, Date fromDate, Date toDate,
			Collection<Integer> nomincalCodes, String dataSourceType) {
		return getExternalBSPTransactions(agentCode, fromDate, toDate, true, nomincalCodes, dataSourceType);

	}

	public List<BSPTnx> getExternalBSPTransactions(String bspCountry, Date fromDate, Date toDate,
			Collection<Integer> nominalCodes, String dataSourceType, String entity) {

		StringBuilder agentQueryBuilder = new StringBuilder();

		agentQueryBuilder.append("SELECT DISTINCT TA.AGENT_CODE ");

		agentQueryBuilder.append("FROM T_AGENT TA ");
		agentQueryBuilder.append("WHERE TA.SERVICE_CHANNEL_CODE IN ('AA', 'AH') ");
		agentQueryBuilder.append("AND TA.AIRLINE_CODE          ='");
		agentQueryBuilder.append(AppSysParamsUtil.getDefaultAirlineIdentifierCode());
		agentQueryBuilder.append("' AND TA.STATION_CODE         IN");
		agentQueryBuilder.append(" (SELECT TS.STATION_CODE");
		agentQueryBuilder.append("  FROM T_COUNTRY TC, T_STATION TS WHERE");
		if (!bspCountry.equals(SELECT_ALL_COUNTRY)) {
			agentQueryBuilder.append("  TC.COUNTRY_CODE ='");
			agentQueryBuilder.append(bspCountry + "' AND");
		}
		agentQueryBuilder.append("  TS.COUNTRY_CODE   = TC.COUNTRY_CODE");
		agentQueryBuilder.append(" )");

		StringBuilder nominalCodesBuilder = new StringBuilder();
		int noOfTnxTypes = nominalCodes.size();
		int counter = 0;
		for (Integer tnxNomincalCode : nominalCodes) {
			counter++;
			nominalCodesBuilder.append(NominalCodeConverter.toAgentTnxNominalCode(String.valueOf(tnxNomincalCode)));
			if (counter != noOfTnxTypes) {
				nominalCodesBuilder.append(",");
			}
		}
		String tnxNominalCodesString = nominalCodesBuilder.toString();
		String agentSelectSubQuery = agentQueryBuilder.toString();

		String[] args = { tnxNominalCodesString, agentSelectSubQuery };

		String query = getQuery(AGENT_BSP_TNX_BY_PRODUCT, args);
		JdbcTemplate template = new JdbcTemplate(InvoicingModuleUtils.getDataSource(dataSourceType));

		Object params[] = { fromDate, toDate,entity };

		if (log.isDebugEnabled()) {
			log.debug("QUERY is " + query);
		}

		List<BSPTnx> tnxList = (List<BSPTnx>) template.query(query, params, new RowMapper() {

			@Override
			public BSPTnx mapRow(ResultSet rs, int rowNum) throws SQLException {
				BSPTnx bspTnx = new BSPTnx();

				bspTnx.setAmount(rs.getBigDecimal("amount"));
				// bspTnx.setLccUniqueTnxID(rs.getString("lcc_unique_txn_id"));
				// bspTnx.setNominalCode(rs.getInt("nominal_code"));
				bspTnx.setNomialDesc(rs.getString("paymentDesc"));
				// bspTnx.setPaxSequence(rs.getInt("pax_sequence"));
				bspTnx.setPayCurrencyAmount(rs.getBigDecimal("amount_paycur"));
				bspTnx.setPayCurrencyCode(rs.getString("payment_currency_code"));
				// bspTnx.setPnrPaxID(rs.getInt("pnr_pax_id"));
				bspTnx.setTnxDate(rs.getDate("tnx_date"));
				bspTnx.setTnxType(rs.getString("dr_cr"));
				if (rs.getInt("tnx_id") != 0) {
					bspTnx.setTnxID(rs.getInt("tnx_id"));
				}
				// bspTnx.setOriginalPaymentID(rs.getInt("original_payment_id"));
				bspTnx.setAgentCode(rs.getString("agent_code"));
				bspTnx.setCountry(rs.getString("country"));
				bspTnx.setIataAgentCode(rs.getString("iata_agent_code"));
				bspTnx.setPnr(rs.getString("pnr"));
				bspTnx.setExternalReference(rs.getString("ext_reference"));
				// bspTnx.setPaxName(rs.getString("pax_name"));
				// bspTnx.setFreshReservationPayment(CommonsConstants.YES.equals(rs.getString("first_payment")) ? true :
				// false);
				return bspTnx;
			}
		});
		return tnxList;
	}

	@SuppressWarnings("unchecked")
	private List<BSPTnx> getAllBSPTransactions(String queryParam, Date fromDate, Date toDate, boolean isSingleAgent,
			Collection<Integer> tnxNominalCodes, String dataSourceType, String entity) {

		StringBuilder agentQueryBuilder = new StringBuilder();
		if (isSingleAgent) {
			agentQueryBuilder.append("'" + queryParam + "'");
		} else {
			agentQueryBuilder.append("SELECT DISTINCT TA.AGENT_CODE ");

			/**
			 * No need to check payment type for report, there can be agents who had BSP, then removed.
			 * agentQueryBuilder.append("FROM T_AGENT TA, T_AGENT_PAYMENT_METHOD TAPM ");
			 * agentQueryBuilder.append("WHERE TA.AGENT_CODE = TAPM.AGENT_CODE "); agentQueryBuilder.append("AND
			 * TAPM.PAYMENT_CODE = 'BSP' ");
			 **/
			agentQueryBuilder.append("FROM T_AGENT TA ");
			agentQueryBuilder.append("WHERE TA.SERVICE_CHANNEL_CODE IN ('AA', 'AH') ");
			agentQueryBuilder.append("AND TA.AIRLINE_CODE          ='");
			agentQueryBuilder.append(AppSysParamsUtil.getDefaultAirlineIdentifierCode());
			agentQueryBuilder.append("' AND TA.STATION_CODE         IN");
			agentQueryBuilder.append(" (SELECT TS.STATION_CODE");
			agentQueryBuilder.append("  FROM T_COUNTRY TC, T_STATION TS WHERE");
			if (!queryParam.equals(SELECT_ALL_COUNTRY)) {
				agentQueryBuilder.append("  TC.COUNTRY_CODE ='");
				agentQueryBuilder.append(queryParam + "' AND");
			}
			agentQueryBuilder.append("  TS.COUNTRY_CODE   = TC.COUNTRY_CODE");
			agentQueryBuilder.append(" )");
		}

		StringBuilder nominalCodesBuilder = new StringBuilder();
		int noOfTnxTypes = tnxNominalCodes.size();
		int counter = 0;
		for (Integer tnxNomincalCode : tnxNominalCodes) {
			counter++;
			nominalCodesBuilder.append(tnxNomincalCode);
			if (counter != noOfTnxTypes) {
				nominalCodesBuilder.append(",");
			}
		}
		String tnxNominalCodesString = nominalCodesBuilder.toString();
		String agentSelectSubQuery = agentQueryBuilder.toString();

		String[] args = { tnxNominalCodesString, agentSelectSubQuery, tnxNominalCodesString, agentSelectSubQuery,
				tnxNominalCodesString, agentSelectSubQuery };

		String query = getQuery(AGENT_BSP_ALL_TNX, args);
		
		if(entity !=null && !entity.equals("")){
			query += " WHERE V.entity_code = '" + entity + "'";
		}
		JdbcTemplate template = new JdbcTemplate(InvoicingModuleUtils.getDataSource(dataSourceType));

		Object params[] = { fromDate, toDate, AppSysParamsUtil.getDefaultCarrierCode(), fromDate, toDate, fromDate, toDate };

		if (log.isDebugEnabled()) {
			log.debug("QUERY is " + query);
		}

		List<BSPTnx> tnxList = (List<BSPTnx>) template.query(query, params, new RowMapper() {

			@Override
			public BSPTnx mapRow(ResultSet rs, int rowNum) throws SQLException {
				BSPTnx bspTnx = new BSPTnx();

				bspTnx.setAmount(rs.getBigDecimal("amount"));
				bspTnx.setLccUniqueTnxID(rs.getString("lcc_unique_txn_id"));
				bspTnx.setNominalCode(rs.getInt("nominal_code"));
				bspTnx.setNomialDesc(rs.getString("paymentDesc"));
				bspTnx.setPaxSequence(rs.getInt("pax_sequence"));
				bspTnx.setPayCurrencyAmount(rs.getBigDecimal("amount_paycur"));
				bspTnx.setPayCurrencyCode(rs.getString("payment_currency_code"));
				bspTnx.setPnrPaxID(rs.getInt("pnr_pax_id"));
				bspTnx.setTnxDate(rs.getDate("tnx_date"));
				bspTnx.setTnxType(rs.getString("dr_cr"));
				if (rs.getInt("tnxId") != 0) {
					bspTnx.setTnxID(rs.getInt("tnxId"));
				}
				bspTnx.setOriginalPaymentID(rs.getInt("original_payment_id"));
				bspTnx.setAgentCode(rs.getString("agent_code"));
				bspTnx.setCountry(rs.getString("country"));
				bspTnx.setIataAgentCode(rs.getString("iata_agent_code"));
				bspTnx.setPnr(rs.getString("pnr"));
				bspTnx.setPaxName(rs.getString("pax_name"));
				bspTnx.setFreshReservationPayment(CommonsConstants.YES.equals(rs.getString("first_payment")) ? true : false);
				return bspTnx;
			}
		});
		return tnxList;
	}

	private List<BSPTnx> getExternalBSPTransactions(String queryParam, Date fromDate, Date toDate, boolean isSingleAgent,
			Collection<Integer> tnxNominalCodes, String dataSourceType) {

		StringBuilder agentQueryBuilder = new StringBuilder();
		if (isSingleAgent) {
			agentQueryBuilder.append("'" + queryParam + "'");
		} else {
			agentQueryBuilder.append("SELECT DISTINCT TA.AGENT_CODE ");
			agentQueryBuilder.append("FROM T_AGENT TA ");
			agentQueryBuilder.append("WHERE TA.SERVICE_CHANNEL_CODE IN ('AA', 'AH') ");
			agentQueryBuilder.append("AND TA.AIRLINE_CODE          ='");
			agentQueryBuilder.append(AppSysParamsUtil.getDefaultAirlineIdentifierCode());
			agentQueryBuilder.append("' AND TA.STATION_CODE         IN");
			agentQueryBuilder.append(" (SELECT TS.STATION_CODE");
			agentQueryBuilder.append("  FROM T_COUNTRY TC, T_STATION TS WHERE");
			if (!queryParam.equals(SELECT_ALL_COUNTRY)) {
				agentQueryBuilder.append("  TC.COUNTRY_CODE ='");
				agentQueryBuilder.append(queryParam + "' AND");
			}
			agentQueryBuilder.append("  TS.COUNTRY_CODE   = TC.COUNTRY_CODE");
			agentQueryBuilder.append(" )");
		}

		StringBuilder nominalCodesBuilder = new StringBuilder();
		int noOfTnxTypes = tnxNominalCodes.size();
		int counter = 0;
		for (Integer tnxNomincalCode : tnxNominalCodes) {
			counter++;
			nominalCodesBuilder.append(tnxNomincalCode);
			if (counter != noOfTnxTypes) {
				nominalCodesBuilder.append(",");
			}
		}
		String agentSelectSubQuery = agentQueryBuilder.toString();
		String nominalCodeSubQuery = nominalCodesBuilder.toString();

		String[] args = { agentSelectSubQuery, nominalCodeSubQuery };

		String query = getQuery(EXTERNAL_AGENT_BSP_TNX, args);
		JdbcTemplate template = new JdbcTemplate(InvoicingModuleUtils.getDataSource(dataSourceType));

		Object params[] = { fromDate, toDate };

		if (log.isDebugEnabled()) {
			log.debug("QUERY is " + query);
		}

		List<BSPTnx> tnxList = (List<BSPTnx>) template.query(query, params, new RowMapper() {

			@Override
			public BSPTnx mapRow(ResultSet rs, int rowNum) throws SQLException {
				BSPTnx bspTnx = new BSPTnx();

				bspTnx.setAmount(rs.getBigDecimal("amount"));
				bspTnx.setNomialDesc(rs.getString("payment_desc"));
				bspTnx.setPayCurrencyAmount(rs.getBigDecimal("amount_local"));
				bspTnx.setPayCurrencyCode(rs.getString("currency_code"));
				bspTnx.setTnxDate(rs.getDate("tnx_date"));
				bspTnx.setAgentCode(rs.getString("agent_code"));
				bspTnx.setCountry(rs.getString("country"));
				bspTnx.setIataAgentCode(rs.getString("iata_agent_code"));
				bspTnx.setExternalReference(rs.getString("ext_reference"));
				bspTnx.setPaxName(rs.getString("pax_name"));
				bspTnx.setProductType(rs.getString("product_type"));
				return bspTnx;
			}
		});
		return tnxList;
	}

	public List<BSPTnx> getExternalBSPAgentErrorFixedTransactions(String agentCode) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		String query = getQuery(BSP_EXTERNAL_AGENT_ERROR_FIXED_TNX);

		Object params[] = { agentCode, agentCode, BSPErrorLogStatus.RESUBMIT.toString() };

		if (log.isDebugEnabled()) {
			log.debug("QUERY is " + query);
		}

		List<BSPTnx> tnxList = (List<BSPTnx>) template.query(query, params, new RowMapper() {

			@Override
			public BSPTnx mapRow(ResultSet rs, int rowNum) throws SQLException {
				BSPTnx bspTnx = new BSPTnx();

				bspTnx.setAmount(rs.getBigDecimal("amount"));
				bspTnx.setNomialDesc(rs.getString("payment_desc"));
				bspTnx.setPayCurrencyAmount(rs.getBigDecimal("amount_local"));
				bspTnx.setPayCurrencyCode(rs.getString("currency_code"));
				bspTnx.setTnxDate(rs.getDate("tnx_date"));
				bspTnx.setAgentCode(rs.getString("agent_code"));
				bspTnx.setCountry(rs.getString("country"));
				bspTnx.setIataAgentCode(rs.getString("iata_agent_code"));
				bspTnx.setExternalReference(rs.getString("ext_reference"));
				bspTnx.setPaxName(rs.getString("pax_name"));

				return bspTnx;
			}
		});
		return tnxList;
	}

	private List<BSPTnx> getAllBSPTransactions(Date fromDate, Date toDate, boolean isSingleAgent,
			Collection<Integer> tnxNominalCodes) {

		String[] args = {};

		String query = getQuery(AGENT_ALL_TNX, null);
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		Object params[] = { fromDate, toDate, AppSysParamsUtil.getDefaultCarrierCode(), fromDate, toDate, fromDate, toDate };

		if (log.isDebugEnabled()) {
			log.debug("QUERY is " + query);
		}

		List<BSPTnx> tnxList = (List<BSPTnx>) template.query(query, params, new RowMapper() {

			@Override
			public BSPTnx mapRow(ResultSet rs, int rowNum) throws SQLException {
				BSPTnx bspTnx = new BSPTnx();

				bspTnx.setAmount(rs.getBigDecimal("amount"));
				bspTnx.setLccUniqueTnxID(rs.getString("lcc_unique_txn_id"));
				bspTnx.setNominalCode(rs.getInt("nominal_code"));
				bspTnx.setPaxSequence(rs.getInt("pax_sequence"));
				bspTnx.setPayCurrencyAmount(rs.getBigDecimal("amount_paycur"));
				bspTnx.setPayCurrencyCode(rs.getString("payment_currency_code"));
				bspTnx.setPnrPaxID(rs.getInt("pnr_pax_id"));
				bspTnx.setTnxDate(rs.getDate("tnx_date"));
				bspTnx.setTnxType(rs.getString("dr_cr"));
				bspTnx.setTnxID(rs.getInt("tnxId"));
				bspTnx.setOriginalPaymentID(rs.getInt("original_payment_id"));
				bspTnx.setAgentCode(rs.getString("agent_code"));
				bspTnx.setIataAgentCode(rs.getString("iata_agent_code"));
				bspTnx.setPnr(rs.getString("pnr"));
				bspTnx.setPaxName(rs.getString("pax_name"));

				return bspTnx;
			}
		});

		return tnxList;

	}

	public BSPAgentDataDTO getBSPAgent(String countryCode, String agentCode) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		String query = getQuery(SINGLE_BSP_ENABLED_AGENT_FOR_COUNTRY);
		Object params[] = { AppSysParamsUtil.getDefaultAirlineIdentifierCode(), countryCode, agentCode };

		if (log.isDebugEnabled()) {
			log.debug("QUERY is " + query);
		}

		BSPAgentDataDTO agentDTO = (BSPAgentDataDTO) template.queryForObject(query, params, new RowMapper<BSPAgentDataDTO>() {

			@Override
			public BSPAgentDataDTO mapRow(ResultSet rs, int arg1) throws SQLException {
				BSPAgentDataDTO agentDataDTO = new BSPAgentDataDTO();
				agentDataDTO.setAgentCode(rs.getString("agent_code"));
				agentDataDTO.setIataCode(rs.getString("agent_iata_number"));
				return agentDataDTO;
			}
		});
		return agentDTO;
	}

	public List<BSPAgentDataDTO> getBSPEnabledAgents(String countryCode) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		String query = getQuery(BSP_ENABLED_AGENTS_FOR_COUNTRY);
		Object params[] = { AppSysParamsUtil.getDefaultAirlineIdentifierCode(), countryCode };

		if (log.isDebugEnabled()) {
			log.debug("QUERY is " + query);
		}

		List<BSPAgentDataDTO> agentDataDTOs = (List<BSPAgentDataDTO>) template.query(query, params, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<BSPAgentDataDTO> results = new ArrayList<BSPAgentDataDTO>();

				while (rs.next()) {
					BSPAgentDataDTO agentDataDTO = new BSPAgentDataDTO();
					agentDataDTO.setAgentCode(rs.getString("agent_code"));
					agentDataDTO.setIataCode(rs.getString("agent_iata_number"));

					results.add(agentDataDTO);
				}

				return results;
			}
		});

		return agentDataDTOs;
	}

	public List<BSPAgentDataDTO> getCTOandATOAgents(String types) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		String query = getQuery(RET_FILE_GENERATION_AGENTS);
		String args = " AND ta.agent_type_code  IN  (" + types + ") ORDER BY ta.agent_code ";
		Object params[] = { AppSysParamsUtil.getDefaultAirlineIdentifierCode() };

		if (log.isDebugEnabled()) {
			log.debug("QUERY is " + query);
		}

		List<BSPAgentDataDTO> agentDataDTOs = (List<BSPAgentDataDTO>) template.query(query + args, params,
				new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						List<BSPAgentDataDTO> results = new ArrayList<BSPAgentDataDTO>();

						while (rs.next()) {
							BSPAgentDataDTO agentDataDTO = new BSPAgentDataDTO();
							agentDataDTO.setAgentCode(rs.getString("agent_code"));
							agentDataDTO.setIataCode(rs.getString("agent_iata_number"));
							agentDataDTO.setStation_code(rs.getString("station_code"));
							agentDataDTO.setAccountCode(rs.getString("account_code"));
							agentDataDTO.setCountaryCode(rs.getString("country_code"));
							results.add(agentDataDTO);
						}

						return results;
					}
				});

		return agentDataDTOs;
	}

	public List<BSPAgentDataDTO> getAgentsForRETFiles() {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		String query = getQuery(RET_FILE_GENERATION_AGENTS);
		Object params[] = { AppSysParamsUtil.getDefaultAirlineIdentifierCode() };

		if (log.isDebugEnabled()) {
			log.debug("QUERY is " + query);
		}

		List<BSPAgentDataDTO> agentDataDTOs = (List<BSPAgentDataDTO>) template.query(query, params, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<BSPAgentDataDTO> results = new ArrayList<BSPAgentDataDTO>();

				while (rs.next()) {
					BSPAgentDataDTO agentDataDTO = new BSPAgentDataDTO();
					agentDataDTO.setAgentCode(rs.getString("agent_code"));
					agentDataDTO.setIataCode(rs.getString("agent_iata_number"));

					results.add(agentDataDTO);
				}

				return results;
			}
		});

		return agentDataDTOs;
	}

	public String getAgentCode(String iatanumber) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		String query = getQuery(AGENT_CODE_FOR_IATA_NUMBER);
		Object params[] = { iatanumber };

		if (log.isDebugEnabled()) {
			log.debug("QUERY is " + query);
		}

		String agentCode = (String) template.query(query, params, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				String agentCode = null;
				while (rs.next()) {
					agentCode = rs.getString("");
				}
				return agentCode;
			}
		});

		return agentCode;
	}

	public List<BSPSegmentInfoDTO> getSegmentInformation(int pnrPaxId) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		String query = getQuery(BSP_TNX_SEGMENT_INFO);
		Object params[] = { pnrPaxId, pnrPaxId };

		if (log.isDebugEnabled()) {
			log.debug("QUERY is " + query);
		}

		List<BSPSegmentInfoDTO> segmentInfo = (List<BSPSegmentInfoDTO>) template.query(query, params, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<BSPSegmentInfoDTO> results = new ArrayList<BSPSegmentInfoDTO>();

				while (rs.next()) {
					BSPSegmentInfoDTO infoDTO = new BSPSegmentInfoDTO();
					infoDTO.setCarrierCode(rs.getString("carrier_code"));
					infoDTO.setDepartureDate(rs.getTimestamp("departure_date"));
					infoDTO.setDepartureDateZulu(rs.getTimestamp("departure_date_zulu"));
					infoDTO.setFlightNumber(rs.getString("flight_number"));
					infoDTO.setPnr(rs.getString("pnr"));
					infoDTO.setPnrPaxID(rs.getInt("pnr_pax_id"));

					String segmentCode = rs.getString("segment_code");
					String[] airPorts = segmentCode.split("/");

					infoDTO.setSegmentCode(segmentCode);
					infoDTO.setOrigin(airPorts[0]);
					infoDTO.setDestination(airPorts[airPorts.length - 1]);
					// infoDTO.setFreeBaggageAllowance(rs.getString("baggage_weight"));

					results.add(infoDTO);
				}

				return results;
			}
		});

		return segmentInfo;
	}

	public BSPTnx getOriginalLCCTransaction(Integer originalTnxID, int paxSequnce) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		String query = getQuery(BSP_ORIGINAL_TNX_FOR_LCC);
		Object params[] = { originalTnxID, paxSequnce };

		if (log.isDebugEnabled()) {
			log.debug("QUERY is " + query);
		}

		BSPTnx bspTnx = (BSPTnx) template.query(query, params, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				BSPTnx bspTnx = null;
				while (rs.next()) {
					bspTnx = new BSPTnx();
					bspTnx.setAmount(rs.getBigDecimal("amount"));
					bspTnx.setLccUniqueTnxID(rs.getString("lcc_unique_txn_id"));
					bspTnx.setNominalCode(rs.getInt("nominal_code"));
					bspTnx.setPaxSequence(rs.getInt("pax_sequence"));
					bspTnx.setPayCurrencyAmount(rs.getBigDecimal("amount_paycur"));
					bspTnx.setPayCurrencyCode(rs.getString("payment_currency_code"));
					bspTnx.setPnrPaxID(rs.getInt("pnr_pax_id"));
					bspTnx.setTnxDate(rs.getDate("tnx_date"));
					bspTnx.setTnxType(rs.getString("dr_cr"));
					bspTnx.setTnxID(rs.getInt("tnxId"));
					bspTnx.setFreshReservationPayment(CommonsConstants.YES.equals(rs.getString("first_payment")) ? true : false);
				}
				return bspTnx;
			}
		});

		return bspTnx;
	}

	public BSPTnx getOriginalOwnTransaction(Integer originalTnxID, int paxSequnce) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		String query = getQuery(BSP_ORIGINAL_TNX_FOR_OWN);
		Object params[] = { originalTnxID, paxSequnce };

		if (log.isDebugEnabled()) {
			log.debug("QUERY is " + query);
		}

		BSPTnx bspTnx = (BSPTnx) template.query(query, params, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				BSPTnx bspTnx = null;
				while (rs.next()) {
					bspTnx = new BSPTnx();

					bspTnx.setAmount(rs.getBigDecimal("amount"));
					bspTnx.setLccUniqueTnxID(rs.getString("lcc_unique_txn_id"));
					bspTnx.setNominalCode(rs.getInt("nominal_code"));
					bspTnx.setPaxSequence(rs.getInt("pax_sequence"));
					bspTnx.setPayCurrencyAmount(rs.getBigDecimal("amount_paycur"));
					bspTnx.setPayCurrencyCode(rs.getString("payment_currency_code"));
					bspTnx.setPnrPaxID(rs.getInt("pnr_pax_id"));
					bspTnx.setTnxDate(rs.getDate("tnx_date"));
					bspTnx.setTnxType(rs.getString("dr_cr"));
					bspTnx.setTnxID(rs.getInt("tnxId"));
					bspTnx.setFreshReservationPayment(CommonsConstants.YES.equals(rs.getString("first_payment")) ? true : false);
				}
				return bspTnx;
			}
		});

		return bspTnx;
	}

	public List<BSPTnx> getAgentErrorFixedBSPTransactions(String agentCode) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		String query = getQuery(BSP_ERROR_FIXED_AGENT_TNX);

		Object params[] = { agentCode, agentCode, BSPErrorLogStatus.RESUBMIT.toString(),
				AppSysParamsUtil.getDefaultCarrierCode(), agentCode, agentCode, BSPErrorLogStatus.RESUBMIT.toString(), agentCode,
				agentCode, BSPErrorLogStatus.RESUBMIT.toString() };

		if (log.isDebugEnabled()) {
			log.debug("QUERY is " + query);
		}

		List<BSPTnx> tnxList = (List<BSPTnx>) template.query(query, params, new RowMapper() {

			@Override
			public BSPTnx mapRow(ResultSet rs, int rowNum) throws SQLException {
				BSPTnx bspTnx = new BSPTnx();

				bspTnx.setAmount(rs.getBigDecimal("amount"));
				bspTnx.setLccUniqueTnxID(rs.getString("lcc_unique_txn_id"));
				bspTnx.setNominalCode(rs.getInt("nominal_code"));
				bspTnx.setPaxSequence(rs.getInt("pax_sequence"));
				bspTnx.setPayCurrencyAmount(rs.getBigDecimal("amount_paycur"));
				bspTnx.setPayCurrencyCode(rs.getString("payment_currency_code"));
				bspTnx.setPnrPaxID(rs.getInt("pnr_pax_id"));
				bspTnx.setTnxDate(rs.getDate("tnx_date"));
				bspTnx.setTnxType(rs.getString("dr_cr"));
				bspTnx.setTnxID(rs.getInt("tnxId"));
				bspTnx.setOriginalPaymentID(rs.getInt("original_payment_id"));
				bspTnx.setFreshReservationPayment(CommonsConstants.YES.equals(rs.getString("first_payment")) ? true : false);

				return bspTnx;
			}
		});
		return tnxList;
	}

	@Override
	public List<BSPTnx> getAllBSPTransactionsForReconciliation(String bspCountry, Date fromDate, Date toDate,
			Collection<Integer> nominalCodes, String dataSourceType, String entity) {
		return getAllBSPTransactions(bspCountry, fromDate, toDate, false, nominalCodes, dataSourceType, entity);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BSPReconciliationStatus> getReconciliationInformation(List<Integer> ownTnxIDs, List<String> lccUniqueIDs,
			String dataSourceType, List<String> extReferences) {
		JdbcTemplate template = new JdbcTemplate(InvoicingModuleUtils.getDataSource(dataSourceType));
		StringBuilder queryBuilder = new StringBuilder();

		queryBuilder.append("SELECT FDL.TNX_ID, FDL.LCC_UNIQUE_TXN_ID,FDL.PRIMARY_E_TICKET,FDL.SECONDARY_E_TICKET,");
		queryBuilder.append(
				" FDL.RECONCILED, FDL.PROCESSED_AMOUNT, FDL.PROCESSED_CURRENCY, HFL.DPC_PROCESSED_DATE, FL.RET_FILE_STATUS,FDL.PAX_SEQUENCE,FDL.EXT_REFERENCE ");
		queryBuilder.append(" FROM T_BSP_FILE_DATA_LOG FDL,T_BSP_HOT_FILE_LOG HFL, T_BSP_FILE_LOG FL ");
		queryBuilder.append("WHERE (");

		if (ownTnxIDs != null && !ownTnxIDs.isEmpty()) {
			queryBuilder.append(Util.getReplaceStringForIN("FDL.TNX_ID", ownTnxIDs));

		}

		if (lccUniqueIDs != null && !lccUniqueIDs.isEmpty()) {
			if (ownTnxIDs != null && !ownTnxIDs.isEmpty()) {
				queryBuilder.append(" OR ");
			}
			queryBuilder.append(Util.getReplaceStringForIN("FDL.LCC_UNIQUE_TXN_ID", lccUniqueIDs));
		}
		
		if (extReferences != null && !extReferences.isEmpty()) {
			if (lccUniqueIDs != null && !lccUniqueIDs.isEmpty()) {
				queryBuilder.append(" OR ");
			}
			queryBuilder.append(Util.getReplaceStringForIN("FDL.EXT_REFERENCE", extReferences));
		}
		
		queryBuilder.append(") ");
		queryBuilder.append("AND FDL.BSP_HOT_FILE_LOG_ID = HFL.BSP_HOT_FILE_LOG_ID(+) ");
		queryBuilder.append("AND FDL.BSP_FILE_LOG_ID = FL.BSP_FILE_LOG_ID ");
		String query = queryBuilder.toString();

		if (log.isDebugEnabled()) {
			log.debug("QUERY is " + query);
		}
		Object params[] = {};

		List<BSPReconciliationStatus> bspReconStatusList = (List<BSPReconciliationStatus>) template.query(query, params,
				new RowMapper() {

					@Override
					public BSPReconciliationStatus mapRow(ResultSet rs, int rowNum) throws SQLException {
						BSPReconciliationStatus bspReconStatus = new BSPReconciliationStatus();

						bspReconStatus.setLccUniqueTnxID(rs.getString("LCC_UNIQUE_TXN_ID"));
						if (rs.getInt("TNX_ID") != 0) {
							bspReconStatus.setTnxID(rs.getInt("TNX_ID"));
						}
						bspReconStatus.setReconciled(rs.getString("RECONCILED"));
						bspReconStatus.setDpcProcessedDate(rs.getDate("DPC_PROCESSED_DATE"));
						bspReconStatus.setPrimaryEticket(rs.getLong("PRIMARY_E_TICKET"));
						bspReconStatus.setSecondaryEticket(rs.getLong("SECONDARY_E_TICKET"));
						bspReconStatus.setProcessedAmount(rs.getBigDecimal("PROCESSED_AMOUNT"));
						bspReconStatus.setProcessedCurrency(rs.getString("PROCESSED_CURRENCY"));
						bspReconStatus.setRETFileStatus(rs.getString("RET_FILE_STATUS"));
						bspReconStatus.setPaxSequence(rs.getInt("PAX_SEQUENCE"));
						bspReconStatus.setExternalReference(rs.getString("EXT_REFERENCE"));
						return bspReconStatus;
					}
				});
		return bspReconStatusList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ETLRCouponRec> getETLRCouponRec(Date startDate, Date endDate) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		String query = getQuery(E_TICKET_LIFT_DATA);
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy HH:mm:ss");
		// Object params[] = { "23-MAR-14", "24-MAR-14" };
		Date fmtStartDate = startDate;
		Date fmtEndDate = endDate;
		try {
			fmtStartDate = sdf.parse(sdf.format(startDate));
			fmtEndDate = sdf.parse(sdf.format(endDate));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Object params[] = { fmtStartDate, fmtEndDate };

		if (log.isDebugEnabled()) {
			log.debug("QUERY is " + query);
		}

		List<ETLRCouponRec> etlrCouponList = (List<ETLRCouponRec>) template.query(query, params, new RowMapper() {

			@Override
			public ETLRCouponRec mapRow(ResultSet rs, int rowNum) throws SQLException {
				ETLRCouponRec etlrCouponRec = new ETLRCouponRec();
				etlrCouponRec.seteTicketNumber(rs.getString("eTicketNumber"));
				etlrCouponRec.seteTicketVersion(rs.getInt("version"));
				etlrCouponRec.setModifiedDate(rs.getDate("modifiedDate"));
				etlrCouponRec.setCreatedDate(rs.getDate("dateCreated"));
				etlrCouponRec.setPaxTypeCode(rs.getString("paxTypeCode"));
				etlrCouponRec.setFirstName(rs.getString("firstName"));
				etlrCouponRec.setLastName(rs.getString("lastName"));
				etlrCouponRec.setPaxID(rs.getString("paxid"));
				etlrCouponRec.setCouponNumber(rs.getString("couponNumber"));
				etlrCouponRec.setCouponStatus(rs.getString("couponStatus"));
				etlrCouponRec.setSegmentCode(rs.getString("segmentCode"));
				etlrCouponRec.setDepatureDate(rs.getDate("departureTime"));
				etlrCouponRec.setFlightNumber(rs.getString("flightNumber"));
				etlrCouponRec.setReturnFlag(rs.getString("returnFlag"));
				etlrCouponRec.setPnr(rs.getString("pnr"));
				etlrCouponRec.setTotalSurchargeAmount(rs.getBigDecimal("totalSurchargeAmount"));
				etlrCouponRec.setTaxAmount(rs.getBigDecimal("taxAmount"));
				etlrCouponRec.setFareAmount(rs.getBigDecimal("fareAmount"));
				etlrCouponRec.setAgentTypeCode(rs.getString("agentTypeCode"));
				etlrCouponRec.setAgentCountaryCode(rs.getString("agentCountaryCode"));
				etlrCouponRec.setAgentID(rs.getString("agentID"));
				etlrCouponRec.setClassofService(rs.getString("classofService"));
				// System.out.println(etlrCouponRec.geteTicketNumber());
				return etlrCouponRec;
			}
		});
		return etlrCouponList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ETicketCouponStatus> getETicketCouponStatus(String eticketNumber) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		String query = getQuery(E_TICKET_COUPONS_STATUS);
		Object params[] = { eticketNumber };

		if (log.isDebugEnabled()) {
			log.debug("QUERY is " + query);
		}

		List<ETicketCouponStatus> eticketCouponStatusList = (List<ETicketCouponStatus>) template.query(query, params,
				new RowMapper() {

					@Override
					public ETicketCouponStatus mapRow(ResultSet rs, int rowNum) throws SQLException {
						ETicketCouponStatus eticketCouponStatus = new ETicketCouponStatus();
						eticketCouponStatus.seteTicketNumber(rs.getString("etktNumber"));
						eticketCouponStatus.setModifiedDate(rs.getDate("modifiedDate"));
						eticketCouponStatus.setCouponNumber(rs.getString("couponNumber"));
						eticketCouponStatus.setCouponStatus(rs.getString("status"));
						eticketCouponStatus.setPnr(rs.getString("pnr"));
						eticketCouponStatus.setPsudoCity(rs.getString("psudocity"));
						eticketCouponStatus.setAgentID(rs.getString("agentid"));
						eticketCouponStatus.setAgentCode(rs.getString("agentcode"));
						return eticketCouponStatus;
					}
				});
		return eticketCouponStatusList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ETicketFOP> getETicketFOP(String eticketNumber, String tnxDate, String pnrPaxIDS) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		String query = getQuery(E_TICKET_FOP);
		Object params[] = { pnrPaxIDS, tnxDate, tnxDate };

		if (log.isDebugEnabled()) {
			log.debug("QUERY is " + query);
		}
		List<ETicketFOP> eticketFOP = (List<ETicketFOP>) template.query(query, params, new RowMapper() {

			@Override
			public ETicketFOP mapRow(ResultSet rs, int rowNum) throws SQLException {
				ETicketFOP eticketFOP = new ETicketFOP();
				eticketFOP.setFopDescription(rs.getString("fopdescription"));
				eticketFOP.setTnxAmount(rs.getBigDecimal("tnxamount"));
				return eticketFOP;
			}
		});
		return eticketFOP;
	}

	@Override
	public String getNextIATAEMDSSQID(String sequenceName) {
		log.debug("Inside getNextIATAEMDSSQID");

		if (sequenceName != null && !sequenceName.isEmpty()) {

			JdbcTemplate jt = new JdbcTemplate(getDataSource());
			String sql = " SELECT " + sequenceName + ".NEXTVAL EMDS FROM DUAL ";

			log.debug("############################################");
			log.debug(" SQL to excute            : " + sql);
			log.debug("############################################");

			String emds = (String) jt.query(sql, new ResultSetExtractor() {

				public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
					String emds = null;

					if (rs != null) {
						if (rs.next()) {
							emds = BeanUtils.nullHandler(rs.getString("EMDS"));
						}
					}
					return emds;
				}
			});

			log.debug("Exit getNextIATAEMDSSQID");
			return emds;
		}
		return null;

	}

	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<BSPTransactionTaxData> getBSPPaxTnxTaxData(String pnrPaxId) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		String query = getQuery(BSP_PAX_TNX_TAX_DATA);
		Object params[] = { pnrPaxId };

		if (log.isDebugEnabled()) {
			log.debug("QUERY is " + query);
		}

		ArrayList<BSPTransactionTaxData> paxTaxCollection = (ArrayList<BSPTransactionTaxData>) template.query(query, params,
				new ResultSetExtractor() {

					@Override
					public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
						ArrayList<BSPTransactionTaxData> results = new ArrayList<BSPTransactionTaxData>();

						while (rs.next()) {
							BSPTransactionTaxData taxInfoDTO = new BSPTransactionTaxData();
							taxInfoDTO.setTaxCode(rs.getString("taxcode"));
							taxInfoDTO.setCurrancyCode(AppSysParamsUtil.getBaseCurrency());
							taxInfoDTO.setAmount(rs.getString("amount"));
							taxInfoDTO.setOndCode(rs.getString("ond_code"));
							taxInfoDTO.setCountryCode(rs.getString("country_code"));
							taxInfoDTO.setDepatureAirportLocalCurrency(rs.getString("currency_code"));
							results.add(taxInfoDTO);
						}

						return results;
					}
				});

		return paxTaxCollection;
	}

	@Override
	public String getCountryEMDSSequnceName(String countryCode) {
		log.debug("Inside getCountryEMDSSequnceName");
		JdbcTemplate jt = new JdbcTemplate(getDataSource());
		String sql = "SELECT TSN.BSP_TKT_SEQ_NAME  FROM T_E_TICKET_SEQUENCE_NAME TSN " + "WHERE TSN.COUNTRY_CODE = '"
				+ countryCode + "' AND TSN.TICKET_TYPE= '" + TicketGeneratorCriteriaType.BSP_EMDS.getCode() + "'";

		log.debug("############################################");
		log.debug(" SQL to excute            : " + sql);
		log.debug("############################################");

		String eTicketSequnceName = (String) jt.query(sql, new ResultSetExtractor() {

			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				String emdsSequnceName = null;

				if (rs != null) {
					if (rs.next()) {
						emdsSequnceName = BeanUtils.nullHandler(rs.getString("BSP_TKT_SEQ_NAME"));
					}
				}

				return emdsSequnceName;
			}
		});
		log.debug("Exit getCountryEMDSSequnceName");
		return eTicketSequnceName;

	}

	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<ETLRCoupon> getETKTCoupon(String eTicketNUmber) {
		JdbcTemplate template = new JdbcTemplate(getDataSource());
		String query = getQuery(E_TICKET_COUPON);
		Object params[] = { eTicketNUmber };

		if (log.isDebugEnabled()) {
			log.debug("QUERY is " + query);
		}

		ArrayList<ETLRCoupon> paxTaxCollection = (ArrayList<ETLRCoupon>) template.query(query, params, new ResultSetExtractor() {

			@Override
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				ArrayList<ETLRCoupon> results = new ArrayList<ETLRCoupon>();

				while (rs.next()) {
					ETLRCoupon etktCoupon = new ETLRCoupon();
					etktCoupon.setCouponNumber(rs.getString("couponnumber"));
					etktCoupon.setModifiedDate(rs.getDate("dateStausChange"));
					etktCoupon.setDepatureDate(rs.getDate("depatureDate"));
					etktCoupon.setCouponStatus(rs.getString("status"));
					etktCoupon.setSegmentCode(rs.getString("segmentCode"));
					etktCoupon.setFlightNumber(rs.getString("fltnumber"));
					etktCoupon.setBagWeight(rs.getString("bagweight"));

					results.add(etktCoupon);
				}

				return results;
			}
		});

		return paxTaxCollection;
	}

	@Override
	public List<BSPTnx> getBSPTransactionForHotFile(Integer txnID, String lccUniqueTxnID, Collection<Integer> tnxNominalCodes) {
		String query = "";
		Object[] params;

		StringBuilder nominalCodesBuilder = new StringBuilder();
		int noOfTnxTypes = tnxNominalCodes.size();
		int counter = 0;
		for (Integer tnxNomincalCode : tnxNominalCodes) {
			counter++;
			nominalCodesBuilder.append(tnxNomincalCode);
			if (counter != noOfTnxTypes) {
				nominalCodesBuilder.append(",");
			}
		}
		String tnxNominalCodesString = nominalCodesBuilder.toString();

		if (lccUniqueTxnID != null) {
			String[] args = { tnxNominalCodesString, tnxNominalCodesString };
			query = getQuery(BSP_LCC_TXN_DETAILS_FOR_HOT_FILE_PROCESS, args);
			params = new Object[] { AppSysParamsUtil.getDefaultCarrierCode(), lccUniqueTxnID, lccUniqueTxnID };
		} else {
			String[] args = { tnxNominalCodesString };
			query = getQuery(BSP_OWN_TXN_DETAILS_FOR_HOT_FILE_PROCESS, args);
			params = new Object[] { txnID };
		}

		JdbcTemplate template = new JdbcTemplate(getDataSource());

		if (log.isDebugEnabled()) {
			log.debug("QUERY is " + query);
		}

		List<BSPTnx> tnxList = (List<BSPTnx>) template.query(query, params, new RowMapper() {

			@Override
			public BSPTnx mapRow(ResultSet rs, int rowNum) throws SQLException {
				BSPTnx bspTnx = new BSPTnx();

				bspTnx.setAmount(rs.getBigDecimal("amount"));
				bspTnx.setLccUniqueTnxID(rs.getString("lcc_unique_txn_id"));
				bspTnx.setNominalCode(rs.getInt("nominal_code"));
				bspTnx.setPaxSequence(rs.getInt("pax_sequence"));
				bspTnx.setPayCurrencyAmount(rs.getBigDecimal("amount_paycur"));
				bspTnx.setPayCurrencyCode(rs.getString("payment_currency_code"));
				bspTnx.setPnrPaxID(rs.getInt("pnr_pax_id"));
				bspTnx.setTnxType(rs.getString("dr_cr"));
				bspTnx.setAgentCode(rs.getString("agent_code"));
				bspTnx.setIataAgentCode(rs.getString("iata_agent_code"));
				return bspTnx;
			}
		});
		return tnxList;
	}

	/**
	 * Fetch data from internal core table froor transfer BSP sales summary to external system(X3) Implementation for
	 * getBSPsForAgents(Date fromDate, Date toDate)returns Collection of BSPResultsDTO Objects for a give fromDate and
	 * toDate
	 * 
	 * @param fromDate
	 * @param toDate
	 * @throws CommonsDataAccessException
	 */
	@SuppressWarnings("unchecked")
	public Collection getBSPsToTransfer(Date fromDate, Date toDate) {
		if (fromDate == null || toDate == null) {
			throw new CommonsDataAccessException("bsp.param.nullempty", InvoicingConstants.MODULE_NAME);
		}
		Object params[] = { fromDate, toDate };
		DataSource ds = InvoicingModuleUtils.getDatasource();
		JdbcTemplate templete = new JdbcTemplate(ds);
		String queryString = " select  sum(ta.amount) as amount,sum(ta.amount_local) as amount_local,ta.currency_code,"
				+ " ta.agent_code,c.country_code as country, ag.agent_iata_number as iata_agent_code "
				+ " from t_agent_transaction ta  join t_agent ag on ta.agent_code = ag.agent_code   "
				+ "join t_station s on ag.station_code = s.station_code "
				+ "   join t_country c on s.country_code = c.country_code" + " where ta.product_type = 'P' "
				+ " and ta.nominal_code in (" + AgentTransactionNominalCode.BSP_ACC_SALE + ", "
				+ AgentTransactionNominalCode.BSP_REFUND + ")" + " and ta.tnx_date between ? and ?"
				+ " and ag.service_channel_code in ('AA', 'AH')" + " and ag.airline_code = '"
				+ AppSysParamsUtil.getDefaultAirlineIdentifierCode() + "' GROUP BY ta.agent_code, ag.agent_iata_number,"
				+ " ta.currency_code, c.country_code";

		Object col = templete.query(queryString, params, new ResultSetExtractor() {
			public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
				Collection<BSPResultsDTO> colInvoiceDTOs = new ArrayList<BSPResultsDTO>();
				while (rs.next()) {
					BSPResultsDTO bspDTO = new BSPResultsDTO();
					bspDTO.setSalesPeriodFrom(fromDate);
					bspDTO.setSalesPeriodTo(toDate);
					bspDTO.setAgentCode(rs.getString("agent_code"));
					bspDTO.setPaymentAmount(rs.getBigDecimal("amount"));
					bspDTO.setPaymentLocalAmount(rs.getBigDecimal("amount_local"));
					bspDTO.setPaymentCurrency(rs.getString("currency_code"));
					bspDTO.setAgentIATACode(rs.getString("iata_agent_code"));
					bspDTO.setCountry(rs.getString("country"));
					colInvoiceDTOs.add(bspDTO);
				}
				return colInvoiceDTOs;
			}
		});
		return (Collection) col;
	}

	/**
	 * Accepts Collection of BSPsDTO collection and inserts to Internal summary table : T_BSP_SALES
	 * 
	 * @param fromDate
	 * @param toDate
	 * @param bspCollection
	 * @param transferStatus
	 * @throws CommonsDataAccessException
	 */
	public void insertBSPsToInternalSummaryTable(Date fromDate, Date toDate, Collection bspCollection, String transferStatus) {
		if (bspCollection == null || bspCollection.size() == 0) {
			throw new CommonsDataAccessException("airtravelagent.param.nullempty", AirTravelAgentConstants.MODULE_NAME);
		}
		DataSource ds = InvoicingModuleUtils.getDatasource();
		Iterator it = bspCollection.iterator();
		// common sql queries
		String insertStatment = "insert into  T_BSP_SALES  values (?,?,?,?,?,?,?,?,?,?,?,?)";
		String selectStatment = "select max(BSP_SALES_ID) as MAXIMUM from T_BSP_SALES ";
		String deleteStatement = "delete from  T_BSP_SALES where SALES_PERIOD_FROM = ? and SALES_PERIOD_TO = ? and AGENT_CODE = ?";
		PreparedStatement insertStmt = null;
		PreparedStatement selectStmt = null;
		PreparedStatement deleteStmt = null;
		Connection connection = null;
		ResultSet res = null;
		java.sql.Date currentTimeStamp = new java.sql.Date(new Date().getTime());
		// create the prepared statements
		try {
			connection = ds.getConnection();
			insertStmt = connection.prepareStatement(insertStatment);
			selectStmt = connection.prepareStatement(selectStatment);
			deleteStmt = connection.prepareStatement(deleteStatement);
			Integer value = 0;
			// Iterate through the BSPs List
			while (it.hasNext()) {
				BSPResultsDTO bsp = (BSPResultsDTO) it.next();
				// Delete If already In Interface Table
				java.sql.Date salesPeriodFromDate = new java.sql.Date(fromDate.getTime());
				java.sql.Date salesPeriodToDate = new java.sql.Date(toDate.getTime());

				deleteStmt.setDate(1, salesPeriodFromDate);
				deleteStmt.setDate(2, salesPeriodToDate);
				deleteStmt.setString(3, bsp.getAgentCode());
				deleteStmt.executeUpdate();
				res = selectStmt.executeQuery();
				while (res.next()) {
					value = res.getInt("MAXIMUM");
					if (value == null) {
						value = 0;
					}
					bsp.setSalesID(value + 1);
				}
				// set params to insert statment
				insertStmt.setInt(1, value + 1);
				insertStmt.setDate(2, salesPeriodFromDate);
				insertStmt.setDate(3, salesPeriodToDate);
				insertStmt.setString(4, bsp.getAgentCode());
				insertStmt.setString(5, bsp.getAgentIATACode());
				insertStmt.setString(6, bsp.getCountry());
				insertStmt.setDouble(7, bsp.getPaymentAmount().doubleValue());
				insertStmt.setDouble(8, bsp.getPaymentLocalAmount().doubleValue());
				insertStmt.setString(9, bsp.getPaymentCurrency());
				insertStmt.setString(10, transferStatus);
				insertStmt.setDate(11, currentTimeStamp);
				insertStmt.setDouble(12, bsp.getVersion());
				insertStmt.executeUpdate();
			}
		} catch (SQLException e) {
			log.error("Error when getting prepared statment" + e);
			throw new CommonsDataAccessException(e, "airtravelagent.bsptransfer.error", AirTravelAgentConstants.MODULE_NAME);
		} catch (Exception e) {
			log.error("failed when writing to iternal summary", e);
			throw new CommonsDataAccessException(e, "airtravelagent.bsptransfer.error", AirTravelAgentConstants.MODULE_NAME);
		} finally {
			try {
				connection.close();
				insertStmt.close();
				selectStmt.close();
				deleteStmt.close();
				res.close();
			} catch (SQLException e) {
				throw new CommonsDataAccessException(e, "airtravelagent.resourceClose.error", AirTravelAgentConstants.MODULE_NAME);
			}
		}
	}

	/**
	 * Method used to update BSP transfer status in T_BSP_SALES
	 * 
	 * @param transferStatus
	 * @param collection
	 */
	public void updateBSPStatus(String transferStatus, Collection collection) {
		try {
			DataSource ds = InvoicingModuleUtils.getDatasource();
			JdbcTemplate templete = new JdbcTemplate(ds);
			int value = 0;
			StringBuilder builder = new StringBuilder("update T_BSP_SALES set TRANSFER_STATUS=? where BSP_SALES_ID in ( ");
			if (collection != null && collection.size() > 0) {
				Iterator iterator = collection.iterator();
				while (iterator.hasNext()) {
					value = (int) iterator.next();
					builder.append(value + ",");
				}
				builder.deleteCharAt(builder.length() - 1);
				builder.append(")");
			}
			Object params[] = { transferStatus };
			String sql = builder.toString();
			templete.update(sql, params);
		} catch (Exception e) {
			log.error("failed when updating status to  iternal summary", e);
			throw new CommonsDataAccessException(e, "airtravelagent.updateBSPStatus.error", AirTravelAgentConstants.MODULE_NAME);
		}
	}

	/**
	 * Accepts Collection of BSPsDTO collection and inserts to SQL SERVER Interface table : INT_T_BSP_SALES
	 * 
	 * @param bspCollection
	 * @throws CommonsDataAccessException
	 */
	public void insertBSPsToInterfaceTable(Collection bspCollection) {
		if (bspCollection == null || bspCollection.size() == 0) {
			throw new CommonsDataAccessException("airtravelagent.param.nullempty", AirTravelAgentConstants.MODULE_NAME);
		}
		Connection connection = BLUtil.getExternalDBConnectionUtilForX3().getExternalConnection();
		Iterator it = bspCollection.iterator();
		// common sql queries
		String insertStatment = "insert into  INT_T_BSP_SALES (BSP_SALES_ID,SALES_PERIOD_FROM,SALES_PERIOD_TO,AGENT_CODE,AGENT_IATA_CODE,AGENT_COUNTRY_CODE,AMOUNT,AMOUNT_LOCAL,CURRENCY_CODE) values (?,?,?,?,?,?,?,?,?)";
		String deleteStatement = "delete from  INT_T_BSP_SALES where SALES_PERIOD_FROM = ? and SALES_PERIOD_TO = ? and AGENT_CODE = ?";
		PreparedStatement insertStmt = null;
		PreparedStatement deleteStmt = null;
		// create the prepared statements
		try {
			insertStmt = connection.prepareStatement(insertStatment);
			deleteStmt = connection.prepareStatement(deleteStatement);
			connection.setAutoCommit(false);
		} catch (SQLException e) {
			log.error("Error when getting prepared statment" + e);
			throw new CommonsDataAccessException(e, "airtravelagent.prepstatment.error", AirTravelAgentConstants.MODULE_NAME);
		}
		try {
			// Iterate through the BSPs List
			while (it.hasNext()) {
				BSPResultsDTO bsp = (BSPResultsDTO) it.next();
				Date from = bsp.getSalesPeriodFrom();
				Date to = bsp.getSalesPeriodTo();
				java.sql.Date salesPeriodFromDate = new java.sql.Date(from.getTime());
				java.sql.Date salesPeriodToDate = new java.sql.Date(to.getTime());
				// Delete If already In Summary Table
				deleteStmt.setDate(1, salesPeriodFromDate);
				deleteStmt.setDate(2, salesPeriodToDate);
				deleteStmt.setString(3, bsp.getAgentCode());
				deleteStmt.executeUpdate();
				// end of delete
				// set params to insert statment
				insertStmt.setInt(1, bsp.getSalesID());
				insertStmt.setDate(2, salesPeriodFromDate);
				insertStmt.setDate(3, salesPeriodToDate);
				insertStmt.setString(4, bsp.getAgentCode());
				insertStmt.setString(5, bsp.getAgentIATACode());
				insertStmt.setString(6, bsp.getCountry());
				insertStmt.setDouble(7, bsp.getPaymentAmount().doubleValue());
				insertStmt.setDouble(8, bsp.getPaymentLocalAmount().doubleValue());
				insertStmt.setString(9, bsp.getPaymentCurrency());
				insertStmt.executeUpdate();
			}
		} catch (Exception e) {
			log.error("failed when transfering BSP to External", e);
			XDBConnectionUtil.rollback(connection);
			throw new CommonsDataAccessException(e, "airtravelagent.invoicetransfer.error", AirTravelAgentConstants.MODULE_NAME);
		}
		XDBConnectionUtil.commit(connection);
	}
}
