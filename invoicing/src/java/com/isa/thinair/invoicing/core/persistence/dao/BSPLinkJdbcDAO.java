/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.invoicing.core.persistence.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.isa.thinair.invoicing.api.model.bsp.BSPAgentDataDTO;
import com.isa.thinair.invoicing.api.model.bsp.BSPReconciliationStatus;
import com.isa.thinair.invoicing.api.model.bsp.BSPSegmentInfoDTO;
import com.isa.thinair.invoicing.api.model.bsp.BSPTnx;
import com.isa.thinair.invoicing.api.model.bsp.BSPTransactionTaxData;
import com.isa.thinair.invoicing.api.model.bsp.ETLRCoupon;
import com.isa.thinair.invoicing.api.model.bsp.ETLRCouponRec;
import com.isa.thinair.invoicing.api.model.bsp.ETicketCouponStatus;
import com.isa.thinair.invoicing.api.model.bsp.ETicketFOP;

public interface BSPLinkJdbcDAO {
	public List<BSPTnx> getAgentAllBSPTransactions(String agentCode, Date fromDate, Date toDate,
			Collection<Integer> nomincalCodes, String dataSourceType);

	public List<BSPTnx> getExternalAgentBSPTransactions(String agentCode, Date fromDate, Date toDate,
			Collection<Integer> nomincalCodes, String dataSourceType);

	public List<BSPTnx> getExternalBSPTransactions(String bspCountry, Date fromDate, Date toDate,
			Collection<Integer> nominalCodes, String dataSourceType, String entity);

	public BSPAgentDataDTO getBSPAgent(String countryCode, String agentCode);

	public List<BSPAgentDataDTO> getBSPEnabledAgents(String countryCode);

	public List<BSPAgentDataDTO> getAgentsForRETFiles();

	public List<BSPSegmentInfoDTO> getSegmentInformation(int pnrPaxId);

	public BSPTnx getOriginalLCCTransaction(Integer originalTnxID, int paxSequnce);

	public BSPTnx getOriginalOwnTransaction(Integer originalTnxID, int paxSequnce);

	public List<BSPTnx> getAgentErrorFixedBSPTransactions(String agentCode);

	public List<BSPTnx> getExternalBSPAgentErrorFixedTransactions(String agentCode);

	public String getAgentCode(String iatanumber);

	public List<BSPTnx> getAllBSPTransactionsForReconciliation(String bspCountry, Date fromDate, Date toDate,
			Collection<Integer> nominalCodes, String dataSourceType, String entity);

	public List<BSPReconciliationStatus> getReconciliationInformation(List<Integer> ownTnxIDs, List<String> lccUniqueIDs,
			String dataSourceType, List<String> extReferences);

	public String getNextIATAEMDSSQID(String sequenceName);

	public String getCountryEMDSSequnceName(String countryCode);

	public Collection<BSPAgentDataDTO> getCTOandATOAgents(String types);

	public List<ETLRCouponRec> getETLRCouponRec(Date startDate, Date endDate);

	public List<ETicketCouponStatus> getETicketCouponStatus(String eticketNumber);

	public List<ETicketFOP> getETicketFOP(String eticketNumber, String tnxDate, String pnrPaxIDS);

	public ArrayList<BSPTransactionTaxData> getBSPPaxTnxTaxData(String pnrPaxId);

	public ArrayList<ETLRCoupon> getETKTCoupon(String eTicketNUmber);

	public List<BSPTnx> getBSPTransactionForHotFile(Integer txnID, String lccUniqueTxnID, Collection<Integer> tnxNominalCodes);

	public void insertBSPsToInterfaceTable(Collection bspsToTransfer);

	public void insertBSPsToInternalSummaryTable(Date startDate, Date endDate, Collection bSPsales, String inProgress);

	public Collection getBSPsToTransfer(Date startDate, Date endDate);

	public void updateBSPStatus(String transferedSuccessfuly, Collection<Integer> bspSalesIds);
}
