/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.invoicing.core.persistence.dao;

import java.util.Collection;
import java.util.Date;

/**
 * @author Chamindap
 * 
 */
public interface TransferInvoiceDAO {

	public void updateInvoiceTransferStatus(String status, Collection invoiceNumbers);

	public void updateEmailsStatus(String status, Collection invoiceNumbers);

	public void updateInvoiceStatus(Date from, Collection agentCodes, String transferStatus);

	public Collection getInvoicesForAgents(Date fromDate, Collection agentCodes, String system);
}
