package com.isa.thinair.invoicing.core.util;

import com.isa.thinair.invoicing.api.service.InvoicingModuleUtils;
import com.isa.thinair.invoicing.core.config.InvoicingConfig;
import com.isa.thinair.login.util.ForceLoginInvoker;

public class CredentialInvokerUtil {

	public static void invokeCredentials() {
		InvoicingConfig invoicingConfig = InvoicingModuleUtils.getInvoicingConfig();

		if (invoicingConfig.isJmsEar()) {
			String userName = invoicingConfig.getUserName();
			String password = invoicingConfig.getPassword();
			ForceLoginInvoker.login(userName, password);
		}
	}
	
	public static void close() {
		InvoicingConfig invoicingConfig = InvoicingModuleUtils.getInvoicingConfig();

		if (invoicingConfig.isJmsEar()) {
			ForceLoginInvoker.close();
		}
	}

}
