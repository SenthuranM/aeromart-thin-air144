package com.isa.thinair.invoicing.core.util;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.util.ExchangeRateProxy;
import com.isa.thinair.airproxy.api.dto.ETicketInfoTO;
import com.isa.thinair.airproxy.api.dto.ReservationPaxSegmentPaymentTO;
import com.isa.thinair.airproxy.api.dto.SegmentChargePaymentTO;
import com.isa.thinair.airproxy.api.service.AirproxySegmentBD;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationPaxAdditionalInfo;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airreservation.api.service.PassengerBD;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airtravelagents.api.model.AgentTransactionNominalCode;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.dto.BasicTrackInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.Pair;
import com.isa.thinair.invoicing.api.model.BSPFailedTnx;
import com.isa.thinair.invoicing.api.model.BSPFileLog;
import com.isa.thinair.invoicing.api.model.BSPFileLog.RET_FILE_STATUS;
import com.isa.thinair.invoicing.api.model.DishFileTnxLogData;
import com.isa.thinair.invoicing.api.model.EMDSTransaction;
import com.isa.thinair.invoicing.api.model.bsp.BSPException;
import com.isa.thinair.invoicing.api.model.bsp.BSPReconciliationRecordDTO;
import com.isa.thinair.invoicing.api.model.bsp.BSPReconciliationStatus;
import com.isa.thinair.invoicing.api.model.bsp.BSPRejectedTnxInfo;
import com.isa.thinair.invoicing.api.model.bsp.BSPSegmentInfoDTO;
import com.isa.thinair.invoicing.api.model.bsp.BSPTnx;
import com.isa.thinair.invoicing.api.model.bsp.BSPTransactionDataDTO;
import com.isa.thinair.invoicing.api.model.bsp.BSPTransactionDataDTO.TaxCodes;
import com.isa.thinair.invoicing.api.model.bsp.BSPTransactionDataDTO.TransactionType;
import com.isa.thinair.invoicing.api.model.bsp.BSPTransactionTaxData;
import com.isa.thinair.invoicing.api.service.InvoicingModuleUtils;
import com.isa.thinair.invoicing.api.util.InvoicingInternalConstants.BSPErrorLogStatus;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;

public class BSPCoreUtils {

	private static Log log = LogFactory.getLog(BSPCoreUtils.class);

	private enum ChargeGroupCode {
		FAR, SUR, TAX, INF, MOD
	};

	private static final int FRCA_MAX_LENGTH = 87;
	private static final String FARE_CALC_AREA_SEPERATOR = " ";
	private static final String STR_EROOR = "ERROR";
	
	private static final String DEFAULT_EXT_PAX_NAME = "HOLIDAY PAX";

	public static List<BSPTransactionDataDTO> composeBSPTransactionDataDTOs(String countryCode, String agentCode, Date fromDate,
			Date toDate, UserPrincipal userPrincipal, boolean processErrorsOnly) throws ModuleException, BSPException {

		List<BSPTransactionDataDTO> bspTransactionDataDTOs = new ArrayList<>();
		bspTransactionDataDTOs
				.addAll(getInternalTransactions(countryCode, agentCode, fromDate, toDate, userPrincipal, processErrorsOnly));
		bspTransactionDataDTOs
				.addAll(getExternalTransactions(countryCode, agentCode, fromDate, toDate, userPrincipal, processErrorsOnly));

		return bspTransactionDataDTOs;
	}

	public static List<BSPTransactionDataDTO> getInternalTransactions(String countryCode, String agentCode, Date fromDate,
			Date toDate, UserPrincipal userPrincipal, boolean processErrorsOnly) throws ModuleException, BSPException {

		PassengerBD paxBD = InvoicingModuleUtils.getPassengerBD();
		AirproxySegmentBD airproxySegmentBD = InvoicingModuleUtils.getAirproxySegmentBD();
		List<BSPTransactionDataDTO> bspTransactionDataDTOs = new ArrayList<BSPTransactionDataDTO>();
		List<Integer> nominalCodes = new ArrayList<Integer>();
		String emdsSequenceName = InvoicingModuleUtils.getBSPLinkJdbcDao().getCountryEMDSSequnceName(countryCode);
		String emdsPrefix = AppSysParamsUtil.getBSPTicketingAirlineCode() + AppSysParamsUtil.getIATAEMDSFormCode();

		if (emdsSequenceName == null) {
			throw new ModuleException("EMD sequence not defined for country: " + countryCode);
		}
		// using a set to avoid any duplications between all tnxs and error
		// fixed tnxs
		Set<BSPTnx> bspTnxSet = new HashSet<BSPTnx>();

		if (!processErrorsOnly) {
			// getting all the BSP transactions include the own,dry and
			// interline
			List<BSPTnx> bspTnxList = InvoicingModuleUtils.getBSPLinkJdbcDao().getAgentAllBSPTransactions(agentCode, fromDate,
					toDate, ReservationTnxNominalCode.getBSPAccountTypeNominalCodes(),
					ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
			for (BSPTnx tnx : bspTnxList) {
				bspTnxSet.add(tnx);
			}
		}
		List<BSPTnx> previousErrorFixedTnxs = InvoicingModuleUtils.getBSPLinkJdbcDao()
				.getAgentErrorFixedBSPTransactions(agentCode);
		if (previousErrorFixedTnxs != null && previousErrorFixedTnxs.size() > 0) {
			for (BSPTnx tnx : previousErrorFixedTnxs) {
				bspTnxSet.add(tnx);
			}
		}

		if (bspTnxSet.isEmpty()) {
			return bspTransactionDataDTOs;
		}
		List<Integer> ownTnxIDs = new ArrayList<Integer>();
		List<String> lccUniqueIDs = new ArrayList<String>();
		List<String> lccUniqueTmpIds = new ArrayList<String>();

		/*
		 * adding bsp payment nominal code, we are not adding refund code as we will get the original transaction from
		 * refund later
		 */
		nominalCodes.add(36);

		// prepare dry/interline lcc unique ids and own tnx ids seperately to
		// get the segment payments
		for (BSPTnx tnx : bspTnxSet) {
			if (tnx.getLccUniqueTnxID() != null) {
				lccUniqueIDs.add(tnx.getLccUniqueTnxID());
			} else {
				ownTnxIDs.add(tnx.getTnxID());
			}
		}

		// Fix for Slowness in retrieving etickets for dry-interline bookings
		for (String lccUniqueID : lccUniqueIDs) {
			if (!lccUniqueTmpIds.contains(lccUniqueID)) {
				lccUniqueTmpIds.add(lccUniqueID);
			}
		}

		// getting the segment wise payments per pax for own, dry and interline
		Map<Pair<String, Integer>, Collection<ReservationPaxSegmentPaymentTO>> segmentPaymentInfo = paxBD
				.getReservationPaxSegmentPayments(ownTnxIDs, lccUniqueIDs, nominalCodes);
		// getting the handling charges to use as the commission
		Map<Pair<String, Integer>, BigDecimal> handlingCharges = paxBD.getHandlingChargesForPaxTnxs(ownTnxIDs, lccUniqueIDs);
		// getting the e ticket information required for the TKTT
		Map<Pair<String, Integer>, Set<ETicketInfoTO>> eTicketInfo = airproxySegmentBD.getETicketInformation(lccUniqueTmpIds,
				ownTnxIDs, getBasicTrackInfo(userPrincipal));

		List<BSPFailedTnx> errorTnxs = new ArrayList<BSPFailedTnx>();

		BSPTnx[] tnxArr = new BSPTnx[bspTnxSet.size()];
		Arrays.sort(bspTnxSet.toArray(tnxArr), new Comparator<BSPTnx>() {
			public int compare(BSPTnx o1, BSPTnx o2) {
				return o1.getTnxType().compareTo(o2.getTnxType());
			}
		});
		TNXLOOP: for (BSPTnx tnx : tnxArr) {
			// TODO validate if the BSP payment is previously processed
			BSPTransactionDataDTO bspTransactionDataDTO = new BSPTransactionDataDTO();

			ReservationPax pax = paxBD.getPassenger(tnx.getPnrPaxID(), false);
			bspTransactionDataDTO.setPaxName(pax.getFirstName() + " " + pax.getLastName());
			bspTransactionDataDTO.setDateOfIssue(tnx.getTnxDate());
			bspTransactionDataDTO.setPayCurrencyAmount(tnx.getPayCurrencyAmount().negate());
			bspTransactionDataDTO.setPayCurrencyCode(tnx.getPayCurrencyCode());
			bspTransactionDataDTO.setPnr(pax.getReservation().getPnr());
			bspTransactionDataDTO.setTicketingAirlineCode(AppSysParamsUtil.getBSPTicketingAirlineCode());
			bspTransactionDataDTO.setAaLCCUniqueID(tnx.getLccUniqueTnxID());
			bspTransactionDataDTO.setAaTnxID(tnx.getTnxID());
			bspTransactionDataDTO.setPaxSequnce(tnx.getPaxSequence());
			bspTransactionDataDTO.setAaAgentCode(tnx.getAgentCode());
			// Get exchange rate for the original transaction date
			CurrencyExchangeRate currencyExchangeRate = new ExchangeRateProxy(tnx.getTnxDate())
					.getCurrencyExchangeRate(tnx.getPayCurrencyCode());
			bspTransactionDataDTO.setCurrencyExchangeRate(currencyExchangeRate.getMultiplyingExchangeRate());

			if (tnx.getTnxType().equals(ReservationInternalConstants.TnxTypes.CREDIT)) {

				if (tnx.isFreshReservationPayment()) {

					List<BSPFailedTnx> errorsInFreshTnx = new ArrayList<BSPFailedTnx>();

					// create pair using the either lcc unique id or tnx id and
					// pax sequence
					Pair<String, Integer> pair = Pair.of(
							(tnx.getLccUniqueTnxID() != null) ? tnx.getLccUniqueTnxID() : Integer.toString(tnx.getTnxID()),
							tnx.getPaxSequence());
					Collection<ReservationPaxSegmentPaymentTO> segmentPayments = segmentPaymentInfo.get(pair);
					BigDecimal commission = handlingCharges.get(pair);
					boolean fallbackToEMDS = false;
					if (segmentPayments == null) {
						String error = TransactionType.TKTT.toString() + ":" + "No segment wise payments for : " + pair.getLeft()
								+ " : " + pair.getRight();
						if (log.isErrorEnabled())
							log.error(error);
						errorsInFreshTnx.add(composeErrorDTO(countryCode, agentCode, tnx, userPrincipal, error));
						fallbackToEMDS = true;
						// continue TNXLOOP;
					} else {

						List<BSPSegmentInfoDTO> paxAllSegments = InvoicingModuleUtils.getBSPLinkJdbcDao()
								.getSegmentInformation(tnx.getPnrPaxID());

						Object[] eticketAndSegments = composeBSPSegments(pair, agentCode, tnx, userPrincipal, paxAllSegments,
								segmentPayments, eTicketInfo, TransactionType.TKTT, errorsInFreshTnx, countryCode, false);

						if (eticketAndSegments[0] != null && eticketAndSegments[0] instanceof String
								&& ((String) eticketAndSegments[0]).equals(STR_EROOR)) {
							// continue TNXLOOP;
							fallbackToEMDS = true;
						} else {

							List<BSPSegmentInfoDTO> paxBSPSegments = (List<BSPSegmentInfoDTO>) eticketAndSegments[2];
							String fareCalcArea = composeFareCalculationArea(paxBSPSegments,
									currencyExchangeRate.getMultiplyingExchangeRate(), pax.getTotalFare(), segmentPayments, true);
							String[] fareCalcSplits = getFareCalculationAreasCollection(fareCalcArea, agentCode, tnx,
									userPrincipal, TransactionType.TKTT, errorsInFreshTnx, countryCode);
							if (fareCalcSplits[0] != null && fareCalcSplits[0].equals(STR_EROOR)) {
								fallbackToEMDS = true;
								// continue TNXLOOP;
							} else {
								bspTransactionDataDTO.setTnxType(TransactionType.TKTT);
								bspTransactionDataDTO.setFareCalculationAreas(fareCalcSplits);
								bspTransactionDataDTO
										.setFirstSetOfSegments((Pair<Long, List<BSPSegmentInfoDTO>>) eticketAndSegments[0]);
								bspTransactionDataDTO
										.setSecondSetOfSegments((Pair<Long, List<BSPSegmentInfoDTO>>) eticketAndSegments[1]);
								bspTransactionDataDTO.setTotalFare(getTotalFare(segmentPayments, true));
								bspTransactionDataDTO.setTaxCollection(composeTaxCollection(segmentPayments));
								bspTransactionDataDTO.setAgentCommission(commission);
							}
						}

					}

					if (fallbackToEMDS) {
						if (log.isErrorEnabled()) {
							StringBuilder errorBuilder = new StringBuilder();
							errorBuilder.append(
									TransactionType.TKTT.toString() + ": Falling back to EMD for following transactions :: ");
							errorBuilder.append(pair.getLeft() + " : " + pair.getRight());
							errorBuilder.append(":lccTxnId: " + tnx.getLccUniqueTnxID() + ",");
							errorBuilder.append("txnId:" + tnx.getTnxID() + ",error_desc:");
							for (BSPFailedTnx failedTnx : errorsInFreshTnx) {
								errorBuilder.append(failedTnx.getDescription() + "|");
							}
							log.error(errorBuilder.toString());
						}
						try {
							paxBD.markPaymentTransactionNotFirst(tnx.getPnrPaxID(), tnx.getTnxID(), tnx.getLccUniqueTnxID());
							processTxnAsEMD(emdsSequenceName, emdsPrefix, tnx, bspTransactionDataDTO);
						} catch (ModuleException e) {
							String error = TransactionType.TKTT.toString() + ":" + "EMD Fall back failed : " + pair.getLeft()
									+ " : " + pair.getRight();
							if (log.isErrorEnabled())
								log.error(error);
							errorsInFreshTnx.add(composeErrorDTO(countryCode, agentCode, tnx, userPrincipal, error));
						}
					}
				} else {
					processTxnAsEMD(emdsSequenceName, emdsPrefix, tnx, bspTransactionDataDTO);
				}
			} else {
				bspTransactionDataDTO.setTnxType(TransactionType.RFND);

				List<Integer> ownTnxIDsForRFnd = new ArrayList<Integer>();
				List<String> lccUniqueIDsForRFnd = new ArrayList<String>();

				BSPTnx originalTnx = null;
				String originalTransactionID = null;
				if (tnx.getLccUniqueTnxID() != null) {
					originalTnx = InvoicingModuleUtils.getBSPLinkJdbcDao().getOriginalLCCTransaction(tnx.getOriginalPaymentID(),
							tnx.getPaxSequence());
					if (originalTnx == null) {

						originalTnx = InvoicingModuleUtils.getBSPLinkJdbcDao()
								.getOriginalOwnTransaction(tnx.getOriginalPaymentID(), tnx.getPaxSequence());
						if (originalTnx == null) {
							String error = TransactionType.RFND.toString() + ":" + "Original Transaction not found : "
									+ tnx.getLccUniqueTnxID() + " " + tnx.getPaxSequence();
							if (log.isErrorEnabled())
								log.error(error);
							errorTnxs.add(composeErrorDTO(countryCode, agentCode, tnx, userPrincipal, error));
							continue TNXLOOP;
						} else {
							originalTransactionID = tnx.getOriginalPaymentID().toString();
							ownTnxIDsForRFnd.add(new Integer(originalTransactionID));
							bspTransactionDataDTO.setOriginalPaymentTnxDate(originalTnx.getTnxDate());
						}
					} else {
						originalTransactionID = originalTnx.getLccUniqueTnxID();
						lccUniqueIDsForRFnd.add(originalTransactionID);
						bspTransactionDataDTO.setOriginalPaymentTnxDate(originalTnx.getTnxDate());
					}
				} else {
					originalTnx = InvoicingModuleUtils.getBSPLinkJdbcDao().getOriginalOwnTransaction(tnx.getOriginalPaymentID(),
							tnx.getPaxSequence());
					if (originalTnx == null) {
						String error = TransactionType.RFND.toString() + ":" + "Original Transaction not found : "
								+ tnx.getTnxID() + " " + tnx.getPaxSequence();
						if (log.isErrorEnabled())
							log.error(error);
						errorTnxs.add(composeErrorDTO(countryCode, agentCode, tnx, userPrincipal, error));
						continue TNXLOOP;
					} else {
						originalTransactionID = tnx.getOriginalPaymentID().toString();
						ownTnxIDsForRFnd.add(new Integer(originalTransactionID));
						bspTransactionDataDTO.setOriginalPaymentTnxDate(originalTnx.getTnxDate());
					}
				}

				if (originalTnx.isFreshReservationPayment()) {

					// getting the segment wise payments per pax for own, dry
					// and interline for original refund transaction
					Map<Pair<String, Integer>, Collection<ReservationPaxSegmentPaymentTO>> segmentPaymentInfoForRfnd = paxBD
							.getReservationPaxSegmentPayments(ownTnxIDsForRFnd, lccUniqueIDsForRFnd, nominalCodes);

					// getting the handling charges to use as the commission for
					// original refund transaction
					// Map<Pair<String, Integer>, BigDecimal>
					// handlingChargesForRfnd =
					// paxBD.getHandlingChargesForPaxTnxs(
					// ownTnxIDsForRFnd, lccUniqueIDsForRFnd);

					Map<Pair<String, Integer>, Set<ETicketInfoTO>> eTicketInfoForRfnd = airproxySegmentBD
							.getETicketInformation(lccUniqueIDsForRFnd, ownTnxIDsForRFnd, getBasicTrackInfo(userPrincipal));

					Pair<String, Integer> pair = Pair.of(originalTransactionID, tnx.getPaxSequence());
					Collection<ReservationPaxSegmentPaymentTO> segmentPayments = segmentPaymentInfoForRfnd.get(pair);
					// BigDecimal commission = handlingChargesForRfnd.get(pair);
					if (segmentPayments == null) {
						String error = TransactionType.RFND.toString() + ":" + "No segment wise payments for : " + pair.getLeft()
								+ " : " + pair.getRight();
						if (log.isErrorEnabled())
							log.error(error);
						errorTnxs.add(composeErrorDTO(countryCode, agentCode, tnx, userPrincipal, error));
						continue TNXLOOP;
					}
					// bspTransactionDataDTO.setTaxCollection(composeTaxCollection(segmentPayments));
					// bspTransactionDataDTO.setAgentCommission(commission);

					List<BSPSegmentInfoDTO> paxAllSegments = InvoicingModuleUtils.getBSPLinkJdbcDao()
							.getSegmentInformation(tnx.getPnrPaxID());
					Object[] eticketAndSegments = composeBSPSegments(pair, agentCode, tnx, userPrincipal, paxAllSegments,
							segmentPayments, eTicketInfoForRfnd, TransactionType.RFND, errorTnxs, countryCode, false);
					if (eticketAndSegments[0] != null && eticketAndSegments[0] instanceof String
							&& ((String) eticketAndSegments[0]).equals(STR_EROOR)) {
						continue TNXLOOP;
					} else {
						bspTransactionDataDTO.setFirstSetOfSegments((Pair<Long, List<BSPSegmentInfoDTO>>) eticketAndSegments[0]);
						bspTransactionDataDTO.setSecondSetOfSegments((Pair<Long, List<BSPSegmentInfoDTO>>) eticketAndSegments[1]);
					}
				} else {

					EMDSTransaction originalEMDSTransaction = InvoicingModuleUtils.getBSPLinkDao().getEMDSTransction(
							originalTnx.getTnxID(), originalTnx.getLccUniqueTnxID(), originalTnx.getPaxSequence(), null);
					if (originalEMDSTransaction != null) {
						bspTransactionDataDTO.setOriginalEMDSTransaction(originalEMDSTransaction);
					} else {
						String error = TransactionType.RFND.toString() + ": No original EMDS Trasnaction found:tnxID="
								+ BeanUtils.nullHandler(originalTnx.getTnxID()) + "lccUniqueTnxID="
								+ BeanUtils.nullHandler(originalTnx.getLccUniqueTnxID());
						if (log.isErrorEnabled())
							log.error(error);
						errorTnxs.add(composeErrorDTO(countryCode, agentCode, tnx, userPrincipal, error));
						continue TNXLOOP;
					}
				}
			}
			bspTransactionDataDTOs.add(bspTransactionDataDTO);
		}
		// updating previously fixed errors
		if (previousErrorFixedTnxs != null && previousErrorFixedTnxs.size() > 0) {
			updatePreviouslyFixedErrors(previousErrorFixedTnxs, userPrincipal);
		}
		// recording errors
		if (errorTnxs.size() > 0) {
			recordBSPFailedErrors(errorTnxs);
		}

		return bspTransactionDataDTOs;
	}

	private static void processTxnAsEMD(String emdsSequenceName, String emdsPrefix, BSPTnx tnx,
			BSPTransactionDataDTO bspTransactionDataDTO) {
		EMDSTransaction emdsTransaction = InvoicingModuleUtils.getBSPLinkDao().getEMDSTransction(tnx.getTnxID(),
				tnx.getLccUniqueTnxID(), tnx.getPaxSequence(), null);

		if (emdsTransaction == null) {
			String emdsTicketNo = emdsPrefix + InvoicingModuleUtils.getBSPLinkJdbcDao().getNextIATAEMDSSQID(emdsSequenceName);
			// Generate EMDS transaction and set information
			emdsTransaction = composeEMDSTransaction(tnx, emdsTicketNo);
			InvoicingModuleUtils.getBSPLinkDao().saveOrUpdateEMDSTransaction(emdsTransaction);
		}
		bspTransactionDataDTO.setTnxType(TransactionType.EMDS);
		bspTransactionDataDTO.setEmdsTransaction(emdsTransaction);
	}

	public static List<BSPTransactionDataDTO> getExternalTransactions(String countryCode, String agentCode, Date fromDate,
			Date toDate, UserPrincipal userPrincipal, boolean processErrorsOnly) throws ModuleException, BSPException {

		List<BSPTransactionDataDTO> bspTransactionDataDTOs = new ArrayList<BSPTransactionDataDTO>();
		BSPTransactionDataDTO bspTransactionDataDTO;
		EMDSTransaction emdsTransaction;

		String emdsSequenceName = InvoicingModuleUtils.getBSPLinkJdbcDao().getCountryEMDSSequnceName(countryCode);
		String emdsPrefix = AppSysParamsUtil.getBSPTicketingAirlineCode() + AppSysParamsUtil.getIATAEMDSFormCode();
		if (emdsSequenceName == null) {
			throw new ModuleException("EMD sequence not defined for country: " + countryCode);
		}

		if (!processErrorsOnly) {
			List<BSPTnx> bspTnxList = InvoicingModuleUtils.getBSPLinkJdbcDao().getExternalAgentBSPTransactions(agentCode,
					fromDate, toDate, AgentTransactionNominalCode.getExternalProductBSPAccountTypeNominalCodes(),
					ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);

			for (BSPTnx bspTnx : bspTnxList) {

				bspTransactionDataDTO = new BSPTransactionDataDTO();

				String emdsTicketNo = emdsPrefix + InvoicingModuleUtils.getBSPLinkJdbcDao().getNextIATAEMDSSQID(emdsSequenceName);
				// Generate EMDS transaction and set information
				emdsTransaction = composeEMDSTransaction(bspTnx, emdsTicketNo);
				InvoicingModuleUtils.getBSPLinkDao().saveOrUpdateEMDSTransaction(emdsTransaction);

				bspTransactionDataDTO.setTnxType(TransactionType.EMDS);
				bspTransactionDataDTO.setPayCurrencyCode(bspTnx.getPayCurrencyCode());
				bspTransactionDataDTO.setPayCurrencyAmount(bspTnx.getPayCurrencyAmount());
				bspTransactionDataDTO.setDateOfIssue(bspTnx.getTnxDate());
				bspTransactionDataDTO.setTicketingAirlineCode(AppSysParamsUtil.getBSPTicketingAirlineCode());
				bspTransactionDataDTO.setAaAgentCode(bspTnx.getAgentCode());
				// bspTransactionDataDTO.setPaymentDesc(bspTnx.getNomialDesc());
				bspTransactionDataDTO.setEmdsTransaction(emdsTransaction);
				bspTransactionDataDTO.setExternalReference(bspTnx.getExternalReference());
				bspTransactionDataDTO.setProductType(bspTnx.getProductType());
								
				if (bspTnx.getPaxName() == null || "".equals(bspTnx.getPaxName().trim())) {
					bspTransactionDataDTO.setPaxName(DEFAULT_EXT_PAX_NAME);
				} else {
					bspTransactionDataDTO.setPaxName(bspTnx.getPaxName());
				}
				

				bspTransactionDataDTOs.add(bspTransactionDataDTO);

			}
		}

		List<BSPTnx> previousErrorFixedTnxs = InvoicingModuleUtils.getBSPLinkJdbcDao()
				.getExternalBSPAgentErrorFixedTransactions(agentCode);
		if (previousErrorFixedTnxs != null && previousErrorFixedTnxs.size() > 0) {
			for (BSPTnx bspTnx : previousErrorFixedTnxs) {
				bspTransactionDataDTO = new BSPTransactionDataDTO();

				String emdsTicketNo = emdsPrefix + InvoicingModuleUtils.getBSPLinkJdbcDao().getNextIATAEMDSSQID(emdsSequenceName);
				// Generate EMDS transaction and set information
				emdsTransaction = composeEMDSTransaction(bspTnx, emdsTicketNo);
				InvoicingModuleUtils.getBSPLinkDao().saveOrUpdateEMDSTransaction(emdsTransaction);

				bspTransactionDataDTO.setTnxType(TransactionType.EMDS);
				bspTransactionDataDTO.setPayCurrencyCode(bspTnx.getPayCurrencyCode());
				bspTransactionDataDTO.setPayCurrencyAmount(bspTnx.getPayCurrencyAmount());
				bspTransactionDataDTO.setDateOfIssue(bspTnx.getTnxDate());
				bspTransactionDataDTO.setTicketingAirlineCode(AppSysParamsUtil.getBSPTicketingAirlineCode());
				bspTransactionDataDTO.setAaAgentCode(bspTnx.getAgentCode());
				// bspTransactionDataDTO.setPaymentDesc(bspTnx.getNomialDesc());
				bspTransactionDataDTO.setEmdsTransaction(emdsTransaction);
				bspTransactionDataDTO.setExternalReference(bspTnx.getExternalReference());
				if (bspTnx.getPaxName() == null || "".equals(bspTnx.getPaxName().trim())) {
					bspTransactionDataDTO.setPaxName(DEFAULT_EXT_PAX_NAME);
				} else {
					bspTransactionDataDTO.setPaxName(bspTnx.getPaxName());
				}

				bspTransactionDataDTOs.add(bspTransactionDataDTO);
			}
		}

		return bspTransactionDataDTOs;
	}

	public static List<BSPTransactionDataDTO> composeRetTransactionDataDTOs(String countryCode, String agentCode, Date fromDate,
			Date toDate, UserPrincipal userPrincipal, boolean processErrorsOnly, boolean isNomialCodeFromConfig,
			ExchangeRateProxy exRateProxy) throws ModuleException, BSPException {

		PassengerBD paxBD = InvoicingModuleUtils.getPassengerBD();
		AirproxySegmentBD airproxySegmentBD = InvoicingModuleUtils.getAirproxySegmentBD();
		List<BSPTransactionDataDTO> bspTransactionDataDTOs = new ArrayList<BSPTransactionDataDTO>();
		List<Integer> nominalCodes = new ArrayList<Integer>();

		// using a set to avoid any duplications between all tnxs and error
		// fixed tnxs
		Set<BSPTnx> bspTnxSet = new HashSet<BSPTnx>();
		if (isNomialCodeFromConfig) {
			String nomialCodes = AppSysParamsUtil.getNomialCodesSupportsForReporting();
			String[] nCodes = nomialCodes.split(",");
			for (int i = 0; i < nCodes.length; i++) {
				nominalCodes.add(Integer.parseInt(nCodes[i]));
			}
		}

		if (!processErrorsOnly) {
			// getting all the BSP transactions include the own,dry and
			// interline
			if (isNomialCodeFromConfig) {
				List<BSPTnx> bspTnxList = InvoicingModuleUtils.getBSPLinkJdbcDao().getAgentAllBSPTransactions(agentCode, fromDate,
						toDate, nominalCodes, ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				for (BSPTnx tnx : bspTnxList) {
					bspTnxSet.add(tnx);
				}
			} else {
				List<BSPTnx> bspTnxList = InvoicingModuleUtils.getBSPLinkJdbcDao().getAgentAllBSPTransactions(agentCode, fromDate,
						toDate, ReservationTnxNominalCode.getBSPAccountTypeNominalCodes(),
						ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				for (BSPTnx tnx : bspTnxList) {
					bspTnxSet.add(tnx);
				}
			}
		}
		List<BSPTnx> previousErrorFixedTnxs = InvoicingModuleUtils.getBSPLinkJdbcDao()
				.getAgentErrorFixedBSPTransactions(agentCode);
		if (previousErrorFixedTnxs != null && previousErrorFixedTnxs.size() > 0) {
			for (BSPTnx tnx : previousErrorFixedTnxs) {
				bspTnxSet.add(tnx);
			}
		}
		List<Integer> ownTnxIDs = new ArrayList<Integer>();
		List<String> lccUniqueIDs = new ArrayList<String>();

		/*
		 * adding bsp payment nominal code, we are not adding refund code as we will get the original transaction from
		 * refund later
		 */
		if (!isNomialCodeFromConfig)
			nominalCodes.add(36);

		// prepare dry/interline lcc unique ids and own tnx ids seperately to
		// get the segment payments
		for (BSPTnx tnx : bspTnxSet) {
			if (tnx.getLccUniqueTnxID() != null) {
				lccUniqueIDs.add(tnx.getLccUniqueTnxID());
			} else {
				ownTnxIDs.add(tnx.getTnxID());
			}
		}

		// getting the segment wise payments per pax for own, dry and interline
		Map<Pair<String, Integer>, Collection<ReservationPaxSegmentPaymentTO>> segmentPaymentInfo = paxBD
				.getReservationPaxSegmentPayments(ownTnxIDs, lccUniqueIDs, nominalCodes);
		// getting the handling charges to use as the commission
		Map<Pair<String, Integer>, BigDecimal> handlingCharges = paxBD.getHandlingChargesForPaxTnxs(ownTnxIDs, lccUniqueIDs);
		// getting the e ticket information required for the TKTT
		Map<Pair<String, Integer>, Set<ETicketInfoTO>> eTicketInfo = airproxySegmentBD.getETicketInformation(lccUniqueIDs,
				ownTnxIDs, getBasicTrackInfo(userPrincipal));

		List<BSPFailedTnx> errorTnxs = new ArrayList<BSPFailedTnx>();

		BSPTnx[] tnxArr = new BSPTnx[bspTnxSet.size()];
		Arrays.sort(bspTnxSet.toArray(tnxArr), new Comparator<BSPTnx>() {
			public int compare(BSPTnx o1, BSPTnx o2) {
				return o1.getTnxType().compareTo(o2.getTnxType());
			}
		});
		TNXLOOP: for (BSPTnx tnx : tnxArr) {
			// TODO validate if the BSP payment is previously processed
			BSPTransactionDataDTO bspTransactionDataDTO = new BSPTransactionDataDTO();

			ReservationPax pax = paxBD.getPassenger(tnx.getPnrPaxID(), false);
			bspTransactionDataDTO.setLongPaxName(
					pax.getFirstName() + "/" + pax.getLastName() + " " + getPaxType(pax.getTitle(), pax.getPaxType()));
			bspTransactionDataDTO.setDateOfIssue(tnx.getTnxDate());
			bspTransactionDataDTO.setPayCurrencyAmount(tnx.getPayCurrencyAmount().negate());
			bspTransactionDataDTO.setPayCurrencyCode(tnx.getPayCurrencyCode());
			bspTransactionDataDTO.setBaseCurrencyAmount(tnx.getAmount().negate());
			bspTransactionDataDTO.setPnr(pax.getReservation().getPnr());
			bspTransactionDataDTO.setTicketingAirlineCode(AppSysParamsUtil.getBSPTicketingAirlineCode());
			bspTransactionDataDTO.setAaLCCUniqueID(tnx.getLccUniqueTnxID());
			bspTransactionDataDTO.setAaTnxID(tnx.getTnxID());
			bspTransactionDataDTO.setPaxSequnce(tnx.getPaxSequence());
			bspTransactionDataDTO.setAaAgentCode(tnx.getAgentCode());
			if (tnx.getIataAgentCode() != null) {
				bspTransactionDataDTO.setIataAgentCode(Long.parseLong(tnx.getIataAgentCode()));
			}

			bspTransactionDataDTO.setPaxSpecificData(getPaxSpecificData(pax.getPaxAdditionalInfoSet()) + "");

			// Get exchange rate for the original transaction date
			// CurrencyExchangeRate currencyExchangeRate = new
			// ExchangeRateProxy(tnx.getTnxDate()).getCurrencyExchangeRate(tnx
			// .getPayCurrencyCode());
			CurrencyExchangeRate currencyExchangeRate = exRateProxy.getCurrencyExchangeRate(tnx.getPayCurrencyCode());
			bspTransactionDataDTO.setCurrencyExchangeRate(currencyExchangeRate.getMultiplyingExchangeRate());

			if (tnx.getTnxType().equals(ReservationInternalConstants.TnxTypes.CREDIT)) {
				if (tnx.getNominalCode() == 3) {
					bspTransactionDataDTO.setTnxType(TransactionType.CANX);
				} else {
					bspTransactionDataDTO.setTnxType(TransactionType.TKTT);
				}
				// create pair using the either lcc unique id or tnx id and pax
				// sequence
				Pair<String, Integer> pair = Pair.of(
						(tnx.getLccUniqueTnxID() != null) ? tnx.getLccUniqueTnxID() : Integer.toString(tnx.getTnxID()),
						tnx.getPaxSequence());
				Collection<ReservationPaxSegmentPaymentTO> segmentPayments = segmentPaymentInfo.get(pair);
				BigDecimal commission = handlingCharges.get(pair);

				if (segmentPayments == null) {
					String error = TransactionType.TKTT.toString() + ":" + "No segment wise payments for : " + pair.getLeft()
							+ " : " + pair.getRight();
					if (log.isErrorEnabled())
						log.error(error);
					errorTnxs.add(composeErrorDTO(countryCode, agentCode, tnx, userPrincipal, error));
					continue TNXLOOP;
				}
				if (tnx.getNominalCode() == 3) {
					bspTransactionDataDTO.setTnxType(TransactionType.CANX);
				} else {
					bspTransactionDataDTO.setTnxType(TransactionType.TKTT);
					for (ReservationPaxSegmentPaymentTO segpayment : segmentPayments) {
						for (SegmentChargePaymentTO segChrgPayment : segpayment.getSegmentChargePaymentCollection()) {
							if (segChrgPayment.getChargeGroupCode().equals(ChargeGroupCode.MOD.toString())) {
								bspTransactionDataDTO.setTnxType(TransactionType.EMDA);
							}
						}
					}
				}
				bspTransactionDataDTO.setTotalFare(getTotalFare(segmentPayments, false));
				bspTransactionDataDTO.setTaxCollection(composeTaxCollection(segmentPayments));
				bspTransactionDataDTO.setAgentCommission(commission);

				List<BSPSegmentInfoDTO> paxAllSegments = InvoicingModuleUtils.getBSPLinkJdbcDao()
						.getSegmentInformation(tnx.getPnrPaxID());

				Object[] eticketAndSegments = composeBSPSegments(pair, agentCode, tnx, userPrincipal, paxAllSegments,
						segmentPayments, eTicketInfo, TransactionType.TKTT, errorTnxs, countryCode, false);
				if (eticketAndSegments[0] != null && eticketAndSegments[0] instanceof String
						&& ((String) eticketAndSegments[0]).equals(STR_EROOR)) {
					continue TNXLOOP;
				} else {
					bspTransactionDataDTO.setFirstSetOfSegments((Pair<Long, List<BSPSegmentInfoDTO>>) eticketAndSegments[0]);
					bspTransactionDataDTO.setSecondSetOfSegments((Pair<Long, List<BSPSegmentInfoDTO>>) eticketAndSegments[1]);
				}
				List<BSPSegmentInfoDTO> paxBSPSegments = (List<BSPSegmentInfoDTO>) eticketAndSegments[2];
				String fareCalcArea = composeFareCalculationArea(paxBSPSegments,
						currencyExchangeRate.getMultiplyingExchangeRate(), pax.getTotalFare(), segmentPayments, false);
				String[] fareCalcSplits = getFareCalculationAreasCollection(fareCalcArea, agentCode, tnx, userPrincipal,
						TransactionType.TKTT, errorTnxs, countryCode);
				if (fareCalcSplits[0] != null && fareCalcSplits[0].equals(STR_EROOR)) {
					continue TNXLOOP;
				} else {
					bspTransactionDataDTO.setFareCalculationAreas(fareCalcSplits);
				}
				ArrayList<BSPTransactionTaxData> paxTaxData = InvoicingModuleUtils.getBSPLinkJdbcDao()
						.getBSPPaxTnxTaxData(tnx.getPnrPaxID().toString());
				bspTransactionDataDTO.setTnxOndTaxInfoCollection(paxTaxData);

			} else {
				bspTransactionDataDTO.setTnxType(TransactionType.RFND);

				List<Integer> ownTnxIDsForRFnd = new ArrayList<Integer>();
				List<String> lccUniqueIDsForRFnd = new ArrayList<String>();

				String originalTransactionID = null;
				if (tnx.getLccUniqueTnxID() != null) {
					BSPTnx originalTnx = InvoicingModuleUtils.getBSPLinkJdbcDao()
							.getOriginalLCCTransaction(tnx.getOriginalPaymentID(), tnx.getPaxSequence());
					if (originalTnx == null) {
						String error = TransactionType.RFND.toString() + ":" + "Original Transaction not found : "
								+ tnx.getLccUniqueTnxID() + " " + tnx.getPaxSequence();
						if (log.isErrorEnabled())
							log.error(error);
						errorTnxs.add(composeErrorDTO(countryCode, agentCode, tnx, userPrincipal, error));
					} else {
						originalTransactionID = originalTnx.getLccUniqueTnxID();
						lccUniqueIDsForRFnd.add(originalTransactionID);
						bspTransactionDataDTO.setOriginalPaymentTnxDate(originalTnx.getTnxDate());
					}
				} else {
					// BSPTnx originalTnx =
					// InvoicingModuleUtils.getBSPLinkJdbcDao().getOriginalOwnTransaction(
					// tnx.getOriginalPaymentID(), tnx.getPaxSequence());
					BSPTnx originalTnx = InvoicingModuleUtils.getBSPLinkJdbcDao().getOriginalOwnTransaction(tnx.getTnxID(),
							tnx.getPaxSequence());
					if (originalTnx == null) {
						String error = TransactionType.RFND.toString() + ":" + "Original Transaction not found : "
								+ tnx.getTnxID() + " " + tnx.getPaxSequence();
						if (log.isErrorEnabled())
							log.error(error);
						errorTnxs.add(composeErrorDTO(countryCode, agentCode, tnx, userPrincipal, error));
					} else {
						originalTransactionID = tnx.getOriginalPaymentID().toString();
						ownTnxIDsForRFnd.add(new Integer(originalTransactionID));
						bspTransactionDataDTO.setOriginalPaymentTnxDate(originalTnx.getTnxDate());
					}
				}

				// getting the segment wise payments per pax for own, dry and
				// interline for original refund transaction
				Map<Pair<String, Integer>, Collection<ReservationPaxSegmentPaymentTO>> segmentPaymentInfoForRfnd = paxBD
						.getReservationPaxSegmentPayments(ownTnxIDsForRFnd, lccUniqueIDsForRFnd, nominalCodes);

				// getting the handling charges to use as the commission for
				// original refund transaction
				// Map<Pair<String, Integer>, BigDecimal> handlingChargesForRfnd
				// = paxBD.getHandlingChargesForPaxTnxs(
				// ownTnxIDsForRFnd, lccUniqueIDsForRFnd);

				Map<Pair<String, Integer>, Set<ETicketInfoTO>> eTicketInfoForRfnd = airproxySegmentBD
						.getETicketInformation(lccUniqueIDsForRFnd, ownTnxIDsForRFnd, getBasicTrackInfo(userPrincipal));

				Pair<String, Integer> pair = Pair.of(originalTransactionID, tnx.getPaxSequence());
				Collection<ReservationPaxSegmentPaymentTO> segmentPayments = segmentPaymentInfoForRfnd.get(pair);
				// BigDecimal commission = handlingChargesForRfnd.get(pair);
				if (segmentPayments == null) {
					String error = TransactionType.RFND.toString() + ":" + "No segment wise payments for : " + pair.getLeft()
							+ " : " + pair.getRight();
					if (log.isErrorEnabled())
						log.error(error);
					errorTnxs.add(composeErrorDTO(countryCode, agentCode, tnx, userPrincipal, error));
					continue TNXLOOP;
				}
				// bspTransactionDataDTO.setTaxCollection(composeTaxCollection(segmentPayments));
				// bspTransactionDataDTO.setAgentCommission(commission);

				List<BSPSegmentInfoDTO> paxAllSegments = InvoicingModuleUtils.getBSPLinkJdbcDao()
						.getSegmentInformation(tnx.getPnrPaxID());
				Object[] eticketAndSegments = composeBSPSegments(pair, agentCode, tnx, userPrincipal, paxAllSegments,
						segmentPayments, eTicketInfoForRfnd, TransactionType.RFND, errorTnxs, countryCode, false);
				if (eticketAndSegments[0] != null && eticketAndSegments[0] instanceof String
						&& ((String) eticketAndSegments[0]).equals(STR_EROOR)) {
					continue TNXLOOP;
				} else {
					bspTransactionDataDTO.setFirstSetOfSegments((Pair<Long, List<BSPSegmentInfoDTO>>) eticketAndSegments[0]);
					bspTransactionDataDTO.setSecondSetOfSegments((Pair<Long, List<BSPSegmentInfoDTO>>) eticketAndSegments[1]);
				}
			}
			bspTransactionDataDTOs.add(bspTransactionDataDTO);
		}

		return bspTransactionDataDTOs;
	}

	public static List<BSPTransactionDataDTO> composeHotTransactionDataDTOs(String countryCode, String agentCode, Date fromDate,
			Date toDate, UserPrincipal userPrincipal) throws ModuleException, BSPException {

		PassengerBD paxBD = InvoicingModuleUtils.getPassengerBD();
		AirproxySegmentBD airproxySegmentBD = InvoicingModuleUtils.getAirproxySegmentBD();
		List<BSPTransactionDataDTO> bspTransactionDataDTOs = new ArrayList<BSPTransactionDataDTO>();
		List<Integer> nominalCodes = new ArrayList<Integer>();

		// using a set to avoid any duplications between all tnxs and error
		// fixed tnxs
		Set<BSPTnx> bspTnxSet = new HashSet<BSPTnx>();

		String nomialCodes = AppSysParamsUtil.getNomialCodesSupportsForReporting();
		String[] nCodes = nomialCodes.split(",");
		for (int i = 0; i < nCodes.length; i++) {
			nominalCodes.add(Integer.parseInt(nCodes[i]));

		}

		List<BSPTnx> bspTnxList = InvoicingModuleUtils.getBSPLinkJdbcDao().getAgentAllBSPTransactions(agentCode, fromDate, toDate,
				nominalCodes, ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
		for (BSPTnx tnx : bspTnxList) {
			bspTnxSet.add(tnx);

		}
		List<BSPTnx> previousErrorFixedTnxs = InvoicingModuleUtils.getBSPLinkJdbcDao()
				.getAgentErrorFixedBSPTransactions(agentCode);
		if (previousErrorFixedTnxs != null && previousErrorFixedTnxs.size() > 0) {
			for (BSPTnx tnx : previousErrorFixedTnxs) {
				bspTnxSet.add(tnx);
			}
		}
		List<Integer> ownTnxIDs = new ArrayList<Integer>();
		List<String> lccUniqueIDs = new ArrayList<String>();

		for (BSPTnx tnx : bspTnxSet) {
			if (tnx.getLccUniqueTnxID() != null) {
				lccUniqueIDs.add(tnx.getLccUniqueTnxID());
			} else {
				ownTnxIDs.add(tnx.getTnxID());
			}
		}

		// getting the segment wise payments per pax for own, dry and interline
		Map<Pair<String, Integer>, Collection<ReservationPaxSegmentPaymentTO>> segmentPaymentInfo = paxBD
				.getReservationPaxSegmentPayments(ownTnxIDs, lccUniqueIDs, nominalCodes);
		// getting the handling charges to use as the commission
		Map<Pair<String, Integer>, BigDecimal> handlingCharges = paxBD.getHandlingChargesForPaxTnxs(ownTnxIDs, lccUniqueIDs);
		// getting the e ticket information required for the TKTT
		Map<Pair<String, Integer>, Set<ETicketInfoTO>> eTicketInfo = airproxySegmentBD.getETicketInformation(lccUniqueIDs,
				ownTnxIDs, getBasicTrackInfo(userPrincipal));

		List<BSPFailedTnx> errorTnxs = new ArrayList<BSPFailedTnx>();

		BSPTnx[] tnxArr = new BSPTnx[bspTnxSet.size()];
		Arrays.sort(bspTnxSet.toArray(tnxArr), new Comparator<BSPTnx>() {
			public int compare(BSPTnx o1, BSPTnx o2) {
				return o1.getTnxType().compareTo(o2.getTnxType());
			}
		});
		TNXLOOP: for (BSPTnx tnx : tnxArr) {
			// TODO validate if the BSP payment is previously processed
			BSPTransactionDataDTO bspTransactionDataDTO = new BSPTransactionDataDTO();

			ReservationPax pax = paxBD.getPassenger(tnx.getPnrPaxID(), false);
			bspTransactionDataDTO.setLongPaxName(pax.getFirstName() + " " + pax.getLastName());
			bspTransactionDataDTO.setTitle(pax.getTitle());
			bspTransactionDataDTO.setDateOfIssue(tnx.getTnxDate());
			bspTransactionDataDTO.setPayCurrencyAmount(tnx.getPayCurrencyAmount().negate());
			bspTransactionDataDTO.setPayCurrencyCode(tnx.getPayCurrencyCode());
			bspTransactionDataDTO.setPnr(pax.getReservation().getPnr());
			bspTransactionDataDTO.setTicketingAirlineCode(AppSysParamsUtil.getBSPTicketingAirlineCode());
			bspTransactionDataDTO.setAaLCCUniqueID(tnx.getLccUniqueTnxID());
			bspTransactionDataDTO.setAaTnxID(tnx.getTnxID());
			bspTransactionDataDTO.setPaxSequnce(tnx.getPaxSequence());
			bspTransactionDataDTO.setAaAgentCode(tnx.getAgentCode());
			bspTransactionDataDTO.setPaymentDesc(tnx.getNomialDesc());
			if (tnx.getIataAgentCode() != null) {
				bspTransactionDataDTO.setIataAgentCode(Long.decode(tnx.getIataAgentCode()));
			}

			// Get exchange rate for the original transaction date
			CurrencyExchangeRate currencyExchangeRate = new ExchangeRateProxy(tnx.getTnxDate())
					.getCurrencyExchangeRate(tnx.getPayCurrencyCode());
			bspTransactionDataDTO.setCurrencyExchangeRate(currencyExchangeRate.getMultiplyingExchangeRate());

			if (tnx.getTnxType().equals(ReservationInternalConstants.TnxTypes.CREDIT)) {
				if (tnx.getNominalCode() == 3) {
					bspTransactionDataDTO.setTnxType(TransactionType.CANX);
				} else {
					bspTransactionDataDTO.setTnxType(TransactionType.TKTT);
				}
				// create pair using the either lcc unique id or tnx id and pax
				// sequence
				Pair<String, Integer> pair = Pair.of(
						(tnx.getLccUniqueTnxID() != null) ? tnx.getLccUniqueTnxID() : Integer.toString(tnx.getTnxID()),
						tnx.getPaxSequence());
				Collection<ReservationPaxSegmentPaymentTO> segmentPayments = segmentPaymentInfo.get(pair);
				BigDecimal commission = handlingCharges.get(pair);
				if (segmentPayments == null) {
					String error = TransactionType.TKTT.toString() + ":" + "No segment wise payments for : " + pair.getLeft()
							+ " : " + pair.getRight();
					if (log.isErrorEnabled())
						log.error(error);
					errorTnxs.add(composeErrorDTO(countryCode, agentCode, tnx, userPrincipal, error));
					continue TNXLOOP;
				}
				bspTransactionDataDTO.setTotalFare(getTotalFare(segmentPayments, false));
				bspTransactionDataDTO.setTaxCollection(composeTaxCollection(segmentPayments));
				bspTransactionDataDTO.setAgentCommission(commission);

				List<BSPSegmentInfoDTO> paxAllSegments = InvoicingModuleUtils.getBSPLinkJdbcDao()
						.getSegmentInformation(tnx.getPnrPaxID());

				Object[] eticketAndSegments = composeBSPSegments(pair, agentCode, tnx, userPrincipal, paxAllSegments,
						segmentPayments, eTicketInfo, TransactionType.TKTT, errorTnxs, countryCode, false);
				if (eticketAndSegments[0] != null && eticketAndSegments[0] instanceof String
						&& ((String) eticketAndSegments[0]).equals(STR_EROOR)) {
					continue TNXLOOP;
				} else {
					bspTransactionDataDTO.setFirstSetOfSegments((Pair<Long, List<BSPSegmentInfoDTO>>) eticketAndSegments[0]);
					bspTransactionDataDTO.setSecondSetOfSegments((Pair<Long, List<BSPSegmentInfoDTO>>) eticketAndSegments[1]);
				}
				List<BSPSegmentInfoDTO> paxBSPSegments = (List<BSPSegmentInfoDTO>) eticketAndSegments[2];
				String fareCalcArea = composeFareCalculationArea(paxBSPSegments,
						currencyExchangeRate.getMultiplyingExchangeRate(), pax.getTotalFare(), segmentPayments, false);
				String[] fareCalcSplits = getFareCalculationAreasCollection(fareCalcArea, agentCode, tnx, userPrincipal,
						TransactionType.TKTT, errorTnxs, countryCode);
				if (fareCalcSplits[0] != null && fareCalcSplits[0].equals(STR_EROOR)) {
					continue TNXLOOP;
				} else {
					bspTransactionDataDTO.setFareCalculationAreas(fareCalcSplits);
				}

			} else {
				bspTransactionDataDTO.setTnxType(TransactionType.RFND);

				List<Integer> ownTnxIDsForRFnd = new ArrayList<Integer>();
				List<String> lccUniqueIDsForRFnd = new ArrayList<String>();

				String originalTransactionID = null;
				if (tnx.getLccUniqueTnxID() != null) {
					BSPTnx originalTnx = InvoicingModuleUtils.getBSPLinkJdbcDao()
							.getOriginalLCCTransaction(tnx.getOriginalPaymentID(), tnx.getPaxSequence());
					if (originalTnx == null) {
						String error = TransactionType.RFND.toString() + ":" + "Original Transaction not found : "
								+ tnx.getLccUniqueTnxID() + " " + tnx.getPaxSequence();
						if (log.isErrorEnabled())
							log.error(error);
						errorTnxs.add(composeErrorDTO(countryCode, agentCode, tnx, userPrincipal, error));
					} else {
						originalTransactionID = originalTnx.getLccUniqueTnxID();
						lccUniqueIDsForRFnd.add(originalTransactionID);
						bspTransactionDataDTO.setOriginalPaymentTnxDate(originalTnx.getTnxDate());
					}
				} else {
					BSPTnx originalTnx = InvoicingModuleUtils.getBSPLinkJdbcDao()
							.getOriginalOwnTransaction(tnx.getOriginalPaymentID(), tnx.getPaxSequence());
					if (originalTnx == null) {
						String error = TransactionType.RFND.toString() + ":" + "Original Transaction not found : "
								+ tnx.getTnxID() + " " + tnx.getPaxSequence();
						if (log.isErrorEnabled())
							log.error(error);
						errorTnxs.add(composeErrorDTO(countryCode, agentCode, tnx, userPrincipal, error));
					} else {
						originalTransactionID = tnx.getOriginalPaymentID().toString();
						ownTnxIDsForRFnd.add(new Integer(originalTransactionID));
						bspTransactionDataDTO.setOriginalPaymentTnxDate(originalTnx.getTnxDate());
					}
				}

				// getting the segment wise payments per pax for own, dry and
				// interline for original refund
				// transaction
				Map<Pair<String, Integer>, Collection<ReservationPaxSegmentPaymentTO>> segmentPaymentInfoForRfnd = paxBD
						.getReservationPaxSegmentPayments(ownTnxIDsForRFnd, lccUniqueIDsForRFnd, nominalCodes);

				// getting the handling charges to use as the commission for
				// original refund transaction
				// Map<Pair<String, Integer>, BigDecimal> handlingChargesForRfnd
				// =
				// paxBD.getHandlingChargesForPaxTnxs(
				// ownTnxIDsForRFnd, lccUniqueIDsForRFnd);

				Map<Pair<String, Integer>, Set<ETicketInfoTO>> eTicketInfoForRfnd = airproxySegmentBD
						.getETicketInformation(lccUniqueIDsForRFnd, ownTnxIDsForRFnd, getBasicTrackInfo(userPrincipal));

				Pair<String, Integer> pair = Pair.of(originalTransactionID, tnx.getPaxSequence());
				Collection<ReservationPaxSegmentPaymentTO> segmentPayments = segmentPaymentInfoForRfnd.get(pair);
				// BigDecimal commission = handlingChargesForRfnd.get(pair);
				if (segmentPayments == null) {
					String error = TransactionType.RFND.toString() + ":" + "No segment wise payments for : " + pair.getLeft()
							+ " : " + pair.getRight();
					if (log.isErrorEnabled())
						log.error(error);
					errorTnxs.add(composeErrorDTO(countryCode, agentCode, tnx, userPrincipal, error));
					continue TNXLOOP;
				}
				// bspTransactionDataDTO.setTaxCollection(composeTaxCollection(segmentPayments));
				// bspTransactionDataDTO.setAgentCommission(commission);

				List<BSPSegmentInfoDTO> paxAllSegments = InvoicingModuleUtils.getBSPLinkJdbcDao()
						.getSegmentInformation(tnx.getPnrPaxID());
				Object[] eticketAndSegments = composeBSPSegments(pair, agentCode, tnx, userPrincipal, paxAllSegments,
						segmentPayments, eTicketInfoForRfnd, TransactionType.RFND, errorTnxs, countryCode, false);
				if (eticketAndSegments[0] != null && eticketAndSegments[0] instanceof String
						&& ((String) eticketAndSegments[0]).equals(STR_EROOR)) {
					continue TNXLOOP;
				} else {
					bspTransactionDataDTO.setFirstSetOfSegments((Pair<Long, List<BSPSegmentInfoDTO>>) eticketAndSegments[0]);
					bspTransactionDataDTO.setSecondSetOfSegments((Pair<Long, List<BSPSegmentInfoDTO>>) eticketAndSegments[1]);
				}
			}
			bspTransactionDataDTOs.add(bspTransactionDataDTO);
		}

		return bspTransactionDataDTOs;

	}

	public static List<BSPReconciliationRecordDTO> getBSPTransactionsForReconciliation(ReportsSearchCriteria searchCriteria,
			UserPrincipal userPrincipal) throws ModuleException {

		PassengerBD paxBD = InvoicingModuleUtils.getPassengerBD();
		AirproxySegmentBD airproxySegmentBD = InvoicingModuleUtils.getAirproxySegmentBD();
		List<BSPReconciliationRecordDTO> bspReconciliationRecords = new ArrayList<BSPReconciliationRecordDTO>();

		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

		try {
			Date tnxFromDate = dateFormat.parse(searchCriteria.getDateRangeFrom() + " 00:00:00");
			Date tnxToDate = dateFormat.parse(searchCriteria.getDateRangeTo() + " 23:59:59");

			// Extract all BSP transactions
			List<BSPTnx> bspTnxList = InvoicingModuleUtils.getBSPLinkJdbcDao().getAllBSPTransactionsForReconciliation(
					searchCriteria.getCountryCode(), tnxFromDate, tnxToDate, searchCriteria.getNominalCodes(),
					searchCriteria.getDataSoureType(), searchCriteria.getEntity());

			List<BSPTnx> bspExtTnxList = InvoicingModuleUtils.getBSPLinkJdbcDao().getExternalBSPTransactions(
					searchCriteria.getCountryCode(), tnxFromDate, tnxToDate, searchCriteria.getNominalCodes(),
					searchCriteria.getDataSoureType(), searchCriteria.getEntity());

			bspTnxList.addAll(bspExtTnxList);

			List<Integer> ownTnxIDs = new ArrayList<Integer>();
			List<String> extReferences = new ArrayList<String>();
			List<String> lccUniqueIDs = new ArrayList<String>();
			List<Integer> nominalCodes = new ArrayList<Integer>();

			nominalCodes.add(36);

			for (BSPTnx tnx : bspTnxList) {
				if (tnx.getExternalReference() != null) {
					extReferences.add(tnx.getExternalReference());
				} else if (tnx.getLccUniqueTnxID() != null) {
					lccUniqueIDs.add(tnx.getLccUniqueTnxID());
				} else {
					ownTnxIDs.add(tnx.getTnxID());
				}
			}

			Map<Integer, BSPReconciliationRecordDTO> reconciledOwnTnxMap = new HashMap<Integer, BSPReconciliationRecordDTO>();
			Map<Pair<String, Integer>, BSPReconciliationRecordDTO> reconciledLccTnxMap = new HashMap<Pair<String, Integer>, BSPReconciliationRecordDTO>();
			Map<String, BSPReconciliationRecordDTO> reconciledExtTnxMap = new HashMap<String, BSPReconciliationRecordDTO>();

			// Update them with eticket numbers
			Map<Pair<String, Integer>, Collection<ReservationPaxSegmentPaymentTO>> segmentPaymentInfo = paxBD
					.getReservationPaxSegmentPayments(ownTnxIDs, lccUniqueIDs, nominalCodes);
			// getting the e ticket information required for the TKTT
			Map<Pair<String, Integer>, Set<ETicketInfoTO>> eTicketInfo = airproxySegmentBD.getETicketInformation(lccUniqueIDs,
					ownTnxIDs, getBasicTrackInfo(userPrincipal));

			for (BSPTnx bspTransaction : bspTnxList) {
				BSPReconciliationRecordDTO reconRecord = new BSPReconciliationRecordDTO();

				reconRecord.setPaxName(bspTransaction.getPaxName());
				reconRecord.setTransactionDate(bspTransaction.getTnxDate());
				reconRecord.setCurrency(bspTransaction.getPayCurrencyCode());
				reconRecord.setPnr(bspTransaction.getPnr());
				reconRecord.setAgentCode(bspTransaction.getAgentCode());
				reconRecord.setIataAgentCode(bspTransaction.getIataAgentCode());
				reconRecord.setTnxID(bspTransaction.getTnxID());
				reconRecord.setLccUniqueTnxID(bspTransaction.getLccUniqueTnxID());
				reconRecord.setSubmitted(CommonsConstants.NO);
				reconRecord.setReconciled(CommonsConstants.NO);
				reconRecord.setPaxSequence(bspTransaction.getPaxSequence());
				reconRecord.setCountry(bspTransaction.getCountry());
				reconRecord.setExternalReference(bspTransaction.getExternalReference());

				if (reconRecord.getExternalReference() != null) {
					reconciledExtTnxMap.put(reconRecord.getExternalReference(), reconRecord);
				} else if (reconRecord.getTnxID() != null) {
					reconciledOwnTnxMap.put(reconRecord.getTnxID(), reconRecord);
				} else {
					Pair<String, Integer> pair = Pair.of(reconRecord.getLccUniqueTnxID(), reconRecord.getPaxSequence());
					reconciledLccTnxMap.put(pair, reconRecord);
				}

				if (bspTransaction.getTnxType().equals(ReservationInternalConstants.TnxTypes.CREDIT)) {

					if (bspTransaction.isFreshReservationPayment()) {
						reconRecord.setPaymentAmount(bspTransaction.getPayCurrencyAmount().negate());
						reconRecord.setTransactionType(TransactionType.TKTT.toString());
						Pair<String, Integer> pair = Pair.of(
								(bspTransaction.getLccUniqueTnxID() != null)
										? bspTransaction.getLccUniqueTnxID()
										: Integer.toString(bspTransaction.getTnxID()),
								bspTransaction.getPaxSequence());
						Collection<ReservationPaxSegmentPaymentTO> segmentPayments = segmentPaymentInfo.get(pair);
						if (segmentPayments == null) {
							continue;
						}

						List<BSPSegmentInfoDTO> paxAllSegments = InvoicingModuleUtils.getBSPLinkJdbcDao()
								.getSegmentInformation(bspTransaction.getPnrPaxID());

						Object[] eticketAndSegments = composeBSPSegments(pair, null, bspTransaction, userPrincipal,
								paxAllSegments, segmentPayments, eTicketInfo, TransactionType.TKTT, null, null, true);

						if (eticketAndSegments[0] != null && eticketAndSegments[0] instanceof String
								&& ((String) eticketAndSegments[0]).equals(STR_EROOR)) {
							continue;
						} else {
							reconRecord
									.setPrimaryEticketNo(((Pair<Long, List<BSPSegmentInfoDTO>>) eticketAndSegments[0]).getLeft());
							if (eticketAndSegments[1] != null) {
								reconRecord.setSecondaryEticketNo(
										((Pair<Long, List<BSPSegmentInfoDTO>>) eticketAndSegments[1]).getLeft());
							}
						}
					} else {
						// emds
						EMDSTransaction emdsTransaction = InvoicingModuleUtils.getBSPLinkDao().getEMDSTransction(
								bspTransaction.getTnxID(), bspTransaction.getLccUniqueTnxID(), bspTransaction.getPaxSequence(),
								bspTransaction.getExternalReference());
						if (emdsTransaction != null) {
							reconRecord.setPrimaryEticketNo(new Long(emdsTransaction.getEmdsTicketNo()));
							reconRecord.setTransactionType(TransactionType.EMDS.toString());
							reconRecord.setPaymentAmount(emdsTransaction.getAmount().abs());
						}
					}

				} else {

					reconRecord.setTransactionType(TransactionType.RFND.toString());
					reconRecord.setRefundAmount(bspTransaction.getPayCurrencyAmount());

					List<Integer> ownTnxIDsForRFnd = new ArrayList<Integer>();
					List<String> lccUniqueIDsForRFnd = new ArrayList<String>();

					BSPTnx originalTnx = null;
					String originalTransactionID = null;
					if (bspTransaction.getLccUniqueTnxID() != null) {
						originalTnx = InvoicingModuleUtils.getBSPLinkJdbcDao().getOriginalLCCTransaction(
								bspTransaction.getOriginalPaymentID(), bspTransaction.getPaxSequence());
						if (originalTnx == null) {
							continue;
						} else {
							originalTransactionID = originalTnx.getLccUniqueTnxID();
							lccUniqueIDsForRFnd.add(originalTransactionID);
						}
					} else {
						originalTnx = InvoicingModuleUtils.getBSPLinkJdbcDao().getOriginalOwnTransaction(
								bspTransaction.getOriginalPaymentID(), bspTransaction.getPaxSequence());
						if (originalTnx == null) {
							continue;
						} else {
							originalTransactionID = bspTransaction.getOriginalPaymentID().toString();
							ownTnxIDsForRFnd.add(new Integer(originalTransactionID));
						}
					}

					if (originalTnx.isFreshReservationPayment()) {
						Map<Pair<String, Integer>, Collection<ReservationPaxSegmentPaymentTO>> segmentPaymentInfoForRfnd = paxBD
								.getReservationPaxSegmentPayments(ownTnxIDsForRFnd, lccUniqueIDsForRFnd, nominalCodes);

						Map<Pair<String, Integer>, Set<ETicketInfoTO>> eTicketInfoForRfnd = airproxySegmentBD
								.getETicketInformation(lccUniqueIDsForRFnd, ownTnxIDsForRFnd, getBasicTrackInfo(userPrincipal));

						Pair<String, Integer> pair = Pair.of(originalTransactionID, bspTransaction.getPaxSequence());
						Collection<ReservationPaxSegmentPaymentTO> segmentPayments = segmentPaymentInfoForRfnd.get(pair);

						if (segmentPayments == null) {
							continue;
						}

						List<BSPSegmentInfoDTO> paxAllSegments = InvoicingModuleUtils.getBSPLinkJdbcDao()
								.getSegmentInformation(bspTransaction.getPnrPaxID());
						Object[] eticketAndSegments = composeBSPSegments(pair, null, bspTransaction, userPrincipal,
								paxAllSegments, segmentPayments, eTicketInfoForRfnd, TransactionType.RFND, null, null, true);
						if (eticketAndSegments[0] != null && eticketAndSegments[0] instanceof String
								&& ((String) eticketAndSegments[0]).equals(STR_EROOR)) {
							continue;
						} else {
							reconRecord
									.setPrimaryEticketNo(((Pair<Long, List<BSPSegmentInfoDTO>>) eticketAndSegments[0]).getLeft());
							if (eticketAndSegments[1] != null) {
								reconRecord.setSecondaryEticketNo(
										((Pair<Long, List<BSPSegmentInfoDTO>>) eticketAndSegments[1]).getLeft());
							}
						}
					} else {
						// emds
						EMDSTransaction emdsTransaction = InvoicingModuleUtils.getBSPLinkDao().getEMDSTransction(
								originalTnx.getTnxID(), originalTnx.getLccUniqueTnxID(), originalTnx.getPaxSequence(),
								bspTransaction.getExternalReference());
						if (emdsTransaction != null) {
							reconRecord.setPrimaryEticketNo(new Long(emdsTransaction.getEmdsTicketNo()));
						}
					}

				}
			}

			// Now join reconciliation information

			if (!ownTnxIDs.isEmpty() || !lccUniqueIDs.isEmpty() || !extReferences.isEmpty()) {
				List<BSPReconciliationStatus> reconStatuses = InvoicingModuleUtils.getBSPLinkJdbcDao()
						.getReconciliationInformation(ownTnxIDs, lccUniqueIDs, searchCriteria.getDataSoureType(), extReferences);
				for (BSPReconciliationStatus reconStatus : reconStatuses) {

					BSPReconciliationRecordDTO recordDTO;
					
					if (reconStatus.getExternalReference() != null) {
						recordDTO = reconciledExtTnxMap.get(reconStatus.getExternalReference());
					} else if (reconStatus.getTnxID() != null) {
						recordDTO = reconciledOwnTnxMap.get(reconStatus.getTnxID());
					} else {
						Pair<String, Integer> pair = Pair.of(reconStatus.getLccUniqueTnxID(), reconStatus.getPaxSequence());
						recordDTO = reconciledLccTnxMap.get(pair);
					}

					String submitted = (RET_FILE_STATUS.UPLOADED.toString().equals(reconStatus.getRETFileStatus())
							? CommonsConstants.YES
							: CommonsConstants.NO);
					recordDTO.setSubmitted(submitted);
					recordDTO.setReconciled(reconStatus.getReconciled());
					recordDTO.setDpcProcesssedDate(reconStatus.getDpcProcessedDate());

					if (TransactionType.TKTT.toString().equals(recordDTO.getTransactionType())
							|| TransactionType.EMDS.toString().equals(recordDTO.getTransactionType())) {
						recordDTO.setProcessedPaymentAmount(reconStatus.getProcessedAmount());
					} else if (TransactionType.RFND.toString().equals(recordDTO.getTransactionType())) {
						recordDTO.setProcessedRefundAmount(reconStatus.getProcessedAmount());
					} else {
						log.error("An unsupported Transaction type encountered");
						throw new ModuleException("um.bsp.recon.data.retrive.failed");
					}
					recordDTO.setProcessedCurrency(reconStatus.getProcessedCurrency());

					if (recordDTO.getPrimaryEticketNo() == null) {
						recordDTO.setPrimaryEticketNo(reconStatus.getPrimaryEticket());
					}

					if (recordDTO.getSecondaryEticketNo() == null) {
						recordDTO.setSecondaryEticketNo(reconStatus.getSecondaryEticket());
					}
				}

				List<BSPReconciliationRecordDTO> totalRecords = new ArrayList<BSPReconciliationRecordDTO>();

				totalRecords.addAll(reconciledOwnTnxMap.values());
				totalRecords.addAll(reconciledLccTnxMap.values());
				totalRecords.addAll(reconciledExtTnxMap.values());

				// Let's filter records

				String status = searchCriteria.getStatus();

				boolean filterByStatus = false;
				boolean filterByDPCProcessedDateFrom = false;
				boolean filterByDPCProcessedDateTo = false;
				if (status != null && !"".equals(status)) {
					filterByStatus = true;
				}

				Date dpcProcessedDateFrom = null;
				Date dpcProcessedDateTo = null;
				if (searchCriteria.getDateRange2From() != null && !"".equals(searchCriteria.getDateRange2From())) {
					dpcProcessedDateFrom = dateFormat.parse(searchCriteria.getDateRange2From() + " 00:00:00");
					filterByDPCProcessedDateFrom = true;
				}

				if (searchCriteria.getDateRange2To() != null && !"".equals(searchCriteria.getDateRange2To())) {
					dpcProcessedDateTo = dateFormat.parse(searchCriteria.getDateRange2To() + " 23:59:59");
					filterByDPCProcessedDateTo = true;
				}

				for (BSPReconciliationRecordDTO record : totalRecords) {

					Date dpcProcessedDate = record.getDpcProcesssedDate();
					boolean statusMatch = true;
					boolean dpcDateMatch = true;

					if (filterByStatus) {
						if (!status.equals(record.getReconciled())) {
							statusMatch = false;
						}
					}

					if (filterByDPCProcessedDateFrom) {
						if (dpcProcessedDate == null || dpcProcessedDate.before(dpcProcessedDateFrom)) {
							dpcDateMatch = false;
						}
					}

					if (filterByDPCProcessedDateTo) {
						if (dpcProcessedDate == null || dpcProcessedDate.after(dpcProcessedDateTo)) {
							dpcDateMatch = false;
						}
					}

					if (statusMatch && dpcDateMatch) {
						bspReconciliationRecords.add(record);
					}
				}
			}

		} catch (Exception e) {
			log.error(e.getMessage());
			throw new ModuleException("um.bsp.recon.data.retrive.failed", e);
		}

		Collections.sort(bspReconciliationRecords);
		return bspReconciliationRecords;
	}

	private static EMDSTransaction composeEMDSTransaction(BSPTnx bspTransaction, String emdsTicketNo) {
		EMDSTransaction emdsTransaction = new EMDSTransaction();
		emdsTransaction.setEmdsTicketNo(emdsTicketNo);
		if (bspTransaction.getLccUniqueTnxID() != null) {
			emdsTransaction.setLccUniqueID(bspTransaction.getLccUniqueTnxID());
		} else {
			emdsTransaction.setTnxID(bspTransaction.getTnxID());
		}

		emdsTransaction.setAmount(bspTransaction.getPayCurrencyAmount());
		emdsTransaction.setCurrency(bspTransaction.getPayCurrencyCode());
		emdsTransaction.setRequestForIssuanceCode(EMDSTransaction.RFIC.A.toString());
		emdsTransaction.setRequestForIssuanceSubCode(EMDSTransaction.RFISC.UPGRADES.getCode());
		emdsTransaction.setStatus(EMDSTransaction.STATUS.I.toString());
		emdsTransaction.setPaxSequence(bspTransaction.getPaxSequence());
		emdsTransaction.setExternalReference(bspTransaction.getExternalReference());
		return emdsTransaction;
	}

	private static Object[] composeBSPSegments(Pair<String, Integer> pair, String agentCode, BSPTnx tnx,
			UserPrincipal userPrincipal, List<BSPSegmentInfoDTO> paxAllSegments,
			Collection<ReservationPaxSegmentPaymentTO> segmentPayments,
			Map<Pair<String, Integer>, Set<ETicketInfoTO>> eTicketInfo, TransactionType tnxType, List<BSPFailedTnx> errorTnxs,
			String countryCode, boolean eticketExtractionForReconciliation) {

		List<BSPSegmentInfoDTO> paxBSPSegments = new ArrayList<BSPSegmentInfoDTO>();
		// we need to filter the BSP payment related segments of initial create
		// booking
		for (BSPSegmentInfoDTO segDto : paxAllSegments) {
			for (ReservationPaxSegmentPaymentTO paymentTO : segmentPayments) {
				if (segDto.getSegmentCode().equals(paymentTO.getSegmentCode())
						&& segDto.getCarrierCode().equals(paymentTO.getCarrierCode())
						&& segDto.getDepartureDateZulu().equals(paymentTO.getDepartureDateZulu())) {
					paxBSPSegments.add(segDto);
				}
			}
		}
		if (paxBSPSegments.size() == 0) {
			String error = tnxType.toString() + ":" + "BSP Segments not found";
			if (log.isErrorEnabled())
				log.error(error);
			if (!eticketExtractionForReconciliation) {
				errorTnxs.add(composeErrorDTO(countryCode, agentCode, tnx, userPrincipal, error));
			}
			return new Object[] { STR_EROOR };
		} else if (paxBSPSegments.size() > 8) {
			String error = tnxType.toString() + ":" + "BSP does not allowed more than 8 segments";
			if (log.isErrorEnabled())
				log.error(error);
			if (!eticketExtractionForReconciliation) {
				errorTnxs.add(composeErrorDTO(countryCode, agentCode, tnx, userPrincipal, error));
			}
			return new Object[] { STR_EROOR };
		}
		// creating two collections of 4 segments as it allowed only 8 coupons
		List<BSPSegmentInfoDTO> firstSetOfBSPSegments = new ArrayList<BSPSegmentInfoDTO>();
		List<BSPSegmentInfoDTO> secoondSetOfBSPSegments = new ArrayList<BSPSegmentInfoDTO>();

		int segmentID = 1;
		for (int i = 0; i < paxBSPSegments.size(); ++i) {
			// setting the e ticket and segment identifier
			paxBSPSegments.get(i).setSegmentIdentifier(segmentID++);
			if (eTicketInfo.get(pair) == null) {
				String error = tnxType.toString() + ":" + "No e tickets for " + pair.getLeft() + " " + pair.getRight();
				if (log.isErrorEnabled())
					log.error(error);
				if (!eticketExtractionForReconciliation) {
					errorTnxs.add(composeErrorDTO(countryCode, agentCode, tnx, userPrincipal, error));
				}
				return new Object[] { STR_EROOR };
			}
			for (ETicketInfoTO infoTO : eTicketInfo.get(pair)) {
				if (infoTO.getDepDateZulu().equals(paxBSPSegments.get(i).getDepartureDateZulu())
						&& infoTO.getFlightNumber().equals(paxBSPSegments.get(i).getFlightNumber())
						&& infoTO.getSegmentCode().equals(paxBSPSegments.get(i).getSegmentCode())) {
					paxBSPSegments.get(i).seteTicketNumber(infoTO.geteTicketNo());
				}
			}
			// double checking the e ticket as it is the most important record
			// in BSP
			if (!(paxBSPSegments.get(i).geteTicketNumber() > 0)) {
				String error = tnxType.toString() + ":" + "No e ticket found for " + paxBSPSegments.get(i).getPnr() + " : "
						+ paxBSPSegments.get(i).getSegmentCode();
				if (log.isErrorEnabled())
					log.error(error);
				if (!eticketExtractionForReconciliation) {
					errorTnxs.add(composeErrorDTO(countryCode, agentCode, tnx, userPrincipal, error));
				}
				return new Object[] { STR_EROOR };
			}
			// adding elements to the two collections
			if (i < 4) {
				firstSetOfBSPSegments.add(paxBSPSegments.get(i));
			} else {
				secoondSetOfBSPSegments.add(paxBSPSegments.get(i));
			}
			if (segmentID > 4) {
				segmentID = 1;
			}
		}
		// to get the minimum out of the e ticket collections
		List<Long> firstSetOfETickets = new ArrayList<Long>();
		for (BSPSegmentInfoDTO infoDTO : firstSetOfBSPSegments) {
			firstSetOfETickets.add(infoDTO.geteTicketNumber());
		}

		List<Long> secondSetOfETickets = new ArrayList<Long>();
		for (BSPSegmentInfoDTO infoDTO : secoondSetOfBSPSegments) {
			secondSetOfETickets.add(infoDTO.geteTicketNumber());
		}
		Collections.sort(firstSetOfETickets);
		Collections.sort(secondSetOfETickets);

		Object[] ret = new Object[3];
		Pair<Long, List<BSPSegmentInfoDTO>> firstSegmentsPair = Pair.of(firstSetOfETickets.get(0), firstSetOfBSPSegments);
		ret[0] = firstSegmentsPair;
		if (secondSetOfETickets.size() > 0) {
			if (firstSetOfETickets.get(0) != (secondSetOfETickets.get(0) - 1)) {
				String error = "Conjunction ticket numbering is not sequential";
				if (log.isErrorEnabled())
					log.error(error);
				if (!eticketExtractionForReconciliation) {
					errorTnxs.add(composeErrorDTO(countryCode, agentCode, tnx, userPrincipal, error));
				}
				return new Object[] { STR_EROOR };
			}
			Pair<Long, List<BSPSegmentInfoDTO>> secSegmentsPair = Pair.of(secondSetOfETickets.get(0), secoondSetOfBSPSegments);
			ret[1] = secSegmentsPair;
		} else {
			ret[1] = null;
		}
		ret[2] = paxBSPSegments;
		return ret;
	}

	private static String composeFareCalculationArea(List<BSPSegmentInfoDTO> segments, BigDecimal rateOfExchange,
			BigDecimal totalFare, Collection<ReservationPaxSegmentPaymentTO> segmentPayments, boolean isBSPFileGeneration) {
		StringBuffer fareCalcBuffer = new StringBuffer();

		int i = 0;
		boolean isCustomRetFileGenerationEnabled = AppSysParamsUtil.isRetFileGenerationForCustomNominalCodesEnabled();
		BigDecimal totalFare_Surcharge = AccelAeroCalculator.getDefaultBigDecimalZero();
		for (BSPSegmentInfoDTO segDto : segments) {
			BigDecimal totalSurchargesForSegment = AccelAeroCalculator.getDefaultBigDecimalZero();
			BigDecimal totalFareForSegment = AccelAeroCalculator.getDefaultBigDecimalZero();
			Collection<SegmentChargePaymentTO> segmentChargePaymentTOs = null;
			for (ReservationPaxSegmentPaymentTO paymentTO : segmentPayments) {
				if (segDto.getSegmentCode().equals(paymentTO.getSegmentCode())
						&& segDto.getCarrierCode().equals(paymentTO.getCarrierCode())
						&& segDto.getDepartureDateZulu().equals(paymentTO.getDepartureDateZulu())) {
					segmentChargePaymentTOs = paymentTO.getSegmentChargePaymentCollection();
				}
			}

			if (isCustomRetFileGenerationEnabled && !isBSPFileGeneration) {
				for (SegmentChargePaymentTO chargePaymentTO : segmentChargePaymentTOs) {
					if (chargePaymentTO.getNominalCode() == 18 || chargePaymentTO.getNominalCode() == 19) {
						if (chargePaymentTO.getChargeGroupCode().equals(ChargeGroupCode.SUR.toString())) {
							totalSurchargesForSegment = AccelAeroCalculator.add(totalSurchargesForSegment,
									chargePaymentTO.getAmount());
						} else if (chargePaymentTO.getChargeGroupCode().equals(ChargeGroupCode.INF.toString())) {
							totalSurchargesForSegment = AccelAeroCalculator.add(totalSurchargesForSegment,
									chargePaymentTO.getAmount());
						} else if (chargePaymentTO.getChargeGroupCode().equals(ChargeGroupCode.FAR.toString())) {
							totalFareForSegment = AccelAeroCalculator.add(totalFareForSegment, chargePaymentTO.getAmount());
						}
					}
				}
			} else {
				for (SegmentChargePaymentTO chargePaymentTO : segmentChargePaymentTOs) {
					if (chargePaymentTO.getChargeGroupCode().equals(ChargeGroupCode.SUR.toString())) {
						totalSurchargesForSegment = AccelAeroCalculator.add(totalSurchargesForSegment,
								chargePaymentTO.getAmount());
					} else if (chargePaymentTO.getChargeGroupCode().equals(ChargeGroupCode.INF.toString())) {
						totalSurchargesForSegment = AccelAeroCalculator.add(totalSurchargesForSegment,
								chargePaymentTO.getAmount());
					} else if (chargePaymentTO.getChargeGroupCode().equals(ChargeGroupCode.FAR.toString())) {
						totalFareForSegment = AccelAeroCalculator.add(totalFareForSegment, chargePaymentTO.getAmount());
					}
				}
			}

			String strTotalSurchargesForSegment = "";
			String strTotalFareForSegment = "";

			if (!totalSurchargesForSegment.equals(AccelAeroCalculator.getDefaultBigDecimalZero()))
				if (InvoicingModuleUtils.getInvoicingConfig().isFareLadderInRetFmt()) {
					// strTotalSurchargesForSegment = " Q" +
					// AccelAeroCalculator.formatAsDecimal(totalSurchargesForSegment);
					if (!totalFareForSegment.equals(AccelAeroCalculator.getDefaultBigDecimalZero()))
						strTotalFareForSegment = " "
								+ AccelAeroCalculator.formatAsDecimal(totalFareForSegment.add(totalSurchargesForSegment));
				} else {
					// for MAHAN ret file IT07 FRCA need surcharges with Q
					// prefix
					strTotalSurchargesForSegment = " Q" + AccelAeroCalculator.formatAsDecimal(totalSurchargesForSegment);
					if (isCustomRetFileGenerationEnabled && !isBSPFileGeneration) {
						if (!totalFareForSegment.equals(AccelAeroCalculator.getDefaultBigDecimalZero()))
							strTotalFareForSegment = " " + AccelAeroCalculator.formatAsDecimal(totalFareForSegment);
					}
				}
			if (isBSPFileGeneration) {
				if (!totalFareForSegment.equals(AccelAeroCalculator.getDefaultBigDecimalZero()))
					strTotalFareForSegment = " " + AccelAeroCalculator.formatAsDecimal(totalFareForSegment);

			}

			if (i == 0) {
				fareCalcBuffer.append(segDto.getOrigin() + " " + segDto.getCarrierCode() + " " + segDto.getDestination()
						+ strTotalSurchargesForSegment + strTotalFareForSegment + " ");
			} else {
				fareCalcBuffer.append(segDto.getCarrierCode() + " " + segDto.getDestination() + strTotalSurchargesForSegment
						+ strTotalFareForSegment + " ");
			}
			totalFare_Surcharge = totalFare_Surcharge.add(totalFareForSegment.add(totalSurchargesForSegment));
			++i;
		}
		StringBuffer strTaxBuffer = new StringBuffer();
		for (ReservationPaxSegmentPaymentTO segmentPaymentTO : segmentPayments) {
			for (SegmentChargePaymentTO chargePaymentTO : segmentPaymentTO.getSegmentChargePaymentCollection()) {
				if (chargePaymentTO.getChargeGroupCode().equals(ChargeGroupCode.TAX.toString())) {
					strTaxBuffer.append(AccelAeroCalculator.formatAsDecimal(chargePaymentTO.getAmount()) + "AE");
				}
			}
		}
		String strTax = "";
		if (strTaxBuffer.length() > 0) {
			strTax = "XT" + strTaxBuffer.toString();
		}

		if (isCustomRetFileGenerationEnabled && !isBSPFileGeneration) {
			fareCalcBuffer.append(AppSysParamsUtil.getBaseCurrency() + AccelAeroCalculator.formatAsDecimal(totalFare_Surcharge)
					+ "END ROE" + AccelAeroCalculator.formatAsDecimal(rateOfExchange));
		} else {
			fareCalcBuffer.append(AppSysParamsUtil.getBaseCurrency() + AccelAeroCalculator.formatAsDecimal(totalFare) + " END ROE"
					+ AccelAeroCalculator.formatAsDecimal(rateOfExchange) + " " + strTax);
		}

		return fareCalcBuffer.toString();
	}

	private static Map<String, BigDecimal> composeTaxCollection(Collection<ReservationPaxSegmentPaymentTO> segmentPayments) {

		Map<String, BigDecimal> taxMap = new HashMap<String, BigDecimal>();

		if (segmentPayments != null) {
			int k = 1;
			BigDecimal tx1 = null;
			BigDecimal tx2 = null;
			BigDecimal xt = AccelAeroCalculator.getDefaultBigDecimalZero();
			for (ReservationPaxSegmentPaymentTO segmentPaymentTO : segmentPayments) {
				for (SegmentChargePaymentTO chargePaymentTO : segmentPaymentTO.getSegmentChargePaymentCollection()) {
					if (chargePaymentTO.getChargeGroupCode().equals(ChargeGroupCode.TAX.toString())) {
						if (k == 1)
							tx1 = chargePaymentTO.getAmount();
						else if (k == 2)
							tx2 = chargePaymentTO.getAmount();
						else if (k > 2)
							xt = AccelAeroCalculator.add(xt, chargePaymentTO.getAmount());
						++k;
					}
				}
			}

			taxMap.put(TaxCodes.TX1.toString(), tx1);
			taxMap.put(TaxCodes.TX2.toString(), tx2);
			taxMap.put(TaxCodes.TX3.toString(), xt);
		}

		return taxMap;
	}

	private static String[] getFareCalculationAreasCollection(String fareCalcArea, String agentCode, BSPTnx tnx,
			UserPrincipal userPrincipal, TransactionType tnxType, List<BSPFailedTnx> errorTnxs, String countryCode) {
		String[] fareCalcAreas = new String[4];
		if (fareCalcArea != null) {
			if (fareCalcArea.length() > (FRCA_MAX_LENGTH * 4)) {
				String error = tnxType.toString() + ":" + " Fair Calculation Area is too long";
				if (log.isErrorEnabled())
					log.error(error);
				errorTnxs.add(composeErrorDTO(countryCode, agentCode, tnx, userPrincipal, error));
				return new String[] { STR_EROOR };
			} else {
				if (fareCalcArea.length() <= FRCA_MAX_LENGTH) {
					fareCalcAreas[0] = fareCalcArea;
				} else {
					int firstIndex = fareCalcArea.substring(0, FRCA_MAX_LENGTH).lastIndexOf(FARE_CALC_AREA_SEPERATOR);
					fareCalcAreas[0] = fareCalcArea.substring(0, firstIndex);
					if (fareCalcArea.length() <= (FRCA_MAX_LENGTH * 2)) {
						fareCalcAreas[1] = fareCalcArea.substring(firstIndex + 1);
					} else {
						int secondIndex = fareCalcArea.substring(firstIndex, firstIndex + FRCA_MAX_LENGTH)
								.lastIndexOf(FARE_CALC_AREA_SEPERATOR) + fareCalcAreas[0].length();
						fareCalcAreas[1] = fareCalcArea.substring(firstIndex, secondIndex);
						if (fareCalcArea.length() <= (FRCA_MAX_LENGTH * 3)) {
							fareCalcAreas[2] = fareCalcArea.substring(secondIndex + 1);
						} else {
							int thirdIndex = fareCalcArea.substring(secondIndex, secondIndex + FRCA_MAX_LENGTH).lastIndexOf(
									FARE_CALC_AREA_SEPERATOR) + fareCalcAreas[0].length() + fareCalcAreas[1].length();
							fareCalcAreas[2] = fareCalcArea.substring(secondIndex, thirdIndex);
							fareCalcAreas[3] = fareCalcArea.substring(thirdIndex + 1);
						}
					}
				}
			}
		}
		return fareCalcAreas;
	}

	private static BigDecimal getTotalFare(Collection<ReservationPaxSegmentPaymentTO> segmentPayments,
			boolean isBSPFileGeneration) {
		BigDecimal totalFare = AccelAeroCalculator.getDefaultBigDecimalZero();
		for (ReservationPaxSegmentPaymentTO segPaymentTO : segmentPayments) {
			for (SegmentChargePaymentTO chgPaymentTO : segPaymentTO.getSegmentChargePaymentCollection()) {
				if (isBSPFileGeneration) {
					if (chgPaymentTO.getChargeGroupCode().equals(ChargeGroupCode.FAR.toString())) {
						totalFare = AccelAeroCalculator.add(totalFare, chgPaymentTO.getAmount());
					}
				} else {
					if (chgPaymentTO.getChargeGroupCode().equals(ChargeGroupCode.FAR.toString())
							|| chgPaymentTO.getChargeGroupCode().equals(ChargeGroupCode.SUR.toString())) {
						if (chgPaymentTO.getNominalCode() == 18 || chgPaymentTO.getNominalCode() == 19) {
							totalFare = AccelAeroCalculator.add(totalFare, chgPaymentTO.getAmount());
						}
					}
				}
			}
		}
		return totalFare;
	}

	private static BasicTrackInfo getBasicTrackInfo(UserPrincipal userPrincipal) {
		BasicTrackInfo bt = null;
		if (userPrincipal != null) {
			bt = new BasicTrackInfo();
			bt.setSessionId(userPrincipal.getSessionId());
			bt.setCallingInstanceId(userPrincipal.getCallingInstanceId());
		}
		return bt;
	}

	public static BSPFailedTnx composeErrorDTO(String countryCode, String agentCode, BSPTnx bspTnx, UserPrincipal userPrincipal,
			String description) {
		BSPFailedTnx failedTnxDTO = new BSPFailedTnx();

		failedTnxDTO.setAgentCode(agentCode);
		failedTnxDTO.setDescription(description);
		failedTnxDTO.setLccUniqueID(bspTnx.getLccUniqueTnxID());
		failedTnxDTO.setReportingUserID(userPrincipal.getUserId());
		failedTnxDTO.setStatus(BSPErrorLogStatus.ERROR.toString());
		failedTnxDTO.setPaxSequence(bspTnx.getPaxSequence());
		if (bspTnx.getTnxID() != null && bspTnx.getTnxID() == 0) {
			failedTnxDTO.setTnxID(null);
		} else {
			failedTnxDTO.setTnxID(bspTnx.getTnxID());
		}
		Date date = new Date();
		failedTnxDTO.setCreatingDate(date);
		failedTnxDTO.setModifiedBy(userPrincipal.getUserId());
		failedTnxDTO.setModifiedDate(date);
		failedTnxDTO.setCountryCode(countryCode);

		return failedTnxDTO;
	}

	public static BSPFailedTnx composeErrorDTO(String countryCode, String status, BSPTransactionDataDTO bspTnx, String userId,
			String description) {
		BSPFailedTnx failedTnxDTO = new BSPFailedTnx();

		failedTnxDTO.setAgentCode(bspTnx.getAaAgentCode());
		failedTnxDTO.setDescription(description);
		failedTnxDTO.setLccUniqueID(bspTnx.getAaLCCUniqueID());
		failedTnxDTO.setReportingUserID(userId);
		failedTnxDTO.setStatus(status);
		failedTnxDTO.setPaxSequence(bspTnx.getPaxSequnce());
		if (bspTnx.getAaTnxID() != null && bspTnx.getAaTnxID() == 0) {
			failedTnxDTO.setTnxID(null);
		} else {
			failedTnxDTO.setTnxID(bspTnx.getAaTnxID());
		}
		Date date = new Date();
		failedTnxDTO.setCreatingDate(date);
		failedTnxDTO.setModifiedBy(userId);
		failedTnxDTO.setModifiedDate(date);
		failedTnxDTO.setCountryCode(countryCode);

		return failedTnxDTO;
	}

	public static void recordBSPFailedErrors(List<BSPFailedTnx> errorTnxs) {
		InvoicingModuleUtils.getBSPLinkDao().saveOrUpdateBSPFailedTransactions(errorTnxs);
	}

	private static void updatePreviouslyFixedErrors(List<BSPTnx> errorFixedTnxs, UserPrincipal userPrincipal)
			throws ModuleException {
		List<BSPFailedTnx> allFailedTnxs = new ArrayList<BSPFailedTnx>();
		for (BSPTnx tnx : errorFixedTnxs) {

			if (tnx.getTnxID() != null && tnx.getTnxID() > 0) {
				allFailedTnxs.add(InvoicingModuleUtils.getBSPLinkDao().getFailedOwnTnx(tnx.getTnxID(), tnx.getPaxSequence()));
			} else if (tnx.getLccUniqueTnxID() != null) {
				allFailedTnxs.add(
						InvoicingModuleUtils.getBSPLinkDao().getFailedExternalTnx(tnx.getLccUniqueTnxID(), tnx.getPaxSequence()));
			}
		}
		Date date = new Date();
		for (BSPFailedTnx tnx : allFailedTnxs) {
			tnx.setModifiedBy(userPrincipal.getUserId());
			tnx.setModifiedDate(date);
			tnx.setStatus(BSPErrorLogStatus.SUBMITTED);
		}
		InvoicingModuleUtils.getBSPLinkDao().saveOrUpdateBSPFailedTransactions(allFailedTnxs);
	}

	public static void logDishFileInfoAndTnxData(String dishFileName, List<BSPTransactionDataDTO> tnxs,
			UserPrincipal userPrincipal) {

		List<DishFileTnxLogData> logs = new ArrayList<DishFileTnxLogData>();
		Date date = new Date();
		for (BSPTransactionDataDTO tnx : tnxs) {
			DishFileTnxLogData logData = new DishFileTnxLogData();

			logData.setCreatedBy(userPrincipal.getUserId());
			logData.setCreatedDate(date);
			logData.setDateOfTnx(tnx.getDateOfIssue());
			logData.setLccUniqueID(tnx.getAaLCCUniqueID());
			logData.setModifiedby(userPrincipal.getUserId());
			logData.setModifiedDate(date);
			logData.setPaxName(tnx.getPaxName());
			logData.setExternalReference(tnx.getExternalReference());
			logData.setPaxSequence(tnx.getPaxSequnce());
			logData.setPnr(tnx.getPnr());
			logData.setPrimaryETicket(tnx.getFirstSetOfSegments().getLeft());
			if (tnx.getSecondSetOfSegments() != null) {
				logData.setSecondaryEticket(tnx.getSecondSetOfSegments().getLeft());
			}
			logData.setTnxID((tnx.getAaTnxID() != null && tnx.getAaTnxID().intValue() != 0) ? tnx.getAaTnxID() : null);
			logData.setTnxType(tnx.getTnxType().toString());
			logData.setProductType(tnx.getProductType());

			logs.add(logData);
		}

		InvoicingModuleUtils.getBSPLinkDao().saveOrUpdateDishFileTnxLogData(logs);
	}

	public static List<BSPRejectedTnxInfo> parseBSPErrors(List<String> errors) {
		List<BSPRejectedTnxInfo> rejectedTnxs = new ArrayList<BSPRejectedTnxInfo>();

		for (String error : errors) {
			BSPRejectedTnxInfo rejectedTnx = new BSPRejectedTnxInfo();

			rejectedTnx.setRecordID(Integer.parseInt(error.substring(0, 2)));
			rejectedTnx.setTnxNumber(Integer.parseInt(error.substring(2, 8)));
			rejectedTnx.setAgentIATACode(Integer.parseInt(error.substring(8, 16)));
			rejectedTnx.seteTicketNumber(Long.parseLong(error.substring(16, 31)));
			rejectedTnx.setDescription(error.substring(31, 46));
			rejectedTnx.setErrorType(error.substring(46, 46));
			rejectedTnx.setCode(error.substring(46, 49));

			rejectedTnxs.add(rejectedTnx);
		}

		return rejectedTnxs;
	}

	public static BSPFailedTnx composeErrorDTO(BSPRejectedTnxInfo rejectedTnx, UserPrincipal userPrincipal, Date date,
			String countryCode) {
		DishFileTnxLogData logData = InvoicingModuleUtils.getBSPLinkDao().getBSPDishFileLogData(rejectedTnx.geteTicketNumber());

		BSPFailedTnx bspFailedTnx = new BSPFailedTnx();

		bspFailedTnx.setAgentCode(InvoicingModuleUtils.getBSPLinkJdbcDao().getAgentCode(rejectedTnx.getAgentIATACode() + ""));
		bspFailedTnx.setDescription(rejectedTnx.getDescription() + " : " + rejectedTnx.getCode());
		bspFailedTnx.setLccUniqueID(logData.getLccUniqueID());
		bspFailedTnx.setPaxSequence(logData.getPaxSequence());
		bspFailedTnx.setStatus(BSPErrorLogStatus.ERROR.toString());
		bspFailedTnx.setTnxID((logData.getTnxID() != null && logData.getTnxID().intValue() != 0) ? logData.getTnxID() : null);
		bspFailedTnx.setCreatingDate(date);
		bspFailedTnx.setModifiedBy(userPrincipal.getUserId());
		bspFailedTnx.setModifiedDate(date);
		bspFailedTnx.setReportingUserID(userPrincipal.getUserId());
		bspFailedTnx.setCountryCode(countryCode);

		return bspFailedTnx;
	}

	public static int getDishFileSeqence(String countryCode, Date date) throws ModuleException {
		int fileSequence = 0;

		Integer lastSeq = InvoicingModuleUtils.getBSPLinkDao().getLastFileSequence(countryCode, date);
		if (lastSeq != null) {
			fileSequence = ++lastSeq;
		}

		return fileSequence;
	}

	public static void recordBSPFileLog(String countryCode, int fileSequence, UserPrincipal userPrincipal, String fileName,
			RET_FILE_STATUS ret_file_status, List<BSPTransactionDataDTO> tnxs) {
		Date date = new Date();

		BSPFileLog fileLog = new BSPFileLog();

		fileLog.setCountryCode(countryCode);
		fileLog.setCreatedBy(userPrincipal.getUserId());
		fileLog.setCreatedDate(date);
		fileLog.setFileName(fileName);
		fileLog.setFileSequence(fileSequence);
		fileLog.setModifiedby(userPrincipal.getUserId());
		fileLog.setModifiedDate(date);
		fileLog.setRETFileStatus(ret_file_status.toString());

		if (tnxs != null && !tnxs.isEmpty()) {
			Set<DishFileTnxLogData> logs = new HashSet<DishFileTnxLogData>();
			for (BSPTransactionDataDTO tnx : tnxs) {
				DishFileTnxLogData logData = new DishFileTnxLogData();
				logData.setCreatedBy(userPrincipal.getUserId());
				logData.setCreatedDate(date);
				logData.setDateOfTnx(tnx.getDateOfIssue());
				logData.setLccUniqueID(tnx.getAaLCCUniqueID());
				logData.setModifiedby(userPrincipal.getUserId());
				logData.setModifiedDate(date);
				logData.setPaxName(tnx.getPaxName());
				logData.setPaxSequence(tnx.getPaxSequnce());
				logData.setPnr(tnx.getPnr());
				logData.setExternalReference(tnx.getExternalReference());
				
				if ((tnx.getTnxType() != TransactionType.EMDS)
						|| (tnx.getTnxType() == TransactionType.RFND && tnx.getOriginalEMDSTransaction() == null)) {

				} else {
					logData.setEmds(tnx.getEmdsTransaction().getEmdsTicketNo());
				}

				if ((tnx.getTnxType() == TransactionType.TKTT)
						|| (tnx.getTnxType() == TransactionType.RFND && tnx.getOriginalEMDSTransaction() == null)) {
					logData.setPrimaryETicket(tnx.getFirstSetOfSegments().getLeft());
					if (tnx.getSecondSetOfSegments() != null) {
						logData.setSecondaryEticket(tnx.getSecondSetOfSegments().getLeft());
					}
				} else if (tnx.getTnxType() == TransactionType.EMDS) {
					logData.setEmds(tnx.getEmdsTransaction().getEmdsTicketNo());
				} else if (tnx.getTnxType() == TransactionType.RFND && tnx.getOriginalEMDSTransaction() != null) {
					logData.setEmds(tnx.getOriginalEMDSTransaction().getEmdsTicketNo());
				}

				logData.setTnxID((tnx.getAaTnxID() != null && tnx.getAaTnxID().intValue() != 0) ? tnx.getAaTnxID() : null);
				logData.setTnxType(tnx.getTnxType().toString());
				logData.setReconciled(CommonsConstants.NO);
				logData.setBspFileLog(fileLog);
				logData.setProductType(tnx.getProductType());
				logs.add(logData);
			}
			fileLog.setFileTransactionLogs(logs);
		}

		InvoicingModuleUtils.getBSPLinkDao().saveOrUpdateBSPFileLog(fileLog);
	}

	public static void logTransactionsForResubmission(List<BSPTransactionDataDTO> transactions, String countryCode,
			String description, String userId) {

		List<BSPFailedTnx> submisstionFailedBSPTransactions = new ArrayList<BSPFailedTnx>();

		for (BSPTransactionDataDTO transaction : transactions) {
			BSPFailedTnx submissioFailedTransaction = composeErrorDTO(countryCode, BSPErrorLogStatus.RESUBMIT.toString(),
					transaction, userId, "Submission Failed Tnx");
			submisstionFailedBSPTransactions.add(submissioFailedTransaction);
		}

		if (!submisstionFailedBSPTransactions.isEmpty()) {
			recordBSPFailedErrors(submisstionFailedBSPTransactions);
		}
	}

	private static String getPaxType(String paxTitle, String paxType) {
		String retPaxType = "MR";
		if ("AD".equalsIgnoreCase(paxType)) {
			if ("MS".equalsIgnoreCase(paxTitle)) {
				retPaxType = "MRS";
			}
		} else if ("CH".equalsIgnoreCase(paxType)) {
			retPaxType = "CHD";
		} else if ("IN".equalsIgnoreCase(paxType)) {
			retPaxType = "INF";
		}
		return retPaxType;
	}

	private static String getPaxSpecificData(Set<ReservationPaxAdditionalInfo> paxAdditionalInfoSet) {
		StringBuilder paxAdditionalInfo = new StringBuilder("");
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");

		for (ReservationPaxAdditionalInfo paxAddiInfo : paxAdditionalInfoSet) {
			if (paxAddiInfo.getPassportNo() != null) {
				paxAdditionalInfo.append(paxAddiInfo.getPassportNo());
				paxAdditionalInfo.append("|");
				if (paxAddiInfo.getPassportExpiry() != null) {
					paxAdditionalInfo.append(formatter.format(paxAddiInfo.getPassportExpiry()));
				} else {
					paxAdditionalInfo.append(" ");
				}
				paxAdditionalInfo.append("|");
				paxAdditionalInfo.append(paxAddiInfo.getPassportIssuedCntry());
			}
		}
		return paxAdditionalInfo.toString();

	}
}
