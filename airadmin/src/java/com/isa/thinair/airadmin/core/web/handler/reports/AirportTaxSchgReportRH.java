/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airadmin.core.web.handler.reports;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;

/**
 * @author Thushara
 * 
 */
public class AirportTaxSchgReportRH extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(CompPaymentPOSReportRH.class);

	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	/**
	 * Main Execute Method for company payment report by point of sales
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @param response
	 *            the HttpServletResponse
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {

		String strHdnMode = request.getParameter("hdnMode");
		String forward = WebConstants.ACTION_FORWARD_SUCCESS;

		try {
			setDisplayData(request);
			log.debug("AirportTaxSchgReportRH SETDISPLAYDATA() SUCCESS");
		} catch (Exception e) {
			log.error("AirportTaxSchgReportRH SETDISPLAYDATA() FAILED " + e.getMessage());
		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				log.error("AirportTaxSchgReportRH setReportView Success");
				return null;
			}

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("AirportTaxSchgReportRH setReportView Failed " + e.getMessageString());
		}

		return forward;
	}

	/**
	 * Sets the Display Data
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	private static void setDisplayData(HttpServletRequest request) throws Exception {
		setClientErrors(request);
		setLiveStatus(request);
		setReportingPeriod(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);
	}

	/**
	 * Sets the report medium to the request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleExceptions
	 */
	private static void setLiveStatus(HttpServletRequest request) throws ModuleException {
		String strLive = request.getParameter(WebConstants.REP_HDN_LIVE);
		if (strLive != null)
			request.setAttribute(WebConstants.REP_SET_LIVE, strLive);
	}

	/**
	 * Sets the Client Validations for reports
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setClientErrors(HttpServletRequest request) throws ModuleException {
		String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);
		if (strClientErrors == null) {
			strClientErrors = "";
		}
		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);

	}

	/**
	 * Setting the reporting period to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setReportingPeriod(HttpServletRequest request) throws ModuleException {
		int period = new Integer(globalConfig.getBizParam(SystemParamKeys.REPORTING_PERIOD)).intValue();
		request.setAttribute(WebConstants.REP_RPT_START_DAY, ReportsHTMLGenerator.getReportingPeriod(period));
	}

	/**
	 * Displays the Report
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @param response
	 *            the HttpServletResponse
	 * @throws ModuleException
	 *             the ModuleException
	 */
	protected static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		ResultSet resultSet = null;
		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String value = request.getParameter("radReportOption");
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);
		String id = "UC_REPM_032";
		String template = "AirportTaxSurchargeReport.jasper";

		try {
			ReportsSearchCriteria search = new ReportsSearchCriteria();

			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}

			Map<String, Object> parameters = new HashMap<String, Object>();
			if (fromDate != null && !fromDate.equals("")) {
				search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate) + " 00:00:00");
			}

			if (toDate != null && !toDate.equals("")) {
				search.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate) + " 23:59:59");
			}
			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
					ReportsHTMLGenerator.getReportTemplate(template));

			resultSet = ModuleServiceLocator.getDataExtractionBD().getAirportTaxData(search);

			parameters.put("FROM_DATE", fromDate);
			parameters.put("TO_DATE", toDate);
			parameters.put("ID", id);
			parameters.put("REPORT_MEDIUM", strlive);

			String strLogo = AppSysParamsUtil.getReportLogo(false);
			String reportsRootDir = "../../images/" + AppSysParamsUtil.getReportLogo(true);
			String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);

			// To provide Report Format Options
			String reportNumFormat = request.getParameter("radRptNumFormat");
			ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

			if (value.trim().equals("HTML")) {
				parameters.put("IMG", reportsRootDir);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null,
						reportsRootDir, response);

			} else if (value.trim().equals("PDF")) {
				reportsRootDir = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG", reportsRootDir);
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);

			} else if (value.trim().equals("EXCEL")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=AirportTaxSurchargeReport.xls");
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);

			} else if (value.trim().equals("CSV")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=AirportTaxSurchargeReport.csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);

			}
		} catch (Exception e) {
			log.error(e);
		}
	}
}