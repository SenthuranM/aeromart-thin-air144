/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airadmin.core.web.handler.reports;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleConfig;
import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author Duminda Anus
 */
public class TransactionReportRH extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(TransactionReportRH.class);

	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	/**
	 * The constructor.
	 */
	public TransactionReportRH() {
		super();
	}

	/**
	 * @param request
	 * @return
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {
		String strHdnMode = request.getParameter("hdnMode");
		String forward = WebConstants.ACTION_FORWARD_SUCCESS;

		try {
			setDisplayData(request);
			log.debug("TransactionReportRH success");
		} catch (Exception e) {
			forward = AdminStrutsConstants.AdminAction.ERROR;
			log.error("TransactionReportRH execute()" + e.getMessage());
		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				log.error("TransactionReportRH setReportView Success");
				return null;
			} else {
				log.error("TransactionReportRH setReportView not selected");
			}

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("TransactionReportRH setReportView Failed " + e.getMessageString());
		}

		return forward;
	}

	/**
	 * 
	 * @param request
	 * @throws Exception
	 */
	public static void setDisplayData(HttpServletRequest request) throws Exception {
		setClientErrors(request);
		setLiveStatus(request);
		setReportingPeriod(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);
		setInvoiceEntities(request);
	}

	/**
	 * 
	 * @param request
	 */
	public static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the report medium to the request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleExceptions
	 */
	private static void setLiveStatus(HttpServletRequest request) throws ModuleException {
		String strLive = request.getParameter(WebConstants.REP_HDN_LIVE);
		if (strLive != null) {
			request.setAttribute(WebConstants.REP_SET_LIVE, strLive);
		}

	}

	/**
	 * Setting the reporting period to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setReportingPeriod(HttpServletRequest request) throws ModuleException {
		int period = new Integer(globalConfig.getBizParam(SystemParamKeys.REPORTING_PERIOD)).intValue();
		request.setAttribute(WebConstants.REP_RPT_START_DAY, ReportsHTMLGenerator.getReportingPeriod(period));
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		ResultSet resultSet = null;
		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String value = request.getParameter("radReportOption");
		String reportOption = request.getParameter("ccReportName");
 		boolean isDetailReport = new Boolean(request.getParameter("hdnDetail"));
 		String entity = request.getParameter("selEntity");
 		String entityText = request.getParameter("hdnEntityText");

		String id = "UC_REPM_015";
		String reportTemplate = "";
		String strLogo = AppSysParamsUtil.getReportLogo(false);
		String image = "../../images/" +  AppSysParamsUtil.getReportLogo(true);
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);
		AiradminModuleConfig airadminConfig = AiradminModuleUtils.getConfig();

		boolean isIncludeAdvanceCCDetails = ((("true").equals(airadminConfig.getIncludeAdvanceCCTransactionDetails())) ? true
				: false);

		try {

			ReportsSearchCriteria search = new ReportsSearchCriteria();

			if (fromDate != null && !fromDate.equals("")) {
				if (reportOption.equals("CC_TRAN_DETAIL") || reportOption.equals("CC_REFUND_DETAIL")) {
					search.setDateRangeFrom(ReportsHTMLGenerator.convertDateFromDetail(fromDate));
				} else {
					search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate));
				}
			}
			if (toDate != null && !toDate.equals("")) {
				if (reportOption.equals("CC_TRAN_DETAIL") || reportOption.equals("CC_REFUND_DETAIL")) {
					search.setDateRangeTo(ReportsHTMLGenerator.convertDateFromDetail(toDate));
				} else {
					search.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate));
				}
			}

			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}
			search.setIncludeAdvanceCCDetails(isIncludeAdvanceCCDetails);
			search.setEntity(entity);
			/**
			 * Choose the jasper according to the report options and the resultSet will be generating based on the
			 * option
			 */
			if (reportOption.equals("CC_TRAN") && isDetailReport) {
				value = "CSV";
				reportTemplate = "CCPaymnetDetailTR.jasper";
				resultSet = ModuleServiceLocator.getDataExtractionBD().getPerDayCCFraudDetails(search);
			} else if (reportOption.equals("CC_TOPUP") && isDetailReport) {
				value = "CSV";
				reportTemplate = "CCPaymnetDetailTR.jasper";
				resultSet = ModuleServiceLocator.getDataExtractionBD().getPerDayCCTopUpFraudDetails(search);
			} else if (reportOption.equals("CC_REFUND") && isDetailReport) {
				value = "CSV";
				reportTemplate = "CCRefundDetailTR.jasper";
				search.setReportType(ReservationInternalConstants.ExtPayTxStatus.REFUNDED);
				resultSet = ModuleServiceLocator.getDataExtractionBD().getPerDayCCFraudDetails(search);
			} else if (reportOption.equals("CC_TRAN") || reportOption.equals("CC_TOPUP")) {
				reportTemplate = "CCPaymnetTR.jasper";
				if (reportOption.equals("CC_TOPUP")) {
					resultSet = ModuleServiceLocator.getDataExtractionBD().getCCTOPTransactionDetails(search);
				} else
					resultSet = ModuleServiceLocator.getDataExtractionBD().getCCTransactionDetails(search);
			} else if (reportOption.equals("CC_TRAN_DETAIL")) {
				reportTemplate = "CCPaymnetDetailTR.jasper";
				resultSet = ModuleServiceLocator.getDataExtractionBD().getPerDayCCFraudDetails(search);
			} else if (reportOption.equals("CC_REFUND")) {
				reportTemplate = "CCPaymnetTRRefund.jasper";
				resultSet = ModuleServiceLocator.getDataExtractionBD().getCCTransactionRefundDetails(search);
			} else if (reportOption.equals("CC_REFUND_DETAIL")) {
				reportTemplate = "CCRefundDetailTR.jasper";
				search.setReportType(ReservationInternalConstants.ExtPayTxStatus.REFUNDED);
				resultSet = ModuleServiceLocator.getDataExtractionBD().getPerDayCCFraudDetails(search);
			} else {
				reportTemplate = "CCPaymnetTRPendingRefund.jasper";
				resultSet = ModuleServiceLocator.getDataExtractionBD().getCCTransactionPendingRefundDetails(search);
			}

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("FROM_DATE", fromDate);
			parameters.put("TO_DATE", toDate);
			parameters.put("ID", id);
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
			parameters.put("INCLUDE_ADVANCE_CC_DETAILS", isIncludeAdvanceCCDetails);
			parameters.put("USE_EXISTING_FORMAT", !isIncludeAdvanceCCDetails);
			parameters.put("ENTITY", entityText);
			parameters.put("ENTITY_CODE", entity);

			// To provide Report Format Options
			String reportNumFormat = request.getParameter("radRptNumFormat");
			ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

			if (value.trim().equals("HTML")) {
				ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
						ReportsHTMLGenerator.getReportTemplate(reportTemplate));
				parameters.put("IMG", image);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null, null,
						response);
			} else if (value.trim().equals("PDF")) {
				ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
						ReportsHTMLGenerator.getReportTemplate(reportTemplate));
				image = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG", image);
				response.reset();
				if (reportOption.equals("CC_TRAN_DETAIL")) {
					response.addHeader("Content-Disposition", "attachment;filename=DailyCCTransactionReport.pdf");
				} else if (reportOption.equals("CC_REFUND_DETAIL")) {
					response.addHeader("Content-Disposition", "attachment;filename=DailyCCRefundReport.pdf");
				} else if (reportOption.equals("CC_TRAN") && isDetailReport) {
					response.addHeader("Content-Disposition", "attachment;filename=CCTransactionDetailReport.pdf");
				} else if (reportOption.equals("CC_REFUND") && isDetailReport) {
					response.addHeader("Content-Disposition", "attachment;filename=CCRefundTransactionDetailReport.pdf");
				} else {
					response.addHeader("Content-Disposition", "attachment;filename=CCTransactionReport.pdf");
				}
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("EXCEL")) {
				ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
						ReportsHTMLGenerator.getReportTemplate(reportTemplate));
				response.reset();
				if (reportOption.equals("CC_TRAN_DETAIL")) {
					response.addHeader("Content-Disposition", "attachment;filename=DailyCCTransactionReport.xls");
				} else if (reportOption.equals("CC_REFUND_DETAIL")) {
					response.addHeader("Content-Disposition", "attachment;filename=DailyCCRefundReport.xls");
				} else if (reportOption.equals("CC_TRAN") && isDetailReport) {
					response.addHeader("Content-Disposition", "attachment;filename=CCTransactionDetailReport.xls");
				} else if (reportOption.equals("CC_REFUND") && isDetailReport) {
					response.addHeader("Content-Disposition", "attachment;filename=CCRefundTransactionDetailReport.xls");
				} else {
					response.addHeader("Content-Disposition", "attachment;filename=CCTransactionReport.xls");
				}
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("CSV")) {
				ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
						ReportsHTMLGenerator.getReportTemplate(reportTemplate));
				response.reset();
				if (reportOption.equals("CC_TRAN_DETAIL")) {
					response.addHeader("Content-Disposition", "attachment;filename=DailyCCTransactionReport.csv");
				} else if (reportOption.equals("CC_REFUND_DETAIL")) {
					response.addHeader("Content-Disposition", "attachment;filename=DailyCCRefundReport.csv");
				} else if (reportOption.equals("CC_TRAN") && isDetailReport) {
					response.addHeader("Content-Disposition", "attachment;filename=CCTransactionDetailReport.csv");
				} else if (reportOption.equals("CC_REFUND") && isDetailReport) {
					response.addHeader("Content-Disposition", "attachment;filename=CCRefundTransactionDetailReport.csv");
				} else {
					response.addHeader("Content-Disposition", "attachment;filename=CCTransactionReport.csv");
				}
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);
			}
		} catch (Exception e) {
			log.error(e);
		}
	}
	
	private static void setInvoiceEntities(HttpServletRequest request) throws ModuleException {
		request.setAttribute(WebConstants.REQ_ENTITIES, SelectListGenerator.createEntityListByName());
	}
}
