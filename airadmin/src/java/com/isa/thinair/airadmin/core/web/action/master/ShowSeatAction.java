package com.isa.thinair.airadmin.core.web.action.master;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.handler.master.SeatMapRH;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Results({ @Result(name = AdminStrutsConstants.AdminAction.SUCCESS, value = AdminStrutsConstants.AdminJSP.SEATMAP_CHG_TMPLT_JSP),
		@Result(name = AdminStrutsConstants.AdminAction.VIEW, value = AdminStrutsConstants.AdminJSP.SEATMAP_ADMIN_JSP),
		@Result(name = AdminStrutsConstants.AdminAction.ERROR, value = AdminStrutsConstants.AdminJSP.ERROR_JSP) })
public class ShowSeatAction extends BaseRequestAwareAction {

	public String execute() throws Exception {
		return SeatMapRH.execute(request);
	}
}
