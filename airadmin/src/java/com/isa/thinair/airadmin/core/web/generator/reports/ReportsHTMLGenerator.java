/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airadmin.core.web.generator.reports;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airmaster.api.model.Airport;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.airsecurity.api.dto.external.UserTO;
import com.isa.thinair.airsecurity.api.utils.Constants;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.model.AgentIncentive;
import com.isa.thinair.airtravelagents.api.model.AgentIncentiveSlab;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.platform.api.constants.PlatformConstants;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants.PromotionCriteriaTypes;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants.PromotionCriteriaTypesDesc;
import com.isa.thinair.reporting.api.model.PaymentMode;
import com.isa.thinair.webplatform.api.v2.util.DateUtil;
import com.isa.thinair.webplatform.core.commons.AgentStatus;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.webplatform.core.util.LookupUtils;

/**
 * @author Chamindap
 * 
 */
public class ReportsHTMLGenerator {

	private static GlobalConfig globalConfig = CommonsServices.getGlobalConfig();

	private static final String SINGLE_QUOTE = "%27";

	/**
	 * Creates Payment Mode Multiselect Box
	 * 
	 * @param includeLoyaltyPayment
	 * 
	 * @return String created MultiSelect Box Array
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final static String createPaymentModeHtml(boolean includeLoyaltyPayment) throws ModuleException {
		Collection<PaymentMode> payments = null;
		StringBuffer sb = new StringBuffer("var arrData = new Array();");
		sb.append("var methods = new Array();");

		payments = ModuleServiceLocator.getDataExtractionBD().getPaymentsMode();
		int tempInt = 0;
		PaymentMode paymentMode = null;

		Iterator<PaymentMode> rolesIter = payments.iterator();
		while (rolesIter.hasNext()) {

			paymentMode = rolesIter.next();
			if (ReservationTnxNominalCode.LOYALTY_PAYMENT.getCode() == Integer.parseInt(paymentMode.getPaymentCode())
					&& !includeLoyaltyPayment) {
				continue;
			}
			sb.append("methods[" + tempInt + "] = new Array('" + paymentMode.getPaymentDescription() + "','"
					+ paymentMode.getPaymentCode() + "'); ");
			tempInt++;
		}

		sb.append("arrData[0] = methods;");
		sb.append(" var lspm = new Listbox('lstPModes', 'lstAssignedPModes', 'spn2', 'lspm');");
		sb.append("lspm.group1 = arrData[0];");
		sb.append("lspm.height = '100px'; lspm.width = '170px';");
		sb.append("lspm.headingLeft = '&nbsp;&nbsp;All Payment Modes';");
		sb.append("lspm.headingRight = '&nbsp;&nbsp;Selected Payment Modes';");
		sb.append("lspm.drawListBox();");
		return sb.toString();
	}

	
	/**
	 * Creates Airport list by Country wise Multi=select Box
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public final static String getAirportListHtml() throws ModuleException {
		String airportCodes=SelectListGenerator.getCountryWiseActiveAirports();
		return airportCodes;
	}
	/**
	 * Creates Sales Channel Multiselect Box
	 * 
	 * @return String created MultiSelect Box Array
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final static String createSalesChannelHtml() throws ModuleException {
		Collection<String[]> channels = null;
		StringBuffer sb = new StringBuffer("var arrData1 = new Array();");
		sb.append("var methods1 = new Array();");

		channels = SelectListGenerator.createSalesChannelList();

		Iterator<String[]> iter = channels.iterator();

		int i = 0;
		while (iter.hasNext()) {
			String s[] = iter.next();
			String code = s[0];
			String desc1 = StringUtils.replace(s[1], "'", SINGLE_QUOTE);

			sb.append("methods1[" + i + "] = new Array('" + desc1 + "','" + code + "'); ");
			i++;
		}

		sb.append("arrData1[0] = methods1;");
		sb.append(" var lspm1 = new Listbox('lstSChannels', 'lstAssignedSChannels', 'spn6', 'lspm1');");
		sb.append("lspm1.group1 = arrData1[0];");
		sb.append("lspm1.height = '100px'; lspm1.width = '150px';");
		sb.append("lspm1.headingLeft = '&nbsp;&nbsp;All Sales Channels';");
		sb.append("lspm1.headingRight = '&nbsp;&nbsp;Selected Sales Channels';");
		sb.append("lspm1.drawListBox();");
		return sb.toString();
	}

	/**
	 * Creates Chargers Multiselect box.
	 * 
	 * @return String created MultiSelect Box Array
	 */
	public final static String createChargesList() {

		Collection<String[]> chargers = SelectListGenerator.createChargesList();

		StringBuffer sb = new StringBuffer("var arrChargesData = new Array();");
		sb.append("var chargers = new Array();");

		Iterator<String[]> iter = chargers.iterator();

		int i = 0;
		while (iter.hasNext()) {
			String s[] = iter.next();
			String code = s[0];
			String desc1 = StringUtils.replace(s[2], "'", SINGLE_QUOTE);

			sb.append("chargers[" + i + "] = new Array('" + desc1 + "','" + code + "'); ");
			i++;
		}

		sb.append("arrChargesData[0] = chargers;");
		sb.append(" var lsChargers = new Listbox('lstChagresCodes', 'lstAssignedChargesCodes', 'spnCharges', 'lsChargers');");
		sb.append("lsChargers.group1 = arrChargesData[0];");
		sb.append("lsChargers.height = '75px'; lsChargers.width = '350px';");
		sb.append("lsChargers.headingLeft = '&nbsp;&nbsp;All Charges Codes';");
		sb.append("lsChargers.headingRight = '&nbsp;&nbsp;Selected Charges Codes';");
		sb.append("lsChargers.drawListBox();");
		return sb.toString();
	}

	public final static String createChargesListWithGroup() {

		Collection<String[]> chargers = SelectListGenerator.createChargesListWithGroup();

		StringBuffer sb = new StringBuffer("var arrChargesData = new Array();");
		sb.append("var chargers = new Array();");

		Iterator<String[]> iter = chargers.iterator();
		sb.append("var groups_arr = new Array(); ");
		Map<String, StringBuilder> groupMap = new HashMap<String, StringBuilder>();
		int i = -1;
		String currGrp = null;
		while (iter.hasNext()) {
			String s[] = iter.next();
			String code = s[0];
			String desc1 = StringUtils.replace(s[3], "'", SINGLE_QUOTE);
			String group_desc = s[1];
			String group_code = "(" + s[2] + ")";
			if (!group_code.equals(currGrp)) {
				currGrp = new String(group_code);
				i++;
			}
			// assuming already grouped by list.
			if (groupMap.get(group_code) == null) {
				sb.append("groups_arr[" + i + "] = new Array('" + group_desc + group_code + "','" + s[2] + "');");
				StringBuilder sbb = new StringBuilder();
				sbb.append("chargers[" + i + "] = new Array(); ");
				sbb.append("chargers[" + i + "][0] = new Array('" + desc1 + "','" + code + "'); ");
				groupMap.put(group_code, sbb);
			} else {
				groupMap.get(group_code).append("chargers[" + i + "].push(new Array('" + desc1 + "','" + code + "')); ");
			}

		}
		for (Entry<String, StringBuilder> strings : groupMap.entrySet()) {
			sb.append(strings.getValue());
		}

		sb.append("arrChargesData[0] = chargers;");
		sb.append(" var lsChargers = new Listbox('lstChagresCodes', 'lstAssignedChargesCodes', 'spnCharges', 'lsChargers');");
		sb.append("lsChargers.group1 = groups_arr;");
		sb.append(" lsChargers.list1 = chargers;");
		sb.append("lsChargers.height = '75px'; lsChargers.width = '350px';");
		sb.append("lsChargers.headingLeft = '&nbsp;&nbsp;All Charges Codes';");
		sb.append("lsChargers.headingRight = '&nbsp;&nbsp;Selected Charges Codes';");
		sb.append("lsChargers.drawListBox();");
		return sb.toString();
	}

	/**
	 * Creates Carrier Code Multiselect Box
	 * 
	 * @return String created MultiSelect Box Array
	 * @throws ModuleException
	 *             the ModuleException
	 */

	public final static String createCarrierCodeHtml(Collection<String> carrierCodes) throws ModuleException {

		StringBuffer sb = new StringBuffer("var arrData = new Array();");
		sb.append("var methods = new Array();");

		int tempInt = 0;
		String carrierCode;

		Iterator<String> carrierIter = carrierCodes.iterator();

		while (carrierIter.hasNext()) {
			carrierCode = carrierIter.next();
			sb.append("methods[" + tempInt + "] = new Array('" + carrierCode + "','" + carrierCode + "'); ");
			tempInt++;
		}

		sb.append("arrData[0] = methods;");
		sb.append(" var lscc = new Listbox('lstCarrierCodes', 'lstAssignedCarrierCodes', 'spn2', 'lscc');");
		sb.append("lscc.group1 = arrData[0];");
		sb.append("lscc.height = '75px'; lscc.width = '170px';");
		sb.append("lscc.headingLeft = '&nbsp;&nbsp;All Carrier Codes';");
		sb.append("lscc.headingRight = '&nbsp;&nbsp;Selected Carrier Codes';");
		sb.append("lscc.drawListBox();");
		return sb.toString();
	}

	/**
	 * Creates Carrier Code with default carrier Multiselect Box
	 * 
	 * @return String created MultiSelect Box Array
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final static String createCarrierCodeWithDefaultHtml(Collection<String> userCarrierCodes) throws ModuleException {

		String defaultCarrierCode = globalConfig.getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE);

		StringBuffer sb = new StringBuffer();
		if (userCarrierCodes != null && userCarrierCodes.size() > 0) {
			sb.append("var showCarrierCombo = true;");
			sb.append("var defaultCarrier = '" + defaultCarrierCode + "';");
			sb.append("var arrData = new Array();");
			sb.append("var methods = new Array();");
			int tempInt = 0;
			String carrierCode;

			Iterator<String> carrierIter = userCarrierCodes.iterator();
			// sb.append("methods[" + tempInt + "] = new Array('" + defaultCarrierCode + "','" + defaultCarrierCode +
			// "'); ");
			// tempInt = tempInt + 1;

			while (carrierIter.hasNext()) {
				carrierCode = carrierIter.next();
				sb.append("methods[" + tempInt + "] = new Array('" + carrierCode + "','" + carrierCode + "'); ");
				tempInt++;
			}

			sb.append("arrData[0] = methods;");
			sb.append("var lscc2 = new Listbox('lstCarrierCodes1', 'lstAssignedCarrierCodes1', 'spn3', 'lscc2');");
			sb.append("lscc2.group1 = arrData[0];");
			sb.append("lscc2.height = '75px'; lscc2.width = '170px';");
			sb.append("lscc2.headingLeft = '&nbsp;&nbsp;All Carrier Codes';");
			sb.append("lscc2.headingRight = '&nbsp;&nbsp;Selected Carrier Codes';");
			sb.append("lscc2.drawListBox();");
		} else {
			sb.append("var showCarrierCombo = false;");
		}

		return sb.toString();

	}

	/**
	 * Creates Carrier Codes Multiselect Box
	 * 
	 * @return String created MultiSelect Box Array
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final static String createCarrierCodesHtml() throws ModuleException {
		Collection<String[]> carriers = null;
		StringBuffer sb = new StringBuffer("var arrCarrierCodes = new Array();");
		sb.append("var methods = new Array();");
		carriers = SelectListGenerator.createActiveCarrierListWithCode();
		Iterator<String[]> iter = carriers.iterator();

		int i = 0;
		while (iter.hasNext()) {
			String s[] = iter.next();
			String code = s[1];
			String value = StringUtils.replace(s[1], "'", SINGLE_QUOTE);

			sb.append("methods[" + i + "] = new Array('" + code + "','" + value + "'); ");
			i++;
		}

		sb.append("arrCarrierCodes[0] = methods;");
		sb.append(" var lscc = new Listbox('lstCCodes', 'lstAssignedCCodes', 'spn4', 'lscc');");
		sb.append("lscc.group1 = arrCarrierCodes[0];");
		sb.append("lscc.height = '100px'; lscc.width = '150px';");
		sb.append("lscc.headingLeft = '&nbsp;&nbsp;Carrier Codes';");
		sb.append("lscc.headingRight = '&nbsp;&nbsp;Selected Carrier Codes';");
		sb.append("lscc.drawListBox();");
		return sb.toString();
	}

	/**
	 * Creates Carrier Code with default carrier Multiselect Box
	 * 
	 * @return String created MultiSelect Box Array
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final static String createCarrierCodesHtml(Collection<String> userCarrierCodes) throws ModuleException {

		String defaultCarrierCode = globalConfig.getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE);

		StringBuffer sb = new StringBuffer();
		if (userCarrierCodes != null && userCarrierCodes.size() > 0) {
			sb.append("var showCarrierCombo = true;");
			sb.append("var defaultCarrier = '" + defaultCarrierCode + "';");
			sb.append("var arrData = new Array();");
			sb.append("var methods = new Array();");
			int tempInt = 0;
			String carrierCode;

			Iterator<String> carrierIter = userCarrierCodes.iterator();
			sb.append("methods[" + tempInt + "] = new Array('" + defaultCarrierCode + "','" + defaultCarrierCode + "'); ");
			tempInt = tempInt + 1;

			while (carrierIter.hasNext()) {
				carrierCode = carrierIter.next();
				sb.append("methods[" + tempInt + "] = new Array('" + carrierCode + "','" + carrierCode + "'); ");
				tempInt++;
			}

			sb.append("arrData[0] = methods;");
			sb.append("var lscc2 = new Listbox('lstCarrierCodes1', 'lstAssignedCarrierCodes1', 'spn3', 'lscc2');");
			sb.append("lscc2.group1 = arrData[0];");
			sb.append("lscc2.height = '75px'; lscc2.width = '170px';");
			sb.append("lscc2.headingLeft = '&nbsp;&nbsp;All Carrier Codes';");
			sb.append("lscc2.headingRight = '&nbsp;&nbsp;Selected Carrier Codes';");
			sb.append("lscc2.drawListBox();");
		} else {
			sb.append("var showCarrierCombo = false;");
		}

		return sb.toString();

	}

	/**
	 * Creates Payment Mode Multiselect Box
	 * 
	 * @return String created MultiSelect Box Array
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final static String createStationList() throws ModuleException {
		List<Map<String, String>> stations = null;
		StringBuffer sb = new StringBuffer("var arrstnData = new Array();");
		sb.append("var stations = new Array();");
		String stn = null;
		stations = SelectListGenerator.createStationCodes();
		int tempInt = 0;
		Iterator<Map<String, String>> stnIter = stations.iterator();
		while (stnIter.hasNext()) {
			Map<String, String> keyValues = stnIter.next();
			Set<String> keys = keyValues.keySet();
			Iterator<String> keyIterator = keys.iterator();

			while (keyIterator.hasNext()) {
				stn = keyValues.get(keyIterator.next());
				sb.append("stations[" + tempInt + "] = new Array('" + stn + "','" + stn + "'); ");
				tempInt++;
			}
		}

		sb.append("arrstnData[0] = stations;");
		sb.append(" var lsstn = new Listbox('lstStns', 'lstAssignedStnss', 'spn3', 'lsstn');");
		sb.append("lsstn.group1 = arrstnData[0];");
		sb.append("lsstn.height = '200px'; lsstn.width = '170px';");
		sb.append("lsstn.headingLeft = '&nbsp;&nbsp;All Point of Sales';");
		sb.append("lsstn.headingRight = '&nbsp;&nbsp;Selected Point of Sales';");
		sb.append("lsstn.drawListBox();");
		return sb.toString();
	}

	/**
	 * Create Ancillary List Multiselect Box
	 * 
	 * @return String created MultiSelect Box Array
	 * @throws ModuleException
	 */
	public final static String createAncillaryList() throws ModuleException {
		StringBuffer sb = new StringBuffer("var arrAnciTypeData = new Array();");
		sb.append("var anciTypes = new Array();");

		sb.append("anciTypes[0] = new Array('Seat Map','SEAT_MAP'); ");
		sb.append("anciTypes[1] = new Array('Meals','MEALS'); ");
		sb.append("anciTypes[2] = new Array('SSR','SSR'); ");
		sb.append("anciTypes[3] = new Array('Hala','HALA'); ");
		sb.append("anciTypes[4] = new Array('Insurance','INSURANCE'); ");
		sb.append("anciTypes[5] = new Array('Baggages','BAGGAGES'); ");
		sb.append("anciTypes[6] = new Array('Airport Tranfer','AIRPORT_TRANSFER'); ");

		sb.append("arrAnciTypeData[0] = anciTypes;");
		sb.append(" var lsanci = new Listbox('lsAnci', 'lstAssignedAnci', 'spn4', 'lsanci');");
		sb.append("lsanci.group1 = arrAnciTypeData[0];");
		sb.append("lsanci.height = '100px'; lsanci.width = '170px';");
		sb.append("lsanci.headingLeft = '&nbsp;&nbsp;All Ancillary';");
		sb.append("lsanci.headingRight = '&nbsp;&nbsp;Selected Ancillary';");
		sb.append("lsanci.drawListBox();");
		return sb.toString();
	}

	/**
	 * Creates a Country Multiselect Box
	 * 
	 * @return String the Created Country Array
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final static String createCountryHtml() throws ModuleException {
		Collection<Airport> airportList = null;
		StringBuffer sb = new StringBuffer("var arrData = new Array();");
		sb.append("var cntries = new Array();");

		airportList = ModuleServiceLocator.getAirportServiceBD().getAirports();
		int tempInt = 0;
		Airport country = null;

		Iterator<Airport> countryIte = airportList.iterator();
		while (countryIte.hasNext()) {
			country = countryIte.next();

			sb.append("cntries[" + tempInt + "] = new Array('" + country.getAirportCode() + "','" + country.getAirportCode()
					+ "'); ");
			tempInt++;
		}

		sb.append("arrData[0] = cntries;");
		sb.append(" var ls = new Listbox('lstCountries', 'lstCountriesAssigned', 'spn1');");
		sb.append("ls.group1 = arrData[0];");
		sb.append("ls.height = '165px'; ls.width = '170px';");
		sb.append("ls.headingLeft = '&nbsp;&nbsp;All Airports';");
		sb.append("ls.headingRight = '&nbsp;&nbsp;Assigned Airports';");
		sb.append("ls.drawListBox();");
		return sb.toString();
	}

	/**
	 * Creates Agent Multselect Box for Agent Types
	 * 
	 * @param strAgentType
	 *            the Agent Type
	 * @param blnWithTAs
	 *            the true - with Reporting TAs
	 * @param blnWithCOs
	 *            the true -With Reporting COs
	 * @return String the Created Multi Select Box
	 * @throws ModuleException
	 *             the ModuleException
	 */
	protected final static String createAgentGSAMultiSelectByStation(String strAgentType, boolean blnWithTAs, boolean blnWithCOs)
			throws ModuleException {

		StringBuffer sb = new StringBuffer();
		Collection<String[]> agents = null;
		Collection<String[]> gsas = null;
		String strAssignGSAHtml = "";
		strAssignGSAHtml = "var ls = new Listbox('lstRoles', 'lstAssignedRoles', 'spn1');";

		if (strAgentType != null) {
			if (!strAgentType.equals("") && !strAgentType.equals("All") && !blnWithTAs && !blnWithCOs) {

				sb.append("var agntArr = new Array(); ");
				String agentTypes[] = { strAgentType };
				agents = SelectListGenerator.createAgentsByType(agentTypes);
				if (!agents.isEmpty()) {
					int tempInt = 0;
					Iterator<String[]> agentIter = agents.iterator();

					while (agentIter.hasNext()) {
						String str[] = agentIter.next();
						sb.append("agntArr[" + tempInt + "] = ");
						sb.append(" new Array('" + str[0] + "','" + str[1] + "'); ");
						tempInt++;
					}
					strAssignGSAHtml = "var ls = new Listbox('lstRoles', 'lstAssignedRoles', 'spn1');" + "ls.group1 = agntArr;";
				}

			} else if (!strAgentType.equals("") && !strAgentType.equals("All") && (blnWithTAs || blnWithCOs)) {

				String[] agentType = new String[5];
				agentType[0] = strAgentType;
				String[] gsaAraay = null;
				String strGsaCode = "";
				if (blnWithTAs && blnWithCOs) {
					agentType[1] = "SGSA";
					agentType[2] = "TA";
					agentType[3] = "STA";
					agentType[4] = "CO";
				} else if (blnWithCOs) {
					agentType[1] = "CO";
					agentType[2] = "";
					agentType[3] = "";
					agentType[4] = "";
				} else {
					agentType[1] = "SGSA";
					agentType[2] = "TA";
					agentType[3] = "STA";
					agentType[4] = "";
				}

				gsas = SelectListGenerator.createGSATAList(agentType);
				if (!gsas.isEmpty()) {
					sb.append("var agntArr = new Array(); ");
					sb.append("var agents = new Array(); ");
					int tempCat = -1;
					int tinc = 0;
					int check = 0;
					Iterator<String[]> gsaIter = gsas.iterator();
					while (gsaIter.hasNext()) {
						gsaAraay = gsaIter.next();
						if (strGsaCode.equals(gsaAraay[0])) {
							strGsaCode = gsaAraay[0];
							sb.append("agentItem_" + tempCat + "[" + tinc + "] = ");
							sb.append(" new Array('" + gsaAraay[5] + "','" + gsaAraay[4] + "'); ");
							tinc++;

						} else {

							tempCat++;
							tinc = 0;
							strGsaCode = gsaAraay[0];
							sb.append("agntArr[" + tempCat + "] = ");
							sb.append(" new Array('" + gsaAraay[1] + "','" + gsaAraay[0] + "'); ");
							sb.append("var agentItem_" + tempCat + " = new Array();  ");

							if (gsaAraay[4] != null) {

								sb.append("agentItem_" + tempCat + "[" + tinc + "] = ");
								sb.append(" new Array('" + gsaAraay[5] + "','" + gsaAraay[4] + "'); ");
								tinc++;
							}

						}
						if (tempCat > check) {
							sb.append("agents[" + check + "] = agentItem_" + check + ";");
							check++;
						}

					}
					sb.append("agents[" + check + "] = agentItem_" + check + ";");

					strAssignGSAHtml = "var arrGroup2 = new Array();" + "var arrData2 = new Array();"
							+ "arrData2[0] = new Array();" + "var ls = new Listbox('lstRoles', 'lstAssignedRoles', 'spn1');"
							+ "ls.group1 = agntArr;" + "ls.group2 = arrGroup2;" + "ls.list1 = agents;" + "ls.list2 = arrData2;";

				}
			} else if (strAgentType.equals("All")) {

				List<Map<String, String>> lstAgents = SelectListGenerator.createAgentTypesList();
				Iterator<Map<String, String>> ite = lstAgents.iterator();

				sb.append("var agntArr = new Array(); ");
				sb.append("var agents = new Array();");

				int tempCat = 0;
				int tinc = 0;
				String agentTpes[] = new String[1];
				String agentType = null;
				Collection<String[]> col = null;
				Map<String, String> keyValues = null;
				Set<String> keys = null;
				Iterator<String> keyIterator = null;
				Iterator<String[]> iteAgents = null;

				while (ite.hasNext()) {

					keyValues = ite.next();
					keys = keyValues.keySet();
					keyIterator = keys.iterator();

					while (keyIterator.hasNext()) {

						agentType = keyValues.get(keyIterator.next());
						sb.append("agntArr[" + tempCat + "] = '" + agentType + "'; ");
						agentTpes[0] = agentType;

						col = SelectListGenerator.createAgentsByType(agentTpes);

						if (col != null) {
							sb.append("agentItem_" + tempCat + " = new Array(); ");

						}
						if (col.size() == 0) {
							sb.append("new Array(), ");

						} else {
							iteAgents = col.iterator();
							tinc = 0;
							while (iteAgents.hasNext()) {

								String str[] = iteAgents.next();
								sb.append("agentItem_" + tempCat + "[" + tinc + "] =  ");
								sb.append(" new Array('" + str[0] + "','" + str[1] + "') ;");
								tinc++;
							}

						}

					}
					sb.append("agents[" + tempCat + "] = agentItem_" + tempCat + ";");
					tempCat++;

				}

				strAssignGSAHtml = "var arrGroup2 = new Array();" + "var arrData2 = new Array();" + "arrData2[0] = new Array();"
						+ "var ls = new Listbox('lstRoles', 'lstAssignedRoles', 'spn1');" + "ls.group1 = agntArr;"
						+ "ls.list1 = agents;";
			}
		}

		sb.append(strAssignGSAHtml);
		sb.append("ls.height = '190px'; ls.width  = '350px';");
		sb.append("ls.headingLeft = '&nbsp;&nbsp;All Agents';");
		sb.append("ls.headingRight = '&nbsp;&nbsp;Selected Agents';");
		sb.append("ls.drawListBox();");

		return sb.toString();
	}

	/**
	 * Creates a Agents Multi select Box for a given GSA
	 * 
	 * @param gsaCode
	 *            the GSA
	 * @param blnWithTAs
	 *            the true false
	 * @return String created multiselect Box
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final static String createTAsOfGSAMultiSelect(String gsaCode, boolean blnWithTAs) throws ModuleException {

		StringBuffer sb = new StringBuffer();
		List<String[]> gsas = null;

		if (gsaCode != null && !gsaCode.equals("") && !gsaCode.equals("All") && blnWithTAs) {

			String[] agentType = new String[1];
			agentType[0] = gsaCode;
			String[] gsaAraay = null;
			gsas = (List<String[]>) SelectListGenerator.createTasFromGSA(agentType);
			if (!gsas.isEmpty()) {
				sb.append("var arrGsas = new Array();");
				int i = 0;
				sb.append(" arrGsas[" + i + "] = new Array('" + (gsas.get(0))[1] + "','" + (gsas.get(0))[0] + "') ;");

				Iterator<String[]> gsaIter = gsas.iterator();
				i = i + 1;
				while (gsaIter.hasNext()) {
					gsaAraay = gsaIter.next();
					sb.append("arrGsas[" + i + "] =  new Array('" + gsaAraay[3] + "','" + gsaAraay[2] + "') ;");
					i++;
				}

				sb.append("var arrGroup2 = new Array();");
				sb.append("var arrData2 = new Array(); arrData2[0] = new Array();");
				sb.append("var ls = new Listbox('lstRoles', 'lstAssignedRoles', 'spn1');");
				sb.append("ls.group1 = arrGsas;" + "ls.group2 = arrGroup2;");
			}
		} else {

			sb.append("var ls = new Listbox('lstRoles', 'lstAssignedRoles', 'spn1');");

		}
		sb.append("ls.height = '200px'; ls.width  = '350px';");
		sb.append("ls.headingLeft = '&nbsp;&nbsp;All Agents';");
		sb.append("ls.headingRight = '&nbsp;&nbsp;Selected Agents';");
		sb.append("ls.drawListBox();");

		return sb.toString();
	}

	/**
	 * Creates a Multiselect box According to Agent type
	 * 
	 * @param strAgentType
	 *            the Agent Type
	 * @return String the Created Multi select Box
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final static String createUserMultiSelect(String strAgentType) throws ModuleException {

		StringBuffer sb = new StringBuffer();
		Collection<String[]> users = null;
		Collection<Agent> agents = null;
		String user[] = null;
		Agent gsa = null;

		if (strAgentType != null && !strAgentType.equals("")) {
			agents = ModuleServiceLocator.getTravelAgentBD().getAgents(strAgentType);
			if (!agents.isEmpty()) {

				sb.append("var arrAgncs = new Array();");
				sb.append("var agArray = new Array();");
				int tempCat = 0;
				int tempInt = 0;
				Iterator<Agent> gsaIter = agents.iterator();
				String agentCode[] = new String[1];
				Iterator<String[]> userIter = null;

				while (gsaIter.hasNext()) {
					gsa = gsaIter.next();
					sb.append("arrAgncs[" + tempCat + "] = ");
					sb.append(" new Array('" + gsa.getAgentDisplayName() + "','" + gsa.getAgentCode() + "'); ");

					agentCode[0] = gsa.getAgentCode();
					users = SelectListGenerator.createAgentUsers(agentCode);

					if (!users.isEmpty()) {
						userIter = users.iterator();
					}

					sb.append("agitem_" + tempCat + " = new Array();  ");

					if (users.size() == 0) {
						sb.append("new Array();");
					}
					tempInt = 0;
					while (userIter != null && userIter.hasNext()) {
						user = userIter.next();

						if (user != null && gsa.getAgentCode() != null) {
							sb.append("agitem_" + tempCat + "[" + tempInt + "] = ");
							sb.append(" new Array('" + user[0] + "','" + user[1] + "'); ");
							tempInt++;
						}
					}
					sb.append("agArray[" + tempCat + "] = agitem_" + tempCat + ";");
					tempCat++;

				}

				sb.append("var arrGroup2 = new Array();");
				sb.append("var arrData2 = new Array(); arrData2[0] = new Array();");
				sb.append("var ls = new Listbox('lstUsers', 'lstAssignedUsers', 'spn1');");
				sb.append("ls.group1 = arrAgncs;" + "ls.group2 = arrGroup2;");
				sb.append("ls.list1 = agArray;" + "ls.list2 = arrData2;");

			}
		} else {

			sb.append("var ls = new Listbox('lstUsers', 'lstAssignedUsers', 'spn1');");
		}
		sb.append("ls.height = '165px'; ls.width  = '350px';");
		sb.append("ls.headingLeft = '&nbsp;&nbsp;All Agent Users';");
		sb.append("ls.headingRight = '&nbsp;&nbsp;Selected Agent Users';");
		sb.append("ls.drawListBox();");

		return sb.toString();
	}

	/**
	 * 
	 * @TODO : Should make this a common method.
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public final static String createMultiSelect() throws ModuleException {

		// String defaultCarrierCode = globalConfig.getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE);
		Collection<String> data = new ArrayList<String>();
		data.add("Fully paid bookings");
		data.add("Partially paid bookings");
		// data.add("Atleast one payment bookings");

		StringBuffer sb = new StringBuffer();
		if (data != null && data.size() > 0) {
			sb.append("var showCarrierCombo = true;");
			// sb.append("var defaultCarrier = '" + defaultCarrierCode + "';");
			sb.append("var arrData = new Array();");
			sb.append("var methods = new Array();");
			int tempInt = 0;
			String carrierCode;

			Iterator<String> carrierIter = data.iterator();
			// sb.append("methods[" + tempInt + "] = new Array('" + defaultCarrierCode + "','" + defaultCarrierCode +
			// "'); ");
			// tempInt = tempInt + 1;
			int code = 1;
			while (carrierIter.hasNext()) {
				carrierCode = carrierIter.next();
				sb.append("methods[" + tempInt + "] = new Array('" + carrierCode + "','" + code + "'); ");
				tempInt++;
				code++;
			}

			sb.append("arrData[0] = methods;");
			sb.append("var lscc2 = new Listbox('lstBookingPayments', 'lstAssignedBookingPayments', 'spnPaymentTypes', 'lscc2');");
			sb.append("lscc2.group1 = arrData[0];");
			sb.append("lscc2.height = '75px'; lscc2.width = '350px';");
			sb.append("lscc2.headingLeft = '&nbsp;&nbsp;All payment modes';");
			sb.append("lscc2.headingRight = '&nbsp;&nbsp;Select reservations payment modes';");
			sb.append("lscc2.drawListBox();");
		} else {
			sb.append("var showCarrierCombo = false;");
		}

		return sb.toString();

	}

	/**
	 * Method convert date to dd-MMM-YYYY format
	 * 
	 * @param dateString
	 * @return String
	 */
	public static String convertDate(String dateString) throws ModuleException {
		return convertDate(dateString, false, null, false);
	}

	/**
	 * Converts the date to dd-MMM-YYYY format if convertToZule parameter is false, otherwise converts the date to
	 * dd-MMM-yyyy HH:mm:ss format.
	 * 
	 * The GMT difference is takes from the airport agent is assigned in.
	 * 
	 * When converting to Zulu(GMT) time fromDate will be takes at 00:00:00 time and toDates will be taken as 23:59:59.
	 * This will be determined by the toDate boolean parameter.
	 * 
	 * @param dateString
	 *            The date string in dd/MM/YYYY format
	 * @param convertToZulu
	 *            Whether this date should be converted to Zulu(GMT) time or not.
	 * @param userID
	 *            currentUser's ID(user name)
	 * @param toDate
	 *            Whether the date is going to be used as an end of a date range (to date of a from date and to date
	 *            pair).
	 * @return the formatted date according to the given parameters
	 * @throws ModuleException
	 */
	public static String convertDate(String dateString, boolean convertToZulu, String userID, boolean toDate)
			throws ModuleException {
		String date = null;
		int day = Integer.parseInt(dateString.substring(0, 2));
		int month = Integer.parseInt(dateString.substring(3, 5));
		int year = Integer.parseInt(dateString.substring(6));

		DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		Calendar cal = new GregorianCalendar(year, (month - 1), day);
		if (convertToZulu && userID != null) {
			if (toDate) {
				cal = new GregorianCalendar(year, (month - 1), day, 23, 59, 59);
			}
			int time_difference = ModuleServiceLocator.getSecurityBD().getUserGMTTimeDifference(userID);
			cal.add(Calendar.MINUTE, time_difference);
			dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
		}
		date = dateFormat.format(cal.getTime());

		return date;
	}
	
	public static String convertDateToFirstOfMonth(String dateString)
			throws ModuleException {
		String date = null;
		int day = 1;
		int month = Integer.parseInt(dateString.substring(3, 5));
		int year = Integer.parseInt(dateString.substring(6));

		DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		Calendar cal = new GregorianCalendar(year, (month - 1), day);
		date = dateFormat.format(cal.getTime());
		return date;
	}
	
	public static String convertDateToLastOfMonth(String dateString)
			throws ModuleException {
		String date = null;
		int day = Integer.parseInt(dateString.substring(0, 2));
		int month = Integer.parseInt(dateString.substring(3, 5));
		int year = Integer.parseInt(dateString.substring(6));
		
		DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		Calendar cal = new GregorianCalendar(year, (month - 1), day);
		day = Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH);
		cal = new GregorianCalendar(year, (month - 1), day);
		date = dateFormat.format(cal.getTime());

		return date;
	}

	public static int[] getYearMonthDay(String dateString) {
		int day = Integer.parseInt(dateString.substring(0, 2));
		int month = Integer.parseInt(dateString.substring(3, 5));
		int year = Integer.parseInt(dateString.substring(6));
		return new int[] { year, month, day };
	}

	/**
	 * conwert YYYY-MM-dd to a date
	 * 
	 * @param dateString
	 * @return
	 */
	public static String convertDateFromDetail(String dateString) {
		String date = null;
		int year = Integer.parseInt(dateString.substring(0, 4));
		int month = Integer.parseInt(dateString.substring(5, 7));
		int day = Integer.parseInt(dateString.substring(8));

		DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		Calendar cal = new GregorianCalendar(year, (month - 1), day);
		date = dateFormat.format(cal.getTime());

		return date;
	}

	/**
	 * Method String date to util date.
	 * 
	 * @param dateString
	 * @return String
	 */
	public static Timestamp toDateType(String dateString, boolean fromDate) {
		Timestamp dateTime = null;

		int day = Integer.parseInt(dateString.substring(0, 2));
		int month = Integer.parseInt(dateString.substring(3, 5));
		int year = Integer.parseInt(dateString.substring(6));

		Calendar cal = new GregorianCalendar(year, (month - 1), day);

		if (fromDate) {
			cal.set(Calendar.HOUR, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
		} else {
			cal.set(Calendar.HOUR, 23);
			cal.set(Calendar.MINUTE, 59);
			cal.set(Calendar.SECOND, 59);
		}

		dateTime = new Timestamp(cal.getTimeInMillis());

		return dateTime;
	}

	/**
	 * Gets the reports file path.
	 * 
	 * @param fileName
	 * @return
	 */
	public static String getReportTemplate(String fileName) {

		String reportsRootDir = null;
		reportsRootDir = PlatformConstants.getConfigRootAbsPath() + "/templates/reports/" + fileName;
		return reportsRootDir;
	}

	/**
	 * Gets users station wise bt not for gsa with ta.
	 * 
	 * @param strAgentType
	 * @param blnWithTAs
	 * 
	 * @return String an arry of users js file
	 */
	@SuppressWarnings("rawtypes")
	public final static String createAgentGSAUsersMultiSelect(String strAgentType, boolean blnWithTAs, boolean blnWithCOs)
			throws ModuleException {

		StringBuffer sb = new StringBuffer();
		Collection agents = null;
		Collection<String[]> gsas = null;
		String strAssignGSAHtml = "";
		boolean blnTA = false;
		boolean blnAll = false;

		strAssignGSAHtml = "var ls = new Listbox('lstRoles', 'lstAssignedRoles', 'spn1');";
		if (strAgentType != null) {

			if (strAgentType.equals("TA") || strAgentType.equals("All")) { // if TA or All selected

				if (strAgentType.equals("TA")) { // For TA get Station list
					blnTA = true;
					agents = SelectListGenerator.createStationCodes();
				}

				if (strAgentType.equals("All")) { // for All get Agent Type List
					blnAll = true;
					agents = SelectListGenerator.createStationCodes();
				}

				Iterator ite = agents.iterator();
				sb.append("var stns = new Array(); ");

				sb.append(" var usersArr = new Array();");
				// sb.append(" var agentsArr = new Array();");
				int tempCat = 0;
				int tinc = 0;
				Collection col = null;
				Map keyValues = null;
				Set keys = null;
				Iterator keyIterator = null;
				String agentTpes[] = new String[1];
				String agentType = "";

				while (ite.hasNext()) {

					keyValues = (Map) ite.next();
					keys = keyValues.keySet();
					keyIterator = keys.iterator();

					while (keyIterator.hasNext()) {

						agentType = (String) keyValues.get(keyIterator.next());

						// Populate the Group Array
						sb.append("stns[" + tempCat + "] = '" + agentType + "'; ");
						agentTpes[0] = agentType;

						if (blnTA) { // For TA get Station wise agents
							col = SelectListGenerator.createTAsByStation(agentTpes);
						}
						if (blnAll) {// For All get Agent type wise Agents
							// col =
							// SelectListGenerator.createAgentsByType(agentTpes);
							// sudheera
							col = SelectListGenerator.createAgentsByStation(agentTpes);
						}

						if (col != null) {
							sb.append("var agents_" + tempCat + " = new Array(); ");

						}
						if (col.size() == 0) {
							sb.append("new Array();");

						} else {
							Iterator iteAgents = col.iterator();
							tinc = 0;
							while (iteAgents.hasNext()) {

								String str[] = (String[]) iteAgents.next();
								sb.append("agents_" + tempCat + "[" + tinc + "] = ");
								sb.append("new Array ('" + str[0] + "','" + str[1] + "');");
								tinc++;

							}

						}
					}
					sb.append("agentsArr[" + tempCat + "] = agents_" + tempCat + ";");
					tempCat++;

				}

				strAssignGSAHtml = "var arrGroup2 = new Array();" + "var arrData2 = new Array();" + "arrData2[0] = new Array();"

				+ "var ls = new Listbox('lstRoles', 'lstAssignedRoles', 'spn1');" + "ls.group1 = stns;" + "ls.list1 = agentsArr;";

			} else if (!strAgentType.equals("") && !strAgentType.equals("All") && !blnWithTAs && !blnWithCOs) {

				sb.append("var agts = new Array(); ");
				String agentTypes[] = { strAgentType };
				agents = SelectListGenerator.createAgentsByType(agentTypes);
				if (!agents.isEmpty()) {
					int tempInt = 0;
					Iterator agentIter = agents.iterator();

					while (agentIter.hasNext()) {
						String str[] = (String[]) agentIter.next();
						sb.append("agts[" + tempInt + "] = new Array('" + str[0] + "','" + str[1] + "'); ");
						tempInt++;
					}
					strAssignGSAHtml = "var ls = new Listbox('lstRoles', " + "'lstAssignedRoles', 'spn1');" + "ls.group1 = agts;";
				}

			} else if (!strAgentType.equals("") && !strAgentType.equals("All") && (blnWithTAs || blnWithCOs)) {

				String[] agentType = new String[5];
				agentType[0] = strAgentType;
				String[] gsaAraay = null;
				String strGsaCode = "";
				int tinc = 0;
				if (blnWithTAs && blnWithCOs) {
					agentType[1] = "SGSA";
					agentType[2] = "TA";
					agentType[3] = "STA";
					agentType[4] = "CO";
				} else if (blnWithCOs) {
					agentType[1] = "CO";
					agentType[2] = "";
					agentType[3] = "";
					agentType[4] = "";
				} else {
					agentType[1] = "SGSA";
					agentType[2] = "TA";
					agentType[3] = "STA";
					agentType[4] = "";
				}
				gsas = SelectListGenerator.createGSATAList(agentType);
				if (!gsas.isEmpty()) {

					sb.append("var stns = new Array();");
					sb.append("var agentsArr = new Array();");
					int tempCat = -1;
					int check = 0;
					Iterator gsaIter = gsas.iterator();
					while (gsaIter.hasNext()) {
						gsaAraay = (String[]) gsaIter.next();
						if (strGsaCode.equals(gsaAraay[0])) {
							strGsaCode = gsaAraay[0];
							sb.append(" agents_" + tempCat + "[" + tinc + "] = ");
							sb.append("	new Array('" + gsaAraay[5] + "','" + gsaAraay[4] + "');");

							tinc++;
						} else {
							tempCat++;

							tinc = 0;
							sb.append("stns[" + tempCat + "] = new Array('" + gsaAraay[1] + "','" + gsaAraay[0] + "'); ");

							sb.append("var agents_" + tempCat + " = new Array(); ");

							strGsaCode = gsaAraay[0];

							if (gsaAraay[4] != null) {

								sb.append("agents_" + tempCat + "[" + tinc + "] = new Array('" + gsaAraay[5] + "','"
										+ gsaAraay[4] + "'); ");
								tinc++;
							}

						}

						if (tempCat > check) {
							sb.append("agentsArr[" + check + "] = agents_" + check + ";");
							check++;
						}

					}
					sb.append("agentsArr[" + check + "] = agents_" + check + ";");
					strAssignGSAHtml = "var arrGroup2 = new Array();" + "var arrData2 = new Array();"
							+ "arrData2[0] = new Array();"

							+ "var ls = new Listbox('lstRoles', 'lstAssignedRoles', 'spn1');"
							+ "ls.group1 = stns; ls.group2 = arrGroup2;" + "ls.list1 = agentsArr; ls.list2 = arrData2;";
				}
			}

		}
		sb.append(strAssignGSAHtml);
		sb.append("ls.height = '190px'; ls.width  = '350px';");
		sb.append("ls.headingLeft = '&nbsp;&nbsp;All Agents';");
		sb.append("ls.headingRight = '&nbsp;&nbsp;Selected Agents';");
		sb.append("ls.drawListBox();");

		return sb.toString();
	}

	
	@SuppressWarnings("rawtypes")
	public final static String createAgentGSAMultiSelect(String strAgentType, boolean blnWithTAs, boolean blnWithCOs)
			throws ModuleException {

		StringBuffer sb = new StringBuffer();
		Collection agents = null;
		Collection gsas = null;
		String strAssignGSAHtml = "";
		boolean blnTA = false;
		boolean blnAll = false;

		strAssignGSAHtml = "var ls = new Listbox('lstRoles', 'lstAssignedRoles', 'spn1');";
		if (strAgentType != null) {

			if (strAgentType.equals("TA") || strAgentType.equals("All")) { // if
																			// TA
																			// or
																			// All
																			// selected

				if (strAgentType.equals("TA")) { // For TA get Station list
					blnTA = true;
					agents = SelectListGenerator.createStationCodes();
				}

				if (strAgentType.equals("All")) { // for All get Agent Type
													// List
					blnAll = true;
					agents = SelectListGenerator.createStationCodes();
				}

				Iterator ite = agents.iterator();
				sb.append("var stns = new Array(); ");

				sb.append(" var agentsArr = new Array();");
				int tempCat = 0;
				int tinc = 0;
				Collection col = null;
				Map keyValues = null;
				Set keys = null;
				Iterator keyIterator = null;
				String agentTpes[] = new String[1];
				String agentType = "";

				while (ite.hasNext()) {

					keyValues = (Map) ite.next();
					keys = keyValues.keySet();
					keyIterator = keys.iterator();

					while (keyIterator.hasNext()) {

						agentType = (String) keyValues.get(keyIterator.next());

						// Populate the Group Array
						sb.append("stns[" + tempCat + "] = '" + agentType + "'; ");
						agentTpes[0] = agentType;

						if (blnTA) { // For TA get Station wise agents
							col = SelectListGenerator.createTAsByStation(agentTpes);
						}
						if (blnAll) {// For All get Agent type wise Agents
							// col =
							// SelectListGenerator.createAgentsByType(agentTpes);
							// sudheera
							col = SelectListGenerator.createAgentsByStation(agentTpes);
						}

						if (col != null) {
							sb.append("var agents_" + tempCat + " = new Array(); ");

						}
						if (col.size() == 0) {
							sb.append("new Array();");

						} else {
							Iterator iteAgents = col.iterator();
							tinc = 0;
							while (iteAgents.hasNext()) {

								String str[] = (String[]) iteAgents.next();
								sb.append("agents_" + tempCat + "[" + tinc + "] = ");
								sb.append("new Array ('" + str[0] + "','" + str[1] + "');");
								tinc++;

							}

						}
					}
					sb.append("agentsArr[" + tempCat + "] = agents_" + tempCat + ";");
					tempCat++;

				}

				strAssignGSAHtml = "var arrGroup2 = new Array();" + "var arrData2 = new Array();" + "arrData2[0] = new Array();"

				+ "var ls = new Listbox('lstRoles', 'lstAssignedRoles', 'spn1');" + "ls.group1 = stns;" + "ls.list1 = agentsArr;";

			} else if (!strAgentType.equals("") && !strAgentType.equals("All") && !blnWithTAs && !blnWithCOs) {

				sb.append("var agts = new Array(); ");
				String agentTypes[] = { strAgentType };
				agents = SelectListGenerator.createAgentsByType(agentTypes);
				if (!agents.isEmpty()) {
					int tempInt = 0;
					Iterator agentIter = agents.iterator();

					while (agentIter.hasNext()) {
						String str[] = (String[]) agentIter.next();
						sb.append("agts[" + tempInt + "] = new Array('" + str[0] + "','" + str[1] + "'); ");
						tempInt++;
					}
					strAssignGSAHtml = "var ls = new Listbox('lstRoles', " + "'lstAssignedRoles', 'spn1');" + "ls.group1 = agts;";
				}

			} else if (!strAgentType.equals("") && !strAgentType.equals("All") && (blnWithTAs || blnWithCOs)) {

				String[] agentType = new String[5];
				agentType[0] = strAgentType;
				String[] gsaAraay = null;
				String strGsaCode = "";
				int tinc = 0;
				if (blnWithTAs && blnWithCOs) {
					agentType[1] = "SGSA";
					agentType[2] = "TA";
					agentType[3] = "STA";
					agentType[4] = "CO";
				} else if (blnWithCOs) {
					agentType[1] = "CO";
					agentType[2] = "";
					agentType[3] = "";
					agentType[4] = "";
				} else {
					agentType[1] = "SGSA";
					agentType[2] = "TA";
					agentType[3] = "STA";
					agentType[4] = "";
				}
				gsas = SelectListGenerator.createGSATAList(agentType);
				if (!gsas.isEmpty()) {

					sb.append("var stns = new Array();");
					sb.append("var agentsArr = new Array();");
					int tempCat = -1;
					int check = 0;
					Iterator gsaIter = gsas.iterator();
					while (gsaIter.hasNext()) {
						gsaAraay = (String[]) gsaIter.next();
						if (strGsaCode.equals(gsaAraay[0])) {
							strGsaCode = gsaAraay[0];
							sb.append(" agents_" + tempCat + "[" + tinc + "] = ");
							sb.append("	new Array('" + gsaAraay[5] + "','" + gsaAraay[4] + "');");

							tinc++;
						} else {
							tempCat++;

							tinc = 0;
							sb.append("stns[" + tempCat + "] = new Array('" + gsaAraay[1] + "','" + gsaAraay[0] + "'); ");

							sb.append("var agents_" + tempCat + " = new Array(); ");

							strGsaCode = gsaAraay[0];

							if (gsaAraay[4] != null) {

								sb.append("agents_" + tempCat + "[" + tinc + "] = new Array('" + gsaAraay[5] + "','"
										+ gsaAraay[4] + "'); ");
								tinc++;
							}

						}

						if (tempCat > check) {
							sb.append("agentsArr[" + check + "] = agents_" + check + ";");
							check++;
						}

					}
					sb.append("agentsArr[" + check + "] = agents_" + check + ";");
					strAssignGSAHtml = "var arrGroup2 = new Array();" + "var arrData2 = new Array();"
							+ "arrData2[0] = new Array();"

							+ "var ls = new Listbox('lstRoles', 'lstAssignedRoles', 'spn1');"
							+ "ls.group1 = stns; ls.group2 = arrGroup2;" + "ls.list1 = agentsArr; ls.list2 = arrData2;";
				}
			}

		}
		sb.append(strAssignGSAHtml);
		sb.append("ls.height = '190px'; ls.width  = '350px';");
		sb.append("ls.headingLeft = '&nbsp;&nbsp;All Agents';");
		sb.append("ls.headingRight = '&nbsp;&nbsp;Selected Agents';");
		sb.append("ls.filter = true;");
		sb.append("ls.drawListBox();");

		return sb.toString().replace("\"", "").replace("\',)", "\')");
	}
	
	/**
	 * Gets agents station wise bt not for gsa with ta.
	 * 
	 * @param strAgentType
	 * @param blnWithTAs
	 * @param agentStatus TODO
	 * @return String an arry of agents js file
	 */
	@SuppressWarnings("rawtypes")
	public final static String createAgentGSAMultiSelect(String strAgentType, boolean blnWithTAs, boolean blnWithCOs, String agentStatus)
			throws ModuleException {
		
		boolean checkForStatus = false;
		
		if (agentStatus != null && (agentStatus.equals(AgentStatus.ACTIVE.getStatus())
				|| agentStatus.equals(AgentStatus.INACTIVE.getStatus()))) {

			checkForStatus = true;
		}
		
		StringBuffer sb = new StringBuffer();
		Collection agents = null;
		Collection gsas = null;
		String strAssignGSAHtml = "";
		boolean blnTA = false;
		boolean blnAll = false;

		strAssignGSAHtml = "var ls = new Listbox('lstRoles', 'lstAssignedRoles', 'spn1');";
		if (strAgentType != null) {

			if (strAgentType.equals("TA") || strAgentType.equals("All")) { // if
																			// TA
																			// or
																			// All
																			// selected

				if (strAgentType.equals("TA")) { // For TA get Station list
					blnTA = true;
					agents = SelectListGenerator.createStationCodes();
				}

				if (strAgentType.equals("All")) { // for All get Agent Type
													// List
					blnAll = true;
					agents = SelectListGenerator.createStationCodes();
				}

				Iterator ite = agents.iterator();
				sb.append("var stns = new Array(); ");

				sb.append(" var agentsArr = new Array();");
				int tempCat = 0;
				int tinc = 0;
				Collection col = null;
				Map keyValues = null;
				Set keys = null;
				Iterator keyIterator = null;
			//	String agentTpes[] = new String[1];
				String agentType = "";

				while (ite.hasNext()) {

					keyValues = (Map) ite.next();
					keys = keyValues.keySet();
					keyIterator = keys.iterator();

					while (keyIterator.hasNext()) {
						
						List<String> agentTypesList = new ArrayList<>();
						agentType = (String) keyValues.get(keyIterator.next());

						// Populate the Group Array
						sb.append("stns[" + tempCat + "] = '" + agentType + "'; ");
						//agentTpes[0] = agentType;
						agentTypesList.add(agentType);
						
						if(checkForStatus){
							agentTypesList.add(agentStatus);
							
							String [] agentTypeArray = new String [agentTypesList.size()];
							agentTypesList.toArray(agentTypeArray);
							
							if (blnTA) { 
								col = SelectListGenerator.createTAsByStationAndStatus(agentTypeArray);
							}
							if (blnAll) {
								col = SelectListGenerator.createAgentsByStationAndStatus(agentTypeArray);
							}	
							
						}else{
							
							String [] agentTypeArray = new String [agentTypesList.size()];
							agentTypesList.toArray(agentTypeArray);
							
							if (blnTA) { // For TA get Station wise agents
								col = SelectListGenerator.createTAsByStation(agentTypeArray);
							}
							if (blnAll) {// For All get Agent type wise Agents
								// col =
								// SelectListGenerator.createAgentsByType(agentTpes);
								// sudheera
								col = SelectListGenerator.createAgentsByStation(agentTypeArray);
							}	
						}

						if (col != null) {
							sb.append("var agents_" + tempCat + " = new Array(); ");

						}
						if (col.size() == 0) {
							sb.append("new Array();");

						} else {
							Iterator iteAgents = col.iterator();
							tinc = 0;
							while (iteAgents.hasNext()) {

								String str[] = (String[]) iteAgents.next();
								sb.append("agents_" + tempCat + "[" + tinc + "] = ");
								sb.append("new Array ('" + str[0] + "','" + str[1] + "');");
								tinc++;

							}

						}
					}
					sb.append("agentsArr[" + tempCat + "] = agents_" + tempCat + ";");
					tempCat++;

				}

				strAssignGSAHtml = "var arrGroup2 = new Array();" + "var arrData2 = new Array();" + "arrData2[0] = new Array();"

				+ "var ls = new Listbox('lstRoles', 'lstAssignedRoles', 'spn1');" + "ls.group1 = stns;" + "ls.list1 = agentsArr;";

			} else if (!strAgentType.equals("") && !strAgentType.equals("All") && !blnWithTAs && !blnWithCOs) {

				sb.append("var agts = new Array(); ");
				
				if(checkForStatus){
					
					String agentTypes[] = { strAgentType,agentStatus };
					agents = SelectListGenerator.createAgentsByTypeAndStatus(agentTypes);
					
				}else{
					String agentTypes[] = { strAgentType };
					agents = SelectListGenerator.createAgentsByType(agentTypes);
				}
				
				if (!agents.isEmpty()) {
					int tempInt = 0;
					Iterator agentIter = agents.iterator();

					while (agentIter.hasNext()) {
						String str[] = (String[]) agentIter.next();
						sb.append("agts[" + tempInt + "] = new Array('" + str[0] + "','" + str[1] + "'); ");
						tempInt++;
					}
					strAssignGSAHtml = "var ls = new Listbox('lstRoles', " + "'lstAssignedRoles', 'spn1');" + "ls.group1 = agts;";
				}

			} else if (!strAgentType.equals("") && !strAgentType.equals("All") && (blnWithTAs || blnWithCOs)) {	
				List<String> agentTypeList = new ArrayList<>();
				agentTypeList.add(strAgentType);
				if(checkForStatus){
					agentTypeList.add(agentStatus);
				}
				
				String[] gsaAraay = null;
				String strGsaCode = "";
				int tinc = 0;
				if (blnWithTAs && blnWithCOs) {
					agentTypeList.add("TA");
					agentTypeList.add("CO");
				} else if (blnWithCOs) {
					agentTypeList.add("CO");
					agentTypeList.add("");
				} else {
					agentTypeList.add("TA");
					agentTypeList.add("");
				}

				if(checkForStatus){
					
					String[] agentTypeArray = new String[agentTypeList.size()];
					agentTypeList.toArray(agentTypeArray);
					gsas = SelectListGenerator.createGSATAListByStatus(agentTypeArray);
					
				}else{
				
					String[] agentTypeArray = new String[agentTypeList.size()];
					agentTypeList.toArray(agentTypeArray);
					gsas = SelectListGenerator.createGSATAList(agentTypeArray);
				}
				if (!gsas.isEmpty()) {

					sb.append("var stns = new Array();");
					sb.append("var agentsArr = new Array();");
					int tempCat = -1;
					int check = 0;
					Iterator gsaIter = gsas.iterator();
					while (gsaIter.hasNext()) {
						gsaAraay = (String[]) gsaIter.next();
						if (strGsaCode.equals(gsaAraay[0])) {
							strGsaCode = gsaAraay[0];
							sb.append(" agents_" + tempCat + "[" + tinc + "] = ");
							sb.append("	new Array('" + gsaAraay[5] + "','" + gsaAraay[4] + "');");

							tinc++;
						} else {
							tempCat++;

							tinc = 0;
							sb.append("stns[" + tempCat + "] = new Array('" + gsaAraay[1] + "','" + gsaAraay[0] + "'); ");

							sb.append("var agents_" + tempCat + " = new Array(); ");

							strGsaCode = gsaAraay[0];

							if (gsaAraay[4] != null) {

								sb.append("agents_" + tempCat + "[" + tinc + "] = new Array('" + gsaAraay[5] + "','"
										+ gsaAraay[4] + "'); ");
								tinc++;
							}

						}

						if (tempCat > check) {
							sb.append("agentsArr[" + check + "] = agents_" + check + ";");
							check++;
						}

					}
					sb.append("agentsArr[" + check + "] = agents_" + check + ";");
					strAssignGSAHtml = "var arrGroup2 = new Array();" + "var arrData2 = new Array();"
							+ "arrData2[0] = new Array();"

							+ "var ls = new Listbox('lstRoles', 'lstAssignedRoles', 'spn1');"
							+ "ls.group1 = stns; ls.group2 = arrGroup2;" + "ls.list1 = agentsArr; ls.list2 = arrData2;";
				}
			}

		}
		sb.append(strAssignGSAHtml);
		sb.append("ls.height = '190px'; ls.width  = '350px';");
		sb.append("ls.headingLeft = '&nbsp;&nbsp;All Agents';");
		sb.append("ls.headingRight = '&nbsp;&nbsp;Selected Agents';");
		sb.append("ls.filter = true;");
		sb.append("ls.drawListBox();");

		return sb.toString().replace("\"", "").replace("\',)", "\')");
	}

	/**
	 * Create listbox with all roles
	 * 
	 * @return String
	 * @throws ModuleException
	 */
	public final static String createRoleMultiSelect() throws ModuleException {
		StringBuffer sb = new StringBuffer();
		Collection<String[]> roles = null;
		String strRoles = "";

		roles = SelectListGenerator.getAllRoles();
		strRoles = "var ls3 = new Listbox('lstAllRoles', 'lstSelectedRoles', 'spn3', 'ls3');";

		if (!roles.isEmpty()) {
			sb.append("var roles = new Array(); ");
			int tempInt = 0;
			Iterator<String[]> rolesIter = roles.iterator();

			while (rolesIter.hasNext()) {
				String str[] = (String[]) rolesIter.next();
				if (!str[0].equals("0")) {
					if (!str[0].equals("-1")) {
						if (!str[0].equals("-2")) {
							sb.append("roles[" + tempInt + "] = new Array('" + str[1] + "','" + str[0] + "'); ");
							tempInt++;
						}
					}
				}
			}
			strRoles = strRoles + " ls3.group1 = roles;";
		}

		sb.append(strRoles);
		sb.append("ls3.height = '190px'; ls3.width  = '350px';");
		sb.append("ls3.headingLeft = '&nbsp;&nbsp;All Rolles';");
		sb.append("ls3.headingRight = '&nbsp;&nbsp;Selected Roles';");
		sb.append("ls3.drawListBox();");
		return sb.toString();
	}

	public final static String createFilteredRoleMultiSelect(String strUsers) throws ModuleException {
		StringBuffer sb = new StringBuffer();
		Collection<String[]> roles = null;

		String strRoles = "var ls3 = new Listbox('lstAllRoles', 'lstAllAssignedRoles', 'spn3', 'ls3');";

		if (strUsers != null) {
			String[] userArr = strUsers.split(",");
			roles = ModuleServiceLocator.getSecurityBD().getUserRoleSortedMap(new ArrayList<String>(Arrays.asList(userArr)));

			if (!roles.isEmpty()) {

				sb.append("var roles = new Array(); ");
				int tempInt = 0;
				Iterator<String[]> rolesIter = roles.iterator();

				while (rolesIter.hasNext()) {
					String[] strings = (String[]) rolesIter.next();
					sb.append("roles[" + tempInt + "] = new Array('" + strings[1] + "','" + strings[0] + "'); ");
					tempInt++;
				}

				strRoles = strRoles + "ls3.group1 = roles;";
			}

		}
		sb.append(strRoles);
		sb.append("ls3.height = '190px'; ls3.width  = '350px';");
		sb.append("ls3.headingLeft = '&nbsp;&nbsp;All Rolles';");
		sb.append("ls3.headingRight = '&nbsp;&nbsp;Selected Roles';");
		sb.append("ls3.drawListBox();");

		return sb.toString();
	}

	/**
	 * Create privilege list for selected roles
	 * 
	 * @param strRoles
	 *            String
	 * @return String
	 * @throws ModuleException
	 */
	public final static String createRolePrivilegeMultiSelect(String roles) throws ModuleException {
		StringBuffer sb = new StringBuffer();
		Collection<String[]> privileges = null;
		String strPrivileges = "var ls4 = new Listbox('lstPrivileges', 'lstSelectedPrivileges', 'spn4', 'ls4');";

		if (roles != null) {
			String[] RoleArray = roles.split(",");
			ArrayList<String> roleList = new ArrayList<String>();
			for (int i = 0; i < RoleArray.length; i++) {
				roleList.add(RoleArray[i]);
			}
			privileges = ModuleServiceLocator.getSecurityBD().getDistinctPrivilegesForRoles(roleList, Constants.INTERNAL_CHANNEL);

			if (!privileges.isEmpty()) {
				sb.append("var privileges = new Array(); ");
				int tempInt = 0;
				Iterator<String[]> privilegeIter = privileges.iterator();

				while (privilegeIter.hasNext()) {
					String str[] = (String[]) privilegeIter.next();
					sb.append("privileges[" + tempInt + "] = new Array('" + str[1] + "','" + str[0] + "'); ");
					tempInt++;
				}
				strPrivileges = strPrivileges + " ls4.group1 = privileges;";
			}
		}
		sb.append(strPrivileges);
		sb.append("ls4.height = '190px'; ls4.width  = '350px';");
		sb.append("ls4.headingLeft = '&nbsp;&nbsp;All Privileges';");
		sb.append("ls4.headingRight = '&nbsp;&nbsp;Selected Privileges';");
		sb.append("ls4.drawListBox();");
		return sb.toString();
	}

	/**
	 * Gets agents station wise bt not for gsa with ta.
	 * 
	 * @param strAgentType
	 * @param blnWithTAs
	 * 
	 * @return String an arry of agents js file
	 */
	@SuppressWarnings("rawtypes")
	public final static String createOwnAgentGSAMultiSelect(String strAgentType, boolean blnWithTAs, boolean blnWithCOs)
			throws ModuleException {

		StringBuffer sb = new StringBuffer();
		Collection agents = null;
		Collection gsas = null;
		String strAssignGSAHtml = "";
		boolean blnTA = false;
		boolean blnAll = false;
		String defaultAirlineCode = AppSysParamsUtil.getDefaultAirlineIdentifierCode();

		strAssignGSAHtml = "var ls = new Listbox('lstRoles', 'lstAssignedRoles', 'spn1');";
		if (strAgentType != null) {

			if (strAgentType.equals("TA") || strAgentType.equals("All")) { // if
																			// TA
																			// or
																			// All
																			// selected

				if (strAgentType.equals("TA")) { // For TA get Station list
					blnTA = true;
					agents = SelectListGenerator.createOwnStationCodes();
				}

				if (strAgentType.equals("All")) { // for All get Agent Type
													// List
					blnAll = true;
					agents = SelectListGenerator.createOwnStationCodes();
				}

				Iterator ite = agents.iterator();
				sb.append("var stns = new Array(); ");

				sb.append(" var agentsArr = new Array();");
				int tempCat = 0;
				int tinc = 0;
				Collection col = null;
				Map keyValues = null;
				Set keys = null;
				Iterator keyIterator = null;
				String agentTpes[] = new String[2];
				String agentType = "";

				while (ite.hasNext()) {

					keyValues = (Map) ite.next();
					keys = keyValues.keySet();
					keyIterator = keys.iterator();

					while (keyIterator.hasNext()) {

						agentType = (String) keyValues.get(keyIterator.next());

						// Populate the Group Array
						sb.append("stns[" + tempCat + "] = '" + agentType + "'; ");
						agentTpes[0] = agentType;
						agentTpes[1] = defaultAirlineCode;

						if (blnTA) { // For TA get Station wise agents
							col = SelectListGenerator.createOwnTAsByStation(agentTpes);
						}
						if (blnAll) {// For All get Agent type wise Agents
							// col =
							// SelectListGenerator.createAgentsByType(agentTpes);
							// sudheera
							col = SelectListGenerator.createOwnAgentsByStation(agentTpes);
						}

						if (col != null) {
							sb.append("var agents_" + tempCat + " = new Array(); ");

						}
						if (col.size() == 0) {
							sb.append("new Array();");

						} else {
							Iterator iteAgents = col.iterator();
							tinc = 0;
							while (iteAgents.hasNext()) {

								String str[] = (String[]) iteAgents.next();
								sb.append("agents_" + tempCat + "[" + tinc + "] = ");
								sb.append("new Array ('" + str[0] + "','" + str[1] + "');");
								tinc++;

							}

						}
					}
					sb.append("agentsArr[" + tempCat + "] = agents_" + tempCat + ";");
					tempCat++;

				}

				strAssignGSAHtml = "var arrGroup2 = new Array();" + "var arrData2 = new Array();" + "arrData2[0] = new Array();"

				+ "var ls = new Listbox('lstRoles', 'lstAssignedRoles', 'spn1');" + "ls.group1 = stns;" + "ls.list1 = agentsArr;";

			} else if (!strAgentType.equals("") && !strAgentType.equals("All") && !blnWithTAs && !blnWithCOs) {

				sb.append("var agts = new Array(); ");
				String params[] = { strAgentType, defaultAirlineCode };
				agents = SelectListGenerator.createOwnAgentsByType(params);
				if (!agents.isEmpty()) {
					int tempInt = 0;
					Iterator agentIter = agents.iterator();

					while (agentIter.hasNext()) {
						String str[] = (String[]) agentIter.next();
						sb.append("agts[" + tempInt + "] = new Array('" + str[0] + "','" + str[1] + "'); ");
						tempInt++;
					}
					strAssignGSAHtml = "var ls = new Listbox('lstRoles', " + "'lstAssignedRoles', 'spn1');" + "ls.group1 = agts;";
				}

			} else if (!strAgentType.equals("") && !strAgentType.equals("All") && (blnWithTAs || blnWithCOs)) {

				String[] agentType = new String[5];
				agentType[0] = strAgentType;
				agentType[1] = defaultAirlineCode;
				String[] gsaAraay = null;
				String strGsaCode = "";
				int tinc = 0;
				if (blnWithTAs && blnWithCOs) {
					agentType[2] = "TA";
					agentType[3] = "CO";
				} else if (blnWithCOs) {
					agentType[2] = "CO";
					agentType[3] = "";
				} else {
					agentType[2] = "TA";
					agentType[3] = "";
				}
				agentType[4] = defaultAirlineCode;

				gsas = SelectListGenerator.createOwnGSATAList(agentType);
				if (!gsas.isEmpty()) {

					sb.append("var stns = new Array();");
					sb.append("var agentsArr = new Array();");
					int tempCat = -1;
					int check = 0;
					Iterator gsaIter = gsas.iterator();
					while (gsaIter.hasNext()) {
						gsaAraay = (String[]) gsaIter.next();
						if (strGsaCode.equals(gsaAraay[0])) {
							strGsaCode = gsaAraay[0];
							sb.append(" agents_" + tempCat + "[" + tinc + "] = ");
							sb.append("	new Array('" + gsaAraay[5] + "','" + gsaAraay[4] + "');");

							tinc++;
						} else {
							tempCat++;

							tinc = 0;
							sb.append("stns[" + tempCat + "] = new Array('" + gsaAraay[1] + "','" + gsaAraay[0] + "'); ");

							sb.append("var agents_" + tempCat + " = new Array(); ");

							strGsaCode = gsaAraay[0];

							if (gsaAraay[4] != null) {

								sb.append("agents_" + tempCat + "[" + tinc + "] = new Array('" + gsaAraay[5] + "','"
										+ gsaAraay[4] + "'); ");
								tinc++;
							}

						}

						if (tempCat > check) {
							sb.append("agentsArr[" + check + "] = agents_" + check + ";");
							check++;
						}

					}
					sb.append("agentsArr[" + check + "] = agents_" + check + ";");
					strAssignGSAHtml = "var arrGroup2 = new Array();" + "var arrData2 = new Array();"
							+ "arrData2[0] = new Array();"

							+ "var ls = new Listbox('lstRoles', 'lstAssignedRoles', 'spn1');"
							+ "ls.group1 = stns; ls.group2 = arrGroup2;" + "ls.list1 = agentsArr; ls.list2 = arrData2;";
				}
			}

		}
		sb.append(strAssignGSAHtml);
		sb.append("ls.height = '190px'; ls.width  = '350px';");
		sb.append("ls.headingLeft = '&nbsp;&nbsp;All Agents';");
		sb.append("ls.headingRight = '&nbsp;&nbsp;Selected Agents';");
		sb.append("ls.drawListBox();");

		return sb.toString();
	}

	/**
	 * Gets users agent wise.
	 * 
	 * @param strAgentType
	 * @param blnWithTAs
	 * 
	 * @return String an arry of agents js file
	 */
	public final static String createUserAgentMultiSelect(String strAgents) throws ModuleException {

		StringBuffer sb = new StringBuffer();
		Map<String, Collection<UserTO>> userMap = null;

		sb.append("var userAgentArr = new Array(); ");
		sb.append("var usersArr = new Array();");

		if (strAgents != null) {
			String[] AgentArr = strAgents.split(",");
			userMap = ModuleServiceLocator.getSecurityBD().getAgentUserSortedMap(new ArrayList<String>(Arrays.asList(AgentArr)));
			Collection<String> tempAgents = userMap.keySet();
			int agentCount = 0;
			for (Iterator<String> iter = tempAgents.iterator(); iter.hasNext();) {
				String agentCode = (String) iter.next();

				Collection<UserTO> tempUsers = (Collection<UserTO>) userMap.get(agentCode);

				String agentName = "";
				int userCount = 0;
				for (UserTO user : tempUsers) {
					agentName = user.getAgentName();
					if (userCount == 0) {
						sb.append("usersArr[" + agentCount + "] = new Array(); ");
					}
					sb.append("usersArr[" + agentCount + "][" + userCount + "] = new Array('" + user.getUserId() + " - "
							+ user.getDisplayName() + "','" + user.getUserId() + "'); ");
					userCount++;
				}

				sb.append("userAgentArr[" + agentCount + "] = '" + agentName + "'; ");

				agentCount++;
			}

		}
		sb.append("var arrGroup3 = new Array();" + "var arrData3 = new Array(); arrData3[0] = new Array();");
		sb.append("var ls2 = new Listbox('lstUsers', 'lstAssignedUsers', 'spn2', 'ls2'); ls2.group1 = userAgentArr; ls2.group2 = arrGroup3;");
		sb.append("ls2.list1 = usersArr; ls2.list2 = arrData3;");

		sb.append("ls2.height = '125px'; ls2.width  = '350px';");
		sb.append("ls2.headingLeft = '&nbsp;&nbsp;All Users';");
		sb.append("ls2.headingRight = '&nbsp;&nbsp;Selected Users';");
		sb.append("ls2.drawListBox();");

		return sb.toString();
	}

	/**
	 * creates multiselect for BC category
	 * 
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public final static String createBCCategoryMultiSelect() throws ModuleException {

		Collection<String> data = new ArrayList<String>();
		data.add("Standard");
		data.add("Non-Standard");
		data.add("Fixed");

		StringBuffer sb = new StringBuffer();
		if (data != null && data.size() > 0) {
			sb.append("var showBCCategory = true;");

			sb.append("var arrData = new Array();");
			sb.append("var methods = new Array();");
			int tempInt = 0;
			String BCCategoryCode;

			Iterator<String> BCCategoryIter = data.iterator();

			while (BCCategoryIter.hasNext()) {
				BCCategoryCode = BCCategoryIter.next();
				sb.append("methods[" + tempInt + "] = new Array('" + BCCategoryCode + "','" + BCCategoryCode + "'); ");
				tempInt++;
			}

			sb.append("arrData[0] = methods;");
			sb.append("var bccat = new Listbox('lstBCCategory', 'lstAssignedBCCategory', 'spnBCCat', 'bccat');");
			sb.append("bccat.group1 = arrData[0];");
			sb.append("bccat.height = '75px'; bccat.width = '170px';");
			sb.append("bccat.headingLeft = '&nbsp;&nbsp;Booking Class Category';");
			sb.append("bccat.headingRight = '&nbsp;&nbsp;Selected Booking Class Category';");
			sb.append("bccat.drawListBox();");
		} else {
			sb.append("var showBCCategory = false;");
		}
		return sb.toString();

	}

	/**
	 * Returns the First date of reporting
	 * 
	 * @return String the first date of reporting
	 */
	public final static String getReportingPeriod(int noofmonths) throws ModuleException {

		String date = null;
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Calendar cal = new GregorianCalendar();
		cal.add(Calendar.MONTH, -noofmonths);
		date = dateFormat.format(cal.getTime());

		return date;

	}

	public final static String createCurrencyCodeHtml() throws ModuleException {

		Collection<String[]> currencyArray = null;
		StringBuffer sb = new StringBuffer("var arrData = new Array();");
		sb.append("var currencies = new Array();");

		currencyArray = SelectListGenerator.getCurrencyCodeList();
		int tempInt = 0;

		for (String[] strCurrency : currencyArray) {
			sb.append("currencies[" + tempInt + "] = new Array('" + strCurrency[0] + "','" + strCurrency[1] + "'); ");
			tempInt++;
		}

		sb.append("arrData[0] = currencies;");
		sb.append(" var lspm = new Listbox('lstPModes', 'lstAssignedPModes', 'spn1', 'lspm');");
		sb.append("lspm.group1 = arrData[0];");
		sb.append("lspm.height = '100px'; lspm.width = '170px';");
		sb.append("lspm.headingLeft = '&nbsp;&nbsp;All Currency';");
		sb.append("lspm.headingRight = '&nbsp;&nbsp;Selected Currency';");
		sb.append("lspm.drawListBox();");

		return sb.toString();
	}

	/**
	 * Creates Fare Rule Multi Select Box
	 * 
	 * @return String created MultiSelect Box Array
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final static String createFareRuleList() throws ModuleException {
		Map<String, String> fareRules = null;
		StringBuffer sb = new StringBuffer("var arrFareRuleData = new Array();");
		sb.append("var fareRules = new Array();");
		fareRules = SelectListGenerator.createFareRuleIDCodeList();
		int tempInt = 0;
		for (Entry<String, String> frEntry : fareRules.entrySet()) {
			sb.append("fareRules[" + tempInt + "] = new Array('" + frEntry.getValue() + "','" + frEntry.getKey() + "'); ");
			tempInt++;
		}

		sb.append("arrFareRuleData[0] = fareRules;");
		sb.append(" var lsfrules = new Listbox('lstFReules', 'lstAssignedFRules', 'spnFareRules', 'lsfrules');");
		sb.append("lsfrules.group1 = arrFareRuleData[0];");
		sb.append("lsfrules.height = '100px'; lsfrules.width = '120px';");
		sb.append("lsfrules.headingLeft = '&nbsp;&nbsp;All Fare Rules';");
		sb.append("lsfrules.headingRight = '&nbsp;&nbsp;Selected Fare Rules';");
		sb.append("lsfrules.drawListBox();");
		return sb.toString();
	}

	/**
	 * Creates a multi Select Box for Active Agents
	 * 
	 * @return String the agents multy select box
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final static String createAgentListHtml() throws ModuleException {

		List<Map<String, String>> Agent = null;
		StringBuffer jsb = new StringBuffer("var arrData = new Array();");
		jsb.append("var methods = new Array();");

		Agent = SelectListGenerator.createAgentListWithStatus();
		int tempInt = 0;
		String agentName = null;
		String agentCode = null;

		Iterator<Map<String, String>> agentItr = Agent.iterator();
		while (agentItr.hasNext()) {

			Map<String, String> keyValues = agentItr.next();
			Set<String> keySet = keyValues.keySet();
			Iterator<String> keySetItr = keySet.iterator();

			if (keySetItr.hasNext()) {
				agentName = keyValues.get(keySetItr.next());
				agentCode = keyValues.get(keySetItr.next());
				jsb.append("methods[" + tempInt + "] = new Array('" + agentName + "','" + agentCode + "'); ");
				tempInt++;
			}

		}

		jsb.append("arrData[0] = methods;");
		jsb.append(" var lsagt = new Listbox('lstAgents', 'lstAssignedAgents', 'spnAgents', 'lsagt');");
		jsb.append("lsagt.group1 = arrData[0];");
		jsb.append("lsagt.height = '100px'; lsagt.width = '170px';");
		jsb.append("lsagt.headingLeft = '&nbsp;&nbsp;Agents';");
		jsb.append("lsagt.headingRight = '&nbsp;&nbsp;Selected Agents';");
		jsb.append("lsagt.drawListBox();");
		return jsb.toString();
	}

	/**
	 * Creates Fare Visibility Multi Select Box
	 * 
	 * @return String created MultiSelect Box Array
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final static String createVisibilityList() throws ModuleException {
		List<Map<String, String>> fvisiblityRules = null;
		StringBuffer sb = new StringBuffer("var arrFareVisiData = new Array();");
		sb.append("var fareVisibility = new Array();");

		String fvisiValue = null;
		fvisiblityRules = SelectListGenerator.createSalesChannelVisibilityMultiList();
		int tempInt = 0;

		Iterator<Map<String, String>> fareVisiIter = fvisiblityRules.iterator();

		while (fareVisiIter.hasNext()) {
			Map<String, String> keyValues = fareVisiIter.next();
			Set<String> keys = keyValues.keySet();

			Iterator<String> keyIterator = keys.iterator();

			while (keyIterator.hasNext()) {
				fvisiValue = keyValues.get(keyIterator.next());
				sb.append("fareVisibility[" + tempInt + "] = new Array('" + fvisiValue + "','" + fvisiValue + "'); ");
				tempInt++;
			}
		}

		sb.append("arrFareVisiData[0] = fareVisibility;");
		sb.append(" var lsfvisibility = new Listbox('lstFVisibility', 'lstAssignedFVisibility', 'spnFareVisibility', 'lsfvisibility');");
		sb.append("lsfvisibility.group1 = arrFareVisiData[0];");
		sb.append("lsfvisibility.height = '100px'; lsfrules.width = '120px';");
		sb.append("lsfvisibility.headingLeft = '&nbsp;&nbsp;All Fare Visibility';");
		sb.append("lsfvisibility.headingRight = '&nbsp;&nbsp;Selected Fare Visibility';");
		sb.append("lsfvisibility.drawListBox();");
		return sb.toString();

	}

	/**
	 * Create Client Validations for All the Reports
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Created Client validation Array
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final static String getClientErrors(HttpServletRequest request) throws ModuleException {

		Properties moduleErrs = new Properties();

		moduleErrs.setProperty("um.report.todate.futureDate", "toDtExceedsToday");
		moduleErrs.setProperty("um.report.fromdate.futureDate", "fromDtExceedsToday");
		moduleErrs.setProperty("um.report.fromdate.exeeedsToDate", "fromDtExceed");
		moduleErrs.setProperty("um.report.fromdate.empty", "fromDtEmpty");
		moduleErrs.setProperty("um.report.fromdate.invalid", "fromDtInvalid");
		moduleErrs.setProperty("um.report.daterange.required", "dateRangeRequired");
		moduleErrs.setProperty("um.report.bookedFromDate.invalid", "bookedFromDtInvalid");
		moduleErrs.setProperty("um.report.bookedToDate.invalid", "bookedToDtInvalid");
		moduleErrs.setProperty("um.report.fromBookedFromoDate.invalid", "deptrFromDtInvalid");
		moduleErrs.setProperty("um.report.fromBookedToDate.invalid", "deptrToDtInvalid");
		moduleErrs.setProperty("um.report.reportOption.notSelected", "reportOptionNotSelected");
		moduleErrs.setProperty("um.report.todate.empty", "toDtEmpty");
		moduleErrs.setProperty("um.report.todate.invalid", "toDtinvalid");
		moduleErrs.setProperty("um.report.station.empty", "stationEmpty");
		moduleErrs.setProperty("um.report.printDetails.invalid", "printDTinvalid");
		moduleErrs.setProperty("um.report.reportingperiod.exceedsonemonth", "exceedOneMonth");
		moduleErrs.setProperty("um.report.viaPoint.source.dest.required", "viaPointsWithoutSD");
		moduleErrs.setProperty("um.report.viaPoint.conflict", "viaPointConflict");

		moduleErrs.setProperty("um.report.segmentFrom.empty", "segmentFromEmpty");
		moduleErrs.setProperty("um.report.segmentTo.empty", "segmentToEmpty");
		moduleErrs.setProperty("um.report.flightNo.empty", "flightNoEmpty");
		moduleErrs.setProperty("um.report.flightOrSegment.required", "flightOrSegmentEmpty");
		moduleErrs.setProperty("um.report.nationality.required", "nationalityEmpty");
		moduleErrs.setProperty("um.report.validation.success", "validationSuccess");
		moduleErrs.setProperty("um.report.country.required", "countryEmpty");
		moduleErrs.setProperty("um.report.firstName.required", "fNameEmpty");
		moduleErrs.setProperty("um.report.lastName.required", "lNameEmpty");
		moduleErrs.setProperty("um.report.sectorFrom.required", "sectorFromEmpty");
		moduleErrs.setProperty("um.report.book.status.transition.req", "sectorBookingStatusEmpty");
		moduleErrs.setProperty("um.report.sectorTo.required", "sectorToEmpty");
		moduleErrs.setProperty("um.report.year.required", "yearEmpty");
		moduleErrs.setProperty("um.report.month.required", "monthEmpty");
		moduleErrs.setProperty("um.report.gsa.required", "gsaEmpty");

		moduleErrs.setProperty("um.report.fromDate.past", "fromDtPast");
		moduleErrs.setProperty("um.report.toDate.past", "toDtPast");

		moduleErrs.setProperty("um.report.noOfAgents.required", "noOfAgentsRqrd");
		moduleErrs.setProperty("um.report.noOfAgents.exceed", "agentExceed");
		moduleErrs.setProperty("um.report.salesChanels.required", "saleChannelEmpty");

		moduleErrs.setProperty("um.report.stationTo.required", "stationToRqrd");

		moduleErrs.setProperty("um.report.stationFrom.required", "stationFromRqrd");

		moduleErrs.setProperty("um.report.agentType.required", "agentTypeRqrd");
		moduleErrs.setProperty("um.report.users.required", "usersRqrd");
		moduleErrs.setProperty("um.report.agents.required", "agentsRqrd");
		moduleErrs.setProperty("um.report.roles.required", "rolesRqrd");
		moduleErrs.setProperty("um.report.taskGroup.required", "taskGroupRqrd");
		moduleErrs.setProperty("um.report.agentsStations.required", "agentsOrStationsRqrd");
		moduleErrs.setProperty("um.report.segmentFrom.and.flight.empty", "segmentandflight");
		moduleErrs.setProperty("um.report.segmentto.and.flight.empty", "segmenttoandflight");
		moduleErrs.setProperty("um.report.agency.null", "agencyNull");
		moduleErrs.setProperty("um.report.select.type", "reportRqrd");
		moduleErrs.setProperty("um.report.common.invaliddate", "invaliddate");
		moduleErrs.setProperty("um.report.reservation.criterianull", "criterianull");
		moduleErrs.setProperty("um.report.agency.list.null", "agencyListNull");
		moduleErrs.setProperty("um.report.airports.required", "airportRqrd");
		moduleErrs.setProperty("um.report.month.greater", "monthGreater");
		moduleErrs.setProperty("um.report.segment.same", "sameSegment");
		moduleErrs.setProperty("um.report.via.equal", "sameVia");
		moduleErrs.setProperty("um.report.via.and.departure", "sameViaDept");
		moduleErrs.setProperty("um.report.via.and.arrival", "sameViaArival");
		moduleErrs.setProperty("um.report.selected.agency.null", "selectedAgency");
		moduleErrs.setProperty("um.report.selected.agent.users", "selectedUsers");
		moduleErrs.setProperty("um.report.booking.payment.type", "selectedBookingType");
		moduleErrs.setProperty("um.report.agenceyWeb.required", "agenceyWebRqrd");
		moduleErrs.setProperty("um.report.station.required", "posrequired");

		moduleErrs.setProperty("um.report.reportTypeValue.required", "rptTypeRqrd");
		moduleErrs.setProperty("um.report.inobcontime.required", "rptInbotbConRqrd");
		moduleErrs.setProperty("um.report.inobcontime.maxtimeexceed", "maxtimeexceed");

		moduleErrs.setProperty("um.report.period.sevenexceed", "sevenexceed");
		moduleErrs.setProperty("um.report.maxBcCat.allowed", "maxBcCategory");
		moduleErrs.setProperty("um.report.days.less.than", "daysLessThan31");

		// got from optimize to revenue report
		moduleErrs.setProperty("um.searchOptimizeInventory.form.depatureRequired.required", "depatureRequired");
		moduleErrs.setProperty("um.searchOptimizeInventory.form.arrivalRequired.required", "arrivalRequired");
		moduleErrs.setProperty("um.searchOptimizeInventory.form.depatureArriavlSame.Same", "depatureArriavlSame");
		moduleErrs.setProperty("um.searchOptimizeInventory.form.arriavlViaSame.Same", "arriavlViaSame");
		moduleErrs.setProperty("um.searchOptimizeInventory.form.depatureViaSame.Same", "depatureViaSame");
		moduleErrs.setProperty("um.searchOptimizeInventory.form.vianotinSequence.notin", "vianotinSequence");
		moduleErrs.setProperty("um.searchOptimizeInventory.form.via.same", "viaSame");
		moduleErrs.setProperty("um.searchOptimizeInventory.form.OnD.exist", "OnDExists");

		moduleErrs.setProperty("um.report.search.startdate.exceeds", "rptReriodExceeds");
		moduleErrs.setProperty("um.report.carrierCode.required", "carrierCodeRqrd");

		moduleErrs.setProperty("um.report.paymentModes.required", "paymentmodesRqrd");

		moduleErrs.setProperty("um.report.booking.payment.type", "selectedBookingType");
		moduleErrs.setProperty("um.report.fromdate.pastDate", "fromDatelessThanToday");
		moduleErrs.setProperty("um.report.daterange.empty", "emptyDateRanges");
		moduleErrs.setProperty("um.report.flightno.invalid", "flightNoInvalid");
		moduleErrs.setProperty("um.currency.form.currencycode.empty", "currencyCodeRqrd");
		moduleErrs.setProperty("um.report.ssrCode.required", "ssrCodeRqrd");
		moduleErrs.setProperty("um.report.airport.transfer.required", "aptCodeRqrd");

		// Fare details report error messages
		moduleErrs.setProperty("um.report.sal.effec.frm.date.empty", "slEffecFrmDtEmp");
		moduleErrs.setProperty("um.report.sal.effec.to.date.empty", "slEffecToDtEmp");
		moduleErrs.setProperty("um.report.sal.effec.date.range.from.grt.to", "slEffecFrmDtGrt");
		moduleErrs.setProperty("um.report.dep.val.frm.date.empty", "depFrmDtEmp");
		moduleErrs.setProperty("um.report.dep.val.to.date.empty", "depToDtEmp");
		moduleErrs.setProperty("um.report.dep.val.date.range.from.grt.to", "depFrmDtGrt");
		moduleErrs.setProperty("um.report.sal.effec.frm.date.invalid", "slEffecFrmDtInvld");
		moduleErrs.setProperty("um.report.sal.effec.to.date.invalid", "slEffecToDtInvld");
		moduleErrs.setProperty("um.report.dep.val.frm.date.invalid", "depFrmDtInvld");
		moduleErrs.setProperty("um.report.dep.val.to.date.invalid", "depToDtInvld");
		moduleErrs.setProperty("um.report.ond.empty", "ondEmpty");
		moduleErrs.setProperty("um.report.invalid.liveReport", "invalidLiveReport");

		// Application Parameter report
		moduleErrs.setProperty("um.report.select.param.empty", "selParamEmpty");
		moduleErrs.setProperty("um.report.select.field", "selDrpValue");
		moduleErrs.setProperty("um.ssr.form.code.cat.service.required", "servCatReq");
		moduleErrs.setProperty("um.report.book.status.transition.req", "bookStatusReq");
		moduleErrs.setProperty("um.report.either.sales.refunds.required", "salesOrRefundsNotSelected");
		moduleErrs.setProperty("um.report.booking.date.range.invalid", "bookDatePerInvld");

		// These messages are used in segment Contribution report , Used in set charges screen also
		moduleErrs.setProperty("um.charges.depature.required", "depatureRqrd");
		moduleErrs.setProperty("um.charges.arrival.required", "arrivalRqrd");
		moduleErrs.setProperty("um.charges.via.required", "viaPointRqrd");
		moduleErrs.setProperty("um.charges.arrivalDepature.same", "depatureArriavlSame");
		moduleErrs.setProperty("um.charges.arrivalVia.same", "arriavlViaSame");
		moduleErrs.setProperty("um.charges.depatureVia.same", "depatureViaSame");
		moduleErrs.setProperty("um.charges.via.sequence", "vianotinSequence");
		moduleErrs.setProperty("um.charges.via.equal", "viaEqual");
		moduleErrs.setProperty("um.charges.departure.inactive", "departureInactive");
		moduleErrs.setProperty("um.charges.arrival.inactive", "arrivalInactive");
		moduleErrs.setProperty("um.charges.via.inactine", "viaInactive");
		moduleErrs.setProperty("um.charges.ond.required", "ondRequired");
		moduleErrs.setProperty("um.report.seg.required", "segmentsNotSelected");
		moduleErrs.setProperty("um.report.seg.flightnum.notallowed", "segmetntAndFlightNumCannotSelected");

		moduleErrs.setProperty("um.viewfare.origin.required", "originRequired");
		moduleErrs.setProperty("um.viewfare.destination.required", "destinationRequired");
		moduleErrs.setProperty("um.report.date.invalid", "dateInvalid");
		moduleErrs.setProperty("um.report.sales.amount.invalid", "salesAmountInvalid");
		moduleErrs.setProperty("um.report.sales.amount.required", "salesAmountRequired");

		return JavascriptGenerator.createClientErrors(moduleErrs);
	}

	public static void setPreferedReportFormat(String prefRptFormat, Map<String, Object> parameters) {
		parameters.put("LOCALE", "US");
		parameters.put("DELIMITER", ",");
		if (prefRptFormat != null && !prefRptFormat.equals("")) {
			if (prefRptFormat.equalsIgnoreCase("FR") || prefRptFormat.equalsIgnoreCase("IT")) {
				parameters.put("LOCALE", "IT");
				parameters.put("DELIMITER", ";");
			}
		}
	}

	public final static void createPreferedReportOptions(HttpServletRequest request) throws ModuleException {
		UserPrincipal user = (UserPrincipal) request.getUserPrincipal();
		Agent agent = ModuleServiceLocator.getTravelAgentBD().getAgent(user.getAgentCode());

		String prefReportFormat = agent.getPrefferedRptFormat();
		request.setAttribute("AgentRptFormat", prefReportFormat);
		StringBuilder sb = new StringBuilder();
		sb.append("<table><tr>");
		sb.append("<td width='110' align='left'>");
		sb.append("<input type='radio' name='radRptNumFormat' id='radUSFormat' value='US'  class='noBorder'");
		if (prefReportFormat.equals("US")) {
			sb.append("checked");
		}
		sb.append("><font>US Format</font>");
		sb.append("</td>");
		sb.append("<td width='110' align='left'>");
		sb.append("<input type='radio' name='radRptNumFormat' id='radEUROFormat' value='IT'  class='noBorder'");
		if (!prefReportFormat.equals("US")) {
			// sb.append("value='"+prefReportFormat+"' checked");
			sb.append("checked");
		}
		sb.append("><font>EURO Format</font>");
		sb.append("</td>");
		sb.append("</tr></table>");

		request.setAttribute(WebConstants.REPORT_FORMAT_OPTIONS, sb.toString());
	}

	@SuppressWarnings("rawtypes")
	public final static String createAgentGSAMultiSelectStation(String strAgentType, boolean blnWithTAs, boolean blnWithCOs,
			String height, String width) throws ModuleException {

		StringBuffer sb = new StringBuffer();
		Collection agents = null;
		Collection gsas = null;
		String strAssignGSAHtml = "";
		boolean blnTA = false;
		boolean blnAll = false;

		strAssignGSAHtml = "var ls = new Listbox('lstRoles', 'lstAssignedRoles', 'spn1');";
		if (strAgentType != null) {

			if (strAgentType.equals("TA") || strAgentType.equals("All")) { // if
																			// TA
																			// or
																			// All
																			// selected

				if (strAgentType.equals("TA")) { // For TA get Station list
					blnTA = true;
					agents = SelectListGenerator.createStationCodes();
				}

				if (strAgentType.equals("All")) { // for All get Agent Type
													// List
					blnAll = true;
					agents = SelectListGenerator.createStationCodes();
				}

				Iterator ite = agents.iterator();
				sb.append("var stns = new Array(); ");

				sb.append(" var agentsArr = new Array();");
				int tempCat = 0;
				int tinc = 0;
				Collection col = null;
				Map keyValues = null;
				Set keys = null;
				Iterator keyIterator = null;
				String agentTpes[] = new String[1];
				String agentType = "";

				while (ite.hasNext()) {

					keyValues = (Map) ite.next();
					keys = keyValues.keySet();
					keyIterator = keys.iterator();

					while (keyIterator.hasNext()) {

						agentType = (String) keyValues.get(keyIterator.next());

						// Populate the Group Array
						sb.append("stns[" + tempCat + "] = '" + agentType + "'; ");
						agentTpes[0] = agentType;

						if (blnTA) { // For TA get Station wise agents
							col = SelectListGenerator.createTAsByStation(agentTpes);
						}
						if (blnAll) {// For All get Agent type wise Agents
							// col =
							// SelectListGenerator.createAgentsByType(agentTpes);
							// sudheera
							col = SelectListGenerator.createAgentsByStation(agentTpes);
						}

						if (col != null) {
							sb.append("var agents_" + tempCat + " = new Array(); ");

						}
						if (col.size() == 0) {
							sb.append("new Array();");

						} else {
							Iterator iteAgents = col.iterator();
							tinc = 0;
							while (iteAgents.hasNext()) {

								String str[] = (String[]) iteAgents.next();
								sb.append("agents_" + tempCat + "[" + tinc + "] = ");
								sb.append("new Array ('" + str[0] + "','" + str[1] + "');");
								tinc++;

							}

						}
					}
					sb.append("agentsArr[" + tempCat + "] = agents_" + tempCat + ";");
					tempCat++;

				}

				strAssignGSAHtml = "var arrGroup2 = new Array();" + "var arrData2 = new Array();" + "arrData2[0] = new Array();"

				+ "var ls = new Listbox('lstRoles', 'lstAssignedRoles', 'spn1');" + "ls.group1 = stns;" + "ls.list1 = agentsArr;";

			} else if (!strAgentType.equals("") && !strAgentType.equals("All") && !blnWithTAs && !blnWithCOs) {

				sb.append("var agts = new Array(); ");
				String agentTypes[] = { strAgentType };
				agents = SelectListGenerator.createAgentsByType(agentTypes);
				if (!agents.isEmpty()) {
					int tempInt = 0;
					Iterator agentIter = agents.iterator();

					while (agentIter.hasNext()) {
						String str[] = (String[]) agentIter.next();
						sb.append("agts[" + tempInt + "] = new Array('" + str[0] + "','" + str[1] + "'); ");
						tempInt++;
					}
					strAssignGSAHtml = "var ls = new Listbox('lstRoles', " + "'lstAssignedRoles', 'spn1');" + "ls.group1 = agts;";
				}

			} else if (!strAgentType.equals("") && !strAgentType.equals("All") && (blnWithTAs || blnWithCOs)) {

				String[] agentType = new String[5];
				agentType[0] = strAgentType;
				String[] gsaAraay = null;
				String strGsaCode = "";
				int tinc = 0;
				if (blnWithTAs && blnWithCOs) {
					agentType[1] = "SGSA";
					agentType[2] = "TA";
					agentType[3] = "STA";
					agentType[4] = "CO";
				} else if (blnWithCOs) {
					agentType[1] = "CO";
					agentType[2] = "";
					agentType[3] = "";
					agentType[4] = "";
				} else {
					agentType[1] = "SGSA";
					agentType[2] = "TA";
					agentType[3] = "STA";
					agentType[4] = "";
				}

				gsas = SelectListGenerator.createGSATAList(agentType);
				if (!gsas.isEmpty()) {

					sb.append("var stns = new Array();");
					sb.append("var agentsArr = new Array();");
					int tempCat = -1;
					int check = 0;
					Iterator gsaIter = gsas.iterator();
					while (gsaIter.hasNext()) {
						gsaAraay = (String[]) gsaIter.next();
						if (strGsaCode.equals(gsaAraay[0])) {
							strGsaCode = gsaAraay[0];
							sb.append(" agents_" + tempCat + "[" + tinc + "] = ");
							sb.append("	new Array('" + gsaAraay[5] + "','" + gsaAraay[4] + "');");

							tinc++;
						} else {
							tempCat++;

							tinc = 0;
							sb.append("stns[" + tempCat + "] = new Array('" + gsaAraay[1] + "','" + gsaAraay[0] + "'); ");

							sb.append("var agents_" + tempCat + " = new Array(); ");

							strGsaCode = gsaAraay[0];

							if (gsaAraay[4] != null) {

								sb.append("agents_" + tempCat + "[" + tinc + "] = new Array('" + gsaAraay[5] + "','"
										+ gsaAraay[4] + "'); ");
								tinc++;
							}

						}

						if (tempCat > check) {
							sb.append("agentsArr[" + check + "] = agents_" + check + ";");
							check++;
						}

					}
					sb.append("agentsArr[" + check + "] = agents_" + check + ";");
					strAssignGSAHtml = "var arrGroup2 = new Array();" + "var arrData2 = new Array();"
							+ "arrData2[0] = new Array();"

							+ "var ls = new Listbox('lstRoles', 'lstAssignedRoles', 'spn1');"
							+ "ls.group1 = stns; ls.group2 = arrGroup2;" + "ls.list1 = agentsArr; ls.list2 = arrData2;";
				}
			}

		}
		sb.append(strAssignGSAHtml);
		sb.append("ls.height = '" + height + "'; ls.width  = '" + width + "';");
		sb.append("ls.headingLeft = '&nbsp;&nbsp;All Agents';");
		sb.append("ls.headingRight = '&nbsp;&nbsp;Selected Agents';");
		sb.append("ls.drawListBox();");

		return sb.toString();
	}

	/**
	 * Creates the Multiselect for Incentive Scheme with Scheme Slabs
	 * 
	 * @return
	 * @throws ModuleException
	 */
	@SuppressWarnings("unused")
	public static String createIncentiveMultiSelect() throws ModuleException {
		StringBuffer sb = new StringBuffer();
		Collection<AgentIncentive> colIncentive = null;
		// String strIncenHtml = "";
		final String DATE_FORMAT = "dd/MM/yyyy";

		colIncentive = ModuleServiceLocator.getTravelAgentBD().createIncentivCodes();

		Iterator<AgentIncentive> ite = colIncentive.iterator();
		sb.append("var schemes = new Array(); ");
		int intc = 0;
		Integer schemeId = null;

		while (ite.hasNext()) {

			AgentIncentive incen = (AgentIncentive) ite.next();
			schemeId = incen.getAgentIncSchemeID();
			Set<AgentIncentiveSlab> slbSet = incen.getAgentIncentiveSlabs();

			// Populate the Group Array
			sb.append("schemes[" + intc + "] = new Array('" + incen.getSchemeName() + "','" + incen.getAgentIncSchemeID() + "');");
			sb.append("schemes[" + ++intc + "] = new Array(' ");
			sb.append(DateUtil.formatDate(incen.getEffectiveFrom(), DATE_FORMAT) + " - ");
			sb.append(DateUtil.formatDate(incen.getEffectiveTo(), DATE_FORMAT) + " - ");
			sb.append("','" + incen.getAgentIncSchemeID() + "');");
			if (slbSet != null && !slbSet.isEmpty()) {
				Iterator<AgentIncentiveSlab> iter = slbSet.iterator();
				int cnt = 1;
				while (iter.hasNext()) {
					String slabLbl = "SLAB " + cnt;
					AgentIncentiveSlab slab = iter.next();
					sb.append("schemes[" + ++intc + "] = new Array('");
					sb.append(slabLbl + " " + slab.getRateStartValue());
					sb.append(" - " + slab.getRateEndValue());
					sb.append(" " + slab.getPersentage());
					sb.append(" %','" + incen.getAgentIncSchemeID() + "');");
					cnt++;
				}
			}
			intc++;
		}
		return sb.toString();
	}

	public static String createAppParamsMultiSelect(String selection) throws ModuleException {
		StringBuffer sb = new StringBuffer();

		String strAppParamListHtml = "";

		strAppParamListHtml = "var ls = new Listbox('lstRoles', 'lstAssignedRoles', 'spn1');";
		sb.append(strAppParamListHtml);

		if (selection != null) {
			// String paramsDiplayList = "";
			sb.append("var paramData = new Array();");
			sb.append("var paramGroup = new Array();");
			if (selection.equals("Key")) {
				List<String[]> l = LookupUtils.LookupDAO().getTwoColumnMap("paramKeys");
				int i = 0;
				for (Iterator<String[]> iter = l.iterator(); iter.hasNext(); i++) {
					String s[] = iter.next();
					String code = s[0];
					code = code.replace("\\", "\\\\");
					sb.append("paramData_" + i + "= new Array();");
					sb.append("paramGroup[" + i + "] =  '" + code + "';");
					sb.append("paramData_" + i + "[" + i + "] = '" + code + "','" + code + "';");
				}
				strAppParamListHtml = "var ls = new Listbox('lstRoles', 'lstAssignedRoles', 'spn1');"
						+ "ls.group1 = paramGroup; ls.list1 = paramData;";
				sb.append(strAppParamListHtml);
			} else if (selection.equals("Description")) {
				List<String[]> l = LookupUtils.LookupDAO().getTwoColumnMap("paramDescriptions");
				sb.append("var paramData = new Array();");
				sb.append("var paramGroup = new Array();");
				int i = 0;
				for (Iterator<String[]> iter = l.iterator(); iter.hasNext(); i++) {
					String s[] = iter.next();
					String code = s[0];
					code = code.replace("\\", "\\\\");
					sb.append("paramData_" + i + "= new Array();");
					sb.append("paramGroup[" + i + "] =  '" + code + "';");
					sb.append("paramData_" + i + "[" + i + "] = '" + code + "','" + code + "';");
				}
				strAppParamListHtml = "var ls = new Listbox('lstRoles', 'lstAssignedRoles', 'spn1');"
						+ "ls.group1 = paramGroup; ls.list1 = paramData;";
				sb.append(strAppParamListHtml);
			} else {
				sb.append("var paramData = new Array();");
				sb.append("var paramGroup = new Array();");
				strAppParamListHtml = "var ls = new Listbox('lstRoles', 'lstAssignedRoles', 'spn1');";
				sb.append(strAppParamListHtml);
			}
		}

		sb.append("ls.height = '190px'; ls.width  = '350px';");
		sb.append("ls.headingLeft = '&nbsp;&nbsp;All Parameters';");
		sb.append("ls.headingRight = '&nbsp;&nbsp;Selected Parameters';");
		sb.append("ls.drawListBox();");

		return sb.toString();
	}

	/**
	 * Create Promo Code Multiselect Box
	 * 
	 * @return String
	 * @throws ModuleException
	 */
	public final static String createPromoCodesHtml() throws ModuleException {
		StringBuffer sb = new StringBuffer("var arrPromoCodeData = new Array();");
		sb.append("var promoCodes = new Array();");

		sb.append("promoCodes[0] = new Array('" + PromotionCriteriaTypesDesc.BUY_AND_GET_FREE_CRIERIA + "','"
				+ PromotionCriteriaTypes.BUYNGET + "'); ");
		sb.append("promoCodes[1] = new Array('" + PromotionCriteriaTypesDesc.DISCOUNT_CRITERIA + "','"
				+ PromotionCriteriaTypes.DISCOUNT + "'); ");
		sb.append("promoCodes[2] = new Array('" + PromotionCriteriaTypesDesc.FREE_SERVICE_CRITERIA + "','"
				+ PromotionCriteriaTypes.FREESERVICE + "'); ");
		sb.append("promoCodes[3] = new Array('" + PromotionCriteriaTypesDesc.SYS_GEN_PROMO + "','"
				+ PromotionCriteriaTypes.SYSGEN + "'); ");

		sb.append("arrPromoCodeData[0] = promoCodes;");
		sb.append(" var lsPromo1 = new Listbox('lsPromo', 'lstAssignedPromo', 'spnPromoInc', 'lsPromo1');");
		sb.append("lsPromo1.group1 = arrPromoCodeData[0];");
		sb.append("lsPromo1.height = '65px'; lsPromo1.width = '200px';");
		sb.append("lsPromo1.headingLeft = '&nbsp;&nbsp;All Promotions';");
		sb.append("lsPromo1.headingRight = '&nbsp;&nbsp;Selected Promotions';");
		sb.append("lsPromo1.drawListBox();");
		return sb.toString();
	}

	/**
	 * Creates Bundled Fare Multiselect Box
	 * 
	 * @return String created MultiSelect Box Array
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final static String createBundledFareHtml() throws ModuleException {
		Collection<String[]> bundledFares = null;
		StringBuffer sb = new StringBuffer("var arrBundledFareData = new Array();");
		sb.append("var bundledFares = new Array();");

		bundledFares = SelectListGenerator.createBundledFaresList();

		Iterator<String[]> iter = bundledFares.iterator();

		int i = 0;
		while (iter.hasNext()) {
			String s[] = iter.next();
			String code = s[0];
			String desc1 = StringUtils.replace(s[1], "'", SINGLE_QUOTE);

			sb.append("bundledFares[" + i + "] = new Array('" + desc1 + "','" + code + "'); ");
			i++;
		}

		sb.append("arrBundledFareData[0] = bundledFares;");
		sb.append(" var lsBundledFare = new Listbox('lsAllBundledFare', 'lstAssignedBundledFare', 'spnBundledFare', 'lsBundledFare');");
		sb.append("lsBundledFare.group1 = arrBundledFareData[0];");
		sb.append("lsBundledFare.height = '100px'; lsBundledFare.width = '150px';");
		sb.append("lsBundledFare.headingLeft = '&nbsp;&nbsp;All Bundled Services';");
		sb.append("lsBundledFare.headingRight = '&nbsp;&nbsp;Selected Bundled Services';");
		sb.append("lsBundledFare.drawListBox();");

		return sb.toString();
	}
	
	/**
	 * Create Loyalty Product Codes List
	 * 
	 * @return String
	 * @throws ModuleException
	 */
	public final static String createLoyaltyProductCodesList() throws ModuleException {
		Collection<String[]> loyaltyProductCodesList = null;
		StringBuffer sb = new StringBuffer("var loyaltyProductCodes = new Array();");

		loyaltyProductCodesList = SelectListGenerator.createLoyaltyProductCodesList();
		Iterator<String[]> iter = loyaltyProductCodesList.iterator();

		int i = 0;
		while (iter.hasNext()) {
			String s[] = iter.next();
			String code = s[0];
			String desc1 = StringUtils.replace(s[1], "'", SINGLE_QUOTE);

			sb.append("loyaltyProductCodes[" + i + "] = new Array('" + code + "','" + desc1 + "'); ");
			i++;
		}
		
		return sb.toString();
	}

	public static String getMonthName(Date date) {
		Format formatter = new SimpleDateFormat("MMMM");
		return formatter.format(date);
	}
}
