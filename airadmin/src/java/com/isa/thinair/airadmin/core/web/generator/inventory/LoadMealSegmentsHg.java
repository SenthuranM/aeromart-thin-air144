package com.isa.thinair.airadmin.core.web.generator.inventory;

import java.util.Collection;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airinventory.api.dto.meal.FlightMealDTO;
import com.isa.thinair.airinventory.api.service.MealBD;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.commons.api.exception.ModuleException;

public class LoadMealSegmentsHg {

	/**
	 * Creates the Attached Template codes
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getFlightData(HttpServletRequest request) throws ModuleException {

		MealBD mealBd = ModuleServiceLocator.getMealBD();
		FlightBD flightbd = ModuleServiceLocator.getFlightServiceBD();

		Collection<FlightSegement> colSegs = null;
		String strFlightId = request.getParameter("fltId");
		String classOfService = request.getParameter("cos");

		StringBuilder sb = new StringBuilder();

		FlightMealDTO fltMealDTO = null;
		FlightSegement segment = null;
		Collection<FlightMealDTO> colMealTempl = null;
		Iterator<FlightSegement> fltIter = null;
		Iterator<FlightMealDTO> mealIter = null;
		int i = 0;
		int segId = 0;
		int mealSegId = 0;
		boolean isSuceeded = false;
		if (strFlightId != null && !strFlightId.equals("")) {
			Integer flightId = new Integer(strFlightId);
			colSegs = flightbd.getSegments(flightId);
			colMealTempl = mealBd.getMealTempleateIds(flightId, classOfService);
		}

		if (classOfService != null) {
			request.setAttribute("classOfService", classOfService);
		} else {
			classOfService = "";
		}

		if (colSegs != null) {
			fltIter = colSegs.iterator();
			while (fltIter.hasNext()) {
				segment = (FlightSegement) fltIter.next();
				segId = segment.getFltSegId();
				sb.append("arrMealSegData[" + i + "] = new Array();");
				sb.append("arrMealSegData[" + i + "][1] = '" + segment.getSegmentCode() + "';");
				sb.append("arrMealSegData[" + i + "][2] = '" + segment.getFltSegId() + "';");
				sb.append("arrMealSegData[" + i + "][3] = '';");
				sb.append("arrMealSegData[" + i + "][4] = '" + classOfService + "';");
				if (colMealTempl != null) {
					mealIter = colMealTempl.iterator();
					while (mealIter.hasNext()) {
						fltMealDTO = (FlightMealDTO) mealIter.next();
						mealSegId = fltMealDTO.getFlightSegmentID();

						if (mealSegId == segId) {
							sb.append("arrMealSegData[" + i + "][3] = '" + fltMealDTO.getTemplateId() + "';");
							isSuceeded = true;
						}
					}
				}
				i++;
			}
		}
		if (isSuceeded) {
			request.setAttribute(WebConstants.MEAL_SCED_STATS, "blnIsSced = true;");
		}
		request.setAttribute("mealFlightId", strFlightId);
		return sb.toString();
	}

}