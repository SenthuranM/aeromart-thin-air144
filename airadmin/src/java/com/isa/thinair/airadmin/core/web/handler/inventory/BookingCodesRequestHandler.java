package com.isa.thinair.airadmin.core.web.handler.inventory;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.inventory.BookingCodesHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airinventory.api.dto.BookingClassDTO;
import com.isa.thinair.airinventory.api.dto.SearchBCsCriteria;
import com.isa.thinair.airinventory.api.dto.SearchBCsCriteria.BookingClassTypes;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.model.GDSBookingClass;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author srikantha
 * 
 */

public final class BookingCodesRequestHandler extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(BookingCodesRequestHandler.class);
	private static AiradminConfig airadminConfig = new AiradminConfig();

	private static final String PARAM_MODE = "hdnMode";

	// Input Parameters
	private static final String PARAM_VERSION = "hdnVersion";
	private static final String PARAM_BOOKINGCODE = "txtBookingClass";
	private static final String PARAM_BOOKINGDESCRIPTION = "txtBookingClassDescription";
	private static final String PARAM_STATUS = "chkStatus";
	private static final String PARAM_CLASSOFSERVICE = "selClassofService";
	private static final String PARAM_LOGICALCABINCLASS = "selLogicalCC";
	private static final String PARAM_STN_CLASS = "chkStandardClass";
	private static final String PARAM_NESTRANK = "txtNestRank";
	private static final String PARAM_NESTSHIFT = "chkNestShift";
	private static final String PARAM_COMMENTS = "txtRulesComments";
	private static final String PARAM_FIXEDCLASS = "chkFixedClass";
	private static String PARAM_BC_TYPE = "sltBCType";
	private static String PARAM_CHARGE_CODE = "hdnCCOdes";
	private static final String PARAM_ALLOCATIONTYPE = "selAllocationType";
	private static final String PARAM_PAXCATEGORY = "selPaxCat";
	private static final String PARAM_GDS_CODE = "selGDSPublishing";
	private static final String PARAM_FARE_CATEGORY = "selFareCat";

	private static String PARAM_ROW_NUM = "hdnRowNum";
	private static final String PARAM_PAXTYPE = "hdnPaxType";
	private static final String PARAM_RELEASE_TIME = "txtCutOverTime";
	private static final String PARAM_RELESAE_STATUS = "chkReleaseTime";
	private static final String PARAM_ONHOLD_STATUS = "chkOnHold";
	private static final String PARAM_GROUP_ID = "hdnGroupId";
	private static final int DEFAULT_GROUP_ID = 0;

	private static boolean isExceptionOccured = false;
	private static boolean isSaveTransactionSuccessful = false;
	private static String saveSuccess = "var isSaveTransactionSuccessful = false;";
	private static String strModeJS = "var isAddMode = 'Display';";

	private static boolean isSuccessfulSaveMessageDisplayEnabled() {
		boolean isMessageEnabled = false;
		if (airadminConfig.getMessage("um.airadmin.savemessage.display").equals("true")) {
			isMessageEnabled = true;
		}
		return isMessageEnabled;
	}

	/**
	 * main Execute Method For Booking Class Action
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {

		String isSuccessfulSaveMessageDisplayEnabledJS = "var isSuccessfulSaveMessageDisplayEnabled = "
				+ isSuccessfulSaveMessageDisplayEnabled() + ";";

		String strHdnMode = request.getParameter(PARAM_MODE);
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;

		try {

			if (strHdnMode != null) {

				if (strHdnMode.equals(WebConstants.ACTION_ADD)) {
					checkPrivilege(request, WebConstants.PLAN_INVN_BC_ADD);
					strModeJS = "var isAddMode = 'Add';";
					setAttribInRequest(request, WebConstants.REQ_OPERATION_MODE, strModeJS);
					saveData(request, strHdnMode);
					isSaveTransactionSuccessful = true;
				}
				if (strHdnMode.equals(WebConstants.ACTION_UPDATE)) {
					checkPrivilege(request, WebConstants.PLAN_INVN_BC_EDIT);
					strModeJS = "var isAddMode =  'Edit';";
					setAttribInRequest(request, WebConstants.REQ_OPERATION_MODE, strModeJS);
					saveData(request, strHdnMode);
					isSaveTransactionSuccessful = true;
				}
				if (strHdnMode.equals(WebConstants.ACTION_DELETE)) {
					checkPrivilege(request, WebConstants.PLAN_INVN_BC_DELETE);
					deleteData(request);
				}
			}
			setDisplayData(request);

		} catch (Exception exception) {
			log.error("Exception in BookingCodesRequestHandler.execute()", exception);
			if (exception instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
			}
		}
		request.setAttribute(WebConstants.REQ_TRANSACTION_DISPLAYSTATUS, isSuccessfulSaveMessageDisplayEnabledJS);
		request.setAttribute(WebConstants.REQ_TRANSACTION_STATUS, saveSuccess);
		return forward;
	}

	private static void deleteData(HttpServletRequest request) throws Exception {
		String strBookingCode = request.getParameter(PARAM_BOOKINGCODE);
		try {
			if (strBookingCode != null) {
				ModuleServiceLocator.getBookingClassBD().deleteBookingClass(strBookingCode);
				saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_DELETE_SUCCESS), WebConstants.MSG_SUCCESS);
			}
		} catch (ModuleException moduleException) {
			log.error("Exception in BookingCodesRequestHandler.deleteData() [origin module=" + moduleException.getModuleDesc()
					+ "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);

		} catch (Exception exception) {
			log.error("Exception in BookingCodesRequestHandler.deleteData()", exception);
			if (exception instanceof RuntimeException) {
				throw exception;
			} else {
				saveMessage(request, exception.getMessage(), WebConstants.MSG_ERROR);
			}
		}
	}

	/**
	 * Saves the Booking Class
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @param strMode
	 *            the Action Mode Add/Update
	 * @throws Exception
	 *             the Exception
	 */
	private static void saveData(HttpServletRequest request, String strMode) throws Exception {

		BookingClass bookingClass = null;
		Properties BcProp = getBCproperty(request);
		String[] gdsIds = request.getParameterValues((PARAM_GDS_CODE));

		try {

			bookingClass = new BookingClass();
			bookingClass.setBookingCode(BcProp.getProperty(PARAM_BOOKINGCODE).toUpperCase());

			// bookingCode setters will goes here
			bookingClass.setBookingCodeDescription(BcProp.getProperty(PARAM_BOOKINGDESCRIPTION));
			bookingClass.setCabinClassCode(BcProp.getProperty(PARAM_CLASSOFSERVICE));
			bookingClass.setLogicalCCCode(BcProp.getProperty(PARAM_LOGICALCABINCLASS));

			// setting allocation type
			bookingClass.setAllocationType(BcProp.getProperty(PARAM_ALLOCATIONTYPE));
			if (BcProp.getProperty(PARAM_FIXEDCLASS).equals(WebConstants.CHECKBOX_ON)) {
				bookingClass.setFixedFlag(true);
			} else {
				bookingClass.setFixedFlag(false);
			}

			bookingClass.setPaxType(BookingClass.PassengerType.ADULT);
			bookingClass.setRemarks(BcProp.getProperty(PARAM_COMMENTS));

			if (BcProp.getProperty(PARAM_STN_CLASS).equals(WebConstants.CHECKBOX_ON)) {
				bookingClass.setStandardCode(true);
				bookingClass.setNestRank(new Integer(BcProp.getProperty(PARAM_NESTRANK)));
			} else {
				bookingClass.setStandardCode(false);
				bookingClass.setNestRank(null);
			}

			if (BcProp.getProperty(PARAM_STATUS).equals(WebConstants.CHECKBOX_ON)) {
				bookingClass.setStatus(BookingClass.Status.ACTIVE);
			} else {
				bookingClass.setStatus(BookingClass.Status.INACTIVE);
			}

			// setting pax category
			bookingClass.setPaxCategoryCode(BcProp.getProperty(PARAM_PAXCATEGORY));

			try {
				if (gdsIds != null && gdsIds.length > 0) {
					Set<GDSBookingClass> gdsBookingClasses = new HashSet<GDSBookingClass>();
					for (String gdsId : gdsIds) {
						if (gdsId != null && gdsId.length() > 0) {
							GDSBookingClass gdsBookingClass = new GDSBookingClass();
							gdsBookingClass.setBookingCode(bookingClass.getBookingCode());
							gdsBookingClass.setGdsId(Integer.valueOf(gdsId));
							gdsBookingClass.setMappedBC(bookingClass.getBookingCode());
							gdsBookingClasses.add(gdsBookingClass);
						}
					}
					if (gdsBookingClasses.size() > 0) {
						bookingClass.setGdsBCs(gdsBookingClasses);
					}
				} else {
					bookingClass.setGdsBCs(null);
				}
			} catch (NumberFormatException e) {
				bookingClass.setGdsBCs(null);
			}
			bookingClass.setFareCategoryCode(BcProp.getProperty(PARAM_FARE_CATEGORY));

			bookingClass.setBcType(BcProp.getProperty(PARAM_BC_TYPE));

			if (!BcProp.getProperty(PARAM_CHARGE_CODE).equals("")) {
				Set<String> setCC = new HashSet<String>();
				if (BcProp.getProperty(PARAM_CHARGE_CODE).indexOf(",") != -1) {
					String arr[] = BcProp.getProperty(PARAM_CHARGE_CODE).split(",");
					for (int r = 0; r < arr.length; r++) {
						setCC.add(arr[r]);
					}
				} else {
					setCC.add(BcProp.getProperty(PARAM_CHARGE_CODE));
				}
				bookingClass.setApplicapableCharges(setCC);

			}
			bookingClass.setChargeFlags(getChargeFlags(bookingClass.getApplicapableCharges()));
			BookingClassDTO bookingClassDTO = new BookingClassDTO();

			if (BcProp.getProperty(PARAM_NESTSHIFT).equals(WebConstants.CHECKBOX_ON))
				bookingClassDTO.setShiftingNestRankAllowed(true);
			else
				bookingClassDTO.setShiftingNestRankAllowed(false);

			// adding release times
			if (BcProp.getProperty(PARAM_RELESAE_STATUS).equals(WebConstants.CHECKBOX_ON)) {
				bookingClass.setReleaseFlag(BookingClass.ReleaseFlag.ACTIVE);
			} else {
				bookingClass.setReleaseFlag(BookingClass.ReleaseFlag.INACTIVE);
			}
			if (!BcProp.getProperty(PARAM_RELEASE_TIME).equals("")) {
				String[] timeinMins = BcProp.getProperty(PARAM_RELEASE_TIME).split(":");
				bookingClass.setReleaseCutover((new Integer(timeinMins[0]) * 60) + new Integer(timeinMins[1]));
			} else {
				bookingClass.setReleaseCutover(null);
			}
			if (BcProp.getProperty(PARAM_ONHOLD_STATUS).equals(WebConstants.CHECKBOX_ON)) {
				bookingClass.setOnHold(true);
			} else {
				bookingClass.setOnHold(false);
			}

			// Insert as new record
			if (strMode.equals(WebConstants.ACTION_ADD)) {
				bookingClass.setGroupId(DEFAULT_GROUP_ID);
				bookingClassDTO.setBookingClass(bookingClass);
				ModuleServiceLocator.getBookingClassBD().createBookingClass(bookingClassDTO);
			}
			// Update record
			if (strMode.equals(WebConstants.ACTION_UPDATE)) {
				if (!"".equals(BcProp.getProperty(PARAM_GROUP_ID))) {
					bookingClass.setGroupId(Integer.valueOf(BcProp.getProperty(PARAM_GROUP_ID)));
				} else {
					bookingClass.setGroupId(DEFAULT_GROUP_ID);
				}

				if (!"".equals(BcProp.getProperty(PARAM_VERSION))) {
					bookingClass.setVersion(Long.parseLong(BcProp.getProperty(PARAM_VERSION)));
				}
				bookingClassDTO.setBookingClass(bookingClass);
				ModuleServiceLocator.getBookingClassBD().updateBookingClass(bookingClassDTO);
			}

			bookingClass = null;
			bookingClassDTO = null;

			saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS), WebConstants.MSG_SUCCESS);
			log.debug("BookingCodesRequestHandler.saveData() method is successfully executed.");
			isExceptionOccured = false;
			setAttribInRequest(request, "isExceptionOccured", new Boolean(isExceptionOccured));

		} catch (ModuleException moduleException) {
			isExceptionOccured = true;
			setAttribInRequest(request, "isExceptionOccured", new Boolean(isExceptionOccured));
			prepareFormData(request, BcProp);
			log.error("Exception in BookingCodesRequestHandler.saveData() [origin module=" + moduleException.getModuleDesc()
					+ "]", moduleException);

			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);

		} catch (Exception exception) {
			isExceptionOccured = true;
			setAttribInRequest(request, "isExceptionOccured", new Boolean(isExceptionOccured));
			log.error("Exception in BookingCodesRequestHandler.saveData()", exception);

			if (exception instanceof RuntimeException) {
				isExceptionOccured = true;
				setAttribInRequest(request, "isExceptionOccured", new Boolean(isExceptionOccured));
				throw exception;
			} else {
				isExceptionOccured = true;
				setAttribInRequest(request, "isExceptionOccured", new Boolean(isExceptionOccured));
				prepareFormData(request, BcProp);
				saveMessage(request, exception.getMessage(), WebConstants.MSG_ERROR);
			}
		}
	}

	private static String getChargeFlags(Set<String> setCC) {
		String tax = "N";
		String sur = "N";
		if (setCC != null && setCC.contains("TAX")) {
			tax = "Y";
		}
		if (setCC != null && setCC.contains("SUR")) {
			sur = "Y";
		}
		return "S" + sur + "T" + tax;
	}

	/**
	 * Creates the Forms Data for Booking Class & Sets to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	private static void prepareFormData(HttpServletRequest request, Properties erP) throws Exception {
		StringBuffer ersb = new StringBuffer("var arrFormData = new Array();");

		ersb.append("arrFormData[0] = new Array();");
		ersb.append("arrFormData[0][1] = '" + erP.getProperty(PARAM_BOOKINGCODE) + "';");
		ersb.append("arrFormData[0][2] = '" + erP.getProperty(PARAM_BOOKINGDESCRIPTION) + "';");
		ersb.append("arrFormData[0][3] = '" + erP.getProperty(PARAM_STATUS) + "';");
		ersb.append("arrFormData[0][4] = '" + erP.getProperty(PARAM_CLASSOFSERVICE) + "';");
		ersb.append("arrFormData[0][5] = '" + erP.getProperty(PARAM_STN_CLASS) + "';");
		ersb.append("arrFormData[0][6] = '" + erP.getProperty(PARAM_NESTRANK) + "';");
		ersb.append("arrFormData[0][7] = '" + erP.getProperty(PARAM_NESTSHIFT) + "';");
		ersb.append("arrFormData[0][8] = '" + erP.getProperty(PARAM_COMMENTS) + "';");
		ersb.append("arrFormData[0][9] = '" + erP.getProperty(PARAM_FIXEDCLASS) + "';");
		ersb.append("arrFormData[0][10] = '" + erP.getProperty(PARAM_PAXTYPE) + "';");
		ersb.append("arrFormData[0][11] = '" + erP.getProperty(PARAM_MODE) + "';");
		ersb.append("arrFormData[0][12] = '" + erP.getProperty(PARAM_VERSION) + "';");
		ersb.append("arrFormData[0][13] = '" + erP.getProperty(PARAM_BC_TYPE) + "';");
		ersb.append("arrFormData[0][14] = '" + erP.getProperty(PARAM_CHARGE_CODE) + "';");
		ersb.append("arrFormData[0][15] = '" + erP.getProperty(PARAM_ROW_NUM) + "';");
		ersb.append("arrFormData[0][16] = '" + erP.getProperty(PARAM_ALLOCATIONTYPE) + "';");
		ersb.append("arrFormData[0][17] = '" + erP.getProperty(PARAM_PAXCATEGORY) + "';");
		ersb.append("arrFormData[0][18] = '" + erP.getProperty(PARAM_FARE_CATEGORY) + "';");
		ersb.append("arrFormData[0][19] = '" + erP.getProperty(PARAM_RELEASE_TIME) + "';");
		ersb.append("arrFormData[0][20] = '" + erP.getProperty(PARAM_RELESAE_STATUS) + "';");
		ersb.append("arrFormData[0][21] = '" + erP.getProperty(PARAM_GDS_CODE) + "';");
		ersb.append("arrFormData[0][22] = '" + erP.getProperty(PARAM_ONHOLD_STATUS) + "';");
		ersb.append("arrFormData[0][23] = '" + erP.getProperty(PARAM_LOGICALCABINCLASS) + "';");

		request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, ersb.toString());
	}

	public static boolean getBoolValue(HttpServletRequest request, String key) {
		Object str = request.getAttribute(key);
		boolean val = false;
		if (str != null) {
			val = Boolean.getBoolean(str.toString());
		}
		return val;
	}

	/**
	 * Sets the Booking Class Group List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static void setGroupList(HttpServletRequest request) throws ModuleException {
		String strHtml = JavascriptGenerator.createChargegroupsForBC();
		request.setAttribute(WebConstants.REQ_GROUP_LIST, strHtml);
	}

	/**
	 * Sets the Display Data for Booking Class Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static void setDisplayData(HttpServletRequest request) throws ModuleException {
		String mode = request.getParameter(PARAM_MODE);
		setBookingCodesRowHtml(request);
		setBookingCodesList(request);
		setGDSListHtml(request);// Haider 08Sep08 GDS
		setClassOfServicesList(request);
		setLogicalCabinClassList(request);
		setBCTypeList(request);
		setGroupList(request);
		setAllocationTypeList(request);
		setClientErrors(request);
		setFareCategory(request);
		setPaxCategory(request);
		setBookingClassType(request); // Naeem 27Aprl10
		request.setAttribute(WebConstants.REQ_OPERATION_MODE, strModeJS);

		if (mode == null) {

			isSaveTransactionSuccessful = false;
			setAttribInRequest(request, "isSaveTransactionSuccessful", new Boolean(isSaveTransactionSuccessful));
			saveSuccess = "var isSaveTransactionSuccessful = false;";
			setAttribInRequest(request, WebConstants.REQ_TRANSACTION_STATUS, new Boolean(saveSuccess));

		} else if (mode.equals(WebConstants.ACTION_ADD) || mode.equals(WebConstants.ACTION_UPDATE)) {

			if (request.getAttribute("isExceptionOccured").toString().equals("false")) {
				isSaveTransactionSuccessful = true;
				setAttribInRequest(request, "isSaveTransactionSuccessful", new Boolean(isSaveTransactionSuccessful));
				saveSuccess = "var isSaveTransactionSuccessful = true;";
				setAttribInRequest(request, WebConstants.REQ_TRANSACTION_STATUS, new Boolean(saveSuccess));

			} else {

				isSaveTransactionSuccessful = false;
				setAttribInRequest(request, "isSaveTransactionSuccessful", new Boolean(isSaveTransactionSuccessful));
				saveSuccess = "var isSaveTransactionSuccessful = false;";
				setAttribInRequest(request, WebConstants.REQ_TRANSACTION_STATUS, new Boolean(saveSuccess));
			}
		} else {
			isSaveTransactionSuccessful = false;
			setAttribInRequest(request, "isSaveTransactionSuccessful", new Boolean(isSaveTransactionSuccessful));
			saveSuccess = "var isSaveTransactionSuccessful = false;";
			setAttribInRequest(request, WebConstants.REQ_TRANSACTION_STATUS, new Boolean(saveSuccess));
		}
	}

	/**
	 * Sets the Client Validations for the Booking Class page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = BookingCodesHTMLGenerator.getClientErrors(request);
			if (strClientErrors == null)
				strClientErrors = "";

			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error(
					"Exception in BookingCodesRequestHandler.setClientErrors() [origin module=" + moduleException.getModuleDesc()
							+ "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * sets the GDS list added by Haider 08Sep08
	 * 
	 * @param request
	 */
	private static void setGDSListHtml(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createGDSList();
		request.setAttribute(WebConstants.SES_HTML_GDS_LIST_DATA, strHtml);
	}

	/**
	 * Sets the Booking Code List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setBookingCodesList(HttpServletRequest request) {
		try {
			String strHtml = SelectListGenerator.createBookingCodeList();
			request.setAttribute(WebConstants.REQ_HTML_BOOKINGCODES_LIST, strHtml);
		} catch (ModuleException moduleException) {
			log.error(
					"Exception in BookingCodesRequestHandler.setBookingCodesList() [origin module="
							+ moduleException.getModuleDesc() + "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Booking Classs Type List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setBCTypeList(HttpServletRequest request) {
		String strHtml = "";
		Iterator<String> iterBCTypes = SearchBCsCriteria.getBookingClassTypes().iterator();
		while (iterBCTypes.hasNext()) {
			String strBCType = (String) iterBCTypes.next();
			if (!strBCType.equals(BookingClassTypes.ALL)) {
				strHtml += "<option value='" + strBCType + "'>" + strBCType + "</option>";
			}
		}
		request.setAttribute(WebConstants.REQ_HTML_BOOKINGCODES_TYPES_LIST, strHtml);
	}

	/**
	 * Sets the Allocation Types to the Request values are hard coded not keeping any where
	 * 
	 * @param request
	 */
	private static void setAllocationTypeList(HttpServletRequest request) {
		String strHtml = "";
		strHtml += "<option value='" + BookingClass.AllocationType.COMBINED + "'>Combine</option>";
		strHtml += "<option value='" + BookingClass.AllocationType.CONNECTION + "'>Connection</option>";
		strHtml += "<option value='" + BookingClass.AllocationType.RETURN + "'>Return</option>";
		strHtml += "<option value='" + BookingClass.AllocationType.SEGMENT + "'>Segment</option>";
		request.setAttribute(WebConstants.REQ_HTML_ALLOCATION_TYPES_LIST, strHtml);
	}

	/**
	 * Sets the Cabin Class list to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setClassOfServicesList(HttpServletRequest request) {
		try {
			String strHtml = SelectListGenerator.createCabinClassList();
			request.setAttribute(WebConstants.REQ_HTML_CLASSOFSERVICES_LIST, strHtml);
		} catch (ModuleException moduleException) {
			log.error(
					"Exception in BookingCodesRequestHandler.setClassOfServicesList() [origin module="
							+ moduleException.getModuleDesc() + "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Logical Cabin Class list to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setLogicalCabinClassList(HttpServletRequest request) {
		try {
			String strHtml = SelectListGenerator.createLogicalCabinClassList();
			String strCCWiseLCCHtml = SelectListGenerator.createCCWiseLogicalCCList();
			request.setAttribute(WebConstants.REQ_HTML_LOGICAL_CABIN_CLASS_LIST, strHtml);
			request.setAttribute(WebConstants.CC_WISE_LOGICAL_CABIN_CLASS_LIST, strCCWiseLCCHtml);
		} catch (ModuleException moduleException) {
			log.error(
					"Exception in BookingCodesRequestHandler.setLogicalCabinClassList() [origin module="
							+ moduleException.getModuleDesc() + "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Booking Class Data Row to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setBookingCodesRowHtml(HttpServletRequest request) {
		try {
			BookingCodesHTMLGenerator htmlGen = new BookingCodesHTMLGenerator();
			String strHtml = htmlGen.getBookingCodesRowHtml(request);
			request.setAttribute(WebConstants.REQ_HTML_BOOKING_CODE_DATA, strHtml);
		} catch (ModuleException moduleException) {
			log.error(
					"Exception in BookingCodesRequestHandler.setBookingCodesRowHtml() [origin module="
							+ moduleException.getModuleDesc() + "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Fare Type list to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setFareCategory(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createActiveFareCategoryList();
		request.setAttribute(WebConstants.REQ_FARE_CAT_LIST, strHtml);
	}

	/**
	 * Sets Pax Category Type to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setPaxCategory(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createPaxCatagoryList();
		request.setAttribute(WebConstants.REQ_PAXCAT_LIST, strHtml);
	}

	/**
	 * Set Booking class Type to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 **/
	private static void setBookingClassType(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createBookingClassTypeList();
		request.setAttribute(WebConstants.REQ_HTML_BOOKING_CLASS_TYPES_LIST, strHtml);
	}

	private static Properties getBCproperty(HttpServletRequest req) {
		Properties prop = new Properties();
		prop.setProperty(PARAM_VERSION, AiradminUtils.getNotNullString(req.getParameter(PARAM_VERSION)));
		prop.setProperty(PARAM_BOOKINGCODE, AiradminUtils.getNotNullString(req.getParameter(PARAM_BOOKINGCODE)));
		prop.setProperty(PARAM_BOOKINGDESCRIPTION, AiradminUtils.getNotNullString(req.getParameter(PARAM_BOOKINGDESCRIPTION)));
		prop.setProperty(PARAM_STATUS, AiradminUtils.getNotNullString(req.getParameter(PARAM_STATUS)));
		prop.setProperty(PARAM_CLASSOFSERVICE, AiradminUtils.getNotNullString(req.getParameter(PARAM_CLASSOFSERVICE)));
		prop.setProperty(PARAM_LOGICALCABINCLASS, AiradminUtils.getNotNullString(req.getParameter(PARAM_LOGICALCABINCLASS)));
		prop.setProperty(PARAM_STN_CLASS, AiradminUtils.getNotNullString(req.getParameter(PARAM_STN_CLASS)));
		prop.setProperty(PARAM_NESTRANK, AiradminUtils.getNotNullString(req.getParameter(PARAM_NESTRANK)));
		prop.setProperty(PARAM_NESTSHIFT, AiradminUtils.getNotNullString(req.getParameter(PARAM_NESTSHIFT)));
		prop.setProperty(PARAM_COMMENTS, AiradminUtils.getNotNullString(req.getParameter(PARAM_COMMENTS)));
		prop.setProperty(PARAM_FIXEDCLASS, AiradminUtils.getNotNullString(req.getParameter(PARAM_FIXEDCLASS)));
		prop.setProperty(PARAM_PAXTYPE, AiradminUtils.getNotNullString(req.getParameter(PARAM_PAXTYPE)));
		prop.setProperty(PARAM_BC_TYPE, AiradminUtils.getNotNullString(req.getParameter(PARAM_BC_TYPE)));
		prop.setProperty(PARAM_CHARGE_CODE, AiradminUtils.getNotNullString(req.getParameter(PARAM_CHARGE_CODE)));
		prop.setProperty(PARAM_ALLOCATIONTYPE, AiradminUtils.getNotNullString(req.getParameter(PARAM_ALLOCATIONTYPE)));
		prop.setProperty(PARAM_PAXCATEGORY, AiradminUtils.getNotNullString(req.getParameter(PARAM_PAXCATEGORY)));
		prop.setProperty(PARAM_FARE_CATEGORY, AiradminUtils.getNotNullString(req.getParameter(PARAM_FARE_CATEGORY)));
		prop.setProperty(PARAM_MODE, AiradminUtils.getNotNullString(req.getParameter(PARAM_MODE)));
		prop.setProperty(PARAM_ROW_NUM, AiradminUtils.getNotNullString(req.getParameter(PARAM_ROW_NUM)));
		prop.setProperty(PARAM_RELEASE_TIME, AiradminUtils.getNotNullString(req.getParameter(PARAM_RELEASE_TIME)));
		prop.setProperty(PARAM_RELESAE_STATUS, AiradminUtils.getNotNullString(req.getParameter(PARAM_RELESAE_STATUS)));
		prop.setProperty(PARAM_ONHOLD_STATUS, AiradminUtils.getNotNullString(req.getParameter(PARAM_ONHOLD_STATUS)));
		prop.setProperty(PARAM_GROUP_ID, AiradminUtils.getNotNullString(req.getParameter(PARAM_GROUP_ID)));

		return prop;
	}
}
