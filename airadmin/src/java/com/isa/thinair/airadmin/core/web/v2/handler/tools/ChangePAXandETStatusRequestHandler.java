package com.isa.thinair.airadmin.core.web.v2.handler.tools;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.tools.ChangePAXandETStatusHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.commons.api.exception.ModuleException;

public class ChangePAXandETStatusRequestHandler  extends BasicRequestHandler {
	
	public static void setDisplayData(HttpServletRequest request) throws ModuleException {

		setClientErrors(request);
	}
	
	private static void setClientErrors(HttpServletRequest request) throws ModuleException {
		String strClientErrors = ChangePAXandETStatusHTMLGenerator.getClientErrors(request);
		if (strClientErrors == null) {
			strClientErrors = "";
		}
		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
	}

}
