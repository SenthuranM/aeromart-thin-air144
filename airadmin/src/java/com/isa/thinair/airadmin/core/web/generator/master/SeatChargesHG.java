package com.isa.thinair.airadmin.core.web.generator.master;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airmaster.api.dto.AirCraftModelSeatsDTO;
import com.isa.thinair.airmaster.api.model.AirCraftModelSeats;
import com.isa.thinair.airpricing.api.criteria.ChargeTemplateSearchCriteria;
import com.isa.thinair.airpricing.api.model.ChargeTemplate;
import com.isa.thinair.airpricing.api.model.SeatCharge;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

public class SeatChargesHG {

	private static Log log = LogFactory.getLog(SeatChargesHG.class);

	private final Properties props;
	private static final String PARAM_SEARCH_MODEL = "selModelNo";
	private static final String PARAM_SEARCH_TEMPLATE = "selTemplate";
	private static final String PARAM_RECNO = "hdnRecNo";
	private static final String PARAM_MODEL = "selFltModelNo";
	private static final String PARAM_TMPL_ID = "hdnTmplId";

	/**
	 * Constuct with Properties
	 * 
	 * @param props
	 *            the props
	 */
	public SeatChargesHG(Properties props) {
		this.props = props;

	}

	/**
	 * Gets the Charge Templates Data as per search Criteria
	 * 
	 * @param request
	 *            the request
	 * @return String the array of Data
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final String getSeatChargesHtml(HttpServletRequest request) throws ModuleException {
		if (log.isDebugEnabled()) {
			log.debug("geting the tepmlate data");
		}

		Page<ChargeTemplate> page = null;
		int recNo = 0;
		int totRec = 0;
		request.setAttribute(WebConstants.REQ_TOTAL_RECORDS, totRec);
		if (!props.getProperty(PARAM_RECNO).equals("")) {
			recNo = Integer.parseInt(props.getProperty(PARAM_RECNO));
			recNo = recNo - 1;
		}

		String strModel = props.getProperty(PARAM_SEARCH_MODEL);
		String strTempcode = props.getProperty(PARAM_SEARCH_TEMPLATE);
		ChargeTemplateSearchCriteria search = new ChargeTemplateSearchCriteria();

		if (!strModel.equals("")) {
			search.setModelNo(strModel);
		} else {
			search.setModelNo(null);
		}

		if (!strTempcode.equals("")) {
			search.setTemplateCode(strTempcode);
		} else {
			search.setTemplateCode(null);
		}

		Collection<ChargeTemplate> templCol = new ArrayList<ChargeTemplate>();
		if (strModel != null) {
			page = ModuleServiceLocator.getChargeBD().getChargeTemplates(search, recNo, 20);
		}

		if (page != null) {
			templCol.addAll(page.getPageData());
			totRec = page.getTotalNoOfRecords();
		}
		request.setAttribute(WebConstants.REQ_TOTAL_RECORDS, totRec);
		return createSeatCharges(templCol);
	}

	/**
	 * Creates the Data Array for Charge Template
	 * 
	 * @param templCol
	 *            the collection of Charge Templates
	 * @return String the Array of data
	 * @throws ModuleException
	 */
	public String createSeatCharges(Collection<ChargeTemplate> templCol) throws ModuleException {
		StringBuilder sb = new StringBuilder();
		int tempInt = 0;
		for (ChargeTemplate chgTemplate : templCol) {
			sb.append("modelChargeData[" + tempInt + "] = new Array();");
			if (chgTemplate.getModelNo() != null) {
				sb.append("modelChargeData[" + tempInt + "][1]= '" + chgTemplate.getModelNo() + "';");
			} else {
				sb.append("modelChargeData[" + tempInt + "][1]= '';");
			}
			sb.append("modelChargeData[" + tempInt + "][2]= '" + chgTemplate.getTemplateId() + "';");
			if (chgTemplate.getTemplateCode() != null) {
				sb.append("modelChargeData[" + tempInt + "][3]= '" + chgTemplate.getTemplateCode() + "';");
			} else {
				sb.append("modelChargeData[" + tempInt + "][3]= '';");
			}
			if (chgTemplate.getDescription() != null) {
				sb.append("modelChargeData[" + tempInt + "][4]= '" + chgTemplate.getDescription() + "';");
			} else {
				sb.append("modelChargeData[" + tempInt + "][4]= '';");
			}

			if (chgTemplate.getDefaultChargeAmount() != null) {
				sb.append("modelChargeData[" + tempInt + "][5]= '" + chgTemplate.getDefaultChargeAmount() + "';");
			} else {
				sb.append("modelChargeData[" + tempInt + "][5]= '';");
			}
			if (chgTemplate.getUserNote() != null) {
				sb.append("modelChargeData[" + tempInt + "][6]= '" + chgTemplate.getUserNote() + "';");
			} else {
				sb.append("modelChargeData[" + tempInt + "][6]= '';");
			}
			sb.append("modelChargeData[" + tempInt + "][7]= '" + chgTemplate.getVersion() + "';");
			sb.append("modelChargeData[" + tempInt + "][8]= new Array();");
			Collection<SeatCharge> colSeatcharge = chgTemplate.getSeatCharges();

			if (colSeatcharge != null && !colSeatcharge.isEmpty()) {
				int j = 0;
				for (SeatCharge seatCharge : colSeatcharge) {
					sb.append("modelChargeData[" + tempInt + "][8][" + j + "] = new Array();");
					sb.append("modelChargeData[" + tempInt + "][8][" + j + "][0] = '" + seatCharge.getChargeId() + "';");
					sb.append("modelChargeData[" + tempInt + "][8][" + j + "][1] = '" + seatCharge.getSeatId() + "';");
					sb.append("modelChargeData[" + tempInt + "][8][" + j + "][2] = '" + seatCharge.getTemplate().getTemplateId()
							+ "';");
					sb.append("modelChargeData[" + tempInt + "][8][" + j + "][3] = '" + seatCharge.getLocalCurrencyAmount() + "';");
					sb.append("modelChargeData[" + tempInt + "][8][" + j + "][4] = '" + seatCharge.getStatus() + "';");
					sb.append("modelChargeData[" + tempInt + "][8][" + j + "][5] = '" + seatCharge.getVersion() + "';");
					j++;
				}
			}
			sb.append("modelChargeData[" + tempInt + "][9]= '" + chgTemplate.getStatus() + "';");
			sb.append("modelChargeData[" + tempInt + "][10]= '" + chgTemplate.getChargeLocalCurrencyCode() + "';");
			tempInt++;
		}
		return sb.toString();
	}

	public String getSeatCharges(HttpServletRequest request) throws ModuleException {
		String strModel = request.getParameter(PARAM_MODEL);
		String templateId = request.getParameter(PARAM_TMPL_ID);
		ChargeTemplateSearchCriteria search = new ChargeTemplateSearchCriteria();
		int recNo = 0;
		int totRec = 0;
		request.setAttribute(WebConstants.REQ_TOTAL_RECORDS, totRec);
		if (!props.getProperty(PARAM_RECNO).equals("")) {
			recNo = Integer.parseInt(props.getProperty(PARAM_RECNO));
			recNo = recNo - 1;
		}

		Page<ChargeTemplate> page = null;
		if (!strModel.equals("")) {
			search.setModelNo(strModel);
		} else {
			search.setModelNo(null);
		}

		if (!templateId.equals("")) {
			search.setTemplateId(new Integer(templateId));
		} else {
			search.setTemplateCode(null);
		}

		Collection<ChargeTemplate> templCol = new ArrayList<ChargeTemplate>();
		if (strModel != null) {
			page = ModuleServiceLocator.getChargeBD().getSeatCharges(search, recNo, 20);
		}

		if (page != null) {
			templCol.addAll(page.getPageData());
		}

		StringBuilder sb = new StringBuilder();
		int tempInt = 0;
		for (ChargeTemplate chgTemplate : templCol) {
			sb.append("modelChargeData[" + tempInt + "] = new Array();");
			if (chgTemplate.getModelNo() != null) {
				sb.append("modelChargeData[" + tempInt + "][1]= '" + chgTemplate.getModelNo() + "';");
			} else {
				sb.append("modelChargeData[" + tempInt + "][1]= '';");
			}
			sb.append("modelChargeData[" + tempInt + "][2]= '" + chgTemplate.getTemplateId() + "';");
			if (chgTemplate.getTemplateCode() != null) {
				sb.append("modelChargeData[" + tempInt + "][3]= '" + chgTemplate.getTemplateCode() + "';");
			} else {
				sb.append("modelChargeData[" + tempInt + "][3]= '';");
			}
			if (chgTemplate.getDescription() != null) {
				sb.append("modelChargeData[" + tempInt + "][4]= '" + chgTemplate.getDescription() + "';");
			} else {
				sb.append("modelChargeData[" + tempInt + "][4]= '';");
			}

			if (chgTemplate.getDefaultChargeAmount() != null) {
				sb.append("modelChargeData[" + tempInt + "][5]= '" + chgTemplate.getDefaultChargeAmount() + "';");
			} else {
				sb.append("modelChargeData[" + tempInt + "][5]= '';");
			}
			if (chgTemplate.getUserNote() != null) {
				sb.append("modelChargeData[" + tempInt + "][6]= '" + chgTemplate.getUserNote() + "';");
			} else {
				sb.append("modelChargeData[" + tempInt + "][6]= '';");
			}
			sb.append("modelChargeData[" + tempInt + "][7]= '" + chgTemplate.getVersion() + "';");
			sb.append("modelChargeData[" + tempInt + "][8]= new Array();");
			Collection<SeatCharge> colSeatcharge = chgTemplate.getSeatCharges();

			if (colSeatcharge != null && !colSeatcharge.isEmpty()) {
				int j = 0;
				for (SeatCharge seatCharge : colSeatcharge) {
					sb.append("modelChargeData[" + tempInt + "][8][" + j + "] = new Array();");
					sb.append("modelChargeData[" + tempInt + "][8][" + j + "][0] = '" + seatCharge.getChargeId() + "';");
					sb.append("modelChargeData[" + tempInt + "][8][" + j + "][1] = '" + seatCharge.getSeatId() + "';");
					sb.append("modelChargeData[" + tempInt + "][8][" + j + "][2] = '" + seatCharge.getTemplate().getTemplateId()
							+ "';");
					sb.append("modelChargeData[" + tempInt + "][8][" + j + "][3] = '" + seatCharge.getLocalCurrencyAmount() + "';");
					sb.append("modelChargeData[" + tempInt + "][8][" + j + "][4] = '" + seatCharge.getStatus() + "';");
					sb.append("modelChargeData[" + tempInt + "][8][" + j + "][5] = '" + seatCharge.getVersion() + "';");
					j++;
				}
			}
			sb.append("modelChargeData[" + tempInt + "][9]= '" + chgTemplate.getStatus() + "';");
			sb.append("modelChargeData[" + tempInt + "][10]= '" + chgTemplate.getChargeLocalCurrencyCode() + "';");
			tempInt++;
		}
		return sb.toString();
	}

	public final String getSeatMapHtml(HttpServletRequest request) throws ModuleException {

		String strModel = request.getParameter(PARAM_MODEL);
		Collection<AirCraftModelSeats> seatCollection = new ArrayList<AirCraftModelSeats>();
		int totRows = 0;
		int totalCols = 0;
		int totSeats = 0;
		if (strModel != null) {
			AirCraftModelSeatsDTO seatTo = ModuleServiceLocator.getAircraftServiceBD().getAirCraftModelSeats(strModel);
			if (seatTo != null) {
				seatCollection.addAll(seatTo.getAirCraftModelSeats());
				totRows = seatTo.getTotalRows();
				totalCols = seatTo.getTotalColumns();
				totSeats = seatTo.getTotalseats();
			}
		}
		request.setAttribute("reqtotalRows", totRows);
		request.setAttribute("reqtotalCols", totalCols);
		request.setAttribute("reqtotalSeats", totSeats);
		return createSeatDetailsHTML(seatCollection);
	}

	private String createSeatDetailsHTML(Collection<AirCraftModelSeats> seatCollection) throws ModuleException {
		StringBuilder sb = new StringBuilder();
		StringBuilder stSb = new StringBuilder();
		int prevColGrpId = -1;
		int prevRowGroupId = -1;
		int prevRowId = -1;
		int clsInc = -1;
		int rowgpInc = -1;
		int colgpInc = -1;
		int rowInc = -1;
		int colInc = -1;
		int seatInc = 0;
		String strModelNo = null;
		String strprevCabinclass = "";

		for (AirCraftModelSeats aircraftSeatMod : seatCollection) {
			if (aircraftSeatMod != null) {
				String strCabinclassCode = aircraftSeatMod.getCabinClassCode();
				int colGroupId = aircraftSeatMod.getColGroupId();
				strModelNo = aircraftSeatMod.getModelNo();
				int rowGroupId = aircraftSeatMod.getRowGroupId();
				int rowId = aircraftSeatMod.getRowId();
				String strSeatCode = aircraftSeatMod.getSeatCode();
				int seatId = aircraftSeatMod.getSeatID();
				String strStatus = aircraftSeatMod.getStatus();
				long lngVersion = aircraftSeatMod.getVersion();
				String strType = aircraftSeatMod.getSeatType();
				String seatVisibility = aircraftSeatMod.getSeatVisibility();

				if (!strCabinclassCode.equals(strprevCabinclass)) {
					clsInc++;
					colgpInc = -1;
					prevColGrpId = -1;
					strprevCabinclass = strCabinclassCode;
					sb.append(" stMap [" + clsInc + "] = new Array();");
				}
				if (prevColGrpId != colGroupId) {
					colgpInc++;
					rowgpInc = -1;
					prevRowGroupId = -1;
					prevColGrpId = colGroupId;
					sb.append(" stMap [" + clsInc + "][" + colgpInc + "] = new Array();");
				}
				if (prevRowGroupId != rowGroupId) {
					rowgpInc++;
					rowInc = -1;
					prevRowId = -1;
					prevRowGroupId = rowGroupId;
					sb.append(" stMap [" + clsInc + "][" + colgpInc + "][" + rowgpInc + "] = new Array();");
				}

				if (prevRowId != rowId) {
					rowInc++;
					colInc = -1;
					prevRowId = rowId;
					sb.append(" stMap [" + clsInc + "][" + colgpInc + "][" + rowgpInc + "][" + rowInc + "] = new Array();");
				}
				colInc++;
				sb.append(" stMap [" + clsInc + "][" + colgpInc + "][" + rowgpInc + "][" + rowInc + "][" + colInc
						+ "] = new Array('" + seatId + "','" + strSeatCode + "','" + strStatus + "','" + lngVersion + "','"
						+ strType + "','" + seatVisibility + "');");
				stSb.append("seatCharges[" + seatInc + "] = new Array('', '" + seatId + "','','0.0','A','');");
				seatInc++;
			}
		}

		if (strModelNo != null) {
			sb.append(stSb.toString() + "aircraftModel ='" + strModelNo + "';");
		}

		return sb.toString();
	}

	/**
	 * Gets the client validation details
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the cloction of validations
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {

		String clientErrors = null;
		Properties moduleErrs = new Properties();
		moduleErrs.setProperty("um.seatmap.form.code.required", "codeFromRqrd");
		moduleErrs.setProperty("um.seatmap.form.model.required", "modelRqrd");
		moduleErrs.setProperty("um.seatmap.form.description.required", "descriptionRqrd");
		moduleErrs.setProperty("um.airadmin.delete.confirmation", "deleteConf");
		moduleErrs.setProperty("um.seatmap.form.localcurrencycode.required", "localCurrencyCodeRqrd");

		clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);

		return clientErrors;

	}
}
