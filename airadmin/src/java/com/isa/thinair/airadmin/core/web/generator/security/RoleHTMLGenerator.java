package com.isa.thinair.airadmin.core.web.generator.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airsecurity.api.model.Privilege;
import com.isa.thinair.airsecurity.api.model.Role;
import com.isa.thinair.airsecurity.api.model.RoleAccessibility;
import com.isa.thinair.airsecurity.api.model.RoleVisibility;
import com.isa.thinair.airsecurity.api.utils.Constants;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

public class RoleHTMLGenerator {

	private static String clientErrors;
	private Properties props;
	private static final String PARAM_ROLE_SEARCH_ID = "txtRoleNameSearch";
	private static final String PARAM_RECNO = "hdnRecNo";

	/**
	 * Constructor Crates an instance withe Property values
	 * 
	 * @param props
	 *            the Properties of the RoleRequestHandler
	 */
	public RoleHTMLGenerator(Properties props) {
		this.props = props;
	}

	/**
	 * Gets the Array for Role Grid Data
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Grid Row
	 * @throws ModuleException
	 *             the ModuleException
	 */
	@SuppressWarnings("unchecked")
	public final String getRoleRowHtml(HttpServletRequest request) throws ModuleException {

		Collection<Role> colRoles = null;
		Page page = null;
		int recNo = 0;
		int totRec = 0;

		if (!props.getProperty(PARAM_RECNO).equals("")) {
			recNo = Integer.parseInt(props.getProperty(PARAM_RECNO));
		}

		List<ModuleCriterion> critrian = new ArrayList<ModuleCriterion>();
		ModuleCriterion MCname = new ModuleCriterion();

		// Set criterian - Name
		MCname.setCondition(ModuleCriterion.CONDITION_LIKE_IGNORECASE);
		MCname.setFieldName("roleName");
		String strRoleNameSearch = props.getProperty(PARAM_ROLE_SEARCH_ID);
		if (strRoleNameSearch != null && !strRoleNameSearch.equals("")) {
			List<String> value = new ArrayList<String>();
			value.add("%" + props.getProperty(PARAM_ROLE_SEARCH_ID) + "%");
			MCname.setValue(value);
			critrian.add(MCname);
		}

		// Set criterian - Service channel
		ModuleCriterion MCchannel = new ModuleCriterion();
		MCchannel.setCondition(ModuleCriterion.CONDITION_IN);
		MCchannel.setFieldName("serviceChannel");
		List<String> value = new ArrayList<String>();
		value.add(Constants.INTERNAL_CHANNEL);
		value.add(Constants.BOTH_CHANNELS);
		value.add(Constants.EXTERNAL_CHANNEL);
		MCchannel.setValue(value);
		critrian.add(MCchannel);

		ModuleCriterion MCedit = new ModuleCriterion();

		// Set Criterian - UnEditable Roles roleId <> Role.UNEDITABLE_ROLE_ID
		MCedit.setCondition(ModuleCriterion.CONDITION_NOT_EQUAL);
		MCedit.setFieldName("roleId");
		List<String> value1 = new ArrayList<String>();
		value1.add(AppSysParamsUtil.getDefaultAirlineIdentifierCode() + Role.UNEDITABLE_ROLE_ID);
		MCedit.setValue(value1);
		critrian.add(MCedit);

		ModuleCriterion MCedit2 = new ModuleCriterion();
		MCedit2.setCondition(ModuleCriterion.CONDITION_NOT_EQUAL);
		MCedit2.setFieldName("roleId");
		List<String> value2 = new ArrayList<String>();
		value2.add(AppSysParamsUtil.getDefaultAirlineIdentifierCode() + Role.UNEDITABLE_ROLE_ID2);
		MCedit2.setValue(value2);
		critrian.add(MCedit2);

		ModuleCriterion MCedit3 = new ModuleCriterion();
		MCedit3.setCondition(ModuleCriterion.CONDITION_NOT_EQUAL);
		MCedit3.setFieldName("roleId");
		List<String> value3 = new ArrayList<String>();
		value3.add(AppSysParamsUtil.getDefaultAirlineIdentifierCode() + Role.UNEDITABLE_ROLE_ID3);
		MCedit3.setValue(value3);
		critrian.add(MCedit3);

		ModuleCriterion MCedit4 = new ModuleCriterion();
		MCedit4.setCondition(ModuleCriterion.CONDITION_LIKE);
		MCedit4.setFieldName("roleId");
		List<String> value4 = new ArrayList<String>();
		value4.add(AppSysParamsUtil.getDefaultAirlineIdentifierCode() + "%");
		MCedit4.setValue(value4);
		critrian.add(MCedit4);

		page = ModuleServiceLocator.getSecurityBD().searchRoles(critrian, recNo - 1, 20);
		String strJavascriptTotalNoOfRecs = "";
		if (recNo > page.getTotalNoOfRecords()) {
			page = ModuleServiceLocator.getSecurityBD().searchRoles(critrian, 0, 20);
			strJavascriptTotalNoOfRecs += " top[0].intLastRec = 1; ";
		}

		if (page != null) {
			colRoles = page.getPageData();
			totRec = page.getTotalNoOfRecords();
			request.setAttribute(WebConstants.REQ_TOTAL_RECORDS, "'" + new Integer(totRec).toString() + "';");
		}

		return createRoleRowHtml(colRoles, strJavascriptTotalNoOfRecs);
	}

	/**
	 * Creates the Grid Row From the Collection of Roles
	 * 
	 * @param roles
	 *            the Collection of Roles
	 * @param additionalHTML
	 *            the Record No data
	 * @return String the Grid Row Array String
	 * @throws ModuleException
	 *             the ModuleException
	 */
	@SuppressWarnings("unchecked")
	private static String createRoleRowHtml(Collection<Role> roles, String additionalHTML) throws ModuleException {

		StringBuffer sb = new StringBuffer("var roleData = new Array();");
		sb.append(additionalHTML);
		Role role = null;
		int tempInt = 0;

		Collection<String> roleIdCollection = new ArrayList<String>();
		Iterator<Role> rolesIter = null;
		if (roles != null) {
			rolesIter = roles.iterator();
			while (rolesIter.hasNext()) {
				role = (Role) rolesIter.next();
				roleIdCollection.add(role.getRoleId());
			}
		}

		Map<String, Collection<Privilege>> privilagesByRoleIdMap = ModuleServiceLocator.getSecurityBD().getPrivilegesForRoleIds(roleIdCollection,
				Constants.INTERNAL_CHANNEL);
		
		Map<String, Set<String>> assignedAgentTypesForRolesMap = ModuleServiceLocator.getSecurityBD().getAssignedAgentTypesForRoles(roleIdCollection);

		if (roles != null) {
			rolesIter = roles.iterator();
			while (rolesIter.hasNext()) {
				role = (Role) rolesIter.next();
				String strAssign = "";
				sb.append("roleData[" + tempInt + "] = new Array();");
				sb.append("roleData[" + tempInt + "][0] = '" + tempInt + "';");
				sb.append("roleData[" + tempInt + "][1] = '" + role.getRoleId() + "';");
				if (role.getRoleName() != null)
					sb.append("roleData[" + tempInt + "][2] = '" + role.getRoleName() + "';");
				else
					sb.append("roleData[" + tempInt + "][2] = '';");

				if (role.getStatus() != null)
					sb.append("roleData[" + tempInt + "][3] = '" + AiradminUtils.getModifiedStatus(role.getStatus()) + "';");
				else
					sb.append("roleData[" + tempInt + "][3] = '';");

				if (role.getRemarks() != null)
					sb.append("roleData[" + tempInt + "][6] = '" + role.getRemarks() + "';");
				else
					sb.append("roleData[" + tempInt + "][6] = '';");

				sb.append("roleData[" + tempInt + "][4] = '" + role.getVersion() + "';");
				sb.append("roleData[" + tempInt + "][7] = '" + (role.isEditable() ? "Y" : "N") + "';");

				// Gets the Assigned privileges for the Role
				if (privilagesByRoleIdMap != null) {
					Collection<Privilege> privilagesCol = (Collection<Privilege>) privilagesByRoleIdMap.get(role.getRoleId());
					if (privilagesCol != null && privilagesCol.size() > 0) {
						for (Iterator<Privilege> iterator = privilagesCol.iterator(); iterator.hasNext();) {
							Privilege privilage = (Privilege) iterator.next();
							strAssign += privilage.getPrivilegeId() + ",";
						}
					}
				}

				if (strAssign.length() > 2) {
					strAssign = strAssign.substring(0, strAssign.length() - 1);
				}
				sb.append("roleData[" + tempInt + "][5] = '" + strAssign + "';");
				sb.append("roleData[" + tempInt + "][9] = new Array();");
				sb.append("roleData[" + tempInt + "][10] = new Array();");
				if (role.getAgentTypeVisibilities() != null && !role.getAgentTypeVisibilities().isEmpty()) {
					Set<RoleVisibility> setAgs = role.getAgentTypeVisibilities();
					int al = 0;
					RoleVisibility roleVisi = null;
					for (Iterator<RoleVisibility> agIter = setAgs.iterator(); agIter.hasNext();) {
						roleVisi = (RoleVisibility) agIter.next();
						sb.append("roleData[" + tempInt + "][9][" + al + "] = '" + roleVisi.getAgentTypeCode() + "';");
						al++;
					}
				}
				if (role.getAgentTypeAccessiblity() != null && !role.getAgentTypeAccessiblity().isEmpty()) {// change to
																											// accible
																											// method
					Set<RoleAccessibility> setAgs = role.getAgentTypeAccessiblity();
					int al = 0;
					RoleAccessibility roleAcc = null;
					for (Iterator<RoleAccessibility> agIter = setAgs.iterator(); agIter.hasNext();) {
						roleAcc = (RoleAccessibility) agIter.next();
						sb.append("roleData[" + tempInt + "][10][" + al + "] = '" + roleAcc.getAgentTypeCode() + "';");
						al++;
					}
				}
				
				sb.append("roleData[" + tempInt + "][11] = '" + role.getServiceChannel() + "';");
				sb.append("roleData[" + tempInt + "][12] = '" + PlatformUtiltiies.nullHandler(role.getIncludeExcludeFlag())
						+ "';");
				sb.append("roleData[" + tempInt + "][13] = '" + role.getAirlineCode() + "';");
				sb.append("roleData[" + tempInt + "][14] = new Array();");
				if (assignedAgentTypesForRolesMap != null && assignedAgentTypesForRolesMap.size() > 0) {
					Object assignedAgentTypes = assignedAgentTypesForRolesMap.get(role.getRoleId());
					int al = 0;
					if(assignedAgentTypes != null){
						Set<String> agentTypes = (Set<String>)assignedAgentTypes;		
						for(String agentType: agentTypes){
							sb.append("roleData[" + tempInt + "][14][" + al + "] = '" + agentType + "';");
							al++;
						}
						
					}
				}
				
				
				tempInt++;
			}
		}
		return sb.toString();
	}

	/**
	 * Gets the Client Validations
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the array of Client validatins
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {

		if (clientErrors == null) {
			Properties moduleErrs = new Properties();

			moduleErrs.setProperty("um.role.form.id.required", "roleIdRqrd");
			moduleErrs.setProperty("um.role.form.name.required", "roleNameRqrd");
			moduleErrs.setProperty("um.airadmin.delete.confirmation", "deleteConf");
			moduleErrs.setProperty("um.role.form.agType.required", "agentRqed");
			moduleErrs.setProperty("um.role.form.accType.required", "accAgentReqrd");
			moduleErrs.setProperty("um.role.form.servCh.required", "servChRqrd");
			moduleErrs.setProperty("um.role.form.exc.invalid", "excRoleInvalid");
			moduleErrs.setProperty("um.role.form.airline.invalid", "applicableAirlineInvalid");
			moduleErrs.setProperty("um.role.form.agType.inconsistent", "agTypeChangeWarn");
			clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}

		return clientErrors;
	}
}
