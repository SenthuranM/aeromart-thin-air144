/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */

/**
 * @author srikantha
 *
 * 
 * Window - Preferences - Java - Code Style - Code Templates
 */

package com.isa.thinair.airadmin.core.web.generator.tools;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Iterator;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airreservation.api.model.PfsPaxEntry;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;

public class PFSDetailProcessingHTMLGenerator {

	private static String clientErrors;
	private static final String PARAM_CURRENT_PFS = "hdnCurrentPfs";
	private static final String PARAM_SEARCH_PROCESS_STATUS = "selProcessType";

	private String strFormFieldsVariablesJS = "";
	private static final String DATE_FORMAT_DDMMYYYY = "dd/MM/yyyy HH:mm";
	SimpleDateFormat outputDateFormat;

	/**
	 * Gets the PFS Passenger Details Rows for the Selected PFS
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array of PFS Detail Rows
	 * @throws ModuleException
	 *             the ModuleException
	 * @throws Exception
	 *             the Exception
	 */
	public final String getPFSProcessingRowHtml(HttpServletRequest request) throws ModuleException, Exception {

		String strUIModeJS = "var isSearchMode = false; var strPFSContent;";
		String strProcessStatus = "";
		outputDateFormat = new SimpleDateFormat(DATE_FORMAT_DDMMYYYY);
		strUIModeJS = "var isSearchMode = true; var strPFSContent;";

		strProcessStatus = request.getParameter(PARAM_SEARCH_PROCESS_STATUS);
		strFormFieldsVariablesJS += "var processStatus ='" + strProcessStatus + "';";

		request.setAttribute(WebConstants.REQ_OPERATION_UI_MODE, strUIModeJS);
		setFormFieldValues(strFormFieldsVariablesJS);

		return createPFSProcessingRowHTML(request);
	}

	/**
	 * Creates the PFS Processing Row for the Selected PFS
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array containig the PFS details
	 * @throws Exception
	 *             the Exception
	 */
	private String createPFSProcessingRowHTML(HttpServletRequest request) throws Exception {

		String strCurrentPFS = "";

		strCurrentPFS = AiradminUtils.getNotNullString(request.getParameter(PARAM_CURRENT_PFS));
		StringBuffer sb = new StringBuffer("var arrData = new Array();");

		String tempDesc = "";
		String paxProcessedStatus = "";
		String paxErrorStatus = "";
		String[] arrCurrentPFS = strCurrentPFS.split(",");
		Collection<PfsPaxEntry> pfsPaxEntries = ModuleServiceLocator.getReservationBD().getPfsParseEntries(
				Integer.valueOf(arrCurrentPFS[3]).intValue());

		if (pfsPaxEntries != null && !pfsPaxEntries.isEmpty()) {
			int i = 0;
			String strTemp = "";
			for (Iterator<PfsPaxEntry> iterPfsPaxEntries = pfsPaxEntries.iterator(); iterPfsPaxEntries.hasNext();) {
				PfsPaxEntry pfsPaxEntry = (PfsPaxEntry) iterPfsPaxEntries.next();
				paxProcessedStatus = "";
				paxErrorStatus = "";
				if (pfsPaxEntry.getPaxType() != null && pfsPaxEntry.getPaxType().equals("IN")
						&& pfsPaxEntry.getParentPpId() != null) {
					continue;
				}

				sb.append("arrData[" + i + "] = new Array();");

				if (pfsPaxEntry.getTitle() != null) {
					sb.append("arrData[" + i + "][0]= '" + pfsPaxEntry.getTitle() + "';");
				} else {
					sb.append("arrData[" + i + "][0] = ''; ");
				}

				if (pfsPaxEntry.getFirstName() != null) {
					sb.append("arrData[" + i + "][1]= '" + pfsPaxEntry.getFirstName() + "'; ");
				} else {
					sb.append("arrData[" + i + "][1] = ''; ");
				}

				if (pfsPaxEntry.getIsParent() != null && pfsPaxEntry.getIsParent().equals("Y")) {
					boolean isInfantFound = false;
					for (Iterator<PfsPaxEntry> iterPaxEntriesInf = pfsPaxEntries.iterator(); iterPaxEntriesInf.hasNext();) {
						PfsPaxEntry pfsEntryInf = (PfsPaxEntry) iterPaxEntriesInf.next();
						if (pfsEntryInf.getPaxType().equals("IN") && pfsEntryInf.getParentPpId() != null
								&& pfsEntryInf.getParentPpId().equals(pfsPaxEntry.getPpId())) {
							paxProcessedStatus = pfsEntryInf.getProcessedStatus();
							paxErrorStatus = pfsEntryInf.getErrorDescription();
							sb.append("arrData[" + i + "][16] = '"
									+ (pfsEntryInf.getTitle() != null ? pfsEntryInf.getTitle() : "") + " "
									+ pfsEntryInf.getFirstName() + " " + pfsEntryInf.getLastName() + "';");
							isInfantFound = true;
							break;
						}
					}

					if (!isInfantFound) {
						sb.append("arrData[" + i + "][16] = ''; ");
					}
				} else {
					sb.append("arrData[" + i + "][16] = ''; ");
				}

				if (pfsPaxEntry.getLastName() != null) {
					// sb.append("arrData[" + i + "][2] = '" + pfsPaxEntry.getLastName() + "'; ");
					sb.append("arrData[" + i + "][2] = '" + pfsPaxEntry.getLastName() + "'; ");
				} else {
					sb.append("arrData[" + i + "][2] = ''; ");
				}

				if (pfsPaxEntry.getPnr() != null) {
					sb.append("arrData[" + i + "][3] = '" + pfsPaxEntry.getPnr() + "'; ");
				} else {
					sb.append("arrData[" + i + "][3] = ''; ");
				}

				if (pfsPaxEntry.getArrivalAirport() != null) {
					sb.append("arrData[" + i + "][4] = '" + pfsPaxEntry.getArrivalAirport() + "'; ");
				} else {
					sb.append("arrData[" + i + "][4] = ''; ");
				}

				if (pfsPaxEntry.getEntryStatus() != null) {
					tempDesc = "";
					if (pfsPaxEntry.getEntryStatus().equals(ReservationInternalConstants.PfsPaxStatus.GO_SHORE))
						tempDesc = "GOSHO";
					else if (pfsPaxEntry.getEntryStatus().equals(ReservationInternalConstants.PfsPaxStatus.NO_SHORE))
						tempDesc = "NOSHO";
					else if (pfsPaxEntry.getEntryStatus().equals(ReservationInternalConstants.PfsPaxStatus.NO_REC))
						tempDesc = "NOREC";
					else if (pfsPaxEntry.getEntryStatus().equals(ReservationInternalConstants.PfsPaxStatus.OFF_LD))
						tempDesc = "OFFLD";
					else
						tempDesc = "Unknown";

					sb.append("arrData[" + i + "][5] = '" + tempDesc + "';");
				} else {
					sb.append("arrData[" + i + "][5] = '';");
				}

				if (pfsPaxEntry.getProcessedStatus() != null) {
					tempDesc = "";
					if (pfsPaxEntry.getProcessedStatus().equals(ReservationInternalConstants.PfsProcessStatus.ERROR_OCCURED)
							|| paxErrorStatus != null && !paxErrorStatus.equals("")) {
						tempDesc = "Error Occured";
						if (pfsPaxEntry.getErrorDescription() != null && !pfsPaxEntry.getErrorDescription().trim().equals("")) {
							strTemp = "<a HREF='javascript:void(0);' title='"
									+ (pfsPaxEntry.getErrorDescription() == null ? "" : pfsPaxEntry.getErrorDescription().trim())
									+ "'>" + tempDesc + "</a>";
							char doublequote = '"';
							sb.append("arrData[" + i + "][6] = " + doublequote + strTemp + doublequote + ";");
						} else if (paxErrorStatus != null && !paxErrorStatus.trim().equals("")) {
							strTemp = "<a HREF='javascript:void(0);' title='"
									+ (paxErrorStatus == null ? "" : paxErrorStatus.trim()) + "'>" + tempDesc + "</a>";
							char doublequote = '"';
							sb.append("arrData[" + i + "][6] = " + doublequote + strTemp + doublequote + ";");

						} else {
							sb.append("arrData[" + i + "][6] = '" + tempDesc + "';");
						}
					} else if (pfsPaxEntry.getProcessedStatus().equals(
							ReservationInternalConstants.PfsProcessStatus.NOT_PROCESSED)) {
						tempDesc = "Not Processed";
						sb.append("arrData[" + i + "][6] = '" + tempDesc + "';");
					} else if (pfsPaxEntry.getProcessedStatus().equals(ReservationInternalConstants.PfsProcessStatus.PROCESSED)) {
						if (paxProcessedStatus.equals("")
								|| paxProcessedStatus.equals(ReservationInternalConstants.PfsProcessStatus.PROCESSED)) {
							tempDesc = "Processed";
							if (pfsPaxEntry.getErrorDescription() != null && !pfsPaxEntry.getErrorDescription().trim().equals("")) {
								strTemp = "<a HREF='javascript:void(0);' title='"
										+ (pfsPaxEntry.getErrorDescription() == null ? "" : pfsPaxEntry.getErrorDescription().trim())
										+ "'>" + tempDesc + "</a>";
								char doublequote = '"';
								sb.append("arrData[" + i + "][6] = " + doublequote + strTemp + doublequote + ";");
							}else{
								sb.append("arrData[" + i + "][6] = '" + tempDesc + "';");
							}
						} else {
							tempDesc = "Not Processed";
							sb.append("arrData[" + i + "][6] = '" + tempDesc + "';");
						}
					} else {
						tempDesc = "Unknown";
						sb.append("arrData[" + i + "][6] = '" + tempDesc + "';");
					}
				} else {
					sb.append("arrData[" + i + "][6] = '';");
				}

				if (pfsPaxEntry.getEntryStatus() != null) {
					sb.append("arrData[" + i + "][7] = '" + pfsPaxEntry.getEntryStatus() + "';");
				} else {
					sb.append("arrData[" + i + "][7] = '';");
				}

				if (pfsPaxEntry.getProcessedStatus() != null) {
					sb.append("arrData[" + i + "][8] = '" + pfsPaxEntry.getProcessedStatus() + "';");
				} else {
					sb.append("arrData[" + i + "][8] = '';");
				}

				if (pfsPaxEntry.getPaxType() != null) {
					sb.append("arrData[" + i + "][15]= '" + pfsPaxEntry.getPaxType() + "'; ");
				} else {
					sb.append("arrData[" + i + "][15] = ''; ");
				}
				sb.append("arrData[" + i + "][9] = '" + pfsPaxEntry.getPpId() + "';");
				sb.append("arrData[" + i + "][10] = '" + pfsPaxEntry.getVersion() + "';");
				sb.append("arrData[" + i + "][11] = '" + pfsPaxEntry.getPfsId().toString() + "';");
				sb.append("arrData[" + i + "][12] = '"
						+ (pfsPaxEntry.getErrorDescription() == null ? "" : pfsPaxEntry.getErrorDescription().trim()) + "';");
				sb.append("arrData[" + i + "][13] = '" + pfsPaxEntry.getCabinClassCode() + "';");
				sb.append("arrData[" + i + "][14] = '" + pfsPaxEntry.getPaxCategoryCode() + "';");
				String isParent = (pfsPaxEntry.getIsParent() != null && !pfsPaxEntry.getIsParent().equals("")) ? "Y" : "N";
				sb.append("arrData[" + i + "][17] = '" + isParent + "';");
				if (pfsPaxEntry.getExtETicketNo() != null) {
					sb.append("arrData[" + i + "][18] = '" + pfsPaxEntry.getExtETicketNo().trim() + "';");
				} else {
					sb.append("arrData[" + i + "][18] = '"
							+ (pfsPaxEntry.geteTicketNo() == null ? "" : pfsPaxEntry.geteTicketNo().trim()) + "';");
				}
				i++;
			}
		}
		return sb.toString();
	}

	/**
	 * Creates Client Validations for PFS Detail Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array Containing the Client Validations
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {

		if (clientErrors == null) {
			Properties moduleErrs = new Properties();

			moduleErrs.setProperty("um.pfsprocess.form.fromdate.required", "fromDateRqrd");
			moduleErrs.setProperty("um.pfsprocess.form.todate.required", "toDateRqrd");
			moduleErrs.setProperty("um.pfsprocess.form.fromdate.invalid", "fromDateInvalid");
			moduleErrs.setProperty("um.pfsprocess.form.todate.invalid", "toDateInvalid");
			moduleErrs.setProperty("um.process.form.airport.required", "airportRqrd");
			moduleErrs.setProperty("um.pfsprocess.form.todate.lessthan.fromdate", "todateLessthanFromDate");
			moduleErrs.setProperty("um.pfsprocess.form.todate.lessthan.currentdate", "todateLessthanCurrentDate");
			moduleErrs.setProperty("um.process.form.flightDate.required", "flightDateRqrd");
			moduleErrs.setProperty("um.pfsprocess.form.flightDateFormat.invaid", "flightDateFormatInvalid");
			moduleErrs.setProperty("um.pfsprocess.form.flightTimeFormat.invaid", "flightTimeFormatInvalid");
			moduleErrs.setProperty("um.process.form.downloadTS.required", "downloadTSRqrd");
			moduleErrs.setProperty("um.pfsprocess.form.downloadTSDateFormat.invaid", "downloadTSDateFormatInvalid");
			moduleErrs.setProperty("um.pfsprocess.form.downloadTSTimeFormat.invaid", "downloadTSTimeFormatInvalid");
			moduleErrs.setProperty("um.process.form.flightNo.required", "flightNoRqrd");
			moduleErrs.setProperty("um.process.form.title.required", "titleRqrd");
			moduleErrs.setProperty("um.pfsprocess.form.pax.type.required", "paxTypeRqrd");
			moduleErrs.setProperty("um.pfsprocess.form.firstName.required", "firstNameRqrd");
			moduleErrs.setProperty("um.pfsprocess.form.lastName.required", "lastNameRqrd");
			moduleErrs.setProperty("um.pfsprocess.form.PNR.required", "PNRRqrd");
			moduleErrs.setProperty("um.pfsprocess.form.destination.required", "destinationRqrd");
			moduleErrs.setProperty("um.pfsprocess.form.paxStatus.required", "paxStatusRqrd");
			moduleErrs.setProperty("um.airadmin.delete.confirmation", "deleteMessage");
			moduleErrs.setProperty("um.airadmin.delete.all.confirmation", "deleteAllMessage");
			moduleErrs.setProperty("um.pfsprocess.form.noofpax.required", "paxnoRqrd");
			moduleErrs.setProperty("um.pfsprocess.form.cc.notmatch", "ccnotmatch");
			moduleErrs.setProperty("um.pfsprocess.form.pfs.paxDuplicate", "paxDuplicate");
			moduleErrs.setProperty("um.pfsprocess.form.pfs.paxWrongCCCode", "paxWrongCCCode");
			moduleErrs.setProperty("um.pfsprocess.form.pfs.infantPaxParentReq", "infantPaxParentReq");
			moduleErrs.setProperty("um.airadmin.form.eTicketReq", "eTicketReq");

			clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}
		return clientErrors;
	}

	private void setFormFieldValues(String strFormFieldsVariablesJS) {
		this.strFormFieldsVariablesJS = strFormFieldsVariablesJS;
	}

	public String getFormFieldValues() {
		return strFormFieldsVariablesJS;
	}

	public String getInfantPaxParentHTML(HttpServletRequest request) throws ModuleException {

		String strCurrentPFS = "";
		StringBuffer sb = new StringBuffer("<option value=''></option>");

		strCurrentPFS = AiradminUtils.getNotNullString(request.getParameter(PARAM_CURRENT_PFS));
		String[] arrCurrentPFS = strCurrentPFS.split(",");
		Collection<PfsPaxEntry> pfsPaxEntries = ModuleServiceLocator.getReservationBD().getPfsParseEntries(
				Integer.valueOf(arrCurrentPFS[3]).intValue());

		if (pfsPaxEntries != null && !pfsPaxEntries.isEmpty()) {
			for (Iterator<PfsPaxEntry> pfsPaxIterator = pfsPaxEntries.iterator(); pfsPaxIterator.hasNext();) {
				PfsPaxEntry pfsPaxEntry = (PfsPaxEntry) pfsPaxIterator.next();

				if (pfsPaxEntry.getPaxType() != null && pfsPaxEntry.getPaxType().equals("AD")) {
					sb.append(" <option value='" + pfsPaxEntry.getPpId() + "'>" + pfsPaxEntry.getTitle() + " "
							+ pfsPaxEntry.getFirstName() + " " + pfsPaxEntry.getLastName() + "</option>");
				}
			}

		}

		return sb.toString();
	}

}
