/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airadmin.core.web.util;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.commons.api.constants.DayOfWeek;
import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;

/**
 * @author Srikantha
 * 
 */
public class SeatInventoryHelper {

	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	/**
	 * Updates the Status of the Flights
	 * 
	 * @param colFlightIDs
	 *            the Collection of Flight Ids
	 * @param strStatus
	 *            the dtatus of the Flight
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public void updateFlightStatus(Collection<Integer> colFlightIDs, FlightStatusEnum strStatus) throws ModuleException {
		ModuleServiceLocator.getFlightServiceBD().changeFlightStatus(colFlightIDs, strStatus);
	}

	/**
	 * Creates a Cabin Class Code List
	 * 
	 * @param cabinClasses
	 *            the List of Cabin Classes
	 * @return String the Cabin Class code String
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public String prepareCabinClassCodeList(List<String> cabinClasses) throws ModuleException {

		StringBuffer sb = new StringBuffer();
		if (cabinClasses != null) {
			for (int i = 0; i < cabinClasses.size(); i++) {
				String ccCode = ((String) cabinClasses.get(i));
				sb.append(ccCode + ",");
			}
		}
		return sb.toString();
	}

	/**
	 * Gets the Default Cabin Class
	 * 
	 * @return String the Default Cabin Class
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public String getDefaultCabinClass() throws ModuleException {
		return globalConfig.getBizParam(SystemParamKeys.CLASS_OF_SERVICE);
	}

	/**
	 * Gets the Frequency of a Given Flight
	 * 
	 * @param flightId
	 *            the Flight ID
	 * @return String the Frequency
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public String getFrequencyForFlight(int flightId) throws ModuleException {
		Iterator<DayOfWeek> iterDays = null;
		String strFrequencyJS = "var frequncy = ";
		StringBuffer frequecyBuff = new StringBuffer();
		Frequency frequency = ModuleServiceLocator.getFlightServiceBD().getScheduleFrequencyOfFlight(flightId);
		if (frequency != null) {
			iterDays = CalendarUtil.getDaysFromFrequency(frequency).iterator();

			while (iterDays.hasNext()) {
				DayOfWeek strDay = (DayOfWeek) iterDays.next();
				if (strDay.equals(DayOfWeek.SATURDAY))
					frequecyBuff.append("Sat,");
				if (strDay.equals(DayOfWeek.SUNDAY))
					frequecyBuff.append("Sun,");
				if (strDay.equals(DayOfWeek.MONDAY))
					frequecyBuff.append("Mon,");
				if (strDay.equals(DayOfWeek.TUESDAY))
					frequecyBuff.append("Tue,");
				if (strDay.equals(DayOfWeek.WEDNESDAY))
					frequecyBuff.append("Wed,");
				if (strDay.equals(DayOfWeek.THURSDAY))
					frequecyBuff.append("Thu,");
				if (strDay.equals(DayOfWeek.FRIDAY))
					frequecyBuff.append("Fri,");
			}
			strFrequencyJS += "'" + frequecyBuff.toString() + "';";
		} else {
			strFrequencyJS += "'';";
		}

		return strFrequencyJS;
	}
}
