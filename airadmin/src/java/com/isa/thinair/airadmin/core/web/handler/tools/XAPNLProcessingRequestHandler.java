/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */

/**
 * @author Srikantha
 *
 */

package com.isa.thinair.airadmin.core.web.handler.tools;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.tools.XAPNLProcessingHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airreservation.api.model.Pfs;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author srikantha
 * 
 * 
 *         Preferences - Java - Code Style - Code Templates
 */

public final class XAPNLProcessingRequestHandler extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(XAPNLProcessingRequestHandler.class);

	private static AiradminConfig airadminConfig = new AiradminConfig();

	private static final String PARAM_VERSION = "hdnVersion";
	private static final String PARAM_PFS_DATA = "hdnPFSData";
	private static final String PARAM_CURRENT_PFS = "hdnCurrentPfs";
	private static final String PARAM_GRIDROW = "hdnCurrentRowNum";
	private static final String PARAM_PAXCOUNT = "hdnPaxCount";
	private static final String PARAM_PFS_DOWNLOAD_TS = "txtDownloadTS";
	private static final String PARAM_PFS_DOWNLOAD_TIME = "txtDownloadTime";
	private static final String PARAM_PFS_FLIGHT_NO = "txtFlightNo";
	private static final String PARAM_PFS_FLIGHT_DATE = "txtFlightDateTime";
	private static final String PARAM_PFS_FLIGHT_TIME = "txFlightTime";
	private static final String PARAM_PFS_FROM_AIRPORT = "selDest";
	private static final String PARAM_PFS_FROM_ADDRESS = "txFromAddress";
	private static final String PARAM_PFS_STATUS = "selStatus";
	private static final String PARAM_CURRENT_PFS_ID = "hdnPFSID";
	private static final String PARAM_MODE = "hdnMode";
	private static final String PARAM_UI_MODE = "hdnUIMode";
	private static final String PARAM_UI_PFS_CONTENT = "txtaRulesCmnts";

	private static final SimpleDateFormat outputDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	private static final SimpleDateFormat outputDateFormatHHmm = new SimpleDateFormat("HH:mm");

	/**
	 * Sucess Values: 0 - Saved Successfuly 1 - Processed Successfully 2 - Delete Successfully -1 - Error occurred - No
	 * releavant field identified -2 - Error occurred - Downloaded Date -3 - Error occurred - Downloaded Time -4 - Error
	 * occurred - Flight Number -5 - Error occurred - Flight Date -6 - Error occurred - Flight Time -7 - Error occurred
	 * - Airport -8 - Error occurred - Sita Address
	 * 
	 */
	private static void setIntSuccess(HttpServletRequest request, int value) {
		setAttribInRequest(request, "intSuccess", new Integer(value));
	}

	private static int getIntSuccess(HttpServletRequest request) {
		Integer returnValue = (Integer) getAttribInRequest(request, "intSuccess");
		if (returnValue != null)
			return returnValue.intValue();
		else
			return -1;
	}

	private static void setExceptionOccured(HttpServletRequest request, boolean value) {
		setAttribInRequest(request, "isExceptionOccured", new Boolean(value));
	}

	private static boolean isExceptionOccured(HttpServletRequest request) {
		return ((Boolean) getAttribInRequest(request, "isExceptionOccured")).booleanValue();
	}

	/**
	 * Main Execute Method For PFS Processing & Sets the Success int 0-Not Applicable, 1-PFS process Success, 2-PFS save
	 * Success 3-Fail
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {

		String strHdnMode = request.getParameter(PARAM_MODE);
		String forward = WebConstants.FORWARD_SUCCESS;
		String strUIModeJS = "var isSearchMode = false; var strPFSContent;";
		setExceptionOccured(request, false);
		String strFormData = "var saveSuccess = " + getIntSuccess(request) + ";";
		strFormData += " var arrFormData = new Array();";
		request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
		request.setAttribute(WebConstants.REQ_OPERATION_UI_MODE, strUIModeJS);

		try {
			if ((strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_PROCESS))
					|| (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_SAVE))
					|| (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_DELETE))) {
				processPFSData(request);
			}
			setDisplayData(request);
		} catch (Exception exception) {
			if (exception instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
			}
		}
		return forward;
	}

	/**
	 * Validate the Flight No entered
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @param flightNumber
	 *            the flightNumber
	 * @return boolean the validate-true not-false
	 * @throws Exception
	 *             the Exception
	 */
	private static boolean validateFlightNumber(HttpServletRequest request, String flightNumber) throws Exception {
		Collection<String> flightNumbers = ModuleServiceLocator.getFlightServiceBD().getFlightNumbers();
		for (Iterator<String> iterFlightNumbers = flightNumbers.iterator(); iterFlightNumbers.hasNext();) {
			String flightNo = (String) iterFlightNumbers.next();
			if (flightNo.equalsIgnoreCase(flightNumber.trim())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Process the PFS
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return boolean processed-true not processed-false
	 * @throws Exception
	 *             the Exception
	 */
	private static boolean processPFSData(HttpServletRequest request) throws Exception {

		String strPFSData = null;
		String strCurrentPFS = null;
		String strVersion = null;
		String strGridRowNo = null;
		String strDownLoadTimeStamp = null;
		String strDownLoadTime = "";
		Date downLoadTimeStamp = null;
		String strFlightNo = null;
		String strFlightDate = null;
		String strFlightTime = "";
		Date flightDate = null;
		String strFromAirPort = null;
		String strFromAddress = null;
		String strStatus = null;
		String strCurrentPFSID = null;
		String strMode = null;
		String strHdnUIMode = "";
		String strPFSContent = "";
		String strPaxCount = "0";

		try {
			String strHdnMode = request.getParameter(PARAM_MODE);
			strHdnUIMode = AiradminUtils.getNotNullString(request.getParameter(PARAM_UI_MODE));
			strVersion = (request.getParameter(PARAM_VERSION) == null || request.getParameter(PARAM_VERSION).trim().equals("")) ? "-1"
					: request.getParameter(PARAM_VERSION);
			strPFSData = AiradminUtils.getNotNullString(request.getParameter(PARAM_PFS_DATA));
			strPFSContent = AiradminUtils.getNotNullString(request.getParameter(PARAM_UI_PFS_CONTENT));
			strCurrentPFS = AiradminUtils.getNotNullString(request.getParameter(PARAM_CURRENT_PFS));
			strGridRowNo = AiradminUtils.getNotNullString(request.getParameter(PARAM_GRIDROW));
			strDownLoadTimeStamp = AiradminUtils.getNotNullString(request.getParameter(PARAM_PFS_DOWNLOAD_TS)).trim();
			strDownLoadTime = AiradminUtils.getNotNullString(request.getParameter(PARAM_PFS_DOWNLOAD_TIME)).trim();
			downLoadTimeStamp = strDownLoadTimeStamp.equals("") ? null : outputDateFormat.parse(strDownLoadTimeStamp
					+ (strDownLoadTime.equals("") ? "" : " " + strDownLoadTime));
			strFlightNo = AiradminUtils.getNotNullString(request.getParameter(PARAM_PFS_FLIGHT_NO)).trim().toUpperCase();
			strPaxCount = request.getParameter(PARAM_PAXCOUNT) == null ? "0" : request.getParameter(PARAM_PAXCOUNT);
			strFlightDate = AiradminUtils.getNotNullString(request.getParameter(PARAM_PFS_FLIGHT_DATE));
			strFlightTime = AiradminUtils.getNotNullString(request.getParameter(PARAM_PFS_FLIGHT_TIME)).trim();
			if (!strFlightDate.equals("")) {
				flightDate = strFlightDate.equals("") ? null : outputDateFormat.parse(strFlightDate
						+ (strFlightTime.equals("") ? " 00:00" : " " + strFlightTime));
			}
			strFromAirPort = AiradminUtils.getNotNullString(request.getParameter(PARAM_PFS_FROM_AIRPORT));
			strFromAddress = AiradminUtils.getNotNullString(request.getParameter(PARAM_PFS_FROM_ADDRESS));
			strStatus = AiradminUtils.getNotNullString(request.getParameter(PARAM_PFS_STATUS));
			strCurrentPFSID = (request.getParameter(PARAM_CURRENT_PFS_ID) == null || request.getParameter(PARAM_CURRENT_PFS_ID)
					.equals("")) ? "0" : request.getParameter(PARAM_CURRENT_PFS_ID);
			strMode = AiradminUtils.getNotNullString(request.getParameter(PARAM_MODE));

			if (!strHdnMode.equals("") && strHdnMode.equals("SAVE")) {
				Pfs pfs = new Pfs();

				if (strFlightNo.equals("") || validateFlightNumber(request, strFlightNo)) {
					if (strFlightNo != null && !strFlightNo.equals("") && strFromAirPort != null && !strFromAirPort.equals("-1")
							&& flightDate != null) {
						List<FlightSegement> flightSegments = (List<FlightSegement>) ModuleServiceLocator.getFlightServiceBD()
								.getFlightSegmentsForLocalDate(strFromAirPort, null, strFlightNo, flightDate, strFlightTime.equals(""));
						if (flightSegments != null
								&& flightSegments.size() == 1
								&& (strFlightTime.equals("") || (!strFlightTime.equals("") && outputDateFormatHHmm.format(
										((FlightSegement) flightSegments.get(0)).getEstTimeDepatureLocal()).equals(strFlightTime)))) {
							FlightSegement flightSegement = (FlightSegement) flightSegments.get(0);

							pfs.setDateDownloaded(downLoadTimeStamp);
							pfs.setDepartureDate(flightSegement.getEstTimeDepatureLocal());
							pfs.setFlightNumber(strFlightNo.equals("") ? null : strFlightNo.toUpperCase());
							pfs.setFromAddress(strFromAddress.equals("") ? null : strFromAddress);
							pfs.setFromAirport(strFromAirPort.equals("-1") ? null : strFromAirPort);
							pfs.setPpId(Integer.valueOf(strCurrentPFSID).intValue());
							pfs.setProcessedStatus(strStatus);
							pfs.setPfsContent(strPFSContent);
							pfs.setVersion(Long.parseLong(strVersion));

							ModuleServiceLocator.getReservationBD().savePfsEntry(pfs);
							// to set the success message to client
							setIntSuccess(request, 0);

							saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS),
									WebConstants.MSG_SUCCESS);
							setExceptionOccured(request, false);
							log.debug("XAPNLProcessingRequestHandler.processPSFData() method is successfully executed.");
						} else if (flightSegments == null || flightSegments.isEmpty()) {
							// Error no flight found
							setIntSuccess(request, -4);
							setErrorFormData(request, strPFSData, strCurrentPFS, strVersion, strGridRowNo, strDownLoadTimeStamp,
									strFlightNo, strFlightDate, strFromAirPort, strFromAddress, strStatus, strCurrentPFSID,
									strMode, strHdnUIMode, strDownLoadTime, strFlightTime, strPFSContent, strPaxCount);
							saveMessage(request, airadminConfig.getMessage("um.process.form.pfs.no.flight"),
									WebConstants.MSG_ERROR);

						} else {
							// One or More flights found
							String flightTimes = "";
							int i = 0;
							for (Iterator<FlightSegement> iterFlightSegs = flightSegments.iterator(); iterFlightSegs.hasNext();) {
								if (!flightTimes.equals("") && i < flightSegments.size() - 1) {
									flightTimes += ", ";
								} else if (!flightTimes.equals("") && i == flightSegments.size() - 1) {
									flightTimes += " and ";
								}
								FlightSegement flightSegement = (FlightSegement) iterFlightSegs.next();
								flightTimes += outputDateFormatHHmm.format(flightSegement.getEstTimeDepatureLocal());
								i++;
							}
							setIntSuccess(request, -6);
							if (strFlightTime.equals("")) {
								saveMessage(request, airadminConfig.getMessage("um.process.form.pfs.ambiguous.flight") + " "
										+ flightTimes, WebConstants.MSG_ERROR);
							} else {
								saveMessage(request, airadminConfig.getMessage("um.pfsprocess.form.pfs.select.flight") + " "
										+ flightTimes, WebConstants.MSG_ERROR);
							}
							setErrorFormData(request, strPFSData, strCurrentPFS, strVersion, strGridRowNo, strDownLoadTimeStamp,
									strFlightNo, strFlightDate, strFromAirPort, strFromAddress, strStatus, strCurrentPFSID,
									strMode, strHdnUIMode, strDownLoadTime, strFlightTime, strPFSContent, strPaxCount);
						}
					} else {
						pfs.setDateDownloaded(downLoadTimeStamp);
						pfs.setDepartureDate(flightDate);
						pfs.setFlightNumber(strFlightNo.equals("") ? null : strFlightNo.toUpperCase());
						pfs.setFromAddress(strFromAddress.equals("") ? null : strFromAddress);
						pfs.setFromAirport(strFromAirPort.equals("-1") ? null : strFromAirPort);
						pfs.setPpId(Integer.valueOf(strCurrentPFSID).intValue());
						pfs.setPfsContent(strPFSContent);
						pfs.setProcessedStatus(strFlightTime.equals("") ? ReservationInternalConstants.PfsStatus.UN_PARSED_WITH_ERRORS
								: ReservationInternalConstants.PfsStatus.UN_PARSED);
						pfs.setVersion(Long.parseLong(strVersion));

						ModuleServiceLocator.getReservationBD().savePfsEntry(pfs);
						// to set the success message to client
						setIntSuccess(request, 0);
						saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS), WebConstants.MSG_SUCCESS);
						setExceptionOccured(request, false);
						log.debug("XAPNLProcessingRequestHandler.processPSFData() method is successfully executed.");
					}
				} else {
					// Invalid flight Number
					setIntSuccess(request, -4);
					setErrorFormData(request, strPFSData, strCurrentPFS, strVersion, strGridRowNo, strDownLoadTimeStamp,
							strFlightNo, strFlightDate, strFromAirPort, strFromAddress, strStatus, strCurrentPFSID, strMode,
							strHdnUIMode, strDownLoadTime, strFlightTime, strPFSContent, strPaxCount);
					saveMessage(request, airadminConfig.getMessage("um.pfsprocess.form.pfs.flightnumber.invalid"),
							WebConstants.MSG_ERROR);
				}
			} else if (!strHdnMode.equals("") && strHdnMode.equals("DELETE")) {
				// Delete pressed
				setIntSuccess(request, 2);
				Pfs pfs = new Pfs();
				pfs.setDateDownloaded(downLoadTimeStamp);
				pfs.setDepartureDate(flightDate);
				pfs.setFlightNumber(strFlightNo.equals("") ? null : strFlightNo.toUpperCase());
				pfs.setFromAddress(strFromAddress.equals("") ? null : strFromAddress);
				pfs.setFromAirport(strFromAirPort.equals("-1") ? null : strFromAirPort);
				pfs.setPpId(Integer.valueOf(strCurrentPFSID).intValue());
				pfs.setProcessedStatus(strStatus);
				pfs.setPfsContent(strPFSContent);
				pfs.setVersion(Long.parseLong(strVersion));

				ModuleServiceLocator.getReservationBD().deletePfsEntry(pfs);
			}
			if (!strHdnMode.equals("") && strHdnMode.equals("PROCESS")) {
				// Reconcile reservation
				ServiceResponce serviceResponce = null;
				serviceResponce = ModuleServiceLocator.getXApnlBD().processXAPnlReconcilation();
				saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_PFS_PROCESS_SUCCESS), WebConstants.MSG_SUCCESS);

				if (serviceResponce != null && serviceResponce.isSuccess()) {
					setIntSuccess(request, 1);
				} else {
					setIntSuccess(request, 3);
				}
				saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_PFS_PROCESS_SUCCESS), WebConstants.MSG_SUCCESS);
				setExceptionOccured(request, false);
				log.debug("XAPNLProcessingRequestHandler.processPSFData() method is successfully executed.");
			}
		} catch (ModuleException moduleException) {
			log.error("XAPNLProcessingRequestHandler.processPSFData() method is failed :" + moduleException.getMessageString(),
					moduleException);
			setIntSuccess(request, -1);
			setErrorFormData(request, strPFSData, strCurrentPFS, strVersion, strGridRowNo, strDownLoadTimeStamp, strFlightNo,
					strFlightDate, strFromAirPort, strFromAddress, strStatus, strCurrentPFSID, strMode, strHdnUIMode,
					strDownLoadTime, strFlightTime, strPFSContent, strPaxCount);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);

		} catch (Exception exception) {
			setIntSuccess(request, -1);
			if (exception instanceof RuntimeException) {
				throw exception;
			}
		}
		return true;
	}

	/**
	 * Sets the Error Form Data & Sets the Sucess int 0-Not Applicable, 1-SendNewPNLSuccess, 2-SendNewADLSuccess
	 * 3-ResendPNLSuccess, 4-ResendADLSuccess 5-PrintSuccess, 6-Fail
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @param strPFSData
	 *            the PFS Data
	 * @param strCurrentPFS
	 *            the Current PFS
	 * @param strVersion
	 *            the Version
	 * @param strGridRowNo
	 *            the Grid Row
	 * @param strDownLoadTimeStamp
	 *            the Download Time Stamp
	 * @param strFlightNo
	 *            the Flight No
	 * @param strFlightDate
	 *            the Depature Date
	 * @param strFromAirPort
	 *            the Origin
	 * @param strFromAddress
	 *            the From Address
	 * @param strStatus
	 *            the Status
	 * @param strCurrentPFSID
	 *            the Current PFS Id
	 * @param strMode
	 *            the Mode
	 * @param strHdnUIMode
	 *            the UI Mode
	 * @param strDownLoadTime
	 *            the Download Time
	 * @param strFlightTime
	 *            the Flight Time
	 * @param strPFSContent
	 *            the PFS Content
	 * @param strPaxCount
	 *            the Pax Count
	 */
	private static void setErrorFormData(HttpServletRequest request, String strPFSData, String strCurrentPFS, String strVersion,
			String strGridRowNo, String strDownLoadTimeStamp, String strFlightNo, String strFlightDate, String strFromAirPort,
			String strFromAddress, String strStatus, String strCurrentPFSID, String strMode, String strHdnUIMode,
			String strDownLoadTime, String strFlightTime, String strPFSContent, String strPaxCount) {

		setExceptionOccured(request, true);
		String strFormData = "var arrFormData = new Array();";
		strFormData += " var saveSuccess = " + getIntSuccess(request) + ";";

		strFormData += "arrFormData[0] = '" + strPFSData + "';";
		strFormData += "arrFormData[1] = '" + strCurrentPFS + "';";
		strFormData += "arrFormData[2] = '" + strVersion + "';";
		strFormData += "arrFormData[3] = '" + strGridRowNo + "';";
		strFormData += "arrFormData[4] = '" + strDownLoadTimeStamp + "';";
		strFormData += "arrFormData[5] = '" + strFlightNo + "';";
		strFormData += "arrFormData[6] = '" + strFlightDate + "';";
		strFormData += "arrFormData[7] = '" + strFromAirPort + "';";
		strFormData += "arrFormData[8] = '" + strFromAddress + "';";
		strFormData += "arrFormData[9] = '" + strStatus + "';";
		strFormData += "arrFormData[10] = '" + strCurrentPFSID + "';";
		strFormData += "arrFormData[11] = '" + strMode + "';";
		strFormData += "arrFormData[12] = '" + strHdnUIMode + "';";
		strFormData += "arrFormData[13] = '" + strDownLoadTime + "';";
		strFormData += "arrFormData[14] = '" + strFlightTime + "';";
		strFormData += "arrFormData[15] = '" + strPFSContent + "';";
		strFormData += "arrFormData[16] = '" + strPaxCount + "';";

		request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
	}

	/**
	 * Sets the Display Data for PFS Process Data sets the Success int for the form 0-Not Applicable, 1-PFS process
	 * Success, 2-PFS save Success 3-Fail
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	private static void setDisplayData(HttpServletRequest request) throws Exception {
		setPFSProcessingRowHtml(request);
		setAgentList(request);
		setAirportList(request);
		setClientErrors(request);
		String strFormData = "";
		if (!isExceptionOccured(request)) {
			strFormData = " var arrFormData = new Array();";
			strFormData += " var saveSuccess = " + getIntSuccess(request) + ";";
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
		}
	}

	/**
	 * Sets the Active Online Airport List
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setAirportList(HttpServletRequest request) {

		try {
			String strHtml = SelectListGenerator.createActiveAirportCodeList();
			request.setAttribute(WebConstants.REQ_HTML_AIRPORT_LIST, strHtml);
		} catch (ModuleException moduleException) {
			log.error("SendPNLADLRequestHandler.setAirportList() method is failed :" + moduleException.getMessageString(),
					moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Agents List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setAgentList(HttpServletRequest request) {

		try {
			String strHtml = SelectListGenerator.createAgentCodes();
			request.setAttribute(WebConstants.REQ_HTML_AGENT_LIST, strHtml);
		} catch (ModuleException moduleException) {
			log.error("XAPNLProcessingRequestHandler.setAgentList() method is failed :" + moduleException.getMessageString(),
					moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Client Validation for PFS To the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setClientErrors(HttpServletRequest request) {

		try {
			String strClientErrors = XAPNLProcessingHTMLGenerator.getClientErrors(request);
			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("XAPNLProcessingRequestHandler.setClientErrors() method is failed :" + moduleException.getMessageString(),
					moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Set the PFS Details Row to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	protected static void setPFSProcessingRowHtml(HttpServletRequest request) throws Exception {

		try {
			XAPNLProcessingHTMLGenerator htmlGen = new XAPNLProcessingHTMLGenerator();
			String strHtml = htmlGen.getPFSProcessingRowHtml(request);
			request.setAttribute(WebConstants.REQ_HTML_ROWS, strHtml);
			request.setAttribute(WebConstants.REQ_FORM_FIELD_VALUES, htmlGen.getFormFieldValues());

		} catch (ModuleException moduleException) {
			log.error("PFSProcessingHandler.setPFSProcessingRowHtml() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}
}
