package com.isa.thinair.airadmin.core.web.v2.action.mis;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.v2.dto.mis.MISProcessParams;
import com.isa.thinair.airadmin.core.web.v2.handler.mis.MISEmailRH;
import com.isa.thinair.airadmin.core.web.v2.util.CommonUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Result(name = AdminStrutsConstants.AdminAction.SUCCESS, type = JSONResult.class, value = "")
public class ShowMISReportsAction extends BaseRequestResponseAwareAction {
	private static Log log = LogFactory.getLog(ShowMISReportsAction.class);
	private boolean success = true;
	private String messageTxt;

	public String execute() throws Exception {
		try {
			MISProcessParams mpParams = new MISProcessParams(request);
			request.setAttribute("req_InitialParams", CommonUtil.convertToJSON(mpParams));
			MISEmailRH.setReportView(request.getParameter("hdnFromDate"), request.getParameter("hdnToDate"), response);
		} catch (ModuleException e) {
			log.error("Generic exception caught.", e);
			success = false;
			messageTxt = e.getMessage();
		}

		return null;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

}
