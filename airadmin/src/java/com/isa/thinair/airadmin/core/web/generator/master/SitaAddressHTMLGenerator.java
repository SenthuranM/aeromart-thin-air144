/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */

/**
 * @author srikantha
 *
 * 
 * Window - Preferences - Java - Code Style - Code Templates
 */

package com.isa.thinair.airadmin.core.web.generator.master;

import java.util.Collection;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airmaster.api.criteria.SitaSearchCriteria;
import com.isa.thinair.airmaster.api.model.SITAAddress;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

public class SitaAddressHTMLGenerator {

	private static Log log = LogFactory.getLog(SitaAddressHTMLGenerator.class);

	private static String clientErrors;

	// searching parameters
	private static final String PARAM_SEARCH_AIRPORTCODE = "selAirport";

	private static final String PARAM_SEARCH_ACTIVESTATUS = "selActiveStatus";

	private static final String PARAM_SEARCH_CARRIER_CODE = "selCarrierCode";

	private static final String PARAM_SEARCH_SITA_ADDRESS = "txtSitaAddress";

	private static final String PARAM_UI_MODE = "hdnUIMode";

	private static final String PARAM_RECORD_NUMBER = "hdnRecNo";

	//private static GlobalConfig globalConfig = CommonsServices.getGlobalConfig();

	private String strFormFieldsVariablesJS = "";

	// private static String strUIModeJS = "var isSearchMode = false;";

	@SuppressWarnings("unchecked")
	public final String getSitaAddressRowHtml(HttpServletRequest request) throws ModuleException {

		String strUIModeJS = "var isSearchMode = false;";

		log.debug("inside SitaAddressHTMLGenerator.getSitaAddressRowHtml()");
		String strUIMode = request.getParameter(PARAM_UI_MODE);

		String strAirportCode = "";
		String strActiveStatus = "";
		String strCarrierCode = "";
		String strSitaAddress = "";
		List<SITAAddress> list = null;

		int recordNo = 0;
		if (request.getParameter(PARAM_RECORD_NUMBER) != null && !request.getParameter(PARAM_RECORD_NUMBER).equals(""))
			recordNo = Integer.parseInt(request.getParameter(PARAM_RECORD_NUMBER)) - 1;
		Page page = null;

		int totalRecords = 0;
		String strJavascriptTotalNoOfRecs = "";

		// if (strMode != null && strMode.equals(WebConstants.ACTION_SEARCH)) {
		if (strUIMode != null && strUIMode.equals(WebConstants.ACTION_SEARCH)) {

			strUIModeJS = "var isSearchMode = true;";

			SitaSearchCriteria criteria = new SitaSearchCriteria();

			strAirportCode = request.getParameter(PARAM_SEARCH_AIRPORTCODE);
			strFormFieldsVariablesJS = "var airportCode ='" + strAirportCode + "';";

			strCarrierCode = request.getParameter(PARAM_SEARCH_CARRIER_CODE);
			strFormFieldsVariablesJS += "var carrierCode ='" + strCarrierCode + "';";

			strActiveStatus = request.getParameter(PARAM_SEARCH_ACTIVESTATUS);
			strFormFieldsVariablesJS += "var activeStatus ='" + strActiveStatus + "';";

			strSitaAddress = request.getParameter(PARAM_SEARCH_SITA_ADDRESS);
			strFormFieldsVariablesJS += "var sitaAddress ='" + strSitaAddress.trim() + "';";

			if (strAirportCode != null && !strAirportCode.equals("-1")) {
				criteria.setAirportId(strAirportCode);
			}

			if (strActiveStatus != null && !strActiveStatus.equals("-1")) {
				if (strActiveStatus.equals(SITAAddress.ACTIVE_STATUS_YES))
					criteria.setActiveStatus(SITAAddress.ACTIVE_STATUS_YES);
				if (strActiveStatus.equals(SITAAddress.ACTIVE_STATUS_NO))
					criteria.setActiveStatus(SITAAddress.ACTIVE_STATUS_NO);
			}

			if (strCarrierCode != null && !strCarrierCode.equals("-1")) {
				criteria.setCarrierCode(strCarrierCode);
			}

			if (strSitaAddress != null && !strSitaAddress.equals("")) {
				criteria.setSitaAddress(strSitaAddress.trim());
			}

			page = ModuleServiceLocator.getAirportServiceBD().getSITAAddresses(criteria, recordNo, 20);
			totalRecords = page.getTotalNoOfRecords();
			strJavascriptTotalNoOfRecs = "var totalRecords = " + totalRecords + ";";
			request.setAttribute(WebConstants.REQ_TOTAL_RECORDS, strJavascriptTotalNoOfRecs);
			request.setAttribute(WebConstants.REQ_OPERATION_UI_MODE, strUIModeJS);

			list = (List<SITAAddress>) page.getPageData();

		} else {

			strUIModeJS = "var isSearchMode = false;";

			strFormFieldsVariablesJS = "var airportCode ='';";
			strFormFieldsVariablesJS += "var carrierCode ='';";
			strFormFieldsVariablesJS += "var activeStatus ='';";
			strFormFieldsVariablesJS += "var sitaAddress ='';";

			page = ModuleServiceLocator.getAirportServiceBD().getSITAAddresses(new SitaSearchCriteria(), recordNo, 20);
			totalRecords = page.getTotalNoOfRecords();
			strJavascriptTotalNoOfRecs = "var totalRecords = " + totalRecords + ";";
			request.setAttribute(WebConstants.REQ_TOTAL_RECORDS, strJavascriptTotalNoOfRecs);
			request.setAttribute(WebConstants.REQ_OPERATION_UI_MODE, strUIModeJS);

			list = (List<SITAAddress>) page.getPageData();
			log.debug("inside SitaAddressHTMLGenerator.getSitaAddressRowHtml SEARCH else condition()");
		}

		setFormFieldValues(strFormFieldsVariablesJS);
		return createSitaAddressRowHTML(list);
	}

	private String createSitaAddressRowHTML(Collection<SITAAddress> sitaAddresses) {

		List<SITAAddress> list = (List<SITAAddress>) sitaAddresses;

		Object[] listArr = list.toArray();
		StringBuffer sb = new StringBuffer();
		// String isPnlAdlConfigEmail =
		// globalConfig.getBizParam(SystemParamKeys.PNL_ADL_CONFIGURE_TO_EMAIL).equalsIgnoreCase("Y") ? "Y" : "N";

		String strMyArr = "var arrData = new Array();";
		// strMyArr += "var isPnlAdlConfigEmail = '"+ isPnlAdlConfigEmail + "';";
		SITAAddress sitaAddress = null;

		for (int i = 0; i < listArr.length; i++) {
			sitaAddress = (SITAAddress) listArr[i];

			strMyArr += "arrData[" + i + "] = new Array();";
			strMyArr += "arrData[" + i + "][1] = '" + sitaAddress.getAirportSITAId() + "';";
			if (sitaAddress.getSitaAddress() != null) {
				strMyArr += "arrData[" + i + "][2] = '" + sitaAddress.getSitaAddress() + "';";
			} else {
				strMyArr += "arrData[" + i + "][2] = '';";
			}

			if (sitaAddress.getCarriercodes() != null) {
				strMyArr += "arrData[" + i + "][3] = '"
						+ sitaAddress.getCarriercodes().toString().replace("[", "").replace("]", "").replace(" ", "") + "';";
			} else {
				strMyArr += "arrData[" + i + "][3] = '';";
			}

			strMyArr += "arrData[" + i + "][4] = '" + sitaAddress.getAirportCode() + "';";
			if (sitaAddress.getAirportCode() != null) {
				strMyArr += "arrData[" + i + "][4] = '" + sitaAddress.getAirportCode() + "';";
			} else {
				strMyArr += "arrData[" + i + "][4] = '';";
			}

			if (sitaAddress.getSitaLocation() != null) {
				strMyArr += "arrData[" + i + "][5] = '" + sitaAddress.getSitaLocation() + "';";
			} else {
				strMyArr += "arrData[" + i + "][5] = '';";
			}

			if (sitaAddress.getActiveStatus() != null) {
				if (sitaAddress.getActiveStatus().equals(SITAAddress.ACTIVE_STATUS_YES))
					strMyArr += "arrData[" + i + "][6] = 'Active';";
				if (sitaAddress.getActiveStatus().equals(SITAAddress.ACTIVE_STATUS_NO))
					strMyArr += "arrData[" + i + "][6] = 'In-Active';";
			} else {
				strMyArr += "arrData[" + i + "][6] = '';";
			}

			strMyArr += "arrData[" + i + "][7] = '" + sitaAddress.getVersion() + "';";

			if (sitaAddress.getEmailId() != null) {
				strMyArr += "arrData[" + i + "][8] = '" + sitaAddress.getEmailId() + "';";
			} else {
				strMyArr += "arrData[" + i + "][8] = '';";
			}
			if (sitaAddress.getSitaDeliveryMethod() != null) {
				strMyArr += "arrData[" + i + "][9] = '" + sitaAddress.getSitaDeliveryMethod() + "';";
			} else {
				strMyArr += "arrData[" + i + "][9] = '';";
			}

			strMyArr += "arrData[" + i + "][10] = "+ sitaAddress.isAllowForPNLADL() +";";
			strMyArr += "arrData[" + i + "][11] = "+ sitaAddress.isAllowForPALCAL() +";";
			
			if (sitaAddress.getOndCodes() != null) {
				strMyArr += "arrData[" + i + "][12] = '"
						+ sitaAddress.getOndCodes().toString().replace("[", "").replace("]", "").replace(" ", "") + "';";
			} else {
				strMyArr += "arrData[" + i + "][12] = '';";
			}
			
			if (sitaAddress.getFlightNumbers() != null) {
				strMyArr += "arrData[" + i + "][13] = '"
						+ sitaAddress.getFlightNumbers().toString().replace("[", "").replace("]", "").replace(" ", "") + "';";
			} else {
				strMyArr += "arrData[" + i + "][13] = '';";
			}

		}
		sb.append(strMyArr);
		return sb.toString();
	}

	public static String getClientErrors(HttpServletRequest request) throws ModuleException {

		if (clientErrors == null) {
			Properties moduleErrs = new Properties();

			moduleErrs.setProperty("um.sitaaddress.form.id.required", "sitaIdRqrd");
			moduleErrs.setProperty("um.sitaaddress.form.airport.required", "sitaAirportRqrd");
			moduleErrs.setProperty("um.sitaaddress.form.location.required", "sitaLocationRqrd");
			moduleErrs.setProperty("um.sitaaddress.form.carriercode.required", "sitaCarrierCodeRqrd");
			moduleErrs.setProperty("um.sitaaddress.form.address.required", "sitaAddressOrEmailRqrd");
			moduleErrs.setProperty("um.sitaaddress.form.delivery.method.required", "deliveryMethodRqrd");
			moduleErrs.setProperty("um.sitaaddress.form.add.required", "sitaAddressRqrd");
			// moduleErrs.setProperty("um.sitaaddress.form.type.required",
			// "sitaTypeRqrd");
			moduleErrs.setProperty("um.sitaaddress.form.activestatus.required", "sitaActiveStatusRqrd");
			moduleErrs.setProperty("um.airadmin.delete.confirmation", "deleteConf");
			moduleErrs.setProperty("um.sitaaddress.form.email.invalid", "invalidEmail");
			moduleErrs.setProperty("um.sitaaddress.form.email.only.one", "onlyOneEmail");
			moduleErrs.setProperty("um.sitaaddress.from.pnladl.or.palcal.requires", "pnladlOrpalcalRqrd");
			
			moduleErrs.setProperty("um.searchOptimizeInventory.form.depatureRequired.required", "depatureRequired");
			moduleErrs.setProperty("um.searchOptimizeInventory.form.arrivalRequired.required", "arrivalRequired");
			moduleErrs.setProperty("um.searchOptimizeInventory.form.depatureArriavlSame.Same", "depatureArriavlSame");
			moduleErrs.setProperty("um.searchOptimizeInventory.form.arriavlViaSame.Same", "arriavlViaSame");
			moduleErrs.setProperty("um.searchOptimizeInventory.form.depatureViaSame.Same", "depatureViaSame");
			moduleErrs.setProperty("um.searchOptimizeInventory.form.depatureShouldSame.Same", "depatureShouldSame");

			clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}

		return clientErrors;
	}

	private void setFormFieldValues(String strFormFieldsVariablesJS) {

		this.strFormFieldsVariablesJS = strFormFieldsVariablesJS;
	}

	public String getFormFieldValues() {

		return strFormFieldsVariablesJS;
	}
}
