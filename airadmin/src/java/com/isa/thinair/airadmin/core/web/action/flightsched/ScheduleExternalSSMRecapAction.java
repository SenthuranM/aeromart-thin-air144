package com.isa.thinair.airadmin.core.web.action.flightsched;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.quartz.ObjectAlreadyExistsException;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airmaster.api.model.SSMRecap;
import com.isa.thinair.auditor.api.model.Audit;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.TasksUtil;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Results(@Result(name = AdminStrutsConstants.AdminAction.SUCCESS, type = JSONResult.class, value = ""))
public class ScheduleExternalSSMRecapAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(ScheduleExternalSSMRecapAction.class);
	public static final String SSM_RECAP = "SSM_RECAP";

	private String scheduledDate;
	private String scheduledTime;
	private String selectedScheduleRange;
	private String scheduleStartDate;
	private String scheduleEndDate;
	private String emailAddress;
	private String sitaAddress;
	private boolean success = true;
	private String messageTxt;
	private boolean sitaSelect;
	private boolean emailSelect;

	public String execute() {

		try {
			SSMRecap ssmRecap = getValidatedSSMRecap();
			scheduleExternalSSMRecap(ssmRecap);
			return AdminStrutsConstants.AdminAction.SUCCESS;
		} catch (ObjectAlreadyExistsException e) {
			messageTxt = "Job Already Scheduled";
			success = false;
		} catch (Exception e) {
			if (e.getMessage() != null && e.getMessage() != "") {
				messageTxt = e.getMessage();
			} else {
				messageTxt = "Error Occured while scheduling";
			}
			e.printStackTrace();
			success = false;
		}

		return S2Constants.Result.SUCCESS;
	}

	private void scheduleExternalSSMRecap(SSMRecap ssmRecap) throws Exception {
		String scheduleTime = formatSQLDate(ssmRecap.getScheduledTime());
		String jobID = "SSMRECAP/" + ssmRecap.getEmailAddress() + ssmRecap.getSitaAddress() + "/" + scheduleTime;
		Map<String, String> jobDataMap = new HashMap<String, String>();
		jobDataMap.put("JOB_NAME", jobID);
		jobDataMap.put("JOB_GROUP_NAME", SSM_RECAP);
		if (ssmRecap.isSelectedRange()) {
			jobDataMap.put("PROP_GDS_IS_SELECTED_SCHED", "Y");
		} else {
			jobDataMap.put("PROP_GDS_IS_SELECTED_SCHED", "N");
		}
		if (ssmRecap.getScheduleStartDate() == null) {
			ssmRecap.setScheduleStartDate(new Date());
		}
		if (ssmRecap.getScheduleEndDate() == null) {
			GregorianCalendar currentDate = new GregorianCalendar();
			// Schedules for next 3 years
			currentDate.set(GregorianCalendar.YEAR, currentDate.get(GregorianCalendar.YEAR) + 3);
			ssmRecap.setScheduleEndDate(currentDate.getTime());
		}
		jobDataMap.put("PROP_GDS_SSMRECAP_SCHED_START_DATE", formatSQLDate(ssmRecap.getScheduleStartDate()));
		jobDataMap.put("PROP_GDS_SSMRECAP_SCHED_END_DATE", formatSQLDate(ssmRecap.getScheduleEndDate()));
		jobDataMap.put("PROP_EXTERNAL_SSMRECAP_EMAIL", ssmRecap.getEmailAddress());
		jobDataMap.put("PROP_EXTERNAL_SSMRECAP_SITA", ssmRecap.getSitaAddress());
		jobDataMap.put("PROP_EXTERNAL_SSMRECAP_SENDING", "Y");
		jobDataMap.put("CLASS_NAME", "com.isa.thinair.scheduledservices.core.client.ssmasm.SendSSMRecapJob");
		jobDataMap.put("SCHEDULED_TIME", formatSQLDate(ssmRecap.getScheduledTime()));

		log.info("#########################################################################################");
		log.info("Scheduling new Job for JobID  " + jobID + " Scheduled For :" + scheduleTime);
		log.info("#########################################################################################");
		ModuleServiceLocator.getScheduleServiceBD().scheduleSSMRecap(jobDataMap);
		log.info("Finished saving new Job for JobID  " + jobID + " Scheduled For :" + scheduleTime);
		createAuditForSchedulingRecap(ssmRecap.getEmailAddress(), ssmRecap.getSitaAddress(),
				formatSQLDate(ssmRecap.getScheduledTime()));

	}

	private SSMRecap getValidatedSSMRecap() throws Exception {

		SSMRecap ssmRecap = new SSMRecap();
		if (scheduledDate != null && scheduledDate != "" && scheduledTime != null && scheduledTime != "") {
			Date scheduleDate = composeScheduledDateTime(scheduledDate, scheduledTime);
			Date currentDate = new Date();
			if (scheduleDate.before(currentDate)) {
				throw new Exception("Scheduled date and time cannot come before current date and time");
			}
			ssmRecap.setScheduledTime(scheduleDate);
		} else {
			throw new Exception("Invalid Scheduled Date");
		}

		if (selectedScheduleRange != null && selectedScheduleRange != "") {
			if (selectedScheduleRange.equals("selected")) {
				ssmRecap.setSelectedRange(true);
				if (scheduleStartDate != null && scheduleStartDate != "") {
					ssmRecap.setScheduleStartDate(parseToDate(scheduleStartDate));

				} else {
					throw new Exception("Invalid Flight Schedule Start Date");
				}

				if (scheduleEndDate != null && scheduleEndDate != "") {
					ssmRecap.setScheduleEndDate(parseToDate(scheduleEndDate));

				} else {
					throw new Exception("Invalid Flight Schedule End Date");
				}

			} else if (selectedScheduleRange.equals("all")) {
				ssmRecap.setSelectedRange(true);
			}

		} else {
			throw new Exception("Invalid Flight Schedule Range");
		}

		if (emailSelect) {
			if (emailAddress != null && emailAddress != "") {
				ssmRecap.setEmailAddress(emailAddress);
			} else {
				throw new Exception("Invalid Email Address");
			}
		} else {
			ssmRecap.setEmailAddress("");
		}

		if (sitaSelect) {
			if (sitaAddress != null && sitaAddress != "") {
				ssmRecap.setSitaAddress(sitaAddress);
			} else {
				throw new Exception("Invalid SITA Address");
			}
		} else {
			ssmRecap.setSitaAddress("");
		}
		return ssmRecap;

	}

	private Date composeScheduledDateTime(String strDate, String strTime) throws Exception {
		Date date = null;
		Date time = null;
		SimpleDateFormat dateFormatDate = null;
		SimpleDateFormat dateFormatTime = new SimpleDateFormat("HH:mm");

		if (strDate.indexOf('-') != -1)
			dateFormatDate = new SimpleDateFormat("dd-MM-yy");
		if (strDate.indexOf('/') != -1)
			dateFormatDate = new SimpleDateFormat("dd/MM/yy");
		try {
			date = dateFormatDate.parse(strDate);
			time = dateFormatTime.parse(strTime);
		} catch (Exception e) {
			log.error(e);
			throw new Exception("Invalid Scheduled Date");
		}
		return getTimeAddedDate(date, time);
	}

	private Date parseToDate(String strDate) throws Exception {
		Date date = null;
		SimpleDateFormat dateFormat = null;

		if (strDate.indexOf('-') != -1)
			dateFormat = new SimpleDateFormat("dd-MM-yy");
		if (strDate.indexOf('/') != -1)
			dateFormat = new SimpleDateFormat("dd/MM/yy");
		try {
			date = dateFormat.parse(strDate);
		} catch (Exception e) {
			log.error(e);
			throw new Exception("Invalid Scheduled Date");
		}

		return date;
	}

	private String formatSQLDate(Date date) {
		String sqlDate = null;
		if (date != null) {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			sqlDate = dateFormat.format(date);
		}
		return sqlDate;
	}

	private Date getTimeAddedDate(Date date, Date time) {

		GregorianCalendar timeCalender = new GregorianCalendar();
		timeCalender.setTime(time);

		GregorianCalendar dateCalender = new GregorianCalendar();
		dateCalender.setTime(date);

		dateCalender.set(GregorianCalendar.HOUR_OF_DAY, timeCalender.get(GregorianCalendar.HOUR_OF_DAY));
		dateCalender.set(GregorianCalendar.MINUTE, timeCalender.get(GregorianCalendar.MINUTE));
		dateCalender.set(GregorianCalendar.SECOND, timeCalender.get(GregorianCalendar.SECOND));

		return dateCalender.getTime();
	}

	private void createAuditForSchedulingRecap(String email, String sita, String time) throws ModuleException {
		String content = "sita:=" + sita + "||email:=" + email + "||scheduled time:=" + time;
		UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
		Audit scheduledRecapAudit = new Audit(TasksUtil.MASTER_SCHEDULE_SSM_RECAP_EXTERNAL, new Date(), "airadmin",
				userPrincipal.getUserId(), content);
		ModuleServiceLocator.getAuditorServiceBD().audit(scheduledRecapAudit, null);
	}

	public String getSitaAddress() {
		return sitaAddress;
	}

	public void setSitaAddress(String sitaAddress) {
		this.sitaAddress = sitaAddress;
	}

	public String getScheduledDate() {
		return scheduledDate;
	}

	public void setScheduledDate(String scheduledDate) {
		this.scheduledDate = scheduledDate;
	}

	public String getScheduledTime() {
		return scheduledTime;
	}

	public void setScheduledTime(String scheduledTime) {
		this.scheduledTime = scheduledTime;
	}

	public String getSelectedScheduleRange() {
		return selectedScheduleRange;
	}

	public void setSelectedScheduleRange(String selectedScheduleRange) {
		this.selectedScheduleRange = selectedScheduleRange;
	}

	public String getScheduleStartDate() {
		return scheduleStartDate;
	}

	public void setScheduleStartDate(String scheduleStartDate) {
		this.scheduleStartDate = scheduleStartDate;
	}

	public String getScheduleEndDate() {
		return scheduleEndDate;
	}

	public void setScheduleEndDate(String scheduleEndDate) {
		this.scheduleEndDate = scheduleEndDate;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public boolean isSitaSelect() {
		return sitaSelect;
	}

	public void setSitaSelect(boolean sitaSelect) {
		this.sitaSelect = sitaSelect;
	}

	public boolean isEmailSelect() {
		return emailSelect;
	}

	public void setEmailSelect(boolean emailSelect) {
		this.emailSelect = emailSelect;
	}

}
