package com.isa.thinair.airadmin.core.web.v2.handler.master;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airmaster.api.dto.SSRTemplateSearchCriteria;
import com.isa.thinair.airmaster.api.model.SSRTemplate;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * 
 * @author rumesh
 * 
 */

public class SSRTemplateRH {
	public static Page<SSRTemplate> searchSSRTemplate(int pageNo, SSRTemplateSearchCriteria criteria) throws ModuleException {
		return ModuleServiceLocator.getSsrServiceBD().getSSRTemplates(criteria, (pageNo - 1) * 20, 20);
	}

	public static void saveTemplate(SSRTemplate ssrTemplate) throws ModuleException {
		ModuleServiceLocator.getSsrServiceBD().saveTemplate(ssrTemplate);
	}

	public static void deleteTemplate(int templateId) throws ModuleException {
		ModuleServiceLocator.getSsrServiceBD().deleteTemplate(templateId);
	}
	
}
