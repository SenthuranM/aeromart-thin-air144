package com.isa.thinair.airadmin.core.web.handler.master;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.master.CityPairHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airmaster.api.model.Airport;
import com.isa.thinair.airmaster.api.model.RouteInfo;
import com.isa.thinair.airmaster.api.model.RouteTransitsInfo;
import com.isa.thinair.commons.api.exception.MessagesUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author Thushara Modified By : Dhanusha
 * @version 1.1
 */
public class CityPairRequestHandler extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(CityPairRequestHandler.class);

	public static final String PARAM_VERSION = "hdnVersion";
	public static final String PARAM_ORIGIN = "selOrigin";
	public static final String PARAM_DESTINATION = "selDestination";
	public static final String PARAM_VIA_1 = "selVia1";
	public static final String PARAM_VIA_2 = "selVia2";
	public static final String PARAM_VIA_3 = "selVia3";
	public static final String PARAM_VIA_4 = "selVia4";
	public static final String PARAM_DISTANCE = "txtDistance";
	public static final String PARAM_DURATION = "txtDuration";
	public static final String PARAM_TOLERANCE = "txtTolerance";
	public static final String PARAM_MODE = "hdnMode";
	public static final String PARAM_ACTION = "hdnAction";
	public static final String PARAM_STATUS = "chkStatus";
	public static final String PARAM_ROUTE_CODE = "hdnROuteCode";
	private static final String PARAM_GRIDROW = "hdnGridRow";
	private static final String PARAM_ROUTE_ID = "hdnRouteId";
	private static final String PARAM_HDN_ID = "hdnId";
	private static final String PARAM_HDN_VIA1_ID = "hdnVia1Id";
	private static final String PARAM_HDN_VIA2_ID = "hdnVia2Id";
	private static final String PARAM_HDN_VIA3_ID = "hdnVia3Id";
	private static final String PARAM_HDN_VIA4_ID = "hdnVia4Id";
	public static final String PARAM_LCC_CON_STATUS = "chkLCCPublishedStatus";

	private static AiradminConfig airadminConfig = new AiradminConfig();

	private static void setIntSuccess(HttpServletRequest request, int value) {
		setAttribInRequest(request, "intSuccess", new Integer(value));
	}

	private static int getIntSuccess(HttpServletRequest request) {
		Integer returnValue = (Integer) getAttribInRequest(request, "intSuccess");
		if (returnValue != null)
			return returnValue.intValue();
		else
			return -1;
	}

	private static void setExceptionOccured(HttpServletRequest request, boolean value) {
		setAttribInRequest(request, "isExceptionOccured", new Boolean(value));
	}

	private static boolean isExceptionOccured(HttpServletRequest request) {
		return ((Boolean) getAttribInRequest(request, "isExceptionOccured")).booleanValue();
	}

	/**
	 * Main Execute Method for Route Details Action & set the Success int 0-Not Applicable, 1-Success, 2-Fail
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {
		String strHdnMode = request.getParameter(PARAM_MODE);
		String strHdnAction = request.getParameter(PARAM_ACTION);
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;
		String strVersion = request.getParameter(PARAM_VERSION);
		String strRouteId = null;
		String strGridRow = null;
		strRouteId = request.getParameter(PARAM_ROUTE_ID);
		strGridRow = AiradminUtils.getNotNullString(request.getParameter(PARAM_GRIDROW));

		setIntSuccess(request, 0);
		setExceptionOccured(request, false);
		setAttribInRequest(request, "strModeJS", "var isAddMode = false;");
		setAttribInRequest(request, "strOriginFocusJS", "var isOriginFocus = false;");
		setAttribInRequest(request, "strDestFocusJS", "var isDestFocus = false;");
		/**
		 * LCC Connectiviy status related displayData
		 * 
		 * @author Navod Ediriweera
		 */

		String strFormData = "var saveSuccess = " + getIntSuccess(request) + ";";
		strFormData += " var arrFormData = new Array();";
		if (checkLCCConnectivityStatus()) {
			strFormData += "var blnLCCConnectivity = true;";
		} else {
			strFormData += "var blnLCCConnectivity = false;";
		}
		request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);

		try {
			if (strHdnMode != null && (strHdnMode.equals(WebConstants.ACTION_SAVE)) && strHdnAction != null) {

				if (strHdnAction != null && strHdnAction.equals(WebConstants.ACTION_ADD)
						|| strHdnAction.equals(WebConstants.ACTION_EDIT)) {

					int validity = checkValidAirport(request.getParameter(PARAM_ROUTE_CODE),
							AiradminUtils.getNotNullString(request.getParameter(PARAM_ORIGIN)),
							AiradminUtils.getNotNullString(request.getParameter(PARAM_DESTINATION)));

					if (validity == 2) {
						setAttribInRequest(request, "strOriginFocusJS", "var isOriginFocus = true;");
						setExceptionOccured(request, true);
						saveMessage(request, airadminConfig.getMessage("um.user.form.origin.inactive"), WebConstants.MSG_ERROR);
						log.debug("CityPairRequestHandler.saveData() method failed due to invalid origin.");

					} else {
						setAttribInRequest(request, "strOriginFocusJS", "var isOriginFocus = false;");
						setExceptionOccured(request, false);

						if (validity == 1) {
							setAttribInRequest(request, "strDestFocusJS", "var isDestFocus = true;");
							setExceptionOccured(request, true);
							saveMessage(request, airadminConfig.getMessage("um.user.form.destination.inactive"),
									WebConstants.MSG_ERROR);
							log.debug("CityPairRequestHandler.saveData() " + "method failed due to invalid destination.");

						} else {
							setAttribInRequest(request, "strDestFocusJS", "var isDestFocus = false;");
							setExceptionOccured(request, false);
						}
					}

					if (isExceptionOccured(request)) {
						strFormData = createErrorForm(request);
						setIntSuccess(request, 0);
						strFormData += " var saveSuccess = " + getIntSuccess(request) + "; ";
						if (strHdnAction != null && strHdnAction.equals(WebConstants.ACTION_EDIT)) {
							strFormData += " var prevVersion = " + strVersion + ";";
							strFormData += " var prevRouteId = '" + strRouteId + "';";
							strFormData += " var prevGridRow = " + strGridRow + ";";
						}
						request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
					}

					request.setAttribute(WebConstants.REQ_ORIGIN_FOCUS, getAttribInRequest(request, "strOriginFocusJS"));
					request.setAttribute(WebConstants.REQ_DESTINATION_FOCUS, getAttribInRequest(request, "strDestFocusJS"));
				}

				if (strHdnAction.equals(WebConstants.ACTION_EDIT)) {

					setAttribInRequest(request, "strModeJS", "var isAddMode = false;");
					checkPrivilege(request, WebConstants.PRIV_SYS_MAS_ROUTE_EDIT);
					if (!isExceptionOccured(request)) {
						saveData(getProperties(request));

						setIntSuccess(request, 1);
						strFormData += " var saveSuccess = " + getIntSuccess(request) + ";";
						request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
						saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_UPDATE_SUCCESS), WebConstants.MSG_SUCCESS);
					}
				} else if (strHdnAction.equals(WebConstants.ACTION_ADD)) {

					setAttribInRequest(request, "strModeJS", "var isAddMode = true;");
					checkPrivilege(request, WebConstants.PRIV_SYS_MAS_ROUTE_ADD);
					if (!isExceptionOccured(request)) {
						saveData(getProperties(request));

						setIntSuccess(request, 1);
						strFormData += " var saveSuccess = " + getIntSuccess(request) + ";";
						request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
						saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS), WebConstants.MSG_SUCCESS);
					}
				}
			}
		} catch (ModuleException moduleException) {
			log.error("Exception in CityPairRequestHandler:execute() [origin module=" + moduleException.getModuleDesc() + "]",
					moduleException);

			setExceptionOccured(request, true);
			strFormData = createErrorForm(request);
			setIntSuccess(request, 2);
			strFormData += " var saveSuccess = " + getIntSuccess(request) + "; ";
			if (strHdnAction != null && strHdnAction.equals(WebConstants.ACTION_EDIT)) {
				strFormData += " var prevVersion = " + strVersion + ";";
				strFormData += " var prevRouteId = '" + strRouteId + "';";
				strFormData += " var prevGridRow = " + strGridRow + ";";
			}
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);

			if (moduleException.getMessageString().equals(MessagesUtil.getMessage("module.duplicate.key"))) {
				saveMessage(request, airadminConfig.getMessage("um.citypair.form.id.dupkey"), WebConstants.MSG_ERROR);
			} else {
				saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
			}
		} catch (Exception e) {

			log.error("Exception in CityPairRequestHandler:execute()", e);
			setExceptionOccured(request, true);

			if (e instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, e.getMessage(), "", "");

			} else {
				strFormData = createErrorForm(request);
				setIntSuccess(request, 2);
				strFormData += " var saveSuccess = " + getIntSuccess(request) + ";";
				request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
				saveMessage(request, e.getMessage(), WebConstants.MSG_ERROR);
			}
		}

		try {
			if (strHdnMode != null && (strHdnMode.equals(WebConstants.ACTION_DELETE))) {

				checkPrivilege(request, WebConstants.PRIV_SYS_MAS_ROUTE_DELETE);
				String routeCode = request.getParameter(PARAM_ROUTE_CODE);
				RouteInfo existingRouteInfo = ModuleServiceLocator.getCommonServiceBD().getRouteInfo(routeCode);
				if (existingRouteInfo == null)
					ModuleServiceLocator.getCommonServiceBD().removeRouteInfo(routeCode);
				else {
					strVersion = request.getParameter(PARAM_VERSION);
					if (strVersion != null && !"".equals(strVersion)) {
						existingRouteInfo.setVersion(Long.parseLong(strVersion));
						ModuleServiceLocator.getCommonServiceBD().removeRouteInfo(existingRouteInfo);
					} else {
						ModuleServiceLocator.getCommonServiceBD().removeRouteInfo(routeCode);
					}
				}
				saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_DELETE_SUCCESS), WebConstants.MSG_SUCCESS);
			}

		} catch (ModuleException moduleException) {
			log.error("Exception in CityPairRequestHandler:execute() [origin module=" + moduleException.getModuleDesc() + "]",
					moduleException);
			if (moduleException.getMessageString().equals(MessagesUtil.getMessage("module.constraint.childrecord"))) {
				saveMessage(request, airadminConfig.getMessage("um.airadmin.childrecord"), WebConstants.MSG_ERROR);
			} else {
				saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
			}

		}

		try {
			setDisplayData(request);
			strFormData += " var saveSuccess = " + getIntSuccess(request) + ";";
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
			request.setAttribute(WebConstants.REQ_OPERATION_MODE, getAttribInRequest(request, "strModeJS"));

		} catch (ModuleException e) {
			log.error("Exception in CityPairRequestHandler:execute() [origin module=" + e.getModuleDesc() + "]", e);
			setIntSuccess(request, 2);
			strFormData = " var saveSuccess = " + getIntSuccess(request) + ";";
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
		}
		return forward;
	}

	/**
	 * Check the Valid Airport
	 * 
	 * @param routeInfoId
	 *            the route Info ID
	 * @param originAirportId
	 *            the Origin Airport Code
	 * @param destinationAirportId
	 *            Destination Airport Code
	 * @return int the Success 0- Success 2- The changed Origin Airport inactive 1- The changed Destination Airport
	 *         inactive
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static int checkValidAirport(String routeInfoId, String originAirportId, String destinationAirportId)
			throws ModuleException {

		RouteInfo routeInfo = null;
		if (routeInfoId != null && !routeInfoId.equals("")) {
			routeInfo = ModuleServiceLocator.getCommonServiceBD().getRouteInfo(routeInfoId);
		}
		Airport destinationAirport = ModuleServiceLocator.getAirportServiceBD().getAirport(destinationAirportId);
		if (destinationAirport == null)
			return 1;
		Airport originAirport = ModuleServiceLocator.getAirportServiceBD().getAirport(originAirportId);
		if (originAirport == null)
			return 2;

		if (destinationAirport.getStatus().equals(Airport.STATUS_INACTIVE)) {
			if (routeInfo == null)
				return 1;
			else {
				Airport destAirportofRouteInfo = ModuleServiceLocator.getAirportServiceBD().getAirport(
						routeInfo.getToAirportCode());
				if (destAirportofRouteInfo == null)
					return 1;
				if (!destAirportofRouteInfo.getAirportCode().equals(destinationAirport.getAirportCode()))
					return 1;
			}
		}

		if (originAirport.getStatus().equals(Airport.STATUS_INACTIVE)) {
			if (routeInfo == null)
				return 2;
			else {
				Airport originAirportofRouteInfo = ModuleServiceLocator.getAirportServiceBD().getAirport(
						routeInfo.getFromAirportCode());
				if (originAirportofRouteInfo == null)
					return 2;
				if (!originAirportofRouteInfo.getAirportCode().equals(originAirport.getAirportCode()))
					return 2;
			}
		}
		return 0;
	}

	/**
	 * Sets the Display Data to the ROUTE INFO page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static void setDisplayData(HttpServletRequest request) throws ModuleException {
		setClientErrors(request);
		setAirportHtml(request);
		setCityPairRowHtml(request);
		setDirectRouteUsageRowHtml(request);
		setRoutesInUseRowHtml(request);
	}

	/**
	 * Sets the Client Validations for the ROUTE Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static void setClientErrors(HttpServletRequest request) throws ModuleException {
		String strClientErrors = CityPairHTMLGenerator.getClientErrors(request);
		if (strClientErrors == null) {
			strClientErrors = "";
		}
		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
	}

	/**
	 * Sets the Route Detail List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static void setCityPairRowHtml(HttpServletRequest request) throws ModuleException {

		CityPairHTMLGenerator cityPairHTMLGenerator = new CityPairHTMLGenerator();
		String strHtml = cityPairHTMLGenerator.getCityPairRowHtml(request);
		request.setAttribute(WebConstants.REQ_HTML_CITY_PAIR_DATA, strHtml);
	}

	/**
	 * Sets the Online Active Airport List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 * 
	 *             Modified By : Dhanusha
	 */
	public static void setAirportHtml(HttpServletRequest request) throws ModuleException {
		String strViaPointsList = SelectListGenerator.createActiveAirportCodeList();
		request.setAttribute(WebConstants.REQ_HTML_VIAPOINT_SELECT_LIST, strViaPointsList);
	}

	/**
	 * Sets the Route ID List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static void setDirectRouteUsageRowHtml(HttpServletRequest request) throws ModuleException {
		String arrDirectRoutUsage = SelectListGenerator.createDirectRouteUsageArray();
		request.setAttribute(WebConstants.REQ_HTML_DIRECT_ROUT_USAGE_DATA, arrDirectRoutUsage);
	}

	/**
	 * Sets the Route ID List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static void setRoutesInUseRowHtml(HttpServletRequest request) throws ModuleException {
		String arrRoutesInUse = SelectListGenerator.createRoutesInUseArray();
		request.setAttribute(WebConstants.REQ_HTML_ROUTES_IN_USE_DATA, arrRoutesInUse);
	}

	/**
	 * Saves the Data Using the Property file Data
	 * 
	 * @param props
	 *            the Property File containing the ROUTE Data
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void saveData(Properties props) throws ModuleException {
		RouteInfo modelRouteInfo = null;
		String strVersion = props.getProperty(PARAM_VERSION);
		String strHdnMode = props.getProperty(PARAM_MODE);
		String strHdnId = props.getProperty(PARAM_HDN_ID);
		String strHdnVia1Id = props.getProperty(PARAM_HDN_VIA1_ID);
		String strHdnVia2Id = props.getProperty(PARAM_HDN_VIA2_ID);
		String strHdnVia3Id = props.getProperty(PARAM_HDN_VIA3_ID);
		String strHdnVia4Id = props.getProperty(PARAM_HDN_VIA4_ID);

		int totDuration = 0;
		int durationHrs = 0;
		int durationMins = 0;

		if (strHdnMode.equals("SAVE")) {
			modelRouteInfo = new RouteInfo();
			if (props.getProperty(PARAM_DURATION) != null && !("".equals(props.getProperty(PARAM_DURATION)))) {
				String strArray[] = props.getProperty(PARAM_DURATION).split(":");
				durationHrs = Integer.parseInt(strArray[0]);
				durationMins = Integer.parseInt(strArray[1]);
			}
			totDuration = (durationHrs * 60) + durationMins;
			modelRouteInfo.setDistance(new BigDecimal(props.getProperty(PARAM_DISTANCE)));
			modelRouteInfo.setDuration(totDuration);
			if (props.getProperty(PARAM_TOLERANCE) != null && !("".equals(props.getProperty(PARAM_TOLERANCE)))) {
				modelRouteInfo.setDurationTolerance(new BigDecimal(props.getProperty(PARAM_TOLERANCE)));
			} else {
				modelRouteInfo.setDurationTolerance(new BigDecimal(0));
			}

			modelRouteInfo.setFromAirportCode(props.getProperty(PARAM_ORIGIN).toUpperCase().trim());
			modelRouteInfo.setToAirportCode(props.getProperty(PARAM_DESTINATION).toUpperCase().trim());

			// Sets the version
			if (strVersion != null && !"".equals(strVersion)) {
				modelRouteInfo.setVersion(Long.parseLong(strVersion));
			}

			// Sets the ID
			if (strHdnId != null && !"".equals(strHdnId)) {
				modelRouteInfo.setRouteId(Long.parseLong(strHdnId));
			}

			String strActiveStatus = props.getProperty(PARAM_STATUS) == null ? "" : props.getProperty(PARAM_STATUS);
			if (strActiveStatus != null && strActiveStatus.equals("Active")) {
				modelRouteInfo.setStatus(RouteInfo.STATUS_ACTIVE);
			} else {
				modelRouteInfo.setStatus(RouteInfo.STATUS_INACTIVE);
			}

			// Sets the OND Code
			String stSegmentFrom = props.getProperty(PARAM_ORIGIN).toUpperCase().trim() + "/";
			String stSegmentTo = props.getProperty(PARAM_DESTINATION).toUpperCase().trim();
			String stSegmentVia1 = ((props.getProperty(PARAM_VIA_1) != null && !("".equals(props.getProperty(PARAM_VIA_1))))) ? props
					.getProperty(PARAM_VIA_1).toUpperCase().trim()
					+ "/"
					: "";
			String stSegmentVia2 = ((props.getProperty(PARAM_VIA_2) != null && !("".equals(props.getProperty(PARAM_VIA_2))))) ? props
					.getProperty(PARAM_VIA_2).toUpperCase().trim()
					+ "/"
					: "";
			String stSegmentVia3 = ((props.getProperty(PARAM_VIA_3) != null && !("".equals(props.getProperty(PARAM_VIA_3))))) ? props
					.getProperty(PARAM_VIA_3).toUpperCase().trim()
					+ "/"
					: "";
			String stSegmentVia4 = ((props.getProperty(PARAM_VIA_4) != null && !("".equals(props.getProperty(PARAM_VIA_4))))) ? props
					.getProperty(PARAM_VIA_4).toUpperCase().trim()
					+ "/"
					: "";

			String stSegmentCode = stSegmentFrom + stSegmentVia1 + stSegmentVia2 + stSegmentVia3 + stSegmentVia4 + stSegmentTo;
			modelRouteInfo.setRouteCode(stSegmentCode.trim());

			// Sets the Via points
			List<RouteTransitsInfo> routeTransitsInfoList = new ArrayList<RouteTransitsInfo>();
			int transitsCnt = 0;
			RouteTransitsInfo routeTransitsInfo;
			Collection<String> viaPoints = new HashSet<String>();
			if (props.getProperty(PARAM_VIA_1) != null && !("".equals(props.getProperty(PARAM_VIA_1)))) {
				routeTransitsInfo = new RouteTransitsInfo(props.getProperty(PARAM_VIA_1), 1);
				routeTransitsInfo.setRouteInfo(modelRouteInfo);

				// Sets the Id
				if (strHdnVia1Id != null && !"".equals(strHdnVia1Id)) {
					routeTransitsInfo.setRouteTransitId(Long.parseLong(strHdnVia1Id));
				}
				viaPoints.add(props.getProperty(PARAM_VIA_1));
				routeTransitsInfoList.add(routeTransitsInfo);
				transitsCnt++;
			}

			if (props.getProperty(PARAM_VIA_2) != null && !("".equals(props.getProperty(PARAM_VIA_2)))) {
				routeTransitsInfo = new RouteTransitsInfo(props.getProperty(PARAM_VIA_2), 2);
				routeTransitsInfo.setRouteInfo(modelRouteInfo);

				// Sets the Id
				if (strHdnVia2Id != null && !"".equals(strHdnVia2Id)) {
					routeTransitsInfo.setRouteTransitId(Long.parseLong(strHdnVia2Id));
				}
				viaPoints.add(props.getProperty(PARAM_VIA_2));
				routeTransitsInfoList.add(routeTransitsInfo);
				transitsCnt++;
			}

			if (props.getProperty(PARAM_VIA_3) != null && !("".equals(props.getProperty(PARAM_VIA_3)))) {
				routeTransitsInfo = new RouteTransitsInfo(props.getProperty(PARAM_VIA_3), 3);
				routeTransitsInfo.setRouteInfo(modelRouteInfo);

				// Sets the Id
				if (strHdnVia3Id != null && !"".equals(strHdnVia3Id)) {
					routeTransitsInfo.setRouteTransitId(Long.parseLong(strHdnVia3Id));
				}
				viaPoints.add(props.getProperty(PARAM_VIA_3));
				routeTransitsInfoList.add(routeTransitsInfo);
				transitsCnt++;
			}

			if (props.getProperty(PARAM_VIA_4) != null && !("".equals(props.getProperty(PARAM_VIA_4)))) {
				routeTransitsInfo = new RouteTransitsInfo(props.getProperty(PARAM_VIA_4), 4);
				routeTransitsInfo.setRouteInfo(modelRouteInfo);

				// Sets the Id
				if (strHdnVia4Id != null && !"".equals(strHdnVia4Id)) {
					routeTransitsInfo.setRouteTransitId(Long.parseLong(strHdnVia4Id));
				}
				viaPoints.add(props.getProperty(PARAM_VIA_4));
				routeTransitsInfoList.add(routeTransitsInfo);
				transitsCnt++;
			}

			// Sets the direct flag
			if (props.getProperty(PARAM_VIA_1) == null || "".equals(props.getProperty(PARAM_VIA_1))) {
				modelRouteInfo.setDirectFlg(RouteInfo.DIRECT_TRUE);
			} else {
				Collection<String> hubs = new HashSet<String>();
				if (!viaPoints.isEmpty()) {
					hubs = ModuleServiceLocator.getLocationServiceBD().getValidHubCodes(viaPoints);
				}

				if (!hubs.isEmpty()) {
					modelRouteInfo.setDirectFlg(RouteInfo.DIRECT_FALSE);
					log.debug("Has Hub");
				} else {
					modelRouteInfo.setDirectFlg(RouteInfo.DIRECT_TRUE);
				}
			}

			// Sets the No of Transits
			modelRouteInfo.setNoOfTransits(transitsCnt);

			// Sets the Route Transits Info child data to the parent Route Info
			modelRouteInfo.setRouteTransits(routeTransitsInfoList);

			// Setting up LCC Published Status
			modelRouteInfo.setIsLCCPubilshed(RouteInfo.LCC_PUBLISHED_FALSE);

			ModuleServiceLocator.getCommonServiceBD().saveRouteInfo(modelRouteInfo);
		}

	}

	/**
	 * Get a Property file Containg ROUTE Data
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return Properties the Proerty file containing ROUTE DATA
	 */
	protected static Properties getProperties(HttpServletRequest request) {
		Properties props = new Properties();

		String strVersion = AiradminUtils.getNotNullString(request.getParameter(PARAM_VERSION));
		String strOrigin = AiradminUtils.getNotNullString(request.getParameter(PARAM_ORIGIN));
		String strDestination = AiradminUtils.getNotNullString(request.getParameter(PARAM_DESTINATION));
		String strVia1 = AiradminUtils.getNotNullString(request.getParameter(PARAM_VIA_1));
		String strVia2 = AiradminUtils.getNotNullString(request.getParameter(PARAM_VIA_2));
		String strVia3 = AiradminUtils.getNotNullString(request.getParameter(PARAM_VIA_3));
		String strVia4 = AiradminUtils.getNotNullString(request.getParameter(PARAM_VIA_4));
		String strDistance = AiradminUtils.getNotNullString(request.getParameter(PARAM_DISTANCE));
		String strDuration = AiradminUtils.getNotNullString(request.getParameter(PARAM_DURATION));
		String strTolerance = AiradminUtils.getNotNullString(request.getParameter(PARAM_TOLERANCE));
		String strMode = AiradminUtils.getNotNullString(request.getParameter(PARAM_MODE));
		String strStatus = AiradminUtils.getNotNullString(request.getParameter(PARAM_STATUS));
		String strAction = AiradminUtils.getNotNullString(request.getParameter(PARAM_ACTION));
		String strHdnId = AiradminUtils.getNotNullString(request.getParameter(PARAM_HDN_ID));
		String strHdnVia1Id = AiradminUtils.getNotNullString(request.getParameter(PARAM_HDN_VIA1_ID));
		String strHdnVia2Id = AiradminUtils.getNotNullString(request.getParameter(PARAM_HDN_VIA2_ID));
		String strHdnVia3Id = AiradminUtils.getNotNullString(request.getParameter(PARAM_HDN_VIA3_ID));
		String strHdnVia4Id = AiradminUtils.getNotNullString(request.getParameter(PARAM_HDN_VIA4_ID));

		props.setProperty(PARAM_VERSION, strVersion);
		props.setProperty(PARAM_ORIGIN, strOrigin);
		props.setProperty(PARAM_DESTINATION, strDestination);
		props.setProperty(PARAM_VIA_1, strVia1);
		props.setProperty(PARAM_VIA_2, strVia2);
		props.setProperty(PARAM_VIA_3, strVia3);
		props.setProperty(PARAM_VIA_4, strVia4);
		props.setProperty(PARAM_DISTANCE, strDistance);
		props.setProperty(PARAM_DURATION, strDuration);
		props.setProperty(PARAM_TOLERANCE, strTolerance);
		props.setProperty(PARAM_MODE, strMode);
		props.setProperty(PARAM_ACTION, strAction);
		props.setProperty(PARAM_STATUS, strStatus);
		props.setProperty(PARAM_HDN_ID, strHdnId);
		props.setProperty(PARAM_HDN_VIA1_ID, strHdnVia1Id);
		props.setProperty(PARAM_HDN_VIA2_ID, strHdnVia2Id);
		props.setProperty(PARAM_HDN_VIA3_ID, strHdnVia3Id);
		props.setProperty(PARAM_HDN_VIA4_ID, strHdnVia4Id);

		return props;
	}

	/**
	 * Creates the Forms Data from the request
	 * 
	 * @param req
	 *            the HttpServletRequest
	 * @return String the String array containing forms filed data
	 */
	private static String createErrorForm(HttpServletRequest req) {
		StringBuffer erSb = new StringBuffer("var arrFormData = new Array();");
		Properties erp = getProperties(req);
		erSb.append("arrFormData[0] = new Array();");
		erSb.append("arrFormData[0][1] = '" + erp.getProperty(PARAM_ORIGIN) + "';");
		erSb.append("arrFormData[0][2] = '" + erp.getProperty(PARAM_DESTINATION) + "';");
		erSb.append("arrFormData[0][3] = '" + erp.getProperty(PARAM_DISTANCE) + "';");
		erSb.append("arrFormData[0][4] = '" + erp.getProperty(PARAM_DURATION) + "';");
		erSb.append("arrFormData[0][5] = '" + erp.getProperty(PARAM_TOLERANCE) + "';");
		erSb.append("arrFormData[0][6] = '" + erp.getProperty(PARAM_STATUS) + "';");
		erSb.append("arrFormData[0][7] = '" + erp.getProperty(PARAM_VIA_1) + "';");
		erSb.append("arrFormData[0][8] = '" + erp.getProperty(PARAM_VIA_2) + "';");
		erSb.append("arrFormData[0][9] = '" + erp.getProperty(PARAM_VIA_3) + "';");
		erSb.append("arrFormData[0][10] = '" + erp.getProperty(PARAM_VIA_4) + "';");
		return erSb.toString();
	}

	private static boolean checkLCCConnectivityStatus() {
		return AppSysParamsUtil.isLCCConnectivityEnabled();
	}
}
