package com.isa.thinair.airadmin.core.web.v2.action.mis;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.v2.dto.mis.MISProcessParams;
import com.isa.thinair.airadmin.core.web.v2.util.CommonUtil;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.reporting.api.criteria.MISReportsSearchCriteria;
import com.isa.thinair.reporting.api.model.MISAccountSummaryTO;
import com.isa.thinair.reporting.api.model.MISOutstandingInvoicesTO;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Result(name = AdminStrutsConstants.AdminAction.SUCCESS, type = JSONResult.class, value = "")
public class ShowMISAccountsAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(ShowMISAccountsAction.class);
	private boolean success = true;
	private String messageTxt;
	private MISReportsSearchCriteria misSearchCriteria;
	private Map<String, Map<String, String>> accPaymentsNRefundsSummary;
	private String highestValue;
	private Collection<MISOutstandingInvoicesTO> outStandingInvoices;
	private String totalRefunds;
	private String totalSales;
	private String totalOutstandingCredits;
	private Map<String, String> outStandingByRegion;
	private String outStandingByRegionTotal;

	public String execute() throws Exception {
		String strForward = WebConstants.FORWARD_SUCCESS;
		MISProcessParams mpParams = new MISProcessParams(request);
		request.setAttribute("req_InitialParams", CommonUtil.convertToJSON(mpParams));
		try {
			misSearchCriteria = new MISReportsSearchCriteria();
			misSearchCriteria.setFromDate(request.getParameter("fromDate"));
			misSearchCriteria.setToDate(request.getParameter("toDate"));
			if (!"ALL".equalsIgnoreCase(request.getParameter("region"))) {
				misSearchCriteria.setRegion(request.getParameter("region"));
			}
			if (!"ALL".equalsIgnoreCase(request.getParameter("pos"))) {
				misSearchCriteria.setPos(request.getParameter("pos"));
			}
			misSearchCriteria.setRefundNominalCodes(getRefundNominalCodes());
			misSearchCriteria.setPaymentNominalCodes(getPaymentNominalCodes());

			accPaymentsNRefundsSummary = getAccountsPaymentsNRefundsSummaryData();
			outStandingInvoices = getMISOutStandingInvoiceData();
			outStandingByRegion = getOutstandingByRegion();
			totalOutstandingCredits = getTotalOutstandingCreditAmount();

		} catch (Exception e) {
			log.error("Generic exception caught.", e);
			success = false;
			messageTxt = e.getMessage();
		}
		return strForward;
	}

	private String getRefundNominalCodes() {
		String nominalCodes = Util.buildIntegerInClauseContent(ReservationTnxNominalCode.getRefundTypeNominalCodes());
		return nominalCodes;
	}

	private String getPaymentNominalCodes() {
		String nominalCodes = Util.buildIntegerInClauseContent(ReservationTnxNominalCode.getPaymentTypeNominalCodes());
		return nominalCodes;
	}

	private Map<Integer, String> getCreditCardNominalCodes() {
		List<Integer> creditCardNominalCodes = ReservationTnxNominalCode.getCreditCardNominalCodes();
		Map<Integer, String> creditCardNominalCodesMap = new HashMap<Integer, String>();
		for (Integer nominalCode : creditCardNominalCodes) {
			creditCardNominalCodesMap.put(nominalCode, nominalCode.toString());
		}

		return creditCardNominalCodesMap;
	}

	private Map<Integer, String> getCashNominalCodes() {
		List<Integer> nominalCodesList = ReservationTnxNominalCode.getCashTypeNominalCodes();
		Map<Integer, String> nominalCodesMap = new HashMap<Integer, String>();
		for (Integer nominalCode : nominalCodesList) {
			nominalCodesMap.put(nominalCode, nominalCode.toString());
		}

		return nominalCodesMap;
	}

	private Map<Integer, String> getCreditTypeNominalCodes() {
		List<Integer> nominalCodesList = ReservationTnxNominalCode.getCreditTypeNominalCodes();
		Map<Integer, String> nominalCodesMap = new HashMap<Integer, String>();
		for (Integer nominalCode : nominalCodesList) {
			nominalCodesMap.put(nominalCode, nominalCode.toString());
		}

		return nominalCodesMap;
	}

	private Map<Integer, String> getOnAccountNominalCodes() {
		List<Integer> nominalCodesList = ReservationTnxNominalCode.getOnAccountTypeNominalCodes();
		Map<Integer, String> nominalCodesMap = new HashMap<Integer, String>();
		for (Integer nominalCode : nominalCodesList) {
			nominalCodesMap.put(nominalCode, nominalCode.toString());
		}

		return nominalCodesMap;
	}

	/**
	 * Prepares refund and revenue data for all main categories.
	 * 
	 * @return
	 */
	private Map<String, Map<String, String>> getAccountsPaymentsNRefundsSummaryData() {
		Collection<MISAccountSummaryTO> refundsData = ModuleServiceLocator.getMISDataProviderBD().getMISAccountRefunds(
				misSearchCriteria);
		Collection<MISAccountSummaryTO> salesData = ModuleServiceLocator.getMISDataProviderBD().getMISAccountSales(
				misSearchCriteria);

		Map<Integer, String> creditCardNominalCodesMap = getCreditCardNominalCodes();
		Map<Integer, String> cashNominalCodesMap = getCashNominalCodes();
		Map<Integer, String> creditTypeNominalCodesMap = getCreditTypeNominalCodes();
		Map<Integer, String> onAccountNominalCodesMap = getOnAccountNominalCodes();

		BigDecimal totalCreditCardAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalCashAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalCreditTypeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		BigDecimal totalOnAccountAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		Map<String, String> accountsSummary = new HashMap<String, String>();
		Map<String, Map<String, String>> totalSalesSummaryData = new HashMap<String, Map<String, String>>();

		// Process Refunds and group it
		for (MISAccountSummaryTO misAccountSummaryTO : refundsData) {
			if (isCreditCardPamentOrRefund(creditCardNominalCodesMap, misAccountSummaryTO.getNominalCode())) {
				totalCreditCardAmount = AccelAeroCalculator.add(totalCreditCardAmount, misAccountSummaryTO.getAmount());
			} else if (isCreditTypePamentOrRefund(creditTypeNominalCodesMap, misAccountSummaryTO.getNominalCode())) {
				totalCreditTypeAmount = AccelAeroCalculator.add(totalCreditTypeAmount, misAccountSummaryTO.getAmount());
			} else if (isCashPamentOrRefund(cashNominalCodesMap, misAccountSummaryTO.getNominalCode())) {
				totalCashAmount = AccelAeroCalculator.add(totalCashAmount, misAccountSummaryTO.getAmount());
			} else if (isOnAccountPamentOrRefund(onAccountNominalCodesMap, misAccountSummaryTO.getNominalCode())) {
				totalOnAccountAmount = AccelAeroCalculator.add(totalOnAccountAmount, misAccountSummaryTO.getAmount());
			}
		}
		accountsSummary.put("Cash", AccelAeroCalculator.formatAsDecimal(totalCashAmount));
		accountsSummary.put("Credit Card", AccelAeroCalculator.formatAsDecimal(totalCreditCardAmount));
		accountsSummary.put("Credit", AccelAeroCalculator.formatAsDecimal(totalCreditTypeAmount));
		accountsSummary.put("On Account", AccelAeroCalculator.formatAsDecimal(totalOnAccountAmount));
		totalSalesSummaryData.put(MISAccountSummaryTO.SALES_TYPE_REFUND, accountsSummary);
		BigDecimal totRefund = AccelAeroCalculator.add(totalCashAmount, totalCreditCardAmount, totalCreditTypeAmount,
				totalOnAccountAmount);
		this.totalRefunds = AccelAeroCalculator.formatAsDecimal(totRefund);

		// Process sales data and group it
		accountsSummary = new HashMap<String, String>();
		totalCreditCardAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		totalCashAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		totalCreditTypeAmount = AccelAeroCalculator.getDefaultBigDecimalZero();
		totalOnAccountAmount = AccelAeroCalculator.getDefaultBigDecimalZero();

		for (MISAccountSummaryTO misAccountSummaryTO : salesData) {
			if (isCreditCardPamentOrRefund(creditCardNominalCodesMap, misAccountSummaryTO.getNominalCode())) {
				totalCreditCardAmount = AccelAeroCalculator.add(totalCreditCardAmount, misAccountSummaryTO.getAmount());
			} else if (isCreditTypePamentOrRefund(creditTypeNominalCodesMap, misAccountSummaryTO.getNominalCode())) {
				totalCreditTypeAmount = AccelAeroCalculator.add(totalCreditTypeAmount, misAccountSummaryTO.getAmount());
			} else if (isCashPamentOrRefund(cashNominalCodesMap, misAccountSummaryTO.getNominalCode())) {
				totalCashAmount = AccelAeroCalculator.add(totalCashAmount, misAccountSummaryTO.getAmount());
			} else if (isOnAccountPamentOrRefund(onAccountNominalCodesMap, misAccountSummaryTO.getNominalCode())) {
				totalOnAccountAmount = AccelAeroCalculator.add(totalOnAccountAmount, misAccountSummaryTO.getAmount());
			}
		}
		accountsSummary.put("Cash", AccelAeroCalculator.formatAsDecimal(totalCashAmount));
		accountsSummary.put("Credit Card", AccelAeroCalculator.formatAsDecimal(totalCreditCardAmount));
		accountsSummary.put("Credit", AccelAeroCalculator.formatAsDecimal(totalCreditTypeAmount));
		accountsSummary.put("On Account", AccelAeroCalculator.formatAsDecimal(totalOnAccountAmount));
		totalSalesSummaryData.put(MISAccountSummaryTO.SALES_TYPE_REVENUE, accountsSummary);
		BigDecimal totSales = AccelAeroCalculator.add(totalCashAmount, totalCreditCardAmount, totalCreditTypeAmount,
				totalOnAccountAmount);
		this.totalSales = AccelAeroCalculator.formatAsDecimal(totSales);

		// Set the highest value from revenue
		// this.highestValue = getHighestValue(accountsSummary);
		return totalSalesSummaryData;

	}

	/**
	 * Returns the highest value from the given map
	 * 
	 * @param summaryData
	 * @return
	 */
	private String getHighestValue(Map<String, BigDecimal> summaryData) {
		BigDecimal highVal = AccelAeroCalculator.getDefaultBigDecimalZero();
		for (String type : summaryData.keySet()) {
			if (summaryData.get(type).compareTo(highVal) > 0) {
				highVal = summaryData.get(type);
			}
		}
		// highVal = AccelAeroCalculator.add(highVal, AccelAeroCalculator.divide(highVal, 10));
		return highVal.toPlainString();
	}

	/**
	 * Returns total outstanding invoice amounts due for each region
	 * 
	 * @return
	 */
	private Map<String, String> getOutstandingByRegion() {

		Map<String, BigDecimal> outstandingByRegionMap = ModuleServiceLocator.getMISDataProviderBD().getMISOutstandingByRegion(
				misSearchCriteria);
		Map<String, String> regionalOutstandingAmount = new HashMap<String, String>();
		BigDecimal outstanadingTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
		for (String region : outstandingByRegionMap.keySet()) {
			outstanadingTotal = AccelAeroCalculator.add(outstanadingTotal, outstandingByRegionMap.get(region));
			regionalOutstandingAmount.put(region, AccelAeroCalculator.formatAsDecimal(outstandingByRegionMap.get(region)));
		}
		this.outStandingByRegionTotal = outstanadingTotal.toPlainString();

		return regionalOutstandingAmount;

	}

	private String getTotalOutstandingCreditAmount() {
		BigDecimal totalOutStandingCredits = ModuleServiceLocator.getMISDataProviderBD().getMISAccountTotalOutstandingCredits(
				misSearchCriteria);
		return totalOutStandingCredits.toPlainString();
	}

	private Collection<MISOutstandingInvoicesTO> getMISOutStandingInvoiceData() {
		return ModuleServiceLocator.getMISDataProviderBD().getMISAccountOutstandingInvoices(misSearchCriteria);
	}

	private boolean isCreditCardPamentOrRefund(Map<Integer, String> creditCardNominalCodesMap, Integer nominalCode) {
		return (creditCardNominalCodesMap.containsKey(nominalCode));
	}

	private boolean isCreditTypePamentOrRefund(Map<Integer, String> creditTypeNominalCodesMap, Integer nominalCode) {
		return (creditTypeNominalCodesMap.containsKey(nominalCode));
	}

	private boolean isCashPamentOrRefund(Map<Integer, String> cashNominalCodesMap, Integer nominalCode) {
		return (cashNominalCodesMap.containsKey(nominalCode));
	}

	private boolean isOnAccountPamentOrRefund(Map<Integer, String> onAccNominalCodesMap, Integer nominalCode) {
		return (onAccNominalCodesMap.containsKey(nominalCode));
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public MISReportsSearchCriteria getMisSearchCriteria() {
		return misSearchCriteria;
	}

	public void setMisSearchCriteria(MISReportsSearchCriteria misSearchCriteria) {
		this.misSearchCriteria = misSearchCriteria;
	}

	public Map<String, Map<String, String>> getAccPaymentsNRefundsSummary() {
		return accPaymentsNRefundsSummary;
	}

	public void setAccPaymentsNRefundsSummary(Map<String, Map<String, String>> accPaymentsNRefundsSummary) {
		this.accPaymentsNRefundsSummary = accPaymentsNRefundsSummary;
	}

	public String getHighestValue() {
		return highestValue;
	}

	public void setHighestValue(String highestValue) {
		this.highestValue = highestValue;
	}

	public Collection<MISOutstandingInvoicesTO> getOutStandingInvoices() {
		return outStandingInvoices;
	}

	public void setOutStandingInvoices(Collection<MISOutstandingInvoicesTO> outStandingInvoices) {
		this.outStandingInvoices = outStandingInvoices;
	}

	public String getTotalRefunds() {
		return totalRefunds;
	}

	public void setTotalRefunds(String totalRefunds) {
		this.totalRefunds = totalRefunds;
	}

	public String getTotalSales() {
		return totalSales;
	}

	public void setTotalSales(String totalSales) {
		this.totalSales = totalSales;
	}

	public Map<String, String> getOutStandingByRegion() {
		return outStandingByRegion;
	}

	public void setOutStandingByRegion(Map<String, String> outStandingByRegion) {
		this.outStandingByRegion = outStandingByRegion;
	}

	public String getOutStandingByRegionTotal() {
		return outStandingByRegionTotal;
	}

	public void setOutStandingByRegionTotal(String outStandingByRegionTotal) {
		this.outStandingByRegionTotal = outStandingByRegionTotal;
	}

	public String getTotalOutstandingCredits() {
		return totalOutstandingCredits;
	}

	public void setTotalOutstandingCredits(String totalOutstandingCredits) {
		this.totalOutstandingCredits = totalOutstandingCredits;
	}

}
