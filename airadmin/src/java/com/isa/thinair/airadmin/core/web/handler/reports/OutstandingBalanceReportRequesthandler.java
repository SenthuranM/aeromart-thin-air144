/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airadmin.core.web.handler.reports;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author Chamindap
 * 
 */
public class OutstandingBalanceReportRequesthandler extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(OutstandingBalanceReportRequesthandler.class);

	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	/**
	 * The constructor.
	 */
	public OutstandingBalanceReportRequesthandler() {
		super();
	}

	/**
	 * @param request
	 * @return
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {

		String strHdnMode = request.getParameter("hdnMode");
		String forward = WebConstants.ACTION_FORWARD_SUCCESS;

		try {
			setDisplayData(request);
			log.debug("OutstandingBalanceReportRequesthandler SETDISPLAYDATA() SUCCESS");
		} catch (Exception e) {
			log.error("OutstandingBalanceReportRequesthandler SETDISPLAYDATA() FAILED " + e.getMessage());
		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				log.debug("OutstandingBalanceReportRequesthandler setReportView Success");
				return null;
			} else {
				log.debug("OutstandingBalanceReportRequesthandler setReportView not selected");
			}

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("OutstandingBalanceReportRequesthandler setReportView Failed " + e.getMessageString());
		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_DETAIL)) {
				setDetailReportView(request, response);
				log.debug("OutstandingBalanceReportRequesthandler setDetailReportView Success");
				return null;
			} else {
				log.debug("OutstandingBalanceReportRequesthandler setDetailReportView not selected");
			}
		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("OutstandingBalanceReportRequesthandler setReportView Failed " + e.getMessageString());
		}

		return forward;
	}

	/**
	 * 
	 * @param request
	 */
	public static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());

		}
	}

	/**
	 * 
	 * @param request
	 * @throws Exception
	 */
	public static void setDisplayData(HttpServletRequest request) throws Exception {
		setClientErrors(request);
		setAgencyTypesList(request);
		setLiveStatus(request);
		setReportingPeriod(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setAgencyTypesList(HttpServletRequest request) throws ModuleException {
		String strList = SelectListGenerator.createAgentGSAList();
		request.setAttribute(WebConstants.REQ_HTML_GSA_LIST, strList);
	}

	/**
	 * Sets the report medium to the request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleExceptions
	 */
	private static void setLiveStatus(HttpServletRequest request) throws ModuleException {
		String strLive = request.getParameter(WebConstants.REP_HDN_LIVE);
		if (strLive != null)
			request.setAttribute(WebConstants.REP_SET_LIVE, strLive);

	}

	/**
	 * Setting the reporting period to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setReportingPeriod(HttpServletRequest request) throws ModuleException {
		int period = new Integer(globalConfig.getBizParam(SystemParamKeys.REPORTING_PERIOD)).intValue();
		request.setAttribute(WebConstants.REP_RPT_START_DAY, ReportsHTMLGenerator.getReportingPeriod(period));
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		ResultSet resultSet = null;
		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String value = request.getParameter("radReportOption");
		String debitCredit = request.getParameter("radDebitCredit");
		String id = "UC_REPM_007";
		String strLogo = AppSysParamsUtil.getReportLogo(false);// globalConfig.getBizParam(SystemParamKeys.REPORT_LOGO);
		String image = "../../images/" + AppSysParamsUtil.getReportLogo(true);
		String reportTemplate = null;
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String strBase = globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY);
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);
		Map<String, Object> parameters = new HashMap<String, Object>();

		try {
			ReportsSearchCriteria search = new ReportsSearchCriteria();

			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}

			search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate));
			search.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate));

			if (debitCredit.trim().equals("DebitCredit")) {
				reportTemplate = "OutstandingBalance.jasper";
				search.setReportOption(ReportsSearchCriteria.REPORT_OPTION_CREDIT_AND_DEBIT);
			} else if (debitCredit.trim().equals("Debit")) {
				reportTemplate = "OutstandingBalanceDebit.jasper";
				search.setReportOption(ReportsSearchCriteria.REPORT_OPTION_DEBIT_ONLY);
			} else if (debitCredit.trim().equals("Credit")) {
				reportTemplate = "OutstandingBalanceCredit.jasper";
				search.setReportOption(ReportsSearchCriteria.REPORT_OPTION_CREDIT_ONLY);
			}

			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));

			resultSet = ModuleServiceLocator.getDataExtractionBD().getOutstandingBalanceSummaryData(search);

			parameters.put("FROM_DATE", fromDate);
			parameters.put("TO_DATE", toDate);
			parameters.put("DR_CR", debitCredit);
			parameters.put("ID", id);
			parameters.put("OPTION", value);
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
			parameters.put("AMT_1", "(" + strBase + ")");

			if (value.trim().equals("HTML")) {
				parameters.put("IMG", image);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null, null,
						response);
			} else if (value.trim().equals("PDF")) {
				image = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG", image);
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("EXCEL")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=OutstandingBalanceSummary.xls");
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("CSV")) {
				response.addHeader("Content-Disposition", "attachment;filename=OutstandingBalanceSummary.csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);
			}

		} catch (Exception e) {
			log.error("OutstandingBalanceReportRequesthandler setReportView" + e);
		}
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setDetailReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {
		ResultSet resultSet = null;

		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String value = request.getParameter("radReportOption");
		String debitCredit = request.getParameter("radDebitCredit");
		String agent = request.getParameter("hdnAgentTypeCode");
		String agentName = request.getParameter("selagent");
		String id = " UC_REPM_007";
		String reportTemplate = null;
		String strLogo = AppSysParamsUtil.getReportLogo(false);
		String image = "../../images/" +  AppSysParamsUtil.getReportLogo(true);
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String strBase = globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY);

		try {

			ReportsSearchCriteria search = new ReportsSearchCriteria();
			search.setAgentCode(agentName);
			search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate));
			search.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate));

			if (debitCredit.trim().equals("DebitCredit")) {
				reportTemplate = "OutstandingBalanceDetail.jasper";
				search.setReportOption(ReportsSearchCriteria.REPORT_OPTION_CREDIT_AND_DEBIT);
			} else if (debitCredit.trim().equals("Debit")) {
				reportTemplate = "OutstandingBalanceDebitDetail.jasper";
				search.setReportOption(ReportsSearchCriteria.REPORT_OPTION_DEBIT_ONLY);
			} else if (debitCredit.trim().equals("Credit")) {
				reportTemplate = "OutstandingBalanceCreditDetail.jasper";
				search.setReportOption(ReportsSearchCriteria.REPORT_OPTION_CREDIT_ONLY);
			}

			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));

			resultSet = ModuleServiceLocator.getDataExtractionBD().getOutstandingBalanceDetailData(search);

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("FROM_DATE", fromDate);
			parameters.put("TO_DATE", toDate);
			parameters.put("ID", id);
			parameters.put("AGENTID", agentName);
			parameters.put("NAME", agent);
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
			parameters.put("AMT_1", "(" + strBase + ")");

			// To provide Report Format Options
			String reportNumFormat = request.getParameter("radRptNumFormat");
			ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

			if (value.trim().equals("HTML")) {
				parameters.put("IMG", image);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null, null,
						response);
			} else if (value.trim().equals("PDF")) {
				image = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG", image);
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("EXCEL")) {
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("CSV")) {
				response.addHeader("Content-Disposition", "attachment;filename=OutstandingBalancedetail.csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);
			}
		} catch (Exception e) {
			log.error("OutstandingBalanceReportRequesthandler setReportView" + e);
		}
	}
}