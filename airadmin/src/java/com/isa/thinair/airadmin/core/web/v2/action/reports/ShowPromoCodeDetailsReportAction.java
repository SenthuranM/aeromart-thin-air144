package com.isa.thinair.airadmin.core.web.v2.action.reports;

import java.sql.ResultSet;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONUtil;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airadmin.core.web.v2.util.ReportingUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;

/**
 * 
 * @author rumesh
 * 
 */

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.Reporting.RPTS_PROMO_CODE_DETAILS_JSP)
public class ShowPromoCodeDetailsReportAction extends BaseRequestResponseAwareAction {

	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();
	private static String PROMO_CODE_REPORT_TEMPLATE = "PromoCodeDetailsReport.jasper";

	private String hdnPromoTypes;
	private String departureDateFrm;
	private String departureDateTo;
	private String bookedDateFrm;
	private String bookedDateTo;
	private String hdnAgents;
	private String hdnChannels;
	private String radReportOption;
	private Collection<String> colOnds;
	private Collection<String> colFlightNos;

	public String execute() {
		String forward = S2Constants.Result.SUCCESS;
		try {
			setReportView();
			return null;
		} catch (ModuleException e) {
			forward = S2Constants.Result.ERROR;
		}
		return forward;
	}

	private void setReportView() throws ModuleException {
		ReportsSearchCriteria search = new ReportsSearchCriteria();
		ResultSet resultSet = null;
		String id = "UC_REPM_084";
		String strLogo = AppSysParamsUtil.getReportLogo(false);
		String image = ReportingUtil.ReportingLocations.image_loc + AppSysParamsUtil.getReportLogo(true);
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		Map<String, Object> parameters = new HashMap<String, Object>();
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);

		ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
				ReportsHTMLGenerator.getReportTemplate(PROMO_CODE_REPORT_TEMPLATE));

		search.setDepartureDateRangeFrom(this.departureDateFrm);
		search.setDepartureDateRangeTo(this.departureDateTo);
		if (this.bookedDateFrm != null && !this.bookedDateFrm.isEmpty()) {
			search.setDateRangeFrom(this.bookedDateFrm + " 00:00:00");
			search.setBookingDateFrom(this.bookedDateFrm + " 00:00:00");
		} else {
			search.setDateRangeFrom(this.bookedDateFrm);
			search.setBookingDateFrom(this.bookedDateFrm);
		}
		if (this.bookedDateTo != null && !this.bookedDateTo.isEmpty()) {
			search.setDateRangeTo(this.bookedDateTo + " 23:59:59");
			search.setBookingDateTo(this.bookedDateTo + " 23:59:59");
		} else {
			search.setDateRangeTo(this.bookedDateTo);
			search.setBookingDateTo(this.bookedDateTo);
		}

		if (!StringUtil.isNullOrEmpty(this.hdnPromoTypes)) {
			String[] arrPromotions = this.hdnPromoTypes.split(",");
			Set<String> lstPromo = new HashSet<String>();
			for (String promoType : arrPromotions) {
				lstPromo.add(promoType);
			}
			search.setPromotionTypes(lstPromo);
		}

		search.setSegmentCodes(this.colOnds);
		search.setFlightNoCollection(colFlightNos);

		if (!StringUtil.isNullOrEmpty(this.hdnAgents)) {
			String[] arrAgents = this.hdnAgents.split(",");
			Collection<String> lstAgent = new HashSet<String>();
			for (String agentCode : arrAgents) {
				lstAgent.add(agentCode);
			}
			search.setAgents(lstAgent);
		}

		if (!StringUtil.isNullOrEmpty(this.hdnChannels)) {
			String[] arrChannels = this.hdnChannels.split(",");
			Collection<String> lstChannel = new HashSet<String>();
			for (String salesChannenl : arrChannels) {
				lstChannel.add(salesChannenl);
			}
			search.setSalesChannels(lstChannel);
		}

		if (strlive != null) {
			if (strlive.equals(WebConstants.REP_LIVE)) {
				search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
			} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
				search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
			}
		}

		resultSet = ModuleServiceLocator.getDataExtractionBD().getPromoCodeDetailsData(search);

		parameters.put("CURRENCY", AppSysParamsUtil.getBaseCurrency());
		parameters.put("ID", id);
		parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
		parameters.put("FROM_DATE", this.departureDateFrm);
		parameters.put("TO_DATE", this.departureDateTo);

		if (radReportOption.trim().equals("HTML")) {
			parameters.put("IMG", image);
			ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null, null,
					response);
		} else if (radReportOption.trim().equals("PDF")) {
			image = ReportsHTMLGenerator.getReportTemplate(strLogo);
			parameters.put("IMG", image);
			response.reset();
			response.addHeader("Content-Disposition", "attachment;filename=PromoCodeDetailsReport.pdf");
			ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);
		} else if (radReportOption.trim().equals("EXCEL")) {
			response.reset();
			response.addHeader("Content-Disposition", "attachment;filename=PromoCodeDetailsReport.xls");
			ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);
		} else if (radReportOption.trim().equals("CSV")) {
			response.reset();
			response.addHeader("Content-Disposition", "attachment;filename=PromoCodeDetailsReport.csv");
			ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);
		}

	}

	public void setHdnPromoTypes(String hdnPromoTypes) {
		this.hdnPromoTypes = hdnPromoTypes;
	}

	public void setDepartureDateFrm(String departureDateFrm) {
		this.departureDateFrm = departureDateFrm;
	}

	public void setDepartureDateTo(String departureDateTo) {
		this.departureDateTo = departureDateTo;
	}

	public void setBookedDateFrm(String bookedDateFrm) {
		this.bookedDateFrm = bookedDateFrm;
	}

	public void setBookedDateTo(String bookedDateTo) {
		this.bookedDateTo = bookedDateTo;
	}

	public void setHdnAgents(String hdnAgents) {
		this.hdnAgents = hdnAgents;
	}

	public void setHdnChannels(String hdnChannels) {
		this.hdnChannels = hdnChannels;
	}

	public void setRadReportOption(String radReportOption) {
		this.radReportOption = radReportOption;
	}

	public void setHdnOnds(String hdnOnds) throws JSONException {
		Set<String> ondSet = new HashSet((List<String>) JSONUtil.deserialize(hdnOnds));
		this.colOnds = ondSet;
	}

	public void setHdnFlightNos(String hdnFlightNos) throws JSONException {
		Set<String> flightNoSet = new HashSet((List<String>) JSONUtil.deserialize(hdnFlightNos));
		this.colFlightNos = flightNoSet;
	}
}
