/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airadmin.core.web.filter;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

public class GlobalFilter implements Filter {
	private static Log log = LogFactory.getLog(GlobalFilter.class);

	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	/**
	 * Filter authentication
	 */
	public void doFilter(ServletRequest sRequest, ServletResponse sResponse, FilterChain chain) throws ServletException,
			IOException {
		if (globalConfig.isGenerateAppCookie()) {
			if (log.isDebugEnabled()) {
				HttpServletRequest request = (HttpServletRequest) sRequest;
				Cookie[] reqCookies = request.getCookies();

				StringBuilder message = new StringBuilder();
				message.append(request.getRequestURI());
				if (reqCookies != null && reqCookies.length > 0) {
					for (int i = 0; i < reqCookies.length; ++i) {
						Cookie cookie = reqCookies[i];
						message.append(":").append(cookie.getName()).append("=").append(cookie.getValue());
					}
				}
				message.append("::").append(request.getRequestedSessionId());
				log.debug(message.toString());
			}

			HttpServletResponse response = (HttpServletResponse) sResponse;
			String cookieName = globalConfig.getCookieName();
			InetAddress host = null;
			String ip = null;
			String cookieValue = null;
			try {
				host = InetAddress.getLocalHost();
				ip = host.getHostAddress();
				Map<String, String> cookieValueMap = globalConfig.getCookieValueMap();
				if (cookieValueMap != null) {
					cookieValue = (String) cookieValueMap.get(ip);
				}
				if ((cookieName == null) || (cookieName.trim().equals("")) || (cookieValue == null)
						|| (cookieValue.trim().equals(""))) {
					throw new Exception("No cookie value");
				}
			} catch (UnknownHostException uhe) {
				log.error("Error in doFilter - server name could not be obtained to get the cookie value - [" + uhe.getMessage()
						+ "]");
				return;
			} catch (Exception e) {
				log.error("Error in doFilter - [" + e.getMessage() + "]");
				return;
			}
			Cookie cookie = new Cookie(cookieName, cookieValue);
			response.addCookie(cookie);
		}

		if (globalConfig.isControlAirAdminCachingHeaders()) {
			String requestURI = ((HttpServletRequest) sRequest).getRequestURI();
			if (requestURI != null) {
				try {
					Map<String, String> expirationDurationsMap = globalConfig.getCacheControlProperties();
					Iterator<String> expirationDurationsMapIt = expirationDurationsMap.keySet().iterator();
					String path;
					while (expirationDurationsMapIt.hasNext()) {
						path = (String) expirationDurationsMapIt.next();
						if (requestURI.indexOf(path) != -1) {
							((HttpServletResponse) sResponse).addHeader("Cache-Control",
									"max-age=" + expirationDurationsMap.get(path));
							break;
						}
					}
				} catch (Exception e) {
					log.error("Error in doFilter - [" + e.getMessage() + "]", e);
					return;
				}
			}
		}

		if (globalConfig.isControlAirAdminSecureHeaders()) {
			((HttpServletResponse) sResponse).addHeader("Cache-Control", "1");
			((HttpServletResponse) sResponse).addHeader("X-Content-Type-Options", "nosniff");
			if (globalConfig.isSecureHeadersIframeDeny()) {
				((HttpServletResponse) sResponse).addHeader("X-Frame-Options", "deny");
			} else if (globalConfig.isSecureHeadersIframeSameOrigin()) {
				((HttpServletResponse) sResponse).addHeader("X-Frame-Options", "sameorigin");
			}
		}

		if (globalConfig.isLogAirAdminAccessInfo()) {
			HttpServletRequest httpReq = (HttpServletRequest) sRequest;
			String requestURI = PlatformUtiltiies.nullHandler(httpReq.getRequestURI());
			if (requestURI.endsWith(".do") || requestURI.endsWith(".action")) {
				log.info("AccessInfo:" + httpReq.getRemoteAddr() + ":" + httpReq.getRequestURI() + ":"
						+ httpReq.getRequestedSessionId());
			}
		}

		chain.doFilter(sRequest, sResponse);
	}

	public void init(FilterConfig filterConfig) throws ServletException {

	}

	public void destroy() {

	}
}
