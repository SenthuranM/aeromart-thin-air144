package com.isa.thinair.airadmin.core.web.v2.dto.mis;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;

/**
 * Handle privileges for MIS screens
 * 
 * @author lalanthi
 * 
 */
public class MISProcessParams {
	/** Privileges for MIS tabs */
	private boolean misDashboard;
	private boolean misDistribution;
	private boolean misRegional;
	private boolean misAgents;
	private boolean misAccounts;
	private boolean misAncillary;
	private boolean misProfitability;

	/** Privileges for Ancillary types */
	private boolean halaEnabled;
	private boolean insuranceEnabled;
	private boolean mealsEnabled;
	private boolean seatEnabled;

	/** Privileges for FLA - Optimization */
	private boolean flaOptimaization;
	private boolean flaAnalyze;
	private boolean flaAlerts;

	/** Update optimize data */
	private boolean updateSegmentAlloc;
	private boolean updateBookingClassAlloc;

	private int misMaxSearchPeriod;

	private static final GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();

	public MISProcessParams(HttpServletRequest request) {

		if (BasicRequestHandler.hasPrivilege(request, "mis.dashboard")) {
			misDashboard = true;
		}
		if (BasicRequestHandler.hasPrivilege(request, "mis.distribution")) {
			misDistribution = true;
		}
		if (BasicRequestHandler.hasPrivilege(request, "mis.regional")) {
			misRegional = true;
		}
		if (BasicRequestHandler.hasPrivilege(request, "mis.agents")) {
			misAgents = true;
		}
		if (BasicRequestHandler.hasPrivilege(request, "mis.accounts")) {
			misAccounts = true;
		}
		if (BasicRequestHandler.hasPrivilege(request, "mis.ancillary")) {
			misAncillary = true;
		}

		if (BasicRequestHandler.hasPrivilege(request, "mis.profitability")) {
			misProfitability = true;
		}

		if ("Y".equalsIgnoreCase(globalConfig.getBizParam(SystemParamKeys.SHOW_SEAT_MAP))) {
			seatEnabled = true;
		}

		if ("Y".equalsIgnoreCase(globalConfig.getBizParam(SystemParamKeys.SHOW_TRAVEL_INSURANCE))) {
			insuranceEnabled = true;
		}

		if ("Y".equalsIgnoreCase(globalConfig.getBizParam(SystemParamKeys.SHOW_MEAL))) {
			mealsEnabled = true;
		}

		if ("Y".equalsIgnoreCase(globalConfig.getBizParam(SystemParamKeys.SHOW_AIRPORT_SERVICES))) {
			halaEnabled = true;
		}

		if (BasicRequestHandler.hasPrivilege(request, "admin.fla.optimize")) {
			flaOptimaization = true;
		}

		if (BasicRequestHandler.hasPrivilege(request, "admin.fla.analyze")) {
			flaAnalyze = true;
		}
		if (BasicRequestHandler.hasPrivilege(request, "admin.fla.alerts")) {
			flaAlerts = true;
		}

		if (BasicRequestHandler.hasPrivilege(request, "plan.invn.alloc.bc.segupdate")) {
			updateSegmentAlloc = true;
		}

		if (BasicRequestHandler.hasPrivilege(request, "plan.invn.alloc.bc.update")) {
			updateBookingClassAlloc = true;
		}

		misMaxSearchPeriod = AppSysParamsUtil.getMISReriod();
	}

	public boolean isMisDashboard() {
		return misDashboard;
	}

	public void setMisDashboard(boolean misDashboard) {
		this.misDashboard = misDashboard;
	}

	public boolean isMisDistribution() {
		return misDistribution;
	}

	public void setMisDistribution(boolean misDistribution) {
		this.misDistribution = misDistribution;
	}

	public boolean isMisRegional() {
		return misRegional;
	}

	public void setMisRegional(boolean misRegional) {
		this.misRegional = misRegional;
	}

	public boolean isMisAgents() {
		return misAgents;
	}

	public void setMisAgents(boolean misAgents) {
		this.misAgents = misAgents;
	}

	public boolean isMisAccounts() {
		return misAccounts;
	}

	public void setMisAccounts(boolean misAccounts) {
		this.misAccounts = misAccounts;
	}

	public boolean isMisAncillary() {
		return misAncillary;
	}

	public void setMisAncillary(boolean misAncillary) {
		this.misAncillary = misAncillary;
	}

	public boolean isMisProfitability() {
		return misProfitability;
	}

	public void setMisProfitability(boolean misProfitability) {
		this.misProfitability = misProfitability;
	}

	public boolean isHalaEnabled() {
		return halaEnabled;
	}

	public void setHalaEnabled(boolean halaEnabled) {
		this.halaEnabled = halaEnabled;
	}

	public boolean isInsuranceEnabled() {
		return insuranceEnabled;
	}

	public void setInsuranceEnabled(boolean insuranceEnabled) {
		this.insuranceEnabled = insuranceEnabled;
	}

	public boolean isMealsEnabled() {
		return mealsEnabled;
	}

	public void setMealsEnabled(boolean mealsEnabled) {
		this.mealsEnabled = mealsEnabled;
	}

	public boolean isSeatEnabled() {
		return seatEnabled;
	}

	public void setSeatEnabled(boolean seatEnabled) {
		this.seatEnabled = seatEnabled;
	}

	public boolean isFlaOptimaization() {
		return flaOptimaization;
	}

	public void setFlaOptimaization(boolean flaOptimaization) {
		this.flaOptimaization = flaOptimaization;
	}

	public boolean isFlaAnalyze() {
		return flaAnalyze;
	}

	public void setFlaAnalyze(boolean flaAnalyze) {
		this.flaAnalyze = flaAnalyze;
	}

	public boolean isFlaAlerts() {
		return flaAlerts;
	}

	public void setFlaAlerts(boolean flaAlerts) {
		this.flaAlerts = flaAlerts;
	}

	public boolean isUpdateSegmentAlloc() {
		return updateSegmentAlloc;
	}

	public void setUpdateSegmentAlloc(boolean updateSegmentAlloc) {
		this.updateSegmentAlloc = updateSegmentAlloc;
	}

	public boolean isUpdateBookingClassAlloc() {
		return updateBookingClassAlloc;
	}

	public void setUpdateBookingClassAlloc(boolean updateBookingClassAlloc) {
		this.updateBookingClassAlloc = updateBookingClassAlloc;
	}

	public int getMisMaxSearchPeriod() {
		return misMaxSearchPeriod;
	}

	public void setMisMaxSearchPeriod(int misMaxSearchPeriod) {
		this.misMaxSearchPeriod = misMaxSearchPeriod;
	}
}
