package com.isa.thinair.airadmin.core.web.v2.action.tools;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airproxy.api.model.reservation.commons.BlacklistPAXCriteriaSearchTO;
import com.isa.thinair.airproxy.api.model.reservation.commons.BlacklistPAXCriteriaTO;
import com.isa.thinair.airreservation.core.remoting.ejb.ReservationServiceBean;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.promotion.api.to.PromotionCriteriaTO;

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class BlacklistedPAXCriteriaAction {
	
	/** BlacklistPAXCriteriaTO DTO object to transfer data to the back end */
	private BlacklistPAXCriteriaTO blacklistPAXCriteriaTO = new BlacklistPAXCriteriaTO();
	
	/** BlacklistPAXCriteriaSearchTO DTO object to transfer data to the back end */
	private BlacklistPAXCriteriaSearchTO blacklistPAXCriteriaSearchTO = new BlacklistPAXCriteriaSearchTO();
	
	private static Log log = LogFactory.getLog(BlacklistedPAXCriteriaAction.class);

	/** Operation success status indicator */
	private boolean success = true;

	/** Success/Failure message for UI display */
	private String messageTxt;

	/** Search result and pagination data */
	private Collection<Map<String, Object>> rows;
	private int page;
	private int total;
	private int records;
	private String blacklistPAXAvailable;
	private Integer blacklistPaxId;
	private String blPaxHasReservations;

	/** Date format for front end data data */
	private final String DATE_FORMAT = "dd/MM/yyyy";
	
	/*
	 * Save method. Create new blacklist PAX 
	 * 
	 */
	public String save() {

		try {
			ModuleServiceLocator.getBlacklisPAXCriteriaAdminBD().saveBlacklistPAX(blacklistPAXCriteriaTO);
			
		} catch (Exception e) {
			log.error(messageTxt, e);
			success = false;
		}

		return S2Constants.Result.SUCCESS;
	}
	
	/*
	 * Update method. Saves changes to existing Blacklis PAX
	 */
	public String update() {

		try {
			long blacklistPAXCriteriaID = ModuleServiceLocator.getBlacklisPAXCriteriaAdminBD().updateBlacklistPAX(blacklistPAXCriteriaTO);
			if (blacklistPAXCriteriaID == -1) {
				success = false;
				messageTxt = S2Constants.messageText.DUPLICATE_RECORD;
			} else {
				blacklistPAXCriteriaTO.setBlacklistPAXCriteriaID(blacklistPAXCriteriaID);
			}
		} catch (Exception e) {
			log.error(messageTxt, e);
			success = false;
		}

		return S2Constants.Result.SUCCESS;
	}
	
	/*
	 * Search method
	 */
	public String search() {

		try {
			Page<BlacklistPAXCriteriaTO> criteriaSearchPage = null;
			int start = (page - 1) * 20;
			Map<String, Object> row;

			if (blacklistPAXCriteriaSearchTO != null) {

				criteriaSearchPage = ModuleServiceLocator.getBlacklisPAXCriteriaAdminBD().searchBlacklistPAXCriteria(
						blacklistPAXCriteriaSearchTO, start, 20);

				this.setRecords(criteriaSearchPage.getTotalNoOfRecords());
				this.total = criteriaSearchPage.getTotalNoOfRecords() / 20;
				int mod = criteriaSearchPage.getTotalNoOfRecords() % 20;
				if (mod > 0) {
					this.total = this.total + 1;
				}

				Collection<BlacklistPAXCriteriaTO> criterias = criteriaSearchPage.getPageData();
				List<BlacklistPAXCriteriaTO> criteriaList = new ArrayList<BlacklistPAXCriteriaTO>(criterias);
				Collections.sort(criteriaList, new Comparator<BlacklistPAXCriteriaTO>() {
					public int compare(BlacklistPAXCriteriaTO p1, BlacklistPAXCriteriaTO p2) {
						return p2.getPaxFullName().compareTo(p1.getPaxFullName());
					}
				});
				
				rows = new ArrayList<Map<String, Object>>();

				
				int i = 1;
				for (BlacklistPAXCriteriaTO criteria : criteriaList) {

					row = new HashMap<String, Object>();
					row.put("id", ((page - 1) * 20) + i++);
					row.put("criteria", criteria);

					rows.add(row);
				}
				
			}

		} catch (Exception e) {
			log.error(messageTxt, e);
			success = false;
		}

		return S2Constants.Result.SUCCESS;
	}
	
	public String searchBlacklistPAX() {
		try {
			if (ModuleServiceLocator.getBlacklisPAXCriteriaAdminBD().isBlacklistPAXAvailable(
					blacklistPAXCriteriaSearchTO.getBlacklistPAXCriteriaID())) {
				blacklistPAXAvailable = "Y";
			} else {
				blacklistPAXAvailable = "N";
			}

		} catch (Exception e) {
			log.error(messageTxt, e);
			success = false;
		}
		return S2Constants.Result.SUCCESS;
	}

	public String isblPaxHasReservations(){
		try {
			if(ModuleServiceLocator.getBlacklisPAXCriteriaAdminBD().isBlackListPaxAlreadyInReservations(blacklistPaxId)){
				blPaxHasReservations ="Y";
			}else{
				blPaxHasReservations="N";
			}			
		} catch (Exception e) {
			log.error(messageTxt, e);
			success = false;
		}
		return S2Constants.Result.SUCCESS;		
	}
	
	public String deleteBlPax() {

		try {
			ModuleServiceLocator.getBlacklisPAXCriteriaAdminBD().deleteBlacklistPAX(blacklistPAXCriteriaTO);
			
		} catch (Exception e) {
			log.error(messageTxt, e);
			success = false;
		}

		return S2Constants.Result.SUCCESS;
	}
	
	
	
	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public Collection<Map<String, Object>> getRows() {
		return rows;
	}

	public void setRows(Collection<Map<String, Object>> rows) {
		this.rows = rows;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public BlacklistPAXCriteriaTO getBlacklistPAXCriteriaTO() {
		return blacklistPAXCriteriaTO;
	}

	public void setBlacklistPAXCriteriaTO(
			BlacklistPAXCriteriaTO blacklistPAXCriteriaTO) {
		this.blacklistPAXCriteriaTO = blacklistPAXCriteriaTO;
	}
	
	public void setDateOfBirth(String dateOfBirth) throws ParseException {
		blacklistPAXCriteriaTO.setDateOfBirth(new SimpleDateFormat(DATE_FORMAT).parse(dateOfBirth));
	}
	
	public void setEffectiveFrom(String effectiveFrom) throws ParseException {
		blacklistPAXCriteriaTO.setEffectiveFrom(new SimpleDateFormat(DATE_FORMAT).parse(effectiveFrom));
	}
	
	public void setEffectiveTo(String effectiveTo) throws ParseException {
		blacklistPAXCriteriaTO.setEffectiveTo(new SimpleDateFormat(DATE_FORMAT).parse(effectiveTo));
	}

	public BlacklistPAXCriteriaSearchTO getBlacklistPAXCriteriaSearchTO() {
		return blacklistPAXCriteriaSearchTO;
	}

	public void setBlacklistPAXCriteriaSearchTO(
			BlacklistPAXCriteriaSearchTO blacklistPAXCriteriaSearchTO) {
		this.blacklistPAXCriteriaSearchTO = blacklistPAXCriteriaSearchTO;
	}
	
	public void setSearchDateOfBirth(String searchDateOfBirth) throws ParseException {
		blacklistPAXCriteriaSearchTO.setDateOfBirth(new SimpleDateFormat(DATE_FORMAT).parse(searchDateOfBirth));
	}
	
	public void setValidUntil(String validUntil) throws ParseException {
		blacklistPAXCriteriaTO.setValidUntil(new SimpleDateFormat(DATE_FORMAT).parse(validUntil));
	}

	public String getBlacklistPAXAvailable() {
		return blacklistPAXAvailable;
	}

	public void setBlacklistPAXAvailable(String blacklistPAXAvailable) {
		this.blacklistPAXAvailable = blacklistPAXAvailable;
	}

	public Integer getBlacklistPaxId() {
		return blacklistPaxId;
	}

	public void setBlacklistPaxId(Integer blacklistPaxId) {
		this.blacklistPaxId = blacklistPaxId;
	}

	public String isBlPaxHasReservations() {
		return blPaxHasReservations;
	}

	public void setBlPaxHasReservations(String blPaxHasReservations) {
		this.blPaxHasReservations = blPaxHasReservations;
	}
	
	
	
}