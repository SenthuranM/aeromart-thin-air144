package com.isa.thinair.airadmin.core.web.v2.action.promotion;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants.DiscountApplyTo;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * Action class to load configurable data for promotion criteria management screen. Failing silently in case of an
 * exception because some functionality is possible without these loaded data.
 * 
 * @author thihara
 * 
 */
@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class LoadPromotionFormInitDataAction {

	private static Log log = LogFactory.getLog(LoadPromotionTemplatesManagementAction.class);

	private Map<String, String> airports;

	private Collection<String> flightNos;

	private Collection<String> availableLCC;

	private Collection<String> availableCOS;

	private Collection<String> availableBC;

	private Map<String, String> availableChannels;

	private Map<String, String> agents;

	private Collection<String> availableAnci;

	private Collection<String> promotionTypes;

	private Collection<String> status;

	private Collection<String> applyToOptions;

	private Collection<String> types;

	private Collection<String> discountType;

	private Collection<String> selectedFlights;

	private String selectedRoute;
	
	private String maxDatePeriodCount;

	public String execute() {
		try {

			airports = SelectListGenerator.createActiveAirportCodeMap();

			/*
			 * airports.put("All", "***"); airports = CommonUtil.sortByValue(airports);
			 */

			agents = SelectListGenerator.getAgentsCodeNameMap();

			promotionTypes = Arrays.asList(PromotionCriteriaConstants.PromotionCriteriaTypesDesc.BUY_AND_GET_FREE_CRIERIA,
					PromotionCriteriaConstants.PromotionCriteriaTypesDesc.DISCOUNT_CRITERIA,
					PromotionCriteriaConstants.PromotionCriteriaTypesDesc.FREE_SERVICE_CRITERIA);

			availableAnci = Arrays.asList(PromotionCriteriaConstants.ANCILLARIES.AIRPORT_SERVICE.getCode(),
					PromotionCriteriaConstants.ANCILLARIES.BAGGAGE.getCode(),
					PromotionCriteriaConstants.ANCILLARIES.MEAL.getCode(),
					PromotionCriteriaConstants.ANCILLARIES.SEAT_MAP.getCode(),
					PromotionCriteriaConstants.ANCILLARIES.SSR.getCode(),
					PromotionCriteriaConstants.ANCILLARIES.INSURANCE.getCode());

			status = Arrays.asList(PromotionCriteriaConstants.PromotionStatus.ACTIVE,
					PromotionCriteriaConstants.PromotionStatus.INACTIVE);

			// applyToOptions =
			// Arrays.asList(PromotionCriteriaConstants.ApplyToOptions.FARE,
			// PromotionCriteriaConstants.ApplyToOptions.FARE_SURCHARGES,
			// PromotionCriteriaConstants.ApplyToOptions.TOTAL);

			applyToOptions = new ArrayList<String>();
			for (DiscountApplyTo discAppltTo : DiscountApplyTo.values()) {
				applyToOptions.add(discAppltTo.getName());
			}

			types = Arrays.asList(PromotionCriteriaConstants.DiscountApplyAsTypes.CREDIT,
					PromotionCriteriaConstants.DiscountApplyAsTypes.MONEY);

			discountType = Arrays.asList(PromotionCriteriaConstants.DiscountTypes.VALUE,
					PromotionCriteriaConstants.DiscountTypes.PERCENTAGE);

			flightNos = SelectListGenerator.getFlightNoList();

			availableLCC = SelectListGenerator.getActiveLogicalCabinClassList();
			availableCOS = SelectListGenerator.getCabinClassList();
			availableBC = SelectListGenerator.getOnlyBookingClassCodeList();
			availableChannels = SelectListGenerator.getVisibleSalesChannel();
			maxDatePeriodCount = AppSysParamsUtil.getMaxDatePeriodsForPromotionCriteria();
		} catch (Exception e) {
			log.error("execute ", e);
		}
		return S2Constants.Result.SUCCESS;
	}

	public Map<String, String> getAirports() {
		return airports;
	}

	public Collection<String> getFlightNos() {
		return flightNos;
	}

	public Collection<String> getAvailableAnci() {
		return availableAnci;
	}

	public Collection<String> getAvailableLCC() {
		return availableLCC;
	}

	public Collection<String> getAvailableCOS() {
		return availableCOS;
	}

	public Collection<String> getAvailableBC() {
		return availableBC;
	}

	public Map<String, String> getAvailableChannels() {
		return availableChannels;
	}

	public Map<String, String> getAgents() {
		return agents;
	}

	public Collection<String> getPromotionTypes() {
		return promotionTypes;
	}

	public Collection<String> getStatus() {
		return status;
	}

	public Collection<String> getApplyToOptions() {
		return applyToOptions;
	}

	public Collection<String> getTypes() {
		return types;
	}

	public Collection<String> getDiscountType() {
		return discountType;
	}

	public String getSeletedFlights() {
		try {
			String routes = selectedRoute;
			selectedFlights = SelectListGenerator.getSelectedFlightNoList(selectedRoute);

		} catch (Exception e) {
			log.error("execute ", e);
		}
		return S2Constants.Result.SUCCESS;
	}

	public Collection<String> getSelectedFlights() {
		return selectedFlights;
	}

	public void setSelectedFlights(Collection<String> selectedFlights) {
		this.selectedFlights = selectedFlights;
	}

	public String getSelectedRoute() {
		return selectedRoute;
	}

	public void setSelectedRoute(String selectedRoute) {
		this.selectedRoute = selectedRoute;
	}

	public String getMaxDatePeriodCount() {
		return maxDatePeriodCount;
	}

	public void setMaxDatePeriodCount(String maxDatePeriodCount) {
		this.maxDatePeriodCount = maxDatePeriodCount;
	}
}