package com.isa.thinair.airadmin.core.web.action.pricing;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airmaster.api.model.Airport;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ShowMasterLinkFaresAction {
	private static final Log log = LogFactory.getLog(ShowMasterLinkFaresAction.class);
	private static AiradminConfig airadminConfig = new AiradminConfig();

	private String origin;

	private String destination;

	private String via1;

	private String via2;

	private String via3;

	private String via4;

	private Map<String, String> masterFares = new HashMap<String, String>();

	private String messageTxt = "";

	private boolean success;

	private String ondCode;

	private String fareId;

	public String execute() {

		try {
			if (isValidAirports()) {
				ondCode = composeOndCode();
				Map<Integer, String> masterFaresForOnd = ModuleServiceLocator.getFareBD().getMasterFaresForOnD(ondCode);
				if (masterFaresForOnd != null && masterFaresForOnd.size() > 0) {
					for (Integer key : masterFaresForOnd.keySet()) {
						if (!AiradminUtils.getNotNullString(fareId).equals(key.toString())) {
							masterFares.put(key.toString(), masterFaresForOnd.get(key));
						}
					}
					success = true;
				} else {
					success = false;
					messageTxt = "Master fares not defined for the " + ondCode + " combination";
				}

			}
		} catch (ModuleException e) {
			log.error("ShowMasterLinkFaresAction error " + e.getMessage());
			success = false;
			messageTxt = "Searching master fares failed. Please try again later.";
		} catch (Exception ex) {
			log.error("ShowMasterLinkFaresAction error " + ex.toString());
			success = false;
			messageTxt = "Searching master fares failed. Please try again later.";
		}

		return S2Constants.Result.SUCCESS;
	}

	private boolean isValidAirports() throws ModuleException {
		boolean validairport = true;
		List<Map<String, String>> lstAirport = SelectListGenerator.createAirportsListWithStatus();
		if (AiradminUtils.getNotNullString(origin).length() > 0) {

			validairport = validateAirports(lstAirport, origin);
			if (!validairport) {
				messageTxt = airadminConfig.getMessage("um.fare.origin.not.active");
			}
			if (AiradminUtils.getNotNullString(via1).length() > 0) {
				if (validairport) {
					validairport = validateAirports(lstAirport, via1);
					if (!validairport) {
						messageTxt = airadminConfig.getMessage("um.fare.via1.not.active");
					}
				}
			}
			if (AiradminUtils.getNotNullString(via2).length() > 0) {
				if (validairport) {
					validairport = validateAirports(lstAirport, via2);
					if (!validairport) {
						messageTxt = airadminConfig.getMessage("um.fare.via2.not.active");
					}
				}
			}
			if (AiradminUtils.getNotNullString(via3).length() > 0) {
				if (validairport) {
					validairport = validateAirports(lstAirport, via3);
					if (!validairport) {

						messageTxt = airadminConfig.getMessage("um.fare.via3.not.active");
					}
				}
			}
			if (AiradminUtils.getNotNullString(via4).length() > 0) {
				if (validairport) {
					validairport = validateAirports(lstAirport, via4);
					if (!validairport) {
						messageTxt = airadminConfig.getMessage("um.fare.via4.not.active");
					}
				}
			}
			if (AiradminUtils.getNotNullString(destination).length() > 0) {
				validairport = validateAirports(lstAirport, destination);
				if (!validairport) {
					messageTxt = airadminConfig.getMessage("um.fare.destination.not.active");
				}
			}
		}
		return validairport;

	}

	private static boolean validateAirports(List<Map<String, String>> lstAirport, String airport) {

		boolean valid = false;
		try {
			Iterator<Map<String, String>> ite = lstAirport.iterator();
			while (ite.hasNext()) {

				Map<String, String> keyValues = (Map<String, String>) ite.next();
				Set<String> keys = keyValues.keySet();
				Iterator<String> keyIterator = keys.iterator();

				while (keyIterator.hasNext()) {
					String status = (String) keyValues.get(keyIterator.next());
					String port = (String) keyValues.get(keyIterator.next());

					if (port.equals(airport)) {
						if (status.equals(Airport.STATUS_ACTIVE)) {
							return true;
						} else {
							return false;
						}
					}
				}
			}

		} catch (Exception e) {
			log.error("ShowMasterLinkFaresAction.validateAirports() method is failed :" + e.getMessage());
		}
		return valid;
	}

	private String composeOndCode() {
		String ondCode = "";
		
		if (AiradminUtils.getNotNullString(origin).length() > 0 && AiradminUtils.getNotNullString(destination).length() > 0) {
			ondCode += origin + "/";

			if (AiradminUtils.getNotNullString(via1).length() > 0) {
				ondCode += via1 + "/";
			}

			if (AiradminUtils.getNotNullString(via2).length() > 0) {
				ondCode += via2 + "/";
			}

			if (AiradminUtils.getNotNullString(via3).length() > 0) {
				ondCode += via3 + "/";
			}

			if (AiradminUtils.getNotNullString(via4).length() > 0) {
				ondCode += via4 + "/";
			}
			ondCode += destination;
		}

		return ondCode;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getVia1() {
		return via1;
	}

	public void setVia1(String via1) {
		this.via1 = via1;
	}

	public String getVia2() {
		return via2;
	}

	public void setVia2(String via2) {
		this.via2 = via2;
	}

	public String getVia3() {
		return via3;
	}

	public void setVia3(String via3) {
		this.via3 = via3;
	}

	public String getVia4() {
		return via4;
	}

	public void setVia4(String via4) {
		this.via4 = via4;
	}

	public Map<String, String> getMasterFares() {
		return masterFares;
	}

	public void setMasterFares(Map<String, String> masterFares) {
		this.masterFares = masterFares;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getOndCode() {
		return ondCode;
	}

	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}

	public String getFareId() {
		return fareId;
	}

	public void setFareId(String fareId) {
		this.fareId = fareId;
	}
}