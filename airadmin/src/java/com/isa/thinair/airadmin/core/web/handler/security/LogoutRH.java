package com.isa.thinair.airadmin.core.web.handler.security;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleConfig;
import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.TasksUtil;
import com.isa.thinair.login.util.ForceLoginInvoker;

public class LogoutRH extends BasicRequestHandler {

	private static AiradminModuleConfig config = AiradminModuleUtils.getConfig();

	private static Log log = LogFactory.getLog(LogoutRH.class);

	/**
	 * main Execute Method for Log out. Invalidate the session & Audit
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {

		String forward = AdminStrutsConstants.AdminAction.SUCCESS;

		if ("true".equals(config.getEnableAudit())) {
			try {
				audit(request, TasksUtil.SECURITY_LOG_OUT);
			} catch (ModuleException e) {
				log.error(e);
			}
		} else {
			User user = (User) request.getSession().getAttribute(WebConstants.SES_CURR_USER);
			log.info("LOGOUT USER [userID = " + user.getUserId() + "]");
		}

		if (!request.getSession().isNew()) {
			request.getSession().invalidate();
			ForceLoginInvoker.close();
		}
		return forward;
	}
}
