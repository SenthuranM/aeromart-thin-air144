package com.isa.thinair.airadmin.core.web.action.reports;

import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.handler.reports.AirportTaxReportRH;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;

/**
 * @author harshaa
 * 
 */
@Results({
		@Result(name = AdminStrutsConstants.AdminAction.SUCCESS, value = AdminStrutsConstants.AdminJSP.REPORTING_AIRPORT_TAX_JSP),
		@Result(name = AdminStrutsConstants.AdminAction.ERROR, value = AdminStrutsConstants.AdminJSP.ERROR_JSP) })
public class ShowAirportTaxReportAction extends BaseRequestResponseAwareAction {
	public String execute() {
		return AirportTaxReportRH.execute(request, response);
	}

}
