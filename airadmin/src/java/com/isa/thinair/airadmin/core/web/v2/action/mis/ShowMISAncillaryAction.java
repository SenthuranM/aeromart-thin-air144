package com.isa.thinair.airadmin.core.web.v2.action.mis;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.v2.dto.mis.MISProcessParams;
import com.isa.thinair.airadmin.core.web.v2.util.CommonUtil;
import com.isa.thinair.airreservation.api.dto.ExternalChgDTO;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.EXTERNAL_CHARGES;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.reporting.api.criteria.MISReportsSearchCriteria;
import com.isa.thinair.reporting.api.model.AncillaryByChannelTO;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Result(name = AdminStrutsConstants.AdminAction.SUCCESS, type = JSONResult.class, value = "")
public class ShowMISAncillaryAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(ShowMISAncillaryAction.class);
	private static String ANCI_SEAT = "SEAT";
	private static String ANCI_HALA = "HALA";
	private static String ANCI_MEAL = "MEAL";
	private static String ANCI_INSURANCE = "INSURANCE";

	private boolean success = true;
	private String messageTxt;
	private MISReportsSearchCriteria misSearchCriteria;
	private Map<String, BigDecimal> ancillarySales;
	private String ancillarySalesTotalValue;
	private Map<String, Map<String, String>> ancillarySalesByRoute;
	private Collection<AncillaryByChannelTO> ancillarySalesByChannel;
	private Map<String, String> salesByChannelTotals;
	private String baseCurrency;
	private String allChargesTotal;
	private boolean anciByCount = false;
	private Map<String, String> enabledAncillary;

	public String execute() throws Exception {
		String strForward = WebConstants.FORWARD_SUCCESS;
		MISProcessParams mpParams = new MISProcessParams(request);
		request.setAttribute("req_InitialParams", CommonUtil.convertToJSON(mpParams));
		try {
			misSearchCriteria = new MISReportsSearchCriteria();
			misSearchCriteria.setFromDate(request.getParameter("fromDate"));
			misSearchCriteria.setToDate(request.getParameter("toDate"));
			if (!"ALL".equalsIgnoreCase(request.getParameter("region"))) {
				misSearchCriteria.setRegion(request.getParameter("region"));
			}
			if (!"ALL".equalsIgnoreCase(request.getParameter("pos"))) {
				misSearchCriteria.setPos(request.getParameter("pos"));
			}
			misSearchCriteria.setAncillaryChargeCodes(getChargeCodeData(mpParams));
			setEnabledAncillaryData(mpParams);

			if (request.getParameter("anciMode") != null && request.getParameter("anciMode").equalsIgnoreCase("COUNT")) {
				misSearchCriteria.setAnciByCount(true);
				this.setAnciByCount(true);
			}

			ancillarySales = getAncillarySalesSummary();
			ancillarySalesByRoute = getAncillarySalesDataByRoute();
			ancillarySalesByChannel = getAncillarySalesDataByChannel();

		} catch (Exception e) {
			log.error("Generic exception caught.", e);
			success = false;
			messageTxt = e.getMessage();
		}
		return strForward;
	}

	private void setEnabledAncillaryData(MISProcessParams mpParams) {
		this.enabledAncillary = new HashMap<String, String>();
		if (mpParams.isSeatEnabled()) {
			this.enabledAncillary.put(ANCI_SEAT, ANCI_SEAT);
		}
		if (mpParams.isHalaEnabled()) {
			this.enabledAncillary.put(ANCI_HALA, ANCI_HALA);
		}
		if (mpParams.isInsuranceEnabled()) {
			this.enabledAncillary.put(ANCI_INSURANCE, ANCI_INSURANCE);
		}
		if (mpParams.isMealsEnabled()) {
			this.enabledAncillary.put(ANCI_MEAL, ANCI_MEAL);
		}
	}

	@SuppressWarnings("unchecked")
	/**
	 * Retrieves the charge codes for Ancillary services from the configuration.
	 */
	private String getChargeCodeData(MISProcessParams mpParams) throws ModuleException {
		String externalCharges = "";
		Collection<EXTERNAL_CHARGES> colEXTERNAL_CHARGES = new ArrayList<EXTERNAL_CHARGES>();

		if (mpParams.isSeatEnabled()) {
			colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.SEAT_MAP);
		}
		if (mpParams.isHalaEnabled()) {
			colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.AIRPORT_SERVICE);
		}
		if (mpParams.isInsuranceEnabled()) {
			colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.INSURANCE);
		}
		if (mpParams.isMealsEnabled()) {
			colEXTERNAL_CHARGES.add(EXTERNAL_CHARGES.MEAL);
		}

		Map<EXTERNAL_CHARGES, ExternalChgDTO> externalChargesMap = ModuleServiceLocator.getReservationBD().getQuotedExternalCharges(
				colEXTERNAL_CHARGES, null, null);
		ExternalChgDTO objExCharges = null;
		if (mpParams.isSeatEnabled()) {
			objExCharges = externalChargesMap.get(EXTERNAL_CHARGES.SEAT_MAP);
			externalCharges += "'" + objExCharges.getChargeCode() + "',";
		}
		if (mpParams.isHalaEnabled()) {
			objExCharges = externalChargesMap.get(EXTERNAL_CHARGES.AIRPORT_SERVICE);
			externalCharges += "'" + objExCharges.getChargeCode() + "',";
		}
		if (mpParams.isInsuranceEnabled()) {
			objExCharges = externalChargesMap.get(EXTERNAL_CHARGES.INSURANCE);
			externalCharges += "'" + objExCharges.getChargeCode() + "',";
		}
		if (mpParams.isMealsEnabled()) {
			objExCharges = externalChargesMap.get(EXTERNAL_CHARGES.MEAL);
			externalCharges += "'" + objExCharges.getChargeCode() + "'";
		}

		return externalCharges;
	}

	/**
	 * This method will return ancillary summary data for a given search criteria.
	 * 
	 * @return Map key : Ancillary Category (Ex: HALA) Value : Sales
	 */
	private Map<String, BigDecimal> getAncillarySalesSummary() {
		Map<String, BigDecimal> ancillarySalesData = ModuleServiceLocator.getMISDataProviderBD().getAncillarySalesSummary(
				misSearchCriteria);
		if (misSearchCriteria.isAnciByCount()) {
			for (String chargeCode : ancillarySalesData.keySet()) {
				BigDecimal salesVal = ancillarySalesData.get(chargeCode);
				if (salesVal != null) {
					ancillarySalesData.put(chargeCode, salesVal.setScale(0));
				}
			}
		} else {
			for (String chargeCode : ancillarySalesData.keySet()) {
				BigDecimal formattedVal = ancillarySalesData.get(chargeCode);
				if (formattedVal != null) {
					formattedVal = new BigDecimal(AccelAeroCalculator.formatAsDecimal(formattedVal));
					ancillarySalesData.put(chargeCode, formattedVal);
				}
			}
		}
		return ancillarySalesData;
	}

	/**
	 * This method will return ancillary revenue for each category segment wise.
	 * 
	 * @return Map key : Ancillary Category (Ex: HALA) Value : Map<Key : Segment Code (SHJ/CMB), Value: Revenue (1500.00
	 *         AED )
	 */
	private Map<String, Map<String, String>> getAncillarySalesDataByRoute() {

		return ModuleServiceLocator.getMISDataProviderBD().getAncillarySalesByRoute(misSearchCriteria);

	}

	/**
	 * Prepares Ancillary sales data by Channel. if the search criteria is by count, then no need to include decimal
	 * places else - search criteria is by sales - need to include decimal places.
	 * 
	 * @return
	 */
	private Collection<AncillaryByChannelTO> getAncillarySalesDataByChannel() {

		Map<String, Map<String, BigDecimal>> ancSalesByChannel = ModuleServiceLocator.getMISDataProviderBD()
				.getAncillarySalesBychannel(misSearchCriteria);
		if (misSearchCriteria.isAnciByCount()) {
			return prepareAncillaryDataForCount(ancSalesByChannel);
		} else {
			return prepareAncillaryDataForSales(ancSalesByChannel);
		}
	}

	/**
	 * Search criteria is by sales - need to include decimal places
	 * 
	 * @param ancSalesByChannel
	 * @return
	 */
	private Collection<AncillaryByChannelTO> prepareAncillaryDataForSales(Map<String, Map<String, BigDecimal>> ancSalesByChannel) {
		Collection<AncillaryByChannelTO> anciByChannel = new ArrayList<AncillaryByChannelTO>();
		String baseCurrency = AppSysParamsUtil.getBaseCurrency();
		AncillaryByChannelTO ancillaryByChannelTO = null;
		Map<String, String> salesByChannelMap = new HashMap<String, String>();
		BigDecimal allSalesTotal = AccelAeroCalculator.getDefaultBigDecimalZero();

		// Prepares channel wise totals
		if (ancSalesByChannel != null && ancSalesByChannel.size() > 0) {
			for (String charge : ancSalesByChannel.keySet()) {
				if (isThisAnciEnabled(charge)) {
					ancillaryByChannelTO = new AncillaryByChannelTO();
					BigDecimal sales = AccelAeroCalculator.getDefaultBigDecimalZero();
					Map<String, BigDecimal> salesPerChannleMap = ancSalesByChannel.get(charge);
					for (String channel : salesPerChannleMap.keySet()) {
						sales = AccelAeroCalculator.add(sales, salesPerChannleMap.get(channel));
						salesByChannelMap.put(channel, null);
					}
					ancillaryByChannelTO.setChargeCode(charge);
					ancillaryByChannelTO.setSalesByChannel(salesPerChannleMap);
					ancillaryByChannelTO.setTotalAmount(AccelAeroCalculator.formatAsDecimal(sales) + " " + baseCurrency);
					allSalesTotal = AccelAeroCalculator.add(sales, allSalesTotal);
					anciByChannel.add(ancillaryByChannelTO);
				}
			}

			// Prepare sales channel wise totals
			for (AncillaryByChannelTO obj : anciByChannel) {
				Map<String, BigDecimal> salesPerCatMap = obj.getSalesByChannel();
				for (String key : salesPerCatMap.keySet()) {
					if (salesByChannelMap.get(key) != null) {
						BigDecimal channelSales = AccelAeroCalculator.add(new BigDecimal(salesByChannelMap.get(key)),
								salesPerCatMap.get(key));
						salesByChannelMap.put(key, AccelAeroCalculator.formatAsDecimal(channelSales));
					} else {
						salesByChannelMap.put(key, AccelAeroCalculator.formatAsDecimal(salesPerCatMap.get(key)));
					}
				}
			}

			// Add zeros to empty sales channels
			for (AncillaryByChannelTO obj : anciByChannel) {
				Map<String, String> salesPerCatDisplayMap = new HashMap<String, String>();
				Map<String, BigDecimal> salesPerCatMap = obj.getSalesByChannel();
				for (String key : salesByChannelMap.keySet()) {
					if (!salesPerCatMap.containsKey(key)) {
						salesPerCatMap.put(key, AccelAeroCalculator.getDefaultBigDecimalZero());
					}
				}
				Map<String, BigDecimal> salesPerCategory = obj.getSalesByChannel();
				for (String key : salesByChannelMap.keySet()) {
					salesPerCatDisplayMap.put(key, AccelAeroCalculator.formatAsDecimal(salesPerCategory.get(key)));
				}
				obj.setDisplaySalesByChannel(salesPerCatDisplayMap);
			}

			this.salesByChannelTotals = salesByChannelMap;
			this.baseCurrency = baseCurrency;
			this.allChargesTotal = AccelAeroCalculator.formatAsDecimal(allSalesTotal);
		}
		return anciByChannel;
	}

	private Collection<AncillaryByChannelTO> prepareAncillaryDataForCount(Map<String, Map<String, BigDecimal>> ancSalesByChannel) {

		Collection<AncillaryByChannelTO> anciByChannel = new ArrayList<AncillaryByChannelTO>();
		AncillaryByChannelTO ancillaryByChannelTO = null;
		Map<String, String> salesByChannelMap = new HashMap<String, String>();
		BigDecimal allSalesTotal = AccelAeroCalculator.getDefaultBigDecimalZero();
		// Prepares channel wise totals
		if (ancSalesByChannel != null && ancSalesByChannel.size() > 0) {
			for (String charge : ancSalesByChannel.keySet()) {
				if (isThisAnciEnabled(charge)) {
					ancillaryByChannelTO = new AncillaryByChannelTO();
					BigDecimal sales = AccelAeroCalculator.getDefaultBigDecimalZero();
					Map<String, BigDecimal> salesPerChannleMap = ancSalesByChannel.get(charge);
					for (String channel : salesPerChannleMap.keySet()) {
						sales = AccelAeroCalculator.add(sales, salesPerChannleMap.get(channel));
						salesByChannelMap.put(channel, null);
					}
					sales = sales.setScale(0);
					ancillaryByChannelTO.setChargeCode(charge);
					ancillaryByChannelTO.setSalesByChannel(salesPerChannleMap);
					ancillaryByChannelTO.setTotalAmount(sales.toPlainString());
					allSalesTotal = AccelAeroCalculator.add(sales, allSalesTotal);
					anciByChannel.add(ancillaryByChannelTO);
					allSalesTotal = allSalesTotal.setScale(0);
				}
			}

			// Prepare sales channel wise totals
			for (AncillaryByChannelTO obj : anciByChannel) {
				Map<String, BigDecimal> salesPerCatMap = obj.getSalesByChannel();
				for (String key : salesPerCatMap.keySet()) {
					if (salesByChannelMap.get(key) != null) {
						BigDecimal channelTotal = AccelAeroCalculator.add(new BigDecimal(salesByChannelMap.get(key)),
								salesPerCatMap.get(key));
						salesByChannelMap.put(key, channelTotal.setScale(0).toPlainString());
					} else {
						salesByChannelMap.put(key, salesPerCatMap.get(key).setScale(0).toPlainString());
					}
				}
			}

			// Add zeros to empty sales channels
			for (AncillaryByChannelTO obj : anciByChannel) {
				Map<String, String> salesPerCatDisplayMap = new HashMap<String, String>();
				Map<String, BigDecimal> salesPerCatMap = obj.getSalesByChannel();
				for (String key : salesByChannelMap.keySet()) {
					if (!salesPerCatMap.containsKey(key)) {
						salesPerCatMap.put(key, AccelAeroCalculator.getDefaultBigDecimalZero().setScale(0));
					}
				}
				Map<String, BigDecimal> salesPerCategory = obj.getSalesByChannel();
				for (String key : salesByChannelMap.keySet()) {
					salesPerCatDisplayMap.put(key, (salesPerCategory.get(key).toPlainString()));
				}
				obj.setDisplaySalesByChannel(salesPerCatDisplayMap);
			}
			this.salesByChannelTotals = salesByChannelMap;
			allSalesTotal = allSalesTotal.setScale(0);
			this.allChargesTotal = allSalesTotal.toPlainString();
			this.baseCurrency = "";
		}

		return anciByChannel;
	}

	private boolean isThisAnciEnabled(String chargeCode) {
		return (this.enabledAncillary.containsKey(chargeCode));
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public MISReportsSearchCriteria getMisSearchCriteria() {
		return misSearchCriteria;
	}

	public void setMisSearchCriteria(MISReportsSearchCriteria misSearchCriteria) {
		this.misSearchCriteria = misSearchCriteria;
	}

	public Map<String, BigDecimal> getAncillarySales() {
		return ancillarySales;
	}

	public void setAncillarySales(Map<String, BigDecimal> ancillarySales) {
		this.ancillarySales = ancillarySales;
	}

	public String getAncillarySalesTotalValue() {
		return ancillarySalesTotalValue;
	}

	public void setAncillarySalesTotalValue(String ancillarySalesTotalValue) {
		this.ancillarySalesTotalValue = ancillarySalesTotalValue;
	}

	public Map<String, Map<String, String>> getAncillarySalesByRoute() {
		return ancillarySalesByRoute;
	}

	public void setAncillarySalesByRoute(Map<String, Map<String, String>> ancillarySalesByRoute) {
		this.ancillarySalesByRoute = ancillarySalesByRoute;
	}

	public Map<String, String> getSalesByChannelTotals() {
		return salesByChannelTotals;
	}

	public void setSalesByChannelTotals(Map<String, String> salesByChannelTotals) {
		this.salesByChannelTotals = salesByChannelTotals;
	}

	public Collection<AncillaryByChannelTO> getAncillarySalesByChannel() {
		return ancillarySalesByChannel;
	}

	public void setAncillarySalesByChannel(Collection<AncillaryByChannelTO> ancillarySalesByChannel) {
		this.ancillarySalesByChannel = ancillarySalesByChannel;
	}

	public String getBaseCurrency() {
		return baseCurrency;
	}

	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}

	public String getAllChargesTotal() {
		return allChargesTotal;
	}

	public void setAllChargesTotal(String allChargesTotal) {
		this.allChargesTotal = allChargesTotal;
	}

	public boolean isAnciByCount() {
		return anciByCount;
	}

	public void setAnciByCount(boolean anciByCount) {
		this.anciByCount = anciByCount;
	}

	public Map<String, String> getEnabledAncillary() {
		return enabledAncillary;
	}

	public void setEnabledAncillary(Map<String, String> enabledAncillary) {
		this.enabledAncillary = enabledAncillary;
	}

}
