package com.isa.thinair.airadmin.core.web.action.flightsched;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airreservation.api.dto.OnWardConnectionDTO;
import com.isa.thinair.airreservation.api.dto.ReservationBasicsDTO;
import com.isa.thinair.airreservation.api.dto.ReservationPaxDetailsDTO;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.apache.struts2.json.JSONResult;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Results({@Result(name = AdminStrutsConstants.AdminAction.SUCCESS, type = JSONResult.class, value = "")})
public class SelectPNRToReProtectAction extends BaseRequestResponseAwareAction {

    private static final String PARAM_FLIGHT_SEG_ID = "hdnFlightSegId";
    private static final String PARAM_TO_FLIGHT_SEG_ID = "hdnNewFlightSegId";
    private static final String PARAM_LOGICAL_CABIN_CLASS_CODE = "hdnCabinClass";
    private List<ReservationBasicsDTO> pnrPaxMap = new ArrayList<ReservationBasicsDTO>();

    public String execute() throws Exception {

        String forward = AdminStrutsConstants.AdminAction.SUCCESS;
        try {

            getFlightSegPaxDetails(request);
        } catch (Exception e) {

        }
        return forward;
    }

    public void getFlightSegPaxDetails(HttpServletRequest request) throws ModuleException {
        String flightSegId = request.getParameter(PARAM_FLIGHT_SEG_ID);
        String logicalCabinClass = request.getParameter(PARAM_LOGICAL_CABIN_CLASS_CODE);
        String newFlightSegId = request.getParameter(PARAM_TO_FLIGHT_SEG_ID);
        pnrPaxMap = ModuleServiceLocator.getReservationBD().getFlightSegmentPAXDetails(new Integer(flightSegId),
                logicalCabinClass);
        FlightSegement flightSegement = ModuleServiceLocator.getFlightServiceBD().getFlightSegment(Integer.parseInt(flightSegId));
		FlightSegement newFlightSegment = ModuleServiceLocator.getFlightServiceBD()
				.getFlightSegment(Integer.parseInt(newFlightSegId.split(",")[0]));
        for (ReservationBasicsDTO dto : pnrPaxMap) {
            for (ReservationPaxDetailsDTO pax : dto.getPassengers()) {
                Collection<OnWardConnectionDTO> connectionDTOs = ModuleServiceLocator.getReservationAuxilliaryBD().getAllOutBoundSequences(
                        dto.getPnr(), pax.getPnrPaxId(), flightSegement.getSegmentCode().split("/")[0], flightSegement.getFlightId());

                dto.setMissedOnWardConnection(0);

                for (OnWardConnectionDTO onWardConnectionDTO : connectionDTOs) {
                    String[] minMaxTransitDurations = null;
                    try {

                        dto.setConnectingSector(onWardConnectionDTO.getSegmentCode());

                        minMaxTransitDurations = CommonsServices.getGlobalConfig().getMixMaxTransitDurations(onWardConnectionDTO.getSegmentCode().split("/")[0], null, null);

                        long minTime = TimeUnit.HOURS.toMillis(Integer.parseInt(minMaxTransitDurations[0].split(":")[0])) +
                                TimeUnit.MINUTES.toMillis(Integer.parseInt(minMaxTransitDurations[0].split(":")[1]));
                        long maxTime = TimeUnit.HOURS.toMillis(Integer.parseInt(minMaxTransitDurations[1].split(":")[0])) +
                                TimeUnit.MINUTES.toMillis(Integer.parseInt(minMaxTransitDurations[1].split(":")[1]));
                        Date depDate = DateUtils.addHours(onWardConnectionDTO.getDepartureDate(), Integer.parseInt(onWardConnectionDTO.getDeparturetime().substring(0, 2)));
                        depDate = DateUtils.addMinutes(depDate, Integer.parseInt(onWardConnectionDTO.getDeparturetime().substring(2)));

                        long timeDiff = depDate.getTime() - newFlightSegment.getEstTimeArrivalLocal().getTime();
                        if (timeDiff < 0 || timeDiff > maxTime || timeDiff < minTime) {
                            dto.setMissedOnWardConnection(1);
                        }
                    } catch (ModuleRuntimeException ex) {
                        if ("commons.data.transitdurations.notconfigured".equalsIgnoreCase(ex.getExceptionCode())) {
                            dto.setMissedOnWardConnection(0);
                            return;
                        }
                    }
                }
            }
        }
    }

    public List<ReservationBasicsDTO> getPnrPaxMap() {
        return pnrPaxMap;
    }

    public void setPnrPaxMap(List<ReservationBasicsDTO> pnrPaxMap) {
        this.pnrPaxMap = pnrPaxMap;
    }

}
