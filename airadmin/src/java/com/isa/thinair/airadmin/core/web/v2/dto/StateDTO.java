package com.isa.thinair.airadmin.core.web.v2.dto;

import java.io.Serializable;

public class StateDTO implements Serializable{

	private static final long serialVersionUID = -8875126025521449326L;
	
	private Integer stateId;

	private String stateCode;

	private String stateName;

	private String status;

	private String countryCode;
	
	private String airlineOfficeSTAddress1;
	
	private String airlineOfficeSTAddress2;
	
	private String airlineOfficeCity;
	
	private String airlineOfficeTaxRegNo;
	
	private String stateDigitalSignature;

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public Integer getStateId() {
		return stateId;
	}

	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getAirlineOfficeSTAddress1() {
		return airlineOfficeSTAddress1;
	}

	public void setAirlineOfficeSTAddress1(String airlineOfficeSTAddress1) {
		this.airlineOfficeSTAddress1 = airlineOfficeSTAddress1;
	}

	public String getAirlineOfficeSTAddress2() {
		return airlineOfficeSTAddress2;
	}

	public void setAirlineOfficeSTAddress2(String airlineOfficeSTAddress2) {
		this.airlineOfficeSTAddress2 = airlineOfficeSTAddress2;
	}

	public String getAirlineOfficeCity() {
		return airlineOfficeCity;
	}

	public void setAirlineOfficeCity(String airlineOfficeCity) {
		this.airlineOfficeCity = airlineOfficeCity;
	}

	public String getAirlineOfficeTaxRegNo() {
		return airlineOfficeTaxRegNo;
	}

	public void setAirlineOfficeTaxRegNo(String airlineOfficeTaxRegNo) {
		this.airlineOfficeTaxRegNo = airlineOfficeTaxRegNo;
	}

	public String getStateDigitalSignature() {
		return stateDigitalSignature;
	}

	public void setStateDigitalSignature(String stateDigitalSignature) {
		this.stateDigitalSignature = stateDigitalSignature;
	}
	
}
