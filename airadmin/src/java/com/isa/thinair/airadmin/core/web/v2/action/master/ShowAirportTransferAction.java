package com.isa.thinair.airadmin.core.web.v2.action.master;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.v2.action.common.CommonAdminRequestResponseAwareCommonAction;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airmaster.api.model.AirportTransfer;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.util.DataUtil;
import com.isa.thinair.webplatform.core.util.RowDecorator;

/**
 * 
 * @author rimaz
 * 
 */
@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ShowAirportTransferAction extends CommonAdminRequestResponseAwareCommonAction {

	private static final Log log = LogFactory.getLog(ShowAirportTransferAction.class);
	private static final int PAGE_LENGTH = 10;

	// inputs
	private String searchCode;
	private String searchStatus;

	// outputs
	private int page;
	private int total;
	private int records;
	private Collection<Map<String, Object>> rows;

	// save
	private AirportTransfer transfer = null;

	public String search() {
		try {
			init();
			int startIndex = (page - 1) * PAGE_LENGTH;
			Page<AirportTransfer> tranfers = ModuleServiceLocator.getAirportTransferServiceBD().getAirportTransfers(startIndex,
					PAGE_LENGTH, searchCode, searchStatus);
			bindData(tranfers);
		} catch (Exception e) {
			log.info("Error in searching airport transfers", e);
		}
		return S2Constants.Result.SUCCESS;

	}

	public String save() {
		try {
			if (validateAirportTransfer()) {
				ModuleServiceLocator.getAirportTransferServiceBD().saveAirportTransfers(transfer);
				setDefaultSuccessMessage();
			}

		} catch (Exception e) {
			this.handleException(e);
			log.info("Error in saving airport transfers", e);
		}

		return S2Constants.Result.SUCCESS;
	}

	// HELPER FUNCTIONS
	private void init() {
		if (searchCode == null || searchCode.isEmpty()) {
			searchCode = null;
		}
		if (searchStatus == null || searchStatus.isEmpty() || searchStatus.equals("ALL")) {
			searchStatus = null;
		}
		if (page < 1) {
			page = 1;
		}
	}

	private void bindData(Page<AirportTransfer> tranfers) {
		if (tranfers != null) {
			this.page = getCurrentPageNo(tranfers.getStartPosition());
			this.total = getTotalNoOfPages(tranfers.getTotalNoOfRecords());
			this.records = tranfers.getTotalNoOfRecords();

			this.rows = (Collection<Map<String, Object>>) DataUtil.createGridDataDecorateOnly(tranfers.getStartPosition(),
					tranfers.getPageData(), decorateRow());

		}
	}

	private RowDecorator<AirportTransfer> decorateRow() {
		RowDecorator<AirportTransfer> row = new RowDecorator<AirportTransfer>() {
			@Override
			public void decorateRow(HashMap<String, Object> row, AirportTransfer record) {
				row.put("transfer.airportCode", record.getAirportCode());
				row.put("transfer.providerName", record.getProviderName());
				row.put("transfer.providerAddess", record.getProviderAddess());
				row.put("transfer.providerNumber", record.getProviderNumber());
				row.put("transfer.providerEmail", record.getProviderEmail());
				row.put("transfer.status", record.getStatus());
				row.put("transfer.version", record.getVersion());
				row.put("transfer.createdBy", record.getCreatedBy());
				row.put("transfer.createdDate", record.getCreatedDate());
				row.put("transfer.modifiedBy", record.getModifiedBy());
				row.put("transfer.modifiedDate", record.getModifiedDate());
				row.put("transfer.airportTransferId", record.getAirportTransferId());

			}
		};
		return row;
	}

	private boolean validateAirportTransfer() throws ModuleException {
		boolean valid = true;
		if (transfer != null) {
			String airportCode = transfer.getAirportCode();
			AirportTransfer persistedTranfer = ModuleServiceLocator.getAirportTransferServiceBD().getAirportTransfer(airportCode);
			if (persistedTranfer != null) {
				Integer persistedId = persistedTranfer.getAirportTransferId();
				Integer requestId = transfer.getAirportTransferId();

				if (requestId == null || !requestId.equals(persistedId)) {
					valid = false;
					this.message = "Airport Transfer for this airport is already defined in the system, please edit the original entry";
					this.msgType = WebConstants.MSG_ERROR;
				}
			}
		}
		return valid;
	}

	private int getCurrentPageNo(int startPosition) {
		int currentPageNo = 1;
		if (PAGE_LENGTH != 0) {
			currentPageNo = startPosition / PAGE_LENGTH + 1;
		}
		return currentPageNo;
	}

	private int getTotalNoOfPages(int totoalNoOfRecords) {
		int totalNoOfPages = 1;
		if (PAGE_LENGTH != 0) {
			totalNoOfPages = (int) totoalNoOfRecords / PAGE_LENGTH;
			if (totoalNoOfRecords % PAGE_LENGTH > 0)
				totalNoOfPages++;
		}
		return totalNoOfPages;
	}

	public void setSearchCode(String searchCode) {
		this.searchCode = searchCode;
	}

	public void setSearchStatus(String searchStatus) {
		this.searchStatus = searchStatus;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public Collection<Map<String, Object>> getRows() {
		return rows;
	}

	public void setRows(Collection<Map<String, Object>> rows) {
		this.rows = rows;
	}

	public AirportTransfer getTransfer() {
		return transfer;
	}

	public void setTransfer(AirportTransfer transfer) {
		this.transfer = transfer;
	}

	@Override
	public String getMessage() {
		return message;
	}

	@Override
	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String getMsgType() {
		return msgType;
	}

	@Override
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

}
