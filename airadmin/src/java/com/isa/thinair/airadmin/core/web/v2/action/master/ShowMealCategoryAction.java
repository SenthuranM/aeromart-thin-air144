package com.isa.thinair.airadmin.core.web.v2.action.master;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.AiradminModuleConfig;
import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airadmin.core.web.v2.handler.master.MealCategoryRH;
import com.isa.thinair.airadmin.core.web.v2.util.DataUtil;
import com.isa.thinair.airmaster.api.dto.MealCategoryDTO;
import com.isa.thinair.airmaster.api.dto.MealCategorySearchDTO;
import com.isa.thinair.airmaster.api.model.MealCategory;
import com.isa.thinair.commons.api.dto.ModuleCriteria;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.MessagesUtil;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * Action Class for handling the meal category maintenance.
 * 
 * @author sanjaya
 */
@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ShowMealCategoryAction {

	/** Class logger **/
	private static Log log = LogFactory.getLog(ShowMealAction.class);

	/** **/
	private Object[] rows;

	/** **/
	private int page;

	/** **/
	private int total;

	/** **/
	private int records;

	/** **/
	private String succesMsg;

	/** **/
	private String msgType;

	/** **/
	private String mealCategoryName;

	/** **/
	private long version;

	/** **/
	private String createdBy;

	/** **/
	private Date createdDate;

	/** **/
	private Integer mealCategoryId;

	/** **/
	private String uploadImage;

	/** **/
	private String uploadImageThumbnail;

	/** **/
	private String hdnMode;

	/** **/
	private MealCategorySearchDTO mealCategorySrch;

	/** **/
	private String mealCatDescForDisplay;

	/** **/
	private static AiradminConfig airadminConfig = new AiradminConfig();

	/**
	 * 
	 * @return
	 */
	public String execute() {

		MealCategoryDTO savedMealCatDTO = null;
		MealCategoryDTO mealCatDTO = new MealCategoryDTO();
		mealCatDTO.setMealCategoryName(mealCategoryName);
		mealCatDTO.setVersion(version);
		mealCatDTO.setCreatedBy(createdBy);
		mealCatDTO.setCreatedDate(createdDate);
		mealCatDTO.setMealCategoryId(mealCategoryId);
		mealCatDTO.setSelectedLanguageTranslations(mealCatDescForDisplay);

		if (this.hdnMode != null
				&& ((hdnMode.equals(WebConstants.ACTION_ADD)) || (hdnMode.equals(WebConstants.ACTION_EDIT)) || (hdnMode
						.equals(WebConstants.ACTION_SAVE_LIST_FOR_DISPLAY)))) {
			try {
				MealCategoryRH.saveMealCategory(mealCatDTO);

				if (this.uploadImage != null && this.uploadImage.trim().equalsIgnoreCase("UPLOADED")) {
					AiradminModuleConfig adminCon = AiradminModuleUtils.getConfig();
					String mountPath = adminCon.getMealImages();
					String fullFileName = mountPath + "mealCategoryImage_test.jpg";
					File tmpFile = new File(fullFileName);
					String imageFile = mountPath + "mealCategory_" + mealCatDTO.getMealCategoryId() + ".jpg"; // no
																												// need
																												// to
																												// add
																												// the
																												// suffix
					File dataFile = new File(imageFile);
					FileUtils.copyFile(tmpFile, dataFile);
				}

				if (this.uploadImageThumbnail != null && this.uploadImageThumbnail.trim().equalsIgnoreCase("UPLOADED")) {
					AiradminModuleConfig adminCon = AiradminModuleUtils.getConfig();
					String mountPath = adminCon.getMealImages();
					String fullFileName = mountPath + "mealThumbnailImage_test.jpg";
					File tmpFile = new File(fullFileName);
					String imageFile = mountPath + "mealCategory_Thumbnail_" + mealCatDTO.getMealCategoryId() + ".jpg"; // no
																														// need
																														// to
																														// add
																														// the
																														// suffix
					File dataFile = new File(imageFile);
					FileUtils.copyFile(tmpFile, dataFile);
				}
				this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS);
				this.msgType = WebConstants.MSG_SUCCESS;

			} catch (ModuleException e) {
				this.succesMsg = e.getMessageString();
				if (e.getMessageString().equals(MessagesUtil.getMessage("module.duplicate.key"))) {
					this.succesMsg = airadminConfig.getMessage("um.meal.form.mealcode.dulply");
				}
				this.msgType = WebConstants.MSG_ERROR;
			} catch (Exception ex) {
				this.succesMsg = "System Error Please Contact Administrator";
				this.msgType = WebConstants.MSG_ERROR;
				if (log.isErrorEnabled()) {
					log.error(ex);
				}
			}
		} else {
			savedMealCatDTO = mealCatDTO;
		}

		this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS);
		this.msgType = WebConstants.MSG_SUCCESS;

		return S2Constants.Result.SUCCESS;
	}

	public String deleteMealCategory() {

		try {
			// TODO - remove meal category translations
			// ModuleServiceLocator.getTranslationBD().removeMeals(mealId);
			MealCategoryRH.deleteMealCategory(mealCategoryId);
			this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_DELETE_SUCCESS);
			this.msgType = WebConstants.MSG_SUCCESS;
		} catch (ModuleException e) {
			this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_DELETE_FAIL);
			if (e.getMessageString().equals(MessagesUtil.getMessage("module.constraint.childrecord"))) {
				this.succesMsg = airadminConfig.getMessage("um.airadmin.childrecord");
			}

			this.msgType = WebConstants.MSG_ERROR;
		}

		return S2Constants.Result.SUCCESS;
	}

	public String searchMealCategory() {

		try {
			List<ModuleCriterion> critrian = new ArrayList<ModuleCriterion>();
			if (mealCategorySrch != null) {
				if (mealCategorySrch.getMealCategoryName() != null && !mealCategorySrch.getMealCategoryName().trim().equals("")) {
					ModuleCriteria criteriaMLName = new ModuleCriteria();
					List<String> lstmealCategoryName = new ArrayList<String>();
					criteriaMLName.setFieldName("mealCategoryName");
					criteriaMLName.setCondition(ModuleCriteria.CONDITION_LIKE_IGNORECASE);
					lstmealCategoryName.add("%" + mealCategorySrch.getMealCategoryName() + "%");
					criteriaMLName.setValue(lstmealCategoryName);
					critrian.add(criteriaMLName);
				}
			}
			Page pgmealCategory = MealCategoryRH.searchMealCategory(this.page, critrian);
			this.records = pgmealCategory.getTotalNoOfRecords();
			this.total = pgmealCategory.getTotalNoOfRecords() / 20;
			int mod = pgmealCategory.getTotalNoOfRecords() % 20;
			if (mod > 0)
				this.total = this.total + 1;
			if (this.page <= 0)
				this.page = 1;
			Collection<MealCategory> colMealCategory = pgmealCategory.getPageData();
			String strMeals = null;
			if (colMealCategory != null) {
				Object[] dataRow = new Object[colMealCategory.size()];
				int index = 1;
				Iterator<MealCategory> iter = colMealCategory.iterator();
				while (iter.hasNext()) {
					Map<String, Object> counmap = new HashMap<String, Object>();

					MealCategory grdMealCategory = (MealCategory) iter.next();

					MealCategoryDTO jSonMealCategory = new MealCategoryDTO();

					counmap.put("id", new Integer(((page - 1) * 20) + index).toString());

					jSonMealCategory.setMealCategoryId(grdMealCategory.getMealCategoryId());
					jSonMealCategory.setMealCategoryName(grdMealCategory.getMealCategoryName());
					jSonMealCategory.setVersion(grdMealCategory.getVersion());
					jSonMealCategory.setCreatedBy(grdMealCategory.getCreatedBy());
					jSonMealCategory.setCreatedDate(grdMealCategory.getCreatedDate());
					jSonMealCategory.setModifiedBy(grdMealCategory.getModifiedBy());
					jSonMealCategory.setModifiedDate(grdMealCategory.getModifiedDate());

					String langTraStr = "";

					try {
						if (grdMealCategory.getI18nMessageKey() != null) {
							langTraStr = DataUtil.convertLanguageTranslations(grdMealCategory.getI18nMessageKey()
									.getI18nMessages());
						}
					} catch (Exception e) {
						langTraStr = "";
					}

					counmap.put("mealCategory", jSonMealCategory);
					counmap.put("mealCatDescriptionForDisplay", langTraStr); // language
																				// translation

					dataRow[index - 1] = counmap;
					index++;
				}

				this.rows = dataRow;
			}

		} catch (Exception ex) {
			this.succesMsg = "System Error Please Contact Administrator";
			this.msgType = WebConstants.MSG_ERROR;
			if (log.isErrorEnabled()) {
				log.error(ex);
			}
		}

		return S2Constants.Result.SUCCESS;
	}

	/**
	 * @return
	 */
	public Object[] getRows() {
		return rows;
	}

	/**
	 * @param rows
	 */
	public void setRows(Object[] rows) {
		this.rows = rows;
	}

	/**
	 * @return
	 */
	public int getPage() {
		return page;
	}

	/**
	 * @param page
	 */
	public void setPage(int page) {
		this.page = page;
	}

	/**
	 * @return
	 */
	public int getTotal() {
		return total;
	}

	/**
	 * @param total
	 */
	public void setTotal(int total) {
		this.total = total;
	}

	/**
	 * @return
	 */
	public int getRecords() {
		return records;
	}

	/**
	 * @param records
	 */
	public void setRecords(int records) {
		this.records = records;
	}

	/**
	 * @return
	 */
	public String getSuccesMsg() {
		return succesMsg;
	}

	/**
	 * @param succesMsg
	 */
	public void setSuccesMsg(String succesMsg) {
		this.succesMsg = succesMsg;
	}

	/**
	 * @return
	 */
	public String getMsgType() {
		return msgType;
	}

	/**
	 * @param msgType
	 */
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	/**
	 * @return
	 */
	public String getMealCategoryName() {
		return mealCategoryName;
	}

	/**
	 * @param mealCategoryName
	 */
	public void setMealCategoryName(String mealCategoryName) {
		this.mealCategoryName = mealCategoryName;
	}

	/**
	 * @return
	 */
	public long getVersion() {
		return version;
	}

	/**
	 * @param version
	 */
	public void setVersion(long version) {
		this.version = version;
	}

	/**
	 * @return
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return
	 */
	public Integer getMealCategoryId() {
		return mealCategoryId;
	}

	/**
	 * @param mealCategoryId
	 */
	public void setMealCategoryId(Integer mealCategoryId) {
		this.mealCategoryId = mealCategoryId;
	}

	/**
	 * @return
	 */
	public String getUploadImage() {
		return uploadImage;
	}

	/**
	 * @param uploadImage
	 */
	public void setUploadImage(String uploadImage) {
		this.uploadImage = uploadImage;
	}

	/**
	 * @return
	 */
	public String getUploadImageThumbnail() {
		return uploadImageThumbnail;
	}

	/**
	 * @param uploadImageThumbnail
	 */
	public void setUploadImageThumbnail(String uploadImageThumbnail) {
		this.uploadImageThumbnail = uploadImageThumbnail;
	}

	/**
	 * @return
	 */
	public String getHdnMode() {
		return hdnMode;
	}

	/**
	 * @param hdnMode
	 */
	public void setHdnMode(String hdnMode) {
		this.hdnMode = hdnMode;
	}

	/**
	 * @return
	 */
	public MealCategorySearchDTO getMealCategorySrch() {
		return mealCategorySrch;
	}

	/**
	 * @param mealCategorySrch
	 */
	public void setMealCategorySrch(MealCategorySearchDTO mealCategorySrch) {
		this.mealCategorySrch = mealCategorySrch;
	}

	/**
	 * @return
	 */
	public String getMealCatDescForDisplay() {
		return mealCatDescForDisplay;
	}

	/**
	 * @param hdnMode
	 */
	public void setMealCatDescForDisplay(String mealCatDescForDisplay) {
		this.mealCatDescForDisplay = mealCatDescForDisplay;
	}
}
