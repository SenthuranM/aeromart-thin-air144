package com.isa.thinair.airadmin.core.web.generator.master;

import java.util.Collection;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airmaster.api.model.Territory;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author srikantha
 * 
 */
public class TerritoryHTMLGenerator {

	private static Log log = LogFactory.getLog(TerritoryHTMLGenerator.class);

	private static String clientErrors;

	private Properties props;

	private static final String PARAM_SEARCHDATA = "hdnSearchData";

	private static final String PARAM_SEARCH_TERRITORYID = "selTerritoryID";

	private static final String PARAM_SEARCH_TERRITORYDESC = "selTerritoryDesc";

	/**
	 * Construct HG Using Property file Values
	 * 
	 * @param props
	 *            the Properties file containg Request values
	 */
	public TerritoryHTMLGenerator(Properties props) {
		this.props = props;
	}

	/**
	 * Gets the Territory Grid Data Array for the Record No
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Territory Grid Array
	 * @throws ModuleException
	 *             the ModuleException
	 */
	@SuppressWarnings("unchecked")
	public final String getTerritoryRowHtml(HttpServletRequest request) throws ModuleException {

		log.debug("inside TerritoryHTMLGenerator.getTerritoryRowHtml()");
		List<Territory> list = null;

		String strSearchData = props.getProperty(PARAM_SEARCHDATA);

		request.setAttribute("reqTerritorySearchData", strSearchData);

		Territory territorySearchDTO = new Territory();

		if (!props.getProperty(PARAM_SEARCH_TERRITORYID).trim().equals("")
				&& !props.getProperty(PARAM_SEARCH_TERRITORYID).trim().equalsIgnoreCase("ALL")) {
			territorySearchDTO.setTerritoryCode(props.getProperty(PARAM_SEARCH_TERRITORYID));
		}

		if (!props.getProperty(PARAM_SEARCH_TERRITORYDESC).trim().equals("")
				&& !props.getProperty(PARAM_SEARCH_TERRITORYDESC).trim().equalsIgnoreCase("ALL")) {
			territorySearchDTO.setDescription(props.getProperty(PARAM_SEARCH_TERRITORYDESC));
		}

		int recordNo = 0;
		if (request.getParameter("hdnRecNo") != null && !request.getParameter("hdnRecNo").equals("")) {
			recordNo = Integer.parseInt(request.getParameter("hdnRecNo")) - 1;
		}

		Page page = null;

		int totalRecords = 0;

		String searchDataArr[] = null;
		if (strSearchData != null && !strSearchData.equals("")) {
			searchDataArr = strSearchData.split(",");
			for (int i = 0; i < searchDataArr.length; i++) {
				if (searchDataArr[i].trim().equalsIgnoreCase("*")) {
					searchDataArr[i] = null;
				}
			}
		}

		// If All option selected return all the countries
		if (strSearchData == null || strSearchData.equals("")
				|| ((strSearchData != "") && (searchDataArr[0] == null && searchDataArr[1] == null)))
			page = ModuleServiceLocator.getLocationServiceBD().getTerritorys(recordNo, 20, null, null);
		else
			page = ModuleServiceLocator.getLocationServiceBD().getTerritorys(recordNo, 20, searchDataArr[0], searchDataArr[1]);

		String strJavascriptTotalNoOfRecs = "";
		totalRecords = page.getTotalNoOfRecords();
		strJavascriptTotalNoOfRecs = "var totalRecords = " + totalRecords + ";";
		request.setAttribute(WebConstants.REQ_TOTAL_RECORDS, strJavascriptTotalNoOfRecs);

		list = (List<Territory>) page.getPageData();
		log.debug("inside TerritoryHTMLGenerator.getTerritoryRowHtml SEARCH else condition()");
		return createTerritoryRowHTML(list);
	}

	/**
	 * Create the Grid row for Territories from a Collection
	 * 
	 * @param territories
	 *            the Collection of territories
	 * @return String the Created Grid array for Territories
	 */
	private String createTerritoryRowHTML(Collection<Territory> territories) {
		List<Territory> list = (List<Territory>) territories;
		Object[] listArr = list.toArray();
		StringBuffer sb = new StringBuffer("var arrData = new Array();");

		Territory territory = null;

		for (int i = 0; i < listArr.length; i++) {
			territory = (Territory) listArr[i];
			sb.append("arrData[" + i + "] = new Array();");
			sb.append("arrData[" + i + "][1] = '" + territory.getTerritoryCode() + "';");
			sb.append("arrData[" + i + "][2] = '" + territory.getDescription() + "';");

			if (territory.getRemarks() != null)
				sb.append("arrData[" + i + "][3] = '" + territory.getRemarks() + "';");
			else
				sb.append("arrData[" + i + "][3] = '';");

			sb.append("arrData[" + i + "][4] = '" + AiradminUtils.getModifiedStatus(territory.getStatus()) + "';");
			sb.append("arrData[" + i + "][6] = '" + territory.getVersion() + "';");
			
			sb.append("arrData[" + i + "][7] = '" + AiradminUtils.getNotNullString(territory.getBank()) + "';");
			sb.append("arrData[" + i + "][8] = '" + AiradminUtils.getNotNullString(territory.getAccountNumber()) + "';");
			sb.append("arrData[" + i + "][9] = '" + AiradminUtils.getNotNullString(territory.getSwiftCode()) + "';");
			sb.append("arrData[" + i + "][10] = '" + AiradminUtils.getNotNullString(territory.getBankAddress()) + "';");
			sb.append("arrData[" + i + "][11] = '" + AiradminUtils.getNotNullString(territory.getIBANCode()) + "';");
			sb.append("arrData[" + i + "][12] = '" + AiradminUtils.getNotNullString(territory.getBeneficiaryName()) + "';");
		}
		return sb.toString();
	}

	/**
	 * Sets the Client Validations for Territory Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array Containing the Client Validations
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {
		if (clientErrors == null) {
			Properties moduleErrs = new Properties();

			moduleErrs.setProperty("um.territory.form.id.required", "territoryIdRqrd");
			moduleErrs.setProperty("um.territory.form.description.required", "territoryDescRqrd");
			moduleErrs.setProperty("um.territory.form.id.already.exist", "territoryIdExist");
			moduleErrs.setProperty("um.airadmin.delete.confirmation", "deleteConf");

			clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}
		return clientErrors;
	}
}
