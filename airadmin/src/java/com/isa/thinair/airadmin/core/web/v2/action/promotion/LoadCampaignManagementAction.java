package com.isa.thinair.airadmin.core.web.v2.action.promotion;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.promotion.core.util.PromotionModuleServiceLocator;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class LoadCampaignManagementAction {
	
	private static Log log = LogFactory.getLog(LoadCampaignManagementAction.class);
	
	private List<String[]> airportsList;
	
	private String locationsList;
	
	private List<String> promoCodeList;
	
	private int promoCriteriaId;

	public String execute() {
		try {
			if (promoCriteriaId != 0) {
				promoCodeList = ModuleServiceLocator.getPromotionCriteriaAdminBD().getPromoCodesByID(promoCriteriaId);
			}
			airportsList = SelectListGenerator.createActiveAirportCodes();
			locationsList = SelectListGenerator.createCountryList();
		} catch (Exception e) {
			log.error("execute ", e);
		}

		return S2Constants.Result.SUCCESS;
	}

	public List<String[]> getAirportsList() {
		return airportsList;
	}

	public void setAirportsList(List<String[]> airportsList) {
		this.airportsList = airportsList;
	}
	
	public String getLocationsList() {
		return locationsList;
	}

	public void setLocationsList(String locationsList) {
		this.locationsList = locationsList;
	}


	public int getPromoCriteriaId() {
		return promoCriteriaId;
	}


	public void setPromoCriteriaId(int promoCriteriaId) {
		this.promoCriteriaId = promoCriteriaId;
	}


	public List<String> getPromoCodeList() {
		return promoCodeList;
	}


	public void setPromoCodeList(List<String> promoCodeList) {
		this.promoCodeList = promoCodeList;
	}
	
}
