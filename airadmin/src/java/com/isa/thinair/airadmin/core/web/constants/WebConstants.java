package com.isa.thinair.airadmin.core.web.constants;

/**
 * @author srikantha
 */

public final class WebConstants {
	
	public static final String DEFAULT_LANGUAGE = "en";

	public static final String ACTION_FORWARD_SUCCESS = "success";
	public static final String ACTION_FORWARD_REPROTECT = "reprotect";
	public static final String ACTION_GRID = "grid";

	public static final String REQ_HTML_ROWS = "reqHtmlRows";
	public static final String REQ_HTML_DETAILS = "reqHtmlDetails";
	public static final String REQ_MESSAGE = "reqMessage";
	public static final String REQ_POPUPMESSAGE = "reqPopupMessage";
	public static final String SES_CURR_USER = "sesCurrUser";
	public static final String SES_PRIVILEGE_IDS = "sesPrivilegeIds";
	public static final String SES_MAIN_MENUSCRIPT = "sesMainMenuscript";
	public static final String GDS_AUTO_PUBLISHED_ROUTES = "autoPublishedRoutes";
	public static final String GDS_AUTO_PUBLISHED_CARRIERS = "autoPublishedRoutes";

	public static final String KEY_SAVE_FAIL = "um.airadmin.save.fail";
	public static final String KEY_SAVE_SUCCESS = "um.airadmin.save.success";
	public static final String KEY_LOAD_FAIL = "um.airadmin.load.fail";
	public static final String KEY_LOAD_SUCCESS = "um.airadmin.load.success";
	public static final String KEY_ADD_SUCCESS = "um.airadmin.add.success";
	public static final String KEY_DELETE_SUCCESS = "um.airadmin.delete.success";
	public static final String KEY_DELETE_FAIL = "um.airadmin.delete.fail";
	public static final String KEY_UPDATE_SUCCESS = "um.airadmin.update.success";
	public static final String KEY_SPLIT_SUCCESS = "um.airadmin.split.success";
	public static final String KEY_BUILD_SUCCESS = "um.airadmin.build.success";
	public static final String KEY_COPY_SUCCESS = "um.airadmin.copy.success";
	public static final String KEY_DISABLE_ROLLBACK_SUCCESS = "um.airadmin.disable.rollback.success";
	public static final String KEY_SEND_ALERTS_SUCCESS = "um.airadmin.send.alerts.success";
	public static final String KEY_SEND_EMAIL_SUCCESS = "um.airadmin.send.emails.success";
	public static final String KEY_SEND_ALERTS_EMAILS_SUCCESS = "um.airadmin.send.alerts.emails.success";
	public static final String KEY_GENERATE_SUCCESS = "um.airadmin.generate.success";
	public static final String KEY_TRANSFER_SUCCESS = "um.airadmin.transfer.success";
	public static final String KEY_TRANSFER_FAILED = "um.airadmin.transfer.failed";
	public static final String KEY_REPRINT_SUCCESS = "um.airadmin.reprint.success";
	public static final String KEY_PFS_PROCESS_SUCCESS = "um.airadmin.pfs.process.success";
	public static final String KEY_SENDNEW_PNL_SUCCESS = "um.airadmin.sendnew.pnl.success";
	public static final String KEY_SENDNEW_ADL_SUCCESS = "um.airadmin.sendnew.adl.success";
	public static final String KEY_RESEND_PNL_SUCCESS = "um.airadmin.resend.pnl.success";
	public static final String KEY_RESEND_ADL_SUCCESS = "um.airadmin.resend.adl.success";
	public static final String KEY_PRINT_SUCCESS = "um.airadmin.print.success";
	public static final String KEY_REPROTECT_SUCCESS = "um.airadmin.reprotect.success";
	public static final String KEY_REPROTECT_NOT_SUCCESS = "um.airadmin.reprotect.not.success";
	public static final String KEY_ROLLFORWARD_SUCCESS = "um.airadmin.rollforward.success";
	public static final String KEY_ROLLFORWARD_NOT_SUCCESS = "um.airadmin.rollforward.not.success";

	public static final String KEY_SCHEDULE_SAVE_SUCCESS = "um.airadmin.schedule.save.success";
	public static final String KEY_SCHEDULE_DELETE_SUCCESS = "um.airadmin.schedule.delete.success";
	public static final String KEY_SCHEDULE_UPDATE_SUCCESS = "um.airadmin.schedule.update.success";
	public static final String KEY_SCHEDULE_SPLIT_SUCCESS = "um.airadmin.schedule.split.success";
	public static final String KEY_SCHEDULE_BUILD_SUCCESS = "um.airadmin.schedule.build.success";
	public static final String KEY_SCHEDULE_COPY_SUCCESS = "um.airadmin.schedule.copy.success";
	public static final String KEY_SCHEDULE_GDS_PUBLISH_SUCCESS = "um.airadmin.schedule.gds.publish.success";
	public static final String KEY_SCHEDULE_GDS_UNPUBLISH_SUCCESS = "um.airadmin.schedule.gds.unpublish.success";

	public static final String KEY_PRL_PROCESS_SUCCESS = "um.airadmin.prl.process.success";

	/*
	 * Error Handling Server
	 */
	public static final String REQ_ERROR_SERVER_MESSAGES = "reqServerErrors";
	public static final String REQ_ERROR_REDIRECT = "reqErrorRedirect";
	public static final String REQ_ERROR_FRAME = "reqErrorFrame";
	/*
	 * Login
	 */
	public static final String REQ_HTML_LOGIN_STATUS = "logStatus";
	public static final String REQ_HTML_LOGIN_UID = "logUID";

	public static final String FORWARD_SUCCESS = "success";
	public static final String FORWARD_ERROR = "error";
	public static final String CANCEL_TRUE = "true";
	public static final String REQ_CANCEL_TRUE = "reqcancel";

	// Time modes
	public static final String LOCALTIME = "LOCAL";
	public static final String ZULUTIME = "ZULU";
	public static final String DURATIONERROR = "durationError";
	public static final String HASDURATIONERROR = "hasdurationError";
	public static final String POPUPMESSAGE = "popupmessage";

	/*
	 * Menu Related
	 */
	public static final String REQ_HTML_MENU_DATA = "menuList";
	public static final String REQ_HTML_PWD_RESETED = "userPwdReseted";
	public static final String REQ_HTML_SHOW_RESET_MSG = "pwdResetMsg";

	// User message types
	public static final String MSG_TYPE = "reqMsgType";

	public static final String MSG_SUCCESS = "Confirmation";
	public static final String MSG_ERROR = "Error";
	public static final String MSG_WARNING = "Warning";

	public static final String PARAM_HDN_ACTION = "hdnAction";

	public static final String SUCCESS = "SUCCESS";
	public static final String FAIL = "FAIL";
	public static final String ACTION_LOAD = "LOAD";
	public static final String ACTION_ADD = "ADD";
	public static final String ACTION_UPDATE = "UPDATE";
	public static final String ACTION_EDIT = "EDIT";
	public static final String ACTION_EDITLEG = "EDITLEG";
	public static final String ACTION_ADD_UPDATE = "ADD.UPDATE";
	public static final String ACTION_SAVE = "SAVE";
	public static final String ACTION_SYNC_ALL = "SYNC_ALL";
	public static final String ACTION_UPLOAD = "UPLOAD";
	public static final String ACTION_SAVE_LIST_FOR_DISPLAY = "SAVELISTFORDISP"; // Haider
	public static final String ACTION_EXPORT = "EXPORT";
	public static final String ACTION_DELETE = "DELETE";
	public static final String ACTION_DELETE_ALL = "DELETEALL";
	public static final String ACTION_FLIGHT = "FLIGHT";
	public static final String ACTION_ROWCLICK = "rowClick";
	public static final String ACTION_SEARCH = "SEARCH";
	public static final String ACTION_CANCEL = "CANCEL";
	public static final String ACTION_BUILD = "BUILD";
	public static final String ACTION_COPY = "COPY";
	public static final String ACTION_SPLIT = "SPLIT";
	public static final String ACTION_GRID_CLICKED = "GRIDCLICKED";
	public static final String ACTION_UPDATE_CANCEL = "UPDATE.CANCEL";
	public static final String ACTION_UPDATE_CANCELLEG = "UPDATE.CANCELLEG";
	public static final String ACTION_CANCEL_FORCE = "CANCELFORCE";
	public static final String ACTION_UPDATE_CANCEL_REPROTECT = "UPDATE.CANCEL.REPROTECT";
	public static final String ACTION_CANCEL_REPROTECT_PAX = "CANCEL.REPROTECT.PAX";
	public static final String ACTION_CONFIRM_CANCEL = "CONFIRM.CANCEL";
	public static final String ACTION_FORM_RELOAD = "RELOAD";
	public static final String ACTION_UPDATE_REPROTECT = "UPDATEREPROTECT";
	public static final String ACTION_DISPLAY = "DISPLAY";
	public static final String ACTION_GENERATE = "GENERATE";
	public static final String ACTION_REPRINT = "REPRINT";
	public static final String ACTION_PROCESS = "PROCESS";
	public static final String ACTION_UPDATE_CONTENT = "UPDATE";
	public static final String ACTION_TRANSFER = "TRANSFER";
	public static final String ACTION_EMAIL = "EMAIL";
	public static final String ACTION_CONFIRM = "CONFIRM";
	public static final String ACTION_CONFIRM_REPROTECT = "CONFIRM.REPROTECT";
	public static final String ACTION_ROLLFORWARD = "ROLLFORWARD";
	public static final String ACTION_DISABLE_ROLLBACK = "DISABLEROLLBACK";
	public static final String ACTION_NEWPNL = "NEWPNL";
	public static final String ACTION_NEWADL = "NEWADL";
	public static final String ACTION_RESENDPNL = "RESENDPNL";
	public static final String ACTION_RESENDADL = "RESENDADL";
	public static final String ACTION_PRINT = "PRINT";
	public static final String ACTION_REPRINT_PNL = "PNLREPRINT";
	public static final String ACTION_REPRINT_ADL = "ADLREPRINT";
	public static final String ACTION_PRINT_NEW_PNL = "NEWPNLPRINT";
	public static final String ACTION_PRINT_NEW_ADL = "NEWADLPRINT";
	public static final String ACTION_RESEND_PNRGOV = "SENDPNRGOV";
	public static final String ACTION_ACTIVATE_PAST_FLIGHT = "ACTIVATEPASTFLIGHTS";

	public static final String ACTION_SEATALLOCATION = "SEATALLOCATION";
	public static final String ACTION_VIEW = "VIEW";
	public static final String ACTION_DETAIL = "DETAIL";
	public static final String ACTION_DETAILO = "DETAILO";
	public static final String ACTION_PARSE = "PARSE";

	public static final String REQ_SYSTEM_DATE = "reqSystemDate";

	public static final String REQ_FORM_FIELD_VALUES = "reqFormFieldValues";

	public static final String RES_DEFAULT_CARRIER_NAME = "reqCarrName";

	public static final String RES_DEFAULT_AIRLINE_ID_CODE = "defaultAirlineId";

	// Select HTML control constants
	public static final String REQ_HTML_COUNTRY_LIST = "reqCountryList";
	public static final String REQ_HTML_LANGUAGE_LIST = "reqLanguageList";
	public static final String REQ_HTML_COUNTRY_COMBO = "reqCountryCombo";
	public static final String REQ_HTML_TERRITORY_LIST = "reqTerritoryList";
	public static final String REQ_HTML_TERRITORY_COMBO = "reqTerritoryCombo";
	public static final String REQ_HTML_AIRPORT_LIST = "reqAirportList";
	public static final String REQ_HTML_ADMIN_FEE_AGENT_LIST = "reqAdminFeeAgentList";
	public static final String REQ_HTML_ACTIVE_AIRPORT_LIST = "reqActiveAirportList";
	public static final String REQ_HTML_AIRPORT_CODENAME_LIST = "reqAirportCodeNameList";
	public static final String REQ_HTML_PARAM_CC_LIST = "reqParamCCList";
	public static final String REQ_HTML_PARAM_NAME_LIST = "reqParamNameList";
	public static final String REQ_HTML_PARAM_KEY_LIST = "reqParamKeyList";
	public static final String REQ_HTML_PARAM_TYPE_LIST = "reqParamTypeList";
	public static final String REQ_HTML_AIRPORT_COMBO = "reqAirportCombo";
	public static final String REQ_HTML_CURRENCY_COMBO = "reqCurrencyCombo";
	public static final String REQ_HTML_AIRPORTDST_LIST = "reqAirportDstList";
	public static final String REQ_HTML_AIRPORT_TERRITORY_LIST = "reqStatTerList";
	public static final String REQ_HTML_MAILSERVER_LIST = "reqMailServerList";
	public static final String REQ_IS_EXCEPTION = "reqIsExceptionOccured";
	public static final String REQ_HTML_SCHED_PUBLISH_MECHANISM = "reqPublishMechList"; // Haider
																						// 14Oct08
	public static final String REQ_HTML_COUNTRY_STATE_LIST = "reqCountryStateList";
	public static final String REQ_HTML_COUNTRY_STATE_ARRAY_FOR_MULTI_SELECT = "reqCountryStateArrayForMultiSelect";
	public static final String REQ_HTML_SSR_CATEGORY_LIST = "reqSSRCategoryList"; // Dhanya
																					// 10/03/2009
	public static final String LIST_FOR_DISPLAY_ENABLED = "reqListForDisplayEnabled";
	public static final String FOC_MEAL_SEL_ENABLED = "reqFOCMealSelEnable";

	public static final String REQ_HTML_BSPSTATUS_LIST = "reqBSPStatusList";
	public static final String REQ_HTML_BSPAGENT_COUNTRYCODE_LIST = "reqBSPAgentCountryCodeList";
	public static final String REQ_BSP_ENABLED_FOR_AIRLINE = "reqBSPEnableForAirLine";

	public static final String REQ_HTML_CURRENCY_LIST = "reqCurrencyList";
	public static final String REQ_HTML_REGION_LIST = "reqRegionList";

	public static final String REQ_HTML_CLASSOFSERVICES_LIST = "reqClassOfServicesList";
	public static final String REQ_HTML_BOOKINGCODES_LIST = "reqBookingCodesList";
	public static final String REQ_HTML_LOGICAL_CABIN_CLASS_LIST = "reqLogicalCabinClassList";
	public static final String CC_WISE_LOGICAL_CABIN_CLASS_LIST = "ccWiseLogicalCabinClassList";
	public static final String REQ_HTML_BOOKINGCODES_BYCCCODE_LIST = "reqBookingCodesByCCCodeList";
	public static final String REQ_HTML_BOOKINGCODES_TYPES_LIST = "reqBCTypeList";
	public static final String REQ_HTML_BOOKING_CLASS_TYPES_LIST = "reqBookingClassTypeList";
	public static final String REQ_HTML_ALLOCATION_TYPES_LIST = "reqAllocationTypeList";

	public static final String REQ_HTML_AGENTTYPE_LIST = "reqAgentTypeList";
	public static final String REQ_HTML_CMP_PAY_AGENTSTATUS_LIST = "reqCmpPayAgentStatusList";
	public static final String REQ_HTML_AGENTSTATUS_LIST = "reqAgentStatusList";
	public static final String REQ_HTML_SERVICE_CHANNELS = "reqServiceChannels";
	public static final String REQ_CREDIT_HISTORY_ROWS = "reqCreditHistoryRow";
	public static final String REQ_ADJUST_PAYROWS = "reqAdjustedPayment";
	public static final String REQ_HTML_GSA_LIST = "reqGSAList";
	public static final String REQ_HTML_AGENT_USER_LIST = "reqAgentUserList";
	public static final String REQ_HTML_ROLE_LIST = "reqRoleList";
	public static final String REQ_HTML_ROLE_PRIVILEGE_LIST = "reqRolePrivilegeList";
	public static final String REQ_HTML_GSA_COMBO = "reqGSACombo";
	public static final String REQ_HTML_GSATERRITORY_LIST = "reqGSATerritoryList";
	public static final String REQ_HTML_AGENT_LIST = "reqAgentList";
	public static final String REQ_HTML_AGENT_COMBO = "reqAgentCombo";
	public static final String REQ_HTML_UNASSIGNED_USER_LIST = "reqUnAssignedUserList";
	public static final String REQ_HTML_UNASSIGNED_USER_ARRAY = "reqUnAssignedUserArray";
	public static final String ACTION_ASSIGNUSER = "assignUser";
	public static final String REQ_HTML_PAYMENTTYPE_LIST = "reqPaymentType";
	public static final String REQ_HTML_STN_LIST = "reqStationList";
	public static final String REQ_HTML_ANCI_LIST = "reqAncillaryList";
	public static final String REQ_HTML_PAYMENTPURPOSE_LIST = "reqPaymentPurpose";
	public static final String REQ_HTML_RECEIPT_ARRAY = "reqReceiptArray";
	public static final String REQ_HTML_INVOICE_ARRAY = "reqInvoiceArray";
	public static final String REQ_INVOICEPERIOD = "reqInvPeriods";
	public static final String REQ_IS_WEEKLYINVOICE_ENABLED = "isWeeklyInvoiceEnabled";
	public static final String REQ_HTML_RECEIPT_DETAILS = "reqReceiptDetails";
	public static final String REQ_HTML_INVOICE_ROWS = "reqInvoiceRows";
	public static final String REQ_HTML_COUNTTRY_CURR = "Country Currency";
	public static final String REQ_HTML_INVOIVEGEN_DETAILS = "reqinvoiceGenDetails";
	public static final String REQ_HTML_SALES_CHANNEL_LIST = "reqSalesChannel";
	public static final String REQ_GSA_V2 = "reqGSAStructureV2Enabled";

	public static final String REQ_HTML_CABINCLASS_LIST = "reqCabinClassList";
	public static final String REQ_HTML_BUNDLEDESC_TEMPLATE_LIST = "reqBundleDescTemplateList";
	public static final String REQ_HTML_LOGICALCABINCLASS_LIST = "reqLogicalCabinClassList";
	public static final String REQ_HTML_LOGICALCABINCLASS_SIZE = "reqLogicalCabinClassSize";

	public static final String REQ_HTML_OND_LIST_DATA = "reqOndListData";
	public static final String REQ_HTML_FLIGHTNO_LIST_DATA = "reqFlightListData";
	public static final String REQ_HTML_FLIGHTNO_ORIGIN_LIST_DATA = "reqFlightWithOriginListData";
	public static final String SES_HTML_ONLINE_ACTIVE_AIRPORTCODE_LIST_DATA = "sesOnlineActiveAirportCodeListData";
	public static final String SES_HTML_ONLINE_AIRPORTCODE_LIST_DATA = "sesOnlineAirportCodeListData";
	public static final String SES_HTML_OPERATIONTYPE_LIST_DATA = "sesOperationTypeListData";
	public static final String SES_HTML_OPERATIONTYPE_DETAILS = "sesOperationTypeDetails";
	public static final String SES_HTML_STATUS_LIST_DATA = "sesStatusListData";
	public static final String SES_HTML_GDS_LIST_DATA = "sesGDSListData";
	public static final String SES_HTML_CodeShare_LIST_DATA = "sesCodeShareListData";
	public static final String SES_HTML_EVENT_CODE_LIST = "sesEventCodeList";
	public static final String SES_HTML_BUILDSTATUS_LIST_DATA = "sesBuildStatusListData";
	public static final String SES_HTML_LOYALTY_PRODUCT_CODES_LIST = "sesLoyaltyProductCodesList";
	public static final String REQ_HTML_FLIGHTSCHEDULE_DATA = "reqFlightScheduleData";
	public static final String REQ_HTML_TIMEIN = "reqTimeIn";
	public static final String SES_HTML_AIRPORT_LIST_DATA = "sesAirportListData";
	public static final String SES_HTML_AIRCRAFTMODEL_LIST_DATA = "sesAircraftModelListData";
	public static final String SES_HTML_AIRCRAFTMODEL_DETAILS = "sesAircraftModelDetails";
	public static final String SES_HTML_MODELROUTEINFO_DETAILS = "sesModelRouteInfoDetails";
	public static final String SES_HTML_MINSTOPOVERTIME_DATA = "sesMinStopOverTimeData";
	public static final String REQ_HTML_SEGMENT_VALIDATION_DATA = "reqSegmentValidationData";
	public static final String REQ_HTML_TERMINAL_VALIDATION_DATA = "reqTerminalValidationData";
	public static final String REQ_HTML_TERMINAL_DISPLAY_ALERT = "reqTerminalDisplayAlert";
	public static final String REQ_HTML_SCHEDULE_LIST_DATA = "reqScheduleListData";
	public static final String REQ_HTML_DEFAULT_OPERATIONTYPE = "reqDefaultOperationType";
	public static final String REQ_HTML_HIDE_LOGICAL_CC_DETAILS = "hideLogicalCC";
	public static final String REQ_HTML_HIDE_WAIT_LISTING_DETAILS = "hideWaitListing";
	public static final String REQ_HTML_DEFAULT_MIN_STOP_OVERTIME = "reqDefaultMinStopOverTime";
	public static final String REQ_HTML_DEFAULT_SCHEDULESTATUS = "reqScheduleStatus";
	public static final String REQ_HTML_LEGS = "reqLegs";
	public static final String REQ_HTML_LEG_DTLS = "reqLegDtls";
	public static final String REQ_HTML_OVERLAPFLAG = "reqOverlapFlag";
	public static final String REQ_HTML_OVERLAPABLE_SCHEDULE_DATA = "reqOverLapSchedData";
	public static final String REQ_HTML_OVERLAPABLE_SCHEDULE_ARRAY_DATA = "reqOverLapSchedDataArray";
	public static final String SES_HTML_MODEL_DATA = "sesSchedData";
	public static final String SES_HTML_FLTMODEL_DATA = "sesFltData";
	public static final String REQ_MINIMUM_TIME_ALLOWEDFORSCHEDULE = "reqMinTineForSched";
	public static final String REQ_DAY_OFFSET = "reqDayOffSet";
	public static final String REQ_HTML_DEFAULT_MAIL_SERVER = "reqDefaultMailServer";
	public static final String REQ_HTML_ADD_OPERATIONS_ONLY = "reqAddOperationsOnly";
	public static final String REQ_HTML_EDIT_FLIGHTNUMBER = "reqEditFltNumber";
	public static final String REQ_HTML_DEL_FLOWN_FLIGHTS = "reqDelFlownFlt";
	public static final String REQ_HTML_FLIGHTNUMBER = "reqFltNumber";
	public static final String REQ_HTML_USER_NOTE = "reqUserNote";
	public static final String REQ_HTML_FLIGHTNUMBER_LENGTH = "reqFltNumberLength";
	public static final String REQ_HTML_ACTIVATE_PAST_FLIGHTS = "reqActivatePastFlights";
	public static final String REQ_HTML_PAST_FLIGHTS_ACTIVATION_DURATION_IN_DAYS = "reqPastFlightActivationDurationInDays";
	public static final String REQ_HTML_CHANNELS = "reqChannels";
	public static final String REQ_HTML_AGENTS = "reqAgents";
	public static final String REQ_HTML_PROMO_TYPES = "reqPromoTypes";
	public static final String REQ_HTML_BUNDLED_FARES = "reqBundledFares";
	public static final String REQ_HTML_BOOKING_CLASS_LIST = "reqBookingClasses";
	public static final String REQ_CS_ENABLE = "reqCodeShareEnable";
	public static final String REQ_GDS_CARRIER_CODE_ARRAY = "gdsCarrierCodeArray";
	public static final String REQ_HTML_CHARGES_LIST = "reqChargesList";
	public static final String REQ_HTML_SERVICE_TAX_REPORT_CATEGORY = "reqServiceTaxReportCategory";	
	public static final String REQ_HTML_OVERRIDE_FREQUENCY_ON_ROLL_FWD = "reqOverrideFrequency";
	public static final String REQ_HTML_TIME_CHANGE_CANCELLATION_AND_REPROTECTION_FOR_PAST_FLIGHTS = "reqTimeChangeCancellationAndReprotectionForPastFlights";
	
	public static final String REQ_HTML_BUILD_DEFAULT_INV_TEMPLATE = "reqBuildDefaultInvTemplate";

	public static final String REQ_TOTAL_RECORDS = "totalNoOfRecords";
	public static final String REQ_RECORD_NUM = "recNo";
	public static final String REQ_TOTAL_STRING = "totalString";

	public static final String REQ_CLIENT_MESSAGES = "reqClientMessages";
	public static final String REQ_BL_RULES_DATA_ELEMENTS = "reqBlRulesDataElementList";

	public static final String REQ_HTML_CITY_PAIR_DATA = "reqHtmlCityPairData";
	public static final String REQ_HTML_DIRECT_ROUT_USAGE_DATA = "reqHtmlDirectRouteUsageData";
	public static final String REQ_HTML_ROUTES_IN_USE_DATA = "reqHtmlRoutesInUseData";

	public static final String REQ_HTML_USERDOMAIN_DATA = "reqUserDomain";
	public static final String REQ_HTML_USER_ROLE_DATA = "reqUserRoleHtmlData";
	public static final String REQ_HTML_BC_DATA = "reqBcHtmlData";
	public static final String REQ_HTML_STN_DATA = "reqStnHtmlData";
	public static final String REQ_HTML_BCCATEGORY_DATA = "reqBcCategoryData";

	public static final String REQ_HTML_USER_DATA = "reqUserData";
	public static final String REQ_HTML_PARAM_DATA = "reqParamData";
	public static final String REQ_HTML_ROLE_DATA = "reqRoleHtmlData";
	public static final String REQ_DEPENDANCY_ARRAY = "reqDependancyArray";
	public static final String REQ_HTML_PRIVILEGE_DATA = "reqHtmlPrivilegeData";
	public static final String REQ_HTML_SITA_DATA = "reqSITAData";
	public static final String REQ_HTML_CARD_TYPE_DATA = "reqCardTypeData";
	public static final String REQ_HTML_REPORT_SCHEDULER_APP_PARAM = "reqReportSchedularAppParam";

	public static final String REQ_HTML_COMMITION_TYPE_LIST = "reqCommTypeList";
	public static final String REQ_HTML_QUEUE_PAGES = "reqQueuePages";
	

	// Fare Class Conetnts
	public static final String REQ_HTML_FARECLASS_SELECT_LIST = "reqFareClassList";
	public static final String REQ_HTML_FARERULE_DATA = "reqFareRuleData";
	public static final String REQ_HTML_FIXED_BC_FARERULE_DATA = "reqFixedBCFareRuleData";
	public static final String REQ_HTML_BOOKINGCODE_SELECT_LIST = "reqBookingCodeList";
	public static final String REQ_HTML_EVENT_CODE_SELECT_LIST = "reqEventCodeList";
	public static final String REQ_HTML_GDS_CODE_SELECT_LIST = "reqGdsCodeList";
	public static final String REQ_HTML_FAREVISIBILITY_SELECT_LIST = "reqVisibilityList";
	public static final String REQ_HTML_FARE_GRID = "reqFareGrid";
	public static final String ACTION_AGENTWISE_FC = "AgentFares";
	public static final String ACTION_LINKAGENTS = "LinkAgents";
	public static final String REQ_AGENT_SELECT_LIST = "reqAgentList";
	public static final String REQ_AGENT_FC_GRID = "reqAgentFareGrid";
	public static final String REQ_AGENTFClINK_DDL = "reqAgentDDL";
	public static final String REQ_PAXCAT_LIST = "reqPaxCatList";
	public static final String REQ_FARE_CAT_LIST = "reqFareCatList";
	public static final String REQ_HUBS = "reqHubs";
	public static final String REQ_CARRIER_SELECT_LIST = "reqCarrierList";
	public static final String REQ_HTML_FARERULE_MULTI_LIST = "reqFareRuleMultiList";
	public static final String REQ_HTML_FARE_VISIBILITY_MULTI_LIST = "reqFareVisibilityMultiList";
	public static final String REQ_FLEXI_CODE_LIST = "reqFlexiCodeList";
	public static final String REQ_FLEXI_JOURNEY_LIST = "reqFlexiJourneyList";
	public static final String REQ_HTML_FARE_MOD_CANC_TYPES = "reqFareModCancTypeList";
	public static final String REQ_HTML_SHOW_NO_FARE_SPLIT_OPTION = "isDisplayNoFareSplitOption";
	public static final String REQ_HTML_SHOW_SELECTED_CURRENCY_OPTION = "isDisplaySelectedCurrencyOption";

	// Airport Charges/Charges
	public static final String REQ_HTML_AIRPORT_CHARGES_GRID = "reqAirportCharges";
	public static final String REQ_AIRPORT_LIST = "reqAirportList";
	public static final String REQ_AIRPORT_CODE_LIST = "reqAirportCodeList";
	public static final String REQ_COUNTRY_CODE = "reqCountryCode";

	public static final String REQ_CHARGE_CODE_LIST = "reqChargeCodeList";
	public static final String REQ_GROUP_LIST = "reqGroupList";
	public static final String REQ_GROUP_STATUS_LIST = "reqGroupStatusList";
	public static final String REQ_CHARGE_APPLIES_TO_LIST = "reqChargeAppliesToList";
	public static final String REQ_CATEGORY_LIST = "reqCategoryList";
	public static final String REQ_SEGMENT_LIST = "reqSegmentList";
	public static final String REQ_HTML_CHARGES_GRID = "reqChargesGrid";
	public static final String REQ_HTML_MODIFY_CHARGES_GRID = "reqModifyChargesGrid";
	public static final String REQ_CHARGES_DATA = "reqChargesData";
	public static final String REQ_CHARGES_MODEL_DATA = "reqChargesData";
	public static final String REQ_BUNDLED_FARES_ENABLED = "reqBundledFaresEnabled";

	// syatem param
	public static final String REQ_MODEL_DATA = "reqModelData";
	public static final String REQ_HTML_MODEL_LIST = "reqHtmlModelList";
	public static final String REQ_HTML_AIRCRAFT_TAIL_ROWS = "reqHtmlAircraftTailRows";
	public static final String REQ_HTML_DEFAULT_CLASSOFSERVICE = "reqclassofservice";
	public static final String REQ_HTML_DEFAULT_CRRIERCODE = "reqcrriercode";
	public static final String REQ_HTML_USER_CRRIERCODE = "requsercrriercode";
	public static final String REQ_HTML_LCC_ENABLE_STATUS = "reqLCCEnableStatus";
	public static final String REQ_HTML_GROUND_SERVICE_ENABLED = "reqIsGroundServiceEnabled";

	// SSR
	public static final String SSR_CHARGE_STATUS = "reqSsrChargeStatus";
	public static final String SSR_CHARGE_COMMISSION_STATUS = "reqSsrChargeCommisionStatus";

	//DefaultAnciTemplate
	public static final String REQ_ANCI_TEMPL_TYPE = "reqAnciTemplType";
	public static final String REQ_DEFLT_ANCI_TEMPL_STATUS = "reqAnciTemplStatus";
	public static final String REQ_ANCI_TEMPL = "reAnciTempl";
	
	// request parameters for aircraft schedule
	public static final String REQ_MODEL = "reqModel";
	public static final String REQ_MODE = "reqMode";
	public static final String REQ_SEATS_TO_BE_REPROTECTED = "reqSeatsToBeReprotected";
	public static final String REQ_FLIFHTS_TO_BE_REPROTECTED = "reqFlightsToBeReprotected";

	// VIEW FARES
	public static final String REQ_ORIGIN_SELECT_LIST = "reqOriginList";
	public static final String REQ_DESTINATION_SELECT_LIST = "reqDestList";
	public static final String REQ_HTML_CARRIER_SELECT_LIST = "reqCarrierList";
	public static final String REQ_HTML_VIAPOINT_SELECT_LIST = "reqViaList";
	public static final String REQ_HTML_ACTIVE_AIRPORT_SELECT_LIST = "reqActiveViaList";
	public static final String REQ_HTML_COS_LIST = "reqCOSList";
	public static final String REQ_HTML_BASIS_LIST = "reqBasisList";
	public static final String REQ_HTML_BC_LIST = "reqBCList";
	public static final String REQ_HTML_STATUS_SELECT_LIST = "reqStatusList";
	public static final String REQ_VIEW_FARE_GRID = "reqViewFareGrid";
	public static final String REQ_FARE_SUMMARY_GRID = "reqFareSummaryGrid";
	public static final String REQ_CABIN_CLASSES = "reqCabinClasses";
	public static final String REQ_INCEXC_AIRPORT = "reqIncExcList";

	public static final String REQ_VIEW_ONDFARE_GRID = "reqViewONDFareGrid";

	// Contains BookingCodes Grid Data
	public static final String REQ_HTML_BOOKING_CODE_DATA = "reqBookingCodes";

	// Contains FareClass Grid Data for BookingCode
	public static final String REQ_HTML_FARECLASS_DATA = "reqFareClasses";

	public static final String REQ_HTML_LEGS_HTML = null;

	// Contains SeatAllocation Grid Data for BookingCode
	public static final String REQ_HTML_SEATALLOC_BCODE_DATA = "reqSeatAllocBookingCodeData";

	public static final String REQ_FORM_RELOAD = "reqFormReload";
	public static final String REQ_FORM_FLIGHT_SUMM = "reqFlightSumm";
	public static final String REQ_FORM_FLIGHT_ROLL_FWD_AUDIT_SUMM = "reqFlightRollForwardAuditSumm";
	public static final String REQ_FORM_FLIGHT_DEFAULT_CAPACITY = "reqFlightDefCap";

	public static final String REQ_HTML_BOOKINGCODES_DESC = "reqBookingCodesDesc";
	public static final String REQ_HTML_BUNDLED_FARE_NAME_LIST = "reqBundledFareNameList";
	public static final String REQ_HTML_BUNDLED_CATEGORY_LIST = "reqBundledCategoryList";
	public static final String REQ_HTML_BUNDLED_OND_LIST = "reqBundledONDList";
	public static final String REQ_HTML_BUNDLED_BOOKING_CLASSES_LIST = "reqBundledBookingClassesList";
	public static final String REQ_HTML_BUNDLED_STATUS_LIST = "reqBundledStatusList";

	// constatnts for schedule response

	public static final String SCHEDULE_UPDATE_SUCCESS = "scheduleUpdateSuccess";
	public static final String SCHEDULE_FLIGHT_CAPACITY_INSUFFICIENT = "scheduleFlightCapInsuficient";
	public static final String SCHEDULE_LEG_RESERVATION_EXISTS = "scheduleLegresreExist";
	public static final String SCHEDULE_FLIGHT_REPROTECT_NEED = "scheduleFlightreprotectNeeded";
	public static final String SCHEDULE_FLIGHT_MODIFY_NEED = "scheduleFlightModifyNeed";
	public static final String SCHEDULE_FLIGHT_MODIFY_REPROTECT_NEED = "scheduleFlightModifyReprotectNeed";

	public static final String CONFLICT_FLIGHT = "CONFLICT_FLIGHT";

	public static final String SCHEDULE_SCHEDULE_CONFLICT = "um.airschedules.logic.bl.schedule.schedule.overlapped";
	public static final String SCHEDULE_FLIGHT_CONFLICT = "um.airschedules.logic.bl.schedule.flight.overlapped";
	public static final String SCHEDULE_INVALID_LEG = "um.airschedules.logic.bl.schedule.schedule.durationerr";
	public static final String SCHEDULE_ALLOWED_DURATION_ZERO = "um.airschedules.logic.bl.schedule.schedule.durationzero";
	public static final String CANNOT_DELETE_BC_INVENTORY = "um.airschedules.logic.bl.bc.inventory.not.deletable";
	public static final String CABIN_CLASS_NOT_FOUND = "um.airschedules.logic.bl.cabin.class.not.found";
	public static final String CC_CAPACITY_INSUFFICIENT = "um.airschedules.logic.bl.cc.capacity.insufficient";
	public static final String SCHEDULE_UNHANDLED_ERROR = "um.airschedules.logic.bl.schedule.schedule.unhandlederr";
	public static final String ALL_SEGMENTS_INVALID = "um.airschedules.logic.bl.allsegments.invalid";
	public static final String INVALID_OVERLAPPING_SCHEDULE = "um.airschedules.logic.bl.schedule.invalidoverlap";
	public static final String OVERLAPPING_ALREADY_OVERLAPPING_SCHEDULE = "um.airschedules.logic.bl.schedule.alreadyoverlap";
	public static final String INVALID_OVERLAPPING_FLIGHT = "um.airschedules.logic.bl.flight.invalidoverlap";
	public static final String OVERLAPPING_ALREADY_OVERLAPPING_FLIGHT = "um.airschedules.logic.bl.flight.alreadyoverlap";
	public static final String INVALID_SCHEDULE_FREQUENCE_FOR_DATE_RANGE = "um.airschedules.logic.bl.schedule.copy.invalid.daterange";
	public static final String TRYING_ILLEGAL_FLIGHT_UPDATION = "um.airschedules.logic.bl.flight.illegalupdation";
	public static final String INVALID_OVERLAPPING_SEGMENT_LEG = "um.airschedules.logic.bl.schedule.overlapsegment.invalid";
	public static final String INVALID_OVERLAPPING_SCHEDULE_STATUS = "um.airschedules.logic.bl.schedule.overlapbuildstatus.invalid";
	public static final String INVALID_OVERLAPPING_FLIGHT_EXISTING_FLIGHT = "um.airschedules.logic.bl.flight.invalidoverlap.existingflight";
	public static final String BUILD_SCHEDULE_PARTIALLY_SUCCESSFUL = "um.airschedules.logic.bl.schedule.buildschedule.partially.successful";
	public static final String BUILD_SCHEDULE_MAXTIME_EXCEEDED = "um.airschedules.logic.bl.schedule.buildschedule.maxtime.exceeded";
	public static final String BUILD_SCHEDULE_FAILED = "um.airschedules.logic.bl.schedule.buildschedule.failed";
	public static final String AIRCRAFT_UPDATED_MODEL_SEATATTACHED = "um.airschedules.logic.bl.flight.seatattached";
	public static final String INVALID_DEPARTURE_DATE = "um.flightschedule.leg.invaliddeptDate";
	public static final String FIXED_SEATS_INSUFFICIENT = "um.airschedules.logic.bl.cc.fixedseats.insufficient";

	public static final String FLIGHT_FLIGHT_CONFLICT = "um.airschedules.logic.bl.flight.schedule.overlapped";
	public static final String FLIGHT_SCHEDULE_CONFLICT = "um.airschedules.logic.bl.flight.flight.overlapped";

	// Auto check-in template
	public static final String REQ_ERROR_AIRPORT_CODE_EXIST = "um.autochecktemplate.form.airport.code.exist";

	public static final String REQ_HTML_REPROTECT_INVENTORY_DATA_BY_CCCODE = "reqReprotectFlightDataByCCCode";

	// flight data
	public static final String REQ_HTML_FLIGHT_DATA = "reqFlightData";
	public static final String SES_HTML_STATUS_DATA = "sesFlightStatusData";
	public static final String REQ_HTML_DEFAULT_FLIGHTSTATUS = "reqFlightDefStatus";
	public static final String REQ_HTML_FLIGHT_TYPES = "reqFlightTypes";
	public static final String REQ_HTML_FARE_DISCOUNT_TYPES = "reqFareDiscountTypes";
	public static final String REQ_MULTIPLE_FLIGHT_TYPE_AVAILABILITY = "reqMultiFltTypesAvailability";
	public static final String REQ_HDN_FLIGHT_NO = "hdnFlightNo";
	public static final String REQ_HDN_FLIGHT_ID = "hdnFlightId";
	public static final String REQ_TXT_DEPT_DATE = "txtDepatureDate";
	public static final String REQ_HDN_DEPT_DATE = "hdnDepatureDate";
	public static final String REQ_HDN_DEPT_DATE_TIME = "hdnDepatureDateTime";

	// to get the form data
	public static final String REQ_HTML_FORM_DATA = "reqFormData";

	// to get the PNL ADL Text data
	public static final String REQ_HTML_PNLADL_TEXT_DATA = "reqPNLADLTextData";

	public static final String CHECKBOX_ON = "on";
	public static final String REQ_HTML_SITA_ACTIVESTATUS_LIST = "reqActiveStatusList";
	public static final String REQ_HTML_SITA_ADDRESSTYPE_LIST = "reqSitaAddressTypeList";

	public static final String REQ_HTML_BUSINESS_SYSTEM_PARAMETER_VALUES = "reqHtmlBusinessSystemParameterValues";
	public static final String REQ_BUSINESS_SYSTEM_PARAMETER_TAB = "reqBusinessSystemParameterTab";

	public static final String REQ_HDN_AGENT_CODE = "reqHdnAgentCode";
	public static final String REQ_HDN_FROM_DATE = "reqHdnFromDate";
	public static final String REQ_HDN_TO_DATE = "reqHdnToDate";

	public static final String REQ_HDN_AIRPORT_CODE = "reqHdnAirportCode";

	public static final String REQ_OPERATION_MODE = "reqOperationMode";
	public static final String REQ_OPERATION_UI_MODE = "reqOperationUIMode";
	public static final String REQ_TRANSACTION_STATUS = "reqTransactionStatus";
	public static final String REQ_TRANSACTION_DISPLAYSTATUS = "reqDisplayTransactionStatus";
	public static final String REQ_DEFAULT_AIRLINE_CODE = "reqDefaultAirlineCode";
	public static final String REQ_IS_REMOTE_USER = "reqIsRemoteUser";
	public static final String REQ_ORIGIN_FOCUS = "reqOriginFocus";
	public static final String REQ_DESTINATION_FOCUS = "reqDestinationFocus";
	public static final String REQ_AIRPORT_FOCUS = "reqAirportFocus";
	public static final String REQ_STATION_FOCUS = "reqStationFocus";
	public static final String REQ_GOSHOW_FOCUS = "reqGoShowFocus";
	public static final String REQ_DELIVERY_METHOD_DISPLAY_STATUS = "reqDeliveryMethodDisplayStatus";
	public static final String REQ_PAL_CAL_DISPLAY_STATUS = "reqPalCalDisplayStatus";
	// System parameters
	public static final String REQ_CANCELLATION_CHARGE = "CNX";
	public static final String REQ_MODIFICATION_CHARGE = "MOD";

	public static final String REQ_SYS_PARAM = "reqSysParam";
	public static final String REQ_HTML_STATION_SELECT_LIST = "reqStationList";
	public static final String REQ_HTML_CITY_SELECT_LIST = "reqCityList";
	public static final String REQ_HTML_ACTIVE_STATION_SELECT_LIST = "reqActStationList";
	public static final String REQ_HTML_GO_SHORE_AGENT_SELECT_LIST = "reqGoShoreAgentList";

	public static final String REQ_HTML_FLIGHTSTATUS_LIST = "reqFlightStatusList";
	public static final String REQ_HTML_AIRPORT_STATUS_LIST = "reqAirportStatus";

	public static final String REQ_SERV_CH = "reqServiceChannel";
	public static final String REQ_FARES_RULE_DISCOUNT_VISIBILITY = "fareDiscountVisibility";
	public static final String REQ_AGENT_INSTRUCTIONS_DETAILS_VISIBILITY = "agentInstructionsVisibility";

	public static final String REQ_AGENT_FARE_COMMISSION_VISIBILITY = "agentFareCommissionVisibility";

	public static final String IS_EXRATE_AUTOMATION_ENABLED = "exRateAutomationEnabled";
	public static final String SHOW_ITINERARY_FARE_BREAKDOWN_OPTION = "showItineraryFareBreakDownOption";

	public static final String PLAN_FARES_CHARGE_ADD = "plan.fares.charge.add";
	public static final String PLAN_FARES_CHARGE_EDIT = "plan.fares.charge.edit";
	public static final String PLAN_FARES_CHARGE_DELETE = "plan.fares.charge.delete";
	public static final String PLAN_FARES_CHARGE_RATE_ADD = "plan.fares.charge.rate.add";
	public static final String PLAN_FARES_CHARGE_RATE_EDIT = "plan.fares.charge.rate.edit";
	public static final String PLAN_FARES_CREATE_BUNDLED_FARE_CHARGE = "plan.fares.charge.bundledfares.add";
	public static final String PLAN_FARES_CHARGE_DEFINE_EXCLUDE = "plan.fares.charge.define.exclude";

	public static final String PLAN_FARES_RULE_ADD = "plan.fares.rule.add";
	public static final String PLAN_FARES_RULE_EDIT = "plan.fares.rule.edit";
	public static final String PLAN_FARES_RULE_DELETE = "plan.fares.rule.delete";
	public static final String PLAN_FARES_RULE_AGENT = "plan.fares.rule.agent";

	public static final String PLAN_FLIGHT_MANAGE_ADD_ANY_FLIGHT = "plan.flight.flt.add";
	public static final String PLAN_FLIGHT_MANAGE_CANCEL = "plan.flight.flt.cancel";
	public static final String PLAN_FLIGHT_MANAGE_COPY = "plan.flight.flt.copy";
	public static final String PLAN_FLIGHT_MANAGE_EDIT = "plan.flight.flt.edit";
	public static final String PLAN_FLIGHT_SEGMENT_OVERLAP = "plan.flight.flt.seg.overlap";
	public static final String PLAN_INVENTORY_SEAT_ALLOCATION = "plan.invn.alloc";
	public static final String PLAN_FLIGHT_REPROTECT_TRANSFER = "plan.flight.flt.transfer";
	public static final String PLAN_FLIGHT_MANAGE_ADD_OPERATIONS_FLIGHT = "plan.flight.flt.add.optype.operations";
	public static final String PLAN_FLIGHT_EDIT_FLIGHT_NUMBER = "plan.flight.edit.flightNumber";
	public static final String PLAN_FLIGHT_DELETE_FLOWN_FLIGHTS = "plan.flight.flt.deleteFlown";
	public static final String PLAN_FLIGHT_DOWNGRADE_TO_ANY_MODEL = "plan.flight.flt.downGradeToAnyModel";
	public static final String PLAN_FLIGHT_ACTIVATE_PAST_FLIGHT = "plan.flight.flt.activate.flown";
	public static final String OVERRIDE_FREQUENCY_ON_ROLL_FWD = "plan.invn.alloc.roll.fwd.alter.freq";

	public static final String PLAN_FLIGHT_SCHEDULE_MANAGE_ADD = "plan.flight.sch.add";
	public static final String PLAN_FLIGHT_SCHEDULE_MANAGE_BUILD = "plan.flight.sch.build";
	public static final String PLAN_FLIGHT_SCHEDULE_MANAGE_CANCEL = "plan.flight.sch.cancel";
	public static final String PLAN_FLIGHT_SCHEDULE_MANAGE_COPY = "plan.flight.sch.copy";
	public static final String PLAN_FLIGHT_SCHEDULE_MANAGE_EDIT = "plan.flight.sch.edit";
	public static final String PLAN_FLIGHT_SCHEDULE_MANAGE_PRINT = "plan.flight.sch.print";
	public static final String PLAN_FLIGHT_SCHEDULE_SEGMENT_OVERLAP = "plan.flight.sch.seg.overlap";
	public static final String PLAN_FLIGHT_SCHEDULE_MANAGE_SPLIT = "plan.flight.sch.split";
	public static final String PLAN_FLIGHT_SCHEDULE_SHOW_FLIGHTS = "plan.flight.sch.showflights";
	public static final String PLAN_FLIGHT_SCHEDULE_MANAGE_GDS = "plan.flight.sch.gds"; // Haider 22Oct
    public static final String PRIVI_REPROTECT_PNL_ADD = "plan.flight.flt.transfer.pnr.list";
    public static final String PLAN_ROLL_ADVANCE_CANCLE_BATCH = "plan.invn.alloc.bc.roll.advance.cancel.batch";


    public static final String PLAN_INVN_BC_ADD = "plan.invn.bc.add";
	public static final String PLAN_INVN_BC_EDIT = "plan.invn.bc.edit";
	public static final String PLAN_INVN_BC_DELETE = "plan.invn.bc.delete";
	public static final String PLAN_INVN_BC_STATUS = "plan.invn.alloc.bc.status";

	public static final String PLAN_INVN_OPT_EDIT = "plan.invn.opt.edit";
	public static final String PLAN_INVN_OPT_EXPORT = "plan.invn.opt.export";

	public static final String PRIV_SYS_PARAM = "sys.param";

	public static final String PRIV_SYS_MAS_AIRMODEL = "sys.mas.airmodel";
	public static final String PRIV_SYS_MAS_AIRMODEL_ADD = "sys.mas.airmodel.add";
	public static final String PRIV_SYS_MAS_AIRMODEL_EDIT = "sys.mas.airmodel.edit";
	public static final String PRIV_SYS_MAS_AIRMODEL_DELETE = "sys.mas.airmodel.delete";

	public static final String PRIV_SYS_MAS_AIRPORT = "sys.mas.airport";
	public static final String PRIV_SYS_MAS_AIRPORT_ADD = "sys.mas.airport.add";
	public static final String PRIV_SYS_MAS_AIRPORT_EDIT = "sys.mas.airport.edit";
	public static final String PRIV_SYS_MAS_AIRPORT_DELETE = "sys.mas.airport.delete";
	public static final String PRIV_SYS_MAS_AIRPORT_FOR_DISPLAY = "sys.mas.airport.fordisp"; // Haider

	public static final String PRIV_SYS_MAS_AIRPORT_DST = "sys.mas.airport.dst";
	public static final String PRIV_SYS_MAS_AIRPORT_DST_ADD = "sys.mas.airport.dst.add";
	public static final String PRIV_SYS_MAS_AIRPORT_DST_EDIT = "sys.mas.airport.dst.edt";
	public static final String PRIV_SYS_MAS_AIRPORT_DST_ROLLBACK = "sys.mas.airport.dst.rollback";

	public static final String PRIV_SYS_MAS_AIRPORT_CIT = "sys.mas.airport.cit";
	public static final String PRIV_SYS_MAS_AIRPORT_CIT_ADD = "sys.mas.airport.cit.add";
	public static final String PRIV_SYS_MAS_AIRPORT_CIT_EDIT = "sys.mas.airport.cit.edit";
	public static final String PRIV_SYS_MAS_AIRPORT_CIT_DELETE = "sys.mas.airport.cit.delete";

	public static final String PRIV_SYS_MAS_AIRPORT_TERM = "sys.mas.airport.term";
	public static final String PRIV_SYS_MAS_AIRPORT_TERM_ADD = "sys.mas.airport.term.add";
	public static final String PRIV_SYS_MAS_AIRPORT_TERM_EDIT = "sys.mas.airport.term.edit";
	public static final String PRIV_SYS_MAS_AIRPORT_TERM_DELETE = "sys.mas.airport.term.delete";

	public static final String PRIV_SYS_MAS_AIRTAIL = "sys.mas.airtail";
	public static final String PRIV_SYS_MAS_AIRTAIL_ADD = "sys.mas.airtail.add";
	public static final String PRIV_SYS_MAS_AIRTAIL_EDIT = "sys.mas.airtail.edit";
	public static final String PRIV_SYS_MAS_AIRTAIL_DELETE = "sys.mas.airtail.delete";

	public static final String PRIV_SYS_MAS_COUNTRY = "sys.mas.country";
	public static final String PRIV_SYS_MAS_COUNTRY_ADD = "sys.mas.country.add";
	public static final String PRIV_SYS_MAS_COUNTRY_EDIT = "sys.mas.country.edit";
	public static final String PRIV_SYS_MAS_COUNTRY_DELETE = "sys.mas.country.delete";

	public static final String PRIV_SYS_MAS_CURRENCY = "sys.mas.currency";
	public static final String PRIV_SYS_MAS_CURRENCY_ADD = "sys.mas.currency.add";
	public static final String PRIV_SYS_MAS_CURRENCY_EDIT = "sys.mas.currency.edit";
	public static final String PRIV_SYS_MAS_CURRENCY_DELETE = "sys.mas.currency.delete";
	public static final String PRIV_SYS_MAS_CURRENCY_FOR_DISPLAY = "sys.mas.currency.fordisp"; // Haider

	public static final String PRIV_SYS_MAS_ROUTE = "sys.mas.route";
	public static final String PRIV_SYS_MAS_ROUTE_ADD = "sys.mas.route.add";
	public static final String PRIV_SYS_MAS_ROUTE_EDIT = "sys.mas.route.edit";
	public static final String PRIV_SYS_MAS_ROUTE_DELETE = "sys.mas.route.delete";

	public static final String PRIV_SYS_MAS_SITA = "sys.mas.sita";
	public static final String PRIV_SYS_MAS_SITA_ADD = "sys.mas.sita.add";
	public static final String PRIV_SYS_MAS_SITA_EDIT = "sys.mas.sita.edit";
	public static final String PRIV_SYS_MAS_SITA_DELETE = "sys.mas.sita.delete";

	public static final String PRIV_SYS_MAS_STATION = "sys.mas.station";
	public static final String PRIV_SYS_MAS_STATION_ADD = "sys.mas.station.add";
	public static final String PRIV_SYS_MAS_STATION_EDIT = "sys.mas.station.edit";
	public static final String PRIV_SYS_MAS_STATION_DELETE = "sys.mas.station.delete";

	public static final String PRIV_SYS_MAS_TERRITORY = "sys.mas.territory";
	public static final String PRIV_SYS_MAS_TERRITORY_ADD = "sys.mas.territory.add";
	public static final String PRIV_SYS_MAS_TERRITORY_EDIT = "sys.mas.territory.edit";
	public static final String PRIV_SYS_MAS_TERRITORY_DELETE = "sys.mas.territory.delete";

	public static final String PRIV_SYS_SECURITY_ROLE = "sys.security.role";
	public static final String PRIV_SYS_SECURITY_ROLE_ADD = "sys.security.role.add";
	public static final String PRIV_SYS_SECURITY_ROLE_EDIT = "sys.security.role.edit";
	public static final String PRIV_SYS_SECURITY_ROLE_DELETE = "sys.security.role.delete";
	public static final String PRIV_SYS_SECURITY_ROLE_COPY = "sys.security.role.copy";

	public static final String PRIV_SYS_SECURITY_USER = "sys.security.user";
	public static final String PRIV_SYS_SECURITY_USER_ADD = "sys.security.user.add";
	public static final String PRIV_SYS_SECURITY_USER_EDIT = "sys.security.user.edit";
	public static final String PRIV_SYS_SECURITY_USER_DELETE = "sys.security.user.delete";

	public static final String PRIV_SYS_MAS_SSR = "sys.mas.ssr"; // Dhanya
																	// 10/03/2009
	public static final String PRIV_SYS_MAS_SSR_ADD = "sys.mas.ssr.add";
	public static final String PRIV_SYS_MAS_SSR_EDIT = "sys.mas.ssr.edit";
	public static final String PRIV_SYS_MAS_SSR_DELETE = "sys.mas.ssr.delete";
	
	public static final String PRIV_INV_ALLOC_ROLL_FORWARD_ADVANCE = "plan.invn.alloc.bc.roll.advance";

	public static final String PRIVI_AIRPORT_TRANSFER_SSR_ADD = "sys.mas.apt.chgs.add";
	public static final String PRIVI_AIRPORT_TRANSFER_SSR_EDIT = "sys.mas.apt.chgs.edit";
	public static final String PRIVI_AIRPORT_TRANSFER_SSR_DELETE = "sys.mas.apt.chgs.delete";
	public static final String TA_INVOICE = "ta.invoice";
	public static final String TA_INVOICE_DETAILS = "ta.invoice.details";
	public static final String TA_INVOICE_EMAIL = "ta.invoice.email";
	public static final String TA_INVOICE_GENERATE = "ta.invoice.generate";
	public static final String TA_INVOICE_TRANSFER = "ta.invoice.transfer";

	public static final String TA_MAINT = "ta.maint";
	public static final String TA_MAINT_ADD = "ta.maint.add";
	public static final String TA_MAINT_EDIT = "ta.maint.edit";
	public static final String TA_MAINT_DELETE = "ta.maint.delete";
	public static final String TA_MAINT_CREDIT = "ta.maint.credit";
	public static final String TA_MAINT_CREDIT_UPDATE = "ta.maint.credit.update";

	public static final String TA_PAY = "ta.pay";
	public static final String TA_PAY_SETTLE = "ta.pay.settle";
	public static final String TA_PAY_SETTLE_PAYMENT = "ta.pay.settle.payment";
	public static final String TA_PAY_SETTLE_RECEIPT = "ta.pay.settle.recipt";
	public static final String TA_PAY_SETTLE_INVOICE = "ta.pay.settle.invoice";
	public static final String TA_PAY_REVERSE = "ta.pay.reverse";

	public static final String PRIVI_EXCLUDE_CHARGES = "xbe.res.exclude.charges";

	// Manage Inventory
	public static final String REQ_MANAGE_INVENTORY_FARES = "reqFareDetails";
	// Parameter name indicating whether the user has privilege to change BC allocation status
	public static final String REQ_HAS_BC_STATUS_CHANGE_PRIVILEGE = "hasBCStatusChangePrivilege";

	// Optimize Inventory
	public static final String REQ_OPTIMIZE_RESULT = "reqOptimizeResult";
	public static final String REQ_OPTIMIZE_STATUS = "reqOptimizeStatus";
	public static final String REQ_OPTIMIZE_MAX_FLIGHTS = "reqOptimizeMaxFlights";

	// reprotec shedule variables
	public static final String REQ_REPRO_SCHEDID = "reqrepshid";
	public static final String REQ_REPRO_STARTD = "reqrepstd";
	public static final String REQ_REPRO_STOPD = "reqrepstpd";
	public static final String REQ_REPRO_FLTNO = "reqrepfltno";
	public static final String REQ_REPRO_DESTINATION = "reqrepdest";
	public static final String REQ_REPRO_FREQUENCY = "reqrepfreq";
	public static final String REQ_REPRO_ORIGIN = "reqreporg";

	// reprotec flight variables
	public static final String REQ_REPRO_FLIGHTID = "reqRepFltId";
	public static final String REQ_REPRO_FLIGHT_NUMBER = "reqRepFltNum";
	public static final String REQ_REPRO_DEP_DATE = "reqRepDepDate";
	public static final String REQ_REPRO_FLT_DESTINATION = "reqRepFltDest";
	public static final String REQ_REPRO_FLT_ORIGIN = "reqRepFltOrg";

	public static final String HANDLING_CHARGES_REQ = "reqHandlingCharge";
	public static final String REQ_HTML_TERRITORY_COUNTRY = "reqTerritoryCountry";
	public static final String REQ_HTML_STATION_COUNTRY = "reqStationCountry";
	public static final String REQ_MSG = "saveMsg";
	public static final String REQ_REC_NO = "recNo";
	public static final String REQ_DELETE_MSG = "deleteMsg";

	public static final String TOTAL_CHARGE_REC = "totalChargeRecs";
	public static final String REQ_AGENT_TYPE_CREDIT_APPLICABLE = "reqAgentTypeCreditApplicable";
	// Error codes
	public static final String ERR_UNAUTHORIZED_ACCESS = "err.unauthorized.access";

	public static final String REQ_BASE_CURRENCY = "reqBaseCurrency";

	public static final String REQ_SWIFT_CODE = "reqSwiftCode";

	public static final String REQ_ADHOC_FARE_DATA = "reqAdhocData";

	// fare rules
	public static final String REQ_ADCHCONFIG = "reqPaxFareData";

	public static final String REQ_INVOICEYEAR = "reqBasePrevious";
	public static final String REQ_PAYMENT_TERMS = "reqPaymentTerms";
	public static final String REQ_ACCOUNT_NO = "reqAccountNo";
	public static final String REQ_ACCOUNT_NAME = "reqAccountName";
	public static final String REQ_BANK_NAME = "reqBankName";
	public static final String REQ_BANK_ADDRESS = "reqBankAddress";
	public static final String REQ_CURRENT_MONTH = "reqCurrentMonth";
	public static final String REQ_CURRENT_YEAR = "reqCurrentYear";
	public static final String REQ_AIRPORT_CURRENCY = "reqAirportCurrency";
	public static final String REQ_LINKED_FARES_BASED_ON_MASTER_FARES_VISIBILITY = "reqLinkedFaresVisibility";
	public static final String REQ_SAME_FARE_MODIFICATION_VISIBILITY = "reqSameFareModificationVisibility";

	// Reports
	public static final String REQ_ALL_GSA = "reqGSA";
	public static final String REQ_STATION_LIST = "reqStationList";
	public static final String REQ_NATIONALITY_LIST = "reqNationalityList";
	public static final String REQ_STATUS_LIST = "reqStatusList";
	public static final String REQ_COUNTRY_LIST = "reqCountryList";
	public static final String REQ_CITY_COUNTRY_LIST = "reqCityCountryList";
	public static final String REQ_AGENT_TYPE = "reqagentType";
	public static final String REQ_TERRITORY = "reqterritory";
	public static final String REQ_MODELS = "reqAircraftModels";
	public static final String REQ_FLIGHT_STATUS = "reqFlightStatus";
	public static final String REQ_FLIGHT_BUILD_STATUS = "reqFlightBuildStatus";
	public static final String REQ_FLIGHT_NO_LIST = "reqFlightNoList";
	public static final String REQ_AGENT_USER_LIST = "reqAgentUser";
	public static final String REPORT_REF = "WebReport1";
	public static final String REPORT_HTML = "HTML";
	public static final String REPORT_PDF = "PDF";
	public static final String REPORT_EXCEL = "EXCEL";
	public static final String REPORT_CSV = "CSV";
	public static final String APP_CODE = "airadmin";
	public static final String REQ_BOOKING_TYPE_LIST = "reqBookingType";
	public static final String REQ_PAX_STATUS_LIST = "reqPaxStatusList";
	public static final String REQ_PFS_STATUS_LIST = "reqPfsStatusList";
	public static final String REQ_AGENTS_STATION = "reqAgentStation";
	public static final String REQ_ISTRNINV = "reqIsTrnWiseInv";
	public static final String REQ_VARISTRNINV = "reqVarIsTrnInv";
	public static final String REQ_USER_ID_LIST = "reqUserIdList";
	public static final String REQ_TASK_GROUP_LIST = "reqTaskGroupList";
	public static final String REQ_TASK_LIST = "reqTaskList";
	public static final String REQ_REPORT_URL = "reqRepUrl";
	public static final String REQ_SALES_CHANNEL_CODES = "reqSalesChannelCodes";
	public static final String REQ_AGENTS_LIMIT = "reqAgentLimit";

	public static final String TRANS_DEBIT = "DR";
	public static final String TRANS_CREDIT = "CR";

	public static final String PAX_TITLES = "reqPaxTitles";
	public static final String PAX_TITLES_FOR_CHD = "reqPaxTitlesForChild";
	public static final String INFANT_TITLES = "reqInfTitles";
	public static final String PAX_TYPE = "reqPaxType";

	public static final String REQ_HTML_CARRIER_CODE_LIST = "carrierCodeList";

	public static final String REQ_HTML_CHAGRES_CODE_LIST = "chargesCodeList";

	public static final String REQ_HTML_CURRENCY_CODE_LIST = "reqCurrencyCodeList";

	public static final String REQ_SKIP_REPORT_PERIOD = "skipReportPeriod";

	public static final String REQ_SCHEDULE_PROMO_REPORT = "schedulePromoInfoRpt";

	// Foer travel agent invoice settlement
	public static final String REQ_AGENT_CODE = "reqAgentCode";
	public static final String REQ_AGENT_NAME = "reqAgentName";
	public static final String REQ_MAX_CON_TIME = "reqMaxConTime";

	public static final String REQ_REPORTING_GROUP = "reqReportingGroup";
	public static final String REQ_JOURNEY = "reqJourney";

	public static final String REP_CARRIER_NAME = "CARRIER";

	public static final String REQ_HTML_REQUEST_DATA = "reqRequestData";
	public static final String REQ_HTML_REQUEST = "reqRequest";
	public static final String REQ_RESPONSE_STRING = "reqResponseString";
	public static final String REQ_ACCOUNT_CODE = "reqAccountCode";
	public static final String REQ_CARRIER_AGENT = "reqCarrierAgent";
	public static final String APP_CARRIER_NAME = "appCarrierName";

	// for reports
	public static final String REP_HDN_LIVE = "hdnLive";
	public static final String REP_HDN_RPT_TYPE = "hdnRptType";
	public static final String REP_LIVE = "LIVE";
	public static final String REP_OFFLINE = "OFFLINE";
	public static final String REP_SET_LIVE = "reqHdnLive";
	public static final String REP_SET_RPT = "reqHdnRptType";
	public static final String REP_RPT_TYPE = "SPLIT";
	public static final String REP_RPT_SHOW_PAY = "reqSowPay";
	public static final String REP_MOD_DETAILS_ENABLED = "reqModDetailsEnabled";
	public static final String REP_RPT_START_DAY = "reqStartDay";
	public static final String REP_AIRLINE_WISE_COLLECTION_TYPE = "reqAirlineWiseColType";
	public static final String REP_CMP_PAY_STATIONS_ENABLED= "reqStationsFilterForCompanyPayRpt";

	public static final String APP_RELEASE_VERSION = "appAccelAeroRelVersion";
	public static final String APP_RELEASE_VERSION_URL_KEY = "relversion";
	public static final String APP_BOOLEAN_TRUE = "true";

	
	public static final String SECURE_URL = "secureUrl";
	
	// currency boundary visibility attribute
	public static final String CURR_BOUNDARY = "currencyBoundary";

	public static final String ENGINE_VISIBILITY = "engineVisibility";

	public static final String CHILD_VISIBILITY = "childVisibility";

	public static final String PRIVI_VIEW_FULL_SEAT_INVENT_RPT = "rpt.pln.icf.restrict";

	public static final String PRIVI_SHOW_REVENUE_SEAT_INVENT_RPT = "rpt.pln.icf.show.revenue";

	public static final String CURRENCY_STATUS = "reqCurrStatus";

	public static final String CURRENCY_SYS_DATE = "reqSysDate";

	public static final String CHARGES_SYS_DATE = "reqChargesSysDate";

	public static final String REQ_PAYMENT_GATEWAY_IBE = "ibePaymentGatewayList";

	public static final String REQ_PAYMENT_GATEWAY_XBE = "xbePaymentGatewayList";

	public static final String OPEN_ENDED_RETURN_SUPPORT = "openReturnSupport";

	/* fare rule modifications and load factor */
	public static final String ALLOW_MODIFY = "Y";

	public static final String NOT_ALLOW_MODIFY_ = "N";

	public static interface Reports {
		public static interface CCTransactionPrivileges {
			public static final String CCTRANSACTION_MENU = "rpt.cc.txn";
			public static final String CHARGE = "rpt.cc.txn.charge";
			public static final String REFUND = "rpt.cc.txn.refund";
			public static final String PENDING_REFUND = "rpt.cc.txn.refund.pending";
		}
	}

	// Hub Details screen attributes
	public static final String REQ_HTML_HUBS = "reqHubs";
	public static final String REQ_HTML_CARRIER_CODES = "reqCarrierCodes";

	// Hub Details btn privileges
	public static final String PRIV_SYS_MAS_HUB_DET = "sys.mas.hub";
	public static final String PRIV_SYS_MAS_HUB_DET_ADD = "sys.mas.hub.add";
	public static final String PRIV_SYS_MAS_HUB_DET_EDIT = "sys.mas.hub.edit";
	public static final String PRIV_SYS_MAS_HUB_DET_DELETE = "sys.mas.hub.delete";

	// GDS privileges
	public static final String PRIV_SYS_MAS_GDS = "sys.mas.gds";
	public static final String PRIV_SYS_MAS_GDS_ADD = "sys.mas.gds.add";
	public static final String PRIV_SYS_MAS_GDS_EDIT = "sys.mas.gds.edit";
	public static final String PRIV_SYS_GDS_AUTOPUBLISH_VIEW = "sys.mas.gds.autopublish.view";
	public static final String PRIV_SYS_GDS_AUTOPUBLISH_EDIT = "sys.mas.gds.autopublish.edit";

	// inventory
	public static final String REQ_HTML_USR_NOTES = "reqUserNotes";
	public static final String REQ_NO_OF_CARRIERS = "reqNoofCarriers";

	// Seat Map data

	public static final String SEAT_CHRAGES_DATA = "reqSeatCharges";
	public static final String SEAT_TEMPL_CODE = "reqSeatTempl";
	public static final String SEAT_MODEL_DATA = "reqFlightModel";
	public static final String SEAT_MAP_DATA = "reqSeatMap";
	public static final String SEAT_SEGMENT_DATA = "reqSeatSegment";
	public static final String SEAT_EDIT_DATA = "reqSeatEdit";
	public static final String SEAT_SCED_STATS = "reqIsSCED";
	public static final String MEAL_TEMPL_CODE = "reqMealTempl";
	public static final String BAGGAGE_TEMPL_CODE = "reqBaggageTempl";

	// privilegse
	public static final String MAS_SEAT_CHARGE_ADD = "mas.seat.charge.add";
	public static final String MAS_SEAT_CHARGE_EDIT = "mas.seat.charge.edit";
	public static final String MAS_SEAT_CHARGE_CANCELL = "mas.seat.charge.cancel";
	public static final String MAS_SEAT_CHARGE_SEAT = "mas.seat.charge.map";
	public static final String MAS_CURR_PAY_ENABLE = "rqPayenable";
	
	//privilages ancillary charges on local currency 
	public static final String MAS_BG_CHRG_LOCL_CURR="mas.baggage.chrg.locl.currency";
	public static final String MAS_MEAL_CHRG_LOCL_CURR="mas.meal.chrg.locl.currency";
	public static final String MAS_SEAT_CHRG_LOCL_CURR="mas.seat.chrg.locl.currency";
	public static final String MAS_SSR_CHRG_LOCL_CURR="mas.ssr.chrg.locl.currency";

	public static final String REQ_BG_CHRG_LOCL_CURR="reqPrivBgLoclCUrr";
	public static final String REQ_MEAL_CHRG_LOCL_CURR="reqPrivMealLoclCUrr";
	public static final String REQ_SEAT_CHRG_LOCL_CURR="reqPrivSeatLoclCUrr";
	public static final String REQ_BUNDLE_TEMPLATE_ENABLE="reqBundleDescTemplateEnabled";
	public static final String REQ_SSR_CHRG_LOCL_CURR="reqPrivSsrLoclCUrr";
	
	// Change card pay
	public static final String PRIV_SYS_MAS_CURRENCY_ENABLE_CCPAY = "sys.mas.currency.enable.ccpay";

	public static final String MEAL_SEGMENT_DATA = "reqMealSegment";
	public static final String MEAL_SCED_STATS = "reqIsSCED";

	public static final String BAGGAGE_SEGMENT_DATA = "reqBaggageSegment";
	public static final String BAGGAGE_SCED_STATS = "reqIsSCED";
	public static final String SHOW_BAGGAGE_ALLOWANCE = "reqShowBagAllowance";
	public static final String SHOW_BAGGAGE = "reqShowBaggage";

	public static final String REPORT_FORMAT_OPTIONS = "rptFormatOption";

	// Cash Sales Report View
	public static final String REQ_VIEW_REPORT_STATUS = "reqViewReportStatus";

	// shceduled Reports
	public static final String REQ_SCHEDULED_REPORT_NAMES = "reqScheduledReportNames";

	// agent transaction report
	public static final String REQ_AGENT_TRANSACTION_SEARCH_FULL = "priviSearchFullAllowed";

	// Ond list publish respose
	public static final String REQ_OND_UPDATE_STATUS = "reqUpdateStatus";
	// SSR Code List
	public static final String REQ_HTML_SSR_CODE_LIST = "reqSSRCodeList";
	public static final String REQ_HTML_APT_CODE_LIST = "reqAPTCodeList";

	// INFANT PAX PARENT
	public static final String REQ_PAX_PARENT = "reqPaxParent";

	public static final String REQ_SSR_CODE_WITH_CATEGORY = "reqSsrAndCategoryList";

	public static final String REQ_HTML_TERMS_TEMPLATE_NAME_LIST = "reqTermsTemplateName";
	
	public static final String REQ_HTML_VOUCHER_TERMS_TEMPLATE_NAME_LIST = "reqVoucherTermsTemplateName";

	public interface ReportFormatType {
		public static final String EXCEL_FORMAT = ".xls";
		public static final String PDF_FORMAT = ".pdf";
		public static final String CSV_FORMAT = ".csv";
	}
	
	public interface RouteDesignators {
		public static final String OUTBOUND = "OB";
		public static final String INBOUND = "IB";
	}

	public interface Delimeters {
		public static final String COMMA_WITH_SPACE = ", ";
	}

	public interface PromotionReportsType {
		public static final String PROMOTION_REQUEST_SUMMARY = "Promotion Request Summary";
		public static final String PROMO_NEXT_SEAT_FREE_SUMMARY = "Next Seat Summary";
		public static final String PROMO_FLEXI_DATE_SUMMARY = "Flexi Date Summary";
		public static final String PROMO_NEXT_SEAT_FREE_SUMMARY_FOR_CREW = "Promotion Request Summary For Crew";
	}

	public static final String REQ_PRO_REPORT_LIST = "reqPromotionsReportList";
	public static final String ANCI_OFFER_TEMPLATE_TYPES = "reqAnciOfferTypes";
	public static final String REQ_ALL_DESIGNATOR_STRING = "reqAllDesignatorString";
	public static final String REQ_ALL_DESIGNATOR_INT = "reqAllDesignatorInt";

	public static final String ALLOW_GROUP_BOOKING_FOR_ALL_STATIONS = "um.airadmin.tool.station";

	public static final String ADMIN_LOGIN_CAPTCHA = "isCaptchaValidated";

	//ETickets
	public static final String SHOW_ETICKET_OPENING_SECTION = "showETOpeningSection";

        public static final String REQ_HTML_FLIGHTNUMBER_LIST = "reqFlightNoList";
	
	public static final String REQ_ENTITIES = "reqEntities";
	
	public static final String REQ_HAS_ROLL_CANCLE_BATCH_PRIVILEGE = "hasRollCancleBatchPrivilege";
	
}
