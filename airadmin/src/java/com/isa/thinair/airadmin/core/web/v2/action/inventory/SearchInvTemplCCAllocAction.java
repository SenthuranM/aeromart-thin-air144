package com.isa.thinair.airadmin.core.web.v2.action.inventory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airinventory.api.dto.inventory.InvTempCCAllocDTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class SearchInvTemplCCAllocAction extends BaseRequestAwareAction {

	Log log = LogFactory.getLog(SearchInvTemplCCAllocAction.class);

	private String airCraftModel;

	private int pageSize = 20;

	private int page;

	private int records;

	private int total;

	private int invTempID;

	private ArrayList<Map<String, Object>> invTemplateCCAllocList;

	public String execute() {
		
		boolean isEdited = false;
		
		if (invTempID > 0) {
			isEdited = true;
		} else {
			isEdited =false;
		}
		int start = (page - 1) * 20;

		Page<InvTempCCAllocDTO> invTempCCAllocPage = ModuleServiceLocator.getInventoryTemplateBD().searchInvTemplCCAlloc(start,
				pageSize, airCraftModel, isEdited, invTempID);

		setInvTemplateCCAllocList(getInvTempCCAllocDTOList((List<InvTempCCAllocDTO>) invTempCCAllocPage.getPageData()));
		this.setRecords(invTempCCAllocPage.getTotalNoOfRecords());
		this.total = invTempCCAllocPage.getTotalNoOfRecordsInSystem() / 20;
		int mod = invTempCCAllocPage.getTotalNoOfRecordsInSystem() % 20;
		if (mod > 0) {
			this.total = this.total + 1;
		}
		return S2Constants.Result.SUCCESS;
	}

	public static ArrayList<Map<String, Object>> getInvTempCCAllocDTOList(List<InvTempCCAllocDTO> invTemplCCAllocList) {
		ArrayList<Map<String, Object>> invTempCCAllocDTOList = new ArrayList<Map<String, Object>>();
		InvTempCCAllocDTO invTempCCAllocDTO = null;
		int i = 0;
		Iterator<InvTempCCAllocDTO> iterator = invTemplCCAllocList.iterator();
		while (iterator.hasNext()) {
			invTempCCAllocDTO = iterator.next();
			Map<String, Object> row = new HashMap<String, Object>();
			row.put("invTempCCAllocDTO", invTempCCAllocDTO);
			row.put("id", i++);
			invTempCCAllocDTOList.add(row);
		}

		return invTempCCAllocDTOList;
	}

	public String getAirCraftModel() {
		return airCraftModel;
	}

	public void setAirCraftModel(String airCraftModel) {
		this.airCraftModel = airCraftModel;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public ArrayList<Map<String, Object>> getInvTemplateCCAllocList() {
		return invTemplateCCAllocList;
	}

	public void setInvTemplateCCAllocList(ArrayList<Map<String, Object>> invTemplateCCAllocList) {
		this.invTemplateCCAllocList = invTemplateCCAllocList;
	}

	public int getInvTempID() {
		return invTempID;
	}

	public void setInvTempID(int invTempID) {
		this.invTempID = invTempID;
	}

}
