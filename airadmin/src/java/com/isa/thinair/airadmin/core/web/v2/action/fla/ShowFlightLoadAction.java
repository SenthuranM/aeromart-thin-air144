package com.isa.thinair.airadmin.core.web.v2.action.fla;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.master.DemoHG;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airadmin.core.web.v2.dto.mis.MISProcessParams;
import com.isa.thinair.airadmin.core.web.v2.util.CommonUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Results({ @Result(name = AdminStrutsConstants.AdminAction.SUCCESS, value = S2Constants.Jsp.FLA.FLA_FLIGHT_LOAD),
		@Result(name = AdminStrutsConstants.AdminAction.ERROR, value = AdminStrutsConstants.AdminJSP.ERROR_JSP) })
public class ShowFlightLoadAction extends BaseRequestAwareAction {

	public String execute() throws Exception {
		String strForward = WebConstants.FORWARD_SUCCESS;
		try {
			setDisplayData(request);
			MISProcessParams mpParams = new MISProcessParams(request);
			request.setAttribute("req_InitialParams", CommonUtil.convertToJSON(mpParams));
		} catch (Exception ex) {
			strForward = WebConstants.FORWARD_ERROR;
		}
		return strForward;
	}

	private static void setDisplayData(HttpServletRequest request) throws ModuleException {

		Collection collection = ModuleServiceLocator.getAirportServiceBD().getAirports();
		StringBuffer sbFlightPerform = new StringBuffer();
		sbFlightPerform.append(" var jsFlghtPerformance = ");
		sbFlightPerform.append(AppSysParamsUtil.getFlaFlightPerformance());
		sbFlightPerform.append(";");
		int period = AppSysParamsUtil.getReportingPeriod().intValue();
		request.setAttribute("req_fla_startDay", "var flaStartDate = '" + ReportsHTMLGenerator.getReportingPeriod(period) + "';");
		request.setAttribute("req_fla_performance", sbFlightPerform.toString());
		request.setAttribute("req_Airports", DemoHG.createAirportList(collection));
	}
}
