package com.isa.thinair.airadmin.core.web.generator.common;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.AiradminModuleConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airmaster.api.model.AircraftCabinCapacity;
import com.isa.thinair.airmaster.api.model.AircraftModel;
import com.isa.thinair.airmaster.api.model.Language;
import com.isa.thinair.airmaster.api.model.SITAAddress;
import com.isa.thinair.airreservation.api.dto.CreditCardInformationDTO;
import com.isa.thinair.airreservation.api.dto.UserDetails;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.DCS_PAX_MSG_GROUP_TYPE;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airsecurity.api.model.Privilege;
import com.isa.thinair.airsecurity.api.model.PrivilegeCategory;
import com.isa.thinair.airsecurity.api.model.PrivilegeDependantsDTO;
import com.isa.thinair.airsecurity.api.utils.Constants;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.dto.HubAirportTO;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.webplatform.api.util.PrivilegeUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

public final class JavascriptGenerator {

	private static Log log = LogFactory.getLog(JavascriptGenerator.class);
	private static AiradminConfig airadminConfig = new AiradminConfig();
	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();

	private static final String YES = "on";
	private static final String NO = "off";
	private static final String COMMA_SEPERATOR = ",";

	public final static String createHubHtml() {
		StringBuilder jsb = new StringBuilder("var hubs = new Array();");
		Collection<HubAirportTO> hubList = globalConfig.getHubsMap().values();

		if (hubList != null && !hubList.isEmpty()) {
			int count = 0;
			for (HubAirportTO hubAirportTO : hubList) {
				HubAirportTO hubTo = hubAirportTO;
				jsb.append("hubs[" + count + "] = '" + hubTo.getAirportCode() + "';");
				count++;
			}
		}
		return jsb.toString();
	}

	// method to load the adult child configuration
	public final static String createPaxFareParams() {
		StringBuilder jsb = new StringBuilder("var arrType = new Array();");

		Collection<PaxTypeTO> paxTypeList = globalConfig.getPaxTypes();

		if (paxTypeList != null && !paxTypeList.isEmpty()) {
			int x = 0;
			for (PaxTypeTO type : paxTypeList) {
				if (type != null) {
					jsb.append(" arrType[" + x + "] = new Array('" + type.getPaxTypeCode() + "','" // pax type
							+ type.getDescription() + "','" // description
							+ String.valueOf(type.getVersion()) + "','" // version
							+ type.getFareAmountType() + "','" // amount
							+ AccelAeroCalculator.formatAsDecimal(type.getFareAmount()) + "','" // fare amount
							+ ((type.isApplyFare() == true) ? YES : NO) + "','" // apply fare
							+ ((type.isApplyModCharge() == true) ? YES : NO) + "','" // mod
							+ ((type.isApplyCnxCharge() == true) ? YES : NO) + "','" // cnx
							+ ((type.isFareRefundable() == true) ? YES : NO) + "','" // refundable
							+ ((type.isXbeBookingsAllowed() == true) ? YES : NO) + "','" // xbe booking allowed
							+ String.valueOf(type.getCutOffAgeInYears()) + "','-1','','" // age
							+ AccelAeroCalculator.formatAsDecimal(type.getNoShowChargeAmount()) + "','','','','0.00');");
					x++;
				}
			}
		}

		return jsb.toString();
	}

	/**
	 * Create Users Multi Select Box for Given Travel Agent
	 * 
	 * @param strTavelAgent
	 *            the Travel agent
	 * @return String the List Box of Users
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final static String createUsersForTavelAgentHtml(String strTavelAgent) throws ModuleException {

		Collection<UserDetails> usersCollec = null;
		StringBuffer jsb = new StringBuffer("var arrGroup1 = new Array();");
		jsb.append("arrGroup1[0] = '' ;");
		jsb.append("var arrData = new Array();");
		jsb.append("var users = new Array();");

		usersCollec = ModuleServiceLocator.getReservationAuxilliaryBD().getUsersForTravelAgent(strTavelAgent);

		int tempInt = 0;
		UserDetails user = null;
		if (usersCollec != null) {
			Iterator<UserDetails> usersIter = usersCollec.iterator();
			while (usersIter.hasNext()) {
				user = usersIter.next();

				jsb.append("users[" + tempInt + "] = new Array('" + user.getDisplayName() + "','" + user.getUserId() + "'); ");
				tempInt++;
			}
		}

		jsb.append("arrData[0] = users; arrData[1] = new Array();");
		jsb.append("var ls = new Listbox('lstUsers', 'lstSelectedUsers', 'spn1');");
		jsb.append("ls.group1 = arrData[0];");
		jsb.append("ls.width = '130px';");
		jsb.append("ls.height = '110px';");
		jsb.append("ls.headingLeft = '&nbsp;&nbsp;All Users';");
		jsb.append("ls.headingRight = '&nbsp;&nbsp;Selected Users';");
		jsb.append("ls.drawListBox();");

		return jsb.toString();
	}

	/**
	 * Create Credit Card Types Multi Select Box
	 * 
	 * @return String the Cards multi Select Box
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final static String createCardTypeHtml() throws ModuleException {

		Collection<CreditCardInformationDTO> cardsCollec = null;
		StringBuffer jsb = new StringBuffer("var arrGroup1 = new Array();");
		jsb.append("arrGroup1[0] = '' ;");
		jsb.append("var arrData = new Array();");
		jsb.append("var cards = new Array();");

		cardsCollec = ModuleServiceLocator.getReservationAuxilliaryBD().getActiveCards();
		int tempInt = 0;
		CreditCardInformationDTO creditCardTypes = null;
		if (cardsCollec != null) {
			Iterator<CreditCardInformationDTO> cardTypesIter = cardsCollec.iterator();
			while (cardTypesIter.hasNext()) {
				creditCardTypes = cardTypesIter.next();
				jsb.append("cards[" + tempInt + "] = new Array('" + creditCardTypes.getCardType() + "','"
						+ creditCardTypes.getId() + "');");
				tempInt++;
			}
		}

		jsb.append(" arrData[0] = cards;  arrData[1] = new Array();");
		jsb.append("var ls = new Listbox('lstCardTypes', 'lstSelectedCardTypes', 'spn1');");
		jsb.append("ls.group1 = arrData[0];");
		jsb.append("ls.width = '130px'; ls.height = '110px';");
		jsb.append("ls.headingLeft = '&nbsp;&nbsp;All Card Types';");
		jsb.append("ls.headingRight = '&nbsp;&nbsp;Selected Card Types';");
		jsb.append("ls.drawListBox();");

		return jsb.toString();
	}

	/**
	 * Creates Sita Multi Select for a Given Airport
	 * 
	 * @param airportCode
	 *            the Airport
	 * @return String the SITA list box
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final static String createSITAHtml(String airportCode, String carrierCode, DCS_PAX_MSG_GROUP_TYPE msgType) throws ModuleException {

		Collection<SITAAddress> sitaCollec = null;
		StringBuffer jsb = new StringBuffer("var arrGroup1 = new Array();");
		jsb.append("arrGroup1[0] = '' ;");
		jsb.append("var arrData = new Array();");
		jsb.append("var sitas = new Array();");

		sitaCollec = ModuleServiceLocator.getAirportServiceBD().getActiveSITAAddresses(airportCode, carrierCode, msgType);
		int tempInt = 0;
		SITAAddress sitaAddress = null;
		if (sitaCollec != null) {
			Iterator<SITAAddress> sitaIter = sitaCollec.iterator();
			while (sitaIter.hasNext()) {
				sitaAddress = sitaIter.next();
				jsb.append("sitas[" + tempInt + "] = new Array('" + sitaAddress.getSitaAddress() + "','"
						+ sitaAddress.getSitaAddress() + "'); ");
				tempInt++;
			}
		}
		jsb.append("arrData[0] = sitas; arrData[1] = new Array();");
		jsb.append("var ls = new Listbox('lstSITAAddresses', 'lstAssignedSITAAdresses', 'spn1');");
		jsb.append("ls.group1 = arrData[0];");
		jsb.append("ls.width = '150px'; ls.height = '150px';");
		jsb.append("ls.headingLeft = '&nbsp;&nbsp;All SITA Addresses';");
		jsb.append("ls.headingRight = '&nbsp;&nbsp;Selected SITA Addresses';");
		jsb.append("ls.drawListBox();");

		return jsb.toString();
	}

	/**
	 * Creates Booking Code Multiselect Box
	 * 
	 * @return String the Booking codes Multi Select
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final static String createBookingCodeHtml() throws ModuleException {
		StringBuffer jsb = new StringBuffer("var arrGroup1 = new Array();");
		jsb.append(" arrGroup1[0] = '' ; var arrData = new Array();");
		jsb.append(" var bcs = new Array();");

		List<Map<String, String>> lstBc = SelectListGenerator.createBookingClasses();
		Iterator<Map<String, String>> iteBc = lstBc.iterator();
		int temp = 0;
		if (lstBc != null && !lstBc.isEmpty()) {
			while (iteBc.hasNext()) {
				Map<String, String> keyValues = iteBc.next();
				String code = keyValues.get("BOOKING_CODE");
				jsb.append("bcs[" + temp + "] = new Array('" + code + "','" + code + "');");
				temp++;
			}
		}
		jsb.append("arrData[0] = bcs; arrData[1] = new Array();");
		jsb.append("var arrGroup2 = new Array();");
		jsb.append("var arrData2 = new Array();");
		jsb.append("arrData2[0] = new Array();");
		jsb.append("var bcls = new Listbox('lstBcs', 'lstAssignedBcs', 'spnBC','bcls');");
		jsb.append("bcls.group1 = arrData[0];" + "bcls.list2 = arrData2;");
		jsb.append("bcls.headingLeft = '&nbsp;&nbsp;Booking Classes';");
		jsb.append("bcls.headingRight = '&nbsp;&nbsp;Assigned Booking Classes';");

		return jsb.toString();
	}

	/**
	 * Creates Booking Code Multiselect Box
	 * 
	 * @return String the Booking codes Multi Select
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final static String createBookingCodeWithCosHtml() throws ModuleException {
		StringBuffer jsb = new StringBuffer("var arrGroup1 = new Array();");
		jsb.append(" arrGroup1[0] = '' ; var arrData = new Array();");
		jsb.append(" var bcs = new Array();");

		Collection<String[]> colBCList = SelectListGenerator.getBookingClassCodeList();
		Iterator<String[]> iteBc = colBCList.iterator();
		int temp = 0;
		if (colBCList != null && !colBCList.isEmpty()) {
			while (iteBc.hasNext()) {
				String[] keyValues = iteBc.next();
				jsb.append("bcs[" + temp + "] = new Array('" + keyValues[0] + "','" + keyValues[0] + "','" + keyValues[2] + "','"
						+ keyValues[1] + "');");
				temp++;
			}
		}
		jsb.append("arrData[0] = bcs; arrData[1] = new Array();");
		jsb.append("var arrGroup2 = new Array();");
		jsb.append("var arrData2 = new Array();");
		jsb.append("arrData2[0] = new Array();");
		jsb.append("var bcls = new Listbox('lstBcs', 'lstAssignedBcs', 'spnBC','bcls');");
		jsb.append("bcls.group1 = arrData[0];" + "bcls.list2 = arrData2;");
		jsb.append("bcls.headingLeft = '&nbsp;&nbsp;Booking Classes';");
		jsb.append("bcls.headingRight = '&nbsp;&nbsp;Assigned Booking Classes';");

		return jsb.toString();
	}

	/**
	 * Creates SSR Code Multiselect Box
	 * 
	 * @return String the SSR codes Multi Select
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final static String createSSRCodesHtml() throws ModuleException {
		StringBuffer jsb = new StringBuffer("var arrGroup1 = new Array();");
		jsb.append(" arrGroup1[0] = '' ; var arrData = new Array();");
		jsb.append(" var ssr = new Array();");

		List<Map<String, String>> lstSsr = SelectListGenerator.createSSRCodeList();
		Iterator<Map<String, String>> iteBc = lstSsr.iterator();
		int temp = 0;
		if (lstSsr != null && !lstSsr.isEmpty()) {
			while (iteBc.hasNext()) {
				Map<String, String> keyValues = iteBc.next();
				String code = keyValues.get("SSR_CODE");
				jsb.append("ssr[" + temp + "] = new Array('" + code + "','" + code + "');");
				temp++;
			}
		}
		jsb.append("arrData[0] = ssr; arrData[1] = new Array();");
		jsb.append("var arrGroup2 = new Array();");
		jsb.append("var arrData2 = new Array();");
		jsb.append("arrData2[0] = new Array();");
		jsb.append("var ssrs = new Listbox('lstSsr', 'lstSelectedSsr', 'spnSSR','ssrs');");
		jsb.append("ssrs.group1 = arrData[0];" + "ssrs.list2 = arrData2;");
		jsb.append("ssrs.headingLeft = '&nbsp;&nbsp;SSR Codes';");
		jsb.append("ssrs.headingRight = '&nbsp;&nbsp;Selected SSR Codes';");

		return jsb.toString();
	}
	
	public final static String createAirportTransferCodesHtml() throws ModuleException {
		StringBuffer jsb = new StringBuffer("var arrGroup1 = new Array();");
		jsb.append(" arrGroup1[0] = '' ; var arrData = new Array();");
		jsb.append(" var ssr = new Array();");

		List<Map<String, String>> lstSsr = SelectListGenerator.createAirportTransferCodeList();
		Iterator<Map<String, String>> iteBc = lstSsr.iterator();
		int temp = 0;
		if (lstSsr != null && !lstSsr.isEmpty()) {
			while (iteBc.hasNext()) {
				Map<String, String> keyValues = iteBc.next();
				String code = keyValues.get("SSR_CODE");
				jsb.append("ssr[" + temp + "] = new Array('" + code + "','" + code + "');");
				temp++;
			}
		}
		jsb.append("arrData[0] = ssr; arrData[1] = new Array();");
		jsb.append("var arrGroup2 = new Array();");
		jsb.append("var arrData2 = new Array();");
		jsb.append("arrData2[0] = new Array();");
		jsb.append("var ssrs = new Listbox('lstApt', 'lstSelectedApt', 'spnApt','ssrs');");
		jsb.append("ssrs.group1 = arrData[0];" + "ssrs.list2 = arrData2;");
		jsb.append("ssrs.headingLeft = '&nbsp;&nbsp;Airport Transfer Codes';");
		jsb.append("ssrs.headingRight = '&nbsp;&nbsp;Selected Airport Transfer Codes';");

		return jsb.toString();
	}

	/**
	 * Creates the Privileges Multi Select Box
	 * 
	 * @return String the Privileges Multi Select Box
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final static String createPrivilegeHtml(boolean sysUser) throws ModuleException {

		StringBuffer jsb = new StringBuffer();
		Collection<Privilege> privilegesCollec = null;
		Collection<PrivilegeCategory> privilegeCategoryCollec = null;

		privilegeCategoryCollec = ModuleServiceLocator.getSecurityUtilBD().getPrivilegeCategories();
		Iterator<PrivilegeCategory> catIter = privilegeCategoryCollec.iterator();
		Iterator<PrivilegeCategory> listIter = privilegeCategoryCollec.iterator();
		PrivilegeCategory tempCategory = null;

		int tempCat = 0;
		int tempInt = 0;
		Privilege privilege = null;
		Iterator<Privilege> privIter = null;
		if (sysUser) {
			privilegesCollec = ModuleServiceLocator.getSecurityBD().getActivePrivileges();
		} else {
			privilegesCollec = ModuleServiceLocator.getSecurityBD().getActiveVisiblePrivileges();
		}

		jsb.append(" var privis = new Array(); var privArray = new Array();");

		while (catIter.hasNext()) {
			tempCategory = catIter.next();
			// Add the Node Privileges
			jsb.append("privis[" + tempCat + "] = '" + tempCategory.getPrivilegeCategoryDesc() + "';");

			tempInt = 0;
			privIter = privilegesCollec.iterator();
			tempCategory = listIter.next();
			jsb.append("var pritem_" + tempCat + " = new Array(); ");

			// Add leaf privileges & Description
			if (privilegesCollec.size() == 0) {
				jsb.append("new Array();");
			}
			while (privIter.hasNext()) {
				privilege = privIter.next();
				if (privilege.getPrivilegeCategoryID().equals(tempCategory.getPrivilegeCategoryId())) {
					jsb.append(" pritem_" + tempCat + "[" + tempInt + "] = new Array('" + privilege.getPrivilege() + "','"
							+ privilege.getPrivilegeId() + "','" + privilege.getServiceChannel() + "'); ");
					tempInt++;
				}
			}
			jsb.append("privArray[" + tempCat + "] = pritem_" + tempCat + ";");
			tempCat++;
		}

		jsb.append("var arrGroup1 = privis; var arrGroup2 = new Array();");
		jsb.append("var arrData2 = new Array();");
		jsb.append("arrData2[0] = new Array();");
		jsb.append("var ls = new Listbox2('lstRoles', 'lstAssignedRoles', 'spn1');");
		jsb.append("checkMoveValidity= true; ");
		jsb.append("ls.group1 = arrGroup1; ls.group2 = arrGroup2;");
		jsb.append("ls.list1 = privArray; ls.list2 = arrData2;");
		jsb.append("ls.height = '310px'; ls.width = '200px';");
		jsb.append("ls.headingLeft = '&nbsp;&nbsp;Privileges';");
		jsb.append("ls.headingRight = '&nbsp;&nbsp;Assigned Privileges';");
		jsb.append("ls.ancestorArray = dependancy;");
		jsb.append("ls.drawListBox();");

		return jsb.toString();
	}

	/**
	 * Creates the Dependency Array for Privileges (new improved method not been used)
	 * 
	 * @return String the Array of dependencies
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final static String createPrivilegeDependancyHtml() throws ModuleException {
		StringBuffer jsb = new StringBuffer();
		Privilege privilege = null;
		String strDependancy = "";
		int count = 0;

		Collection<Privilege> privilegesCollec = ModuleServiceLocator.getSecurityBD().getActivePrivileges(
				Constants.INTERNAL_CHANNEL);
		Collection<String> colPrivilegesIDs = new ArrayList<String>();
		jsb.append("var dependancy = new Array(" + privilegesCollec.size() + ");");
		Iterator<Privilege> iter = privilegesCollec.iterator();
		while (iter.hasNext()) {
			privilege = iter.next();
			colPrivilegesIDs.add(privilege.getPrivilegeId());
		}

		HashMap<String, List<String>> privilagesMap = ModuleServiceLocator.getSecurityBD().getPrivilegeDependencies(
				colPrivilegesIDs, Constants.INTERNAL_CHANNEL);
		for (String string : privilagesMap.keySet()) {

			String privId = string;
			strDependancy += "dependancy[" + count + "] = new Array('" + privId + "',";
			Collection<String> ansPrivs = privilagesMap.get(privId);
			for (String string2 : ansPrivs) {
				String dependentPriv = string2;
				strDependancy += " '" + dependentPriv + "',";
			}

			strDependancy = strDependancy.substring(0, strDependancy.lastIndexOf(","));
			strDependancy += ");";
			count++;
		}

		jsb.append(strDependancy);
		return jsb.toString();
	}

	/**
	 * Creates the Dependency Array for Privileges (the Method used in the system)
	 * 
	 * @return String the Dependency array
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final static String createPrivilegeDependancyHtmlTemp() throws ModuleException {
		StringBuffer jsb = new StringBuffer();
		Collection<String> dependancyCollec = null;
		Iterator<String> dependancyIter = null;
		Privilege privilege = null;
		int count = 0;
		int idCount = 1;
		PrivilegeDependantsDTO privilegeDependantsDTO = null;
		Collection<Privilege> privilegesCollec = ModuleServiceLocator.getSecurityBD().getActivePrivileges(
				Constants.INTERNAL_CHANNEL);
		Map<String, Privilege> activePrivilegeMap = PrivilegeUtil.getActivePrivilegeMap();

		jsb.append("var dependancy = new Array(" + privilegesCollec.size() + ");");
		Iterator<Privilege> iter = privilegesCollec.iterator();

		while (iter.hasNext()) {
			privilege = iter.next();

			jsb.append("var depitems_" + count + " = new Array();");
			jsb.append("depitems_" + count + "[0] = '" + privilege.getPrivilegeId() + "';");
			privilegeDependantsDTO = ModuleServiceLocator.getSecurityBD().getPrivilegeDependencies(privilege.getPrivilegeId(),
					Constants.INTERNAL_CHANNEL);
			if (privilegeDependantsDTO != null) {
				dependancyCollec = privilegeDependantsDTO.getDependantPrevilegeIds();
				dependancyIter = dependancyCollec.iterator();
				idCount = 1;
				while (dependancyIter.hasNext()) {
					String dependantPriviId = dependancyIter.next();

					if (activePrivilegeMap.get(dependantPriviId) != null) {
						jsb.append("depitems_" + count + "[" + idCount + "] =  '" + dependantPriviId + "';");
						idCount++;
					}
				}
			}
			jsb.append("dependancy[" + count + "] = depitems_" + count + ";");
			count++;
		}
		return jsb.toString();
	}

	/**
	 * Creates Agent Multi select Box by Station with Agent Type Attached. 
	 * It also has agent status as the last element.
	 * 
	 * @return String the Multi Select Box
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final static String createAgentStationMDList() throws ModuleException {

		int tempCat = 0;
		int stcount = 0;
		StringBuffer sb = new StringBuffer("g1 = new Array();");
		sb.append(" var agents = new Array();");

		List<Map<String, String>> lstAgents = SelectListGenerator.createStationCodes();
		Iterator<Map<String, String>> ite = lstAgents.iterator();

		while (ite.hasNext()) {

			Map<String, String> keyValues = ite.next();
			Set<String> keys = keyValues.keySet();
			Iterator<String> keyIterator = keys.iterator();

			while (keyIterator.hasNext()) {

				String agentTpes[] = new String[1];
				String agentType = keyValues.get(keyIterator.next());
				sb.append("g1[" + tempCat + "] = '" + agentType + "'; ");
				sb.append("var stnags_" + tempCat + " = new Array();");
				agentTpes[0] = agentType;
				Collection<String[]> col = SelectListGenerator.createAllAgentsByStation(agentTpes);

				if (col != null) {
					Iterator<String[]> iteAgents = col.iterator();
					stcount = 0;
					while (iteAgents.hasNext()) {
						String str[] = iteAgents.next();

						sb.append("stnags_" + tempCat + "[" + stcount + " ] = new Array('" + str[0] + "','" + str[1] + "','"
								+ str[2] + "');");
						stcount++;
					}
				}
				sb.append("agents[" + tempCat + "] = stnags_" + tempCat + ";");
			}
			tempCat++;
		}
		return sb.toString();
	}

	/**
	 * Create the Cabin Class List
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final static void createCabinClasses(HttpServletRequest request) throws ModuleException {

		Set<String> cabinClassesSet = CommonsServices.getGlobalConfig().getActiveCabinClassesMap().keySet();
		String cabinClasses = "";
		for (String cabinClass : cabinClassesSet) {
			if (cabinClasses.equals("")) {
				cabinClasses = cabinClass;
			} else {
				cabinClasses = cabinClasses + "," + cabinClass;
			}
		}
		request.setAttribute(WebConstants.REQ_CABIN_CLASSES, "'" + cabinClasses + "';");

	}

	/**
	 * Creates the Client validations from a Prperty File
	 * 
	 * @param moduleErrs
	 *            the Property file of client validation keys
	 * @return String the Array of client validation messages
	 * @throws ModuleException
	 *             the ModuleException
	 */
	@SuppressWarnings("rawtypes")
	public static String createClientErrors(Properties moduleErrs) throws ModuleException {

		StringBuffer sb = new StringBuffer("\n");
		Set keySet = moduleErrs.keySet();

		for (Iterator iter = keySet.iterator(); iter.hasNext();) {
			String key = (String) iter.next();
			String jsVarName = moduleErrs.getProperty(key);
			String jsErr = airadminConfig.getMessage(key);

			jsVarName = ("".equals(jsVarName)) ? key : jsVarName;
			sb.append("var " + jsVarName + "='" + jsErr + "';\n");
		}
		return sb.toString();
	}

	/**
	 * Creates the Client validations from a Property File
	 * 
	 * @param moduleErrs
	 *            the Property file of client validation keys
	 * @return List<String[]> of client validation messages
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static List<Map<String, String>> createClientErrorList(Properties moduleErrs) throws ModuleException {

		List<Map<String, String>> errorList = new ArrayList<Map<String, String>>();
		Set keySet = moduleErrs.keySet();

		for (Iterator iter = keySet.iterator(); iter.hasNext();) {
			String key = (String) iter.next();
			String jsVarName = moduleErrs.getProperty(key);
			String jsErr = airadminConfig.getMessage(key);

			jsVarName = ("".equals(jsVarName)) ? key : jsVarName;
			Map<String, String> error = new HashMap<String, String>();
			error.put(jsVarName, jsErr);
			errorList.add(error);
		}
		return errorList;
	}

	/**
	 * Create Aircraft Models Details
	 * 
	 * @return String the Array of Airvraft Models
	 * @throws ModuleException
	 */
	public final static String createAircraftModelDtls() throws ModuleException {

		Collection<AircraftModel> aircraftModelCol = null;
		aircraftModelCol = ModuleServiceLocator.getAircraftServiceBD().getModels();
		AircraftModel aircraftModel = null;
		StringBuffer jsb = new StringBuffer("var arrAircraftModelData = new Array();");

		int i = 0;
		Iterator<AircraftModel> aircraftModelsIter = aircraftModelCol.iterator();
		while (aircraftModelsIter.hasNext()) {
			aircraftModel = aircraftModelsIter.next();

			Collection<AircraftCabinCapacity> capasitys = aircraftModel.getAircraftCabinCapacitys();
			Iterator<AircraftCabinCapacity> itCaps = capasitys.iterator();
			int infantCap = 0;
			int seatCap = 0;
			String cabinClassCodes = "";

			while (itCaps.hasNext()) {
				AircraftCabinCapacity cpasity = itCaps.next();
				infantCap = infantCap + cpasity.getInfantCapacity();
				seatCap = seatCap + cpasity.getSeatCapacity();
				cabinClassCodes += (cpasity.getCabinClassCode() == null ? "" : cpasity.getCabinClassCode() + "#");
			}

			jsb.append("arrAircraftModelData[" + i + "] = new Array('" + aircraftModel.getModelNumber() + "','Base Capacity "
					+ seatCap + " / Infant Capacity " + infantCap + "', '" + cabinClassCodes + "', '" + aircraftModel.getStatus()
					+ "', '" + aircraftModel.getAircraftTypeCode() + "', '" + aircraftModel.getDescription() + "'); ");
			i++;
		}
		return jsb.toString();
	}

	/**
	 * Create the Minimum stop over time for Airports
	 * 
	 * @return String the Create Minimum stop over times
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final static String createMinStopOverTime() throws ModuleException {

		String strMinStopOverTimeHtml = "var arrMinStopOverTimeData = new Array();";
		strMinStopOverTimeHtml += SelectListGenerator.createMinStopoverTime();
		return strMinStopOverTimeHtml;

	}

	/**
	 * Sets the Server Error to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @param strErrorMsg
	 *            the Error Message
	 * @param strFrame
	 *            the frame contatins the message box
	 * @param strReturnPage
	 *            the Page
	 */
	public static void setServerError(HttpServletRequest request, String strErrorMsg, String strFrame, String strReturnPage) {

		request.setAttribute(WebConstants.REQ_ERROR_REDIRECT, strReturnPage);
		request.setAttribute(WebConstants.REQ_ERROR_FRAME, strFrame);
		request.setAttribute(WebConstants.REQ_ERROR_SERVER_MESSAGES, strErrorMsg);
	}

	/**
	 * Gets a Formatted System Date
	 * 
	 * @return String the Formatted System Date
	 * @throws ModuleException
	 *             the ModuleException
	 * @author Srikanth
	 */
	public static String getSystemDate() throws ModuleException {

		Date systemDate = new Date();
		SimpleDateFormat dtfmt = new SimpleDateFormat("dd/MM/yyyy");
		String systemDateFormatted = null;

		try {
			systemDateFormatted = dtfmt.format(systemDate);
		} catch (Exception ex) {
			log.error("error in parsing system date" + ex.getMessage());
		}
		String strSystemDateJS = "var systemDate = '" + systemDateFormatted + "';";
		return strSystemDateJS;
	}

	/**
	 * Creates the Default Values for the Menu (top)
	 * 
	 * @return String the Array with default Data
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String createDefaultValuesHtml() throws ModuleException {
		StringBuffer strbData = new StringBuffer();
		Date dtCurrent = new Date();
		SimpleDateFormat smpDtFormat = new SimpleDateFormat("dd/MM/yyyy");

		strbData.append("arrParams[0] = '" + smpDtFormat.format(dtCurrent) + "';");
		strbData.append("arrParams[6] = '" + ReservationInternalConstants.PassengerType.INFANT.toString() + "';");

		return strbData.toString();
	}

	/**
	 * Creates Segment Booking Code List
	 * 
	 * @param segmentsBCInvsMap
	 *            the map containing the Segments
	 * @return String the Array of Booking Class
	 */
	public static String createSegmentsBookingCodesListHTML(LinkedHashMap<String, List<Object[]>> segmentsBCInvsMap) {
		StringBuffer strArrays = new StringBuffer();
		strArrays.append("var segmentCodes = new Array();");
		strArrays.append("var bookingCodes = new Array();");
		Iterator<String> segmentsIt = segmentsBCInvsMap.keySet().iterator();
		int segmentsCount = 0;
		while (segmentsIt.hasNext()) {
			String segmentCode = segmentsIt.next();
			List<Object[]> bookingClasses = segmentsBCInvsMap.get(segmentCode);
			if (bookingClasses != null && !bookingClasses.isEmpty()) {

				Map<String, List<Object[]>> logicalCCWiseBookingClasses = getLogicalCabinClassWiseBookingClasses(bookingClasses);

				for (String lccCode : logicalCCWiseBookingClasses.keySet()) {
					List<Object[]> lccBookingClasses = logicalCCWiseBookingClasses.get(lccCode);
					strArrays.append("segmentCodes[" + segmentsCount + "]='" + segmentCode + " - " + lccCode + "';");
					strArrays.append("bookingCodes[" + segmentsCount + "]= new Array();");
					for (int i = 0; i < lccBookingClasses.size(); i++) {
						Object[] bcInv = lccBookingClasses.get(i);
						String bookingCode = (String) bcInv[0];
						String value = bookingCode;
						if (((String) bcInv[1]).equals("Y")) {
							value += " [Standard]";
						} else if (((String) bcInv[2]).equals("Y")) {
							value += " [Fixed]";
						} else {
							value += " [NonStandard]";
						}
						strArrays.append("bookingCodes[" + segmentsCount + "][" + i + "]= new Array();");
						strArrays.append("bookingCodes[" + segmentsCount + "][" + i + "][1]= '" + value + "';");
						strArrays.append("bookingCodes[" + segmentsCount + "][" + i + "][0]= '" + value + "';");
					}
					++segmentsCount;
				}
			}
		}
		return strArrays.toString();
	}

	/**
	 * @param bookingClasses
	 * @return
	 */
	private static Map<String, List<Object[]>> getLogicalCabinClassWiseBookingClasses(List<Object[]> bookingClasses) {
		Map<String, List<Object[]>> logicalCCWiseBookingClasses = new HashMap<String, List<Object[]>>();

		for (Object[] bookingClass : bookingClasses) {

			List<Object[]> lccWiseBCs = logicalCCWiseBookingClasses.get(bookingClass[3].toString());
			if (lccWiseBCs == null) {
				lccWiseBCs = new ArrayList<Object[]>();
				logicalCCWiseBookingClasses.put(bookingClass[3].toString(), lccWiseBCs);
			}
			lccWiseBCs.add(bookingClass);
		}

		return logicalCCWiseBookingClasses;
	}

	/**
	 * Creates Segment Code List
	 * 
	 * @param segmentsBCInvsMap
	 *            the map containing the Segments
	 * @return String the Array of Booking Class
	 */
	public static String createSegmentsListHTML(int FlightId) throws ModuleException {
		StringBuffer strArrays = new StringBuffer();
		strArrays.append("var segmentCodes = new Array();");
		Collection<FlightSegement> colSegs = ModuleServiceLocator.getFlightServiceBD().getSegments(FlightId);
		Iterator<FlightSegement> segmentsIt = colSegs.iterator();
		int segmentsCount = 0;
		while (segmentsIt.hasNext()) {
			FlightSegement segment = segmentsIt.next();
			if (segment != null) {
				strArrays.append("segmentCodes[" + segmentsCount + "]='" + segment.getSegmentCode() + "';");

				++segmentsCount;
			}
		}
		return strArrays.toString();
	}

	/**
	 * Creates Charge Code Groups For Booking Class
	 * 
	 * @return String the Array of Charge Groups
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String createChargegroupsForBC() throws ModuleException {

		Collection<Map<String, String>> ccCollec = null;
		StringBuffer jsb = new StringBuffer("var arrData = new Array();");
		jsb.append("var chgrs = new Array();");

		ccCollec = SelectListGenerator.createChargegroupsForBCList();
		int tempint = 0;
		if (ccCollec != null) {
			Iterator<Map<String, String>> ite = ccCollec.iterator();
			while (ite.hasNext()) {
				Map<String, String> keyValues = ite.next();
				Set<String> keys = keyValues.keySet();
				Iterator<String> keyIterator = keys.iterator();
				while (keyIterator.hasNext()) {
					String id = keyValues.get(keyIterator.next());
					jsb.append("chgrs[" + tempint + "] = new Array('" + id + "','" + id + "');");
					tempint++;
				}
			}
		}
		jsb.append("arrData[0] = chgrs");
		return jsb.toString();
	}

	/**
	 * Creates a multi Select Box for Stations by Country
	 * 
	 * @return String the Station box by Country
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final static String createStationsByCountry() throws ModuleException {
		StringBuffer jsb = new StringBuffer();
		String strCountry[] = new String[1];
		strCountry[0] = "";
		Collection<String[]> stnCol = null;
		Iterator<String[]> stnite = null;
		String stncode = null;
		String stndesc = null;
		String strStnArr[] = new String[2];
		jsb.append(" var countries = new Array();");
		jsb.append(" var cntryData = new Array();");

		int i = 0;
		int tempint = 0;
		Collection<Map<String, String>> lstcntry = SelectListGenerator.createAllCountry(strCountry);
		Iterator<Map<String, String>> itecntry = lstcntry.iterator();
		if (lstcntry != null && !lstcntry.isEmpty()) {
			while (itecntry.hasNext()) {
				Map<String, String> keyValues = itecntry.next();
				Set<String> keys = keyValues.keySet();
				Iterator<String> keyIterator = keys.iterator();

				if (keyIterator.hasNext()) {
					String code = keyValues.get(keyIterator.next());
					String desc = keyValues.get(keyIterator.next());
					strCountry[0] = code;
					jsb.append("countries[" + i + "] = '" + desc + "'; ");
					jsb.append("var cities_" + i + " = new Array();");
					tempint = 0;
					stnCol = SelectListGenerator.createStationCountry(strCountry);

					stnite = stnCol.iterator();
					while (stnite.hasNext()) {
						strStnArr = stnite.next();
						stncode = strStnArr[0];
						stndesc = strStnArr[1];
						jsb.append("cities_" + i + "[" + tempint + "] = new Array('" + stndesc + "','" + stncode + "');");
						tempint++;
					}
					jsb.append("cntryData[" + i + "] = cities_" + i + ";");
					i++;
				}
			}
		}

		jsb.append("var cntryGroup1 = countries; var cntryGroup2 = new Array();");
		jsb.append("var cntryData2 = new Array();");
		jsb.append("cntryData2[0] = new Array();");
		jsb.append("var stls = new Listbox('lstSTC', 'lstAssignedSTC', 'spnSTC','stls');");
		jsb.append("stls.group1 = cntryGroup1; stls.group2 = cntryGroup2;");
		jsb.append("stls.list1 = cntryData; stls.list2 = cntryData2;");
		jsb.append("stls.height = '125px'; stls.width = '160px';");
		jsb.append("stls.headingLeft = '&nbsp;&nbsp;Agent Stations';");
		jsb.append("stls.headingRight = '&nbsp;&nbsp;Assigned Agent Station';");
		jsb.append("stls.drawListBox();");
		return jsb.toString();
	}

	/**
	 * Creates a multi Select Box for Active Stations
	 * 
	 * @return String the Station multy select box
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final static String createStationHtml() throws ModuleException {

		List<Map<String, String>> Stations = null;
		StringBuffer jsb = new StringBuffer("var arrData = new Array();");
		jsb.append("var methods = new Array();");

		Stations = SelectListGenerator.createStationsListWithStatus();
		int tempInt = 0;
		String stationName = null;
		String stationCode = null;
		// String stations[] = new String[2];

		Iterator<Map<String, String>> stationsItr = Stations.iterator();
		while (stationsItr.hasNext()) {

			Map<String, String> keyValues = stationsItr.next();
			Set<String> keySet = keyValues.keySet();
			Iterator<String> keySetItr = keySet.iterator();

			if (keySetItr.hasNext()) {
				stationCode = keyValues.get(keySetItr.next());
				stationName = keyValues.get(keySetItr.next());
				// if(status != null && status.equals("ACT")) {
				jsb.append("methods[" + tempInt + "] = new Array('" + stationCode + "  -  " + stationName + "','" + stationCode
						+ "'); ");
				tempInt++;
			}

		}

		jsb.append("arrData[0] = methods;");
		jsb.append(" var lssta = new Listbox('lstStations', 'lstAssignedStations', 'spnSta', 'lssta');");
		jsb.append("lssta.group1 = arrData[0];");
		jsb.append("lssta.height = '100px'; lssta.width = '170px';");
		jsb.append("lssta.headingLeft = '&nbsp;&nbsp;Stations';");
		jsb.append("lssta.headingRight = '&nbsp;&nbsp;Selected Stations';");
		// jsb.append("lssta.drawListBox();");
		return jsb.toString();
	}

	/**
	 * Creates a multi Select Box for all countries
	 * 
	 * @return String the Station multy select box
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final static String createCountryHtml() throws ModuleException {

		List<Map<String, String>> countries = null;
		StringBuffer jsb = new StringBuffer("var arrData = new Array();");
		jsb.append("var countries = new Array();");

		countries = SelectListGenerator.createCountryListWithCode();
		int tempInt = 0;
		String countryName = null;
		String countryCode = null;

		Iterator<Map<String, String>> countryItr = countries.iterator();
		while (countryItr.hasNext()) {

			Map<String, String> keyValues = countryItr.next();
			Set<String> keySet = keyValues.keySet();
			Iterator<String> keySetItr = keySet.iterator();

			if (keySetItr.hasNext()) {
				countryCode = keyValues.get(keySetItr.next());
				countryName = keyValues.get(keySetItr.next());
				jsb.append("countries[" + tempInt + "] = new Array('" + countryName + "','" + countryName + "'); ");
				tempInt++;
			}

		}
		jsb.append("arrData[0] = countries;");
		jsb.append(" var countryListBox = new Listbox('countries', 'selectedCountries', 'spnCountrList', 'countryListBox');");
		jsb.append("countryListBox.group1 = arrData[0];");
		jsb.append("countryListBox.height = '100px'; countryListBox.width = '170px';");
		jsb.append("countryListBox.headingLeft = '&nbsp;&nbsp;Countries';");
		jsb.append("countryListBox.headingRight = '&nbsp;&nbsp;Selected Countries';");
		return jsb.toString();
	}

	/**
	 * Creates a multi Select Box for Active Agents
	 * 
	 * @return String the agents multy select box
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final static String createAgentListHtml() throws ModuleException {

		List<Map<String, String>> Agent = null;
		StringBuffer jsb = new StringBuffer("var arrData = new Array();");
		jsb.append("var methods = new Array();");

		Agent = SelectListGenerator.createAgentListWithStatus();
		int tempInt = 0;
		String agentName = null;
		String agentCode = null;
		// String agents[] = new String[2];

		Iterator<Map<String, String>> agentItr = Agent.iterator();
		while (agentItr.hasNext()) {

			Map<String, String> keyValues = agentItr.next();
			Set<String> keySet = keyValues.keySet();
			Iterator<String> keySetItr = keySet.iterator();

			if (keySetItr.hasNext()) {
				agentName = keyValues.get(keySetItr.next());
				agentCode = keyValues.get(keySetItr.next());
				// if(status != null && status.equals("ACT")) {
				jsb.append("methods[" + tempInt + "] = new Array('" + agentCode + "  -  " + agentName + "','" + agentCode
						+ "'); ");
				tempInt++;
			}

		}

		jsb.append("arrData[0] = methods;");
		jsb.append(" var lsagt = new Listbox('lstAgents', 'lstAssignedAgents', 'spnAgents', 'lsagt');");
		jsb.append("lsagt.group1 = arrData[0];");
		jsb.append("lsagt.height = '100px'; lsagt.width = '170px';");
		jsb.append("lsagt.headingLeft = '&nbsp;&nbsp;Agents';");
		jsb.append("lsagt.headingRight = '&nbsp;&nbsp;Selected Agents';");
		// jsb.append("lsagt.drawListBox();");
		return jsb.toString();
	}
	
	/**
	 * Creates a multi Select Box for Active Charges with out Taxes
	 * 
	 * @return String the Charges multy select box
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final static String createChargesWithOutServiceTaxListHtml() throws ModuleException {

		List<Map<String, String>> charges = null;
		StringBuffer jsb = new StringBuffer("var arrData = new Array();");
		jsb.append("var methods = new Array();");

		charges = SelectListGenerator.createChargesWithOutServiceTax();
		int tempInt = 0;
		String chargeName = null;
		String chargeCode = null;
		// String agents[] = new String[2];

		Iterator<Map<String, String>> chargesItr = charges.iterator();
		while (chargesItr.hasNext()) {

			Map<String, String> keyValues = chargesItr.next();
			Set<String> keySet = keyValues.keySet();
			Iterator<String> keySetItr = keySet.iterator();

			if (keySetItr.hasNext()) {
				chargeCode = keyValues.get(keySetItr.next());
				chargeName = keyValues.get(keySetItr.next());
				// if(status != null && status.equals("ACT")) {
				jsb.append("methods[" + tempInt + "] = new Array('" + chargeCode + "-" + chargeName  + "','"  +chargeCode
						+ "'); ");
				tempInt++;
			}

		}

		jsb.append("arrData[0] = methods;");
		jsb.append(" var lsCharge = new Listbox('lstCharges', 'lstAssignedCharges', 'spnPopCharges', 'lsCharge');");
		jsb.append("lsCharge.group1 = arrData[0];");
		jsb.append("lsCharge.height = '100px'; lsCharge.width = '170px';");
		jsb.append("lsCharge.headingLeft = '&nbsp;&nbsp;Charges';");
		jsb.append("lsCharge.headingRight = '&nbsp;&nbsp;Selected Exclude Charges';");
		return jsb.toString();
	}
	
	/**
	 * Creates a country wise Service Tax Category Report
	 * 
	 * @return Country wise Service Tax Category Report
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final static String createCountryWiseServiceTaxCategoryListHTML() throws ModuleException {

		Map<String, String> countryWiseServiceTaxReportCategory = null;
		StringBuffer sb = new StringBuffer("var countryServiceTaxReportCategory = new Array();");
		countryWiseServiceTaxReportCategory = globalConfig.getCountryServiceTaxReportCategory();
		// String agents[] = new String[2];

		if (countryWiseServiceTaxReportCategory != null) {
			countryWiseServiceTaxReportCategory.forEach((country, description) -> {
				StringBuilder sb2 = new StringBuilder();
				sb2.append('"');
				String[] taxReportingCategory = description.split(COMMA_SEPERATOR);
				for (String valuePair : taxReportingCategory) {
					String[] valuePairMap = valuePair.split("=");
					sb2.append("<option value='" + valuePairMap[0] + "'>" + valuePairMap[1] + "</option>");
				}
				sb2.append('"');
				sb.append("countryServiceTaxReportCategory" + "['" + country + "'] = " + sb2.toString() + "; ");
			});
		}

		return sb.toString();
	}

	/**
	 * Number format
	 * 
	 * @param dblAmount
	 * @return
	 */
	public static String numberFormat(Double dblAmount) {
		String strNumber = "";
		if (dblAmount != null) {
			DecimalFormat decimalFormat = new DecimalFormat("#########0.00");
			strNumber = decimalFormat.format(dblAmount);
		}
		return strNumber;
	}
	
	// AEROMART-2923
	public final static String createStationsByCountryForComapanyPamentRptGSA(UserPrincipal user)
			throws ModuleException {

		Agent agent = ModuleServiceLocator.getTravelAgentBD().getAgent(user.getAgentCode());
		String strAgentCode = agent.getAgentCode();
		StringBuffer jsb = new StringBuffer();
		String strCountry[] = new String[1];
		strCountry[0] = "";
		Collection<String[]> stnCol = null;
		Iterator<String[]> stnite = null;
		String stncode = null;
		String stndesc = null;
		String strStnArr[] = new String[2];
		jsb.append(" var countries = new Array();");
		jsb.append(" var cntryData = new Array();");

		int i = 0;
		int tempint = 0;
		Collection<Map<String, String>> lstcntry = SelectListGenerator.createAllCountry(strCountry);
		Iterator<Map<String, String>> itecntry = lstcntry.iterator();
		if (lstcntry != null && !lstcntry.isEmpty()) {
			while (itecntry.hasNext()) {
				Map<String, String> keyValues = itecntry.next();
				Set<String> keys = keyValues.keySet();
				Iterator<String> keyIterator = keys.iterator();

				if (keyIterator.hasNext()) {
					String[] params = new String[3];

					String code = keyValues.get(keyIterator.next());
					String desc = keyValues.get(keyIterator.next());

					params[0] = strAgentCode;
					params[1] = strAgentCode;
					params[2] = code;
					strCountry[0] = code;

					tempint = 0;
					stnCol = SelectListGenerator.createStationCountryForGSA(params);

					if(!stnCol.isEmpty()){
						jsb.append("countries[" + i + "] = '" + desc + "'; ");
						jsb.append("var cities_" + i + " = new Array();");
					}
					stnite = stnCol.iterator();
					while (stnite.hasNext()) {
						strStnArr = stnite.next();
						stncode = strStnArr[0];
						stndesc = strStnArr[1];
						jsb.append(
								"cities_" + i + "[" + tempint + "] = new Array('" + stndesc + "','" + stncode + "');");
						tempint++;
					}
					if(!stnCol.isEmpty()){
						jsb.append("cntryData[" + i + "] = cities_" + i + ";");
						i++;
					}
				}
			}
		}

		jsb.append("var cntryGroup1 = countries; var cntryGroup2 = new Array();");
		jsb.append("var cntryData2 = new Array();");
		jsb.append("cntryData2[0] = new Array();");
		jsb.append("var stls = new Listbox('lstSTC', 'lstAssignedSTC', 'spnSTC','stls');");
		jsb.append("stls.group1 = cntryGroup1; stls.group2 = cntryGroup2;");
		jsb.append("stls.list1 = cntryData; stls.list2 = cntryData2;");
		jsb.append("stls.height = '125px'; stls.width = '160px';");
		jsb.append("stls.headingLeft = '&nbsp;&nbsp;Agent Stations';");
		jsb.append("stls.headingRight = '&nbsp;&nbsp;Assigned Agent Station';");
		jsb.append("stls.drawListBox();");
		return jsb.toString();
	}

	public static String getLanguageOptions() {
		Collection<Language> languages = null;
		try {
			languages = ModuleServiceLocator.getCommonServiceBD().getLanguages();
		} catch (ModuleException e) {
			log.error(e);
		}
		StringBuilder sb = new StringBuilder();
		sb.append("<option value='' ></option>");
		if (languages != null) {
			for (Language language : languages) {

				sb.append("<option value='");
				sb.append(language.getLanguageCode());
				sb.append("' >");
				sb.append(language.getLanguageName());
				sb.append("</option>");

			}
		}
		return sb.toString();
	}

}
