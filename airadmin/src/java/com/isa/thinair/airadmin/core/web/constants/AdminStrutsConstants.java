package com.isa.thinair.airadmin.core.web.constants;

public final class AdminStrutsConstants {

	public static interface AdminAction {
		public static final String SUCCESS = "success";
		public static final String ERROR = "error";
		public static final String VIEW = "view";
		public static final String OND_SUCCESS = "successOND";
		public static final String RES_SUCCESS = "successres";
		public static final String SHOW_MAIN = "showMain";
		public static final String CHANGE_PASS = "showChangePassword";
		public static final String PASS_CHANGE = "showPasswordChange";
		public static final String EDIT_TEMPLATE = "editTemplate";
		public static final String RE_PROTECT = "reprotect";
		public static final String AMEND_FLIGHT = "amendFlightInstance";
		public static final String CANCELL_SUCCESS = "cancelFlightSuccess";
		public static final String CANCELL_FLIGHT = "cancelFlightInstance";
		public static final String SAVE_SUCCESS = "Savesuccess";
		public static final String GRID = "grid";
		public static final String SUCCESS_ADVANCE = "advanceSuccess";
		public static final String SUCCESS_V2 = "success_v2";
		public static final String REPROTECTION_SEARCH = "searchRP";
		public static final String REPROTECT_FLIGTH = "reprotectFlight";
		public static final String LOAD_MSG = "loadMsg";
		public static final String INVTEMPLATE_BC = "invtemplatebc";

	}

	public static interface AdminJSP {
		public static final String ERROR_JSP = "/WEB-INF/jsp/common/ErrorRedirect.jsp";

		public static final String MAIN_JSP = "/WEB-INF/jsp/Menu/Main.jsp";

		public static final String LOGIN_JSP = "/WEB-INF/jsp/login/Login.jsp";

		public static final String TERRITORY_ADMIN_JSP = "/WEB-INF/jsp/territory/TerritoryAdmin.jsp";

		public static final String AIRCRAFT_MODEL_ADMIN_JSP = "/WEB-INF/jsp/aircraftModel/AircraftAdmin.jsp";

		public static final String AIRPORT_DST_ADMIN_JSP = "/WEB-INF/jsp/airport/AirportAdminDayLight.jsp";
		public static final String AIRPORT_CIT_ADMIN_JSP = "/WEB-INF/jsp/airport/AirportAdminCheckIn.jsp";
		public static final String AIRPORT_TERMINAL_ADMIN_JSP = "/WEB-INF/jsp/airport/AirportTerminal.jsp";
		public static final String AIRPPORT_ADMIN_JSP = "/WEB-INF/jsp/airport/AirportAdmin.jsp";
		public static final String V2_FLA_ALERT_EMAIL_JSP = "/WEB-INF/jsp/v2/fla/FlaAlertEmail.jsp";

		public static final String BC_ADMIN_JSP = "/WEB-INF/jsp/bookingcodes/ManageBookingCode.jsp";

		public static final String CHARGE_ADMIN_JSP = "/WEB-INF/jsp/managecharges/ManageCharges.jsp";

		public static final String ROUTE_ADMIN_JSP = "/WEB-INF/jsp/citypair/CityPairAdmin.jsp";

		public static final String COUNTRY_ADMIN_JSP = "/WEB-INF/jsp/country/CountryAdmin.jsp";

		public static final String CURRENCY_ADMIN_JSP = "/WEB-INF/jsp/currency/CurrencyAdmin.jsp";

		public static final String FARE_CHG_DUMMY_JSP = "/WEB-INF/jsp/fareClasses/AdminData.jsp";
		public static final String FARE_CHG_CREATE_JSP = "/WEB-INF/jsp/fareClasses/CreateFare.jsp";
		public static final String FARE_CHG_UPLOAD_JSP = "/WEB-INF/jsp/fareClasses/UploadFares.jsp";
		public static final String FARE_CHG_ADMIN_JSP = "/WEB-INF/jsp/fareClasses/ViewFares.jsp";

		public static final String FARE_RULE_LINK_JSP = "/WEB-INF/jsp/fareClasses/LinkAgents.jsp";
		public static final String FARE_RULE_AGENT_JSP = "/WEB-INF/jsp/fareClasses/AgentsFare.jsp";
		public static final String FARE_RULE_ADMIN_JSP = "/WEB-INF/jsp/fareClasses/ManageFareClasses.jsp";
		public static final String FARE_RULE_ADMIN_JSP_V2 = "/WEB-INF/jsp/v2/fareClasses/ManageFareClasses.jsp";
		public static final String FARE_RULE_GRID_DATA_V2 = "/WEB-INF/jsp/v2/fareClasses/testData.jsp";
		public static final String FARE_AGENT_FARE_JSP = "/WEB-INF/jsp/fareClasses/LinkAgentsForFare.jsp";
		public static final String FARE_OVERWRITE_JSP = "/WEB-INF/jsp/fareClasses/OverwriteFarerule.jsp";

		public static final String FLIGHT_GDS_JSP = "/WEB-INF/jsp/flight/FltGDSPublishing.jsp";
		public static final String FLIGHT_SEGMENT_JSP = "/WEB-INF/jsp/flight/ValidateFlightSegments.jsp";
		public static final String FLIGHT_TERMINAL_JSP = "/WEB-INF/jsp/flight/ValidateFlightTerminals.jsp";
		public static final String FLIGHT_ADMIN_JSP = "/WEB-INF/jsp/flight/Flight.jsp";
		public static final String FLIGHT_CANCEL_JSP = "/WEB-INF/jsp/flight/FlightCancel.jsp";
		public static final String FLIGHT_AMEND_JSP = "/WEB-INF/jsp/flight/AmendFlight.jsp";

		public static final String FLIGHT_REPROTECT_JSP = "/WEB-INF/jsp/flightschedule/FlightCancel.jsp";
		public static final String FLIGHT_SHED_SEGMENT_JSP = "/WEB-INF/jsp/flightschedule/ValidateSegments.jsp";
		public static final String FLIGHT_SHED_ADMIN_JSP = "/WEB-INF/jsp/flightschedule/FlightSchedule.jsp";
		public static final String FLIGHT_SHED_REPROTECT_JSP = "/WEB-INF/jsp/flightschedule/DisplayReProtectFlights.jsp";
		public static final String FLIGHT_SHED_REPROTECT_JSP_V2 = "/WEB-INF/jsp/v2/flightschedule/DisplayReProtectFlights.jsp";
		public static final String FLIGHT_SHED_REPROTECT_ACTION = "/private/master/showFlightsToReprotect.action";
		public static final String MANAGE_FLIGHT = "/WEB-INF/jsp/flight/ManageFlight.jsp";
		public static final String FLIGHT_SHED_GDS_JSP = "/WEB-INF/jsp/flightschedule/GDSPublishing.jsp";
		public static final String GDS_ADMIN_JSP = "/WEB-INF/jsp/gds/GDSAdmin.jsp";
		public static final String GDS_SSM_RECAP = "/WEB-INF/jsp/gds/ssmRecap.jsp";
		public static final String GDS_PUBLISH_ROUTES = "/WEB-INF/jsp/gds/PublishRoutes.jsp";
		public static final String EXTERNAL_SSM_RECAP = "/WEB-INF/jsp/externalSSMRecap/externalSSMRecap.jsp";
		public static final String HUB_ADMIN_JSP = "/WEB-INF/jsp/hubdetails/HubDetails.jsp";

		public static final String SEAT_ALLOC_ROLL_JSP = "/WEB-INF/jsp/manageseatallocation/SeatRollforward.jsp";
		public static final String SEAT_ALLOC_VIEW_NOTE_JSP = "/WEB-INF/jsp/manageseatallocation/ViewUserNote.jsp";
		public static final String SEAT_ALLOC_ADD_NOTE_JSP = "/WEB-INF/jsp/manageseatallocation/AddUserNote.jsp";
		public static final String SEAT_ALLOC_MARKT_ADMIN_JSP = "/WEB-INF/jsp/manageseatallocation/ViewMrkCarrierSummary.jsp";
		public static final String SEAT_ALLOC_SEAT_CHG_JSP = "/WEB-INF/jsp/manageseatallocation/SeatCharges.jsp";
		public static final String SEAT_ALLOC_SEAT_MAP_JSP = "/WEB-INF/jsp/manageseatallocation/SeatMap.jsp";
		public static final String SEAT_ALLOC_SEARCH_JSP = "/WEB-INF/jsp/manageseatallocation/ManageSeatInventorySearch.jsp";
		public static final String SEAT_ALLOC_VIEW_FARE_JSP = "/WEB-INF/jsp/manageseatallocation/ViewFareDetails.jsp";
		public static final String SEAT_ALLOC_ADMIN_JSP = "/WEB-INF/jsp/manageseatallocation/ManageSeatInventoryAllocation.jsp";
		public static final String SEAT_ALLOC_INV_ROLL_JSP = "/WEB-INF/jsp/manageseatallocation/ManageSeatInventoryRollforward.jsp";

		public static final String MEAL_ALLOC_MEAL_MAP_JSP = "/WEB-INF/jsp/manageseatallocation/MealMap.jsp";
		public static final String MEAL_ALLOC_MEAL_CHG_JSP = "/WEB-INF/jsp/manageseatallocation/MealCharges.jsp";
		public static final String MEAL_ALLOC_ROLL_JSP = "/WEB-INF/jsp/manageseatallocation/MealRollforward.jsp";

		public static final String BAGGAGE_ALLOC_BAGGAGE_CHG_JSP = "/WEB-INF/jsp/manageseatallocation/BaggageCharges.jsp";
		public static final String BAGGAGE_ALLOC_ROLL_JSP = "/WEB-INF/jsp/manageseatallocation/BaggageRollforward.jsp";

		public static final String MENU_JSP = "/WEB-INF/jsp/Menu/Menu.jsp";

		public static final String OPTIMIZE_SEARCH_JSP = "/WEB-INF/jsp/optimizeseatallocation/OptimizeSeatAllocation.jsp";
		public static final String OPTIMIZE_RESULT_JSP = "/WEB-INF/jsp/optimizeseatallocation/OptimizeSeatAllocationGrid.jsp";

		public static final String APP_PARAM_ADMIN_JSP = "/WEB-INF/jsp/bizsysparameters/BusinessSystemParametersAdmin.jsp";

		public static final String PASS_CHANGE_DUMMY_JSP = "/WEB-INF/jsp/passwordchange/DummyChange.jsp";
		public static final String PASS_CHANGE_JSP = "/WEB-INF/jsp/passwordchange/PasswordChange.jsp";

		public static final String SCHED_SRV_CASH_SALES_JSP = "/WEB-INF/jsp/scheduledservices/CashSalesTransfer.jsp";
		public static final String SCHED_SRV_CREDIT_SALES_JSP = "/WEB-INF/jsp/scheduledservices/CreditCardSalesTransfer.jsp";
		public static final String SCHED_SRV_PFS_DETAIL_JSP = "/WEB-INF/jsp/scheduledservices/PFSDetails.jsp";
		public static final String SCHED_SRV_PFS_PROCC_JSP = "/WEB-INF/jsp/scheduledservices/PFSProcessing.jsp";

		public static final String CONFIG_TIME_LIMITS_JSP = "/WEB-INF/jsp/flightschedule/ConfigTimeLimits.jsp";

		public static final String SCHED_SRV_INV_REPRINT_JSP = "/WEB-INF/jsp/scheduledservices/ReprintGenInv.jsp";
		public static final String SCHED_SRV_XAPNL_DETAIL_JSP = "/WEB-INF/jsp/scheduledservices/XAPNLDetails.jsp";
		public static final String SCHED_SRV_XAPNL_PROCC_JSP = "/WEB-INF/jsp/scheduledservices/XAPNLProcessing.jsp";
		public static final String SCHED_SRV_ETL_PROCC_JSP = "/WEB-INF/jsp/scheduledservices/ETLProcessing.jsp";
		public static final String SCHED_SRV_ETL_DETAIL_JSP = "/WEB-INF/jsp/scheduledservices/ETLDetails.jsp";
		public static final String SCHED_SRV_PRL_PROCC_JSP = "/WEB-INF/jsp/scheduledservices/PRLProcessing.jsp";
		public static final String SCHED_SRV_PRL_DETAIL_JSP = "/WEB-INF/jsp/scheduledservices/PRLDetails.jsp";
		public static final String RQST_QUE_JSP = "/WEB-INF/jsp/requestqueue/RequestQueueAdmin.jsp";
		
		
		public static final String SEATMAP_ADMIN_JSP = "/WEB-INF/jsp/seatmap/SeatMap.jsp";
		public static final String SEATMAP_CHG_TMPLT_JSP = "/WEB-INF/jsp/seatmap/CargesTemplate.jsp";

		public static final String ROLE_ADMIN_JSP = "/WEB-INF/jsp/security/role/RolePrivAdmin.jsp";

		public static final String SITA_ADMIN_JSP = "/WEB-INF/jsp/sitaaddress/SitaAdmin.jsp";
		
		public static final String ADMIN_FEE_AGENT_ADMIN_JSP = "/WEB-INF/jsp/adminfeeagent/AdminFeeAgentAdmin.jsp";

		public static final String SSR_ADMIN_JSP = "/WEB-INF/jsp/ssr/SsrAdmin.jsp";

		public static final String SSR_CHARGE = "/WEB-INF/jsp/ssr/SsrCharge.jsp";

		public static final String STATION_ADMIN_JSP = "/WEB-INF/jsp/station/StationAdmin.jsp";
		public static final String CITY_ADMIN_JSP = "/WEB-INF/jsp/v2/master/city/CityAdmin.jsp";

		public static final String RPTS_AGENT_COMM_JSP = "/WEB-INF/jsp/reports/AgentCommisionReport.jsp";
		public static final String RPTS_AGENT_PRODUCTIVITY_JSP = "/WEB-INF/jsp/reports/AgentsProductivityReport.jsp";
		public static final String RPTS_AIRPORT_TAX_JSP = "/WEB-INF/jsp/reports/AirportTaxSurchargeReport.jsp";
		public static final String RPTS_COMP_PAY_POS_JSP = "/WEB-INF/jsp/reports/CompPaymentPOSReport.jsp";
		public static final String RPTS_CUST_EXIST_CREDIT_JSP = "/WEB-INF/jsp/reports/customerExistingCreditReport.jsp";
		public static final String RPTS_RESERVATIONS_JSP = "/WEB-INF/jsp/reports/reservationsReport.jsp";
		public static final String RPTS_OND_RESULTS_JSP = "/WEB-INF/jsp/reports/ONDResult.jsp";
		public static final String RPTS_OND_JSP = "/WEB-INF/jsp/reports/ONDReport.jsp";
		public static final String MEALS_DETAIL_REPORT = "/WEB-INF/jsp/reports/MealsDetailReport.jsp";
		public static final String EXTERNAL_PMT_RECONCIL_REPORT = "/WEB-INF/jsp/reports/ExtPmtReconcilReport.jsp";
		public static final String RPTS_LOYALTY_POINTS_JSP = "/WEB-INF/jsp/reports/LoyaltyPointsReport.jsp";

		public static final String REPORTING_FLTSCHD_CAP_VARIENCE_JSP = "/WEB-INF/jsp/reporting/FlightScheduleCapacityVarianceReport.jsp";
		public static final String REPORTING_FLTSCHD_INBOUND_OUTBOUND_JSP = "/WEB-INF/jsp/reporting/FlightScheduleInboundOutboundReport.jsp";
		public static final String REPORTING_ONHLD_PAX_JSP = "/WEB-INF/jsp/reports/onHoldReport.jsp";
		public static final String REPORTING_AGENT_CHARGE_ADJUSTMENTS_JSP = "/WEB-INF/jsp/reports/AgentChargeAdjustmentsReport.jsp";
		public static final String REPORTING_AGENT_GSA_DETAIL_JSP = "/WEB-INF/jsp/reporting/GSAAgentDetailsReport.jsp";
		public static final String REPORTING_AGENT_SALES_JSP = "/WEB-INF/jsp/reporting/AgentSalesReport.jsp";
		public static final String REPORTING_AGENT_SALES_STATUS_JSP = "/WEB-INF/jsp/reporting/AgentSalesStatusReport.jsp";
		public static final String REPORTING_AGENT_STATEMENT_JSP = "/WEB-INF/jsp/reporting/AgentStatementReport.jsp";
		public static final String REPORTING_AGENT_TRANSACTION_JSP = "/WEB-INF/jsp/reporting/AgentTransactionDetailsReport.jsp";
		public static final String REPORTING_USER_PRIVILEGES_JSP = "/WEB-INF/jsp/reporting/AgentUserPrivilegesReport.jsp";
		public static final String REPORTING_AUDIT_JSP = "/WEB-INF/jsp/reporting/AdministrationAuditReport.jsp";
		public static final String REPORTING_RESERVE_AUDIT_JSP = "/WEB-INF/jsp/reporting/ReservationAuditReport.jsp";
		public static final String REPORTING_AVS_SEAT_JSP = "/WEB-INF/jsp/reporting/AVSSeatReport.jsp";
		public static final String REPORTING_CNX_RESRV_JSP = "/WEB-INF/jsp/reporting/CancelledReserReport.jsp";
		public static final String REPORTING_COMP_PAYMENT_JSP = "/WEB-INF/jsp/reporting/CompanyPaymentReport.jsp";
		public static final String REPORTING_CUST_TRAVEL_HISTORY_JSP = "/WEB-INF/jsp/reporting/CustomerTravelHistory.jsp";
		public static final String REPORTING_ENPLANMENT_JSP = "/WEB-INF/jsp/reporting/EnplanementReport.jsp";
		public static final String REPORTING_INTERLINE_REVENUE_JSP = "/WEB-INF/jsp/reporting/InterlineRevenueReport.jsp";
		public static final String REPORTING_PASSENGER_CREDIT_JSP = "/WEB-INF/jsp/reporting/PassengerCreditReport.jsp";
		public static final String REPORTING_GSTR1_JSP = "/WEB-INF/jsp/reporting/GSTR1Report.jsp";
		public static final String REPORTING_GSTR3_JSP = "/WEB-INF/jsp/reporting/GSTR3Report.jsp";
		public static final String REPORTING_GST_ADDITIONAL_REPORT_JSP = "/WEB-INF/jsp/reporting/GSTAdditionalReport.jsp";
		public static final String REPORTING_FLIGHT_DETAIL_JSP = "/WEB-INF/jsp/reporting/FlightDetailsReport.jsp";
		public static final String REPORTING_INBOUND_OUTBOUND_CONN_JSP = "/WEB-INF/jsp/reporting/InboundOutBoundConnectionsReport.jsp";
		public static final String REPORTING_INSURENCE_JSP = "/WEB-INF/jsp/reporting/InsurenceReport.jsp";
		public static final String REPORTING_INTERLNE_SALES_JSP = "/WEB-INF/jsp/reporting/InterlineSalesReport.jsp";
		public static final String REPORTING_MODE_OF_PAYMENT_JSP = "/WEB-INF/jsp/reporting/ModeOfPayments.jsp";
		public static final String REPORTING_INV_SUMMARY_JSP = "/WEB-INF/jsp/reporting/InvoiceSummaryReport.jsp";
		public static final String REPORTING_MONITOR_PERFOMANCE_SALES_STAFF_JSP = "/WEB-INF/jsp/reporting/MonitorPerformanceOfSalesStaff.jsp";
		public static final String REPORTING_OUTSTANDING_BALANCE_JSP = "/WEB-INF/jsp/reporting/OutstandingBalanceReport.jsp";
		public static final String REPORTING_PAX_CONTACT_DETAIL_JSP = "/WEB-INF/jsp/reporting/PaxContactDetails.jsp";
		public static final String REPORTING_REFUND_JSP = "/WEB-INF/jsp/reporting/RefundReport.jsp";
		public static final String REPORTING_SECTOR_CONTRIB_JSP = "/WEB-INF/jsp/reporting/SectorContributionByAgent.jsp";
		public static final String REPORTING_TOP_AGENT_REPORT = "/WEB-INF/jsp/reporting/TopAgentsReport.jsp";
		public static final String REPORTING_SCHEDULE_DETAIL_JSP = "/WEB-INF/jsp/reporting/SeheduleDetailsReport.jsp";
		public static final String REPORTING_TOP_SECTOR_JSP = "/WEB-INF/jsp/reporting/TopSectorReport.jsp";
		public static final String REPORTING_WEB_PROFILE_JSP = "/WEB-INF/jsp/reporting/WebProfileReport.jsp";
		public static final String REPORTING_TRANSACTION_JSP = "/WEB-INF/jsp/reporting/TransactionReport.jsp";
		public static final String REPORTING_VIEW_SEAT_INV_COLLECTION_JSP = "/WEB-INF/jsp/reporting/ViewSeatInventoryCollections.jsp";
		public static final String REPORTING_REVENUE_REPORT_JSP = "/WEB-INF/jsp/reporting/ROPRevenueReport.jsp";
		public static final String REPORTING_REVENUE_FORWARD_SUCCESS_JSP = "/WEB-INF/jsp/reporting/RevenueForwardResult.jsp";
		public static final String REPORTING_REVENUE_FORWARD_JSP = "/WEB-INF/jsp/reporting/ROPRevenueForwardReport.jsp";
		public static final String REPORTING_BOOKING_CLASS_JSP = "/WEB-INF/jsp/reporting/BookingClassReport.jsp";
		public static final String REPORTING_INVOICE_SETTLE_HISTORY_JSP = "/WEB-INF/jsp/reporting/InvoiceSettlementHistoryReport.jsp";
		public static final String REPORTING_NAME_CHANGE_DETAILS_JSP = "/WEB-INF/jsp/reporting/NameChangeDetailsReport.jsp";
		public static final String REPORTING_BOOKED_SSR_DETAILS_REPORT_JSP = "/WEB-INF/jsp/reporting/BookedSSRDetailsReport.jsp";
		public static final String REPORTING_AUTOMATIC_CHECKING_REPORT_JSP = "/WEB-INF/jsp/reporting/AutomaticCheckinReport.jsp";
		public static final String REPORTING_AIRPORT_TRANSFERS_REPORT_JSP = "/WEB-INF/jsp/reporting/AirportTransfersReport.jsp";
		public static final String REPORTING_VOID_RESERVATION_DETAIL_REPORT_JSP = "/WEB-INF/jsp/reporting/VoidReservationDetailsReport.jsp";
		public static final String REPORTING_BOOKING_COUNT_DETAIL_REPORT_JSP = "/WEB-INF/jsp/reporting/BookingCountDetailsReport.jsp";
		public static final String REPORTING_TICKET_WISE_FORWARD_SALES_JSP = "/WEB-INF/jsp/reporting/TicketWiseForwardSalesReport.jsp";
		public static final String REPORTING_AGENT_WISE_FORWARD_SALES_JSP = "/WEB-INF/jsp/reporting/AgentWiseForwardSalesReport.jsp";
		public static final String REPORTING_INTERNATIONAL_FLIGHT_ARRIVAL_DEPARTURE_JSP = "/WEB-INF/jsp/reporting/IntlFlightArrivalDepartureReport.jsp";
		public static final String FLIGHT_LOYALTY_MEMBER_JSP = "/WEB-INF/jsp/v2/promotion/lms/FlightLoyaltyMember.jsp";
		public static final String POINT_REDEMPTION_DETAILED_REPORT ="/WEB-INF/jsp/v2/promotion/lms/PointRedemptionReport.jsp";
		public static final String REPORTING_BLACKLIST_PAX_JSP = "/WEB-INF/jsp/reports/BlacklistPAXReport.jsp";
		public static final String PROMO_CODE_JSP = "/WEB-INF/jsp/v2/promotion/PromoCodeManagement.jsp";
		
		public static final String REPORTING_PAX_STATUS_FORWARD_JSP = "/WEB-INF/jsp/reporting/PAXStatusReport.jsp";

		public static final String REPORTING_HALA_COMMISSION_JSP = "/WEB-INF/jsp/reporting/HalaCommissionReport.jsp";
		public static final String REPORTING_ANCILLARY_REVENUE_JSP = "/WEB-INF/jsp/reporting/AncillaryRevenueReport.jsp";
		public static final String REPORTING_ADVANANCED_ANCILLARY_REVENUE_JSP = "/WEB-INF/jsp/reporting/AdvancedAncillaryRevenueReport.jsp";
		public static final String REPORTING_AIRPORT_TAX_JSP = "/WEB-INF/jsp/reporting/AirportTaxReport.jsp";
		public static final String REPORTING_BOOKED_PAX_SEGMENT = "/WEB-INF/jsp/reporting/BookedPAXSegmentReport.jsp";
		public static final String REPORTING_ORIGIN_COUNTRY_OF_CALL = "/WEB-INF/jsp/reporting/OriginCountryOfCallReport.jsp";
		public static final String REPORTING_ADJUSTMENT_AUDIT_JSP = "/WEB-INF/jsp/reporting/AdjustmentAuditReport.jsp";

		public static final String REPORTING_PAX_SEGMENT_CHART = "/WEB-INF/jsp/reporting/chartReports/segmentPaxChartReport.jsp";
		public static final String REPORTING_CURRENCY_CONVERSION_DETAILS_JSP = "/WEB-INF/jsp/reporting/CurrencyConversionDetailsReport.jsp";

		public static final String REPORTING_AIRPORT_LIST_JSP = "/WEB-INF/jsp/reporting/AirportListReport.jsp";
		public static final String REPORTING_ROUTE_DETAILS_JSP = "/WEB-INF/jsp/reporting/RouteDetailsReport.jsp";
		public static final String REPORTING_IBE_EXIT_DETAILS_REPORT_JSP = "/WEB-INF/jsp/reporting/IbeExitDetailsReport.jsp";

		public static final String FLITGT_SCED_COPY = "/WEB-INF/jsp/flightschedule/CopySchedule.jsp";
		public static final String FLITGT_SCED_SPILT = "/WEB-INF/jsp/flightschedule/SplitSchedule.jsp";
		public static final String FLITGT_SCED_REPROTECT = "/WEB-INF/jsp/flightschedule/ReProtect.jsp";
		public static final String FLITGT_SCED_REPROTECT_ROLLFWD = "/WEB-INF/jsp/flightschedule/ReprotectRollForward.jsp";
		public static final String FLITGT_CANCEL = "/WEB-INF/jsp/flightschedule/FlightCancel.jsp";
		public static final String FLITGT_COPY = "/WEB-INF/jsp/flight/CopyFlight.jsp";
		public static final String FLITGT_AMEND = "/WEB-INF/jsp/flight/AmendFlight.jsp";
		public static final String REVENUE_RPT_RESULT = "/WEB-INF/jsp/reporting/RevenueResult.jsp";
		public static final String INTERLINE_REVENUE_RPT_RESULT = "/WEB-INF/jsp/reporting/InterlineRevenueResult.jsp";
		public static final String SELECT_REPROTECT_PNR_JSP = "/WEB-INF/jsp/flightschedule/SelectReProtectPNR.jsp";
		public static final String SELECT_REPROTECT_PNR_JSP_V2 = "/WEB-INF/jsp/v2/flightschedule/SelectReProtectPNR.jsp";

		public static final String REPORTING_FARE_RULE_DETAILS_JSP = "/WEB-INF/jsp/reporting/FareRuleDetailsReport.jsp";

		public static final String REPORTING_LCC_DRY_COLLECTION = "/WEB-INF/jsp/reporting/LCCAndDryCollectionReport.jsp";

		public static final String REPORTING_YEILD_TREND_ANALYSIS_JSP = "/WEB-INF/jsp/reporting/YeildTrendAnalysisReport.jsp";

		public static final String REPORTING_SHOW_YEILD_TREND_ANALYSIS_JSP = "/WEB-INF/jsp/reporting/ShowYeildTrendAnalysisReport.jsp";

		public static final String REPORTING_FARE_DETAILS_JSP = "/WEB-INF/jsp/reporting/FareDetailsReport.jsp";
		public static final String REPORTING_APP_PARAMS_LIST_JSP = "/WEB-INF/jsp/reporting/AppParameterListReport.jsp";
		public static final String REPORTING_FLIGHT_CONNECTIVITY_JSP = "/WEB-INF/jsp/reporting/FlightConnectivityReport.jsp";
		public static final String REPORTING_AGENT_SALES_PERFORMANCE_JSP = "/WEB-INF/jsp/reporting/AgentSalesPerformanceReport.jsp";

		public static final String REPORTING_PROMOTIONS_JSP = "/WEB-INF/jsp/reporting/PromotionsReport.jsp";
		public static final String REPORTING_OVER_BOOKING_JSP = "/WEB-INF/jsp/reporting/FlightOverBookingReport.jsp";
		public static final String REPORTING_SEAT_MAP_CHANGES_JSP = "/WEB-INF/jsp/reporting/FlightSeatMapChangesReport.jsp";
		public static final String MANAGE_TERMS_TEMPLATES_JSP = "/WEB-INF/jsp/v2/tools/TermsTemplate.jsp";
		public static final String EDIT_TERMS_TEMPLATES_JSP = "/WEB-INF/jsp/v2/tools/EditTermsTemplate.jsp";
		public static final String REPORTING_NAME_CHANGE_CHARGE_JSP = "/WEB-INF/jsp/reporting/NameChangeChargeReport.jsp";
		public static final String REPORTING_NIL_TRANSACTIONS_AGENTS_LIST = "/WEB-INF/jsp/reporting/NILTransactionsAgentsListReport.jsp";
		public static final String LOAD_MSG = "/WEB-INF/jsp/Menu/LoadMsg.jsp";
		public static final String PAX_DETAILS = "/WEB-INF/jsp/v2/flightschedule/ShowPaxDetails.jsp";

		public static final String INVENTORY_TEMPLATES_JSP = "/WEB-INF/jsp/invtemps/MngInvTemps.jsp";
		public static final String TEMPLATES_BC_ALLOC_JSP = "/WEB-INF/jsp/invtemps/MngTempBCAlloctn.jsp";
		
		public static final String MANAGE_VOUCHER_TERMS_TEMPLATES_JSP = "/WEB-INF/jsp/v2/tools/voucher/VoucherTermsTemplate.jsp";
		public static final String EDIT_VOUCHER_TERMS_TEMPLATES_JSP = "/WEB-INF/jsp/v2/tools/voucher/EditVoucherTermsTemplate.jsp";
		public static final String REPORTING_TRANSACTIONS_LMS_JSP = "/WEB-INF/jsp/reporting/CCTransactionLMSPointsReport.jsp";
		public static final String ROLLFORWARD_CONFIRMATION_PAGE = "/WEB-INF/jsp/v2/rollforward/advanceConfirmation.jsp";
	}

	public static interface AdminNameSpace {
		public static final String PRIVATE_NAME_SPACE = "/private/master";
		public static final String PUBLIC_NAME_SPACE = "/public/master";
	}

}
