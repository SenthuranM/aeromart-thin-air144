package com.isa.thinair.airadmin.core.web.v2.action.master;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.api.OnholdTimeConfigsResultsTO;
import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airreservation.api.dto.OnHoldReleaseTimeDTO;
import com.isa.thinair.airreservation.api.model.OnholdReleaseTime;
import com.isa.thinair.commons.api.dto.ConfigOnholdTimeLimitsSearchDTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;
import com.isa.thinair.webplatform.core.util.DataUtil;
import com.isa.thinair.webplatform.core.util.RowDecorator;

/**
 * 
 * @author asiri
 * 
 */
@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ShowConfigOnholdTimeLimitsAction extends BaseRequestResponseAwareAction {

	private static final Log log = LogFactory.getLog(ShowConfigOnholdTimeLimitsAction.class);

	private static final int PAGE_LENGTH = 10;

	private static final String MOD_IBE = "IBE";

	private static final String MOD_XBE = "XBE";

	private static final String MOD_API = "API";

	private static final String ANY = "ANY";

	private static final String OPT_DEP_DATE = "DEP_DATE";

	private static final String OPT_BKG_DATE = "BKG_DATE";

	private static final String ondSeparater = "/";

	private int page;

	private int total;

	private int records;

	private String hdnMode;

	private int hdnId;

	private long version;

	private ConfigOnholdTimeLimitsSearchDTO timeLimitSearch;

	private OnholdTimeConfigsResultsTO onholdTimeConfigsResultsTO;

	private Collection<Map<String, Object>> colOnholdTimeConfigs;

	private String msgType;

	private String messageTxt;

	private int configID;

	private String selFromOpt;

	private String selToOpt;

	private String selBookingClass;

	private String selFltType;

	private String selCabinClass;

	private String selAgentCode;

	private String selModuleCode;

	private int releaseTime;

	private int startCutoverTime;

	private int endCutoverTime;

	private String selRelTimeRespectTo;

	private int selRank;

	private static AiradminConfig airadminConfig = new AiradminConfig();

	public String execute() {
		try {

			if (timeLimitSearch == null) {
				timeLimitSearch = new ConfigOnholdTimeLimitsSearchDTO();
			}

			int startIndex = 0;
			if (page > 1) {
				startIndex = (page - 1) * PAGE_LENGTH;
			}

			Page pagedReocrds = ModuleServiceLocator.getReservationBD().getOnholdReleaseTimes(timeLimitSearch, startIndex,
					PAGE_LENGTH);

			this.colOnholdTimeConfigs = (Collection<Map<String, Object>>) DataUtil.createGridDataDecorateOnly(
					pagedReocrds.getStartPosition(), pagedReocrds.getPageData(), decorateRow());

			this.page = getCurrentPageNo(pagedReocrds.getStartPosition());
			this.total = getTotalNoOfPages(pagedReocrds.getTotalNoOfRecords());
			this.records = pagedReocrds.getTotalNoOfRecords();

			if (this.hdnMode != null && ((hdnMode.equals(WebConstants.ACTION_ADD)) || (hdnMode.equals(WebConstants.ACTION_EDIT)))) {

				OnholdReleaseTime onholdRelTime = new OnholdReleaseTime();
				onholdRelTime.setRelTimeId(hdnId);
				onholdRelTime.setAgentCode(getSelAgentCode());
				onholdRelTime.setBookingCode(getSelBookingClass());
				onholdRelTime.setCabinClassCode(getSelCabinClass());

				if ("DOM".equals(getSelFltType())) {
					onholdRelTime.setIsDomestic('Y');
				} else if ("INT".equals(getSelFltType())) {
					onholdRelTime.setIsDomestic('N');
				}

				if ("XBE".equals(getSelModuleCode())) {
					onholdRelTime.setModuleCode(MOD_XBE);
				} else if ("IBE".equals(getSelModuleCode())) {
					onholdRelTime.setModuleCode(MOD_IBE);
				} else if ("API".equals(getSelModuleCode())) {
					onholdRelTime.setModuleCode(MOD_API);
				} else if (ANY.equals(getSelModuleCode())) {
					onholdRelTime.setModuleCode(ANY);
				}

				if (ANY.equals(getSelFromOpt()) || ANY.equals(getSelToOpt())) {
					onholdRelTime.setOndCode(ANY);
				} else {
					onholdRelTime.setOndCode(getSelFromOpt() + ondSeparater + getSelToOpt());
				}

				if (OPT_DEP_DATE.equals(selRelTimeRespectTo)) {
					onholdRelTime.setRelTimeWrt(OPT_DEP_DATE);
				} else if (OPT_BKG_DATE.equals(selRelTimeRespectTo)) {
					onholdRelTime.setRelTimeWrt(OPT_BKG_DATE);
				}

				onholdRelTime.setRanking(getSelRank());
				onholdRelTime.setStartCutover(getStartCutoverTime());
				onholdRelTime.setEndCutover(getEndCutoverTime());
				onholdRelTime.setReleaseTime(getReleaseTime());
				onholdRelTime.setVersion(getVersion());

				Map<String, String[]> contentMap = new HashMap<String, String[]>();

				if (getVersion() > 0) {
					OnholdReleaseTime originalConfig = ModuleServiceLocator.getReservationBD().getOnholdReleaseTime(
							onholdRelTime.getRelTimeId());
					createUpdatedDataMapForAudit(contentMap, originalConfig, onholdRelTime);
				}
				ModuleServiceLocator.getReservationBD().saveOrUpdateOnholdReleaseTime(onholdRelTime, contentMap);
				this.messageTxt = airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS);
				this.msgType = WebConstants.MSG_SUCCESS;
			}

		} catch (ModuleException me) {
			this.msgType = WebConstants.MSG_ERROR;
			this.messageTxt = me.getMessage();
			if (log.isErrorEnabled()) {
				log.error(me.getMessage());
			}
		} catch (Exception e) {
			this.msgType = WebConstants.MSG_ERROR;
			this.messageTxt = e.getMessage();
			if (log.isErrorEnabled()) {
				log.error(e.getMessage());
			}
		}
		return S2Constants.Result.SUCCESS;
	}

	public String deleteOnholdTimeConfig() {
		try {
			ModuleServiceLocator.getReservationBD().deleteOnholdReleaseTimeConfig(hdnId);
			this.messageTxt = airadminConfig.getMessage(WebConstants.KEY_DELETE_SUCCESS);
			this.msgType = WebConstants.MSG_SUCCESS;
		} catch (ModuleException e) {
			this.messageTxt = airadminConfig.getMessage(WebConstants.KEY_DELETE_FAIL);
		}
		return S2Constants.Result.SUCCESS;
	}

	public int getCurrentPageNo(int startPosition) {
		int currentPageNo = 1;
		if (PAGE_LENGTH != 0) {
			currentPageNo = startPosition / PAGE_LENGTH + 1;
		}
		return currentPageNo;
	}

	public int getTotalNoOfPages(int totoalNoOfRecords) {
		int totalNoOfPages = 1;
		if (PAGE_LENGTH != 0) {
			totalNoOfPages = (int) totoalNoOfRecords / PAGE_LENGTH;
			if (totoalNoOfRecords % PAGE_LENGTH > 0)
				totalNoOfPages++;
		}
		return totalNoOfPages;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public Collection<Map<String, Object>> getColOnholdTimeConfigs() {
		return colOnholdTimeConfigs;
	}

	public void setColOnholdTimeConfigs(Collection<Map<String, Object>> colOnholdTimeConfigs) {
		this.colOnholdTimeConfigs = colOnholdTimeConfigs;
	}

	public ConfigOnholdTimeLimitsSearchDTO getTimeLimitSearch() {
		return timeLimitSearch;
	}

	public void setTimeLimitSearch(ConfigOnholdTimeLimitsSearchDTO timeLimitSearch) {
		this.timeLimitSearch = timeLimitSearch;
	}

	public OnholdTimeConfigsResultsTO getOnholdTimeConfigsResultsTO() {
		return onholdTimeConfigsResultsTO;
	}

	public void setOnholdTimeConfigsResultsTO(OnholdTimeConfigsResultsTO onholdTimeConfigsResultsTO) {
		this.onholdTimeConfigsResultsTO = onholdTimeConfigsResultsTO;
	}

	private RowDecorator<OnHoldReleaseTimeDTO> decorateRow() {
		RowDecorator<OnHoldReleaseTimeDTO> decorator = new RowDecorator<OnHoldReleaseTimeDTO>() {
			@Override
			public void decorateRow(HashMap<String, Object> row, OnHoldReleaseTimeDTO record) {

				try {
					row.put("onholdTimeConfigsResultsTO.releaseTimeId", record.getReleaseTimeId());
					row.put("onholdTimeConfigsResultsTO.agentCode", record.getAgentCode());
					row.put("onholdTimeConfigsResultsTO.bookingClass", record.getBookingClass());
					row.put("onholdTimeConfigsResultsTO.cabinClass", record.getCabinClass());
					row.put("onholdTimeConfigsResultsTO.moduleCode", record.getModuleCode());
					row.put("onholdTimeConfigsResultsTO.ondCode", record.getOndCode());
					row.put("onholdTimeConfigsResultsTO.releaseTime", record.getReleaseTime());
					row.put("onholdTimeConfigsResultsTO.startCutoverTime", record.getStartCutoverTime());
					row.put("onholdTimeConfigsResultsTO.endCutoverTime", record.getEndCutoverTime());
					row.put("onholdTimeConfigsResultsTO.rank", record.getRank());
					row.put("onholdTimeConfigsResultsTO.version", record.getVersion());
					row.put("onholdTimeConfigsResultsTO.relTimeWrt", record.getReleaseTimeWithRelativeTo());

					if (record.isDomestic()) {
						row.put("onholdTimeConfigsResultsTO.fltType", "Domestic");
					} else {
						row.put("onholdTimeConfigsResultsTO.fltType", "International");
					}

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		};
		return decorator;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public String getHdnMode() {
		return hdnMode;
	}

	public void setHdnMode(String hdnMode) {
		this.hdnMode = hdnMode;
	}

	public int getConfigID() {
		return configID;
	}

	public void setConfigID(int configID) {
		this.configID = configID;
	}

	public String getSelFromOpt() {
		return selFromOpt;
	}

	public void setSelFromOpt(String selFromOpt) {
		this.selFromOpt = selFromOpt;
	}

	public String getSelToOpt() {
		return selToOpt;
	}

	public void setSelToOpt(String selToOpt) {
		this.selToOpt = selToOpt;
	}

	public String getSelBookingClass() {
		return selBookingClass;
	}

	public void setSelBookingClass(String selBookingClass) {
		this.selBookingClass = selBookingClass;
	}

	public String getSelFltType() {
		return selFltType;
	}

	public void setSelFltType(String selFltType) {
		this.selFltType = selFltType;
	}

	public String getSelCabinClass() {
		return selCabinClass;
	}

	public void setSelCabinClass(String selCabinClass) {
		this.selCabinClass = selCabinClass;
	}

	public String getSelAgentCode() {
		return selAgentCode;
	}

	public void setSelAgentCode(String selAgentCode) {
		this.selAgentCode = selAgentCode;
	}

	public String getSelModuleCode() {
		return selModuleCode;
	}

	public void setSelModuleCode(String selModuleCode) {
		this.selModuleCode = selModuleCode;
	}

	public int getReleaseTime() {
		return releaseTime;
	}

	public void setReleaseTime(int releaseTime) {
		this.releaseTime = releaseTime;
	}

	public int getStartCutoverTime() {
		return startCutoverTime;
	}

	public void setStartCutoverTime(int startCutoverTime) {
		this.startCutoverTime = startCutoverTime;
	}

	public int getEndCutoverTime() {
		return endCutoverTime;
	}

	public void setEndCutoverTime(int endCutoverTime) {
		this.endCutoverTime = endCutoverTime;
	}

	public int getSelRank() {
		return selRank;
	}

	public void setSelRank(int selRank) {
		this.selRank = selRank;
	}

	public int getHdnId() {
		return hdnId;
	}

	public void setHdnId(int hdnId) {
		this.hdnId = hdnId;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public String getSelRelTimeRespectTo() {
		return selRelTimeRespectTo;
	}

	public void setSelRelTimeRespectTo(String selRelTimeRespectTo) {
		this.selRelTimeRespectTo = selRelTimeRespectTo;
	}

	private void createUpdatedDataMapForAudit(Map<String, String[]> contentMap, OnholdReleaseTime originalConfig,
			OnholdReleaseTime onholdRelTime) {
		List<String> changedValList = new ArrayList<String>();
		if (!(onholdRelTime.getAgentCode().equals(originalConfig.getAgentCode()))) {
			changedValList.add("AgentCode " + originalConfig.getAgentCode() + " modified to " + onholdRelTime.getAgentCode());
		}
		if (!(onholdRelTime.getBookingCode().equals(originalConfig.getBookingCode()))) {
			changedValList.add("BookingCode " + originalConfig.getBookingCode() + " modified to "
					+ onholdRelTime.getBookingCode());
		}
		if (!(onholdRelTime.getCabinClassCode().equals(originalConfig.getCabinClassCode()))) {
			changedValList.add("CabinClassCode " + originalConfig.getCabinClassCode() + " modified to "
					+ onholdRelTime.getCabinClassCode());
		}
		if (onholdRelTime.getIsDomestic() != originalConfig.getIsDomestic()) {
			changedValList.add((originalConfig.getIsDomestic() == 'Y' ? "Domestic" : "International") + " modified to "
					+ (onholdRelTime.getIsDomestic() == 'Y' ? "Domestic" : "International"));
		}
		if (!(onholdRelTime.getModuleCode().equals(originalConfig.getModuleCode()))) {
			changedValList.add("ModuleCode " + originalConfig.getModuleCode() + " modified to " + onholdRelTime.getModuleCode());
		}
		if (!(onholdRelTime.getOndCode().equals(originalConfig.getOndCode()))) {
			changedValList.add("OndCode " + originalConfig.getOndCode() + " modified to " + onholdRelTime.getOndCode());
		}
		if (!(onholdRelTime.getRanking().equals(originalConfig.getRanking()))) {
			changedValList.add("Rank " + Integer.toString(originalConfig.getRanking()) + " modified to "
					+ Integer.toString(onholdRelTime.getRanking()));
		}
		if (!(onholdRelTime.getReleaseTime().equals(originalConfig.getReleaseTime()))) {
			changedValList.add("ReleaseTime " + Integer.toString(originalConfig.getReleaseTime()) + " modified to "
					+ Integer.toString(onholdRelTime.getReleaseTime()));
		}
		if (!(onholdRelTime.getRelTimeWrt().equals(originalConfig.getRelTimeWrt()))) {
			changedValList.add("ReleaseTimeWRT "
					+ (originalConfig.getRelTimeWrt().equals("DEP_DATE") ? "Departure Date" : "Booking Date") + " modified to "
					+ (onholdRelTime.getRelTimeWrt().equals("DEP_DATE") ? "Departure Date" : "Booking Date"));
		}
		if (!(onholdRelTime.getStartCutover().equals(originalConfig.getStartCutover()))) {
			changedValList.add("StartCutover " + Integer.toString(originalConfig.getStartCutover()) + " modified to "
					+ Integer.toString(onholdRelTime.getStartCutover()));
		}
		if (!(onholdRelTime.getEndCutover().equals(originalConfig.getEndCutover()))) {
			changedValList.add("EndCutover " + Integer.toString(originalConfig.getEndCutover()) + " modified to "
					+ Integer.toString(onholdRelTime.getEndCutover()));
		}
		
		if(changedValList.size() > 0){
			
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
			Date today = new Date();
			changedValList.add("Modified Date " + formatter.format(today));
			changedValList.add("Modified By " + originalConfig.getUser());
		}
		contentMap.put("updated_config", changedValList.toArray(new String[changedValList.size()]));
	}

}
