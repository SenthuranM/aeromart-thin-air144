package com.isa.thinair.airadmin.core.web.v2.handler.master;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airpricing.api.criteria.MealTemplateSearchCriteria;
import com.isa.thinair.airpricing.api.model.DefaultAnciTemplate;
import com.isa.thinair.airpricing.api.dto.MealTemplateDTO;
import com.isa.thinair.airpricing.api.model.MealTemplate;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

public class MealTemplateRH {

	private static Log log = LogFactory.getLog(MealTemplateRH.class);

	public static Page<MealTemplate> searchMealTemplate(int pageNo, MealTemplateSearchCriteria criteria) {
		// call back end

		Page<MealTemplate> page = null;
		try {
			page = ModuleServiceLocator.getChargeBD().getMealTemplates(criteria, (pageNo - 1) * 20, 20);
		} catch (ModuleException e) {
			log.error("Load meal templates failed", e);
		}
		return page;
	}

	public static boolean saveMealTemplate(MealTemplateDTO mealTemplateDTO) throws ModuleException {
		ModuleServiceLocator.getChargeBD().saveMealTemplate(mealTemplateDTO, null);
		return true;
	}

	public static boolean deleteMealTemplate(MealTemplateDTO mealTemplateDTO) throws ModuleException {
		ModuleServiceLocator.getChargeBD().deleteMealTemplate(mealTemplateDTO);
		return true;
	}
	
	public static void checkTemplateAttachedToRouteWiseDefAnciTempl(int templateId) throws ModuleException{
		ModuleServiceLocator.getCommonAncillaryServiceBD().checkTemplateAttachedToRouteWiseDefAnciTempl(DefaultAnciTemplate.ANCI_TEMPLATES.MEAL, templateId);	
	}

}
