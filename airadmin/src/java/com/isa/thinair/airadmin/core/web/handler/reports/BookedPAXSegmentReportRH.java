/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airadmin.core.web.handler.reports;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.util.AirTravelAgentConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.commons.core.constants.AccelAeroClients;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author Jagath
 * 
 */
public class BookedPAXSegmentReportRH extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(BookedPAXSegmentReportRH.class);

	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	/**
	 * @param request
	 * @return
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {

		String strHdnMode = request.getParameter("hdnMode");
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;
		setAttribInRequest(request, "displayAgencyMode", "-1");
		setAttribInRequest(request, "currentAgentCode", "");

		try {
			setDisplayData(request);
			log.debug("BookedPAXSegmentReportRH SETDISPLAYDATA() SUCCESS");
		} catch (Exception e) {
			log.error("BookedPAXSegmentReportRH SETDISPLAYDATA() FAILED " + e.getMessage());
		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				log.error("BookedPAXSegmentReportRH setReportView Success");
				return null;
			} else {
				log.error("BookedPAXSegmentReportRH setReportView not selected");
			}

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("ModeOfPaymentsRequestHandler setReportView Failed " + e.getMessageString());
		}

		return forward;
	}

	protected static void setGSAMultiSelectList(HttpServletRequest request) throws ModuleException {

		boolean blnWithTAs = false;
		boolean blnWithCOs = false;
		String strAgentType = "";
		String strList = "";

		if (request.getParameter("selAgencies") != null) {
			strAgentType = request.getParameter("selAgencies");
		}
		if (request.getParameter("chkCOs") != null) {
			blnWithCOs = (request.getParameter("chkCOs").equals("on") ? true : false);
		}
		if (request.getParameter("chkTAs") != null) {
			blnWithTAs = (request.getParameter("chkTAs").equals("on") ? true : false);
		} else if (getAttribInRequest(request, "displayAgencyMode").equals("2")) {
			blnWithTAs = true;
		}

		if (getAttribInRequest(request, "displayAgencyMode").equals("-1")) {
			// No privilege to access the reports
			throw new ModuleRuntimeException(WebConstants.ERR_UNAUTHORIZED_ACCESS);
		} else if (getAttribInRequest(request, "displayAgencyMode").equals("0")) {
			// No Agent Population Controls or Agents list box are displayed
			String userId = request.getUserPrincipal().getName();
			if (userId != null && !userId.equals("")) {
				Agent currentAgent = ModuleServiceLocator.getTravelAgentBD().getAgent(
						ModuleServiceLocator.getSecurityBD().getUserBasicDetails(userId).getAgentCode());
				setAttribInRequest(request, "currentAgentCode", currentAgent.getAgentCode());
			} else {
				// No privilege to access the reports
				throw new ModuleRuntimeException(WebConstants.ERR_UNAUTHORIZED_ACCESS);
			}
		} else if (getAttribInRequest(request, "displayAgencyMode").equals("1")) {
			// Both Agent Population Controls and Agents list box are displayed
			strList = ReportsHTMLGenerator.createAgentGSAMultiSelect(strAgentType, blnWithTAs, blnWithCOs);
		} else if (getAttribInRequest(request, "displayAgencyMode").equals("2")) {
			// Only Agents list box is displayed
			String userId = request.getUserPrincipal().getName();
			if (userId != null && !userId.equals("")) {
				Agent currentAgent = ModuleServiceLocator.getTravelAgentBD().getAgent(
						ModuleServiceLocator.getSecurityBD().getUserBasicDetails(userId).getAgentCode());
				setAttribInRequest(request, "currentAgentCode", currentAgent.getAgentCode());
				if (currentAgent.getAgentTypeCode().equals(AirTravelAgentConstants.AgentTypes.GSA)) {
					// Agent is a GSA
					strList = ReportsHTMLGenerator.createTAsOfGSAMultiSelect(currentAgent.getAgentCode(), true);
				} else {
					// Agent is not a GSA
					// Only select the current agent
					setAttribInRequest(request, "displayAgencyMode", "0");
				}
			} else {
				// No privilege to access the reports
				throw new ModuleRuntimeException(WebConstants.ERR_UNAUTHORIZED_ACCESS);
			}
		}

		request.setAttribute(WebConstants.REQ_HTML_DETAILS,
				"displayAgencyMode = " + getAttribInRequest(request, "displayAgencyMode") + ";" + "currentAgentCode = '"
						+ getAttribInRequest(request, "currentAgentCode") + "';");

		request.setAttribute(WebConstants.REQ_HTML_GSA_LIST, strList);
	}

	/**
	 * 
	 * @param request
	 * @throws Exception
	 */
	public static void setDisplayData(HttpServletRequest request) throws Exception {
		setDisplayAgencyMode(request);
		setAgentTypes(request);
		setSalesChannelList(request);
		setClientErrors(request);
		setGSAMultiSelectList(request);
		// setPaymentModeList(request);
		setOriginStationList(request);
		setCarrierCodeList(request);
		setLiveStatus(request);
		setPaySource(request);
		setReportingPeriod(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);
	}

	private static void setDisplayAgencyMode(HttpServletRequest request) {

		Map mapPrivileges = (Map) request.getSession().getAttribute(WebConstants.SES_PRIVILEGE_IDS);
		if (mapPrivileges.get("rpt.ta.comp.all") != null) {
			// Show both the controls
			setAttribInRequest(request, "displayAgencyMode", "1");
		} else if (mapPrivileges.get("rpt.ta.comp.rpt") != null) {
			setAttribInRequest(request, "displayAgencyMode", "2");
		} else if (mapPrivileges.get("rpt.ta.comp") != null) {
			setAttribInRequest(request, "displayAgencyMode", "0");
		} else {
			setAttribInRequest(request, "displayAgencyMode", "-1");
		}
	}

	/*
	 * private static void setPaymentModeList(HttpServletRequest request) throws ModuleException { String strList =
	 * ReportsHTMLGenerator.createPaymentModeHtml(); request.setAttribute(WebConstants.REQ_HTML_PAYMENTTYPE_LIST,
	 * strList); }
	 */

	private static void setSalesChannelList(HttpServletRequest request) throws ModuleException {
		String strList = ReportsHTMLGenerator.createSalesChannelHtml();
		request.setAttribute(WebConstants.REQ_HTML_SALES_CHANNEL_LIST, strList);
	}

	private static void setAgentTypes(HttpServletRequest request) throws ModuleException {
		String strATList = SelectListGenerator.createAgentTypeList_SG();
		request.setAttribute(WebConstants.REQ_HTML_AGENTTYPE_LIST, strATList);
	}

	private static void setOriginStationList(HttpServletRequest request) throws ModuleException {
		String strSList = JavascriptGenerator.createStationsByCountry();
		request.setAttribute(WebConstants.REQ_HTML_STATION_COUNTRY, strSList);
	}

	private static void setCarrierCodeList(HttpServletRequest request) throws ModuleException {
		String strCCList = ReportsHTMLGenerator.createCarrierCodesHtml();
		request.setAttribute(WebConstants.REQ_HTML_CARRIER_CODES, strCCList);
	}

	/**
	 * Sets the report medium to the request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleExceptions
	 */
	private static void setLiveStatus(HttpServletRequest request) throws ModuleException {
		String strLive = request.getParameter(WebConstants.REP_HDN_LIVE);
		if (strLive != null)
			request.setAttribute(WebConstants.REP_SET_LIVE, strLive);

	}

	private static void setPaySource(HttpServletRequest request) throws ModuleException {
		String strPay = "false";
		if (AccelAeroClients.AIR_ARABIA.equals(CommonsServices.getGlobalConfig().getBizParam(
				SystemParamKeys.ACCELAERO_CLIENT_IDENTIFIER))
				|| AccelAeroClients.AIR_ARABIA_GROUP.equals(CommonsServices.getGlobalConfig().getBizParam(
						SystemParamKeys.ACCELAERO_CLIENT_IDENTIFIER))) {
			strPay = "true";
		}
		request.setAttribute(WebConstants.REP_RPT_SHOW_PAY, strPay);
	}

	private static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Setting the reporting period to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setReportingPeriod(HttpServletRequest request) throws ModuleException {
		int period = new Integer(globalConfig.getBizParam(SystemParamKeys.REPORTING_PERIOD)).intValue();
		request.setAttribute(WebConstants.REP_RPT_START_DAY, ReportsHTMLGenerator.getReportingPeriod(period));
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		ResultSet resultSet = null;
		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String value = request.getParameter("radReportOption");
		String reportOutOption = request.getParameter("radReportOutOption");
		String agents = "";
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);
		String strPaySource = request.getParameter("hdnPayments");
		String salesChannels = request.getParameter("hdnSalesChannels");
		String stations = request.getParameter("hdnStations");
		String carrierCodes = request.getParameter("hdnCarrierCode");
		String onHoldRequired = request.getParameter("hdnOnholdStatus");

		// String arrPaySources[] = strPaySource.split(",");
		String arrSalesChannels[] = salesChannels.split(",");

		String arrCCodes[] = carrierCodes.split(",");

		// Vector<String> paySourceCol = new Vector<String>();
		Vector<String> salesChanCol = new Vector<String>();
		Vector<String> stationCol = new Vector<String>();
		Vector<String> cCodesCol = new Vector<String>();

		for (int i = 0; i < arrSalesChannels.length; i++) {
			salesChanCol.add(arrSalesChannels[i]);
		}

		for (int i = 0; i < arrCCodes.length; i++) {
			cCodesCol.add(arrCCodes[i]);
		}

		if (strPaySource == null) {
			strPaySource = "INTERNAL";
		}
		if (getAttribInRequest(request, "displayAgencyMode").equals("0")) {
			agents = (String) getAttribInRequest(request, "currentAgentCode");
		} else {
			agents = request.getParameter("hdnAgents");
		}
		ArrayList<String> agentCol = new ArrayList<String>();
		String payments = request.getParameter("hdnPayments");

		String id = "UC_REPM_075";
		String templatePaxSegment = "BookedPAXSegment.jasper";
		String templatePaxSegmentSummary = "BookedPAXSegmentSummary.jasper";

		String showPaymentCurrency = "N";
		String showOnlyPaymentBreakDown = "N";
		String showCurrencyWithBreakDown = "N";

		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";

		try {
			ReportsSearchCriteria search = new ReportsSearchCriteria();

			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}

			Map<String, Object> parameters = new HashMap<String, Object>();

			search.setSalesChannels(salesChanCol);
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
			parameters.put("REPORT_MEDIUM", strlive);
			search.setCarrierCodes(cCodesCol);
			if (onHoldRequired != null && onHoldRequired.equalsIgnoreCase("true")) {
				search.setIncldeOnhold(true);
				parameters.put("SHOW_ONHOLD_SEGMENT", "Y");
			} else {
				search.setIncldeOnhold(false);
				parameters.put("SHOW_ONHOLD_SEGMENT", "N");
			}

			if (fromDate != null && !fromDate.equals("")) {
				search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate) + " 00:00:00");
			}

			if (toDate != null && !toDate.equals("")) {
				search.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate) + " 23:59:59");
			}

			if (agents != "") {
				String agentArr[] = agents.split(",");
				for (int r = 0; r < agentArr.length; r++) {
					agentCol.add(agentArr[r]);
				}
				search.setAgents(agentCol);
			} else {
				search.setAgents(null);
			}

			if (stations != "") {
				String arrStations[] = stations.split(",");
				for (int i = 0; i < arrStations.length; i++) {
					stationCol.add(arrStations[i]);
				}
				search.setStations(stationCol);
			} else {
				search.setStations(null);
			}

			search.setPaymentSource(strPaySource);

			String paymentBreakDown = globalConfig.getBizParam(SystemParamKeys.SHOW_BREAKDOWN);
			String reportInPayCur = globalConfig.getBizParam(SystemParamKeys.REPORTS_IN_AGENTS_CURRENCY);
			String chkBase = request.getParameter("chkBase");

			if (reportOutOption.equalsIgnoreCase("DETAIL")) {
				search.setReportType(ReportsSearchCriteria.BOOKED_PAX_SEGMENT_DETAIL);
				ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
						ReportsHTMLGenerator.getReportTemplate(templatePaxSegment));

			} else if (reportOutOption.equalsIgnoreCase("SUMMARY")) {
				search.setReportType(ReportsSearchCriteria.BOOKED_PAX_SEGMENT_SUMMARY);
				ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
						ReportsHTMLGenerator.getReportTemplate(templatePaxSegmentSummary));
			}

			resultSet = ModuleServiceLocator.getDataExtractionBD().getBookedPAXSegmentData(search);

			parameters.put("FROM_DATE", fromDate);
			parameters.put("TO_DATE", toDate);
			parameters.put("ID", id);
			parameters.put("AGENT_CODE", agents);
			parameters.put("AGENT_NAME", request.getParameter("hdnAgentName"));
			parameters.put("PAYMENT_MODES", payments);
			parameters.put("REPORT_MEDIUM", strlive);
			parameters.put("SOURCE", strPaySource);

			parameters.put("SHOW_BREAKDOWN", showOnlyPaymentBreakDown);

			if (chkBase != null) {
				parameters.put("CHK_BASE", "on");
			}

			String strLogo = AppSysParamsUtil.getReportLogo(false);// globalConfig.getBizParam(SystemParamKeys.REPORT_LOGO);
			String reportsRootDir = "../../images/" + AppSysParamsUtil.getReportLogo(true);
			// String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME)+" Reservation System";
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);

			// To provide Report Format Options
			String reportNumFormat = request.getParameter("radRptNumFormat");
			ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

			if (value.trim().equals("HTML")) {
				parameters.put("IMG", reportsRootDir);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null,
						reportsRootDir, response);

			} else if (value.trim().equals("PDF")) {
				reportsRootDir = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG", reportsRootDir);
				response.reset();
				response.addHeader("Content-Disposition", "filename=BookedPAXSegment.pdf");
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);

			} else if (value.trim().equals("EXCEL")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=BookedPAXSegment.xls");
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);

			} else if (value.trim().equals("CSV")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=BookedPAXSegment.csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);

			}
		} catch (Exception e) {
			log.error(e);
		}
	}

}