package com.isa.thinair.airadmin.core.web.v2.action.master;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.v2.action.common.CommonAdminRequestResponseAwareCommonAction;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airadmin.core.web.v2.generator.master.AirportMessageHG;
import com.isa.thinair.airadmin.core.web.v2.util.CommonUtil;
import com.isa.thinair.airmaster.api.criteria.AirportMsgSearchCriteria;
import com.isa.thinair.airmaster.api.model.AirportMessage;
import com.isa.thinair.airmaster.api.to.AirportMessageTO;
import com.isa.thinair.airpricing.api.model.Channel;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.webplatform.api.v2.util.MultiSelectDataParser;
import com.isa.thinair.webplatform.core.util.DataUtil;
import com.isa.thinair.webplatform.core.util.RowDecorator;

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class AirportMessagesAction extends CommonAdminRequestResponseAwareCommonAction {

	private static Log log = LogFactory.getLog(AirportMessagesAction.class);

	private static final int PAGE_LENGTH = 10;
	private static HashMap<Integer, Channel> salesChannelMap;

	private Collection<Map<String, Object>> rows;
	private int page;
	private int total;
	private int records;

	private AirportMessageTO airportMsgTO;
	private AirportMsgSearchCriteria airportMsgSearchCriteria;

	@SuppressWarnings("unchecked")
	public String searchMessages() {
		try {

			if (salesChannelMap == null) {
				salesChannelMap = AirportMessageHG.getSaleasChannels();
				log.debug(" Sales Channels loaded ");
			}

			int startIndex = (page - 1) * PAGE_LENGTH;
			if (airportMsgSearchCriteria == null) {
				airportMsgSearchCriteria = new AirportMsgSearchCriteria();
			}
			Page pagedReocrds = ModuleServiceLocator.getAirportServiceBD().getAirportMessages(airportMsgSearchCriteria,
					startIndex, PAGE_LENGTH);

			this.page = getCurrentPageNo(pagedReocrds.getStartPosition());
			this.total = getTotalNoOfPages(pagedReocrds.getTotalNoOfRecords());
			this.records = pagedReocrds.getTotalNoOfRecords();

			this.rows = (Collection<Map<String, Object>>) DataUtil.createGridDataDecorateOnly(pagedReocrds.getStartPosition(),
					pagedReocrds.getPageData(), decorateRow());
		} catch (Exception e) {
			this.handleException(e);
			log.error(" Exception occured while laoding airprt messages ", e);
		} finally {
			airportMsgSearchCriteria = null;
		}
		return S2Constants.Result.SUCCESS;
	}

	private RowDecorator<AirportMessage> decorateRow() {
		RowDecorator<AirportMessage> decorator = new RowDecorator<AirportMessage>() {

			@Override
			public void decorateRow(HashMap<String, Object> row, AirportMessage record) {

				row.put("messageId", record.getMessageId());
				row.put("airport", record.getAirport());
				row.put("depArrType", record.getDepArrType());
				row.put("messageType", record.getMessageType());
				row.put("displayFrom", CalendarUtil.getDateInFormattedString("dd/MM/yyyy", record.getDisplayFrom()));
				row.put("dispalyTo", CalendarUtil.getDateInFormattedString("dd/MM/yyyy", record.getDisplayTo()));
				row.put("validFrom", CalendarUtil.getDateInFormattedString("dd/MM/yyyy", record.getValidFrom()));
				row.put("validTo", CalendarUtil.getDateInFormattedString("dd/MM/yyyy", record.getValidTo()));
				row.put("message", record.getMessage());

				Object[] flightNumbers = AirportMessageHG.convertFlightList(record.getFlights(), "FLIGHT_NUMBER");
				row.put("flightNumbers", CommonUtil.convertToJSON(flightNumbers[0]));
				row.put("flightNumbersInclusion", flightNumbers[1]);

				Object[] ONDs = AirportMessageHG.convertONDList(record.getOnds(), "OND");
				row.put("ONDs", CommonUtil.convertToJSON(ONDs[0]));
				row.put("ONDsInclusion", ONDs[1]);

				if (record.getI18nMessageKey() != null && record.getI18nMessageKey().getI18nMessages() != null
						&& !record.getI18nMessageKey().getI18nMessages().isEmpty()) {
					List<MultiSelectDataParser> langaugetranslations = AirportMessageHG.convertLanguageTranslations(record
							.getI18nMessageKey().getI18nMessages());
					row.put("languageList", CommonUtil.convertToJSON(langaugetranslations));
				}

				Object[] salesChannels = AirportMessageHG.convertSalesChannelList(record.getSalesChannels(), salesChannelMap,
						"SALES_CHANNEL");

				row.put("salesChannels", CommonUtil.convertToJSON(salesChannels[0]));

			}
		};
		return decorator;
	}

	public String addEditMessage() {
		try {

			ModuleServiceLocator.getAirportServiceBD().saveAirportMessage(airportMsgTO);
			this.setDefaultSuccessMessage();
		} catch (Exception e) {
			this.handleException(e);
			log.error(" Exception occured while saving airprt messages ", e);
		} finally {
			airportMsgTO = null;
		}
		return S2Constants.Result.SUCCESS;
	}

	public String addEditMessageLanguageTranslation() {

		try {

			ModuleServiceLocator.getAirportServiceBD().saveAirportMessageTranslations(airportMsgTO);
			this.setDefaultSuccessMessage();
		} catch (Exception e) {
			this.handleException(e);
			log.error(" Exception occured while saving airprt message translations ", e);
		} finally {
			airportMsgTO = null;
		}
		return S2Constants.Result.SUCCESS;
	}

	public int getCurrentPageNo(int startPosition) {
		int currentPageNo = 1;
		if (PAGE_LENGTH != 0) {
			currentPageNo = startPosition / PAGE_LENGTH + 1;
		}
		return currentPageNo;
	}

	public int getTotalNoOfPages(int totoalNoOfRecords) {
		int totalNoOfPages = 1;
		if (PAGE_LENGTH != 0) {
			totalNoOfPages = (int) totoalNoOfRecords / PAGE_LENGTH;
			if (totoalNoOfRecords % PAGE_LENGTH > 0)
				totalNoOfPages++;
		}
		return totalNoOfPages;
	}

	@Override
	public String getMessage() {
		return message;
	}

	@Override
	public void setMessage(String message) {
		this.message = message;

	}

	@Override
	public String getMsgType() {
		return msgType;
	}

	@Override
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public Collection<Map<String, Object>> getRows() {
		return rows;
	}

	public void setRows(Collection<Map<String, Object>> rows) {
		this.rows = rows;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public AirportMessageTO getAirportMsgTO() {
		return airportMsgTO;
	}

	public void setAirportMsgTO(AirportMessageTO airportMsgTO) {
		this.airportMsgTO = airportMsgTO;
	}

	public AirportMsgSearchCriteria getAirportMsgSearchCriteria() {
		return airportMsgSearchCriteria;
	}

	public void setAirportMsgSearchCriteria(AirportMsgSearchCriteria airportMsgSearchCriteria) {
		this.airportMsgSearchCriteria = airportMsgSearchCriteria;
	}

}
