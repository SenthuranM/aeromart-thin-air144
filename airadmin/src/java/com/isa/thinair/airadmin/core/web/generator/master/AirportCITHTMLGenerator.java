package com.isa.thinair.airadmin.core.web.generator.master;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airmaster.api.model.AirportCheckInTime;
import com.isa.thinair.commons.api.exception.ModuleException;

public class AirportCITHTMLGenerator {

	private static Log log = LogFactory.getLog(AirportCITHTMLGenerator.class);

	private static String clientErrors;

	/**
	 * Gets the Airport CIT GRID ROW for Selected Airport
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the CIT ROW Data
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final String getAirportCITRowHtml(HttpServletRequest request) throws ModuleException {

		log.debug("inside AirportCITHTMLGenerator.getAirportCITRowHtml()");
		String strAirportCode = request.getParameter("txtAirportCode");

		List<AirportCheckInTime> listArr = new ArrayList<AirportCheckInTime>();
		if (strAirportCode != null && !strAirportCode.equals(""))
			listArr = (List<AirportCheckInTime>) ModuleServiceLocator.getAirportServiceBD().getAirportCITs(strAirportCode);

		return createAirportCITRowHTML(listArr);
	}

	/**
	 * Creates the CIT Grid from a Collection of CITs
	 * 
	 * @param airportCits
	 *            the Collection of CITs
	 * @return String the Created CIT Grid
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private String createAirportCITRowHTML(Collection<AirportCheckInTime> airportCits) throws ModuleException {

		List<AirportCheckInTime> list = (List<AirportCheckInTime>) airportCits;
		Object[] listArr = list.toArray();
		AirportCheckInTime airportCit = null;
		StringBuffer sb = new StringBuffer("var arrData = new Array();");

		for (int i = 0; i < listArr.length; i++) {
			airportCit = (AirportCheckInTime) listArr[i];

			sb.append("arrData[" + i + "] = new Array();");
			sb.append("arrData[" + i + "][1] = '" + ((airportCit.getCarrierCode() != null) ? airportCit.getCarrierCode() : "")
					+ "';");
			sb.append("arrData[" + i + "][2] = '" + ((airportCit.getFlightNumber() != null) ? airportCit.getFlightNumber() : "")
					+ "';");
			sb.append("arrData[" + i + "][3] = '" + formatTime(airportCit.getCheckInGap()) + "';");
			sb.append("arrData[" + i + "][4] = '" + airportCit.getVersion() + "';");
			sb.append("arrData[" + i + "][5] = '" + airportCit.getAirportCheckInTime() + "';");
			sb.append("arrData[" + i + "][6] = '" + formatTime(airportCit.getCheckInClosingTime()) + "';");

		}
		return sb.toString();
	}

	/**
	 * Creates Client Validations for CIT Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array of Client Validations
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {

		if (clientErrors == null) {
			Properties moduleErrs = new Properties();
			moduleErrs.setProperty("um.airadmin.delete.confirmation", "deleteRecoredCfrm");
			moduleErrs.setProperty("um.airportcit.form.time.required", "airportCitRqrd");
			moduleErrs.setProperty("um.airportcit.form.carrier.required", "airportCarrierCodeRqrd");
			moduleErrs.setProperty("um.airportcit.form.carrier.flight.invalid", "airportCitCarrierFlightInvalid");
			moduleErrs.setProperty("um.airportcit.form.carrier.flight.exists", "airportCitCarrierFlightExists");

			moduleErrs.setProperty("um.airportcit.form.time.invalid", "airportCitInvalid");
			moduleErrs.setProperty("um.airportcit.form.closing.time.invalid", "airportCIClosingTimeInvalid");
			moduleErrs.setProperty("um.airportcit.form.time.greater.than.to.time", "airportCIFromTimeGraterThanToTime");
			clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}
		return clientErrors;
	}

	// Time format when returns minutes
	private static String formatTime(int timeInMinutes) {
		int numerator = timeInMinutes;
		int denominator = 60;

		int quotient = numerator / denominator;
		int remainder = numerator % denominator;

		String strRemainder;
		String strQuotientRemainder;
		if (remainder < 10)
			strRemainder = "0" + remainder;
		else
			strRemainder = "" + remainder;

		if (quotient < 10)
			strQuotientRemainder = "0" + quotient;
		else
			strQuotientRemainder = "" + quotient;

		return strQuotientRemainder + ":" + strRemainder;
	}
}
