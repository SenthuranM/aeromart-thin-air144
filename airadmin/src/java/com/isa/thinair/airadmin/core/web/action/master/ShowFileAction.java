package com.isa.thinair.airadmin.core.web.action.master;

import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airschedules.api.utils.FlightUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import java.text.ParseException;

/**
 * @author Thushara Fernando
 * 
 *         This is a special action file to show JSP files directly with out any presentations logic being executed.
 *         File Name itself is used instead of separate result value to make mappings simple. So in the result
 *         annotation filename itself is specified as name as well as the value.
 * 
 *         eg : @Result(name=S2Constants.Jsp.Common.EXAMPLE_FILE, value=S2Constants.Jsp.Common.EXAMPLE_FILE)
 * 
 * 
 *         To work this <constant name="struts.enable.DynamicMethodInvocation" value="true" /> should be set in the
 *         struts.xml file
 * 
 */
@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Results({
		@Result(name = AdminStrutsConstants.AdminJSP.FLITGT_SCED_COPY, value = AdminStrutsConstants.AdminJSP.FLITGT_SCED_COPY),
		@Result(name = AdminStrutsConstants.AdminJSP.FLITGT_SCED_SPILT, value = AdminStrutsConstants.AdminJSP.FLITGT_SCED_SPILT),
		@Result(name = AdminStrutsConstants.AdminJSP.FLITGT_SCED_REPROTECT_ROLLFWD, value = AdminStrutsConstants.AdminJSP.FLITGT_SCED_REPROTECT_ROLLFWD),
		@Result(name = AdminStrutsConstants.AdminJSP.SELECT_REPROTECT_PNR_JSP, value = AdminStrutsConstants.AdminJSP.SELECT_REPROTECT_PNR_JSP),
		@Result(name = AdminStrutsConstants.AdminJSP.ROLLFORWARD_CONFIRMATION_PAGE, value = AdminStrutsConstants.AdminJSP.ROLLFORWARD_CONFIRMATION_PAGE),
		@Result(name = AdminStrutsConstants.AdminJSP.FLITGT_CANCEL, value = AdminStrutsConstants.AdminJSP.FLITGT_CANCEL),
		@Result(name = AdminStrutsConstants.AdminJSP.FLITGT_COPY, value = AdminStrutsConstants.AdminJSP.FLITGT_COPY),
		@Result(name = AdminStrutsConstants.AdminJSP.FLITGT_AMEND, value = AdminStrutsConstants.AdminJSP.FLITGT_AMEND),

		@Result(name = AdminStrutsConstants.AdminJSP.REVENUE_RPT_RESULT, value = AdminStrutsConstants.AdminJSP.REVENUE_RPT_RESULT),
		@Result(name = AdminStrutsConstants.AdminJSP.INTERLINE_REVENUE_RPT_RESULT, value = AdminStrutsConstants.AdminJSP.INTERLINE_REVENUE_RPT_RESULT),
		@Result(name = AdminStrutsConstants.AdminJSP.SELECT_REPROTECT_PNR_JSP_V2, value = AdminStrutsConstants.AdminJSP.SELECT_REPROTECT_PNR_JSP_V2),
		@Result(name = AdminStrutsConstants.AdminJSP.PAX_DETAILS, value = AdminStrutsConstants.AdminJSP.PAX_DETAILS) })
public class ShowFileAction extends BaseRequestAwareAction {

	public String copySchedule() {
		return AdminStrutsConstants.AdminJSP.FLITGT_SCED_COPY;
	}

	public String splitSchedule() {
		return AdminStrutsConstants.AdminJSP.FLITGT_SCED_SPILT;
	}

	public String reprotectRollForwad() {
		return AdminStrutsConstants.AdminJSP.FLITGT_SCED_REPROTECT_ROLLFWD;
	}

	public String cancelFlight() {
		return AdminStrutsConstants.AdminJSP.FLITGT_CANCEL;
	}

	public String advanceRollForwardResult() {
		return AdminStrutsConstants.AdminJSP.ROLLFORWARD_CONFIRMATION_PAGE;
	}
	public String copyFlight() {
		return AdminStrutsConstants.AdminJSP.FLITGT_COPY;
	}

	public String amendFlight() throws ParseException {
		String flightDeptTime = request.getParameter(WebConstants.REQ_HDN_DEPT_DATE_TIME);
		request.setAttribute(WebConstants.SHOW_ETICKET_OPENING_SECTION,
				FlightUtil.showETOpeningSection(flightDeptTime));
		return AdminStrutsConstants.AdminJSP.FLITGT_AMEND;
	}

	public String revenueSucess() {
		return AdminStrutsConstants.AdminJSP.REVENUE_RPT_RESULT;
	}

	public String interlineRevenue() {
		return AdminStrutsConstants.AdminJSP.INTERLINE_REVENUE_RPT_RESULT;
	}

	public String selectReprotectPNR() {

		request.setAttribute("hdnFlightSegId", request.getParameter("hdnFlightSegId"));
		request.setAttribute("hdnCabinClass", request.getParameter("hdnCabinClass"));
		request.setAttribute("hdnTransferAll", request.getParameter("hdnTransferAll"));
		request.setAttribute("hdnSelRowNo", request.getParameter("rowNo"));
		return AdminStrutsConstants.AdminJSP.SELECT_REPROTECT_PNR_JSP;
	}
	
	public String selectReprotectPNR_v2() {

		request.setAttribute("hdnFlightSegId", request.getParameter("hdnFlightSegId"));
		request.setAttribute("hdnCabinClass", request.getParameter("hdnCabinClass"));
		request.setAttribute("hdnTransferAll", request.getParameter("hdnTransferAll"));
		request.setAttribute("hdnSelRowNo", request.getParameter("rowNo"));
        request.setAttribute("hdnNewFlightSegId", request.getParameter("hdnNewFlightSegId"));

        return AdminStrutsConstants.AdminJSP.SELECT_REPROTECT_PNR_JSP_V2;
	}

	public String paxDetails() {
		request.setAttribute("hdnFlightId", request.getParameter("hdnFlightId"));

		return AdminStrutsConstants.AdminJSP.PAX_DETAILS;
	}

}
