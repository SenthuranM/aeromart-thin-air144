package com.isa.thinair.airadmin.core.web.v2.action.promotion;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONResult;
import org.apache.struts2.json.JSONUtil;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airmaster.api.model.I18nMessage;
import com.isa.thinair.airmaster.api.model.I18nMessageKey;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.promotion.api.to.PromoCriteriaSearchTO;
import com.isa.thinair.promotion.api.to.PromotionCriteriaTO;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ManagePromotionCriteriaAction {

	/** PromotionCriteria DTO object to transfer data to the back end */
	private PromotionCriteriaTO promotionCriteriaTO = new PromotionCriteriaTO();

	/** Promotion criteria search data */
	private PromoCriteriaSearchTO promoCriteriaSearch = new PromoCriteriaSearchTO();

	private static Log log = LogFactory.getLog(ManagePromotionCriteriaAction.class);

	/** Operation success status indicator */
	private boolean success = true;

	/** Success/Failure message for UI display */
	private String messageTxt;

	/** Search result and pagination data */
	private Collection<Map<String, Object>> rows;
	private int page;
	private int total;
	private int records;

	/** Date format for front end data data */
	private final String DATE_FORMAT = "dd/MM/yyyy";
	private int promoCriteriaID;
	private String reservationAvailable;

	private String pcSaveForDes;

	/** In the case of multiple promotion code generation */
	private int numOfCodes;

	private List<String> sysGenPromoCodes;

	private boolean multiplePromoCodeGenerated = false;

	private List<String[]> pos;

	/*
	 * Search method
	 */
	public String search() {

		try {
			Page<PromotionCriteriaTO> criteriaSearchPage = null;
			int start = (page - 1) * 20;
			Map<String, Object> row;

			if (promoCriteriaSearch != null) {

				criteriaSearchPage = ModuleServiceLocator.getPromotionCriteriaAdminBD()
						.searchPromotionCriteria(promoCriteriaSearch, start, 20);

				this.setRecords(criteriaSearchPage.getTotalNoOfRecords());
				this.total = criteriaSearchPage.getTotalNoOfRecords() / 20;
				int mod = criteriaSearchPage.getTotalNoOfRecords() % 20;
				if (mod > 0) {
					this.total = this.total + 1;
				}

				Collection<PromotionCriteriaTO> criterias = criteriaSearchPage.getPageData();
				List<PromotionCriteriaTO> criteriaList = new ArrayList<PromotionCriteriaTO>(criterias);
				Collections.sort(criteriaList, new Comparator<PromotionCriteriaTO>() {
					public int compare(PromotionCriteriaTO p1, PromotionCriteriaTO p2) {
						return p2.getPromoCriteriaID().compareTo(p1.getPromoCriteriaID());
					}
				});
				rows = new ArrayList<Map<String, Object>>();

				// language translations
				Map<String, Map<String, String>> pcForDisplayList = ModuleServiceLocator.getCommonServiceBD()
						.getTranslations("PRM_DES");

				int i = 1;
				for (PromotionCriteriaTO criteria : criteriaList) {

					Map<String, String> criteriaTrans = pcForDisplayList.get("PRM_DES" + criteria.getPromoCriteriaID());

					if (criteriaTrans != null) {
						criteria.setPcDescForDisplay(formPCDisplayString(criteriaTrans, criteria.getPromoCriteriaID()));
					}
					row = new HashMap<String, Object>();
					row.put("id", ((page - 1) * 20) + i++);
					row.put("criteria", criteria);

					String strPCTrans = null;

					row.put("pctranslations", strPCTrans);
					rows.add(row);
				}
			}

		} catch (Exception e) {
			log.error(messageTxt, e);
			success = false;
		}

		return S2Constants.Result.SUCCESS;
	}

	/*
	 * Save method. Create new PromtionCriteria
	 */
	public String save() {

		try {

			multiplePromoCodeGenerated = promotionCriteriaTO.getMultiCodeGenerated();

			Long promotionCrieteriaID = ModuleServiceLocator.getPromotionCriteriaAdminBD()
					.savePromotionCriteria(promotionCriteriaTO);
			if (promotionCrieteriaID == -1) {
				success = false;
				messageTxt = S2Constants.messageText.DUPLICATE_RECORD;
			} else {
				promotionCriteriaTO.setPromoCriteriaID(promotionCrieteriaID);
			}

		} catch (Exception e) {
			log.error(messageTxt, e);
			success = false;
		}

		return S2Constants.Result.SUCCESS;
	}

	/*
	 * Update method. Saves changes to existing PromotionCriteria
	 */
	public String update() {

		try {
			long promotionCrieteriaID = ModuleServiceLocator.getPromotionCriteriaAdminBD()
					.updatePromotionCriteria(promotionCriteriaTO);
			if (promotionCrieteriaID == -1) {
				success = false;
				messageTxt = S2Constants.messageText.DUPLICATE_RECORD;
			} else {
				promotionCriteriaTO.setPromoCriteriaID(promotionCrieteriaID);
			}
		} catch (Exception e) {
			log.error(messageTxt, e);
			success = false;
		}

		return S2Constants.Result.SUCCESS;
	}

	public String statusUpdate() {

		try {
			long promotionCrieteriaID = ModuleServiceLocator.getPromotionCriteriaAdminBD()
					.updatePromotionCriteriaStatus(promotionCriteriaTO);
			if (promotionCrieteriaID == -1) {
				success = false;
				messageTxt = S2Constants.messageText.DUPLICATE_RECORD;
			} else {
				promotionCriteriaTO.setPromoCriteriaID(promotionCrieteriaID);
			}
		} catch (Exception e) {
			log.error(messageTxt, e);
			success = false;
		}

		return S2Constants.Result.SUCCESS;
	}

	/*
	 * Delete method.
	 */
	public String delete() {

		try {
			ModuleServiceLocator.getPromotionCriteriaAdminBD().deletePromotionCriteria(promotionCriteriaTO);
		} catch (Exception e) {
			log.error(messageTxt, e);
			success = false;
		}

		return S2Constants.Result.SUCCESS;
	}

	public String searchReservation() {
		try {
			if (ModuleServiceLocator.getReservationBD().isReservationAvailable(promotionCriteriaTO.getPromoCriteriaID())) {
				reservationAvailable = "Y";
			} else {
				reservationAvailable = "N";
			}

		} catch (Exception e) {
			log.error(messageTxt, e);
			success = false;
		}
		return S2Constants.Result.SUCCESS;
	}

	public String updatePCDesc() {

		try {
			String val = pcSaveForDes;
			String[] outerValues = val.split("~");
			Map<String, String> translationsMap = new HashMap<String, String>();
			Collection<I18nMessageKey> msgKeys = new ArrayList<I18nMessageKey>();
			I18nMessageKey msgKey = new I18nMessageKey();

			long id = 0;
			String messageKey;

			// msgKey.setVersion(1);
			Set<I18nMessage> msgSet = new HashSet<I18nMessage>();
			for (int i = 0; i < outerValues.length; i++) {

				String[] innerValues = outerValues[i].split(",");// langCode,pcName,id
				id = Long.parseLong(innerValues[2]);
				/*
				 * String[] innerValues = outerValues[i].split(","); I18nMessage msg = new I18nMessage();
				 * msg.setMessageLocale("en"); msg.setMessageContent(Hibernate.createClob(StringUtil.convertToHex("Test"
				 * + i))); msg.setMessageId(Long.parseLong(innerValues[3])); // msg.setI18nMessageKey(msgKey);
				 * msgKey.addI18nMessage(msg);formPCDisplayString
				 */
				translationsMap.put(innerValues[0], innerValues[1]);

			}

			msgKeys.add(msgKey);
			ModuleServiceLocator.getCommonServiceBD().saveTranslationWithoutMessageKey(id, translationsMap);
			ModuleServiceLocator.getPromotionCriteriaAdminBD().updatePromotionCodeDescKey(id, null);

		} catch (Exception e) {
			log.error(messageTxt, e);
			success = false;
		}

		return S2Constants.Result.SUCCESS;
	}

	private String formPCDisplayString(Map<String, String> criteriaTrans, long id) {

		StringBuilder sb = new StringBuilder();

		String ucs = "";
		// langCode,pcName,id,ver
		for (String lang : criteriaTrans.keySet()) {
			try {
				ucs = criteriaTrans.get(lang);
			} catch (Exception e) {
				ucs = "Undefined";
			}
			ucs = ucs.replace(",", "^");
			sb.append(lang);
			sb.append(",");
			sb.append(ucs);
			sb.append(",");
			sb.append(id);
			sb.append(",");
			sb.append(-1);

			sb.append("~");

		}

		return sb.toString();
	}

	public String generateCodes() {

		try {
			sysGenPromoCodes = ModuleServiceLocator.getPromotionCriteriaAdminBD().generatePromoCodes(numOfCodes);
		} catch (Exception e) {
			log.error(messageTxt, e);
			success = false;
		}

		return S2Constants.Result.SUCCESS;
	}

	public String loadPOSData() {
		String forward = S2Constants.Result.SUCCESS;
		try {
			pos = SelectListGenerator.getCountryNameList();
		} catch (ModuleException ex) {
			log.error(ex);
			success = false;
			messageTxt = ex.getMessageString();
		} catch (Exception ex) {
			log.error(ex);
			success = false;
			messageTxt = ex.getMessage();
		}
		return forward;
	}

	public PromotionCriteriaTO getPromotionCriteriaTO() {
		return promotionCriteriaTO;
	}

	public void setPromotionCriteriaTO(PromotionCriteriaTO promotionCriteriaTO) {
		this.promotionCriteriaTO = promotionCriteriaTO;
	}

	public PromoCriteriaSearchTO getPromoCriteriaSearch() {
		return promoCriteriaSearch;
	}

	public void setPromoCriteriaSearch(PromoCriteriaSearchTO promoCriteriaSearch) {
		this.promoCriteriaSearch = promoCriteriaSearch;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public Collection<Map<String, Object>> getRows() {
		return rows;
	}

	public void setRows(Collection<Map<String, Object>> rows) {
		this.rows = rows;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public void setSearchReservationFromDate(String reservationFromDate) throws ParseException {
		promoCriteriaSearch.setReservationFrom((new SimpleDateFormat(DATE_FORMAT).parse(reservationFromDate)));
	}

	public void setSearchReservationToDate(String reservationToDate) throws ParseException {
		promoCriteriaSearch.setReservationTo(new SimpleDateFormat(DATE_FORMAT).parse(reservationToDate));
	}

	public void setConstFlightFromDate(String constFlightFromDate) throws ParseException {
		promotionCriteriaTO.setConstFlightFrom(new SimpleDateFormat(DATE_FORMAT).parse(constFlightFromDate));
	}

	public void setConstFlightToDate(String constFlightToDate) throws ParseException {
		promotionCriteriaTO.setConstFlightTo(new SimpleDateFormat(DATE_FORMAT).parse(constFlightToDate));
	}

	public void setConstValidFromDate(String constValidFromDate) throws ParseException {
		promotionCriteriaTO.setConstValidFrom(new SimpleDateFormat(DATE_FORMAT).parse(constValidFromDate));
	}

	public void setConstValidToDate(String constValidToDate) throws ParseException {
		promotionCriteriaTO.setConstValidTo(new SimpleDateFormat(DATE_FORMAT).parse(constValidToDate));
	}

	public void setApplicableONDs(String applicableONDs) throws JSONException {
		Set<String> ondList = new HashSet<String>((List<String>) JSONUtil.deserialize(applicableONDs));
		promotionCriteriaTO.setApplicableONDs(ondList);
	}

	public void setFlightNOs(String flightNOs) throws JSONException {
		Set<String> fltNoList = new HashSet<String>((List<String>) JSONUtil.deserialize(flightNOs));
		promotionCriteriaTO.setFlightNOs(fltNoList);
	}

	public void setApplicableCOSs(String applicableCOSs) throws JSONException {
		Set<String> ondSet = new HashSet((List<String>) JSONUtil.deserialize(applicableCOSs));
		promotionCriteriaTO.setApplicableCOSs(ondSet);
	}

	public void setApplicableLCCs(String applicableLCCs) throws JSONException {
		Set<String> ondSet = new HashSet((List<String>) JSONUtil.deserialize(applicableLCCs));
		promotionCriteriaTO.setApplicableLCCs(ondSet);
	}

	public void setApplicableBCs(String applicableBCs) throws JSONException {
		Set<String> ondSet = new HashSet((List<String>) JSONUtil.deserialize(applicableBCs));
		promotionCriteriaTO.setApplicableBCs(ondSet);
	}

	public void setApplicableSalesChannels(String applicableSalesChannels) throws JSONException {
		Set<String> ondSet = new HashSet((List<String>) JSONUtil.deserialize(applicableSalesChannels));
		promotionCriteriaTO.setApplicableSalesChannels(ondSet);
	}

	public void setApplicableAgents(String applicableAgents) throws JSONException {
		Set<String> agentSet = new HashSet((List<String>) JSONUtil.deserialize(applicableAgents));
		promotionCriteriaTO.setApplicableAgents(agentSet);
	}

	public void setFreeCriteria(String freeCriteria) throws JSONException {
		Map<String, String> freeCriteriaMap = (Map<String, String>) JSONUtil.deserialize(freeCriteria);
		promotionCriteriaTO.setFreeCriteria(freeCriteriaMap);
	}

	public void setApplicableAncillaries(String applicableAncillaries) throws JSONException {
		Set<String> anciSet = new HashSet((List<String>) JSONUtil.deserialize(applicableAncillaries));
		promotionCriteriaTO.setApplicableAncillaries(anciSet);
	}

	public void setApplicableBINs(String applicableBINs) throws JSONException {
		Set<Integer> binSet = new HashSet<Integer>();
		for (String str : (List<String>) JSONUtil.deserialize(applicableBINs)) {
			binSet.add(Integer.parseInt(str));
		}
		promotionCriteriaTO.setApplicableBINs(binSet);
	}

	public void setPosCountries(String posCountries) throws JSONException {
		Set<String> countriesList = new HashSet<String>((List<String>) JSONUtil.deserialize(posCountries));
		promotionCriteriaTO.setPosCountries(countriesList);
	}

	public int getPromoCriteriaID() {
		return promoCriteriaID;
	}

	public void setPromoCriteriaID(int promoCriteriaID) {
		this.promoCriteriaID = promoCriteriaID;
	}

	public String getReservationAvailable() {
		return reservationAvailable;
	}

	public String getPcSaveForDes() {
		return pcSaveForDes;
	}

	public void setPcSaveForDes(String pcSaveForDes) {
		this.pcSaveForDes = pcSaveForDes;
	}

	public int getNumOfCodes() {
		return numOfCodes;
	}

	public void setNumOfCodes(int numOfCodes) {
		this.numOfCodes = numOfCodes;
	}

	public List<String> getSysGenPromoCodes() {
		return sysGenPromoCodes;
	}

	public void setSysGenPromoCodes(List<String> sysGenPromoCodes) {
		this.sysGenPromoCodes = sysGenPromoCodes;
	}

	public Boolean getMultiplePromoCodeGenerated() {
		return multiplePromoCodeGenerated;
	}

	public void setMultiplePromoCodeGenerated(Boolean multiplePromoCodeGenerated) {
		this.multiplePromoCodeGenerated = multiplePromoCodeGenerated;
	}

	public List<String[]> getPos() {
		return pos;
	}

	public void setRegistrationPeriods(String registrationPeriods) throws JSONException {
		Set<String> regPeriodSet = new HashSet((List<String>) JSONUtil.deserialize(registrationPeriods));
		promotionCriteriaTO.setRegistrationPeriods(regPeriodSet);
	}

	public void setFlightPeriods(String flightPeriods) throws JSONException {
		Set<String> fltPeriodSet = new HashSet((List<String>) JSONUtil.deserialize(flightPeriods));
		promotionCriteriaTO.setFlightPeriods(fltPeriodSet);
	}

}
