package com.isa.thinair.airadmin.core.web.v2.handler.master;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airmaster.api.model.State;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

public class StateRH {

	private static Log log = LogFactory.getLog(StateRH.class);

	public static Page<State> searchState(int pageNo, List<ModuleCriterion> criterion) {
		Page<State> page = null;
		try {
			if (criterion == null) {
				page = ModuleServiceLocator.getCommonServiceBD().getStates((pageNo - 1) * 20, 20);
			} else {
				if (pageNo == 0) {
					pageNo = 1;
				}
				page = ModuleServiceLocator.getCommonServiceBD().getStates((pageNo - 1) * 20, 20, criterion);
			}
		} catch (ModuleException e) {
			log.error(e);
		}
		return page;
	}

	public static boolean saveState(State state) throws ModuleException {
		ModuleServiceLocator.getCommonServiceBD().saveStates(state);
		return true;
	}

	public static boolean deleteState(Integer stateId) throws ModuleException {
		ModuleServiceLocator.getCommonServiceBD().deleteState(stateId);
		return true;
	}

	public static State getState(Integer stateId) throws ModuleException {
		State state = ModuleServiceLocator.getCommonServiceBD().getState(stateId);
		return state;
	}

	public static List<State> getStates(String countryCode)
			throws ModuleException {
		List<State> stateList= ModuleServiceLocator.getCommonServiceBD().getStates(countryCode);
		return stateList;
	}
	
}
