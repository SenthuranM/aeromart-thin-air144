package com.isa.thinair.airadmin.core.web.action.tools;

import java.util.Collection;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.auditor.api.dto.PFSAuditDTO;
import com.isa.thinair.auditor.api.service.AuditorBD;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ShowPFSHistoryAction extends BaseRequestAwareAction {

	private Collection<PFSAuditDTO> rows;
	private int page;
	private int records;
	private long pfsID;
	private String ipAddress;

	public String execute() {

		try {
			AuditorBD auditorBD = ModuleServiceLocator.getAuditorServiceBD();
			Collection<PFSAuditDTO> pfsAuditDTOs = auditorBD.retrievePFSAudits(getPfsID());
			setDisplayData(pfsAuditDTOs);

		} catch (Exception e) {
			// TODO: handle exception
		}
		return S2Constants.Result.SUCCESS;

	}

	private void setDisplayData(Collection<PFSAuditDTO> pfsAuditDTOs) {

		this.records = pfsAuditDTOs.size();
		if (this.page <= 0)
			this.page = 1;
		this.rows = pfsAuditDTOs;
	}

	public void setRows(Collection<PFSAuditDTO> rows) {
		this.rows = rows;
	}

	public Collection<PFSAuditDTO> getRows() {
		return rows;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPage() {
		return page;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public int getRecords() {
		return records;
	}

	public void setPfsID(long pfsID) {
		this.pfsID = pfsID;
	}

	public long getPfsID() {
		return pfsID;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

}
