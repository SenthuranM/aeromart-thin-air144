/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */

/**
 *
 *
 *
 * Window - Preferences - Java - Code Style - Code Templates
 */

package com.isa.thinair.airadmin.core.web.generator.tools;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.messagepasser.api.model.PRL;
import com.isa.thinair.messagepasser.api.utils.ParserConstants;

public class PRLProcessingHTMLGenerator {

	private static String clientErrors;

	// searching parameters
	private static final String PARAM_SEARCH_AIRPORT = "selAirport";

	private static final String PARAM_SEARCH_FROM = "txtFrom";

	private static final String PARAM_SEARCH_TO = "txtTo";

	private static final String PARAM_START_REC_NO = "hdnRecNo";

	private static final String PARAM_SEARCH_PROCESS_STATUS = "selProcessType";

	private static final String PARAM_MODE = "hdnMode";

	private String strFormFieldsVariablesJS = "";

	private static final String DATE_FORMAT_DDMMYYYYHHmm = "dd/MM/yyyy HH:mm";

	private static final String DATE_FORMAT_DDMMYYYY = "dd/MM/yyyy";

	SimpleDateFormat outputDateFormat;
	SimpleDateFormat outputDateFormat_onlyDate;

	@SuppressWarnings("unchecked")
	public final String getPRLProcessingRowHtml(HttpServletRequest request) throws ModuleException, Exception {

		String strMode = request.getParameter(PARAM_MODE);
		String strStartRecNo = (request.getParameter(PARAM_START_REC_NO) == null || request
				.getParameter(PARAM_START_REC_NO).trim().equals("")) ? "1" : request.getParameter(PARAM_START_REC_NO);
		String strUIModeJS = "var isSearchMode = false; var strPRLContent;";
		String strAirportCode = "";
		String strFromDate = "";
		String strToDate = "";
		String strProcessStatus = "";
		Date fromDate = null;
		Date toDate = null;

		Page page = null;

		outputDateFormat = new SimpleDateFormat(DATE_FORMAT_DDMMYYYYHHmm);
		outputDateFormat_onlyDate = new SimpleDateFormat(DATE_FORMAT_DDMMYYYY);

		if (strMode != null) {
			strUIModeJS = "var isSearchMode = true; var strPRLContent;";

			strAirportCode = AiradminUtils.getNotNullString(request.getParameter(PARAM_SEARCH_AIRPORT));
			strFormFieldsVariablesJS = "var airportCode ='" + strAirportCode + "';";

			strFromDate = AiradminUtils.getNotNullString(request.getParameter(PARAM_SEARCH_FROM));
			strFormFieldsVariablesJS += "var fromDate ='" + strFromDate + "';";

			strToDate = AiradminUtils.getNotNullString(request.getParameter(PARAM_SEARCH_TO));
			strFormFieldsVariablesJS += "var toDate ='" + strToDate + "';";

			strProcessStatus = (request.getParameter(PARAM_SEARCH_PROCESS_STATUS) == null || request
					.getParameter(PARAM_SEARCH_PROCESS_STATUS).trim().toUpperCase().equals("ALL")) ?
					"" :
					request.getParameter(PARAM_SEARCH_PROCESS_STATUS).trim();
			strFormFieldsVariablesJS += "var processStatus ='" + strProcessStatus + "';";

			SimpleDateFormat dateFormat = null;

			if (!strFromDate.equals("")) {
				if (strFromDate.indexOf('-') != -1) {
					dateFormat = new SimpleDateFormat("dd-MM-yy");
				}
				if (strFromDate.indexOf('/') != -1) {
					dateFormat = new SimpleDateFormat("dd/MM/yy");
				}
				if (strFromDate.indexOf(' ') != -1) {
					strFromDate = strFromDate.substring(0, strFromDate.indexOf(' '));
				}

				fromDate = dateFormat.parse(strFromDate);
			} else {
				fromDate = null;
			}

			if (!strToDate.equals("")) {
				if (strToDate.indexOf('-') != -1) {
					dateFormat = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
				}
				if (strToDate.indexOf('/') != -1) {
					dateFormat = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
				}
				if (strToDate.indexOf(' ') != -1) {
					strToDate = strToDate.substring(0, strToDate.indexOf(' '));
				}

				toDate = dateFormat.parse(strToDate + " 23:59:59");
			} else {
				toDate = null;
			}

			request.setAttribute(WebConstants.REQ_OPERATION_UI_MODE, strUIModeJS);

			// Make Criterians
			List<ModuleCriterion> critrian = new ArrayList<ModuleCriterion>();
			List<String> orderList = new ArrayList<String>();
			orderList.add("dateDownloaded");

			if (fromDate != null) {
				// Set criterian - fromDate
				ModuleCriterion MCname = new ModuleCriterion();
				MCname.setCondition(ModuleCriterion.CONDITION_GREATER_THAN_OR_EQUALS);
				MCname.setFieldName("dateDownloaded");
				List<Date> value = new ArrayList<Date>();
				value.add(fromDate);
				MCname.setValue(value);
				critrian.add(MCname);
			}

			if (toDate != null) {
				// Set criterian - toDate
				ModuleCriterion MCname = new ModuleCriterion();
				MCname.setCondition(ModuleCriterion.CONDITION_LESS_THAN_OR_EQUALS);
				MCname.setFieldName("dateDownloaded");
				List<Date> value = new ArrayList<Date>();
				value.add(toDate);
				MCname.setValue(value);
				critrian.add(MCname);
			}

			if (!strAirportCode.equals("")) {
				// Set criterian - strAirportCode
				ModuleCriterion MCname = new ModuleCriterion();
				MCname.setCondition(ModuleCriterion.CONDITION_EQUALS);
				MCname.setFieldName("fromAirport");
				List<String> value = new ArrayList<String>();
				value.add(strAirportCode);
				MCname.setValue(value);
				critrian.add(MCname);
			}

			if (!strProcessStatus.equals("")) {
				// Set criterian - strProcessStatus
				ModuleCriterion MCname = new ModuleCriterion();
				MCname.setCondition(ModuleCriterion.CONDITION_EQUALS);
				MCname.setFieldName("processedStatus");
				List<String> value = new ArrayList<String>();
				value.add(strProcessStatus);
				MCname.setValue(value);
				critrian.add(MCname);
			}

			page = ModuleServiceLocator.getPRLBD()
					.getPagedPRLData(critrian, Integer.valueOf(strStartRecNo).intValue() - 1, 20, orderList);

			if (page != null) {
				request.getSession().setAttribute("PRLDataSet", page.getPageData());
			} else {
				request.getSession().setAttribute("PRLDataSet", new ArrayList<PRL>());
			}

			strFormFieldsVariablesJS += " totalRes  ='" + page.getTotalNoOfRecords() + "';";
		}

		setFormFieldValues(strFormFieldsVariablesJS);

		if (page != null) {
			return createPRLProcessingRowHTML(page.getPageData());
		} else {
			return createPRLProcessingRowHTML(new ArrayList<PRL>());
		}

	}

	private String createPRLProcessingRowHTML(Collection<PRL> listPRLProcessing) throws ModuleException {

		List<PRL> list = (List<PRL>) listPRLProcessing;

		Object[] listArr = list.toArray();
		StringBuffer sb = new StringBuffer("var arrData = new Array();");

		PRL prl = null;
		String tempDesc = "";
		for (int i = 0; i < listArr.length; i++) {
			prl = (PRL) listArr[i];
			sb.append("arrData[" + i + "] = new Array();");

			sb.append("arrData[" + i + "][0] = '" + outputDateFormat.format(prl.getDateDownloaded()) + "';");

			if (prl.getFlightNumber() != null) {
				sb.append("arrData[" + i + "][1] = '" + prl.getFlightNumber() + "';");
			} else {
				sb.append("arrData[" + i + "][1] = '';");
			}

			if (prl.getDepartureDate() != null) {
				sb.append("arrData[" + i + "][2] = '" + outputDateFormat.format(prl.getDepartureDate()) + "';");
			} else {
				sb.append("arrData[" + i + "][2] = '';");
			}

			sb.append("arrData[" + i + "][3] = '" + prl.getPrlId() + "';");

			if (ParserConstants.PRLStatus.PARSED.equals(prl.getProcessedStatus())) {
				tempDesc = "Parsed";
			} else if (ParserConstants.PRLStatus.UN_PARSED.equals(prl.getProcessedStatus())) {
				tempDesc = "Unparsed";
			} else if (ParserConstants.PRLProcessStatus.ERROR_OCCURED.equals(prl.getProcessedStatus())) {
				tempDesc = "Processed with errors";
			} else if (ParserConstants.PRLStatus.RECONCILE_SUCCESS.equals(prl.getProcessedStatus())) {
				tempDesc = "Processed";
			} else {
				tempDesc = "Unknown";
			}
			sb.append("arrData[" + i + "][4] = '" + tempDesc + "';");

			if (prl.getPrlContent() != null) {
				sb.append(
						"arrData[" + i + "][5] = '" + StringUtils.replace(prl.getPrlContent(), "\n", "<\\BR>") + "';");

			} else {
				sb.append("arrData[" + i + "][5] = '';");
			}
			if (ParserConstants.PRLProcessStatus.ERROR_OCCURED.equals(prl.getProcessedStatus())) {
				sb.append("arrData[" + i + "][6] = '" + ParserConstants.PRLProcessStatus.ERROR_OCCURED + "';");
			} else {
				sb.append("arrData[" + i + "][6] = '" + prl.getProcessedStatus() + "';");
			}

			sb.append("arrData[" + i + "][7] = new Array();");

			sb.append("arrData[" + i + "][8] = '';");

			if (prl.getFlightNumber() != null) {
				sb.append("arrData[" + i + "][9] = '" + prl.getFlightNumber() + "';");
			} else {
				sb.append("arrData[" + i + "][9] = '';");
			}
			if (prl.getDepartureDate() != null) {
				if (ParserConstants.PRLProcessStatus.ERROR_OCCURED.equals(prl.getProcessedStatus())) {
					sb.append("arrData[" + i + "][10] = '" + outputDateFormat_onlyDate.format(prl.getDepartureDate())
							+ "';");
				} else {
					sb.append("arrData[" + i + "][10] = '" + outputDateFormat.format(prl.getDepartureDate()) + "';");
				}
			} else {
				sb.append("arrData[" + i + "][10] = '';");
			}
			if (prl.getFromAirport() != null) {
				sb.append("arrData[" + i + "][11] = '" + prl.getFromAirport() + "';");
			} else {
				sb.append("arrData[" + i + "][11] = '';");
			}
			if (prl.getFromAddress() != null) {
				sb.append("arrData[" + i + "][12] = '" + prl.getFromAddress() + "';");
			} else {
				sb.append("arrData[" + i + "][12] = '';");
			}

			sb.append("arrData[" + i + "][13] = " + prl.getVersion() + ";");

			sb.append("arrData[" + i + "][14] = "
					+ ModuleServiceLocator.getPRLBD().getPRLParseEntryCount(prl.getPrlId(), null) + ";");

		}

		return sb.toString();
	}

	private void setFormFieldValues(String strFormFieldsVariablesJS) {
		this.strFormFieldsVariablesJS = strFormFieldsVariablesJS;
	}

	public String getFormFieldValues() {
		return strFormFieldsVariablesJS;
	}

	public static String getClientErrors() throws Exception {
		if (clientErrors == null) {
			Properties props = new Properties();
			props.setProperty("um.prlprocess.form.fromdate.lessthan.currentdate", "fromDateLessthanCurrentDate");
			props.setProperty("um.prlprocess.form.todate.lessthan.currentdate", "toDateLessthanToday");
			props.setProperty("um.prlprocess.form.todate.lessthan.fromdate", "todateLessthanFromDate");
			props.setProperty("um.airadmin.delete.confirmation", "deleteMessage");

			clientErrors = JavascriptGenerator.createClientErrors(props);
		}
		return clientErrors;
	}

}
