package com.isa.thinair.airadmin.core.web.v2.action.pricing;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.api.FareInfoDTO;
import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airadmin.core.web.v2.util.ErrorMessageUtil;
import com.isa.thinair.airadmin.core.web.v2.util.FareUtil;
import com.isa.thinair.airpricing.api.criteria.FareSearchCriteria;
import com.isa.thinair.airpricing.api.criteria.ONDChargeSearchCriteria;
import com.isa.thinair.airpricing.api.dto.ManageFareDTO;
import com.isa.thinair.airpricing.api.dto.ondcharge.OriginDestinationTO;
import com.isa.thinair.airpricing.api.service.ChargeBD;
import com.isa.thinair.airpricing.api.service.FareBD;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * 
 * @author Jagath Kumara
 * 
 */
@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class FareSearchAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(FareRuleSearchAction.class);

	private boolean success = true;

	private String messageTxt;

	private String status;

	private String originAirport;

	private String destinationAirport;

	private String via1;

	private String via2;

	private String via3;

	private String via4;

	private String bookingClassCode;

	private String fareBasisCode;

	private String showAllOND;

	private String effectiveDuring;

	private String cos;

	private String fromDate;

	private String toDate;

	private String fareRuleCode;

	private int page = 1;

	private int total;

	private int records;

	private Collection rows;

	private FareInfoDTO fare;

	private Map errorInfo;

	private String posStation;

	private String sord;

	private String sidx;

	@SuppressWarnings("unused")
	public String execute() {
		String forward = S2Constants.Result.SUCCESS;

		try {
			Page pageInt = null;
			Collection<ManageFareDTO> colFares;

			int totRec;
			List<String> viaList = new ArrayList<String>();

			FareSearchCriteria fareSearch = new FareSearchCriteria();
			FareBD fareBD = ModuleServiceLocator.getFareBD();

			if (originAirport != null && !originAirport.equals("") && !originAirport.equals("Select")) {
				fareSearch.setOriginAirportCode(originAirport);
			}

			if (destinationAirport != null && !destinationAirport.equals("") && !destinationAirport.equals("Select")) {
				fareSearch.setDestinationAirportCode(destinationAirport);
			}

			if (via1 != null && !via1.equals("") && !via1.equals("Select")) {
				viaList.add(via1);
			}
			if (via2 != null && !via2.equals("") && !via2.equals("Select")) {
				viaList.add(via2);
			}
			if (via3 != null && !via3.equals("") && !via3.equals("Select")) {
				viaList.add(via3);
			}
			if (via4 != null && !via4.equals("") && !via4.equals("Select")) {
				viaList.add(via4);
			}
			if ((via1 != null && !via1.equals("") && !via1.equals("Select"))
					|| (via2 != null && !via2.equals("") && !via2.equals("Select"))
					|| (via3 != null && !via3.equals("") && !via3.equals("Select"))
					|| (via4 != null && !via4.equals("") && !via4.equals("Select"))) {

				fareSearch.setViaAirportCodes(viaList);
			}

			if (showAllOND != null) {
				if (showAllOND.equals("AllComb")) {
					fareSearch.setShowAllONDCombinations(true);
					fareSearch.setExactCombination(false);
				} else if (showAllOND.equals("selectedComb")) {
					fareSearch.setExactCombination(true);
					fareSearch.setShowAllONDCombinations(false);
				}
			}
			SimpleDateFormat dateForamt = new SimpleDateFormat("dd/MM/yyyy");
			if (effectiveDuring != null) {
				if (effectiveDuring.equals("From")) {
					if (fromDate != null && !fromDate.equals("")) {
						String dateArr[] = fromDate.split("/");
						GregorianCalendar cal = new GregorianCalendar(Integer.parseInt(dateArr[2]),
								Integer.parseInt(dateArr[1]) - 1, Integer.parseInt(dateArr[0]));
						fareSearch.setFromDate(cal.getTime());

					} else {
						fromDate = dateForamt.format(new Date()).toString();
						String dateArr[] = fromDate.split("/");
						GregorianCalendar cal = new GregorianCalendar(Integer.parseInt(dateArr[2]),
								Integer.parseInt(dateArr[1]) - 1, Integer.parseInt(dateArr[0]));
						fareSearch.setFromDate(cal.getTime());
					}

					if (toDate != null && !toDate.equals("")) {
						String dateArr[] = toDate.split("/");
						GregorianCalendar cal = new GregorianCalendar(Integer.parseInt(dateArr[2]),
								Integer.parseInt(dateArr[1]) - 1, Integer.parseInt(dateArr[0]));
						fareSearch.setToDate(cal.getTime());

					} else {
						fareSearch.setToDate(null);
					}

				} else {

					fromDate = dateForamt.format(new Date()).toString();
					String dateArr[] = fromDate.split("/");
					GregorianCalendar cal = new GregorianCalendar(Integer.parseInt(dateArr[2]), Integer.parseInt(dateArr[1]) - 1,
							Integer.parseInt(dateArr[0]));
					fareSearch.setFromDate(cal.getTime());
				}
			}

			if (status != null && !status.equals("All")) {
				fareSearch.setStatus(status);
			}

			if (fareBasisCode != null && !fareBasisCode.equals("") && !fareBasisCode.equals("Select")) {
				fareSearch.setFareBasisCode(fareBasisCode);
			}

			if (cos != null && !cos.equals("") && !cos.equals("Select")) {
				if (cos.indexOf("_") > -1) {
					fareSearch.setLogicalCabinClass(cos.substring(0, cos.indexOf("_")));
				} else {
					fareSearch.setCabinClassCode(cos);
				}
			}
			if (bookingClassCode != null && !bookingClassCode.equals("")) {
				fareSearch.setBookingClassCode(bookingClassCode);
			}
			if (fareRuleCode != null && !fareRuleCode.equals("")) {
				fareSearch.setFareRuleCode(fareRuleCode);
			}

			if (sidx != null && !sidx.equals("") && sord != null && !sord.equals("")) {
				if (FareUtil.getSortingFields().get(sidx) != null) {
					fareSearch.setSortingField(FareUtil.getSortingFields().get(sidx));
					fareSearch.setSortingOder(sord);
				}
			}

			pageInt = fareBD.getFares(fareSearch, (page - 1) * 10, 10, null);
			colFares = pageInt.getPageData();
			records = pageInt.getTotalNoOfRecords();
			int totalCount = pageInt.getTotalNoOfRecords() / 10;
			int mod = pageInt.getTotalNoOfRecords() % 10;
			if (mod > 0)
				totalCount = totalCount + 1;
			total = totalCount;
			rows = createFareGridData(colFares);
		} catch (Exception ex) {
			log.error(ex);
			success = false;
			messageTxt = ex.getMessage();
		}
		return forward;
	}

	public String createPageInitailData() {
		String forward = S2Constants.Result.SUCCESS;
		try {
			if (fare == null) {
				fare = new FareInfoDTO();
			}
			fare.setSystemDate(FareUtil.getSystemDate());
			fare.setViaPointList(SelectListGenerator.createAirportComboList());
			fare.setCosList(SelectListGenerator.createCabinClassAndLogicalCabinClasses());
			fare.setFareBasisCodeList(SelectListGenerator.createBasisCodeListMap());
			fare.setPosList(SelectListGenerator.createStationCodes());
			fare.setChildVisible(FareUtil.isChildVisible());
			String defaultClassOfService = AppSysParamsUtil.getDefaultSelectedCOSInAiradminSetupFareScreen();
			if (defaultClassOfService.trim() != null && !defaultClassOfService.trim().equals("")) {
				String defalutCOSSeleted = defaultClassOfService + "_" + defaultClassOfService;
				fare.setDefaultSelectedCos(defalutCOSSeleted);
			}

			errorInfo = ErrorMessageUtil.getFareErrors(AiradminConfig.defaultLanguage);
		} catch (ModuleException ex) {
			log.error(ex);
			success = false;
			messageTxt = ex.getMessageString();
		} catch (Exception ex) {
			log.error(ex);
			success = false;
			messageTxt = ex.getMessage();
		}
		return forward;
	}

	public String searchOndChargesData() {
		String forward = S2Constants.Result.SUCCESS;
		try {
			Page page = null;

			ChargeBD chargeBD = ModuleServiceLocator.getChargeBD();
			ONDChargeSearchCriteria ondSearch = new ONDChargeSearchCriteria();

			List<String> viaList = new ArrayList<String>();
			if (originAirport != null && !originAirport.equals("") && !originAirport.equals("Select")) {
				ondSearch.setOriginAirportCode(originAirport);
			}

			if (destinationAirport != null && !destinationAirport.equals("") && !destinationAirport.equals("Select")) {
				ondSearch.setDestinationAirportCode(destinationAirport);
			}

			if (via1 != null && !via1.equals("") && !via1.equals("Select")) {
				viaList.add(via1);
			}
			if (via2 != null && !via2.equals("") && !via2.equals("Select")) {
				viaList.add(via2);
			}
			if (via3 != null && !via3.equals("") && !via3.equals("Select")) {
				viaList.add(via3);
			}
			if (via4 != null && !via4.equals("") && !via4.equals("Select")) {
				viaList.add(via4);
			}
			if ((via1 != null && !via1.equals("") && !via1.equals("Select"))
					|| (via2 != null && !via2.equals("") && !via2.equals("Select"))
					|| (via3 != null && !via3.equals("") && !via3.equals("Select"))
					|| (via4 != null && !via4.equals("") && !via4.equals("Select"))) {

				ondSearch.setViaAirportCodes(viaList);
			}
			ondSearch.setPosStation(posStation);

			SimpleDateFormat dateForamt = new SimpleDateFormat("dd/MM/yyyy");
			if (effectiveDuring != null) {
				if (effectiveDuring.equals("From")) {
					if (fromDate != null && !fromDate.equals("")) {
						String dateArr[] = fromDate.split("/");
						GregorianCalendar cal = new GregorianCalendar(Integer.parseInt(dateArr[2]),
								Integer.parseInt(dateArr[1]) - 1, Integer.parseInt(dateArr[0]));
						ondSearch.setEffectiveFromDate(cal.getTime());

					} else {
						fromDate = dateForamt.format(new Date()).toString();
						String dateArr[] = fromDate.split("/");
						GregorianCalendar cal = new GregorianCalendar(Integer.parseInt(dateArr[2]),
								Integer.parseInt(dateArr[1]) - 1, Integer.parseInt(dateArr[0]));
						ondSearch.setEffectiveFromDate(cal.getTime());
					}

					if (toDate != null && !toDate.equals("")) {
						String dateArr[] = toDate.split("/");
						GregorianCalendar cal = new GregorianCalendar(Integer.parseInt(dateArr[2]),
								Integer.parseInt(dateArr[1]) - 1, Integer.parseInt(dateArr[0]));
						ondSearch.setEffectiveToDate(cal.getTime());

					} else {
						ondSearch.setEffectiveToDate(null);
					}

				} else {

					fromDate = dateForamt.format(new Date()).toString();
					String dateArr[] = fromDate.split("/");
					GregorianCalendar cal = new GregorianCalendar(Integer.parseInt(dateArr[2]), Integer.parseInt(dateArr[1]) - 1,
							Integer.parseInt(dateArr[0]));
					ondSearch.setEffectiveFromDate(cal.getTime());
				}
			}

			List<String> order = new ArrayList<String>();
			order.add("ond.ONDCode");

			Collection<OriginDestinationTO> colFareCharges = null;
			page = chargeBD.getONDCharges(ondSearch, 1, 100, order);
			colFareCharges = page.getPageData();
			rows = createFareChargesGridData(colFareCharges);

		} catch (ModuleException ex) {
			log.error(ex);
			success = false;
			messageTxt = ex.getMessageString();
		} catch (Exception ex) {
			log.error(ex);
			success = false;
			messageTxt = ex.getMessage();
		}
		return forward;
	}

	@SuppressWarnings("unchecked")
	private Collection createFareGridData(Collection<ManageFareDTO> colFares) throws ModuleException {
		List fares = new ArrayList();
		Map fareMap = null;
		int id = page * 10 - 10;
		if (colFares != null) {
			SimpleDateFormat dateForamt = new SimpleDateFormat("HH:mm");
			for (ManageFareDTO fareDTO : colFares) {
				fareMap = new HashMap();
				fareMap.put("id", ++id);
				FareUtil.addFareData(fareDTO, fareMap, dateForamt);
				fares.add(fareMap);
			}
		}
		return fares;
	}

	@SuppressWarnings("unchecked")
	private Collection createFareChargesGridData(Collection<OriginDestinationTO> colFareCharges) throws ModuleException {
		List fareCharges = new ArrayList();
		Map fareChargeMap = null;
		if (colFareCharges != null) {
			SimpleDateFormat dateForamt = new SimpleDateFormat("HH:mm");
			for (OriginDestinationTO originDestinationTO : colFareCharges) {
				fareChargeMap = new HashMap();
				FareUtil.addFareChargeData(originDestinationTO, fareChargeMap, dateForamt);
				fareCharges.add(fareChargeMap);
			}
		}
		return fareCharges;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public Collection getRows() {
		return rows;
	}

	public void setRows(Collection rows) {
		this.rows = rows;
	}

	public Map getErrorInfo() {
		return errorInfo;
	}

	public void setErrorInfo(Map errorInfo) {
		this.errorInfo = errorInfo;
	}

	public FareInfoDTO getFare() {
		return fare;
	}

	public void setFare(FareInfoDTO fare) {
		this.fare = fare;
	}

	public String getOriginAirport() {
		return originAirport;
	}

	public void setOriginAirport(String originAirport) {
		this.originAirport = originAirport;
	}

	public String getDestinationAirport() {
		return destinationAirport;
	}

	public void setDestinationAirport(String destinationAirport) {
		this.destinationAirport = destinationAirport;
	}

	public String getVia1() {
		return via1;
	}

	public void setVia1(String via1) {
		this.via1 = via1;
	}

	public String getVia2() {
		return via2;
	}

	public void setVia2(String via2) {
		this.via2 = via2;
	}

	public String getVia3() {
		return via3;
	}

	public void setVia3(String via3) {
		this.via3 = via3;
	}

	public String getVia4() {
		return via4;
	}

	public void setVia4(String via4) {
		this.via4 = via4;
	}

	public String getBookingClassCode() {
		return bookingClassCode;
	}

	public void setBookingClassCode(String bookingClassCode) {
		this.bookingClassCode = bookingClassCode;
	}

	public String getFareBasisCode() {
		return fareBasisCode;
	}

	public void setFareBasisCode(String fareBasisCode) {
		this.fareBasisCode = fareBasisCode;
	}

	public String getShowAllOND() {
		return showAllOND;
	}

	public void setShowAllOND(String showAllOND) {
		this.showAllOND = showAllOND;
	}

	public String getEffectiveDuring() {
		return effectiveDuring;
	}

	public void setEffectiveDuring(String effectiveDuring) {
		this.effectiveDuring = effectiveDuring;
	}

	public String getCos() {
		return cos;
	}

	public void setCos(String cos) {
		this.cos = cos;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getFareRuleCode() {
		return fareRuleCode;
	}

	public void setFareRuleCode(String fareRuleCode) {
		this.fareRuleCode = fareRuleCode;
	}

	public String getPosStation() {
		return posStation;
	}

	public void setPosStation(String posStation) {
		this.posStation = posStation;
	}

	public String getSord() {
		return sord;
	}

	public void setSord(String sord) {
		this.sord = sord;
	}

	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

}
