package com.isa.thinair.airadmin.core.web.v2.action.fla;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.reporting.api.model.FlaAlertDTO;

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class SearchAlertsAction {
	private static Log log = LogFactory.getLog(SearchAlertsAction.class);
	private String flightID;
	private boolean success = true;
	private String message = "";
	List<FlaAlertDTO> lstAlertDTO;

	public String execute() {

		List<Integer> lstFlightIDs = new ArrayList<Integer>();
		lstFlightIDs.add(new Integer(flightID));
		try {
			lstAlertDTO = ModuleServiceLocator.getFLADataProviderBD().getFlaAlerts(lstFlightIDs);
		} catch (ModuleException e) {
			this.setSuccess(false);
			this.setMessage("TODO error message");
		}
		String strForward = WebConstants.FORWARD_SUCCESS;
		return strForward;
	}

	public String getFlightID() {
		return flightID;
	}

	public void setFlightID(String flightID) {
		this.flightID = flightID;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessage() {
		return message;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<FlaAlertDTO> getLstAlertDTO() {
		return lstAlertDTO;
	}

	public void setLstAlertDTO(List<FlaAlertDTO> lstAlertDTO) {
		this.lstAlertDTO = lstAlertDTO;
	}

}
