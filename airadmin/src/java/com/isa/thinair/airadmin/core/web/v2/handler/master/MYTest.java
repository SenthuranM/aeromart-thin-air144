package com.isa.thinair.airadmin.core.web.v2.handler.master;

import java.io.IOException;
import java.net.URLDecoder;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HostConfiguration;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class MYTest {

	private static Log log = LogFactory.getLog(MYTest.class);

	// private static String url =
	// "http://www.alayoubi-sms.com/sendsms/sendsms.asp?username=AIR ARABIA&password=12345678&mno=971503601554&msg=Test SMS Solution via client &Sid=alayoubi&fl=0&mt=0";

	// private static String url = "http://www.alayoubi-sms.com/sendsms/checklogin.asp?user=AIR%20ARABIA&pass=12345678";
	static String msg = "Test SMS Solution via client ";
	private static String url = "http://www.alayoubi-sms.com/sendsms/sendsms.asp?username=AIR%20ARABIA&password=12345678&mno=971503601554&msg="
			+ URLDecoder.decode(msg) + "&Sid=AIR ARABIA&fl=0&mt=0&ipcl=10.200.2.178";

	public static void main(String[] args) {
		HttpClient client = new HttpClient();

		HostConfiguration hcon = new HostConfiguration();
		hcon.setProxy("10.200.2.11", 8080);
		client.setHostConfiguration(hcon);

		// Create a method instance.
		GetMethod method = new GetMethod(url);
		// Provide custom retry handler is necessary
		method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(0, false));
		// method.getParams().setParameter(HttpMethodParams.REJECT_HEAD_BODY, true);

		try {
			// Execute the method.
			int statusCode = client.executeMethod(method);

			if (statusCode != HttpStatus.SC_OK) {
				System.err.println("Method failed: " + method.getStatusLine());
			}

			// Read the response body.
			byte[] responseBody = method.getResponseBody();

			// Deal with the response.
			// Use caution: ensure correct character encoding and is not binary data
			System.out.println(new String(responseBody));

		} catch (HttpException e) {
			log.error("Fatal protocol violation: " + e.getMessage());
		} catch (IOException e) {
			log.error("Fatal transport error: " + e.getMessage());
		} finally {
			// Release the connection.
			method.releaseConnection();
		}

	}
}
