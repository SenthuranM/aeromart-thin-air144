package com.isa.thinair.airadmin.core.web.v2.generator.promotion;

import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.commons.api.exception.ModuleException;

public class PromotionMessageHG {

	private static String templateclientErrors;

	public static String getTemplateClientErrors(HttpServletRequest request) throws ModuleException {

		if (templateclientErrors == null) {
			Properties moduleErrs = new Properties();

			moduleErrs.setProperty("um.charges.arrivalDepature.same", "depatureArriavlSame");
			moduleErrs.setProperty("um.charges.via.required", "viaPointRqrd");
			moduleErrs.setProperty("um.charges.arrivalVia.same", "arriavlViaSame");
			moduleErrs.setProperty("um.charges.depatureVia.same", "depatureViaSame");
			moduleErrs.setProperty("um.charges.via.sequence", "vianotinSequence");
			moduleErrs.setProperty("um.charges.via.equal", "viaEqual");
			moduleErrs.setProperty("um.promotion.template.incorrect.origin.dest", "incorrectAllOriginDest");

			moduleErrs.setProperty("um.promotion.template.promotion.type.required", "promotionTypeRequired");
			moduleErrs.setProperty("um.promotion.template.promotion.type.startdate", "startDateRequired");
			moduleErrs.setProperty("um.promotion.template.promotion.type.enddate", "endDateRequired");
			moduleErrs.setProperty("um.promotion.template.promotion.type.applicableroutes", "applicableRouteRequired");
			moduleErrs.setProperty("um.promotion.template.promotion.type.rewards", "chargesrewardsRequired");
			moduleErrs.setProperty("um.promotion.template.promotion.type.status", "statusRequired");
			moduleErrs.setProperty("um.promotion.template.promotion.type.minseats", "minseatsRequired");
			moduleErrs.setProperty("um.promotion.template.promotion.type.maxseats", "maxseatsRequired");
			moduleErrs.setProperty("um.promotion.template.promotion.type.minvacantseats", "minvacantseatsRequired");
			moduleErrs.setProperty("um.promotion.template.promotion.type.preservseats", "preservRequired");
			moduleErrs.setProperty("um.promotion.template.promotion.type.lowerboundry", "lowerboundryRequired");
			moduleErrs.setProperty("um.promotion.template.promotion.type.upperboundry", "upperboundryRequired");

			moduleErrs.setProperty("um.promotion.template.promotion.type.minseats.isnumber", "minseatsIsNumber");
			moduleErrs.setProperty("um.promotion.template.promotion.type.maxseats.isnumber", "maxseatsIsNumber");
			moduleErrs.setProperty("um.promotion.template.promotion.type.minvacantseats.isnumber", "minvacantseatsIsNumber");
			moduleErrs.setProperty("um.promotion.template.promotion.type.preserve.isnumber", "preserveIsNumber");
			moduleErrs.setProperty("um.promotion.template.promotion.type.lowerboundry.isnumber", "lowerboundryIsNumber");
			moduleErrs.setProperty("um.promotion.template.promotion.type.upperboundry.isnumber", "upperboundryIsNumber");
			moduleErrs.setProperty("um.promotion.template.promotion.type.chargesrewards.isnumber", "chargesrewardsIsNumber");
			moduleErrs.setProperty("um.promotion.template.promotion.type.negative.number", "negativeNumber");
			moduleErrs.setProperty("um.promotion.template.promotion.date.lower", "lowerCurrentDate");
			
			// For voucher template management
			moduleErrs.setProperty("um.promotion.voucher.template.name.required", "voucherNameRequired");
			moduleErrs.setProperty("um.promotion.voucher.template.amount.required", "amountRequired");
			moduleErrs.setProperty("um.promotion.voucher.template.startdate.required", "startDateRequired");
			moduleErrs.setProperty("um.promotion.voucher.template.enddate.required", "endDateRequired");
			moduleErrs.setProperty("um.promotion.voucher.template.date.lower", "lowEndDate");
			moduleErrs.setProperty("um.promotion.voucher.template.date.lowercurrent", "lowerCurrentDate");
			moduleErrs.setProperty("um.promotion.voucher.template.negative.amount", "negativeNumber");
			moduleErrs.setProperty("um.promotion.voucher.template.isnumber.amount", "amounIsNumber");
			moduleErrs.setProperty("um.promotion.voucher.template.no.record", "selectRecord");
			moduleErrs.setProperty("um.promotion.voucher.template.name.invalid", "voucherNameInvalid");
			
			moduleErrs.setProperty("um.airportdst.form.startdate.required", "startIsRequired");
			moduleErrs.setProperty("um.airportdst.form.enddate.required", "endIsRequired");
			moduleErrs.setProperty("um.flight.fltDate.invalidDate", "invalidDate");
			moduleErrs.setProperty("um.flight.stopdate.greater", "stopdategreat");
			moduleErrs.setProperty("um.searchinventory.form.fromdatelessthancurrentdate.voilated", "toCannotLessThanFromDate");

			moduleErrs.setProperty("um.bookingcode.form.id.required", "bookingClassRequired");
			moduleErrs.setProperty("um.charges.chargecode.required", "chargeCodeRequired");
			moduleErrs.setProperty("um.ondCode.form.ondCode.required", "ondCodeRequired");

			moduleErrs.setProperty("um.promotion.template.promotion.enddate.greater", "enddategreat");
			moduleErrs.setProperty("um.promotion.template.promotion.name.required", "promotionNameRequired");
			moduleErrs.setProperty("um.promotion.template.promotion.code.required", "promotionCodeRequired");
			moduleErrs.setProperty("um.promotion.template.promotion.Date.required", "promotionDateRequired");
			moduleErrs.setProperty("um.promotion.template.promotion.codes.required", "noPromoCodesGenerated");

			// to do remove unnessary properties -Gayan
			moduleErrs.setProperty("um.airadmin.delete.confirmation", "deleteRecoredCfrm");

			moduleErrs.setProperty("um.ssr.form.code.required", "ssrCodeReq"); // SSR Code required
			moduleErrs.setProperty("um.ssr.form.desc.required", "ssrDescReq"); // SSR Description required
			moduleErrs.setProperty("um.ssr.form.category.required", "ssrCatReq"); // SSR Category is Required
			moduleErrs.setProperty("um.ssr.form.id.already.exist", "ssrCodeExst"); // SSR Code already exists
			moduleErrs.setProperty("um.ssr.form.code.four.char.required", "ssrLenErr");// SSR Code should have 4
																						// characters
			moduleErrs.setProperty("um.ssr.form.code.cat.service.required", "servCatReq");// Select a Service Category
			moduleErrs.setProperty("um.ssr.form.code.subcat.service.required", "servSubCatReq"); // Select a Service
			moduleErrs.setProperty("um.ssr.form.code.airport.list.empty", "airportListReq"); // Applicable Airport List
																								// is Empty
			moduleErrs.setProperty("um.ssr.form.code.ond.list.empty", "ondListReq");// Applicable OnD list is empty
			moduleErrs.setProperty("um.ssr.form.code.trans.max.invalid", "maxTransTimeErr");// Transit Max time is
																							// invalid
			moduleErrs.setProperty("um.ssr.form.code.trans.min.invalid", "minTransTimeErr");// Transit Min time is
																							// invalid
			moduleErrs.setProperty("um.ssr.form.code.trans.time.invalid", "transTimeErr");// Transit Max time should
																							// greater than Min time
			moduleErrs.setProperty("um.ssr.form.name.required", "ssrNameReq");
			moduleErrs.setProperty("um.ssr.form.only.airport.service.enabled", "onlyHalaSerEnabled");

			moduleErrs.setProperty("um.ssr.form.code.depature.required", "depatureRqrd");
			moduleErrs.setProperty("um.ssr.form.code.arrival.required", "arrivalRqrd");
			moduleErrs.setProperty("um.ssr.form.code.arrivalDepature.same", "depatureArriavlSame");
			moduleErrs.setProperty("um.ssr.form.code.arrivalVia.same", "arriavlViaSame");
			moduleErrs.setProperty("um.ssr.form.code.depatureVia.same", "depatureViaSame");
			moduleErrs.setProperty("um.ssr.form.code.via.sequence", "vianotinSequence");
			moduleErrs.setProperty("um.ssr.form.code.via.equal", "viaEqual");
			moduleErrs.setProperty("um.ssr.form.code.departure.inactive", "departureInactive");
			moduleErrs.setProperty("um.ssr.form.code.arrival.inactive", "arrivalInactive");
			moduleErrs.setProperty("um.ssr.form.code.via.inactine", "viaInactive");
			moduleErrs.setProperty("um.ssr.form.code.ond.required", "ondRequired");

			moduleErrs.setProperty("um.ssr.form.code.max.qty.required", "maxQtyRequired");
			moduleErrs.setProperty("um.ssr.form.code.invalid.max.qty", "invalidMaxQty");

			moduleErrs.setProperty("um.ssr.form.name.required", "ssrNameReq");
			moduleErrs.setProperty("um.ssr.form.only.airport.service.enabled", "onlyHalaSerEnabled");

			// For the promo types
			moduleErrs.setProperty("um.promotion.template.promotion.Apply.required", "promotionApplyRequired");

			moduleErrs.setProperty("um.promotion.template.promotion.BankID.required", "bankIDRequired");
			moduleErrs.setProperty("um.promotion.template.promotion.ApplytoDiscount.required", "applyToDiscountRequired");
			moduleErrs.setProperty("um.promotion.template.promotion.ApplytoDiscountPromoType.required",
					"ApplytoDiscountPromoTypeRequired");
			moduleErrs.setProperty("um.promotion.template.promotion.DiscountValue.required", "discountValueRequired");
			moduleErrs.setProperty("um.promotion.template.promotion.ApplyDiscountType.required", "applyDiscountTypeRequired");

			moduleErrs.setProperty("um.promotion.template.promotion.Ancillaries.required", "ancillariesRequired");
			moduleErrs.setProperty("um.promotion.template.promotion.BuyNGetParam.required", "buyNGetParamRequired");
			moduleErrs.setProperty("um.promotion.template.promotion.LoadFactor.required", "loadFactorRequired");
			moduleErrs.setProperty("um.promotion.template.promotion.PromoCodeRequiredValue.required",
					"promoCodeRequiredValueRequired");
			moduleErrs.setProperty("um.promotion.template.promotion.PromoCodeUniqueValue.required",
					"promoCodeUniqueValueRequired");
			moduleErrs.setProperty("um.promotion.template.promotion.ChannelSelection.required", "promoCodeChlSelectionRequired");
			moduleErrs.setProperty("um.promotion.template.promotion.DiscountValueNonZero.required",
					"promoCodeDisValNonZeroRequired");

			moduleErrs.setProperty("um.promotion.bundled.fare.name.required", "bundledFareNameRequired");
			moduleErrs.setProperty("um.promotion.bundled.fare.free.service.required", "bundledFareFreeServiceRequired");
			moduleErrs.setProperty("um.promotion.bundled.fare.select.template", "bundledFareSelectTemplate");
			moduleErrs.setProperty("um.promotion.bundled.fare.fee.required", "bundledFeeRequired");
			moduleErrs.setProperty("um.promotion.bundled.fare.cutoff.time.invalid", "cutOffTimeInvalid");
			
			moduleErrs.setProperty("um.promotion.bundled.fare.select.at.least.one.service", "selectAtleastOne");
			moduleErrs.setProperty("um.promotion.bundled.fare.delink.confirmation", "delinkConfirmation");

			moduleErrs.setProperty("um.promotion.promocode.nocodes.tosend", "noCodesToSend");

			templateclientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}
		return templateclientErrors;
	}
}
