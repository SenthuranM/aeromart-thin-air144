package com.isa.thinair.airadmin.core.web.handler.reports;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author M.Rikaz
 * 
 */
public class NameChangeChargeReportRH extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(NameChangeChargeReportRH.class);

	private static final String NAME_CHANGE_CHARGE_REPORT = "NameChangeChargeReport.jasper";

	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();
	
	/**
	 * Main Execute Method for Name Change Report Action
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @param response
	 *            the HttpServletResponse
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {

		String forward = AdminStrutsConstants.AdminAction.SUCCESS;
		String strHdnMode = request.getParameter("hdnMode");

		try {
			setDisplayData(request);
			log.debug("NameChangeChargeReportRH setDisplayData() SUCCESS");
		} catch (Exception e) {
			log.error("NameChangeChargeReportRH setDisplayData() FAILED " + e.getMessage());
		}
		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				log.debug("NameChangeChargeReportRH setReportView Success");
				return null;
			} else {
				log.error("NameChangeChargeReportRH setReportView not selected");
			}
		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("NameChangeChargeReportRH setReportView Failed " + e.getMessageString());
		}
		return forward;
	}

	/**
	 * Sets the Client Validations to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setClientErrors(HttpServletRequest request) {

		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);

		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Display data For Name Change Report
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	private static void setDisplayData(HttpServletRequest request) throws Exception {
		setClientErrors(request);
		setLiveStatus(request);
		setUserIdList(request);
		setAirportComboList(request);
		setReportingPeriod(request);
		setReportTypeList(request);
		setAirportList(request);
		setAgentTypes(request);
		setGSAMultiSelectList(request);
		setLiveStatus(request);
		setReportingPeriod(request);
		setSalesChannel(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);

	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setAirportComboList(HttpServletRequest request) throws ModuleException {
		String strStation = SelectListGenerator.createActiveAirportCodeList();
		request.setAttribute(WebConstants.REQ_STATION_LIST, strStation);
	}

	/**
	 * Sets the user ids to the request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setUserIdList(HttpServletRequest request) throws ModuleException {

		String strUserIdList = SelectListGenerator.createUsersList();
		request.setAttribute(WebConstants.REQ_USER_ID_LIST, strUserIdList);
	}

	/**
	 * Sets the report medium to the request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleExceptions
	 */
	private static void setLiveStatus(HttpServletRequest request) throws ModuleException {
		String strLive = request.getParameter(WebConstants.REP_HDN_LIVE);
		if (strLive != null) {
			request.setAttribute(WebConstants.REP_SET_LIVE, strLive);
		}
	}

	/**
	 * Setting the reporting period to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setReportingPeriod(HttpServletRequest request) throws ModuleException {
		int period = new Integer(globalConfig.getBizParam(SystemParamKeys.REPORTING_PERIOD)).intValue();
		request.setAttribute(WebConstants.REP_RPT_START_DAY, ReportsHTMLGenerator.getReportingPeriod(period));
	}

	/**
	 * Displays the Name Change Report
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		ResultSet resultSet = null;
		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String value = request.getParameter("radReportOption");
		String bookedFromDate = request.getParameter("txtBookedFromDate");
		String bookedToDate = request.getParameter("txtBookedToDate");
		String strAgents = request.getParameter("hdnAgents");		
		String strSegments = request.getParameter("hdnSegments");
		String strReportType = request.getParameter("selReportType");
		String strReportFormat = request.getParameter("radRptNumFormat");
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);
		String strChannels = request.getParameter("hdnChannels");		
		
		String reportTemplate = null;
		String strLogo = AppSysParamsUtil.getReportLogo(false); 
		String strPath = "../../images/" + AppSysParamsUtil.getReportLogo(true); 
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String strBase = globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY);

		try {
			ReportsSearchCriteria search = new ReportsSearchCriteria();			
			
			search.setReportType(strReportType);
			if (fromDate != null && !fromDate.equals("")) {
				search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate) + " 00:00:00");
			}

			if (toDate != null && !toDate.equals("")) {
				search.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate) + " 23:59:59");
			}

			if ((bookedFromDate != null && !bookedFromDate.isEmpty()) && (bookedToDate != null && !bookedToDate.isEmpty())) {
				search.setDateRange2From(ReportsHTMLGenerator.convertDate(bookedFromDate) + " 00:00:00");
				search.setDateRange2To(ReportsHTMLGenerator.convertDate(bookedToDate) + " 23:59:59");
			}
			
			
			if (!StringUtil.isNullOrEmpty(strAgents)) {	
				ArrayList<String> agentCol = new ArrayList<String>();
				String agentArr[] = strAgents.split(",");
				for (int r = 0; r < agentArr.length; r++) {
					agentCol.add(agentArr[r]);
				}
				search.setAgents(agentCol);
			}
			
			
			
			if (!StringUtil.isNullOrEmpty(strSegments)) {
				String segmentsArr[] = strSegments.split(",");
				ArrayList<String> segmentsCol = new ArrayList<String>();
				for (String seg : segmentsArr) {
					segmentsCol.add(seg);
				}
				search.setSegmentCodes(segmentsCol);
			}

			if (strReportFormat != null && !strReportFormat.equals("")) {
				if (strReportFormat.equalsIgnoreCase("IT")) {
					search.setReqReportFormat("FR");
				} else
					search.setReqReportFormat("");
			}
			
			if (!StringUtil.isNullOrEmpty(strChannels)) {
				String[] arrChannels = strChannels.split(",");
				Collection<String> lstChannel = new HashSet<String>();
				for (String salesChannenl : arrChannels) {
					lstChannel.add(salesChannenl);
				}
				search.setSalesChannels(lstChannel);
			}
			

			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}			

			resultSet = ModuleServiceLocator.getDataExtractionBD().getNameChangeChargeReportData(search);

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("FROM_DATE", fromDate);
			parameters.put("TO_DATE", toDate);
//			parameters.put("USER_ID", userId);
			parameters.put("REPORT_TYPE", value);
			parameters.put("REPORT_CATE", strReportType);
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
			parameters.put("AMT_1", "(" + strBase + ")");
			parameters.put("ID", "UC_REPM_089");

			reportTemplate = NAME_CHANGE_CHARGE_REPORT;

			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs(WebConstants.REPORT_REF,
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));
			String reportTypeStr = "";
			if (ReportsSearchCriteria.NAME_CHANGE_REPORT_TYPE_AGENT.equalsIgnoreCase(strReportType)) {
				reportTypeStr = "ByAgent";
			}else if (ReportsSearchCriteria.NAME_CHANGE_REPORT_TYPE_CHANNEL.equalsIgnoreCase(strReportType)) {
				reportTypeStr = "ByChannel";
			}else if (ReportsSearchCriteria.NAME_CHANGE_REPORT_TYPE_SEGMENT.equalsIgnoreCase(strReportType)) {
				reportTypeStr = "BySegment";
			}
			
			String fileNameStr = "NameChangeCharges"+reportTypeStr;

			if (value.trim().equals(WebConstants.REPORT_HTML)) {
				parameters.put("IMG", strPath);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport(WebConstants.REPORT_REF, parameters, resultSet,
						null, null, response);

			} else if (value.trim().equals(WebConstants.REPORT_PDF)) {
				response.reset();
				response.addHeader("Content-Disposition", "filename="+fileNameStr+".pdf");
				strPath = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG", strPath);
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport(WebConstants.REPORT_REF, parameters, resultSet,
						response);
			} else if (value.trim().equals(WebConstants.REPORT_EXCEL)) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename="+fileNameStr+".xls");
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport(WebConstants.REPORT_REF, parameters, resultSet,
						response);
			} else if (value.trim().equals(WebConstants.REPORT_CSV)) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename="+fileNameStr+".csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);
			}
		} catch (Exception e) {
			log.error("setReportView :: ", e);
		}
	}
	
	private static void setReportTypeList(HttpServletRequest request) throws ModuleException {

		StringBuffer sb = new StringBuffer();

		/* Need to check if selReportType is already selected */

		String strReportType = request.getParameter("selReportType");

		if (ReportsSearchCriteria.NAME_CHANGE_REPORT_TYPE_AGENT.equalsIgnoreCase(strReportType)) {
			sb.append("<option value='NCCRPT_CHANNEL'>Charges By Channel</option>");
			sb.append("<option selected='selected' value='NCCRPT_AGENT'>Charges By Agent</option>");
			sb.append("<option value='NCCRPT_SEGMENT'>Charges By Segment</option>");
		} else if (ReportsSearchCriteria.NAME_CHANGE_REPORT_TYPE_CHANNEL.equalsIgnoreCase(strReportType)) {
			sb.append("<option selected='selected' value='NCCRPT_CHANNEL'>Charges By Channel</option>");
			sb.append("<option value='NCCRPT_AGENT'>Charges By Agent</option>");
			sb.append("<option value='NCCRPT_SEGMENT'>Charges By Segment</option>");
		} else if (ReportsSearchCriteria.NAME_CHANGE_REPORT_TYPE_SEGMENT.equalsIgnoreCase(strReportType)) {
			sb.append("<option value='NCCRPT_CHANNEL'>Charges By Channel</option>");
			sb.append("<option value='NCCRPT_AGENT'>Charges By Agent</option>");
			sb.append("<option selected='selected' value='NCCRPT_SEGMENT'>Charges By Segment</option>");
		} else {
			sb.append("<option value='NCCRPT_CHANNEL'>Charges By Channel</option>");
			sb.append("<option value='NCCRPT_AGENT'>Charges By Agent</option>");
			sb.append("<option value='NCCRPT_SEGMENT'>Charges By Segment</option>");
		}

		String strHtml = sb.toString();

		request.setAttribute("reqReportTypeList", strHtml);
	}
	
	private static void setAirportList(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createAirportCodeList();
		request.setAttribute(WebConstants.REQ_HTML_AIRPORT_LIST, strHtml);
	}

	protected static void setGSAMultiSelectList(HttpServletRequest request) throws ModuleException {
		boolean blnWithTAs = false;
		boolean blnWithCOs = false;
		String strAgentType = "";
		String strList = "";
		if (request.getParameter("selAgencies") != null) {
			strAgentType = request.getParameter("selAgencies");
		}
		if (request.getParameter("chkCOs") != null) {
			blnWithCOs = (request.getParameter("chkCOs").equals("on") ? true : false);
		}
		if (request.getParameter("chkTAs") != null) {
			blnWithTAs = (request.getParameter("chkTAs").equals("on") ? true : false);
		}
		strList = ReportsHTMLGenerator.createAgentGSAMultiSelect(strAgentType, blnWithTAs, blnWithCOs);
		request.setAttribute(WebConstants.REQ_HTML_GSA_LIST, strList);
	}

	protected static void setAgentTypes(HttpServletRequest request) throws ModuleException {
		String strATList = SelectListGenerator.createAgentTypeList_SG();
		request.setAttribute(WebConstants.REQ_HTML_AGENTTYPE_LIST, strATList);
	}
	
	private static void setSalesChannel(HttpServletRequest request) throws ModuleException {
		request.setAttribute(WebConstants.REQ_HTML_CHANNELS, ReportsHTMLGenerator.createSalesChannelHtml());
	}

}
