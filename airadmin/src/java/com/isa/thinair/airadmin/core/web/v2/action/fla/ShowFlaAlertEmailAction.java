package com.isa.thinair.airadmin.core.web.v2.action.fla;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.messaging.api.dto.SimpleEmailDTO;

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ShowFlaAlertEmailAction {

	private static Log log = LogFactory.getLog(ShowFlaAlertEmailAction.class);
	private String strHdnMode;
	private String toAddress;
	private String subject;
	private String content;
	boolean success = true;

	public String execute() {
		if (log.isDebugEnabled())
			log.debug("Begin FLA Email Alert:" + System.currentTimeMillis());

		String FROM_ADDRESS = "res@accelaero.com";
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;
		try {
			if (strHdnMode != null && strHdnMode.equals("EMAIL")) {
				if (toAddress != null && !"".equals(toAddress)) {
					SimpleEmailDTO emailDTO = new SimpleEmailDTO();
					emailDTO.setToEmailAddress(toAddress);
					emailDTO.setSubject(subject);
					emailDTO.setContent(content);
					emailDTO.setFromAddress(FROM_ADDRESS);
					ModuleServiceLocator.getMessagingBD().sendSimpleEmail(emailDTO);

					if (log.isDebugEnabled())
						log.debug("Email has succesfully sent:" + System.currentTimeMillis());
				}
			}
		} catch (Exception me) {
			success = false;
		}
		return forward;
	}

	public static Log getLog() {
		return log;
	}

	public String getStrHdnMode() {
		return strHdnMode;
	}

	public String getToAddress() {
		return toAddress;
	}

	public String getSubject() {
		return subject;
	}

	public String getContent() {
		return content;
	}

	public boolean isSuccess() {
		return success;
	}

	public static void setLog(Log log) {
		ShowFlaAlertEmailAction.log = log;
	}

	public void setStrHdnMode(String strHdnMode) {
		this.strHdnMode = strHdnMode;
	}

	public void setToAddress(String toAddress) {
		this.toAddress = toAddress;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

}
