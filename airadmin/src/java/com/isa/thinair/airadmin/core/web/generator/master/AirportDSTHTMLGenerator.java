package com.isa.thinair.airadmin.core.web.generator.master;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airmaster.api.model.AirportDST;
import com.isa.thinair.airmaster.api.model.SITAAddress;
import com.isa.thinair.airmaster.api.to.AirportDstTO;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author srikantha
 * 
 */
public class AirportDSTHTMLGenerator {

	private static Log log = LogFactory.getLog(AirportDSTHTMLGenerator.class);

	private static String clientErrors;

	// Time format when returns minutes with sign
	private static String formatTimeWithSign(int timeInMinutes) {
		String strSign = "-";
		if (timeInMinutes < 0)
			strSign = "-";
		else
			strSign = "+";

		int numerator = Math.abs(timeInMinutes);
		int denominator = 60;
		int quotient = numerator / denominator;
		int remainder = numerator % denominator;

		String strRemainder;
		if (remainder < 10)
			strRemainder = "0" + remainder;
		else
			strRemainder = "" + remainder;

		return strSign + quotient + ":" + strRemainder;

	}

	/**
	 * Gets the Airport DST GRID ROW for Selected Airport
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the DST ROW Data
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final String getAirportDSTRowHtml(HttpServletRequest request) throws ModuleException {

		log.debug("inside AirportDSTHTMLGenerator.getAirportDSTRowHtml()");
		String strAirportCode = request.getParameter("txtAirportCode");

		List<AirportDstTO> listArr = new ArrayList<AirportDstTO>();
		if (strAirportCode != null && !strAirportCode.equals(""))
			listArr = (List<AirportDstTO>) ModuleServiceLocator.getAirportServiceBD().getAirportDSTs(strAirportCode);

		return createAirportDSTRowHTML(listArr);
	}

	/**
	 * Creates the DST Grid from a Collection of DSTs
	 * 
	 * @param airportDsts
	 *            the Collection of DSTSs
	 * @return String the Created DST Grid
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private String createAirportDSTRowHTML(Collection<AirportDstTO> airportDsts) throws ModuleException {

		List<AirportDstTO> list = (List<AirportDstTO>) airportDsts;
		Object[] listArr = list.toArray();
		AirportDstTO airportDstTo = null;
		StringBuffer sb = new StringBuffer("var arrData = new Array();");

		for (int i = 0; i < listArr.length; i++) {
			airportDstTo = (AirportDstTO) listArr[i];

			AirportDST airportDst = airportDstTo.getAirportDST();
			sb.append("arrData[" + i + "] = new Array();");
			sb.append("arrData[" + i + "][1] = '" + changeDateFormat(airportDst.getDstStartDateTime()) + " "
					+ changeTimeFormat(airportDst.getDstStartDateTime()) + "';");
			sb.append("arrData[" + i + "][2] = '" + changeDateFormat(airportDst.getDstEndDateTime()) + " "
					+ changeTimeFormat(airportDst.getDstEndDateTime()) + "';");
			sb.append("arrData[" + i + "][3] = '" + formatTimeWithSign(airportDst.getDstAdjustTime()) + "';");
			if (airportDst.getModifiedDate() != null) {
				sb.append("arrData[" + i + "][4] = '" + changeDateFormat(airportDst.getModifiedDate()) + " "
						+ changeTimeFormat(airportDst.getModifiedDate()) + "';");
			} else {
				sb.append("arrData[" + i + "][4] = '';");
			}
			sb.append("arrData[" + i + "][5] = '" + airportDst.getVersion() + "';");

			if (airportDst.getDSTStatus() != null) {

				if (airportDst.getDSTStatus().equals(SITAAddress.ACTIVE_STATUS_YES))
					sb.append("arrData[" + i + "][6] = 'Active';");
				if (airportDst.getDSTStatus().equals(SITAAddress.ACTIVE_STATUS_NO))
					sb.append("arrData[" + i + "][6] = 'In-Active';");
			} else {
				sb.append("arrData[" + i + "][6] = '';");
			}
			sb.append("arrData[" + i + "][7] = '" + airportDst.getDstAdjustTime() + "';");

			String strFlightsExists = "";
			if (airportDstTo.getContainsFlights().toUpperCase().equals("YES"))
				strFlightsExists = "Yes";
			else
				strFlightsExists = "No";
			sb.append("arrData[" + i + "][8] = '" + strFlightsExists + "';");
			sb.append("arrData[" + i + "][9] = '" + airportDst.getDstCode() + "';");
		}
		return sb.toString();
	}

	/**
	 * Creates Client Validations for DST Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array of Client Validations
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {

		if (clientErrors == null) {
			Properties moduleErrs = new Properties();

			moduleErrs.setProperty("um.airportdst.form.startdate.required", "airportDstStartDateRqrd");
			moduleErrs.setProperty("um.airportdst.form.enddate.required", "airportDstEndDateRqrd");
			moduleErrs.setProperty("um.airportdst.form.starttime.required", "airportDstStartTimeRqrd");
			moduleErrs.setProperty("um.airportdst.form.endtime.required", "airportDstEndTimeRqrd");
			moduleErrs.setProperty("um.airportdst.form.time.required", "airportDstTimeRqrd");
			moduleErrs.setProperty("um.airportdst.form.startdate.format.invalid", "airportDstStartDateFormatInvalid");
			moduleErrs.setProperty("um.airportdst.form.enddate.format.invalid", "airportDstEndDateFormatInvalid");
			moduleErrs.setProperty("um.airportdst.form.starttime.invalid", "airportDstStartTimeInvalid");
			moduleErrs.setProperty("um.airportdst.form.endtime.invalid", "airportDstEndTimeInvalid");
			moduleErrs.setProperty("um.airportdst.form.time.invalid", "airportDstTimeInvalid");
			moduleErrs.setProperty("um.airportdst.form.email.required", "airportDstEmailIdRqrd");
			moduleErrs.setProperty("um.airportdst.form.enddate.lessthan.startdate", "airportDstEndLessthanStartDate");
			moduleErrs.setProperty("um.airportdst.form.startdate.lessthan.currentdate", "airportDstStartLessthanCurrentDate");
			moduleErrs.setProperty("um.airportdst.form.email.invalid", "airportDstEmailIdInvalid");
			moduleErrs.setProperty("um.airportdst.form.endtime.less.starttime.sameday", "airportDstEndTimelessForSameDay");
			moduleErrs.setProperty("um.airportdst.adj.time.invalid", "airportAdjTimeInvalid");
			clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}
		return clientErrors;
	}

	private String changeDateFormat(Date date) {
		SimpleDateFormat dtfmt = new SimpleDateFormat("dd/MM/yyyy");
		return dtfmt.format(date);
	}

	private String changeTimeFormat(Date date) {
		SimpleDateFormat dtfmt = new SimpleDateFormat("HH:mm");
		return dtfmt.format(date);
	}
}
