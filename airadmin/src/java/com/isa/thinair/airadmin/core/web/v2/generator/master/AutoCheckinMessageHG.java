package com.isa.thinair.airadmin.core.web.v2.generator.master;

import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Mohamed Asaruteen
 * 
 */
public class AutoCheckinMessageHG {

	private static String templateClientErrors;

	public static String getACITemplateClientErrors(HttpServletRequest request) throws ModuleException {
		if (templateClientErrors == null) {
			Properties moduleErrs = new Properties();
			moduleErrs.setProperty("um.autochecktemplate.form.chrgrp.required", "chargeGroup"); // Charge Group
																							// required
			moduleErrs.setProperty("um.autochecktemplate.form.amount.required", "baseCurrency"); // Base Currency
																							// required
			templateClientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}
		return templateClientErrors;
	}
}
