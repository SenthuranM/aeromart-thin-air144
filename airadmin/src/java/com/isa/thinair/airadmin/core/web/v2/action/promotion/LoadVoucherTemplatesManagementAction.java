package com.isa.thinair.airadmin.core.web.v2.action.promotion;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class LoadVoucherTemplatesManagementAction {

	private static Log log = LogFactory.getLog(LoadVoucherTemplatesManagementAction.class);

	// out
	private List<String[]> voucherCurrencyList;

	public String execute() {
		if (AppSysParamsUtil.isVoucherEnabled()) {
			try {
				voucherCurrencyList = SelectListGenerator.createVoucherCurrencyList();
			} catch (Exception e) {
				log.error("execute ", e);
			}
		}
		return S2Constants.Result.SUCCESS;
	}

	public List<String[]> getVoucherCurrencyList() {
		return voucherCurrencyList;
	}

}
