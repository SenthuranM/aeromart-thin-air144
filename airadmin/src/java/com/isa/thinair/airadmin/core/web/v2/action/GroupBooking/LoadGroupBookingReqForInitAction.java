package com.isa.thinair.airadmin.core.web.v2.action.GroupBooking;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;


@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class LoadGroupBookingReqForInitAction extends BaseRequestResponseAwareAction {

	private Map<String, String> airports;
	private String stations;
	private String agents;
	private String baseCurrency;
	private int farePeriod ;
	private int paymentPeriod;
	
	private Log log = LogFactory.getLog(LoadGroupBookingReqForInitAction.class);
	
	public String execute() {
		try {
			airports = SelectListGenerator.createActiveAirportCodeMap();
			baseCurrency=AppSysParamsUtil.getBaseCurrency();
			agents = SelectListGenerator.createAgentsNameList();
			stations = SelectListGenerator.createStationCodeList();
			farePeriod = AppSysParamsUtil.getFare_Valid_Till();
			paymentPeriod = AppSysParamsUtil.getTarget_Payment_Date();
		} catch (Exception e) {
			log.error("Error loading init data", e);
		}
		return S2Constants.Result.SUCCESS;

	}

	public String getAgents() {
		return agents;
	}

	public void setAgents(String agents) {
		this.agents = agents;
	}

	public String getStations() {
		return stations;
	}

	public void setStations(String stations) {
		this.stations = stations;
	}

	public Map<String, String> getAirports() {
		return airports;
	}

	public void setAirports(Map<String, String> airports) {
		this.airports = airports;
	}

	public String getBaseCurrency() {
		return baseCurrency;
	}

	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}

	public int getFarePeriod() {
		return farePeriod;
	}

	public void setFarePeriod(int farePeriod) {
		this.farePeriod = farePeriod;
	}

	public int getPaymentPeriod() {
		return paymentPeriod;
	}

	public void setPaymentPeriod(int paymentPeriod) {
		this.paymentPeriod = paymentPeriod;
	}

}
