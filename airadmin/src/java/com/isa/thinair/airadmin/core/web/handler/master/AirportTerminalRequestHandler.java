package com.isa.thinair.airadmin.core.web.handler.master;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.master.AirportTerminalHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airmaster.api.model.AirportTerminal;
import com.isa.thinair.commons.api.exception.ModuleException;

public final class AirportTerminalRequestHandler extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(AirportTerminalRequestHandler.class);

	private static AiradminConfig airadminConfig = new AiradminConfig();

	private static final String PARAM_VERSION = "hdnVersion";
	private static final String PARAM_GRIDROW = "hdnGridRow";
	private static final String PARAM_MODE = "hdnMode";
	private static final String PARAM_AIRPORTCODE = "txtAirportCode";
	private static final String PARAM_TERM_CODE = "txtTerminalCode";
	private static final String PARAM_TERM_NAME = "txtTerminalName";
	private static final String PARAM_TERM_DES = "txtTerminalDes";
	private static final String PARAM_TERM_DEFAULT = "chkDefault";
	private static final String PARAM_TERM_ACTIVE = "chkActive";

	private static final String PARAM_TERM_ID = "hdnTerminalId";

	// int 0-Not Applicable, 1-Success, 2-Fail

	private static void setIntSuccess(HttpServletRequest request, int value) {
		setAttribInRequest(request, "citIntSuccess", new Integer(value));
	}

	private static int getIntSuccess(HttpServletRequest request) {
		Integer returnValue = (Integer) getAttribInRequest(request, "citIntSuccess");
		if (returnValue != null)
			return returnValue.intValue();
		else
			return -1;
	}

	private static void setExceptionOccured(HttpServletRequest request, boolean value) {
		setAttribInRequest(request, "isExceptionOccuredCIT", new Boolean(value));
	}

	private static boolean isExceptionOccured(HttpServletRequest request) {
		return ((Boolean) getAttribInRequest(request, "isExceptionOccuredCIT")).booleanValue();
	}

	/**
	 * Execute Method for Terminal Action & Sets Succces Int int 0-Not Applicable, 1-Success, 2-Fail
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {

		String strHdnMode = request.getParameter(PARAM_MODE);
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;

		setIntSuccess(request, 0);
		String strFormData = "var saveSuccess = " + getIntSuccess(request) + ";";
		strFormData += " var arrFormData = new Array();";

		request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);

		setIntSuccess(request, 0);
		setExceptionOccured(request, false);
		setAttribInRequest(request, "strModeJSCIT", "var isAddMode = false;");

		try {
			if (strHdnMode != null) {
				if (strHdnMode.equals(WebConstants.ACTION_ADD)) {

					setAttribInRequest(request, "strModeJSCIT", "var isAddMode = true;");
					checkPrivilege(request, WebConstants.PRIV_SYS_MAS_AIRPORT_TERM_ADD);
					saveData(request);
				}
				if (strHdnMode.equals(WebConstants.ACTION_EDIT)) {

					setAttribInRequest(request, "strModeJSCIT", "var isAddMode = false;");
					checkPrivilege(request, WebConstants.PRIV_SYS_MAS_AIRPORT_TERM_EDIT);
					saveData(request);

				}
				if (strHdnMode.equals(WebConstants.ACTION_DELETE)) {

					setAttribInRequest(request, "strModeJSCIT", "var isAddMode = false;");
					checkPrivilege(request, WebConstants.PRIV_SYS_MAS_AIRPORT_TERM_DELETE);
					deleteData(request);

				}

			}
			request.setAttribute(WebConstants.REQ_OPERATION_MODE, getAttribInRequest(request, "strModeJSCIT"));

			log.debug("AirportCITRequestHandler.setDisplayData() method is successfully executed.");

		} catch (Exception exception) {
			log.error("Exception in AirportCITRequestHandler:execute()", exception);
			if (exception instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
			}
		}

		setDisplayData(request);
		return forward;
	}

	/**
	 * Saves the Terminal Data for an Airport
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return boolean true-success false -not
	 * @throws Exception
	 *             the Exception
	 */
	@SuppressWarnings("unused")
	private static boolean saveData(HttpServletRequest request) throws Exception {

		log.debug("Inside the AirportTerminalRequestHandler.saveData() method...");

		AirportTerminal airportTerm = new AirportTerminal();

		String strVersion = null;
		String strHdnMode = null;
		String strAirportCode = null;

		String strTerminalId = null;
		String strGridRow = null;
		String strTerminalCode = null;
		String strTerminalName = null;
		String strTerminalDes = null;
		String strTerminalDefault = null;
		String strTerminalActive = null;

		setExceptionOccured(request, false);

		try {
			strTerminalId = request.getParameter(PARAM_TERM_ID);
			strVersion = AiradminUtils.getNotNullString(request.getParameter(PARAM_VERSION));
			strGridRow = AiradminUtils.getNotNullString(request.getParameter(PARAM_GRIDROW));
			strHdnMode = AiradminUtils.getNotNullString(request.getParameter(PARAM_MODE));
			strAirportCode = AiradminUtils.getNotNullString(request.getParameter(PARAM_AIRPORTCODE));
			strTerminalCode = AiradminUtils.getNotNullString(request.getParameter(PARAM_TERM_CODE));
			strTerminalName = AiradminUtils.getNotNullString(request.getParameter(PARAM_TERM_NAME));
			strTerminalDes = AiradminUtils.getNotNullString(request.getParameter(PARAM_TERM_DES));
			strTerminalDefault = AiradminUtils.getNotNullString(request.getParameter(PARAM_TERM_DEFAULT));
			strTerminalActive = AiradminUtils.getNotNullString(request.getParameter(PARAM_TERM_ACTIVE));

			if (!"".equals(strVersion)) {
				airportTerm.setVersion(Long.parseLong(strVersion));
			}
			if (strHdnMode.equals(WebConstants.ACTION_EDIT)) {
				airportTerm.setTerminalId(Integer.parseInt(strTerminalId));
			}

			airportTerm.setAirportCode(strAirportCode);
			airportTerm.setTerminalCode(strTerminalCode);
			airportTerm.setTerminalShortName(strTerminalName);
			airportTerm.setTerminalDescription(strTerminalDes);

			AirportTerminal tmpObj = ModuleServiceLocator.getAirportServiceBD().getDefaultTerminal(strAirportCode);

			if (strTerminalDefault.equals(WebConstants.CHECKBOX_ON)) {
				airportTerm.setDefaultTerminal("Y");
			} else {
				if (null == tmpObj) {
					airportTerm.setDefaultTerminal("Y");
				} else {
					airportTerm.setDefaultTerminal("N");
				}
			}
			if (strTerminalActive.equals(WebConstants.CHECKBOX_ON)) {
				airportTerm.setStatus("ACT");
			} else {
				airportTerm.setStatus("INA");
			}

			log.debug("Inside the AirportTerminalRequestHandler.saveData()- setters are successfully called...");
			// add airport terminal
			ModuleServiceLocator.getAirportServiceBD().addNewTerminal(airportTerm);
			// if this is the default terminal update all flights related to that airport
			// (In case if schedules/Flights already added for the airport, then default terminal should be updated on
			// all flights already existing. )
			if ("Y".equals(airportTerm.getDefaultTerminal()) && "Y".equals(airportTerm.getStatus())) {
				AirportTerminal terminalObj = ModuleServiceLocator.getAirportServiceBD().getDefaultTerminal(strAirportCode);
				ModuleServiceLocator.getFlightServiceBD().updateFlightSegmentTerminals(strAirportCode,
						terminalObj.getTerminalId(), true, false);
			}
			// When a terminal linked to a schedule/flight INA, should set all the departure and arrival terminal ids in
			// t_flight_segment to null IF there are no other terminals.
			if (strHdnMode.equals(WebConstants.ACTION_EDIT) && "INA".equals(airportTerm.getStatus())) {
				Collection<AirportTerminal> col = (Collection<AirportTerminal>) ModuleServiceLocator.getAirportServiceBD()
						.getAirportTerminals(strAirportCode, true);

				if (col.isEmpty()) {
					ModuleServiceLocator.getFlightServiceBD().updateFlightSegmentTerminals(strAirportCode, null, false, false);
				}
				AirportTerminal terminalObj = ModuleServiceLocator.getAirportServiceBD().getDefaultTerminal(strAirportCode);
				if (!col.isEmpty() && null != terminalObj && null != terminalObj.getTerminalId()) {
					ModuleServiceLocator.getFlightServiceBD().updateFlightSegmentTerminals(strAirportCode,
							terminalObj.getTerminalId(), false, true);
				} else {
					String displayAlert = "displayAlert = 'true';";
					request.setAttribute(WebConstants.REQ_HTML_TERMINAL_DISPLAY_ALERT, displayAlert);
					// ModuleServiceLocator.getFlightServiceBD().updateFlightSegmentTerminals(strAirportCode, null,
					// false, false);
				}
			}

			saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS), WebConstants.MSG_SUCCESS);

			airportTerm = null;

		} catch (ModuleException moduleException) {

			postExceptionOccured(request);

			log.error("Exception in AirportTerminalRequestHandler:saveData() [origin module=" + moduleException.getModuleDesc()
					+ "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);

		} catch (Exception exception) {

			postExceptionOccured(request);
			log.error("Exception in AirportTerminalRequestHandler:saveData()", exception);

			if (exception instanceof RuntimeException) {
				throw exception;
			} else {
				saveMessage(request, exception.getMessage(), WebConstants.MSG_ERROR);
			}
		}
		return true;
	}

	/**
	 * Deletes the Terminal Data for an Airport
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return boolean true-success false -not
	 * @throws Exception
	 *             the Exception
	 */
	@SuppressWarnings("unused")
	private static boolean deleteData(HttpServletRequest request) throws Exception {

		log.debug("Inside the AirportTerminalRequestHandler.deleteData() method...");

		AirportTerminal airportTerm = new AirportTerminal();

		String strVersion = null;
		String strHdnMode = null;
		String strAirportCode = null;

		String strTerminalId = null;

		setExceptionOccured(request, false);

		try {
			strTerminalId = request.getParameter(PARAM_TERM_ID);
			strVersion = AiradminUtils.getNotNullString(request.getParameter(PARAM_VERSION));
			strHdnMode = AiradminUtils.getNotNullString(request.getParameter(PARAM_MODE));
			strAirportCode = AiradminUtils.getNotNullString(request.getParameter(PARAM_AIRPORTCODE));

			if (!"".equals(strVersion)) {
				airportTerm.setVersion(Long.parseLong(strVersion));
			}

			airportTerm.setTerminalId(Integer.parseInt(strTerminalId));
			airportTerm.setAirportCode(strAirportCode);

			log.debug("Inside the AirportTerminalRequestHandler.deleteData()- setters are successfully called...");

			ModuleServiceLocator.getAirportServiceBD().deleteAirportTerminal(airportTerm);

			saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_DELETE_SUCCESS), WebConstants.MSG_SUCCESS);

			airportTerm = null;

		} catch (ModuleException moduleException) {

			postExceptionOccured(request);

			log.error("Exception in AirportTerminalRequestHandler:deleteData() [origin module=" + moduleException.getModuleDesc()
					+ "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);

		} catch (Exception exception) {

			postExceptionOccured(request);
			log.error("Exception in AirportCITRequestHandler:deleteData()", exception);

			if (exception instanceof RuntimeException) {
				throw exception;
			} else {
				saveMessage(request, exception.getMessage(), WebConstants.MSG_ERROR);
			}
		}
		return true;
	}

	/**
	 * Set the Display Data For Terminal Page
	 * 
	 * @param request
	 *            the HttpServletRequests
	 */
	public static void setDisplayData(HttpServletRequest request) {
		setAirportTerminalRowHtml(request);
		setClientErrors(request);
		if (!isExceptionOccured(request)) {
			String strFormData = "var arrFormData = new Array();";
			strFormData += " var saveSuccess = " + getIntSuccess(request) + ";";
			strFormData += " var displayAlert = 'false';";

			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);

			request.setAttribute(WebConstants.REQ_OPERATION_MODE, getAttribInRequest(request, "strModeJSCIT"));
		}
	}

	/**
	 * Sets the Client Validations for Terminal Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setClientErrors(HttpServletRequest request) {

		try {

			String strClientErrors = AirportTerminalHTMLGenerator.getClientErrors(request);
			if (strClientErrors == null)
				strClientErrors = "";

			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);

		} catch (ModuleException moduleException) {
			log.error(
					"Exception in AirportTerminalRequestHandler:setClientErrors() [origin module="
							+ moduleException.getModuleDesc() + "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Terminal Row to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	protected static void setAirportTerminalRowHtml(HttpServletRequest request) {

		try {

			AirportTerminalHTMLGenerator htmlGen = new AirportTerminalHTMLGenerator();

			String strHtml = htmlGen.getAirportTerminalRowHtml(request);
			request.setAttribute(WebConstants.REQ_HTML_ROWS, strHtml);

		} catch (ModuleException moduleException) {

			log.error(
					"Exception in AirportTerminalRequestHandler:setAirportTerminalRowHtml() [origin module="
							+ moduleException.getModuleDesc() + "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	@SuppressWarnings("unused")
	private static void postExceptionOccured(HttpServletRequest request) {
		String strVersion = null;
		String strHdnMode = null;
		String strAirportCode = null;
		String strTerminalId = null;
		String strGridRow = null;
		String strTerminalCode = null;
		String strTerminalName = null;
		String strTerminalDes = null;
		String strTerminalDefault = null;
		String strTerminalActive = null;

		strTerminalId = request.getParameter(PARAM_TERM_ID);
		strVersion = AiradminUtils.getNotNullString(request.getParameter(PARAM_VERSION));
		strGridRow = AiradminUtils.getNotNullString(request.getParameter(PARAM_GRIDROW));
		strHdnMode = AiradminUtils.getNotNullString(request.getParameter(PARAM_MODE));
		strAirportCode = AiradminUtils.getNotNullString(request.getParameter(PARAM_AIRPORTCODE));
		strTerminalCode = AiradminUtils.getNotNullString(request.getParameter(PARAM_TERM_CODE));
		strTerminalName = AiradminUtils.getNotNullString(request.getParameter(PARAM_TERM_NAME));
		strTerminalDes = AiradminUtils.getNotNullString(request.getParameter(PARAM_TERM_DES));
		strTerminalDefault = AiradminUtils.getNotNullString(request.getParameter(PARAM_TERM_DEFAULT));
		strTerminalActive = AiradminUtils.getNotNullString(request.getParameter(PARAM_TERM_ACTIVE));

		setExceptionOccured(request, true);

		String strFormData = "var arrFormData = new Array();";
		strFormData += "arrFormData[0] = new Array();";
		strFormData += "arrFormData[0][1] = '" + strTerminalCode + "';";
		strFormData += "arrFormData[0][2] = '" + strTerminalName + "';";
		strFormData += "arrFormData[0][3] = '" + strTerminalDes + "';";
		strFormData += "arrFormData[0][4] = '" + strTerminalDefault + "';";
		strFormData += "arrFormData[0][5] = '" + strTerminalActive + "';";
		strFormData += "arrFormData[0][6] = '" + strVersion + "';";
		strFormData += "arrFormData[0][7] = '" + strTerminalId + "';";

		setIntSuccess(request, 2);
		strFormData += " var saveSuccess = " + getIntSuccess(request) + ";";

		if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_EDIT)) {

			strFormData += " var prevVersion = " + strVersion + ";";
			strFormData += " var prevTerminalId = " + strTerminalId + ";";
			strFormData += " var prevGridRow = " + strGridRow + ";";
		}
		request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
	}

}
