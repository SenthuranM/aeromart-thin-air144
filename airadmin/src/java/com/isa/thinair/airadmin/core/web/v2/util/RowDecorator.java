package com.isa.thinair.airadmin.core.web.v2.util;

import java.util.HashMap;

public interface RowDecorator<T> {

	public void decorateRow(HashMap<String, Object> row, T record);
}
