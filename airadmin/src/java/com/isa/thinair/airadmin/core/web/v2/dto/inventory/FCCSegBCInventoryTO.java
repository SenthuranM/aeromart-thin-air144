package com.isa.thinair.airadmin.core.web.v2.dto.inventory;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Map;



public class FCCSegBCInventoryTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer index;

	private Integer fccsbInvId;

	private String bookingCode;

	private String standardCode;
	
	private String standard;

	private String fixedFlag;

	private boolean priorityFlag;

	private Integer seatsAllocated;

	private Integer actualSeatsSold;

	private Integer actualSeatsOnHold;

	private Integer actualWaitListedSeats;

	private Integer allocatedWaitListSeats;

	private Integer seatsCancelled;

	private Integer seatsAcquired;

	private Integer seatsAvailable;

	private boolean statusClosed;

	private String fareLink;

	private String linkedEffectiveAgents;

	private String statusChangeAction;

	private long version;

	private String stausEdited = "N";

	private String enableFareLink;

	private Integer seatsSold;

	private Integer onHoldSeats;

	private Integer nestRank;

	private Integer seatsSoldAquiredByNesting = 0;

	private Integer seatsSoldNested = 0;

	private Integer seatsOnHoldAquiredByNesting = 0;

	private Integer seatsOnHoldNested = 0;
	
	private Integer oneWayPaxCount = 0;
	
	private Integer oneWayConnectionPaxCount = 0;
	
	private Map<String, Integer> oneWayConnectionSegments = Collections.emptyMap();
	
	private Integer returnPaxCount = 0;
	
	private Integer returnConnectionPaxCount = 0;
	
	private Map<String, Integer> returnConnectionSegments = Collections.emptyMap();

	private Integer gdsId;

	private String bcType;

	private Integer overBookCount;

	private Integer groupId;

	private boolean isExposedToGDS;
	
	private String allocationType;

	/*
	 * Should be one of following actions Added : A, Edited : E, Deleted : D
	 */
	private String action;

	private List<FareTO> fareTOs;

	private Integer seatsAvailableNested;

	public boolean isExposedToGDS() {
		return isExposedToGDS;
	}

	public void setExposedToGDS(boolean isExposedToGDS) {
		this.isExposedToGDS = isExposedToGDS;
	}

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	public Integer getFccsbInvId() {
		return fccsbInvId;
	}

	public void setFccsbInvId(Integer fccsbInvId) {
		this.fccsbInvId = fccsbInvId;
	}

	public String getBookingCode() {
		return bookingCode;
	}

	public void setBookingCode(String bookingCode) {
		this.bookingCode = bookingCode;
	}

	public String isStandardCode() {
		return standardCode;
	}

	public void setStandardCode(String standardCode) {
		this.standardCode = standardCode;
	}

	public String isFixedFlag() {
		return fixedFlag;
	}

	public void setFixedFlag(String fixedFlag) {
		this.fixedFlag = fixedFlag;
	}

	public boolean isPriorityFlag() {
		return priorityFlag;
	}

	public void setPriorityFlag(boolean priorityFlag) {
		this.priorityFlag = priorityFlag;
	}

	public Integer getSeatsAllocated() {
		return seatsAllocated;
	}

	public void setSeatsAllocated(Integer seatsAllocated) {
		this.seatsAllocated = seatsAllocated;
	}

	public Integer getActualSeatsSold() {
		return actualSeatsSold;
	}

	public void setActualSeatsSold(Integer actualSeatsSold) {
		this.actualSeatsSold = actualSeatsSold;
	}

	public Integer getActualSeatsOnHold() {
		return actualSeatsOnHold;
	}

	public void setActualSeatsOnHold(Integer actualSeatsOnHold) {
		this.actualSeatsOnHold = actualSeatsOnHold;
	}

	public Integer getSeatsCancelled() {
		return seatsCancelled;
	}

	public void setSeatsCancelled(Integer seatsCancelled) {
		this.seatsCancelled = seatsCancelled;
	}

	public Integer getSeatsAcquired() {
		return seatsAcquired;
	}

	public void setSeatsAcquired(Integer seatsAcquired) {
		this.seatsAcquired = seatsAcquired;
	}

	public Integer getSeatsAvailable() {
		return seatsAvailable;
	}

	public void setSeatsAvailable(Integer seatsAvailable) {
		this.seatsAvailable = seatsAvailable;
	}

	public boolean isStatusClosed() {
		return statusClosed;
	}

	public void setStatusClosed(boolean statusClosed) {
		this.statusClosed = statusClosed;
	}

	public String getFareLink() {
		return fareLink;
	}

	public void setFareLink(String fareLink) {
		this.fareLink = fareLink;
	}

	public String getLinkedEffectiveAgents() {
		return linkedEffectiveAgents;
	}

	public void setLinkedEffectiveAgents(String linkedEffectiveAgents) {
		this.linkedEffectiveAgents = linkedEffectiveAgents;
	}

	public String getStatusChangeAction() {
		return statusChangeAction;
	}

	public void setStatusChangeAction(String statusChangeAction) {
		this.statusChangeAction = statusChangeAction;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public String getStausEdited() {
		return stausEdited;
	}

	public void setStausEdited(String stausEdited) {
		this.stausEdited = stausEdited;
	}

	public String getEnableFareLink() {
		return enableFareLink;
	}

	public void setEnableFareLink(String enableFareLink) {
		this.enableFareLink = enableFareLink;
	}

	public Integer getSeatsSold() {
		return seatsSold;
	}

	public void setSeatsSold(Integer seatsSold) {
		this.seatsSold = seatsSold;
	}

	public Integer getOnHoldSeats() {
		return onHoldSeats;
	}

	public void setOnHoldSeats(Integer onHoldSeats) {
		this.onHoldSeats = onHoldSeats;
	}

	public Integer getNestRank() {
		return nestRank;
	}

	public void setNestRank(Integer nestRank) {
		this.nestRank = nestRank;
	}

	public Integer getSeatsSoldAquiredByNesting() {
		return seatsSoldAquiredByNesting;
	}

	public void setSeatsSoldAquiredByNesting(Integer seatsSoldAquiredByNesting) {
		this.seatsSoldAquiredByNesting = seatsSoldAquiredByNesting;
	}

	public Integer getSeatsSoldNested() {
		return seatsSoldNested;
	}

	public void setSeatsSoldNested(Integer seatsSoldNested) {
		this.seatsSoldNested = seatsSoldNested;
	}

	public Integer getSeatsOnHoldAquiredByNesting() {
		return seatsOnHoldAquiredByNesting;
	}

	public void setSeatsOnHoldAquiredByNesting(Integer seatsOnHoldAquiredByNesting) {
		this.seatsOnHoldAquiredByNesting = seatsOnHoldAquiredByNesting;
	}

	public Integer getSeatsOnHoldNested() {
		return seatsOnHoldNested;
	}

	public void setSeatsOnHoldNested(Integer seatsOnHoldNested) {
		this.seatsOnHoldNested = seatsOnHoldNested;
	}

	public Integer getOneWayPaxCount() {
		return oneWayPaxCount;
	}

	public void setOneWayPaxCount(Integer oneWayPaxCount) {
		this.oneWayPaxCount = oneWayPaxCount;
	}

	
	public Integer getOneWayConnectionPaxCount() {
		return oneWayConnectionPaxCount;
	}

	public void setOneWayConnectionPaxCount(Integer oneWayConnectionPaxCount) {
		this.oneWayConnectionPaxCount = oneWayConnectionPaxCount;
	}

	public Integer getReturnPaxCount() {
		return returnPaxCount;
	}

	public void setReturnPaxCount(Integer returnPaxCount) {
		this.returnPaxCount = returnPaxCount;
	}

	public Integer getReturnConnectionPaxCount() {
		return returnConnectionPaxCount;
	}

	public void setReturnConnectionPaxCount(Integer returnConnectionPaxCount) {
		this.returnConnectionPaxCount = returnConnectionPaxCount;
	}

	public Map<String, Integer> getOneWayConnectionSegments() {
		return oneWayConnectionSegments;
	}

	public void setOneWayConnectionSegments(final Map<String, Integer> oneWayConnectionSegments) {
		this.oneWayConnectionSegments = oneWayConnectionSegments;
	}

	public Map<String, Integer> getReturnConnectionSegments() {
		return returnConnectionSegments;
	}

	public void setReturnConnectionSegments(Map<String, Integer> returnConnectionSegments) {
		this.returnConnectionSegments = returnConnectionSegments;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Integer getGdsId() {
		return gdsId;
	}

	public void setGdsId(Integer gdsId) {
		this.gdsId = gdsId;
	}

	public String getBcType() {
		return bcType;
	}

	public void setBcType(String bcType) {
		this.bcType = bcType;
	}

	public Integer getOverBookCount() {
		return overBookCount;
	}

	public void setOverBookCount(Integer overBookCount) {
		this.overBookCount = overBookCount;
	}

	public List<FareTO> getFareTOs() {
		return fareTOs;
	}

	public void setFareTOs(List<FareTO> fareTOs) {
		this.fareTOs = fareTOs;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getStandardCode() {
		return standardCode;
	}

	public String getFixedFlag() {
		return fixedFlag;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public Integer getActualWaitListedSeats() {
		return actualWaitListedSeats;
	}

	public void setActualWaitListedSeats(Integer actualWaitListedSeats) {
		this.actualWaitListedSeats = actualWaitListedSeats;
	}

	public Integer getAllocatedWaitListSeats() {
		return allocatedWaitListSeats;
	}

	public void setAllocatedWaitListSeats(Integer allocatedWaitListSeats) {
		this.allocatedWaitListSeats = allocatedWaitListSeats;
	}

	public Integer getSeatsAvailableNested() {
		return seatsAvailableNested;
	}

	public void setSeatsAvailableNested(Integer seatsAvailableNested) {
		this.seatsAvailableNested = seatsAvailableNested;
	}

	public String getAllocationType() {
		return allocationType;
	}

	public void setAllocationType(String allocationType) {
		this.allocationType = allocationType;
	}

	public String getStandard() {
		return standard;
	}

	public void setStandard(String standardClass) {
		this.standard = standardClass;
	}

}
