package com.isa.thinair.airadmin.core.web.generator.inventory;

import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.commons.api.exception.ModuleException;

public class InventoryTemplateHTMLGenerator {

	private static Log log = LogFactory.getLog(InventoryTemplateHTMLGenerator.class);

	public static String getInventoryTemplatesClientErrors() throws ModuleException {
		String clientErrors = null;
		Properties moduleErrs = new Properties();
		moduleErrs.setProperty("um.inventorytemplate.segment.requierd", "segmentRqrd");
		clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		return clientErrors;
	}
}
