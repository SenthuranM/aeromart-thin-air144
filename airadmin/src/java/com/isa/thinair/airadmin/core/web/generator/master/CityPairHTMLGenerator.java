package com.isa.thinair.airadmin.core.web.generator.master;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airmaster.api.model.RouteInfo;
import com.isa.thinair.airmaster.api.model.RouteTransitsInfo;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

public class CityPairHTMLGenerator {

	private static String clientErrors;
	private static final String SEPERATOR = "/";

	/**
	 * Gets the ROUTEINFO grid data for the record no
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array containging the corresponding Grid Data
	 * @throws ModuleException
	 *             the ModuleException
	 */
	@SuppressWarnings("unchecked")
	public final String getCityPairRowHtml(HttpServletRequest request) throws ModuleException {
		Collection<RouteInfo> colCityPair = null;
		Page page = null;
		int recNo = 0;
		int totRec = 0;

		String strOrigin = "";
		String strDestination = "";
		String strVia1 = "";
		String strVia2 = "";
		String strVia3 = "";
		String strVia4 = "";

		strOrigin = request.getParameter("selOriginSearch");
		strVia1 = request.getParameter("selVia1Search");
		strVia2 = request.getParameter("selVia2Search");
		strVia3 = request.getParameter("selVia3Search");
		strVia4 = request.getParameter("selVia4Search");
		strDestination = request.getParameter("selDestinationSearch");

		if (request.getParameter("hdnRecNo") != null && !("".equals(request.getParameter("hdnRecNo")))) {
			recNo = Integer.parseInt(request.getParameter("hdnRecNo"));
			recNo = recNo - 1;
		}

		List<ModuleCriterion> critrian = new ArrayList<ModuleCriterion>();

		if (strOrigin != null && !(strOrigin.equals(""))) {
			ModuleCriterion moduleCriterionOrigin = new ModuleCriterion();
			moduleCriterionOrigin.setCondition(ModuleCriterion.CONDITION_EQUALS);
			moduleCriterionOrigin.setFieldName("fromAirportCode");

			List<String> valueOrigin = new ArrayList<String>();
			valueOrigin.add(strOrigin);

			moduleCriterionOrigin.setValue(valueOrigin);
			critrian.add(moduleCriterionOrigin);
		}
		if (strDestination != null && !(strDestination.equals(""))) {
			ModuleCriterion moduleCriterionDes = new ModuleCriterion();
			moduleCriterionDes.setCondition(ModuleCriterion.CONDITION_EQUALS);
			moduleCriterionDes.setFieldName("toAirportCode");

			List<String> valueDes = new ArrayList<String>();
			valueDes.add(strDestination);

			moduleCriterionDes.setValue(valueDes);
			critrian.add(moduleCriterionDes);
		}

		if ((strOrigin != null && !(strOrigin.equals(""))) || (strVia1 != null && !(strVia1.equals("")))
				|| (strVia2 != null && !(strVia2.equals(""))) || (strVia3 != null && !(strVia3.equals("")))
				|| (strVia4 != null && !(strVia4.equals(""))) || (strDestination != null && !(strDestination.equals("")))) {

			ModuleCriterion moduleCriterionVia = new ModuleCriterion();
			moduleCriterionVia.setCondition(ModuleCriterion.CONDITION_LIKE);
			moduleCriterionVia.setFieldName("routeCode");

			List<String> values = new ArrayList<String>();

			String searchValue = getRouteCode(strOrigin, strVia1, strVia2, strVia3, strVia4, strDestination);

			values.add(searchValue);
			moduleCriterionVia.setValue(values);
			critrian.add(moduleCriterionVia);
		}

		page = ModuleServiceLocator.getCommonServiceBD().getRouteInfo(critrian, recNo, 20);
		if (page != null) {
			colCityPair = page.getPageData();
			totRec = page.getTotalNoOfRecords();
			request.setAttribute(WebConstants.REQ_TOTAL_RECORDS, "'" + new Integer(totRec).toString() + "';");
		}
		return cityPairRowHtml(colCityPair);
	}

	private static String getRouteCode(String strOri, String strVia1, String strVia2, String strVia3, String strVia4,
			String strDes) {
		int maxViaPoint = getMaxViaPoint(strVia1, strVia2, strVia3, strVia4);
		String via1 = "", via2 = "", via3 = "", via4 = "", ori = "", des = "", searchStr = "";

		if ((strDes != null) && !(strDes.equals(""))) {
			ori = "%" + (((strOri != null) && !(strOri.equals(""))) ? (strOri + SEPERATOR + "%") : "___/");

		} else {

			ori = ((strOri != null) && !(strOri.equals(""))) ? (strOri + SEPERATOR + "%") : "___/";
		}

		des = (((strDes != null) && !(strDes.equals(""))) ? (strDes) : "___");

		if (maxViaPoint >= 1) {
			via1 = ((strVia1 != null) && !(strVia1.equals(""))) ? (strVia1 + SEPERATOR + "%") : "___/";
		}

		if (maxViaPoint >= 2) {
			via2 = ((strVia2 != null) && !(strVia2.equals(""))) ? (strVia2 + SEPERATOR + "%") : "___/";
		}

		if (maxViaPoint >= 3) {
			via3 = ((strVia3 != null) && !(strVia3.equals(""))) ? (strVia3 + SEPERATOR + "%") : "___/";
		}

		if (maxViaPoint == 4) {
			via4 = ((strVia4 != null) && !(strVia4.equals(""))) ? (strVia4 + SEPERATOR + "%") : "___/";
		}

		searchStr = ori;
		searchStr += via1;
		searchStr += via2;
		searchStr += via3;
		searchStr += via4;
		searchStr += des;
		return searchStr;
	}

	private static int getMaxViaPoint(String strVia1, String strVia2, String strVia3, String strVia4) {
		int maxVar = 0;

		if (strVia4 != null && !strVia4.equals("")) {
			maxVar = 4;
		} else if (strVia3 != null && !strVia3.equals("")) {
			maxVar = 3;
		} else if (strVia2 != null && !strVia2.equals("")) {
			maxVar = 2;
		} else if (strVia1 != null && !strVia1.equals("")) {
			maxVar = 1;
		}

		return maxVar;
	}

	/**
	 * Creates the ROUTE INFO Grid Data from a Collection of ROUTEINFOs
	 * 
	 * @param citypairs
	 *            the ollection of ROUTEINFOs
	 * @return String the array Containg ROUTEINFO Grid data
	 * @throws ModuleException
	 */
	private static String cityPairRowHtml(Collection<RouteInfo> citypairs) throws ModuleException {

		StringBuilder sb = new StringBuilder();

		sb.append("var cityPairData = new Array();");

		RouteInfo modelRouteInfo = null;
		int temp = 0;
		int noOFHours = 0;
		int noOfMins = 0;

		if (citypairs != null) {
			Iterator<RouteInfo> iter = citypairs.iterator();
			while (iter.hasNext()) {
				modelRouteInfo = (RouteInfo) iter.next();
				String strDuration = "";
				noOFHours = modelRouteInfo.getDuration() / 60;
				noOfMins = modelRouteInfo.getDuration() % 60;
				if (noOFHours == 0) {
					strDuration += "00";
				} else if (noOFHours < 10) {
					strDuration += "0" + noOFHours;
				} else {
					strDuration += noOFHours;
				}
				if (noOfMins == 0) {
					strDuration += ":00";
				} else if (noOfMins < 10) {
					strDuration += ":0" + noOfMins;
				} else {
					strDuration += ":" + noOfMins;
				}

				sb.append("cityPairData[" + temp + "] = new Array();");
				sb.append("cityPairData[" + temp + "][1]= '" + modelRouteInfo.getFromAirportCode() + "';");
				sb.append("cityPairData[" + temp + "][7]= '" + modelRouteInfo.getFromAirportCode() + "';");
				sb.append("cityPairData[" + temp + "][2]= '" + modelRouteInfo.getToAirportCode() + "';");
				sb.append("cityPairData[" + temp + "][8]= '" + modelRouteInfo.getToAirportCode() + "';");

				// Via Points
				Collection<RouteTransitsInfo> routeTransitsInfoCol = modelRouteInfo.getRouteTransits();
				if (routeTransitsInfoCol != null && routeTransitsInfoCol.size() > 0) {

					Iterator<RouteTransitsInfo> iterDetails = routeTransitsInfoCol.iterator();
					int seqNo = 0;

					while (iterDetails.hasNext()) {
						RouteTransitsInfo routeTransitsInfo = (RouteTransitsInfo) iterDetails.next();
						seqNo = routeTransitsInfo.getSeqNo();

						switch (seqNo) {
						case 1:
							// Via 1
							sb.append("cityPairData[" + temp + "][11]= '" + routeTransitsInfo.getTransitAirportCode() + "';");
							sb.append("cityPairData[" + temp + "][16]= '" + routeTransitsInfo.getRouteTransitId() + "';");
							break;

						case 2:
							// Via 2
							sb.append("cityPairData[" + temp + "][12]= '" + routeTransitsInfo.getTransitAirportCode() + "';");
							sb.append("cityPairData[" + temp + "][17]= '" + routeTransitsInfo.getRouteTransitId() + "';");
							break;

						case 3:
							// Via 3
							sb.append("cityPairData[" + temp + "][13]= '" + routeTransitsInfo.getTransitAirportCode() + "';");
							sb.append("cityPairData[" + temp + "][18]= '" + routeTransitsInfo.getRouteTransitId() + "';");
							break;

						case 4:
							// Via 4
							sb.append("cityPairData[" + temp + "][14]= '" + routeTransitsInfo.getTransitAirportCode() + "';");
							sb.append("cityPairData[" + temp + "][19]= '" + routeTransitsInfo.getRouteTransitId() + "';");
							break;

						}
					}

					// Sets the missing Via Points
					int colSize = routeTransitsInfoCol.size();

					switch (colSize) {
					case 1:
						sb.append("cityPairData[" + temp + "][12]= '';");
						sb.append("cityPairData[" + temp + "][13]= '';");
						sb.append("cityPairData[" + temp + "][14]= '';");
						sb.append("cityPairData[" + temp + "][17]= '';");
						sb.append("cityPairData[" + temp + "][18]= '';");
						sb.append("cityPairData[" + temp + "][19]= '';");
						break;

					case 2:
						sb.append("cityPairData[" + temp + "][13]= '';");
						sb.append("cityPairData[" + temp + "][14]= '';");
						sb.append("cityPairData[" + temp + "][18]= '';");
						sb.append("cityPairData[" + temp + "][19]= '';");
						break;

					case 3:
						sb.append("cityPairData[" + temp + "][14]= '';");
						sb.append("cityPairData[" + temp + "][19]= '';");
						break;

					}

				} else {
					sb.append("cityPairData[" + temp + "][11]= '';");
					sb.append("cityPairData[" + temp + "][12]= '';");
					sb.append("cityPairData[" + temp + "][13]= '';");
					sb.append("cityPairData[" + temp + "][14]= '';");
					sb.append("cityPairData[" + temp + "][16]= '';");
					sb.append("cityPairData[" + temp + "][17]= '';");
					sb.append("cityPairData[" + temp + "][18]= '';");
					sb.append("cityPairData[" + temp + "][19]= '';");
				}

				// Via points for display purposes
				sb.append("cityPairData[" + temp + "][20]= '" + getViaPointsForDisplay(routeTransitsInfoCol) + "';");

				sb.append("cityPairData[" + temp + "][3]= '" + String.valueOf(modelRouteInfo.getDistance()) + "';");
				sb.append("cityPairData[" + temp + "][4]= '" + strDuration + "';");

				String strModTolerance = "";
				if (modelRouteInfo.getDurationTolerance() != null) {
					strModTolerance = String.valueOf(modelRouteInfo.getDurationTolerance());
				}

				sb.append("cityPairData[" + temp + "][5]= '" + strModTolerance + "';");
				if (modelRouteInfo.getStatus() != null) {
					if (modelRouteInfo.getStatus().equals(RouteInfo.STATUS_ACTIVE))
						sb.append("cityPairData[" + temp + "][6] = 'Active';");
					if (modelRouteInfo.getStatus().equals(RouteInfo.STATUS_INACTIVE))
						sb.append("cityPairData[" + temp + "][6] = 'In-Active';");
				} else {
					sb.append("cityPairData[" + temp + "][6] = '';");
				}
				sb.append("cityPairData[" + temp + "][9]= '" + modelRouteInfo.getVersion() + "';");
				sb.append("cityPairData[" + temp + "][10]= '" + modelRouteInfo.getRouteCode() + "';");
				sb.append("cityPairData[" + temp + "][15]= '" + modelRouteInfo.getRouteId() + "';");

				// LCC Connectivity status for display purposess
				sb.append("cityPairData[" + temp + "][21]= '"
						+ (RouteInfo.LCC_PUBLISHED_TRUE.equals(modelRouteInfo.getIsLCCPubilshed()) ? "true" : "false") + "';");
				temp++;
			}
		}
		return sb.toString();
	}

	/**
	 * Creates the Client Validations for ROUTE INFO page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the String array containing the Client validations
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {
		if (clientErrors == null) {
			Properties moduleErrs = new Properties();

			moduleErrs.setProperty("um.citypair.form.origin.required", "originRqrd");
			moduleErrs.setProperty("um.citypair.form.destination.required", "destinationRqrd");
			moduleErrs.setProperty("um.citypair.form.distance.required", "distanceRqrd");
			moduleErrs.setProperty("um.citypair.form.duration.name.required", "durationRqrd");
			moduleErrs.setProperty("um.citypair.form.duration.format", "durationFmt");
			moduleErrs.setProperty("um.citypair.form.tolerance.format", "toleranceFmt");
			moduleErrs.setProperty("um.citypair.form.distance.format", "distanceFmt");
			moduleErrs.setProperty("um.citypair.form.distance.zero", "distanceZero");
			moduleErrs.setProperty("um.citypair.form.duration.zero", "durationZero");
			moduleErrs.setProperty("um.airadmin.delete.confirmation", "deleteConf");
			moduleErrs.setProperty("um.citypair.form.invalid.route", "errInvalidRoute");
			moduleErrs.setProperty("um.citypair.form.viapoint.required", "errViaOrder");
			moduleErrs.setProperty("um.citypair.form.segment.route.in.use", "segmentRouteInUse");
			moduleErrs.setProperty("um.airadmin.delete.confirmation.segment.routes", "deleteConfSegRoute");
			moduleErrs.setProperty("um.citypair.form.segment.route.exists", "routeExists");
			clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}
		return clientErrors;
	}

	/**
	 * Gets the Via Points for display purposes
	 * 
	 * @param viaPointsCol
	 * @return String
	 * @author Dhanusha
	 * @since 23 Feb, 2008
	 */
	private static String getViaPointsForDisplay(Collection<RouteTransitsInfo> viaPointsCol) {
		StringBuffer sb = new StringBuffer();

		if (viaPointsCol != null && viaPointsCol.size() > 0) {
			int cnt = 0;
			Iterator<RouteTransitsInfo> iterDetails = viaPointsCol.iterator();

			while (iterDetails.hasNext()) {
				RouteTransitsInfo routeTransitsInfo = (RouteTransitsInfo) iterDetails.next();

				if (cnt < (viaPointsCol.size() - 1)) {
					sb.append(routeTransitsInfo.getTransitAirportCode() + "/ ");
				} else {
					sb.append(routeTransitsInfo.getTransitAirportCode());
				}
				cnt++;
			}
		}

		return sb.toString();

	}

}
