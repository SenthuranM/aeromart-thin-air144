package com.isa.thinair.airadmin.core.service;

import com.isa.thinair.airinventory.api.service.BaggageBusinessDelegate;
import com.isa.thinair.airinventory.api.service.BookingClassBD;
import com.isa.thinair.airinventory.api.service.FlightInventoryBD;
import com.isa.thinair.airinventory.api.service.FlightInventoryResBD;
import com.isa.thinair.airinventory.api.service.InventoryTemplateBD;
import com.isa.thinair.airinventory.api.service.LogicalCabinClassBD;
import com.isa.thinair.airinventory.api.service.MealBD;
import com.isa.thinair.airinventory.api.service.SeatMapBD;
import com.isa.thinair.airinventory.api.utils.AirinventoryConstants;
import com.isa.thinair.airmaster.api.service.AircraftBD;
import com.isa.thinair.airmaster.api.service.AirportBD;
import com.isa.thinair.airmaster.api.service.AirportTransferBD;
import com.isa.thinair.airmaster.api.service.AutomaticCheckinBD;
import com.isa.thinair.airmaster.api.service.CommonMasterBD;
import com.isa.thinair.airmaster.api.service.GdsBD;
import com.isa.thinair.airmaster.api.service.LocationBD;
import com.isa.thinair.airmaster.api.service.SmartSearchBD;
import com.isa.thinair.airmaster.api.service.SsrBD;
import com.isa.thinair.airmaster.api.service.TranslationBD;
import com.isa.thinair.airmaster.api.utils.AirmasterConstants;
import com.isa.thinair.airpricing.api.service.ChargeBD;
import com.isa.thinair.airpricing.api.service.CommonAncillaryServiceBD;
import com.isa.thinair.airpricing.api.service.FareBD;
import com.isa.thinair.airpricing.api.service.FareRuleBD;
import com.isa.thinair.airpricing.api.utils.AirpricingConstants;
import com.isa.thinair.airreservation.api.service.BlacklistPAXBD;
import com.isa.thinair.airreservation.api.service.GroupBookingServiceBD;
import com.isa.thinair.airreservation.api.service.PassengerBD;
import com.isa.thinair.airreservation.api.service.ReservationAuxilliaryBD;
import com.isa.thinair.airreservation.api.service.ReservationBD;
import com.isa.thinair.airreservation.api.service.ReservationQueryBD;
import com.isa.thinair.airreservation.api.service.SegmentBD;
import com.isa.thinair.airreservation.api.utils.AirreservationConstants;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.airschedules.api.service.ScheduleBD;
import com.isa.thinair.airschedules.api.service.ScheduleBMTBD;
import com.isa.thinair.airschedules.api.utils.AirschedulesConstants;
import com.isa.thinair.airsecurity.api.service.SecurityBD;
import com.isa.thinair.airsecurity.api.service.SecurityUtilBD;
import com.isa.thinair.airsecurity.api.utils.AirsecurityConstants;
import com.isa.thinair.airtravelagents.api.service.TravelAgentBD;
import com.isa.thinair.airtravelagents.api.service.TravelAgentFinanceBD;
import com.isa.thinair.airtravelagents.api.utils.AirtravelagentsConstants;
import com.isa.thinair.alerting.api.service.AlertingBD;
import com.isa.thinair.alerting.api.utils.AlertingConstants;
import com.isa.thinair.auditor.api.service.AuditorBD;
import com.isa.thinair.auditor.api.utils.AuditorConstants;
import com.isa.thinair.commons.core.util.GenericLookupUtils;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.invoicing.api.service.InvoicingBD;
import com.isa.thinair.invoicing.api.utils.InvoicingConstants;
import com.isa.thinair.lccclient.api.service.LCCMasterDataBD;
import com.isa.thinair.lccclient.api.service.LCCMasterDataPublisherBD;
import com.isa.thinair.lccclient.api.utils.LccclientConstants;
import com.isa.thinair.messagepasser.api.service.ETLBD;
import com.isa.thinair.messagepasser.api.service.PRLBD;
import com.isa.thinair.messagepasser.api.service.XApnlBD;
import com.isa.thinair.messagepasser.api.utils.MessagepasserConstants;
import com.isa.thinair.messaging.api.service.MessagingServiceBD;
import com.isa.thinair.messaging.api.utils.MessagingConstants;
import com.isa.thinair.paymentbroker.api.service.PaymentBrokerBD;
import com.isa.thinair.paymentbroker.api.utils.PaymentbrokerConstants;
import com.isa.thinair.platform.api.IServiceDelegate;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;
import com.isa.thinair.promotion.api.service.AnciOfferBD;
import com.isa.thinair.promotion.api.service.BunldedFareBD;
import com.isa.thinair.promotion.api.service.LoyaltyManagementBD;
import com.isa.thinair.promotion.api.service.PromotionAdministrationBD;
import com.isa.thinair.promotion.api.service.PromotionCriteriaAdminBD;
import com.isa.thinair.promotion.api.service.PromotionManagementBD;
import com.isa.thinair.promotion.api.service.VoucherBD;
import com.isa.thinair.promotion.api.service.VoucherTemplateBD;
import com.isa.thinair.promotion.api.utils.PromotionConstants;
import com.isa.thinair.reporting.api.service.DataExtractionBD;
import com.isa.thinair.reporting.api.service.FLADataProviderBD;
import com.isa.thinair.reporting.api.service.MISDataProviderBD;
import com.isa.thinair.reporting.api.service.ScheduledReportBD;
import com.isa.thinair.reporting.api.utils.ReportingConstants;
import com.isa.thinair.reportingframework.api.service.ReportingFrameworkBD;
import com.isa.thinair.reportingframework.api.utils.ReportingframeworkConstants;
import com.isa.thinair.scheduler.api.service.SchedulerBD;
import com.isa.thinair.scheduler.api.utils.SchedulerConstants;

/**
 * @author Menaka
 * @author Srikantha
 * 
 *         ModuleServiceLocator is the common class for locating the business delegate services
 */
public final class ModuleServiceLocator {

	// private static final Log log = LogFactory.getLog(ModuleServiceLocator.class);

	/**
	 * Gets the Global Config
	 * 
	 * @return GlobalConfig the global config delegate
	 */
	public static GlobalConfig getGlobalConfig() {
		return (GlobalConfig) LookupServiceFactory.getInstance().getBean(PlatformBeanUrls.Commons.GLOBAL_CONFIG_BEAN);
	}

	/**
	 * Gets Schedule Service(Flight) BD
	 * 
	 * @return ScheduleBD the Flight schedule delegate
	 */
	public final static ScheduleBD getScheduleServiceBD() {
		return (ScheduleBD) ModuleServiceLocator.lookupEJB3Service(AirschedulesConstants.MODULE_NAME, ScheduleBD.SERVICE_NAME);
	}

	/**
	 * Gets Schedule Service(Flight) BD
	 * 
	 * @return ScheduleBD the Flight schedule delegate
	 */
	public final static ScheduleBMTBD getScheduleServiceBMTBD() {
		return (ScheduleBMTBD) ModuleServiceLocator.lookupEJB3Service(AirschedulesConstants.MODULE_NAME,
				ScheduleBMTBD.SERVICE_NAME);
	}

	/**
	 * Gets the Flight Service BD
	 * 
	 * @return FlightBD the Flight delegate
	 */
	public final static FlightBD getFlightServiceBD() {
		return (FlightBD) ModuleServiceLocator.lookupEJB3Service(AirschedulesConstants.MODULE_NAME, FlightBD.SERVICE_NAME);
	}

	/**
	 * Gets the Location Service from Air Master
	 * 
	 * @return LocationBD the Location delegate
	 */
	public final static LocationBD getLocationServiceBD() {
		return (LocationBD) ModuleServiceLocator.lookupEJB3Service(AirmasterConstants.MODULE_NAME, LocationBD.SERVICE_NAME);
	}

	/**
	 * Gets the Booking Class Service from Inventory
	 * 
	 * @return BookingClassBD the Booking class delegate
	 */
	public final static BookingClassBD getBookingClassBD() {
		return (BookingClassBD) ModuleServiceLocator.lookupEJB3Service(AirinventoryConstants.MODULE_NAME,
				BookingClassBD.SERVICE_NAME);
	}

	/**
	 * Gets the Logical Cabin Class Service from Inventory
	 * 
	 * @return LogicalCabinClassBD the Logical Cabin class delegate
	 */
	public final static LogicalCabinClassBD getLogicalCabinClassBD() {
		return (LogicalCabinClassBD) ModuleServiceLocator.lookupEJB3Service(AirinventoryConstants.MODULE_NAME,
				LogicalCabinClassBD.SERVICE_NAME);
	}

	/**
	 * Gets the Flight inventory Service
	 * 
	 * @return FlightInventoryBD the Flight Inventory delegate
	 */
	public final static FlightInventoryBD getFlightInventoryBD() {
		return (FlightInventoryBD) ModuleServiceLocator.lookupEJB3Service(AirinventoryConstants.MODULE_NAME,
				FlightInventoryBD.SERVICE_NAME);
	}

	/**
	 * Gets an Airport Service from Air master
	 * 
	 * @return AirportBD the Airport delegate
	 */
	public final static AirportBD getAirportServiceBD() {
		return (AirportBD) ModuleServiceLocator.lookupEJB3Service(AirmasterConstants.MODULE_NAME, AirportBD.SERVICE_NAME);
	}
	
	/**
	 * Gets an Airport Transfer Service from Air master
	 * 
	 * @return AirportBD the Airport delegate
	 */
	public final static AirportTransferBD getAirportTransferServiceBD() {
		return (AirportTransferBD) ModuleServiceLocator.lookupEJB3Service(AirmasterConstants.MODULE_NAME, AirportTransferBD.SERVICE_NAME);
	}

	/**
	 * Gets an Airport Service from Air master
	 * 
	 * @return AirportBD the Airport delegate
	 */
	public final static GdsBD getGdsServiceBD() {
		return (GdsBD) ModuleServiceLocator.lookupEJB3Service(AirmasterConstants.MODULE_NAME, GdsBD.SERVICE_NAME);
	}

	/**
	 * Gets an Aircraft Service fro Air Master
	 * 
	 * @return AircraftBD the Aircraft delegate
	 */
	public final static AircraftBD getAircraftServiceBD() {
		return (AircraftBD) ModuleServiceLocator.lookupEJB3Service(AirmasterConstants.MODULE_NAME, AircraftBD.SERVICE_NAME);
	}

	/**
	 * Gets a Common Master Service from Air Master
	 * 
	 * @return CommonMasterBD the common master delegate
	 */
	public final static CommonMasterBD getCommonServiceBD() {
		return (CommonMasterBD) ModuleServiceLocator.lookupEJB3Service(AirmasterConstants.MODULE_NAME,
				CommonMasterBD.SERVICE_NAME);
	}

	/**
	 * Gets TravelAgent service fro Air Travel Agent
	 * 
	 * @return TravelAgentBD the Travel Agent delegate
	 */
	public final static TravelAgentBD getTravelAgentBD() {
		return (TravelAgentBD) ModuleServiceLocator.lookupEJB3Service(AirtravelagentsConstants.MODULE_NAME,
				TravelAgentBD.SERVICE_NAME);
	}

	/**
	 * Gets Security Service from Air Security
	 * 
	 * @return SecurityBD the Security delegate
	 */
	public final static SecurityBD getSecurityBD() {
		return (SecurityBD) ModuleServiceLocator.lookupEJB3Service(AirsecurityConstants.MODULE_NAME, SecurityBD.SERVICE_NAME);
	}

	/**
	 * Gets the Security Util Service
	 * 
	 * @return SecurityUtilBD the Security Util delegate
	 */
	public final static SecurityUtilBD getSecurityUtilBD() {
		return (SecurityUtilBD) ModuleServiceLocator.lookupEJB3Service(AirsecurityConstants.MODULE_NAME,
				SecurityUtilBD.SERVICE_NAME);
	}

	/**
	 * Gets Travel Agent Finance Service from Air Travel Agent
	 * 
	 * @return TravelAgentFinanceBD the Travel Agent Finance delegate
	 */
	public final static TravelAgentFinanceBD getTravelAgentFinanceBD() {
		return (TravelAgentFinanceBD) ModuleServiceLocator.lookupEJB3Service(AirtravelagentsConstants.MODULE_NAME,
				TravelAgentFinanceBD.SERVICE_NAME);
	}

	/**
	 * Gets Invoicing delegate
	 */
	public final static InvoicingBD getInvoicingBD() {
		return (InvoicingBD) ModuleServiceLocator.lookupEJB3Service(InvoicingConstants.MODULE_NAME, InvoicingBD.SERVICE_NAME);
	}

	/**
	 * Gets fare Rule Service from Air Pricing
	 * 
	 * @return FareRuleBD the Fare Rule delegate
	 */
	public final static FareRuleBD getRuleBD() {
		return (FareRuleBD) ModuleServiceLocator.lookupEJB3Service(AirpricingConstants.MODULE_NAME, FareRuleBD.SERVICE_NAME);
	}

	/**
	 * Gets a Charge Service from Air Pricing
	 * 
	 * @return ChargeBD the Charge delegate
	 */
	public final static ChargeBD getChargeBD() {
		return (ChargeBD) ModuleServiceLocator.lookupEJB3Service(AirpricingConstants.MODULE_NAME, ChargeBD.SERVICE_NAME);
	}

	/**
	 * Gets Fare Service from Air Pricing
	 * 
	 * @return FareBD the Fare delegate
	 */
	public final static FareBD getFareBD() {
		return (FareBD) ModuleServiceLocator.lookupEJB3Service(AirpricingConstants.MODULE_NAME, FareBD.SERVICE_NAME);
	}

	/**
	 * Gets the Data Extraction Service from Reporting
	 * 
	 * @return DataExtractionBD the data extraction delegate
	 */
	public final static DataExtractionBD getDataExtractionBD() {
		return (DataExtractionBD) ModuleServiceLocator.lookupEJB3Service(ReportingConstants.MODULE_NAME,
				DataExtractionBD.SERVICE_NAME);
	}

	/**
	 * Gets the scheduled reporting frame work.
	 * 
	 * @return ScheduledReportBD the Reporting Frame work delegate
	 */
	public final static ScheduledReportBD getScheduledReportBD() {
		return (ScheduledReportBD) ModuleServiceLocator.lookupEJB3Service(ReportingConstants.MODULE_NAME,
				ScheduledReportBD.SERVICE_NAME);
	}

	/**
	 * Gets the Audit Service fro Audit
	 * 
	 * @return AuditorBD the Audit delegate
	 */
	public final static AuditorBD getAuditorServiceBD() {
		return (AuditorBD) ModuleServiceLocator.lookupServiceBD(AuditorConstants.MODULE_NAME,
				AuditorConstants.BDKeys.AUDITOR_SERVICE);
	}

	/**
	 * Added By ChamindaP Gets the reporting frame work.
	 * 
	 * @return ReportingFrameworkBD the Reporting Frame work delegate
	 */
	public final static ReportingFrameworkBD getReportingFrameworkBD() {
		return (ReportingFrameworkBD) ModuleServiceLocator.lookupServiceBD(ReportingframeworkConstants.MODULE_NAME,
				ReportingframeworkConstants.BDKeys.REPORTINGFRAMEWORK_SERVICE);
	}
	
	/**
	 * Added By Sashrika Gets the AlertingBD.
	 * 
	 * @return getAlertingBD 
	 */
	public final static AlertingBD getAlertingBD() {
		return (AlertingBD) ModuleServiceLocator.lookupServiceBD(AlertingConstants.MODULE_NAME,
				AlertingConstants.BDKeys.ALERTING_SERVICE);
		//return (AlertingBD) ModuleServiceLocator.lookupEJB3Service(AlertingConstants.MODULE_NAME,
				//AlertingConstants.BDKeys.ALERTING_SERVICE);
	}

	/**
	 * Gets the Flight Inventory Reservation Service from Inventory
	 * 
	 * @return FlightInventoryResBD the Flight Inventory reservation delegate
	 */
	public final static FlightInventoryResBD getFlightInventoryReservationBD() {
		return (FlightInventoryResBD) ModuleServiceLocator.lookupEJB3Service(AirinventoryConstants.MODULE_NAME,
				FlightInventoryResBD.SERVICE_NAME);
	}

	/**
	 * Gets Reservation Service from Reservation
	 * 
	 * @return ReservationBD the Reservation delegate
	 */
	public final static ReservationBD getReservationBD() {
		return (ReservationBD) ModuleServiceLocator.lookupEJB3Service(AirreservationConstants.MODULE_NAME,
				ReservationBD.SERVICE_NAME);
	}

	/**
	 * Gets XApnl Service
	 * 
	 * @return XApnlBD the XApnl delegate
	 */
	public final static XApnlBD getXApnlBD() {
		return (XApnlBD) ModuleServiceLocator.lookupEJB3Service(MessagepasserConstants.MODULE_NAME, XApnlBD.SERVICE_NAME);
	}

	/**
	 * Gets ETL Service
	 * 
	 * @return EtlBD the ETL delegate
	 */
	public final static ETLBD getEtlBD() {
		return (ETLBD) ModuleServiceLocator.lookupEJB3Service(MessagepasserConstants.MODULE_NAME, ETLBD.SERVICE_NAME);
	}

	/**
	 * Gets PRL Service
	 * 
	 * @return PRLBD the PRL delegate
	 */
	public final static PRLBD getPRLBD() {
		return (PRLBD) ModuleServiceLocator.lookupEJB3Service(MessagepasserConstants.MODULE_NAME, PRLBD.SERVICE_NAME);
	}

	/**
	 * Gets the Reservation Auxiliary Service from Reservation
	 * 
	 * @return ReservationAuxilliaryBD the Reservation Auxiliary delegate
	 */
	public final static ReservationAuxilliaryBD getReservationAuxilliaryBD() {
		return (ReservationAuxilliaryBD) ModuleServiceLocator.lookupEJB3Service(AirreservationConstants.MODULE_NAME,
				ReservationAuxilliaryBD.SERVICE_NAME);
	}

	/**
	 * Gets the Messaging Service from Messaging
	 * 
	 * @return MessagingServiceBD the Messaging delegate
	 */
	public final static MessagingServiceBD getMessagingBD() {
		return (MessagingServiceBD) ModuleServiceLocator.lookupEJB3Service(MessagingConstants.MODULE_NAME,
				MessagingServiceBD.SERVICE_NAME);
	}

	/**
	 * Gets a Reservation Segment Service from Reservation
	 * 
	 * @return SegmentBD the Reservation Segment delegate
	 */
	public final static SegmentBD getResSegmentBD() {
		return (SegmentBD) ModuleServiceLocator.lookupEJB3Service(AirreservationConstants.MODULE_NAME, SegmentBD.SERVICE_NAME);
	}

	public final static SeatMapBD getSeatMapBD() {
		return (SeatMapBD) ModuleServiceLocator.lookupEJB3Service(AirinventoryConstants.MODULE_NAME, SeatMapBD.SERVICE_NAME);
	}

	/**
	 * Gets Meal Service from Air Inventory
	 * 
	 * @return MealBD
	 */
	public final static MealBD getMealBD() {
		return (MealBD) ModuleServiceLocator.lookupEJB3Service(AirinventoryConstants.MODULE_NAME, MealBD.SERVICE_NAME);
	}

	/**
	 * Gets an Translation Service from Air master
	 * 
	 * @return TranslationBD
	 */
	public final static TranslationBD getTranslationBD() {
		return (TranslationBD) ModuleServiceLocator.lookupEJB3Service(AirmasterConstants.MODULE_NAME, TranslationBD.SERVICE_NAME);
	}

	/**
	 * Gets the SSR Service from Air Master
	 * 
	 * @return SsrBD the SSR delegate
	 */
	public final static SsrBD getSsrServiceBD() {
		return (SsrBD) ModuleServiceLocator.lookupEJB3Service(AirmasterConstants.MODULE_NAME, SsrBD.SERVICE_NAME);
	}

	public final static LogicalCabinClassBD getLogicalCabinClassServiceBD() {
		return (LogicalCabinClassBD) ModuleServiceLocator.lookupEJB3Service(AirinventoryConstants.MODULE_NAME,
				LogicalCabinClassBD.SERVICE_NAME);
	}

	/**
	 * Returns a Service Delegate for a given Module
	 * 
	 * @param targetModuleName
	 *            the Module name
	 * @param BDKeyWithoutLocality
	 *            BD key with out locality (remote/local)
	 * @return IServiceDelegate the service Delegate
	 */
	private static IServiceDelegate lookupServiceBD(String targetModuleName, String BDKeyWithoutLocality) {
		return GenericLookupUtils.lookupEJB2ServiceBD(targetModuleName, BDKeyWithoutLocality, AiradminModuleUtils.getConfig(),
				"airadmin", "airadmin.config.dependencymap.invalid");
	}

	/**
	 * Returns EJB3 transparent service delegate
	 * 
	 * @param targetModuleName
	 * @param BDKeyWithoutLocality
	 * @return
	 */
	private static Object lookupEJB3Service(String targetModuleName, String serviceName) {
		return GenericLookupUtils.lookupEJB3Service(targetModuleName, serviceName, AiradminModuleUtils.getConfig(), "airadmin",
				"airadmin.config.dependencymap.invalid");
	}

	/**
	 * Returns EJB3 transparent service delegate
	 * 
	 * @param targetModuleName
	 * @param BDKeyWithoutLocality
	 * @return
	 */
	public final static PaymentBrokerBD getPaymentBrokerBD() {
		return (PaymentBrokerBD) ModuleServiceLocator.lookupEJB3Service(PaymentbrokerConstants.MODULE_NAME,
				PaymentBrokerBD.SERVICE_NAME);
	}

	public final static MISDataProviderBD getMISDataProviderBD() {
		return (MISDataProviderBD) ModuleServiceLocator.lookupEJB3Service(ReportingConstants.MODULE_NAME,
				MISDataProviderBD.SERVICE_NAME);
	}

	public final static FLADataProviderBD getFLADataProviderBD() {
		return (FLADataProviderBD) ModuleServiceLocator.lookupEJB3Service(ReportingConstants.MODULE_NAME,
				FLADataProviderBD.SERVICE_NAME);
	}

	public final static LCCMasterDataPublisherBD getLCCMasterDataPublisherBD() {
		return (LCCMasterDataPublisherBD) ModuleServiceLocator.lookupEJB3Service(LccclientConstants.MODULE_NAME,
				LCCMasterDataPublisherBD.SERVICE_NAME);
	}

	/**
	 * Gets Baggage Service from Air Inventory
	 * 
	 * @return MealBD
	 */
	public final static BaggageBusinessDelegate getBaggageBD() {
		return (BaggageBusinessDelegate) ModuleServiceLocator.lookupEJB3Service(AirinventoryConstants.MODULE_NAME,
				BaggageBusinessDelegate.SERVICE_NAME);

	}

	/**
	 * Gets a Passenger Service from Reservation
	 * 
	 * @return PassengerBD the Passenger delegate
	 */
	public final static PassengerBD getPassengerBD() {
		return (PassengerBD) ModuleServiceLocator
				.lookupEJB3Service(AirreservationConstants.MODULE_NAME, PassengerBD.SERVICE_NAME);
	}

	/**
	 * Return transparent reservation query business delegate
	 * 
	 * @return
	 */
	public final static ReservationQueryBD getReservationQueryBD() {
		return (ReservationQueryBD) ModuleServiceLocator.lookupEJB3Service(AirreservationConstants.MODULE_NAME,
				ReservationBD.SERVICE_NAME);
	}

	public final static PromotionManagementBD getPromotionManagementBD() {
		return (PromotionManagementBD) ModuleServiceLocator.lookupEJB3Service(PromotionConstants.MODULE_NAME,
				PromotionManagementBD.SERVICE_NAME);
	}

	public final static PromotionAdministrationBD getPromotionAdministrationBD() {
		return (PromotionAdministrationBD) ModuleServiceLocator.lookupEJB3Service(PromotionConstants.MODULE_NAME,
				PromotionAdministrationBD.SERVICE_NAME);
	}
	
	public final static VoucherTemplateBD getVoucherTemplateBD() {
		return (VoucherTemplateBD) ModuleServiceLocator.lookupEJB3Service(PromotionConstants.MODULE_NAME,
				VoucherTemplateBD.SERVICE_NAME);
	}
	
	public final static VoucherBD getVoucherBD() {
		return (VoucherBD) ModuleServiceLocator.lookupEJB3Service(PromotionConstants.MODULE_NAME,
				VoucherBD.SERVICE_NAME);
	}
	
	

	/**
	 * Returns an instance of the {@link PromotionCriteriaAdminBD}
	 */
	public final static PromotionCriteriaAdminBD getPromotionCriteriaAdminBD() {
		return (PromotionCriteriaAdminBD) ModuleServiceLocator.lookupEJB3Service(PromotionConstants.MODULE_NAME,
				PromotionCriteriaAdminBD.SERVICE_NAME);
	}
	
	public final static BlacklistPAXBD getBlacklisPAXCriteriaAdminBD() {
		return (BlacklistPAXBD) ModuleServiceLocator.lookupEJB3Service(AirreservationConstants.MODULE_NAME,
				BlacklistPAXBD.SERVICE_NAME);
	}

	public final static GroupBookingServiceBD getGroupBookingBD() {
		return (GroupBookingServiceBD) ModuleServiceLocator.lookupEJB3Service(AirreservationConstants.MODULE_NAME,
				GroupBookingServiceBD.SERVICE_NAME);
	}

	public final static AnciOfferBD getAnciOfferBD() {
		return (AnciOfferBD) ModuleServiceLocator.lookupEJB3Service(PromotionConstants.MODULE_NAME, AnciOfferBD.SERVICE_NAME);
	}

	public final static LCCMasterDataBD getLCCMasterDataBD() {
		return (LCCMasterDataBD) lookupEJB3Service(LccclientConstants.MODULE_NAME, LCCMasterDataBD.SERVICE_NAME);
	}

	public static final BunldedFareBD getBundledFareBD() {
		return (BunldedFareBD) ModuleServiceLocator.lookupEJB3Service(PromotionConstants.MODULE_NAME, BunldedFareBD.SERVICE_NAME);
	}

	public static final SegmentBD getSegmentBD() {
		return (SegmentBD) lookupEJB3Service(AirreservationConstants.MODULE_NAME, SegmentBD.SERVICE_NAME);
	}

	public static SchedulerBD getSchedularBD() {
		return (SchedulerBD) lookupEJB3Service(SchedulerConstants.MODULE_NAME, SchedulerBD.SERVICE_NAME);
	}
		/**
	 * Gets Loyalty Management Service
	 * 
	 * @return LoyaltyManagementBD
	 */
	public final static LoyaltyManagementBD getLoyaltyManagementBD() {
		return (LoyaltyManagementBD) ModuleServiceLocator.lookupEJB3Service(PromotionConstants.MODULE_NAME,
				LoyaltyManagementBD.SERVICE_NAME);
	}
	
	public final static CommonAncillaryServiceBD getCommonAncillaryServiceBD() {
		return (CommonAncillaryServiceBD) ModuleServiceLocator.lookupEJB3Service(AirpricingConstants.MODULE_NAME,
				CommonAncillaryServiceBD.SERVICE_NAME);
	}

	public static final SmartSearchBD getSmartSearchBD() {
		return (SmartSearchBD) lookupEJB3Service(AirmasterConstants.MODULE_NAME, SmartSearchBD.SERVICE_NAME);
	}
	/**
	 * Gets the Inventory Template Service
	 * 
	 * @return InventoryTemplateBD
	 */
	public final static InventoryTemplateBD getInventoryTemplateBD() {
		return (InventoryTemplateBD) ModuleServiceLocator.lookupEJB3Service(AirinventoryConstants.MODULE_NAME,
				InventoryTemplateBD.SERVICE_NAME);
	}

	/**
	 * Gets Automatic Checkin Service from Air Admin
	 * 
	 * @return AutomaticCheckinBD
	 */
	public final static AutomaticCheckinBD getAutomaticCheckinTemplateBD() {
		return (AutomaticCheckinBD) ModuleServiceLocator.lookupEJB3Service(AirmasterConstants.MODULE_NAME,
				AutomaticCheckinBD.SERVICE_NAME);

	}
}