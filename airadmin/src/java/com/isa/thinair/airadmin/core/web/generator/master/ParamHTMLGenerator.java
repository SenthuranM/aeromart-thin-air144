package com.isa.thinair.airadmin.core.web.generator.master;

import java.util.Collection;
import java.util.Iterator;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airmaster.api.model.BusinessSystemParameter;
import com.isa.thinair.airsecurity.api.model.ParamSearchDTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;

public class ParamHTMLGenerator {

	private static String clientErrors;

	private static final String PARAM_RECNO = "hdnRecNo";

	private static final String PARAM_SEARCHDATA = "hdnSearchData";

	private static final String PARAM_GRIDDATA = "hdnGridData";

	private static final String PARAM_SEARCH_PARAM_KEY = "selParamKey";

	private static final String PARAM_SEARCH_PARAM_NAME = "selParameterName";

	private static final String PARAM_SEARCH_CARRIER_CODE = "selCarrierCode";

	private Properties props;

	private static GlobalConfig globalConfig = CommonsServices.getGlobalConfig();

	/**
	 * Construct HG Using Property file Values
	 * 
	 * @param props
	 *            the Properties file containg Request values
	 */
	public ParamHTMLGenerator(Properties props) {
		this.props = props;
	}

	/**
	 * Gets the Param Grid Row Array For the Search
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Grid Array
	 * @throws ModuleException
	 *             the ModuleException
	 */
	@SuppressWarnings("unchecked")
	public final String getParamRowHtml(HttpServletRequest request) throws ModuleException {

		Collection<BusinessSystemParameter> colRoles = null;
		Page page = null;
		int recNo = 0;
		int totRec = 0;
		String strSearchData = props.getProperty(PARAM_SEARCHDATA);
		String strGridData = props.getProperty(PARAM_GRIDDATA);

		if (!props.getProperty(PARAM_RECNO).equals("")) {
			recNo = Integer.parseInt(props.getProperty(PARAM_RECNO));
			recNo = recNo - 1;
		}
		request.setAttribute("recordNo", new Integer(recNo + 1).toString());
		request.setAttribute("reqParamSearchData", strSearchData);
		request.setAttribute("reqParamGridData", strGridData);
		ParamSearchDTO paramSearchDTO = new ParamSearchDTO();

		if (!props.getProperty(PARAM_SEARCH_CARRIER_CODE).trim().equals("")
				&& !props.getProperty(PARAM_SEARCH_CARRIER_CODE).trim().equalsIgnoreCase("ALL")) {
			paramSearchDTO.setCarrierCode("%" + props.getProperty(PARAM_SEARCH_CARRIER_CODE).toUpperCase().trim() + "%");
		}

		if (!props.getProperty(PARAM_SEARCH_PARAM_KEY).trim().equals("")
				&& !props.getProperty(PARAM_SEARCH_PARAM_KEY).trim().equalsIgnoreCase("ALL")) {
			paramSearchDTO.setParameterKey("%" + props.getProperty(PARAM_SEARCH_PARAM_KEY).toUpperCase().trim() + "%");
		}

		if (!props.getProperty(PARAM_SEARCH_PARAM_NAME).trim().equals("")
				&& !props.getProperty(PARAM_SEARCH_PARAM_NAME).trim().equalsIgnoreCase("ALL")) {
			paramSearchDTO.setParameterName("%" + props.getProperty(PARAM_SEARCH_PARAM_NAME).toUpperCase().trim() + "%");
		}

		page = ModuleServiceLocator.getCommonServiceBD().searchBusinessSystemParameters(paramSearchDTO, recNo, 20);

		if (page != null) {
			colRoles = page.getPageData();
			totRec = page.getTotalNoOfRecords();
			request.setAttribute(WebConstants.REQ_TOTAL_RECORDS, "'" + new Integer(totRec).toString() + "';");
		}
		return createParamRowHtml(colRoles);

	}

	/**
	 * Creates the Grid array from a Collecion of BusinessSystemParameters
	 * 
	 * @param users
	 *            the collection of BusinessSystemParameters
	 * @return String the Grid Array
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static String createParamRowHtml(Collection<BusinessSystemParameter> params) throws ModuleException {

		StringBuffer sb = new StringBuffer("var paramData = new Array();");
		BusinessSystemParameter param = null;
		int tempInt = 0;

		if (params != null) {
			Iterator<BusinessSystemParameter> paramIter = params.iterator();
			while (paramIter.hasNext()) {

				param = (BusinessSystemParameter) paramIter.next();

				sb.append("paramData[" + tempInt + "] = new Array();");
				sb.append("paramData[" + tempInt + "][0] = '" + tempInt + "';");
				sb.append("paramData[" + tempInt + "][1] = '" + param.getCarrierCode() + "';");
				sb.append("paramData[" + tempInt + "][3] = '" + param.getParamKey() + "';");

				if (param.getParamValue() != null && !param.getParamValue().trim().equals("null")
						&& !param.getParamValue().trim().equals("")) {
					sb.append("paramData[" + tempInt + "][4] = '" + param.getParamValue() + "';");
				} else {
					sb.append("paramData[" + tempInt + "][4] = '&nbsp;';");
				}

				sb.append("paramData[" + tempInt + "][5] = '" + param.getParamType() + "';");
				sb.append("paramData[" + tempInt + "][6] = '" + param.getEditable() + "';");
				sb.append("paramData[" + tempInt + "][7] = '" + param.getMaxLengthParamVal() + "';");
				sb.append("paramData[" + tempInt + "][8] = " + param.getParamId() + ";");

				if (globalConfig.getBizParam(param.getParamKey(), param.getCarrierCode()) != null
						&& !globalConfig.getBizParam(param.getParamKey(), param.getCarrierCode()).equals(param.getParamValue())) {
					sb.append("paramData[" + tempInt + "][2] = '" + param.getDescription() + "  **';");
					sb.append("paramData[" + tempInt + "][9] = " + false + ";");
				} else {
					sb.append("paramData[" + tempInt + "][2] = '" + param.getDescription() + "';");
					sb.append("paramData[" + tempInt + "][9] = " + true + ";");
				}

				tempInt++;
			}
		}

		return sb.toString();
	}

	/**
	 * Gets the Client Validations For the User Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array of Client Validations
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {

		if (clientErrors == null) {
			Properties moduleErrs = new Properties();
			clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}

		return clientErrors;
	}
}
