package com.isa.thinair.airadmin.core.web.generator.inventory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airinventory.api.dto.BookingClassDTO;
import com.isa.thinair.airinventory.api.dto.SearchBCsCriteria;
import com.isa.thinair.airinventory.api.dto.SearchBCsCriteria.BookingClassTypes;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.model.GDSBookingClass;
import com.isa.thinair.airpricing.api.util.AirPricingCustomConstants;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.StaticFileNameUtil;

/**
 * @author srikantha
 * 
 */
public class BookingCodesHTMLGenerator {

	private static String clientErrors;

	/**
	 * Gets Booking class Data Grid Row for a given search critiriea
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the array containing Data Grid Array
	 * @throws ModuleException
	 *             the ModuleException
	 */
	@SuppressWarnings("unchecked")
	public final String getBookingCodesRowHtml(HttpServletRequest request) throws ModuleException {

		String strMode = request.getParameter("hdnMode");
		String strBookingClass = request.getParameter("selBookingClass");
		String strBCType = request.getParameter("selBCType");
		String strCOS = request.getParameter("selCOS");
		String strLogicalCC = request.getParameter("selLCC");
		String strAllocType = request.getParameter("selAllocSearch");
		String strStatusSearch = request.getParameter("selStatusSearch");
		Collection<BookingClassDTO> list = null;
		int recordNo = 0;
		if (request.getParameter("hdnRecNo") != null) {
			recordNo = Integer.parseInt(request.getParameter("hdnRecNo")) - 1;
		}
		Page page = null;

		int totalRecords = 0;
		String strJavascriptTotalNoOfRecs = "";
		String strBookingCodeJS = "var strBookingCode=-1;";
		String strCOSJS = "var strCOS=-1;";
		String strLogicalCCJS = "var strLogicalCC=-1;";
		String strBCTypeJS = "var strBCType=-1;";
		String strAllocTypeJS = "var strAllocType=-1;";
		String strStatus = "var strStatus=-1;";

		if (strMode != null && !strMode.equals(WebConstants.ACTION_DELETE)) {

			strBookingCodeJS = "var strBookingCode='" + strBookingClass + "';";
			strCOSJS = "var strCOS='" + strCOS + "';";
			strLogicalCCJS = "var strLogicalCC='" + strLogicalCC + "';";
			strBCTypeJS = "var strBCType='" + strBCType + "';";
			strAllocTypeJS = "var strAllocType = '" + strAllocType + "';";
			strStatus = "var strStatus = '" + strStatusSearch + "';";
			// If All selected
			if (strBookingClass != null && strBookingClass.equals("-1")) {

				SearchBCsCriteria criteria = new SearchBCsCriteria();
				if (strCOS != null && !strCOS.equals("-1")) {
					criteria.setClassOfService(strCOS);
				} else {
					criteria.setClassOfService(null);
				}
				if (strLogicalCC != null && !strLogicalCC.equals("-1")) {
					criteria.setLogicalCCCode(strLogicalCC);
				} else {
					criteria.setLogicalCCCode(null);
				}
				if (strBCType != null && !strBCType.equals("-1")) {
					criteria.setBookingClassType(strBCType);
				} else {
					criteria.setBookingClassType(BookingClassTypes.ALL);
				}
				if (strAllocType != null && !strAllocType.equals("-1")) {
					criteria.setAllocationType(strAllocType);
				} else {
					criteria.setAllocationType(null);
				}
				if (strStatusSearch != null && strStatusSearch.equalsIgnoreCase(BookingClass.Status.ACTIVE)) {
					criteria.setStatus(BookingClass.Status.ACTIVE);
				} else if (strStatusSearch != null && strStatusSearch.equalsIgnoreCase(BookingClass.Status.INACTIVE)) {
					criteria.setStatus(BookingClass.Status.INACTIVE);
				}

				criteria.setStartIndex(recordNo);
				criteria.setPageSize(20);
				page = ModuleServiceLocator.getBookingClassBD().getBookingClassDTOs(criteria);

				totalRecords = page.getTotalNoOfRecords();
				strJavascriptTotalNoOfRecs = "var totalRecords = " + totalRecords + ";";
				request.setAttribute(WebConstants.REQ_TOTAL_RECORDS, strJavascriptTotalNoOfRecs);

				request.setAttribute("BookingCode", strBookingCodeJS);
				request.setAttribute("COS", strCOSJS);
				request.setAttribute("LogicalCC", strLogicalCCJS);
				request.setAttribute("BCType", strBCTypeJS);
				request.setAttribute("ALLOCType", strAllocTypeJS);
				request.setAttribute("Status", strStatus);

				list = page.getPageData();
			} else {
				// If particular BookingClass is selected
				list = new ArrayList<BookingClassDTO>();
				BookingClassDTO bookingClassDTO = ModuleServiceLocator.getBookingClassBD().getBookingClassDTO(strBookingClass);

				// Only One Record
				if (bookingClassDTO != null) {
					list.add(bookingClassDTO);
					totalRecords = 1;
				}
				strJavascriptTotalNoOfRecs = "var totalRecords = " + totalRecords + ";";
				request.setAttribute(WebConstants.REQ_TOTAL_RECORDS, strJavascriptTotalNoOfRecs);

				request.setAttribute("BookingCode", strBookingCodeJS);
				request.setAttribute("COS", strCOSJS);
				request.setAttribute("LogicalCC", strLogicalCCJS);
				request.setAttribute("BCType", strBCTypeJS);
				request.setAttribute("ALLOCType", strAllocTypeJS);
				request.setAttribute("Status", strStatus);
			}
		} else {
			SearchBCsCriteria criteria = new SearchBCsCriteria();
			criteria.setClassOfService(null);
			criteria.setStatus(BookingClass.Status.ACTIVE);
			criteria.setBookingClassType(BookingClassTypes.ALL);
			criteria.setStartIndex(recordNo);
			criteria.setPageSize(20);

			page = ModuleServiceLocator.getBookingClassBD().getBookingClassDTOs(criteria);
			totalRecords = page.getTotalNoOfRecords();
			strJavascriptTotalNoOfRecs = "var totalRecords = " + totalRecords + ";";
			request.setAttribute(WebConstants.REQ_TOTAL_RECORDS, strJavascriptTotalNoOfRecs);

			request.setAttribute("BookingCode", strBookingCodeJS);
			request.setAttribute("COS", strCOSJS);
			request.setAttribute("LogicalCC", strLogicalCCJS);
			request.setAttribute("BCType", strBCTypeJS);
			request.setAttribute("ALLOCType", strAllocTypeJS);
			request.setAttribute("Status", "var strStatus = 'ACT';");

			list = page.getPageData();
		}
		String bcStr = createBookingCodesRowHTML(list);
		return bcStr;
	}

	/**
	 * Creates the Booking class Grid from a collection of Booking class
	 * 
	 * @param bookingClasses
	 *            the Collection of booking class
	 * @return String the created Array of Grid Data
	 */
	private String createBookingCodesRowHTML(Collection<BookingClassDTO> bookingClasses) {
		StringBuffer sb = new StringBuffer("var a = new Array();");
		int i = 0;
		for (BookingClassDTO bookingClassDTO : bookingClasses) {
			BookingClass bookingClass = bookingClassDTO.getBookingClass();
			String strBookingCodeDesc = "";
			if (bookingClass.getFareCategoryCode().equalsIgnoreCase(AirPricingCustomConstants.FareCategoryType.RESTRICTED)) {

				if (bookingClass.getBookingCodeDescription().indexOf("*") != -1) {
					strBookingCodeDesc = bookingClass.getBookingCodeDescription();
				} else {
					strBookingCodeDesc = bookingClass.getBookingCodeDescription() + " *";
				}
			} else {
				strBookingCodeDesc = bookingClass.getBookingCodeDescription();
			}

			sb.append("a[" + i + "] = new Array();");
			sb.append("a[" + i + "][1] = '" + bookingClass.getBookingCode() + "';"); // 0
			sb.append("a[" + i + "][2] = '" + strBookingCodeDesc + "';"); // 1
			sb.append("a[" + i + "][3] = '" + bookingClass.getCabinClassCode() + "';"); //
			// Service -2

			if (bookingClass.getStandardCode()) {
				sb.append("a[" + i + "][4] = 'Y';"); // 3
			} else {
				sb.append("a[" + i + "][4] = 'N';"); // 3
			}

			if (bookingClass.getNestRank() != null) {
				sb.append("a[" + i + "][5] = '" + bookingClass.getNestRank() + "';"); // 4
			} else {
				sb.append("a[" + i + "][5] = '';"); // 4
			}

			if (bookingClass.getFixedFlag()) {
				sb.append("a[" + i + "][6] = 'Y';"); // 5
			} else {
				sb.append("a[" + i + "][6] = 'N';"); // 5
			}
			// 6
			sb.append("a[" + i + "][7] = '" + AiradminUtils.getModifiedStatus(bookingClass.getStatus()) + "';");
			String strFareRuleIds = bookingClassDTO.isHasLinkedFareRules() ? "Y" : "N";
			sb.append("a[" + i + "][8] = '" + strFareRuleIds + "';"); // FareClass -7

			if (bookingClass.getRemarks() != null) {
				sb.append("a[" + i + "][9] = '" + bookingClass.getRemarks() + "';"); // 8
			} else {
				sb.append("a[" + i + "][9] = '';"); // 8
			}

			sb.append("a[" + i + "][10] = '" + bookingClass.getPaxType() + "';"); // 9
			sb.append("a[" + i + "][11] = '" + bookingClass.getVersion() + "';"); // 10
			sb.append("a[" + i + "][12] = '" + StaticFileNameUtil.getCorrected("../../images/Right.gif") + "';"); // 11

			sb.append("a[" + i + "][13] = '" + bookingClass.getCabinClassCode() + "["
					+ getCabinClassDescription(bookingClass.getCabinClassCode()) + "]" + "';"); // 12

			sb.append("a[" + i + "][14] = '';");// 13??
			sb.append("a[" + i + "][15] = '" + (bookingClassDTO.isHasLinkedAgents() ? "Y" : "N") + "';"); // 14
			sb.append("a[" + i + "][16] = '" + (bookingClassDTO.isHasLinkedInventories() ? "Y" : "N") + "';"); // 15
			// 16
			sb.append("a[" + i + "][17] = '" + (bookingClass.getBcType() == null ? "" : bookingClass.getBcType()) + "';");

			String charges = "";
			String chg = "";

			Set<String> set = bookingClass.getApplicapableCharges();

			if (set != null) {
				Iterator<String> ite = set.iterator();
				while (ite.hasNext()) {
					String str = ite.next();
					charges += str + ", ";
					chg += str + ",";
				}
			}
			if (charges.indexOf(",") != -1) {
				chg = chg.substring(0, chg.lastIndexOf(","));
				sb.append("a[" + i + "][18] = '" + charges.substring(0, charges.lastIndexOf(",")) + "';"); // 17
			} else {
				sb.append("a[" + i + "][18] = '" + charges + "';"); // 17
			}

			sb.append("a[" + i + "][19] = '" + chg + "';"); // 18

			// adding the allocation type to the Booking Class taking 22 to be in the safe side
			if (bookingClass.getAllocationType() != null) {
				sb.append("a[" + i + "][22] = '" + bookingClass.getAllocationType() + "';");
			} else {
				sb.append("a[" + i + "][22] = '';");
			}

			if (bookingClass.getPaxCategoryCode() != null) {
				sb.append("a[" + i + "][23] = '" + bookingClass.getPaxCategoryCode() + "';");
			} else {
				sb.append("a[" + i + "][23] = '';");
			}
			if (bookingClass.getFareCategoryCode() != null) {
				sb.append("a[" + i + "][24] = '" + bookingClass.getFareCategoryCode() + "';");
			} else {
				sb.append("a[" + i + "][24] = '';");
			}

			if (bookingClass.getReleaseCutover() != null) {

				sb.append("a[" + i + "][25] = '" + bookingClass.getReleaseCutover() / 60 + ":" + bookingClass.getReleaseCutover()
						% 60 + "';");
			} else {
				sb.append("a[" + i + "][25] = '';");
			}
			if (bookingClass.getReleaseFlag() != null) {
				sb.append("a[" + i + "][26] = '" + bookingClass.getReleaseFlag() + "';");
			} else {
				sb.append("a[" + i + "][26] = 'Y';");
			}
			sb.append("a[" + i + "][27] = new Array();");
			Set<GDSBookingClass> gdsBCset = bookingClass.getGdsBCs();
			if (gdsBCset != null) {
				int al = 0;
				Iterator<GDSBookingClass> iter = gdsBCset.iterator();
				while (iter.hasNext()) {
					GDSBookingClass gdsBc = iter.next();
					if(gdsBc != null) {
						sb.append("a[" + i + "][27][" + al +"] = '" + gdsBc.getGdsId() +"';");
						al++;
					}
				}
			}

			if (bookingClass.getOnHold()) {
				sb.append("a[" + i + "][28] = 'Y';"); // 5
			} else {
				sb.append("a[" + i + "][28] = 'N';");
			}

			sb.append("a[" + i + "][29] = '" + bookingClass.getLogicalCCCode() + "';");
			sb.append("a[" + i + "][30] = '" + bookingClass.getGroupId() + "';");
			i++;
		}
		return sb.toString();
	}

	/**
	 * Create the Client Validation for Booking Class page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array of Client Validations
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {

		if (clientErrors == null) {
			Properties moduleErrs = new Properties();
			moduleErrs.setProperty("um.bookingcode.form.id.required", "bookingClassIdRqrd");
			moduleErrs.setProperty("um.bookingcode.form.des.required", "bookingClassDesRqrd");
			moduleErrs.setProperty("um.bookingcode.form.classofservice.required", "classOfServiceRqrd");
			moduleErrs.setProperty("um.bookingcode.form.logicalCC.required", "logicalCCRqrd");
			moduleErrs.setProperty("um.bookingcode.form.standardclass.required", "standardClassRqrd");
			moduleErrs.setProperty("um.manageinventory.nestRank.blank", "nestRankBlank");
			moduleErrs.setProperty("um.manageinventory.select.aRow", "selectaRow");
			moduleErrs.setProperty("um.bookingcode.nestRank.nan", "nestRankNaN");
			moduleErrs.setProperty("um.bookingcode.form.BcType.rqrd", "BCTypeRqrd");
			moduleErrs.setProperty("um.bookingcode.form.AllocType.rqrd", "ALLOCTypeRqrd");
			moduleErrs.setProperty("um.bookingcode.form.relesetime.invalid", "rlseTimeInvalid");
			moduleErrs.setProperty("um.bookingcode.form.relesetime.greater", "rlseTimeGreater");
			moduleErrs.setProperty("um.bookingcode.form.BcType.invalid", "BCIdInvalid");
			clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}
		return clientErrors;
	}

	/**
	 * Gets Cabin Class description for a given Cabin class code
	 * 
	 * @param strCabinClassCode
	 *            the cabin class code
	 * @return String the Cabin class description
	 */
	private static String getCabinClassDescription(String strCabinClassCode) {
		return CommonsServices.getGlobalConfig().getActiveCabinClassesMap().get(strCabinClassCode);
	}
}
