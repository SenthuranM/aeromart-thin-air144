package com.isa.thinair.airadmin.core.web.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.web.auth.AuthorizationManager;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airsecurity.api.model.User;

public class AuthorizationFilter extends BasicFilter {
	private static Log log = LogFactory.getLog(CommonFilter.class);
	private static final AiradminConfig airadminConfig = new AiradminConfig();
	private static final List ajaxActions = setAjaxActions();

	public void doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain chain) throws ServletException, IOException {
		HttpServletRequest request = (HttpServletRequest) arg0;
		HttpSession session = request.getSession(false);
		User user = (User) session.getAttribute(WebConstants.SES_CURR_USER);

		// Get relevant URI.
		String url = request.getServletPath();

		if (log.isDebugEnabled())
			log.debug("url: " + url);
		// Invoke AuthorizationManager method to see if user can
		// access resource.
		boolean authorized = AuthorizationManager.isUserAuthorized(user, url);

		if (authorized) {
			chain.doFilter(arg0, arg1);
		} else {
			String msg = airadminConfig.getMessage("um.authorization.failed");
			msg = StringUtils.replace(msg, "#1", url);
			log.error("Authorization failed:" + user.getUserId() + ":" + url);

			if (ajaxActions.contains(url)) {
				BasicRequestHandler.sendMessage(arg1, WebConstants.MSG_ERROR, msg);
			} else {
				JavascriptGenerator.setServerError(request, msg, "", "");
				BasicRequestHandler.forward(arg0, arg1, errorPage);
			}
			return;

		}
	}

	private static final List<String> setAjaxActions() {
		List<String> ajaxActions = new ArrayList<String>();

		ajaxActions.add("/private/loadData.do");
		ajaxActions.add("/private/saveData.do");

		return ajaxActions;
	}

}
