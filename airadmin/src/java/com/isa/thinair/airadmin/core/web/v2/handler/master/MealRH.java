package com.isa.thinair.airadmin.core.web.v2.handler.master;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airmaster.api.model.Meal;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

public class MealRH {

	private static Log log = LogFactory.getLog(MealRH.class);

	public static Page<Meal> searchMeal(int pageNo, List<ModuleCriterion> criterion) {
		// call back end

		Page<Meal> page = null;
		try {
			if (criterion == null) {
				page = ModuleServiceLocator.getCommonServiceBD().getMeals((pageNo - 1) * 20, 20);
			} else {
				if (pageNo == 0) {
					pageNo = 1;
				}
				page = ModuleServiceLocator.getCommonServiceBD().getMeals((pageNo - 1) * 20, 20, criterion);
			}
		} catch (ModuleException e) {
			log.error(e);
		}
		return page;
	}

	public static boolean saveMeal(Meal meal) throws ModuleException {
		ModuleServiceLocator.getCommonServiceBD().saveMeal(meal);
		return true;
	}

	public static boolean deleteMeal(Integer mealId) throws ModuleException {
		ModuleServiceLocator.getCommonServiceBD().deleteMeal(mealId);
		return true;
	}

	public static Meal getMeal(String mealName) throws ModuleException {
		Meal meal = ModuleServiceLocator.getCommonServiceBD().getMeal(mealName);
		return meal;
	}

}
