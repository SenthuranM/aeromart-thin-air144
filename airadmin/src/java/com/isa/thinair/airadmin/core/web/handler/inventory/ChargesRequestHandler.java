package com.isa.thinair.airadmin.core.web.handler.inventory;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.inventory.ChargesHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airmaster.api.model.State;
import com.isa.thinair.airpricing.api.criteria.ChargeSearchCriteria;
import com.isa.thinair.airpricing.api.model.Charge;
import com.isa.thinair.airpricing.api.model.ChargeAgent;
import com.isa.thinair.airpricing.api.model.ChargeApplicableCharges;
import com.isa.thinair.airpricing.api.model.ChargePos;
import com.isa.thinair.airpricing.api.model.ChargeRate;
import com.isa.thinair.airpricing.api.model.ReportChargeGroup;
import com.isa.thinair.airpricing.api.service.ChargeBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

public class ChargesRequestHandler extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(ChargesRequestHandler.class);

	private static final String PARAM_CHARGE_CODE = "txtChargeCode";
	private static final String PARAM_GROUP = "hdnGroup";
	private static final String PARAM_DESCRIPTION = "hdnDesc";
	private static final String PARAM_REFUND = "hdnRefund";
	private static final String PARAM_REFUND_ONLY_FOR_MOD = "hdnRefundOnlyForMOD";
	private static final String PARAM_REFUND_WHEN_EXCHANGE = "hdnRefundWhenExchange";
	private static final String PARAM_EXCLUDE_FROM_FARE_QUOTE = "hdnExcludeFromFareQuote";
	private static final String PARAM_APPLIES_TO = "hdnApplyTo";
	private static final String PARAM_OND_SEGMENT = "hdnOndSeg";
	private static final String PARAM_CHANNEL = "hdnChannel";
	private static final String PARAM_CAT = "hdnCat";
	private static final String PARAM_POS = "hdnStation";
	private static final String PARAM_SEGMENT = "hdnSelSelected";
	private static final String PARAM_SEGMENT_SELECTED = "hdnSegments";
	private static final String PARAM_CHARGE_DATA = "hdnChargeData";
	private static final String PARAM_STATUS = "hdnStatus";
	private static final String PARAM_HDN_CC = "hdnChargeCode";
	private static final String PARAM_HDN_RATECODES = "hdnRateIDs";
	private static final String PARAM_HDN_CR_VERSIONS = "hdnRateVersion";
	private static final String PARAM_PAYTYPE = "hdnpayType";
	private static final String PARAM_JOURNEY = "hdnJourney";
	private static final String PARAM_STATIONS = "hdnStations";
	private static final String PARAM_INC = "hdnInc";
	private static final String PARAM_OND = "hdnAgent";
	private static final String PARAM_INC_OND = "hdnIncOnd";
	private static final String PARAM_AGENTS = "hdnAgents";
	private static final String PARAM_REVENUE = "chkRevenue";
	private static final String PARAM_REVENUE_VAL = "txtRevenue";
	private static final String PARAM_CURRENCYCODE = "selCurrencyCode";
	private static final String PARAM_CHKINWARDTRANSIT = "chkInwardTrn";
	private static final String PARAM_CHKOUTWARDTRANSIT = "chkOutwardTrn";
	private static final String PARAM_MINTRANSITDURATION = "hdnMinTrnDuration";
	private static final String PARAM_MAXTRANSITDURATION = "hdnMaxTrnDuration";
	private static final String PARAM_CHKALLTRN = "chkAllTrn";
	private static final String PARAM_MINDAYGAP = "txtMinNoOfDays";
	private static final String PARAM_MAXDAYGAP = "txtMaxNoOfDays";
	private static final String PARAM_CHKAIRPORT_TAX = "hdnAirportTax";
	private static final String PARAM_AIRPORT_TAX_TYPE = "hdnAirportTaxType";
	private static final String PARAM_BUNDLED_FARE_CHARGE = "hdnBundledFareCharge";
	private static final String PARAM_APPLICABLE_CHARGES = "hdnCharges";
	private static final String PARAM_SERVICE_TAX_INTER_STATE = "hdnServiceTaxInterState";
	private static final String PARAM_SERVICE_TAX_TICKETING = "hdnServiceTaxTicketing";
	private static final String PARAM_SERVICE_TAX_COUNTRY = "selCountry";
	private static final String PARAM_SERVICE_TAX_STATE = "selState";
	private static final String PARAM_SERVICE_TAX_INVOICE_CATEGORY = "selGenerateReport";

	private static AiradminConfig airadminConfig = new AiradminConfig();
	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();

	/* version */
	private static final String VERSION = "hdnVersion";

	/* mode */
	private static final String MODE = "hdnMode";

	private static final String REPORTING_GROUP = "selReportingGroup";

	private static final String PARAM_REPORT_CHARGE_GROUP_TYPE = "hdnRptChargeGroupType";

	private static final String PARAM_REPORT_CHARGE_GROUP_CODE = "txtRptChargeGroupCode";

	private static final String PARAM_REPORT_CHARGE_GROUP_DESC = "txtRptChargeGroupDesc";

	/** Transit Applicability */
	public enum Transit {
		INCLUDE('I'), EXCULDE('E'), NOT_IN_EFFECT('N'), APPLY('A'), DISCARD('D');
		private final char status;

		private Transit(char status) {
			this.status = status;
		}

		public char getTransit() {
			return status;
		}
	}

	/**
	 * Main Execute Method for CHARGE Action
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {
		String saveSuccess = "var isSaveTransactionSuccessful = false;";
		String isSuccessfulSaveMessageDisplayEnabledJS = "var isSuccessfulSaveMessageDisplayEnabled = "
				+ isSuccessfulSaveMessageDisplayEnabled() + ";";
		boolean isSaveTransactionSuccessful = false;

		String forward = AdminStrutsConstants.AdminAction.SUCCESS;
		String mode = request.getParameter(MODE);

		/* Save Action */
		try {
			if (mode != null && mode.equals(WebConstants.ACTION_SAVE)) {
				try {
					checkPrivilege(request, WebConstants.PLAN_FARES_CHARGE_ADD);
				} catch (ModuleRuntimeException mre) {
					checkPrivilege(request, WebConstants.PLAN_FARES_CHARGE_EDIT);
				}
				isSaveTransactionSuccessful = true;
				saveData(request);
				saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS), WebConstants.MSG_SUCCESS);

			}
		} catch (ModuleException ex) {
			isSaveTransactionSuccessful = false;
			saveMessage(request, ex.getMessageString(), WebConstants.MSG_ERROR);
			setChargeModelData(request);

		} catch (Exception e) {
			log.error("Error occured in ChargesRequestHandler ", e);
			setChargeModelData(request);
		}
		if (isSaveTransactionSuccessful) {
			saveSuccess = "var isSaveTransactionSuccessful = true;";
		} else {
			saveSuccess = "var isSaveTransactionSuccessful = false;";
		}

		/* delete Action */
		try {
			if (mode != null && mode.equals(WebConstants.ACTION_DELETE)) {
				checkPrivilege(request, WebConstants.PLAN_FARES_CHARGE_DELETE);
				deleteChargesData(request);
				saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_DELETE_SUCCESS), WebConstants.MSG_SUCCESS);

			}

		} catch (ModuleException ex) {
			log.error("Error in ChargesRequestHandler execute ", ex);
			saveMessage(request, ex.getMessageString(), WebConstants.MSG_ERROR);

		} catch (Exception e) {
			log.error("Error in ChargesRequestHandler execute ", e);
			// setChargeModelData(request);

		}

		try {
			setDisplayData(request);

		} catch (ModuleException ex) {
			setChargeModelData(request);
			saveMessage(request, ex.getMessageString(), WebConstants.MSG_ERROR);

		} catch (Exception e) {
			setChargeModelData(request);

		}
		request.setAttribute(WebConstants.REQ_TRANSACTION_DISPLAYSTATUS, isSuccessfulSaveMessageDisplayEnabledJS);
		request.setAttribute(WebConstants.REQ_TRANSACTION_STATUS, saveSuccess);
		request.setAttribute("reqAllowAssCookieCharges",
				"var isAllowAssCookieCharges = " + AppSysParamsUtil.isCookieBasedSurchargesApplyForIBE() + ";");

		return forward;
	}

	/**
	 * Saves the Charge Data
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	public static void saveData(HttpServletRequest request) throws Exception {

		ChargeBD chargeBD = ModuleServiceLocator.getChargeBD();
		Charge charge = new Charge();
		// SimpleDateFormat dateForamt = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Properties props = getProperties(request);

		String chargeCode = props.getProperty(PARAM_CHARGE_CODE);
		boolean updateCharge = false;
		
		if (chargeCode == null || chargeCode.equals("")) {
			chargeCode = props.getProperty(PARAM_HDN_CC).toString();
			updateCharge = true;
		} 

		charge.setChargeCode(chargeCode);
		charge.setInterState("N");
		charge.setTicketingServiceTax("N");
		charge.setChargeGroupCode(props.getProperty(PARAM_GROUP).toString());
		charge.setChargeDescription(props.getProperty(PARAM_DESCRIPTION).toString());
		String refundable = props.getProperty(PARAM_REFUND).toString();

		if (refundable.equals(WebConstants.CHECKBOX_ON)) {
			charge.setRefundableChargeFlag(Charge.REFUNDABLE_CHARGE);
		} else {
			charge.setRefundableChargeFlag(Charge.NONE_REFUNDABLE_CHARGE);
		}

		String refundableOnlyForMOD = props.getProperty(PARAM_REFUND_ONLY_FOR_MOD).toString();
		if (refundableOnlyForMOD.equals(WebConstants.CHECKBOX_ON)) {
			charge.setRefundableOnlyforModifications("Y");
		} else {
			charge.setRefundableOnlyforModifications("N");
		}

		String refundableWhenExchange = props.getProperty(PARAM_REFUND_WHEN_EXCHANGE).toString();
		if (refundableWhenExchange.equals(WebConstants.CHECKBOX_ON)) {
			charge.setRefundableWhenExchange("Y");
		} else {
			charge.setRefundableWhenExchange("N");
		}
		String excludeFromFareQuote = props.getProperty(PARAM_EXCLUDE_FROM_FARE_QUOTE).toString();
		if (excludeFromFareQuote.equals(WebConstants.CHECKBOX_ON)) {
			charge.setExcludable("Y");
		} else {
			charge.setExcludable("N");
		}

		String bundledFareCharge = props.getProperty(PARAM_BUNDLED_FARE_CHARGE).toString();
		if (bundledFareCharge.equals(WebConstants.CHECKBOX_ON)) {
			charge.setBundledFareCharge("Y");
		} else {
			if(updateCharge){
				boolean hasLinkedBundledFares = ModuleServiceLocator.getBundledFareBD().isChargeCodeLinkedToBundledFare(chargeCode);
				if(hasLinkedBundledFares){
					throw new ModuleException("airpricing.charge.linked.to.bundldedfare");
				}
			}
			charge.setBundledFareCharge("N");
		}
		
		charge.setMinFlightSearchGap((!"".equals(props.getProperty(PARAM_MINDAYGAP))) ? new Integer(props
				.getProperty(PARAM_MINDAYGAP)) : 0);
		charge.setMaxFlightSearchGap((!"".equals(props.getProperty(PARAM_MAXDAYGAP))) ? new Integer(props
				.getProperty(PARAM_MAXDAYGAP)) : 0);
		charge.setJourneyType(props.getProperty(PARAM_JOURNEY));

		String revenueChk = props.getProperty(PARAM_REVENUE).toString();

		if (revenueChk.equals(WebConstants.CHECKBOX_ON)) {
			String revenuePerc = props.getProperty(PARAM_REVENUE_VAL).toString();
			charge.setSurRevenue("Y");
			charge.setSurRevenuePerc(revenuePerc);
		} else {

			charge.setSurRevenue("N");

		}

		String appliesTo = props.getProperty(PARAM_APPLIES_TO).toString();

		if (appliesTo != null && !appliesTo.equals("")) {
			charge.setAppliesTo(Integer.parseInt(appliesTo));
		}

		String ondSegment = props.getProperty(PARAM_OND_SEGMENT).toString();
		String channelId = props.getProperty(PARAM_CHANNEL).toString();

		charge.setOndApplicability(ondSegment);
		charge.setChannelId(Integer.valueOf(channelId));

		String category = props.getProperty(PARAM_CAT).toString();
		charge.setChargeCategoryCode(category);
		String onds = props.getProperty(PARAM_SEGMENT_SELECTED);
		String rptGroup = props.getProperty(REPORTING_GROUP);
		if ("NEW".equals(props.getProperty(PARAM_REPORT_CHARGE_GROUP_TYPE))) {
			ReportChargeGroup reportChargeGroup = new ReportChargeGroup();
			reportChargeGroup.setReportChargeGroupCode(props.getProperty(PARAM_REPORT_CHARGE_GROUP_CODE));
			reportChargeGroup.setDescription(props.getProperty(PARAM_REPORT_CHARGE_GROUP_DESC));
			chargeBD.saveReportChargeGroup(reportChargeGroup);

			reportChargeGroup = chargeBD.getReportChargeGroupByRptChargeGroupCode(props
					.getProperty(PARAM_REPORT_CHARGE_GROUP_CODE));
			charge.setReportingChargeGroupId(reportChargeGroup.getReportChargeGroupID());
		}
		if (!rptGroup.equals("")) {
			charge.setReportingChargeGroupId(new Integer(rptGroup));
		}
		if (category.equals("POS")) {
			Set<String> ondSet = new HashSet<String>();
			// charge.setChargeCategoryValue(props.getProperty(PARAM_POS)
			// .toString());
			if (onds != null && !onds.equals("")) {
				String arrOnd[] = onds.split(",");
				for (int y = 0; y < arrOnd.length; y++) {
					if (arrOnd[y] != null && !arrOnd[y].equals("")) {
						ondSet.add(arrOnd[y]);
					}
				}
				if (ondSet.size() > 0) {
					charge.setOndCodes(ondSet);
				}
			}

			String stations = props.getProperty(PARAM_STATIONS).toString();
			String[] StationList = stations.split(",");
			String inc = props.getProperty(PARAM_INC).toString();
			String incVal = "";
			if (inc.equalsIgnoreCase("Include")) {
				incVal = "I";
			} else {
				incVal = "E";
			}

			for (int a = 0; a < StationList.length; a++) {
				ChargePos chargePos = new ChargePos();
				chargePos.setPos(StationList[a]);
				chargePos.setIncludeOrExclude(incVal);
				if ("I".equals(incVal)) {
					charge.setChargeCategoryValue("I");
				} else {
					charge.setChargeCategoryValue("E");
				}
				charge.addPosCharges(chargePos);
			}

			String agents = props.getProperty(PARAM_AGENTS).toString();
			String[] AgentList = agents.split(",");
			String incOnd = props.getProperty(PARAM_INC_OND).toString();
			String incValOnd = "";

			if (incOnd.equalsIgnoreCase("Include")) {
				incValOnd = "I";
			} else {
				incValOnd = "E";
			}

			for (int a = 0; a < AgentList.length; a++) {

				if (AgentList[a] != null && (!AgentList[a].equals(""))) {

					ChargeAgent chargeAgent = new ChargeAgent();
					chargeAgent.setAgentCode(AgentList[a]);
					chargeAgent.setIncludeOrExclude(incValOnd);
					if ("I".equals(incValOnd)) {
						charge.setChargeAgentFilter("I");

					} else {
						charge.setChargeAgentFilter("E");

					}
					charge.addAgentCharges(chargeAgent); // to be checked - modify if new field has to be added
				}
			}

		}
		if (category.equals("OND")) {
			Set<String> ondSet = new HashSet<String>();

			if (onds != null && !onds.equals("")) {
				String arrOnd[] = onds.split(",");
				for (int y = 0; y < arrOnd.length; y++) {
					if (arrOnd[y] != null && !arrOnd[y].equals("")) {
						ondSet.add(arrOnd[y]);
					}
				}
				if (ondSet.size() > 0) {
					charge.setOndCodes(ondSet);
				}
			}

			String agents = props.getProperty(PARAM_AGENTS).toString();
			String[] AgentList = agents.split(",");
			String inc = props.getProperty(PARAM_INC_OND).toString();
			String incVal = "";

			if (inc.equalsIgnoreCase("Include")) {
				incVal = "I";
			} else {
				incVal = "E";
			}

			for (int a = 0; a < AgentList.length; a++) {

				if (AgentList[a] != null && (!AgentList[a].equals(""))) {
					ChargeAgent chargeAgent = new ChargeAgent();
					chargeAgent.setAgentCode(AgentList[a]);
					chargeAgent.setIncludeOrExclude(incVal);
					if ("I".equals(incVal)) {
						charge.setChargeAgentFilter("I");

					} else {
						charge.setChargeAgentFilter("E");

					}
					charge.addAgentCharges(chargeAgent); // to be checked - modify if new field has to be added
				}
			}
		}
		
		if (category.equals("SERVICE_TAX")) {
			boolean countryExists = false;
			boolean stateExists = false;
			String[] chargesList = null;
			State state= null;
			String charges = props.getProperty(PARAM_APPLICABLE_CHARGES);
			if(charges != null && !"".equals(charges)){
				chargesList = charges.split(",");
			}
			
			String serviceTaxInterState = props.getProperty(PARAM_SERVICE_TAX_INTER_STATE).toString();
			String serviceTaxCountry = props.getProperty(PARAM_SERVICE_TAX_COUNTRY).toString();
			String serviceTaxState = props.getProperty(PARAM_SERVICE_TAX_STATE).toString();
			String serviceTaxInvoiceCategory = props.getProperty(PARAM_SERVICE_TAX_INVOICE_CATEGORY).toString();
			String serviceTaxTicketing = props.getProperty(PARAM_SERVICE_TAX_TICKETING).toString();
			
			if(serviceTaxCountry != null && !"".equals(serviceTaxCountry)){
				charge.setCountryCode(serviceTaxCountry);
				countryExists = true;
			} else {
				throw new ModuleException("airpricing.charge.serviceTax.country.undefined");
			}
			
			if(countryExists && serviceTaxState != null && !"".equals(serviceTaxState)){
				state = ModuleServiceLocator.getCommonServiceBD().getState(serviceTaxState.trim());
				if(state != null && state.getStateId() != null){
					charge.setStateId(state.getStateId());
					stateExists = true;
				} else {
					throw new ModuleException("airpricing.charge.serviceTax.state.undefined");
				}
			}
			
			if (stateExists && serviceTaxInterState.equals(WebConstants.CHECKBOX_ON)) {
				charge.setInterState("Y");
			}
			
			if (serviceTaxTicketing.equals(WebConstants.CHECKBOX_ON)) {
				charge.setTicketingServiceTax("Y");
			} 
			
			if(serviceTaxInvoiceCategory != null && !"".equals(serviceTaxInvoiceCategory)){
				charge.setInvoiceTaxCategory(serviceTaxInvoiceCategory);
			} else {
				throw new ModuleException("airpricing.charge.serviceTax.InvoiceMapping.undefined");
			}
			
			// validation for Service Tax			
			if(!validateServiceTax(serviceTaxCountry, state, serviceTaxInterState, serviceTaxTicketing, chargeCode)){
				if(countryExists && !stateExists){
					throw new ModuleException("airpricing.charge.serviceTax.exists.country");
				} else if(stateExists && !serviceTaxInterState.equals(WebConstants.CHECKBOX_ON)){
					throw new ModuleException("airpricing.charge.serviceTax.exists.state");
				} else if(serviceTaxInterState.equals(WebConstants.CHECKBOX_ON)){
					throw new ModuleException("airpricing.charge.serviceTax.exists.interState");
				}
			}
			
			if (chargesList != null && chargesList.length > 0) {
				for (int a = 0; a < chargesList.length; a++) {

					if (chargesList[a] != null && (!chargesList[a].equals(""))) {
						ChargeApplicableCharges applicableCharge = new ChargeApplicableCharges();
						applicableCharge.setApplicableChargeCode(chargesList[a]);
						applicableCharge.setChargeCode(chargeCode);
						applicableCharge.setApplyStatus("E"); // TODO at the moment only excluded charges are allowing
						charge.addApplicableCharges(applicableCharge);
					}
				}
				charge.setChargeCodeFilter("E");// TODO at the moment only excluded charges are allowing and if not set as null
			} else {
				charge.setApplicableCharges(new HashSet<ChargeApplicableCharges>());
				charge.setChargeCodeFilter(null);// TODO at the moment only excluded charges are allowing and if not set as null
			}

		}

		boolean isAirportTax = WebConstants.CHECKBOX_ON.equals(props.getProperty(PARAM_CHKAIRPORT_TAX));

		if (isAirportTax) {
			charge.setAirportTaxCategory(props.getProperty(PARAM_AIRPORT_TAX_TYPE));
		}

		String version = props.getProperty(VERSION).toString();
		if (version != null && !"".equals(version)) {
			charge.setVersion(Long.parseLong(version));
		}
		// adding status
		String paymentType = props.getProperty(PARAM_PAYTYPE);
		if (paymentType != null) {
			charge.setDepartureOrArival(paymentType.toCharArray()[0]);
		}

		if (props.getProperty(PARAM_STATUS).toString().equals("on")) {
			charge.setStatus(Charge.STATUS_ACTIVE);
		} else {
			charge.setStatus(Charge.STATUS_INACTIVE);
		}

		Set<ChargeRate> colRates = new HashSet<ChargeRate>();

		String rates = props.getProperty(PARAM_CHARGE_DATA).toString();

		String arrRates[] = rates.split(";");
		String rateVersions = props.getProperty(PARAM_HDN_CR_VERSIONS).toString();
		String rateCodes = props.getProperty(PARAM_HDN_RATECODES).toString();

		String arrVersionRates[] = null;
		if (rateVersions != null && !rateVersions.equals("")) {
			String versions = rateVersions.substring(0, rateVersions.lastIndexOf(","));
			arrVersionRates = versions.split(",");
		}

		String arrrateCodes[] = null;
		if (rateCodes != null && !rateCodes.equals("")) {
			String codes = rateCodes.substring(0, rateCodes.lastIndexOf(","));
			arrrateCodes = codes.split(",");
		}

		for (int t = 0; t < arrRates.length; t++) {
			ChargeRate rate = new ChargeRate();
			String dataRow[] = arrRates[t].split(",");
			if (dataRow != null) {
				if (dataRow.length > 0) {
					if (!dataRow[0].equals("")) {

						String effFrom = dataRow[0];
						rate.setChargeCode(chargeCode);
						rate.setChargeEffectiveFromDate(dateFormat.parse(effFrom + ":00"));
						String effTo = dataRow[1];
						rate.setChargeEffectiveToDate(dateFormat.parse(effTo + ":59"));

						if (dataRow[3] != null && !dataRow[3].equals("")) {
							rate.setChargeValuePercentage(new Double(dataRow[3]));
						} else {
							rate.setChargeValuePercentage(new Double(0));
						}
						if (dataRow[4] != null && !dataRow[4].equals("")) {
							rate.setValueInLocalCurrency(new Double(dataRow[4]));
						}

						String active = dataRow[5];
						// active="on";
						String flag = dataRow[2];

						if (active.equals("true")) {
							rate.setStatus(ChargeRate.Status_Active);
						} else {
							rate.setStatus(ChargeRate.Status_InActive);
						}

						rate.setValuePercentageFlag(dataRow[2]);
						
						//added restriction for service taxes
						if(!category.equals("SERVICE_TAX") && dataRow[8].equals(ChargeRate.CHARGE_BASIS_PTFST)){
							throw new ModuleException("airpricing.charge.chargeRate.flag.PTFST.notAllowed");
						} else if(category.equals("SERVICE_TAX") && !dataRow[8].equals(ChargeRate.CHARGE_BASIS_PTFST)){
							throw new ModuleException("airpricing.charge.chargeRate.flag.PTFST.allowed");
						}
						
						if (!flag.equals(ChargeRate.CHARGE_BASIS_V)) {
							// should remove when removing value percentage
							// flag
							if (dataRow[8] != null && !dataRow[8].equals("")) {
								rate.setChargeBasis(dataRow[8]);
							}

						} else {
							rate.setChargeBasis(ChargeRate.CHARGE_BASIS_V);
						}

						if (dataRow[6] != null && !dataRow[6].equals("")) {
							rate.setChargeValueMin(new BigDecimal(dataRow[6]));
						}
						if (dataRow[7] != null && !dataRow[7].equals("")) {
							rate.setChargeValueMax(new BigDecimal(dataRow[7]));
						}

						// set rate type whether create , modify or all
						String strOptType = dataRow[9];
						rate.setOperationType(strOptType != null ? Integer.valueOf(strOptType) : 0);
						// set rate type whether create , modify or all
						String strJourneyType = dataRow[10];
						rate.setJourneyType(strJourneyType != null ? Integer.valueOf(strJourneyType) : 0);

						String cabinClassCode = dataRow[11];
						rate.setCabinClassCode(cabinClassCode.equals("-1") ? null : cabinClassCode);

						if (StringUtils.isNotEmpty(dataRow[12])) {
							if (!rate.getChargeBasis().equals(ChargeRate.CHARGE_BASIS_V)) {
								rate.setBreakPoint(new BigDecimal(dataRow[12]));
							}
						}

						if (StringUtils.isNotEmpty(dataRow[13])) {
							if (!rate.getChargeBasis().equals(ChargeRate.CHARGE_BASIS_V)) {
								rate.setBoundryValue(new BigDecimal(dataRow[13]));
							}
						}

						if (arrVersionRates != null) {
							if (arrVersionRates.length > t) {
								rate.setVersion(Long.parseLong(arrVersionRates[t]));
							}
						}

						if (arrrateCodes != null) {
							if (arrrateCodes.length > t) {
								rate.setChargeRateCode(Integer.parseInt(arrrateCodes[t]));
							}
						}

						if (isShowChargesInLocalCurr().equalsIgnoreCase("Y") && !props.getProperty(PARAM_CURRENCYCODE).equals("")
								&& !props.getProperty(PARAM_CURRENCYCODE).equals("-1")) {
							rate.setCurrencyCode(props.getProperty(PARAM_CURRENCYCODE).trim());
						}

						String salesFrom = dataRow[14];
						String salesTo = dataRow[15];

						if (!StringUtil.isNullOrEmpty(salesFrom) && !StringUtil.isNullOrEmpty(salesTo)) {
							rate.setSalesFromDate(dateFormat.parse(salesFrom + ":00"));
							rate.setSaleslToDate(dateFormat.parse(salesTo + ":59"));
						} else {
							rate.setSalesFromDate(dateFormat.parse(effFrom + ":00"));
							rate.setSaleslToDate(dateFormat.parse(effTo + ":59"));
						}

						colRates.add(rate);
					}
				}
			}
		}

		// set transit flags
		if (props.getProperty(PARAM_CHKINWARDTRANSIT).equals("I")) {
			charge.setHasInwardTransit(Transit.INCLUDE.getTransit());
		} else if (props.getProperty(PARAM_CHKINWARDTRANSIT).equals("E")) {
			charge.setHasInwardTransit(Transit.EXCULDE.getTransit());
		} else {
			charge.setHasInwardTransit(Transit.NOT_IN_EFFECT.getTransit());
		}

		if (props.getProperty(PARAM_CHKOUTWARDTRANSIT).equals("I")) {
			charge.setHasOutwardTransit(Transit.INCLUDE.getTransit());
		} else if (props.getProperty(PARAM_CHKOUTWARDTRANSIT).equals("E")) {
			charge.setHasOutwardTransit(Transit.EXCULDE.getTransit());
		} else {
			charge.setHasOutwardTransit(Transit.NOT_IN_EFFECT.getTransit());
		}

		if (props.getProperty(PARAM_CHKALLTRN).equals("I")) {
			charge.setHasTransit(Transit.INCLUDE.getTransit());
		} else if (props.getProperty(PARAM_CHKALLTRN).equals("E")) {
			charge.setHasTransit(Transit.EXCULDE.getTransit());
		} else {
			charge.setHasTransit(Transit.NOT_IN_EFFECT.getTransit());
		}

		if (props.getProperty(PARAM_MINTRANSITDURATION) != null && !props.getProperty(PARAM_MINTRANSITDURATION).equals("")) {
			String[] arrMinTrnTime = props.getProperty(PARAM_MINTRANSITDURATION).split(":");
			long minutes = Long.parseLong(arrMinTrnTime[0]);
			long lngMinTrnxTime = (minutes * 60 * 1000);
			charge.setMinTransitTime(lngMinTrnxTime);
		}

		if (props.getProperty(PARAM_MAXTRANSITDURATION) != null && !props.getProperty(PARAM_MAXTRANSITDURATION).equals("")) {
			String[] arrTrnTime = props.getProperty(PARAM_MAXTRANSITDURATION).split(":");
			long minutes = Long.parseLong(arrTrnTime[0]);
			long lngTrnxTime = (minutes * 60 * 1000);
			if (lngTrnxTime != 0) {
				charge.setMaxTransitTime(lngTrnxTime);
			}
		}

		if (colRates.size() > 0) {
			charge.setChargeRates(colRates);
		}

		chargeBD.updateCharge(charge);

	}

	private static Date getApplicableFromDate(SimpleDateFormat dateFormat, String effectiveDate) throws ParseException {
		Date applicableDate = dateFormat.parse(effectiveDate + ":00");

		Date applicableStartDate = CalendarUtil.getStartTimeOfDate(applicableDate);
		Date currentStateDate = CalendarUtil.getStartTimeOfDate(new Date());

		if (applicableStartDate.compareTo(currentStateDate) == 0) {
			return CalendarUtil.getStartTimeOfDate(applicableDate);
		} else {
			return applicableDate;
		}
	}

	private static String isShowChargesInLocalCurr() {
		String showFareDefByDepCountryCurrency = globalConfig.getBizParam(SystemParamKeys.SHOW_FARE_DEF_BY_DEP_COUNTRY_CURRENCY);
		return (showFareDefByDepCountryCurrency != null && showFareDefByDepCountryCurrency.equalsIgnoreCase("Y")) ? "Y" : "N";
	}

	/**
	 * Sets the Display data for CHARGE Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static void setDisplayData(HttpServletRequest request) throws ModuleException {

		try {

			setSystemDate(request);
			setClientErrors(request);
			setChargeCodeList(request);
			setGroupList(request);
			setGroupWithDisplayList(request);
			setChargeAppliesToDisplayList(request);
			setCategoryList(request);
			setChargesRowHtml(request);
			setModifyChargesRowHtml(request);
			setAirportList(request);
			setReportingGroup(request);
			setStationList(request);
			setAgentList(request);
			setCurrencyCodeList(request);
			setBundledFaresEnabled(request);
			setExcludeChargesFromFareQuote(request);
			setCountryList(request);
			setCountryStateMap(request);
			setChargesWithOutServiceTax(request);
			setCountryServiceTaxCategoryReportList(request);

		} catch (Exception e) {
			log.error("Error", e);
		}
	}

	/**
	 * Sets the Currency List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setCurrencyCodeList(HttpServletRequest request) {

		try {

			String strCurrencyCodeHtml = SelectListGenerator.createCurrencyList();
			request.setAttribute(WebConstants.REQ_HTML_CURRENCY_LIST, strCurrencyCodeHtml);

		} catch (ModuleException moduleException) {
			log.error(
					"Exception in ChargesRequestHandler:setCurrencyCodeList() [origin module=" + moduleException.getModuleDesc()
							+ "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Reporting Group to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setReportingGroup(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createReporingChargeGroups();
		request.setAttribute(WebConstants.REQ_REPORTING_GROUP, strHtml);
	}

	/**
	 * Sets the Server Date to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setSystemDate(HttpServletRequest request) throws ModuleException {
		String strHtml = JavascriptGenerator.getSystemDate();
		request.setAttribute(WebConstants.REQ_SYSTEM_DATE, strHtml);
		setCurrentDate(request);
	}

	/**
	 * Sets the Charge Model Data
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setChargeModelData(HttpServletRequest request) {
		String html = ChargesHTMLGenerator.getChargesData(request);
		request.setAttribute(WebConstants.REQ_MODEL_DATA, html);
	}

	/**
	 * Delete the Charge
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void deleteChargesData(HttpServletRequest request) throws ModuleException {
		Properties props = getProperties(request);
		ChargeBD chargeBD = ModuleServiceLocator.getChargeBD();
		String version = props.getProperty(VERSION);
		String cc = props.getProperty(PARAM_HDN_CC).toString();
		long lgVersion = 0;
		if (version != null && !"".equals(version)) {
			lgVersion = Long.parseLong(version);
		}
		chargeBD.deleteCharge(cc, lgVersion);
		request.setAttribute(WebConstants.REQ_DELETE_MSG, "isDelete=true");
	}

	/**
	 * Sets the Client Validations For Charge Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setClientErrors(HttpServletRequest request) throws ModuleException {
		String strClientErrors = ChargesHTMLGenerator.getClientErrors(request);
		if (strClientErrors == null) {
			strClientErrors = "";
		}
		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
	}

	/**
	 * Sets the Chrage Code List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setChargeCodeList(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createChargeCodeList();
		request.setAttribute(WebConstants.REQ_CHARGE_CODE_LIST, strHtml);
	}

	/**
	 * Sets the Charge Group List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setGroupList(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createChargeGroupList();
		request.setAttribute(WebConstants.REQ_GROUP_LIST, strHtml);
	}

	/**
	 * Sets the Group with Display List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setGroupWithDisplayList(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createChargeGroupWithDisplayList();
		request.setAttribute(WebConstants.REQ_GROUP_STATUS_LIST, strHtml);
	}

	/**
	 * Sets the charge applies to list to the request
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setChargeAppliesToDisplayList(HttpServletRequest request) throws ModuleException {
		// String strHtml = SelectListGenerator.createChargeAppliesToDisplayList();
		String strHtml = SelectListGenerator.createChargeAppliesToListNoTags();

		request.setAttribute(WebConstants.REQ_CHARGE_APPLIES_TO_LIST, strHtml);
	}

	/**
	 * Sets the Category list to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setCategoryList(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createChargeCategoryList();
		request.setAttribute(WebConstants.REQ_CATEGORY_LIST, strHtml);
	}

	/**
	 * Sets the Charge Row to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setChargesRowHtml(HttpServletRequest request) throws ModuleException {
		ChargesHTMLGenerator HTML = new ChargesHTMLGenerator();
		String strHtml = HTML.getChargesRowHtml(request);
		request.setAttribute(WebConstants.REQ_HTML_CHARGES_GRID, strHtml);
	}

	/**
	 * Sets the Modify Charge Row Html to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setModifyChargesRowHtml(HttpServletRequest request) throws ModuleException {
		ChargesHTMLGenerator HTML = new ChargesHTMLGenerator();
		String strHtml = HTML.getModifyChargesRowHtml(request);
		request.setAttribute(WebConstants.REQ_HTML_MODIFY_CHARGES_GRID, strHtml);
	}

	/**
	 * Sets the Stations with Status to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setStationList(HttpServletRequest request) throws ModuleException {
		String strHtml = JavascriptGenerator.createStationHtml();
		request.setAttribute(WebConstants.REQ_HTML_ACTIVE_STATION_SELECT_LIST, strHtml);
	}

	/**
	 * Sets the Stations with Status to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setAgentList(HttpServletRequest request) throws ModuleException {
		String strHtml = JavascriptGenerator.createAgentListHtml();
		request.setAttribute(WebConstants.REQ_HTML_AGENT_LIST, strHtml);
	}
	
	/**
	 * Sets the Charges without Service Tax to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setChargesWithOutServiceTax(HttpServletRequest request) throws ModuleException {
		String strHtml = JavascriptGenerator.createChargesWithOutServiceTaxListHtml();
		request.setAttribute(WebConstants.REQ_HTML_CHARGES_LIST, strHtml);
	}
	
	/**
	 * Sets the Charges without Service Tax to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setCountryServiceTaxCategoryReportList(HttpServletRequest request) throws ModuleException {
		String strHtml = JavascriptGenerator.createCountryWiseServiceTaxCategoryListHTML();
		request.setAttribute(WebConstants.REQ_HTML_SERVICE_TAX_REPORT_CATEGORY, strHtml);
	}

	/**
	 * Sets the Airport With Status to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setAirportList(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createAirportsWithStatusList();
		request.setAttribute(WebConstants.REQ_HTML_ACTIVE_AIRPORT_SELECT_LIST, strHtml);
	}

	private static boolean isSuccessfulSaveMessageDisplayEnabled() {
		boolean isMessageEnabled = false;
		if (airadminConfig.getMessage("um.airadmin.savemessage.display").equals("true")) {
			isMessageEnabled = true;
		}
		return isMessageEnabled;
	}

	/**
	 * Grts the CHARGE Properties from the Request Data
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return Properties the property file with CHARGE DATA
	 */
	protected static Properties getProperties(HttpServletRequest request) {
		Properties props = new Properties();

		String chargeCode = AiradminUtils.getNotNullString(request.getParameter(PARAM_CHARGE_CODE));
		String hdnChargeCode = AiradminUtils.getNotNullString(request.getParameter(PARAM_HDN_CC));
		String group = AiradminUtils.getNotNullString(request.getParameter(PARAM_GROUP));
		String desc = AiradminUtils.getNotNullString(request.getParameter(PARAM_DESCRIPTION));
		String refund = AiradminUtils.getNotNullString(request.getParameter(PARAM_REFUND));
		String refundOnlyForMOD = AiradminUtils.getNotNullString(request.getParameter(PARAM_REFUND_ONLY_FOR_MOD));
		String refundWhenExchange = AiradminUtils.getNotNullString(request.getParameter(PARAM_REFUND_WHEN_EXCHANGE));
		String excludeFromFareQuote = AiradminUtils.getNotNullString(request.getParameter(PARAM_EXCLUDE_FROM_FARE_QUOTE));
		String appliesTo = AiradminUtils.getNotNullString(request.getParameter(PARAM_APPLIES_TO));
		String cat = AiradminUtils.getNotNullString(request.getParameter(PARAM_CAT));
		String selectedSegments = AiradminUtils.getNotNullString(request.getParameter(PARAM_SEGMENT_SELECTED));
		String pos = AiradminUtils.getNotNullString(request.getParameter(PARAM_POS));
		String segment = AiradminUtils.getNotNullString(request.getParameter(PARAM_SEGMENT));
		String chargeData = AiradminUtils.getNotNullString(request.getParameter(PARAM_CHARGE_DATA));
		String version = AiradminUtils.getNotNullString(request.getParameter(VERSION));
		String status = AiradminUtils.getNotNullString(request.getParameter(PARAM_STATUS));
		String rateVersions = AiradminUtils.getNotNullString(request.getParameter(PARAM_HDN_CR_VERSIONS));
		String rateCodes = AiradminUtils.getNotNullString(request.getParameter(PARAM_HDN_RATECODES));
		String payType = AiradminUtils.getNotNullString(request.getParameter(PARAM_PAYTYPE));
		String rptGroup = AiradminUtils.getNotNullString(request.getParameter(REPORTING_GROUP));
		String ondSegment = AiradminUtils.getNotNullString(request.getParameter(PARAM_OND_SEGMENT));
		String channelId = AiradminUtils.getNotNullString(request.getParameter(PARAM_CHANNEL));
		String journey = AiradminUtils.getNotNullString(request.getParameter(PARAM_JOURNEY));
		String stations = AiradminUtils.getNotNullString(request.getParameter(PARAM_STATIONS));
		String inc = AiradminUtils.getNotNullString(request.getParameter(PARAM_INC));

		String agents = AiradminUtils.getNotNullString(request.getParameter(PARAM_AGENTS));
		String incOnd = AiradminUtils.getNotNullString(request.getParameter(PARAM_INC_OND));
		String ond = AiradminUtils.getNotNullString(request.getParameter(PARAM_OND));

		String chkRev = AiradminUtils.getNotNullString(request.getParameter(PARAM_REVENUE));
		String txtRevenue = AiradminUtils.getNotNullString(request.getParameter(PARAM_REVENUE_VAL));
		String currencyCode = AiradminUtils.getNotNullString(request.getParameter(PARAM_CURRENCYCODE));
		String inwardTrn = AiradminUtils.getNotNullString(request.getParameter(PARAM_CHKINWARDTRANSIT));
		String outwardTrn = AiradminUtils.getNotNullString(request.getParameter(PARAM_CHKOUTWARDTRANSIT));
		String minTrnDuration = AiradminUtils.getNotNullString(request.getParameter(PARAM_MINTRANSITDURATION));
		String maxTrnDuration = AiradminUtils.getNotNullString(request.getParameter(PARAM_MAXTRANSITDURATION));
		String trnAll = AiradminUtils.getNotNullString(request.getParameter(PARAM_CHKALLTRN));

		String airportTax = AiradminUtils.getNotNullString(request.getParameter(PARAM_CHKAIRPORT_TAX));
		String airportTaxType = AiradminUtils.getNotNullString(request.getParameter(PARAM_AIRPORT_TAX_TYPE));
		String reportChargeGroupType = AiradminUtils.getNotNullString(request.getParameter(PARAM_REPORT_CHARGE_GROUP_TYPE));
		String reportChargeGroupCode = AiradminUtils.getNotNullString(request.getParameter(PARAM_REPORT_CHARGE_GROUP_CODE));
		String reportChargeGroupDescription = AiradminUtils
				.getNotNullString(request.getParameter(PARAM_REPORT_CHARGE_GROUP_DESC));
		String minFlightSearchGapInDays = AiradminUtils.getNotNullString(request.getParameter(PARAM_MINDAYGAP));
		String maxFlightSearchGapInDays = AiradminUtils.getNotNullString(request.getParameter(PARAM_MAXDAYGAP));
		String bundledFareCharge = AiradminUtils.getNotNullString(request.getParameter(PARAM_BUNDLED_FARE_CHARGE));
		
		String applicableCharges = AiradminUtils.getNotNullString(request.getParameter(PARAM_APPLICABLE_CHARGES));
		String serviceTaxInterState = AiradminUtils.getNotNullString(request.getParameter(PARAM_SERVICE_TAX_INTER_STATE));
		String serviceTaxCountry = AiradminUtils.getNotNullString(request.getParameter(PARAM_SERVICE_TAX_COUNTRY));
		String serviceTaxState = AiradminUtils.getNotNullString(request.getParameter(PARAM_SERVICE_TAX_STATE));
		String serviceTaxInvoiceCategory = AiradminUtils.getNotNullString(request.getParameter(PARAM_SERVICE_TAX_INVOICE_CATEGORY));
		String serviceTaxTicketing = AiradminUtils.getNotNullString(request.getParameter(PARAM_SERVICE_TAX_TICKETING));

		props.setProperty(PARAM_CHARGE_CODE, chargeCode.trim());
		props.setProperty(PARAM_GROUP, group.trim());
		props.setProperty(PARAM_DESCRIPTION, desc.trim());
		props.setProperty(PARAM_REFUND, refund.trim());
		props.setProperty(PARAM_REFUND_ONLY_FOR_MOD, refundOnlyForMOD.trim());
		props.setProperty(PARAM_REFUND_WHEN_EXCHANGE, refundWhenExchange.trim());
		props.setProperty(PARAM_EXCLUDE_FROM_FARE_QUOTE, excludeFromFareQuote.trim());
		props.setProperty(PARAM_APPLIES_TO, appliesTo.trim());
		props.setProperty(PARAM_OND_SEGMENT, ondSegment.trim());
		props.setProperty(PARAM_CHANNEL, channelId.trim());
		props.setProperty(PARAM_CAT, cat.trim());
		props.setProperty(PARAM_SEGMENT_SELECTED, selectedSegments.trim());
		props.setProperty(PARAM_POS, pos.trim());
		props.setProperty(PARAM_SEGMENT, segment.trim());
		props.setProperty(PARAM_CHARGE_DATA, chargeData.trim());
		props.setProperty(PARAM_JOURNEY, journey.trim());
		props.setProperty(VERSION, version.trim());
		props.setProperty(PARAM_STATUS, status.trim());
		props.setProperty(PARAM_HDN_CC, hdnChargeCode);
		props.setProperty(PARAM_HDN_CR_VERSIONS, rateVersions.trim());
		props.setProperty(PARAM_HDN_RATECODES, rateCodes.trim());
		props.setProperty(PARAM_PAYTYPE, payType.trim());
		props.setProperty(REPORTING_GROUP, rptGroup.trim());
		props.setProperty(PARAM_STATIONS, stations);
		props.setProperty(PARAM_INC, inc);
		props.setProperty(PARAM_AGENTS, agents);
		props.setProperty(PARAM_INC_OND, incOnd);
		props.setProperty(PARAM_OND, ond.trim());
		props.setProperty(PARAM_REVENUE, chkRev);
		props.setProperty(PARAM_REVENUE_VAL, txtRevenue.trim());
		props.setProperty(PARAM_CURRENCYCODE, currencyCode.trim());
		props.setProperty(PARAM_CHKINWARDTRANSIT, inwardTrn);
		props.setProperty(PARAM_CHKOUTWARDTRANSIT, outwardTrn);
		props.setProperty(PARAM_MINTRANSITDURATION, minTrnDuration);
		props.setProperty(PARAM_MAXTRANSITDURATION, maxTrnDuration);
		props.setProperty(PARAM_CHKALLTRN, trnAll);
		props.setProperty(PARAM_CHKAIRPORT_TAX, airportTax);
		props.setProperty(PARAM_AIRPORT_TAX_TYPE, airportTaxType);
		props.setProperty(PARAM_REPORT_CHARGE_GROUP_TYPE, reportChargeGroupType);
		props.setProperty(PARAM_REPORT_CHARGE_GROUP_CODE, reportChargeGroupCode);
		props.setProperty(PARAM_REPORT_CHARGE_GROUP_DESC, reportChargeGroupDescription);
		props.setProperty(PARAM_MINDAYGAP, minFlightSearchGapInDays);
		props.setProperty(PARAM_MAXDAYGAP, maxFlightSearchGapInDays);
		props.setProperty(PARAM_BUNDLED_FARE_CHARGE, bundledFareCharge);
		props.setProperty(PARAM_APPLICABLE_CHARGES, applicableCharges);
		props.setProperty(PARAM_SERVICE_TAX_INTER_STATE, serviceTaxInterState);
		props.setProperty(PARAM_SERVICE_TAX_COUNTRY, serviceTaxCountry);
		props.setProperty(PARAM_SERVICE_TAX_STATE, serviceTaxState);
		props.setProperty(PARAM_SERVICE_TAX_INVOICE_CATEGORY, serviceTaxInvoiceCategory);
		props.setProperty(PARAM_SERVICE_TAX_TICKETING, serviceTaxTicketing);
		return props;
	}

	private static void setCurrentDate(HttpServletRequest request) throws ModuleException {
		Date date = CalendarUtil.getCurrentSystemTimeInZulu();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		String strHTML = sdf.format(date);
		request.setAttribute(WebConstants.CHARGES_SYS_DATE, strHTML);
	}

	private static void setBundledFaresEnabled(HttpServletRequest request) throws ModuleException {
		String varIsBundledFares = "var isBundledFaresEnabled = ";
		if (hasPrivilege(request, WebConstants.PLAN_FARES_CREATE_BUNDLED_FARE_CHARGE)) {
			varIsBundledFares += " true; ";
		} else {
			varIsBundledFares += " false; ";
		}

		request.setAttribute(WebConstants.REQ_BUNDLED_FARES_ENABLED, varIsBundledFares);
	}

	private static void setExcludeChargesFromFareQuote(HttpServletRequest request) {
		if (hasPrivilege(request, WebConstants.PLAN_FARES_CHARGE_DEFINE_EXCLUDE)) {
			request.setAttribute("enableExcludeChargesFromFQ", "true");
		} else {
			request.setAttribute("enableExcludeChargesFromFQ", "false");
		}
	}
	
	/**
	 * Sets country list to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setCountryList(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createCountryList();
		request.setAttribute(WebConstants.REQ_HTML_COUNTRY_LIST, strHtml);
	}
	
	/**
	 * Sets the Country to State Map to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setCountryStateMap(HttpServletRequest request) {

		try {
			String strHtml = SelectListGenerator.createCountryState();
			request.setAttribute(WebConstants.REQ_HTML_COUNTRY_STATE_LIST, strHtml);

		} catch (ModuleException moduleException) {
			log.error("Exception in StationHTMLGenerator.setCountryStateMap() [origin module=" + moduleException.getModuleDesc()
					+ "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}
	
	private static boolean validateServiceTax(String serviceTaxCountry, State state, String serviceTaxInterState,
			String serviceTaxTicketing, String chargeCode) throws ModuleException {
		
		boolean validate = true;
		boolean validateServiceTax = false;
		ChargeBD chargeBD = ModuleServiceLocator.getChargeBD();
		ChargeSearchCriteria chargeSearchCriteria = new ChargeSearchCriteria();

	
		Charge existingCharge = chargeBD.getCharge(chargeCode);
		if (existingCharge == null) {
			validateServiceTax = true;
		} else {
			if (!existingCharge.getCountryCode().equals(serviceTaxCountry)
					|| ((existingCharge.getStateId() == null && state != null) || (existingCharge.getStateId() != null && state == null) || (existingCharge.getStateId() != null && state != null && !existingCharge.getStateId().equals(state.getStateId()))
					|| !existingCharge.getInterState().equals(serviceTaxInterState.equals(WebConstants.CHECKBOX_ON) ? "Y" : "N"))
					|| !existingCharge.getTicketingServiceTax().equals(
							serviceTaxTicketing.equals(WebConstants.CHECKBOX_ON) ? "Y" : "N")) {
				validateServiceTax = true;
			}
		}
		if (validateServiceTax) {
			chargeSearchCriteria.setCountryCode(serviceTaxCountry);
			if (state != null && state.getStateId() != null) {
				chargeSearchCriteria.setStateId(state.getStateId());
				if (serviceTaxInterState.equals(WebConstants.CHECKBOX_ON)) {
					chargeSearchCriteria.setInterState("Y");
				} else {
					chargeSearchCriteria.setInterState("N");
				}
			} else {
				chargeSearchCriteria.setStateId(null);
				chargeSearchCriteria.setInterState("N");
			}
			if (serviceTaxTicketing.equals(WebConstants.CHECKBOX_ON)) {
				chargeSearchCriteria.setTicketingServiceTax("Y");
			} else {
				chargeSearchCriteria.setTicketingServiceTax("N");
			}

			try {
				Charge charge = chargeBD.getCharge(chargeSearchCriteria);
				if (charge != null) {
					validate = false;
				}
			} catch (ModuleException e) {
				log.error("Error in validateServiceTax execute ", e);
			}
		}
		return validate;
	}

}
