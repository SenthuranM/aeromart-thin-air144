package com.isa.thinair.airadmin.core.web.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.util.Constants;

public class CaptchaFilter extends BasicFilter {

	private static Log log = LogFactory.getLog(CaptchaFilter.class);

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain filterChain) throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) req;
		String userIP = getIpAddress(request);

		try {
			if (AppSysParamsUtil.isCaptchaEnabledForAirAdminLogin()
					&& ModuleServiceLocator.getSecurityBD().isCaptchaEnableForUser(userIP)) {

				try {
					boolean isCaptchaValidated = true;
					isCaptchaValidated = (boolean) request.getSession().getAttribute(WebConstants.ADMIN_LOGIN_CAPTCHA);

					if (!isCaptchaValidated) {
						BasicRequestHandler.forward(req, res, errorPage);
						return;
					}
				} catch (NullPointerException e) {
					log.error(e);
				}

			}
		} catch (Exception e) {
			JavascriptGenerator.setServerError(request, e.getMessage(), "", "");
			log.error(e);
			BasicRequestHandler.forward(req, res, errorPage);
			return;
		}

		filterChain.doFilter(req, res);

	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {

	}

	@Override
	public void destroy() {

	}

	private String getIpAddress(HttpServletRequest request) {
		String ip = request.getHeader("X-Forwarded-For");

		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_CLIENT_IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}

		if (ip != null && !ip.trim().isEmpty()) {
			String[] clientIpArray = ip.split(Constants.COMMA_SEPARATOR);
			if (clientIpArray.length > 0) {
				for (String ipAddress : clientIpArray) {
					if (ipAddress != null && !ipAddress.trim().isEmpty()) {
						ip = ipAddress.trim();
						break;
					}
				}
			}
		}

		return ip;
	}

}
