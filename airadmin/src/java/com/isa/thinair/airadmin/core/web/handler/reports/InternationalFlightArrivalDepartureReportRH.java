package com.isa.thinair.airadmin.core.web.handler.reports;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.util.AirTravelAgentConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

public class InternationalFlightArrivalDepartureReportRH extends BasicRequestHandler{
	private static Log log = LogFactory.getLog(InternationalFlightArrivalDepartureReportRH.class);
	
	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();
	private static final String PARAM_AGENTS = "hdnAgents";
	
	public static String execute(HttpServletRequest request, HttpServletResponse response) {
		String strHdnMode = request.getParameter("hdnMode");
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;
		setAttribInRequest(request, "displayAgencyMode", "-1");
		setAttribInRequest(request, "currentAgentCode", "");
		try {
			setDisplayData(request);
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				return null;
			}
		} catch (ModuleException e) {
			
		}
		return forward;
	}
	
	private static void setDisplayData(HttpServletRequest request) throws ModuleException {
		setDisplayAgencyMode(request);
		setClientErrors(request);
		setGSAMultiSelectList(request);
		setAgentTypes(request);
	}
	
	private static void setDisplayAgencyMode(HttpServletRequest request) throws ModuleException {

		String strCrrAgentType = AppSysParamsUtil.getCarrierAgent();
		String userId = request.getUserPrincipal().getName();
		Agent currentAgent = null;
		
		if (userId != null && !userId.equals("")) {
			currentAgent = ModuleServiceLocator.getTravelAgentBD().getAgent(
					ModuleServiceLocator.getSecurityBD().getUserBasicDetails(userId).getAgentCode());
		}
		
		if(currentAgent !=null && currentAgent.getAgentTypeCode().equals(strCrrAgentType)){
			setAttribInRequest(request, "displayAgencyMode", "1");
		} else if(currentAgent !=null && currentAgent.getAgentTypeCode().equals(AirTravelAgentConstants.AgentTypes.GSA)){
			setAttribInRequest(request, "displayAgencyMode", "2");
		}else{
			setAttribInRequest(request, "displayAgencyMode", "0");
		}

	}
	
	private static void setAgentTypes(HttpServletRequest request) throws ModuleException {
		String strATList = SelectListGenerator.createAgentTypeList_SG();
		request.setAttribute(WebConstants.REQ_HTML_AGENTTYPE_LIST, strATList);
	}
	
	
	protected static void setGSAMultiSelectList(HttpServletRequest request) throws ModuleException {

		boolean blnWithTAs = false;
		boolean blnWithCOs = false;
		String strAgentType = "";
		String strList = "";

		if (request.getParameter("selAgencies") != null) {
			strAgentType = request.getParameter("selAgencies");
		}
		if (request.getParameter("chkCOs") != null) {
			blnWithCOs = (request.getParameter("chkCOs").equals("on") ? true : false);
		}
		if (request.getParameter("chkTAs") != null) {
			blnWithTAs = (request.getParameter("chkTAs").equals("on") ? true : false);
		} else if (getAttribInRequest(request, "displayAgencyMode").equals("2")) {
			blnWithTAs = true;
		}

		if (getAttribInRequest(request, "displayAgencyMode").equals("-1")) {
			// No privilege to access the reports
			throw new ModuleRuntimeException(WebConstants.ERR_UNAUTHORIZED_ACCESS);
		} else if (getAttribInRequest(request, "displayAgencyMode").equals("0")) {
			// No Agent Population Controls or Agents list box are displayed
			String userId = request.getUserPrincipal().getName();
			if (userId != null && !userId.equals("")) {
				Agent currentAgent = ModuleServiceLocator.getTravelAgentBD().getAgent(
						ModuleServiceLocator.getSecurityBD().getUserBasicDetails(userId).getAgentCode());
				setAttribInRequest(request, "currentAgentCode", currentAgent.getAgentCode());
			} else {
				// No privilege to access the reports
				throw new ModuleRuntimeException(WebConstants.ERR_UNAUTHORIZED_ACCESS);
			}
		} else if (getAttribInRequest(request, "displayAgencyMode").equals("1")) {
			// Both Agent Population Controls and Agents list box are displayed
			strList = ReportsHTMLGenerator.createAgentGSAMultiSelect(strAgentType, blnWithTAs, blnWithCOs);
		} else if (getAttribInRequest(request, "displayAgencyMode").equals("2")) {
			// Only Agents list box is displayed
			String userId = request.getUserPrincipal().getName();
			if (userId != null && !userId.equals("")) {
				Agent currentAgent = ModuleServiceLocator.getTravelAgentBD().getAgent(
						ModuleServiceLocator.getSecurityBD().getUserBasicDetails(userId).getAgentCode());
				setAttribInRequest(request, "currentAgentCode", currentAgent.getAgentCode());
				if (currentAgent.getAgentTypeCode().equals(AirTravelAgentConstants.AgentTypes.GSA)) {
					// Agent is a GSA
					strList = ReportsHTMLGenerator.createTAsOfGSAMultiSelect(currentAgent.getAgentCode(), true);
				} else {
					// Agent is not a GSA
					// Only select the current agent
					setAttribInRequest(request, "displayAgencyMode", "0");
				}
			} else {
				// No privilege to access the reports
				throw new ModuleRuntimeException(WebConstants.ERR_UNAUTHORIZED_ACCESS);
			}
		}

		request.setAttribute(WebConstants.REQ_HTML_DETAILS,
				"displayAgencyMode = " + getAttribInRequest(request, "displayAgencyMode") + ";");

		request.setAttribute(WebConstants.REQ_HTML_GSA_LIST, strList);
	}
	
	private static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);
			if (strClientErrors == null)
				strClientErrors = "";
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);

		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());

		}
	}
	
	protected static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {
		ResultSet resultSet = null;
		String value = request.getParameter("radReportOption");
		String flightNo = request.getParameter("txtFlightNumber");
		String depFromDate = request.getParameter("txtFromDepDate");
		String depToDate = request.getParameter("txtToDepDate");
		String arrFromDate = request.getParameter("txtFromArrDate");
		String arrToDate = request.getParameter("txtToArrDate");
		String rptOption = request.getParameter("radOption");
		
		String agents = "";
		String users = "";
		String id = "UC_REPM_088";
		String reportTemplate = "IntlFlightArrivalDepartureReport.jasper";
		String summaryReportTemplate = "IntlFlightArrivalDepartureSummaryReport.jasper";
		String passengerDetailsReportTemplate = "IntlFlightArrivalDeparturePaxDetailsReport.jasper";
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);

		try {

			ReportsSearchCriteria search = new ReportsSearchCriteria();
			Map<String, Object> parameters = new HashMap<String, Object>();

			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}

			if (getAttribInRequest(request, "displayAgencyMode").equals("0")) {
				agents = (String) getAttribInRequest(request, "currentAgentCode");
			} else {
				agents = request.getParameter("hdnAgents");
			}
			String agentArr[] = agents.split(",");
			ArrayList<String> agentCol = new ArrayList<String>();
			for (int r = 0; r < agentArr.length; r++) {
				if (agentArr[r] != null && !agentArr[r].equals("")) {
					agentCol.add(agentArr[r]);
				}
				
			}

			if (depFromDate != null && !depFromDate.equals("")) {
				String DepartureFromDate = ReportsHTMLGenerator.convertDate(depFromDate);
				search.setDepartureDateRangeFrom(DepartureFromDate);
				parameters.put("DEP_FROM_DATE", DepartureFromDate);
			} else {
				parameters.put("DEP_FROM_DATE", "");
			}

			if (depToDate != null && !depToDate.equals("")) {
				String departureToDate = ReportsHTMLGenerator.convertDate(depToDate);
				search.setDepartureDateRangeTo(departureToDate);
				parameters.put("DEP_TO_DATE", departureToDate);
			} else {
				parameters.put("DEP_TO_DATE", "");
			}
			
			if (arrFromDate != null && !arrFromDate.equals("")) {
				String arrivalFromDate = ReportsHTMLGenerator.convertDate(arrFromDate);
				search.setArrivalDateRangeFrom(arrivalFromDate);
				parameters.put("ARR_FROM_DATE", arrivalFromDate);
			} else {
				parameters.put("ARR_FROM_DATE", "");
			}
			
			if (arrToDate != null && !arrToDate.equals("")) {
				String arrivalToDate = ReportsHTMLGenerator.convertDate(arrToDate);
				search.setArrivalDateRangeTo(arrivalToDate);
				parameters.put("ARR_TO_DATE", arrivalToDate);
			} else {
				parameters.put("ARR_TO_DATE", "");
			}
			
			search.setReportType(rptOption);
			search.setFlightNumber(flightNo);
			search.setAgents(agentCol);
			search.setAgentCode(agents);
			
			if (rptOption.equals("DETAIL")) {
				ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
						ReportsHTMLGenerator.getReportTemplate(reportTemplate));

			} else if (rptOption.equals("SUMMARY")) {
				ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
						ReportsHTMLGenerator.getReportTemplate(summaryReportTemplate));
			} else if(rptOption.equals("PAX_DETAILS")){
				ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
						ReportsHTMLGenerator.getReportTemplate(passengerDetailsReportTemplate));
			}

			resultSet = ModuleServiceLocator.getDataExtractionBD().getInternationalFlightDepartureArrivalDataForReports(search);

			
			parameters.put("ID", id);
			parameters.put("FLIGHT_NUMBER", flightNo);
			
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
			String strLogo = AppSysParamsUtil.getReportLogo(true);// globalConfig.getBizParam(SystemParamKeys.REPORT_LOGO);
			String reportsRootDir = "../../images/" + strLogo;

			if (value.trim().equals("HTML")) {
				parameters.put("IMG", reportsRootDir);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null, null,
						response);
			} else if (value.trim().equals("PDF")) {
				response.reset();
				response.addHeader("Content-Disposition", "filename=IntlFlightArrvDeptDetails.pdf");
				reportsRootDir = ReportsHTMLGenerator.getReportTemplate(AppSysParamsUtil.getReportLogo(true));
				parameters.put("IMG", reportsRootDir);
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("EXCEL")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=IntlFlightArrvDeptDetails.xls");
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("CSV")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=IntlFlightArrvDeptDetails.csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);

			}
		} catch (Exception e) {
			log.error(e);
		}
	}
	
	private static boolean isNotEmptyOrNull(String str) {
		return !((str == null) || str.trim().equals("") || str.trim().equals("-1"));
	}

}
