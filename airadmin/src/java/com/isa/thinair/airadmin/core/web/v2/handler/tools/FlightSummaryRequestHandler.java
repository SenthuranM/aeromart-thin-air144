package com.isa.thinair.airadmin.core.web.v2.handler.tools;

import java.util.Collection;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.flightsSummary.FlightSummaryHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airinventory.api.dto.FCCInventoryDTO;
import com.isa.thinair.airschedules.api.criteria.FlightSearchCriteria;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.commons.api.exception.ModuleException;

public class FlightSummaryRequestHandler extends BasicRequestHandler {

	public static int getFlightId(String flightNumber, Date departureDate) throws ModuleException {
		FlightSearchCriteria searchCriteria = new FlightSearchCriteria();
		searchCriteria.setFlightNumber(flightNumber);
		searchCriteria.setEstDepartureDate(departureDate);
		Flight flight = ModuleServiceLocator.getFlightServiceBD().getSpecificFlightDetail(searchCriteria);
		if (flight != null) {
			return flight.getFlightId();
		}
		return -1;
	}

	public static Collection<FCCInventoryDTO> getFCCSegInvDTOList(int flightId) throws ModuleException {
		return ModuleServiceLocator.getFlightInventoryBD().getFCCInventory(flightId, "%%", null, false);
	}

	private static void setClientErrors(HttpServletRequest request) throws ModuleException {
		String strClientErrors = FlightSummaryHTMLGenerator.getClientErrors(request);
		if (strClientErrors == null) {
			strClientErrors = "";
		}
		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
	}

	public static void setDisplayData(HttpServletRequest request) throws ModuleException {

		setClientErrors(request);
	}

}