package com.isa.thinair.airadmin.core.web.action.flightsched;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Result(name = AdminStrutsConstants.AdminAction.SUCCESS, type = JSONResult.class, value = "")
public class UserNoteRollForwardAvailabilityAction extends BaseRequestResponseAwareAction {

	private static Log log = LogFactory.getLog(UserNoteRollForwardAvailabilityAction.class);
	private boolean isSuccess = true;
	private String startDate;
	private String endDate;
	private Collection<Flight> availFlightRS;
	private String deptDate;
	private String schedId;
	private String userNote;
	private String flightId;

	public String execute() throws Exception {

		String forward = AdminStrutsConstants.AdminAction.SUCCESS;

		try {
			SimpleDateFormat ftSlash = new SimpleDateFormat("dd/MM/yyyy");
			Date toDate = ftSlash.parse(endDate);
			Date fromDate = ftSlash.parse(startDate);
			availFlightRS = ModuleServiceLocator.getScheduleServiceBD().getFutureFlightsOfSchedule(Integer.parseInt(schedId),
					fromDate, toDate);

			// set data in the view
		} catch (Exception e) {
			log.error("Error in UserNoteRollForwardAvailabilityAction : " + e);
		}
		request.getAttribute("hdnFromDateAjax");
		return forward;
	}

	public boolean isSuccess() {
		return isSuccess;
	}

	public Collection<Flight> getAvailFlightRS() {
		return availFlightRS;
	}

	public void setAvailFlightRS(Collection<Flight> availFlightRS) {
		this.availFlightRS = availFlightRS;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getDeptDate() {
		return deptDate;
	}

	public void setDeptDate(String deptDate) {
		this.deptDate = deptDate;
	}

	public String getSchedId() {
		return schedId;
	}

	public void setSchedId(String schedId) {
		this.schedId = schedId;
	}

	public String getUserNote() {
		return userNote;
	}

	public void setUserNote(String userNote) {
		this.userNote = userNote;
	}

	public String getFlightId() {
		return flightId;
	}

	public void setFlightId(String flightId) {
		this.flightId = flightId;
	}

}
