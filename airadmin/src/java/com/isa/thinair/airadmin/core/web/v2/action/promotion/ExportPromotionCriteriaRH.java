package com.isa.thinair.airadmin.core.web.v2.action.promotion;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;


public class ExportPromotionCriteriaRH extends BasicRequestHandler{
	
	private static Log log = LogFactory.getLog(ExportPromotionCriteriaRH.class);

	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();
	
	private static final String REPORT_TEMPLATE = "PromotionCriteriaDetails.jasper";
	
	public static String execute(HttpServletRequest request, HttpServletResponse response) {

		String forward = WebConstants.ACTION_FORWARD_SUCCESS;

		try {			
			setReportView(request, response);
			return null;		

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("ExportPromotionCriteriaRH exporting data Failed " + e.getMessageString());
		}
		return forward;
	}
	
	protected static void setReportView (HttpServletRequest request, HttpServletResponse response) throws ModuleException{
		
		ResultSet resultSet = null;		
		String promoCriteriaId = request.getParameter("promoCriteriaIdHdn");		
		String reportTemplate = REPORT_TEMPLATE;
		String reportName = "PromotionCriteriaDetailsReport";
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		
		parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
		
		try {
			
			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs(WebConstants.REPORT_REF,
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));

			ReportsSearchCriteria search = new ReportsSearchCriteria();
			
			search.setPromoCriteriaID(promoCriteriaId);	

			resultSet = ModuleServiceLocator.getDataExtractionBD().getPromotionCriteriaDetails(search);	
			
			response.reset();
			response.addHeader("Content-Disposition", "attachment;filename=" + reportName + ".xls");
			
			ModuleServiceLocator.getReportingFrameworkBD().createXLSReport(WebConstants.REPORT_REF, parameters, resultSet,
						response);
			
		} catch (Exception e) {
			log.error(e);
		}		
		
	}
}