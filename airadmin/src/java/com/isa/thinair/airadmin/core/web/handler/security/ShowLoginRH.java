/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airadmin.core.web.handler.security;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleConfig;
import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.security.LoginHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;

/**
 * 
 * @author Chamindap
 * 
 */
public class ShowLoginRH extends BasicRequestHandler {
	private static Log log = LogFactory.getLog(ShowLoginRH.class);
	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	/**
	 * Execute Method For Login Action
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {

		String forward = AdminStrutsConstants.AdminAction.SUCCESS;

		try {
			setDisplayData(request);
		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
		} catch (RuntimeException re) {
			forward = AdminStrutsConstants.AdminAction.ERROR;
			String msg = null;

			if (re instanceof ModuleRuntimeException) {
				ModuleRuntimeException mre = (ModuleRuntimeException) re;
				msg = mre.getMessageString();
			} else {
				msg = re.getMessage();
			}

			if ("null".equals(msg)) {
				msg = "NullPointer Exception";
			}
			log.error(msg);
			JavascriptGenerator.setServerError(request, msg, "top", "");
		}
		return forward;
	}

	/**
	 * Sets the Display Data for Login Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static void setDisplayData(HttpServletRequest request) throws ModuleException {
		setClientErrors(request);
		setCarrierName(request);
		setRequestHtml(request);
		setAccountLockStatus(request);
	}
	
	private static void setAccountLockStatus(HttpServletRequest request) {
		if (AppSysParamsUtil.isEnableAccountLock()) {
			String message = (String)request.getAttribute("lockedMessage");
			if (message != null) {
				request.setAttribute(WebConstants.REQ_ERROR_SERVER_MESSAGES, "Your account has been locked until " + message + " GMT");
			}
		}
		
	}

	/**
	 * Sets the Client Validations for Login Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setClientErrors(HttpServletRequest request) throws ModuleException {
		String strClientErrors = LoginHTMLGenerator.getClientErrors(request);
		BasicRequestHandler.saveClientErrors(request, strClientErrors);
	}

	/**
	 * Sets the Carrier Code to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setCarrierName(HttpServletRequest request) {
		String strCarrName = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME);
		String strDefaultAirlineID = AppSysParamsUtil.getRequiredDefaultAirlineIdentifierCode();
		request.setAttribute(WebConstants.RES_DEFAULT_CARRIER_NAME, strCarrName);
		request.setAttribute(WebConstants.RES_DEFAULT_AIRLINE_ID_CODE, strDefaultAirlineID);
	}

	/**
	 * Sets the Data to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setRequestHtml(HttpServletRequest request) {
		StringBuffer sb = new StringBuffer();
		AiradminModuleConfig config = AiradminModuleUtils.getConfig();

		String port = config.getPort();
		String sslPort = config.getSslPort();
		String secureLoginUrl = BasicRequestHandler.getSecureUrl(request);
		sb.append("request['secureLoginUrl']='" + secureLoginUrl + "';");
		sb.append("request['port']='" + port + "';");
		sb.append("request['sslPort']='" + sslPort + "';");
		sb.append("request['useSecureLogin']=" +  AiradminModuleUtils.getConfig().isUseSecureLogin() + ";");

		request.setAttribute(WebConstants.REQ_HTML_REQUEST_DATA, sb.toString());
	}
}
