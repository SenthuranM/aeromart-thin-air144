package com.isa.thinair.airadmin.core.web.handler.reports;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

public class AirportTransferReportRH extends BasicRequestHandler {

	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();
	private static Log log = LogFactory.getLog(AirportTransferReportRH.class);

	public static String execute(HttpServletRequest request, HttpServletResponse response) {

		String strHdnMode = request.getParameter("hdnMode");
		String forward = WebConstants.ACTION_FORWARD_SUCCESS;

		try {
			setDisplayData(request);
		} catch (Exception e) {
			forward = AdminStrutsConstants.AdminAction.ERROR;
			log.error("AirportTransferReportRH execute()" + e.getMessage());
		}

		boolean isJapserReport = true;

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportViewAll(request, response, isJapserReport);
				forward = AdminStrutsConstants.AdminAction.RES_SUCCESS;
				return null;
			}

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("AirportTransferReportRH setReportView Failed " + e.getMessageString());
		} catch (Exception e) {
			log.error("AirportTransferReportRH setReportView Failed ", e);
		}

		return forward;
	}

	public static void setDisplayData(HttpServletRequest request) throws Exception {
		setClientErrors(request);
		setAirportList(request);
		setLiveStatus(request);
		setReportingPeriod(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);
		setSSRCodesWithCategory(request);
	}

	public static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString(), moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	protected static void setReportViewAll(HttpServletRequest request, HttpServletResponse response, boolean isJapserReport)
			throws ModuleException {

		try {

			String fromDate = request.getParameter("txtFromDate");
			String toDate = request.getParameter("txtToDate");

			String transferReqFromDate = request.getParameter("transferReqFromDate");
			String transferReqToDate = request.getParameter("transferReqToDate");

			boolean filterByConfirmed = request.getParameter("chkOnlyConfirmed") != null
					&& request.getParameter("chkOnlyConfirmed").trim().equals("CNF");
			String flightNo = request.getParameter("txtFlightNumber");
			String aptCodes = request.getParameter("hdnAptCodes");
			String segments = request.getParameter("hdnSegments");

			String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);
			String strReportFormat = request.getParameter("radRptNumFormat");

			ReportsSearchCriteria search = new ReportsSearchCriteria();

			if (fromDate != null && !fromDate.equals("")) {
				search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate) + " 00:00:00");
			}

			if (toDate != null && !toDate.equals("")) {
				search.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate) + " 23:59:59");
			}

			if ((transferReqFromDate != null && !transferReqFromDate.isEmpty())
					&& (transferReqToDate != null && !transferReqToDate.isEmpty())) {
				search.setDateRange2From(ReportsHTMLGenerator.convertDate(transferReqFromDate) + " 00:00:00");
				search.setDateRange2To(ReportsHTMLGenerator.convertDate(transferReqToDate) + " 23:59:59");
			}

			search.setFilterByConfirmed(filterByConfirmed);

			if (strReportFormat != null && !strReportFormat.equals("")) {
				if (strReportFormat.equalsIgnoreCase("IT")) {
					search.setReqReportFormat("FR");
				} else
					search.setReqReportFormat("");
			}

			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}

			if (flightNo != null && !flightNo.equals("")) {
				search.setFlightNumber(flightNo);
			}

			if (aptCodes != null && !aptCodes.equals("")) {
				String aptCodesArr[] = aptCodes.split(",");
				ArrayList<String> aptCodesCol = new ArrayList<String>();
				for (String apt : aptCodesArr) {
					aptCodesCol.add(apt);
				}
				search.setLstAptTransferCodes(aptCodesCol);
			}

			if (segments != null && !segments.equals("")) {
				String segmentsArr[] = segments.split(",");
				ArrayList<String> segmentsCol = new ArrayList<String>();
				for (String seg : segmentsArr) {
					segmentsCol.add(seg);
				}
				search.setSegmentCodes(segmentsCol);
			}

			if (isJapserReport) {
				// setting the parameter to execute the cursor to return a resultset.
				search.setIsCurSerRequired(ReportsSearchCriteria.IS_CUR_REQUIRED_Y);
				// setting to execute in US format only because PDF/HTML are only genegerated in US
				search.setReqReportFormat("");
			}

			ResultSet rs = ModuleServiceLocator.getDataExtractionBD().getAirportTransferDataforReports(search);

			viewJasperReport(request, response, rs, search, fromDate, toDate);

		} catch (Exception e) {
			log.error("download method is failed :", e);
			throw new ModuleException("Error in report data retrieval");
		}
	}

	private static void setAirportList(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createAirportCodeList();
		request.setAttribute(WebConstants.REQ_HTML_AIRPORT_LIST, strHtml);
	}

	private static void setSSRCodesWithCategory(HttpServletRequest request) throws ModuleException {
		List list = SelectListGenerator.createSSRCodeListWithCategory();
		Iterator itr = list.iterator();
		StringBuilder sb = new StringBuilder();
		while (itr.hasNext()) {
			Map codeMap = (Map) itr.next();
			sb.append(codeMap.get("SSR_CODE"));
			sb.append("|");
			sb.append(codeMap.get("SSR_CAT_ID"));
			sb.append(",");
		}

		request.setAttribute(WebConstants.REQ_SSR_CODE_WITH_CATEGORY, sb.toString());
	}

	private static void setReportingPeriod(HttpServletRequest request) throws ModuleException {
		int period = new Integer(AiradminModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.REPORTING_PERIOD)).intValue();
		request.setAttribute(WebConstants.REP_RPT_START_DAY, ReportsHTMLGenerator.getReportingPeriod(period));
	}

	private static void setLiveStatus(HttpServletRequest request) throws ModuleException {
		String strLive = request.getParameter(WebConstants.REP_HDN_LIVE);
		if (strLive != null)
			request.setAttribute(WebConstants.REP_SET_LIVE, strLive);

	}

	private static void viewJasperReport(HttpServletRequest request, HttpServletResponse response, ResultSet resultSet,
			ReportsSearchCriteria search, String fromDate, String toDate) throws ModuleException {
		String value = request.getParameter("radReportOption");
		String strLogo = AppSysParamsUtil.getReportLogo(false);
		String strPath = "../../images/" + AppSysParamsUtil.getReportLogo(true);
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String strBase = globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY);

		ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs(WebConstants.REPORT_REF,
				ReportsHTMLGenerator.getReportTemplate("AirportTransferReport.jasper"));

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("REPORT_ID", "UC_REPM_088");
		parameters.put("FROM_DATE", fromDate);
		parameters.put("TO_DATE", toDate);
		parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
		parameters.put("CURRENCY", "(" + strBase + ")");

		String reportNumFormat = request.getParameter("radRptNumFormat");
		ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

		if (value.trim().equals(WebConstants.REPORT_HTML)) {
			parameters.put("IMG", strPath);
			ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport(WebConstants.REPORT_REF, parameters, resultSet, null,
					null, response);
		} else if (value.trim().equals(WebConstants.REPORT_PDF)) {
			strPath = ReportsHTMLGenerator.getReportTemplate(strLogo);
			parameters.put("IMG", strPath);
			response.reset();
			response.addHeader("Content-Disposition", String.format("attachment;filename=%s.pdf", "AirportTransferReport"));
			ModuleServiceLocator.getReportingFrameworkBD().createPDFReport(WebConstants.REPORT_REF, parameters, resultSet,
					response);
		} else if (value.trim().equals(WebConstants.REPORT_CSV)) {
			response.reset();
			response.addHeader("Content-Disposition", String.format("attachment;filename=%s.csv", "AirportTransferReport"));
			ModuleServiceLocator.getReportingFrameworkBD().createCSVReport(WebConstants.REPORT_REF, parameters, resultSet,
					response);
		} else if (value.trim().equals(WebConstants.REPORT_EXCEL)) {
			response.reset();
			response.addHeader("Content-Disposition", String.format("attachment;filename=%s.xls", "AirportTransferReport"));
			ModuleServiceLocator.getReportingFrameworkBD().createXLSReport(WebConstants.REPORT_REF, parameters, resultSet,
					response);
		}
	}
}
