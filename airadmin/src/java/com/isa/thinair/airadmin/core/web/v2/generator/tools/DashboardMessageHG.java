package com.isa.thinair.airadmin.core.web.v2.generator.tools;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.v2.util.CommonUtil;
import com.isa.thinair.airmaster.api.model.DashbrdMsgAgent;
import com.isa.thinair.airmaster.api.model.DashbrdMsgAgentPOS;
import com.isa.thinair.airmaster.api.model.DashbrdMsgAgentType;
import com.isa.thinair.airmaster.api.model.DashbrdMsgUser;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.v2.util.MultiSelectDataParser;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.webplatform.core.util.MultiSelectConverterUtil;

public class DashboardMessageHG {

	public static void getClientErrors(HttpServletRequest request) throws ModuleException {

		Properties moduleErrs = new Properties();
		moduleErrs.setProperty("um.dashboard.to.date.should.future", "toDtShdFutrDt");
		moduleErrs.setProperty("um.dashboard.from.date.should.future", "fromDtShdFurDt");
		moduleErrs.setProperty("um.dashboard.message.empty", "messageEmpty");
		moduleErrs.setProperty("um.dashboard.users.empty", "usersEmpty");
		moduleErrs.setProperty("um.dashboard.from.date.empty", "frmDateEmpty");
		moduleErrs.setProperty("um.dashboard.to.date.empty", "toDateEmpty");
		moduleErrs.setProperty("um.dashboard.priority.empty", "priorityEmpty");
		moduleErrs.setProperty("um.dashboard.msgType.empty", "msgTypeEmpty");
		moduleErrs.setProperty("um.dashboard.msgType.empty", "msgTypeEmpty");
		moduleErrs.setProperty("um.dashboard.priority.invalid", "priorityInvalid");
		moduleErrs.setProperty("um.dashboard.from.date.greater.to.date", "fromDateGreater");
		moduleErrs.setProperty("um.dashboard.agent.inclusion", "agentInclusion");
		moduleErrs.setProperty("um.dashboard.agent.type.inclusion", "agentTypeInclusion");
		moduleErrs.setProperty("um.dashboard.user.inclusion", "userInclusion");
		moduleErrs.setProperty("um.dashboard.pos.inclusion", "posInclusion");
		moduleErrs.setProperty("um.dashboard.message.limit.long", "msgLimitLong");
		moduleErrs.setProperty("um.dashboard.message.msg.invalid", "msgInvalid");
		moduleErrs.setProperty("um.dashboard.message.pririty.invalid", "msgPriorityInvalid");
		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, JavascriptGenerator.createClientErrors(moduleErrs));
	}

	public static void setDisplayData(HttpServletRequest request) throws ModuleException {
		setPOSStationList(request);
		setAgentTypeList(request);
		setAgentDisplayData(request);
	}

	private static void setAgentDisplayData(HttpServletRequest request) throws ModuleException {
		String strList = createAgentGSAMultiSelect();
		String strList_act = createActiveAgentGSAMultiSelect();
		// TODO: Since iteration is done anyway. take all records and filter out ACT agents without a DB call
		request.setAttribute(WebConstants.REQ_HTML_GSA_LIST, strList);
		request.setAttribute(WebConstants.REQ_HTML_AGENT_LIST, strList_act);
	}

	/**
	 * Sets the Station List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setPOSStationList(HttpServletRequest request) throws ModuleException {
		List<Map<String, String>> mapedCodes = SelectListGenerator.createStationCodes();
		List<Map<String, String>> actMapedCodes = SelectListGenerator.createActiveStationCodes();
		request.setAttribute(WebConstants.REQ_HTML_STATION_SELECT_LIST, CommonUtil.convertToJSON(MultiSelectConverterUtil
				.convertMapedStringsFromColumnMap(mapedCodes, "STATION_CODE", "POS", "STATION_CODE")));
		request.setAttribute(WebConstants.REQ_HTML_ACTIVE_STATION_SELECT_LIST, CommonUtil.convertToJSON(MultiSelectConverterUtil
				.convertMapedStringsFromColumnMap(actMapedCodes, "STATION_CODE", "POS", "STATION_CODE")));

	}

	private static void setAgentTypeList(HttpServletRequest request) throws ModuleException {
		List<Map<String, String>> mapedCodes = SelectListGenerator.createAgentTypesList();
		request.setAttribute(WebConstants.REQ_AGENT_TYPE, CommonUtil.convertToJSON(MultiSelectConverterUtil
				.convertMapedStringsFromColumnMap(mapedCodes, "AGENT_TYPE_CODE", "AGENT TYPE", "AGENT_TYPE_CODE")));

	}

	@SuppressWarnings("unchecked")
	public static void setAgentList(HttpServletRequest request, User user, int accLevel) throws ModuleException {
		String strAgentGSAHtml = "";
		List<String[]> gsas = null;
		String[] gsaAgents;
		StringBuilder gsb = new StringBuilder();
		List<MultiSelectDataParser> agentMulti = new ArrayList<MultiSelectDataParser>();
		if (accLevel == 1) {
			gsas = (List<String[]>) SelectListGenerator.createAgentsWithType();
		} else if (accLevel > 0) {

			String agentCode = user.getAgentCode();
			String[] agents = new String[1];
			agents[0] = agentCode;

			if (accLevel == 2 || accLevel == 3) {
				gsas = (List<String[]>) SelectListGenerator.createTasFromGSA(agents); // for gsa

			} else if (accLevel == 4) {
				gsas = (List<String[]>) SelectListGenerator.createTADescription(agents); // owner agent
			}

		}

		if (gsas != null && !gsas.isEmpty()) {
			Iterator<String[]> iter = gsas.iterator();
			int i = 0;
			while (iter.hasNext()) {
				gsaAgents = (String[]) iter.next();
				if (gsaAgents != null) {
					if (accLevel == 2) {
						if (i == 0) {
							gsb.append("<option value='" + gsaAgents[0] + "'>" + gsaAgents[1] + "</option>");
							MultiSelectDataParser multiObj = new MultiSelectDataParser();
							multiObj.setCategory("");
							multiObj.setId(gsaAgents[0]);
							multiObj.setText(gsaAgents[1]);
							agentMulti.add(multiObj);
						}
					}
					if (gsaAgents[2] != null) {
						gsb.append("<option value='" + gsaAgents[2] + "'>" + gsaAgents[3] + "</option>");
						MultiSelectDataParser multiObj = new MultiSelectDataParser();
						multiObj.setCategory("");
						multiObj.setId(gsaAgents[2]);
						multiObj.setText(gsaAgents[3]);
						agentMulti.add(multiObj);
					}
					i++;
				}

			}
		}
		strAgentGSAHtml = gsb.toString();
		request.setAttribute(WebConstants.REQ_HTML_AGENT_LIST, strAgentGSAHtml);
		// request.setAttribute(WebConstants.REQ_HTML_AGENT_COMBO,CommonUtil.convertToJSON(agentMulti));

	}

	/**
	 * Sets the Station array to the request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static void setStationHtml(HttpServletRequest request, String gsaCode, int accLevel) throws ModuleException {

		String strAirportList = "";
		if (accLevel == 1) {
			strAirportList = SelectListGenerator.createStationCodeList();
		} else if (accLevel == 2 || accLevel == 3) {
			strAirportList = SelectListGenerator.createGSAStations(gsaCode);
		} else if (accLevel == 4) {
			strAirportList = SelectListGenerator.createOwnerStations(gsaCode);
		}
		request.setAttribute(WebConstants.REQ_HTML_AIRPORT_LIST, strAirportList);

	}

	public static String[] convertAgentSet(Set<DashbrdMsgAgent> setValues) {
		StringBuilder sb = new StringBuilder();
		String includeStatus = null;
		for (Iterator<DashbrdMsgAgent> iterator = setValues.iterator(); iterator.hasNext();) {
			DashbrdMsgAgent agent = iterator.next();
			if (includeStatus == null)
				includeStatus = agent.getApplyStatus();
			sb.append(agent.getAgentCode());
			if (iterator.hasNext()) {
				sb.append(",");
			}
		}
		String[] data = new String[2];
		data[0] = sb.toString();
		data[1] = includeStatus;
		return data;
	}

	public static Object[] convertAgentTypeSet(Set<DashbrdMsgAgentType> setValues, String categoryCode) {
		List<MultiSelectDataParser> multiSelectDataList = new ArrayList<MultiSelectDataParser>();
		String includeStatus = null;
		for (DashbrdMsgAgentType setItem : setValues) {
			MultiSelectDataParser parser = new MultiSelectDataParser();
			if (includeStatus == null)
				includeStatus = setItem.getApplyStatus();
			parser.setCategory(categoryCode);
			parser.setId(setItem.getAgentTypeCode());
			parser.setText(setItem.getAgentTypeCode());
			multiSelectDataList.add(parser);
		}
		Object[] data = new Object[2];
		data[0] = multiSelectDataList;
		data[1] = includeStatus;
		return data;
	}

	public static Object[] convertPOSList(Set<DashbrdMsgAgentPOS> setValues, String categoryCode) {
		String includeStatus = null;
		List<MultiSelectDataParser> multiSelectDataList = new ArrayList<MultiSelectDataParser>();
		for (DashbrdMsgAgentPOS setItem : setValues) {
			MultiSelectDataParser parser = new MultiSelectDataParser();
			if (includeStatus == null)
				includeStatus = setItem.getApplyStatus();
			parser.setCategory(categoryCode);
			parser.setId(setItem.getPosCode());
			parser.setText(setItem.getPosCode());
			multiSelectDataList.add(parser);
		}
		Object[] data = new Object[2];
		data[0] = multiSelectDataList;
		data[1] = includeStatus;
		return data;
	}

	public static Object[] convertUserList(Set<DashbrdMsgUser> setValues) {
		List<String> objData = new ArrayList<String>();
		String includeStatus = null;
		for (DashbrdMsgUser setItem : setValues) {
			if (includeStatus == null)
				includeStatus = setItem.getApplyStatus();
			objData.add(setItem.getUserID());
		}
		Object[] data = new Object[2];
		data[0] = objData;
		data[1] = includeStatus;
		return data;
	}

	/**
	 * Gets agents station wise bt not for gsa with ta.
	 * 
	 * @param strAgentType
	 * @param blnWithTAs
	 * 
	 * @return String an arry of agents js file
	 */
	private final static String createAgentGSAMultiSelect() throws ModuleException {

		StringBuffer sb = new StringBuffer();
		Collection agents = null;
		String strAssignGSAHtml = "";

		strAssignGSAHtml = "var ls = new Listbox('lstRoles', 'lstAssignedRoles', 'spn1');";
		agents = SelectListGenerator.createStationCodes();

		Iterator ite = agents.iterator();
		sb.append("var stns = new Array(); ");

		sb.append(" var agentsArr = new Array();");
		int tempCat = 0;
		int tinc = 0;
		Collection col = null;
		Map keyValues = null;
		Set keys = null;
		Iterator keyIterator = null;
		String agentTpes[] = new String[1];
		String agentType = "";

		while (ite.hasNext()) {

			keyValues = (Map) ite.next();
			keys = keyValues.keySet();
			keyIterator = keys.iterator();

			while (keyIterator.hasNext()) {

				agentType = (String) keyValues.get(keyIterator.next());

				// Populate the Group Array
				sb.append("stns[" + tempCat + "] = '" + agentType + "'; ");
				agentTpes[0] = agentType;
				col = SelectListGenerator.createAgentsByStation(agentTpes);

				if (col != null) {
					sb.append("var agents_" + tempCat + " = new Array(); ");
				}
				if (col.size() == 0) {
					sb.append("new Array();");

				} else {
					Iterator iteAgents = col.iterator();
					tinc = 0;
					while (iteAgents.hasNext()) {
						String str[] = (String[]) iteAgents.next();
						sb.append("agents_" + tempCat + "[" + tinc + "] = ");
						sb.append("new Array ('" + str[0] + "','" + str[1] + "');");
						tinc++;
					}
				}
			}
			sb.append("agentsArr[" + tempCat + "] = agents_" + tempCat + ";");
			tempCat++;
		}

		strAssignGSAHtml = "var arrGroup2 = new Array();" + "var arrData2 = new Array();" + "arrData2[0] = new Array();"

		+ "var ls = new Listbox('lstRoles', 'lstAssignedRoles', 'spn1');" + "ls.group1 = stns;" + "ls.list1 = agentsArr;";

		sb.append(strAssignGSAHtml);
		sb.append("ls.height = '130px'; ls.width  = '160px';");
		sb.append("ls.headingLeft = '&nbsp;&nbsp;All Agents';");
		sb.append("ls.headingRight = '&nbsp;&nbsp;Selected Agents';");
		sb.append("ls.drawListBox();");

		return sb.toString();
	}

	private final static String createActiveAgentGSAMultiSelect() throws ModuleException {

		StringBuffer sb = new StringBuffer();
		Collection agents = null;
		String strAssignGSAHtml = "";

		strAssignGSAHtml = "var ls_act = new Listbox('lstRoles', 'lstAssignedRoles', 'spn2');";
		agents = SelectListGenerator.createActiveStationCodes();

		Iterator ite = agents.iterator();
		sb.append("var stns_act = new Array(); ");

		sb.append(" var agentsArr_act = new Array();");
		int tempCat = 0;
		int tinc = 0;
		Collection col = null;
		Map keyValues = null;
		Set keys = null;
		Iterator keyIterator = null;
		String agentTpes[] = new String[1];
		String agentType = "";

		while (ite.hasNext()) {

			keyValues = (Map) ite.next();
			keys = keyValues.keySet();
			keyIterator = keys.iterator();

			while (keyIterator.hasNext()) {

				agentType = (String) keyValues.get(keyIterator.next());

				// Populate the Group Array
				sb.append("stns_act[" + tempCat + "] = '" + agentType + "'; ");
				agentTpes[0] = agentType;
				col = SelectListGenerator.createAgentsByStationActive(agentTpes);

				if (col != null) {
					sb.append("var agents_act_" + tempCat + " = new Array(); ");
				}
				if (col.size() == 0) {
					sb.append("new Array();");

				} else {
					Iterator iteAgents = col.iterator();
					tinc = 0;
					while (iteAgents.hasNext()) {
						String str[] = (String[]) iteAgents.next();
						sb.append("agents_act_" + tempCat + "[" + tinc + "] = ");
						sb.append("new Array ('" + str[0] + "','" + str[1] + "');");
						tinc++;
					}
				}
			}
			sb.append("agentsArr_act[" + tempCat + "] = agents_act_" + tempCat + ";");
			tempCat++;
		}

		strAssignGSAHtml = "var arrGroup2_act = new Array();" + "var arrData2_act = new Array();"
				+ "arrData2_act[0] = new Array();";

		// + "var ls_act = new Listbox('lstRoles', 'lstAssignedRoles', 'spn2');"
		// + "ls_act.group1 = stns_act;" + "ls_act.list1 = agentsArr_act;";

		sb.append(strAssignGSAHtml);
		// sb.append("ls_act.height = '130px'; ls.width  = '160px';");
		// sb.append("ls_act.headingLeft = '&nbsp;&nbsp;All Agents';");
		// sb.append("ls_act.headingRight = '&nbsp;&nbsp;Selected Agents';");
		// sb.append("ls_act.drawListBox();");

		return sb.toString();
	}
}
