package com.isa.thinair.airadmin.core.web.v2.constants;

public class S2Constants {

	public interface Namespace {

		public static final String PRIVATE_SAMPLES = "/private/samples";
		public static final String PRIVATE_NAME_SPACE = "/private/master";
	}

	public interface Jsp {

		public interface Samples {
			public static final String FLIGHT_LOAD_JSP = "/WEB-INF/jsp/v2/samples/ShowFlightLoad.jsp";
			public static final String COUNTRY_LOAD_JSP = "/WEB-INF/jsp/v2/samples/CountryAdmin.jsp";
		}

		public interface Inventory {
			public static final String LOGICAL_CABIN_CLASS_LOAD_JSP = "/WEB-INF/jsp/v2/inventory/LogicalCabinClass.jsp";
			public static final String GROUP_BOOKING_CLASS_LOAD_JSP = "/WEB-INF/jsp/v2/inventory/GroupBookingClass.jsp";
		}

		public interface Master {
			public static final String COUNTRY_LOAD_JSP = "/WEB-INF/jsp/v2/master/CountryAdmin.jsp";
			public static final String MEAL_LOAD_JSP = "/WEB-INF/jsp/v2/master/meal/MealAdmin.jsp";
			public static final String MEAL_CATEGORY_LOAD_JSP = "/WEB-INF/jsp/v2/master/meal/MealCategoryAdmin.jsp";
			public static final String FLIE_UPLOAD_JSP = "/WEB-INF/jsp/v2/master/FileUpload.jsp";
			public static final String MEAL_TEMP_LOAD_JSP = "/WEB-INF/jsp/v2/master/meal/MealTemplate.jsp";
			public static final String FARE360_JSP = "/WEB-INF/jsp/v2/fare/fare360.jsp";
			public static final String FARE360DATA_JSP = "/WEB-INF/jsp/v2/fare/fare360date.jsp";
			public static final String FARE360EDIT_JSP = "/WEB-INF/jsp/v2/fare/fare360edit.jsp";

			public static final String BAGGAGE_LOAD_JSP = "/WEB-INF/jsp/v2/master/baggage/BaggageAdmin.jsp";
			public static final String BAGGAGE_TEMP_LOAD_JSP = "/WEB-INF/jsp/v2/master/baggage/BaggageTemplate.jsp";
			public static final String OND_BAGGAGE_TEMP_LOAD_JSP = "/WEB-INF/jsp/v2/master/baggage/ONDBaggageTemplate.jsp";
			public static final String OND_TEMP_LOAD_JSP = "/WEB-INF/jsp/v2/master/baggage/ONDTemplate.jsp";

			public static final String SSR_INFO_LOAD_JSP = "/WEB-INF/jsp/v2/master/ssr/SSRInfoAdmin.jsp";
			public static final String SSR_CHARGES_LOAD_JSP = "/WEB-INF/jsp/v2/master/ssr/SSRChargesAdmin.jsp";
			public static final String SSR_TEMPLATE_LOAD_JSP = "/WEB-INF/jsp/v2/master/ssr/SSRTemplate.jsp";
			
			public static final String AIRPORT_TRANSFER_LOAD_JSP = "/WEB-INF/jsp/v2/master/airportTransfer/airportTransfer.jsp";
			public static final String AIRPORT_TRANSFER_TEMPLATE_LOAD_JSP = "/WEB-INF/jsp/v2/master/airportTransfer/airportTransferTemplate.jsp";

			public static final String AIRPORT_MESSAGE_JSP = "/WEB-INF/jsp/v2/master/airportMessages/AirportMessages.jsp";
			
			public static final String ADVERTISEMENT_JSP = "/WEB-INF/jsp/v2/master/advertisement/AdvertisementAdmin.jsp";
			public static final String ROUTEWISE_DEFAULT_ANCI_TEMPLATE ="/WEB-INF/jsp/v2/master/ancillary/defaultTemplate/DefaultAnciTemplate.jsp";
			public static final String STATE_ADMIN_JSP = "/WEB-INF/jsp/v2/master/state/StateAdmin.jsp";
			public static final String BUNDLED_CATEGORY ="/WEB-INF/jsp/v2/promotion/BundledCategory.jsp";
			public static final String AUTOMATIC_CHECKIN_TEMPLATE_LOAD_JSP = "/WEB-INF/jsp/v2/master/automaticCheckin/automaticCheckinTemplate.jsp";

		}

		public interface Flight {
			public static final String FLIGHT_BOOKING_ALLOCATION_PRINT = "/WEB-INF/jsp/v2/flight/flightBookingAllocationPrint.jsp";
			public static final String FLIGHT_REPROTECTION_JSP = "/WEB-INF/jsp/v2/flightschedule/ReProtect.jsp";
		}

		public interface Allocate {
			public static final String FLIGHT_INVANTORY_LOAD_JSP = "/WEB-INF/jsp/v2/manageseatallocation/ManageSeatInventorySearch.jsp";
			public static final String FLIGHT_ALLOCATE_LOAD_JSP = "/WEB-INF/jsp/v2/manageseatallocation/ManageSeatInventoryAllocation.jsp";
		}

		public interface MIS {
			public static final String MIS_REPORTS_CORE_JSP = "/WEB-INF/jsp/v2/mis/MISReports.jsp";
			public static final String MIS_REPORTS_JSP = "/WEB-INF/jsp/v2/mis/reports.jsp";
			public static final String MIS_REGIONAL_POPUP_JSP = "/WEB-INF/jsp/v2/mis/popup_MISRegional.jsp";
		}

		public interface FLA {
			public static final String FLA_FLIGHT_LOAD = "/WEB-INF/jsp/v2/fla/flightLoad.jsp";
		}

		public interface Reporting {
			public static final String RPTS_INCENTIVE_SCHEME_JSP = "/WEB-INF/jsp/v2/reports/IncentiveSchemeReport.jsp";
			public static final String RPTS_AGENT_INCENTIVE_SCHEME_JSP = "/WEB-INF/jsp/v2/reports/AgentIncentiveSchemeReport.jsp";
			public static final String RPTS_PROMO_CODE_DETAILS_JSP = "/WEB-INF/jsp/v2/reports/PromoCodeDetailsReport.jsp";
			public static final String RPTS_JN_TAX_JSP = "/WEB-INF/jsp/v2/reports/JNTaxReport.jsp";
			public static final String RPTS_BSP_RECON_JSP = "/WEB-INF/jsp/v2/reports/BSPReconReport.jsp";
			public static final String RPTS_BUNDLED_FARE_JSP = "/WEB-INF/jsp/v2/reports/BundledFareReport.jsp";
			public static final String RPTS_PAX_STATUS_JSP = "/WEB-INF/jsp/v2/reports/PaxStatusAndRevenueReport.jsp";
		}

		public interface ScheduleReports {
			public static final String SCHE_RPT_MANAGEMENT = "/WEB-INF/jsp/v2/reports/scheduling/scheduleReportsManager.jsp";
		}

		public interface Tools {
			public static final String MESSAGE_MANAGEMENT_JSP = "/WEB-INF/jsp/v2/tools/DashboardMessageManagement.jsp";
			public static final String UPDATE_OND_JSP = "/WEB-INF/jsp/v2/tools/UpdateOndList.jsp";
			public static final String BSPREPORT_LOAD_JSP = "/WEB-INF/jsp/v2/tools/BSPReportManagement.jsp";
			public static final String BSPHOTFILE_LOAD_JSP = "/WEB-INF/jsp/v2/tools/BSPHOTFileManagement.jsp";
			public static final String PFS_HISTORY_JSP = "/WEB-INF/jsp/scheduledservices/PFSHistory.jsp";
			public static final String FLIGHT_SUMMARY_JSP = "/WEB-INF/jsp/v2/tools/FlightSummary.jsp";
			public static final String CONFIG_TIME_LIMITS_JSP = "/WEB-INF/jsp/flightschedule/ConfigTimeLimits.jsp";
			public static final String PNRGOVREPORT_LOAD_JSP = "/WEB-INF/jsp/v2/tools/PNRGOVReportManagement.jsp";
			public static final String GROUP_BOOKING_REQUEST = "/WEB-INF/jsp/GroupBooking/GroupBooking.jsp";
			public static final String USER_STATION_ASSIGNMENT = "/WEB-INF/jsp/GroupBooking/UserAssignStations.jsp";
			public static final String GROUP_BOOKING_STATION_ASSIGNMENT = "/WEB-INF/jsp/GroupBooking/GroupBookingStations.jsp";
			public static final String BLACKLIST_PAX_MANAGEMENT_JSP = "/WEB-INF/jsp/v2/tools/BlacklistPAXManagement.jsp";
			public static final String SEARCH_ROLL_FORWARD_JSP = "/WEB-INF/jsp/v2/tools/SearchRollForward.jsp";
			public static final String OFFICER_NOTIFICATION_CONFIG = "/WEB-INF/jsp/notifications/OfficersNotificationsConfiguration.jsp";
			public static final String CHANGE_PAX_ET_STATUS = "/WEB-INF/jsp/ManagePaxandEtStatus/ChangePaxandEtStatus.jsp";
			public static final String AGENT_USER_ROLES = "/WEB-INF/jsp/reporting/AgentUserRolesReport.jsp";
			public static final String BLACKLIST_RULES_MNG = "/WEB-INF/jsp/v2/tools/BlacklistPAXRulesManage.jsp";
		}

		public interface Fare {
			public static final String FARE_RULE_MANAGE = "/WEB-INF/jsp/v2/fareClasses/ManageFareClasses.jsp";
			public static final String AGENT_FARE = "/WEB-INF/jsp/v2/fareClasses/AgentsFare.jsp";
			public static final String SETUP_FARE = "/WEB-INF/jsp/v2/fareClasses/ViewFares.jsp";
			public static final String OVERRIDEOHD = "/WEB-INF/jsp/v2/fareClasses/overRideOHD.jsp";
			public static final String FARE_RULE_GRID_DATA_V2 = "/WEB-INF/jsp/v2/fareClasses/testData.jsp";
			public static final String PRORATION = "/WEB-INF/jsp/v2/fareClasses/Proration.jsp";
		}

		public interface Charges {
			public static final String CHARGE_ADMIN_JSP = "/WEB-INF/jsp/v2/managecharges/ManageCharges.jsp";
			public static final String CHARGE_GRID_DATA = "/WEB-INF/jsp/v2/managecharges/GridSamleData.jsp";
			public static final String CCCHARGE_ADMIN_JSP = "/WEB-INF/jsp/v2/managecharges/ManageCCCharges.jsp";
		}

		public interface Promotions {
			public static final String PROMO_TEMPLATES_MGMT_JSP = "/WEB-INF/jsp/v2/promotion/CreatePromotion.jsp";
			public static final String PROMO_REQUESTS_MGMT_JSP = "/WEB-INF/jsp/v2/promotion/ManagePromotionRequest.jsp";
			public static final String PROMO_REQUESTS_SMRY_JSP = "/WEB-INF/jsp/v2/promotion/PromotionsApprovalSummary.jsp";
			public static final String PROMO_CODE_MGMT_JSP = "/WEB-INF/jsp/v2/promotion/PromoCodeManagement.jsp";
			public static final String ANCI_OFFER_MGMT_JSP = "/WEB-INF/jsp/v2/promotion/AnciOfferCriteriaManagement.jsp";
			public static final String BUNDLED_FARES_MGMT_JSP = "/WEB-INF/jsp/v2/promotion/BundledFaresManagement.jsp";		
			public static final String BUNDLED_DESCRIPTION_JSP = "/WEB-INF/jsp/v2/promotion/BundleDescription.jsp";
			public static final String VOU_TEMPLATES_MGMT_JSP = "/WEB-INF/jsp/v2/promotion/vouchertemplate/CreateVoucherTemplate.jsp";
            		public static final String CREATE_CAMPAIGN_MGMT_JSP = "/WEB-INF/jsp/v2/promotion/campaign/CreateCampaignTemplate.jsp";
		
		}
	}

	public interface Result {

		public static final String SUCCESS = "success";
		public static final String ERROR = "error";
	}

	public interface RequestConstant {
		public static final String MEAL_CC_CODE = "reqMealCC";
		public static final String BAGGAGE_CC_CODE = "reqBaggageCC";

		public static final String ML_TEMPLATE_CODE = "reqMealTempl";
		public static final String MEAL_LIST = "reqMeallist";
		public static final String MEAL_CATEGORY_LIST = "reqMeaLCatlist";
		public static final String IATA_CODE_LIST = "reqIATACodeList";
		public static final String BG_TEMPLATE_CODE = "reqBaggageTempl";
		public static final String OND_BG_TEMPLATE_CODE = "reqONDBaggageTempl";
		public static final String OND_CODE_LIST = "reqONDCodeList";
		public static final String OND_TEMPLATE_CODE_LIST = "reqONDTemplateCodeList";
		public static final String SEAT_FACTOR_CONDITION_APPLY_FOR = "reqSeatFactorApplyFor";


		public static final String SSR_CODE_ALL = "reqSsrCodeList";
		public static final String SSR_CATEGORY = "reqCategoryList";
		public static final String SSR_SUB_CATEGORY = "reqSubCategoryList";
		public static final String AIRPORT_MULTI_SELECT = "reqAirportMulti";
		public static final String SSR_INV_ENABLED = "reqSSRInventoryEnabled";
		public static final String SSR_CAT_MAP = "reqSSRCatMap";
		public static final String SSR_TEMPLATE_ALL = "reqSSRTempl";
		public static final String AIRCRAFT_TYPE_CODE_ALL = "reqAircraftTypeCodesAll";
		public static final String SSR_CC_CODE = "reqSSRCC";
		public static final String SSR_LIST = "reqSSRList";
		public static final String SSR_CUTOFF_TIME_ENABLED = "reqSSRCutoffTimeEnabled";

		public static final String AGENT_CODE_LIST = "reqAgentCodeList";
		public static final String BSP_COUNTRY_LIST = "reqBSPCountryList";
		public static final String BSP_TNX_TYPES_LIST = "reqBSPTnxTypesList";
		public static final String ALL_COS_LIST = "reqCOSList";
		public static final String PNRGOV_COUNTRY_LIST = "reqPNRGOVCountryList";
		public static final String BSP_TICKETING_AIRLINE_CODE = "reqBSPAirlineCode";

		public static final String PROMOTION_TYPES_LIST = "promotionTypesList";
		public static final String ONLINE_AIRPORT_LIST = "reqOnlineAirportList";
		public static final String FLIGHT_TYPES_LIST = "reqFlightTypesList";
		public static final String BOOKING_CODE_LIST = "reqBookingCodeList";
		public static final String CABIN_CLASS_LIST = "reqCabinClassList";
		public static final String ALL_BOOKING_CODES = "reqAllBookingCodes";
		public static final String ACT_PRIVILEGE_DATA = "actPrivilegeData";
		public static final String VOUCHER_TYPES_LIST = "voucherTypesList";
		public static final String ACI_AIRPORT_LIST = "reqAirportList";
	}

	public interface messageText {
		public static final String DUPLICATE_RECORD = "DPR";
		public static final String INETERNAL_ERROR = "ERR";
	}
}
