/**
 * 	Kasun
	May 18, 2012 
	2012
 */
package com.isa.thinair.airadmin.core.web.v2.action.master;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airadmin.core.web.v2.handler.master.ONDBaggageTemplateRH;
import com.isa.thinair.airadmin.core.web.v2.util.CommonUtil;
import com.isa.thinair.airmaster.api.model.Baggage;
import com.isa.thinair.airpricing.api.criteria.BaggageTemplateSearchCriteria;
import com.isa.thinair.airpricing.api.dto.ONDBaggageTemplateDTO;
import com.isa.thinair.airpricing.api.model.ONDBaggageCharge;
import com.isa.thinair.airpricing.api.model.ONDBaggageTemplate;
import com.isa.thinair.airpricing.api.util.SplitUtil;
import com.isa.thinair.commons.api.constants.CommonsConstants.SeatFactorConditionApplyFor;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.MessagesUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.webplatform.api.util.Constants;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author mano
 */
@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ShowONDBaggageTemplateAction {

	private Object[] rows;
	private int page;
	private int total;
	private int records;
	private String succesMsg;
	private Integer templateId;
	private String templateCode;
	private String description;
	private String status;
	private String baggageCharges;
	private String createdBy;
	private Date createdDate;
	private long version;
	private String selTemplate;
	private String msgType;
	private String templOption;
	private String selStatus;
	private String cabinClass;
	private String isGridBaggage;
	private String baggageValue;
	private String selBaggageId;
	private String baggageForCC;
	private boolean baggageMandatory;
	private String cos;
	private String fromDateTmp;
	private String toDateTmp;
	private String localCurrCode;

	private static AiradminConfig airadminConfig = new AiradminConfig();
	private static Log log = LogFactory.getLog(ShowONDBaggageTemplateAction.class);

	public String execute() {

		log.debug("saving ond baggage template");

		try {
			
			ONDBaggageTemplateDTO oNDBaggageTemplateDTO = getONDBaggageTemplateDTO();
			ONDBaggageTemplateRH.saveONDBaggageTemplate(oNDBaggageTemplateDTO);
			this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS);

			this.msgType = WebConstants.MSG_SUCCESS;
		} catch (ModuleException e) {
			this.succesMsg = e.getMessageString();
			if (e.getMessageString().equals(MessagesUtil.getMessage("module.duplicate.key"))) {
				this.succesMsg = airadminConfig.getMessage("um.baggageTemplate.form.template.exist");
			} else if (e.getMessageString().equals(MessagesUtil.getMessage("um.module.usermessage.code.empty"))) {
				this.succesMsg = airadminConfig.getMessage(e.getExceptionCode());
			}
			this.msgType = WebConstants.MSG_ERROR;
		} catch (Exception ex) {
			this.succesMsg = "System Error Please Contact Administrator";
			this.msgType = WebConstants.MSG_ERROR;
			if (log.isErrorEnabled()) {
				log.error(ex);
			}
		}
		try {
			this.templOption = "<option value=''>All</option>" + SelectListGenerator.createONDBaggageTemplateList();
		} catch (ModuleException e) {
			this.succesMsg = e.getMessageString();
			this.msgType = WebConstants.MSG_ERROR;
		}
		return S2Constants.Result.SUCCESS;
	}

	public String deleteTemplate() {
		if (log.isDebugEnabled()) {
			log.debug("deleting ond baggage template " + templateId);
		}
		ONDBaggageTemplateDTO baggageTemplateDTO = getONDBaggageTemplateDTO();
		try {
			ONDBaggageTemplateRH.deleteONDBaggageTemplate(baggageTemplateDTO);
			this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_DELETE_SUCCESS);
			this.msgType = WebConstants.MSG_SUCCESS;
		} catch (ModuleException e) {
			this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_DELETE_FAIL);
			if (e.getMessageString().equals(MessagesUtil.getMessage("module.constraint.childrecord"))) {
				this.succesMsg = airadminConfig.getMessage("um.airadmin.childrecord");
			}
			this.msgType = WebConstants.MSG_ERROR;
		} catch (Exception ex) {
			this.succesMsg = "System Error Please Contact Administrator";
			this.msgType = WebConstants.MSG_ERROR;
			if (log.isErrorEnabled()) {
				log.error(ex);
			}
		}
		try {
			this.templOption = "<option value=''>All</option>" + SelectListGenerator.createONDBaggageTemplateList();
		} catch (ModuleException e) {
			this.succesMsg = e.getMessageString();
			this.msgType = WebConstants.MSG_ERROR;
		}
		return S2Constants.Result.SUCCESS;
	}

	public String searchONDBaggageTemplate() {

		log.debug("inside searchONDBaggageTemplate");

		BaggageTemplateSearchCriteria criteria = new BaggageTemplateSearchCriteria();

		if (this.selTemplate != null && !this.selTemplate.trim().equals("")) {
			criteria.setTemplateCode(this.selTemplate);
		}

		if (this.selStatus != null && !this.selStatus.trim().equals("")) {
			criteria.setStatus(this.selStatus);
		}

		try {
			Page<ONDBaggageTemplate> pgbaggage = ONDBaggageTemplateRH.searchONDBaggageTemplate(this.page, criteria);
			Collection<Baggage> colBaggage = ModuleServiceLocator.getCommonServiceBD().getBaggages();
			this.records = pgbaggage.getTotalNoOfRecords();
			this.total = pgbaggage.getTotalNoOfRecords() / 20;
			int mod = pgbaggage.getTotalNoOfRecords() % 20;
			if (mod > 0) {
				this.total = this.total + 1;
			}
			if (this.page < 0) {
				this.page = 1;
			}

			Collection<ONDBaggageTemplate> colBaggageTemplates = pgbaggage.getPageData();
			if (colBaggageTemplates != null) {
				Object[] dataRow = new Object[colBaggageTemplates.size()];
				int index = 1;
				for (ONDBaggageTemplate grdCountry : colBaggageTemplates) {
					Map<String, Object> counmap = new HashMap<String, Object>();
					Collection<ONDBaggageCharge> colBaggageCharge = grdCountry.getBaggageCharges();
					counmap.put("id", new Integer(((page - 1) * 20) + index).toString());
					counmap.put("fromDateTmp", CalendarUtil.formatDate(grdCountry.getFromDate(), CalendarUtil.PATTERN1));
					counmap.put("toDateTmp", CalendarUtil.formatDate(grdCountry.getToDate(), CalendarUtil.PATTERN1));
					counmap.put("baggageTemplate", grdCountry);
					counmap.put("baggageCharge", addBaggageName(colBaggageCharge, colBaggage));
					dataRow[index - 1] = counmap;
					index++;
				}
				this.rows = dataRow;
			}
			this.baggageMandatory = AppSysParamsUtil.isBaggageMandatory();

		} catch (ModuleException e) {
			this.succesMsg = e.getMessageString();
			this.msgType = WebConstants.MSG_ERROR;
		} catch (Exception ex) {
			this.succesMsg = "System Error Please Contact Administrator";
			this.msgType = WebConstants.MSG_ERROR;
			log.error("Error occured", ex);
		}
		return S2Constants.Result.SUCCESS;
	}

	private ONDBaggageTemplateDTO getONDBaggageTemplateDTO() {
		ONDBaggageTemplateDTO ondBaggageTemplateDTO = new ONDBaggageTemplateDTO();
		ondBaggageTemplateDTO.setBaggageCharges(baggageCharges);
		ondBaggageTemplateDTO.setCreatedBy(createdBy);
		ondBaggageTemplateDTO.setCreatedDate(createdDate);
		ondBaggageTemplateDTO.setDescription(description);
		if (fromDateTmp != null && !fromDateTmp.isEmpty()) {
			ondBaggageTemplateDTO.setFromDateTmp(AiradminUtils.getFormattedDate(fromDateTmp));
		}	
		ondBaggageTemplateDTO.setLocalCurrCode(localCurrCode);
		ondBaggageTemplateDTO.setStatus(status);
		ondBaggageTemplateDTO.setTemplateCode(templateCode);
		if (toDateTmp != null && !toDateTmp.isEmpty()) {
			ondBaggageTemplateDTO.setToDateTmp(CalendarUtil.add(AiradminUtils.getFormattedDate(toDateTmp), 0, 0, 1, 0, 0, -1));
		}
		ondBaggageTemplateDTO.setVersion(version);
		ondBaggageTemplateDTO.setTemplateId(templateId);
		return ondBaggageTemplateDTO;
	}

	private String addBaggageName(Collection<ONDBaggageCharge> colBaggageCharge, Collection<Baggage> colBaggage)
			throws ModuleException {

		Collection<ONDBaggageCharge> chCol = new HashSet<ONDBaggageCharge>();
		StringBuilder sb = new StringBuilder();
		Map<Integer, Baggage> baggageMap = new HashMap<Integer, Baggage>();
		for (Baggage baggage : colBaggage) {
			baggageMap.put(baggage.getBaggageId(), baggage);
		}

		if (chCol != null) {
			for (ONDBaggageCharge baggageCharge : colBaggageCharge) {
				sb.append(baggageMap.get(baggageCharge.getBaggageId()).getBaggageName()).append(Constants.COMMA_SEPARATOR);
				sb.append(baggageCharge.getChargeId()).append(Constants.COMMA_SEPARATOR);
				sb.append(baggageCharge.getTemplate().getTemplateId()).append(Constants.COMMA_SEPARATOR);
				sb.append(baggageCharge.getBaggageId()).append(Constants.COMMA_SEPARATOR);
				sb.append(baggageCharge.getAmount()).append(Constants.COMMA_SEPARATOR);
				sb.append(baggageCharge.getAllocatedPieces()).append(Constants.COMMA_SEPARATOR);
				sb.append(baggageCharge.getVersion()).append(Constants.COMMA_SEPARATOR);
				sb.append(baggageMap.get(baggageCharge.getBaggageId()).getStatus()).append(Constants.COMMA_SEPARATOR);
				String cabinClass = baggageCharge.getCabinClass();
				String logicalCabinClass = baggageCharge.getLogicalCCCode();
				sb.append(CommonUtil.getCabinClassOrLogicalCabinClassDescription(cabinClass, logicalCabinClass)).append(
						Constants.COMMA_SEPARATOR);
				sb.append(CommonUtil.concatanateCabinClassAndLogicalCabinClass(cabinClass, logicalCabinClass)).append(
						Constants.COMMA_SEPARATOR);
				sb.append(baggageCharge.getDefaultBaggageFlag()).append(Constants.COMMA_SEPARATOR);
				sb.append(baggageCharge.getSeatFactor()).append(Constants.COMMA_SEPARATOR);
				sb.append(baggageCharge.getTotalWeightLimit()).append(Constants.COMMA_SEPARATOR);
				if (baggageCharge.getSeatFactorLimitationApplyFor() != null) {
					sb.append(getBaggageSeatFactorApplyForDescription(baggageCharge.getSeatFactorLimitationApplyFor())).append(
							Constants.COMMA_SEPARATOR).append("~");
				} else {
					sb.append(SeatFactorConditionApplyFor.APPLY_FOR_MOTST_RESTRICTIVE.getDescription()).append(
							Constants.COMMA_SEPARATOR).append("~");
				}

			}
		}
		return sb.toString();
	}

	private static String getBaggageSeatFactorApplyForDescription(int condition) {
		if (SeatFactorConditionApplyFor.APPLY_FOR_MOTST_RESTRICTIVE.getCondition() == condition) {
			return SeatFactorConditionApplyFor.APPLY_FOR_MOTST_RESTRICTIVE.getDescription();
		} else if (SeatFactorConditionApplyFor.APPLY_FOR_FIRST_SEGMENT.getCondition() == condition) {
			return SeatFactorConditionApplyFor.APPLY_FOR_FIRST_SEGMENT.getDescription();
		} else if (SeatFactorConditionApplyFor.APPLY_FOR_LAST_SEGMENT.getCondition() == condition) {
			return SeatFactorConditionApplyFor.APPLY_FOR_LAST_SEGMENT.getDescription();
		}
		return " ";
	}
	
	/**
	 * @return the rows
	 */
	public Object[] getRows() {
		return rows;
	}

	/**
	 * @param rows
	 *            the rows to set
	 */
	public void setRows(Object[] rows) {
		this.rows = rows;
	}

	/**
	 * @return the page
	 */
	public int getPage() {
		return page;
	}

	/**
	 * @param page
	 *            the page to set
	 */
	public void setPage(int page) {
		this.page = page;
	}

	/**
	 * @return the total
	 */
	public int getTotal() {
		return total;
	}

	/**
	 * @param total
	 *            the total to set
	 */
	public void setTotal(int total) {
		this.total = total;
	}

	/**
	 * @return the records
	 */
	public int getRecords() {
		return records;
	}

	/**
	 * @param records
	 *            the records to set
	 */
	public void setRecords(int records) {
		this.records = records;
	}

	/**
	 * @return the succesMsg
	 */
	public String getSuccesMsg() {
		return succesMsg;
	}

	/**
	 * @param succesMsg
	 *            the succesMsg to set
	 */
	public void setSuccesMsg(String succesMsg) {
		this.succesMsg = succesMsg;
	}

	/**
	 * @return the templateId
	 */
	public Integer getTemplateId() {
		return templateId;
	}

	/**
	 * @param templateId
	 *            the templateId to set
	 */
	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}

	/**
	 * @return the templateCode
	 */
	public String getTemplateCode() {
		return templateCode;
	}

	public String getLocalCurrCode() {
		return localCurrCode;
	}

	public void setLocalCurrCode(String localCurrCode) {
		this.localCurrCode = localCurrCode;
	}

	/**
	 * @param templateCode
	 *            the templateCode to set
	 */
	public void setTemplateCode(String templateCode) {
		this.templateCode = templateCode;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the baggageCharges
	 */
	public String getBaggageCharges() {
		return baggageCharges;
	}

	/**
	 * @param baggageCharges
	 *            the baggageCharges to set
	 */
	public void setBaggageCharges(String baggageCharges) {
		this.baggageCharges = baggageCharges;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate
	 *            the createdDate to set
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return the version
	 */
	public long getVersion() {
		return version;
	}

	/**
	 * @param version
	 *            the version to set
	 */
	public void setVersion(long version) {
		this.version = version;
	}

	/**
	 * @return the selTemplate
	 */
	public String getSelTemplate() {
		return selTemplate;
	}

	/**
	 * @param selTemplate
	 *            the selTemplate to set
	 */
	public void setSelTemplate(String selTemplate) {
		this.selTemplate = selTemplate;
	}

	/**
	 * @return the msgType
	 */
	public String getMsgType() {
		return msgType;
	}

	/**
	 * @param msgType
	 *            the msgType to set
	 */
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	/**
	 * @return the templOption
	 */
	public String getTemplOption() {
		return templOption;
	}

	/**
	 * @param templOption
	 *            the templOption to set
	 */
	public void setTemplOption(String templOption) {
		this.templOption = templOption;
	}

	/**
	 * @return the selStatus
	 */
	public String getSelStatus() {
		return selStatus;
	}

	/**
	 * @param selStatus
	 *            the selStatus to set
	 */
	public void setSelStatus(String selStatus) {
		this.selStatus = selStatus;
	}

	/**
	 * @return the cabinClass
	 */
	public String getCabinClass() {
		return cabinClass;
	}

	/**
	 * @param cabinClass
	 *            the cabinClass to set
	 */
	public void setCabinClass(String cabinClass) {
		this.cabinClass = cabinClass;
	}

	/**
	 * @return the isGridBaggage
	 */
	public String getIsGridBaggage() {
		return isGridBaggage;
	}

	/**
	 * @param isGridBaggage
	 *            the isGridBaggage to set
	 */
	public void setIsGridBaggage(String isGridBaggage) {
		this.isGridBaggage = isGridBaggage;
	}

	/**
	 * @return the baggageValue
	 */
	public String getBaggageValue() {
		return baggageValue;
	}

	/**
	 * @param baggageValue
	 *            the baggageValue to set
	 */
	public void setBaggageValue(String baggageValue) {
		this.baggageValue = baggageValue;
	}

	public String getBaggageForCC() {
		return baggageForCC;
	}

	public void setBaggageForCC(String baggageForCC) {
		this.baggageForCC = baggageForCC;
	}

	public String getSelBaggageId() {
		return selBaggageId;
	}

	public void setSelBaggageId(String selBaggageId) {
		this.selBaggageId = selBaggageId;
	}

	public boolean isBaggageMandatory() {
		return baggageMandatory;
	}

	public void setBaggageMandatory(boolean baggageMandatory) {
		this.baggageMandatory = baggageMandatory;
	}

	public String getBaggagesForCabinClass() throws Exception {

		Collection<Baggage> allBaggages = null;
		StringBuffer sb = new StringBuffer();
		if (this.isGridBaggage != null) {
			if (this.isGridBaggage.trim().equals("true")) {
				sb.append("<option value='" + this.selBaggageId + "'>" + this.baggageValue + "</option>");
				this.baggageForCC = sb.toString();

			} else {

				if (this.cos != null && this.cos.trim() != "") {

					String[] splittedCabinClassAndLogicalCabinClass = SplitUtil.getSplittedCabinClassAndLogicalCabinClass(cos);
					allBaggages = ModuleServiceLocator.getCommonServiceBD().getBaggagesCOS(
							splittedCabinClassAndLogicalCabinClass[0], splittedCabinClassAndLogicalCabinClass[1]);
					for (Baggage baggage : allBaggages) {
						sb.append("<option value='" + baggage.getBaggageId() + "'>" + baggage.getBaggageName() + "</option>");
					}
					this.baggageForCC = sb.toString();

				}
			}
		}
		this.baggageMandatory = AppSysParamsUtil.isBaggageMandatory();
		return S2Constants.Result.SUCCESS;
	}

	public String getFromDateTmp() {
		return fromDateTmp;
	}

	public void setFromDateTmp(String fromDateTmp) {
		this.fromDateTmp = fromDateTmp;
	}

	public String getToDateTmp() {
		return toDateTmp;
	}

	public void setToDateTmp(String toDateTmp) {
		this.toDateTmp = toDateTmp;
	}

	/**
	 * @return the cos
	 */
	public String getCos() {
		return cos;
	}

	/**
	 * @param cos
	 *            the cos to set
	 */
	public void setCos(String cos) {
		this.cos = cos;
	}

}
