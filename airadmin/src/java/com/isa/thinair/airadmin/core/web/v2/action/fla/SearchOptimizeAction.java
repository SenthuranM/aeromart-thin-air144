package com.isa.thinair.airadmin.core.web.v2.action.fla;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airadmin.core.web.v2.dto.fla.FlightOptimizeDetailDTO;
import com.isa.thinair.airadmin.core.web.v2.dto.fla.FlightOptimizeSummaryDTO;
import com.isa.thinair.airadmin.core.web.v2.dto.mis.MISProcessParams;
import com.isa.thinair.airadmin.core.web.v2.util.CommonUtil;
import com.isa.thinair.airinventory.api.dto.ExtendedFCCSegBCInventory;
import com.isa.thinair.airinventory.api.dto.FCCInventoryDTO;
import com.isa.thinair.airinventory.api.dto.FCCSegInventoryDTO;
import com.isa.thinair.airinventory.api.dto.FCCSegInventoryUpdateDTO;
import com.isa.thinair.airinventory.api.dto.FCCSegInventoryUpdateStatusDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.model.FCCSegBCInventory;
import com.isa.thinair.airinventory.api.model.FCCSegInventory;
import com.isa.thinair.airinventory.api.model.OptimizeSeatInvCriteriaDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class SearchOptimizeAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(SearchOptimizeAction.class);
	private boolean success = true;
	private String messageTxt;
	private Collection<FlightOptimizeSummaryDTO> flightSummaryDTOList;
	private Collection<FlightOptimizeDetailDTO> details;

	public String execute() throws Exception {

		String strForward = WebConstants.FORWARD_SUCCESS;
		MISProcessParams mpParams = new MISProcessParams(request);
		request.setAttribute("req_InitialParams", CommonUtil.convertToJSON(mpParams));
		String flightID = request.getParameter("flightID");
		try {
			flightSummaryDTOList = getFlightDetailsForOptimaization(flightID);
		} catch (Exception e) {
			log.error("Generic exception caught.", e);
			success = false;
			messageTxt = e.getMessage();
		}
		return strForward;
	}

	@SuppressWarnings("unused")
	private Collection<FlightOptimizeSummaryDTO> getFlightDetailsForOptimaization(String flightID) throws ModuleException {
		Set<Integer> flightIds = new HashSet<Integer>();
		Collection<FlightOptimizeSummaryDTO> flightOptimizeSummaryDataList = new ArrayList<FlightOptimizeSummaryDTO>();
		flightIds.add(new Integer(flightID));
		Collection<FCCInventoryDTO> colOptimize = ModuleServiceLocator.getFlightInventoryBD()
				.getFlightInventoriesForOptimization(flightIds, new OptimizeSeatInvCriteriaDTO());
		if (colOptimize != null) {
			for (FCCInventoryDTO fccInventoryDTO : colOptimize) {

				for (FCCSegInventoryDTO fccSegInventoryDTO : (Collection<FCCSegInventoryDTO>) fccInventoryDTO
						.getFccSegInventoryDTOs()) {
					FlightOptimizeSummaryDTO summaryDTO = new FlightOptimizeSummaryDTO();
					Collection<FlightOptimizeDetailDTO> bookingClassDetails = new ArrayList<FlightOptimizeDetailDTO>();
					summaryDTO.setCabinClass(fccInventoryDTO.getCabinClassCode());
					summaryDTO.setFccInventoryID(fccInventoryDTO.getFccaInvId());
					FCCSegInventory fccSegInventory = fccSegInventoryDTO.getFccSegInventory();
					summaryDTO.setSegmentCode(fccSegInventory.getSegmentCode());
					summaryDTO.setFccsInventoryID(fccSegInventory.getFccsInvId());
					summaryDTO.setVersion(fccSegInventory.getVersion());
					summaryDTO.setAllocatedAdults(String.valueOf(fccSegInventory.getAllocatedSeats()));
					summaryDTO.setAllocatedInfants(String.valueOf(fccSegInventory.getInfantAllocation()));
					summaryDTO.setSoldAdultsInfants(String.valueOf(fccSegInventory.getSeatsSold()) + "/"
							+ fccSegInventory.getSoldInfantSeats());
					summaryDTO.setOnholdAdultsInfants(fccSegInventory.getOnHoldSeats() + "/"
							+ fccSegInventory.getOnholdInfantSeats());
					summaryDTO.setCurtailedSeats(String.valueOf(fccSegInventory.getCurtailedSeats()));
					summaryDTO.setOverSellSeats(String.valueOf(fccSegInventory.getOversellSeats()));
					summaryDTO.setAvailableAdultsInfants(String.valueOf(fccSegInventory.getAvailableSeats()) + "/"
							+ fccSegInventory.getAvailableInfantSeats());

					for (ExtendedFCCSegBCInventory bcInventory : (Collection<ExtendedFCCSegBCInventory>) fccSegInventoryDTO
							.getFccSegBCInventories()) {
						FCCSegBCInventory fccSegBCInventory = bcInventory.getFccSegBCInventory();
						// Set the segment booking class details

						FlightOptimizeDetailDTO flightOptimizeDetailDTO = new FlightOptimizeDetailDTO();
						flightOptimizeDetailDTO.setAcquiredSeats(String.valueOf(fccSegBCInventory.getSeatsAcquired()));
						flightOptimizeDetailDTO.setAllocatedToBookingClass(String.valueOf(fccSegBCInventory.getSeatsAllocated()));
						flightOptimizeDetailDTO.setAvailableSeats(String.valueOf(fccSegBCInventory.getSeatsAvailable()));
						flightOptimizeDetailDTO.setOnholdSeats(String.valueOf(fccSegBCInventory.getActualSeatsOnHold()));
						flightOptimizeDetailDTO.setSoldSeats(String.valueOf(fccSegBCInventory.getActualSeatsSold()));
						flightOptimizeDetailDTO.setCancelledSeats(String.valueOf(fccSegBCInventory.getSeatsCancelled()));
						flightOptimizeDetailDTO.setFccsInvId(fccSegBCInventory.getfccsInvId());
						flightOptimizeDetailDTO.setFccsbcInvId(fccSegBCInventory.getFccsbInvId());
						flightOptimizeDetailDTO.setVersion(fccSegBCInventory.getVersion());
						flightOptimizeDetailDTO.setBookingClassCode(fccSegBCInventory.getBookingCode());
						flightOptimizeDetailDTO.setSegmentCode(fccSegBCInventory.getSegmentCode());
						flightOptimizeDetailDTO.setLogicalCCCode(fccSegBCInventory.getLogicalCCCode());
						flightOptimizeDetailDTO.setClosed((String.valueOf(fccSegBCInventory.getStatus()))
								.equalsIgnoreCase(FCCSegBCInventory.Status.CLOSED) ? '1' : '0');
						flightOptimizeDetailDTO
								.setFixed((String.valueOf((bcInventory.isFixedFlag()))).equalsIgnoreCase("FALSE") ? '0' : '1');
						flightOptimizeDetailDTO.setPriority((fccSegBCInventory.getPriorityFlag()) ? '1' : '0');
						flightOptimizeDetailDTO.setAction(fccSegBCInventory.getStatusChangeAction());
						flightOptimizeDetailDTO.setOldAction(fccSegBCInventory.getStatusChangeAction());
						flightOptimizeDetailDTO.setOldStatus((String.valueOf(fccSegBCInventory.getStatus()))
								.equalsIgnoreCase(FCCSegBCInventory.Status.CLOSED) ? '1' : '0');
						BookingClass bc = ModuleServiceLocator.getBookingClassBD().getLightWeightBookingClass(
								fccSegBCInventory.getBookingCode());
						flightOptimizeDetailDTO.setOpenReturnBc(bc.getBcType().equalsIgnoreCase(
								BookingClass.BookingClassType.OPEN_RETURN) ? true : false);
						bookingClassDetails.add(flightOptimizeDetailDTO);
					}
					summaryDTO.setBookingClassDetails(bookingClassDetails);
					flightOptimizeSummaryDataList.add(summaryDTO);
				}

			}
		}
		return flightOptimizeSummaryDataList;

	}

	public String saveOptimizedData() throws Exception {
		String strForward = WebConstants.FORWARD_SUCCESS;
		String flightID = request.getParameter("flightID");
		String allocatedInfants = request.getParameter("summary.allocatedInfants");
		String overSell = request.getParameter("summary.overSellSeats");
		String curtailed = request.getParameter("summary.curtailedSeats");
		String fccsaInvID = request.getParameter("summary.segInventoryID");
		String version = request.getParameter("summary.version");
		String invID = request.getParameter("summary.InvID");

		Set<FCCSegInventoryUpdateDTO> fccSegInventoryUpdateDTOs = new HashSet<FCCSegInventoryUpdateDTO>();
		FCCSegInventoryUpdateDTO fccSegInventoryDTO = setSummaryOptimizeData(new Integer(fccsaInvID), Integer.parseInt(flightID),
				Integer.parseInt(invID));
		fccSegInventoryDTO.setInfantAllocation(Integer.parseInt(allocatedInfants));
		fccSegInventoryDTO.setOversellSeats(Integer.parseInt(overSell));
		fccSegInventoryDTO.setCurtailedSeats(Integer.parseInt(curtailed));
		fccSegInventoryDTO.setFccsInventoryId(Integer.parseInt(fccsaInvID));
		fccSegInventoryDTO.setVersion(Long.parseLong(version));

		Set<FCCSegBCInventory> updatedFFCSegBC = new HashSet<FCCSegBCInventory>();

		if (details != null && details.size() > 0) {
			for (FlightOptimizeDetailDTO flightOptimizeDetailDTO : details) {
				if (flightOptimizeDetailDTO != null) {
					FCCSegBCInventory fccSegBCInventory = new FCCSegBCInventory();
					fccSegBCInventory.setSeatsAllocated(new Integer(flightOptimizeDetailDTO.getAllocatedToBookingClass()));
					fccSegBCInventory.setStatus(flightOptimizeDetailDTO.getClosed() == '0' ? FCCSegBCInventory.Status.OPEN
							: FCCSegBCInventory.Status.CLOSED);
					fccSegBCInventory.setPriorityFlag(flightOptimizeDetailDTO.getPriority() == '0' ? false : true);

					if (flightOptimizeDetailDTO.getOldStatus() != flightOptimizeDetailDTO.getClosed()) {
						flightOptimizeDetailDTO.setAction(FCCSegBCInventory.StatusAction.MANUAL);
					} else {
						flightOptimizeDetailDTO.setAction(flightOptimizeDetailDTO.getOldAction());
					}
					fccSegBCInventory.setStatusChangeAction(flightOptimizeDetailDTO.getAction());
					fccSegBCInventory.setfccsInvId(flightOptimizeDetailDTO.getFccsInvId());
					fccSegBCInventory.setFccsbInvId(flightOptimizeDetailDTO.getFccsbcInvId());
					fccSegBCInventory.setFlightId(new Integer(flightID));
					fccSegBCInventory.setBookingCode(flightOptimizeDetailDTO.getBookingClassCode());
					fccSegBCInventory.setSeatsAcquired(new Integer(flightOptimizeDetailDTO.getAcquiredSeats()));
					fccSegBCInventory.setSeatsCancelled(new Integer(flightOptimizeDetailDTO.getCancelledSeats()));
					fccSegBCInventory.setOnHoldSeats(new Integer(flightOptimizeDetailDTO.getOnholdSeats()));
					fccSegBCInventory.setSeatsSold(new Integer(flightOptimizeDetailDTO.getSoldSeats()));
					fccSegBCInventory.setSegmentCode(flightOptimizeDetailDTO.getSegmentCode());
					fccSegBCInventory.setLogicalCCCode(flightOptimizeDetailDTO.getLogicalCCCode());
					updatedFFCSegBC.add(fccSegBCInventory);
				}
			}
			fccSegInventoryDTO.setUpdatedFCCSegBCInventories(updatedFFCSegBC);
		}
		fccSegInventoryUpdateDTOs.add(fccSegInventoryDTO);

		try {
			LinkedHashMap flightOptimaizUpdateStatus = ModuleServiceLocator.getFlightInventoryBD().updateFCCSegInventories(
					fccSegInventoryUpdateDTOs);
			for (Integer fccsInvID : (Set<Integer>) flightOptimaizUpdateStatus.keySet()) {
				FCCSegInventoryUpdateStatusDTO updateStatus = (FCCSegInventoryUpdateStatusDTO) flightOptimaizUpdateStatus
						.get(fccsInvID);
				if (updateStatus.getStatus() == FCCSegInventoryUpdateStatusDTO.OPERATION_FAILURE) {
					// throw new ModuleException(AirinventoryCustomConstants.INV_MODULE_CODE);
					success = false;
					messageTxt = updateStatus.getMsg();
				}

			}
			flightSummaryDTOList = getFlightDetailsForOptimaization(flightID);
		} catch (Exception e) {
			log.error("Generic exception caught.", e);
			success = false;
			messageTxt = e.getMessage();
		}
		return strForward;
	}

	/**
	 * Sets the required summary DTO data from the values of retrieved object from the DB
	 * 
	 * @param fccsInvID
	 * @param flightID
	 * @return
	 * @throws ModuleException
	 */
	private FCCSegInventoryUpdateDTO setSummaryOptimizeData(Integer fccsInvID, int flightID, int inventoryID)
			throws ModuleException {
		Set<Integer> flightIds = new HashSet<Integer>();
		flightIds.add(new Integer(flightID));
		Collection<FCCInventoryDTO> colOptimize = ModuleServiceLocator.getFlightInventoryBD()
				.getFlightInventoriesForOptimization(flightIds, new OptimizeSeatInvCriteriaDTO());
		FCCInventoryDTO oldInventoryDTO = null;
		for (FCCInventoryDTO obj : colOptimize) {
			if (obj.getFccaInvId() == inventoryID) {
				oldInventoryDTO = obj;
			}
		}

		FCCSegInventoryUpdateDTO updateDTO = new FCCSegInventoryUpdateDTO();
		FCCSegInventoryDTO oldFccSegInventory = (FCCSegInventoryDTO) oldInventoryDTO.getFccSegInventoryDTOsMap().get(fccsInvID);
		updateDTO.setFccsInventoryId(oldInventoryDTO.getFccaInvId());
		updateDTO.setFlightId(flightID);
		updateDTO.setLogicalCCCode(oldFccSegInventory.getLogicalCCCode());
		updateDTO.setSegmentCode(oldFccSegInventory.getSegmentCode());
		return updateDTO;
	}

	public Collection<FlightOptimizeSummaryDTO> getFlightSummaryDTOList() {
		return flightSummaryDTOList;
	}

	public void setFlightSummaryDTOList(Collection<FlightOptimizeSummaryDTO> flightSummaryDTOList) {
		this.flightSummaryDTOList = flightSummaryDTOList;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public Collection<FlightOptimizeDetailDTO> getDetails() {
		return details;
	}

	public void setDetails(Collection<FlightOptimizeDetailDTO> details) {
		this.details = details;
	}

}
