package com.isa.thinair.airadmin.core.web.v2.action.common;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.commons.api.exception.CommonsException;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;

/**
 * Base Action class to handle Exceptions
 * 
 * @author Navod Ediriweera
 * @since 07 July 2010
 */
public abstract class CommonAdminRequestResponseAwareCommonAction extends BaseRequestResponseAwareAction {

	protected String message;
	protected String msgType;

	protected static AiradminConfig airadminConfig = new AiradminConfig();

	protected void handleException(Exception e) {
		if (message == null) {
			if (e instanceof CommonsException) {
				message = ((CommonsException) e).getMessageString();
			} else {
				message = e.getMessage();
			}
		}
		msgType = WebConstants.MSG_ERROR;
	}

	protected void setDefaultSuccessMessage() {
		message = airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS);
		msgType = WebConstants.MSG_SUCCESS;
	}

	public abstract String getMessage();

	public abstract void setMessage(String message);

	public abstract String getMsgType();

	public abstract void setMsgType(String msgType);

}
