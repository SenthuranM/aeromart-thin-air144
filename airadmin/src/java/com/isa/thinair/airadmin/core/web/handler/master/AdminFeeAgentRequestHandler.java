package com.isa.thinair.airadmin.core.web.handler.master;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.master.AdminFeeAgentHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

public final class AdminFeeAgentRequestHandler extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(AdminFeeAgentRequestHandler.class);

	private static AiradminConfig airadminConfig = new AiradminConfig();

	private static final String PARAM_VERSION = "hdnVersion";
	private static final String PARAM_GRIDROW = "hdnGridRow";
	private static final String PARAM_MODE = "hdnMode";
	private static final String PARAM_AGENT_CODE_ADD = "hdnAddAgentCodes";
	private static final String PARAM_AGENT_CODE_DELETE = "hdnDeleteAgentCodes";

	private static String strHdnMode;

	private static int intSuccess = 0;  // 0-Not
										// Applicable,
										// 1-Success,
										// 2-Fail

	private static void setExceptionOccured(HttpServletRequest request, boolean value) {
		setAttribInRequest(request, "isExceptionOccured", new Boolean(value));
	}

	private static boolean isExceptionOccured(HttpServletRequest request) {
		return ((Boolean) getAttribInRequest(request, "isExceptionOccured")).booleanValue();
	}

	private static boolean isSuccessfulSaveMessageDisplayEnabled() {
		boolean isMessageEnabled = false;
		if (airadminConfig.getMessage("um.airadmin.savemessage.display").equals("true")) {
			isMessageEnabled = true;
		}
		return isMessageEnabled;
	}

	public static String execute(HttpServletRequest request) {

		strHdnMode = request.getParameter(PARAM_MODE);
		String forward = WebConstants.FORWARD_SUCCESS;

		if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_GRID)) {
			return WebConstants.ACTION_GRID;
		}
		log.debug("Mode: " + strHdnMode);
		intSuccess = 0;
		String strFormData = "var saveSuccess = " + intSuccess + ";";
		strFormData += " var arrFormData = new Array();";
		request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);

		setExceptionOccured(request, false);
		setAttribInRequest(request, "strModeJS", "var isAddMode = false;");
		setAttribInRequest(request, "strAirportFocusJS", "var isAirportFocus = false;");
		setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = false;");
		setAttribInRequest(request, "isPalCalEnabledJS", "var isPalCalEnabled = " + AppSysParamsUtil.isEnablePalCalMessage()
				+ ";");

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_SAVE)){
				saveData(request);
			}
			setDisplayData(request);
		} catch (Exception exception) {
			log.error("Exception in AdminFeeAgentRequestHandler.execute()", exception);
			if (exception instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
			}
		}

		return forward;
	}

	private static boolean saveData(HttpServletRequest request) throws Exception {

		log.debug("Entered inside the saveData()...");
		
		String strAgentCodesAdd = null;
		String strAgentCodesDelete = null;
		List<String> agentCodesAdd = null;
		List<String> agentCodesDelete = null;

		try {

			strAgentCodesAdd = AiradminUtils.getNotNullString(request.getParameter(PARAM_AGENT_CODE_ADD));
			strAgentCodesDelete = AiradminUtils.getNotNullString(request.getParameter(PARAM_AGENT_CODE_DELETE));
			
			if (strAgentCodesAdd != null && !"".equals(strAgentCodesAdd)) {
				agentCodesAdd = new ArrayList<String>();
				StringTokenizer strTok = new StringTokenizer(strAgentCodesAdd, ",");
				while (strTok.hasMoreTokens()) {
					agentCodesAdd.add(strTok.nextToken());
				}
			}
			
			if (strAgentCodesDelete != null && !"".equals(strAgentCodesDelete)) {
				agentCodesDelete = new ArrayList<String>();
				StringTokenizer strTok = new StringTokenizer(strAgentCodesDelete, ",");
				while (strTok.hasMoreTokens()) {
					agentCodesDelete.add(strTok.nextToken());
				}
			}
			
			ModuleServiceLocator.getTravelAgentBD().saveAdminFeeAgent(agentCodesAdd, agentCodesDelete);
			
			
			
			setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = true;");
			saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS), WebConstants.MSG_SUCCESS);
			setExceptionOccured(request, false);
			log.debug("AdminFeeAgentHandler.saveData() method is successfully executed.");

		} catch (ModuleException moduleException) {
			log.error(
					"Exception in AdminFeeAgentRequestHandler.saveData() [origin module=" + moduleException.getModuleDesc() + "]",
					moduleException);
			setExceptionOccured(request, true);

		} catch (Exception exception) {
			log.error("Exception in AdminFeeAgentRequestHandler.deleteData()", exception);
			if (exception instanceof RuntimeException) {
				throw exception;
			}
		}
		return true;
	}

	private static void setDisplayData(HttpServletRequest request) {
		setAgentRowHtml(request);
		setAdminFeeAgentList(request);
		setClientErrors(request);

		if (!isExceptionOccured(request)) {
			String strFormData = "var arrFormData = new Array();";
			strFormData += " var saveSuccess = " + intSuccess + ";";
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
		}

		request.setAttribute(WebConstants.REQ_OPERATION_MODE, getAttribInRequest(request, "strModeJS"));
		request.setAttribute(WebConstants.REQ_TRANSACTION_STATUS, getAttribInRequest(request, "isSaveTransactionSuccessfulJS"));
		request.setAttribute(WebConstants.REQ_TRANSACTION_DISPLAYSTATUS, "var isSuccessfulSaveMessageDisplayEnabled = "
				+ isSuccessfulSaveMessageDisplayEnabled() + ";");
	}

	private static void setAdminFeeAgentList(HttpServletRequest request) {

		try {
			String strHtml = SelectListGenerator.createAdminFeeAgentList();
			request.setAttribute(WebConstants.REQ_HTML_ADMIN_FEE_AGENT_LIST, strHtml);
		} catch (ModuleException moduleException) {
			log.error("Exception in AdminFeeAgentRequestHandler.setAirportList() [origin module=" + moduleException.getModuleDesc()
					+ "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	private static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = AdminFeeAgentHTMLGenerator.getClientErrors(request);
			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error(
					"Exception in AdminFeeAgentRequestHandler.setClientErrors() [origin module=" + moduleException.getModuleDesc()
							+ "]", moduleException);
		}
	}

	protected static void setAgentRowHtml(HttpServletRequest request) {
		try {
			AdminFeeAgentHTMLGenerator htmlGen = new AdminFeeAgentHTMLGenerator();
			String strHtml = htmlGen.getAgentRowHtml(request);
			request.setAttribute(WebConstants.REQ_HTML_ROWS, strHtml);

			request.setAttribute(WebConstants.REQ_FORM_FIELD_VALUES, htmlGen.getFormFieldValues());
		} catch (ModuleException moduleException) {
			log.error(
					"Exception in AdminFeeAgentRequestHandler.setAdminFeeAgentRowHtml() [origin module="
							+ moduleException.getModuleDesc() + "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}
}
