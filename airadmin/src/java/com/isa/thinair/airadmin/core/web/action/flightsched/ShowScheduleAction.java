package com.isa.thinair.airadmin.core.web.action.flightsched;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.handler.flightsched.ScheduleRequestHandler;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Results({ @Result(name = AdminStrutsConstants.AdminAction.SUCCESS, value = AdminStrutsConstants.AdminJSP.FLIGHT_SHED_ADMIN_JSP),
		@Result(name = AdminStrutsConstants.AdminAction.RE_PROTECT, value = AdminStrutsConstants.AdminJSP.FLIGHT_REPROTECT_JSP),
		@Result(name = AdminStrutsConstants.AdminAction.ERROR, value = AdminStrutsConstants.AdminJSP.ERROR_JSP) })
public class ShowScheduleAction extends BaseRequestResponseAwareAction {

	public String execute() throws Exception {
		return ScheduleRequestHandler.execute(request, response);
	}

}
