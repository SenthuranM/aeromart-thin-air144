package com.isa.thinair.airadmin.core.web.auth;

import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.commons.api.exception.ModuleException;

public class AuthorizationManager {
	private static final AiradminConfig airadminConfig = new AiradminConfig();
	private static Log log = LogFactory.getLog(AuthorizationManager.class);

	public static final boolean isUserAuthorized(User user, String url) {
		boolean auth = false;
		String priviledge = airadminConfig.getUrlPriviledge(url);
		if (priviledge == null) {
			auth = true;
		} else {
			Collection<String> privColl = null;
			try {
				privColl = user.getPrivitedgeIDs();
			} catch (ModuleException e) {
				if (log.isInfoEnabled()) {
					log.info("Not Authorized : " + e.getMessage());
				}
			}
			if (privColl != null) {
				auth = privColl.contains(priviledge);
			}
		}

		return auth;
	}

}
