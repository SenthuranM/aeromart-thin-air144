package com.isa.thinair.airadmin.core.web.v2.action.pricing;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airadmin.api.FareRuleCommentDTO;
import com.isa.thinair.airadmin.api.FareRuleDTO;
import com.isa.thinair.airadmin.api.FareRuleFeeDTO;
import com.isa.thinair.airadmin.api.FareRuleInfoDTO;
import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airadmin.core.web.v2.util.ErrorMessageUtil;
import com.isa.thinair.airadmin.core.web.v2.util.FareUtil;
import com.isa.thinair.airpricing.api.criteria.PricingConstants;
import com.isa.thinair.airpricing.api.dto.QuotedChargeDTO;
import com.isa.thinair.airpricing.api.model.FareRule;
import com.isa.thinair.airpricing.api.service.ChargeBD;
import com.isa.thinair.airpricing.api.service.FareRuleBD;
import com.isa.thinair.airreservation.api.dto.OnHoldReleaseTimeDTO;
import com.isa.thinair.commons.api.constants.ApplicationEngine;
import com.isa.thinair.commons.api.dto.FlightTypeDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * 
 * @author Pradeep Karunanayake
 * 
 */
@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
@SuppressWarnings("rawtypes")
public class FareRuleAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(FareRuleAction.class);

	private FareRuleInfoDTO fareInfo;

	private FareRuleDTO fareRule;

	private Collection<FareRuleFeeDTO> fees;

	private boolean success = true;

	private String messageTxt;

	private Map errorInfo;

	private List<Map<String, List<String[]>>> agents;

	private List<String[]> pos;

	private Collection<FareRuleCommentDTO> fareRuleComments;

	private String mode;

	public String saveFareRule() {
		String forward = S2Constants.Result.SUCCESS;
		AiradminConfig airadminConfig = new AiradminConfig();
		try {
			try {
				checkPrivilege(request, WebConstants.PLAN_FARES_RULE_ADD);
			} catch (ModuleRuntimeException mre) {
				log.error("Error in ChargesRequestHandler execute ", mre);
				checkPrivilege(request, WebConstants.PLAN_FARES_RULE_EDIT);
			}
			if (isUpdate()) {
				fareRule.getAdPax().setVersion(Integer.parseInt(request.getParameter("versionAD")));
				fareRule.getAdPax().setFarePaxID(Long.parseLong(request.getParameter("paxIDAD")));
				fareRule.getChPax().setVersion(Integer.parseInt(request.getParameter("versionCH")));
				fareRule.getChPax().setFarePaxID(Long.parseLong(request.getParameter("paxIDCH")));
				fareRule.getInPax().setVersion(Integer.parseInt(request.getParameter("versionIN")));
				fareRule.getInPax().setFarePaxID(Long.parseLong(request.getParameter("paxIDIN")));
			}

			FareRule fareRuleNew = FareUtil.getFareRuleFromDto(getFareRule(), fees, isUpdate());

			FareRuleBD fareRuleBD = ModuleServiceLocator.getRuleBD();

			/*
			 * If the checkbox is ticked will onlyupdate comments and agent instructions ignoring everything else
			 */
			boolean splitFares = true;
			if (fareRule.getOnlyUpdateComments() != null && fareRule.getOnlyUpdateComments().equals("Y")) {
				splitFares = false;
				fareRuleNew = fareRuleBD.getFareRule(fareRule.getFareRuleID());
				fareRuleNew.setRulesComments(fareRule.getRulesComments());
				fareRuleNew.setAgentInstructions(fareRule.getAgentComments());
				fareRuleNew.setFareRuleDescription(fareRule.getDescription());
				fareRuleNew.setVisibileAgentCodes(fareRule.getVisibileAgentCol());
				fareRuleNew.setPointOfSale(fareRule.getVisiblePOS());
				fareRuleNew.setApplicableToAllCountries(fareRule.getApplicableToCountries());
			}

			fareRuleBD.createFareRule(fareRuleNew, splitFares);

			if (AppSysParamsUtil.isEnableFareRuleComments()) {
				FareRule fareRule = fareRuleBD.getFareRule(fareRuleNew.getFareRuleCode());
				if (fareRule != null) {
					FareUtil.updateComments(fareRule, fareRuleComments);
					fareRuleBD.createFareRule(fareRule, false);
				}
			}

			messageTxt = airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS);
		} catch (ModuleException ex) {
			log.error(ex);
			success = false;
			messageTxt = ex.getMessageString();
		} catch (Exception ex) {
			log.error(ex);
			success = false;
			messageTxt = ex.getMessage();
		}
		return forward;
	}

	public String deleteFareRule() {
		String forward = S2Constants.Result.SUCCESS;
		AiradminConfig airadminConfig = new AiradminConfig();
		try {
			checkPrivilege(request, WebConstants.PLAN_FARES_RULE_DELETE);

			FareRuleBD fareRuleBD = ModuleServiceLocator.getRuleBD();

			Collection<Integer> fareIds = fareRuleBD.getFareIdsForFareRule(fareRule.getFareRuleID());

			if (fareIds != null && fareIds.size() > 0) {
				log.error("Fares already created for this Fare Rule.");
				success = false;
				messageTxt = airadminConfig.getMessage("um.airadmin.childrecord");
			} else {
				// Delete OHD release time configurations linked with fare rule
				Collection<OnHoldReleaseTimeDTO> ohdRelDTOs = ModuleServiceLocator.getRuleBD().getOHDRelTimeDTOForFareRuleId(
						fareRule.getFareRuleID());
				if (ohdRelDTOs != null && ohdRelDTOs.size() > 0) {
					for (OnHoldReleaseTimeDTO ohdRelCfg : ohdRelDTOs) {
						ModuleServiceLocator.getReservationBD().deleteOnholdReleaseTimeConfig(ohdRelCfg.getReleaseTimeId());
					}
				}

				fareRuleBD.deleteFareRule(fareRule.getFareRuleID());
				messageTxt = airadminConfig.getMessage(WebConstants.KEY_DELETE_SUCCESS);
			}
		} catch (ModuleException ex) {
			log.error(ex);
			success = false;
			messageTxt = ex.getMessageString();
		} catch (Exception ex) {
			log.error(ex);
			success = false;
			messageTxt = ex.getMessage();
		}
		return forward;
	}

	public String createPageInitailData() {
		String forward = S2Constants.Result.SUCCESS;
		try {
			if (fareInfo == null) {
				fareInfo = new FareRuleInfoDTO();
			}
			createFareInfo();
			errorInfo = ErrorMessageUtil.getFareRuleErrors(AiradminConfig.defaultLanguage);
		} catch (ModuleException ex) {
			log.error(ex);
			success = false;
			messageTxt = ex.getMessageString();
		} catch (Exception ex) {
			log.error(ex);
			success = false;
			messageTxt = ex.getMessage();
		}
		return forward;
	}

	public String loadAgentsData() {
		String forward = S2Constants.Result.SUCCESS;
		try {
			agents = SelectListGenerator.createAgentStationMDList();
		} catch (ModuleException ex) {
			log.error(ex);
			success = false;
			messageTxt = ex.getMessageString();
		} catch (Exception ex) {
			log.error(ex);
			success = false;
			messageTxt = ex.getMessage();
		}
		return forward;
	}

	public String loadPOSData() {
		String forward = S2Constants.Result.SUCCESS;
		try {
			pos = SelectListGenerator.getCountryNameList();
		} catch (ModuleException ex) {
			log.error(ex);
			success = false;
			messageTxt = ex.getMessageString();
		} catch (Exception ex) {
			log.error(ex);
			success = false;
			messageTxt = ex.getMessage();
		}
		return forward;
	}

	private void createFareInfo() throws ModuleException {
		fareInfo.setChildVisible(FareUtil.isChildVisible());
		fareInfo.setFareCategoryList(SelectListGenerator.createFareCategoryArrayList());
		fareInfo.setFareModValueTypes(SelectListGenerator.createFareModCancTypesList());
		fareInfo.setPaxChargeTypes(SelectListGenerator.createPaxChargeTypesList());
		fareInfo.setFareRuleIdTypeList(SelectListGenerator.createFareRuleIDTypeList());
		fareInfo.setFareVisibilityList(SelectListGenerator.createFareVisibilityArrayList());
		fareInfo.setFixedBCFareRuleList(SelectListGenerator.createFixedBCFareRuleArrayList());
		fareInfo.setFlexiCategoryList(SelectListGenerator.createFlexiCodeArrayList());
		fareInfo.setFlexiJournylist(SelectListGenerator.createFlexiJourneyArrayList());
		fareInfo.setOpenReturnSupport(FareUtil.isOpenReturnSupport());
		fareInfo.setPaxCategoryList(SelectListGenerator.createPaxCatagoryArrayList());
		fareInfo.setEnableAgentCommission(AppSysParamsUtil.isAgentCommmissionEnabled());
		fareInfo.setEnableFareDiscount(AppSysParamsUtil.isFareDiscountEnabled());
		fareInfo.setEnableAgentInstruction(AppSysParamsUtil.isAgentInstructionsEnabled());
		fareInfo.setEnableFareInDepAirPCurr(FareUtil.isShowFareDefInDepAirPCurr());
		fareInfo.setCurrencyCodeList(SelectListGenerator.createActiveCurrencyCodeList());
		fareInfo.setEnableDomesticBufferTime(false);
		fareInfo.setShowNoFareSplitOption(AppSysParamsUtil.isShowNoFareSplitOption());
		fareInfo.setDomesticModBufferTime(AppSysParamsUtil.getDomesticBufferDurationInMillis());
		fareInfo.setInternationalModBufferTime(AppSysParamsUtil.getInternationalBufferDurationInMillis());
		fareInfo.setMinRequiredSeatsForBTRes(AppSysParamsUtil.getMinRequiredSeatsForBTReservations());
		if (!AppSysParamsUtil.isAllowReturnParticipateInHalfReturn()) {
			fareInfo.setHalfReturnSupport(true);
		}

		ChargeBD chargeBD = ModuleServiceLocator.getChargeBD();
		Collection<String> chargeGroupCol = new ArrayList<String>();

		chargeGroupCol.add(PricingConstants.ChargeGroups.MODIFICATION_CHARGE);
		chargeGroupCol.add(PricingConstants.ChargeGroups.CANCELLATION_CHARGE);

		HashMap map = chargeBD.quoteUniqueChargeForGroups(chargeGroupCol);

		QuotedChargeDTO quoteDTOMod = (QuotedChargeDTO) map.get(PricingConstants.ChargeGroups.MODIFICATION_CHARGE);

		QuotedChargeDTO quoteDTOCancel = (QuotedChargeDTO) map.get(PricingConstants.ChargeGroups.CANCELLATION_CHARGE);
		if (quoteDTOMod != null) {
			fareInfo.setDefaultModCharge(quoteDTOMod.getChargeValuePercentage());
		}
		if (quoteDTOCancel != null) {
			fareInfo.setDefaultCnxCharge(quoteDTOCancel.getChargeValuePercentage());
		}

		List<FlightTypeDTO> lstFltTypes = AppSysParamsUtil.availableFlightTypes();
		for (FlightTypeDTO fltType : lstFltTypes) {
			if (fltType.getFlightType().equals("DOM") && fltType.getFlightTypeStatus().equals("Y")) {
				fareInfo.setEnableDomesticBufferTime(true);
			}
		}

		if (AppSysParamsUtil.isEnableFareRuleLevelNCC(ApplicationEngine.XBE)) {
			fareInfo.setEnableFareRuleNCC(true);
		}

	}

	/**
	 * Checks the Privileges of a User
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @param privilegeId
	 *            the Privilege needs to check
	 */
	private static void checkPrivilege(HttpServletRequest request, String privilegeId) {
		Map mapPrivileges = (Map) request.getSession().getAttribute(WebConstants.SES_PRIVILEGE_IDS);

		if (mapPrivileges.get(privilegeId) == null) {
			log.error("Unauthorize operation:" + request.getUserPrincipal().getName() + ":" + privilegeId);
			throw new ModuleRuntimeException(WebConstants.ERR_UNAUTHORIZED_ACCESS);
		}

	}

	private boolean isUpdate() {
		return mode != null && "EDIT".equalsIgnoreCase(mode);
	}

	public FareRuleInfoDTO getFareInfo() {
		return fareInfo;
	}

	public void setFareInfo(FareRuleInfoDTO fareInfo) {
		this.fareInfo = fareInfo;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public Map getErrorInfo() {
		return errorInfo;
	}

	public void setFareRule(FareRuleDTO fareRule) {
		this.fareRule = fareRule;
	}

	public FareRuleDTO getFareRule() {
		return fareRule;
	}

	public void setFees(Collection<FareRuleFeeDTO> fees) {
		this.fees = fees;
	}

	public Collection<FareRuleFeeDTO> getFees() {
		return fees;
	}

	public List getAgents() {
		return agents;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public List<String[]> getPos() {
		return pos;
	}

	public Collection<FareRuleCommentDTO> getFareRuleComments() {
		return fareRuleComments;
	}

	public void setFareRuleComments(Collection<FareRuleCommentDTO> fareRuleComments) {
		this.fareRuleComments = fareRuleComments;
	}

	public boolean isMultilingualCommentEnabled() {
		return AppSysParamsUtil.isEnableFareRuleComments();
	}

}
