package com.isa.thinair.airadmin.core.web.v2.action.inventory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.v2.action.common.CommonAdminRequestResponseAwareCommonAction;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airadmin.core.web.v2.dto.inventory.FLCCInventoryTO;
import com.isa.thinair.airadmin.core.web.v2.util.CommonUtil;
import com.isa.thinair.airinventory.api.dto.FLCCAllocationDTO;
import com.isa.thinair.airinventory.api.model.LogicalCabinClass;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * @author eric
 * 
 */
@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class LogicalCabinClassWiseAllocationAction extends CommonAdminRequestResponseAwareCommonAction {

	private static Log log = LogFactory.getLog(LogicalCabinClassWiseAllocationAction.class);

	private String flightId;

	private String flightNumber;

	private String modelNo;

	private String cabinClassCode;

	private Collection<FLCCInventoryTO> fccInventoryTOs;

	private int actualAdultCapacity;

	private int actualInfantCapacity;

	private String hint;

	private int splitOption;

	private String splitAdultInfantRatio;

	private boolean logicalCabinClassInventoryRestricted;

	/**
	 * @return
	 */
	public String searchLogicalCabinClassAllocations() {
		try {
			Collection<FLCCAllocationDTO> flccAllocationDTOs = ModuleServiceLocator.getFlightInventoryBD()
					.getLogicalCabinClassWiseAllocations(Integer.parseInt(flightId), cabinClassCode);
			Map<String, LogicalCabinClass> logicalCabinClasses = ModuleServiceLocator.getLogicalCabinClassServiceBD()
					.getLogicalCabinClasses(cabinClassCode);
			this.fccInventoryTOs = this.buildFCCSegInventoryList(flccAllocationDTOs, logicalCabinClasses);

			FLCCAllocationDTO flccAllocationDTO = flccAllocationDTOs.iterator().next();
			this.actualAdultCapacity = flccAllocationDTO.getActualAdultCapacity();
			this.actualInfantCapacity = flccAllocationDTO.getActualInfantCapacity();
			this.hint = "Actual adult capacity : " + flccAllocationDTO.getActualAdultCapacity() + " , Actual infant capacity : "
					+ flccAllocationDTO.getActualInfantCapacity();
			this.splitAdultInfantRatio = AppSysParamsUtil.getSplitAdultInfantRatio();
			setLogicalCabinClassInventoryRestricted(AppSysParamsUtil.isLogicalCabinClassInventoryRestricted());
		} catch (Exception e) {
			this.handleException(e);
			log.error(" Exception occured while loading cabin class allocations ", e);
		} finally {
		}
		return S2Constants.Result.SUCCESS;
	}

	public String splitAllocations() {
		try {
			Collection<FLCCAllocationDTO> flccAllocationDTOs = new ArrayList<FLCCAllocationDTO>();
			for (FLCCInventoryTO flccInventoryTO : fccInventoryTOs) {
				FLCCAllocationDTO flccAllocationDTO = new FLCCAllocationDTO();
				flccAllocationDTO.setCabinClassCode(cabinClassCode);
				flccAllocationDTO.setLogicalCabinClassCode(flccInventoryTO.getLogicalCCCode());
				flccAllocationDTO.setAdultAllocation(flccInventoryTO.getAdultSeatAllocation());
				flccAllocationDTO.setInfantAllocation(flccInventoryTO.getInfantSeatAllocation());
				flccAllocationDTO.setLogicalCCRank(flccInventoryTO.getLogicalCCRank());
				flccAllocationDTOs.add(flccAllocationDTO);
			}
			ModuleServiceLocator.getFlightInventoryBD().createFCCSegInventory(flccAllocationDTOs, Integer.parseInt(flightId),
					cabinClassCode, splitOption);
			this.setDefaultSuccessMessage();
		} catch (ModuleException me) {
			msgType = WebConstants.MSG_ERROR;
			message = CommonUtil.getFormattedMessage(me.getExceptionCode(), me.getMessageParameters());

		} catch (Exception e) {
			this.handleException(e);
			log.error(" Exception occured while saving cabin class allocations ", e);
		} finally {
			fccInventoryTOs = null;
		}
		return S2Constants.Result.SUCCESS;
	}

	/**
	 * @param fccSegInventories
	 * @param logicalCabinClasses
	 * @return
	 */
	private List<FLCCInventoryTO> buildFCCSegInventoryList(Collection<FLCCAllocationDTO> flccAllocationDTOs,
			Map<String, LogicalCabinClass> logicalCabinClasses) {
		List<FLCCInventoryTO> flccInventoryTOs = new ArrayList<FLCCInventoryTO>();
		for (String logicalCabinClassCode : logicalCabinClasses.keySet()) {
			boolean fccSegInvFound = false;
			LogicalCabinClass lcc = logicalCabinClasses.get(logicalCabinClassCode);
			for (FLCCAllocationDTO flccAllocationDTO : flccAllocationDTOs) {
				if (flccAllocationDTO.getLogicalCabinClassCode().equals(logicalCabinClassCode)) {
					FLCCInventoryTO flccInventoryTO = new FLCCInventoryTO();
					flccInventoryTO.setFlightId(Integer.parseInt(flightId));
					flccInventoryTO.setAdultSeatAllocation(flccAllocationDTO.getAdultAllocation());
					flccInventoryTO.setInfantSeatAllocation(flccAllocationDTO.getInfantAllocation());

					flccInventoryTO.setCcCode(lcc.getCabinClassCode());
					flccInventoryTO.setLogicalCCCode(lcc.getLogicalCCCode());
					flccInventoryTO.setLogicalCCName(lcc.getDescription());
					flccInventoryTO.setLogicalCCRank(lcc.getNestRank());
					flccInventoryTO.setLogicalCabinClassDescription(lcc.getDescription() + " [ " + lcc.getLogicalCCCode() + " - "
							+ lcc.getNestRank() + " ]");

					flccInventoryTOs.add(flccInventoryTO);
					fccSegInvFound = true;
					break;
				}
			}
			if (!fccSegInvFound && lcc.getStatus().equals(CommonsConstants.STATUS_ACTIVE)) {
				FLCCInventoryTO flccInventoryTO = new FLCCInventoryTO();
				flccInventoryTO.setLogicalCCCode(lcc.getLogicalCCCode());
				flccInventoryTO.setLogicalCCName(lcc.getDescription());
				flccInventoryTO.setLogicalCCRank(lcc.getNestRank());
				flccInventoryTO.setLogicalCabinClassDescription(lcc.getDescription() + " [ " + lcc.getLogicalCCCode() + " - "
						+ lcc.getNestRank() + " ]");
				flccInventoryTOs.add(flccInventoryTO);
			}
		}
		Collections.sort(flccInventoryTOs);
		return flccInventoryTOs;
	}

	public Collection<FLCCInventoryTO> getFccInventoryTOs() {
		return fccInventoryTOs;
	}

	public void setFccInventoryTOs(Collection<FLCCInventoryTO> fccInventoryTOs) {
		this.fccInventoryTOs = fccInventoryTOs;
	}

	public String getFlightId() {
		return flightId;
	}

	public void setFlightId(String flightId) {
		this.flightId = flightId;
	}

	public String getCabinClassCode() {
		return cabinClassCode;
	}

	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	public String getHint() {
		return hint;
	}

	public void setHint(String hint) {
		this.hint = hint;
	}

	public int getActualAdultCapacity() {
		return actualAdultCapacity;
	}

	public void setActualAdultCapacity(int actualAdultCapacity) {
		this.actualAdultCapacity = actualAdultCapacity;
	}

	public int getActualInfantCapacity() {
		return actualInfantCapacity;
	}

	public void setActualInfantCapacity(int actualInfantCapacity) {
		this.actualInfantCapacity = actualInfantCapacity;
	}

	@Override
	public String getMessage() {
		return message;
	}

	@Override
	public void setMessage(String message) {
		this.message = message;

	}

	@Override
	public String getMsgType() {
		return msgType;
	}

	@Override
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getModelNo() {
		return modelNo;
	}

	public void setModelNo(String modelNo) {
		this.modelNo = modelNo;
	}

	public int getSplitOption() {
		return splitOption;
	}

	public void setSplitOption(int splitOption) {
		this.splitOption = splitOption;
	}

	public String getSplitAdultInfantRatio() {
		return splitAdultInfantRatio;
	}

	public void setSplitAdultInfantRatio(String splitAdultInfantRatio) {
		this.splitAdultInfantRatio = splitAdultInfantRatio;
	}

	public boolean isLogicalCabinClassInventoryRestricted() {
		return logicalCabinClassInventoryRestricted;
	}

	public void setLogicalCabinClassInventoryRestricted(boolean logicalCabinClassInventoryRestricted) {
		this.logicalCabinClassInventoryRestricted = logicalCabinClassInventoryRestricted;
	}

}