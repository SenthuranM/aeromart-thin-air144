package com.isa.thinair.airadmin.core.web.v2.handler.master;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.struts2.json.JSONException;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airmaster.api.model.SSR;
import com.isa.thinair.airpricing.api.criteria.SSRChargesSearchCriteria;
import com.isa.thinair.airpricing.api.model.SSRCharge;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.dto.SSRChargesDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author M.Rikaz
 * 
 */
public class SSRChargesRH {

	public static Page<SSRCharge> searchSSRCharges(int pageNo, SSRChargesSearchCriteria criteria) throws ModuleException {
		return ModuleServiceLocator.getChargeBD().getSSRCharges(criteria, (pageNo - 1) * 20, 20);
	}

	public static void saveSSRCharges(SSRChargesDTO ssrChargesDTO, String cos) throws ModuleException {
		ModuleServiceLocator.getChargeBD().saveSSRCharges(ssrChargesDTO, cos);// code regarding to BL move to
																				// ssrChargeBL
	}

	public static void deleteSSRCharges(Integer chargeId) throws ModuleException {
		SSRCharge extSSRCharge = ModuleServiceLocator.getChargeBD().getSSRCharges(chargeId);

		boolean chargeAssigned = ModuleServiceLocator.getReservationBD().checkSSRChargeAssignedInReservation(
				extSSRCharge.getSsrId());

		if (chargeAssigned) {
			throw new ModuleException("module.constraint.childrecord");
		} else {
			ModuleServiceLocator.getChargeBD().deleteSSRCharges(extSSRCharge);
		}

	}

	public static boolean chargeAlreadyDefined(Integer ssrId, String ssrChargeCode, Integer chargeId, ArrayList<String> ccList,
			ArrayList<String> lccList) throws ModuleException {
		return ModuleServiceLocator.getChargeBD().checkSSRChargeAlreadyDefined(ssrId, ssrChargeCode, chargeId, ccList, lccList);
	}

	public static Map<String, Integer> prepareSSRInfoMap() throws ModuleException, JSONException {
		Collection<SSR> colSSRInfo = ModuleServiceLocator.getSsrServiceBD().getSSRs();
		Map<String, Integer> ssrInfoMap = new HashMap<String, Integer>();

		if (colSSRInfo != null && colSSRInfo.size() > 0) {
			for (SSR ssr : colSSRInfo) {
				ssrInfoMap.put(ssr.getSsrId() + "", ssr.getSsrSubCategory().getCategory().getCatId());
			}
		}

		return ssrInfoMap;
	}
}
