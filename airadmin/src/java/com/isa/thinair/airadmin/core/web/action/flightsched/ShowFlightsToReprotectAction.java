/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airadmin.core.web.action.flightsched;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.handler.flightsched.FlightsToReprotectRequestHandler;
import com.isa.thinair.airinventory.api.dto.FCCInventorySummaryDTO;
import com.isa.thinair.airinventory.api.dto.FCCSegInvSummaryDTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.reporting.api.model.FlightInventorySummaryDTO;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Results({
		@Result(name = AdminStrutsConstants.AdminAction.SUCCESS, value = AdminStrutsConstants.AdminJSP.FLIGHT_SHED_REPROTECT_JSP),
		@Result(name = AdminStrutsConstants.AdminAction.ERROR, value = AdminStrutsConstants.AdminJSP.ERROR_JSP),
		@Result(name = AdminStrutsConstants.AdminAction.SUCCESS_V2, value = AdminStrutsConstants.AdminJSP.FLIGHT_SHED_REPROTECT_JSP_V2),
		@Result(name = AdminStrutsConstants.AdminAction.REPROTECTION_SEARCH, type = JSONResult.class, value = ""),
		@Result(name = AdminStrutsConstants.AdminAction.REPROTECT_FLIGTH, type = JSONResult.class, value = ""),
		@Result(name = AdminStrutsConstants.AdminAction.LOAD_MSG, value = AdminStrutsConstants.AdminJSP.LOAD_MSG) })
public class ShowFlightsToReprotectAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(ShowFlightsToReprotectAction.class);
	private Object[] rows;
	private int page;
	private int total;
	private int records;
	private String succesMsg;
	private String msgType;
	private String popupmessage;
	private String reqMessage;
	private String reqMsgType;
	private String responseRP;
	private Boolean hdnIBETransferFlights;

	private String hdnMode;
	private Boolean hdnAlert;

	private int Paging;
	private String hdnReprotectSegmentIDs;
	private Map<String, Object> updatedRPData;

	private static final String dateFormat = "dd/MM/yyyy";
	private static final String PARAM_MODE = "hdnMode";
	
	public String getResponseRP() {
		return responseRP;
	}

	public void setResponseRP(String responseRP) {
		this.responseRP = responseRP;
	}

	public Boolean getHdnAlert() {
		return hdnAlert;
	}

	public void setHdnAlert(Boolean hdnAlert) {
		this.hdnAlert = hdnAlert;
	}

	public Boolean getHdnIBETransferFlights() {
		return hdnIBETransferFlights;
	}

	public void setHdnIBETransferFlights(Boolean hdnIBETransferFlights) {
		this.hdnIBETransferFlights = hdnIBETransferFlights;
	}

	public Map<String, Object> getUpdatedRPData() {
		return updatedRPData;
	}

	public void setUpdatedRPData(Map<String, Object> updatedRPData) {
		this.updatedRPData = updatedRPData;
	}
	
	public String getHdnReprotectSegmentIDs() {
		return hdnReprotectSegmentIDs;
	}

	public void setHdnReprotectSegmentIDs(String hdnReprotectSegmentIDs) {
		this.hdnReprotectSegmentIDs = hdnReprotectSegmentIDs;
	}

	public String getReqMessage() {
		return reqMessage;
	}

	public void setReqMessage(String reqMessage) {
		this.reqMessage = reqMessage;
	}

	public String getReqMsgType() {
		return reqMsgType;
	}

	public void setReqMsgType(String reqMsgType) {
		this.reqMsgType = reqMsgType;
	}

	public String getPopupmessage() {
		return popupmessage;
	}

	public void setPopupmessage(String popupmessage) {
		this.popupmessage = popupmessage;
	}

	public int getPaging() {
		return Paging;
	}

	public void setPaging(int paging) {
		Paging = paging;
	}

	public String getHdnMode() {
		return hdnMode;
	}

	public void setHdnMode(String hdnMode) {
		this.hdnMode = hdnMode;
	}

	public Object[] getRows() {
		return rows;
	}

	public void setRows(Object[] rows) {
		this.rows = rows;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public String getSuccesMsg() {
		return succesMsg;
	}

	public void setSuccesMsg(String succesMsg) {
		this.succesMsg = succesMsg;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public String execute() throws Exception {
		return FlightsToReprotectRequestHandler.execute(request);
	}
	
	public String execute_V2() throws Exception {
		if (FlightsToReprotectRequestHandler.executeRPFilght(request, page).equals(AdminStrutsConstants.AdminAction.SUCCESS)) {
			return AdminStrutsConstants.AdminAction.SUCCESS_V2;
		} else {
			return AdminStrutsConstants.AdminAction.ERROR;
		}
	}

	public String searchRP() throws Exception {
		Collection<FlightInventorySummaryDTO> reprotectFlightsCollection = null;
		request.setAttribute(PARAM_MODE, hdnMode);
		if (FlightsToReprotectRequestHandler.executeRPFilght(request, page).equals(AdminStrutsConstants.AdminAction.SUCCESS)) {
			@SuppressWarnings("unchecked")
			Page<FlightInventorySummaryDTO> pgRPFlights = (Page<FlightInventorySummaryDTO>) request.getAttribute("pageData");
			String depDate = "";
			SimpleDateFormat dateFormater = new SimpleDateFormat("dd-MM-yyyy HH:mm");
			SimpleDateFormat sdfmt = new SimpleDateFormat(dateFormat);

			if (pgRPFlights != null) {
				this.records = pgRPFlights.getTotalNoOfRecords();
				this.total = pgRPFlights.getTotalNoOfRecords() / 20;
				int mod = pgRPFlights.getTotalNoOfRecords() % 20;
				if (mod > 0) {
					this.total = this.total + 1;
				}
				if (this.page <= 0) {
					this.page = 1;
				}

				reprotectFlightsCollection = pgRPFlights.getPageData();
				if (reprotectFlightsCollection != null && reprotectFlightsCollection.size() > 0) {
					Object[] dataRow = new Object[reprotectFlightsCollection.size()];
					int index = 1;
					Iterator<FlightInventorySummaryDTO> iter = reprotectFlightsCollection.iterator();
					while (iter.hasNext()) {
						Map<String, Object> counmap = new HashMap<String, Object>();
						FlightInventorySummaryDTO grdReprotectFlights = iter.next();

						counmap.put("id", new Integer(((page - 1) * 20) + index).toString());
						counmap.put("flightNumber", grdReprotectFlights.getFlightNumber());
						counmap.put("arrival", grdReprotectFlights.getArrival());
						counmap.put("arrivalDateTime", dateFormater.format(grdReprotectFlights.getArrivalDateTime()));
						counmap.put("depature", grdReprotectFlights.getDeparture());
						counmap.put("depatureDateTime", dateFormater.format(grdReprotectFlights.getDepartureDateTime()));

						// set via points
						List<String> routesList = grdReprotectFlights.getViaPoints();
						String strRoutes = "";
						if (routesList != null) {
							for (int q = 0; q < routesList.size(); q++) {
								strRoutes += routesList.get(q);
								if (q != (routesList.size() - 1)) {
									if ((q % 2 == 0) && (q != 0)) {
										strRoutes += "<BR>";
									} else {
										strRoutes += ",";
									}
								}
							}
						}
						counmap.put("route", strRoutes);

						// preparing inventory data
						Object[] fccInventories = grdReprotectFlights.getFccInventorySummaryDTO().toArray();
						List fccSegInventoryList = new ArrayList();
						for (Object fccInventorie : fccInventories) {
							FCCInventorySummaryDTO fltIntrySummry = (FCCInventorySummaryDTO) fccInventorie;
							fccSegInventoryList.addAll(fltIntrySummry.getFccSegInventorySummary());
						}

						int fccSegInvCount = fccSegInventoryList.size();

						String[] logicalCabinClasses = new String[fccSegInvCount];
						String[] cabinClassesDescription = new String[fccSegInvCount];
						int[] segmentAvailable = new int[fccSegInvCount];
						int[] segmentInfantAvailable = new int[fccSegInvCount];
						int[] segmentSold = new int[fccSegInvCount];
						int[] segmentOnHold = new int[fccSegInvCount];
						int[] segmentInfantSold = new int[fccSegInvCount];
						int[] segmentInfantOnHold = new int[fccSegInvCount];
						String[] segments = new String[fccSegInvCount];
						int[] segmentID = new int[fccSegInvCount];

						/*
						 * 
						 * Preparing the allocations details ------------> LogicalCabinClass wise info | | | \|/ Segment
						 * wise info V
						 */

						for (int q = 0; q < fccSegInvCount; q++) {
							FCCSegInvSummaryDTO fccSegInvSummary = (FCCSegInvSummaryDTO) fccSegInventoryList.get(q);

							String logicalCabinClassCode = fccSegInvSummary.getLogicalCabinClassCode();
							logicalCabinClasses[q] = logicalCabinClassCode;
							// CabinClass cc =
							// ModuleServiceLocator.getAircraftServiceBD().getCabinClass(logicalCabinClassCode);
							// cabinClassesDescription[p] = (cc != null) ? cc.getCabinClassCode() + ":" +
							// cc.getDescription() :
							// ":";

							segmentAvailable[q] = fccSegInvSummary.getAvailableSeats();
							segmentInfantAvailable[q] = fccSegInvSummary.getAvailableInfantSeats();
							segmentOnHold[q] = fccSegInvSummary.getAdultOnhold();
							segmentSold[q] = fccSegInvSummary.getAdultSold();
							segmentInfantSold[q] = fccSegInvSummary.getInfantSold();
							segmentInfantOnHold[q] = fccSegInvSummary.getInfantOnhold();
							segments[q] = fccSegInvSummary.getSegmentCode();
							segmentID[q] = fccSegInvSummary.getFlightSegmentId();

						}

						StringBuffer segmentAvailableBuff = new StringBuffer();
						StringBuffer segmentOnHoldBuff = new StringBuffer();
						StringBuffer segmentSoldBuff = new StringBuffer();
						StringBuffer logicalCabinClassBuff = new StringBuffer();
						StringBuffer cabinClassDesBuff = new StringBuffer();
						List<String> logicalCabinClassesList = new ArrayList<String>();
						StringBuffer segmentsBuff = new StringBuffer();
						StringBuffer segmentsIDBuff = new StringBuffer();

						for (int n = 0; n < fccSegInvCount; n++) {
							segmentAvailableBuff.append(segmentAvailable[n] + "/" + segmentInfantAvailable[n]
									+ ((n == (fccSegInvCount - 1)) ? "<BR>" : ","));
							segmentOnHoldBuff.append(segmentOnHold[n] + "/" + segmentInfantOnHold[n]
									+ ((n == (fccSegInvCount - 1)) ? "<BR>" : ","));
							segmentSoldBuff.append(segmentSold[n] + "/" + segmentInfantSold[n]
									+ ((n == (fccSegInvCount - 1)) ? "<BR>" : ","));
							segmentsBuff.append(segments[n] + ((n == (fccSegInvCount - 1)) ? "<BR>" : ","));
							segmentsIDBuff.append(segmentID[n] + ((n == (fccSegInvCount - 1)) ? "<BR>" : ","));
							logicalCabinClassBuff.append(logicalCabinClasses[n] + ((n == (fccSegInvCount - 1)) ? "<BR>" : ","));
							cabinClassDesBuff.append(cabinClassesDescription[n] + ((n == (fccSegInvCount - 1)) ? "<BR>" : ","));
							if (!logicalCabinClassesList.contains(logicalCabinClasses[n])) {
								logicalCabinClassesList.add(logicalCabinClasses[n]);
							}
						}
						counmap.put("segmentAvailable", segmentAvailableBuff.toString());
						counmap.put("segmentSold", segmentSoldBuff.toString());
						counmap.put("segmentOnHold", segmentOnHoldBuff.toString());
						counmap.put("flightId", grdReprotectFlights.getFlightId());
						counmap.put("scheduleId", grdReprotectFlights.getScheduleId());
						counmap.put("modelNumber", grdReprotectFlights.getModelNumber());

						// segment Map
						StringBuffer buffSegments = new StringBuffer();
						StringBuffer buffSegIDs = new StringBuffer();
						@SuppressWarnings("unchecked")
						Map<Integer, String> segmentsMap = grdReprotectFlights.getSegmentMap();
						if (segmentsMap != null) {
							Set<Integer> segmentsSet = segmentsMap.keySet();
							Iterator<Integer> keyIterator = segmentsSet.iterator();
							if (segmentsSet != null) {
								while (keyIterator.hasNext()) {
									Integer segmentId = keyIterator.next();
									String segmentCode = segmentsMap.get(segmentId);
									buffSegments.append(segmentCode + ",");
									buffSegIDs.append(segmentId + ",");
								}
							}
						}
						counmap.put("segmentMap", buffSegments.toString());

						// logicalLcc
						StringBuffer logicalCCBuff = new StringBuffer();
						if (logicalCabinClasses != null && logicalCabinClasses.length > 0) {
							for (String logicalCabinClasse : logicalCabinClasses) {
								logicalCCBuff.append(logicalCabinClasse + ":");
							}
						}
						counmap.put("logicalLcc", logicalCCBuff.toString());
						counmap.put("segIds", buffSegIDs.toString());
						counmap.put("logicalCabinClass", logicalCabinClassBuff.toString());
						counmap.put("segments", segmentsBuff.toString());
						counmap.put("segmentsID", segmentsIDBuff.toString());

						// set depDate
						depDate = "";
						try {
							depDate = sdfmt.format(dateFormater.parse(dateFormater.format(grdReprotectFlights
									.getDepartureDateTime())));
						} catch (Exception e) {
							log.error("Error in formatting date" + e.getMessage());
							depDate = "";
						}
						counmap.put("depDate", depDate);
						counmap.put("connection", "");
						counmap.put("cabinClassDes", cabinClassDesBuff.toString());

						dataRow[index - 1] = counmap;
						index++;
					}
					this.rows = dataRow;
				}
			} else {

				this.records = 0;
				this.total = 0;
				this.page = 0;
				this.rows = null;
			}
		}
		return AdminStrutsConstants.AdminAction.REPROTECTION_SEARCH;
	}

	public String reprotect() throws Exception {
		
		if (FlightsToReprotectRequestHandler.executeRPFilght(request, page).equals(AdminStrutsConstants.AdminAction.SUCCESS)) {
			this.updatedRPData = FlightsToReprotectRequestHandler.getReprotectDataByCCCode_V2(request);

			setPopupmessage(nullHandler(request.getAttribute(WebConstants.POPUPMESSAGE)));
			setReqMessage(nullHandler(request.getAttribute(WebConstants.REQ_MESSAGE)));
			setReqMsgType(nullHandler(request.getAttribute(WebConstants.MSG_TYPE)));
			setResponseRP(AdminStrutsConstants.AdminAction.SUCCESS);
		} else {
			setResponseRP(AdminStrutsConstants.AdminAction.ERROR);
		}
		return AdminStrutsConstants.AdminAction.REPROTECT_FLIGTH;
	}

	private String nullHandler(Object object) {
		if(object !=null ) {
			return object.toString();
		}
		return "";
	}
	public String loadMsg() {
		return AdminStrutsConstants.AdminAction.LOAD_MSG;
	}
}
