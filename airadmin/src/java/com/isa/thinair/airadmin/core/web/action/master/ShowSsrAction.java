package com.isa.thinair.airadmin.core.web.action.master;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.handler.master.SsrRequestHandler;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

/**
 * @author Dhanya
 */
@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Results({ @Result(name = AdminStrutsConstants.AdminAction.SUCCESS, value = AdminStrutsConstants.AdminJSP.SSR_ADMIN_JSP),
		@Result(name = AdminStrutsConstants.AdminAction.ERROR, value = AdminStrutsConstants.AdminJSP.ERROR_JSP) })
public class ShowSsrAction extends BaseRequestAwareAction {

	public String execute() throws Exception {
		return SsrRequestHandler.execute(request);
	}

}
