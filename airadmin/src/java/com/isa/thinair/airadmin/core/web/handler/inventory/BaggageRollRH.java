/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airadmin.core.web.handler.inventory;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airadmin.core.web.util.SeatInventoryHelper;
import com.isa.thinair.airinventory.api.dto.InvRollForwardFlightsSearchCriteria;
import com.isa.thinair.airinventory.api.service.BaggageBusinessDelegate;
import com.isa.thinair.airschedules.api.dto.FlightSummaryDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.commons.api.constants.DayOfWeek;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.platform.api.ServiceResponce;

/**
 * 
 * @author Harsha
 */
public class BaggageRollRH extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(MIRollforwardRequestHandler.class);

	private static String clientErrors;

	private static Object lock = new Object();

	private static SeatInventoryHelper seatInventoryHelper = new SeatInventoryHelper();

	private static final String PARAM_MODE = "hdnMode";

	private static final String PARAM_FROMDATE = "txtFromDate";

	private static final String PARAM_TODATE = "txtToDate";

	private static final String PARAM_SUNDAY = "chkSunday";

	private static final String PARAM_MONDAY = "chkMonday";

	private static final String PARAM_TUESDAY = "chkTuesday";

	private static final String PARAM_WEDNESDAY = "chkWednesday";

	private static final String PARAM_THURSDAY = "chkThursday";

	private static final String PARAM_FRIDAY = "chkFriday";

	private static final String PARAM_SATURDAY = "chkSaturday";

	private static final String PARAM_SELECTED_SEGS = "hdnSelectedSegs";

	private static final String PARAM_FLIGHT_ID = "hdnFlightIDRF";

	private static final String PARAM_ORIGIN = "hdnOrigin";

	private static final String PARAM_DESTINATION = "hdnDestination";

	private static final String PARAM_FLIGHT_NUMBER = "hdnFlightNumber";

	// private static final String PARAM_ROLL_TYPE = "roll";

	private static final String PARAM_ROLL_TYPE = "rollType";

	private static final String PARAM_COS = "cos";

	/**
	 * Execte Method for Roll Forward the Baggage Charges
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {
		if (log.isDebugEnabled())
			log.debug("Begin Baggage charge Roll forward:" + System.currentTimeMillis());

		String strHdnMode = request.getParameter(PARAM_MODE);
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;

		String rollType = request.getParameter(PARAM_ROLL_TYPE);

		if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_SAVE)) {
			try {
				saveData(request);
			} catch (ModuleException mex) {
				log.error("Exception in  Baggage charge Roll forward:", mex);
				saveMessage(request, mex.getMessageString(), WebConstants.MSG_ERROR);

			} catch (Exception exception) {
				log.error("Exception in  Baggage charge Roll forward:", exception);
				if (exception instanceof RuntimeException) {
					forward = WebConstants.FORWARD_ERROR;
					JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
				} else {
					saveMessage(request, exception.getMessage(), WebConstants.MSG_ERROR);
				}
			}
		}

		try {
			request.setAttribute(PARAM_ROLL_TYPE, rollType);
			setDisplayData(request);
			if (!(strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_SAVE))) {
				setSelectedSegments(request);
			}
		} catch (ModuleException mex) {
			log.error("Exception in  Baggage charge Roll forward:", mex);
			saveMessage(request, mex.getMessage(), WebConstants.MSG_ERROR);

		} catch (Exception exception) {
			log.error("Exception in  Baggage charge Roll forward:", exception);
			if (exception instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
			} else {
				saveMessage(request, exception.getMessage(), WebConstants.MSG_ERROR);
			}
		}
		if (log.isDebugEnabled())
			log.debug("End Baggage charge Roll forward:" + System.currentTimeMillis());
		return forward;
	}

	/**
	 * Saves the Baggage Roll Forward
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	private static void saveData(HttpServletRequest request) throws Exception {

		String strSelectedSeg = request.getParameter(PARAM_SELECTED_SEGS);
		String strFromDate = request.getParameter(PARAM_FROMDATE);
		String strToDate = request.getParameter(PARAM_TODATE);
		String flightId = request.getParameter(PARAM_FLIGHT_ID);
		String origin = request.getParameter(PARAM_ORIGIN);
		String destination = request.getParameter(PARAM_DESTINATION);
		String flightNo = request.getParameter(PARAM_FLIGHT_NUMBER);
		// String roll = request.getParameter(PARAM_ROLL_TYPE);
		String classOfService = request.getParameter(PARAM_COS);
		ServiceResponce resp = null;

		String reqSelectedDataJS = "";
		reqSelectedDataJS += "reqMode = 'afterSave';";
		reqSelectedDataJS += "var ls = new Listbox('Flight Segments', 'Selected Flight Segments', 'spn1');";
		reqSelectedDataJS += "var strSelectedSegmentsAndBCs = '" + request.getParameter(PARAM_SELECTED_SEGS) + "';";
		reqSelectedDataJS += "var strNotSelectedSegmentsAndBCs = '" + request.getParameter("hdnNotSelectedSegs") + "';";
		reqSelectedDataJS += "var strSelectedData = '" + strFromDate + "," + strToDate + "';";

		InvRollForwardFlightsSearchCriteria searchCriteriaDTO = new InvRollForwardFlightsSearchCriteria();

		// preparing and setting the segment and booking codes
		if (strSelectedSeg != null) {
			String[] segsArr = StringUtils.split(strSelectedSeg, "|");
			List<String> bookingCodes = new ArrayList<String>();
			for (int i = 0; i < segsArr.length; i++) {
				searchCriteriaDTO.addSegmentBookingCodes(segsArr[i], bookingCodes);
			}
		}

		// setting the flight number

		searchCriteriaDTO.setFlightNumber(flightNo);

		Collection<DayOfWeek> daysCol = new ArrayList<DayOfWeek>();

		// preparing the frequency
		reqSelectedDataJS += "var selectedFrequency ='";
		if (request.getParameter(PARAM_SUNDAY) != null && request.getParameter(PARAM_SUNDAY).equals(WebConstants.CHECKBOX_ON)) {
			daysCol.add(DayOfWeek.SUNDAY);
			reqSelectedDataJS += "Sun,";
		}
		if (request.getParameter(PARAM_MONDAY) != null && request.getParameter(PARAM_MONDAY).equals(WebConstants.CHECKBOX_ON)) {
			daysCol.add(DayOfWeek.MONDAY);
			reqSelectedDataJS += "Mon,";
		}

		if (request.getParameter(PARAM_TUESDAY) != null && request.getParameter(PARAM_TUESDAY).equals(WebConstants.CHECKBOX_ON)) {
			daysCol.add(DayOfWeek.TUESDAY);
			reqSelectedDataJS += "Tue,";
		}

		if (request.getParameter(PARAM_WEDNESDAY) != null
				&& request.getParameter(PARAM_WEDNESDAY).equals(WebConstants.CHECKBOX_ON)) {
			daysCol.add(DayOfWeek.WEDNESDAY);
			reqSelectedDataJS += "Wed,";
		}

		if (request.getParameter(PARAM_THURSDAY) != null && request.getParameter(PARAM_THURSDAY).equals(WebConstants.CHECKBOX_ON)) {
			daysCol.add(DayOfWeek.THURSDAY);
			reqSelectedDataJS += "Thu,";
		}

		if (request.getParameter(PARAM_FRIDAY) != null && request.getParameter(PARAM_FRIDAY).equals(WebConstants.CHECKBOX_ON)) {
			daysCol.add(DayOfWeek.FRIDAY);
			reqSelectedDataJS += "Fri,";
		}

		if (request.getParameter(PARAM_SATURDAY) != null && request.getParameter(PARAM_SATURDAY).equals(WebConstants.CHECKBOX_ON)) {
			daysCol.add(DayOfWeek.SATURDAY);
			reqSelectedDataJS += "Sat,";
		}
		reqSelectedDataJS += "';";
		request.setAttribute("reqSelectedDataJS", reqSelectedDataJS);
		// setting the frequency
		searchCriteriaDTO.setFrequency(CalendarUtil.getFrequencyFromDays(daysCol));

		searchCriteriaDTO.setOriginAirportCode(origin);

		// setting the origin airport
		searchCriteriaDTO.setDestinationAirportCode(destination);

		// preparing and setting the from date
		SimpleDateFormat dtfmt = new SimpleDateFormat("dd/MM/yyyy");
		Date fromDate = null;

		fromDate = dtfmt.parse(strFromDate);
		searchCriteriaDTO.setFromDate(fromDate);

		// preparing and setting the to date
		Date toDate = null;

		toDate = dtfmt.parse(strToDate);
		Calendar calToDate = new GregorianCalendar();
		calToDate.setTime(toDate);
		calToDate.set(Calendar.HOUR, 23);
		calToDate.set(Calendar.MINUTE, 59);
		calToDate.set(Calendar.SECOND, 59);
		calToDate.set(Calendar.MILLISECOND, 999);
		searchCriteriaDTO.setToDate(calToDate.getTime());

		searchCriteriaDTO.setRestrictNumberOfRecords(false);

		// preparing the flight IDs
		Set<Integer> flightIds = new HashSet<Integer>();
		Collection<FlightSummaryDTO> collectionFligthSummary = ModuleServiceLocator.getFlightServiceBD().getLikeFlightSummary(
				searchCriteriaDTO);
		if (collectionFligthSummary != null && collectionFligthSummary.size() > 0) {
			FlightSummaryDTO flightSummaryDTO = null;
			Iterator<FlightSummaryDTO> iter = collectionFligthSummary.iterator();

			while (iter.hasNext()) {
				flightSummaryDTO = (FlightSummaryDTO) iter.next();
				int flightID = flightSummaryDTO.getFlightId();
				flightIds.add(new Integer(flightID));
			}
		}

		String[] segsArr = null;
		ArrayList<String> lstSelectedSeg = new ArrayList<String>();
		if (strSelectedSeg != null) {
			segsArr = StringUtils.split(strSelectedSeg, "|");
			for (int i = 0; i < segsArr.length; i++) {
				lstSelectedSeg.add(segsArr[i]);
			}
		}
		// If no matching flights found, throw the exception. No need to call
		// the BD method if no
		// matching flights found
		if (flightIds.size() > 0) {

			BaggageBusinessDelegate baggageBd = ModuleServiceLocator.getBaggageBD();
			resp = baggageBd.assignFlightBaggageChargesRollFoward(Integer.parseInt(flightId), flightIds, lstSelectedSeg,
					classOfService);

			if (resp != null && resp.isSuccess()) {
				saveMessage(request, "Rollforward Success", WebConstants.MSG_SUCCESS);
			} else {
				saveMessage(request, "Rollforward Failed", WebConstants.MSG_ERROR);
			}
		} else {
			saveMessage(request, "No matching flight found for rollforwarding", WebConstants.MSG_ERROR);
		}

	}

	/**
	 * Sets the display Methods
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setDisplayData(HttpServletRequest request) throws ModuleException {
		setFlightDetails(request);
		setClientErrors(request);
		setSegmentSelectHtml(request);
		setPrivilagesHtml(request);

	}

	/**
	 * Sets the Segmnt data to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setSegmentSelectHtml(HttpServletRequest request) throws ModuleException {
		int flightId = Integer.parseInt(request.getParameter("fltId"));
		request.setAttribute(WebConstants.REQ_SEGMENT_LIST, JavascriptGenerator.createSegmentsListHTML(flightId));
	}

	/**
	 * Sets the selected Frequency and Segments
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setSelectedSegments(HttpServletRequest request) throws ModuleException {

		int flightId = Integer.parseInt(request.getParameter("fltId"));
		request.setAttribute("reqFrequncy", seatInventoryHelper.getFrequencyForFlight(flightId));
		String reqSelectedDataJS = "reqMode = 'display';";
		reqSelectedDataJS += "var ls = new Listbox('Flight Segments', 'Selected Flight Segments', 'spn1');";
		request.setAttribute("reqSelectedDataJS", reqSelectedDataJS);
	}

	/**
	 * Sets the client Validations
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setClientErrors(HttpServletRequest request) throws ModuleException {
		String strClientErrors = getClientErrors(request);
		if (strClientErrors == null) {
			strClientErrors = "";
		}
		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
	}

	/**
	 * Sets the Flight Details or display
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setFlightDetails(HttpServletRequest request) throws ModuleException {

		int flightId = Integer.parseInt(request.getParameter("fltId"));
		FlightBD flightBd = ModuleServiceLocator.getFlightServiceBD();
		Flight flight = flightBd.getFlight(flightId);
		StringBuilder sb = new StringBuilder();
		SimpleDateFormat sdt = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		if (flight != null) {
			sb.append("flightData[0] = '" + flight.getFlightNumber() + "';");
			sb.append("flightData[1] = '" + flight.getOriginAptCode() + "';");
			sb.append("flightData[2] = '" + flight.getDestinationAptCode() + "';");
			sb.append("flightData[3] = '" + sdt.format(flight.getDepartureDate()) + "';");
			sb.append("flightData[4] = '" + flight.getFlightId() + "';");
		}
		request.setAttribute("reqRollBaggageFlight", sb.toString());
		request.setAttribute("classOfService", request.getParameter(PARAM_COS));
	}

	/**
	 * Creates Client validations for Meal Roll Forward Page using the inventory validates
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Client Validations Array
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {
		synchronized (lock) {
			if (clientErrors == null) {
				Properties moduleErrs = new Properties();

				moduleErrs.setProperty("um.mealinventory.form.rollforward.fromdate.required", "rollForwardFromDateRqrd");
				moduleErrs.setProperty("um.mealinventory.form.rollforward.fromdate.lessthan.currenctdate",
						"rollForwardFromDateLessthanCurrentDate");
				moduleErrs.setProperty("um.mealinventory.form.rollforward.todate.required", "rollForwardToDateRqrd");
				moduleErrs.setProperty("um.mealinventory.form.rollforward.todate.notlessthan.fromdate",
						"rollForwardFromDateLessthanFromDate");
				moduleErrs.setProperty("um.mealinventory.form.rollforward.frequency.required", "rollForwardFrequencyRqrd");
				moduleErrs.setProperty("um.mealinventory.form.rollforward.segmentlist.notselected", "rollForwardSegmentRqrd");
				moduleErrs.setProperty("um.mealinventory.form.rollforward.bcallocation.checked", "rollForwardBCAllocRqrd");
				moduleErrs.setProperty("um.mealinventory.form.rollforward.notdone", "rollForwardNoMatchingBC");

				clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
			}
			return clientErrors;
		}
	}
	
	private static void setPrivilagesHtml(HttpServletRequest request) throws ModuleException {

		HashMap mapPrivileges = (HashMap) request.getSession().getAttribute(WebConstants.SES_PRIVILEGE_IDS);
		boolean overrideFrequencyOnRollFwd = (mapPrivileges.get(WebConstants.OVERRIDE_FREQUENCY_ON_ROLL_FWD) != null) ? true
				: false;
		request.setAttribute(WebConstants.REQ_HTML_OVERRIDE_FREQUENCY_ON_ROLL_FWD, overrideFrequencyOnRollFwd);
	}

}
