package com.isa.thinair.airadmin.core.web.generator.security;

import java.io.File;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXParseException;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.StaticFileNameUtil;

public class MenuHTMLGenerator {

	private static Log log = LogFactory.getLog(MenuHTMLGenerator.class);

	/**
	 * Creates the MENU using XML File
	 * 
	 * @param menuFile
	 *            the Menu XML file
	 * @param hMPriviledgesIDs
	 *            the HashMap Containing Privileges of the User
	 * @return String the Array containing the MENU
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String createMenuHtml(String menuFile, HashMap<String, String> hMPriviledgesIDs) throws ModuleException {
		StringBuffer strData = new StringBuffer();
		strData.append(readXMLFile(menuFile, hMPriviledgesIDs));
		return strData.toString();
	}

	/**
	 * Read the XML file & Create the Menu Array (Parse)
	 * 
	 * @param menuFile
	 *            the XML file containing Menu
	 * @param hMPriviledgesIDs
	 *            the Users Privileges
	 * @return String the Array Containing the MENU
	 */
	public static String readXMLFile(String menuFile, HashMap<String, String> hMPriviledgesIDs) {
		StringBuffer strReturnDataMenu = new StringBuffer();

		try {

			String strFileName = menuFile;
			int intCount = 0;
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(new File(strFileName));

			doc.getDocumentElement().normalize();
			NodeList listOfRecords = doc.getElementsByTagName("r");
			int intRecords = listOfRecords.getLength();
			String strSpace = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

			String strMT = "";
			String strMID = "";
			String strDes = "";
			String strFID = "";
			String strSep = "";
			String strURL = "";
			String strSL = "";
			String strPrev = "";
			String strTab = "";

			// Read the File
			for (int i = 0; i < intRecords; i++) {
				Node menuNode = listOfRecords.item(i);
				NamedNodeMap attributes = menuNode.getAttributes();

				strMT = attributes.getNamedItem("C1").getNodeValue().toString();
				strMID = attributes.getNamedItem("C2").getNodeValue().toString();
				strDes = attributes.getNamedItem("C3").getNodeValue().toString();
				strFID = attributes.getNamedItem("C4").getNodeValue().toString();
				strSep = attributes.getNamedItem("C5").getNodeValue().toString();
				strURL = attributes.getNamedItem("C6").getNodeValue().toString();
				if (strURL != null && strURL.indexOf("#") != -1) {
					int start = strURL.indexOf("#", 0);
					int end = strURL.indexOf("#", start + 1);
					if (start != -1 && end != -1) {
						String replaceStr = strURL.substring(start + 1, end);
						if (replaceStr != null && replaceStr.equals("XBE_URL")) {
							strURL = strURL.substring(0, start)
									+ AiradminModuleUtils.getGlobalConfig().getXbeServerAddressAndPort()
									+ strURL.substring(end + 1);
						}
					}
				}
				strSL = attributes.getNamedItem("C7").getNodeValue().toString();
				strPrev = attributes.getNamedItem("C9").getNodeValue().toString();
				strTab = attributes.getNamedItem("C10").getNodeValue().toString();
				strSpace = "&nbsp;&nbsp;&nbsp;&nbsp;";

				// create the Menu
				boolean blnFound = true;
				if (!strMT.equals("M")) {
					blnFound = checkAvailability(i, intRecords, strMID, listOfRecords, hMPriviledgesIDs);
				}

				if (blnFound) {
					if (strSL.equals("")) {
						strSL = "menu-top";
						strSpace = "";
					}

					// Sub Menu ----------------
					if (strMT.equals("S")) {
						strReturnDataMenu.append(buildMenuArray(intCount, "S", strSL, strDes + strSpace, strURL, "", strMID,
								strTab));
						intCount++;
						if (strSep.equals("1")) {
							if (checkSepAvailability(i, intRecords - 1, strSL, listOfRecords, hMPriviledgesIDs)) {
								strReturnDataMenu.append(buildMenuArray(intCount, "L", strSL, "", "", "", "", strTab));
								intCount++;
							}
						}
					}

					// Menu -------------------
					if (strMT.equals("M")) {
						if (strSL.equals("menu-top")) {
							strMID = strSL;
						}
						if (checkFID(strPrev, hMPriviledgesIDs)) {
							strReturnDataMenu.append(buildMenuArray(intCount, "M", strMID, strDes, strURL, strFID, "", strTab));
							intCount++;

							if (strSep.equals("1")) {
								if (checkSepAvailability(i, intRecords - 1, strSL, listOfRecords, hMPriviledgesIDs)) {
									strReturnDataMenu.append(buildMenuArray(intCount, "L", strMID, "", "", "", "", strTab));
									intCount++;
								}
							}
						}
					}
				}
			}

			// create the Quick Launcher
			String[] arrQL = new String[9];
			arrQL[0] = StaticFileNameUtil.getCorrected("AA149.gif")
					+ ",66,showSchedule.action,SC_SCHD_001,plan.flight.sch,Load Schedules,Schedules";
			arrQL[1] = StaticFileNameUtil.getCorrected("AA150.gif")
					+ ",54,showFlight.action,SC_FLGT_001,plan.flight.flt,Load flights,Flights";
			arrQL[2] = StaticFileNameUtil.getCorrected("AA151.gif")
					+ ",93,showBookingCodes.action,SC_ADMN_005,plan.invn.bc,Load booking class,Booking Classes";
			arrQL[3] = StaticFileNameUtil.getCorrected("AA152.gif")
					+ ",63,showLoadJsp!loadAllocateSearch.action,SC_INVN_001,plan.invn.alloc,Load allocate seats,Allocate";
			arrQL[4] = StaticFileNameUtil.getCorrected("AA153.gif")
					+ ",66,showOptimizeSearch.action,SC_INVN_004,plan.invn.opt,Load seat optimise,Seat Optimise";
			arrQL[5] = StaticFileNameUtil.getCorrected("AA154.gif")
					+ ",77,showLoadFareJsp!manageRule.action?mode=CREATEFARE,SC_ADMN_018,plan.fares.rule,Load fare rules,Fare Rules";
			arrQL[6] = StaticFileNameUtil.getCorrected("AA155.gif")
					+ ",98,showCharges.action,SC_ADMN_010,plan.fares.charge,Load setup charges,Setup Charges";
			arrQL[7] = StaticFileNameUtil.getCorrected("AA156.gif")
					+ ",87,showLoadFareJsp!showSetupFare.action,SC_INVN_020,plan.fares.setup,Load setup fares,Setup Fares";
			arrQL[8] = StaticFileNameUtil.getCorrected("M009.gif")
					+ ",108,showFlightLoad.action,SC_FLGT_002,admin.fla,Flt. Load Analysis,Flt. Load Analysis";

			int intQCount = 0;
			for (int i = 0; i < arrQL.length; i++) {
				String[] arrQD = arrQL[i].split(",");
				if (checkFID(arrQD[4], hMPriviledgesIDs)) {
					strReturnDataMenu.append("arrQuickLaunch[" + intQCount + "]	= new Array('" + arrQD[0] + "','" + arrQD[1]
							+ "','" + arrQD[2] + "','" + arrQD[3] + "','" + arrQD[4] + "','" + arrQD[5] + "', '" + arrQD[6]
							+ "');");
					intQCount++;
				}
			}

		} catch (SAXParseException err) {

			log.error("** Parsing error" + ", line " + err.getLineNumber() + ", uri " + err.getSystemId());
			log.error(err.getMessage());

		} catch (Throwable t) {
			log.error(t);
		}
		return strReturnDataMenu.toString();
	}

	/**
	 * Check availability For Menu
	 * 
	 * @param intStart
	 *            the starting int
	 * @param intLength
	 *            the length
	 * @param strMenuID
	 *            the Menu Id
	 * @param nodeList
	 *            the NODE list
	 * @param hMPriviledgesIDs
	 *            the Map Containing the Privilegs
	 * @return boolean available-true not-false
	 */
	private static boolean checkAvailability(int intStart, int intLength, String strMenuID, NodeList nodeList,
			HashMap<String, String> hMPriviledgesIDs) {
		boolean blnReturn = false;
		String[] arrVal = strMenuID.split("0");
		intStart = intStart + 1;
		for (int x = intStart; x < intLength; x++) {
			String strMID = nodeList.item(x).getAttributes().getNamedItem("C2").getNodeValue().toString();
			String strPrev = nodeList.item(x).getAttributes().getNamedItem("C9").getNodeValue().toString();

			String[] arrCVal = strMID.split("0");
			if (arrCVal[0].length() >= arrVal[0].length()) {
				if (arrCVal[0].substring(0, arrVal[0].length()).equals(arrVal[0])
						&& nodeList.item(x).getAttributes().getNamedItem("C1").getNodeValue().toString().equals("M")) {
					if (strPrev.equals("") || strPrev.equals(null)) {
						blnReturn = true;
						break;
					} else {
						if (checkFID(strPrev, hMPriviledgesIDs)) {
							blnReturn = true;
							break;
						}
					}
				}
			}
		}
		return blnReturn;
	}

	/**
	 * Checks Space Avalability
	 * 
	 * @param intStart
	 *            the Starting int
	 * @param intLength
	 *            the length
	 * @param strMenuID
	 *            the Menu Id
	 * @param nodeList
	 *            the Node List
	 * @param hMPriviledgesIDs
	 *            the map containing the privileges
	 * @return boolean available-true not-false
	 */
	private static boolean checkSepAvailability(int intStart, int intLength, String strMenuID, NodeList nodeList,
			HashMap<String, String> hMPriviledgesIDs) {
		boolean blnReturn = false;

		intStart = intStart + 1;
		for (int x = intStart; x < intLength; x++) {
			String strSL = nodeList.item(x).getAttributes().getNamedItem("C7").getNodeValue().toString();
			String strMID = nodeList.item(x).getAttributes().getNamedItem("C2").getNodeValue().toString();
			String strMT = nodeList.item(x).getAttributes().getNamedItem("C1").getNodeValue().toString();
			if (strSL.equals(strMenuID)) {
				if (strMT.equals("M")) {
					blnReturn = true;
					break;
				} else {
					if (checkAvailability(x, intLength, strMID, nodeList, hMPriviledgesIDs)) {
						blnReturn = true;
						break;
					}
				}
			}
		}
		return blnReturn;
	}

	/**
	 * builds the Menu Array
	 * 
	 * @param intCount
	 *            the array count
	 * @param strMenuType
	 *            the Menu Type sub/main
	 * @param strMenuID
	 *            the Menu Id
	 * @param strMenuDesc
	 *            the Menu Description name
	 * @param strCallPage
	 *            the Address
	 * @param strFID
	 *            the fid
	 * @param strMGroup
	 *            the Group (group)
	 * @param strTabDes
	 *            the Tab Descrption (tab name)
	 * @return
	 */
	private static String buildMenuArray(int intCount, String strMenuType, String strMenuID, String strMenuDesc,
			String strCallPage, String strFID, String strMGroup, String strTabDes) {
		StringBuffer strReturnData = new StringBuffer();
		strReturnData.append("arrMenu[" + intCount + "] = new Array();");
		strReturnData.append("arrMenu[" + intCount + "][0] = '" + strMenuType + "';");
		strReturnData.append("arrMenu[" + intCount + "][1] = '" + strMenuID + "';");
		strReturnData.append("arrMenu[" + intCount + "][2] = '" + strMenuDesc + "';");
		strReturnData.append("arrMenu[" + intCount + "][3] = '" + strCallPage + "';");
		strReturnData.append("arrMenu[" + intCount + "][4] = '" + strFID + "';");
		strReturnData.append("arrMenu[" + intCount + "][5] = '" + strMGroup + "';");
		strReturnData.append("arrMenu[" + intCount + "][6] = '" + strTabDes + "';");
		return strReturnData.toString();
	}

	/**
	 * Checks privilege to Access
	 * 
	 * @param strPrevID
	 *            privilege Id
	 * @param hMPriviledgesIDs
	 *            the Hash map Having Privileges of the User
	 * @return boolean can-true cannot-false
	 */
	private static boolean checkFID(String strPrevID, HashMap<String, String> hMPriviledgesIDs) {
		boolean blnReturn = false;
		if (strPrevID != "") {
			if (hMPriviledgesIDs != null) {
				if (hMPriviledgesIDs.get(strPrevID) != null) {
					blnReturn = true;
				}
			}
		} else {
			blnReturn = true;
		}
		return blnReturn;
	}

}
