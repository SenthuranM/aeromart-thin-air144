package com.isa.thinair.airadmin.core.web.handler.reports;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

public class AVSSeatReportRH extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(AVSSeatReportRH.class);

	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	/**
	 * The constructor.
	 */
	public AVSSeatReportRH() {
		super();
	}

	/**
	 * @param request
	 * @return
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {

		String strHdnMode = request.getParameter("hdnMode");

		String forward = AdminStrutsConstants.AdminAction.SUCCESS;

		try {
			setDisplayData(request);
			log.debug("AVSSeatReportRH success");
		} catch (Exception e) {
			forward = AdminStrutsConstants.AdminAction.ERROR;
			log.error("AVSSeatReportRH execute()" + e.getMessage());
		}
		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				log.error("AVSSeatReportRH setReportView Success");
				return null;
			} else {
				log.error("AVSSeatReportRH setReportView not selected");
			}

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("AVSSeatReportRH setReportView Failed " + e.getMessageString());
		}

		return forward;
	}

	/**
	 * 
	 * @param request
	 * @throws Exception
	 */
	public static void setDisplayData(HttpServletRequest request) throws Exception {
		setGDSCode(request);
		setBCCode(request);
		setEventCode(request);
		setSegmentCode(request);

	}

	protected static void setGDSCode(HttpServletRequest request) throws ModuleException {

		String strGDSCode = SelectListGenerator.createGDSList();
		request.setAttribute(WebConstants.REQ_HTML_GDS_CODE_SELECT_LIST, strGDSCode);

	}

	protected static void setBCCode(HttpServletRequest request) throws ModuleException {

		String strBookingCode = SelectListGenerator.createBookingCodeList();
		request.setAttribute(WebConstants.REQ_HTML_BOOKINGCODE_SELECT_LIST, strBookingCode);
	}

	protected static void setEventCode(HttpServletRequest request) throws ModuleException {

		String strEventCode = SelectListGenerator.createEventCodeList();
		request.setAttribute(WebConstants.REQ_HTML_EVENT_CODE_SELECT_LIST, strEventCode);
	}

	protected static void setSegmentCode(HttpServletRequest request) throws ModuleException {

		String strSegmentCode = SelectListGenerator.createActiveAirportCodeList();
		request.setAttribute(WebConstants.REQ_HTML_STATION_SELECT_LIST, strSegmentCode);
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		ResultSet resultSet = null;
		String value = request.getParameter("radReportOption");

		String gdsCode = request.getParameter("selGDSCode");
		String bcCode = request.getParameter("selBC");
		String eventCode = request.getParameter("selEventCode");
		String flightNum = request.getParameter("txtFlightNum");
		String depTimeLocal = request.getParameter("txtDepTimeLocal");
		String segmentCodeFrom = request.getParameter("selSegmentFrom");
		String segmentCodeTo = request.getParameter("selSegmentTo");

		String reportTemplate = "AVSSeatsReport.jasper";
		String strLogo = AppSysParamsUtil.getReportLogo(false);// globalConfig.getBizParam(SystemParamKeys.REPORT_LOGO);
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String strPath = "../../images/" + AppSysParamsUtil.getReportLogo(true);
		String strReportId = "UC_REPM_022";
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);

		try {
			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs(WebConstants.REPORT_REF,
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));

			ReportsSearchCriteria search = new ReportsSearchCriteria();

			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}
			search.setGdsCode(gdsCode);
			search.setBcCode(bcCode);
			search.setEventCode(eventCode);
			search.setFlightNumber(flightNum);
			search.setDepTimeLocal(depTimeLocal);
			// search.setSegment(segmentCodeFrom);
			// search.setSegment(segmentCodeTo);
			search.setSegmentCodeFrom(segmentCodeFrom);
			search.setSegmentCodeTo(segmentCodeTo);

			resultSet = ModuleServiceLocator.getDataExtractionBD().getAvsSeat(search);

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("REPORT_ID", strReportId);
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
			if (isNotnull(gdsCode))
				parameters.put("GDS_CODE", gdsCode);
			if (isNotnull(bcCode))
				parameters.put("BC_CODE", bcCode);
			if (isNotnull(eventCode))
				parameters.put("EVENT_CODE", eventCode);
			if (isNotnull(flightNum))
				parameters.put("FLT_NUMBER", flightNum);
			if (isNotnull(depTimeLocal))
				parameters.put("DEP_TIME_LOCAL", depTimeLocal);
			if (isNotnull(segmentCodeFrom))
				parameters.put("SEG_CODE_FROM", segmentCodeFrom);
			if (isNotnull(segmentCodeTo))
				parameters.put("SEG_CODE_FROM", segmentCodeTo);

			if (value.trim().equals(WebConstants.REPORT_HTML)) {
				parameters.put("IMG_PATH", strPath);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport(WebConstants.REPORT_REF, parameters, resultSet,
						null, null, response);
			} else if (value.trim().equals(WebConstants.REPORT_PDF)) {
				strPath = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG_PATH", strPath);
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport(WebConstants.REPORT_REF, parameters, resultSet,
						response);
			} else if (value.trim().equals(WebConstants.REPORT_EXCEL)) {
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport(WebConstants.REPORT_REF, parameters, resultSet,
						response);
			} else if (value.trim().equals("CSV")) {
				response.reset();
				// response.addHeader("Content-Disposition",
				// "attachment;filename=FlightDetailsReport.csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);

			}
		} catch (Exception e) {
			log.error(e);
		}
	}

	private static boolean isNotnull(String str) {
		boolean blnCond = false;
		if (str == null || str.trim().equals("Select")) {
			blnCond = false;
		} else {
			blnCond = true;
		}
		return blnCond;
	}

}
