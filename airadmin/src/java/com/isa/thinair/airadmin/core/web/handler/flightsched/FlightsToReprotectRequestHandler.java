package com.isa.thinair.airadmin.core.web.handler.flightsched;

import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airinventory.api.dto.*;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSegment;
import com.isa.thinair.airproxy.api.utils.SegmentUtil;
import com.isa.thinair.airreservation.api.dto.OnWardConnectionDTO;
import com.isa.thinair.airreservation.api.dto.ReservationBasicsDTO;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONException;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.flightsched.FlightsToReprotectHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableConnectedFlight;
import com.isa.thinair.airinventory.api.model.FCCSegInventory;
import com.isa.thinair.airinventory.api.model.LogicalCabinClass;
import com.isa.thinair.airreservation.api.utils.AllocateEnum;
import com.isa.thinair.airreservation.api.utils.ResponseCodes;
import com.isa.thinair.airschedules.api.dto.FlightAlertDTO;
import com.isa.thinair.airschedules.api.dto.PastFlightInfoDTO;
import com.isa.thinair.airschedules.api.utils.FlightUtil;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.reporting.api.criteria.FlightSearchCriteria;
import com.isa.thinair.reporting.api.model.FlightInventorySummaryDTO;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import org.springframework.util.CollectionUtils;

import java.text.ParseException;

/**
 * @author srikantha
 * 
 */

public final class FlightsToReprotectRequestHandler extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(FlightsToReprotectRequestHandler.class);

	private static AiradminConfig airadminConfig = new AiradminConfig();

	private static final String PARAM_MODE = "hdnMode";
	private static final String PARAM_LOGICAL_CABIN_CLASS_CODE = "hdnLogicalCabinClassCode";
	private static final String PARAM_TARGET_CABIN_CLASS_CODE = "hdnTargetCabinClassCode";
	private static final String PARAM_TARGET_FLIGHTIDS = "hdnTargetFlightId";
	private static final String PARAM_TARGET_SEGMENT_CODES = "hdnTargetSegmentCode";
	private static final String PARAM_TARGET_FLIGHT_SEGIDS = "hdnTargetFlightSegmentId";
	private static final String PARAM_SOURCE_FLIGHT_DISTRIBUTIONS = "hdnSourceFlightDistributions";
	private static final String PARAM_ALERT = "hdnAlert";
	private static final String PARAM_SMSALERT = "hdnSMSAlert";
	private static final String PARAM_EMAILALERT = "hdnEmailAlert";
	private static final String PARAM_SMSCNF = "hdnSmsCnf";
	private static final String PARAM_CMSONH = "hdnSmsOnH";
	private static final String PARAM_EMAIL_ORIGIN_AGENT = "hdnEmailOriginAgent";
	private static final String PARAM_EMAIL_OWNER_AGENT = "hdnEmailOwnerAgent";
	private static final String PARAM_SELECTED_PNR = "hdnSelectedPNR";
	private static final String PARAM_IS_ALLOWED_OPEN_ETICKET_STATUS = "hdnOpenEtStatus";
	
	private static final String PARAM_VISIBLE_IBE = "hdnIBETransferFlights";
	private static final String PARAM_REPROTEC_FLIGHT_SEG_IDS = "hdnReprotectSegmentIDs";

	private static FlightSearchCriteria reprotectFlightSearchCriteria = null;
	private static final String dateFormat = "dd/MM/yyyy";
	static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();
	static String allowReprotectPAXToTodaysDepartures = globalConfig.getBizParam(
			SystemParamKeys.ALLOW_REPROTECT_PAX_TO_TODAYS_DEPARTURES).equalsIgnoreCase("Y") ? "Y" : "N";

	/**
	 * Main Execute Method for Flight Reprotect Action
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {

		String strHdnMode = request.getParameter(PARAM_MODE);
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;
		try {
			if (WebConstants.ACTION_CONFIRM_REPROTECT.equals(strHdnMode)) {
				boolean isCnfEticketOpen = Boolean.valueOf(request.getParameter(PARAM_IS_ALLOWED_OPEN_ETICKET_STATUS));

				//Get Pax Fare Segment IDs
				Collection<Integer> getPaxFareSegETIds = null;
				if (isCnfEticketOpen) {
					getPaxFareSegETIds = FlightUtil
							.getPaxFareSegIdsInPastFlight(request.getParameter(WebConstants.REQ_HDN_FLIGHT_ID));
				}
				reprotectPassenger(request);

				//Update Pax and ET status of given Pax Fare Seg IDs
				if (isCnfEticketOpen && !CollectionUtils.isEmpty(getPaxFareSegETIds)) {
					PastFlightInfoDTO pastFlightInfoDTO = new PastFlightInfoDTO();
					pastFlightInfoDTO.setFlightNumber(request.getParameter(WebConstants.REQ_HDN_FLIGHT_NO));
					pastFlightInfoDTO.setDepartureDate(request.getParameter(WebConstants.REQ_HDN_DEPT_DATE));
					pastFlightInfoDTO.setFlightId(request.getParameter(WebConstants.REQ_HDN_FLIGHT_ID));
					pastFlightInfoDTO.setHdnMode(strHdnMode);
					pastFlightInfoDTO.setPaxFareSegETIds(getPaxFareSegETIds);
					FlightUtil.updatePaxETStatus(pastFlightInfoDTO);
				}
			} else if (WebConstants.ACTION_SEARCH.equals(strHdnMode)) {
				String flightDeptTime = request.getParameter(WebConstants.REQ_HDN_DEPT_DATE_TIME);
				request.setAttribute(WebConstants.SHOW_ETICKET_OPENING_SECTION, FlightUtil.showETOpeningSection(flightDeptTime));
			}
			setDisplayData(request);
		} catch (Exception exception) {
			log.error("Exception in FlightsToReprotectRequestHandler.execute", exception);
			if (exception instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
			}
		}
		return forward;
	}

	/**
	 * Sets the Display data of the Reprotect Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setDisplayData(HttpServletRequest request) throws ModuleException, ParseException {
		boolean isPostBack = false;
		String strPostBackJS = null;
		String strSearchMode = request.getParameter(PARAM_MODE);
		if ((strSearchMode != null)
				&& ((strSearchMode.equals(WebConstants.ACTION_SEARCH)) || (strSearchMode
						.equals(WebConstants.ACTION_CONFIRM_REPROTECT)))) {
			setFlightsToReprotectRowHtml(request);
			isPostBack = true;
		}
		setReprotectDataByCCCodeHtml(request);
		setAirportList(request);
		setClientErrors(request);
		setSystemDate(request);
		boolean showRollFwdBtn = setShowRollForwardBtn(request);

		strPostBackJS = "var isPostBack = " + isPostBack + ";";
		request.setAttribute("displayRollFwdBtn", showRollFwdBtn);
		request.setAttribute("PostBack", strPostBackJS);
		request.setAttribute("agentEmailEnabled", AppSysParamsUtil.isAllowEmailsToAgentsInFlightReprotect());
		request.setAttribute("isSelfReprotectionAllowed", AppSysParamsUtil.isSelfReprotectionAllowed());
		request.setAttribute("externalIntlFlightDetailCaptureEnabled", AppSysParamsUtil.isExternalInternationalFlightInfoCaptureEnabled());
		request.setAttribute("dateAutoFillEnabled", AppSysParamsUtil.isAutoDateFillInPaxReprotectFlightSearchScreenEnabled());
	}


	private static void setFlightsToReprotectRowHtml(HttpServletRequest request) {
		try {
			FlightsToReprotectHTMLGenerator htmlGen = new FlightsToReprotectHTMLGenerator();
			String strHtml = htmlGen.getFlightsToReprotectRowHtml(request);
			request.setAttribute(WebConstants.REQ_HTML_ROWS, strHtml);
		} catch (ModuleException moduleException) {
			log.error("FlightsToReprotectRequestHandler." + "setFlightsToReprotectRowHtml() method is failed :"
					+ moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Reprotect data Cabin Class Wise
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setReprotectDataByCCCodeHtml(HttpServletRequest request) {
		try {
			FlightsToReprotectHTMLGenerator htmlGen = new FlightsToReprotectHTMLGenerator();
			String strResultData = htmlGen.getReprotectDataByCCCode(request);
			request.setAttribute(WebConstants.REQ_HTML_REPROTECT_INVENTORY_DATA_BY_CCCODE, strResultData);
		} catch (ModuleException moduleException) {
			log.error("FlightsToReprotectRequestHandler." + "setReprotectDataByCCCodeHtml() method is failed :"
					+ moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Airport List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setAirportList(HttpServletRequest request) {
		try {
			String strHtml = SelectListGenerator.createAirportsList();
			request.setAttribute(WebConstants.REQ_HTML_AIRPORT_LIST, strHtml);
		} catch (ModuleException moduleException) {
			log.error("FlightsToReprotectRequestHandler.setAirportList() " + " method failed :"
					+ moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Server Date to the Requset
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setSystemDate(HttpServletRequest request) {
		try {
			String strHtml = JavascriptGenerator.getSystemDate();
			request.setAttribute(WebConstants.REQ_SYSTEM_DATE, strHtml);
		} catch (ModuleException moduleException) {
			log.error("FlightsToReprotectRequestHandler.setSystemDate() " + " method  failed :"
					+ moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Client Validations to the Requset
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = FlightsToReprotectHTMLGenerator.getClientErrors(request);
			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("FlightsToReprotectRequestHandler.setClientErrors() " + " method failed :"
					+ moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Reprotects the Passengers
	 *
	 * @param request the HttpServletRequest
	 * @throws Exception the Exception
	 */
	private static void reprotectPassenger(HttpServletRequest request) throws Exception {
		String strLogicalCCCode = request.getParameter(PARAM_LOGICAL_CABIN_CLASS_CODE);
		String strTargetCabinClassCode = request.getParameter(PARAM_TARGET_CABIN_CLASS_CODE);

		// preparing the target flight details
		String strTargetFlightIds = request.getParameter(PARAM_TARGET_FLIGHTIDS);
		String strTargetSegCodes = request.getParameter(PARAM_TARGET_SEGMENT_CODES);
		String strTargetFlightSegIds = request.getParameter(PARAM_TARGET_FLIGHT_SEGIDS);
		String selectedPNRStr = request.getParameter(PARAM_SELECTED_PNR);
		// 53688:13072626,13072627@53689:13072629

		// preparing the source flight details
		String hdnSourceFlightDistributions = request.getParameter(PARAM_SOURCE_FLIGHT_DISTRIBUTIONS);
		String[] arrSourceFlightDistributions = hdnSourceFlightDistributions.split(":");

		boolean blnEmailAlert = getBooleanValue(request.getParameter(PARAM_EMAILALERT));
		boolean blnSmsAlert = getBooleanValue(request.getParameter(PARAM_SMSALERT));
		boolean blnSmsCnf = getBooleanValue(request.getParameter(PARAM_SMSCNF));
		boolean blnSmsOnH = getBooleanValue(request.getParameter(PARAM_CMSONH));
		boolean blnEmailAlertOriginAgent = getBooleanValue(request.getParameter(PARAM_EMAIL_ORIGIN_AGENT));
		boolean blnEmailAlertOwnerAgent = getBooleanValue(request.getParameter(PARAM_EMAIL_OWNER_AGENT));

		try {
			ServiceResponce serviceResponce = null;

			List<ReProtectDTO> reProtectDTOs = new ArrayList<>(1);
			ReProtectDTO reProtectDTO = new ReProtectDTO();
			reProtectDTO.setArrSourceFlightDistributions(arrSourceFlightDistributions);
			reProtectDTO.setBlnEmailAlert(blnEmailAlert);
			reProtectDTO.setBlnEmailAlertOriginAgent(blnEmailAlertOriginAgent);
			reProtectDTO.setBlnEmailAlertOwnerAgent(blnEmailAlertOwnerAgent);
			reProtectDTO.setBlnSmsAlert(blnSmsAlert);
			reProtectDTO.setBlnSmsCnf(blnSmsCnf);
			reProtectDTO.setBlnSmsOnH(blnSmsOnH);
			reProtectDTO.setHdnSourceFlightDistributions(hdnSourceFlightDistributions);
			reProtectDTO.setSelectedPNRStr(selectedPNRStr);
			reProtectDTO.setStrLogicalCCCode(strLogicalCCCode);
			reProtectDTO.setStrTargetCabinClassCode(strTargetCabinClassCode);
			reProtectDTO.setStrTargetFlightIds(strTargetFlightIds);
			reProtectDTO.setStrTargetFlightSegIds(strTargetFlightSegIds);
			reProtectDTO.setStrTargetSegCodes(strTargetSegCodes);

			reProtectDTOs.add(0, reProtectDTO);

			for (int count = 0; count < reProtectDTO.getArrSourceFlightDistributions().length; count++) {

				String strSourceFlightDistribution = reProtectDTO.getArrSourceFlightDistributions()[count];
				String[] arrSourceFlightDistParameters = strSourceFlightDistribution.split(",");

				String strSourceFlightSegmentId = arrSourceFlightDistParameters[2];

				FlightSegement trgFlightSegement = ModuleServiceLocator.getFlightServiceBD()
						.getFlightSegment(Integer.parseInt(strTargetFlightSegIds));

				HashMap<String, GroupPaxConnectionDTO> paxMap = groupPassengerConnections(
						strSourceFlightSegmentId, strLogicalCCCode,selectedPNRStr);
				Set<String> keySet = paxMap.keySet();
				for (String key : keySet) {

					GroupPaxConnectionDTO connectionDTO = paxMap.get(key);

					FlightSegement flightSegement = ModuleServiceLocator.getFlightServiceBD()
								.getAllFlightSegmentForLocalDate(connectionDTO.getOnWardConnectionDTO().getFlight(),
										connectionDTO.getOnWardConnectionDTO().getDepartureDate());
					if (flightSegement != null) {
						List<AvailableFlightSegment> availableFlightSegmentsList = ModuleServiceLocator.getFlightServiceBD()
								.getAvailableFlightSegments(SegmentUtil.getFromAirport(flightSegement.getSegmentCode()),
										SegmentUtil.getToAirport(flightSegement.getSegmentCode()), strLogicalCCCode,
										trgFlightSegement.getEstTimeArrivalLocal(), connectionDTO.getPaxList().size());

						if (availableFlightSegmentsList.isEmpty()) {
							break;
						}

						AvailableFlightSegment availableFlightSegment = availableFlightSegmentsList.get(0);

						ReProtectDTO nReProtectDTO = new ReProtectDTO();

						//2139225,CMB/SHJ,2167023,3,true
						String str = flightSegement.getFlightId() + "," + flightSegement.getSegmentCode() + "," + flightSegement
								.getFltSegId() + "," + connectionDTO.getPaxList().size() + "," + false;
						nReProtectDTO.setArrSourceFlightDistributions(new String[] { str });
						nReProtectDTO.setHdnSourceFlightDistributions(str);

						Collection<ReservationPax> paxCollection = connectionDTO.getPaxList();
						Hashtable<String, String> pnlList = new Hashtable<>(1);

						for (ReservationPax reservationPax : paxCollection) {
							if (!pnlList.contains(reservationPax.getReservation().getPnr())) {
								if (nReProtectDTO.getSelectedPNRStr() == null) {
									nReProtectDTO.setSelectedPNRStr(reservationPax.getReservation().getPnr());
								} else {
									nReProtectDTO.setSelectedPNRStr(
											nReProtectDTO.getSelectedPNRStr() + "," + reservationPax.getReservation().getPnr());
								}
								pnlList.put(reservationPax.getReservation().getPnr(), reservationPax.getReservation().getPnr());
							}
						}

						nReProtectDTO.setStrTargetFlightIds(
								String.valueOf(availableFlightSegment.getFirstFlightSegment().getFlightId()));
						nReProtectDTO.setStrTargetFlightSegIds(
								String.valueOf(availableFlightSegment.getFirstFlightSegment().getSegmentId()));
						nReProtectDTO.setStrTargetSegCodes(availableFlightSegment.getFirstFlightSegment().getSegmentCode());

						nReProtectDTO.setStrTargetCabinClassCode(strTargetCabinClassCode);
						nReProtectDTO.setStrLogicalCCCode(strLogicalCCCode);
						nReProtectDTO.setBlnEmailAlert(blnEmailAlert);
						nReProtectDTO.setBlnEmailAlertOriginAgent(blnEmailAlertOriginAgent);
						nReProtectDTO.setBlnEmailAlertOwnerAgent(blnEmailAlertOwnerAgent);
						nReProtectDTO.setBlnSmsAlert(blnSmsAlert);
						nReProtectDTO.setBlnSmsCnf(blnSmsCnf);
						nReProtectDTO.setBlnSmsOnH(blnSmsOnH);
						nReProtectDTO.setPnrListAdded(true);

						reProtectDTOs.add(nReProtectDTO);
					}
				}
			}

			for (ReProtectDTO protectDTO : reProtectDTOs) {
				Collection<TransferSeatDTO> colTransferSeatDTOs = new ArrayList<TransferSeatDTO>();

				TransferSeatDTO transferSeatDTO = new TransferSeatDTO();
				// To avoid transferring standby booking in flight re-protect
				transferSeatDTO.setSkipStandbyBookings(true);
				transferSeatDTO.setLogicalCCCode(protectDTO.getStrLogicalCCCode());
				transferSeatDTO.setCabinClassCode(protectDTO.getStrLogicalCCCode());
				transferSeatDTO.setTargetCabinClassCode(protectDTO.getStrTargetCabinClassCode());
				if (!protectDTO.getStrLogicalCCCode().equals(protectDTO.getStrTargetCabinClassCode())) {
					transferSeatDTO.setCabinClassChanged(true);
				}

				String flightSegIds[] = protectDTO.getStrTargetFlightSegIds().split(":");
				String flightIds[] = protectDTO.getStrTargetFlightIds().split(":");
				String segCodes[] = protectDTO.getStrTargetSegCodes().split(":");
				if (flightSegIds.length > 1) {
					Collection<TransferSeatDTO> connectingTransferSeatDTOs = new ArrayList<TransferSeatDTO>();
					for (int index = 0; index < flightSegIds.length; index++) {
						TransferSeatDTO connectingtransferSeatDTO = new TransferSeatDTO();
						connectingtransferSeatDTO.setTargetFlightSegId(Integer.parseInt(flightSegIds[index]));
						connectingtransferSeatDTO.setTargetFlightId(Integer.parseInt(flightIds[index]));
						connectingtransferSeatDTO.setTargetSegCode(segCodes[index]);
						connectingTransferSeatDTOs.add(connectingtransferSeatDTO);
					}
					transferSeatDTO.setConnectingSeatDTOs(connectingTransferSeatDTOs);
				} else {
					transferSeatDTO.setTargetFlightId(Integer.parseInt(protectDTO.getStrTargetFlightIds()));
					transferSeatDTO.setTargetFlightSegId(Integer.parseInt(protectDTO.getStrTargetFlightSegIds()));
					transferSeatDTO.setTargetSegCode(protectDTO.getStrTargetSegCodes());
				}
				transferSeatDTO.setDirectToConnection(
						(transferSeatDTO.getConnectingSeatDTOs() != null && transferSeatDTO.getConnectingSeatDTOs().size() > 0) ?
								true :
								false);
				Collection<TransferSeatList> colSourceFlightTransferSeatList = new ArrayList<TransferSeatList>();
				boolean isTransferAll = false;
				for (int count = 0; count < protectDTO.getArrSourceFlightDistributions().length; count++) {
					String strSourceFlightDistribution = protectDTO.getArrSourceFlightDistributions()[count];
					String[] arrSourceFlightDistParameters = strSourceFlightDistribution.split(",");

					String strSourceFlightId = arrSourceFlightDistParameters[0];
					String strSourceSegmentCode = arrSourceFlightDistParameters[1];
					String strSourceFlightSegmentId = arrSourceFlightDistParameters[2];

					int transferSeatCount = Integer.parseInt(arrSourceFlightDistParameters[3]);

					TransferSeatList transferSeatList = new TransferSeatList();
					transferSeatList.setSourceFlightId(Integer.parseInt(strSourceFlightId));
					transferSeatList.setSourceSegmentCode(strSourceSegmentCode);
					transferSeatList.setSourceSegmentId(Integer.parseInt(strSourceFlightSegmentId));
					transferSeatList.setTransferSeatCount(transferSeatCount);

					isTransferAll = false;
					if ((arrSourceFlightDistParameters[4] != null) && (arrSourceFlightDistParameters[4].trim()
							.equalsIgnoreCase("false"))) {
						transferSeatList.setPnrList(getSelectedPNRList(strSourceFlightSegmentId, protectDTO.getSelectedPNRStr(),
								protectDTO.isPnrListAdded()));
					}
					colSourceFlightTransferSeatList.add(transferSeatList);
				}

				transferSeatDTO.setSourceFltSegSeatDists(colSourceFlightTransferSeatList);

				colTransferSeatDTOs.add(transferSeatDTO);

				boolean isAlert = false;
				boolean isIBEVisibleOnly = false;
				isAlert = Boolean.valueOf(request.getParameter(PARAM_ALERT)).booleanValue();
				isIBEVisibleOnly = Boolean.valueOf(request.getParameter(PARAM_VISIBLE_IBE)).booleanValue();

				FlightAlertDTO fltAltDTO = new FlightAlertDTO();
				// fltAltDTO.setAlertForCancelledFlight(blnAlert);
				// fltAltDTO.setEmailForCancelledFlight(blnEmail);

				if (AppSysParamsUtil.isSelfReprotectionAllowed()) {
					if (isAlert || isIBEVisibleOnly) {
						if (isAlert) {
							fltAltDTO.setVisibleIbeOnly("N");
						} else {
							fltAltDTO.setVisibleIbeOnly("Y");
						}
						fltAltDTO.setTransferSelectedFlights("Y");
						if (isIBEVisibleOnly) {
							String reprotectedFlightSegmenIds = request.getParameter(PARAM_REPROTEC_FLIGHT_SEG_IDS);
							Set<Integer> flightRPSegments = new HashSet<Integer>();
							JSONArray jsonArray;
							try {
								jsonArray = new JSONArray(reprotectedFlightSegmenIds);
								if (jsonArray != null) {
									int len = jsonArray.length();
									for (int i = 0; i < len; i++) {
										String TempStr = jsonArray.getString(i).replace("<br>", "").trim();
										flightRPSegments.add(Integer.parseInt(TempStr));
									}
								}
								fltAltDTO.setFligthSegIds(flightRPSegments);
							} catch (JSONException execption) {
								log.error(
										"FlightsToReprotectRequestHandler.reprotectPassenger() " + "method failed : " + execption
												.getMessage());
								saveMessage(request, execption.getMessage(), WebConstants.MSG_ERROR);
							}
						} else {
							fltAltDTO.setFligthSegIds(new HashSet<Integer>());
						}
						isAlert = true;
					} else {
						isAlert = false;
					}
				}

				fltAltDTO.setSendEmailsForFlightAlterations(protectDTO.isBlnEmailAlert());
				fltAltDTO.setSendSMSForFlightAlterations(protectDTO.isBlnSmsAlert());
				fltAltDTO.setSendSMSForConfirmedBookings(protectDTO.isBlnSmsCnf());
				fltAltDTO.setSendSMSForOnHoldBookings(protectDTO.isBlnSmsOnH());
				fltAltDTO.setSendEmailToOriginAgent(protectDTO.isBlnEmailAlertOriginAgent());
				fltAltDTO.setSendEmailToOwnerAgent(protectDTO.isBlnEmailAlertOwnerAgent());

				serviceResponce = ModuleServiceLocator.getReservationBD()
						.transferSeats(colTransferSeatDTOs, isAlert, AllocateEnum.OVER_ALL, 0, fltAltDTO);
			}
			if ((serviceResponce.isSuccess()) && ((serviceResponce.getResponseCode() != null) && (serviceResponce
					.getResponseCode().equals(ResponseCodes.TRANSFER_SEATS_SUCCESSFULL)))) {

				Integer seatsTransferred = (Integer) serviceResponce.getResponseParam(ResponseCodes.TRANSFER_SEAT_NUMBER);
				Object[] messageParams = { seatsTransferred };

				request.setAttribute(WebConstants.POPUPMESSAGE,
						airadminConfig.getMessage(WebConstants.KEY_SCHEDULE_SAVE_SUCCESS));

				saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_REPROTECT_SUCCESS, messageParams),
						WebConstants.MSG_SUCCESS);
			} else {

				saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_REPROTECT_NOT_SUCCESS), WebConstants.MSG_ERROR);
			}
		} catch (ModuleException moduleException) {
			log.error("FlightsToReprotectRequestHandler.reprotectPassenger() " + "method failed : " + moduleException
					.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * 
	 * @param str
	 * @return
	 */
	private static boolean getBooleanValue(String str) {
		return (str == null || !str.equals("true")) ? false : true;
	}

	private static ArrayList<String> getSelectedPNRList(String fltSegId, String selectedPNRStr, boolean isPnrOnly) {
		ArrayList<String> pnrList = new ArrayList<String>();
		if(!isPnrOnly) {
			// 53688:13072626#1,13072627#4@53689:13072629#5
			if (!StringUtil.isNullOrEmpty(selectedPNRStr)) {
				String selectedPNRArray[] = selectedPNRStr.split("@");

				for (int i = 0; i < selectedPNRArray.length; i++) {
					String fltSegStr = selectedPNRArray[i];
					if (!StringUtil.isNullOrEmpty(fltSegStr)) {
						String fltSegArray[] = fltSegStr.split(":");

						if (fltSegId.equals(fltSegArray[0])) {
							String pnrArray[] = fltSegArray[1].split(",");

							for (int j = 0; j < pnrArray.length; j++) {

								if (!StringUtil.isNullOrEmpty(pnrArray[j])) {
									if (!StringUtil.isNullOrEmpty(pnrArray[j].split("#")[0]))
										pnrList.add(pnrArray[j].split("#")[0]);

								}
							}
						}

					}

				}

			}
		} else{
			if(selectedPNRStr != null && !selectedPNRStr.isEmpty()) {
				pnrList.addAll(Arrays.asList(selectedPNRStr.split(",")));
			}
		}

		return pnrList;
	}
	
	private static boolean setShowRollForwardBtn(HttpServletRequest request) {
		Integer flightId = Integer.parseInt(request.getParameter("hdnFlightId"));
		boolean showButton = false;
		try {
			showButton = ModuleServiceLocator.getFlightServiceBD().showRollFwdButton(flightId);
		} catch (ModuleException e) {
			log.error("Error at setShowRollForwardBtn " + e.getCause());
		}
		return showButton;
	}

	/**
	 * Main Execute Method for Flight Reprotect Action V2
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @param page
	 *            the page of the grid
	 * @return String the Forward Action
	 */
	public static String executeRPFilght(HttpServletRequest request, int page) {

		String strHdnMode = request.getParameter(PARAM_MODE);
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;
		try {
			if ((strHdnMode != null) && (strHdnMode.equals(WebConstants.ACTION_CONFIRM_REPROTECT))) {
				reprotectPassenger(request);
			}
			getRPFlightData(request, page);
		} catch (Exception exception) {
			log.error("Exception in FlightsToReprotectRequestHandler.execute", exception);
			if (exception instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
			}
		}
		return forward;
	}

	/**
	 * Sets the Display data of the Reprotect Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @param page
	 *            the page of the grid
	 * @throws ModuleException
	 */
	public static void getRPFlightData(HttpServletRequest request, int page) throws ModuleException {
		boolean isPostBack = false;
		String strPostBackJS = null;
		String strSearchMode = request.getParameter(PARAM_MODE);
		String passengerSelfReprotectionEnable = AppSysParamsUtil.isSelfReprotectionAllowed() ? "Y" : "N";

		if ((strSearchMode != null)
				&& ((strSearchMode.equals(WebConstants.ACTION_SEARCH)) || (strSearchMode
						.equals(WebConstants.ACTION_CONFIRM_REPROTECT)))) {
			getFlightsToReprotect(request, page);
			isPostBack = true;
		}
		if(!isPostBack){
			setReprotectDataByCCCodeHtml(request);
			setAirportList(request);
			setClientErrors(request);
			setSystemDate(request);
		}
		
		strPostBackJS = "var isPostBack = " + isPostBack + ";";
		request.setAttribute("PostBack", strPostBackJS);
		request.setAttribute("agentEmailEnabled", AppSysParamsUtil.isAllowEmailsToAgentsInFlightReprotect());
		request.setAttribute("isSelfReprotectionAllowed", AppSysParamsUtil.isSelfReprotectionAllowed());
		request.setAttribute("externalIntlFlightDetailCaptureEnabled",
				AppSysParamsUtil.isExternalInternationalFlightInfoCaptureEnabled());
		request.setAttribute("dateAutoFillEnabled", AppSysParamsUtil.isAutoDateFillInPaxReprotectFlightSearchScreenEnabled());
		request.setAttribute("selfReprotectionEnable", passengerSelfReprotectionEnable);
	}

	/**
	 * Search Flights for Reprotect
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	public static String getFlightsToReprotect(HttpServletRequest request, int pageNO) throws ModuleException {

		AiradminUtils.debug(log, "inside FlightsToReprotectRequestHandler.getFlightsToReprotect()");

		String strFormFieldsVariablesJS = "";
		prepareCabinClasses(request);

		reprotectFlightSearchCriteria = new FlightSearchCriteria();

		// Preparing module criterian for DepartureDateFrom
		String strDepartureDateFrom = request.getParameter("txtDepartureDateFrom") == null ? "" : request
				.getParameter("txtDepartureDateFrom");
		// Preparing module criterian for DepartureDateTo
		String strDepartureDateTo = request.getParameter("txtDepartureDateTo") == null ? "" : request
				.getParameter("txtDepartureDateTo");
		// Preparing module criterian for FlightNumber
		String strFlightNumber = request.getParameter("txtFlightNumber") == null ? "" : request.getParameter("txtFlightNumber");
		// Preparing module criterian for Departure
		String strDeparture = request.getParameter("selDeparture");
		// Preparing module criterian for Arrival
		String strArrival = request.getParameter("selArrival");
		// Preparing module criterian for Via1
		String strVia1 = request.getParameter("selVia1");
		// Preparing module criterian for Via2
		String strVia2 = request.getParameter("selVia2");
		// Preparing module criterian for Via3
		String strVia3 = request.getParameter("selVia3");
		// Preparing module criterian for Via4
		String strVia4 = request.getParameter("selVia4");

		Collection<FlightInventorySummaryDTO> reprotectFlightsCollection;

		Page page = null;
		int totalRecords = 0;

		// if Search button is pressed
		// if ((strMode != null) && (strMode.equals(WebConstants.ACTION_SEARCH))) {
		// preparing and setting the departureDateFrom date
		reprotectFlightSearchCriteria.setDepartureDateFrom(AiradminUtils.getFormattedDate(strDepartureDateFrom));

		// preparing and setting the departureDatTo date
		reprotectFlightSearchCriteria.setDepartureDateTo(AiradminUtils.getFormattedDate(strDepartureDateTo));

		if (strArrival != null && !(strArrival.equals("-1"))) {
			reprotectFlightSearchCriteria.setDestination(strArrival);
		}

		if (strDeparture != null && !(strDeparture.equals("-1"))) {
			reprotectFlightSearchCriteria.setOrigin(strDeparture);
		}

		if (strFlightNumber != null && !strFlightNumber.equals("")) {
			reprotectFlightSearchCriteria.setFlightNumber(strFlightNumber.toUpperCase());
		} else {
			reprotectFlightSearchCriteria.setFlightNumber("%");
		}

		// setting the via points
		ArrayList<String> viaList = new ArrayList<String>();

		if (strVia1 != null && !(strVia1.equals("-1"))) {
			viaList.add(strVia1);
		}

		if (strVia2 != null && !(strVia2.equals("-1"))) {
			if (strVia1 == null || strVia1.equals("")) {
				viaList.add(null);
			}
			viaList.add(strVia2);
		}

		if (strVia3 != null && !(strVia3.equals("-1"))) {
			if (strVia1 == null || strVia1.equals("")) {
				viaList.add(null);
			}
			if (strVia2 == null || strVia2.equals("")) {
				viaList.add(null);
			}
			viaList.add(strVia3);
		}

		if (strVia4 != null && !(strVia4.equals("-1"))) {
			if (strVia1 == null || strVia1.equals("")) {
				viaList.add(null);
			}
			if (strVia2 == null || strVia2.equals("")) {
				viaList.add(null);
			}
			if (strVia3 == null || strVia3.equals("")) {
				viaList.add(null);
			}
			viaList.add(strVia4);
		}

		reprotectFlightSearchCriteria.setViaPoints(viaList);
		reprotectFlightSearchCriteria.addFlightStatus(FlightStatusEnum.ACTIVE.getCode());
		reprotectFlightSearchCriteria.addFlightStatus(FlightStatusEnum.CLOSED.getCode());
		reprotectFlightSearchCriteria.setTimeInLocal(false);
		reprotectFlightSearchCriteria.setSeatFactor(new int[] { 0, 1000 });
		if (pageNO != 0) {
			pageNO = (pageNO - 1) * 20;
		} else if (pageNO == 1) {
			pageNO = 0;
		}
		if (!strDepartureDateFrom.equals("") && !strDepartureDateTo.equals("")) {
			page = ModuleServiceLocator.getDataExtractionBD().searchFlightsForInventoryUpdation(reprotectFlightSearchCriteria,
					pageNO, 20);
		} else {
			page = null;
			request.setAttribute("pageData", null);
		}

		Collection<AvailableConnectedFlight> connectedFlights = new ArrayList<AvailableConnectedFlight>();
		// TODO getAircraftServiceBD().getAllCabinClasses() has removed.
		/*
		 * if (reprotectFlightSearchCriteria.getOrigin() != null && reprotectFlightSearchCriteria.getDestination() !=
		 * null) { List<RouteInfoTO> availableRoutes = ModuleServiceLocator.getFlightServiceBD().getAvailableRoutes(
		 * reprotectFlightSearchCriteria.getOrigin(), reprotectFlightSearchCriteria.getDestination(),
		 * AvailableFlightSearchDTO.ALL_SINGLE_CONNECTED_FLIGHTS); Date currentSysTimeInZulu =
		 * CalendarUtil.getCurrentSystemTimeInZulu(); Map<String, Date> allowableLatesDatesZuluForInterliningMap =
		 * getEffectiveFromDateZuluForInterlining( currentSysTimeInZulu,
		 * CommonsServices.getGlobalConfig().getActiveInterlinedCarriersMap());
		 * 
		 * Collection<CabinClass> colCabinClasses = ModuleServiceLocator.getAircraftServiceBD().getAllCabinClasses();
		 * for (RouteInfoTO routeInfoTO : availableRoutes) { for (CabinClass cc : colCabinClasses) {
		 * Collection<AvailableConnectedFlight> cf =
		 * ModuleServiceLocator.getFlightServiceBD().getAvailableConnectedFlightSegments(
		 * reprotectFlightSearchCriteria.getDepartureDateFrom(), reprotectFlightSearchCriteria.getDepartureDateTo(),
		 * reprotectFlightSearchCriteria.getOrigin(), reprotectFlightSearchCriteria.getDestination(),
		 * routeInfoTO.getTransitAirportCodesList(), 1, 0, cc.getCabinClassCode(), 1,
		 * allowableLatesDatesZuluForInterliningMap, null, null); if(cf!= null){ connectedFlights.addAll(cf); } } }
		 * 
		 * }
		 */
		if (page != null) {
			reprotectFlightsCollection = identifyFlightsToReprotect(page.getPageData(), request);
			if (reprotectFlightsCollection.size() > 0) {
				page.setPageData(reprotectFlightsCollection);
				request.setAttribute("pageData", page);
			} else {
				request.setAttribute("pageData", null);
			}
		}

		return WebConstants.SUCCESS;
	}

	private static void prepareCabinClasses(HttpServletRequest request) throws ModuleException {
		StringBuffer sbCC = new StringBuffer("var arrCabinClassData = new Array();");
		Map<String, String> cabinClasses = CommonsServices.getGlobalConfig().getActiveCabinClassesMap();
		int count = 0;
		for (String cabinClass : cabinClasses.keySet()) {
			sbCC.append("arrCabinClassData[" + count + "] = new Array();");
			sbCC.append("arrCabinClassData[" + count + "][1] = '" + cabinClass + "';");
			sbCC.append("arrCabinClassData[" + count + "][2] = '" + cabinClasses.get(cabinClass) + "';");
			count++;
		}
		request.setAttribute("reqCabinClassData", sbCC.toString());
	}

	private static Collection<FlightInventorySummaryDTO> identifyFlightsToReprotect(
			Collection<FlightInventorySummaryDTO> reprotectFlightsCollection, HttpServletRequest request) throws ModuleException {
		Collection<FlightInventorySummaryDTO> reprotectFlights = new ArrayList<FlightInventorySummaryDTO>();
		long currentTime = CalendarUtil.getCurrentSystemTimeInZulu().getTime();

		if (reprotectFlightsCollection == null) {
			return null;
		}

		long lngClosureBufferTime = Long.parseLong(globalConfig.getBizParam(SystemParamKeys.FLIGHT_CLOSURE_DEPARTURE_GAP));
		lngClosureBufferTime = (lngClosureBufferTime * 60 * 1000);

		Iterator<FlightInventorySummaryDTO> iter = reprotectFlightsCollection.iterator();
		int i = 0;
		while (iter.hasNext()) {

			FlightInventorySummaryDTO flightSummary = iter.next();

			long departureTimeGap = flightSummary.getDepartureDateTime().getTime() - lngClosureBufferTime;
			if ((allowReprotectPAXToTodaysDepartures.equalsIgnoreCase("Y")) && (departureTimeGap < currentTime)) {
				continue;
			} else {
				reprotectFlights.add(flightSummary);
				i++;
			}
		}
		if (allowReprotectPAXToTodaysDepartures.equalsIgnoreCase("Y")) {
			request.setAttribute(WebConstants.REQ_TOTAL_RECORDS, String.valueOf(i));
		}

		return reprotectFlights;
	}
	
	/**
	 * Gets the Reprotect Data Group by Logical Cabin Class
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String Array Containing the Reprotect Flight Data
	 */
	@SuppressWarnings("unused")
	public static Map<String, Object> getReprotectDataByCCCode_V2(HttpServletRequest request) throws ModuleException {

		String sendEmailsForFlightAlterations = globalConfig.getBizParam(SystemParamKeys.SEND_EMAILS_FOR_FLIGHT_ALTERATIONS)
				.equalsIgnoreCase("Y") ? "Y" : "N";
		String sendSMSForFlightAlterations = globalConfig.getBizParam(SystemParamKeys.SEND_SMS_FOR_FLIGHT_ALTERATIONS)
				.equalsIgnoreCase("Y") ? "Y" : "N";

		int flightId = Integer.parseInt(request.getParameter("hdnFlightId"));

		String logicalCCDes = "";
		Integer overlappingFlightId = null;
//		StringBuffer strBaseArray = new StringBuffer("var arrBaseArray = new Array();");
//		StringBuffer strMyArr = new StringBuffer("var arrReprotectFlightInventories  = new Array();");
		StringBuffer reprotectBuff = new StringBuffer();
		
		List strBaseArray = new ArrayList();
		List strMyArr = new ArrayList();
		List arrReprotectFlightInventories = new ArrayList();
		List<String> ReprotectFlightInventories = new ArrayList<String>();
		List ReprotectFlightInventoriesTemp = new ArrayList();
		Map<String, Object> dataMap = new HashMap<String, Object>();
		
		FCCSegInventoryDTO fCCSegInventoryDTO = null;
		Set<String> cabinClasses = CommonsServices.getGlobalConfig().getActiveCabinClassesMap().keySet();
		Map<String, LogicalCabinClass> logicalCCs = ModuleServiceLocator.getLogicalCabinClassServiceBD()
				.getAllLogicalCabinClasses();
		int baseCount = 0;
		int tmpBaseCount = 0;
		for (String cabinClass : cabinClasses) {
			Collection<FCCInventoryDTO> colFccInventories = new ArrayList<FCCInventoryDTO>();
			colFccInventories = ModuleServiceLocator.getFlightInventoryBD().getFCCInventory(flightId, cabinClass,
					overlappingFlightId, false);

			if (colFccInventories != null && colFccInventories.size() > 0) {
				Object[] listArr = colFccInventories.toArray();

				FCCInventoryDTO fCCInventoryDTO = null;
				FCCSegInventory fCCSegInventory = null;

				for (Object element : listArr) {

					fCCInventoryDTO = (FCCInventoryDTO) element;
					Collection<FCCSegInventoryDTO> colFccSegInventories = fCCInventoryDTO.getFccSegInventoryDTOs();

					Iterator<FCCSegInventoryDTO> iterFccSegInventories = colFccSegInventories.iterator();
					int count = 0;

					while (iterFccSegInventories.hasNext()) {
						fCCSegInventoryDTO = iterFccSegInventories.next();
						fCCSegInventory = fCCSegInventoryDTO.getFccSegInventory();
						int totalSeats = fCCSegInventory.getSeatsSold() + fCCSegInventory.getSoldInfantSeats()
								+ fCCSegInventory.getOnHoldSeats() + fCCSegInventory.getOnholdInfantSeats();
						if (totalSeats > 0) {
							ReprotectFlightInventories = new ArrayList<String>();
							ReprotectFlightInventoriesTemp = new ArrayList();
							arrReprotectFlightInventories = new ArrayList();
							// contains the inventory seat allocation details by segment code

							ReprotectFlightInventories.add(0, Integer.toString(count));
							ReprotectFlightInventories.add(1,fCCSegInventory.getSegmentCode());
							ReprotectFlightInventories.add(2,fCCSegInventory.getSeatsSold() + "/" + fCCSegInventory.getSoldInfantSeats());
							ReprotectFlightInventories.add(3,fCCSegInventory.getOnHoldSeats() + "/" + fCCSegInventory.getOnholdInfantSeats());
							ReprotectFlightInventories.add(4,"");
							ReprotectFlightInventories.add(5,"");

							ReprotectFlightInventories.add(6,Integer.toString(flightId));
							ReprotectFlightInventories.add(7,Integer.toString(fCCSegInventory.getFlightSegId()));

							Flight flight = ModuleServiceLocator.getFlightServiceBD().getFlight(flightId);
							SimpleDateFormat sdfmt = new SimpleDateFormat(dateFormat);
							
							arrReprotectFlightInventories.add(0, fCCSegInventory.getLogicalCCCode());

							logicalCCDes = logicalCCs.get(fCCSegInventory.getLogicalCCCode()).getDescription();
							arrReprotectFlightInventories.add(1,logicalCCDes);
							arrReprotectFlightInventories.add(2,flight.getFlightNumber());
							arrReprotectFlightInventories.add(3,flight.getDestinationAptCode());
							arrReprotectFlightInventories.add(4,flight.getOriginAptCode());

							// contains the inventory seat allocation details by segment code
							if (totalSeats > 0) {
								ReprotectFlightInventoriesTemp.add(ReprotectFlightInventories);
								arrReprotectFlightInventories.add(5,ReprotectFlightInventoriesTemp);
							} else {
								arrReprotectFlightInventories.add(5,ReprotectFlightInventoriesTemp);
							}
							arrReprotectFlightInventories.add(6,Integer.toString(flight.getFlightId()));
							String depDate = "";
							try {
								depDate = sdfmt.format(flight.getDepartureDate());
							} catch (Exception e) {
								log.error("Error in formatting date" + e.getMessage());
								depDate = "";
							}
							arrReprotectFlightInventories.add(7,depDate);
							if(flight.getScheduleId() == null){
								arrReprotectFlightInventories.add(8,"null");
							} else {
								arrReprotectFlightInventories.add(8,Integer.toString(flight.getScheduleId()));
							}
							strBaseArray.add(arrReprotectFlightInventories);
							tmpBaseCount++;
						}
					}
				}
			}

			baseCount++;
		}
		dataMap.put("baseArray", strBaseArray);
		dataMap.put("sendEmailForPax", sendEmailsForFlightAlterations);
		dataMap.put("sendSMSForPax", sendSMSForFlightAlterations);
		dataMap.put("allowReprotectPAX", allowReprotectPAXToTodaysDepartures);

		return dataMap;
	}

	/**
	 * get the pnr list and group them by next segment. Key is next segment departure airport value is pnr list
	 *
	 * @return
	 */
	private static HashMap<String, GroupPaxConnectionDTO> groupPassengerConnections(
			String fltSegId, String strLogicalCCCode,String selectedPNRStr) throws ModuleException {
		
		HashMap<String, GroupPaxConnectionDTO> pax = new HashMap<>(1);
		List<String> pnrLst = getSelectedPNRList(fltSegId, selectedPNRStr, false);

		FlightSegement flightSegement = ModuleServiceLocator.getFlightServiceBD().getFlightSegment(Integer.parseInt(fltSegId));
		List<ReservationBasicsDTO> reservationBasicsDTOs = ModuleServiceLocator.getReservationBD()
				.getFlightSegmentPAXDetails(Integer.parseInt(fltSegId), strLogicalCCCode);

		for (ReservationBasicsDTO reservationBasicsDTO : reservationBasicsDTOs) {			
			if (!pnrLst.isEmpty()) {
				boolean pnrExistInSelection = false;
				for (String pnr : pnrLst) {
					if (reservationBasicsDTO.getPnr().equals(pnr)) {
						pnrExistInSelection = true;
					}
				}
				if (pnrExistInSelection) {
					Collection<ReservationPax> passengerDTOs = ModuleServiceLocator.getPassengerBD()
							.getPassengers(reservationBasicsDTO.getPnr(), false);

					for (ReservationPax passengerDTO : passengerDTOs) {
						Collection<OnWardConnectionDTO> connectionDTOs = ModuleServiceLocator.getReservationAuxilliaryBD()
								.getAllOutBoundSequences(reservationBasicsDTO.getPnr(), passengerDTO.getPnrPaxId(),
										flightSegement.getSegmentCode().split("/")[0], flightSegement.getFlightId());
						for (OnWardConnectionDTO connectionDTO : connectionDTOs) {
							if (pax.get(connectionDTO.getSegmentCode()) == null) {
								GroupPaxConnectionDTO dto = new GroupPaxConnectionDTO();
								Collection<ReservationPax> paxCollection = new ArrayList<>(1);
								paxCollection.add(passengerDTO);
								dto.setPaxList(paxCollection);
								dto.setOnWardConnectionDTO(connectionDTO);
								pax.put(connectionDTO.getSegmentCode(), dto);
							} else {
								pax.get(connectionDTO.getSegmentCode()).getPaxList().add(passengerDTO);
							}

						}
					}
				}
			}else {
				Collection<ReservationPax> passengerDTOs = ModuleServiceLocator.getPassengerBD()
						.getPassengers(reservationBasicsDTO.getPnr(), false);

				for (ReservationPax passengerDTO : passengerDTOs) {
					Collection<OnWardConnectionDTO> connectionDTOs = ModuleServiceLocator.getReservationAuxilliaryBD()
							.getAllOutBoundSequences(reservationBasicsDTO.getPnr(), passengerDTO.getPnrPaxId(),
									flightSegement.getSegmentCode().split("/")[0], flightSegement.getFlightId());
					for (OnWardConnectionDTO connectionDTO : connectionDTOs) {
						if (pax.get(connectionDTO.getSegmentCode()) == null) {
							GroupPaxConnectionDTO dto = new GroupPaxConnectionDTO();
							Collection<ReservationPax> paxCollection = new ArrayList<>(1);
							paxCollection.add(passengerDTO);
							dto.setPaxList(paxCollection);
							dto.setOnWardConnectionDTO(connectionDTO);
							pax.put(connectionDTO.getSegmentCode(), dto);
						} else {
							pax.get(connectionDTO.getSegmentCode()).getPaxList().add(passengerDTO);
						}

					}
				}
			}
		}
		return pax;
	}	

	private static class GroupPaxConnectionDTO {
		private OnWardConnectionDTO onWardConnectionDTO;
		private Collection<ReservationPax> paxList;

		public OnWardConnectionDTO getOnWardConnectionDTO() {
			return onWardConnectionDTO;
		}

		public void setOnWardConnectionDTO(OnWardConnectionDTO onWardConnectionDTO) {
			this.onWardConnectionDTO = onWardConnectionDTO;
		}

		public Collection<ReservationPax> getPaxList() {
			return paxList;
		}

		public void setPaxList(Collection<ReservationPax> paxList) {
			this.paxList = paxList;
		}
	}
	
}
