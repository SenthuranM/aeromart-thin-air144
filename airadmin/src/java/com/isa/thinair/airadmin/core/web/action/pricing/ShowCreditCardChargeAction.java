package com.isa.thinair.airadmin.core.web.action.pricing;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.handler.inventory.CreditCardWiseChargeRH;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airpricing.api.dto.CCChargeTO;
import com.isa.thinair.airpricing.api.model.PaymentGatewayWiseCharges;
import com.isa.thinair.commons.api.dto.ModuleCriteria;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Results({ @Result(name = S2Constants.Jsp.Charges.CCCHARGE_ADMIN_JSP, value = S2Constants.Jsp.Charges.CCCHARGE_ADMIN_JSP),
		@Result(name = AdminStrutsConstants.AdminAction.ERROR, value = AdminStrutsConstants.AdminJSP.ERROR_JSP),
		@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class ShowCreditCardChargeAction extends BaseRequestAwareAction {

	public static final String PAYMENT_GATEWAYS = "reqPaymentGateways";
	public static final String DATE_PATTERN = "dd/MM/yyyy";
	public static final String DEFAULT_VALUE = "2";

	private static Log log = LogFactory.getLog(ShowCreditCardChargeAction.class);
	private static AiradminConfig airadminConfig = new AiradminConfig();

	private Object[] rows;
	private int page;
	private int total;
	private int records;

	private String succesMsg;
	private String msgType;
	private static String clientErrors;

	private String effectiveDate;
	private String selPGWSearch;
	private String selStatus;

	private int strPGW;
	private String txtEffectiveDateFrom;
	private String txtEffectiveDateTo;
	private String valuePercentage;
	private float amount;
	private String status;
	private int strCCWCId;
	private String hdnMode;

	public String execute() throws Exception {

		String strPGW = CreditCardWiseChargeRH.getPaymentGateways();
		if (!strPGW.equals("")) {
			request.setAttribute(PAYMENT_GATEWAYS, strPGW);
		}
		getErrorMsgs();

		if (clientErrors != null && !clientErrors.equals("")) {
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, getClientErrors());
		}

		return S2Constants.Jsp.Charges.CCCHARGE_ADMIN_JSP;
	}

	public String searchCCWiseCharge() throws Exception {

		try {

			List<ModuleCriterion> critrian = new ArrayList<ModuleCriterion>();

			if (!(effectiveDate.trim().equals("") && effectiveDate.trim().equals(""))) {
				ModuleCriteria criteriaADSegment = new ModuleCriteria();
				List<java.sql.Date> lsteffectiveDate = new ArrayList<java.sql.Date>();
				criteriaADSegment.setFieldName("effectiveDate");
				criteriaADSegment.setCondition(ModuleCriteria.CONDITION_LIKE);
				SimpleDateFormat format = new SimpleDateFormat(DATE_PATTERN);
				java.util.Date parsed = format.parse(effectiveDate);
				java.sql.Date sqlDate = new java.sql.Date(parsed.getTime());
				lsteffectiveDate.add(sqlDate);
				criteriaADSegment.setValue(lsteffectiveDate);
				critrian.add(criteriaADSegment);
			}
			if (selPGWSearch != null && !selPGWSearch.trim().equals("") && !selPGWSearch.trim().equals("***")) {
				ModuleCriteria criteriaADTitle = new ModuleCriteria();
				List<String> lstPGW = new ArrayList<String>();
				criteriaADTitle.setFieldName("pgwId");
				criteriaADTitle.setCondition(ModuleCriteria.CONDITION_EQUALS);
				lstPGW.add(selPGWSearch);
				criteriaADTitle.setValue(lstPGW);
				critrian.add(criteriaADTitle);
			}
			if (selStatus != null && !selStatus.trim().equals("") && !selStatus.trim().equals("***")) {
				ModuleCriteria criteriaADLanguage = new ModuleCriteria();
				List<String> lststatus = new ArrayList<String>();
				criteriaADLanguage.setFieldName("ccwcStatus");
				criteriaADLanguage.setCondition(ModuleCriteria.CONDITION_EQUALS);
				lststatus.add(selStatus);
				criteriaADLanguage.setValue(lststatus);
				critrian.add(criteriaADLanguage);
			}

			Page<CCChargeTO> pgCCWiseCharge = CreditCardWiseChargeRH.searchCCWiseCharge(this.page, critrian);
			this.records = pgCCWiseCharge.getTotalNoOfRecords();
			this.total = pgCCWiseCharge.getTotalNoOfRecords() / 20;
			int mod = pgCCWiseCharge.getTotalNoOfRecords() % 20;
			if (mod > 0) {
				this.total = this.total + 1;
			}
			if (this.page <= 0) {
				this.page = 1;
			}

			Collection<CCChargeTO> colCCWiseCharge = pgCCWiseCharge.getPageData();

			if (colCCWiseCharge != null) {
		
				this.rows = extractData(colCCWiseCharge);
				
			}
			
		} catch (Exception e) {
			this.succesMsg = "System Error Please Contact Administrator";
			this.msgType = "Error";
			log.error("Error in search payment gateway wise charge", e);
		}
		return S2Constants.Result.SUCCESS;

	}

	public String SaveOrUpdateCCWiseCharge() throws Exception {

		PaymentGatewayWiseCharges paymentGatewayWiseCharges = null;
		SimpleDateFormat format = new SimpleDateFormat(DATE_PATTERN);
		Date effectiveFromDate = format.parse(txtEffectiveDateFrom);
		Date effectiveToDate = format.parse(txtEffectiveDateTo);

		if (hdnMode.equals(WebConstants.ACTION_EDIT)) {
			paymentGatewayWiseCharges = CreditCardWiseChargeRH.getPaymentGatewayWiseCharges(strCCWCId);
			if (paymentGatewayWiseCharges == null) {
				this.succesMsg = "System Error Please Contact Administrator";
				this.msgType = WebConstants.MSG_ERROR;
				return S2Constants.Result.SUCCESS;
			}
		} else {
			paymentGatewayWiseCharges = new PaymentGatewayWiseCharges();
		}
		paymentGatewayWiseCharges.setChargeEffectiveFromDate(effectiveFromDate);
		paymentGatewayWiseCharges.setChargeEffectiveToDate(effectiveToDate);
		paymentGatewayWiseCharges.setValuePercentageFlag(valuePercentage);
		paymentGatewayWiseCharges.setPaymentGatewayId(strPGW);
		paymentGatewayWiseCharges.setStatus(status);
		if (valuePercentage.equals("V")) {
			paymentGatewayWiseCharges.setValueInDefaultCurrency(amount);
		} else {
			paymentGatewayWiseCharges.setValueInDefaultCurrency(Integer.parseInt(DEFAULT_VALUE));
			paymentGatewayWiseCharges.setChargeValuePercentage(amount);
		}

		try {
			CreditCardWiseChargeRH.savePaymentGatewayWiseCharges(paymentGatewayWiseCharges);
			this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS);
			this.msgType = WebConstants.MSG_SUCCESS;

		} catch (Exception ex) {
			this.succesMsg = "System Error Please Contact Administrator";
			this.msgType = WebConstants.MSG_ERROR;
			if (log.isErrorEnabled()) {
				log.error(ex);
			}
		}

		return S2Constants.Result.SUCCESS;
	}

	/**
	 * Creates Client Validations for Credit Card Wise Charge
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array of Client Validations
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getErrorMsgs() throws ModuleException {

		if (clientErrors == null) {
			Properties moduleErrs = new Properties();
			moduleErrs.setProperty("um.ccwc.form.pgw", "pgwReqrd");
			moduleErrs.setProperty("um.ccwc.form.fromDate", "fromDateReqrd");
			moduleErrs.setProperty("um.ccwc.form.toDate", "toDateReqrd");
			moduleErrs.setProperty("um.ccwc.form.amount", "amountReqrd");
			moduleErrs.setProperty("um.ccwc.form.dateValidate", "dateValidate");
			moduleErrs.setProperty("um.ccwc.form.numberValidate", "numberValidate");

			clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}
		return clientErrors;
	}

	private Object[] extractData(Collection<CCChargeTO> colCCWiseCharge){
		Object[] dataRow = new Object[colCCWiseCharge.size()];
				int index = 1;
				Iterator<CCChargeTO> iter = colCCWiseCharge.iterator();
				while (iter.hasNext()) {
					Map<String, Object> counmap = new HashMap<String, Object>();
					CCChargeTO grdCCChargeTO = iter.next();

					counmap.put("id", new Integer(((page - 1) * 20) + index).toString());
					counmap.put("CCWCId", grdCCChargeTO.getCreditCardWiseChargeId());
					counmap.put("PGWId", grdCCChargeTO.getPgwId());
					counmap.put("PGW", grdCCChargeTO.getPgwName());
					counmap.put("description", grdCCChargeTO.getPgwDescription());

					SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_PATTERN);

					counmap.put("effectiveFrom", dateFormat.format(grdCCChargeTO.getEffectiveFromDate()));
					counmap.put("effectiveTo", dateFormat.format(grdCCChargeTO.getEffectiveToDate()));
					counmap.put("valuePercentage", grdCCChargeTO.getValuePercentageFlag());
					if (grdCCChargeTO.getValuePercentageFlag().equals("P")) {
						counmap.put("amount", grdCCChargeTO.getChargeValuePercentage());
					} else {
						counmap.put("amount", grdCCChargeTO.getValueInDefaultCurrency());
					}
					counmap.put("status", grdCCChargeTO.getStatus());

					dataRow[index - 1] = counmap;
					index++;
				}
			return dataRow;

	}
	
	public Object[] getRows() {
		return rows;
	}

	public void setRows(Object[] rows) {
		this.rows = rows;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public String getSuccesMsg() {
		return succesMsg;
	}

	public void setSuccesMsg(String succesMsg) {
		this.succesMsg = succesMsg;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public String getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public String getSelPGWSearch() {
		return selPGWSearch;
	}

	public void setSelPGWSearch(String selPGWSearch) {
		this.selPGWSearch = selPGWSearch;
	}

	public String getSelStatus() {
		return selStatus;
	}

	public void setSelStatus(String selStatus) {
		this.selStatus = selStatus;
	}

	public String getTxtEffectiveDateFrom() {
		return txtEffectiveDateFrom;
	}

	public void setTxtEffectiveDateFrom(String txtEffectiveDateFrom) {
		this.txtEffectiveDateFrom = txtEffectiveDateFrom;
	}

	public String getTxtEffectiveDateTo() {
		return txtEffectiveDateTo;
	}

	public void setTxtEffectiveDateTo(String txtEffectiveDateTo) {
		this.txtEffectiveDateTo = txtEffectiveDateTo;
	}

	public String getValuePercentage() {
		return valuePercentage;
	}

	public void setValuePercentage(String valuePercentage) {
		this.valuePercentage = valuePercentage;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getStrPGW() {
		return strPGW;
	}

	public void setStrPGW(int strPGW) {
		this.strPGW = strPGW;
	}

	public int getStrCCWCId() {
		return strCCWCId;
	}

	public void setStrCCWCId(int strCCWCId) {
		this.strCCWCId = strCCWCId;
	}

	public String getHdnMode() {
		return hdnMode;
	}

	public void setHdnMode(String hdnMode) {
		this.hdnMode = hdnMode;
	}

	public String getClientErrors() {
		return clientErrors;
	}

	public void setClientErrors(String clientErrors) {
		this.clientErrors = clientErrors;
	}

}
