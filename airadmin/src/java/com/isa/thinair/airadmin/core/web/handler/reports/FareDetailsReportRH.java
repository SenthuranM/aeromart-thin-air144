//TODO insert license header
package com.isa.thinair.airadmin.core.web.handler.reports;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * Fare Details Action Request Handler.
 * 
 * @author M.Rikaz
 */
public class FareDetailsReportRH extends BasicRequestHandler {

	/** Class Logger */
	private static Log log = LogFactory.getLog(FareDetailsReportRH.class);

	/** Global Configurations */
	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	public static String execute(HttpServletRequest request, HttpServletResponse response) {

		String strHdnMode = request.getParameter("hdnMode");
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;

		try {
			setDisplayData(request);
			log.debug("FareDetailsReportRH setDisplayData() SUCCESS");
		} catch (Exception ex) {
			log.error("FareDetailsReportRH setDisplayData() FAILED " + ex.getMessage());
		}

		try {

			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				log.debug("FareDetailsReportRH setReportView Success");
				return null;
			} else {
				log.error("FareDetailsReportRH setReportView not selected");
			}

		} catch (ModuleException ex) {
			saveMessage(request, ex.getMessageString(), WebConstants.MSG_ERROR);
			log.error("FareDetailsReportRH setReportView Failed " + ex.getMessageString());
		}

		return forward;

	}

	public static void setDisplayData(HttpServletRequest request) throws Exception {

		setClientErrors(request);
		setAirportList(request);
		setGSAMultiSelectList(request);
		setAgentTypes(request);
		setFlightNoList(request);
		setBookingCodesList(request); // Booking Classes
		setBookingClassType(request); // Booking Class Type
		setBCCategoryList(request);
		setAllocationTypeList(request); // Allocation Type
		setFareClassList(request); // Fare Class
		setClassOfService(request); // class of service

		ReportsHTMLGenerator.createPreferedReportOptions(request);
	}

	/**
	 * TODO : Fill method body. Sets the report data.
	 * 
	 * @param request
	 * @param response
	 * @throws ModuleException
	 */
	protected static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		log.debug("Inside setReportView....");

		String strReportId = "UC_REPM_081";
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);

		String reportTemplate = "FareDetailsReport.jasper";
		String flightWiseReportTemplate = "FlightWiseFareDetailsReport.jasper";

		String value = request.getParameter("radReportOption");
		String radReportType = request.getParameter("radReportType");

		String strLogo = AppSysParamsUtil.getReportLogo(false);
		String strPath = "../../images/" + AppSysParamsUtil.getReportLogo(true);
		String strBase = globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY);
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		boolean logicalCabinClassEnable = AppSysParamsUtil.isLogicalCabinClassEnabled();
		String logicalCCEnable = "" + logicalCabinClassEnable + "";

		ArrayList<String> lstSegments = new ArrayList<String>();
		Collection<String> flightNoColl = new ArrayList<String>();
		Collection<String> bCCatColl = new ArrayList<String>();
		Collection<String> bCTypeColl = new ArrayList<String>();
		Collection<String> allocTypeColl = new ArrayList<String>();
		Collection<String> bcClsColl = new ArrayList<String>();
		Collection<String> fareRulesColl = new ArrayList<String>();
		Collection<String> agentsColl = new ArrayList<String>();

		String salEffecFrmDate = request.getParameter("txtFromDate");
		String salEffecToDate = request.getParameter("txtToDate");
		String selStatus = request.getParameter("selStatus");
		String depValidFrmDate = request.getParameter("txtBookedFromDate");
		String depValidToDate = request.getParameter("txtBookedToDate");
		String selSegmentOnD = request.getParameter("hdnSegments");
		String[] selFlightNo = request.getParameterValues("selFlightNo");
		String[] selBCCat = request.getParameterValues("selBCCat");
		String[] selBCTyp = request.getParameterValues("selBCTyp");
		String[] selAllocTyp = request.getParameterValues("selAllocTyp");
		String[] selBCls = request.getParameterValues("selBCls");
		String[] selFareRuls = request.getParameterValues("selFareRuls");
		String selFlightWayOption = request.getParameter("selFlightWayOption");
		String selSortBy = request.getParameter("selSortBy");
		String selSortByOrder = request.getParameter("selSortByOrder");
		String cos = request.getParameter("selCOS");

		String hdnAgents = request.getParameter("hdnAgents");

		try {

			ReportsSearchCriteria search = new ReportsSearchCriteria();

			if (radReportType != null && radReportType.equalsIgnoreCase("FARE_DET")) {
				ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs(WebConstants.REPORT_REF,
						ReportsHTMLGenerator.getReportTemplate(reportTemplate));
				log.debug("Report Template 1");
				search.setReportType("FARE_DET");
			} else {
				ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs(WebConstants.REPORT_REF,
						ReportsHTMLGenerator.getReportTemplate(flightWiseReportTemplate));
				log.debug("Report Template 2");
				search.setReportType("FLIGHT_WISE");
			}

			if (salEffecFrmDate != null && !salEffecFrmDate.equals("")) {
				search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(salEffecFrmDate) + " 00:00:00");
			}

			if (salEffecToDate != null && !salEffecToDate.equals("")) {
				search.setDateRangeTo(ReportsHTMLGenerator.convertDate(salEffecToDate) + " 23:59:59");
			}

			if ((depValidFrmDate != null && !depValidFrmDate.isEmpty()) && (depValidToDate != null && !depValidToDate.isEmpty())) {
				search.setDateRange2From(ReportsHTMLGenerator.convertDate(depValidFrmDate) + " 00:00:00");
				search.setDateRange2To(ReportsHTMLGenerator.convertDate(depValidToDate) + " 23:59:59");
			}

			search.setStatus(selStatus);

			if (isNotEmptyOrNull(selSegmentOnD)) {
				String[] arrSegCodes = selSegmentOnD.split(",");
				for (String segCode : arrSegCodes) {
					lstSegments.add(segCode);
				}
				search.setSegmentCodes(lstSegments);
			}

			if (selFlightNo != null && selFlightNo.length > 0) {
				for (int i = 0; i < selFlightNo.length; i++) {
					flightNoColl.add(selFlightNo[i]);
				}
				search.setFlightNoCollection(flightNoColl);
			}

			if (selBCCat != null && selBCCat.length > 0) {
				for (int i = 0; i < selBCCat.length; i++) {
					bCCatColl.add(selBCCat[i]);
				}
				search.setBcCategory(bCCatColl);
			}

			if (selBCTyp != null && selBCTyp.length > 0) {
				for (int i = 0; i < selBCTyp.length; i++) {
					bCTypeColl.add(selBCTyp[i]);
				}
				search.setBcTypeCollection(bCTypeColl);
			}

			if (selAllocTyp != null && selAllocTyp.length > 0) {
				for (int i = 0; i < selAllocTyp.length; i++) {
					allocTypeColl.add(selAllocTyp[i]);
				}
				search.setAllocTypeCollection(allocTypeColl);
			}

			if (selBCls != null && selBCls.length > 0) {
				for (int i = 0; i < selBCls.length; i++) {
					bcClsColl.add(selBCls[i]);
				}
				search.setBookingCodes(bcClsColl);
			}

			if (selFareRuls != null && selFareRuls.length > 0) {
				for (int i = 0; i < selFareRuls.length; i++) {
					fareRulesColl.add(selFareRuls[i]);
				}
				search.setSsrCodes(fareRulesColl);
			}

			if (hdnAgents != null && isNotEmptyOrNull(hdnAgents)) {
				String[] strAgents = request.getParameter("hdnAgents").split(",");
				for (int i = 0; i < strAgents.length; i++) {
					agentsColl.add(strAgents[i]);
				}
				search.setAgents(agentsColl);
			}

			if (cos != null && !cos.equals("")) {
				search.setCos(cos);
			}

			search.setOperationType(selFlightWayOption);

			search.setSortByColumnName(selSortBy);
			search.setSortByOrder(selSortByOrder);

			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}

			ResultSet resultSet = ModuleServiceLocator.getDataExtractionBD().getFareDetailsData(search);

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("REPORT_ID", strReportId);
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);

			// To provide Report Format Options
			String reportNumFormat = request.getParameter("radRptNumFormat");
			ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

			parameters.put("LOGICAL_CABIN_CLASS_ENABLE", logicalCCEnable);
			parameters.put("COS", "COS1");
			parameters.put("BC_TYPE", "BC1");
			parameters.put("ALLOCATION_TYPE", "ALLOC1");
			parameters.put("STATUS", "ACT");
			parameters.put("AMT_1", "(" + strBase + ")");

			if (value.trim().equals(WebConstants.REPORT_HTML)) {
				parameters.put("IMG_PATH", strPath);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport(WebConstants.REPORT_REF, parameters, resultSet,
						null, null, response);
			} else if (value.trim().equals(WebConstants.REPORT_PDF)) {
				strPath = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG_PATH", strPath);
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport(WebConstants.REPORT_REF, parameters, resultSet,
						response);
			} else if (value.trim().equals(WebConstants.REPORT_EXCEL)) {
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport(WebConstants.REPORT_REF, parameters, resultSet,
						response);
			} else if (value.trim().equals("CSV")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=FareDetails.csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);
			}

		} catch (Exception e) {
			log.error("setReportView Exception " + e.toString());
		}

	}

	private static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	private static void setAirportList(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createAirportCodeList();
		request.setAttribute(WebConstants.REQ_HTML_AIRPORT_LIST, strHtml);
	}

	protected static void setFlightStatus(HttpServletRequest request) throws ModuleException {
		String strStation = SelectListGenerator.createFlightStatus();
		request.setAttribute(WebConstants.REQ_FLIGHT_STATUS, strStation);
	}

	protected static void setAgentTypes(HttpServletRequest request) throws ModuleException {
		String strATList = SelectListGenerator.createAgentTypeList_SG();
		request.setAttribute(WebConstants.REQ_HTML_AGENTTYPE_LIST, strATList);
	}

	protected static void setGSAMultiSelectList(HttpServletRequest request) throws ModuleException {
		boolean blnWithTAs = false;
		boolean blnWithCOs = false;
		String strAgentType = "";

		if (request.getParameter("selAgencies") != null) {
			strAgentType = request.getParameter("selAgencies");
		}
		if (request.getParameter("chkTAs") != null) {
			blnWithTAs = (request.getParameter("chkTAs").equals("on") ? true : false);
		}
		if (request.getParameter("chkCOs") != null) {
			blnWithCOs = (request.getParameter("chkCOs").equals("on") ? true : false);
		}
		String strList = ReportsHTMLGenerator.createAgentGSAMultiSelect(strAgentType, blnWithTAs, blnWithCOs);
		request.setAttribute(WebConstants.REQ_HTML_GSA_LIST, strList);
	}

	protected static void setFlightNoList(HttpServletRequest request) throws ModuleException {
		String strStation = SelectListGenerator.createFlightNoList();
		request.setAttribute(WebConstants.REQ_FLIGHT_NO_LIST, strStation);
	}

	protected static void setBCCategorySelectList(HttpServletRequest request) throws ModuleException {

		String strHtml = "";
		strHtml += "<option value='Standard'>Standard</option>";
		strHtml += "<option value='Non-Standard'>Non-Standard</option>";
		strHtml += "<option value='Fixed'>Fixed</option>";

		request.setAttribute(WebConstants.REQ_FLIGHT_NO_LIST, strHtml);
	}

	private static void setBookingCodesList(HttpServletRequest request) {
		try {
			// String strHtml = SelectListGenerator.createBookingCodeList();
			String strHtml = SelectListGenerator.getBookingClassWithCatTypeAlloType();
			String strBookingDescCode = SelectListGenerator.getBookingClassWithCatTypeAlloTypeNoTags();

			request.setAttribute(WebConstants.REQ_HTML_BOOKINGCODES_DESC, strBookingDescCode);

			request.setAttribute(WebConstants.REQ_HTML_BOOKINGCODES_LIST, strHtml);

		} catch (ModuleException moduleException) {
			log.error(
					"Exception in BookingCodesRequestHandler.setBookingCodesList() [origin module="
							+ moduleException.getModuleDesc() + "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	private static void setBookingClassType(HttpServletRequest request) throws ModuleException {

		// String strHtml = SelectListGenerator.createBookingClassTypeList();
		/**
		 * above function returns only 2 types once thats fixed we can comment below lines and continue with the above
		 * function [Normal & StandBY only]
		 * */
		String strHtml = "";
		strHtml += "<option value='NORMAL'>Normal</option>";
		strHtml += "<option value='STANDBY'>StandBy</option>";
		strHtml += "<option value='OPENRT'>OpenReturn</option>";

		request.setAttribute(WebConstants.REQ_HTML_BOOKING_CLASS_TYPES_LIST, strHtml);
	}

	private static void setAllocationTypeList(HttpServletRequest request) {
		String strHtml = "";
		strHtml += "<option value='" + BookingClass.AllocationType.COMBINED + "'>Combine</option>";
		strHtml += "<option value='" + BookingClass.AllocationType.CONNECTION + "'>Connection</option>";
		strHtml += "<option value='" + BookingClass.AllocationType.RETURN + "'>Return</option>";
		strHtml += "<option value='" + BookingClass.AllocationType.SEGMENT + "'>Segment</option>";
		request.setAttribute(WebConstants.REQ_HTML_ALLOCATION_TYPES_LIST, strHtml);
	}

	// createFareCodeRuleList
	private static void setFareClassList(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createFareCodeRuleList();
		request.setAttribute(WebConstants.REQ_HTML_FARECLASS_SELECT_LIST, strHtml);
	}

	private static void setBCCategoryList(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createBCCategoryList();
		request.setAttribute(WebConstants.REQ_HTML_BOOKINGCODES_TYPES_LIST, strHtml);
	}

	private static boolean isNotEmptyOrNull(String str) {
		return !((str == null) || str.trim().equals("") || str.trim().equals("-1"));
	}

	private static void setClassOfService(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createCabinClassList();
		request.setAttribute(WebConstants.REQ_HTML_COS_LIST, strHtml);

	}
}
