package com.isa.thinair.airadmin.core.web.generator.master;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airmaster.api.model.Airport;
import com.isa.thinair.airmaster.api.model.AirportDST;
import com.isa.thinair.airmaster.api.model.AirportForDisplay;
import com.isa.thinair.airmaster.api.model.SubStation;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author srikantha
 * 
 */
public class AirportHTMLGenerator {

	private static Log log = LogFactory.getLog(AirportHTMLGenerator.class);

	private static String clientErrors;

	private static final String PARAM_SEARCHDATA = "hdnSearchData";

	// Time format when returns minutes
	private static String formatTime(int timeInMinutes) {
		int numerator = timeInMinutes;
		int denominator = 60;

		int quotient = numerator / denominator;
		int remainder = numerator % denominator;

		String strRemainder;
		if (remainder < 10)
			strRemainder = "0" + remainder;
		else
			strRemainder = "" + remainder;

		return quotient + ":" + strRemainder;
	}

	// Change given minutes into DAYS: HOURS : MIUNTES Format
	private static String formatDayHourTime(int timeInMinutes) {
		int numerator = timeInMinutes;
		int daysDenominator = 1440;
		int hourDenominator = 60;

		int days = numerator / daysDenominator;
		int minutesRemaining = numerator % daysDenominator;
		int hours = minutesRemaining / hourDenominator;
		int mins = minutesRemaining % hourDenominator;

		String strDays;
		String strHours;
		String strMins;
		if (days < 10)
			strDays = "0" + days;
		else
			strDays = "" + days;

		if (hours < 10)
			strHours = "0" + hours;
		else
			strHours = "" + hours;

		if (mins < 10)
			strMins = "0" + mins;
		else
			strMins = "" + mins;

		return strDays + ":" + strHours + ":" + strMins;
	}

	private String changeDateFormat(Date date) {
		SimpleDateFormat dtfmt = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		return dtfmt.format(date);
	}

	private String displayDay(String timeString) {
		return timeString.substring(0, timeString.indexOf(':'));
	}

	private static String displayHourMin(String timeString) {
		return timeString.substring(timeString.indexOf(':') + 1, timeString.length());
	}

	/**
	 * Gets the Airport Data Array According to Search
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array od Airport Data
	 * @throws ModuleException
	 *             the ModuleException
	 */
	@SuppressWarnings("unchecked")
	public final String getAirportRowHtml(HttpServletRequest request) throws ModuleException {
		log.debug("inside AirportHTMLGenerator.getAirportRowHtml()");

		String strSearchData = request.getParameter(PARAM_SEARCHDATA);

		request.setAttribute("reqAirportSearchData", strSearchData);

		String strCountryCode = null;
		List<Airport> list = null;
		int recordNo = 0;
		if (request.getParameter("hdnRecNo") != null && !request.getParameter("hdnRecNo").equals(""))
			recordNo = Integer.parseInt(request.getParameter("hdnRecNo")) - 1;
		Page page = null;

		int totalRecords = 0;
		String strJavascriptTotalNoOfRecs = "";
		String strCountryCodeJS = "var strCountryCode=-1;";
		strCountryCode = request.getParameter("selCountry");
		strCountryCodeJS = "var strCountryCode='" + strCountryCode + "';";

		String searchDataArr[] = null;
		if (strSearchData != null && !strSearchData.equals("")) {
			searchDataArr = strSearchData.split(",");
			for (int i = 0; i < searchDataArr.length; i++) {
				if (searchDataArr[i].trim().equalsIgnoreCase("*")) {
					searchDataArr[i] = null;
				}
			}
		}

		// If All option selected return all the countries
		if (strSearchData == null || strSearchData.equals("")
				|| ((strSearchData != "") && (searchDataArr[0] == null && searchDataArr[1] == null && searchDataArr[2] == null)))
			page = ModuleServiceLocator.getAirportServiceBD().getAirports(recordNo, 20, null, null, null);
		else
			page = ModuleServiceLocator.getAirportServiceBD().getAirports(recordNo, 20, searchDataArr[0], searchDataArr[1],
					searchDataArr[2]);

		totalRecords = page.getTotalNoOfRecords();
		strJavascriptTotalNoOfRecs = "var totalRecords = " + totalRecords + ";";
		request.setAttribute(WebConstants.REQ_TOTAL_RECORDS, strJavascriptTotalNoOfRecs);
		list = (List<Airport>) page.getPageData();

		request.setAttribute(WebConstants.REQ_COUNTRY_CODE, strCountryCodeJS);
		HashMap<String, String> usedAirportCodes = null;
		if (list != null && list.size() > 0) {
			Collection<String> airportIDs = new HashSet<String>();
			for (Iterator<Airport> iterAirportList = list.iterator(); iterAirportList.hasNext();) {
				Airport airport = (Airport) iterAirportList.next();
				airportIDs.add(airport.getAirportCode());
			}
			usedAirportCodes = ModuleServiceLocator.getFlightServiceBD().getUsedAirports(airportIDs);
		}
		return createAirportRowHTML(list, usedAirportCodes);
	}

	/**
	 * Creates the Airport Grid Rows from Collection of Airports
	 * 
	 * @param airports
	 *            the collection of Airports
	 * @param usedAirportCodes
	 *            the Used Airport code Map
	 * @return String the Created Grid Array
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private String createAirportRowHTML(Collection<Airport> airports, HashMap<String, String> usedAirportCodes) throws ModuleException {

		List<Airport> list = (List<Airport>) airports;
		Object[] listArr = list.toArray();
		Airport airport = null;
		StringBuffer sb = new StringBuffer("var arrData = new Array();");

		// Haider 09Mar09-------------->
		// Collect airport name for each airport code
		List<AirportForDisplay> l = ModuleServiceLocator.getTranslationBD().getAirports();
		AirportForDisplay afd = null;
		ArrayList<AirportForDisplay> airportFD;// array to hold airport name in all lang for specific airport code
		int size = l.size();
		Map<String, ArrayList<AirportForDisplay>> airportLangMap = new HashMap<String, ArrayList<AirportForDisplay>>();// <airportCode,
																														// list
																														// of
																														// airport
																														// name
																														// in
																														// all
																														// lang>
		String airportCode = "";
		for (int k = 0; k < size; k++) {
			afd = (AirportForDisplay) l.get(k);
			airportCode = afd.getAirportCode();
			airportFD = airportLangMap.get(airportCode);
			if (airportFD == null) {
				airportFD = new ArrayList<AirportForDisplay>();
				airportLangMap.put(airportCode, airportFD);
			}
			airportFD.add(afd);
		}
		// <--------------------------------
		Map<String, Agent> agentMAP = new HashMap<String, Agent>();
		Collection<String> colGoShowAgentCodes = new ArrayList<String>();

		for (Iterator<Airport> iter = airports.iterator(); iter.hasNext();) {
			Airport obj = (Airport) iter.next();
			colGoShowAgentCodes.add(obj.getGoshowAgentCode());
		}

		if (colGoShowAgentCodes != null && !colGoShowAgentCodes.isEmpty()) {
			agentMAP = ModuleServiceLocator.getTravelAgentBD().getAgentsMap(colGoShowAgentCodes);
		}

		for (int i = 0; i < listArr.length; i++) {
			airport = (Airport) listArr[i];
			String strAirportDstCheck = "";

			sb.append("arrData[" + i + "] = new Array();");
			sb.append("arrData[" + i + "][1] = '" + airport.getAirportCode() + "';");
			sb.append("arrData[" + i + "][2] = '" + airport.getAirportName() + "';");
			sb.append("arrData[" + i + "][3] = '" + "';");

			if (airport.getOnlineStatus() == 1) {
				sb.append("arrData[" + i + "][4] = 'Online';");
			} else {
				sb.append("arrData[" + i + "][4] = 'Offline';");
			}
			sb.append("arrData[" + i + "][5] = '" + AiradminUtils.getModifiedStatus(airport.getStatus()) + "';");

			if (airport.getModifiedDate() != null && !airport.getModifiedDate().equals("null")) {
				sb.append("arrData[" + i + "][6] = '" + airport.getModifiedDate() + "';");
			} else {
				sb.append("arrData[" + i + "][6] = '';");
			}
			sb.append("arrData[" + i + "][7] = '" + airport.getVersion() + "';");

			if (airport.getContact() != null && !airport.getContact().equals("null")) {
				sb.append("arrData[" + i + "][8] = '" + airport.getContact() + "';");
			} else {
				sb.append("arrData[" + i + "][8] = '';");
			}

			if (airport.getFax() != null && !airport.getFax().equals("null")) {
				sb.append("arrData[" + i + "][9] = '" + airport.getFax() + "';");
			} else {
				sb.append("arrData[" + i + "][9] = '';");
			}

			if (airport.getGmtOffsetAction() != null && !airport.getGmtOffsetAction().equals("null")) {
				sb.append("arrData[" + i + "][10] = '" + airport.getGmtOffsetAction() + "';");
			} else {
				sb.append("arrData[" + i + "][10] = '';");
			}
			sb.append("arrData[" + i + "][11] = '" + formatTime(airport.getGmtOffsetHours()) + "';");

			if (airport.getLatitude() != null && !airport.getLatitude().equals("null")) {
				sb.append("arrData[" + i + "][12] = '" + airport.getLatitude() + "';");
			} else {
				sb.append("arrData[" + i + "][12] = '';");
			}

			if (airport.getLatitudeNorthSouth() != null && !airport.getLatitudeNorthSouth().equals("null")) {
				sb.append("arrData[" + i + "][13] = '" + airport.getLatitudeNorthSouth() + "';");
			} else {
				sb.append("arrData[" + i + "][13] = '';");
			}

			if (airport.getLongitude() != null && !airport.getLongitude().equals("null")) {
				sb.append("arrData[" + i + "][14] = '" + airport.getLongitude() + "';");
			} else {
				sb.append("arrData[" + i + "][14] = '';");
			}

			if (airport.getLongitudeEastWest() != null && !airport.getLongitudeEastWest().equals("null")) {
				sb.append("arrData[" + i + "][15] = '" + airport.getLongitudeEastWest() + "';");
			} else {
				sb.append("arrData[" + i + "][15] = '';");
			}

			if (airport.getTelephone() != null && !airport.getTelephone().equals("null")) {
				sb.append("arrData[" + i + "][16] = '" + airport.getTelephone() + "';");
			} else {
				sb.append("arrData[" + i + "][16] = '';");
			}

			if (airport.getRemarks() != null && !airport.getRemarks().equals("null")) {
				sb.append("arrData[" + i + "][17] = '" + airport.getRemarks() + "';");
			} else {
				sb.append("arrData[" + i + "][17] = '';");
			}

			if (formatTime(airport.getMinStopoverTime()) != null) {
				sb.append("arrData[" + i + "][18] = '" + formatTime(airport.getMinStopoverTime()) + "';");
			} else {
				sb.append("arrData[" + i + "][18] = '';");
			}

			sb.append("arrData[" + i + "][19] = '" + airport.getMinConnectionTime() + "';");

		//	String strCountryCode = null;// airport.getCountryCode();
		//	if (strCountryCode != null) {
		//		sb.append("arrData[" + i + "][20] = '" + strCountryCode + "';");
		//	} else {
				sb.append("arrData[" + i + "][20] = '';");
		//	}
			sb.append("arrData[" + i + "][22] = '" + airport.getClosestAirport() + "';");
			sb.append("arrData[" + i + "][23] = '" + airport.getStationCode() + "';");

			AirportDST airportDST = ModuleServiceLocator.getAirportServiceBD().getCurrentAirportDST(airport.getAirportCode());
			if (airportDST != null) {
				strAirportDstCheck = "Yes";

				sb.append("arrData[" + i + "][24] = '" + changeDateFormat(airportDST.getDstStartDateTime()).toString() + "';");
				sb.append("arrData[" + i + "][25] = '" + changeDateFormat(airportDST.getDstEndDateTime()).toString() + "';");
			} else {
				if (ModuleServiceLocator.getAirportServiceBD().hasPreviousDST(airport.getAirportCode())) {
					strAirportDstCheck = "Yes";
				} else {
					strAirportDstCheck = "No";
				}
				sb.append("arrData[" + i + "][24] = '';");
				sb.append("arrData[" + i + "][25] = '';");
			}
			sb.append("arrData[" + i + "][21] = '" + strAirportDstCheck + "';");

			// To display both GMT Offset Action and value

			if (airport.getGmtOffsetAction() != null && !airport.getGmtOffsetAction().equals("null")) {
				sb.append("arrData[" + i + "][26] = '" + airport.getGmtOffsetAction() + " "
						+ formatTime(airport.getGmtOffsetHours()) + "';");
			} else {
				sb.append("arrData[" + i + "][26] = '';");
			}

			// Get whether airport is used or not
			if (usedAirportCodes != null && usedAirportCodes.get(airport.getAirportCode()) != null) {
				sb.append("arrData[" + i + "][27] = true;");
			} else {
				sb.append("arrData[" + i + "][27] = false;");
			}
			sb.append("arrData[" + i + "][28] = " + (airport.getIBEVisibility() == 0 ? "'N'" : "'Y'") + ";");
			sb.append("arrData[" + i + "][29] = " + (airport.getXBEVisibility() == 0 ? "'N'" : "'Y'") + ";");

			if (airport.getGoshowAgentCode() != null && !airport.getGoshowAgentCode().equals("null")) {
				sb.append("arrData[" + i + "][30] = '" + airport.getGoshowAgentCode() + "';");
				Agent goShowAgent = (Agent) agentMAP.get(airport.getGoshowAgentCode());
				sb.append("arrData[" + i + "][31] = '" + goShowAgent.getAgentDisplayName() + "';");

			} else {
				sb.append("arrData[" + i + "][30] = '';");
				sb.append("arrData[" + i + "][31] = '';");
			}
			// Haider 09Mar09
			// adding airport list for all language for the airport
			sb.append("arrData[" + i + "][32] = new Array();");
			airportFD = airportLangMap.get(airport.getAirportCode());
			String ucs = "";
			if (airportFD != null)
				size = airportFD.size();
			else
				size = 0;
			for (int a = 0; a < size; a++) {
				afd = (AirportForDisplay) airportFD.get(a);
				String airportCodeOl = afd.getAirportCodeOl();
				if (airportCodeOl == null || airportCodeOl.isEmpty() || airportCodeOl.equals("N/A")) {
					ucs = "N/A";
				} else {
					ucs = StringUtil.getUnicode(afd.getAirportCodeOl());
				}
				sb.append("arrData[" + i + "][32][" + a + "]=new Array('" + afd.getLanguageCode() + "','" + ucs + "','"
						+ afd.getId() + "','" + afd.getVersion() + "');");
			}

			if (airport.getManualCheckin() != null && !airport.getManualCheckin().equals(null)) {
				sb.append("arrData[" + i + "][33] = '" + airport.getManualCheckin() + "';");
			}

			sb.append("arrData[" + i + "][34] = '" + airport.getLccVisibility() + "';");
			// Split the results for the view
			String carrierCodes = "";
			if (airport.getCarrierCode().length() > 5) {
				for (int j = 0; j < airport.getCarrierCode().length(); j++) {
					int lastIndex = j + 5;
					if (lastIndex < airport.getCarrierCode().length()) {
						carrierCodes += airport.getCarrierCode().substring(j, lastIndex) + "<br>";
					} else {
						carrierCodes += airport.getCarrierCode().substring(j, airport.getCarrierCode().length());
					}
					j += 5;
				}
			} else if (airport.getCarrierCode().length() < 3) {
				carrierCodes = airport.getCarrierCode() + "  ";
			} else {
				carrierCodes = airport.getCarrierCode();
			}
			sb.append("arrData[" + i + "][35] = '" + carrierCodes + "';");
			sb.append("arrData[" + i + "][36] = '" + airport.getLccPublishStatus() + "';");
			sb.append("arrData[" + i + "][37] = '" + displayDay(formatDayHourTime(airport.getNotificationStartCutovertime()))
					+ "';");
			sb.append("arrData[" + i + "][38] = '" + displayHourMin(formatDayHourTime(airport.getNotificationStartCutovertime()))
					+ "';");
			sb.append("arrData[" + i + "][39] = '" + displayDay(formatDayHourTime(airport.getNotificationEndCutovertime()))
					+ "';");
			sb.append("arrData[" + i + "][40] = '" + displayHourMin(formatDayHourTime(airport.getNotificationEndCutovertime()))
					+ "';");
			sb.append("arrData[" + i + "][41] = '" + airport.getOnlineCheckin() + "';");

			sb.append("arrData[" + i + "][42] = '" + airport.getIsSurfaceStation() + "';");

			String connectingAirport = "";
			if (airport.getConnectingAirport() != null && !airport.getConnectingAirport().equals(null)) {
				connectingAirport = airport.getConnectingAirport();
			}
			sb.append("arrData[" + i + "][43] = '" + connectingAirport + "';");

			// Adding sub station data into the array
			Set<SubStation> subStations = airport.getSubStations();
			sb.append("arrData[" + i + "][44]=new Array();");
			Iterator<SubStation> itr = subStations.iterator();

			int setCount = 0;
			while (itr.hasNext()) {
				SubStation s = (SubStation) itr.next();
				sb.append("arrData[" + i + "][44][" + (setCount++) + "]=new Array('" + s.getShortName() + "','"
						+ s.getDescription() + "','" + s.getStatus() + "');");
			}

			if (airport.getNotificationEmail() != null) {
				sb.append("arrData[" + i + "][45] = '" + airport.getNotificationEmail().trim() + "';");
			} else {
				sb.append("arrData[" + i + "][45] = '';");
			}

			List<String[]> mealNTiming = SelectListGenerator.getMealNotifyTiming(airport.getAirportCode().trim());

			if (mealNTiming.size() > 0) {
				sb.append("arrData[" + i + "][46] = '" + mealNTiming.get(0)[4].trim() + "';");
				sb.append("arrData[" + i + "][47] = '" + mealNTiming.get(0)[1].trim() + "';");
				sb.append("arrData[" + i + "][48] = '" + mealNTiming.get(0)[2].trim() + "';");
				sb.append("arrData[" + i + "][49] = '" + mealNTiming.get(0)[3].trim() + "';");
			} else {
				sb.append("arrData[" + i + "][46] = '';");
				sb.append("arrData[" + i + "][47] = '';");
				sb.append("arrData[" + i + "][48] = '';");
				sb.append("arrData[" + i + "][49] = '';");
			}

			sb.append("arrData[" + i + "][50] = '" + airport.getEtlProcessEnabled() + "';");
			sb.append("arrData[" + i + "][51] = '" + airport.getAutoFlownProcessEnabled() + "';");
			sb.append("arrData[" + i + "][52] = '" + airport.getEnableCFGElementForPNL() + "';");
			sb.append("arrData[" + i + "][53] = " + (airport.getCityId() != null ? airport.getCityId() : "-1") + ";");
		}
		return sb.toString();
	}

	/**
	 * Create Client Validations for Airport Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array of Client validations
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {

		if (clientErrors == null) {
			Properties moduleErrs = new Properties();

			moduleErrs.setProperty("um.airadmin.delete.confirmation", "deleteRecoredCfrm");

			moduleErrs.setProperty("um.airport.form.id.required", "airportIdRqrd");
			moduleErrs.setProperty("um.airport.form.airportcode.min.length", "airportIdMinLength");
			moduleErrs.setProperty("um.airport.form.name.required", "airportNameRqrd");
			moduleErrs.setProperty("um.airport.form.contact.required", "airportContactRqrd");
			moduleErrs.setProperty("um.airport.form.phone.required", "airportPhoneRqrd");
			moduleErrs.setProperty("um.airport.form.fax.required", "airportFaxRqrd");
			moduleErrs.setProperty("um.airport.form.minstopovrtime.required", "airportMinstopovrtimeRqrd");
			moduleErrs.setProperty("um.airport.form.minstopovrtime.invalid", "airportMinstopovrtimeInvalid");
			moduleErrs.setProperty("um.airport.form.minconntime.required", "airportMincontimeRqrd");
			moduleErrs.setProperty("um.airport.form.latitude1.required", "airportLatitute1Rqrd");
			moduleErrs.setProperty("um.airport.form.longitude1.required", "airportLongitude1Rqrd");
			moduleErrs.setProperty("um.airport.form.minstopovrtime.businessrule.violated", "airportMinstopovrtimeBnsRleVltd");
			moduleErrs.setProperty("um.airport.form.minstopovrtime.format.violated", "airportMinstopovrtimeFmtVltd");
			moduleErrs.setProperty("um.airport.form.gmtoffsetction.required", "airportGmtoffsetctionRqrd");
			moduleErrs.setProperty("um.airport.form.gmtoffsetction.businessrule.violated", "airportGmtoffsetctionBnsRleVltd");
			moduleErrs.setProperty("um.airport.form.gmtoffset.invalid", "airportGmtOffSetInvalid");
			moduleErrs.setProperty("um.airport.form.territory.required", "territoryRqrd");
			moduleErrs.setProperty("um.airport.form.station.required", "stationRqrd");
			moduleErrs.setProperty("um.airport.new.airport.added", "newAirportAdded");
			moduleErrs.setProperty("um.airport.form.goShowAgent.cannot.exist", "goShowAgentCannotExist");
			moduleErrs.setProperty("um.airport.select.carrier.required", "carrierSelectionRequired");
			moduleErrs.setProperty("um.airport.select.publish.status.required", "lccPublishStatusSelectionRequired");
			moduleErrs.setProperty("um.airport.form.cutoverstarttime.invalid", "nofiCutoverStartTimeInvalid");
			moduleErrs.setProperty("um.airport.form.cutoverendtime.invalid", "nofiCutoverEndTimeInvalid");
			moduleErrs.setProperty("um.airport.form.cutoverStartEndtime.invalid", "nofiCutoverStartEndTimeInvalid");
			moduleErrs.setProperty("um.airport.form.connectingAirport.required", "connectingAirportRqrd");
			moduleErrs.setProperty("um.airport.form.connectingAirport.invalid", "connectingAirportInvalid");

			moduleErrs.setProperty("um.airport.form.notificationEmail.required", "notificationEmailRequired");
			moduleErrs.setProperty("um.airport.form.notificationStartTime.required", "notificationStartTimeRequired");
			moduleErrs.setProperty("um.airport.form.notificationFrequency.required", "notificationFrequencyRequired");
			moduleErrs.setProperty("um.airport.form.lastNotificationTime.required", "lastNotificationTimeRequired");
			moduleErrs.setProperty("um.airport.form.notificationFrequency.invalid", "notificationFrequencyInvalid");
			moduleErrs.setProperty("um.airport.form.notificationStartTime.invalid", "notificationStartTimeInvalid");
			moduleErrs.setProperty("um.airport.form.lastNotificationTime.invalid", "lastNotificationTimeInvalid");
			moduleErrs.setProperty("um.airport.form.lastNotificationTime.wrong", "lastNotificationTimeWrong");
			moduleErrs.setProperty("um.airport.form.notificationEmail.invalid", "notificationEmailInvalid");
			clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}

		return clientErrors;
	}
}
