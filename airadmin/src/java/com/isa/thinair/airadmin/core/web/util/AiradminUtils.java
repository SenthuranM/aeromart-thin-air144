/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airadmin.core.web.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airinventory.api.util.AirInventoryModuleUtils;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.auditor.api.service.AuditorBD;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Srikantha
 * 
 */
public class AiradminUtils {
	private static Log log = LogFactory.getLog(AiradminUtils.class);

	private static final String ACTIVE = "ACT";
	private static final String INACTIVE = "INA";

	public static final String YES = "Y";
	public static final String NO = "N";

	private static AiradminConfig airadminConfig = new AiradminConfig();

	public static String getModifiedStatus(String strStatus) {
		String strStatusValue = "";

		try {
			if (strStatus != null) {
				if (strStatus.equals(ACTIVE)) {
					strStatusValue = airadminConfig.getMessage("um.airadmin.status.active");
				}
				if (strStatus.equals(INACTIVE)) {
					strStatusValue = airadminConfig.getMessage("um.airadmin.status.inactive");
				}
			}
		} catch (Exception e) {
			log.error("Exception in AiradminModuleUtils.getModifiedStatus()", e);
		}
		return strStatusValue;
	}

	public static String getEnableDisableStatus(String strStatus) {
		String strStatusValue = "";

		try {
			if (strStatus != null) {
				if (YES.equals(strStatus)) {
					strStatusValue = airadminConfig.getMessage("um.airadmin.status.enable");
				}
				if (NO.equals(strStatus)) {
					strStatusValue = airadminConfig.getMessage("um.airadmin.status.disable");
				}
			}
		} catch (Exception e) {
			log.error("Exception in AiradminModuleUtils.getModifiedStatus()", e);
		}
		return strStatusValue;
	}

	public static void debug(Log logger, String strMessage) {
		if (logger.isDebugEnabled()) {
			logger.debug(strMessage);
		}
	}

	public static void info(Log logger, String strMessage) {
		if (logger.isInfoEnabled()) {
			logger.info(strMessage);
		}
	}

	public static void warn(Log logger, String strMessage) {
		if (logger.isWarnEnabled()) {
			logger.warn(strMessage);
		}
	}

	public static void error(Log logger, String strMessage) {
		if (logger.isErrorEnabled()) {
			logger.error(strMessage);
		}
	}

	public static void error(Log logger, String strMessage, Exception exception) {
		if (logger.isErrorEnabled()) {
			logger.error(strMessage, exception);
		}
	}

	public static Date getFormattedDate(String strDate) {
		SimpleDateFormat dtfmt = new SimpleDateFormat("dd/MM/yyyy");
		Date date = null;
		if (strDate != null) {
			try {
				date = dtfmt.parse(strDate);

			} catch (ParseException ex) {
				log.error(ex.getMessage());
			}
		}
		return date;
	}

	/**
	 * Returns not null string
	 * 
	 * @param str
	 *            The String to check
	 * @return String the Not Null String
	 */
	public static String getNotNullString(String str) {
		return (str == null) ? "" : str.trim();
	}

	public static Collection<String> getUserCarrierCodes(HttpServletRequest request) throws ModuleException {
		User user = (User) request.getSession().getAttribute(WebConstants.SES_CURR_USER);
		if (user.getCarriers() == null || user.getCarriers().size() < 1) {
			throw new ModuleException("module.user.carrier");
		}
		return user.getCarriers();
	}

	/**
	 * Returns AuditorBD
	 */
	public static AuditorBD getAuditorBD() {
		return AirInventoryModuleUtils.getAuditorBD();
	}

	public static int getAdvBkinTimeInMinutes(String days, String hours, String minutes) {
		int noOfDaysInMins = 0;
		int noOfHoursInMins = 0;
		int noOfMins = 0;
		if (days != null && !days.trim().equals("")) {
			noOfDaysInMins = Integer.valueOf(days) * 24 * 60;
		}
		if (hours != null && !hours.trim().equals("")) {
			noOfHoursInMins = Integer.valueOf(hours) * 60;
		}
		if (minutes != null && !minutes.trim().equals("")) {
			noOfMins = Integer.valueOf(minutes);
		}
		return noOfDaysInMins + noOfHoursInMins + noOfMins;
	}

	public static String[] getAdvBkDDHHMM(int AdvBkTimeInMins) {
		String[] advBkDDHHMM = { "0", "0", "0" };
		int days = AdvBkTimeInMins / (24 * 60);
		int hours = (AdvBkTimeInMins % (24 * 60)) / 60;
		int mins = (AdvBkTimeInMins % (24 * 60)) % 60;

		advBkDDHHMM[0] = (days == 0) ? "0" : String.valueOf(days);
		advBkDDHHMM[1] = (hours == 0) ? "0" : String.valueOf(hours);
		advBkDDHHMM[2] = (mins == 0) ? "0" : String.valueOf(mins);

		return advBkDDHHMM;
	}
}
