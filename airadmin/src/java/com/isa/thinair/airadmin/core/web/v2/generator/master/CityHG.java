package com.isa.thinair.airadmin.core.web.v2.generator.master;

import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.commons.api.exception.ModuleException;

public class CityHG {

	private static String clientErrors;

	/**
	 * Create Client Validations for City Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array of Client validations
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {

		if (clientErrors == null) {
			Properties moduleErrs = new Properties();
			moduleErrs.setProperty("um.airadmin.delete.confirmation", "deleteRecoredCfrm");
			moduleErrs.setProperty("um.City.form.description.required", "descriptionRequired");
			moduleErrs.setProperty("um.City.form.id.already.exist", "idExists");
			moduleErrs.setProperty("um.City.form.id.length", "cityCodeLengthInvalid");
			moduleErrs.setProperty("um.City.form.city.name.length", "cityNameLengthInvalid");
			moduleErrs.setProperty("um.City.form.Country.Code.required", "countryCodeReq");
			moduleErrs.setProperty("um.City.form.row.required", "segmentRqrd");
			moduleErrs.setProperty("um.City.form.fields.required", "fieldsRequire");
			moduleErrs.setProperty("um.City.system.delete.required", "selectRecord");
			moduleErrs.setProperty("um.City.system.error", "sysError");
			moduleErrs.setProperty("um.City.form.city.code.required", "cityCodeRequire");
			moduleErrs.setProperty("um.City.form.city.code.duplicate", "cityCodeDuplicate");
			moduleErrs.setProperty("um.City.form.city.name.required", "cityNameRequire");
			moduleErrs.setProperty("um.City.form.city.country.required", "cityCountryRequire");
			moduleErrs.setProperty("um.City.select.publish.status.required", "lccPublishStatusSelectionRequired");
			clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}
		
		return clientErrors;
	}
}
