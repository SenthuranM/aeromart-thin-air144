package com.isa.thinair.airadmin.core.web.v2.action.samples;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airmaster.api.model.Country;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

@Namespace(S2Constants.Namespace.PRIVATE_SAMPLES)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ShowCountryAdminAction {

	private static Log log = LogFactory.getLog(ShowCountryAdminAction.class);

	private Object[] rows;
	private int page;
	private int total;
	private int records;
	private String succesMsg;
	private String countryCode;
	private String countryName;
	private String currencyCode;
	private String remarks;
	private String status;
	private long version;

	private static AiradminConfig airadminConfig = new AiradminConfig();

	public String execute() throws Exception {

		Country country = new Country();
		country.setCountryCode(countryCode);
		country.setCountryName(countryName);
		country.setCurrencyCode(currencyCode);
		country.setRemarks(remarks);
		if (status != null)
			country.setStatus(status);
		else
			country.setStatus(Country.STATUS_INACTIVE);
		country.setVersion(version);
		try {
			ModuleServiceLocator.getLocationServiceBD().saveCountry(country);
			this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS);
		} catch (ModuleException e) {
			this.succesMsg = e.getMessageString();
		}
		return S2Constants.Result.SUCCESS;
	}

	public String deleteCountry() {

		Country country = new Country();
		country.setCountryCode(countryCode);
		country.setVersion(version);
		try {
			ModuleServiceLocator.getLocationServiceBD().deleteCountry(country);
			this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_DELETE_SUCCESS);
		} catch (ModuleException e) {
			this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_DELETE_FAIL);
		}
		return S2Constants.Result.SUCCESS;
	}

	public String searchCountry() {

		try {

			Page pgCoutry = ModuleServiceLocator.getLocationServiceBD().getCountries((this.page - 1) * 20, 20, null);
			this.records = pgCoutry.getTotalNoOfRecords();
			this.total = pgCoutry.getTotalNoOfRecords() / 20;
			int mod = pgCoutry.getTotalNoOfRecords() % 20;
			if (mod > 0)
				this.total = this.total + 1;
			if (this.page < 0)
				this.page = 1;

			Collection<Country> colCountry = pgCoutry.getPageData();
			if (colCountry != null) {
				Object[] dataRow = new Object[colCountry.size()];
				int index = 1;
				Iterator<Country> iter = colCountry.iterator();
				while (iter.hasNext()) {
					Map<String, Object> counmap = new HashMap<String, Object>();
					Country grdCountry = (Country) iter.next();
					counmap.put("id", new Integer(((page - 1) * 20) + index).toString());
					counmap.put("country", grdCountry);
					dataRow[index - 1] = counmap;
					index++;
				}
				this.rows = dataRow;
			}
		} catch (ModuleException e) {
			log.error(e);
		}
		return S2Constants.Result.SUCCESS;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public String getSuccesMsg() {
		return succesMsg;
	}

	public void setSuccesMsg(String succesMsg) {
		this.succesMsg = succesMsg;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public Object[] getRows() {
		return rows;
	}

	public void setRows(Object[] rows) {
		this.rows = rows;
	}
}
