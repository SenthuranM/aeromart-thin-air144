package com.isa.thinair.airadmin.core.web.action.reports.lms;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.handler.reports.lms.PointRedemptionReportRequestHandler;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Results({ @Result(name = AdminStrutsConstants.AdminAction.SUCCESS, value = AdminStrutsConstants.AdminJSP.POINT_REDEMPTION_DETAILED_REPORT),
		@Result(name = AdminStrutsConstants.AdminAction.ERROR, value = AdminStrutsConstants.AdminJSP.ERROR_JSP) })
public class PointRedemptionDetailedReportAction extends BaseRequestResponseAwareAction {

	public String execute() throws Exception {
		return PointRedemptionReportRequestHandler.execute(request, response);
	}
}
