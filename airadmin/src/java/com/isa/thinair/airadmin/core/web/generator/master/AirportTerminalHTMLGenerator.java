package com.isa.thinair.airadmin.core.web.generator.master;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airmaster.api.model.AirportTerminal;
import com.isa.thinair.commons.api.exception.ModuleException;

public class AirportTerminalHTMLGenerator {

	private static Log log = LogFactory.getLog(AirportTerminalHTMLGenerator.class);

	private static String clientErrors;

	/**
	 * Gets the Airport Terminal GRID ROW for Selected Airport
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Terminal ROW Data
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final String getAirportTerminalRowHtml(HttpServletRequest request) throws ModuleException {

		log.debug("inside AirportTerminalHTMLGenerator.getAirportTerminalRowHtml()");
		String strAirportCode = request.getParameter("txtAirportCode");

		List<AirportTerminal> listArr = new ArrayList<AirportTerminal>();
		if (strAirportCode != null && !strAirportCode.equals(""))
			listArr = (List<AirportTerminal>) ModuleServiceLocator.getAirportServiceBD().getAirportTerminals(strAirportCode,
					false);

		return createAirportTerminalRowHTML(listArr);
	}

	/**
	 * Creates the Terminal Grid from a Collection of Terminals
	 * 
	 * @param airportTerms
	 *            the Collection of Terminals
	 * @return String the Created Terminal Grid
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private String createAirportTerminalRowHTML(Collection<AirportTerminal> airportTerms) throws ModuleException {

		List<AirportTerminal> list = (List<AirportTerminal>) airportTerms;
		Object[] listArr = list.toArray();
		AirportTerminal airportTerm = null;
		StringBuffer sb = new StringBuffer("var arrData = new Array();");

		for (int i = 0; i < listArr.length; i++) {
			airportTerm = (AirportTerminal) listArr[i];
			boolean allowDelete = (boolean) ModuleServiceLocator.getFlightServiceBD().isTerminalUsedByFlightOrSchedule(
					airportTerm.getTerminalId());

			sb.append("arrData[" + i + "] = new Array();");
			sb.append("arrData[" + i + "][1] = '"
					+ ((airportTerm.getTerminalCode() != null) ? airportTerm.getTerminalCode() : "") + "';");
			sb.append("arrData[" + i + "][2] = '"
					+ ((airportTerm.getTerminalShortName() != null) ? airportTerm.getTerminalShortName() : "") + "';");
			sb.append("arrData[" + i + "][3] = '" + airportTerm.getDefaultTerminal() + "';");
			sb.append("arrData[" + i + "][4] = '" + airportTerm.getStatus() + "';");
			sb.append("arrData[" + i + "][5] = '" + airportTerm.getTerminalDescription() + "';");
			sb.append("arrData[" + i + "][6] = '" + airportTerm.getVersion() + "';");
			sb.append("arrData[" + i + "][7] = '" + airportTerm.getTerminalId() + "';");
			sb.append("arrData[" + i + "][8] = '" + allowDelete + "';");

		}
		return sb.toString();
	}

	/**
	 * Creates Client Validations for Terminal Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array of Client Validations
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {

		if (clientErrors == null) {
			Properties moduleErrs = new Properties();
			moduleErrs.setProperty("um.airadmin.delete.confirmation", "deleteRecoredCfrm");
			moduleErrs.setProperty("um.airportterminal.form.name.required", "terminalNameRqrd");
			moduleErrs.setProperty("um.airportterminal.form.des.required", "terminalDesRqrd");

			clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}
		return clientErrors;
	}

}
