package com.isa.thinair.airadmin.core.web.constants;

public final class AirAdminInternalConstants {

	public static interface MisEmailTemplateNames {
		/** mis regional sales report */
		public static final String MIS_REGIONAL_SALES_EMAIL = "mis_regional_sales_email";
		/** mis dash board sales by channel email */
		public static final String MIS_DASH_BOARD_SALES_BY_CHANNEL_EMAIL = "mis_dashboard_sales_by_channel_email";

	}
}
