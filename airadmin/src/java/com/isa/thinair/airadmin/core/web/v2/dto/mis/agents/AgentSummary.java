package com.isa.thinair.airadmin.core.web.v2.dto.mis.agents;

import java.math.BigDecimal;

public class AgentSummary {

	private String agentCode;

	private String agentName;

	private String turnover;

	private BigDecimal turnoverValue;

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getTurnover() {
		return turnover;
	}

	public void setTurnover(String turnover) {
		this.turnover = turnover;
	}

	public BigDecimal getTurnoverValue() {
		return turnoverValue;
	}

	public void setTurnoverValue(BigDecimal turnoverValue) {
		this.turnoverValue = turnoverValue;
	}
}
