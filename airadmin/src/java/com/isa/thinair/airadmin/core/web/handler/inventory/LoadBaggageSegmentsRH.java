package com.isa.thinair.airadmin.core.web.handler.inventory;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.inventory.LoadBaggageSegmentsHg;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airinventory.api.dto.baggage.FlightBaggageDTO;
import com.isa.thinair.airinventory.api.service.BaggageBusinessDelegate;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

public class LoadBaggageSegmentsRH extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(LoadBaggageSegmentsRH.class);

	private static String clientErrors;

	private static Object lock = new Object();

	private static final String PARAM_MODE = "hdnMode";

	private static AiradminConfig airadminConfig = new AiradminConfig();

	/**
	 * Execute Method for template Charges
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return the forward action
	 */
	public static String execute(HttpServletRequest request) {
		if (log.isDebugEnabled())
			log.debug("Begin LoadBaggageSegmentsRH.execute(HttpServletRequest request)::" + System.currentTimeMillis());

		String strHdnMode = request.getParameter(PARAM_MODE);
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;
		if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_SAVE)) {
			try {
				saveData(request);
				saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS), WebConstants.MSG_SUCCESS);

			} catch (ModuleException mex) {
				log.error("Exception in  Save Baggage:", mex);
				saveMessage(request, mex.getMessageString(), WebConstants.MSG_ERROR);

			} catch (Exception exception) {
				log.error("Exception in  LoadBaggageSegmentsRH.execute()", exception);
				if (exception instanceof RuntimeException) {
					forward = WebConstants.FORWARD_ERROR;
					JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
				} else {
					try {
						setFormData(request);
					} catch (Exception e) {
						log.error(e);
					}
					saveMessage(request, exception.getMessage(), WebConstants.MSG_ERROR);
				}
			}
		}

		try {
			setDisplayData(request);
		} catch (ModuleException mex) {
			log.error("Exception in  Baggage charge Roll forward:", mex);
			saveMessage(request, mex.getMessageString(), WebConstants.MSG_ERROR);

		} catch (Exception exception) {
			log.error("Exception in  LoadSegmentsRH.execute()", exception);
			if (exception instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
			} else {
				saveMessage(request, exception.getMessage(), WebConstants.MSG_ERROR);
			}
		}
		return forward;
	}

	/**
	 * Saves the Data
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	private static void saveData(HttpServletRequest request) throws Exception {
		String strData = request.getParameter("hdnData");
		String classOfService = request.getParameter("cos");
		boolean blnBaggage = Boolean.parseBoolean(request.getParameter("blnBag"));
		boolean blnBgRemarks = Boolean.parseBoolean(request.getParameter("blnBgRmk"));
		if (blnBaggage) {
			FlightBaggageDTO fltBaggageDto = null;
			String[] arrData = strData.split(",");
			Collection<FlightBaggageDTO> colSegs = new HashSet<FlightBaggageDTO>();
			int noOfSegs = arrData.length / 7;
			int dataIndex = 0;
			for (int i = 0; i < noOfSegs; i++) {
				dataIndex = 7 * i;
				fltBaggageDto = new FlightBaggageDTO();
				fltBaggageDto.setFlightSegmentID(new Integer(arrData[dataIndex + 2]));
				if (arrData[dataIndex + 3] != null && !arrData[dataIndex + 3].trim().equals("")) {
					fltBaggageDto.setTemplateId(new Integer(arrData[dataIndex + 3]));
				} else {
					fltBaggageDto.setTemplateId(0);
				}
				if (arrData[dataIndex + 4] != null && !arrData[dataIndex + 4].trim().equals("")) {
					fltBaggageDto.setCabinClassCode(arrData[dataIndex + 4]);
				}
				colSegs.add(fltBaggageDto);
			}
			BaggageBusinessDelegate baggageBd = (BaggageBusinessDelegate) ModuleServiceLocator.getBaggageBD();
			ServiceResponce resp = baggageBd.assignFlightBaggageCharges(colSegs, null, false, classOfService);
			setServiceResponse(resp, request);
		} else if (blnBgRemarks && !blnBaggage) {
			FlightSegement flightSegment = null;
			String[] arrData = strData.split(",");
			FlightBD flightbd = ModuleServiceLocator.getFlightServiceBD();
			int noOfSegs = arrData.length / 7;
			int dataIndex = 0;
			for (int i = 0; i < noOfSegs; i++) {
				dataIndex = 7 * i;
				int fltSegID = new Integer(arrData[dataIndex + 2]);
				flightSegment = flightbd.getFlightSegment(fltSegID);
				if(flightSegment!=null) {
					flightSegment.setBaggageAllowanceRemarks(arrData[dataIndex + 5]);
					flightbd.updateFlightSegment(flightSegment);
				}
			}
		}
		
	}

	/**
	 * sets the service responce data to the request
	 * 
	 * @param resp
	 *            the ServiceResponce
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setServiceResponse(ServiceResponce resp, HttpServletRequest request) {

		if (resp.isSuccess()) {
			saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS), WebConstants.MSG_SUCCESS);
		}

	}

	/**
	 * Sets the display data
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static void setDisplayData(HttpServletRequest request) throws ModuleException {
		setBaggageTemplateCodes(request);
		setClientErrors(request);
		setFlightData(request);
		setRequestAttributes(request);
		// setFlitStatus(request);
	}

	/**
	 * private static void setFlitStatus(HttpServletRequest request) throws ModuleException { String strFlightId =
	 * request.getParameter("fltId"); if (strFlightId != null && !"".equals(strFlightId)) { Flight flight =
	 * ModuleServiceLocator.getFlightServiceBD().getFlight(new Integer(strFlightId)); if (flight.getScheduleId() !=
	 * null) { request.setAttribute(WebConstants.BAGGAGE_SCED_STATS, "blnIsSced = true;"); } }
	 * 
	 * }
	 */

	/**
	 * Sets the template data
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setBaggageTemplateCodes(HttpServletRequest request) throws ModuleException {
		String strModel = request.getParameter("modelNo");
		String s[] = null;
		int i = 0;
		StringBuilder sb = new StringBuilder();
		StringBuilder ssb = new StringBuilder();
		Collection<String[]> colTemplates = SelectListGenerator.createBaggageChargesTemplateIDs(strModel);
		if (colTemplates != null) {
			Iterator<String[]> iter = colTemplates.iterator();
			while (iter.hasNext()) {
				s = (String[]) iter.next();
				ssb.append("arrTemp[" + i + "] = new Array('" + s[0] + "','" + s[1] + "','" + s[2] + "');");
				sb.append("<option value='" + s[0] + "'>" + s[1] + "</option>");
				i++;
			}
		}
		request.setAttribute("reqSelModel", strModel);
		request.setAttribute(WebConstants.BAGGAGE_TEMPL_CODE, sb.toString());
		request.setAttribute("reqTemplArray", ssb.toString());
	}

	/**
	 * Sets the Client validations
	 * 
	 * @param request
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setClientErrors(HttpServletRequest request) throws ModuleException {
		String strClientErrors = getClientErrors(request);
		if (strClientErrors == null) {
			strClientErrors = "";
		}
		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
	}

	private static void setFormData(HttpServletRequest request) throws ModuleException {
		String strData = request.getParameter("hdnData");
		request.setAttribute(WebConstants.REQ_FORM_FIELD_VALUES, strData);
	}

	/**
	 * Gets the client validations
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the client validations
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {
		synchronized (lock) {
			if (clientErrors == null) {
				Properties moduleErrs = new Properties();

				clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
			}
			return clientErrors;
		}
	}

	/**
	 * Sets the Flight Data
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setFlightData(HttpServletRequest request) throws ModuleException {
		String strHtml = LoadBaggageSegmentsHg.getFlightData(request);
		request.setAttribute(WebConstants.BAGGAGE_SEGMENT_DATA, strHtml);
	}
	
	/**
	 * Sets the Baggage Privileges
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setRequestAttributes(HttpServletRequest request) throws ModuleException {
		boolean blnShowBaggage = false;
		boolean blnShowBaggageAllowance = false;
		
		String strBaggageAllowanceRemarks = ModuleServiceLocator.getGlobalConfig().getBizParam(SystemParamKeys.SHOW_BAGGAGE_ALLOWANCE_REMARKS);
		
		if (strBaggageAllowanceRemarks != null && strBaggageAllowanceRemarks.trim().equals("Y")){
			blnShowBaggageAllowance = true;
		}
		
		String strModel = request.getParameter("modelNo");
		String strBaggage = ModuleServiceLocator.getGlobalConfig().getBizParam(SystemParamKeys.SHOW_BAGGAGE);

		if (strBaggage != null && strBaggage.trim().equals("Y")) {
			// enabled in application parameter
			log.debug("show baggage enabled in app-param");
			Collection<String[]> colBaggageTemplates = SelectListGenerator.createBaggageChargesTemplateIDs(strModel);
			if (colBaggageTemplates != null && colBaggageTemplates.size() > 0) {
				// active baggage templates available
				log.debug("baggage template available-showbaggage");
				blnShowBaggage = true;
			}
		}
		
		request.setAttribute(WebConstants.SHOW_BAGGAGE_ALLOWANCE , String.valueOf(blnShowBaggageAllowance));
		request.setAttribute(WebConstants.SHOW_BAGGAGE , String.valueOf(blnShowBaggage));
	}
}
