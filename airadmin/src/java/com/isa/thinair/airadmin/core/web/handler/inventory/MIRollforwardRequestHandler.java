/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airadmin.core.web.handler.inventory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airadmin.core.web.util.SeatInventoryHelper;
import com.isa.thinair.airadmin.core.web.v2.util.CommonUtil;
import com.isa.thinair.airinventory.api.dto.InvRollForwardCriteriaDTO;
import com.isa.thinair.airinventory.api.dto.InvRollForwardFlightsSearchCriteria;
import com.isa.thinair.airinventory.api.dto.InvRollforwardStatusDTO;
import com.isa.thinair.airinventory.api.model.RollForwardBatchREQSEG;
import com.isa.thinair.airinventory.core.util.AirinventoryUtils;
import com.isa.thinair.airschedules.api.dto.FlightSummaryDTO;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.commons.api.constants.DayOfWeek;
import com.isa.thinair.commons.api.exception.MessagesUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.webplatform.core.constants.MessageCodes;

/**
 * @author Srikantha
 * @author Nasly
 */
public class MIRollforwardRequestHandler extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(MIRollforwardRequestHandler.class);

	private static String clientErrors;

	private static Object lock = new Object();

	private static SeatInventoryHelper seatInventoryHelper = new SeatInventoryHelper();

	private static final String CARRIER_SEPERATOR_WITH_ESCAPE = "\\|";

	private static final String PARAM_MODE = "hdnMode";

	public static final String PARAM_FROMDATE_1 = "txtFromDate_1";

	public static final String PARAM_TODATE_1 = "txtToDate_1";

	public static final String PARAM_SUNDAY = "chkSunday";

	public static final String PARAM_MONDAY = "chkMonday";

	public static final String PARAM_TUESDAY = "chkTuesday";

	public static final String PARAM_WEDNESDAY = "chkWednesday";

	public static final String PARAM_THURSDAY = "chkThursday";

	public static final String PARAM_FRIDAY = "chkFriday";

	public static final String PARAM_SATURDAY = "chkSaturday";

	public static final String PARAM_SELECTED_SEGS_AND_BCS = "hdnSelectedSegsAndBCs";

	public static final String PARAM_SELECTED_FLIGHTS = "hdnSelectedFlights";

	public static final String PARAM_DELETE_TARGET_BC_INVENTORIES = "chkDeleteTargetInventories";

	public static final String PARAM_OVERRIDE_LEVEL = "radOverrideLevel";

	public static final String PARAM_SEAT_FACTOR_MIN = "seatFactorMin";

	public static final String PARAM_SEAT_FACTOR_MAX = "seatFactorMax";

	public static final String PARAM_RADIO_MODE = "radMode";

	public static final String PARAM_FROMDATE_2 = "txtFromDate_2";

	public static final String PARAM_TODATE_2 = "txtToDate_2";

	public static final String PARAM_FROMDATE_3 = "txtFromDate_3";

	public static final String PARAM_TODATE_3 = "txtToDate_3";

	public static final String PARAM_FROMDATE_4 = "txtFromDate_4";

	public static final String PARAM_TODATE_4 = "txtToDate_4";

	public static final String PARAM_FROMDATE_5 = "txtFromDate_5";

	public static final String PARAM_TODATE_5 = "txtToDate_5";

	public static final String MODE_ADVANCE = "Advance";

	public static final String CARRIER_SEPERATOR = "|";

	public static final String COMMA_SEPERATOR = ",";

	public static final String SAVED_BATCH_ID = "savedBatchId";

	public static final String REQUEST_ADVANCE_FLAG = "advanceFlag";

	public static final String ADVANCE_MODE_SUCCESS_MSG = "Roll forward Batch saved and processing in progress...";

	public interface rollForawrdOptions {
		String CURTAIL = "radRfC";
		String OVERSELL = "radRfO";
		String WAITLISTING = "radRfW";
		String PRIORITY = "chkCopyPriority";
		String BCSTATUS = "chkBCStatus";
		String DELETE_ODD_BC = "chkDelOddBCs";
		String CREATE_SEG_ALLOCS = "selRollFwdSegAllocs";
	}

	/**
	 * Execte Method for Roll Forward the Inventory Action
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {
		if (log.isDebugEnabled())
			log.debug("Begin MIRollforwardRequestHandler.execute(HttpServletRequest request)::" + System.currentTimeMillis());

		String strHdnMode = request.getParameter(PARAM_MODE);
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;
		if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_SAVE)) {
			try {
				saveData(request);

			} catch (Exception exception) {
				log.error("Exception in  MIRollforwardRequestHandler.execute()", exception);
				if (exception instanceof RuntimeException) {
					forward = WebConstants.FORWARD_ERROR;
					JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
				} else if (exception instanceof ModuleException) {
					saveMessage(request, CommonUtil.getFormattedMessage(((ModuleException) exception).getExceptionCode(),
							((ModuleException) exception).getMessageParameters()), WebConstants.MSG_ERROR);
				} else {
					saveMessage(request, exception.getMessage(), WebConstants.MSG_ERROR);
				}
			}
		}

		try {
			setDisplayData(request);
		} catch (Exception exception) {
			log.error("Exception in  MIRollforwardRequestHandler.execute()", exception);
			if (exception instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
			} else {
				saveMessage(request, exception.getMessage(), WebConstants.MSG_ERROR);
			}
		}
		if (log.isDebugEnabled())
			log.debug("End MIRollforwardRequestHandler.execute(HttpServletRequest request)::" + System.currentTimeMillis());
		return forward;
	}

	/**
	 * Saves the Roll Forward
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	public static void saveData(HttpServletRequest request) throws Exception {

		String strSelectedSegBCs = request.getParameter(MIRollforwardRequestHandler.PARAM_SELECTED_SEGS_AND_BCS);
		String strFlights = request.getParameter(MIRollforwardRequestHandler.PARAM_SELECTED_FLIGHTS);

		String strFromDate_1 = request.getParameter(MIRollforwardRequestHandler.PARAM_FROMDATE_1);
		String strToDate_1 = request.getParameter(MIRollforwardRequestHandler.PARAM_TODATE_1);
		String strFromDate_2 = request.getParameter(MIRollforwardRequestHandler.PARAM_FROMDATE_2);
		String strToDate_2 = request.getParameter(MIRollforwardRequestHandler.PARAM_TODATE_2);
		String strFromDate_3 = request.getParameter(MIRollforwardRequestHandler.PARAM_FROMDATE_3);
		String strToDate_3 = request.getParameter(MIRollforwardRequestHandler.PARAM_TODATE_3);
		String strFromDate_4 = request.getParameter(MIRollforwardRequestHandler.PARAM_FROMDATE_4);
		String strToDate_4 = request.getParameter(MIRollforwardRequestHandler.PARAM_TODATE_4);
		String strFromDate_5 = request.getParameter(MIRollforwardRequestHandler.PARAM_FROMDATE_5);
		String strToDate_5 = request.getParameter(MIRollforwardRequestHandler.PARAM_TODATE_5);

		String strDeleteTarget = request.getParameter(MIRollforwardRequestHandler.PARAM_DELETE_TARGET_BC_INVENTORIES);
		String strOverrideLevel = request.getParameter(MIRollforwardRequestHandler.PARAM_OVERRIDE_LEVEL);
		String strCreateSegAllocs = request.getParameter(MIRollforwardRequestHandler.rollForawrdOptions.CREATE_SEG_ALLOCS);
		String strRadioMode = request.getParameter(MIRollforwardRequestHandler.PARAM_RADIO_MODE);

		String reqSelectedDataJS = "";
		reqSelectedDataJS += "var reqMode = 'afterSave';";
		reqSelectedDataJS += "var ls = new Listbox('Source BC Inventories', 'Selected BC Inventories', 'spn1');";
		reqSelectedDataJS += "var strSelectedSegmentsAndBCs = '"
				+ request.getParameter(MIRollforwardRequestHandler.PARAM_SELECTED_SEGS_AND_BCS) + "';";
		reqSelectedDataJS += "var strNotSelectedSegmentsAndBCs = '" + request.getParameter("hdnNotSelectedSegsAndBCs") + "';";
		reqSelectedDataJS += "var ls = new Listbox('lstPModes', 'lstAssignedPModes', 'spnFlightNoList','lspm');";
		reqSelectedDataJS += "var strFlights = '" + request.getParameter(MIRollforwardRequestHandler.PARAM_SELECTED_FLIGHTS)
				+ "';";
		reqSelectedDataJS += "var strNotSelectedFlights = '" + request.getParameter("hdnNotSelectedFlights") + "';";
		reqSelectedDataJS += "var strSelectedData = '" + strFromDate_1 + "," + strToDate_1 + "," + strOverrideLevel + ","
				+ strDeleteTarget + "," + strCreateSegAllocs + "," + strRadioMode + "';";

		String strFCCInventoryInfo = request.getParameter("hdnFCCInventoryInfo");
		request.setAttribute("reqFCCInventoryInfo", strFCCInventoryInfo);

		// preparing the frequency
		reqSelectedDataJS += "var selectedFrequency ='";

		Collection<DayOfWeek> daysCol = new ArrayList<DayOfWeek>();

		if (request.getParameter(MIRollforwardRequestHandler.PARAM_SUNDAY) != null
				&& request.getParameter(MIRollforwardRequestHandler.PARAM_SUNDAY).equals(WebConstants.CHECKBOX_ON)) {
			daysCol.add(DayOfWeek.SUNDAY);
			reqSelectedDataJS += "Sun,";
		}
		if (request.getParameter(MIRollforwardRequestHandler.PARAM_MONDAY) != null
				&& request.getParameter(MIRollforwardRequestHandler.PARAM_MONDAY).equals(WebConstants.CHECKBOX_ON)) {
			daysCol.add(DayOfWeek.MONDAY);
			reqSelectedDataJS += "Mon,";
		}

		if (request.getParameter(MIRollforwardRequestHandler.PARAM_TUESDAY) != null
				&& request.getParameter(MIRollforwardRequestHandler.PARAM_TUESDAY).equals(WebConstants.CHECKBOX_ON)) {
			daysCol.add(DayOfWeek.TUESDAY);
			reqSelectedDataJS += "Tue,";
		}

		if (request.getParameter(MIRollforwardRequestHandler.PARAM_WEDNESDAY) != null
				&& request.getParameter(MIRollforwardRequestHandler.PARAM_WEDNESDAY).equals(WebConstants.CHECKBOX_ON)) {
			daysCol.add(DayOfWeek.WEDNESDAY);
			reqSelectedDataJS += "Wed,";
		}

		if (request.getParameter(MIRollforwardRequestHandler.PARAM_THURSDAY) != null
				&& request.getParameter(MIRollforwardRequestHandler.PARAM_THURSDAY).equals(WebConstants.CHECKBOX_ON)) {
			daysCol.add(DayOfWeek.THURSDAY);
			reqSelectedDataJS += "Thu,";
		}

		if (request.getParameter(MIRollforwardRequestHandler.PARAM_FRIDAY) != null
				&& request.getParameter(MIRollforwardRequestHandler.PARAM_FRIDAY).equals(WebConstants.CHECKBOX_ON)) {
			daysCol.add(DayOfWeek.FRIDAY);
			reqSelectedDataJS += "Fri,";
		}

		if (request.getParameter(MIRollforwardRequestHandler.PARAM_SATURDAY) != null
				&& request.getParameter(MIRollforwardRequestHandler.PARAM_SATURDAY).equals(WebConstants.CHECKBOX_ON)) {
			daysCol.add(DayOfWeek.SATURDAY);
			reqSelectedDataJS += "Sat,";
		}
		reqSelectedDataJS += "';";

		// set seat factor values to the criteria
		String strSeatFactorMin = request.getParameter(MIRollforwardRequestHandler.PARAM_SEAT_FACTOR_MIN);
		String strSeatFactorMax = request.getParameter(MIRollforwardRequestHandler.PARAM_SEAT_FACTOR_MAX);
		String priority = request.getParameter(MIRollforwardRequestHandler.rollForawrdOptions.PRIORITY);
		String bcStatus = request.getParameter(MIRollforwardRequestHandler.rollForawrdOptions.BCSTATUS);
		String deleteODDBC = request.getParameter(MIRollforwardRequestHandler.rollForawrdOptions.DELETE_ODD_BC);
		String createSegAllocs = request.getParameter(MIRollforwardRequestHandler.rollForawrdOptions.CREATE_SEG_ALLOCS);
		int curTail = MIRollforwardRequestHandler.rollForwardCurtail(request
				.getParameter(MIRollforwardRequestHandler.rollForawrdOptions.CURTAIL));
		int overSell = MIRollforwardRequestHandler.rollForwardCurtail(request
				.getParameter(MIRollforwardRequestHandler.rollForawrdOptions.OVERSELL));
		int waitListing = MIRollforwardRequestHandler.rollForwardCurtail(request
				.getParameter(MIRollforwardRequestHandler.rollForawrdOptions.WAITLISTING));

		StringBuffer daysColSB = new StringBuffer();

		daysColSB.append(request.getParameter(MIRollforwardRequestHandler.PARAM_SUNDAY)
				+ MIRollforwardRequestHandler.COMMA_SEPERATOR);
		daysColSB.append(request.getParameter(MIRollforwardRequestHandler.PARAM_MONDAY)
				+ MIRollforwardRequestHandler.COMMA_SEPERATOR);
		daysColSB.append(request.getParameter(MIRollforwardRequestHandler.PARAM_TUESDAY)
				+ MIRollforwardRequestHandler.COMMA_SEPERATOR);
		daysColSB.append(request.getParameter(MIRollforwardRequestHandler.PARAM_WEDNESDAY)
				+ MIRollforwardRequestHandler.COMMA_SEPERATOR);
		daysColSB.append(request.getParameter(MIRollforwardRequestHandler.PARAM_THURSDAY)
				+ MIRollforwardRequestHandler.COMMA_SEPERATOR);
		daysColSB.append(request.getParameter(MIRollforwardRequestHandler.PARAM_FRIDAY)
				+ MIRollforwardRequestHandler.COMMA_SEPERATOR);
		daysColSB.append(request.getParameter(MIRollforwardRequestHandler.PARAM_SATURDAY));

		Map<String, String> datesCollection = new HashMap<String, String>();
		datesCollection.put(strFromDate_1, strToDate_1);
		if (null != strFromDate_2 && null != strToDate_2) {
			datesCollection.put(strFromDate_2, strToDate_2);
		}
		if (null != strFromDate_3 && null != strToDate_3) {
			datesCollection.put(strFromDate_3, strToDate_3);
		}
		if (null != strFromDate_4 && null != strToDate_4) {
			datesCollection.put(strFromDate_4, strToDate_4);
		}
		if (null != strFromDate_5 && null != strToDate_5) {
			datesCollection.put(strFromDate_5, strToDate_5);
		}

		Boolean isBatchAlreadyScheduled = isBatchAlreadySceduled(strRadioMode, strFCCInventoryInfo, strFlights, datesCollection);

		if (isBatchAlreadyScheduled) {

			saveMessage(request, MessagesUtil.getMessage("airinventory.arg.rollforward.already.scheduled"),
					WebConstants.MSG_WARNING);

		} else {

			String[] fccInventoryInfoArr = StringUtils.split(strFCCInventoryInfo, ",");

			String[] flightNumberArray = strFlights.split(CARRIER_SEPERATOR_WITH_ESCAPE);

			List<InvRollForwardCriteriaDTO> criteriaDTOList = new ArrayList<InvRollForwardCriteriaDTO>();
			for (String flight : flightNumberArray) {
				for (String strFromDate : datesCollection.keySet()) {
					InvRollForwardFlightsSearchCriteria searchCriteriaDTO = new InvRollForwardFlightsSearchCriteria();

					// preparing and setting the segment , logical cabin class and booking codes
					if (strSelectedSegBCs != null) {
						String[] segLccBCsArr = StringUtils.split(strSelectedSegBCs, "|");
						for (int i = 0; i < segLccBCsArr.length; i++) {
							String[] segmentLccBCArr = StringUtils.split(segLccBCsArr[i], ":");
							String[] segLccArr = StringUtils.split(segmentLccBCArr[0], "-");
							String segmentCode = segLccArr[0].trim();
							String logicalCabinClass = segLccArr[1].trim();

							String[] BCsArr = StringUtils.split(segmentLccBCArr[1], ",");
							List<String> bookingCodes = new ArrayList<String>();
							for (int j = 0; j < BCsArr.length; j++) {
								String bookingCode = BCsArr[j];
								bookingCodes.add(bookingCode.substring(0, bookingCode.indexOf(' ')));
							}
							if (!bookingCodes.isEmpty()) {
								searchCriteriaDTO.addBookingClasses(segmentCode, logicalCabinClass, bookingCodes);
							}
						}
					}
					// setting the flight number
					searchCriteriaDTO.setFlightNumber(flight);

					// setting the frequency
					searchCriteriaDTO.setFrequency(CalendarUtil.getFrequencyFromDays(daysCol));

					searchCriteriaDTO.setOriginAirportCode(fccInventoryInfoArr[2]);

					// setting the origin airport
					searchCriteriaDTO.setDestinationAirportCode(fccInventoryInfoArr[3]);

					if (strSeatFactorMin != null && !strSeatFactorMin.equals("")) {
						searchCriteriaDTO.setSeatFactorMin(Integer.parseInt(strSeatFactorMin));
					}
					if (strSeatFactorMax != null && !strSeatFactorMax.equals("")) {
						searchCriteriaDTO.setSeatFactorMax(Integer.parseInt(strSeatFactorMax));
					}

					// preparing and setting the from date
					SimpleDateFormat dtfmt = new SimpleDateFormat("dd/MM/yyyy");
					Date fromDate = null;

					fromDate = dtfmt.parse(strFromDate);
					searchCriteriaDTO.setFromDate(fromDate);

					// preparing and setting the to date
					Date toDate = null;

					toDate = dtfmt.parse(datesCollection.get(strFromDate));
					Calendar calToDate = new GregorianCalendar();
					calToDate.setTime(toDate);
					calToDate.set(Calendar.HOUR, 23);
					calToDate.set(Calendar.MINUTE, 59);
					calToDate.set(Calendar.SECOND, 59);
					calToDate.set(Calendar.MILLISECOND, 999);
					searchCriteriaDTO.setToDate(calToDate.getTime());

					searchCriteriaDTO.setRestrictNumberOfRecords(false);

					// preparing the flight IDs
					Set<Integer> flightIds = new HashSet<Integer>();
					Collection<FlightSummaryDTO> collectionFligthSummary = ModuleServiceLocator.getFlightServiceBD()
							.getLikeFlightSummary(searchCriteriaDTO);
					if (collectionFligthSummary != null && collectionFligthSummary.size() > 0) {
						FlightSummaryDTO flightSummaryDTO = null;
						Iterator<FlightSummaryDTO> iter = collectionFligthSummary.iterator();

						while (iter.hasNext()) {
							flightSummaryDTO = iter.next();
							int flightID = flightSummaryDTO.getFlightId();
							flightIds.add(flightID);
						}
					}

					InvRollForwardCriteriaDTO criteriaDTO = new InvRollForwardCriteriaDTO();
					criteriaDTO.setInvRollForwardFlightsSearchCriteria(searchCriteriaDTO);
					criteriaDTO.setFlights(flightIds);

					if (strDeleteTarget != null && strDeleteTarget.equals(WebConstants.CHECKBOX_ON)) {
						criteriaDTO.setDeleteTargetBCInventoriesFirst(true);
					}

					if (strOverrideLevel != null) {
						if (strOverrideLevel.equals("None")) {
							criteriaDTO.setBcInventoryOverridingLevel(InvRollForwardCriteriaDTO.OVERRIDE_NONE);
						} else if (strOverrideLevel.equals("NoResOnly")) {
							criteriaDTO
									.setBcInventoryOverridingLevel(InvRollForwardCriteriaDTO.OVERRIDE_NOT_HAVING_RESERVATION_ONLY);
						} else if (strOverrideLevel.equals("All")) {
							criteriaDTO.setBcInventoryOverridingLevel(InvRollForwardCriteriaDTO.OVERRIDE_ALL);
						}
					}

					if (request.getParameter(rollForawrdOptions.PRIORITY) != null
							&& request.getParameter(rollForawrdOptions.PRIORITY).equalsIgnoreCase(WebConstants.CHECKBOX_ON)) {
						criteriaDTO.setRollFwdBCPriority(true);
					}

					if (request.getParameter(rollForawrdOptions.BCSTATUS) != null
							&& request.getParameter(rollForawrdOptions.BCSTATUS).equalsIgnoreCase(WebConstants.CHECKBOX_ON)) {
						criteriaDTO.setRollFwdBCStatus(true);
					}

					if (request.getParameter(rollForawrdOptions.DELETE_ODD_BC) != null
							&& request.getParameter(rollForawrdOptions.DELETE_ODD_BC).equalsIgnoreCase(WebConstants.CHECKBOX_ON)) {
						criteriaDTO.setRemoveOddTargetBCs(true);
					}

					if (request.getParameter(rollForawrdOptions.CREATE_SEG_ALLOCS) != null) {
						criteriaDTO.setCreateSegAllocs(Integer.valueOf(strCreateSegAllocs));
					}

					criteriaDTO.setRollForwardCurtail(rollForwardCurtail(request.getParameter(rollForawrdOptions.CURTAIL)));
					criteriaDTO.setRollForwardOversell(rollForwardCurtail(request.getParameter(rollForawrdOptions.OVERSELL)));
					criteriaDTO
							.setRollForwardWaitListing(rollForwardCurtail(request.getParameter(rollForawrdOptions.WAITLISTING)));

					// preparing the flight id
					criteriaDTO.setSourceFlightId(Integer.parseInt(fccInventoryInfoArr[0]));

					// preparing the cabin class code
					criteriaDTO.setSourceCabinClassCode(fccInventoryInfoArr[4]);

					criteriaDTOList.add(criteriaDTO);
				}

			}
			if (MIRollforwardRequestHandler.MODE_ADVANCE.equalsIgnoreCase(strRadioMode)) {

				if (AppSysParamsUtil.isEnableAdvanceRollForwardOperation()
						&& hasPrivilege(request, WebConstants.PRIV_INV_ALLOC_ROLL_FORWARD_ADVANCE)) {

					Integer batchId = 0;
					try {
						batchId = AirinventoryUtils.getRollForwardBatchREQBD().doAdvanceRollForward(criteriaDTOList);

					} catch (Exception exception) {
						MIRollforwardRequestHandler.log.error("Exception in  MIRollforwardRequestHandler.execute()", exception);
						if (exception instanceof RuntimeException) {
							JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
						} else {
							saveMessage(request, exception.getMessage(), WebConstants.MSG_ERROR);
						}
					}
					saveMessage(request, "", WebConstants.MSG_SUCCESS);

					request.setAttribute(MIRollforwardRequestHandler.REQUEST_ADVANCE_FLAG, Boolean.TRUE);

					request.setAttribute(MIRollforwardRequestHandler.SAVED_BATCH_ID, batchId);

					// to set the success message to client
					saveMessage(request, MIRollforwardRequestHandler.ADVANCE_MODE_SUCCESS_MSG, WebConstants.MSG_SUCCESS);
				} else {
					saveMessage(request, MessageCodes.UNAUTHORIZED_ACCESS_TO_SCREEN, WebConstants.MSG_ERROR);
				}

			} else {

				// Existing Basic Rollforward code moved to new method
				doBasicRollForward(request, criteriaDTOList);
			}
		}

		request.setAttribute("reqSelectedDataJS", reqSelectedDataJS);
	}

	private static void doBasicRollForward(HttpServletRequest request, List<InvRollForwardCriteriaDTO> criteriaDTOList)
			throws ParseException, ModuleException {

		StringBuilder seatOverrideLogs = new StringBuilder();

		InvRollForwardCriteriaDTO criteriaDTO = criteriaDTOList.get(0);

		Set<Integer> flightIds = (Set<Integer>) criteriaDTO.getFlights();

		// If no matching flights found, throw the exception. No need to call the BD method if no
		// matching flights found
		if (flightIds.size() > 0) {

			log.info("MIRollforwardRequestHandler.saveData() About to Rollforward inv, Source Flight Id is : "
					+ criteriaDTO.getSourceFlightId() + " And Source Cabin class is : " + criteriaDTO.getSourceCabinClassCode()
					+ ". Target Flight Ids are : " + flightIds.toString());

			if (log.isDebugEnabled()) {
				log.debug("Inventory Roll forward Criteria is  : - " + criteriaDTO.toString());
			}

			InvRollforwardStatusDTO rollForwardStatus = ModuleServiceLocator.getFlightInventoryBD().rollFlightInventory(
					flightIds, criteriaDTO);

			// updating flight status
			if (rollForwardStatus.getFlightIdsForStatusUpdating().size() > 0) {
				Collection<Integer> chunkedLists = new ArrayList<Integer>();
				Iterator<Integer> flightIdsIt = rollForwardStatus.getFlightIdsForStatusUpdating().iterator();
				for (int i = 0; flightIdsIt.hasNext(); ++i) {
					chunkedLists.add(flightIdsIt.next());
					if (((i != 0) && (i % 30 == 0)) || (i == (rollForwardStatus.getFlightIdsForStatusUpdating().size() - 1))) {
						seatInventoryHelper.updateFlightStatus(chunkedLists, FlightStatusEnum.ACTIVE);
						chunkedLists.clear();
					}
				}
			}

			if (rollForwardStatus.getFltInventoryErrorMap().size() > 0) {
				Set<String> flightIdsWithCC = rollForwardStatus.getFltInventoryErrorMap().keySet();
				seatOverrideLogs.append("<Br>Seat override logs : <Br>");
				// This list is used to query the db. We can't use keyset here.
				List<Integer> tmpAuditFlightIds = new ArrayList<Integer>();
				Map<Integer, String> flightIdsAndCC = new HashMap<Integer, String>();
				for (String flightIdAndCC : flightIdsWithCC) {
					String[] idAndCC = flightIdAndCC.split("\\|");
					int auditFlightId = Integer.valueOf(idAndCC[0]);
					flightIdsAndCC.put(auditFlightId, idAndCC[1]);
					tmpAuditFlightIds.add(auditFlightId);
				}
				String rollForwardAudit = "";
				List<Flight> flights = ModuleServiceLocator.getFlightServiceBD().getLightWeightFlightDetails(tmpAuditFlightIds);
				int i = 0;
				for (Flight flight : flights) {
					if (i != 0) {
						rollForwardAudit += "@";
					}
					SimpleDateFormat sdt = new SimpleDateFormat("dd-MM-yyyy HH:mm");
					Date arrivalDateZulu = flight.getDepartureDate();
					Iterator<FlightSegement> segmentIterator = flight.getFlightSegements().iterator();
					if (segmentIterator.hasNext()) {
						arrivalDateZulu = segmentIterator.next().getEstTimeArrivalZulu();
					}
					String flightIdAndCC = flight.getFlightId() + "|" + flightIdsAndCC.get(flight.getFlightId());
					rollForwardAudit += flight.getFlightId() + "|" + flight.getFlightNumber() + "|"
							+ sdt.format(flight.getDepartureDate()) + "|" + flight.getOriginAptCode() + "|"
							+ rollForwardStatus.getFltInventoryErrorMap().get(flightIdAndCC) + "|" + flight.getModelNumber()
							+ "|" + flightIdsAndCC.get(flight.getFlightId()) + "|" + sdt.format(arrivalDateZulu);
					seatOverrideLogs.append(createEntryForViewLog(sdt.format(flight.getDepartureDate()), rollForwardStatus
							.getFltInventoryErrorMap().get(flightIdAndCC)));
					i++;
				}
				request.setAttribute("rollForwardAudit", rollForwardAudit);
			}

			// to set the success message to client
			saveMessage(request, rollForwardStatus.getMsg(),
					rollForwardStatus.getStatus() == InvRollforwardStatusDTO.OPERATION_FAILURE
							? WebConstants.MSG_ERROR
							: WebConstants.MSG_SUCCESS);
			if (rollForwardStatus.getStatus() == InvRollforwardStatusDTO.OPERATION_COMPLETED) {
				StringBuilder strLog = new StringBuilder(rollForwardStatus.getStrLog());
				request.setAttribute("reqLog", seatOverrideLogs.toString().toUpperCase() + strLog.toString());
				request.setAttribute("reqhasLog", true);
			}

			log.info("MIRollforwardRequestHandler.saveData() Finish Rollforwarding inv, Source Flight Id is : "
					+ criteriaDTO.getSourceFlightId() + " And Source Cabin class is : " + criteriaDTO.getSourceCabinClassCode());
		} else {
			saveMessage(request, "No matching flight found for rollforwarding", WebConstants.MSG_WARNING);
		}

	}

	public static String createEntryForViewLog(String dptDate, StringBuffer comment) {
		StringBuilder logEntry = new StringBuilder();
		logEntry.append("DEPARTURE DATE : ").append(dptDate).append(",		");
		logEntry.append("LOG : ").append(comment.toString()).append("<Br>");
		return logEntry.toString();
	}

	private static Boolean isBatchAlreadySceduled(String strRadioMode, String strFCCInventoryInfo, String strFlights,
			Map<String, String> datesCollection) {

		String[] flightNumberArray = strFlights.split(CARRIER_SEPERATOR_WITH_ESCAPE);

		for (String flight : flightNumberArray) {
			for (String fromDate : datesCollection.keySet()) {
				Collection<RollForwardBatchREQSEG> rollForwardBatchREQSEGList = AirinventoryUtils.getRollForwardBatchREQSEGBD()
						.getRollForwardBatchREQSEGByDateAndFlights(fromDate, datesCollection.get(fromDate), flight);
				if (rollForwardBatchREQSEGList.size() > 0) {
					return Boolean.TRUE;
				}
			}
		}

		return Boolean.FALSE;
	}

	public static void setDisplayData(HttpServletRequest request) throws ModuleException {
		setSegmentSelectHtml(request);
		setFlightSelectHtml(request);
		setClientErrors(request);
		setPrivilagesHtml(request);
		request.setAttribute("reqAdvanceRollforward", AppSysParamsUtil.isEnableAdvanceRollForwardOperation());
	}

	private static void setFlightSelectHtml(HttpServletRequest request) throws ModuleException {

		StringBuffer sb = new StringBuffer("var arrData = new Array();");
		sb.append("var methods = new Array();");

		String strFCCInventoryInfo = request.getParameter("hdnFCCInventoryInfo");

		request.setAttribute("reqFCCInventoryInfo", strFCCInventoryInfo);

		String[] fccInventoryInfoArr = StringUtils.split(strFCCInventoryInfo, ",");

		InvRollForwardFlightsSearchCriteria searchCriteriaDTO = new InvRollForwardFlightsSearchCriteria();

		searchCriteriaDTO.setOriginAirportCode(fccInventoryInfoArr[2]);

		searchCriteriaDTO.setDestinationAirportCode(fccInventoryInfoArr[3]);

		Collection<String> collectionFlight = ModuleServiceLocator.getFlightServiceBD().getFutureFlightsNoByOriginDestination(
				fccInventoryInfoArr[2], fccInventoryInfoArr[3]);

		int tempInt = 0;

		Iterator<String> flightsIter = collectionFlight.iterator();
		while (flightsIter.hasNext()) {
			String flight = flightsIter.next();
			sb.append("methods[" + tempInt + "] = new Array('" + flight + "','" + flight + "'); ");
			tempInt++;
		}

		sb.append("arrData[0] = methods;");
		sb.append(" var lspm = new Listbox('lstPModes', 'lstAssignedPModes', 'spnFlightNoList', 'lspm');");
		sb.append("lspm.group1 = arrData[0];");
		sb.append("lspm.height = '100px'; lspm.width = '150px';");
		sb.append("lspm.headingLeft = '&nbsp;&nbsp;All Flights';");
		sb.append("lspm.headingRight = '&nbsp;&nbsp;Selected Flights';");
		sb.append("lspm.drawListBox();");
		request.setAttribute(WebConstants.REQ_HTML_FLIGHTNUMBER_LIST, sb.toString());

	}

	private static void setSegmentSelectHtml(HttpServletRequest request) throws ModuleException {

		String strFCCInventoryInfo = request.getParameter("hdnFCCInventoryInfo");
		SimpleDateFormat dtfmt = new SimpleDateFormat("dd/MM/yyyy");
		strFCCInventoryInfo = strFCCInventoryInfo + "," + dtfmt.format(new Date());
		request.setAttribute("reqFCCInventoryInfo", strFCCInventoryInfo);
		String[] fccInventoryInfoArr = StringUtils.split(strFCCInventoryInfo, ",");

		int flightId = Integer.parseInt(fccInventoryInfoArr[0]);
		String cabinClassCode = fccInventoryInfoArr[4];

		LinkedHashMap<String, List<Object[]>> segmentBCInvsMap = ModuleServiceLocator.getFlightInventoryBD()
				.getFCCSegBCInventories(flightId, cabinClassCode, true);

		request.setAttribute(WebConstants.REQ_SEGMENT_LIST,
				JavascriptGenerator.createSegmentsBookingCodesListHTML(segmentBCInvsMap));

		request.setAttribute("reqFrequncy", seatInventoryHelper.getFrequencyForFlight(flightId));

		request.setAttribute("hdnFCCInventoryInfo", strFCCInventoryInfo);

		String reqSelectedDataJS = "var reqMode = 'display';";
		reqSelectedDataJS += "var ls = new Listbox('Source BC Inventories', 'Selected BC Inventories', 'spn1');";
		request.setAttribute("reqSelectedDataJS", reqSelectedDataJS);
		request.setAttribute("reqIsLogicalCCEnabled", AppSysParamsUtil.isLogicalCabinClassEnabled());
	}

	private static void setClientErrors(HttpServletRequest request) throws ModuleException {
		String strClientErrors = getClientErrors(request);
		if (strClientErrors == null) {
			strClientErrors = "";
		}
		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
	}

	/**
	 * Creates Client validations for Inventory Roll Forward Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Client Validations Array
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {
		synchronized (lock) {
			if (clientErrors == null) {
				Properties moduleErrs = new Properties();

				moduleErrs.setProperty("um.seatinventory.form.rollforward.fromdate.required", "rollForwardFromDateRqrd");
				moduleErrs.setProperty("um.seatinventory.form.rollforward.dates.overlapping",
						"rollForwardFromDateToDateOverlapping");
				moduleErrs.setProperty("um.seatinventory.form.rollforward.fromdate.lessthan.currenctdate",
						"rollForwardFromDateLessthanCurrentDate");
				moduleErrs.setProperty("um.seatinventory.form.rollforward.todate.required", "rollForwardToDateRqrd");
				moduleErrs.setProperty("um.seatinventory.form.rollforward.todate.notlessthan.fromdate",
						"rollForwardFromDateLessthanFromDate");
				moduleErrs.setProperty("um.seatinventory.form.rollforward.advanced.addmorethanfivedate",
						"rollForwardMorethanFiveDateRanges");
				moduleErrs.setProperty("um.seatinventory.form.rollforward.frequency.required", "rollForwardFrequencyRqrd");
				moduleErrs.setProperty("um.seatinventory.form.rollforward.segmentlist.notselected", "rollForwardSegmentRqrd");
				moduleErrs.setProperty("um.seatinventory.form.rollforward.bcallocation.checked", "rollForwardBCAllocRqrd");
				moduleErrs.setProperty("um.seatinventory.form.rollforward.notdone", "rollForwardNoMatchingBC");
				moduleErrs.setProperty("um.seatinventory.form.rollforward.seatfactor.min", "rollForwardSFMin");
				moduleErrs.setProperty("um.seatinventory.form.rollforward.seatfactor.max", "rollForwardSFMax");
				moduleErrs.setProperty("um.seatinventory.form.rollforward.seatfactor.minmax", "rollForwardSFMinMax");

				clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
			}
			return clientErrors;
		}
	}

	public static int rollForwardCurtail(String value) {
		if (value != null) {
			try {
				return Integer.parseInt(value);
			} catch (NumberFormatException e) {
				return 0;
			}
		}
		return 0;
	}

	private static void setPrivilagesHtml(HttpServletRequest request) throws ModuleException {

		HashMap mapPrivileges = (HashMap) request.getSession().getAttribute(WebConstants.SES_PRIVILEGE_IDS);
		boolean overrideFrequencyOnRollFwd = (mapPrivileges.get(WebConstants.OVERRIDE_FREQUENCY_ON_ROLL_FWD) != null)
				? true
				: false;
		request.setAttribute(WebConstants.REQ_HTML_OVERRIDE_FREQUENCY_ON_ROLL_FWD, overrideFrequencyOnRollFwd);
	}

}
