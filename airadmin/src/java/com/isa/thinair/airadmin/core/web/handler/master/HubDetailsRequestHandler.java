package com.isa.thinair.airadmin.core.web.handler.master;

import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.master.HubDetailsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airmaster.api.model.HubInfo;
import com.isa.thinair.commons.api.exception.MessagesUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/*******************************************************************************
 * Description : Hub Details Request Handler
 * 
 * @author : Nadika Dhanusha Mahatantila
 * @since : 25th Feb 2008
 ******************************************************************************/

public final class HubDetailsRequestHandler extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(HubDetailsRequestHandler.class);
	private static AiradminConfig adminConfig = new AiradminConfig();

	private static final String PARAM_HUB_CODE = "selHub";
	private static final String PARAM_MIN_CONNECTION_TIME = "txtMinConnectTime";
	private static final String PARAM_MAX_CONNECTION_TIME = "txtMaxConnectTime";
	private static final String PARAM_IB_CARRIER = "selInCarrier";
	private static final String PARAM_OB_CARRIER = "selOutCarrier";
	private static final String PARAM_STATUS = "chkStatus";

	private static final String PARAM_MODE = "hdnModel";
	private static final String PARAM_VERSION = "hdnVersion";
	private static final String PARAM_HDN_ID = "hdnId";
	private static final String PARAM_RECNO = "hdnRecNo";
	private static final String PARAM_HDN_HUB_CODE = "hdnHubCode";

	private static final String ACT_STATUS = "Active";

	/**
	 * Main Execute method for Hub Details Action & sets the Success Int : 0-Not Applicable, 1-Success, 2-Fail
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {

		String strHdnMode = request.getParameter("hdnModel");
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;

		setIntSuccess(request, 0);
		String strFormData = "var saveSuccess = " + getIntSuccess(request) + ";";
		request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);

		setExceptionOccured(request, false);
		setAttribInRequest(request, "strModeJS", "var isAddMode = false;");
		setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = false;");

		// Save Action
		if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_SAVE)) {
			try {
				saveData(request);
				if (!isExceptionOccured(request)) {
					setIntSuccess(request, 1);
					strFormData += " var saveSuccess = " + getIntSuccess(request) + ";";
					request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
				}
			} catch (Exception exception) {
				log.error("Exception in HubDetailsRequestHandler:execute()", exception);
				if (exception instanceof RuntimeException) {
					forward = WebConstants.FORWARD_ERROR;
					JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
				}
			}
		}

		// Delete Action
		if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_DELETE)) {
			checkPrivilege(request, WebConstants.PRIV_SYS_MAS_HUB_DET_DELETE);
			try {
				deleteData(request);
				log.debug("\nHubDetailsRequestHandler delete() success");

			} catch (ModuleException me) {
				log.error("Exception in HubDetailsRequestHandler:execute() [origin module=" + me.getModuleDesc() + "]", me);

			} catch (Exception exception) {
				log.error("Exception in HubDetailsRequestHandler:execute()", exception);
				if (exception instanceof RuntimeException) {
					forward = WebConstants.FORWARD_ERROR;
					JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
				}
			}
		}

		try {
			setDisplayData(request);
			log.debug("\nHubDetailsRequestHandler setDisplayData() success");

		} catch (ModuleException me) {
			log.error("Exception in HubDetailsRequestHandler:execute() [origin module=" + me.getModuleDesc() + "]", me);
			saveMessage(request, me.getMessageString(), WebConstants.MSG_ERROR);
		}

		return forward;
	}

	/**
	 * Saving the information to the System
	 * 
	 * @param request
	 * @throws Exception
	 */
	private static void saveData(HttpServletRequest request) throws Exception {
		HubInfo hubInfo = new HubInfo();
		String strAction = request.getParameter("hdnMode");
		Properties properties = getProperties(request);

		if (strAction.equals(WebConstants.ACTION_ADD)) {
			checkPrivilege(request, WebConstants.PRIV_SYS_MAS_HUB_DET_ADD);
			setAttribInRequest(request, "strModeJS", "var isAddMode = true;");

		} else if (strAction.equals(WebConstants.ACTION_EDIT)) {
			checkPrivilege(request, WebConstants.PRIV_SYS_MAS_HUB_DET_EDIT);
			setAttribInRequest(request, "strModeJS", "var isAddMode = false;");
		}

		try {

			hubInfo.setAirportCode(properties.getProperty(PARAM_HUB_CODE));
			hubInfo.setMinConnectionTime(properties.getProperty(PARAM_MIN_CONNECTION_TIME));
			hubInfo.setMaxConnectionTime(properties.getProperty(PARAM_MAX_CONNECTION_TIME));
			hubInfo.setIbCarrierCode((properties.getProperty(PARAM_IB_CARRIER).equals("")) ? null : properties
					.getProperty(PARAM_IB_CARRIER));
			hubInfo.setObCarrierCode((properties.getProperty(PARAM_OB_CARRIER).equals("")) ? null : properties
					.getProperty(PARAM_OB_CARRIER));

			// Sets the Status of the Hub
			if (properties.getProperty(PARAM_STATUS).equals(ACT_STATUS)) {
				hubInfo.setStatus(HubInfo.STATUS_ACTIVE);
			} else {
				hubInfo.setStatus(HubInfo.STATUS_INACTIVE);
			}

			// Sets the editable status
			if ((properties.getProperty(PARAM_IB_CARRIER).equals("")) && (properties.getProperty(PARAM_OB_CARRIER).equals(""))) {
				hubInfo.setEditable(HubInfo.EDITABLE_FALSE);
			} else {
				hubInfo.setEditable(HubInfo.EDITABLE_TRUE);
			}

			// Setting the version
			if (properties.getProperty(PARAM_VERSION) != null && !"".equals(properties.getProperty(PARAM_VERSION))) {
				hubInfo.setVersion(Long.parseLong(properties.getProperty(PARAM_VERSION)));
			}

			// Setting the Id
			if (properties.getProperty(PARAM_HDN_ID) != null && !"".equals(properties.getProperty(PARAM_HDN_ID))) {
				hubInfo.setHubId(Long.parseLong(properties.getProperty(PARAM_HDN_ID)));
			}

			ModuleServiceLocator.getLocationServiceBD().saveHubInfo(hubInfo);
			setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = true;");

			hubInfo = null;
			log.debug("\nHubDetailsRequestHandler savedata() success");

			if (strAction.equals(WebConstants.ACTION_ADD)) {
				saveMessage(request, adminConfig.getMessage("um.airadmin.add.success"), WebConstants.MSG_SUCCESS);
				setAttribInRequest(request, "strModeJS", "var isAddMode = true;");

			} else if (strAction.equals(WebConstants.ACTION_EDIT)) {
				saveMessage(request, adminConfig.getMessage("um.airadmin.edit.success"), WebConstants.MSG_SUCCESS);
				setAttribInRequest(request, "strModeJS", "var isAddMode = false;");

			} else {
				saveMessage(request, MessagesUtil.getMessage(WebConstants.KEY_SAVE_SUCCESS), WebConstants.MSG_SUCCESS);
			}

		} catch (ModuleException moduleException) {

			setExceptionOccured(request, true);
			String strFormData = getErrorForm(properties);
			setIntSuccess(request, 2);
			strFormData += " var saveSuccess = " + getIntSuccess(request) + ";";
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);

			log.error("Exception in HubDetailsRequestHandler:saveData() [origin module=" + moduleException.getModuleDesc() + "]",
					moduleException);
			if (moduleException.getMessageString().equals(MessagesUtil.getMessage("module.duplicate.key"))) {
				saveMessage(request, adminConfig.getMessage("um.hubdetails.form.code.defined"), WebConstants.MSG_ERROR);
			} else {
				saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
			}

		} catch (Exception exception) {

			log.error("Exception in HubDetailsRequestHandler:saveData()", exception);
			setExceptionOccured(request, true);
			if (exception instanceof RuntimeException) {
				throw exception;
			} else {

				String strFormData = getErrorForm(properties);
				setIntSuccess(request, 2);
				strFormData += " var saveSuccess = " + getIntSuccess(request) + ";";
				request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
				saveMessage(request, exception.getMessage(), WebConstants.MSG_ERROR);
			}
		}
	}

	/**
	 * Deleting data from the system
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void deleteData(HttpServletRequest request) throws ModuleException {
		Properties properties = getProperties(request);
		String strId = properties.getProperty(PARAM_HDN_ID);
		String strVersion = properties.getProperty(PARAM_VERSION);

		try {
			HubInfo hubInfo = new HubInfo();
			hubInfo.setHubId(Long.parseLong(strId));
			hubInfo.setAirportCode(properties.getProperty(PARAM_HDN_HUB_CODE));
			hubInfo.setVersion(Long.parseLong(strVersion));

			ModuleServiceLocator.getLocationServiceBD().removeHubInfo(hubInfo);

			setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = false;");
			saveMessage(request, adminConfig.getMessage(WebConstants.KEY_DELETE_SUCCESS), WebConstants.MSG_SUCCESS);

		} catch (ModuleException moduleException) {

			setExceptionOccured(request, true);
			log.error("Exception in HubDetailsRequestHandler:deleteData() [origin module=" + moduleException.getModuleDesc()
					+ "]", moduleException);

			if (moduleException.getMessageString().equals(MessagesUtil.getMessage("module.constraint.childrecord"))) {
				saveMessage(request, adminConfig.getMessage("um.airadmin.childrecord"), WebConstants.MSG_ERROR);
			} else {
				saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
			}
		}

	}

	/**
	 * Sets Display Data for Hub Details Page & sets the success int : 0-Not Applicable, 1-Success, 2-Fail
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setDisplayData(HttpServletRequest request) throws ModuleException {

		setClientErrors(request);
		setHubDetailsRowHtml(request);
		setSelectListData(request);

		if (!isExceptionOccured(request)) {
			String strFormData = "var arrFormData = new Array();";
			strFormData += " var saveSuccess = " + getIntSuccess(request) + ";";
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
		}
		request.setAttribute(WebConstants.REQ_OPERATION_MODE, getAttribInRequest(request, "strModeJS"));

		if (request.getParameter("hdnModel") == null) {
			setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = false;");
			request.setAttribute(WebConstants.REQ_TRANSACTION_STATUS,
					getAttribInRequest(request, "isSaveTransactionSuccessfulJS"));

		} else if (request.getParameter("hdnModel") != null && !request.getParameter("hdnModel").equals(WebConstants.ACTION_SAVE)) {

			setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = false;");
			request.setAttribute(WebConstants.REQ_TRANSACTION_STATUS,
					getAttribInRequest(request, "isSaveTransactionSuccessfulJS"));

		} else {
			request.setAttribute(WebConstants.REQ_TRANSACTION_STATUS,
					getAttribInRequest(request, "isSaveTransactionSuccessfulJS"));
		}

		request.setAttribute(WebConstants.REQ_TRANSACTION_DISPLAYSTATUS, "var isSuccessfulSaveMessageDisplayEnabled = "
				+ isSuccessfulSaveMessageDisplayEnabled() + ";");
	}

	/**
	 * Creating the client error messages
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setClientErrors(HttpServletRequest request) throws ModuleException {
		String strClientErrors = HubDetailsHTMLGenerator.getClientErrors(request);
		if (strClientErrors == null)
			strClientErrors = "";

		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
	}

	/**
	 * Sets the Hub Details Grid Data To the Request
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setHubDetailsRowHtml(HttpServletRequest request) throws ModuleException {
		HubDetailsHTMLGenerator hubDetailsHTMLGenerator = new HubDetailsHTMLGenerator();
		String strHTML = hubDetailsHTMLGenerator.getHubDetailsRowHtml(request);
		request.setAttribute(WebConstants.REQ_HTML_ROWS, strHTML);
	}

	/**
	 * Sets the Dropdown data
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setSelectListData(HttpServletRequest request) throws ModuleException {

		request.setAttribute(WebConstants.REQ_HTML_HUBS, SelectListGenerator.createActiveAirportCodeList());
		request.setAttribute(WebConstants.REQ_HTML_CARRIER_CODES, SelectListGenerator.getCarrierCodes());
	}

	/**
	 * Creats a Property File conatining Hub Info. data from the Request
	 * 
	 * @param request
	 * @return Properties
	 */
	private static Properties getProperties(HttpServletRequest request) {
		Properties props = new Properties();

		String strHubCode = AiradminUtils.getNotNullString(request.getParameter(PARAM_HUB_CODE));
		String strMinConnectTime = AiradminUtils.getNotNullString(request.getParameter(PARAM_MIN_CONNECTION_TIME));
		String strMaxConnectTime = AiradminUtils.getNotNullString(request.getParameter(PARAM_MAX_CONNECTION_TIME));
		String strIBCarrier = AiradminUtils.getNotNullString(request.getParameter(PARAM_IB_CARRIER));
		String strOBCarrier = AiradminUtils.getNotNullString(request.getParameter(PARAM_OB_CARRIER));
		String strStatus = AiradminUtils.getNotNullString(request.getParameter(PARAM_STATUS));
		String strMode = AiradminUtils.getNotNullString(request.getParameter(PARAM_MODE));
		String strVersion = AiradminUtils.getNotNullString(request.getParameter(PARAM_VERSION));
		String strHdnId = AiradminUtils.getNotNullString(request.getParameter(PARAM_HDN_ID));
		String strRecNo = request.getParameter(PARAM_RECNO);
		String strHdnHubCode = AiradminUtils.getNotNullString(request.getParameter(PARAM_HDN_HUB_CODE));

		props.setProperty(PARAM_HUB_CODE, strHubCode);
		props.setProperty(PARAM_MIN_CONNECTION_TIME, strMinConnectTime);
		props.setProperty(PARAM_MAX_CONNECTION_TIME, strMaxConnectTime);
		props.setProperty(PARAM_IB_CARRIER, strIBCarrier);
		props.setProperty(PARAM_OB_CARRIER, strOBCarrier);
		props.setProperty(PARAM_STATUS, strStatus);
		props.setProperty(PARAM_MODE, strMode);
		props.setProperty(PARAM_VERSION, strVersion);
		props.setProperty(PARAM_HDN_ID, strHdnId);
		props.setProperty(PARAM_RECNO, (strRecNo == null) ? "1" : strRecNo);
		props.setProperty(PARAM_HDN_HUB_CODE, strHdnHubCode);

		return props;
	}

	/**
	 * Creats the Form Data for Hub Details page from the Property file
	 * 
	 * @param erprop
	 * @return String
	 */
	private static String getErrorForm(Properties erprop) {

		StringBuffer ersb = new StringBuffer("var arrFormData = new Array();");

		ersb.append("arrFormData[0] = new Array();");
		ersb.append("arrFormData[0][1] = '" + erprop.getProperty(PARAM_HUB_CODE) + "';");
		ersb.append("arrFormData[0][2] = '" + erprop.getProperty(PARAM_MIN_CONNECTION_TIME) + "';");
		ersb.append("arrFormData[0][3] = '" + erprop.getProperty(PARAM_MAX_CONNECTION_TIME) + "';");
		ersb.append("arrFormData[0][4] = '" + erprop.getProperty(PARAM_IB_CARRIER) + "';");
		ersb.append("arrFormData[0][5] = '" + erprop.getProperty(PARAM_OB_CARRIER) + "';");
		ersb.append("arrFormData[0][6] = '" + erprop.getProperty(PARAM_STATUS) + "';");
		ersb.append("arrFormData[0][7] = '" + erprop.getProperty(PARAM_VERSION) + "';");
		ersb.append("arrFormData[0][8] = '" + erprop.getProperty(PARAM_HDN_ID) + "';");

		return ersb.toString();
	}

	/**
	 * @return boolean
	 */
	private static boolean isSuccessfulSaveMessageDisplayEnabled() {
		boolean isMessageEnabled = false;
		if (adminConfig.getMessage("um.airadmin.savemessage.display").equals("true")) {
			isMessageEnabled = true;
		}
		return isMessageEnabled;
	}

	/**
	 * @param request
	 * @param value
	 */
	private static void setExceptionOccured(HttpServletRequest request, boolean value) {
		setAttribInRequest(request, "isExceptionOccured", new Boolean(value));
	}

	/**
	 * @param request
	 * @return boolean
	 */
	private static boolean isExceptionOccured(HttpServletRequest request) {
		return ((Boolean) getAttribInRequest(request, "isExceptionOccured")).booleanValue();
	}

	/**
	 * @param request
	 * @param value
	 */
	private static void setIntSuccess(HttpServletRequest request, int value) {
		setAttribInRequest(request, "intSuccess", new Integer(value));
	}

	/**
	 * @param request
	 * @return int
	 */
	private static int getIntSuccess(HttpServletRequest request) {
		Integer returnValue = (Integer) getAttribInRequest(request, "intSuccess");
		if (returnValue != null)
			return returnValue.intValue();
		else
			return -1;
	}

}
