package com.isa.thinair.airadmin.core.web.action.master;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.handler.master.AdminFeeAgentRequestHandler;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Results({ @Result(name = AdminStrutsConstants.AdminAction.SUCCESS, value = AdminStrutsConstants.AdminJSP.ADMIN_FEE_AGENT_ADMIN_JSP),
		@Result(name = AdminStrutsConstants.AdminAction.ERROR, value = AdminStrutsConstants.AdminJSP.ERROR_JSP) })
public class ShowAdminFeeAgentAction extends BaseRequestAwareAction {

	private static final Log log = LogFactory.getLog(ShowAdminFeeAgentAction.class);

	public String execute() throws Exception {
		log.debug("Inside the ShowAdminFeeAgentAction.execute()...");
		return AdminFeeAgentRequestHandler.execute(request);
	}

}
