package com.isa.thinair.airadmin.core.web.generator.tools;

import java.util.Properties;
import javax.servlet.http.HttpServletRequest;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.commons.api.exception.ModuleException;

public class CreateRequestQueueHTMLGenerator {
	private static String clientErrors;
	/**
	 * Create Client Validations for Meal Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array of Client validationsf
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {

		if (clientErrors == null) {
			Properties moduleErrs = new Properties();
			moduleErrs.setProperty("um.requestqueue.search.failed", "searchFailed");
			moduleErrs.setProperty("um.requestqueue.delete.failed", "deleteFailed");
			moduleErrs.setProperty("um.requestqueue.queuename", "queuenameEmpty");
			moduleErrs.setProperty("um.requestqueue.queuedescription", "descriptionEmpty");
			moduleErrs.setProperty("um.requestqueue.applicablepages", "applicablepagesEmpty");
			moduleErrs.setProperty("um.requestqueue.assignedusers", "assignedusersEmpty");
			clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}

		return clientErrors;
	}

}
