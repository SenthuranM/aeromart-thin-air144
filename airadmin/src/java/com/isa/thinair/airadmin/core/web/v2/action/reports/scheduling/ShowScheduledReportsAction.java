package com.isa.thinair.airadmin.core.web.v2.action.reports.scheduling;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.v2.action.common.CommonAdminRequestResponseAwareCommonAction;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airadmin.core.web.v2.util.DataUtil;
import com.isa.thinair.airadmin.core.web.v2.util.RowDecorator;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.reporting.api.criteria.SchduledReportsSearchCriteria;
import com.isa.thinair.reporting.api.dto.scheduledRpts.ScheduledReportsViewTO;
import com.isa.thinair.reporting.api.model.SRSentItem;
import com.isa.thinair.reporting.api.model.ScheduledReport;
import com.isa.thinair.webplatform.core.schedrept.ScheduleReportUtil;

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ShowScheduledReportsAction extends CommonAdminRequestResponseAwareCommonAction {
	
	private static final Log log = LogFactory.getLog(ShowScheduledReportsAction.class);

	private Collection<Map<String, Object>> rows;
	private Collection<Map<String, Object>> sentGridRows;
	private int page;
	private int total;
	private int records;
	private int sentGridPage;
	private int sentGridTotal;
	private int sentGridRecords;
	private static final int PAGE_LENGTH = 10;

	public static enum ReportNames {
		companyPaymentSummary, companyPaymentDetail, modeofPaymentRpt, invoiceSummary, transactionReport,
		seatInventoryCollections, revenueAndTaxReport, ondReport, staffPerformanceReport, agentSalesModifyRefundReport,
		pointRedemptionReport, promotionInfoReport, gstr1Report, gstr3Report, gstAdditionalReport;
	}

	private ScheduledReportsViewTO reportsViewTO;
	private SchduledReportsSearchCriteria reportsSearchTO;

	public String execute() {

		return S2Constants.Result.SUCCESS;
	}

	@SuppressWarnings("unchecked")
	public String searchSchduled() {
		try {
			int startIndex = (page - 1) * PAGE_LENGTH;
			Page pagedReocrds = ModuleServiceLocator.getScheduledReportBD().getAllScheduleReports(reportsSearchTO, startIndex,
					PAGE_LENGTH);
			this.page = getCurrentPageNo(pagedReocrds.getStartPosition());
			this.total = getTotalNoOfPages(pagedReocrds.getTotalNoOfRecords());// total number of pages
			this.records = pagedReocrds.getTotalNoOfRecords();

			this.rows = (Collection<Map<String, Object>>) DataUtil.createGridData(pagedReocrds.getStartPosition(),
					"reportsViewTO", pagedReocrds.getPageData(), decorateRow());
			this.setDefaultSuccessMessage();
		} catch (Exception e) {
			log.error("Error in searching schduled reports", e);
			this.handleException(e);
		}
		return S2Constants.Result.SUCCESS;
	}

	public String saveScheduledReport() {
		try {
			if (reportsViewTO != null) {
				ModuleServiceLocator.getScheduledReportBD().modifyScheduleReport(reportsViewTO);
			}
			this.setDefaultSuccessMessage();
		} catch (Exception e) {
			log.error("Error in saving schduled reports", e);
			this.handleException(e);
		}
		return S2Constants.Result.SUCCESS;
	}

	public String activateDeactivate() {
		try {
			if (reportsViewTO != null) {
				ModuleServiceLocator.getScheduledReportBD().activateDeactivteScheduled(reportsViewTO);
			}
			this.setDefaultSuccessMessage();
		} catch (Exception e) {
			log.error("Error in activate/deactivate schduled reports", e);
			this.handleException(e);
		}
		return S2Constants.Result.SUCCESS;
	}

	public int getTotalNoOfPages(int totoalNoOfRecords) {
		int totalNoOfPages = 1;
		if (PAGE_LENGTH != 0) {
			totalNoOfPages = (int) totoalNoOfRecords / PAGE_LENGTH;
			if (totoalNoOfRecords % PAGE_LENGTH > 0)
				totalNoOfPages++;
		}
		return totalNoOfPages;
	}

	private RowDecorator<ScheduledReport> decorateRow() {
		RowDecorator<ScheduledReport> airlineDecorator = new RowDecorator<ScheduledReport>() {

			@Override
			public void decorateRow(HashMap<String, Object> row, ScheduledReport record) {
				row.put("reportsViewTO.reportName", record.getReportName());
				row.put("reportsViewTO.reportDisplayName", getReportDisplayName(ReportNames.valueOf(record.getReportName())));
				row.put("reportsViewTO.scheduledReportID", record.getScheduledReportId());
				row.put("reportsViewTO.startDate", CalendarUtil.getDateInFormattedString("dd/MM/yyyy", record.getStartDate()));
				row.put("reportsViewTO.endDate", CalendarUtil.getDateInFormattedString("dd/MM/yyyy", record.getEndDate()));
				row.put("reportsViewTO.scheduledBy", record.getScheduledBy());
				row.put("reportsViewTO.status", record.getStatus());
				row.put("reportsViewTO.version", record.getVersion());
				row.put("reportsViewTO.cornExp", record.getCronExpression());
				Boolean isFronmDateEditable = CalendarUtil.isGreaterThan(record.getStartDate(),
						CalendarUtil.add(new Date(), 0, 0, 1, 0, 0, 0));
				Boolean isToDateEditable = CalendarUtil.isGreaterThan(record.getEndDate(),
						CalendarUtil.add(new Date(), 0, 0, 1, 0, 0, 0));
				row.put("isFromDateEditable", isFronmDateEditable.toString());
				row.put("isToDateEditable", isToDateEditable.toString());
				row.put("reportsViewTO.emailIDs", record.getSendReportTo());

			}

		};
		return airlineDecorator;
	}

	private String getReportDisplayName(ReportNames reportTypes) {
		switch (reportTypes) {
			case companyPaymentSummary:
				return ScheduleReportUtil.ScheduledReportReportDisplayNames.COMPANY_PAYMENT;
			case modeofPaymentRpt:
				return ScheduleReportUtil.ScheduledReportReportDisplayNames.MODE_OF_PAYMENT;
			case invoiceSummary:
				return ScheduleReportUtil.ScheduledReportReportDisplayNames.INVOICE_SUMMERY;
			case transactionReport:
				return ScheduleReportUtil.ScheduledReportReportDisplayNames.TRANSACTION_REPORT;
			case seatInventoryCollections:
				return ScheduleReportUtil.ScheduledReportReportDisplayNames.SEAT_INVENTORY_COLLECTIONS;
			case revenueAndTaxReport:
				return ScheduleReportUtil.ScheduledReportReportDisplayNames.REVENUE_AND_TAX;
			case ondReport:
				return ScheduleReportUtil.ScheduledReportReportDisplayNames.OND_REPORT;
			case staffPerformanceReport:
				return ScheduleReportUtil.ScheduledReportReportDisplayNames.STAFF_PERFORMANCE_REPORT;
			case pointRedemptionReport:
				return ScheduleReportUtil.ScheduledReportReportDisplayNames.POINT_REDEMPTION_REPORT;
			case promotionInfoReport:
				return ScheduleReportUtil.ScheduledReportReportDisplayNames.PROMOTION_INFO_REPORT;
			case gstr1Report:
				return ScheduleReportUtil.ScheduledReportReportDisplayNames.GSTR1_REPORT;
			case gstr3Report:
				return ScheduleReportUtil.ScheduledReportReportDisplayNames.GSTR3_REPORT;
			case gstAdditionalReport:
				return ScheduleReportUtil.ScheduledReportReportDisplayNames.GST_ADDITIONAL_REPORT;
			default:
				return "";
		}
	}

	@SuppressWarnings("unchecked")
	public String searchSRSentItems() {
		try {
			String reportID = reportsViewTO.getScheduledReportID();
			if (reportID != null && !"".equals(reportID)) {
				int startIndex = (page - 1) * PAGE_LENGTH;
				Page pagedReocrds = ModuleServiceLocator.getScheduledReportBD().getSRSentItemsForAScheduledReport(
						new Long(reportID), startIndex, PAGE_LENGTH);
				this.sentGridPage = getCurrentPageNo(pagedReocrds.getStartPosition());
				this.sentGridTotal = getTotalNoOfPages(pagedReocrds.getTotalNoOfRecords());// total number of pages
				this.sentGridRecords = pagedReocrds.getTotalNoOfRecords();

				this.sentGridRows = (Collection<Map<String, Object>>) DataUtil.createGridData(pagedReocrds.getStartPosition(),
						"reportsViewTO", pagedReocrds.getPageData(), decorateSRSentItemRow());
			}
			this.setDefaultSuccessMessage();
		} catch (Exception e) {
			log.error("Error in search sent schduled reports", e);
			this.handleException(e);
		}
		return S2Constants.Result.SUCCESS;
	}

	private RowDecorator<SRSentItem> decorateSRSentItemRow() {
		RowDecorator<SRSentItem> srDecorator = new RowDecorator<SRSentItem>() {

			@Override
			public void decorateRow(HashMap<String, Object> row, SRSentItem record) {
				row.put("sentTime", CalendarUtil.getDateInFormattedString("dd/MM/yyyy HH:mm:ss", record.getSentTime()));
				row.put("notes", record.getNotes());
				row.put("status", convertStatus(record.getStatus()));
			}
		};
		return srDecorator;
	}

	private String convertStatus(String status) {
		if (SRSentItem.STATUS_SENT.equals(status))
			return "Success";
		else if (SRSentItem.STATUS_FAILED.equals(status))
			return "Failed";
		else
			return "";
	}

	public int getCurrentPageNo(int startPosition) {
		int currentPageNo = 1;
		if (PAGE_LENGTH != 0) {
			currentPageNo = startPosition / PAGE_LENGTH + 1;
		}
		return currentPageNo;
	}

	public Collection<Map<String, Object>> getRows() {
		return rows;
	}

	public void setRows(Collection<Map<String, Object>> rows) {
		this.rows = rows;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ScheduledReportsViewTO getReportsViewTO() {
		return reportsViewTO;
	}

	public void setReportsViewTO(ScheduledReportsViewTO reportsViewTO) {
		this.reportsViewTO = reportsViewTO;
	}

	public SchduledReportsSearchCriteria getReportsSearchTO() {
		return reportsSearchTO;
	}

	public void setReportsSearchTO(SchduledReportsSearchCriteria reportsSearchTO) {
		this.reportsSearchTO = reportsSearchTO;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public Collection<Map<String, Object>> getSentGridRows() {
		return sentGridRows;
	}

	public void setSentGridRows(Collection<Map<String, Object>> sentGridRows) {
		this.sentGridRows = sentGridRows;
	}

	public int getSentGridPage() {
		return sentGridPage;
	}

	public void setSentGridPage(int sentGridPage) {
		this.sentGridPage = sentGridPage;
	}

	public int getSentGridTotal() {
		return sentGridTotal;
	}

	public void setSentGridTotal(int sentGridTotal) {
		this.sentGridTotal = sentGridTotal;
	}

	public int getSentGridRecords() {
		return sentGridRecords;
	}

	public void setSentGridRecords(int sentGridRecords) {
		this.sentGridRecords = sentGridRecords;
	}

}
