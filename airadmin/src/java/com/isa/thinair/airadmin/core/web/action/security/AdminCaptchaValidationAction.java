package com.isa.thinair.airadmin.core.web.action.security;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.login.util.ForceLoginInvoker;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.Constants;
import com.isa.thinair.webplatform.core.service.captchaService.CaptchaService;

@Namespace(AdminStrutsConstants.AdminNameSpace.PUBLIC_NAME_SPACE)
@Results({ @Result(name = AdminStrutsConstants.AdminAction.SUCCESS, type = JSONResult.class, value = ""),
		@Result(name = AdminStrutsConstants.AdminAction.ERROR, value = AdminStrutsConstants.AdminJSP.ERROR_JSP) })
public class AdminCaptchaValidationAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(AdminCaptchaValidationAction.class);

	private String captchaTxt;

	private boolean isCaptchaValidated = false;

	public String execute() {
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;
		String userIP = getIpAddress(request);

		if (AppSysParamsUtil.isCaptchaEnabledForAirAdminLogin()) {
			ForceLoginInvoker.defaultLogin();
			try {
				if (ModuleServiceLocator.getSecurityBD().isCaptchaEnableForUser(userIP)) {
					isCaptchaValidated = CaptchaService.getInstance().validateResponseForID(request.getSession().getId(),
							captchaTxt);
					request.getSession().setAttribute(WebConstants.ADMIN_LOGIN_CAPTCHA, isCaptchaValidated);
				}
			} catch (Exception e) {
				forward = AdminStrutsConstants.AdminAction.ERROR;
				log.error(e);
				JavascriptGenerator.setServerError(request, e.getMessage(), "", "");
			}
			ForceLoginInvoker.close();
		}

		return forward;
	}

	private String getIpAddress(HttpServletRequest request) {
		String ip = request.getHeader("X-Forwarded-For");

		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_CLIENT_IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}

		if (ip != null && !ip.trim().isEmpty()) {
			String[] clientIpArray = ip.split(Constants.COMMA_SEPARATOR);
			if (clientIpArray.length > 0) {
				for (String ipAddress : clientIpArray) {
					if (ipAddress != null && !ipAddress.trim().isEmpty()) {
						ip = ipAddress.trim();
						break;
					}
				}
			}
		}

		return ip;
	}

	/**
	 * @return the captchaTxt
	 */
	public String getCaptchaTxt() {
		return captchaTxt;
	}

	/**
	 * @param captchaTxt
	 *            the captchaTxt to set
	 */
	public void setCaptchaTxt(String captchaTxt) {
		this.captchaTxt = captchaTxt;
	}

	/**
	 * @return the isCaptchaValidated
	 */
	public boolean isCaptchaValidated() {
		return isCaptchaValidated;
	}

	/**
	 * @param isCaptchaValidated
	 *            the isCaptchaValidated to set
	 */
	public void setCaptchaValidated(boolean isCaptchaValidated) {
		this.isCaptchaValidated = isCaptchaValidated;
	}

}
