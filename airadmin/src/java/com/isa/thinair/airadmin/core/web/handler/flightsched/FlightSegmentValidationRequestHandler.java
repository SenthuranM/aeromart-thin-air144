package com.isa.thinair.airadmin.core.web.handler.flightsched;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.flightsched.FlightHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airadmin.core.web.util.FlightUtils;
import com.isa.thinair.airadmin.core.web.util.ScheduleUtils;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightLeg;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.GlobalConfig;

/**
 * @author Thushara
 * 
 */
public final class FlightSegmentValidationRequestHandler extends BasicRequestHandler {

	private static final String PARAM_MODE = "strSaveMode";
	private static final String PARAM_LEGS = "strLegs";
	private static final String PARAM_LEG_DTLS = "strLegDtls";
	private static final String PARAM_OVERLAP = "strOverlap";
	private static final String PARAM_STARTDATE = "strStartDate";
	private static final String PARAM_MODELNO = "strModel";
	private static final String PARAM_FLIGHTID = "strFlightId";
	private static final String PARAM_STATUS = "strStatus";
	private static final String PARAM_OVERLAPFLIGHTID = "strOverlapFltID";
	private static final String PARAM_TIMEMODE = "hdnTimeMode";
	private static final String PARAM_SCH_OPERATIONTYPE = "selOperationTypeSearch";
	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	private static Log log = LogFactory.getLog(FlightSegmentValidationRequestHandler.class);

	/**
	 * Main Execute Method for Flight Segment Actions
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;
		try {
			setDisplayData(request);
		} catch (Exception e) {
			log.error("Exception in FlightSegmentValidationRequestHandler.execute", e);
			if (e instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, e.getMessage(), "", "");
			} else {
				saveMessage(request, e.getMessage(), WebConstants.MSG_ERROR);
			}
		}
		return forward;
	}

	/**
	 * Sets the Display data for Flight Segment Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	public static void setDisplayData(HttpServletRequest request) throws Exception {
		setClientErrors(request);
		setSegmentValidationRowHtml(request);
		setSegmentValidationParametersHtml(request);
		setOverlapableFlightHtml(request);
		setOverLapableFlightSegmentRow(request);
	}

	/**
	 * Sets the Validation Row for Segments
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	protected static void setSegmentValidationRowHtml(HttpServletRequest request) throws ModuleException {
		String strHdnMode = AiradminUtils.getNotNullString(request.getParameter(PARAM_MODE));
		String strLegs = AiradminUtils.getNotNullString(request.getParameter(PARAM_LEGS));

		FlightHTMLGenerator flthtmGen = new FlightHTMLGenerator();
		String strHtml = "";
		if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_UPDATE)) {
			strHtml = flthtmGen.getSegmentValidationRowHtml(request);

		} else if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_SAVE)) {
			StringBuffer sbseg = new StringBuffer("var arrSegData = new Array();");
			ArrayList<String> arrSegList = ScheduleUtils.getSgements(strLegs);
			for (int i = 0; i < arrSegList.size(); i++) {
				sbseg.append("arrSegData[" + i + "] = new Array();");
				sbseg.append("arrSegData[" + i + "][0] = '" + i + "';");
				sbseg.append("arrSegData[" + i + "][1] = '" + arrSegList.get(i) + "';");
				sbseg.append("arrSegData[" + i + "][2] = 'true';");
			}
			strHtml = sbseg.toString();
		}
		request.setAttribute(WebConstants.REQ_HTML_SEGMENT_VALIDATION_DATA, strHtml);
	}

	/**
	 * Sets Over laping Flights details to Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setSegmentValidationParametersHtml(HttpServletRequest request) throws ModuleException {
		String strLegsHtml = AiradminUtils.getNotNullString(request.getParameter(PARAM_LEGS));
		String strLegDtlsHtml = AiradminUtils.getNotNullString(request.getParameter(PARAM_LEG_DTLS));
		String strOverlapHtml = AiradminUtils.getNotNullString(request.getParameter(PARAM_OVERLAP));
		request.setAttribute(WebConstants.REQ_HTML_LEGS, strLegsHtml);
		request.setAttribute(WebConstants.REQ_HTML_LEG_DTLS, strLegDtlsHtml);
		request.setAttribute(WebConstants.REQ_HTML_OVERLAPFLAG, strOverlapHtml);
	}

	/**
	 * Sets the Client Validations for Flight Segment Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static void setClientErrors(HttpServletRequest request) throws ModuleException {
		String strClientErrors = "";
		Properties moduleErrs = new Properties();
		moduleErrs.setProperty("um.flightschedule.validseg.validseg", "onevalidseg");
		moduleErrs.setProperty("um.flightschedule.validseg.nosegfound", "nosegfound");
		moduleErrs.setProperty("um.flightschedule.validseg.needinvalid", "needinvalid");
		moduleErrs.setProperty("um.flightschedule.validseg.reservationExist", "resExist");
		strClientErrors = JavascriptGenerator.createClientErrors(moduleErrs);

		if (strClientErrors == null) {
			strClientErrors = "";
		}
		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
	}

	/**
	 * Sets the Overlappable Flight Id Set to the request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 * @throws ParseException
	 *             the ParseException
	 */
	public static void setOverlapableFlightHtml(HttpServletRequest request) throws ModuleException, ParseException {
		String strOverlapflt = FlightUtils.createOverlapableFlightList(createFlight(request));
		request.setAttribute(WebConstants.REQ_HTML_OVERLAPABLE_SCHEDULE_DATA, strOverlapflt);
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 * @throws ParseException
	 */
	public static void setOverLapableFlightSegmentRow(HttpServletRequest request) throws ModuleException, ParseException {
		FlightHTMLGenerator htmlGen = new FlightHTMLGenerator();
		String strOverLapSegArr = htmlGen.createFlightSegmentValidationRowHtml(createFlight(request));
		request.setAttribute(WebConstants.REQ_HTML_OVERLAPABLE_SCHEDULE_ARRAY_DATA, strOverLapSegArr);
	}

	/**
	 * Creates A Flight from request Data
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return Flight the created Flight
	 */
	private static Flight createFlight(HttpServletRequest request) {

		String strLegDtlsHtml = AiradminUtils.getNotNullString(request.getParameter(PARAM_LEG_DTLS));
		String strStartDate = AiradminUtils.getNotNullString(request.getParameter(PARAM_STARTDATE));
		String strModelNo = AiradminUtils.getNotNullString(request.getParameter(PARAM_MODELNO));
		String strFlightID = request.getParameter(PARAM_FLIGHTID);
		String strStaus = AiradminUtils.getNotNullString(request.getParameter(PARAM_STATUS));
		String strTimeinMode = AiradminUtils.getNotNullString(request.getParameter(PARAM_TIMEMODE));
		String strOverlapid = AiradminUtils.getNotNullString(request.getParameter(PARAM_OVERLAPFLIGHTID));
		String operationTypeId = AiradminUtils.getNotNullString(request.getParameter(PARAM_SCH_OPERATIONTYPE));

		Flight flight = new Flight();
		String defaultFormat // get the default date format
		= globalConfig.getBizParam(SystemParamKeys.DATE_FORMAT_FOR_DATE_STRINGS);
		SimpleDateFormat dateFormat = new SimpleDateFormat(defaultFormat);

		if (strTimeinMode.equals(WebConstants.LOCALTIME)) {
			flight.setDepartureDateLocal(ScheduleUtils.parseToDate(strStartDate.trim()));
		} else {
			flight.setDepartureDate(ScheduleUtils.parseToDate(strStartDate.trim()));
		}

		flight.setModelNumber(strModelNo);
		flight.setStatus(strStaus);
		if ((operationTypeId != null) && (!operationTypeId.trim().equals(""))) {
			flight.setOperationTypeId(Integer.parseInt(operationTypeId));
		}

		// seting the flight id if available
		if (strFlightID != null && !"".equals(strFlightID.trim())) {
			flight.setFlightId(new Integer(strFlightID));
		}
		StringTokenizer legTok = new StringTokenizer(strLegDtlsHtml, ",");
		// setting flight legs
		Set<FlightLeg> legSet = new HashSet<FlightLeg>();
		int i = 1;
		while (legTok.hasMoreTokens()) {
			String leg = legTok.nextToken();
			String[] legArr = leg.split("_");
			FlightLeg flightLeg = new FlightLeg();
			flightLeg.setOrigin(legArr[0]);
			flightLeg.setDestination(legArr[1]);
			flightLeg.setLegNumber(i);
			try {
				if (strTimeinMode.equals(WebConstants.LOCALTIME)) {
					flightLeg.setEstDepartureTimeLocal(dateFormat.parse(legArr[2].trim()));
					flightLeg.setEstArrivalTimeLocal(dateFormat.parse(legArr[3].trim()));
					flightLeg.setEstDepartureDayOffsetLocal(Integer.parseInt(legArr[4]));
					flightLeg.setEstArrivalDayOffsetLocal(Integer.parseInt(legArr[5]));
				} else {
					flightLeg.setEstDepartureTimeZulu(dateFormat.parse(legArr[2].trim()));
					flightLeg.setEstArrivalTimeZulu(dateFormat.parse(legArr[3].trim()));
					flightLeg.setEstDepartureDayOffset(Integer.parseInt(legArr[4]));
					flightLeg.setEstArrivalDayOffset(Integer.parseInt(legArr[5]));
				}
			} catch (Exception e) {
				log.error(e);
			}
			legSet.add(flightLeg);
			i++;
		}
		flight.setFlightLegs(legSet);
		HashSet<FlightSegement> segmentSet = FlightUtils.getSegmentsFromLegSet(legSet);
		strOverlapid = strOverlapid.trim();

		if (!("").equals(strOverlapid.trim()) && !(strOverlapid.trim().equals("null")) && !(strOverlapid.trim().equals("&nbsp"))) {
			flight.setOverlapingFlightId(new Integer(strOverlapid));
		}
		flight.setFlightSegements(segmentSet);
		return flight;
	}
}
