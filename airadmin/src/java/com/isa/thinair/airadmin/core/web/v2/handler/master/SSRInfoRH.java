package com.isa.thinair.airadmin.core.web.v2.handler.master;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONUtil;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airmaster.api.dto.SSRSearchDTO;
import com.isa.thinair.airmaster.api.model.SSR;
import com.isa.thinair.airmaster.api.model.SSRApplicableAirports;
import com.isa.thinair.airmaster.api.model.SSRApplicableOND;
import com.isa.thinair.airmaster.api.model.SSRCategory;
import com.isa.thinair.airmaster.api.model.SSRConstraints;
import com.isa.thinair.airmaster.api.model.SSRSubCategory;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.SalesChannel;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.dto.SSRInfoDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.util.LookupUtils;

/**
 * @author M.Rikaz
 * 
 */
public class SSRInfoRH {

	private static final String SSR_SUB_CATEGORY_LIST = "ssrSubCategory";

	public static Page searchSSRInfo(int pageNo, SSRSearchDTO ssrSearchDTO) throws ModuleException {
		Page page = null;

		page = ModuleServiceLocator.getSsrServiceBD().getSSRs(ssrSearchDTO, (pageNo - 1) * 20, 20);

		return page;
	}

	public static void saveSSRInfo(SSRInfoDTO ssrInfoDTO) throws ModuleException {
		SSR ssrInfoObj = new SSR();

		ssrInfoObj.setVersion(ssrInfoDTO.getVersion());
		ssrInfoObj.setCreatedBy(ssrInfoDTO.getCreatedBy());
		ssrInfoObj.setCreatedDate(ssrInfoDTO.getCreatedDate());

		Set visibleChannels = new HashSet();

		if (ssrInfoDTO.isShownInIBE()) {
			ssrInfoObj.setShowInIBE("Y");
			visibleChannels.add(SalesChannel.WEB);
			visibleChannels.add(SalesChannel.IOS);
			visibleChannels.add(SalesChannel.ANDROID);
		} else {
			ssrInfoObj.setShowInIBE("N");
		}

		if (ssrInfoDTO.isShownInXBE()) {
			ssrInfoObj.setShowInXBE("Y");
			visibleChannels.add(SalesChannel.TRAVEL_AGENT);
		} else {
			ssrInfoObj.setShowInXBE("N");
		}
		
		if (ssrInfoDTO.isShownInGDS()) {
			ssrInfoObj.setShowInGDS("Y");
			visibleChannels.add(SalesChannel.GDS);
		} else {
			ssrInfoObj.setShowInGDS("N");
		}

		if (ssrInfoDTO.isShownInWs()) {
			ssrInfoObj.setShowInWs("Y");
			visibleChannels.add(SalesChannel.DNATA);
		} else {
			ssrInfoObj.setShowInWs("N");
		}

		ssrInfoObj.setVisibleChannelIds(visibleChannels);

		if (ssrInfoDTO.getSSRId() != null && ssrInfoDTO.getSSRId() > 0) {
			ssrInfoObj.setSsrId(ssrInfoDTO.getSSRId());
		}

		ssrInfoObj.setSsrCode(ssrInfoDTO.getSSRCode());
		ssrInfoObj.setSsrDesc(ssrInfoDTO.getSSRDescription());
		ssrInfoObj.setStatus(ssrInfoDTO.getStatus());
		ssrInfoObj.setSsrName(ssrInfoDTO.getSSRName());
		ssrInfoObj.setSortOrder(ssrInfoDTO.getSortOrder());
		ssrInfoObj.setSsrCutoffTime(ssrInfoDTO.getSsrCutoffTime());
		ssrInfoObj.setValidForPALCAL(ssrInfoDTO.isValidForPALCAL());
		ssrInfoObj.setUserDefinedSSR(ssrInfoDTO.isUserDefinedSSR());
		if (ssrInfoDTO.isUserDefinedSSR()){
			ssrInfoObj.setShowInWs("Y");
			ssrInfoObj.setShowInGDS("N");
			ssrInfoObj.setShowInXBE("N");
			ssrInfoObj.setShowInIBE("N");
		}

		SSRSubCategory ssrSubCategory = ModuleServiceLocator.getSsrServiceBD()
				.getSSRSubCategory(ssrInfoDTO.getSSRSubCategoryId());

		ssrInfoObj.setSsrSubCategory(ssrSubCategory);

		SSRConstraints ssrConstObj = null;

		if ((ssrInfoDTO.getAppConsId() != null) && (!ssrInfoDTO.getAppConsId().trim().equals(""))) {
			ssrConstObj = ModuleServiceLocator.getSsrServiceBD().getSSRConstraint(new Integer(ssrInfoDTO.getAppConsId()));
		} else {
			ssrConstObj = new SSRConstraints();
		}

		ssrConstObj.setApplyAdult("N");
		ssrConstObj.setApplyChild("N");
		ssrConstObj.setApplyInfant("N");

		if (ssrSubCategory.getCategory().getCatId().intValue() == SSRCategory.ID_INFLIGHT_SERVICE.intValue()) {
			ssrConstObj.setApplySegment("Y");
			ssrConstObj.setApplyDeparture("N");
			ssrConstObj.setApplyArrival("N");
			ssrConstObj.setApplyTransit("N");
		} else if (ssrSubCategory.getCategory().getCatId().intValue() == SSRCategory.ID_AIRPORT_SERVICE.intValue()) {
			ssrConstObj.setApplySegment("N");
			ssrConstObj.setApplyDeparture(ssrInfoDTO.getApplDeparture());
			ssrConstObj.setApplyArrival(ssrInfoDTO.getApplArrival());
			ssrConstObj.setApplyTransit(ssrInfoDTO.getApplTransit());
		}else if(ssrSubCategory.getCategory().getCatId().intValue() == SSRCategory.ID_AIRPORT_TRANSFER.intValue()) {
			ssrConstObj.setApplySegment("N");
			ssrConstObj.setApplyDeparture(ssrInfoDTO.getApplDeparture());
			ssrConstObj.setApplyArrival(ssrInfoDTO.getApplArrival());
			ssrConstObj.setApplyTransit("N");
		}

		ssrConstObj.setMaxTransitTimeMins(-1);
		ssrConstObj.setMinTransitTimeMins(-1);

		// Set applicable airport
		String applyAirportStr = ssrInfoDTO.getAirportList();
		String airportArr[] = applyAirportStr.split(",");

		Set applicableAirportsSet = new HashSet();

		if (airportArr != null && airportArr.length > 0) {
			for (int i = 0; i < airportArr.length; i++) {
				if (airportArr[i] != null && !airportArr[i].trim().equals("")) {
					SSRApplicableAirports ssrApplicableAirports = new SSRApplicableAirports();
					ssrApplicableAirports.setAirportCode(airportArr[i].trim());
					ssrApplicableAirports.setAppConstraints(ssrConstObj);

					applicableAirportsSet.add(ssrApplicableAirports);
				}

			}
		}

		if ((ssrConstObj.getAppConsId() != null && ssrConstObj.getAppConsId() > 0)
				&& (ssrConstObj.getApplicableAirports() != null && ssrConstObj.getApplicableAirports().size() != 0)) {
			ModuleServiceLocator.getSsrServiceBD().deleteApplicableAirports(ssrConstObj.getAppConsId());
		}

		ssrConstObj.setApplicableAirports(applicableAirportsSet);

		// Set applicable OnD
		String applicableOndStr = ssrInfoDTO.getOndList();
		String[] ondArr = applicableOndStr.split(",");

		Set applicableOndSet = new HashSet();

		if (ondArr != null && ondArr.length > 0) {
			for (int i = 0; i < ondArr.length; i++) {
				if (ondArr[i] != null && !"".equals(ondArr[i].trim())) {
					SSRApplicableOND appOnd = new SSRApplicableOND();
					appOnd.setOndCode(ondArr[i].trim());
					appOnd.setAppConstraints(ssrConstObj);
					applicableOndSet.add(appOnd);
				}
			}
		}

		if ((ssrConstObj.getAppConsId() != null && ssrConstObj.getAppConsId() > 0)
				&& (ssrConstObj.getApplicableOnds() != null && ssrConstObj.getApplicableOnds().size() != 0)) {
			ModuleServiceLocator.getSsrServiceBD().deleteApplicableOnds(ssrConstObj.getAppConsId());
		}

		ssrConstObj.setApplicableOnds(applicableOndSet);

		ssrInfoObj.setSSRConstraints(ssrConstObj);

		Integer ssrId = ModuleServiceLocator.getSsrServiceBD().saveSSR(ssrInfoObj);

		ssrInfoDTO.setSSRId(ssrId);

		ModuleServiceLocator.getSsrServiceBD().saveSSRLanguageTranslation(ssrInfoDTO);
	}

	public static void deleteSSRInfo(Integer ssrId) throws ModuleException {
		SSR existingSSR = ModuleServiceLocator.getSsrServiceBD().getSSR(ssrId);
		ModuleServiceLocator.getSsrServiceBD().deleteSSR(existingSSR);
	}

	public static String generateSSRSubcategoryInfo() throws JSONException {

		List<String[]> list = LookupUtils.LookupDAO().getThreeColumnMap(SSR_SUB_CATEGORY_LIST);
		Map<Integer, String> ssrSubCatMap = new HashMap<Integer, String>();

		if (list != null) {
			for (String[] strArr : list) {
				if (strArr[2] != "") {
					Integer ssrCatId = new Integer(strArr[2]);

					String optionStr = "<option value='" + strArr[0] + "'>" + strArr[1] + "</option>";
					if (ssrSubCatMap.containsKey(ssrCatId)) {
						optionStr = optionStr.concat(ssrSubCatMap.get(ssrCatId));
					}

					ssrSubCatMap.put(ssrCatId, optionStr);
				}
			}
		}

		return JSONUtil.serialize(ssrSubCatMap);
	}

	public static boolean checkSSRAssignedInReservation(int ssrId) throws ModuleException {
		return ModuleServiceLocator.getReservationBD().checkSSRChargeAssignedInReservation(ssrId);
	}

}
