package com.isa.thinair.airadmin.core.web.v2.generator.master;

import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.commons.api.exception.ModuleException;

public class BaggageMessageHG {

	private static String clientErrors;
	private static String templateclientErrors;

	/**
	 * Create Client Validations for Baggage & Baggage Template page & OND Baggage Template p
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array of Client validations
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {

		if (clientErrors == null) {
			Properties moduleErrs = new Properties();
			moduleErrs.setProperty("um.airadmin.delete.confirmation", "deleteRecoredCfrm");
			moduleErrs.setProperty("um.baggage.form.name.required", "baggageNameRqrd");
			moduleErrs.setProperty("um.baggage.form.description.required", "descriptionRqrd");
			moduleErrs.setProperty("um.baggage.form.iatacode.required", "iataCodeRqrd");
			moduleErrs.setProperty("um.baggage.form.name.dulply", "baggageNmeExst");
			moduleErrs.setProperty("um.meal.form.amount.required", "amountRqrd");
			moduleErrs.setProperty("um.meal.form.mealcode.required", "mealCodeRqrd");
			moduleErrs.setProperty("um.meal.form.amount.invalid", "amountInvalid");
			moduleErrs.setProperty("um.baggage.form.cc.required", "ccRqrd");
			moduleErrs.setProperty("um.baggageTemplate.form.localcurrencycode.required","localCurrencyCodeRqrd");
			

			clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}

		return clientErrors;
	}

	public static String getTeplateClientErrors(HttpServletRequest request) throws ModuleException {

		if (templateclientErrors == null) {
			Properties moduleErrs = new Properties();
			moduleErrs.setProperty("um.airadmin.delete.confirmation", "deleteRecoredCfrm");
			moduleErrs.setProperty("um.baggageTemplate.form.code.required", "tempCodeRqrd");
			moduleErrs.setProperty("um.baggageTemplate.form.description.required", "descriptionRqrd");
			moduleErrs.setProperty("um.baggage.form.cc.required", "ccRqrd");
			moduleErrs.setProperty("um.baggageTemplate.form.baggage.required", "baggageRqrd");
			moduleErrs.setProperty("um.baggageTemplate.form.amount.required", "baggageAmt");
			moduleErrs.setProperty("um.baggageTemplate.form.status.required", "baggageStatus");
			moduleErrs.setProperty("um.baggageTemplate.form.baggage.exist", "baggageExst");
			moduleErrs.setProperty("um.baggageTemplate.form.def.baggage.req", "defBaggRqrd");
			moduleErrs.setProperty("um.baggageTemplate.form.def.baggage.act", "defBaggAct");
			moduleErrs.setProperty("um.ondBaggageTemplate.form.seatFactor.required", "ondBaggSeatFact");
			moduleErrs.setProperty("um.ondBaggageTemplate.form.totalweight.required", "ondBaggTotalWeight");
			moduleErrs.setProperty("um.ondBaggageTemplate.form.bagTemp.required", "ondBaggTempCodeRqrd");
			moduleErrs.setProperty("um.ondBaggageTemplate.from.to.both.exists.or.not", "ondBaggTempFromToBothExistsOrNot");
			moduleErrs.setProperty("um.ondCode.form.ondCode.required", "ondCodeRqrd");
			moduleErrs.setProperty("um.ondCode.form.ondCode.exist", "ondCodeExist");
			moduleErrs.setProperty("um.ondBaggageTemplate.form.localcurrencycode.required","localCurrencyCodeRqrd");
			moduleErrs.setProperty("um.baggageTemplate.form.fromdate.required", "fromDateRqrd");
			moduleErrs.setProperty("um.baggageTemplate.form.todate.required", "toDateRqrd");

			
			moduleErrs.setProperty("um.charges.depature.required", "depatureRqrd");
			moduleErrs.setProperty("um.charges.departure.inactive", "departureInactive");
			moduleErrs.setProperty("um.charges.arrival.required", "arrivalRqrd");
			moduleErrs.setProperty("um.charges.arrival.inactive", "arrivalInactive");
			moduleErrs.setProperty("um.charges.arrivalDepature.same", "depatureArriavlSame");
			moduleErrs.setProperty("um.charges.via.required", "viaPointRqrd");
			moduleErrs.setProperty("um.charges.arrivalVia.same", "arriavlViaSame");
			moduleErrs.setProperty("um.charges.depatureVia.same", "depatureViaSame");
			moduleErrs.setProperty("um.charges.via.sequence", "vianotinSequence");
			moduleErrs.setProperty("um.charges.via.equal", "viaEqual");
			moduleErrs.setProperty("um.charges.via.inactine", "viaInactive");
			moduleErrs.setProperty("um.charges.ond.required", "ondRequired");
			
			moduleErrs.setProperty("um.flight.fltDate.invalidDate", "invalidDate");
			moduleErrs.setProperty("um.common.to.date.greater.than.from.date", "toDateGreaterThanFromDate");
			moduleErrs.setProperty("um.common.from.lessthan.current.date", "fromDateLessThanCurrent");
			moduleErrs.setProperty("um.ondBaggageTemplate.form.seatFactorAppliedFor.required", "seatFactorAppliedForReq");
			templateclientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}

		return templateclientErrors;
	}
}
