package com.isa.thinair.airadmin.core.web.v2.action.tools;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airreservation.api.dto.pnrgov.PNRGOVReportDTO;
import com.isa.thinair.airreservation.api.model.PNRGOVTxHistoryLog;
import com.isa.thinair.commons.api.dto.ModuleCriteria;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ShowPnrGovReportAction extends BaseRequestAwareAction {
	private static final String PARAM_MODE = "hdnMode";
	private static final String PARAM_LOGID = "pnrGovTxHistoryID";

	private Object[] rows;

	private int page;

	private int total;

	private int records;

	private String hdnMode;

	private String txtMsg = "";

	private boolean success;

	private PNRGOVReportDTO pnrGovReportSrch;

	private static Log log = LogFactory.getLog(ShowBSPReportAction.class);

	public String execute() {
		String strHdnMode = request.getParameter(PARAM_MODE);
		String forward = WebConstants.FORWARD_SUCCESS;

		try {
			if (strHdnMode.equals(WebConstants.ACTION_RESEND_PNRGOV)) {
				// Save the updated status field of the BSP Report.
				resendMessage(request);
				success = true;
			}
		} catch (Exception exception) {
			success = false;
			txtMsg = "Resending PNRGOV message Failed. Please try again later... ";
			log.error("Exception in ShowPnrGovReportAction:execute()", exception);
			if (exception instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
			}
		}

		return forward;
	}

	private static void resendMessage(HttpServletRequest request) throws ModuleException {
		String logID = request.getParameter(PARAM_LOGID);

		PNRGOVTxHistoryLog historyLog = (PNRGOVTxHistoryLog) ModuleServiceLocator.getReservationAuxilliaryBD().getHistoryLog(
				logID);

		if (historyLog != null) {
			ModuleServiceLocator.getReservationBD().publishPNRGOV(historyLog.getFltSegID(), historyLog.getCountryCode(),
					historyLog.getAirportCode(), historyLog.getTimePeriod(), historyLog.getInboundOutbound());
		} else {
			throw new ModuleException("PNRGOV History Log Doesn't Exists");
		}
	}

	public String searchPNRGOVReport() {
		List<ModuleCriterion> critrian = new ArrayList<ModuleCriterion>();
		if (pnrGovReportSrch != null) {
			if (pnrGovReportSrch.getCountryName() != null && !pnrGovReportSrch.getCountryName().trim().equals("")) {
				ModuleCriteria criteriaPNRGOVCountryName = new ModuleCriteria();
				List<String> lstcountryName = new ArrayList<String>();
				criteriaPNRGOVCountryName.setFieldName("countryCode");
				criteriaPNRGOVCountryName.setCondition(ModuleCriteria.CONDITION_EQUALS);
				lstcountryName.add(pnrGovReportSrch.getCountryName());
				criteriaPNRGOVCountryName.setValue(lstcountryName);
				critrian.add(criteriaPNRGOVCountryName);
			}
			if (pnrGovReportSrch.getFltSegID() != null && !pnrGovReportSrch.getFltSegID().trim().equals("")) {
				ModuleCriteria criteriaPNRGOVFltSegId = new ModuleCriteria();
				List<String> lstagentCode = new ArrayList<String>();
				criteriaPNRGOVFltSegId.setFieldName("fltSegID");
				criteriaPNRGOVFltSegId.setCondition(ModuleCriteria.CONDITION_EQUALS);
				lstagentCode.add(pnrGovReportSrch.getFltSegID());
				criteriaPNRGOVFltSegId.setValue(lstagentCode);
				critrian.add(criteriaPNRGOVFltSegId);
			}
			if (pnrGovReportSrch.getStatus() != null && !pnrGovReportSrch.getStatus().trim().equals("")) {
				ModuleCriteria criteriaPNRGOVStatus = new ModuleCriteria();
				List<String> lststatus = new ArrayList<String>();
				criteriaPNRGOVStatus.setFieldName("status");
				criteriaPNRGOVStatus.setCondition(ModuleCriteria.CONDITION_EQUALS);
				lststatus.add(pnrGovReportSrch.getStatus());
				criteriaPNRGOVStatus.setValue(lststatus);
				critrian.add(criteriaPNRGOVStatus);
			}
			if (pnrGovReportSrch.getFrmDate() != null && !pnrGovReportSrch.getFrmDate().trim().equals("")) {
				ModuleCriteria criteriaPNRGOVFrmDate = new ModuleCriteria();
				List<Date> lstfrmDate = new ArrayList<Date>();
				criteriaPNRGOVFrmDate.setFieldName("transmissionTimeStamp");
				criteriaPNRGOVFrmDate.setCondition(ModuleCriteria.CONDITION_GREATER_THAN_OR_EQUALS);
				Date bspFromDate = stringToDate(pnrGovReportSrch.getFrmDate());
				lstfrmDate.add(bspFromDate);
				criteriaPNRGOVFrmDate.setValue(lstfrmDate);
				critrian.add(criteriaPNRGOVFrmDate);
			}
			if (pnrGovReportSrch.getToDate() != null && !pnrGovReportSrch.getToDate().trim().equals("")) {
				ModuleCriteria criterianPNRGOVToDate = new ModuleCriteria();
				List<Date> lsttoDate = new ArrayList<Date>();
				criterianPNRGOVToDate.setFieldName("transmissionTimeStamp");
				criterianPNRGOVToDate.setCondition(ModuleCriteria.CONDITION_LESS_THAN);
				Date bsptoDate = stringToDate(pnrGovReportSrch.getToDate());
				lsttoDate.add(bsptoDate);
				criterianPNRGOVToDate.setValue(lsttoDate);
				critrian.add(criterianPNRGOVToDate);
			}
		}

		Page pgPNRGOVReport = searchPNRGOVReport(this.page, critrian);
		this.records = pgPNRGOVReport.getTotalNoOfRecords();
		this.total = pgPNRGOVReport.getTotalNoOfRecords() / 20;
		int mod = pgPNRGOVReport.getTotalNoOfRecords() % 20;
		if (mod > 0)
			this.total = this.total + 1;
		if (this.page <= 0)
			this.page = 1;

		Collection<PNRGOVTxHistoryLog> colBSP = pgPNRGOVReport.getPageData();

		if (colBSP != null) {
			Object[] dataRow = new Object[colBSP.size()];
			int index = 1;
			Iterator<PNRGOVTxHistoryLog> iter = colBSP.iterator();
			while (iter.hasNext()) {
				Map<String, Object> counmap = new HashMap<String, Object>();
				PNRGOVTxHistoryLog grdPNRGOVReport = (PNRGOVTxHistoryLog) iter.next();

				counmap.put("id", new Integer(((page - 1) * 20) + index).toString());
				counmap.put("pnrgovreport", grdPNRGOVReport);
				dataRow[index - 1] = counmap;
				index++;
			}

			this.rows = dataRow;
		}
		return S2Constants.Result.SUCCESS;
	}

	public static Page searchPNRGOVReport(int pageNo, List<ModuleCriterion> criterion) {
		Page tmpPage = null;

		try {
			if (criterion == null) {
				tmpPage = ModuleServiceLocator.getReservationAuxilliaryBD().getPNRGOVReports((pageNo - 1) * 20, 20);

			} else {
				if (pageNo == 0) {
					pageNo = 1;
				}

				tmpPage = ModuleServiceLocator.getReservationAuxilliaryBD().getPNRGOVReports((pageNo - 1) * 20, 20, criterion);

			}
		} catch (ModuleException e) {
			log.error(e);
		}
		return tmpPage;
	}

	public Date stringToDate(String strDate) {
		Date date = null;
		if (strDate != null && !"".equals(strDate)) {
			String[] arrDate = strDate.split("/");
			Calendar cal = new GregorianCalendar();
			cal.set(new Integer(arrDate[2]), new Integer(arrDate[1]) - 1, new Integer(arrDate[0]));
			date = new Date(cal.getTimeInMillis());
		}
		return date;
	}

	public void setHdnMode(String hdnMode) {
		this.hdnMode = hdnMode;
	}

	public String getHdnMode() {
		return hdnMode;
	}

	public void setTxtMsg(String txtMsg) {
		this.txtMsg = txtMsg;
	}

	public String getTxtMsg() {
		return txtMsg;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public boolean isSuccess() {
		return success;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public Object[] getRows() {
		return rows;
	}

	public void setRows(Object[] rows) {
		this.rows = rows;
	}

	public PNRGOVReportDTO getpnrGovReportSrch() {
		return pnrGovReportSrch;
	}

	public void setpnrGovReportSrch(PNRGOVReportDTO pnrGovReportSrch) {
		this.pnrGovReportSrch = pnrGovReportSrch;
	}

}
