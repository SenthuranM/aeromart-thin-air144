package com.isa.thinair.airadmin.core.web.v2.generator.tools;

import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.commons.api.exception.ModuleException;

public class BlacklistPAXMessageHG {
	
	private static String templateclientErrors;

	public static String getTemplateClientErrors(HttpServletRequest request) throws ModuleException {

		if (templateclientErrors == null) {
			Properties moduleErrs = new Properties();

			moduleErrs.setProperty("um.blacklist.pax.mgmnt.first.name.required", "firstNameRequired");
			moduleErrs.setProperty("um.blacklist.pax.mgmnt.last.name.required", "lastNameRequired");
			moduleErrs.setProperty("um.blacklist.pax.mgmnt.passport.no.required", "passPortNoRequired");
			moduleErrs.setProperty("um.blacklist.pax.mgmnt.nationality.required", "nationalityRequired");
			moduleErrs.setProperty("um.blacklist.pax.mgmnt.blacklist.type.required", "blacklistTypeRequired");
			moduleErrs.setProperty("um.blacklist.pax.popup.valid.until.required", "validUntilRequired");
			moduleErrs.setProperty("um.blacklist.pax.popup.remove.reason.required", "remReasonRequired");


			templateclientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}
		return templateclientErrors;
	}

}
