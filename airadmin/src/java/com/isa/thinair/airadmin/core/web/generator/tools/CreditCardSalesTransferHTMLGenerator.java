/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */

/**
 * @author srikantha
 * 
 * Window - Preferences - Java - Code Style - Code Templates
 */

package com.isa.thinair.airadmin.core.web.generator.tools;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airreservation.api.dto.CCSalesHistoryDTO;
import com.isa.thinair.airreservation.api.dto.CreditCardInformationDTO;
import com.isa.thinair.airreservation.api.dto.CreditCardSalesStatusDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

public class CreditCardSalesTransferHTMLGenerator {

	private static String clientErrors;

	// searching parameters
	private static final String PARAM_SEARCH_FROM = "txtFrom";
	private static final String PARAM_SEARCH_TO = "txtTo";
	private static final String PARAM_CARD_TYPES = "hdnCardTypes";
	private static final String PARAM_UI_MODE = "hdnUIMode";
	private String strFormFieldsVariablesJS = "";

	public final String getCreditCardSalesTransferRowHtml(HttpServletRequest request) throws ModuleException, Exception {

		String strUIMode = request.getParameter(PARAM_UI_MODE);
		String strUIModeJS = "var isSearchMode = false;";
		String strSelectedCardTypes = "";
		String strFromDate = "";
		String strToDate = "";
		Date fromDate = null;
		Date toDate = null;
		List<CCSalesHistoryDTO> list = null;
		String strTotalCredit = "";
		CreditCardSalesStatusDTO ccDalesDto = null;

		if (strUIMode != null && strUIMode.equals(WebConstants.ACTION_SEARCH)) {

			strUIModeJS = "var isSearchMode = true;";

			strFromDate = request.getParameter(PARAM_SEARCH_FROM);
			strFormFieldsVariablesJS += "var fromDate ='" + strFromDate + "';";

			strToDate = request.getParameter(PARAM_SEARCH_TO);
			strFormFieldsVariablesJS += "var toDate ='" + strToDate + "';";

			strSelectedCardTypes = request.getParameter(PARAM_CARD_TYPES);
			strFormFieldsVariablesJS += "var selectedCardTypes ='" + strSelectedCardTypes + "';";

			setFormFieldValues(strFormFieldsVariablesJS);
			String strAssignCardTypeValues = request.getParameter(PARAM_CARD_TYPES);

			SimpleDateFormat dateFormat = null;

			if (!strFromDate.equals("")) {
				if (strFromDate.indexOf('-') != -1) {
					dateFormat = new SimpleDateFormat("dd-MM-yy");
				}
				if (strFromDate.indexOf('/') != -1) {
					dateFormat = new SimpleDateFormat("dd/MM/yy");
				}
				if (strFromDate.indexOf(' ') != -1) {
					strFromDate = strFromDate.substring(0, strFromDate.indexOf(' '));
				}

				fromDate = dateFormat.parse(strFromDate);
			}
			if (!strToDate.equals("")) {
				if (strToDate.indexOf('-') != -1) {
					dateFormat = new SimpleDateFormat("dd-MM-yy");
				}
				if (strFromDate.indexOf('/') != -1) {
					dateFormat = new SimpleDateFormat("dd/MM/yy");
				}
				if (strFromDate.indexOf(' ') != -1) {
					strToDate = strToDate.substring(0, strToDate.indexOf(' '));
				}
				// StrstrStartDate + strStartTime
				toDate = dateFormat.parse(strToDate);
			}
			Collection<CreditCardInformationDTO> colCardTypes = ModuleServiceLocator.getReservationAuxilliaryBD().getActiveCards();
			ArrayList<Integer> setAssignCardTypes = new ArrayList<Integer>();
			Integer arrCardTypes[] = null;
			if (strAssignCardTypeValues != null && strAssignCardTypeValues.length() > 0) {
				CreditCardInformationDTO cardType = null;

				Iterator<CreditCardInformationDTO> typeIter = colCardTypes.iterator();
				while (typeIter.hasNext()) {
					cardType = (CreditCardInformationDTO) typeIter.next();
					StringTokenizer strTok = new StringTokenizer(strAssignCardTypeValues, ",");
					while (strTok.hasMoreTokens()) {
						if (cardType.getId() == Integer.parseInt(strTok.nextToken())) {
							setAssignCardTypes.add(new Integer(cardType.getId()));
							break;
						}
					}
					arrCardTypes = new Integer[setAssignCardTypes.size()];
					for (int i = 0; i < setAssignCardTypes.size(); i++) {
						Integer tmp = (Integer) setAssignCardTypes.get(i);
						arrCardTypes[i] = new Integer(tmp.intValue());
					}
				}
			}
			request.setAttribute(WebConstants.REQ_OPERATION_UI_MODE, strUIModeJS);

			ccDalesDto = ModuleServiceLocator.getReservationAuxilliaryBD().getCreditCardSalesDetails(arrCardTypes, fromDate,
					toDate);
			if (ccDalesDto != null) {
				list = ccDalesDto.getCreditSales();
				strTotalCredit = ccDalesDto.getAmountSatus();
			}
			request.getSession().setAttribute("historylist", list);
			if (strTotalCredit != null) {
				request.setAttribute("total_credit", strTotalCredit);
			}

		} else {

			strUIModeJS = "var isSearchMode = false;";
			strFormFieldsVariablesJS += "var fromDate ='';";
			strFormFieldsVariablesJS += "var toDate ='';";
			strFormFieldsVariablesJS += "var selectedCardTypes ='';";

		}
		setFormFieldValues(strFormFieldsVariablesJS);
		return createCreditCardSalesTransferRowHTML(list);
	}

	private String createCreditCardSalesTransferRowHTML(Collection<CCSalesHistoryDTO> listCreditCardSalesTransfers) {

		List<CCSalesHistoryDTO> list = (List<CCSalesHistoryDTO>) listCreditCardSalesTransfers;

		Object[] listArr = list.toArray();
		StringBuffer sb = new StringBuffer("var arrCreditData = new Array();");

		CCSalesHistoryDTO cardSalesHistory = null;

		for (int i = 0; i < listArr.length; i++) {
			cardSalesHistory = (CCSalesHistoryDTO) listArr[i];

			sb.append("arrCreditData[" + i + "] = new Array();");

			if (cardSalesHistory.getDateOfsale() != null) {
				sb.append("arrCreditData[" + i + "][1] = '" + changeDateFormat(cardSalesHistory.getDateOfsale()) + "';");
			} else {
				sb.append("arrCreditData[" + i + "][1] = '';");
			}
			if (cardSalesHistory.getCardType() != null) {
				sb.append("arrCreditData[" + i + "][2] = '" + cardSalesHistory.getCardType() + "';");
			} else {
				sb.append("arrCreditData[" + i + "][2] = 'All Card Types';");
			}

			if (cardSalesHistory.getTotalDailySales().doubleValue() != 0) {
				sb.append("arrCreditData[" + i + "][3] = '" + cardSalesHistory.getTotalDailySales() + "';");
			} else {
				sb.append("arrCreditData[" + i + "][3] = '0.00';");
			}

			if (cardSalesHistory.getTransferTimeStamp() != null) {

				sb.append("arrCreditData[" + i + "][4] = '"
						+ changeTimestampFormat(new Date(cardSalesHistory.getTransferTimeStamp().getTime())) + "';");
			} else {
				sb.append("arrCreditData[" + i + "][4] = '';");
			}

			if (cardSalesHistory.getTransferStatus() != null) {
				if (cardSalesHistory.getTransferStatus().equals("N"))
					sb.append("arrCreditData[" + i + "][5] = 'Not Transferred';");
				else if (cardSalesHistory.getTransferStatus().equals("Y"))
					sb.append("arrCreditData[" + i + "][5] = 'Transferred';");
				else
					sb.append("arrCreditData[" + i + "][5] = 'Not Generated';");
			} else {
				sb.append("arrCreditData[" + i + "][5] = '';");
			}

			if (cardSalesHistory.getTransferStatus() != null) {
				if (cardSalesHistory.getTransferStatus().equals("N"))
					sb.append("arrCreditData[" + i + "][6] = 'T';"); // T-Transfer, G-Generate
				else if (cardSalesHistory.getTransferStatus().equals("Y"))
					sb.append("arrCreditData[" + i + "][6] = 'T';");
				else
					sb.append("arrCreditData[" + i + "][6] = 'G';");
			} else {
				sb.append("arrCreditData[" + i + "][6] = '';");
			}

			if (cardSalesHistory.getCardTypeId() != 0) {
				sb.append("arrCreditData[" + i + "][7] = '" + cardSalesHistory.getCardTypeId() + "';");
			} else {
				sb.append("arrCreditData[" + i + "][7] = '';");
			}

			sb.append("arrCreditData[" + i + "][8] = 'false';");

			if (cardSalesHistory.getTransferStatus() != null) {
				sb.append("arrCreditData[" + i + "][9] = '" + cardSalesHistory.getTransferStatus() + "';");
			} else {
				sb.append("arrCreditData[" + i + "][9] = '';");
			}

		}
		return sb.toString();
	}

	public static String getClientErrors(HttpServletRequest request) throws ModuleException {

		if (clientErrors == null) {
			Properties moduleErrs = new Properties();

			moduleErrs.setProperty("um.airadmin.generate.credit.confirmation", "generateRecoredCfrm");
			moduleErrs.setProperty("um.airadmin.transfer.credit.confirmation", "transferRecoredCfrm");
			moduleErrs.setProperty("um.airadmin.retransfer.credit.confirmation", "reTransferRecoredCfrm");

			moduleErrs.setProperty("um.airadmin.see.log", "seeLogEntry");

			moduleErrs.setProperty("um.creditsales.form.from.date.required", "fromDateRqrd");
			moduleErrs.setProperty("um.creditsales.form.to.date.required", "toDateRqrd");
			moduleErrs.setProperty("um.creditsales.form.from.date.invalid", "fromDateInvalid");
			moduleErrs.setProperty("um.creditsales.form.to.date.invalid", "toDateInvalid");
			moduleErrs.setProperty("um.creditsales.form.cardType.required", "cardTypeRqrd");
			moduleErrs.setProperty("um.creditsales.form.todate.lessthan.fromdate", "todateLessthanFromDate");
			moduleErrs.setProperty("um.creditsales.form.todate.lessthan.currentdate", "todateLessthanCurrentDate");

			clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}

		return clientErrors;
	}

	private void setFormFieldValues(String strFormFieldsVariablesJS) {
		this.strFormFieldsVariablesJS = strFormFieldsVariablesJS;
	}

	public String getFormFieldValues() {
		return strFormFieldsVariablesJS;
	}

	private String changeDateFormat(Date date) {
		SimpleDateFormat dtfmt = new SimpleDateFormat("dd/MM/yyyy");
		return dtfmt.format(date);
	}

	private String changeTimestampFormat(Date date) {
		SimpleDateFormat dtfmt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		return dtfmt.format(date);
	}

}
