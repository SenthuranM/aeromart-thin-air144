package com.isa.thinair.airadmin.core.web.handler.master;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.master.AirportDSTHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airmaster.api.model.Airport;
import com.isa.thinair.airmaster.api.model.AirportDST;
import com.isa.thinair.airmaster.api.to.OperationStatusTO;
import com.isa.thinair.airschedules.api.dto.FlightAlertDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.Util;

/**
 * @author srikantha
 * 
 */

public final class AirportDSTRequestHandler extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(AirportDSTRequestHandler.class);

	private static AiradminConfig airadminConfig = new AiradminConfig();

	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	private static final String PARAM_VERSION = "hdnVersion";
	private static final String PARAM_GRIDROW = "hdnGridRow";
	private static final String PARAM_MODE = "hdnMode";
	private static final String PARAM_AIRPORTCODE = "txtAirportCode";

	private static final String PARAM_STARTDATE = "txtStart";
	private static final String PARAM_ENDDATE = "txtEnd";
	private static final String PARAM_STARTTIME = "txtStartTime";
	private static final String PARAM_ENDTIME = "txtEndTime";
	private static final String PARAM_OPERATOR = "selOperator";
	private static final String PARAM_ADJUSTTIME = "txtAdjTime";
	private static final String PARAM_CHKALERT = "chkAlerts";
	private static final String PARAM_CHKSTATUS = "chkStatus";
	private static final String PARAM_CHKEMAIL = "chkEmails";
	private static final String PARAM_EMAIL = "txtEmailID";
	private static final String PARAM_CHKEMAIL_PAX = "chkEmailPax";
	private static final String PARAM_CHKSMS_PAX = "chkSmsPax";
	private static final String PARAM_CHKSMS_CNF = "chkSmsCNF";
	private static final String PARAM_CHKSMS_OHD = "chkSmsOnhold";

	private static final String PARAM_DST_ID = "hdnDSTId";

	// int 0-Not Applicable, 1-Success, 2-Fail
	// 3-Sent Alerts 4-Sent Emails 5-Sent Alerts & Emails

	private static void setIntSuccess(HttpServletRequest request, int value) {
		setAttribInRequest(request, "dstIntSuccess", new Integer(value));
	}

	private static int getIntSuccess(HttpServletRequest request) {
		Integer returnValue = (Integer) getAttribInRequest(request, "dstIntSuccess");
		if (returnValue != null)
			return returnValue.intValue();
		else
			return -1;
	}

	private static void setExceptionOccured(HttpServletRequest request, boolean value) {
		setAttribInRequest(request, "isExceptionOccuredDST", new Boolean(value));
	}

	private static boolean isExceptionOccured(HttpServletRequest request) {
		return ((Boolean) getAttribInRequest(request, "isExceptionOccuredDST")).booleanValue();
	}

	/**
	 * Execute Method for DST Action & Sets Succces Int int 0-Not Applicable, 1-Success, 2-Fail 3-Sent Alerts 4-Sent
	 * Emails 5-Sent Alerts & Emails
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {

		String strHdnMode = request.getParameter(PARAM_MODE);
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;

		setIntSuccess(request, 0);
		String strFormData = "var saveSuccess = " + getIntSuccess(request) + ";";
		strFormData += setAlertStatus();
		strFormData += " var arrFormData = new Array();";

		request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);

		setIntSuccess(request, 0);
		setExceptionOccured(request, false);
		setAttribInRequest(request, "strModeJSDST", "var isAddMode = false;");

		try {
			if (strHdnMode != null) {
				if (strHdnMode.equals(WebConstants.ACTION_ADD)) {

					setAttribInRequest(request, "strModeJSDST", "var isAddMode = true;");
					checkPrivilege(request, WebConstants.PRIV_SYS_MAS_AIRPORT_DST_ADD);
					saveData(request);
				}
				if (strHdnMode.equals(WebConstants.ACTION_EDIT)) {

					setAttribInRequest(request, "strModeJSDST", "var isAddMode = false;");
					checkPrivilege(request, WebConstants.PRIV_SYS_MAS_AIRPORT_DST_EDIT);
					saveData(request);

				}
				if (strHdnMode.equals(WebConstants.ACTION_DISABLE_ROLLBACK)) {
					checkPrivilege(request, WebConstants.PRIV_SYS_MAS_AIRPORT_DST_ROLLBACK);
					saveData(request);

				}
			}
			request.setAttribute(WebConstants.REQ_OPERATION_MODE, getAttribInRequest(request, "strModeJSDST"));

			log.debug("AirportDSTRequestHandler.setDisplayData() method is successfully executed.");

		} catch (Exception exception) {
			log.error("Exception in AirportDSTRequestHandler:execute()", exception);
			if (exception instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
			}
		}
		setDisplayData(request);
		return forward;
	}

	/**
	 * Saves the DST Data for an Airport
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return boolean true-success false -not
	 * @throws Exception
	 *             the Exception
	 */
	private static boolean saveData(HttpServletRequest request) throws Exception {

		log.debug("Inside the AirportDSTRequestHandler.saveData() method...");

		AirportDST airportDst = new AirportDST();

		String strVersion = null;
		String strHdnMode = null;
		String strAirportCode = null;
		String strStartDate = null;
		String strEndDate = null;
		String strStartTime = null;
		String strEndTime = null;
		String strAdjustTime = null;
		String strOperator = null;
		String strEmailId = null;
		String strEmail = null;
		String strAlert = null;
		String strStatus = null;
		String strDSTId = null;
		String strGridRow = null;
		String strEmailPax = null;
		String strSmsPax = null;
		String strSmsCNF = null;
		String strSmsOHD = null;

		setExceptionOccured(request, false);

		try {
			strDSTId = request.getParameter(PARAM_DST_ID);
			strVersion = AiradminUtils.getNotNullString(request.getParameter(PARAM_VERSION));
			strGridRow = AiradminUtils.getNotNullString(request.getParameter(PARAM_GRIDROW));
			strHdnMode = AiradminUtils.getNotNullString(request.getParameter(PARAM_MODE));
			strAirportCode = AiradminUtils.getNotNullString(request.getParameter(PARAM_AIRPORTCODE));
			strStartDate = AiradminUtils.getNotNullString(request.getParameter(PARAM_STARTDATE));
			strEndDate = AiradminUtils.getNotNullString(request.getParameter(PARAM_ENDDATE));
			strStartTime = AiradminUtils.getNotNullString(request.getParameter(PARAM_STARTTIME));
			strEndTime = AiradminUtils.getNotNullString(request.getParameter(PARAM_ENDTIME));
			strAdjustTime = AiradminUtils.getNotNullString(request.getParameter(PARAM_ADJUSTTIME));
			strOperator = AiradminUtils.getNotNullString(request.getParameter(PARAM_OPERATOR));
			strEmailId = AiradminUtils.getNotNullString(request.getParameter(PARAM_EMAIL));
			strEmail = AiradminUtils.getNotNullString(request.getParameter(PARAM_CHKEMAIL));
			strAlert = AiradminUtils.getNotNullString(request.getParameter(PARAM_CHKALERT));
			strStatus = AiradminUtils.getNotNullString(request.getParameter(PARAM_CHKSTATUS));
			strEmailPax = AiradminUtils.getNotNullString(request.getParameter(PARAM_CHKEMAIL_PAX));
			strSmsPax = AiradminUtils.getNotNullString(request.getParameter(PARAM_CHKSMS_PAX));
			strSmsCNF = AiradminUtils.getNotNullString(request.getParameter(PARAM_CHKSMS_CNF));
			strSmsOHD = AiradminUtils.getNotNullString(request.getParameter(PARAM_CHKSMS_OHD));

			if (!"".equals(strVersion)) {
				airportDst.setVersion(Long.parseLong(strVersion));
			}
			if (strHdnMode.equals(WebConstants.ACTION_EDIT) || strHdnMode.equals(WebConstants.ACTION_DISABLE_ROLLBACK)) {
				airportDst.setDstCode(Integer.parseInt(strDSTId));
				airportDst.setModifiedDate(new Date());
			}

			SimpleDateFormat dateFormat = null;

			if (!strStartDate.equals("")) {
				if (strStartDate.indexOf('-') != -1) {
					dateFormat = new SimpleDateFormat("dd-MM-yy HH:mm");
				}
				if (strStartDate.indexOf('/') != -1) {
					dateFormat = new SimpleDateFormat("dd/MM/yy HH:mm");
				}
				if (strStartDate.indexOf(' ') != -1) {
					strStartDate = strStartDate.substring(0, strStartDate.indexOf(' '));
				}

				Date startDate = dateFormat.parse(strStartDate + " " + strStartTime);
				airportDst.setDstStartDateTime(startDate);
			}

			if (!strStartDate.equals("") && (!strStartTime.equals(""))) {
				if (strEndDate.indexOf('-') != -1) {
					dateFormat = new SimpleDateFormat("dd-MM-yy HH:mm");
				}
				if (strEndDate.indexOf('/') != -1) {
					dateFormat = new SimpleDateFormat("dd/MM/yy HH:mm");
				}
				if (strEndDate.indexOf(' ') != -1) {
					strEndDate = strEndDate.substring(0, strEndDate.indexOf(' '));
				}
				Date endDate = dateFormat.parse(strEndDate + " " + strEndTime);
				airportDst.setDstEndDateTime(endDate);
			}

			airportDst.setAirportCode(strAirportCode);

			// Storing MinStopoverTime as minutes. Should be lass than 30 minutes
			String[] strTimes = strAdjustTime.split(":");

			int adjustTimeinMins = Integer.parseInt(strTimes[0]) * 60 + Integer.parseInt(strTimes[1]);

			if (strOperator != null && strOperator.equals("-")) {
				airportDst.setDstAdjustTime(Integer.parseInt(strOperator + adjustTimeinMins));
			} else {
				airportDst.setDstAdjustTime(adjustTimeinMins);
			}

			log.debug("Inside the AirportDSTRequestHandler.saveData()- setters are successfully called...");

			FlightAlertDTO alertDTO = new FlightAlertDTO();
			if (strAlert.equals(WebConstants.CHECKBOX_ON)) {
				alertDTO.setAlertForRescheduledFlight(true);
			} else {
				alertDTO.setAlertForRescheduledFlight(false);
			}
			if (strEmail.equals(WebConstants.CHECKBOX_ON)) {
				alertDTO.setEmailForRescheduledFlight(true);
			} else {
				alertDTO.setEmailForRescheduledFlight(false);
			}
			alertDTO.setEmailTo(strEmailId);

			if (strEmailPax.equals(WebConstants.CHECKBOX_ON)) {
				alertDTO.setSendEmailsForRescheduledFlight(true);
			} else {
				alertDTO.setSendEmailsForRescheduledFlight(false);
			}

			if (strSmsPax.equals(WebConstants.CHECKBOX_ON)) {
				alertDTO.setSendSMSForRescheduledFlight(true);
				if (strSmsCNF.equals(WebConstants.CHECKBOX_ON)) {
					alertDTO.setSendSMSForConfirmedBookings(true);
				} else {
					alertDTO.setSendSMSForConfirmedBookings(false);
				}

				if (strSmsOHD.equals(WebConstants.CHECKBOX_ON)) {
					alertDTO.setSendSMSForOnHoldBookings(true);
				} else {
					alertDTO.setSendSMSForOnHoldBookings(false);
				}
			} else {
				alertDTO.setSendSMSForRescheduledFlight(false);
				alertDTO.setSendSMSForConfirmedBookings(false);
				alertDTO.setSendSMSForOnHoldBookings(false);
			}

			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_DISABLE_ROLLBACK)) {
				OperationStatusTO statusTO = ModuleServiceLocator.getAirportServiceBD().disableAndRollBack(airportDst, alertDTO);

				if (statusTO != null) {

					// if alert and email sent --construct the message
					if (statusTO.isAlertSent() && statusTO.isEmailSent()) {
						setIntSuccess(request, 5);

					} else {
						// if alerts are only sent - construct the message
						if (statusTO.isAlertSent())
							setIntSuccess(request, 3);

						// if email is only sent - construct the message
						if (statusTO.isEmailSent())
							setIntSuccess(request, 4);

						// only flights have been updated with no alerts or email
						if (!(statusTO.isAlertSent()) && !(statusTO.isEmailSent()))
							setIntSuccess(request, 1);

					}

					String strFormData = "var arrFormData = new Array();";
					strFormData += " var saveSuccess = " + getIntSuccess(request) + ";";
					strFormData += setAlertStatus();
					request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);

				}

				saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_DISABLE_ROLLBACK_SUCCESS),
						WebConstants.MSG_SUCCESS);

			} else {

				OperationStatusTO operationStatusTO = ModuleServiceLocator.getAirportServiceBD().addNewDST(airportDst, alertDTO);

				if (operationStatusTO != null) {

					if (operationStatusTO.isAlertSent() && operationStatusTO.isEmailSent()) {
						// if alert and email sent --construct the message
						setIntSuccess(request, 5);

					} else {

						// if alerts are only sent - construct the message
						if (operationStatusTO.isAlertSent())
							setIntSuccess(request, 3);

						// if email is only sent - construct the message
						if (operationStatusTO.isEmailSent())
							setIntSuccess(request, 4);

						// only flights have been updated with no alerts or email
						if (!(operationStatusTO.isAlertSent()) && !(operationStatusTO.isEmailSent()))
							setIntSuccess(request, 1);
					}
					String strFormData = "var arrFormData = new Array();";
					strFormData += " var saveSuccess = " + getIntSuccess(request) + ";";
					strFormData += setAlertStatus();
					request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);

				}

				saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS), WebConstants.MSG_SUCCESS);
			}
			airportDst = null;

			// Need to published to LCC for any DST change
			Airport airport = ModuleServiceLocator.getAirportServiceBD().getAirport(strAirportCode);
			if (airport != null) {
				airport.setLccPublishStatus(Util.LCC_TOBE_REPUBLISHED);
				ModuleServiceLocator.getAirportServiceBD().saveAirport(airport);
			}

		} catch (ModuleException moduleException) {

			setExceptionOccured(request, true);

			String strFormData = "var arrFormData = new Array();";
			strFormData += "arrFormData[0] = new Array();";
			strFormData += "arrFormData[0][1] = '" + strStartDate + "';";
			strFormData += "arrFormData[0][2] = '" + strStartTime + "';";
			strFormData += "arrFormData[0][3] = '" + strEndDate + "';";
			strFormData += "arrFormData[0][4] = '" + strEndTime + "';";
			strFormData += "arrFormData[0][5] = '" + strAdjustTime + "';";
			strFormData += "arrFormData[0][6] = '" + strOperator + "';";
			strFormData += "arrFormData[0][7] = '" + strEmail + "';";
			strFormData += "arrFormData[0][8] = '" + strEmailId + "';";
			strFormData += "arrFormData[0][9] = '" + strAlert + "';";
			strFormData += "arrFormData[0][10] = '" + strStatus + "';";

			setIntSuccess(request, 2);
			strFormData += " var saveSuccess = " + getIntSuccess(request) + ";";
			strFormData += setAlertStatus();

			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_EDIT)
					|| strHdnMode.equals(WebConstants.ACTION_DISABLE_ROLLBACK)) {

				strFormData += " var prevVersion = " + strVersion + ";";
				strFormData += " var prevDSTId = " + strDSTId + ";";
				strFormData += " var prevGridRow = " + strGridRow + ";";
			}
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);

			log.error("Exception in AirportDSTRequestHandler:saveData() [origin module=" + moduleException.getModuleDesc() + "]",
					moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);

		} catch (Exception exception) {

			setExceptionOccured(request, true);

			String strFormData = "var arrFormData = new Array();";
			strFormData += "arrFormData[0] = new Array();";
			strFormData += "arrFormData[0][1] = '" + strStartDate + "';";
			strFormData += "arrFormData[0][2] = '" + strStartTime + "';";
			strFormData += "arrFormData[0][3] = '" + strEndDate + "';";
			strFormData += "arrFormData[0][4] = '" + strEndTime + "';";
			strFormData += "arrFormData[0][5] = '" + strAdjustTime + "';";
			strFormData += "arrFormData[0][6] = '" + strOperator + "';";
			strFormData += "arrFormData[0][7] = '" + strEmail + "';";
			strFormData += "arrFormData[0][8] = '" + strEmailId + "';";
			strFormData += "arrFormData[0][9] = '" + strAlert + "';";
			strFormData += "arrFormData[0][10] = '" + strStatus + "';";

			setIntSuccess(request, 2);
			strFormData += " var saveSuccess = " + getIntSuccess(request) + ";";
			strFormData += setAlertStatus();

			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_EDIT)
					|| strHdnMode.equals(WebConstants.ACTION_DISABLE_ROLLBACK)) {
				strFormData += " var prevVersion = " + strVersion + ";";
				strFormData += " var prevDSTId = " + strDSTId + ";";
				strFormData += " var prevGridRow = " + strGridRow + ";";
			}

			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
			log.error("Exception in AirportDSTRequestHandler:saveData()", exception);

			if (exception instanceof RuntimeException) {
				throw exception;
			} else {
				saveMessage(request, exception.getMessage(), WebConstants.MSG_ERROR);
			}
		}
		return true;
	}

	/**
	 * Set the Display Data For DST Page
	 * 
	 * @param request
	 *            the HttpServletRequests
	 */
	public static void setDisplayData(HttpServletRequest request) {
		setAirportDSTRowHtml(request);
		setClientErrors(request);
		if (!isExceptionOccured(request)) {
			String strFormData = "var arrFormData = new Array();";
			strFormData += " var saveSuccess = " + getIntSuccess(request) + ";";
			strFormData += setAlertStatus();

			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);

			request.setAttribute(WebConstants.REQ_OPERATION_MODE, getAttribInRequest(request, "strModeJSDST"));
		}
	}

	/**
	 * Sets the Client Validations for DST Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setClientErrors(HttpServletRequest request) {

		try {

			String strClientErrors = AirportDSTHTMLGenerator.getClientErrors(request);
			if (strClientErrors == null)
				strClientErrors = "";

			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);

		} catch (ModuleException moduleException) {
			log.error("Exception in AirportDSTRequestHandler:setClientErrors() [origin module=" + moduleException.getModuleDesc()
					+ "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the DST Row to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	protected static void setAirportDSTRowHtml(HttpServletRequest request) {

		try {

			AirportDSTHTMLGenerator htmlGen = new AirportDSTHTMLGenerator();
			String strHtml = htmlGen.getAirportDSTRowHtml(request);
			request.setAttribute(WebConstants.REQ_HTML_ROWS, strHtml);

		} catch (ModuleException moduleException) {

			log.error(
					"Exception in AirportDSTRequestHandler:setAirportDSTRowHtml() [origin module="
							+ moduleException.getModuleDesc() + "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	private static String setAlertStatus() {
		String strStatus = "";
		String sendEmailsForFlightAlterations = globalConfig.getBizParam(SystemParamKeys.SEND_EMAILS_FOR_FLIGHT_ALTERATIONS)
				.equalsIgnoreCase("Y") ? "Y" : "N";
		;
		String sendSMSForFlightAlterations = globalConfig.getBizParam(SystemParamKeys.SEND_SMS_FOR_FLIGHT_ALTERATIONS)
				.equalsIgnoreCase("Y") ? "Y" : "N";

		strStatus += " var sendEmailForPax = '" + sendEmailsForFlightAlterations + "';";
		strStatus += " var sendSMSForPax = '" + sendSMSForFlightAlterations + "';";

		return strStatus;
	}
}
