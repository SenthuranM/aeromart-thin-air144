package com.isa.thinair.airadmin.core.web.generator.master;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airmaster.api.model.HubInfo;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

/*******************************************************************************
 * Description : Hub Details Html Generator
 * 
 * @author : Nadika Dhanusha Mahatantila
 * @since : 25 Feb, 2008
 *******************************************************************************/

public class HubDetailsHTMLGenerator {

	private static Log log = LogFactory.getLog(HubDetailsHTMLGenerator.class);

	/**
	 * Creating the client error messages
	 * 
	 * @param request
	 * @return String
	 * @throws ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {
		String clientErrors = null;

		if (clientErrors == null) {
			Properties moduleErrs = new Properties();

			moduleErrs.setProperty("um.hubdetails.form.selecthub.edit", "errSelectHubToEdit");
			moduleErrs.setProperty("um.hubdetails.form.selecthub.delete", "errSelectHubToDelete");
			moduleErrs.setProperty("um.hubdetails.form.dropdown.required", "errDropdownRequired");
			moduleErrs.setProperty("um.hubdetails.form.field.required", "errFieldRequired");
			moduleErrs.setProperty("um.hubdetails.form.connectiontime.zero", "errZeroConnectionTime");
			moduleErrs.setProperty("um.hubdetails.form.connectiontime.format.invalid", "errInvalidConnectionTimeFormat");
			moduleErrs.setProperty("um.hubdetails.form.connectiontime.value.invalid", "errInvalidConnectionTimeValues");
			moduleErrs.setProperty("um.airadmin.delete.confirmation", "deleteConf");

			clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}
		return clientErrors;
	}

	/**
	 * Gets the Hub Details Grid Data Array for a Search critiriea
	 * 
	 * @param request
	 * @return String
	 * @throws ModuleException
	 */
	@SuppressWarnings("unchecked")
	public final String getHubDetailsRowHtml(HttpServletRequest request) throws ModuleException {

		log.debug("***********inside HubDetailsHTMLGenerator.getHubDetailsRowHtml()");

		int recNo = 0;
		int pageSize = 20;
		Collection<HubInfo> listArr = null;

		if (request.getParameter("hdnRecNo") == null) {
			recNo = 1;
		} else {
			recNo = Integer.parseInt(request.getParameter("hdnRecNo"));
		}

		List<ModuleCriterion> critrianList = new ArrayList<ModuleCriterion>();
		ModuleCriterion moduleCriterion = new ModuleCriterion();

		/**
		 * Set criterian
		 */
		// Hub code
		moduleCriterion.setCondition(ModuleCriterion.CONDITION_EQUALS);
		moduleCriterion.setFieldName("airportCode");

		String strHubCodeSearch = request.getParameter("selSearchHub");

		if (strHubCodeSearch != null) {
			List<String> value = new ArrayList<String>();

			if (!strHubCodeSearch.equals("")) {
				value.add(strHubCodeSearch);
				moduleCriterion.setValue(value);
				critrianList.add(moduleCriterion);
			}
		}

		// Inbound Carrier Code
		moduleCriterion = new ModuleCriterion();
		moduleCriterion.setCondition(ModuleCriterion.CONDITION_EQUALS);
		moduleCriterion.setFieldName("ibCarrierCode");

		String strIBCarrierCodeSearch = request.getParameter("selSearchInCarrier");

		if (strIBCarrierCodeSearch != null) {
			List<String> value = new ArrayList<String>();

			if (!strIBCarrierCodeSearch.equals("")) {
				value.add(strIBCarrierCodeSearch);
				moduleCriterion.setValue(value);
				critrianList.add(moduleCriterion);
			}
		}

		// Outbound Carrier Code
		moduleCriterion = new ModuleCriterion();
		moduleCriterion.setCondition(ModuleCriterion.CONDITION_EQUALS);
		moduleCriterion.setFieldName("obCarrierCode");

		String strOBCarrierCodeSearch = request.getParameter("selSearchOutCarrier");

		if (strOBCarrierCodeSearch != null) {
			List<String> value = new ArrayList<String>();

			if (!strOBCarrierCodeSearch.equals("")) {
				value.add(strOBCarrierCodeSearch);
				moduleCriterion.setValue(value);
				critrianList.add(moduleCriterion);
			}
		}

		Page page = null;
		page = ModuleServiceLocator.getLocationServiceBD().getHubDetails(critrianList, recNo - 1, pageSize);

		String strJavascriptTotalNoOfRecs = "";

		if (recNo > page.getTotalNoOfRecords()) {
			page = ModuleServiceLocator.getLocationServiceBD().getHubDetails(critrianList, 0, pageSize);
			strJavascriptTotalNoOfRecs = "var totalRecords = " + page.getTotalNoOfRecords() + ";";

		} else {
			int totRec = page.getTotalNoOfRecords();
			strJavascriptTotalNoOfRecs = "var totalRecords = " + totRec + ";";
		}

		if (page != null) {
			request.setAttribute(WebConstants.REQ_TOTAL_RECORDS, strJavascriptTotalNoOfRecs);
			listArr = page.getPageData();
		}

		return getGridData(listArr);
	}

	/**
	 * Method to generate the grid data
	 * 
	 * @param hubDetailsList
	 * @return String
	 */
	private static String getGridData(Collection<HubInfo> hubDetailsList) {
		StringBuffer sb = new StringBuffer("var arrData = new Array();");
		HubInfo hubInfo = null;
		int count = 0;

		if (hubDetailsList != null) {
			Iterator<HubInfo> iter = hubDetailsList.iterator();

			while (iter.hasNext()) {
				hubInfo = (HubInfo) iter.next();
				sb.append("arrData[" + count + "] = new Array();");
				sb.append("arrData[" + count + "][0] = '" + nullConvertToString(hubInfo.getHubId()) + "';");
				sb.append("arrData[" + count + "][1] = '" + nullConvertToString(hubInfo.getAirportCode()) + "';");
				sb.append("arrData[" + count + "][2] = '" + nullConvertToString(hubInfo.getMinConnectionTime()) + "';");
				sb.append("arrData[" + count + "][3] = '" + nullConvertToString(hubInfo.getMaxConnectionTime()) + "';");
				sb.append("arrData[" + count + "][4] = '" + nullConvertToString(hubInfo.getIbCarrierCode()) + "';");
				sb.append("arrData[" + count + "][5] = '" + nullConvertToString(hubInfo.getObCarrierCode()) + "';");
				sb.append("arrData[" + count + "][6] = '"
						+ nullConvertToString(AiradminUtils.getModifiedStatus(hubInfo.getStatus())) + "';");
				sb.append("arrData[" + count + "][7] = '" + hubInfo.getVersion() + "';");
				sb.append("arrData[" + count + "][8] = '" + nullConvertToString(hubInfo.getEditable()) + "';");

				count++;
			}
		}

		return sb.toString();
	}

	/**
	 * Null value to String
	 * 
	 * @param obj
	 * @return String
	 */
	private static String nullConvertToString(Object obj) {
		String strReturn = "";

		if (obj == null) {
			strReturn = "";
		} else {
			strReturn = obj.toString().trim();
		}
		return strReturn;
	}

}