package com.isa.thinair.airadmin.core.web.action.tools;

import java.util.ArrayList;
import java.util.List;

public class MyModel {
	private String queueName=null;
	private String description=null;
	private String applicablePages=null;
	private String assignedUsers=null;
	private List<String> li=new ArrayList<String>();
	public String getQueueName() {
		li.add("a");li.add("b");
		return queueName;
	}
	public List<String> getLi() {
		return li;
	}
	public void setLi(List<String> li) {
		this.li = li;
	}
	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getApplicablePages() {
		return applicablePages;
	}
	public void setApplicablePages(String applicablePages) {
		this.applicablePages = applicablePages;
	}
	public String getAssignedUsers() {
		return assignedUsers;
	}
	public void setAssignedUsers(String assignedUsers) {
		this.assignedUsers = assignedUsers;
	}
		
	
}
