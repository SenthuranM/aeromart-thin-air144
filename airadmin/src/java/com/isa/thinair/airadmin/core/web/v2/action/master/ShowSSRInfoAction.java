package com.isa.thinair.airadmin.core.web.v2.action.master;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.AiradminModuleConfig;
import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airadmin.core.web.v2.handler.master.SSRInfoRH;
import com.isa.thinair.airadmin.core.web.v2.util.DataUtil;
import com.isa.thinair.airmaster.api.dto.SSRSearchDTO;
import com.isa.thinair.airmaster.api.model.AirportTransfer;
import com.isa.thinair.airmaster.api.model.SSR;
import com.isa.thinair.airmaster.api.model.SSRApplicableAirports;
import com.isa.thinair.airmaster.api.model.SSRApplicableOND;
import com.isa.thinair.airmaster.api.model.SSRCategory;
import com.isa.thinair.airmaster.api.model.SSRConstraints;
import com.isa.thinair.airmaster.api.model.SSRSubCategory;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.dto.SSRInfoDTO;
import com.isa.thinair.commons.api.exception.MessagesUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author M.Rikaz
 * 
 */

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ShowSSRInfoAction extends BaseRequestAwareAction {

	private SSRSearchDTO ssrSearchDTO;

	private int page;

	private int total;

	private int records;

	private Object[] rows;

	private String succesMsg;

	private String msgType;

	private String description;

	private String status;

	private boolean validForPalCal;
	
	private boolean userDefinedSSR;

	private long version;

	private String createdBy;

	private Date createdDate;

	private String hdnMode;

	private String cabinClassCode;

	private String hdnAirportList;

	private String applDeparture = "N";

	private String applArrival = "N";

	private String applTransit = "N";

	private String appConsId;

	private Integer ssrId;
	private String ssrCode;
	private boolean shownInIBE;

	private boolean shownInXBE;

	private boolean shownInGds;

	private boolean shownInWs;

	private Integer ssrSubCatId;

	private Integer ssrCatId;

	private String ssrDesc;

	private Integer transitMaxMins;

	private Integer transitMinMins;

	private String hdnOldAirportList;

	private String hdnOndList;

	private String hdnOldOndList;

	private String ssrName;

	private String uploadImage;
	private String uploadImageThumbnail;

	private String ssrDescForDisplay;

	private int sortOrder;

	private int airportServiceCount;

	private int inflightServiceCount;

	private int airportTranferCount;

	private String ssrCutoffTime;

	public String getSsrCutoffTime() {
		return ssrCutoffTime;
	}

	public void setSsrCutoffTime(String ssrCutoffTime) {
		this.ssrCutoffTime = ssrCutoffTime;
	}

	public String getSsrDescForDisplay() {
		return ssrDescForDisplay;
	}

	public void setSsrDescForDisplay(String ssrDescForDisplay) {
		this.ssrDescForDisplay = ssrDescForDisplay;
	}

	public String getUploadImageThumbnail() {
		return uploadImageThumbnail;
	}

	public String getUploadImage() {
		return uploadImage;
	}

	public void setUploadImage(String uploadImage) {
		this.uploadImage = uploadImage;
	}

	public void setUploadImageThumbnail(String uploadImageThumbnail) {
		this.uploadImageThumbnail = uploadImageThumbnail;
	}

	public String getSsrName() {
		return ssrName;
	}

	public void setSsrName(String ssrName) {
		this.ssrName = ssrName;
	}

	public String getHdnOldAirportList() {
		return hdnOldAirportList;
	}

	public void setHdnOldAirportList(String hdnOldAirportList) {
		this.hdnOldAirportList = hdnOldAirportList;
	}

	public String getAppConsId() {
		return appConsId;
	}

	public void setAppConsId(String appConsId) {
		this.appConsId = appConsId;
	}

	public String getApplDeparture() {
		return applDeparture;
	}

	public void setApplDeparture(String applDeparture) {
		this.applDeparture = applDeparture;
	}

	public String getApplArrival() {
		return applArrival;
	}

	public void setApplArrival(String applArrival) {
		this.applArrival = applArrival;
	}

	public String getApplTransit() {
		return applTransit;
	}

	public void setApplTransit(String applTransit) {
		this.applTransit = applTransit;
	}

	public String getHdnAirportList() {
		return hdnAirportList;
	}

	public void setHdnAirportList(String hdnAirportList) {
		this.hdnAirportList = hdnAirportList;
	}

	public Integer getSsrId() {
		return ssrId;
	}

	public void setSsrId(Integer ssrId) {
		this.ssrId = ssrId;
	}

	public boolean isShownInIBE() {
		return shownInIBE;
	}

	public void setShownInIBE(boolean shownInIBE) {
		this.shownInIBE = shownInIBE;
	}

	public boolean isShownInXBE() {
		return shownInXBE;
	}

	public void setShownInXBE(boolean shownInXBE) {
		this.shownInXBE = shownInXBE;
	}

	public boolean isShownInGds() {
		return shownInGds;
	}

	public void setShownInGds(boolean shownInGds) {
		this.shownInGds = shownInGds;
	}

	public Integer getTransitMaxMins() {
		return transitMaxMins;
	}

	public void setTransitMaxMins(Integer transitMaxMins) {
		this.transitMaxMins = transitMaxMins;
	}

	public Integer getTransitMinMins() {
		return transitMinMins;
	}

	public void setTransitMinMins(Integer transitMinMins) {
		this.transitMinMins = transitMinMins;
	}

	public String getSsrDesc() {
		return ssrDesc;
	}

	public void setSsrDesc(String ssrDesc) {
		this.ssrDesc = ssrDesc;
	}

	public Integer getSsrCatId() {
		return ssrCatId;
	}

	public void setSsrCatId(Integer ssrCatId) {
		this.ssrCatId = ssrCatId;
	}

	public Integer getSsrSubCatId() {
		return ssrSubCatId;
	}

	public void setSsrSubCatId(Integer ssrSubCatId) {
		this.ssrSubCatId = ssrSubCatId;
	}

	public String getSsrCode() {
		return ssrCode;
	}

	public void setSsrCode(String ssrCode) {
		this.ssrCode = ssrCode;
	}

	public int getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(int sortOrder) {
		this.sortOrder = sortOrder;
	}

	public int getAirportServiceCount() {
		return airportServiceCount;
	}

	public void setAirportServiceCount(int airportServiceCount) {
		this.airportServiceCount = airportServiceCount;
	}

	public int getAirportTranferCount() {
		return airportTranferCount;
	}

	public void setAirportTranferCount(int airportTranferCount) {
		this.airportTranferCount = airportTranferCount;
	}

	public int getInflightServiceCount() {
		return inflightServiceCount;
	}

	public void setInflightServiceCount(int inflightServiceCount) {
		this.inflightServiceCount = inflightServiceCount;
	}

	private String getFormattedSSRCutOffTime() {
		if (ssrCutoffTime != null && !("").equals(ssrCutoffTime)) {
			String[] cutOffTime = ssrCutoffTime.split(":");
			if (cutOffTime.length == 2) {
				String hours = (cutOffTime[0].length() < 2) ? "0" + cutOffTime[0] : cutOffTime[0];
				String mins = cutOffTime[1];
				
				return (hours.trim() + ":" + mins.trim());
			} else {
				return "";
			}
		} else {
			return "";
		}
	}

	private static AiradminConfig airadminConfig = new AiradminConfig();

	private static Log log = LogFactory.getLog(ShowSSRInfoAction.class);

	public String searchSSRInfo() {

		try {

			if (ssrSearchDTO == null) {
				ssrSearchDTO = new SSRSearchDTO();
			}

			Page pgSSRInfo = SSRInfoRH.searchSSRInfo(this.page, ssrSearchDTO);

			this.records = pgSSRInfo.getTotalNoOfRecords();

			this.total = pgSSRInfo.getTotalNoOfRecords() / 20;
			int mod = pgSSRInfo.getTotalNoOfRecords() % 20;
			if (mod > 0)
				this.total = this.total + 1;
			if (this.page <= 0)
				this.page = 1;

			Collection<SSR> colSSRInfo = pgSSRInfo.getPageData();

			if (colSSRInfo != null) {

				Object[] dataRow = new Object[colSSRInfo.size()];
				int index = 1;
				Iterator<SSR> iter = colSSRInfo.iterator();
				while (iter.hasNext()) {
					Map<String, Object> counmap = new HashMap<String, Object>();

					SSR grdSsrData = (SSR) iter.next();

					SSRInfoDTO jSonSsr = new SSRInfoDTO();

					counmap.put("id", new Integer(((page - 1) * 20) + index).toString());

					jSonSsr.setSSRId(grdSsrData.getSsrId());
					jSonSsr.setSSRCode(grdSsrData.getSsrCode());
					jSonSsr.setSSRName(grdSsrData.getSsrName());
					jSonSsr.setSSRDescription(grdSsrData.getSsrDesc());
					jSonSsr.setSSRCategoryId(grdSsrData.getSsrSubCategory().getCategory().getCatId());
					jSonSsr.setSSRSubCategoryId(grdSsrData.getSsrSubCategory().getSubCatId());
					jSonSsr.setStatus(grdSsrData.getStatus());
					jSonSsr.setCreatedBy(grdSsrData.getCreatedBy());
					jSonSsr.setCreatedDate(grdSsrData.getCreatedDate());
					jSonSsr.setModifiedBy(grdSsrData.getModifiedBy());
					jSonSsr.setModifiedDate(grdSsrData.getModifiedDate());
					jSonSsr.setSortOrder(grdSsrData.getSortOrder());
					jSonSsr.setVersion(grdSsrData.getVersion());
					jSonSsr.setSsrCutoffTime(grdSsrData.getSsrCutoffTime());
					jSonSsr.setValidForPALCAL(grdSsrData.isValidForPALCAL());
					jSonSsr.setUserDefinedSSR(grdSsrData.isUserDefinedSSR());

					// Process Applicable AirportList
					String airportList = "";
					String ondList = "";
					String applyConstarints = "";
					String maxMinTransitTimeMins = "0,0";

					SSRConstraints ssrConsExObj = grdSsrData.getSSRConstraints();
					if ((ssrConsExObj != null) && (ssrConsExObj.getAppConsId() != null) && (ssrConsExObj.getAppConsId() > 0)) {
						applyConstarints = ssrConsExObj.getAppConsId() + "," + ssrConsExObj.getApplyDeparture() + ","
								+ ssrConsExObj.getApplyArrival() + "," + ssrConsExObj.getApplyTransit();

						maxMinTransitTimeMins = ssrConsExObj.getMaxTransitTimeMins() + "," + ssrConsExObj.getMinTransitTimeMins();

						if (ssrConsExObj.getApplicableAirports() != null && ssrConsExObj.getApplicableAirports().size() != 0) {
							Set<SSRApplicableAirports> applAiportsList = ssrConsExObj.getApplicableAirports();

							Iterator<SSRApplicableAirports> airportItr = applAiportsList.iterator();

							while (airportItr.hasNext()) {
								SSRApplicableAirports ssrApplicableAirport = airportItr.next();
								airportList += ssrApplicableAirport.getAppAirportId() + ","
										+ ssrApplicableAirport.getAirportCode() + ","
										+ ssrApplicableAirport.getAppConstraints().getAppConsId() + "^";
							}
						}

						if (ssrConsExObj.getApplicableOnds() != null && ssrConsExObj.getApplicableOnds().size() > 0) {
							for (SSRApplicableOND appOnd : (Set<SSRApplicableOND>) ssrConsExObj.getApplicableOnds()) {
								ondList += appOnd.getAppOndId() + "," + appOnd.getOndCode() + ","
										+ appOnd.getAppConstraints().getAppConsId() + "^";
							}
						}

					}

					if (grdSsrData.getShowInIBE() != null && grdSsrData.getShowInIBE().equals("Y"))
						jSonSsr.setShownInIBE(true);

					if (grdSsrData.getShowInXBE() != null && grdSsrData.getShowInXBE().equals("Y"))
						jSonSsr.setShownInXBE(true);

					if (grdSsrData.getShowInGDS() != null && grdSsrData.getShowInGDS().equals("Y"))
						jSonSsr.setShownInGDS(true);

					if (grdSsrData.getShowInWs() != null && grdSsrData.getShowInWs().equals("Y"))
						jSonSsr.setShownInWs(true);

					String langTraStr = "";

					try {
						if (grdSsrData.getI18nMessageKey() != null) {
							langTraStr = DataUtil.convertLanguageTranslations(grdSsrData.getI18nMessageKey().getI18nMessages());
						}
					} catch (Exception e) {
						langTraStr = "";
					}

					counmap.put("ssrInfo", jSonSsr);
					counmap.put("applyConstarints", applyConstarints); // 4,Y,Y,Y
					counmap.put("applicableAirports", airportList); // 3,SHJ,3^
					counmap.put("applicableOnds", ondList);// 1,SHJ/***,2^
					counmap.put("consMaxMinTime", maxMinTransitTimeMins); // maxTime,minTime
					counmap.put("ssrDescriptionForDisplay", langTraStr); // language
																			// translation

					dataRow[index - 1] = counmap;
					index++;
				}

				this.rows = dataRow;
			}

		} catch (ModuleException e) {
			this.succesMsg = e.getMessageString();
			this.msgType = WebConstants.MSG_ERROR;
			if (log.isErrorEnabled()) {
				log.error(e);
			}
		} catch (Exception ex) {
			this.succesMsg = "System Error Please Contact Administrator";
			this.msgType = WebConstants.MSG_ERROR;
			if (log.isErrorEnabled()) {
				log.error(ex);
			}
		}

		return S2Constants.Result.SUCCESS;
	}

	public String deleteSSRInfo() {
		try {

			if (checkConstrainsOnDeletion(ssrId)) {
				SSRInfoRH.deleteSSRInfo(ssrId);
				if (sortOrder != 0) {
					decrementSortOrderAfterDelete(sortOrder);
				}

				airportServiceCount = ModuleServiceLocator.getSsrServiceBD().getSSRCount(SSRCategory.ID_AIRPORT_SERVICE);
				inflightServiceCount = ModuleServiceLocator.getSsrServiceBD().getSSRCount(SSRCategory.ID_INFLIGHT_SERVICE);
				airportTranferCount = ModuleServiceLocator.getSsrServiceBD().getSSRCount(SSRCategory.ID_AIRPORT_TRANSFER);

				this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_DELETE_SUCCESS);
				this.msgType = WebConstants.MSG_SUCCESS;
			} else {
				this.succesMsg = airadminConfig.getMessage("um.ssr.form.insufficient.priviledge");
				this.msgType = WebConstants.MSG_ERROR;
			}

		} catch (ModuleException e) {
			this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_DELETE_FAIL);
			if (e.getMessageString().equals(MessagesUtil.getMessage("module.constraint.childrecord"))) {
				this.succesMsg = airadminConfig.getMessage("um.airadmin.childrecord");
			}
			this.msgType = WebConstants.MSG_ERROR;
			if (log.isErrorEnabled()) {
				log.error(e);
			}
		} catch (Exception ex) {
			this.succesMsg = "System Error Please Contact Administrator";
			this.msgType = WebConstants.MSG_ERROR;
			if (log.isErrorEnabled()) {
				log.error(ex);
			}
		}

		return S2Constants.Result.SUCCESS;
	}

	public String execute() {

		if (this.hdnMode != null
				&& ((hdnMode.equals(WebConstants.ACTION_ADD)) || (hdnMode.equals(WebConstants.ACTION_EDIT)) || (hdnMode
						.equals(WebConstants.ACTION_SAVE_LIST_FOR_DISPLAY)))) {

			try {

				SSRInfoDTO sSRInfoDTO = new SSRInfoDTO();

				sSRInfoDTO.setCreatedBy(createdBy);
				sSRInfoDTO.setCreatedDate(createdDate);
				sSRInfoDTO.setShownInIBE(shownInIBE);
				sSRInfoDTO.setShownInXBE(shownInXBE);
				sSRInfoDTO.setShownInGDS(shownInGds);
				sSRInfoDTO.setSSRCategoryId(ssrCatId);
				sSRInfoDTO.setSSRCode(ssrCode);
				sSRInfoDTO.setSSRDescription(ssrDesc);
				sSRInfoDTO.setSSRId(ssrId);
				sSRInfoDTO.setSSRSubCategoryId(ssrSubCatId);
				sSRInfoDTO.setSSRName(ssrName);
				sSRInfoDTO.setSortOrder(sortOrder);
				sSRInfoDTO.setSsrCutoffTime(getFormattedSSRCutOffTime());
				sSRInfoDTO.setShownInWs(shownInWs);
				sSRInfoDTO.setValidForPALCAL(validForPalCal);
				sSRInfoDTO.setUserDefinedSSR(userDefinedSSR);

				if (status == null)
					status = SSR.STATUS_INACTIVE;

				sSRInfoDTO.setStatus(status);
				sSRInfoDTO.setVersion(version);
				sSRInfoDTO.setAirportList(hdnAirportList);
				sSRInfoDTO.setOndList(hdnOndList);

				if (hdnMode.equals(WebConstants.ACTION_ADD)) {
					sSRInfoDTO.setAppConsId(null);
				} else {
					sSRInfoDTO.setAppConsId(appConsId);
				}

				sSRInfoDTO.setApplDeparture(applDeparture);
				sSRInfoDTO.setApplArrival(applArrival);
				sSRInfoDTO.setApplTransit(applTransit);
				sSRInfoDTO.setTransitMaxMins(transitMaxMins);
				sSRInfoDTO.setTransitMinMins(transitMinMins);
				sSRInfoDTO.setSelectedLanguageTranslations(ssrDescForDisplay);

				String msg = null;

				if (hdnMode.equals(WebConstants.ACTION_EDIT)) {
					checkAndAddNewONDs(hdnOndList);
					this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS);
					this.msgType = WebConstants.MSG_SUCCESS;
				}

				if (hdnMode.equals(WebConstants.ACTION_EDIT) && verfiyAirportList(hdnOldAirportList, hdnAirportList)) {
					this.succesMsg = airadminConfig.getMessage("um.ssr.form.airport.code.depend");
					this.msgType = WebConstants.MSG_ERROR;
				} else if (hdnMode.equals(WebConstants.ACTION_EDIT) && verifyOndList(hdnOldOndList, hdnOndList)) {
					this.succesMsg = airadminConfig.getMessage("um.ssr.form.ond.code.depend");
					this.msgType = WebConstants.MSG_ERROR;
				} else if ((msg = retrieveSSRCategoryLevelRestrictions(ssrCatId)) != null) {
					this.succesMsg = msg;
					this.msgType = WebConstants.MSG_ERROR;
				} else {
					int endSortOrder = 0;
					if (hdnMode.equals(WebConstants.ACTION_EDIT)) {
						SSR existingSSR = ModuleServiceLocator.getSsrServiceBD().getSSR(ssrId);
						endSortOrder = existingSSR.getSortOrder();
					}
					SSRInfoRH.saveSSRInfo(sSRInfoDTO);

					if (this.uploadImage != null && this.uploadImage.trim().equalsIgnoreCase("UPLOADED")) {
						AiradminModuleConfig adminCon = AiradminModuleUtils.getConfig();
						String mountPath = adminCon.getMealImages();
						String fullFileName = mountPath + "ssrImage_test.jpg";
						File tmpFile = new File(fullFileName);
						String imageFile = mountPath + "ssr_" + ssrCode.toUpperCase() + ".jpg"; // no need to
																								// add the
						// suffix
						File dataFile = new File(imageFile);
						FileUtils.copyFile(tmpFile, dataFile);
					}
					if (this.uploadImageThumbnail != null && this.uploadImageThumbnail.trim().equalsIgnoreCase("UPLOADED")) {
						AiradminModuleConfig adminCon = AiradminModuleUtils.getConfig();
						String mountPath = adminCon.getMealImages();
						String fullFileName = mountPath + "ssrThumbnailImage_test.jpg";
						File tmpFile = new File(fullFileName);
						String imageFile = mountPath + "ssr_thumbnail_" + ssrCode.toUpperCase() + ".jpg"; // no need to
																											// add
						// the suffix
						File dataFile = new File(imageFile);
						FileUtils.copyFile(tmpFile, dataFile);
					}

					if (sSRInfoDTO.getSortOrder() != 0) {
						incrementSortOrderValueAfterSave(sSRInfoDTO.getSortOrder(), endSortOrder, sSRInfoDTO.getSSRCode());
					}

					airportServiceCount = ModuleServiceLocator.getSsrServiceBD().getSSRCount(SSRCategory.ID_AIRPORT_SERVICE);
					inflightServiceCount = ModuleServiceLocator.getSsrServiceBD().getSSRCount(SSRCategory.ID_INFLIGHT_SERVICE);
					airportTranferCount = ModuleServiceLocator.getSsrServiceBD().getSSRCount(SSRCategory.ID_AIRPORT_TRANSFER);

					this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS);
					this.msgType = WebConstants.MSG_SUCCESS;
				}

			} catch (ModuleException e) {
				this.succesMsg = e.getMessageString();
				if (e.getMessageString().equals(MessagesUtil.getMessage("module.duplicate.key"))) {
					this.succesMsg = airadminConfig.getMessage("um.ssr.form.id.already.exist");
				}
				this.msgType = WebConstants.MSG_ERROR;
			} catch (Exception ex) {
				this.succesMsg = "System Error Please Contact Administrator";
				this.msgType = WebConstants.MSG_ERROR;
				if (log.isErrorEnabled()) {
					log.error(ex);
				}
			}
		}

		return S2Constants.Result.SUCCESS;
	}

	private String retrieveSSRCategoryLevelRestrictions(Integer ssrCatId) throws ModuleException {
		String msg = null;
		if (ssrCatId != null && ssrCatId.equals(SSRCategory.ID_AIRPORT_TRANSFER)) {
			if (hdnMode.equals(WebConstants.ACTION_ADD)
					&& !BasicRequestHandler.hasPrivilege(request, WebConstants.PRIVI_AIRPORT_TRANSFER_SSR_ADD)) {
				msg = airadminConfig.getMessage("um.ssr.form.insufficient.priviledge");
			} else if (hdnMode.equals(WebConstants.ACTION_EDIT)
					&& !BasicRequestHandler.hasPrivilege(request, WebConstants.PRIVI_AIRPORT_TRANSFER_SSR_EDIT)) {
				msg = airadminConfig.getMessage("um.ssr.form.insufficient.priviledge");
			} else if (isCabServiceAleradyDefined()) {
				msg = airadminConfig.getMessage("um.ssr.form.system.has.already.defined.cabs");
			} else if (!StringUtil.isNullOrEmpty(hdnAirportList)) {

				String[] airportCodes = hdnAirportList.split(",");
				for (String airportCode : airportCodes) {
					AirportTransfer matchedTransfer = ModuleServiceLocator.getAirportTransferServiceBD().getAirportTransfer(
							airportCode);
					if (matchedTransfer == null || matchedTransfer.getStatus().equals("INA")) {
						msg = airadminConfig.getMessage("um.ssr.form.no.active.airport.transfers");
						break;
					}
				}
			}
		}
		return msg;
	}

	/**
	 * 
	 * Should be only one cab service defined for the system
	 */
	private boolean isCabServiceAleradyDefined() throws ModuleException {
		boolean alreadyDefined = false;
		if (ssrSubCatId != null && ssrSubCatId.equals(SSRSubCategory.ID_AIRPORT_CABS)) {
			SSRSearchDTO ssrSearchDTO = new SSRSearchDTO();
			ssrSearchDTO.setSsrSubCategory(String.valueOf(ssrSubCatId));
			Page ssrInfo = ModuleServiceLocator.getSsrServiceBD().getSSRs(ssrSearchDTO, 0, 3);
			Collection<SSR> colSSRInfo = ssrInfo.getPageData();
			if (colSSRInfo != null && !colSSRInfo.isEmpty()) {
				alreadyDefined = true;
				for (SSR ssr : colSSRInfo) {
					String persistedCode = ssr.getSsrCode();
					if (ssrCode.equals(persistedCode)) {
						alreadyDefined = false;
						break;
					}
				}
			}
		}
		return alreadyDefined;
	}

	private boolean checkConstrainsOnDeletion(Integer ssrId) throws ModuleException {
		boolean allow = true;
		if (ssrId != null) {
			Integer ssrCatId = ModuleServiceLocator.getSsrServiceBD().getSSRCategoryFromSSrId(ssrId);
			if (ssrCatId != null && ssrCatId.equals(SSRCategory.ID_AIRPORT_TRANSFER)
					&& !BasicRequestHandler.hasPrivilege(request, WebConstants.PRIVI_AIRPORT_TRANSFER_SSR_DELETE)) {
				allow = false;
			}
		}

		return allow;
	}

	/*
	 * This method will decrement the sort order of SSRs having sort order value greater than deleted SSR
	 */
	private void decrementSortOrderAfterDelete(int sortOrder) throws ModuleException {
		List ssrList = ModuleServiceLocator.getSsrServiceBD().getSSRList(sortOrder, 0);

		if (ssrList != null && ssrList.size() > 0) {
			List<Integer> ssrIdList = new ArrayList<Integer>();
			for (SSR ssr : (List<SSR>) ssrList) {
				ssrIdList.add(ssr.getSsrId());
			}

			ModuleServiceLocator.getSsrServiceBD().saveSSRs(ssrIdList, false);
		}

	}

	/*
	 * This method will increment the sort order of SSRs having sort order value greater than added/updated SSR
	 */
	private void incrementSortOrderValueAfterSave(int modifiedSortOrder, int originalSortOrder, String ssrCode)
			throws ModuleException {

		int startSortOrder = modifiedSortOrder;
		int endSortOrder = originalSortOrder;
		boolean isIncrement = true;

		if (originalSortOrder != 0) {
			if (modifiedSortOrder > originalSortOrder) {
				endSortOrder = modifiedSortOrder;
				startSortOrder = originalSortOrder;
				isIncrement = false;
			} else {
				endSortOrder = originalSortOrder;
				startSortOrder = modifiedSortOrder;
				isIncrement = true;
			}
		}

		List ssrList = ModuleServiceLocator.getSsrServiceBD().getSSRList(startSortOrder, endSortOrder);
		boolean isEdited = false;

		if (ssrList != null && ssrList.size() > 0) {

			List<Integer> ssrEditedList = new ArrayList<Integer>();

			for (SSR ssr : (List<SSR>) ssrList) {
				if (!ssr.getSsrCode().equalsIgnoreCase(ssrCode)) {
					isEdited = true;
					ssrEditedList.add(ssr.getSsrId());
				}
			}

			if (isEdited) {
				ModuleServiceLocator.getSsrServiceBD().saveSSRs(ssrEditedList, isIncrement);
			}
		}

	}

	/**
	 * @return the ssrSearchDTO
	 */
	public SSRSearchDTO getBagSrch() {
		return ssrSearchDTO;
	}

	/**
	 * @param ssrSearchDTO
	 *            the ssrSearchDTO to set
	 */
	public void setBagSrch(SSRSearchDTO bagSrch) {
		this.ssrSearchDTO = bagSrch;
	}

	/**
	 * @return the page
	 */
	public int getPage() {
		return page;
	}

	/**
	 * @param page
	 *            the page to set
	 */
	public void setPage(int page) {
		this.page = page;
	}

	/**
	 * @return the total
	 */
	public int getTotal() {
		return total;
	}

	/**
	 * @param total
	 *            the total to set
	 */
	public void setTotal(int total) {
		this.total = total;
	}

	/**
	 * @return the records
	 */
	public int getRecords() {
		return records;
	}

	/**
	 * @param records
	 *            the records to set
	 */
	public void setRecords(int records) {
		this.records = records;
	}

	/**
	 * @return the rows
	 */
	public Object[] getRows() {
		return rows;
	}

	/**
	 * @param rows
	 *            the rows to set
	 */
	public void setRows(Object[] rows) {
		this.rows = rows;
	}

	/**
	 * @return the succesMsg
	 */
	public String getSuccesMsg() {
		return succesMsg;
	}

	/**
	 * @param succesMsg
	 *            the succesMsg to set
	 */
	public void setSuccesMsg(String succesMsg) {
		this.succesMsg = succesMsg;
	}

	/**
	 * @return the msgType
	 */
	public String getMsgType() {
		return msgType;
	}

	/**
	 * @param msgType
	 *            the msgType to set
	 */
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the version
	 */
	public long getVersion() {
		return version;
	}

	/**
	 * @param version
	 *            the version to set
	 */
	public void setVersion(long version) {
		this.version = version;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate
	 *            the createdDate to set
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return the hdnMode
	 */
	public String getHdnMode() {
		return hdnMode;
	}

	/**
	 * @param hdnMode
	 *            the hdnMode to set
	 */
	public void setHdnMode(String hdnMode) {
		this.hdnMode = hdnMode;
	}

	/**
	 * @return the cabinClassCode
	 */
	public String getCabinClassCode() {
		return cabinClassCode;
	}

	/**
	 * @param cabinClassCode
	 *            the cabinClassCode to set
	 */
	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	public String getHdnOndList() {
		return hdnOndList;
	}

	public void setHdnOndList(String hdnOndList) {
		this.hdnOndList = hdnOndList;
	}

	public String getHdnOldOndList() {
		return hdnOldOndList;
	}

	public void setHdnOldOndList(String hdnOldOndList) {
		this.hdnOldOndList = hdnOldOndList;
	}

	public boolean isValidForPalCal() {
		return validForPalCal;
	}

	public void setValidForPalCal(boolean validForPALCAL) {
		this.validForPalCal = validForPALCAL;
	}
	
	public boolean isUserDefinedSSR() {
		return userDefinedSSR;
	}

	public void setUserDefinedSSR(boolean userDefinedSSR) {
		this.userDefinedSSR = userDefinedSSR;
	}

	// system shouldn't allow an airport to be removed when an airport service
	// is requested from the relevant airport
	// this validates whether this SSR is booked in removed airports
	private boolean verfiyAirportList(String oldAirports, String newAirports) throws ModuleException {
		List<String> missedAirportList = new ArrayList<String>();

		List<String> oldAirportList = new ArrayList<String>();
		List<String> newAirportList = new ArrayList<String>();

		if (!StringUtil.isNullOrEmpty(oldAirports)) {
			oldAirportList = Arrays.asList(oldAirports.split(","));
		}

		if (!StringUtil.isNullOrEmpty(newAirports)) {
			newAirportList = Arrays.asList(newAirports.split(","));
		}

		for (String aptStr : oldAirportList) {
			if (!newAirportList.contains(aptStr)) {
				missedAirportList.add(aptStr);
			}
		}

		Map<Integer, List<String>> bookedHalaBySsrIdMap = SelectListGenerator.getBookedHalaAirportBySsrId();

		if (bookedHalaBySsrIdMap != null && bookedHalaBySsrIdMap.size() > 0 && ssrId > 0) {
			List<String> airportsList = bookedHalaBySsrIdMap.get(ssrId);
			if (airportsList != null && airportsList.size() > 0) {
				for (String missedAirport : missedAirportList) {
					if (airportsList.contains(missedAirport)) {
						return true;
					}
				}
			}
		}

		return false;
	}

	private void checkAndAddNewONDs(String newOnds) throws ModuleException {
		List<String> newOndList = new ArrayList<String>();
		if (!StringUtil.isNullOrEmpty(newOnds)) {
			newOndList = Arrays.asList(newOnds.split(","));
		}

		if (newOndList.size() > 0) {
			// Check and add newly entered ONDs are exists in the system (T_OND table)
			ModuleServiceLocator.getChargeBD().saveONDs(new HashSet<String>(newOndList));
		}
	}

	private boolean verifyOndList(String oldOnds, String newOnds) throws ModuleException {
		List<String> oldOndList = new ArrayList<String>();
		List<String> newOndList = new ArrayList<String>();
		List<String> missedOndList = new ArrayList<String>();

		if (!StringUtil.isNullOrEmpty(oldOnds)) {
			oldOndList = Arrays.asList(oldOnds.split(","));
		}

		if (!StringUtil.isNullOrEmpty(newOnds)) {
			newOndList = Arrays.asList(newOnds.split(","));
		}

		for (String ssrStr : oldOndList) {
			if (!newOndList.contains(ssrStr)) {
				missedOndList.add(ssrStr);
			}
		}

		if (missedOndList.size() > 0) {
			List<String> bookedSSRSegList = SelectListGenerator.getBookedSSRSegment(ssrId.toString());

			Set<String> allSegCombinations = getAllSegmentCombinations(bookedSSRSegList);

			for (String segCode : allSegCombinations) {
				if (missedOndList.contains(segCode)) {
					return true;
				}
			}
		}

		return false;
	}

	private Set<String> getAllSegmentCombinations(List<String> segmentCodes) {
		Set<String> allSegs = new HashSet<String>();
		if (segmentCodes != null) {
			for (String segmentCode : segmentCodes) {
				Set<String> segments = getMultiLegSegList(segmentCode);
				for (String segment : segments) {
					allSegs.add(segment);
					String[] ondArray = segment.split("/");
					if (ondArray.length == 2) {
						String origin = ondArray[0];
						String destination = ondArray[1];
						allSegs.add(origin + "/***");
						allSegs.add("***/" + destination);
					}
				}
			}
		}

		return allSegs;
	}

	private static Set<String> getMultiLegSegList(String segmentCode) {
		Set<String> segments = new HashSet<String>();
		String ssArr[] = segmentCode.split("/");
		if (ssArr.length == 2) {
			segments.add(ssArr[0] + "/" + ssArr[1]);
		} else {
			for (int i = 0; i < ssArr.length - 2; i++) {
				String ori = ssArr[i + 0];
				String con = ssArr[i + 1];
				String des = ssArr[i + 2];

				segments.add(ori + "/" + con);
				segments.add(con + "/" + des);
			}
		}

		return segments;

	}

	public boolean isShownInWs() {
		return shownInWs;
	}

	public void setShownInWs(boolean shownInWs) {
		this.shownInWs = shownInWs;
	}
}
