package com.isa.thinair.airadmin.core.web.v2.action.promotion;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONResult;
import org.apache.struts2.json.JSONUtil;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airreservation.api.dto.TrackInfoDTO;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.promotion.api.to.anciOffer.AnciOfferCriteriaSearchTO;
import com.isa.thinair.promotion.api.to.anciOffer.AnciOfferCriteriaTO;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

/**
 * Managing administrative actions for AnciOfferCriteria
 * 
 * @author thihara
 * 
 */
@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ManageAnciOfferCriteriaAction extends BaseRequestAwareAction {

	private final int PAGE_LENGTH = 20;

	private Collection<AnciOfferCriteriaTO> anciOfferCriterias;

	private AnciOfferCriteriaSearchTO searchCriteria = new AnciOfferCriteriaSearchTO();

	private AnciOfferCriteriaTO anciOffer = new AnciOfferCriteriaTO();

	private int page;
	private int totalRecords;
	private int totalPages;

	private boolean success = true;
	private String messageTxt;

	private boolean isNewSearch;

	private Map<String, String> nameTranslations;

	private Map<String, String> descriptionTranslations;

	private Log log = LogFactory.getLog(ManageAnciOfferCriteriaAction.class);

	public String search() {
		try {
			checkPrivileges();
			setEmptySearchParamsToNull();

			/*
			 * jQGrid keep sending the existing page number even if it's a new search, this snippet will set the page
			 * number to 1 to emulate a fresh search if the necessary variable is set to true.
			 */
			if (isNewSearch)
				page = 1;

			int startingRecordIndex = (page - 1) * PAGE_LENGTH;

			Page<AnciOfferCriteriaTO> resultsPage = ModuleServiceLocator.getAnciOfferBD().searchAnciOfferCriterias(
					searchCriteria, startingRecordIndex, PAGE_LENGTH);

			anciOfferCriterias = resultsPage.getPageData();

			totalRecords = resultsPage.getTotalNoOfRecords();
			totalPages = totalRecords % PAGE_LENGTH > 0 ? (totalRecords / PAGE_LENGTH) + 1 : (totalRecords / PAGE_LENGTH);
		} catch (Exception x) {
			success = false;
			messageTxt = "Failed to search anci offer criterias.";
			log.error(messageTxt, x);
		}
		return S2Constants.Result.SUCCESS;
	}

	public String update() {
		try {
			checkPrivileges();
			setTranslationData();
			TrackInfoDTO trackingInfo = getTrackInfo();
			UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();

			if (!checkForExactMatchingCrteria(anciOffer)) {
				ModuleServiceLocator.getAnciOfferBD().updateAnciOfferCriteria(anciOffer, trackingInfo, userPrincipal, false);
				messageTxt = "Successfully updated anci offer criteria";
			} else {
				ModuleServiceLocator.getAnciOfferBD().updateAnciOfferCriteria(anciOffer, trackingInfo, userPrincipal, true);
				messageTxt = "An Active criteria with the exact same critereons exist. Only name and description details was saved.";
				success = true;
			}
		} catch (Exception x) {
			success = false;
			messageTxt = "Failed to update anci offer criteria";
			log.error(messageTxt, x);
		}
		return S2Constants.Result.SUCCESS;
	}

	public String create() {
		try {
			checkPrivileges();
			setTranslationData();
			TrackInfoDTO trackingInfo = getTrackInfo();
			UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();

			if (!checkForExactMatchingCrteria(anciOffer)) {
				ModuleServiceLocator.getAnciOfferBD().addAnciOfferCriteria(anciOffer, trackingInfo, userPrincipal);
				messageTxt = "Successfully created anci offer criteria";
			} else {
				messageTxt = "An Active criteria with the exact same critereons exist.";
				success = false;
			}
		} catch (Exception x) {
			success = false;
			messageTxt = "Failed to create anci search criteria";
			log.error(messageTxt, x);
		}
		return S2Constants.Result.SUCCESS;
	}

	private void checkPrivileges() {
		if (!BasicRequestHandler.hasPrivilege(request, PriviledgeConstants.ALLOW_ANCI_OFFER_MANAGEMENT)) {
			log.error("Unauthorized operation:" + request.getUserPrincipal().getName() + ":"
					+ PriviledgeConstants.ALLOW_TERMS_N_CONDITIONS_EDITING);
			throw new ModuleRuntimeException(WebConstants.ERR_UNAUTHORIZED_ACCESS);
		}
	}

	private boolean checkForExactMatchingCrteria(AnciOfferCriteriaTO criteria) throws ModuleException {
		return ModuleServiceLocator.getAnciOfferBD().doesAnActiveReplicateCriteriaExist(criteria);
	}

	public Collection<AnciOfferCriteriaTO> getAnciOfferCriterias() {
		return anciOfferCriterias;
	}

	public void setAnciOfferCriterias(Collection<AnciOfferCriteriaTO> anciOfferCriterias) {
		this.anciOfferCriterias = anciOfferCriterias;
	}

	public AnciOfferCriteriaSearchTO getSearchCriteria() {
		return searchCriteria;
	}

	public void setSearchCriteria(AnciOfferCriteriaSearchTO searchCriteria) {
		this.searchCriteria = searchCriteria;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(int totalRecords) {
		this.totalRecords = totalRecords;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setNewSearch(boolean isNewSearch) {
		this.isNewSearch = isNewSearch;
	}

	public String getAnciOffer(String anciOffer) {
		return anciOffer;
	}

	public void setAnciOffer(String anciOffer) throws JSONException, ParseException {
		this.anciOffer = fromJSONToCriteria(anciOffer);
	}

	/**
	 * Parses the JSON string to the necessary java object.
	 * 
	 * @param jsonString
	 *            JSON String with the Anci offer criteria's data.
	 * @return The parsed {@link AnciOfferCriteriaTO} instance.
	 */
	private AnciOfferCriteriaTO fromJSONToCriteria(String jsonString) throws JSONException, ParseException {
		AnciOfferCriteriaTO criteria = new AnciOfferCriteriaTO();

		@SuppressWarnings("unchecked")
		Map<String, Object> parsedDataMap = (Map<String, Object>) JSONUtil.deserialize(jsonString);

		SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyy");

		// Mandatory, should be checked from the UI
		criteria.setAnciTemplateType((String) parsedDataMap.get("anciTemplateType"));
		criteria.setApplicableForOneWay((Boolean) parsedDataMap.get("applicableForOneWay"));
		criteria.setApplicableForReturn((Boolean) parsedDataMap.get("applicableForReturn"));
		criteria.setEligibleForOneWay((Boolean) parsedDataMap.get("eligibleForOneWay"));
		criteria.setEligibleForReturn((Boolean) parsedDataMap.get("eligibleForReturn"));
		criteria.setActive((Boolean) parsedDataMap.get("active"));
		criteria.setTemplateID(Integer.parseInt((String) parsedDataMap.get("templateID")));

		criteria.setApplicableFrom(dateFormatter.parse((String) parsedDataMap.get("applicableFrom")));
		criteria.setApplicableTo(dateFormatter.parse((String) parsedDataMap.get("applicableTo")));

		criteria.setVersion(parsedDataMap.get("version") == null ? null : Long.parseLong(String.valueOf(parsedDataMap
				.get("version"))));

		criteria.setEligiblePaxCounts(this.<String> getSet(parsedDataMap.get("eligiblePaxCounts")));

		criteria.setBookingClasses(this.<String> getSet(parsedDataMap.get("bookingClasses")));

		criteria.setApplyToFlexiOnly((Boolean) parsedDataMap.get("applyToFlexi"));

		criteria.setId(parsedDataMap.get("id") == null ? null : Integer.parseInt(String.valueOf(parsedDataMap.get("id"))));

		criteria.setLccs(this.<String> getSet(parsedDataMap.get("lccs")));

		criteria.setOnds(this.<String> getSet(parsedDataMap.get("onds")));

		criteria.setApplicableOutboundDaysOfTheWeek(getIntegerSet(parsedDataMap.get("applicableOutboundDaysOfTheWeek")));

		criteria.setApplicableInboundDaysOfTheWeek(getIntegerSet(parsedDataMap.get("applicableInboundDaysOfTheWeek")));

		return criteria;
	}

	@SuppressWarnings("unchecked")
	private <T> Set<T> getSet(Object jsonObj) {
		Set<T> tmpSet = new HashSet<T>();
		if (jsonObj != null) {
			tmpSet.addAll((Collection<? extends T>) jsonObj);
		}
		return tmpSet;
	}

	private Set<Integer> getIntegerSet(Object jsonObj) {
		Set<Integer> intSet = new HashSet<Integer>();
		for (String str : this.<String> getSet(jsonObj)) {
			intSet.add(Integer.parseInt(str));
		}
		return intSet;
	}

	private void setTranslationData() {
		anciOffer.setDescriptionTranslations(descriptionTranslations);
		anciOffer.setNameTranslations(nameTranslations);
	}

	/**
	 * JSON data passed to this side is set as empty strings sometimes when it should be null. This method will replace
	 * "" with null.
	 */
	private void setEmptySearchParamsToNull() {
		if ("".equals(searchCriteria.getFareBasisCode()))
			searchCriteria.setFareBasisCode(null);
		if ("".equals(searchCriteria.getBookingClass()))
			searchCriteria.setBookingClass(null);
		if ("".equals(searchCriteria.getLcc()))
			searchCriteria.setLcc(null);
		if ("".equals(searchCriteria.getOND()))
			searchCriteria.setOND(null);
	}

	/*
	 * Weird error when getter is not of the same type as the setter. struts error?
	 */
	public String getNameTranslations() {
		return "";
	}

	@SuppressWarnings("unchecked")
	public void setNameTranslations(String nameTranslations) throws JSONException {
		this.nameTranslations = (Map<String, String>) JSONUtil.deserialize(nameTranslations);
	}

	/*
	 * Weird error when getter is not of the same type as the setter. struts error?
	 */
	public String getDescriptionTranslations() {
		return "";
	}

	@SuppressWarnings("unchecked")
	public void setDescriptionTranslations(String descriptionTranslations) throws JSONException {
		this.descriptionTranslations = (Map<String, String>) JSONUtil.deserialize(descriptionTranslations);
	}
}
