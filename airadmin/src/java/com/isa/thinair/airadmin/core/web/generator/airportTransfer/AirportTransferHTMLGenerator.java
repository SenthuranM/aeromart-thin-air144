package com.isa.thinair.airadmin.core.web.generator.airportTransfer;

import java.util.Collection;
import java.util.Iterator;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.commons.api.exception.ModuleException;

public class AirportTransferHTMLGenerator {
	
	
	/**
	 * Creates Booking class Multiselect Box
	 * @param cabinClassCode TODO
	 * @param isFirstTime TODO

	 * 
	 * @return String created MultiSelect Box Array
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final static String createBookingClassesHtml(String cabinClassCode, boolean isFirstTime) throws ModuleException {
		
		Collection<BookingClass> bookingClasses = null;
		StringBuffer sb = new StringBuffer("var bookingClasses = new Array();");
		sb.append("var methods = new Array();");
		
		if(isFirstTime){
			sb.append("bookingClasses[0] = methods;");
			sb.append(" var lsbc = new Listbox('lstBookingClasses', 'lstAssignedBookingClasses', 'spn2', 'lsbc');");
			sb.append("lsbc.group1 = bookingClasses[0];");
			sb.append("lsbc.height = '100px'; lsbc.width = '170px';");
			sb.append("lsbc.headingLeft = '&nbsp;&nbsp;Booking Classes';");
			sb.append("lsbc.headingRight = '&nbsp;&nbsp;Selected Booking Classes';");
			sb.append("lsbc.drawListBox();");
			return sb.toString();
		}

		bookingClasses = ModuleServiceLocator.getBookingClassBD().getBookingClasses();
		int tempInt = 0;
		BookingClass bookingClass = null;

		Iterator<BookingClass> rolesIter = bookingClasses.iterator();
		while (rolesIter.hasNext()) {

			bookingClass = rolesIter.next();
			if(cabinClassCode == null || cabinClassCode.equals("ANY")){
				sb.append("methods[" + tempInt + "] = new Array('" + bookingClass.getBookingCode() + "','"
						+ bookingClass.getBookingCode() + "'); ");
				tempInt++;
			}else{
				if(cabinClassCode.equals(bookingClass.getCabinClassCode())){
					sb.append("methods[" + tempInt + "] = new Array('" + bookingClass.getBookingCode() + "','"
							+ bookingClass.getBookingCode() + "'); ");
					tempInt++;
				}
			}
			
		}

		sb.append("bookingClasses[0] = methods;");
		sb.append(" lsbc = new Listbox('lstBookingClasses', 'lstAssignedBookingClasses', 'spn2', 'lsbc');");
		sb.append("lsbc.group1 = bookingClasses[0];");
		sb.append("lsbc.height = '100px'; lsbc.width = '170px';");
		sb.append("lsbc.headingLeft = '&nbsp;&nbsp;Booking Classes';");
		sb.append("lsbc.headingRight = '&nbsp;&nbsp;Assigned Booking Classes';");
		sb.append("lsbc.drawListBox();");
		return sb.toString();
	}
	
}
