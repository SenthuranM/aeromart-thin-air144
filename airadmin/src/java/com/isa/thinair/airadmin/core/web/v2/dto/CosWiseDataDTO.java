package com.isa.thinair.airadmin.core.web.v2.dto;

import java.io.Serializable;

public class CosWiseDataDTO implements Serializable {

	private static final long serialVersionUID = 6269107570276711685L;
	private String cosClass;
	private Integer cnfCnt;
	private Integer ohdCnt;
	private Integer infCnfCnt;
	private Integer infOhdCnt;

	public String getCosClass() {
		return cosClass;
	}

	public Integer getCnfCnt() {
		return cnfCnt;
	}

	public Integer getOhdCnt() {
		return ohdCnt;
	}

	public Integer getInfCnfCnt() {
		return infCnfCnt;
	}

	public Integer getInfOhdCnt() {
		return infOhdCnt;
	}

	public void setCosClass(String cosClass) {
		this.cosClass = cosClass;
	}

	public void setCnfCnt(Integer cnfCnt) {
		this.cnfCnt = cnfCnt;
	}

	public void setOhdCnt(Integer ohdCnt) {
		this.ohdCnt = ohdCnt;
	}

	public void setInfCnfCnt(Integer infCnfCnt) {
		this.infCnfCnt = infCnfCnt;
	}

	public void setInfOhdCnt(Integer infOhdCnt) {
		this.infOhdCnt = infOhdCnt;
	}

}
