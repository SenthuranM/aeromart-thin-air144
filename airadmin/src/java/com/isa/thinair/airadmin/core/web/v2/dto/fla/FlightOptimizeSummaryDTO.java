package com.isa.thinair.airadmin.core.web.v2.dto.fla;

import java.util.Collection;

public class FlightOptimizeSummaryDTO {

	private String allocatedAdults;
	private String allocatedInfants;
	private String segmentCode;
	private String cabinClass;
	private String soldAdultsInfants;
	private String soldInfants;
	private String overSellSeats;
	private String curtailedSeats;
	private String availableAdultsInfants;
	private String onholdAdultsInfants;
	private int fccInventoryID;
	private Integer fccsInventoryID;
	private long version;
	private Collection<FlightOptimizeDetailDTO> bookingClassDetails;

	public String getAllocatedAdults() {
		return allocatedAdults;
	}

	public void setAllocatedAdults(String allocatedAdults) {
		this.allocatedAdults = allocatedAdults;
	}

	public String getAllocatedInfants() {
		return allocatedInfants;
	}

	public void setAllocatedInfants(String allocatedInfants) {
		this.allocatedInfants = allocatedInfants;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public String getCabinClass() {
		return cabinClass;
	}

	public void setCabinClass(String cabinClass) {
		this.cabinClass = cabinClass;
	}

	public String getSoldInfants() {
		return soldInfants;
	}

	public void setSoldInfants(String soldInfants) {
		this.soldInfants = soldInfants;
	}

	public String getOverSellSeats() {
		return overSellSeats;
	}

	public void setOverSellSeats(String overSellSeats) {
		this.overSellSeats = overSellSeats;
	}

	public String getCurtailedSeats() {
		return curtailedSeats;
	}

	public void setCurtailedSeats(String curtailedSeats) {
		this.curtailedSeats = curtailedSeats;
	}

	public Collection<FlightOptimizeDetailDTO> getBookingClassDetails() {
		return bookingClassDetails;
	}

	public void setBookingClassDetails(Collection<FlightOptimizeDetailDTO> bookingClassDetails) {
		this.bookingClassDetails = bookingClassDetails;
	}

	public String getSoldAdultsInfants() {
		return soldAdultsInfants;
	}

	public void setSoldAdultsInfants(String soldAdultsInfants) {
		this.soldAdultsInfants = soldAdultsInfants;
	}

	public String getAvailableAdultsInfants() {
		return availableAdultsInfants;
	}

	public void setAvailableAdultsInfants(String availableAdultsInfants) {
		this.availableAdultsInfants = availableAdultsInfants;
	}

	public String getOnholdAdultsInfants() {
		return onholdAdultsInfants;
	}

	public void setOnholdAdultsInfants(String onholdAdultsInfants) {
		this.onholdAdultsInfants = onholdAdultsInfants;
	}

	public int getFccInventoryID() {
		return fccInventoryID;
	}

	public void setFccInventoryID(int fccInventoryID) {
		this.fccInventoryID = fccInventoryID;
	}

	public Integer getFccsInventoryID() {
		return fccsInventoryID;
	}

	public void setFccsInventoryID(Integer fccsInventoryID) {
		this.fccsInventoryID = fccsInventoryID;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

}
