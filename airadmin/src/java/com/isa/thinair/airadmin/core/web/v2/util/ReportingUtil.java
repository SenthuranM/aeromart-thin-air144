package com.isa.thinair.airadmin.core.web.v2.util;

/**
 * Reporting Constants for V2
 * 
 * @author Navod Ediriweera
 * @since 19 Aug 2010
 */
public class ReportingUtil {

	public interface ReportingLocations {
		public final static String image_loc = "../../images/";
	}
}
