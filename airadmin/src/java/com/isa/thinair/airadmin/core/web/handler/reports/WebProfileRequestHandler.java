/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airadmin.core.web.handler.reports;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author Chamindap
 * 
 */
public class WebProfileRequestHandler extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(WebProfileRequestHandler.class);

	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	/**
	 * @param request
	 * @return
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {

		String strHdnMode = request.getParameter("hdnMode");
		String forward = WebConstants.ACTION_FORWARD_SUCCESS;

		try {
			setDisplayData(request);
			log.debug("CustomerTravelHistoryRequestHandler success");
		} catch (Exception e) {
			forward = AdminStrutsConstants.AdminAction.ERROR;
			log.error("CustomerTravelHistoryRequestHandler execute()" + e.getMessage());
		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				log.error("CustomerTravelHistoryRH setReportView Success");
				return null;
			} else {
				log.error("CustomerTravelHistoryRH setReportView not selected");
			}

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("CustomerTravelHistoryRequestHandler setReportView Failed " + e.getMessageString());
		}
		return forward;

	}

	/**
	 * 
	 * @param request
	 * @throws Exception
	 */
	public static void setDisplayData(HttpServletRequest request) throws Exception {
		setClientErrors(request);
		setAirportComboList(request);
		setNationalityList(request);
		setCountryResidenceList(request);
		setLiveStatus(request);
		setReportingPeriod(request);
		setSalesChannelCode(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);
	}

	private static void setSalesChannelCode(HttpServletRequest request) throws Exception {
		StringBuffer sb = new StringBuffer();
		sb.append("<option value='" + SalesChannelsUtil.SALES_CHANNEL_WEB + "'>" + SalesChannelsUtil.SALES_CHANNEL_WEB_KEY
				+ "</option>");
		sb.append("<option value='" + SalesChannelsUtil.SALES_CHANNEL_IOS + "'>" + SalesChannelsUtil.SALES_CHANNEL_IOS_KEY
				+ "</option>");
		sb.append("<option value='" + SalesChannelsUtil.SALES_CHANNEL_ANDROID + "'>" + SalesChannelsUtil.SALES_CHANNEL_ANDROID_KEY
				+ "</option>");
		sb.append("<option value='" + SalesChannelsUtil.SALES_CHANNEL_DNATA_AGENCIES + "'>"
				+ SalesChannelsUtil.SALES_CHANNEL_APIAGENCIES_KEY + "</option>");
		request.setAttribute(WebConstants.REQ_SALES_CHANNEL_CODES, sb.toString());
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setAirportComboList(HttpServletRequest request) throws ModuleException {
		String strStation = SelectListGenerator.createActiveAirportCodeList();
		request.setAttribute(WebConstants.REQ_STATION_LIST, strStation);
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setNationalityList(HttpServletRequest request) throws ModuleException {
		String strNationality = SelectListGenerator.createNationalityList();
		request.setAttribute(WebConstants.REQ_NATIONALITY_LIST, strNationality);
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setCountryResidenceList(HttpServletRequest request) throws ModuleException {
		String strCountry = SelectListGenerator.createCountryList();
		request.setAttribute(WebConstants.REQ_COUNTRY_LIST, strCountry);
	}

	/**
	 * 
	 * @param request
	 */
	private static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the report medium to the request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleExceptions
	 */
	private static void setLiveStatus(HttpServletRequest request) throws ModuleException {
		String strLive = request.getParameter(WebConstants.REP_HDN_LIVE);
		if (strLive != null)
			request.setAttribute(WebConstants.REP_SET_LIVE, strLive);

	}

	/**
	 * Setting the reporting period to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setReportingPeriod(HttpServletRequest request) throws ModuleException {
		int period = new Integer(globalConfig.getBizParam(SystemParamKeys.REPORTING_PERIOD)).intValue();
		request.setAttribute(WebConstants.REP_RPT_START_DAY, ReportsHTMLGenerator.getReportingPeriod(period));
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {
		ResultSet resultSet = null;
		String nationality = request.getParameter("selNationality");
		String countryOfRes = request.getParameter("selCountryOfRes");
		String value = request.getParameter("radReportOption");
		String strNationality = request.getParameter("selNationality");
		String strCountry = request.getParameter("selCountryOfRes");
		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String sector = request.getParameter("chkSector");
		String sectorFrom = request.getParameter("selSectorFrom");
		String sectorTo = request.getParameter("selSectorTo");
		String bookingStatus = request.getParameter("chkBookingStatus");
		String selBookingStatus = request.getParameter("selBookingStatus");
		String salesChannelCode = request.getParameter("selSalesChannelCode");

		boolean isGetAllPassengers = new Boolean(request.getParameter("hdnRequestAllPassengers"));

		String strLogo = AppSysParamsUtil.getReportLogo(false);
		String image = "../../images/" +  AppSysParamsUtil.getReportLogo(true);
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String id = "UC_REPM_026";
		String reportTemplate = "WebProfileReport.jasper";
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);

		try {
			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));

			ReportsSearchCriteria search = new ReportsSearchCriteria();

			if (salesChannelCode != null && !salesChannelCode.equals("")) {
				search.setSalesChannelCode(new Integer(salesChannelCode));
			}

			if (sector != null && !sector.equals("")) {
				if (sector.equals("on")) {
					search.setBySector(true);
				}
			}
			if (bookingStatus != null && !bookingStatus.equals("")) {
				if (bookingStatus.equals("on")) {
					search.setByBookingStatus(true);
				}
			}
			if (strNationality != null && !strNationality.trim().equals("")) {
				search.setNationality(nationality);
				search.setByNationality(true);
			}
			if (strCountry != null && !strCountry.trim().equals("")) {
				search.setCountryOfResidence(countryOfRes);
				search.setByCountryOfResidence(true);
			}
			if (fromDate != null && !fromDate.equals("")) {
				search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate).concat(" 00:00:00"));
			}
			if (toDate != null && !toDate.equals("")) {
				search.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate).concat(" 23:59:59"));
			}
			if (sectorFrom != null && !sectorFrom.equals("")) {
				search.setSectorFrom(sectorFrom);
			}

			if (sectorTo != null && !sectorTo.equals("")) {
				search.setSectorTo(sectorTo);
			}

			if (selBookingStatus != null && !selBookingStatus.equals("")) {
				search.setBookingStatus(selBookingStatus);
			}

			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}

			search.setRequestAllPassengers(isGetAllPassengers);
			resultSet = ModuleServiceLocator.getDataExtractionBD().getWebProfilesData(search);

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("FROM_DATE", fromDate);
			parameters.put("TO_DATE", toDate);
			parameters.put("SECTOR", sector);
			parameters.put("SECTOR_FROM", sectorFrom);
			parameters.put("SECTOR_TO", sectorTo);
			parameters.put("OPTION", value);
			parameters.put("ID", id);
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);

			// To provide Report Format Options
			String reportNumFormat = request.getParameter("radRptNumFormat");
			ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

			if (value.trim().equals("HTML")) {
				parameters.put("IMG", image);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null, null,
						response);
			} else if (value.trim().equals("PDF")) {
				response.reset();
				response.addHeader("Content-Disposition", "filename=CustomeProfileReport.pdf");
				image = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG", image);
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("EXCEL")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=CustomeProfileReport.xls");
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("CSV")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=CustomeProfileReport.csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);

			}
		} catch (Exception e) {
			log.error(e);
		}
	}

}