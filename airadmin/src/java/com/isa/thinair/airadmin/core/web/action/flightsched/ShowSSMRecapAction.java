package com.isa.thinair.airadmin.core.web.action.flightsched;



import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airmaster.api.model.SSMRecap;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Results({ @Result(name = AdminStrutsConstants.AdminAction.SUCCESS, value = AdminStrutsConstants.AdminJSP.GDS_SSM_RECAP),
		@Result(name = AdminStrutsConstants.AdminAction.ERROR, value = AdminStrutsConstants.AdminJSP.ERROR_JSP) })
public class ShowSSMRecapAction extends BaseRequestAwareAction {
	
	private static Log log = LogFactory.getLog(ShowSSMRecapAction.class);
	
	
	private String hdnMode;
	private String strErrorMEssage ;
	private String gdsCode;
	private String gdsID;
	private String scheduledDate;
	private String scheduledTime;
	private String selectedScheduleRange;
	private String scheduleStartDate;
	private String scheduleEndDate;
	private String emailAddress;
	

	public String execute()  {
		
		try {
			String forward = null;
			
			if(hdnMode != null){
				if(hdnMode.equals(WebConstants.ACTION_VIEW)){
					forward = AdminStrutsConstants.AdminAction.SUCCESS;
					return forward;
				} else if (hdnMode.equals(WebConstants.ACTION_UPDATE)){
	             SSMRecap ssmRecap = getValidatedSSMRecap();
	             log.error(ssmRecap.getGdsCode()+" "+ssmRecap.getScheduledTime());
	             forward = AdminStrutsConstants.AdminAction.SUCCESS;
				} 
				
			}	
			return forward;
		} catch (Exception e) {
			request.setAttribute(WebConstants.REQ_MESSAGE, e.getMessage());
			return AdminStrutsConstants.AdminAction.SUCCESS;
		}
		
	}
	
	private SSMRecap getValidatedSSMRecap() throws Exception {
		
		SSMRecap ssmRecap = new SSMRecap();
		
		if (gdsCode != null && gdsCode != "") {
			ssmRecap.setGdsCode(gdsCode);

		} else {
			throw new Exception("Ivalid GDS Code");
		}
		if (gdsID != null && gdsID != "") {
			ssmRecap.setGdsID(Integer.parseInt(gdsID));
		} else {
			throw new Exception("Ivalid GDS ID");
		}
		
		if (scheduledDate != null && scheduledDate != "" && scheduledTime != null && scheduledTime != "" ) {			
			Date scheduleDate = composeScheduledDateTime(scheduledDate,scheduledTime);	
			ssmRecap.setScheduledTime(scheduleDate);
		} else {
			throw new Exception("Invalid Scheduled Date");
		}
				
		if (selectedScheduleRange != null && selectedScheduleRange != "") {
			if(selectedScheduleRange.equals("selected")){
				ssmRecap.setSelectedRange(true);
				if (scheduleStartDate != null && scheduleStartDate != "") {				
					ssmRecap.setScheduleStartDate(parseToDate(scheduleStartDate));					
                
				} else {
					throw new Exception("Invalid Flight Schedule Start Date");
				}
				
				if (scheduleEndDate != null && scheduleEndDate != "") {
					ssmRecap.setScheduleEndDate(parseToDate(scheduleEndDate));

				} else {
					throw new Exception("Invalid Flight Schedule End Date");
				}
				
			} else if(selectedScheduleRange.equals("all")){
				ssmRecap.setSelectedRange(true);
			}

		} else {
			throw new Exception("Invalid Flight Schedule Range");
		}
		
		if (emailAddress != null && emailAddress != "") {
			ssmRecap.setEmailAddress(emailAddress);
		} else {
			throw new Exception("Invalid Email Address");
		}
		
		return ssmRecap;

	}
	
	
	public String getGdsCode() {
		return gdsCode;
	}


	public void setGdsCode(String gdsCode) {
		this.gdsCode = gdsCode;
	}


	public String getGdsID() {
		return gdsID;
	}


	public void setGdsID(String gdsID) {
		this.gdsID = gdsID;
	}


	public String getScheduledDate() {
		return scheduledDate;
	}


	public void setScheduledDate(String scheduledDate) {
		this.scheduledDate = scheduledDate;
	}


	public String getScheduledTime() {
		return scheduledTime;
	}


	public void setScheduledTime(String scheduledTime) {
		this.scheduledTime = scheduledTime;
	}


	public String getSelectedScheduleRange() {
		return selectedScheduleRange;
	}


	public void setSelectedScheduleRange(String selectedScheduleRange) {
		this.selectedScheduleRange = selectedScheduleRange;
	}


	public String getScheduleStartDate() {
		return scheduleStartDate;
	}


	public void setScheduleStartDate(String scheduleStartDate) {
		this.scheduleStartDate = scheduleStartDate;
	}


	public String getScheduleEndDate() {
		return scheduleEndDate;
	}


	public void setScheduleEndDate(String scheduleEndDate) {
		this.scheduleEndDate = scheduleEndDate;
	}
	
	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	
	public String getStrErrorMEssage() {
		return strErrorMEssage;
	}

	public void setStrErrorMEssage(String strErrorMEssage) {
		this.strErrorMEssage = strErrorMEssage;
	}
	
	public String getHdnMode() {
		return hdnMode;
	}

	public void setHdnMode(String hdnMode) {
		this.hdnMode = hdnMode;
	}
	
	private  Date composeScheduledDateTime(String strDate, String strTime) throws Exception {
		Date date = null;
		Date time = null;
		SimpleDateFormat dateFormatDate = null;
		SimpleDateFormat dateFormatTime = new SimpleDateFormat("HH:mm");
		
		if (strDate.indexOf('-') != -1)
			dateFormatDate = new SimpleDateFormat("dd-MM-yy");
		if (strDate.indexOf('/') != -1)
			dateFormatDate = new SimpleDateFormat("dd/MM/yy");
		try {
			date = dateFormatDate.parse(strDate);
			time = dateFormatTime.parse(strTime);
		} catch (Exception e) {
			log.error(e);
			throw new Exception("Invalid Scheduled Date");			
		}		
		return CalendarUtil.getTimeAddedDate(date, time);	
	
	}
	
	private  Date parseToDate(String strDate) throws Exception {
		Date date = null;
		SimpleDateFormat dateFormat = null;

		if (strDate.indexOf('-') != -1)
			dateFormat = new SimpleDateFormat("dd-MM-yy");
		if (strDate.indexOf('/') != -1)
			dateFormat = new SimpleDateFormat("dd/MM/yy");
		try {
			date = dateFormat.parse(strDate);
		} catch (Exception e) {
			log.error(e);
			throw new Exception("Invalid Scheduled Date");			
		}

		return date;
	}

}
