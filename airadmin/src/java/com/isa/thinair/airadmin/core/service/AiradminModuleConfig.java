package com.isa.thinair.airadmin.core.service;

import org.springframework.beans.factory.InitializingBean;

import com.isa.thinair.platform.api.DefaultModuleConfig;

/**
 * @isa.module.config-bean
 * @author Srikantha
 * 
 */
public class AiradminModuleConfig extends DefaultModuleConfig implements InitializingBean{
	private String smtpHost;

	private String port;

	private String sslPort;

	private String nonSecureProtocol;

	private String secureProtocol;

	private String revenueReportPath;

	private String revenueReportFowardSalesPath;

	private String menuConfigFile;

	private String enableAudit;

	private String enableFTPRevenue;

	private String revFTPPath;

	private String rptFTPServer;

	private String rptFTPUserName;

	private String rptFTPPassword;

	private String mealImages;

	private String allowProcessPfsBeforeFlightDeparture;

	private String includeAdvanceCCTransactionDetails;

	private String includeAdvancedNameChangeDetails;

	private String itineraryAdvertisementImages;

	private String agentLimitForReporting;

	private String secureUrl;

	private boolean allowHttpsOnly = false;

	private String skipReportPeriod;

	private String schedulePromoInfoRpt;

	private boolean useSecureLogin = true;
	
	private int itineraryAdvertisementImagesMaxHeight;
	
	private int itineraryAdvertisementImagesMinHeight;
	
	private int itineraryAdvertisementImagesMaxWidth;
	
	private int itineraryAdvertisementImagesMinWidth;
	
	private long itineraryAdvertisementImagesSize;

	/**
	 * @isa.module.config-method default="10.2.240.2"
	 */
	public String getSmtpHost() {
		return smtpHost;
	}

	public void setSmtpHost(String smtpHost) {
		this.smtpHost = smtpHost;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getSslPort() {
		return sslPort;
	}

	public void setSslPort(String sslPort) {
		this.sslPort = sslPort;
	}

	public String getNonSecureProtocol() {
		return nonSecureProtocol;
	}

	public void setNonSecureProtocol(String nonSecureProtocol) {
		this.nonSecureProtocol = nonSecureProtocol;
	}

	public String getSecureProtocol() {
		return secureProtocol;
	}

	public void setSecureProtocol(String secureProtocol) {
		this.secureProtocol = secureProtocol;
	}

	public String getRevenueReportPath() {
		return revenueReportPath;
	}

	public void setRevenueReportPath(String revenueReportPath) {
		this.revenueReportPath = revenueReportPath;
	}

	/**
	 * @return Returns the menuConfigFile.
	 */
	public String getMenuConfigFile() {
		return menuConfigFile;
	}

	/**
	 * @param menuConfigFile
	 *            The menuConfigFile to set.
	 */
	public void setMenuConfigFile(String menuConfigFile) {
		this.menuConfigFile = menuConfigFile;
	}

	/**
	 * @return Returns the enableAudit.
	 */
	public String getEnableAudit() {
		return enableAudit;
	}

	/**
	 * @param enableAudit
	 *            The enableAudit to set.
	 */
	public void setEnableAudit(String enableAudit) {
		this.enableAudit = enableAudit;
	}

	public String getRevFTPPath() {
		return revFTPPath;
	}

	public void setRevFTPPath(String revFTPPath) {
		this.revFTPPath = revFTPPath;
	}

	/**
	 * @return Returns the enableFTPRevenue.
	 */
	public String getEnableFTPRevenue() {
		return enableFTPRevenue;
	}

	/**
	 * @param enableFTPRevenue
	 *            The enableFTPRevenue to set.
	 */
	public void setEnableFTPRevenue(String enableFTPRevenue) {
		this.enableFTPRevenue = enableFTPRevenue;
	}

	/**
	 * @return Returns the rptFTPPassword.
	 */
	public String getRptFTPPassword() {
		return rptFTPPassword;
	}

	/**
	 * @param rptFTPPassword
	 *            The rptFTPPassword to set.
	 */
	public void setRptFTPPassword(String rptFTPPassword) {
		this.rptFTPPassword = rptFTPPassword;
	}

	/**
	 * @return Returns the rptFTPServer.
	 */
	public String getRptFTPServer() {
		return rptFTPServer;
	}

	/**
	 * @param rptFTPServer
	 *            The rptFTPServer to set.
	 */
	public void setRptFTPServer(String rptFTPServer) {
		this.rptFTPServer = rptFTPServer;
	}

	/**
	 * @return Returns the rptFTPUserName.
	 */
	public String getRptFTPUserName() {
		return rptFTPUserName;
	}

	/**
	 * @param rptFTPUserName
	 *            The rptFTPUserName to set.
	 */
	public void setRptFTPUserName(String rptFTPUserName) {
		this.rptFTPUserName = rptFTPUserName;
	}

	public String getRevenueReportFowardSalesPath() {
		return revenueReportFowardSalesPath;
	}

	public void setRevenueReportFowardSalesPath(String revenueReportFowardSalesPath) {
		this.revenueReportFowardSalesPath = revenueReportFowardSalesPath;
	}

	/**
	 * Gets the path to store the Meal Image
	 * 
	 * @return meal image path
	 */
	public String getMealImages() {
		return mealImages;
	}

	public void setMealImages(String mealImages) {
		this.mealImages = mealImages;
	}

	/**
	 * Gets the path to store the Advertisement Image
	 * 
	 * @return advertisement image path
	 */

	public String getItineraryAdvertisementImages() {
		return itineraryAdvertisementImages;
	}

	public void setItineraryAdvertisementImages(String itineraryAdvertisementImages) {
		this.itineraryAdvertisementImages = itineraryAdvertisementImages;
	}

	/**
	 * Gets the status of Allowing Process Pfs Before Flight Departure
	 * 
	 * @return allowProcessPfsBeforeFlightDeparture
	 */
	public String getAllowProcessPfsBeforeFlightDeparture() {
		return allowProcessPfsBeforeFlightDeparture;
	}

	public void setAllowProcessPfsBeforeFlightDeparture(String allowProcessPfsBeforeFlightDeparture) {
		this.allowProcessPfsBeforeFlightDeparture = allowProcessPfsBeforeFlightDeparture;
	}

	/**
	 * @return the includeAdvanceCCTransactionDetails
	 */
	public String getIncludeAdvanceCCTransactionDetails() {
		return includeAdvanceCCTransactionDetails;
	}

	/**
	 * @param includeAdvanceCCTransactionDetails
	 *            the includeAdvanceCCTransactionDetails to set
	 */
	public void setIncludeAdvanceCCTransactionDetails(String includeAdvanceCCTransactionDetails) {
		this.includeAdvanceCCTransactionDetails = includeAdvanceCCTransactionDetails;
	}

	public String getIncludeAdvancedNameChangeDetails() {
		return includeAdvancedNameChangeDetails;
	}

	public void setIncludeAdvancedNameChangeDetails(String includeAdvancedNameChangeDetails) {
		this.includeAdvancedNameChangeDetails = includeAdvancedNameChangeDetails;
	}

	public String getAgentLimitForReporting() {
		return agentLimitForReporting;
	}

	public void setAgentLimitForReporting(String agentLimitForReporting) {
		this.agentLimitForReporting = agentLimitForReporting;
	}

	public void setSecureUrl(String secureUrl) {
		this.secureUrl = secureUrl;
	}

	public String getSecureUrl() {
		return this.secureUrl;
	}

	public boolean isAllowHttpsOnly() {
		return allowHttpsOnly;
	}

	public void setAllowHttpsOnly(boolean allowHttpsOnly) {
		this.allowHttpsOnly = allowHttpsOnly;
	}

	public String getSkipReportPeriod() {
		return skipReportPeriod;
	}

	public void setSkipReportPeriod(String skipReportPeriod) {
		this.skipReportPeriod = skipReportPeriod;
	}

	public String getSchedulePromoInfoRpt() {
		return schedulePromoInfoRpt;
	}

	public void setSchedulePromoInfoRpt(String schedulePromoInfoRpt) {
		this.schedulePromoInfoRpt = schedulePromoInfoRpt;
	}

	public boolean isUseSecureLogin() {
		return useSecureLogin;
	}

	public void setUseSecureLogin(boolean useSecureLogin) {
		this.useSecureLogin = useSecureLogin;
	}

	public int getItineraryAdvertisementImagesMaxHeight() {
		return itineraryAdvertisementImagesMaxHeight;
	}

	public void setItineraryAdvertisementImagesMaxHeight(int itineraryAdvertisementImagesMaxHeight) {
		this.itineraryAdvertisementImagesMaxHeight = itineraryAdvertisementImagesMaxHeight;
	}

	public int getItineraryAdvertisementImagesMinHeight() {
		return itineraryAdvertisementImagesMinHeight;
	}

	public void setItineraryAdvertisementImagesMinHeight(int itineraryAdvertisementImagesMinHeight) {
		this.itineraryAdvertisementImagesMinHeight = itineraryAdvertisementImagesMinHeight;
	}

	public int getItineraryAdvertisementImagesMaxWidth() {
		return itineraryAdvertisementImagesMaxWidth;
	}

	public void setItineraryAdvertisementImagesMaxWidth(int itineraryAdvertisementImagesMaxWidth) {
		this.itineraryAdvertisementImagesMaxWidth = itineraryAdvertisementImagesMaxWidth;
	}

	public int getItineraryAdvertisementImagesMinWidth() {
		return itineraryAdvertisementImagesMinWidth;
	}

	public void setItineraryAdvertisementImagesMinWidth(int itineraryAdvertisementImagesMinWidth) {
		this.itineraryAdvertisementImagesMinWidth = itineraryAdvertisementImagesMinWidth;
	}

	public long getItineraryAdvertisementImagesSize() {
		return itineraryAdvertisementImagesSize;
	}

	public void setItineraryAdvertisementImagesSize(long itineraryAdvertisementImagesSize) {
		this.itineraryAdvertisementImagesSize = itineraryAdvertisementImagesSize;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		if(!(this.itineraryAdvertisementImagesMaxHeight > 0 && this.itineraryAdvertisementImagesMaxWidth > 0 && this.itineraryAdvertisementImagesSize > 0 )){
			throw new Exception("Itinerary Image Height, Width and Size should be larger than zero");
		} 
		if(!(this.itineraryAdvertisementImagesMaxHeight > this.itineraryAdvertisementImagesMinHeight && this.itineraryAdvertisementImagesMaxWidth > this.itineraryAdvertisementImagesMinWidth)){
			throw new Exception("Itinerary Image Max/Min Height and Width not applicable");
		}		
	}

}