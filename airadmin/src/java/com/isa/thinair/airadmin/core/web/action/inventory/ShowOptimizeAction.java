package com.isa.thinair.airadmin.core.web.action.inventory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.handler.pricing.OptimizeRequestHandler;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Results({ @Result(name = AdminStrutsConstants.AdminAction.SUCCESS, value = AdminStrutsConstants.AdminJSP.OPTIMIZE_RESULT_JSP),
		@Result(name = AdminStrutsConstants.AdminAction.ERROR, value = AdminStrutsConstants.AdminJSP.ERROR_JSP) })
public class ShowOptimizeAction extends BaseRequestResponseAwareAction {

	private static final Log log = LogFactory.getLog(ShowOptimizeAction.class);

	public String execute() throws Exception {
		log.debug("Inside the ShowOptimizeAction - execute()");
		return OptimizeRequestHandler.execute(request, response);
	}
}
