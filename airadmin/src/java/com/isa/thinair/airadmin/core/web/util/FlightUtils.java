package com.isa.thinair.airadmin.core.web.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airschedules.api.dto.DisplayFlightDTO;
import com.isa.thinair.airschedules.api.dto.FlightAllowActionDTO;
import com.isa.thinair.airschedules.api.model.CodeShareMCFlight;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightLeg;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.commons.api.dto.FlightTypeDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;

/**
 * @author Thushara
 * 
 */

public class FlightUtils {

	private static Log log = LogFactory.getLog(FlightUtils.class);

	private static final String PARAM_VERSION = "hdnVersion";

	private static final String PARAM_SCH_STARTDATE = "txtStartDateSearch";
	private static final String PARAM_SCH_STOPDATE = "txtStopDateSearch";
	private static final String PARAM_SCH_FLIGHTNUMBER = "txtFlightNoSearch";
	private static final String PARAM_SCH_FROM = "selFromStn6";
	private static final String PARAM_SCH_TO = "selToStn6";
	private static final String PARAM_SCH_OPERATIONTYPE = "selOperationTypeSearch";
	private static final String PARAM_SCH_STATUS = "selStatusSearch";
	private static final String PARAM_SCH_RADTZ = "radTZ";

	private static final String PARAM_DEPATUREDATE = "txtDepatureDate";
	private static final String PARAM_STATUS = "selStatus";
	private static final String PARAM_OPERATIONTYPE = "selOperationType";
	private static final String PARAM_FLIGHTNUMBER = "txtFlightNo";
	private static final String PARAM_FLIGHTSTARTNUMBER = "txtFlightNoStart";
	private static final String PARAM_AIRCRAFTMODEL = "selModelNo";
	private static final String PARAM_FLIGHT_TYPE = "selFlightType";
	private static final String PARAM_GDSPUBLISHING = "hdnGDSIDs"; // add by Haider
																	// 01Sep08 GDS
																	// IDs

	private static final String PARAM_LEG1_DEPATURE = "selFromStn1";
	private static final String PARAM_LEG1_ARRIVAL = "selToStn1";
	private static final String PARAM_LEG1_DEATURETIME = "txtDepTime1";
	private static final String PARAM_LEG1_ARRIVALTIME = "txtArrTime1";
	private static final String PARAM_LEG1_DEPATUREDAY = "selDepDay1";
	private static final String PARAM_LEG1_ARRIVALDAY = "selArrDay1";

	private static final String PARAM_LEG2_DEPATURE = "selFromStn2";
	private static final String PARAM_LEG2_ARRIVAL = "selToStn2";
	private static final String PARAM_LEG2_DEATURETIME = "txtDepTime2";
	private static final String PARAM_LEG2_ARRIVALTIME = "txtArrTime2";
	private static final String PARAM_LEG2_DEPATUREDAY = "selDepDay2";
	private static final String PARAM_LEG2_ARRIVALDAY = "selArrDay2";

	private static final String PARAM_LEG3_DEPATURE = "selFromStn3";
	private static final String PARAM_LEG3_ARRIVAL = "selToStn3";
	private static final String PARAM_LEG3_DEATURETIME = "txtDepTime3";
	private static final String PARAM_LEG3_ARRIVALTIME = "txtArrTime3";
	private static final String PARAM_LEG3_DEPATUREDAY = "selDepDay3";
	private static final String PARAM_LEG3_ARRIVALDAY = "selArrDay3";

	private static final String PARAM_LEG4_DEPATURE = "selFromStn4";
	private static final String PARAM_LEG4_ARRIVAL = "selToStn4";
	private static final String PARAM_LEG4_DEATURETIME = "txtDepTime4";
	private static final String PARAM_LEG4_ARRIVALTIME = "txtArrTime4";
	private static final String PARAM_LEG4_DEPATUREDAY = "selDepDay4";
	private static final String PARAM_LEG4_ARRIVALDAY = "selArrDay4";

	private static final String PARAM_LEG5_DEPATURE = "selFromStn5";
	private static final String PARAM_LEG5_ARRIVAL = "selToStn5";
	private static final String PARAM_LEG5_DEATURETIME = "txtDepTime5";
	private static final String PARAM_LEG5_ARRIVALTIME = "txtArrTime5";
	private static final String PARAM_LEG5_DEPATUREDAY = "selDepDay5";
	private static final String PARAM_LEG5_ARRIVALDAY = "selArrDay5";

	private static final String PARAM_COPY_DATE = "hdnCopyToDay";

	private static final String PARAM_SCHEDULEID = "hdnScheduleId";
	private static final String PARAM_FLIGHTID = "hdnFlightId";
	private static final String PARAM_TIMEMODE = "hdnTimeMode";
	private static final String PARAM_MODE = "hdnMode";
	private static final String PARAM_MODEL_NO = "hdnModelNo";

	private static final String PARAM_LEG_VALIDITY = "hdnOverLapSeg";
	private static final String PARAM_VALID_SEGMENT = "hdnSegArray";
	private static final String PARAM_VALID_TERMINAL = "hdnTerminalArray";
	private static final String PARAM_VALID_SEGMET_OLAPID = "hdnOverLapSegId";
	private static final String PARAM_OVERLAPFLTID = "hdnOverlapFlightId";

	private static final String PARAM_ALLOWCOF = "hdnAllowConf";
	private static final String PARAM_ALLOWDUR = "hdnAllowDuration";
	private static final String PARAM_ALLOWMODEL = "hdnAllowModel";
	private static final String PARAM_ALLOWSEGMENTS = "hdnAllowSegment";
	private static final String PARAM_ALLOWOVERLAP = "hdnAllowOverlap";
	private static final String PARAM_GRIDROW = "hdnGridRow";
	private static final String PARAM_FROM_PAGE = "hdnFromPage";

	private static final String PARAM_FLIGHT_REMARKS = "txtRemarks";
	private static final String PARAM_MEAL_TEMPLATE = "selMealTemplate";
	private static final String PARAM_SEAT_CHARGE_TEMPLATE = "selSeatChargeTemplate";
	private static final String PARAM_USER_NORES = "txtUserNotes";
	private static final String PARAM_FLIGHT_NO = "hdnFlightNo";
	private static final String PARAM_FLIGHT_DEPARTURE_DATE = "hdnFlightDate";
	
	//CodeShare
	private static final String PARAM_CODESHARE = "hndCodeShare";
	
	private static final String PARAM_CSOCCARRIER = "hndCsOCCarrierCode";
	private static final String PARAM_CSOCFLIGHT = "hndCsOCFlightNo";

	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	/**
	 * Formats specified date with specified format
	 * 
	 * @return String the formated date
	 */
	public static String formatDate(Date utilDate, String fmt) {

		String strDate = "";
		if (utilDate != null) {
			SimpleDateFormat sdFmt = null;
			sdFmt = new SimpleDateFormat(fmt);
			strDate = sdFmt.format(utilDate);
		}

		return strDate;
	}

	/**
	 * Gets Segment String from a set
	 * 
	 * @param set
	 *            the Collection of Flight Segments
	 * @return String the segment String
	 */
	public static String getSegment(Set<FlightSegement> set) {
		String strSeg = "";
		String strSegArray = "";
		int prevLen = 0;
		FlightSegement seg = null;
		if (set != null) {
			Iterator<FlightSegement> ite = set.iterator();

			if (ite != null) {
				while (ite.hasNext()) {
					seg = (FlightSegement) ite.next();
					strSeg = seg.getSegmentCode();
					if (prevLen < strSeg.length()) {
						strSegArray = strSeg;
						prevLen = strSeg.length();
					}
				}
			}
		}
		return strSegArray;
	}

	/**
	 * Gets Terminal String from a set
	 * 
	 * @param set
	 *            the Collection of Flight Segments
	 * @return String the segment String
	 */
	public static String getTerminal(Set<FlightSegement> set) {
		String strSeg = "";
		String strArrivalTerminal = "";
		String strDepartureTerminal = "";
		StringBuffer strTerminalArray = new StringBuffer();
		FlightSegement seg = null;
		if (set != null) {
			Iterator<FlightSegement> ite = set.iterator();

			if (ite != null) {
				while (ite.hasNext()) {
					seg = (FlightSegement) ite.next();
					strSeg = seg.getSegmentCode();

					if (seg.getArrivalTerminalId() == null)
						strArrivalTerminal = "";
					else
						strArrivalTerminal = seg.getArrivalTerminalId().toString();
					if (seg.getDepartureTerminalId() == null)
						strDepartureTerminal = "";
					else
						strDepartureTerminal = seg.getDepartureTerminalId().toString();
					strTerminalArray.append("|");
					strTerminalArray.append(strSeg);
					strTerminalArray.append("^");
					strTerminalArray.append(strDepartureTerminal);
					strTerminalArray.append("^");
					strTerminalArray.append(strArrivalTerminal);

				}
			}
		}
		return strTerminalArray.toString().substring(1);
	}

	/**
	 * Makes a set using a leg set
	 * 
	 * @param legset
	 *            les set
	 * @return a hash set
	 */
	public static HashSet<FlightSegement> getSegmentsFromLegSet(Set<FlightLeg> legset) {
		HashSet<FlightSegement> segset = new HashSet<FlightSegement>();
		for (int i = 1; i < legset.size() + 1; i++) {

			Iterator<FlightLeg> legIt = legset.iterator();
			while (legIt.hasNext()) {

				FlightLeg leg1 = (FlightLeg) legIt.next();
				if (leg1.getLegNumber() == i) {

					String segmentCode = leg1.getOrigin() + "/" + leg1.getDestination();

					// create the segment
					FlightSegement seg1 = new FlightSegement();
					seg1.setSegmentCode(segmentCode);
					segset.add(seg1);

					// creating following segments
					for (int j = i + 1; j < legset.size() + 1; j++) {

						Iterator<FlightLeg> restLegIt = legset.iterator();
						while (restLegIt.hasNext()) {

							FlightLeg leg2 = (FlightLeg) restLegIt.next();
							if (leg2.getLegNumber() == j) {

								segmentCode = segmentCode + "/" + leg2.getDestination();

								// create folllowing segment
								FlightSegement seg2 = new FlightSegement();
								seg2.setSegmentCode(segmentCode);
								segset.add(seg2);

								break;
							}
						}
					}
					break;
				}
			}
		}
		return segset;
	}

	/**
	 * method to get the first leg of the given schedule leg list
	 * 
	 * @param legList
	 * @return flight schedule leg
	 */
	public static FlightLeg getFirstFlightLeg(Collection<FlightLeg> legList) {
		Iterator<FlightLeg> it = legList.iterator();
		FlightLeg leg = null;
		while (it.hasNext()) {
			leg = (FlightLeg) it.next();
			if (leg.getLegNumber() == 1)
				break;
		}
		return leg;
	}

	/**
	 * method to get last leg of the given schedule leg list
	 * 
	 * @param legList
	 * @return flight schedule leg
	 */
	public static FlightLeg getLastFlightLeg(Collection<FlightLeg> legList) {
		Iterator<FlightLeg> it = legList.iterator();
		FlightLeg leg = null;
		int legnum = 0;
		while (it.hasNext()) {
			FlightLeg currentLeg = (FlightLeg) it.next();
			if (legnum < currentLeg.getLegNumber()) {
				legnum = currentLeg.getLegNumber();
				leg = currentLeg;
			}
		}
		return leg;
	}

	/**
	 * Create a Flight Using request Data
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return Properties the property file containing the Flights data
	 */
	public static Properties getProperties(HttpServletRequest request) {
		Properties props = new Properties();

		props.setProperty(PARAM_VERSION, getNotNullString(request.getParameter(PARAM_VERSION)));

		props.setProperty(PARAM_SCH_FLIGHTNUMBER, getNotNullString(request.getParameter(PARAM_SCH_FLIGHTNUMBER)));
		props.setProperty(PARAM_SCH_FROM, getNotNullString(request.getParameter(PARAM_SCH_FROM)));
		props.setProperty(PARAM_SCH_OPERATIONTYPE, getNotNullString(request.getParameter(PARAM_SCH_OPERATIONTYPE)));
		props.setProperty(PARAM_SCH_RADTZ, getNotNullString(request.getParameter(PARAM_SCH_RADTZ)));
		props.setProperty(PARAM_SCH_STARTDATE, getNotNullString(request.getParameter(PARAM_SCH_STARTDATE)));
		props.setProperty(PARAM_SCH_STATUS, getNotNullString(request.getParameter(PARAM_SCH_STATUS)));
		props.setProperty(PARAM_SCH_STOPDATE, getNotNullString(request.getParameter(PARAM_SCH_STOPDATE)));
		props.setProperty(PARAM_SCH_TO, getNotNullString(request.getParameter(PARAM_SCH_TO)));

		props.setProperty(PARAM_DEPATUREDATE, getNotNullString(request.getParameter(PARAM_DEPATUREDATE)));
		props.setProperty(PARAM_AIRCRAFTMODEL,
				request.getParameter(PARAM_AIRCRAFTMODEL) == null
						? getNotNullString(request.getParameter(PARAM_MODEL_NO))
						: request.getParameter(PARAM_AIRCRAFTMODEL));

		props.setProperty(PARAM_GDSPUBLISHING, getNotNullString(request.getParameter(PARAM_GDSPUBLISHING)));// added by
																											// Haider
																											// 01Sep08

		props.setProperty(PARAM_FLIGHTNUMBER, getNotNullString(request.getParameter(PARAM_FLIGHTNUMBER)));
		props.setProperty(PARAM_FLIGHTSTARTNUMBER, getNotNullString(request.getParameter(PARAM_FLIGHTSTARTNUMBER)));
		props.setProperty(PARAM_OPERATIONTYPE, getNotNullString(request.getParameter(PARAM_OPERATIONTYPE)));
		props.setProperty(PARAM_STATUS, getNotNullString(request.getParameter(PARAM_STATUS)));

		props.setProperty(PARAM_SCHEDULEID, getNotNullString(request.getParameter(PARAM_SCHEDULEID)));
		props.setProperty(PARAM_FLIGHTID, getNotNullString(request.getParameter(PARAM_FLIGHTID)));
		props.setProperty(PARAM_MODE, getNotNullString(request.getParameter(PARAM_MODE)));
		props.setProperty(PARAM_TIMEMODE, getNotNullString(request.getParameter(PARAM_TIMEMODE)));
		props.setProperty(PARAM_FLIGHT_TYPE, getNotNullString(request.getParameter(PARAM_FLIGHT_TYPE)));
		props.setProperty(PARAM_COPY_DATE, getNotNullString(request.getParameter(PARAM_COPY_DATE)));

		props.setProperty(PARAM_LEG1_ARRIVAL, getNotNullString(request.getParameter(PARAM_LEG1_ARRIVAL)));
		props.setProperty(PARAM_LEG1_ARRIVALDAY, getNotNullString(request.getParameter(PARAM_LEG1_ARRIVALDAY)));
		props.setProperty(PARAM_LEG1_ARRIVALTIME, getNotNullString(request.getParameter(PARAM_LEG1_ARRIVALTIME)));
		props.setProperty(PARAM_LEG1_DEATURETIME, getNotNullString(request.getParameter(PARAM_LEG1_DEATURETIME)));
		props.setProperty(PARAM_LEG1_DEPATURE, getNotNullString(request.getParameter(PARAM_LEG1_DEPATURE)));
		props.setProperty(PARAM_LEG1_DEPATUREDAY, getNotNullString(request.getParameter(PARAM_LEG1_DEPATUREDAY)));

		props.setProperty(PARAM_LEG2_ARRIVAL, getNotNullString(request.getParameter(PARAM_LEG2_ARRIVAL)));
		props.setProperty(PARAM_LEG2_ARRIVALDAY, getNotNullString(request.getParameter(PARAM_LEG2_ARRIVALDAY)));
		props.setProperty(PARAM_LEG2_ARRIVALTIME, getNotNullString(request.getParameter(PARAM_LEG2_ARRIVALTIME)));
		props.setProperty(PARAM_LEG2_DEATURETIME, getNotNullString(request.getParameter(PARAM_LEG2_DEATURETIME)));
		props.setProperty(PARAM_LEG2_DEPATURE, getNotNullString(request.getParameter(PARAM_LEG2_DEPATURE)));
		props.setProperty(PARAM_LEG2_DEPATUREDAY, getNotNullString(request.getParameter(PARAM_LEG2_DEPATUREDAY)));

		props.setProperty(PARAM_LEG3_ARRIVAL, getNotNullString(request.getParameter(PARAM_LEG3_ARRIVAL)));
		props.setProperty(PARAM_LEG3_ARRIVALDAY, getNotNullString(request.getParameter(PARAM_LEG3_ARRIVALDAY)));
		props.setProperty(PARAM_LEG3_ARRIVALTIME, getNotNullString(request.getParameter(PARAM_LEG3_ARRIVALTIME)));
		props.setProperty(PARAM_LEG3_DEATURETIME, getNotNullString(request.getParameter(PARAM_LEG3_DEATURETIME)));
		props.setProperty(PARAM_LEG3_DEPATURE, getNotNullString(request.getParameter(PARAM_LEG3_DEPATURE)));
		props.setProperty(PARAM_LEG3_DEPATUREDAY, getNotNullString(request.getParameter(PARAM_LEG3_DEPATUREDAY)));

		props.setProperty(PARAM_LEG4_ARRIVAL, getNotNullString(request.getParameter(PARAM_LEG4_ARRIVAL)));
		props.setProperty(PARAM_LEG4_ARRIVALDAY, getNotNullString(request.getParameter(PARAM_LEG4_ARRIVALDAY)));
		props.setProperty(PARAM_LEG4_ARRIVALTIME, getNotNullString(request.getParameter(PARAM_LEG4_ARRIVALTIME)));
		props.setProperty(PARAM_LEG4_DEATURETIME, getNotNullString(request.getParameter(PARAM_LEG4_DEATURETIME)));
		props.setProperty(PARAM_LEG4_DEPATURE, getNotNullString(request.getParameter(PARAM_LEG4_DEPATURE)));
		props.setProperty(PARAM_LEG4_DEPATUREDAY, getNotNullString(request.getParameter(PARAM_LEG4_DEPATUREDAY)));

		props.setProperty(PARAM_LEG5_ARRIVAL, getNotNullString(request.getParameter(PARAM_LEG5_ARRIVAL)));
		props.setProperty(PARAM_LEG5_ARRIVALDAY, getNotNullString(request.getParameter(PARAM_LEG5_ARRIVALDAY)));
		props.setProperty(PARAM_LEG5_ARRIVALTIME, getNotNullString(request.getParameter(PARAM_LEG5_ARRIVALTIME)));
		props.setProperty(PARAM_LEG5_DEATURETIME, getNotNullString(request.getParameter(PARAM_LEG5_DEATURETIME)));
		props.setProperty(PARAM_LEG5_DEPATURE, getNotNullString(request.getParameter(PARAM_LEG5_DEPATURE)));
		props.setProperty(PARAM_LEG5_DEPATUREDAY, getNotNullString(request.getParameter(PARAM_LEG5_DEPATUREDAY)));

		props.setProperty(PARAM_ALLOWCOF, getNotNullString(request.getParameter(PARAM_ALLOWCOF)));
		props.setProperty(PARAM_ALLOWDUR, getNotNullString(request.getParameter(PARAM_ALLOWDUR)));
		props.setProperty(PARAM_ALLOWMODEL, getNotNullString(request.getParameter(PARAM_ALLOWMODEL)));
		props.setProperty(PARAM_ALLOWSEGMENTS, getNotNullString(request.getParameter(PARAM_ALLOWSEGMENTS)));
		props.setProperty(PARAM_ALLOWOVERLAP, getNotNullString(request.getParameter(PARAM_ALLOWOVERLAP)));
		props.setProperty(PARAM_GRIDROW, getNotNullString(request.getParameter(PARAM_GRIDROW)));

		props.setProperty(PARAM_LEG_VALIDITY, getNotNullString(request.getParameter(PARAM_LEG_VALIDITY)));
		props.setProperty(PARAM_VALID_SEGMENT, getNotNullString(request.getParameter(PARAM_VALID_SEGMENT)));
		props.setProperty(PARAM_VALID_TERMINAL, getNotNullString(request.getParameter(PARAM_VALID_TERMINAL)));
		props.setProperty(PARAM_VALID_SEGMET_OLAPID, getNotNullString(request.getParameter(PARAM_VALID_SEGMET_OLAPID)));
		props.setProperty(PARAM_OVERLAPFLTID, getNotNullString(request.getParameter(PARAM_OVERLAPFLTID)));
		props.setProperty(PARAM_FROM_PAGE, getNotNullString(request.getParameter(PARAM_FROM_PAGE)));

		props.setProperty(PARAM_FLIGHT_REMARKS, getNotNullString(request.getParameter(PARAM_FLIGHT_REMARKS)));
		props.setProperty(PARAM_MEAL_TEMPLATE, getNotNullString(request.getParameter(PARAM_MEAL_TEMPLATE)));
		props.setProperty(PARAM_SEAT_CHARGE_TEMPLATE, getNotNullString(request.getParameter(PARAM_SEAT_CHARGE_TEMPLATE)));
		props.setProperty(PARAM_USER_NORES, getNotNullString(request.getParameter(PARAM_USER_NORES)));
		props.setProperty(PARAM_FLIGHT_NO, getNotNullString(request.getParameter(PARAM_FLIGHT_NO)));
		props.setProperty(PARAM_FLIGHT_DEPARTURE_DATE, getNotNullString(request.getParameter(PARAM_FLIGHT_DEPARTURE_DATE)));
		
		// CodeShare
		props.setProperty(PARAM_CODESHARE, AiradminUtils.getNotNullString(request.getParameter(PARAM_CODESHARE)));
		
		props.setProperty(PARAM_CSOCCARRIER, AiradminUtils.getNotNullString(request.getParameter(PARAM_CSOCCARRIER)));
		props.setProperty(PARAM_CSOCFLIGHT, AiradminUtils.getNotNullString(request.getParameter(PARAM_CSOCFLIGHT)));


		return props;
	}

	/**
	 * Creates the Flight
	 * 
	 * @param props
	 *            the property file Containg flight data
	 * @return Flight the Created Flight
	 * @throws ModuleException
	 *             the ModuleException
	 * @throws ParseException
	 *             the ParseException
	 */
	public static Flight createFlight(Properties props) throws ModuleException, ParseException {
		Flight flight = new Flight();

		ArrayList<String> depaturelist = new ArrayList<String>();
		addToList(depaturelist, props.getProperty(PARAM_LEG1_DEPATURE));
		addToList(depaturelist, props.getProperty(PARAM_LEG2_DEPATURE));
		addToList(depaturelist, props.getProperty(PARAM_LEG3_DEPATURE));
		addToList(depaturelist, props.getProperty(PARAM_LEG4_DEPATURE));
		addToList(depaturelist, props.getProperty(PARAM_LEG5_DEPATURE));

		ArrayList<String> arrivalList = new ArrayList<String>();
		addToList(arrivalList, props.getProperty(PARAM_LEG1_ARRIVAL));
		addToList(arrivalList, props.getProperty(PARAM_LEG2_ARRIVAL));
		addToList(arrivalList, props.getProperty(PARAM_LEG3_ARRIVAL));
		addToList(arrivalList, props.getProperty(PARAM_LEG4_ARRIVAL));
		addToList(arrivalList, props.getProperty(PARAM_LEG5_ARRIVAL));

		ArrayList<String> arrivalTimelist = new ArrayList<String>();
		addToList(arrivalTimelist, props.getProperty(PARAM_LEG1_ARRIVALTIME));
		addToList(arrivalTimelist, props.getProperty(PARAM_LEG2_ARRIVALTIME));
		addToList(arrivalTimelist, props.getProperty(PARAM_LEG3_ARRIVALTIME));
		addToList(arrivalTimelist, props.getProperty(PARAM_LEG4_ARRIVALTIME));
		addToList(arrivalTimelist, props.getProperty(PARAM_LEG5_ARRIVALTIME));

		ArrayList<String> depatureTimelist = new ArrayList<String>();
		addToList(depatureTimelist, props.getProperty(PARAM_LEG1_DEATURETIME));
		addToList(depatureTimelist, props.getProperty(PARAM_LEG2_DEATURETIME));
		addToList(depatureTimelist, props.getProperty(PARAM_LEG3_DEATURETIME));
		addToList(depatureTimelist, props.getProperty(PARAM_LEG4_DEATURETIME));
		addToList(depatureTimelist, props.getProperty(PARAM_LEG5_DEATURETIME));

		ArrayList<String> arrivalVarianceList = new ArrayList<String>();
		addToList(arrivalVarianceList, props.getProperty(PARAM_LEG1_ARRIVALDAY));
		addToList(arrivalVarianceList, props.getProperty(PARAM_LEG2_ARRIVALDAY));
		addToList(arrivalVarianceList, props.getProperty(PARAM_LEG3_ARRIVALDAY));
		addToList(arrivalVarianceList, props.getProperty(PARAM_LEG4_ARRIVALDAY));
		addToList(arrivalVarianceList, props.getProperty(PARAM_LEG5_ARRIVALDAY));

		ArrayList<String> depatureVarianceList = new ArrayList<String>();
		addToList(depatureVarianceList, props.getProperty(PARAM_LEG1_DEPATUREDAY));
		addToList(depatureVarianceList, props.getProperty(PARAM_LEG2_DEPATUREDAY));
		addToList(depatureVarianceList, props.getProperty(PARAM_LEG3_DEPATUREDAY));
		addToList(depatureVarianceList, props.getProperty(PARAM_LEG4_DEPATUREDAY));
		addToList(depatureVarianceList, props.getProperty(PARAM_LEG5_DEPATUREDAY));

		String strTimeinMode = props.getProperty(PARAM_TIMEMODE);
		if (strTimeinMode.equals(WebConstants.LOCALTIME)) {
			flight.setDepartureDateLocal(ScheduleUtils.parseToDate(props.getProperty(PARAM_DEPATUREDATE)));
		} else {
			flight.setDepartureDate(ScheduleUtils.parseToDate(props.getProperty(PARAM_DEPATUREDATE)));
		}

		flight.setFlightNumber((props.getProperty(PARAM_FLIGHTSTARTNUMBER)).toUpperCase()
				+ (props.getProperty(PARAM_FLIGHTNUMBER)).toUpperCase());
		flight.setStatus(props.getProperty(PARAM_STATUS));
		flight.setOperationTypeId(Integer.parseInt(props.getProperty(PARAM_OPERATIONTYPE)));
		flight.setModelNumber(props.getProperty(PARAM_AIRCRAFTMODEL));
		if (!("").equals(props.getProperty(PARAM_FLIGHTID))) {
			flight.setFlightId(new Integer(props.getProperty(PARAM_FLIGHTID)));
		}

		if (!props.getProperty(PARAM_VERSION).equals("")) {
			flight.setVersion(Long.parseLong(props.getProperty(PARAM_VERSION)));
		}
		if (!props.getProperty(PARAM_SCHEDULEID).equals("")) {
			flight.setScheduleId(new Integer(props.getProperty(PARAM_SCHEDULEID)));
		}

		Set<FlightLeg> legSet = new HashSet<FlightLeg>();
		Object[] depatures = depaturelist.toArray();

		Object[] arrivals = arrivalList.toArray();
		Object[] arrivalTimes = arrivalTimelist.toArray();
		Object[] depaturTimes = depatureTimelist.toArray();
		Object[] arrivalVariances = arrivalVarianceList.toArray();
		Object[] depatureVariances = depatureVarianceList.toArray();

		String defaultFormat // get the default schedule status to search
		= globalConfig.getBizParam(SystemParamKeys.DATE_FORMAT_FOR_DATE_STRINGS);

		for (int i = 0; i < depatures.length; i++) {
			FlightLeg flLeg = new FlightLeg();
			flLeg.setOrigin((String) depatures[i]);
			flLeg.setDestination((String) arrivals[i]);
			if (strTimeinMode.equals(WebConstants.LOCALTIME)) {
				flLeg.setEstDepartureTimeLocal(CalendarUtil.getParsedTime((String) depaturTimes[i], defaultFormat));
				flLeg.setEstArrivalTimeLocal(CalendarUtil.getParsedTime((String) arrivalTimes[i], defaultFormat));
				flLeg.setEstArrivalDayOffsetLocal(Integer.parseInt((String) arrivalVariances[i]));
				flLeg.setEstDepartureDayOffsetLocal(Integer.parseInt((String) depatureVariances[i]));
			} else {
				flLeg.setEstDepartureTimeZulu(CalendarUtil.getParsedTime((String) depaturTimes[i], defaultFormat));
				flLeg.setEstArrivalTimeZulu(CalendarUtil.getParsedTime((String) arrivalTimes[i], defaultFormat));
				flLeg.setEstArrivalDayOffset(Integer.parseInt((String) arrivalVariances[i]));
				flLeg.setEstDepartureDayOffset(Integer.parseInt((String) depatureVariances[i]));
			}
			flLeg.setLegNumber(i + 1);
			legSet.add(flLeg);
		}
		flight.setFlightLegs(legSet);

		// setting flight segments
		if (props.getProperty(PARAM_LEG_VALIDITY).equals("true")
				|| props.getProperty(PARAM_MODE).equals(WebConstants.ACTION_UPDATE)) {
			if (props.getProperty(PARAM_OVERLAPFLTID) != null && !"".equals(props.getProperty(PARAM_OVERLAPFLTID))) {
				flight.setOverlapingFlightId(new Integer(props.getProperty(PARAM_OVERLAPFLTID)));
			}

			HashSet<FlightSegement> segmentSet = getValidatedSegments(props.getProperty(PARAM_VALID_SEGMENT),
					getValidatedTerminals(props.getProperty(PARAM_VALID_TERMINAL)));
			HashSet<FlightSegement> segmentLegSet = getSegmentsFromLegSet(legSet);
			HashSet<FlightSegement> comparedSet = FlightUtils.getComparedSegments(segmentLegSet, segmentSet);
			flight.setFlightSegements(comparedSet);
		} else if (!props.getProperty(PARAM_MODE).equals(WebConstants.ACTION_ADD)) {
			HashSet<FlightSegement> segmentSet = getValidatedSegments(props.getProperty(PARAM_VALID_SEGMENT),
					getValidatedTerminals(props.getProperty(PARAM_VALID_TERMINAL)));
			flight.setFlightSegements(segmentSet);
		} else {
			HashSet<FlightSegement> segmentSet = getSegmentsFromLegSet(legSet);
			flight.setFlightSegements(segmentSet);
		}

		String strOlapFltId = props.getProperty(PARAM_OVERLAPFLTID);
		if (!strOlapFltId.equals("null") && !(strOlapFltId.equals(""))) {
			flight.setOverlapingFlightId(new Integer(strOlapFltId));
		}

		// Haider 01Sep08 set the GDS Ids to flight
		String gdsIds = props.getProperty(PARAM_GDSPUBLISHING);// return comma separated string
		if (!("").equals(gdsIds)) {
			String[] ids = StringUtils.split(gdsIds, ",");
			for (int i = 0; i < ids.length; i++) {
				flight.addGdsId(Integer.valueOf(ids[i]));
			}
		}
		// -------------

		String fltType = props.getProperty(PARAM_FLIGHT_TYPE);
		if (!fltType.equals("null") && !fltType.equals("")) {
			flight.setFlightType(fltType);
		}

		String remarks = props.getProperty(PARAM_FLIGHT_REMARKS);
		if (!remarks.equals("null") && !remarks.equals("")) {
			flight.setRemarks(remarks);
		} else {
			flight.setRemarks("");
		}

		String userNotes = props.getProperty(PARAM_USER_NORES);
		if (!userNotes.equals("") && userNotes != null) {
			flight.setUserNotes(userNotes);
		} else {
			flight.setUserNotes("");
		}

		String mealTemplateCode = props.getProperty(PARAM_MEAL_TEMPLATE);
		String seatChargeTemplateCode = props.getProperty(PARAM_SEAT_CHARGE_TEMPLATE);

		if (!mealTemplateCode.equals("null") && !mealTemplateCode.equals("")) {
			flight.setMealTemplateId(new Integer(mealTemplateCode));
		}

		if (!seatChargeTemplateCode.equals("null") && !seatChargeTemplateCode.equals("")) {
			flight.setSeatChargeTemplateId(new Integer(seatChargeTemplateCode));
		}
		//CodeShare
		Set<CodeShareMCFlight> codeShareMCFlights =FlightUtils.getCodeShareMCFlight(props.getProperty(PARAM_CODESHARE));
		flight.setCodeShareMCFlights(codeShareMCFlights);	

		flight.setCsOCCarrierCode(props.getProperty(PARAM_CSOCCARRIER));
		flight.setCsOCFlightNumber(props.getProperty(PARAM_CSOCFLIGHT));

		
		return flight;
	}

	/**
	 * Get the Arranged,Validate Segment set fron a String
	 * 
	 * @param strArr
	 *            the Segment Array
	 * @return HashSet the set containg Segments
	 */
	private static HashSet<FlightSegement> getValidatedSegments(String strArr, HashMap<String, FlightSegement> map) {
		HashSet<FlightSegement> segset = new HashSet<FlightSegement>();
		StringTokenizer st = new StringTokenizer(strArr, ",");
		while (st.hasMoreTokens()) {
			FlightSegement seg = new FlightSegement();
			String strToken = st.nextToken();
			String[] strValSegs = strToken.split("_");
			seg.setSegmentCode(strValSegs[0]);
			seg.setValidFlag(strValSegs[1].equals("true"));
			// This is to set terminal ids for the flight
			if (!map.isEmpty() && null != map.get(strValSegs[0])) {
				seg.setArrivalTerminalId(((FlightSegement) map.get(strValSegs[0])).getArrivalTerminalId());
				seg.setDepartureTerminalId(((FlightSegement) map.get(strValSegs[0])).getDepartureTerminalId());
			}
			segset.add(seg);
		}
		return segset;
	}

	/**
	 * Get the Arranged,Validate Terminal set from a String
	 * 
	 * @param strArr
	 *            the Segment Array
	 * @return HashMap the map containg Terminals
	 */
	private static HashMap<String, FlightSegement> getValidatedTerminals(String strArr) {
		HashMap<String, FlightSegement> segmap = new HashMap<String, FlightSegement>();
		StringTokenizer st = new StringTokenizer(strArr, "|");
		try {
			while (st.hasMoreTokens()) {
				FlightSegement seg = new FlightSegement();
				String strToken = st.nextToken();
				String[] strValSegs = strToken.split("\\^");
				if (strValSegs.length >= 2 && !("").equals(strValSegs[1]))
					seg.setDepartureTerminalId(Integer.parseInt(strValSegs[1]));
				if (strValSegs.length == 3 && !("").equals(strValSegs[2]))
					seg.setArrivalTerminalId(Integer.parseInt(strValSegs[2]));
				segmap.put(strValSegs[0], seg);
			}
		} catch (Exception e) {
			log.error(e);
		}
		return segmap;
	}

	/**
	 * Gets a sement after Cpomparring with Another Segment
	 * 
	 * @param legSeg
	 *            the Segment set need to be compared
	 * @param validSeg
	 *            the Segment set Comparing against
	 * @return HashSet the set containg the segments after comparing
	 */
	private static HashSet<FlightSegement> getComparedSegments(HashSet<FlightSegement> legSeg, HashSet<FlightSegement> validSeg) {
		HashSet<FlightSegement> segset = new HashSet<FlightSegement>();
		Iterator<FlightSegement> legIte = legSeg.iterator();
		while (legIte.hasNext()) {
			FlightSegement leg = (FlightSegement) legIte.next();
			Iterator<FlightSegement> validIte = validSeg.iterator();
			while (validIte.hasNext()) {
				FlightSegement valid = (FlightSegement) validIte.next();
				if (leg.getSegmentCode().equals(valid.getSegmentCode())) {
					leg.setValidFlag(valid.getValidFlag());
					leg.setArrivalTerminalId(valid.getArrivalTerminalId());
					leg.setDepartureTerminalId(valid.getDepartureTerminalId());
				}
			}
			segset.add(leg);
		}
		return segset;
	}

	/**
	 * Sets the Allowed actions for the FlightAllowaction DTO
	 * 
	 * @param props
	 *            the property file containg request data
	 * @return FlightAllowActionDTO with allowed actions
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static FlightAllowActionDTO createAllowActions(Properties props) throws ModuleException {
		FlightAllowActionDTO fltAllowDTO = new FlightAllowActionDTO();
		fltAllowDTO.setAllowConflicts(getAllowValue(props.getProperty(PARAM_ALLOWCOF)));
		fltAllowDTO.setAllowAnyDuration(getAllowValue(props.getProperty(PARAM_ALLOWDUR)));
		fltAllowDTO.setAllowModelForAnyCC(getAllowValue(props.getProperty(PARAM_ALLOWMODEL)));
		fltAllowDTO.setAllowOverlapForAnyCC(getAllowValue(props.getProperty(PARAM_ALLOWOVERLAP)));
		fltAllowDTO.setAllowSegmentV2I(getAllowValue(props.getProperty(PARAM_ALLOWSEGMENTS)));
		return fltAllowDTO;
	}

	/**
	 * Gets the Longest Segment from the Set of Segments
	 * 
	 * @param set
	 *            the Set containg the Segments
	 * @return FlightSegement the longest Segment
	 */
	public static FlightSegement getDetailsOfLongestSegment(Set<FlightSegement> set) {
		String strSeg = null;
		int prevLen = 0;
		FlightSegement flightSeg = null;
		FlightSegement seg = null;
		if (set != null) {
			Iterator<FlightSegement> ite = set.iterator();
			if (ite != null) {
				while (ite.hasNext()) {
					seg = (FlightSegement) ite.next();
					strSeg = seg.getSegmentCode();
					if (prevLen < strSeg.length()) {
						prevLen = strSeg.length();
						flightSeg = seg;
					}
				}
			}
		}
		return flightSeg;
	}

	/**
	 * Creates the Segment array From Collection of segments
	 * 
	 * @param segs
	 *            the Collection of segments
	 * @return String the Segment array
	 */
	public static String createSegmentArr(Collection<FlightSegement> segs) {
		StringBuffer segb = new StringBuffer("var arrSegDtls = new Array();");// leg detail array
		int j = 0;
		if (segs != null) {
			Iterator<FlightSegement> iter = segs.iterator();
			while (iter.hasNext()) {
				FlightSegement fltSeg = (FlightSegement) iter.next();
				segb.append("arrSegDtls[" + j + "] =new Array('" + fltSeg.getFltSegId() + "','" + fltSeg.getSegmentCode() + "','"
						+ fltSeg.getValidFlag() + "','" + fltSeg.getOverlapSegmentId() + "');");
				j++;
			}
		}
		return segb.toString();
	}

	/**
	 * Creates Possible Overlaping Flight List for a Given Flight
	 * 
	 * @param flight
	 *            the Flight
	 * @return String the List of Possible overlapping Flights
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String createOverlapableFlightList(Flight flight) throws ModuleException {
		StringBuffer sb = new StringBuffer();
		Object[] overlapFlightArr = ModuleServiceLocator.getFlightServiceBD().getPossibleOverlappingFlights(flight).toArray();
		for (int i = 0; i < overlapFlightArr.length; i++) {
			Flight olapFlight = (Flight) overlapFlightArr[i];
			sb.append("<option value='" + olapFlight.getFlightId() + "'>" + olapFlight.getFlightId() + "</option>");
		}
		return sb.toString();
	}

	private static String getNotNullString(String str) {
		return (str == null || str.equals("null") || str.equals("&nbsp")) ? "" : str;
	}

	private static ArrayList<String> addToList(ArrayList<String> list, String str) {
		if (str != null && !str.trim().equals(""))
			list.add(str);
		return list;
	}

	private static boolean getAllowValue(String str) {
		return str.equals("true");
	}

	// Haider 04Sep08
	/**
	 * method to get flight ids
	 * 
	 * @param flights
	 * @return flight id's list
	 */
	public static List<Integer> getFlightIds(Collection<DisplayFlightDTO> flights) {

		if (flights == null || flights.size() == 0)
			return null;

		List<Integer> idList = new ArrayList<Integer>();
		Iterator<DisplayFlightDTO> itFlights = flights.iterator();

		while (itFlights.hasNext()) {

			DisplayFlightDTO flightDTO = (DisplayFlightDTO) itFlights.next();
			Flight flight = flightDTO.getFlight();
			idList.add(flight.getFlightId());
		}

		return idList;
	}

	public static String checkForMultipleFltTypesAvailability() {
		List<FlightTypeDTO> lstFltTypes = AppSysParamsUtil.availableFlightTypes();
		int enabledFlightTypes = 0;
		String tmpFlightType = null;
		String jsonFlightTypeString = null;

		for (FlightTypeDTO fltType : lstFltTypes) {
			if ("Y".equals(fltType.getFlightTypeStatus())) {
				enabledFlightTypes++;
				tmpFlightType = fltType.getFlightType();
			}
		}

		if (enabledFlightTypes == 1) {
			jsonFlightTypeString = "var multipleFltTypeVisibility = {'onlySingle':true,'enabledType':'" + tmpFlightType + "'}";
		} else {
			jsonFlightTypeString = "var multipleFltTypeVisibility = {'onlySingle':false,'enabledType':''}";
		}
		return jsonFlightTypeString;
	}
	/**
	 * Get CodeShare Carrier & Flight set from a String
	 * 
	 * @param strArr
	 *            the Codeshare Array
	 * @return HashSet the codeShareMCFlights
	 */
	public static HashSet<CodeShareMCFlight> getCodeShareMCFlight(String strArr) {
		HashSet<CodeShareMCFlight> CodeShareMCFlightSh =new HashSet<CodeShareMCFlight>();
		StringTokenizer st = new StringTokenizer(strArr, ",");
		try {
			while (st.hasMoreTokens()) {				
				String strToken = st.nextToken();
				String[] strValSegs = strToken.split("\\|");
				String carrier = strValSegs[0];
				String flight = strValSegs[1];
				if (strValSegs.length == 2 && !("").equals(strValSegs[1])){
					CodeShareMCFlight codeShareMCFlight = new CodeShareMCFlight();
					codeShareMCFlight.setCsMCCarrierCode(carrier);
					codeShareMCFlight.setCsMCFlightNumber(flight);
				CodeShareMCFlightSh.add(codeShareMCFlight);				
				}
			}
		} catch (Exception e) {
			log.error(e);
		}
		return CodeShareMCFlightSh;
	}
}
