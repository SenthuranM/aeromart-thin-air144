package com.isa.thinair.airadmin.core.web.v2.action.system;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

/**
 * 
 * @author Baladewa Welathanhtri
 * 
 */
@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Results({ @Result(name = S2Constants.Jsp.Charges.CHARGE_ADMIN_JSP, value = S2Constants.Jsp.Charges.CHARGE_ADMIN_JSP),
		@Result(name = S2Constants.Jsp.Charges.CHARGE_GRID_DATA, value = S2Constants.Jsp.Charges.CHARGE_GRID_DATA),
		@Result(name = AdminStrutsConstants.AdminAction.ERROR, value = AdminStrutsConstants.AdminJSP.ERROR_JSP) })
public class ShowLoadChargesJspAction extends BaseRequestAwareAction {

	public String setup() {
		return S2Constants.Jsp.Charges.CHARGE_ADMIN_JSP;
	}

	public String gridV2() {
		return S2Constants.Jsp.Charges.CHARGE_GRID_DATA;
	}

}
