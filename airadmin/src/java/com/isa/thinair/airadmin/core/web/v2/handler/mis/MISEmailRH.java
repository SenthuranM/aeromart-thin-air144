package com.isa.thinair.airadmin.core.web.v2.handler.mis;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AirAdminInternalConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.messaging.api.model.MessageProfile;
import com.isa.thinair.messaging.api.model.Topic;
import com.isa.thinair.messaging.api.model.UserMessaging;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.reporting.api.model.RegionalProductSalesTO;
import com.isa.thinair.reportingframework.api.service.ReportingFrameworkBD;

public class MISEmailRH {

	public static void sendRegionalProductSalesEmail(String emailID, String contents,
			Map<String, Map<String, RegionalProductSalesTO>> regionalProductSales) {

		// User Message
		UserMessaging userMessaging = new UserMessaging();

		userMessaging.setLastName("MIS");
		userMessaging.setToAddres(emailID);
		List<UserMessaging> messages = new ArrayList<UserMessaging>();
		messages.add(userMessaging);

		HashMap salesMap = new HashMap();
		salesMap.put("salesData", regionalProductSales);
		salesMap.put("subject", "Regional Product Sales Data");
		salesMap.put("comments", contents);
		// Topic
		Topic topic = new Topic();
		topic.setTopicParams(salesMap);
		topic.setTopicName(AirAdminInternalConstants.MisEmailTemplateNames.MIS_REGIONAL_SALES_EMAIL);
		topic.setAttachMessageBody(false);

		// User Profile
		MessageProfile messageProfile = new MessageProfile();
		messageProfile.setUserMessagings(messages);
		messageProfile.setTopic(topic);

		// Send the message
		ModuleServiceLocator.getMessagingBD().sendMessage(messageProfile);

	}

	public static void setReportView(String fromDate, String toDate, HttpServletResponse response) throws ModuleException {
		ReportsSearchCriteria search = new ReportsSearchCriteria();
		ResultSet resultSet = null;
		String reportTemplate = "MISRegionaSalesReport.jasper";
		search.setDateRangeFrom(fromDate);
		search.setDateRangeTo(toDate);
		Map<String, Object> parameters = new HashMap<String, Object>();
		ReportingFrameworkBD reportingFrameworkBD = ModuleServiceLocator.getReportingFrameworkBD();
		reportingFrameworkBD.storeJasperTemplateRefs("WebReport1", ReportsHTMLGenerator.getReportTemplate(reportTemplate));

		parameters.put("FROM_DATE", fromDate);
		parameters.put("TO_DATE", toDate);
		resultSet = ModuleServiceLocator.getDataExtractionBD().getMisProductSalesData(search);
		response.reset();
		response.addHeader("Content-Disposition", "attachment;filename=MISProductSalesAnalysisReport.csv");
		reportingFrameworkBD.createCSVReport("WebReport1", parameters, resultSet, response);
	}

	public static void sendDashBoardSalesByChannelEmail(String emailID, String contents, String chartURL, String fromDate,
			String toDate, String region, String pos, String chart2Src) {
		// User Message
		UserMessaging userMessaging = new UserMessaging();

		userMessaging.setLastName("MIS");
		userMessaging.setToAddres(emailID);
		List<UserMessaging> messages = new ArrayList<UserMessaging>();
		messages.add(userMessaging);
		String title = "";

		title += "Date period : " + fromDate + " - " + toDate + "<br>";
		title += "Region : " + region + "<br>";
		title += "Station : " + pos;

		HashMap salesMap = new HashMap();
		salesMap.put("chartData", chartURL);
		salesMap.put("subject", "Dash borad - Sales By Channel");
		salesMap.put("title", title);
		salesMap.put("comments", contents);
		salesMap.put("chartPaxData", chart2Src);
		// Topic
		Topic topic = new Topic();
		topic.setTopicParams(salesMap);
		topic.setTopicName(AirAdminInternalConstants.MisEmailTemplateNames.MIS_DASH_BOARD_SALES_BY_CHANNEL_EMAIL);
		topic.setAttachMessageBody(false);

		// User Profile
		MessageProfile messageProfile = new MessageProfile();
		messageProfile.setUserMessagings(messages);
		messageProfile.setTopic(topic);

		// Send the message
		ModuleServiceLocator.getMessagingBD().sendMessage(messageProfile);
	}

}
