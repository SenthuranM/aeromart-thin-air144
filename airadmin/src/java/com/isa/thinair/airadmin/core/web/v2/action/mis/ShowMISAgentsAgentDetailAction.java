package com.isa.thinair.airadmin.core.web.v2.action.mis;

import java.util.Collection;
import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.reporting.api.criteria.MISReportsSearchCriteria;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Result(name = AdminStrutsConstants.AdminAction.SUCCESS, type = JSONResult.class, value = "")
public class ShowMISAgentsAgentDetailAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(ShowMISAgentsAgentDetailAction.class);
	private boolean success = true;
	private String messageTxt = null;
	private String agentCode = null;
	private String fromDate = null;
	private String toDate = null;
	private String region = null;
	private String pos = null;
	private Collection<HashMap<String, String>> roteWiseRevenue = null;
	private Collection<HashMap<String, String>> staffWiseRevenue = null;

	public String execute() throws Exception {

		String strForward = WebConstants.FORWARD_SUCCESS;

		try {
			if (log.isDebugEnabled()) {
				log.debug("agentCode = " + agentCode);
			}

			MISReportsSearchCriteria misSearchCriteria = new MISReportsSearchCriteria();
			misSearchCriteria.setFromDate(this.fromDate);
			misSearchCriteria.setToDate(this.toDate);
			misSearchCriteria.setRegion(this.region);
			misSearchCriteria.setPos(this.pos);

			roteWiseRevenue = ModuleServiceLocator.getMISDataProviderBD().getRouteWiseAgentRevenue(misSearchCriteria, agentCode);
			staffWiseRevenue = ModuleServiceLocator.getMISDataProviderBD().getStaffWiseAgentRevenue(misSearchCriteria, agentCode);

		} catch (Exception e) {
			log.error("Generic exception caught.", e);
			success = false;
			messageTxt = e.getMessage();
		}
		return strForward;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getPos() {
		return pos;
	}

	public void setPos(String pos) {
		this.pos = pos;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public Collection<HashMap<String, String>> getRoteWiseRevenue() {
		return roteWiseRevenue;
	}

	public void setRoteWiseRevenue(Collection<HashMap<String, String>> roteWiseRevenue) {
		this.roteWiseRevenue = roteWiseRevenue;
	}

	public Collection<HashMap<String, String>> getStaffWiseRevenue() {
		return staffWiseRevenue;
	}

	public void setStaffWiseRevenue(Collection<HashMap<String, String>> staffWiseRevenue) {
		this.staffWiseRevenue = staffWiseRevenue;
	}
}
