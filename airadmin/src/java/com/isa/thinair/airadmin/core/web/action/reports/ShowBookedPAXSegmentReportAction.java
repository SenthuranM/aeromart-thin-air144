package com.isa.thinair.airadmin.core.web.action.reports;

import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.handler.reports.BookedPAXSegmentReportRH;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;

/**
 * @author Jagath
 * 
 */
@Results({
		@Result(name = AdminStrutsConstants.AdminAction.SUCCESS, value = AdminStrutsConstants.AdminJSP.REPORTING_BOOKED_PAX_SEGMENT),
		@Result(name = AdminStrutsConstants.AdminAction.ERROR, value = AdminStrutsConstants.AdminJSP.ERROR_JSP) })
public class ShowBookedPAXSegmentReportAction extends BaseRequestResponseAwareAction {
	public String execute() {
		return BookedPAXSegmentReportRH.execute(request, response);
	}

}
