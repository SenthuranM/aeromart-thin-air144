package com.isa.thinair.airadmin.core.web.handler.security;

import java.security.Principal;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleConfig;
import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.security.MenuHTMLGenerator;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.airsecurity.api.utils.AirSecurityUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.webplatform.core.commons.DatabaseUtil;

public class ShowMenuRH {

	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();
	private static AiradminModuleConfig config = AiradminModuleUtils.getConfig();
	private static final Log log = LogFactory.getLog(ShowMenuRH.class);

	/**
	 * Main execute Method for Menu Actions
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;
		try {
			setDisplayData(request);
		} catch (Throwable ex) {
			log.error("Error in initializing login", ex);
			throw new RuntimeException(ex);
		}
		return forward;
	}

	/**
	 * Sets Display Data for MENU
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static void setDisplayData(HttpServletRequest request) throws ModuleException {
		Principal userPrincipal = request.getUserPrincipal();
		User user = (User) request.getSession().getAttribute(WebConstants.SES_CURR_USER);
		boolean isPasswordReset = DatabaseUtil.getIsUserPWDReseted(userPrincipal.getName());
		if (AirSecurityUtil.isPasswordExpire(user.getPasswordExpiryDate())) {
			// TODO use different variable to propagate the expiry date 
			isPasswordReset = true;
		}	

		setPrivileges(request);
		setLogicalCabinClassVisibility(request);
		setMenuHtml(request, isPasswordReset);
		setDefaultData(request);
		setUserInfo(request);
		setCarrierName(request);
		setGlobalConfigs(request);
		setChangePassword(request, isPasswordReset);
		setReportSchedulerAppParam(request);
	}

	/**
	 * Sets the Default Data to the Menu Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static void setDefaultData(HttpServletRequest request) throws ModuleException {
		request.setAttribute(WebConstants.REQ_SYS_PARAM, JavascriptGenerator.createDefaultValuesHtml());
	}

	public static void setLogicalCabinClassVisibility(HttpServletRequest request) throws ModuleException {
		request.setAttribute("reqLogicalCCVisibility", AppSysParamsUtil.isLogicalCabinClassEnabled());
	}

	/**
	 * No idea wt's this is used for
	 * 
	 * @param request
	 */
	public static void setUserInfo(HttpServletRequest request) {
		request.setAttribute(WebConstants.REQ_HTML_LOGIN_UID, "TEST USER");
	}

	/**
	 * Sets the Menu According to Privileges
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	@SuppressWarnings("unchecked")
	public static void setMenuHtml(HttpServletRequest request, boolean isPasswordReset) throws ModuleException {
		String menuFile = request.getSession().getServletContext().getRealPath(config.getMenuConfigFile());
		HashMap<String, String> hashMapPrevi = (HashMap<String, String>) request.getSession().getAttribute(WebConstants.SES_PRIVILEGE_IDS);
		if (isPasswordReset) {
			hashMapPrevi.clear();
			request.setAttribute(WebConstants.REQ_HTML_MENU_DATA, MenuHTMLGenerator.createMenuHtml(menuFile, hashMapPrevi));
		} else {
			request.setAttribute(WebConstants.REQ_HTML_MENU_DATA, MenuHTMLGenerator.createMenuHtml(menuFile, hashMapPrevi));
		}
	}

	/**
	 * Sets the Privileges to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	@SuppressWarnings("unchecked")
	private static void setPrivileges(HttpServletRequest request) {
		StringBuffer sb = new StringBuffer();

		Map<String, String> colPrivilegeIds = (Map<String, String>) request.getSession().getAttribute(WebConstants.SES_PRIVILEGE_IDS);
		Set<String> set = colPrivilegeIds.keySet();

		for (Iterator<String> iter = set.iterator(); iter.hasNext();) {
			String prinilegeId = (String) iter.next();
			sb.append("arrPrivi['" + prinilegeId + "']=1;");
		}
		request.setAttribute(WebConstants.REQ_HTML_PRIVILEGE_DATA, sb.toString());
	}

	/**
	 * Sets the Carrier Code for Initial Pages
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setCarrierName(HttpServletRequest request) {
		String strCarrName = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME);
		request.setAttribute("reqCarrName", strCarrName);
	}

	/**
	 * Sets the Carrier Code for Initial Pages
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setGlobalConfigs(HttpServletRequest request) {
		StringBuffer sb = new StringBuffer();
		String maxAlertsToShow = globalConfig.getBizParam(SystemParamKeys.MAX_ALERTS_TO_SHOW);

		sb.append("gConfig['maxAlerts']='");
		sb.append(maxAlertsToShow);
		sb.append("';");

		request.setAttribute("reqGlobalConfig", sb.toString());
	}

	/**
	 * Check whether the password was reset by admin
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setChangePassword(HttpServletRequest request, boolean isPasswordReset) throws ModuleException {
		request.setAttribute(WebConstants.REQ_HTML_PWD_RESETED, isPasswordReset);
	}

	/**
	 * Sets the Appparameter for Report Scheduling
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setReportSchedulerAppParam(HttpServletRequest request) throws ModuleException {
		Boolean isReportSchedulingEnabled = AppSysParamsUtil.isScheduledReportsAllowed();
		request.setAttribute(WebConstants.REQ_HTML_REPORT_SCHEDULER_APP_PARAM, isReportSchedulingEnabled.toString());
	}
}
