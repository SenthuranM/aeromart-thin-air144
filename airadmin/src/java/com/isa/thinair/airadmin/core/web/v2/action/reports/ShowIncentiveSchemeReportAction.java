package com.isa.thinair.airadmin.core.web.v2.action.reports;

import java.sql.ResultSet;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airadmin.core.web.v2.util.ReportingUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.Reporting.RPTS_INCENTIVE_SCHEME_JSP) })
// @Result(name = S2Constants.Result.ERROR, value = S2Constants.Jsp.Common.ERROR_REDIRECT) })
public class ShowIncentiveSchemeReportAction extends BaseRequestResponseAwareAction {

	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();

	private String searchDateFrm;
	private String searchDateTo;
	private String hdnReportView;
	private String hdnIncentives;
	private String radReportOption;

	public String execute() {
		String forward = S2Constants.Result.SUCCESS;
		try {
			setReportView();
			return null;
		} catch (ModuleException e) {
			forward = S2Constants.Result.ERROR;
		}
		return forward;
	}

	private void setReportView() throws ModuleException {
		ReportsSearchCriteria search = new ReportsSearchCriteria();
		ResultSet resultSet = null;
		String id = "UC_REPM_054";
		String strLogo = AppSysParamsUtil.getReportLogo(false); // globalConfig.getBizParam(SystemParamKeys.REPORT_LOGO);
		String image = ReportingUtil.ReportingLocations.image_loc + AppSysParamsUtil.getReportLogo(true); // AA145.jpg
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String template = "IncentiveSchemeReport.jasper";

		ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1", ReportsHTMLGenerator.getReportTemplate(template));

		if (isNotEmptyOrNull(this.searchDateFrm)) {
			search.setDateRangeFrom("'" + ReportsHTMLGenerator.convertDate(this.searchDateFrm)
					+ " 00:00:00', 'dd-mon-yyyy HH24:mi:ss'");
		}
		if (isNotEmptyOrNull(this.searchDateTo)) {
			search.setDateRangeTo("'" + ReportsHTMLGenerator.convertDate(searchDateTo) + " 23:59:59', 'dd-mon-yyyy HH24:mi:ss'");
		}
		if (isNotEmptyOrNull(hdnIncentives)) {
			Collection<String> lstIncenID = new HashSet<String>();
			String[] arrIncenID = hdnIncentives.split(",");
			for (String IncentiveID : arrIncenID) {
				lstIncenID.add(IncentiveID);
			}
			search.setSchemeCodes(lstIncenID);
		}

		Map<String, Object> parameters = new HashMap<String, Object>();
		resultSet = ModuleServiceLocator.getDataExtractionBD().getIncentiveSchemeData(search);
		parameters.put("ID", id);
		parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
		parameters.put("FROM_DATE", this.searchDateFrm);
		parameters.put("TO_DATE", this.searchDateTo);

		if (radReportOption.trim().equals("HTML")) {
			parameters.put("IMG", image);
			ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null, null, response);
		} else if (radReportOption.trim().equals("PDF")) {
			image = ReportsHTMLGenerator.getReportTemplate(strLogo);
			parameters.put("IMG", image);
			ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);
		} else if (radReportOption.trim().equals("EXCEL")) {
			ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);
		} else if (radReportOption.trim().equals("CSV")) {
			response.reset();
			response.addHeader("Content-Disposition", "attachment;filename=IncentiveSchemeReport.csv");
			ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);
		}

	}

	public String getSearchDateFrm() {
		return searchDateFrm;
	}

	public void setSearchDateFrm(String searchDateFrm) {
		this.searchDateFrm = searchDateFrm;
	}

	public String getSearchDateTo() {
		return searchDateTo;
	}

	public void setSearchDateTo(String searchDateTo) {
		this.searchDateTo = searchDateTo;
	}

	public String getHdnReportView() {
		return hdnReportView;
	}

	public void setHdnReportView(String hdnReportView) {
		this.hdnReportView = hdnReportView;
	}

	public String getHdnIncentives() {
		return hdnIncentives;
	}

	public void setHdnIncentives(String hdnIncentives) {
		this.hdnIncentives = hdnIncentives;
	}

	public String getRadReportOption() {
		return radReportOption;
	}

	public void setRadReportOption(String radReportOption) {
		this.radReportOption = radReportOption;
	}

	private static boolean isNotEmptyOrNull(String str) {
		return !((str == null) || str.trim().equals("") || str.trim().equals("-1"));
	}
}
