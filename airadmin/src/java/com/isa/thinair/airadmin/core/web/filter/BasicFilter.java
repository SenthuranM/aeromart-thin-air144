package com.isa.thinair.airadmin.core.web.filter;

import javax.servlet.Filter;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;

public abstract class BasicFilter implements Filter {

	FilterConfig filterConfig;

	String errorPage = "/private/showError";

	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
	}

	public void destroy() {

	}
	//
	// protected static void saveMessage(HttpServletRequest request,
	// String userMessage, String msgType) {
	// // set the message
	// request.setAttribute(WebConstants.REQ_MESSAGE, userMessage);
	//
	// // set the type of message. It could be Error, Warning, Confirmation
	// request.setAttribute(WebConstants.MSG_TYPE, msgType);
	// }

}
