package com.isa.thinair.airadmin.core.web.v2.action.mis;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.v2.dto.mis.MISProcessParams;
import com.isa.thinair.airadmin.core.web.v2.util.CommonUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.reporting.api.criteria.MISReportsSearchCriteria;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.reporting.api.model.BookingsTO;
import com.isa.thinair.reporting.api.model.DistributionFlightInfoTO;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Result(name = AdminStrutsConstants.AdminAction.SUCCESS, type = JSONResult.class, value = "")
public class ShowMISDistributionAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(ShowMISDistributionAction.class);
	private boolean success = true;
	private String messageTxt;
	private String returnData = "";
	private MISReportsSearchCriteria misSearchCriteria;
	private ReportsSearchCriteria reportsSearchCriteria;
	private Collection<DistributionFlightInfoTO> countryWiseFlights;
	private List<String> countryList;
	private String highestValue;
	private Map<String, String> salesChannelList;
	private Map<String, Collection<BookingsTO>> channelWiseBookingsPerCountry;
	private String revHighestValue;
	private Map<String, BigDecimal> channelWiseRevenue;

	private Map<String, Map<String, BigDecimal>> salesFactorMap;
	private Map<String, Map<String, BigDecimal>> yieldMap;
	private Map<String, String> monthsMap;

	private static String DAY_01 = "01";

	public String execute() throws Exception {

		// tmp

		// new CommonSchedularJobsTestAction().execute();

		// /ScheduledServicesUtil.callingScheduledSeatFactorSummary();

		// tmp end

		String strForward = WebConstants.FORWARD_SUCCESS;
		MISProcessParams mpParams = new MISProcessParams(request);
		request.setAttribute("req_InitialParams", CommonUtil.convertToJSON(mpParams));

		try {
			misSearchCriteria = new MISReportsSearchCriteria();
			misSearchCriteria.setFromDate(request.getParameter("fromDate"));
			misSearchCriteria.setToDate(request.getParameter("toDate"));
			if (!"ALL".equalsIgnoreCase(request.getParameter("region"))) {
				misSearchCriteria.setRegion(request.getParameter("region"));
			}
			if (!"ALL".equalsIgnoreCase(request.getParameter("pos"))) {
				misSearchCriteria.setPos(request.getParameter("pos"));
			}

			countryWiseFlights = getCountryWiseFlightsNAgents();
			channelWiseBookingsPerCountry = getChannelwiseBookingBreakdownForCountry(countryWiseFlights);
			channelWiseRevenue = getRevenueByChannel();

			populateSFAndYields();
			// salesFactorMap = getSalesFactors();
			// yieldMap = getYields();

		} catch (Exception e) {
			log.error("Generic exception caught.", e);
			success = false;
			messageTxt = e.getMessage();
		}
		return strForward;
	}

	/**
	 * This method will retrieve the countries, no of flights, no of cities and no of agents belongs to each country for
	 * a given region or all regions.
	 * 
	 * @param request
	 * @return
	 * @throws ModuleException
	 */
	private Collection<DistributionFlightInfoTO> getCountryWiseFlightsNAgents() throws ModuleException {

		List<DistributionFlightInfoTO> countryWiseFlights = new ArrayList<DistributionFlightInfoTO>(ModuleServiceLocator
				.getMISDataProviderBD().getMISDistFlightsNCitiesByCountry(misSearchCriteria));
		// Collections.reverse(countryWiseFlights);
		return countryWiseFlights;
	}

	private Map<String, Collection<BookingsTO>> getChannelwiseBookingBreakdownForCountry(
			Collection<DistributionFlightInfoTO> flightInfoList) throws ModuleException {
		/**
		 * this map contains key : Country Code value : Map <key : SalesChannelCode, Value : BookingsTO>
		 */
		Map<String, Map<String, BookingsTO>> salesByChannelPerCntry = new TreeMap<String, Map<String, BookingsTO>>();
		Map<String, Collection<BookingsTO>> channelWiseBookingInfo = new TreeMap<String, Collection<BookingsTO>>();
		Long highestVal = new Long(0);
		countryList = new ArrayList<String>();
		salesChannelList = new HashMap<String, String>();
		Collection<String[]> salesChannels = SelectListGenerator.createSalesChannelList();
		Collection<String[]> salesChannelColors = SelectListGenerator.createSalesChannelColoursList();
		Map<String, BookingsTO> salesChannelMap = new HashMap<String, BookingsTO>();
		for (String[] salesChannel : salesChannels) {
			salesChannelMap.put(salesChannel[0], null); // Creates a map for all sales channels with an empty object
		}

		for (String[] salesChannel : salesChannelColors) {
			salesChannelList.put(salesChannel[0], salesChannel[1]); // Creates a map for all sales channels with their
																	// colors
		}

		for (DistributionFlightInfoTO flightInfo : flightInfoList) {
			countryList.add(flightInfo.getCountryCode());
		}

		misSearchCriteria.setCountryCodes(Util.buildStringInClauseContent(countryList));
		Map<String, Collection<BookingsTO>> channelWiseSalesForCountries = ModuleServiceLocator.getMISDataProviderBD()
				.getMISDistChannelwiseSalesForCountry(misSearchCriteria);
		countryList.clear();
		for (DistributionFlightInfoTO flightInfo : flightInfoList) {
			Collection<BookingsTO> channelWiseSalesForCountry = channelWiseSalesForCountries.get(flightInfo.getCountryCode());
			if (channelWiseSalesForCountry != null && channelWiseSalesForCountry.size() > 0) {
				long totalBookingsForCountry = 0;
				Map<String, BookingsTO> emptySalesChannelMap = new HashMap<String, BookingsTO>();
				emptySalesChannelMap.putAll(salesChannelMap);
				salesByChannelPerCntry.put(flightInfo.getCountry(), emptySalesChannelMap);// For each country that has
																							// bookings, put the sales
																							// channel map
				for (BookingsTO bookingsTO : channelWiseSalesForCountry) {
					totalBookingsForCountry += Long.parseLong(bookingsTO.getNoOfBookings());
					Map<String, BookingsTO> channelWiseCountryInfo = salesByChannelPerCntry.get(flightInfo.getCountry());
					channelWiseCountryInfo.put(bookingsTO.getSalesChannel(), bookingsTO);// Assign actual sales channel
																							// bookings
					channelWiseBookingInfo.put(bookingsTO.getSalesChannel(), null);// Initially create a map with only
																					// salesChannel code as key
				}

				if (totalBookingsForCountry > highestVal) {
					highestVal = totalBookingsForCountry;
				}
			} else {
				salesByChannelPerCntry.put(flightInfo.getCountry(), new HashMap<String, BookingsTO>());
			}
		}

		if (channelWiseBookingInfo.size() > 0) { // There are sales for some sales channels
			for (String country : salesByChannelPerCntry.keySet()) { // Construct bookings for all sales channels for
																		// each country
				Map<String, BookingsTO> channelWiseSalesCountryInfo = salesByChannelPerCntry.get(country);
				if (channelWiseSalesCountryInfo.size() > 0) {// Has bookings for some sales channels
					for (String salesChannel : channelWiseBookingInfo.keySet()) {
						if (channelWiseSalesCountryInfo.containsKey(salesChannel)
								&& channelWiseSalesCountryInfo.get(salesChannel) != null) {
							if (channelWiseBookingInfo.get(salesChannel) == null) {// For the sales channels map, still
																					// there is no value inserted
								channelWiseBookingInfo.put(salesChannel, new ArrayList<BookingsTO>());
							}
							channelWiseBookingInfo.get(salesChannel).add(channelWiseSalesCountryInfo.get(salesChannel)); // Add
																															// this
																															// sales
																															// for
																															// the
																															// sales
																															// cahnnels
																															// map
						} else { // There are no sales from this sales channel for this country,so add empty object
							if (channelWiseBookingInfo.get(salesChannel) == null) {
								channelWiseBookingInfo.put(salesChannel, new ArrayList<BookingsTO>());
							}
							BookingsTO bookingsTO = new BookingsTO(salesChannel, "0", country, "", "");
							channelWiseBookingInfo.get(salesChannel).add(bookingsTO);
						}
					}
				} else { // This country doesn't have any sales for any sales channel
					for (String salesChannel : channelWiseBookingInfo.keySet()) {
						if (channelWiseBookingInfo.get(salesChannel) == null) {
							channelWiseBookingInfo.put(salesChannel, new ArrayList<BookingsTO>());
						}
						BookingsTO bookingsTO = new BookingsTO(salesChannel, "0", country, "", "");
						channelWiseBookingInfo.get(salesChannel).add(bookingsTO);
					}
				}
				countryList.add(country);
			}
			highestVal += highestVal / 100;
		}
		this.setHighestValue(String.valueOf(highestVal));
		return channelWiseBookingInfo;
	}

	/**
	 * Gets revenue by channel data
	 * 
	 * @param request
	 * @return
	 */
	private Map<String, BigDecimal> getRevenueByChannel() {
		Float fltHightestValue = new Float(0);
		Map<String, BigDecimal> channelWiseSales = ModuleServiceLocator.getMISDataProviderBD().getMISDashSalesByChannel(
				misSearchCriteria);
		for (String channel : channelWiseSales.keySet()) {
			String salesValue = channelWiseSales.get(channel).toPlainString();
			if (Double.parseDouble(salesValue) > fltHightestValue) {
				fltHightestValue = Float.valueOf(salesValue);
			}
		}
		setRevHighestValue(StringUtils.split(fltHightestValue.toString(), ".")[0]);
		return channelWiseSales;
	}

	/**
	 * TODO: Remove this hard coded values
	 * 
	 * @param request
	 * @return
	 */
	private String getPerfomranceOfPortals(HttpServletRequest request) {
		StringBuffer strbBuffer = new StringBuffer();

		strbBuffer.append("jsPP = [");
		strbBuffer
				.append("		{porta:\"expedia.com\", sales:\"$52,400,000\", cont:\"26.2%\", bkg:\"137,281\", conv:\"97%\", comm:\"$52,400\"},");
		strbBuffer
				.append("		{porta:\"makemytrip.com\", sales:\"$1,000,000\", cont:\"0.5%\", bkg:\"4,152\", conv:\"72%\", comm:\"$1,000\"},");
		strbBuffer
				.append("		{porta:\"booknow.com\", sales:\"$10,200,000\", cont:\"5.1%\", bkg:\"45,535\", conv:\"69%\", comm:\"$10,200\"},");
		strbBuffer
				.append("		{porta:\"thomascook.com\", sales:\"$4,200,000\", cont:\"2.1%\", bkg:\"17,077\", conv:\"75%\", comm:\"$4,200\"},");
		strbBuffer
				.append("		{porta:\"akbartravels.com\", sales:\"$4,000,000\", cont:\"2.3%\", bkg:\"15,719\", conv:\"92%\", comm:\"$4,000\"},");
		strbBuffer
				.append("		{porta:\"expedia.com\", sales:\"$3,800,000\", cont:\"2%\", bkg:\"14,473\", conv:\"86%\", comm:\"$3,800\"},");
		strbBuffer
				.append("		{porta:\"travelocity.com\", sales:\"$2,400,000\", cont:\"1.9%\", bkg:\"10,706\", conv:\"87%\", comm:\"$2,400\"},");
		strbBuffer
				.append("		{porta:\"orbitz.com\", sales:\"$3,400,000\", cont:\"1.2%\", bkg:\"7,648\", conv:\"82%\", comm:\"$3,400\"},");
		strbBuffer
				.append("		{porta:\"tripadvisor.com\", sales:\"$2,200,000\", cont:\"1.7%\", bkg:\"11,708\", conv:\"89%\", comm:\"$2,200\"}");
		strbBuffer.append("		];\n");

		return strbBuffer.toString();
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public String getReturnData() {
		return returnData;
	}

	public void setReturnData(String returnData) {
		this.returnData = returnData;
	}

	public MISReportsSearchCriteria getMisSearchCriteria() {
		return misSearchCriteria;
	}

	public void setMisSearchCriteria(MISReportsSearchCriteria misSearchCriteria) {
		this.misSearchCriteria = misSearchCriteria;
	}

	public Collection<DistributionFlightInfoTO> getCountryWiseFlights() {
		return countryWiseFlights;
	}

	public void setCountryWiseFlights(Collection<DistributionFlightInfoTO> countryWiseFlights) {
		this.countryWiseFlights = countryWiseFlights;
	}

	public List<String> getCountryList() {
		return countryList;
	}

	public void setCountryList(List<String> countryList) {
		this.countryList = countryList;
	}

	public String getHighestValue() {
		return highestValue;
	}

	public void setHighestValue(String highestValue) {
		this.highestValue = highestValue;
	}

	public Map<String, String> getSalesChannelList() {
		return salesChannelList;
	}

	public void setSalesChannelList(Map<String, String> salesChannelList) {
		this.salesChannelList = salesChannelList;
	}

	public Map<String, Collection<BookingsTO>> getChannelWiseBookingsPerCountry() {
		return channelWiseBookingsPerCountry;
	}

	public void setChannelWiseBookingsPerCountry(Map<String, Collection<BookingsTO>> channelWiseBookingsPerCountry) {
		this.channelWiseBookingsPerCountry = channelWiseBookingsPerCountry;
	}

	public String getRevHighestValue() {
		return revHighestValue;
	}

	public void setRevHighestValue(String revHighestValue) {
		this.revHighestValue = revHighestValue;
	}

	public Map<String, BigDecimal> getChannelWiseRevenue() {
		return channelWiseRevenue;
	}

	public void setChannelWiseRevenue(Map<String, BigDecimal> channelWiseRevenue) {
		this.channelWiseRevenue = channelWiseRevenue;
	}

	/**
	 * Gets getSalesFactorAndYieldMap
	 * 
	 * @param request
	 * @return
	 */
	private void populateSFAndYields() {
		ReportsSearchCriteria reportsSearchCriteria = new ReportsSearchCriteria();
		Map<String, Map<String, BigDecimal>> salesFactorMap1 = new TreeMap<String, Map<String, BigDecimal>>();
		Map<String, BigDecimal> map = new TreeMap<String, BigDecimal>();
		monthsMap = new TreeMap<String, String>();

		String currentMonth = null;
		String prevMonth = null;

		Calendar calendarCurrYrAndMon = Calendar.getInstance();
		Calendar calendarCurrYrAndPrvMon = Calendar.getInstance();
		Calendar calendarPrvYrAndCurrMon = Calendar.getInstance();
		Calendar calendarPrvYrAndPrvMon = Calendar.getInstance();

		String dateRangeArray[][] = new String[4][2];

		currentMonth = getCharMonth(calendarCurrYrAndMon.get(Calendar.MONTH));
		dateRangeArray[0][0] = DAY_01 + "-" + getCharMonth(calendarCurrYrAndMon.get(Calendar.MONTH)) + "-"
				+ calendarCurrYrAndMon.get(Calendar.YEAR);
		dateRangeArray[0][1] = getMaximumDays(calendarCurrYrAndMon) + "-"
				+ getCharMonth(calendarCurrYrAndMon.get(Calendar.MONTH)) + "-" + calendarCurrYrAndMon.get(Calendar.YEAR);

		calendarCurrYrAndPrvMon.set(Calendar.MONTH, calendarCurrYrAndMon.get(Calendar.MONTH) - 1);
		prevMonth = getCharMonth(calendarCurrYrAndPrvMon.get(Calendar.MONTH));
		dateRangeArray[1][0] = DAY_01 + "-" + getCharMonth(calendarCurrYrAndPrvMon.get(Calendar.MONTH)) + "-"
				+ calendarCurrYrAndPrvMon.get(Calendar.YEAR);
		dateRangeArray[1][1] = getMaximumDays(calendarCurrYrAndPrvMon) + "-"
				+ getCharMonth(calendarCurrYrAndPrvMon.get(Calendar.MONTH)) + "-" + calendarCurrYrAndPrvMon.get(Calendar.YEAR);

		calendarPrvYrAndCurrMon.set(Calendar.YEAR, calendarCurrYrAndMon.get(Calendar.YEAR) - 1);
		dateRangeArray[2][0] = DAY_01 + "-" + getCharMonth(calendarPrvYrAndCurrMon.get(Calendar.MONTH)) + "-"
				+ calendarPrvYrAndCurrMon.get(Calendar.YEAR);
		dateRangeArray[2][1] = getMaximumDays(calendarPrvYrAndCurrMon) + "-"
				+ getCharMonth(calendarPrvYrAndCurrMon.get(Calendar.MONTH)) + "-" + calendarPrvYrAndCurrMon.get(Calendar.YEAR);

		calendarPrvYrAndPrvMon.set(Calendar.YEAR, calendarCurrYrAndMon.get(Calendar.YEAR) - 1);
		calendarPrvYrAndPrvMon.set(Calendar.MONTH, calendarCurrYrAndMon.get(Calendar.MONTH) - 1);
		dateRangeArray[3][0] = DAY_01 + "-" + getCharMonth(calendarPrvYrAndPrvMon.get(Calendar.MONTH)) + "-"
				+ calendarPrvYrAndPrvMon.get(Calendar.YEAR);
		dateRangeArray[3][1] = getMaximumDays(calendarPrvYrAndPrvMon) + "-"
				+ getCharMonth(calendarPrvYrAndPrvMon.get(Calendar.MONTH)) + "-" + calendarPrvYrAndPrvMon.get(Calendar.YEAR);

		reportsSearchCriteria.setDateRangeArray(dateRangeArray);

		reportsSearchCriteria.setRegion(misSearchCriteria.getRegion());
		reportsSearchCriteria.setStation(misSearchCriteria.getPos());
		reportsSearchCriteria.setFlightStatus("ACT");
		reportsSearchCriteria.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());

		Map<String, Map<String, Map<String, BigDecimal>>> resultMap = ModuleServiceLocator.getMISDataProviderBD()
				.getMISSeatFactorReportData(reportsSearchCriteria);
		if (resultMap != null && !resultMap.isEmpty()) {
			salesFactorMap = resultMap.get("SF");
			yieldMap = resultMap.get("YD");
			monthsMap.put("currentMonth", currentMonth);
			monthsMap.put("prevMonth", prevMonth);
		}
	}

	private static int getMaximumDays(Calendar calendar) {
		int month = calendar.get(Calendar.MONTH);
		int year = calendar.get(Calendar.YEAR);

		if (month == 1) {
			if (year % 4 == 0)
				return 29;
			else if (year % 4 != 0)
				return 28;
		}

		if (month == 0 || month == 2 || month == 4 || month == 6 || month == 7 || month == 9 || month == 11)
			return 31;

		if (month == 3 || month == 5 || month == 8 || month == 10)
			return 30;

		return -1;
	}

	private static String getCharMonth(int indMonth) {

		switch (indMonth) {
		case 0:
			return "Jan";
		case 1:
			return "Feb";
		case 2:
			return "Mar";
		case 3:
			return "Apr";
		case 4:
			return "May";
		case 5:
			return "Jun";
		case 6:
			return "Jul";
		case 7:
			return "Aug";
		case 8:
			return "Sep";
		case 9:
			return "Oct";
		case 10:
			return "Nov";
		case 11:
			return "Dec";
		default:
			break;
		}
		return "";
	}

	/**
	 * @return the salesFactorMap
	 */
	public Map<String, Map<String, BigDecimal>> getSalesFactorMap() {
		return salesFactorMap;
	}

	/**
	 * @param salesFactorMap
	 *            the salesFactorMap to set
	 */
	public void setSalesFactorMap(Map<String, Map<String, BigDecimal>> salesFactorMap) {
		this.salesFactorMap = salesFactorMap;
	}

	/**
	 * @return the yieldMap
	 */
	public Map<String, Map<String, BigDecimal>> getYieldMap() {
		return yieldMap;
	}

	/**
	 * @param yieldMap
	 *            the yieldMap to set
	 */
	public void setYieldMap(Map<String, Map<String, BigDecimal>> yieldMap) {
		this.yieldMap = yieldMap;
	}

	/**
	 * @return the monthsMap
	 */
	public Map<String, String> getMonthsMap() {
		return monthsMap;
	}

	/**
	 * @param monthsMap
	 *            the monthsMap to set
	 */
	public void setMonthsMap(Map<String, String> monthsMap) {
		this.monthsMap = monthsMap;
	}

}
