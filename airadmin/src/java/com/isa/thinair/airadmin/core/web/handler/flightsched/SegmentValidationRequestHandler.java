package com.isa.thinair.airadmin.core.web.handler.flightsched;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.flightsched.ScheduleHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airadmin.core.web.util.ScheduleUtils;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.FlightScheduleLeg;
import com.isa.thinair.airschedules.api.model.FlightScheduleSegment;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.commons.api.constants.DayOfWeek;
import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;

/**
 * @author Thushara
 * 
 */
public final class SegmentValidationRequestHandler extends BasicRequestHandler {

	private static final String PARAM_MODE = "strSaveMode";
	private static final String PARAM_LEGS = "strLegs";
	private static final String PARAM_LEG_DTLS = "strLegDtls";
	private static final String PARAM_OVERLAP = "strOverlap";
	private static final String PARAM_STARTDATE = "strStartDate";
	private static final String PARAM_STOPDATE = "strStopDate";
	private static final String PARAM_MODELNO = "strModel";
	private static final String PARAM_OPTYPE = "strOpType";
	private static final String PARAM_BUILDSTATUS = "strBldStatus";
	private static final String PARAM_MONDAY = "strMonday";
	private static final String PARAM_TUESDAY = "strTuesday";
	private static final String PARAM_WEDNESDAY = "strWednesday";
	private static final String PARAM_THURSDAY = "strThursday";
	private static final String PARAM_FRIDAY = "strFriday";
	private static final String PARAM_SATURDAY = "strSaturday";
	private static final String PARAM_SUNDAY = "strSunday";
	private static final String PARAM_SCHEDULID = "strSchdId";
	private static final String PARAM_OVERLAPSCEDID = "hdnOverlapSchID";
	private static final String PARAM_TIMEMODE = "hdnTimeMode";

	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();
	private static Log log = LogFactory.getLog(SegmentValidationRequestHandler.class);

	/**
	 * Main Execute Method for Segment Action
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;
		try {
			setDisplayData(request);
		} catch (Exception e) {
			log.error("Exception in SegmentValidationRequestHandler.execute", e);
			if (e instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, e.getMessage(), "", "");
			} else {
				saveMessage(request, e.getMessage(), WebConstants.MSG_ERROR);
			}
		}
		return forward;
	}

	/**
	 * Set data to show in the jsp file
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	public static void setDisplayData(HttpServletRequest request) throws Exception {

		setClientErrors(request);
		setSegmentValidationRowHtml(request);
		setSegmentValidationParametersHtml(request);
		setOverlapableScheduleHtml(request);
		setOverLapableSegmentRow(request);

	}

	/**
	 * Sets the Segment Validation Row Html
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	protected static void setSegmentValidationRowHtml(HttpServletRequest request) throws ModuleException {
		String strHdnMode = AiradminUtils.getNotNullString(request.getParameter(PARAM_MODE));
		String strLegs = AiradminUtils.getNotNullString(request.getParameter(PARAM_LEGS));

		ScheduleHTMLGenerator htmlGen = new ScheduleHTMLGenerator();

		String strHtml = "";
		if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_EDITLEG)) {
			strHtml = htmlGen.getSegmentValidationRowHtml(request);

		} else if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_SAVE)) {
			StringBuffer strSegbuff = new StringBuffer("var arrSegData = new Array();");
			ArrayList<String> arrSegList = ScheduleUtils.getSgements(strLegs);
			for (int i = 0; i < arrSegList.size(); i++) {
				strSegbuff.append("arrSegData[" + i + "] = new Array();");
				strSegbuff.append("arrSegData[" + i + "][0] = '" + i + "';");
				strSegbuff.append("arrSegData[" + i + "][1] = '" + arrSegList.get(i) + "';");
				strSegbuff.append("arrSegData[" + i + "][2] = 'true';");
			}
			strHtml = strSegbuff.toString();
		}

		request.setAttribute(WebConstants.REQ_HTML_SEGMENT_VALIDATION_DATA, strHtml);
	}

	/**
	 * Sets the Parameters need to validate segment
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setSegmentValidationParametersHtml(HttpServletRequest request) throws ModuleException {
		// if a overlapped schedule is exists set the overlapped schedule id to the request object
		String strLegsHtml = AiradminUtils.getNotNullString(request.getParameter(PARAM_LEGS));
		String strLegDtlsHtml = AiradminUtils.getNotNullString(request.getParameter(PARAM_LEG_DTLS));
		String strOverlapHtml = AiradminUtils.getNotNullString(request.getParameter(PARAM_OVERLAP));

		request.setAttribute(WebConstants.REQ_HTML_LEGS, strLegsHtml);
		request.setAttribute(WebConstants.REQ_HTML_LEG_DTLS, strLegDtlsHtml);
		request.setAttribute(WebConstants.REQ_HTML_OVERLAPFLAG, strOverlapHtml);
	}

	/**
	 * Sets the Client Validations in the Segment Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static void setClientErrors(HttpServletRequest request) throws ModuleException {

		String strClientErrors = "";
		Properties moduleErrs = new Properties();
		moduleErrs.setProperty("um.flightschedule.validseg.validseg", "onevalidseg");
		moduleErrs.setProperty("um.flightschedule.validseg.nosegfound", "nosegfound");
		moduleErrs.setProperty("um.flightschedule.validseg.needinvalid", "needinvalid");
		strClientErrors = JavascriptGenerator.createClientErrors(moduleErrs);

		if (strClientErrors == null) {
			strClientErrors = "";
		}
		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
	}

	/**
	 * Sets the Overlapable Schedules List (if there's one)
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static void setOverlapableScheduleHtml(HttpServletRequest request) throws ModuleException {
		String strOverlapSced = ScheduleUtils.createOverlapableScheduleList(createFlightSchedule(request));
		request.setAttribute(WebConstants.REQ_HTML_OVERLAPABLE_SCHEDULE_DATA, strOverlapSced);
	}

	/**
	 * Sets the Grid Data if there's Overlappable Schedules
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setOverLapableSegmentRow(HttpServletRequest request) throws ModuleException {
		ScheduleHTMLGenerator htmlGen = new ScheduleHTMLGenerator();
		String strOverLapSegArr = htmlGen.createSegmentValidationRowHtml(createFlightSchedule(request));
		request.setAttribute(WebConstants.REQ_HTML_OVERLAPABLE_SCHEDULE_ARRAY_DATA, strOverLapSegArr);
	}

	/**
	 * Creates the Flight From Request Data
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return FlightSchedule the Created FlightSchedule
	 */
	private static FlightSchedule createFlightSchedule(HttpServletRequest request) {

		String strLegDtlsHtml = AiradminUtils.getNotNullString(request.getParameter(PARAM_LEG_DTLS));
		String strStartDate = AiradminUtils.getNotNullString(request.getParameter(PARAM_STARTDATE));
		String strStopDAte = AiradminUtils.getNotNullString(request.getParameter(PARAM_STOPDATE));
		String strModelNo = AiradminUtils.getNotNullString(request.getParameter(PARAM_MODELNO));
		String strMonday = AiradminUtils.getNotNullString(request.getParameter(PARAM_MONDAY));
		String strTuesday = AiradminUtils.getNotNullString(request.getParameter(PARAM_TUESDAY));
		String strWednesday = AiradminUtils.getNotNullString(request.getParameter(PARAM_WEDNESDAY));
		String strThursday = AiradminUtils.getNotNullString(request.getParameter(PARAM_THURSDAY));
		String strFriday = AiradminUtils.getNotNullString(request.getParameter(PARAM_FRIDAY));
		String strSaturday = AiradminUtils.getNotNullString(request.getParameter(PARAM_SATURDAY));
		String strSunday = AiradminUtils.getNotNullString(request.getParameter(PARAM_SUNDAY));
		String strSchedId = request.getParameter(PARAM_SCHEDULID);
		String strHdnOverlapschedId = AiradminUtils.getNotNullString(request.getParameter(PARAM_OVERLAPSCEDID));
		String strTimeinMode = AiradminUtils.getNotNullString(request.getParameter(PARAM_TIMEMODE));
		String strOptype = AiradminUtils.getNotNullString(request.getParameter(PARAM_OPTYPE));
		String strBuildStatus = AiradminUtils.getNotNullString(request.getParameter(PARAM_BUILDSTATUS));

		FlightSchedule flightSched = new FlightSchedule();

		String defaultFormat // get the default date format
		= globalConfig.getBizParam(SystemParamKeys.DATE_FORMAT_FOR_DATE_STRINGS);
		SimpleDateFormat dateFormat = new SimpleDateFormat(defaultFormat);

		flightSched.setModelNumber(strModelNo);
		if (!strOptype.equals("")) {
			flightSched.setOperationTypeId(Integer.parseInt(strOptype));
		}
		if (!strBuildStatus.equals("") && !strBuildStatus.equals("null")) {
			flightSched.setBuildStatusCode(strBuildStatus);
		}

		// seeting the scedule id if available
		if (strSchedId != null && !"".equals(strSchedId.trim())) {
			flightSched.setScheduleId(new Integer(strSchedId));
		}
		// setting the schedule frequency
		Collection<DayOfWeek> dayList = new ArrayList<DayOfWeek>();

		if (strSunday.equals(WebConstants.APP_BOOLEAN_TRUE))
			dayList.add(DayOfWeek.SUNDAY);
		if (strMonday.equals(WebConstants.APP_BOOLEAN_TRUE))
			dayList.add(DayOfWeek.MONDAY);
		if (strTuesday.equals(WebConstants.APP_BOOLEAN_TRUE))
			dayList.add(DayOfWeek.TUESDAY);
		if (strWednesday.equals(WebConstants.APP_BOOLEAN_TRUE))
			dayList.add(DayOfWeek.WEDNESDAY);
		if (strThursday.equals(WebConstants.APP_BOOLEAN_TRUE))
			dayList.add(DayOfWeek.THURSDAY);
		if (strFriday.equals(WebConstants.APP_BOOLEAN_TRUE))
			dayList.add(DayOfWeek.FRIDAY);
		if (strSaturday.equals(WebConstants.APP_BOOLEAN_TRUE))
			dayList.add(DayOfWeek.SATURDAY);

		Frequency fqn = CalendarUtil.getFrequencyFromDays(dayList);

		if (strTimeinMode.equals(WebConstants.LOCALTIME)) {
			flightSched.setStartDateLocal(ScheduleUtils.parseToDate(strStartDate.trim()));
			flightSched.setStopDateLocal(ScheduleUtils.parseToDate(strStopDAte.trim()));
			flightSched.setFrequencyLocal(fqn);

		} else {
			flightSched.setStartDate(ScheduleUtils.parseToDate(strStartDate.trim()));
			flightSched.setStopDate(ScheduleUtils.parseToDate(strStopDAte.trim()));
			flightSched.setFrequency(fqn);
		}
		StringTokenizer legTok = new StringTokenizer(strLegDtlsHtml, ",");

		// setting schedule legs
		Set<FlightScheduleLeg> legSet = new HashSet<FlightScheduleLeg>();
		int i = 1;
		while (legTok.hasMoreTokens()) {
			String leg = legTok.nextToken();
			String[] legArr = leg.split("_");
			FlightScheduleLeg fScheLeg = new FlightScheduleLeg();
			fScheLeg.setOrigin(legArr[0]);
			fScheLeg.setDestination(legArr[1]);
			fScheLeg.setLegNumber(i);
			try {
				if (strTimeinMode.equals(WebConstants.LOCALTIME)) {
					fScheLeg.setEstDepartureTimeLocal(dateFormat.parse(legArr[2].trim()));
					fScheLeg.setEstArrivalTimeLocal(dateFormat.parse(legArr[3].trim()));
					fScheLeg.setEstDepartureDayOffsetLocal(Integer.parseInt(legArr[4]));
					fScheLeg.setEstArrivalDayOffsetLocal(Integer.parseInt(legArr[5]));
				} else {
					fScheLeg.setEstDepartureTimeZulu(dateFormat.parse(legArr[2].trim()));
					fScheLeg.setEstArrivalTimeZulu(dateFormat.parse(legArr[3].trim()));
					fScheLeg.setEstDepartureDayOffset(Integer.parseInt(legArr[4]));
					fScheLeg.setEstArrivalDayOffset(Integer.parseInt(legArr[5]));
				}
			} catch (Exception e) {
				log.error(e);
			}
			legSet.add(fScheLeg);
			i++;
		}
		flightSched.setFlightScheduleLegs(legSet);
		HashMap<String, FlightSegement> segTerminals = new HashMap<String, FlightSegement>();
		HashSet<FlightScheduleSegment> segmentSet = ScheduleUtils.getSegments(legSet, segTerminals);
		flightSched.setFlightScheduleSegments(segmentSet);
		if (!(strHdnOverlapschedId.equals("")) && !(strHdnOverlapschedId.equals("null"))) {
			flightSched.setOverlapingScheduleId(new Integer(strHdnOverlapschedId));
		}
		return flightSched;
	}

}
