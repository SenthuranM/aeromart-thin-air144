/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airadmin.core.web.handler.reports;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * 
 * @author Chamindap & Shakir
 * 
 */
public class ReservationsReportRequestHandler extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(ReservationsReportRequestHandler.class);

	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	/**
	 * Main Execute Method for Reservation Breake Down Report Action
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @param response
	 *            the HttpServletResponse
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {
		String forward = WebConstants.ACTION_FORWARD_SUCCESS;
		String strHdnMode = request.getParameter("hdnMode");

		try {
			setDisplayData(request);
			log.debug("ReservationsReportRequestHandler SETDISPLAYDATA() SUCCESS");
		} catch (Exception e) {
			log.error("ReservationsReportRequestHandler SETDISPLAYDATA() FAILED " + e.getMessage());
		}
		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				log.error("CallCentreSalesReportRequestHandler setReportView Success");
				return null;
			} else {
				log.error("CallCentreSalesReportRequestHandler setReportView not selected");
			}
		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("CallCentreSalesReportRequestHandler setReportView Failed " + e.getMessageString());
		}
		return forward;
	}

	/**
	 * Sets the Client Validations to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setClientErrors(HttpServletRequest request) {

		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Display data For Reservation Breake Down Report
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	private static void setDisplayData(HttpServletRequest request) throws Exception {
		setAgentTypes(request);
		setClientErrors(request);
		setGSAMultiSelectList(request);
		setStationComboList(request);
		setLiveStatus(request);
		setReportingPeriod(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);
	}

	/**
	 * Sets the Agent Multi Select Box to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setGSAMultiSelectList(HttpServletRequest request) throws ModuleException {

		boolean blnWithTAs = false;
		boolean blnWithCOs = false;
		String strAgentType = "";

		if (request.getParameter("selAgencies") != null) {
			strAgentType = request.getParameter("selAgencies");
		}
		if (request.getParameter("chkTAs") != null) {
			blnWithTAs = (request.getParameter("chkTAs").equals(WebConstants.CHECKBOX_ON) ? true : false);
		}
		if (request.getParameter("chkCOs") != null) {
			blnWithCOs = (request.getParameter("chkCOs").equals(WebConstants.CHECKBOX_ON) ? true : false);
		}
		String strList = ReportsHTMLGenerator.createAgentGSAMultiSelect(strAgentType, blnWithTAs, blnWithCOs);
		request.setAttribute(WebConstants.REQ_HTML_GSA_LIST, strList);
	}

	/**
	 * Sets the Agent types to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setAgentTypes(HttpServletRequest request) throws ModuleException {
		String strATList = SelectListGenerator.createAgentTypeList_SG();
		request.setAttribute(WebConstants.REQ_HTML_AGENTTYPE_LIST, strATList);
	}

	/**
	 * Sets the Station List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setStationComboList(HttpServletRequest request) throws ModuleException {
		String strStation = SelectListGenerator.createAirportsList();
		request.setAttribute(WebConstants.REQ_STATION_LIST, strStation);
	}

	/**
	 * Sets the report medium to the request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleExceptions
	 */
	private static void setLiveStatus(HttpServletRequest request) throws ModuleException {
		String strLive = request.getParameter(WebConstants.REP_HDN_LIVE);
		if (strLive != null)
			request.setAttribute(WebConstants.REP_SET_LIVE, strLive);

	}

	/**
	 * Setting the reporting period to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setReportingPeriod(HttpServletRequest request) throws ModuleException {
		int period = new Integer(globalConfig.getBizParam(SystemParamKeys.REPORTING_PERIOD)).intValue();
		request.setAttribute(WebConstants.REP_RPT_START_DAY, ReportsHTMLGenerator.getReportingPeriod(period));
	}

	/**
	 * Displays the Reservation Breake Down Reports
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		ResultSet resultSet = null;
		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String value = request.getParameter("radReportOption");
		String[] strAgents = request.getParameter("hdnAgents").split(",");
		String strReservation = request.getParameter("chkOption");
		String strDeatil = request.getParameter("hdnDeatail");
		String agentName = request.getParameter("agentName");
		Collection<String> agentCol = new ArrayList<String>();
		Collection<String> paymentCol = new ArrayList<String>();
		String strAgent = request.getParameter("hdnAgent");
		String strSegment = request.getParameter("chkSegment");
		String strFrom = request.getParameter("selFrom");
		String strTo = request.getParameter("selTo");
		String strFlightNo = request.getParameter("txtFlight");
		String strTotal = request.getParameter("total");
		String reportTemplate = null;
		String strLogo = AppSysParamsUtil.getReportLogo(false); // globalConfig.getBizParam(SystemParamKeys.REPORT_LOGO);
		String strPath = "../../images/" + AppSysParamsUtil.getReportLogo(true); // AA145.jpg
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String strBase = globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY);
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);

		try {
			ReportsSearchCriteria search = new ReportsSearchCriteria();

			search.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate));
			search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate));

			for (int i = 0; i < strAgents.length; i++) {
				agentCol.add(strAgents[i]);
			}
			search.setAgents(agentCol);
			search.setPaymentTypes(paymentCol);

			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("FROM_DATE", fromDate);
			parameters.put("TO_DATE", toDate);
			parameters.put("GSA_DETAIL", request.getParameter("hdnAgents"));
			parameters.put("REPORT_TYPE", value);
			parameters.put("AGENT_DETAIL", agentName);
			parameters.put("AGENT_SEGMENT", strFrom + "/" + strTo);
			parameters.put("AGENT_FLIGHT", strFlightNo);
			parameters.put("AGENT_DETCODE", strAgent);
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
			parameters.put("AMT_1", "(" + strBase + ")");
			parameters.put("REPORT_MEDIUM", strlive);

			// To provide Report Format Options
			String reportNumFormat = request.getParameter("radRptNumFormat");
			ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

			if (strTotal != null) {
				String[] strTotVal = strTotal.split(",");
				parameters.put("TTL_FARE", strTotVal[0]);
				parameters.put("TTL_TAX", strTotVal[1]);
				parameters.put("TTL_SURCHARGE", strTotVal[2]);
				parameters.put("TTL_TOTAL", strTotVal[3]);
				parameters.put("TTL_REFUND", strTotVal[4]);
			}

			if (strDeatil != null && strDeatil.equals("DETAIL")) {
				reportTemplate = "ReservationBreakdownDetail.jasper";
				parameters.put("REPORT_ID", "UC_REPM_001_1");
				ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs(WebConstants.REPORT_REF,
						ReportsHTMLGenerator.getReportTemplate(reportTemplate));
				if (strAgent != null) {
					search.setAgentCode(strAgent);
				}
				resultSet = ModuleServiceLocator.getDataExtractionBD().getReservationBreakdownDetailData(search);
			} else if (strReservation != null && strReservation.equals("TS")) {
				parameters.put("REPORT_ID", "UC_REPM_001_2");
				reportTemplate = "ReservationBreakdownTaxes.jasper";
				ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs(WebConstants.REPORT_REF,
						ReportsHTMLGenerator.getReportTemplate(reportTemplate));
				resultSet = ModuleServiceLocator.getDataExtractionBD().getReservationBreakdownTaxesData(search);
			} else if (strReservation != null && strReservation.equals("PR") && strSegment != null
					&& strSegment.equals("SEGMENT")) {
				parameters.put("REPORT_ID", "UC_REPM_001_3");
				search.setSegment(strSegment);
				search.setFrom(strFrom);
				search.setTo(strTo);
				reportTemplate = "ResBreakdownSegment.jasper";
				ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs(WebConstants.REPORT_REF,
						ReportsHTMLGenerator.getReportTemplate(reportTemplate));
				resultSet = ModuleServiceLocator.getDataExtractionBD().getReservationBreakdownDetailData(search);
			} else if (strReservation != null && strReservation.equals("PR") && strSegment != null && strSegment.equals("FLIGHT")) {
				parameters.put("REPORT_ID", "UC_REPM_001_4");
				search.setSegment(strSegment);
				search.setFlightNumber(strFlightNo);
				reportTemplate = "ResBreakdownFlight.jasper";
				ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs(WebConstants.REPORT_REF,
						ReportsHTMLGenerator.getReportTemplate(reportTemplate));
				resultSet = ModuleServiceLocator.getDataExtractionBD().getReservationBreakdownDetailData(search);
			} else {
				reportTemplate = "ReservationBreakdown.jasper";
				parameters.put("REPORT_ID", "UC_REPM_001");
				ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs(WebConstants.REPORT_REF,
						ReportsHTMLGenerator.getReportTemplate(reportTemplate));
				resultSet = ModuleServiceLocator.getDataExtractionBD().getReservationBreakdownSummaryData(search);
			}

			if (value.trim().equals(WebConstants.REPORT_HTML)) {
				parameters.put("IMG_PATH", strPath);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport(WebConstants.REPORT_REF, parameters, resultSet,
						null, null, response);

			} else if (value.trim().equals(WebConstants.REPORT_PDF)) {
				strPath = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG_PATH", strPath);
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=ReservationBreakdownDetail.pdf");
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport(WebConstants.REPORT_REF, parameters, resultSet,
						response);
			} else if (value.trim().equals(WebConstants.REPORT_EXCEL)) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=ReservationBreakdownDetail.xls");
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport(WebConstants.REPORT_REF, parameters, resultSet,
						response);
			} else if (value.trim().equals("CSV")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=ReservationBreakdownDetail.csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);
			}
		} catch (Exception e) {
			log.error(e);
		}
	}
}
