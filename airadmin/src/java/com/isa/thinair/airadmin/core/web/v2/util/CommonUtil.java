package com.isa.thinair.airadmin.core.web.v2.util;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONUtil;
import com.isa.thinair.airadmin.core.web.v2.dto.mis.MISProcessParams;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.commons.api.dto.LogicalCabinClassDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.gdsservices.core.util.MessageUtil;
import com.isa.thinair.webplatform.api.util.Constants;

/**
 * Utility class for common methods. mod
 * 
 * @author lalanthi
 * 
 */
public class CommonUtil {

	static SimpleDateFormat dtfmt = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

	/**
	 * <b> Deprecated </b> Directly use JSONUtil.serialize
	 * 
	 * @param rpParams
	 * @return
	 */
	@Deprecated
	public static String convertToJSON(Object object) {
		try {
			return JSONUtil.serialize(object);
		} catch (JSONException e) {
			return null;
		}
	}

	/**
	 * <b> Deprecated </b> Directly use JSONUtil.serialize
	 * 
	 * @param rpParams
	 * @return
	 */
	@Deprecated
	public static String convertToJSON(MISProcessParams rpParams) {
		try {
			return JSONUtil.serialize(rpParams);
		} catch (JSONException e) {
			return null;
		}
	}

	public static Map<String, String> getMonthNames() {
		Map<String, String> monthsMap = new TreeMap<String, String>();
		monthsMap.put("01", "Jan");
		monthsMap.put("02", "Feb");
		monthsMap.put("03", "Mar");
		monthsMap.put("04", "Apr");
		monthsMap.put("05", "May");
		monthsMap.put("06", "Jun");
		monthsMap.put("07", "Jul");
		monthsMap.put("08", "Aug");
		monthsMap.put("09", "Sep");
		monthsMap.put("10", "Oct");
		monthsMap.put("11", "Nov");
		monthsMap.put("12", "Dec");

		return monthsMap;
	}

	/**
	 * gets formated message
	 * 
	 * @param messageCode
	 * @param args
	 * @return
	 */
	public static String getFormattedMessage(String messageCode, List<String> args) {
		String strMsg = MessageUtil.getMessage(messageCode);

		if (args != null) {
			for (int i = 0; i < args.size(); i++) {
				strMsg = strMsg.replace(String.valueOf("#" + (i + 1)), args.get(i));
			}
		}

		return strMsg;
	}

	public static String getRequiredCabinClassOrLogicalCabinClass(String logicalCabinClass) {
		Map<String, LogicalCabinClassDTO> logicalCabinClasses = CommonsServices.getGlobalConfig().getAvailableLogicalCCMap();
		Map<String, String> cabinClasses = CommonsServices.getGlobalConfig().getActiveCabinClassesMap();
		LogicalCabinClassDTO logicalCabinClassDTO = logicalCabinClasses.get(logicalCabinClass);
		if (logicalCabinClassDTO != null) {
			if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
				return logicalCabinClassDTO.getDescription();
			} else {
				return cabinClasses.get(logicalCabinClassDTO.getCabinClassCode());
			}
		} else {
			return "";
		}
	}

	public static String[] getSplittedCabinClassAndLogicalCabinClass(String cabinClassAndLogicalCabinClass) {
		String[] returnCabinClassAndLogicalCabinClass = new String[2];
		String[] splittedCabinClassAndLogicalCabinClass = cabinClassAndLogicalCabinClass
				.split(Constants.UNDERSCORE_SEPERATOR);
		if (splittedCabinClassAndLogicalCabinClass[0].trim().equalsIgnoreCase(splittedCabinClassAndLogicalCabinClass[1].trim())) {
			returnCabinClassAndLogicalCabinClass[0] = splittedCabinClassAndLogicalCabinClass[0].trim();
		} else {
			returnCabinClassAndLogicalCabinClass[1] = splittedCabinClassAndLogicalCabinClass[0].trim().split(
					"\\" + Constants.CARET_SEPERATOR)[0];
		}
		return returnCabinClassAndLogicalCabinClass;
	}

	public static String concatanateCabinClassAndLogicalCabinClass(String cabinClass, String logicalCabinClass)
			throws ModuleException {
		if (!BeanUtils.nullHandler(cabinClass).equals("") && !BeanUtils.nullHandler(logicalCabinClass).equals("")) {
			return logicalCabinClass + Constants.CARET_SEPERATOR
					+ Constants.UNDERSCORE_SEPERATOR + cabinClass;
		} else if (!BeanUtils.nullHandler(cabinClass).equals("")) {
			return cabinClass + Constants.UNDERSCORE_SEPERATOR + cabinClass;
		} else if (!BeanUtils.nullHandler(logicalCabinClass).equals("")) {
			return logicalCabinClass + Constants.CARET_SEPERATOR
					+ Constants.UNDERSCORE_SEPERATOR
					+ CommonsServices.getGlobalConfig().getAvailableLogicalCCMap().get(logicalCabinClass).getCabinClassCode();
		}
		throw new ModuleException("airschedules.arg.invalid.null");
	}

	public static String getCabinClassOrLogicalCabinClassDescription(String cabinClass, String logicalCabinClass)
			throws ModuleException {
		if (!BeanUtils.nullHandler(logicalCabinClass).equals("")) {
			return CommonUtil.getRequiredCabinClassOrLogicalCabinClass(logicalCabinClass);
		} else if (!BeanUtils.nullHandler(cabinClass).equals("")) {
			return CommonsServices.getGlobalConfig().getActiveCabinClassesMap().get(cabinClass);
		}
		throw new ModuleException("airschedules.arg.invalid.null");
	}

	public static Map<String, String> sortByValue(Map<String, String> map) {
		List<Map.Entry<String, String>> list = new LinkedList<Map.Entry<String, String>>(map.entrySet());

		Collections.sort(list, new Comparator<Map.Entry<String, String>>() {

			public int compare(Map.Entry<String, String> m1, Map.Entry<String, String> m2) {
				return (m1.getValue()).compareTo(m2.getValue());
			}
		});

		Map<String, String> result = new LinkedHashMap<String, String>();
		for (Map.Entry<String, String> entry : list) {
			result.put(entry.getKey(), entry.getValue());
		}
		return result;
	}
}
