//$Id$
package com.isa.thinair.airadmin.core.web.generator.security;

import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.commons.api.exception.ModuleException;

public class PasswordChangeHG {

	private static String clientErrors;

	/**
	 * Creates the Client validations for password Change Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array containing Client Validations
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {

		if (clientErrors == null) {
			Properties moduleErrs = new Properties();

			moduleErrs.setProperty("um.user.form.oldpassword.required", "userOldPasswordRqrd");
			moduleErrs.setProperty("um.user.form.newpassword.required", "userNewPasswordRqrd");
			moduleErrs.setProperty("um.user.form.password.cmbinvalid", "cmbinvalid");
			moduleErrs.setProperty("um.user.form.confpassword.required", "userConfPasswordRqrd");
			moduleErrs.setProperty("um.user.form.password.not.confirmed", "userPasswordNotConf");
			moduleErrs.setProperty("um.user.form.newpassword.length", "userNewPasswordlength");
			moduleErrs.setProperty("um.user.form.confpassword.length", "userConfPasswordlength");
			moduleErrs.setProperty("um.user.form.oldnewpassword.cannotsame", "userSamePassword");

			clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}
		return clientErrors;
	}
}
