package com.isa.thinair.airadmin.core.web.handler.reports;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;

public class InboundOutBoundConnectionsReportRH extends BasicRequestHandler {
	private static Log log = LogFactory.getLog(InboundOutBoundConnectionsReportRH.class);

	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	/**
	 * The constructor.
	 */
	public InboundOutBoundConnectionsReportRH() {
		super();
	}

	/**
	 * @param request
	 * @return
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {

		String strHdnMode = request.getParameter("hdnMode");
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;

		String strConnectionTime // get the Max connection time
		= globalConfig.getBizParam(SystemParamKeys.MAX_TRANSIT_TIME);
		request.setAttribute(WebConstants.REQ_MAX_CON_TIME, strConnectionTime);

		try {
			setDisplayData(request);
			log.debug("PaymentReportRequestHandler SETDISPLAYDATA() SUCCESS");
		} catch (Exception e) {
			log.error("PaymentReportRequestHandler SETDISPLAYDATA() FAILED " + e.getMessage());
		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				log.error("PaymentReportRequestHandler setReportView Success");
				return null;
			} else {
				log.error("PaymentReportRequestHandler setReportView not selected");
			}

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("PaymentReportRequestHandler setReportView Failed " + e.getMessageString());
		}

		return forward;
	}

	public static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());

		}
	}

	/**
	 * 
	 * @param request
	 * @throws Exception
	 */
	public static void setDisplayData(HttpServletRequest request) throws Exception {
		setClientErrors(request);
		setCountryList(request);
		setLiveStatus(request);
		setReportingPeriod(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setCountryList(HttpServletRequest request) throws ModuleException {
		String strList = ReportsHTMLGenerator.createCountryHtml();
		request.setAttribute(WebConstants.REQ_HTML_GSA_LIST, strList);
	}

	/**
	 * Sets the report medium to the request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleExceptions
	 */
	private static void setLiveStatus(HttpServletRequest request) throws ModuleException {
		String strLive = request.getParameter(WebConstants.REP_HDN_LIVE);
		if (strLive != null)
			request.setAttribute(WebConstants.REP_SET_LIVE, strLive);

	}

	/**
	 * Setting the reporting period to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setReportingPeriod(HttpServletRequest request) throws ModuleException {
		int period = new Integer(globalConfig.getBizParam(SystemParamKeys.REPORTING_PERIOD)).intValue();
		request.setAttribute(WebConstants.REP_RPT_START_DAY, ReportsHTMLGenerator.getReportingPeriod(period));
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		ResultSet resultSet = null;
		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String value = request.getParameter("radReportOption");
		String flightNo = request.getParameter("FlightNo");
		String countries = request.getParameter("hdnCountries");
		String inbOut = request.getParameter("radOpt");
		String strConTime = request.getParameter("txtMaxConTime");

		String id = "UC_REPM_004";
		String inboundTemplate = "InBoundConnections.jasper";
		String outboundTemplate = "OutBoundConnections.jasper";
		String reportName = "InboundConnectionsReport";
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);

		try {
			if (inbOut.equals("I")) {
				reportName = "InboundConnectionsReport";
				ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
						ReportsHTMLGenerator.getReportTemplate(inboundTemplate));

			} else {
				reportName = "OutboundConnectionsReport";
				ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
						ReportsHTMLGenerator.getReportTemplate(outboundTemplate));

			}

			ReportsSearchCriteria search = new ReportsSearchCriteria();
			if (fromDate != null && !fromDate.equals("")) {
				search.setDateFrom(ReportsHTMLGenerator.toDateType(fromDate, true));
			}

			if (toDate != null && !toDate.equals("")) {
				search.setDateTo(ReportsHTMLGenerator.toDateType(toDate, false));
			}

			search.setFlightNumber(flightNo.toUpperCase());
			search.setCountryOfResidence(countries);
			search.setReportOption(inbOut);
			search.setConTime(strConTime);

			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}

			resultSet = ModuleServiceLocator.getDataExtractionBD().getInbDatas(search);

			String strLogo = AppSysParamsUtil.getReportLogo(false);// globalConfig.getBizParam(SystemParamKeys.REPORT_LOGO);
			String reportsRootDir = "../../images/" + AppSysParamsUtil.getReportLogo(true);
			String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("ID", id);
			parameters.put("FROM_DATE", fromDate);
			parameters.put("TO_DATE", toDate);
			parameters.put("FLIGHR_NO", flightNo.toUpperCase());
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);

			// To provide Report Format Options
			String reportNumFormat = request.getParameter("radRptNumFormat");
			ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

			if (value.trim().equals("HTML")) {
				parameters.put("IMG", reportsRootDir);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null, null,
						response);
			} else if (value.trim().equals("PDF")) {
				reportsRootDir = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG", reportsRootDir);
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=" + reportName + ".pdf");
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("EXCEL")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=" + reportName + ".xls");
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals(WebConstants.REPORT_CSV)) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=" + reportName + ".csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport(WebConstants.REPORT_REF, parameters, resultSet,
						response);
			}
		} catch (Exception e) {
			log.error(e);
		}
	}

}
