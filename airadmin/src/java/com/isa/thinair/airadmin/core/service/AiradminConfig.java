package com.isa.thinair.airadmin.core.service;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Thushara Keeps configurations used in airadmin module
 */
public class AiradminConfig {

	private static Log log = LogFactory.getLog(AiradminConfig.class);

	private static final String USER_MESSEAGES_CODES_RESOURCE_BUNDLE_NAME = "resources/resource-bundles/airadminusermessages";

	private static final String URL_PRIVILEDGES_RESOURCE_BUNDLE_NAME = "resources/resource-bundles/UrlPriviledgesMappings";

	private static final String CLIENT_MESSEAGES_RESOURCE_BUNDLE_NAME = "resources/resource-bundles/AirAdminClientMessages";

	private static final String SERVER_MESSEAGES_RESOURCE_BUNDLE_NAME = "resources/resource-bundles/AirAdminServerMessages";

	private static final String DEFULT_USER_MESSAGE_CODE = "um.module.usermessage.code.empty";

	private static final String DEFULT_CLIENT_MESSAGE_CODE = "default.client.messsage";

	private static final String DEFULT_SERVER_MESSAGE_CODE = "default.server.messsage";

	private static ResourceBundle clientMessageCodes = null;

	private static ResourceBundle serverMessageCodes = null;

	private static ResourceBundle urlPriviledgesMappings;

	ResourceBundle userMessagesCodes;

	public static String defaultLanguage = "en";

	public String getDefaultLanguage() {
		return defaultLanguage;
	}

	@SuppressWarnings("static-access")
	public void setDefaultLanguage(String localeLanguage) {
		this.defaultLanguage = localeLanguage;
	}

	private void loadResources() {
		if (clientMessageCodes == null) {
			clientMessageCodes = loadResourceBundle(CLIENT_MESSEAGES_RESOURCE_BUNDLE_NAME);
		}
		if (serverMessageCodes == null) {
			serverMessageCodes = loadResourceBundle(SERVER_MESSEAGES_RESOURCE_BUNDLE_NAME);
		}
	}

	public AiradminConfig() {
		super();
		loadResourceBundles();
		loadResources();
	}

	/**
	 * Gets the Appropriate Message from the Property File for a given message code
	 * 
	 * @param messageCode
	 *            the Message Code
	 * @return String the Message
	 */
	public String getMessage(String messageCode) {
		if (messageCode == null || messageCode.equals(""))
			return userMessagesCodes.getString(DEFULT_USER_MESSAGE_CODE);
		if (messageCode.startsWith("um.")) {
			String message = userMessagesCodes.getString(messageCode);
			if (message != null)
				return message;
		}
		return userMessagesCodes.getString(DEFULT_USER_MESSAGE_CODE);
	}

	/**
	 * Gets the Appropriate Message from the Property File for a given message code
	 * 
	 * @param messageCode
	 *            the Message Code
	 * @return String the Message
	 */
	public String getMessage(String messageCode, Object[] params) {
		String message = null;

		if (messageCode == null || messageCode.equals("")) {
			message = userMessagesCodes.getString(DEFULT_USER_MESSAGE_CODE);

		} else if (messageCode.startsWith("um.")) {

			message = userMessagesCodes.getString(messageCode);
			if (message == null) {
				message = userMessagesCodes.getString(DEFULT_USER_MESSAGE_CODE);
			}
		} else {

			message = userMessagesCodes.getString(DEFULT_USER_MESSAGE_CODE);
		}

		MessageFormat messageFormat = new MessageFormat(message);
		return messageFormat.format(params);
	}

	/**
	 * Gets the Server Message from the Property File for a given Message code
	 * 
	 * @param messageCode
	 *            the Message Code
	 * @return String the Message
	 */
	public static final String getServerMessage(String messageCode) {
		if (messageCode == null || messageCode.equals("")) {
			return serverMessageCodes.getString(DEFULT_SERVER_MESSAGE_CODE);
		} else {
			return serverMessageCodes.getString(messageCode);
		}
	}

	/**
	 * Gets the client messages from a Proerty * no file is defined
	 * 
	 * @param messageCode
	 * @return
	 */
	public static final String getClientMessage(String messageCode) {
		if (messageCode == null || messageCode.equals("")) {
			return clientMessageCodes.getString(DEFULT_CLIENT_MESSAGE_CODE);
		} else {
			return clientMessageCodes.getString(messageCode);
		}
	}

	/**
	 * Loads the Resource Bundle According to the Locale & Privileges mapping Use en loclace can be configured to take
	 * any other locale
	 * 
	 */
	private void loadResourceBundles() {
		Locale locale = new Locale((getDefaultLanguage() != null) ? getDefaultLanguage() : "en");
		this.userMessagesCodes = ResourceBundle.getBundle(USER_MESSEAGES_CODES_RESOURCE_BUNDLE_NAME, locale);

		if (urlPriviledgesMappings == null) {
			urlPriviledgesMappings = loadResourceBundle(URL_PRIVILEDGES_RESOURCE_BUNDLE_NAME);
		}
	}

	/**
	 * Load the Resource Bundle
	 * 
	 * @param bundleName
	 *            Bundle Name
	 * @return ResourceBundle the Resource Bundle
	 */
	private final synchronized ResourceBundle loadResourceBundle(String bundleName) {
		ResourceBundle messageCodes = null;
		Locale locale = new Locale((getDefaultLanguage() != null) ? getDefaultLanguage() : "en");

		try {
			messageCodes = ResourceBundle.getBundle(bundleName, locale);

		} catch (RuntimeException re) {
			log.fatal("Cannot load the resouce bundle:" + bundleName + ":" + re.getMessage());
			throw re;
		}

		return messageCodes;
	}

	/**
	 * gets the Privilege for the Url (key)
	 * 
	 * @param url
	 *            the Url
	 * @return String the privilege
	 */
	public final String getUrlPriviledge(String url) {
		String priviledge = null;

		try {
			priviledge = urlPriviledgesMappings.getString(url);
		} catch (RuntimeException e) {
			if (!(e instanceof MissingResourceException)) {
				throw e;
			}
		}
		return priviledge;
	}

}
