package com.isa.thinair.airadmin.core.web.v2.action.promotion;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.AiradminModuleConfig;
import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.promotion.api.to.bundledFare.BundledFareDisplayDTO;
import com.isa.thinair.promotion.api.to.bundledFare.BundledFareDisplaySettingsDTO;
import com.isa.thinair.promotion.api.to.constants.BundledFareConstants;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.JSONFETOParser;

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class BundledFaresDisplaySettingsAction extends BaseRequestAwareAction {

	private static final Log log = LogFactory.getLog(BundledFaresDisplaySettingsAction.class);
	private static AiradminConfig airadminConfig = new AiradminConfig();

	private BundledFareDisplayDTO displayDTO;
	private Integer bundledFarePeriodId;

	private String uploadImage;
	private Integer displayTemplateId;
	private String nameTranslations;
	private String descTranslations;

	private boolean success = true;
	private String messageTxt;

	public String search() {
		try {
			validateSearchOperation();
			displayDTO = ModuleServiceLocator.getBundledFareBD().getBundledFareDisplayDTO(bundledFarePeriodId);

		} catch (ModuleException me) {
			success = false;
			messageTxt = airadminConfig.getMessage(me.getExceptionCode());
			log.error(messageTxt, me);
		} catch (Exception e) {
			success = false;
			messageTxt = airadminConfig.getMessage("um.promotion.bundled.fare.display.settings.search.fail");
			log.error(messageTxt, e);
		}

		return S2Constants.Result.SUCCESS;
	}

	public String add() {
		try {
			validateAddOrEditOperation();
			BundledFareDisplaySettingsDTO displaySettingDTO = prepareBundledFareDisplaySettingsDTO();
			ModuleServiceLocator.getBundledFareBD().saveBundledFareDisplaySettings(displaySettingDTO);
			messageTxt = airadminConfig.getMessage("um.promotion.bundled.fare.settings.saved");
		} catch (ModuleException me) {
			success = false;
			messageTxt = airadminConfig.getMessage(me.getExceptionCode());
			log.error(messageTxt, me);
		} catch (Exception e) {
			success = false;
			messageTxt = airadminConfig.getMessage("um.promotion.bundled.fare.failed.to.save.settings");
			log.error(messageTxt, e);
		}

		return S2Constants.Result.SUCCESS;
	}

	private BundledFareDisplaySettingsDTO prepareBundledFareDisplaySettingsDTO() throws ModuleException {
		BundledFareDisplaySettingsDTO displaySettingDTO = new BundledFareDisplaySettingsDTO();

		Map<String, String> tumbnails;
		try {
			tumbnails = prepareThumbnails();
		} catch (IOException e) {
			log.error("Unable to upload bundle images", e);
			throw new ModuleException("um.promotion.bundled.unable.to.upload.image");
		}

		@SuppressWarnings("unchecked")
		Map<String, String> nameTranslationMap = JSONFETOParser.parseMap(HashMap.class, String.class, nameTranslations);
		@SuppressWarnings("unchecked")
		Map<String, String> descriptionTranslationMap = JSONFETOParser.parseMap(HashMap.class, String.class, descTranslations);

		displaySettingDTO.setBundleFarePeriodId(bundledFarePeriodId);
		displaySettingDTO.setDisplayTemplateId(displayTemplateId);
		displaySettingDTO.setThumbnails(tumbnails);
		displaySettingDTO.setNameTranslations(nameTranslationMap);
		displaySettingDTO.setDescTranslations(descriptionTranslationMap);

		return displaySettingDTO;
	}

	private void validateSearchOperation() throws ModuleException {
		BasicRequestHandler.checkOrConditionForPrivileges(request, PriviledgeConstants.ALLOW_VIEW_BUNDLED_FARE_CONFIG);
		boolean valid = false;
		if (bundledFarePeriodId != null) {
			valid = true;
		}
		if (!valid) {
			throw new ModuleException("um.promotion.bundled.fare.not.saved");
		}
	}

	private void validateAddOrEditOperation() throws ModuleException {
		BasicRequestHandler.checkOrConditionForPrivileges(request, PriviledgeConstants.ALLOW_ADD_BUNLED_FARE_CONFIG,
				PriviledgeConstants.ALLOW_EDIT_BUNLED_FARE_CONFIG);
	}

	private Map<String, String> prepareThumbnails() throws IOException {
		Map<String, String> uploadedThumbnails = new HashMap<String, String>();
		AiradminModuleConfig adminCon = AiradminModuleUtils.getConfig();
		String mountPath = adminCon.getMealImages();
		File tmpFile = null;
		String imageFile = null;

		if (uploadImage != null && !uploadImage.isEmpty()) {
			@SuppressWarnings("unchecked")
			Map<String, String> langWiseUploads = JSONFETOParser.parseMap(HashMap.class, String.class, uploadImage);
			if (!langWiseUploads.isEmpty()) {
				for (Entry<String, String> uploadEntry : langWiseUploads.entrySet()) {
					String langCode = uploadEntry.getKey();
					String uploadStatus = uploadEntry.getValue();
					if (uploadStatus != null && uploadStatus.equals("true")) {

						String fullFileName = mountPath + BundledFareConstants.IMG_PREFIX + "test_" + langCode
								+ BundledFareConstants.IMG_SUFFIX;
						tmpFile = new File(fullFileName);

						String imageName = BundledFareConstants.IMG_PREFIX + bundledFarePeriodId + "_" + langCode
								+ BundledFareConstants.IMG_SUFFIX;
						imageFile = mountPath + imageName;
						File dataFile = new File(imageFile);

						FileUtils.copyFile(tmpFile, dataFile);
						populateImageThumbnails(uploadedThumbnails, langCode, imageName);
					}
				}

			}
		}
		return uploadedThumbnails;

	}

	private void populateImageThumbnails(Map<String, String> uploadedThumbnails, String langCode, String imageName) {
		String strImgpath = AppSysParamsUtil.getImageUploadPath();
		String imageUrl = strImgpath + imageName;
		uploadedThumbnails.put(langCode, imageUrl);
	}

	public BundledFareDisplayDTO getDisplayDTO() {
		return displayDTO;
	}

	public void setBundledFarePeriodId(Integer bundledFarePeriodId) {
		this.bundledFarePeriodId = bundledFarePeriodId;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setUploadImage(String uploadImage) {
		this.uploadImage = uploadImage;
	}

	public void setDisplayTemplateId(Integer displayTemplateId) {
		this.displayTemplateId = displayTemplateId;
	}

	public void setNameTranslations(String nameTranslations) {
		this.nameTranslations = nameTranslations;
	}

	public void setDescTranslations(String descTranslations) {
		this.descTranslations = descTranslations;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

}
