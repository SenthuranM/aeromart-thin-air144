package com.isa.thinair.airadmin.core.web.action.reports;

import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.handler.reports.AdvancedAncillaryRevenueReportRH;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;

@Results({
		@Result(name = AdminStrutsConstants.AdminAction.SUCCESS, value = AdminStrutsConstants.AdminJSP.REPORTING_ADVANANCED_ANCILLARY_REVENUE_JSP),
		@Result(name = AdminStrutsConstants.AdminAction.ERROR, value = AdminStrutsConstants.AdminJSP.ERROR_JSP) })
public class ShowAdvancedAncillaryRevenueReportAction extends BaseRequestResponseAwareAction {
	public String execute() {
		return AdvancedAncillaryRevenueReportRH.execute(request, response);
	}
}
