package com.isa.thinair.airadmin.core.web.handler.reports;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airinventory.api.util.OndFareUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.reporting.api.model.FareRuleDetailsDTO;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

public class FareRuleDetailsReportRH extends BasicRequestHandler {

	private static String clientErrors;
	private static Log log = LogFactory.getLog(FareRuleDetailsReportRH.class);

	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	public FareRuleDetailsReportRH() {
		super();
	}

	public static String execute(HttpServletRequest request, HttpServletResponse response) {

		String strHdnMode = request.getParameter("hdnMode");
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;

		try {
			setDisplayData(request);
			log.debug("FareRuleDetailsREportRH Success");
		} catch (Exception ex) {
			forward = AdminStrutsConstants.AdminAction.ERROR;
			log.error("FareRuleDetailsREportRH execute()" + ex.getMessage());
		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				log.debug("FareRuleDetailsREportRH Success");
				return null;
			} else {
				log.debug("FareRuleDetailsREportRH setReportView not selected");
			}
		} catch (ModuleException ex) {
			saveMessage(request, ex.getMessage(), WebConstants.MSG_ERROR);
			log.debug("FareRuleDetailsREportRH setReportView Failed" + ex.getMessage());

		}

		return forward;
	}

	public static void setDisplayData(HttpServletRequest request) throws Exception {
		setFareRulesMultiSelect(request);
		setFareVisibilityMultiSelect(request);
		setGSAMultiSelectList(request);
		setAgentTypes(request);
		setClientErrors(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);
	}

	protected static void setFareRulesMultiSelect(HttpServletRequest request) throws ModuleException {
		String fareRulesMultiList = ReportsHTMLGenerator.createFareRuleList();
		request.setAttribute(WebConstants.REQ_HTML_FARERULE_MULTI_LIST, fareRulesMultiList);
	}

	protected static void setFareVisibilityMultiSelect(HttpServletRequest request) throws ModuleException {
		String fareVisibilityMultiList = ReportsHTMLGenerator.createVisibilityList();
		request.setAttribute(WebConstants.REQ_HTML_FARE_VISIBILITY_MULTI_LIST, fareVisibilityMultiList);
	}

	protected static void setGSAMultiSelectList(HttpServletRequest request) throws ModuleException {
		boolean blnWithTAs = false;
		boolean blnWithCOs = false;
		String strAgentType = "";

		if (request.getParameter("selAgencies") != null) {
			strAgentType = request.getParameter("selAgencies");
		}
		if (request.getParameter("chkTAs") != null) {
			blnWithTAs = (request.getParameter("chkTAs").equals("on") ? true : false);
		}
		if (request.getParameter("chkCOs") != null) {
			blnWithCOs = (request.getParameter("chkCOs").equals("on") ? true : false);
		}
		String strList = ReportsHTMLGenerator.createAgentGSAMultiSelect(strAgentType, blnWithTAs, blnWithCOs);
		request.setAttribute(WebConstants.REQ_HTML_GSA_LIST, strList);
	}

	protected static void setAgentTypes(HttpServletRequest request) throws ModuleException {
		String strATList = SelectListGenerator.createAgentTypeList_SG();
		request.setAttribute(WebConstants.REQ_HTML_AGENTTYPE_LIST, strATList);
	}

	public static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);

		} catch (ModuleException exception) {
			log.error("setClientErrors() method is failed : " + exception.getMessageString());
			saveMessage(request, exception.getMessageString(), WebConstants.MSG_ERROR);
		}

	}

	public static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {
		ResultSet resultSet = null;
		String value = request.getParameter("radReportOption");
		String fareRules = request.getParameter("hdnFareRules");
		String status = request.getParameter("selFareRuleStatus");
		String flightWay = request.getParameter("selFlightWay");
		String fareRuleVisibility = request.getParameter("hdnFareRuleVisibility");
		String fareRuleAgents = request.getParameter("hdnFareAgents");
		String id = "UC_REPM_074";
		String reportTemplate = "FareRuleDetailsReport.jasper";
		String strLogo = AppSysParamsUtil.getReportLogo(false);
		String image = "../../images/" +  AppSysParamsUtil.getReportLogo(true);
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String strBase = globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY);
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);

		Vector<String> fareRulesCol = new Vector<String>();
		Vector<String> fareRulesVisibilityCol = new Vector<String>();
		Vector<String> fareRulesAgentsCols = new Vector<String>();
		String arrFareRules[] = fareRules.split(",");
		String arrFareVisibility[] = fareRuleVisibility.split(",");
		String arrFareAgents[] = fareRuleAgents.split(",");

		Map<String, Object> parameters = new HashMap<String, Object>();

		for (int i = 0; i < arrFareRules.length; i++) {
			fareRulesCol.add(arrFareRules[i]);
		}

		for (int i = 0; i < arrFareVisibility.length; i++) {
			fareRulesVisibilityCol.add(arrFareVisibility[i]);
		}

		for (int i = 0; i < arrFareAgents.length; i++) {
			fareRulesAgentsCols.add(arrFareAgents[i]);
		}

		try {

			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));
			ReportsSearchCriteria search = new ReportsSearchCriteria();

			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}

			search.setStatus(status);
			search.setOperationType(flightWay);
			search.setAgents(fareRulesAgentsCols);
			search.setCarrierCodes(fareRulesVisibilityCol);
			search.setBcCategory(fareRulesCol);

			resultSet = ModuleServiceLocator.getDataExtractionBD().getFareRuleDetailsData(search);
			Collection<FareRuleDetailsDTO> fareRuleDTOColl = new ArrayList<FareRuleDetailsDTO>();

			while (resultSet.next()) {
				FareRuleDetailsDTO fareRuleDTO = new FareRuleDetailsDTO();
				StringBuffer agents = new StringBuffer();
				StringBuffer visibilityCodes = new StringBuffer();

				String fareRuleCode = resultSet.getString("FARERULE");
				Integer fareRuleId = resultSet.getInt("FARERULEID");

				ReportsSearchCriteria fareRuleCriteria = new ReportsSearchCriteria();
				fareRuleCriteria.setFareRuleCode(fareRuleCode);
				fareRuleCriteria.setFareRuleId(fareRuleId);
				ResultSet agentResultSet = ModuleServiceLocator.getDataExtractionBD().getAgentsForFareRuleData(fareRuleCriteria);
				ResultSet visibilityResultSet = ModuleServiceLocator.getDataExtractionBD().getFareRuleVisibilityData(
						fareRuleCriteria);

				while (agentResultSet.next()) {
					agents.append(agentResultSet.getString("AGENTS"));
					agents.append(", ");
				}

				while (visibilityResultSet.next()) {
					visibilityCodes.append(visibilityResultSet.getString("VISIBILITY"));
					visibilityCodes.append(", ");
				}

				if (agents != null && !agents.toString().equals("")) {
					agents.setCharAt(agents.lastIndexOf(","), ' ');
				}
				if (visibilityCodes != null && !visibilityCodes.toString().equals("")) {
					visibilityCodes.setCharAt(visibilityCodes.lastIndexOf(","), ' ');
				}

				fareRuleDTO.setAgents(agents.toString());
				fareRuleDTO.setVisibilityCode(visibilityCodes.toString());

				fareRuleDTO.setFareRule(resultSet.getString("FARERULE"));
				fareRuleDTO.setDescription(resultSet.getString("DESCRIPTION"));
				fareRuleDTO.setBasis(resultSet.getString("BASIS"));

				String returnWay = resultSet.getString("FLIGHTWAY");
				if (returnWay != null && returnWay.equals("Y")) {
					fareRuleDTO.setFlightWay("RT");
				} else
					fareRuleDTO.setFlightWay("OW");

				fareRuleDTO.setStatus(resultSet.getString("STATUS"));

				String modificationCharges = resultSet.getString("CHARGE_RESTRICTTION");
				if (modificationCharges != null && modificationCharges.equalsIgnoreCase("Y")) {
					fareRuleDTO.setChargeDescription("YES");
				} else
					fareRuleDTO.setChargeDescription("NO");

				String expiryDate = resultSet.getString("PRINT_EXPIRY_DATE");
				if (expiryDate != null && expiryDate.equalsIgnoreCase("Y")) {
					fareRuleDTO.setPrintExpiryDate("YES");
				} else
					fareRuleDTO.setPrintExpiryDate("NO");

				fareRuleDTO.setModificationCharges(resultSet.getBigDecimal("MODI_CHARGE"));
				fareRuleDTO.setCancelCharges(resultSet.getBigDecimal("CANCEL_CHARGE"));
				fareRuleDTO.setAdvBookingDate(resultSet.getString("ADV_BOOKING_DATE"));

				if (fareRuleDTO.getAdvBookingDate() != null && !fareRuleDTO.getAdvBookingDate().equals("")) {
					fareRuleDTO.setAdvBookingDate(fareRuleDTO.getAdvBookingDate() + "d");
				}

				BigDecimal minStayMin = resultSet.getBigDecimal("MIN_STAY_MINS");
				BigDecimal maxStayMin = resultSet.getBigDecimal("MAX_STAY_MINS");

				fareRuleDTO.setMaxMonths(resultSet.getString("MAX_MONTH"));

				String[] elements;

				if (maxStayMin != null) {
					elements = OndFareUtil.parseMins(maxStayMin.longValue());
					fareRuleDTO.setMaxDays(elements[0]);
					fareRuleDTO.setMaxHours(elements[1]);
					fareRuleDTO.setMaxMins(elements[2]);
				} else {
					fareRuleDTO.setMaxDays("");
					fareRuleDTO.setMaxHours("");
					fareRuleDTO.setMaxMins("");
				}

				fareRuleDTO.setMinMonths(resultSet.getString("MIN_MONTH"));
				if (minStayMin != null) {
					elements = OndFareUtil.parseMins(minStayMin.longValue());
					fareRuleDTO.setMinDays(elements[0]);
					fareRuleDTO.setMinHours(elements[1]);
					fareRuleDTO.setMinMins(elements[2]);
				} else {
					fareRuleDTO.setMinDays("");
					fareRuleDTO.setMinHours("");
					fareRuleDTO.setMinMins("");
				}

				StringBuffer minStayOver = new StringBuffer("");
				if (fareRuleDTO.getMinMonths() != null && !fareRuleDTO.getMinMonths().equals("")) {
					minStayOver.append(fareRuleDTO.getMinMonths());
				}
				if (fareRuleDTO.getMinDays() != null && !fareRuleDTO.getMinDays().equals("")) {
					minStayOver.append(" ," + fareRuleDTO.getMinDays() + "d");
				}
				if (fareRuleDTO.getMinHours() != null && !fareRuleDTO.getMinHours().equals("")) {
					minStayOver.append(" ," + fareRuleDTO.getMinHours() + "h");
				}
				if (fareRuleDTO.getMinMins() != null && !fareRuleDTO.getMinMins().equals("")) {
					minStayOver.append(" ," + fareRuleDTO.getMinMins() + "mi");
				}

				StringBuffer maxStayOver = new StringBuffer("");
				if (fareRuleDTO.getMaxMonths() != null && !fareRuleDTO.getMaxMonths().equals("")) {
					maxStayOver.append(fareRuleDTO.getMaxMonths());
				}
				if (fareRuleDTO.getMaxDays() != null && !fareRuleDTO.getMaxDays().equals("")) {
					maxStayOver.append(" ," + fareRuleDTO.getMaxDays() + "d");
				}
				if (fareRuleDTO.getMaxHours() != null && !fareRuleDTO.getMaxHours().equals("")) {
					maxStayOver.append(" ," + fareRuleDTO.getMaxHours() + "h");
				}
				if (fareRuleDTO.getMaxMins() != null && !fareRuleDTO.getMaxMins().equals("")) {
					maxStayOver.append(" ," + fareRuleDTO.getMaxMins() + "mi");
				}

				fareRuleDTO.setMinStayOverTime(minStayOver.toString());
				fareRuleDTO.setMaxStayOverTime(maxStayOver.toString());

				fareRuleDTO.setpExpDate(resultSet.getString("P_EXP_DATE"));

				StringBuffer inBound = new StringBuffer("");

				String inb1 = resultSet.getString("INB1");
				if (inb1 != null && inb1.equals("1")) {
					inBound.append("Mon ");
				}

				String inb2 = resultSet.getString("INB2");
				if (inb2 != null && inb2.equals("1")) {
					inBound.append("Tue ");
				}

				String inb3 = resultSet.getString("INB3");
				if (inb3 != null && inb3.equals("1")) {
					inBound.append("Wed ");
				}

				String inb4 = resultSet.getString("INB4");
				if (inb4 != null && inb4.equals("1")) {
					inBound.append("Thu ");
				}

				String inb5 = resultSet.getString("INB5");
				if (inb5 != null && inb5.equals("1")) {
					inBound.append("Fri ");
				}

				String inb6 = resultSet.getString("INB6");
				if (inb6 != null && inb6.equals("1")) {
					inBound.append("Sat ");
				}

				String inb7 = resultSet.getString("INB7");
				if (inb7 != null && inb7.equals("1")) {
					inBound.append("Sun ");
				}
				fareRuleDTO.setInTravelTimeFrom(resultSet.getString("IN_TRAVEL_TIME_FROM"));
				fareRuleDTO.setInTravelTimeTo(resultSet.getString("IN_TRAVEL_TIME_TO"));

				if (fareRuleDTO.getInTravelTimeFrom() != null && !fareRuleDTO.getInTravelTimeFrom().equals("")) {
					inBound.append(" ");
					inBound.append(fareRuleDTO.getInTravelTimeFrom());
				}
				if (fareRuleDTO.getInTravelTimeTo() != null && !fareRuleDTO.getInTravelTimeTo().equals("")) {
					inBound.append(" ");
					inBound.append(fareRuleDTO.getInTravelTimeTo());
				}
				fareRuleDTO.setInBound(inBound.toString());

				StringBuffer outBound = new StringBuffer("");
				String ob1 = resultSet.getString("OB1");
				if (ob1 != null && ob1.equals("1")) {
					outBound.append("Mon ");
				}

				String ob2 = resultSet.getString("OB2");
				if (ob2 != null && ob2.equals("1")) {
					outBound.append("Tue ");
				}

				String ob3 = resultSet.getString("OB3");
				if (ob3 != null && ob3.equals("1")) {
					outBound.append("Wed ");
				}

				String ob4 = resultSet.getString("OB4");
				if (ob4 != null && ob4.equals("1")) {
					outBound.append("Thu ");
				}

				String ob5 = resultSet.getString("OB5");
				if (ob5 != null && ob5.equals("1")) {
					outBound.append("Fri ");
				}

				String ob6 = resultSet.getString("OB6");
				if (ob6 != null && ob6.equals("1")) {
					outBound.append("Sat ");
				}

				String ob7 = resultSet.getString("OB7");
				if (ob7 != null && ob7.equals("1")) {
					outBound.append("Sun ");
				}
				fareRuleDTO.setdTravelTimeFrom(resultSet.getString("D_TRAVEL_TIME_FROM"));
				fareRuleDTO.setdTravelTimeto(resultSet.getString("D_TRAVEL_TIME_TO"));

				if (fareRuleDTO.getdTravelTimeto() != null && !fareRuleDTO.getdTravelTimeto().equals("")) {
					outBound.append(" ");
					outBound.append(fareRuleDTO.getdTravelTimeFrom());
				}
				if (fareRuleDTO.getdTravelTimeFrom() != null && !fareRuleDTO.getdTravelTimeFrom().equals("")) {
					outBound.append(" ");
					outBound.append(fareRuleDTO.getdTravelTimeto());
				}
				fareRuleDTO.setOutBound(outBound.toString());

				fareRuleDTO.setPaxCat(resultSet.getString("PAX_CAT"));
				fareRuleDTO.setFareCat(resultSet.getString("FARE_CAT"));

				String bufferTime = resultSet.getString("MOD_BUFF_TIME");
				StringBuffer modifyBufferTime = new StringBuffer("");
				if (bufferTime != null && !bufferTime.equals("")) {
					String[] arrBufferTime = bufferTime.split(":");
					if (arrBufferTime[0] != "") {
						modifyBufferTime.append(arrBufferTime[0] + "d");
					}
					if (arrBufferTime[1] != "") {
						modifyBufferTime.append(" ," + arrBufferTime[1] + "h");
					}
					if (arrBufferTime[2] != "") {
						modifyBufferTime.append(" ," + arrBufferTime[2] + "mi");
					}
				}
				fareRuleDTO.setModBufferTime(modifyBufferTime.toString());

				fareRuleDTO.setRuleComments(resultSet.getString("RULE_COMMENTS"));
				fareRuleDTO.setmFRules(resultSet.getString("M_F_RULE"));

				String childPay = resultSet.getString("CHILD_PAY_APPLICABLE");
				if (childPay != null && childPay.equals("CH")) {
					fareRuleDTO.setChildPayApplicable("YES");
				} else
					fareRuleDTO.setChildPayApplicable("NO");

				String childRefund = resultSet.getString("CHILD_REFUND");
				if (childRefund != null && childRefund.equals("Y")) {
					fareRuleDTO.setChildRefund("Refundable");
				} else
					fareRuleDTO.setChildRefund("Non-Refundable");

				String childModf = resultSet.getString("CHILD_MODF");
				if (childModf != null && childModf.equals("Y")) {
					fareRuleDTO.setChildMode("MOD Allowed");
				} else
					fareRuleDTO.setChildMode("MOD Not Allowed");

				String childCnx = resultSet.getString("CHILD_CNX");
				if (childCnx != null && childCnx.equals("Y")) {
					fareRuleDTO.setChildCnx("CNX Allowed");
				} else
					fareRuleDTO.setChildCnx("CNX Not Allowed");

				fareRuleDTO.setChildNoShow(resultSet.getBigDecimal("CHILD_NOSHOW"));

				String infantPay = resultSet.getString("INFANT_PAY");
				if (infantPay != null && infantPay.equals("IN")) {
					fareRuleDTO.setInfantPay("YES");
				} else
					fareRuleDTO.setInfantPay("NO");

				String infantRefund = resultSet.getString("INFANT_REFUND");
				if (infantRefund != null && infantRefund.equals("IN")) {
					fareRuleDTO.setInfantRefund("Refundable");
				} else
					fareRuleDTO.setInfantRefund("Non-Refundable");

				String infantModf = resultSet.getString("INFANT_MODF");
				if (infantModf != null && infantModf.equals("Y")) {
					fareRuleDTO.setInfantMode("MOD Allowed");
				} else
					fareRuleDTO.setInfantMode("MOD Not Allowed");

				String infantCnx = resultSet.getString("INFANT_CNX");
				if (infantCnx != null && infantCnx.equals("Y")) {
					fareRuleDTO.setInfantCnx("CNX Allowed");
				} else
					fareRuleDTO.setInfantCnx("CNX Not Allowed");

				fareRuleDTO.setInfantNoShow(resultSet.getBigDecimal("INFANT_NOSHOW"));

				String adultPay = resultSet.getString("ADULT_PAY");
				if (adultPay != null && adultPay.equals("AD")) {
					fareRuleDTO.setAdultPay("YES");
				} else
					fareRuleDTO.setAdultPay("NO");

				String adultRefund = resultSet.getString("ADULT_REFUND");
				if (adultRefund != null && adultRefund.equals("Y")) {
					fareRuleDTO.setAdultRefund("Refundable");
				} else
					fareRuleDTO.setAdultRefund("Non-Refundable");

				String adultModf = resultSet.getString("ADULT_MODF");
				if (adultModf != null && adultModf.equals("Y")) {
					fareRuleDTO.setAdultMode("MOD Allowed");
				} else
					fareRuleDTO.setAdultMode("MOD Not Allowed");

				String adultCnx = resultSet.getString("ADULT_CNX");
				if (adultCnx != null && adultCnx.equals("Y")) {
					fareRuleDTO.setAdultCnx("CNX Allowed");
				} else
					fareRuleDTO.setAdultCnx("CNX Not Allowed");

				fareRuleDTO.setAdultNoShow(resultSet.getBigDecimal("ADULT_NOSHOW"));

				String openReturn = resultSet.getString("OPEN_RETURN");
				if (openReturn != null && openReturn.equals("Y")) {
					fareRuleDTO.setOpenReturn("YES");
				} else
					fareRuleDTO.setOpenReturn("NO");

				BigDecimal openReturnPeriod = resultSet.getBigDecimal("OPEN_RETURN_PERIOD_MINS");

				fareRuleDTO.setOrMonth(resultSet.getString("OR_MONTH"));
				if (openReturnPeriod != null) {
					elements = OndFareUtil.parseMins(openReturnPeriod.longValue());
					fareRuleDTO.setOrDays(elements[0]);
					fareRuleDTO.setOrHours(elements[1]);
					fareRuleDTO.setOrMins(elements[2]);
				} else {
					fareRuleDTO.setOrDays("");
					fareRuleDTO.setOrHours("");
					fareRuleDTO.setOrMins("");
				}

				StringBuffer openReturnLimit = new StringBuffer();
				if (fareRuleDTO.getOrMonth() != null && !fareRuleDTO.getOrMonth().equals("")) {
					openReturnLimit.append(fareRuleDTO.getOrMonth());
				}
				if (fareRuleDTO.getOrDays() != null && !fareRuleDTO.getOrDays().equals("")) {
					openReturnLimit.append(" ," + fareRuleDTO.getOrDays() + "d");
				}
				if (fareRuleDTO.getOrHours() != null && !fareRuleDTO.getOrHours().equals("")) {
					openReturnLimit.append(" ," + fareRuleDTO.getOrHours() + "h");
				}
				if (fareRuleDTO.getOrMins() != null && !fareRuleDTO.getOrMins().equals("")) {
					openReturnLimit.append(" ," + fareRuleDTO.getOrMins() + "mi");
				}

				fareRuleDTO.setOpenReturnLimit(openReturnLimit.toString());

				fareRuleDTOColl.add(fareRuleDTO);
			}

			parameters.put("REPORT_ID", id);
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
			parameters.put("OPTION", value);

			parameters.put("ID", id);
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
			parameters.put("AMT_1", "(" + strBase + ")");
			parameters.put("REPORT_MEDIUM", strlive);

			// To provide Report Format Options
			String reportNumFormat = request.getParameter("radRptNumFormat");
			ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

			if (value.trim().equals("HTML")) {
				parameters.put("IMG", image);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, fareRuleDTOColl, null,
						null, response);
			} else if (value.trim().equals("PDF")) {
				response.reset();
				response.addHeader("Content-Disposition", "filename=FareRuleDetailsReport.pdf");
				image = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG", image);
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, fareRuleDTOColl,
						response);
			} else if (value.trim().equals("EXCEL")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=FareRuleDetailsReport.xls");
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, fareRuleDTOColl,
						response);
			} else if (value.trim().equals("CSV")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=FareRuleDetailsReport.csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, fareRuleDTOColl,
						response);
			}

		} catch (Exception ex) {
			log.error(ex);
		}

	}

	/**
	 * Creates Client Validations for Fare Rules Details Report Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array of Client Validations
	 * @throws ModuleException
	 *             the ModuleException
	 * 
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {

		if (clientErrors == null) {
			Properties moduleErrors = new Properties();

			moduleErrors.setProperty("um.farerule.farerule.required", "fareclassRqrd");
			moduleErrors.setProperty("um.farerule.bulkticket.minimim.seats.required", "btMinSeatsRqrd");
			moduleErrors.setProperty("um.farerule.bulkticket.minimim.seats.incorrect", "btMinSeatsIncorrect");
			moduleErrors.setProperty("um.report.agentType.required", "agentTypeRqrd");

			clientErrors = JavascriptGenerator.createClientErrors(moduleErrors);
		}

		return clientErrors;
	}

}
