package com.isa.thinair.airadmin.core.web.v2.generator.promotion;

import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author chanaka
 *
 */
public class CampaignMessageHG {
	
	private static String campaignErrors;	

	public static String getCampaignClientErrors(HttpServletRequest request) throws ModuleException {

		if (campaignErrors == null) {
			
			Properties moduleErrs = new Properties();
		
			moduleErrs.setProperty("um.campaign.details.create.success", "createSuccess");
			moduleErrs.setProperty("um.campaign.details.invalid.date", "invalidDate");
			moduleErrs.setProperty("um.campaign.details.ond.select", "ondSelect");
			moduleErrs.setProperty("um.campaign.details.ond.same", "ondSame");
			moduleErrs.setProperty("um.campaign.details.viapoints.order", "viaPointsOrder");
			moduleErrs.setProperty("um.campaign.details.viapoints.duplicate", "viapointsDuplicate");
			moduleErrs.setProperty("um.campaign.details.value.exit", "valueExist");
			moduleErrs.setProperty("um.campaign.details.name.empty", "campaignNameEmpty");
			moduleErrs.setProperty("um.campaign.details.bookingdate.to.empty", "bookingDateToEmpty");
			moduleErrs.setProperty("um.campaign.details.bookingdate.from.empty", "bookingDateFromEmpty");
			moduleErrs.setProperty("um.campaign.details.traveldate.from.empty", "travelDateFromEmpty");
			moduleErrs.setProperty("um.campaign.details.traveldate.to.empty", "travelDateToEmpty");
			moduleErrs.setProperty("um.campaign.details.routes.empty", "routesEmpty");
			moduleErrs.setProperty("um.campaign.details.filter.error", "filterError");
			moduleErrs.setProperty("um.campaign.details.filter.success", "filterSuccess");
			moduleErrs.setProperty("um.campaign.email.sent.success", "emailSent");
			moduleErrs.setProperty("um.campaign.email.sent.error", "emailSentFailed");
			moduleErrs.setProperty("um.campaign.no.promocodes.error", "noPromoCodesToSend");
			
			campaignErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}
		return campaignErrors;
	}
}
