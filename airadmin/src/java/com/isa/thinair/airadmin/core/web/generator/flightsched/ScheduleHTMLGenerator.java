package com.isa.thinair.airadmin.core.web.generator.flightsched;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airadmin.core.web.util.ScheduleUtils;
import com.isa.thinair.airmaster.api.model.Gds;
import com.isa.thinair.airschedules.api.dto.FlightSegmentResInvSummaryDTO;
import com.isa.thinair.airschedules.api.dto.FlightSummaryDTO;
import com.isa.thinair.airschedules.api.model.CodeShareMCFlightSchedule;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.FlightScheduleLeg;
import com.isa.thinair.airschedules.api.model.FlightScheduleSegment;
import com.isa.thinair.airschedules.api.utils.FlightUtil;
import com.isa.thinair.airschedules.api.utils.ModelNamesAndFieldNames;
import com.isa.thinair.airschedules.api.utils.SegmentUtil;
import com.isa.thinair.commons.api.constants.DayOfWeek;
import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.api.dto.ModuleCriteria;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author Thushara
 * 
 */
public class ScheduleHTMLGenerator {
	private static Log log = LogFactory.getLog(ScheduleHTMLGenerator.class);
	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();
	private static final String PARAM_TIMEMODE = "hdnTimeMode";
	private static final String PARAM_LEGS = "strLegs";
	private static String clientErrors;
	private static final String strSMDateFormat = "dd/MM/yyyy";

	private static final String defaultFormat // get the default date format
	= globalConfig.getBizParam(SystemParamKeys.DATE_FORMAT_FOR_DATE_STRINGS);

	String dayOffSet // get the default schedule status to search
	= globalConfig.getBizParam(SystemParamKeys.OFFSET_FROM_THE_STANDED_FIRST_DAY_OF_WEEK);
	String strTimeMode = "";

	/**
	 * Gets the Grid Array for the Flight Schedule default search is commented
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return the Grid Array for Schedules
	 * @throws ModuleException
	 *             the ModuleException
	 */
	@SuppressWarnings("unchecked")
	public final String getScheduleRowHtml(HttpServletRequest request) throws ModuleException {

		strTimeMode = request.getParameter(PARAM_TIMEMODE) == null ? "" : request.getParameter(PARAM_TIMEMODE);
		Page page = null;
		Collection<FlightSchedule> colSchedules = null;
		int recNo = 0;
		int totRec = 0;
		boolean localtime = false;

		if (request.getParameter("hdnRecNo") == null) {
			recNo = 1;
		} else if (!("".equals(request.getParameter("hdnRecNo")))) {
			recNo = Integer.parseInt(request.getParameter("hdnRecNo"));
		}
		recNo = recNo - 1;

		log.debug("inside ScheduleHTMLGenerator.getScheduleRowHtml()");
		String strMode = request.getParameter("hdnMode");

		String defaultOperationType // get the default operation type to search
		= globalConfig.getBizParam(SystemParamKeys.DEFAULT_OPERATION_TYPE);
		String defaultStatus // get the default schedule status to search
		= globalConfig.getBizParam(SystemParamKeys.DEFAULT_SCHEDULE_STATUS);
		String minimumTimeAllowdForSchedule // get the default schedule status to search
		= globalConfig.getBizParam(SystemParamKeys.MINIMUM_TIME_ALLOWED_FOR_SCHEDULE);
		dayOffSet // get the default schedule status to search
		= globalConfig.getBizParam(SystemParamKeys.OFFSET_FROM_THE_STANDED_FIRST_DAY_OF_WEEK);

		request.setAttribute(WebConstants.REQ_HTML_DEFAULT_OPERATIONTYPE, defaultOperationType);
		request.setAttribute(WebConstants.REQ_HTML_DEFAULT_SCHEDULESTATUS, defaultStatus);
		request.setAttribute(WebConstants.REQ_MINIMUM_TIME_ALLOWEDFORSCHEDULE, minimumTimeAllowdForSchedule);
		request.setAttribute(WebConstants.REQ_DAY_OFFSET, dayOffSet);

		// fields relavant to both searchs (default/non-default)
		String strStartDate = "";
		String strStopDate = "";
		String strOperationtype = "";
		String strStatus = "";
		String strBuildStatus = "";
		String strFlightType = "";

		// fields not relevant to the default search
		String strFlightNo = "";
		String strFlightStartNo = "";
		String strFrom = "";
		String strTo = "";

		Date dStartDate = null;
		Date dStopDate = null;

		if (strMode != null) {

			strStartDate = request.getParameter("txtStartDateSearch");
			strStopDate = request.getParameter("txtStopDateSearch");
			DateFormat formatter = new SimpleDateFormat(strSMDateFormat);

			try {

				dStartDate = formatter.parse(strStartDate);
				dStopDate = formatter.parse(strStopDate);

			} catch (Exception e) {
				log.error(e);
			}

			strFlightNo = request.getParameter("txtFlightNoSearch").trim();
			strFlightStartNo = request.getParameter("txtFlightNoStartSearch");
			strFrom = request.getParameter("selFromStn6");
			strTo = request.getParameter("selToStn6");
			strOperationtype = request.getParameter("selOperationTypeSearch");
			strStatus = request.getParameter("selStatusSearch");
			strBuildStatus = request.getParameter("selBuildStatusSearch");
			strFlightType = request.getParameter("selFlightTypeSearch");

			/*
			 * }else{
			 * 
			 * Calendar cal = Calendar.getInstance(); dStartDate = cal.getTime(); // Current date
			 * cal.add(Calendar.MONTH,1); dStopDate = cal.getTime(); //Next Month Date strOperationtype =
			 * defaultOperationType; //System Parameter strStatus = defaultStatus; //System Parameter strBuildStatus =
			 * ""; //ALL // strFlightNo = "G9";
			 * 
			 * }
			 */

			// setting the criteria list to search
			List<ModuleCriterion> critrian = new ArrayList<ModuleCriterion>();
			// setting the start date as criteria
			if (dStartDate != null) {

				ModuleCriterion moduleCriterionStartD = new ModuleCriterion();

				moduleCriterionStartD.setCondition(ModuleCriterion.CONDITION_GREATER_THAN_OR_EQUALS);
				moduleCriterionStartD.setFieldName(ModelNamesAndFieldNames.FlightSchedule.FieldNames.START_DATE); // start
																													// date
																													// of
																													// the
																													// schedule

				List<Date> valueStartDate = new ArrayList<Date>();
				valueStartDate.add(dStartDate);

				moduleCriterionStartD.setValue(valueStartDate);

				critrian.add(moduleCriterionStartD);
			}
			// setting the stop date as criteria
			if (dStopDate != null) {

				ModuleCriterion moduleCriterionStopD = new ModuleCriterion();

				moduleCriterionStopD.setCondition(ModuleCriterion.CONDITION_LESS_THAN_OR_EQUALS);
				moduleCriterionStopD.setFieldName(ModelNamesAndFieldNames.FlightSchedule.FieldNames.STOP_DATE); // stop
																												// date
																												// of
																												// the
																												// schedule

				List<Date> valueStopDate = new ArrayList<Date>();
				valueStopDate.add(dStopDate);

				moduleCriterionStopD.setValue(valueStopDate);

				critrian.add(moduleCriterionStopD);
			}
			// setting the operation type as criteria
			if (ScheduleUtils.isNotEmptyOrNull(strOperationtype)) {

				ModuleCriterion moduleCriterionOptType = new ModuleCriterion();

				moduleCriterionOptType.setCondition(ModuleCriterion.CONDITION_EQUALS);
				moduleCriterionOptType.setFieldName(ModelNamesAndFieldNames.FlightSchedule.FieldNames.OPERATION_TYPE_ID);// operation
																															// type
																															// of
																															// the
																															// schedule

				List<Integer> valueOptType = new ArrayList<Integer>();
				valueOptType.add(new Integer(strOperationtype));

				moduleCriterionOptType.setValue(valueOptType);

				critrian.add(moduleCriterionOptType);
			}
			// setting the schedule status as criteria
			if (ScheduleUtils.isNotEmptyOrNull(strStatus)) {

				ModuleCriterion moduleCriterionStatus = new ModuleCriterion();

				moduleCriterionStatus.setCondition(ModuleCriterion.CONDITION_EQUALS);
				moduleCriterionStatus.setFieldName(ModelNamesAndFieldNames.FlightSchedule.FieldNames.STATUS_CODE); // status
																													// code
																													// of
																													// the
																													// schedule

				List<String> valueStatus = new ArrayList<String>();
				valueStatus.add(strStatus);

				moduleCriterionStatus.setValue(valueStatus);

				critrian.add(moduleCriterionStatus);
			}

			// setting the flight type as criteria
			if (ScheduleUtils.isNotEmptyOrNull(strFlightType)) {

				ModuleCriterion moduleCriterionStatus = new ModuleCriterion();

				moduleCriterionStatus.setCondition(ModuleCriterion.CONDITION_EQUALS);
				moduleCriterionStatus.setFieldName(ModelNamesAndFieldNames.FlightSchedule.FieldNames.FLIGHT_TYPE);
				List<String> valueStatus = new ArrayList<String>();
				valueStatus.add(strFlightType);

				moduleCriterionStatus.setValue(valueStatus);

				critrian.add(moduleCriterionStatus);
			}

			// setting the schedule build status as criteria
			if (ScheduleUtils.isNotEmptyOrNull(strBuildStatus)) {

				ModuleCriterion moduleCriterionBuildStatus = new ModuleCriterion();

				moduleCriterionBuildStatus.setCondition(ModuleCriterion.CONDITION_EQUALS);
				moduleCriterionBuildStatus.setFieldName(ModelNamesAndFieldNames.FlightSchedule.FieldNames.BUILD_STATUS_CODE); // build
																																// status
																																// of
																																// the
																																// schedule

				List<String> valueBuildStatus = new ArrayList<String>();
				valueBuildStatus.add(strBuildStatus);

				moduleCriterionBuildStatus.setValue(valueBuildStatus);

				critrian.add(moduleCriterionBuildStatus);
			}
			// setting the flight number as criteria
			if (ScheduleUtils.isNotEmptyOrNull(strFlightNo) || ScheduleUtils.isNotEmptyOrNull(strFlightStartNo)) {
				CommonUtil.checkCarrierCodeAccessibility(AiradminUtils.getUserCarrierCodes(request), strFlightStartNo);
				ModuleCriterion moduleCriterionFlightNo = new ModuleCriterion();

				moduleCriterionFlightNo.setCondition(ModuleCriterion.CONDITION_LIKE);
				moduleCriterionFlightNo.setFieldName(ModelNamesAndFieldNames.FlightSchedule.FieldNames.FLIGHT_NUMBER); // flight
																														// number
																														// of
																														// the
																														// schedule

				List<String> valueFlightNo = new ArrayList<String>();
				String flightNumber = strFlightStartNo + strFlightNo;
				valueFlightNo.add(flightNumber != null ? (flightNumber.toUpperCase() + "%") : "");

				moduleCriterionFlightNo.setValue(valueFlightNo);

				critrian.add(moduleCriterionFlightNo);
			} else {
				Collection<String> strCarrierCodes = AiradminUtils.getUserCarrierCodes(request);
				if (strCarrierCodes.size() == 1) {
					ModuleCriterion moduleCriterionFlightNo = new ModuleCriterion();
					moduleCriterionFlightNo.setCondition(ModuleCriterion.CONDITION_LIKE);
					// flight number of the flight
					moduleCriterionFlightNo.setFieldName(ModelNamesAndFieldNames.FlightSchedule.FieldNames.FLIGHT_NUMBER);

					List<String> valueFlightNo = new ArrayList<String>();
					String ccode = "";
					for (String carCode : strCarrierCodes) {
						ccode = carCode;
						break;
					}

					valueFlightNo.add(ccode.toUpperCase() + "%");

					moduleCriterionFlightNo.setValue(valueFlightNo);
					critrian.add(moduleCriterionFlightNo);

				} else {
					ModuleCriteria moduleCriterionFlightNo = new ModuleCriteria();
					moduleCriterionFlightNo.setFieldName(ModelNamesAndFieldNames.FlightSchedule.FieldNames.FLIGHT_NUMBER);
					List<ModuleCriterion> moduleCriterions = new ArrayList<ModuleCriterion>();
					for (String strCC : strCarrierCodes) {
						ModuleCriterion m = new ModuleCriterion();
						m.setCondition(ModuleCriterion.CONDITION_LIKE);
						m.setFieldName(ModelNamesAndFieldNames.FlightSchedule.FieldNames.FLIGHT_NUMBER);

						List<String> valueFlightNo = new ArrayList<String>();
						valueFlightNo.add(strCC.toUpperCase() + "%");
						m.setValue(valueFlightNo);
						moduleCriterions.add(m);
					}
					moduleCriterionFlightNo.setLeaf(false);
					moduleCriterionFlightNo.setCondition(ModuleCriteria.JOIN_CONDITION_OR);
					moduleCriterionFlightNo.setModulecriterionList(moduleCriterions);
					critrian.add(moduleCriterionFlightNo);
				}

			}
			// setting the departure airport code as criteria
			if (ScheduleUtils.isNotEmptyOrNull(strFrom)) {

				ModuleCriterion moduleCriterionFromStn = new ModuleCriterion();

				moduleCriterionFromStn.setCondition(ModuleCriterion.CONDITION_EQUALS);
				moduleCriterionFromStn.setFieldName(ModelNamesAndFieldNames.FlightSchedule.FieldNames.DEPARTURE_APT_CODE);// departure
																															// airport
																															// code
																															// of
																															// the
																															// schedule

				List<String> valueFromStn = new ArrayList<String>();
				valueFromStn.add(strFrom);

				moduleCriterionFromStn.setValue(valueFromStn);

				critrian.add(moduleCriterionFromStn);
			}
			// setting the arrival airport code as criteria
			if (ScheduleUtils.isNotEmptyOrNull(strTo)) {

				ModuleCriterion moduleCriterionToStn = new ModuleCriterion();

				moduleCriterionToStn.setCondition(ModuleCriterion.CONDITION_EQUALS);
				moduleCriterionToStn.setFieldName(ModelNamesAndFieldNames.FlightSchedule.FieldNames.ARRIVAL_APT_CODE);// arrival
																														// airport
																														// code
																														// of
																														// the
																														// schedule

				List<String> valueToStn = new ArrayList<String>();
				valueToStn.add(strTo);

				moduleCriterionToStn.setValue(valueToStn);

				critrian.add(moduleCriterionToStn);
			}

			// localtime = strTimeMode.equals(WebConstants.LOCALTIME); if need to change with time mode uncomment this
			page = ModuleServiceLocator.getScheduleServiceBD().searchFlightSchedulesForDisplay(critrian, recNo, 20, localtime);
			if (page != null) {
				colSchedules = page.getPageData();
				totRec = page.getTotalNoOfRecords();

			}
		}
		request.setAttribute(WebConstants.REQ_TOTAL_RECORDS, new Integer(totRec).toString());
		log.debug("inside ScheduleHTMLGenerator.getScheduleRowHtml search values");
		return createScheduleRowHTML(colSchedules);
	}

	/**
	 * Creates the Flight Schedule Grid Array from a Coolection of Schedules time modes are commented out to cater both
	 * zulu & local
	 * 
	 * @param flightSchedules
	 *            the Collection of Schedules
	 * @return String the Grid Array
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private String createScheduleRowHTML(Collection<FlightSchedule> flightSchedules) throws ModuleException {
		StringBuffer sb = new StringBuffer();
		String sendEmailsForFlightAlterations = globalConfig.getBizParam(SystemParamKeys.SEND_EMAILS_FOR_FLIGHT_ALTERATIONS)
				.equalsIgnoreCase("Y") ? "Y" : "N";
		String sendSMSForFlightAlterations = globalConfig.getBizParam(SystemParamKeys.SEND_SMS_FOR_FLIGHT_ALTERATIONS)
				.equalsIgnoreCase("Y") ? "Y" : "N";

		String allowReprotectPAXToTodaysDepartures = globalConfig.getBizParam(
				SystemParamKeys.ALLOW_REPROTECT_PAX_TO_TODAYS_DEPARTURES).equalsIgnoreCase("Y") ? "Y" : "N";

		if (flightSchedules != null) {
			List<FlightSchedule> list = (List<FlightSchedule>) flightSchedules;
			// get number of openFLts
			List<Integer> strSchedIDs = new ArrayList<Integer>();
			for (FlightSchedule fltScd : list) {
				strSchedIDs.add(fltScd.getScheduleId());
			}
			Map<Integer, Integer> mapNumberOfOpenFlts = new HashMap<Integer, Integer>();
			Map<Integer, Integer> mapNumberOfFutureFlts = new HashMap<Integer, Integer>();
			if (!strSchedIDs.isEmpty()) {
				mapNumberOfOpenFlts = ModuleServiceLocator.getFlightServiceBD().getOpenFlightCount(strSchedIDs);
				mapNumberOfFutureFlts = ModuleServiceLocator.getFlightServiceBD().getFutureFlights(strSchedIDs);
			}

			Object[] listArr = list.toArray();

			FlightSchedule flightSchedule = null;

			for (int i = 0; i < listArr.length; i++) {
				flightSchedule = (FlightSchedule) listArr[i];
				String strStart = "";
				String strStop = "";
				String strStartZulu = "";
				String strStopZulu = "";
				String strStartFullZulu = "";
				String strStopFullZulu = "";				
				String strStartFullLocal = "";
				String strStopFullLocal = "";
				
				Frequency frq = null;
				Frequency frqLocal = null;

				if ((mapNumberOfOpenFlts != null && !mapNumberOfOpenFlts.isEmpty())
						&& mapNumberOfOpenFlts.get(flightSchedule.getScheduleId()) != null) {
					flightSchedule.setNumberOfFlights(mapNumberOfOpenFlts.get(flightSchedule.getScheduleId()));
				}

				// if (strTimeMode.equals(WebConstants.LOCALTIME)) {
				strStart = ScheduleUtils.formatDate(flightSchedule.getStartDateLocal(), strSMDateFormat);
				strStop = ScheduleUtils.formatDate(flightSchedule.getStopDateLocal(), strSMDateFormat);
				strStartFullLocal = ScheduleUtils.formatDate(flightSchedule.getStartDateLocal(), strSMDateFormat);
				strStopFullLocal = ScheduleUtils.formatDate(flightSchedule.getStopDateLocal(), strSMDateFormat);
				
				frqLocal = flightSchedule.getFrequencyLocal();
				// }else {
				strStartZulu = ScheduleUtils.formatDate(flightSchedule.getStartDate(), strSMDateFormat);
				strStopZulu = ScheduleUtils.formatDate(flightSchedule.getStopDate(), strSMDateFormat);
				
				strStartFullZulu = ScheduleUtils.formatDate(flightSchedule.getStartDate(), strSMDateFormat);
				strStopFullZulu =  ScheduleUtils.formatDate(flightSchedule.getStopDate(), strSMDateFormat);
				
				frq = flightSchedule.getFrequency();
				// }

				sb.append("arrData[" + i + "] = new Array();");
				sb.append("arrData[" + i + "][0] = '" + i + "';");
				sb.append("arrData[" + i + "][1] = '" + getNotNullString(flightSchedule.getFlightNumber()) + "';");
				sb.append("arrData[" + i + "][2] = '" + strStartZulu + "';");
				sb.append("arrData[" + i + "][3] = '" + strStopZulu + "';");
				sb.append("arrData[" + i + "][30] = '" + strStart + "';");
				sb.append("arrData[" + i + "][31] = '" + strStop + "';");
				// Add full date format 
				sb.append("arrData[" + i + "][52] = '" + strStartFullZulu + "';");
				sb.append("arrData[" + i + "][53] = '" + strStopFullZulu + "';");				
				sb.append("arrData[" + i + "][54] = '" + strStartFullLocal + "';");
				sb.append("arrData[" + i + "][55] = '" + strStopFullLocal + "';");


				String strFrequency = createSegmentArray(frq);

				HashMap<DayOfWeek, Boolean> daysMap = createDayOfWeekMap(frq);
				HashMap<DayOfWeek, Boolean> daysLocalMap = createDayOfWeekMap(frqLocal);

				sb.append("arrData[" + i + "][4] = '" + strFrequency + "';");

				String strSegmentDtls = "";// all legs;
				String legArr = "var arrLegDtls = new Array();";// leg detail array
				String legArrZulu = "var arrLegZuluDtls = new Array();";// leg detail array
				Collection<FlightScheduleLeg> segDetails = flightSchedule.getFlightScheduleLegs();

				for (int legnum = 1; legnum < segDetails.size() + 1; legnum++) {

					Iterator<FlightScheduleLeg> segDtls = segDetails.iterator();
					while (segDtls.hasNext()) {

						FlightScheduleLeg legs = (FlightScheduleLeg) segDtls.next();

						if (legs.getLegNumber() == legnum) {

							int arrayIndex = legnum - 1;
							strSegmentDtls += legs.getOrigin() + ", " + legs.getDestination() + ", ";

							// if (strTimeMode.equals(WebConstants.LOCALTIME)) {

							legArr += "arrLegDtls[" + arrayIndex + "] =new Array('" + legs.getLegNumber() + "','"
									+ legs.getOrigin() + "','" + legs.getDestination() + "','"
									+ ScheduleUtils.formatDate(legs.getEstDepartureTimeLocal(), defaultFormat) + "','"
									+ legs.getEstDepartureDayOffsetLocal() + "','"
									+ ScheduleUtils.formatDate(legs.getEstArrivalTimeLocal(), defaultFormat) + "','"
									+ legs.getEstArrivalDayOffsetLocal() + "','" + legs.getDuration() + "','" + legs.getId()
									+ "','" + SegmentUtil.isOverlappingLeg(legs, flightSchedule) + "');";

							if (legs.getLegNumber() == 1) {
								sb.append("arrData[" + i + "][28] ='"
										+ ScheduleUtils.formatDate(legs.getEstDepartureTimeLocal(), defaultFormat) + "';");
							}

							// }else {

							legArrZulu += "arrLegZuluDtls[" + arrayIndex + "] =new Array('" + legs.getLegNumber() + "','"
									+ legs.getOrigin() + "','" + legs.getDestination() + "','"
									+ ScheduleUtils.formatDate(legs.getEstDepartureTimeZulu(), defaultFormat) + "','"
									+ legs.getEstDepartureDayOffset() + "','"
									+ ScheduleUtils.formatDate(legs.getEstArrivalTimeZulu(), defaultFormat) + "','"
									+ legs.getEstArrivalDayOffset() + "','" + legs.getDuration() + "','" + legs.getId() + "','"
									+ SegmentUtil.isOverlappingLeg(legs, flightSchedule) + "');";
							if (legs.getLegNumber() == 1) {
								sb.append("arrData[" + i + "][29] ='"
										+ ScheduleUtils.formatDate(legs.getEstDepartureTimeZulu(), defaultFormat) + "';");
							}
							// }
							break;
						}
					}
				}
				if (segDetails.size() == 0) {
					sb.append("arrData[" + i + "][28] ='&nbsp';");
					sb.append("arrData[" + i + "][29] ='&nbsp';");
				}
				if (!"".equals(strSegmentDtls)) {
					strSegmentDtls = strSegmentDtls.substring(0, strSegmentDtls.lastIndexOf(","));
				}
				sb.append("arrData[" + i + "][5] = '" + ScheduleUtils.getSegment(strSegmentDtls) + "';");
				sb.append("arrData[" + i + "][6] = '" + getOperationType(flightSchedule.getOperationTypeId()) + "';");
				sb.append("arrData[" + i + "][40] = '" + flightSchedule.getOperationTypeId() + "';");

				if (flightSchedule.getAvailableSeatKilometers() != null) {
					sb.append("arrData[" + i + "][7] = '" + flightSchedule.getAvailableSeatKilometers().intValue() / 1000 + "';");
				} else {
					sb.append("arrData[" + i + "][7] = '&nbsp';");
				}

				if (flightSchedule.isOverlapping()) {
					sb.append("arrData[" + i + "][8] = 'Y';");
				} else {
					sb.append("arrData[" + i + "][8] = 'N';");
				}

				if (flightSchedule.getNumberOfDepartures() != null) {
					sb.append("arrData[" + i + "][9] = '" + flightSchedule.getNumberOfDepartures() + "';");
				} else {
					sb.append("arrData[" + i + "][9] = '" + 0 + "';");
				}

				sb.append("arrData[" + i + "][10] = '" + flightSchedule.getBuildStatusCode() + "';");
				sb.append("arrData[" + i + "][11] = '" + flightSchedule.getScheduleId() + "';");
				sb.append("arrData[" + i + "][12] = '" + flightSchedule.getStatusCode() + "';");

				sb.append("arrData[" + i + "][13] = '" + flightSchedule.getModelNumber() + "';");
				sb.append("arrData[" + i + "][14] = '" + flightSchedule.getDepartureStnCode() + "';");
				sb.append("arrData[" + i + "][15] = '" + flightSchedule.getArrivalStnCode() + "';");
				sb.append("arrData[" + i + "][16] = '" + ((Boolean) daysMap.get(DayOfWeek.SATURDAY)).booleanValue() + "';");
				sb.append("arrData[" + i + "][17] = '" + ((Boolean) daysMap.get(DayOfWeek.SUNDAY)).booleanValue() + "';");
				sb.append("arrData[" + i + "][18] = '" + ((Boolean) daysMap.get(DayOfWeek.MONDAY)).booleanValue() + "';");
				sb.append("arrData[" + i + "][19] = '" + ((Boolean) daysMap.get(DayOfWeek.TUESDAY)).booleanValue() + "';");
				sb.append("arrData[" + i + "][20] = '" + ((Boolean) daysMap.get(DayOfWeek.WEDNESDAY)).booleanValue() + "';");
				sb.append("arrData[" + i + "][21] = '" + ((Boolean) daysMap.get(DayOfWeek.THURSDAY)).booleanValue() + "';");
				sb.append("arrData[" + i + "][22] = '" + ((Boolean) daysMap.get(DayOfWeek.FRIDAY)).booleanValue() + "';");

				sb.append("arrData[" + i + "][33] = '" + ((Boolean) daysLocalMap.get(DayOfWeek.SATURDAY)).booleanValue() + "';");
				sb.append("arrData[" + i + "][34] = '" + ((Boolean) daysLocalMap.get(DayOfWeek.SUNDAY)).booleanValue() + "';");
				sb.append("arrData[" + i + "][35] = '" + ((Boolean) daysLocalMap.get(DayOfWeek.MONDAY)).booleanValue() + "';");
				sb.append("arrData[" + i + "][36] = '" + ((Boolean) daysLocalMap.get(DayOfWeek.TUESDAY)).booleanValue() + "';");
				sb.append("arrData[" + i + "][37] = '" + ((Boolean) daysLocalMap.get(DayOfWeek.WEDNESDAY)).booleanValue() + "';");
				sb.append("arrData[" + i + "][38] = '" + ((Boolean) daysLocalMap.get(DayOfWeek.THURSDAY)).booleanValue() + "';");
				sb.append("arrData[" + i + "][39] = '" + ((Boolean) daysLocalMap.get(DayOfWeek.FRIDAY)).booleanValue() + "';");

				sb.append(legArr);
				sb.append(legArrZulu);
				sb.append("arrData[" + i + "][23] = arrLegZuluDtls;");
				sb.append("arrData[" + i + "][32] = arrLegDtls;");
				sb.append("arrData[" + i + "][24] = '" + flightSchedule.getManuallyChanged() + "';"); // Flights Change
																										// flag
				sb.append("arrData[" + i + "][25] = '" + flightSchedule.getVersion() + "';");
				sb.append("arrData[" + i + "][26] = '" + flightSchedule.getOverlapingScheduleId() + "';");
				Collection<FlightScheduleSegment> segCol = flightSchedule.getFlightScheduleSegments();
				sb.append(setSegmentArr(segCol));
				sb.append("arrData[" + i + "][27] = arrSegDtls;");
				sb.append("arrData[" + i + "][41] = '" + flightSchedule.getModelNumber() + "';");

				Set<Integer> gdsIds = flightSchedule.getGdsIds();
				String gdsString = "new Array(";
				if (gdsIds != null) {

					Iterator<Integer> gdsItr = gdsIds.iterator();
					while (gdsItr.hasNext()) {
						Integer gdsId = (Integer) gdsItr.next();
						if ("new Array(".equals(gdsString)) {
							gdsString += "'" + gdsId + "'";
						} else {
							gdsString += ",'" + gdsId + "'";
						}
					}

					gdsString += ");";
					sb.append("arrData[" + i + "][42] = " + gdsString);

				} else {
					sb.append("arrData[" + i + "][42] = new Array();");
				}
				// number of Open Flights
				sb.append("arrData[" + i + "][43] = '" + flightSchedule.getNumberOfFlights() + "';");

				if ((mapNumberOfFutureFlts != null && !mapNumberOfFutureFlts.isEmpty())
						&& mapNumberOfFutureFlts.get(flightSchedule.getScheduleId()) != null) {
					sb.append("arrData[" + i + "][44] = '" + mapNumberOfFutureFlts.get(strSchedIDs) + "';");
				} else {
					sb.append("arrData[" + i + "][44] = '0';");
				}
				sb.append("arrData[" + i + "][45] = '" + ScheduleUtils.getTerminal(flightSchedule.getFlightScheduleSegments())
						+ "';");
				sb.append("arrData[" + i + "][46] = '" + flightSchedule.getFlightType() + "';");

				sb.append("arrData[" + i + "][47] = '" + FlightUtil.getFlightTypeDescription(flightSchedule.getFlightType())
						+ "';");
				if (flightSchedule.getRemarks() != null) {
					sb.append("arrData[" + i + "][48] = '" + flightSchedule.getRemarks() + "';");
				} else {
					sb.append("arrData[" + i + "][48] = '';");
				}

				if (flightSchedule.getMealTemplateId() != null) {
					sb.append("arrData[" + i + "][49] = '" + flightSchedule.getMealTemplateId() + "';");
				} else {
					sb.append("arrData[" + i + "][49] = '';");
				}

				if (flightSchedule.getSeatChargeTemplateId() != null) {
					sb.append("arrData[" + i + "][50] = '" + flightSchedule.getSeatChargeTemplateId() + "';");
				} else {
					sb.append("arrData[" + i + "][50] = '';");
				}
				sb.append("arrData[" + i + "][51] = 'false';");
				// CodeShare details
				Collection<CodeShareMCFlightSchedule> CodeShare = flightSchedule.getCodeShareMCFlightSchedules();
				sb.append(setCodeShareArr(CodeShare));
				sb.append("arrData[" + i + "][56] = arrCodeShareDtls;");
				
				if (flightSchedule.getCsOCCarrierCode() != null) {
					sb.append("arrData[" + i + "][57] = '" + flightSchedule.getCsOCCarrierCode() + "';");
				} else {
					sb.append("arrData[" + i + "][57] = '';");
				}
				if (flightSchedule.getCsOCFlightNumber() != null) {
					sb.append("arrData[" + i + "][58] = '" + flightSchedule.getCsOCFlightNumber() + "';");
				} else {
					sb.append("arrData[" + i + "][58] = '';");
				}

				sb.append("arrData[" + i + "][59] = '" + flightSchedule.getViaScheduleMessaging() + "';");

			} // end for
		}
		sb.append("var sendEmailForPax = '" + sendEmailsForFlightAlterations + "';");
		sb.append("var sendSMSForPax = '" + sendSMSForFlightAlterations + "';");
		sb.append("var allowReprotectPAX = '" + allowReprotectPAXToTodaysDepartures + "';");

		return sb.toString();
	}

	/**
	 * Gets Segment Araay for populating the Fron end
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the array of Segment details
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final String getSegmentValidationRowHtml(HttpServletRequest request) throws ModuleException {
		// --Edit mode-- if the segment validation data already exists, populate the data with the grid
		StringBuffer sb = new StringBuffer("var arrSegData = new Array();");
		String strSchedId = request.getParameter("strSchdId");
		String strLegs = request.getParameter(PARAM_LEGS) == null ? "" : request.getParameter(PARAM_LEGS);
		ArrayList<String> arrSegList = ScheduleUtils.getSgements(strLegs);
		boolean blnsegment = false;

		for (int i = 0; i < arrSegList.size(); i++) {
			blnsegment = false;
			sb.append("arrSegData[" + i + "] = new Array();");
			sb.append("arrSegData[" + i + "][0] = '" + i + "';");
			sb.append("arrSegData[" + i + "][1] = '" + arrSegList.get(i) + "';");

			if (strSchedId != null) {
				FlightSchedule flightsched = ModuleServiceLocator.getScheduleServiceBD().getFlightSchedule(
						Integer.parseInt(strSchedId));
				Set<FlightScheduleSegment> segSet = flightsched.getFlightScheduleSegments();
				if (segSet != null) {
					Iterator<FlightScheduleSegment> segIte = segSet.iterator();
					while (segIte.hasNext()) {
						FlightScheduleSegment segment = (FlightScheduleSegment) segIte.next();
						if (segment.getSegmentCode().equals((String) arrSegList.get(i))) {
							blnsegment = true;
							if (segment.getValidFlag()) {
								sb.append("arrSegData[" + i + "][2] = 'true';");
							} else {
								sb.append("arrSegData[" + i + "][2] = 'false';");
							}
						}
					}
				}
			}
			if (!blnsegment) {
				sb.append("arrSegData[" + i + "][2] = 'true';");
			}
		}
		return sb.toString();
	}

	/**
	 * Creates the Segment Array for Validation
	 * 
	 * @param flightSchedule
	 *            the Flight schedule
	 * @return String the Segment array from schedule
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public String createSegmentValidationRowHtml(FlightSchedule flightSchedule) throws ModuleException {

		StringBuffer sb = new StringBuffer("var arrOLSegData = new Array();");

		Integer olapSchedId = flightSchedule.getOverlapingScheduleId();
		if (olapSchedId != null) {
			FlightSchedule olapSched = ModuleServiceLocator.getScheduleServiceBD().getFlightSchedule(olapSchedId.intValue());

			Set<FlightScheduleSegment> segSet = olapSched.getFlightScheduleSegments();
			if (segSet != null) {
				Iterator<FlightScheduleSegment> segIte = segSet.iterator();
				int i = 0;
				while (segIte.hasNext()) {
					FlightScheduleSegment segment = (FlightScheduleSegment) segIte.next();
					sb.append("arrOLSegData[" + i + "] = new Array();");
					sb.append("arrOLSegData[" + i + "][0] = '" + i + "';");
					sb.append("arrOLSegData[" + i + "][1] = '" + segment.getSegmentCode() + "';");
					if (segment.getValidFlag()) {
						sb.append("arrOLSegData[" + i + "][2] = 'Y';");
					} else {
						sb.append("arrOLSegData[" + i + "][2] = 'N';");
					}
					sb.append("arrOLSegData[" + i + "][3] = '" + segment.getFlSchSegId() + "';");
					i++;
				}
			}

		} else {
			Collection<FlightSchedule> schedCol = ModuleServiceLocator.getScheduleServiceBD().getPossibleOverlappingSchedules(
					flightSchedule);

			if (schedCol != null) {
				Iterator<FlightSchedule> ite = schedCol.iterator();
				while (ite.hasNext()) {
					FlightSchedule sched = (FlightSchedule) ite.next();
					int schedId = sched.getScheduleId().intValue();
					sb.append("arrOLSegData[" + schedId + "] = new Array();");

					Set<FlightScheduleSegment> segSet = sched.getFlightScheduleSegments();
					if (segSet != null) {
						Iterator<FlightScheduleSegment> segIte = segSet.iterator();
						int i = 0;
						while (segIte.hasNext()) {
							FlightScheduleSegment segment = (FlightScheduleSegment) segIte.next();

							sb.append("arrOLSegData[" + schedId + "][" + i + "] = new Array();");
							sb.append("arrOLSegData[" + schedId + "][" + i + "][0] = '" + i + "';");
							sb.append("arrOLSegData[" + schedId + "][" + i + "][1] = '" + segment.getSegmentCode() + "';");
							if (segment.getValidFlag()) {
								sb.append("arrOLSegData[" + schedId + "][" + i + "][2] = 'Y';");
							} else {
								sb.append("arrOLSegData[" + schedId + "][" + i + "][2] = 'N';");
							}
							sb.append("arrOLSegData[" + schedId + "][" + i + "][3] = '" + segment.getFlSchSegId() + "';");
							i++;
						}
					}
				}
			}
		}
		return sb.toString();
	}

	/**
	 * Creates a Day of Week Map
	 * 
	 * @param freq
	 *            the Frequency
	 * @return HashMap contating Day of Week
	 */
	private HashMap<DayOfWeek, Boolean> createDayOfWeekMap(Frequency freq) {

		// initialize hashmap
		HashMap<DayOfWeek, Boolean> daysMap = new HashMap<DayOfWeek, Boolean>();
		daysMap.put(DayOfWeek.MONDAY, new Boolean(false));
		daysMap.put(DayOfWeek.SUNDAY, new Boolean(false));
		daysMap.put(DayOfWeek.TUESDAY, new Boolean(false));
		daysMap.put(DayOfWeek.WEDNESDAY, new Boolean(false));
		daysMap.put(DayOfWeek.THURSDAY, new Boolean(false));
		daysMap.put(DayOfWeek.FRIDAY, new Boolean(false));
		daysMap.put(DayOfWeek.SATURDAY, new Boolean(false));
		daysMap.put(DayOfWeek.SUNDAY, new Boolean(false));

		Collection<DayOfWeek> days = CalendarUtil.getDaysFromFrequency(freq);
		Iterator<DayOfWeek> itDays = days.iterator();

		while (itDays.hasNext()) {
			DayOfWeek day = (DayOfWeek) itDays.next();

			// reset map with selected days
			daysMap.put(day, new Boolean(true));
		}

		return daysMap;
	}

	/**
	 * Method to get the Frequency segment
	 * 
	 * @param freq
	 *            the Frequency
	 * @return String the string requency Su_TuWd__Sa
	 */
	public String createSegmentArray(Frequency freq) {

		HashMap<DayOfWeek, Boolean> daysMap = createDayOfWeekMap(freq);

		// create frequency string using daysMap
		String strFrq = "";
		String[] dayweek = { "Su", "Mo", "Tu", "We", "Th", "Fr", "Sa", };

		int intoffset = Integer.parseInt(dayOffSet);
		intoffset = intoffset % 7;
		for (int i = 0; i < 7; i++) {
			if (intoffset == 7)
				intoffset = 0;
			strFrq += (((Boolean) daysMap.get(CalendarUtil.getDayFromDayNumber(i))).booleanValue()) ? dayweek[intoffset] : "__";
			intoffset++;
		}
		return strFrq;
	}

	/**
	 * Gets the Client validations for Flight Schedule
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array of Client Validations
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {
		if (clientErrors == null) {
			Properties moduleErrs = new Properties();

			moduleErrs.setProperty("um.flightschedule.startdate.null", "startDate");
			moduleErrs.setProperty("um.Flightschedule.stopdate.null", "stopDate");
			moduleErrs.setProperty("um.Flightschedule.search.mandatory", "mandatorysearch");
			moduleErrs.setProperty("um.flightschedule.stopdate.notlessthanequalstartdate", "startdateless");
			moduleErrs.setProperty("um.flightSchedule.departurearrival.same", "departurearrival");
			moduleErrs.setProperty("um.flightschedule.startdate.lessthanorequal.currentdate", "lesscurrentdate");
			moduleErrs.setProperty("um.flightschedule.stopdate.notgte.startdate", "greaterthanstart");
			moduleErrs.setProperty("um.flightschedule.scheduledaterange.invalid", "invalidrange");
			moduleErrs.setProperty("um.flightschedule.operationtype.null", "optTypenull");
			moduleErrs.setProperty("um.flightschedule.operationtype.invalid", "opyTypeinvalid");
			moduleErrs.setProperty("um.flightschedule.flightno.null", "flightnonull");
			moduleErrs.setProperty("um.flightschedule.type.null", "flttypenull");
			moduleErrs.setProperty("um.flightschedule.aircraftModel.null", "aircraftModelnull");
			moduleErrs.setProperty("um.flightschedule.aircraftModel.invalidmodel", "invalidmodel");
			moduleErrs.setProperty("um.flightschedule.flightnumber.notvalid1", "flightinvalid");
			moduleErrs.setProperty("um.flightschedule.flightnumber.notvalid2", "flightinvalidno");
			moduleErrs.setProperty("um.flightschedule.frequency.not.atleastone", "invalidFreq");
			moduleErrs.setProperty("um.flightschedule.frequency.new.newfreq", "newFreq");
			moduleErrs.setProperty("um.flightschedule.edit.bothless", "editbothless");
			moduleErrs.setProperty("um.flightschedule.edit.bothgreat", "editbothgreat");
			moduleErrs.setProperty("um.flightschedule.frequency.not.atleastoneleg", "onelegneeded");
			moduleErrs.setProperty("um.flightschedule.legdetails.notcompleted", "incompleteleg");
			moduleErrs.setProperty("um.flightschedule.departure.invalid", "invaliddepature");
			moduleErrs.setProperty("um.flightschedule.departuretime.invalid", "invaliddepTime");
			moduleErrs.setProperty("um.flightschedule.allsegments.invalid", "invalidsegments");
			moduleErrs.setProperty("um.flightschedule.legdetails.morethanoneleg", "morethanoneleg");
			moduleErrs.setProperty("um.flightschedule.leg.invaliddepttime", "invaliddepaturetime");
			moduleErrs.setProperty("um.flightschedule.leg.invalidarrivaltime", "invalidarrivaltime");
			moduleErrs.setProperty("um.flightschedule.leg.invalidroute", "invalidroute");
			moduleErrs.setProperty("um.flightschedule.leg.inactivedeptarue", "inactivedeptarue");
			moduleErrs.setProperty("um.flightschedule.leg.inactivearrival", "inactivearrival");

			moduleErrs.setProperty("um.flightnumber.alreadyused", "usedflight");
			moduleErrs.setProperty("um.FlightSchedule.overlap.existing", "overlap");
			moduleErrs.setProperty("um.segmentsoverlap.confirm", "confirmoverlap");
			moduleErrs.setProperty("um.flightschedule.selectschedules.needbuild", "buildneed");
			moduleErrs.setProperty("um.flightschedule.selectschedules.noopenflts", "noOpenFlts");

			moduleErrs.setProperty("um.Arrival.departure.times.Invalid", "invalidrange");
			moduleErrs.setProperty("um.flightschedule.nextstartdate.null", "nextstartdate");
			moduleErrs.setProperty("um.flightschedule.stopdate.greater", "stopdategreatercaldate");

			moduleErrs.setProperty("um.flightschedule.copy.fromday.null", "copyfromdate");
			moduleErrs.setProperty("um.flightschedule.copy.todate.null", "copytodate");
			moduleErrs.setProperty("um.flightschedule.copy.lessthancurr", "cpylessthancurr");
			moduleErrs.setProperty("um.flightschedule.copy.tolessthanfrom", "tolessthanfrom");
			moduleErrs.setProperty("um.flightschedule.copy.fromconflict", "fromconflict");
			moduleErrs.setProperty("um.flightschedule.copy.toconflict", "toconflict");

			moduleErrs.setProperty("um.flightschedule.split.fromnull", "splfromnull");
			moduleErrs.setProperty("um.flightschedule.split.fromlesscurr", "splfromlesscurr");
			moduleErrs.setProperty("um.flightschedule.split.tolessfrom", "spltolessfrom");
			moduleErrs.setProperty("um.flightschedule.split.rangclash", "rangeclash");
			moduleErrs.setProperty("um.flightschedule.split.samerange", "splsamerange");
			moduleErrs.setProperty("um.flightschedule.split.frequency", "splfrequency");
			moduleErrs.setProperty("um.flightschedule.split.bothless", "splbothless");
			moduleErrs.setProperty("um.flightschedule.split.boyhgreat", "splboyhgreat");
			moduleErrs.setProperty("um.flightschedule.split.splinvaliddate", "splinvaliddate");
			moduleErrs.setProperty("um.flightschedule.split.splstless", "splstless");
			moduleErrs.setProperty("um.flightschedule.split.orggreat", "splstpgreat");
			moduleErrs.setProperty("um.airadmin.delete.confirmation", "deletemessage");

			moduleErrs.setProperty("um.flightschedule.email.noaddress", "noaddress");
			moduleErrs.setProperty("um.flightschedule.email.invalidmailformat", "invalidmailformat");
			moduleErrs.setProperty("um.flightschedule.email.invalidlength", "invalidlength");
			moduleErrs.setProperty("um.flightschedule.search.invalidcarrier", "invalidcarrier");

			moduleErrs.setProperty("um.flightschedule.sms.empty", "smsempty");
			moduleErrs.setProperty("um.flight.remarks.char.invalid", "invalidCharRem");
			
			moduleErrs.setProperty("um.flight.gds.cs.carrier.null", "CScarriernull");
			moduleErrs.setProperty("um.flight.gds.cs.flightNum.null", "CSfltnonull");
			moduleErrs.setProperty("um.flight.gds.cs.OC.MC.invalid", "invalidOCandMC");
			moduleErrs.setProperty("um.flight.gds.cs.alreadyexist", "codeShareExists");
			moduleErrs.setProperty("um.flight.gds.cs.un.publish", "codeShareUnPublish");
			moduleErrs.setProperty("um.flightschedule.selFromStn.null","fromAirportNull");
			
			clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}
		return clientErrors;
	}

	/**
	 * Method to Create the Segment array
	 * 
	 * @param segs
	 *            the ollection of segments
	 * @return String the Segment Detail Array
	 */
	private String setSegmentArr(Collection<FlightScheduleSegment> segs) {

		String segArr = "var arrSegDtls = new Array();";// leg detail array
		int j = 0;
		if (segs != null) {
			Iterator<FlightScheduleSegment> iter = segs.iterator();
			while (iter.hasNext()) {
				FlightScheduleSegment schSeg = (FlightScheduleSegment) iter.next();
				segArr += "arrSegDtls[" + j + "] =new Array('" + schSeg.getFlSchSegId() + "','" + schSeg.getSegmentCode() + "','"
						+ schSeg.getValidFlag() + "','" + schSeg.getOverlapSegmentId() + "');";

				j++;
			}
		}
		return segArr;
	}

	/**
	 * Creates the Model date from front end values
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array contatiing the Model
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public String createModel(HttpServletRequest request) throws ModuleException {

		String strStartDate = request.getParameter("txtStartDate");
		String strStopDate = request.getParameter("txtStopDate");
		String strOpType = request.getParameter("selOperationType");
		String strFlightNo = request.getParameter("txtFlightNo");
		String strAircraftModel = request.getParameter("selAircraftModel");
		String strLeg1from = request.getParameter("selFromStn1");
		String strLeg1to = request.getParameter("selToStn1");
		String strLeg1Depature = request.getParameter("txtD_Time1");
		String strLeg1Arrival = request.getParameter("txtA_Time1");
		String strLeg1Depday = request.getParameter("selD_Day1");
		String strLeg1Arrday = request.getParameter("selA_Day1");
		String strLeg2from = request.getParameter("selFromStn2");
		String strLeg2to = request.getParameter("selToStn2");
		String strLeg2Depature = request.getParameter("txtD_Time2");
		String strLeg2Arrival = request.getParameter("txtA_Time2");
		String strLeg2Depday = request.getParameter("selD_Day2");
		String strLeg2Arrday = request.getParameter("selA_Day2");
		String strLeg3from = request.getParameter("selFromStn3");
		String strLeg3to = request.getParameter("selToStn3");
		String strLeg3Depature = request.getParameter("txtD_Time3");
		String strLeg3Arrival = request.getParameter("txtA_Time3");
		String strLeg3Depday = request.getParameter("selD_Day3");
		String strLeg3Arrday = request.getParameter("selA_Day3");
		String strLeg4from = request.getParameter("selFromStn4");
		String strLeg4to = request.getParameter("selToStn4");
		String strLeg4Depature = request.getParameter("txtD_Time4");
		String strLeg4Arrival = request.getParameter("txtA_Time4");
		String strLeg4Depday = request.getParameter("selD_Day4");
		String strLeg4Arrday = request.getParameter("selA_Day4");
		String strLeg5from = request.getParameter("selFromStn5");
		String strLeg5to = request.getParameter("selToStn5");
		String strLeg5Depature = request.getParameter("txtD_Time5");
		String strLeg5Arrival = request.getParameter("txtA_Time5");
		String strLeg5Depday = request.getParameter("selD_Day5");
		String strLeg5Arrday = request.getParameter("selA_Day5");
		String strOverLapSeg = request.getParameter("hdnOverLapSeg");
		String strOverLapSegId = request.getParameter("hdnOverLapSegId");
		String strOverLapSegSchedId = request.getParameter("hdnOverLapSegSchedId");
		String strSegArray = request.getParameter("hdnSegArray");
		String strTerminalArray = request.getParameter("hdnTerminalArray");
		String strScheduleId = request.getParameter("hdnScheduleId");
		String strOverlapSchID = request.getParameter("hdnOverlapSchID");
		String strMonday = request.getParameter("chkMonday");
		String strTuesday = request.getParameter("chkTuesday");
		String strWednesday = request.getParameter("chkWednesday");
		String strThursday = request.getParameter("chkThursday");
		String strFriday = request.getParameter("chkFriday");
		String strSaturday = request.getParameter("chkSaturday");
		String strSunday = request.getParameter("chkSunday");
		String strMode = request.getParameter("hdnMode");
		String strFltstart = request.getParameter("txtFlightNoStart");
		String strVersion = request.getParameter("hdnVersion");
		String strStatus = request.getParameter("hdnStatus");
		String strGridrow = request.getParameter("hdnGridRow");
		String strBuildStatus = request.getParameter("hdnBulidStatus");
		String strFlightType = request.getParameter("selFlightType");
		String txtRemarks = request.getParameter("txtRemarks");
		String selSeatChargeTemplate = request.getParameter("selSeatChargeTemplate");
		String selMealTemplate = request.getParameter("selMealTemplate");
		String strCodeShareMC= request.getParameter("hndCodeShare");
		

		StringBuffer sb = new StringBuffer();
		sb.append("var model= new Array();");
		String formFields[] = { "txtStartDate", "txtStopDate", "selOperationType", "txtFlightNo", "selAircraftModel",
				"selFromStn1", "selToStn1", "txtD_Time1", "txtA_Time1", "selD_Day1", "selA_Day1", "selFromStn2", "selToStn2",
				"txtD_Time2", "txtA_Time2", "selD_Day2", "selA_Day2", "selFromStn3", "selToStn3", "txtD_Time3", "txtA_Time3",
				"selD_Day3", "selA_Day3", "selFromStn4", "selToStn4", "txtD_Time4", "txtA_Time4", "selD_Day4", "selA_Day4",
				"selFromStn5", "selToStn5", "txtD_Time5", "txtA_Time5", "selD_Day5", "selA_Day5", "hdnOverLapSeg",
				"hdnOverLapSegId", "hdnOverLapSegSchedId", "hdnSegArray", "hdnScheduleId", "hdnOverlapSchID", "chkMonday",
				"chkTuesday", "chkWednesday", "chkThursday", "chkFriday", "chkSaturday", "chkSunday", "hdnMode",
				"txtFlightNoStart", "hdnVersion", "hdnStatus", "hdnGridRow", "hdnBulidStatus", "hdnTerminalArray",
				"selFlightType", "txtRemarks", "selSeatChargeTemplate", "selMealTemplate","sel_CodeShare"};

		String formData[] = new String[60];
		formData[0] = strStartDate;
		formData[1] = strStopDate;
		formData[2] = strOpType;
		formData[3] = strFlightNo;
		formData[4] = strAircraftModel;
		formData[5] = strLeg1from;
		formData[6] = strLeg1to;
		formData[7] = strLeg1Depature;
		formData[8] = strLeg1Arrival;
		formData[9] = strLeg1Depday;
		formData[10] = strLeg1Arrday;
		formData[11] = strLeg2from;
		formData[12] = strLeg2to;
		formData[13] = strLeg2Depature;
		formData[14] = strLeg2Arrival;
		formData[15] = strLeg2Depday;
		formData[16] = strLeg2Arrday;
		formData[17] = strLeg3from;
		formData[18] = strLeg3to;
		formData[19] = strLeg3Depature;
		formData[20] = strLeg3Arrival;
		formData[21] = strLeg3Depday;
		formData[22] = strLeg3Arrday;
		formData[23] = strLeg4from;
		formData[24] = strLeg4to;
		formData[25] = strLeg4Depature;
		formData[26] = strLeg4Arrival;
		formData[27] = strLeg4Depday;
		formData[28] = strLeg4Arrday;
		formData[29] = strLeg5from;
		formData[30] = strLeg5to;
		formData[31] = strLeg5Depature;
		formData[32] = strLeg5Arrival;
		formData[33] = strLeg5Depday;
		formData[34] = strLeg5Arrday;
		formData[35] = strOverLapSeg;
		formData[36] = strOverLapSegId;
		formData[37] = strOverLapSegSchedId;
		formData[38] = strSegArray;
		formData[39] = strScheduleId;
		formData[40] = strOverlapSchID;
		formData[41] = strMonday;
		formData[42] = strTuesday;
		formData[43] = strWednesday;
		formData[44] = strThursday;
		formData[45] = strFriday;
		formData[46] = strSaturday;
		formData[47] = strSunday;
		formData[48] = strMode;
		formData[49] = strFltstart;
		formData[50] = strVersion;
		formData[51] = strStatus;
		formData[52] = strGridrow;
		formData[53] = strBuildStatus;
		formData[54] = strTerminalArray;
		formData[55] = strFlightType;
		formData[56] = txtRemarks;
		formData[57] = selSeatChargeTemplate;
		formData[58] = selMealTemplate;
		formData[59] = strCodeShareMC;

		for (int i = 0; i < formData.length; i++) {
			sb.append("model['" + formFields[i] + "']='" + formData[i] + "';");
		}

		return sb.toString();

	}

	/**
	 * Gets an array of Flights Can be Reprotected to
	 * 
	 * @param map
	 *            the Map of Flight summary DTOs
	 * @return String array of Can be Reprotected flights
	 */
	@SuppressWarnings("rawtypes")
	public String getCanReprotectArray(HashMap map) {

		StringBuffer strReprot = new StringBuffer("var arrreproData = new Array();");
		FlightSummaryDTO fltSum = null;
		FlightSegmentResInvSummaryDTO fltSegResSumDTO = null;
		ArrayList sumArray = null;
		Set summarySet = map.keySet();
		Iterator iter = summarySet.iterator();
		int i = 0;
		while (iter.hasNext()) {

			fltSum = (FlightSummaryDTO) iter.next();
			SimpleDateFormat sfmt = new SimpleDateFormat(strSMDateFormat);
			String strdDepDate = "";
			Date depDt = fltSum.getDepatureDate();
			if (depDt != null) {
				try {
					strdDepDate = sfmt.format(depDt);
				} catch (Exception e) {
					// do nothing
				}
			}

			sumArray = (ArrayList) map.get(fltSum);
			for (int j = 0; j < sumArray.size(); j++) {
				fltSegResSumDTO = (FlightSegmentResInvSummaryDTO) sumArray.get(j);
				strReprot.append("arrreproData[" + i + "] = new Array();");
				strReprot.append("arrreproData[" + i + "][0] = '" + i + "';");
				strReprot.append("arrreproData[" + i + "][1] = '" + strdDepDate + "';");
				strReprot.append("arrreproData[" + i + "][2] = '" + fltSum.getFlightId() + "';");

				strReprot.append("arrreproData[" + i + "][3] = '" + fltSegResSumDTO.getSegmentCode() + "';");
				strReprot.append("arrreproData[" + i + "][4] = '" + fltSegResSumDTO.getSoldSeatsAdult() + "/"
						+ fltSegResSumDTO.getSoldSeatsInfant() + "';");
				strReprot.append("arrreproData[" + i + "][5] = '" + fltSegResSumDTO.getConfirmedPnrs() + "';");
				strReprot.append("arrreproData[" + i + "][6] = '" + fltSegResSumDTO.getOnholdSeatsAdult() + "/"
						+ fltSegResSumDTO.getOnholdSeatsInfant() + "';");
				strReprot.append("arrreproData[" + i + "][7] = '" + fltSegResSumDTO.getOnholdPnrs() + "';");
				i++;
			}
		}

		return strReprot.toString();
	}

	/**
	 * Gets a not null string for Grid
	 * 
	 * @param str
	 *            the String
	 * @return String the not null string for Grid
	 */
	private static String getNotNullString(String str) {
		return (str == null) ? "&nbsp" : str;
	}

	/**
	 * Gets the Operation Type
	 * 
	 * @param strInt
	 *            the Operation type int value
	 * @return String the operation type
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static String getOperationType(int strInt) throws ModuleException {
		Collection<String[]> opcol = SelectListGenerator.getOperationType();
		if (opcol != null) {
			Iterator<String[]> it = opcol.iterator();
			while (it.hasNext()) {
				String s[] = (String[]) it.next();
				if (strInt == Integer.parseInt(s[0])) {
					return s[1];
				}
			}
		}
		return "&nbsp;";
	}

	// Haider 23Oct08
	private static boolean isGDSPublished(int id, String[] gdsIds) {
		int len = gdsIds.length;
		for (int j = 0; j < len; j++) {
			if (id == Integer.valueOf(gdsIds[j]).intValue())
				return true;
		}
		return false;
	}

	public final String getGDSData(String gdsIds) throws ModuleException {
		
		StringBuffer sb = new StringBuffer("var arrData = new Array();");
		String[] arGdsIds = StringUtils.split(gdsIds, ',');
		List<Gds> gdsList = ModuleServiceLocator.getGdsServiceBD().getGDSs();
		if (gdsList != null) {
			Object[] listArr = gdsList.toArray();
			HashMap<String, String> publishMechMap = (HashMap<String, String>) CommonsServices.getGlobalConfig()
					.getPublishMechanismMap();
			int len = gdsList.size();
			int index = 0;
			Gds gds = null;
			for (int i = 0; i < len; i++) {
				gds = (Gds) listArr[i];
				if(gds.getSchedulingSystem()){
					continue;
				}
				sb.append("arrData[" + index + "] = new Array();");
				sb.append("arrData[" + index + "][1] = '" + gds.getGdsCode() + "';");
				sb.append("arrData[" + index + "][2] = '" + (isGDSPublished(gds.getGdsId(), arGdsIds) ? 1 : 0) + "';");
				sb.append("arrData[" + index + "][3] = '" + ("ACT".equals(gds.getStatus()) ? "Active" : "Inactive") + "';");
				sb.append("arrData[" + index + "][4] = '" + publishMechMap.get(gds.getSchedPublishMechanismCode()) + "';");
				sb.append("arrData[" + index + "][5] = '';");
				sb.append("arrData[" + index + "][6] = '" + gds.getGdsId() + "';");
				sb.append("arrData[" + index + "][7] = '" + gds.getCodeShareCarrier() + "';");
				
				++index;
			}
		}
		return sb.toString();
	}
	/**
	 * Method to Create the CodeShare array
	 * 
	 * @param CodeShares
	 *            the collection of CodeShare
	 * @return String the CodeShare Detail Array
	 */
	private String setCodeShareArr(Collection<CodeShareMCFlightSchedule> CodeShares) {

		String CodeShareArr = "var arrCodeShareDtls = new Array();";// CodeShare detail array
		int j = 0;
		if (CodeShares != null) {
			Iterator<CodeShareMCFlightSchedule> iter = CodeShares.iterator();
			while (iter.hasNext()) {
				CodeShareMCFlightSchedule schCodeShare = (CodeShareMCFlightSchedule) iter.next();
				CodeShareArr += "arrCodeShareDtls[" + j + "] =new Array('" + schCodeShare.getCsMCCarrierCode() + "|" + schCodeShare.getCsMCFlightNumber()+ "');";

				j++;
			}
		}
		return CodeShareArr;
	}
}
