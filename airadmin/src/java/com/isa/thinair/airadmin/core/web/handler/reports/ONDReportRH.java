package com.isa.thinair.airadmin.core.web.handler.reports;

import java.io.File;
import java.io.FileInputStream;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleConfig;
import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.FTPServerProperty;
import com.isa.thinair.commons.core.util.FTPUtil;
import com.isa.thinair.platform.api.constants.PlatformConstants;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.api.v2.util.DateUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

public class ONDReportRH extends BasicRequestHandler {
	private static Log log = LogFactory.getLog(ONDReportRH.class);

	/**
	 * @param request
	 * @return
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {

		String strHdnMode = request.getParameter("hdnMode");
		String forward = WebConstants.ACTION_FORWARD_SUCCESS;
		String timestamp = DateUtil.formatDate(DateUtil.getZuluTime(), "yyMMddHHmmss");

		try {
			setDisplayData(request);
			log.debug("ONDReportRH success");
		} catch (Exception e) {
			forward = AdminStrutsConstants.AdminAction.ERROR;
			log.error("ONDReportRH execute()" + e.getMessage());
		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response, timestamp);

				AiradminModuleConfig adminCon = AiradminModuleUtils.getConfig();
				if (adminCon.getEnableFTPRevenue().equals("true")) {
					downloadLocal(request, timestamp);
				} 
				// Setting the nfs mounted url to download the report
				User curUser = (User) request.getSession().getAttribute("sesCurrUser");
				String reportUrl = AppSysParamsUtil.getReportURLPRefix() + "reven_rept_ond_" + curUser.getUserId() + "_" + timestamp + ".zip";
				request.setAttribute(WebConstants.REQ_REPORT_URL, reportUrl);

				forward = AdminStrutsConstants.AdminAction.OND_SUCCESS;
			}

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("ONDReportRH setReportView Failed " + e.getMessageString());
		} catch (Exception e) {
			log.error("ONDReportRH setReportView Failed " + e);
		}
		try {
			if (strHdnMode != null && strHdnMode.equals("download")) {
				setReportResult(request, response, timestamp);
				forward = AdminStrutsConstants.AdminAction.OND_SUCCESS;

			} else {
				log.error("ROPRevenueReportRH setReportView not selected");
			}

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("ROPRevenueReportRH download Failed " + e.getMessageString());
		} catch (Exception e) {
			log.error("ROPRevenueReportRH download Failed " + e);
		}

		return forward;
	}

	/**
	 * Sets the Display Data
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	public static void setDisplayData(HttpServletRequest request) throws Exception {
		setClientErrors(request);
		setAirportList(request);
		setBcHtml(request);
		setAgentTypes(request);
		setGSAMultiSelectList(request);
		setStaions(request);
		setLiveStatus(request);
		setReportingPeriod(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);
	}

	/**
	 * Sets the Client Validations for Reports
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * 
	 */
	public static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);
			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	private static void setAirportList(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createAirportCodeList();
		request.setAttribute(WebConstants.REQ_HTML_AIRPORT_LIST, strHtml);
	}

	public static void setBcHtml(HttpServletRequest request) throws ModuleException {
		String strBcList = JavascriptGenerator.createBookingCodeHtml();
		request.setAttribute(WebConstants.REQ_HTML_BC_DATA, strBcList);
	}

	public static void setStaions(HttpServletRequest request) throws ModuleException {
		String strstnList = JavascriptGenerator.createStationsByCountry();
		request.setAttribute(WebConstants.REQ_HTML_STN_DATA, strstnList);
	}

	protected static void setGSAMultiSelectList(HttpServletRequest request) throws ModuleException {
		boolean blnWithTAs = false;
		boolean blnWithCOs = false;
		String strAgentType = "";
		String strList = "";
		if (request.getParameter("selAgencies") != null) {
			strAgentType = request.getParameter("selAgencies");
		}
		if (request.getParameter("chkCOs") != null) {
			blnWithCOs = (request.getParameter("chkCOs").equals("on") ? true : false);
		}
		if (request.getParameter("chkTAs") != null) {
			blnWithTAs = (request.getParameter("chkTAs").equals("on") ? true : false);
		}
		strList = ReportsHTMLGenerator.createAgentGSAMultiSelect(strAgentType, blnWithTAs, blnWithCOs);
		request.setAttribute(WebConstants.REQ_HTML_GSA_LIST, strList);
	}

	protected static void setAgentTypes(HttpServletRequest request) throws ModuleException {
		String strATList = SelectListGenerator.createAgentTypeList_SG();
		request.setAttribute(WebConstants.REQ_HTML_AGENTTYPE_LIST, strATList);
	}

	/**
	 * Setting the reporting period to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setReportingPeriod(HttpServletRequest request) throws ModuleException {
		int period = new Integer(AiradminModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.REPORTING_PERIOD)).intValue();
		request.setAttribute(WebConstants.REP_RPT_START_DAY, ReportsHTMLGenerator.getReportingPeriod(period));
	}

	/**
	 * Display the Report for Search Data
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @param response
	 *            the HttpServletResponse
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setReportView(HttpServletRequest request, HttpServletResponse response, String timestamp) throws ModuleException {

		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		User curUser = (User) request.getSession().getAttribute("sesCurrUser");
		String strUsrerId = curUser.getUserId()+ "_" + timestamp;
		
		String reportTemplate = "ONDReport.jasper";

		String strAgents = request.getParameter("hdnAgents");
		String strBCs = request.getParameter("hdnBCs");
		String strSegments = request.getParameter("hdnSegments");
		String strStations = request.getParameter("hdnStations");
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);

		// To provide Report Format Options
		String strReportFormat = request.getParameter("radRptNumFormat");

		try {
			ReportsSearchCriteria search = new ReportsSearchCriteria();

			search.setReportType(ReportsSearchCriteria.OND_REPORT);
			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));
			if (fromDate != null && !fromDate.equals("")) {
				search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate) + " 00:00:00");
			}

			if (toDate != null && !toDate.equals("")) {
				search.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate) + " 23:59:59");
			}
			if (strAgents != null && !strAgents.equals("")) {
				search.setAgentCode(strAgents);
			}
			if (strBCs != null && !strBCs.equals("")) {
				search.setBookinClass(strBCs);
			}
			if (strSegments != null && !strSegments.equals("")) {
				search.setSegment(strSegments);
			}
			if (strStations != null && !strStations.equals("")) {
				search.setStation(strStations);
			}
			if (strUsrerId != null) {
				search.setUserId(strUsrerId);
			}

			if (strReportFormat != null && !strReportFormat.equals("")) {
				if (strReportFormat.equalsIgnoreCase("IT")) {
					search.setReqReportFormat("FR");
				} else
					search.setReqReportFormat("");
			}

			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}
			ModuleServiceLocator.getDataExtractionBD().getONDData(search);
		} catch (Exception e) {
			log.error("OND report view failed * :" + e);
		}
	}

	/**
	 * Sets the report medium to the request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleExceptions
	 */
	private static void setLiveStatus(HttpServletRequest request) throws ModuleException {
		String strLive = request.getParameter(WebConstants.REP_HDN_LIVE);
		if (strLive != null)
			request.setAttribute(WebConstants.REP_SET_LIVE, strLive);

	}

	protected static void setReportResult(HttpServletRequest request, HttpServletResponse response, String timestamp) throws ModuleException {

		User curUser = (User) request.getSession().getAttribute("sesCurrUser");
		String strUsrerId = curUser.getUserId() + "_" + timestamp;
		
		AiradminModuleConfig adminCon = AiradminModuleUtils.getConfig();
		String strRevPath = adminCon.getRevenueReportPath();
		String reportsRootDir = PlatformConstants.getConfigRootAbsPath() + "/templates/reports/reven_rept_ond";

		String filePath = "";
		if (adminCon.getEnableFTPRevenue().equals("true")) {
			filePath = reportsRootDir + "_" + strUsrerId + ".csv";
		} else {
			filePath = strRevPath + "_ond_" + strUsrerId + ".csv";
		}

		String fileNameNew = "ONDRevenueReport.csv";

		try {
			File uFile = new File(filePath);
			int fSize = (int) uFile.length();
			log.debug("file size " + fSize);
			FileInputStream fis = new FileInputStream(uFile);
			response.setContentType("application/x-msdownload");
			response.addHeader("Content-Disposition", "attachment; filename=" + fileNameNew);
			response.setContentLength(fSize);
			PrintWriter pw = response.getWriter();
			int c = -1;
			// Loop to read and write bytes.

			while ((c = fis.read()) != -1) {
				pw.print((char) c);
			}
			// Close output and input resources.
			fis.close();
			pw.flush();
			pw = null;
			request.setAttribute("Revresult", "Download Complete");
		} catch (Exception e) {
			request.setAttribute("Revresult", "Download Failed. Please try again later.");
			log.error("download method is failed :" + e);
		}

	}

	private static void downloadLocal(HttpServletRequest request, String timestamp) throws Exception {

		AiradminModuleConfig adminCon = AiradminModuleUtils.getConfig();
		User curUser = (User) request.getSession().getAttribute("sesCurrUser");
		String strUsrerId = curUser.getUserId() + "_" + timestamp;
	
		String strRevPath = adminCon.getRevFTPPath();
		strRevPath = strRevPath + "_ond_" + strUsrerId + ".csv";
		String reportsRootDir = PlatformConstants.getConfigRootAbsPath() + "/templates/reports/reven_rept_ond";

		reportsRootDir = reportsRootDir + "_" + strUsrerId + ".csv";
		FTPServerProperty serverProperty = new FTPServerProperty();
		FTPUtil ftpUtil = new FTPUtil();

		// Code to FTP
		serverProperty.setServerName(adminCon.getRptFTPServer());
		if (adminCon.getRptFTPUserName() == null) {
			serverProperty.setUserName("");
		} else {
			serverProperty.setUserName(adminCon.getRptFTPUserName());
		}

		if (adminCon.getRptFTPPassword() == null) {
			serverProperty.setPassword("");
		} else {
			serverProperty.setPassword(adminCon.getRptFTPPassword());
		}
		boolean downloaded = false;

		try {
			if (ftpUtil.connectAndLogin(serverProperty.getServerName(), serverProperty.getUserName(),
					serverProperty.getPassword())) {
				ftpUtil.binary();
				downloaded = ftpUtil.downloadFile(strRevPath, reportsRootDir);
				if (downloaded) {
					downloaded = new File(reportsRootDir).exists();
				}
			}
		} catch (Exception e) {
			log.error("download attachment failed [" + e.getMessage() + "]", e);
		} finally {
			try {
				if (ftpUtil.isConnected()) {
					ftpUtil.disconnect();
				}
			} catch (Exception e) {
				log.error("Disconnecting FTP connection failed.", e);
			}
		}

	}

}
