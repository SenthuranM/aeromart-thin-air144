package com.isa.thinair.airadmin.core.web.handler.inventory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.inventory.MIAllocationHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airinventory.api.dto.FCCSegInventoryUpdateDTO;
import com.isa.thinair.airinventory.api.dto.FCCSegInventoryUpdateStatusDTO;
import com.isa.thinair.airinventory.api.dto.OperationStatusDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.model.FCCSegBCInventory;
import com.isa.thinair.airinventory.api.util.BookingClassUtil;
import com.isa.thinair.airmaster.api.model.AircraftCabinCapacity;
import com.isa.thinair.airmaster.api.model.AircraftModel;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author cBnadara
 * 
 */
public class MIAllocationRequestHandler extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(MIAllocationRequestHandler.class);
	private static final String PARAM_MODE = "hdnMode";

	/**
	 * Main Execute Method for Seat Inventory Action
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {
		String strHdnMode = request.getParameter(PARAM_MODE);
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;
		request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, "var saveSuccess = '';");
		try {
			if (strHdnMode != null && (strHdnMode.equals(WebConstants.ACTION_SAVE) || strHdnMode.equals("SAVEOL"))) {

				FCCSegInventoryUpdateStatusDTO updateStatus = saveData(request);
				saveMessage(request, updateStatus.getMsg(),
						updateStatus.getStatus() == OperationStatusDTO.OPERATION_SUCCESS ? WebConstants.MSG_SUCCESS
								: WebConstants.MSG_ERROR);

				if (updateStatus.getStatus() == OperationStatusDTO.OPERATION_SUCCESS
						|| updateStatus.getStatus() == OperationStatusDTO.OPERATION_COMPLETED) {

					request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, "var saveSuccess = 1;");

				} else if (updateStatus.getStatus() == OperationStatusDTO.OPERATION_FAILURE) {
					request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, "var saveSuccess = 0;");
				}

			}
		} catch (Exception exception) {
			log.error("MIAllocationRequestHandler.execute() method is failed :", exception);
			if (exception instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
			} else {
				saveMessage(request, exception.getMessage(), WebConstants.MSG_ERROR);
			}
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, "var saveSuccess = 0;");
		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_GRID_CLICKED)) {
				log.debug("GRID CLICKED - execute()");
				String strFareId = request.getParameter("fareId");
				String strFareDetails = MIAllocationHTMLGenerator.getFareDetails(strFareId);
				request.setAttribute(WebConstants.REQ_MANAGE_INVENTORY_FARES, strFareDetails);
			} else {
				setDisplayData(request);
			}
		} catch (Exception e) {
			if (e instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, e.getMessage(), "", "");
				log.error("MIAllocationRequestHandler.execute() method is failed :", e);
			} else {
				log.error("MIAllocationRequestHandler.execute() method is failed :", e);
				saveMessage(request, e.getMessage(), WebConstants.MSG_ERROR);
			}
		}
		return forward;
	}

	/**
	 * saves the Inventory Data
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return FCCSegInventoryUpdateStatusDTO the response data
	 * @throws Exception
	 *             the Exception
	 */
	private static FCCSegInventoryUpdateStatusDTO saveData(HttpServletRequest request) throws Exception {
		FCCSegInventoryUpdateStatusDTO updateStatus = null;
		FCCSegInventoryUpdateDTO fCCSegInventoryUpdateDTO = new FCCSegInventoryUpdateDTO();
		// Variable to set the newly added inventories
		Set<FCCSegBCInventory> setAddedInventories = null;
		// Variable to set the edited inventories
		Set<FCCSegBCInventory> setEditedInventories = null;
		// Variable to set the deleted inventories
		Set<Integer> setDeletedInventories = null;

		String strSegSaveData = AiradminUtils.getNotNullString(request.getParameter("hdnSaveSegData"));
		String strBcSaveData = AiradminUtils.getNotNullString(request.getParameter("hdnSaveBCData"));

		if (strSegSaveData.length() > 0) {
			String[] strSegmentColData = strSegSaveData.split("~");
			int flightId = Integer.parseInt(strSegmentColData[1]);
			// strSegInv=fccsInvId|flightId|segCode|cos|oversell|curtailed|infantalloc|version
			fCCSegInventoryUpdateDTO.setFccsInventoryId(Integer.parseInt(strSegmentColData[0]));
			fCCSegInventoryUpdateDTO.setFlightId(flightId);
			fCCSegInventoryUpdateDTO.setSegmentCode(strSegmentColData[2]);
			fCCSegInventoryUpdateDTO.setLogicalCCCode(strSegmentColData[3]);
			fCCSegInventoryUpdateDTO.setOversellSeats(Integer.parseInt(strSegmentColData[4]));
			fCCSegInventoryUpdateDTO.setCurtailedSeats(Integer.parseInt(strSegmentColData[5]));
			fCCSegInventoryUpdateDTO.setInfantAllocation(Integer.parseInt(strSegmentColData[6]));
			fCCSegInventoryUpdateDTO.setVersion(Long.parseLong(strSegmentColData[7]));

			if (strBcSaveData.length() > 0) {
				String[] strBcRowData = strBcSaveData.split("@");

				for (String element : strBcRowData) {

					String[] strBcColData = element.split("~");

					if (strBcColData.length == 1) {
						// bc inv deleted
						if (setDeletedInventories == null) {
							setDeletedInventories = new HashSet<Integer>();
							fCCSegInventoryUpdateDTO.setDeletedFCCSegBCInventories(setDeletedInventories);
						}
						setDeletedInventories.add(new Integer(strBcColData[0]));
					} else if (strBcColData[0].equals("A")) {// bc added
						// strBCInv=|bookingCode|priority|seatsAlloc|status|manuallyClosed|version
						if (setAddedInventories == null) {
							setAddedInventories = new HashSet<FCCSegBCInventory>();
							fCCSegInventoryUpdateDTO.setAddedFCCSegBCInventories(setAddedInventories);
						}
						FCCSegBCInventory bcInv = new FCCSegBCInventory();
						bcInv.setfccsInvId(Integer.parseInt(strSegmentColData[0]));
						bcInv.setFlightId(Integer.parseInt(strSegmentColData[1]));
						bcInv.setSegmentCode(strSegmentColData[2]);
						bcInv.setLogicalCCCode(strSegmentColData[3]);
						bcInv.setBookingCode(strBcColData[1]);
						bcInv.setPriorityFlag(strBcColData[2].equals("true") ? true : false);
						bcInv.setSeatsAllocated(Integer.parseInt(strBcColData[3]));
						bcInv.setStatus(strBcColData[4].equals("true") ? FCCSegBCInventory.Status.CLOSED
								: FCCSegBCInventory.Status.OPEN);

						bcInv.setStatusChangeAction(strBcColData[5]);
						bcInv.setSeatsAcquired(Integer.parseInt(strBcColData[6]));
						bcInv.setVersion(Long.parseLong(strBcColData[7]));

						setAddedInventories.add(bcInv);

					} else {// bc edited
						// strBCInv=fccsbInvId|bookingCode|priority|seatsAlloc|status|manuallyClosed|version
						if (setEditedInventories == null) {
							setEditedInventories = new HashSet<FCCSegBCInventory>();
							fCCSegInventoryUpdateDTO.setUpdatedFCCSegBCInventories(setEditedInventories);
						}
						FCCSegBCInventory bcInv = new FCCSegBCInventory();
						bcInv.setFccsbInvId(new Integer(strBcColData[0]));
						bcInv.setfccsInvId(Integer.parseInt(strSegmentColData[0]));
						bcInv.setFlightId(Integer.parseInt(strSegmentColData[1]));
						bcInv.setSegmentCode(strSegmentColData[2]);
						bcInv.setLogicalCCCode(strSegmentColData[3]);
						bcInv.setBookingCode(strBcColData[1]);
						bcInv.setPriorityFlag(strBcColData[2].equals("true") ? true : false);
						bcInv.setSeatsAllocated(Integer.parseInt(strBcColData[3]));
						bcInv.setStatus(strBcColData[4].equals("true") ? FCCSegBCInventory.Status.CLOSED
								: FCCSegBCInventory.Status.OPEN);

						bcInv.setStatusChangeAction(strBcColData[5]);
						bcInv.setSeatsAcquired(Integer.parseInt(strBcColData[6]));
						bcInv.setVersion(Long.parseLong(strBcColData[7]));

						setEditedInventories.add(bcInv);
					}
				}
			}

			updateStatus = ModuleServiceLocator.getFlightInventoryBD().updateFCCSegInventory(fCCSegInventoryUpdateDTO);
			if (updateStatus.getStatus() == OperationStatusDTO.OPERATION_SUCCESS) {
				// update flight status if necessary
				int bcInvCount = ModuleServiceLocator.getFlightInventoryBD().getFCCSegBCInventoriesCount(flightId);
				Collection<Integer> flightIds = new ArrayList<Integer>();
				flightIds.add(new Integer(flightId));
				if (bcInvCount > 0) {// check status update is really required
					ModuleServiceLocator.getFlightServiceBD().changeFlightStatus(flightIds, FlightStatusEnum.ACTIVE);
				} else {
					ModuleServiceLocator.getFlightServiceBD().changeFlightStatus(flightIds, FlightStatusEnum.CREATED);
				}
			}
		} else {
			updateStatus = new FCCSegInventoryUpdateStatusDTO(OperationStatusDTO.OPERATION_FAILURE, "No changes for update",
					null, FCCSegInventoryUpdateStatusDTO.UNIDENTIFIED_FAILURE, null, null);
		}
		return updateStatus;
	}

	/**
	 * Sets the Display Data for Inventory Allocation Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	private static void setDisplayData(HttpServletRequest request) throws Exception {

		String strFlightSummary = request.getParameter("hdnFlightSumm");
		String strFlightRollForwardAuditSummary = request.getParameter("hdnFlightRollForwardAuditSummary");
		String strFlightNo = request.getParameter("hdnFlightID");
		String strCabinClass = request.getParameter("hdnClassOfService");

		// seat factor
		String seatFactorMin = request.getParameter("hdnSeatFactorMin");
		String seatFactorMax = request.getParameter("hdnSeatFactorMax");

		String strAircraftModelNumber = request.getParameter("hdnModelNo");

		Collection<String[]> colTemplates = SelectListGenerator.createChargesTemplateIDs(strAircraftModelNumber);

		boolean blnSeatMap = false;
		boolean blnSeatMapTmlt = false;
		boolean blnShowMeal = false;
		boolean blnShowBaggage = false;

		request.setAttribute("reqFlightID", "var strflightId = '" + strFlightNo + "'");
		request.setAttribute("reqCabinClass", "var strCabinClass = '" + strCabinClass + "'");
		request.setAttribute("fromFromPage", "var hdnFromFromPage = '" + request.getParameter("hdnFromFromPage") + "'");

		String strSeatMap = ModuleServiceLocator.getGlobalConfig().getBizParam(SystemParamKeys.SHOW_SEAT_MAP);
		if (strSeatMap != null && strSeatMap.trim().equals("Y")) {
			blnSeatMap = true;

			if (colTemplates != null && colTemplates.size() > 0) {
				blnSeatMapTmlt = true;
			}
		}
		String strMeal = ModuleServiceLocator.getGlobalConfig().getBizParam(SystemParamKeys.SHOW_MEAL);
		if (strMeal != null && strMeal.trim().equals("Y")) {
			blnShowMeal = true;
		}

		String strBaggage = ModuleServiceLocator.getGlobalConfig().getBizParam(SystemParamKeys.SHOW_BAGGAGE);

		if (strBaggage != null && strBaggage.trim().equals("Y")) {
			// enabled in application parameter
			log.debug("show baggage enabled in app-param");
			Collection<String[]> colBaggageTemplates = SelectListGenerator
					.createBaggageChargesTemplateIDs(strAircraftModelNumber);
			if (colBaggageTemplates != null && colBaggageTemplates.size() > 0) {
				// active baggage templates available
				log.debug("baggage template available-showbaggage");
				blnShowBaggage = true;
			}
		}

		request.setAttribute("reqSeatMap", " blnSeatMap = " + blnSeatMap + ";");
		request.setAttribute("reqSeatMapTmlt", " blnSeatMapTmlt = " + blnSeatMapTmlt + ";");
		request.setAttribute("reqShowMeal", " blnShowMeal = " + blnShowMeal + ";");
		request.setAttribute("reqShowBaggage", " blnShowBaggage = " + blnShowBaggage + ";");

		if (seatFactorMin == null || seatFactorMin.equals("")) {
			seatFactorMin = "0";
		}
		if (seatFactorMax == null || seatFactorMax.equals("")) {
			seatFactorMax = "100";
		}
		request.setAttribute("reqSeatFactorMin", " seatFactorMin = " + seatFactorMin + ";");
		request.setAttribute("reqseatFactorMax", " seatFactorMax = " + seatFactorMax + ";");

		if (strFlightSummary != null && !strFlightSummary.equals("")) {
			if (strFlightSummary.trim().length() != (strFlightSummary.trim().lastIndexOf("@") + 1)) {
				strFlightSummary = strFlightSummary + "@";
			}
			request.setAttribute(WebConstants.REQ_FORM_FLIGHT_SUMM,
					"var varFlightSummary = '" + strFlightSummary.substring(0, (strFlightSummary.length() - 1)) + "'");
		}

		if (strFlightRollForwardAuditSummary != null && !strFlightRollForwardAuditSummary.equals("")) {
			if (strFlightRollForwardAuditSummary.trim().length() != (strFlightRollForwardAuditSummary.trim().lastIndexOf("@") + 1)) {
				strFlightRollForwardAuditSummary = strFlightRollForwardAuditSummary + "@";
			}
			request.setAttribute(WebConstants.REQ_FORM_FLIGHT_ROLL_FWD_AUDIT_SUMM, "varFlightRollForwardAuditSummary = '"
					+ strFlightRollForwardAuditSummary.substring(0, (strFlightRollForwardAuditSummary.length() - 1)) + "'");
		}

		setCabinClassList(request);
		setBookingCodesList(request);
		setDefaultCabinClassCapacities(request);
		setSegmentAllocationRowHtml(request);
		checkInterlineAirLineAvailability(request);
		setClientErrors(request);
	}

	/**
	 * Sets the Cabin Class List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setCabinClassList(HttpServletRequest request) {
		try {
			String strHtml = SelectListGenerator.createAllCabinClass();
			request.setAttribute(WebConstants.REQ_HTML_CABINCLASS_LIST, strHtml);
		} catch (ModuleException moduleException) {
			log.error("MIAllocationRequestHandler.setCabinClassList() method is failed :" + moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * If diplay TRUE operating carrier would see the summary of marketting carrier seat allocation of certain operating
	 * carrier flight.
	 */
	private static void checkInterlineAirLineAvailability(HttpServletRequest request) throws ModuleException {
		GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();
		/* Retrieve all the interline airlines */
		Collection<String> interLines = globalConfig.getActiveInterlinedCarriersMap().keySet();

		int flightId = 0;
		/* Get the system carrier codes */
		Collection<String> systemCarriers = globalConfig.getActiveSysCarriersMap().keySet();
		boolean display = false;

		if (request.getParameter("hdnFlightID") != null) {
			flightId = Integer.parseInt(request.getParameter("hdnFlightID"));
		}
		Flight flight = ModuleServiceLocator.getFlightServiceBD().getFlight(flightId);

		if (flight != null && !systemCarriers.contains(AppSysParamsUtil.extractCarrierCode(flight.getFlightNumber()))
				&& interLines.contains(AppSysParamsUtil.extractCarrierCode(flight.getFlightNumber()))) {
			display = true;
		}

		request.setAttribute("interLineDisplay", display);
	}

	/**
	 * Sets the Client Validations to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = MIAllocationHTMLGenerator.getAllocationClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("MIAllocationRequestHandler.setClientErrors() method is failed :" + moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Inventory Allocation Row to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	private static void setSegmentAllocationRowHtml(HttpServletRequest request) throws Exception {

		try {
			MIAllocationHTMLGenerator htmlGen = new MIAllocationHTMLGenerator();
			String strHtml = htmlGen.getSegmentAllocationRowHtml(request);
			request.setAttribute(WebConstants.REQ_HTML_ROWS, strHtml);
		} catch (ModuleException moduleException) {
			log.error("MIAllocationRequestHandler.setSegmentAllocationRowHtml() method is failed :", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		} catch (Exception exception) {
			log.error("MIAllocationRequestHandler.setSegmentAllocationRowHtml() method is failed :", exception);
			if (exception instanceof RuntimeException) {
				throw exception;
			}
			saveMessage(request, exception.getMessage(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Prepare bookingCodes list
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setBookingCodesList(HttpServletRequest request) throws ModuleException {
		StringBuffer bcSelectList = new StringBuffer();
		Collection<Object[]> bookingCodes = ModuleServiceLocator.getBookingClassBD().getBookingCodesWithFlags(
				request.getParameter("hdnClassOfService"));
		if (bookingCodes != null) {
			Iterator<Object[]> bookingCodesIt = bookingCodes.iterator();
			while (bookingCodesIt.hasNext()) {
				Object[] bcWithFlags = bookingCodesIt.next();
				// if (bcWithFlags[5] != null) {
				// // GDS
				// continue;
				// }
				String strStdFlag = (String) bcWithFlags[1];
				String strFixedFlag = (String) bcWithFlags[2];
				String strBCType = BookingClassUtil.getBcTypeThreeLetterCode(((String) bcWithFlags[3]));
				String strAllocType = (String) bcWithFlags[4];
				String suffix = " ";
				if (strStdFlag.equals("Y")) {
					suffix += "[STD,";
				} else if (strFixedFlag.equals("Y")) {
					suffix += "[FXD,";
				} else {
					suffix += "[NSD,";
				}
				if (strAllocType.equals(BookingClass.AllocationType.SEGMENT)) {
					suffix += "SEG,";
				} else if (strAllocType.equals(BookingClass.AllocationType.CONNECTION)) {
					suffix += "CON,";
				} else if (strAllocType.equals(BookingClass.AllocationType.RETURN)) {
					suffix += "RET,";
				} else {
					suffix += "COM,";
				}
				suffix += strBCType + "]";
				bcSelectList.append("<option value='" + bcWithFlags[0] + "," + bcWithFlags[1] + "," + bcWithFlags[2] + "'>"
						+ bcWithFlags[0] + suffix + "</option>");
			}
		}
		request.setAttribute(WebConstants.REQ_HTML_BOOKINGCODES_LIST, bcSelectList.toString());
	}

	/**
	 * Sets the Default Cabin Classes to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setDefaultCabinClassCapacities(HttpServletRequest request) throws ModuleException {
		AircraftCabinCapacity ccCapacity = null;
		String strAircraftModelNumber = request.getParameter("hdnModelNo");
		String strSelectedCabinClassCode = request.getParameter("hdnClassOfService");
		if (strSelectedCabinClassCode.trim().equals("")) {
			strSelectedCabinClassCode = AppSysParamsUtil.getDefaultCOS();
		}
		if (!strAircraftModelNumber.trim().equals("") && strAircraftModelNumber != null) {
			AircraftModel aircraftModel = ModuleServiceLocator.getAircraftServiceBD().getAircraftModel(strAircraftModelNumber);
			if (aircraftModel != null) {
				Set<AircraftCabinCapacity> ccCapacities = aircraftModel.getAircraftCabinCapacitys();
				if (ccCapacities != null) {
					Iterator<AircraftCabinCapacity> ccCapacitiesIt = ccCapacities.iterator();
					while (ccCapacitiesIt.hasNext()) {
						ccCapacity = ccCapacitiesIt.next();
						if (ccCapacity.getCabinClassCode().equals(strSelectedCabinClassCode)) {
							ccCapacity.getSeatCapacity();// default adult capacity
							ccCapacity.getInfantCapacity();// default infant capacity

							break;
						}
					}
				}
			}
			request.setAttribute(WebConstants.REQ_FORM_FLIGHT_DEFAULT_CAPACITY,
					"var varFlightDefCap = '" + ccCapacity.getSeatCapacity() + "/" + ccCapacity.getInfantCapacity() + "'");
		}
	}

}