package com.isa.thinair.airadmin.core.web.v2.dto;

import java.io.Serializable;

public class CityDTO implements Serializable{

	private static final long serialVersionUID = -8574698632547145864L;
	                                              
	private Integer cityId;

	private String cityCode;

	private String cityName;

	private String status;

	private String countryCode;
	
	private String lccPublishStatus;

	public Integer getCityId() {
		return cityId;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getLccPublishStatus() {
		return lccPublishStatus;
	}

	public void setLccPublishStatus(String lccPublishStatus) {
		this.lccPublishStatus = lccPublishStatus;
	}
}
