package com.isa.thinair.airadmin.core.web.handler.master;

import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.master.TerritoryHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airmaster.api.model.Territory;
import com.isa.thinair.commons.api.exception.MessagesUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author srikantha
 * 
 */

public final class TerritoryRequestHandler extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(TerritoryRequestHandler.class);

	private static AiradminConfig airadminConfig = new AiradminConfig();

	private static final String PARAM_VERSION = "hdnVersion";
	private static final String PARAM_TERRITORYCODE = "txtTerritoryId";
	private static final String PARAM_DESCRIPTION = "txtDesc";
	private static final String PARAM_REMARKS = "txtRemarks";
	private static final String PARAM_MODE = "hdnMode";
	private static final String PARAM_STATUS = "chkStatus";

	// Bank details
	private static final String PARAM_BANK = "txtBank";
	private static final String PARAM_BANK_ADDRESS = "txtBankAddress";
	private static final String PARAM_ACCOUNT_NUMBER = "txtAccountNumber";
	private static final String PARAM_SWIFT_CODE = "txtSwiftCode";
	private static final String PARAM_IBAN_NO = "txtIBANNo";
	private static final String PARAM_BENEFICIARY_NAME = "txtBeneficiaryName";

	private static final String PARAM_SEARCHDATA = "hdnSearchData";

	private static final String PARAM_SEARCH_TERRITORYCODE = "selTerritoryID";
	private static final String PARAM_SEARCH_TERRITORYDESC = "selTerritoryDesc";

	private static boolean isSuccessfulSaveMessageDisplayEnabled() {
		boolean isMessageEnabled = false;
		if (airadminConfig.getMessage("um.airadmin.savemessage.display").equals("true")) {
			isMessageEnabled = true;
		}
		return isMessageEnabled;
	}

	private static void setExceptionOccured(HttpServletRequest request, boolean value) {
		setAttribInRequest(request, "isExceptionOccured", new Boolean(value));
	}

	private static boolean isExceptionOccured(HttpServletRequest request) {
		return ((Boolean) getAttribInRequest(request, "isExceptionOccured")).booleanValue();
	}

	/**
	 * Main Execute Method for Territory Actions
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {

		String strHdnMode = request.getParameter(PARAM_MODE);
		String forward = WebConstants.FORWARD_SUCCESS;

		setExceptionOccured(request, false);
		setAttribInRequest(request, "strModeJS", "var isAddMode = false;");
		setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = false;");
		if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_GRID)) {
			return WebConstants.ACTION_GRID;
		}
		try {

			if (strHdnMode != null) {
				if (strHdnMode.equals(WebConstants.ACTION_ADD)) {
					checkPrivilege(request, WebConstants.PRIV_SYS_MAS_TERRITORY_ADD);
					setAttribInRequest(request, "strModeJS", "var isAddMode = true;");
					saveData(request);
				}
				if (strHdnMode.equals(WebConstants.ACTION_EDIT)) {
					checkPrivilege(request, WebConstants.PRIV_SYS_MAS_TERRITORY_EDIT);
					setAttribInRequest(request, "strModeJS", "var isAddMode = false;");
					saveData(request);
				}
				if (strHdnMode.equals(WebConstants.ACTION_DELETE)) {
					checkPrivilege(request, WebConstants.PRIV_SYS_MAS_TERRITORY_DELETE);
					deleteData(request);
				}
			}
			setDisplayData(request);
		} catch (Exception exception) {
			if (exception instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
			}
		}
		return forward;
	}

	/**
	 * Delete Terrritory
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	private static void deleteData(HttpServletRequest request) throws Exception {

		log.debug("Entered inside the deleteData()...");
		String strTerritoryId = request.getParameter("txtTerritoryId");

		try {
			if (log.isDebugEnabled()) {
				log.debug("deleteData()..." + strTerritoryId);
			}
			if (strTerritoryId != null) {

				Territory existingTerritory = ModuleServiceLocator.getLocationServiceBD().getTerritory(strTerritoryId);
				if (existingTerritory == null)
					ModuleServiceLocator.getLocationServiceBD().deleteTerritory(strTerritoryId);
				else {
					String strVersion = request.getParameter(PARAM_VERSION);
					if (strVersion != null && !"".equals(strVersion)) {
						existingTerritory.setVersion(Long.parseLong(strVersion));
						ModuleServiceLocator.getLocationServiceBD().deleteTerritory(existingTerritory);
					} else {
						ModuleServiceLocator.getLocationServiceBD().deleteTerritory(strTerritoryId);
					}
				}
				setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = false;");
				saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_DELETE_SUCCESS), WebConstants.MSG_SUCCESS);
			}
		} catch (ModuleException moduleException) {

			log.error("TerritoryRequestHandler.deleteData() method is failed :" + moduleException.getMessageString());
			setAttribInRequest(request, "strModeJS", "var isAddMode = false;");
			if (moduleException.getMessageString().equals(MessagesUtil.getMessage("module.constraint.childrecord"))) {
				saveMessage(request, airadminConfig.getMessage("um.airadmin.childrecord"), WebConstants.MSG_ERROR);
			} else {
				saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
			}
		} catch (Exception exception) {
			setAttribInRequest(request, "strModeJS", "var isAddMode = false;");
			if (exception instanceof RuntimeException) {
				throw exception;
			}
		}
	}

	/**
	 * Saves the Territory
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	private static void saveData(HttpServletRequest request) throws Exception {

		log.debug("Entered inside the saveData()...");
		Territory territory = new Territory();

		String strVersion = null;
		String strTerritoryCode = null;
		String strRemarks = null;
		String strDescription = null;
		String strStatus = null;
		String bank = null;
		String bankAddress = null;
		String accountNumber = null;
		String swiftCode = null;
		String IBANNo = null;
		String beneficiaryName = null;

		try {
			strVersion = AiradminUtils.getNotNullString(request.getParameter(PARAM_VERSION));
			strTerritoryCode = AiradminUtils.getNotNullString(request.getParameter(PARAM_TERRITORYCODE));
			if (strTerritoryCode != null && strTerritoryCode.length() > 0) {
				strTerritoryCode = AppSysParamsUtil.getRequiredDefaultAirlineIdentifierCode() + strTerritoryCode;
			}
			strRemarks = AiradminUtils.getNotNullString(request.getParameter(PARAM_REMARKS));
			strDescription = AiradminUtils.getNotNullString(request.getParameter(PARAM_DESCRIPTION));
			strStatus = AiradminUtils.getNotNullString(request.getParameter(PARAM_STATUS));

			// bank details
			bank = AiradminUtils.getNotNullString(request.getParameter(PARAM_BANK));
			bankAddress = AiradminUtils.getNotNullString(request.getParameter(PARAM_BANK_ADDRESS));
			accountNumber = AiradminUtils.getNotNullString(request.getParameter(PARAM_ACCOUNT_NUMBER));
			swiftCode = AiradminUtils.getNotNullString(request.getParameter(PARAM_SWIFT_CODE));
			IBANNo = AiradminUtils.getNotNullString(request.getParameter(PARAM_IBAN_NO));
			beneficiaryName = AiradminUtils.getNotNullString(request.getParameter(PARAM_BENEFICIARY_NAME));

			if (strVersion != null && !"".equals(strVersion)) {
				territory.setVersion(Long.parseLong(strVersion));
			}

			territory.setTerritoryCode(strTerritoryCode.toUpperCase());
			territory.setDescription(strDescription.trim());
			territory.setRemarks(strRemarks.trim());

			if (strStatus.equals("on"))
				territory.setStatus(Territory.STATUS_ACTIVE);
			else
				territory.setStatus(Territory.STATUS_INACTIVE);

			// set bank details
			territory.setBank(bank);
			territory.setBankAddress(bankAddress);
			territory.setAccountNumber(accountNumber);
			territory.setSwiftCode(swiftCode);
			territory.setIBANCode(IBANNo);
			territory.setBeneficiaryName(beneficiaryName);
			territory.setAirlineCode(AppSysParamsUtil.getDefaultAirlineIdentifierCode());

			String strHdnMode = request.getParameter(PARAM_MODE);

			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_ADD)) {
				territory.setLccPublishStatus(Util.LCC_TOBE_PUBLISHED);
			} else if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_EDIT)) {
				territory.setLccPublishStatus(Util.LCC_TOBE_REPUBLISHED);
			}

			ModuleServiceLocator.getLocationServiceBD().saveTerritory(territory);
			setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = true;");
			territory = null;

			saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS), WebConstants.MSG_SUCCESS);
			log.debug("TerritoryRequestHandler.saveData() method is successfully executed.");

		} catch (ModuleException moduleException) {
			log.error("Exception in TerritoryRequestHandler.saveData() [origin module=" + moduleException.getModuleDesc() + "]",
					moduleException);
			setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = false;");
			setExceptionOccured(request, true);

			String strFormData = "var arrFormData = new Array();";

			strFormData += "arrFormData[0] = new Array();";
			strFormData += "arrFormData[0][1] = '" + strTerritoryCode + "';";
			strFormData += "arrFormData[0][2] = '" + strDescription + "';";
			strFormData += "arrFormData[0][3] = '" + strRemarks + "';";
			strFormData += "arrFormData[0][4] = '" + strStatus + "';";
			strFormData += "arrFormData[0][5] = '" + strVersion + "';";

			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);

			if (moduleException.getExceptionCode().equals("module.duplicate.key")) {
				saveMessage(request, airadminConfig.getMessage("um.territory.form.id.already.exist"), WebConstants.MSG_ERROR);
			} else {
				saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
			}

		} catch (Exception exception) {
			log.error("Exception in TerritoryRequestHandler.saveData()", exception);
			if (exception instanceof RuntimeException) {
				throw exception;
			}
		}
	}

	/**
	 * Set Display Data for the Territory page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setDisplayData(HttpServletRequest request) {

		setTerritoryList(request);
		setTerritoryRowHtml(request);
		setClientErrors(request);

		if (!isExceptionOccured(request)) {
			String strFormData = "var arrFormData = new Array();";
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
		}

		request.setAttribute(WebConstants.REQ_OPERATION_MODE, getAttribInRequest(request, "strModeJS"));

		if (request.getParameter(PARAM_MODE) == null) {

			setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = false;");

			request.setAttribute(WebConstants.REQ_TRANSACTION_STATUS,
					getAttribInRequest(request, "isSaveTransactionSuccessfulJS"));
		} else if (request.getParameter(PARAM_MODE) != null && !request.getParameter(PARAM_MODE).equals(WebConstants.ACTION_ADD)
				&& !request.getParameter(PARAM_MODE).equals(WebConstants.ACTION_EDIT)) {

			setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = false;");

			request.setAttribute(WebConstants.REQ_TRANSACTION_STATUS,
					getAttribInRequest(request, "isSaveTransactionSuccessfulJS"));
		} else {

			request.setAttribute(WebConstants.REQ_TRANSACTION_STATUS,
					getAttribInRequest(request, "isSaveTransactionSuccessfulJS"));
		}

		request.setAttribute(WebConstants.REQ_TRANSACTION_DISPLAYSTATUS, "var isSuccessfulSaveMessageDisplayEnabled = "
				+ isSuccessfulSaveMessageDisplayEnabled() + ";");
		request.setAttribute(WebConstants.REQ_DEFAULT_AIRLINE_CODE, AppSysParamsUtil.getRequiredDefaultAirlineIdentifierCode());
		UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
		if (userPrincipal != null) {
			request.setAttribute(WebConstants.REQ_IS_REMOTE_USER, "false"); // FIXME remove this field in the F/E
		}
	}

	/**
	 * Sets the Client validations For Territory Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = TerritoryHTMLGenerator.getClientErrors(request);
			if (strClientErrors == null)
				strClientErrors = "";

			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);

		} catch (ModuleException moduleException) {
			log.error("Exception in TerritoryRequestHandler.setClientErrors() [origin module=" + moduleException.getModuleDesc()
					+ "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Territory ID list to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setTerritoryList(HttpServletRequest request) {
		try {
			String strHtml = SelectListGenerator.createTerritoryListWOTag();
			request.setAttribute(WebConstants.REQ_HTML_TERRITORY_LIST, strHtml);
		} catch (ModuleException moduleException) {
			log.error("Exception in TerritoryRequestHandler:setTerritoryList() [origin module=" + moduleException.getModuleDesc()
					+ "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Territory Data Array To the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	protected static void setTerritoryRowHtml(HttpServletRequest request) {

		try {
			TerritoryHTMLGenerator htmlGen = new TerritoryHTMLGenerator(getProperty(request));
			String strHtml = htmlGen.getTerritoryRowHtml(request);
			request.setAttribute(WebConstants.REQ_HTML_ROWS, strHtml);

		} catch (ModuleException moduleException) {
			log.error(
					"Exception in TerritoryRequestHandler.setTerritoryRowHtml() [origin module="
							+ moduleException.getModuleDesc() + "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Creats a Propert file with Country Data from the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return Properties the property File with Country data
	 */
	private static Properties getProperty(HttpServletRequest request) {

		Properties props = new Properties();

		String strSearchData = request.getParameter(PARAM_SEARCHDATA);

		String searchDataArr[] = null;

		if (strSearchData != null && !strSearchData.equals("")) {
			searchDataArr = strSearchData.split(",");
		}

		props.setProperty(PARAM_SEARCHDATA, AiradminUtils.getNotNullString(strSearchData));
		props.setProperty(PARAM_VERSION, AiradminUtils.getNotNullString(request.getParameter(PARAM_VERSION)));
		props.setProperty(PARAM_TERRITORYCODE, AiradminUtils.getNotNullString(request.getParameter(PARAM_TERRITORYCODE)));
		props.setProperty(PARAM_DESCRIPTION, AiradminUtils.getNotNullString(request.getParameter(PARAM_DESCRIPTION)));
		props.setProperty(PARAM_SEARCH_TERRITORYCODE, (searchDataArr != null ? searchDataArr[0] : ""));
		props.setProperty(PARAM_SEARCH_TERRITORYDESC, (searchDataArr != null ? searchDataArr[1] : ""));
		return props;
	}
}
