package com.isa.thinair.airadmin.core.web.handler.reports;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airtravelagents.api.model.Agent;
import com.isa.thinair.airtravelagents.api.util.AirTravelAgentConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

public class AgentGSADetailReportRH extends BasicRequestHandler {
	private static Log log = LogFactory.getLog(ScheduleDetailReportRH.class);

	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	/**
	 * The constructor.
	 */
	public AgentGSADetailReportRH() {
		super();
	}

	/**
	 * @param request
	 * @return
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {
		String strHdnMode = request.getParameter("hdnMode");

		String forward = AdminStrutsConstants.AdminAction.SUCCESS;
		setAttribInRequest(request, "displayAgencyMode", "-1");
		setAttribInRequest(request, "currentAgentCode", "");

		try {
			setDisplayData(request);
			log.debug("AgentGSADetailReportRH success");
		} catch (Exception e) {
			forward = AdminStrutsConstants.AdminAction.ERROR;
			log.error("AgentGSADetailReportRH execute()" + e.getMessage());
		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				log.error("AgentGSADetailReportRH setReportView Success");
				return null;
			} else {
				log.error("AgentGSADetailReportRH setReportView not selected");
			}

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("AgentGSADetailReportRH setReportView Failed " + e.getMessageString());
		}

		return forward;
	}

	/**
	 * 
	 * @param request
	 * @throws Exception
	 */
	public static void setDisplayData(HttpServletRequest request) throws Exception {
		setDisplayAgencyMode(request);
		setAgentTypes(request);
		setGSAMultiSelectList(request);
		setClientErrors(request);
		setCountryList(request);
		setStationComboList(request);
		setTerritoryList(request);
		setLiveStatus(request);
		setReportingPeriod(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);

	}

	protected static void setDisplayAgencyMode(HttpServletRequest request) {
		Map mapPrivileges = (Map) request.getSession().getAttribute(WebConstants.SES_PRIVILEGE_IDS);
		if (mapPrivileges.get("rpt.ta.pta.all") != null) {
			// Show both the controls
			setAttribInRequest(request, "displayAgencyMode", "1");
		} else if (mapPrivileges.get("rpt.ta.pta.gsa") != null) {
			setAttribInRequest(request, "displayAgencyMode", "2");
		} else if (mapPrivileges.get("rpt.ta.pta") != null) {
			setAttribInRequest(request, "displayAgencyMode", "0");
		} else {
			setAttribInRequest(request, "displayAgencyMode", "-1");
		}
	}

	protected static void setGSAMultiSelectList(HttpServletRequest request) throws ModuleException {
		boolean blnWithTAs = false;
		boolean blnWithCOs = false;
		String strAgentType = "";
		String strList = "";

		if (request.getParameter("selAgencies") != null) {
			strAgentType = request.getParameter("selAgencies");
		}
		if (request.getParameter("chkTAs") != null) {
			blnWithTAs = (request.getParameter("chkTAs").equals("on") ? true : false);
		} else if (getAttribInRequest(request, "displayAgencyMode").equals("2")) {
			blnWithTAs = true;
		}
		if (request.getParameter("chkCOs") != null) {
			blnWithCOs = (request.getParameter("chkCOs").equals("on") ? true : false);
		}

		if (getAttribInRequest(request, "displayAgencyMode").equals("-1")) {
			// No privilege to access the reports
			throw new ModuleRuntimeException(WebConstants.ERR_UNAUTHORIZED_ACCESS);
		} else if (getAttribInRequest(request, "displayAgencyMode").equals("0")) {
			// No Agent Population Controls or Agents list box are displayed
			String userId = request.getUserPrincipal().getName();
			if (userId != null && !userId.equals("")) {
				Agent currentAgent = ModuleServiceLocator.getTravelAgentBD().getAgent(
						ModuleServiceLocator.getSecurityBD().getUserBasicDetails(userId).getAgentCode());
				setAttribInRequest(request, "currentAgentCode", currentAgent.getAgentCode());
			} else {
				// No privilege to access the reports
				throw new ModuleRuntimeException(WebConstants.ERR_UNAUTHORIZED_ACCESS);
			}
		} else if (getAttribInRequest(request, "displayAgencyMode").equals("1")) {
			// Both Agent Population Controls and Agents list box are displayed
			strList = ReportsHTMLGenerator.createAgentGSAMultiSelect(strAgentType, blnWithTAs, blnWithCOs);
		} else if (getAttribInRequest(request, "displayAgencyMode").equals("2")) {
			// Only Agents list box is displayed
			String userId = request.getUserPrincipal().getName();
			if (userId != null && !userId.equals("")) {
				Agent currentAgent = ModuleServiceLocator.getTravelAgentBD().getAgent(
						ModuleServiceLocator.getSecurityBD().getUserBasicDetails(userId).getAgentCode());
				setAttribInRequest(request, "currentAgentCode", currentAgent.getAgentCode());
				if (currentAgent.getAgentTypeCode().equals(AirTravelAgentConstants.AgentTypes.GSA)) {
					// Agent is a GSA
					strList = ReportsHTMLGenerator.createTAsOfGSAMultiSelect(currentAgent.getAgentCode(), true);
				} else {
					// Agent is not a GSA
					// Only select the current agent
					setAttribInRequest(request, "displayAgencyMode", "0");
				}
			} else {
				// No privilege to access the reports
				throw new ModuleRuntimeException(WebConstants.ERR_UNAUTHORIZED_ACCESS);
			}
		}

		request.setAttribute(WebConstants.REQ_HTML_DETAILS,
				"displayAgencyMode = " + getAttribInRequest(request, "displayAgencyMode") + ";");

		request.setAttribute(WebConstants.REQ_HTML_GSA_LIST, strList);
	}

	protected static void setAgentTypes(HttpServletRequest request) throws ModuleException {
		String strATList = SelectListGenerator.createAgentTypeList_SG();
		request.setAttribute(WebConstants.REQ_HTML_AGENTTYPE_LIST, strATList);
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setStationComboList(HttpServletRequest request) throws ModuleException {
		String strStation = SelectListGenerator.createStationList();
		request.setAttribute(WebConstants.REQ_STATION_LIST, strStation);
	}

	public static void setCountryList(HttpServletRequest request) throws ModuleException {
		String country = SelectListGenerator.createCountryList();
		request.setAttribute(WebConstants.REQ_HTML_COUNTRY_LIST, country);
	}

	public static void setAgentTypeList(HttpServletRequest request) throws ModuleException {
		String types = SelectListGenerator.createAgentTypeCodesList();
		request.setAttribute(WebConstants.REQ_AGENT_TYPE, types);
	}

	public static void setTerritoryList(HttpServletRequest request) throws ModuleException {
		String types = SelectListGenerator.createTerritoryList();
		request.setAttribute(WebConstants.REQ_TERRITORY, types);
	}

	/**
	 * Sets the report medium to the request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleExceptions
	 */
	private static void setLiveStatus(HttpServletRequest request) throws ModuleException {
		String strLive = request.getParameter(WebConstants.REP_HDN_LIVE);
		if (strLive != null)
			request.setAttribute(WebConstants.REP_SET_LIVE, strLive);

	}

	/**
	 * sets the Status list
	 * 
	 * @param request
	 */

	public static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Setting the reporting period to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setReportingPeriod(HttpServletRequest request) throws ModuleException {
		int period = new Integer(globalConfig.getBizParam(SystemParamKeys.REPORTING_PERIOD)).intValue();
		request.setAttribute(WebConstants.REP_RPT_START_DAY, ReportsHTMLGenerator.getReportingPeriod(period));
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		ResultSet resultSet = null;
		String agents = "";
		String station = request.getParameter("selStation");
		String territory = request.getParameter("selTerritory");
		String country = request.getParameter("selCountry");
		String reportType = request.getParameter("hdnRptType");
		String value = request.getParameter("radReportOption");
		String agentName = request.getParameter("hdnAgentName");
		String strStatus = request.getParameter("selStatus");
		String strcreatedDate = request.getParameter("txtCreatedDate");
		String strcreatedDateTo = request.getParameter("txtCreatedDateTo");
		String strAgentType = request.getParameter("selAgencies");
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);

		if (getAttribInRequest(request, "displayAgencyMode").equals("0")) {
			agents = (String) getAttribInRequest(request, "currentAgentCode");
		} else {
			agents = request.getParameter("hdnAgents");
		}

		String id = "UC_REPM_009";
		String reportTemplateSummary = "AgentGSASummaryReport.jasper";
		String reportTemplateDetails = "AgentGSADetailsReport.jasper";
		Map<String, Object> parameters = new HashMap<String, Object>();

		try {
			ReportsSearchCriteria search = new ReportsSearchCriteria();

			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}

			if (reportType.equals("Summary")) {
				ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
						ReportsHTMLGenerator.getReportTemplate(reportTemplateSummary));
				search.setReportType(ReportsSearchCriteria.AGENT_GSA_SUMMARY);
				ArrayList<String> agentCol = new ArrayList<String>();
				String agentArr[] = agents.split(",");
				for (int r = 0; r < agentArr.length; r++) {
					agentCol.add(agentArr[r]);
				}

				search.setCountryOfResidence(country);
				search.setAgents(agentCol);
				search.setTerritory(territory);
				search.setStation(station);
				if (strAgentType != null && !strAgentType.equals("") && !strAgentType.equals("All")) {
					search.setAgentType(strAgentType);
				}
				if (strcreatedDate != null && !strcreatedDate.trim().equals("")) {
					search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(strcreatedDate));
				}
				if (strStatus != null) {
					search.setStatus(strStatus);
				}
				if (strcreatedDateTo != null && !strcreatedDateTo.trim().equals("")) {
					search.setDateRangeTo(ReportsHTMLGenerator.convertDate(strcreatedDateTo));
				}

			} else {
				ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
						ReportsHTMLGenerator.getReportTemplate(reportTemplateDetails));
				search.setReportType(ReportsSearchCriteria.AGENT_GSA_DETAILS);
				search.setAgentCode(agents);
				String strCredit = globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY);
				parameters.put("AMT_1", "(" + strCredit + ")");
			}

			resultSet = ModuleServiceLocator.getDataExtractionBD().getAgentGSAData(search);
			String strLogo = AppSysParamsUtil.getReportLogo(false); // globalConfig.getBizParam(SystemParamKeys.REPORT_LOGO);
			String reportsRootDir = "../../images/" + AppSysParamsUtil.getReportLogo(true); // AA145.jpg
			String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";

			parameters.put("ID", id);
			parameters.put("OPTION", value);
			parameters.put("AGENT_NAME", agentName);
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
			parameters.put("REPORT_MEDIUM", strlive);

			// To provide Report Format Options
			String reportNumFormat = request.getParameter("radRptNumFormat");
			ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

			if (value.trim().equals("HTML")) {
				parameters.put("IMG", reportsRootDir);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null, null,
						response);
			} else if (value.trim().equals("PDF")) {
				reportsRootDir = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG", reportsRootDir);
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=AgentGSAReport.pdf");
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("EXCEL")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=AgentGSAReport.xls");
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("CSV")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=AgentGSASummaryReport.csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);
			}
		} catch (Exception e) {
			log.error(e);
		}
	}
}
