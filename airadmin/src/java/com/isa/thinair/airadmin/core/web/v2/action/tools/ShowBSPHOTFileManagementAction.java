/**
 * 
 */
package com.isa.thinair.airadmin.core.web.v2.action.tools;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.invoicing.api.model.BSPHOTFileLog;
import com.isa.thinair.platform.api.constants.PlatformConstants;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

/**
 * @author Janaka Padukka
 * 
 */
@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ShowBSPHOTFileManagementAction extends BaseRequestAwareAction {

	private static final String PARAM_MODE = "hdnMode";

	private String fileName;
	private String status;
	private String uploadedDateFrom;
	private String uploadedDateTo;
	private String processedDateFrom;
	private String processedDateTo;
	private String dpcProcessedDateFrom;
	private String dpcProcessedDateTo;
	private String uploadedBy;
	private String processedBy;

	private File fileHOTFile;

	private Collection<Map<String, Object>> rows;
	private int page;
	private int total;
	private int records;

	private String hdnMode;

	private String hdnHOTFileLogId;
	private String hdnHOTFileName;
	
	private String succesMsg;
	private String msgType;

	private static Log log = LogFactory.getLog(ShowBSPHOTFileManagementAction.class);
	
	private static AiradminConfig airadminConfig = new AiradminConfig();

	public String searchHOTFiles() {
		int pageSize = 20;
		if (page == 0) {
			page = 1;
		}
		int start = (page - 1) * pageSize;
		List<ModuleCriterion> critrian = new ArrayList<ModuleCriterion>();
		Map<String, Object> row;

		if (fileName != null && !"".equals(fileName)) {
			ModuleCriterion moduleCriterionFileName = new ModuleCriterion();
			moduleCriterionFileName.setCondition(ModuleCriterion.CONDITION_LIKE_IGNORECASE);
			moduleCriterionFileName.setFieldName("fileName");
			List<String> valueFileName = new ArrayList<String>();
			valueFileName.add(fileName + "%");
			moduleCriterionFileName.setValue(valueFileName);
			critrian.add(moduleCriterionFileName);
		}

		if (status != null && !"".equals(status)) {
			ModuleCriterion moduleCriterionStatus = new ModuleCriterion();
			moduleCriterionStatus.setCondition(ModuleCriterion.CONDITION_EQUALS);
			moduleCriterionStatus.setFieldName("fileStatus");
			List<String> valueStatus = new ArrayList<String>();
			valueStatus.add(status);
			moduleCriterionStatus.setValue(valueStatus);
			critrian.add(moduleCriterionStatus);
		}

		if (uploadedDateFrom != null && !"".equals(uploadedDateFrom)) {
			ModuleCriterion moduleCriterionUploadStart = new ModuleCriterion();
			moduleCriterionUploadStart.setCondition(ModuleCriterion.CONDITION_GREATER_THAN_OR_EQUALS);
			moduleCriterionUploadStart.setFieldName("uploadedOn");
			List<Date> valueUploadStartDate = new ArrayList<Date>();
			valueUploadStartDate.add(AiradminUtils.getFormattedDate(uploadedDateFrom));
			moduleCriterionUploadStart.setValue(valueUploadStartDate);
			critrian.add(moduleCriterionUploadStart);

		}

		if (uploadedDateTo != null && !"".equals(uploadedDateTo)) {
			ModuleCriterion moduleCriterionUploadEnd = new ModuleCriterion();
			moduleCriterionUploadEnd.setCondition(ModuleCriterion.CONDITION_LESS_THAN_OR_EQUALS);
			moduleCriterionUploadEnd.setFieldName("uploadedOn");
			List<Date> valueUploadEndDate = new ArrayList<Date>();
			valueUploadEndDate.add(AiradminUtils.getFormattedDate(uploadedDateTo));
			moduleCriterionUploadEnd.setValue(valueUploadEndDate);
			critrian.add(moduleCriterionUploadEnd);
		}

		if (processedDateFrom != null && !"".equals(processedDateFrom)) {
			ModuleCriterion moduleCriterionProcStart = new ModuleCriterion();
			moduleCriterionProcStart.setCondition(ModuleCriterion.CONDITION_GREATER_THAN_OR_EQUALS);
			moduleCriterionProcStart.setFieldName("processedOn");
			List<Date> valueProcStartDate = new ArrayList<Date>();
			valueProcStartDate.add(AiradminUtils.getFormattedDate(processedDateFrom));
			moduleCriterionProcStart.setValue(valueProcStartDate);
			critrian.add(moduleCriterionProcStart);

		}

		if (processedDateTo != null && !"".equals(processedDateTo)) {
			ModuleCriterion moduleCriterionProcEnd = new ModuleCriterion();
			moduleCriterionProcEnd.setCondition(ModuleCriterion.CONDITION_LESS_THAN_OR_EQUALS);
			moduleCriterionProcEnd.setFieldName("processedOn");
			List<Date> valueProcEndDate = new ArrayList<Date>();
			valueProcEndDate.add(AiradminUtils.getFormattedDate(processedDateTo));
			moduleCriterionProcEnd.setValue(valueProcEndDate);
			critrian.add(moduleCriterionProcEnd);
		}

		if (dpcProcessedDateFrom != null && !"".equals(dpcProcessedDateFrom)) {
			ModuleCriterion moduleCriterionDpcStart = new ModuleCriterion();
			moduleCriterionDpcStart.setCondition(ModuleCriterion.CONDITION_GREATER_THAN_OR_EQUALS);
			moduleCriterionDpcStart.setFieldName("dpcProcessedDate");
			List<Date> valueDpcStartDate = new ArrayList<Date>();
			valueDpcStartDate.add(AiradminUtils.getFormattedDate(dpcProcessedDateFrom));
			moduleCriterionDpcStart.setValue(valueDpcStartDate);
			critrian.add(moduleCriterionDpcStart);

		}

		if (dpcProcessedDateTo != null && !"".equals(dpcProcessedDateTo)) {
			ModuleCriterion moduleCriterionDpcEnd = new ModuleCriterion();
			moduleCriterionDpcEnd.setCondition(ModuleCriterion.CONDITION_LESS_THAN_OR_EQUALS);
			moduleCriterionDpcEnd.setFieldName("dpcProcessedDate");
			List<Date> valueDpcEndDate = new ArrayList<Date>();
			valueDpcEndDate.add(AiradminUtils.getFormattedDate(dpcProcessedDateTo));
			moduleCriterionDpcEnd.setValue(valueDpcEndDate);
			critrian.add(moduleCriterionDpcEnd);
		}

		if (uploadedBy != null && !"".equals(uploadedBy)) {
			ModuleCriterion moduleCriterionUploadedBy = new ModuleCriterion();
			moduleCriterionUploadedBy.setCondition(ModuleCriterion.CONDITION_LIKE_IGNORECASE);
			moduleCriterionUploadedBy.setFieldName("uploadedBy");
			List<String> valueUploadedBy = new ArrayList<String>();
			valueUploadedBy.add(uploadedBy + "%");
			moduleCriterionUploadedBy.setValue(valueUploadedBy);
			critrian.add(moduleCriterionUploadedBy);
		}

		if (processedBy != null && !"".equals(processedBy)) {
			ModuleCriterion moduleCriterionProcBy = new ModuleCriterion();
			moduleCriterionProcBy.setCondition(ModuleCriterion.CONDITION_LIKE_IGNORECASE);
			moduleCriterionProcBy.setFieldName("processedBy");
			List<String> valueProcBy = new ArrayList<String>();
			valueProcBy.add(processedBy + "%");
			moduleCriterionProcBy.setValue(valueProcBy);
			critrian.add(moduleCriterionProcBy);
		}

		try {
			Page<BSPHOTFileLog> pagedData = ModuleServiceLocator.getInvoicingBD().getBSPHOTFileRecords(start, pageSize,
					critrian);

			this.records = pagedData.getTotalNoOfRecords();
			this.total = pagedData.getTotalNoOfRecords() / pageSize;
			int mod = pagedData.getTotalNoOfRecords() % pageSize;
			if (mod > 0) {
				this.total = this.total + 1;
			}

			Collection<BSPHOTFileLog> fileLogs = pagedData.getPageData();
			rows = new ArrayList<Map<String, Object>>();
			int i = 1;
			for (BSPHOTFileLog fileLog : fileLogs) {
				row = new HashMap<String, Object>();
				row.put("id", ((page - 1) * pageSize) + i++);
				row.put("hotFile", fileLog);
				rows.add(row);
			}
		} catch (Exception e) {
			log.error("searchHOTFiles ", e);
		}

		return S2Constants.Result.SUCCESS;
	}

	public String execute() {
		String strHdnMode = request.getParameter(PARAM_MODE);
		String forward = WebConstants.FORWARD_SUCCESS;

		try {
			if (strHdnMode.equals(WebConstants.ACTION_DELETE)) {
				// Delete file from db
				ModuleServiceLocator.getInvoicingBD().deleteBSPHOTFile(new Integer(hdnHOTFileLogId));
				// Delete file from location
				File hotFile = new File(PlatformConstants.getAbsBSPDownloadPath() + File.separatorChar + hdnHOTFileName);
				hotFile.delete();
				this.msgType = WebConstants.MSG_SUCCESS;
				this.succesMsg = airadminConfig.getMessage("um.bsp.hotfile.delete.successful");;
			} else if (strHdnMode.equals(WebConstants.ACTION_PROCESS)) {

				File hotFile = new File(PlatformConstants.getAbsBSPDownloadPath() + File.separatorChar + hdnHOTFileName);
				boolean processSuccess = false;
				
				try {
					ModuleServiceLocator.getInvoicingBD().processBSPHOTFile(
							FileUtils.readFileToString(hotFile), hdnHOTFileName);
					processSuccess = true;
				} catch (Exception e) {
					log.error(e);
					this.msgType = WebConstants.MSG_ERROR;
					if (e instanceof ModuleException) {
						this.succesMsg = airadminConfig.getMessage(((ModuleException) e).getExceptionCode());
					} else if (e instanceof IOException) {
						this.succesMsg = airadminConfig.getMessage("um.bsp.hotfile.read.failed");
					} else {
						this.succesMsg = airadminConfig.getMessage(e.getMessage());
					}
				}
				
				if (processSuccess) {
					this.msgType = WebConstants.MSG_SUCCESS;
					this.succesMsg = airadminConfig.getMessage("um.bsp.hotfile.processing.successful");;
				}
			}
		} catch (Exception exception) {
			this.msgType = WebConstants.MSG_ERROR;
			this.succesMsg = "Please contact adminstrator... ";
			log.error("Exception in ShowBSPHOTFileManagementAction()", exception);
			if (exception instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
			}
		}

		return forward;
	}

	public void setHdnMode(String hdnMode) {
		this.hdnMode = hdnMode;
	}

	public String getHdnMode() {
		return hdnMode;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setUploadedDateFrom(String uploadedDateFrom) {
		this.uploadedDateFrom = uploadedDateFrom;
	}

	public void setUploadedDateTo(String uploadedDateTo) {
		this.uploadedDateTo = uploadedDateTo;
	}

	public void setProcessedDateFrom(String processedDateFrom) {
		this.processedDateFrom = processedDateFrom;
	}

	public void setProcessedDateTo(String processedDateTo) {
		this.processedDateTo = processedDateTo;
	}

	public void setDpcProcessedDateFrom(String dpcProcessedDateFrom) {
		this.dpcProcessedDateFrom = dpcProcessedDateFrom;
	}

	public void setDpcProcessedDateTo(String dpcProcessedDateTo) {
		this.dpcProcessedDateTo = dpcProcessedDateTo;
	}

	public void setUploadedBy(String uploadedBy) {
		this.uploadedBy = uploadedBy;
	}

	public void setProcessedBy(String processedBy) {
		this.processedBy = processedBy;
	}

	public void setRows(Collection<Map<String, Object>> rows) {
		this.rows = rows;
	}

	public Collection<Map<String, Object>> getRows() {
		return rows;
	}

	public File getFileHOTFile() {
		return fileHOTFile;
	}

	public void setFileHOTFile(File fileHOTFile) {
		this.fileHOTFile = fileHOTFile;
	}

	public String getHdnHOTFileLogId() {
		return hdnHOTFileLogId;
	}

	public void setHdnHOTFileLogId(String hdnHOTFileLogId) {
		this.hdnHOTFileLogId = hdnHOTFileLogId;
	}

	public String getHdnHOTFileName() {
		return hdnHOTFileName;
	}

	public void setHdnHOTFileName(String hdnHOTFileName) {
		this.hdnHOTFileName = hdnHOTFileName;
	}

	public String getSuccesMsg() {
		return succesMsg;
	}

	public void setSuccesMsg(String succesMsg) {
		this.succesMsg = succesMsg;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}
}
