package com.isa.thinair.airadmin.core.web.v2.dto.mis.agents;

import java.util.ArrayList;
import java.util.Collection;

public class CountryInfo {

	private Collection<GSASummary> gsaList = new ArrayList<GSASummary>();

	private String countryCode;

	private String countryName;

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public void addGSASummary(GSASummary gsaSummary) {
		this.gsaList.add(gsaSummary);
	}

	public Collection<GSASummary> getGsaList() {
		return gsaList;
	}

	public void setGsaList(Collection<GSASummary> gsaList) {
		this.gsaList = gsaList;
	}
}
