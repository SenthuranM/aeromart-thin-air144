/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airadmin.core.web.handler.inventory;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.inventory.LoadMealSegmentsHg;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airinventory.api.dto.meal.FlightMealDTO;
import com.isa.thinair.commons.api.dto.LogicalCabinClassDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author Harsha
 */
public class LoadMealSegmentsRH extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(LoadMealSegmentsRH.class);

	private static String clientErrors;

	private static Object lock = new Object();

	private static final String PARAM_MODE = "hdnMode";

	private static AiradminConfig airadminConfig = new AiradminConfig();

	/**
	 * Execute Method for template Charges
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return the forward action
	 */
	public static String execute(HttpServletRequest request) {
		if (log.isDebugEnabled())
			log.debug("Begin LoadMealSegmentsRH.execute(HttpServletRequest request)::" + System.currentTimeMillis());

		String strHdnMode = request.getParameter(PARAM_MODE);
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;
		if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_SAVE)) {
			try {
				saveData(request);
				saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS), WebConstants.MSG_SUCCESS);

			} catch (ModuleException mex) {
				log.error("Exception in  Save Meal:", mex);
				saveMessage(request, mex.getMessageString(), WebConstants.MSG_ERROR);

			} catch (Exception exception) {
				log.error("Exception in  LoadSegmentsRH.execute()", exception);
				if (exception instanceof RuntimeException) {
					forward = WebConstants.FORWARD_ERROR;
					JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
				} else {
					try {
						setFormData(request);
					} catch (Exception e) {
						log.error(e);
					}
					saveMessage(request, exception.getMessage(), WebConstants.MSG_ERROR);
				}
			}
		}

		try {
			setDisplayData(request);
		} catch (ModuleException mex) {
			log.error("Exception in  Meal charge Roll forward:", mex);
			saveMessage(request, mex.getMessageString(), WebConstants.MSG_ERROR);

		} catch (Exception exception) {
			log.error("Exception in  LoadSegmentsRH.execute()", exception);
			if (exception instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
			} else {
				saveMessage(request, exception.getMessage(), WebConstants.MSG_ERROR);
			}
		}
		return forward;
	}

	/**
	 * Saves the Data
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	private static void saveData(HttpServletRequest request) throws Exception {
		String strData = request.getParameter("hdnData");
		String[] arrData = strData.split(",");
		Collection<FlightMealDTO> colSegs = new HashSet<FlightMealDTO>();
		int noOfSegs = arrData.length / 6;
		int dataIndex = 0;
		for (int i = 0; i < noOfSegs; i++) {
			dataIndex = 6 * i;
			String cabinClass = arrData[dataIndex + 4];
			Map<String, LogicalCabinClassDTO> activeLogicalCabinClasses = CommonsServices.getGlobalConfig()
					.getAvailableLogicalCCMap();
			for (LogicalCabinClassDTO logicalCabinClassDTO : activeLogicalCabinClasses.values()) {
				if (logicalCabinClassDTO.getCabinClassCode().equals(cabinClass)) {
					FlightMealDTO fltMealDto = new FlightMealDTO();
					fltMealDto.setFlightSegmentID(new Integer(arrData[dataIndex + 2]));
					if (arrData[dataIndex + 3] != null && !arrData[dataIndex + 3].trim().equals("")) {
						fltMealDto.setTemplateId(new Integer(arrData[dataIndex + 3]));
					}
					fltMealDto.setLogicalCCCode(logicalCabinClassDTO.getLogicalCCCode());
					colSegs.add(fltMealDto);
				}
			}
			FlightMealDTO fltMealDto = new FlightMealDTO();
			fltMealDto.setFlightSegmentID(new Integer(arrData[dataIndex + 2]));
			if (arrData[dataIndex + 3] != null && !arrData[dataIndex + 3].trim().equals("")) {
				fltMealDto.setTemplateId(new Integer(arrData[dataIndex + 3]));
			}
			fltMealDto.setCabinClassCode(cabinClass);
			colSegs.add(fltMealDto);
		}
		ServiceResponce resp = ModuleServiceLocator.getMealBD().assignFlightMealCharges(colSegs, null, false);
		setServiceResponse(resp, request);
	}

	/**
	 * sets the service responce data to the request
	 * 
	 * @param resp
	 *            the ServiceResponce
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setServiceResponse(ServiceResponce resp, HttpServletRequest request) {

		if (resp.isSuccess()) {
			saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS), WebConstants.MSG_SUCCESS);
		}

	}

	/**
	 * Sets the display data
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static void setDisplayData(HttpServletRequest request) throws ModuleException {
		setMealTemplateCodes(request);
		setClientErrors(request);
		setFlightData(request);
		// setFlitStatus(request);
	}

	/**
	 * Sets the template data
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setMealTemplateCodes(HttpServletRequest request) throws ModuleException {
		String strModel = request.getParameter("modelNo");
		String s[] = null;
		int i = 0;
		StringBuilder sb = new StringBuilder();
		StringBuilder ssb = new StringBuilder();
		Collection<String[]> colTemplates = SelectListGenerator.createMealChargesTemplateIDs(strModel);
		if (colTemplates != null) {
			Iterator<String[]> iter = colTemplates.iterator();
			while (iter.hasNext()) {
				s = iter.next();
				ssb.append("arrTemp[" + i + "] = new Array('" + s[0] + "','" + s[1] + "','" + s[2] + "');");
				sb.append("<option value='" + s[0] + "'>" + s[1] + "</option>");
				i++;
			}
		}
		request.setAttribute("reqSelModel", strModel);
		request.setAttribute(WebConstants.MEAL_TEMPL_CODE, sb.toString());
		request.setAttribute("reqTemplArray", ssb.toString());
	}

	/**
	 * Sets the Client validations
	 * 
	 * @param request
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setClientErrors(HttpServletRequest request) throws ModuleException {
		String strClientErrors = getClientErrors(request);
		if (strClientErrors == null) {
			strClientErrors = "";
		}
		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
	}

	private static void setFormData(HttpServletRequest request) throws ModuleException {
		String strData = request.getParameter("hdnData");
		request.setAttribute(WebConstants.REQ_FORM_FIELD_VALUES, strData);
	}

	/**
	 * Gets the client validations
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the client validations
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {
		synchronized (lock) {
			if (clientErrors == null) {
				Properties moduleErrs = new Properties();

				clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
			}
			return clientErrors;
		}
	}

	/**
	 * Sets the Flight Data
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setFlightData(HttpServletRequest request) throws ModuleException {
		String strHtml = LoadMealSegmentsHg.getFlightData(request);
		request.setAttribute(WebConstants.MEAL_SEGMENT_DATA, strHtml);
	}

}
