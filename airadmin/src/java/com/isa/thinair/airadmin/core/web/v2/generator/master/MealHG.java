package com.isa.thinair.airadmin.core.web.v2.generator.master;

import java.util.Collection;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airmaster.api.model.Meal;
import com.isa.thinair.commons.api.exception.ModuleException;

public class MealHG {

	private static String clientErrors;
	private static String templateclientErrors;

	/**
	 * Create Client Validations for Meal Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array of Client validations
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {

		if (clientErrors == null) {
			Properties moduleErrs = new Properties();
			moduleErrs.setProperty("um.airadmin.delete.confirmation", "deleteRecoredCfrm");
			moduleErrs.setProperty("um.meal.form.name.required", "mealNameRqrd");
			moduleErrs.setProperty("um.meal.form.description.required", "descriptionRqrd");
			moduleErrs.setProperty("um.meal.form.amount.required", "amountRqrd");
			moduleErrs.setProperty("um.meal.form.mealcode.required", "mealCodeRqrd");
			moduleErrs.setProperty("um.meal.form.amount.invalid", "amountInvalid");
			clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}

		return clientErrors;
	}

	public static String setMealOptHTML() throws ModuleException {
		StringBuilder sb = new StringBuilder();
		Collection<Meal> colMeal = ModuleServiceLocator.getCommonServiceBD().getMeals();
		if (colMeal != null) {
			for (Meal meal : colMeal) {
				if (meal.getStatus().equals(Meal.STATUS_ACTIVE)) {
					sb.append("<option value='");
					sb.append(meal.getMealId());
					sb.append(",");
					sb.append(meal.getMealName());
					sb.append("'>");
					sb.append(meal.getMealName());
					sb.append("</option>");
				}

			}
		}
		return sb.toString();
	}

	public static String getTeplateClientErrors(HttpServletRequest request) throws ModuleException {

		if (templateclientErrors == null) {
			Properties moduleErrs = new Properties();
			moduleErrs.setProperty("um.airadmin.delete.confirmation", "deleteRecoredCfrm");
			moduleErrs.setProperty("um.mealTemplate.form.code.required", "tempCodeRqrd");
			moduleErrs.setProperty("um.mealTemplate.form.description.required", "descriptionRqrd");
			moduleErrs.setProperty("um.mealTemplate.form.cc.required", "ccRqrd");
			moduleErrs.setProperty("um.mealTemplate.form.meal.required", "mealRqrd");
			moduleErrs.setProperty("um.mealTemplate.form.meal.code.required", "mealCodeRqrd");
			moduleErrs.setProperty("um.mealTemplate.form.qty.required", "mealQtyRqrd");
			moduleErrs.setProperty("um.mealTemplate.form.meal.exist", "mealExist");
			moduleErrs.setProperty("um.mealTemplate.form.localcurrencycode.required","localCurrencyCodeRqrd");
			moduleErrs.setProperty("um.mealTemplate.form.popularity.popularityRqrd", "popularityRqrd");
			moduleErrs.setProperty("um.mealTemplate.form.popularity.popularityExist", "popularityExist");
			moduleErrs.setProperty("um.mealTemplate.form.popularity.popularityvalid", "popularityValid");
			moduleErrs.setProperty("um.mealTemplate.form.meal.price.required", "priceRqrd");

			templateclientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}

		return templateclientErrors;
	}
}
