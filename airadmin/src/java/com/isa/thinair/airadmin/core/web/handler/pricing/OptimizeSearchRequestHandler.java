package com.isa.thinair.airadmin.core.web.handler.pricing;

import java.util.Iterator;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.pricing.OptimizeHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author cBandara
 * 
 */

public class OptimizeSearchRequestHandler extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(OptimizeSearchRequestHandler.class);

	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	/**
	 * Main Execute Method for Optimize Action
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;
		try {
			setDisplayData(request);
		} catch (Exception exception) {
			log.error("OptimizeSearchRequestHandler.execute() method is failed :" + exception);
			if (exception instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
			} else {
				saveMessage(request, exception.getMessage(), WebConstants.MSG_ERROR);
			}
		}
		return forward;
	}

	/**
	 * Sets the Display Data for Optimize Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static void setDisplayData(HttpServletRequest request) throws ModuleException {
		String strMaxFlights = globalConfig.getBizParam(SystemParamKeys.OPTIMIZE_MAX_FLIGHTS);
		request.setAttribute(WebConstants.REQ_OPTIMIZE_MAX_FLIGHTS, strMaxFlights);
		setClientErrors(request);
		setAirportList(request);
		setTravelAgentList(request);
		setLogicalCabinClassList(request);
		setBcHtml(request);
		setUserCarriers(request);
	}

	/**
	 * Method to set Carrier codes
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setUserCarriers(HttpServletRequest request) throws ModuleException {
		User user = (User) request.getSession().getAttribute(WebConstants.SES_CURR_USER);
		StringBuilder usb = new StringBuilder();

		String strDefaultCarrierCode = "";
		strDefaultCarrierCode = user.getDefaultCarrierCode();
		if (!(strDefaultCarrierCode != null && !strDefaultCarrierCode.trim().equals(""))) {
			strDefaultCarrierCode = globalConfig.getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE);
		}
		request.setAttribute(WebConstants.REQ_HTML_DEFAULT_CRRIERCODE, strDefaultCarrierCode);

		Set<String> stCarriers = user.getCarriers();
		if (stCarriers != null) {
			int i = 0;
			Iterator<String> stIter = stCarriers.iterator();
			while (stIter.hasNext()) {
				usb.append(" arrCarriers[" + i + "] = '" + (String) stIter.next() + "';");
				i++;
			}
		}
		request.setAttribute(WebConstants.REQ_HTML_USER_CRRIERCODE, usb.toString());
	}

	/**
	 * Sets the Client Validations for Optimize Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static void setClientErrors(HttpServletRequest request) throws ModuleException {

		String strClientErrors = OptimizeHTMLGenerator.getSearchClientErrors(request);
		if (strClientErrors == null) {
			strClientErrors = "";
		}
		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
	}

	/**
	 * Sets the Airport list to the request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setAirportList(HttpServletRequest request) {
		try {
			String strHtml = SelectListGenerator.createActiveAirportCodeList();
			request.setAttribute(WebConstants.REQ_HTML_AIRPORT_LIST, strHtml);
		} catch (ModuleException moduleException) {
			log.error("OptimizeSearchRequestHandler.AirportList() method is failed :" + moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Travel Agent List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	protected static void setTravelAgentList(HttpServletRequest request) {
		try {
			String strAgentList = SelectListGenerator.createAgentList_SG();
			request.setAttribute(WebConstants.REQ_HTML_AGENT_LIST, strAgentList);
		} catch (ModuleException moduleException) {
			log.error("OptimizeSearchRequestHandler.setTravelAgentList() method is failed :" + moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}
	
	
	/**
	 * @param request
	 * the HttpServletRequest
	 */
	public static void setLogicalCabinClassList(HttpServletRequest request) {
		try {
		String strHtmlLCC = SelectListGenerator.createAllLogicalCabinClass();
		request.setAttribute(WebConstants.REQ_HTML_LOGICALCABINCLASS_LIST, strHtmlLCC);
	} catch (ModuleException moduleException) {
		log.error("OptimizeSearchRequestHandler.setLogicalCabinClassList() method is failed :" + moduleException);
		saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
	}
	}

	/**
	 * Sets the Booking Code List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	public static void setBcHtml(HttpServletRequest request) {
		try {
			String strBcList = JavascriptGenerator.createBookingCodeHtml();
			request.setAttribute(WebConstants.REQ_HTML_BC_DATA, strBcList);
		} catch (ModuleException moduleException) {
			log.error("OptimizeSearchRequestHandler.setBcHtml() method is failed :" + moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}
}
