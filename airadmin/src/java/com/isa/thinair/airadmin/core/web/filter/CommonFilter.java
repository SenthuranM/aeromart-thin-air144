package com.isa.thinair.airadmin.core.web.filter;

import java.io.IOException;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleConfig;
import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.commons.core.constants.TasksUtil;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.webplatform.api.util.PrivilegeUtil;

public class CommonFilter extends BasicFilter {

	private static Log log = LogFactory.getLog(CommonFilter.class);

	private static AiradminModuleConfig config = AiradminModuleUtils.getConfig();

	public void doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain filterChain)
			throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) arg0;
		HttpServletResponse response = (HttpServletResponse) arg1;
		String nonSecureUrl = BasicRequestHandler.getNonSecureUrl(request);
		String secureUrl = BasicRequestHandler.getSecureUrl(request);

		String strStatus = (String) request.getSession().getAttribute("HTTP_STATUS");
		if (config.isAllowHttpsOnly() && !request.isSecure()) {
			response.sendRedirect(response.encodeRedirectURL(secureUrl));
		}
		if (!config.getNonSecureProtocol().equals(config.getSecureProtocol()) && strStatus == null) {
			request.getSession().setAttribute("HTTP_STATUS", "DONE");
			if (!config.isAllowHttpsOnly()) {
				response.sendRedirect(response.encodeRedirectURL(nonSecureUrl));
			}
			return;
		} else if (request.getSession().getAttribute(WebConstants.SES_CURR_USER) == null) {
			String userId = request.getRemoteUser();

			try {
				User user = ModuleServiceLocator.getSecurityBD().getUser(userId);
				request.getSession().setAttribute(WebConstants.SES_CURR_USER, user);

				UserPrincipal userPrincipal = (UserPrincipal) request.getUserPrincipal();
				Map<String, String> mapPrivilegeIds = PrivilegeUtil.getXBEAndAdminUserPrivileges(user, userPrincipal);
				// Validation for displaying Credit Card Transaction Report MenuItem. If No report options there
				// ,have to eliminate the MenuItem from the MainMenu
				this.checkCCTransactionPrivileges(mapPrivilegeIds);

				request.getSession().setAttribute(WebConstants.SES_PRIVILEGE_IDS, mapPrivilegeIds);
				log.debug(user.getUserId() + "'s priviledges: " + mapPrivilegeIds);

				// log.debug(user.getUserId() + "'s priviledges: "
				// + user.getPrivitedgeIDs());
				if ("true".equals(config.getEnableAudit())) {
					BasicRequestHandler.audit(request, TasksUtil.SECURITY_LOG_IN);
				} else {
					log.info("LOGIN USER [userID=" + user.getUserId() + "]");
				}

			} catch (ModuleException e) {
				String msg = e.getMessage();
				JavascriptGenerator.setServerError(request, msg, "", "");
				log.error(msg);
				BasicRequestHandler.forward(arg0, arg1, errorPage);
				return;
			} catch (RuntimeException re) {
				String msg = null;

				if (re instanceof ModuleRuntimeException) {
					ModuleRuntimeException mre = (ModuleRuntimeException) re;
					msg = mre.getMessageString();
				} else {
					msg = re.getMessage();
				}

				if ("null".equals(msg)) {
					msg = "NullPointer Exception";
				}

				log.error(msg);
				BasicRequestHandler.forward(arg0, arg1, errorPage);
				return;
			}

		}

		filterChain.doFilter(arg0, arg1);
	}

	protected void checkCCTransactionPrivileges(Map<String, String> mapPrivilegeIds) {
		if (mapPrivilegeIds.containsKey(WebConstants.Reports.CCTransactionPrivileges.CCTRANSACTION_MENU)) {

			if (!(mapPrivilegeIds.containsKey(WebConstants.Reports.CCTransactionPrivileges.CHARGE)
					|| mapPrivilegeIds.containsKey(WebConstants.Reports.CCTransactionPrivileges.REFUND)
					|| mapPrivilegeIds.containsKey(WebConstants.Reports.CCTransactionPrivileges.PENDING_REFUND))) {

				mapPrivilegeIds.remove(WebConstants.Reports.CCTransactionPrivileges.CCTRANSACTION_MENU);
			}
		}
	}

}
