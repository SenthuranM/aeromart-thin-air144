/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */

package com.isa.thinair.airadmin.core.web.handler.tools;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.tools.PRLDetailProcessingHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.FlightReconcileDTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.messagepasser.api.model.PRL;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.messagepasser.api.model.PRLPaxEntry;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.messagepasser.api.utils.ParserConstants;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.Util;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public final class PRLDetailProcessingRequestHandler extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(PRLDetailProcessingRequestHandler.class);
	private static AiradminConfig airadminConfig = new AiradminConfig();

	private static final String PARAM_MODE = "hdnMode";
	private static final String PARAM_CURRENT_PRL = "hdnCurrentPrl";
	private static final String PARAM_CURRENT_PRL_ID = "hdnPRLID";
	private static final String PARAM_FIRST_TIME = "hdnState";
	private static final String PARAM_PRL_PAX_ID = "hdnPaxID";
	private static final String PARAM_PAX_TYPE = "selPaxType";
	private static final String PARAM_FIRST_NAME = "txtFirstName";
	private static final String PARAM_LAST_NAME = "txtLastName";
	private static final String PARAM_PNR = "txtPNR";
	private static final String PARAM_DESTINATION = "selDest";
	private static final String PARAM_PAX_VERSION = "hdnPaxVersion";
	private static final String PARAM_PROCESS_STATUS = "hdnProcessSta";
	private static final String PARAM_DOWNLOAD_DATE = "hdnDownloadDate";
	private static final String PARAM_FLIGHT_NUMBER = "hdnFlightNo";
	private static final String PARAM_FLIGHT_DATE = "hdnFlightDate";
	private static final String PARAM_ORIGIN = "hdnOrigin";
	private static final String PARAM_PAX_TITLE = "selTitle";
	private static final String PARAM_STATUS = "selAddStatus";
	private static final String PARAM_ETICKET_NUMBER = "txtEtNo";
	private static final String PARAM_INFANT_ETICKET_NUMBER = "txtInfEtNo";
	private static final String PARAM_COUPEN_NUMBER = "hdnCoupNumber";
	private static final String PARAM_INF_COUPEN_NUMBER = "hdnInfCoupNumber";
	private static final String PARAM_CABIN_CLASS_CODE = "selCC";
	private static final String PARAM_PASSPORT_NUMBER= "txtPassportNo";
	private static final String PARAM_PASSPORT_EXP_DATE = "txtPassportExpDate";
	private static final String PARAM_COUNTRY_OF_ISSUE = "txtCountryOfIssue";
	private static final String PARAM_SEAT_NO = "txtSeatNo";
	private static final String PARAM_WEIGHT_INDICATOR = "selWeightIndicator";
	private static final String PARAM_NO_OF_CHECKED_BAGGAGE = "txtNoOfCheckedBag";
	private static final String PARAM_CHECKED_BAG_WEIGHT= "txtCheckedBagWeight";
	private static final String PARAM_UNCHECKED_BAG_WEIGHT = "txtUncheckedBagWeight";

	private static final String PAX_CHILD_TYPE = "CH";
	private static final String PAX_CHILD = "CHILD";
	private static final String DATE_FORMAT_DDMMYYYY = "dd/MM/yyyy HH:mm";
	private static final SimpleDateFormat outputDateFormat = new SimpleDateFormat(DATE_FORMAT_DDMMYYYY);

	// PNR related validation
	private static final int SUCCESS = 0;
	private static final int NO_FLT_SEG = 1;
	private static final int SEG_NOT_IN_RES = 2;
	private static final int INVALID_PNR = 3;
	private static final int CNL_RES = 5;
	private static final int AMBIGUOUS_FLIGHT = 6;
	private static final int CNL_SEG = 7;
	private static final int INVALID_ET = 17;


	/**
	 * Sucess Values: 0 - Saved Successfuly 1 - Delete Successfully -1 - Error
	 * occurred - No releavant field identified -2 - Error occurred - Pax Title
	 * -3 - Error occurred - Pax First Name -4 - Error occurred - Pax Last Name
	 * -5 - Error occurred - PNR -6 - Error occurred - Destination -7 - Error
	 * occurred - Status
	 * 
	 */
	private static int getIntSuccess(HttpServletRequest request) {
		Integer returnValue = (Integer) getAttribInRequest(request, "intSuccess");
		if (returnValue != null)
			return returnValue.intValue();
		else
			return -1;
	}
	private static void setIntSuccess(HttpServletRequest request, int value) {
		setAttribInRequest(request, "intSuccess", new Integer(value));
	}


	private static void setExceptionOccured(HttpServletRequest request, boolean value) {
		setAttribInRequest(request, "isExceptionOccured", new Boolean(value));
	}

	private static boolean isExceptionOccured(HttpServletRequest request) {
		return ((Boolean) getAttribInRequest(request, "isExceptionOccured")).booleanValue();
	}

	/**
	 * Main Execute Method For PRL Actions Sets the Succes Values 0-Not
	 * Applicable, 1-PRL process Success, 2-PRL save Success 3-Fail
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {

		String strHdnMode = request.getParameter(PARAM_MODE);
		String forward = WebConstants.FORWARD_SUCCESS;
		String strUIModeJS = "var isSearchMode = false; var strPRLContent;";
		setExceptionOccured(request, false);
		String strFormData = "var saveSuccess = " + getIntSuccess(request) + ";";
		strFormData += " var arrFormData = new Array();";
		request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
		request.setAttribute(WebConstants.REQ_OPERATION_UI_MODE, strUIModeJS);

		try {
			if (WebConstants.ACTION_SAVE.equals(strHdnMode) || (WebConstants.ACTION_DELETE.equals(strHdnMode))) {
				processPRLData(request);
			}
			setDisplayData(request);
		} catch (Exception exception) {
			if (exception instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
			}
		}

		return forward;
	}

	/**
	 * Sets the Display Data For PRL Processing Page & Succes int value //0-Not
	 * Applicable, 1-PRL process Success, 2-PRL save Success 3-Fail
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	private static void setDisplayData(HttpServletRequest request) throws Exception {
		setTitleList(request);
		setPaxTypesList(request);
		setAirportList(request);
		setClientErrors(request);
		setDestinationAirportList(request);
		setCabinClassHtml(request);
		setPaxCategory(request);
		setPaxParents(request);
		setPRLProcessingRowHtml(request);
		String strFormData = "";
		if (!isExceptionOccured(request)) {
			strFormData = " var arrFormData = new Array();";
			strFormData += " var saveSuccess = " + getIntSuccess(request) + ";";
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
		}
	}

	/**
	 * Sets the Destination airport list to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	private static void setDestinationAirportList(HttpServletRequest request) throws Exception {

		try {
			StringBuffer sb = new StringBuffer();

			String strCurrentPRL = request.getParameter(PARAM_CURRENT_PRL) == null ? "" : request
					.getParameter(PARAM_CURRENT_PRL);
			String[] arrCurrentPRL = new String[0];
			if (strCurrentPRL != null && !strCurrentPRL.equals("")) {

				arrCurrentPRL = strCurrentPRL.split(",");

				List<String> l = ModuleServiceLocator.getFlightServiceBD().getPossibleDestinations(
						arrCurrentPRL[1].toUpperCase(), outputDateFormat.parse(arrCurrentPRL[2]), arrCurrentPRL[11]);

				Iterator<String> iter = l.iterator();
				while (iter.hasNext()) {
					String s = (String) iter.next();

					sb.append("<option value='" + s + "'>" + s + "</option>");
				}
				request.setAttribute(WebConstants.REQ_HTML_AIRPORT_COMBO, sb.toString());
			} else {
				request.setAttribute(WebConstants.REQ_HTML_AIRPORT_COMBO,
						SelectListGenerator.createActiveAirportCodeList());
			}

		} catch (ModuleException moduleException) {
			log.error(
					"SendPNLADLRequestHandler.setAirportList() method is failed :" + moduleException.getMessageString(),
					moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);

		}
	}

	/**
	 * Sets the Title list to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	private static void setTitleList(HttpServletRequest request) throws Exception {
		String arrTitles = SelectListGenerator.createPaxTitleArray();
		request.setAttribute(WebConstants.PAX_TITLES, arrTitles);
	}

	private static void setPaxTypesList(HttpServletRequest request) throws ModuleException {
		String arrPaxTypes = SelectListGenerator.createPaxTypesArray();
		request.setAttribute(WebConstants.PAX_TYPE, arrPaxTypes);
	}

	/**
	 * Sets the Active Online Airport List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setAirportList(HttpServletRequest request) {
		try {
			String strHtml = SelectListGenerator.createActiveAirportCodeList();
			request.setAttribute(WebConstants.REQ_HTML_AIRPORT_LIST, strHtml);
		} catch (ModuleException moduleException) {
			log.error(
					"PRLDetailProcessingRequestHandler.setAirportList() method is failed :"
							+ moduleException.getMessageString(), moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Client Validations to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = PRLDetailProcessingHTMLGenerator.getClientErrors(request);
			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error(
					"PRLProcessingRequestHandler.setClientErrors() method is failed :"
							+ moduleException.getMessageString(), moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the PRL Processing rows to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	protected static void setPRLProcessingRowHtml(HttpServletRequest request) throws Exception {
		try {
			PRLDetailProcessingHTMLGenerator htmlGen = new PRLDetailProcessingHTMLGenerator();
			String strHtml = htmlGen.getPRLProcessingRowHtml(request);

			String strFirstTime = request.getParameter(PARAM_FIRST_TIME) == null ? "" : request
					.getParameter(PARAM_FIRST_TIME);
			int currentPrlId = 0;
			PRL prl = null;
			Object activePRL = request.getParameter(PARAM_CURRENT_PRL_ID);
			if (activePRL != null) {
				currentPrlId = Integer.parseInt(activePRL.toString());
			}
			if (currentPrlId != 0) {
				prl = ModuleServiceLocator.getPRLBD().getPRL(currentPrlId);
			}
			if (prl != null && strFirstTime.equals("")) {
				strHtml += " prlState = '" + prl.getProcessedStatus() + "'; ";
			}
			request.setAttribute(WebConstants.REQ_HTML_ROWS, strHtml);

			request.setAttribute(WebConstants.REQ_FORM_FIELD_VALUES, htmlGen.getFormFieldValues());
		} catch (ModuleException moduleException) {
			log.error("PRLProcessingHandler.setPRLProcessingRowHtml() method is failed :"
					+ moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Making the String Compatible Spaces
	 * 
	 * @param value
	 *            the string to make compatible
	 * @return String the compatible String
	 */
//	private static String makeStringCompliant(String value) {
//		char[] charArray = PlatformUtiltiies.nullHandler(value).toUpperCase().toCharArray();
//		for (int i = 0; i < charArray.length; i++) {
//			int ascii = (int) charArray[i];
//			if (ascii < 65 | ascii > 90) {
//				charArray[i] = ' ';
//			}
//		}
//		String g = new String(charArray);
//		return g.replaceAll(" ", "");
//	}

	/**
	 * Sets the Cabinclasses List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setCabinClassHtml(HttpServletRequest request) throws ModuleException {
		String strCabinClassList = SelectListGenerator.createCabinClassList();
		request.setAttribute(WebConstants.REQ_HTML_CABINCLASS_LIST, strCabinClassList);
	}

	/**
	 * Sets Pax Category Type to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setPaxCategory(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createPaxCatagoryList();
		request.setAttribute(WebConstants.REQ_PAXCAT_LIST, strHtml);
	}

	private static void setPaxParents(HttpServletRequest request) throws ModuleException {
		PRLDetailProcessingHTMLGenerator htmlGen = new PRLDetailProcessingHTMLGenerator();
		String paxInfParentHtml = htmlGen.getInfantPaxParentHTML(request);
		request.setAttribute(WebConstants.REQ_PAX_PARENT, paxInfParentHtml);
	}
	/**
	 * Process the PRL pax entry
	 *
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the exception
	 */

	private static void processPRLData(HttpServletRequest request) throws Exception, ModuleException {

		String strHdnMode = request.getParameter(PARAM_MODE);
		String prlId = AiradminUtils.getNotNullString(request.getParameter(PARAM_CURRENT_PRL_ID));
		String prlPaxId = request.getParameter(PARAM_PRL_PAX_ID);
		String paxTitle = request.getParameter(PARAM_PAX_TITLE);
		String paxType = request.getParameter(PARAM_PAX_TYPE);
		String firstName = request.getParameter(PARAM_FIRST_NAME);
		String lastName = request.getParameter(PARAM_LAST_NAME);
		String strPnr = request.getParameter(PARAM_PNR);
		String eticketNumber = request.getParameter(PARAM_ETICKET_NUMBER);
		String infEticketNumber = request.getParameter(PARAM_INFANT_ETICKET_NUMBER);
		String coupenNumber = request.getParameter(PARAM_COUPEN_NUMBER);
		String infCoupenNumber = request.getParameter(PARAM_INF_COUPEN_NUMBER);
		String status = request.getParameter(PARAM_STATUS);
		String destination = request.getParameter(PARAM_DESTINATION);
		String receivedDate = request.getParameter(PARAM_DOWNLOAD_DATE);
		String flightDate = request.getParameter(PARAM_FLIGHT_DATE);
		String flightNumber = request.getParameter(PARAM_FLIGHT_NUMBER);
		String strPaxVersion = AiradminUtils.getNotNullString(request.getParameter(PARAM_PAX_VERSION));
		String cabinClassCode = request.getParameter(PARAM_CABIN_CLASS_CODE);
		String departureAirport = request.getParameter(PARAM_ORIGIN);
		String processStatus = request.getParameter(PARAM_PROCESS_STATUS);
		String passportNo = request.getParameter(PARAM_PASSPORT_NUMBER);
		String passportExpDate = request.getParameter(PARAM_PASSPORT_EXP_DATE);
		String countryOfIssue = request.getParameter(PARAM_COUNTRY_OF_ISSUE);
		String seatNo = request.getParameter(PARAM_SEAT_NO);
		String weightIndicator = request.getParameter(PARAM_WEIGHT_INDICATOR);
		String noOfCheckedBaggage = request.getParameter(PARAM_NO_OF_CHECKED_BAGGAGE);
		String checkedBagWeight = request.getParameter(PARAM_CHECKED_BAG_WEIGHT);
		String uncheckedBagWeight = request.getParameter(PARAM_UNCHECKED_BAG_WEIGHT);

		PRL currentPrl = null;

		if (!StringUtil.isNullOrEmpty(prlId)) {
			currentPrl = ModuleServiceLocator.getPRLBD().getPRL(Integer.parseInt(prlId));
		}

		if (strHdnMode.equals(WebConstants.ACTION_SAVE)) {

			PRLPaxEntry prlPaxEntry = new PRLPaxEntry();
			prlPaxEntry.setPnr(strPnr);

			if (!StringUtil.isNullOrEmpty(prlPaxId)) {
				prlPaxEntry.setPrlPaxId(new Integer(prlPaxId));
			}

			if (ReservationInternalConstants.PassengerType.INFANT.equals(paxType)) {
				if (!StringUtil.isNullOrEmpty(infEticketNumber)) {
					prlPaxEntry.setInfEticketNumber(infEticketNumber);
				}
				if (!StringUtil.isNullOrEmpty(infCoupenNumber)) {
					prlPaxEntry.setInfCoupNumber(new Integer(infCoupenNumber));
				}
			} else {
				prlPaxEntry.setEticketNumber(eticketNumber);
				prlPaxEntry.setCoupNumber(new Integer(coupenNumber));
			}
			if (Long.parseLong(strPaxVersion) == -1) {
				prlPaxEntry.setProcessedStatus(ParserConstants.PRLProcessStatus.NOT_PROCESSED);
			} else {
				prlPaxEntry.setProcessedStatus(processStatus);
			}
			prlPaxEntry.setPrlId(new Integer(prlId));
			if (!StringUtil.isNullOrEmpty(receivedDate)) {
				prlPaxEntry.setReceivedDate(outputDateFormat.parse(receivedDate));
			}
			if (!StringUtil.isNullOrEmpty(passportExpDate)) {
				prlPaxEntry.setFlightDate(outputDateFormat.parse(flightDate));
			}
			prlPaxEntry.setFlightNumber(flightNumber);
			prlPaxEntry.setDepartureAirport(departureAirport);
			prlPaxEntry.setTitle(paxTitle);
			prlPaxEntry.setPaxType(paxType);
			prlPaxEntry.setFirstName(firstName);
			prlPaxEntry.setLastName(lastName);
			prlPaxEntry.setPaxStatus(status);
			prlPaxEntry.setArrivalAirport(destination);
			prlPaxEntry.setPassportNumber(passportNo);
			if (!StringUtil.isNullOrEmpty(passportExpDate)) {
				prlPaxEntry.setPassportExpiryDate(new SimpleDateFormat(CalendarUtil.PATTERN1).parse(passportExpDate));
			}
			prlPaxEntry.setCountryOfIssue(countryOfIssue);
			prlPaxEntry.setSeatNumber(seatNo);
			prlPaxEntry.setWeightIndicator(weightIndicator);

			prlPaxEntry.setNoOfCheckedBaggage(
					StringUtil.isNullOrEmpty(noOfCheckedBaggage) ? 0 : new Integer(noOfCheckedBaggage));
			prlPaxEntry.setCheckedBaggageWeight(
					StringUtil.isNullOrEmpty(checkedBagWeight) ? 0 : new Integer(checkedBagWeight));
			prlPaxEntry.setUncheckedBaggageWeight(
					StringUtil.isNullOrEmpty(uncheckedBagWeight) ? 0 : new Integer(uncheckedBagWeight));
			prlPaxEntry.setCabinClassCode(cabinClassCode);
			if (!StringUtil.isNullOrEmpty(strPaxVersion)) {
				prlPaxEntry.setVersion(Long.parseLong(strPaxVersion));
			}

			int pnrValidation;
			pnrValidation = validatePNR(strPnr, paxTitle, paxType, firstName, lastName, flightNumber,
					outputDateFormat.parse(flightDate), departureAirport, destination, eticketNumber);

			if (pnrValidation == SUCCESS) {
				ModuleServiceLocator.getPRLBD().savePRLParseEntry(prlPaxEntry);
				setIntSuccess(request, 0);
				saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS),
						WebConstants.MSG_SUCCESS);
				setExceptionOccured(request, false);
				log.debug("PRLProcessingRequestHandler.processPRLData() " + "method is successfully executed.");
			} else {
				log.error("PRLProcessingRequestHandler.processPRLData() " + "method is failed : Invalid PNR");
				if (pnrValidation == INVALID_PNR) {
					// Passenger cannot identify in the PNR
					saveMessage(request, airadminConfig.getMessage("um.pfsprocess.form.pfs.pnr.pax.invalid"),
							WebConstants.MSG_ERROR);
				} else if (pnrValidation == CNL_RES) {
					// Reservation already cancelled
					saveMessage(request, airadminConfig.getMessage("um.pfsprocess.form.pfs.reservation.cancelled"),
							WebConstants.MSG_ERROR);
				} else if (pnrValidation == AMBIGUOUS_FLIGHT) {
					// Many flights exists for the selected flight number, departure date / time and the segment
					saveMessage(request, airadminConfig.getMessage("um.pfsprocess.form.pfs.ambiguous.flights"),
							WebConstants.MSG_ERROR);
				} else if (pnrValidation == SEG_NOT_IN_RES) {
					// Segment is not available in the reservation
					saveMessage(request, airadminConfig.getMessage("um.pfsprocess.form.pfs.no.segment"),
							WebConstants.MSG_ERROR);
				} else if (pnrValidation == CNL_SEG) {
					// Segment is already cancelled
					saveMessage(request, airadminConfig.getMessage("um.pfsprocess.form.pfs.segment.cancelled"),
							WebConstants.MSG_ERROR);
				} else if (pnrValidation == INVALID_ET) {
					// An invalid E-Ticket number.
					saveMessage(request, airadminConfig.getMessage("um.pfsprocess.form.pfs.invalidETicket"),
							WebConstants.MSG_ERROR);
				} else {
					// PNR segment is not valid and Passenger is not confirmed
					saveMessage(request, airadminConfig.getMessage("um.pfsprocess.form.pfs.pnr.pax.notconfirmed"),
							WebConstants.MSG_ERROR);
				}
			}

		} else if (strHdnMode.equals(WebConstants.ACTION_DELETE)) {

			ModuleServiceLocator.getPRLBD().deletePRLParseEntry(currentPrl, new Integer(prlPaxId));
			setIntSuccess(request, 1);
			saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_DELETE_SUCCESS), WebConstants.MSG_SUCCESS);
			setExceptionOccured(request, false);
			log.debug("PRLProcessingRequestHandler.processPRLData() method is successfully executed.");
		} else {
			setIntSuccess(request, -9);
			throw new ModuleException("airreservation.pfs.invalid.eticket.number");
		}
	}

	private static boolean validateEticket(Set<ReservationPax> passengers, String strEticketNumber)
			throws ModuleException {
		boolean isValidETicket = false;
		Integer pnrPaxID = ModuleServiceLocator.getPassengerBD().getPassengerIdByETicketNumber(strEticketNumber);
		if (pnrPaxID != null) {
			ReservationPax reservationPax;
			for (Iterator<ReservationPax> iterPaxs = passengers.iterator(); iterPaxs.hasNext(); ) {
				reservationPax = (ReservationPax) iterPaxs.next();
				if (pnrPaxID.equals(reservationPax.getPnrPaxId())) {
					isValidETicket = true;
					return isValidETicket;
				}
			}
			return isValidETicket;
		} else {
			return isValidETicket;
		}
	}

	/**
	 * validate the PNR coming from PRL
	 * @param strPNR
	 * @param strTitle
	 * @param strPaxType
	 * @param strFirstName
	 * @param strLastName
	 * @param flightNumber
	 * @param departureDate
	 * @param fromSegmentCode
	 * @param toSegmentCode
	 * @param strEticketNumber
	 * @return
	 * @throws ModuleException
	 */
	private static int validatePNR(String strPNR, String strTitle, String strPaxType, String strFirstName,
			String strLastName, String flightNumber, Date departureDate, String fromSegmentCode, String toSegmentCode,
			String strEticketNumber) throws ModuleException {

		if (!StringUtil.isNullOrEmpty(strPNR)) {
			Reservation reservation;
			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			pnrModesDTO.setPnr(strPNR);
			pnrModesDTO.setLoadPaxAvaBalance(true);
			reservation = ModuleServiceLocator.getReservationBD().getReservation(pnrModesDTO, null);
			Set<ReservationPax> passengers = reservation.getPassengers();

			// check whether reservation is already cancelled or not
			if (reservation.getStatus().equals(ReservationInternalConstants.ReservationStatus.CANCEL)){
				return CNL_RES;
			}

			// check whether flight segment id is valid or not
			Integer validateFlightSegId = validateFlightSegmentID(strPNR, flightNumber, departureDate, fromSegmentCode,
					toSegmentCode, reservation);
			if (validateFlightSegId != null) {
				return validateFlightSegId;
			}
			//check whether eticket number is valid or not
			if (!StringUtil.isNullOrEmpty(strEticketNumber)) {
				boolean resEtFound = validateEticket(passengers, strEticketNumber);
				if (!resEtFound) {
					return INVALID_ET;
				}
			}

			//check whether passenger exists in the pnr or not
			boolean resPaxFound = validatePassenger(strTitle, strPaxType, strFirstName, strLastName, passengers);
			if (!resPaxFound) {
				return INVALID_PNR;
			}

		} else
			return SUCCESS;

		return SUCCESS;
	}

	/**
	 * Check whether flight segment id exist in the reservation
	 * @param strPNR
	 * @param flightNumber
	 * @param departureDate
	 * @param fromSegmentCode
	 * @param toSegmentCode
	 * @param reservation
	 * @return
	 * @throws ModuleException
	 */
	private static Integer validateFlightSegmentID(String strPNR, String flightNumber, Date departureDate,
			String fromSegmentCode, String toSegmentCode, Reservation reservation) throws ModuleException {
		Collection<FlightReconcileDTO> colFlightReconcileDTO;
		FlightReconcileDTO flightReconcileDTO;
		colFlightReconcileDTO = ModuleServiceLocator.getResSegmentBD()
				.getPnrSegmentsForReconcilation(flightNumber, departureDate, fromSegmentCode, toSegmentCode, strPNR);
		Integer flighSegId = null;

		for (Iterator<FlightReconcileDTO> iterFlightReconcileDTO = colFlightReconcileDTO
				.iterator(); iterFlightReconcileDTO.hasNext(); ) {
			flightReconcileDTO = (FlightReconcileDTO) iterFlightReconcileDTO.next();
			if (flighSegId != null && !flighSegId.equals(flightReconcileDTO.getFlightSegId())) {
				return AMBIGUOUS_FLIGHT;
			}
			flighSegId = flightReconcileDTO.getFlightSegId();
		}
		if (flighSegId == null) {
			return NO_FLT_SEG;
		}

		Set<ReservationSegment> segments = reservation.getSegments();
		ReservationSegment reservationSegment = null;
		for (Iterator<ReservationSegment> iterSegs = segments.iterator(); iterSegs.hasNext(); ) {
			reservationSegment = (ReservationSegment) iterSegs.next();
			if (reservationSegment.getFlightSegId().equals(flighSegId)) {
				break;
			}
		}
		if (reservationSegment == null) {
			return SEG_NOT_IN_RES;
		} else if (reservationSegment.getStatus()
				.equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)) {
			return CNL_SEG;
		}
		return null;
	}

	/**
	 * Check whether passenger exists in the pnr or not
	 * @param strTitle
	 * @param strPaxType
	 * @param strFirstName
	 * @param strLastName
	 * @param passengers
	 * @return
	 */
	private static boolean validatePassenger(String strTitle, String strPaxType, String strFirstName, String strLastName,
			Set<ReservationPax> passengers) {
		boolean resPaxFound = false;
		ReservationPax reservationPax;// Get the correct names as appear in PRL
		for (Iterator<ReservationPax> iterPaxs = passengers.iterator(); iterPaxs.hasNext(); ) {
			reservationPax = (ReservationPax) iterPaxs.next();

			if (isPaxExistInResPassengers(strPaxType, strTitle, strFirstName, strLastName, reservationPax)) {
				resPaxFound = true;
				break;
			}
		}
		return resPaxFound;
	}


	/**
	 * Validate the passenger with the Reservations Detail
	 *
	 * @param strPaxType
	 *            the Pax Type
	 * @param strTitle
	 *            the Title
	 * @param strFirstName
	 *            the First Name
	 * @param strLastName
	 *            the Last Name
	 * @param reservationPax
	 *            the Passenger
	 * @return boolean the validated true false
	 */
	private static boolean isPaxExistInResPassengers(String strPaxType, String strTitle, String strFirstName, String strLastName,
			ReservationPax reservationPax) {
		boolean isValidPax = false;
		strTitle = Util.makeStringCompliant(strTitle.toUpperCase());
		strFirstName = Util.makeStringCompliant(strFirstName);
		strLastName = Util.makeStringCompliant(strLastName);
		strPaxType = Util.makeStringCompliant(strPaxType);
		String paxTitle = Util.makeStringCompliant(reservationPax.getTitle());
		String paxFName = Util.makeStringCompliant(reservationPax.getFirstName());
		String paxLName = Util.makeStringCompliant(reservationPax.getLastName());
		String paxType = Util.makeStringCompliant(reservationPax.getPaxType());

		if (strTitle.equals(paxTitle) && strFirstName.equals(paxFName) && strLastName.equals(paxLName)
				&& strPaxType.equals(paxType)) {
			isValidPax = true;
		} else if (paxType != null && paxType.equals(PAX_CHILD_TYPE) && strTitle.equals("")
				&& paxTitle.equalsIgnoreCase(PAX_CHILD)) {
			if (strFirstName.equals(paxFName) && strLastName.equals(paxLName)) {
				isValidPax = true;
			}
		}
		return isValidPax;
	}

}
