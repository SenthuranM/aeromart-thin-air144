package com.isa.thinair.airadmin.core.web.handler.reports;

import java.io.File;
import java.io.FileInputStream;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleConfig;
import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.FTPServerProperty;
import com.isa.thinair.commons.core.util.FTPUtil;
import com.isa.thinair.platform.api.constants.PlatformConstants;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * Forward Sales Report
 * 
 * @author dumindag
 * 
 */
public class ROPRevenueForwardReportRH extends BasicRequestHandler {
	private static Log log = LogFactory.getLog(ROPRevenueForwardReportRH.class);

	public ROPRevenueForwardReportRH() {
		super();
	}

	/**
	 * @param request
	 * @return
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {
		String strHdnMode = request.getParameter("hdnMode");
		String forward = WebConstants.ACTION_FORWARD_SUCCESS;

		try {
			setDisplayData(request);
			log.debug("ROPRevenueForwardReportRH success");

		} catch (Exception e) {
			forward = AdminStrutsConstants.AdminAction.ERROR;
			log.error("ROPRevenueForwardReportRH execute()" + e.getMessage());
		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportViewAll(request, response);
				AiradminModuleConfig adminCon = AiradminModuleUtils.getConfig();
				if (adminCon.getEnableFTPRevenue().equals("true")) {
					downloadLocal(request);
				}

				forward = AdminStrutsConstants.AdminAction.RES_SUCCESS;

			} else {
				log.error("ROPRevenueForwardReportRH setReportView not selected");
			}

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("ROPRevenueForwardReportRH setReportView Failed " + e.getMessageString());
		} catch (Exception e) {
			log.error("ROPRevenueForwardReportRH setReportView Failed ", e);
		}

		try {
			if (strHdnMode != null && strHdnMode.equals("download")) {
				setReportResult(request, response);
				forward = AdminStrutsConstants.AdminAction.RES_SUCCESS;

			} else {
				log.error("ROPRevenueForwardReportRH setReportView not selected");
			}

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("ROPRevenueForwardReportRH download Failed ", e);
		} catch (Exception e) {
			log.error("ROPRevenueForwardReportRH download Failed ", e);
		}

		return forward;
	}

	/**
	 * 
	 * @param request
	 * @throws Exception
	 */
	public static void setDisplayData(HttpServletRequest request) throws Exception {
		setClientErrors(request);
		setAirportList(request);
		setBcHtml(request);
		setAgentTypes(request);
		setGSAMultiSelectList(request);
		setStaions(request);
		setLiveStatus(request);
		setReportingPeriod(request);
		setCarrierCodeList(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);
	}

	/**
	 * @param request
	 * @throws ModuleException
	 */
	private static void setCarrierCodeList(HttpServletRequest request) throws ModuleException {
		String strList = ReportsHTMLGenerator.createCarrierCodeWithDefaultHtml(getUserCarrierCodes(request));
		request.setAttribute(WebConstants.REQ_HTML_CARRIER_CODE_LIST, strList);
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	public static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString(), moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	protected static void setReportViewAll(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		User curUser = (User) request.getSession().getAttribute("sesCurrUser");
		String strUsrerId = curUser.getUserId();

		String strAgents = request.getParameter("hdnAgents");
		String strBCs = request.getParameter("hdnBCs");
		String strSegments = request.getParameter("hdnSegments");
		String strStations = request.getParameter("hdnStations");
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);
		String strCarrierCodes = request.getParameter("hdnCarrierCode");

		// To provide Report Format Options
		String strReportFormat = request.getParameter("radRptNumFormat");

		try {
			ReportsSearchCriteria search = new ReportsSearchCriteria();

			search.setReportType(ReportsSearchCriteria.REVENUE_TAX_REPORT);
			if (fromDate != null && !fromDate.equals("")) {
				search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate) + " 00:00:00");
			}

			if (toDate != null && !toDate.equals("")) {
				search.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate) + " 23:59:59");
			}
			if (strUsrerId != null) {
				search.setUserId(strUsrerId);
			}
			if (strAgents != null && !strAgents.equals("")) {
				search.setAgentCode(strAgents);
			}
			if (strBCs != null && !strBCs.equals("")) {
				search.setBookinClass(strBCs);
			}
			if (strSegments != null && !strSegments.equals("")) {
				search.setSegment(strSegments);
			}
			if (strStations != null && !strStations.equals("")) {
				search.setStation(strStations);
			}

			if (strCarrierCodes != null && !strCarrierCodes.equals("")) {
				search.setCarrierCode(strCarrierCodes);
			} else {
				search.setCarrierCode(CommonUtil.getCollectionElementsStr(getUserCarrierCodes(request)));
			}

			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}

			if (strReportFormat != null && !strReportFormat.equals("")) {
				if (strReportFormat.equalsIgnoreCase("IT")) {
					search.setReqReportFormat("FR");
				} else
					search.setReqReportFormat("");
			}

			ModuleServiceLocator.getDataExtractionBD().getForwardSales(search);
		} catch (Exception e) {
			log.error("download method is failed :", e);
		}
	}

	protected static void setReportResult(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		User curUser = (User) request.getSession().getAttribute("sesCurrUser");
		String strUsrerId = curUser.getUserId();
		AiradminModuleConfig adminCon = AiradminModuleUtils.getConfig();
		String strRevFwdSalesPath = adminCon.getRevenueReportFowardSalesPath();
		String reportsRootDir = PlatformConstants.getConfigRootAbsPath() + "/templates/reports/reven_rept_fwd_sales";

		String filePath = "";
		if (adminCon.getEnableFTPRevenue().equals("true")) {
			filePath = reportsRootDir + "_" + strUsrerId + ".csv";
		} else {
			filePath = strRevFwdSalesPath + "_" + strUsrerId + ".csv";
		}

		String fileNameNew = "ForwardSalesReport.csv";

		try {
			File uFile = new File(filePath);
			int fSize = (int) uFile.length();
			log.debug("file size " + fSize);
			FileInputStream fis = new FileInputStream(uFile);
			response.setContentType("application/x-msdownload");
			response.addHeader("Content-Disposition", "attachment; filename=" + fileNameNew);
			response.setContentLength(fSize);
			PrintWriter pw = response.getWriter();
			int c = -1;
			// Loop to read and write bytes.

			while ((c = fis.read()) != -1) {
				pw.print((char) c);
			}
			// Close output and input resources.
			fis.close();
			pw.flush();
			pw = null;
		} catch (Exception e) {
			log.error("download method is failed :", e);

		}

	}

	private static void setAirportList(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createAirportCodeList();
		request.setAttribute(WebConstants.REQ_HTML_AIRPORT_LIST, strHtml);
	}

	public static void setBcHtml(HttpServletRequest request) throws ModuleException {
		String strBcList = JavascriptGenerator.createBookingCodeHtml();
		request.setAttribute(WebConstants.REQ_HTML_BC_DATA, strBcList);
	}

	public static void setStaions(HttpServletRequest request) throws ModuleException {
		String strstnList = JavascriptGenerator.createStationsByCountry();
		request.setAttribute(WebConstants.REQ_HTML_STN_DATA, strstnList);
	}

	/**
	 * Setting the reporting period to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setReportingPeriod(HttpServletRequest request) throws ModuleException {
		int period = new Integer(AiradminModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.REPORTING_PERIOD)).intValue();
		request.setAttribute(WebConstants.REP_RPT_START_DAY, ReportsHTMLGenerator.getReportingPeriod(period));
	}

	protected static void setGSAMultiSelectList(HttpServletRequest request) throws ModuleException {
		boolean blnWithTAs = false;
		boolean blnWithCOs = false;
		String strAgentType = "";
		String strList = "";
		if (request.getParameter("selAgencies") != null) {
			strAgentType = request.getParameter("selAgencies");
		}
		if (request.getParameter("chkCOs") != null) {
			blnWithCOs = (request.getParameter("chkCOs").equals("on") ? true : false);
		}
		if (request.getParameter("chkTAs") != null) {
			blnWithTAs = (request.getParameter("chkTAs").equals("on") ? true : false);
		}
		strList = ReportsHTMLGenerator.createAgentGSAMultiSelect(strAgentType, blnWithTAs, blnWithCOs);
		request.setAttribute(WebConstants.REQ_HTML_GSA_LIST, strList);
	}

	protected static void setAgentTypes(HttpServletRequest request) throws ModuleException {
		String strATList = SelectListGenerator.createAgentTypeList_SG();
		request.setAttribute(WebConstants.REQ_HTML_AGENTTYPE_LIST, strATList);
	}

	/**
	 * Sets the report medium to the request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleExceptions
	 */
	private static void setLiveStatus(HttpServletRequest request) throws ModuleException {
		String strLive = request.getParameter(WebConstants.REP_HDN_LIVE);
		if (strLive != null)
			request.setAttribute(WebConstants.REP_SET_LIVE, strLive);

	}

	private static void downloadLocal(HttpServletRequest request) throws Exception {

		AiradminModuleConfig adminCon = AiradminModuleUtils.getConfig();
		User curUser = (User) request.getSession().getAttribute("sesCurrUser");
		String strUsrerId = curUser.getUserId();
		String strRevPath = adminCon.getRevFTPPath();
		strRevPath = strRevPath + "_fwd_sales_" + strUsrerId + ".csv";
		String reportsRootDir = PlatformConstants.getConfigRootAbsPath() + "/templates/reports/reven_rept_fwd_sales";

		reportsRootDir = reportsRootDir + "_" + strUsrerId + ".csv";
		FTPServerProperty serverProperty = new FTPServerProperty();
		FTPUtil ftpUtil = new FTPUtil();

		// Code to FTP
		serverProperty.setServerName(adminCon.getRptFTPServer());
		if (adminCon.getRptFTPUserName() == null) {
			serverProperty.setUserName("");
		} else {
			serverProperty.setUserName(adminCon.getRptFTPUserName());
		}

		if (adminCon.getRptFTPPassword() == null) {
			serverProperty.setPassword("");
		} else {
			serverProperty.setPassword(adminCon.getRptFTPPassword());
		}
		boolean downloaded = false;

		try {
			if (ftpUtil.connectAndLogin(serverProperty.getServerName(), serverProperty.getUserName(),
					serverProperty.getPassword())) {
				ftpUtil.binary();
				downloaded = ftpUtil.downloadFile(strRevPath, reportsRootDir);
				if (downloaded) {
					downloaded = new File(reportsRootDir).exists();
				}
			}
		} catch (Exception e) {
			log.error("download attachment failed [" + e.getMessage() + "]", e);
		} finally {
			try {
				if (ftpUtil.isConnected()) {
					ftpUtil.disconnect();
				}
			} catch (Exception e) {
				log.error("Disconnecting FTP connection failed.", e);
			}
		}

	}
}
