package com.isa.thinair.airadmin.core.web.v2.action.master;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.HashSet;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.dispatcher.multipart.MultiPartRequestWrapper;
import org.json.JSONArray;
import org.json.JSONException;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.AiradminModuleConfig;
import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airadmin.core.web.v2.handler.master.AdvertisementRH;
import com.isa.thinair.airmaster.api.dto.AdvertisementSearchDTO;
import com.isa.thinair.airmaster.api.model.ItineraryAdvertisement;
import com.isa.thinair.commons.api.dto.ModuleCriteria;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.MessagesUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ShowAdvertisementAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(ShowAdvertisementAction.class);
	private Object[] rows;
	private int page;
	private int total;
	private int records;
	private String succesMsg;
	private String msgType;

	private Integer advertisementId;
	private String advertisementSegmentCode;
	private String advertisementTitle;
	private String advertisementLanguage;
	private String advertisementImage;
	private String status;
	private String advertisementRemarks;
	private String editAdvertisementImage;

	private long version;
	private String createdBy;
	private Date createdDate;

	private AdvertisementSearchDTO advertisementSrch;

	private String hdnMode;
	private File uploadImage;
	private File editUploadImage;
	private String mode;

	private String advertisementDeleteId;

	public String getAdvertisementDeleteId() {
		return advertisementDeleteId;
	}

	public void setAdvertisementDeleteId(String advertisementDeleteId) {
		this.advertisementDeleteId = advertisementDeleteId;
	}

	public String getEditAdvertisementImage() {
		return editAdvertisementImage;
	}

	public void setEditAdvertisementImage(String editAdvertisementImage) {
		this.editAdvertisementImage = editAdvertisementImage;
	}

	public File getEditUploadImage() {
		return editUploadImage;
	}

	public void setEditUploadImage(File editUploadImage) {
		this.editUploadImage = editUploadImage;
	}

	public Integer getAdvertisementId() {
		return advertisementId;
	}

	public void setAdvertisementId(Integer advertisementId) {
		this.advertisementId = advertisementId;
	}

	public String getAdvertisementSegmentCode() {
		return advertisementSegmentCode;
	}

	public void setAdvertisementSegmentCode(String advertisementSegmentCode) {
		this.advertisementSegmentCode = advertisementSegmentCode;
	}

	public String getAdvertisementTitle() {
		return advertisementTitle;
	}

	public void setAdvertisementTitle(String advertisementTitle) {
		this.advertisementTitle = advertisementTitle;
	}

	public String getAdvertisementLanguage() {
		return advertisementLanguage;
	}

	public void setAdvertisementLanguage(String advertisementLanguage) {
		this.advertisementLanguage = advertisementLanguage;
	}

	public String getAdvertisementImage() {
		return advertisementImage;
	}

	public void setAdvertisementImage(String advertisementImage) {
		this.advertisementImage = advertisementImage;
	}

	public String getAdvertisementRemarks() {
		return advertisementRemarks;
	}

	public void setAdvertisementRemarks(String advertisementRemarks) {
		this.advertisementRemarks = advertisementRemarks;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	private static AiradminConfig airadminConfig = new AiradminConfig();

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public String getSuccesMsg() {
		return succesMsg;
	}

	public void setSuccesMsg(String succesMsg) {
		this.succesMsg = succesMsg;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public Object[] getRows() {
		return rows;
	}

	public void setRows(Object[] rows) {
		this.rows = rows;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public String getHdnMode() {
		return hdnMode;
	}

	public void setHdnMode(String hdnMode) {
		this.hdnMode = hdnMode;
	}

	public File getUploadImage() {
		return uploadImage;
	}

	public void setUploadImage(File uploadImage) {
		this.uploadImage = uploadImage;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getMode() {
		return mode;
	}

	public AdvertisementSearchDTO getAdvertisementSrch() {
		return advertisementSrch;
	}

	public void setAdvertisementSrch(AdvertisementSearchDTO advertisementSrch) {
		this.advertisementSrch = advertisementSrch;
	}

	public String execute() {

		try {

			ItineraryAdvertisement savedAdvertisement = null;
			ItineraryAdvertisement advertisement = new ItineraryAdvertisement();
			// advertisement.setAdvertisementSegmentCode("ABB/CMB/APL");
			advertisement.setAdvertisementTitle(advertisementTitle.trim());
			advertisement.setAdvertisementLanguage(advertisementLanguage.trim());
			advertisement.setAdvertisementImage(advertisementImage.trim());
			if (status != null) {
				advertisement.setStatus(status);
			} else {
				advertisement.setStatus(ItineraryAdvertisement.STATUS_INACTIVE);
			}
			advertisement.setAdvertisementRemarks(advertisementRemarks.trim());
			advertisement.setVersion(version);
			advertisement.setCreatedBy(createdBy);
			advertisement.setCreatedDate(createdDate);
			advertisement.setAdvertisementId(advertisementId);

			Set<String> advertisementSegmentList = new HashSet<String>();
			String[] segmentArray = advertisementSegmentCode.split(",");
			for (String s : segmentArray) {
				advertisementSegmentList.add(s.trim());
			}
			advertisement.setApplicableONDs(advertisementSegmentList);
			if (isNewONDsExists(advertisementSegmentCode)) {
				if (this.hdnMode != null
						&& ((hdnMode.equals(WebConstants.ACTION_ADD)) || (hdnMode.equals(WebConstants.ACTION_EDIT)))) {
					try {

						AiradminModuleConfig adminCon = AiradminModuleUtils.getConfig();
						String fullFileName = null;
						File theFile = null;
						File theExistingFile = null;
						if ("advertisementImage".equals(mode) && hdnMode.equals(WebConstants.ACTION_ADD)) {
							if ((AdvertisementRH.advertisementExistFor(advertisementTitle, advertisementLanguage))) {
								checkFileExtention("uploadImage");
								String uniqueFileName = UUID.randomUUID().toString() + "_" + advertisementLanguage.toUpperCase()
										+ "_no_cache.jpg";
								fullFileName = adminCon.getItineraryAdvertisementImages() + uniqueFileName; // change
								theFile = new File(fullFileName);
								FileUtils.copyFile(uploadImage, theFile);
								advertisement.setAdvertisementImage(uniqueFileName);
							} else {
								this.succesMsg = airadminConfig.getMessage("um.itinerary.TNL.validate");
								this.msgType = WebConstants.MSG_ERROR;
								return S2Constants.Result.SUCCESS;
							}
						} else if (hdnMode.equals(WebConstants.ACTION_EDIT) && !editAdvertisementImage.equals("")) {
							checkFileExtention("editUploadImage");
							String uniqueFileName = UUID.randomUUID() + "_" + advertisementLanguage.toUpperCase()
									+ "_no_cache.jpg";
							fullFileName = adminCon.getItineraryAdvertisementImages() + uniqueFileName; // change
							String ExisitingFileName = adminCon.getItineraryAdvertisementImages() + advertisementImage.trim();
							theExistingFile = new File(ExisitingFileName);
							FileUtils.forceDelete(theExistingFile);
							theFile = new File(fullFileName);
							FileUtils.copyFile(editUploadImage, theFile);
							advertisement.setAdvertisementImage(uniqueFileName);
						}
						AdvertisementRH.saveAdvertisement(advertisement);
						savedAdvertisement = AdvertisementRH.getAdvertisement(advertisementTitle.trim(),
								advertisementLanguage.trim());

						this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS);
						this.msgType = WebConstants.MSG_SUCCESS;

					} catch (ModuleException e) {
						this.succesMsg = e.getMessageString();
						if (e.getMessageString().equals(MessagesUtil.getMessage("module.duplicate.key"))) {
							this.succesMsg = airadminConfig.getMessage("um.itinerary.advertisement.duply");
						}
						this.msgType = WebConstants.MSG_ERROR;
					} catch (IOException e) {
						this.succesMsg = airadminConfig.getMessage("um.itinerary.file.upload");
						this.msgType = WebConstants.MSG_ERROR;
						if (log.isErrorEnabled()) {
							log.error(e);
						}
					} catch (IllegalArgumentException ex) {
						this.succesMsg = airadminConfig.getMessage("um.itinerary.ImgType.validate");
						this.msgType = WebConstants.MSG_ERROR;
						if (log.isErrorEnabled()) {
							log.error(ex);
						}
					} catch (Exception ex) {
						this.succesMsg = airadminConfig.getMessage("um.itinerary.system.error");
						this.msgType = WebConstants.MSG_ERROR;
						if (log.isErrorEnabled()) {
							log.error(ex);
						}
					}
				} else {
					savedAdvertisement = advertisement;
				}
			} else {
				this.succesMsg = airadminConfig.getMessage("um.itinerary.OND.validate");
				this.msgType = WebConstants.MSG_ERROR;
			}

		} catch (Exception e) {
			this.succesMsg = airadminConfig.getMessage("um.itinerary.system.error");
			this.msgType = WebConstants.MSG_ERROR;
			log.error("Error in saving advertisement", e);
		}
		return S2Constants.Result.SUCCESS;
	}

	public String searchAdvertisement() {

		try {
			List<ModuleCriterion> critrian = new ArrayList<ModuleCriterion>();

			if (advertisementSrch != null) {
				String origin = "";
				String destination = "";

				if (advertisementSrch.getOrigin() != null && !advertisementSrch.getOrigin().trim().equals("")) {
					origin += advertisementSrch.getOrigin();
				}
				if (advertisementSrch.getDestination() != null && !advertisementSrch.getDestination().trim().equals("")) {
					destination = advertisementSrch.getDestination();
				}
				if (!(origin.trim().equals("") && destination.trim().equals(""))) {
					ModuleCriteria criteriaADSegment = new ModuleCriteria();
					List<String> lstadvertisementSegment = new ArrayList<String>();
					criteriaADSegment.setFieldName("advertisement.advertisementSegmentCode");
					criteriaADSegment.setCondition(ModuleCriteria.CONDITION_LIKE);
					lstadvertisementSegment.add(origin + "%" + destination);
					criteriaADSegment.setValue(lstadvertisementSegment);
					critrian.add(criteriaADSegment);
				}
				if (advertisementSrch.getTitle() != null && !advertisementSrch.getTitle().trim().equals("")) {
					ModuleCriteria criteriaADTitle = new ModuleCriteria();
					List<String> lstadvertisementTitle = new ArrayList<String>();
					criteriaADTitle.setFieldName("advertisement.advertisementTitle");
					criteriaADTitle.setCondition(ModuleCriteria.CONDITION_LIKE);
					lstadvertisementTitle.add("%" + advertisementSrch.getTitle() + "%");
					criteriaADTitle.setValue(lstadvertisementTitle);
					critrian.add(criteriaADTitle);
				}
				if (advertisementSrch.getLanguage() != null && !advertisementSrch.getLanguage().trim().equals("")) {
					ModuleCriteria criteriaADLanguage = new ModuleCriteria();
					List<String> lstadvertisementLanguage = new ArrayList<String>();
					criteriaADLanguage.setFieldName("advertisement.advertisementLanguage");
					criteriaADLanguage.setCondition(ModuleCriteria.CONDITION_LIKE);
					lstadvertisementLanguage.add("%" + advertisementSrch.getLanguage() + "%");
					criteriaADLanguage.setValue(lstadvertisementLanguage);
					critrian.add(criteriaADLanguage);
				}
			}

			Page<ItineraryAdvertisement> pgmeal = AdvertisementRH.searchAdvertisement(this.page, critrian);
			this.records = pgmeal.getTotalNoOfRecords();
			this.total = pgmeal.getTotalNoOfRecords() / 20;
			int mod = pgmeal.getTotalNoOfRecords() % 20;
			if (mod > 0) {
				this.total = this.total + 1;
			}
			if (this.page <= 0) {
				this.page = 1;
			}

			Collection<ItineraryAdvertisement> colAdvertisement = pgmeal.getPageData();

			if (!StringUtil.isNullOrEmpty(colAdvertisement.toString())) {
				Object[] dataRow = new Object[colAdvertisement.size()];
				int index = 1;
				Iterator<ItineraryAdvertisement> iter = colAdvertisement.iterator();
				while (iter.hasNext()) {
					Map<String, Object> counmap = new HashMap<String, Object>();
					ItineraryAdvertisement grdAdvertisement = iter.next();
					String[] segmentData = grdAdvertisement.getApplicableONDs().toArray(new String[0]);
					String strsegmentData = "";
					for (String s : segmentData) {
						strsegmentData += s + ",";
					}
					strsegmentData = strsegmentData.substring(0, strsegmentData.length() - 1);
					// grdAdvertisement.setAdvertisementSegmentCode(strsegmentData);
					setAdvertisementSegmentCode(strsegmentData);
					counmap.put("id", new Integer(((page - 1) * 20) + index).toString());
					counmap.put("advertisement", grdAdvertisement);
					counmap.put("advertisementSegmentCode", advertisementSegmentCode);
					dataRow[index - 1] = counmap;
					index++;
				}

				this.rows = dataRow;
			}
		} catch (Exception e) {
			this.succesMsg = airadminConfig.getMessage("um.itinerary.system.error");
			this.msgType = WebConstants.MSG_ERROR;
			log.error("Error in searchAdvertisement", e);
		}
		return S2Constants.Result.SUCCESS;
	}

	public String deleteAdvertisement() {

		JSONArray jsonArray;
		List<String> advertismentIdList = new ArrayList<String>();
		try {
			jsonArray = new JSONArray(advertisementDeleteId);
			if (jsonArray != null) {
				int len = jsonArray.length();
				for (int i = 0; i < len; i++) {
					advertismentIdList.add(jsonArray.getString(i));
				}
			}
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (advertismentIdList.size() > 0) {
			for (int i = 0; i < advertismentIdList.size(); i++) {
				try {
					AiradminModuleConfig adminCon = AiradminModuleUtils.getConfig();
					Integer advertisementID = Integer.parseInt(advertismentIdList.get(i));
					ItineraryAdvertisement exist = AdvertisementRH.getAdvertisement(advertisementID);
					if (!StringUtil.isNullOrEmpty(exist.toString())) {
						String fullFileName = null;
						File theFile = null;
						try {
							fullFileName = adminCon.getItineraryAdvertisementImages() + exist.getAdvertisementImage();
							theFile = new File(fullFileName);
							FileUtils.forceDelete(theFile);
							AdvertisementRH.deleteAdvertisement(advertisementID);
							this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_DELETE_SUCCESS);
							this.msgType = WebConstants.MSG_SUCCESS;
						} catch (Exception e) {
							this.succesMsg = airadminConfig.getMessage("um.itinerary.file.delete");
							this.msgType = WebConstants.MSG_ERROR;
							log.error("Error in deleting advertisement image", e);
						}
					}
				} catch (ModuleException e) {
					this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_DELETE_FAIL);
					if (e.getMessageString().equals(MessagesUtil.getMessage("module.constraint.childrecord"))) {
						this.succesMsg = airadminConfig.getMessage("um.airadmin.childrecord");
					}
					log.error("Error in deleting advertisement", e);
					this.msgType = WebConstants.MSG_ERROR;
				} catch (Exception e) {
					this.succesMsg = airadminConfig.getMessage("um.itinerary.system.error");
					this.msgType = WebConstants.MSG_ERROR;
					log.error("Error in deleting advertisement", e);
				}
			}
		} else {
			this.succesMsg = airadminConfig.getMessage("um.itinerary.system.error");
			this.msgType = WebConstants.MSG_ERROR;
			log.error("Error in deleting advertisement");
		}

		return S2Constants.Result.SUCCESS;
	}

	private void checkFileExtention(String fileToBeChecked) {
		// Other wise this is an empty file upload request. (No File Detected.
		// ). No need to validate the format.
		if (ServletActionContext.getRequest() instanceof MultiPartRequestWrapper) {
			Collection<String> allowedFormats = AppSysParamsUtil.getAllowedImageFormatsForUpload();
			MultiPartRequestWrapper requestWrapper = ((MultiPartRequestWrapper) ServletActionContext.getRequest());
			if (!FilenameUtils.isExtension(requestWrapper.getFileNames(fileToBeChecked)[0], allowedFormats)) {
				throw new IllegalArgumentException("File type must be of types " + allowedFormats);
			}
		}
	}

	private boolean isNewONDsExists(String newOnds) throws ModuleException {
		List<String> newOndList = new ArrayList<String>();
		if (!StringUtil.isNullOrEmpty(newOnds)) {
			newOndList = Arrays.asList(newOnds.split(", "));
		}

		if (newOndList.size() > 0) {
			// Check newly entered ONDs are exists in the system (T_OND table)
			if (!ModuleServiceLocator.getChargeBD().isONDsExists(new HashSet<String>(newOndList))) {
				return false;
			}
		}

		return true;
	}

}
