package com.isa.thinair.airadmin.core.web.v2.dto.inventory;

import java.io.Serializable;
import java.util.List;

public class FlightForSeatAllocationTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String flightNumber;

	private String departure;

	private String departureDateTime;

	private String arrival;

	private String arrivalDateTime;

	private String viaPoints;

	private String modelNumber;

	private String segmentAllocation;

	private String segmentSold;

	private String segmentOnHold;

	private int flightId;

	private List<String[]> cosList;

	private String flightStatus;

	private String defaultCabinClass;

	private Integer overlappingFlightId;

	private String availableSeatKilometers;

	private Integer scheduleId;

	private String seatFactor;

	private String fromFromPage = "ALLOC";

	private String lowestPubFare;

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getDeparture() {
		return departure;
	}

	public void setDeparture(String departure) {
		this.departure = departure;
	}

	public String getDepartureDateTime() {
		return departureDateTime;
	}

	public void setDepartureDateTime(String departureDateTime) {
		this.departureDateTime = departureDateTime;
	}

	public String getArrival() {
		return arrival;
	}

	public void setArrival(String arrival) {
		this.arrival = arrival;
	}

	public String getArrivalDateTime() {
		return arrivalDateTime;
	}

	public void setArrivalDateTime(String arrivalDateTime) {
		this.arrivalDateTime = arrivalDateTime;
	}

	public String getViaPoints() {
		return viaPoints;
	}

	public void setViaPoints(String viaPoints) {
		this.viaPoints = viaPoints;
	}

	public String getModelNumber() {
		return modelNumber;
	}

	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}

	public String getSegmentAllocation() {
		return segmentAllocation;
	}

	public void setSegmentAllocation(String segmentAllocation) {
		this.segmentAllocation = segmentAllocation;
	}

	public String getSegmentSold() {
		return segmentSold;
	}

	public void setSegmentSold(String segmentSold) {
		this.segmentSold = segmentSold;
	}

	public String getSegmentOnHold() {
		return segmentOnHold;
	}

	public void setSegmentOnHold(String segmentOnHold) {
		this.segmentOnHold = segmentOnHold;
	}

	public int getFlightId() {
		return flightId;
	}

	public void setFlightId(int flightId) {
		this.flightId = flightId;
	}

	public List<String[]> getCosList() {
		return cosList;
	}

	public void setCosList(List<String[]> cosList) {
		this.cosList = cosList;
	}

	public String getFlightStatus() {
		return flightStatus;
	}

	public void setFlightStatus(String flightStatus) {
		this.flightStatus = flightStatus;
	}

	public String getDefaultCabinClass() {
		return defaultCabinClass;
	}

	public void setDefaultCabinClass(String defaultCabinClass) {
		this.defaultCabinClass = defaultCabinClass;
	}

	public Integer getOverlappingFlightId() {
		return overlappingFlightId;
	}

	public void setOverlappingFlightId(Integer overlappingFlightId) {
		this.overlappingFlightId = overlappingFlightId;
	}

	public String getAvailableSeatKilometers() {
		return availableSeatKilometers;
	}

	public void setAvailableSeatKilometers(String availableSeatKilometers) {
		this.availableSeatKilometers = availableSeatKilometers;
	}

	public Integer getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(Integer scheduleId) {
		this.scheduleId = scheduleId;
	}

	public String getSeatFactor() {
		return seatFactor;
	}

	public void setSeatFactor(String seatFactor) {
		this.seatFactor = seatFactor;
	}

	public String getFromFromPage() {
		return fromFromPage;
	}

	public void setFromFromPage(String fromFromPage) {
		this.fromFromPage = fromFromPage;
	}

	public String getLowestPubFare() {
		return lowestPubFare;
	}

	public void setLowestPubFare(String lowestPubFare) {
		this.lowestPubFare = lowestPubFare;
	}
}
