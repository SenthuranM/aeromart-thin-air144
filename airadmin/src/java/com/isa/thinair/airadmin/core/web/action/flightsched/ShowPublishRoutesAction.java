package com.isa.thinair.airadmin.core.web.action.flightsched;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.handler.master.ShowPublishRoutesRH;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Results({ @Result(name = AdminStrutsConstants.AdminAction.SUCCESS, value = AdminStrutsConstants.AdminJSP.GDS_PUBLISH_ROUTES),
		@Result(name = AdminStrutsConstants.AdminAction.ERROR, value = AdminStrutsConstants.AdminJSP.ERROR_JSP) })
public class ShowPublishRoutesAction extends BaseRequestAwareAction {

	public String execute() {

		return ShowPublishRoutesRH.execute(request);
	}

}
