package com.isa.thinair.airadmin.core.web.handler.reports;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants.ReportFormatType;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * 
 * @author asiri
 * 
 */
public class VoidReservationDetailsRH extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(VoidReservationDetailsRH.class);

	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();

	private static final String PARAM_USERS = "hdnUsers";

	private static final String PARAM_MODE = "hdnMode";

	private static final String PARAM_AGENTS = "hdnAgents";

	private static final String PARAM_AGENCIES = "selAgencies";

	private static final String PARAM_CHK_TA = "chkTAs";

	private static final String PARAM_CHK_CO = "chkCOs";

	private static final String PARAM_DEP_FROM_DATE = "txtDepFromDate";

	private static final String PARAM_DEP_TO_DATE = "txtDepToDate";

	private static final String PARAM_PNR = "txtPnrNumber";

	private static final String PARAM_REPORT_OPTION = "radReportOption";

	public static String execute(HttpServletRequest request, HttpServletResponse response) {

		String forward = AdminStrutsConstants.AdminAction.SUCCESS;

		Properties prop = getProperties(request);

		try {
			setDisplayData(prop, request);
			if (prop.getProperty(PARAM_MODE) != null && prop.getProperty(PARAM_MODE).equals(WebConstants.ACTION_VIEW)) {
				setReportView(prop, request, response);
			}
		} catch (ModuleException ex) {
			forward = AdminStrutsConstants.AdminAction.ERROR;
			log.error("VoidReservationDetailsRH setReportView Failed " + ex.getMessageString());
		} catch (Exception e) {
			forward = AdminStrutsConstants.AdminAction.ERROR;
			log.error("Error in VoidReservationDetailsRH execute()" + e.getMessage());
		}

		return forward;
	}

	private static void setDisplayData(Properties prop, HttpServletRequest request) throws ModuleException {
		setClientErrors(request);
		setAgentTypes(request);
		setGSAMultiSelectList(prop, request);
		setUserMultiSelectList(prop, request, false);
	}

	protected static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	protected static void setAgentTypes(HttpServletRequest request) throws ModuleException {
		String strATList = SelectListGenerator.createAgentTypeList_SG();
		request.setAttribute(WebConstants.REQ_HTML_AGENTTYPE_LIST, strATList);
	}

	protected static void setGSAMultiSelectList(Properties prop, HttpServletRequest request) throws ModuleException {
		boolean blnWithTAs = false;
		boolean blnWithCOs = false;
		if (!"".equals(prop.getProperty(PARAM_CHK_TA))) {
			blnWithTAs = (prop.getProperty(PARAM_CHK_TA).equals("on") ? true : false);
		}
		if (!"".equals(prop.getProperty(PARAM_CHK_CO))) {
			blnWithCOs = (prop.getProperty(PARAM_CHK_CO).equals("on") ? true : false);
		}
		String strList = ReportsHTMLGenerator.createAgentGSAMultiSelect(prop.getProperty(PARAM_AGENCIES), blnWithTAs, blnWithCOs);
		request.setAttribute(WebConstants.REQ_HTML_GSA_LIST, strList);
	}

	protected static void setUserMultiSelectList(Properties prop, HttpServletRequest request, boolean loadFromAgent)
			throws ModuleException {
		String strAgents = null;
		if (!prop.getProperty(PARAM_AGENTS).equals("")) {
			strAgents = prop.getProperty(PARAM_AGENTS);
		}

		String strList = ReportsHTMLGenerator.createUserAgentMultiSelect(strAgents);
		request.setAttribute(WebConstants.REQ_HTML_AGENT_USER_LIST, strList);
	}

	protected static void setReportView(Properties prop, HttpServletRequest request, HttpServletResponse response)
			throws ModuleException {
		ReportsSearchCriteria search = null;
		ResultSet resultSet = null;
		ArrayList<String> lstSegments = new ArrayList<String>();

		String id = "UC_REPM_082";
		String strLogo = AppSysParamsUtil.getReportLogo(false);
		String image = "../../images/" +  AppSysParamsUtil.getReportLogo(true);
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);
		String reportTemplate = "VoidReservationDetailsReport.jasper";
		String reportNameStr = "VoidReservationDetailsReport";

		search = new ReportsSearchCriteria();

		ArrayList<String> userCol = new ArrayList<String>();
		String userArr[] = prop.getProperty(PARAM_USERS).split(",");
		for (int r = 0; r < userArr.length; r++) {
			userCol.add(userArr[r]);
		}

		search.setUsers(userCol);

		if (strlive != null) {
			if (strlive.equals(WebConstants.REP_LIVE)) {
				search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
			} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
				search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
			}
		}

		if (isNotEmptyOrNull(prop.getProperty(PARAM_DEP_FROM_DATE)) && isNotEmptyOrNull(prop.getProperty(PARAM_DEP_TO_DATE))) {
			String strFromDate = prop.getProperty(PARAM_DEP_FROM_DATE);
			String strToDate = prop.getProperty(PARAM_DEP_TO_DATE);
			search.setDateRangeFrom(strFromDate);
			search.setDateRangeTo(strToDate);
		}

//		if (isNotEmptyOrNull(prop.getProperty(PARAM_PNR))) {
//			search.setPnr(prop.getProperty(PARAM_PNR));
//		}

		ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
				ReportsHTMLGenerator.getReportTemplate(reportTemplate));
		resultSet = ModuleServiceLocator.getDataExtractionBD().getVoidReservationDetailsReportData(search);

		Map<String, Object> parameters = new HashMap<String, Object>();

		parameters.put("FROM_DATE", prop.getProperty(PARAM_DEP_FROM_DATE));
		parameters.put("TO_DATE", prop.getProperty(PARAM_DEP_TO_DATE));
		parameters.put("ID", id);
		parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);

		// To provide Report Format Options
		String reportNumFormat = request.getParameter("radRptNumFormat");
		ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

		if (prop.getProperty(PARAM_REPORT_OPTION).trim().equals(WebConstants.REPORT_HTML)) {
			parameters.put("IMG", image);
			ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport(WebConstants.REPORT_REF, parameters, resultSet, null,
					null, response);
		} else if (prop.getProperty(PARAM_REPORT_OPTION).trim().equals(WebConstants.REPORT_PDF)) {
			image = ReportsHTMLGenerator.getReportTemplate(strLogo);
			parameters.put("IMG", image);
			response.addHeader("Content-Disposition",
					String.format("attachment;filename=%s" + ReportFormatType.PDF_FORMAT, reportNameStr));
			ModuleServiceLocator.getReportingFrameworkBD().createPDFReport(WebConstants.REPORT_REF, parameters, resultSet,
					response);

		} else if (prop.getProperty(PARAM_REPORT_OPTION).trim().equals(WebConstants.REPORT_EXCEL)) {

			response.reset();
			response.addHeader("Content-Disposition",
					String.format("attachment;filename=%s" + ReportFormatType.EXCEL_FORMAT, reportNameStr));
			ModuleServiceLocator.getReportingFrameworkBD().createXLSReport(WebConstants.REPORT_REF, parameters, resultSet,
					response);

		} else if (prop.getProperty(PARAM_REPORT_OPTION).trim().equals(WebConstants.REPORT_CSV)) {

			response.reset();
			response.addHeader("Content-Disposition", "attachment;filename=VoidReservationDetailsReport.csv");
			ModuleServiceLocator.getReportingFrameworkBD().createCSVReport(WebConstants.REPORT_REF, parameters, resultSet,
					response);

		}
	}

	protected static Properties getProperties(HttpServletRequest request) {
		Properties prop = new Properties();

		String selAgencies = request.getParameter(PARAM_AGENCIES);
		String hdnAgents = request.getParameter(PARAM_AGENTS);
		String chkTAs = request.getParameter(PARAM_CHK_TA);
		String chkCOs = request.getParameter(PARAM_CHK_CO);
		String hdnMode = request.getParameter(PARAM_MODE);
		String hdnUsers = request.getParameter(PARAM_USERS);
		String depFromDate = request.getParameter(PARAM_DEP_FROM_DATE);
		String depToDate = request.getParameter(PARAM_DEP_TO_DATE);
		//String pnrNumber = request.getParameter(PARAM_PNR);
		String reportOption = request.getParameter(PARAM_REPORT_OPTION);

		prop.setProperty(PARAM_AGENCIES, AiradminUtils.getNotNullString(selAgencies));
		prop.setProperty(PARAM_AGENTS, AiradminUtils.getNotNullString(hdnAgents));
		prop.setProperty(PARAM_CHK_TA, AiradminUtils.getNotNullString(chkTAs));
		prop.setProperty(PARAM_CHK_CO, AiradminUtils.getNotNullString(chkCOs));
		prop.setProperty(PARAM_MODE, AiradminUtils.getNotNullString(hdnMode));
		prop.setProperty(PARAM_USERS, AiradminUtils.getNotNullString(hdnUsers));
		prop.setProperty(PARAM_DEP_FROM_DATE, AiradminUtils.getNotNullString(depFromDate));
		prop.setProperty(PARAM_DEP_TO_DATE, AiradminUtils.getNotNullString(depToDate));
		//prop.setProperty(PARAM_PNR, AiradminUtils.getNotNullString(pnrNumber));
		prop.setProperty(PARAM_REPORT_OPTION, AiradminUtils.getNotNullString(reportOption));

		return prop;
	}

	private static boolean isNotEmptyOrNull(String str) {
		return !((str == null) || str.trim().equals("") || str.trim().equals("-1"));
	}
}
