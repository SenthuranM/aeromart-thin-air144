/**
 * 
 */
package com.isa.thinair.airadmin.core.web.v2.handler.master;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airpricing.api.criteria.BaggageTemplateSearchCriteria;
import com.isa.thinair.airpricing.api.dto.BaggageTemplateDTO;
import com.isa.thinair.airpricing.api.model.BaggageTemplate;
import com.isa.thinair.airpricing.api.model.DefaultAnciTemplate;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author mano
 * 
 */
public class BaggageTemplateRH {

	public static Page<BaggageTemplate> searchBaggageTemplate(int pageNo, BaggageTemplateSearchCriteria criteria)
			throws ModuleException {
		return ModuleServiceLocator.getChargeBD().getBaggageTemplates(criteria, (pageNo - 1) * 20, 20);
	}

	public static void saveBaggageTemplate(BaggageTemplateDTO baggageTemplateDTO) throws ModuleException {
		ModuleServiceLocator.getChargeBD().saveBaggageTemplate(baggageTemplateDTO, null);
	}

	public static void deleteBaggageTemplate(BaggageTemplateDTO baggageTemplateDTO) throws ModuleException {
		ModuleServiceLocator.getChargeBD().deleteBaggageTemplate(baggageTemplateDTO);
	}
	
	public static void checkTemplateAttachedToRouteWiseDefAnciTempl(int templateId) throws ModuleException{
		ModuleServiceLocator.getCommonAncillaryServiceBD().checkTemplateAttachedToRouteWiseDefAnciTempl(DefaultAnciTemplate.ANCI_TEMPLATES.BAGGAGE, templateId);	
	}
}
