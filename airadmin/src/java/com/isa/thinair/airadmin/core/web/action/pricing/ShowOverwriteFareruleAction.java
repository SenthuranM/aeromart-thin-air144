package com.isa.thinair.airadmin.core.web.action.pricing;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Results({ @Result(name = AdminStrutsConstants.AdminAction.SUCCESS, value = AdminStrutsConstants.AdminJSP.FARE_OVERWRITE_JSP),
		@Result(name = AdminStrutsConstants.AdminAction.ERROR, value = AdminStrutsConstants.AdminJSP.ERROR_JSP) })
public class ShowOverwriteFareruleAction extends BaseRequestAwareAction {

	public String execute() throws Exception {
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;
		String fareRuleCode = request.getParameter("frc");
		String fareRuleId = request.getParameter("frId");
		String rqFrom = request.getParameter("rqFrom");
		String version = request.getParameter("version");
		String localCurrency = request.getParameter("locCurrency");

		if (localCurrency == null || localCurrency.equals("null"))
			localCurrency = "";

		request.setAttribute("requestFrom", rqFrom);
		request.setAttribute("fareruleCode", fareRuleCode);
		request.setAttribute("fareruleId", fareRuleId);
		request.setAttribute("ruleVersion", version);
		request.setAttribute("ruleLocalCurrency", localCurrency);
		return forward;
	}

}
