package com.isa.thinair.airadmin.core.web.generator.master;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.isa.thinair.airmaster.api.model.Airport;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.util.CommonUtil;

public class DemoHG {

	/**
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static String createAirportList(Collection collection) throws ModuleException {
		StringBuffer strbBuffer = new StringBuffer();

		Iterator iterator = collection.iterator();
		strbBuffer.append("arrAirports = new Array();");
		while (iterator.hasNext()) {
			Airport airport = (Airport) iterator.next();
			strbBuffer.append("arrAirports.push(new Array('" + airport.getAirportCode() + "','" + airport.getAirportName()
					+ "'));\n");
		}

		return strbBuffer.toString();
	}

	/**
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static String getDashBoardRevenueData(Collection collection) throws ModuleException {
		StringBuffer strbBuffer = new StringBuffer();
		String[] arrData = new String[4];

		arrData[0] = "$383,141.21";
		arrData[1] = "$332,216.21";
		arrData[2] = "$123,120.00";
		arrData[3] = "$42,216.02";

		if (collection != null && collection.size() > 0) {
			Object[] objects = collection.toArray();
			arrData = (String[]) objects[0];
		}

		strbBuffer.append("jsRevenue = ({revenue:\"" + arrData[0] + "\", ");
		strbBuffer.append("	 			fare:\"" + arrData[1] + "\", ");
		strbBuffer.append("	 			tax:\"" + arrData[2] + "\", ");
		strbBuffer.append("  			surcharges:\"" + arrData[3] + "\"});\n");
		return strbBuffer.toString();
	}

	/**
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static String getDashBoardRevenueBreakDown(Collection collection) throws ModuleException {
		StringBuffer strbBuffer = new StringBuffer();

		String strMonths = "";
		String[][] arrValues = new String[2][6];
		arrValues[0][0] = "0";
		arrValues[0][1] = "0";
		arrValues[0][2] = "0";
		arrValues[0][3] = "0";
		arrValues[0][4] = "0";
		arrValues[0][5] = "0";

		arrValues[1][0] = "0";
		arrValues[1][1] = "0";
		arrValues[1][2] = "0";
		arrValues[1][3] = "0";
		arrValues[1][4] = "0";
		arrValues[1][5] = "0";

		Float fltHightestValue = new Float(0);
		if (collection != null && collection.size() > 0) {
			Object[] objects = collection.toArray();
			int intLen = objects.length;
			for (int i = 0; i < intLen; i++) {
				String[] arrData = (String[]) objects[i];

				arrValues[0][i] = arrData[0];
				arrValues[1][i] = arrData[1];

				if (i != 0) {
					strMonths += "|";
				}
				strMonths += arrData[2];

				if (Double.parseDouble(arrData[0]) > fltHightestValue) {
					fltHightestValue = Float.valueOf(arrData[0]);
				}

				if (Double.parseDouble(arrData[1]) > fltHightestValue) {
					fltHightestValue = Float.valueOf(arrData[1]);
				}
			}
		}

		// TODO
		// Data is not sorted order
		// hard coded the month to display it as in sorted order

		strMonths = "May|Jun|Jul|Aug|Sep|Oct";
		fltHightestValue = fltHightestValue + 10;
		strbBuffer.append("jsRevGraphData = ({months:\"" + strMonths + "\",");
		strbBuffer.append("					highestValue:" + StringUtils.split(fltHightestValue.toString(), ".")[0] + ",");
		strbBuffer.append("					valuesTypeWise:");
		strbBuffer.append("								[");
		strbBuffer.append("									{month1:" + arrValues[0][0] + ", month2:" + arrValues[0][1] + ", month3:" + arrValues[0][2]
				+ ", month4:" + arrValues[0][3] + ", month5:" + arrValues[0][4] + ", month6:" + arrValues[0][5] + "},");
		strbBuffer.append("									{month1:" + arrValues[1][0] + ", month2:" + arrValues[1][1] + ", month3:" + arrValues[1][2]
				+ ", month4:" + arrValues[1][3] + ", month5:" + arrValues[1][4] + ", month6:" + arrValues[1][5] + "}");
		strbBuffer.append("								]");
		strbBuffer.append("				});\n");

		return strbBuffer.toString();
	}

	/**
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static String getDashBoardYTD(Collection collection) throws ModuleException {
		StringBuffer strbBuffer = new StringBuffer();

		if (collection != null && collection.size() > 0) {
			Object[] objects = collection.toArray();
			String[] arrObj = (String[]) objects[0];

			strbBuffer.append("jsBkgYTD = ({budget:5000000, value:" + arrObj[0] + "});\n");
		}

		return strbBuffer.toString();
	}

	/**
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static String getDashBoardSalesChannelWiseInfo(Collection collection) throws ModuleException {
		StringBuffer strbBuffer = new StringBuffer();

		String[][] arrData = new String[6][3];
		arrData[0][0] = "0"; // AA - Web
		arrData[1][0] = "0"; // CO
		arrData[2][0] = "0"; // GDS
		arrData[3][0] = "0"; // GSA
		arrData[4][0] = "0"; // TA
		arrData[5][0] = "0"; // SO - Call Center

		Float fltHightestValue = new Float(0);
		if (collection != null && collection.size() > 0) {
			Object[] objects = collection.toArray();
			int intLen = objects.length;
			for (int i = 0; i < intLen; i++) {
				String[] arrTypeValues = (String[]) objects[i];

				if (arrTypeValues[1].equals("AA")) {
					arrData[0][0] = arrTypeValues[0];

				} else if (arrTypeValues[1].equals("CO")) {
					arrData[1][0] = arrTypeValues[0];

				} else if (arrTypeValues[1].equals("GDS")) {
					arrData[2][0] = arrTypeValues[0];

				} else if (arrTypeValues[1].equals("GSA")) {
					arrData[3][0] = arrTypeValues[0];

				} else if (arrTypeValues[1].equals("TA")) {
					arrData[4][0] = arrTypeValues[0];

				} else if (arrTypeValues[1].equals("SO")) {
					arrData[5][0] = arrTypeValues[0];
				}

				if (Double.parseDouble(arrTypeValues[0]) > fltHightestValue) {
					fltHightestValue = Float.valueOf(arrTypeValues[0]);
				}
			}
		}

		fltHightestValue = fltHightestValue + 1000;

		strbBuffer.append("jsSalesChnl = ({");
		strbBuffer.append("				highestValue:" + StringUtils.split(fltHightestValue.toString(), ".")[0] + ",");
		strbBuffer.append("				summary:[");
		strbBuffer.append("							{type:\"AA\", value:" + arrData[0][0] + ", perValue:\"+ 10.00%\", upDown:\"U\"},");
		strbBuffer.append("							{type:\"CO\",  value:" + arrData[1][0] + ", perValue:\"+ 0.02%\", upDown:\"U\"},");
		strbBuffer.append("							{type:\"GDS\", value:" + arrData[2][0] + ", perValue:\"- 2.00%\", upDown:\"D\"},");
		strbBuffer.append("							{type:\"GSA\", value:" + arrData[3][0] + ", perValue:\"+ 0.18%\", upDown:\"U\"},");
		strbBuffer.append("							{type:\"TA\", value:" + arrData[4][0] + ", perValue:\"+ 13.00%\", upDown:\"U\"},");
		strbBuffer.append("							{type:\"SO\", value:" + arrData[5][0] + ", perValue:\"+ 16.00%\", upDown:\"U\"}");
		strbBuffer.append("						]");
		strbBuffer.append("			});\n");

		return strbBuffer.toString();
	}

	/**
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static String getDistPerformanceOfPortals() throws ModuleException {
		StringBuffer strbBuffer = new StringBuffer();

		strbBuffer.append("jsPP = [");
		strbBuffer
				.append("		{porta:\"expedia.com\", sales:\"$52,400,000\", cont:\"26.2%\", bkg:\"137,281\", conv:\"97%\", comm:\"$52,400\"},");
		strbBuffer
				.append("		{porta:\"makemytrip.com\", sales:\"$1,000,000\", cont:\"0.5%\", bkg:\"4,152\", conv:\"72%\", comm:\"$1,000\"},");
		strbBuffer
				.append("		{porta:\"booknow.com\", sales:\"$10,200,000\", cont:\"5.1%\", bkg:\"45,535\", conv:\"69%\", comm:\"$10,200\"},");
		strbBuffer
				.append("		{porta:\"thomascook.com\", sales:\"$4,200,000\", cont:\"2.1%\", bkg:\"17,077\", conv:\"75%\", comm:\"$4,200\"},");
		strbBuffer
				.append("		{porta:\"akbartravels.com\", sales:\"$4,000,000\", cont:\"2.3%\", bkg:\"15,719\", conv:\"92%\", comm:\"$4,000\"},");
		strbBuffer
				.append("		{porta:\"expedia.com\", sales:\"$3,800,000\", cont:\"2%\", bkg:\"14,473\", conv:\"86%\", comm:\"$3,800\"},");
		strbBuffer
				.append("		{porta:\"travelocity.com\", sales:\"$2,400,000\", cont:\"1.9%\", bkg:\"10,706\", conv:\"87%\", comm:\"$2,400\"},");
		strbBuffer
				.append("		{porta:\"orbitz.com\", sales:\"$3,400,000\", cont:\"1.2%\", bkg:\"7,648\", conv:\"82%\", comm:\"$3,400\"},");
		strbBuffer
				.append("		{porta:\"tripadvisor.com\", sales:\"$2,200,000\", cont:\"1.7%\", bkg:\"11,708\", conv:\"89%\", comm:\"$2,200\"}");
		strbBuffer.append("		];\n");

		return strbBuffer.toString();
	}

	/**
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static String getDistBookingByChannel() throws ModuleException {
		StringBuffer strbBuffer = new StringBuffer();

		strbBuffer.append("jsBkgChnl = [");
		strbBuffer.append("					{cntry:\"Egypt\", cities:4, flts:2, agents:5},");
		strbBuffer.append("					{cntry:\"Aleppo\", cities:16, flts:5, agents:\"\"},");
		strbBuffer.append("					{cntry:\"Beirut\", cities:53, flts:13, agents:185},");
		strbBuffer.append("					{cntry:\"Sri Lanka\", cities:3, flts:2, agents:8},");
		strbBuffer.append("					{cntry:\"Qatar\", cities:4, flts:\"\", agents:7},");
		strbBuffer.append("					{cntry:\"Turkey\", cities:1, flts:1, agents:3},");
		strbBuffer.append("					{cntry:\"Nepal\", cities:11, flts:7, agents:21},");
		strbBuffer.append("					{cntry:\"Oman\", cities:11, flts:1, agents:\"\"},");
		strbBuffer.append("					{cntry:\"India\", cities:\"\", flts:2, agents:4},");
		strbBuffer.append("					{cntry:\"Pakistan\", cities:\"\", flts:\"\", agents:1},");
		strbBuffer.append("					{cntry:\"Sharjah\", cities:388, flts:25, agents:1364},");
		strbBuffer.append("					{cntry:\"Tehran\", cities:1, flts:2, agents:3}");
		strbBuffer.append("				];\n");

		return strbBuffer.toString();
	}

	/**
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static String getDistRevenueByChannel() throws ModuleException {
		StringBuffer strbBuffer = new StringBuffer();

		strbBuffer.append("jsRevChnl = ({");
		strbBuffer.append("				highestValue:190123,");
		strbBuffer.append("				summary:[");
		strbBuffer.append("							{type:\"WEB\", value:78223, perValue:\"+ 12.00%\", upDown:\"U\"},");
		strbBuffer.append("							{type:\"CO\", value:123212, perValue:\"+ 0.02%\", upDown:\"U\"},");
		strbBuffer.append("							{type:\"GDS\", value:180123, perValue:\"- 0.03%\", upDown:\"D\"},");
		strbBuffer.append("							{type:\"PO\", value:55123, perValue:\"- 0.18%\", upDown:\"D\"},");
		strbBuffer.append("							{type:\"TA\", value:8923, perValue:\"+ 14.00%\", upDown:\"U\"},");
		strbBuffer.append("							{type:\"CC\", value:18823, perValue:\"+ 22.00%\", upDown:\"U\"}");
		strbBuffer.append("						]");
		strbBuffer.append("				});\n");

		return strbBuffer.toString();
	}

	/**
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static String getDistBookingByChannelGraph() throws ModuleException {
		StringBuffer strbBuffer = new StringBuffer();

		strbBuffer.append("jsDisBkgChnl = [");
		strbBuffer.append("				{type:\"traelAgent\", ondWise:[");
		strbBuffer.append("												{rowID:0, value:10},");
		strbBuffer.append("												{rowID:1, value:25},");
		strbBuffer.append("												{rowID:2, value:2},");
		strbBuffer.append("												{rowID:3, value:10},");
		strbBuffer.append("												{rowID:4, value:15},");
		strbBuffer.append("												{rowID:5, value:8},");
		strbBuffer.append("												{rowID:6, value:13},");
		strbBuffer.append("												{rowID:7, value:25},");
		strbBuffer.append("												{rowID:8, value:11},");
		strbBuffer.append("												{rowID:9, value:13},");
		strbBuffer.append("												{rowID:10, value:25},");
		strbBuffer.append("												{rowID:11, value:10}");
		strbBuffer.append("											]},");
		strbBuffer.append("				{type:\"web\", ondWise:[");
		strbBuffer.append("												{rowID:0, value:15},");
		strbBuffer.append("												{rowID:1, value:7},");
		strbBuffer.append("												{rowID:2, value:18},");
		strbBuffer.append("												{rowID:3, value:20},");
		strbBuffer.append("												{rowID:4, value:20},");
		strbBuffer.append("												{rowID:5, value:2},");
		strbBuffer.append("												{rowID:6, value:12},");
		strbBuffer.append("												{rowID:7, value:8},");
		strbBuffer.append("												{rowID:8, value:5},");
		strbBuffer.append("												{rowID:9, value:10},");
		strbBuffer.append("												{rowID:10, value:13},");
		strbBuffer.append("												{rowID:11, value:13}");
		strbBuffer.append("											]},");
		strbBuffer.append("				{type:\"corporate\", ondWise:[");
		strbBuffer.append("												{rowID:0, value:20},");
		strbBuffer.append("												{rowID:1, value:3},");
		strbBuffer.append("												{rowID:2, value:15},");
		strbBuffer.append("												{rowID:3, value:30},");
		strbBuffer.append("												{rowID:4, value:30},");
		strbBuffer.append("												{rowID:5, value:10},");
		strbBuffer.append("												{rowID:6, value:15},");
		strbBuffer.append("												{rowID:7, value:9},");
		strbBuffer.append("												{rowID:8, value:30},");
		strbBuffer.append("												{rowID:9, value:20},");
		strbBuffer.append("												{rowID:10, value:10},");
		strbBuffer.append("												{rowID:11, value:12}");
		strbBuffer.append("											]},");
		strbBuffer.append("				{type:\"GDS\", ondWise:[");
		strbBuffer.append("												{rowID:0, value:35},");
		strbBuffer.append("												{rowID:1, value:35},");
		strbBuffer.append("												{rowID:2, value:15},");
		strbBuffer.append("												{rowID:3, value:16},");
		strbBuffer.append("												{rowID:4, value:12},");
		strbBuffer.append("												{rowID:5, value:13},");
		strbBuffer.append("												{rowID:6, value:20},");
		strbBuffer.append("												{rowID:7, value:2},");
		strbBuffer.append("												{rowID:8, value:40},");
		strbBuffer.append("												{rowID:9, value:24},");
		strbBuffer.append("												{rowID:10, value:12},");
		strbBuffer.append("												{rowID:11, value:10}");
		strbBuffer.append("											]},");
		strbBuffer.append("				{type:\"cc\", ondWise:[");
		strbBuffer.append("												{rowID:0, value:15},");
		strbBuffer.append("												{rowID:1, value:5},");
		strbBuffer.append("												{rowID:2, value:18},");
		strbBuffer.append("												{rowID:3, value:15},");
		strbBuffer.append("												{rowID:4, value:8},");
		strbBuffer.append("												{rowID:5, value:10},");
		strbBuffer.append("												{rowID:6, value:14},");
		strbBuffer.append("												{rowID:7, value:30},");
		strbBuffer.append("												{rowID:8, value:5},");
		strbBuffer.append("												{rowID:9, value:20},");
		strbBuffer.append("												{rowID:10, value:12},");
		strbBuffer.append("												{rowID:11, value:20}");
		strbBuffer.append("											]},");
		strbBuffer.append("				{type:\"portals\", ondWise:[");
		strbBuffer.append("												{rowID:0, value:5},");
		strbBuffer.append("												{rowID:1, value:30},");
		strbBuffer.append("												{rowID:2, value:32},");
		strbBuffer.append("												{rowID:3, value:10},");
		strbBuffer.append("												{rowID:4, value:20},");
		strbBuffer.append("												{rowID:5, value:10},");
		strbBuffer.append("												{rowID:6, value:12},");
		strbBuffer.append("												{rowID:7, value:40},");
		strbBuffer.append("												{rowID:8, value:13},");
		strbBuffer.append("												{rowID:9, value:13},");
		strbBuffer.append("												{rowID:10, value:8},");
		strbBuffer.append("												{rowID:11, value:30}");
		strbBuffer.append("											]}");
		strbBuffer.append("				];\n");

		return strbBuffer.toString();
	}

	/**
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static String getRegionalYearlySales(Collection collection) throws ModuleException {
		StringBuffer strbBuffer = new StringBuffer();

		// TODO
		// Remove the hard coded values

		Float fltHightestValue = new Float(0);
		String[][] arrYearlyData = new String[3][2];
		arrYearlyData[0][0] = "30";
		arrYearlyData[0][1] = "2006";

		arrYearlyData[1][0] = "80";
		arrYearlyData[1][1] = "2007";

		arrYearlyData[2][0] = "50";
		arrYearlyData[2][1] = "2008";

		if (collection != null && collection.size() > 0) {
			Object[] objects = collection.toArray();
			int intLen = objects.length;
			for (int i = 0; i < intLen; i++) {
				String[] arrData = (String[]) objects[i];
				arrYearlyData[i][0] = arrData[0];
				arrYearlyData[i][1] = arrData[1];

				if (Double.parseDouble(arrData[0]) > fltHightestValue) {
					fltHightestValue = Float.valueOf(arrData[0]);
				}
			}

			fltHightestValue = fltHightestValue + 1000000;
		} else {
			fltHightestValue = new Float(100);
		}

		strbBuffer.append("jsRegionYearlySales = ({period:\"" + arrYearlyData[0][1] + "|" + arrYearlyData[1][1] + "|"
				+ arrYearlyData[2][1] + "\",");
		strbBuffer.append("	 						highestValue:" + StringUtils.split(fltHightestValue.toString(), ".")[0] + ",");
		strbBuffer.append("	 						value :");
		strbBuffer.append("								[");
		strbBuffer.append("									{year1:" + arrYearlyData[0][0] + ", year2:" + arrYearlyData[1][0] + ", year3:"
				+ arrYearlyData[2][0] + "}");
		strbBuffer.append("								]");
		strbBuffer.append("							});\n");

		return strbBuffer.toString();
	}

	/**
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static String getRegionalMonthRolling() throws ModuleException {
		StringBuffer strbBuffer = new StringBuffer();

		strbBuffer.append("jsRegion6MonthSales = ({months:\"Mar|Apr|May|Jun|Jul|Aug\",");
		strbBuffer.append("		 					highestValue:2400000,");
		strbBuffer.append("	 						valuesRegionWise :");
		strbBuffer.append("							[");
		strbBuffer
				.append("								{month1:287505, month2:290000, month3:570213, month4:451020, month5:1750212, month6:2300400},");
		strbBuffer
				.append("								{month1:578750, month2:730000, month3:420213, month4:1431020, month5:860212, month6:1750400},");
		strbBuffer
				.append("								{month1:867505, month2:1142000, month3:1650213, month4:1811020, month5:350212, month6:2220400}");
		strbBuffer.append("							]");
		strbBuffer.append("						});\n");

		return strbBuffer.toString();
	}

	/**
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static String getRegionalProductSales() throws ModuleException {
		StringBuffer strbBuffer = new StringBuffer();

		strbBuffer.append("jsDisProd = [");
		strbBuffer.append("			 {region:\"Africa\",");
		strbBuffer.append("						markets:[");
		strbBuffer
				.append("									{location:\"Morocco\", desc:\"Total\", target:\"115,011\", actual:\"62,621\", variance:\"21,212\", variancePer:\"21%\", YTD:\"0\", YTDVar:\"0\", seatFact:\"0\", schCapVar:\"0\",");
		strbBuffer.append("										details:[");
		strbBuffer
				.append("													{agent:\"Agent #1\", target:\"15,121\", actual:\"62,621\", variance:\"21,212\", variancePer:\"21%\", YTD:\"0\", YTDVar:\"0\", seatFact:\"0\", schCapVar:\"0\"},");
		strbBuffer
				.append("													{agent:\"Agent #2\", target:\"35,421\", actual:\"62,621\", variance:\"21,212\", variancePer:\"21%\", YTD:\"0\", YTDVar:\"0\", seatFact:\"0\", schCapVar:\"0\"}");
		strbBuffer.append("												]");
		strbBuffer.append("									},");
		strbBuffer
				.append("									{location:\"Kenya\", desc:\"Total\", target:\"12,721\", actual:\"62,621\", variance:\"21,212\", variancePer:\"21%\", YTD:\"0\", YTDVar:\"0\", seatFact:\"0\", schCapVar:\"0\",");
		strbBuffer.append("										details:[");
		strbBuffer
				.append("													{agent:\"Agent #3\", target:\"13,011\", actual:\"62,621\", variance:\"21,212\", variancePer:\"21%\", YTD:\"0\", YTDVar:\"0\", seatFact:\"0\", schCapVar:\"0\"},");
		strbBuffer
				.append("													{agent:\"Agent #4\", target:\"23,011\", actual:\"62,621\", variance:\"21,212\", variancePer:\"21%\", YTD:\"0\", YTDVar:\"0\", seatFact:\"0\", schCapVar:\"0\"}");
		strbBuffer.append("												]");
		strbBuffer.append("									}");
		strbBuffer.append("								]},");
		strbBuffer.append("	 		{region:\"Middle East\", ");
		strbBuffer.append("							markets:[");
		strbBuffer
				.append("									{location:\"Sharjah\", desc:\"Total\", target:\"115,011\", actual:\"62,621\", variance:\"21,212\", variancePer:\"21%\", YTD:\"0\", YTDVar:\"0\", seatFact:\"0\", schCapVar:\"0\",");
		strbBuffer.append("										details:[");
		strbBuffer
				.append("													{agent:\"Agent #5\", target:\"115,011\", actual:\"62,621\", variance:\"21,212\", variancePer:\"21%\", YTD:\"0\", YTDVar:\"0\", seatFact:\"0\", schCapVar:\"0\"},");
		strbBuffer
				.append("													{agent:\"Agent #6\", target:\"115,011\", actual:\"62,621\", variance:\"21,212\", variancePer:\"21%\", YTD:\"0\", YTDVar:\"0\", seatFact:\"0\", schCapVar:\"0\"}");
		strbBuffer.append("												]");
		strbBuffer.append("									},");
		strbBuffer
				.append("									{location:\"Doha\", desc:\"Total\", target:\"115,011\", actual:\"62,621\", variance:\"21,212\", variancePer:\"21%\", YTD:\"0\", YTDVar:\"0\", seatFact:\"0\", schCapVar:\"0\",");
		strbBuffer.append("										details:[");
		strbBuffer
				.append("													{agent:\"Agent #7\", target:\"115,011\", actual:\"62,621\", variance:\"21,212\", variancePer:\"21%\", YTD:\"0\", YTDVar:\"0\", seatFact:\"0\", schCapVar:\"0\"},");
		strbBuffer
				.append("													{agent:\"Agent #8\", target:\"115,011\", actual:\"62,621\", variance:\"21,212\", variancePer:\"21%\", YTD:\"0\", YTDVar:\"0\", seatFact:\"0\", schCapVar:\"0\"}");
		strbBuffer.append("												]");
		strbBuffer.append("									}");
		strbBuffer.append("								]},");
		strbBuffer.append("			{region:\"Asia\", ");
		strbBuffer.append("						markets:[");
		strbBuffer
				.append("									{location:\"India\", desc:\"Total\", target:\"115,011\", actual:\"62,621\", variance:\"21,212\", variancePer:\"21%\", YTD:\"0\", YTDVar:\"0\", seatFact:\"0\", schCapVar:\"0\",");
		strbBuffer.append("										details:[");
		strbBuffer
				.append("													{agent:\"Agent #9\", target:\"115,011\", actual:\"62,621\", variance:\"21,212\", variancePer:\"21%\", YTD:\"0\", YTDVar:\"0\", seatFact:\"0\", schCapVar:\"0\"},");
		strbBuffer
				.append("													{agent:\"Agent #10\", target:\"115,011\", actual:\"62,621\", variance:\"21,212\", variancePer:\"21%\", YTD:\"0\", YTDVar:\"0\", seatFact:\"0\", schCapVar:\"0\"}");
		strbBuffer.append("												]");
		strbBuffer.append("									},");
		strbBuffer
				.append("									{location:\"Sri Lanka\", desc:\"Total\", target:\"115,011\", actual:\"62,621\", variance:\"21,212\", variancePer:\"21%\", YTD:\"0\", YTDVar:\"0\", seatFact:\"0\", schCapVar:\"0\",");
		strbBuffer.append("										details:[");
		strbBuffer
				.append("													{agent:\"Agent #11\", target:\"115,011\", actual:\"62,621\", variance:\"21,212\", variancePer:\"21%\", YTD:\"0\", YTDVar:\"0\", seatFact:\"0\", schCapVar:\"0\"},");
		strbBuffer
				.append("													{agent:\"Agent #12\", target:\"115,011\", actual:\"62,621\", variance:\"21,212\", variancePer:\"21%\", YTD:\"0\", YTDVar:\"0\", seatFact:\"0\", schCapVar:\"0\"}");
		strbBuffer.append("												]");
		strbBuffer.append("									}");
		strbBuffer.append("								]}");
		strbBuffer.append("		 ];\n");

		return strbBuffer.toString();
	}

	/**
	 * 
	 * @param strMode
	 * @return
	 * @throws ModuleException
	 */
	public static String getRegionalPerformance(String strMode, Collection collection, Collection collectionAvg)
			throws ModuleException {
		StringBuffer strbBuffer = new StringBuffer();

		Object[] objects = collection.toArray();

		if ((strMode == null) || (!strMode.equals("REGIONAL_LP")) || (strMode.equals("TAB_3"))) {
			strbBuffer.append("jsRP = [");
		} else {
			strbBuffer.append("jsLP = [");
		}
		if (collectionAvg != null && collectionAvg.size() > 0) {
			objects = collection.toArray();
			int intLen = objects.length;
			for (int i = 0; i < intLen; i++) {
				String[] arrData = (String[]) objects[i];
				strbBuffer.append("		 {agent:\"" + arrData[1] + "\", location:\"" + arrData[2] + "\", amount:\"" + arrData[3]
						+ "\", address1:\"" + CommonUtil.nullHandler(arrData[4]) + "\", address2:\""
						+ CommonUtil.nullHandler(arrData[5]) + "\", city:\"" + CommonUtil.nullHandler(arrData[6])
						+ "\", country:\"" + CommonUtil.nullHandler(arrData[9]) + "\", telNo:\""
						+ CommonUtil.nullHandler(arrData[10]) + "\", email:\"" + CommonUtil.nullHandler(arrData[11])
						+ "\", contact:\"" + CommonUtil.nullHandler(arrData[12]) + "\", lastSale:\"01/06/2009\"},");
			}
			/*
			 * strbBuffer.append(
			 * "		 {agent:\"Youne Tours\", location:\"MOROCCO\", amount:\"$152,400\", address1:\"Bur dubai\", address2:\"dubai\", city:\"Dubai\", country:\"Morocco\", telNo:\"962123123123\", email:\"test@google.com\", contact:\"Mr. Abu Kadir\", lastSale:\"01/06/2009\"},"
			 * ); strbBuffer.append(
			 * "		 {agent:\"Air Arabia\", location:\"MOROCCO\", amount:\"$132,310\", address1:\"Bur dubai\", address2:\"dubai\", city:\"Dubai\", country:\"Morocco\", telNo:\"962123123123\", email:\"test@google.com\", contact:\"Mr. Silva\", lastSale:\"01/06/2009\"},"
			 * ); strbBuffer.append(
			 * "		 {agent:\"Alma Tours\", location:\"PARIS\", amount:\"$112,492\", address1:\"Bur dubai\", address2:\"dubai\", city:\"Dubai\", country:\"Paris\", telNo:\"962123123123\", email:\"test@google.com\", contact:\"Mr. Abu Kadir\", lastSale:\"01/06/2009\"},"
			 * ); strbBuffer.append(
			 * "		 {agent:\"SACBO\", location:\"MILAN\", amount:\"$105,341\", address1:\"Bur dubai\", address2:\"dubai\", city:\"Dubai\", country:\"Italy\", telNo:\"962123123123\", email:\"test@google.com\", contact:\"Mr. Abu Kadir\", lastSale:\"01/06/2009\"},"
			 * ); strbBuffer.append(
			 * "		 {agent:\"Jedda Tours\", location:\"ITALY\", amount:\"$103,216\", address1:\"Bur dubai\", address2:\"dubai\", city:\"Dubai\", country:\"Italy\", telNo:\"962123123123\", email:\"test@google.com\", contact:\"Mr. Abu Kadir\", lastSale:\"01/06/2009\"},"
			 * ); strbBuffer.append(
			 * "		 {agent:\"Reman Arabia\", location:\"MIDDLE E.\", amount:\"$102,133\", address1:\"Bur dubai\", address2:\"dubai\", city:\"Dubai\", country:\"U.A.E\", telNo:\"962123123123\", email:\"test@google.com\", contact:\"Mr. Abu Kadir\", lastSale:\"01/06/2009\"},"
			 * ); strbBuffer.append(
			 * "		 {agent:\"Ando Voyage\", location:\"INDIA\", amount:\"$100,722\", address1:\"Bur dubai\", address2:\"dubai\", city:\"Dubai\", country:\"India\", telNo:\"962123123123\", email:\"test@google.com\", contact:\"Mr. Abu Kadir\", lastSale:\"01/06/2009\"},"
			 * ); strbBuffer.append(
			 * "		 {agent:\"Cap Tours\", location:\"PAKISTAN\", amount:\"$99,247\", address1:\"Bur dubai\", address2:\"dubai\", city:\"Dubai\", country:\"Pakistan\", telNo:\"962123123123\", email:\"test@google.com\", contact:\"Mr. Abu Kadir\", lastSale:\"01/06/2009\"},"
			 * ); strbBuffer.append(
			 * "		 {agent:\"Nassim Voyages\", location:\"INDIA\", amount:\"$98,143\", address1:\"Bur dubai\", address2:\"dubai\", city:\"Dubai\", country:\"India\", telNo:\"962123123123\", email:\"test@google.com\", contact:\"Mr. Abu Kadir\", lastSale:\"01/06/2009\"},"
			 * ); strbBuffer.append(
			 * "		 {agent:\"Fly Well\", location:\"MILAN\", amount:\"$97,613\", address1:\"Bur dubai\", address2:\"dubai\", city:\"Dubai\", country:\"Italy\", telNo:\"962123123123\", email:\"test@google.com\", contact:\"Mr. Abu Kadir\", lastSale:\"01/06/2009\"}"
			 * );
			 */
		}
		strbBuffer.append("	 ];\n");

		objects = collectionAvg.toArray();
		String strAvgAmt = "$84,984";
		if (collectionAvg != null && collectionAvg.size() > 0) {
			String[] arrData = (String[]) objects[0];
			strAvgAmt = arrData[0];
		}

		strbBuffer.append("intAvgSales = \"" + strAvgAmt + "\";\n");

		return strbBuffer.toString();
	}

	/**
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static String getAccountsRevnueSummary() throws ModuleException {
		StringBuffer strbBuffer = new StringBuffer();

		strbBuffer.append("jsSalesInfo = [");
		strbBuffer.append("				{type:\"Cash Sales\", value:\"12.5 /20 (63 %)\"},");
		strbBuffer.append("				{type:\"Credit Card\", value:\"0.6 /1 (64 %)\"},");
		strbBuffer.append("				{type:\"Bank Guar.\", value:\"0.6 /1 (64 %)\"},");
		strbBuffer.append("				{type:\"Credit Fac.\", value:\"0.6 /1 (64 %)\"},");
		strbBuffer.append("				{type:\"BSP\", value:\"3.2 /5 (64 %)\"}");
		strbBuffer.append("			];\n");

		return strbBuffer.toString();
	}

	/**
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static String getAccountsSalesSummary() throws ModuleException {
		StringBuffer strbBuffer = new StringBuffer();

		strbBuffer.append("jsSalesSummary = [");
		strbBuffer
				.append("						{revenue:\"Flown\", cashSales:\"$100,124.00\", creditSales:\"$50,714.60\", BSP:\"$12,123.15\"},");
		strbBuffer.append("						{revenue:\"Refunds\", cashSales:\"$10,422.65\", creditSales:\"$8,123.21\", BSP:\"$4,127.21\"},");
		strbBuffer
				.append("						{revenue:\"Invoiced\", cashSales:\"$98.516.87\", creditSales:\"$39,241.20\", BSP:\"$9,215.21\"},");
		strbBuffer
				.append("						{revenue:\"Collected\", cashSales:\"$64,256.00\", creditSales:\"$24,711.02\", BSP:\"$7,617.26\"}");
		strbBuffer.append("					];\n");

		return strbBuffer.toString();
	}

	/**
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static String getAccountsCreditInfo() throws ModuleException {
		StringBuffer strbBuffer = new StringBuffer();

		strbBuffer.append("jsAccoCredit = ({creditPaxCount:220, creditValue:\"443,512.02\"});\n");

		return strbBuffer.toString();
	}

	/**
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static String getAccountsOutstandingPaymentsData() throws ModuleException {
		StringBuffer strbBuffer = new StringBuffer();

		strbBuffer.append("jsOutInvoice = [");
		strbBuffer.append("				{invoiceNo:\"INV00123\", date:\"12/02/2009\", amount:\"782.00\"},");
		strbBuffer.append("				{invoiceNo:\"INV00231\", date:\"14/03/2009\", amount:\"78.00\"},");
		strbBuffer.append("				{invoiceNo:\"INV00513\", date:\"18/03/2009\", amount:\"178.00\"},");
		strbBuffer.append("				{invoiceNo:\"INV00713\", date:\"19/04/2009\", amount:\"722.00\"},");
		strbBuffer.append("				{invoiceNo:\"INV00912\", date:\"20/04/2009\", amount:\"714.00\"},");
		strbBuffer.append("				{invoiceNo:\"INV01123\", date:\"22/05/2009\", amount:\"522.00\"},");
		strbBuffer.append("				{invoiceNo:\"INV01129\", date:\"23/06/2009\", amount:\"422.00\"}");
		strbBuffer.append("				];\n");

		return strbBuffer.toString();
	}

	/**
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static String getAccountsOutstandingPayablesData() throws ModuleException {
		StringBuffer strbBuffer = new StringBuffer();

		strbBuffer.append("jsOutPayments = [");
		strbBuffer.append("						{desc:\"Jordan Health tax\", date:\"12/02/2009\", amount:\"7,382.00\"},");
		strbBuffer.append("						{desc:\"EGYPT GOVT TAX 1\", date:\"14/03/2009\", amount:\"8,718.00\"},");
		strbBuffer.append("						{desc:\"SHARJAH APT TAX\", date:\"29/03/2009\", amount:\"2,178.00\"},");
		strbBuffer.append("						{desc:\"Addnl Fuel Surcharge\", date:\"30/04/2009\", amount:\"6,722.00\"},");
		strbBuffer.append("						{desc:\"Delhi UDF\", date:\"25/04/2009\", amount:\"8,814.00\"},");
		strbBuffer.append("						{desc:\"Nepal Departure Tax 2\", date:\"26/05/2009\", amount:\"1,232.00\"},");
		strbBuffer.append("						{desc:\"Bahrain Tax\", date:\"28/06/2009\", amount:\"1,462.00\"}");
		strbBuffer.append("					];\n");

		return strbBuffer.toString();
	}

	/**
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static String getAnsiSalesInfo() throws ModuleException {
		StringBuffer strbBuffer = new StringBuffer();
		strbBuffer.append("jsAnsiChart = ({seat:\"17\", meal:\"17\", ins:\"30\", hala:\"36\"});\n");
		return strbBuffer.toString();
	}

	/**
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static String getAnsiSalesByChannelInfo() throws ModuleException {
		StringBuffer strbBuffer = new StringBuffer();

		strbBuffer.append("jsAnciByChannel = [");
		strbBuffer
				.append("					{desc:\"Seat Selection\", web:\"$1,123.00\", cc:\"$700.00\", agent:\"$900.00\", total:\"$2,723.00\"},");
		strbBuffer
				.append("					{desc:\"Meal Selection\", web:\"$2,123.00\", cc:\"$2,500.00\", agent:\"$1,900.00\", total:\"$6,523.00\"},");
		strbBuffer
				.append("					{desc:\"Insurance\", web:\"$9,123.00\", cc:\"$2,700.00\", agent:\"$4,900.00\", total:\"$16,723.00\"},");
		strbBuffer
				.append("					{desc:\"HALA Service\", web:\"$1,092.00\", cc:\"$8,617.00\", agent:\"$2,909.00\", total:\"$12,168.00\"},");
		strbBuffer
				.append("					{desc:\"T O T A L \", web:\"$13,461.00\", cc:\"$14,067.00\", agent:\"$10,609.00\", total:\"$38,137.00\"}");
		strbBuffer.append("					];\n");

		return strbBuffer.toString();
	}

	/**
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static String getAnsiKPIViewerInfo(String strMode) throws ModuleException {
		StringBuffer strbBuffer = new StringBuffer();

		if ((strMode == null) || (strMode.equals("ANSI_S")) || (strMode.equals("TAB_6"))) {
			strbBuffer.append("jsAnciKPISeat = [");

		} else if (strMode.equals("ANSI_M")) {
			strbBuffer.append("jsAnciKPIMeal = [");

		} else if (strMode.equals("ANSI_I")) {
			strbBuffer.append("jsAnciKPIINS = [");

		} else {
			strbBuffer.append("jsAnciKPIHala = [");
		}

		strbBuffer.append("					{route:\"SHJ/CMB\", revenue:\"$12,300\"},");
		strbBuffer.append("					{route:\"SHJ/BOM\", revenue:\"$12,231\"},");
		strbBuffer.append("					{route:\"DOH/KWI\", revenue:\"$11,132\"},");
		strbBuffer.append("					{route:\"KWI/SHJ\", revenue:\"$9,271\"},");
		strbBuffer.append("					{route:\"SHJ/CMN\", revenue:\"$9,431\"},");
		strbBuffer.append("					{route:\"SHJ/AUH\", revenue:\"$9,122\"},");
		strbBuffer.append("					{route:\"KWI/SHJ\", revenue:\"$8,271\"},");
		strbBuffer.append("					{route:\"SHJ/BEY\", revenue:\"$8,431\"},");
		strbBuffer.append("					{route:\"BEY/KRT\", revenue:\"$7,122\"},");
		strbBuffer.append("					{route:\"KRT/LXR\", revenue:\"$6,231\"}");
		strbBuffer.append("				];\n");

		return strbBuffer.toString();
	}

	/**
	 * 
	 * @return
	 * @throws ModuleException
	 */
	public static String getAgentsCountryCities(Collection collection) throws ModuleException {
		StringBuffer strbBuffer = new StringBuffer();

		strbBuffer.append("jsCntryCity = [");
		if ((collection != null) && (collection.size() > 0)) {
			String[] arrColumns = new String[4];

			arrColumns[0] = "countryCode";
			arrColumns[1] = "countryName";
			arrColumns[2] = "stationCode";
			arrColumns[3] = "stationDesc";

			strbBuffer.append(generateJsonString(collection, arrColumns, 0));
			strbBuffer.append("			];\n");

		} else {
			// TODO
			// Remoe this
			strbBuffer
					.append("				{countryCode:\"AF\", countryName:\"Afghanistan\", stationCode:\"KBL\", stationDesc:\"Kabul\"},");
			strbBuffer
					.append("				{countryCode:\"AM\", countryName:\"Armenia\", stationCode:\"EVN\", stationDesc:\"Yerevan \"},");
			strbBuffer.append("				{countryCode:\"BH\", countryName:\"Bahrain\", stationCode:\"BAH\", stationDesc:\"Bahrain\"},");
			strbBuffer
					.append("				{countryCode:\"BD\", countryName:\"Bangladesh\", stationCode:\"CGP\", stationDesc:\"Chittagong\"},");
			strbBuffer.append("				{countryCode:\"YE\", countryName:\"Yemen\", stationCode:\"SAH\", stationDesc:\"Sanaa\"}");
			strbBuffer.append("			];\n");
		}

		return strbBuffer.toString();
	}

	/**
	 * 
	 * @param strMode
	 * @return
	 * @throws ModuleException
	 */
	public static String getAgentsAllAgentsInfo(String strMode, Collection collection) throws ModuleException {
		StringBuffer strbBuffer = new StringBuffer();

		int intMax = 0;
		// All
		if ((strMode == null) || (strMode.equals("AGENTS_A") || (strMode.equals("TAB_4")))) {
			strbBuffer.append("jsAllAgents = [");

			// Top
		} else if (strMode.equals("AGENTS_T")) {
			strbBuffer.append("jsTop = [");
			intMax = 10;

			// Non
		} else if (strMode.equals("AGENTS_N")) {
			strbBuffer.append("jsLow = [");
			intMax = 10;

			// Outstanding
		} else if (strMode.equals("AGENTS_O")) {
			strbBuffer.append("jsOut = [");
			intMax = 10;
		}

		if ((collection != null) && (collection.size() > 0)) {
			String[] arrColumns = new String[3];

			arrColumns[0] = "agentCode";
			arrColumns[1] = "agentType";
			arrColumns[2] = "agentName";

			strbBuffer.append(generateJsonStringFromArray(collection, arrColumns, intMax));
			strbBuffer.append("			];\n");

		} else {
			strbBuffer.append("jsAllAgents = [");
			strbBuffer.append("				{agentCode:\"SAW98\", agentType:\"TA\", agentName:\"SESA TURIZM SAW\"},");
			strbBuffer.append("				{agentCode:\"SAW99\", agentType:\"TA\", agentName:\"OBA TURIZM SAW\"},");
			strbBuffer.append("				{agentCode:\"SHJ331\", agentType:\"TA\", agentName:\"Mercan Dummy\"}");
			strbBuffer.append("];");
		}
		return strbBuffer.toString();
	}

	/**
	 * 
	 * @param strMode
	 * @return
	 * @throws ModuleException
	 */
	public static String getAgentsIndividualDetails(Collection collection) throws ModuleException {
		StringBuffer strbBuffer = new StringBuffer();

		// Branch Informations
		strbBuffer.append("jsAgntBranch = [");
		strbBuffer.append("				{branch:\"Istanbul\", revenue:\"$0.12\", branchCode:\"KUL\"},");
		strbBuffer.append("				{branch:\"Ankara\", revenue:\"$0.15\", branchCode:\"DXB\"},");
		strbBuffer.append("				{branch:\"Izmir\", revenue:\"$1.00\", branchCode:\"BOM\"},");
		strbBuffer.append("				{branch:\"Bursa\", revenue:\"$0.11\", branchCode:\"MLE\"},");
		strbBuffer.append("				{branch:\"Adana\", revenue:\"$0.15\", branchCode:\"SYD\"},");
		strbBuffer.append("				{branch:\"Gaziantep\", revenue:\"$0.20\", branchCode:\"ABC\"},");
		strbBuffer.append("				{branch:\"Konya\", revenue:\"$0.54\", branchCode:\"XYZ\"},");
		strbBuffer.append("				{branch:\"Antalya\", revenue:\"$0.06\", branchCode:\"MLE\"},");
		strbBuffer.append("				{branch:\"Kayseri\", revenue:\"$0.07\", branchCode:\"BLR\"}");
		strbBuffer.append("			];\n");

		// Agents Pay History
		strbBuffer.append("jsAgntPayHistory = [");
		strbBuffer
				.append("					{desc:\"\", month:\"Jun-09\", additions:\"Additions<br>[in Jun-09]\", percentage:\"% Meeting Goal<br>[in Jun-09]\"},");
		strbBuffer.append("					{desc:\"All Collectors\", month:\"2388\", additions:\"-387\", percentage:\"62%\"},");
		strbBuffer.append("					{desc:\"Exp < 6 Months\", month:\"185\", additions:\"51\", percentage:\"63%\"},");
		strbBuffer.append("					{desc:\"Exp >= 6 Months\", month:\"1533\", additions:\"-438\", percentage:\"61%\"}");
		strbBuffer.append("					];\n");

		// Agent Individual Details
		Object[] objAgentInfo = collection.toArray();
		String[] arrAgentInfo = (String[]) objAgentInfo[0];
		strbBuffer.append("jsAgentIndiInfo = ({");
		strbBuffer.append("				agentName:\"" + arrAgentInfo[0] + "\",");
		strbBuffer.append("				designation:\"" + arrAgentInfo[1] + "\",");
		strbBuffer.append("				reportTo:\"" + arrAgentInfo[2] + "\",");
		strbBuffer.append("				turnOver:\"" + arrAgentInfo[3] + "\",");
		strbBuffer.append("				employees:\"" + arrAgentInfo[4] + "\",");
		strbBuffer.append("				branches:\"" + arrAgentInfo[5] + "\",");
		strbBuffer.append("				yom:\"" + arrAgentInfo[6] + "\",");
		strbBuffer.append("				management:[");
		strbBuffer.append("				            {name:\"Howard Raufer\"},");
		strbBuffer.append("				            {name:\" Mark Richard\"}");
		strbBuffer.append("				            ]");
		strbBuffer.append("				});\n");

		return strbBuffer.toString();
	}

	/**
	 * 
	 * @param strMode
	 * @return
	 * @throws ModuleException
	 */
	public static String getAgentsBranchInfo() throws ModuleException {
		StringBuffer strbBuffer = new StringBuffer();

		// Route Info
		strbBuffer.append("jsAgntRouteWise = [");
		strbBuffer.append("						{location:\"SHJ\", value:\"0.13\"},");
		strbBuffer.append("						{location:\"KUL\", value:\"0.12\"},");
		strbBuffer.append("						{location:\"DOH\", value:\"0.12\"},");
		strbBuffer.append("						{location:\"SYD\", value:\"0.11\"},");
		strbBuffer.append("						{location:\"CMB\", value:\"0.09\"},");
		strbBuffer.append("						{location:\"BOM\", value:\"0.06\"},");
		strbBuffer.append("						{location:\"NYK\", value:\"0.10\"},");
		strbBuffer.append("						{location:\"CMN\", value:\"0.07\"},");
		strbBuffer.append("						{location:\"SAW\", value:\"0.05\"},");
		strbBuffer.append("						{location:\"KWI\", value:\"0.04\"}");
		strbBuffer.append("						];\n");

		// Agent Staff Wise
		strbBuffer.append("jsAgntStaffWise = [");
		strbBuffer.append("						{staffName:\"Kareche Danielle\", revenue:\"1.83\", sectors:\"36\", BC:\"9%\", TKT:\"76%\"},");
		strbBuffer.append("						{staffName:\"Debnar Kallal\", revenue:\"0.44\", sectors:\"63\", BC:\"75%\", TKT:\"73%\"},");
		strbBuffer.append("						{staffName:\"Worrell Ryan\", revenue:\"0.49\", sectors:\"36\", BC:\"10%\", TKT:\"49%\"},");
		strbBuffer.append("						{staffName:\"Jessica Dominguez\", revenue:\"0.24\", sectors:\"75\", BC:\"33%\", TKT:\"46%\"},");
		strbBuffer.append("						{staffName:\"Ogando Price\", revenue:\"0.27\", sectors:\"36\", BC:\"98%\", TKT:\"62%\"},");
		strbBuffer.append("						{staffName:\"Kabis Hasim\", revenue:\"0.41\", sectors:\"60\", BC:\"67%\", TKT:\"61%\"},");
		strbBuffer.append("						{staffName:\"Rifki Hadil\", revenue:\"0.21\", sectors:\"52\", BC:\"87%\", TKT:\"71%\"},");
		strbBuffer.append("						{staffName:\"Henry Jeorge\", revenue:\"0.23\", sectors:\"36\", BC:\"52%\", TKT:\"67%\"},");
		strbBuffer.append("						{staffName:\"Simor Perris\", revenue:\"0.60\", sectors:\"90\", BC:\"64%\", TKT:\"52%\"},");
		strbBuffer.append("						{staffName:\"Ricky Richard\", revenue:\"0.27\", sectors:\"48\", BC:\"61%\", TKT:\"45%\"}");
		strbBuffer.append("						];\n");

		// Graph Data
		strbBuffer.append("jsAgntFareClass = [");
		strbBuffer
				.append("					{location:\"KUL\", ec:\"10\", y1:\"20\", m3:\"22\", m1:\"22\", flexi:\"5\", lowEco:\"3\", perf:\"4\"},");
		strbBuffer
				.append("					{location:\"SHJ\", ec:\"30\", y1:\"36\", m3:\"16\", m1:\"16\", flexi:\"5\", lowEco:\"2\", perf:\"2\"},");
		strbBuffer
				.append("					{location:\"CMB\", ec:\"60\", y1:\"32\", m3:\"12\", m1:\"12\", flexi:\"1\", lowEco:\"2\", perf:\"3\"},");
		strbBuffer
				.append("					{location:\"BOM\", ec:\"35\", y1:\"22\", m3:\"12\", m1:\"12\", flexi:\"3\", lowEco:\"3\", perf:\"6\"},");
		strbBuffer
				.append("					{location:\"MLE\", ec:\"40\", y1:\"32\", m3:\"12\", m1:\"12\", flexi:\"3\", lowEco:\"3\", perf:\"6\"},");
		strbBuffer
				.append("					{location:\"DOH\", ec:\"20\", y1:\"22\", m3:\"12\", m1:\"12\", flexi:\"4\", lowEco:\"1\", perf:\"9\"}");
		strbBuffer.append("				];\n");

		return strbBuffer.toString();
	}

	/**
	 * 
	 * @param collection
	 * @param arrColumns
	 * @return
	 * @throws ModuleException
	 */
	private static String generateJsonString(Collection collection, String[] arrColumns, int intMax) throws ModuleException {

		StringBuffer strbBuffer = new StringBuffer();

		Iterator iterator = collection.iterator();
		int intCol = 0;
		int intCount = 0;
		String strComma = "";
		String strColComma = "";
		if (iterator != null) {
			while (iterator.hasNext()) {
				Map keyValues = (Map) iterator.next();
				Set keys = keyValues.keySet();
				Iterator keyIterator = keys.iterator();

				if (keyIterator.hasNext()) {
					intCol = 0;
					strColComma = "";
					strbBuffer.append(strComma + "\n");

					strbBuffer.append("{");
					while (keyIterator.hasNext()) {
						String code = (String) keyValues.get(keyIterator.next());

						strbBuffer.append(strColComma);
						strbBuffer.append(arrColumns[intCol] + ":\"" + code + "\" ");
						strColComma = ", ";
						intCol++;

					}
					strbBuffer.append("}");
					strComma = ", ";

					intCount++;

					if (intMax > 0) {
						if (intCount >= intMax) {
							break;
						}
					}
				}
			}
		}
		return strbBuffer.toString();
	}

	/**
	 * 
	 * @param collection
	 * @param arrColumns
	 * @return
	 * @throws ModuleException
	 */
	private static String generateJsonStringFromArray(Collection collection, String[] arrColumns, int intMax)
			throws ModuleException {

		StringBuffer strbBuffer = new StringBuffer();

		Iterator iterator = collection.iterator();
		int intCol = arrColumns.length;
		int intCount = 0;
		String strComma = "";
		String strColComma = "";
		if (iterator != null) {
			Object[] object = collection.toArray();
			int intSize = object.length;
			for (int i = 0; i < intSize; i++) {
				String[] arrColums = (String[]) object[i];
				strColComma = "";
				strbBuffer.append(strComma);
				strbBuffer.append("{");
				for (int x = 0; x < intCol; x++) {
					strbBuffer.append(strColComma);
					strbBuffer.append(arrColumns[x] + ":\"" + arrColums[x] + "\" ");
					strColComma = ", ";
				}
				strbBuffer.append("}");
				strComma = ", ";
			}
		}
		return strbBuffer.toString();
	}

	/**
	 * 
	 * @param collection
	 * @return
	 * @throws ModuleException
	 */
	public static String getFirstCountryCode(Collection collection) throws ModuleException {
		StringBuffer strbBuffer = new StringBuffer();

		Iterator iterator = collection.iterator();
		int intCol = 0;
		int intCount = 0;
		String strComma = "";
		String strColComma = "";
		if (iterator != null) {
			while (iterator.hasNext()) {
				Map keyValues = (Map) iterator.next();
				Set keys = keyValues.keySet();
				Iterator keyIterator = keys.iterator();

				if (keyIterator.hasNext()) {
					strbBuffer.append((String) keyValues.get(keyIterator.next()));
					break;
				}
			}
		}
		return strbBuffer.toString();
	}
}
