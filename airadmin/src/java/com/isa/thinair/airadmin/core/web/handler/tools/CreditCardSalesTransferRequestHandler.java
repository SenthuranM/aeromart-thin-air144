/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */

/**
 * @author Srikantha
 *
 */

package com.isa.thinair.airadmin.core.web.handler.tools;

import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.generator.tools.CreditCardSalesTransferHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;

/**
 * @author srikantha
 * 
 * 
 *         Preferences - Java - Code Style - Code Templates
 */

public final class CreditCardSalesTransferRequestHandler extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(CreditCardSalesTransferRequestHandler.class);

	private static AiradminConfig airadminConfig = new AiradminConfig();

	private static final String PARAM_SELECTED_DATES = "hdnSelectedDates";
	private static final String PARAM_MODE = "hdnMode";
	private static final String PARAM_UI_MODE = "hdnUIMode";

	private static void setIntSuccess(HttpServletRequest request, int value) {
		setAttribInRequest(request, "intSuccess", new Integer(value));
	}

	private static int getIntSuccess(HttpServletRequest request) {
		Integer returnValue = (Integer) getAttribInRequest(request, "intSuccess");
		if (returnValue != null)
			return returnValue.intValue();
		else
			return -1;
	}

	private static void setExceptionOccured(HttpServletRequest request, boolean value) {
		setAttribInRequest(request, "isExceptionOccured", new Boolean(value));
	}

	private static boolean isExceptionOccured(HttpServletRequest request) {
		return ((Boolean) getAttribInRequest(request, "isExceptionOccured")).booleanValue();
	}

	/**
	 * @param request
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {
		Boolean isViewReport = AppSysParamsUtil.isDownLoadCSVCashSalesReport();
		String strHdnMode = request.getParameter(PARAM_MODE);
		String forward = WebConstants.FORWARD_SUCCESS;
		String strUIModeJS = "var isSearchMode = false;";
		String strCashSalesStatus = "var isShowReport = " + isViewReport.toString() + ";";
		setExceptionOccured(request, false);
		setIntSuccess(request, 0);
		String strFormData = "var saveSuccess = " + getIntSuccess(request) + ";";
		strFormData += " var arrFormData = new Array();";
		request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
		request.setAttribute(WebConstants.REQ_OPERATION_UI_MODE, strUIModeJS);
		request.setAttribute(WebConstants.REQ_VIEW_REPORT_STATUS, strCashSalesStatus);

		try {
			if (strHdnMode != null && strHdnMode.equals("REPORT") && AppSysParamsUtil.isDownLoadCSVCashSalesReport()) {
				String strSelectedDates = request.getParameter(PARAM_SELECTED_DATES);
				String ds[] = strSelectedDates.split(",");
				Collection<Date> dateList = new ArrayList<Date>();
				for (int i = 0; i < ds.length; i++) {
					if (!ds[i].equals("")) {
						Date day = getDate(ds[i]);
						dateList.add(day);
					}
				}
				setReportView(response, dateList);
				setDisplayData(request);
				return null;
			} else if ((strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_GENERATE))
					|| (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_TRANSFER))) {
				if (transferCreditSales(request)) {

				}
			}
			setDisplayData(request);
		} catch (Exception exception) {
			if (exception instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
			}
		}

		return forward;
	}

	private static boolean transferCreditSales(HttpServletRequest request) throws Exception {

		try {
			String strHdnMode = request.getParameter(PARAM_MODE);
			String strSelectedDates = request.getParameter(PARAM_SELECTED_DATES);

			String ds[] = strSelectedDates.split(",");
			Collection<Date> dateList = new ArrayList<Date>();
			for (int i = 0; i < ds.length; i++) {
				if (!ds[i].equals("")) {
					Date day = getDate(ds[i]);
					dateList.add(day);
				}
			}

			if (!strHdnMode.equals("") && strHdnMode.equals("GENERATE")) { // When Status equals U

				ModuleServiceLocator.getReservationAuxilliaryBD().transferManualCreditCardSalesCollection(dateList);
				setIntSuccess(request, 1);
				saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_TRANSFER_SUCCESS), WebConstants.MSG_SUCCESS);

			} else if (!strHdnMode.equals("") && strHdnMode.equals("TRANSFER")) { // When Status equals N

				ModuleServiceLocator.getReservationAuxilliaryBD().transferManualCreditCardSalesCollection(dateList);
				setIntSuccess(request, 2);
				saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_TRANSFER_SUCCESS), WebConstants.MSG_SUCCESS);
			}

			setExceptionOccured(request, false);

		} catch (ModuleException moduleException) {

			setExceptionOccured(request, true);
			setIntSuccess(request, 3);
			String strFormData = "var arrFormData = new Array();";
			strFormData += "arrFormData[0] = new Array();";
			strFormData += " var saveSuccess = " + getIntSuccess(request) + ";";
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);

		} catch (Exception exception) {

			setIntSuccess(request, 3);
			if (exception instanceof RuntimeException) {
				throw exception;
			}
		}
		return true;
	}

	public static Date getDate(String format) throws ParseException {
		SimpleDateFormat dateFormat = null;
		Date dt = null;
		if (!format.equals("")) {
			if (format.indexOf('-') != -1) {
				dateFormat = new SimpleDateFormat("dd-MM-yy");
			}
			if (format.indexOf('/') != -1) {
				dateFormat = new SimpleDateFormat("dd/MM/yy");
			}
			if (format.indexOf(' ') != -1) {
				format = format.substring(0, format.indexOf(' '));
			}

			dt = dateFormat.parse(format);
		}
		return dt;
	}

	private static void setDisplayData(HttpServletRequest request) throws Exception {

		String strUIMode = request.getParameter(PARAM_UI_MODE);

		if (strUIMode != null && strUIMode.equals(WebConstants.ACTION_SEARCH)) {
			setCreditCardSalesTransferRowHtml(request);
		}
		setCardTypeHtml(request);
		setClientErrors(request);

		if (!isExceptionOccured(request)) {
			String strFormData = "var arrFormData = new Array();";
			strFormData += " var saveSuccess = " + getIntSuccess(request) + ";";
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
		}

	}

	public static void setCardTypeHtml(HttpServletRequest request) throws ModuleException {
		String strCardTypeList = JavascriptGenerator.createCardTypeHtml();
		request.setAttribute(WebConstants.REQ_HTML_CARD_TYPE_DATA, strCardTypeList);

	}

	private static void setClientErrors(HttpServletRequest request) {

		try {
			String strClientErrors = CreditCardSalesTransferHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error(
					"CreditCardSalesTransferRequestHandler.setClientErrors() method is failed :"
							+ moduleException.getMessageString(), moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);

		}
	}

	protected static void setCreditCardSalesTransferRowHtml(HttpServletRequest request) throws Exception {

		try {
			CreditCardSalesTransferHTMLGenerator htmlGen = new CreditCardSalesTransferHTMLGenerator();
			String strHtml = htmlGen.getCreditCardSalesTransferRowHtml(request);
			request.setAttribute(WebConstants.REQ_HTML_ROWS, strHtml);

			request.setAttribute(WebConstants.REQ_FORM_FIELD_VALUES, htmlGen.getFormFieldValues());
		} catch (ModuleException moduleException) {
			log.error(
					"CreditCardSalesTransferHandler.setCreditCardSalesTransferRowHtml() method is failed :"
							+ moduleException.getMessageString(), moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);

		}
	}

	private static void setReportView(HttpServletResponse response, Collection<Date> dateList) throws ModuleException {
		try {
			Map<String, Object> parameters = new HashMap<String, Object>();

			String reportTemplate = "creditCardSalesTransferReport.jasper";
			ResultSet resultSet = ModuleServiceLocator.getReservationAuxilliaryBD().getSalesTransferServiceData(dateList,
					ReservationTnxNominalCode.getCreditCardNominalCodes());

			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1", ReportsHTMLGenerator.getReportTemplate(reportTemplate));
			response.reset();
			response.addHeader("Content-Disposition", "attachment;filename=CreditSalesTransferReport.csv");
			ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);
		} catch (Exception e) {
			log.error(e);
		}

	}
}
