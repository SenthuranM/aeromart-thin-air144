//TODO insert license header
package com.isa.thinair.airadmin.core.web.handler.reports;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.reporting.api.model.AirportDetailsDTO;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * Airport List Action Request Handler.
 * 
 * @author sanjaya
 */
public class AirportListReportRH extends BasicRequestHandler {

	/** Class Logger */
	private static Log log = LogFactory.getLog(AirportListReportRH.class);

	/** Global Configurations */
	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	public static String execute(HttpServletRequest request, HttpServletResponse response) {

		String strHdnMode = request.getParameter("hdnMode");
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;

		try {
			setDisplayData(request);
			log.debug("AirportListReportRH setDisplayData() SUCCESS");
		} catch (Exception ex) {
			log.error("AirportListReportRH setDisplayData() FAILED " + ex.getMessage());
		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				log.debug("AirportListReportRH setReportView Success");
				return null;
			} else {
				log.error("AirportListReportRH setReportView not selected");
			}

		} catch (ModuleException ex) {
			saveMessage(request, ex.getMessageString(), WebConstants.MSG_ERROR);
			log.error("AirportListReportRH setReportView Failed " + ex.getMessageString());
		}

		return forward;

	}

	/**
	 * Sets the initial data fill-up for the airport list interface.
	 * 
	 * @param request
	 *            : The ServletRequest.
	 * @throws Exception
	 */
	public static void setDisplayData(HttpServletRequest request) throws Exception {
		setClientErrors(request);
		setCountryList(request);
		setStationComboList(request);
		setAirportList(request);
		setLiveStatus(request);

		// TODO <= Do we need this here? What does it do?
		ReportsHTMLGenerator.createPreferedReportOptions(request);
	}

	/**
	 * TODO : Fill method body. Sets the report data.
	 * 
	 * @param request
	 * @param response
	 * @throws ModuleException
	 */
	protected static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		String strReportId = "UC_REPM_077";
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);
		String reportTemplate = "AirportListReport.jasper";
		String value = request.getParameter("radReportOption");
		String strLogo = AppSysParamsUtil.getReportLogo(false);
		String strPath = "../../images/" + AppSysParamsUtil.getReportLogo(true);
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";

		String country = request.getParameter("selCountry");
		String station = request.getParameter("selStation");
		String airport = request.getParameter("selAirport");
		String airportStatus = request.getParameter("selStatus");
		String sortBy = request.getParameter("selSortBy");
		String sortByOrder = request.getParameter("selSortByOrder");

		if (log.isDebugEnabled()) {
			log.debug("Arguments : country" + country + "station:" + station + "airport" + airport);
		}

		try {
			ReportsSearchCriteria search = new ReportsSearchCriteria();

			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs(WebConstants.REPORT_REF,
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));

			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}

			search.setAirportCode(airport);
			search.setCountryCode(country);
			search.setStation(station);
			search.setStatus(airportStatus);

			// this is to hide the column names from jsp.
			if ("COUNTRY".equals(sortBy)) {
				search.setSortByColumnName(" COUNTRY_NAME ");
			}

			if ("AIRPORT".equals(sortBy)) {
				search.setSortByColumnName(" AIRPORT_NAME ");
			}

			search.setSortByOrder(sortByOrder);

			ResultSet resultSet = ModuleServiceLocator.getDataExtractionBD().getAirportListData(search);

			List<AirportDetailsDTO> airportDetailsDTOList = createAirportDTOList(resultSet);

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("ID", strReportId);
			parameters.put("CARRIER", strCarrier);

			// To provide Report Format Options
			String reportNumFormat = request.getParameter("radRptNumFormat");
			ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

			if (value.trim().equals(WebConstants.REPORT_HTML)) {
				parameters.put("IMG", strPath);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport(WebConstants.REPORT_REF, parameters,
						airportDetailsDTOList, null, null, response);
			} else if (value.trim().equals(WebConstants.REPORT_PDF)) {
				response.reset();
				response.addHeader("Content-Disposition", "filename=Airport_List_Report.pdf");
				strPath = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG", strPath);
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport(WebConstants.REPORT_REF, parameters,
						airportDetailsDTOList, response);
			} else if (value.trim().equals(WebConstants.REPORT_EXCEL)) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=Airport_List_Report.xls");
				strPath = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG", strPath);
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport(WebConstants.REPORT_REF, parameters,
						airportDetailsDTOList, response);
			} else if (value.trim().equals("CSV")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=Airport_List_Report.csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport(WebConstants.REPORT_REF, parameters,
						airportDetailsDTOList, response);
			}

		} catch (Exception ex) {
			log.error("Airport List Report Generation Failed: ", ex);
		}

	}

	private static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	private static void setAirportList(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createAirportCodeList();
		request.setAttribute(WebConstants.REQ_HTML_AIRPORT_LIST, strHtml);
	}

	protected static void setStationComboList(HttpServletRequest request) throws ModuleException {
		String strStation = SelectListGenerator.createStationList();
		request.setAttribute(WebConstants.REQ_STATION_LIST, strStation);
	}

	public static void setCountryList(HttpServletRequest request) throws ModuleException {
		String country = SelectListGenerator.createCountryList();
		request.setAttribute(WebConstants.REQ_HTML_COUNTRY_LIST, country);
	}

	private static void setLiveStatus(HttpServletRequest request) throws ModuleException {
		String strLive = request.getParameter(WebConstants.REP_HDN_LIVE);
		if (strLive != null) {
			request.setAttribute(WebConstants.REP_SET_LIVE, strLive);
		}

	}

	private static List<AirportDetailsDTO> createAirportDTOList(ResultSet airportDetailsResults) {
		List<AirportDetailsDTO> airportDetailsList = new ArrayList<AirportDetailsDTO>();

		try {
			while (airportDetailsResults.next()) {
				AirportDetailsDTO airportDetail = new AirportDetailsDTO();

				airportDetail.setAirportName(airportDetailsResults.getString("AIRPORT_NAME"));
				airportDetail.setStationName(airportDetailsResults.getString("STATION_NAME"));
				airportDetail.setCountryName(airportDetailsResults.getString("COUNTRY_NAME"));
				airportDetail.setOnlineOfflineStatus(airportDetailsResults.getString("ONLINE_STATUS"));
				airportDetail.setStatus(airportDetailsResults.getString("STATUS"));
				airportDetail.setGoShowAgent(airportDetailsResults.getString("AGENT_NAME"));
				airportDetail.setLccPublishStatus(airportDetailsResults.getString("LCC_PUBLISH_STATUS"));
				airportDetail.setBusStation(airportDetailsResults.getString("IS_SURFACE_STATION"));

				airportDetail.setGmtOffset(airportDetailsResults.getString("GMT_OFFSET_ACTION") + ""
						+ formatTime(airportDetailsResults.getInt("GMT_OFFSET_HOURS")));

				int airportDSTTime = airportDetailsResults.getInt("DST_ADJUST_TIME");
				if (airportDSTTime < 0) {
					airportDetail.setEffectiveDSTOffset("-" + formatTime(Math.abs(airportDSTTime)));
				} else if (airportDSTTime > 0) {
					airportDetail.setEffectiveDSTOffset("+" + formatTime(airportDSTTime));
				} else {
					airportDetail.setEffectiveDSTOffset("n/a");
				}

				airportDetail.setMinStopoverTime(formatTime(airportDetailsResults.getInt("MIN_STOPOVER_TIME")));

				int anciCutoverStart = airportDetailsResults.getInt("ANCI_NOTIFY_START_CUTOVER");
				int anciCutoverEnd = airportDetailsResults.getInt("ANCI_NOTIFY_END_CUTOVER");
				String anciCuroverRange = formatDayHourTime(anciCutoverStart) + "/" + formatDayHourTime(anciCutoverEnd);

				airportDetail.setAnciReminderStartEnd(anciCuroverRange);
				airportDetailsList.add(airportDetail);

			}
		} catch (SQLException sqlEx) {
			log.error("Error occurred while iterating the airport list :" + sqlEx);
		}

		return airportDetailsList;
	}

	private static String formatTime(int timeInMinutes) {
		int numerator = timeInMinutes;
		int denominator = 60;

		int quotient = numerator / denominator;
		int remainder = numerator % denominator;

		String strRemainder;
		if (remainder < 10)
			strRemainder = "0" + remainder;
		else
			strRemainder = "" + remainder;

		return quotient + ":" + strRemainder;
	}

	// Change given minutes into DAYS: HOURS : MIUNTES Format
	private static String formatDayHourTime(int timeInMinutes) {
		int numerator = timeInMinutes;
		int daysDenominator = 1440;
		int hourDenominator = 60;

		int days = numerator / daysDenominator;
		int minutesRemaining = numerator % daysDenominator;
		int hours = minutesRemaining / hourDenominator;
		int mins = minutesRemaining % hourDenominator;

		String strDays;
		String strHours;
		String strMins;
		if (days < 10)
			strDays = "0" + days;
		else
			strDays = "" + days;

		if (hours < 10)
			strHours = "0" + hours;
		else
			strHours = "" + hours;

		if (mins < 10)
			strMins = "0" + mins;
		else
			strMins = "" + mins;

		return strDays + ":" + strHours + ":" + strMins;
	}

}
