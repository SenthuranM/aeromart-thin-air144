package com.isa.thinair.airadmin.core.web.generator.flightsched;

import com.isa.thinair.airadmin.api.FlightsToReprotectDTO;
import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airinventory.api.dto.FCCInventoryDTO;
import com.isa.thinair.airinventory.api.dto.FCCInventorySummaryDTO;
import com.isa.thinair.airinventory.api.dto.FCCSegInvSummaryDTO;
import com.isa.thinair.airinventory.api.dto.FCCSegInventoryDTO;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableConnectedFlight;
import com.isa.thinair.airinventory.api.dto.seatavailability.AvailableFlightSegment;
import com.isa.thinair.airinventory.api.dto.seatavailability.SeatAllocationDTO;
import com.isa.thinair.airinventory.api.model.FCCSegInventory;
import com.isa.thinair.airinventory.api.model.LogicalCabinClass;
import com.isa.thinair.airmaster.api.model.CabinClass;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.utils.FlightStatusEnum;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.FlightSearchCriteria;
import com.isa.thinair.reporting.api.model.FlightInventorySummaryDTO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * @author srikantha
 */
public class FlightsToReprotectHTMLGenerator {

	private static Log log = LogFactory.getLog(FlightsToReprotectHTMLGenerator.class);
	private static final String dateFormat = "dd/MM/yyyy";
	private FlightSearchCriteria reprotectFlightSearchCriteria = null;

	GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();
	String allowReprotectPAXToTodaysDepartures = globalConfig.getBizParam(
			SystemParamKeys.ALLOW_REPROTECT_PAX_TO_TODAYS_DEPARTURES).equalsIgnoreCase("Y") ? "Y" : "N";

	/**
	 * Gets the Flights to Re protectS
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Grid array containg the Flights to Re protect
	 * @throws ModuleException
	 *             the ModuleException
	 */
	@SuppressWarnings("unchecked")
	public final String getFlightsToReprotectRowHtml(HttpServletRequest request) throws ModuleException {

		AiradminUtils.debug(log, "inside FlightsToReprotectHTMLGenerator.getFlightsToReprotectRowHtml()");

		String strFormFieldsVariablesJS = "";
		prepareCabinClasses(request);

		reprotectFlightSearchCriteria = new FlightSearchCriteria();

		// Preparing module criterian for DepartureDateFrom
		String strDepartureDateFrom = request.getParameter("txtDepartureDateFrom") == null ? "" : request
				.getParameter("txtDepartureDateFrom");
		// Preparing module criterian for DepartureDateTo
		String strDepartureDateTo = request.getParameter("txtDepartureDateTo") == null ? "" : request
				.getParameter("txtDepartureDateTo");
		// Preparing module criterian for FlightNumber
		String strFlightNumber = request.getParameter("txtFlightNumber") == null ? "" : request.getParameter("txtFlightNumber");
		// Preparing module criterian for Departure
		String strDeparture = request.getParameter("selDeparture");
		// Preparing module criterian for Arrival
		String strArrival = request.getParameter("selArrival");
		// Preparing module criterian for Via1
		String strVia1 = request.getParameter("selVia1");
		// Preparing module criterian for Via2
		String strVia2 = request.getParameter("selVia2");
		// Preparing module criterian for Via3
		String strVia3 = request.getParameter("selVia3");
		// Preparing module criterian for Via4
		String strVia4 = request.getParameter("selVia4");

		strFormFieldsVariablesJS = "var departureDateFrom ='" + strDepartureDateFrom + "';";
		strFormFieldsVariablesJS += "var departureDateTo ='" + strDepartureDateTo + "';";
		strFormFieldsVariablesJS += "var flightNumber ='" + strFlightNumber + "';";
		strFormFieldsVariablesJS += "var departure ='" + strDeparture + "';";
		strFormFieldsVariablesJS += "var arrival ='" + strArrival + "';";
		strFormFieldsVariablesJS += "var via1 ='" + strVia1 + "';";
		strFormFieldsVariablesJS += "var via2 ='" + strVia2 + "';";
		strFormFieldsVariablesJS += "var via3 ='" + strVia3 + "';";
		strFormFieldsVariablesJS += "var via4 ='" + strVia4 + "';";
		request.setAttribute(WebConstants.REQ_FORM_FIELD_VALUES, strFormFieldsVariablesJS);

		Collection<FlightInventorySummaryDTO> reprotectFlightsCollection = null;

		int recNo = 0;
		if (request.getParameter("hdnRecNo") == null) {
			recNo = 1;
		} else if (!("".equals(request.getParameter("hdnRecNo")))) {
			recNo = Integer.parseInt(request.getParameter("hdnRecNo"));
		}
		recNo = recNo - 1;

		Page page = null;
		int totalRecords = 0;

		// if Search button is pressed
		// if ((strMode != null) && (strMode.equals(WebConstants.ACTION_SEARCH))) {
		// preparing and setting the departureDateFrom date
		reprotectFlightSearchCriteria.setDepartureDateFrom(AiradminUtils.getFormattedDate(strDepartureDateFrom));

		// preparing and setting the departureDatTo date
		reprotectFlightSearchCriteria.setDepartureDateTo(AiradminUtils.getFormattedDate(strDepartureDateTo));

		if (strArrival != null && !(strArrival.equals("-1"))) {
			reprotectFlightSearchCriteria.setDestination(strArrival);
		}

		if (strDeparture != null && !(strDeparture.equals("-1"))) {
			reprotectFlightSearchCriteria.setOrigin(strDeparture);
		}

		if (strFlightNumber != null && !strFlightNumber.equals("")) {
			reprotectFlightSearchCriteria.setFlightNumber(strFlightNumber.toUpperCase());
		} else {
			reprotectFlightSearchCriteria.setFlightNumber("%");
		}

		// setting the via points
		ArrayList<String> viaList = new ArrayList<String>();

		if (strVia1 != null && !(strVia1.equals("-1"))) {
			viaList.add(strVia1);
		}

		if (strVia2 != null && !(strVia2.equals("-1"))) {
			if (strVia1 == null || strVia1.equals("")) {
				viaList.add(null);
			}
			viaList.add(strVia2);
		}

		if (strVia3 != null && !(strVia3.equals("-1"))) {
			if (strVia1 == null || strVia1.equals("")) {
				viaList.add(null);
			}
			if (strVia2 == null || strVia2.equals("")) {
				viaList.add(null);
			}
			viaList.add(strVia3);
		}

		if (strVia4 != null && !(strVia4.equals("-1"))) {
			if (strVia1 == null || strVia1.equals("")) {
				viaList.add(null);
			}
			if (strVia2 == null || strVia2.equals("")) {
				viaList.add(null);
			}
			if (strVia3 == null || strVia3.equals("")) {
				viaList.add(null);
			}
			viaList.add(strVia4);
		}

		reprotectFlightSearchCriteria.setViaPoints(viaList);
		reprotectFlightSearchCriteria.addFlightStatus(FlightStatusEnum.ACTIVE.getCode());
		reprotectFlightSearchCriteria.addFlightStatus(FlightStatusEnum.CLOSED.getCode());
		reprotectFlightSearchCriteria.setTimeInLocal(false);
		reprotectFlightSearchCriteria.setSeatFactor(new int[] { 0, 1000 });
		page = ModuleServiceLocator.getDataExtractionBD().searchFlightsForInventoryUpdation(reprotectFlightSearchCriteria, recNo,
				20);

		Collection<AvailableConnectedFlight> connectedFlights = new ArrayList<AvailableConnectedFlight>();
		// TODO getAircraftServiceBD().getAllCabinClasses() has removed.
		/*
		 * if (reprotectFlightSearchCriteria.getOrigin() != null && reprotectFlightSearchCriteria.getDestination() !=
		 * null) { List<RouteInfoTO> availableRoutes = ModuleServiceLocator.getFlightServiceBD().getAvailableRoutes(
		 * reprotectFlightSearchCriteria.getOrigin(), reprotectFlightSearchCriteria.getDestination(),
		 * AvailableFlightSearchDTO.ALL_SINGLE_CONNECTED_FLIGHTS); Date currentSysTimeInZulu =
		 * CalendarUtil.getCurrentSystemTimeInZulu(); Map<String, Date> allowableLatesDatesZuluForInterliningMap =
		 * getEffectiveFromDateZuluForInterlining( currentSysTimeInZulu,
		 * CommonsServices.getGlobalConfig().getActiveInterlinedCarriersMap());
		 * 
		 * Collection<CabinClass> colCabinClasses = ModuleServiceLocator.getAircraftServiceBD().getAllCabinClasses();
		 * for (RouteInfoTO routeInfoTO : availableRoutes) { for (CabinClass cc : colCabinClasses) {
		 * Collection<AvailableConnectedFlight> cf =
		 * ModuleServiceLocator.getFlightServiceBD().getAvailableConnectedFlightSegments(
		 * reprotectFlightSearchCriteria.getDepartureDateFrom(), reprotectFlightSearchCriteria.getDepartureDateTo(),
		 * reprotectFlightSearchCriteria.getOrigin(), reprotectFlightSearchCriteria.getDestination(),
		 * routeInfoTO.getTransitAirportCodesList(), 1, 0, cc.getCabinClassCode(), 1,
		 * allowableLatesDatesZuluForInterliningMap, null, null); if(cf!= null){ connectedFlights.addAll(cf); } } }
		 * 
		 * }
		 */
		if (page != null) {
			reprotectFlightsCollection = page.getPageData();
			totalRecords = page.getTotalNoOfRecords();
			request.setAttribute(WebConstants.REQ_TOTAL_RECORDS, new Integer(totalRecords).toString());
		}

		// }
		request.setAttribute(WebConstants.REQ_RECORD_NUM, new Integer(recNo + 1).toString());
		String currentHTML = createFlightsToReprotectRowHTML(reprotectFlightsCollection, request);
		return createFlightsToReprotectRowHTMLForConnections(connectedFlights, request, currentHTML);
	}

	/**
	 * Creates the Flights Need to be Re protected
	 * 
	 * @param reprotectFlightsCollection
	 *            the Collection of Flights to be Reprotected
	 * @return String the Array Of Flights to be Reprotected
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private String createFlightsToReprotectRowHTML(Collection<FlightInventorySummaryDTO> reprotectFlightsCollection,
			HttpServletRequest request) throws ModuleException {

		long currentTime = CalendarUtil.getCurrentSystemTimeInZulu().getTime();

		if (reprotectFlightsCollection == null) {
			return "var arrFlightsToReprotectData = new Array();";
		}

		Integer flightId = Integer.parseInt(request.getParameter("hdnFlightId"));
		Flight flight = ModuleServiceLocator.getFlightServiceBD().getFlight(flightId);
		String sourceFlightCsOcCarrierCode = flight.getCsOCCarrierCode();

		long lngClosureBufferTime = Long.parseLong(globalConfig.getBizParam(SystemParamKeys.FLIGHT_CLOSURE_DEPARTURE_GAP));
		lngClosureBufferTime = (lngClosureBufferTime * 60 * 1000);

		StringBuffer strMyArr = new StringBuffer("var arrFlightsToReprotectData = new Array();");
		String depDate = "";

		Iterator<FlightInventorySummaryDTO> iter = reprotectFlightsCollection.iterator();
		int i = 0;
		SimpleDateFormat dateFormater = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		SimpleDateFormat sdfmt = new SimpleDateFormat(dateFormat);
		while (iter.hasNext()) {
			// Get the Bean and run the business methosd
			FlightInventorySummaryDTO flightSummary = iter.next();

			long departureTimeGap = flightSummary.getDepartureDateTime().getTime() - lngClosureBufferTime;
			if ((allowReprotectPAXToTodaysDepartures.equalsIgnoreCase("Y")) && (departureTimeGap < currentTime)) {
				continue;
			} else {

				String targetFlightCsOcCarrierCode = flightSummary.getCsOcCarrierCode();

				if ((sourceFlightCsOcCarrierCode == null && targetFlightCsOcCarrierCode == null)
						|| (sourceFlightCsOcCarrierCode != null && targetFlightCsOcCarrierCode != null && (sourceFlightCsOcCarrierCode
								.equals(targetFlightCsOcCarrierCode)))) {
					strMyArr.append("arrFlightsToReprotectData[" + i + "] = new Array();");
					strMyArr.append("arrFlightsToReprotectData[" + i + "][1] = '" + flightSummary.getFlightNumber() + "';"); // Flight
																																// Number

					if (flightSummary.getDeparture() != null) {
						strMyArr.append("arrFlightsToReprotectData[" + i + "][2] = '" + flightSummary.getDeparture() + "';"); // Departure
					} else {
						strMyArr.append("arrFlightsToReprotectData[" + i + "][2] = '';"); // Departure
					}

					if (flightSummary.getDepartureDateTime() != null) {
						strMyArr.append("arrFlightsToReprotectData[" + i + "][3] = '"
								+ dateFormater.format(flightSummary.getDepartureDateTime()) + "';");
					} else {
						strMyArr.append("arrFlightsToReprotectData[" + i + "][3] = '';");
					}
					if (flightSummary.getArrival() != null) {
						strMyArr.append("arrFlightsToReprotectData[" + i + "][4] = '" + flightSummary.getArrival() + "';"); // Arrival
					} else {
						strMyArr.append("arrFlightsToReprotectData[" + i + "][4] = '';"); // Arrival
					}

					if (flightSummary.getArrivalDateTime() != null) {
						strMyArr.append("arrFlightsToReprotectData[" + i + "][5] = '"
								+ dateFormater.format(flightSummary.getArrivalDateTime()) + "';"); // Arrival time
					} else {
						strMyArr.append("arrFlightsToReprotectData[" + i + "][5] = '';");
					}

					List<String> routesList = flightSummary.getViaPoints();
					String strRoutes = "";
					if (routesList != null) {
						for (int q = 0; q < routesList.size(); q++) {
							strRoutes += routesList.get(q);
							if (q != (routesList.size() - 1)) {
								if ((q % 2 == 0) && (q != 0)) {
									strRoutes += "<BR>";
								} else {
									strRoutes += ",";
								}
							}
						}
					}

					strMyArr.append("arrFlightsToReprotectData[" + i + "][6] = '" + strRoutes + "';"); // Via points

					// preparing inventory data
					Object[] fccInventories = flightSummary.getFccInventorySummaryDTO().toArray();
					List fccSegInventoryList = new ArrayList();
					for (Object fccInventorie : fccInventories) {
						FCCInventorySummaryDTO fltIntrySummry = (FCCInventorySummaryDTO) fccInventorie;
						fccSegInventoryList.addAll(fltIntrySummry.getFccSegInventorySummary());
					}

					int fccSegInvCount = fccSegInventoryList.size();

					String[] logicalCabinClasses = new String[fccSegInvCount];
					String[] cabinClassesDescription = new String[fccSegInvCount];
					int[] segmentAvailable = new int[fccSegInvCount];
					int[] segmentInfantAvailable = new int[fccSegInvCount];
					int[] segmentSold = new int[fccSegInvCount];
					int[] segmentOnHold = new int[fccSegInvCount];
					int[] segmentInfantSold = new int[fccSegInvCount];
					int[] segmentInfantOnHold = new int[fccSegInvCount];
					String[] segments = new String[fccSegInvCount];
					int[] segmentID = new int[fccSegInvCount];

					/*
					 * 
					 * Preparing the allocations details ------------> LogicalCabinClass wise info | | | \|/ Segment
					 * wise info V
					 */

					for (int q = 0; q < fccSegInvCount; q++) {
						FCCSegInvSummaryDTO fccSegInvSummary = (FCCSegInvSummaryDTO) fccSegInventoryList.get(q);

						String logicalCabinClassCode = fccSegInvSummary.getLogicalCabinClassCode();
						logicalCabinClasses[q] = logicalCabinClassCode;
						// CabinClass cc =
						// ModuleServiceLocator.getAircraftServiceBD().getCabinClass(logicalCabinClassCode);
						// cabinClassesDescription[p] = (cc != null) ? cc.getCabinClassCode() + ":" +
						// cc.getDescription() :
						// ":";

						segmentAvailable[q] = fccSegInvSummary.getAvailableSeats();
						segmentInfantAvailable[q] = fccSegInvSummary.getAvailableInfantSeats();
						segmentOnHold[q] = fccSegInvSummary.getAdultOnhold();
						segmentSold[q] = fccSegInvSummary.getAdultSold();
						segmentInfantSold[q] = fccSegInvSummary.getInfantSold();
						segmentInfantOnHold[q] = fccSegInvSummary.getInfantOnhold();
						segments[q] = fccSegInvSummary.getSegmentCode();
						segmentID[q] = fccSegInvSummary.getFlightSegmentId();

					}

					StringBuffer segmentAvailableBuff = new StringBuffer();
					StringBuffer segmentOnHoldBuff = new StringBuffer();
					StringBuffer segmentSoldBuff = new StringBuffer();
					StringBuffer logicalCabinClassBuff = new StringBuffer();
					StringBuffer cabinClassDesBuff = new StringBuffer();
					List<String> logicalCabinClassesList = new ArrayList<String>();
					StringBuffer segmentsBuff = new StringBuffer();
					StringBuffer segmentsIDBuff = new StringBuffer();

					for (int n = 0; n < fccSegInvCount; n++) {
						segmentAvailableBuff.append(segmentAvailable[n] + "/" + segmentInfantAvailable[n]
								+ ((n == (fccSegInvCount - 1)) ? "<BR>" : ","));
						segmentOnHoldBuff.append(segmentOnHold[n] + "/" + segmentInfantOnHold[n]
								+ ((n == (fccSegInvCount - 1)) ? "<BR>" : ","));
						segmentSoldBuff.append(segmentSold[n] + "/" + segmentInfantSold[n]
								+ ((n == (fccSegInvCount - 1)) ? "<BR>" : ","));
						segmentsBuff.append(segments[n] + ((n == (fccSegInvCount - 1)) ? "<BR>" : ","));
						segmentsIDBuff.append(segmentID[n] + ((n == (fccSegInvCount - 1)) ? "<BR>" : ","));
						logicalCabinClassBuff.append(logicalCabinClasses[n] + ((n == (fccSegInvCount - 1)) ? "<BR>" : ","));
						cabinClassDesBuff.append(cabinClassesDescription[n] + ((n == (fccSegInvCount - 1)) ? "<BR>" : ","));
						if (!logicalCabinClassesList.contains(logicalCabinClasses[n])) {
							logicalCabinClassesList.add(logicalCabinClasses[n]);
						}
					}

					strMyArr.append("arrFlightsToReprotectData[" + i + "][7] = '" + segmentAvailableBuff.toString() + "';");
					strMyArr.append("arrFlightsToReprotectData[" + i + "][8] = '" + segmentSoldBuff.toString() + "';");
					strMyArr.append("arrFlightsToReprotectData[" + i + "][9] = '" + segmentOnHoldBuff.toString() + "';");
					strMyArr.append("arrFlightsToReprotectData[" + i + "][10] = '" + flightSummary.getFlightId() + "';"); // flight
																															// id
					strMyArr.append("arrFlightsToReprotectData[" + i + "][11] = '" + flightSummary.getScheduleId() + "';");
					if (flightSummary.getModelNumber() != null) {
						strMyArr.append("arrFlightsToReprotectData[" + i + "][12] = '" + flightSummary.getModelNumber() + "';"); // Aircraft
																																	// Model
					} else {
						strMyArr.append("arrFlightsToReprotectData[" + i + "][12] = '';");
					}
					/*
					 * strMyArr += seatInventoryHelper.getAircraftModelCapacity( flightSummary.getModelNumber());
					 * strMyArr += "arrFlightsToReprotectData[" + i + "][13] = arrAircraftModelCapacityData;";
					 */
					strMyArr.append("arrFlightsToReprotectData[" + i + "][13] = '';");
					StringBuffer buffSegments = new StringBuffer();
					StringBuffer buffSegIDs = new StringBuffer();
					Map<Integer, String> segmentsMap = flightSummary.getSegmentMap();
					if (segmentsMap != null) {
						Set<Integer> segmentsSet = segmentsMap.keySet();
						Iterator<Integer> keyIterator = segmentsSet.iterator();
						if (segmentsSet != null) {
							while (keyIterator.hasNext()) {
								Integer segmentId = keyIterator.next();
								String segmentCode = segmentsMap.get(segmentId);
								buffSegments.append(segmentCode + ",");
								buffSegIDs.append(segmentId + ",");
							}
						}
					}

					strMyArr.append("arrFlightsToReprotectData[" + i + "][14] = '" + buffSegments.toString() + "';");

					StringBuffer logicalCCBuff = new StringBuffer();
					if (logicalCabinClasses != null && logicalCabinClasses.length > 0) {
						for (String logicalCabinClasse : logicalCabinClasses) {
							logicalCCBuff.append(logicalCabinClasse + ":");
						}
					}

					strMyArr.append("arrFlightsToReprotectData[" + i + "][15] = '" + logicalCCBuff.toString() + "';");
					strMyArr.append("arrFlightsToReprotectData[" + i + "][16] = '" + buffSegIDs.toString() + "';");
					strMyArr.append("arrFlightsToReprotectData[" + i + "][17] = '" + logicalCabinClassBuff.toString() + "';");
					strMyArr.append("arrFlightsToReprotectData[" + i + "][18] = '" + segmentsBuff.toString() + "';");
					strMyArr.append("arrFlightsToReprotectData[" + i + "][19] = '" + segmentsIDBuff.toString() + "';");
					depDate = "";
					try {
						depDate = sdfmt.format(dateFormater.parse(dateFormater.format(flightSummary.getDepartureDateTime())));
					} catch (Exception e) {
						log.error("Error in formatting date" + e.getMessage());
						depDate = "";
					}
					strMyArr.append("arrFlightsToReprotectData[" + i + "][20] = '" + depDate + "';");
					strMyArr.append("arrFlightsToReprotectData[" + i + "][21] = '';");
					strMyArr.append("arrFlightsToReprotectData[" + i + "][22] = '" + cabinClassDesBuff.toString() + "';");

					i++;
				}
			}
		}
		if (allowReprotectPAXToTodaysDepartures.equalsIgnoreCase("Y")) {
			request.setAttribute(WebConstants.REQ_TOTAL_RECORDS, String.valueOf(i));
		}

		return strMyArr.toString();
	}

	/**
	 * Creates the Flights Need to be Re protected for Connections
	 * 
	 * @param connectedFlights
	 *            *
	 * @return String the Array Of Flights to be Reprotected
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private String createFlightsToReprotectRowHTMLForConnections(Collection<AvailableConnectedFlight> connectedFlights,
			HttpServletRequest request, String currentHTML) throws ModuleException {

		long currentTime = CalendarUtil.getCurrentSystemTimeInZulu().getTime();

		if (connectedFlights == null || connectedFlights.isEmpty()) {
			return currentHTML;
		}

		long lngClosureBufferTime = Long.parseLong(globalConfig.getBizParam(SystemParamKeys.FLIGHT_CLOSURE_DEPARTURE_GAP));
		lngClosureBufferTime = (lngClosureBufferTime * 60 * 1000);
		SimpleDateFormat dateFormater = new SimpleDateFormat("dd-MM-yyyy HH:mm");

		StringBuffer strMyArr = new StringBuffer(currentHTML);

		Iterator<AvailableConnectedFlight> iter = connectedFlights.iterator();
		int i = Integer.parseInt(request.getAttribute(WebConstants.REQ_TOTAL_RECORDS).toString());
		// List<FlightsToReprotectDTO> flightsToReprotectDTOs = new ArrayList<FlightsToReprotectDTO>();
		Map<String, FlightsToReprotectDTO> flightsToReprotectDTOs = new HashMap<String, FlightsToReprotectDTO>();
		while (iter.hasNext()) {

			AvailableConnectedFlight availableConnectedFlight = iter.next();
			String flightNumber = "";
			String departure = "";
			String allDepartureStations = "";
			String arrival = "";
			String departureDateTime = "";
			String arrivalDateTime = "";
			String segmentAvailable = "";
			String segmentSold = "";
			String segmentOnhold = "";
			String flightId = "";
			String scheduleId = "";
			String modelNumber = "";

			String segmentCode = "";
			String segmentId = "";
			String cc = "";
			// String cabinClasses = "";
			String segments = "";
			String segmentIds = "";
			for (AvailableFlightSegment availableFlightSegment : availableConnectedFlight.getAvailableFlightSegments()) {
				if (!flightNumber.equals("")) {
					flightNumber = flightNumber + "<BR>" + availableFlightSegment.getFlightSegmentDTO().getFlightNumber();
				} else {
					flightNumber = availableFlightSegment.getFlightSegmentDTO().getFlightNumber();
				}

				// TODO AvailableAdultConunt/AvailableInfantConunt has removed
				// if(!segmentAvailable.equals("")){
				// segmentAvailable = availableFlightSegment.getCabinClassCode() + "-" + segmentAvailable + ":" +
				// availableFlightSegment.getAvailableAdultConunt() + "/" +
				// availableFlightSegment.getAvailableInfantConunt();
				// }
				// else{
				// segmentAvailable = availableFlightSegment.getAvailableAdultConunt() + "/" +
				// availableFlightSegment.getAvailableInfantConunt();
				// }
				SeatAllocationDTO seatAllocation = availableFlightSegment.getSeatAllocationDTO();
				if (!segmentSold.equals("")) {
					segmentSold = availableFlightSegment.getCabinClassCode() + "-" + segmentSold + ":"
							+ seatAllocation.getSoldSeats() + "/" + seatAllocation.getSoldInfantSeats();
				} else {
					segmentSold = seatAllocation.getSoldSeats() + "/" + seatAllocation.getSoldInfantSeats();
				}

				if (!segmentOnhold.equals("")) {
					segmentOnhold = availableFlightSegment.getCabinClassCode() + "-" + segmentOnhold + ":"
							+ seatAllocation.getOnHoldSeats() + "/" + seatAllocation.getOnholdInfantSeats();
				} else {
					segmentOnhold = seatAllocation.getOnHoldSeats() + "/" + seatAllocation.getOnholdInfantSeats();
				}

				if (!flightId.equals("")) {
					flightId = flightId + ":" + availableFlightSegment.getFlightSegmentDTO().getFlightId();
				} else {
					flightId = availableFlightSegment.getFlightSegmentDTO().getFlightId().toString();
				}

				if (!scheduleId.equals("") && availableFlightSegment.getFlightSegmentDTO().getFlightScheduleId() != null) {
					scheduleId = scheduleId + ":" + availableFlightSegment.getFlightSegmentDTO().getFlightScheduleId();
				} else {
					scheduleId = availableFlightSegment.getFlightSegmentDTO().getFlightScheduleId().toString();
				}

				if (!modelNumber.equals("")) {
					modelNumber = modelNumber + "<BR>" + availableFlightSegment.getFlightSegmentDTO().getFlightModelNumber();
				} else {
					modelNumber = availableFlightSegment.getFlightSegmentDTO().getFlightModelNumber();
				}

				if (!segmentCode.equals("")) {
					segmentCode = segmentCode + ":" + availableFlightSegment.getFlightSegmentDTO().getSegmentCode();
				} else {
					segmentCode = availableFlightSegment.getFlightSegmentDTO().getSegmentCode();
				}

				if (!segmentId.equals("")) {
					segmentId = segmentId + ":" + availableFlightSegment.getFlightSegmentDTO().getSegmentId();
				} else {
					segmentId = availableFlightSegment.getFlightSegmentDTO().getSegmentId().toString();
				}
				cc = availableFlightSegment.getCabinClassCode();
				// if(!cabinClasses.equals("")){
				// cabinClasses = cabinClasses + "," + availableFlightSegment.getCabinClassCode();
				// }
				// else{
				// cabinClasses = availableFlightSegment.getCabinClassCode();
				// }

				if (!segments.equals("")) {
					segments = segments + ":" + availableFlightSegment.getFlightSegmentDTO().getSegmentCode();
				} else {
					segments = availableFlightSegment.getFlightSegmentDTO().getSegmentCode();
				}

				if (!segmentIds.equals("")) {
					segmentIds = segmentIds + ":" + availableFlightSegment.getFlightSegmentDTO().getSegmentId();
				} else {
					segmentIds = availableFlightSegment.getFlightSegmentDTO().getSegmentId().toString();
				}

				if (!allDepartureStations.equals("")) {
					allDepartureStations = allDepartureStations + ":"
							+ availableFlightSegment.getFlightSegmentDTO().getDepartureAirportCode();
				} else {
					allDepartureStations = availableFlightSegment.getFlightSegmentDTO().getDepartureAirportCode();
				}

			}

			departure = availableConnectedFlight.getFirstFlightSegment().getDepartureAirportCode();
			arrival = availableConnectedFlight.getLastFlightSegment().getArrivalAirportCode();

			departureDateTime = dateFormater.format(availableConnectedFlight.getFirstFlightSegment().getDepartureDateTime());
			arrivalDateTime = dateFormater.format(availableConnectedFlight.getLastFlightSegment().getArrivalDateTime());

			// cabinClasses = cabinClasses + "<BR>";
			segments = segments + "<BR>";
			segmentIds = segmentIds + "<BR>";

			long departureTimeGap = availableConnectedFlight.getFirstFlightSegment().getDepartureDateTime().getTime()
					- lngClosureBufferTime;
			if ((allowReprotectPAXToTodaysDepartures.equalsIgnoreCase("Y")) && (departureTimeGap < currentTime)) {
				continue;
			} else {
				String uniqueId = getUniqueIdFromFlightDetails(flightNumber, allDepartureStations, departureDateTime, arrival,
						arrivalDateTime);
				if (flightsToReprotectDTOs.get(uniqueId) != null) {
					FlightsToReprotectDTO flightsToReprotectDTO = flightsToReprotectDTOs.get(uniqueId);
					flightsToReprotectDTO.setSegmentAvailable(flightsToReprotectDTO.getSegmentAvailable() + "<BR>"
							+ segmentAvailable);
					flightsToReprotectDTO.setSegmentSold((flightsToReprotectDTO.getSegmentSold() + "<BR>" + segmentSold));
					flightsToReprotectDTO.setSegmentOnhold((flightsToReprotectDTO.getSegmentOnhold() + "<BR>" + segmentOnhold));
					flightsToReprotectDTO.getCabinClassSet().add(cc);
					flightsToReprotectDTOs.put(uniqueId, flightsToReprotectDTO);
				} else {
					FlightsToReprotectDTO flightsToReprotectDTO = new FlightsToReprotectDTO();
					flightsToReprotectDTO.setFlightNumber(flightNumber);
					flightsToReprotectDTO.setDeparture(departure);
					flightsToReprotectDTO.setDepartureDateTime(departureDateTime);
					flightsToReprotectDTO.setArrival(arrival);
					flightsToReprotectDTO.setArrivalDateTime(arrivalDateTime);
					flightsToReprotectDTO.setOndCode(availableConnectedFlight.getOndCode());
					flightsToReprotectDTO.setSegmentAvailable(segmentAvailable);
					flightsToReprotectDTO.setSegmentSold(segmentSold);
					flightsToReprotectDTO.setSegmentOnhold(segmentOnhold);
					flightsToReprotectDTO.setFlightId(flightId);
					flightsToReprotectDTO.setScheduleId(scheduleId);
					flightsToReprotectDTO.setModelNumber(modelNumber);
					flightsToReprotectDTO.setSegmentCode(segmentCode);
					flightsToReprotectDTO.setCc(cc);
					flightsToReprotectDTO.getCabinClassSet().add(cc);
					flightsToReprotectDTO.setSegmentId(segmentId);
					flightsToReprotectDTO.setSegments(segments);
					// flightsToReprotectDTO.setCabinClasses(cabinClasses);
					flightsToReprotectDTO.setSegmentIds(segmentIds);
					flightsToReprotectDTOs.put(uniqueId, flightsToReprotectDTO);
				}
			}
		}
		if (allowReprotectPAXToTodaysDepartures.equalsIgnoreCase("Y")) {
			request.setAttribute(WebConstants.REQ_TOTAL_RECORDS, String.valueOf(i));
		}

		return createFlightsToReprotectRowHTMLFromDTOs(flightsToReprotectDTOs.values(), currentHTML, request);
	}

	private String getUniqueIdFromFlightDetails(String flightNumber, String departure, String departureDateTime, String arrival,
			String arrivalDateTime) {
		return flightNumber + departure + departureDateTime + arrival + arrivalDateTime;
	}

	private String createFlightsToReprotectRowHTMLFromDTOs(Collection<FlightsToReprotectDTO> flightsToReprotectDTOs,
			String currentHTML, HttpServletRequest request) throws ModuleException {
		int i = Integer.parseInt(request.getAttribute(WebConstants.REQ_TOTAL_RECORDS).toString());
		StringBuffer strMyArr = new StringBuffer(currentHTML);
		for (FlightsToReprotectDTO flightsToReprotectDTO : flightsToReprotectDTOs) {
			strMyArr.append("arrFlightsToReprotectData[" + i + "] = new Array();");
			strMyArr.append("arrFlightsToReprotectData[" + i + "][1] = '" + flightsToReprotectDTO.getFlightNumber() + "';");
			strMyArr.append("arrFlightsToReprotectData[" + i + "][2] = '" + flightsToReprotectDTO.getDeparture() + "';");
			strMyArr.append("arrFlightsToReprotectData[" + i + "][3] = '" + flightsToReprotectDTO.getDepartureDateTime() + "';");
			strMyArr.append("arrFlightsToReprotectData[" + i + "][4] = '" + flightsToReprotectDTO.getArrival() + "';");
			strMyArr.append("arrFlightsToReprotectData[" + i + "][5] = '" + flightsToReprotectDTO.getArrivalDateTime() + "';");
			strMyArr.append("arrFlightsToReprotectData[" + i + "][6] = '" + flightsToReprotectDTO.getOndCode() + "';");
			strMyArr.append("arrFlightsToReprotectData[" + i + "][7] = '" + flightsToReprotectDTO.getSegmentAvailable() + "';");
			strMyArr.append("arrFlightsToReprotectData[" + i + "][8] = '" + flightsToReprotectDTO.getSegmentSold() + "';");
			strMyArr.append("arrFlightsToReprotectData[" + i + "][9] = '" + flightsToReprotectDTO.getSegmentOnhold() + "';");
			strMyArr.append("arrFlightsToReprotectData[" + i + "][10] = '" + flightsToReprotectDTO.getFlightId() + "';");
			strMyArr.append("arrFlightsToReprotectData[" + i + "][11] = '" + flightsToReprotectDTO.getScheduleId() + "';");
			strMyArr.append("arrFlightsToReprotectData[" + i + "][12] = '" + flightsToReprotectDTO.getModelNumber() + "';");
			strMyArr.append("arrFlightsToReprotectData[" + i + "][13] = '';");
			strMyArr.append("arrFlightsToReprotectData[" + i + "][14] = '" + flightsToReprotectDTO.getSegmentCode() + "';");
			String cc = "";
			String cabinClasses = "";
			String cabinClassesDes = "";
			for (String ccVal : flightsToReprotectDTO.getCabinClassSet()) {
				CabinClass cabinClass = null;
				// TODO check this method
				// CabinClass cabinClass = ModuleServiceLocator.getAircraftServiceBD().getCabinClass(ccVal);
				cc = ccVal + ":" + cc;
				cabinClasses = cabinClasses + "," + ccVal;
				if (!cabinClassesDes.equals("")) {
					cabinClassesDes = cabinClassesDes + ","
							+ ((cabinClass != null) ? cabinClass.getCabinClassCode() + ":" + cabinClass.getDescription() : ":");
				} else {
					cabinClassesDes = ((cabinClass != null)
							? cabinClass.getCabinClassCode() + ":" + cabinClass.getDescription()
							: ":");
				}
			}
			cabinClasses = cabinClasses + "<BR>";
			strMyArr.append("arrFlightsToReprotectData[" + i + "][15] = '" + cc + "';");
			strMyArr.append("arrFlightsToReprotectData[" + i + "][16] = '" + flightsToReprotectDTO.getSegmentId() + "';");
			strMyArr.append("arrFlightsToReprotectData[" + i + "][17] = '" + cabinClasses + "';");
			strMyArr.append("arrFlightsToReprotectData[" + i + "][18] = '" + flightsToReprotectDTO.getSegments() + "';");
			strMyArr.append("arrFlightsToReprotectData[" + i + "][19] = '" + flightsToReprotectDTO.getSegmentIds() + "';");
			strMyArr.append("arrFlightsToReprotectData[" + i + "][20] = '" + flightsToReprotectDTO.getDepartureDateTime() + "';");
			strMyArr.append("arrFlightsToReprotectData[" + i + "][21] = 'connection';");
			strMyArr.append("arrFlightsToReprotectData[" + i + "][22] = '" + cabinClassesDes + "';");
			i++;
		}
		if (allowReprotectPAXToTodaysDepartures.equalsIgnoreCase("Y")) {
			request.setAttribute(WebConstants.REQ_TOTAL_RECORDS, String.valueOf(i));
		}
		return strMyArr.toString();
	}

	/**
	 * Sets the Cabin Classes to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void prepareCabinClasses(HttpServletRequest request) throws ModuleException {
		StringBuffer sbCC = new StringBuffer("var arrCabinClassData = new Array();");
		Map<String, String> cabinClasses = CommonsServices.getGlobalConfig().getActiveCabinClassesMap();
		int count = 0;
		for (String cabinClass : cabinClasses.keySet()) {
			sbCC.append("arrCabinClassData[" + count + "] = new Array();");
			sbCC.append("arrCabinClassData[" + count + "][1] = '" + cabinClass + "';");
			sbCC.append("arrCabinClassData[" + count + "][2] = '" + cabinClasses.get(cabinClass) + "';");
			count++;
		}
		request.setAttribute("reqCabinClassData", sbCC.toString());
	}

	/**
	 * Gets the Reprotect Data Group by Logical Cabin Class
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String Array Containing the Reprotect Flight Data
	 */
	@SuppressWarnings("unused")
	public String getReprotectDataByCCCode(HttpServletRequest request) throws ModuleException {

		String sendEmailsForFlightAlterations = globalConfig.getBizParam(SystemParamKeys.SEND_EMAILS_FOR_FLIGHT_ALTERATIONS)
				.equalsIgnoreCase("Y") ? "Y" : "N";
		String sendSMSForFlightAlterations = globalConfig.getBizParam(SystemParamKeys.SEND_SMS_FOR_FLIGHT_ALTERATIONS)
				.equalsIgnoreCase("Y") ? "Y" : "N";

		int flightId = Integer.parseInt(request.getParameter("hdnFlightId"));

		String logicalCCDes = "";
		Integer overlappingFlightId = null;
		StringBuffer strBaseArray = new StringBuffer("var arrBaseArray = new Array();");
		StringBuffer strMyArr = new StringBuffer("var arrReprotectFlightInventories  = new Array();");
		StringBuffer reprotectBuff = new StringBuffer();

		FCCSegInventoryDTO fCCSegInventoryDTO = null;
		Set<String> cabinClasses = CommonsServices.getGlobalConfig().getActiveCabinClassesMap().keySet();
		Map<String, LogicalCabinClass> logicalCCs = ModuleServiceLocator.getLogicalCabinClassServiceBD()
				.getAllLogicalCabinClasses();
		int baseCount = 0;
		int tmpBaseCount = 0;
		for (String cabinClass : cabinClasses) {
			Collection<FCCInventoryDTO> colFccInventories = new ArrayList<FCCInventoryDTO>();
			colFccInventories = ModuleServiceLocator.getFlightInventoryBD().getFCCInventory(flightId, cabinClass,
					overlappingFlightId, AppSysParamsUtil.isEnablePaxReprotectPostCNX());

			if (colFccInventories != null && colFccInventories.size() > 0) {
				Object[] listArr = colFccInventories.toArray();

				FCCInventoryDTO fCCInventoryDTO = null;
				FCCSegInventory fCCSegInventory = null;

				for (Object element : listArr) {

					fCCInventoryDTO = (FCCInventoryDTO) element;
					Collection<FCCSegInventoryDTO> colFccSegInventories = fCCInventoryDTO.getFccSegInventoryDTOs();

					Iterator<FCCSegInventoryDTO> iterFccSegInventories = colFccSegInventories.iterator();
					int count = 0;

					while (iterFccSegInventories.hasNext()) {
						fCCSegInventoryDTO = iterFccSegInventories.next();
						fCCSegInventory = fCCSegInventoryDTO.getFccSegInventory();
						int totalSeats = fCCSegInventory.getSeatsSold() + fCCSegInventory.getSoldInfantSeats()
								+ fCCSegInventory.getOnHoldSeats() + fCCSegInventory.getOnholdInfantSeats();
						if (totalSeats > 0) {
							// contains the inventory seat allocation details by segment code
							strMyArr.append("arrReprotectFlightInventories[" + count + "] = new Array();");
							strMyArr.append("arrReprotectFlightInventories[" + count + "][0] = '" + count + "';");
							strMyArr.append("arrReprotectFlightInventories[" + count + "][1] = '"
									+ fCCSegInventory.getSegmentCode() + "';");
							strMyArr.append("arrReprotectFlightInventories[" + count + "][2] = '"
									+ fCCSegInventory.getSeatsSold() + "/" + fCCSegInventory.getSoldInfantSeats() + "';");

							strMyArr.append("arrReprotectFlightInventories[" + count + "][3] = '"
									+ fCCSegInventory.getOnHoldSeats() + "/" + fCCSegInventory.getOnholdInfantSeats() + "';");

							strMyArr.append("arrReprotectFlightInventories[" + count + "][4] = '';");
							strMyArr.append("arrReprotectFlightInventories[" + count + "][5] = '';");
							strMyArr.append("arrReprotectFlightInventories[" + count + "][6] = '" + flightId + "';");

							strMyArr.append("arrReprotectFlightInventories[" + count + "][7] = '"
									+ fCCSegInventory.getFlightSegId() + "';");

							Flight flight = ModuleServiceLocator.getFlightServiceBD().getFlight(flightId);
							SimpleDateFormat sdfmt = new SimpleDateFormat(dateFormat);
							strBaseArray.append("arrBaseArray[" + tmpBaseCount + "] = new Array();");

							strBaseArray.append("arrBaseArray[" + tmpBaseCount + "][0] = '" + fCCSegInventory.getLogicalCCCode()
									+ "';");
							logicalCCDes = logicalCCs.get(fCCSegInventory.getLogicalCCCode()).getDescription();
							strBaseArray.append("arrBaseArray[" + tmpBaseCount + "][1] = '" + logicalCCDes + "';");

							strBaseArray.append("arrBaseArray[" + tmpBaseCount + "][2] = '" + flight.getFlightNumber() + "';");
							strBaseArray.append("arrBaseArray[" + tmpBaseCount + "][3] = '" + flight.getDestinationAptCode()
									+ "';");
							strBaseArray.append("arrBaseArray[" + tmpBaseCount + "][4] = '" + flight.getOriginAptCode() + "';");
							strBaseArray.append(strMyArr);

							// contains the inventory seat allocation details by segment code
							if (totalSeats > 0) {
								strBaseArray.append("arrBaseArray[" + tmpBaseCount + "][5] = arrReprotectFlightInventories;");
							} else {
								strBaseArray.append("arrBaseArray[" + tmpBaseCount + "][5] = new Array();");
							}

							strBaseArray.append("arrBaseArray[" + tmpBaseCount + "][6] = '" + flight.getFlightId() + "';");
							String depDate = "";
							try {
								depDate = sdfmt.format(flight.getDepartureDate());
							} catch (Exception e) {
								log.error("Error in formatting date" + e.getMessage());
								depDate = "";
							}
							String depDateTime = new SimpleDateFormat(CalendarUtil.PATTERN11)
									.format(flight.getDepartureDate());
							strBaseArray.append("arrBaseArray[" + tmpBaseCount + "][7] = '" + depDate + "';");
							strBaseArray.append("arrBaseArray[" + tmpBaseCount + "][8] = '" + flight.getScheduleId()
									+ "';");
							strBaseArray
									.append("arrBaseArray[" + tmpBaseCount + "][9] = '" + depDateTime + "';");
							tmpBaseCount++;
						}
					}
				}
			}

			baseCount++;
		}
		strBaseArray.append("var sendEmailForPax = '" + sendEmailsForFlightAlterations + "';");
		strBaseArray.append("var sendSMSForPax = '" + sendSMSForFlightAlterations + "';");

		strBaseArray.append("var allowReprotectPAX = '" + allowReprotectPAXToTodaysDepartures + "';");

		reprotectBuff.append(strBaseArray);

		return reprotectBuff.toString();
	}

	/**
	 * Sets the Client Validations For the Reprotect Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array of Client Validations
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {
		String clientErrors;
		Properties moduleErrs = new Properties();
		moduleErrs.setProperty("um.flightschedule.reprotect.form.fromdate.required", "departureDateFromRqrd");
		moduleErrs.setProperty("um.flightschedule.reprotect.form.todate.required", "departureDateToRqrd");
		moduleErrs.setProperty("um.flightschedule.reprotect.form.fromdatelessthancurrentdate.violated",
				"departureDateFromBnsRleVltd");
		moduleErrs.setProperty("um.flightschedule.reprotect.form.depdategreaterthanarrivaldate.violated",
				"departureDateToBnsRleVltd");
		moduleErrs.setProperty("um.flightschedule.reprotect.form.departureandarrivalsame.voilated",
				"departureAndArrivalBnsRleVltd");
		moduleErrs.setProperty("um.flightschedule.reprotect.form.viapoints.invalid", "reprotectViaPointsInvalid");
		moduleErrs.setProperty("um.flightschedule.reprotect.form.reprotectadult.required", "reprotectAdultRqrd");
		moduleErrs.setProperty("um.flightschedule.reprotect.form.reprotectadult.value.invalid", "reprotectAdultValueInvalid");
		moduleErrs.setProperty("um.flightschedule.reprotect.form.reprotectadult.value.greater", "reprotectAdultValueGter");
		moduleErrs.setProperty("um.flightschedule.reprotect.form.reprotectadult.segment.notselected", "reprotectSegNotSelected");
		moduleErrs.setProperty("um.flightschedule.reprotect.form.reprotectadult.segment.morethanone.selected",
				"reprotectSegMoreSelected");
		moduleErrs.setProperty("um.flightschedule.reprotect.form.sameflight.selected", "sameFlightSelected");
		moduleErrs.setProperty("um.flightschedule.reprotect.form.classofservice.notavailable", "classOfServiceNotAvailable");
		moduleErrs.setProperty("um.flightschedule.reprotect.form.reprotectadult.value.empty", "adultValueEmpty");
		moduleErrs.setProperty("um.flightschedule.reprotect.form.grid.notselected", "gridNotSelected");
		moduleErrs.setProperty("um.flightschedule.sms.empty", "smsempty");

		// RollForward Reprotect PAX form validations properties
		moduleErrs.setProperty("um.flightschedule.reprotect.rollforward.form.frequncy.invalid", "rollForwardFrequncyInvalid");
		moduleErrs.setProperty("um.flightschedule.reprotect.rollforward.form.fromdate.required", "rollForwardFromDateRqrd");
		moduleErrs.setProperty("um.flightschedule.reprotect.rollforward.form.tilldate.required", "rollForwardTillDateRqrd");
		moduleErrs.setProperty("um.flightschedule.reprotect.rollforward.form.tilldate.invalid",
				"rollForwardTillDateLessthanFromDate");
		moduleErrs
				.setProperty("um.flightschedule.reprotect.rollforward.form.samescheduledflight.selected", "sameScheduledFlight");
		moduleErrs.setProperty("um.flightschedule.reprotect.rollforward.form.nonscheduledflight.selected", "nonScheduledFlight");
		
		moduleErrs.setProperty("um.flightschedule.reprotect.form.selected.pnradult.value.not.equal", "pnrAdultValueNotEqual");
		
		
		clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		return clientErrors;
	}
}
