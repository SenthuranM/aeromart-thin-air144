package com.isa.thinair.airadmin.core.web.generator.master;

import java.util.Collection;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airmaster.api.model.Country;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author srikantha
 * 
 */
public class CountryHTMLGenerator {

	private static Log log = LogFactory.getLog(CountryHTMLGenerator.class);

	private static String clientErrors;

	private static final String PARAM_SEARCHDATA = "hdnSearchData";

	private static final String PARAM_SEARCH_COUNTRYNAME = "selCountryDesc";

	/**
	 * Gets Country Grid Data Row for the Page No
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array of Country Data
	 * @throws ModuleException
	 *             the ModuleException
	 */
	@SuppressWarnings("unchecked")
	public final String getCountryRowHtml(HttpServletRequest request) throws ModuleException {

		log.debug("inside CountryHTMLGenerator.getCountryRowHtml()");
		List<Country> list = null;

		String strSearchData = request.getParameter(PARAM_SEARCHDATA);

		request.setAttribute("reqCountrySearchData", strSearchData);

		Country CountrySearchDTO = new Country();

		if (request.getParameter(PARAM_SEARCH_COUNTRYNAME) != null && !request.getParameter(PARAM_SEARCH_COUNTRYNAME).equals("")
				&& !request.getParameter(PARAM_SEARCH_COUNTRYNAME).equalsIgnoreCase("ALL")) {
			CountrySearchDTO.setCountryName(request.getParameter(PARAM_SEARCH_COUNTRYNAME));
		}

		int recordNo = 0;
		if (request.getParameter("hdnRecNo") != null && !request.getParameter("hdnRecNo").equals(""))
			recordNo = Integer.parseInt(request.getParameter("hdnRecNo")) - 1;

		Page page = null;

		int totalRecords = 0;
		// If All option selected return all the countries
		if (strSearchData == null || strSearchData.equalsIgnoreCase("ALL") || strSearchData.equals(""))
			page = ModuleServiceLocator.getLocationServiceBD().getCountries(recordNo, 20, null);
		else
			page = ModuleServiceLocator.getLocationServiceBD().getCountries(recordNo, 20, strSearchData);

		totalRecords = page.getTotalNoOfRecords();
		String strJavascriptTotalNoOfRecs = "var totalRecords = " + totalRecords + ";";
		request.setAttribute(WebConstants.REQ_TOTAL_RECORDS, strJavascriptTotalNoOfRecs);
		list = (List<Country>) page.getPageData();
		return createCountryRowHTML(list);
	}

	/**
	 * Creates the Country Grid Array from a Collection of Countrys
	 * 
	 * @param countries
	 *            the Collection of Countrys
	 * @return String the Created Array of grid rows
	 */
	private String createCountryRowHTML(Collection<Country> countries) {

		List<Country> list = (List<Country>) countries;
		Object[] listArr = list.toArray();

		StringBuffer sb = new StringBuffer("var arrData = new Array();");
		Country country = null;

		for (int i = 0; i < listArr.length; i++) {
			country = (Country) listArr[i];

			sb.append("arrData[" + i + "] = new Array();");
			sb.append("arrData[" + i + "][1] = '" + country.getCountryCode() + "';");

			if (country.getCountryName() != null) {
				sb.append("arrData[" + i + "][2] = '" + country.getCountryName() + "';");
			} else {
				sb.append("arrData[" + i + "][2] = '';");
			}
			sb.append("arrData[" + i + "][3] = '" + AiradminUtils.getModifiedStatus(country.getStatus()) + "';");

			if (country.getRemarks() != null) {
				sb.append("arrData[" + i + "][4] = '" + country.getRemarks() + "';");
			} else {
				sb.append("arrData[" + i + "][4] = '';");
			}
			sb.append("arrData[" + i + "][5] = '" + country.getVersion() + "';");
			if (country.getCurrencyCode() != null) {
				sb.append("arrData[" + i + "][6] = '" + country.getCurrencyCode() + "';");
			} else {
				sb.append("arrData[" + i + "][6] = '';");
			}
			if (country.getRegionCode() != null) {
				sb.append("arrData[" + i + "][7] = '" + country.getRegionCode() + "';");
			} else {
				sb.append("arrData[" + i + "][7] = '';");
			}

			sb.append("arrData[" + i + "][8] = '" + AiradminUtils.getEnableDisableStatus(country.getOnholdEnabled()) + "';");

			/*
			 * Send actual value of onholdEnabled field to front-end
			 */
			if (country.getOnholdEnabled() != null) {
				sb.append("arrData[" + i + "][9] = '" + country.getOnholdEnabled() + "';");
			} else {
				sb.append("arrData[" + i + "][9] = '';");
			}

			if (country.getBspEnabled() != null) {
				sb.append("arrData[" + i + "][10] = '" + country.getBspEnabled() + "';");
			} else {
				sb.append("arrData[" + i + "][10] = '';");
			}

			sb.append("arrData[" + i + "][11] = '" + country.getIcaoCode() + "';");

		}
		return sb.toString();
	}

	/**
	 * Creates the Client Validations for Country Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array Containing the Client Validations
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {

		if (clientErrors == null) {
			Properties moduleErrs = new Properties();
			moduleErrs.setProperty("um.airadmin.delete.confirmation", "deleteRecoredCfrm");
			moduleErrs.setProperty("um.country.form.id.required", "countryIdRqrd");
			moduleErrs.setProperty("um.country.form.desc.required", "countryDescRqrd");
			moduleErrs.setProperty("um.country.form.id.already.exist", "countryIdExist");
			moduleErrs.setProperty("um.country.form.currency.required", "currencyCodeRqrd");
			moduleErrs.setProperty("um.country.form.desc.already.exist", "countryDescExist");
			moduleErrs.setProperty("um.country.form.id.length.mustbetwo", "countryIdLenViolates");
			moduleErrs.setProperty("um.country.form.region.required", "regionCodeRqrd");
			moduleErrs.setProperty("um.country.form.iso.alpha3.required", "isoAlpha3CodeRqrd");
			clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}
		return clientErrors;
	}
}
