package com.isa.thinair.airadmin.core.web.v2.action.reports;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airadmin.core.service.AiradminModuleConfig;
import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * 
 * @author rumesh
 * 
 */

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.Reporting.RPTS_PROMO_CODE_DETAILS_JSP),
		@Result(name = S2Constants.Result.ERROR, value = AdminStrutsConstants.AdminJSP.ERROR_JSP) })
public class LoadPromoCodeDetailsReportAction extends BaseRequestResponseAwareAction {

	public String execute() {
		String forward = S2Constants.Result.SUCCESS;
		try {
			String agentmultiSelect = ReportsHTMLGenerator.createAgentGSAMultiSelectStation("All", true, false, "100px", "350px");
			boolean skipReportPeriod = false;
			boolean schedulePromoInfoReport = false;
			AiradminModuleConfig adminCon = AiradminModuleUtils.getConfig();
			skipReportPeriod = (adminCon.getSkipReportPeriod() != null && adminCon.getSkipReportPeriod().equals("true"))
					? true
					: false;
			schedulePromoInfoReport = (adminCon.getSchedulePromoInfoRpt() != null && adminCon.getSchedulePromoInfoRpt().equals(
					"true")) ? true : false;
			request.setAttribute(WebConstants.REQ_SKIP_REPORT_PERIOD, skipReportPeriod);
			request.setAttribute(WebConstants.REQ_SCHEDULE_PROMO_REPORT, schedulePromoInfoReport);
			request.setAttribute(WebConstants.REQ_HTML_AGENTS, agentmultiSelect);
			request.setAttribute(WebConstants.REQ_HTML_CHANNELS, ReportsHTMLGenerator.createSalesChannelHtml());
			request.setAttribute(WebConstants.REQ_HTML_PROMO_TYPES, ReportsHTMLGenerator.createPromoCodesHtml());
			request.setAttribute(WebConstants.REQ_HTML_AIRPORT_LIST, SelectListGenerator.createAirportCodeList());
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, ReportsHTMLGenerator.getClientErrors(request));
			request.setAttribute(WebConstants.REP_SET_LIVE, request.getParameter(WebConstants.REP_HDN_LIVE));
		} catch (ModuleException e) {
			forward = S2Constants.Result.ERROR;
		}
		return forward;
	}
}
