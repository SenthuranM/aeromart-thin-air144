package com.isa.thinair.airadmin.core.web.handler.inventory;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.inventory.INVNotesHG;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Thushara
 * 
 */

public final class INVNotesRH extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(INVNotesRH.class);

	private static final String PARAM_MODE = "hdnMode";

	private static AiradminConfig airadminConfig = new AiradminConfig();

	/**
	 * Main execute method for Inventory User notes
	 * 
	 * @param request
	 * @return
	 */
	public static String execute(HttpServletRequest request) {
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;
		String strMode = request.getParameter(PARAM_MODE);
		if (strMode != null && strMode.equals("VIEW")) {
			try {
				setNotes(request);
			} catch (ModuleException me) {
				if (log.isErrorEnabled()) {
					log.error("Error in geting inventory Notes " + me.getMessage());
				}
				saveMessage(request, me.getMessageString(), WebConstants.MSG_ERROR);
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Error in geting inventory Notes " + e.getMessage());
				}
				if (e instanceof RuntimeException) {
					forward = WebConstants.FORWARD_ERROR;
					JavascriptGenerator.setServerError(request, e.getMessage(), "", "");
				} else {
					saveMessage(request, e.getMessage(), WebConstants.MSG_ERROR);
				}
			}

			forward = AdminStrutsConstants.AdminAction.VIEW;
		}

		if (strMode != null && strMode.equals("SAVE")) {
			try {
				saveNotes(request);
				saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_ADD_SUCCESS), WebConstants.MSG_SUCCESS);

			} catch (ModuleException me) {
				if (log.isErrorEnabled()) {
					log.error("Error in saving inventory Notes " + me.getMessage());
				}
				saveMessage(request, me.getMessageString(), WebConstants.MSG_ERROR);
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Error in saving inventory Notes " + e.getMessage());
				}
				if (e instanceof RuntimeException) {
					forward = WebConstants.FORWARD_ERROR;
					JavascriptGenerator.setServerError(request, e.getMessage(), "", "");
				} else {
					saveMessage(request, e.getMessage(), WebConstants.MSG_ERROR);
				}
			}
		}
		return forward;
	}

	/**
	 * SETS THE USER NOTES TO THE REQUEST
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setNotes(HttpServletRequest request) throws ModuleException {
		String strHtml = INVNotesHG.getUserNotesDetails(request);
		request.setAttribute(WebConstants.REQ_HTML_USR_NOTES, strHtml);
	}

	/**
	 * saves the user notes
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void saveNotes(HttpServletRequest request) throws ModuleException {

		String strFligtSegId = request.getParameter("hdnflightSegId");
		String strUserNote = request.getParameter("txtUsetNotes");
		if (strFligtSegId != null && !strFligtSegId.equals("")) {
			ModuleServiceLocator.getFlightInventoryBD().addInventoryNotes(new Integer(strFligtSegId), strUserNote);
		}

	}

}
