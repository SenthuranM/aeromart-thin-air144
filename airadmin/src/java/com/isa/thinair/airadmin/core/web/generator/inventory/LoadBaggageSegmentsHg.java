package com.isa.thinair.airadmin.core.web.generator.inventory;

import java.util.Collection;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airinventory.api.dto.baggage.FlightBaggageDTO;
import com.isa.thinair.airinventory.api.service.BaggageBusinessDelegate;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.service.FlightBD;
import com.isa.thinair.commons.api.exception.ModuleException;

public class LoadBaggageSegmentsHg {

	/**
	 * Creates the Attached Template codes
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getFlightData(HttpServletRequest request) throws ModuleException {

		BaggageBusinessDelegate baggageBd = ModuleServiceLocator.getBaggageBD();
		FlightBD flightbd = ModuleServiceLocator.getFlightServiceBD();

		Collection<FlightSegement> colSegs = null;
		String strFlightId = request.getParameter("fltId");
		String classOfService = request.getParameter("cos");
		StringBuilder sb = new StringBuilder();

		FlightBaggageDTO fltBaggageDTO = null;
		FlightSegement segment = null;
		Collection<FlightBaggageDTO> colBaggageTempl = null;
		Iterator<FlightSegement> fltIter = null;
		Iterator<FlightBaggageDTO> baggageIter = null;
		int i = 0;
		int segId = 0;
		int baggageSegId = 0;
		boolean isSuceeded = false;
		if (strFlightId != null && !strFlightId.equals("")) {
			Integer flightId = new Integer(strFlightId);
			colSegs = flightbd.getSegments(flightId);
			colBaggageTempl = baggageBd.getBaggageTempleateIds(flightId);
		}

		if (classOfService != null) {
			request.setAttribute("classOfService", classOfService);
		} else {
			classOfService = "";
		}

		if (colSegs != null) {
			fltIter = colSegs.iterator();
			while (fltIter.hasNext()) {
				segment = (FlightSegement) fltIter.next();
				segId = segment.getFltSegId();
				sb.append("arrBaggageSegData[" + i + "] = new Array();");
				sb.append("arrBaggageSegData[" + i + "][1] = '" + segment.getSegmentCode() + "';");
				sb.append("arrBaggageSegData[" + i + "][2] = '" + segment.getFltSegId() + "';");
				sb.append("arrBaggageSegData[" + i + "][3] = '';");
				sb.append("arrBaggageSegData[" + i + "][4] = '" + classOfService + "';");
				String baggageAllowanceRemarks = segment.getBaggageAllowanceRemarks();
				if (baggageAllowanceRemarks == null) {
					baggageAllowanceRemarks = "0";
				}
				sb.append("arrBaggageSegData[" + i + "][5] = '" + baggageAllowanceRemarks + "';");
				if (colBaggageTempl != null) {
					baggageIter = colBaggageTempl.iterator();
					while (baggageIter.hasNext()) {
						fltBaggageDTO = (FlightBaggageDTO) baggageIter.next();
						baggageSegId = fltBaggageDTO.getFlightSegmentID();

						if (baggageSegId == segId && !classOfService.isEmpty() && classOfService.equals(fltBaggageDTO.getCabinClassCode())) {
							sb.append("arrBaggageSegData[" + i + "][3] = '" + fltBaggageDTO.getTemplateId() + "';");
							sb.append("arrBaggageSegData[" + i + "][4] = '" + fltBaggageDTO.getCabinClassCode() + "';");
							isSuceeded = true;
						}
					}
				}
				i++;
			}
		}
		if (isSuceeded) {
			request.setAttribute(WebConstants.BAGGAGE_SCED_STATS, "blnIsSced = true;");
		}
		request.setAttribute("baggageFlightId", strFlightId);
		return sb.toString();
	}

}