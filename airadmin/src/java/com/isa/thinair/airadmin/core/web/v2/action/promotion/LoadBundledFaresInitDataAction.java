package com.isa.thinair.airadmin.core.web.v2.action.promotion;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants.ChargeGroup;
import com.isa.thinair.promotion.api.to.constants.PromotionCriteriaConstants;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * Action class to load configurable data for bundled fare management screen.
 * 
 * @author rumesh
 * 
 */
@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class LoadBundledFaresInitDataAction {

	private static Log log = LogFactory.getLog(LoadPromotionTemplatesManagementAction.class);

	private Collection<String> flightNos;

	private Collection<String> availableBC;

	private Map<String, String> agents;

	private Collection<String> availableAnci;

	private Collection<String> status;

	private Collection<String> selectedFlights;

	private String selectedRoute;
	
	private Map<String, String> availableChannels;

	private Collection<String> searchTags;
	
    private List<String[]> countries;	

	public String execute() {
		try {

			agents = SelectListGenerator.getAgentsCodeNameMap();

			availableAnci = Arrays.asList(PromotionCriteriaConstants.ANCILLARIES.AIRPORT_SERVICE.getCode(),
					PromotionCriteriaConstants.ANCILLARIES.BAGGAGE.getCode(),
					PromotionCriteriaConstants.ANCILLARIES.MEAL.getCode(),
					PromotionCriteriaConstants.ANCILLARIES.SEAT_MAP.getCode(),
					PromotionCriteriaConstants.ANCILLARIES.SSR.getCode(),
					PromotionCriteriaConstants.ANCILLARIES.INSURANCE.getCode());

			status = Arrays.asList(PromotionCriteriaConstants.PromotionStatus.ACTIVE,
					PromotionCriteriaConstants.PromotionStatus.INACTIVE);

			flightNos = SelectListGenerator.getFlightNoWithoutBusList();

			availableBC = SelectListGenerator.getBookingClassCodeList(ChargeGroup.SUR);
			
			availableChannels = SelectListGenerator.getVisibleSalesChannel();
			
			countries = SelectListGenerator.getCountryNameList();

			// searchTags = SelectListGenerator.getSearchTags(SearchType.OND_CODE);
			searchTags = new ArrayList<String>();

		} catch (Exception e) {
			log.error("execute ", e);
		}
		return S2Constants.Result.SUCCESS;
	}

	public Collection<String> getFlightNos() {
		return flightNos;
	}

	public Collection<String> getAvailableAnci() {
		return availableAnci;
	}

	public Map<String, String> getAgents() {
		return agents;
	}

	public Collection<String> getStatus() {
		return status;
	}

	public Collection<String> getAvailableBC() {
		return availableBC;
	}

	public String getSeletedFlights() {
		try {
			selectedFlights = SelectListGenerator.getSelectedFlightNoList(selectedRoute);
		} catch (Exception e) {
			log.error("execute ", e);
		}
		return S2Constants.Result.SUCCESS;
	}

	public Collection<String> getSelectedFlights() {
		return selectedFlights;
	}

	public void setSelectedFlights(Collection<String> selectedFlights) {
		this.selectedFlights = selectedFlights;
	}

	public String getSelectedRoute() {
		return selectedRoute;
	}

	public void setSelectedRoute(String selectedRoute) {
		this.selectedRoute = selectedRoute;
	}

	public Map<String, String> getAvailableChannels() {
		return availableChannels;
	}

	public Collection<String> getSearchTags() {
		return searchTags;
	}

	public void setSearchTags(Collection<String> searchTags) {
		this.searchTags = searchTags;
	}

	public List<String[]> getCountries() {
		return countries;
	}

	public void setCountries(List<String[]> countries) {
		this.countries = countries;
	}	

}
