/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airadmin.core.web.handler.reports;

import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author Chamindap
 * 
 */
public class CustomerTravelHistoryRequestHandler extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(CustomerTravelHistoryRequestHandler.class);

	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	/**
	 * The constructor.
	 */
	public CustomerTravelHistoryRequestHandler() {
		super();
	}

	/**
	 * @param request
	 * @return
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {

		String strHdnMode = request.getParameter("hdnMode");
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;

		try {
			setDisplayData(request);
			log.debug("CustomerTravelHistoryRequestHandler success");
		} catch (Exception e) {
			forward = AdminStrutsConstants.AdminAction.ERROR;
			log.error("CustomerTravelHistoryRequestHandler execute()" + e.getMessage());
		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				log.error("CustomerTravelHistoryRH setReportView Success");
				return null;
			} else {
				log.error("CustomerTravelHistoryRH setReportView not selected");
			}

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("CustomerTravelHistoryRequestHandler setReportView Failed " + e.getMessageString());
		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_DETAIL)) {
				setDetailReportView(request, response);
				log.error("CustomerTravelHistoryRH setDetailReportView Success");
				return null;
			} else {
				log.error("CustomerTravelHistoryRH setDetailReportView not selected");
			}
		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("CustomerTravelHistoryRequestHandler setReportView Failed " + e.getMessageString());
		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_DETAILO)) {
				setDetailHistoryReport(request, response);
				log.error("CustomerTravelHistoryRH setDetailHistoryReport Success");
				return null;
			} else {
				log.error("CustomerTravelHistoryRH setDetailHistoryReport not selected");
			}

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("CustomerTravelHistoryRequestHandler setReportView Failed " + e.getMessageString());
		}

		return forward;

	}

	/**
	 * 
	 * @param request
	 * @throws Exception
	 */
	public static void setDisplayData(HttpServletRequest request) throws Exception {
		setClientErrors(request);
		setAirportComboList(request);
		setNationalityList(request);
		setCountryResidenceList(request);
		setLiveStatus(request);
		setReportingPeriod(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setAirportComboList(HttpServletRequest request) throws ModuleException {
		String strStation = SelectListGenerator.createActiveAirportCodeList();
		request.setAttribute(WebConstants.REQ_STATION_LIST, strStation);
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setNationalityList(HttpServletRequest request) throws ModuleException {
		String strNationality = SelectListGenerator.createNationalityList();
		request.setAttribute(WebConstants.REQ_NATIONALITY_LIST, strNationality);
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setCountryResidenceList(HttpServletRequest request) throws ModuleException {
		String strCountry = SelectListGenerator.createCountryList();
		request.setAttribute(WebConstants.REQ_COUNTRY_LIST, strCountry);
	}

	/**
	 * Sets the report medium to the request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleExceptions
	 */
	private static void setLiveStatus(HttpServletRequest request) throws ModuleException {
		String strLive = request.getParameter(WebConstants.REP_HDN_LIVE);
		if (strLive != null)
			request.setAttribute(WebConstants.REP_SET_LIVE, strLive);

	}

	/**
	 * Setting the reporting period to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setReportingPeriod(HttpServletRequest request) throws ModuleException {
		int period = new Integer(globalConfig.getBizParam(SystemParamKeys.REPORTING_PERIOD)).intValue();
		request.setAttribute(WebConstants.REP_RPT_START_DAY, ReportsHTMLGenerator.getReportingPeriod(period));
	}

	/**
	 * 
	 * @param request
	 */
	public static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		ResultSet resultSet = null;
		String nationality = request.getParameter("selNationality");
		String countryOfRes = request.getParameter("selCountryOfRes");
		String value = request.getParameter("radReportOption");
		String strNationality = request.getParameter("selNationality");
		String strCountry = request.getParameter("selCountryOfRes");
		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String sector = request.getParameter("chkSector");
		String customer = request.getParameter("chkCustomer");
		String sectorFrom = request.getParameter("selSectorFrom");
		String sectorTo = request.getParameter("selSectorTo");
		String firstName = request.getParameter("txtFirstName");
		String lastName = request.getParameter("txtLastName");
		String strLogo = AppSysParamsUtil.getReportLogo(false); 
		String image = "../../images/" + AppSysParamsUtil.getReportLogo(true); 
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String id = "UC_REPM_021";
		String reportTemplate = "CustomeProfileReport.jasper";
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);

		try {
			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));

			ReportsSearchCriteria search = new ReportsSearchCriteria();

			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}

			if (customer != null && !customer.equals("")) {
				if (customer.equals("on")) {
					search.setByCustomer(true);
				}
			}
			if (sector != null && !sector.equals("")) {
				if (sector.equals("on")) {
					search.setBySector(true);
				}
			}
			if (firstName != null && !firstName.equals("")) {
				search.setCustomerFirstName(firstName.trim());
			}
			if (lastName != null && !lastName.equals("")) {
				search.setCustomerLastName(lastName.trim());
			}

			if (strNationality != null && !strNationality.trim().equals("")) {
				search.setNationality(nationality);
				search.setByNationality(true);
			}
			if (strCountry != null && !strCountry.trim().equals("")) {
				search.setCountryOfResidence(countryOfRes);
				search.setByCountryOfResidence(true);
			}

			if (fromDate != null && !fromDate.equals("")) {
				search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate));
			}

			if (toDate != null && !toDate.equals("")) {
				search.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate));
			}

			if (sectorFrom != null && !sectorFrom.equals("")) {
				search.setSectorFrom(sectorFrom);
			}

			if (sectorTo != null && !sectorTo.equals("")) {
				search.setSectorTo(sectorTo);
			}

			resultSet = ModuleServiceLocator.getDataExtractionBD().getCustomerProfileSummaryData(search);

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("FROM_DATE", fromDate);
			parameters.put("TO_DATE", toDate);
			parameters.put("SECTOR", sector);
			parameters.put("SECTOR_FROM", sectorFrom);
			parameters.put("SECTOR_TO", sectorTo);
			parameters.put("OPTION", value);
			parameters.put("ID", id);
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
			parameters.put("REPORT_MEDIUM", strlive);

			// To provide Report Format Options
			String reportNumFormat = request.getParameter("radRptNumFormat");
			ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

			if (value.trim().equals("HTML")) {
				parameters.put("IMG", image);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null, null,
						response);
			} else if (value.trim().equals("PDF")) {
				response.reset();
				response.addHeader("Content-Disposition", "filename=CustomeProfileReport.pdf");
				image = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG", image);
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("EXCEL")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=CustomeProfileReport.xls");
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("CSV")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=CustomeProfileReport.csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);

			}
		} catch (Exception e) {
			log.error(e);
		}
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setDetailReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {
		ResultSet resultSet = null;

		String loginId = request.getParameter("login");
		String value = request.getParameter("option");
		String fromDate = request.getParameter("from");
		String toDate = request.getParameter("to");
		String id = "UC_REPM_021";
		String reportTemplate = "CustomeProfileReportDetail.jasper";
		String strLogo = AppSysParamsUtil.getReportLogo(false); 
		String image = "../../images/" + AppSysParamsUtil.getReportLogo(true); 
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);

		try {
			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));

			ReportsSearchCriteria search = new ReportsSearchCriteria();
			search.setCustomerId(loginId);
			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}
			resultSet = ModuleServiceLocator.getDataExtractionBD().getCustomerProfileDetailData(search);

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("FROM_DATE", fromDate);
			parameters.put("TO_DATE", toDate);
			parameters.put("ID", id);
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);

			// To provide Report Format Options
			String reportNumFormat = request.getParameter("radRptNumFormat");
			ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

			if (value.trim().equals("HTML")) {
				parameters.put("IMG", image);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null, null,
						response);
			} else if (value.trim().equals("PDF")) {
				image = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG", image);
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("EXCEL")) {
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("CSV")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=CustomeProfileReportDetail.csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);

			}
		} catch (Exception e) {
			log.error(e);
		}
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setDetailHistoryReport(HttpServletRequest request, HttpServletResponse response) throws ModuleException {
		ResultSet resultSet = null;
		String loginId = request.getParameter("login");
		String value = request.getParameter("option");
		String fromDate = request.getParameter("from");
		String toDate = request.getParameter("to");
		String sector = request.getParameter("sector");
		String sectorFrom = request.getParameter("sFrom");
		String sectorTo = request.getParameter("sTo");
		String id = "UC_REPM_021";
		String reportTemplate = "CustomerTravelHistorysDetail.jasper";
		String strLogo = AppSysParamsUtil.getReportLogo(false); // globalConfig.getBizParam(SystemParamKeys.REPORT_LOGO);
		String image = "../../images/" + AppSysParamsUtil.getReportLogo(true); // AA145.jpg
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
		DateFormat currentFormat = new SimpleDateFormat("dd-MMM-yyyy");
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);

		try {
			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));

			ReportsSearchCriteria search = new ReportsSearchCriteria();
			search.setCustomerId(loginId);

			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}

			if (fromDate != null && !fromDate.equals("") && toDate != null && !toDate.equals("")) {
				Calendar currentDate = new GregorianCalendar();

				if (ReportsHTMLGenerator.convertDate(toDate).equals(currentFormat.format(currentDate.getTime()))) {
					int day = Integer.parseInt(toDate.substring(0, 2));
					int month = Integer.parseInt(toDate.substring(3, 5));
					int year = Integer.parseInt(toDate.substring(6));

					Calendar cal = new GregorianCalendar();
					cal.set(Calendar.DATE, day);
					cal.set(Calendar.MONTH, (month - 1));
					cal.set(Calendar.YEAR, year);

					String date = dateFormat.format(cal.getTime());
					search.setDateRangeTo(date);
					search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate).concat(" 00:00:00"));
				} else {
					search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate).concat(" 00:00:00"));
					search.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate).concat(" 23:59:59"));
				}
			}

			if (sector != null && !sector.equals("")) {
				if (sector.equals("on")) {
					search.setBySector(true);
					search.setSectorFrom(sectorFrom);
					search.setSectorTo(sectorTo);
				}
			}

			resultSet = ModuleServiceLocator.getDataExtractionBD().getCustomerTravelHistoryDetailData(search);

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("FROM_DATE", fromDate);
			parameters.put("TO_DATE", toDate);
			parameters.put("ID", id);
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);

			// To provide Report Format Options
			String reportNumFormat = request.getParameter("radRptNumFormat");
			ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

			if (value.trim().equals("HTML")) {
				parameters.put("IMG", image);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null, null,
						response);
			} else if (value.trim().equals("PDF")) {
				image = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG", image);
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("EXCEL")) {
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("CSV")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=CustomerTravelHistorysDetail.csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);

			}
		} catch (Exception e) {
			log.error(e);
		}
	}
}