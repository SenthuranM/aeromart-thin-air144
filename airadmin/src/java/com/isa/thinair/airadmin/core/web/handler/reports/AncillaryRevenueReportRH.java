package com.isa.thinair.airadmin.core.web.handler.reports;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleConfig;
import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;

/**
 * @author harshaa
 * 
 */
public class AncillaryRevenueReportRH extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(AncillaryRevenueReportRH.class);

	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();

	public static String execute(HttpServletRequest request, HttpServletResponse response) {
		String strHdnMode = request.getParameter("hdnMode");
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;

		try {
			setDisplayData(request);
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				return null;
			}
		} catch (ModuleException e) {
			forward = AdminStrutsConstants.AdminAction.ERROR;
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("AncillaryRevenueReportRH setReportView Failed " + e.getMessageString());
		} catch (Exception e) {
			forward = AdminStrutsConstants.AdminAction.ERROR;
			log.error("Error in AncillaryRevenueReportRH execute()" + e.getMessage());
		}
		return forward;
	}

	/**
	 * Sets the initial data for the report.
	 * 
	 * @param request
	 * @throws Exception
	 */
	public static void setDisplayData(HttpServletRequest request) throws ModuleException {
		setClientErrors(request);
		setAgentsMultiSelect(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);
	}

	private static void setAgentsMultiSelect(HttpServletRequest request) throws ModuleException {
		String agentType = "All";
		String agentList = ReportsHTMLGenerator.createAgentGSAMultiSelect(agentType, false, false);
		request.setAttribute(WebConstants.REQ_AGENTS_STATION, agentList);
	}

	/**
	 * Sets client error messages
	 * 
	 * @param request
	 */
	public static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);
			AiradminModuleConfig adminCon = AiradminModuleUtils.getConfig();

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
			request.setAttribute(WebConstants.REQ_AGENTS_LIMIT, adminCon.getAgentLimitForReporting());
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Gets the report data and sets to the corresponding view option.
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {
		ReportsSearchCriteria search = null;
		ResultSet resultSet = null;
		ArrayList<String> lstAgents = new ArrayList<String>();
		String byAgentUser = "N";
		String showMeal = "N";
		String showBaggage = "N";
		String showSeat = "N";
		String showInsuarance = "N";
		String showDiscount = "N";
		String id = "UC_REPM_070";
		String strLogo = AppSysParamsUtil.getReportLogo(false);
		String image = "../../images/" + AppSysParamsUtil.getReportLogo(true);
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String strBase = globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY);
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);
		String reportTemplate = "AncillaryRevenueReport.jasper";
		Map<String, Object> parameters = new HashMap<String, Object>();

		search = new ReportsSearchCriteria();
		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String value = request.getParameter("radReportOption");
		String reportCategory = request.getParameter("radReportCat");
		String agents = request.getParameter("hdnAgents");
		String webSales = request.getParameter("chkWebAgents");
		String allAgentsSelected = request.getParameter("hdnAllAgentsSelected");
		boolean blnIncldWebSls = (webSales != null && webSales.equals("on")) ? true : false;
		search.setSelectedAllAgents((allAgentsSelected != null && allAgentsSelected.equals("true")) ? true : false);		

		if (strlive != null) {
			if (strlive.equals(WebConstants.REP_LIVE)) {
				search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
			} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
				search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
			}
		}

		if (isNotEmptyOrNull(fromDate) && isNotEmptyOrNull(toDate)) {
			String strFromDate = "'" + fromDate + " 00:00:00'," + "'DD/MM/YYYY HH24:mi:ss'";
			String strToDate = "'" + toDate + " 23:59:00'," + "'DD/MM/YYYY HH24:mi:ss'";
			search.setDateRangeFrom(strFromDate);
			search.setDateRangeTo(strToDate);

		}else{
			throw new ModuleException("reporting.date.invalid");
		}
		
		
		if (isNotEmptyOrNull(agents)) {
			String[] arrAgents = agents.split(",");
			for (String agent : arrAgents) {
				lstAgents.add(agent);
			}
			search.setAgents(lstAgents);
		}

		ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
				ReportsHTMLGenerator.getReportTemplate(reportTemplate));
		if (isNotEmptyOrNull(reportCategory) && reportCategory.equals("AGENT")) {
			search.setReportOption(reportCategory);
		} else {
			search.setReportOption(reportCategory);
			byAgentUser = "Y";
		}
		search.setByCustomer(!blnIncldWebSls);

		resultSet = ModuleServiceLocator.getDataExtractionBD().getAncillaryRevenueData(search);
		if (AppSysParamsUtil.isShowMeals()) {
			showMeal = "Y";
		}
		if (AppSysParamsUtil.isShowSeatMap()) {
			showSeat = "Y";
		}
		if (AppSysParamsUtil.isShowTravelInsurance()) {
			showInsuarance = "Y";
		}
		if (AppSysParamsUtil.isShowBaggages()) {
			showBaggage = "Y";
		}
		if (AppSysParamsUtil.isPromoCodeEnabled()) {
			showDiscount = "Y";
		}
		parameters.put("BY_AGENT_USER", byAgentUser);
		parameters.put("FROM_DATE", fromDate);
		parameters.put("TO_DATE", toDate);
		parameters.put("ID", id);
		parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
		parameters.put("AMT_1", "(" + strBase + ")");
		parameters.put("REPORT_MEDIUM", value);
		parameters.put("SHW_MEAL", showMeal);
		parameters.put("SHW_SEAT", showSeat);
		parameters.put("SHW_AIG", showInsuarance);
		parameters.put("SHW_BAGGAGE", showBaggage);
		parameters.put("SHW_DISCOUNT", showDiscount);

		// To provide Report Format Options
		String reportNumFormat = request.getParameter("radRptNumFormat");
		ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

		if (value.trim().equals("HTML")) {
			parameters.put("IMG", image);
			ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null, null,
					response);
		} else if (value.trim().equals("PDF")) {
			image = ReportsHTMLGenerator.getReportTemplate(strLogo);
			parameters.put("IMG", image);
			ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);
		} else if (value.trim().equals("EXCEL")) {
			ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);
		} else if (value.trim().equals("CSV")) {
			response.reset();
			response.addHeader("Content-Disposition", "attachment;filename=AncillaryRevenueReport.csv");
			ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);
		}
	}

	private static boolean isNotEmptyOrNull(String str) {
		return !((str == null) || str.trim().equals("") || str.trim().equals("-1"));
	}

}
