package com.isa.thinair.airadmin.core.web.action.flightsched;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Results(@Result(name = AdminStrutsConstants.AdminAction.SUCCESS, type = JSONResult.class, value = ""))
public class UserNoteRollForwardAction extends BaseRequestResponseAwareAction {

	private static Log log = LogFactory.getLog(UserNoteRollForwardAction.class);
	private String jsonRollForwardFlights;
	private String schedId;
	private String userNote;

	public String execute() throws Exception {

		String forward = AdminStrutsConstants.AdminAction.SUCCESS;
		try {

			if (userNote == null) {
				throw new NullPointerException();
			}
			ModuleServiceLocator.getScheduleServiceBD().rollForwardUserNote(getfightIDList(jsonRollForwardFlights), userNote,
					schedId);
		} catch (Exception e) {

		}
		return forward;
	}

	private Collection<Integer> getfightIDList(String jsoString) {
		String[] ids = jsoString.split(",");
		Collection<Integer> list = new ArrayList<Integer>();
		for (int i = 0; i < ids.length; i++) {
			if (ids[i] != "") {
				list.add(Integer.parseInt(ids[i]));
			}
		}
		return list;
	}

	public String getJsonRollForwardFlights() {
		return jsonRollForwardFlights;
	}

	public void setJsonRollForwardFlights(String jsonRollForwardFlights) {
		this.jsonRollForwardFlights = jsonRollForwardFlights;
	}

	public String getSchedId() {
		return schedId;
	}

	public void setSchedId(String schedId) {
		this.schedId = schedId;
	}

	public String getUserNote() {
		return userNote;
	}

	public void setUserNote(String userNote) {
		this.userNote = userNote;
	}

}
