package com.isa.thinair.airadmin.core.web.action.tools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.json.JSONArray;
import org.json.JSONException;
import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.alerting.api.dto.RequestQueueDTO;
import com.isa.thinair.alerting.api.dto.RequestQueueUser;
import com.isa.thinair.alerting.api.model.ApplicablePage;
import com.isa.thinair.alerting.api.model.Queue;
import com.isa.thinair.alerting.api.model.QueueUserDTO;
import com.isa.thinair.alerting.api.model.RequestQueueSearchResult;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ShowRequestTableAction extends BaseRequestAwareAction {

	private final Log log = LogFactory.getLog(getClass());

	private ArrayList<Map<String, Object>> requestsList;
	private int page;
	private int total;
	private int records;
	private String applicablePageIDForSearchQueues;
	private String pageIdlist;
	private String userIdlist;
	private String queueName;
	private String description;
	private String status;
	private int queueId;
	private String stringifyUserIds;
	private String userName; 
	private String pageNamePart; 
	List<RequestQueueUser> userSearchResult; 
	List<ApplicablePage> pagedResult; 
	private boolean success;
	private String message;	

	public String searchRequestQueue() {
		success = true;
		message = "";
		Page<RequestQueueSearchResult> pagedSearchQueueResult = null;
		try {
		pagedSearchQueueResult = (Page<RequestQueueSearchResult>)ModuleServiceLocator.getAlertingBD().searchRequestQueue(
					(this.page - 1) * 5, 5, queueName, applicablePageIDForSearchQueues);
		} catch (ModuleException e) {
			log.error("Search Request Queue Failed");
			success = false;
			message = "um.requestqueue.search.failed";
		}
		if (success) {
			total = pagedSearchQueueResult.getTotalNoOfRecords() / 5;
			records = pagedSearchQueueResult.getTotalNoOfRecords();
			int mod = pagedSearchQueueResult.getTotalNoOfRecords() % 5;

			if (mod > 0) {
				this.total = this.total + 1;
			}
			setRequestsList(getQueueRequestDTOList((List<RequestQueueSearchResult>) pagedSearchQueueResult
					.getPageData()));
		}
		return S2Constants.Result.SUCCESS;
	}

	public ArrayList<Map<String, Object>> getQueueRequestDTOList(
			List<RequestQueueSearchResult> requestList) {
		ArrayList<Map<String, Object>> queueRequestDTOList = new ArrayList<Map<String, Object>>();
		RequestQueueSearchResult requestQueueSearchResult = null;
		Iterator<RequestQueueSearchResult> iterator = requestList.iterator();

		List<String> tempPageIds = null;
		List<String> tempPageNameList = null;
		List<String> tempUserNameList = null;
		List<String> tempUserIdList = null;

		while (iterator.hasNext()) {
			tempPageIds = new ArrayList<String>();
			tempPageNameList = new ArrayList<String>();
			tempUserNameList = new ArrayList<String>();
			tempUserIdList = new ArrayList<String>();

			requestQueueSearchResult = iterator.next();
			RequestQueueDTO requestQueueDto = new RequestQueueDTO();
			requestQueueDto.setQueueId(requestQueueSearchResult.getQueueId());
			requestQueueDto.setQueueName(requestQueueSearchResult.getQueueName());
			requestQueueDto.setDescription(requestQueueSearchResult.getDescription());

			Iterator<ApplicablePage> pageIterator = requestQueueSearchResult.getApplicablePages().iterator();
			ApplicablePage page = null;
			while (pageIterator.hasNext()) {
				page = pageIterator.next();
				tempPageIds.add(page.getPageId());
				tempPageNameList.add(page.getPageName());
			}
			requestQueueDto.setPageIdList(tempPageIds);
			requestQueueDto.setPageNameList(tempPageNameList);

			Iterator<QueueUserDTO> QueueUserIterator = requestQueueSearchResult.getQueueUsers().iterator();
			QueueUserDTO queueUserDto = null;
			while (QueueUserIterator.hasNext()) {
				queueUserDto = QueueUserIterator.next();
				tempUserIdList.add(queueUserDto.getUserId());
				tempUserNameList.add(queueUserDto.getDisplayName());
				requestQueueDto.setUserIdList(tempUserIdList);
				requestQueueDto.setUserNameList(tempUserNameList);
			}
			Map<String, Object> row = new HashMap<String, Object>();
			row.put("queueRequestDTO", requestQueueDto);
			queueRequestDTOList.add(row);
		}
		return queueRequestDTOList;
	}

	public String saveRequestQueue() {
		success = true;
		JSONArray jsonArray;
		List<String> idList = new ArrayList<String>();
		List<String> nameList = new ArrayList<String>();
		try {
			jsonArray = new JSONArray(pageIdlist);
			if (jsonArray != null) {
				int len = jsonArray.length();
				for (int i = 0; i < len; i++) {
					idList.add(jsonArray.getString(i));
				}
			}
			jsonArray = new JSONArray(userIdlist);
			if (jsonArray != null) {
				int len = jsonArray.length();
				for (int i = 0; i < len; i++) {
					nameList.add(jsonArray.getString(i));
				}
			}
		} catch (JSONException e1) {
			success = false;
			e1.printStackTrace();
		}

		try {
			Queue queue = new Queue((UserPrincipal) request.getUserPrincipal());
			queue.setQueueName(queueName);
			queue.setDescription(description);
			queue.setStatus(CommonsConstants.STATUS_ACTIVE);
			ModuleServiceLocator.getAlertingBD()
					.saveRequestQueue(queue, idList,nameList);
			//ModuleServiceLocator.getAlertingBD().addRemoveUsers(nameList, null,
					//queue.getQueueId());
		} catch (ModuleException e) {
			e.printStackTrace();
			success = false;
		}
		return S2Constants.Result.SUCCESS;
	}

	public String deleteRequestQueue() {
		try {
			ServiceResponce response = ModuleServiceLocator.getAlertingBD()  
					.deleteRequestQueue(queueId); 
			this.success = response.isSuccess();
			this.message = response.getResponseCode();
		} catch (ModuleException e) {
			success = false;
		}
		return S2Constants.Result.SUCCESS;
	}

	public String updateRequestQueue() {
		success = true;
		JSONArray jsonArray;
		List<String> oldUserIdList = new ArrayList<String>();
		List<String> idList = new ArrayList<String>();
		List<String> newUserList = new ArrayList<String>();
		List<String> usersToAdd = new ArrayList<String>();
		List<String> usersToDelete = new ArrayList<String>();
		try {
			jsonArray = new JSONArray(stringifyUserIds);
			if (jsonArray != null) {
				int len = jsonArray.length();
				for (int i = 0; i < len; i++) {
					oldUserIdList.add(jsonArray.getString(i));
				}
			}
			jsonArray = new JSONArray(pageIdlist);
			if (jsonArray != null) {
				int len = jsonArray.length();
				for (int i = 0; i < len; i++) {
					idList.add(jsonArray.getString(i));
				}
			}
			jsonArray = new JSONArray(userIdlist);
			if (jsonArray != null) {
				int len = jsonArray.length();
				for (int i = 0; i < len; i++) {
					newUserList.add(jsonArray.getString(i));
				}
			}
		} catch (JSONException e1) {
			e1.printStackTrace();
		}

		for (String newUser : newUserList) {
			boolean notPresent = true;
			for (String oldUser : oldUserIdList) {
				if (newUser.equals(oldUser)) {
					notPresent = false;
				}
			}
			if (notPresent) {
				usersToAdd.add(newUser);
			}
		}

		for (String oldUser : oldUserIdList) {
			boolean notPresent = true;
			for (String newUser : newUserList) {
				if (newUser.equals(oldUser)) {
					notPresent = false;
				}
			}
			if (notPresent) {
				usersToDelete.add(oldUser);
			}
		}

		try {
			Queue queue = (Queue) ModuleServiceLocator.getAlertingBD()
					.readRequestQueue(queueId);
			queue.setUserDetails((UserPrincipal) request.getUserPrincipal());

			queue.setQueueName(queueName);
			queue.setDescription(description);

			ModuleServiceLocator.getAlertingBD().updateRequestQueue(queue, idList);
			ModuleServiceLocator.getAlertingBD().addRemoveUsers(usersToAdd,
					usersToDelete, queueId);

		} catch (ModuleException e) {
			success = false;
			e.printStackTrace();
		}
		return S2Constants.Result.SUCCESS;
	}

	public String searchForUsers() {
		try {
			List<RequestQueueUser> userSearchResultToSet = ModuleServiceLocator
					.getAlertingBD().searchUser(userName);
			setUserSearchResult(userSearchResultToSet);
		} catch (ModuleException e) {

			e.printStackTrace();
		}
		return S2Constants.Result.SUCCESS;
	}

	public String searchForPages() {
		try {
			List<ApplicablePage> pageSearchResult = ModuleServiceLocator
					.getAlertingBD().searchPages(pageNamePart);
			setPagedResult(pageSearchResult);
		} catch (ModuleException e) {
			e.printStackTrace();
		}
		return S2Constants.Result.SUCCESS;
	}
	
	public List<ApplicablePage> getPagedResult() {
		return pagedResult;
	}

	public void setPagedResult(List<ApplicablePage> pagedResult) {
		this.pagedResult = pagedResult;
	}

	public List<RequestQueueUser> getUserSearchResult() {
		return userSearchResult;
	}

	public void setUserSearchResult(List<RequestQueueUser> userSearchResult) {
		this.userSearchResult = userSearchResult;
	}

	public String getApplicablePageIDForSearchQueues() {
		return applicablePageIDForSearchQueues;
	}

	public void setApplicablePageIDForSearchQueues(
			String applicablePageIDForSearchQueues) {
		this.applicablePageIDForSearchQueues = applicablePageIDForSearchQueues;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getPageNamePart() {
		return pageNamePart;
	}

	public void setPageNamePart(String pageNamePart) {
		this.pageNamePart = pageNamePart;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getStringifyUserIds() {
		return stringifyUserIds;
	}

	public void setStringifyUserIds(String stringifyUserIds) {
		this.stringifyUserIds = stringifyUserIds;
	}

	public int getQueueId() {
		return queueId;
	}

	public void setQueueId(int queueId) {
		this.queueId = queueId;
	}

	public String getUserIdlist() {
		return userIdlist;
	}

	public void setUserIdlist(String userIdlist) {
		this.userIdlist = userIdlist;
	}

	public String getPageIdlist() {
		return pageIdlist;
	}

	public void setPageIdlist(String pageIdlist) {
		this.pageIdlist = pageIdlist;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getQueueName() {
		return queueName;
	}

	public void setQueueName(String queueName) {
		this.queueName = queueName;
	}

	public ArrayList<Map<String, Object>> getRequestsList() {
		return requestsList;
	}

	public void setRequestsList(ArrayList<Map<String, Object>> requestsList) {
		this.requestsList = requestsList;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public String execute() {
		return S2Constants.Result.SUCCESS;
	}
}