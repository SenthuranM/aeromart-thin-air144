package com.isa.thinair.airadmin.core.web.handler.reports;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

public class FlightConnectivityReportRH extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(FlightConnectivityReportRH.class);

	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	/**
	 * Main Execute method for Flight Connectivity Report
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @param response
	 *            the HttpServletResponse
	 * @return
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {

		String strHdnMode = request.getParameter("hdnMode");
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;

		try {
			setDisplayData(request);
			log.debug("FlightConnectivityReportRH success");
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				log.error("FlightConnectivityReportRH setReportView Success");
				return null;
			} else {
				log.error("FlightConnectivityReportRH setReportView not selected");
			}
		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("FlightConnectivityReportRH setReportView Failed " + e.getMessageString());
		} catch (Exception e) {
			forward = AdminStrutsConstants.AdminAction.ERROR;
			log.error("FlightConnectivityReportRH execute()" + e.getMessage());
		}

		return forward;
	}

	/**
	 * Sets the Display Data for Scedule Capacity Varience Report Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	public static void setDisplayData(HttpServletRequest request) throws Exception {

		setClientErrors(request);
		setAirportHtml(request);

	}

	/**
	 * Sets the Client Validations to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setClientErrors(HttpServletRequest request) {

		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);
			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}



	/**
	 * Displays the Schedule Capacity Varience Report
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @param response
	 *            the HttpServletResponse
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		ResultSet resultSet = null;
		//String fromDate = request.getParameter("txtFromDate");
		//String toDate = request.getParameter("txtToDate");
		String value = request.getParameter("radReportOption");
		String operationType = request.getParameter("selOperationType");
		//String flightNo = request.getParameter("txtFlightNumber");
		//String from = request.getParameter("selFrom");
		//String to = request.getParameter("selTo");
		String date = request.getParameter("txtFlightDate");
		String flightType = request.getParameter("radFlightOption");
		String operation = request.getParameter("hdnOperation");
		String origin = request.getParameter("selOrigin");
		String destination = request.getParameter("selDestination");
		String id = "UC_REPM_083";
		String reportTemplate = "FlightConnectivityReport.jasper";
		String strLogo = AppSysParamsUtil.getReportLogo(false);
		String image = "../../images/" +  AppSysParamsUtil.getReportLogo(true);
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);

		try {
			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));

			ReportsSearchCriteria search = new ReportsSearchCriteria();

			if (origin != null && !origin.equals("")) {
				search.setOriginAirport(origin);
			}

			if (destination != null && !destination.equals("")) {
				search.setDestinationAirport(destination);
			}

			if (operationType != null && !operationType.equals("")) {
				search.setOperationType(operationType);
			}
			
			if (date != null && !date.equals("")) {
				search.setDate(date);
			}

			if (flightType != null && !flightType.equals("")) {
				search.setFlightType(flightType);
			}

			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}

			// resultSet = ModuleServiceLocator.getDataExtractionBD().getScheduleCapasityVarience(search);
			resultSet = ModuleServiceLocator.getDataExtractionBD().getFlightConnectivityData(search);

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("FROM", origin);
			parameters.put("TO", destination);
			parameters.put("DATE", date);
			parameters.put("FLIGHT_TYPE", flightType);
			parameters.put("ID", id);
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);

			// To provide Report Format Options
			String reportNumFormat = request.getParameter("radRptNumFormat");
			ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

			if (value.trim().equals("HTML")) {
				parameters.put("IMG", image);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null, null,
						response);
			} else if (value.trim().equals("PDF")) {
				response.reset();
				response.addHeader("Content-Disposition", "filename=FlightConnectivityReport.pdf");
				image = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG", image);
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("EXCEL")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=FlightConnectivityReport.xls");
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("CSV")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=FlightConnectivityReport.csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);

			}
		} catch (Exception e) {
			log.error(e);
		}
	}

	/**
	 * Sets the Online Active Airport List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 * 
	 */
	public static void setAirportHtml(HttpServletRequest request) throws ModuleException {
		String strViaPointsList = SelectListGenerator.createActiveAirportCodeList();
		request.setAttribute(WebConstants.REQ_HTML_VIAPOINT_SELECT_LIST, strViaPointsList);
	}
}
