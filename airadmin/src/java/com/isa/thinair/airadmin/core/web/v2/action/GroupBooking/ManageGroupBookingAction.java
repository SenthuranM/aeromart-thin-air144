package com.isa.thinair.airadmin.core.web.v2.action.GroupBooking;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONResult;
import org.apache.struts2.json.JSONUtil;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airreservation.api.dto.GroupBooking.GroupBookingReqRoutesTO;
import com.isa.thinair.airreservation.api.dto.GroupBooking.GroupBookingRequestHistoryTO;
import com.isa.thinair.airreservation.api.dto.GroupBooking.GroupBookingRequestTO;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.JSONFETOParser;

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ManageGroupBookingAction extends BaseRequestAwareAction {

	private GroupBookingRequestTO grpBookingReq;
	private List<GroupBookingReqRoutesTO> groupBookingRoutes;
	private Collection<Map<String, Object>> rows;
	private boolean mainReqestOnly;
	private int page;
	private int total;
	private int records;
	private String msgType;
	private Boolean sharedRequest = Boolean.FALSE;
	private User user;
	private String userID;
	private long requestID;
	private String pnr;
	private String paymentDetails;
	private Map<Long, BigDecimal> reqIdFareMap;

	private Log log = LogFactory.getLog(ManageGroupBookingAction.class);

	public String search() {
		UserPrincipal user = (UserPrincipal) request.getUserPrincipal();
		boolean allowAllStations = false;// checkPrivilege(request, WebConstants.ALLOW_GROUP_BOOKING_FOR_ALL_STATIONS);

		Page<GroupBookingRequestTO> criteriaSearchPage = null;
		int start = (getPage() - 1) * 20;
		Map<String, Object> row;
		try {
			String stationsExist = ModuleServiceLocator.getGroupBookingBD().getUserStation(user.getUserId());
			getGrpBookingReq().setGroupBookingRoutes(new HashSet<GroupBookingReqRoutesTO>(getGroupBookingRoutes()));
			if (allowAllStations) {
				criteriaSearchPage = ModuleServiceLocator.getGroupBookingBD().getGroupBookingRequestDetails(getGrpBookingReq(),
						null, start, mainReqestOnly);
			} else {
				if (!sharedRequest) {
					criteriaSearchPage = ModuleServiceLocator.getGroupBookingBD().getGroupBookingRequestForStations(
							getGrpBookingReq(), user.getAgentStation(), stationsExist, start, mainReqestOnly);
				} else {
					criteriaSearchPage = ModuleServiceLocator.getGroupBookingBD()
							.getGroupBookingRequestForStations(getGrpBookingReq(), null, stationsExist, start, mainReqestOnly);
				}
			}

			this.setRecords(criteriaSearchPage.getTotalNoOfRecords());
			this.setTotal(criteriaSearchPage.getTotalNoOfRecords() / 20);
			int mod = criteriaSearchPage.getTotalNoOfRecords() % 20;
			if (mod > 0) {
				this.setTotal(this.getTotal() + 1);
			}

			Collection<GroupBookingRequestTO> groupRequests = criteriaSearchPage.getPageData();
			List<GroupBookingRequestTO> groupReqList = new ArrayList<GroupBookingRequestTO>(groupRequests);

			Collections.sort(groupReqList, new Comparator<GroupBookingRequestTO>() {
				public int compare(GroupBookingRequestTO p1, GroupBookingRequestTO p2) {
					return p2.getRequestID().compareTo(p1.getRequestID());
				}
			});

			Collection<String> userIds = new ArrayList<String>();
			for (GroupBookingRequestTO groupRequest : groupReqList) {
				userIds.add(groupRequest.getCraeteUserCode());
			}
			Collection<User> users = ModuleServiceLocator.getSecurityBD().getUsersByID(userIds);

			setRows(new ArrayList<Map<String, Object>>());

			int i = 1;

			for (GroupBookingRequestTO groupRequest : groupReqList) {
				for (User usr : users) {
					if (groupRequest.getCraeteUserCode().equals(usr.getUserId())) {
						groupRequest.setAgentName(usr.getDisplayName());
					}
				}
				row = new HashMap<String, Object>();
				row.put("id", ((getPage() - 1) * 20) + i++);
				row.put("groupRequest", groupRequest);
				getRows().add(row);

			}
			setMsgType(S2Constants.Result.SUCCESS);
		} catch (Exception e) {
			setMsgType(S2Constants.Result.ERROR);
			log.error(e);
		}

		return getMsgType();

	}

	public String adminStatusUpdate() {

		try {
			UserPrincipal user = (UserPrincipal) request.getUserPrincipal();
			grpBookingReq.setCraeteUserCode(user.getAgentCode());
			ModuleServiceLocator.getGroupBookingBD().updateGroupBookingAdminStatus(getGrpBookingReq(),
					new HashSet<Long>(reqIdFareMap.keySet()));
			setMsgType(S2Constants.Result.SUCCESS);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			setMsgType(S2Constants.Result.ERROR);
			log.error(e);
		}

		return getMsgType();
	}

	public String approve() {

		try {
			UserPrincipal user = (UserPrincipal) request.getUserPrincipal();
			grpBookingReq.setCraeteUserCode(user.getAgentCode());
			ModuleServiceLocator.getGroupBookingBD().updateApproveStatus(getGrpBookingReq(), reqIdFareMap);
			setMsgType(S2Constants.Result.SUCCESS);
		} catch (Exception e) {
			setMsgType(S2Constants.Result.ERROR);
			log.error(e);
		}

		return getMsgType();
	}

	public String searchGroupBookingHistory() {
		UserPrincipal user = (UserPrincipal) request.getUserPrincipal();
		boolean allowAllStations = false;// checkPrivilege(request, WebConstants.ALLOW_GROUP_BOOKING_FOR_ALL_STATIONS);

		Page<GroupBookingRequestHistoryTO> criteriaSearchPage = null;
		int start = (getPage() - 1) * 20;
		Map<String, Object> row;
		try {

			criteriaSearchPage = ModuleServiceLocator.getGroupBookingBD()
					.getGroupBookingRequestHistory(getGrpBookingReq().getRequestID());

			this.setRecords(criteriaSearchPage.getTotalNoOfRecords());
			this.setTotal(criteriaSearchPage.getTotalNoOfRecords() / 20);
			int mod = criteriaSearchPage.getTotalNoOfRecords() % 20;
			if (mod > 0) {
				this.setTotal(this.getTotal() + 1);
			}

			Collection<GroupBookingRequestHistoryTO> groupRequests = criteriaSearchPage.getPageData();
			List<GroupBookingRequestHistoryTO> groupReqList = new ArrayList<GroupBookingRequestHistoryTO>(groupRequests);

			Collections.sort(groupReqList, new Comparator<GroupBookingRequestHistoryTO>() {
				public int compare(GroupBookingRequestHistoryTO p1, GroupBookingRequestHistoryTO p2) {
					return p2.getHistoryID().compareTo(p1.getHistoryID());
				}
			});
			setRows(new ArrayList<Map<String, Object>>());
			int i = 1;
			for (GroupBookingRequestHistoryTO groupRequest : groupReqList) {
				row = new HashMap<String, Object>();
				row.put("id", ((getPage() - 1) * 20) + i++);
				row.put("groupRequestHostory", groupRequest);
				getRows().add(row);

			}
			setMsgType(S2Constants.Result.SUCCESS);
		} catch (Exception e) {
			setMsgType(S2Constants.Result.ERROR);
			log.error(e);
		}

		return getMsgType();

	}

	public String getUserDetails() {
		try {
			user = ModuleServiceLocator.getSecurityBD().getUser(userID);
			if (requestID > 0) {
				Collection<Reservation> reservation = ModuleServiceLocator.getGroupBookingBD().getReservation(requestID);
				StringBuilder pnrs = new StringBuilder();
				StringBuilder searchPNR = new StringBuilder();
				boolean isFirst = true;
				if (reservation != null) {
					for (Reservation res : reservation) {
						if (isFirst) {
							pnrs.append(res.getPnr());
							searchPNR.append("'" + res.getPnr() + "'");
							isFirst = false;
						} else {
							pnrs.append("," + res.getPnr());
							searchPNR.append(",'" + res.getPnr() + "'");
						}
					}
				}
				pnr = pnrs.toString();
				paymentDetails = ModuleServiceLocator.getGroupBookingBD().getActualPaymentByPnr(searchPNR.toString());
			}
			setMsgType(S2Constants.Result.SUCCESS);
		} catch (Exception e) {
			setMsgType(S2Constants.Result.ERROR);
			log.error(e);
		}
		return getMsgType();
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public Collection<Map<String, Object>> getRows() {
		return rows;
	}

	public void setRows(Collection<Map<String, Object>> rows) {
		this.rows = rows;
	}

	public GroupBookingRequestTO getGrpBookingReq() {
		return grpBookingReq;
	}

	public void setGrpBookingReq(GroupBookingRequestTO grpBookingReq) {
		this.grpBookingReq = grpBookingReq;
	}

	public List<GroupBookingReqRoutesTO> getGroupBookingRoutes() {
		return groupBookingRoutes;
	}

	public void setGroupBookingRoutes(List<GroupBookingReqRoutesTO> groupBookingRoutes) {
		this.groupBookingRoutes = groupBookingRoutes;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	private boolean checkPrivilege(HttpServletRequest request, String privilegeId) {
		Map mapPrivileges = (Map) request.getSession().getAttribute(WebConstants.SES_PRIVILEGE_IDS);

		if (mapPrivileges.get(privilegeId) == null) {
			return false;
		} else {
			return true;
		}

	}

	public Boolean getSharedRequest() {
		return sharedRequest;
	}

	public void setSharedRequest(Boolean sharedRequest) {
		this.sharedRequest = sharedRequest;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public long getRequestID() {
		return requestID;
	}

	public void setRequestID(long requestID) {
		this.requestID = requestID;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public boolean isMainReqestOnly() {
		return mainReqestOnly;
	}

	public void setMainReqestOnly(boolean mainReqestOnly) {
		this.mainReqestOnly = mainReqestOnly;
	}

	public String getPaymentDetails() {
		return paymentDetails;
	}

	public void setPaymentDetails(String paymentDetails) {
		this.paymentDetails = paymentDetails;
	}

	/**
	 * @return the selectedDetails
	 */
	public String getSelectedDetails() {
		return "";
	}

	public void setSelectedDetails(String selectedDetailsData) throws JSONException {
		reqIdFareMap = new HashMap<Long, BigDecimal>();
		List<Map<String, Object>> parsedDataMap = (List<Map<String, Object>>) JSONUtil.deserialize(selectedDetailsData);
		for (Map<String, Object> entry : parsedDataMap) {
			reqIdFareMap.put(Long.parseLong((String) entry.get("key")), new BigDecimal((String) entry.get("value")));
		}
	}
}
