/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */

/**
 * @author srikantha
 *
 *
 * Window - Preferences - Java - Code Style - Code Templates
 */

package com.isa.thinair.airadmin.core.web.generator.tools;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airreservation.api.dto.CashSalesHistoryDTO;
import com.isa.thinair.airreservation.api.dto.CashSalesStatusDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

public class CashSalesTransferHTMLGenerator {

	private static String clientErrors;

	// searching parameters
	private static final String PARAM_SEARCH_AGENTCODE = "selAgent";

	private static final String PARAM_SEARCH_FROM = "txtFrom";

	private static final String PARAM_SEARCH_TO = "txtTo";

	private static final String PARAM_USER_LIST = "hdnUsers";

	private static final String PARAM_UI_MODE = "hdnUIMode";

	private String strFormFieldsVariablesJS = "";

	public final String getCashSalesTransferRowHtml(HttpServletRequest request) throws ModuleException, Exception {

		String strUIMode = request.getParameter(PARAM_UI_MODE);
		String strUIModeJS = "var isSearchMode = false; var isAgentSelect = false;";
		String strAgentCode = "";
		String strFromDate = "";
		String strToDate = "";
		String strSelectedUsers = "";
		Date fromDate = null;
		Date toDate = null;
		List<CashSalesHistoryDTO> list = null;
		String strTotalCash = "";

		if (strUIMode != null && strUIMode.equals("AGENTCHANGE")) {

			strUIModeJS = "var isAgentSelect = true;";
			strUIModeJS += "var isSearchMode = false;";

			strAgentCode = request.getParameter(PARAM_SEARCH_AGENTCODE);
			strFormFieldsVariablesJS = "var agentCode ='" + strAgentCode + "';";

			strFromDate = request.getParameter(PARAM_SEARCH_FROM);
			strFormFieldsVariablesJS += "var fromDate ='" + strFromDate + "';";

			strToDate = request.getParameter(PARAM_SEARCH_TO);
			strFormFieldsVariablesJS += "var toDate ='" + strToDate.trim() + "';";

			strSelectedUsers = request.getParameter(PARAM_USER_LIST);
			strFormFieldsVariablesJS += "var selectedUsers ='" + strSelectedUsers + "';";

			request.setAttribute(WebConstants.REQ_OPERATION_UI_MODE, strUIModeJS);
			return null;

		} else if (strUIMode != null && strUIMode.equals(WebConstants.ACTION_SEARCH)) {

			strUIModeJS = "var isSearchMode = true;";
			strUIModeJS += "var isAgentSelect = false;";

			strAgentCode = request.getParameter(PARAM_SEARCH_AGENTCODE);
			strFormFieldsVariablesJS = "var agentCode ='" + strAgentCode + "';";

			strFromDate = request.getParameter(PARAM_SEARCH_FROM);
			strFormFieldsVariablesJS += "var fromDate ='" + strFromDate + "';";

			strToDate = request.getParameter(PARAM_SEARCH_TO);
			strFormFieldsVariablesJS += "var toDate ='" + strToDate.trim() + "';";

			strSelectedUsers = request.getParameter(PARAM_USER_LIST);
			strFormFieldsVariablesJS += "var SelectedUsers ='" + strSelectedUsers + "';";

			String strAssignUserValues = request.getParameter(PARAM_USER_LIST);

			SimpleDateFormat dateFormat = null;

			if (!strFromDate.equals("")) {
				if (strFromDate.indexOf('-') != -1) {
					dateFormat = new SimpleDateFormat("dd-MM-yy");
				}
				if (strFromDate.indexOf('/') != -1) {
					dateFormat = new SimpleDateFormat("dd/MM/yy");
				}
				if (strFromDate.indexOf(' ') != -1) {
					strFromDate = strFromDate.substring(0, strFromDate.indexOf(' '));
				}
				fromDate = dateFormat.parse(strFromDate);
			}
			if (!strToDate.equals("")) {
				if (strToDate.indexOf('-') != -1) {
					dateFormat = new SimpleDateFormat("dd-MM-yy");
				}
				if (strFromDate.indexOf('/') != -1) {
					dateFormat = new SimpleDateFormat("dd/MM/yy");
				}
				if (strFromDate.indexOf(' ') != -1) {
					strToDate = strToDate.substring(0, strToDate.indexOf(' '));
				}
				toDate = dateFormat.parse(strToDate);
			}

			String arrUsers[] = strAssignUserValues.split(",");
			request.setAttribute(WebConstants.REQ_OPERATION_UI_MODE, strUIModeJS);

			CashSalesStatusDTO cashSalesStatusDTO = ModuleServiceLocator.getReservationAuxilliaryBD().getCashSalesDetails(
					arrUsers, fromDate, toDate, strAgentCode);
			if (cashSalesStatusDTO != null) {
				list = cashSalesStatusDTO.getCashSales();
				strTotalCash = cashSalesStatusDTO.getAmountSatus();
			}
			request.getSession().setAttribute("historylist", list);
			request.getSession().setAttribute("agent_code", strAgentCode);
			if (strTotalCash != null) {
				request.setAttribute("total_cash", strTotalCash);
			}

		} else {

			strUIModeJS = "var isSearchMode = false;";
			strUIModeJS += "var isAgentSelect = false;";
			strFormFieldsVariablesJS = "var agentCode ='';";
			strFormFieldsVariablesJS += "var fromDate ='';";
			strFormFieldsVariablesJS += "var toDate ='';";
			strFormFieldsVariablesJS += "var SelectedUsers ='';";
			request.setAttribute(WebConstants.REQ_OPERATION_UI_MODE, strUIModeJS);
		}

		setFormFieldValues(strFormFieldsVariablesJS);
		return createCashSalesTransferRowHTML(list);
	}

	private String createCashSalesTransferRowHTML(Collection<CashSalesHistoryDTO> listCashSalesTransfers) {

		List<CashSalesHistoryDTO> list = (List<CashSalesHistoryDTO>) listCashSalesTransfers;
		Object[] listArr = list.toArray();
		StringBuffer sb = new StringBuffer("var arrCashData = new Array();");
		CashSalesHistoryDTO cashSalesHistory = null;

		for (int i = 0; i < listArr.length; i++) {
			cashSalesHistory = (CashSalesHistoryDTO) listArr[i];

			sb.append("arrCashData[" + i + "] = new Array();");
			if (cashSalesHistory.getDateOfSale() != null) {
				sb.append("arrCashData[" + i + "][1] = '" + changeDateFormat(cashSalesHistory.getDateOfSale()) + "';");
			} else {
				sb.append("arrCashData[" + i + "][1] = '';");
			}

			if (cashSalesHistory.getStaffId() != null) {
				sb.append("arrCashData[" + i + "][2] = '" + cashSalesHistory.getStaffId() + "';");
			} else {
				sb.append("arrCashData[" + i + "][2] = '';");
			}

			if (cashSalesHistory.getTotal().doubleValue() != 0) {
				sb.append("arrCashData[" + i + "][3] = '" + cashSalesHistory.getTotal() + "';");
			} else {
				sb.append("arrCashData[" + i + "][3] = '0.00';");
			}
			if (cashSalesHistory.getTransferTimeStamp() != null) {

				sb.append("arrCashData[" + i + "][4] = '"
						+ changeTimestampFormat(new Date(cashSalesHistory.getTransferTimeStamp().getTime())) + "';");
			} else {
				sb.append("arrCashData[" + i + "][4] = '';");
			}

			if (cashSalesHistory.getTransferStatus() != null) {
				if (cashSalesHistory.getTransferStatus().equals("N"))
					sb.append("arrCashData[" + i + "][5] = 'Not Transferred';");
				else if (cashSalesHistory.getTransferStatus().equals("Y"))
					sb.append("arrCashData[" + i + "][5] = 'Transferred';");
				else
					sb.append("arrCashData[" + i + "][5] = 'Not Generated';");
			} else {
				sb.append("arrCashData[" + i + "][5] = '';");
			}

			if (cashSalesHistory.getTransferStatus() != null) {
				if (cashSalesHistory.getTransferStatus().equals("N"))
					sb.append("arrCashData[" + i + "][6] = 'T';"); // T-Transfer, G-Generate
				else if (cashSalesHistory.getTransferStatus().equals("Y"))
					sb.append("arrCashData[" + i + "][6] = 'T';");
				else
					sb.append("arrCashData[" + i + "][6] = 'G';");
			} else {
				sb.append("arrCashData[" + i + "][6] = '';");
			}

			sb.append("arrCashData[" + i + "][7] = 'false';");

			if (cashSalesHistory.getDisplayName() != null) {
				sb.append("arrCashData[" + i + "][8] = '" + cashSalesHistory.getDisplayName() + "';");
			} else {
				sb.append("arrCashData[" + i + "][8] = 'All Users';");
			}
			if (cashSalesHistory.getTransferStatus() != null) {
				sb.append("arrCashData[" + i + "][9] = '" + cashSalesHistory.getTransferStatus() + "';");
			} else {
				sb.append("arrCashData[" + i + "][9] = '';");
			}
		}
		return sb.toString();
	}

	public static String getClientErrors(HttpServletRequest request) throws ModuleException {

		if (clientErrors == null) {
			Properties moduleErrs = new Properties();

			moduleErrs.setProperty("um.airadmin.generate.cash.confirmation", "generateRecoredCfrm");
			moduleErrs.setProperty("um.airadmin.transfer.cash.confirmation", "transferRecoredCfrm");
			moduleErrs.setProperty("um.airadmin.retransfer.cash.confirmation", "reTransferRecoredCfrm");

			moduleErrs.setProperty("um.airadmin.see.log", "seeLogEntry");
			moduleErrs.setProperty("um.cashsales.form.from.date.required", "fromDateRqrd");
			moduleErrs.setProperty("um.cashsales.form.to.date.required", "toDateRqrd");
			moduleErrs.setProperty("um.cashsales.form.from.date.invalid", "fromDateInvalid");
			moduleErrs.setProperty("um.cashsales.form.to.date.invalid", "toDateInvalid");
			moduleErrs.setProperty("um.cashsales.form.agent.required", "agentRqrd");
			moduleErrs.setProperty("um.cashsales.form.user.required", "userRqrd");
			moduleErrs.setProperty("um.cashsales.form.todate.lessthan.fromdate", "todateLessthanFromDate");
			moduleErrs.setProperty("um.cashsales.form.nousers.for.agent.selected", "noUsersForAgent");
			moduleErrs.setProperty("um.cashsales.form.todate.lessthan.currentdate", "todateLessthanCurrentDate");

			clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}

		return clientErrors;
	}

	private void setFormFieldValues(String strFormFieldsVariablesJS) {

		this.strFormFieldsVariablesJS = strFormFieldsVariablesJS;
	}

	public String getFormFieldValues() {

		return strFormFieldsVariablesJS;
	}

	private String changeDateFormat(Date date) {
		SimpleDateFormat dtfmt = new SimpleDateFormat("dd/MM/yyyy");
		return dtfmt.format(date);
	}

	private String changeTimestampFormat(Date date) {
		SimpleDateFormat dtfmt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		return dtfmt.format(date);
	}
}
