package com.isa.thinair.airadmin.core.web.generator.inventory;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airadmin.core.web.util.SeatInventoryHelper;
import com.isa.thinair.airinventory.api.dto.FCCInventorySummaryDTO;
import com.isa.thinair.airinventory.api.dto.FCCSegInvSummaryDTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.reporting.api.criteria.FlightSearchCriteria;
import com.isa.thinair.reporting.api.model.FlightInventorySummaryDTO;

public class MISearchHTMLGenerator {

	private static Log log = LogFactory.getLog(MISearchHTMLGenerator.class);
	private static SeatInventoryHelper seatInventoryHelper = new SeatInventoryHelper();
	private FlightSearchCriteria flightSearchCriteria = null;

	@SuppressWarnings("unchecked")
	public final String getSeatInventoryRowHtml(HttpServletRequest request) throws ModuleException {

		AiradminUtils.debug(log, "inside SeatInventoryHTMLGenerator.getSeatInventoryRowHtml()");
		String strMode = request.getParameter("hdnMode");
		flightSearchCriteria = new FlightSearchCriteria();

		String strDepartureDateFrom = AiradminUtils.getNotNullString(request.getParameter("txtDepartureDateFrom"));
		String strDepartureDateTo = AiradminUtils.getNotNullString(request.getParameter("txtDepartureDateTo"));
		String strFlightNumber = AiradminUtils.getNotNullString(request.getParameter("txtFlightNumber"));
		String strTimeInAirport = AiradminUtils.getNotNullString(request.getParameter("radTime"));
		String strFltStatus = request.getParameter("selFltStatus");
		String strDeparture = request.getParameter("selDeparture");
		String strArrival = request.getParameter("selArrival");
		String strVia1 = request.getParameter("selVia1");
		String strVia2 = request.getParameter("selVia2");
		String strVia3 = request.getParameter("selVia3");
		String strVia4 = request.getParameter("selVia4");
		String strCabinClass = request.getParameter("selCabinClass");
		String strMinSFactor = request.getParameter("txtMinSeatFactor");
		String strMaxSFactor = request.getParameter("txtMaxSeatFactor");

		Collection<FlightInventorySummaryDTO> flightCollections = null;

		int recordNo = 0;
		if (request.getParameter("hdnRecNo") != null && !request.getParameter("hdnRecNo").equals("")) {
			recordNo = Integer.parseInt(request.getParameter("hdnRecNo")) - 1;
		}
		// AiradminUtils.debug(log, "Record Number: " + recordNo);

		Page page = null;
		int totalRecords = 0;
		String strJavascriptTotalNoOfRecs = "";

		// if Search button is pressed
		if (strMode != null && strMode.equals(WebConstants.ACTION_SEARCH)) {

			// preparing and setting the departureDateFrom date
			flightSearchCriteria.setDepartureDateFrom(AiradminUtils.getFormattedDate(strDepartureDateFrom));

			// preparing and setting the departureDatTo date
			flightSearchCriteria.setDepartureDateTo(AiradminUtils.getFormattedDate(strDepartureDateTo));

			if (strArrival != null && !(strArrival.equals("-1")))
				flightSearchCriteria.setDestination(strArrival);

			if (strDeparture != null && !(strDeparture.equals("-1")))
				flightSearchCriteria.setOrigin(strDeparture);

			if (strFlightNumber != null && !strFlightNumber.equals(""))
				flightSearchCriteria.setFlightNumber(strFlightNumber + "%");
			else
				flightSearchCriteria.setFlightNumber("%");

			// setting the via points
			ArrayList<String> viaList = new ArrayList<String>();

			if (strVia1 != null && !(strVia1.equals("-1")))
				viaList.add(strVia1);

			if (strVia2 != null && !(strVia2.equals("-1"))) {
				if (strVia1 == null || strVia1.equals(""))
					viaList.add(null);
				viaList.add(strVia2);
			}

			if (strVia3 != null && !(strVia3.equals("-1"))) {
				if (strVia1 == null || strVia1.equals(""))
					viaList.add(null);
				if (strVia2 == null || strVia2.equals(""))
					viaList.add(null);
				viaList.add(strVia3);
			}

			if (strVia4 != null && !(strVia4.equals("-1"))) {
				if (strVia1 == null || strVia1.equals(""))
					viaList.add(null);
				if (strVia2 == null || strVia2.equals(""))
					viaList.add(null);
				if (strVia3 == null || strVia3.equals(""))
					viaList.add(null);
				viaList.add(strVia4);
			}

			// setting the cabin class code search criteria
			if (strCabinClass != null && !(strCabinClass.equals("-1")))
				flightSearchCriteria.setCabinClassCode(strCabinClass);

			// AiradminUtils.debug(log, "CC Code: " + strCabinClass);

			flightSearchCriteria.setViaPoints(viaList);

			if (!strFltStatus.equals("-1")) {
				flightSearchCriteria.addFlightStatus(strFltStatus);
			}

			if (strTimeInAirport != null && strTimeInAirport.equals("L")) {
				flightSearchCriteria.setTimeInLocal(true);
			} else {
				flightSearchCriteria.setTimeInLocal(false);
			}

			flightSearchCriteria.setSeatFactor(setSeatFactor(strMinSFactor, strMaxSFactor));

			page = ModuleServiceLocator.getDataExtractionBD().searchFlightsForInventoryUpdation(flightSearchCriteria, recordNo,
					20);

			if (page != null) {
				flightCollections = page.getPageData();
				totalRecords = page.getTotalNoOfRecords();
			}

			strJavascriptTotalNoOfRecs = "var totalRecords = " + totalRecords + ";";
			request.setAttribute(WebConstants.REQ_TOTAL_RECORDS, strJavascriptTotalNoOfRecs);
		}
		return createSeatInventoryRowHTML(flightCollections);
	}

	private static int[] setSeatFactor(String min, String max) {
		try {
			// return (min != null && max != null && !min.equals("") && !max.equals("") ? new int
			// []{Integer.parseInt(min),Integer.parseInt(max)} : new int[]{0, 100});
			if (min == null || min.equals(""))
				min = "0";
			if (max == null || max.equals(""))
				max = "100";
			return new int[] { Integer.parseInt(min), Integer.parseInt(max) };
		} catch (NumberFormatException e) {
			return new int[] { 0, 100 };
		}
	}

	/**
	 * Creates the Flights Grid Row from a Collection of FlightSummaryDTOs
	 * 
	 * @param colFlightCollections
	 *            the Collection of Flight Summarys DTOs
	 * @return String the Created Grid Array
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private String createSeatInventoryRowHTML(Collection<FlightInventorySummaryDTO> colFlightCollections) throws ModuleException {

		Date currentDate = new Date();

		StringBuffer sb = new StringBuffer("var f = new Array();");

		String defaultCabinClass = seatInventoryHelper.getDefaultCabinClass();

		if (colFlightCollections != null) {
			Iterator<FlightInventorySummaryDTO> iter = colFlightCollections.iterator();
			int i = 0;

			while (iter.hasNext()) {

				FlightInventorySummaryDTO flightSummary = iter.next();

				sb.append("f[" + i + "] = new Array();");
				// Flight Number
				sb.append("f[" + i + "][1] = '" + flightSummary.getFlightNumber() + "';");

				// Departure
				if (flightSummary.getDeparture() != null) {
					sb.append("f[" + i + "][2] = '" + flightSummary.getDeparture() + "';");
				} else {
					sb.append("f[" + i + "][2] = '';");
				}

				SimpleDateFormat dateFormater = new SimpleDateFormat("dd-MM-yyyy HH:mm");

				// Departure Date Time
				if (flightSummary.getDepartureDateTime() != null) {
					sb.append("f[" + i + "][3] = '" + dateFormater.format(flightSummary.getDepartureDateTime()) + "';");

				} else {
					sb.append("f[" + i + "][3] = '';");
				}

				// Arrival
				if (flightSummary.getArrival() != null) {
					sb.append("f[" + i + "][4] = '" + flightSummary.getArrival() + "';");
				} else {
					sb.append("f[" + i + "][4] = '';");
				}

				// Arrival Date Time
				if (flightSummary.getArrivalDateTime() != null) {
					sb.append("f[" + i + "][5] = '" + dateFormater.format(flightSummary.getArrivalDateTime()) + "';");
				} else {
					sb.append("f[" + i + "][5] = '';");
				}

				List<String> viaPointsList = flightSummary.getViaPoints();
				String strViaPoints = "";
				if (viaPointsList != null) {
					for (int q = 0; q < viaPointsList.size(); q++) {
						strViaPoints += viaPointsList.get(q);
						if (q != (viaPointsList.size() - 1)) {
							if ((q % 2 == 0) && (q != 0)) {
								strViaPoints += "<BR>";
							} else {
								strViaPoints += ",";
							}
						}
					}
				}
				// Via points
				sb.append("f[" + i + "][6] = '" + strViaPoints + "';");

				// Aircraft Model
				if (flightSummary.getModelNumber() != null) {
					sb.append("f[" + i + "][7] = '" + flightSummary.getModelNumber() + "';");

				} else {
					sb.append("f[" + i + "][7] = '';");
				}

				// preparing inventory data
				Object[] fccInventories = flightSummary.getFccInventorySummaryDTO().toArray();
				int ccCount = fccInventories.length;
				String strCabinClassCodes = "";
				int fccSegInvCount = 0;
				String[] cabinClasses = new String[ccCount];
				int[][] segmentAllocations = null;
				int[][] segmentInfantAllocations = null;
				int[][] segmentSold = null;
				int[][] segmentOnHold = null;
				int[][] segmentInfantSold = null;
				int[][] segmentInfantOnHold = null;
				int[][] segmentSeatFactor = null;
				for (int p = 0; p < ccCount; p++) {
					FCCInventorySummaryDTO fltIntrySummry = (FCCInventorySummaryDTO) fccInventories[p];

					// prepare cabinclasses in flight
					if (strCabinClassCodes.equals("")) {
						strCabinClassCodes += fltIntrySummry.getCabinClassCode();
					} else {
						strCabinClassCodes += "#" + fltIntrySummry.getCabinClassCode();
					}

					String cabinClassCode = fltIntrySummry.getCabinClassCode();
					cabinClasses[p] = cabinClassCode;

					Object[] fccSegInventories = fltIntrySummry.getFccSegInventorySummary().toArray();
					fccSegInvCount = fccSegInventories.length;
					segmentAllocations = new int[ccCount][fccSegInvCount];
					segmentInfantAllocations = new int[ccCount][fccSegInvCount];
					segmentOnHold = new int[ccCount][fccSegInvCount];
					segmentSold = new int[ccCount][fccSegInvCount];
					segmentInfantSold = new int[ccCount][fccSegInvCount];
					segmentInfantOnHold = new int[ccCount][fccSegInvCount];
					segmentSeatFactor = new int[ccCount][fccSegInvCount];

					for (int q = 0; q < fccSegInvCount; q++) {
						FCCSegInvSummaryDTO fltSegIntrySummry = (FCCSegInvSummaryDTO) fccSegInventories[q];
						segmentAllocations[p][q] = fltSegIntrySummry.getAdultAllocation();
						segmentInfantAllocations[p][q] = fltSegIntrySummry.getInfantAllocation();
						segmentOnHold[p][q] = fltSegIntrySummry.getAdultOnhold();
						segmentSold[p][q] = fltSegIntrySummry.getAdultSold();
						segmentInfantSold[p][q] = fltSegIntrySummry.getInfantSold();
						segmentInfantOnHold[p][q] = fltSegIntrySummry.getInfantOnhold();
						segmentSeatFactor[p][q] = fltSegIntrySummry.getSeatFactor();
					}
				}

				StringBuffer segmentAllocationsBuff = new StringBuffer();
				StringBuffer segmentOnHoldBuff = new StringBuffer();
				StringBuffer segmentSoldBuff = new StringBuffer();
				List<String> cabinClassesList = new ArrayList<String>();
				StringBuilder strSeatFactor = new StringBuilder();

				for (int n = 0; n < fccSegInvCount; n++) {
					for (int m = 0; m < ccCount; m++) {
						segmentAllocationsBuff.append(segmentAllocations[m][n] + "/" + segmentInfantAllocations[m][n]
								+ ((m == (ccCount - 1)) ? "<BR>" : ","));
						segmentOnHoldBuff.append(segmentOnHold[m][n] + "/" + segmentInfantOnHold[m][n]
								+ ((m == (ccCount - 1)) ? "<BR>" : ","));
						segmentSoldBuff.append(segmentSold[m][n] + "/" + segmentInfantSold[m][n]
								+ ((m == (ccCount - 1)) ? "<BR>" : ","));
						if (!cabinClassesList.contains(cabinClasses[m]))
							cabinClassesList.add(cabinClasses[m]);

						strSeatFactor.append(segmentSeatFactor[m][n]).append("<BR>");
					}
				}

				// Allocated Adults/Infants (F/J/Y)
				sb.append("f[" + i + "][8] = '" + segmentAllocationsBuff.toString() + "';");

				// Seats Sold Adult/Infant (F/J/Y)
				sb.append("f[" + i + "][9] = '" + segmentSoldBuff.toString() + "';");

				// Seats on Hold Adult/Infant (F/J/Y)
				sb.append("f[" + i + "][10] = '" + segmentOnHoldBuff.toString() + "';");

				// flight id
				sb.append("f[" + i + "][11] = '" + flightSummary.getFlightId() + "';");
				sb.append("f[" + i + "][12] = '';");
				sb.append("f[" + i + "][13] = '" + strCabinClassCodes + "';");

				sb.append("f[" + i + "][14] = '';");
				sb.append("f[" + i + "][15] = '';");
				sb.append("f[" + i + "][16] = '" + seatInventoryHelper.prepareCabinClassCodeList(cabinClassesList) + "';");
				sb.append("f[" + i + "][17] = '" + flightSummary.getFlightStatus() + "';");
				sb.append("f[" + i + "][18] = '" + defaultCabinClass + "';");
				sb.append("f[" + i + "][19] = '" + flightSummary.getOverlappingFlightId() + "';");

				// Checking for disabling seg and bc inventories
				int intCompareDate = currentDate.compareTo(flightSummary.getDepartureDateTime());
				if (intCompareDate > 0 || flightSummary.getFlightStatus().equals("CNX")
						|| flightSummary.getFlightStatus().equals("CLS")) {
					sb.append("f[" + i + "][20] = 'N';"); // Cannot be edited
				} else {
					sb.append("f[" + i + "][20] = 'Y';"); // Can be edited
				}
				sb.append("f[" + i + "][21] = '" + flightSummary.getAvailableSeatKilometers() + "';");
				sb.append("f[" + i + "][22] = '" + (flightSummary.getScheduleId() == null ? "N" : "Y") + "';");

				sb.append("f[" + i + "][23] = '" + strSeatFactor.toString() + "';");
				i++;
			}

		}

		return sb.toString();
	}

	/**
	 * Creates the Client Validations for the Search page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Created Client Validation Array
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {

		String clientErrors = null;

		Properties moduleErrs = new Properties();
		moduleErrs.setProperty("um.searchinventory.form.fromdate.required", "departureDateFromRqrd");
		moduleErrs.setProperty("um.searchinventory.form.todate.required", "departureDateToRqrd");
		moduleErrs.setProperty("um.searchinventory.form.fromdatelessthancurrentdate.voilated", "departureDateFromBnsRleVltd");
		moduleErrs.setProperty("um.searchinventory.form.depdategreaterthanarrivaldate.voilated", "departureDateToBnsRleVltd");
		moduleErrs.setProperty("um.searchinventory.form.departureandarrivalsame.voilated", "departureAndArrivalBnsRleVltd");
		moduleErrs.setProperty("um.searchinventory.form.viapoints.invalid", "viapointsinvalid");
		moduleErrs.setProperty("um.flightschedule.search.invalidcarrier", "invalidcarrier");
		moduleErrs.setProperty("um.searchinventory.form.flight.null", "carriernull");
		clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);

		return clientErrors;

	}
}
