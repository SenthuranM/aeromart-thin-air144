package com.isa.thinair.airadmin.core.web.handler.reports;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author Ruchith Arambepola
 * 
 */
public class BookingClassReportRH extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(BookingClassReportRH.class);

	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	/**
	 * The constructor.
	 */
	public BookingClassReportRH() {
		super();
	}

	/**
	 * @param request
	 * @return
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {
		String strHdnMode = request.getParameter("hdnMode");
		String forward = WebConstants.ACTION_FORWARD_SUCCESS;

		try {
			setDisplayData(request);
			log.debug("BookingClassReportRH success");

		} catch (Exception e) {
			forward = "error";
			log.error("BookingClassReportRH execute()" + e.getMessage());
		}
		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				log.error("BookingClassReportRH setReportView Success");
				return null;
			} else {
				log.error("BookingClassReportRH setReportView not selected");
			}

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("BookingClassReportRH setReportView Failed " + e.getMessageString());
		}

		return forward;
	}

	/**
	 * 
	 * @param request
	 * @throws Exception
	 */
	public static void setDisplayData(HttpServletRequest request) throws Exception {

		setLiveStatus(request);
		setClassOfServicesList(request);
		setLogicalCabinClassList(request);
		setAllocationTypeList(request);
		setBcHtml(request);
		setBCCategoryMultiSelectList(request);
		setClientErrors(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);
	}

	private static void setLogicalCabinClassList(HttpServletRequest request) throws ModuleException {
		String strHtmlLCC = SelectListGenerator.createAllLogicalCabinClass();
		request.setAttribute(WebConstants.REQ_HTML_LOGICALCABINCLASS_LIST, strHtmlLCC);
	}

	/**
	 * sets the Class of service to the request
	 * 
	 * @param request
	 * @return
	 */

	private static void setClassOfServicesList(HttpServletRequest request) throws ModuleException {
		String strCOSList = SelectListGenerator.createCabinClassList();
		request.setAttribute(WebConstants.REQ_HTML_CABINCLASS_LIST, strCOSList);
	}

	/**
	 * Sets the Allocation Types to the Request values are hard coded not keeping any where
	 * 
	 * @param request
	 */
	private static void setAllocationTypeList(HttpServletRequest request) {
		String strHtml = "";
		strHtml += "<option value='" + BookingClass.AllocationType.COMBINED + "'>Combine</option>";
		strHtml += "<option value='" + BookingClass.AllocationType.CONNECTION + "'>Connection</option>";
		strHtml += "<option value='" + BookingClass.AllocationType.RETURN + "'>Return</option>";
		strHtml += "<option value='" + BookingClass.AllocationType.SEGMENT + "'>Segment</option>";
		request.setAttribute(WebConstants.REQ_HTML_ALLOCATION_TYPES_LIST, strHtml);
	}

	public static void setBcHtml(HttpServletRequest request) throws ModuleException {
		String strBcList = JavascriptGenerator.createBookingCodeWithCosHtml();
		request.setAttribute(WebConstants.REQ_HTML_BC_DATA, strBcList);
	}

	public static void setBCCategoryMultiSelectList(HttpServletRequest request) throws ModuleException {

		String strBCCatList = ReportsHTMLGenerator.createBCCategoryMultiSelect();
		request.setAttribute(WebConstants.REQ_HTML_BCCATEGORY_DATA, strBCCatList);
	}

	/**
	 * Sets the Client Validations to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setClientErrors(HttpServletRequest request) {

		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);

		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	protected static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {
		ResultSet resultSet = null;
		String logicalCC = null;
		String value = request.getParameter("radReportOption");

		String[] bc = null;

		if (!"".equals(request.getParameter("hdnBCs"))) {
			bc = request.getParameter("hdnBCs").split(",");
		}
		logicalCC = request.getParameter("selLogicalCC");
		String cos = request.getParameter("selCOS");

		String[] bccategory = null;

		if (!"".equals(request.getParameter("hdnBCCat"))) {
			bccategory = request.getParameter("hdnBCCat").split(",");
		}
		String bctype = request.getParameter("selBCType");
		String allocationtype = request.getParameter("selAllocationType");
		String status = request.getParameter("selStatus");
		Collection<String> bcCol = new ArrayList<String>();
		Collection<String> bcCatCol = new ArrayList<String>();

		String reportTemplate = "BookingClassReport.jasper";
		String strLogo = AppSysParamsUtil.getReportLogo(false);// globalConfig.getBizParam(SystemParamKeys.REPORT_LOGO);
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String strPath = "../../images/" + AppSysParamsUtil.getReportLogo(true);
		String strReportId = "UC_REPM_029";
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);
		boolean logicalCabinClassEnable = AppSysParamsUtil.isLogicalCabinClassEnabled();
		String logicalCCEnable = "" + logicalCabinClassEnable + "";

		try {
			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs(WebConstants.REPORT_REF,
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));

			ReportsSearchCriteria search = new ReportsSearchCriteria();

			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}
			if (logicalCC != null)
				search.setLogicalCC(logicalCC);

			search.setCos(cos);
			search.setBcType(bctype);
			search.setAllocationType(allocationtype);
			search.setStatus(status);

			if (bc != null) {
				for (int i = 0; i < bc.length; i++) {
					bcCol.add(bc[i]);
				}
				search.setBookingCodes(bcCol);
			} else {
				search.setBookingCodes(null);
			}

			if (bccategory != null) {
				for (int i = 0; i < bccategory.length; i++) {
					bcCatCol.add(bccategory[i]);
				}
				search.setBcCategory(bcCatCol);
			} else {
				search.setBcCategory(null);
			}
			resultSet = ModuleServiceLocator.getDataExtractionBD().getBCReport(search);

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("REPORT_ID", strReportId);
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);

			// To provide Report Format Options
			String reportNumFormat = request.getParameter("radRptNumFormat");
			ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

			parameters.put("LOGICAL_CABIN_CLASS_ENABLE", logicalCCEnable);
			if (isNotnull(cos))
				parameters.put("COS", cos);
			if (isNotnull(cos))
				parameters.put("BC_TYPE", bctype);
			if (isNotnull(cos))
				parameters.put("ALLOCATION_TYPE", allocationtype);
			if (isNotnull(cos))
				parameters.put("STATUS", status);

			if (value.trim().equals(WebConstants.REPORT_HTML)) {
				parameters.put("IMG_PATH", strPath);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport(WebConstants.REPORT_REF, parameters, resultSet,
						null, null, response);
			} else if (value.trim().equals(WebConstants.REPORT_PDF)) {
				response.reset();
				response.addHeader("Content-Disposition", "filename=BookingClassDetails.pdf");
				strPath = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG_PATH", strPath);
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport(WebConstants.REPORT_REF, parameters, resultSet,
						response);
			} else if (value.trim().equals(WebConstants.REPORT_EXCEL)) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=BookingClassDetails.xls");
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport(WebConstants.REPORT_REF, parameters, resultSet,
						response);
			} else if (value.trim().equals("CSV")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=BookingClassDetails.csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);
			}
		} catch (Exception e) {
			log.error(e);
		}
	}

	private static boolean isNotnull(String str) {
		boolean blnCond = false;
		if (str == null || str.trim().equals("Select")) {
			blnCond = false;
		} else {
			blnCond = true;
		}
		return blnCond;
	}
	
	/**
	 * Sets the report medium to the request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleExceptions
	 */
	private static void setLiveStatus(HttpServletRequest request) throws ModuleException {
		String strLive = request.getParameter(WebConstants.REP_HDN_LIVE);
		if (strLive != null)
			request.setAttribute(WebConstants.REP_SET_LIVE, strLive);

	}
}
