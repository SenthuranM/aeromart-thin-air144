package com.isa.thinair.airadmin.core.web.action.flightsched;

import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airadmin.core.web.v2.util.CommonUtil;
import com.isa.thinair.airmaster.api.model.Gds;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class LoadInitDataForPublishGdsAction extends BaseRequestAwareAction{
	
	private static Log log = LogFactory.getLog(LoadInitDataForPublishGdsAction.class);

	private Map<String, String> airports;
	private Set<String> carriers;
	private Set<String> gdsPublishedOndList;
//	private Set<String> gdsPublishedCarrierCodes;
	private String enableRouteWiseAutoPublish;
	
	public Map<String, String> getAirports() {
		return airports;
	}

	public void setAirports(Map<String, String> airports) {
		this.airports = airports;
	}

	public Set<String> getCarriers() {
		return carriers;
	}

	public void setCarriers(Set<String> carriers) {
		this.carriers = carriers;
	}
	
	public Set<String> getGdsPublishedOndList() {
		return gdsPublishedOndList;
	}

	public void setGdsPublishedOndList(Set<String> gdsPublishedOndList) {
		this.gdsPublishedOndList = gdsPublishedOndList;
	}

/*	public Set<String> getGdsPublishedCarrierCodes() {
		return gdsPublishedCarrierCodes;
	}

	public void setGdsPublishedCarrierCodes(Set<String> gdsPublishedCarrierCodes) {
		this.gdsPublishedCarrierCodes = gdsPublishedCarrierCodes;
	}
	*/

	public String getEnableRouteWiseAutoPublish() {
		return enableRouteWiseAutoPublish;
	}

	public void setEnableRouteWiseAutoPublish(String enableRouteWiseAutoPublish) {
		this.enableRouteWiseAutoPublish = enableRouteWiseAutoPublish;
	}
	
	public String execute() {
		try {

			Map<String, String> activeAirports = SelectListGenerator.createActiveAirportCodeMap();
			activeAirports.put("All", "***");
			setAirports(CommonUtil.sortByValue(activeAirports)); 
			setCarriers(getUserCarrierCodes(request));
			String gdsId = request.getParameter("gdsId");

			if (gdsId != null) {

				Gds gds = getGds(gdsId);
				setEnableRouteWiseAutoPublish(isRouteWiseAutoPublishEnabledForGds(gds));
				//setGdsPublishedCarrierCodes(getGdsWiseAutoPublishCarriers(gds));
				setGdsPublishedOndList(getGdsWiseAutoPublishOnds(gds));
			}

		} catch (Exception e) {
			log.error("execute ", e);
		}
		return S2Constants.Result.SUCCESS;
	}

	private Set<String> getUserCarrierCodes(HttpServletRequest request) throws ModuleException {
		User user = (User) request.getSession().getAttribute(WebConstants.SES_CURR_USER);
		if (user.getCarriers() == null || user.getCarriers().size() < 1) {
			throw new ModuleException("module.user.carrier");
		}
		return user.getCarriers();
	}

	private static Gds getGds(String gdsId) throws NumberFormatException, ModuleException {
		Gds gds = ModuleServiceLocator.getGdsServiceBD().getGds(Integer.parseInt(gdsId));
		return gds;
	}

	private static String isRouteWiseAutoPublishEnabledForGds(Gds gds) {

		boolean enableRouteWiseAutoPublish = gds.isRouteWiseAutoPublishEnabled();

		return Boolean.toString(enableRouteWiseAutoPublish);
	}

	private static Set<String> getGdsWiseAutoPublishOnds(Gds gds) throws NumberFormatException, ModuleException {

		return gds.getGdsAutoPublishRoutes();

	}

/*	private static Set<String> getGdsWiseAutoPublishCarriers(Gds gds) throws NumberFormatException, ModuleException {

		return gds.getGdsAutoPublishCarriers();

	}*/

	
}
