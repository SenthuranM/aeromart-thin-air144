package com.isa.thinair.airadmin.core.web.handler.flightsched;

import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.flightsched.ScheduleHTMLGenerator;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airadmin.core.web.util.FlightUtils;
import com.isa.thinair.airadmin.core.web.util.ScheduleUtils;
import com.isa.thinair.airmaster.api.model.Airport;
import com.isa.thinair.airmaster.api.model.AirportDST;
import com.isa.thinair.airschedules.api.dto.FlightAlertDTO;
import com.isa.thinair.airschedules.api.dto.OverlappedFlightDTO;
import com.isa.thinair.airschedules.api.dto.OverlappedScheduleDTO;
import com.isa.thinair.airschedules.api.model.CodeShareMCFlightSchedule;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.FlightScheduleLeg;
import com.isa.thinair.airschedules.api.model.FlightScheduleSegment;
import com.isa.thinair.airschedules.api.utils.ResponceCodes;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.commons.api.constants.DayOfWeek;
import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.FrequencyUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author Thushara
 * 
 * 
 */

public final class ScheduleRequestHandler extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(ScheduleRequestHandler.class);

	private static final String PARAM_VERSION = "hdnVersion";

	private static final String PARAM_IN_STARTDATE = "txtStartDate";
	private static final String PARAM_IN_STOPDATE = "txtStopDate";
	private static final String PARAM_IN_OPERATIONTYPE = "selOperationType";
	private static final String PARAM_IN_FLIGHTNUMBER = "txtFlightNo";
	private static final String PARAM_IN_FLIGHTNUMBERSTART = "txtFlightNoStart";
	private static final String PARAM_IN_AIRCRAFTMODEL = "selAircraftModel";
	private static final String PARAM_IN_MONDAY = "chkMonday";
	private static final String PARAM_IN_TUESDAY = "chkTuesday";
	private static final String PARAM_IN_WEDNESDAY = "chkWednesday";
	private static final String PARAM_IN_THURSDAY = "chkThursday";
	private static final String PARAM_IN_FRIDAY = "chkFriday";
	private static final String PARAM_IN_SATURDAY = "chkSaturday";
	private static final String PARAM_IN_SUNDAY = "chkSunday";
	private static final String PARAM_IN_DEPART_STN = "hdnDepartStn";
	private static final String PARAM_IN_ARRIVAL_STN = "hdnArrivalStn";
	private static final String PARAM_IN_GDS_IDS = "hdnGDSPublishing";

	private static final String PARAM_COPY_STARTDATE = "hdnCopyStartD";
	private static final String PARAM_COPY_STOPDATE = "hdnCopyStopD";
	private static final String PARAM_SPLIT_STARTDATE = "hdnSplitStartD";
	private static final String PARAM_SPLIT_STOPDATE = "hdnSplitStopD";
	private static final String PARAM_SPLIT_MONDAY = "chkMonday";
	private static final String PARAM_SPLIT_TUESDAY = "chkTuesday";
	private static final String PARAM_SPLIT_WEDNESDAY = "chkWednesday";
	private static final String PARAM_SPLIT_THURSDAY = "chkThursday";
	private static final String PARAM_SPLIT_FRIDAY = "chkFriday";
	private static final String PARAM_SPLIT_SATURDAY = "chkSaturday";
	private static final String PARAM_SPLIT_SUNDAY = "chkSunday";

	private static final String PARAM_SCHEDULEID = "hdnScheduleId";
	private static final String PARAM_TIMEMODE = "hdnTimeMode";
	private static final String PARAM_ALLSCHEDULEIDS = "hdnSelectedSchedules";
	private static final String PARAM_MODE = "hdnMode";
	private static final String PARAM_STATUS = "hdnStatus";
	private static final String PARAM_FLIGHT_TYPE = "selFlightType";
	private static final String PARAM_BUILD_STATUS = "hdnBulidStatus";

	// leg properties
	private static final String PARAM_LEG_ARRIVAL_1 = "selToStn1";
	private static final String PARAM_LEG_DEPATURE_1 = "selFromStn1";
	private static final String PARAM_LEG_ARRIVAL_TIME_1 = "txtA_Time1";
	private static final String PARAM_LEG_DEPATURE_TIME_1 = "txtD_Time1";
	private static final String PARAM_LEG_ARRIVAL_VARIANCE_1 = "selA_Day1";
	private static final String PARAM_LEG_DEPATURE_VARIANCE_1 = "selD_Day1";

	private static final String PARAM_LEG_ARRIVAL_2 = "selToStn2";
	private static final String PARAM_LEG_DEPATURE_2 = "selFromStn2";
	private static final String PARAM_LEG_ARRIVAL_TIME_2 = "txtA_Time2";
	private static final String PARAM_LEG_DEPATURE_TIME_2 = "txtD_Time2";
	private static final String PARAM_LEG_ARRIVAL_VARIANCE_2 = "selA_Day2";
	private static final String PARAM_LEG_DEPATURE_VARIANCE_2 = "selD_Day2";

	private static final String PARAM_LEG_ARRIVAL_3 = "selToStn3";
	private static final String PARAM_LEG_DEPATURE_3 = "selFromStn3";
	private static final String PARAM_LEG_ARRIVAL_TIME_3 = "txtA_Time3";
	private static final String PARAM_LEG_DEPATURE_TIME_3 = "txtD_Time3";
	private static final String PARAM_LEG_ARRIVAL_VARIANCE_3 = "selA_Day3";
	private static final String PARAM_LEG_DEPATURE_VARIANCE_3 = "selD_Day3";

	private static final String PARAM_LEG_ARRIVAL_4 = "selToStn4";
	private static final String PARAM_LEG_DEPATURE_4 = "selFromStn4";
	private static final String PARAM_LEG_ARRIVAL_TIME_4 = "txtA_Time4";
	private static final String PARAM_LEG_DEPATURE_TIME_4 = "txtD_Time4";
	private static final String PARAM_LEG_ARRIVAL_VARIANCE_4 = "selA_Day4";
	private static final String PARAM_LEG_DEPATURE_VARIANCE_4 = "selD_Day4";

	private static final String PARAM_LEG_ARRIVAL_5 = "selToStn5";
	private static final String PARAM_LEG_DEPATURE_5 = "selFromStn5";
	private static final String PARAM_LEG_ARRIVAL_TIME_5 = "txtA_Time5";
	private static final String PARAM_LEG_DEPATURE_TIME_5 = "txtD_Time5";
	private static final String PARAM_LEG_ARRIVAL_VARIANCE_5 = "selA_Day5";
	private static final String PARAM_LEG_DEPATURE_VARIANCE_5 = "selD_Day5";

	private static final String PARAM_LEG_VALIDITY = "hdnOverLapSeg";
	private static final String PARAM_VALID_SEGMENT = "hdnSegArray";
	private static final String PARAM_VALID_TERMINAL = "hdnTerminalArray";
	private static final String PARAM_VALID_SEGMET_OLAPID = "hdnOverLapSegId";
	private static final String PARAM_VALID_OSCHEDID = "hdnOverLapSegSchedId";
	private static final String PARAM_OVERLAPSCEDID = "hdnOverlapSchID";

	private static final String PARAM_REPROTECTALERT = "hdnCancelAlert";
	private static final String PARAM_REPROTECTEMAIL = "hdnCancelEmail";
	private static final String PARAM_RESCHEDULEALERT = "hdnReSchedAlert";
	private static final String PARAM_RESCHEDULEEMAIL = "hdnReSchedEmail";
	private static final String PARAM_RESCHEDULEALL = "hdnRescheduleAll";

	private static final String PARAM_EMAILTO = "hdnEmailTo";
	private static final String PARAM_EMAILSUBJECT = "hdnEmailSubject";
	private static final String PARAM_EMAILCONTENT = "hdnEmailContent";
	private static final String PARAM_REPROTECT = "hdnReprotect";

	private static final String PARAM_SMSALERT = "hdnSMSAlert";
	private static final String PARAM_EMAILALERT = "hdnEmailAlert";
	private static final String PARAM_SMSCNF = "hdnSmsCnf";
	private static final String PARAM_CMSONH = "hdnSmsOnH";
	private static final String PARAM_SMSALERT2 = "hdnSMSAlert2";
	private static final String PARAM_EMAILALERT2 = "hdnEmailAlert2";

	private static final String PARAM_FLIGHT_REMARKS = "txtRemarks";
	private static final String PARAM_MEAL_TEMPLATE = "selMealTemplate";
	private static final String PARAM_SEAT_CHARGE_TEMPLATE = "selSeatChargeTemplate";
	private static final String PARAM_BUILD_DEFAULT_INV_TEMPLATE = "hdnBuildDefaultInventory";
	
	//CodeShare properties
	private static final String PARAM_CODESHARE = "hndCodeShare";
	
	private static final String PARAM_CSOCCARRIER = "hndCsOCCarrierCode";
	private static final String PARAM_CSOCFLIGHT = "hndCsOCFlightNo";	
	private static final String PARAM_VIEW_AUDIT_TYPE = "hdnAuditType";
	
	private static final String RECEIVED_MESSAGES = "IN_MSG";
	private static final String PUBLISHED_MESSAGES = "PUB_MSG";
	

	private static AiradminConfig airadminConfig = new AiradminConfig();
	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	private static String strErrorMEssage = "";
	private static boolean isSaveError = false;

	/**
	 * Main execute Method for FLIGHTSCHEDULE Action
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @param response TODO
	 * @return the Forward Action
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;
		strErrorMEssage = "";
		isSaveError = false;
		
		String hdnAuditType = request.getParameter(PARAM_VIEW_AUDIT_TYPE);
		try {
			if (RECEIVED_MESSAGES.equals(hdnAuditType)) {
				viewScheduleMsgReport(request, response);
				return null;
			} else if (PUBLISHED_MESSAGES.equals(hdnAuditType)) {
				viewFlightPublishedMessagesAudit(request, response);
				return null;
			} else {
				forward = saveData(request);
			}
		} catch (ModuleException e) {
			isSaveError = true;
			log.error("Exception in ScheduleRequestHandler.execute " + "[origin module=" + e.getModuleDesc() + "]", e);
			// /for any error return the error page -- /public/showError

			strErrorMEssage = e.getMessageString();
			request.setAttribute(WebConstants.REQ_MESSAGE, e);
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
		} catch (Exception e) {
			isSaveError = true;
			log.error("Exception in ScheduleRequestHandler.save", e);
			if (e instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, e.getMessage(), "", "");
			} else {
				strErrorMEssage = e.getMessage();
				saveMessage(request, e.getMessage(), WebConstants.MSG_ERROR);
			}
			try {
				setModel(request);
			} catch (Exception ex) {

			}
		}

		try {
			if (!forward.equalsIgnoreCase(WebConstants.ACTION_FORWARD_REPROTECT)) {
				setDisplayData(request);
			}
		} catch (ModuleException e) {
			log.error(
					"Exception in ScheduleRequestHandler.setDisplay " + "- setDisplayData(request)" + " [origin module="
							+ e.getModuleDesc() + "]", e);
			// /for any error return the error page -- /public/showError
			if (!isSaveError) {
				strErrorMEssage = e.getMessageString();
			}

			request.setAttribute(WebConstants.REQ_MESSAGE, e);
			saveMessage(request, strErrorMEssage, WebConstants.MSG_ERROR);
		} catch (Exception e) {
			log.error("Exception in ScheduleRequestHandler.execute " + "- setDisplayData(request)", e);
			if (e instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, e.getMessage(), "", "");
			} else {
				if (!isSaveError) {
					strErrorMEssage += " : " + e.getMessage();
				}
				saveMessage(request, strErrorMEssage, WebConstants.MSG_ERROR);
			}

		}
		return forward;
	}

	/**
	 * Saves Flight Schedule Data
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 * @throws ModuleException
	 *             the ModuleException
	 * @throws ParseException
	 *             the ParseException
	 */
	private static String saveData(HttpServletRequest request) throws ModuleException, ParseException {
		Properties schedProp = getProperties(request);
		String strCopyStartDate = schedProp.getProperty(PARAM_COPY_STARTDATE);
		String strCopyStopDate = schedProp.getProperty(PARAM_COPY_STOPDATE);
		String strSplitStartDate = schedProp.getProperty(PARAM_SPLIT_STARTDATE);
		String strSplitStopDate = schedProp.getProperty(PARAM_SPLIT_STOPDATE);
		String strSPLITMonday = schedProp.getProperty(PARAM_SPLIT_MONDAY);
		String strSPLITTuesday = schedProp.getProperty(PARAM_SPLIT_TUESDAY);
		String strSPLITWednesday = schedProp.getProperty(PARAM_SPLIT_WEDNESDAY);
		String strSPLITThursday = schedProp.getProperty(PARAM_SPLIT_THURSDAY);
		String strSPLITFriday = schedProp.getProperty(PARAM_SPLIT_FRIDAY);
		String strSPLITSaturday = schedProp.getProperty(PARAM_SPLIT_SATURDAY);
		String strSPLITSunday = schedProp.getProperty(PARAM_SPLIT_SUNDAY);
		String strAllScheduleIds = schedProp.getProperty(PARAM_ALLSCHEDULEIDS);
		String strScheduleId = schedProp.getProperty(PARAM_SCHEDULEID);
		String strHdnMode = schedProp.getProperty(PARAM_MODE);
		boolean blnAlert = getBooleanValue(schedProp.getProperty(PARAM_REPROTECTALERT));
		boolean blnEmail = getBooleanValue(schedProp.getProperty(PARAM_REPROTECTEMAIL));
		boolean blnReschedAlert = getBooleanValue(schedProp.getProperty(PARAM_RESCHEDULEALERT));
		boolean blnReschedEmail = getBooleanValue(schedProp.getProperty(PARAM_RESCHEDULEEMAIL));
		boolean blnReschedAll = getBooleanValue(schedProp.getProperty(PARAM_RESCHEDULEALL));
		String strTimeinMode = schedProp.getProperty(PARAM_TIMEMODE);
		boolean blnTimeLocal = strTimeinMode.equals(WebConstants.LOCALTIME);

		String strEmailTo = schedProp.getProperty(PARAM_EMAILTO);
		String strEmailSubject = schedProp.getProperty(PARAM_EMAILSUBJECT);
		String strEmailContent = schedProp.getProperty(PARAM_EMAILCONTENT);

		boolean blnEmailAlert = getBooleanValue(request.getParameter(PARAM_EMAILALERT));
		boolean blnSmsAlert = getBooleanValue(request.getParameter(PARAM_SMSALERT));
		boolean blnSmsCnf = getBooleanValue(request.getParameter(PARAM_SMSCNF));
		boolean blnSmsOnH = getBooleanValue(request.getParameter(PARAM_CMSONH));
		boolean blnEmailAlert2 = getBooleanValue(request.getParameter(PARAM_EMAILALERT2));
		boolean blnSmsAlert2 = getBooleanValue(request.getParameter(PARAM_SMSALERT2));
		// String strValidatedTerminals = schedProp.getProperty(PARAM_VALID_TERMINAL);

		boolean buildDefaultInventory = getBooleanValue(schedProp.getProperty(PARAM_BUILD_DEFAULT_INV_TEMPLATE));

		// declaring the Service response object
		ServiceResponce serviseResponce = null;
		String strResponseMessage = "success";

		// Declaring flight schedule
		FlightSchedule flightSchedule = null;

		// if action is adding data
		if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_ADD)) {
			checkPrivilege(request, WebConstants.PLAN_FLIGHT_SCHEDULE_MANAGE_ADD);
			flightSchedule = createFlightSchedule(schedProp);

			// calling the backend function
			CommonUtil.checkCarrierCodeAccessibility(getUserCarrierCodes(request),
					CommonUtil.getCarrierCode(flightSchedule.getFlightNumber()));
			serviseResponce = ModuleServiceLocator.getScheduleServiceBD().createSchedule(flightSchedule, false, false);
		} else if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_COPY)) {
			// if action is copy
			checkPrivilege(request, WebConstants.PLAN_FLIGHT_SCHEDULE_MANAGE_COPY);
			Date dateCopyStartDate = null;
			Date dateCopyStopDate = null;

			if (strCopyStartDate.indexOf(' ') != -1)
				strCopyStartDate = strCopyStartDate.substring(0, strCopyStartDate.indexOf(' '));

			if (strCopyStopDate.indexOf(' ') != -1)
				strCopyStopDate = strCopyStopDate.substring(0, strCopyStopDate.indexOf(' '));

			dateCopyStartDate = ScheduleUtils.parseToDate(strCopyStartDate);
			dateCopyStopDate = ScheduleUtils.parseToDate(strCopyStopDate);

			// calling the backend function
			serviseResponce = ModuleServiceLocator.getScheduleServiceBD().copySchedule(Integer.parseInt(strScheduleId),
					dateCopyStartDate, dateCopyStopDate, blnTimeLocal);
		} else if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_BUILD)) {
			// if the action is build
			checkPrivilege(request, WebConstants.PLAN_FLIGHT_SCHEDULE_MANAGE_BUILD);
			if (!strAllScheduleIds.trim().equals("")) {
				String s[] = strAllScheduleIds.split(",");
				ArrayList<Integer> list = new ArrayList<Integer>();
				for (int i = 0; i < s.length; i++) {
					list.add(i, new Integer(s[i]));
				}

				Collection<Integer> scheduleIdcol = list;
				// calling the backend function
				serviseResponce = ModuleServiceLocator.getScheduleServiceBMTBD().buildSchedulesBMT(scheduleIdcol,
						buildDefaultInventory);
			}
		} else if ((strHdnMode != null) && (strHdnMode.equals(WebConstants.ACTION_CANCEL))
				&& (Boolean.valueOf(request.getParameter(PARAM_REPROTECT)).booleanValue())) {
			// if action is cancel and refreshing the cancellation screen when reprotect
			// NOTE : This method should always be called before cancel
			checkPrivilege(request, WebConstants.PLAN_FLIGHT_SCHEDULE_MANAGE_CANCEL);
			flightSchedule = ModuleServiceLocator.getScheduleServiceBD().getFlightSchedule(Integer.parseInt(strScheduleId));
			serviseResponce = ModuleServiceLocator.getScheduleServiceBD().getFlightReservationsForCancel(
					Integer.parseInt(strScheduleId), false);
		} else if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_CANCEL)) {
			// if the action is cancel
			checkPrivilege(request, WebConstants.PLAN_FLIGHT_SCHEDULE_MANAGE_CANCEL);
			// calling the backend function
			flightSchedule = ModuleServiceLocator.getScheduleServiceBD().getFlightSchedule(Integer.parseInt(strScheduleId));
			serviseResponce = ModuleServiceLocator.getScheduleServiceBD().cancelSchedule(Integer.parseInt(strScheduleId), false,
					new FlightAlertDTO(), false, false);
		} else if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_SPLIT)) {
			// if the action is split
			checkPrivilege(request, WebConstants.PLAN_FLIGHT_SCHEDULE_MANAGE_SPLIT);
			Date dateSplitStartDate = null;
			Date dateSplitStopDate = null;

			if (strSplitStartDate.indexOf(' ') != -1)
				strSplitStartDate = strSplitStartDate.substring(0, strSplitStartDate.indexOf(' '));

			if (strSplitStopDate.indexOf(' ') != -1)
				strSplitStopDate = strSplitStopDate.substring(0, strSplitStopDate.indexOf(' '));

			dateSplitStartDate = ScheduleUtils.parseToDate(strSplitStartDate);
			if (!"".equals(strSplitStopDate)) {
				dateSplitStopDate = ScheduleUtils.parseToDate(strSplitStopDate);
			}

			Collection<DayOfWeek> dayList = new ArrayList<DayOfWeek>();

			if (strSPLITSunday.equals(WebConstants.APP_BOOLEAN_TRUE))
				dayList.add(DayOfWeek.SUNDAY);
			if (strSPLITMonday.equals(WebConstants.APP_BOOLEAN_TRUE))
				dayList.add(DayOfWeek.MONDAY);
			if (strSPLITTuesday.equals(WebConstants.APP_BOOLEAN_TRUE))
				dayList.add(DayOfWeek.TUESDAY);
			if (strSPLITWednesday.equals(WebConstants.APP_BOOLEAN_TRUE))
				dayList.add(DayOfWeek.WEDNESDAY);
			if (strSPLITThursday.equals(WebConstants.APP_BOOLEAN_TRUE))
				dayList.add(DayOfWeek.THURSDAY);
			if (strSPLITFriday.equals(WebConstants.APP_BOOLEAN_TRUE))
				dayList.add(DayOfWeek.FRIDAY);
			if (strSPLITSaturday.equals(WebConstants.APP_BOOLEAN_TRUE))
				dayList.add(DayOfWeek.SATURDAY);

			Frequency fqn = CalendarUtil.getFrequencyFromDays(dayList);
			flightSchedule = ModuleServiceLocator.getScheduleServiceBD().getFlightSchedule(Integer.parseInt(strScheduleId));

			serviseResponce = ModuleServiceLocator.getScheduleServiceBD().splitSchedule(Integer.parseInt(strScheduleId),
					dateSplitStartDate, dateSplitStopDate, fqn, blnTimeLocal, true, false);
		} else if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_UPDATE)) {
			// if schedule change
			checkPrivilege(request, WebConstants.PLAN_FLIGHT_SCHEDULE_MANAGE_EDIT);
			flightSchedule = createFlightSchedule(schedProp);
			flightSchedule.setScheduleId(new Integer(strScheduleId));
			FlightAlertDTO fltAlertDto = new FlightAlertDTO();
			serviseResponce = ModuleServiceLocator.getScheduleServiceBD().updateSchedule(flightSchedule, false, fltAlertDto,
					true, hasPrivilege(request, WebConstants.PLAN_FLIGHT_DOWNGRADE_TO_ANY_MODEL), false, false);
		} else if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_EDITLEG)) {
			// if leg changed
			checkPrivilege(request, WebConstants.PLAN_FLIGHT_SCHEDULE_MANAGE_EDIT);
			flightSchedule = createFlightSchedule(schedProp);
			FlightAlertDTO fltAlertDto = new FlightAlertDTO();
			flightSchedule.setScheduleId(new Integer(strScheduleId));
			int firstLegDayDiff = Integer.parseInt(schedProp.getProperty(PARAM_LEG_DEPATURE_VARIANCE_1));
			CommonUtil.checkCarrierCodeAccessibility(getUserCarrierCodes(request),
					CommonUtil.getCarrierCode(flightSchedule.getFlightNumber()));
			serviseResponce = ModuleServiceLocator.getScheduleServiceBD().updateScheduleLegs(flightSchedule, false, fltAlertDto,
					false, firstLegDayDiff);
		} else if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_CANCEL_FORCE)) {
			checkPrivilege(request, WebConstants.PLAN_FLIGHT_SCHEDULE_MANAGE_CANCEL);
			FlightAlertDTO fltAltDTO = new FlightAlertDTO();
			fltAltDTO.setAlertForCancelledFlight(blnAlert);
			fltAltDTO.setEmailForCancelledFlight(blnEmail);

			fltAltDTO.setSendEmailsForFlightAlterations(blnEmailAlert);
			fltAltDTO.setSendSMSForFlightAlterations(blnSmsAlert);
			fltAltDTO.setSendSMSForConfirmedBookings(blnSmsCnf);
			fltAltDTO.setSendSMSForOnHoldBookings(blnSmsOnH);
			fltAltDTO.setSendEmailsForRescheduledFlight(blnEmailAlert2);
			fltAltDTO.setSendSMSForRescheduledFlight(blnSmsAlert2);

			if (blnEmail) {
				fltAltDTO.setEmailTo(strEmailTo);
				fltAltDTO.setEmailSubject(strEmailSubject);
				fltAltDTO.setEmailContent(strEmailContent);
			}
			serviseResponce = ModuleServiceLocator.getScheduleServiceBD().cancelSchedule(Integer.parseInt(strScheduleId), true,
					fltAltDTO, true, false);
		} else if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_UPDATE_CANCEL)) {
			checkPrivilege(request, WebConstants.PLAN_FLIGHT_SCHEDULE_MANAGE_CANCEL);
			flightSchedule = createFlightSchedule(schedProp);
			flightSchedule.setScheduleId(new Integer(strScheduleId));
			FlightAlertDTO fltAltDTO = new FlightAlertDTO();
			fltAltDTO.setAlertForCancelledFlight(blnAlert);
			fltAltDTO.setEmailForCancelledFlight(blnEmail);
			fltAltDTO.setAlertForRescheduledFlight(blnReschedAlert);
			fltAltDTO.setEmailForRescheduledFlight(blnReschedEmail);

			fltAltDTO.setSendEmailsForFlightAlterations(blnEmailAlert);
			fltAltDTO.setSendSMSForFlightAlterations(blnSmsAlert);
			fltAltDTO.setSendSMSForConfirmedBookings(blnSmsCnf);
			fltAltDTO.setSendSMSForOnHoldBookings(blnSmsOnH);
			fltAltDTO.setSendEmailsForRescheduledFlight(blnEmailAlert2);
			fltAltDTO.setSendSMSForRescheduledFlight(blnSmsAlert2);

			if (blnEmail || blnReschedEmail) {
				fltAltDTO.setEmailTo(strEmailTo);
				fltAltDTO.setEmailSubject(strEmailSubject);
				fltAltDTO.setEmailContent(strEmailContent);
			}
			serviseResponce = ModuleServiceLocator.getScheduleServiceBD().updateSchedule(flightSchedule, true, fltAltDTO,
					blnReschedAll, hasPrivilege(request, WebConstants.PLAN_FLIGHT_DOWNGRADE_TO_ANY_MODEL), false, false);

		} else if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_UPDATE_CANCELLEG)) {
			checkPrivilege(request, WebConstants.PLAN_FLIGHT_SCHEDULE_MANAGE_CANCEL);
			flightSchedule = createFlightSchedule(schedProp);
			int firstLegDayDiff = Integer.parseInt(schedProp.getProperty(PARAM_LEG_DEPATURE_VARIANCE_1));
			flightSchedule.setScheduleId(new Integer(strScheduleId));
			FlightAlertDTO fltAltDTO = new FlightAlertDTO();
			fltAltDTO.setAlertForCancelledFlight(blnAlert);
			fltAltDTO.setEmailForCancelledFlight(blnEmail);
			fltAltDTO.setAlertForRescheduledFlight(blnReschedAlert);
			fltAltDTO.setEmailForRescheduledFlight(blnReschedEmail);

			fltAltDTO.setSendEmailsForFlightAlterations(blnEmailAlert);
			fltAltDTO.setSendSMSForFlightAlterations(blnSmsAlert);
			fltAltDTO.setSendSMSForConfirmedBookings(blnSmsCnf);
			fltAltDTO.setSendSMSForOnHoldBookings(blnSmsOnH);
			fltAltDTO.setSendEmailsForRescheduledFlight(blnEmailAlert2);
			fltAltDTO.setSendSMSForRescheduledFlight(blnSmsAlert2);

			if (blnEmail || blnReschedEmail) {
				fltAltDTO.setEmailTo(strEmailTo);
				fltAltDTO.setEmailSubject(strEmailSubject);
				fltAltDTO.setEmailContent(strEmailContent);
			}
			CommonUtil.checkCarrierCodeAccessibility(getUserCarrierCodes(request),
					CommonUtil.getCarrierCode(flightSchedule.getFlightNumber()));
			serviseResponce = ModuleServiceLocator.getScheduleServiceBD().updateScheduleLegs(flightSchedule, true, fltAltDTO,
					blnReschedAll, firstLegDayDiff);

		}

		if (serviseResponce != null) {

			if (flightSchedule != null) {
				// get the EFFECTIVE DATE for the time conversion
				Date effectiveDate = null;
				// calculate the local date
				Date startDateZulu = null;
				Date stopDateZulu = null;
				Date startDateLocal = null;
				Date stopDateLocal = null;
				FlightScheduleLeg firstLeg = ScheduleUtils.getFirstScheduleLeg(flightSchedule.getFlightScheduleLegs());
				String origin = firstLeg.getOrigin();
				if (flightSchedule.getStartDate() == null && flightSchedule.getStartDateLocal() != null) {

					startDateLocal = CalendarUtil.getTimeAddedDate(flightSchedule.getStartDateLocal(),
							firstLeg.getEstDepartureTimeLocal());
					effectiveDate = ScheduleUtils.getZuluDateTime(origin, startDateLocal);
					startDateZulu = effectiveDate;
					flightSchedule.setStartDate(startDateZulu);

				}
				if (flightSchedule.getStopDate() == null && flightSchedule.getStopDateLocal() != null) {

					stopDateLocal = CalendarUtil.getTimeAddedDate(flightSchedule.getStopDateLocal(),
							firstLeg.getEstDepartureTimeLocal());
					stopDateZulu = ScheduleUtils.getZuluDateTimeAsEffective(origin, effectiveDate, stopDateLocal);
					flightSchedule.setStopDate(stopDateZulu);
				}
				if (flightSchedule.getFrequency() == null && flightSchedule.getFrequencyLocal() != null) {
					// setting found out fields
					if (startDateLocal != null && startDateZulu != null) {
						int dif = CalendarUtil.daysUntil(startDateLocal, startDateZulu);
						Frequency frequencyZulu = FrequencyUtil.getCopyOfFrequency(flightSchedule.getFrequencyLocal());
						frequencyZulu = FrequencyUtil.shiftFrequencyBy(frequencyZulu, dif);
						flightSchedule.setFrequency(frequencyZulu);
					}
				}
			}
			strResponseMessage = setResponsePage(request, serviseResponce, flightSchedule);
		}

		return strResponseMessage;

	}

	/**
	 * method to get converted local time/date as zulu time/date for the given station
	 */
	public static Date getZuluDateTime(String airport, Date localDate) throws ModuleException {

		int dstOffset = 0;

		// find GMT for the airport
		int gmtOffset = getGMTOffset(airport);

		// calculate the zulu date without DST
		Date zuluDate = CalendarUtil.getZuluDate(localDate, gmtOffset, dstOffset);

		// find DST with zulu date
		AirportDST aptDST = ModuleServiceLocator.getAirportServiceBD().getEffectiveAirportDST(airport, zuluDate);

		dstOffset = (aptDST != null) ? aptDST.getDstAdjustTime() : 0;

		// calculate the DSt adjusted zulu date
		zuluDate = CalendarUtil.getZuluDate(localDate, gmtOffset, dstOffset);

		AirportDST aptDSTZulu = ModuleServiceLocator.getAirportServiceBD().getEffectiveAirportDST(airport, zuluDate);

		if ((aptDSTZulu != null && aptDST != null && aptDSTZulu.getDstCode() != aptDST.getDstCode())
				|| (aptDSTZulu != null && aptDST == null) || (aptDSTZulu == null && aptDST != null))
			throw new ModuleException("airschedules.arg.invalid.cannot.calculate.zulutime");
		return zuluDate;

	}

	private static int getGMTOffset(String airport) throws ModuleException {

		Airport apt = ModuleServiceLocator.getAirportServiceBD().getAirport(airport);
		int gmtOffset = 0;
		if (apt != null) {

			gmtOffset = (apt.getGmtOffsetAction().equals("-")) ? (-1 * apt.getGmtOffsetHours()) : apt.getGmtOffsetHours();
		}

		return gmtOffset;
	}

	/**
	 * Sets the Response to the Front After Executing the Action
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @param respon
	 *            the ServiceResponce response object
	 * @param sched
	 *            the FlightSchedule
	 * @return String the response String
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static String setResponsePage(HttpServletRequest request, ServiceResponce respon, FlightSchedule sched) {

		String strResponseMessage = WebConstants.FORWARD_SUCCESS;
		String strHdnMode = AiradminUtils.getNotNullString(request.getParameter(PARAM_MODE));
		String responceCode = AiradminUtils.getNotNullString(respon.getResponseCode());

		if (respon.isSuccess()) {

			if (responceCode.equals(ResponceCodes.CREATE_SCHEDULE_SUCCESSFULL)) {
				if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_COPY)) {
					saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_COPY_SUCCESS), WebConstants.MSG_SUCCESS);
					request.setAttribute(WebConstants.POPUPMESSAGE,
							airadminConfig.getMessage(WebConstants.KEY_SCHEDULE_COPY_SUCCESS));
				} else {
					saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS), WebConstants.MSG_SUCCESS);
					request.setAttribute(WebConstants.POPUPMESSAGE,
							airadminConfig.getMessage(WebConstants.KEY_SCHEDULE_SAVE_SUCCESS));
				}

			} else if (responceCode.equals(ResponceCodes.AMMEND_SCHEDULE_SUCCESSFULL)) {
				Collection<String> skippedFlightDates = (Collection<String>) respon.getResponseParam(ResponceCodes.SKIPPED_FLIGHT_DATES);
				saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_UPDATE_SUCCESS), WebConstants.MSG_SUCCESS);
				
				if (!skippedFlightDates.isEmpty()&& skippedFlightDates.size() > 0) {
					request.setAttribute(WebConstants.POPUPMESSAGE,
							airadminConfig.getMessage(WebConstants.KEY_SCHEDULE_UPDATE_SUCCESS)
								+ " Skipping aircraft model change on aircraft model different flights : "+ skippedFlightDates.toString());
				}else{
					request.setAttribute(WebConstants.POPUPMESSAGE,
						airadminConfig.getMessage(WebConstants.KEY_SCHEDULE_UPDATE_SUCCESS));
				}

			} else if (responceCode.equals(ResponceCodes.CANCEL_SCHEDULE_SUCCESSFULL)) {

				saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_DELETE_SUCCESS), WebConstants.MSG_SUCCESS);
				request.setAttribute(WebConstants.POPUPMESSAGE,
						airadminConfig.getMessage(WebConstants.KEY_SCHEDULE_DELETE_SUCCESS));

			} else if (responceCode.equals(ResponceCodes.SPLIT_SCHEDULE_SUCCESSFULL)) {

				saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_SPLIT_SUCCESS), WebConstants.MSG_SUCCESS);
				request.setAttribute(WebConstants.POPUPMESSAGE,
						airadminConfig.getMessage(WebConstants.KEY_SCHEDULE_SPLIT_SUCCESS));

			} else if (responceCode.equals(ResponceCodes.COPY_SCHEDULE_SUCCESSFULL)) {

				saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_COPY_SUCCESS), WebConstants.MSG_SUCCESS);
				request.setAttribute(WebConstants.POPUPMESSAGE, airadminConfig.getMessage(WebConstants.KEY_SCHEDULE_COPY_SUCCESS));

			} else if (responceCode.equals(ResponceCodes.BUILD_SCHEDULE_SUCCESSFULL)) {

				saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_BUILD_SUCCESS), WebConstants.MSG_SUCCESS);
				request.setAttribute(WebConstants.POPUPMESSAGE,
						airadminConfig.getMessage(WebConstants.KEY_SCHEDULE_BUILD_SUCCESS));
			}

		} else if (responceCode.equals(ResponceCodes.RESERVATIONS_FOUND_FOR_SCHEDULE)) {

			HashMap resrMap = (HashMap) respon.getResponseParam(ResponceCodes.RESERVATIONS_FOUND_FOR_SCHEDULE);
			ScheduleHTMLGenerator htmlGen = new ScheduleHTMLGenerator();
			String strCan = htmlGen.getCanReprotectArray(resrMap);
			SimpleDateFormat smdt = new SimpleDateFormat("dd/MM/yyyy");
			request.setAttribute(WebConstants.REQ_SEATS_TO_BE_REPROTECTED, strCan);
			request.setAttribute(WebConstants.REQ_CANCEL_TRUE, WebConstants.CANCEL_TRUE);
			request.setAttribute(WebConstants.REQ_REPRO_SCHEDID, sched.getScheduleId());
			request.setAttribute(WebConstants.REQ_REPRO_STARTD, smdt.format(sched.getStartDate()));
			request.setAttribute(WebConstants.REQ_REPRO_STOPD, smdt.format(sched.getStopDate()));
			request.setAttribute(WebConstants.REQ_REPRO_FLTNO, sched.getFlightNumber());
			request.setAttribute(WebConstants.REQ_REPRO_DESTINATION, sched.getArrivalStnCode());
			request.setAttribute(WebConstants.REQ_REPRO_ORIGIN, sched.getDepartureStnCode());
			request.setAttribute(WebConstants.REQ_REPRO_FREQUENCY, htmlGen.createSegmentArray(sched.getFrequency()));

			if (Boolean.valueOf(request.getParameter(PARAM_REPROTECT)).booleanValue()) {
				strResponseMessage = WebConstants.ACTION_FORWARD_REPROTECT;
			}
		} else if (responceCode.equals(ResponceCodes.RESERVATIONS_NOT_FOUND_FOR_SCHEDULE)) {

			HashMap resrMap = new HashMap();
			ScheduleHTMLGenerator htmlGen = new ScheduleHTMLGenerator();
			String strCan = htmlGen.getCanReprotectArray(resrMap);
			SimpleDateFormat smdt = new SimpleDateFormat("dd/MM/yyyy");
			request.setAttribute(WebConstants.REQ_SEATS_TO_BE_REPROTECTED, strCan);
			request.setAttribute(WebConstants.REQ_CANCEL_TRUE, WebConstants.CANCEL_TRUE);
			request.setAttribute(WebConstants.REQ_REPRO_SCHEDID, sched.getScheduleId());
			request.setAttribute(WebConstants.REQ_REPRO_STARTD, smdt.format(sched.getStartDate()));
			request.setAttribute(WebConstants.REQ_REPRO_STOPD, smdt.format(sched.getStopDate()));
			request.setAttribute(WebConstants.REQ_REPRO_FLTNO, sched.getFlightNumber());
			request.setAttribute(WebConstants.REQ_REPRO_DESTINATION, sched.getArrivalStnCode());
			request.setAttribute(WebConstants.REQ_REPRO_ORIGIN, sched.getDepartureStnCode());
			request.setAttribute(WebConstants.REQ_REPRO_FREQUENCY, htmlGen.createSegmentArray(sched.getFrequency()));

			if (Boolean.valueOf(request.getParameter(PARAM_REPROTECT)).booleanValue()) {
				strResponseMessage = WebConstants.ACTION_FORWARD_REPROTECT;
			}
		} else if (responceCode.equals(ResponceCodes.SCHEDULE_FLIGHT_CONFLICT)) {

			request.setAttribute(WebConstants.REQ_MODEL, sched);
			Collection<OverlappedFlightDTO> col = (Collection) respon.getResponseParam(ResponceCodes.SCHEDULE_FLIGHT_CONFLICT);
			Iterator<OverlappedFlightDTO> iter = col.iterator();
			OverlappedFlightDTO flight = iter.next();
			saveMessage(request, airadminConfig.getMessage(WebConstants.SCHEDULE_FLIGHT_CONFLICT) + col.size()
					+ " Flights. A Conflicting Flight = " + flight.getFlightNumber() + "," + flight.getDepartureDate(),
					WebConstants.MSG_ERROR);
			isSaveError = true;
			strErrorMEssage = airadminConfig.getMessage(WebConstants.SCHEDULE_FLIGHT_CONFLICT) + col.size()
					+ " Flights. A Conflicting Flight = " + flight.getFlightNumber() + "," + flight.getDepartureDate();
		} else if (responceCode.equals(ResponceCodes.SCHEDULE_SCHEDULE_CONFLICT)) {

			request.setAttribute(WebConstants.REQ_MODEL, sched);
			Collection<OverlappedScheduleDTO> col = (Collection) respon
					.getResponseParam(ResponceCodes.SCHEDULE_SCHEDULE_CONFLICT);
			Iterator<OverlappedScheduleDTO> iter = col.iterator();
			OverlappedScheduleDTO flightSch = iter.next();
			saveMessage(
					request,
					airadminConfig.getMessage(WebConstants.SCHEDULE_SCHEDULE_CONFLICT) + "Schedule = "
							+ flightSch.getFlightNumber() + "," + flightSch.getStartDate(), WebConstants.MSG_ERROR);
			isSaveError = true;
			strErrorMEssage = airadminConfig.getMessage(WebConstants.SCHEDULE_SCHEDULE_CONFLICT) + "Schedule = "
					+ flightSch.getFlightNumber() + "," + flightSch.getStartDate();

		} else if (responceCode.equals(ResponceCodes.INVALID_LEG_DURATION)) {

			FlightSchedule flsched = (FlightSchedule) respon.getResponseParam(ResponceCodes.INVALID_LEG_DURATION);

			Collection<FlightScheduleLeg> legs = flsched.getFlightScheduleLegs();
			Iterator<FlightScheduleLeg> itLegs = legs.iterator();
			String strError = "";
			String strDurations = "var duration = new Array();";

			while (itLegs.hasNext()) {
				FlightScheduleLeg leg = itLegs.next();

				strError = strError + "(" + leg.getLegNumber() + "," + leg.getDurationError() + ")";
				strDurations += "duration[" + leg.getLegNumber() + "] = new Array('" + leg.getDuration() + "','"
						+ leg.getDurationError() + "');";
			}

			saveMessage(request, airadminConfig.getMessage(WebConstants.SCHEDULE_INVALID_LEG) + strError, WebConstants.MSG_ERROR);
			request.setAttribute(WebConstants.DURATIONERROR, strDurations);
			request.setAttribute(WebConstants.HASDURATIONERROR, "true");

			isSaveError = true;
			strErrorMEssage = airadminConfig.getMessage(WebConstants.SCHEDULE_INVALID_LEG) + strError;
		} else if (responceCode.equals(ResponceCodes.AllOWED_DURATION_ZERO)) {
			FlightScheduleLeg leg = (FlightScheduleLeg) respon.getResponseParam(ResponceCodes.AllOWED_DURATION_ZERO);
			saveMessage(request, airadminConfig.getMessage(WebConstants.SCHEDULE_ALLOWED_DURATION_ZERO) + leg.getModelRouteId(),
					WebConstants.MSG_ERROR);
			isSaveError = true;
			strErrorMEssage = airadminConfig.getMessage(WebConstants.SCHEDULE_ALLOWED_DURATION_ZERO) + leg.getModelRouteId();
		} else if (responceCode.equals(ResponceCodes.CANNOT_DELETE_BC_INVENTORY)) {
			strErrorMEssage = airadminConfig.getMessage(
					WebConstants.CANNOT_DELETE_BC_INVENTORY,
					new Object[] { respon.getResponseParam(ResponceCodes.CANNOT_DELETE_BC_INVENTORY),
							respon.getResponseParam(ResponceCodes.DATE) });
			saveMessage(request, strErrorMEssage, WebConstants.MSG_ERROR);
			isSaveError = true;
		} else if (responceCode.equals(ResponceCodes.CABIN_CLASS_NOT_FOUND)) {
			strErrorMEssage = airadminConfig.getMessage(
					WebConstants.CABIN_CLASS_NOT_FOUND,
					new Object[] { respon.getResponseParam(ResponceCodes.CABIN_CLASS_NOT_FOUND),
							respon.getResponseParam(ResponceCodes.DATE) });
			saveMessage(request, strErrorMEssage, WebConstants.MSG_ERROR);
			isSaveError = true;
		} else if (responceCode.equals(ResponceCodes.CC_CAPACITY_INSUFFICIENT)) {
			strErrorMEssage = airadminConfig.getMessage(
					WebConstants.CC_CAPACITY_INSUFFICIENT,
					new Object[] { respon.getResponseParam(ResponceCodes.CC_CAPACITY_INSUFFICIENT),
							respon.getResponseParam(ResponceCodes.DATE) });
			saveMessage(request, strErrorMEssage, WebConstants.MSG_ERROR);
			isSaveError = true;
		} else if (responceCode.equals(ResponceCodes.ALL_SEGMENTS_INVALID)) {
			saveMessage(request, airadminConfig.getMessage(WebConstants.ALL_SEGMENTS_INVALID), WebConstants.MSG_ERROR);
			isSaveError = true;
			strErrorMEssage = airadminConfig.getMessage(WebConstants.ALL_SEGMENTS_INVALID);
		} else if (responceCode.equals(ResponceCodes.INVALID_OVERLAPPING_SCHEDULE)) {
			saveMessage(request, airadminConfig.getMessage(WebConstants.INVALID_OVERLAPPING_SCHEDULE), WebConstants.MSG_ERROR);
			isSaveError = true;
			strErrorMEssage = airadminConfig.getMessage(WebConstants.INVALID_OVERLAPPING_SCHEDULE);
		} else if (responceCode.equals(ResponceCodes.INVALID_SCHEDULE_FREQUENCE_FOR_DATE_RANGE)) {
			saveMessage(request, airadminConfig.getMessage(WebConstants.INVALID_SCHEDULE_FREQUENCE_FOR_DATE_RANGE),
					WebConstants.MSG_ERROR);
			isSaveError = true;
			strErrorMEssage = airadminConfig.getMessage(WebConstants.INVALID_SCHEDULE_FREQUENCE_FOR_DATE_RANGE);
		} else if (responceCode.equals(ResponceCodes.OVERLAPPING_ALREADY_OVERLAPPING_SCHEDULE)) {
			saveMessage(request, airadminConfig.getMessage(WebConstants.OVERLAPPING_ALREADY_OVERLAPPING_SCHEDULE),
					WebConstants.MSG_ERROR);
			isSaveError = true;
			strErrorMEssage = airadminConfig.getMessage(WebConstants.OVERLAPPING_ALREADY_OVERLAPPING_SCHEDULE);
		} else if (responceCode.equals(ResponceCodes.INVALID_OVERLAPPING_SEGMENT_LEG)) {
			saveMessage(request, airadminConfig.getMessage(WebConstants.INVALID_OVERLAPPING_SEGMENT_LEG), WebConstants.MSG_ERROR);
			isSaveError = true;
			strErrorMEssage = airadminConfig.getMessage(WebConstants.INVALID_OVERLAPPING_SEGMENT_LEG);
		} else if (responceCode.equals(ResponceCodes.INVALID_OVERLAPPING_SCHEDULE_STATUS)) {
			saveMessage(request, airadminConfig.getMessage(WebConstants.INVALID_OVERLAPPING_SCHEDULE_STATUS),
					WebConstants.MSG_ERROR);
			isSaveError = true;
			strErrorMEssage = airadminConfig.getMessage(WebConstants.INVALID_OVERLAPPING_SCHEDULE_STATUS);
		} else if (responceCode.equals(ResponceCodes.BUILD_SCHEDULE_PARTIALLY_SUCCESSFUL)) {
			saveMessage(request, airadminConfig.getMessage(WebConstants.BUILD_SCHEDULE_PARTIALLY_SUCCESSFUL),
					WebConstants.MSG_ERROR);
			isSaveError = true;
			strErrorMEssage = airadminConfig.getMessage(WebConstants.BUILD_SCHEDULE_PARTIALLY_SUCCESSFUL);
		} else if (responceCode.equals(ResponceCodes.BUILD_SCHEDULE_FAILED)) {
			saveMessage(request, airadminConfig.getMessage(WebConstants.BUILD_SCHEDULE_FAILED), WebConstants.MSG_ERROR);
			isSaveError = true;
			strErrorMEssage = airadminConfig.getMessage(WebConstants.BUILD_SCHEDULE_FAILED);
		} else if (responceCode.equals(ResponceCodes.AIRCRAFT_UPDATED_MODEL_SEATATTACHED)) {
			saveMessage(request, airadminConfig.getMessage(WebConstants.AIRCRAFT_UPDATED_MODEL_SEATATTACHED),
					WebConstants.MSG_ERROR);
			isSaveError = true;
			strErrorMEssage = airadminConfig.getMessage(WebConstants.AIRCRAFT_UPDATED_MODEL_SEATATTACHED);
		} else if (responceCode.equals(ResponceCodes.CC_CURTAILED_CAPACITY_INSUFFICIENT)) {
			strErrorMEssage = airadminConfig.getMessage(ResponceCodes.CC_CURTAILED_CAPACITY_INSUFFICIENT);
			saveMessage(request, strErrorMEssage, WebConstants.MSG_ERROR);
			isSaveError = true;
		} else if (responceCode.equals(ResponceCodes.CC_INFANT_CAPACITY_INSUFFICIENT)) {
			strErrorMEssage = airadminConfig.getMessage(ResponceCodes.CC_INFANT_CAPACITY_INSUFFICIENT);
			saveMessage(request, strErrorMEssage, WebConstants.MSG_ERROR);
			isSaveError = true;
		} else {
			saveMessage(request, airadminConfig.getMessage(WebConstants.SCHEDULE_UNHANDLED_ERROR), WebConstants.MSG_ERROR);
			isSaveError = true;
			strErrorMEssage = airadminConfig.getMessage(WebConstants.SCHEDULE_UNHANDLED_ERROR);
		}
		return strResponseMessage;
	}

	private static FlightSchedule createFlightSchedule(Properties sched) throws ModuleException, ParseException {

		FlightSchedule flightSchedule = new FlightSchedule();

		// get the schedule infomation from Property file
		ArrayList<String> depaturelist = new ArrayList<String>();
		addToList(depaturelist, sched.getProperty(PARAM_LEG_DEPATURE_1));
		addToList(depaturelist, sched.getProperty(PARAM_LEG_DEPATURE_2));
		addToList(depaturelist, sched.getProperty(PARAM_LEG_DEPATURE_3));
		addToList(depaturelist, sched.getProperty(PARAM_LEG_DEPATURE_4));
		addToList(depaturelist, sched.getProperty(PARAM_LEG_DEPATURE_5));

		ArrayList<String> arrivalList = new ArrayList<String>();
		addToList(arrivalList, sched.getProperty(PARAM_LEG_ARRIVAL_1));
		addToList(arrivalList, sched.getProperty(PARAM_LEG_ARRIVAL_2));
		addToList(arrivalList, sched.getProperty(PARAM_LEG_ARRIVAL_3));
		addToList(arrivalList, sched.getProperty(PARAM_LEG_ARRIVAL_4));
		addToList(arrivalList, sched.getProperty(PARAM_LEG_ARRIVAL_5));

		ArrayList<String> arrivalTimelist = new ArrayList<String>();
		addToList(arrivalTimelist, sched.getProperty(PARAM_LEG_ARRIVAL_TIME_1));
		addToList(arrivalTimelist, sched.getProperty(PARAM_LEG_ARRIVAL_TIME_2));
		addToList(arrivalTimelist, sched.getProperty(PARAM_LEG_ARRIVAL_TIME_3));
		addToList(arrivalTimelist, sched.getProperty(PARAM_LEG_ARRIVAL_TIME_4));
		addToList(arrivalTimelist, sched.getProperty(PARAM_LEG_ARRIVAL_TIME_5));

		ArrayList<String> depatureTimelist = new ArrayList<String>();
		addToList(depatureTimelist, sched.getProperty(PARAM_LEG_DEPATURE_TIME_1));
		addToList(depatureTimelist, sched.getProperty(PARAM_LEG_DEPATURE_TIME_2));
		addToList(depatureTimelist, sched.getProperty(PARAM_LEG_DEPATURE_TIME_3));
		addToList(depatureTimelist, sched.getProperty(PARAM_LEG_DEPATURE_TIME_4));
		addToList(depatureTimelist, sched.getProperty(PARAM_LEG_DEPATURE_TIME_5));

		ArrayList<String> arrivalVarianceList = new ArrayList<String>();
		addToList(arrivalVarianceList, sched.getProperty(PARAM_LEG_ARRIVAL_VARIANCE_1));
		addToList(arrivalVarianceList, sched.getProperty(PARAM_LEG_ARRIVAL_VARIANCE_2));
		addToList(arrivalVarianceList, sched.getProperty(PARAM_LEG_ARRIVAL_VARIANCE_3));
		addToList(arrivalVarianceList, sched.getProperty(PARAM_LEG_ARRIVAL_VARIANCE_4));
		addToList(arrivalVarianceList, sched.getProperty(PARAM_LEG_ARRIVAL_VARIANCE_5));

		ArrayList<String> depatureVarianceList = new ArrayList<String>();
		addToList(depatureVarianceList, sched.getProperty(PARAM_LEG_DEPATURE_VARIANCE_1));
		addToList(depatureVarianceList, sched.getProperty(PARAM_LEG_DEPATURE_VARIANCE_2));
		addToList(depatureVarianceList, sched.getProperty(PARAM_LEG_DEPATURE_VARIANCE_3));
		addToList(depatureVarianceList, sched.getProperty(PARAM_LEG_DEPATURE_VARIANCE_4));
		addToList(depatureVarianceList, sched.getProperty(PARAM_LEG_DEPATURE_VARIANCE_5));

		String strLegValidated = sched.getProperty(PARAM_LEG_VALIDITY);
		String strValidatedSegs = sched.getProperty(PARAM_VALID_SEGMENT);
		String strValidatedTerminals = sched.getProperty(PARAM_VALID_TERMINAL);
		String strValidOlapId = sched.getProperty(PARAM_VALID_OSCHEDID);

		if (!sched.getProperty(PARAM_VERSION).equals("")) {
			flightSchedule.setVersion(Long.parseLong(sched.getProperty(PARAM_VERSION)));
		}

		if (!sched.getProperty(PARAM_STATUS).equals("")) {
			flightSchedule.setStatusCode(sched.getProperty(PARAM_STATUS));
		}

		if (!sched.getProperty(PARAM_BUILD_STATUS).equals("")) {
			flightSchedule.setBuildStatusCode(sched.getProperty(PARAM_BUILD_STATUS));
		}
		// phasing the start date
		Date startDate = null;
		startDate = ScheduleUtils.parseToDate(sched.getProperty(PARAM_IN_STARTDATE).trim());

		// pharsing the stop date
		Date stopDate = null;
		stopDate = ScheduleUtils.parseToDate(sched.getProperty(PARAM_IN_STOPDATE).trim());

		// setting schedule properties
		flightSchedule.setDepartureStnCode(sched.getProperty(PARAM_LEG_DEPATURE_1));
		flightSchedule.setArrivalStnCode(sched.getProperty(PARAM_IN_ARRIVAL_STN));
		flightSchedule.setOperationTypeId(Integer.parseInt(sched.getProperty(PARAM_IN_OPERATIONTYPE)));
		flightSchedule.setFlightNumber(sched.getProperty(PARAM_IN_FLIGHTNUMBERSTART).toUpperCase()
				+ sched.getProperty(PARAM_IN_FLIGHTNUMBER).toUpperCase());
		flightSchedule.setModelNumber(sched.getProperty(PARAM_IN_AIRCRAFTMODEL));
		flightSchedule.setFlightType(sched.getProperty(PARAM_FLIGHT_TYPE));
		flightSchedule.setRemarks(sched.getProperty(PARAM_FLIGHT_REMARKS));

		String mealTemplateCode = sched.getProperty(PARAM_MEAL_TEMPLATE);
		String seatChargeTemplateCode = sched.getProperty(PARAM_SEAT_CHARGE_TEMPLATE);

		if (!mealTemplateCode.equals("null") && !mealTemplateCode.equals("")) {
			flightSchedule.setMealTemplateId(new Integer(mealTemplateCode));
		}

		if (!seatChargeTemplateCode.equals("null") && !seatChargeTemplateCode.equals("")) {
			flightSchedule.setSeatChargeTemplateId(new Integer(seatChargeTemplateCode));
		}

		// setting the schedule frequency
		Collection<DayOfWeek> dayList = new ArrayList<DayOfWeek>();

		if (sched.getProperty(PARAM_IN_SUNDAY).equals("on"))
			dayList.add(DayOfWeek.SUNDAY);
		if (sched.getProperty(PARAM_IN_MONDAY).equals("on"))
			dayList.add(DayOfWeek.MONDAY);
		if (sched.getProperty(PARAM_IN_TUESDAY).equals("on"))
			dayList.add(DayOfWeek.TUESDAY);
		if (sched.getProperty(PARAM_IN_WEDNESDAY).equals("on"))
			dayList.add(DayOfWeek.WEDNESDAY);
		if (sched.getProperty(PARAM_IN_THURSDAY).equals("on"))
			dayList.add(DayOfWeek.THURSDAY);
		if (sched.getProperty(PARAM_IN_FRIDAY).equals("on"))
			dayList.add(DayOfWeek.FRIDAY);
		if (sched.getProperty(PARAM_IN_SATURDAY).equals("on"))
			dayList.add(DayOfWeek.SATURDAY);

		Frequency fqn = CalendarUtil.getFrequencyFromDays(dayList);

		// setting schedule legs
		Set<FlightScheduleLeg> legSet = new HashSet<FlightScheduleLeg>();

		Object[] depatures = depaturelist.toArray();

		Object[] arrivals = arrivalList.toArray();
		Object[] arrivalTimes = arrivalTimelist.toArray();
		Object[] depaturTimes = depatureTimelist.toArray();
		Object[] arrivalVariances = arrivalVarianceList.toArray();
		Object[] depatureVariances = depatureVarianceList.toArray();

		String defaultFormat // get the default time format
		= globalConfig.getBizParam(SystemParamKeys.DATE_FORMAT_FOR_DATE_STRINGS);

		for (int i = 0; i < depatures.length; i++) {

			FlightScheduleLeg fScheLeg = new FlightScheduleLeg();
			fScheLeg.setOrigin((String) depatures[i]);
			fScheLeg.setDestination((String) arrivals[i]);

			if (sched.getProperty(PARAM_TIMEMODE).equals(WebConstants.LOCALTIME)) {
				fScheLeg.setEstDepartureTimeLocal(CalendarUtil.getParsedTime((String) depaturTimes[i], defaultFormat));
				fScheLeg.setEstArrivalTimeLocal(CalendarUtil.getParsedTime((String) arrivalTimes[i], defaultFormat));
				fScheLeg.setEstArrivalDayOffsetLocal(Integer.parseInt((String) arrivalVariances[i]));
				fScheLeg.setEstDepartureDayOffsetLocal(Integer.parseInt((String) depatureVariances[i]));

				flightSchedule.setStartDateLocal(startDate);
				flightSchedule.setStopDateLocal(stopDate);
				flightSchedule.setFrequencyLocal(fqn);
				flightSchedule.setLocalChang(true);
			} else {
				fScheLeg.setEstDepartureTimeZulu(CalendarUtil.getParsedTime((String) depaturTimes[i], defaultFormat));
				fScheLeg.setEstArrivalTimeZulu(CalendarUtil.getParsedTime((String) arrivalTimes[i], defaultFormat));
				fScheLeg.setEstArrivalDayOffset(Integer.parseInt((String) arrivalVariances[i]));
				fScheLeg.setEstDepartureDayOffset(Integer.parseInt((String) depatureVariances[i]));

				flightSchedule.setStartDate(startDate);
				flightSchedule.setStopDate(stopDate);
				flightSchedule.setFrequency(fqn);
				flightSchedule.setLocalChang(false);
			}
			fScheLeg.setLegNumber(i + 1);
			legSet.add(fScheLeg);
		}
		flightSchedule.setFlightScheduleLegs(legSet);

		// setting schedule segments
		if (strLegValidated.equals(WebConstants.APP_BOOLEAN_TRUE)
				|| sched.getProperty(PARAM_MODE).equals(WebConstants.ACTION_EDITLEG)) {
			if (strValidOlapId != null && !"".equals(strValidOlapId)) {
				flightSchedule.setOverlapingScheduleId(new Integer(strValidOlapId));
			}

			HashSet<FlightScheduleSegment> segmentSet = ScheduleUtils.getValidatedSegments(strValidatedSegs,
					ScheduleUtils.getValidatedTerminals(strValidatedTerminals));
			HashSet<FlightScheduleSegment> segmentLegSet = ScheduleUtils.getSegments(legSet,
					ScheduleUtils.getValidatedTerminals(strValidatedTerminals));
			HashSet<FlightScheduleSegment> comparedSet = ScheduleUtils.getComparedSegments(segmentLegSet, segmentSet);
			flightSchedule.setFlightScheduleSegments(comparedSet);
		} else if (!sched.getProperty(PARAM_MODE).equals(WebConstants.ACTION_ADD)) {
			HashSet<FlightScheduleSegment> segmentSet = ScheduleUtils.getValidatedSegments(strValidatedSegs,
					ScheduleUtils.getValidatedTerminals(strValidatedTerminals));
			flightSchedule.setFlightScheduleSegments(segmentSet);
		} else {
			HashSet<FlightScheduleSegment> segmentSet = ScheduleUtils.getSegments(legSet,
					ScheduleUtils.getValidatedTerminals(strValidatedTerminals));
			flightSchedule.setFlightScheduleSegments(segmentSet);
		}

		if (!sched.getProperty(PARAM_OVERLAPSCEDID).equals("null") && !(sched.getProperty(PARAM_OVERLAPSCEDID).equals(""))) {
			flightSchedule.setOverlapingScheduleId(new Integer(sched.getProperty(PARAM_OVERLAPSCEDID)));
		}
		//get CodeShare details
		HashSet<CodeShareMCFlightSchedule> CodeShareMCFlightSchedules =ScheduleUtils.getCodeShareMCFlight(sched.getProperty(PARAM_CODESHARE));
		flightSchedule.setCodeShareMCFlightSchedules(CodeShareMCFlightSchedules);	
		
		flightSchedule.setCsOCCarrierCode(sched.getProperty(PARAM_CSOCCARRIER));
		flightSchedule.setCsOCFlightNumber(sched.getProperty(PARAM_CSOCFLIGHT));

		
		return flightSchedule;
	}

	private static Set<Integer> getGdsIdList(String gdsIdStr) {

		String gdsidarr[] = StringUtils.split(gdsIdStr, ",");
		HashSet<Integer> set = new HashSet<Integer>();
		for (int i = 0; i < gdsidarr.length; i++) {
			if (!"".equals(gdsidarr[i])) {
				set.add(new Integer(gdsidarr[i]));
			}
		}

		return set;
	}

	/**
	 * Method to set request values to the property file
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return Properties the Property file with Request values
	 */
	protected static Properties getProperties(HttpServletRequest request) {
		Properties props = new Properties();

		props.setProperty(PARAM_IN_STARTDATE, AiradminUtils.getNotNullString(request.getParameter(PARAM_IN_STARTDATE)));
		props.setProperty(PARAM_IN_STOPDATE, AiradminUtils.getNotNullString(request.getParameter(PARAM_IN_STOPDATE)));
		props.setProperty(PARAM_IN_OPERATIONTYPE, AiradminUtils.getNotNullString(request.getParameter(PARAM_IN_OPERATIONTYPE)));
		props.setProperty(PARAM_IN_FLIGHTNUMBER, AiradminUtils.getNotNullString(request.getParameter(PARAM_IN_FLIGHTNUMBER)));
		props.setProperty(PARAM_IN_FLIGHTNUMBERSTART,
				AiradminUtils.getNotNullString(request.getParameter(PARAM_IN_FLIGHTNUMBERSTART)));
		props.setProperty(PARAM_IN_AIRCRAFTMODEL, AiradminUtils.getNotNullString(request.getParameter(PARAM_IN_AIRCRAFTMODEL)));
		props.setProperty(PARAM_IN_MONDAY, AiradminUtils.getNotNullString(request.getParameter(PARAM_IN_MONDAY)));
		props.setProperty(PARAM_IN_TUESDAY, AiradminUtils.getNotNullString(request.getParameter(PARAM_IN_TUESDAY)));
		props.setProperty(PARAM_IN_WEDNESDAY, AiradminUtils.getNotNullString(request.getParameter(PARAM_IN_WEDNESDAY)));
		props.setProperty(PARAM_IN_THURSDAY, AiradminUtils.getNotNullString(request.getParameter(PARAM_IN_THURSDAY)));
		props.setProperty(PARAM_IN_FRIDAY, AiradminUtils.getNotNullString(request.getParameter(PARAM_IN_FRIDAY)));
		props.setProperty(PARAM_IN_SATURDAY, AiradminUtils.getNotNullString(request.getParameter(PARAM_IN_SATURDAY)));
		props.setProperty(PARAM_IN_SUNDAY, AiradminUtils.getNotNullString(request.getParameter(PARAM_IN_SUNDAY)));
		props.setProperty(PARAM_IN_DEPART_STN, AiradminUtils.getNotNullString(request.getParameter(PARAM_IN_DEPART_STN)));
		props.setProperty(PARAM_IN_ARRIVAL_STN, AiradminUtils.getNotNullString(request.getParameter(PARAM_IN_ARRIVAL_STN)));
		props.setProperty(PARAM_IN_GDS_IDS, AiradminUtils.getNotNullString(request.getParameter(PARAM_IN_GDS_IDS)));
		props.setProperty(PARAM_VERSION, AiradminUtils.getNotNullString(request.getParameter(PARAM_VERSION)));

		// Copy & Split
		props.setProperty(PARAM_COPY_STARTDATE, AiradminUtils.getNotNullString(request.getParameter(PARAM_COPY_STARTDATE)));
		props.setProperty(PARAM_COPY_STOPDATE, AiradminUtils.getNotNullString(request.getParameter(PARAM_COPY_STOPDATE)));
		props.setProperty(PARAM_SPLIT_STARTDATE, AiradminUtils.getNotNullString(request.getParameter(PARAM_SPLIT_STARTDATE)));
		props.setProperty(PARAM_SPLIT_STOPDATE, AiradminUtils.getNotNullString(request.getParameter(PARAM_SPLIT_STOPDATE)));
		props.setProperty(PARAM_SPLIT_MONDAY, AiradminUtils.getNotNullString(request.getParameter(PARAM_SPLIT_MONDAY)));
		props.setProperty(PARAM_SPLIT_TUESDAY, AiradminUtils.getNotNullString(request.getParameter(PARAM_SPLIT_TUESDAY)));
		props.setProperty(PARAM_SPLIT_WEDNESDAY, AiradminUtils.getNotNullString(request.getParameter(PARAM_SPLIT_WEDNESDAY)));
		props.setProperty(PARAM_SPLIT_THURSDAY, AiradminUtils.getNotNullString(request.getParameter(PARAM_SPLIT_THURSDAY)));
		props.setProperty(PARAM_SPLIT_FRIDAY, AiradminUtils.getNotNullString(request.getParameter(PARAM_SPLIT_FRIDAY)));
		props.setProperty(PARAM_SPLIT_SATURDAY, AiradminUtils.getNotNullString(request.getParameter(PARAM_SPLIT_SATURDAY)));
		props.setProperty(PARAM_SPLIT_SUNDAY, AiradminUtils.getNotNullString(request.getParameter(PARAM_SPLIT_SUNDAY)));

		props.setProperty(PARAM_SCHEDULEID, AiradminUtils.getNotNullString(request.getParameter(PARAM_SCHEDULEID)));
		props.setProperty(PARAM_TIMEMODE, AiradminUtils.getNotNullString(request.getParameter(PARAM_TIMEMODE)));
		props.setProperty(PARAM_ALLSCHEDULEIDS, AiradminUtils.getNotNullString(request.getParameter(PARAM_ALLSCHEDULEIDS)));
		props.setProperty(PARAM_MODE, AiradminUtils.getNotNullString(request.getParameter(PARAM_MODE)));
		props.setProperty(PARAM_STATUS, AiradminUtils.getNotNullString(request.getParameter(PARAM_STATUS)));
		props.setProperty(PARAM_FLIGHT_TYPE, AiradminUtils.getNotNullString(request.getParameter(PARAM_FLIGHT_TYPE)));
		props.setProperty(PARAM_BUILD_STATUS, AiradminUtils.getNotNullString(request.getParameter(PARAM_BUILD_STATUS)));

		// leg properties
		props.setProperty(PARAM_LEG_ARRIVAL_1, AiradminUtils.getNotNullString(request.getParameter(PARAM_LEG_ARRIVAL_1)));
		props.setProperty(PARAM_LEG_DEPATURE_1, AiradminUtils.getNotNullString(request.getParameter(PARAM_LEG_DEPATURE_1)));
		props.setProperty(PARAM_LEG_ARRIVAL_TIME_1,
				AiradminUtils.getNotNullString(request.getParameter(PARAM_LEG_ARRIVAL_TIME_1)));
		props.setProperty(PARAM_LEG_DEPATURE_TIME_1,
				AiradminUtils.getNotNullString(request.getParameter(PARAM_LEG_DEPATURE_TIME_1)));
		props.setProperty(PARAM_LEG_ARRIVAL_VARIANCE_1,
				AiradminUtils.getNotNullString(request.getParameter(PARAM_LEG_ARRIVAL_VARIANCE_1)));
		props.setProperty(PARAM_LEG_DEPATURE_VARIANCE_1,
				AiradminUtils.getNotNullString(request.getParameter(PARAM_LEG_DEPATURE_VARIANCE_1)));

		props.setProperty(PARAM_LEG_ARRIVAL_2, AiradminUtils.getNotNullString(request.getParameter(PARAM_LEG_ARRIVAL_2)));
		props.setProperty(PARAM_LEG_DEPATURE_2, AiradminUtils.getNotNullString(request.getParameter(PARAM_LEG_DEPATURE_2)));
		props.setProperty(PARAM_LEG_ARRIVAL_TIME_2,
				AiradminUtils.getNotNullString(request.getParameter(PARAM_LEG_ARRIVAL_TIME_2)));
		props.setProperty(PARAM_LEG_DEPATURE_TIME_2,
				AiradminUtils.getNotNullString(request.getParameter(PARAM_LEG_DEPATURE_TIME_2)));
		props.setProperty(PARAM_LEG_ARRIVAL_VARIANCE_2,
				AiradminUtils.getNotNullString(request.getParameter(PARAM_LEG_ARRIVAL_VARIANCE_2)));
		props.setProperty(PARAM_LEG_DEPATURE_VARIANCE_2,
				AiradminUtils.getNotNullString(request.getParameter(PARAM_LEG_DEPATURE_VARIANCE_2)));

		props.setProperty(PARAM_LEG_ARRIVAL_3, AiradminUtils.getNotNullString(request.getParameter(PARAM_LEG_ARRIVAL_3)));
		props.setProperty(PARAM_LEG_DEPATURE_3, AiradminUtils.getNotNullString(request.getParameter(PARAM_LEG_DEPATURE_3)));
		props.setProperty(PARAM_LEG_ARRIVAL_TIME_3,
				AiradminUtils.getNotNullString(request.getParameter(PARAM_LEG_ARRIVAL_TIME_3)));
		props.setProperty(PARAM_LEG_DEPATURE_TIME_3,
				AiradminUtils.getNotNullString(request.getParameter(PARAM_LEG_DEPATURE_TIME_3)));
		props.setProperty(PARAM_LEG_ARRIVAL_VARIANCE_3,
				AiradminUtils.getNotNullString(request.getParameter(PARAM_LEG_ARRIVAL_VARIANCE_3)));
		props.setProperty(PARAM_LEG_DEPATURE_VARIANCE_3,
				AiradminUtils.getNotNullString(request.getParameter(PARAM_LEG_DEPATURE_VARIANCE_3)));

		props.setProperty(PARAM_LEG_ARRIVAL_4, AiradminUtils.getNotNullString(request.getParameter(PARAM_LEG_ARRIVAL_4)));
		props.setProperty(PARAM_LEG_DEPATURE_4, AiradminUtils.getNotNullString(request.getParameter(PARAM_LEG_DEPATURE_4)));
		props.setProperty(PARAM_LEG_ARRIVAL_TIME_4,
				AiradminUtils.getNotNullString(request.getParameter(PARAM_LEG_ARRIVAL_TIME_4)));
		props.setProperty(PARAM_LEG_DEPATURE_TIME_4,
				AiradminUtils.getNotNullString(request.getParameter(PARAM_LEG_DEPATURE_TIME_4)));
		props.setProperty(PARAM_LEG_ARRIVAL_VARIANCE_4,
				AiradminUtils.getNotNullString(request.getParameter(PARAM_LEG_ARRIVAL_VARIANCE_4)));
		props.setProperty(PARAM_LEG_DEPATURE_VARIANCE_4,
				AiradminUtils.getNotNullString(request.getParameter(PARAM_LEG_DEPATURE_VARIANCE_4)));

		props.setProperty(PARAM_LEG_ARRIVAL_5, AiradminUtils.getNotNullString(request.getParameter(PARAM_LEG_ARRIVAL_5)));
		props.setProperty(PARAM_LEG_DEPATURE_5, AiradminUtils.getNotNullString(request.getParameter(PARAM_LEG_DEPATURE_5)));
		props.setProperty(PARAM_LEG_ARRIVAL_TIME_5,
				AiradminUtils.getNotNullString(request.getParameter(PARAM_LEG_ARRIVAL_TIME_5)));
		props.setProperty(PARAM_LEG_DEPATURE_TIME_5,
				AiradminUtils.getNotNullString(request.getParameter(PARAM_LEG_DEPATURE_TIME_5)));
		props.setProperty(PARAM_LEG_ARRIVAL_VARIANCE_5,
				AiradminUtils.getNotNullString(request.getParameter(PARAM_LEG_ARRIVAL_VARIANCE_5)));
		props.setProperty(PARAM_LEG_DEPATURE_VARIANCE_5,
				AiradminUtils.getNotNullString(request.getParameter(PARAM_LEG_DEPATURE_VARIANCE_5)));

		props.setProperty(PARAM_LEG_VALIDITY, AiradminUtils.getNotNullString(request.getParameter(PARAM_LEG_VALIDITY)));
		props.setProperty(PARAM_VALID_SEGMENT, AiradminUtils.getNotNullString(request.getParameter(PARAM_VALID_SEGMENT)));
		props.setProperty(PARAM_VALID_TERMINAL, AiradminUtils.getNotNullString(request.getParameter(PARAM_VALID_TERMINAL)));
		props.setProperty(PARAM_VALID_SEGMET_OLAPID,
				AiradminUtils.getNotNullString(request.getParameter(PARAM_VALID_SEGMET_OLAPID)));
		props.setProperty(PARAM_VALID_OSCHEDID, AiradminUtils.getNotNullString(request.getParameter(PARAM_VALID_OSCHEDID)));
		props.setProperty(PARAM_OVERLAPSCEDID, AiradminUtils.getNotNullString(request.getParameter(PARAM_OVERLAPSCEDID)));

		// for reprotect
		props.setProperty(PARAM_REPROTECTALERT, AiradminUtils.getNotNullString(request.getParameter(PARAM_REPROTECTALERT)));
		props.setProperty(PARAM_REPROTECTEMAIL, AiradminUtils.getNotNullString(request.getParameter(PARAM_REPROTECTEMAIL)));
		props.setProperty(PARAM_RESCHEDULEALERT, AiradminUtils.getNotNullString(request.getParameter(PARAM_RESCHEDULEALERT)));
		props.setProperty(PARAM_RESCHEDULEEMAIL, AiradminUtils.getNotNullString(request.getParameter(PARAM_RESCHEDULEEMAIL)));
		props.setProperty(PARAM_RESCHEDULEALL, AiradminUtils.getNotNullString(request.getParameter(PARAM_RESCHEDULEALL)));

		props.setProperty(PARAM_EMAILTO, AiradminUtils.getNotNullString(request.getParameter(PARAM_EMAILTO)));
		props.setProperty(PARAM_EMAILSUBJECT, AiradminUtils.getNotNullString(request.getParameter(PARAM_EMAILSUBJECT)));
		props.setProperty(PARAM_EMAILCONTENT, AiradminUtils.getNotNullString(request.getParameter(PARAM_EMAILCONTENT)));
		props.setProperty(PARAM_REPROTECT, AiradminUtils.getNotNullString(request.getParameter(PARAM_REPROTECT)));
		props.setProperty(PARAM_FLIGHT_REMARKS, AiradminUtils.getNotNullString(request.getParameter(PARAM_FLIGHT_REMARKS)));

		props.setProperty(PARAM_MEAL_TEMPLATE, AiradminUtils.getNotNullString(request.getParameter(PARAM_MEAL_TEMPLATE)));
		props.setProperty(PARAM_SEAT_CHARGE_TEMPLATE,
				AiradminUtils.getNotNullString(request.getParameter(PARAM_SEAT_CHARGE_TEMPLATE)));
		// CodeShare
		props.setProperty(PARAM_CODESHARE, AiradminUtils.getNotNullString(request.getParameter(PARAM_CODESHARE)));
		
		props.setProperty(PARAM_CSOCCARRIER, AiradminUtils.getNotNullString(request.getParameter(PARAM_CSOCCARRIER)));
		props.setProperty(PARAM_CSOCFLIGHT, AiradminUtils.getNotNullString(request.getParameter(PARAM_CSOCFLIGHT)));


		return props;

	}

	/**
	 * Sets the Display Data to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static void setDisplayData(HttpServletRequest request) throws ModuleException {

		setClientErrors(request);
		setOnlineActiveAirportHtml(request);
		setOperationTypeHtml(request);
		setAircraftModelData(request);
		setStatusHtml(request);
		setFlightTypesHtml(request);
		setGDSListHtml(request);
		setBuildStatusHtml(request);
		setTimeInMode(request);
		setAircraftModelDetails(request);
		// setModelRouteInfoDetails(request);
		setStopOverTime(request);
		setOnlineAirportHtml(request);
		setUserCarriers(request);
		setModel(request);
		setScheduleRowHtml(request);
		setAvailabilityOfMultipleFltTypes(request);
		setMealTemplateCodes(request);
		setSeatChargeTemplateCodesStr(request);
		setMaxFlightNumberLength(request);
		setCodeShareListHtml(request);
		isCodeShareEnable(request);
		setGdsCarrierCodeArray(request);
		setBuildDefaultInvTemplate(request);
	}

	/**
	 * sets the Online Active Airport list
	 * 
	 * @param request
	 */
	private static void setOnlineActiveAirportHtml(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createOnlineActiveAirportListWOTag();
		request.setAttribute(WebConstants.SES_HTML_ONLINE_ACTIVE_AIRPORTCODE_LIST_DATA, strHtml);
	}

	/**
	 * sets Online Airport list
	 * 
	 * @param request
	 */
	private static void setOnlineAirportHtml(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createOnlineAirportListWOTag();
		request.setAttribute(WebConstants.SES_HTML_ONLINE_AIRPORTCODE_LIST_DATA, strHtml);
	}

	/**
	 * sets the Operation Type list
	 * 
	 * @param request
	 */
	private static void setOperationTypeHtml(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createOperationTypeForSchedule();
		request.setAttribute(WebConstants.SES_HTML_OPERATIONTYPE_LIST_DATA, strHtml);
	}

	/**
	 * sets the Status list
	 * 
	 * @param request
	 */
	private static void setStatusHtml(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createScheduleStatus();
		request.setAttribute(WebConstants.SES_HTML_STATUS_LIST_DATA, strHtml);
	}

	/**
	 * Sets flight type list
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setFlightTypesHtml(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createFlightTypeList(true);
		request.setAttribute(WebConstants.REQ_HTML_FLIGHT_TYPES, strHtml);
	}

	/**
	 * sets the GDS list
	 * 
	 * @param request
	 */
	private static void setGDSListHtml(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createGDSList();
		request.setAttribute(WebConstants.SES_HTML_GDS_LIST_DATA, strHtml);
	}
	
	/**
	 * sets the CodeShare list
	 * 
	 * @param request
	 */
	private static void setCodeShareListHtml(HttpServletRequest request) throws ModuleException {
		
		StringBuffer sb = new StringBuffer();
		Map<String, GDSStatusTO> gdsStatusMap = globalConfig.getActiveGdsMap();
		for (GDSStatusTO gdsStatusTO : gdsStatusMap.values()) {
			if (gdsStatusTO.isCodeShareCarrier()) {
				sb.append("<option value='" + gdsStatusTO.getCarrierCode() + "'>" + gdsStatusTO.getCarrierCode() + " </option>");
			}
		}
		
		String strHtml = sb.toString();
		request.setAttribute(WebConstants.SES_HTML_CodeShare_LIST_DATA , strHtml);
	}

	/**
	 * sets the Build Status list
	 * 
	 * @param request
	 */
	private static void setBuildStatusHtml(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createBuildStatus();
		request.setAttribute(WebConstants.SES_HTML_BUILDSTATUS_LIST_DATA, strHtml);
	}

	/**
	 * sets the Aircraft Model list
	 * 
	 * @param request
	 */
	private static void setAircraftModelData(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createActiveAircraftModelList_SG();
		request.setAttribute(WebConstants.SES_HTML_AIRCRAFTMODEL_LIST_DATA, strHtml);
	}

	/**
	 * sets the client errors for Schedules
	 * 
	 * @param request
	 */
	public static void setClientErrors(HttpServletRequest request) throws ModuleException {
		String strClientErrors = ScheduleHTMLGenerator.getClientErrors(request);

		if (strClientErrors == null) {
			strClientErrors = "";
		}
		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
	}

	/**
	 * sets the html for user list into request a request variable
	 * 
	 * @param request
	 */
	protected static void setScheduleRowHtml(HttpServletRequest request) throws ModuleException {

		ScheduleHTMLGenerator htmlGen = new ScheduleHTMLGenerator();
		String strHtml = htmlGen.getScheduleRowHtml(request);
		request.setAttribute(WebConstants.REQ_HTML_FLIGHTSCHEDULE_DATA, strHtml);
	}

	private static void setAvailabilityOfMultipleFltTypes(HttpServletRequest request) {
		String fltAvailablity = FlightUtils.checkForMultipleFltTypesAvailability();
		request.setAttribute(WebConstants.REQ_MULTIPLE_FLIGHT_TYPE_AVAILABILITY, fltAvailablity);
	}

	/**
	 * sets default time mode [ZULU / LOCAL]
	 * 
	 * @param request
	 */
	private static void setTimeInMode(HttpServletRequest request) throws ModuleException {
		if (AppSysParamsUtil.isDefaultTimeZulu()) {
			request.setAttribute(WebConstants.REQ_HTML_TIMEIN, "var isTimeInZulu=true;");
		} else {
			request.setAttribute(WebConstants.REQ_HTML_TIMEIN, "var isTimeInZulu=false;");
		}
	}

	/**
	 * Sets the Aircraft Models to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setAircraftModelDetails(HttpServletRequest request) throws ModuleException {

		String strHtml = JavascriptGenerator.createAircraftModelDtls();
		request.setAttribute(WebConstants.SES_HTML_AIRCRAFTMODEL_DETAILS, strHtml);
	}

	/**
	 * Sets the Stop over times of the Airports to the request
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setStopOverTime(HttpServletRequest request) throws ModuleException {

		String strHtml = JavascriptGenerator.createMinStopOverTime();
		request.setAttribute(WebConstants.SES_HTML_MINSTOPOVERTIME_DATA, strHtml);

	}

	/**
	 * Sets the Model to the request Schedule from Front end
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setModel(HttpServletRequest request) throws ModuleException {
		ScheduleHTMLGenerator htmlGen = new ScheduleHTMLGenerator();
		String strHtml = htmlGen.createModel(request);
		request.setAttribute(WebConstants.SES_HTML_MODEL_DATA, strHtml);

	}

	private static void setUserCarriers(HttpServletRequest request) throws ModuleException {
		User user = (User) request.getSession().getAttribute(WebConstants.SES_CURR_USER);
		StringBuilder usb = new StringBuilder();

		String strDefaultCarrierCode = "";
		strDefaultCarrierCode = user.getDefaultCarrierCode();
		if (!(strDefaultCarrierCode != null && !strDefaultCarrierCode.trim().equals(""))) {
			strDefaultCarrierCode = globalConfig.getBizParam(SystemParamKeys.DEFAULT_CARRIER_CODE);
		}
		request.setAttribute(WebConstants.REQ_HTML_DEFAULT_CRRIERCODE, strDefaultCarrierCode);

		Set<String> stCarriers = user.getCarriers();
		if (stCarriers != null) {
			int i = 0;
			Iterator<String> stIter = stCarriers.iterator();
			while (stIter.hasNext()) {
				usb.append(" arrCarriers[" + i + "] = '" + stIter.next() + "';");
				i++;
			}
		}
		request.setAttribute(WebConstants.REQ_HTML_USER_CRRIERCODE, usb.toString());
	}

	private static ArrayList<String> addToList(ArrayList<String> list, String str) {
		if (str != null && !str.trim().equals(""))
			list.add(str);
		return list;
	}

	private static boolean getBooleanValue(String str) {
		return (str == null || !str.equals(WebConstants.APP_BOOLEAN_TRUE)) ? false : true;
	}

	private static void setMealTemplateCodes(HttpServletRequest request) throws ModuleException {
		String strTmplCodes = SelectListGenerator.createMealTemplateList();
		request.setAttribute(WebConstants.MEAL_TEMPL_CODE, strTmplCodes);
	}

	private static void setSeatChargeTemplateCodes(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createSeatMapChargeTemplateList();
		request.setAttribute(WebConstants.SEAT_TEMPL_CODE, strHtml);
	}

	private static void setSeatChargeTemplateCodesStr(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createSeatMapChargeTemplateListStr();
		request.setAttribute(WebConstants.SEAT_TEMPL_CODE, strHtml);
	}

	private static void setMaxFlightNumberLength(HttpServletRequest request) throws ModuleException {
		request.setAttribute(WebConstants.REQ_HTML_FLIGHTNUMBER_LENGTH, AppSysParamsUtil.getMaximumFlightNumberLength());
	}
	
	private static void isCodeShareEnable(HttpServletRequest request)
			throws ModuleException {
		request.setAttribute(WebConstants.REQ_CS_ENABLE, AppSysParamsUtil.isCodeShareEnable());
	}
	
	private static void setGdsCarrierCodeArray(HttpServletRequest request) throws ModuleException {
		String gdsCarrierCodeArr = "var gdsCarrierCodeArray = new Array();";
		Map<String, GDSStatusTO> gdsStatusMap = globalConfig.getActiveGdsMap();
		if (gdsStatusMap != null && !gdsStatusMap.isEmpty()) {
			int i = 0;
			for (GDSStatusTO gdsStatusTO : gdsStatusMap.values()) {
				if (gdsStatusTO != null) {
					gdsCarrierCodeArr += "gdsCarrierCodeArray[" + i + "] = '" + gdsStatusTO.getGdsId() + "|"
							+ gdsStatusTO.getCarrierCode() + "';";
				}
				i++;
			}

		}
		request.setAttribute(WebConstants.REQ_GDS_CARRIER_CODE_ARRAY, gdsCarrierCodeArr);
	}

	private static void setBuildDefaultInvTemplate(HttpServletRequest request) {
		request.setAttribute(WebConstants.REQ_HTML_BUILD_DEFAULT_INV_TEMPLATE,
				AppSysParamsUtil.isEnableDefaultInvCreationWithBuildFlights());
	}

	private static void viewScheduleMsgReport(HttpServletRequest request, HttpServletResponse response) {
		try {
			Properties schedProp = getProperties(request);
			String scheduleId = schedProp.getProperty(PARAM_SCHEDULEID);
			String flightNo = schedProp.getProperty(PARAM_IN_FLIGHTNUMBER);

			Date fromDate = CalendarUtil.getStartTimeOfDate(CalendarUtil.getParsedTime(request.getParameter("fromDate"),
					"dd/MM/yyyy"));
			Date toDate = CalendarUtil.getEndTimeOfDate(CalendarUtil.getParsedTime(request.getParameter("toDate"), "dd/MM/yyyy"));
			String strFromDate = CalendarUtil.getFormattedDate(fromDate, "dd/MM/yyyy HH:mm:ss");
			String strToDate = CalendarUtil.getFormattedDate(toDate, "dd/MM/yyyy HH:mm:ss");

			ResultSet resultSet = ModuleServiceLocator.getDataExtractionBD().getScheduleMessageDetailsReport(scheduleId,
					strFromDate, strToDate);

			String reportTemplate = "ScheduleMsgHistoryReport.jasper";
			String strLogo = AppSysParamsUtil.getReportLogo(true);
			String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
			String reportsRootDir = "../../images/" + strLogo;

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("REPORTTYPE", WebConstants.REPORT_HTML);
			parameters.put("IMG", reportsRootDir);
			parameters.put("FLIGHT_NO", flightNo);
			parameters.put("SCHEDULE_ID", scheduleId);
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);

			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));
			ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null, null,
					response);
		} catch (Exception e) {
			log.error("viewFlightPublishedMessagesAudit :: ", e);
		}
	}

	private static void viewFlightPublishedMessagesAudit(HttpServletRequest request, HttpServletResponse response) {
		try {
			Properties properties = FlightUtils.getProperties(request);
			String scheduleId = properties.getProperty(PARAM_SCHEDULEID);
			String flightNo = properties.getProperty(PARAM_IN_FLIGHTNUMBER);

			Date fromDate = CalendarUtil.getStartTimeOfDate(CalendarUtil.getParsedTime(request.getParameter("fromDate"),
					"dd/MM/yyyy"));
			Date toDate = CalendarUtil.getEndTimeOfDate(CalendarUtil.getParsedTime(request.getParameter("toDate"), "dd/MM/yyyy"));
			String strFromDate = CalendarUtil.getFormattedDate(fromDate, "dd/MM/yyyy HH:mm:ss");
			String strToDate = CalendarUtil.getFormattedDate(toDate, "dd/MM/yyyy HH:mm:ss");

			ResultSet resultSet = ModuleServiceLocator.getDataExtractionBD().getPublishedMessageDetailsReport(scheduleId,
					strFromDate, strToDate);

			String reportTemplate = "PublishedMsgHistoryReport.jasper";
			String strLogo = AppSysParamsUtil.getReportLogo(true);
			String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
			String reportsRootDir = "../../images/" + strLogo;

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("REPORTTYPE", WebConstants.REPORT_HTML);
			parameters.put("IMG", reportsRootDir);
			parameters.put("FLIGHT_NO", flightNo);
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);

			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));
			ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null, null,
					response);
		} catch (Exception e) {
			log.error("viewFlightPublishedMessagesAudit :: ", e);
		}
	}
}
