/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */
package com.isa.thinair.airadmin.core.web.handler.flightsched;

/**
 * @author Srikantha
 *
 */
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airinventory.api.dto.TransferSeatDTO;
import com.isa.thinair.airinventory.api.dto.TransferSeatList;
import com.isa.thinair.airreservation.api.utils.AllocateEnum;
import com.isa.thinair.airschedules.api.dto.FlightAlertDTO;
import com.isa.thinair.commons.api.constants.DayOfWeek;
import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.platform.api.ServiceResponce;

public class ReprotectRollForwardRequestHandler extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(ReprotectRollForwardRequestHandler.class);
	private static AiradminConfig airadminConfig = new AiradminConfig();

	private static final String PARAM_MODE = "hdnMode";

	private static final String PARAM_LOGICAL_CABIN_CLASS_CODE = "hdnLogicalCabinClassCode";
	private static final String PARAM_TARGET_FLIGHTID = "hdnTargetFlightId";
	private static final String PARAM_TARGET_SEGMENT_CODE = "hdnTargetSegmentCode";
	private static final String PARAM_TARGET_FLIGHT_SEGID = "hdnTargetFlightSegmentId";

	private static final String PARAM_SOURCE_FLIGHT_DISTRIBUTIONS = "hdnSourceFlightDistributions";

	private static final String PARAM_ALLOC_MODE = "optAllocMode";
	private static final String PARAM_OVER_ALLOC_SEATS = "txtOverAllocSeats";

	private static final String PARAM_FROMDATE = "txtFromDate";
	private static final String PARAM_TILLDATE = "txtTillDate";

	private static final String PARAM_SUNDAY = "chkSunday";
	private static final String PARAM_MONDAY = "chkMonday";
	private static final String PARAM_TUESDAY = "chkTuesday";
	private static final String PARAM_WEDNESDAY = "chkWednesday";
	private static final String PARAM_THURSDAY = "chkThursday";
	private static final String PARAM_FRIDAY = "chkFriday";
	private static final String PARAM_SATURDAY = "chkSaturday";
	private static final String PARAM_CLOSED_FLIGHT = "chkClosedFlights";
	private static final String PARAM_ALERT = "hdnlAlert";
	private static final String PARAM_SMSALERT = "hdnSMSAlert";
	private static final String PARAM_EMAILALERT = "hdnEmailAlert";
	private static final String PARAM_SMSCNF = "hdnSmsCnf";
	private static final String PARAM_CMSONH = "hdnSmsOnH";
	private static final String PARAM_EMAIL_ORIGIN_AGENT = "hdnEmailOriginAgent";
	private static final String PARAM_EMAIL_OWNER_AGENT = "hdnEmailOwnerAgent";

	/**
	 * Main Execute Method for Roll Forward Action
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {
		String strHdnMode = request.getParameter(PARAM_MODE);
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;
		try {
			if ((strHdnMode != null) && (strHdnMode.equals(WebConstants.ACTION_ROLLFORWARD))) {
				rollForwardReprotect(request);
			}
		} catch (Exception exception) {
			log.error("Exception in ReprotectRollForwardRequestHandler.execute", exception);
			if (exception instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
			} else {
				saveMessage(request, exception.getMessage(), WebConstants.MSG_ERROR);
			}
		}

		return forward;
	}

	/**
	 * 
	 * @param str
	 * @return
	 */
	private static boolean getBooleanValue(String str) {
		return (str == null || !str.equals("true")) ? false : true;
	}

	/**
	 * Roll Forward a One flights Reprotect to the Whole Flight Schedule
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	private static void rollForwardReprotect(HttpServletRequest request) throws Exception {
		String strLogicalCCCode = request.getParameter(PARAM_LOGICAL_CABIN_CLASS_CODE);
		// preparing the target flight details
		String strTargetFlightId = request.getParameter(PARAM_TARGET_FLIGHTID);
		String strTargetSegCode = request.getParameter(PARAM_TARGET_SEGMENT_CODE);
		String strTargetFlightSegId = request.getParameter(PARAM_TARGET_FLIGHT_SEGID);
		// preparing the source flight details
		String hdnSourceFlightDistributions = request.getParameter(PARAM_SOURCE_FLIGHT_DISTRIBUTIONS);
		String[] arrSourceFlightDistributions = hdnSourceFlightDistributions.split(":");

		boolean blnEmailAlert = getBooleanValue(request.getParameter(PARAM_EMAILALERT));
		boolean blnSmsAlert = getBooleanValue(request.getParameter(PARAM_SMSALERT));
		boolean blnSmsCnf = getBooleanValue(request.getParameter(PARAM_SMSCNF));
		boolean blnSmsOnH = getBooleanValue(request.getParameter(PARAM_CMSONH));
		boolean blnEmailAlertOriginAgent = getBooleanValue(request.getParameter(PARAM_EMAIL_ORIGIN_AGENT));
		boolean blnEmailAlertOwnerAgent = getBooleanValue(request.getParameter(PARAM_EMAIL_OWNER_AGENT));
		
		long startTime= System.currentTimeMillis();

		try {
			// preparing the TransferSeatDTO properties
			TransferSeatDTO transferSeatDTO = new TransferSeatDTO();
			// To avoid transferring standby booking in flight re-protect
			transferSeatDTO.setSkipStandbyBookings(true);
			transferSeatDTO.setLogicalCCCode(strLogicalCCCode);
			transferSeatDTO.setTargetFlightId(Integer.parseInt(strTargetFlightId));
			transferSeatDTO.setTargetFlightSegId(Integer.parseInt(strTargetFlightSegId));
			transferSeatDTO.setTargetSegCode(strTargetSegCode);

			Collection<TransferSeatList> colSourceFlightTransferSeatList = new ArrayList<TransferSeatList>();
			boolean isTransferAll = false;
			for (int count = 0; count < arrSourceFlightDistributions.length; count++) {
				String strSourceFlightDistribution = arrSourceFlightDistributions[count];
				String[] arrSourceFlightDistParameters = strSourceFlightDistribution.split(",");

				String strSourceFlightId = arrSourceFlightDistParameters[0];
				String strSourceSegmentCode = arrSourceFlightDistParameters[1];
				String strSourceFlightSegmentId = arrSourceFlightDistParameters[2];
				int transferSeatCount = Integer.parseInt(arrSourceFlightDistParameters[3]);
				isTransferAll = false;
				if ((arrSourceFlightDistParameters[4] != null)
						&& (arrSourceFlightDistParameters[4].trim().equalsIgnoreCase("true"))) {
					isTransferAll = true;
				}

				TransferSeatList transferSeatList = new TransferSeatList();
				transferSeatList.setSourceFlightId(Integer.parseInt(strSourceFlightId));
				transferSeatList.setSourceSegmentCode(strSourceSegmentCode);
				transferSeatList.setSourceSegmentId(Integer.parseInt(strSourceFlightSegmentId));
				transferSeatList.setTransferSeatCount(transferSeatCount);
				transferSeatList.setTransferAll(isTransferAll);

				colSourceFlightTransferSeatList.add(transferSeatList);
			}

			transferSeatDTO.setSourceFltSegSeatDists(colSourceFlightTransferSeatList);

			FlightAlertDTO fltAltDTO = new FlightAlertDTO();

			fltAltDTO.setSendEmailsForFlightAlterations(blnEmailAlert);
			fltAltDTO.setSendSMSForFlightAlterations(blnSmsAlert);
			fltAltDTO.setSendSMSForConfirmedBookings(blnSmsCnf);
			fltAltDTO.setSendSMSForOnHoldBookings(blnSmsOnH);
			fltAltDTO.setSendEmailToOriginAgent(blnEmailAlertOriginAgent);
			fltAltDTO.setSendEmailToOwnerAgent(blnEmailAlertOwnerAgent);

			// Setting the include frequency
			Frequency frequency = null;
			Collection<DayOfWeek> daysCol = new ArrayList<DayOfWeek>();

			if ((request.getParameter(PARAM_SUNDAY) != null) && (request.getParameter(PARAM_SUNDAY).equals("on"))) {
				daysCol.add(DayOfWeek.SUNDAY);
			}
			if ((request.getParameter(PARAM_MONDAY) != null) && (request.getParameter(PARAM_MONDAY).equals("on"))) {
				daysCol.add(DayOfWeek.MONDAY);
			}
			if ((request.getParameter(PARAM_TUESDAY) != null) && (request.getParameter(PARAM_TUESDAY).equals("on"))) {
				daysCol.add(DayOfWeek.TUESDAY);
			}
			if ((request.getParameter(PARAM_WEDNESDAY) != null) && (request.getParameter(PARAM_WEDNESDAY).equals("on"))) {
				daysCol.add(DayOfWeek.WEDNESDAY);
			}
			if ((request.getParameter(PARAM_THURSDAY) != null) && (request.getParameter(PARAM_THURSDAY).equals("on"))) {
				daysCol.add(DayOfWeek.THURSDAY);
			}
			if ((request.getParameter(PARAM_FRIDAY) != null) && (request.getParameter(PARAM_FRIDAY).equals("on"))) {
				daysCol.add(DayOfWeek.FRIDAY);
			}
			if ((request.getParameter(PARAM_SATURDAY) != null) && (request.getParameter(PARAM_SATURDAY).equals("on"))) {
				daysCol.add(DayOfWeek.SATURDAY);
			}
			frequency = CalendarUtil.getFrequencyFromDays(daysCol);

			// Setting the allocation mode from the AllocateEnum
			AllocateEnum allocateEnum = null;
			String strAllocateMode = request.getParameter(PARAM_ALLOC_MODE);
			if (strAllocateMode.equals("OVER_ALLOC")) {
				allocateEnum = AllocateEnum.OVER_ALL;
			}

			if (strAllocateMode.equals("AUTO_FIT")) {
				allocateEnum = AllocateEnum.AUTO_FIT;
			}

			// Setting the over allocate seats
			int overAllocSeats = 0;
			if ((request.getParameter(PARAM_OVER_ALLOC_SEATS) != null)
					&& (!request.getParameter(PARAM_OVER_ALLOC_SEATS).equals(""))) {
				overAllocSeats = Integer.parseInt(request.getParameter(PARAM_OVER_ALLOC_SEATS));
			}

			// Setting from date
			String strFromDate = request.getParameter(PARAM_FROMDATE);
			SimpleDateFormat dtfmt = new SimpleDateFormat("dd/MM/yyyy");
			Date fromDate = null;
			if (strFromDate != null) {
				try {
					fromDate = dtfmt.parse(strFromDate);
				} catch (ParseException ex) {
					AiradminUtils.error(log, ex.getMessage());
				}
			}

			// Setting till date
			String strTillDate = request.getParameter(PARAM_TILLDATE);
			Date tillDate = null;
			if (strTillDate != null) {
				try {
					tillDate = dtfmt.parse(strTillDate);
				} catch (ParseException ex) {
					AiradminUtils.error(log, ex.getMessage());
				}
			}

			boolean isAlert = Boolean.valueOf(request.getParameter(PARAM_ALERT)).booleanValue();

			boolean includeCLSflights = false;
			if ((request.getParameter(PARAM_CLOSED_FLIGHT) != null) && (request.getParameter(PARAM_CLOSED_FLIGHT).equals("on"))) {
				includeCLSflights = true;
			}

			if (log.isInfoEnabled()) {
				log.info("fromDate          : " + fromDate);
				log.info("tillDate          : " + tillDate);
				log.info("frequency         : " + frequency.getSummary());
				log.info("strAllocateMode   : " + strAllocateMode);
				log.info("overAllocSeats    : " + overAllocSeats);
				log.info("isAlert           : " + isAlert);
				log.info("includeCLSflights : " + includeCLSflights);
			}

			ServiceResponce rollforwardResponse = ModuleServiceLocator.getReservationBD().rollForwardReservations(
					transferSeatDTO, isAlert, allocateEnum, overAllocSeats, fromDate, tillDate, frequency, includeCLSflights,
					fltAltDTO);

			if (rollforwardResponse.isSuccess()) {
				request.setAttribute(WebConstants.POPUPMESSAGE, airadminConfig.getMessage(WebConstants.KEY_SCHEDULE_SAVE_SUCCESS));
				saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_ROLLFORWARD_SUCCESS), WebConstants.MSG_SUCCESS);
			} else {
				// if service reponce is unsuccessfull the entire operation will get rollbacked.

				saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_ROLLFORWARD_NOT_SUCCESS), WebConstants.MSG_ERROR);
			}
		} catch (Exception exception) {
			AiradminUtils.error(log, exception.getMessage(), exception);
			if (exception instanceof RuntimeException) {
				throw exception;
			} else {
				saveMessage(request, exception.getMessage(), WebConstants.MSG_ERROR);
			}
		}finally{
			
			long endTime = System.currentTimeMillis();
			log.info("######## Time taken for the roll fowarding of Reprotection: (ms) " + (endTime - startTime));
		}
	}
}
