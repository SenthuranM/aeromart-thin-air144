package com.isa.thinair.airadmin.core.web.generator.master;

import java.util.Collection;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.gdsservices.api.util.TypeAConstants;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airmaster.api.model.Gds;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author srikantha
 * 
 */
public class GDSHTMLGenerator {

	private static Log log = LogFactory.getLog(GDSHTMLGenerator.class);

	private static String clientErrors;

	/**
	 * Gets the GDS Grid Data Array for the Record No
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the GDS Grid Array
	 * @throws ModuleException
	 *             the ModuleException
	 */
	@SuppressWarnings("unchecked")
	public final String getGDSRowHtml(HttpServletRequest request) throws ModuleException {

		log.debug("inside GDSHTMLGenerator.getGDSRowHtml()");
		List<Gds> list = null;

		int recordNo = 0;
		if (request.getParameter("hdnRecNo") != null && !request.getParameter("hdnRecNo").equals("")) {
			recordNo = Integer.parseInt(request.getParameter("hdnRecNo")) - 1;
		}

		Page page = null;

		int totalRecords = 0;
		String strJavascriptTotalNoOfRecs = "";
		page = ModuleServiceLocator.getGdsServiceBD().getGDSs(recordNo, 20);
		totalRecords = page.getTotalNoOfRecords();
		strJavascriptTotalNoOfRecs = "var totalRecords = " + totalRecords + ";";
		request.setAttribute(WebConstants.REQ_TOTAL_RECORDS, strJavascriptTotalNoOfRecs);

		list = (List<Gds>) page.getPageData();
		log.debug("inside GDSHTMLGenerator.getGDSRowHtml SEARCH else condition()");
		return createGDSRowHTML(list);
	}

	/**
	 * Create the Grid row for GDSs from a Collection
	 * 
	 * @param territories
	 *            the Collection of GDSs
	 * @return String the Created Grid array for GDSs
	 */
	private String createGDSRowHTML(Collection<Gds> gdss) {
		List<Gds> list = (List<Gds>) gdss;
		Object[] listArr = list.toArray();
		StringBuffer sb = new StringBuffer("var arrData = new Array();");

		Gds gds = null;

		for (int i = 0; i < listArr.length; i++) {
			gds = (Gds) listArr[i];
			sb.append("arrData[" + i + "] = new Array();");
			sb.append("arrData[" + i + "][1] = '" + gds.getGdsCode() + "';");
			sb.append("arrData[" + i + "][2] = '" + gds.getDescription() + "';");

			if (gds.getRemarks() != null)
				sb.append("arrData[" + i + "][3] = '" + gds.getRemarks() + "';");
			else
				sb.append("arrData[" + i + "][3] = '';");

			sb.append("arrData[" + i + "][4] = '" + AiradminUtils.getModifiedStatus(gds.getStatus()) + "';");
			sb.append("arrData[" + i + "][5] = '" + gds.getSchedPublishMechanismCode() + "';");

			sb.append("arrData[" + i + "][6] = '" + gds.getVersion() + "';");
			sb.append("arrData[" + i + "][7] = '" + gds.getGdsId() + "';");
			sb.append("arrData[" + i + "][8] = '" + (gds.getAvsAvailThreshold() == null ? "" : gds.getAvsAvailThreshold()) + "';");
			sb.append("arrData[" + i + "][9] = '" + (gds.getAvsClosureThreshold() == null ? "" : gds.getAvsClosureThreshold())
					+ "';");
			sb.append("arrData[" + i + "][10] = '" + gds.getEnableNavs()+ "';");			
			sb.append("arrData[" + i + "][11] = '" + gds.getTypeAVersion() + "';");
			sb.append("arrData[" + i + "][12] = '" + gds.getTypeASenderId() + "';");
			sb.append("arrData[" + i + "][13] = '" + gds.getTypeASyntaxId() + "';");
			sb.append("arrData[" + i + "][14] = '" + gds.getTypeASyntaxVersionNumber() + "';");
			sb.append("arrData[" + i + "][15] = '" + TypeAConstants.TYPE_A_CONTROLLING_AGENCY + "';");
			sb.append("arrData[" + i + "][16] = '" + gds.getAirlineResModifiable() + "';");
			sb.append("arrData[" + i + "][17] = '" + gds.getSchedulingSystem() + "';");
			
		}
		return sb.toString();
	}

	/**
	 * Sets the Client Validations for GDS Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array Containing the Client Validations
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {
		if (clientErrors == null) {
			Properties moduleErrs = new Properties();

			moduleErrs.setProperty("um.gds.form.id.required", "gdsIdRqrd");
			moduleErrs.setProperty("um.gds.form.description.required", "gdsDescRqrd");
			moduleErrs.setProperty("um.gds.form.id.already.exist", "gdsIdExist");
			moduleErrs.setProperty("um.gds.form.id.pub.mech.required", "gdsPubMechRequired");

			clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}
		return clientErrors;
	}
}
