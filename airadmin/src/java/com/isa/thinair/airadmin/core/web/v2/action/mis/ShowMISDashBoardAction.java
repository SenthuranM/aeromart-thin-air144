package com.isa.thinair.airadmin.core.web.v2.action.mis;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.v2.dto.mis.MISProcessParams;
import com.isa.thinair.airadmin.core.web.v2.handler.mis.MISEmailRH;
import com.isa.thinair.airadmin.core.web.v2.util.CommonUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.reporting.api.criteria.MISReportsSearchCriteria;
import com.isa.thinair.reporting.api.model.SalesRevenueTO;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * 
 * @author lalanthi
 * 
 */

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Result(name = AdminStrutsConstants.AdminAction.SUCCESS, type = JSONResult.class, value = "")
public class ShowMISDashBoardAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(ShowMISDashBoardAction.class);
	private boolean success = true;
	private String messageTxt;
	private String returnData = "";
	private MISReportsSearchCriteria misSearchCriteria;
	private Map<String, String> stationsList;
	private Map<String, Integer> salesByChannelPaxCount;
	private String maxPassengerCount;
	private String totSalesValue;
	private Map<String, BigDecimal> channelWiseSales;
	private String baseCurrency;

	public String execute() throws Exception {
		String strForward = WebConstants.FORWARD_SUCCESS;
		MISProcessParams mpParams = new MISProcessParams(request);
		request.setAttribute("req_InitialParams", CommonUtil.convertToJSON(mpParams));

		try {
			misSearchCriteria = new MISReportsSearchCriteria();
			misSearchCriteria.setFromDate(request.getParameter("fromDate"));
			misSearchCriteria.setToDate(request.getParameter("toDate"));
			if (!"ALL".equalsIgnoreCase(request.getParameter("region"))) {
				misSearchCriteria.setRegion(request.getParameter("region"));
			}
			if (!"ALL".equalsIgnoreCase(request.getParameter("pos"))) {
				misSearchCriteria.setPos(request.getParameter("pos"));
			}
			returnData = prepareMISDashBoardData(request);
			salesByChannelPaxCount = getChannelWiseSalesDataForPassengers();
			channelWiseSales = getChannelWiseSalesData();
			baseCurrency = AppSysParamsUtil.getBaseCurrency();

		} catch (Exception e) {
			log.error("Generic exception caught.", e);
			success = false;
			messageTxt = e.getMessage();
		}
		return strForward;
	}

	/**
	 * Prepares all the data to be displayed in the DashBoard UI.
	 * 
	 * @param request
	 * @return
	 */
	private String prepareMISDashBoardData(HttpServletRequest request) {

		StringBuilder misDashBoardData = new StringBuilder();
		misDashBoardData.append(getYtdSalesData(request));
		misDashBoardData.append(getRevenueBreakDown(request));
		misDashBoardData.append(getGetRevenueBreakdownForPeriod(request));

		return misDashBoardData.toString();
	}

	/**
	 * Returns year to date sales data with the given target
	 * 
	 * @param request
	 * @return TODO : Fix target amount.
	 */
	private String getYtdSalesData(HttpServletRequest request) {

		StringBuilder strYtdSales = new StringBuilder();
		long misYtdBookings = ModuleServiceLocator.getMISDataProviderBD().getMISDashboardYtdBookings(misSearchCriteria);
		long misGoal = ModuleServiceLocator.getMISDataProviderBD().getMISDashboardGoal(misSearchCriteria);
		strYtdSales.append("jsBkgYTD = ({budget:" + misGoal + ", value:" + misYtdBookings + "});\n");

		return strYtdSales.toString();

	}

	/**
	 * Returns revenue breakdown (Fares, Charges,Taxes etc )
	 * 
	 * @param request
	 * @return
	 */
	private String getRevenueBreakDown(HttpServletRequest request) {

		StringBuilder strRevBreakdown = new StringBuilder();

		ArrayList<BigDecimal> collection = new ArrayList<BigDecimal>(ModuleServiceLocator.getMISDataProviderBD()
				.getMISDashRevenueBreakdown(misSearchCriteria));
		strRevBreakdown.append("jsRevenue = ({revenue:\"" + collection.get(0) + "\", ");
		strRevBreakdown.append("	 			fare:\"" + collection.get(1) + "\", ");
		strRevBreakdown.append("	 			tax:\"" + collection.get(2) + "\", ");
		strRevBreakdown.append("  			surcharges:\"" + collection.get(3) + "\"});\n");
		return strRevBreakdown.toString();
	}

	/**
	 * Returns the revenue breakdown monthly for a given period.
	 */
	private String getGetRevenueBreakdownForPeriod(HttpServletRequest request) {
		StringBuilder strBuilder = new StringBuilder();
		String fromDate = request.getParameter("fromDate");
		String toDate = request.getParameter("toDate");
		String[] params = new String[2];
		params[0] = fromDate;
		params[1] = toDate;
		Map<String, List<SalesRevenueTO>> revenueByType = new HashMap<String, List<SalesRevenueTO>>();
		revenueByType.put("Fares", new ArrayList<SalesRevenueTO>());
		revenueByType.put("Charges", new ArrayList<SalesRevenueTO>());
		Float fltHightestValue = new Float(0);
		String strMonths = "";
		String strYears = "";

		Collection<SalesRevenueTO> colRevenueByType = ModuleServiceLocator.getMISDataProviderBD().getMISDashYearlySales(
				misSearchCriteria);

		if (colRevenueByType != null && colRevenueByType.size() > 0) {

			int i = 0;
			for (SalesRevenueTO revTO : colRevenueByType) {

				if (revTO.getSourceOfRevenue().equalsIgnoreCase(SalesRevenueTO.REV_TYPE_FARE)) {
					revenueByType.get("Fares").add(revTO);
					// Builds the axis data only for fares as this is same for the charges
					String monthName = CommonUtil.getMonthNames().get(revTO.getMonth());
					strMonths += monthName + "|";
					if (strYears.indexOf(revTO.getYear()) > 0) {
						strYears += "|";
					} else {
						strYears += revTO.getYear() + "|";
					}
				} else {
					revenueByType.get("Charges").add(revTO);
				}

				if (Double.parseDouble(revTO.getAmount()) > fltHightestValue) {
					fltHightestValue = Float.valueOf(revTO.getAmount());
				}

				i++;
			}

			strYears = strYears.substring(0, strYears.lastIndexOf("|"));
			strMonths = strMonths.substring(0, strMonths.lastIndexOf("|"));
			fltHightestValue = fltHightestValue + 10;
			strBuilder.append("jsRevGraphData = ({months:\"" + strMonths + "\",");
			strBuilder.append("                 years:\"" + strYears + "\",");
			strBuilder.append("					highestValue:\"" + StringUtils.split(fltHightestValue.toString(), ".")[0] + "\",");
			strBuilder.append("fareValues :" + CommonUtil.convertToJSON(revenueByType.get("Fares")) + ",");
			strBuilder.append("chargeValues:" + CommonUtil.convertToJSON(revenueByType.get("Charges")) + "}); \n");
		} else {
			strBuilder.append("jsRevGraphData = {fareValues: {},chargeValues: {}}");
		}

		return strBuilder.toString();
	}

	/**
	 * Returns channel wise sales data for a given period.
	 * 
	 * @return
	 */
	private Map<String, BigDecimal> getChannelWiseSalesData() {
		BigDecimal totSales = AccelAeroCalculator.getDefaultBigDecimalZero();
		Map<String, BigDecimal> channelWiseSales = ModuleServiceLocator.getMISDataProviderBD().getMISDistRevenueByChannel(
				misSearchCriteria);
		for (String channel : channelWiseSales.keySet()) {
			totSales = AccelAeroCalculator.add(totSales, channelWiseSales.get(channel));
		}
		this.totSalesValue = AccelAeroCalculator.formatAsDecimal(totSales);
		return channelWiseSales;
	}

	private Map<String, Integer> getChannelWiseSalesDataForPassengers() {
		Map<String, Integer> channelWisePaxCount = ModuleServiceLocator.getMISDataProviderBD().getMISDashSalesByChannelPaxCount(
				misSearchCriteria);
		int maxCount = 0;
		for (String channel : channelWisePaxCount.keySet()) {
			maxCount += channelWisePaxCount.get(channel);
		}
		this.maxPassengerCount = String.valueOf(maxCount);
		return channelWisePaxCount;
	}

	/**
	 * Returns stations list for a given region.
	 */
	public String getStations() {
		String strForward = WebConstants.FORWARD_SUCCESS;
		String region = request.getParameter("region");
		String[] params = new String[1];
		params[0] = region;
		try {
			Collection<String[]> stations = SelectListGenerator.createStationsForRegion(params);
			stationsList = new TreeMap<String, String>();
			for (String[] stationArr : stations) {
				stationsList.put(stationArr[0], stationArr[0] + " - " + stationArr[1]);
			}
		} catch (ModuleException e) {
			log.error("Generic exception caught.", e);
			success = false;
			messageTxt = e.getMessage();
		}

		return strForward;
	}

	public String sendEmail() {
		String strForward = WebConstants.FORWARD_SUCCESS;
		String chartSrc = request.getParameter("chartData");
		String emailID = request.getParameter("emailID");
		String feedBackContents = request.getParameter("comments");
		String fromDate = request.getParameter("fromDate");
		String toDate = request.getParameter("toDate");
		String region = request.getParameter("region");
		String pos = request.getParameter("pos");
		String chart2Src = request.getParameter("chartPaxData");

		if (chartSrc != null && chartSrc.length() > 0) {
			chartSrc = "'" + chartSrc.trim().replaceAll("#", "&").replaceAll("\t", "") + "'";
		}
		if (chart2Src != null && chart2Src.length() > 0) {
			chart2Src = "'" + chart2Src.trim().replaceAll("#", "&").replaceAll("\t", "") + "'";
		}

		MISEmailRH
				.sendDashBoardSalesByChannelEmail(emailID, feedBackContents, chartSrc, fromDate, toDate, region, pos, chart2Src);
		return strForward;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public String getReturnData() {
		return returnData;
	}

	public void setReturnData(String returnData) {
		this.returnData = returnData;
	}

	public MISReportsSearchCriteria getMisSearchCriteria() {
		return misSearchCriteria;
	}

	public void setMisSearchCriteria(MISReportsSearchCriteria misSearchCriteria) {
		this.misSearchCriteria = misSearchCriteria;
	}

	public Map<String, String> getStationsList() {
		return stationsList;
	}

	public void setStationsList(Map<String, String> stationsList) {
		this.stationsList = stationsList;
	}

	public Map<String, Integer> getSalesByChannelPaxCount() {
		return salesByChannelPaxCount;
	}

	public void setSalesByChannelPaxCount(Map<String, Integer> salesByChannelPaxCount) {
		this.salesByChannelPaxCount = salesByChannelPaxCount;
	}

	public String getMaxPassengerCount() {
		return maxPassengerCount;
	}

	public void setMaxPassengerCount(String maxPassengerCount) {
		this.maxPassengerCount = maxPassengerCount;
	}

	public Map<String, BigDecimal> getChannelWiseSales() {
		return channelWiseSales;
	}

	public void setChannelWiseSales(Map<String, BigDecimal> channelWiseSales) {
		this.channelWiseSales = channelWiseSales;
	}

	public String getBaseCurrency() {
		return baseCurrency;
	}

	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}

	public String getTotSalesValue() {
		return totSalesValue;
	}

	public void setTotSalesValue(String totSalesValue) {
		this.totSalesValue = totSalesValue;
	}

}
