package com.isa.thinair.airadmin.core.web.v2.action.master;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airadmin.core.web.v2.handler.master.SSRChargesRH;
import com.isa.thinair.airadmin.core.web.v2.util.CommonUtil;
import com.isa.thinair.airmaster.api.dto.AirportServiceDTO;
import com.isa.thinair.airmaster.api.model.SSR;
import com.isa.thinair.airmaster.api.model.SSRCategory;
import com.isa.thinair.airpricing.api.criteria.SSRChargesSearchCriteria;
import com.isa.thinair.airpricing.api.model.SSRCharge;
import com.isa.thinair.airpricing.api.model.SSRChargesCOS;
import com.isa.thinair.airpricing.api.util.DelimeterConstants;
import com.isa.thinair.airpricing.api.util.SplitUtil;
import com.isa.thinair.commons.api.dto.LogicalCabinClassDTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.dto.SSRChargesDTO;
import com.isa.thinair.commons.api.exception.MessagesUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.Constants;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author M.Rikaz
 * 
 */

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ShowSSRChargesAction extends BaseRequestAwareAction {

	private static AiradminConfig airadminConfig = new AiradminConfig();

	private static Log log = LogFactory.getLog(ShowSSRChargesAction.class);

	private int page;

	private int total;

	private int records;

	private Object[] rows;

	private String succesMsg;

	private String msgType;

	private String description;

	private String status;

	private long version;

	private String createdBy;

	private Date createdDate;

	private String hdnMode;

	private Integer ssrId;

	private Integer ssrSubCatId;

	private Integer ssrCatId;

	private String ssrDesc;

	private int ssrInfoCatId;

	private String cos;

	private String srchChargeCode;
	private Integer chargeId;
	private String chargeCode;
	private String applyBy;
	private BigDecimal adultAmount;
	private BigDecimal childAmount;
	private BigDecimal infantAmount;
	private BigDecimal reservationAmount;
	private BigDecimal serviceCharge;
	private String selSsrCode;
	private String selCategory;
	private String selStatus;
	private String updatedSsrCodes;
	private String localCurrCode;

	public String deleteSSRCharge() {

		try {

			if (checkConstrainsOnDeletion(chargeId)) {
				SSRChargesRH.deleteSSRCharges(chargeId);
				this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_DELETE_SUCCESS);
				this.msgType = WebConstants.MSG_SUCCESS;
			} else {
				this.succesMsg = airadminConfig.getMessage("um.ssr.form.insufficient.priviledge");
				this.msgType = WebConstants.MSG_ERROR;
			}
		} catch (ModuleException e) {
			this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_DELETE_FAIL);
			if (e.getMessageString().equals(MessagesUtil.getMessage("module.constraint.childrecord"))) {
				this.succesMsg = airadminConfig.getMessage("um.airadmin.childrecord");
			}
			this.msgType = WebConstants.MSG_ERROR;
			if (log.isErrorEnabled()) {
				log.error(e);
			}
		} catch (Exception ex) {
			this.succesMsg = "System Error Please Contact Administrator";
			this.msgType = WebConstants.MSG_ERROR;
			if (log.isErrorEnabled()) {
				log.error(ex);
			}
		}

		return S2Constants.Result.SUCCESS;
	}

	public String execute() {

		if (this.hdnMode != null && ((hdnMode.equals(WebConstants.ACTION_ADD)) || (hdnMode.equals(WebConstants.ACTION_EDIT)))) {

			try {

				SSRChargesDTO ssrChargesDTO = new SSRChargesDTO();
				ssrChargesDTO.setChargeId(chargeId);
				ssrChargesDTO.setSsrChargeCode(chargeCode.toUpperCase());
				ssrChargesDTO.setDescription(description);
				ssrChargesDTO.setSsrId(ssrId);

				if (SSRCategory.ID_INFLIGHT_SERVICE.intValue() == ssrInfoCatId) {
					ssrChargesDTO.setLocalCurrAdultAmount(serviceCharge);
					ssrChargesDTO.setLocalCurrChildAmount(new BigDecimal(0));
					ssrChargesDTO.setLocalCurrInfantAmount(new BigDecimal(0));
					ssrChargesDTO.setLocalCurrReservationAmount(new BigDecimal(0));

					ssrChargesDTO.setApplicablityType(AirportServiceDTO.APPLICABILITY_TYPE_PAX);
				} else if (SSRCategory.ID_AIRPORT_SERVICE.intValue() == ssrInfoCatId
						|| SSRCategory.ID_AIRPORT_TRANSFER.intValue() == ssrInfoCatId) {
					ssrChargesDTO.setApplicablityType(applyBy);
					if (applyBy != null && applyBy.equals(AirportServiceDTO.APPLICABILITY_TYPE_PAX)) {
						ssrChargesDTO.setLocalCurrAdultAmount(adultAmount);
						ssrChargesDTO.setLocalCurrChildAmount(childAmount);
						ssrChargesDTO.setLocalCurrInfantAmount(infantAmount);
						ssrChargesDTO.setLocalCurrReservationAmount(new BigDecimal(0));
					} else {
						ssrChargesDTO.setLocalCurrAdultAmount(new BigDecimal(0));
						ssrChargesDTO.setLocalCurrChildAmount(new BigDecimal(0));
						ssrChargesDTO.setLocalCurrInfantAmount(new BigDecimal(0));
						ssrChargesDTO.setLocalCurrReservationAmount(reservationAmount);
					}
				}

				ssrChargesDTO.setVersion(version);
				ArrayList<String> ccList = new ArrayList<String>();
				ArrayList<String> lccList = new ArrayList<String>();
				String[] cosList = cos.split(Constants.CARET_SEPERATOR);
				int i = 0;
				while (i < cosList.length) {
					String[] splittedCabinClassAndLogicalCabinClass = SplitUtil
							.getSplittedCabinClassAndLogicalCabinClass(cosList[i]);
					if (splittedCabinClassAndLogicalCabinClass[0] != null) {
						ccList.add(splittedCabinClassAndLogicalCabinClass[0]);
					} else {
						lccList.add(splittedCabinClassAndLogicalCabinClass[1]);
					}
					i++;
				}
				// always ssr charges are active
				ssrChargesDTO.setStatus("ACT");
				ssrChargesDTO.setChargeLocalCurrencyCode(localCurrCode);
				if (hdnMode.equals(WebConstants.ACTION_ADD)
						&& SSRChargesRH.chargeAlreadyDefined(ssrId, chargeCode, -1, ccList, lccList)) {
					this.succesMsg = airadminConfig.getMessage("um.ssrcharge.form.ssr.or.charge.code.exit");
					this.msgType = WebConstants.MSG_ERROR;
				} else if (hdnMode.equals(WebConstants.ACTION_EDIT)
						&& SSRChargesRH.chargeAlreadyDefined(ssrId, null, chargeId, ccList, lccList)) {
					this.succesMsg = airadminConfig.getMessage("um.ssrcharge.form.ssr.active.charge.exit");
					this.msgType = WebConstants.MSG_ERROR;
				} else if (isViolateSSRCategoryLevelRestrictions(ssrInfoCatId)) {
					this.succesMsg = airadminConfig.getMessage("um.ssr.form.insufficient.priviledge");
					this.msgType = WebConstants.MSG_ERROR;
				} else {
					SSRChargesRH.saveSSRCharges(ssrChargesDTO, cos);
					this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS);
					this.msgType = WebConstants.MSG_SUCCESS;
				}

			} catch (ModuleException e) {
				this.succesMsg = e.getMessageString();
				if (e.getMessageString().equals(MessagesUtil.getMessage("module.duplicate.key"))) {
					this.succesMsg = airadminConfig.getMessage("um.ssrcharge.form.id.already.exist");
				} else if (e.getMessageString().equals("module.internal.error")) {
					this.succesMsg = airadminConfig.getMessage("um.ssrcharge.form.effecfrm.daterange.invalid");
				}
				this.msgType = WebConstants.MSG_ERROR;
				if (log.isErrorEnabled()) {
					log.error(e);
				}
			} catch (Exception ex) {
				this.succesMsg = "System Error Please Contact Administrator";
				this.msgType = WebConstants.MSG_ERROR;
				if (log.isErrorEnabled()) {
					log.error(ex);
				}
			}
		}
		return S2Constants.Result.SUCCESS;
	}

	public String searchSSRCharges() {
		try {

			SSRChargesSearchCriteria criteria = new SSRChargesSearchCriteria();

			if (this.srchChargeCode != null && !this.srchChargeCode.trim().equals("")) {
				criteria.setChargeCode(this.srchChargeCode);
			}

			if (this.selSsrCode != null && !this.selSsrCode.trim().equals("")) {
				criteria.setSsrCode(this.selSsrCode);
			}

			// if (this.selCategory != null && !this.selCategory.trim().equals(""))
			// criteria.setCategory(this.selCategory);
			//
			// if (this.selStatus != null && !this.selStatus.trim().equals(""))
			// criteria.setStatus(this.selStatus);

			/*
			 * retreive the adminVisible SSR and get charges only of those
			 */
			Collection<String> ssrIdArray = SelectListGenerator.getAdminVisibleSSRId();
			criteria.setAdminVisibleSSRId(ssrIdArray);

			Page<SSRCharge> pgSSRCharges = SSRChargesRH.searchSSRCharges(this.page, criteria);

			Collection<SSR> colSSRInfo = ModuleServiceLocator.getSsrServiceBD().getSSRs();

			this.records = pgSSRCharges.getTotalNoOfRecords();
			this.total = pgSSRCharges.getTotalNoOfRecords() / 20;
			int mod = pgSSRCharges.getTotalNoOfRecords() % 20;
			if (mod > 0) {
				this.total = this.total + 1;
			}

			if (this.page < 0) {
				this.page = 1;
			}

			Collection<SSRCharge> colSSRCharges = pgSSRCharges.getPageData();

			if (colSSRCharges != null) {
				Object[] dataRow = new Object[colSSRCharges.size()];
				int index = 1;
				Iterator<SSRCharge> iter = colSSRCharges.iterator();

				while (iter.hasNext()) {

					Map<String, Object> counmap = new HashMap<String, Object>();
					SSRCharge ssrChgObj = iter.next();

					SSRChargesDTO jSonSSRChg = new SSRChargesDTO();

					jSonSSRChg.setChargeId(ssrChgObj.getChargeId());
					jSonSSRChg.setSsrChargeCode(ssrChgObj.getSsrChargeCode());
					jSonSSRChg.setDescription(ssrChgObj.getDescription());
					jSonSSRChg.setApplicablityType(ssrChgObj.getApplicablityType());
					jSonSSRChg.setStatus(ssrChgObj.getStatus());
					jSonSSRChg.setSsrId(ssrChgObj.getSsrId());
					jSonSSRChg.setChargeLocalCurrencyCode(ssrChgObj.getchargeLocalCurrencyCode());

					// jSonSSRChg.setCreatedBy(ssrChgObj.getCreatedBy());
					// jSonSSRChg.setCreatedDate(ssrChgObj.getCreatedDate());
					// jSonSSRChg.setModifiedBy(ssrChgObj.getModifiedBy());
					// jSonSSRChg.setModifiedDate(ssrChgObj.getModifiedDate());
					jSonSSRChg.setVersion(ssrChgObj.getVersion());

					addSSRInfo(jSonSSRChg.getSsrId(), colSSRInfo, jSonSSRChg, ssrChgObj);
					Map<String, LogicalCabinClassDTO> lccMap = CommonsServices.getGlobalConfig().getAvailableLogicalCCMap();
					ArrayList<String> ccList = new ArrayList<String>();
					ArrayList<String> lccList = new ArrayList<String>();
					for (SSRChargesCOS ssrCOSList : ssrChgObj.getSsrChargeCOS()) {
						if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
							if (ssrCOSList.getLogicalCCCode() != null) {
								lccList.add(ssrCOSList.getLogicalCCCode() + Constants.CARET_SEPERATOR);
							}
							if (ssrCOSList.getCabinClassCode() != null) {
								ccList.add(ssrCOSList.getCabinClassCode());
							}
						} else {

							if (ssrCOSList.getCabinClassCode() != null) {
								ccList.add(ssrCOSList.getCabinClassCode());
							} else {
								ccList.add((lccMap.get(ssrCOSList.getLogicalCCCode())).getCabinClassCode());
							}
						}
					}

					counmap.put("id", new Integer(((page - 1) * 20) + index).toString());
					counmap.put("ssrCharge", jSonSSRChg);
					counmap.put("ccList", ccList);
					counmap.put("lccList", lccList);

					dataRow[index - 1] = counmap;
					index++;
				}

				this.rows = dataRow;

			}

		} catch (ModuleException e) {
			this.succesMsg = e.getMessageString();
			this.msgType = WebConstants.MSG_ERROR;
			if (log.isErrorEnabled()) {
				log.error(e);
			}
		} catch (Exception ex) {
			this.succesMsg = "System Error Please Contact Administrator";
			this.msgType = WebConstants.MSG_ERROR;
			if (log.isErrorEnabled()) {
				log.error(ex);
			}
		}
		return S2Constants.Result.SUCCESS;
	}

	private void addSSRInfo(Integer ssrId, Collection<SSR> sSRInfoCol, SSRChargesDTO jSonSSRChg, SSRCharge ssrChgObj) {
		if (sSRInfoCol != null && sSRInfoCol.size() > 0) {

			for (SSR ssrInfo : sSRInfoCol) {
				if (ssrInfo.getSsrId() == ssrId) {
					jSonSSRChg.setSsrInfoCode(ssrInfo.getSsrCode());
					jSonSSRChg.setSsrInfoDescription(ssrInfo.getSsrDesc());
					Integer ssrCatId = ssrInfo.getSsrSubCategory().getCategory().getCatId();
					jSonSSRChg.setSsrInfoCatId(ssrCatId);

					if (SSRCategory.ID_INFLIGHT_SERVICE.intValue() == ssrCatId.intValue()) {
						jSonSSRChg.setChargeAmount(ssrChgObj.getAdultAmount());
						jSonSSRChg.setAdultAmount(new BigDecimal(0));
						jSonSSRChg.setChildAmount(new BigDecimal(0));
						jSonSSRChg.setInfantAmount(new BigDecimal(0));
						jSonSSRChg.setReservationAmount(new BigDecimal(0));

						jSonSSRChg.setLocalCurrChargeAmount(ssrChgObj.getLocalCurrAdultAmount());
						jSonSSRChg.setLocalCurrAdultAmount(new BigDecimal(0));
						jSonSSRChg.setLocalCurrChildAmount(new BigDecimal(0));
						jSonSSRChg.setLocalCurrInfantAmount(new BigDecimal(0));
						jSonSSRChg.setLocalCurrReservationAmount(new BigDecimal(0));
					} else if (SSRCategory.ID_AIRPORT_SERVICE.intValue() == ssrCatId.intValue()
							|| SSRCategory.ID_AIRPORT_TRANSFER.intValue() == ssrCatId.intValue()) {
						jSonSSRChg.setAdultAmount(ssrChgObj.getAdultAmount());
						jSonSSRChg.setChildAmount(ssrChgObj.getChildAmount());
						jSonSSRChg.setInfantAmount(ssrChgObj.getInfantAmount());
						jSonSSRChg.setReservationAmount(ssrChgObj.getReservationAmount());
						jSonSSRChg.setChargeAmount(new BigDecimal(0));

						jSonSSRChg.setLocalCurrAdultAmount(ssrChgObj.getLocalCurrAdultAmount());
						jSonSSRChg.setLocalCurrChildAmount(ssrChgObj.getLocalCurrChildAmount());
						jSonSSRChg.setLocalCurrInfantAmount(ssrChgObj.getLocalCurrInfantAmount());
						jSonSSRChg.setLocalCurrReservationAmount(ssrChgObj.getLocalCurrReservationAmount());
						jSonSSRChg.setLocalCurrChargeAmount(new BigDecimal(0));
					}

					jSonSSRChg.setSsrInfoCategory(ssrInfo.getSsrSubCategory().getCategory().getDescription());
					jSonSSRChg.setSsrInfoSubCatId(ssrInfo.getSsrSubCategory().getSubCatId());
					jSonSSRChg.setSsrInfoSubCategory(ssrInfo.getSsrSubCategory().getDescription());
					jSonSSRChg.setAdminVisibility(ssrInfo.getSsrSubCategory().getAdminVisibility());
					break;
				}
			}
		}
	}

	public String getAvailableSSR() {

		try {
			String strSSRCodeList = SelectListGenerator.createSSRCodeListAll();
			strSSRCodeList += "<option value='-1' selected='selected'></option>";
			this.updatedSsrCodes = strSSRCodeList;
		} catch (ModuleException moduleException) {
			log.error("cannot load updated ssr codes :" + moduleException);
			return S2Constants.Result.ERROR;
		}

		return S2Constants.Result.SUCCESS;
	}

	private boolean isViolateSSRCategoryLevelRestrictions(Integer ssrCategory) {
		boolean violates = false;

		if (ssrCategory != null && ssrCategory.equals(SSRCategory.ID_AIRPORT_TRANSFER)) {
			if (hdnMode.equals(WebConstants.ACTION_ADD)
					&& !BasicRequestHandler.hasPrivilege(request, WebConstants.PRIVI_AIRPORT_TRANSFER_SSR_ADD)) {
				violates = true;
			} else if (hdnMode.equals(WebConstants.ACTION_EDIT)
					&& !BasicRequestHandler.hasPrivilege(request, WebConstants.PRIVI_AIRPORT_TRANSFER_SSR_EDIT)) {
				violates = true;
			}
		}
		return violates;
	}

	private boolean checkConstrainsOnDeletion(Integer chargeId) throws ModuleException {
		boolean allow = true;

		SSRCharge extSSRCharge = ModuleServiceLocator.getChargeBD().getSSRCharges(chargeId);
		if (extSSRCharge != null) {
			Integer ssrCatId = ModuleServiceLocator.getSsrServiceBD().getSSRCategoryFromSSrId(extSSRCharge.getSsrId());
			if (ssrCatId.equals(SSRCategory.ID_AIRPORT_TRANSFER)
					&& !BasicRequestHandler.hasPrivilege(request, WebConstants.PRIVI_AIRPORT_TRANSFER_SSR_DELETE)) {
				allow = false;
			}
		}

		return allow;
	}

	/**
	 * @return the page
	 */
	public int getPage() {
		return page;
	}

	/**
	 * @param page
	 *            the page to set
	 */
	public void setPage(int page) {
		this.page = page;
	}

	/**
	 * @return the total
	 */
	public int getTotal() {
		return total;
	}

	/**
	 * @param total
	 *            the total to set
	 */
	public void setTotal(int total) {
		this.total = total;
	}

	/**
	 * @return the records
	 */
	public int getRecords() {
		return records;
	}

	/**
	 * @param records
	 *            the records to set
	 */
	public void setRecords(int records) {
		this.records = records;
	}

	/**
	 * @return the rows
	 */
	public Object[] getRows() {
		return rows;
	}

	/**
	 * @param rows
	 *            the rows to set
	 */
	public void setRows(Object[] rows) {
		this.rows = rows;
	}

	/**
	 * @return the succesMsg
	 */
	public String getSuccesMsg() {
		return succesMsg;
	}

	/**
	 * @param succesMsg
	 *            the succesMsg to set
	 */
	public void setSuccesMsg(String succesMsg) {
		this.succesMsg = succesMsg;
	}

	/**
	 * @return the msgType
	 */
	public String getMsgType() {
		return msgType;
	}

	/**
	 * @param msgType
	 *            the msgType to set
	 */
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the version
	 */
	public long getVersion() {
		return version;
	}

	/**
	 * @param version
	 *            the version to set
	 */
	public void setVersion(long version) {
		this.version = version;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate
	 *            the createdDate to set
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return the hdnMode
	 */
	public String getHdnMode() {
		return hdnMode;
	}

	/**
	 * @param hdnMode
	 *            the hdnMode to set
	 */
	public void setHdnMode(String hdnMode) {
		this.hdnMode = hdnMode;
	}

	public BigDecimal getServiceCharge() {
		return serviceCharge;
	}

	public void setServiceCharge(BigDecimal serviceCharge) {
		this.serviceCharge = serviceCharge;
	}

	public String getUpdatedSsrCodes() {
		return updatedSsrCodes;
	}

	public void setUpdatedSsrCodes(String updatedSsrCodes) {
		this.updatedSsrCodes = updatedSsrCodes;
	}

	public Integer getSsrId() {
		return ssrId;
	}

	public void setSsrId(Integer ssrId) {
		this.ssrId = ssrId;
	}

	public BigDecimal getAdultAmount() {
		return adultAmount;
	}

	public void setAdultAmount(BigDecimal adultAmount) {
		this.adultAmount = adultAmount;
	}

	public BigDecimal getChildAmount() {
		return childAmount;
	}

	public void setChildAmount(BigDecimal childAmount) {
		this.childAmount = childAmount;
	}

	public BigDecimal getInfantAmount() {
		return infantAmount;
	}

	public void setInfantAmount(BigDecimal infantAmount) {
		this.infantAmount = infantAmount;
	}

	public BigDecimal getReservationAmount() {
		return reservationAmount;
	}

	public void setReservationAmount(BigDecimal reservationAmount) {
		this.reservationAmount = reservationAmount;
	}

	public String getApplyBy() {
		return applyBy;
	}

	public void setApplyBy(String applyBy) {
		this.applyBy = applyBy;
	}

	public String getChargeCode() {
		return chargeCode;
	}

	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}

	public Integer getChargeId() {
		return chargeId;
	}

	public void setChargeId(Integer chargeId) {
		this.chargeId = chargeId;
	}

	public String getSrchChargeCode() {
		return srchChargeCode;
	}

	public void setSrchChargeCode(String srchChargeCode) {
		this.srchChargeCode = srchChargeCode;
	}

	public String getSelSsrCode() {
		return selSsrCode;
	}

	public void setSelSsrCode(String selSsrCode) {
		this.selSsrCode = selSsrCode;
	}

	public String getSelCategory() {
		return selCategory;
	}

	public void setSelCategory(String selCategory) {
		this.selCategory = selCategory;
	}

	public String getSelStatus() {
		return selStatus;
	}

	public void setSelStatus(String selStatus) {
		this.selStatus = selStatus;
	}

	public String getSsrDesc() {
		return ssrDesc;
	}

	public void setSsrDesc(String ssrDesc) {
		this.ssrDesc = ssrDesc;
	}

	public Integer getSsrCatId() {
		return ssrCatId;
	}

	public void setSsrCatId(Integer ssrCatId) {
		this.ssrCatId = ssrCatId;
	}

	public Integer getSsrSubCatId() {
		return ssrSubCatId;
	}

	public void setSsrSubCatId(Integer ssrSubCatId) {
		this.ssrSubCatId = ssrSubCatId;
	}

	public int getSsrInfoCatId() {
		return ssrInfoCatId;
	}

	public void setSsrInfoCatId(int ssrInfoCatId) {
		this.ssrInfoCatId = ssrInfoCatId;
	}

	/**
	 * @return the cos
	 */
	public String getCos() {
		return cos;
	}

	/**
	 * @param cos
	 *            the cos to set
	 */
	public void setCos(String cos) {
		this.cos = cos;
	}

	public String getLocalCurrCode() {
		return localCurrCode;
	}

	public void setLocalCurrCode(String localCurrCode) {
		this.localCurrCode = localCurrCode;
	}
}
