package com.isa.thinair.airadmin.core.web.generator.master;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airmaster.api.criteria.StationSearchCriteria;
import com.isa.thinair.airmaster.api.model.Country;
import com.isa.thinair.airmaster.api.model.State;
import com.isa.thinair.airmaster.api.model.Station;
import com.isa.thinair.airmaster.api.model.Territory;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author srikantha
 * 
 */
public class StationHTMLGenerator {

	private static Log log = LogFactory.getLog(StationHTMLGenerator.class);

	private static String clientErrors;

	/**
	 * Gets Station Grid Row For the Search
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array containing the Station Grid data
	 * @throws ModuleException
	 *             the ModuleException
	 */
	@SuppressWarnings("unchecked")
	public final String getStationRowHtml(HttpServletRequest request) throws ModuleException {

		log.debug("inside StationHTMLGenerator.getStationRowHtml()");

		String strCountryCode = "";
		Collection<Station> list = new ArrayList<Station>();

		int recordNo = 0;
		if (request.getParameter("hdnRecNo") != null && !request.getParameter("hdnRecNo").equals("")
				&& (new Integer(request.getParameter("hdnRecNo"))).intValue() > 0)
			recordNo = Integer.parseInt(request.getParameter("hdnRecNo")) - 1;

		Page page = null;
		int totalRecords = 0;
		String strJavascriptTotalNoOfRecs = "";
		String strCountryCodeJS = "var strCountryCode=-1;";

		StationSearchCriteria searchCriteria = new StationSearchCriteria();
		strCountryCode = request.getParameter("selCountry1");
		strCountryCodeJS = "var strCountryCode = '" + strCountryCode + "';";
		if (strCountryCode != null && !strCountryCode.equals("-1")) {
			searchCriteria.setCountryCode(strCountryCode);
		}

		page = ModuleServiceLocator.getLocationServiceBD().getStations(searchCriteria, recordNo, 20);

		if (page != null)
			totalRecords = page.getTotalNoOfRecords();

		strJavascriptTotalNoOfRecs = "var totalRecords = " + totalRecords + ";";
		request.setAttribute(WebConstants.REQ_TOTAL_RECORDS, strJavascriptTotalNoOfRecs);

		list = (List<Station>) page.getPageData();
		request.setAttribute("CountryCode", strCountryCodeJS);
		return createStationRowHTML(list);
	}

	/**
	 * Creates Station Grid data from a Collection of Stations
	 * 
	 * @param stations
	 *            the Collection of Stations
	 * @return String the Array contatining the Station grid
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private String createStationRowHTML(Collection<Station> stations) throws ModuleException {

		Station station = null;
		List<Station> list = (List<Station>) stations;
		Object[] listArr = list.toArray();
		StringBuffer sb = new StringBuffer("var arrData = new Array();");
		Collection<Country> countrie = ModuleServiceLocator.getLocationServiceBD().getCountries();
		Collection<Territory> territories = ModuleServiceLocator.getLocationServiceBD().getTerritories();

		for (int i = 0; i < listArr.length; i++) {
			station = (Station) listArr[i];
			sb.append("arrData[" + i + "] = new Array();");
			sb.append("arrData[" + i + "][1] = '" + station.getStationCode() + "';");
			sb.append("arrData[" + i + "][2] = '" + station.getStationName() + "';");
			sb.append("arrData[" + i + "][3] = '" + getTerritoryDescription(station.getTerritoryCode(), territories) + "';");
			sb.append("arrData[" + i + "][4] = '" + getCountryDescription(station.getCountryCode(), countrie) + "';");
			if (station.getOnlineStatus() == Station.ONLINE_STATUS_ACTIVE)
				sb.append("arrData[" + i + "][5] = 'Yes';");
			else
				sb.append("arrData[" + i + "][5] = 'No';");

			sb.append("arrData[" + i + "][6] = '" + AiradminUtils.getModifiedStatus(station.getStatus()) + "';");
			if (station.getRemarks() != null)
				sb.append("arrData[" + i + "][7] = '" + station.getRemarks() + "';");
			else
				sb.append("arrData[" + i + "][7] = '';");

			sb.append("arrData[" + i + "][8] = '" + station.getVersion() + "';");

			if (station.getTerritoryCode() != null)
				sb.append("arrData[" + i + "][9] = '" + station.getTerritoryCode() + "';");
			else
				sb.append("arrData[" + i + "][9] = '';");

			sb.append("arrData[" + i + "][10] = '" + station.getCountryCode() + "';");

			if (station.getContact() != null) {
				sb.append("arrData[" + i + "][11] = '" + station.getContact() + "';");
			} else {
				sb.append("arrData[" + i + "][11] = '';");
			}

			if (station.getTelephone() != null) {
				sb.append("arrData[" + i + "][12] = '" + station.getTelephone() + "';");
			} else {
				sb.append("arrData[" + i + "][12] = '';");
			}

			if (station.getFax() != null) {
				sb.append("arrData[" + i + "][13] = '" + station.getFax() + "';");
			} else {
				sb.append("arrData[" + i + "][13] = '';");
			}

			if (station.getNameChangeThresholdTime() != null) {
				sb.append("arrData[" + i + "][14] = '" + station.getNameChangeThresholdTime() + "';");
			} else {
				sb.append("arrData[" + i + "][14] = '';");
			}
			if (station.getStateId() != null) {
				State state = ModuleServiceLocator.getCommonServiceBD().getState(station.getStateId());
				if(state != null){
					sb.append("arrData[" + i + "][15] = '" + state.getStateCode().trim() + "';");
				} else {
					sb.append("arrData[" + i + "][15] = '';");
				}				
			} else {
				sb.append("arrData[" + i + "][15] = '';");
			}
		}
		return sb.toString();
	}

	/**
	 * Create the Client validations for Station Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array of Client Validations
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {

		if (clientErrors == null) {
			Properties moduleErrs = new Properties();

			moduleErrs.setProperty("um.station.form.id.required", "stationIdRqrd");
			moduleErrs.setProperty("um.station.form.description.required", "stationDescRqrd");
			moduleErrs.setProperty("um.station.form.id.already.exist", "stationIdExist");
			moduleErrs.setProperty("um.airadmin.delete.confirmation", "deleteConf");
			moduleErrs.setProperty("um.station.form.id.length", "stationIdlength");
			moduleErrs.setProperty("um.station.form.Territory.Code.required", "stationtertryRqrd");
			moduleErrs.setProperty("um.station.form.Country.Code.required", "stationcntryReqrd");
			moduleErrs.setProperty("um.station.form.row.required", "stationrowRqrd");
			moduleErrs.setProperty("um.station.form.state.required", "stateReqrd");

			clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}
		return clientErrors;
	}

	/**
	 * Gets the Territory Description for a Given Territory code
	 * 
	 * @param strTerritoryCode
	 *            the Territory Code
	 * @param territories
	 *            the Territory Collection
	 * @return String the Territory Description
	 */
	private String getTerritoryDescription(String strTerritoryCode, Collection<Territory> territories) {
		String strTerritoryDesc = "";
		Territory terry = null;
		if (territories != null) {
			Iterator<Territory> iter = territories.iterator();
			while (iter.hasNext()) {
				terry = (Territory) iter.next();
				if (terry.getTerritoryCode().equals(strTerritoryCode)) {
					strTerritoryDesc = terry.getDescription();
					break;
				}
			}
		}
		return strTerritoryDesc;
	}

	/**
	 * Get the Country Name for a Given Country Code
	 * 
	 * @param strCountryCode
	 *            the Country Code
	 * @param countries
	 *            the Country Collection
	 * @return the Country Name
	 */
	private String getCountryDescription(String strCountryCode, Collection<Country> countries) {
		String countryName = "";
		Country country = null;

		if (countries != null) {
			Iterator<Country> iter = countries.iterator();
			while (iter.hasNext()) {
				country = (Country) iter.next();
				if (country.getCountryCode().equals(strCountryCode)) {
					countryName = country.getCountryName();
					break;
				}
			}
		}
		return countryName;
	}
}
