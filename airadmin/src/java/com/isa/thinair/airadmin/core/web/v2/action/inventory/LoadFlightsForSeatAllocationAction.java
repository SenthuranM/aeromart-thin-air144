package com.isa.thinair.airadmin.core.web.v2.action.inventory;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airadmin.core.web.util.SeatInventoryHelper;
import com.isa.thinair.airadmin.core.web.v2.action.common.CommonAdminRequestResponseAwareCommonAction;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airadmin.core.web.v2.dto.inventory.FlightForSeatAllocationTO;
import com.isa.thinair.airinventory.api.dto.FCCInventorySummaryDTO;
import com.isa.thinair.airinventory.api.dto.FCCSegInvSummaryDTO;
import com.isa.thinair.commons.api.constants.DayOfWeek;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.reporting.api.criteria.FlightSearchCriteria;
import com.isa.thinair.reporting.api.model.FlightInventorySummaryDTO;
import com.isa.thinair.webplatform.core.util.DataUtil;

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class LoadFlightsForSeatAllocationAction extends CommonAdminRequestResponseAwareCommonAction {

	private static Log log = LogFactory.getLog(LoadFlightsForSeatAllocationAction.class);

	private static final int PAGE_LENGTH = 20;

	private static final String ON = "on";

	private String txtDepartureDateFrom;

	private String txtDepartureDateTo;

	private String txtFlightNumber;

	private String radTime;

	private String selFltStatus;

	private String selDeparture;

	private String selArrival;

	private String selVia1;

	private String selVia2;

	private String selVia3;

	private String selVia4;

	private String selCabinClass;

	private String txtMinSeatFactor;

	private String txtMaxSeatFactor;

	private int page;

	private int total;

	private int records;

	private String hdnMode;

	private String chkSunday;

	private String chkMonday;

	private String chkTueday;

	private String chkWedday;

	private String chkThuday;

	private String chkFriday;

	private String chkSatday;

	private Collection<Map<String, Object>> rows;

	private SeatInventoryHelper seatInventoryHelper = new SeatInventoryHelper();

	private FlightSearchCriteria flightSearchCriteria = null;

	/**
	 * main Execute Method For Load Flights For Seat Allocation Action
	 * 
	 * @return String the Forward Action
	 */
	public String execute() {

		String forward = AdminStrutsConstants.AdminAction.SUCCESS;
		setDefaultLoadSuccessMessage();

		try {
			if (hdnMode != null && hdnMode.equals(WebConstants.ACTION_SEARCH)) {
				flightSearchCriteria = new FlightSearchCriteria();
				setFlightsForSeatAllocation();
			}
		} catch (Exception exception) {
			log.error("Exception in LoadFlightsForSeatAllocationAction.execute()", exception);
			this.msgType = WebConstants.MSG_ERROR;
			this.message = airadminConfig.getMessage(WebConstants.KEY_LOAD_FAIL);
		}

		return forward;
	}

	private void setDefaultLoadSuccessMessage() {
		this.message = airadminConfig.getMessage(WebConstants.KEY_LOAD_SUCCESS);
		this.msgType = WebConstants.MSG_SUCCESS;
	}

	private void setFlightsForSeatAllocation() throws ModuleException {

		List<FlightForSeatAllocationTO> flightForSeatAllocationTOList = null;

		// preparing and setting the departureDateFrom date
		flightSearchCriteria.setDepartureDateFrom(AiradminUtils.getFormattedDate(txtDepartureDateFrom));

		// preparing and setting the departureDatTo date
		flightSearchCriteria.setDepartureDateTo(AiradminUtils.getFormattedDate(txtDepartureDateTo));

		if (selArrival != null && !(selArrival.equals("-1"))) {
			flightSearchCriteria.setDestination(selArrival);
		}

		if (selDeparture != null && !(selDeparture.equals("-1"))) {
			flightSearchCriteria.setOrigin(selDeparture);
		}

		if (txtFlightNumber != null && !txtFlightNumber.equals("")) {
			flightSearchCriteria.setFlightNumber(txtFlightNumber + "%");
		} else {
			flightSearchCriteria.setFlightNumber("%");
		}

		// setting the via points
		ArrayList<String> viaList = new ArrayList<String>();

		if (selVia1 != null && !(selVia1.equals("-1"))) {
			viaList.add(selVia1);
		}

		if (selVia2 != null && !(selVia2.equals("-1"))) {
			if (selVia1 == null || selVia1.equals("")) {
				viaList.add(null);
			}
			viaList.add(selVia2);
		}

		if (selVia3 != null && !(selVia3.equals("-1"))) {
			if (selVia1 == null || selVia1.equals("")) {
				viaList.add(null);
			}
			if (selVia2 == null || selVia2.equals("")) {
				viaList.add(null);
			}
			viaList.add(selVia3);
		}

		if (selVia4 != null && !(selVia4.equals("-1"))) {
			if (selVia1 == null || selVia1.equals("")) {
				viaList.add(null);
			}
			if (selVia2 == null || selVia2.equals("")) {
				viaList.add(null);
			}
			if (selVia3 == null || selVia3.equals("")) {
				viaList.add(null);
			}
			viaList.add(selVia4);
		}

		if (ON.equals(chkSunday) || ON.equals(chkMonday) || ON.equals(chkTueday) || ON.equals(chkWedday) || ON.equals(chkThuday)
				|| ON.equals(chkFriday) || ON.equals(chkSatday)) {
			StringBuilder dayNums = new StringBuilder();
			Integer count = new Integer(0);
			if (ON.equals(chkSunday)) {
				count = prepareDayNumInClause(count, dayNums, DayOfWeek.SUNDAY);
			}
			if (ON.equals(chkMonday)) {
				count = prepareDayNumInClause(count, dayNums, DayOfWeek.MONDAY);
			}
			if (ON.equals(chkTueday)) {
				count = prepareDayNumInClause(count, dayNums, DayOfWeek.TUESDAY);
			}
			if (ON.equals(chkWedday)) {
				count = prepareDayNumInClause(count, dayNums, DayOfWeek.WEDNESDAY);
			}
			if (ON.equals(chkThuday)) {
				count = prepareDayNumInClause(count, dayNums, DayOfWeek.THURSDAY);
			}
			if (ON.equals(chkFriday)) {
				count = prepareDayNumInClause(count, dayNums, DayOfWeek.FRIDAY);
			}
			if (ON.equals(chkSatday)) {
				count = prepareDayNumInClause(count, dayNums, DayOfWeek.SATURDAY);
			}
			flightSearchCriteria.setDayNumber(dayNums.toString());
		}

		flightSearchCriteria.setViaPoints(viaList);

		// setting the cabin class code search criteria
		if (selCabinClass != null && !(selCabinClass.equals("-1"))) {
			flightSearchCriteria.setCabinClassCode(selCabinClass);
		}

		if (!selFltStatus.equals("-1")) {
			flightSearchCriteria.addFlightStatus(selFltStatus);
		}

		if (radTime != null && radTime.equals("L")) {
			flightSearchCriteria.setTimeInLocal(true);
		} else {
			flightSearchCriteria.setTimeInLocal(false);
		}

		flightSearchCriteria.setSeatFactor(setSeatFactor(txtMinSeatFactor, txtMaxSeatFactor));

		int startIndex = (page - 1) * PAGE_LENGTH;
		Collection<FlightInventorySummaryDTO> flightCollections = null;
		Page<FlightInventorySummaryDTO> pagedReocrds = ModuleServiceLocator.getDataExtractionBD()
				.searchFlightsForInventoryUpdation(flightSearchCriteria, startIndex, PAGE_LENGTH);

		if (pagedReocrds != null) {
			this.page = getCurrentPageNo(pagedReocrds.getStartPosition());
			this.total = getTotalNoOfPages(pagedReocrds.getTotalNoOfRecords());
			this.records = pagedReocrds.getTotalNoOfRecords();

			flightCollections = pagedReocrds.getPageData();
			flightForSeatAllocationTOList = setFlightsForSeatAllocationPerPageTO(flightCollections);
			this.rows = DataUtil.createGridData(pagedReocrds.getStartPosition(), "flightForSeatAllocationTO",
					flightForSeatAllocationTOList);
		}
	}

	private List<FlightForSeatAllocationTO> setFlightsForSeatAllocationPerPageTO(
			Collection<FlightInventorySummaryDTO> flightCollections) throws ModuleException {

		List<FlightForSeatAllocationTO> flightForSeatAllocationTOList = new ArrayList<FlightForSeatAllocationTO>();

		String defaultCabinClass = seatInventoryHelper.getDefaultCabinClass();
		SimpleDateFormat dateFormater = new SimpleDateFormat("dd-MM-yyyy HH:mm");

		for (FlightInventorySummaryDTO flightInventorySummaryDTO : flightCollections) {
			FlightForSeatAllocationTO flightForSeatAllocationTO = new FlightForSeatAllocationTO();

			flightForSeatAllocationTO.setFlightNumber(flightInventorySummaryDTO.getFlightNumber());
			flightForSeatAllocationTO.setDeparture(flightInventorySummaryDTO.getDeparture());
			flightForSeatAllocationTO.setDepartureDateTime(dateFormater.format(flightInventorySummaryDTO.getDepartureDateTime()));
			flightForSeatAllocationTO.setArrival(flightInventorySummaryDTO.getArrival());
			flightForSeatAllocationTO.setArrivalDateTime(dateFormater.format(flightInventorySummaryDTO.getArrivalDateTime()));

			List<String> viaPointsList = flightInventorySummaryDTO.getViaPoints();
			String strViaPoints = "";
			if (viaPointsList != null) {
				for (int q = 0; q < viaPointsList.size(); q++) {
					strViaPoints += viaPointsList.get(q);
					if (q != (viaPointsList.size() - 1)) {
						if ((q % 2 == 0) && (q != 0)) {
							strViaPoints += "<BR>";
						} else {
							strViaPoints += ",";
						}
					}
				}
			}

			flightForSeatAllocationTO.setViaPoints(strViaPoints);
			flightForSeatAllocationTO.setModelNumber(flightInventorySummaryDTO.getModelNumber());

			// preparing inventory data
			List fccInventories = flightInventorySummaryDTO.getFccInventorySummaryDTO();
			String[] cabinClasses = new String[fccInventories.size()];

			List fccSegInvList = new ArrayList();
			for (int p = 0; p < fccInventories.size(); p++) {
				FCCInventorySummaryDTO fltIntrySummry = (FCCInventorySummaryDTO) fccInventories.get(p);
				fccSegInvList.addAll(fltIntrySummry.getFccSegInventorySummary());
				cabinClasses[p] = fltIntrySummry.getCabinClassCode();
			}
			int fccSegInvCount = fccSegInvList.size();

			String[] segmentAllocations = new String[fccSegInvCount];
			String[] segmentInfantAllocations = new String[fccSegInvCount];
			String[] segmentSold = new String[fccSegInvCount];
			String[] segmentOnHold = new String[fccSegInvCount];
			String[] segmentInfantSold = new String[fccSegInvCount];
			String[] segmentInfantOnHold = new String[fccSegInvCount];
			String[] segmentSeatFactor = new String[fccSegInvCount];
			String[] segmentLowestPubFare = new String[fccSegInvCount];
			StringBuffer segAllocations = new StringBuffer();
			StringBuffer segSold = new StringBuffer();
			StringBuffer segOnHold = new StringBuffer();
			StringBuffer lowestPubFares = new StringBuffer();
			String firstSegCode = null;

			for (int q = 0; q < fccSegInvCount; q++) {
				FCCSegInvSummaryDTO fltSegIntrySummry = (FCCSegInvSummaryDTO) fccSegInvList.get(q);
				if (firstSegCode == null) {
					firstSegCode = fltSegIntrySummry.getSegmentCode();
				}
				if (firstSegCode != null && firstSegCode.equalsIgnoreCase(fltSegIntrySummry.getSegmentCode())) {
					segmentAllocations[q] = String.valueOf(fltSegIntrySummry.getAdultAllocation());
					segmentInfantAllocations[q] = String.valueOf(fltSegIntrySummry.getInfantAllocation());
				}
				segmentOnHold[q] = String.valueOf(fltSegIntrySummry.getAdultOnhold());
				segmentSold[q] = String.valueOf(fltSegIntrySummry.getAdultSold());
				segmentInfantSold[q] = String.valueOf(fltSegIntrySummry.getInfantSold());
				segmentInfantOnHold[q] = String.valueOf(fltSegIntrySummry.getInfantOnhold());
				segmentSeatFactor[q] = String.valueOf(fltSegIntrySummry.getSeatFactor());
				Object[] fareDetails = ModuleServiceLocator.getBookingClassBD().getMinimumPublicFare(
						flightInventorySummaryDTO.getFlightId(), fltSegIntrySummry.getSegmentCode(),
						fltSegIntrySummry.getLogicalCabinClassCode());
				segmentLowestPubFare[q] = getFareDetails(fareDetails);
			}

			StringBuilder strSeatFactor = new StringBuilder();

			for (int n = 0; n < fccSegInvCount; n++) {
				if (AppSysParamsUtil.isHideAllocateScreenExtraDetails()) {
					if (segmentAllocations[n] != null) {
						segAllocations.append((n == 0 ? "" : " ,") + segmentAllocations[n]);
					}
					segSold.append((n == 0 ? "" : " ,") + segmentSold[n]);
					segOnHold.append((n == 0 ? "" : " ,") + segmentOnHold[n]);
				} else {
					if (segmentAllocations[n] != null) {
						segAllocations.append((n == 0 ? "" : " ,") + segmentAllocations[n]).append("/")
								.append(segmentInfantAllocations[n]);
					}
					segSold.append((n == 0 ? "" : " ,") + segmentSold[n]).append("/").append(segmentInfantSold[n]);
					segOnHold.append((n == 0 ? "" : " ,") + segmentOnHold[n]).append("/").append(segmentInfantOnHold[n]);
				}
				strSeatFactor.append(segmentSeatFactor[n] + ((n == (fccSegInvCount - 1)) ? "<BR>" : ", "));
				if (segmentLowestPubFare[n] != null && !segmentLowestPubFare[n].equals("")) {
					lowestPubFares.append(segmentLowestPubFare[n] + ((n == (fccSegInvCount - 1)) ? "<BR>" : ", "));
				} else {
					lowestPubFares.append("_" + ((n == (fccSegInvCount - 1)) ? "<BR>" : ", "));
				}
			}

			flightForSeatAllocationTO.setSegmentAllocation(segAllocations.toString());
			flightForSeatAllocationTO.setSegmentSold(segSold.toString());
			flightForSeatAllocationTO.setSegmentOnHold(segOnHold.toString());
			flightForSeatAllocationTO.setFlightId(flightInventorySummaryDTO.getFlightId());
			flightForSeatAllocationTO.setFlightStatus(flightInventorySummaryDTO.getFlightStatus());
			flightForSeatAllocationTO.setDefaultCabinClass(defaultCabinClass);
			flightForSeatAllocationTO.setOverlappingFlightId(flightInventorySummaryDTO.getOverlappingFlightId());
			flightForSeatAllocationTO.setAvailableSeatKilometers(flightInventorySummaryDTO.getAvailableSeatKilometers());
			flightForSeatAllocationTO.setScheduleId(flightInventorySummaryDTO.getScheduleId());
			flightForSeatAllocationTO.setSeatFactor(strSeatFactor.toString());
			flightForSeatAllocationTO.setLowestPubFare(lowestPubFares.toString());

			List<String[]> cosList = new ArrayList<String[]>();
			Map<String, String> activeCabinClasses = AiradminModuleUtils.getGlobalConfig().getActiveCabinClassesMap();
			for (String cos : cabinClasses) {
				for (String cabinClass : activeCabinClasses.keySet()) {
					if (cabinClass.equals(cos)) {
						String[] ccCodeAndDes = { cabinClass, activeCabinClasses.get(cabinClass) };
						cosList.add(ccCodeAndDes);
					}
				}
			}
			flightForSeatAllocationTO.setCosList(cosList);
			flightForSeatAllocationTOList.add(flightForSeatAllocationTO);
		}

		return flightForSeatAllocationTOList;

	}

	private int[] setSeatFactor(String min, String max) {
		try {
			// return (min != null && max != null && !min.equals("") && !max.equals("") ? new int
			// []{Integer.parseInt(min),Integer.parseInt(max)} : new int[]{0, 100});
			if (min == null || min.equals("")) {
				min = "0";
			}
			if (max == null || max.equals("")) {
				max = "100";
			}
			return new int[] { Integer.parseInt(min), Integer.parseInt(max) };
		} catch (NumberFormatException e) {
			return new int[] { 0, 100 };
		}
	}

	private int getCurrentPageNo(int startPosition) {
		int currentPageNo = 1;
		if (PAGE_LENGTH != 0) {
			currentPageNo = startPosition / PAGE_LENGTH + 1;
		}
		return currentPageNo;
	}

	private int getTotalNoOfPages(int totoalNoOfRecords) {
		int totalNoOfPages = 1;
		if (PAGE_LENGTH != 0) {
			totalNoOfPages = totoalNoOfRecords / PAGE_LENGTH;
			if (totoalNoOfRecords % PAGE_LENGTH > 0) {
				totalNoOfPages++;
			}
		}
		return totalNoOfPages;
	}

	private String getFareDetails(Object[] lowerstFareDetails) {
		StringBuilder fareDetails = new StringBuilder();
		if (lowerstFareDetails != null && lowerstFareDetails.length == 3) {
			fareDetails.append(lowerstFareDetails[0]).append(" ").append(lowerstFareDetails[1]).append(" ")
					.append(lowerstFareDetails[2]);
		}

		return fareDetails.toString();
	}

	private Integer prepareDayNumInClause(Integer count, StringBuilder dayNums, DayOfWeek dayOfWeek) {
		if (count == 0) {
			dayNums.append((Integer) CalendarUtil.getDayNumber(dayOfWeek));
		} else {
			dayNums.append(",").append((Integer) CalendarUtil.getDayNumber(dayOfWeek));
		}
		return ++count;
	}

	public String getTxtDepartureDateFrom() {
		return txtDepartureDateFrom;
	}

	public void setTxtDepartureDateFrom(String txtDepartureDateFrom) {
		this.txtDepartureDateFrom = txtDepartureDateFrom;
	}

	public String getTxtDepartureDateTo() {
		return txtDepartureDateTo;
	}

	public void setTxtDepartureDateTo(String txtDepartureDateTo) {
		this.txtDepartureDateTo = txtDepartureDateTo;
	}

	public String getTxtFlightNumber() {
		return txtFlightNumber;
	}

	public void setTxtFlightNumber(String txtFlightNumber) {
		this.txtFlightNumber = txtFlightNumber;
	}

	public String getRadTime() {
		return radTime;
	}

	public void setRadTime(String radTime) {
		this.radTime = radTime;
	}

	public String getSelFltStatus() {
		return selFltStatus;
	}

	public void setSelFltStatus(String selFltStatus) {
		this.selFltStatus = selFltStatus;
	}

	public String getSelDeparture() {
		return selDeparture;
	}

	public void setSelDeparture(String selDeparture) {
		this.selDeparture = selDeparture;
	}

	public String getSelArrival() {
		return selArrival;
	}

	public void setSelArrival(String selArrival) {
		this.selArrival = selArrival;
	}

	public String getSelVia1() {
		return selVia1;
	}

	public void setSelVia1(String selVia1) {
		this.selVia1 = selVia1;
	}

	public String getSelVia2() {
		return selVia2;
	}

	public void setSelVia2(String selVia2) {
		this.selVia2 = selVia2;
	}

	public String getSelVia3() {
		return selVia3;
	}

	public void setSelVia3(String selVia3) {
		this.selVia3 = selVia3;
	}

	public String getSelVia4() {
		return selVia4;
	}

	public void setSelVia4(String selVia4) {
		this.selVia4 = selVia4;
	}

	public String getSelCabinClass() {
		return selCabinClass;
	}

	public void setSelCabinClass(String selCabinClass) {
		this.selCabinClass = selCabinClass;
	}

	public String getTxtMinSeatFactor() {
		return txtMinSeatFactor;
	}

	public void setTxtMinSeatFactor(String txtMinSeatFactor) {
		this.txtMinSeatFactor = txtMinSeatFactor;
	}

	public String getTxtMaxSeatFactor() {
		return txtMaxSeatFactor;
	}

	public void setTxtMaxSeatFactor(String txtMaxSeatFactor) {
		this.txtMaxSeatFactor = txtMaxSeatFactor;
	}

	public String getHdnMode() {
		return hdnMode;
	}

	public void setHdnMode(String hdnMode) {
		this.hdnMode = hdnMode;
	}

	public Collection<Map<String, Object>> getRows() {
		return rows;
	}

	public void setRows(Collection<Map<String, Object>> rows) {
		this.rows = rows;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public SeatInventoryHelper getSeatInventoryHelper() {
		return seatInventoryHelper;
	}

	public void setSeatInventoryHelper(SeatInventoryHelper seatInventoryHelper) {
		this.seatInventoryHelper = seatInventoryHelper;
	}

	public String getChkSunday() {
		return chkSunday;
	}

	public void setChkSunday(String chkSunday) {
		this.chkSunday = chkSunday;
	}

	public String getChkMonday() {
		return chkMonday;
	}

	public void setChkMonday(String chkMonday) {
		this.chkMonday = chkMonday;
	}

	public String getChkTueday() {
		return chkTueday;
	}

	public void setChkTueday(String chkTueday) {
		this.chkTueday = chkTueday;
	}

	public String getChkWedday() {
		return chkWedday;
	}

	public void setChkWedday(String chkWedday) {
		this.chkWedday = chkWedday;
	}

	public String getChkThuday() {
		return chkThuday;
	}

	public void setChkThuday(String chkThuday) {
		this.chkThuday = chkThuday;
	}

	public String getChkFriday() {
		return chkFriday;
	}

	public void setChkFriday(String chkFriday) {
		this.chkFriday = chkFriday;
	}

	public String getChkSatday() {
		return chkSatday;
	}

	public void setChkSatday(String chkSatday) {
		this.chkSatday = chkSatday;
	}

	@Override
	public String getMessage() {
		return message;
	}

	@Override
	public void setMessage(String message) {
		this.message = message;

	}

	@Override
	public String getMsgType() {
		return msgType;
	}

	@Override
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}
}
