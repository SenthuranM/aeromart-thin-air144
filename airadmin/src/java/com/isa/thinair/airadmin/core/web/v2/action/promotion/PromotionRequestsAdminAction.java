package com.isa.thinair.airadmin.core.web.v2.action.promotion;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airschedules.api.dto.DisplayFlightDTO;
import com.isa.thinair.airschedules.api.utils.ModelNamesAndFieldNames;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.promotion.api.utils.PromotionType;
import com.isa.thinair.webplatform.api.util.JSONFETOParser;

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class PromotionRequestsAdminAction {

	private static Log log = LogFactory.getLog(PromotionRequestsAdminAction.class);

	// in
	private String flightDepStart;
	private String flightDepEnd;
	private String flightNumber;
	private Integer flightId;
	private Integer promotionType;
	private String promoApprovalData;

	// out
	private Collection<Map<String, Object>> rows;
	private int page;
	private int total;
	private int records;
	private String succesMsg;
	private String msgType;

	private static AiradminConfig airadminConfig = new AiradminConfig();

	public String initSearch() {
		return S2Constants.Result.SUCCESS;
	}

	public String searchFlights() {
		int pageSize = 20;
		int start = (page - 1) * pageSize;
		List<ModuleCriterion> critrian = new ArrayList<ModuleCriterion>();
		Map<String, Object> row;

		Date depStart = flightDepStart != null ? AiradminUtils.getFormattedDate(flightDepStart) : new Date();
		Date depEnd = flightDepEnd != null ? AiradminUtils.getFormattedDate(flightDepEnd) : CalendarUtil.addDateVarience(
				new Date(), 1);

		// criteria start date
		ModuleCriterion moduleCriterionStartD = new ModuleCriterion();
		moduleCriterionStartD.setCondition(ModuleCriterion.CONDITION_GREATER_THAN_OR_EQUALS);
		moduleCriterionStartD.setFieldName(ModelNamesAndFieldNames.Flight.FieldNames.DEPARTURE_DATE);
		List<Date> valueStartDate = new ArrayList<Date>();
		valueStartDate.add(depStart);
		moduleCriterionStartD.setValue(valueStartDate);
		critrian.add(moduleCriterionStartD);

		// criteria end date
		ModuleCriterion moduleCriterionStopD = new ModuleCriterion();
		moduleCriterionStopD.setCondition(ModuleCriterion.CONDITION_LESS_THAN_OR_EQUALS);
		moduleCriterionStopD.setFieldName(ModelNamesAndFieldNames.Flight.FieldNames.DEPARTURE_DATE);
		List<Date> valueStopDate = new ArrayList<Date>();
		valueStopDate.add(depEnd);
		moduleCriterionStopD.setValue(valueStopDate);
		critrian.add(moduleCriterionStopD);

		// criteria flight number
		ModuleCriterion moduleCriterionFlightNo = new ModuleCriterion();
		moduleCriterionFlightNo.setCondition(ModuleCriterion.CONDITION_LIKE);
		moduleCriterionFlightNo.setFieldName(ModelNamesAndFieldNames.Flight.FieldNames.FLIGHT_NUMBER);

		// flight number
		List<String> valueFlightNo = new ArrayList<String>();
		valueFlightNo.add(flightNumber.toUpperCase() + "%");
		moduleCriterionFlightNo.setValue(valueFlightNo);
		critrian.add(moduleCriterionFlightNo);

		try {
			Page<DisplayFlightDTO> pagedData = ModuleServiceLocator.getFlightServiceBD().searchFlightsForDisplay(critrian, start,
					pageSize, false, null, true, false, null);

			this.records = pagedData.getTotalNoOfRecords();
			this.total = pagedData.getTotalNoOfRecords() / pageSize;
			int mod = pagedData.getTotalNoOfRecords() % pageSize;
			if (mod > 0) {
				this.total = this.total + 1;
			}

			Collection<DisplayFlightDTO> templates = pagedData.getPageData();
			rows = new ArrayList<Map<String, Object>>();
			int a = 1;
			for (DisplayFlightDTO template : templates) {
				row = new HashMap<String, Object>();
				row.put("id", ((page - 1) * pageSize) + a++);
				row.put("flight", template);
				rows.add(row);
			}
		} catch (Exception e) {
			log.error("searchFlights ", e);
		}

		return S2Constants.Result.SUCCESS;
	}

	public String retrievePromotionApprovalView() {

		try {
			List<Object> results = ModuleServiceLocator.getPromotionAdministrationBD().getPromotionInquiryView(
					PromotionType.PROMOTION_FLEXI_DATE, flightId);

			this.records = results.size();
			this.total = 1;

			rows = new ArrayList<Map<String, Object>>();
			int a = 1;
			for (Object view : results) {
				Map<String, Object> row = new HashMap<String, Object>();
				row.put("id", a++);
				row.put("view", view);
				rows.add(row);
			}
		} catch (Exception e) {
			log.error("retrievePromotionApprovalView ", e);
		}

		return S2Constants.Result.SUCCESS;
	}

	public String approvePromotionRequests() {

		try {
			List<HashMap<String, Object>> approvalData = JSONFETOParser.parseCollection(ArrayList.class, HashMap.class,
					promoApprovalData);
			ModuleServiceLocator.getPromotionAdministrationBD().approveRejectPromotionRequests(
					PromotionType.PROMOTION_FLEXI_DATE, approvalData);
			this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_SEND_EMAIL_SUCCESS);
			this.msgType = WebConstants.MSG_SUCCESS;

		} catch (Exception e) {
			this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_TRANSFER_FAILED);
			this.msgType = WebConstants.MSG_ERROR;
			log.error("approvePromotionRequests ", e);
		}

		return S2Constants.Result.SUCCESS;
	}

	public void setFlightDepStart(String flightDepStart) {
		this.flightDepStart = flightDepStart;
	}

	public void setFlightDepEnd(String flightDepEnd) {
		this.flightDepEnd = flightDepEnd;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public void setFlightId(Integer flightId) {
		this.flightId = flightId;
	}

	public void setPromotionType(Integer promotionType) {
		this.promotionType = promotionType;
	}

	public String getPromoApprovalData() {
		return promoApprovalData;
	}

	public void setPromoApprovalData(String promoApprovalData) {
		this.promoApprovalData = promoApprovalData;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public Collection<Map<String, Object>> getRows() {
		return rows;
	}

	public int getTotal() {
		return total;
	}

	public String getSuccesMsg() {
		return succesMsg;
	}

	public void setSuccesMsg(String succesMsg) {
		this.succesMsg = succesMsg;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

}
