package com.isa.thinair.airadmin.core.web.handler.security;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.security.RoleHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airsecurity.api.model.Privilege;
import com.isa.thinair.airsecurity.api.model.Role;
import com.isa.thinair.airsecurity.api.model.RoleAccessibility;
import com.isa.thinair.airsecurity.api.model.RoleVisibility;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.airsecurity.api.utils.Constants;
import com.isa.thinair.commons.api.exception.MessagesUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author Thushara
 * 
 */
public class RoleRequestHandler extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(RoleRequestHandler.class);

	private static final String PARAM_VERSION = "hdnVersion";
	private static final String PARAM_ROLE_SEARCH_ID = "txtRoleNameSearch";
	private static final String PARAM_ROLE_ID = "txtRoleId";
	private static final String PARAM_HDN_ROLE_ID = "hdnRoleId";
	private static final String PARAM_ROLE_NAME = "txtRoleName";
	private static final String PARAM_REMARKS = "txtRemarks";
	private static final String PARAM_STATUS = "chkStatus";
	private static final String PARAM_ASSIGNED_PRIVILEGES = "hdnAssignPrivileges";
	private static final String PARAM_MODE = "hdnMode";
	private static final String PARAM_RECNO = "hdnRecNo";
	private static final String PARAM_AGENT_TYPE = "selAgentType";
	private static final String PARAM_ACCESSIBLE_TYPE = "selVisibleAgentType";
	private static final String PARAM_NOAGENT = "hdnNoAgent";
	private static final String PARAM_SERVICE_CHANNEL = "selServiceChannel";
	private static final String PARAM_SEL_AIRLINE_CODE = "selApplicableAirline";
	private static final String PARAM_SEL_INCLUDE_EXCLUDE = "selIncExc";
	private static final String PARAM_AIRLINE_CODE = "airlineCode";
	private static final String PARAM_INCLUDE_EXCLUDE = "includeExclude";

	private static AiradminConfig airadminConfig = new AiradminConfig();

	private static void setExceptionOccured(HttpServletRequest request, boolean value) {
		setAttribInRequest(request, "isExceptionOccured", new Boolean(value));
	}

	private static boolean isExceptionOccured(HttpServletRequest request) {
		return ((Boolean) getAttribInRequest(request, "isExceptionOccured")).booleanValue();
	}

	private static boolean isSuccessfulSaveMessageDisplayEnabled() {
		boolean isMsgDisplay = false;
		if (airadminConfig.getMessage("um.airadmin.savemessage.display").equals("true")) {
			isMsgDisplay = true;
		}
		return isMsgDisplay;
	}

	/**
	 * Main Excute Method for Roles Action
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {

		String strHdnAction = "";
		String strHdnMode = request.getParameter("hdnMode");
		strHdnAction = request.getParameter("hdnAction");
		String forward = WebConstants.ACTION_FORWARD_SUCCESS;

		if (strHdnAction != null && strHdnAction.equals(WebConstants.ACTION_ADD)) {
			setAttribInRequest(request, "strModeJS", "var isAddMode = true; var isCopyMode = false;");
		} else if (strHdnAction != null && strHdnAction.equals(WebConstants.ACTION_EDIT)) {
			setAttribInRequest(request, "strModeJS", "var isAddMode = false; var isCopyMode = false;");
		} else if (strHdnAction != null && strHdnAction.equals(WebConstants.ACTION_COPY)) {
			setAttribInRequest(request, "strModeJS", "var isAddMode = false; var isCopyMode = true;");
		}
		StringBuffer strSCBuff = new StringBuffer();
		if (hasPrivilege(request, "sys.security.role.servicechannel.view")) {
			strSCBuff.append("var isServiceCh = true;");
		} else {
			strSCBuff.append("var isServiceCh = false;");
		}

		if (hasPrivilege(request, "sys.security.role.servicechannel.edit")) {
			strSCBuff.append("var isServiceChEditable = true;");
		} else {
			strSCBuff.append("var isServiceChEditable = false;");
		}
		setAttribInRequest(request, "strServChJS", strSCBuff.toString());

		setExceptionOccured(request, false);
		setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = false;");

		try {
			if (strHdnMode != null && (strHdnMode.equals(WebConstants.ACTION_SAVE)) && strHdnAction != null) {

				if (strHdnAction.equals(WebConstants.ACTION_EDIT)) {
					checkPrivilege(request, WebConstants.PRIV_SYS_SECURITY_ROLE_EDIT);
					Properties props = getProperties(request);
					/*
					 * Role role = ModuleServiceLocator.getSecurityBD().getRole(props.getProperty(PARAM_ROLE_ID));
					 * //setting the previous airline code and include_exclude fields from the role. //Note . We Can't
					 * override the airline code when editing since role may be a dry Include/Exclude role
					 * 
					 * if (role.getAirlineCode() != null) { props.setProperty(PARAM_AIRLINE_CODE,
					 * role.getAirlineCode()); }
					 * 
					 * if (role.getIncludeExcludeFlag() != null) { props.setProperty(PARAM_INCLUDE_EXCLUDE,
					 * role.getIncludeExcludeFlag()); }
					 */

					saveData(request, props);
					saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_UPDATE_SUCCESS), WebConstants.MSG_SUCCESS);
				} else if (strHdnAction.equals(WebConstants.ACTION_ADD)) {
					checkPrivilege(request, WebConstants.PRIV_SYS_SECURITY_ROLE_ADD);
					Properties props = getProperties(request);
					// For newly created roles the airline code will be default airline code
					// props.setProperty(PARAM_AIRLINE_CODE, AppSysParamsUtil.getDefaultAirlineIdentifierCode());
					saveData(request, props);
					saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS), WebConstants.MSG_SUCCESS);
				} else if (strHdnAction.equals(WebConstants.ACTION_COPY)) {
					// checkPrivilege(request,WebConstants.PRIV_SYS_SECURITY_ROLE_ADD);
					Properties props = getProperties(request);
					// For newly created roles the airline code will be default airline code
					// props.setProperty(PARAM_AIRLINE_CODE, AppSysParamsUtil.getDefaultAirlineIdentifierCode());
					saveData(request, props);
					saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_COPY_SUCCESS), WebConstants.MSG_SUCCESS);
				}
			}
		} catch (ModuleException moduleException) {
			log.error("Exception in RoleRequestHandler.execute() [origin module=" + moduleException.getModuleDesc() + "]",
					moduleException);
			if (moduleException.getMessageString().equals(MessagesUtil.getMessage("module.duplicate.key"))) {
				saveMessage(request, airadminConfig.getMessage("um.role.form.id.dupkey"), WebConstants.MSG_ERROR);
			} else {
				saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
			}
			setExceptionOccured(request, true);
			setErrorFormData(request, getProperties(request));

		} catch (Exception exception) {
			log.error("Exception in RoleRequestHandler.execute()", exception);
			setExceptionOccured(request, true);

			if (exception instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
			} else {
				saveMessage(request, exception.getMessage(), WebConstants.MSG_ERROR);
				setErrorFormData(request, getProperties(request));
			}
		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_DELETE)) {
				checkPrivilege(request, WebConstants.PRIV_SYS_SECURITY_ROLE_DELETE);
				setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = false;");
				if (request.getParameter(PARAM_HDN_ROLE_ID) != null) {

					String strRoleId = request.getParameter(PARAM_HDN_ROLE_ID);
					Role existingRole = ModuleServiceLocator.getSecurityBD().getRole(strRoleId);
					if (existingRole == null)
						ModuleServiceLocator.getSecurityBD().removeRole(strRoleId, Constants.INTERNAL_CHANNEL);
					else {
						String strVersion = request.getParameter(PARAM_VERSION);
						if (strVersion != null && !"".equals(strVersion)) {
							existingRole.setVersion(Long.parseLong(strVersion));
							ModuleServiceLocator.getSecurityBD().removeRole(existingRole);
						} else {
							ModuleServiceLocator.getSecurityBD().removeRole(strRoleId, Constants.INTERNAL_CHANNEL);
						}
					}
					try {
						if (AppSysParamsUtil.isLCCDataSyncEnabled()) {
							existingRole.setStatus(Role.STATUS_DELETE);
							ModuleServiceLocator.getCommonServiceBD().publishSpecificUserDataUpdate(existingRole);
						}
					} catch (Exception exp) {
						log.error("Error in realtime LCC sync while Deleting ROLE");
					}

					saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_DELETE_SUCCESS), WebConstants.MSG_SUCCESS);
				} else {

					saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_DELETE_FAIL), WebConstants.MSG_ERROR);
				}
			}

		} catch (ModuleException moduleException) {
			log.error("Exception in RoleRequestHandler.execute() [origin module=" + moduleException.getModuleDesc() + "]",
					moduleException);
			if (moduleException.getMessageString().equals(MessagesUtil.getMessage("module.constraint.childrecord"))) {
				saveMessage(request, airadminConfig.getMessage("um.airadmin.childrecord"), WebConstants.MSG_ERROR);
			} else {
				saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
			}

		}

		try {
			setDisplayData(request);

		} catch (ModuleException moduleException) {
			log.error("Exception in RoleRequestHandler.execute() [origin module=" + moduleException.getModuleDesc() + "]",
					moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}

		return forward;

	}

	/**
	 * Sets the Roles Display Data
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setDisplayData(HttpServletRequest request) throws ModuleException {

		setClientErrors(request);
		setRoleRowHtml(request);
		setPrivilegeHtml(request);
		setDependancyHtml(request);
		setTravelAgentListHtml(request);
		setCarrierAgentType(request);
		setServiceChannelsHtml(request);
		setInclusionExclusion(request);
		setInterlinedAirlineCodes(request);

		if (!isExceptionOccured(request)) {
			String strFormData = "var arrFormData = new Array();";
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
		}
		request.setAttribute(WebConstants.REQ_OPERATION_MODE, getAttribInRequest(request, "strModeJS"));
		request.setAttribute(WebConstants.REQ_SERV_CH, getAttribInRequest(request, "strServChJS"));
		request.setAttribute(WebConstants.REQ_TRANSACTION_STATUS, getAttribInRequest(request, "isSaveTransactionSuccessfulJS"));
		request.setAttribute(WebConstants.REQ_TRANSACTION_DISPLAYSTATUS, "var isSuccessfulSaveMessageDisplayEnabled = "
				+ isSuccessfulSaveMessageDisplayEnabled() + ";");

		String strDefAirlineCode = "var defAirlineCode = '" + AppSysParamsUtil.getDefaultAirlineIdentifierCode() + "'";

		request.setAttribute("reqDefaultAirlineCode", strDefAirlineCode);
	}

	private static void setInclusionExclusion(HttpServletRequest request) throws ModuleException {
		request.setAttribute("reqIncExc", SelectListGenerator.createInclusionExclusionList());
	}

	private static void setInterlinedAirlineCodes(HttpServletRequest request) throws ModuleException {
		request.setAttribute("reqApplicableAirline", SelectListGenerator.createInterlinedAirlineCodesList());
	}

	private static void setCarrierAgentType(HttpServletRequest request) throws ModuleException {
		String strAgnentType = AppSysParamsUtil.getCarrierAgent();
		request.setAttribute("reqCarrierAgent", "crragent[0] ='" + strAgnentType + "';");
	}

	/**
	 * Sets the Client validations to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setClientErrors(HttpServletRequest request) throws ModuleException {

		String strClientErrors = RoleHTMLGenerator.getClientErrors(request);
		if (strClientErrors == null)
			strClientErrors = "";
		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);

	}

	/**
	 * Set the ServiceChannels to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuelException
	 */
	private static void setServiceChannelsHtml(HttpServletRequest request) throws ModuleException {
		String strSrvChs = SelectListGenerator.createServiceChannels();
		request.setAttribute(WebConstants.REQ_HTML_SERVICE_CHANNELS, strSrvChs);
	}

	/**
	 * Sets the Travel Agent Type List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setTravelAgentListHtml(HttpServletRequest request) throws ModuleException {

		String strAgentList = SelectListGenerator.createAgentTypeList_SG();
		request.setAttribute(WebConstants.REQ_HTML_AGENTTYPE_LIST, strAgentList);
	}

	/**
	 * Sets the Roles Grid Row to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setRoleRowHtml(HttpServletRequest request) throws ModuleException {
		RoleHTMLGenerator roleHtmlGenerator = new RoleHTMLGenerator(getProperties(request));
		String strHtml = roleHtmlGenerator.getRoleRowHtml(request);
		request.setAttribute(WebConstants.REQ_HTML_ROLE_DATA, strHtml);
	}

	/**
	 * Sets the Privileges Array to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setPrivilegeHtml(HttpServletRequest request) throws ModuleException {

		Principal principal = request.getUserPrincipal();
		User existingUser = ModuleServiceLocator.getSecurityBD().getUser(principal.getName());
		boolean sysUser = false;
		if (!existingUser.isEditable()) {
			sysUser = true;
		}
		String strRoleList = JavascriptGenerator.createPrivilegeHtml(sysUser);
		request.setAttribute(WebConstants.REQ_HTML_PRIVILEGE_DATA, strRoleList);
	}

	/**
	 * Sets the Dependancy Array to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setDependancyHtml(HttpServletRequest request) throws ModuleException {
		String strdependancy = JavascriptGenerator.createPrivilegeDependancyHtmlTemp();
		request.setAttribute(WebConstants.REQ_DEPENDANCY_ARRAY, strdependancy);
	}

	/**
	 * Saves the Role Data
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @param props
	 *            the Property file containg the request Data
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static void saveData(HttpServletRequest request, Properties props) throws ModuleException {

		Role role = null;
		String strVersion = props.getProperty(PARAM_VERSION);
		String strRoleId = props.getProperty(PARAM_ROLE_ID);
		String strAsgnPrivilegse = props.getProperty(PARAM_ASSIGNED_PRIVILEGES);
		String[] strAgentType = request.getParameterValues(PARAM_AGENT_TYPE);
		String[] strVisibleType = request.getParameterValues(PARAM_ACCESSIBLE_TYPE);
		String airlineCode = props.getProperty(PARAM_AIRLINE_CODE);
		String includeExclude = props.getProperty(PARAM_INCLUDE_EXCLUDE);
		String strCrrAgentType = AppSysParamsUtil.getCarrierAgent();
		Map<String, String[]> mapCustomContents = new LinkedHashMap<String, String[]>();
		mapCustomContents.put("granted_privileges", new String[] {});
		mapCustomContents.put("revoked_privileges", new String[] {});
		String serviceChannel = props.getProperty(PARAM_SERVICE_CHANNEL);

		String strHdnAction = request.getParameter("hdnAction");

		if (serviceChannel == null || serviceChannel.trim().isEmpty()) {
			log.error("Service channel is empty. Please fix channel code .. AA, AH. HO");
		}

		if (role == null) {
			role = new Role();
			Set<Privilege> setprivilege = new HashSet<Privilege>();
			Collection<Privilege> privCollec = ModuleServiceLocator.getSecurityBD().getActivePrivileges(
					Constants.INTERNAL_CHANNEL);

			List<String> newlyAddedPrivList = new ArrayList<String>();
			List<String> newlyRemovedPrivList = new ArrayList<String>();

			if (strRoleId != null && !"".equals(strRoleId)) {
				role.setRoleId(strRoleId);
			} else {
				String airlineIdentifier = AppSysParamsUtil.getDefaultAirlineIdentifierCode();
				String nextSeqNo = ModuleServiceLocator.getSecurityBD().getNextSequenceForRole();
				role.setRoleId(airlineIdentifier + nextSeqNo);
			}
			role.setRoleName(props.getProperty(PARAM_ROLE_NAME));
			role.setRemarks(props.getProperty(PARAM_REMARKS));
			role.setStatus(props.getProperty(PARAM_STATUS).equals("Active") ? Role.STATUS_ACTIVE : Role.STATUS_INACTIVE);
			role.setServiceChannel(serviceChannel);
			role.setAirlineCode(airlineCode);
			role.setIncludeExcludeFlag(includeExclude);

			if (strHdnAction.equals(WebConstants.ACTION_ADD) || strHdnAction.equals(WebConstants.ACTION_COPY)) {
				role.setLccPublishStatus(Util.LCC_TOBE_PUBLISHED);
			} else if (strHdnAction.equals(WebConstants.ACTION_EDIT)) {
				role.setLccPublishStatus(Util.LCC_TOBE_REPUBLISHED);
			} 

			if (strAsgnPrivilegse != null) {
				Privilege privilege = null;
				Iterator<Privilege> privIter = privCollec.iterator();
				while (privIter.hasNext()) {
					privilege = (Privilege) privIter.next();
					StringTokenizer strTok = new StringTokenizer(strAsgnPrivilegse, ",");
					while (strTok.hasMoreTokens()) {

						String strToken = strTok.nextToken();
						if (privilege.getPrivilegeId().equals(strToken)) {
							setprivilege.add(privilege);
							break;
						}
					}
				}
				role.setPrivileges(setprivilege);

				if (strRoleId != null && !"".equals(strRoleId)) {
					// No need to pass serviceChannel as we need to support all channels
					Collection<Privilege> privCol = ModuleServiceLocator.getSecurityBD().getPrivilegesForRole(strRoleId);

					Map<String, Privilege> newPrivMap = new HashMap<String, Privilege>();
					for (Privilege pri : setprivilege) {
						newPrivMap.put(pri.getPrivilegeId(), pri);
					}

					Map<String, Privilege> existingPrivMap = new HashMap<String, Privilege>();
					for (Privilege pri : privCol) {
						existingPrivMap.put(pri.getPrivilegeId(), pri);
					}

					for (Privilege pri : setprivilege) {
						if (!existingPrivMap.containsKey(pri.getPrivilegeId())) {
							newlyAddedPrivList.add(pri.getPrivilege() + " - (" + pri.getPrivilegeId() + ")");
						}
					}

					for (Privilege pri : privCol) {
						if (!newPrivMap.containsKey(pri.getPrivilegeId())) {
							newlyRemovedPrivList.add(pri.getPrivilege() + " - (" + pri.getPrivilegeId() + ")");
						}

					}

				} else {
					Iterator<Privilege> itSetPriv = setprivilege.iterator();
					while (itSetPriv.hasNext()) {
						Privilege priv = itSetPriv.next();
						newlyAddedPrivList.add(priv.getPrivilege() + " - (" + priv.getPrivilegeId() + ")");

					}
				}
				// mapCustomContents = new LinkedHashMap<String, String[]>();
				mapCustomContents.put("granted_privileges", newlyAddedPrivList.toArray(new String[newlyAddedPrivList.size()]));
				mapCustomContents
						.put("revoked_privileges", newlyRemovedPrivList.toArray(new String[newlyRemovedPrivList.size()]));

			}
			Set<RoleVisibility> setAgTypes = new HashSet<RoleVisibility>();
			RoleVisibility roleVisibility = new RoleVisibility();
			roleVisibility.setAgentTypeCode(strCrrAgentType);
			setAgTypes.add(roleVisibility);

			if (hasPrivilege(request, "sys.security.role.visible.type") && strAgentType != null) {
				for (int al = 0; al < strAgentType.length; al++) {
					if (!strAgentType[al].equals(strCrrAgentType)) {
						roleVisibility = new RoleVisibility();
						roleVisibility.setAgentTypeCode(strAgentType[al]);
						setAgTypes.add(roleVisibility);
					}
				}
			} else {
				if (strRoleId != null && !"".equals(strRoleId)) {
					String[] noAgent = request.getParameterValues("hdnNoAgent");
					if (noAgent != null) {
						String strNoAgent = noAgent[0];
						StringTokenizer strTok = new StringTokenizer(strNoAgent, ",");
						while (strTok.hasMoreTokens()) {
							String strTempAgent = strTok.nextToken();
							if (!strCrrAgentType.equals(strTempAgent)) {
								roleVisibility = new RoleVisibility();
								roleVisibility.setAgentTypeCode(strTempAgent);
								setAgTypes.add(roleVisibility);
							}
						}
					}
				}
			}
			role.setAgentTypeVisibilities(setAgTypes);

			if (hasPrivilege(request, "sys.security.role.assign.type") && strVisibleType != null) { // change to
																									// accibility
				Set<RoleAccessibility> setAcTypes = new HashSet<RoleAccessibility>();
				RoleAccessibility roleAccessibility = null;
				for (int al = 0; al < strVisibleType.length; al++) {
					roleAccessibility = new RoleAccessibility();
					roleAccessibility.setAgentTypeCode(strVisibleType[al]);
					setAcTypes.add(roleAccessibility);
				}
				role.setAgentTypeAccessiblity(setAcTypes);
			}

		}

		if (strVersion != null && !"".equals(strVersion)) {
			role.setVersion(Long.parseLong(strVersion));
		}

		Role newRole = ModuleServiceLocator.getSecurityBD().saveRole(role, mapCustomContents);
		try {
			if (AppSysParamsUtil.isLCCDataSyncEnabled()) {
				ModuleServiceLocator.getCommonServiceBD().publishSpecificUserDataUpdate(newRole);
			}
		} catch (Exception exp) {
			log.error("Error in realtime LCC sync while saving ROLE");
		}

		setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = true;");
		role = null;
	}

	/**
	 * Method to set request Values to a Property
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return Properties Containing the Roles request Values
	 */
	protected static Properties getProperties(HttpServletRequest request) {

		Properties props = new Properties();

		String strVersion = request.getParameter(PARAM_VERSION);
		String strRoleId = request.getParameter(PARAM_ROLE_ID);
		String strSearchRoleId = request.getParameter(PARAM_ROLE_SEARCH_ID);
		String strRoleName = request.getParameter(PARAM_ROLE_NAME);
		String strRemarks = request.getParameter(PARAM_REMARKS);
		String strAssignPrivileges = request.getParameter(PARAM_ASSIGNED_PRIVILEGES);
		String strMode = request.getParameter(PARAM_MODE);
		String strStatus = request.getParameter(PARAM_STATUS);
		String strRecNo = request.getParameter(PARAM_RECNO);
		String strServiceChannel = request.getParameter(PARAM_SERVICE_CHANNEL);
		String strIncExcType = request.getParameter(PARAM_SEL_INCLUDE_EXCLUDE);
		String strAirlineCode = request.getParameter(PARAM_SEL_AIRLINE_CODE);

		props.setProperty(PARAM_VERSION, AiradminUtils.getNotNullString(strVersion));
		props.setProperty(PARAM_ROLE_ID, AiradminUtils.getNotNullString(strRoleId));
		props.setProperty(PARAM_ROLE_SEARCH_ID, AiradminUtils.getNotNullString(strSearchRoleId));
		props.setProperty(PARAM_ROLE_NAME, AiradminUtils.getNotNullString(strRoleName));
		props.setProperty(PARAM_REMARKS, AiradminUtils.getNotNullString(strRemarks));
		props.setProperty(PARAM_ASSIGNED_PRIVILEGES, AiradminUtils.getNotNullString(strAssignPrivileges));
		props.setProperty(PARAM_MODE, AiradminUtils.getNotNullString(strMode));
		props.setProperty(PARAM_STATUS, (strStatus == null) ? Role.STATUS_ACTIVE : strStatus);
		props.setProperty(PARAM_RECNO, (strRecNo == null) ? "1" : strRecNo);
		props.setProperty(PARAM_SERVICE_CHANNEL, (strServiceChannel == null) ? Constants.INTERNAL_CHANNEL : strServiceChannel);
		props.setProperty(PARAM_AIRLINE_CODE, strAirlineCode == null
				? AppSysParamsUtil.getDefaultAirlineIdentifierCode()
				: strAirlineCode);
		props.setProperty(PARAM_INCLUDE_EXCLUDE, PlatformUtiltiies.nullHandler(strIncExcType));
		return props;
	}

	private static void setErrorFormData(HttpServletRequest req, Properties errProp) {

		String[] strAgentType = req.getParameterValues(PARAM_AGENT_TYPE);
		String[] strAccType = req.getParameterValues(PARAM_ACCESSIBLE_TYPE);
		String strFormData = "var arrFormData = new Array();";
		strFormData += "arrFormData[0] = new Array();";
		strFormData += "arrFormData[0][1] = '" + errProp.getProperty(PARAM_ROLE_ID) + "';";
		strFormData += "arrFormData[0][2] = '" + errProp.getProperty(PARAM_ROLE_NAME) + "';";
		strFormData += "arrFormData[0][3] = '" + errProp.getProperty(PARAM_REMARKS) + "';";
		strFormData += "arrFormData[0][4] = '" + errProp.getProperty(PARAM_STATUS) + "';";
		strFormData += "arrFormData[0][6] = '" + errProp.getProperty(PARAM_VERSION) + "';";
		strFormData += "arrFormData[0][7] = '" + errProp.getProperty(PARAM_ASSIGNED_PRIVILEGES) + "';";
		if (strAgentType != null) {
			strFormData += "arrFormData[0][8] = '" + strAgentType.toString() + "';";
		} else {
			strFormData += "arrFormData[0][8] = '';";
		}
		if (strAccType != null) {
			strFormData += "arrFormData[0][9] = '" + strAccType.toString() + "';";
		} else {
			strFormData += "arrFormData[0][9] = '';";
		}
		strFormData += "arrFormData[0][10] = '" + errProp.getProperty(PARAM_SERVICE_CHANNEL) + "';";

		req.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
	}
}
