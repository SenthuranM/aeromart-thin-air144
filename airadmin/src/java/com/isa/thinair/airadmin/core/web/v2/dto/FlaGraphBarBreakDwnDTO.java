package com.isa.thinair.airadmin.core.web.v2.dto;

import java.io.Serializable;

public class FlaGraphBarBreakDwnDTO implements Serializable {

	private static final long serialVersionUID = 7121452749312292027L;

	private String agentName;
	private String agentCode;
	private String pnr;
	private String totCharges;
	private String totPayments;

	public String getAgentName() {
		return agentName;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public String getPnr() {
		return pnr;
	}

	public String getTotCharges() {
		return totCharges;
	}

	public String getTotPayments() {
		return totPayments;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public void setTotCharges(String totCharges) {
		this.totCharges = totCharges;
	}

	public void setTotPayments(String totPayments) {
		this.totPayments = totPayments;
	}

}
