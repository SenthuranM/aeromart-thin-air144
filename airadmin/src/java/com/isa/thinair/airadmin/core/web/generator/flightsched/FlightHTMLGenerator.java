package com.isa.thinair.airadmin.core.web.generator.flightsched;

import com.isa.thinair.airadmin.core.service.AiradminModuleConfig;
import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airadmin.core.web.util.FlightUtils;
import com.isa.thinair.airadmin.core.web.util.ScheduleUtils;
import com.isa.thinair.airmaster.api.model.AirportTerminal;
import com.isa.thinair.airmaster.api.model.Gds;
import com.isa.thinair.airschedules.api.dto.DisplayFlightDTO;
import com.isa.thinair.airschedules.api.dto.FlightSegmentResInvSummaryDTO;
import com.isa.thinair.airschedules.api.dto.FlightSummaryDTO;
import com.isa.thinair.airschedules.api.model.CodeShareMCFlight;
import com.isa.thinair.airschedules.api.model.Flight;
import com.isa.thinair.airschedules.api.model.FlightLeg;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.airschedules.api.utils.FlightUtil;
import com.isa.thinair.airschedules.api.utils.ModelNamesAndFieldNames;
import com.isa.thinair.airschedules.api.utils.SegmentUtil;
import com.isa.thinair.commons.api.constants.DayOfWeek;
import com.isa.thinair.commons.api.dto.ModuleCriteria;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.StaticFileNameUtil;
import com.isa.thinair.webplatform.api.util.CommonUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.http.HttpServletRequest;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.TimeZone;

/**
 * @author Thushara
 * 
 */
public class FlightHTMLGenerator {
	private static Log log = LogFactory.getLog(FlightHTMLGenerator.class);
	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();
	private static AiradminModuleConfig adminConfig = AiradminModuleUtils.getConfig();
	private static final String PARAM_TIMEMODE = "hdnTimeMode";
	private static final String PARAM_LEGS = "strLegs";
	private static final String PARAM_FRMPAGE = "hdnFromPage";
	private static final String PARAM_SEARCH_CRITERIA = "searchCriteria";
	private static final String defaultFormat // get the default date format
	= globalConfig.getBizParam(SystemParamKeys.DATE_FORMAT_FOR_DATE_STRINGS);

	private static final String strSMDateFormat = "dd/MM/yyyy";
	private static String clientErrors;
	String strTimeMode = "";

	/**
	 * Gets the Flight Row For the Search Critireia
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Flight Data String
	 * @throws ModuleException
	 *             the ModuleException
	 */
	@SuppressWarnings("unchecked")
	public final String getFlightRowHtml(HttpServletRequest request) throws ModuleException {
		String strMode = request.getParameter("hdnMode");
		strTimeMode = request.getParameter(PARAM_TIMEMODE) == null ? "" : request.getParameter(PARAM_TIMEMODE);
		String strFromPage = request.getParameter(PARAM_FRMPAGE);

		Page page = null;
		int recNo = 0;
		int totRec = 0;
		Collection<DisplayFlightDTO> flightCol = null;
		boolean localtime = false;

		if (request.getParameter("hdnRecNo") == null) {
			recNo = 1;
		} else if (!("".equals(request.getParameter("hdnRecNo")))) {
			recNo = Integer.parseInt(request.getParameter("hdnRecNo"));
		}
		recNo = recNo - 1;

		// get the default operation type to search
		String defaultOperationType = globalConfig.getBizParam(SystemParamKeys.DEFAULT_OPERATION_TYPE);
		// get the default schedule status to search
		String defaultStatus = globalConfig.getBizParam(SystemParamKeys.DEFAULT_FLIGHT_STATUS);
		// get the default minimum time allowed for amendments
		String minimumTimeAllowdForSchedule = globalConfig.getBizParam(SystemParamKeys.MINIMUM_TIME_ALLOWED_FOR_SCHEDULE);
		// get the default minimum time allowed for amendments
		String strDefaultClassofService = globalConfig.getBizParam(SystemParamKeys.CLASS_OF_SERVICE);

		Boolean hideLogicalCC = !AppSysParamsUtil.isLogicalCabinClassEnabled();

		Boolean hideWaitListing = !AppSysParamsUtil.isWaitListedReservationsEnabled();

		request.setAttribute(WebConstants.REQ_HTML_DEFAULT_OPERATIONTYPE, defaultOperationType);
		request.setAttribute(WebConstants.REQ_HTML_DEFAULT_FLIGHTSTATUS, defaultStatus);
		request.setAttribute(WebConstants.REQ_MINIMUM_TIME_ALLOWEDFORSCHEDULE, minimumTimeAllowdForSchedule);
		request.setAttribute(WebConstants.REQ_HTML_DEFAULT_CLASSOFSERVICE, strDefaultClassofService);
		request.setAttribute(WebConstants.REQ_HTML_HIDE_LOGICAL_CC_DETAILS, hideLogicalCC);
		request.setAttribute(WebConstants.REQ_HTML_HIDE_WAIT_LISTING_DETAILS, hideWaitListing);

		String strStartDate = "";
		String strStopDate = "";
		String strFlightNo = "";
		String strFlightStartNo = "";
		String strFrom = "";
		String strTo = "";
		String strOperationtype = "";
		String strStatus = "";
		String strScheduleId = "";
		String searchCriteria = null;
		String[] searchParameters = null;
		String strFlightType = "";
		boolean defaultSearch = false;
		// String strStartTime = request.getParameter("hdnStartTime");
		// String strEndTime = request.getParameter("hdnEndTime");
		String strFrequency = request.getParameter("hdnFrequency");
		String strScheduledOnly = request.getParameter("hdnIncludeScheduled");
		strStatus = request.getParameter("selStatusSearch");

		// Default search values
		Date dStartDate = null;
		Date dStopDate = null;
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

		if (strMode == null) {
			defaultSearch = true;
		}

		if (!defaultSearch) {
			if ((strFromPage != null)
					&& (strFromPage.trim().equalsIgnoreCase("SCHEDULE") || strFromPage.trim().equalsIgnoreCase("MANAGE"))) {
				strScheduleId = request.getParameter("hdnScheduleId");
			} else {
				// Called from seat allocation - back button click
				if ((strFromPage != null) && (strFromPage.trim().equalsIgnoreCase("INVENTORY"))) {
					searchCriteria = request.getParameter(PARAM_SEARCH_CRITERIA);
					if (searchCriteria != null) {
						searchParameters = searchCriteria.split(",");
						if (searchParameters != null) {
							strStartDate = searchParameters[0];
							strStopDate = searchParameters[1];
							strFlightNo = searchParameters[2];
							strFrom = searchParameters[3];
							strTo = searchParameters[4];
							strOperationtype = searchParameters[5];
							strStatus = searchParameters[6];
							strFlightStartNo = searchParameters[8];
							strScheduledOnly = searchParameters[9];

						}
					}
					if ((searchCriteria == null) || (searchParameters == null)) {
						defaultSearch = true;
					}
				} else {
					// Called from flight - search button click
					strStartDate = request.getParameter("hdnStartDateSearch");
					strStopDate = request.getParameter("hdnStopDateSearch");
					strFlightNo = request.getParameter("hdnFlightNoSearch");
					strFlightStartNo = request.getParameter("hdnFlightNoStartSearch");

					strOperationtype = request.getParameter("hdnSelOperationTypeSearch");
					strFrom = request.getParameter("hdnSelFromStn6");
					strTo = request.getParameter("hdnSelToStn6");
					strStatus = request.getParameter("hdnSelStatusSearch");
					strFlightType = request.getParameter("selFlightTypeSearch");
				}
				try {
					dStartDate = formatter.parse(strStartDate);
					dStopDate = formatter.parse(strStopDate);
					Calendar cal = Calendar.getInstance();
					cal.setTime(dStartDate);
					dStartDate = cal.getTime();
					cal.setTime(dStopDate);
					dStopDate = cal.getTime();

					// Set Start date

				} catch (Exception e) {
					log.error("Error in getFlightRowHtml(HttpServletRequest request)" + " - Fromat Setted Date" + e.getMessage());
					defaultSearch = true;
				}
				if (strFlightNo != null) {
					strFlightNo = strFlightNo.trim();
				}
			}
		}

		/*
		 * if (defaultSearch) { // Called from flight - initial load or default search strStartDate = ""; strStopDate =
		 * ""; strFlightNo = ""; strFlightStartNo = ""; strFrom = ""; strTo = ""; strOperationtype = ""; strStatus = "";
		 * strScheduleId = ""; dStartDate = null; dStopDate = null; try { Calendar cal = Calendar.getInstance();
		 * dStartDate = cal.getTime(); // Current date cal.add(Calendar.MONTH,1); dStopDate = cal.getTime(); //Next
		 * Month Date
		 * 
		 * String fromDate = formatter.format(dStartDate); String toDate = formatter.format(dStopDate);
		 * 
		 * Calendar calCurrent = Calendar.getInstance(); calCurrent.setTime(formatter.parse(fromDate)); dStartDate =
		 * calCurrent.getTime(); //Set Start date calCurrent.setTime(formatter.parse(toDate)); dStopDate =
		 * calCurrent.getTime(); //Set Stop date } catch (Exception e) {
		 * log.error("Error in getFlightRowHtml(HttpServletRequest request)" + " - Fromat Current Date" +
		 * e.getMessage()); } strOperationtype = defaultOperationType; //System Parameter strStatus = defaultStatus; }
		 */

		if (!defaultSearch) {

			// setting the criteria list to search
			List<ModuleCriterion> critrian = new ArrayList<ModuleCriterion>();
			// setting the start date as criteria
			if (dStartDate != null) {

				ModuleCriterion moduleCriterionStartD = new ModuleCriterion();

				moduleCriterionStartD.setCondition(ModuleCriterion.CONDITION_GREATER_THAN_OR_EQUALS);
				// start date of the schedule
				moduleCriterionStartD.setFieldName(ModelNamesAndFieldNames.Flight.FieldNames.DEPARTURE_DATE);

				List<Date> valueStartDate = new ArrayList<Date>();
				valueStartDate.add(dStartDate);

				moduleCriterionStartD.setValue(valueStartDate);

				critrian.add(moduleCriterionStartD);
			}
			// setting the stop date as criteria
			if (dStopDate != null) {

				ModuleCriterion moduleCriterionStopD = new ModuleCriterion();

				moduleCriterionStopD.setCondition(ModuleCriterion.CONDITION_LESS_THAN_OR_EQUALS);
				// stop date of the schedule
				moduleCriterionStopD.setFieldName(ModelNamesAndFieldNames.Flight.FieldNames.DEPARTURE_DATE);

				List<Date> valueStopDate = new ArrayList<Date>();
				valueStopDate.add(dStopDate);

				moduleCriterionStopD.setValue(valueStopDate);

				critrian.add(moduleCriterionStopD);
			}
			// setting the operation type as criteria
			if (ScheduleUtils.isNotEmptyOrNull(strOperationtype)) {

				ModuleCriterion moduleCriterionOptType = new ModuleCriterion();

				moduleCriterionOptType.setCondition(ModuleCriterion.CONDITION_EQUALS);
				// operation type of the flight
				moduleCriterionOptType.setFieldName(ModelNamesAndFieldNames.Flight.FieldNames.OPERATION_TYPE_ID);

				List<Integer> valueOptType = new ArrayList<Integer>();
				valueOptType.add(new Integer(strOperationtype));

				moduleCriterionOptType.setValue(valueOptType);

				critrian.add(moduleCriterionOptType);
			}
			if (strFromPage != null
					&& (strFromPage.trim().equalsIgnoreCase("MANAGE") || strFromPage.trim().equalsIgnoreCase("SCHEDULE"))) {
				strStatus = "All";
			}
			// setting the schedule status as criteria
			if (ScheduleUtils.isNotEmptyOrNull(strStatus)) {
				ModuleCriterion moduleCriterionStatus = new ModuleCriterion();
				moduleCriterionStatus.setCondition(ModuleCriterion.CONDITION_EQUALS);
				// status code of the flight
				moduleCriterionStatus.setFieldName(ModelNamesAndFieldNames.Flight.FieldNames.STATUS);
				List<String> valueStatus = new ArrayList<String>();
				valueStatus.add(strStatus);

				moduleCriterionStatus.setValue(valueStatus);
				critrian.add(moduleCriterionStatus);
			}

			// setting flight type as a criteria
			if (ScheduleUtils.isNotEmptyOrNull(strFlightType)) {
				ModuleCriterion moduleCriterionStatus = new ModuleCriterion();
				moduleCriterionStatus.setCondition(ModuleCriterion.CONDITION_EQUALS);
				// status code of the flight
				moduleCriterionStatus.setFieldName(ModelNamesAndFieldNames.Flight.FieldNames.FLIGHT_TYPE);
				List<String> valueStatus = new ArrayList<String>();
				valueStatus.add(strFlightType);

				moduleCriterionStatus.setValue(valueStatus);
				critrian.add(moduleCriterionStatus);
			}

			// setting the flight number as criteria
			if (ScheduleUtils.isNotEmptyOrNull(strFlightNo) || ScheduleUtils.isNotEmptyOrNull(strFlightStartNo)) {
				CommonUtil.checkCarrierCodeAccessibility(AiradminUtils.getUserCarrierCodes(request), strFlightStartNo);
				ModuleCriterion moduleCriterionFlightNo = new ModuleCriterion();
				moduleCriterionFlightNo.setCondition(ModuleCriterion.CONDITION_LIKE);
				// flight number of the flight
				moduleCriterionFlightNo.setFieldName(ModelNamesAndFieldNames.Flight.FieldNames.FLIGHT_NUMBER);

				List<String> valueFlightNo = new ArrayList<String>();
				String flightNumber = strFlightStartNo + strFlightNo;
				valueFlightNo.add(flightNumber != null ? (flightNumber.toUpperCase() + "%") : "");

				moduleCriterionFlightNo.setValue(valueFlightNo);
				critrian.add(moduleCriterionFlightNo);
			} else {
				Collection<String> strCarrierCodes = AiradminUtils.getUserCarrierCodes(request);
				if (strCarrierCodes.size() == 1) {
					ModuleCriterion moduleCriterionFlightNo = new ModuleCriterion();
					moduleCriterionFlightNo.setCondition(ModuleCriterion.CONDITION_LIKE);
					// flight number of the flight
					moduleCriterionFlightNo.setFieldName(ModelNamesAndFieldNames.Flight.FieldNames.FLIGHT_NUMBER);

					List<String> valueFlightNo = new ArrayList<String>();
					String ccode = "";
					for (String carCode : strCarrierCodes) {
						ccode = carCode;
						break;
					}

					valueFlightNo.add(ccode.toUpperCase() + "%");

					moduleCriterionFlightNo.setValue(valueFlightNo);
					critrian.add(moduleCriterionFlightNo);

				} else {
					ModuleCriteria moduleCriterionFlightNo = new ModuleCriteria();
					moduleCriterionFlightNo.setFieldName(ModelNamesAndFieldNames.Flight.FieldNames.FLIGHT_NUMBER);
					List<ModuleCriterion> moduleCriterions = new ArrayList<ModuleCriterion>();
					for (String strCC : strCarrierCodes) {
						ModuleCriterion m = new ModuleCriterion();
						m.setCondition(ModuleCriterion.CONDITION_LIKE);
						m.setFieldName(ModelNamesAndFieldNames.Flight.FieldNames.FLIGHT_NUMBER);

						List<String> valueFlightNo = new ArrayList<String>();
						valueFlightNo.add(strCC.toUpperCase() + "%");
						m.setValue(valueFlightNo);
						moduleCriterions.add(m);
					}
					moduleCriterionFlightNo.setLeaf(false);
					moduleCriterionFlightNo.setCondition(ModuleCriteria.JOIN_CONDITION_OR);
					moduleCriterionFlightNo.setModulecriterionList(moduleCriterions);
					critrian.add(moduleCriterionFlightNo);
				}

			}
			// setting the departure airport code as criteria
			if (ScheduleUtils.isNotEmptyOrNull(strFrom)) {
				ModuleCriterion moduleCriterionFromStn = new ModuleCriterion();
				moduleCriterionFromStn.setCondition(ModuleCriterion.CONDITION_EQUALS);
				// departure airport code of the flight
				moduleCriterionFromStn.setFieldName(ModelNamesAndFieldNames.Flight.FieldNames.ORIGIN_APT_CODE);

				List<String> valueFromStn = new ArrayList<String>();
				valueFromStn.add(strFrom);

				moduleCriterionFromStn.setValue(valueFromStn);
				critrian.add(moduleCriterionFromStn);
			}
			// setting the arrival airport code as criteria
			if (ScheduleUtils.isNotEmptyOrNull(strTo)) {
				ModuleCriterion moduleCriterionToStn = new ModuleCriterion();
				moduleCriterionToStn.setCondition(ModuleCriterion.CONDITION_EQUALS);
				// arrival airport code of the schedule
				moduleCriterionToStn.setFieldName(ModelNamesAndFieldNames.Flight.FieldNames.DESTINATION_APT_CODE);

				List<String> valueToStn = new ArrayList<String>();
				valueToStn.add(strTo);

				moduleCriterionToStn.setValue(valueToStn);

				critrian.add(moduleCriterionToStn);
			}

			// setting the opt flight frequency
			if (ScheduleUtils.isNotEmptyOrNull(strFrequency)) {
				String[] flightDays;
				String strSunday = null;
				String strMonday = null;
				String strTuesday = null;
				String strWednesday = null;
				String strThursday = null;
				String strFriday = null;
				String strSaturday = null;
				if (strFrequency != null) {
					flightDays = strFrequency.split(",");
					if (flightDays != null) {
						strSunday = flightDays[0];
						strMonday = flightDays[1];
						strTuesday = flightDays[2];
						strWednesday = flightDays[3];
						strThursday = flightDays[4];
						strFriday = flightDays[5];
						strSaturday = flightDays[6];
					}
				}

				Collection<DayOfWeek> daysCol = new ArrayList<DayOfWeek>();
				if (strSunday != null && strSunday.equals("true")) {
					daysCol.add(DayOfWeek.SUNDAY);
				}
				if (strMonday != null && strMonday.equals("true")) {
					daysCol.add(DayOfWeek.MONDAY);
				}

				if (strTuesday != null && strTuesday.equals("true")) {
					daysCol.add(DayOfWeek.TUESDAY);
				}

				if (strWednesday != null && strWednesday.equals("true")) {
					daysCol.add(DayOfWeek.WEDNESDAY);
				}

				if (strThursday != null && strThursday.equals("true")) {
					daysCol.add(DayOfWeek.THURSDAY);
				}

				if (strFriday != null && strFriday.equals("true")) {
					daysCol.add(DayOfWeek.FRIDAY);
				}

				if (strSaturday != null && strSaturday.equals("true")) {
					daysCol.add(DayOfWeek.SATURDAY);
				}

				if (daysCol != null && daysCol.size() > 0) {
					ModuleCriterion moduleCriterionFrequency = new ModuleCriterion();
					moduleCriterionFrequency.setCondition(ModuleCriterion.CONDITION_IN);
					// arrival airport code of the schedule
					moduleCriterionFrequency.setFieldName(ModelNamesAndFieldNames.Flight.FieldNames.DAY_NUMBER);

					List<Integer> valueFrequency = new ArrayList<Integer>();
					Iterator<DayOfWeek> days = daysCol.iterator();
					while (days.hasNext()) {

						DayOfWeek day = (DayOfWeek) days.next();
						Integer dayNumber = (Integer) CalendarUtil.getDayNumber(day);
						if (dayNumber == 0) {
							valueFrequency.add(dayNumber);
						}
						if (dayNumber == 1) {
							valueFrequency.add(dayNumber);
						}
						if (dayNumber == 2) {
							valueFrequency.add(dayNumber);
						}
						if (dayNumber == 3) {
							valueFrequency.add(dayNumber);
						}
						if (dayNumber == 4) {
							valueFrequency.add(dayNumber);
						}
						if (dayNumber == 5) {
							valueFrequency.add(dayNumber);
						}
						if (dayNumber == 6) {
							valueFrequency.add(dayNumber);
						}

					}
					moduleCriterionFrequency.setValue(valueFrequency);
					critrian.add(moduleCriterionFrequency);
				}
			}

			if (ScheduleUtils.isNotEmptyOrNull(strScheduledOnly) && !strScheduledOnly.equals("null")
					&& !strScheduledOnly.equals("undefined")) {
				if (strScheduledOnly.equals("true")) {
					ModuleCriterion moduleCriterionSchedId = new ModuleCriterion();
					moduleCriterionSchedId.setCondition(ModuleCriterion.CONDITION_IS_NOT_NULL);
					moduleCriterionSchedId.setFieldName(ModelNamesAndFieldNames.Flight.FieldNames.SCHEDULE_ID);
					critrian.add(moduleCriterionSchedId);
				}
			}

			// setting the schedule id as criteria
			if (ScheduleUtils.isNotEmptyOrNull(strScheduleId)) {
				if (!strScheduleId.equals("null") && !strScheduleId.equals("undefined")) {
					ModuleCriterion moduleCriterionSchedId = new ModuleCriterion();
					moduleCriterionSchedId.setCondition(ModuleCriterion.CONDITION_EQUALS);
					// arrival airport code of the schedule
					moduleCriterionSchedId.setFieldName(ModelNamesAndFieldNames.Flight.FieldNames.SCHEDULE_ID);

					List<Integer> valueSchedId = new ArrayList<Integer>();
					valueSchedId.add(new Integer(strScheduleId));

					moduleCriterionSchedId.setValue(valueSchedId);

					critrian.add(moduleCriterionSchedId);
				}

				// if manage flight
				if (strFromPage != null && strFromPage.trim().equalsIgnoreCase("MANAGE")) {
					Date sysDate = CalendarUtil.getCurrentSystemTimeInZulu(); // set start date as current zulu date
					String strMngStartDate = request.getParameter("txtStartDateSearch");
					try {
						dStartDate = formatter.parse(strMngStartDate);
						Calendar cal = Calendar.getInstance();
						cal.setTime(dStartDate);
						dStartDate = cal.getTime();
					} catch (ParseException e) {
						dStartDate = sysDate;
					} catch (NullPointerException ex) {
						dStartDate = sysDate;
					}

					if (dStartDate.compareTo(sysDate) < 0) {
						dStartDate = sysDate;
					}

					if (dStartDate != null) {

						ModuleCriterion moduleCriterionStartD = new ModuleCriterion();

						moduleCriterionStartD.setCondition(ModuleCriterion.CONDITION_GREATER_THAN_OR_EQUALS);
						// start date of the schedule
						moduleCriterionStartD.setFieldName(ModelNamesAndFieldNames.Flight.FieldNames.DEPARTURE_DATE);

						List<Date> valueStartDate = new ArrayList<Date>();
						valueStartDate.add(dStartDate);

						moduleCriterionStartD.setValue(valueStartDate);

						critrian.add(moduleCriterionStartD);
					}
					String strStpDate = request.getParameter("txtStopDateSearch");
					try {
						dStopDate = formatter.parse(strStpDate);
						Calendar cal = Calendar.getInstance();
						cal.setTime(dStartDate);
						dStartDate = cal.getTime();
						cal.setTime(dStopDate);
						dStopDate = cal.getTime();
					} catch (ParseException e) {
						dStopDate = null;
					} catch (NullPointerException ex) {
						dStopDate = null;
					}

					if (dStopDate != null) {

						ModuleCriterion moduleCriterionStopD = new ModuleCriterion();
						moduleCriterionStopD.setCondition(ModuleCriterion.CONDITION_LESS_THAN_OR_EQUALS);
						moduleCriterionStopD.setFieldName(ModelNamesAndFieldNames.Flight.FieldNames.DEPARTURE_DATE);
						List<Date> valueStopDate = new ArrayList<Date>();
						valueStopDate.add(dStopDate);
						moduleCriterionStopD.setValue(valueStopDate);
						critrian.add(moduleCriterionStopD);
					}
				}

			}

			page = ModuleServiceLocator.getFlightServiceBD().searchFlightsForDisplay(critrian, recNo, 20, localtime, null, true, false, null);

			// get the page data
			if (page != null) {
				flightCol = page.getPageData();
				totRec = page.getTotalNoOfRecords();

			}
		}
		request.setAttribute(WebConstants.REQ_TOTAL_RECORDS, new Integer(totRec).toString());
		log.debug("inside ScheduleHTMLGenerator.getScheduleRowHtml search values");

		return createFlightRowHTML(flightCol, getFrequencyList(request));

	}

	/**
	 * 
	 * @param request
	 * @return
	 */
	private static Collection<String> getFrequencyList(HttpServletRequest request) {
		Collection<String> arrFr = new ArrayList<String>();

		if (request.getParameter("chkAll") != null && request.getParameter("chkAll").equalsIgnoreCase("on")) {
			arrFr.add("ALL");
		} else {

			if (request.getParameter("chkSunday") != null && request.getParameter("chkSunday").equalsIgnoreCase("on")) {
				arrFr.add("Sun");
			}
			if (request.getParameter("chkMonday") != null && request.getParameter("chkMonday").equalsIgnoreCase("on")) {
				arrFr.add("Mon");
			}
			if (request.getParameter("chkTuesday") != null && request.getParameter("chkTuesday").equalsIgnoreCase("on")) {
				arrFr.add("Tue");
			}
			if (request.getParameter("chkWednesday") != null && request.getParameter("chkWednesday").equalsIgnoreCase("on")) {
				arrFr.add("Wed");
			}
			if (request.getParameter("chkThursday") != null && request.getParameter("chkThursday").equalsIgnoreCase("on")) {
				arrFr.add("Thu");
			}
			if (request.getParameter("chkFriday") != null && request.getParameter("chkFriday").equalsIgnoreCase("on")) {
				arrFr.add("Fri");
			}
			if (request.getParameter("chkSaturday") != null && request.getParameter("chkSaturday").equalsIgnoreCase("on")) {
				arrFr.add("Sat");
			}

		}
		return arrFr;
	}

	/**
	 * Creates the Flight Data Grid Rows
	 * 
	 * @param flights
	 *            the Collection of Flights
	 * @return String the Created Flight Grid Row Array
	 */
	private String createFlightRowHTML(Collection<DisplayFlightDTO> flights, Collection<String> freq) throws ModuleException {

		String sendEmailsForFlightAlterations = globalConfig.getBizParam(SystemParamKeys.SEND_EMAILS_FOR_FLIGHT_ALTERATIONS)
				.equalsIgnoreCase("Y") ? "Y" : "N";
		String sendSMSForFlightAlterations = globalConfig.getBizParam(SystemParamKeys.SEND_SMS_FOR_FLIGHT_ALTERATIONS)
				.equalsIgnoreCase("Y") ? "Y" : "N";

		Iterator<DisplayFlightDTO> iter = null;
		DisplayFlightDTO flightDTO = null;
		StringBuffer sb = new StringBuffer();
		String arrZulu = null;
		String depZulu = null;
		String arrLoc = null;
		String depLoc = null;
		int i = 0;
		Date currentDate = new Date();

		if (flights != null) {
			iter = flights.iterator();
		}
		if (iter != null) {
			/*
			 * //Haider 04Sep08 get a hasmap with schedule gds ids for each flight List flightIds =
			 * FlightUtils.getFlightIds(flights); HashMap gdsIdsMap = null; if(flightIds!=null){ gdsIdsMap =
			 * flightDelegate.getScheduleGDSIdsForFlight(flightIds); }
			 */
			while (iter.hasNext()) {

				flightDTO = (DisplayFlightDTO) iter.next();

				Flight flight = flightDTO.getFlight();
				String strStart = "";
				String strStartZulu = "";
				arrZulu = "";
				depZulu = "";
				arrLoc = "";
				depLoc = "";

				Set<FlightSegement> segSet = flight.getFlightSegements();
				Collection<FlightLeg> legCol = flight.getFlightLegs();
				FlightLeg firstLeg = FlightUtils.getFirstFlightLeg(legCol);
				FlightLeg lastLeg = FlightUtils.getLastFlightLeg(legCol);
				boolean isPassedDepartureDate = isPassedDepartureTime(segSet);
				// uncomment the condition if you want from one time mode
				// if (strTimeMode.equals(WebConstants.LOCALTIME)) {
				strStart = FlightUtils.formatDate(flight.getDepartureDateLocal(), "dd/MM/yyyy");
				// }else {
				strStartZulu = FlightUtils.formatDate(flight.getDepartureDate(), "dd/MM/yyyy");
				// }
				String strDay = FlightUtils.formatDate(flight.getDepartureDate(), "EEE");
				boolean isInFreq = true;
				if (strDay != null && !freq.contains("ALL") && freq.size() > 0) {
					isInFreq = freq.contains(strDay);
				}
				if (isInFreq) {
					sb.append("flightData[" + i + "] = new Array();");
					sb.append("flightData[" + i + "][1] = '" + flight.getFlightNumber() + "';");
					sb.append("flightData[" + i + "][2] = '" + strStartZulu + "';");
					sb.append("flightData[" + i + "][22] = '" + strStart + "';");
					sb.append("flightData[" + i + "][3] = '" + flight.getOriginAptCode() + "';");
					sb.append("flightData[" + i + "][4] = '" + flight.getDestinationAptCode() + "';");
					sb.append("flightData[" + i + "][5] = '" + FlightUtils.getSegment(segSet) + "';");
					// uncomment if want the condition
					/*
					 * if(firstLeg != null && strTimeMode.equals(WebConstants.LOCALTIME)) { strArr += "flightData[" + i
					 * + "][6] = '"+ ScheduleUtils .formatDate(firstLeg.getEstDepartureTimeLocal(),defaultFormat) +"';"
					 * ; //etd strArr += "flightData[" + i + "][7] = '"+ ScheduleUtils
					 * .formatDate(lastLeg.getEstArrivalTimeLocal(), defaultFormat) +"';" ;//eta }else
					 */if (firstLeg != null) {
						sb.append("flightData[" + i + "][6] = '"
								+ ScheduleUtils.formatDate(firstLeg.getEstDepartureTimeZulu(), defaultFormat) + "';"); // etd
																														// -
																														// zulu
						sb.append("flightData[" + i + "][7] = '"
								+ ScheduleUtils.formatDate(lastLeg.getEstArrivalTimeZulu(), defaultFormat) + "';");// eta
						sb.append("flightData[" + i + "][24] =  '"
								+ ScheduleUtils.formatDate(firstLeg.getEstDepartureTimeLocal(), defaultFormat) + "';"); // etd
																														// -
																														// local
					} else {
						sb.append("flightData[" + i + "][6] = '&nbsp';");
						sb.append("flightData[" + i + "][7] = '&nbsp';");
					}
					sb.append("flightData[" + i + "][8] = '" + flight.getOperationTypeId() + "';");
					if (flight.getOverlapingFlightId() != null) {
						sb.append("flightData[" + i + "][9] = '" + flight.getOverlapingFlightId().intValue() + "';");
					} else {
						sb.append("flightData[" + i + "][9] = '&nbsp';");
					}

					sb.append("flightData[" + i + "][10] = '" + flightDTO.getAllocated() + "';");
					sb.append("flightData[" + i + "][11] = '" + flightDTO.getSeatsSold() + "';"); // no of seat sold
					sb.append("flightData[" + i + "][12] = '" + flightDTO.getOnHold() + "';"); // no of on hold
					sb.append("flightData[" + i + "][13] = '" + flight.getStatus() + "';");
					if (flight.getAvailableSeatKilometers() != null) {
						sb.append("flightData[" + i + "][14] = '" + flight.getAvailableSeatKilometers().intValue() / 1000 + "';");
					} else {
						sb.append("flightData[" + i + "][14] = '&nbsp';");
					}

					if (flight.getScheduleId() != null) {
						sb.append("flightData[" + i + "][15] = '" + flight.getScheduleId() + "';");
						
						int numberOfFlights = ModuleServiceLocator.getScheduleServiceBD().getNoOfFlightsForSchedule(flight.getScheduleId());
						sb.append("flightData[" + i + "][47] = '" + numberOfFlights + "';");
														
					} else {
						sb.append("flightData[" + i + "][15] = '&nbsp';");
					}

					sb.append("flightData[" + i + "][16] = '" + flight.getFlightId() + "';");
					sb.append("flightData[" + i + "][17] = '" + flight.getModelNumber() + "';");
					sb.append(setLegs(legCol, false, flight));
					sb.append("flightData[" + i + "][18] =  arrLegDtls;");
					sb.append(setLegs(legCol, true, flight));
					sb.append("flightData[" + i + "][23] =  arrLegDtls;");
					sb.append("flightData[" + i + "][19] = '" + flight.getManuallyChanged() + "';");
					sb.append("flightData[" + i + "][20] = '" + flight.getVersion() + "';");
					sb.append("flightData[" + i + "][21] = '" + flightDTO.getReservatinsExists() + "';");

					// Used for the integration of seat allocation
					arrZulu = FlightUtils.formatDate(FlightUtils.getDetailsOfLongestSegment(segSet).getEstTimeArrivalZulu(),
							"dd-MM-yyyy HH:mm");
					sb.append("flightData[" + i + "][25] = '" + arrZulu + "';");
					depZulu = FlightUtils.formatDate(FlightUtils.getDetailsOfLongestSegment(segSet).getEstTimeDepatureZulu(),
							"dd-MM-yyyy HH:mm");
					sb.append("flightData[" + i + "][26] = '" + depZulu + "';");
					sb.append(FlightUtils.createSegmentArr(segSet));
					sb.append("flightData[" + i + "][27] = arrSegDtls;");

					// setting a local time
					arrLoc = FlightUtils.formatDate(FlightUtils.getDetailsOfLongestSegment(segSet).getEstTimeArrivalLocal(),
							"dd-MM-yyyy HH:mm");
					sb.append("flightData[" + i + "][28] = '" + arrLoc + "';");
					depLoc = FlightUtils.formatDate(FlightUtils.getDetailsOfLongestSegment(segSet).getEstTimeDepatureLocal(),
							"dd-MM-yyyy HH:mm");
					sb.append("flightData[" + i + "][29] = '" + depLoc + "';");

					int intCompareDate = currentDate.compareTo(flight.getDepartureDate());
					if (intCompareDate > 0 || flight.getStatus().equals("CNX") || flight.getStatus().equals("CLS")) {
						sb.append("flightData[" + i + "][30] = 'N';"); // Cannot be edited
					} else {
						sb.append("flightData[" + i + "][30] = 'Y';"); // Can be edited
					}

					// Haider send flight gds ids
					Set<Integer> gdsIds = flight.getGdsIds();
					if (gdsIds != null) {
						Object[] ids = gdsIds.toArray();
						String strIds = "";
						for (int id = 0; id < ids.length; id++) {
							if (strIds.length() > 0)
								strIds += ",";
							strIds += ids[id];
						}
						sb.append("flightData[" + i + "][31] = '" + strIds + "';");
					}
					String imagePath = "../../images/";
					if (flight.getStatus().equals("ACT")) {
						sb.append("flightData[" + i + "][32] = '" + imagePath + StaticFileNameUtil.getCorrected("act_status.gif")
								+ "';"); // Can be edited
					} else if (flight.getStatus().equals("CLS")) {
						sb.append("flightData[" + i + "][32] = '" + imagePath + StaticFileNameUtil.getCorrected("cls_status.gif")
								+ "';");
					} else if (flight.getStatus().equals("CNX")) {
						sb.append("flightData[" + i + "][32] = '" + imagePath + StaticFileNameUtil.getCorrected("cnx_status.gif")
								+ "';");
					} else if (flight.getStatus().equals("CRE")) {
						sb.append("flightData[" + i + "][32] = '" + imagePath + StaticFileNameUtil.getCorrected("cre_status.gif")
								+ "';");
					} else {
						sb.append("flightData[" + i + "][32] = '" + imagePath + StaticFileNameUtil.getCorrected("Warn.gif")
								+ "';");
					}

					sb.append("flightData[" + i + "][33] = 'false';");
					sb.append("flightData[" + i + "][34] = '" + isPassedDepartureDate + "';");
					sb.append("flightData[" + i + "][35] = '" + FlightUtils.getTerminal(segSet) + "';");
					sb.append("flightData[" + i + "][36] = '" + flight.getFlightType() + "';");

					sb.append("flightData[" + i + "][37] = '" + FlightUtil.getFlightTypeDescription(flight.getFlightType())
							+ "';");
					if (flight.getRemarks() != null) {
						sb.append("flightData[" + i + "][38] = '" + flight.getRemarks() + "';");
					} else {
						sb.append("flightData[" + i + "][38] = '';");
					}

					if (flight.getMealTemplateId() != null) {
						sb.append("flightData[" + i + "][39] = '" + flight.getMealTemplateId() + "';");
					} else {
						sb.append("flightData[" + i + "][39] = '';");
					}

					if (flight.getSeatChargeTemplateId() != null) {
						sb.append("flightData[" + i + "][40] = '" + flight.getSeatChargeTemplateId() + "';");
					} else {
						sb.append("flightData[" + i + "][40] = '';");
					}
					if (flight.getUserNotes() != null) {
						sb.append("flightData[" + i + "][41] = '" + flight.getUserNotes() + "';");
					} else {
						sb.append("flightData[" + i + "][41] = '';");
					}
					sb.append("flightData[" + i + "][42] = '" + isEligibleForActivatePastFlights(segSet) + "';");
					sb.append("flightData[" + i + "][43] = '" + flight.getModelDesc() + "';");
					

					if (flight.getCsOCCarrierCode() != null) {
						sb.append("flightData[" + i + "][44] = '" + flight.getCsOCCarrierCode() + "';");
					} else {
						sb.append("flightData[" + i + "][44] = '';");
					}
					if (flight.getCsOCFlightNumber() != null) {
						sb.append("flightData[" + i + "][45] = '" + flight.getCsOCFlightNumber() + "';");
					} else {
						sb.append("flightData[" + i + "][45] = '';");
					}
					//CodeShare details
					Collection<CodeShareMCFlight> CodeShare = flight.getCodeShareMCFlights();
					sb.append(setCodeShareArr(CodeShare));
					sb.append("flightData[" + i + "][46] = arrCodeShareDtls;");
					sb.append("flightData[" + i + "][48] = '" + flight.getViaScheduleMessaging() + "';");

					i++;
				}
			}
		}
		sb.append("var sendEmailForPax = '" + sendEmailsForFlightAlterations + "';");
		sb.append("var sendSMSForPax = '" + sendSMSForFlightAlterations + "';");
		return sb.toString();
	}

	/**
	 * Sets the Leg Array from Collection of Legs,Time Mode & Flight
	 * 
	 * @param legs
	 *            the Collection of flights
	 * @param local
	 *            the Time Mode LOCAL-true, ZULU-false
	 * @param flight
	 *            the Flight
	 * @return String the leg array string
	 */
	private String setLegs(Collection<FlightLeg> legs, boolean local, Flight flight) {
		String legArr = "var arrLegDtls = new Array();";// leg detail array

		if (legs != null) {

			for (int legnum = 1; legnum < legs.size() + 1; legnum++) {
				Iterator<FlightLeg> iter = legs.iterator();
				while (iter.hasNext()) {
					FlightLeg flLeg = (FlightLeg) iter.next();
					if (flLeg.getLegNumber() == legnum) {

						int arrayIndex = legnum - 1;
						// uncomment to get the condition
						if (local) { // if (strTimeMode.equals(WebConstants.LOCALTIME)) {

							legArr += "arrLegDtls[" + arrayIndex + "] =new Array('" + flLeg.getLegNumber() + "','"
									+ flLeg.getOrigin() + "','" + flLeg.getDestination() + "','"
									+ ScheduleUtils.formatDate(flLeg.getEstDepartureTimeLocal(), defaultFormat) + "','"
									+ flLeg.getEstDepartureDayOffsetLocal() + "','"
									+ ScheduleUtils.formatDate(flLeg.getEstArrivalTimeLocal(), defaultFormat) + "','"
									+ flLeg.getEstArrivalDayOffsetLocal() + "','" + flLeg.getDuration() + "','"
									+ SegmentUtil.isOverlappingLeg(flLeg, flight) + "');";
						} else {
							legArr += "arrLegDtls[" + arrayIndex + "] =new Array('" + flLeg.getLegNumber() + "','"
									+ flLeg.getOrigin() + "','" + flLeg.getDestination() + "','"
									+ ScheduleUtils.formatDate(flLeg.getEstDepartureTimeZulu(), defaultFormat) + "','"
									+ flLeg.getEstDepartureDayOffset() + "','"
									+ ScheduleUtils.formatDate(flLeg.getEstArrivalTimeZulu(), defaultFormat) + "','"
									+ flLeg.getEstArrivalDayOffset() + "','" + flLeg.getDuration() + "','"
									+ SegmentUtil.isOverlappingLeg(flLeg, flight) + "');";
						}
						break;
					}

				}
			}
		}
		return legArr;

	}

	/**
	 * Creates the Client Validations for Flight Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Client validation array String
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {
		if (clientErrors == null) {
			Properties moduleErrs = new Properties();

			moduleErrs.setProperty("um.flight.startdate.null", "startdate");
			moduleErrs.setProperty("um.flight.stopdate.null", "stopdate");
			moduleErrs.setProperty("um.flight.startdate.greater", "stdategreat");
			moduleErrs.setProperty("um.flight.stopdate.greater", "stopdategreat");
			moduleErrs.setProperty("um.flight.optype.null", "optypenull");
			moduleErrs.setProperty("um.flight.status.null", "statusnull");

			moduleErrs.setProperty("um.flight.depdate.null", "dptdatenull");
			moduleErrs.setProperty("um.flight.depdate.lesscurent", "lesscurent");
			moduleErrs.setProperty("um.flight.fltno.null", "fltnonull");
			moduleErrs.setProperty("um.flight.type.null", "flttypenull");
			moduleErrs.setProperty("um.flight.fltDate.invalidDate", "invalidDate");
			moduleErrs.setProperty("um.flight.flt.fltnotg", "fltnotg");
			moduleErrs.setProperty("um.flight.flt.fltgno", "fltgno");
			moduleErrs.setProperty("um.flight.modle.modelg", "modelg");
			moduleErrs.setProperty("um.flight.modle.null", "modelnull");
			moduleErrs.setProperty("um.flight.leg.leastone", "leastone");
			moduleErrs.setProperty("um.flight.leg.fromnull", "fromnull");
			moduleErrs.setProperty("um.flight.leg.tomnull", "tonull");
			moduleErrs.setProperty("um.flight.leg.sameairport", "sameairport");
			moduleErrs.setProperty("um.flight.leg.indeptime", "indeptime");
			moduleErrs.setProperty("um.flight.leg.arrtimenull", "arrtimenull");
			moduleErrs.setProperty("um.flight.leg.deptimenull", "deptimenull");
			moduleErrs.setProperty("um.flight.leg.arrdepsame", "arrdepsame");
			moduleErrs.setProperty("um.flight.leg.mlitilegvalid", "mlitilegvalid");
			moduleErrs.setProperty("um.flight.status.statusinvalid", "statusinvalid");
			moduleErrs.setProperty("um.flight.leg.deptimeinvalid", "deptimeinvalid");
			moduleErrs.setProperty("um.flight.leg.arrtimeinvalid", "arrtimeinvalid");
			moduleErrs.setProperty("um.flight.leg.lessdeptime", "deptimelesser");

			moduleErrs.setProperty("um.flight.cpy.tonull", "cpytonull");
			moduleErrs.setProperty("um.flight.cpy.togreater", "togreater");
			moduleErrs.setProperty("um.flight.cpy.samedate", "samedate");
			moduleErrs.setProperty("um.flight.cpy.anotherdate", "anotherdate");
			moduleErrs.setProperty("um.flight.leg.singlelegvalid", "singlelegvalid");
			moduleErrs.setProperty("um.flight.aircraft.model.inactive", "modelinactive");
			moduleErrs.setProperty("um.flight.dep.leg.inactive", "depleginactive");
			moduleErrs.setProperty("um.flight.arr.leg.inactive", "arrleginactive");
			moduleErrs.setProperty("um.flight.fltNo.invalidFltNo", "invalidFltNo");
			moduleErrs.setProperty("um.flight.add.onlyOpTypeOp", "onlyOpTypeOp");
			moduleErrs.setProperty("um.flight.add.editOpTypeOp", "editOpTypeOp");

			moduleErrs.setProperty("um.flightschedule.email.noaddress", "noaddress");
			moduleErrs.setProperty("um.flightschedule.email.invalidmailformat", "invalidmailformat");
			moduleErrs.setProperty("um.flightschedule.email.invalidlength", "invalidlength");
			moduleErrs.setProperty("um.flightschedule.search.invalidcarrier", "invalidcarrier");

			moduleErrs.setProperty("um.flightschedule.sms.empty", "smsempty");
			moduleErrs.setProperty("um.flight.remarks.char.invalid", "invalidCharRem");
			
			moduleErrs.setProperty("um.flight.gds.cs.carrier.null", "CScarriernull");
			moduleErrs.setProperty("um.flight.gds.cs.flightNum.null", "CSfltnonull");
			moduleErrs.setProperty("um.flight.gds.cs.OC.MC.invalid", "invalidOCandMC");
			moduleErrs.setProperty("um.flight.gds.cs.alreadyexist", "codeShareExists");
			moduleErrs.setProperty("um.flight.gds.cs.un.publish", "codeShareUnPublish");
			

			clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}
		return clientErrors;
	}

	/**
	 * Creates the Flight Segment Validation Row
	 * 
	 * @param flight
	 *            the Flight
	 * @return String the Segment Array String
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public String createFlightSegmentValidationRowHtml(Flight flight) throws ModuleException {

		StringBuffer sb = new StringBuffer();
		sb.append("var arrOLSegData = new Array();");

		Integer overLapId = flight.getOverlapingFlightId();

		if (overLapId != null) {
			Flight olapFlight = ModuleServiceLocator.getFlightServiceBD().getFlight(overLapId.intValue());
			Set<FlightSegement> segSet = olapFlight.getFlightSegements();
			if (segSet != null) {
				Iterator<FlightSegement> segIte = segSet.iterator();
				int i = 0;
				while (segIte.hasNext()) {
					FlightSegement segment = (FlightSegement) segIte.next();
					sb.append("arrOLSegData[" + i + "] = new Array();");
					sb.append("arrOLSegData[" + i + "][0] = '" + i + "';");
					sb.append("arrOLSegData[" + i + "][1] = '" + segment.getSegmentCode() + "';");
					if (segment.getValidFlag()) {
						sb.append("arrOLSegData[" + i + "][2] = 'Y';");
					} else {
						sb.append("arrOLSegData[" + i + "][2] = 'N';");
					}
					sb.append("arrOLSegData[" + i + "][3] = '" + segment.getFltSegId() + "';");
					i++;
				}

			}

		} else {
			Collection<Flight> flightCol = ModuleServiceLocator.getFlightServiceBD().getPossibleOverlappingFlights(flight);

			if (flightCol != null) {
				Iterator<Flight> ite = flightCol.iterator();
				while (ite.hasNext()) {
					Flight flt = (Flight) ite.next();
					int flightId = flt.getFlightId().intValue();
					sb.append("arrOLSegData[" + flightId + "] = new Array();");

					Set<FlightSegement> segSet = flt.getFlightSegements();
					if (segSet != null) {
						Iterator<FlightSegement> segIte = segSet.iterator();
						int i = 0;
						while (segIte.hasNext()) {
							FlightSegement segment = (FlightSegement) segIte.next();

							sb.append("arrOLSegData[" + flightId + "][" + i + "] = new Array();");
							sb.append("arrOLSegData[" + flightId + "][" + i + "][0] = '" + i + "';");
							sb.append("arrOLSegData[" + flightId + "][" + i + "][1] = '" + segment.getSegmentCode() + "';");
							if (segment.getValidFlag()) {
								sb.append("arrOLSegData[" + flightId + "][" + i + "][2] = 'Y';");
							} else {
								sb.append("arrOLSegData[" + flightId + "][" + i + "][2] = 'N';");
							}
							sb.append("arrOLSegData[" + flightId + "][" + i + "][3] = '" + segment.getFltSegId() + "';");
							i++;
						}
					}
				}
			}
		}
		return sb.toString();
	}

	/**
	 * Gets Segment Validation Row for Edit Purpose
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String Segment row Array
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final String getSegmentValidationRowHtml(HttpServletRequest request) throws ModuleException {
		// --Edit mode-- if the segment validation data already exists, populate the data with the grid
		StringBuffer sb = new StringBuffer();
		sb.append("var arrSegData = new Array();");
		String strFlightId = request.getParameter("strFlightId");
		String strLegs = request.getParameter(PARAM_LEGS) == null ? "" : request.getParameter(PARAM_LEGS);
		ArrayList<String> arrSegList = ScheduleUtils.getSgements(strLegs);
		Collection<Integer> invSegIds = new ArrayList<Integer>();
		Collection<Integer> segIds = new ArrayList<Integer>();
		FlightSegement flightSegment = null;
		boolean validFlag = true;
		boolean resFound = false;

		if (strFlightId != null) {
			Flight flight = ModuleServiceLocator.getFlightServiceBD().getFlight(Integer.parseInt(strFlightId));
			Set<FlightSegement> segSet = flight.getFlightSegements();
			if (segSet != null) {
				Iterator<FlightSegement> segIte = segSet.iterator();
				while (segIte.hasNext()) {
					flightSegment = (FlightSegement) segIte.next();
					invSegIds.add(flightSegment.getFltSegId());
				}
				segIds = ModuleServiceLocator.getFlightInventoryReservationBD().filterSegmentsHavingReservations(invSegIds);

				for (int i = 0; i < arrSegList.size(); i++) {
					validFlag = true;
					resFound = false;
					sb.append("arrSegData[" + i + "] = new Array();");
					sb.append("arrSegData[" + i + "][0] = '" + i + "';");
					sb.append("arrSegData[" + i + "][1] = '" + (String) arrSegList.get(i) + "';");

					segIte = segSet.iterator();
					while (segIte.hasNext()) {
						flightSegment = (FlightSegement) segIte.next();
						if (flightSegment.getSegmentCode().trim().equalsIgnoreCase(((String) arrSegList.get(i)).trim())) {
							if (!flightSegment.getValidFlag()) {
								validFlag = false;
							}
							// if flight segment having reservations or not
							if (segIds.contains(flightSegment.getFltSegId())) {
								resFound = true;
							}
							break;
						}
					}

					if (validFlag) {
						sb.append("arrSegData[" + i + "][2] = 'true';");
					} else {
						sb.append("arrSegData[" + i + "][2] = 'false';");

					}
					if (resFound) {
						sb.append("arrSegData[" + i + "][3] = 'true';");
					} else {
						sb.append("arrSegData[" + i + "][3] = 'false';");
					}
				}
			}
		}
		return sb.toString();
	}

	/**
	 * Gets Segment Validation Row for Edit Purpose
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String Segment row Array
	 * @throws ModuleException
	 *             the ModuleException
	 */
	@SuppressWarnings("unused")
	public final String getTerminalValidationRowHtml(HttpServletRequest request) throws ModuleException {
		// --Edit mode-- if the segment validation data already exists, populate the data with the grid
		StringBuffer sb = new StringBuffer();
		sb.append("var arrSegData = new Array();");
		String strFlightId = request.getParameter("strFlightId");
		String strLegs = request.getParameter(PARAM_LEGS) == null ? "" : request.getParameter(PARAM_LEGS);
		ArrayList<String> arrSegList = ScheduleUtils.getSgements(strLegs);

		FlightSegement flightSegment = null;

		if (strFlightId != null) {
			Flight flight = ModuleServiceLocator.getFlightServiceBD().getFlight(Integer.parseInt(strFlightId));
			Set<FlightSegement> segSet = flight.getFlightSegements();
			if (segSet != null) {
				Iterator<FlightSegement> segIte = segSet.iterator();
				while (segIte.hasNext()) {
					flightSegment = (FlightSegement) segIte.next();

				}

				for (int i = 0; i < arrSegList.size(); i++) {

					sb.append("arrSegData[" + i + "] = new Array();");
					sb.append("arrSegData[" + i + "][0] = '" + i + "';");
					sb.append("arrSegData[" + i + "][1] = '" + arrSegList.get(i) + "';");
					sb.append("arrSegData[" + i + "][2] = '" + getTerminalsForAirport(arrSegList.get(i).toString(), true) + "';");
					sb.append("arrSegData[" + i + "][3] = '" + getTerminalsForAirport(arrSegList.get(i).toString(), false) + "';");

				}
			}
		}
		return sb.toString();
	}

	public static String getTerminalsForAirport(String segment, boolean isArrival) throws ModuleException {
		String airport = "";
		String segArr[] = segment.split("/");
		if (isArrival)
			airport = segArr[0];
		else
			airport = segArr[segArr.length - 1];
		FlightHTMLGenerator flthtmGen = new FlightHTMLGenerator();
		return flthtmGen.getTerminalsForAirport(airport);
	}

	/**
	 * Gets the Array of Flights Can be Reprotected To
	 * 
	 * @param map
	 *            the Map comtaing summary Keys
	 * @return String the Reprotectable Flights Array String
	 */
	@SuppressWarnings("rawtypes")
	public String getCanReprotectArray(HashMap map) {

		// GlobalConfig globalConfig = ReservationModuleUtils.getGlobalConfig();

		String sendEmailsForFlightAlterations = globalConfig.getBizParam(SystemParamKeys.SEND_EMAILS_FOR_FLIGHT_ALTERATIONS)
				.equalsIgnoreCase("Y") ? "Y" : "N";
		String sendSMSForFlightAlterations = globalConfig.getBizParam(SystemParamKeys.SEND_SMS_FOR_FLIGHT_ALTERATIONS)
				.equalsIgnoreCase("Y") ? "Y" : "N";

		StringBuffer strReprot = new StringBuffer("var arrreproData = new Array();");
		FlightSummaryDTO fltSum = null;
		FlightSegmentResInvSummaryDTO fltSegResSumDTO = null;
		ArrayList sumArray = null;
		Set summarySet = map.keySet();
		Iterator iter = summarySet.iterator();
		int i = 0;
		while (iter.hasNext()) {
			fltSum = (FlightSummaryDTO) iter.next();
			SimpleDateFormat sfmt = new SimpleDateFormat(strSMDateFormat);
			String strdDepDate = "";
			Date depDt = fltSum.getDepatureDate();
			if (depDt != null) {
				try {
					strdDepDate = sfmt.format(depDt);
				} catch (Exception e) {
					// do nothing
				}
			}

			sumArray = (ArrayList) map.get(fltSum);
			for (int j = 0; j < sumArray.size(); j++) {
				fltSegResSumDTO = (FlightSegmentResInvSummaryDTO) sumArray.get(j);
				strReprot.append("arrreproData[" + i + "] = new Array();");
				strReprot.append("arrreproData[" + i + "][0] = '" + i + "';");
				strReprot.append("arrreproData[" + i + "][1] = '" + strdDepDate + "';");
				strReprot.append("arrreproData[" + i + "][2] = '" + fltSum.getFlightId() + "';");

				strReprot.append("arrreproData[" + i + "][3] = '" + fltSegResSumDTO.getSegmentCode() + "';");
				strReprot.append("arrreproData[" + i + "][4] = '" + fltSegResSumDTO.getSoldSeatsAdult() + "/"
						+ fltSegResSumDTO.getSoldSeatsInfant() + "';");
				strReprot.append("arrreproData[" + i + "][5] = '" + fltSegResSumDTO.getConfirmedPnrs() + "';");
				strReprot.append("arrreproData[" + i + "][6] = '" + fltSegResSumDTO.getOnholdSeatsAdult() + "/"
						+ fltSegResSumDTO.getOnholdSeatsInfant() + "';");
				strReprot.append("arrreproData[" + i + "][7] = '" + fltSegResSumDTO.getOnholdPnrs() + "';");
				i++;
			}
		}
		strReprot.append("var sendEmailForPax = '" + sendEmailsForFlightAlterations + "';");
		strReprot.append("var sendSMSForPax = '" + sendSMSForFlightAlterations + "';");
		return strReprot.toString();
	}

	/**
	 * Creating the Model Data pass to the Front end if Any Error/Exception Occure
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array of created flight data sends from front end
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public String createModel(HttpServletRequest request, String message) throws ModuleException {

		String strDepatureDate = request.getParameter("txtDepatureDate");
		String strOpType = request.getParameter("selOperationType");
		String strFlightNo = request.getParameter("txtFlightNo");
		String strAircraftModel = request.getParameter("selModelNo");
		String strStatus = request.getParameter("selStatus");
		String strFlightType = request.getParameter("selFlightType");
		String hdnLegArray = request.getParameter("hdnLegArray");
		String strLeg1from = request.getParameter("selFromStn1");
		String strLeg1to = request.getParameter("selToStn1");
		String strLeg1Depature = request.getParameter("txtDepTime1");
		String strLeg1Arrival = request.getParameter("txtArrTime1");
		String strLeg1Depday = request.getParameter("selDepDay1");
		String strLeg1Arrday = request.getParameter("selArrDay1");

		String strLeg2from = request.getParameter("selFromStn2");
		String strLeg2to = request.getParameter("selToStn2");
		String strLeg2Depature = request.getParameter("txtDepTime2");
		String strLeg2Arrival = request.getParameter("txtArrTime2");
		String strLeg2Depday = request.getParameter("selDepDay2");
		String strLeg2Arrday = request.getParameter("selArrDay2");

		String strLeg3from = request.getParameter("selFromStn3");
		String strLeg3to = request.getParameter("selToStn3");
		String strLeg3Depature = request.getParameter("txtDepTime3");
		String strLeg3Arrival = request.getParameter("txtArrTime3");
		String strLeg3Depday = request.getParameter("selDepDay3");
		String strLeg3Arrday = request.getParameter("selArrDay3");

		String strLeg4from = request.getParameter("selFromStn4");
		String strLeg4to = request.getParameter("selToStn4");
		String strLeg4Depature = request.getParameter("txtDepTime4");
		String strLeg4Arrival = request.getParameter("txtArrTime4");
		String strLeg4Depday = request.getParameter("selDepDay4");
		String strLeg4Arrday = request.getParameter("selArrDay4");

		String strLeg5from = request.getParameter("selFromStn5");
		String strLeg5to = request.getParameter("selToStn5");
		String strLeg5Depature = request.getParameter("txtDepTime5");
		String strLeg5Arrival = request.getParameter("txtArrTime5");
		String strLeg5Depday = request.getParameter("selDepDay5");
		String strLeg5Arrday = request.getParameter("selArrDay5");

		String strOverLapSeg = request.getParameter("hdnOverLapSeg");
		String strOverLapSegId = request.getParameter("hdnOverLapSegId");
		String strSegArray = request.getParameter("hdnSegArray");
		String strTerminalArray = request.getParameter("hdnTerminalArray");
		String strScheduleId = request.getParameter("hdnScheduleId");
		String strFlightId = request.getParameter("hdnFlightId");
		String strOverlapFlightID = request.getParameter("hdnOverlapFlightId");

		String strAllowConf = request.getParameter("hdnAllowConf");
		String strAllowDuration = request.getParameter("hdnAllowDuration");
		String strAllowModel = request.getParameter("hdnAllowModel");
		String strAllowSegment = request.getParameter("hdnAllowSegment");
		String strAllowOverlap = request.getParameter("hdnAllowOverlap");
		String strVersion = request.getParameter("hdnVersion");
		String strFltstart = request.getParameter("txtFlightNoStart");

		String strMode = request.getParameter("hdnMode");
		String strCopytoDate = request.getParameter("hdnCopyToDay");
		String strprevstat = request.getParameter("hdnPrevStatus");
		String gridRow = request.getParameter("hdnGridRow");
		String timeMode = request.getParameter("hdnTimeMode");
		String mainMode = request.getParameter("hdnMainMode");
		String reAlert = request.getParameter("hdnReSchedAlert");
		String reEmail = request.getParameter("hdnReSchedEmail");
		String fromPage = request.getParameter("hdnFromPage");
		String cnAlert = request.getParameter("hdnCancelAlert");
		String cnEmail = request.getParameter("hdnCancelEmail");
		String emailTo = request.getParameter("hdnEmailTo");
		String emailSubject = request.getParameter("hdnEmailSubject");
		String emailContent = request.getParameter("hdnEmailContent");
		String fromFromPage = request.getParameter("hdnFromFromPage");
		String opnEtickets = request.getParameter("hdnOpenEtStatus");
		// String strStartTime = request.getParameter("hdnStartTime");
		// String strEndTime = request.getParameter("hdnEndTime");
		String strFrequency = request.getParameter("hdnFrequency");
		String strScheduledOnly = request.getParameter("hdnIncludeScheduled");

		String txtRemarks = request.getParameter("txtRemarks");
		String selSeatChargeTemplate = request.getParameter("selSeatChargeTemplate");
		String selMealTemplate = request.getParameter("selMealTemplate");
		String txtUserNotes = request.getParameter("txtUserNotes");
		String csOcCarrierCode = request.getParameter("hndCsOCCarrierCode");
		String csOcFlightNo = request.getParameter("hndCsOCFlightNo");
		// String GDSIDs = request.getParameter("hdnGDSIDs");//Haider 01Sep08
		String strCodeShareMC= request.getParameter("hndCodeShare");

		StringBuffer sb = new StringBuffer();
		sb.append("var model= new Array();");
		String formFields[] = { "txtDepatureDate", "selOperationType", "txtFlightNo", "selModelNo", "selStatus", "selFromStn1",
				"selToStn1", "txtDepTime1", "txtArrTime1", "selDepDay1", "selArrDay1", "selFromStn2", "selToStn2", "txtDepTime2",
				"txtArrTime2", "selDepDay2", "selArrDay2", "selFromStn3", "selToStn3", "txtDepTime3", "txtArrTime3",
				"selDepDay3", "selArrDay3", "selFromStn4", "selToStn4", "txtDepTime4", "txtArrTime4", "selDepDay4", "selArrDay4",
				"selFromStn5", "selToStn5", "txtDepTime5", "txtArrTime5", "selDepDay5", "selArrDay5", "hdnOverLapSeg",
				"hdnOverLapSegId", "hdnSegArray", "hdnScheduleId", "hdnFlightId", "hdnOverlapFlightId", "hdnMode",
				"hdnAllowConf", "hdnAllowDuration", "hdnAllowModel", "hdnAllowSegment", "hdnAllowOverlap", "hdnVersion",
				"txtFlightNoStart", "hdnCopyToDay", "hdnPrevStatus", "hdnGridRow", "hdnTimeMode", "hdnReSchedAlert",
				"hdnReSchedEmail", "hdnFromPage", "hdnCancelAlert", "hdnCancelEmail", "hdnEmailTo", "hdnEmailSubject",
				"hdnEmailContent", "hdnFromFromPage", "hdnFrequency", "hdnIncludeScheduled", "hdnTerminalArray", "selFlightType",
				"txtRemarks", "selSeatChargeTemplate", "selMealTemplate", "txtUserNotes", "displayWarning", "hdnLegArray", 
				"txtCsOCCarrierCode", "txtCsOCFlightNo","sel_CodeShare", "hdnOpenEtStatus"};
		String formData[] = new String[76];
		formData[0] = strDepatureDate;
		formData[1] = strOpType;
		formData[2] = strFlightNo;
		formData[3] = strAircraftModel;
		formData[4] = strStatus;
		formData[5] = strLeg1from;
		formData[6] = strLeg1to;
		formData[7] = strLeg1Depature;
		formData[8] = strLeg1Arrival;
		formData[9] = strLeg1Depday;
		formData[10] = strLeg1Arrday;
		formData[11] = strLeg2from;
		formData[12] = strLeg2to;
		formData[13] = strLeg2Depature;
		formData[14] = strLeg2Arrival;
		formData[15] = strLeg2Depday;
		formData[16] = strLeg2Arrday;
		formData[17] = strLeg3from;
		formData[18] = strLeg3to;
		formData[19] = strLeg3Depature;
		formData[20] = strLeg3Arrival;
		formData[21] = strLeg3Depday;
		formData[22] = strLeg3Arrday;
		formData[23] = strLeg4from;
		formData[24] = strLeg4to;
		formData[25] = strLeg4Depature;
		formData[26] = strLeg4Arrival;
		formData[27] = strLeg4Depday;
		formData[28] = strLeg4Arrday;
		formData[29] = strLeg5from;
		formData[30] = strLeg5to;
		formData[31] = strLeg5Depature;
		formData[32] = strLeg5Arrival;
		formData[33] = strLeg5Depday;
		formData[34] = strLeg5Arrday;
		formData[35] = strOverLapSeg;
		formData[36] = strOverLapSegId;
		formData[37] = strSegArray;
		formData[38] = strScheduleId;
		formData[39] = strFlightId;
		formData[40] = strOverlapFlightID;
		formData[41] = strMode;
		formData[42] = strAllowConf;
		formData[43] = strAllowDuration;
		formData[44] = strAllowModel;
		formData[45] = strAllowSegment;
		formData[46] = strAllowOverlap;
		formData[47] = strVersion;
		formData[48] = strFltstart;
		formData[49] = strCopytoDate;
		formData[50] = strprevstat;
		formData[51] = gridRow;
		formData[52] = timeMode;
		formData[53] = reAlert;
		formData[54] = reEmail;
		formData[55] = fromPage;
		formData[56] = cnAlert;
		formData[57] = cnEmail;
		formData[58] = emailTo;
		formData[59] = emailSubject;
		formData[60] = emailContent;
		formData[61] = fromFromPage;
		formData[62] = strFrequency;
		formData[63] = strScheduledOnly;
		formData[64] = strTerminalArray;
		formData[65] = strFlightType;
		formData[66] = txtRemarks;
		formData[67] = selSeatChargeTemplate;
		formData[68] = selMealTemplate;
		formData[69] = txtUserNotes;

		formData[70] = message;

		formData[71] = hdnLegArray;
		formData[72] = csOcCarrierCode;
		formData[73] = csOcFlightNo;
		formData[74] = strCodeShareMC;
		formData[75] = opnEtickets;

		for (int i = 0; i < formData.length; i++) {
			sb.append("model['" + formFields[i] + "']='" + formData[i] + "';");
		}
		return sb.toString();

	}

	public final String getGDSData(String gdsIds, int flightId) throws ModuleException {
		StringBuffer sb = new StringBuffer("var arrData = new Array();");
		String[] arGdsIds = StringUtils.split(gdsIds, ',');
		int	numberOfFlights = 0;
		Flight flt = ModuleServiceLocator.getFlightServiceBD().getFlight(flightId);
		HashMap<String, String> publishMechMap = (HashMap<String, String>) CommonsServices.getGlobalConfig()
				.getPublishMechanismMap();
		//validate schedule with one flight
		if(flt.getScheduleId()!= null){
			 numberOfFlights = ModuleServiceLocator.getScheduleServiceBD().getNoOfFlightsForSchedule(flt.getScheduleId());
			}
		if (flt.getScheduleId() == null || numberOfFlights == 1) {
			List<Gds> gdsList = ModuleServiceLocator.getGdsServiceBD().getGDSs();
			if (gdsList != null) {
				Object[] listArr = gdsList.toArray();
				int len = gdsList.size();
				int index = 0;
				Gds gds = null;
				for (int i = 0; i < len; i++) {
					gds = (Gds) listArr[i];
					if(gds.getSchedulingSystem()){
						continue;
					}
					sb.append("arrData[" + index + "] = new Array();");
					sb.append("arrData[" + index + "][1] = '" + gds.getGdsCode() + "';");
					sb.append("arrData[" + index + "][2] = '" + (isGDSPublished(gds.getGdsId(), arGdsIds) ? 1 : 0) + "';");
					sb.append("arrData[" + index + "][3] = '" + ("ACT".equals(gds.getStatus()) ? "Active" : "Inactive") + "';");
					sb.append("arrData[" + index + "][4] = '" + publishMechMap.get(gds.getSchedPublishMechanismCode()) + "';");
					sb.append("arrData[" + index + "][5] = '';");
					sb.append("arrData[" + index + "][6] = '" + gds.getGdsId() + "';");
					sb.append("arrData[" + index + "][7] = '" + gds.getCodeShareCarrier() + "';");
					++ index;
				}
			}
		} else {
			Gds gds = null;
			int index = 0;
			for (int i = 0; i < arGdsIds.length; i++) {
				gds = ModuleServiceLocator.getGdsServiceBD().getGds(Integer.valueOf(arGdsIds[i]).intValue());
				if(gds.getSchedulingSystem()){
					continue;
				}
				sb.append("arrData[" + index + "] = new Array();");
				sb.append("arrData[" + index + "][1] = '" + gds.getGdsCode() + "';");
				sb.append("arrData[" + index + "][2] = '1';");
				sb.append("arrData[" + index + "][3] = '" + ("ACT".equals(gds.getStatus()) ? "Active" : "Inactive") + "';");
				sb.append("arrData[" + index + "][4] = '" + publishMechMap.get(gds.getSchedPublishMechanismCode()) + "';");
				sb.append("arrData[" + index + "][5] = '';");
				sb.append("arrData[" + index + "][6] = '" + gds.getGdsId() + "';");
				sb.append("arrData[" + index + "][7] = '" + gds.getCodeShareCarrier() + "';");
				++index;
			}
		}
		return sb.toString();
	}

	// Haider 2Nov08
	private static boolean isGDSPublished(int id, String[] gdsIds) {
		int len = gdsIds.length;
		for (int j = 0; j < len; j++) {
			if (id == Integer.valueOf(gdsIds[j]).intValue())
				return true;
		}
		return false;
	}

	public static boolean isEligibleForActivatePastFlights(Set<FlightSegement> fltSegments) {
		try {
			Date zuluDepDate;
			if (fltSegments != null) {
				Iterator<FlightSegement> ite = fltSegments.iterator();

				if (ite != null) {
					while (ite.hasNext()) {
						FlightSegement seg = (FlightSegement) ite.next();
						Date today = new Date();
						zuluDepDate = new Date(seg.getEstTimeDepatureZulu().getTime());
						int diffDays = today.getDate() - zuluDepDate.getDate();
						if (AppSysParamsUtil.getPastFlightActivationDurationInDays() > 0) {
							if (diffDays < AppSysParamsUtil.getPastFlightActivationDurationInDays()) {
								return true;
							}
						}
						break;
					}
				}
			}
			return false;
		} catch (Exception e) {
			log.error("Error in isEligibleForActivatePastFlights");
			return false;

		}
	}

	public static boolean isPassedDepartureTime(Set<FlightSegement> fltSegments) {
		try {
			Date zuluDepDate;
			int closureTimeDiff = 0;
			if (fltSegments != null) {
				Iterator<FlightSegement> ite = fltSegments.iterator();

				if (ite != null) {
					while (ite.hasNext()) {
						FlightSegement seg = (FlightSegement) ite.next();
						zuluDepDate = new Date(seg.getEstTimeDepatureZulu().getTime());
						closureTimeDiff = zuluDepDate.compareTo(getSystemTime());
						break;
					}
				}

				if (log.isDebugEnabled()) {
					log.debug("Sys Time " + getSystemTime().toString());
					log.debug("closure Time Diff " + String.valueOf(closureTimeDiff));
				}
				if (closureTimeDiff <= 0) {
					return true;
				}
			}
			return false;
		} catch (Exception e) {
			log.error("Error in isPassedDepartureTime");
			return false;

		}
	}

	private static Date getSystemTime() {
		String strTimeZone = "GMT";
		Calendar cal = Calendar.getInstance();
		cal.setTimeZone(TimeZone.getTimeZone(strTimeZone));
		cal.set(Calendar.SECOND, 0); // flight doesnt have sec and minutes set
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	public String getTerminalsForAirport(String airport) throws ModuleException {
		StringBuffer sb = new StringBuffer("");
		Collection<AirportTerminal> list = ModuleServiceLocator.getAirportServiceBD().getAirportTerminals(airport, true);
		String selected = "";
		if (1 == list.size())
			selected = "selected";
		for (AirportTerminal terminal : list) {
			sb.append("<option value=\"" + terminal.getTerminalId() + "\" " + selected + ">" + terminal.getTerminalShortName()
					+ "</option>");
		}
		return sb.toString();
	}
	/**
	 * Method to Create the CodeShare array
	 * 
	 * @param CodeShares
	 *            the collection of CodeShare
	 * @return String the CodeShare Detail Array
	 */
	private String setCodeShareArr(Collection<CodeShareMCFlight> CodeShares) {

		String CodeShareArr = "var arrCodeShareDtls = new Array();";// CodeShare detail array
		int j = 0;
		if (CodeShares != null) {
			Iterator<CodeShareMCFlight> iter = CodeShares.iterator();
			while (iter.hasNext()) {
				CodeShareMCFlight schCodeShare = (CodeShareMCFlight) iter.next();
				CodeShareArr += "arrCodeShareDtls[" + j + "] =new Array('" + schCodeShare.getCsMCCarrierCode() + "|" + schCodeShare.getCsMCFlightNumber()+ "');";

				j++;
			}
		}
		return CodeShareArr;
	}

}
