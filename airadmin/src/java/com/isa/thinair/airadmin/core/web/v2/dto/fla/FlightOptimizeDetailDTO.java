package com.isa.thinair.airadmin.core.web.v2.dto.fla;

public class FlightOptimizeDetailDTO {

	private String allocatedToBookingClass;
	private char closed;
	private char fixed;
	private char priority;
	private String soldSeats;
	private String onholdSeats;
	private String cancelledSeats;
	private String acquiredSeats;
	private String availableSeats;
	private int fccsInvId;
	private Integer fccsbcInvId;
	private long version;
	private String bookingClassCode;
	private String segmentCode;
	private String logicalCCCode;
	private String action;
	private String oldAction;
	private char oldStatus;
	private boolean openReturnBc;

	public String getAllocatedToBookingClass() {
		return allocatedToBookingClass;
	}

	public void setAllocatedToBookingClass(String allocatedToBookingClass) {
		this.allocatedToBookingClass = allocatedToBookingClass;
	}

	public String getSoldSeats() {
		return soldSeats;
	}

	public void setSoldSeats(String soldSeats) {
		this.soldSeats = soldSeats;
	}

	public String getOnholdSeats() {
		return onholdSeats;
	}

	public void setOnholdSeats(String onholdSeats) {
		this.onholdSeats = onholdSeats;
	}

	public String getCancelledSeats() {
		return cancelledSeats;
	}

	public void setCancelledSeats(String cancelledSeats) {
		this.cancelledSeats = cancelledSeats;
	}

	public String getAcquiredSeats() {
		return acquiredSeats;
	}

	public void setAcquiredSeats(String acquiredSeats) {
		this.acquiredSeats = acquiredSeats;
	}

	public String getAvailableSeats() {
		return availableSeats;
	}

	public void setAvailableSeats(String availableSeats) {
		this.availableSeats = availableSeats;
	}

	public char getFixed() {
		return fixed;
	}

	public void setFixed(char fixed) {
		this.fixed = fixed;
	}

	public int getFccsInvId() {
		return fccsInvId;
	}

	public void setFccsInvId(int fccsInvId) {
		this.fccsInvId = fccsInvId;
	}

	public Integer getFccsbcInvId() {
		return fccsbcInvId;
	}

	public void setFccsbcInvId(Integer fccsbcInvId) {
		this.fccsbcInvId = fccsbcInvId;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public String getBookingClassCode() {
		return bookingClassCode;
	}

	public void setBookingClassCode(String bookingClassCode) {
		this.bookingClassCode = bookingClassCode;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public char getClosed() {
		return closed;
	}

	public void setClosed(char closed) {
		this.closed = closed;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}
	
	public String getLogicalCCCode() {
		return logicalCCCode;
	}

	public void setLogicalCCCode(String logicalCCCode) {
		this.logicalCCCode = logicalCCCode;
	}

	public char getPriority() {
		return priority;
	}

	public void setPriority(char priority) {
		this.priority = priority;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getOldAction() {
		return oldAction;
	}

	public void setOldAction(String oldAction) {
		this.oldAction = oldAction;
	}

	public char getOldStatus() {
		return oldStatus;
	}

	public void setOldStatus(char oldStatus) {
		this.oldStatus = oldStatus;
	}

	public boolean isOpenReturnBc() {
		return openReturnBc;
	}

	public void setOpenReturnBc(boolean openReturnBc) {
		this.openReturnBc = openReturnBc;
	}

}
