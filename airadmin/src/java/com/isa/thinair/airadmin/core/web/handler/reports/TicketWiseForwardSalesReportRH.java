package com.isa.thinair.airadmin.core.web.handler.reports;

import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

public class TicketWiseForwardSalesReportRH extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(TicketWiseForwardSalesReportRH.class);

	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	public static String execute(HttpServletRequest request, HttpServletResponse response) {
		String forward = WebConstants.ACTION_FORWARD_SUCCESS;
		String strHdnMode = request.getParameter("hdnMode");

		try {
			setDisplayData(request);
			log.debug("TicketWiseForwardSalesReportRH setDisplay Success");
		} catch (Exception e) {
			log.error("TicketWiseForwardSalesReportRH setDisplayData Failed");
		}
		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				log.debug("TicketWiseForwardSalesReportRH setReportView Success");
				return null;
			}
		} catch (ModuleException e) {
			forward = AdminStrutsConstants.AdminAction.ERROR;
			log.error("TicketWiseForwardSalesReportRH setReportView Failed");
		}
		return forward;
	}

	public static void setDisplayData(HttpServletRequest request) throws Exception {
		setCountrySelectList(request);
		setStationList(request);
	}

	private static void setStationList(HttpServletRequest request) throws ModuleException {
		String strStation = SelectListGenerator.createActiveAirportCodeList();
		request.setAttribute(WebConstants.REQ_STATION_LIST, strStation);
	}

	private static void setCountrySelectList(HttpServletRequest request) throws ModuleException {
		String strCountry = SelectListGenerator.createCountryList();
		request.setAttribute(WebConstants.REQ_COUNTRY_LIST, strCountry);
	}

	protected static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {
		ResultSet resultSet = null;
		Calendar cal = null;
		String formattedDate = null;
		DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String country = request.getParameter("selCountry");
		String station = request.getParameter("selStations");
		String fromTktNumber = request.getParameter("txtFromETktNumber");
		String toTktNumber = request.getParameter("txtToETktNumber");
		String value = "CSV";
		String id = "UC_REPM_058";
		String reportTemplate = "TicketWiseForwardSalesReport.jasper";
		String strLogo = AppSysParamsUtil.getReportLogo(false);
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String strBase = globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY);
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);

		Map<String, Object> parameters = new HashMap<String, Object>();
		try {
			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));

			ReportsSearchCriteria search = new ReportsSearchCriteria();
			search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate));
			search.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate));
			search.setCountryCode(country);
			search.setStation(station);
			search.setFromTktNumber(fromTktNumber);
			search.setToTktNumber(toTktNumber);
			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}

			cal = new GregorianCalendar();

			formattedDate = formatter.format(cal.getTime());
			resultSet = ModuleServiceLocator.getDataExtractionBD().getTicketWiseForwardSalesReport(search);

			parameters.put("OPTION", value);
			parameters.put("ID", id);
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
			parameters.put("AMT_1", "(" + strBase + ")");
			parameters.put("REPORT_MEDIUM", strlive);
			parameters.put("fromdate", fromDate);
			parameters.put("todate", toDate);

			response.reset();
			response.addHeader("Content-Disposition", "attachment;filename=TicketWiseForwardSalesReport.csv");
			ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);

		} catch (Exception e) {
			log.error(e);
		}

	}
}