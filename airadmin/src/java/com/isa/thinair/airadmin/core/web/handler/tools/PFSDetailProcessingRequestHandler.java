/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */

/**
 * @author Srikantha
 *
 */

package com.isa.thinair.airadmin.core.web.handler.tools;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.tools.PFSDetailProcessingHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airproxy.api.model.reservation.core.LCCClientPnrModesDTO;
import com.isa.thinair.airreservation.api.dto.FlightReconcileDTO;
import com.isa.thinair.airreservation.api.dto.ReservationSegmentDTO;
import com.isa.thinair.airreservation.api.dto.eticket.EticketTO;
import com.isa.thinair.airreservation.api.model.Pfs;
import com.isa.thinair.airreservation.api.model.PfsPaxEntry;
import com.isa.thinair.airreservation.api.model.Reservation;
import com.isa.thinair.airreservation.api.model.ReservationPax;
import com.isa.thinair.airreservation.api.model.ReservationSegment;
import com.isa.thinair.airreservation.api.utils.BeanUtils;
import com.isa.thinair.airreservation.api.utils.ReservationApiUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airreservation.api.utils.ResponseCodes;
import com.isa.thinair.auditor.api.dto.PFSAuditDTO;
import com.isa.thinair.auditor.api.service.AuditorBD;
import com.isa.thinair.commons.api.constants.CommonsConstants.EticketStatus;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author srikantha
 * 
 *         Last Modified 12/05/2009
 */

public final class PFSDetailProcessingRequestHandler extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(PFSDetailProcessingRequestHandler.class);

	private static AiradminConfig airadminConfig = new AiradminConfig();
	private static final String PARAM_UI_MODE = "hdnUIMode";

	private static final String PARAM_CURRENT_PFS = "hdnCurrentPfs";
	private static final String PARAM_MODE = "hdnMode";
	private static final String PARAM_TITLE = "selTitle";
	private static final String PARAM_PAX_TYPE = "selPaxType";
	private static final String PARAM_FIRST_NAME = "txtFirstName";
	private static final String PARAM_LAST_NAME = "txtLastName";
	private static final String PARAM_PNR = "txtPNR";
	private static final String PARAM_DESTINATION = "selDest";
	private static final String PARAM_PAX_STATUS = "selAddStatus";
	private static final String PARAM_PAX_ID = "hdnPaxID";
	private static final String PARAM_PAX_VERSION = "hdnPaxVersion";
	private static final String PARAM_PROCESS_STATUS = "hdnProcessSta";
	private static final String PARAM_GRIDROW = "hdnCurrentRowNum";
	private static final String PARAM_CURRENT_PFS_ID = "hdnPFSID";
	private static final String PARAM_FIRST_TIME = "hdnState";
	private static final String PARAM_NO_PAX = "txtPaxNO";
	private static final String PARAM_CABINCLAS = "selCC";
	private static final String PARAM_PAXCATEGORY = "selPaxCat";
	private static final String PAX_CHILD_TYPE = "CH";
	private static final String PAX_CHILD = "CHILD";
	private static final String DATE_FORMAT_DDMMYYYY = "dd/MM/yyyy HH:mm";
	private static final SimpleDateFormat outputDateFormat = new SimpleDateFormat(DATE_FORMAT_DDMMYYYY);
	private static final String ACTIVE_PFS_ID = "CurrentPFS";
	private static final String PAX_INF_PARENT = "selParent";
	private static final String PAX_IS_PARENT = "hdnIsParent";
	private static final String E_TICKET = "txtETicket";

	/**
	 * 
	 * @param request
	 * @param value
	 */
	private static void setIntSuccess(HttpServletRequest request, int value) {
		setAttribInRequest(request, "intSuccess", new Integer(value));
	}

	/**
	 * Sucess Values: 0 - Saved Successfuly 1 - Delete Successfully -1 - Error occurred - No releavant field identified
	 * -2 - Error occurred - Pax Title -3 - Error occurred - Pax First Name -4 - Error occurred - Pax Last Name -5 -
	 * Error occurred - PNR -6 - Error occurred - Destination -7 - Error occurred - Status
	 * 
	 */
	private static int getIntSuccess(HttpServletRequest request) {
		Integer returnValue = (Integer) getAttribInRequest(request, "intSuccess");
		if (returnValue != null)
			return returnValue.intValue();
		else
			return -1;
	}

	private static void setExceptionOccured(HttpServletRequest request, boolean value) {
		setAttribInRequest(request, "isExceptionOccured", new Boolean(value));
	}

	private static boolean isExceptionOccured(HttpServletRequest request) {
		return ((Boolean) getAttribInRequest(request, "isExceptionOccured")).booleanValue();
	}

	/**
	 * Main Execute Method For PFS Actions Sets the Succes Values 0-Not Applicable, 1-PFS process Success, 2-PFS save
	 * Success 3-Fail
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {

		String strHdnMode = request.getParameter(PARAM_MODE);
		String forward = WebConstants.FORWARD_SUCCESS;
		String strUIModeJS = "var isSearchMode = false; var strPFSContent;";
		setExceptionOccured(request, false);
		String strFormData = "var saveSuccess = " + getIntSuccess(request) + ";";
		strFormData += " var arrFormData = new Array();";
		request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
		request.setAttribute(WebConstants.REQ_OPERATION_UI_MODE, strUIModeJS);

		try {
			if ((strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_DELETE))
					|| (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_SAVE))
					|| (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_DELETE_ALL))) {
				processPSFData(request);
			}

			setDisplayData(request);
		} catch (Exception exception) {
			if (exception instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
			}
		}

		return forward;
	}

	/**
	 * Process the PFS
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return boolean processed-true not-false
	 * @throws Exception
	 *             the exception
	 */
	@SuppressWarnings({ "unused", "unchecked" })
	private static boolean processPSFData(HttpServletRequest request) throws Exception {

		String strCurrentPFS = null;
		String strCurrentPFSID = null;
		String strTitle = null;
		String strPaxType = null;
		String strFirstNAme = null;
		String strLastName = null;
		String strPNR = null;
		String strDestination = null;
		String strPAXStatus = null;
		String strPaxVersion = null;
		String strProcessStatus = null;
		String strGridRowNo = null;
		String strPaxId = null;
		String strHdnMode = "";
		String strHdnUIMode = "";
		String strFirstTime = "";
		String strNoPax = null;
		String strCC = null;
		String strPaxCat = null;
		String strPaxParent = "";
		String strIsParentPax = "";
		String strETicket = "";
		Integer pnrSegID = null;
		AuditorBD auditorBD = null;
		PFSAuditDTO pfsAuditDTO = null;

		try {
			strHdnMode = request.getParameter(PARAM_MODE);
			strHdnUIMode = AiradminUtils.getNotNullString(request.getParameter(PARAM_UI_MODE));
			strCurrentPFS = AiradminUtils.getNotNullString(request.getParameter(PARAM_CURRENT_PFS));
			strCurrentPFSID = AiradminUtils.getNotNullString(request.getParameter(PARAM_CURRENT_PFS_ID));
			strTitle = AiradminUtils.getNotNullString(request.getParameter(PARAM_TITLE)).trim();
			strPaxType = AiradminUtils.getNotNullString(request.getParameter(PARAM_PAX_TYPE)).trim();
			strFirstNAme = AiradminUtils.getNotNullString(request.getParameter(PARAM_FIRST_NAME));
			strLastName = AiradminUtils.getNotNullString(request.getParameter(PARAM_LAST_NAME));
			strPNR = AiradminUtils.getNotNullString(request.getParameter(PARAM_PNR)).trim();
			strDestination = AiradminUtils.getNotNullString(request.getParameter(PARAM_DESTINATION));
			strPAXStatus = AiradminUtils.getNotNullString(request.getParameter(PARAM_PAX_STATUS));
			strPaxVersion = AiradminUtils.getNotNullString(request.getParameter(PARAM_PAX_VERSION));
			strProcessStatus = AiradminUtils.getNotNullString(request.getParameter(PARAM_PROCESS_STATUS));
			strGridRowNo = AiradminUtils.getNotNullString(request.getParameter(PARAM_GRIDROW));
			strPaxId = AiradminUtils.getNotNullString(request.getParameter(PARAM_PAX_ID));
			strFirstTime = AiradminUtils.getNotNullString(request.getParameter(PARAM_FIRST_TIME));
			strNoPax = request.getParameter(PARAM_NO_PAX);
			strCC = AiradminUtils.getNotNullString(request.getParameter(PARAM_CABINCLAS));
			strPaxCat = AiradminUtils.getNotNullString(request.getParameter(PARAM_PAXCATEGORY));
			strPaxParent = AiradminUtils.getNotNullString(request.getParameter(PAX_INF_PARENT));
			strIsParentPax = AiradminUtils.getNotNullString(request.getParameter(PAX_IS_PARENT));
			strETicket = AiradminUtils.getNotNullString(request.getParameter(E_TICKET));
			pfsAuditDTO = createPfsAuditDTO(request);
			auditorBD = ModuleServiceLocator.getAuditorServiceBD();

			// If the eticket number is there and the pnr is empty. We need to locate the PNR
			// for the processing
			if (strETicket.length() > 0 && strPNR.length() == 0) {
				String pnr = BeanUtils.nullHandler(ModuleServiceLocator.getReservationQueryBD().getPNRByEticketNo(strETicket));

				if (pnr.length() == 0) {
					throw new ModuleException("airreservation.pfs.invalid.eticket.number");
				} else {
					strPNR = pnr;
				}
			}

			boolean isFlownOrNoShowPaxEntry = false;
			Pfs currentPfs = null;

			if (strCurrentPFSID != null && strCurrentPFSID != "") {
				currentPfs = ModuleServiceLocator.getReservationBD().getPFS(Integer.parseInt(strCurrentPFSID));
			}

			if (ReservationInternalConstants.PfsPaxStatus.NO_REC.equals(strPAXStatus)) {							
				isFlownOrNoShowPaxEntry = ModuleServiceLocator
						.getReservationBD().isFlownOrNoShowPaxEntry(
								strETicket,
								currentPfs.getFromAirport() + "/"
										+ strDestination);				
			}

			if (!isFlownOrNoShowPaxEntry) {
				String[] arrCurrentPFS = new String[0];

				if (!strHdnMode.equals("")
						&& (strHdnMode.equals(WebConstants.ACTION_SAVE) || strHdnMode.equals(WebConstants.ACTION_DELETE))) {
					int pnrValidation = 0;

					if (strCurrentPFS != null && !strCurrentPFS.equals("")) {
						// A PFS record is selected

						arrCurrentPFS = strCurrentPFS.split(",");

						// Get the PFS data set in the PFS form
						// Collection pfsDataSet = (Collection) getAttribInRequest(request,"PFSDataSet");
						// Find the current pfs record
						if (currentPfs == null) { // || strFirstTime.equals("")) { // removed OR to avoid Concurrent
													// update
													// issue.
							Collection<Pfs> pfsDataSet = (Collection<Pfs>) request.getSession().getAttribute("PFSDataSet");
							for (Iterator<Pfs> iterPfsData = pfsDataSet.iterator(); iterPfsData.hasNext();) {
								Pfs pfs = (Pfs) iterPfsData.next();
								if (pfs.getPpId() == Integer.valueOf(strCurrentPFSID).intValue()) {
									currentPfs = pfs;
									break;
								}
							}
						}

						PfsPaxEntry pfsPaxEntry = new PfsPaxEntry();
						PfsPaxEntry pfsInfantPaxEntry = new PfsPaxEntry();
						pfsPaxEntry.setArrivalAirport(strDestination);
						pfsPaxEntry.setCabinClassCode(strCC);
						pfsPaxEntry.setDepartureAirport(arrCurrentPFS[11]);
						pfsPaxEntry.setEntryStatus(strPAXStatus);
						pfsPaxEntry.setFirstName(strFirstNAme);
						pfsPaxEntry.setLastName(strLastName);
						pfsPaxEntry.setProcessedStatus("N");
						pfsPaxEntry.setTitle(strTitle);
						pfsPaxEntry.setPaxType(strPaxType);
						pfsPaxEntry.seteTicketNo(BeanUtils.nullHandler(strETicket));
						if (strPAXStatus.equalsIgnoreCase(ReservationInternalConstants.PfsPaxStatus.NO_REC) && strPNR != null
								&& !strPNR.equals("")) {
							// Check for same flight segment on future date with CNF booking.

							Reservation reservation = null;
							LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();

							pnrModesDTO.setPnr(strPNR);
							pnrModesDTO.setLoadPaxAvaBalance(true);
							pnrModesDTO.setLoadSegView(true);
							pnrModesDTO.setLoadFares(true);
							pnrModesDTO.setLoadSegViewBookingTypes(true);
							reservation = ModuleServiceLocator.getReservationBD().getReservation(pnrModesDTO, null);

							Collection<ReservationSegmentDTO> colSegements = reservation.getSegmentsView();
							for (ReservationSegmentDTO segdto : colSegements) {
								// Check for same segment
								// if(segdto.getSegmentCode().equalsIgnoreCase(arrCurrentPFS[11]+"/"+strDestination)){
								if (segdto.getOrigin().equalsIgnoreCase(arrCurrentPFS[11])
										&& segdto.getDestination().equalsIgnoreCase(strDestination)) { // AARESAA-5957:
																										// Only
																										// origin and
																										// destination
																										// will
																										// be checked to
																										// support
																										// multileg
									// Check for Confirmed booking which is not utilized (future flight)
									if (segdto.getStatus().equalsIgnoreCase(
											ReservationInternalConstants.ReservationPaxStatus.CONFIRMED)) {
										if (segdto.getDepartureDate().after(outputDateFormat.parse(arrCurrentPFS[2]))
												|| segdto.getDepartureDate().getTime() == outputDateFormat
														.parse(arrCurrentPFS[2]).getTime()) {
											if (!segdto.isOpenReturnSegment()) {
												// check weather Local or Zulu
												pfsPaxEntry.setFlightDate(segdto.getDepartureDate());
												pfsPaxEntry.setFlightNumber(segdto.getFlightNo());
												pnrSegID = segdto.getPnrSegId();
											} else {
												pnrValidation = 18;
											}
										} else {
											// Error:flight should be on future flight than the PFS processing flight
											pnrValidation = 8;
										}
									} else {
										// Error: passenger does not have confirmed segment
										pnrValidation = 9;
									}

								} else {
									// Error: Segment not matching
									pnrValidation = 10;
								}
							}
							// To handle multiple segments, if one matching segments available, should allow continue
							if (pfsPaxEntry.getFlightDate() != null) {
								pnrValidation = 0;
							}

						} else if (((strPAXStatus.equalsIgnoreCase(ReservationInternalConstants.PfsPaxStatus.NO_SHORE) || strPAXStatus
								.equalsIgnoreCase(ReservationInternalConstants.PfsPaxStatus.OFF_LD)))
								&& strPNR != null
								&& !strPNR.equals("")) {

							Reservation reservation = null;
							boolean segmentExists = false;
							LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();

							pnrModesDTO.setPnr(strPNR);
							pnrModesDTO.setLoadSegView(true);
							pnrModesDTO.setLoadSegViewBookingTypes(true);
							pnrModesDTO.setLoadPaxAvaBalance(true);
							pnrModesDTO.setLoadFares(true);
							pnrModesDTO.setLoadEtickets(true);

							reservation = ModuleServiceLocator.getReservationBD().getReservation(pnrModesDTO, null);

							Collection<ReservationSegmentDTO> colSegements = reservation.getSegmentsView();
							for (ReservationSegmentDTO segdto : colSegements) {
								// Check for same segment
								// if(segdto.getSegmentCode().equalsIgnoreCase(arrCurrentPFS[11]+"/"+strDestination)){
								if (segdto.getOrigin().equalsIgnoreCase(arrCurrentPFS[11])
										&& segdto.getDestination().equalsIgnoreCase(strDestination)) { // AARESAA-5957:
																										// Only
																										// origin and
																										// destination
																										// will
																										// be checked to
																										// support
																										// multileg
									// Check for Confirmed booking
									if (segdto.getStatus().equalsIgnoreCase(
											ReservationInternalConstants.ReservationPaxStatus.CONFIRMED)) {
										if (segdto.getDepartureDate().getTime() == outputDateFormat.parse(arrCurrentPFS[2])
												.getTime()) {
											segmentExists = true;
											pnrSegID = segdto.getPnrSegId();
											break;
										}
									}
								}
							}
							if (segmentExists) {
								pnrValidation = 0;
							} else {
								pnrValidation = 16;
							}

							if (pnrValidation == 0) {
								pax_loop: for (ReservationPax pax : reservation.getPassengers()) {
									for (EticketTO eticketTO : pax.geteTickets()) {
										if (eticketTO.getEticketNumber().equals(strETicket)
												&& eticketTO.getTicketStatus().equals(
														ReservationInternalConstants.PaxFareSegmentTypes.FLOWN)
												&& eticketTO.getPnrSegId().equals(pnrSegID) && (isSegmentModifiableAsPerETicketStatus(reservation,pnrSegID))) {
											pnrValidation = 19;
											break pax_loop;
										}
									}
								}

							}

							pfsPaxEntry.setFlightDate(outputDateFormat.parse(arrCurrentPFS[2]));
							pfsPaxEntry.setFlightNumber(arrCurrentPFS[1].toUpperCase());
						} else {
							pfsPaxEntry.setFlightDate(outputDateFormat.parse(arrCurrentPFS[2]));
							pfsPaxEntry.setFlightNumber(arrCurrentPFS[1].toUpperCase());
							if (AppSysParamsUtil.isDuplicateNameCheckEnabled() && !strHdnMode.equals("")
									&& strHdnMode.equals(WebConstants.ACTION_SAVE)) {
								ArrayList<String> adultNameList = new ArrayList<String>();
								ArrayList<String> childNameList = new ArrayList<String>();
								if (pfsPaxEntry.getPaxType().equals(ReservationInternalConstants.PassengerType.ADULT)
										&& pfsPaxEntry.getProcessedStatus().equals(
												ReservationInternalConstants.PfsProcessStatus.NOT_PROCESSED)) {
									String fullName = BeanUtils.nullHandler(pfsPaxEntry.getTitle())
											+ ReservationInternalConstants.DLIM
											+ BeanUtils.nullHandler(pfsPaxEntry.getFirstName())
											+ ReservationInternalConstants.DLIM
											+ BeanUtils.nullHandler(pfsPaxEntry.getLastName());

									if (fullName.length() > 0 && !fullName.contains("T B A")) {
										adultNameList.add(fullName);
									}
								} else if (pfsPaxEntry.getPaxType().equals(ReservationInternalConstants.PassengerType.CHILD)
										&& pfsPaxEntry.getProcessedStatus().equals(
												ReservationInternalConstants.PfsProcessStatus.NOT_PROCESSED)) {
									String fullName = BeanUtils.nullHandler(pfsPaxEntry.getTitle())
											+ ReservationInternalConstants.DLIM
											+ BeanUtils.nullHandler(pfsPaxEntry.getFirstName())
											+ ReservationInternalConstants.DLIM
											+ BeanUtils.nullHandler(pfsPaxEntry.getLastName());

									if (fullName.length() > 0 && !fullName.contains("T B A")) {
										childNameList.add(fullName);
									}
								}

								if (!(adultNameList.isEmpty() && childNameList.isEmpty())) {
									Collection<Integer> colFlightSegIDs = ModuleServiceLocator.getResSegmentBD()
											.getFlightSegmentIds(pfsPaxEntry.getFlightNumber(), pfsPaxEntry.getFlightDate(),
													pfsPaxEntry.getDepartureAirport(), pfsPaxEntry.getArrivalAirport());

									Collection<String> dupNameCollection = ModuleServiceLocator.getReservationQueryBD()
											.getDuplicateNameReservations(colFlightSegIDs, adultNameList, childNameList, null,
													null);

									if (!dupNameCollection.isEmpty()) {
										log.error("airreservations.arg.duplicateNamesExist:" + dupNameCollection.toString());
										throw new ModuleException(ResponseCodes.DUPLICATE_NAMES_IN_FLIGHT_SEGMENT,
												dupNameCollection);
									}
								}
							}
						}

						pfsPaxEntry.setPfsId(Integer.valueOf(strCurrentPFSID));
						pfsPaxEntry.setPnr(strPNR);
						if (!strPaxId.trim().equals("")) {
							pfsPaxEntry.setPpId(Integer.valueOf(strPaxId).intValue());
						}
						pfsPaxEntry.setReceivedDate(outputDateFormat.parse(arrCurrentPFS[0]));
						pfsPaxEntry.setPaxCategoryCode(strPaxCat);
						pfsPaxEntry.setIsParent(strIsParentPax);
						if (strPaxParent != null && !strPaxParent.equals("")) {
							pfsInfantPaxEntry = (PfsPaxEntry) ModuleServiceLocator.getReservationBD().getPfsInfantPaxParent(
									Integer.valueOf(strPaxParent));
							if (pfsInfantPaxEntry == null) {
								pfsPaxEntry.setParentPpId(Integer.valueOf(strPaxParent));
								if (strPAXStatus.equalsIgnoreCase(ReservationInternalConstants.PfsPaxStatus.NO_SHORE)) {
									pfsPaxEntry.setPnr(strPNR);
								} else {
									PfsPaxEntry parentPfsPaxEntry = (PfsPaxEntry) ModuleServiceLocator.getReservationBD()
											.getPfsPaxEntry(Integer.valueOf(strPaxParent));
									pfsPaxEntry.setPnr(parentPfsPaxEntry.getPnr());
								}
							} else {
								// throw exception
								log.error("PFSProcessingRequestHandler.processPSFData() method failed : ADULT WITH PAX");
								setErrorFormData(request, strCurrentPFS, strTitle, strPaxType, strFirstNAme, strLastName, strPNR,
										strDestination, strPAXStatus, strPaxVersion, strProcessStatus, strGridRowNo, strHdnMode,
										strHdnUIMode, strPaxId, strCurrentPFSID, strNoPax, strCC, strPaxCat, strETicket);
								saveMessage(request, airadminConfig.getMessage("um.pfsprocess.form.pfs.ParentPaxLimit"),
										WebConstants.MSG_ERROR);
								return true;
							}
						}

						if (!strHdnMode.equals("") && strHdnMode.equals(WebConstants.ACTION_SAVE)) {
							int psgrNo = 1;

							if (strNoPax != null && !strNoPax.trim().equals("")) {
								psgrNo = Integer.parseInt(strNoPax);
							}
							if (pnrValidation == 0) {
								pnrValidation = validatePNR(pfsPaxEntry, strHdnUIMode, pnrSegID);
							}

							if (pfsPaxEntry.getPaxType() != null && pfsPaxEntry.getPaxType().equals("IN")) {
								// we do not need to check parent id, if eticket number is available for infant
								if (pfsPaxEntry.getParentPpId() == null && strETicket.equals("")) {
									// For Infant Pax, Adult Pax is required
									pnrValidation = 15;
								}
							}

							if (pnrValidation == 0) {
								for (int i = 0; i < psgrNo; i++) {
									if (strPaxVersion != null && !strPaxVersion.equals(""))
										pfsPaxEntry.setVersion(Long.parseLong(strPaxVersion));
									ModuleServiceLocator.getReservationBD().savePfsParseEntry(currentPfs, pfsPaxEntry);
								}

								setIntSuccess(request, 0);
								saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS),
										WebConstants.MSG_SUCCESS);
								// Get latest currentPfs
								request.getSession().setAttribute(ACTIVE_PFS_ID, String.valueOf(currentPfs.getPpId()));
								auditorBD.preparePFSAudit(pfsAuditDTO, currentPfs.getPpId(),
										ReservationInternalConstants.AuditActionStatus.AUDIT_ACTION_PAX_SAVED, "User saved pax "
												+ String.valueOf(currentPfs.getPpId()));
								setExceptionOccured(request, false);
								log.debug("PFSProcessingRequestHandler.processPSFData() method is successfully executed.");

							} else {
								log.error("PFSProcessingRequestHandler.processPSFData() method failed : Invalid PNR");
								setIntSuccess(request, -5);
								setErrorFormData(request, strCurrentPFS, strTitle, strPaxType, strFirstNAme, strLastName, strPNR,
										strDestination, strPAXStatus, strPaxVersion, strProcessStatus, strGridRowNo, strHdnMode,
										strHdnUIMode, strPaxId, strCurrentPFSID, strNoPax, strCC, strPaxCat, strETicket);
								if (pnrValidation == 3) {
									// Passenger cannot identify in the PNR
									saveMessage(request, airadminConfig.getMessage("um.pfsprocess.form.pfs.pnr.pax.invalid"),
											WebConstants.MSG_ERROR);
									auditorBD.preparePFSAudit(pfsAuditDTO, Integer.valueOf(strCurrentPFSID),
											ReservationInternalConstants.AuditActionStatus.AUDIT_ACTION_PAX_SAVED,
											"Pax couldn't idenetify by the PNR ");

								} else if (pnrValidation == 5) {
									// Reservation already cancelled
									saveMessage(request,
											airadminConfig.getMessage("um.pfsprocess.form.pfs.reservation.cancelled"),
											WebConstants.MSG_ERROR);

									auditorBD.preparePFSAudit(pfsAuditDTO, Integer.valueOf(strCurrentPFSID),
											ReservationInternalConstants.AuditActionStatus.AUDIT_ACTION_PAX_SAVED,
											"Pax couldn't save reservation is already cancelled ");

								} else if (pnrValidation == 6) {
									// Many flights exists for the selected flight number,
									// departure date / time and the segment
									saveMessage(request, airadminConfig.getMessage("um.pfsprocess.form.pfs.ambiguous.flights"),
											WebConstants.MSG_ERROR);
									auditorBD
											.preparePFSAudit(pfsAuditDTO, Integer.valueOf(strCurrentPFSID),
													ReservationInternalConstants.AuditActionStatus.AUDIT_ACTION_PAX_SAVED,
													"Many flieghts avialable for the selected flight number/ departure date/ time and segment ");

								} else if (pnrValidation == 2) {
									// Segment is not available in reservation
									saveMessage(request, airadminConfig.getMessage("um.pfsprocess.form.pfs.no.segment"),
											WebConstants.MSG_ERROR);
									auditorBD.preparePFSAudit(pfsAuditDTO, Integer.valueOf(strCurrentPFSID),
											ReservationInternalConstants.AuditActionStatus.AUDIT_ACTION_PAX_SAVED,
											"Segment is not available in the reservation ");

								} else if (pnrValidation == 7) {
									// Segment is already cancelled
									saveMessage(request, airadminConfig.getMessage("um.pfsprocess.form.pfs.segment.cancelled"),
											WebConstants.MSG_ERROR);
									auditorBD.preparePFSAudit(pfsAuditDTO, Integer.valueOf(strCurrentPFSID),
											ReservationInternalConstants.AuditActionStatus.AUDIT_ACTION_PAX_SAVED,
											"Segment is already cancelled ");

								} else if (pnrValidation == 8) {
									// NOREC passenger should only from a present or future flights of the processing
									// flight
									saveMessage(request, airadminConfig.getMessage("um.pfsprocess.form.pfs.norec.flights"),
											WebConstants.MSG_ERROR);
									auditorBD.preparePFSAudit(pfsAuditDTO, Integer.valueOf(strCurrentPFSID),
											ReservationInternalConstants.AuditActionStatus.AUDIT_ACTION_PAX_SAVED,
											"Pax couldn't idenetify by the PNR ");

								} else if (pnrValidation == 9) {
									// No confirmed segment found for NOREC Passenger
									saveMessage(request, airadminConfig.getMessage("um.pfsprocess.form.pfs.norec.noconfirm"),
											WebConstants.MSG_ERROR);
									auditorBD.preparePFSAudit(pfsAuditDTO, Integer.valueOf(strCurrentPFSID),
											ReservationInternalConstants.AuditActionStatus.AUDIT_ACTION_PAX_SAVED,
											"No confirmed segment found for NOREC Passenger ");

								} else if (pnrValidation == 10) {
									// Origin and Destination should be same to accept NOREC Passenger
									saveMessage(request, airadminConfig.getMessage("um.pfsprocess.form.pfs.norec.segment"),
											WebConstants.MSG_ERROR);
									auditorBD.preparePFSAudit(pfsAuditDTO, Integer.valueOf(strCurrentPFSID),
											ReservationInternalConstants.AuditActionStatus.AUDIT_ACTION_PAX_SAVED,
											"Origin and Destination should be same to accept NOREC Passenger ");

								} else if (pnrValidation == 11) {
									// PFS ADULT Count cannot be greater with Reservation Adult Count
									saveMessage(request, airadminConfig.getMessage("um.pfsprocess.form.pfs.paxType.adultcount"),
											WebConstants.MSG_ERROR);
									auditorBD.preparePFSAudit(pfsAuditDTO, Integer.valueOf(strCurrentPFSID),
											ReservationInternalConstants.AuditActionStatus.AUDIT_ACTION_PAX_SAVED,
											"PFS ADULT Count cannot be greater with Reservation Adult Count ");

								} else if (pnrValidation == 12) {
									// PFS Child Count cannot be greater with Reservation Child Count
									saveMessage(request, airadminConfig.getMessage("um.pfsprocess.form.pfs.paxType.childcount"),
											WebConstants.MSG_ERROR);
									auditorBD.preparePFSAudit(pfsAuditDTO, Integer.valueOf(strCurrentPFSID),
											ReservationInternalConstants.AuditActionStatus.AUDIT_ACTION_PAX_SAVED,
											"PFS Child Count cannot be greater with Reservation Child Count ");

								} else if (pnrValidation == 13) {
									// PFS Child Passanger is being Duplicating
									saveMessage(request, airadminConfig.getMessage("um.pfsprocess.form.pfs.paxDuplicate"),
											WebConstants.MSG_ERROR);
									auditorBD.preparePFSAudit(pfsAuditDTO, Integer.valueOf(strCurrentPFSID),
											ReservationInternalConstants.AuditActionStatus.AUDIT_ACTION_PAX_SAVED,
											"PFS Child Passanger is being Duplicating ");

								} else if (pnrValidation == 14) {
									// Pax Cabin Class Code does not match
									saveMessage(request, airadminConfig.getMessage("um.pfsprocess.form.pfs.paxWrongCCCode"),
											WebConstants.MSG_ERROR);
									auditorBD.preparePFSAudit(pfsAuditDTO, Integer.valueOf(strCurrentPFSID),
											ReservationInternalConstants.AuditActionStatus.AUDIT_ACTION_PAX_SAVED,
											"pax Cabin Class Code does not match ");

								} else if (pnrValidation == 15) {
									// Inf Pax req Adult Parent
									saveMessage(request, airadminConfig.getMessage("um.pfsprocess.form.pfs.paxInfParentReq"),
											WebConstants.MSG_ERROR);
									auditorBD.preparePFSAudit(pfsAuditDTO, Integer.valueOf(strCurrentPFSID),
											ReservationInternalConstants.AuditActionStatus.AUDIT_ACTION_PAX_SAVED,
											"Error : Infant Pax req Adult Parent ");

								} else if (pnrValidation == 16) {
									// Segment is already canceled or an invalid segment.
									saveMessage(request, airadminConfig.getMessage("um.pfsprocess.form.pfs.invalidSegment"),
											WebConstants.MSG_ERROR);
									auditorBD.preparePFSAudit(pfsAuditDTO, Integer.valueOf(strCurrentPFSID),
											ReservationInternalConstants.AuditActionStatus.AUDIT_ACTION_PAX_SAVED,
											"Error : Pax couldn't idenetify by the PNR ");

								} else if (pnrValidation == 17) {
									// An invalid E-Ticket number.
									saveMessage(request, airadminConfig.getMessage("um.pfsprocess.form.pfs.invalidETicket"),
											WebConstants.MSG_ERROR);
									auditorBD.preparePFSAudit(pfsAuditDTO, Integer.valueOf(strCurrentPFSID),
											ReservationInternalConstants.AuditActionStatus.AUDIT_ACTION_PAX_SAVED,
											"Error : Pax couldn't idenetify by the PNR ");

								} else if (pnrValidation == 18) {
									// An open return segment found for norec
									saveMessage(request, airadminConfig.getMessage("um.pfsprocess.can.not.norec.openrt"),
											WebConstants.MSG_ERROR);
									auditorBD.preparePFSAudit(pfsAuditDTO, Integer.valueOf(strCurrentPFSID),
											ReservationInternalConstants.AuditActionStatus.AUDIT_ACTION_PAX_SAVED,
											airadminConfig.getMessage("um.pfsprocess.can.not.norec.openrt"));
								} else if (pnrValidation == 19) {
									// An open return segment found for norec
									saveMessage(request, airadminConfig.getMessage("um.pfsprocess.inconsistent.pax.status"),
											WebConstants.MSG_ERROR);
									auditorBD.preparePFSAudit(pfsAuditDTO, Integer.valueOf(strCurrentPFSID),
											ReservationInternalConstants.AuditActionStatus.AUDIT_ACTION_PAX_SAVED,
											airadminConfig.getMessage("um.pfsprocess.inconsistent.pax.status"));
								} else if (pnrValidation == 20) {
									// Code Share booking
									saveMessage(request, airadminConfig.getMessage("um.pfsprocess.codeshare.pnr.pax"),
											WebConstants.MSG_ERROR);
									auditorBD.preparePFSAudit(pfsAuditDTO, Integer.valueOf(strCurrentPFSID),
											ReservationInternalConstants.AuditActionStatus.AUDIT_ACTION_PAX_SAVED,
											airadminConfig.getMessage("um.pfsprocess.codeshare.pnr.pax"));	
								} else {
									// PNR segment is not valid
									// Passenger is not confirmed
									saveMessage(request,
											airadminConfig.getMessage("um.pfsprocess.form.pfs.pnr.pax.notconfirmed"),
											WebConstants.MSG_ERROR);
									auditorBD.preparePFSAudit(pfsAuditDTO, Integer.valueOf(strCurrentPFSID),
											ReservationInternalConstants.AuditActionStatus.AUDIT_ACTION_PAX_SAVED,
											"Error : PNR segment is not valid / Passenger is not confirmed");
								}

							}
						} else if (!strHdnMode.equals("") && strHdnMode.equals(WebConstants.ACTION_DELETE)) {
							// reservationAuxBD.deletePfsParsedEntry(strPaxId);
							if (strPaxVersion != null && !strPaxVersion.equals("")) {
								pfsPaxEntry.setVersion(Long.parseLong(strPaxVersion));
							}
							ModuleServiceLocator.getReservationBD().deletePfsParseEntry(currentPfs, pfsPaxEntry);
							setIntSuccess(request, 1);
							saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_DELETE_SUCCESS),
									WebConstants.MSG_SUCCESS);
							// Get latest currentPfs
							request.getSession().setAttribute(ACTIVE_PFS_ID, String.valueOf(currentPfs.getPpId()));
							setExceptionOccured(request, false);
							auditorBD.preparePFSAudit(pfsAuditDTO, currentPfs.getPpId(),
									ReservationInternalConstants.AuditActionStatus.AUDIT_ACTION_PAX_DELETED, "User deleted pax "
											+ String.valueOf(currentPfs.getPpId()));
							log.debug("PFSProcessingRequestHandler.processPSFData() method is successfully executed.");
						}
					}
				} else if ((!("").equals(strHdnMode)) && (strHdnMode.equals(WebConstants.ACTION_DELETE_ALL))) {
					ModuleServiceLocator.getReservationBD().deleteAllErrorEntries(currentPfs);
					setIntSuccess(request, 1);
					saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_DELETE_SUCCESS), WebConstants.MSG_SUCCESS);
					// Get latest currentPfs
					request.getSession().setAttribute(ACTIVE_PFS_ID, String.valueOf(currentPfs.getPpId()));
					setExceptionOccured(request, false);
					auditorBD.preparePFSAudit(pfsAuditDTO, currentPfs.getPpId(),
							ReservationInternalConstants.AuditActionStatus.AUDIT_ACTION_PAX_DELETED,
							"User deleted pax " + String.valueOf(currentPfs.getPpId()));
					log.debug("PFSProcessingRequestHandler.processPSFData() method is successfully executed.");

				}
			} else {
				throw new ModuleException("um.pfsprocess.form.pfs.pax.alreadyReconiled");
			}

		} catch (ModuleException moduleException) {
			log.error("PFSProcessingRequestHandler.processPSFData() method is failed :" + moduleException.getMessageString(),
					moduleException);
			// 4. PNR no. foriegn key validation should give ameaning full message.
			if (moduleException.getExceptionCode().equals("module.saveconstraint.childerror")) {
				setIntSuccess(request, -5);
				saveMessage(request, airadminConfig.getMessage("um.pfsprocess.form.pfs.pnr.invalid"), WebConstants.MSG_ERROR);
			} else if (moduleException.getExceptionCode().equals("airreservations.arg.noLongerExist")) {
				setIntSuccess(request, -5);
				saveMessage(request, airadminConfig.getMessage("um.pfsprocess.form.pfs.pnr.invalid"), WebConstants.MSG_ERROR);
			} else if (moduleException.getExceptionCode().equals("um.pfsprocess.form.pfs.pax.alreadyReconiled")) {
				setIntSuccess(request, -5);
				saveMessage(request, airadminConfig.getMessage("um.pfsprocess.form.pfs.pax.alreadyReconiled"),
						WebConstants.MSG_ERROR);
			} else {
				saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
			}
			setErrorFormData(request, strCurrentPFS, strTitle, strPaxType, strFirstNAme, strLastName, strPNR, strDestination,
					strPAXStatus, strPaxVersion, strProcessStatus, strGridRowNo, strHdnMode, strHdnUIMode, strPaxId,
					strCurrentPFSID, strNoPax, strCC, strPaxCat, strETicket);
			auditorBD.preparePFSAudit(pfsAuditDTO, Integer.valueOf(strCurrentPFSID),
					ReservationInternalConstants.AuditActionStatus.AUDIT_ACTION_SYSTEM_ERROR, "System error occured..");
		} catch (Exception exception) {
			setIntSuccess(request, -1);
			if (exception instanceof RuntimeException) {
				throw exception;
			}
		}

		return true;
	}

	private static PFSAuditDTO createPfsAuditDTO(HttpServletRequest request) {
		PFSAuditDTO pfsAuditDTO = new PFSAuditDTO(((UserPrincipal) request.getUserPrincipal()).getIpAddress(),
				((UserPrincipal) request.getUserPrincipal()).getUserId(), null, null);
		return pfsAuditDTO;
	}

	/**
	 * Sets the Form Data if An error occured & sets the Succes Int value 0-Not Applicable, 1-SendNewPNLSuccess,
	 * 2-SendNewADLSuccess 3-ResendPNLSuccess, 4-ResendADLSuccess 5-PrintSuccess, 6-Fail
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @param strCurrentPFS
	 *            the current PFS
	 * @param strTitle
	 *            the Title
	 * @param strFirstNAme
	 *            the First Name
	 * @param strLastName
	 *            the Last Name
	 * @param strPNR
	 *            the Reservation
	 * @param strDestination
	 *            the Destination
	 * @param strPAXStatus
	 *            the Passenger Status
	 * @param strPaxVersion
	 *            the Pax Version
	 * @param strProcessStatus
	 *            the PFS Process Status
	 * @param strGridRowNo
	 *            the Grid Row No
	 * @param strHdnMode
	 *            the Mode
	 * @param strHdnUIMode
	 *            the UI Mode
	 * @param strPaxId
	 *            the Passenger ID
	 * @param strCurrentPFSID
	 *            the Current PFS ID
	 * @param strPaxNo
	 *            the Passenger Number
	 */
	private static void setErrorFormData(HttpServletRequest request, String strCurrentPFS, String strTitle, String strPaxType,
			String strFirstNAme, String strLastName, String strPNR, String strDestination, String strPAXStatus,
			String strPaxVersion, String strProcessStatus, String strGridRowNo, String strHdnMode, String strHdnUIMode,
			String strPaxId, String strCurrentPFSID, String strPaxNo, String strCabin, String strPaxCat, String strETicket) {

		setExceptionOccured(request, true);

		String strFormData = "var arrFormData = new Array();";
		strFormData += "arrFormData[0] = '" + strCurrentPFS + "';";
		strFormData += "arrFormData[1] = '" + strTitle + "';";
		strFormData += "arrFormData[2] = '" + strFirstNAme + "';";
		strFormData += "arrFormData[3] = '" + strLastName + "';";
		strFormData += "arrFormData[4] = '" + strPNR + "';";
		strFormData += "arrFormData[5] = '" + strDestination + "';";
		strFormData += "arrFormData[6] = '" + strPAXStatus + "';";
		strFormData += "arrFormData[7] = '" + strPaxVersion + "';";
		strFormData += "arrFormData[8] = '" + strProcessStatus + "';";
		strFormData += "arrFormData[9] = '" + strGridRowNo + "';";
		strFormData += "arrFormData[10] = '" + strHdnMode + "';";
		strFormData += "arrFormData[11] = '" + strHdnUIMode + "';";
		strFormData += "arrFormData[12] = '" + strPaxId + "';";
		strFormData += "arrFormData[13] = '" + strCurrentPFSID + "';";
		strFormData += "arrFormData[14] = '" + strPaxNo + "';";
		strFormData += "arrFormData[15] = '" + strCabin + "';";
		strFormData += "arrFormData[16] = '" + strPaxCat + "';";
		strFormData += "arrFormData[17] = '" + strPaxType + "';";
		strFormData += "arrFormData[18] = '" + strETicket + "';";

		strFormData += " var saveSuccess = " + getIntSuccess(request) + ";";
		strFormData += " var strPFSContent = '';";
		request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);

	}

	/**
	 * Validate Reservation
	 * 
	 * @param strPNR
	 *            the Reservation
	 * @param strTitle
	 *            the Title
	 * @param strFirstName
	 *            the First Name
	 * @param strLastName
	 *            the Last Name
	 * @param flightNumber
	 *            the Flight Number
	 * @param departureDate
	 *            the Depature Date
	 * @param fromSegmentCode
	 *            the Origin
	 * @param toSegmentCode
	 *            the Destination
	 * @return int the success value 0-success 1-PNR segment is not valid 2-Segment is not available in reservation
	 *         3-Passenger cannot identify in the PNR 4-Passenger is not confirmed 5-Reservation Cancelled 6-Flight Id
	 *         is Not maching 7-Segment is already cancelled 11-Adult count in reservation donot match 12-Child count in
	 *         reservation donot match 13-Passanger record Duplicated
	 * @throws ModuleException
	 *             the ModuleException
	 */
	@SuppressWarnings("unused")
	private static int validatePNR(PfsPaxEntry pfsPaxEntry, String mode, Integer pnrSegID) throws ModuleException {

		if (pfsPaxEntry.getPnr() != null && !pfsPaxEntry.getPnr().equals("")) {

			FlightReconcileDTO flightReconcileDTO = null;
			LCCClientPnrModesDTO pnrModesDTO = new LCCClientPnrModesDTO();
			Reservation reservation = null;
			ReservationPax reservationPax = null;
			Collection<FlightReconcileDTO> colFlightReconcileDTO = null;

			pnrModesDTO.setPnr(pfsPaxEntry.getPnr());
			pnrModesDTO.setLoadPaxAvaBalance(true);
			pnrModesDTO.setLoadSegView(true);
			pnrModesDTO.setLoadFares(true);
			reservation = ModuleServiceLocator.getReservationBD().getReservation(pnrModesDTO, null);
			if (reservation.getStatus().equals(ReservationInternalConstants.ReservationStatus.CANCEL)) {
				return 5;
			}

			colFlightReconcileDTO = ModuleServiceLocator.getResSegmentBD().getPnrSegmentsForReconcilation(
					pfsPaxEntry.getFlightNumber(), pfsPaxEntry.getFlightDate(), pfsPaxEntry.getDepartureAirport(),
					pfsPaxEntry.getArrivalAirport(), pfsPaxEntry.getPnr());
			Integer flighSegId = null;
			Integer flightId = null;
			for (Iterator<FlightReconcileDTO> iterFlightReconcileDTO = colFlightReconcileDTO.iterator(); iterFlightReconcileDTO
					.hasNext();) {
				flightReconcileDTO = (FlightReconcileDTO) iterFlightReconcileDTO.next();
				if (flighSegId != null && !flighSegId.equals(flightReconcileDTO.getFlightSegId())) {
					return 6;
				}
				flighSegId = flightReconcileDTO.getFlightSegId();
				flightId = flightReconcileDTO.getFlightId();
			}
			if (flighSegId == null) {
				return 1;
			}

			// see flightsegid exist in reservation
			Set<ReservationSegment> segs = reservation.getSegments();
			ReservationSegment reservationSegment = null;
			for (Iterator<ReservationSegment> iterSegs = segs.iterator(); iterSegs.hasNext();) {
				reservationSegment = (ReservationSegment) iterSegs.next();
				if (reservationSegment.getFlightSegId().equals(flighSegId)
						&& !reservationSegment.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)) {
					break;
				}
			}
			if (reservationSegment == null) {
				return 2;
			} else if (reservationSegment.getStatus().equals(ReservationInternalConstants.ReservationSegmentStatus.CANCEL)) {
				return 7;
			}
			
			// MARKETING/CODESHARE FLIGHT INFORMATION
			if (ReservationApiUtils.isCodeShareReservation(reservation.getGdsId())
					&& !StringUtil.isNullOrEmpty(reservationSegment.getCodeShareFlightNo())
					&& !StringUtil.isNullOrEmpty(reservationSegment.getCodeShareBc())) {

//				return 20;
				 String segmentCode = flightReconcileDTO.getSegementCode();
				 String csFlightNo = reservationSegment.getCodeShareFlightNo();
				 String csBookingClass = reservationSegment.getCodeShareBc();
				 String cabinClass = reservationSegment.getCabinClassCode();
				
				 String marketingFlightInfo = ReservationApiUtils.getMarketingFlightData(csFlightNo, csBookingClass,
				 segmentCode,
				 flightReconcileDTO.getDepartureDateTimeLocal());
				
				 pfsPaxEntry.setMarketingFlightElement(marketingFlightInfo);
				 pfsPaxEntry.setExternalRecordLocator(reservation.getExternalRecordLocator());
				 pfsPaxEntry.setCodeShareBc(csBookingClass);
				 pfsPaxEntry.setCodeShareFlightNo(csFlightNo);
				 pfsPaxEntry.setCabinClassCode(cabinClass);
				 pfsPaxEntry.setGdsId(reservation.getGdsId());

			}		
			

			boolean resPaxFound = false;
			Set<ReservationPax> paxs = reservation.getPassengers();
			String strEticket = pfsPaxEntry.geteTicketNo();

			if (!"".equals(strEticket)) {
				if (!isPassengerDetailsForEticketNumberFound(paxs, pfsPaxEntry, strEticket)) {
					return 17;
				}
			}
			// Get the correct names as apear in PFS
			String strTitle = makeStringCompliant((pfsPaxEntry.getTitle() != null)
					? pfsPaxEntry.getTitle().toUpperCase()
					: pfsPaxEntry.getTitle());
			String strFirstName = makeStringCompliant(pfsPaxEntry.getFirstName());
			String strLastName = makeStringCompliant(pfsPaxEntry.getLastName());
			String strPaxType = makeStringCompliant(pfsPaxEntry.getPaxType());
			String strPnr = pfsPaxEntry.getPnr();

			int similarPaxCount = 0;
			int duplicatePfsPax = 0;

			for (Iterator<ReservationPax> iterPaxs = paxs.iterator(); iterPaxs.hasNext();) {
				reservationPax = (ReservationPax) iterPaxs.next();

				// Validate the passenger
				if (validatePassenger(strPaxType, strTitle, strFirstName, strLastName, reservationPax)) {
					resPaxFound = true;
					break;
				}
			}
			if (!resPaxFound && !ReservationInternalConstants.PfsPaxStatus.GO_SHORE.equals(pfsPaxEntry.getEntryStatus())) {
				return 3;
			}
			if (reservationPax == null || reservationPax.getTotalAvailableBalance().doubleValue() > 0) {
				return 4;// Passenger is not confirmed
			}

			// CHECK For Same Pax Details info
			for (Iterator<ReservationPax> itPax = paxs.iterator(); itPax.hasNext();) {
				reservationPax = (ReservationPax) itPax.next();
				if (strTitle.equalsIgnoreCase(makeStringCompliant(reservationPax.getTitle()))
						&& strFirstName.equalsIgnoreCase(makeStringCompliant(reservationPax.getFirstName()))
						&& strLastName.equalsIgnoreCase(makeStringCompliant(reservationPax.getLastName()))
						&& strPaxType.equalsIgnoreCase(makeStringCompliant(reservationPax.getPaxType()))) {
					similarPaxCount++;
				}
			}

			String resCabinClassCode = ModuleServiceLocator.getReservationBD().getReservationCabinClass(reservation.getPnr(),
					pnrSegID);
			if (!resCabinClassCode.equals(pfsPaxEntry.getCabinClassCode())) {
				return 14;
			}

			Collection<PfsPaxEntry> pfsPaxEntries = ModuleServiceLocator.getReservationBD().getPfsParseEntries(
					pfsPaxEntry.getPfsId());
			for (Iterator<PfsPaxEntry> itPfsEntry = pfsPaxEntries.iterator(); itPfsEntry.hasNext();) {
				PfsPaxEntry paxEntry = (PfsPaxEntry) itPfsEntry.next();
				if (strTitle.equalsIgnoreCase(makeStringCompliant(paxEntry.getTitle()))
						&& strFirstName.equalsIgnoreCase(makeStringCompliant(paxEntry.getFirstName()))
						&& strLastName.equalsIgnoreCase(makeStringCompliant(paxEntry.getLastName()))
						&& strPaxType.equalsIgnoreCase(makeStringCompliant(paxEntry.getPaxType()))
						&& strPnr.equalsIgnoreCase(paxEntry.getPnr())) {
					duplicatePfsPax++;
				}
			}
			if (mode != null && mode.equals("EDIT")) {
				if (duplicatePfsPax > 0 && duplicatePfsPax > similarPaxCount) {
					return 13;
				}
			} else if (duplicatePfsPax > 0 && duplicatePfsPax >= similarPaxCount) {
				return 13;
			}

			// This is not valid condition for NoShow and NoRec situations since split reservation happens.

			/*
			 * int resAdultPaxCount = reservation.getTotalPaxAdultCount(); int resChildPaxCount =
			 * reservation.getTotalPaxChildCount();
			 * 
			 * String pfsPaxType = pfsPaxEntry.getPaxType(); int pfsPaxTypeCount =
			 * ModuleServiceLocator.getReservationBD().getPFSPaxTypeEntryCount(pfsPaxEntry.getPfsId(),
			 * pfsPaxEntry.getPnr(), pfsPaxType);
			 * 
			 * if (mode != null && !mode.equals("EDIT")) { if (pfsPaxType.equals("AD")) { if (pfsPaxTypeCount >=
			 * resAdultPaxCount) { return 11; } } else if (pfsPaxType.equals("CH")) { if (pfsPaxTypeCount >=
			 * resChildPaxCount) { return 12; } } }
			 */

		} else
			return 0;

		return 0;
	}

	/**
	 * Validate the passenger with the Reservations Detail
	 * 
	 * @param strPaxType
	 *            the Pax Type
	 * @param strTitle
	 *            the Title
	 * @param strFirstName
	 *            the First Name
	 * @param strLastName
	 *            the Last Name
	 * @param reservationPax
	 *            the Passenger
	 * @return boolean the validated true false
	 */
	private static boolean validatePassenger(String strPaxType, String strTitle, String strFirstName, String strLastName,
			ReservationPax reservationPax) {
		boolean isValide = false;
		String paxTitle = makeStringCompliant(reservationPax.getTitle());
		String paxFName = makeStringCompliant(reservationPax.getFirstName());
		String paxLName = makeStringCompliant(reservationPax.getLastName());
		String paxType = makeStringCompliant(reservationPax.getPaxType());

		if (strTitle.equals(paxTitle) && strFirstName.equals(paxFName) && strLastName.equals(paxLName)
				&& strPaxType.equals(paxType)) {
			isValide = true;
		} else if (paxType != null && paxType.equals(PAX_CHILD_TYPE) && strTitle.equals("")
				&& paxTitle.equalsIgnoreCase(PAX_CHILD)) {
			if (strFirstName.equals(paxFName) && strLastName.equals(paxLName)) {
				isValide = true;// defect fix AARESAA-95
			}
		}
		return isValide;
	}

	private static boolean isPassengerDetailsForEticketNumberFound(Set<ReservationPax> paxs, PfsPaxEntry pfsPaxEntry,
			String strEticket) throws ModuleException {
		Integer pnrPaxID = ModuleServiceLocator.getPassengerBD().getPassengerIdByETicketNumber(strEticket);
		if (pnrPaxID != null) {
			ReservationPax reservationPax;
			for (Iterator<ReservationPax> iterPaxs = paxs.iterator(); iterPaxs.hasNext();) {
				reservationPax = (ReservationPax) iterPaxs.next();
				if (pnrPaxID.equals(reservationPax.getPnrPaxId())) {
					pfsPaxEntry.setTitle(reservationPax.getTitle());
					pfsPaxEntry.setFirstName(reservationPax.getFirstName());
					pfsPaxEntry.setLastName(reservationPax.getLastName());
					pfsPaxEntry.setPaxType(reservationPax.getPaxType());
					return true;
				}
			}
			return false;
		} else {
			return false;
		}
	}

	/**
	 * Validate the passenger with the PFS Entries Detail
	 * 
	 * @param strPaxType
	 *            the Pax Type
	 * @param strTitle
	 *            the Title
	 * @param strFirstName
	 *            the First Name
	 * @param strLastName
	 *            the Last Name
	 * @param reservationPax
	 *            the Passenger
	 * @return boolean the validated true false
	 */
	// private static boolean validatePassengerInPFSRecord(String strPaxType, String strTitle, String strFirstName,
	// String strLastName, String strPnr, PfsPaxEntry pfsPaxEntry) {
	// boolean isInValid = false;
	// String paxTitle = makeStringCompliant(pfsPaxEntry.getTitle());
	// String paxFName = makeStringCompliant(pfsPaxEntry.getFirstName());
	// String paxLName = makeStringCompliant(pfsPaxEntry.getLastName());
	// String paxType = makeStringCompliant(pfsPaxEntry.getPaxType());
	// String pnr = pfsPaxEntry.getPnr();
	// if (strTitle.equals(paxTitle) && strFirstName.equals(paxFName) && strLastName.equals(paxLName)
	// && strPaxType.equals(paxType) && strPnr.equals(pnr)) {
	// isInValid = true;
	// }
	// return isInValid;
	// }

	/**
	 * Sets the Display Data For PFS Processing Page & Succes int value //0-Not Applicable, 1-PFS process Success, 2-PFS
	 * save Success 3-Fail
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	private static void setDisplayData(HttpServletRequest request) throws Exception {
		setPFSProcessingRowHtml(request);
		setTitleListForAdult(request);
		setTitleListForChild(request);
		setInfantTitles(request);
		setPaxTypesList(request);
		setAirportList(request);
		setClientErrors(request);
		setDestinationAirportList(request);
		setCabinClassHtml(request);
		setPaxCategory(request);
		setPaxParents(request);
		String strFormData = "";
		if (!isExceptionOccured(request)) {
			strFormData = " var arrFormData = new Array();";
			strFormData += " var saveSuccess = " + getIntSuccess(request) + ";";
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
		}
	}

	/**
	 * Sets the Destination airport list to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	private static void setDestinationAirportList(HttpServletRequest request) throws Exception {

		try {
			StringBuffer sb = new StringBuffer();

			String strCurrentPFS = request.getParameter(PARAM_CURRENT_PFS) == null ? "" : request.getParameter(PARAM_CURRENT_PFS);
			String[] arrCurrentPFS = new String[0];
			if (strCurrentPFS != null && !strCurrentPFS.equals("")) {
				// A PFS record is selected
				arrCurrentPFS = strCurrentPFS.split(",");

				List<String> l = ModuleServiceLocator.getFlightServiceBD().getPossibleDestinations(
						arrCurrentPFS[1].toUpperCase(), outputDateFormat.parse(arrCurrentPFS[2]), arrCurrentPFS[11]);

				Iterator<String> iter = l.iterator();
				while (iter.hasNext()) {
					String s = (String) iter.next();

					sb.append("<option value='" + s + "'>" + s + "</option>");
				}
				request.setAttribute(WebConstants.REQ_HTML_AIRPORT_COMBO, sb.toString());
			} else {
				request.setAttribute(WebConstants.REQ_HTML_AIRPORT_COMBO, SelectListGenerator.createActiveAirportCodeList());
			}

		} catch (ModuleException moduleException) {
			log.error("SendPNLADLRequestHandler.setAirportList() method is failed :" + moduleException.getMessageString(),
					moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);

		}
	}

	/**
	 * Sets the Title list to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	private static void setTitleListForAdult(HttpServletRequest request) throws Exception {
		String arrTitles = SelectListGenerator.createPaxTitleArrayForAdult();
		request.setAttribute(WebConstants.PAX_TITLES, arrTitles);
	}

	private static void setTitleListForChild(HttpServletRequest request) throws Exception {
		String arrTitles = SelectListGenerator.createPaxTitleArrayWithChild();
		request.setAttribute(WebConstants.PAX_TITLES_FOR_CHD, arrTitles);
	}

	private static void setInfantTitles(HttpServletRequest request) throws Exception {
		String arrInfantTitles = SelectListGenerator.createInfantTitlesArray();
		request.setAttribute(WebConstants.INFANT_TITLES, arrInfantTitles);
	}

	private static void setPaxTypesList(HttpServletRequest request) throws ModuleException {
		String arrPaxTypes = SelectListGenerator.createPaxTypesArray();
		request.setAttribute(WebConstants.PAX_TYPE, arrPaxTypes);
	}

	/**
	 * Sets the Active Online Airport List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setAirportList(HttpServletRequest request) {
		try {
			String strHtml = SelectListGenerator.createActiveAirportCodeList();
			request.setAttribute(WebConstants.REQ_HTML_AIRPORT_LIST, strHtml);
		} catch (ModuleException moduleException) {
			log.error("SendPNLADLRequestHandler.setAirportList() method is failed :" + moduleException.getMessageString(),
					moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Client Validations to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = PFSDetailProcessingHTMLGenerator.getClientErrors(request);
			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("PFSProcessingRequestHandler.setClientErrors() method is failed :" + moduleException.getMessageString(),
					moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the PFS Processing rows to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	protected static void setPFSProcessingRowHtml(HttpServletRequest request) throws Exception {
		try {
			PFSDetailProcessingHTMLGenerator htmlGen = new PFSDetailProcessingHTMLGenerator();
			String strHtml = htmlGen.getPFSProcessingRowHtml(request);

			String strFirstTime = request.getParameter(PARAM_FIRST_TIME) == null ? "" : request.getParameter(PARAM_FIRST_TIME);
			int currentPfsId = 0;
			Pfs pfs = null;
			// Object sesActivePFS = request.getSession().getAttribute(ACTIVE_PFS_ID);
			Object activePFS = request.getParameter(PARAM_CURRENT_PFS_ID);
			if (activePFS != null) {
				currentPfsId = Integer.parseInt(activePFS.toString());
			}
			if (currentPfsId != 0) {
				pfs = ModuleServiceLocator.getReservationBD().getPFS(currentPfsId);
			}
			if (pfs != null && strFirstTime.equals("")) {
				strHtml += " pfsState = '" + pfs.getProcessedStatus() + "'; ";
			}
			request.setAttribute(WebConstants.REQ_HTML_ROWS, strHtml);

			request.setAttribute(WebConstants.REQ_FORM_FIELD_VALUES, htmlGen.getFormFieldValues());
		} catch (ModuleException moduleException) {
			log.error("PFSProcessingHandler.setPFSProcessingRowHtml() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Making the String Compatible Spaces
	 * 
	 * @param value
	 *            the string to make compatible
	 * @return String the compatible String
	 */
	private static String makeStringCompliant(String value) {
		char[] charArray = PlatformUtiltiies.nullHandler(value).toUpperCase().toCharArray();
		for (int i = 0; i < charArray.length; i++) {
			int ascii = (int) charArray[i];
			if (ascii < 65 | ascii > 90) {
				charArray[i] = ' ';
			}
		}
		String g = new String(charArray);
		return g.replaceAll(" ", "");
	}

	/**
	 * Sets the Cabinclasses List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setCabinClassHtml(HttpServletRequest request) throws ModuleException {
		String strCabinClassList = SelectListGenerator.createCabinClassList();
		request.setAttribute(WebConstants.REQ_HTML_CABINCLASS_LIST, strCabinClassList);
	}

	/**
	 * Sets Pax Category Type to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setPaxCategory(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createPaxCatagoryList();
		request.setAttribute(WebConstants.REQ_PAXCAT_LIST, strHtml);
	}

	// private static String getCabinClassCode(String pnr) {
	//
	// /**
	// *
	// select bc.cabin_class_code from T_reservation r, t_pnr_segment ps, t_pnr_pax_fare_segment ppfs,
	// * t_pnr_pax_ond_charges ppoc, t_ond_fare oc, t_booking_class bc where r.pnr = 21327538 and r.pnr = ps.pnr and
	// * ps.pnr_seg_id = ppfs.pnr_seg_id and ppfs.ppf_id = ppoc.ppf_id and ppoc.fare_id = oc.fare_id and
	// * oc.booking_code = bc.booking_code
	// *
	// *
	// *
	// * */
	//
	// return "";
	// }

	private static void setPaxParents(HttpServletRequest request) throws ModuleException {
		PFSDetailProcessingHTMLGenerator htmlGen = new PFSDetailProcessingHTMLGenerator();
		String paxInfParentHtml = htmlGen.getInfantPaxParentHTML(request);
		request.setAttribute(WebConstants.REQ_PAX_PARENT, paxInfParentHtml);
	}
	
	public static boolean isSegmentModifiableAsPerETicketStatus(Reservation reservation, Integer pnrSegId) {
		if (AppSysParamsUtil.isRestrictSegmentModificationByETicketStatus()) {
			for (ReservationPax pax : reservation.getPassengers()) {
				for (EticketTO eTicket : pax.geteTickets()) {
					if (eTicket.getPnrSegId().intValue() == pnrSegId.intValue()) {
						if (EticketStatus.CHECKEDIN.code().equals(eTicket.getTicketStatus())
								|| EticketStatus.BOARDED.code().equals(eTicket.getTicketStatus())) {
							return false;
						}
					}
				}
			}
		}
		return true;
	}
}
