package com.isa.thinair.airadmin.core.web.v2.action.master;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.v2.action.common.CommonAdminRequestResponseAwareCommonAction;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airmaster.api.criteria.AutomaticCheckinTemplatesSearchCriteria;
import com.isa.thinair.airmaster.api.model.AutoCheckinTemplateAirport;
import com.isa.thinair.airmaster.api.model.AutomaticCheckinTemplate;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.util.DataUtil;
import com.isa.thinair.webplatform.core.util.RowDecorator;

/**
 * @author Mohamed
 *
 */
@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ShowAutoCheckInTemplateAction extends CommonAdminRequestResponseAwareCommonAction {

	private static final Log log = LogFactory.getLog(ShowAutoCheckInTemplateAction.class);
	private static final int PAGE_LENGTH = 10;

	private int page;
	private int total;
	private int records;
	private Collection<Map<String, Object>> rows;
	private String succesMsg;
	private String errorMsg;
	private String msgType;

	private int selTemplateId;
	private String selStatus;
	private AutomaticCheckinTemplatesSearchCriteria actSearchCriteria;

	private Integer automaticCheckinTemplateId;
	private String airportCode;
	private String status;
	private BigDecimal amount;
	private String selAirportLst;
	private String AUTOCHECKIN = "autoCheckin";
	private String TEMPLATE_ID = "autoCheckin.templateId";
	private String AIRPORTLIST = "autoCheckin.airportlist";
	private String AMOUNT = "autoCheckin.amount";
	private String STATUS = "autoCheckin.status";

	public String searchACITemplates() throws ModuleException {

		try {
			init();
			int startIndex = (page - 1) * PAGE_LENGTH;
			Page<AutomaticCheckinTemplate> autoCheckinTemplate = ModuleServiceLocator.getAutomaticCheckinTemplateBD()
					.getAutomaticCheckinTemplates(startIndex, PAGE_LENGTH, actSearchCriteria);
			bindData(autoCheckinTemplate);
		} catch (Exception e) {
			log.info("Error in searching airport transfers", e);
		}
		return S2Constants.Result.SUCCESS;

	}

	public String execute() {
		try {
			if (validateAutoCheckin()) {
				ModuleServiceLocator.getAutomaticCheckinTemplateBD().saveAutomaticCheckinTemplate(
						createOrEditAutoCheckinTemplate());
				this.setSuccesMsg(airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS));
				this.msgType = WebConstants.MSG_SUCCESS;
			}
		} catch (Exception e) {
			this.msgType = WebConstants.MSG_ERROR;
			this.handleException(e);
			log.error("Error in saving automatic checkin template", e);
		}
		return S2Constants.Result.SUCCESS;
	}

	/**
	 * This is used to validate to avoid duplicate templates
	 * 
	 * @return
	 * @throws ModuleException
	 */
	private boolean validateAutoCheckin() throws ModuleException {
		boolean valid = true;
		AutomaticCheckinTemplate persistedAutoCheckinTemplate;
		if (selAirportLst != null) {
			String[] airportCodeArr = selAirportLst.split(",");
			List<String> airportCol = Arrays.stream(airportCodeArr).collect(Collectors.toList());
			if (automaticCheckinTemplateId == null) {
				valid = validateIsAirportAlreadyExist(valid, airportCol);
			} else {
				persistedAutoCheckinTemplate = ModuleServiceLocator.getAutomaticCheckinTemplateBD().getAutomaticCheckinTemplate(
						automaticCheckinTemplateId);
				for (AutoCheckinTemplateAirport checkinTemplateAirport : persistedAutoCheckinTemplate
						.getAutoCheckinTemplateAirport()) {
					if (airportCol.contains(checkinTemplateAirport.getAirportCode())) {
						airportCol.remove(checkinTemplateAirport.getAirportCode());
					}
				}
				valid = validateIsAirportAlreadyExist(valid, airportCol);
			}
		}
		return valid;
	}

	/**
	 * This method will check and validate already exist or not for given airport list
	 * 
	 * @param valid
	 * @param airportCol
	 * @return
	 * @throws ModuleException
	 */
	private boolean validateIsAirportAlreadyExist(boolean valid, List<String> airportCol) throws ModuleException {
		List<String> persistedAutoCheckinAirports;
		if (!airportCol.isEmpty()) {
			persistedAutoCheckinAirports = ModuleServiceLocator.getAutomaticCheckinTemplateBD().getAutoCheckinAirportList(
					airportCol);
			if (persistedAutoCheckinAirports != null && !persistedAutoCheckinAirports.isEmpty()) {
				valid = false;
				this.setErrorMsg(airadminConfig.getMessage(WebConstants.REQ_ERROR_AIRPORT_CODE_EXIST,
						new Object[] { persistedAutoCheckinAirports }));
				this.msgType = WebConstants.MSG_ERROR;
			}
		}
		return valid;
	}

	/**
	 * This method will set the Search Criteria based on request
	 */
	private AutomaticCheckinTemplatesSearchCriteria init() {
		actSearchCriteria = new AutomaticCheckinTemplatesSearchCriteria();
		if (airportCode != null && !airportCode.isEmpty()) {
			actSearchCriteria.setAirportCode(airportCode);
		}
		if (selStatus != null && !selStatus.isEmpty() && !selStatus.equals(CommonsConstants.ALL_DESIGNATOR_STRING)) {
			actSearchCriteria.setStatus(selStatus);
		}

		if (page < 1) {
			page = 1;
		}
		return actSearchCriteria;
	}

	private void bindData(Page<AutomaticCheckinTemplate> autoCheckinTemplate) {
		if (autoCheckinTemplate != null) {
			this.page = getCurrentPageNo(autoCheckinTemplate.getStartPosition());
			this.total = getTotalNoOfPages(autoCheckinTemplate.getTotalNoOfRecords());
			this.records = autoCheckinTemplate.getTotalNoOfRecords();
			this.rows = (Collection<Map<String, Object>>) DataUtil.createGridData(autoCheckinTemplate.getStartPosition(),
					AUTOCHECKIN, autoCheckinTemplate.getPageData(), this.decorateACIRow());

		}
	}

	private RowDecorator<AutomaticCheckinTemplate> decorateACIRow() {
		try {
			RowDecorator<AutomaticCheckinTemplate> aciRows = new RowDecorator<AutomaticCheckinTemplate>() {
				@Override
				public void decorateRow(HashMap<String, Object> row, AutomaticCheckinTemplate record) {
					row.put(TEMPLATE_ID, record.getAutomaticCheckinTemplateId());
					row.put(AIRPORTLIST, buildAirportList(record.getAutoCheckinTemplateAirport()));
					row.put(AMOUNT, record.getAmount());
					row.put(STATUS, record.getStatus());
				}
			};
			return aciRows;
		} catch (Exception e) {
			log.error("expection ---" + e.getMessage());
		}
		return null;
	}

	private String buildAirportList(Set<AutoCheckinTemplateAirport> autoCheckinTemplateAirport) {
		StringBuilder sb = new StringBuilder();
		String airportList;
		for (AutoCheckinTemplateAirport acta : autoCheckinTemplateAirport) {
			sb.append(acta.getAirportCode()).append(",");
		}
		airportList = sb.substring(0, sb.length() - 1);
		return airportList;
	}

	private int getCurrentPageNo(int startPosition) {
		int currentPageNo = 1;
		if (PAGE_LENGTH != 0) {
			currentPageNo = startPosition / PAGE_LENGTH + 1;
		}
		return currentPageNo;
	}

	private int getTotalNoOfPages(int totoalNoOfRecords) {
		int totalNoOfPages = 1;
		if (PAGE_LENGTH != 0) {
			totalNoOfPages = (int) totoalNoOfRecords / PAGE_LENGTH;
			if (totoalNoOfRecords % PAGE_LENGTH > 0)
				totalNoOfPages++;
		}
		return totalNoOfPages;
	}

	/**
	 * This method is used to create or edit template for auto check-in ancillary
	 * 
	 * @return
	 * @throws ModuleException
	 */
	private AutomaticCheckinTemplate createOrEditAutoCheckinTemplate() throws ModuleException {
		AutomaticCheckinTemplate automaticCheckinTemplate = new AutomaticCheckinTemplate();
		Set<AutoCheckinTemplateAirport> autoCheckinTempAirportList;

		if (!selAirportLst.isEmpty()) {
			String[] airportCodes = selAirportLst.split(",");

			if (automaticCheckinTemplateId != null) {
				// removing old airport list in-case-of edit functionality
				automaticCheckinTemplate = removeOldTemplateAirportList(automaticCheckinTemplate);
			}
			automaticCheckinTemplate.setAmount(amount);
			if (status != null) {
				automaticCheckinTemplate.setStatus(status);
			} else {
				automaticCheckinTemplate.setStatus(CommonsConstants.STATUS_INACTIVE);
			}
			autoCheckinTempAirportList = new HashSet<AutoCheckinTemplateAirport>();
			for (String airportCode : airportCodes) {
				AutoCheckinTemplateAirport autoCheckinTempAirport = new AutoCheckinTemplateAirport();
				autoCheckinTempAirport.setAutomaticCheckinTemplate(automaticCheckinTemplate);
				autoCheckinTempAirport.setAirportCode(airportCode);
				autoCheckinTempAirportList.add(autoCheckinTempAirport);
			}
			automaticCheckinTemplate.setAutoCheckinTemplateAirport(autoCheckinTempAirportList);
		}
		return automaticCheckinTemplate;
	}

	/**
	 * Clearing existing airport codes to avoid duplicates
	 * 
	 * @param autoCheckinTemp
	 * @return
	 */
	private AutomaticCheckinTemplate removeOldTemplateAirportList(AutomaticCheckinTemplate autoCheckinTemp)
			throws ModuleException {
		List<AutoCheckinTemplateAirport> templateAirportlist;
		autoCheckinTemp = ModuleServiceLocator.getAutomaticCheckinTemplateBD().getAutomaticCheckinTemplate(
				automaticCheckinTemplateId);
		templateAirportlist = ModuleServiceLocator.getAutomaticCheckinTemplateBD().getAutomaticCheckinAirports(
				automaticCheckinTemplateId);
		autoCheckinTemp.getAutoCheckinTemplateAirport().removeAll(templateAirportlist);
		autoCheckinTemp = ModuleServiceLocator.getAutomaticCheckinTemplateBD().updateAutomaticCheckinTemplate(autoCheckinTemp);
		return autoCheckinTemp;
	}

	@Override
	public String getMessage() {
		return message;
	}

	@Override
	public void setMessage(String message) {
		this.message = message;

	}

	@Override
	public String getMsgType() {
		return msgType;
	}

	@Override
	public void setMsgType(String msgType) {
		this.msgType = msgType;

	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public Collection<Map<String, Object>> getRows() {
		return rows;
	}

	public void setRows(Collection<Map<String, Object>> rows) {
		this.rows = rows;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getSelTemplateId() {
		return selTemplateId;
	}

	public void setSelTemplateId(int selTemplateId) {
		this.selTemplateId = selTemplateId;
	}

	public String getSelStatus() {
		return selStatus;
	}

	public void setSelStatus(String selStatus) {
		this.selStatus = selStatus;
	}

	public AutomaticCheckinTemplatesSearchCriteria getActSearchCriteria() {
		return actSearchCriteria;
	}

	public void setActSearchCriteria(AutomaticCheckinTemplatesSearchCriteria actSearchCriteria) {
		this.actSearchCriteria = actSearchCriteria;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getSelAirportLst() {
		return selAirportLst;
	}

	public void setSelAirportLst(String selAirportLst) {
		this.selAirportLst = selAirportLst;
	}

	public String getAirportCode() {
		return airportCode;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}

	/**
	 * @return the succesMsg
	 */
	public String getSuccesMsg() {
		return succesMsg;
	}

	/**
	 * @param succesMsg
	 *            the succesMsg to set
	 */
	public void setSuccesMsg(String succesMsg) {
		this.succesMsg = succesMsg;
	}

	/**
	 * @return the errorMsg
	 */
	public String getErrorMsg() {
		return errorMsg;
	}

	/**
	 * @param errorMsg
	 *            the errorMsg to set
	 */
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	/**
	 * @return the automaticCheckinTemplateId
	 */
	public Integer getAutomaticCheckinTemplateId() {
		return automaticCheckinTemplateId;
	}

	/**
	 * @param automaticCheckinTemplateId
	 *            the automaticCheckinTemplateId to set
	 */
	public void setAutomaticCheckinTemplateId(Integer automaticCheckinTemplateId) {
		this.automaticCheckinTemplateId = automaticCheckinTemplateId;
	}

}
