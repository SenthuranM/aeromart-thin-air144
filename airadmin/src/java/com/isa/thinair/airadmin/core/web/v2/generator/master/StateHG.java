package com.isa.thinair.airadmin.core.web.v2.generator.master;

import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.commons.api.exception.ModuleException;

public class StateHG {

	private static String clientErrors;

	/**
	 * Create Client Validations for State Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array of Client validations
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {

		if (clientErrors == null) {
			Properties moduleErrs = new Properties();
			moduleErrs.setProperty("um.State.form.id.required", "deleteRecoredCfrm");
			moduleErrs.setProperty("um.State.form.description.required", "descriptionRequired");
			moduleErrs.setProperty("um.State.form.id.already.exist", "idExists");
			moduleErrs.setProperty("um.State.form.id.length", "lengthInvalid");
			moduleErrs.setProperty("um.State.form.Territory.Code.required", "territoryReq");
			moduleErrs.setProperty("um.State.form.Country.Code.required", "countryCodeReq");
			moduleErrs.setProperty("um.State.form.row.required", "segmentRqrd");
			moduleErrs.setProperty("um.State.form.fields.required", "fieldsRequire");
			moduleErrs.setProperty("um.State.form.state.code.required", "stateCodeRequire");
			moduleErrs.setProperty("um.State.form.state.name.required", "stateNameRequire");
			moduleErrs.setProperty("um.State.form.state.country.required", "stateCountryRequire");
			moduleErrs.setProperty("um.State.form.arl.street.required", "arlOffStAdressfieldsRequire");
			moduleErrs.setProperty("um.State.form.arl.city.required", "arlOffcityfieldsRequire");
			moduleErrs.setProperty("um.State.form.arl.taxReg.required", "arlOffTaxRegfieldsRequire");
			moduleErrs.setProperty("um.State.form.signature.length", "signatureLengthExceeds");
			moduleErrs.setProperty("um.State.system.delete.required", "selectRecord");
			moduleErrs.setProperty("um.State.system.error", "sysError");
			clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}

		return clientErrors;
	}
}
