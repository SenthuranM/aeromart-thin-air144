package com.isa.thinair.airadmin.core.web.v2.handler.master;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airpricing.api.criteria.ONDCodeSearchCriteria;
import com.isa.thinair.airpricing.api.model.ONDBaggageChargeTemplate;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author Kasun
 * 
 */
public class ONDCodeRH {

	public static Page searchONDCodes(int pageNo, ONDCodeSearchCriteria criteria) throws ModuleException {
		return ModuleServiceLocator.getChargeBD().searchBaggageTemplates(criteria, (pageNo - 1) * 20, 20);
	}

	public static void saveONDCodes(ONDBaggageChargeTemplate baggageChargeTemplate) throws ModuleException {
		ModuleServiceLocator.getChargeBD().saveONDCodes(baggageChargeTemplate, null);
	}

	public static void deleteONDCodes(ONDBaggageChargeTemplate baggageChargeTemplate) throws ModuleException {
		ModuleServiceLocator.getChargeBD().deleteONDCodes(baggageChargeTemplate);
	}
}
