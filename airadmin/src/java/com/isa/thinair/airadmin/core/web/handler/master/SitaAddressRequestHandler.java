/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */

/**
 * @author Srikantha
 *
 */

package com.isa.thinair.airadmin.core.web.handler.master;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.master.SitaAddressHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airmaster.api.model.Airport;
import com.isa.thinair.airmaster.api.model.SITAAddress;
import com.isa.thinair.commons.api.exception.MessagesUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author srikantha
 * 
 *         Preferences - Java - Code Style - Code Templates
 */

public final class SitaAddressRequestHandler extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(SitaAddressRequestHandler.class);

	private static AiradminConfig airadminConfig = new AiradminConfig();

	private static final String PARAM_VERSION = "hdnVersion";
	private static final String PARAM_GRIDROW = "hdnGridRow";
	private static final String PARAM_SITA_ID = "hdnSitaId";
	private static final String PARAM_SITA_AIRPORTCODE = "selFormAirport";
	private static final String PARAM_LOCATION_DESCRIPTION = "txtLocation";
	private static final String PARAM_SITA_ADDRESS = "txtFormSitaAddress";
	private static final String PARAM_ACTIVE_STATUS = "chkStatus";
	private static final String PARAM_MODE = "hdnMode";
	private static final String PARAM_EMAIL = "txtEmail";
	private static final String PARAM_CARRIER_CODES = "hdnCarrierCodes";
	private static final String PARAM_SITA_DELIVERY_METHOD = "selDeliveryMethod";
	private static final String PARAM_SITA_ALLOW_PNLADL = "allowPnlAdl";
	private static final String PARAM_SITA_ALLOW_PALCAL = "allowPalCal";
	private static final String PARAM_OND_CODES = "hdnOndCodes";
	private static final String PARAM_FLIGHT_NUMBERS = "hdnFlightNumbers";

	private static String strHdnMode;

	private static int intSuccess = 0; // 0-Not
										// Applicable,
										// 1-Success,
										// 2-Fail

	private static void setExceptionOccured(HttpServletRequest request, boolean value) {
		setAttribInRequest(request, "isExceptionOccured", new Boolean(value));
	}

	private static boolean isExceptionOccured(HttpServletRequest request) {
		return ((Boolean) getAttribInRequest(request, "isExceptionOccured")).booleanValue();
	}

	private static int checkValidAirport(String airportId) throws ModuleException {
		// Returns
		// 0- Success
		// 1- The changed Airport inactive

		Airport airport = ModuleServiceLocator.getAirportServiceBD().getAirport(airportId);
		if (airport != null)
			return airport.getStatus().equals(Airport.STATUS_ACTIVE) ? 0 : 1;
		else
			return 0;
	}

	private static boolean isSuccessfulSaveMessageDisplayEnabled() {
		boolean isMessageEnabled = false;
		if (airadminConfig.getMessage("um.airadmin.savemessage.display").equals("true")) {
			isMessageEnabled = true;
		}
		return isMessageEnabled;
	}

	/**
	 * Main Execute Method For SITA Address Actions & sets the Succes int 0-Not Applicable, 1-Success, 2-Fail";
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {

		strHdnMode = request.getParameter(PARAM_MODE);
		String forward = WebConstants.FORWARD_SUCCESS;

		if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_GRID)) {
			return WebConstants.ACTION_GRID;
		}
		log.debug("Mode: " + strHdnMode);
		intSuccess = 0;
		String strFormData = "var saveSuccess = " + intSuccess + ";";
		strFormData += " var arrFormData = new Array();";
		request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);

		setExceptionOccured(request, false);
		setAttribInRequest(request, "strModeJS", "var isAddMode = false;");
		setAttribInRequest(request, "strAirportFocusJS", "var isAirportFocus = false;");
		setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = false;");
		setAttribInRequest(request, "isPalCalEnabledJS", "var isPalCalEnabled = " + AppSysParamsUtil.isEnablePalCalMessage()
				+ ";");

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_ADD)) {
				checkPrivilege(request, WebConstants.PRIV_SYS_MAS_SITA_ADD);

				setAttribInRequest(request, "strModeJS", "var isAddMode = true;");
				if (saveData(request)) {

					if (!isExceptionOccured(request)) {
						intSuccess = 1;
						strFormData = " var saveSuccess = " + intSuccess + ";";
						strFormData += " var arrFormData = new Array();";
						request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
					}
				}
			}
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_EDIT)) {
				checkPrivilege(request, WebConstants.PRIV_SYS_MAS_SITA_EDIT);

				setAttribInRequest(request, "strModeJS", "var isAddMode = false;");
				saveData(request);

				if (!isExceptionOccured(request)) {
					intSuccess = 1;
					strFormData = " var saveSuccess = " + intSuccess + ";"; // 0-Not Applicable, 1-Success, 2-Fail";
					strFormData += " var arrFormData = new Array();";
					request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
				}
			}

			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_DELETE)) {
				checkPrivilege(request, WebConstants.PRIV_SYS_MAS_SITA_DELETE);
				deleteData(request);
			}

			setDisplayData(request);
		} catch (Exception exception) {
			log.error("Exception in SitaAddressRequestHandler.execute()", exception);
			if (exception instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
			}
		}

		return forward;
	}

	/**
	 * Delete the SITA
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void deleteData(HttpServletRequest request) throws ModuleException {
		log.debug("Entered inside the deleteData()...");
		String strSitaId = request.getParameter(PARAM_SITA_ID);

		try {
			log.debug("deleteData()..." + strSitaId);
			if (strSitaId != null && !strSitaId.equals("0")) {

				int strSitaAddressId = Integer.parseInt(strSitaId);
				SITAAddress existingSitaAddress = ModuleServiceLocator.getAirportServiceBD().getSITAAddress(
						new Integer(strSitaId));
				if (existingSitaAddress == null)
					ModuleServiceLocator.getAirportServiceBD().removeSITAAddress(strSitaAddressId);
				else {
					String strVersion = request.getParameter(PARAM_VERSION);
					if (strVersion != null && !"".equals(strVersion)) {
						existingSitaAddress.setVersion(Long.parseLong(strVersion));
						ModuleServiceLocator.getAirportServiceBD().removeSITAAddress(existingSitaAddress);
					} else {
						ModuleServiceLocator.getAirportServiceBD().removeSITAAddress(strSitaAddressId);
					}
				}
				saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_DELETE_SUCCESS), WebConstants.MSG_SUCCESS);
			}
		} catch (ModuleException moduleException) {
			log.error("Exception in SitaAddressRequestHandler.deleteData() [origin module=" + moduleException.getModuleDesc()
					+ "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Save Add/Update SITA Address & set success int 0-Not Applicable, 1-Success, 2-Fail
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return boolean the success-true not-false
	 * @throws Exception
	 *             the Exception
	 */
	private static boolean saveData(HttpServletRequest request) throws Exception {

		log.debug("Entered inside the saveData()...");
		SITAAddress sitaAddress = new SITAAddress();
		String strSitaId = null;
		String strVersion = null;
		String strGridRow = null;
		String strSitaAirportCode = null;
		String strLocation = null;
		String strSitaAddress = null;
		String strActiveStatus = null;
		String strEmailId = null;
		String strCarrierCodes = null;
		String strDeliveryMethod = null;
		String strAllowPnlAdl=null;
		String strAllowPalCal=null;
		boolean allowPnlAdl =false;
	    boolean allowPalCal =false;
	    String strOndCodes = null;
	    String strFlightNumbers = null;

		try {
			strSitaId = request.getParameter(PARAM_SITA_ID);
			strVersion = AiradminUtils.getNotNullString(request.getParameter(PARAM_VERSION));
			strGridRow = AiradminUtils.getNotNullString(request.getParameter(PARAM_GRIDROW));
			strSitaAirportCode = AiradminUtils.getNotNullString(request.getParameter(PARAM_SITA_AIRPORTCODE));
			strLocation = AiradminUtils.getNotNullString(request.getParameter(PARAM_LOCATION_DESCRIPTION)).trim();
			strSitaAddress = AiradminUtils.getNotNullString(request.getParameter(PARAM_SITA_ADDRESS)).trim();
			strActiveStatus = AiradminUtils.getNotNullString(request.getParameter(PARAM_ACTIVE_STATUS));
			strEmailId = AiradminUtils.getNotNullString(request.getParameter(PARAM_EMAIL)).trim();
			strCarrierCodes = AiradminUtils.getNotNullString(request.getParameter(PARAM_CARRIER_CODES)).trim();
			strAllowPnlAdl = AiradminUtils.getNotNullString(request.getParameter(PARAM_SITA_ALLOW_PNLADL));
			strAllowPalCal = AiradminUtils.getNotNullString(request.getParameter(PARAM_SITA_ALLOW_PALCAL));
			strOndCodes = AiradminUtils.getNotNullString(request.getParameter(PARAM_OND_CODES));
			strFlightNumbers = AiradminUtils.getNotNullString(request.getParameter(PARAM_FLIGHT_NUMBERS));
			


			if (AppSysParamsUtil.isDeliveryMethodDisplayEnabled()) {
				strDeliveryMethod = AiradminUtils.getNotNullString(request.getParameter(PARAM_SITA_DELIVERY_METHOD).trim());
			} else {
				strDeliveryMethod = SITAAddress.SITA_MESSAGE_VIA_MCONNECT;
			}
			

			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_EDIT)) {
				sitaAddress.setAirportSITAId(Integer.parseInt(strSitaId));
			}

			if (strVersion != null && !"".equals(strVersion)) {
				sitaAddress.setVersion(Long.parseLong(strVersion));
			}

			if (strSitaAirportCode != null && !"".equals(strSitaAirportCode)) {
				sitaAddress.setAirportCode(strSitaAirportCode);
			}

			if (strLocation != null && !"".equals(strLocation)) {
				sitaAddress.setSitaLocation(strLocation);
			}

			if (strSitaAddress != null && !"".equals(strSitaAddress)) {
				sitaAddress.setSitaAddress(strSitaAddress);
			}

			if (strActiveStatus != null && strActiveStatus.equals(WebConstants.CHECKBOX_ON)) {
				sitaAddress.setActiveStatus(SITAAddress.ACTIVE_STATUS_YES);
			} else {
				sitaAddress.setActiveStatus(SITAAddress.ACTIVE_STATUS_NO);
			}

			if (strDeliveryMethod != null && !"".equals(strDeliveryMethod)) {
				sitaAddress.setSitaDeliveryMethod(strDeliveryMethod);
			}

			if (strEmailId != null && !"".equals(strEmailId)) {
				sitaAddress.setEmailId(strEmailId);
			}

			allowPnlAdl =strAllowPnlAdl != null && strAllowPnlAdl.equals(WebConstants.CHECKBOX_ON);
		    allowPalCal =strAllowPalCal != null && strAllowPalCal.equals(WebConstants.CHECKBOX_ON);

			sitaAddress.setAllowForPNLADL(allowPnlAdl);
			sitaAddress.setAllowForPALCAL(allowPalCal);

			if (strCarrierCodes != null && !"".equals(strCarrierCodes)) {
				Set<String> carrierCodes = new HashSet<String>();
				StringTokenizer strTok = new StringTokenizer(strCarrierCodes, ",");
				while (strTok.hasMoreTokens()) {
					carrierCodes.add(strTok.nextToken());
				}
				sitaAddress.setCarriercodes(carrierCodes);
			}
			
			if (strOndCodes != null && !"".equals(strOndCodes)) {
				Set<String> ondCodes = new HashSet<String>();
				StringTokenizer strTok = new StringTokenizer(strOndCodes, ",");
				while (strTok.hasMoreTokens()) {
					ondCodes.add(strTok.nextToken());
				}
				sitaAddress.setOndCodes(ondCodes);
			}
			
			if (strFlightNumbers != null && !"".equals(strFlightNumbers)) {
				Set<String> flightNumbers = new HashSet<String>();
				StringTokenizer strTok = new StringTokenizer(strFlightNumbers, ",");
				while (strTok.hasMoreTokens()) {
					flightNumbers.add(strTok.nextToken());
				}
				sitaAddress.setFlightNumbers(flightNumbers);
			}

			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_ADD)) {
				if (checkValidAirport(request.getParameter(PARAM_SITA_AIRPORTCODE) == null ? "" : request
						.getParameter(PARAM_SITA_AIRPORTCODE)) == 1) {

					setAttribInRequest(request, "strAirportFocusJS", "var isAirportFocus = true;");
					setExceptionOccured(request, true);
					String strFormData = "var arrFormData = new Array();";
					strFormData += "arrFormData[0] = new Array();";
					strFormData += "arrFormData[0][1] = '" + strSitaAddress + "';";
					strFormData += "arrFormData[0][2] = '" + strCarrierCodes + "';";
					strFormData += "arrFormData[0][3] = '" + strSitaAirportCode + "';";
					strFormData += "arrFormData[0][4] = '" + strLocation + "';";
					strFormData += "arrFormData[0][5] = '" + strActiveStatus + "';";
					strFormData += "arrFormData[0][6] = '" + strEmailId + "';";
					strFormData += "arrFormData[0][7] = " + allowPnlAdl + ";";
					strFormData += "arrFormData[0][8] = " + allowPalCal + ";";
					strFormData += "arrFormData[0][9] = " + strOndCodes + ";";
					strFormData += "arrFormData[0][10] = " + strFlightNumbers + ";";

					intSuccess = 2;
					strFormData += " var saveSuccess = " + intSuccess + "; ";

					if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_EDIT)) {
						strFormData += " var prevVersion = " + strVersion + ";";
						strFormData += " var prevSitaId = " + strSitaId + ";";
						strFormData += " var prevGridRow = " + strGridRow + ";";
					}

					request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);

					saveMessage(request, airadminConfig.getMessage("um.user.form.airport.inactive"), WebConstants.MSG_ERROR);
					log.debug("SitaAddressRequestHandler.saveData() method failed due to invalid airport.");
					return false;
				}
			}
			ModuleServiceLocator.getAirportServiceBD().saveSITAAddress(sitaAddress);
			setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = true;");
			sitaAddress = null;
			saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS), WebConstants.MSG_SUCCESS);
			setExceptionOccured(request, false);
			log.debug("SitaAddressRequestHandler.saveData() method is successfully executed.");

		} catch (ModuleException moduleException) {
			log.error(
					"Exception in SitaAddressRequestHandler.saveData() [origin module=" + moduleException.getModuleDesc() + "]",
					moduleException);
			setExceptionOccured(request, true);

			String strFormData = "var arrFormData = new Array();";
			strFormData += "arrFormData[0] = new Array();";
			strFormData += "arrFormData[0][1] = '" + strSitaAddress + "';";
			strFormData += "arrFormData[0][2] = '" + strCarrierCodes + "';";
			strFormData += "arrFormData[0][3] = '" + strSitaAirportCode + "';";
			strFormData += "arrFormData[0][4] = '" + strLocation + "';";
			strFormData += "arrFormData[0][5] = '" + strActiveStatus + "';";
			strFormData += "arrFormData[0][6] = '" + strEmailId + "';";
			strFormData += "arrFormData[0][7] = " + allowPnlAdl + ";";
			strFormData += "arrFormData[0][8] = " + allowPalCal + ";";
			strFormData += "arrFormData[0][9] = " + strOndCodes + ";";
			strFormData += "arrFormData[0][10] = " + strFlightNumbers + ";";


			intSuccess = 2;
			strFormData += " var saveSuccess = " + intSuccess + ";"; // 0-Not Applicable, 1-Success, 2-Fail";

			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_EDIT)) {
				strFormData += " var prevVersion = " + strVersion + ";";
				strFormData += " var prevSitaId = " + strSitaId + ";";
				strFormData += " var prevGridRow = " + strGridRow + ";";
			}

			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
			if (moduleException.getMessageString().equals(MessagesUtil.getMessage("sita.address.unique.violation"))) {
				saveMessage(request, airadminConfig.getMessage("um.sitaaddress.form.id.unique"), WebConstants.MSG_ERROR);
			} else {
				saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
			}

		} catch (Exception exception) {
			log.error("Exception in SitaAddressRequestHandler.deleteData()", exception);
			if (exception instanceof RuntimeException) {
				throw exception;
			}
		}
		return true;
	}

	/**
	 * Sets the Display Data for SITA page & sets success int 0-Not Applicable, 1-Success, 2-Fail";
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setDisplayData(HttpServletRequest request) {
		setSitaAddressRowHtml(request);
		setAirportList(request);
		setActiveStatusList(request);
		setClientErrors(request);
		setCarrierCodesList(request);
		setFlightNumbersList(request);
		setOndCodes(request);
		setFlightNumbersWithOriginList(request);

		if (!isExceptionOccured(request)) {
			String strFormData = "var arrFormData = new Array();";
			strFormData += " var saveSuccess = " + intSuccess + ";";
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
		}
		
		request.setAttribute(WebConstants.REQ_AIRPORT_FOCUS, getAttribInRequest(request, "strAirportFocusJS"));
		request.setAttribute(WebConstants.REQ_OPERATION_MODE, getAttribInRequest(request, "strModeJS"));
		request.setAttribute(WebConstants.REQ_TRANSACTION_STATUS, getAttribInRequest(request, "isSaveTransactionSuccessfulJS"));
		request.setAttribute(WebConstants.REQ_TRANSACTION_DISPLAYSTATUS, "var isSuccessfulSaveMessageDisplayEnabled = "
				+ isSuccessfulSaveMessageDisplayEnabled() + ";");
		request.setAttribute(WebConstants.REQ_DELIVERY_METHOD_DISPLAY_STATUS, "var isDeliveryMethodDisplayEnabled = "
				+ AppSysParamsUtil.isDeliveryMethodDisplayEnabled());
		request.setAttribute(WebConstants.REQ_PAL_CAL_DISPLAY_STATUS, "var isPalCalEnabled = "
				+ AppSysParamsUtil.isEnablePalCalMessage());
	}

	/**
	 * Sets the Active onlone Airport List
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setAirportList(HttpServletRequest request) {

		try {
			String strHtml = SelectListGenerator.createActiveAirportCodeList();
			request.setAttribute(WebConstants.REQ_HTML_AIRPORT_LIST, strHtml);
		} catch (ModuleException moduleException) {
			log.error("Exception in SitaAddressRequestHandler.setAirportList() [origin module=" + moduleException.getModuleDesc()
					+ "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}
	
	/**
	 * Sets the Flight Numbers List
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setFlightNumbersList(HttpServletRequest request) {

		try {
			String strHtml = SelectListGenerator.createFlightNoListWithoutBus();
			request.setAttribute(WebConstants.REQ_HTML_FLIGHTNO_LIST_DATA, strHtml);
		} catch (ModuleException moduleException) {
			log.error("Exception in SitaAddressRequestHandler.setFlightNumbersList() [origin module=" + moduleException.getModuleDesc()
					+ "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}
	
	private static void setFlightNumbersWithOriginList(HttpServletRequest request) {

		try {
			String strHtml = SelectListGenerator.createOriginFlightNoList();
			request.setAttribute(WebConstants.REQ_HTML_FLIGHTNO_ORIGIN_LIST_DATA, strHtml);
		} catch (ModuleException moduleException) {
			log.error("Exception in SitaAddressRequestHandler.setFlightNumbersList() [origin module=" + moduleException.getModuleDesc()
					+ "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}
	
	private static void setOndCodes(HttpServletRequest request) {

		try {
			String strHtml = SelectListGenerator.createRouteOndCodeList();
			request.setAttribute(WebConstants.REQ_HTML_OND_LIST_DATA, strHtml);
		} catch (ModuleException moduleException) {
			log.error("Exception in SitaAddressRequestHandler.setFlightNumbersList() [origin module=" + moduleException.getModuleDesc()
					+ "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Status list of the SITA
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setActiveStatusList(HttpServletRequest request) {
		String strHtml = "";
		strHtml += "<option value='" + SITAAddress.ACTIVE_STATUS_YES + "'>Active</option>";
		strHtml += "<option value='" + SITAAddress.ACTIVE_STATUS_NO + "'>In-Active</option>";
		request.setAttribute(WebConstants.REQ_HTML_SITA_ACTIVESTATUS_LIST, strHtml);
	}

	/**
	 * Sets the Client Validations to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = SitaAddressHTMLGenerator.getClientErrors(request);
			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error(
					"Exception in SitaAddressRequestHandler.setClientErrors() [origin module=" + moduleException.getModuleDesc()
							+ "]", moduleException);
		}
	}

	/**
	 * Sets the SITA Address Data Row to the request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	protected static void setSitaAddressRowHtml(HttpServletRequest request) {
		try {
			SitaAddressHTMLGenerator htmlGen = new SitaAddressHTMLGenerator();
			String strHtml = htmlGen.getSitaAddressRowHtml(request);
			request.setAttribute(WebConstants.REQ_HTML_ROWS, strHtml);

			request.setAttribute(WebConstants.REQ_FORM_FIELD_VALUES, htmlGen.getFormFieldValues());
		} catch (ModuleException moduleException) {
			log.error(
					"Exception in SitaAddressRequestHandler.setSitaAddressRowHtml() [origin module="
							+ moduleException.getModuleDesc() + "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the carrier codes List
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setCarrierCodesList(HttpServletRequest request) {

		try {
			String strHtml = SelectListGenerator.getSystemCarrierCodes();
			request.setAttribute(WebConstants.REQ_CARRIER_SELECT_LIST, strHtml);
		} catch (ModuleException moduleException) {
			log.error("Exception in SitaAddressRequestHandler.setAirportList() [origin module=" + moduleException.getModuleDesc()
					+ "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}
}
