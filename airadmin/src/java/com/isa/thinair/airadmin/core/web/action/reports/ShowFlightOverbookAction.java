package com.isa.thinair.airadmin.core.web.action.reports;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.reports.FlightOverBookSummaryReportRH;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;

@Results({
		@Result(name = AdminStrutsConstants.AdminAction.SUCCESS, value = AdminStrutsConstants.AdminJSP.REPORTING_OVER_BOOKING_JSP),
		@Result(name = AdminStrutsConstants.AdminAction.ERROR, value = AdminStrutsConstants.AdminJSP.ERROR_JSP) })
public class ShowFlightOverbookAction extends BaseRequestResponseAwareAction {
	private static Log log = LogFactory.getLog(ShowPromotionsReportAction.class);

	public String execute() {

		String overBookType = request.getParameter("selFlightOverbookType");
		if (overBookType != null) {

			return FlightOverBookSummaryReportRH.execute(request, response);

		} else {
			try {
				String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

				if (strClientErrors == null) {
					strClientErrors = "";
				}
				request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
			} catch (ModuleException moduleException) {
				log.error("setClientErrors failed : ", moduleException);
				return AdminStrutsConstants.AdminAction.ERROR;
			}
			return AdminStrutsConstants.AdminAction.SUCCESS;
		}

	}

}
