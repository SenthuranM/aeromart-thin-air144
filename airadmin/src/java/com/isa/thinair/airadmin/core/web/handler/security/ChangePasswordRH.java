package com.isa.thinair.airadmin.core.web.handler.security;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.security.PasswordChangeHG;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.airsecurity.api.service.AirSecurityUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.Util;
import com.isa.thinair.login.util.ForceLoginInvoker;

public class ChangePasswordRH extends BasicRequestHandler {

	private static final String PARAM_OLD_PWD = "pwdOld";
	private static final String PARAM_NEW_PWD = "pwdNew";

	private static AiradminConfig airadminConfig = new AiradminConfig();

	/**
	 * Main Execute Method for Password Change Action
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {

		String forward = AdminStrutsConstants.AdminAction.CHANGE_PASS;
		String userId = "";
		boolean loginSuccess = false;
		String strMode = request.getParameter("hdnMode");
		if (strMode != null && strMode.equals("CLOSE")) {
			setClose(request);
		} else {

			try {
				userId = request.getUserPrincipal().getName();
				if (authenticateUser(userId, request.getParameter(PARAM_OLD_PWD))) {
					String newPassword = request.getParameter(PARAM_NEW_PWD);
					if (AppSysParamsUtil.isEnablePasswordHistory() && AirSecurityUtil.getSecurityBD().isPasswordUsed(userId, newPassword)) {
						forward = AdminStrutsConstants.AdminAction.PASS_CHANGE;
						saveMessage(request, airadminConfig.getMessage("um.user.form.password.usedpassword"), WebConstants.MSG_ERROR);
						loginSuccess = false;
					} else {
						changePassword(userId, newPassword);
						request.getSession().invalidate();
						ForceLoginInvoker.close();
						saveMessage(request, AiradminConfig.getServerMessage("passwordchange.operation.success"),
								WebConstants.MSG_SUCCESS);
						loginSuccess = true;
					}
				} else {
					forward = AdminStrutsConstants.AdminAction.PASS_CHANGE;
					saveMessage(request, airadminConfig.getMessage("um.user.form.oldpassword.invalid"), WebConstants.MSG_ERROR);
					loginSuccess = false;
				}
			} catch (ModuleException ex) {
				forward = AdminStrutsConstants.AdminAction.ERROR;
				saveMessage(request, ex.getMessageString(), WebConstants.MSG_ERROR);
				loginSuccess = false;
			}
			try {
				setDisplayData(request);
				if (loginSuccess) {
					String strHtml = "var loggedInUserId = '';";
					strHtml += "alert('Password Changed Successfully. Please Restart the Application');top.close(); sPage = true;";
					request.setAttribute(WebConstants.REQ_HTML_LOGIN_UID, strHtml);
				}
			} catch (ModuleException e) {
				saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
				loginSuccess = false;
				forward = AdminStrutsConstants.AdminAction.PASS_CHANGE;
			}
		}
		return forward;
	}

	/**
	 * Save the Changed password
	 * 
	 * @param userId
	 *            the User Id
	 * @param newPassword
	 *            the New Password
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void changePassword(String userId, String newPassword) throws ModuleException {
		User user = ModuleServiceLocator.getSecurityBD().getUser(userId);
		user.setPasswordReseted("N");
		user.setPassword(newPassword);
		user.setLccPublishStatus(Util.LCC_TOBE_REPUBLISHED);

		ModuleServiceLocator.getSecurityBD().saveUser(user);
		if (AppSysParamsUtil.isLCCDataSyncEnabled()) {
			User savedUser = ModuleServiceLocator.getSecurityBD().getUser(user.getUserId());
			ModuleServiceLocator.getCommonServiceBD().publishSpecificUserDataUpdate(savedUser);
		}
	}

	/**
	 * Authenticate the Logged in User
	 * 
	 * @param userId
	 *            the User id
	 * @param password
	 *            the Password
	 * @return boolean authenticated-true not-false
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static boolean authenticateUser(String userId, String password) throws ModuleException {
		User user = ModuleServiceLocator.getSecurityBD().authenticate(userId, password);
		return (user != null);
	}

	/**
	 * Sets the Client Validations to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static void setClientErrors(HttpServletRequest request) throws ModuleException {
		String strClientErrors = PasswordChangeHG.getClientErrors(request);
		if (strClientErrors == null) {
			strClientErrors = "";
		}
		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
	}

	/**
	 * Sets the Display data for Changed password
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static void setDisplayData(HttpServletRequest request) throws ModuleException {
		setClientErrors(request);
		setChangePasswordHtml(request);
	}

	/**
	 * Sets the User Id to the Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	protected static void setChangePasswordHtml(HttpServletRequest request) throws ModuleException {
		Principal principal = request.getUserPrincipal();
		String strHtml = "var loggedInUserId = '" + principal.getName() + "';";
		request.setAttribute(WebConstants.REQ_HTML_LOGIN_UID, strHtml);
	}

	/**
	 * Close the page secure/non secure Issue to solve
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setClose(HttpServletRequest request) {
		String strHtml = "";
		String strPath = BasicRequestHandler.getContextPath(request);
		strHtml += "window.location.replace('" + strPath + "/private/master/showChangePassword'); sPage = true";
		request.setAttribute(WebConstants.REQ_HTML_LOGIN_UID, strHtml);
	}
}
