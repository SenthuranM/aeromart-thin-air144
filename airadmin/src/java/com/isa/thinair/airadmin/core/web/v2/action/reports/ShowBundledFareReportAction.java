package com.isa.thinair.airadmin.core.web.v2.action.reports;

import java.sql.ResultSet;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONUtil;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airadmin.core.web.v2.util.ReportingUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;

/**
 * 
 * @author rumesh
 * 
 */

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.Reporting.RPTS_BUNDLED_FARE_JSP)
public class ShowBundledFareReportAction extends BaseRequestResponseAwareAction {

	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();
	private static String BUNDLED_FARE_REPORT_TEMPLATE = "BundledFareReport.jasper";
	private static String BUNDLED_FARE_DETAIL_REPORT_TEMPLATE = "BundledFareDetailReport.jasper";
	private static String OPT_DETAIL = "DETAIL";

	private String salesDateFrm;
	private String salesDateTo;
	private String flightDateFrm;
	private String flightDateTo;
	private String hdnBundledFares;
	private String hdnChannels;
	private Collection<String> colSectors;
	private String radReportOption;
	private String radOption;

	public String execute() {
		String forward = S2Constants.Result.SUCCESS;
		try {
			setReportView();
			return null;
		} catch (ModuleException e) {
			forward = S2Constants.Result.ERROR;
		}
		return forward;
	}

	private void setReportView() throws ModuleException {
		ReportsSearchCriteria search = new ReportsSearchCriteria();
		ResultSet resultSet = null;
		String id = "UC_REPM_086";
		String strLogo = AppSysParamsUtil.getReportLogo(false);
		String image = ReportingUtil.ReportingLocations.image_loc + AppSysParamsUtil.getReportLogo(true);
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		Map<String, Object> parameters = new HashMap<String, Object>();
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);

		String fileName = "BundledFareReport";
		String reportTemplate = BUNDLED_FARE_REPORT_TEMPLATE;
		if (OPT_DETAIL.equals(radOption)) {
			reportTemplate = BUNDLED_FARE_DETAIL_REPORT_TEMPLATE;
			fileName = "BundledFareDetailReport";
			search.setDetailReport(true);
		}

		ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
				ReportsHTMLGenerator.getReportTemplate(reportTemplate));

		search.setDateRangeFrom(this.salesDateFrm);
		search.setDateRangeTo(this.salesDateTo);
		search.setDateRange2From(this.flightDateFrm);
		search.setDateRange2To(this.flightDateTo);
		search.setSegmentCodes(this.colSectors);

		if (!StringUtil.isNullOrEmpty(this.hdnChannels)) {
			String[] arrChannels = this.hdnChannels.split(",");
			Collection<String> salesChannels = new HashSet<String>();
			for (String salesChannenl : arrChannels) {
				salesChannels.add(salesChannenl);
			}
			search.setSalesChannels(salesChannels);
		}

		if (!StringUtil.isNullOrEmpty(this.hdnBundledFares)) {
			String[] periodIds = this.hdnBundledFares.split(",");
			Set<Integer> bundledFarePeriodIds = new HashSet<Integer>();
			for (String periodId : periodIds) {
				if (periodId != null && !"".equals(periodId)) {
					bundledFarePeriodIds.add(Integer.parseInt(periodId));
				}
			}
			search.setBundledFarePeriodIds(bundledFarePeriodIds);
		}

		if (strlive != null) {
			if (strlive.equals(WebConstants.REP_LIVE)) {
				search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
			} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
				search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
			}
		}

		resultSet = ModuleServiceLocator.getDataExtractionBD().getBundledFareReportData(search);

		parameters.put("CURRENCY", AppSysParamsUtil.getBaseCurrency());
		parameters.put("ID", id);
		parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
		parameters.put("FROM_DATE", this.salesDateFrm);
		parameters.put("TO_DATE", this.salesDateTo);

		if (radReportOption.trim().equals("HTML")) {
			parameters.put("IMG", image);
			ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null, null,
					response);
		} else if (radReportOption.trim().equals("PDF")) {
			response.reset();
			response.addHeader("Content-Disposition", "filename=" + fileName + ".pdf");
			image = ReportsHTMLGenerator.getReportTemplate(strLogo);
			parameters.put("IMG", image);
			ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);
		} else if (radReportOption.trim().equals("EXCEL")) {
			response.reset();
			response.addHeader("Content-Disposition", "attachment;filename=" + fileName + ".xls");
			ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);
		} else if (radReportOption.trim().equals("CSV")) {
			response.reset();
			response.addHeader("Content-Disposition", "attachment;filename=" + fileName + ".csv");
			ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);
		}
	}

	public void setSalesDateFrm(String salesDateFrm) {
		this.salesDateFrm = salesDateFrm;
	}

	public void setSalesDateTo(String salesDateTo) {
		this.salesDateTo = salesDateTo;
	}

	public void setFlightDateFrm(String flightDateFrm) {
		this.flightDateFrm = flightDateFrm;
	}

	public void setFlightDateTo(String flightDateTo) {
		this.flightDateTo = flightDateTo;
	}

	public void setHdnBundledFares(String hdnBundledFares) {
		this.hdnBundledFares = hdnBundledFares;
	}

	public void setHdnChannels(String hdnChannels) {
		this.hdnChannels = hdnChannels;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void setHdnSectors(String hdnSectors) throws JSONException {
		Set<String> sectorSet = new HashSet((List<String>) JSONUtil.deserialize(hdnSectors));
		this.colSectors = sectorSet;
	}

	public void setRadReportOption(String radReportOption) {
		this.radReportOption = radReportOption;
	}

	public void setRadOption(String radOption) {
		this.radOption = radOption;
	}
}
