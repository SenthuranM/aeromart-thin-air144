package com.isa.thinair.airadmin.core.web.v2.action.tools;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airinventory.api.dto.SearchRollForwardCriteriaSearchTO;
import com.isa.thinair.airinventory.api.dto.SearchRollForwardCriteriaTO;
import com.isa.thinair.airinventory.api.model.RollForwardBatchREQSEG;
import com.isa.thinair.airinventory.core.util.AirinventoryUtils;
import com.isa.thinair.commons.api.constants.CommonsConstants.BatchStatus;
import com.isa.thinair.commons.api.dto.Page;

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class SearchRollForwardCriteriaAction {

	/** SearchRollForwardCriteriaTO DTO object to transfer data to the back end */
	private SearchRollForwardCriteriaTO searchRollForwardCriteriaTO = new SearchRollForwardCriteriaTO();

	/** SearchRollForwardCriteriaSearchTO DTO object to transfer data to the back end */
	private SearchRollForwardCriteriaSearchTO searchRollForwardCriteriaSearchTO = new SearchRollForwardCriteriaSearchTO();

	private static Log log = LogFactory.getLog(SearchRollForwardCriteriaAction.class);
	/** Operation success status indicator */
	private boolean success = true;

	/** Success/Failure message for UI display */
	private String messageTxt;

	/** Search result and pagination data */
	private Collection<Map<String, Object>> rows;
	private int page;
	private int total;
	private int records;

	/** Date format for front end data data */
	private final String DATE_FORMAT = "dd/MM/yyyy";

	/*
	 * Delete method. deletes an existing batch job
	 */
	public String delete() {
		try {

			RollForwardBatchREQSEG rollForwardBatchREQSEG = AirinventoryUtils.getRollForwardBatchREQSEGBD()
					.getRollForwardBatchREQSEG(searchRollForwardCriteriaSearchTO.getBatchSegId());

			if (rollForwardBatchREQSEG.getStatus().equalsIgnoreCase(BatchStatus.WAITING)) {
				AirinventoryUtils.getRollForwardBatchREQSEGBD().updateRollForwardBatchREQSEGStatus(
						searchRollForwardCriteriaSearchTO.getBatchSegId(), searchRollForwardCriteriaSearchTO.getFlightNumber(),
						searchRollForwardCriteriaSearchTO.getFromDate(), searchRollForwardCriteriaSearchTO.getToDate(),
						BatchStatus.TERMINATED, null);
			}
		} catch (Exception exception) {
			log.error(messageTxt, exception);
			success = false;
		}
		return S2Constants.Result.SUCCESS;
	}

	/*
	 * Search method
	 */
	public String search() {

		try {
			Page<SearchRollForwardCriteriaTO> criteriaSearchPage = null;
			int start = (page - 1) * 20;
			Map<String, Object> row;

			if (searchRollForwardCriteriaSearchTO != null) {

				if (searchRollForwardCriteriaSearchTO.getFlightNumber() != null
						&& searchRollForwardCriteriaSearchTO.getFlightNumber().trim().length() == 0) {
					searchRollForwardCriteriaSearchTO.setFlightNumber(null);
				}
				if (searchRollForwardCriteriaSearchTO.getStatus() != null
						&& searchRollForwardCriteriaSearchTO.getStatus().trim().length() == 0) {
					searchRollForwardCriteriaSearchTO.setStatus("-");
				}
				if(searchRollForwardCriteriaSearchTO.getToDate() != null){
					// preparing and setting the to date
					Calendar calToDate = new GregorianCalendar();
					calToDate.setTime(searchRollForwardCriteriaSearchTO.getToDate());
					calToDate.set(Calendar.HOUR, 23);
					calToDate.set(Calendar.MINUTE, 59);
					calToDate.set(Calendar.SECOND, 59);
					calToDate.set(Calendar.MILLISECOND, 999);
				
					searchRollForwardCriteriaSearchTO.setToDate(calToDate.getTime());
				}
				
				criteriaSearchPage = AirinventoryUtils.getRollForwardBatchREQSEGBD()
						.search(searchRollForwardCriteriaSearchTO, start, 20);

				this.setRecords(criteriaSearchPage.getTotalNoOfRecords());
				this.total = criteriaSearchPage.getTotalNoOfRecords() / 20;
				int mod = criteriaSearchPage.getTotalNoOfRecords() % 20;
				if (mod > 0) {
					this.total = this.total + 1;
				}

				Collection<SearchRollForwardCriteriaTO> criterias = criteriaSearchPage.getPageData();
				List<SearchRollForwardCriteriaTO> criteriaList = new ArrayList<SearchRollForwardCriteriaTO>(criterias);
				Collections.sort(criteriaList, new Comparator<SearchRollForwardCriteriaTO>() {
					public int compare(SearchRollForwardCriteriaTO p1, SearchRollForwardCriteriaTO p2) {
						return p2.getBatchId().compareTo(p1.getBatchId());
					}
				});

				rows = new ArrayList<Map<String, Object>>();

				int i = 1;
				for (SearchRollForwardCriteriaTO criteria : criteriaList) {

					row = new HashMap<String, Object>();
					row.put("id", ((page - 1) * 20) + i++);
					row.put("criteria", criteria);

					rows.add(row);
				}

			}

		} catch (Exception exception) {
			log.error(messageTxt, exception);
			success = false;
		}

		return S2Constants.Result.SUCCESS;
	}

	public SearchRollForwardCriteriaSearchTO getSearchRollForwardCriteriaSearchTO() {
		return searchRollForwardCriteriaSearchTO;
	}

	public void setSearchRollForwardCriteriaSearchTO(SearchRollForwardCriteriaSearchTO searchRollForwardCriteriaSearchTO) {
		this.searchRollForwardCriteriaSearchTO = searchRollForwardCriteriaSearchTO;
	}

	public SearchRollForwardCriteriaTO getSearchRollForwardCriteriaTO() {
		return searchRollForwardCriteriaTO;
	}

	public void setSearchRollForwardCriteriaTO(SearchRollForwardCriteriaTO searchRollForwardCriteriaTO) {
		this.searchRollForwardCriteriaTO = searchRollForwardCriteriaTO;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public Collection<Map<String, Object>> getRows() {
		return rows;
	}

	public void setRows(Collection<Map<String, Object>> rows) {
		this.rows = rows;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public String getDATE_FORMAT() {
		return DATE_FORMAT;
	}

}
