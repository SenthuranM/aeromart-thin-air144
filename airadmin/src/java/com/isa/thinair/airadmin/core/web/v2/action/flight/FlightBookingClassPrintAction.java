package com.isa.thinair.airadmin.core.web.v2.action.flight;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airadmin.api.flight.FlightBookingClassDTO;
import com.isa.thinair.airadmin.api.flight.FlightCabinClassDTO;
import com.isa.thinair.airadmin.api.flight.FlightSeatMvSummaryDTO;
import com.isa.thinair.airadmin.api.flight.FlightSegmentAllocationDTO;
import com.isa.thinair.airadmin.api.flight.FlightSummaryDTO;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airinventory.api.dto.ExtendedFCCSegBCInventory;
import com.isa.thinair.airinventory.api.dto.FCCInventoryDTO;
import com.isa.thinair.airinventory.api.dto.FCCSegInventoryDTO;
import com.isa.thinair.airinventory.api.model.FCCSegBCInventory;
import com.isa.thinair.airinventory.api.model.FCCSegInventory;
import com.isa.thinair.airpricing.api.dto.FareSummaryLightDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.TemplateEngine;
import com.isa.thinair.reporting.api.model.FCCAgentsSeatMvDTO;
import com.isa.thinair.reporting.api.model.SeatMvSummaryDTO;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * 
 * @author Pradeep Karunanayake
 * 
 */
@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Results({
		@Result(name = S2Constants.Result.ERROR, value = AdminStrutsConstants.AdminJSP.ERROR_JSP),
		@Result(name = S2Constants.Jsp.Flight.FLIGHT_BOOKING_ALLOCATION_PRINT, value = S2Constants.Jsp.Flight.FLIGHT_BOOKING_ALLOCATION_PRINT) })
public class FlightBookingClassPrintAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(FlightBookingClassPrintAction.class);

	private Integer fltId;
	private Integer olFlightId;
	private String hdnFlightSumm;

	public String execute() {
		String forward = S2Constants.Jsp.Flight.FLIGHT_BOOKING_ALLOCATION_PRINT;
		// TODO Move to config file
		String templateName = "email/flight_bookingclass_alloc.xml.vm";

		try {
			Map<String, String> cabinClassMap = extractCabinClassMap(SelectListGenerator.createCabinClass());
			FlightSummaryDTO flightSummaryDTO = new FlightSummaryDTO();
			List<FlightCabinClassDTO> flightCabinClassDTOs = new ArrayList<FlightCabinClassDTO>();
			FlightCabinClassDTO flightCabinClassDTO = null;

			for (String classOfService : cabinClassMap.keySet()) {
				flightCabinClassDTO = new FlightCabinClassDTO();
				flightCabinClassDTO.setCabinClassName(cabinClassMap.get(classOfService));
				FCCAgentsSeatMvDTO seatMvmntInfoWithOriginAgent = ModuleServiceLocator.getDataExtractionBD()
						.getFCCSeatMovementSummary(fltId, classOfService, false);
				FCCAgentsSeatMvDTO seatMvmntInfoWithOwnerAgent = ModuleServiceLocator.getDataExtractionBD()
						.getOwnerAgentWiseFCCSeatMovementSummary(fltId, classOfService, false);
				addFlightAllocationInfo(
						ModuleServiceLocator.getFlightInventoryBD().getFCCInventory(fltId, classOfService, olFlightId,false),
						seatMvmntInfoWithOriginAgent, flightCabinClassDTO, seatMvmntInfoWithOwnerAgent);
				flightCabinClassDTOs.add(flightCabinClassDTO);
			}

			StringWriter writer = new StringWriter();
			HashMap<String, Object> printDataMap = new HashMap<String, Object>();
			addFlightSummaryData(hdnFlightSumm, flightSummaryDTO);
			printDataMap.put("flight", flightSummaryDTO);
			printDataMap.put("cabinClassDtos", flightCabinClassDTOs);

			try {
				new TemplateEngine().writeTemplate(printDataMap, templateName, writer);
			} catch (Exception e) {
				log.error("Template creating process is fail", e);
				throw new ModuleException("module.bd.createexception", e);
			}
			request.setAttribute("reqFlightBookingAllocBodyHtml", writer.toString());

		} catch (Exception ex) {
			forward = S2Constants.Result.ERROR;
			log.error("FlightBookingClassPrintAction.execute() method is failed :", ex);
			saveMessage(request, ex.getMessage(), WebConstants.MSG_ERROR);
		}

		return forward;
	}

	private void addFlightSummaryData(String flightSummary, FlightSummaryDTO flightSummaryDTO) {
		// G90674|HMB|17-12-2011 14:35|SHJ|17-12-2011 17:35||ACT|N|Y#|Y|284295|A320-211|null|Y|397000|Y|@
		if (flightSummary != null) {
			String[] flightData = flightSummary.split("\\|");

			if (flightData.length >= 12) {
				flightSummaryDTO.setFlightNo(flightData[0]);
				flightSummaryDTO.setModel(flightData[11]);
				flightSummaryDTO.setDeparture(flightData[1] + " " + flightData[2]);
				flightSummaryDTO.setArrival(flightData[3] + " " + flightData[4]);
			}
		}

	}

	private void addFlightAllocationInfo(Collection<FCCInventoryDTO> fccInventoryDTOs,
			FCCAgentsSeatMvDTO fccSeatMvDTOWithOriginAgent, FlightCabinClassDTO flightCabinClassDTO,
			FCCAgentsSeatMvDTO fccSeatMvDTOWithOwnerAgent) {
		if (fccInventoryDTOs != null) {
			FlightBookingClassDTO flightBookingClassDTO = null;
			StringBuilder fareLink = null;
			FlightSegmentAllocationDTO segmentAllocationDTO = null;
			FlightSeatMvSummaryDTO flightSeatMvSummaryDTO = null;
			for (FCCInventoryDTO fccSegInventoryDTO : fccInventoryDTOs) {
				if (fccSegInventoryDTO != null) {
					Collection<FCCSegInventoryDTO> fccSegInventoryDTOs = fccSegInventoryDTO.getFccSegInventoryDTOs();
					if (fccSegInventoryDTOs != null) {
						for (FCCSegInventoryDTO fccSegInventoryDTO2 : fccSegInventoryDTOs) {
							if (fccSegInventoryDTO2 != null) {
								FCCSegInventory fccSegInventory = fccSegInventoryDTO2.getFccSegInventory();
								segmentAllocationDTO = new FlightSegmentAllocationDTO();
								segmentAllocationDTO.setFlightNo(fccSegInventoryDTO2.getFlightNumber());
								segmentAllocationDTO.setSegmentCode(fccSegInventory.getSegmentCode());
								segmentAllocationDTO.setAllocatedAdult(fccSegInventory.getAllocatedSeats());
								segmentAllocationDTO.setOversellAdult(fccSegInventory.getOversellSeats());
								segmentAllocationDTO.setCurtailedAdult(fccSegInventory.getCurtailedSeats());
								segmentAllocationDTO.setAllocatedInfant(fccSegInventory.getInfantAllocation());
								segmentAllocationDTO.setSoldAdultInfant(fccSegInventory.getSeatsSold() + "/"
										+ fccSegInventory.getSoldInfantSeats());
								segmentAllocationDTO.setOnholdAdultInfant(fccSegInventory.getOnHoldSeats() + "/"
										+ fccSegInventory.getOnholdInfantSeats());
								segmentAllocationDTO.setAvailableAdultInfant(fccSegInventory.getAvailableSeats() + "/"
										+ fccSegInventory.getAvailableInfantSeats());
								int overbookCount = (fccSegInventory.getSeatsSold() + fccSegInventory.getOnHoldSeats()
										- fccSegInventory.getOversellSeats() - fccSegInventory.getAllocatedSeats() + fccSegInventory
										.getCurtailedSeats());
								segmentAllocationDTO.setOverbookAdult(overbookCount < 0 ? 0 : overbookCount);
								segmentAllocationDTO.setStatus(fccSegInventoryDTO2.getFlightSegmentStatus());
								flightCabinClassDTO.addSegmentAllocationDTO(segmentAllocationDTO);

								// Add Seat Movement Info
								if (fccSeatMvDTOWithOriginAgent != null) {
									Collection<SeatMvSummaryDTO> seatMvSummaryDTOs = fccSeatMvDTOWithOriginAgent
											.getSegAgentsSeatMvWithFCCAgentsSeatMv(new Integer(fccSegInventory.getFlightSegId()));
									if (seatMvSummaryDTOs != null) {
										for (SeatMvSummaryDTO seatMvSummary : seatMvSummaryDTOs) {
											if (seatMvSummary != null) {
												flightSeatMvSummaryDTO = new FlightSeatMvSummaryDTO();
												flightSeatMvSummaryDTO.setAgentName(seatMvSummary.getAgentName());
												flightSeatMvSummaryDTO.setSoldSeats(seatMvSummary.getSoldSeats() + "/"
														+ seatMvSummary.getSoldChildSeats() + "/"
														+ seatMvSummary.getSoldInfantSeats());
												flightSeatMvSummaryDTO.setOnholdSeats(seatMvSummary.getOnholdSeats() + "/"
														+ seatMvSummary.getOnHoldChildSeats() + "/"
														+ seatMvSummary.getOnholdInfantSeats());
												flightSeatMvSummaryDTO.setCancelledSeats(seatMvSummary.getCancelledSeats() + "/"
														+ seatMvSummary.getCancelledChildSeats() + "/"
														+ seatMvSummary.getCancelledInfantSeats());
												flightSeatMvSummaryDTO.setTotalSold(seatMvSummary.getSoldSeatsSum() + "/"
														+ seatMvSummary.getSoldChildSeatsSum() + "/"
														+ seatMvSummary.getSoldInfantSeatsSum());
												flightSeatMvSummaryDTO.setTotalOnhold(seatMvSummary.getOnholdSeatsSum() + "/"
														+ seatMvSummary.getOnHoldChildSeatsSum() + "/"
														+ seatMvSummary.getOnholdInfantSeatsSum());
												flightSeatMvSummaryDTO.setTotalCancelled(seatMvSummary.getCancelledSeatsSum()
														+ "/" + seatMvSummary.getCancelledChildSeatsSum() + "/"
														+ seatMvSummary.getCancelledInfantSeatsSum());
												segmentAllocationDTO.addFlightSeatMvSummaryDTO(flightSeatMvSummaryDTO);
											}
										}
									}
								}

								// Add seat movement informations with respective to owner agents

								if (fccSeatMvDTOWithOwnerAgent != null) {
									Collection<SeatMvSummaryDTO> seatMvSummaryDTOs = fccSeatMvDTOWithOwnerAgent
											.getSegAgentsSeatMvWithFCCAgentsSeatMv(new Integer(fccSegInventory.getFlightSegId()));
									if (seatMvSummaryDTOs != null) {
										for (SeatMvSummaryDTO seatMvSummary : seatMvSummaryDTOs) {
											if (seatMvSummary != null) {
												flightSeatMvSummaryDTO = new FlightSeatMvSummaryDTO();
												flightSeatMvSummaryDTO.setAgentName(seatMvSummary.getAgentName());
												flightSeatMvSummaryDTO.setSoldSeats(seatMvSummary.getSoldSeats() + "/"
														+ seatMvSummary.getSoldChildSeats() + "/"
														+ seatMvSummary.getSoldInfantSeats());
												flightSeatMvSummaryDTO.setOnholdSeats(seatMvSummary.getOnholdSeats() + "/"
														+ seatMvSummary.getOnHoldChildSeats() + "/"
														+ seatMvSummary.getOnholdInfantSeats());
												flightSeatMvSummaryDTO.setCancelledSeats(seatMvSummary.getCancelledSeats() + "/"
														+ seatMvSummary.getCancelledChildSeats() + "/"
														+ seatMvSummary.getCancelledInfantSeats());
												flightSeatMvSummaryDTO.setTotalSold(seatMvSummary.getSoldSeatsSum() + "/"
														+ seatMvSummary.getSoldChildSeatsSum() + "/"
														+ seatMvSummary.getSoldInfantSeatsSum());
												flightSeatMvSummaryDTO.setTotalOnhold(seatMvSummary.getOnholdSeatsSum() + "/"
														+ seatMvSummary.getOnHoldChildSeatsSum() + "/"
														+ seatMvSummary.getOnholdInfantSeatsSum());
												flightSeatMvSummaryDTO.setTotalCancelled(seatMvSummary.getCancelledSeatsSum()
														+ "/" + seatMvSummary.getCancelledChildSeatsSum() + "/"
														+ seatMvSummary.getCancelledInfantSeatsSum());
												segmentAllocationDTO
														.addFlightSeatMvSummaryDTOWithOwnerAgent(flightSeatMvSummaryDTO);
											}
										}
									}
								}

								Collection<ExtendedFCCSegBCInventory> fccSegBCInventories = fccSegInventoryDTO2
										.getFccSegBCInventories();
								if (fccSegBCInventories != null) {
									for (ExtendedFCCSegBCInventory bcInventory : fccSegBCInventories) {
										if (bcInventory != null) {
											FCCSegBCInventory fccSegBCInventory = bcInventory.getFccSegBCInventory();
											flightBookingClassDTO = new FlightBookingClassDTO();
											flightBookingClassDTO.setBookingClass(fccSegBCInventory.getBookingCode());

											if (bcInventory.getNestRank() != null) {
												flightBookingClassDTO.setRank(bcInventory.getNestRank().toString());
											} else {
												flightBookingClassDTO.setRank("");
											}

											flightBookingClassDTO.setFixed(bcInventory.isFixedFlag() ? 'Y' : 'N');
											flightBookingClassDTO.setPriority(fccSegBCInventory.getPriorityFlag() ? 'Y' : 'N');
											flightBookingClassDTO.setAllocation(fccSegBCInventory.getSeatsAllocated());
											flightBookingClassDTO.setSold(fccSegBCInventory.getSeatsSold());
											flightBookingClassDTO.setOnhold(fccSegBCInventory.getOnHoldSeats());
											flightBookingClassDTO.setCancelled(fccSegBCInventory.getSeatsCancelled());
											flightBookingClassDTO.setAquired(fccSegBCInventory.getSeatsAcquired());
											flightBookingClassDTO.setAvailable(fccSegBCInventory.getSeatsAvailable());

											if (fccSegBCInventory.getStatus().endsWith(FCCSegBCInventory.Status.CLOSED)) {
												flightBookingClassDTO.setClosed('Y');
											} else {
												flightBookingClassDTO.setClosed('N');
											}

											flightBookingClassDTO.setSeatsSoldAquiredByNesting(fccSegBCInventory
													.getSeatsSoldAquiredByNesting());
											flightBookingClassDTO.setSeatsSoldNested(fccSegBCInventory.getSeatsSoldNested());
											flightBookingClassDTO.setSeatsOnHoldAquiredByNesting(fccSegBCInventory
													.getSeatsOnHoldAquiredByNesting());
											flightBookingClassDTO.setSeatsOnHoldNested(fccSegBCInventory.getSeatsOnHoldNested());

											fareLink = new StringBuilder();
											Collection<FareSummaryLightDTO> summaryLightDTOs = bcInventory
													.getLinkedEffectiveFares();
											if (summaryLightDTOs != null) {

												for (FareSummaryLightDTO fareSummaryDTO : summaryLightDTOs) {
													double fareAmount = fareSummaryDTO.getFareAmount();
													if (summaryLightDTOs.size() == 1) {
														fareLink.append(fareAmount);
													} else {
														fareLink.append(fareAmount);
														fareLink.append(" ");
													}
												}
											}
											flightBookingClassDTO.setFareFareRules(fareLink.toString());

											if (bcInventory.getLinkedEffectiveAgents() != null) {
												flightBookingClassDTO.setAgents(bcInventory.getLinkedEffectiveAgents().size());
											} else {
												flightBookingClassDTO.setAgents(0);
											}

											segmentAllocationDTO.addFlightBookingClassDTO(flightBookingClassDTO);
										}
									}
								}
							}
						}
					}

				}
			}

		}
	}

	private Map<String, String> extractCabinClassMap(List<Map<String, String>> mapsList) {
		Map<String, String> cabinClassMap = new HashMap<String, String>();
		if (mapsList != null) {
			for (Map<String, String> map : mapsList) {
				if (map != null) {
					cabinClassMap.put(map.get("CABIN_CLASS_CODE"), map.get("DESCRIPTION"));
				}
			}
		}
		return cabinClassMap;
	}

	private static void saveMessage(HttpServletRequest request, String userMessage, String msgType) {
		request.setAttribute(WebConstants.REQ_MESSAGE, userMessage);
		request.setAttribute(WebConstants.MSG_TYPE, msgType);
	}

	public void setFltId(Integer fltId) {
		this.fltId = fltId;
	}

	public void setOlFlightId(Integer olFlightId) {
		this.olFlightId = olFlightId;
	}

	public void setHdnFlightSumm(String hdnFlightSumm) {
		this.hdnFlightSumm = hdnFlightSumm;
	}

}
