package com.isa.thinair.airadmin.core.web.action.inventory;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.handler.inventory.INVNotesRH;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Results({
		@Result(name = AdminStrutsConstants.AdminAction.SUCCESS, value = AdminStrutsConstants.AdminJSP.SEAT_ALLOC_ADD_NOTE_JSP),
		@Result(name = AdminStrutsConstants.AdminAction.VIEW, value = AdminStrutsConstants.AdminJSP.SEAT_ALLOC_VIEW_NOTE_JSP),
		@Result(name = AdminStrutsConstants.AdminAction.ERROR, value = AdminStrutsConstants.AdminJSP.ERROR_JSP) })
public class ShowINVNotesAction extends BaseRequestAwareAction {

	public String execute() throws Exception {
		return INVNotesRH.execute(request);
	}

}
