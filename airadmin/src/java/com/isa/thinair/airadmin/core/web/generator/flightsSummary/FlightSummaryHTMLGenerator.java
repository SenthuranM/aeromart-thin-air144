package com.isa.thinair.airadmin.core.web.generator.flightsSummary;

import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.commons.api.exception.ModuleException;

public class FlightSummaryHTMLGenerator {

	private static String clientErrors;

	public static String getClientErrors(HttpServletRequest request) throws ModuleException {
		if (clientErrors == null) {
			Properties moduleErrs = new Properties();

			moduleErrs.setProperty("um.Flightschedule.search.mandatory", "mandatorysearch");
			moduleErrs.setProperty("um.pfsprocess.form.flightDateFormat.invaid", "invaldate");

			clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}
		return clientErrors;
	}

}
