package com.isa.thinair.airadmin.core.web.v2.dto.inventory;

import java.io.Serializable;
import java.util.List;

public class FCCSegInventoryTO implements Serializable, Comparable<FCCSegInventoryTO> {

	private static final long serialVersionUID = 1L;

	private String flightNo;

	private int flightId;

	private int fccsInvId;

	private Integer fccInvId;

	private String segmentCode;

	private String ccCode;

	private String logicalCCCode;

	private Integer logicalCCRank;

	private int allocatedSeats;

	private int oversellSeats;

	private int curtailedSeats;
	
	private int waitListSeats;
	
	private int waitListedSeats;

	private String seatsSold;

	private String onHoldSeats;

	private int fixedSeats;

	private List<FCCSegBCInventoryTO> fccSegBCInventories;

	private List<SeatMvSummaryTO> seatMvSummaryTOs;

	private List<String[]> bcCodeList;

	/*
	 * availabe seats is calculated considering allocated, oversell, curtailed, sold and onhold on the segment and sold,
	 * onhold, fixed on the intercepting segments
	 */
	private String availableSeats;

	private int infantAllocation;

	private int soldInfantSeats;

	private int onholdInfantSeats;

	/*
	 * availabe infant seats is calculated considering infant allocated, infant sold and infant onhold on the segment
	 * and infant sold, infant onhold on the intercepting segments
	 */
	private int availableInfantSeats;

	private long version;

	private String flightSegmentStatus;

	private int overbookCount;

	private int flightSegId;

	private int totalBCAllocation = 0;

	private int totalFixedAllocation = 0;

	public int compareTo(FCCSegInventoryTO o) {
		return this.getLogicalCCRank().compareTo(o.getLogicalCCRank());
	}

	public String getFlightNo() {
		return flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public int getFlightId() {
		return flightId;
	}

	public void setFlightId(int flightId) {
		this.flightId = flightId;
	}

	public int getFccsInvId() {
		return fccsInvId;
	}

	public void setFccsInvId(int fccsInvId) {
		this.fccsInvId = fccsInvId;
	}

	public Integer getFccInvId() {
		return fccInvId;
	}

	public void setFccInvId(Integer fccInvId) {
		this.fccInvId = fccInvId;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public String getCcCode() {
		return ccCode;
	}

	public void setCcCode(String ccCode) {
		this.ccCode = ccCode;
	}

	public String getLogicalCCCode() {
		return logicalCCCode;
	}

	public void setLogicalCCCode(String logicalCCCode) {
		this.logicalCCCode = logicalCCCode;
	}

	public int getAllocatedSeats() {
		return allocatedSeats;
	}

	public void setAllocatedSeats(int allocatedSeats) {
		this.allocatedSeats = allocatedSeats;
	}

	public int getOversellSeats() {
		return oversellSeats;
	}

	public void setOversellSeats(int oversellSeats) {
		this.oversellSeats = oversellSeats;
	}

	public int getCurtailedSeats() {
		return curtailedSeats;
	}

	public void setCurtailedSeats(int curtailedSeats) {
		this.curtailedSeats = curtailedSeats;
	}

	public String getSeatsSold() {
		return seatsSold;
	}

	public void setSeatsSold(String seatsSold) {
		this.seatsSold = seatsSold;
	}

	public String getOnHoldSeats() {
		return onHoldSeats;
	}

	public void setOnHoldSeats(String onHoldSeats) {
		this.onHoldSeats = onHoldSeats;
	}

	public int getFixedSeats() {
		return fixedSeats;
	}

	public void setFixedSeats(int fixedSeats) {
		this.fixedSeats = fixedSeats;
	}

	public List<FCCSegBCInventoryTO> getFccSegBCInventories() {
		return fccSegBCInventories;
	}

	public void setFccSegBCInventories(List<FCCSegBCInventoryTO> fccSegBCInventories) {
		this.fccSegBCInventories = fccSegBCInventories;
	}

	public List<SeatMvSummaryTO> getSeatMvSummaryTOs() {
		return seatMvSummaryTOs;
	}

	public void setSeatMvSummaryTOs(List<SeatMvSummaryTO> seatMvSummaryTOs) {
		this.seatMvSummaryTOs = seatMvSummaryTOs;
	}

	public List<String[]> getBcCodeList() {
		return bcCodeList;
	}

	public void setBcCodeList(List<String[]> bcCodeList) {
		this.bcCodeList = bcCodeList;
	}

	public String getAvailableSeats() {
		return availableSeats;
	}

	public void setAvailableSeats(String availableSeats) {
		this.availableSeats = availableSeats;
	}

	public int getInfantAllocation() {
		return infantAllocation;
	}

	public void setInfantAllocation(int infantAllocation) {
		this.infantAllocation = infantAllocation;
	}

	public int getSoldInfantSeats() {
		return soldInfantSeats;
	}

	public void setSoldInfantSeats(int soldInfantSeats) {
		this.soldInfantSeats = soldInfantSeats;
	}

	public int getOnholdInfantSeats() {
		return onholdInfantSeats;
	}

	public void setOnholdInfantSeats(int onholdInfantSeats) {
		this.onholdInfantSeats = onholdInfantSeats;
	}

	public int getAvailableInfantSeats() {
		return availableInfantSeats;
	}

	public void setAvailableInfantSeats(int availableInfantSeats) {
		this.availableInfantSeats = availableInfantSeats;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public String getFlightSegmentStatus() {
		return flightSegmentStatus;
	}

	public void setFlightSegmentStatus(String flightSegmentStatus) {
		this.flightSegmentStatus = flightSegmentStatus;
	}

	public int getOverbookCount() {
		return overbookCount;
	}

	public void setOverbookCount(int overbookCount) {
		this.overbookCount = overbookCount;
	}

	public int getFlightSegId() {
		return flightSegId;
	}

	public void setFlightSegId(int flightSegId) {
		this.flightSegId = flightSegId;
	}

	public int getTotalBCAllocation() {
		return totalBCAllocation;
	}

	public void setTotalBCAllocation(int totalBCAllocation) {
		this.totalBCAllocation = totalBCAllocation;
	}

	public int getTotalFixedAllocation() {
		return totalFixedAllocation;
	}

	public void setTotalFixedAllocation(int totalFixedAllocation) {
		this.totalFixedAllocation = totalFixedAllocation;
	}

	public Integer getLogicalCCRank() {
		return logicalCCRank;
	}

	public void setLogicalCCRank(Integer logicalCCRank) {
		this.logicalCCRank = logicalCCRank;
	}

	public int getWaitListSeats() {
		return waitListSeats;
	}

	public void setWaitListSeats(int waitListSeats) {
		this.waitListSeats = waitListSeats;
	}

	public int getWaitListedSeats() {
		return waitListedSeats;
	}

	public void setWaitListedSeats(int waitListedSeats) {
		this.waitListedSeats = waitListedSeats;
	}
	

}
