package com.isa.thinair.airadmin.core.web.util;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.util.CollectionUtils;

import com.isa.thinair.airadmin.core.service.AiradminModuleConfig;
import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.promotion.api.model.BundledFare;
import com.isa.thinair.promotion.api.model.BundledFarePeriod;
import com.isa.thinair.promotion.api.to.bundledFare.BundledFareDisplayDTO;
import com.isa.thinair.promotion.api.to.bundledFare.BundledFareDisplaySettingsDTO;
import com.isa.thinair.promotion.api.to.constants.BundledFareConstants;

public class BundledFareDisplayUtil {

	private static final Log log = LogFactory.getLog(BundledFareDisplayUtil.class);

	/**
	 * Copy matching display settings from source template to target template
	 * 
	 * @param sourceBundledFareId
	 * @param targetBundledFareId
	 * @throws ModuleException
	 */
	public static void copyDisplaySettings(Integer sourceBundledFareId, Integer targetBundledFareId) throws ModuleException {

		BundledFare sourceBundleFare = ModuleServiceLocator.getBundledFareBD().getBundledFareById(sourceBundledFareId);
		BundledFare targetBundledFare = ModuleServiceLocator.getBundledFareBD().getBundledFareById(targetBundledFareId);

		Set<BundledFarePeriod> sourceBundledFarePeriods = sourceBundleFare.getBundledFarePeriods();
		Set<BundledFarePeriod> targetBundledPeriods = targetBundledFare.getBundledFarePeriods();

		if (!CollectionUtils.isEmpty(sourceBundledFarePeriods) && !CollectionUtils.isEmpty(targetBundledPeriods)) {
			for (BundledFarePeriod sourceBundledFarePeriod : sourceBundledFarePeriods) {
				Date sourceFromDate = sourceBundledFarePeriod.getFlightDateFrom();
				Date sourceToDate = sourceBundledFarePeriod.getFlightDateTo();

				for (BundledFarePeriod targetBundledPeriod : targetBundledPeriods) {
					Date targetFromDate = targetBundledPeriod.getFlightDateFrom();
					Date targetToDate = targetBundledPeriod.getFlightDateTo();

					if (CalendarUtil.isSameDay(sourceFromDate, targetFromDate)
							&& CalendarUtil.isSameDay(sourceToDate, targetToDate)) {

						Integer sourcePeriodId = sourceBundledFarePeriod.getBundleFarePeriodId();
						Integer targetPeriodId = targetBundledPeriod.getBundleFarePeriodId();

						BundledFareDisplayDTO sourceDisplayDTO = ModuleServiceLocator.getBundledFareBD()
								.getBundledFareDisplayDTO(sourcePeriodId);

						BundledFareDisplaySettingsDTO targetDisplaySettingsDTO = transformToDisplaySettingsDTO(targetPeriodId,
								sourceDisplayDTO);

						ModuleServiceLocator.getBundledFareBD().saveBundledFareDisplaySettings(targetDisplaySettingsDTO);

					}
				}

			}

		}

	}

	private static BundledFareDisplaySettingsDTO transformToDisplaySettingsDTO(Integer targetPeriodId,
			BundledFareDisplayDTO sourceDisplayDTO) {

		BundledFareDisplaySettingsDTO targetDisplaySettingsDTO = new BundledFareDisplaySettingsDTO();
		targetDisplaySettingsDTO.setBundleFarePeriodId(targetPeriodId);

		transformDescriptionTemplate(targetDisplaySettingsDTO, sourceDisplayDTO.getDisplayTemplateId());
		transformTranslations(targetDisplaySettingsDTO, sourceDisplayDTO.getTranslations());
		transformThumbnails(targetDisplaySettingsDTO, sourceDisplayDTO.getThumbnails(), targetPeriodId);

		return targetDisplaySettingsDTO;
	}

	private static void transformDescriptionTemplate(BundledFareDisplaySettingsDTO targetDisplaySettingsDTO,
			Integer sourceTemplateId) {
		targetDisplaySettingsDTO.setDisplayTemplateId(sourceTemplateId);
	}

	private static void transformTranslations(BundledFareDisplaySettingsDTO targetDisplaySettingsDTO,
			Map<String, Map<String, String>> translations) {
		Map<String, String> nameTranslations = new HashMap<String, String>();
		Map<String, String> descTranslations = new HashMap<String, String>();

		if (translations != null && !translations.isEmpty()) {
			for (String language : translations.keySet()) {
				if (!StringUtil.isNullOrEmpty(language)) {
					Map<String, String> contents = translations.get(language);
					populateTranslations(language, nameTranslations, contents, BundledFareConstants.DisplayKeys.name);
					populateTranslations(language, descTranslations, contents, BundledFareConstants.DisplayKeys.description);
				}
			}
		}
		targetDisplaySettingsDTO.setNameTranslations(nameTranslations);
		targetDisplaySettingsDTO.setDescTranslations(descTranslations);

	}

	private static void populateTranslations(String language, Map<String, String> targetTranslations,
			Map<String, String> sourceTranslations, String displayType) {
		if (sourceTranslations != null && !sourceTranslations.isEmpty()) {
			String sourceTranslation = sourceTranslations.get(displayType);
			if (!StringUtil.isNullOrEmpty(sourceTranslation)) {
				targetTranslations.put(language, sourceTranslation);
			}
		}
	}

	private static void transformThumbnails(BundledFareDisplaySettingsDTO targetDisplaySettingsDTO,
			Map<String, String> sourceTemplates, Integer targetPeriodId) {
		Map<String, String> targetThumbnails = new HashMap<String, String>();
		if (sourceTemplates != null && !sourceTemplates.isEmpty()) {
			for (String sourceLanguage : sourceTemplates.keySet()) {
				if (!StringUtil.isNullOrEmpty(sourceLanguage)) {
					String sourceUrl = sourceTemplates.get(sourceLanguage);
					if (!StringUtil.isNullOrEmpty(sourceUrl)) {
						String targetUrl = safeCopy(sourceUrl, targetPeriodId, sourceLanguage);
						targetThumbnails.put(sourceLanguage, targetUrl);
					}
				}
			}
		}
		targetDisplaySettingsDTO.setThumbnails(targetThumbnails);
	}

	/**
	 * Safe copy of the <source image> to <target image> with a different name and will return the mounted image url
	 * 
	 * - In DEV environments matching image files will be not available
	 * 
	 * - Failure will fall back to the <source image url>
	 * 
	 * @param sourceUrl
	 * @param targetPeriodId
	 * @param language
	 */
	private static String safeCopy(String sourceUrl, Integer targetPeriodId, String language) {

		String targetImageUrl = null;
		try {
			AiradminModuleConfig adminConfig = AiradminModuleUtils.getConfig();
			String localImageStore = adminConfig.getMealImages();
			String serverMountUrl = AppSysParamsUtil.getImageUploadPath();

			String[] sourceImageComponents = sourceUrl.split(BundledFareConstants.IMG_PREFIX);
			String sourceImageId = sourceImageComponents[1];
			String sourceImageName = BundledFareConstants.IMG_PREFIX + sourceImageId;
			String sourceImageFullName = localImageStore + sourceImageName;
			File sourceImage = new File(sourceImageFullName);

			String targetImageName = BundledFareConstants.IMG_PREFIX + targetPeriodId + "_" + language
					+ BundledFareConstants.IMG_SUFFIX;
			String targetImageFullName = localImageStore + targetImageName;
			File targetImage = new File(targetImageFullName);
			FileUtils.copyFile(sourceImage, targetImage);

			targetImageUrl = serverMountUrl + sourceImageName;

		} catch (IOException ie) {
			targetImageUrl = sourceUrl;
			log.error("Bundled fare source image does not exists,  will fall back to source image", ie);
		} catch (Exception e) {
			targetImageUrl = sourceUrl;
			log.error("Bundled fare target image copy failed, will fall back to source image", e);
		}
		return targetImageUrl;
	}
}
