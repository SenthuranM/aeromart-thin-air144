package com.isa.thinair.airadmin.core.web.handler.master;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.master.GDSHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airmaster.api.model.Gds;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author srikantha
 * 
 */

public final class GDSRequestHandler extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(GDSRequestHandler.class);

	private static AiradminConfig airadminConfig = new AiradminConfig();

	private static final String PARAM_VERSION = "hdnVersion";
	private static final String PARAM_GDSCODE = "txtGDSCode";
	private static final String PARAM_DESCRIPTION = "txtDesc";
	private static final String PARAM_REMARKS = "txtRemarks";
	private static final String PARAM_PUBLISH_MECHANISM = "selPublishMechanism";
	private static final String PARAM_MODE = "hdnMode";
	private static final String PARAM_GDS_ID = "hdnGDSId";
	private static final String PARAM_STATUS = "chkStatus";
	private static final String PARAM_AVS_AVAIL_THRESHOLD = "txtAVSAvailThreshold";
	private static final String PARAM_AVS_CLOSURE_THRESHOLD = "txtAVSClosureThreshold";
	private static final String PARAM_NAVS_ENABLED = "chkEnableNAVS";
	private static final String PARAM_TYPE_A_VER_NO = "typeAVersion";
	private static final String PARAM_TYPE_A_SENDER_ID = "typeASenderId";
	private static final String PARAM_TYPE_A_SYNTAX_NO = "typeASyntaxId";
	private static final String PARAM_TYPE_A_SYNTAX_VER_NO = "typeASyntaxVersionNumber";
	private static final String PARAM_TYPE_A_CTRL_AGENCY = "typeAControllingAgency";
	private static final String AIRLINE_RES_MODIFIABLE = "airlineResModifiable";

	

	private static boolean isSuccessfulSaveMessageDisplayEnabled() {
		boolean isMessageEnabled = false;
		if (airadminConfig.getMessage("um.airadmin.savemessage.display").equals("true")) {
			isMessageEnabled = true;
		}
		return isMessageEnabled;
	}

	private static void setExceptionOccured(HttpServletRequest request, boolean value) {
		setAttribInRequest(request, "isExceptionOccured", new Boolean(value));
	}

	private static boolean isExceptionOccured(HttpServletRequest request) {
		return ((Boolean) getAttribInRequest(request, "isExceptionOccured")).booleanValue();
	}

	/**
	 * Main Execute Method for GDS Actions
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {

		String strHdnMode = request.getParameter(PARAM_MODE);
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;

		setExceptionOccured(request, false);
		setAttribInRequest(request, "strModeJS", "var isAddMode = false;");
		setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = false;");

		try {

			if (strHdnMode != null) {
				if (strHdnMode.equals(WebConstants.ACTION_ADD)) {
					checkPrivilege(request, WebConstants.PRIV_SYS_MAS_GDS_ADD);
					setAttribInRequest(request, "strModeJS", "var isAddMode = true;");
					saveData(request);
				}
				if (strHdnMode.equals(WebConstants.ACTION_EDIT)) {
					checkPrivilege(request, WebConstants.PRIV_SYS_MAS_GDS_EDIT);
					setAttribInRequest(request, "strModeJS", "var isAddMode = false;");
					saveData(request);
				}
			}
			setDisplayData(request);
		} catch (Exception exception) {
			if (exception instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
			}
		}
		return forward;
	}

	/**
	 * Saves the GDS
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	private static void saveData(HttpServletRequest request) throws Exception {

		log.debug("Entered inside the saveData()...");
		Gds gds = new Gds();

		String strGDSId = null;
		String strVersion = null;
		String strGDSCode = null;
		String strRemarks = null;
		String strDescription = null;
		String strPublishMechanism = null;
		String strStatus = null;
		String strAVSAvailThreshold = null;
		String strAVSClosureThreshold = null;
		String navsEnabled = null;
		String typeAVersion = null;
		String typeASenderId = null;
		String typeASyntaxId = null;
		String typeASyntaxVerId = null;
		String typeAControllingAgency = null;
		String airlineResModifiable = null;

		try {
			strGDSId = AiradminUtils.getNotNullString(request.getParameter(PARAM_GDS_ID));
			strVersion = AiradminUtils.getNotNullString(request.getParameter(PARAM_VERSION));
			strGDSCode = AiradminUtils.getNotNullString(request.getParameter(PARAM_GDSCODE));
			strRemarks = AiradminUtils.getNotNullString(request.getParameter(PARAM_REMARKS));
			strDescription = AiradminUtils.getNotNullString(request.getParameter(PARAM_DESCRIPTION));
			strPublishMechanism = AiradminUtils.getNotNullString(request.getParameter(PARAM_PUBLISH_MECHANISM));
			strStatus = AiradminUtils.getNotNullString(request.getParameter(PARAM_STATUS));
			strAVSAvailThreshold = AiradminUtils.getNotNullString(request.getParameter(PARAM_AVS_AVAIL_THRESHOLD));
			strAVSClosureThreshold = AiradminUtils.getNotNullString(request.getParameter(PARAM_AVS_CLOSURE_THRESHOLD));
			navsEnabled = AiradminUtils.getNotNullString(request.getParameter(PARAM_NAVS_ENABLED));
			typeAVersion = AiradminUtils.getNotNullString(request.getParameter(PARAM_TYPE_A_VER_NO));
			typeASenderId = AiradminUtils.getNotNullString(request.getParameter(PARAM_TYPE_A_SENDER_ID));
			typeASyntaxId = AiradminUtils.getNotNullString(request.getParameter(PARAM_TYPE_A_SYNTAX_NO));
			typeASyntaxVerId = AiradminUtils.getNotNullString(request.getParameter(PARAM_TYPE_A_SYNTAX_VER_NO));
			typeAControllingAgency = AiradminUtils.getNotNullString(request.getParameter(PARAM_TYPE_A_CTRL_AGENCY));
			airlineResModifiable = AiradminUtils.getNotNullString(request.getParameter(AIRLINE_RES_MODIFIABLE));


			if (!"".equalsIgnoreCase(strGDSId))
				gds.setGdsId(Integer.valueOf(strGDSId));
			if (strVersion != null && !"".equals(strVersion)) {
				gds.setVersion(Long.parseLong(strVersion));
			}

			gds.setGdsCode(strGDSCode.toUpperCase());
			gds.setDescription(strDescription.trim());
			gds.setRemarks(strRemarks.trim());
			gds.setSchedPublishMechanismCode(strPublishMechanism);
			if (strStatus.equals("on"))
				gds.setStatus(Gds.STATUS_ACTIVE);
			else
				gds.setStatus(Gds.STATUS_INACTIVE);
			try {
				gds.setAvsAvailThreshold(Integer.valueOf(strAVSAvailThreshold));
			} catch (NumberFormatException e) {
				gds.setAvsAvailThreshold(null);
			}
			try {
				gds.setAvsClosureThreshold(Integer.valueOf(strAVSClosureThreshold));
			} catch (NumberFormatException e) {
				gds.setAvsClosureThreshold(null);
			}
			
			if (navsEnabled.equals("on")) {
				gds.setEnableNavs(Gds.ENABLED_Y);
			}				
			else {
				gds.setEnableNavs(Gds.DISABLED_N);
			}

			gds.setAirlineResModifiable(airlineResModifiable.equals("on"));
			gds.setTypeAVersion(typeAVersion);
			gds.setTypeASenderId(typeASenderId);
			gds.setTypeASyntaxId(typeASyntaxId);
			gds.setTypeASyntaxVersionNumber(typeASyntaxVerId);
			
			gds.setTypeBSyncType(1);
			gds.setOnlyANumeric(Gds.DISABLED_N);
			gds.setEnableRouteWiseAutoPublish(Gds.DISABLED_N);
			gds.setEnableAutoScheduleSplit(Gds.DISABLED_N);
			gds.setUseAeroMartETS(Gds.DISABLED_N);

			ModuleServiceLocator.getGdsServiceBD().saveGds(gds);
			setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = true;");
			gds = null;

			saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS), WebConstants.MSG_SUCCESS);
			log.debug("GDSRequestHandler.saveData() method is successfully executed.");

		} catch (ModuleException moduleException) {
			log.error("Exception in GDSRequestHandler.saveData() [origin module=" + moduleException.getModuleDesc() + "]",
					moduleException);
			setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = false;");
			setExceptionOccured(request, true);

			String strFormData = "var arrFormData = new Array();";

			strFormData += "arrFormData[0] = new Array();";
			strFormData += "arrFormData[0][1] = '" + strGDSCode + "';";
			strFormData += "arrFormData[0][2] = '" + strDescription + "';";
			strFormData += "arrFormData[0][3] = '" + strRemarks + "';";
			strFormData += "arrFormData[0][4] = '" + strStatus + "';";
			strFormData += "arrFormData[0][5] = '" + strPublishMechanism + "';";
			strFormData += "arrFormData[0][6] = '" + strVersion + "';";
			strFormData += "arrFormData[0][7] = '" + strGDSId + "';";
			strFormData += "arrFormData[0][8] = '" + strAVSAvailThreshold + "';";
			strFormData += "arrFormData[0][9] = '" + strAVSClosureThreshold + "';";
			strFormData += "arrFormData[0][10] = '" + navsEnabled + "';";
			strFormData += "arrFormData[0][11] = '" + typeAVersion + "';";
			strFormData += "arrFormData[0][12] = '" + typeASenderId + "';";
			strFormData += "arrFormData[0][13] = '" + typeASyntaxId + "';";
			strFormData += "arrFormData[0][14] = '" + typeASyntaxVerId + "';";
			strFormData += "arrFormData[0][15] = '" + typeAControllingAgency + "';";
			strFormData += "arrFormData[0][16] = '" + airlineResModifiable + "';";

			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);

			if (moduleException.getExceptionCode().equals("module.duplicate.key")) {
				saveMessage(request, airadminConfig.getMessage("um.gds.form.id.already.exist"), WebConstants.MSG_ERROR);
			} else {
				saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
			}

		} catch (Exception exception) {
			log.error("Exception in GDSRequestHandler.saveData()", exception);
			if (exception instanceof RuntimeException) {
				throw exception;
			}
		}
	}

	/**
	 * Set Display Data for the GDS page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setDisplayData(HttpServletRequest request) {

		setSchedPublishMechanismList(request);
		setGDSRowHtml(request);
		setClientErrors(request);

		if (!isExceptionOccured(request)) {
			String strFormData = "var arrFormData = new Array();";
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
		}

		request.setAttribute(WebConstants.REQ_OPERATION_MODE, getAttribInRequest(request, "strModeJS"));

		if (request.getParameter(PARAM_MODE) == null) {

			setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = false;");

			request.setAttribute(WebConstants.REQ_TRANSACTION_STATUS,
					getAttribInRequest(request, "isSaveTransactionSuccessfulJS"));
		} else if (request.getParameter(PARAM_MODE) != null && !request.getParameter(PARAM_MODE).equals(WebConstants.ACTION_ADD)
				&& !request.getParameter(PARAM_MODE).equals(WebConstants.ACTION_EDIT)) {

			setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = false;");

			request.setAttribute(WebConstants.REQ_TRANSACTION_STATUS,
					getAttribInRequest(request, "isSaveTransactionSuccessfulJS"));
		} else {

			request.setAttribute(WebConstants.REQ_TRANSACTION_STATUS,
					getAttribInRequest(request, "isSaveTransactionSuccessfulJS"));
		}

		request.setAttribute(WebConstants.REQ_TRANSACTION_DISPLAYSTATUS, "var isSuccessfulSaveMessageDisplayEnabled = "
				+ isSuccessfulSaveMessageDisplayEnabled() + ";");

	}

	// Haider 14Oct08
	/**
	 * Sets the Schedule Publish Mechanism list to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setSchedPublishMechanismList(HttpServletRequest request) {
		try {
			String strHtml = SelectListGenerator.createSchedPublishMechanismList();
			request.setAttribute(WebConstants.REQ_HTML_SCHED_PUBLISH_MECHANISM, strHtml);
		} catch (ModuleException moduleException) {
			log.error("Exception in GDSRequestHandler:setCountryList() [origin module=" + moduleException.getModuleDesc() + "]",
					moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Client validations For GDS Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = GDSHTMLGenerator.getClientErrors(request);
			if (strClientErrors == null)
				strClientErrors = "";

			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);

		} catch (ModuleException moduleException) {
			log.error("Exception in GDSRequestHandler.setClientErrors() [origin module=" + moduleException.getModuleDesc() + "]",
					moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the GDS Data Array To the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	protected static void setGDSRowHtml(HttpServletRequest request) {

		try {
			GDSHTMLGenerator htmlGen = new GDSHTMLGenerator();
			String strHtml = htmlGen.getGDSRowHtml(request);
			request.setAttribute(WebConstants.REQ_HTML_ROWS, strHtml);

		} catch (ModuleException moduleException) {
			log.error("Exception in GDSRequestHandler.setGDSRowHtml() [origin module=" + moduleException.getModuleDesc() + "]",
					moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}
}
