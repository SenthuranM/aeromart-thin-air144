package com.isa.thinair.airadmin.core.web.v2.handler.master;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airmaster.api.model.ItineraryAdvertisement;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

public class AdvertisementRH {

	private static Log log = LogFactory.getLog(AdvertisementRH.class);

	public static Page<ItineraryAdvertisement> searchAdvertisement(int pageNo, List<ModuleCriterion> criterion) {
		// call back end

		Page<ItineraryAdvertisement> page = null;
		try {
			if (criterion == null) {
				page = ModuleServiceLocator.getCommonServiceBD().getAdvertisements((pageNo - 1) * 20, 20);
			} else {
				if (pageNo == 0) {
					pageNo = 1;
				}
				page = ModuleServiceLocator.getCommonServiceBD().getAdvertisements((pageNo - 1) * 20, 20, criterion);
			}
		} catch (ModuleException e) {
			log.error(e);
		}
		return page;
	}

	public static boolean saveAdvertisement(ItineraryAdvertisement advertisement) throws ModuleException {
		ModuleServiceLocator.getCommonServiceBD().saveAdvertisement(advertisement);
		return true;
	}

	public static boolean deleteAdvertisement(Integer advertisementId) throws ModuleException {
		ModuleServiceLocator.getCommonServiceBD().deleteAdvertisement(advertisementId);
		return true;
	}

	public static ItineraryAdvertisement getAdvertisement(Integer advertisementId) throws ModuleException {
		ItineraryAdvertisement advertisement = ModuleServiceLocator.getCommonServiceBD().getAdvertisement(advertisementId);
		return advertisement;
	}

	public static ItineraryAdvertisement getAdvertisement(String advertisementTitle, String advertisementLanguage)
			throws ModuleException {
		ItineraryAdvertisement advertisement = ModuleServiceLocator.getCommonServiceBD().getAdvertisement(advertisementTitle,
				advertisementLanguage);
		return advertisement;
	}
	 
	public static boolean advertisementExistFor(String advertisementTitle,
			String advertisementLanguage) throws ModuleException {
		ItineraryAdvertisement advertisement = ModuleServiceLocator.getCommonServiceBD().getAdvertisement(advertisementTitle, advertisementLanguage);

		if (advertisement == null) {
			return true;
		} else {
			return false;
		}
	}

	public static String getAdvertisementSegmentWise(String ONDsegment) throws ModuleException {
		String advertisementIdSet = ModuleServiceLocator.getCommonServiceBD().getAdvertisementSegmentWise(ONDsegment);
		return advertisementIdSet;
	}
}
