package com.isa.thinair.airadmin.core.web.v2.action.master;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airadmin.core.web.v2.handler.master.SSRTemplateRH;
import com.isa.thinair.airadmin.core.web.v2.util.CommonUtil;
import com.isa.thinair.airmaster.api.dto.SSRTemplateSearchCriteria;
import com.isa.thinair.airmaster.api.model.SSR;
import com.isa.thinair.airmaster.api.model.SSRCategory;
import com.isa.thinair.airmaster.api.model.SSRTemplate;
import com.isa.thinair.airmaster.api.model.SSRTemplateCabinQuantity;
import com.isa.thinair.airpricing.api.util.SplitUtil;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.dto.SSRInfoDTO;
import com.isa.thinair.commons.api.exception.MessagesUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.SSRUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * 
 * @author rumesh
 * 
 */

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ShowSSRTemplateAction {

	private static Log log = LogFactory.getLog(ShowSSRTemplateAction.class);
	private static AiradminConfig airadminConfig = new AiradminConfig();

	private Object[] rows;
	private int page;
	private int total;
	private int records;
	private String succesMsg;
	private String msgType;
	private Integer templateId;
	private String templateCode;
	private String description;
	private String status;

	private int maxQuantity;
	private String createdBy;
	private Date createdDate;
	private long version;
	private int selTemplateId;
	private String selStatus;
	private String cabinClass;
	private String isGridSSR;
	private String ssrValue;
	private String ssrsForCC;
	private String ssrCabinQtys;
	private String templOption;
	private String cos;

	public String execute() {
		SSRTemplate ssrTemplate = createTemplate();

		try {
			SSRTemplateRH.saveTemplate(ssrTemplate);
			this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS);
			this.msgType = WebConstants.MSG_SUCCESS;
		} catch (ModuleException me) {
			this.succesMsg = me.getMessageString();
			if (me.getMessageString().equals(MessagesUtil.getMessage("module.duplicate.key"))) {
				this.succesMsg = airadminConfig.getMessage("um.mealTemplate.form.template.exist");
			}
			this.msgType = WebConstants.MSG_ERROR;
		} catch (Exception ex) {
			this.succesMsg = "System Error Please Contact Administrator";
			this.msgType = WebConstants.MSG_ERROR;
		}

		try {
			this.templOption = "<option value=''>All</option>" + SelectListGenerator.createSSRTemplateListAll();
		} catch (ModuleException e) {
			this.succesMsg = e.getMessageString();
			this.msgType = WebConstants.MSG_ERROR;
		}

		return S2Constants.Result.SUCCESS;
	}

	public String deleteTemplate() {
		try {
			if (templateId != null) {
				SSRTemplateRH.deleteTemplate(templateId);
			} else {
				throw new ModuleException("airadmin.desc");
			}
			this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_DELETE_SUCCESS);
			this.msgType = WebConstants.MSG_SUCCESS;
		} catch (ModuleException e) {
			this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_DELETE_FAIL);
			if (e.getMessageString().equals(MessagesUtil.getMessage("module.constraint.childrecord"))) {
				this.succesMsg = airadminConfig.getMessage("um.airadmin.childrecord");
			}
			this.msgType = WebConstants.MSG_ERROR;
		}

		try {
			this.templOption = "<option value=''>All</option>" + SelectListGenerator.createSSRTemplateListAll();
		} catch (ModuleException e) {
			this.succesMsg = e.getMessageString();
			this.msgType = WebConstants.MSG_ERROR;
		}

		return S2Constants.Result.SUCCESS;
	}

	public String searchSSRTemplates() throws ModuleException {

		SSRTemplateSearchCriteria criteria = new SSRTemplateSearchCriteria();

		if (selTemplateId != 0) {
			criteria.setTemplateId(selTemplateId);
		}
		if (selStatus != null && !"".equals(selStatus)) {
			criteria.setStatus(selStatus);
		}

		Page<SSRTemplate> ssrPage = null;
		if (this.page <= 0)
			this.page = 1;
		try {
			ssrPage = SSRTemplateRH.searchSSRTemplate(page, criteria);
		} catch (ModuleException exception) {
			this.succesMsg = exception.getMessageString();
			this.msgType = WebConstants.MSG_ERROR;
			if (log.isErrorEnabled()) {
				log.error("Error occured", exception);
			}
		}

		if (ssrPage != null) {
			this.records = ssrPage.getTotalNoOfRecords();
			this.total = ssrPage.getTotalNoOfRecords() / 20;
			int mod = ssrPage.getTotalNoOfRecords() % 20;
			if (mod > 0) {
				this.total = this.total + 1;
			}
			if (this.page < 0) {
				this.page = 1;
			}

			Collection<SSRTemplate> colSSRTemplates = ssrPage.getPageData();
			if (colSSRTemplates != null) {
				Object[] dataRow = new Object[colSSRTemplates.size()];
				int index = 1;
				for (SSRTemplate ssrTemplate : colSSRTemplates) {
					Map<String, Object> countMap = new HashMap<String, Object>();
					countMap.put("id", new Integer(((page - 1) * 20) + index).toString());
					countMap.put("ssrTemplate", ssrTemplate);
					countMap.put("ssrTemplQty", prepareSSRTemplateCabinQty(ssrTemplate.getSsrTemplateCabinQtySet()));
					dataRow[index - 1] = countMap;
					index++;
				}
				this.rows = dataRow;
			}
		}
		return S2Constants.Result.SUCCESS;
	}

	public String getSSRsForCabinClass() {
		try {
			List<SSR> lstSSR = null;
			StringBuffer sb = new StringBuffer();
			if (this.isGridSSR != null) {
				if (this.isGridSSR.trim().equals("true")) {
					String[] mealDataArry = this.ssrValue.split("#");
					sb.append("<option value='" + this.ssrValue + "'>" + mealDataArry[1] + "</option>");
					this.ssrsForCC = sb.toString();
				} else {
					if (this.cos != null && this.cos.trim() != "") {
						String[] splittedCabinClassAndLogicalCabinClass = SplitUtil
								.getSplittedCabinClassAndLogicalCabinClass(cos);
						lstSSR = ModuleServiceLocator.getSsrServiceBD().getActiveSSRs(splittedCabinClassAndLogicalCabinClass[0],
								splittedCabinClassAndLogicalCabinClass[1], SSRCategory.ID_INFLIGHT_SERVICE);
						for (SSR ssr : lstSSR) {
							sb.append("<option value='" + ssr.getSsrId() + "#" + ssr.getSsrName() + "'>" + ssr.getSsrName()
									+ "</option>");
						}
						this.ssrsForCC = sb.toString();
					}
				}
			}
		} catch (ModuleException e) {
			this.succesMsg = "System Error Please Contact Administrator";
			this.msgType = WebConstants.MSG_ERROR;
			if (log.isErrorEnabled()) {
				log.error(e);
			}
		}
		return S2Constants.Result.SUCCESS;
	}

	private SSRTemplate createTemplate() {
		SSRTemplate ssrTemplate = new SSRTemplate();

		ssrTemplate.setTemplateCode(templateCode.toUpperCase().trim());
		ssrTemplate.setDescription(description.trim());
		if (templateId != null) {
			ssrTemplate.setTemplateId(templateId);
		}
		if (status != null) {
			ssrTemplate.setStatus(status);
		} else {
			ssrTemplate.setStatus(SSR.STATUS_INACTIVE);
		}
		ssrTemplate.setVersion(version);
		ssrTemplate.setCreatedBy(createdBy);
		ssrTemplate.setCreatedDate(createdDate);

		Set<SSRTemplateCabinQuantity> setSSRQty = new HashSet<SSRTemplateCabinQuantity>();
		if (ssrCabinQtys != null && !ssrTemplate.equals("")) {
			String[] arrSSRs = ssrCabinQtys.split("~");
			for (int i = 0; i < arrSSRs.length; i++) {
				if (arrSSRs[i] != null && !arrSSRs[i].equals("")) {
					String[] strSSRs = arrSSRs[i].split("#");
					SSRTemplateCabinQuantity ssrQty = new SSRTemplateCabinQuantity();
					ssrQty.setSsrTemplate(ssrTemplate);
					ssrQty.setSsrId(new Integer(strSSRs[2]));
					ssrQty.setMaxQuantity(new Integer(strSSRs[3]));
					ssrQty.setVersion(new Integer(strSSRs[4]));
					ssrQty.setStatus(strSSRs[5]);
					if (strSSRs[6] != null && !strSSRs[6].trim().equals("")) {
						ssrQty.setTemplateCabinQtyId(new Integer(strSSRs[6]));
					}
					String[] splittedCabinClassAndLogicalCabinClass = SplitUtil
							.getSplittedCabinClassAndLogicalCabinClass(strSSRs[7]);
					ssrQty.setLogicalCCCode(splittedCabinClassAndLogicalCabinClass[1]);
					ssrQty.setCabinClass(splittedCabinClassAndLogicalCabinClass[0]);
					setSSRQty.add(ssrQty);
				}
			}
		}
		ssrTemplate.setSsrTemplateCabinQtySet(setSSRQty);

		return ssrTemplate;
	}

	private String prepareSSRTemplateCabinQty(Collection<SSRTemplateCabinQuantity> colSSRTmplCabinQty) throws ModuleException {
		StringBuilder sb = new StringBuilder();
		if (colSSRTmplCabinQty != null) {
			for (SSRTemplateCabinQuantity ssrTemplateCabinQty : colSSRTmplCabinQty) {
				int ssrId = ssrTemplateCabinQty.getSsrId();
				SSRInfoDTO ssrInfoDTO = SSRUtil.getSSRInfo(ssrId);
				sb.append((ssrInfoDTO != null) ? ssrInfoDTO.getSSRName() : "").append("#");
				sb.append(ssrTemplateCabinQty.getSsrTemplate().getTemplateId()).append("#");
				sb.append(ssrId).append("#");
				sb.append(ssrTemplateCabinQty.getMaxQuantity()).append("#");
				sb.append(ssrTemplateCabinQty.getVersion()).append("#");
				sb.append(ssrTemplateCabinQty.getStatus()).append("#");
				sb.append(ssrTemplateCabinQty.getTemplateCabinQtyId()).append("#");
				String cabinClassCode = ssrTemplateCabinQty.getCabinClass();
				String logicalCabinClass = ssrTemplateCabinQty.getLogicalCCCode();
				sb.append(CommonUtil.getCabinClassOrLogicalCabinClassDescription(cabinClassCode, logicalCabinClass)).append("#");
				sb.append(CommonUtil.concatanateCabinClassAndLogicalCabinClass(cabinClassCode, logicalCabinClass)).append("#")
						.append("~");
			}
		}

		return sb.toString();
	}
	
	public Object[] getRows() {
		return rows;
	}

	public void setRows(Object[] rows) {
		this.rows = rows;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public String getSuccesMsg() {
		return succesMsg;
	}

	public void setSuccesMsg(String succesMsg) {
		this.succesMsg = succesMsg;
	}

	public Integer getTemplateId() {
		return templateId;
	}

	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}

	public String getTemplateCode() {
		return templateCode;
	}

	public void setTemplateCode(String templateCode) {
		this.templateCode = templateCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getMaxQuantity() {
		return maxQuantity;
	}

	public void setMaxQuantity(int maxQuantity) {
		this.maxQuantity = maxQuantity;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public int getSelTemplateId() {
		return selTemplateId;
	}

	public void setSelTemplateId(int selTemplateId) {
		this.selTemplateId = selTemplateId;
	}

	public String getSelStatus() {
		return selStatus;
	}

	public void setSelStatus(String selStatus) {
		this.selStatus = selStatus;
	}

	public String getCabinClass() {
		return cabinClass;
	}

	public void setCabinClass(String cabinClass) {
		this.cabinClass = cabinClass;
	}

	public String getIsGridSSR() {
		return isGridSSR;
	}

	public void setIsGridSSR(String isGridSSR) {
		this.isGridSSR = isGridSSR;
	}

	public String getSsrValue() {
		return ssrValue;
	}

	public void setSsrValue(String ssrValue) {
		this.ssrValue = ssrValue;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public String getSsrsForCC() {
		return ssrsForCC;
	}

	public void setSsrsForCC(String ssrsForCC) {
		this.ssrsForCC = ssrsForCC;
	}

	public String getSsrCabinQtys() {
		return ssrCabinQtys;
	}

	public void setSsrCabinQtys(String ssrCabinQtys) {
		this.ssrCabinQtys = ssrCabinQtys;
	}

	public String getTemplOption() {
		return templOption;
	}

	public void setTemplOption(String templOption) {
		this.templOption = templOption;
	}

	/**
	 * @return the cos
	 */
	public String getCos() {
		return cos;
	}

	/**
	 * @param cos
	 *            the cos to set
	 */
	public void setCos(String cos) {
		this.cos = cos;
	}

}
