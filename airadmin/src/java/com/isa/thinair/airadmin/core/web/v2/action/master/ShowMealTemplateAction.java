package com.isa.thinair.airadmin.core.web.v2.action.master;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airadmin.core.web.v2.handler.master.MealTemplateRH;
import com.isa.thinair.airadmin.core.web.v2.util.CommonUtil;
import com.isa.thinair.airmaster.api.model.Meal;
import com.isa.thinair.airpricing.api.criteria.MealTemplateSearchCriteria;
import com.isa.thinair.airpricing.api.dto.MealTemplateDTO;
import com.isa.thinair.airpricing.api.model.MealCharge;
import com.isa.thinair.airpricing.api.model.MealTemplate;
import com.isa.thinair.airpricing.api.model.MultiSelectCategoryRestriction;
import com.isa.thinair.airpricing.api.util.SplitUtil;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.MessagesUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.util.Constants;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ShowMealTemplateAction {

	private static Log log = LogFactory.getLog(ShowMealTemplateAction.class);
	private Object[] rows;
	private int page;
	private int total;
	private int records;
	private String succesMsg;
	private Integer templateId;
	private String templateCode;
	private String description;
	private String status;
	private String mealCharges;
	private String createdBy;
	private Date createdDate;
	private long version;
	private String selTemplate;
	private String msgType;
	private String templOption;
	private String selStatus;
	private String cabinClass;
	private String mealsForCC;
	private String isGridMeal;
	private String mealValue;
	private String cos;
	private String localCurrCode;
	private String multiRestrictions;

	public String getSelStatus() {
		return selStatus;
	}

	public void setSelStatus(String selStatus) {
		this.selStatus = selStatus;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public String getSelTemplate() {
		return selTemplate;
	}

	public void setSelTemplate(String selTemplate) {
		this.selTemplate = selTemplate;
	}

	private static AiradminConfig airadminConfig = new AiradminConfig();

	public String getMealCharges() {
		return mealCharges;
	}

	public void setMealCharges(String mealCharges) {
		this.mealCharges = mealCharges;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Integer getTemplateId() {
		return templateId;
	}

	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}

	public String getTemplateCode() {
		return templateCode;
	}

	public void setTemplateCode(String templateCode) {
		this.templateCode = templateCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public String getSuccesMsg() {
		return succesMsg;
	}

	public void setSuccesMsg(String succesMsg) {
		this.succesMsg = succesMsg;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public Object[] getRows() {
		return rows;
	}

	public void setRows(Object[] rows) {
		this.rows = rows;
	}

	public String getTemplOption() {
		return templOption;
	}

	public void setTemplOption(String templOption) {
		this.templOption = templOption;
	}

	/**
	 * @return the cabinClass
	 */
	public String getCabinClass() {
		return cabinClass;
	}

	/**
	 * @param cabinClass
	 *            the cabinClass to set
	 */
	public void setCabinClass(String cabinClass) {
		this.cabinClass = cabinClass;
	}

	/**
	 * @return the cabinClass
	 */
	public String getMealsForCC() {
		return mealsForCC;
	}

	/**
	 * @param mealsForCC
	 *            the mealsForCC to set
	 */
	public void setMealsForCC(String mealsForCC) {
		this.mealsForCC = mealsForCC;
	}

	/**
	 * @return the isGridMeal
	 */
	public String getIsGridMeal() {
		return isGridMeal;
	}

	/**
	 * @param isGridMeal
	 *            the isGridMeal to set
	 */
	public void setIsGridMeal(String isGridMeal) {
		this.isGridMeal = isGridMeal;
	}

	/**
	 * @return the mealValue
	 */
	public String getMealValue() {
		return mealValue;
	}

	/**
	 * @param mealValue
	 *            the mealValue to set
	 */
	public void setMealValue(String mealValue) {
		this.mealValue = mealValue;
	}
	
	

	/**
	 * @return the multiRestrictions
	 */
	public String getMultiRestrictions() {
		return multiRestrictions;
	}

	/**
	 * @param multiRestrictions the multiRestrictions to set
	 */
	public void setMultiRestrictions(String multiRestrictions) {
		this.multiRestrictions = multiRestrictions;
	}

	public String execute() {
		try {			
			MealTemplateDTO mealTemplateDTO = getMealTemplateDTO();
			validate(mealTemplateDTO, false);			
			MealTemplateRH.saveMealTemplate(mealTemplateDTO);
			this.templOption = "<option value=''>All</option>" + SelectListGenerator.createMealTemplateList();
			this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS);
			this.msgType = WebConstants.MSG_SUCCESS;
		} catch (ModuleException e) {
			this.succesMsg = e.getMessageString();
			if (e.getMessageString().equals(MessagesUtil.getMessage("module.duplicate.key"))) {
				this.succesMsg = airadminConfig.getMessage("um.mealTemplate.form.template.exist");
			}
			this.msgType = WebConstants.MSG_ERROR;
		} catch (Exception ex) {
			log.error("Error is savig meal templates", ex);
			this.succesMsg = "System Error Please Contact Administrator";
			this.msgType = WebConstants.MSG_ERROR;
		}
		return S2Constants.Result.SUCCESS;
	}

	public String deleteTemplate() throws Exception {

		try {
			this.templOption = "<option value=''>All</option>" + SelectListGenerator.createMealTemplateList();
			MealTemplateDTO mealTemplateDTO = getMealTemplateDTO();
			validate(mealTemplateDTO, true);			
			MealTemplateRH.deleteMealTemplate(mealTemplateDTO);
			this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_DELETE_SUCCESS);
			this.msgType = WebConstants.MSG_SUCCESS;
		} catch (ModuleException e) {
			this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_DELETE_FAIL);
			if (e.getMessageString().equals(MessagesUtil.getMessage("module.constraint.childrecord"))) {
				this.succesMsg = airadminConfig.getMessage("um.airadmin.childrecord");
			}
			this.msgType = WebConstants.MSG_ERROR;
		}

		return S2Constants.Result.SUCCESS;
	}

	public String getMealsForCabinClass() {
		try {
			StringBuffer sb = new StringBuffer();
			if (this.isGridMeal != null) {
				if (this.isGridMeal.trim().equals("true")) {
					String[] mealDataArry = this.getMealValue().split(Constants.COMMA_SEPARATOR);
					sb.append("<option value='" + this.getMealValue() + "'>" + mealDataArry[1] + "</option>");
					this.mealsForCC = sb.toString();
				} else {
					String[] splittedCabinClassAndLogicalCabinClass = SplitUtil.getSplittedCabinClassAndLogicalCabinClass(cos);
					Collection<Meal> allMeals = ModuleServiceLocator.getCommonServiceBD().getAllMeals(
							splittedCabinClassAndLogicalCabinClass[0], splittedCabinClassAndLogicalCabinClass[1]);
					for (Meal meal : allMeals) {
						sb.append("<option value='" + meal.getMealId() + Constants.COMMA_SEPARATOR + meal.getMealName() + "'>"
								+ meal.getMealName() + "</option>");
					}
					this.mealsForCC = sb.toString();
				}
			}
		} catch (Exception e) {
			log.error("Errror in getMealsForCabinClass ", e);
			this.msgType = WebConstants.MSG_ERROR;
		}
		return S2Constants.Result.SUCCESS;
	}

	public String searchMealTemplate() {

		try {
			MealTemplateSearchCriteria criteria = new MealTemplateSearchCriteria();
			if (this.selTemplate != null && !this.selTemplate.trim().equals("")) {
				criteria.setTemplateCode(this.selTemplate);
			}
			if (this.selStatus != null && !this.selStatus.trim().equals("")) {
				criteria.setStatus(this.selStatus);
			}
			Page<MealTemplate> pgmeal = MealTemplateRH.searchMealTemplate(this.page, criteria);
			Collection<Meal> colMeal = null;
			try {
				colMeal = ModuleServiceLocator.getCommonServiceBD().getMeals();
			} catch (ModuleException exception) {
				log.error("Load meals failed", exception);
			}

			this.records = pgmeal.getTotalNoOfRecords();
			this.total = pgmeal.getTotalNoOfRecords() / 20;
			int mod = pgmeal.getTotalNoOfRecords() % 20;
			if (mod > 0) {
				this.total = this.total + 1;
			}
			if (this.page < 0) {
				this.page = 1;
			}

			Collection<MealTemplate> colMealTeplates = pgmeal.getPageData();
			if (colMealTeplates != null) {
				Object[] dataRow = new Object[colMealTeplates.size()];
				int index = 1;
				for (MealTemplate grdCountry : colMealTeplates) {
					Map<String, Object> counmap = new HashMap<String, Object>();
					counmap.put("id", new Integer(((page - 1) * 20) + index).toString());
					counmap.put("mealTemplate", grdCountry);
					counmap.put("mealCharge", addMealName(grdCountry.getMealCharges(), colMeal));
					counmap.put("catRestriction", getRestrictions(grdCountry.getCategoryRestrictions()));
					dataRow[index - 1] = counmap;
					index++;
				}
				this.rows = dataRow;
			}
		} catch (Exception e) {
			log.error("Errror in searchMealTemplate ", e);
			this.succesMsg = "System Error Please Contact Administrator";
			this.msgType = WebConstants.MSG_ERROR;
		}
		return S2Constants.Result.SUCCESS;
	}

	private MealTemplateDTO getMealTemplateDTO(){
		MealTemplateDTO mealTmpDTO = new MealTemplateDTO();
		mealTmpDTO.setCreatedBy(createdBy);
		mealTmpDTO.setCreatedDate(createdDate);
		mealTmpDTO.setDescription(description);
		mealTmpDTO.setLocalCurrCode(localCurrCode);
		mealTmpDTO.setMealCharges(mealCharges);
		mealTmpDTO.setStatus(status);
		mealTmpDTO.setTemplateCode(templateCode);
		mealTmpDTO.setTemplateId(templateId);
		mealTmpDTO.setVersion(version);
		mealTmpDTO.setMultiRestrictions(multiRestrictions);
		
		return mealTmpDTO;
	}

	private static void validate(MealTemplateDTO template, boolean fromDelete) throws ModuleException {
		final String STATUS_ACTIVE = "ACT";
		if (fromDelete || (template.getVersion() != -1 && !STATUS_ACTIVE.equals(template.getStatus())))
			MealTemplateRH.checkTemplateAttachedToRouteWiseDefAnciTempl(template.getTemplateId());
	}
	


	private String addMealName(Collection<MealCharge> colMealCharge, Collection<Meal> colMeal) throws ModuleException {
		Collection<MealCharge> chCol = new HashSet<MealCharge>();
		StringBuilder sb = new StringBuilder();
		if (colMealCharge != null && colMeal != null) {
			for (MealCharge mCharge : colMealCharge) {
				for (Meal meal : colMeal) {
					if (meal.getMealId().equals(mCharge.getMealId())) {
						mCharge.setMealName(meal.getMealName());
						break;
					}
				}
				chCol.add(mCharge);
			}
		} else {
			chCol = colMealCharge;
		}

		if (chCol != null) {
			for (MealCharge mealCharge : chCol) {
				sb.append(mealCharge.getMealName()).append(Constants.COMMA_SEPARATOR);
				sb.append(mealCharge.getChargeId()).append(Constants.COMMA_SEPARATOR);
				sb.append(mealCharge.getTemplateId().getTemplateId()).append(Constants.COMMA_SEPARATOR);
				sb.append(mealCharge.getMealId()).append(Constants.COMMA_SEPARATOR);
				sb.append(mealCharge.getAmount()).append(Constants.COMMA_SEPARATOR);
				sb.append(mealCharge.getAllocatedMeal()).append(Constants.COMMA_SEPARATOR);
				sb.append(mealCharge.getVersion()).append(Constants.COMMA_SEPARATOR);
				sb.append(mealCharge.getStatus()).append(Constants.COMMA_SEPARATOR);
				String cabinClass = mealCharge.getCabinClass();
				String logicalCabinClass = mealCharge.getLogicalCCCode();
				sb.append(CommonUtil.getCabinClassOrLogicalCabinClassDescription(cabinClass, logicalCabinClass)).append(
						Constants.COMMA_SEPARATOR);
				sb.append(CommonUtil.concatanateCabinClassAndLogicalCabinClass(cabinClass, logicalCabinClass))
						.append(Constants.COMMA_SEPARATOR);
				sb.append(mealCharge.getPopularity()).append(Constants.COMMA_SEPARATOR).append("~");
			}
		}
		return sb.toString();
	}

	/**
	 * @return the cos
	 */
	public String getCos() {
		return cos;
	}

	/**
	 * @param cos
	 *            the cos to set
	 */
	public void setCos(String cos) {
		this.cos = cos;
	}

	public String getLocalCurrCode() {
		return localCurrCode;
	}

	public void setLocalCurrCode(String localCurrCode) {
		this.localCurrCode = localCurrCode;
	}
	
	private String getRestrictions(Collection<MultiSelectCategoryRestriction> restrictions) {
		StringBuilder sb = new StringBuilder();

		if (restrictions != null) {
			for (MultiSelectCategoryRestriction restriction : restrictions) {
				sb.append(restriction.getMealCategoryId()).append(Constants.COMMA_SEPARATOR);
				sb.append(restriction.getMultiSelectRestrictionCatId()).append(Constants.COMMA_SEPARATOR);
				sb.append(restriction.getVersion());
				sb.append(Constants.COMMA_SEPARATOR).append("~");
			}
		}
		return sb.toString();
	}
}
