 package com.isa.thinair.airadmin.core.web.v2.action.master;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.api.OfficerMobNumsConfigsResultsTO;
import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airreservation.api.dto.OfficerMobNumsDTO;
import com.isa.thinair.airreservation.api.model.OfficersMobileNumbers;
import com.isa.thinair.commons.api.dto.OfficerMobNumsConfigsSearchDTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.util.DataUtil;
import com.isa.thinair.webplatform.core.util.RowDecorator;

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class showOfficersNotificationsConfigurationAction {
	
	private static final Log log = LogFactory.getLog(showOfficersNotificationsConfigurationAction.class);
	
	private static final int PAGE_LENGTH = 10;
	 
	private OfficerMobNumsConfigsResultsTO officerMobNumsConfigsResultsTO;
	
	private OfficerMobNumsConfigsSearchDTO officerMobNumsConfigsSearchDTO;
	
	private String hdnMode;
	
	private int page;

	private String msgType;

	private String messageTxt;
	
	private int total;

	private int records;
	
	private int hdnId;
	
	private String contactName;
	
	private String contactEmail;
	
	private String moblieNumber;
	
	private long version;
	
	private static AiradminConfig airadminConfig = new AiradminConfig();

	private Collection<Map<String, Object>> colOfficersMobileNumbersConfigs;
	
	public String execute(){
		try {
			
			if (officerMobNumsConfigsSearchDTO == null) {
				officerMobNumsConfigsSearchDTO = new OfficerMobNumsConfigsSearchDTO();
			}
			
			int startIndex = 0;
			
			if (page > 1) {
				startIndex = (page - 1) * PAGE_LENGTH;
			}
			
			Page pagedReocrds = ModuleServiceLocator.getReservationBD().getOfficersMobileNumbers(officerMobNumsConfigsSearchDTO,startIndex,PAGE_LENGTH);
			
			this.colOfficersMobileNumbersConfigs =((Collection<Map<String, Object>>) DataUtil.createGridDataDecorateOnly(
					pagedReocrds.getStartPosition(), pagedReocrds.getPageData(), decorateRow()));
			
			this.page = getCurrentPageNo(pagedReocrds.getStartPosition());
			this.setTotal(getTotalNoOfPages(pagedReocrds.getTotalNoOfRecords()));
			this.setRecords(pagedReocrds.getTotalNoOfRecords());
			
			if (this.hdnMode != null && ((hdnMode.equals(WebConstants.ACTION_ADD)) || (hdnMode.equals(WebConstants.ACTION_EDIT)))) {

				OfficersMobileNumbers OfficersMobNums = new OfficersMobileNumbers();
				OfficersMobNums.setOfficerId(hdnId);
				OfficersMobNums.setOfficerName(getContactName());
				OfficersMobNums.setMobNumber(getMoblieNumber());
				OfficersMobNums.setVersion(getVersion());
				OfficersMobNums.setEmailAddress(getContactEmail());
			
						Map<String, String[]> contentMap = new HashMap<String, String[]>();

				if (hdnMode.equals(WebConstants.ACTION_EDIT)) {
					OfficersMobileNumbers originalConfig = ModuleServiceLocator.getReservationBD().getOfficersMobNums(
							OfficersMobNums.getOfficerId());
				createUpdatedDataMapForAudit(contentMap, originalConfig, OfficersMobNums);
				}
				ModuleServiceLocator.getReservationBD().saveOrUpdateOfficersMobileNumbersConfig(OfficersMobNums, contentMap);
				this.setMessageTxt(airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS));
				this.setMsgType(WebConstants.MSG_SUCCESS);
			}
			

		} catch (ModuleException me) {
			this.setMsgType(WebConstants.MSG_ERROR);
			this.setMessageTxt(me.getMessage());
			if (log.isErrorEnabled()) {
				log.error(me.getMessage());
			}
		} catch (Exception e) {
			this.setMsgType(WebConstants.MSG_ERROR);
			this.setMessageTxt(e.getMessage());
			if (log.isErrorEnabled()) {
				log.error(e.getMessage());
			}
		}
		
		
		return S2Constants.Result.SUCCESS;

	}
	
	public int getHdnId() {
		return hdnId;
	}

	public void setHdnId(int hdnId) {
		this.hdnId = hdnId;
	}

	public String getHdnMode() {
		return hdnMode;
	}

	public void setHdnMode(String hdnMode) {
		this.hdnMode = hdnMode;
	}

	private void createUpdatedDataMapForAudit(Map<String, String[]> contentMap,OfficersMobileNumbers originalConfig,
			OfficersMobileNumbers officersMobNums) {
		
			List<String> changedValList = new ArrayList<String>();
			if (!(officersMobNums.getOfficerName().equals(originalConfig.getOfficerName()))) {
				changedValList.add("OfficerName " + originalConfig.getOfficerName() + " modified to " + officersMobNums.getOfficerName());
			}
			if (!(officersMobNums.getMobNumber().equals(originalConfig.getMobNumber()))) {
				changedValList.add("MobileNumber " + originalConfig.getMobNumber() + " modified to "+ officersMobNums.getMobNumber());
			}			
			if(changedValList.size() > 0){
				
				SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
				Date today = new Date();
				changedValList.add("Modified Date " + formatter.format(today));
			}
			contentMap.put("updated_config", changedValList.toArray(new String[changedValList.size()]));

	}

	public String deleteOfficersMobileNumbersConfig() {
		try {
			ModuleServiceLocator.getReservationBD().deleteOfficersMobileNumbersConfig(hdnId);
			this.setMessageTxt(airadminConfig.getMessage(WebConstants.KEY_DELETE_SUCCESS));
			this.setMsgType(WebConstants.MSG_SUCCESS);
		} catch (ModuleException e) {
			this.setMessageTxt(airadminConfig.getMessage(WebConstants.KEY_DELETE_FAIL));
		}
		return S2Constants.Result.SUCCESS;
	}



	private RowDecorator<OfficerMobNumsDTO> decorateRow() {
		RowDecorator<OfficerMobNumsDTO> decorator = new RowDecorator<OfficerMobNumsDTO>() {
			@Override
			public void decorateRow(HashMap<String, Object> row, OfficerMobNumsDTO record) {
	
				try {
					row.put("OfficerMobNumsConfigsResultsTO.OfficerId", record.getOfficerId());
					row.put("OfficerMobNumsConfigsResultsTO.OfficerName", record.getOfficerName());
					row.put("OfficerMobNumsConfigsResultsTO.MobNumber", record.getMobNumber());
					row.put("OfficerMobNumsConfigsResultsTO.emailAddress", record.getContactEmail());
					row.put("OfficerMobNumsConfigsResultsTO.version", record.getVersion());

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		};
		return decorator;
	}
	public int getCurrentPageNo(int startPosition) {
		int currentPageNo = 1;
		if (PAGE_LENGTH != 0) {
			currentPageNo = startPosition / PAGE_LENGTH + 1;
		}
		return currentPageNo;
	}

	public int getTotalNoOfPages(int totoalNoOfRecords) {
		int totalNoOfPages = 1;
		if (PAGE_LENGTH != 0) {
			totalNoOfPages = (int) totoalNoOfRecords / PAGE_LENGTH;
			if (totoalNoOfRecords % PAGE_LENGTH > 0)
				totalNoOfPages++;
		}
		return totalNoOfPages;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public Collection<Map<String, Object>> getColOfficersMobileNumbersConfigs() {
		return colOfficersMobileNumbersConfigs;
	}

	public void setColOfficersMobileNumbersConfigs(
			Collection<Map<String, Object>> colOfficersMobileNumbersConfigs) {
		this.colOfficersMobileNumbersConfigs = colOfficersMobileNumbersConfigs;
	}

	public OfficerMobNumsConfigsResultsTO getOfficerMobNumsConfigsResultsTO() {
		return officerMobNumsConfigsResultsTO;
	}

	public void setOfficerMobNumsConfigsResultsTO(
			OfficerMobNumsConfigsResultsTO officerMobNumsConfigsResultsTO) {
		this.officerMobNumsConfigsResultsTO = officerMobNumsConfigsResultsTO;
	}

	public OfficerMobNumsConfigsSearchDTO getOfficerMobNumsConfigsSearchDTO() {
		return officerMobNumsConfigsSearchDTO;
	}

	public void setOfficerMobNumsConfigsSearchDTO(
			OfficerMobNumsConfigsSearchDTO officerMobNumsConfigsSearchDTO) {
		this.officerMobNumsConfigsSearchDTO = officerMobNumsConfigsSearchDTO;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	/**
	 * @return the contactEmail
	 */
	public String getContactEmail() {
		return contactEmail;
	}

	/**
	 * @param contactEmail the contactEmail to set
	 */
	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getMoblieNumber() {
		return moblieNumber;
	}

	public void setMoblieNumber(String moblieNumber) {
		this.moblieNumber = moblieNumber;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}
	
	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}
}
