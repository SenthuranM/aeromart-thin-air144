package com.isa.thinair.airadmin.core.web.handler.reports;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airproxy.api.utils.DateUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.ResultSet;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class GSTAdditionalReportRH extends BasicRequestHandler {

	private static final Log log = LogFactory.getLog(GSTAdditionalReportRH.class);

	private static final GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	public static String execute(HttpServletRequest request, HttpServletResponse response) {
		String strHdnMode = request.getParameter("hdnMode");

		String forward = AdminStrutsConstants.AdminAction.SUCCESS;
		try {
			setDisplayData(request);
			log.debug("GSTAdditionalReportReportRH success");
		} catch (Exception e) {
			forward = AdminStrutsConstants.AdminAction.ERROR;
			log.error("GSTAdditionalReportReportRHexecute()" + e.getMessage());
		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				log.error("GSTAdditionalReportReportRHReportRH setReportView Success");
				return null;
			} else {
				log.error("GSTAdditionalReportReportRHReportRH setReportView not selected");
			}

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("GSTAdditionalReportReportRHReportRH setReportView Failed " + e.getMessageString());
		}
		return forward;
	}

	private static void setDisplayData(HttpServletRequest request) throws Exception {
		setClientErrors(request);
		setCountriesAndStates(request);
		setLiveStatus(request);
		setReportingPeriod(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);
	}

	private static void setCountriesAndStates(HttpServletRequest request) throws ModuleException {
		String strStatesList = SelectListGenerator.createCountryStateArrayForMultiSelect();
		request.setAttribute(WebConstants.REQ_HTML_COUNTRY_STATE_ARRAY_FOR_MULTI_SELECT, strStatesList);

		String strCountriesHtml = SelectListGenerator.createCountryList();
		request.setAttribute(WebConstants.REQ_HTML_COUNTRY_LIST, strCountriesHtml);
	}

	private static void setLiveStatus(HttpServletRequest request) {
		String liveOrOffline = request.getParameter(WebConstants.REP_HDN_LIVE);
		if (liveOrOffline != null)
			request.setAttribute(WebConstants.REP_SET_LIVE, liveOrOffline);

	}

	private static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	private static void setReportingPeriod(HttpServletRequest request) throws ModuleException {
		int period = new Integer(globalConfig.getBizParam(SystemParamKeys.REPORTING_PERIOD));
		request.setAttribute(WebConstants.REP_RPT_START_DAY, ReportsHTMLGenerator.getReportingPeriod(period));
	}

	private static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		ResultSet resultSet;
		String value = request.getParameter("radReportOption");
		String reportTemplate = "GSTAdditionalReport.jasper";
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String liveOrOffline = request.getParameter(WebConstants.REP_HDN_LIVE);
		try {
			ModuleServiceLocator.getReportingFrameworkBD()
					.storeJasperTemplateRefs("WebReport1", ReportsHTMLGenerator.getReportTemplate(reportTemplate));
			String txtToDate = request.getParameter("txtToDate");
			String txtFromDate = request.getParameter("txtFromDate");
			String countryCode = request.getParameter("selCountry");

			ReportsSearchCriteria search = new ReportsSearchCriteria();
			search.setCountryCode(countryCode);
			search.setStateCode(request.getParameter("hdnStateList"));
			search.setDateRangeTo(txtToDate + " 23:59:59");
			search.setDateRangeFrom(txtFromDate + " 00:00:00");

			if (liveOrOffline != null) {
				if (liveOrOffline.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (liveOrOffline.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}

			resultSet = ModuleServiceLocator.getDataExtractionBD().getGSTAdditionalReportData(search);
			Date currentTime = CalendarUtil.getCurrentZuluDateTime();

			Map<String, Object> parameters = new HashMap<>();
			parameters.put("FROM_DATE", txtFromDate);
			parameters.put("TO_DATE", txtToDate);
			parameters.put("ZULU_PRINT_DATE", DateUtil.formatDate(currentTime, DateUtil.formatDate(currentTime, "dd/MM/yyyy")));
			parameters.put("ZULU_PRINT_TIME", DateUtil.formatDate(currentTime, "HH:mm:ss"));
			parameters.put("COUNTRY", ModuleServiceLocator.getCommonServiceBD().getCountryByName(countryCode).getCountryName());
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);

			String reportNumFormat = request.getParameter("radRptNumFormat");
			ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

			String strLogo = AppSysParamsUtil.getReportLogo(false);
			String reportsRootDir = "../../images/" + AppSysParamsUtil.getReportLogo(true);
			if (value.trim().equals("HTML")) {
				parameters.put("IMG", reportsRootDir);
				ModuleServiceLocator.getReportingFrameworkBD()
						.createHTMLReport("WebReport1", parameters, resultSet, null, null, response);
			} else if (value.trim().equals("PDF")) {
				response.reset();
				response.addHeader("Content-Disposition", "filename=GSTAdditionalReport.pdf");
				reportsRootDir = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG", reportsRootDir);
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("EXCEL")) {
				response.reset();
				response.addHeader("Content-Disposition",
						String.format("attachment;filename=%s" + WebConstants.ReportFormatType.EXCEL_FORMAT,
								"GSTAdditionalReport"));
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("CSV")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=GSTAdditionalReport.csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);
			}
		} catch (Exception e) {
			log.error(e);
		}
	}

}
