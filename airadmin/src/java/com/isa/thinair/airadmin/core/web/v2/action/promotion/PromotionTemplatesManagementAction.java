package com.isa.thinair.airadmin.core.web.v2.action.promotion;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.promotion.api.to.PromotionTemplateTo;
import com.isa.thinair.promotion.api.to.PromotionsSearchTo;

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class PromotionTemplatesManagementAction {

	private static final Log log = LogFactory.getLog(PromotionTemplatesManagementAction.class);

	// in
	private String startDate;
	private String endDate;
	private PromotionTemplateTo promotionTemplateTo;
	private PromotionsSearchTo promotionsSearchTo;

	// out
	private Collection<Map<String, Object>> rows;
	private int page;
	private int total;
	private int records;
	private boolean success;
	private String message;

	public String savePromotionTemplate() {
		try {
			promotionTemplateTo.setPromoStartDate(AiradminUtils.getFormattedDate(startDate));
			promotionTemplateTo.setPromoEndDate(AiradminUtils.getFormattedDate(endDate));
			ServiceResponce resp = ModuleServiceLocator.getPromotionManagementBD().savePromotionTemplate(promotionTemplateTo);
			success = resp.isSuccess();
			message = resp.getResponseCode();
		} catch (Exception e) {
			log.error("savePromotionTemplate ", e);
		}
		return S2Constants.Result.SUCCESS;
	}

	public String updatePromotionTemplate() {

		promotionTemplateTo.setPromoStartDate(AiradminUtils.getFormattedDate(startDate));
		promotionTemplateTo.setPromoEndDate(AiradminUtils.getFormattedDate(endDate));
		try {
			ServiceResponce resp = ModuleServiceLocator.getPromotionManagementBD().updatePromotionTemplate(promotionTemplateTo);
			success = resp.isSuccess();
			message = resp.getResponseCode();
		} catch (Exception e) {
			log.error("updatePromotionTemplate ", e);
		}
		return S2Constants.Result.SUCCESS;
	}

	public String searchPromotionTemplates() {

		Page<PromotionTemplateTo> templatesSearchPage = null;
		int start = (page - 1) * 20;
		Map<String, Object> row;

		try {
			if (promotionsSearchTo != null) {
				promotionsSearchTo.setPromotionStartDate(startDate != null ? AiradminUtils.getFormattedDate(startDate) : null);
				promotionsSearchTo.setPromotionEndDate(endDate != null ? AiradminUtils.getFormattedDate(endDate) : null);
				templatesSearchPage = ModuleServiceLocator.getPromotionManagementBD().searchPromotionTemplates(
						promotionsSearchTo, start, 20);

				this.records = templatesSearchPage.getTotalNoOfRecords();
				this.total = templatesSearchPage.getTotalNoOfRecords() / 20;
				int mod = templatesSearchPage.getTotalNoOfRecords() % 20;
				if (mod > 0) {
					this.total = this.total + 1;
				}

				Collection<PromotionTemplateTo> templates = templatesSearchPage.getPageData();
				rows = new ArrayList<Map<String, Object>>();
				int a = 1;
				for (PromotionTemplateTo template : templates) {
					row = new HashMap<String, Object>();
					row.put("id", ((page - 1) * 20) + a++);
					row.put("template", template);
					rows.add(row);
				}
			}
		} catch (Exception e) {
			log.error("searchPromotionTemplates ", e);
		}

		return S2Constants.Result.SUCCESS;
	}

	public PromotionTemplateTo getPromotionTemplateTo() {
		return promotionTemplateTo;
	}

	public void setPromotionTemplateTo(PromotionTemplateTo promotionTemplateTo) {
		this.promotionTemplateTo = promotionTemplateTo;
	}

	public PromotionsSearchTo getPromotionsSearchTo() {
		return promotionsSearchTo;
	}

	public void setPromotionsSearchTo(PromotionsSearchTo promotionsSearchTo) {
		this.promotionsSearchTo = promotionsSearchTo;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public Collection<Map<String, Object>> getRows() {
		return rows;
	}

	public int getTotal() {
		return total;
	}

	public boolean getSuccess() {
		return success;
	}

	public String getMessage() {
		return message;
	}

}
