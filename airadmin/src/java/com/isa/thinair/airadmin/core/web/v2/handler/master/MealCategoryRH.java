package com.isa.thinair.airadmin.core.web.v2.handler.master;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airmaster.api.dto.MealCategoryDTO;
import com.isa.thinair.airmaster.api.model.MealCategory;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * 
 * @author sanjaya
 * 
 */
public class MealCategoryRH {

	private static Log log = LogFactory.getLog(MealCategoryRH.class);

	public static Page searchMealCategory(int pageNo, List<ModuleCriterion> criterion) {
		// call back end

		Page page = null;
		try {
			if (criterion == null) {
				page = ModuleServiceLocator.getCommonServiceBD().getMealCategories((pageNo - 1) * 20, 20);
			} else {
				if (pageNo == 0) {
					pageNo = 1;
				}
				page = ModuleServiceLocator.getCommonServiceBD().getMealCategories((pageNo - 1) * 20, 20, criterion);
			}
		} catch (ModuleException e) {
			log.error(e);
		}
		return page;
	}

	public static boolean saveMealCategory(MealCategoryDTO mealCatDTO) throws ModuleException {

		MealCategory mealCategory = new MealCategory();

		mealCategory.setVersion(mealCatDTO.getVersion());
		mealCategory.setCreatedBy(mealCatDTO.getCreatedBy());
		mealCategory.setCreatedDate(mealCatDTO.getCreatedDate());

		mealCategory.setMealCategoryName(mealCatDTO.getMealCategoryName());
		mealCategory.setMealCategoryId(mealCatDTO.getMealCategoryId());

		Integer mealCatId = ModuleServiceLocator.getCommonServiceBD().saveMealCategory(mealCategory);
		mealCatDTO.setMealCategoryId(mealCatId);

		ModuleServiceLocator.getCommonServiceBD().saveMealCatLanTranslation(mealCatDTO);

		return true;
	}

	public static boolean deleteMealCategory(Integer mealCategoryId) throws ModuleException {
		ModuleServiceLocator.getCommonServiceBD().deleteMealCategory(mealCategoryId);
		return true;
	}

	public static MealCategory getMealCategory(String mealCategoryName) throws ModuleException {
		MealCategory mealCategory = (MealCategory) ModuleServiceLocator.getCommonServiceBD().getMealCategory(mealCategoryName);
		return mealCategory;
	}

}
