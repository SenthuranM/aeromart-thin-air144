package com.isa.thinair.airadmin.core.web.v2.generator.master;

import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airmaster.api.model.SSR;
import com.isa.thinair.airmaster.api.model.SSRCategory;
import com.isa.thinair.commons.api.exception.ModuleException;

public class SSRMessageHG {

	private static String clientErrors;
	private static String chargesClientErrors;
	private static String templateClientErrors;
	private static String airportTransferClientErrors;
	private static String airportTransferTemplateClientErrors;

	/**
	 * Create Client Validations for SSR Info & SSR Charges Admin UI
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array of Client validations
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getSSRInfoClientErrors(HttpServletRequest request) throws ModuleException {

		if (clientErrors == null) {
			Properties moduleErrs = new Properties();
			moduleErrs.setProperty("um.airadmin.delete.confirmation", "deleteRecoredCfrm");

			moduleErrs.setProperty("um.ssr.form.code.required", "ssrCodeReq"); // SSR Code required
			moduleErrs.setProperty("um.ssr.form.desc.required", "ssrDescReq"); // SSR Description required
			moduleErrs.setProperty("um.ssr.form.category.required", "ssrCatReq"); // SSR Category is Required
			moduleErrs.setProperty("um.ssr.form.id.already.exist", "ssrCodeExst"); // SSR Code already exists
			moduleErrs.setProperty("um.ssr.form.code.four.char.required", "ssrLenErr");// SSR Code should have 4
																						// characters
			moduleErrs.setProperty("um.ssr.form.code.cat.service.required", "servCatReq");// Select a Service Category
			moduleErrs.setProperty("um.ssr.form.code.subcat.service.required", "servSubCatReq"); // Select a Service
			moduleErrs.setProperty("um.ssr.form.code.airport.list.empty", "airportListReq"); // Applicable Airport List
																								// is Empty
			moduleErrs.setProperty("um.ssr.form.code.ond.list.empty", "ondListReq");// Applicable OnD list is empty
			moduleErrs.setProperty("um.ssr.form.code.trans.max.invalid", "maxTransTimeErr");// Transit Max time is
																							// invalid
			moduleErrs.setProperty("um.ssr.form.code.trans.min.invalid", "minTransTimeErr");// Transit Min time is
																							// invalid
			moduleErrs.setProperty("um.ssr.form.code.trans.time.invalid", "transTimeErr");// Transit Max time should
																							// greater than Min time
			moduleErrs.setProperty("um.ssr.form.name.required", "ssrNameReq");
			moduleErrs.setProperty("um.ssr.form.only.airport.service.enabled", "onlyHalaSerEnabled");

			moduleErrs.setProperty("um.ssr.form.code.depature.required", "depatureRqrd");
			moduleErrs.setProperty("um.ssr.form.code.arrival.required", "arrivalRqrd");
			moduleErrs.setProperty("um.ssr.form.code.arrivalDepature.same", "depatureArriavlSame");
			moduleErrs.setProperty("um.ssr.form.code.arrivalVia.same", "arriavlViaSame");
			moduleErrs.setProperty("um.ssr.form.code.depatureVia.same", "depatureViaSame");
			moduleErrs.setProperty("um.ssr.form.code.via.sequence", "vianotinSequence");
			moduleErrs.setProperty("um.ssr.form.code.via.equal", "viaEqual");
			moduleErrs.setProperty("um.ssr.form.code.departure.inactive", "departureInactive");
			moduleErrs.setProperty("um.ssr.form.code.arrival.inactive", "arrivalInactive");
			moduleErrs.setProperty("um.ssr.form.code.via.inactine", "viaInactive");
			moduleErrs.setProperty("um.ssr.form.code.ond.required", "ondRequired");

			moduleErrs.setProperty("um.ssr.form.code.max.qty.required", "maxQtyRequired");
			moduleErrs.setProperty("um.ssr.form.code.invalid.max.qty", "invalidMaxQty");

			moduleErrs.setProperty("um.ssr.form.name.required", "ssrNameReq");
			moduleErrs.setProperty("um.ssr.form.only.airport.service.enabled", "onlyHalaSerEnabled");
			moduleErrs.setProperty("um.ssr.form.ssr.cutoff.invalid" , "invalidTimeFormat");

			clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}

		return clientErrors;
	}

	public static String getSSRChargesClientErrors(HttpServletRequest request) throws ModuleException {

		if (chargesClientErrors == null) {
			Properties moduleErrs = new Properties();
			moduleErrs.setProperty("um.airadmin.delete.confirmation", "deleteRecoredCfrm");

			moduleErrs.setProperty("um.ssrcharge.form.code.required", "chargCodeReq"); // Charge Code required
			moduleErrs.setProperty("um.ssrcharge.form.id.already.exist", "chargCodeExst"); // Charge Code already exists
			moduleErrs.setProperty("um.ssrcharge.form.desc.required", "chargDescReq"); // Charge Description required
			moduleErrs.setProperty("um.ssrcharge.form.ssrcode.required", "ssrCodeReq"); // Select a SSR Code
			moduleErrs.setProperty("um.ssrcharge.form.adu.amt.invalid", "adultAmtErr");// Adult Amount is invalid
			moduleErrs.setProperty("um.ssrcharge.form.chi.amt.invalid", "childAmtErr");// Child Amount is invalid
			moduleErrs.setProperty("um.ssrcharge.form.inf.amt.invalid", "infantAmtErr");// Infant Amount is invalid
			moduleErrs.setProperty("um.ssrcharge.form.res.amt.invalid", "reserAmtErr");// Reservation Amount is invalid
			moduleErrs.setProperty("um.ssrcharge.form.service.amt.invalid", "serChgAmtErr");// in-flight Service's
																							// charge is inavlid
			moduleErrs.setProperty("um.ssrcharge.form.fromdate.required", "effFrmDateReq"); // Effective From date is
																							// Empty
			moduleErrs.setProperty("um.ssrcharge.form.todate.required", "effToDateReq"); // Effective To date is
																							// Empty
			moduleErrs.setProperty("um.ssrcharge.form.effecfrm.date.invalid", "effFrmDateErr"); // Effective From date
																								// is invalid
			moduleErrs.setProperty("um.ssrcharge.form.effecto.date.invalid", "effToDateErr"); // Effective To date is
																								// invalid
			moduleErrs.setProperty("um.ssrcharge.form.effecfrm.daterange.invalid", "effDateRangErr"); // Effective Date
																										// range invalid

			moduleErrs.setProperty("um.ssrcharge.form.ssr.or.charge.code.exit", "ssrChargeCodeExst"); // Effective Date

			moduleErrs.setProperty("um.ssrcharge.form.cabin.class.required", "cabinClassReq"); // Effective Date
			moduleErrs.setProperty("um.ssrcharge.form.localcurrencycode.required", "localCurrencyCodeRqrd");
			chargesClientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}

		return chargesClientErrors;
	}

	public static String getSSRTemplateClientErrors(HttpServletRequest request) throws ModuleException {
		if (templateClientErrors == null) {
			Properties moduleErrs = new Properties();
			moduleErrs.setProperty("um.airadmin.delete.confirmation", "deleteRecoredCfrm");
			moduleErrs.setProperty("um.ssrtemplate.form.code.required", "templateCodeReq"); // Template Code required
			moduleErrs.setProperty("um.ssrtemplate.form.desc.required", "templateDescReq"); // Template Description
																							// required
			moduleErrs.setProperty("um.ssrtemplate.form.cc.required", "ccRqrd"); // Cabin class required
			moduleErrs.setProperty("um.ssrtemplate.form.ssr.required", "ssrRqrd"); // SSR required
			moduleErrs.setProperty("um.ssrtemplate.form.qty.required", "qtyRqrd"); // Max Qty required
			moduleErrs.setProperty("um.ssrtemplate.form.qty.invalid", "qtyInvalid"); // Max Qty required
			moduleErrs.setProperty("um.ssrtemplate.form.status.required", "statusRequired"); // status required
			moduleErrs.setProperty("um.ssrtemplate.form.ssr.exist", "ssrExst"); // SSR Already exists

			templateClientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}

		return templateClientErrors;
	}

	public static String setSSROptHTML() throws ModuleException {
		StringBuilder sb = new StringBuilder();
		List<SSR> lstSSR = ModuleServiceLocator.getSsrServiceBD().getActiveSSRs(null, null, SSRCategory.ID_INFLIGHT_SERVICE);
		if (lstSSR != null && lstSSR.size() > 0) {
			for (SSR ssr : lstSSR) {
				sb.append("<option value='");
				sb.append(ssr.getSsrId());
				sb.append(",");
				sb.append(ssr.getSsrName());
				sb.append("'>");
				sb.append(ssr.getSsrName());
				sb.append("</option>");
			}
		}

		return sb.toString();
	}

	public static void setAirportTransferClientErrors(HttpServletRequest request) throws ModuleException {
		if (airportTransferClientErrors == null) {
			Properties moduleErrs = new Properties();
			moduleErrs.setProperty("um.airadmin.delete.confirmation", "deleteRecoredCfrm");
			// TODO SET UP required error messages
			airportTransferClientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}
		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, chargesClientErrors);

	}

	public static void setAirportTransferTemplateClientErrors(HttpServletRequest request) throws ModuleException {
		if (airportTransferTemplateClientErrors == null) {
			Properties moduleErrs = new Properties();
			moduleErrs.setProperty("um.airadmin.delete.confirmation", "deleteRecoredCfrm");
			// TODO SET UP required error messages
			airportTransferTemplateClientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}
		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, airportTransferTemplateClientErrors);

	}
}
