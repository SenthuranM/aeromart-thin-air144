package com.isa.thinair.airadmin.core.util.pricing;

import java.util.Collection;
import java.util.List;

import com.isa.thinair.airadmin.api.FareOverwriteInfoTO;
import com.isa.thinair.airadmin.api.FareRuleCommentDTO;
import com.isa.thinair.airadmin.api.FareRuleDTO;
import com.isa.thinair.airpricing.api.model.FareRule;
import com.isa.thinair.airpricing.api.model.FareRuleComment;
import com.isa.thinair.airpricing.api.model.FareRuleFee;
import com.isa.thinair.commons.core.util.Pair;
import com.isa.thinair.platform.api.util.PlatformUtiltiies;

public class FareUtil {
	private static final String STR_SEARCH_ALL = "All";

	public static final String ACTION_ADD = "add";
	public static final String ACTION_EDIT = "edit";
	public static final String ACTION_DELETE = "delete";

	public static void populateFareOverWriteInfo(Collection<FareRuleFee> fareFees, List<FareOverwriteInfoTO> fareOverwriteInfo,
			String compareAgainst, String timeType, String chargeType) {

		boolean isSearchCriteriaExist = (PlatformUtiltiies.nullHandler(compareAgainst).length() > 0
				&& PlatformUtiltiies.nullHandler(timeType).length() > 0 && PlatformUtiltiies.nullHandler(chargeType).length() > 0);

		for (FareRuleFee fareFee : fareFees) {
			FareOverwriteInfoTO overwriteInfoTO = new FareOverwriteInfoTO();

			overwriteInfoTO.setChargeAmount(fareFee.getChargeAmount());
			overwriteInfoTO.setChargeAmountInLocal(fareFee.getChargeAmountInLocal());
			overwriteInfoTO.setChargeAmountType(fareFee.getChargeAmountType());
			overwriteInfoTO.setChargeType(fareFee.getChargeType());
			overwriteInfoTO.setCompareAgainst(fareFee.getCompareAgainst());
			overwriteInfoTO.setId(fareFee.getFareRuleFeeId());
			overwriteInfoTO.setMax(fareFee.getMaximumPerChargeAmt());
			overwriteInfoTO.setMin(fareFee.getMinimumPerChargeAmt());
			overwriteInfoTO.setStatus((PlatformUtiltiies.nullHandler(fareFee.getStatus()).equals("ACT")) ? "Y" : "N");
			overwriteInfoTO.setTimeType(fareFee.getTimeType());
			overwriteInfoTO.setTimeValue(fareFee.getTimeValue());
			overwriteInfoTO.setCompareAgainstText(getCompareAgainstTextForFareFee(fareFee.getCompareAgainst()));
			overwriteInfoTO.setTimeTypeText(getTimeTypeTextForMasterFee(fareFee.getTimeType()));
			overwriteInfoTO.setStatusTxt((PlatformUtiltiies.nullHandler(fareFee.getStatus())));

			populateResultWithSearchCriteria(overwriteInfoTO, fareOverwriteInfo, isSearchCriteriaExist, compareAgainst, timeType,
					chargeType);
		}
	}

	public static FareRuleFee composeFareRuleFee(FareOverwriteInfoTO infoTO) {
		FareRuleFee fee = new FareRuleFee();

		fee.setChargeAmount(infoTO.getChargeAmount());
		fee.setChargeAmountInLocal(infoTO.getChargeAmountInLocal());
		fee.setChargeAmountType(infoTO.getChargeAmountType());
		fee.setChargeType(infoTO.getChargeType());
		fee.setCompareAgainst(infoTO.getCompareAgainst());
		fee.setMaximumPerChargeAmt(infoTO.getMax());
		fee.setMinimumPerChargeAmt(infoTO.getMin());
		fee.setStatus((PlatformUtiltiies.nullHandler(infoTO.getStatus()).equals("Y")) ? "ACT" : "INA");
		fee.setTimeType(infoTO.getTimeType());
		fee.setTimeValue(infoTO.getTimeValue());

		return fee;
	}

	public static FareRuleFee getFareRuleFee(Collection<FareRuleFee> fees, Long id) {
		FareRuleFee ret = null;
		for (FareRuleFee fee : fees) {
			if (fee.getFareRuleFeeId().equals(id)) {
				ret = fee;
				break;
			}
		}
		return ret;
	}


	public static void updateFareRuleFeeFields(FareRuleFee fareRuleFee, FareOverwriteInfoTO fee) {
		fareRuleFee.setChargeAmount(fee.getChargeAmount());
		fareRuleFee.setChargeAmountInLocal(fee.getChargeAmountInLocal());
		fareRuleFee.setChargeAmountType(fee.getChargeAmountType());
		fareRuleFee.setChargeType(fee.getChargeType());
		fareRuleFee.setCompareAgainst(fee.getCompareAgainst());
		fareRuleFee.setMaximumPerChargeAmt(fee.getMax());
		fareRuleFee.setMinimumPerChargeAmt(fee.getMin());
		fareRuleFee.setStatus((PlatformUtiltiies.nullHandler(fee.getStatus()).equals("Y")) ? "ACT" : "INA");
		fareRuleFee.setTimeType(fee.getTimeType());
		fareRuleFee.setTimeValue(fee.getTimeValue());
	}

	private static String getCompareAgainstTextForFareFee(String compareAgainst) {
		String compareAgainstTxt = "";
		if (compareAgainst.equals(FareRuleFee.COMPARE_AGAINTS.AFTER_DEPARTURE)) {
			compareAgainstTxt = FareOverwriteInfoTO.TXT_CA_AFTER_DEPARTURE;
		} else if (compareAgainst.equals(FareRuleFee.COMPARE_AGAINTS.BEFORE_DEPARTURE)) {
			compareAgainstTxt = FareOverwriteInfoTO.TXT_BEFORE_DEPARTURE;
		}
		return compareAgainstTxt;
	}

	private static String getTimeTypeTextForMasterFee(String timeType) {
		String timeTypeTxt = "";
		if (timeType.equals(FareRuleFee.TIME_TYPE.DAYS)) {
			timeTypeTxt = FareOverwriteInfoTO.TXT_TT_DAY;
		} else if (timeType.equals(FareRuleFee.TIME_TYPE.HOURS)) {
			timeTypeTxt = FareOverwriteInfoTO.TXT_TT_HRS;
		} else if (timeType.equals(FareRuleFee.TIME_TYPE.MINUTES)) {
			timeTypeTxt = FareOverwriteInfoTO.TXT_TT_MIN;
		} else if (timeType.equals(FareRuleFee.TIME_TYPE.MONTH)) {
			timeTypeTxt = FareOverwriteInfoTO.TXT_TT_MON;
		} else if (timeType.equals(FareRuleFee.TIME_TYPE.WEEK)) {
			timeTypeTxt = FareOverwriteInfoTO.TXT_TT_WEK;
		}
		return timeTypeTxt;
	}

	private static void populateResultWithSearchCriteria(FareOverwriteInfoTO overwriteInfoTO,
			List<FareOverwriteInfoTO> fareOverwriteInfo, boolean isSearchCriteriaExist, String compareAgainst, String timeType,
			String chargeType) {
		// we do filtering here as there is not having large set of data
		if (isSearchCriteriaExist) {
			boolean isSearchWithChargeType = !chargeType.equalsIgnoreCase(STR_SEARCH_ALL);
			boolean isSearchWithCompareAgainst = !compareAgainst.equalsIgnoreCase(STR_SEARCH_ALL);
			boolean isSearchWithTimeType = !timeType.equalsIgnoreCase(STR_SEARCH_ALL);

			if (isSearchWithChargeType && isSearchWithCompareAgainst && isSearchWithTimeType) {
				// 1
				if (overwriteInfoTO.getChargeType().equals(chargeType)
						&& overwriteInfoTO.getCompareAgainst().equals(compareAgainst)
						&& overwriteInfoTO.getTimeType().equals(timeType)) {
					fareOverwriteInfo.add(overwriteInfoTO);
				} else {
					return;
				}
			} else if (isSearchWithChargeType && !isSearchWithCompareAgainst && isSearchWithTimeType) {
				// 2
				if (overwriteInfoTO.getChargeType().equals(chargeType) && overwriteInfoTO.getTimeType().equals(timeType)) {
					fareOverwriteInfo.add(overwriteInfoTO);
				} else {
					return;
				}
			} else if (isSearchWithChargeType && !isSearchWithCompareAgainst && !isSearchWithTimeType) {
				// 3
				if (overwriteInfoTO.getChargeType().equals(chargeType)) {
					fareOverwriteInfo.add(overwriteInfoTO);
				} else {
					return;
				}
			} else if (isSearchWithChargeType && isSearchWithCompareAgainst && !isSearchWithTimeType) {
				// 4
				if (overwriteInfoTO.getChargeType().equals(chargeType)
						&& overwriteInfoTO.getCompareAgainst().equals(compareAgainst)) {
					fareOverwriteInfo.add(overwriteInfoTO);
				} else {
					return;
				}
			} else if (!isSearchWithChargeType && isSearchWithCompareAgainst && !isSearchWithTimeType) {
				// 5
				if (overwriteInfoTO.getCompareAgainst().equals(compareAgainst)) {
					fareOverwriteInfo.add(overwriteInfoTO);
				} else {
					return;
				}
			} else if (!isSearchWithChargeType && isSearchWithCompareAgainst && isSearchWithTimeType) {
				// 6
				if (overwriteInfoTO.getCompareAgainst().equals(compareAgainst) && overwriteInfoTO.getTimeType().equals(timeType)) {
					fareOverwriteInfo.add(overwriteInfoTO);
				} else {
					return;
				}
			} else if (!isSearchWithChargeType && !isSearchWithCompareAgainst && isSearchWithTimeType) {
				// 7
				if (overwriteInfoTO.getTimeType().equals(timeType)) {
					fareOverwriteInfo.add(overwriteInfoTO);
				} else {
					return;
				}
			} else {
				// 8
				fareOverwriteInfo.add(overwriteInfoTO);
			}
		} else {
			fareOverwriteInfo.add(overwriteInfoTO);
		}

	}

	public static Pair<Boolean, String> validateFareFees(Collection<FareRuleFee> existingFees, FareRuleFee updatingFee,
			String actionMode) {

		Pair<Boolean, String> ret = Pair.of(true, "SUCCESS");

		if (!actionMode.equals(ACTION_DELETE)) {
			if (updatingFee.getTimeType().equals(FareRuleFee.TIME_TYPE.MINUTES)
					&& Long.parseLong(updatingFee.getTimeValue()) >= 60) {
				// minutes value must not be greater than 60
				return Pair.of(false, "Invalid value for time value as minutes.");
			} else if (updatingFee.getTimeType().equals(FareRuleFee.TIME_TYPE.HOURS)
					&& Long.parseLong(updatingFee.getTimeValue()) >= 24) {
				// hours value must not be greater than 24
				return Pair.of(false, "Invalid value for time value as hours.");
			} else if (existingFees != null && existingFees.size() > 0) {

				String updatingChargeType = updatingFee.getChargeType();
				String updatingTimeType = updatingFee.getTimeType();
				String updatingTimeValue = updatingFee.getTimeValue();
				String updatingCompareAgainst = updatingFee.getCompareAgainst();
				boolean isDuplicatesfound = false;
				boolean isOverlappingTimesFound = false;

				for (FareRuleFee existingFee : existingFees) {
					if (existingFee.getChargeType().equals(updatingChargeType)
							&& existingFee.getTimeType().equals(updatingTimeType)
							&& existingFee.getTimeValue().equals(updatingTimeValue)
							&& existingFee.getCompareAgainst().equals(updatingCompareAgainst)) {
						if (!actionMode.equals(ACTION_EDIT)) {
							isDuplicatesfound = true;
							ret = Pair
									.of(false,
											"Duplicate records with same time values and types are not allowed for the same charge type.");
							break;
						} else if (!(existingFee.getFareRuleFeeId() == updatingFee.getFareRuleFeeId())) {
							isDuplicatesfound = true;
							ret = Pair
									.of(false,
											"Duplicate records with same time values and types are not allowed for the same charge type.");
							break;
						}
					} else {
						if (existingFee.getChargeType().equals(updatingChargeType)
								&& existingFee.getCompareAgainst().equals(updatingCompareAgainst)
								&& existingFee.getTimeType().equals(FareRuleFee.TIME_TYPE.DAYS)
								&& updatingTimeType.equals(FareRuleFee.TIME_TYPE.WEEK)) {
							if (!actionMode.equals(ACTION_EDIT)) {
								if (Long.parseLong(updatingTimeValue) * 7 == Long.parseLong(existingFee.getTimeValue())) {
									isOverlappingTimesFound = true;
									ret = Pair.of(false, "Records overlapping with the existing time values of fee id "
											+ existingFee.getFareRuleFeeId());
									break;
								}
							} else if (!(existingFee.getFareRuleFeeId() == updatingFee.getFareRuleFeeId())) {
								if (Long.parseLong(updatingTimeValue) * 7 == Long.parseLong(existingFee.getTimeValue())) {
									isOverlappingTimesFound = true;
									ret = Pair.of(false, "Records overlapping with the existing time values of fee id "
											+ existingFee.getFareRuleFeeId());
									break;
								}
							}
						}
						if (existingFee.getChargeType().equals(updatingChargeType)
								&& existingFee.getCompareAgainst().equals(updatingCompareAgainst)
								&& existingFee.getTimeType().equals(FareRuleFee.TIME_TYPE.WEEK)
								&& updatingTimeType.equals(FareRuleFee.TIME_TYPE.DAYS)) {
							if (!actionMode.equals(ACTION_EDIT)) {
								if (Long.parseLong(existingFee.getTimeValue()) * 7 == Long.parseLong(updatingTimeValue)) {
									isOverlappingTimesFound = true;
									ret = Pair.of(false, "Records overlapping with the existing time values of fee id "
											+ existingFee.getFareRuleFeeId());
									break;
								}
							} else if (!(existingFee.getFareRuleFeeId() == updatingFee.getFareRuleFeeId())) {
								if (Long.parseLong(updatingTimeValue) == Long.parseLong(existingFee.getTimeValue()) * 7) {
									isOverlappingTimesFound = true;
									ret = Pair.of(false, "Records overlapping with the existing time values of fee id "
											+ existingFee.getFareRuleFeeId());
									break;
								}
							}
						}
					}
				}

				if (isDuplicatesfound || isOverlappingTimesFound) {
					return ret;
				}
			}
		}

		return ret;
	}

}
