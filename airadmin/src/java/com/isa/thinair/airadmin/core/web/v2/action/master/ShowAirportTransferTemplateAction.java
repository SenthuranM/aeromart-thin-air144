package com.isa.thinair.airadmin.core.web.v2.action.master;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airadmin.api.AirportTransferTemplateDTO;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.v2.action.common.CommonAdminRequestResponseAwareCommonAction;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airmaster.api.dto.AirportTransferDTO;
import com.isa.thinair.airmaster.api.model.AirportTransferTemplate;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.webplatform.core.util.DataUtil;
import com.isa.thinair.webplatform.core.util.RowDecorator;
import com.isa.thinair.airadmin.core.web.generator.airportTransfer.AirportTransferHTMLGenerator;
import com.isa.thinair.airmaster.api.model.AirportTransferTemplateBC;

import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

/**
 * @author Manoj Dhanushka
 */
@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ShowAirportTransferTemplateAction extends CommonAdminRequestResponseAwareCommonAction {

	private static final Log log = LogFactory.getLog(ShowAirportTransferTemplateAction.class);

	private static final int PAGE_LENGTH = 10;

	private String searchCode;
	private String flightNo;
	private String cabinClassCode;
	private String aircraftModel;
	private String status;
	private int page;
	private int total;
	private int records;
	private Collection<Map<String, Object>> rows;
	private AirportTransferTemplateDTO transferTemplate = null;
	private String bookingClasses;

	public String save() {
		try {
			
			if(transferTemplate.getAirportTransferTemplateId() == null){
				ModuleServiceLocator.getAirportTransferServiceBD().saveAirportTransferTemplate(createTemplate());
			}else{
				ModuleServiceLocator.getAirportTransferServiceBD().updateAirportTransferTemplate(createTemplate());
			}
			
			setDefaultSuccessMessage();
		} catch (Exception e) {
			this.handleException(e);
			log.info("Error in saving airport transfers", e);
		}

		return S2Constants.Result.SUCCESS;
	}

	private AirportTransferTemplate createTemplate() {
		AirportTransferTemplate transferTem = new AirportTransferTemplate();
		transferTem.setAirportTransferTemplateId(transferTemplate.getAirportTransferTemplateId());
		transferTem.setOndCode(transferTemplate.getOndCode());
		transferTem.setCabinClassCode(transferTemplate.getCabinClassCode());
		transferTem.setStatus(transferTemplate.getStatus());
		transferTem.setFlightNumber(transferTemplate.getFlightNumber());
		transferTem.setModelNumber(transferTemplate.getModelNumber());
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		if (transferTemplate.getDepartureDate() != null && transferTemplate.getDepartureDate().length() > 0) {
			try {
				transferTem.setDepartureDate(formatter.parse(transferTemplate.getDepartureDate()));
			} catch (Exception e) {
				// do nothing
			}
		}
		if (transferTemplate.getAdultAmount() != null) {
			transferTem.setAdultAmount(transferTemplate.getAdultAmount());
		} else {
			transferTem.setAdultAmount(BigDecimal.ZERO);
		}
		if (transferTemplate.getChildAmount() != null) {
			transferTem.setChildAmount(transferTemplate.getChildAmount());
		} else {
			transferTem.setChildAmount(BigDecimal.ZERO);
		}
		if (transferTemplate.getInfantAmount() != null) {
			transferTem.setInfantAmount(transferTemplate.getInfantAmount());
		} else {
			transferTem.setInfantAmount(BigDecimal.ZERO);
		}
		
		if(!transferTemplate.getBookingClasses().isEmpty()){
			String[] bookingClassCodes = transferTemplate.getBookingClasses().split(",");
			
			Map<String,Integer> airportTransferTemplateBCs = null;
			
			if(transferTemplate.getAirportTransferTemplateId() != null){
				airportTransferTemplateBCs = ModuleServiceLocator.getAirportTransferServiceBD().getAirportTransferBookingClasses(
																							transferTemplate.getAirportTransferTemplateId());
			}
			
			Set<AirportTransferTemplateBC> airportTransTempBCs = new HashSet<AirportTransferTemplateBC>();
			int i = 0;
			for(String bookingClassCode : bookingClassCodes){
				AirportTransferTemplateBC airportTransTempBC = new AirportTransferTemplateBC();
				airportTransTempBC.setAirportTransferTemplate(transferTem);
				airportTransTempBC.setBookingClassCode(bookingClassCode);
				if(airportTransferTemplateBCs != null && !airportTransferTemplateBCs.isEmpty()){
					airportTransTempBC.setAirportTransferTempBcId(airportTransferTemplateBCs.get(bookingClassCode));
				}
				airportTransTempBCs.add(airportTransTempBC);
			}
			
			transferTem.setAirportTransferTempBookingClasses(airportTransTempBCs);
		}
		
		
		return transferTem;
	}

	public String search() {
		try {
			init();
			int startIndex = (page - 1) * PAGE_LENGTH;
			AirportTransferDTO airportTransferDTO = new AirportTransferDTO();
			if (cabinClassCode != null && !cabinClassCode.isEmpty()) {
				airportTransferDTO.setCabinClassCode(cabinClassCode);
			}
			if (flightNo != null && !flightNo.isEmpty()) {
				airportTransferDTO.setFlightNo(flightNo);
			}
			if (aircraftModel != null && !aircraftModel.isEmpty()) {
				airportTransferDTO.setAircraftModel(aircraftModel);
			}
			if (status != null && !status.isEmpty()) {
				airportTransferDTO.setStatus(status);
			}
			Page<AirportTransferTemplate> tranferTemplates = ModuleServiceLocator.getAirportTransferServiceBD()
					.getAirportTransferTemplates(startIndex, PAGE_LENGTH, airportTransferDTO);
			bindData(tranferTemplates);
		} catch (Exception e) {
			log.info("Error in searching airport transfer Templates", e);
		}
		return S2Constants.Result.SUCCESS;
	}

	private void bindData(Page<AirportTransferTemplate> tranferTemplates) {
		if (tranferTemplates != null) {
			this.page = getCurrentPageNo(tranferTemplates.getStartPosition());
			this.total = getTotalNoOfPages(tranferTemplates.getTotalNoOfRecords());
			this.records = tranferTemplates.getTotalNoOfRecords();

			this.rows = (Collection<Map<String, Object>>) DataUtil.createGridDataDecorateOnly(
					tranferTemplates.getStartPosition(), tranferTemplates.getPageData(), decorateRow());
		}
	}

	private int getCurrentPageNo(int startPosition) {
		int currentPageNo = 1;
		if (PAGE_LENGTH != 0) {
			currentPageNo = startPosition / PAGE_LENGTH + 1;
		}
		return currentPageNo;
	}

	private int getTotalNoOfPages(int totoalNoOfRecords) {
		int totalNoOfPages = 1;
		if (PAGE_LENGTH != 0) {
			totalNoOfPages = (int) totoalNoOfRecords / PAGE_LENGTH;
			if (totoalNoOfRecords % PAGE_LENGTH > 0)
				totalNoOfPages++;
		}
		return totalNoOfPages;
	}

	private RowDecorator<AirportTransferTemplate> decorateRow() {
		RowDecorator<AirportTransferTemplate> row = new RowDecorator<AirportTransferTemplate>() {
			@Override
			public void decorateRow(HashMap<String, Object> row, AirportTransferTemplate record) {
				row.put("transferTemplate.ondCode", record.getOndCode());
				row.put("transferTemplate.cabinClassCode", record.getCabinClassCode());
				row.put("transferTemplate.flightNumber", record.getFlightNumber());
				if (record.getDepartureDate() != null) {
					SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
					row.put("transferTemplate.departureDate", formatter.format(record.getDepartureDate()));
				}
				row.put("transferTemplate.modelNumber", record.getModelNumber());
				row.put("transferTemplate.adultAmount", record.getAdultAmount());
				row.put("transferTemplate.childAmount", record.getChildAmount());
				row.put("transferTemplate.infantAmount", record.getInfantAmount());
				row.put("transferTemplate.airportTransferTemplateId", record.getAirportTransferTemplateId());
				row.put("transferTemplate.status", record.getStatus());
				
				String bookingClasses = "";
				String ids = "";
				
				if(record.getAirportTransferTempBookingClasses() != null && record.getAirportTransferTempBookingClasses().size() != 0){
					for(AirportTransferTemplateBC airportTransferTemplateBC : record.getAirportTransferTempBookingClasses()){
						bookingClasses += airportTransferTemplateBC.getBookingClassCode() + ",";
						ids += airportTransferTemplateBC.getAirportTransferTempBcId() + ",";
					}
					bookingClasses = bookingClasses.substring(0, bookingClasses.length() - 1);
					ids = ids.substring(0, ids.length() - 1);
				}
				
				row.put("transferTemplate.bookingClasses", bookingClasses);
				row.put("transferTemplate.bookingClassesIds", ids);
			}
		};
		return row;
	}

	private void init() {
		if (searchCode == null || searchCode.isEmpty()) {
			searchCode = null;
		}
		if (page < 1) {
			page = 1;
		}
	}
	
	public String loadBookingClassesAPTTemplate(){
		
		try{
			bookingClasses = AirportTransferHTMLGenerator.createBookingClassesHtml(cabinClassCode, false);
		}catch(Exception e){
			this.handleException(e);
			log.info("Error in retriving booking classes", e);
		}
		
		return S2Constants.Result.SUCCESS;
	}

	@Override
	public String getMessage() {
		return message;
	}

	@Override
	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String getMsgType() {
		return msgType;
	}

	@Override
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public AirportTransferTemplateDTO getTransferTemplate() {
		return transferTemplate;
	}

	public void setTransferTemplate(AirportTransferTemplateDTO transferTemplate) {
		this.transferTemplate = transferTemplate;
	}

	public String getSearchCode() {
		return searchCode;
	}

	public int getPage() {
		return page;
	}

	public int getTotal() {
		return total;
	}

	public int getRecords() {
		return records;
	}

	public Collection<Map<String, Object>> getRows() {
		return rows;
	}

	public void setSearchCode(String searchCode) {
		this.searchCode = searchCode;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public void setRows(Collection<Map<String, Object>> rows) {
		this.rows = rows;
	}

	public String getFlightNo() {
		return flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public String getCabinClassCode() {
		return cabinClassCode;
	}

	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	public String getAircraftModel() {
		return aircraftModel;
	}

	public void setAircraftModel(String aircraftModel) {
		this.aircraftModel = aircraftModel;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getBookingClasses() {
		return bookingClasses;
	}

	public void setBookingClasses(String bookingClasses) {
		this.bookingClasses = bookingClasses;
	}
		
}
