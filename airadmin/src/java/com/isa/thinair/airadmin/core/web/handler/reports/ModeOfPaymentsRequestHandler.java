/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airadmin.core.web.handler.reports;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airmaster.api.util.ZuluLocalTimeConversionHelper;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.security.UserPrincipal;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author Chamindap
 * 
 */
public class ModeOfPaymentsRequestHandler extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(ModeOfPaymentsRequestHandler.class);

	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	/**
	 * The constructor.
	 */
	public ModeOfPaymentsRequestHandler() {
		super();
	}

	/**
	 * @param request
	 * @return
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {

		String strHdnMode = request.getParameter("hdnMode");
		String forward = WebConstants.ACTION_FORWARD_SUCCESS;

		try {
			setDisplayData(request);
			log.debug("ModeOfPaymentsRequestHandler SETDISPLAYDATA() SUCCESS");
		} catch (Exception e) {
			log.error("ModeOfPaymentsRequestHandler SETDISPLAYDATA() FAILED " + e.getMessage());
		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				log.debug("ModeOfPaymentsRequestHandler setReportView Success");
				return null;
			}
		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("ModeOfPaymentsRequestHandler setReportView Failed " + e.getMessageString());
		}

		return forward;
	}

	/**
	 * 
	 * @param request
	 * @throws Exception
	 */
	public static void setDisplayData(HttpServletRequest request) throws Exception {
		setPaymentModeList(request);
		setClientErrors(request);
		setLiveStatus(request);
		setReportingPeriod(request);
		setEntities(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);
	}

	public static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setPaymentModeList(HttpServletRequest request) throws ModuleException {
		String strList = ReportsHTMLGenerator.createPaymentModeHtml(true);
		request.setAttribute(WebConstants.REQ_HTML_PAYMENTTYPE_LIST, strList);
	}

	/**
	 * Sets the report medium to the request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleExceptions
	 */
	private static void setLiveStatus(HttpServletRequest request) throws ModuleException {
		String strLive = request.getParameter(WebConstants.REP_HDN_LIVE);
		if (strLive != null)
			request.setAttribute(WebConstants.REP_SET_LIVE, strLive);

	}

	/**
	 * Setting the reporting period to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setReportingPeriod(HttpServletRequest request) throws ModuleException {
		int period = new Integer(globalConfig.getBizParam(SystemParamKeys.REPORTING_PERIOD)).intValue();
		request.setAttribute(WebConstants.REP_RPT_START_DAY, ReportsHTMLGenerator.getReportingPeriod(period));
	}
	
	/**
	 * Setting the entities to the report
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setEntities(HttpServletRequest request) throws ModuleException {
		String entityList = SelectListGenerator.createEntityListByName();		
		request.setAttribute(WebConstants.REQ_ENTITIES, entityList);
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		ResultSet resultSet = null;
		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String value = request.getParameter("radReportOption");
		String agents = request.getParameter("hdnAgents");
		String payments = request.getParameter("hdnPayments");
		String reportBy = request.getParameter("chkOption");
		String reoprtType = request.getParameter("hdnRptType");
		String paymentGw = request.getParameter("hdnPaymentGw");
		String strEntity = request.getParameter("selEntity");
		String hdnEntity = request.getParameter("hdnEntity");

		String strTime = request.getParameter("hdnTime");
		String reportView = request.getParameter("chkNewview");
		boolean isNewView = (reportView != null && reportView.equalsIgnoreCase("on")) ? true : false;

		String agentName = request.getParameter("hdnAgentName");
		String paymentDesc = request.getParameter("hdnPaymentDesc");
		String id = "UC_REPM_002";
		String modeOFPaymentSummaryRpt = "ModeOfPaymentSummary.jasper";
		String modeOFPaymentDetailRpt = "ModeOfPaymentDetails.jasper";
		String modeOFPaymentDetailCashRpt = "ModeOfPaymentCashDetails.jasper";
		String modeOFPaymentSummaryAgentRpt = "ModeOfPaymentAgentSummary.jasper";
		String modeOFPaymentDetailAgentRpt = "ModeOfPaymentAgentDetail.jasper";
		String modeOFPaymentSummaryAgentNameRpt = "ModeOfPaymentAgentNameSummary.jasper";
		String modeOFPaymentExternalSummaryRpt = "ModeOfPaymentExternalSummary.jasper";
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);
		ReportsSearchCriteria search = new ReportsSearchCriteria();

		Date currentTimestamp = CalendarUtil.getCurrentSystemTimeInZulu();
		List<Integer> arrNominalcodes = new ArrayList<Integer>();

		if (fromDate != null && !fromDate.equals("")) {
			if (strTime.equals(WebConstants.LOCALTIME)) {
				search.setDateRangeFrom(getLocalDateForMOP(fromDate + " 00:00:00"));
			} else {
				search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate) + " 00:00:00");
			}
		}

		if (toDate != null && !toDate.equals("")) {
			if (strTime.equals(WebConstants.LOCALTIME)) {
				search.setDateRangeTo(getLocalDateForMOP(toDate + " 23:59:59"));
			} else {
				search.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate) + " 23:59:59");
			}
		}
		search.setReportViewNew(isNewView);
		search.setEntity(strEntity);

		String zuluTimePart = "";
		String agentStation = "";
		String localTimePart = "";
		Date localtime;
		ZuluLocalTimeConversionHelper timeConverter;

		UserPrincipal user = (UserPrincipal) request.getUserPrincipal();
		agentStation = user.getAgentStation();
		timeConverter = new ZuluLocalTimeConversionHelper(ModuleServiceLocator.getAirportServiceBD());
		localtime = timeConverter.getLocalDateTime(agentStation, currentTimestamp);

		if (!strTime.equals(WebConstants.LOCALTIME)) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(currentTimestamp);
			zuluTimePart = calendar.get(Calendar.HOUR) + ":" + calendar.get(Calendar.MINUTE) + ":"
					+ calendar.get(Calendar.SECOND);
		} else {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(localtime);
			localTimePart = calendar.get(Calendar.HOUR) + ":" + calendar.get(Calendar.MINUTE) + ":"
					+ calendar.get(Calendar.SECOND);
		}

		try {

			Map<String, Object> parameters = new HashMap<String, Object>();

			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}

			if (reportBy.equals("PM")) {
				if (reoprtType.equals("SUMMARY_PM")) {
					parameters.put("BASE_CURRENCY", globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY));
					ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
							ReportsHTMLGenerator.getReportTemplate(modeOFPaymentSummaryRpt));

					arrNominalcodes.addAll(ReservationTnxNominalCode.getOnAccountTypeNominalCodes());
					search.setPaxCreditNominalCodes(Arrays.asList(ReservationTnxNominalCode.CREDIT_BF.getCode()));
					search.setCreditConsumeAgents(AppSysParamsUtil.getCreditConsumeAgents());
					search.setNominalCodes(arrNominalcodes);

					search.setReportType(ReportsSearchCriteria.MODE_OF_PAYMENT_SUMMARY);

				} else if (paymentDesc.equals("Cash")) {
					ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
							ReportsHTMLGenerator.getReportTemplate(modeOFPaymentDetailCashRpt));
					search.setReportType(ReportsSearchCriteria.MODE_OF_PAYMENT_DETAIL);
				} else if (paymentDesc.equals("On Account") || paymentDesc.equals("BSP") || paymentDesc.equals("BSP EXTERNAL")) {
					ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
							ReportsHTMLGenerator.getReportTemplate(modeOFPaymentDetailCashRpt));
					search.setReportType(ReportsSearchCriteria.MODE_OF_PAYMENT_ONACCOUNT_DETAIL);
				} else if (paymentDesc.equals("Loyalty Payment")) {
					ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
							ReportsHTMLGenerator.getReportTemplate(modeOFPaymentDetailCashRpt));
					search.setReportType(ReportsSearchCriteria.MODE_OF_PAYMENT_ONACCOUNT_DETAIL);
				} else if (paymentDesc.equals("Voucher Payment")) {
					ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
							ReportsHTMLGenerator.getReportTemplate(modeOFPaymentDetailCashRpt));
					search.setReportType(ReportsSearchCriteria.MODE_OF_PAYMENT_DETAIL);
				} else {
					ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
							ReportsHTMLGenerator.getReportTemplate(modeOFPaymentDetailRpt));
					search.setReportType(ReportsSearchCriteria.MODE_OF_PAYMENT_CREDIT_DETAIL);
					search.setPaymentGW(paymentGw);
				}
			} else if (reportBy.equals("AC")) {
				if (reoprtType.equals("SUMMARY_AC")) {
					parameters.put("BASE_CURRENCY", globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY));
					ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
							ReportsHTMLGenerator.getReportTemplate(modeOFPaymentSummaryAgentRpt));
					search.setReportType(ReportsSearchCriteria.MODE_OF_PAYMENT_AGENT_SUMMARY);

				} else {
					ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
							ReportsHTMLGenerator.getReportTemplate(modeOFPaymentDetailAgentRpt));
					search.setReportType(ReportsSearchCriteria.MODE_OF_PAYMENT_AGENT_DETAILS);
					if (agents.equalsIgnoreCase("WEB")) {
						search.setUserId(agents);
					} else {
						search.setUserId(agentName);
					}
					search.setPaymentGW(paymentGw);
				}
			} else if (reportBy.equals("AN")) { // report type
				if (reoprtType.equals("SUMMARY_AN")) {
					parameters.put("BASE_CURRENCY", globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY));
					ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
							ReportsHTMLGenerator.getReportTemplate(modeOFPaymentSummaryAgentNameRpt));
					search.setReportType(ReportsSearchCriteria.MODE_OF_PAYMENT_AGENT_NAME_SUMMARY);
				} else {
					// future requirements if a separate type of detailed report is needed
					ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
							ReportsHTMLGenerator.getReportTemplate(modeOFPaymentDetailAgentRpt));
					search.setReportType(ReportsSearchCriteria.MODE_OF_PAYMENT_AGENT_DETAILS);
				}

			} else if (reportBy.equals("EP")) { // report type
				if (reoprtType.equals("SUMMARY_EXTERNAL")) {
					parameters.put("BASE_CURRENCY", globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY));
					ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
							ReportsHTMLGenerator.getReportTemplate(modeOFPaymentExternalSummaryRpt));
					search.setReportType(ReportsSearchCriteria.MODE_OF_PAYMENT_EXTERNAL_SUMMARY);
				}

			} else if (reportBy.equals("BC")) { // report type
				if (reoprtType.equals("CURRENCY_SUMMARY")) {
					parameters.put("BASE_CURRENCY", globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY));
					if (isNewView) {
						parameters.put("VIEW", "ON");
					}
					ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
							ReportsHTMLGenerator.getReportTemplate("ModeOfPaymentSummaryByCurrency.jasper"));
					search.setReportType(ReportsSearchCriteria.MODE_OF_PAYMENT_CURRENCY_SUMMARY);
				} else {
					search.setPaymentCode(paymentDesc);
					ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
							ReportsHTMLGenerator.getReportTemplate(modeOFPaymentDetailAgentRpt));
					search.setReportType(ReportsSearchCriteria.MODE_OF_PAYMENT_CURRENCY_DETAIL);
				}

			}

			ArrayList<String> paymentCol = new ArrayList<String>();
			String paymentArr[] = null;

			if (payments.indexOf(",") != -1) {
				paymentArr = payments.split(",");
			} else {
				paymentArr = new String[1];
				paymentArr[0] = payments;
			}

			String paymentStr = "";
			if (paymentArr != null) {
				for (int r = 0; r < paymentArr.length; r++) {
					if (paymentArr[r].indexOf(".") != -1) {
						paymentStr = paymentArr[r].substring(0, paymentArr[r].indexOf("."));
					} else {
						paymentStr = paymentArr[r];
					}
					paymentCol.add(paymentStr);
				}
			}
			search.setPaymentTypes(paymentCol);

			resultSet = ModuleServiceLocator.getDataExtractionBD().getCallCentreModeOfPaymentsData(search);

			parameters.put("TIME_ZONE", strTime);
			parameters.put("LOCAL_TIME", localTimePart);
			parameters.put("ZULU_TIME", zuluTimePart);
			parameters.put("FROM_DATE", fromDate);
			parameters.put("TO_DATE", toDate);
			parameters.put("ID", id);
			parameters.put("OPTION", value);
			parameters.put("USER_ID", agents);
			parameters.put("PAYMENT_MODE", paymentDesc);
			parameters.put("TIME_MODE", strTime);
			parameters.put("REPORT_MEDIUM", strlive);
			parameters.put("REPORT_NAME", search.getReportType());
			parameters.put("ENTITY_NAME", hdnEntity);
			

			parameters.put("DISPLAY_ADDITIONAL_PAYMENT_MODE", new Boolean(AppSysParamsUtil.isAllowCapturePayRef()).toString());
			String strLogo = AppSysParamsUtil.getReportLogo(false);// globalConfig.getBizParam(SystemParamKeys.REPORT_LOGO);
			String reportsRootDir = "../../images/" +  AppSysParamsUtil.getReportLogo(true);
			String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);

			// To provide Report Format Options
			String reportNumFormat = request.getParameter("radRptNumFormat");
			ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

			if (reoprtType.equals("SUMMARY_AN")) {

				if (value.trim().equals("HTML")) {
					// reportsRootDir="../../images/"+strLogo;
					parameters.put("IMG", reportsRootDir);
					ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null,
							reportsRootDir, response);

				} else if (value.trim().equals("PDF")) {
					reportsRootDir = ReportsHTMLGenerator.getReportTemplate(strLogo);
					parameters.put("IMG", reportsRootDir);
					response.reset();
					response.addHeader("Content-Disposition", "attachment;filename=ModeOfPaymentDetails.pdf");
					ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);

				} else if (value.trim().equals("EXCEL")) {
					response.reset();
					response.addHeader("Content-Disposition", "attachment;filename=ModeOfPaymentDetails.xls");
					ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);

				} else if (value.trim().equals("CSV")) {
					response.reset();
					response.addHeader("Content-Disposition", "attachment;filename=ModeOfPaymentDetails.csv");
					ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);

				} else if (value.trim().equals("CSV_AGENT")) {
					response.reset();
					response.addHeader("Content-Disposition", "attachment;filename=ModeOfPaymenAgenttDetails.csv");
					ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);
				}

			} else {
				//
				if (value.trim().equals("HTML")) {
					reportsRootDir = "../../images/" + strLogo;
					parameters.put("IMG", reportsRootDir);
					ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null,
							reportsRootDir, response);

				} else if (value.trim().equals("PDF")) {
					reportsRootDir = ReportsHTMLGenerator.getReportTemplate(strLogo);
					parameters.put("IMG", reportsRootDir);
					response.reset();
					response.addHeader("Content-Disposition", "attachment;filename=ModeOfPaymentDetails.pdf");
					ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);

				} else if (value.trim().equals("EXCEL")) {

					response.reset();
					response.addHeader("Content-Disposition", "attachment;filename=ModeOfPaymentDetails.xls");
					ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);

				} else if (value.trim().equals("CSV")) {
					response.reset();
					response.addHeader("Content-Disposition", "attachment;filename=ModeOfPaymentDetails.csv");
					ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);

				} else if (value.trim().equals("CSV_AGENT")) {
					response.reset();
					response.addHeader("Content-Disposition", "attachment;filename=ModeOfPaymenAgenttDetails.csv");
					ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);
				}

			}// end report
		} catch (Exception e) {
			log.error(e);
		}
	}

	private static String getLocalDateForMOP(String str) {
		String strNew = "";
		SimpleDateFormat sdt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		SimpleDateFormat convsdt = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
		long localt = 4 * 3600 * 1000;
		Date serchDate = null;
		try {
			serchDate = sdt.parse(str);
			long searct = serchDate.getTime();
			searct = searct - localt;
			serchDate = new Date(searct);
			strNew = convsdt.format(serchDate);
		} catch (Exception e) {
			log.error("Error in prsingdate in MOde of payment" + e);
		}
		return strNew;
	}
}