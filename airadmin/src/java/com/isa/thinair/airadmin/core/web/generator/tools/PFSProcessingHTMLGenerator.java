/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */

/**
 * @author srikantha
 *
 * 
 * Window - Preferences - Java - Code Style - Code Templates
 */

package com.isa.thinair.airadmin.core.web.generator.tools;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airreservation.api.model.Pfs;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

public class PFSProcessingHTMLGenerator {

	private static String clientErrors;

	// searching parameters
	private static final String PARAM_SEARCH_AIRPORT = "selAirport";

	private static final String PARAM_SEARCH_FROM = "txtFrom";

	private static final String PARAM_SEARCH_TO = "txtTo";
	
	private static final String PARAM_SEARCH_Flight_FROM = "txtFlightFrom";
	
	private static final String PARAM_SEARCH_Flight_TO = "txtFlightTo";
	
	private static final String PARAM_SEARCH_Flight_NO = "txtSFlightNo";

	private static final String PARAM_START_REC_NO = "hdnRecNo";

	private static final String PARAM_SEARCH_PROCESS_STATUS = "selProcessType";

	private static final String PARAM_MODE = "hdnMode";

	private String strFormFieldsVariablesJS = "";

	private static final String DATE_FORMAT_DDMMYYYYHHmm = "dd/MM/yyyy HH:mm";

	private static final String DATE_FORMAT_DDMMYYYY = "dd/MM/yyyy";

	SimpleDateFormat outputDateFormat;
	SimpleDateFormat outputDateFormat_onlyDate;

	@SuppressWarnings("unchecked")
	public final String getPFSProcessingRowHtml(HttpServletRequest request) throws ModuleException, Exception {

		String strMode = request.getParameter(PARAM_MODE);
		String strStartRecNo = (request.getParameter(PARAM_START_REC_NO) == null || request.getParameter(PARAM_START_REC_NO)
				.trim().equals("")) ? "1" : request.getParameter(PARAM_START_REC_NO);
		String strUIModeJS = "var isSearchMode = false; var strPFSContent;";
		String strAirportCode = "";
		String strFromDate = "";
		String strToDate = "";
		String strProcessStatus = "";
		String strFlightFromDate = "";
		String strFlightToDate = "";
		String strFlightNo = "";
		Date fromDate = null;
		Date toDate = null;
		
		Date flightFrom = null;
		Date flightTo = null;

		Page page = null;

		outputDateFormat = new SimpleDateFormat(DATE_FORMAT_DDMMYYYYHHmm);
		outputDateFormat_onlyDate = new SimpleDateFormat(DATE_FORMAT_DDMMYYYY);

		if (strMode != null) {
			strUIModeJS = "var isSearchMode = true; var strPFSContent;";

			strAirportCode = AiradminUtils.getNotNullString(request.getParameter(PARAM_SEARCH_AIRPORT));
			strFormFieldsVariablesJS = "var airportCode ='" + strAirportCode + "';";

			strFromDate = AiradminUtils.getNotNullString(request.getParameter(PARAM_SEARCH_FROM));
			strFormFieldsVariablesJS += "var fromDate ='" + strFromDate + "';";

			strToDate = AiradminUtils.getNotNullString(request.getParameter(PARAM_SEARCH_TO));
			strFormFieldsVariablesJS += "var toDate ='" + strToDate + "';";
			
			strFlightFromDate = AiradminUtils.getNotNullString(request.getParameter(PARAM_SEARCH_Flight_FROM));
			strFormFieldsVariablesJS += "var flightFromDate ='" + strFlightFromDate + "';";

			strFlightToDate = AiradminUtils.getNotNullString(request.getParameter(PARAM_SEARCH_Flight_TO));
			strFormFieldsVariablesJS += "var flightToDate ='" + strFlightToDate + "';";
			
			strFlightNo = AiradminUtils.getNotNullString(request.getParameter(PARAM_SEARCH_Flight_NO));
			strFormFieldsVariablesJS += "var flightNo ='" + strFlightNo + "';";

			strProcessStatus = (request.getParameter(PARAM_SEARCH_PROCESS_STATUS) == null || request
					.getParameter(PARAM_SEARCH_PROCESS_STATUS).trim().toUpperCase().equals("ALL")) ? "" : request.getParameter(
					PARAM_SEARCH_PROCESS_STATUS).trim();
			strFormFieldsVariablesJS += "var processStatus ='" + strProcessStatus + "';";

			SimpleDateFormat dateFormat = null;

			if (!strFromDate.equals("")) {
				if (strFromDate.indexOf('-') != -1) {
					dateFormat = new SimpleDateFormat("dd-MM-yy");
				}
				if (strFromDate.indexOf('/') != -1) {
					dateFormat = new SimpleDateFormat("dd/MM/yy");
				}
				if (strFromDate.indexOf(' ') != -1) {
					strFromDate = strFromDate.substring(0, strFromDate.indexOf(' '));
				}

				fromDate = dateFormat.parse(strFromDate);
			} else {
				fromDate = null;
			}

			if (!strToDate.equals("")) {
				if (strToDate.indexOf('-') != -1) {
					dateFormat = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
				}
				if (strToDate.indexOf('/') != -1) {
					dateFormat = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
				}
				if (strToDate.indexOf(' ') != -1) {
					strToDate = strToDate.substring(0, strToDate.indexOf(' '));
				}

				toDate = dateFormat.parse(strToDate + " 23:59:59");
			} else {
				toDate = null;
			}
			
			if (!strFlightFromDate.equals("")) {
				if (strFlightFromDate.indexOf('-') != -1) {
					dateFormat = new SimpleDateFormat("dd-MM-yy");
				}
				if (strFlightFromDate.indexOf('/') != -1) {
					dateFormat = new SimpleDateFormat("dd/MM/yy");
				}
				if (strFlightFromDate.indexOf(' ') != -1) {
					strFlightFromDate = strFlightFromDate.substring(0, strFlightFromDate.indexOf(' '));
				}

				flightFrom = dateFormat.parse(strFlightFromDate);
			} else {
				flightFrom = null;
			}
			
			if (!strFlightToDate.equals("")) {
				if (strFlightToDate.indexOf('-') != -1) {
					dateFormat = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
				}
				if (strFlightToDate.indexOf('/') != -1) {
					dateFormat = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
				}
				if (strFlightToDate.indexOf(' ') != -1) {
					strFlightToDate = strFlightToDate.substring(0, strFlightToDate.indexOf(' '));
				}

				flightTo = dateFormat.parse(strFlightToDate + " 23:59:59");
			} else {
				flightTo = null;
			}

			request.setAttribute(WebConstants.REQ_OPERATION_UI_MODE, strUIModeJS);

			// Make Criterians
			List<ModuleCriterion> critrian = new ArrayList<ModuleCriterion>();
			List<String> orderList = new ArrayList<String>();
			orderList.add("dateDownloaded");

			if (fromDate != null) {
				// Set criterian - fromDate
				ModuleCriterion MCname = new ModuleCriterion();
				MCname.setCondition(ModuleCriterion.CONDITION_GREATER_THAN_OR_EQUALS);
				MCname.setFieldName("dateDownloaded");
				List<Date> value = new ArrayList<Date>();
				value.add(fromDate);
				MCname.setValue(value);
				critrian.add(MCname);
			}

			if (toDate != null) {
				// Set criterian - toDate
				ModuleCriterion MCname = new ModuleCriterion();
				MCname.setCondition(ModuleCriterion.CONDITION_LESS_THAN_OR_EQUALS);
				MCname.setFieldName("dateDownloaded");
				List<Date> value = new ArrayList<Date>();
				value.add(toDate);
				MCname.setValue(value);
				critrian.add(MCname);
			}
			
			if (flightFrom != null) {
				// Set criterian - flightFrom
				ModuleCriterion MCname = new ModuleCriterion();
				MCname.setCondition(ModuleCriterion.CONDITION_GREATER_THAN_OR_EQUALS);
				MCname.setFieldName("departureDate");
				List<Date> value = new ArrayList<Date>();
				value.add(flightFrom);
				MCname.setValue(value);
				critrian.add(MCname);
			}

			if (flightTo != null) {
				// Set criterian - flightTo
				ModuleCriterion MCname = new ModuleCriterion();
				MCname.setCondition(ModuleCriterion.CONDITION_LESS_THAN_OR_EQUALS);
				MCname.setFieldName("departureDate");
				List<Date> value = new ArrayList<Date>();
				value.add(flightTo);
				MCname.setValue(value);
				critrian.add(MCname);
			}
			
			if (!strFlightNo.equals("")) {
				// Set criterian - strFlightNo
				ModuleCriterion MCname = new ModuleCriterion();
				MCname.setCondition(ModuleCriterion.CONDITION_LIKE);
				MCname.setFieldName("flightNumber");
				List<String> value = new ArrayList<String>();
				value.add(strFlightNo);
				MCname.setValue(value);
				critrian.add(MCname);
			}

			if (!strAirportCode.equals("")) {
				// Set criterian - strAirportCode
				ModuleCriterion MCname = new ModuleCriterion();
				MCname.setCondition(ModuleCriterion.CONDITION_EQUALS);
				MCname.setFieldName("fromAirport");
				List<String> value = new ArrayList<String>();
				value.add(strAirportCode);
				MCname.setValue(value);
				critrian.add(MCname);
			}

			if (!strProcessStatus.equals("")) {
				// Set criterian - strProcessStatus
				ModuleCriterion MCname = new ModuleCriterion();
				MCname.setCondition(ModuleCriterion.CONDITION_EQUALS);
				MCname.setFieldName("processedStatus");
				List<String> value = new ArrayList<String>();
				value.add(strProcessStatus);
				MCname.setValue(value);
				critrian.add(MCname);
			}

			page = ModuleServiceLocator.getReservationBD().getPagedPFSData(critrian,
					Integer.valueOf(strStartRecNo).intValue() - 1, 20, orderList);

			if (page != null) {
				request.getSession().setAttribute("PFSDataSet", page.getPageData());
			} else {
				request.getSession().setAttribute("PFSDataSet", new ArrayList<Pfs>());
			}

			strFormFieldsVariablesJS += " totalRes  ='" + page.getTotalNoOfRecords() + "';";
		}

		setFormFieldValues(strFormFieldsVariablesJS);

		if (page != null) {
			return createPFSProcessingRowHTML(page.getPageData());
		} else {
			return createPFSProcessingRowHTML(new ArrayList<Pfs>());
		}

	}

	private String createPFSProcessingRowHTML(Collection<Pfs> listPFSProcessing) throws ModuleException {

		List<Pfs> list = (List<Pfs>) listPFSProcessing;

		Object[] listArr = list.toArray();
		StringBuffer sb = new StringBuffer("var arrData = new Array();");

		Pfs pfs = null;
		String tempDesc = "";
		for (int i = 0; i < listArr.length; i++) {
			pfs = (Pfs) listArr[i];
			sb.append("arrData[" + i + "] = new Array();");

			sb.append("arrData[" + i + "][0] = '" + outputDateFormat.format(pfs.getDateDownloaded()) + "';");

			if (pfs.getFlightNumber() != null) {
				sb.append("arrData[" + i + "][1] = '" + pfs.getFlightNumber() + "';");
			} else {
				sb.append("arrData[" + i + "][1] = '';");
			}

			if (pfs.getDepartureDate() != null) {
				sb.append("arrData[" + i + "][2] = '" + outputDateFormat.format(pfs.getDepartureDate()) + "';");
			} else {
				sb.append("arrData[" + i + "][2] = '';");
			}

			sb.append("arrData[" + i + "][3] = '" + pfs.getPpId() + "';");

			tempDesc = "";

			if (pfs.getProcessedStatus().equals(ReservationInternalConstants.PfsStatus.PARSED))
				tempDesc = "Parsed";
			else if (pfs.getProcessedStatus().equals(ReservationInternalConstants.PfsStatus.UN_PARSED_WITH_ERRORS))
				tempDesc = "Processed with errors";
			else if (pfs.getProcessedStatus().equals(ReservationInternalConstants.PfsStatus.RECONCILE_SUCCESS))
				tempDesc = "Processed";
			else if (pfs.getProcessedStatus().equals(ReservationInternalConstants.PfsStatus.UN_PARSED))
				tempDesc = "Unparsed";
			else if (pfs.getProcessedStatus().equals(ReservationInternalConstants.PfsStatus.WAITING_FOR_ETL_PROCESS))
				tempDesc = "Waiting";
			else
				tempDesc = "Unknown";

			sb.append("arrData[" + i + "][4] = '" + tempDesc + "';");

			if (pfs.getPfsContent() != null) {
				sb.append("arrData[" + i + "][5] = '" + StringUtils.replace(pfs.getPfsContent(), "\n", "<\\BR>") + "';");

			} else {
				sb.append("arrData[" + i + "][5] = '';");
			}
			if (pfs.getProcessedStatus() == ReservationInternalConstants.PfsStatus.UN_PARSED_WITH_ERRORS) {
				sb.append("arrData[" + i + "][6] = '" + ReservationInternalConstants.PfsStatus.UN_PARSED + "';");
			} else {
				sb.append("arrData[" + i + "][6] = '" + pfs.getProcessedStatus() + "';");
			}

			sb.append("arrData[" + i + "][7] = new Array();");

			sb.append("arrData[" + i + "][8] = '';");

			if (pfs.getFlightNumber() != null) {
				sb.append("arrData[" + i + "][9] = '" + pfs.getFlightNumber() + "';");
			} else {
				sb.append("arrData[" + i + "][9] = '';");
			}
			if (pfs.getDepartureDate() != null) {
				if (pfs.getProcessedStatus() == ReservationInternalConstants.PfsStatus.UN_PARSED_WITH_ERRORS) {
					sb.append("arrData[" + i + "][10] = '" + outputDateFormat_onlyDate.format(pfs.getDepartureDate()) + "';");
				} else {
					sb.append("arrData[" + i + "][10] = '" + outputDateFormat.format(pfs.getDepartureDate()) + "';");
				}
			} else {
				sb.append("arrData[" + i + "][10] = '';");
			}
			if (pfs.getFromAirport() != null) {
				sb.append("arrData[" + i + "][11] = '" + pfs.getFromAirport() + "';");
			} else {
				sb.append("arrData[" + i + "][11] = '';");
			}
			if (pfs.getFromAddress() != null) {
				sb.append("arrData[" + i + "][12] = '" + pfs.getFromAddress() + "';");
			} else {
				sb.append("arrData[" + i + "][12] = '';");
			}

			sb.append("arrData[" + i + "][13] = " + pfs.getVersion() + ";");

			sb.append("arrData[" + i + "][14] = "
					+ ModuleServiceLocator.getReservationBD().getPfsParseEntryCount(pfs.getPpId(), null) + ";");

		}

		return sb.toString();
	}

	public static StringBuffer createAlternatePFSList(List<FlightSegement> segmentList) {
		StringBuffer sb = new StringBuffer("var arrFLData = new Array();");
		SimpleDateFormat outputDateFormat = new SimpleDateFormat();
		Object[] listArr = segmentList.toArray();
		for (int element = 0; element < listArr.length; element++) {
			FlightSegement flightSegement = (FlightSegement) listArr[element];
			sb.append("arrFLData[" + element + "] = new Array();");
			sb.append("arrFLData[" + element + "][0] = '" + flightSegement.getFlightId() + "';");
			sb.append("arrFLData[" + element + "][1] = '" + outputDateFormat.format(flightSegement.getEstTimeDepatureLocal())
					+ "';");
			sb.append("arrFLData[" + element + "][2] = '" + flightSegement.getStatus() + "';");
		}

		return sb;
	}

	public static String getClientErrors(HttpServletRequest request) throws ModuleException {

		if (clientErrors == null) {
			Properties moduleErrs = new Properties();

			moduleErrs.setProperty("um.pfsprocess.form.fromdate.required", "fromDateRqrd");
			moduleErrs.setProperty("um.pfsprocess.form.todate.required", "toDateRqrd");
			moduleErrs.setProperty("um.pfsprocess.form.fromdate.invalid", "fromDateInvalid");
			moduleErrs.setProperty("um.pfsprocess.form.todate.invalid", "toDateInvalid");
			moduleErrs.setProperty("um.process.form.airport.required", "airportRqrd");
			moduleErrs.setProperty("um.pfsprocess.form.todate.lessthan.fromdate", "todateLessthanFromDate");
			moduleErrs.setProperty("um.pfsprocess.form.fromdate.lessthan.currentdate", "fromDateLessthanCurrentDate");
			moduleErrs.setProperty("um.pfsprocess.form.todate.lessthan.currentdate", "toDateLessthanToday");
			
			moduleErrs.setProperty("um.pfsprocess.form.flighttodate.lessthan.flightfromdate", "todateLessthanFlightFromDate");
			moduleErrs.setProperty("um.pfsprocess.form.flightfromdate.lessthan.currentdate", "flightFromDateLessthanCurrentDate");
			moduleErrs.setProperty("um.pfsprocess.form.flighttodate.lessthan.currentdate", "flightToDateLessthanToday");

			moduleErrs.setProperty("um.process.form.flightDate.required", "flightDateRqrd");
			moduleErrs.setProperty("um.pfsprocess.form.flightDateFormat.invaid", "flightDateFormatInvalid");
			moduleErrs.setProperty("um.pfsprocess.form.flightTimeFormat.invaid", "flightTimeFormatInvalid");
			moduleErrs.setProperty("um.process.form.downloadTS.required", "downloadTSRqrd");
			moduleErrs.setProperty("um.pfsprocess.form.downloadTSDateFormat.invaid", "downloadTSDateFormatInvalid");
			moduleErrs.setProperty("um.pfsprocess.form.downloadTSTimeFormat.invaid", "downloadTSTimeFormatInvalid");
			moduleErrs.setProperty("um.process.form.flightNo.required", "flightNoRqrd");

			moduleErrs.setProperty("um.process.form.downloadTS.lessthanFlightTs", "DownloadTSGreaterFlightTs");
			moduleErrs.setProperty("um.process.form.downloadTS.lessthanToday", "DownloadTSlessThanToday");
			moduleErrs.setProperty("um.airadmin.delete.confirmation", "deleteMessage");
			moduleErrs.setProperty("um.process.form.downloadTime.required", "downloadTimeRqrd");
			moduleErrs.setProperty("um.process.form.pfs.flightTS.lessthanToday", "FlightTSGreaterFlightTs");

			clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}

		return clientErrors;
	}

	private void setFormFieldValues(String strFormFieldsVariablesJS) {
		this.strFormFieldsVariablesJS = strFormFieldsVariablesJS;
	}

	public String getFormFieldValues() {
		return strFormFieldsVariablesJS;
	}

}
