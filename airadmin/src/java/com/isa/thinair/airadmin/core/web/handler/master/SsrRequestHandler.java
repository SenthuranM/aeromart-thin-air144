package com.isa.thinair.airadmin.core.web.handler.master;

import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.master.SsrHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airmaster.api.model.SSR;
import com.isa.thinair.commons.api.exception.MessagesUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author Dhanya
 * 
 */

public class SsrRequestHandler extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(SsrRequestHandler.class);

	private static AiradminConfig airadminConfig = new AiradminConfig();

	private static final String PARAM_VERSION = "hdnVersion";
	private static final String PARAM_MODE = "hdnMode";
	private static final String PARAM_STATUS = "chkStatus";

	private static final String PARAM_SSRID = "hdnSSRId";
	private static final String PARAM_SSRCODE = "txtSSRCode";
	private static final String PARAM_SSRDESC = "txtSSRDesc";
	private static final String PARAM_DEFAULTCHARGE = "txtDefaultCharge";
	private static final String PARAM_SSRCATEGORY = "selSSRCategory";
	private static final String PARAM_SSRVISIBILITY = "selSSRVisibility";

	private static void setExceptionOccured(HttpServletRequest request, boolean value) {
		setAttribInRequest(request, "isExceptionOccured", new Boolean(value));
	}

	private static boolean isExceptionOccured(HttpServletRequest request) {
		return ((Boolean) getAttribInRequest(request, "isExceptionOccured")).booleanValue();
	}

	private static boolean isSuccessfulSaveMessageDisplayEnabled() {
		boolean isMessageEnabled = false;
		if (airadminConfig.getMessage("um.airadmin.savemessage.display").equals("true")) {
			isMessageEnabled = true;
		}
		return isMessageEnabled;
	}

	/**
	 * Main Execute Method for Country Actions
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {

		String strHdnMode = request.getParameter(PARAM_MODE);
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;

		setAttribInRequest(request, "strModeJS", "var isAddMode = false;");
		setExceptionOccured(request, false);

		try {
			if (strHdnMode != null) {
				if (strHdnMode.equals(WebConstants.ACTION_ADD)) {
					checkPrivilege(request, WebConstants.PRIV_SYS_MAS_SSR_ADD);
					setAttribInRequest(request, "strModeJS", "var isAddMode = true;");
					saveData(request);
				} else if (strHdnMode.equals(WebConstants.ACTION_EDIT)) {
					checkPrivilege(request, WebConstants.PRIV_SYS_MAS_SSR_EDIT);
					setAttribInRequest(request, "strModeJS", "var isAddMode = false;");
					saveData(request);
				} else if (strHdnMode.equals(WebConstants.ACTION_DELETE)) {
					checkPrivilege(request, WebConstants.PRIV_SYS_MAS_SSR_DELETE);
					deleteData(request);
				}

			}
			setDisplayData(request);

		} catch (Exception exception) {
			log.error("Exception in SsrRequestHandler:execute()", exception);
			if (exception instanceof RuntimeException) {
				forward = AdminStrutsConstants.AdminAction.ERROR;
				JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
			}
		}

		return forward;
	}

	/**
	 * Saves the SSR Data
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	private static void saveData(HttpServletRequest request) throws Exception {
		log.debug("Inside the SsrRequestHandler saveData()...");

		Properties ssrProp = getProperty(request);

		try {

			SSR ssr = null;

			String strSSRId = request.getParameter(PARAM_SSRID);
			String[] strVisibilities = request.getParameterValues(PARAM_SSRVISIBILITY);

			if (strSSRId != null && !"".equals(strSSRId)) {
				ssr = ModuleServiceLocator.getSsrServiceBD().getSSR(Integer.parseInt(strSSRId));
				if (ssr == null)
					ssr = new SSR();
			} else
				ssr = new SSR();

			if (!"".equals(ssrProp.getProperty(PARAM_VERSION))) {
				ssr.setVersion(Long.parseLong(ssrProp.getProperty(PARAM_VERSION)));
			}

			if (strSSRId != null && !"".equals(strSSRId))
				ssr.setSsrId(Integer.parseInt(strSSRId));

			ssr.setSsrCode(ssrProp.getProperty(PARAM_SSRCODE));
			ssr.setSsrDesc(ssrProp.getProperty(PARAM_SSRDESC).trim());
			ssr.setDefaultCharge(Double.parseDouble(ssrProp.getProperty(PARAM_DEFAULTCHARGE)));

			if (!ssrProp.getProperty(PARAM_SSRCATEGORY).equals("-1") && !ssrProp.getProperty(PARAM_SSRCATEGORY).equals("")) {
				ssr.setSsrCatId(Integer.parseInt(ssrProp.getProperty(PARAM_SSRCATEGORY)));
			}

			if (ssrProp.getProperty(PARAM_STATUS).equals("on"))
				ssr.setStatus(SSR.STATUS_ACTIVE);
			else
				ssr.setStatus(SSR.STATUS_INACTIVE);

			Set<Integer> visibleChannelIds = new HashSet<Integer>();
			if (strVisibilities != null) {
				for (int al = 0; al < strVisibilities.length; al++) {
					visibleChannelIds.add(Integer.valueOf(strVisibilities[al]));
				}
			}

			ssr.setVisibleChannelIds(visibleChannelIds);

			ModuleServiceLocator.getSsrServiceBD().saveSSR(ssr);

			setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = true;");
			ssr = null;
			saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS), WebConstants.MSG_SUCCESS);
			log.debug("SsrRequestHandler.saveData() method is successfully executed.");

		} catch (ModuleException moduleException) {
			log.error("Exception in SsrRequestHandler:saveData() [origin module=" + moduleException.getModuleDesc() + "]",
					moduleException);

			setExceptionOccured(request, true);
			String strFormData = getErroForm(ssrProp);
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
			if (moduleException.getMessageString().equals(MessagesUtil.getMessage("module.duplicate.key"))) {
				saveMessage(request, airadminConfig.getMessage("um.ssr.form.id.already.exist"), WebConstants.MSG_ERROR);
			} else {
				saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
			}

		} catch (Exception exception) {

			log.error("Exception in SsrRequestHandler:saveData()", exception);
			setExceptionOccured(request, true);

			if (exception instanceof RuntimeException) {
				throw exception;
			} else {

				String strFormData = getErroForm(ssrProp);
				request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
				saveMessage(request, exception.getMessage(), WebConstants.MSG_ERROR);
			}
		}
	}

	/**
	 * Delete the SSR Data
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void deleteData(HttpServletRequest request) {

		String strSSRId = request.getParameter(PARAM_SSRID);

		try {
			if (strSSRId != null && !"".equals(strSSRId)) {
				SSR existingSSR = ModuleServiceLocator.getSsrServiceBD().getSSR(Integer.parseInt(strSSRId));
				if (existingSSR == null)
					ModuleServiceLocator.getSsrServiceBD().deleteSSR(Integer.parseInt(strSSRId));
				else {
					String strVersion = request.getParameter(PARAM_VERSION);
					if (strVersion != null && !"".equals(strVersion)) {
						existingSSR.setVersion(Long.parseLong(strVersion));
						ModuleServiceLocator.getSsrServiceBD().deleteSSR(existingSSR);
					} else {
						ModuleServiceLocator.getSsrServiceBD().deleteSSR(Integer.parseInt(strSSRId));
					}
				}
				setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = false;");
				log.debug("SsrRequestHandler.deleteData() method is successfully executed.");
				saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_DELETE_SUCCESS), WebConstants.MSG_SUCCESS);
			}
		} catch (ModuleException moduleException) {
			log.error("Exception in SsrRequestHandler:execute() [origin module=" + moduleException.getModuleDesc() + "]",
					moduleException);
			if (moduleException.getMessageString().equals(MessagesUtil.getMessage("module.constraint.childrecord"))) {
				saveMessage(request, airadminConfig.getMessage("um.airadmin.childrecord"), WebConstants.MSG_ERROR);
			} else {
				saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
			}
		}
	}

	/**
	 * Sets the Display Data for the SSR Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	public static void setDisplayData(HttpServletRequest request) {

		setSsrRowHtml(request);
		setSsrCategoryList(request);
		setSalesChannelList(request);
		setClientErrors(request);

		if (!isExceptionOccured(request)) {
			String strFormData = "var arrFormData = new Array();";
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
		}
		request.setAttribute(WebConstants.REQ_OPERATION_MODE, getAttribInRequest(request, "strModeJS"));

		if (request.getParameter(PARAM_MODE) == null) {

			setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = false;");
			request.setAttribute(WebConstants.REQ_TRANSACTION_STATUS,
					getAttribInRequest(request, "isSaveTransactionSuccessfulJS"));

		} else if (request.getParameter(PARAM_MODE) != null && !request.getParameter(PARAM_MODE).equals(WebConstants.ACTION_ADD)
				&& !request.getParameter(PARAM_MODE).equals(WebConstants.ACTION_EDIT)) {

			setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = false;");
			request.setAttribute(WebConstants.REQ_TRANSACTION_STATUS,
					getAttribInRequest(request, "isSaveTransactionSuccessfulJS"));
		} else {

			request.setAttribute(WebConstants.REQ_TRANSACTION_STATUS,
					getAttribInRequest(request, "isSaveTransactionSuccessfulJS"));
		}
		request.setAttribute(WebConstants.REQ_TRANSACTION_DISPLAYSTATUS, "var isSuccessfulSaveMessageDisplayEnabled = "
				+ isSuccessfulSaveMessageDisplayEnabled() + ";");
	}

	/**
	 * Sets the Client Validations for the SSR Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setClientErrors(HttpServletRequest request) {

		try {

			String strClientErrors = SsrHTMLGenerator.getClientErrors(request);
			if (strClientErrors == null)
				strClientErrors = "";

			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);

		} catch (ModuleException moduleException) {
			log.error("Exception in SsrRequestHandler:setClientErrors() [origin module=" + moduleException.getModuleDesc() + "]",
					moduleException);
		}
	}

	/**
	 * Sets the SSR Data Row to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setSsrRowHtml(HttpServletRequest request) {

		try {

			SsrHTMLGenerator htmlGen = new SsrHTMLGenerator();
			String strHtml = htmlGen.getSsrRowHtml(request);
			request.setAttribute(WebConstants.REQ_HTML_ROWS, strHtml);

		} catch (ModuleException moduleException) {
			log.error("Exception in SsrRequestHandler:setSsrRowHtml() [origin module=" + moduleException.getModuleDesc() + "]",
					moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the SSR Category list to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setSsrCategoryList(HttpServletRequest request) {
		try {
			String strHtml = SelectListGenerator.createSsrCategoryList();
			request.setAttribute(WebConstants.REQ_HTML_SSR_CATEGORY_LIST, strHtml);
		} catch (ModuleException moduleException) {
			log.error("Exception in SsrRequestHandler:setSsrCategoryList() [origin module=" + moduleException.getModuleDesc()
					+ "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Sales Channel list to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setSalesChannelList(HttpServletRequest request) {
		try {
			String strChannels = SelectListGenerator.createSalesChannelVisibilityList();
			request.setAttribute("reqSalesChannels", strChannels);
		} catch (ModuleException moduleException) {
			log.error("Exception in SsrRequestHandler:setSalesChannelList() [origin module=" + moduleException.getModuleDesc()
					+ "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Creats a Propert file with SSR Data from the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return Properties the property File with SSR data
	 */
	private static Properties getProperty(HttpServletRequest request) {

		Properties props = new Properties();
		props.setProperty(PARAM_VERSION, AiradminUtils.getNotNullString(request.getParameter(PARAM_VERSION)));
		props.setProperty(PARAM_SSRCODE, AiradminUtils.getNotNullString(request.getParameter(PARAM_SSRCODE)));
		props.setProperty(PARAM_SSRDESC, AiradminUtils.getNotNullString(request.getParameter(PARAM_SSRDESC)));
		props.setProperty(PARAM_DEFAULTCHARGE, AiradminUtils.getNotNullString(request.getParameter(PARAM_DEFAULTCHARGE)));
		props.setProperty(PARAM_SSRCATEGORY, AiradminUtils.getNotNullString(request.getParameter(PARAM_SSRCATEGORY)));
		props.setProperty(PARAM_STATUS, AiradminUtils.getNotNullString(request.getParameter(PARAM_STATUS)));

		return props;
	}

	/**
	 * Creates Form Data Array from a Property File
	 * 
	 * @param erp
	 *            the Property file with Form Data
	 * @return String the Form Data Array
	 */
	private static String getErroForm(Properties erp) {
		StringBuffer ersb = new StringBuffer("var arrFormData = new Array();");
		ersb.append("arrFormData[0] = new Array();");
		ersb.append("arrFormData[0][1] = '" + erp.getProperty(PARAM_SSRCODE) + "';");
		ersb.append("arrFormData[0][2] = '" + erp.getProperty(PARAM_SSRDESC) + "';");
		ersb.append("arrFormData[0][3] = '" + erp.getProperty(PARAM_DEFAULTCHARGE) + "';");
		ersb.append("arrFormData[0][4] = '" + erp.getProperty(PARAM_SSRCATEGORY) + "';");
		ersb.append("arrFormData[0][5] = '" + erp.getProperty(PARAM_STATUS) + "';");
		ersb.append("arrFormData[0][6] = '" + erp.getProperty(PARAM_VERSION) + "';");
		return ersb.toString();
	}

}
