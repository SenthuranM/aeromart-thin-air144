package com.isa.thinair.airadmin.core.web.handler.tools;

import javax.servlet.http.HttpServletRequest;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

public class RequestQueueHandler extends BasicRequestHandler{


	/**
	 * Main Execute Method For Request Queue Management 
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {
		String forward = S2Constants.Result.SUCCESS;		
		try {			
			setComboboxData(request);				
		} catch (Exception e) {			
			e.printStackTrace();
			forward = S2Constants.Result.ERROR;
		}		
		return forward;
	}
	
	private static void setComboboxData(HttpServletRequest request) throws Exception{		
		setApplicablePages(request);		
	}
	
	private static void setApplicablePages(HttpServletRequest request) throws ModuleException{		
		String queuePages = SelectListGenerator.createQueuePagesList();
		request.setAttribute(WebConstants.REQ_HTML_QUEUE_PAGES, queuePages);		
	}
}