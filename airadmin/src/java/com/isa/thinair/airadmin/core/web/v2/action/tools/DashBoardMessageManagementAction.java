package com.isa.thinair.airadmin.core.web.v2.action.tools;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.v2.action.common.CommonAdminRequestResponseAwareCommonAction;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airadmin.core.web.v2.generator.tools.DashboardMessageHG;
import com.isa.thinair.airadmin.core.web.v2.util.CommonUtil;
import com.isa.thinair.airmaster.api.criteria.DashBoardMsgSearchCriteria;
import com.isa.thinair.airmaster.api.model.DashboardMessage;
import com.isa.thinair.airmaster.api.model.DashbrdMsgUser;
import com.isa.thinair.airmaster.api.to.DashboardMsgTO;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.airsecurity.api.model.UserSearchDTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.webplatform.api.v2.util.MultiSelectDataParser;
import com.isa.thinair.webplatform.core.util.DataUtil;
import com.isa.thinair.webplatform.core.util.RowDecorator;

/**
 * DashBoard Message Add/Edit
 * 
 * @author Navod Ediriweera
 * @since 03 Aug 2010
 */
@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "") })
public class DashBoardMessageManagementAction extends CommonAdminRequestResponseAwareCommonAction {
	private static Log log = LogFactory.getLog(DashBoardMessageManagementAction.class);

	private Collection<Map<String, Object>> userRows = new ArrayList<Map<String, Object>>();
	private Map<String, String> userIDtoDisplayMap = new HashMap<String, String>();
	private int userPage;
	private int userTotal;
	private int userRecords;

	private Collection<Map<String, Object>> rows;
	private int page;
	private int total;
	private int records;
	private static final int PAGE_LENGTH = 10;
	private DashboardMsgTO dashboardMsgTO;
	private DashBoardMsgSearchCriteria msgSearchCriteria;
	private String userLoadMode;

	private UserSearchDTO userSearchDTO;
	private List<MultiSelectDataParser> multiSelectAll;

	@SuppressWarnings("unchecked")
	public String searchMessages() {
		try {
			Principal principal = request.getUserPrincipal();
			User user = ModuleServiceLocator.getSecurityBD().getUserBasicDetails(principal.getName());
			int startIndex = (page - 1) * PAGE_LENGTH;
			if (msgSearchCriteria == null) {
				msgSearchCriteria = new DashBoardMsgSearchCriteria();
			}
			Page pagedReocrds = ModuleServiceLocator.getCommonServiceBD().getDashBoardMessages(msgSearchCriteria, startIndex,
					PAGE_LENGTH);
			this.page = getCurrentPageNo(pagedReocrds.getStartPosition());
			this.total = getTotalNoOfPages(pagedReocrds.getTotalNoOfRecords());
			this.records = pagedReocrds.getTotalNoOfRecords();

			this.rows = (Collection<Map<String, Object>>) DataUtil.createGridDataDecorateOnly(pagedReocrds.getStartPosition(),
					pagedReocrds.getPageData(), decorateRow(user.getAgentCode()));
			this.setUserIDs(pagedReocrds.getPageData());
		} catch (Exception e) {
			this.handleException(e);
		} finally {
			msgSearchCriteria = null;
		}
		return S2Constants.Result.SUCCESS;
	}

	private RowDecorator<DashboardMessage> decorateRow(String agentCode) {
		final String agentStr = agentCode;
		RowDecorator<DashboardMessage> decorator = new RowDecorator<DashboardMessage>() {

			@SuppressWarnings("unchecked")
			@Override
			public void decorateRow(HashMap<String, Object> row, DashboardMessage record) {
				row.put("dashboardMsgTO.messageID", record.getMessageID());
				row.put("dashboardMsgTO.messageContent", record.getMessage());
				row.put("dashboardMsgTO.messageType", record.getMessageType());
				row.put("dashboardMsgTO.userList", "*".equals(record.getUserList()) ? "All" : record.getUserList());
				row.put("dashboardMsgTO.priority", record.getPriorityID());
				row.put("dashboardMsgTO.fromDate", CalendarUtil.getDateInFormattedString("dd/MM/yyyy", record.getEffectiveFrom()));
				row.put("dashboardMsgTO.toDate", CalendarUtil.getDateInFormattedString("dd/MM/yyyy", record.getEffectiveTo()));
				row.put("dashboardMsgTO.userAgentCode", agentStr);
				row.put("dashboardMsgTO.version", record.getVersion());
				row.put("status", record.getStatus());

				Object[] userData = DashboardMessageHG.convertUserList(record.getUser());
				List<String> userIDs = (List<String>) userData[0];
				if (userIDs.size() > 0)
					row.put("users", CommonUtil.convertToJSON(userIDs));
				String[] agentData = DashboardMessageHG.convertAgentSet(record.getAgents());
				row.put("agents", agentData[0]);
				Object[] objDataAT = DashboardMessageHG.convertAgentTypeSet(record.getAgentType(), "AGENT_TYPE");
				List<MultiSelectDataParser> dataParse_agentType = (List<MultiSelectDataParser>) objDataAT[0];
				if (dataParse_agentType.size() > 0)
					row.put("agentTypes", CommonUtil.convertToJSON(dataParse_agentType));
				Object[] objDataPOS = DashboardMessageHG.convertPOSList(record.getPos(), "POS");
				List<MultiSelectDataParser> dataParse_pos = (List<MultiSelectDataParser>) objDataPOS[0];
				if (dataParse_pos.size() > 0)
					row.put("pos", CommonUtil.convertToJSON(dataParse_pos));

				row.put("agentTypeInclusion", objDataAT[1]);
				row.put("userInclusion", userData[1]);
				row.put("posInclusion", objDataPOS[1]);
				row.put("agentInclusion", agentData[1]);
				Boolean isFronmDateEditable = CalendarUtil.isGreaterThan(record.getEffectiveFrom(),
						CalendarUtil.add(new Date(), 0, 0, 1, 0, 0, 0));
				Boolean isToDateEditable = CalendarUtil.isGreaterThan(record.getEffectiveTo(),
						CalendarUtil.add(new Date(), 0, 0, 1, 0, 0, 0));
				row.put("isFromDateEditable", isFronmDateEditable.toString());
				row.put("isToDateEditable", isToDateEditable.toString());
			}

		};
		return decorator;
	}

	private void setUserIDs(Collection<DashboardMessage> allMessages) {
		Collection<String> userIDSet = new ArrayList<String>();
		userIDtoDisplayMap = new HashMap<String, String>();
		for (DashboardMessage dashboardMessage : allMessages) {
			for (DashbrdMsgUser dashbrdMsgUser : (Set<DashbrdMsgUser>) dashboardMessage.getUser()) {
				userIDSet.add(dashbrdMsgUser.getUserID());
			}
		}
		try {
			Collection<User> userList = ModuleServiceLocator.getSecurityBD().getUsersByID(userIDSet);
			for (User user : userList) {
				userIDtoDisplayMap.put(user.getUserId(), user.getDisplayName());
			}
		} catch (ModuleException e) {
			log.error("Error in Retriving Users");
		}
	}

	public int getCurrentPageNo(int startPosition) {
		int currentPageNo = 1;
		if (PAGE_LENGTH != 0) {
			currentPageNo = startPosition / PAGE_LENGTH + 1;
		}
		return currentPageNo;
	}

	public int getTotalNoOfPages(int totoalNoOfRecords) {
		int totalNoOfPages = 1;
		if (PAGE_LENGTH != 0) {
			totalNoOfPages = (int) totoalNoOfRecords / PAGE_LENGTH;
			if (totoalNoOfRecords % PAGE_LENGTH > 0)
				totalNoOfPages++;
		}
		return totalNoOfPages;
	}

	public String addEditMessage() {
		try {
			ModuleServiceLocator.getCommonServiceBD().saveDashboardMsg(dashboardMsgTO);
			this.setDefaultSuccessMessage();
		} catch (Exception e) {
			log.error(e);
			this.handleException(e);
		} finally {
			dashboardMsgTO = null;
		}
		return S2Constants.Result.SUCCESS;
	}

	@SuppressWarnings("unchecked")
	public String searchUserGridData() {
		try {
			if (userSearchDTO != null) {
				userSearchDTO.setReportingAgentCodes(new ArrayList());
				userSearchDTO.setStatus("ACT");
				int startIndex = (userPage - 1) * PAGE_LENGTH;
				Page pageUser = ModuleServiceLocator.getSecurityBD().searchUsers(userSearchDTO, startIndex, PAGE_LENGTH);
				userPage = getCurrentPageNo(pageUser.getStartPosition());
				userTotal = getTotalNoOfPages(pageUser.getTotalNoOfRecords());
				userRecords = pageUser.getTotalNoOfRecords();
				userRows = (Collection<Map<String, Object>>) DataUtil.createGridDataDecorateOnly(pageUser.getStartPosition(),
						pageUser.getPageData(), decorateRow());
			}
		} catch (Exception e) {
			this.handleException(e);
		} finally {
			userSearchDTO = null;
		}
		return S2Constants.Result.SUCCESS;
	}

	private RowDecorator<User> decorateRow() {

		RowDecorator<User> decorator = new RowDecorator<User>() {

			@Override
			public void decorateRow(HashMap<String, Object> row, User record) {
				row.put("pagedUser.userId", record.getUserId());
				row.put("pagedUser.displayName", record.getDisplayName());
				row.put("pagedUser.agentCode", record.getAgentCode());
			}
		};
		return decorator;
	}

	public String deleteMessage() {
		return S2Constants.Result.SUCCESS;

	}

	public Collection<Map<String, Object>> getRows() {
		return rows;
	}

	public void setRows(Collection<Map<String, Object>> rows) {
		this.rows = rows;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public DashboardMsgTO getDashboardMsgTO() {
		return dashboardMsgTO;
	}

	public void setDashboardMsgTO(DashboardMsgTO dashboardMsgTO) {
		this.dashboardMsgTO = dashboardMsgTO;
	}

	public DashBoardMsgSearchCriteria getMsgSearchCriteria() {
		return msgSearchCriteria;
	}

	public void setMsgSearchCriteria(DashBoardMsgSearchCriteria msgSearchCriteria) {
		this.msgSearchCriteria = msgSearchCriteria;
	}

	public List<MultiSelectDataParser> getMultiSelectAll() {
		return multiSelectAll;
	}

	public void setMultiSelectAll(List<MultiSelectDataParser> multiSelectAll) {
		this.multiSelectAll = multiSelectAll;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getUserLoadMode() {
		return userLoadMode;
	}

	public void setUserLoadMode(String userLoadMode) {
		this.userLoadMode = userLoadMode;
	}

	public UserSearchDTO getUserSearchDTO() {
		return userSearchDTO;
	}

	public void setUserSearchDTO(UserSearchDTO userSearchDTO) {
		this.userSearchDTO = userSearchDTO;
	}

	public Collection<Map<String, Object>> getUserRows() {
		return userRows;
	}

	public void setUserRows(Collection<Map<String, Object>> userRows) {
		this.userRows = userRows;
	}

	public int getUserPage() {
		return userPage;
	}

	public void setUserPage(int userPage) {
		this.userPage = userPage;
	}

	public int getUserTotal() {
		return userTotal;
	}

	public void setUserTotal(int userTotal) {
		this.userTotal = userTotal;
	}

	public int getUserRecords() {
		return userRecords;
	}

	public void setUserRecords(int userRecords) {
		this.userRecords = userRecords;
	}

	public Map<String, String> getUserIDtoDisplayMap() {
		return userIDtoDisplayMap;
	}

	public void setUserIDtoDisplayMap(Map<String, String> userIDtoDisplayMap) {
		this.userIDtoDisplayMap = userIDtoDisplayMap;
	}

}
