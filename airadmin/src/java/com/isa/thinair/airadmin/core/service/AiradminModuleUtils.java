package com.isa.thinair.airadmin.core.service;

import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.platform.api.IModule;
import com.isa.thinair.platform.api.LookupServiceFactory;
import com.isa.thinair.platform.api.constants.PlatformBeanUrls;

/**
 * @author srikantha
 * 
 */
public class AiradminModuleUtils {

	public static IModule lookupModule(String moduleName) {
		return LookupServiceFactory.getInstance().getModule(moduleName);
	}

	public static GlobalConfig getGlobalConfig() {
		return (GlobalConfig) LookupServiceFactory.getInstance().getBean(PlatformBeanUrls.Commons.GLOBAL_CONFIG_BEAN);
	}

	public static AiradminModuleConfig getConfig() {
		AiradminModuleConfig config = (AiradminModuleConfig) LookupServiceFactory.getInstance().getBean(
				"isa:base://modules/airadmin?id=airadminModuleConfig");
		return config;
	}

}
