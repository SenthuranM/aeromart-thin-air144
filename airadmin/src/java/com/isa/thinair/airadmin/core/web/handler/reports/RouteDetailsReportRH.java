package com.isa.thinair.airadmin.core.web.handler.reports;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airmaster.api.model.RouteInfo;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

public class RouteDetailsReportRH extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(RouteDetailsReportRH.class);

	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	public static final String PARAM_VERSION = "hdnVersion";

	public static final String PARAM_ORIGIN = "selOrigin";
	public static final String PARAM_DESTINATION = "selDestination";
	public static final String PARAM_VIA_1 = "selVia1";
	public static final String PARAM_VIA_2 = "selVia2";
	public static final String PARAM_VIA_3 = "selVia3";
	public static final String PARAM_VIA_4 = "selVia4";
	public static final String PARAM_STATUS = "selStatus";

	public static final String PARAM_SORT_BY_COLUMN = "OND_CODE";
	public static final String PARAM_REPORT_NUM_FORMAT = "radRptNumFormat";
	public static final String PARAM_REPORT_TYPE = "radReportOption";

	public static final String PARAM_MODE = "hdnMode";
	public static final String PARAM_ACTION = "hdnAction";

	private static void setIntSuccess(HttpServletRequest request, int value) {
		setAttribInRequest(request, "intSuccess", new Integer(value));
	}

	private static int getIntSuccess(HttpServletRequest request) {
		Integer returnValue = (Integer) getAttribInRequest(request, "intSuccess");
		if (returnValue != null)
			return returnValue.intValue();
		else
			return -1;
	}

	private static void setExceptionOccured(HttpServletRequest request, boolean value) {
		setAttribInRequest(request, "isExceptionOccured", new Boolean(value));
	}

	private static boolean isExceptionOccured(HttpServletRequest request) {
		return ((Boolean) getAttribInRequest(request, "isExceptionOccured")).booleanValue();
	}

	/**
	 * Main Execute Method for Route Details Action & set the Success int 0-Not Applicable, 1-Success, 2-Fail
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {
		String strHdnMode = request.getParameter(PARAM_MODE);
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;

		try {
			setDisplayData(request);
			log.debug("RouteDetailsReportRH setDisplayData() SUCCESS");
			setIntSuccess(request, 1);
			setExceptionOccured(request, false);
		} catch (Exception ex) {
			log.error("RouteDetailsReportRH setDisplayData() FAILED " + ex.getMessage());
			setIntSuccess(request, 2);
			setExceptionOccured(request, true);
		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				log.debug("RouteDetailsReportRH setReportView Success");
				setIntSuccess(request, 1);
				setExceptionOccured(request, false);
				return null;
			}

		} catch (ModuleException ex) {
			saveMessage(request, ex.getMessageString(), WebConstants.MSG_ERROR);
			log.error("RouteDetailsReportRH setReportView Failed " + ex.getMessageString());
			setIntSuccess(request, 2);
			setExceptionOccured(request, true);
		}

		return forward;
	}

	private static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);
		String reportType = request.getParameter(PARAM_REPORT_TYPE);

		String strReportId = "UC_REPM_080";
		String reportTemplate = "RouteListReport.jasper";

		String strLogo = AppSysParamsUtil.getReportLogo(false);
		String strPath = "../../images/" + AppSysParamsUtil.getReportLogo(true);
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";

		// read values
		String origin = request.getParameter(PARAM_ORIGIN);
		String destination = request.getParameter(PARAM_DESTINATION);
		String viaPoint1 = request.getParameter(PARAM_VIA_1);
		String viaPoint2 = request.getParameter(PARAM_VIA_2);
		String viaPoint3 = request.getParameter(PARAM_VIA_3);
		String viaPoint4 = request.getParameter(PARAM_VIA_4);
		String selStatus = request.getParameter(PARAM_STATUS);

		if (log.isDebugEnabled()) {
			log.debug("Arguments : origin" + origin + "destination:" + destination + "viaPoint1" + viaPoint1 + "viaPoint2" + viaPoint2
					+ "viaPoint3" + viaPoint3 + "viaPoint4" + viaPoint4 + "selStatus" + selStatus);
		}

		try {
			ReportsSearchCriteria search = new ReportsSearchCriteria();

			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs(WebConstants.REPORT_REF,
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));

			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}

			search.setOriginAirport(origin);
			search.setDestinationAirport(destination);
			// set viapoints
			List<String> visPoints = getViaPointsList(viaPoint1, viaPoint2, viaPoint3, viaPoint4);
			search.setViaPoints(visPoints);
			search.setStatus(selStatus);
			// sort by
			search.setSortByColumnName(PARAM_SORT_BY_COLUMN);

			ResultSet resultSet = ModuleServiceLocator.getDataExtractionBD().getRouteDetailData(search);

			List<RouteInfo> RouteList = createRouteList(resultSet);

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("ID", strReportId);
			parameters.put("CARRIER", strCarrier);

			// To provide Report Format Options
			String reportFormat = request.getParameter(PARAM_REPORT_NUM_FORMAT);
			ReportsHTMLGenerator.setPreferedReportFormat(reportFormat, parameters);

			if (reportType.trim().equals(WebConstants.REPORT_HTML)) {
				parameters.put("IMG", strPath);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport(WebConstants.REPORT_REF, parameters, RouteList,
						null, null, response);
			} else if (reportType.trim().equals(WebConstants.REPORT_PDF)) {
				response.reset();
				response.addHeader("Content-Disposition", "filename=Route_List_Report.pdf");
				strPath = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG", strPath);
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport(WebConstants.REPORT_REF, parameters, RouteList,
						response);
			} else if (reportType.trim().equals(WebConstants.REPORT_EXCEL)) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=Route_List_Report.xls");
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport(WebConstants.REPORT_REF, parameters, RouteList,
						response);
			} else if (reportType.trim().equals("CSV")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=Route_List_Report.csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport(WebConstants.REPORT_REF, parameters, RouteList,
						response);
			}

		} catch (Exception ex) {
			log.error("Route List Report Generation Failed: ", ex);
		}

	}

	private static List<String> getViaPointsList(String via1, String via2, String via3, String via4) {

		List<String> viaPoints = new ArrayList<String>();

		int maxVar = 0;

		if (via4 != null && !via4.equals("")) {
			maxVar = 4;
		} else if (via3 != null && !via3.equals("")) {
			maxVar = 3;
		} else if (via2 != null && !via2.equals("")) {
			maxVar = 2;
		} else if (via1 != null && !via1.equals("")) {
			maxVar = 1;
		}

		if (maxVar > 0) {
			if (maxVar >= 1) {
				viaPoints.add(via1.trim());
			}
			if (maxVar >= 2) {
				viaPoints.add(via2.trim());
			}
			if (maxVar >= 3) {
				viaPoints.add(via3.trim());
			}
			if (maxVar >= 4) {
				viaPoints.add(via4.trim());
			}
		}
		return viaPoints;

	}

	private static List<RouteInfo> createRouteList(ResultSet routeResultSet) {

		List<RouteInfo> routesList = new ArrayList<RouteInfo>();

		try {
			while (routeResultSet.next()) {
				RouteInfo route = new RouteInfo();
				route.setRouteCode(routeResultSet.getString("OND_CODE"));
				route.setDuration(routeResultSet.getInt("DURATION"));
				route.setDurationTolerance(routeResultSet.getBigDecimal("DURATION_TOLERANCE"));
				route.setDistance(routeResultSet.getBigDecimal("DISTANCE"));
				route.setIsLCCPubilshed(routeResultSet.getString("IS_LCC_PUBLISHED"));
				route.setStatus(routeResultSet.getString("STATUS"));
				route.setDirectFlg(routeResultSet.getString("DIRECT_FLAG"));
				route.setNoOfTransits(routeResultSet.getInt("NO_OF_TRANSITS"));
				route.setAncilaryRemindersEnabled(routeResultSet.getString("ENABLE_ANCI_REMINDERS"));

				routesList.add(route);

			}

		} catch (SQLException exSql) {
			log.error("Error occurred while iterating the route list :" + exSql);
		}

		return routesList;

	}

	/**
	 * Sets the Display Data to the ROUTE INFO page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static void setDisplayData(HttpServletRequest request) throws ModuleException {
		setClientErrors(request);
		setAirportHtml(request);
		setLiveStatus(request);
	}

	/**
	 * Sets the Client Validations for the ROUTE Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Online Active Airport List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 * 
	 */
	public static void setAirportHtml(HttpServletRequest request) throws ModuleException {
		String strViaPointsList = SelectListGenerator.createActiveAirportCodeList();
		request.setAttribute(WebConstants.REQ_HTML_VIAPOINT_SELECT_LIST, strViaPointsList);
	}
	
	/**
	 * Sets the report medium to the request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleExceptions
	 */
	private static void setLiveStatus(HttpServletRequest request) throws ModuleException {
		String strLive = request.getParameter(WebConstants.REP_HDN_LIVE);
		if (strLive != null)
			request.setAttribute(WebConstants.REP_SET_LIVE, strLive);

	}
}
