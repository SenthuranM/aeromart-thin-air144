/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airadmin.core.web.handler.reports;

import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants.ReportFormatType;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;


/**
 * @author Chamindap
 * 
 */
public class MonitorPerformanceOfSalesStaffRH extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(MonitorPerformanceOfSalesStaffRH.class);

	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	/**
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {

		String forward = AdminStrutsConstants.AdminAction.SUCCESS;
		String strHdnMode = request.getParameter("hdnMode");

		try {
			setDisplayData(request);
			log.debug("MonitorPerformanceOfSalesStaffRH SETDISPLAYDATA() SUCCESS");
		} catch (Exception e) {
			log.error("MonitorPerformanceOfSalesStaffRH SETDISPLAYDATA() FAILED " + e.getMessage());
		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				log.error("MonitorPerformanceOfSalesStaffRH setReportView Success");
				return null;
			} else {
				log.error("MonitorPerformanceOfSalesStaffRH setReportView not selected");
			}

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("MonitorPerformanceOfSalesStaffRH setReportView Failed " + e.getMessageString());
		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_DETAIL)) {
				setDetailReportView(request, response);
				log.error("MonitorPerformanceOfSalesStaffRH setDetailReportView Success");
				return null;
			} else {
				log.error("MonitorPerformanceOfSalesStaffRH setDetailReportView not selected");
			}
		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("CustomerTravelHistoryRequestHandler setReportView Failed " + e.getMessageString());
		}

		return forward;
	}

	/**
	 * 
	 * @param request
	 * @throws Exception
	 */
	public static void setDisplayData(HttpServletRequest request) throws Exception {
		setClientErrors(request);
		setAgentTypes(request);
		setUserMultiSelectList(request);
		setAllAgents(request);
		setLiveStatus(request);
		setReportingPeriod(request);
		setBookingTypesMultiSelect(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);

	}

	protected static void setBookingTypesMultiSelect(HttpServletRequest request) throws ModuleException {

		String strList = ReportsHTMLGenerator.createMultiSelect();
		request.setAttribute(WebConstants.REQ_BOOKING_TYPE_LIST, strList);
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setUserMultiSelectList(HttpServletRequest request) throws ModuleException {
		String strAgentType = "";

		if (request.getParameter("selAgencies") != null) {
			strAgentType = request.getParameter("selAgencies");
		}
		String strList = ReportsHTMLGenerator.createUserMultiSelect(strAgentType);
		request.setAttribute(WebConstants.REQ_AGENT_USER_LIST, strList);
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setAllAgents(HttpServletRequest request) throws ModuleException {
		String strList = SelectListGenerator.createAgents();
		request.setAttribute(WebConstants.REQ_HTML_AGENT_LIST, strList);
	}

	/**
	 * 
	 * @param request
	 */
	public static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setAgentTypes(HttpServletRequest request) throws ModuleException {
		String strATList = SelectListGenerator.createAgentTypeList_SG();
		request.setAttribute(WebConstants.REQ_HTML_AGENTTYPE_LIST, strATList);
	}

	/**
	 * Sets the report medium to the request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleExceptions
	 */
	private static void setLiveStatus(HttpServletRequest request) throws ModuleException {
		String strLive = request.getParameter(WebConstants.REP_HDN_LIVE);
		if (strLive != null)
			request.setAttribute(WebConstants.REP_SET_LIVE, strLive);

	}

	/**
	 * Setting the reporting period to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setReportingPeriod(HttpServletRequest request) throws ModuleException {
		int period = new Integer(globalConfig.getBizParam(SystemParamKeys.REPORTING_PERIOD)).intValue();
		request.setAttribute(WebConstants.REP_RPT_START_DAY, ReportsHTMLGenerator.getReportingPeriod(period));
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		ResultSet resultSet = null;
		Calendar cal = null;
		String formattedDate = null;
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String value = request.getParameter("radReportOption");
		String userIds = request.getParameter("hdnUserCode");
		String paymentTypes = request.getParameter("hdnBookingPaymentType");
		String period = request.getParameter("radReportPeriod");
		String id = "UC_REPM_010";
		String reportTemplate = "PerformanceOfSalesStaff.jasper";
		String strLogo = AppSysParamsUtil.getReportLogo(false);
		String image = "../../images/" +  AppSysParamsUtil.getReportLogo(true);
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String strBase = globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY);
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);
		Vector<String> usersCol = new Vector<String>();
		Vector<String> paymentTypeCol = new Vector<String>();
		String users[] = userIds.split(",");
		String paymentTypeIds[] = paymentTypes.split(",");
		Map<String, Object> parameters = new HashMap<String, Object>();

		for (int i = 0; i < users.length; i++) {
			usersCol.add(users[i]);
		}

		for (int i = 0; i < paymentTypeIds.length; i++) {
			paymentTypeCol.add(paymentTypeIds[i]);
		}

		try {
			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));

			ReportsSearchCriteria search = new ReportsSearchCriteria();

			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}

			if (period.trim().equals("Today")) {
				cal = new GregorianCalendar();

				formattedDate = formatter.format(cal.getTime());
				search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(formattedDate));
				search.setDateRangeTo(ReportsHTMLGenerator.convertDate(formattedDate));
				parameters.put("FROM_DATE", formattedDate);
				parameters.put("TO_DATE", formattedDate);
			} else if (period.trim().equals("ThisWeek")) {
				cal = new GregorianCalendar();

				cal.set(Calendar.DAY_OF_WEEK, 1);
				formattedDate = formatter.format(cal.getTime());
				parameters.put("FROM_DATE", formattedDate);
				search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(formattedDate));

				cal.set(Calendar.DAY_OF_WEEK, 7);
				formattedDate = formatter.format(cal.getTime());
				search.setDateRangeTo(ReportsHTMLGenerator.convertDate(formattedDate));
				parameters.put("TO_DATE", formattedDate);

			} else if (period.trim().equals("ThisMonth")) {
				cal = new GregorianCalendar();

				cal.set(Calendar.DAY_OF_MONTH, 1);
				formattedDate = formatter.format(cal.getTime());
				parameters.put("FROM_DATE", formattedDate);
				search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(formattedDate));

				cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
				formattedDate = formatter.format(cal.getTime());
				parameters.put("TO_DATE", formattedDate);
				search.setDateRangeTo(ReportsHTMLGenerator.convertDate(formattedDate));
			} else if (period.trim().equals("ThisYear")) {
				cal = new GregorianCalendar();

				cal.set(Calendar.DAY_OF_YEAR, 1);
				formattedDate = formatter.format(cal.getTime());
				parameters.put("FROM_DATE", formattedDate);
				search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(formattedDate));

				cal.set(Calendar.DAY_OF_YEAR, cal.getActualMaximum(Calendar.DAY_OF_YEAR));
				formattedDate = formatter.format(cal.getTime());
				parameters.put("TO_DATE", formattedDate);
				search.setDateRangeTo(ReportsHTMLGenerator.convertDate(formattedDate));
			} else if (period.trim().equals("Other")) {
				if (fromDate != null && !fromDate.equals("")) {
					search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate));
				}
				if (toDate != null && !toDate.equals("")) {
					search.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate));
				}
				parameters.put("FROM_DATE", fromDate);
				parameters.put("TO_DATE", toDate);
			}

			search.setUsers(usersCol);

			search.setPaymentTypes(paymentTypeCol);

			resultSet = ModuleServiceLocator.getDataExtractionBD().getPerformanceOfSalesStaffSummaryData(search);

			parameters.put("OPTION", value);
			parameters.put("ID", id);
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
			parameters.put("AMT_1", "(" + strBase + ")");
			parameters.put("REPORT_MEDIUM", strlive);
			parameters.put("PAYMENT_TYPES", paymentTypes);

			if (AppSysParamsUtil.isPromoCodeEnabled()) {
				parameters.put("SHW_DISCOUNT", true);
			} else {
				parameters.put("SHW_DISCOUNT", false);
			}
			
			parameters.put("DETAIL_ABS_TARGET", "");

			// To provide Report Format Options
			String reportNumFormat = request.getParameter("radRptNumFormat");
			ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);
			String reportName = "PerformanaceOfSalesStaffDetail";

			if (value.trim().equals("HTML")) {
				parameters.put("IMG", image);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null, null,
						response);
			} else if (value.trim().equals("PDF")) {
				response.reset();
				response.addHeader("Content-Disposition", 
						String.format("filename=%s" + ReportFormatType.PDF_FORMAT, reportName));
				image = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG", image);
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("EXCEL")) {
				response.reset();
				response.addHeader("Content-Disposition",
						String.format("attachment;filename=%s" + ReportFormatType.EXCEL_FORMAT, reportName));
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("CSV")) {
				response.reset();
				response.addHeader("Content-Disposition",
						String.format("attachment;filename=%s" + ReportFormatType.CSV_FORMAT, reportName));
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);

			}
		} catch (Exception e) {
			log.error(e);
		}
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setDetailReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {
		ResultSet resultSet = null;

		String userId = request.getParameter("login");
		String value = request.getParameter("option");
		String fromDate = request.getParameter("from");
		String toDate = request.getParameter("to");
		String userName = request.getParameter("userName");
		String strAgentCode = request.getParameter("agentCode");
		String id = "UC_REPM_010";
		String reportTemplate = "PerformanaceOfSalesStaffDetail.jasper";
		String strLogo = AppSysParamsUtil.getReportLogo(false);
		String image = "../../images/" +  AppSysParamsUtil.getReportLogo(true);
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String strBase = globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY);
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);
		String paymentTypes = request.getParameter("hdnBookingPaymentType");
		String paymentTypeIds[] = paymentTypes.split(",");
		Vector<String> paymentTypeCol = new Vector<String>();
		try {
			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));

			ReportsSearchCriteria search = new ReportsSearchCriteria();
			search.setUserId(userId);
			search.setAgentCode(strAgentCode);
			search.setDateFrom(ReportsHTMLGenerator.toDateType(fromDate, true));
			search.setDateTo(ReportsHTMLGenerator.toDateType(toDate, false));
			for (int i = 0; i < paymentTypeIds.length; i++) {
				paymentTypeCol.add(paymentTypeIds[i]);
			}
			search.setPaymentTypes(paymentTypeCol);
			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}

			resultSet = ModuleServiceLocator.getDataExtractionBD().getPerformanceOfSalesStaffDetailData(search);

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("FROM_DATE", fromDate);
			parameters.put("TO_DATE", toDate);
			parameters.put("ID", id);
			parameters.put("LOGIN_ID", userId);
			parameters.put("USER_NAME", userName);
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
			parameters.put("AMT_1", "(" + strBase + ")");

			if (AppSysParamsUtil.isPromoCodeEnabled()) {
				parameters.put("SHW_DISCOUNT", true);
			} else {
				parameters.put("SHW_DISCOUNT", false);
			}

			// To provide Report Format Options
			String reportNumFormat = request.getParameter("radRptNumFormat");
			ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);
			String reportName = "PerformanaceOfSalesStaffDetail";

			if (value.trim().equals("HTML")) {
				parameters.put("IMG", image);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null, null,
						response);
			} else if (value.trim().equals("PDF")) {
				response.reset();
				response.addHeader("Content-Disposition",
						String.format("filename=%s" + ReportFormatType.PDF_FORMAT, reportName));
				image = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG", image);
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("EXCEL")) {
				response.reset();
				response.addHeader("Content-Disposition",
						String.format("attachment;filename=%s" + ReportFormatType.EXCEL_FORMAT, reportName));
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("CSV")) {
				response.reset();
				response.addHeader("Content-Disposition",
						String.format("attachment;filename=%s" + ReportFormatType.CSV_FORMAT, reportName));
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);
			}
		} catch (Exception e) {
			log.error(e);
		}
	}
}