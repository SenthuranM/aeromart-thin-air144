/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airadmin.core.web.handler.reports;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;

public class LccAndDryCollectionRH extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(LccAndDryCollectionRH.class);

	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	private static enum CollectionType {
		PAYABLE, RECEIVABLE
	}

	/**
	 * @param request
	 * @return
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {

		String strHdnMode = request.getParameter("hdnMode");
		String forward = WebConstants.ACTION_FORWARD_SUCCESS;

		try {
			setDisplayData(request);
			log.debug("LccCollectionRH success");
		} catch (Exception e) {
			forward = AdminStrutsConstants.AdminAction.ERROR;
			log.error("CustomerTravelHistoryRequestHandler execute()" + e.getMessage());
		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				log.error("LccCollectionRH setReportView Success");
				return null;
			} else {
				log.error("LccCollectionRH setReportView not selected");
			}

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("LccCollectionRH setReportView Failed " + e.getMessageString());
		}
		return forward;

	}

	/**
	 * 
	 * @param request
	 * @throws Exception
	 */
	public static void setDisplayData(HttpServletRequest request) throws Exception {
		setClientErrors(request);
		setLiveStatus(request);
		setCollcetions(request);
		setReportingPeriod(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);
	}

	private static void setCollcetions(HttpServletRequest request) {
		StringBuffer sb = new StringBuffer();
		for (CollectionType collectionType : CollectionType.values()) {
			sb.append(String.format("<option value='%s'>%s</option>", collectionType, collectionType));
		}
		request.setAttribute(WebConstants.REP_AIRLINE_WISE_COLLECTION_TYPE, sb.toString());
	}

	/**
	 * 
	 * @param request
	 */
	private static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);
			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the report medium to the request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleExceptions
	 */
	private static void setLiveStatus(HttpServletRequest request) throws ModuleException {
		String strLive = request.getParameter(WebConstants.REP_HDN_LIVE);
		if (strLive != null)
			request.setAttribute(WebConstants.REP_SET_LIVE, strLive);

	}

	/**
	 * Setting the reporting period to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setReportingPeriod(HttpServletRequest request) throws ModuleException {
		int period = new Integer(globalConfig.getBizParam(SystemParamKeys.REPORTING_PERIOD)).intValue();
		request.setAttribute(WebConstants.REP_RPT_START_DAY, ReportsHTMLGenerator.getReportingPeriod(period));
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	private static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		ResultSet resultSet = null;
		String value = request.getParameter("radReportOption");
		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String strColOption = request.getParameter("selColType");
		String strLogo = AppSysParamsUtil.getReportLogo(false);
		String image = "../../images/" +  AppSysParamsUtil.getReportLogo(true);
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String baseCurrency = AppSysParamsUtil.getBaseCurrency();
		String id = "UC_REPM_026";
		String reportTemplate = null;
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);
		String reportView = request.getParameter("hdnRptType");
		String paymentMode = request.getParameter("paymentMode");
		String dryCarrierCode = request.getParameter("dryCarrierCode");
		String currencyCode = request.getParameter("currencyCode");
		String agentCode = request.getParameter("agentCode");
		String agentName = request.getParameter("agentName");
		String stationCode = request.getParameter("stationCode");
		String strView = request.getParameter("chkNewview");
		boolean isNewView = (strView != null && strView.equalsIgnoreCase("on")) ? true : false;

		List<Integer> arrNominalcodes = new ArrayList<Integer>();
		List<Integer> arrExtNominalcodes = new ArrayList<Integer>();

		Collection<String> interlinedCarriers = globalConfig.getInterlineCarriers();

		try {

			ReportsSearchCriteria search = new ReportsSearchCriteria();

			if (reportView != null && reportView.trim().equals("DETAIL")) { // detail
				// report
				if (strColOption != null) { // lcc-report
					CollectionType reportType = CollectionType.valueOf(strColOption);
					if (reportType.equals(CollectionType.PAYABLE)) {
						reportTemplate = "LccAgentwiseCollectionDetailReport.jasper";

						arrNominalcodes.addAll(getNCForPaymentType(paymentMode));

						search.setReportOption("DETAIL_PAYABLE");
						search.setCarrierCode(dryCarrierCode);
						search.setCarrierCodes(interlinedCarriers);
						search.setAgentCode(agentCode);
					} else if (reportType.equals(CollectionType.RECEIVABLE)) {
						reportTemplate = "LccAgentwiseCollectionDetailReport.jasper";

						arrNominalcodes.addAll(getNCForPaymentType(paymentMode));

						search.setReportOption("DETAIL_RECEIVABLE");
						search.setCarrierCode(dryCarrierCode);
						search.setCarrierCodes(interlinedCarriers);
						search.setAgentCode(agentCode);
					}
				}

			} else { // summary report
				if (strColOption != null) { // lcc-report
					if (strColOption.equals(CollectionType.PAYABLE.toString())) {
						reportTemplate = "LccAgentwiseCollectionSummaryReport.jasper";

						arrNominalcodes.addAll(ReservationTnxNominalCode.getPaymentAndRefundNominalCodes());
						arrNominalcodes.add(getCreditShuffelNC());

						search.setReportOption("SUMMARY_PAYABLE");
						search.setCarrierCodes(interlinedCarriers);
					} else if (strColOption.equals(CollectionType.RECEIVABLE.toString())) {
						reportTemplate = "LccAgentwiseCollectionSummaryReport.jasper";

						arrNominalcodes.addAll(ReservationTnxNominalCode.getPaymentAndRefundNominalCodes());
						arrNominalcodes.add(getCreditShuffelNC());

						search.setReportOption("SUMMARY_RECEIVABLE");
						search.setCarrierCodes(interlinedCarriers);
					}
				}
			}

			// set the nominal code and external payment nominal code
			search.setNominalCodes(arrNominalcodes);
			search.setExtPaymentNominalCodes(arrExtNominalcodes);
			search.setReportViewNew(isNewView);

			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
					ReportsHTMLGenerator.getReportTemplate(reportTemplate));

			if (fromDate != null && !fromDate.equals("")) {
				search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate).concat(" 00:00:00"));
			}
			if (toDate != null && !toDate.equals("")) {
				search.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate).concat(" 23:59:59"));
			}
			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}
			resultSet = ModuleServiceLocator.getDataExtractionBD().getLccAndDryCollection(search);

			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("FROM_DATE", fromDate);
			parameters.put("TO_DATE", toDate);
			parameters.put("OPTION", value);
			parameters.put("ID", id);
			parameters.put("COLLECTION_TYPE", strColOption);
			parameters.put("BASE_CURRENCY", baseCurrency);
			parameters.put("AGENT_CODE", agentCode);
			parameters.put("AGENT_NAME", agentName);
			parameters.put("STATION_CODE", stationCode);
			parameters.put("PAY_CARRIER", dryCarrierCode);
			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
			if (isNewView) {
				parameters.put("VIEW", "ON");
			}

			// To provide Report Format Options
			String reportNumFormat = request.getParameter("radRptNumFormat");
			ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

			if (value.trim().equals("HTML")) {
				parameters.put("IMG", image);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet,
						null, null, response);
			} else if (value.trim().equals("PDF")) {
				response.reset();
				response.addHeader("Content-Disposition", String.format("filename=%s.pdf", reportTemplate));
				image = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG", image);
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet,
						response);
			} else if (value.trim().equals("EXCEL")) {
				response.reset();
				response.addHeader("Content-Disposition", String.format("attachment;filename=%s.xls", reportTemplate));
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet,
						response);
			} else if (value.trim().equals("CSV")) {
				response.reset();
				response.addHeader("Content-Disposition", String.format("attachment;filename=%s.csv", reportTemplate));
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet,
						response);
			}
		} catch (Exception e) {
			log.error(e);
		}
	}

	private static List<Integer> getNCForPaymentType(String paymentMode) {
		List<Integer> nc = new ArrayList<Integer>();
		if (paymentMode.equals("ON_ACCOUNT")) {
			nc.addAll(ReservationTnxNominalCode.getOnAccountTypeNominalCodes());
		} else if (paymentMode.equals("CASH")) {
			nc.addAll(ReservationTnxNominalCode.getCashTypeNominalCodes());
		} else if (paymentMode.equals("VISA")) {
			nc.addAll(ReservationTnxNominalCode.getVisaTypeNominalCodes());
		} else if (paymentMode.equals("MASTER")) {
			nc.addAll(ReservationTnxNominalCode.getMasterTypeNominalCodes());
		} else if (paymentMode.equals("AMEX")) {
			nc.addAll(ReservationTnxNominalCode.getAmexTypeNominalCodes());
		} else if (paymentMode.equals("DINERS")) {
			nc.addAll(ReservationTnxNominalCode.getDinersTypeNominalCodes());
		} else if (paymentMode.equals("GENERIC")) {
			nc.addAll(ReservationTnxNominalCode.getGenericTypeNominalCodes());
		} else if (paymentMode.equals("CMI")) {
			nc.addAll(ReservationTnxNominalCode.getCMITypeNominalCodes());
		} else if (paymentMode.equals("CREDIT")) {
			/*
			 * if(reportType.equals(CollectionType.PAYABLE)){
			 * nc.add(ReservationTnxNominalCode.CREDIT_CF.getCode()); }else
			 * if(reportType.equals(CollectionType.RECEIVABLE)){
			 * nc.add(ReservationTnxNominalCode.CREDIT_BF.getCode()); }
			 */
			nc.addAll(ReservationTnxNominalCode.getCreditTypeNominalCodes());
		} else if (paymentMode.equals("AIRLINE_CREDIT")) {
			// //this is to be divided among actual MOP when data migration . No
			// longer use with lcc Dry P2
			nc.add(Integer.valueOf(0));
		}
		return nc;
	}

	/**
	 * This is to be divided among actual MOP later when backend is supported.
	 * 
	 * @return
	 */
	private static Integer getCreditShuffelNC() {
		return Integer.valueOf(0);
	}

}