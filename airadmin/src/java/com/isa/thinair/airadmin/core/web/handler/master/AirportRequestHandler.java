package com.isa.thinair.airadmin.core.web.handler.master;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.master.AirportHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airmaster.api.model.Airport;
import com.isa.thinair.airmaster.api.model.AirportCheckInTime;
import com.isa.thinair.airmaster.api.model.AirportForDisplay;
import com.isa.thinair.airmaster.api.model.MealNotifyTimings;
import com.isa.thinair.airmaster.api.model.Station;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author srikantha
 * 
 */

public final class AirportRequestHandler extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(AirportRequestHandler.class);

	private static AiradminConfig airadminConfig = new AiradminConfig();
	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();

	private static final String PARAM_GOSHOWAGENT = "hdnGoShowAgent";
	private static final String PARAM_VERSION = "hdnVersion";
	private static final String PARAM_GRIDROW = "hdnGridRow";
	private static final String PARAM_MODE = "hdnMode";
	private static final String PARAM_ONLINESTATUS = "radOnline1";
	private static final String PARAM_OFFLINESTATUS = "radOnline2";
	private static final String PARAM_AIRPORTCODE = "txtAirportCode";
	private static final String PARAM_AIRPORTNAME = "txtAirportName";
	private static final String PARAM_AIRPORTLIST_IBE = "hdnAirportForDisplay";
	private static final String PARAM_CONTACT = "txtContact";
	private static final String PARAM_PHONE = "txtPhone";
	private static final String PARAM_FAX = "txtFax";
	private static final String PARAM_REMARKS = "txtaRemarks";
	private static final String PARAM_STARTS = "lblStarts";
	private static final String PARAM_ENDS = "lblEnds";
	private static final String PARAM_DST = "chkDst";
	private static final String PARAM_ACTIVE = "chkActive";
	private static final String PARAM_STATIONCODE = "selFormStation";
	private static final String PARAM_CITY_ID = "selFormCity";
	private static final String PARAM_CLOSESTAIRPOST = "selClosestAirport";
	private static final String PARAM_SEL_GMTOFFSETACTION = "selGmtOffSetAction";
	private static final String PARAM_TXT_GMTOFFSETACTION = "txtGmtOffSetAction";
	private static final String PARAM_MINSTOPOVERTIME = "txtMinStopOvrTime";
	private static final String PARAM_LATITUDE = "rndLatitude";
	private static final String PARAM_TXT_LATITUDE1 = "txtLatitude1";
	private static final String PARAM_TXT_LATITUDE2 = "txtLatitude2";
	private static final String PARAM_TXT_LATITUDE3 = "txtLatitude3";
	private static final String PARAM_LONGTITUTE = "rndLongitude";
	private static final String PARAM_TXT_LONGTITUTE1 = "txtLongitude1";
	private static final String PARAM_TXT_LONGTITUTE2 = "txtLongitude2";
	private static final String PARAM_TXT_LONGTITUTE3 = "txtLongitude3";

	private static final String PARAM_IBEVISIBILITY = "chkIBEVisibililty";
	private static final String PARAM_XBEVISIBILITY = "chkXBEVisibililty";
	private static final String PARAM_CHECKIN = "chkManualCheckIn";
	private static final String PARAM_LCC_VISIBILITY = "hdnLCCVisiblity";
	private static final String PARAM_CARRIER_CODE = "hdnCarrierCode";
	private static final int LCC_DEFAULT_VISIBILITY = 1;

	private static final String PARAM_SEARCHDATA = "hdnSearchData";

	private static final String PARAM_SEARCH_COUNTRYNAME = "selCountryName";

	private static final String PARAM_SEARCH_AIRPORTCODE = "selAirportCode";

	private static final String PARAM_SEARCH_AIRPORTNAME = "selAirportName";

	private static final String PARAM_LCC_PUBLISH_STATUS = "hdnLccPublishStatus";

	// private static final String PARAM_SEL_LCC_PUBLISH_STATUS = "selLccPublishStatus";

	private static final String PARAM_NOTIFICATION_START_CUTOVER_DAY = "anciStartCutoverDay";
	private static final String PARAM_NOTIFICATION_START_CUTOVER_TIME = "anciStartCutoverTime";
	private static final String PARAM_NOTIFICATION_END_CUTOVER_DAY = "anciEndCutoverDay";
	private static final String PARAM_NOTIFICATION_END_CUTOVER_TIME = "anciEndCutoverTime";

	private static final String PARAM_ONLINE_CHECKIN = "chkOnlineCheckin";

	private static final String PARAM_SURFACE_SEGMENT = "chkSurfaceSegment";
	private static final String PARAM_CONNECTING_AIRPORT = "selConnectingAirport";

	private static final String PARAM_MEAL_NOTIFICATION_STATUS = "chkMealNotification";
	private static final String PARAM_MEAL_NOTIFICATION_EMAILS = "txtNotifyEmail";
	private static final String PARAM_MEAL_NOTIFICATION_START_TIME = "mealNotifyStartTime";
	private static final String PARAM_MEAL_NOTIFICATION_FREQUENCY = "notificationFrequency";
	private static final String PARAM_MEAL_NOTIFICATION_END_TIME = "lastNotifyTime";
	private static final String PARAM_ETL_PROCESS_ENABLED = "chkETLProcessEnabled";
	private static final String PARAM_AUTO_FLOWN_PROCESS_ENABLED = "chkAutoFlownProcessEnabled";
	private static final String PARAM_ENABLE_PNL_CFG = "enableCFG";

	private static void setIntSuccess(HttpServletRequest request, int value) {
		// 0-Not Applicable, 1-Success, 2-Fail"3-New Airport
		setAttribInRequest(request, "intSuccess", new Integer(value));
	}

	private static int getIntSuccess(HttpServletRequest request) {
		Integer returnValue = (Integer) getAttribInRequest(request, "intSuccess");
		if (returnValue != null)
			return returnValue.intValue();
		else
			return -1;
	}

	private static void setExceptionOccured(HttpServletRequest request, boolean value) {
		setAttribInRequest(request, "isExceptionOccured", new Boolean(value));
	}

	private static boolean isExceptionOccured(HttpServletRequest request) {
		return ((Boolean) getAttribInRequest(request, "isExceptionOccured")).booleanValue();
	}

	/**
	 * Main Execute Method for Airport Action & Sets the Success int 0-Not Applicable, 1-Success, 2-Fail
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {

		String strHdnMode = request.getParameter(PARAM_MODE);
		String forward = WebConstants.FORWARD_SUCCESS;

		if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_GRID)) {
			return WebConstants.ACTION_GRID;
		}
		setIntSuccess(request, 0);
		String strFormData = "var saveSuccess = " + getIntSuccess(request) + ";";
		strFormData += " var arrFormData = new Array();";
		request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);

		String strGroundService = " var isGroundServiceEnabled = " + AppSysParamsUtil.isGroundServiceEnabled() + ";";
		request.setAttribute(WebConstants.REQ_HTML_GROUND_SERVICE_ENABLED, strGroundService);

		setExceptionOccured(request, false);
		setAttribInRequest(request, "strModeJS", "var isAddMode = false;");
		setAttribInRequest(request, "strStationFocusJS", "var isStationFocus = false;");
		setAttribInRequest(request, "strAirportFocusJS", "var isAirportFocus = false;");
		setAttribInRequest(request, "strGoShowFocusJS", "var isGoShowFocus = false;");
		setAttribInRequest(request, "strCityFocusJS", "var isGoShowFocus = false;");
		try {
			if (strHdnMode != null) {
				if (strHdnMode.equals(WebConstants.ACTION_ADD)) {
					checkPrivilege(request, WebConstants.PRIV_SYS_MAS_AIRPORT_ADD);

					setAttribInRequest(request, "strModeJS", "var isAddMode = true;");
					saveData(request);
					if (!isExceptionOccured(request)) {

						setIntSuccess(request, 3);
						strFormData = " var saveSuccess = " + getIntSuccess(request) + ";";
						strFormData += " var arrFormData = new Array();";
						request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
					}
				}

				if (strHdnMode.equals(WebConstants.ACTION_EDIT)) {
					checkPrivilege(request, WebConstants.PRIV_SYS_MAS_AIRPORT_EDIT);

					setAttribInRequest(request, "strModeJS", "var isAddMode = false;");
					saveData(request);
					if (!isExceptionOccured(request)) {
						setIntSuccess(request, 1);
						strFormData = " var saveSuccess = " + getIntSuccess(request) + ";";
						strFormData += " var arrFormData = new Array();";
						request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
					}
				}

				if (strHdnMode.equals(WebConstants.ACTION_DELETE)) {
					checkPrivilege(request, WebConstants.PRIV_SYS_MAS_AIRPORT_DELETE);
					deleteData(request);
				}
				// Haider
				if (strHdnMode.equals(WebConstants.ACTION_SAVE_LIST_FOR_DISPLAY)) {
					checkPrivilege(request, WebConstants.PRIV_SYS_MAS_AIRPORT_FOR_DISPLAY);

					setAttribInRequest(request, "strModeJS", "var isAddMode = false;");
					saveAirportForDisplay(request);
					if (!isExceptionOccured(request)) {
						setIntSuccess(request, 1);
						strFormData = " var saveSuccess = " + getIntSuccess(request) + ";";
						strFormData += " var arrFormData = new Array();";
						request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
					}
				}
			}

			setDisplayData(request);
		} catch (Exception exception) {
			log.error("Exception in AirportRequestHandler:execute()", exception);
			if (exception instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
			}
		}

		return forward;
	}

	/**
	 * Checks the Validity of the Airport
	 * 
	 * @param selectedAirport
	 *            the Selected Airport
	 * @param airportCode
	 *            the Airport Code
	 * @return int 0- Success 1- The changed closest Airport inactive
	 * @throws ModuleException
	 */
	private static int checkValidAirport(String selectedAirport, String airportCode) throws ModuleException {

		Airport airport = null;
		if (airportCode != null) {
			airport = ModuleServiceLocator.getAirportServiceBD().getAirport(airportCode);
		}
		Airport closebyAirport = ModuleServiceLocator.getAirportServiceBD().getAirport(selectedAirport);

		if (closebyAirport.getStatus().equals(Airport.STATUS_INACTIVE)) {
			if (airport == null)
				return 1;
			else {
				if (airport.getClosestAirport() == null)
					return 1;
				Airport closestAirport = ModuleServiceLocator.getAirportServiceBD().getAirport((airport.getClosestAirport()));
				if (closestAirport == null)
					return 1;
				if (!closestAirport.getAirportCode().equals(closebyAirport.getAirportCode()))
					return 1;
			}
		}
		return 0;
	}

	/**
	 * Checks for Validity of the Station
	 * 
	 * @param stationId
	 *            the Station Id
	 * @param airportCode
	 *            the Airport CodeS
	 * @return int 0- Success 1- The changed closest Airport inactive
	 * @throws ModuleException
	 */
	private static int checkValidStation(String stationId, String airportCode) throws ModuleException {
		Airport airport = null;
		if (airportCode != null) {
			airport = ModuleServiceLocator.getAirportServiceBD().getAirport(airportCode);
		}
		Station station = ModuleServiceLocator.getLocationServiceBD().getStation(stationId);
		if (station.getStatus().equals(Station.STATUS_INACTIVE)) {
			if (airport == null)
				return 1;
			else {
				Station stationAirport = ModuleServiceLocator.getLocationServiceBD().getStation((airport.getStationCode()));
				if (stationAirport == null)
					return 1;
				if (!stationAirport.getStationCode().equals(station.getStationCode()))
					return 1;
			}
		}
		return 0;
	}

	public static String addLeadingZeros(String s) {
		int stringLength = s.length();
		if (stringLength == 2)
			return "0" + s;
		else if (stringLength == 1)
			return "00" + s;
		else
			return s;

	}

	/**
	 * Saves Airport for display Data
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	private static void saveAirportForDisplay(HttpServletRequest request) throws Exception {
		if (AppSysParamsUtil.isDisplayIBEListsDynamicallyEnabled() == false)
			return;

		Properties apProp = getProperty(request);
		// Haider 05Mar09
		String airportListIBEStr = apProp.getProperty(PARAM_AIRPORTLIST_IBE);
		String[] airortListIBE = StringUtils.split(airportListIBEStr, "|");
		String[] airportIBE;
		AirportForDisplay at = null;
		for (int i = 0; i < airortListIBE.length; i++) {
			at = new AirportForDisplay();
			airportIBE = StringUtils.split(airortListIBE[i], "^");
			at.setAirportCode(airportIBE[4]);
			at.setAirportCodeOl(airportIBE[1]);
			at.setAirportNameOl(airportIBE[1]);
			at.setLanguageCode(airportIBE[0]);
			if (!"-1".equals(airportIBE[2]))// id
				at.setId(Integer.valueOf(airportIBE[2]));
			if (!"-1".equals(airportIBE[3]))// version
				at.setVersion(Long.valueOf(airportIBE[3]).longValue());
			if (!"null".equals(at.getAirportCodeOl()))
				ModuleServiceLocator.getTranslationBD().saveOrUpdateAirport(at);
			else
				ModuleServiceLocator.getTranslationBD().removeAirport(at.getId());
		}
		// <--------------
		saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS), WebConstants.MSG_SUCCESS);
	}

	/**
	 * Saves Airport Data (ADD/UPDATE)
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	private static void saveData(HttpServletRequest request) throws Exception {

		Properties apProp = getProperty(request);
		String strEngineVisible = globalConfig.getBizParam(SystemParamKeys.ENGINE_VISIBILITY);

		try {
			Airport airport = new Airport();

			if (!"".equals(apProp.getProperty(PARAM_VERSION))) {
				airport.setVersion(Long.parseLong(apProp.getProperty(PARAM_VERSION)));
			}
			airport.setAirportCode(apProp.getProperty(PARAM_AIRPORTCODE));
			airport.setAirportName(StringEscapeUtils.escapeJava(apProp.getProperty(PARAM_AIRPORTNAME).trim()));

			if (!apProp.getProperty(PARAM_STATIONCODE).equals("-1") && !apProp.getProperty(PARAM_STATIONCODE).equals("")) {
				airport.setStationCode(apProp.getProperty(PARAM_STATIONCODE));
			}
			
			
			boolean validCityStationCombination = true;
			if (!apProp.getProperty(PARAM_CITY_ID).equals("-1") && !apProp.getProperty(PARAM_CITY_ID).equals("")) {
				airport.setCityId(apProp.getProperty(PARAM_CITY_ID));
				validCityStationCombination = ModuleServiceLocator.getLocationServiceBD()
						.isValidCityStationCombination(airport.getStationCode(), airport.getCityId());				
			}
			
			if (apProp.getProperty(PARAM_ACTIVE).equals(WebConstants.CHECKBOX_ON))

				airport.setStatus(Airport.STATUS_ACTIVE);
			else
				airport.setStatus(Airport.STATUS_INACTIVE);

			if (!apProp.getProperty(PARAM_CLOSESTAIRPOST).equals("-1") && !apProp.getProperty(PARAM_CLOSESTAIRPOST).equals("")) {
				airport.setClosestAirport(apProp.getProperty(PARAM_CLOSESTAIRPOST));
			}
			airport.setContact(apProp.getProperty(PARAM_CONTACT).trim());
			airport.setTelephone(apProp.getProperty(PARAM_PHONE));
			airport.setFax(apProp.getProperty(PARAM_FAX));
			airport.setGmtOffsetAction(apProp.getProperty(PARAM_SEL_GMTOFFSETACTION));

			apProp.setProperty(PARAM_TXT_LATITUDE1, addLeadingZeros(apProp.getProperty(PARAM_TXT_LATITUDE1)));
			apProp.setProperty(PARAM_TXT_LATITUDE2, addLeadingZeros(apProp.getProperty(PARAM_TXT_LATITUDE2)));
			apProp.setProperty(PARAM_TXT_LATITUDE3, addLeadingZeros(apProp.getProperty(PARAM_TXT_LATITUDE3)));

			airport.setLatitude(apProp.getProperty(PARAM_TXT_LATITUDE1) + apProp.getProperty(PARAM_TXT_LATITUDE2)
					+ apProp.getProperty(PARAM_TXT_LATITUDE3));

			apProp.setProperty(PARAM_TXT_LONGTITUTE1, addLeadingZeros(apProp.getProperty(PARAM_TXT_LONGTITUTE1)));
			apProp.setProperty(PARAM_TXT_LONGTITUTE2, addLeadingZeros(apProp.getProperty(PARAM_TXT_LONGTITUTE2)));
			apProp.setProperty(PARAM_TXT_LONGTITUTE3, addLeadingZeros(apProp.getProperty(PARAM_TXT_LONGTITUTE3)));

			airport.setLongitude(apProp.getProperty(PARAM_TXT_LONGTITUTE1) + apProp.getProperty(PARAM_TXT_LONGTITUTE2)
					+ apProp.getProperty(PARAM_TXT_LONGTITUTE3));

			airport.setLatitudeNorthSouth(apProp.getProperty(PARAM_LATITUDE));
			airport.setLongitudeEastWest(apProp.getProperty(PARAM_LONGTITUTE));
			airport.setRemarks(apProp.getProperty(PARAM_REMARKS).trim());

			airport.setGoshowAgentCode(apProp.getProperty(PARAM_GOSHOWAGENT));

			// Storing the GmtOffsetAction as minutes
			if (!apProp.getProperty(PARAM_TXT_GMTOFFSETACTION).equals("0")) {
				String[] strGmtOffsetActionTimes = apProp.getProperty(PARAM_TXT_GMTOFFSETACTION).split(":");
				int gmtOffsetActionTimes = Integer.parseInt(strGmtOffsetActionTimes[0]) * 60
						+ Integer.parseInt(strGmtOffsetActionTimes[1]);
				airport.setGmtOffsetHours(gmtOffsetActionTimes);
			}

			String[] strTimes = apProp.getProperty(PARAM_MINSTOPOVERTIME).split(":");

			int minStopOverTimeinMins = Integer.parseInt(strTimes[0]) * 60 + Integer.parseInt(strTimes[1]);
			airport.setMinStopoverTime(minStopOverTimeinMins);

			if (apProp.getProperty(PARAM_ONLINESTATUS).equals("1") && apProp.getProperty(PARAM_OFFLINESTATUS).equals("0")) {
				apProp.setProperty(PARAM_ONLINESTATUS, "1");
			} else {
				if ("".equals(apProp.getProperty(PARAM_GOSHOWAGENT))) {
					apProp.setProperty(PARAM_ONLINESTATUS, "0");
				} else {
					setExceptionOccured(request, true);
					setAttribInRequest(request, "strGoShowFocusJS", "var isGoShowFocus = true;");
					saveMessage(request, airadminConfig.getMessage("um.airport.form.goShowAgent.cannot.exist"),
							WebConstants.MSG_ERROR);
					log.debug("AirportRequestHandler.saveData() method failed due to invalid goShow Agent existing.");
				}
			}

			airport.setOnlineStatus(Integer.parseInt(apProp.getProperty(PARAM_ONLINESTATUS)));

			if (apProp.getProperty(PARAM_IBEVISIBILITY).equals("Y") || !strEngineVisible.equals("Y")) {
				airport.setIBEVisibility(1);
			} else {
				airport.setIBEVisibility(0);
			}
			if (apProp.getProperty(PARAM_XBEVISIBILITY).equals("Y") || !strEngineVisible.equals("Y")) {
				airport.setXBEVisibility(1);
			} else {
				airport.setXBEVisibility(0);
			}
			airport.setLccPublishStatus(apProp.getProperty(PARAM_LCC_PUBLISH_STATUS));
			// ADDED NEW FIELDS
			if (apProp.getProperty(PARAM_CHECKIN).equals(WebConstants.CHECKBOX_ON)) {
				airport.setManualCheckin(Airport.YES);
			} else {
				airport.setManualCheckin(Airport.NO);
			}

			if (apProp.getProperty(PARAM_MODE).equals(WebConstants.ACTION_ADD)) {
				airport.setLccVisibility(LCC_DEFAULT_VISIBILITY);
			} else {
				airport.setLccVisibility(new Integer(apProp.getProperty(PARAM_LCC_VISIBILITY)).intValue());
			}
			if (apProp.getProperty(PARAM_CARRIER_CODE).equals("")) {
				airport.setCarrierCode(AppSysParamsUtil.getDefaultCarrierCode());
			} else {
				String carrierCodes = apProp.getProperty(PARAM_CARRIER_CODE).trim();
				if (carrierCodes.endsWith(",")) {
					airport.setCarrierCode(carrierCodes.substring(0, carrierCodes.length() - 1));
				} else {
					airport.setCarrierCode(carrierCodes);
				}
			}

			int nofiStartCutOverTimes = 0;
			if (!apProp.getProperty(PARAM_NOTIFICATION_START_CUTOVER_DAY).equals("")
					&& !apProp.getProperty(PARAM_NOTIFICATION_START_CUTOVER_DAY).equals("0")) {
				nofiStartCutOverTimes = Integer.parseInt(apProp.getProperty(PARAM_NOTIFICATION_START_CUTOVER_DAY)) * 1440;
			}
			if (!apProp.getProperty(PARAM_NOTIFICATION_START_CUTOVER_TIME).equals("0")) {
				String[] strNofiCutoverTimes = apProp.getProperty(PARAM_NOTIFICATION_START_CUTOVER_TIME).split(":");
				nofiStartCutOverTimes = nofiStartCutOverTimes
						+ (Integer.parseInt(strNofiCutoverTimes[0]) * 60 + Integer.parseInt(strNofiCutoverTimes[1]));
			}
			airport.setNotificationStartCutovertime(nofiStartCutOverTimes);

			int nofiEndCutOverTimes = 0;
			if (!apProp.getProperty(PARAM_NOTIFICATION_END_CUTOVER_DAY).equals("")
					&& !apProp.getProperty(PARAM_NOTIFICATION_END_CUTOVER_DAY).equals("0")) {
				nofiEndCutOverTimes = Integer.parseInt(apProp.getProperty(PARAM_NOTIFICATION_END_CUTOVER_DAY)) * 1440;
			}
			if (!apProp.getProperty(PARAM_NOTIFICATION_END_CUTOVER_TIME).equals("0")) {
				String[] endNofiCutoverTimes = apProp.getProperty(PARAM_NOTIFICATION_END_CUTOVER_TIME).split(":");
				nofiEndCutOverTimes = nofiEndCutOverTimes
						+ (Integer.parseInt(endNofiCutoverTimes[0]) * 60 + Integer.parseInt(endNofiCutoverTimes[1]));
			}
			airport.setNotificationEndCutovertime(nofiEndCutOverTimes);

			// SAVE ONLINE CHECKIN FLAG
			if (apProp.getProperty(PARAM_ONLINE_CHECKIN).equals(WebConstants.CHECKBOX_ON)) {
				airport.setOnlineCheckin(Airport.YES);
			} else {
				airport.setOnlineCheckin(Airport.NO);
			}

			if (apProp.getProperty(PARAM_SURFACE_SEGMENT).equals(WebConstants.CHECKBOX_ON)) {
				airport.setIsSurfaceStation(Airport.YES);
			} else {
				airport.setIsSurfaceStation(Airport.NO);
			}

			if (apProp.getProperty(PARAM_ETL_PROCESS_ENABLED).equals(WebConstants.CHECKBOX_ON)) {
				airport.setEtlProcessEnabled(Airport.YES);
			} else {
				airport.setEtlProcessEnabled(Airport.NO);
			}

			if (apProp.getProperty(PARAM_AUTO_FLOWN_PROCESS_ENABLED).equals(WebConstants.CHECKBOX_ON)) {
				airport.setAutoFlownProcessEnabled(Airport.YES);
			} else {
				airport.setAutoFlownProcessEnabled(Airport.NO);
			}
			
			if (apProp.getProperty(PARAM_ENABLE_PNL_CFG).equals(WebConstants.CHECKBOX_ON)) {
				airport.setEnableCFGElementForPNL(Airport.YES);
			} else {
				airport.setEnableCFGElementForPNL(Airport.NO);
			}


			airport.setConnectingAirport(apProp.getProperty(PARAM_CONNECTING_AIRPORT));

			airport.setNotificationEmail(apProp.getProperty(PARAM_MEAL_NOTIFICATION_EMAILS));

			Airport extAirport = ModuleServiceLocator.getAirportServiceBD().getAirport(apProp.getProperty(PARAM_AIRPORTCODE));
			// setting the correct accelaero dcs enable value
			if (extAirport != null) {
				airport.setIsAADCSEnabled(extAirport.getIsAADCSEnabled());
			} else {
				airport.setIsAADCSEnabled(Airport.NO);
			}
			MealNotifyTimings mealNotifyTiming = new MealNotifyTimings();

			List<String[]> mealTiming = null;
			if (extAirport != null) {
				mealTiming = SelectListGenerator.getMealNotifyTiming(extAirport.getAirportCode().trim());
			}

			if ((extAirport == null) || (extAirport != null && mealTiming.size() == 0)) {
				if (apProp.getProperty(PARAM_MEAL_NOTIFICATION_STATUS).equals("on")) {
					mealNotifyTiming.setNotifyStart(apProp.getProperty(PARAM_MEAL_NOTIFICATION_START_TIME));
					mealNotifyTiming.setLastNotify(apProp.getProperty(PARAM_MEAL_NOTIFICATION_END_TIME));
					mealNotifyTiming.setNotifyGap(Integer.parseInt(apProp.getProperty(PARAM_MEAL_NOTIFICATION_FREQUENCY)));
					mealNotifyTiming.setStatus("ACT");
				} else {
					mealNotifyTiming.setNotifyStart("00:00");
					mealNotifyTiming.setLastNotify("00:00");
					mealNotifyTiming.setNotifyGap(0);
					mealNotifyTiming.setStatus("INA");
				}
				mealNotifyTiming.setVersion(0);
			} else {
				boolean isMealNotifyChange = false;
				if (apProp.getProperty(PARAM_MEAL_NOTIFICATION_STATUS).equals("on")) {
					if (mealTiming.get(0)[4].trim().equals("INA")) {
						isMealNotifyChange = true;
					}
					mealNotifyTiming.setStatus("ACT");
					mealNotifyTiming.setMealNotifyTimingId(Integer.parseInt(mealTiming.get(0)[0].trim()));
					mealNotifyTiming.setLastNotify(apProp.getProperty(PARAM_MEAL_NOTIFICATION_END_TIME));
					mealNotifyTiming.setNotifyGap(Integer.parseInt(apProp.getProperty(PARAM_MEAL_NOTIFICATION_FREQUENCY)));
					mealNotifyTiming.setNotifyStart(apProp.getProperty(PARAM_MEAL_NOTIFICATION_START_TIME));

					if ((!mealTiming.get(0)[2].trim().equals(apProp.getProperty(PARAM_MEAL_NOTIFICATION_END_TIME)))
							|| (Integer.parseInt(mealTiming.get(0)[3].trim()) != Integer.parseInt(apProp
									.getProperty(PARAM_MEAL_NOTIFICATION_FREQUENCY)))
							|| (!mealTiming.get(0)[1].trim().equals(apProp.getProperty(PARAM_MEAL_NOTIFICATION_START_TIME)))) {
						isMealNotifyChange = true;
					}
				} else {
					if (mealTiming.get(0)[4].trim().equals("ACT")) {
						isMealNotifyChange = true;
					}
					mealNotifyTiming.setStatus("INA");
					mealNotifyTiming.setMealNotifyTimingId(Integer.parseInt(mealTiming.get(0)[0].trim()));
					mealNotifyTiming.setLastNotify(mealTiming.get(0)[2]);
					mealNotifyTiming.setNotifyGap(Integer.parseInt(mealTiming.get(0)[3].trim()));
					mealNotifyTiming.setNotifyStart(mealTiming.get(0)[1]);
					airport.setNotificationEmail(extAirport.getNotificationEmail());
				}

				if (isMealNotifyChange) {
					mealNotifyTiming.setVersion(mealNotifyTiming.getVersion() + 1);
				}
			}
			mealNotifyTiming.setAirportCode(apProp.getProperty(PARAM_AIRPORTCODE));
			mealNotifyTiming.setApplyToAll("Y");
			mealNotifyTiming.setZuluStartDate(CalendarUtil.getDate(2011, 02, 25));
			mealNotifyTiming.setZuluEndDate(CalendarUtil.getDate(9011, 12, 31));

			if (apProp.getProperty(PARAM_MODE).equals(WebConstants.ACTION_ADD)
					|| apProp.getProperty(PARAM_MODE).equals(WebConstants.ACTION_EDIT)) {

				if (checkValidStation(apProp.getProperty(PARAM_STATIONCODE), apProp.getProperty(PARAM_AIRPORTCODE)) == 1) {

					setAttribInRequest(request, "strStationFocusJS", "var isStationFocus = true;");
					setExceptionOccured(request, true);
					saveMessage(request, airadminConfig.getMessage("um.user.form.station.inactive"), WebConstants.MSG_ERROR);
					log.debug("AirportRequestHandler.saveData() method failed due to invalid station.");

				} else if (!validCityStationCombination) {
					setExceptionOccured(request, true);
					setAttribInRequest(request, "strCityFocusJS", "var isCityFocus = true;");
					saveMessage(request, airadminConfig.getMessage("um.airport.form.city.station.combination.invalid"),
							WebConstants.MSG_ERROR);
					log.debug("Invalid combination of station and city since the country codes are not matching");
				} else {

					setAttribInRequest(request, "strStationFocusJS", "var isStationFocus = false;");
					setAttribInRequest(request, "strAirportFocusJS", "var isAirportFocus = false;");
					setExceptionOccured(request, false);

					if (!apProp.getProperty(PARAM_CLOSESTAIRPOST).equals("-1")
							&& !apProp.getProperty(PARAM_CLOSESTAIRPOST).equals("")) {
						if (checkValidAirport(apProp.getProperty(PARAM_CLOSESTAIRPOST), apProp.getProperty(PARAM_AIRPORTCODE)) == 1) {

							setExceptionOccured(request, true);
							setAttribInRequest(request, "strAirportFocusJS", "var isAirportFocus = true;");
							saveMessage(request, airadminConfig.getMessage("um.user.form.airport.inactive"),
									WebConstants.MSG_ERROR);
							log.debug("AirportRequestHandler.saveData() method failed due to invalid airport.");

						} else {
							// Check whether in the edit mode
							if (request.getParameter(PARAM_MODE).equals(WebConstants.ACTION_EDIT)) {
								// Check whether GMT offset changed
								Airport existingAirport = ModuleServiceLocator.getAirportServiceBD().getAirport(
										apProp.getProperty(PARAM_AIRPORTCODE));
								if (existingAirport != null
										&& existingAirport.getVersion() == airport.getVersion()
										&& (!existingAirport.getGmtOffsetAction().equals(airport.getGmtOffsetAction()) || existingAirport
												.getGmtOffsetHours() != airport.getGmtOffsetHours())) {

									// GMT offset changed. Check whether the change is posible by checking the
									// airport is never used in flights

									Collection<String> colAirport = new HashSet<String>();
									colAirport.add(airport.getAirportCode());

									if (ModuleServiceLocator.getFlightServiceBD().getUsedAirports(colAirport)
											.get(airport.getAirportCode()) != null) {

										setExceptionOccured(request, true);
										saveMessage(request, airadminConfig.getMessage("um.user.form.airport.gmt.refered"),
												WebConstants.MSG_ERROR);
										log.debug("AirportRequestHandler.saveData() method failed "
												+ "due to change of GMT value of a used airport.");
									}
								}
							}
						}
					}
				}

				if (isExceptionOccured(request)) {
					String strFormData = getErrorForm(apProp);
					setIntSuccess(request, 0);
					strFormData += " var saveSuccess = " + getIntSuccess(request) + "; ";

					if (apProp.getProperty(PARAM_MODE).equals(WebConstants.ACTION_EDIT)) {
						strFormData += " var prevVersion = " + apProp.getProperty(PARAM_VERSION) + ";";
						strFormData += " var prevGridRow = " + apProp.getProperty(PARAM_GRIDROW) + ";";
					}
					request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
				}
				request.setAttribute(WebConstants.REQ_AIRPORT_FOCUS, getAttribInRequest(request, "strAirportFocusJS"));
				request.setAttribute(WebConstants.REQ_STATION_FOCUS, getAttribInRequest(request, "strStationFocusJS"));

			}

			if (!isExceptionOccured(request)) {
				// saveAirportForDisplay(request);
				// Haider 05Mar09
				String airportListIBEStr = apProp.getProperty(PARAM_AIRPORTLIST_IBE);
				String[] airortListIBE = StringUtils.split(airportListIBEStr, "|");
				String[] airportIBE;
				AirportForDisplay at = null;
				ArrayList<AirportForDisplay> al = new ArrayList<AirportForDisplay>();
				for (int i = 0; i < airortListIBE.length; i++) {
					at = new AirportForDisplay();
					airportIBE = StringUtils.split(airortListIBE[i], "^");
					at.setAirportCode(airportIBE[4].trim().toUpperCase());
					at.setAirportCodeOl(airportIBE[1]);
					at.setLanguageCode(airportIBE[0]);
					at.setAirportNameOl(airportIBE[1]);
					if (!"-1".equals(airportIBE[2]))// id
						at.setId(Integer.valueOf(airportIBE[2]));
					if (!"-1".equals(airportIBE[3]))// version
						at.setVersion(Long.valueOf(airportIBE[3]).longValue());
					al.add(at);
				}
				// <--------------

				ModuleServiceLocator.getAirportServiceBD().saveAirport(airport, al, mealNotifyTiming);
				airport = null;
				saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS), WebConstants.MSG_SUCCESS);
				log.debug("AirportRequestHandler.saveData() method is successfully executed");
			}
		} catch (ModuleException moduleException) {
			log.error("Exception in AirportRequestHandler:saveData() [origin module=" + moduleException.getModuleDesc() + "]",
					moduleException);

			setExceptionOccured(request, true);
			String strFormData = getErrorForm(apProp);
			setIntSuccess(request, 2);
			strFormData += " var saveSuccess = " + getIntSuccess(request) + ";";

			if (apProp.getProperty(PARAM_MODE).equals(WebConstants.ACTION_EDIT)) {
				strFormData += " var prevVersion = " + apProp.getProperty(PARAM_VERSION) + ";";
				strFormData += " var prevGridRow = " + apProp.getProperty(PARAM_GRIDROW) + ";";
			}

			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);

			if (moduleException.getExceptionCode().equals("module.duplicate.key")) {
				saveMessage(request, airadminConfig.getMessage("um.user.form.airport.already.exist"), WebConstants.MSG_ERROR);
			} else {
				saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
			}

		} catch (Exception exception) {
			log.error("Exception in AirportRequestHandler:saveData()", exception);
			setExceptionOccured(request, true);

			if (exception instanceof RuntimeException) {
				throw exception;
			} else {
				String strFormData = getErrorForm(apProp);
				setIntSuccess(request, 2);
				strFormData += " var saveSuccess = " + getIntSuccess(request) + ";";
				if (apProp.getProperty(PARAM_MODE).equals(WebConstants.ACTION_EDIT)) {
					strFormData += " var prevVersion = " + apProp.getProperty(PARAM_VERSION) + ";";
					strFormData += " var prevGridRow = " + apProp.getProperty(PARAM_GRIDROW) + ";";
				}
				request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
				saveMessage(request, exception.getMessage(), WebConstants.MSG_ERROR);
			}
		}
	}

	/**
	 * Delete the Airport
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void deleteData(HttpServletRequest request) {
		String strAirportCode = null;
		List<String[]> mealTiming = null;
		boolean isMealNotifyEnable = false;
		int mealNotifyId = 0;
		strAirportCode = request.getParameter(PARAM_AIRPORTCODE) == null ? "" : request.getParameter(PARAM_AIRPORTCODE);

		try {
			if ((strAirportCode != null) || (strAirportCode != "")) {
				Airport existingAirport = ModuleServiceLocator.getAirportServiceBD().getAirport(strAirportCode);
				mealTiming = SelectListGenerator.getMealNotifyTiming(strAirportCode);
				if (mealTiming.size() > 0) {
					isMealNotifyEnable = true;
					mealNotifyId = Integer.parseInt(mealTiming.get(0)[0]);
				}

				if (existingAirport == null) {
					// delete the airport for display list
					List<AirportForDisplay> l = ModuleServiceLocator.getTranslationBD().getAirportsByAirportCode(strAirportCode);
					if (l != null && l.size() > 0) {
						ModuleServiceLocator.getTranslationBD().removeAirport(strAirportCode, isMealNotifyEnable, mealNotifyId);
					}
					List<AirportCheckInTime> list = (List<AirportCheckInTime>) ModuleServiceLocator.getAirportServiceBD()
							.getAirportCITs(strAirportCode);
					if (list != null && list.size() > 0) {
						ModuleServiceLocator.getAirportServiceBD().deleteAirportCIT(strAirportCode);
					}
					ModuleServiceLocator.getAirportServiceBD().removeAirport(strAirportCode, isMealNotifyEnable, mealNotifyId);
				} else {
					String strVersion = request.getParameter(PARAM_VERSION);
					// delete the airport for display list
					List<AirportForDisplay> l = ModuleServiceLocator.getTranslationBD().getAirportsByAirportCode(strAirportCode);
					if (l != null && l.size() > 0) {
						ModuleServiceLocator.getTranslationBD().removeAirport(strAirportCode, isMealNotifyEnable, mealNotifyId);
					}
					List<AirportCheckInTime> list = (List<AirportCheckInTime>) ModuleServiceLocator.getAirportServiceBD()
							.getAirportCITs(strAirportCode);
					if (list != null && list.size() > 0) {
						ModuleServiceLocator.getAirportServiceBD().deleteAirportCIT(strAirportCode);
					}
					if (strVersion != null && !"".equals(strVersion)) {
						existingAirport.setVersion(Long.parseLong(strVersion));
						ModuleServiceLocator.getAirportServiceBD().removeAirport(existingAirport, isMealNotifyEnable,
								mealNotifyId);
					} else {
						ModuleServiceLocator.getAirportServiceBD()
								.removeAirport(strAirportCode, isMealNotifyEnable, mealNotifyId);
					}
				}
				saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_DELETE_SUCCESS), WebConstants.MSG_SUCCESS);
				log.debug("AirportRequestHandler.deleteData() method is successfully executed.");
			}
		} catch (ModuleException moduleException) {
			log.error("Exception in AirportRequestHandler:deleteData() [origin module=" + moduleException.getModuleDesc() + "]",
					moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Display Data for the Airport Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	private static void setDisplayData(HttpServletRequest request) throws Exception {

		setAirportRowHtml(request);
		setCountryList(request);
		setAirportCodeNameList(request);
		setLanguageList(request);// Haider
		setStationList(request);
		setAirportList(request);
		setCityList(request);
		setGoShoreAgentList(request);
		setClientErrors(request);
		setMinStopOverTimeBizParam(request);
		setEngineVisibility(request);
		setCarrierCodesList(request);
		setLCCEnableStatus(request);
		setActiveNonSurfaceAirportList(request);
		// Haider
		if (AppSysParamsUtil.isDisplayIBEListsDynamicallyEnabled())
			request.setAttribute(WebConstants.LIST_FOR_DISPLAY_ENABLED, "true");
		else
			request.setAttribute(WebConstants.LIST_FOR_DISPLAY_ENABLED, "false");
		// <--------
		request.setAttribute(WebConstants.REQ_GOSHOW_FOCUS, getAttribInRequest(request, "strGoShowFocusJS"));

		if (!isExceptionOccured(request)) {
			String strFormData = "var arrFormData = new Array();";
			strFormData += " var saveSuccess = " + getIntSuccess(request) + ";";
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
		}
		request.setAttribute(WebConstants.REQ_OPERATION_MODE, getAttribInRequest(request, "strModeJS"));
	}

	/**
	 * Sets the Client Validations to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setClientErrors(HttpServletRequest request) {

		try {
			String strClientErrors = AirportHTMLGenerator.getClientErrors(request);
			if (strClientErrors == null)
				strClientErrors = "";

			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);

		} catch (ModuleException moduleException) {
			log.error("Exception in AirportRequestHandler:setClientErrors() [origin module=" + moduleException.getModuleDesc()
					+ "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Country list to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setCountryList(HttpServletRequest request) {
		try {
			String strHtml = SelectListGenerator.createCountryDescListWOTag();
			request.setAttribute(WebConstants.REQ_HTML_COUNTRY_LIST, strHtml);
		} catch (ModuleException moduleException) {
			log.error("Exception in AirportRequestHandler:setCountryList() [origin module=" + moduleException.getModuleDesc()
					+ "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Airport code and name list to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setAirportCodeNameList(HttpServletRequest request) {
		try {
			String strHtml = SelectListGenerator.getAirportCodeNameCountryCodeList();
			request.setAttribute(WebConstants.REQ_HTML_AIRPORT_CODENAME_LIST, strHtml);
		} catch (ModuleException moduleException) {
			log.error("Exception in AirportRequestHandler:setCountryList() [origin module=" + moduleException.getModuleDesc()
					+ "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Language list to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setLanguageList(HttpServletRequest request) {
		try {
			String strHtml = SelectListGenerator.createLanguageList();
			request.setAttribute(WebConstants.REQ_HTML_LANGUAGE_LIST, strHtml);
		} catch (ModuleException moduleException) {
			log.error("Exception in AirportRequestHandler:setLanguageList() [origin module=" + moduleException.getModuleDesc()
					+ "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Go Shore Agents List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setGoShoreAgentList(HttpServletRequest request) {
		try {
			String strHtml = SelectListGenerator.createAgentListForAirport();
			request.setAttribute(WebConstants.REQ_HTML_GO_SHORE_AGENT_SELECT_LIST, strHtml);

		} catch (ModuleException moduleException) {
			log.error(
					"Exception in AirportRequestHandler:setGoShoreAgentList() [origin module=" + moduleException.getModuleDesc()
							+ "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Station List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setStationList(HttpServletRequest request) {
		try {
			String strHtml = SelectListGenerator.createStationList();
			request.setAttribute(WebConstants.REQ_HTML_STATION_SELECT_LIST, strHtml);

		} catch (ModuleException moduleException) {
			log.error("Exception in AirportRequestHandler:setStationlist() [origin module=" + moduleException.getModuleDesc()
					+ "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}
	
	/**
	 * Sets the Station List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setCityList(HttpServletRequest request) {
		try {
			String strHtml = SelectListGenerator.createCitiesList();
			request.setAttribute(WebConstants.REQ_HTML_CITY_SELECT_LIST, strHtml);

		} catch (ModuleException moduleException) {
			log.error("Exception in AirportRequestHandler:setStationlist() [origin module=" + moduleException.getModuleDesc()
					+ "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Airport List to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setAirportList(HttpServletRequest request) {

		try {
			String strHtml = SelectListGenerator.createAirportsList();
			request.setAttribute(WebConstants.REQ_HTML_AIRPORT_LIST, strHtml);

		} catch (ModuleException moduleException) {
			log.error("Exception in AirportRequestHandler:setAirportList() [origin module=" + moduleException.getModuleDesc()
					+ "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * sets the active airport List to the request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setActiveNonSurfaceAirportList(HttpServletRequest request) {
		try {
			String strHtml = "";
			if (AppSysParamsUtil.isGroundServiceEnabled()) {
				strHtml = SelectListGenerator.createActiveNonSurfaceAirportsCodeList();
			}
			request.setAttribute(WebConstants.REQ_HTML_ACTIVE_AIRPORT_LIST, strHtml);
		} catch (ModuleException moduleException) {
			log.error(
					"Exception in AirportRequestHandler:setActiveAirportList() [origin module=" + moduleException.getModuleDesc()
							+ "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the AirPort Grid row to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	protected static void setAirportRowHtml(HttpServletRequest request) {

		try {
			AirportHTMLGenerator htmlGen = new AirportHTMLGenerator();
			String strHtml = htmlGen.getAirportRowHtml(request);
			request.setAttribute(WebConstants.REQ_HTML_ROWS, strHtml);

		} catch (ModuleException moduleException) {
			log.error("Exception in AirportRequestHandler:setAirportRowHtml() [origin module=" + moduleException.getModuleDesc()
					+ "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Minimum Stop over time to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setMinStopOverTimeBizParam(HttpServletRequest request) {
		String defaultMinStopOverTime = globalConfig.getBizParam(SystemParamKeys.MIN_STOP_OVERTIME);
		String defaultMinStopOverTimeJS = "var defaultMinStopOverTime = '" + defaultMinStopOverTime + "';";
		request.setAttribute(WebConstants.REQ_HTML_DEFAULT_MIN_STOP_OVERTIME, defaultMinStopOverTimeJS);
	}

	/**
	 * Sets the Engine visibility or not according to app parameter
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setEngineVisibility(HttpServletRequest request) {
		GlobalConfig config = ModuleServiceLocator.getGlobalConfig();
		String strVisible = config.getBizParam(SystemParamKeys.ENGINE_VISIBILITY);
		boolean visibility = (strVisible != null && strVisible.equals("Y")) ? true : false;
		request.setAttribute(WebConstants.ENGINE_VISIBILITY, visibility);
	}

	/**
	 * Sets the LCC enable status according to the app-parameter
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setLCCEnableStatus(HttpServletRequest request) {
		boolean isLCCenabled = AppSysParamsUtil.isLCCConnectivityEnabled();
		request.setAttribute(WebConstants.REQ_HTML_LCC_ENABLE_STATUS, isLCCenabled);
	}

	/**
	 * Sets the Request Data to a Property File
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return Properties the File with AirPort Data
	 */
	private static Properties getProperty(HttpServletRequest request) {
		Properties prop = new Properties();

		prop.setProperty(PARAM_MODE, AiradminUtils.getNotNullString(request.getParameter(PARAM_MODE)));
		prop.setProperty(PARAM_VERSION, AiradminUtils.getNotNullString(request.getParameter(PARAM_VERSION)));
		prop.setProperty(PARAM_GRIDROW, AiradminUtils.getNotNullString(request.getParameter(PARAM_GRIDROW)));
		prop.setProperty(PARAM_ONLINESTATUS, getNotNullIntStr(request.getParameter(PARAM_ONLINESTATUS)));
		prop.setProperty(PARAM_OFFLINESTATUS, getNotNullIntStr(request.getParameter(PARAM_OFFLINESTATUS)));
		prop.setProperty(PARAM_AIRPORTCODE, AiradminUtils.getNotNullString(request.getParameter(PARAM_AIRPORTCODE)));
		prop.setProperty(PARAM_AIRPORTNAME, AiradminUtils.getNotNullString(request.getParameter(PARAM_AIRPORTNAME)));
		prop.setProperty(PARAM_AIRPORTLIST_IBE, AiradminUtils.getNotNullString(request.getParameter(PARAM_AIRPORTLIST_IBE)));
		prop.setProperty(PARAM_CONTACT, AiradminUtils.getNotNullString(request.getParameter(PARAM_CONTACT)));
		prop.setProperty(PARAM_PHONE, AiradminUtils.getNotNullString(request.getParameter(PARAM_PHONE)));
		prop.setProperty(PARAM_FAX, AiradminUtils.getNotNullString(request.getParameter(PARAM_FAX)));
		prop.setProperty(PARAM_REMARKS, AiradminUtils.getNotNullString(request.getParameter(PARAM_REMARKS)));
		prop.setProperty(PARAM_STARTS, AiradminUtils.getNotNullString(request.getParameter(PARAM_STARTS)));
		prop.setProperty(PARAM_ENDS, AiradminUtils.getNotNullString(request.getParameter(PARAM_ENDS)));
		prop.setProperty(PARAM_DST, AiradminUtils.getNotNullString(request.getParameter(PARAM_DST)));
		prop.setProperty(PARAM_ACTIVE, AiradminUtils.getNotNullString(request.getParameter(PARAM_ACTIVE)));
		prop.setProperty(PARAM_STATIONCODE, AiradminUtils.getNotNullString(request.getParameter(PARAM_STATIONCODE)));
		prop.setProperty(PARAM_CLOSESTAIRPOST, AiradminUtils.getNotNullString(request.getParameter(PARAM_CLOSESTAIRPOST)));
		prop.setProperty(PARAM_SEL_GMTOFFSETACTION,
				AiradminUtils.getNotNullString(request.getParameter(PARAM_SEL_GMTOFFSETACTION)));
		prop.setProperty(PARAM_TXT_GMTOFFSETACTION, getNotNullIntStr(request.getParameter(PARAM_TXT_GMTOFFSETACTION)));
		prop.setProperty(PARAM_MINSTOPOVERTIME, getNotNullIntStr(request.getParameter(PARAM_MINSTOPOVERTIME)));
		prop.setProperty(PARAM_LATITUDE, AiradminUtils.getNotNullString(request.getParameter(PARAM_LATITUDE)));
		prop.setProperty(PARAM_LONGTITUTE, AiradminUtils.getNotNullString(request.getParameter(PARAM_LONGTITUTE)));
		prop.setProperty(PARAM_TXT_LATITUDE1, getNotNullIntStr(request.getParameter(PARAM_TXT_LATITUDE1)));
		prop.setProperty(PARAM_TXT_LATITUDE2, getNotNullIntStr(request.getParameter(PARAM_TXT_LATITUDE2)));
		prop.setProperty(PARAM_TXT_LATITUDE3, getNotNullIntStr(request.getParameter(PARAM_TXT_LATITUDE3)));
		prop.setProperty(PARAM_TXT_LONGTITUTE1, getNotNullIntStr(request.getParameter(PARAM_TXT_LONGTITUTE1)));
		prop.setProperty(PARAM_TXT_LONGTITUTE2, getNotNullIntStr(request.getParameter(PARAM_TXT_LONGTITUTE2)));
		prop.setProperty(PARAM_TXT_LONGTITUTE3, getNotNullIntStr(request.getParameter(PARAM_TXT_LONGTITUTE3)));
		prop.setProperty(PARAM_IBEVISIBILITY,
				(request.getParameter(PARAM_IBEVISIBILITY) == null) ? "N" : request.getParameter(PARAM_IBEVISIBILITY));
		prop.setProperty(PARAM_XBEVISIBILITY,
				(request.getParameter(PARAM_XBEVISIBILITY) == null) ? "N" : request.getParameter(PARAM_XBEVISIBILITY));
		prop.setProperty(PARAM_GOSHOWAGENT, AiradminUtils.getNotNullString(request.getParameter(PARAM_GOSHOWAGENT)));
		prop.setProperty(PARAM_CHECKIN, AiradminUtils.getNotNullString(request.getParameter(PARAM_CHECKIN)));
		prop.setProperty(PARAM_LCC_VISIBILITY, AiradminUtils.getNotNullString(request.getParameter(PARAM_LCC_VISIBILITY)));
		prop.setProperty(PARAM_CARRIER_CODE, AiradminUtils.getNotNullString(request.getParameter(PARAM_CARRIER_CODE)));

		String strSearchData = request.getParameter(PARAM_SEARCHDATA);

		String searchDataArr[] = null;
		if (strSearchData != null && !strSearchData.equals("")) {
			searchDataArr = strSearchData.split(",");
		}

		prop.setProperty(PARAM_SEARCHDATA, AiradminUtils.getNotNullString(strSearchData));
		prop.setProperty(PARAM_SEARCH_COUNTRYNAME, (searchDataArr != null ? searchDataArr[0].trim() : ""));
		prop.setProperty(PARAM_SEARCH_AIRPORTCODE, (searchDataArr != null ? searchDataArr[1].trim() : ""));
		prop.setProperty(PARAM_SEARCH_AIRPORTNAME, (searchDataArr != null ? searchDataArr[2].trim() : ""));

		prop.setProperty(PARAM_LCC_PUBLISH_STATUS, AiradminUtils.getNotNullString(request.getParameter(PARAM_LCC_PUBLISH_STATUS)));

		prop.setProperty(PARAM_NOTIFICATION_START_CUTOVER_DAY,
				getNotNullIntStr(request.getParameter(PARAM_NOTIFICATION_START_CUTOVER_DAY)));
		prop.setProperty(PARAM_NOTIFICATION_START_CUTOVER_TIME,
				AiradminUtils.getNotNullString(request.getParameter(PARAM_NOTIFICATION_START_CUTOVER_TIME)));
		prop.setProperty(PARAM_NOTIFICATION_END_CUTOVER_DAY,
				getNotNullIntStr(request.getParameter(PARAM_NOTIFICATION_END_CUTOVER_DAY)));
		prop.setProperty(PARAM_NOTIFICATION_END_CUTOVER_TIME,
				AiradminUtils.getNotNullString(request.getParameter(PARAM_NOTIFICATION_END_CUTOVER_TIME)));

		prop.setProperty(PARAM_ONLINE_CHECKIN, AiradminUtils.getNotNullString(request.getParameter(PARAM_ONLINE_CHECKIN)));

		prop.setProperty(PARAM_SURFACE_SEGMENT, AiradminUtils.getNotNullString(request.getParameter(PARAM_SURFACE_SEGMENT)));
		prop.setProperty(PARAM_CONNECTING_AIRPORT, AiradminUtils.getNotNullString(request.getParameter(PARAM_CONNECTING_AIRPORT)));

		prop.setProperty(PARAM_MEAL_NOTIFICATION_STATUS,
				AiradminUtils.getNotNullString(request.getParameter(PARAM_MEAL_NOTIFICATION_STATUS)));
		prop.setProperty(PARAM_MEAL_NOTIFICATION_EMAILS,
				AiradminUtils.getNotNullString(request.getParameter(PARAM_MEAL_NOTIFICATION_EMAILS)));
		prop.setProperty(PARAM_MEAL_NOTIFICATION_START_TIME,
				AiradminUtils.getNotNullString(request.getParameter(PARAM_MEAL_NOTIFICATION_START_TIME)));
		prop.setProperty(PARAM_MEAL_NOTIFICATION_FREQUENCY,
				AiradminUtils.getNotNullString(request.getParameter(PARAM_MEAL_NOTIFICATION_FREQUENCY)));
		prop.setProperty(PARAM_MEAL_NOTIFICATION_END_TIME,
				AiradminUtils.getNotNullString(request.getParameter(PARAM_MEAL_NOTIFICATION_END_TIME)));

		prop.setProperty(PARAM_ETL_PROCESS_ENABLED,
				AiradminUtils.getNotNullString(request.getParameter(PARAM_ETL_PROCESS_ENABLED)));
		prop.setProperty(PARAM_AUTO_FLOWN_PROCESS_ENABLED,
				AiradminUtils.getNotNullString(request.getParameter(PARAM_AUTO_FLOWN_PROCESS_ENABLED)));

		prop.setProperty(PARAM_ENABLE_PNL_CFG, AiradminUtils.getNotNullString(request.getParameter(PARAM_ENABLE_PNL_CFG)));
		prop.setProperty(PARAM_CITY_ID, AiradminUtils.getNotNullString(request.getParameter(PARAM_CITY_ID)));

		return prop;
	}

	private static String getErrorForm(Properties erp) {
		StringBuffer ersb = new StringBuffer("var arrFormData = new Array();");

		ersb.append("arrFormData[0] = new Array();");
		ersb.append("arrFormData[0][1] = '" + erp.getProperty(PARAM_ONLINESTATUS) + "';");
		ersb.append("arrFormData[0][2] = '" + erp.getProperty(PARAM_AIRPORTCODE) + "';");
		ersb.append("arrFormData[0][3] = '" + erp.getProperty(PARAM_AIRPORTNAME) + "';");
		ersb.append("arrFormData[0][4] = '" + erp.getProperty(PARAM_CONTACT) + "';");
		ersb.append("arrFormData[0][5] = '" + erp.getProperty(PARAM_PHONE) + "';");
		ersb.append("arrFormData[0][6] = '" + erp.getProperty(PARAM_FAX) + "';");
		ersb.append("arrFormData[0][7] = '" + erp.getProperty(PARAM_REMARKS) + "';");
		ersb.append("arrFormData[0][8] = '" + erp.getProperty(PARAM_STARTS) + "';");
		ersb.append("arrFormData[0][9] = '" + erp.getProperty(PARAM_ENDS) + "';");

		ersb.append("arrFormData[0][10] = '" + erp.getProperty(PARAM_DST) + "';");
		ersb.append("arrFormData[0][11] = '" + erp.getProperty(PARAM_ACTIVE) + "';");
		ersb.append("arrFormData[0][12] = '" + erp.getProperty(PARAM_STATIONCODE) + "';");
		ersb.append("arrFormData[0][13] = '" + erp.getProperty(PARAM_CLOSESTAIRPOST) + "';");
		ersb.append("arrFormData[0][14] = '" + erp.getProperty(PARAM_SEL_GMTOFFSETACTION) + "';");
		ersb.append("arrFormData[0][15] = '" + erp.getProperty(PARAM_TXT_GMTOFFSETACTION) + "';");
		ersb.append("arrFormData[0][16] = '" + erp.getProperty(PARAM_MINSTOPOVERTIME) + "';");
		ersb.append("arrFormData[0][17] = '';");
		ersb.append("arrFormData[0][18] = '" + erp.getProperty(PARAM_LATITUDE) + "';");
		ersb.append("arrFormData[0][19] = '" + erp.getProperty(PARAM_TXT_LATITUDE1) + "';");
		ersb.append("arrFormData[0][20] = '" + erp.getProperty(PARAM_LONGTITUTE) + "';");

		ersb.append("arrFormData[0][21] = '" + erp.getProperty(PARAM_TXT_LONGTITUTE1) + "';");
		ersb.append("arrFormData[0][22] = '" + erp.getProperty(PARAM_MODE) + "';");
		ersb.append("arrFormData[0][23] = '" + erp.getProperty(PARAM_TXT_LATITUDE2) + "';");
		ersb.append("arrFormData[0][24] = '" + erp.getProperty(PARAM_TXT_LATITUDE3) + "';");
		ersb.append("arrFormData[0][25] = '" + erp.getProperty(PARAM_TXT_LONGTITUTE2) + "';");
		ersb.append("arrFormData[0][26] = '" + erp.getProperty(PARAM_TXT_LONGTITUTE3) + "';");

		ersb.append("arrFormData[0][27] = '" + erp.getProperty(PARAM_IBEVISIBILITY) + "';");
		ersb.append("arrFormData[0][28] = '" + erp.getProperty(PARAM_XBEVISIBILITY) + "';");
		ersb.append("arrFormData[0][29] = '" + erp.getProperty(PARAM_GOSHOWAGENT) + "';");

		ersb.append("arrFormData[0][30] = '" + erp.getProperty(PARAM_NOTIFICATION_START_CUTOVER_DAY) + "';");
		ersb.append("arrFormData[0][31] = '" + erp.getProperty(PARAM_NOTIFICATION_START_CUTOVER_TIME) + "';");
		ersb.append("arrFormData[0][32] = '" + erp.getProperty(PARAM_NOTIFICATION_END_CUTOVER_DAY) + "';");
		ersb.append("arrFormData[0][33] = '" + erp.getProperty(PARAM_NOTIFICATION_END_CUTOVER_TIME) + "';");
		ersb.append("arrFormData[0][34] = '" + erp.getProperty(PARAM_SURFACE_SEGMENT) + "';");
		ersb.append("arrFormData[0][35] = '" + erp.getProperty(PARAM_CONNECTING_AIRPORT) + "';");
		ersb.append("arrFormData[0][36] = '" + erp.getProperty(PARAM_LCC_PUBLISH_STATUS) + "';");
		ersb.append("arrFormData[0][37] = '" + erp.getProperty(PARAM_CARRIER_CODE) + "';");
		ersb.append("arrFormData[0][38] = '" + erp.getProperty(PARAM_CITY_ID) + "';");
		ersb.append("arrFormData[0][39] = '" + erp.getProperty(PARAM_LCC_VISIBILITY) + "';");

		// TODO Add missing parameters up until 51
		return ersb.toString();
	}

	private static String getNotNullIntStr(String str) {
		return (str == null) ? "0" : str;
	}

	/**
	 * Sets the carrier codes List
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setCarrierCodesList(HttpServletRequest request) {

		try {
			String strHtml = SelectListGenerator.getSystemCarrierCodes();
			request.setAttribute(WebConstants.REQ_CARRIER_SELECT_LIST, strHtml);
		} catch (ModuleException moduleException) {
			log.error(
					"Exception in AirportRequestHandler.setCarrierCodesList() [origin module=" + moduleException.getModuleDesc()
							+ "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}
}
