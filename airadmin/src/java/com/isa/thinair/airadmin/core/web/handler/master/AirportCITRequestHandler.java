package com.isa.thinair.airadmin.core.web.handler.master;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.master.AirportCITHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airmaster.api.model.AirportCheckInTime;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

public final class AirportCITRequestHandler extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(AirportCITRequestHandler.class);

	private static AiradminConfig airadminConfig = new AiradminConfig();

	private static final String PARAM_VERSION = "hdnVersion";
	private static final String PARAM_GRIDROW = "hdnGridRow";
	private static final String PARAM_MODE = "hdnMode";
	private static final String PARAM_AIRPORTCODE = "txtAirportCode";
	private static final String PARAM_CARRIERCODE = "selCarrierCode";
	private static final String PARAM_FLIGHT_NO = "txtFlightNo";
	private static final String PARAM_CHECKIN_TIME = "txtCIT";
	private static final String PARAM_CHECKIN_CLOSING_TIME = "txtCIClosingTime";

	private static final String PARAM_CIT_ID = "hdnCITId";

	// int 0-Not Applicable, 1-Success, 2-Fail

	private static void setIntSuccess(HttpServletRequest request, int value) {
		setAttribInRequest(request, "citIntSuccess", new Integer(value));
	}

	private static int getIntSuccess(HttpServletRequest request) {
		Integer returnValue = (Integer) getAttribInRequest(request, "citIntSuccess");
		if (returnValue != null)
			return returnValue.intValue();
		else
			return -1;
	}

	private static void setExceptionOccured(HttpServletRequest request, boolean value) {
		setAttribInRequest(request, "isExceptionOccuredCIT", new Boolean(value));
	}

	private static boolean isExceptionOccured(HttpServletRequest request) {
		return ((Boolean) getAttribInRequest(request, "isExceptionOccuredCIT")).booleanValue();
	}

	/**
	 * Execute Method for CIT Action & Sets Succces Int int 0-Not Applicable, 1-Success, 2-Fail
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {

		String strHdnMode = request.getParameter(PARAM_MODE);
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;

		setIntSuccess(request, 0);
		String strFormData = "var saveSuccess = " + getIntSuccess(request) + ";";

		strFormData += " var arrFormData = new Array();";

		request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);

		setIntSuccess(request, 0);
		setExceptionOccured(request, false);
		setAttribInRequest(request, "strModeJSCIT", "var isAddMode = false;");

		try {
			if (strHdnMode != null) {
				if (strHdnMode.equals(WebConstants.ACTION_ADD)) {

					setAttribInRequest(request, "strModeJSCIT", "var isAddMode = true;");
					checkPrivilege(request, WebConstants.PRIV_SYS_MAS_AIRPORT_CIT_ADD);
					saveData(request);
				}
				if (strHdnMode.equals(WebConstants.ACTION_EDIT)) {

					setAttribInRequest(request, "strModeJSCIT", "var isAddMode = false;");
					checkPrivilege(request, WebConstants.PRIV_SYS_MAS_AIRPORT_CIT_EDIT);
					saveData(request);

				}
				if (strHdnMode.equals(WebConstants.ACTION_DELETE)) {

					setAttribInRequest(request, "strModeJSCIT", "var isAddMode = false;");
					checkPrivilege(request, WebConstants.PRIV_SYS_MAS_AIRPORT_CIT_DELETE);
					deleteData(request);

				}

			}
			request.setAttribute(WebConstants.REQ_OPERATION_MODE, getAttribInRequest(request, "strModeJSCIT"));

			log.debug("AirportCITRequestHandler.setDisplayData() method is successfully executed.");

		} catch (Exception exception) {
			log.error("Exception in AirportCITRequestHandler:execute()", exception);
			if (exception instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
			}
		}

		setDisplayData(request);
		return forward;
	}

	/**
	 * Saves the CIT Data for an Airport
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return boolean true-success false -not
	 * @throws Exception
	 *             the Exception
	 */
	private static boolean saveData(HttpServletRequest request) throws Exception {

		log.debug("Inside the AirportCITRequestHandler.saveData() method...");

		AirportCheckInTime airportCit = new AirportCheckInTime();

		String strVersion = null;
		String strHdnMode = null;
		String strAirportCode = null;

		String strCITId = null;
		String strGridRow = null;
		String strCarrierCode = null;
		String strFlightNo = null;
		String strCheckInTimeGap = null;
		String strCheckInClosingTime = null;
		int chkInStartingTimeinMins = 0;
		int chkInClosingTimeinMins = 0;

		setExceptionOccured(request, false);

		try {
			strCITId = request.getParameter(PARAM_CIT_ID);
			strVersion = AiradminUtils.getNotNullString(request.getParameter(PARAM_VERSION));
			strGridRow = AiradminUtils.getNotNullString(request.getParameter(PARAM_GRIDROW));
			strHdnMode = AiradminUtils.getNotNullString(request.getParameter(PARAM_MODE));
			strAirportCode = AiradminUtils.getNotNullString(request.getParameter(PARAM_AIRPORTCODE));

			strCarrierCode = AiradminUtils.getNotNullString(request.getParameter(PARAM_CARRIERCODE));
			strFlightNo = AiradminUtils.getNotNullString(request.getParameter(PARAM_FLIGHT_NO));
			strCheckInTimeGap = AiradminUtils.getNotNullString(request.getParameter(PARAM_CHECKIN_TIME));
			strCheckInClosingTime = AiradminUtils.getNotNullString(request.getParameter(PARAM_CHECKIN_CLOSING_TIME));

			if (!"".equals(strVersion)) {
				airportCit.setVersion(Long.parseLong(strVersion));
			}
			if (strHdnMode.equals(WebConstants.ACTION_EDIT)) {
				airportCit.setAirportCheckInTime(Integer.parseInt(strCITId));

			}

			if (strCheckInTimeGap != null && !strCheckInTimeGap.trim().equals("")) {
				String[] strTimes = strCheckInTimeGap.split(":");
				chkInStartingTimeinMins = Integer.parseInt(strTimes[0]) * 60 + Integer.parseInt(strTimes[1]);
			}

			if (strCheckInClosingTime != null && !strCheckInClosingTime.trim().equals("")) {
				String[] chkInClosinTimes = strCheckInClosingTime.split(":");
				chkInClosingTimeinMins = Integer.parseInt(chkInClosinTimes[0]) * 60 + Integer.parseInt(chkInClosinTimes[1]);
			}

			airportCit.setAirportCode(strAirportCode);
			airportCit.setCarrierCode(strCarrierCode);
			airportCit.setFlightNumber(strFlightNo);
			airportCit.setCheckInGap(chkInStartingTimeinMins);
			airportCit.setCheckInClosingTime(chkInClosingTimeinMins);

			log.debug("Inside the AirportCITRequestHandler.saveData()- setters are successfully called...");

			ModuleServiceLocator.getAirportServiceBD().addNewCIT(airportCit);

			saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS), WebConstants.MSG_SUCCESS);

			airportCit = null;

		} catch (ModuleException moduleException) {

			setExceptionOccured(request, true);

			String strFormData = "var arrFormData = new Array();";
			strFormData += "arrFormData[0] = new Array();";
			strFormData += "arrFormData[0][1] = '" + strCarrierCode + "';";
			strFormData += "arrFormData[0][2] = '" + strFlightNo + "';";
			strFormData += "arrFormData[0][3] = '" + strCheckInTimeGap + "';";
			strFormData += "arrFormData[0][4] = '" + strVersion + "';";
			strFormData += "arrFormData[0][5] = '" + strCITId + "';";
			strFormData += "arrFormData[0][6] = '" + strCheckInClosingTime + "';";

			setIntSuccess(request, 2);
			strFormData += " var saveSuccess = " + getIntSuccess(request) + ";";

			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_EDIT)
					|| strHdnMode.equals(WebConstants.ACTION_DISABLE_ROLLBACK)) {

				strFormData += " var prevVersion = " + strVersion + ";";
				strFormData += " var prevCITId = " + strCITId + ";";
				strFormData += " var prevGridRow = " + strGridRow + ";";
			}
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);

			log.error("Exception in AirportCITRequestHandler:saveData() [origin module=" + moduleException.getModuleDesc() + "]",
					moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);

		} catch (Exception exception) {

			setExceptionOccured(request, true);

			String strFormData = "var arrFormData = new Array();";
			strFormData += "arrFormData[0] = new Array();";
			strFormData += "arrFormData[0][1] = '" + strCarrierCode + "';";
			strFormData += "arrFormData[0][2] = '" + strFlightNo + "';";
			strFormData += "arrFormData[0][3] = '" + strCheckInTimeGap + "';";
			strFormData += "arrFormData[0][4] = '" + strVersion + "';";
			strFormData += "arrFormData[0][5] = '" + strCITId + "';";
			strFormData += "arrFormData[0][6] = '" + strCheckInClosingTime + "';";

			setIntSuccess(request, 2);
			strFormData += " var saveSuccess = " + getIntSuccess(request) + ";";

			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_EDIT)
					|| strHdnMode.equals(WebConstants.ACTION_DISABLE_ROLLBACK)) {
				strFormData += " var prevVersion = " + strVersion + ";";
				strFormData += " var prevCITId = " + strCITId + ";";
				strFormData += " var prevGridRow = " + strGridRow + ";";
			}

			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
			log.error("Exception in AirportCITRequestHandler:saveData()", exception);

			if (exception instanceof RuntimeException) {
				throw exception;
			} else {
				saveMessage(request, exception.getMessage(), WebConstants.MSG_ERROR);
			}
		}
		return true;
	}

	/**
	 * Deletes the CIT Data for an Airport
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return boolean true-success false -not
	 * @throws Exception
	 *             the Exception
	 */
	private static boolean deleteData(HttpServletRequest request) throws Exception {

		log.debug("Inside the AirportCITRequestHandler.deleteData() method...");

		AirportCheckInTime airportCit = new AirportCheckInTime();

		String strVersion = null;
		String strHdnMode = null;
		String strAirportCode = null;

		String strCITId = null;
		String strGridRow = null;
		String strCarrierCode = null;
		String strFlightNo = null;
		String strCheckInTimeGap = null;
		String strCheckInClosingTime = null;
		int checkInTimeGap = 0;
		int checkInClosingTime = 0;

		setExceptionOccured(request, false);

		try {
			strCITId = request.getParameter(PARAM_CIT_ID);
			strVersion = AiradminUtils.getNotNullString(request.getParameter(PARAM_VERSION));
			strGridRow = AiradminUtils.getNotNullString(request.getParameter(PARAM_GRIDROW));
			strHdnMode = AiradminUtils.getNotNullString(request.getParameter(PARAM_MODE));
			strAirportCode = AiradminUtils.getNotNullString(request.getParameter(PARAM_AIRPORTCODE));

			strCarrierCode = AiradminUtils.getNotNullString(request.getParameter(PARAM_CARRIERCODE));
			strFlightNo = AiradminUtils.getNotNullString(request.getParameter(PARAM_FLIGHT_NO));
			strCheckInTimeGap = AiradminUtils.getNotNullString(request.getParameter(PARAM_CHECKIN_TIME));
			strCheckInClosingTime = AiradminUtils.getNotNullString(request.getParameter(PARAM_CHECKIN_CLOSING_TIME));

			if (strCheckInTimeGap != null && !strCheckInTimeGap.equals("")) {
				String[] chkInTime = strCheckInTimeGap.split(":", 2);
				checkInTimeGap = Integer.parseInt(chkInTime[0]) * 60 + Integer.parseInt(chkInTime[1]);
			}

			if (strCheckInClosingTime != null && !strCheckInClosingTime.equals("")) {
				String[] chkInTime = strCheckInClosingTime.split(":", 2);
				checkInClosingTime = Integer.parseInt(chkInTime[0]) * 60 + Integer.parseInt(chkInTime[1]);
			}

			if (!"".equals(strVersion)) {
				airportCit.setVersion(Long.parseLong(strVersion));
			}

			airportCit.setAirportCheckInTime(Integer.parseInt(strCITId));

			airportCit.setAirportCode(strAirportCode);
			airportCit.setCarrierCode(strCarrierCode);
			airportCit.setFlightNumber(strFlightNo);
			airportCit.setCheckInGap(checkInTimeGap);
			airportCit.setCheckInClosingTime(checkInClosingTime);

			log.debug("Inside the AirportCITRequestHandler.deleteData()- setters are successfully called...");

			ModuleServiceLocator.getAirportServiceBD().deleteAirportCIT(airportCit);

			saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_DELETE_SUCCESS), WebConstants.MSG_SUCCESS);

			airportCit = null;

		} catch (ModuleException moduleException) {

			setExceptionOccured(request, true);

			String strFormData = "var arrFormData = new Array();";
			strFormData += "arrFormData[0] = new Array();";
			strFormData += "arrFormData[0][1] = '" + strCarrierCode + "';";
			strFormData += "arrFormData[0][2] = '" + strFlightNo + "';";
			strFormData += "arrFormData[0][3] = '" + strCheckInTimeGap + "';";
			strFormData += "arrFormData[0][4] = '" + strVersion + "';";
			strFormData += "arrFormData[0][5] = '" + strCITId + "';";
			strFormData += "arrFormData[0][6] = '" + strCheckInClosingTime + "';";

			setIntSuccess(request, 2);
			strFormData += " var saveSuccess = " + getIntSuccess(request) + ";";

			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_EDIT)) {

				strFormData += " var prevVersion = " + strVersion + ";";
				strFormData += " var prevCITId = " + strCITId + ";";
				strFormData += " var prevGridRow = " + strGridRow + ";";
			}
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);

			log.error("Exception in AirportCITRequestHandler:deleteData() [origin module=" + moduleException.getModuleDesc()
					+ "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);

		} catch (Exception exception) {

			setExceptionOccured(request, true);

			String strFormData = "var arrFormData = new Array();";
			strFormData += "arrFormData[0] = new Array();";
			strFormData += "arrFormData[0][1] = '" + strCarrierCode + "';";
			strFormData += "arrFormData[0][2] = '" + strFlightNo + "';";
			strFormData += "arrFormData[0][3] = '" + strCheckInTimeGap + "';";
			strFormData += "arrFormData[0][4] = '" + strVersion + "';";
			strFormData += "arrFormData[0][5] = '" + strCITId + "';";
			strFormData += "arrFormData[0][6] = '" + strCheckInClosingTime + "';";

			setIntSuccess(request, 2);
			strFormData += " var saveSuccess = " + getIntSuccess(request) + ";";

			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_EDIT)) {
				strFormData += " var prevVersion = " + strVersion + ";";
				strFormData += " var prevCITId = " + strCITId + ";";
				strFormData += " var prevGridRow = " + strGridRow + ";";
			}

			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
			log.error("Exception in AirportCITRequestHandler:deleteData()", exception);

			if (exception instanceof RuntimeException) {
				throw exception;
			} else {
				saveMessage(request, exception.getMessage(), WebConstants.MSG_ERROR);
			}
		}
		return true;
	}

	/**
	 * Set the Display Data For CIT Page
	 * 
	 * @param request
	 *            the HttpServletRequests
	 */
	public static void setDisplayData(HttpServletRequest request) {
		setAirportCITRowHtml(request);
		setCarrierCodesList(request);
		setFlightNoList(request);
		setClientErrors(request);
		if (!isExceptionOccured(request)) {
			String strFormData = "var arrFormData = new Array();";
			strFormData += " var saveSuccess = " + getIntSuccess(request) + ";";

			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);

			request.setAttribute(WebConstants.REQ_OPERATION_MODE, getAttribInRequest(request, "strModeJSCIT"));
		}
	}

	/**
	 * Sets the Client Validations for CIT Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setClientErrors(HttpServletRequest request) {

		try {

			String strClientErrors = AirportCITHTMLGenerator.getClientErrors(request);
			if (strClientErrors == null)
				strClientErrors = "";

			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);

		} catch (ModuleException moduleException) {
			log.error("Exception in AirportCITRequestHandler:setClientErrors() [origin module=" + moduleException.getModuleDesc()
					+ "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the CIT Row to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	protected static void setAirportCITRowHtml(HttpServletRequest request) {

		try {

			AirportCITHTMLGenerator htmlGen = new AirportCITHTMLGenerator();

			String strHtml = htmlGen.getAirportCITRowHtml(request);
			request.setAttribute(WebConstants.REQ_HTML_ROWS, strHtml);

		} catch (ModuleException moduleException) {

			log.error(
					"Exception in AirportCITRequestHandler:setAirportCITRowHtml() [origin module="
							+ moduleException.getModuleDesc() + "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the carrier codes List
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setCarrierCodesList(HttpServletRequest request) {

		try {
			String strHtml = SelectListGenerator.getSystemCarrierCodes();
			request.setAttribute(WebConstants.REQ_CARRIER_SELECT_LIST, strHtml);
		} catch (ModuleException moduleException) {
			log.error(
					"Exception in AirportCITRequestHandler.setCarrierCodesList() [origin module="
							+ moduleException.getModuleDesc() + "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Flight number List
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setFlightNoList(HttpServletRequest request) {

		try {
			String strHtml = SelectListGenerator.createFlightNoList();
			request.setAttribute(WebConstants.REQ_HTML_FLIGHTNO_LIST_DATA, strHtml);
		} catch (ModuleException moduleException) {
			log.error("Exception in AirportCITRequestHandler.setFlightNoList() [origin module=" + moduleException.getModuleDesc()
					+ "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

}
