package com.isa.thinair.airadmin.core.web.v2.dto.mis.agents;

import java.util.ArrayList;
import java.util.Collection;

public class GSADetailDTO {

	private String turnOver;
	private String employees;
	private String branches;
	private String periodStart;
	private String periodEnd;
	private String currencyCode;
	private Collection<String> management = new ArrayList<String>();
	private Collection<AgentSummary> agentSummaryList = new ArrayList<AgentSummary>();

	// outstanding payment info
	private String totalCollection;
	private String totalSettlement;
	private String totalNonInvoiced;
	private String outstanding30;
	private String outstanding60;
	private String outstanding90;
	private String outstandingOther;

	public String getTurnOver() {
		return turnOver;
	}

	public void setTurnOver(String turnOver) {
		this.turnOver = turnOver;
	}

	public String getEmployees() {
		return employees;
	}

	public void setEmployees(String employees) {
		this.employees = employees;
	}

	public String getBranches() {
		return branches;
	}

	public void setBranches(String branches) {
		this.branches = branches;
	}

	public Collection<String> getManagement() {
		return management;
	}

	public void setManagement(Collection<String> management) {
		this.management = management;
	}

	public void addManagement(String name) {
		this.management.add(name);
	}

	public String getPeriodStart() {
		return periodStart;
	}

	public void setPeriodStart(String periodStart) {
		this.periodStart = periodStart;
	}

	public String getPeriodEnd() {
		return periodEnd;
	}

	public void setPeriodEnd(String periodEnd) {
		this.periodEnd = periodEnd;
	}

	public Collection<AgentSummary> getAgentSummaryList() {
		return agentSummaryList;
	}

	public void setAgentSummaryList(Collection<AgentSummary> agentSummaryList) {
		this.agentSummaryList = agentSummaryList;
	}

	public void addAgentSummary(AgentSummary agentSummary) {
		this.agentSummaryList.add(agentSummary);
	}

	public String getTotalCollection() {
		return totalCollection;
	}

	public void setTotalCollection(String totalCollection) {
		this.totalCollection = totalCollection;
	}

	public String getOutstanding30() {
		return outstanding30;
	}

	public void setOutstanding30(String outstanding30) {
		this.outstanding30 = outstanding30;
	}

	public String getOutstanding60() {
		return outstanding60;
	}

	public void setOutstanding60(String outstanding60) {
		this.outstanding60 = outstanding60;
	}

	public String getOutstanding90() {
		return outstanding90;
	}

	public void setOutstanding90(String outstanding90) {
		this.outstanding90 = outstanding90;
	}

	public String getOutstandingOther() {
		return outstandingOther;
	}

	public void setOutstandingOther(String outstandingOther) {
		this.outstandingOther = outstandingOther;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getTotalSettlement() {
		return totalSettlement;
	}

	public void setTotalSettlement(String totalSettlement) {
		this.totalSettlement = totalSettlement;
	}

	public String getTotalNonInvoiced() {
		return totalNonInvoiced;
	}

	public void setTotalNonInvoiced(String totalNonInvoiced) {
		this.totalNonInvoiced = totalNonInvoiced;
	}
}
