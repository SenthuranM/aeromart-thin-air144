package com.isa.thinair.airadmin.core.web.generator.master;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.model.CurrencyForDisplay;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.paymentbroker.api.dto.IPGPaymentOptionDTO;

/**
 * @author Shakir
 * 
 */
public class CurrencyHTMLGenerator {

	private static Log log = LogFactory.getLog(CurrencyHTMLGenerator.class);

	private static String MODULE_XBE = "XBE";
	private static String MODULE_IBE = "IBE";
	private static String clientErrors;

	/**
	 * Gets the Currency Grid Data Array for a Search critiriea
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array Containing the Currency Grid Data
	 * @throws ModuleException
	 *             the ModuleException
	 */
	@SuppressWarnings("unchecked")
	public final String getCurrencyRowHtml(HttpServletRequest request) throws ModuleException {

		log.debug("***********inside CurrencyHTMLGenerator.getCurrencyRowHtml()");
		int RecNo;
		Collection<Currency> listArr = null;
		if (request.getParameter("hdnRecNo") == null) {
			RecNo = 1;
		} else {
			RecNo = Integer.parseInt(request.getParameter("hdnRecNo"));
		}

		List<ModuleCriterion> critrian = new ArrayList<ModuleCriterion>();
		ModuleCriterion MCname = new ModuleCriterion();

		// Set criterian - Name
		MCname.setCondition(ModuleCriterion.CONDITION_LIKE_IGNORECASE);
		MCname.setFieldName("currencyCode");

		String strCurrencyCodeSearch = request.getParameter("txtCurrencyCodeSearch");

		if (strCurrencyCodeSearch != null && !strCurrencyCodeSearch.equals("")) {
			List<String> value = new ArrayList<String>();
			value.add("%" + strCurrencyCodeSearch.toUpperCase() + "%");
			MCname.setValue(value);
			critrian.add(MCname);
		}

		ModuleCriterion MCCode = new ModuleCriterion();

		// Set criterian - Code
		MCCode.setCondition(ModuleCriterion.CONDITION_LIKE_IGNORECASE);
		MCCode.setFieldName("currencyDescriprion");

		String strCurrencyDescSearch = request.getParameter("txtDescriptionSearch");

		if (strCurrencyDescSearch != null && !strCurrencyDescSearch.equals("")) {
			List<String> value = new ArrayList<String>();
			value.add("%" + strCurrencyDescSearch + "%");
			MCCode.setValue(value);
			critrian.add(MCCode);
		}

		ModuleCriterion MCStatus = new ModuleCriterion();

		// Set criterian - status
		MCStatus.setCondition(ModuleCriterion.CONDITION_LIKE_IGNORECASE);
		MCStatus.setFieldName("status");
		String strCurrencyStatus = request.getParameter("selStatus");
		if (strCurrencyStatus != null && !strCurrencyStatus.equals("")) {
			List<String> value = new ArrayList<String>();
			value.add("%" + strCurrencyStatus + "%");
			MCStatus.setValue(value);
			critrian.add(MCStatus);
		} else if (strCurrencyStatus == null) {// this is first load coming as null
			List<String> value = new ArrayList<String>();
			value.add("ACT");
			MCStatus.setValue(value);
			critrian.add(MCStatus);
		}

		ModuleCriterion MCFrom = new ModuleCriterion();

		// Set criterian - Date range from
		MCFrom.setCondition(ModuleCriterion.CONDITION_LIKE_IGNORECASE);
		MCFrom.setFieldName("effectiveFrom");
		String strCurrencyFrom = request.getParameter("txtDateFrom");
		if (strCurrencyFrom != null && !strCurrencyFrom.equals("")) {
			List<Date> value = new ArrayList<Date>();
			String dateArr[] = strCurrencyFrom.split("/");
			GregorianCalendar cal = new GregorianCalendar(Integer.parseInt(dateArr[2]), Integer.parseInt(dateArr[1]) - 1,
					Integer.parseInt(dateArr[0]));
			value.add(cal.getTime());
			MCFrom.setValue(value);
			critrian.add(MCFrom);
		}

		ModuleCriterion MCTo = new ModuleCriterion();

		// Set criterian -date range to
		MCTo.setCondition(ModuleCriterion.CONDITION_LIKE_IGNORECASE);
		MCTo.setFieldName("effectiveTo");
		String strCurrencyTo = request.getParameter("txtDateTo");
		if (strCurrencyTo != null && !strCurrencyTo.equals("")) {
			List<Date> value = new ArrayList<Date>();
			String dateArr[] = strCurrencyTo.split("/");
			GregorianCalendar cal = new GregorianCalendar(Integer.parseInt(dateArr[2]), Integer.parseInt(dateArr[1]) - 1,
					Integer.parseInt(dateArr[0]));
			value.add(cal.getTime());
			MCTo.setValue(value);
			critrian.add(MCTo);
		}

		Page page = null;
		page = ModuleServiceLocator.getCommonServiceBD().getCurrencies(critrian, RecNo - 1, 20);

		String strJavascriptTotalNoOfRecs = "";
		if (RecNo > page.getTotalNoOfRecords()) {
			page = ModuleServiceLocator.getCommonServiceBD().getCurrencies(critrian, 0, 20);
			strJavascriptTotalNoOfRecs = "var totalRecords = " + page.getTotalNoOfRecords() + ";";
		} else {
			int totRec = page.getTotalNoOfRecords();
			strJavascriptTotalNoOfRecs = "var totalRecords = " + totRec + ";";
		}
		if (page != null) {
			request.setAttribute(WebConstants.REQ_TOTAL_RECORDS, strJavascriptTotalNoOfRecs);
			listArr = page.getPageData();
		}
		List<CurrencyForDisplay> l = ModuleServiceLocator.getTranslationBD().getCurrencies();

		return createCurrenyGridData(listArr, l);
	}

	/**
	 * Gets the payment gateway details
	 * 
	 */

	public static List<IPGPaymentOptionDTO> getPaymentGatewatDetails(String modulecode) throws ModuleException {

		return ModuleServiceLocator.getPaymentBrokerBD().getPaymentGateways(modulecode);
	}

	/**
	 * Creates the Currency Grid Data Array from Collection of Currencies
	 * 
	 * @param currencies
	 *            the Collection of Currency
	 * @return String the Created array of Grid Data
	 */
	private static String createCurrenyGridData(Collection<Currency> currencies, List<CurrencyForDisplay> currencyForDisplayList)
			throws ModuleException {
		StringBuffer sb = new StringBuffer("var arrData = new Array();");
		sb.append("var arrExRateData = new Array();");
		final String baseCurrencyYes = "Y";
		final String baseCurrencyNo = "N";
		String defaultBaseCurrency = CommonsServices.getGlobalConfig().getBizParam(SystemParamKeys.BASE_CURRENCY);
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		Currency currency = null;
		int count = 0;
		int chgRateCnt = 0;
		boolean isExpired = false;
		boolean effFromDateEditable = true;
		boolean effToDateEditable = true;
		boolean exRateValueEditable = true;
		boolean statusEditable = true;
		Date currentTimestamp = CalendarUtil.getCurrentSystemTimeInZulu();

		List<IPGPaymentOptionDTO> paymentGwList = getPaymentGatewatDetails(MODULE_XBE);
		HashMap<Integer, String> mapPGXBE = new HashMap<Integer, String>();
		HashMap<Integer, String> mapPGdescXBE = new HashMap<Integer, String>();

		Iterator<IPGPaymentOptionDTO> itPG = paymentGwList.iterator();

		while (itPG.hasNext()) {

			IPGPaymentOptionDTO ipgDefaultPG = (IPGPaymentOptionDTO) itPG.next();

			mapPGXBE.put(ipgDefaultPG.getPaymentGateway(), ipgDefaultPG.getDescription());

			mapPGdescXBE.put(ipgDefaultPG.getPaymentGateway(), ipgDefaultPG.getProviderName());

		}

		List<IPGPaymentOptionDTO> paymentGwListIbe = getPaymentGatewatDetails(MODULE_IBE);
		HashMap<Integer, String> mapPGIBE = new HashMap<Integer, String>();
		HashMap<Integer, String> mapPGdescIBE = new HashMap<Integer, String>();

		Iterator<IPGPaymentOptionDTO> itPGIBE = paymentGwListIbe.iterator();

		while (itPGIBE.hasNext()) {

			IPGPaymentOptionDTO ipgDefaultPG = (IPGPaymentOptionDTO) itPGIBE.next();

			mapPGIBE.put(ipgDefaultPG.getPaymentGateway(), ipgDefaultPG.getDescription());
			mapPGdescIBE.put(ipgDefaultPG.getPaymentGateway(), ipgDefaultPG.getProviderName());

		}

		if (currencies != null) {

			// Haider 09Mar09-------------->
			// Collect airport name for each airport code
			CurrencyForDisplay cfd = null;
			ArrayList<CurrencyForDisplay> currencyFD;// array to hold currency name in all lang for specific currency
														// code
			int size = currencyForDisplayList.size();
			Map<String, ArrayList<CurrencyForDisplay>> currencyLangMap = new HashMap<String, ArrayList<CurrencyForDisplay>>();// <currencyCode,
																																// list
																																// of
																																// currency
																																// name
																																// in
																																// all
																																// lang>
			String currencyCode = "";
			for (int k = 0; k < size; k++) {
				cfd = (CurrencyForDisplay) currencyForDisplayList.get(k);
				currencyCode = cfd.getCurrencyCode();
				currencyFD = currencyLangMap.get(currencyCode);
				if (currencyFD == null) {
					currencyFD = new ArrayList<CurrencyForDisplay>();
					currencyLangMap.put(currencyCode, currencyFD);
				}
				currencyFD.add(cfd);
			}
			// <--------------------------------

			Iterator<Currency> iter = currencies.iterator();
			while (iter.hasNext()) {
				currency = (Currency) iter.next();
				sb.append("arrData[" + count + "] = new Array();");
				sb.append("arrData[" + count + "][1] = '" + currency.getCurrencyCode() + "';");

				if (currency.getCurrencyDescriprion() != null)
					sb.append("arrData[" + count + "][2] = '" + currency.getCurrencyDescriprion() + "';");
				else
					sb.append("arrData[" + count + "][2] = '';");

				if (currency.getCurrencyCode().equalsIgnoreCase(defaultBaseCurrency)) {
					sb.append("arrData[" + count + "][4] = '" + baseCurrencyYes + "';");
				} else {
					sb.append("arrData[" + count + "][4] = '" + baseCurrencyNo + "';");
				}

				sb.append("arrData[" + count + "][5] = '" + AiradminUtils.getModifiedStatus(currency.getStatus()) + "';");

				sb.append("arrData[" + count + "][6] = '" + currency.getVersion() + "';");
				// sb.append("arrData[" + count + "][7] = "+ (currency.getBookVisibililty()==0?"'N'":"'Y'") +";");
				if (currency.getBoundryValue() != null)
					sb.append("arrData[" + count + "][8] = '" + currency.getBoundryValue() + "';");
				else
					sb.append("arrData[" + count + "][8] = '';");
				if (currency.getBreakPoint() != null)
					sb.append("arrData[" + count + "][9] = '" + currency.getBreakPoint() + "';");
				else
					sb.append("arrData[" + count + "][9] = '';");
				sb.append("arrData[" + count + "][10] = " + (currency.getXBEVisibility() == 0 ? "'N'" : "'Y'") + ";");
				sb.append("arrData[" + count + "][11] = " + (currency.getIBEVisibility() == 0 ? "'N'" : "'Y'") + ";");
				sb.append("arrData[" + count + "][12] = " + (currency.getCardPaymentVisibility() == 0 ? "'N'" : "'Y'") + ";");

				sb.append("arrData[" + count + "][13] = new Array();");

				chgRateCnt = 0;
				isExpired = false;
				effFromDateEditable = true;
				effToDateEditable = true;
				exRateValueEditable = true;
				statusEditable = true;
				Collection<CurrencyExchangeRate> curExRates = currency.getExchangeRates();
				boolean isCurDeletable = true;
				BigDecimal currentExRateValue = null;
				if (curExRates != null && curExRates.size() > 0) {
					for (Iterator<CurrencyExchangeRate> curExRatesIt = curExRates.iterator(); curExRatesIt.hasNext();) {
						CurrencyExchangeRate curExRate = (CurrencyExchangeRate) curExRatesIt.next();

						sb.append("arrData[" + count + "][13][" + chgRateCnt + "] = new  Array();");
						sb.append("arrData[" + count + "][13][" + chgRateCnt + "][0] = '" + chgRateCnt + "';");

						String effectiveFrom = dateFormat.format(curExRate.getEffectiveFrom());
						String effectiveTo = dateFormat.format(curExRate.getEffectiveTo());
						sb.append("arrData[" + count + "][13][" + chgRateCnt + "][1] = '" + effectiveFrom + "';");
						sb.append("arrData[" + count + "][13][" + chgRateCnt + "][2] = '" + effectiveTo + "';");

						// sb.append("arrData[" + count + "][13][" + chgRateCnt + "][3] = '" +
						// curExRate.getExchangeRate() + "';");
						sb.append("arrData[" + count + "][13][" + chgRateCnt + "][3] = '" + curExRate.getExrateBaseToCurNumber()
								+ "';");// JIRA : AARESAA-3087
						sb.append("arrData[" + count + "][13][" + chgRateCnt + "][4] = '" + curExRate.getStatus() + "';");
						sb.append("arrData[" + count + "][13][" + chgRateCnt + "][5] = '" + curExRate.getVersion() + "';");
						sb.append("arrData[" + count + "][13][" + chgRateCnt + "][6] = '" + curExRate.getExchangeRateId() + "';");
						if (curExRate.getExrateCurToBaseNumber() == null) {
							sb.append("arrData[" + count + "][13][" + chgRateCnt + "][9] = '';");
						} else {
							sb.append("arrData[" + count + "][13][" + chgRateCnt + "][9] = '"
									+ curExRate.getExrateCurToBaseNumber() + "';");
						}
						isExpired = curExRate.getEffectiveTo().before(currentTimestamp);
						if (isExpired) {
							isCurDeletable = false;
							effFromDateEditable = false;
							effToDateEditable = false;
							exRateValueEditable = false;
							statusEditable = false;
						} else if (curExRate.getEffectiveFrom().before(currentTimestamp)) { // An exchange rate which is
																							// currently effective
							isCurDeletable = false;
							effFromDateEditable = false;
							exRateValueEditable = false;
							effToDateEditable = true;
							statusEditable = true;
						} else { // An exchange rate effective in future
							effFromDateEditable = true;
							exRateValueEditable = true;
							effToDateEditable = true;
							statusEditable = true;
						}

						if (currentExRateValue == null && currentTimestamp.after(curExRate.getEffectiveFrom())
								&& currentTimestamp.before(curExRate.getEffectiveTo()) && currency.getStatus().equals("ACT")
								&& curExRate.getStatus().equals("ACT")) {
							// currentExRateValue = curExRate.getExchangeRate();
							currentExRateValue = curExRate.getExrateBaseToCurNumber();
							/** JIRA : AARESAA-3087 - Lalanthi */
						}

						sb.append("arrData[" + count + "][13][" + chgRateCnt + "][7] = new Array();");
						sb.append("arrData[" + count + "][13][" + chgRateCnt + "][7][0] = '" + (!isExpired ? "Y" : "N") + "';");// IsEditable
						sb.append("arrData[" + count + "][13][" + chgRateCnt + "][7][1] = '" + (effFromDateEditable ? "Y" : "N")
								+ "';");
						sb.append("arrData[" + count + "][13][" + chgRateCnt + "][7][2] = '" + (effToDateEditable ? "Y" : "N")
								+ "';");
						sb.append("arrData[" + count + "][13][" + chgRateCnt + "][7][3] = '" + (exRateValueEditable ? "Y" : "N")
								+ "';");
						sb.append("arrData[" + count + "][13][" + chgRateCnt + "][7][4] = '" + (statusEditable ? "Y" : "N")
								+ "';");
						sb.append("arrData[" + count + "][13][" + chgRateCnt + "][8] = 'UNCHANGED';");
						++chgRateCnt;
					}
				}

				sb.append("arrData[" + count + "][14] = '" + (isCurDeletable ? "Y" : "N") + "';");
				sb.append("arrData[" + count + "][15] = '" + (currentExRateValue != null ? currentExRateValue : "Not Defined")
						+ "';");

				sb.append("arrData[" + count + "][16] = new Array();");
				currencyFD = currencyLangMap.get(currency.getCurrencyCode());
				String ucs = "";
				if (currencyFD != null)
					size = currencyFD.size();
				else
					size = 0;
				for (int a = 0; a < size; a++) {
					cfd = (CurrencyForDisplay) currencyFD.get(a);
					ucs = StringUtil.getUnicode(cfd.getCurrencyNameOl());
					sb.append("arrData[" + count + "][16][" + a + "]=new Array('" + cfd.getLanguageCode() + "','" + ucs + "','"
							+ cfd.getId() + "','" + cfd.getVersion() + "');");
				}

				String strPaymentDet = "";
				String strPGCode = "";

				if (currency.getDefaultIbePGId() != null
						&& !"null".equalsIgnoreCase((String) mapPGdescIBE.get(currency.getDefaultIbePGId().intValue()))) {
					strPaymentDet = (String) mapPGIBE.get(currency.getDefaultIbePGId().intValue());
					strPGCode = (String) mapPGdescIBE.get(currency.getDefaultIbePGId().intValue());
				}

				sb.append("arrData[" + count + "][17] = '" + strPaymentDet + "';");
				sb.append("arrData[" + count + "][18] = '" + currency.getDefaultIbePGId() + "';");

				String strPaymentDetXbe = "";
				String strPGCodeXBE = "";

				if (currency.getDefaultXbePGId() != null
						&& !"null".equalsIgnoreCase((String) mapPGdescXBE.get(currency.getDefaultXbePGId().intValue()))) {
					strPaymentDetXbe = (String) mapPGXBE.get(currency.getDefaultXbePGId().intValue());
					strPGCodeXBE = (String) mapPGdescXBE.get(currency.getDefaultXbePGId().intValue());
				}

				sb.append("arrData[" + count + "][19] = '" + strPaymentDetXbe + "';");
				sb.append("arrData[" + count + "][20] = '" + currency.getDefaultXbePGId() + "';");
				sb.append("arrData[" + count + "][21] = '" + strPGCode + "';");
				sb.append("arrData[" + count + "][22] = '" + strPGCodeXBE + "';");

				// exchange rate automation relevant data
				String exRateAutoUpdateStatus = currency.getAutoExRateEnabled();
				BigDecimal exRateVariance = currency.getExRateUpdateVariance();
				String strExRateVariance = "";
				if (exRateVariance != null) {
					strExRateVariance = exRateVariance.toPlainString();
				}

				sb.append("arrData[" + count + "][23] = '" + exRateAutoUpdateStatus + "';");
				sb.append("arrData[" + count + "][24] = '" + strExRateVariance + "';");
				sb.append("arrData[" + count + "][25] = '" + currency.getDecimalPlaces() + "';");
				sb.append("arrData[" + count + "][26] = '" + currency.getItineraryFareBreakDownEnabled() + "';");

				++count;
			}
		}
		return sb.toString();

	}

	/**
	 * Creates the Client Validations For Currency Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array of Client Validations
	 * @throws ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {
		if (clientErrors == null) {
			Properties moduleErrs = new Properties();

			moduleErrs.setProperty("um.currency.form.basecurrency.cannotedit", "CannotEditBaseCurrency");
			moduleErrs.setProperty("um.currency.form.selectcurrency.edit", "SelectCurrencytoEdit");
			moduleErrs.setProperty("um.currency.form.basecurrency.cannotdelete", "CannotDeleteBaseCurrency");
			moduleErrs.setProperty("um.currency.form.selectcurrency.delete", "SelectCurrencyToDelete");
			moduleErrs.setProperty("um.currency.form.currencycode.empty", "CurrencyCodeFieldIsEmpty");
			moduleErrs.setProperty("um.currency.form.description.empty", "DescriptionFieldIsEmpty");
			moduleErrs.setProperty("um.currency.form.rate.empty", "RateFieldIsEmpty");
			moduleErrs.setProperty("um.currency.form.code.defined", "CodeDefined");
			moduleErrs.setProperty("um.airadmin.delete.confirmation", "deleteConf");

			moduleErrs.setProperty("um.airadmin.form.rate.greater", "rateGreater");
			moduleErrs.setProperty("um.airadmin.form.boundary.greater", "boundGreater");
			moduleErrs.setProperty("um.airadmin.form.breakpoint.greater", "bpGreater");
			moduleErrs.setProperty("um.airadmin.form.rate.invalid", "rateInvalid");
			moduleErrs.setProperty("um.airadmin.form.bound.invalid", "boundInvalid");
			moduleErrs.setProperty("um.airadmin.form.bp.invalid", "bpInvalid");
			moduleErrs.setProperty("um.airadmin.form.bp.notempty", "bpNotempty");
			moduleErrs.setProperty("um.airadmin.form.bp.bpGreater", "bpGreater");
			moduleErrs.setProperty("um.airadmin.form.bound.notempty", "boundNotEmpty");
			moduleErrs.setProperty("um.currency.exrate.noteditable", "NotAllowedToEditExRate");

			moduleErrs.setProperty("um.currency.exrate.fromblank", "fromdateblank");
			moduleErrs.setProperty("um.currency.exrate.toblank", "todateblank");
			moduleErrs.setProperty("um.currency.exrate.fromgreat", "fromdategreater");
			moduleErrs.setProperty("um.currency.exrate.invalidfromat", "formatincorrect");
			moduleErrs.setProperty("um.currency.exrate.exchangeblank", "rateblank");
			moduleErrs.setProperty("um.currency.exrate.exchangezero", "ratezero");
			moduleErrs.setProperty("um.currency.exrate.toless", "toless");
			moduleErrs.setProperty("um.currency.exrate.fromgless", "fromless");
			moduleErrs.setProperty("um.currency.exrate.gridblank", "gridblank");

			moduleErrs.setProperty("um.airadmin.form.pg.ibepg", "ibepg");

			clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}
		return clientErrors;
	}
}