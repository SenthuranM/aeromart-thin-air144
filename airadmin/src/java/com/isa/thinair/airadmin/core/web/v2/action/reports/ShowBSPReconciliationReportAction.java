/**
 * 
 */
package com.isa.thinair.airadmin.core.web.v2.action.reports;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airadmin.core.web.v2.util.ReportingUtil;
import com.isa.thinair.airreservation.api.model.ReservationTnxNominalCode;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.invoicing.api.model.bsp.BSPReconciliationRecordDTO;
import com.isa.thinair.invoicing.api.model.bsp.BSPTransactionDataDTO;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;

/**
 * @author Janaka Padukka
 * 
 */

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Results({ @Result(name = S2Constants.Result.SUCCESS, value = S2Constants.Jsp.Reporting.RPTS_BSP_RECON_JSP),
		@Result(name = S2Constants.Result.ERROR, value = AdminStrutsConstants.AdminJSP.ERROR_JSP) })
public class ShowBSPReconciliationReportAction extends BaseRequestResponseAwareAction {

	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();
	private static String BSP_RECON_REPORT_TEMPLATE = "BSPReconReport.jasper";
	private static String BSP_RECON_ALL_COUNTRY_REPORT_TEMPLATE = "BSPReconReportAllCountry.jasper";
	private static String OUTPUT_FILE_NAME = "BSPReconciliationReport";
	private static String SELECT_ALL_COUNTRY = "All";

	private String tnxDateFrom;
	private String tnxDateTo;
	private String dpcProcessedDateFrom;
	private String dpcProcessedDateTo;
	private String selBspTnxType;
	private String selBspCountryCode;
	private String selReconStatus;
	private String radReportOption;
	private String selEntity;
	private String hdnEntityText;

	public String execute() {
		String forward = S2Constants.Result.SUCCESS;
		try {
			setReportView();
			return null;
		} catch (ModuleException e) {
			forward = S2Constants.Result.ERROR;
		}
		return forward;
	}

	private void setReportView() throws ModuleException {
		ReportsSearchCriteria search = new ReportsSearchCriteria();
		String id = "UC_REPM_087";
		String strLogo = AppSysParamsUtil.getReportLogo(false);
		String image = ReportingUtil.ReportingLocations.image_loc + AppSysParamsUtil.getReportLogo(true);
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);

		if (selBspCountryCode != null && selBspCountryCode.equals(SELECT_ALL_COUNTRY)) {
			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs(WebConstants.REPORT_REF,
					ReportsHTMLGenerator.getReportTemplate(BSP_RECON_ALL_COUNTRY_REPORT_TEMPLATE));
		} else {

			ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs(WebConstants.REPORT_REF,
					ReportsHTMLGenerator.getReportTemplate(BSP_RECON_REPORT_TEMPLATE));
		}
		search.setDateRangeFrom(this.tnxDateFrom);
		search.setDateRangeTo(this.tnxDateTo);
		search.setDateRange2From(this.dpcProcessedDateFrom);
		search.setDateRange2To(this.dpcProcessedDateTo);
		search.setStatus(this.selReconStatus);
		search.setCountryCode(this.selBspCountryCode);

		List<Integer> bspNominalCodes = new ArrayList<Integer>();
		if (!StringUtil.isNullOrEmpty(this.selBspTnxType)) {
			if (BSPTransactionDataDTO.TransactionType.TKTT.toString().equals(this.selBspTnxType)) {
				bspNominalCodes.add(ReservationTnxNominalCode.BSP_PAYMENT.getCode());
			} else if (BSPTransactionDataDTO.TransactionType.RFND.toString().equals(this.selBspTnxType)) {
				bspNominalCodes.add(ReservationTnxNominalCode.REFUND_BSP.getCode());
			}
		} else {
			bspNominalCodes.add(ReservationTnxNominalCode.BSP_PAYMENT.getCode());
			bspNominalCodes.add(ReservationTnxNominalCode.REFUND_BSP.getCode());
		}
		search.setNominalCodes(bspNominalCodes);
		
		if (strlive != null) {
			if (strlive.equals(WebConstants.REP_LIVE)) {
				search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
			} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
				search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
			}
		}

		search.setEntity(this.selEntity);
		List<BSPReconciliationRecordDTO> reportData = ModuleServiceLocator.getInvoicingBD()
				.getBSPReconciliationReportData(search);

		parameters.put("ID", id);
		parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
		parameters.put("TNX_FROM_DATE", this.tnxDateFrom);
		parameters.put("TNX_TO_DATE", this.tnxDateTo);
		parameters.put("PROCESSED_FROM_DATE", this.dpcProcessedDateFrom);
		parameters.put("PROCESSED_TO_DATE", this.dpcProcessedDateTo);
		parameters.put("ENTITY", this.hdnEntityText);

		if (radReportOption.trim().equals("HTML")) {
			parameters.put("IMG", image);
			ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport(WebConstants.REPORT_REF, parameters,
					reportData, null, null, response);
		} else if (radReportOption.trim().equals("PDF")) {
			response.reset();
			response.addHeader("Content-Disposition", "filename=" + OUTPUT_FILE_NAME + ".pdf");
			image = ReportsHTMLGenerator.getReportTemplate(strLogo);
			parameters.put("IMG", image);
			ModuleServiceLocator.getReportingFrameworkBD().createPDFReport(WebConstants.REPORT_REF, parameters,
					reportData, response);
		} else if (radReportOption.trim().equals("EXCEL")) {
			response.reset();
			response.addHeader("Content-Disposition", "attachment;filename=" + OUTPUT_FILE_NAME + ".xls");
			ModuleServiceLocator.getReportingFrameworkBD().createXLSReport(WebConstants.REPORT_REF, parameters,
					reportData, response);
		} else if (radReportOption.trim().equals("CSV")) {
			response.reset();
			response.addHeader("Content-Disposition", "attachment;filename=" + OUTPUT_FILE_NAME + ".csv");
			ModuleServiceLocator.getReportingFrameworkBD().createCSVReport(WebConstants.REPORT_REF, parameters,
					reportData, response);
		}
	}

	public void setTnxDateFrom(String tnxDateFrom) {
		this.tnxDateFrom = tnxDateFrom;
	}

	public void setTnxDateTo(String tnxDateTo) {
		this.tnxDateTo = tnxDateTo;
	}

	public void setDpcProcessedDateFrom(String dpcProcessedDateFrom) {
		this.dpcProcessedDateFrom = dpcProcessedDateFrom;
	}

	public void setDpcProcessedDateTo(String dpcProcessedDateTo) {
		this.dpcProcessedDateTo = dpcProcessedDateTo;
	}

	public void setSelBspTnxType(String selBspTnxType) {
		this.selBspTnxType = selBspTnxType;
	}

	public void setSelBspCountryCode(String selBspCountryCode) {
		this.selBspCountryCode = selBspCountryCode;
	}

	public void setSelReconStatus(String selReconStatus) {
		this.selReconStatus = selReconStatus;
	}

	public void setRadReportOption(String radReportOption) {
		this.radReportOption = radReportOption;
	}

	public void setSelEntity(String selEntity) {
		this.selEntity = selEntity;
	}

	public void setHdnEntityText(String hdnEntityText) {
		this.hdnEntityText = hdnEntityText;
	}
	
	
}
