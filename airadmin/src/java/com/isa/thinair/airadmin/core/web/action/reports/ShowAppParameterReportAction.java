/**
 * 
 */
package com.isa.thinair.airadmin.core.web.action.reports;

import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.handler.reports.AppParameterRH;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;

/**
 * The struts action class for Application Parameter Report.
 * 
 * @author janandith jayawardena
 * 
 */

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Results({
		@Result(name = AdminStrutsConstants.AdminAction.SUCCESS, value = AdminStrutsConstants.AdminJSP.REPORTING_APP_PARAMS_LIST_JSP),
		@Result(name = AdminStrutsConstants.AdminAction.ERROR, value = AdminStrutsConstants.AdminJSP.ERROR_JSP) })
public class ShowAppParameterReportAction extends BaseRequestResponseAwareAction {

	public String execute() throws Exception {
		return AppParameterRH.execute(request, response);
	}
}
