package com.isa.thinair.airadmin.core.web.v2.util;

import java.nio.charset.Charset;
import java.sql.NClob;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.engine.jdbc.NonContextualLobCreator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.isa.thinair.airadmin.api.FarePaxTypeConfigDTO;
import com.isa.thinair.airadmin.api.FareRuleCommentDTO;
import com.isa.thinair.airadmin.api.FareRuleDTO;
import com.isa.thinair.airadmin.api.FareRuleFeeDTO;
import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airmaster.api.service.AircraftBD;
import com.isa.thinair.airpricing.api.dto.FareDTO;
import com.isa.thinair.airpricing.api.dto.ManageFareDTO;
import com.isa.thinair.airpricing.api.dto.ondcharge.OndChargeRateTO;
import com.isa.thinair.airpricing.api.dto.ondcharge.OndChargeTO;
import com.isa.thinair.airpricing.api.dto.ondcharge.OriginDestinationTO;
import com.isa.thinair.airpricing.api.model.Channel;
import com.isa.thinair.airpricing.api.model.Fare;
import com.isa.thinair.airpricing.api.model.FareRule;
import com.isa.thinair.airpricing.api.model.FareRuleComment;
import com.isa.thinair.airpricing.api.model.FareRuleFee;
import com.isa.thinair.airpricing.api.model.FareRulePax;
import com.isa.thinair.airpricing.api.service.FareBD;
import com.isa.thinair.airpricing.api.service.FareRuleBD;
import com.isa.thinair.airreservation.api.dto.OnHoldReleaseTimeDTO;
import com.isa.thinair.commons.api.constants.CommonsConstants;
import com.isa.thinair.commons.api.constants.DayOfWeek;
import com.isa.thinair.commons.api.dto.ChargeApplicabilityTO;
import com.isa.thinair.commons.api.dto.Frequency;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

public class FareUtil {

	private static Log log = LogFactory.getLog(FareUtil.class);
	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();
	private static final GlobalConfig config = ModuleServiceLocator.getGlobalConfig();

	public static boolean isChildVisible() {
		String strVisible = AiradminModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.CHILD_VISIBILITY);
		return (strVisible != null && strVisible.equalsIgnoreCase("N")) ? false : true;
	}

	public static String getSystemDate() {
		SimpleDateFormat dtfmt = new SimpleDateFormat("dd/MM/yyyy");
		return dtfmt.format(new Date());
	}

	public static boolean isEnableSameFareChecking() {
		String strEnableSameFare = AiradminModuleUtils.getGlobalConfig().getBizParam(
				SystemParamKeys.ENABLE_SAME_FARE_MODIFICATION);
		return (strEnableSameFare != null && strEnableSameFare.equalsIgnoreCase("N")) ? false : true;
	}

	public static boolean isEnableLinkFare() {
		String strEnableLinkFare = AiradminModuleUtils.getGlobalConfig().getBizParam(
				SystemParamKeys.ENABLE_LINKED_FARES_BASED_ON_MASTER_FARES);
		return (strEnableLinkFare != null && strEnableLinkFare.equalsIgnoreCase("N")) ? false : true;
	}

	public static List<String[]> getBookingClassList() throws ModuleException {
		List<Map<String, String>> lstBC = SelectListGenerator.createActiveBookingClassList();
		AircraftBD aircraftBD = ModuleServiceLocator.getAircraftServiceBD();
		Iterator<Map<String, String>> ite = lstBC.iterator();
		List<String[]> bookingClassList = new ArrayList<String[]>();
		String bookingClass[] = null;
		while (ite.hasNext()) {
			Map<String, String> keyValues = ite.next();
			String bc = keyValues.get("BOOKING_CODE");
			String standard = keyValues.get("STANDARD_CODE");
			String fixed = keyValues.get("FIXED_FLAG");
			String cos = keyValues.get("CABIN_CLASS_CODE");
			String status = keyValues.get("STATUS");
			String alloc = keyValues.get("ALLOCATION_TYPE");
			String bctype = keyValues.get("BC_TYPE");
			String paxType = keyValues.get("PAX_TYPE");
			String paxCat = keyValues.get("PAX_CATEGORY_CODE");
			String fareCat = keyValues.get("FARE_CAT_ID");
			if (bctype != null && bctype.equals("OPENRT")) {
				continue;
			}
			String value = "";
			if (cos != null && !cos.equals("")) {
				String cabin = CommonsServices.getGlobalConfig().getActiveCabinClassesMap().get(cos);
				value = standard + "," + fixed + "," + cabin + "," + status + "," + alloc + "," + bctype;
				if (paxType != null && (paxType.trim().equals("GOSHOW") || paxType.trim().equals("INTERLINE"))) {
					value += "," + paxType;
				} else {
					value += ",";
				}
				value += "," + paxCat;
				value += "," + fareCat;
				bookingClass = new String[2];
				bookingClass[0] = value;
				bookingClass[1] = bc;
				bookingClassList.add(bookingClass);
			}
		}
		return bookingClassList;
	}

	public static boolean isOpenReturnSupport() {
		String strVisible = AiradminModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.OPEN_ENDED_RETURN_SUPPORT);
		return (strVisible != null && strVisible.equalsIgnoreCase("N")) ? false : true;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void addFareRuleData(FareRule fareRule, Map ruleMap, SimpleDateFormat dateForamt) throws ModuleException {
		ruleMap.put("fareid", fareRule.getFareRuleId());
		ruleMap.put("ruleCode", fareRule.getFareRuleCode());
		ruleMap.put("description", fareRule.getFareRuleDescription());
		ruleMap.put("fareBasisCode", fareRule.getFareBasisCode());
		ruleMap.put("oneWayReturn", fareRule.getReturnFlag() != CommonsConstants.CHR_YES ? "One Way" : "Return");
		ruleMap.put("allowHalfRT", fareRule.getHalfReturn());

		if (fareRule.getAdvanceBookingDays() != null && fareRule.getAdvanceBookingDays() > 0) {
			ruleMap.put("advanceBooking", parseMins(fareRule.getAdvanceBookingDays().longValue()));
		}

		String outFreq = getFrequencyForFare(fareRule, false);
		String outArr[] = outFreq.split(":");
		String outFrequency = "";
		String out = "";
		for (String element : outArr) {
			outFrequency += element;
		}
		if (fareRule.getReturnFlag() == CommonsConstants.CHR_YES) {
			String inFrequency = "";
			String inFreq = getFrequencyForFare(fareRule, true);
			String arrIn[] = inFreq.split(":");
			for (String element : arrIn) {
				inFrequency += element;
			}
			ruleMap.put("validDaysOfWeek", "Out- " + outFrequency + "<br>In- " + inFrequency);

			if (fareRule.getOutboundDepatureTimeFrom() != null && !fareRule.getOutboundDepatureTimeFrom().equals("")) {
				out += dateForamt.format(fareRule.getOutboundDepatureTimeFrom()) + ",";
			}
			if (fareRule.getOutboundDepatureTimeTo() != null && !fareRule.getOutboundDepatureTimeTo().equals("")) {
				out += dateForamt.format(fareRule.getOutboundDepatureTimeTo()) + ",";
			}
			if (fareRule.getInboundDepatureTimeFrom() != null && !fareRule.getInboundDepatureTimeFrom().equals("")) {
				out += dateForamt.format(fareRule.getInboundDepatureTimeFrom()) + ",";
			}
			if (fareRule.getInboundDepatureTimeTo() != null && !fareRule.getInboundDepatureTimeTo().equals("")) {
				out += dateForamt.format(fareRule.getInboundDepatureTimeTo());
			}

			if (fareRule.getMaximumStayoverMonths() != null && fareRule.getMaximumStayoverMins() != null) {
				ruleMap.put("maxStayOver",
						fareRule.getMaximumStayoverMonths() + ":" + parseMins(fareRule.getMaximumStayoverMins()));
			} else if (fareRule.getMaximumStayoverMins() != null) {
				ruleMap.put("maxStayOver", "0:" + parseMins(fareRule.getMaximumStayoverMins()));
			} else if (fareRule.getMaximumStayoverMonths() != null) {
				ruleMap.put("maxStayOver", fareRule.getMaximumStayoverMonths() + ":0");
			} else {
				ruleMap.put("maxStayOver", "");
			}

			if (fareRule.getMinimumStayoverMonths() != null && fareRule.getMinimumStayoverMins() != null) {
				ruleMap.put("minStayOver",
						fareRule.getMinimumStayoverMonths() + ":" + parseMins(fareRule.getMinimumStayoverMins()));
			} else if (fareRule.getMinimumStayoverMins() != null) {
				ruleMap.put("minStayOver", "0:" + parseMins(fareRule.getMinimumStayoverMins()));
			} else if (fareRule.getMinimumStayoverMonths() != null) {
				ruleMap.put("maxStayOver", fareRule.getMinimumStayoverMonths() + ":0");
			} else {
				ruleMap.put("minStayOver", "");
			}
		} else {
			ruleMap.put("validDaysOfWeek", "Out- " + outFrequency + "<br>");
			if (fareRule.getOutboundDepatureTimeFrom() != null && !fareRule.getOutboundDepatureTimeFrom().equals("")) {
				out += dateForamt.format(fareRule.getOutboundDepatureTimeFrom()) + ",";
			}
			if (fareRule.getOutboundDepatureTimeTo() != null && !fareRule.getOutboundDepatureTimeTo().equals("")) {
				out += dateForamt.format(fareRule.getOutboundDepatureTimeTo());
			}
		}
		ruleMap.put("validDaysOfTime", out);

		Set visibilites = fareRule.getVisibleChannelIds();
		List lstVisibilites = SelectListGenerator.createSalesChannels();
		String visibility = "";
		String visibilityCodes = "";
		if (lstVisibilites.size() != visibilites.size()) {
			if (visibilites != null) {
				if (visibilites.size() > 0) {
					Collection<Channel> colVisibilites = ModuleServiceLocator.getRuleBD().getChannels(new ArrayList(visibilites));
					Iterator<Channel> iteVisibilites = colVisibilites.iterator();
					while (iteVisibilites.hasNext()) {
						Channel channel = iteVisibilites.next();
						visibility += channel.getChannelDescription() + ", ";
						visibilityCodes += channel.getChannelId() + ",";
					}
				}
			}

			if (visibility.indexOf(',') != -1) {
				ruleMap.put("visibility", visibility.substring(0, visibility.lastIndexOf(',')));
			} else {
				ruleMap.put("visibility", visibility);
			}
		} else {
			ruleMap.put("visibility", "");
		}
		ruleMap.put("visibilityCodes", visibilityCodes);
		Set<String> visibleAgents = fareRule.getVisibileAgentCodes();
		if (visibleAgents.size() > 0) {
			ruleMap.put("agtFR", "Y");
		} else {
			ruleMap.put("agtFR", "N");
		}
		ruleMap.put("visibleAgents", visibleAgents);
		if ("ACT".equalsIgnoreCase(fareRule.getStatus().trim())) {
			ruleMap.put("status", "Active");
		} else {
			ruleMap.put("status", "In-Active");
		}

		ruleMap.put("paxCat", fareRule.getPaxCategoryCode());
		ruleMap.put("comments", fareRule.getRulesComments());

		String openRTFlag = "N";
		if (fareRule.getOPenRetrun()) {
			openRTFlag = "Y";
			ruleMap.put("ortPeriod",
					getIntValue(fareRule.getOpenRenConfPeriodMonths()) + ":" + parseMins(fareRule.getOpenReturnConfPeriod()));
		}
		ruleMap.put("ortConfirm", openRTFlag);
		String modBufTimeInt = getModifyBufferString(fareRule.getInternationalModifyBufferTime());
		if (!modBufTimeInt.equals("0:0:0")) {
			ruleMap.put("modBufferTimeInt", modBufTimeInt);
		}

		String modBufTimeDom = getModifyBufferString(fareRule.getDomesticModifyBufferTime());
		if (!modBufTimeDom.equals("0:0:0")) {
			ruleMap.put("modBufferTimeDom", modBufTimeDom);
		}

		Integer loadFactorMin = fareRule.getLoadFactorMin();
		Integer loadFactorMax = fareRule.getLoadFactorMax();
		if (loadFactorMin != null && loadFactorMax != null) {
			ruleMap.put("loadFactor", loadFactorMin + "-" + loadFactorMax);
		} else {
			ruleMap.put("loadFactor", "");
		}

		Set<Integer> tmpFlexiCodes = fareRule.getFlexiCodes();
		Iterator<Integer> iter = tmpFlexiCodes.iterator();
		String strFlexiCodes = "";
		while (iter.hasNext()) {
			strFlexiCodes += iter.next().toString();
			if (iter.hasNext()) {
				strFlexiCodes += ",";
			}
		}
		ruleMap.put("flexiCode", strFlexiCodes);
		ruleMap.put("fareCat", fareRule.getFareCategoryCode());
		ruleMap.put("printExp", fareRule.getPrintExpDate());
		ruleMap.put("allowModDate", fareRule.getModifyByDate());
		ruleMap.put("allowModOND", fareRule.getModifyByRoute());

		if ((fareRule.getModificationChargeAmount() == 0.0 && fareRule.getCancellationChargeAmount() == 0.0)
				&& (fareRule.getModChargeAmountInLocal() != null && fareRule.getModChargeAmountInLocal() == 0.0
						&& fareRule.getCnxChargeAmountInLocal() != null && fareRule.getCnxChargeAmountInLocal() == 0.0)) {
			ruleMap.put("chargeRest", "N");
		} else {
			ruleMap.put("chargeRest", "Y");
		}

		ruleMap.put("modificationChargeAmount", fareRule.getModificationChargeAmount());
		ruleMap.put("modificationChargeType", fareRule.getModificationChargeType());
		ruleMap.put("modificationChargeLocal", fareRule.getModChargeAmountInLocal());
		ruleMap.put("minModChargeAmount", fareRule.getMinModificationAmount());
		ruleMap.put("maxModChargeAmount", fareRule.getMaximumModificationAmount());
		ruleMap.put("cancellationChargeAmount", fareRule.getCancellationChargeAmount());
		ruleMap.put("cancellationChargeType", fareRule.getCancellationChargeType());
		ruleMap.put("cancellationChargeLocal", fareRule.getCnxChargeAmountInLocal());
		ruleMap.put("minCnxChargeAmount", fareRule.getMinCancellationAmount());
		ruleMap.put("maxCnxChargeAmount", fareRule.getMaximumCancellationAmount());

		ruleMap.put("nameChangeChargeAmount", fareRule.getNameChangeChargeAmount());
		ruleMap.put("nameChangeChargeType", fareRule.getNameChangeChargeType());
		ruleMap.put("nameChangeChargeLocal", fareRule.getNameChangeChargeAmountLocal());
		ruleMap.put("minNCCAmount", fareRule.getMinNCCAmount());
		ruleMap.put("maxNCCAmount", fareRule.getMaxNCCAmount());

		ruleMap.put("localCurrencyCode", fareRule.getLocalCurrencyCode());
		ruleMap.put("minFareDisc", fareRule.getFareDiscountMin());
		ruleMap.put("maxFareDisc", fareRule.getFareDiscountMax());
		ruleMap.put("agentInstructions", fareRule.getAgentInstructions());
		ruleMap.put("agentCommissionAmount", fareRule.getAgentCommissionAmount());
		ruleMap.put("agentCommissionType", fareRule.getAgentCommissionType());
		ruleMap.put("agentCommissionAmountLocal", fareRule.getAgentCommissionAmountInLocal());
		ruleMap.put("twlHrsCutOverApplied", fareRule.getFeeTwlHrsCutOverApplied());
		ruleMap.put("fareDiscountMin", fareRule.getFareDiscountMin());
		ruleMap.put("fareDiscountMax", fareRule.getFareDiscountMax());

		List appModelList = new ArrayList();
		Collection<FareRulePax> paxDetails = fareRule.getPaxDetails();
		if (paxDetails != null && !paxDetails.isEmpty()) {
			Map appModelMap = null;
			for (FareRulePax fareRulePax : paxDetails) {
				FareRulePax def = fareRulePax;
				appModelMap = new HashMap();
				appModelMap.put("paxID", def.getFarePaxID());
				appModelMap.put("paxVersion", def.getVersion());
				appModelMap.put("paxType", def.getPaxType());
				appModelMap.put("fare", def.getApplyFares());
				appModelMap.put("ref", def.getRefundableFares());
				appModelMap.put("mod", def.getApplyModCharges());
				appModelMap.put("cnx", def.getApplyCNXCharges());
				appModelMap.put("noshow", def.getNoShowChrge());
				appModelMap.put("noshowBasis", def.getNoShowChgBasis());
				appModelMap.put("noshowBreak", def.getNoShowChgBreakPoint());
				appModelMap.put("noshowBoundry", def.getNoShowChgBoundary());
				appModelMap.put("noshowLocal", def.getNoShowChrgeInLocal());
				appModelList.add(appModelMap);
			}
		}
		ruleMap.put("appModel", appModelList);

		List appModelCNXDataList = new ArrayList();
		List appModelMODDataList = new ArrayList();
		List appModelNCCDataList = new ArrayList();
		Collection<FareRuleFee> fareRuleFees = fareRule.getFees();
		if (fareRuleFees != null) {
			for (FareRuleFee fee : fareRuleFees) {
				Map feeMap = new HashMap();
				feeMap.put("fareRuleFeeID", fee.getFareRuleFeeId());
				feeMap.put("feeVersion", fee.getVersion());
				feeMap.put("chargeAmount", fee.getChargeAmount());
				feeMap.put("chargeAmountLocal", fee.getChargeAmountInLocal());
				feeMap.put("chargeAmountType", fee.getChargeAmountType());
				feeMap.put("chargeType", fee.getChargeType());
				feeMap.put("maxPerCharge", fee.getMaximumPerChargeAmt());
				feeMap.put("minPerCharge", fee.getMinimumPerChargeAmt());
				feeMap.put("compareAgainst", fee.getCompareAgainst());
				feeMap.put("status", fee.getStatus());
				feeMap.put("timeType", fee.getTimeType());
				feeMap.put("timeValue", fee.getTimeValue());

				if (FareRuleFee.CHARGE_TYPES.MODIFICATION.equalsIgnoreCase(fee.getChargeType())) {
					appModelMODDataList.add(feeMap);
				} else if (FareRuleFee.CHARGE_TYPES.CANCELLATION.equalsIgnoreCase(fee.getChargeType())) {
					appModelCNXDataList.add(feeMap);
				} else if (FareRuleFee.CHARGE_TYPES.NAME_CHANGE.equalsIgnoreCase(fee.getChargeType())) {
					appModelNCCDataList.add(feeMap);
				}
				
			}
		}
		ruleMap.put("appModelCNXData", appModelCNXDataList);
		ruleMap.put("appModelMODData", appModelMODDataList);
				ruleMap.put("appModelNCCData", appModelNCCDataList);

		Collection<FareRuleComment> fareRuleComments = fareRule.getFareRuleComments();
		List<FareRuleCommentDTO> fareRuleCommentList = new ArrayList<>();
		if (fareRuleComments != null) {
			for (FareRuleComment fareRuleComment : fareRuleComments) {
				FareRuleCommentDTO fareRuleCommentDTO = new FareRuleCommentDTO();
				fareRuleCommentDTO.setFareRuleCommentID(fareRuleComment.getCommentID());
				try {
					fareRuleCommentDTO.setFareRuleComments(StringUtil.getUnicode(fareRuleComment.getComments()));
				} catch (NumberFormatException ex) {
					fareRuleCommentDTO.setFareRuleComments(fareRuleComment.getComments());
					log.debug("Fare rule comment is in pure string for fare rule id" + fareRuleCommentDTO.getFareRuleID());
				}				
				fareRuleCommentDTO.setFareRuleID(fareRuleComment.getFareRule().getFareRuleId());
				fareRuleCommentDTO.setLanguageCode(fareRuleComment.getLanguageCode());
				fareRuleCommentList.add(fareRuleCommentDTO);
			}
		}
		ruleMap.put("fareRuleComments", fareRuleCommentList);
		ruleMap.put("version", fareRule.getVersion());
		ruleMap.put("fareRuleID", fareRule.getFareRuleId());
		ruleMap.put("pointOfSale", fareRule.getPointOfSale());
		ruleMap.put("applicableToCountries", fareRule.getApplicableToAllCountries());
		ruleMap.put("bulkTicket", fareRule.getBulkTicket());
		ruleMap.put("minBulkTicketSeatCount", fareRule.getMinBulkTicketSeatCount());
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void addFareData(ManageFareDTO mFareDTO, Map fareMap, SimpleDateFormat dateForamt) throws ModuleException {

		FareBD fareBD = ModuleServiceLocator.getFareBD();

		fareMap.put("fareId", mFareDTO.getFareId());
		String ondCode = mFareDTO.getOndCode();
		if (ondCode != null) {
			String[] onds = ondCode.split("/");
			if (onds.length == 1) {
				fareMap.put("origin", onds[0]);
			} else if (onds.length == 2) {
				fareMap.put("origin", onds[0]);
				fareMap.put("destination", onds[1]);
			} else if (onds.length == 3) {
				fareMap.put("origin", onds[0]);
				fareMap.put("via1", onds[1]);
				fareMap.put("destination", onds[2]);
			} else if (onds.length == 4) {
				fareMap.put("origin", onds[0]);
				fareMap.put("via1", onds[1]);
				fareMap.put("via2", onds[2]);
				fareMap.put("destination", onds[3]);
			} else if (onds.length == 5) {
				fareMap.put("origin", onds[0]);
				fareMap.put("via1", onds[1]);
				fareMap.put("via2", onds[2]);
				fareMap.put("via3", onds[3]);
				fareMap.put("destination", onds[4]);
			} else if (onds.length == 6) {
				fareMap.put("origin", onds[0]);
				fareMap.put("via1", onds[1]);
				fareMap.put("via2", onds[2]);
				fareMap.put("via3", onds[3]);
				fareMap.put("via4", onds[4]);
				fareMap.put("destination", onds[5]);
			}
		}

		fareMap.put("status", mFareDTO.getStatus());
		fareMap.put("bookingClass", mFareDTO.getBookingClass());
		fareMap.put("classOfService", mFareDTO.getClassOfService());
		fareMap.put("returnFlag", mFareDTO.getReturnFlag());
		fareMap.put("ondCode", mFareDTO.getOndCode());
		fareMap.put("fixedAllocation", mFareDTO.getFixedAllocation());
		fareMap.put("visibilityChannels", getFareVisibility(mFareDTO.getChannelVisibilities()));
		fareMap.put("paxCategoryCode", mFareDTO.getPaxCategoryCode());
		fareMap.put("currencyCode", mFareDTO.getCurrencyCode());
		if (mFareDTO.getSalesValidTo().before(new Date())) {
			fareMap.put("exp", "Y");
		} else {
			fareMap.put("exp", "N");
		}

		FareDTO fDto = fareBD.getFare(mFareDTO.getFareId());
		FareRule fareRule = fDto.getFareRule();

		fareMap.put("fareRuleCode", fareRule.getFareRuleCode());
		fareMap.put("fareRuleId", fareRule.getFareRuleId());
		fareMap.put("validFromDate", mFareDTO.getValidFrom());
		fareMap.put("validToDate", mFareDTO.getValidTo());
		fareMap.put("rtValidFromDate", mFareDTO.getReturnValidFrom());
		fareMap.put("rtValidToDate", mFareDTO.getReturnValidTo());
		fareMap.put("salesValidFromDate", mFareDTO.getSalesValidFrom());
		fareMap.put("salesValidToDate", mFareDTO.getSalesValidTo());

		fareMap.put("modifyToSameFare", mFareDTO.getModToSameFare());

		double fareAmount = fDto.getFare().getFareAmount();
		double childFare = fDto.getFare().getChildFare();
		double infantFare = fDto.getFare().getInfantFare();
		if (fDto.getFare().getLocalCurrencyCode() != null && isShowFareDefInDepAirPCurr()) {
			fareAmount = fDto.getFare().getFareAmountInLocal();
			childFare = fDto.getFare().getChildFareInLocal();
			infantFare = fDto.getFare().getInfantFareInLocal();
		}

		fareMap.put("fareAmount", fareAmount);
		fareMap.put("childFareType", fDto.getFare().getChildFareType());
		fareMap.put("childFareAmount", childFare);
		fareMap.put("infantFareType", fDto.getFare().getInfantFareType());
		fareMap.put("infantFareAmount", infantFare);

		fareMap.put("fareBasisCode", mFareDTO.getFareBasisCode());
		fareMap.put("masterFareRefCode", mFareDTO.getMasterFareRefCode());
		fareMap.put("masterFareId", mFareDTO.getMasterFareID());
		fareMap.put("masterFarePer", mFareDTO.getMasterFarePercentage());
		fareMap.put("version", mFareDTO.getVersion());

		Map fareRuleMap = new HashMap();
		addFareRuleData(fareRule, fareRuleMap, dateForamt);
		fareMap.put("fareRuleData", fareRuleMap);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void addFareChargeData(OriginDestinationTO originDestinationTO, Map fareChargeMap, SimpleDateFormat dateForamt)
			throws ModuleException {

		DecimalFormat formatter = new DecimalFormat("#.00");
		fareChargeMap.put("ondCode", originDestinationTO.getOndCode());
		Map<Integer, ChargeApplicabilityTO> chargeApplicabilitiesMap = globalConfig.getChargeApplicabilities();

		List chargeGroupList = new ArrayList();

		List<OndChargeTO> ondCharges = originDestinationTO.getCharges();
		Iterator<OndChargeTO> iteCharges = ondCharges.iterator();
		OndChargeTO charge = null;
		Map chargeGroupMap = null;

		while (iteCharges.hasNext()) {
			chargeGroupMap = new HashMap();
			charge = (OndChargeTO) iteCharges.next();
			chargeGroupMap.put("chargeGroupCode", charge.getChargeGroupCode());
			chargeGroupMap.put("chargeCode", charge.getChargeCode());
			chargeGroupMap.put("chargeCategoryCode", charge.getChargeCategoryCode());
			chargeGroupMap.put("chargeDescription", charge.getChargeDescription());
			chargeGroupMap.put("chargeRefFlag", charge.getRefundableChargeFlag());
			ChargeApplicabilityTO chargeApplicabilityTO = chargeApplicabilitiesMap.get(charge.getAppliesTo());
			chargeGroupMap.put("chargeApplicability", chargeApplicabilityTO.getAppliesToDesc());
			List<OndChargeRateTO> chargeRates = charge.getChargeRates();
			Iterator<OndChargeRateTO> iteRates = chargeRates.iterator();
			List chargeRateList = new ArrayList();
			Map chargeRateMap = null;
			OndChargeRateTO chargeRate = null;
			while (iteRates.hasNext()) {
				chargeRateMap = new HashMap();
				chargeRate = (OndChargeRateTO) iteRates.next();
				chargeRateMap.put("rateEffFromDate", chargeRate.getChargeEffectiveFromDate());
				chargeRateMap.put("rateEffToDate", chargeRate.getChargeEffectiveToDate());
				chargeRateMap.put("rateValuePerFlag", chargeRate.getValuePercentageFlag());
				chargeRateMap.put("rateValuePer", formatter.format(chargeRate.getChargeValuePercentage()));
				chargeRateList.add(chargeRateMap);
			}
			chargeGroupMap.put("chargeRateList", chargeRateList);
			chargeGroupList.add(chargeGroupMap);
		}

		fareChargeMap.put("chargeGroupList", chargeGroupList);
	}

	private static long getIntValue(Long objVal) {
		long value = 0;
		if (objVal != null) {
			value = objVal;
		}
		return value;
	}

	private static String parseMins(Long totalMinutes) {
		StringBuffer durationFormat = new StringBuffer();
		if (totalMinutes != null) {

			long days = totalMinutes / (24 * 60);
			long hours = (totalMinutes % (24 * 60)) / 60;
			long minutes = (totalMinutes % (24 * 60)) % 60;

			durationFormat.append(days);
			durationFormat.append(":");
			durationFormat.append(hours);
			durationFormat.append(":");
			durationFormat.append(minutes);
		}
		return durationFormat.toString();
	}

	public static List<FarePaxTypeConfigDTO> getPaxFareParams() {
		List<FarePaxTypeConfigDTO> paxList = new ArrayList<FarePaxTypeConfigDTO>();
		FarePaxTypeConfigDTO pax = null;
		Collection<PaxTypeTO> paxTypeList = globalConfig.getPaxTypes();
		if (paxTypeList != null && !paxTypeList.isEmpty()) {
			for (PaxTypeTO type : paxTypeList) {
				if (type != null) {
					pax = new FarePaxTypeConfigDTO();
					pax.setApplyCnxCharge(type.isApplyCnxCharge());
					pax.setApplyFare(type.isApplyFare());
					pax.setApplyModChg(type.isApplyModCharge());
					pax.setCutOffAgeInYears(type.getCutOffAgeInYears());
					pax.setDescription(type.getDescription());
					pax.setFareAmount(AccelAeroCalculator.formatAsDecimal(type.getFareAmount()));
					pax.setFareAmountType(type.getFareAmountType());
					pax.setFareRefundable(type.isFareRefundable());
					pax.setNoShowChargeAmount(AccelAeroCalculator.formatAsDecimal(type.getNoShowChargeAmount()));
					pax.setPaxTypeCode(type.getPaxTypeCode());
					pax.setVersion(type.getVersion());
					paxList.add(pax);
				}
			}
		}
		return paxList;
	}

	public static boolean isShowFareDefInDepAirPCurr() {
		String showFareDefByDepCountryCurrency = config.getBizParam(SystemParamKeys.SHOW_FARE_DEF_BY_DEP_COUNTRY_CURRENCY);
		return (showFareDefByDepCountryCurrency != null && showFareDefByDepCountryCurrency.equalsIgnoreCase("Y")) ? true : false;
	}

	public static Date buildTime(String time) throws ParseException {
		Date timeDisplay = null;
		if (time != null && !time.trim().isEmpty()) {
			SimpleDateFormat dateForamt = new SimpleDateFormat("HH:mm:ss");
			if (time.split(":").length == 2) {
				time += ":00";
			}
			timeDisplay = dateForamt.parse(time);
		}
		return timeDisplay;
	}

	public static void addFees(Collection<FareRuleFeeDTO> fees, FareRule fareRule, boolean isUpdate) {
		if (fees != null) {
			FareRuleFee fareRuleFee = null;
			Long feeID = null;
			for (FareRuleFeeDTO feeDto : fees) {
				fareRuleFee = new FareRuleFee();
				feeID = feeDto.getFareRuleFeeId();
				if (isUpdate && feeID != null) {
					fareRuleFee.setFareRuleFeeId(feeDto.getFareRuleFeeId());
				}
				fareRuleFee.setChargeAmount(feeDto.getChargeAmount());
				fareRuleFee.setChargeAmountInLocal(feeDto.getChargeAmountInLocal());
				fareRuleFee.setChargeAmountType(feeDto.getChargeAmountType());
				fareRuleFee.setChargeType(feeDto.getChargeType());
				fareRuleFee.setCompareAgainst(feeDto.getCompareAgainst());
				fareRuleFee.setFareRule(feeDto.getFareRule());
				fareRuleFee.setMaximumPerChargeAmt(feeDto.getMaximumPerChargeAmt());
				fareRuleFee.setMinimumPerChargeAmt(feeDto.getMinimumPerChargeAmt());
				fareRuleFee.setStatus(feeDto.getStatus());
				fareRuleFee.setTimeType(feeDto.getTimeType());
				fareRuleFee.setTimeValue(feeDto.getTimeValue());
				fareRule.addFee(fareRuleFee);
			}
		}
	}

	public static FareRulePax getPaxApplicability(JSONObject paxAppObj, FareRulePax fareRulePax) {
		fareRulePax.setApplyFares((paxAppObj.get("fareApplicable").toString().equals("true")) ? true : false);
		fareRulePax.setRefundableFares((paxAppObj.get("fareRefundable").toString().equals("true")) ? true : false);
		fareRulePax.setApplyModCharges((paxAppObj.get("modChgApplicable").toString().equals("true")) ? true : false);
		fareRulePax.setApplyCNXCharges((paxAppObj.get("cnxChgApplicable").toString().equals("true")) ? true : false);
		fareRulePax.setNoShowChgBasis(paxAppObj.get("noshowChgType").toString());

		if (paxAppObj.get("noshowChgAmount") != null && !paxAppObj.get("noshowChgAmount").equals("")) {
			fareRulePax.setNoShowChrge(Double.valueOf(paxAppObj.get("noshowChgAmount").toString()));
		}

		if (paxAppObj.get("noshowChgLocal") != null && !paxAppObj.get("noshowChgLocal").equals("")) {
			fareRulePax.setNoShowChrgeInLocal(Double.valueOf(paxAppObj.get("noshowChgLocal").toString()));
		}
		if (paxAppObj.get("noshowChgBreakpoint") != null && !paxAppObj.get("noshowChgBreakpoint").equals("")) {
			fareRulePax.setNoShowChgBreakPoint(Double.valueOf(paxAppObj.get("noshowChgBreakpoint").toString()));
		}
		if (paxAppObj.get("noshowChgBoudary") != null && !paxAppObj.get("noshowChgBoudary").equals("")) {
			fareRulePax.setNoShowChgBoundary(Double.valueOf(paxAppObj.get("noshowChgBoudary").toString()));
		}

		return fareRulePax;
	}

	public static Fare getFareFromJSON(JSONObject fareToJsonObj) throws Exception {
		Fare tmpFare = new Fare();

		tmpFare.setFareId(Integer.valueOf(fareToJsonObj.get("fareId").toString()));
		tmpFare.setFareRuleId(Integer.valueOf(fareToJsonObj.get("fareRuleId").toString()));

		String effFromDate = fareToJsonObj.get("validFromDate").toString();
		if (!effFromDate.equals("")) {
			tmpFare.setEffectiveFromDate(parseJsonDate(effFromDate));
		}
		String effToDate = fareToJsonObj.get("validToDate").toString();
		if (!effToDate.equals("")) {
			tmpFare.setEffectiveToDate(CalendarUtil.getEndTimeOfDate(parseJsonDate(effToDate)));
		}

		String salesEffFromDate = fareToJsonObj.get("salesValidFromDate").toString();
		if (!salesEffFromDate.equals("")) {
			tmpFare.setSalesEffectiveFrom(parseJsonDate(salesEffFromDate));
		}

		String salesEffToDate = fareToJsonObj.get("salesValidToDate").toString();
		if (!salesEffToDate.equals("")) {
			tmpFare.setSalesEffectiveTo(CalendarUtil.getEndTimeOfDate(parseJsonDate(salesEffToDate)));
		}

		if (fareToJsonObj.get("rtValidFromDate") != null) {
			String rtSalesEffFromDate = fareToJsonObj.get("rtValidFromDate").toString();
			tmpFare.setReturnEffectiveFromDate(parseJsonDate(rtSalesEffFromDate));
		}

		if (fareToJsonObj.get("rtValidToDate") != null) {
			String rtSalesEffToDate = fareToJsonObj.get("rtValidToDate").toString();
			tmpFare.setReturnEffectiveToDate(CalendarUtil.getEndTimeOfDate(parseJsonDate(rtSalesEffToDate)));
		}

		tmpFare.setFareAmount(Double.valueOf(fareToJsonObj.get("fareAmount").toString()));
		tmpFare.setChildFareType(fareToJsonObj.get("childFareType").toString());
		tmpFare.setChildFare(Double.valueOf(fareToJsonObj.get("childFareAmount").toString()));
		tmpFare.setInfantFareType(fareToJsonObj.get("infantFareType").toString());
		tmpFare.setInfantFare(Double.valueOf(fareToJsonObj.get("infantFareAmount").toString()));
		tmpFare.setBookingCode(fareToJsonObj.get("bookingClass").toString());
		tmpFare.setBookingCode(fareToJsonObj.get("bookingClass").toString());
		tmpFare.setOndCode(fareToJsonObj.get("ondCode").toString());
		tmpFare.setStatus(fareToJsonObj.get("status").toString());
		tmpFare.setVersion(Long.parseLong(fareToJsonObj.get("version").toString()));

		String currencyCode = "";

		if (fareToJsonObj.get("currencyCode") != null) {
			currencyCode = fareToJsonObj.get("currencyCode").toString();
		}

		if (!currencyCode.equals("") && isShowFareDefInDepAirPCurr()) {
			tmpFare.setFareAmountInLocal(Double.valueOf(fareToJsonObj.get("fareAmount").toString()));
			tmpFare.setLocalCurrencyCode(currencyCode);
			tmpFare.setLocalCurrencySelected(true);
			if (isChildVisible()) {
				tmpFare.setChildFareInLocal(Double.valueOf(fareToJsonObj.get("childFareAmount").toString()));
				tmpFare.setInfantFareInLocal(Double.valueOf(fareToJsonObj.get("infantFareAmount").toString()));
			}
		}

		return tmpFare;
	}

	private static long getMinutesFromString(String timeFormat) {
		// Assume format is (MM : DD : HR : MI)
		long minutes = 0;
		if (!StringUtil.isNullOrEmpty(timeFormat)) {
			String[] durationArray = timeFormat.split(":");
			if (!StringUtil.isNullOrEmpty(durationArray[0])) {
				minutes = Integer.parseInt(durationArray[0]) * 30 * 24 * 60;
			}
			if (!StringUtil.isNullOrEmpty(durationArray[1])) {
				minutes += Integer.parseInt(durationArray[1]) * 24 * 60;
			}
			if (!StringUtil.isNullOrEmpty(durationArray[2])) {
				minutes += Integer.parseInt(durationArray[2]) * 60;
			}
			if (!StringUtil.isNullOrEmpty(durationArray[3])) {
				minutes += Integer.parseInt(durationArray[3]);
			}
		}
		return minutes;
	}

	private static String getModifyBufferString(String modBufTime) {
		String modBufDays = "0";
		String modBufHours = "0";
		String modBufMins = "0";

		if (modBufTime != null) {
			if (modBufTime.indexOf(':') != -1) {
				String arrTimeDuration[] = modBufTime.split(":");
				if (arrTimeDuration.length == 3) {
					modBufDays = arrTimeDuration[0];
					modBufHours = arrTimeDuration[1];
					modBufMins = arrTimeDuration[2];
				} else if (arrTimeDuration.length == 2) {
					modBufHours = arrTimeDuration[0];
					modBufMins = arrTimeDuration[1];
				} else if (arrTimeDuration.length == 1) {
					modBufMins = arrTimeDuration[0];
				}

				return modBufDays + ":" + modBufHours + ":" + modBufMins;
			}
		}

		return modBufDays + ":" + modBufHours + ":" + modBufMins;
	}

	public static String getFrequencyForFare(FareRule fareRule, boolean isInbound) throws ModuleException {

		StringBuffer frequecyBuff = new StringBuffer();
		Frequency frequency = null;

		if (isInbound == true) {
			frequency = convertToPlatFormsFrequency(fareRule.getInboundDepatureFrequency());
		} else {
			frequency = convertToPlatFormsFrequency(fareRule.getOutboundDepatureFreqency());
		}

		Map<DayOfWeek, DayOfWeek> daysTmp = new HashMap<DayOfWeek, DayOfWeek>();
		Iterator<DayOfWeek> iterDays = CalendarUtil.getDaysFromFrequency(frequency).iterator();

		while (iterDays.hasNext()) {
			DayOfWeek strDay = iterDays.next();
			daysTmp.put(strDay, strDay);
		}

		frequecyBuff.append(daysTmp.containsKey(DayOfWeek.MONDAY) ? "Mo:" : "_:");
		frequecyBuff.append(daysTmp.containsKey(DayOfWeek.TUESDAY) ? "Tu:" : "_:");
		frequecyBuff.append(daysTmp.containsKey(DayOfWeek.WEDNESDAY) ? "We:" : "_:");
		frequecyBuff.append(daysTmp.containsKey(DayOfWeek.THURSDAY) ? "Th:" : "_:");
		frequecyBuff.append(daysTmp.containsKey(DayOfWeek.FRIDAY) ? "Fr:" : "_:");
		frequecyBuff.append(daysTmp.containsKey(DayOfWeek.SATURDAY) ? "Sa:" : "_:");
		frequecyBuff.append(daysTmp.containsKey(DayOfWeek.SUNDAY) ? "Su:" : "_:");

		return frequecyBuff.toString();

	}

	private static String getFareVisibility(Set<Integer> visibilities) throws ModuleException {
		List<Map<String, String>> lstVisibilites = SelectListGenerator.createSalesChannels();
		String visibility = "";
		String visibilityCodes = "";
		if (lstVisibilites.size() != visibilities.size()) {
			if (visibilities != null) {
				if (visibilities.size() > 0) {
					Collection<Channel> colVisibilites = ModuleServiceLocator.getRuleBD().getChannels(visibilities);
					Iterator<Channel> iteVisibilites = colVisibilites.iterator();
					while (iteVisibilites.hasNext()) {
						Channel channel = iteVisibilites.next();
						visibility += channel.getChannelDescription() + ", ";
						visibilityCodes += channel.getChannelId() + ",";
					}
				}
			}

			if (visibility.indexOf(',') != -1) {
				return visibility.substring(0, visibility.lastIndexOf(','));
			} else {
				return visibility;
			}
		} else {
			return "";
		}
	}

	private static Date parseJsonDate(String jsonDate) {
		Date d = null;
		if (jsonDate == null || "".equals(jsonDate)) {
			return d;
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		try {
			d = sdf.parse(jsonDate);
		} catch (java.text.ParseException e) {
			d = null;
		}
		return d;
	}

	public static FareRule getFareRuleFromDto(FareRuleDTO fareRuleDto, Collection<FareRuleFeeDTO> fees, boolean isUpdate)
			throws Exception {
		FareRule fareRuleNew = new FareRule();
		fareRuleNew.setFareRuleCode(fareRuleDto.getFareRuleCode().toUpperCase());
		fareRuleNew.setFareRuleDescription(fareRuleDto.getDescription());
		if (fareRuleDto.getAdvanceBookingDays() != null && !fareRuleDto.getAdvanceBookingDays().equals("")) {
			fareRuleNew.setAdvanceBookingDays(parseStringToMins(fareRuleDto.getAdvanceBookingDays()));
		}
		fareRuleNew.setFareBasisCode(fareRuleDto.getFareBasisCode().toUpperCase());
		fareRuleNew.setFareCategoryCode(fareRuleDto.getFareCategoryCode());
		fareRuleNew.setFlexiCodes(fareRuleDto.getFlexiCodesCol());
		fareRuleNew.setInboundDepatureFrequency(fareRuleDto.getInboundDepatureFrequency());
		fareRuleNew.setInboundDepatureTimeFrom(buildTime(fareRuleDto.getInboundDepatureTimeFrom()));
		fareRuleNew.setInboundDepatureTimeTo(buildTime(fareRuleDto.getInboundDepatureTimeTo()));
		fareRuleNew.setLoadFactorMax(fareRuleDto.getLoadFactorMax());
		fareRuleNew.setLoadFactorMin(fareRuleDto.getLoadFactorMin());
		fareRuleNew.setMaximumStayoverMins(fareRuleDto.getMaximumStayoverMins());
		fareRuleNew.setMaximumStayoverMonths(fareRuleDto.getMaximumStayoverMonths());
		fareRuleNew.setMinimumStayoverMins(fareRuleDto.getMinimumStayoverMins());
		fareRuleNew.setMinimumStayoverMonths(fareRuleDto.getMinimumStayoverMonths());
		fareRuleNew.setInternationalModifyBufferTime(convertingToDaysHours(fareRuleDto.getIntnModifyBufferTime()));
		fareRuleNew.setDomesticModifyBufferTime(fareRuleDto.getDomModifyBufferTime());
		fareRuleNew.setModifyByDate(fareRuleDto.getModifyByDate());
		fareRuleNew.setModifyByRoute(fareRuleDto.getModifyByRoute());
		fareRuleNew.setOpenRenConfPeriodMonths(fareRuleDto.getOpenRenConfPeriodMonths());
		fareRuleNew.setOpenReturnConfPeriod(fareRuleDto.getOpenRetConfPeriodMins());
		fareRuleNew.setOpenReturn(CommonsConstants.YES.equals(fareRuleDto.getOpenReturnFlag()) ? true : false);
		fareRuleNew.setHalfReturn(CommonsConstants.YES.equals(fareRuleDto.getHalfReturnFlag()) ? true : false);
		fareRuleNew.setOutboundDepatureFreqency(fareRuleDto.getOutboundDepatureFreqency());
		fareRuleNew.setOutboundDepatureTimeFrom(buildTime(fareRuleDto.getOutboundDepatureTimeFrom()));
		fareRuleNew.setOutboundDepatureTimeTo(buildTime(fareRuleDto.getOutboundDepatureTimeTo()));
		fareRuleNew.setPaxCategoryCode(fareRuleDto.getPaxCategoryCode());
		fareRuleNew.setPrintExpDate(CommonsConstants.YES.equalsIgnoreCase(fareRuleDto.getPrintExpDate()) ? true : false);
		fareRuleNew.setReturnFlag(CommonsConstants.YES.equals(fareRuleDto.getReturnFlag())
				? CommonsConstants.CHR_YES
				: CommonsConstants.CHR_NO);
		fareRuleNew.setRulesComments(fareRuleDto.getRulesComments());
		fareRuleNew.setAgentInstructions(fareRuleDto.getAgentComments());
		fareRuleNew.setStatus(fareRuleDto.getStatus());
		fareRuleNew.setVisibleChannelIds(fareRuleDto.getVisibleChannelCol());
		fareRuleNew.setVisibileAgentCodes(fareRuleDto.getVisibileAgentCol());
		fareRuleNew.setPointOfSale(fareRuleDto.getVisiblePOS());
		fareRuleNew.setApplicableToAllCountries(fareRuleDto.getApplicableToCountries());
		fareRuleNew.setBulkTicket(CommonsConstants.YES.equals(fareRuleDto.getBulkTicket()) 
				? CommonsConstants.CHR_YES
				: CommonsConstants.CHR_NO);
		fareRuleNew.setMinBulkTicketSeatCount(fareRuleDto.getMinBulkTicketSeatCount());
		
		if (!StringUtil.isNullOrEmpty(fareRuleDto.getMinFareDiscountStr())) {
			fareRuleNew.setFareDiscountMin(Integer.parseInt(fareRuleDto.getMinFareDiscountStr()));
		}

		if (!StringUtil.isNullOrEmpty(fareRuleDto.getMaxFareDiscountStr())) {
			fareRuleNew.setFareDiscountMax(Integer.parseInt(fareRuleDto.getMaxFareDiscountStr()));
		}

		boolean isDefinedInLocalCurrency = !StringUtil.isNullOrEmpty(fareRuleDto.getLocalCurrency());
		if (!StringUtil.isNullOrEmpty(fareRuleDto.getAgentCommissionType())) {
			if (isDefinedInLocalCurrency) {
				if (StringUtil.isNullOrEmpty(fareRuleDto.getAgentCommissionAmountLocal())) {
					fareRuleNew.setAgentCommissionType(fareRuleDto.getAgentCommissionType());
					fareRuleNew.setAgentCommissionAmountInLocal(Double.valueOf(fareRuleDto.getAgentCommissionAmountLocal()));
				}
			} else {
				if (!StringUtil.isNullOrEmpty(fareRuleDto.getAgentCommissionAmount())) {
					fareRuleNew.setAgentCommissionType(fareRuleDto.getAgentCommissionType());
					fareRuleNew.setAgentCommissionAmount(Double.valueOf(fareRuleDto.getAgentCommissionAmount()));
				}
			}
		}

		fareRuleNew.setCancellationChargeType(fareRuleDto.getCancellationChargeType().isEmpty() ? FareRule.CHARGE_TYPE_V
				.toString() : fareRuleDto.getCancellationChargeType());
		fareRuleNew.setModificationChargeType(fareRuleDto.getModificationChargeType().isEmpty() ? FareRule.CHARGE_TYPE_V
				.toString() : fareRuleDto.getModificationChargeType());

		if (!StringUtil.isNullOrEmpty(fareRuleDto.getCancellationChargeAmountStr())) {
			fareRuleNew.setCancellationChargeAmount(Double.valueOf(fareRuleDto.getCancellationChargeAmountStr()));
		}
		if (!StringUtil.isNullOrEmpty(fareRuleDto.getModificationChargeAmountStr())) {
			fareRuleNew.setModificationChargeAmount(Double.valueOf(fareRuleDto.getModificationChargeAmountStr()));
		}
		if (!StringUtil.isNullOrEmpty(fareRuleDto.getCancellationChargeLocalStr())) {
			fareRuleNew.setCnxChargeAmountInLocal(Double.valueOf(fareRuleDto.getCancellationChargeLocalStr()));
		}
		if (!StringUtil.isNullOrEmpty(fareRuleDto.getModificationChargeLocalStr())) {
			fareRuleNew.setModChargeAmountInLocal(Double.valueOf(fareRuleDto.getModificationChargeLocalStr()));
		}
		if (isDefinedInLocalCurrency) {
			fareRuleNew.setLocalCurrencyCode(fareRuleDto.getLocalCurrency());
			fareRuleNew.setLocalCurrencySelected(true);
		}
		if (!StringUtil.isNullOrEmpty(fareRuleDto.getMinCnxChargeAmountStr())) {
			fareRuleNew.setMinCancellationAmount(Double.valueOf(fareRuleDto.getMinCnxChargeAmountStr()));
		}
		if (!StringUtil.isNullOrEmpty(fareRuleDto.getMaxCnxChargeAmountStr())) {
			fareRuleNew.setMaximumCancellationAmount(Double.valueOf(fareRuleDto.getMaxCnxChargeAmountStr()));
		}
		if (!StringUtil.isNullOrEmpty(fareRuleDto.getMinModChargeAmountStr())) {
			fareRuleNew.setMinModificationAmount(Double.valueOf(fareRuleDto.getMinModChargeAmountStr()));
		}
		if (!StringUtil.isNullOrEmpty(fareRuleDto.getMaxModChargeAmountStr())) {
			fareRuleNew.setMaximumModificationAmount(Double.valueOf(fareRuleDto.getMaxModChargeAmountStr()));
		}

		fareRuleNew.setNameChangeChargeType(fareRuleDto.getNameChangeChargeType().isEmpty()
				? FareRule.CHARGE_TYPE_V.toString()
				: fareRuleDto.getNameChangeChargeType());
		if (!StringUtil.isNullOrEmpty(fareRuleDto.getNameChangeChargeAmountStr())) {
			fareRuleNew.setNameChangeChargeAmount(Double.valueOf(fareRuleDto.getNameChangeChargeAmountStr()));
		}
		if (!StringUtil.isNullOrEmpty(fareRuleDto.getNameChangeChargeAmountLocalStr())) {
			fareRuleNew.setNameChangeChargeAmountLocal(Double.valueOf(fareRuleDto.getNameChangeChargeAmountLocalStr()));
		}
		if (!StringUtil.isNullOrEmpty(fareRuleDto.getMinNCCAmountStr())) {
			fareRuleNew.setMinNCCAmount(Double.valueOf(fareRuleDto.getMinNCCAmountStr()));
		}
		if (!StringUtil.isNullOrEmpty(fareRuleDto.getMaxNCCAmountStr())) {
			fareRuleNew.setMaxNCCAmount(Double.valueOf(fareRuleDto.getMaxNCCAmountStr()));
		}

		JSONParser parser = new JSONParser();
		JSONArray jsonPaxAppArray = (JSONArray) parser.parse(fareRuleDto.getPaxApplicabilityJSON());
		if (jsonPaxAppArray.size() > 0) {
			JSONObject adPaxAppObj = (JSONObject) jsonPaxAppArray.get(0);
			fareRuleNew.addPaxDetails(FareUtil.getPaxApplicability(adPaxAppObj, fareRuleDto.getAdPax()));

			JSONObject chPaxAppObj = (JSONObject) jsonPaxAppArray.get(1);
			fareRuleNew.addPaxDetails(FareUtil.getPaxApplicability(chPaxAppObj, fareRuleDto.getChPax()));

			JSONObject inPaxAppObj = (JSONObject) jsonPaxAppArray.get(2);
			fareRuleNew.addPaxDetails(FareUtil.getPaxApplicability(inPaxAppObj, fareRuleDto.getInPax()));
		}

		if (fareRuleDto.getVersion() != null)
			fareRuleNew.setVersion(fareRuleDto.getVersion());
		if (fareRuleDto.getFareRuleID() != null)
			fareRuleNew.setFareRuleId(fareRuleDto.getFareRuleID());
		addFees(fees, fareRuleNew, isUpdate);

		if (fareRuleDto.getFareRuleID() != null) {
			FareRuleBD fareRuleBD = ModuleServiceLocator.getRuleBD();
			fareRuleNew.setFeeTwlHrsCutOverApplied(fareRuleBD.getFareRule(fareRuleDto.getFareRuleID())
					.getFeeTwlHrsCutOverApplied());
		}

		fareRuleNew.setPointOfSale(fareRuleDto.getVisiblePOS());
		

		return fareRuleNew;
	}

	private static void addComments(Collection<FareRuleCommentDTO> fareRuleComments, FareRule fareRuleNew, boolean isUpdate)
			throws ModuleException {
		if (fareRuleComments != null) {
			validateComments(fareRuleComments);
			for (FareRuleCommentDTO fareRuleCommentDTO : fareRuleComments) {
				FareRuleComment fareRuleComment = new FareRuleComment();
				if (fareRuleCommentDTO.getFareRuleCommentID() != null) {
					fareRuleComment.setCommentID(fareRuleCommentDTO.getFareRuleCommentID());
				}
				fareRuleComment.setFareRule(fareRuleNew);
				String hexComment = StringUtil.convertToHex(fareRuleCommentDTO.getFareRuleComments());
				fareRuleComment.setComments(hexComment);
				fareRuleComment.setLanguageCode(fareRuleCommentDTO.getLanguageCode());
				fareRuleNew.addFareRuleComment(fareRuleComment);
			}
		}

	}

	private static void validateComments(Collection<FareRuleCommentDTO> fareRuleComments) throws ModuleException {
		List<String> languages = new ArrayList<>();
		for (FareRuleCommentDTO fareRuleComment : fareRuleComments) {
			if (languages.contains(fareRuleComment.getLanguageCode())) {
				throw new ModuleException("duplicate.language.comment");
			} else {
				languages.add(fareRuleComment.getLanguageCode());
			}
		}
	}

	public static Integer parseStringToMins(String timeString) {
		Integer totalTimeInMin = 0;
		if (timeString != null && !timeString.equals("0:0:0") && timeString.indexOf(':') != -1) {
			String[] timeArr = timeString.split(":");
			if (timeArr.length == 1) {
				if (!timeArr[0].equals("") && !timeArr[0].equals("0")) {
					totalTimeInMin += Integer.parseInt(timeArr[0]) * 24 * 60;
				}
			} else if (timeArr.length == 2) {
				if (!timeArr[0].equals("") && !timeArr[0].equals("0")) {
					totalTimeInMin += Integer.parseInt(timeArr[0]) * 24 * 60;
				}
				if (!timeArr[1].equals("") && !timeArr[1].equals("0")) {
					totalTimeInMin += Integer.parseInt(timeArr[1]) * 60;
				}
			} else if (timeArr.length == 3) {
				if (!timeArr[0].equals("") && !timeArr[0].equals("0")) {
					totalTimeInMin += Integer.parseInt(timeArr[0]) * 24 * 60;
				}
				if (!timeArr[1].equals("") && !timeArr[1].equals("0")) {
					totalTimeInMin += Integer.parseInt(timeArr[1]) * 60;
				}
				if (!timeArr[2].equals("") && !timeArr[2].equals("0")) {
					totalTimeInMin += Integer.parseInt(timeArr[2]);
				}
			}
			return totalTimeInMin;
		}

		return totalTimeInMin;
	}
	public static String convertingToDaysHours(String timeStr) {
		String converted = null;
		if (timeStr != null && !timeStr.equals("0:0:0") && timeStr.indexOf(':') != -1) {
			String[] timeArr = timeStr.split(":");
			int MI = Integer.parseInt(timeArr[2]);	
			int HR = Integer.parseInt(timeArr[1]);
			int DD = Integer.parseInt(timeArr[0]);
			if (MI >= 60){
				HR += MI / 60 ; MI = MI % 60 ;
				}
			if(HR >= 24){
				DD += HR/24; HR = HR%24;
			}
		converted = Integer.toString(DD)+ ":" + Integer.toString(HR)+ ":" + Integer.toString(MI) ;
		}
		return converted;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void getFareRuleOHDTimeDate(OnHoldReleaseTimeDTO ohdRelDTO, Map ohdRelDTOMap) {
		ohdRelDTOMap.put("ohdRelId", ohdRelDTO.getReleaseTimeId());
		if (ohdRelDTO.getStartCutoverTime() != null) {
			ohdRelDTOMap.put("startCutOTime", parseMins(ohdRelDTO.getStartCutoverTime()));
		} else {
			ohdRelDTOMap.put("startCutOTime", "0:0:0");
		}
		if (ohdRelDTO.getEndCutoverTime() != null) {
			ohdRelDTOMap.put("endCutOTime", parseMins(ohdRelDTO.getEndCutoverTime()));
		} else {
			ohdRelDTOMap.put("endCutOTime", "0:0:0");
		}
		if (ohdRelDTO.getReleaseTime() != null) {
			ohdRelDTOMap.put("releaseTime", parseMins(ohdRelDTO.getReleaseTime()));
		} else {
			ohdRelDTOMap.put("releaseTime", "0:0:0");
		}

		ohdRelDTOMap.put("moduleCode", ohdRelDTO.getModuleCode());
		ohdRelDTOMap.put("timeWithRespect", ohdRelDTO.getReleaseTimeWithRelativeTo());
		ohdRelDTOMap.put("version", ohdRelDTO.getVersion());
	}

	public static Frequency convertToPlatFormsFrequency(com.isa.thinair.airpricing.api.model.Frequency frequency) {
		Frequency platFrequency = new Frequency();
		platFrequency.setDay0(frequency.getDay0() == 1 ? true : false);
		platFrequency.setDay1(frequency.getDay1() == 1 ? true : false);
		platFrequency.setDay2(frequency.getDay2() == 1 ? true : false);
		platFrequency.setDay3(frequency.getDay3() == 1 ? true : false);
		platFrequency.setDay4(frequency.getDay4() == 1 ? true : false);
		platFrequency.setDay5(frequency.getDay5() == 1 ? true : false);
		platFrequency.setDay6(frequency.getDay6() == 1 ? true : false);
		return platFrequency;

	}

	public static Map<String, String> getSortingFields() {
		Map<String, String> sortingFields = new HashMap<String, String>();
		sortingFields.put("fareId", "t_ond_fare.fare_id");
		sortingFields.put("fareRuleCode", "t_fare_rule.fare_rule_code");
		sortingFields.put("fareBasisCode", "t_fare_rule.fare_basis_code");
		sortingFields.put("validFromDate", "t_ond_fare.effective_from_date");
		sortingFields.put("validToDate", "t_ond_fare.effective_to_date");
		sortingFields.put("fareAmount", "t_ond_fare.fare_amount");
		sortingFields.put("childFareAmount", "t_ond_fare.child_fare");
		sortingFields.put("infantFareAmount", "t_ond_fare.infant_fare");
		sortingFields.put("bookingClass", "t_ond_fare.booking_code");
		sortingFields.put("fixedAllocation", "t_booking_class.fixed_flag");
		sortingFields.put("ondCode", "t_ond_fare.ond_code");
		sortingFields.put("status", "t_ond_fare.status");
		sortingFields.put("visibilityChannels", "t_fare_visibility.sales_channel_code");
		sortingFields.put("rtValidFromDate", "t_ond_fare.rt_effective_from_date");
		sortingFields.put("rtValidToDate", "t_ond_fare.rt_effective_to_date");
		sortingFields.put("salesValidFromDate", "t_ond_fare.sales_valid_from");
		sortingFields.put("salesValidToDate", "t_ond_fare.sales_valid_to");

		return sortingFields;
	}

	public static void updateComments(FareRule fareRule, Collection<FareRuleCommentDTO> existingComments) throws ModuleException {
		fareRule.getFareRuleComments().clear();
		addComments(existingComments, fareRule, false);
	}
}
