package com.isa.thinair.airadmin.core.web.handler.master;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.master.CurrencyHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airmaster.api.model.Currency;
import com.isa.thinair.airmaster.api.model.CurrencyExchangeRate;
import com.isa.thinair.airmaster.api.model.CurrencyForDisplay;
import com.isa.thinair.commons.api.exception.MessagesUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.paymentbroker.api.dto.IPGPaymentOptionDTO;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * @author Shakir
 * 
 */

public final class CurrencyRequestHandler extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(CurrencyRequestHandler.class);
	private static AiradminConfig adminConfig = new AiradminConfig();

	private static final String PARAM_CODE = "txtCurrencyCode";
	private static final String PARAM_DESCRIPTION = "txtDescription";
	private static final String PARAM_STATUS = "chkStatus";
	private static final String PARAM_ITINERARY_BREAKDOWN = "chkItineraryFareBreakDownEnalbed";
	private static final String PARAM_MODE = "hdnModel";
	private static final String PARAM_VERSION = "hdnVersion";
	private static final String PARAM_RECNO = "hdnRecNo";
	private static final String PARAM_CURRENCY_DECIMAL_PLACES = "txtDecimal";

	private static final String PARAM_BOUNDARY = "txtBoundary";
	private static final String PARAM_BREAKPOINT = "txtBreakPoint";
	private static final String PARAM_IBEVISIBILITY = "chkIBEVisibililty";
	private static final String PARAM_XBEVISIBILITY = "chkXBEVisibililty";
	private static final String PARAM_CARDPAY_VISIBILITY = "chkCardPayVisibililty";

	private static final String PARAM_HDNEXRATEDATA = "hdnExRateData";
	private static final String PARAM_HDNEXRATEGRID = "hdnExRateGrid";

	private static final String PARAM_CURRENCY_FOR_DISPLAY = "hdnCurrencyForDisplay";

	private static final String PARAM_PAYMENTGATEWAY_IBE = "selPaymentGatewayIbe";

	private static final String PARAM_PAYMENTGATEWAY_XBE = "selPaymentGatewayXbe";

	private static final String PARAM_EXRATE_AUTO_UPDATE_STATUS = "exrateAutoUpdateStatus";
	private static final String PARAM_EXRATE_VARIANCE = "exrateVariance";

	private static String MODULE_XBE = "XBE";
	private static String MODULE_IBE = "IBE";

	private static boolean isSuccessfulSaveMessageDisplayEnabled() {
		boolean isMessageEnabled = false;
		if (adminConfig.getMessage("um.airadmin.savemessage.display").equals("true")) {
			isMessageEnabled = true;
		}
		return isMessageEnabled;
	}

	private static void setExceptionOccured(HttpServletRequest request, boolean value) {
		setAttribInRequest(request, "isExceptionOccured", new Boolean(value));
	}

	private static boolean isExceptionOccured(HttpServletRequest request) {
		return ((Boolean) getAttribInRequest(request, "isExceptionOccured")).booleanValue();
	}

	private static void setIntSuccess(HttpServletRequest request, int value) {
		setAttribInRequest(request, "intSuccess", new Integer(value));
	}

	private static int getIntSuccess(HttpServletRequest request) {
		Integer returnValue = (Integer) getAttribInRequest(request, "intSuccess");
		if (returnValue != null)
			return returnValue.intValue();
		else
			return -1;
	}

	/**
	 * Main Execute method for Currency Action & sets the Success Int 0-Not Applicable, 1-Success, 2-Fail
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {
		String strHdnMode = request.getParameter("hdnModel");
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;

		setIntSuccess(request, 0);
		String strFormData = "var saveSuccess = " + getIntSuccess(request) + ";";
		request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);

		setExceptionOccured(request, false);
		setAttribInRequest(request, "strModeJS", "var isAddMode = false;");
		setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = false;");

		// Save Action
		if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_SAVE)) {
			try {
				saveData(request);
				if (!isExceptionOccured(request)) {
					setIntSuccess(request, 1);
					strFormData += " var saveSuccess = " + getIntSuccess(request) + ";";
					request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
				}
			} catch (Exception exception) {
				log.error("Exception in CurrencyRequestHandler:execute()", exception);
				if (exception instanceof RuntimeException) {
					forward = WebConstants.FORWARD_ERROR;
					JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
				}
			}
		}

		// Delete Action
		if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_DELETE)) {
			checkPrivilege(request, WebConstants.PRIV_SYS_MAS_CURRENCY_DELETE);
			try {
				deleteCurrency(request);
				log.debug("\nCURRENCYREQUESTHANDLER DELETE() SUCCESS");
			} catch (ModuleException me) {
				log.error("Exception in CurrencyRequestHandler:execute() [origin module=" + me.getModuleDesc() + "]", me);
			}
		}
		// Haider
		if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_SAVE_LIST_FOR_DISPLAY)) {
			try {
				saveCurrencyForDisplay(request);
				if (!isExceptionOccured(request)) {
					setIntSuccess(request, 1);
					strFormData += " var saveSuccess = " + getIntSuccess(request) + ";";
					request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
				}
			} catch (Exception exception) {
				log.error("Exception in CurrencyRequestHandler:execute()", exception);
				if (exception instanceof RuntimeException) {
					forward = WebConstants.FORWARD_ERROR;
					JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
				}
			}
		}

		try {
			setDisplayData(request);
			log.debug("\nCURRENCYREQUESTHANDLER SETDISPLAYDATA() SUCCESS");
		} catch (ModuleException me) {
			log.error("Exception in CurrencyRequestHandler:execute() [origin module=" + me.getModuleDesc() + "]", me);
			saveMessage(request, me.getMessageString(), WebConstants.MSG_ERROR);
		}
		return forward;
	}

	/**
	 * saves the Currecy Data for display
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	private static void saveCurrencyForDisplay(HttpServletRequest request) throws Exception {
		if (AppSysParamsUtil.isDisplayIBEListsDynamicallyEnabled() == false)
			return;
		// Haider 05Mar09
		Properties curp = getProperties(request);
		String currencyListIBEStr = curp.getProperty(PARAM_CURRENCY_FOR_DISPLAY);
		String[] airortListIBE = StringUtils.split(currencyListIBEStr, "|");
		String[] currencyIBE;
		CurrencyForDisplay at = null;
		for (int i = 0; i < airortListIBE.length; i++) {
			at = new CurrencyForDisplay();
			currencyIBE = StringUtils.split(airortListIBE[i], "^");
			at.setCurrencyCode(currencyIBE[4].trim().toUpperCase());
			at.setCurrencyNameOl(currencyIBE[1]);
			at.setLanguageCode(currencyIBE[0]);
			if (!"-1".equals(currencyIBE[2]))// id
				at.setId(Integer.valueOf(currencyIBE[2]).intValue());
			if (!"-1".equals(currencyIBE[3]))// version
				at.setVersion(Long.valueOf(currencyIBE[3]).longValue());
			if (!"null".equals(at.getCurrencyNameOl()))
				ModuleServiceLocator.getTranslationBD().saveOrUpdateCurrency(at);
			else
				ModuleServiceLocator.getTranslationBD().removeCurrency(at.getId());
		}
		saveMessage(request, adminConfig.getMessage("um.airadmin.edit.success"), WebConstants.MSG_SUCCESS);
		setAttribInRequest(request, "strModeJS", "var isAddMode = false;");

		// <--------------
	}

	/**
	 * saves the Currecy Data
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	@SuppressWarnings("unchecked")
	private static void saveData(HttpServletRequest request) throws Exception {
		Currency currency = null;
		String strAction = request.getParameter("hdnMode");
		Properties curp = getProperties(request);
		// String currencyCode = curp.getProperty(PARAM_CODE);

		if (strAction.equals(WebConstants.ACTION_ADD)) {

			checkPrivilege(request, WebConstants.PRIV_SYS_MAS_CURRENCY_ADD);
			setAttribInRequest(request, "strModeJS", "var isAddMode = true;");

		} else if (strAction.equals(WebConstants.ACTION_EDIT)) {
			checkPrivilege(request, WebConstants.PRIV_SYS_MAS_CURRENCY_EDIT);
			setAttribInRequest(request, "strModeJS", "var isAddMode = false;");
		}

		try {
			Long version = null;
			if (curp.getProperty(PARAM_VERSION) != null && !"".equals(curp.getProperty(PARAM_VERSION))) {
				version = Long.parseLong(curp.getProperty(PARAM_VERSION));
			}
			if (version != null && version >= 0) {
				currency = ModuleServiceLocator.getCommonServiceBD().getCurrency(curp.getProperty(PARAM_CODE));
				currency.setVersion(version);
			} else {
				currency = new Currency();
				currency.setCurrencyCode(curp.getProperty(PARAM_CODE));
			}

			currency.setCurrencyDescriprion(curp.getProperty(PARAM_DESCRIPTION));
			if (curp.getProperty(PARAM_STATUS).equals("on")) {
				currency.setStatus(Currency.STATUS_ACTIVE);
			} else {
				currency.setStatus(Currency.STATUS_INACTIVE);
			}
			if (curp.getProperty(PARAM_ITINERARY_BREAKDOWN).equals("on")
					|| !AppSysParamsUtil.hideChargesInItineraryPassengerDetails()) {
				currency.setItineraryFareBreakDownEnabled(Currency.ITINERARY_BREAKDOWN_ENABLED);
			} else {
				currency.setItineraryFareBreakDownEnabled(Currency.ITINERARY_BREAKDOWN_DISABLED);
			}

			if (!curp.getProperty(PARAM_BOUNDARY).equals("")) {
				BigDecimal dblBound = new BigDecimal(curp.getProperty(PARAM_BOUNDARY));
				currency.setBoundryValue(dblBound);
			} else {
				currency.setBoundryValue(null);
			}
			if (!curp.getProperty(PARAM_BREAKPOINT).equals("")) {
				BigDecimal dblBreak = new BigDecimal(curp.getProperty(PARAM_BREAKPOINT));
				currency.setBreakPoint(dblBreak);
			} else {
				currency.setBreakPoint(null);
			}
			if (curp.getProperty(PARAM_IBEVISIBILITY).equals("Y")) {
				currency.setIBEVisibility(1);
			} else {
				currency.setIBEVisibility(0);
			}
			if (curp.getProperty(PARAM_XBEVISIBILITY).equals("Y")) {
				currency.setXBEVisibility(1);
			} else {
				currency.setXBEVisibility(0);
			}

			Map<String, String> mapPrivileges = (Map<String, String>) request.getSession().getAttribute(
					WebConstants.SES_PRIVILEGE_IDS);

			if (mapPrivileges.get(WebConstants.PRIV_SYS_MAS_CURRENCY_ENABLE_CCPAY) == null) {
				if (strAction.equals(WebConstants.ACTION_ADD)) {
					currency.setCardPaymentVisibility(0);
					currency.setDefaultXbePGId(null);
					currency.setDefaultIbePGId(null);
				} else {
					currency.setCardPaymentVisibility(currency.getCardPaymentVisibility());
					currency.setDefaultXbePGId(currency.getDefaultXbePGId());
					currency.setDefaultIbePGId(currency.getDefaultIbePGId());
				}
			} else {
				if (curp.getProperty(PARAM_CARDPAY_VISIBILITY).equals("Y")) {
					currency.setCardPaymentVisibility(1);

					String strPayGtwIbe = (String) curp.getProperty(PARAM_PAYMENTGATEWAY_IBE);
					strPayGtwIbe = (strPayGtwIbe == null) ? "" : strPayGtwIbe;

					if (!strPayGtwIbe.equals("")) {
						currency.setDefaultIbePGId(Integer.valueOf(strPayGtwIbe));
					} else {
						currency.setDefaultIbePGId(null);
					}

					String strPayGtwXbe = (String) curp.getProperty(PARAM_PAYMENTGATEWAY_XBE);
					strPayGtwXbe = (strPayGtwXbe == null) ? "" : strPayGtwXbe;

					if (!strPayGtwXbe.equals("")) {
						currency.setDefaultXbePGId(Integer.valueOf(strPayGtwXbe));
					} else {
						currency.setDefaultXbePGId(null);
					}

				} else {

					currency.setCardPaymentVisibility(0);
					currency.setDefaultXbePGId(null);
					currency.setDefaultIbePGId(null);
				}
			}

			// exchange rate automation relevant data
			if (request.getParameter(PARAM_EXRATE_AUTO_UPDATE_STATUS) != null
					&& !request.getParameter(PARAM_EXRATE_AUTO_UPDATE_STATUS).equals("")) {
				if (request.getParameter(PARAM_EXRATE_AUTO_UPDATE_STATUS).equalsIgnoreCase("on")) {
					currency.setAutoExRateEnabled(Currency.AUTOMATED_EXRATE_ENABLED);
				} else {
					currency.setAutoExRateEnabled(Currency.AUTOMATED_EXRATE_DISABLED);
				}
			} else {
				currency.setAutoExRateEnabled(Currency.AUTOMATED_EXRATE_DISABLED);
			}

			if (request.getParameter(PARAM_EXRATE_VARIANCE) != null && !request.getParameter(PARAM_EXRATE_VARIANCE).equals("")) {
				currency.setExRateUpdateVariance(new BigDecimal(request.getParameter(PARAM_EXRATE_VARIANCE)));
			} else {
				currency.setExRateUpdateVariance(null);
			}

			if (curp.getProperty(PARAM_CURRENCY_DECIMAL_PLACES) != null
					&& !curp.getProperty(PARAM_CURRENCY_DECIMAL_PLACES).equals("")
					&& StringUtils.isNumeric(curp.getProperty(PARAM_CURRENCY_DECIMAL_PLACES))) {
				currency.setDecimalPlaces(Integer.valueOf(curp.getProperty(PARAM_CURRENCY_DECIMAL_PLACES)));
			}

			addOrUpdateExRateData(currency, curp.getProperty(PARAM_HDNEXRATEDATA));

			ModuleServiceLocator.getCommonServiceBD().saveCurrency(currency);
			setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = true;");

			// Haider 05Mar09
			saveCurrencyForDisplay(request);
			// <--------------

			currency = null;
			log.debug("\nCURRENCYREQUESTHANDLER SAVEDATA() SUCCESS");

			if (strAction.equals(WebConstants.ACTION_ADD)) {

				saveMessage(request, adminConfig.getMessage("um.airadmin.add.success"), WebConstants.MSG_SUCCESS);
				setAttribInRequest(request, "strModeJS", "var isAddMode = true;");

			} else if (strAction.equals(WebConstants.ACTION_EDIT)) {

				saveMessage(request, adminConfig.getMessage("um.airadmin.edit.success"), WebConstants.MSG_SUCCESS);
				setAttribInRequest(request, "strModeJS", "var isAddMode = false;");

			} else {
				saveMessage(request, MessagesUtil.getMessage(WebConstants.KEY_SAVE_SUCCESS), WebConstants.MSG_SUCCESS);
			}

		} catch (ModuleException moduleException) {

			setExceptionOccured(request, true);
			String strFormData = getErrorForm(curp);
			setIntSuccess(request, 2);
			strFormData += " var saveSuccess = " + getIntSuccess(request) + ";";
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);

			log.error(
					"Exception in CurrencyRequestHandler:saveData() [origin module=" + moduleException.getMessageString() + "]",
					moduleException);

			if (moduleException.getMessageString().equals(MessagesUtil.getMessage("module.duplicate.key"))) {
				saveMessage(request, adminConfig.getMessage("um.currency.form.code.defined"), WebConstants.MSG_ERROR);
			} else {
				saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
			}

			if (moduleException.getCause() != null && moduleException.getCause().getMessage() != null) {
				if (moduleException.getCause().getMessage().contains("ORA-00001")) {
					saveMessage(request, adminConfig.getMessage("um.currency.form.code.defined"), WebConstants.MSG_ERROR);
				}
			}

		} catch (Exception exception) {

			log.error("Exception in CurrencyRequestHandler:saveData()", exception);
			setExceptionOccured(request, true);
			if (exception instanceof RuntimeException) {
				throw exception;
			} else {

				String strFormData = getErrorForm(curp);
				setIntSuccess(request, 2);
				strFormData += " var saveSuccess = " + getIntSuccess(request) + ";";
				request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
			}
		}
	}

	/**
	 * Sets Display Data For Currency Page & sets the success int 0-Not Applicable, 1-Success, 2-Fail
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setDisplayData(HttpServletRequest request) throws ModuleException {

		setClientErrors(request);
		setLanguageList(request);// Haider
		setCurrencyRowHtml(request);
		setBoundaryVisibility(request);
		setCurrencyStatus(request);
		setCurrentDate(request);
		setDefaultIBEPg(request);
		setDefaultXBEPg(request);

		if (!isExceptionOccured(request)) {
			String strFormData = "var arrFormData = new Array();";
			strFormData += " var saveSuccess = " + getIntSuccess(request) + ";";
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
		}
		request.setAttribute(WebConstants.REQ_OPERATION_MODE, getAttribInRequest(request, "strModeJS"));

		if (request.getParameter("hdnModel") == null) {

			setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = false;");
			request.setAttribute(WebConstants.REQ_TRANSACTION_STATUS,
					getAttribInRequest(request, "isSaveTransactionSuccessfulJS"));

		} else if (request.getParameter("hdnModel") != null && !request.getParameter("hdnModel").equals(WebConstants.ACTION_SAVE)) {

			setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = false;");
			request.setAttribute(WebConstants.REQ_TRANSACTION_STATUS,
					getAttribInRequest(request, "isSaveTransactionSuccessfulJS"));
		} else {

			request.setAttribute(WebConstants.REQ_TRANSACTION_STATUS,
					getAttribInRequest(request, "isSaveTransactionSuccessfulJS"));
		}

		// Haider
		if (AppSysParamsUtil.isDisplayIBEListsDynamicallyEnabled())
			request.setAttribute(WebConstants.LIST_FOR_DISPLAY_ENABLED, "true");
		else
			request.setAttribute(WebConstants.LIST_FOR_DISPLAY_ENABLED, "false");
		// <--------

		// //exchange rate automation app parametre setting
		if (AppSysParamsUtil.isExchangeRateAutomationEnabledInTheSystem()) {
			request.setAttribute(WebConstants.IS_EXRATE_AUTOMATION_ENABLED, "true");
		} else {
			request.setAttribute(WebConstants.IS_EXRATE_AUTOMATION_ENABLED, "false");
		}

		// Show Itinerary fare break down option
		if (AppSysParamsUtil.hideChargesInItineraryPassengerDetails()) { 
			request.setAttribute(WebConstants.SHOW_ITINERARY_FARE_BREAKDOWN_OPTION, true);
		} else {
			request.setAttribute(WebConstants.SHOW_ITINERARY_FARE_BREAKDOWN_OPTION, false);
		}

		request.setAttribute(WebConstants.REQ_TRANSACTION_DISPLAYSTATUS, "var isSuccessfulSaveMessageDisplayEnabled = "
				+ isSuccessfulSaveMessageDisplayEnabled() + ";");
		request.setAttribute(WebConstants.MAS_CURR_PAY_ENABLE,
				"blnPayble=" + hasPrivilege(request, "sys.mas.currency.enable.ccpay") + ";");
	}

	/**
	 * Sets the Payment Gateway to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 */
	private static void setDefaultIBEPg(HttpServletRequest request) throws ModuleException {

		List<IPGPaymentOptionDTO> paymentGwList = CurrencyHTMLGenerator.getPaymentGatewatDetails(MODULE_IBE);

		if (paymentGwList != null && paymentGwList.size() > 0) {

			Iterator<IPGPaymentOptionDTO> itPG = paymentGwList.iterator();
			StringBuffer sbPaymentGatewayList = new StringBuffer("var arrIbeList = new Array();");

			while (itPG.hasNext()) {
				IPGPaymentOptionDTO ipgDefaultPG = (IPGPaymentOptionDTO) itPG.next();
				sbPaymentGatewayList.append("<option value='" + ipgDefaultPG.getPaymentGateway() + "'>"
						+ ipgDefaultPG.getDescription() + "</option>");

			}
			request.setAttribute(WebConstants.REQ_PAYMENT_GATEWAY_IBE, sbPaymentGatewayList.toString());
		}

	}

	/**
	 * Sets the Payment Gateway to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 */
	private static void setDefaultXBEPg(HttpServletRequest request) throws ModuleException {

		List<IPGPaymentOptionDTO> paymentGwList = CurrencyHTMLGenerator.getPaymentGatewatDetails(MODULE_XBE);

		if (paymentGwList != null && paymentGwList.size() > 0) {

			Iterator<IPGPaymentOptionDTO> itPG = paymentGwList.iterator();
			StringBuffer sbPaymentGatewayList = new StringBuffer("var arrXbeList = new Array();");

			while (itPG.hasNext()) {
				IPGPaymentOptionDTO ipgDefaultPG = (IPGPaymentOptionDTO) itPG.next();
				sbPaymentGatewayList.append("<option value='" + ipgDefaultPG.getPaymentGateway() + "'>"
						+ ipgDefaultPG.getDescription() + "</option>");

			}
			request.setAttribute(WebConstants.REQ_PAYMENT_GATEWAY_XBE, sbPaymentGatewayList.toString());
		}

	}

	/**
	 * Sets the Client Validations to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setClientErrors(HttpServletRequest request) throws ModuleException {
		String strClientErrors = CurrencyHTMLGenerator.getClientErrors(request);
		if (strClientErrors == null)
			strClientErrors = "";

		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
	}

	/**
	 * Sets the Language list to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setLanguageList(HttpServletRequest request) {
		try {
			String strHtml = SelectListGenerator.createLanguageList();
			request.setAttribute(WebConstants.REQ_HTML_LANGUAGE_LIST, strHtml);
		} catch (ModuleException moduleException) {
			log.error("Exception in AirportRequestHandler:setLanguageList() [origin module=" + moduleException.getModuleDesc()
					+ "]", moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Currency Grid Data To the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setCurrencyRowHtml(HttpServletRequest request) throws ModuleException {
		CurrencyHTMLGenerator currHTML = new CurrencyHTMLGenerator();
		String strHTML = currHTML.getCurrencyRowHtml(request);
		request.setAttribute(WebConstants.REQ_HTML_ROWS, strHTML);
	}

	private static void setCurrencyStatus(HttpServletRequest request) throws ModuleException {
		String strHTML = "<option value='" + Currency.STATUS_ACTIVE + "' selected>Active</option><option value='"
				+ Currency.STATUS_INACTIVE + "'>In-Active</option>";
		request.setAttribute(WebConstants.CURRENCY_STATUS, strHTML);

	}

	private static void setCurrentDate(HttpServletRequest request) throws ModuleException {
		String strHTML = "";
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		try {
			strHTML = sdf.format(date);
		} catch (Exception e) {
			log.error(e);
			// may not need to throw the exception
		}
		request.setAttribute(WebConstants.CURRENCY_SYS_DATE, strHTML);
	}

	/**
	 * Sets the Boundry value setiing visible or not according to app parameter
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setBoundaryVisibility(HttpServletRequest request) {
		GlobalConfig config = ModuleServiceLocator.getGlobalConfig();
		String strVisible = config.getBizParam(SystemParamKeys.CURRENCY_ROUNDUP);
		boolean visibility = (strVisible != null && strVisible.equals("Y")) ? true : false;
		request.setAttribute(WebConstants.CURR_BOUNDARY, visibility);
	}

	/**
	 * Delete the Currency Data
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void deleteCurrency(HttpServletRequest request) throws ModuleException {
		String currCode = request.getParameter(PARAM_CODE);
		try {
			if (currCode != null) {
				Currency existingCurrency = ModuleServiceLocator.getCommonServiceBD().getCurrency(currCode);
				if (existingCurrency == null) {
					// delete the currency for display list
					List<CurrencyForDisplay> l = ModuleServiceLocator.getTranslationBD().getCurrenciesByCurrencyCode(currCode);
					if (l != null && l.size() > 0) {
						ModuleServiceLocator.getTranslationBD().removeCurrency(currCode);
					}

					ModuleServiceLocator.getCommonServiceBD().deleteCurrency(currCode);

				} else {
					String strVersion = request.getParameter(PARAM_VERSION);
					// delete the currency for display list
					List<CurrencyForDisplay> l = ModuleServiceLocator.getTranslationBD().getCurrenciesByCurrencyCode(currCode);
					if (l != null && l.size() > 0) {
						ModuleServiceLocator.getTranslationBD().removeCurrency(currCode);
					}
					if (strVersion != null && !"".equals(strVersion)) {
						existingCurrency.setVersion(Long.parseLong(strVersion));
						ModuleServiceLocator.getCommonServiceBD().deleteCurrency(existingCurrency);
					} else {
						ModuleServiceLocator.getCommonServiceBD().deleteCurrency(currCode);
					}
				}
				setAttribInRequest(request, "isSaveTransactionSuccessfulJS", "var isSaveTransactionSuccessful = false;");
				saveMessage(request, adminConfig.getMessage(WebConstants.KEY_DELETE_SUCCESS), WebConstants.MSG_SUCCESS);
			}

		} catch (ModuleException moduleException) {

			setExceptionOccured(request, true);
			log.error("Exception in CurrencyRequestHandler:deleteData() [origin module=" + moduleException.getModuleDesc() + "]",
					moduleException);
			if (moduleException.getMessageString().equals(MessagesUtil.getMessage("module.constraint.childrecord"))) {
				saveMessage(request, adminConfig.getMessage("um.airadmin.childrecord"), WebConstants.MSG_ERROR);
			} else {
				saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
			}

		}
	}

	/**
	 * Creats a Property File Conatining Currecy Data fro the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return Properties the Property File with Currency Data
	 */
	private static Properties getProperties(HttpServletRequest request) {
		Properties props = new Properties();

		String strCode = AiradminUtils.getNotNullString(request.getParameter(PARAM_CODE));
		String strDescription = AiradminUtils.getNotNullString(request.getParameter(PARAM_DESCRIPTION));
		String strMode = AiradminUtils.getNotNullString(request.getParameter(PARAM_MODE));
		String strVersion = AiradminUtils.getNotNullString(request.getParameter(PARAM_VERSION));
		String strRecNo = request.getParameter(PARAM_RECNO);
		String strstatus = AiradminUtils.getNotNullString(request.getParameter(PARAM_STATUS));
		String strItineraryFareBreakDownEnabled = AiradminUtils.getNotNullString(request.getParameter(PARAM_ITINERARY_BREAKDOWN));
		String strBoundary = AiradminUtils.getNotNullString(request.getParameter(PARAM_BOUNDARY));
		String strBreakPoint = AiradminUtils.getNotNullString(request.getParameter(PARAM_BREAKPOINT));
		String strIbeVisibility = request.getParameter(PARAM_IBEVISIBILITY);
		String strXbeVisibility = request.getParameter(PARAM_XBEVISIBILITY);
		String strCardPayVisibility = request.getParameter(PARAM_CARDPAY_VISIBILITY);
		String strExRateData = request.getParameter(PARAM_HDNEXRATEDATA);
		String strExRateGrid = request.getParameter(PARAM_HDNEXRATEGRID);
		String strPayGtwIbe = request.getParameter(PARAM_PAYMENTGATEWAY_IBE);
		String strPayGtwXbe = request.getParameter(PARAM_PAYMENTGATEWAY_XBE);
		String strDecimalPlaces = request.getParameter(PARAM_CURRENCY_DECIMAL_PLACES);

		props.setProperty(PARAM_CODE, strCode);
		props.setProperty(PARAM_DESCRIPTION, strDescription);
		props.setProperty(PARAM_MODE, strMode);
		props.setProperty(PARAM_VERSION, strVersion);
		props.setProperty(PARAM_RECNO, (strRecNo == null) ? "1" : strRecNo);
		props.setProperty(PARAM_STATUS, strstatus);
		props.setProperty(PARAM_ITINERARY_BREAKDOWN, strItineraryFareBreakDownEnabled);
		props.setProperty(PARAM_BOUNDARY, strBoundary);
		props.setProperty(PARAM_BREAKPOINT, strBreakPoint);
		props.setProperty(PARAM_IBEVISIBILITY, (strIbeVisibility == null) ? "N" : strIbeVisibility);
		props.setProperty(PARAM_XBEVISIBILITY, (strXbeVisibility == null) ? "N" : strXbeVisibility);
		props.setProperty(PARAM_CARDPAY_VISIBILITY, (strCardPayVisibility == null) ? "N" : strCardPayVisibility);
		props.setProperty(PARAM_PAYMENTGATEWAY_IBE, (strPayGtwIbe == null) ? "" : strPayGtwIbe);
		props.setProperty(PARAM_PAYMENTGATEWAY_XBE, (strPayGtwXbe == null) ? "" : strPayGtwXbe);
		props.setProperty(PARAM_HDNEXRATEDATA, (strExRateData == null || "".equals(strExRateData)) ? "" : strExRateData);
		props.setProperty(PARAM_HDNEXRATEGRID, (strExRateGrid == null || "".equals(strExRateGrid)) ? "" : strExRateGrid);
		props.setProperty(PARAM_CURRENCY_DECIMAL_PLACES, (strDecimalPlaces == null || "".equals(strDecimalPlaces))
				? "0"
				: strDecimalPlaces);

		props.setProperty(PARAM_CURRENCY_FOR_DISPLAY,
				AiradminUtils.getNotNullString(request.getParameter(PARAM_CURRENCY_FOR_DISPLAY)));

		return props;
	}

	/**
	 * Adds and/or updatdes exchange rate data.
	 * 
	 * @param existingExRates
	 * @param addModifiedExRateData
	 * @throws ModuleException
	 * @throws Exception
	 */
	private static void addOrUpdateExRateData(Currency currency, String addModifiedExRateData) throws ModuleException {

		if (!"".equals(addModifiedExRateData)) {
			String[] arrAddModifiedExRateDataRows = StringUtils.split(addModifiedExRateData, "|");
			if (arrAddModifiedExRateDataRows != null && arrAddModifiedExRateDataRows.length > 0) {
				SimpleDateFormat smdt = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

				Date effFrom = null;
				Date effTo = null;
				BigDecimal exRate = null;
				BigDecimal purchaseRate = null;// JIRA : AARESAA-3087
				String status = null;

				for (int i = 0; i < arrAddModifiedExRateDataRows.length; i++) {
					String[] arrAddModifiedExRateDataRow = StringUtils.split(arrAddModifiedExRateDataRows[i], "^");
					CurrencyExchangeRate currentExRate = null;

					try {
						effFrom = smdt.parse(arrAddModifiedExRateDataRow[3] + ":00");
						effTo = smdt.parse(arrAddModifiedExRateDataRow[4] + ":59");
					} catch (ParseException e) {
						throw new ModuleException("airmaster.currency.update.invalid.dates.error");
					}

					exRate = new BigDecimal(arrAddModifiedExRateDataRow[5]);
					status = arrAddModifiedExRateDataRow[6];
					/** JIRA : AARESAA-3087 - Issue 6 */
					if (arrAddModifiedExRateDataRow[8] != null && !"-1".equals(arrAddModifiedExRateDataRow[8])) {
						purchaseRate = new BigDecimal(arrAddModifiedExRateDataRow[8]);
					}

					if ("NEW".equals(arrAddModifiedExRateDataRow[0])) {
						currentExRate = new CurrencyExchangeRate();
						currentExRate.setCurrency(currency);
						currentExRate.setFaresUpdatedForExchangeRateChange(Currency.AUTOMATED_EXRATE_DISABLED);
						currentExRate.setChargesUpdatedForExchangeRateChange(Currency.AUTOMATED_EXRATE_DISABLED);
						currentExRate.setAnciChargesUpdatedForExchangeRateChange(Currency.AUTOMATED_EXRATE_DISABLED);
						if (currency.getExchangeRates() == null) {
							currency.setExchangeRates(new HashSet<CurrencyExchangeRate>());
						}
						currency.getExchangeRates().add(currentExRate);

					} else if ("CHANGED".equals(arrAddModifiedExRateDataRow[0])) {
						Long exRateId = Long.parseLong(arrAddModifiedExRateDataRow[1]);
						for (Iterator<CurrencyExchangeRate> existingExRatesIt = currency.getExchangeRates().iterator(); existingExRatesIt
								.hasNext();) {
							CurrencyExchangeRate existingExRate = (CurrencyExchangeRate) existingExRatesIt.next();
							if (existingExRate.getExchangeRateId().equals(exRateId)) {
								currentExRate = existingExRate;
								break;
							}
						}

						if (currentExRate == null) {
							throw new ModuleException("airmaster.currency.update.inconsistent.data.error");// Inconsistent
																											// data or
																											// manual
																											// deletion
																											// of data
						}
						currentExRate.setVersion(Long.parseLong(arrAddModifiedExRateDataRow[2]));
						currentExRate.setIsModified(new Boolean(true));
					}

					currentExRate.setEffectiveFrom(effFrom);
					currentExRate.setEffectiveTo(effTo);
					// currentExRate.setExchangeRate(exRate);
					currentExRate.setExrateBaseToCurNumber(exRate);// JIRA : AARESAA-3087
					currentExRate.setExrateCurToBaseNumber(purchaseRate);
					currentExRate.setStatus(status);
				}
			}
		}
	}

	/**
	 * Creats the Form Data for Currency Page from e Property file
	 * 
	 * @param erprop
	 *            the Property file with Currency Data
	 * @return String the Array of from data
	 */
	private static String getErrorForm(Properties erprop) {

		StringBuffer ersb = new StringBuffer("var arrFormData = new Array();");
		ersb.append("arrFormData[0] = new Array();");
		ersb.append("arrFormData[0][1] = '" + erprop.getProperty(PARAM_CODE) + "';");
		ersb.append("arrFormData[0][2] = '" + erprop.getProperty(PARAM_DESCRIPTION) + "';");
		ersb.append("arrFormData[0][4] = '" + erprop.getProperty(PARAM_STATUS) + "';");
		ersb.append("arrFormData[0][5] = '" + erprop.getProperty(PARAM_VERSION) + "';");
		ersb.append("arrFormData[0][7] = '" + erprop.getProperty(PARAM_BOUNDARY) + "';");
		ersb.append("arrFormData[0][8] = '" + erprop.getProperty(PARAM_BREAKPOINT) + "';");
		ersb.append("arrFormData[0][9] = '" + erprop.getProperty(PARAM_IBEVISIBILITY) + "';");
		ersb.append("arrFormData[0][10] = '" + erprop.getProperty(PARAM_XBEVISIBILITY) + "';");
		ersb.append("arrFormData[0][11] = '" + erprop.getProperty(PARAM_CARDPAY_VISIBILITY) + "';");
		ersb.append("arrFormData[0][12] = '" + erprop.getProperty(PARAM_HDNEXRATEDATA) + "';");
		ersb.append("arrFormData[0][13] = '" + erprop.getProperty(PARAM_HDNEXRATEGRID) + "';");
		ersb.append("arrFormData[0][14] = '" + erprop.getProperty(PARAM_PAYMENTGATEWAY_IBE) + "';");
		ersb.append("arrFormData[0][15] = '" + erprop.getProperty(PARAM_PAYMENTGATEWAY_XBE) + "';");
		ersb.append("arrFormData[0][16] = '" + erprop.getProperty(PARAM_CURRENCY_DECIMAL_PLACES) + "';");
		return ersb.toString();
	}
}
