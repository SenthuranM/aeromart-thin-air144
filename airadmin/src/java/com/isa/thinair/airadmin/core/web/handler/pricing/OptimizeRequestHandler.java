package com.isa.thinair.airadmin.core.web.handler.pricing;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Properties;
import java.util.Set;
import java.util.Vector;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.pricing.OptimizeHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airinventory.api.dto.FCCSegInventoryUpdateDTO;
import com.isa.thinair.airinventory.api.dto.FCCSegInventoryUpdateStatusDTO;
import com.isa.thinair.airinventory.api.model.FCCSegBCInventory;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * @author cBandara
 * 
 */

public final class OptimizeRequestHandler extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(OptimizeRequestHandler.class);

	private static final String PARAM_MODE = "hdnMode";
	private static final String PARAM_VERSION = "hdnVersion";
	private static final String PARAM_FROMDATE = "txtFromDate";
	private static final String PARAM_TODATE = "txtToDate";
	private static final String PARAM_FLIGHTNO = "txtFlightNo";
	private static final String PARAM_ASSIGN_OND = "hdnAssignOnD";
	private static final String PARAM_ASSIGN_BC = "hdnAssignBC";
	private static final String PARAM_ALLOCS_TYPE = "hdnAllocType"; // Allocations
																	// with + / -
	private static final String PARAM_SEATS = "txtSeats";
	private static final String PARAM_MORELESS = "selMoreLess";
	private static final String PARAM_SEGLEVEL = "radSegLevel";
	private static final String PARAM_BCLEVEL = "radBCLevel";
	private static final String PARAM_SHOWBC = "radShowBc";
	private static final String PARAM_ISAGENT = "radAgent";
	private static final String PARAM_AGENT_CODE = "selAgent";
	private static final String PARAM_BCDIS_TYPE = "hdnBCDisType";
	private static final String PARAM_OPTIMIZE_DATA = "hdnOptimizeValues";
	private static final String PARAM_LOGICAL_CC = "hdnLogicalCC";

	/**
	 * Main Execute Method for OPTIMIZE Action
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @param response
	 *            the HttpServletResponse
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {

		String forward = AdminStrutsConstants.AdminAction.SUCCESS;
		String strHdnMode = request.getParameter(PARAM_MODE);
		request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, "var saveSuccess = '';");

		if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_EXPORT)) {
			exportToExcel(request, response);
			return null;
		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_SAVE)) {
				checkPrivilege(request, WebConstants.PLAN_INVN_OPT_EDIT);
				saveData(request);
				saveMessage(request, "Record updated Successfully", WebConstants.MSG_SUCCESS);
				request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, "var saveSuccess = 1;");
				request.setAttribute(WebConstants.REQ_OPTIMIZE_STATUS, "SAVE");
				log.debug("OptimizeRequestHandler.saveData() method is successfully executed.");
			}
		} catch (Exception exception) {
			log.error("OptimizeRequestHandler.execute() saving data failed :" + exception, exception);
			if (exception instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
			} else {
				saveMessage(request, exception.getMessage(), WebConstants.MSG_ERROR);
			}
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, "var saveSuccess = 0;");
		}

		try {
			setDisplayData(request);
		} catch (Exception exception) {
			log.error("OptimizeRequestHandler.execute() preparing display data failed :" + exception, exception);
			if (exception instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
			} else {
				saveMessage(request, exception.getMessage(), WebConstants.MSG_ERROR);
			}
		}
		return forward;
	}

	/**
	 * Saves the OPTIMIZE Data
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void saveData(HttpServletRequest request) throws ModuleException {
		Set<FCCSegInventoryUpdateDTO> fccSegInventoryUpdateDTOs = new HashSet<FCCSegInventoryUpdateDTO>();
		FCCSegInventoryUpdateDTO updateDTO = null;
		FCCSegBCInventory updatedBC = null;
		LinkedHashMap<Integer, FCCSegInventoryUpdateStatusDTO> shmUpdateStatus = null;
		Set<FCCSegBCInventory> updatedFFCSegBC = null;
		int intTempInvId = -1;
		int intResultCount = 0;
		FCCSegInventoryUpdateStatusDTO fCCSegInventoryUpdateStatusDTO = null;
		StringBuffer optsb = new StringBuffer("var optimizeResult = new Array();");

		String strOptimiseData = request.getParameter(PARAM_OPTIMIZE_DATA);

		if (strOptimiseData != null && strOptimiseData.length() > 0) {
			String[] strEditedRowData = strOptimiseData.split(":");

			for (int i = 0; i < strEditedRowData.length; i++) {

				String[] strEditedColData = strEditedRowData[i].split(",");

				if (intTempInvId != Integer.parseInt(strEditedColData[0])) {
					if (i != 0) {
						updateDTO.setUpdatedFCCSegBCInventories(updatedFFCSegBC);
						fccSegInventoryUpdateDTOs.add(updateDTO);
					}
					updateDTO = new FCCSegInventoryUpdateDTO();

					updateDTO.setFccsInventoryId(Integer.parseInt(strEditedColData[0]));
					updateDTO.setFlightId(Integer.parseInt(strEditedColData[1]));
					updateDTO.setLogicalCCCode(strEditedColData[17]);
					updateDTO.setSegmentCode(strEditedColData[3]);
					updateDTO.setInfantAllocation(Integer.parseInt(strEditedColData[4]));
					updateDTO.setOversellSeats(Integer.parseInt(strEditedColData[5]));
					updateDTO.setCurtailedSeats(Integer.parseInt(strEditedColData[6]));

					updatedFFCSegBC = new HashSet<FCCSegBCInventory>();
					updatedBC = new FCCSegBCInventory();
					if (strEditedColData.length == 18) {
						updatedBC.setFccsbInvId(new Integer(strEditedColData[9]));
						updatedBC.setSeatsAllocated(Integer.parseInt(strEditedColData[7]));
						updatedBC.setFlightId(Integer.parseInt(strEditedColData[1]));
						updatedBC.setLogicalCCCode(strEditedColData[17]);
						updatedBC.setSegmentCode(strEditedColData[3]);
						updatedBC.setBookingCode(strEditedColData[10]);

						if (strEditedColData[8].equals("true")) {
							updatedBC.setStatus(FCCSegBCInventory.Status.CLOSED);

						} else {
							updatedBC.setStatus(FCCSegBCInventory.Status.OPEN);

						}
						if (Boolean.getBoolean(strEditedColData[11])) {
							updatedBC.setPriorityFlag(true);
						} else {
							updatedBC.setPriorityFlag(false);
						}
						if (strEditedColData[14].equals("true")) {
							updatedBC.setPriorityFlag(true);

						} else {
							updatedBC.setPriorityFlag(false);
						}
						updatedBC.setStatusChangeAction(strEditedColData[15]);
						updatedBC.setSeatsAcquired(Integer.parseInt(strEditedColData[16]));

						updateDTO.setVersion(Long.parseLong(strEditedColData[12]));
						updatedBC.setVersion(Long.parseLong(strEditedColData[13]));
						updatedFFCSegBC.add(updatedBC);
					} else {
						updateDTO.setVersion(Long.parseLong(strEditedColData[7]));
					}
					intTempInvId = Integer.parseInt(strEditedColData[0]);
				} else {

					updatedBC = new FCCSegBCInventory();
					updatedBC.setFccsbInvId(new Integer(strEditedColData[9]));
					updatedBC.setSeatsAllocated(Integer.parseInt(strEditedColData[7]));
					updatedBC.setFlightId(Integer.parseInt(strEditedColData[1]));
					updatedBC.setLogicalCCCode(strEditedColData[17]);
					updatedBC.setSegmentCode(strEditedColData[3]);
					updatedBC.setBookingCode(strEditedColData[10]);

					if (strEditedColData[8].equals("true")) {
						updatedBC.setStatus(FCCSegBCInventory.Status.CLOSED);

					} else {
						updatedBC.setStatus(FCCSegBCInventory.Status.OPEN);

					}
					if (Boolean.getBoolean(strEditedColData[11])) {
						updatedBC.setPriorityFlag(true);
					} else {
						updatedBC.setPriorityFlag(false);
					}
					if (strEditedColData[14].equals("true")) {
						updatedBC.setPriorityFlag(true);

					} else {
						updatedBC.setPriorityFlag(false);
					}
					updatedBC.setStatusChangeAction(strEditedColData[15]);
					updatedBC.setSeatsAcquired(Integer.parseInt(strEditedColData[16]));

					updatedBC.setVersion(Long.parseLong(strEditedColData[13]));
					updatedFFCSegBC.add(updatedBC);
				}
			}
			updateDTO.setUpdatedFCCSegBCInventories(updatedFFCSegBC);
			fccSegInventoryUpdateDTOs.add(updateDTO);

			shmUpdateStatus = ModuleServiceLocator.getFlightInventoryBD().updateFCCSegInventories(fccSegInventoryUpdateDTOs);

			Iterator<Integer> iteKey = shmUpdateStatus.keySet().iterator();

			while (iteKey.hasNext()) {
				Integer strInventoryId = (Integer) iteKey.next();
				fCCSegInventoryUpdateStatusDTO = (FCCSegInventoryUpdateStatusDTO) shmUpdateStatus.get(strInventoryId);

				optsb.append("optimizeResult[" + intResultCount + "]    = new Array('','','','','','');");
				optsb.append("optimizeResult[" + intResultCount + "][0] = '" + strInventoryId + "';");
				optsb.append("optimizeResult[" + intResultCount + "][1] = '" + fCCSegInventoryUpdateStatusDTO.getStatus() + "';");
				optsb.append("optimizeResult["
						+ intResultCount
						+ "][2] = '"
						+ (fCCSegInventoryUpdateStatusDTO.getFailureLevel() == null ? " " : fCCSegInventoryUpdateStatusDTO
								.getFailureLevel()) + "';");
				optsb.append("optimizeResult[" + intResultCount + "][3] = '"
						+ (fCCSegInventoryUpdateStatusDTO.getMsg() == null ? " " : fCCSegInventoryUpdateStatusDTO.getMsg())
						+ "';");
				optsb.append("optimizeResult["
						+ intResultCount
						+ "][4] = '"
						+ (fCCSegInventoryUpdateStatusDTO.getFccsbInvId() == null ? " " : fCCSegInventoryUpdateStatusDTO
								.getFccsbInvId().toString()) + "';");
				optsb.append("optimizeResult["
						+ intResultCount
						+ "][5] = '"
						+ (fCCSegInventoryUpdateStatusDTO.getBookingCode() == null ? " " : fCCSegInventoryUpdateStatusDTO
								.getBookingCode()) + "';");
				intResultCount++;
			}
			request.setAttribute(WebConstants.REQ_OPTIMIZE_RESULT, optsb.toString());
		}
	}

	/**
	 * Sets the Diplay Data for Optimizing Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	public static void setDisplayData(HttpServletRequest request) throws Exception {

		setClientErrors(request);
		setOptimizeRowHtml(request);
	}

	/**
	 * Sets the Client Validation for Orimizing Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static void setClientErrors(HttpServletRequest request) throws ModuleException {

		String strClientErrors = OptimizeHTMLGenerator.getSearchClientErrors(request);
		if (strClientErrors == null) {
			strClientErrors = "";
		}
		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
	}

	/**
	 * Sets the Optimizing Grid row to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	protected static void setOptimizeRowHtml(HttpServletRequest request) throws ModuleException {

		OptimizeHTMLGenerator htmlGen = new OptimizeHTMLGenerator(getProperties(request));
		String strHtml = htmlGen.getOptimizeRowHtml(request);
		request.setAttribute(WebConstants.REQ_HTML_ROWS, strHtml);
	}

	/**
	 * Creates an Excel File for the File
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @param response
	 *            the HttpServletResponse
	 */
	@SuppressWarnings("rawtypes")
	private static void exportToExcel(HttpServletRequest request, HttpServletResponse response) {

		try {
			String strExcelRow = "";
			HttpSession session = request.getSession(true);
			Vector vecExcelRowData = (Vector) session.getAttribute("ExcelData");

			String vFileName = "InventoryOptimization.csv";

			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Cache-Control", "cache");
			response.setHeader("Cache-Control", "must-revalidate");
			response.setHeader("Pragma", "public");

			response.setHeader("Content-Disposition", "attachment; filename=\"" + vFileName + "\"");

			ServletOutputStream os = response.getOutputStream();
			OutputStreamWriter wri = new OutputStreamWriter(os);
			BufferedWriter buff = new BufferedWriter(wri);

			for (int i = 0; i < vecExcelRowData.size(); i++) {
				String[] strExcelColData = (String[]) vecExcelRowData.get(i);
				strExcelRow = "";
				for (int j = 0; j < strExcelColData.length; j++) {
					strExcelRow += strExcelColData[j];
					if (!strExcelColData[j].equals(",")) {
						strExcelRow += ",";
					}
				}
				buff.write(strExcelRow);
				buff.write("\n");
			}
			buff.flush();
			buff.close();
			response.flushBuffer();
		} catch (Exception exception) {
			log.error("ExportToExcel failed :" + exception.toString());
		}
	}

	/**
	 * Creates a Property file having Front End Data
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return Properties the Property file with Front End Data
	 */
	protected static Properties getProperties(HttpServletRequest request) {
		Properties props = new Properties();

		String strVersion = request.getParameter(PARAM_VERSION);
		String strFromDate = request.getParameter(PARAM_FROMDATE);
		String strToDate = request.getParameter(PARAM_TODATE);
		String strFlightNo = request.getParameter(PARAM_FLIGHTNO);
		String strAssignOND = request.getParameter(PARAM_ASSIGN_OND);
		String strAssignBC = request.getParameter(PARAM_ASSIGN_BC);
		String strAllocsType = request.getParameter(PARAM_ALLOCS_TYPE);
		String strSeats = request.getParameter(PARAM_SEATS);
		String strMoreLess = request.getParameter(PARAM_MORELESS);
		String strSegLevel = request.getParameter(PARAM_SEGLEVEL);
		String strBCLevel = request.getParameter(PARAM_BCLEVEL);
		String strShowBC = request.getParameter(PARAM_SHOWBC);
		String strIsAgent = request.getParameter(PARAM_ISAGENT);
		String strBCDisType = request.getParameter(PARAM_BCDIS_TYPE);
		String strAgentCode = request.getParameter(PARAM_AGENT_CODE);
		String logicalCC = request.getParameter(PARAM_LOGICAL_CC);

		props.setProperty(PARAM_VERSION, (strVersion == null) ? "" : strVersion);
		props.setProperty(PARAM_FROMDATE, (strFromDate == null) ? "" : strFromDate);
		props.setProperty(PARAM_TODATE, (strToDate == null) ? "" : strToDate);
		props.setProperty(PARAM_FLIGHTNO, (strFlightNo == null) ? "" : strFlightNo);
		props.setProperty(PARAM_ASSIGN_OND, (strAssignOND == null) ? "" : strAssignOND);
		props.setProperty(PARAM_ASSIGN_BC, (strAssignBC == null) ? "" : strAssignBC);
		props.setProperty(PARAM_ALLOCS_TYPE, (strAllocsType == null) ? "" : strAllocsType);
		props.setProperty(PARAM_SEATS, (strSeats == null) ? "" : strSeats);
		props.setProperty(PARAM_MORELESS, (strMoreLess == null) ? "" : strMoreLess);
		props.setProperty(PARAM_SEGLEVEL, (strSegLevel == null) ? "" : strSegLevel);
		props.setProperty(PARAM_BCLEVEL, (strBCLevel == null) ? "" : strBCLevel);
		props.setProperty(PARAM_SHOWBC, (strShowBC == null) ? "" : strShowBC);
		props.setProperty(PARAM_ISAGENT, (strIsAgent == null) ? "" : strIsAgent);
		props.setProperty(PARAM_BCDIS_TYPE, (strBCDisType == null) ? "" : strBCDisType);
		props.setProperty(PARAM_AGENT_CODE, (strAgentCode == null) ? "" : strAgentCode);
		props.setProperty(PARAM_LOGICAL_CC, (logicalCC==null)?"":logicalCC);
		return props;
	}
}
