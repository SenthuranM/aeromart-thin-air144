/*
 * ==============================================================================
 * ISA Software License, Version 1.0
 *
 * Copyright (c) 2003 The Imformation Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * =============================================================================== 
 */
package com.isa.thinair.airadmin.core.web.generator.security;

import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * 
 * @author Chamindap
 * 
 */
public class LoginHTMLGenerator {
	private static String clientErrors;

	/**
	 * Creates the Client validations for Login Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array Containing the Client validations
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {
		if (clientErrors == null) {

			Properties moduleErrs = new Properties();

			moduleErrs.setProperty("um.airadmin.login.invalid.username", "userInvalidID");
			moduleErrs.setProperty("um.airadmin.login.invalid.password", "userInvalidPassword");
			moduleErrs.setProperty("um.airadmin.login.invalid.useridpassword", "userInvalidUserIdPassword");
			moduleErrs.setProperty("um.airadmin.login.inactive.user", "userInactiveStatus");
			clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}
		return clientErrors;
	}
}
