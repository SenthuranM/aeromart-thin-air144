package com.isa.thinair.airadmin.core.web.v2.action.tools;

import java.util.Collection;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airmaster.api.model.Language;
import com.isa.thinair.commons.api.dto.BundleFareDescriptionTemplateDTO;
import com.isa.thinair.commons.api.dto.BundleFareDescriptionTranslationTemplateDTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Result(name = AdminStrutsConstants.AdminAction.SUCCESS, type = JSONResult.class, value = "")
public class ShowBundleDescriptionAction extends BaseRequestAwareAction {

	private static final int PAGE_LENGTH = 10;
	private static AiradminConfig airadminConfig = new AiradminConfig();

	private Integer templateID;
	private Collection<BundleFareDescriptionTemplateDTO> bundleTemplates;
	private BundleFareDescriptionTemplateDTO updatedBundleTemplate;
	private int page;	
	private int totalRecords;
	private int totalPages;
	private String templateNameSearchText;
	private boolean success = true;
	private String messageTxt;
	private boolean isNewSearch;
	private Collection<Language> languagelist;
	private Language baseLanguage;
	
	private BundleFareDescriptionTranslationTemplateDTO translationTemplateDTO;

	private Log log = LogFactory.getLog(ShowBundleDescriptionAction.class);

	public String execute() {
		try {
			//  If it is a new serach then page should be set as 1
			if (isNewSearch)
				page = 1;

			int startingRecordIndex = (page - 1) * PAGE_LENGTH;

			Page<BundleFareDescriptionTemplateDTO> page = ModuleServiceLocator.getBundledFareBD()
					.getBundleFareDescriptionTemplate(StringUtils.defaultIfBlank(templateNameSearchText, ""), startingRecordIndex,
							PAGE_LENGTH);
			bundleTemplates = page.getPageData();

			totalRecords = page.getTotalNoOfRecords();
			totalPages = totalRecords % PAGE_LENGTH > 0 ? (totalRecords / PAGE_LENGTH) + 1 : (totalRecords / PAGE_LENGTH);
		} catch (Exception x) {
			success = false;
			messageTxt = airadminConfig.getMessage("um.airadmin.bundle.desc.update.fail");
			log.error(messageTxt, x);
		}
		return AdminStrutsConstants.AdminAction.SUCCESS;
	}

	public String updateBundleTemplate() {
		try {
			ModuleServiceLocator.getBundledFareBD().saveOrUpdateBundleFareDescriptionTemplate(updatedBundleTemplate);
		} catch (Exception x) {
			success = false;
			messageTxt = airadminConfig.getMessage("um.airadmin.bundle.desc.update.fail");
			log.error(messageTxt, x);
		}
		return AdminStrutsConstants.AdminAction.SUCCESS;
	}
	
	public String updateBundleTraslationTemplate() {
		try {
			ModuleServiceLocator.getBundledFareBD().updateBundleFareDescriptionTranslationTemplate(translationTemplateDTO);
		} catch (Exception x) {
			success = false;
			messageTxt = airadminConfig.getMessage("um.airadmin.bundle.desc.tras.update.fail"); 
			log.error(messageTxt, x);
		}
		return AdminStrutsConstants.AdminAction.SUCCESS;
	}
	
	
	public String retrieveLanguages() {

		try {
			languagelist = ModuleServiceLocator.getCommonServiceBD().getLanguages();

			String baseLanguageCode = AppSysParamsUtil.getSystemDefaultLanguage();
			baseLanguage = languagelist.stream().filter(item -> item.getLanguageCode().equals(baseLanguageCode)).findFirst()
					.orElse(null);
			languagelist.removeIf(item -> item.getLanguageCode().equals(baseLanguageCode));

		} catch (ModuleException e) {
			success = false;
			messageTxt = airadminConfig.getMessage("um.airadmin.bundle.desc.language.fail");
			log.error(messageTxt, e);
		}

		return AdminStrutsConstants.AdminAction.SUCCESS;
	}
	
	public String retrieveBundleTemplateById() {
		updatedBundleTemplate = ModuleServiceLocator.getBundledFareBD()
				.getBundleFareDescriptionTemplateById(templateID);
		if (updatedBundleTemplate == null || updatedBundleTemplate.getTemplateID() == null) {
			success = false;
			messageTxt = airadminConfig.getMessage("um.airadmin.bundle.desc.id.fail");
			log.error(messageTxt);
		}
		return AdminStrutsConstants.AdminAction.SUCCESS;
	}

	public Collection<BundleFareDescriptionTemplateDTO> getBundleTemplates() {
		return bundleTemplates;
	}

	public void setBundleTemplates(Collection<BundleFareDescriptionTemplateDTO> bundleTemplates) {
		this.bundleTemplates = bundleTemplates;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int currentPage) {
		this.page = currentPage;
	}

	public int getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(int totalRecords) {
		this.totalRecords = totalRecords;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public BundleFareDescriptionTemplateDTO getUpdatedBundleTemplate() {
		return updatedBundleTemplate;
	}

	public void setUpdatedBundleTemplate(BundleFareDescriptionTemplateDTO updatedBundleTemplate) {
		this.updatedBundleTemplate = updatedBundleTemplate;
	}

	public Integer getTemplateID() {
		return templateID;
	}

	public void setTemplateID(Integer templateID) {
		this.templateID = templateID;
	}

	public String getTemplateNameSearchText() {
		return templateNameSearchText;
	}

	public void setTemplateNameSearchText(String templateNameSearchText) {
		this.templateNameSearchText = templateNameSearchText;
	}

	public boolean isNewSearch() {
		return isNewSearch;
	}

	public void setNewSearch(boolean isNewSearch) {
		this.isNewSearch = isNewSearch;
	}

	public Collection<Language> getLanguagelist() {
		return languagelist;
	}

	public void setLanguagelist(Collection<Language> langTraslations) {
		this.languagelist = langTraslations;
	}

	public Language getBaseLanguage() {
		return baseLanguage;
	}
	
}
