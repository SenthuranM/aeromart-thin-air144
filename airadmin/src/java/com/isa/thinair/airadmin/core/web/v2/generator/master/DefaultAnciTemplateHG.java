package com.isa.thinair.airadmin.core.web.v2.generator.master;

import java.util.Collection;
import java.util.Iterator;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airpricing.api.model.DefaultAnciTemplate;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

public class DefaultAnciTemplateHG {

	public static void setHTMLComponents(HttpServletRequest request) throws ModuleException {
		request.setAttribute(WebConstants.REQ_HTML_ACTIVE_AIRPORT_SELECT_LIST, getActiveAirportList());
		request.setAttribute(WebConstants.REQ_MODELS, setAirCraftModelList());
		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, getClientErrors());
		request.setAttribute(WebConstants.REQ_DEFLT_ANCI_TEMPL_STATUS, getTemplateStatus());
		request.setAttribute(WebConstants.REQ_ANCI_TEMPL_TYPE, getAnciTemplateTypes());

		request.setAttribute(S2Constants.RequestConstant.ML_TEMPLATE_CODE, SelectListGenerator.createActiveMealTemplateList());
		request.setAttribute(WebConstants.SEAT_TEMPL_CODE, SelectListGenerator.createActSeatMapChargeTemplateListStr());
		request.setAttribute(WebConstants.BAGGAGE_TEMPL_CODE, getBaggageTemplateCodes());

	}

	private static Object getAnciTemplateTypes() {

		StringBuilder sb = new StringBuilder();

		for (DefaultAnciTemplate.ANCI_TEMPLATES templ : DefaultAnciTemplate.ANCI_TEMPLATES.values()) {
			sb.append("<option value='").append(templ).append("' >").append(templ).append("</option>");
		}
		return sb.toString();
	}

	private static String getActiveAirportList() throws ModuleException {
		return SelectListGenerator.createActiveAirportList();
	}

	private static String setAirCraftModelList() throws ModuleException {
		return SelectListGenerator.createActiveAircraftModelList_SG();
	}

	private static String getClientErrors() throws ModuleException {
		Properties moduleErrs = new Properties();
		
		moduleErrs.setProperty("um.DefaultAnciTemplate.routes.required","routesReq");
		moduleErrs.setProperty("um.DefaultAnciTemplate.ancillary.template.required","templateReq");
		moduleErrs.setProperty("um.DefaultAnciTemplate.airCraftModel.required","acModelReq");
		moduleErrs.setProperty("um.DefaultAnciTemplate.status.required","statusReq");
		moduleErrs.setProperty("um.DefaultAnciTemplate.AncillalyType.required","typeReq");
		moduleErrs.setProperty("um.DefaultAnciTemplate.ond.exist","ondExistErr");
		moduleErrs.setProperty("um.DefaultAnciTemplate.invalidRoutes.exist","invalidOndErr");
		moduleErrs.setProperty("um.charges.depature.required", "depatureRqrd");
		moduleErrs.setProperty("um.charges.arrival.required", "arrivalRqrd");
		moduleErrs.setProperty("um.charges.via.required", "viaPointRqrd");
		moduleErrs.setProperty("um.charges.arrivalDepature.same", "depatureArriavlSame");
		moduleErrs.setProperty("um.charges.arrivalVia.same", "arriavlViaSame");
		moduleErrs.setProperty("um.charges.depatureVia.same", "depatureViaSame");
		moduleErrs.setProperty("um.charges.via.sequence", "vianotinSequence");
		moduleErrs.setProperty("um.charges.via.equal", "viaEqual");
		moduleErrs.setProperty("um.charges.departure.inactive", "departureInactive");
		moduleErrs.setProperty("um.charges.arrival.inactive", "arrivalInactive");
		moduleErrs.setProperty("um.charges.via.inactine", "viaInactive");
		moduleErrs.setProperty("um.charges.ond.required", "ondRequired");
		moduleErrs.setProperty("um.airadmin.delete.confirmation", "deleteMessage");
		moduleErrs.setProperty("um.charge.journey.null", "journeynull");
		moduleErrs.setProperty("um.charge.boundry.break.point.both.needed", "boundryValueAndBreakPointNeeded");
		moduleErrs.setProperty("um.charge.trnsit.dur.null", "durNull");
		moduleErrs.setProperty("um.charge.trnsit.dur.format", "DurIncco");
		moduleErrs.setProperty("um.charge.trnsit.dur.zero", "durZero");
		moduleErrs.setProperty("um.airadmin.delete.confirmation", "deleteRecoredCfrm");

		return JavascriptGenerator.createClientErrors(moduleErrs);
	}

	private static String getTemplateStatus() throws ModuleException {
		return "<option value='" + DefaultAnciTemplate.STATUS_ACTIVE + "' selected>Active</option><option value='"
				+ DefaultAnciTemplate.STATUS_INACTIVE + "'>In-Active</option>";
	}

	private static String getBaggageTemplateCodes() throws ModuleException {

		String s[] = null;
		StringBuilder sb = new StringBuilder();
		Collection<String[]> colTemplates = SelectListGenerator.createBaggageChargesTemplateIDs("");
		if (colTemplates != null) {
			Iterator<String[]> iter = colTemplates.iterator();
			while (iter.hasNext()) {
				s = (String[]) iter.next();
				sb.append("<option value='" + s[0] + "'>" + s[1] + "</option>");
			}
		}
		return sb.toString();
	}
}
