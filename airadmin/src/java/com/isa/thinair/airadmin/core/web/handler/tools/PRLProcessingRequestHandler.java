/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */

package com.isa.thinair.airadmin.core.web.handler.tools;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.tools.PRLProcessingHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.messagepasser.api.model.PRL;
import com.isa.thinair.platform.api.ServiceResponce;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class PRLProcessingRequestHandler extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(PRLProcessingRequestHandler.class);
	private static AiradminConfig airadminConfig = new AiradminConfig();

	private static final String PARAM_VERSION = "hdnVersion";
	private static final String PARAM_PAXCOUNT = "hdnPaxCount";
	private static final String PARAM_PRL_DOWNLOAD_TS = "txtDownloadTS";
	private static final String PARAM_PRL_DOWNLOAD_TIME = "txtDownloadTime";
	private static final String PARAM_PRL_FLIGHT_NO = "txtFlightNo";
	private static final String PARAM_PRL_FLIGHT_DATE = "txtFlightDateTime";
	private static final String PARAM_PRL_FLIGHT_TIME = "txFlightTime";
	private static final String PARAM_PRL_FROM_AIRPORT = "selDest";
	private static final String PARAM_PRL_FROM_ADDRESS = "txFromAddress";
	private static final String PARAM_PRL_STATUS = "selStatus";
	private static final String PARAM_CURRENT_PRL_ID = "hdnPRLID";
	private static final String PARAM_MODE = "hdnMode";
	private static final String PARAM_UI_PRL_CONTENT = "txtaRulesCmnts";
	private static final SimpleDateFormat outputDateFormat = new SimpleDateFormat(CalendarUtil.PATTERN8);

	/**
	 * Sucess Values: 0 - Saved Successfuly 1 - Processed Successfully 2 - Delete Successfully 4 - Parse Successfully -1
	 * - Error occurred - No releavant field identified -2 - Error occurred - Downloaded Date -3 - Error occurred -
	 * Downloaded Time -4 - Error occurred - Flight Number -5 - Error occurred - Flight Date -6 - Error occurred -
	 * Flight Time -7 - Error occurred - Airport -8 - Error occurred - Sita Address
	 */
	private static void setIntSuccess(HttpServletRequest request, int value) {
		setAttribInRequest(request, "intSuccess", new Integer(value));
	}

	private static int getIntSuccess(HttpServletRequest request) {
		Integer returnValue = (Integer) getAttribInRequest(request, "intSuccess");
		if (returnValue != null)
			return returnValue.intValue();
		else
			return -1;
	}

	private static void setExceptionOccured(HttpServletRequest request, boolean value) {
		setAttribInRequest(request, "isExceptionOccured", new Boolean(value));
	}

	private static boolean isExceptionOccured(HttpServletRequest request) {
		return ((Boolean) getAttribInRequest(request, "isExceptionOccured")).booleanValue();
	}

	/**
	 * Main Execute Method For PRL Processing & Sets the Success int 0-Not
	 * Applicable, 1-PRL process Success, 2-PRL save Success 3-Fail
	 *
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {

		String strHdnMode = request.getParameter(PARAM_MODE);
		String forward = WebConstants.FORWARD_SUCCESS;
		String strUIModeJS = "var isSearchMode = false;";
		setExceptionOccured(request, false);
		String strFormData = "var saveSuccess = " + getIntSuccess(request) + ";";
		strFormData += " var arrFormData = new Array();";
		request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
		request.setAttribute(WebConstants.REQ_OPERATION_UI_MODE, strUIModeJS);

		try {
			if (WebConstants.ACTION_DELETE.equals(strHdnMode) || WebConstants.ACTION_SAVE.equals(strHdnMode)
					|| WebConstants.ACTION_PROCESS.equals(strHdnMode)) {
				processPRLData(request);
			}
			setDisplayData(request);
		} catch (Exception exception) {
			if (exception instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
			}
		}
		return forward;
	}

	/**
	 * Sets the Display Data for PRL Process Data sets the Success int for the
	 * form 0-Not Applicable, 1-PRL process Success, 2-PRL save Success 3-Fail
	 *
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	private static void setDisplayData(HttpServletRequest request) throws Exception {
		setPRLProcessingRowHtml(request);
		setAgentList(request);
		setAirportList(request);
		setClientErrors(request);
		String strFormData = "";
		if (!isExceptionOccured(request)) {
			strFormData = " var arrFormData = new Array();";
			strFormData += " var saveSuccess = " + getIntSuccess(request) + ";";
			request.setAttribute(WebConstants.REQ_HTML_FORM_DATA, strFormData);
		}
	}

	/**
	 * Sets the Active Online Airport List
	 *
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setAirportList(HttpServletRequest request) {

		try {
			String strHtml = SelectListGenerator.createActiveAirportCodeList();
			request.setAttribute(WebConstants.REQ_HTML_AIRPORT_LIST, strHtml);
		} catch (ModuleException moduleException) {
			log.error(
					"SendPNLADLRequestHandler.setAirportList() method is failed :" + moduleException.getMessageString(),
					moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Sets the Agents List to the Request
	 *
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setAgentList(HttpServletRequest request) {

		try {
			String strHtml = SelectListGenerator.createAgentCodes();
			request.setAttribute(WebConstants.REQ_HTML_AGENT_LIST, strHtml);
		} catch (ModuleException moduleException) {
			log.error(
					"PRLProcessingRequestHandler.setAgentList() method is failed :"
							+ moduleException.getMessageString(), moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Set the PRL Details Row to the Request
	 *
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	protected static void setPRLProcessingRowHtml(HttpServletRequest request) throws Exception {

		try {
			PRLProcessingHTMLGenerator htmlGen = new PRLProcessingHTMLGenerator();
			String strHtml = htmlGen.getPRLProcessingRowHtml(request);
			request.setAttribute(WebConstants.REQ_HTML_ROWS, strHtml);
			request.setAttribute(WebConstants.REQ_FORM_FIELD_VALUES, htmlGen.getFormFieldValues());

		} catch (ModuleException moduleException) {
			log.error("PRLProcessingHandler.setPRLProcessingRowHtml() method is failed :"
					+ moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	private static void setClientErrors(HttpServletRequest request) {

		try {
			String strClientErrors = PRLProcessingHTMLGenerator.getClientErrors();
			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("PRLProcessingRequestHandler.setClientErrors() method is failed :" + moduleException.getMessageString(),
					moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		} catch (Exception e) {
			log.error("PRLProcessingRequestHandler.setClientErrors() method is failed :" + e.getMessage(), e);
			saveMessage(request, e.getMessage(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Process PRL Data
	 *
	 * @param request
	 */
	private static void processPRLData(HttpServletRequest request) throws Exception {

		Date flightDate = null;

		String strPrlContent = request.getParameter(PARAM_UI_PRL_CONTENT);
		String strHdnMode = request.getParameter(PARAM_MODE);
		String strCurrentPrlId = request.getParameter(PARAM_CURRENT_PRL_ID);
		String strVersion = (request.getParameter(PARAM_VERSION) == null || request.getParameter(PARAM_VERSION).trim()
				.equals("")) ? "-1" : request.getParameter(PARAM_VERSION);
		String strFlightNo = AiradminUtils.getNotNullString(request.getParameter(PARAM_PRL_FLIGHT_NO)).trim()
				.toUpperCase();
		String strFromAddress = request.getParameter(PARAM_PRL_FROM_ADDRESS);
		String strFromAirPort = request.getParameter(PARAM_PRL_FROM_AIRPORT);
		String strStatus = request.getParameter(PARAM_PRL_STATUS);
		String strPaxCount = request.getParameter(PARAM_PAXCOUNT);
		String strFlightDate = AiradminUtils.getNotNullString(request.getParameter(PARAM_PRL_FLIGHT_DATE));
		String strFlightTime = AiradminUtils.getNotNullString(request.getParameter(PARAM_PRL_FLIGHT_TIME)).trim();
		String strDownLoadTime = AiradminUtils.getNotNullString(request.getParameter(PARAM_PRL_DOWNLOAD_TIME)).trim();
		String strDownLoadTimeStamp = AiradminUtils.getNotNullString(request.getParameter(PARAM_PRL_DOWNLOAD_TS))
				.trim();
		Date downLoadTimeStamp = strDownLoadTimeStamp.equals("") ?
				null :
				outputDateFormat
						.parse(strDownLoadTimeStamp + (strDownLoadTime.equals("") ? "" : " " + strDownLoadTime));

		if (!StringUtil.isNullOrEmpty(strFlightDate)) {
			flightDate = outputDateFormat
					.parse(strFlightDate + (strFlightTime.equals("") ? " 00:00" : " " + strFlightTime));
		}

		if (WebConstants.ACTION_DELETE.equals(strHdnMode)) {
			setIntSuccess(request, 2);
			PRL prl = new PRL();
			prl.setPrlId(Integer.valueOf(strCurrentPrlId));
			prl.setDateDownloaded(downLoadTimeStamp);
			prl.setDepartureDate(flightDate);
			prl.setFlightNumber(strFlightNo.equals("") ? null : strFlightNo.toUpperCase());
			prl.setFromAddress(strFromAddress.equals("") ? null : strFromAddress);
			prl.setFromAirport(strFromAirPort.equals("-1") ? null : strFromAirPort);
			prl.setNumberOfPassengers(Integer.valueOf(strPaxCount));
			prl.setProcessedStatus(strStatus);
			prl.setPrlContent(strPrlContent);
			prl.setVersion(Long.parseLong(strVersion));
			ModuleServiceLocator.getPRLBD().deletePrlEntry(prl);
			saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_DELETE_SUCCESS), WebConstants.MSG_SUCCESS);
		} else if (WebConstants.ACTION_PROCESS.equals(strHdnMode)) {
			ServiceResponce serviceResponce = null;
			if (!StringUtil.isNullOrEmpty(strCurrentPrlId)) {
				serviceResponce = ModuleServiceLocator.getPRLBD()
						.reconcilePRLReservations(Integer.valueOf(strCurrentPrlId).intValue());
			}

			if (serviceResponce != null && serviceResponce.isSuccess()) {
				setIntSuccess(request, 1);
				saveMessage(request, airadminConfig.getMessage(WebConstants.KEY_PRL_PROCESS_SUCCESS),
						WebConstants.MSG_SUCCESS);
				setExceptionOccured(request, false);
				log.debug("PRLProcessingRequestHandler.processPRLData() method is successfully executed.");
			}
		}
	}
}
