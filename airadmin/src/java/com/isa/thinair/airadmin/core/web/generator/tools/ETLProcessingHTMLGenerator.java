/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */

/**
 * 
 *
 * 
 * Window - Preferences - Java - Code Style - Code Templates
 */

package com.isa.thinair.airadmin.core.web.generator.tools;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.messagepasser.api.model.ETL;
import com.isa.thinair.messagepasser.api.utils.ParserConstants;

public class ETLProcessingHTMLGenerator {

	// searching parameters
	private static final String PARAM_SEARCH_AIRPORT = "selAirport";

	private static final String PARAM_SEARCH_FROM = "txtFrom";

	private static final String PARAM_SEARCH_TO = "txtTo";

	private static final String PARAM_FLIGHT_FROM = "txtFlightFrom";

	private static final String PARAM_FLIGHT_TO = "txtFlightTo";

	private static final String PARAM_FLIGHT_NO = "txtSFlightNo";

	private static final String PARAM_START_REC_NO = "hdnRecNo";

	private static final String PARAM_SEARCH_PROCESS_STATUS = "selProcessType";

	private static final String PARAM_MODE = "hdnMode";

	private String strFormFieldsVariablesJS = "";

	private static final String DATE_FORMAT_DDMMYYYYHHmm = "dd/MM/yyyy HH:mm";

	private static final String DATE_FORMAT_DDMMYYYY = "dd/MM/yyyy";

	SimpleDateFormat outputDateFormat;
	SimpleDateFormat outputDateFormat_onlyDate;

	private static String clientErrors;

	@SuppressWarnings("unchecked")
	public final String getETLProcessingRowHtml(HttpServletRequest request) throws ModuleException, Exception {

		String strMode = request.getParameter(PARAM_MODE);
		String strStartRecNo = (request.getParameter(PARAM_START_REC_NO) == null || request.getParameter(PARAM_START_REC_NO)
				.trim().equals("")) ? "1" : request.getParameter(PARAM_START_REC_NO);
		String strUIModeJS = "var isSearchMode = false; var strETLContent;";
		String strAirportCode = "";
		String strFromDate = "";
		String strToDate = "";
		String strProcessStatus = "";
		String strFlightFromDate = "";
		String strFlightToDate = "";
		String strFlightNo = "";
		Date fromDate = null;
		Date toDate = null;
		Date flightFromDate = null;
		Date flightToDate = null;

		Page page = null;

		outputDateFormat = new SimpleDateFormat(DATE_FORMAT_DDMMYYYYHHmm);
		outputDateFormat_onlyDate = new SimpleDateFormat(DATE_FORMAT_DDMMYYYY);

		if (strMode != null) {
			strUIModeJS = "var isSearchMode = true; var strETLContent;";

			strAirportCode = AiradminUtils.getNotNullString(request.getParameter(PARAM_SEARCH_AIRPORT));
			strFormFieldsVariablesJS = "var airportCode ='" + strAirportCode + "';";

			strFromDate = AiradminUtils.getNotNullString(request.getParameter(PARAM_SEARCH_FROM));
			strFormFieldsVariablesJS += "var fromDate ='" + strFromDate + "';";

			strToDate = AiradminUtils.getNotNullString(request.getParameter(PARAM_SEARCH_TO));
			strFormFieldsVariablesJS += "var toDate ='" + strToDate + "';";

			strProcessStatus = (request.getParameter(PARAM_SEARCH_PROCESS_STATUS) == null || request
					.getParameter(PARAM_SEARCH_PROCESS_STATUS).trim().toUpperCase().equals("ALL")) ? "" : request.getParameter(
					PARAM_SEARCH_PROCESS_STATUS).trim();
			strFormFieldsVariablesJS += "var processStatus ='" + strProcessStatus + "';";

			strFlightFromDate = AiradminUtils.getNotNullString(request.getParameter(PARAM_FLIGHT_FROM));

			strFlightToDate = AiradminUtils.getNotNullString(request.getParameter(PARAM_FLIGHT_TO));

			strFlightNo = AiradminUtils.getNotNullString(request.getParameter(PARAM_FLIGHT_NO));

			SimpleDateFormat dateFormat = null;

			if (!strFromDate.equals("")) {
				if (strFromDate.indexOf('-') != -1) {
					dateFormat = new SimpleDateFormat("dd-MM-yy");
				}
				if (strFromDate.indexOf('/') != -1) {
					dateFormat = new SimpleDateFormat("dd/MM/yy");
				}
				if (strFromDate.indexOf(' ') != -1) {
					strFromDate = strFromDate.substring(0, strFromDate.indexOf(' '));
				}

				fromDate = dateFormat.parse(strFromDate);
			} else {
				fromDate = null;
			}

			if (!strToDate.equals("")) {
				if (strToDate.indexOf('-') != -1) {
					dateFormat = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
				}
				if (strToDate.indexOf('/') != -1) {
					dateFormat = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
				}
				if (strToDate.indexOf(' ') != -1) {
					strToDate = strToDate.substring(0, strToDate.indexOf(' '));
				}

				toDate = dateFormat.parse(strToDate + " 23:59:59");
			} else {
				toDate = null;
			}

			if (!strFlightFromDate.equals("")) {
				if (strFlightFromDate.indexOf('-') != -1) {
					dateFormat = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
				}
				if (strFlightFromDate.indexOf('/') != -1) {
					dateFormat = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
				}
				if (strFlightFromDate.indexOf(' ') != -1) {
					strFlightFromDate = strToDate.substring(0, strFlightFromDate.indexOf(' '));
				}

				flightFromDate = dateFormat.parse(strFlightFromDate + " 00:00:00");
			} else {
				flightFromDate = null;
			}

			if (!strFlightToDate.equals("")) {
				if (strFlightToDate.indexOf('-') != -1) {
					dateFormat = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
				}
				if (strFlightToDate.indexOf('/') != -1) {
					dateFormat = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
				}
				if (strFlightToDate.indexOf(' ') != -1) {
					strFlightToDate = strToDate.substring(0, strFlightToDate.indexOf(' '));
				}

				flightToDate = dateFormat.parse(strFlightToDate + " 23:59:59");
			} else {
				flightToDate = null;
			}

			request.setAttribute(WebConstants.REQ_OPERATION_UI_MODE, strUIModeJS);

			// Make Criterians
			List<ModuleCriterion> critrian = new ArrayList<ModuleCriterion>();
			List<String> orderList = new ArrayList<String>();
			orderList.add("dateDownloaded");

			if (fromDate != null) {
				// Set criterian - fromDate
				ModuleCriterion MCname = new ModuleCriterion();
				MCname.setCondition(ModuleCriterion.CONDITION_GREATER_THAN_OR_EQUALS);
				MCname.setFieldName("dateDownloaded");
				List<Date> value = new ArrayList<Date>();
				value.add(fromDate);
				MCname.setValue(value);
				critrian.add(MCname);
			}

			if (toDate != null) {
				// Set criterian - toDate
				ModuleCriterion MCname = new ModuleCriterion();
				MCname.setCondition(ModuleCriterion.CONDITION_LESS_THAN_OR_EQUALS);
				MCname.setFieldName("dateDownloaded");
				List<Date> value = new ArrayList<Date>();
				value.add(toDate);
				MCname.setValue(value);
				critrian.add(MCname);
			}

			if (!strAirportCode.equals("")) {
				// Set criterian - strAirportCode
				ModuleCriterion MCname = new ModuleCriterion();
				MCname.setCondition(ModuleCriterion.CONDITION_EQUALS);
				MCname.setFieldName("fromAirport");
				List<String> value = new ArrayList<String>();
				value.add(strAirportCode);
				MCname.setValue(value);
				critrian.add(MCname);
			}

			if (!strProcessStatus.equals("")) {
				// Set criterian - strProcessStatus
				ModuleCriterion MCname = new ModuleCriterion();
				MCname.setCondition(ModuleCriterion.CONDITION_EQUALS);
				MCname.setFieldName("processedStatus");
				List<String> value = new ArrayList<String>();
				value.add(strProcessStatus);
				MCname.setValue(value);
				critrian.add(MCname);
			}

			if (!strFlightNo.equals("")) {
				ModuleCriterion MCname = new ModuleCriterion();
				MCname.setCondition(ModuleCriterion.CONDITION_EQUALS);
				MCname.setFieldName("flightNumber");
				List<String> value = new ArrayList<String>();
				value.add(strFlightNo);
				MCname.setValue(value);
				critrian.add(MCname);
			}

			if (flightFromDate != null) {
				ModuleCriterion MCname = new ModuleCriterion();
				MCname.setCondition(ModuleCriterion.CONDITION_GREATER_THAN_OR_EQUALS);
				MCname.setFieldName("departureDate");
				List<Date> value = new ArrayList<Date>();
				value.add(flightFromDate);
				MCname.setValue(value);
				critrian.add(MCname);
			}

			if (flightToDate != null) {
				ModuleCriterion MCname = new ModuleCriterion();
				MCname.setCondition(ModuleCriterion.CONDITION_LESS_THAN_OR_EQUALS);
				MCname.setFieldName("departureDate");
				List<Date> value = new ArrayList<Date>();
				value.add(flightToDate);
				MCname.setValue(value);
				critrian.add(MCname);
			}

			page = ModuleServiceLocator.getEtlBD().getPagedETLData(critrian, Integer.valueOf(strStartRecNo).intValue() - 1, 20,
					orderList);

			if (page != null) {
				request.getSession().setAttribute("ETLDataSet", page.getPageData());
			} else {
				request.getSession().setAttribute("ETLDataSet", new ArrayList<ETL>());
			}

			strFormFieldsVariablesJS += " totalRes  ='" + page.getTotalNoOfRecords() + "';";
		}

		setFormFieldValues(strFormFieldsVariablesJS);

		if (page != null) {
			return createETLProcessingRowHTML(page.getPageData());
		} else {
			return createETLProcessingRowHTML(new ArrayList<ETL>());
		}

	}

	private String createETLProcessingRowHTML(Collection<ETL> listETLProcessing) throws ModuleException {

		List<ETL> list = (List<ETL>) listETLProcessing;

		Object[] listArr = list.toArray();
		StringBuffer sb = new StringBuffer("var arrData = new Array();");

		ETL etl = null;
		String tempDesc = "";
		for (int i = 0; i < listArr.length; i++) {
			etl = (ETL) listArr[i];
			sb.append("arrData[" + i + "] = new Array();");

			sb.append("arrData[" + i + "][0] = '" + outputDateFormat.format(etl.getDateDownloaded()) + "';");

			if (etl.getFlightNumber() != null) {
				sb.append("arrData[" + i + "][1] = '" + etl.getFlightNumber() + "';");
			} else {
				sb.append("arrData[" + i + "][1] = '';");
			}

			if (etl.getDepartureDate() != null) {
				sb.append("arrData[" + i + "][2] = '" + outputDateFormat.format(etl.getDepartureDate()) + "';");
			} else {
				sb.append("arrData[" + i + "][2] = '';");
			}

			sb.append("arrData[" + i + "][3] = '" + etl.getEtlId() + "';");

			tempDesc = "";

			if (etl.getProcessedStatus().equals(ParserConstants.ETLProcessStatus.NOT_PROCESSED))
				tempDesc = "Parsed";
			else if (etl.getProcessedStatus().equals(ParserConstants.ETLProcessStatus.ERROR_OCCURED))
				tempDesc = "ProcessedWithErrors";
			else if (etl.getProcessedStatus().equals(ParserConstants.ETLProcessStatus.PROCESSED))
				tempDesc = "Processed";
			else
				tempDesc = "Unknown";

			sb.append("arrData[" + i + "][4] = '" + tempDesc + "';");

			if (etl.getEtlContent() != null) {
				sb.append("arrData[" + i + "][5] = '" + StringUtils.replace(etl.getEtlContent(), "\n", "<\\BR>") + "';");

			} else {
				sb.append("arrData[" + i + "][5] = '';");
			}
			if (etl.getProcessedStatus() == ParserConstants.ETLProcessStatus.NOT_PROCESSED) {
				sb.append("arrData[" + i + "][6] = '" + ParserConstants.ETLProcessStatus.ERROR_OCCURED + "';");
			} else {
				sb.append("arrData[" + i + "][6] = '" + etl.getProcessedStatus() + "';");
			}

			sb.append("arrData[" + i + "][7] = new Array();");

			sb.append("arrData[" + i + "][8] = '';");

			if (etl.getFlightNumber() != null) {
				sb.append("arrData[" + i + "][9] = '" + etl.getFlightNumber() + "';");
			} else {
				sb.append("arrData[" + i + "][9] = '';");
			}
			if (etl.getDepartureDate() != null) {
				if (etl.getProcessedStatus() == ParserConstants.ETLProcessStatus.ERROR_OCCURED) {
					sb.append("arrData[" + i + "][10] = '" + outputDateFormat_onlyDate.format(etl.getDepartureDate()) + "';");
				} else {
					sb.append("arrData[" + i + "][10] = '" + outputDateFormat.format(etl.getDepartureDate()) + "';");
				}
			} else {
				sb.append("arrData[" + i + "][10] = '';");
			}
			if (etl.getFromAirport() != null) {
				sb.append("arrData[" + i + "][11] = '" + etl.getFromAirport() + "';");
			} else {
				sb.append("arrData[" + i + "][11] = '';");
			}
			if (etl.getFromAddress() != null) {
				sb.append("arrData[" + i + "][12] = '" + etl.getFromAddress() + "';");
			} else {
				sb.append("arrData[" + i + "][12] = '';");
			}

			sb.append("arrData[" + i + "][13] = " + etl.getVersion() + ";");

			sb.append("arrData[" + i + "][14] = " + ModuleServiceLocator.getEtlBD().getEtlParseEntryCount(etl.getEtlId(), null)
					+ ";");

		}

		return sb.toString();
	}

	public static String getClientErrors() throws Exception {
		if (clientErrors == null) {
			Properties props = new Properties();
			props.setProperty("um.process.form.downloadTS.required", "downloadTSRqrd");
			props.setProperty("um.process.form.downloadTime.required", "downloadTimeRqrd");
			props.setProperty("um.process.form.flightDate.required", "flightDateReq");
			props.setProperty("um.process.form.flightTime.required", "flightTimeReq");
			props.setProperty("um.process.form.flightNo.required", "flightNoReq");
			props.setProperty("um.process.form.airport.required", "airportReq");
			props.setProperty("um.process.form.pfs.no.flight", "noFlights");
			props.setProperty("um.process.form.downloadTS.lessthanFlightTs", "DownloadTSGreaterFlightTs");
			props.setProperty("um.process.form.downloadTS.lessthanToday", "DownloadTSlessThanToday");
			props.setProperty("um.process.form.pfs.flightTS.lessthanToday", "FlightTSGreaterFlightTs");
			props.setProperty("um.etlprocess.form.flighttodate.lessthan.flightfromdate", "flightFromDate");
			props.setProperty("um.etlprocess.form.flightfromdate.lessthan.currentdate", "DownloadTSGreaterFlightTs");
			props.setProperty("um.etlprocess.form.flighttodate.lessthan.currentdate", "DownloadTSlessThanToday");
			props.setProperty("um.etlprocess.form.fromdate.lessthan.currentdate", "fromDateLessthanCurrentDate");
			props.setProperty("um.etlprocess.form.todate.lessthan.currentdate", "toDateLessthanToday");
			props.setProperty("um.etlprocess.form.flighttodate.lessthan.flightfromdate", "todateLessthanFlightFromDate");
			props.setProperty("um.etlprocess.form.flightfromdate.lessthan.currentdate", "flightFromDateLessthanCurrentDate");
			props.setProperty("um.etlprocess.form.flighttodate.lessthan.currentdate", "flightToDateLessthanToday");
			clientErrors = JavascriptGenerator.createClientErrors(props);
		}
		return clientErrors;
	}

	private void setFormFieldValues(String strFormFieldsVariablesJS) {
		this.strFormFieldsVariablesJS = strFormFieldsVariablesJS;
	}

	public String getFormFieldValues() {
		return strFormFieldsVariablesJS;
	}

}
