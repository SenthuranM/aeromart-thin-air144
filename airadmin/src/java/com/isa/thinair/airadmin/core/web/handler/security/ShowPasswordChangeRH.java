//$Id$
package com.isa.thinair.airadmin.core.web.handler.security;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.security.PasswordChangeHG;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.commons.api.exception.ModuleException;

public class ShowPasswordChangeRH extends BasicRequestHandler {

	/**
	 * Execute method for display Page Action
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {
		String forward = AdminStrutsConstants.AdminAction.PASS_CHANGE;

		try {
			setDisplayData(request);
		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
		}

		return forward;
	}

	/**
	 * Sets the Display data for PasswordChange Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static void setDisplayData(HttpServletRequest request) throws ModuleException {
		setClientErrors(request);
		setChangePasswordHtml(request);
		setRequestHtml(request);
		setReseted(request);
	}

	/**
	 * Sets the Logged in User Id to the Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	protected static void setChangePasswordHtml(HttpServletRequest request) throws ModuleException {
		Principal principal = request.getUserPrincipal();
		String strHtml = "var loggedInUserId = '" + principal.getName() + "';";
		request.setAttribute(WebConstants.REQ_HTML_LOGIN_UID, strHtml);
	}

	/**
	 * Sets the Client validations for Password change Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static void setClientErrors(HttpServletRequest request) throws ModuleException {
		String strClientErrors = PasswordChangeHG.getClientErrors(request);
		if (strClientErrors == null) {
			strClientErrors = "";
		}
		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
	}

	/**
	 * Sets the Request Data secure URL Protocol status Remote user to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 */
	private static void setRequestHtml(HttpServletRequest request) {
		StringBuffer sb = new StringBuffer();

		sb.append("request['contextPath']='" + BasicRequestHandler.getContextPath(request) + "';");
		sb.append("request['secureUrl']='" + BasicRequestHandler.getSecureUrl(request) + "';");
		sb.append("request['userId']='" + request.getRemoteUser() + "';");
		sb.append("request['protocolStatus']='" + BasicRequestHandler.isProtocolStatusEqual() + "';");
		sb.append("request['useSecureLogin']=" +  AiradminModuleUtils.getConfig().isUseSecureLogin() + ";");
		
		request.setAttribute(WebConstants.REQ_HTML_REQUEST, sb.toString());
	}

	/**
	 * Set request to display screen message to reset password
	 * 
	 * @param request
	 */
	private static void setReseted(HttpServletRequest request) {
		String reset = "";
		reset = request.getParameter("reset");
		if (reset != null && reset.equals("true")) {
			request.setAttribute(WebConstants.REQ_HTML_SHOW_RESET_MSG, "true");
		} else {
			request.setAttribute(WebConstants.REQ_HTML_SHOW_RESET_MSG, "false");
		}
	}

}
