package com.isa.thinair.airadmin.core.web.handler.flightsched;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;

/**
 * To facilitate flight status modification options
 * 
 * @author dumindag
 * 
 */
public final class ManageFlightsRH extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(ManageFlightsRH.class);

	/**
	 * Main Execute Method for Flight Segment Actions
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Forward Action
	 */
	public static String execute(HttpServletRequest request) {
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;
		try {

			if (request.getParameter("hdnMode") != null && request.getParameter("hdnMode").equals("SAVEMANAGE")) {
				// do nothing for now. TODO
			}

			setDisplayData(request);
		} catch (Exception e) {
			log.error("Exception in FlightSegmentValidationRequestHandler.execute", e);
			if (e instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, e.getMessage(), "", "");
			} else {
				saveMessage(request, e.getMessage(), WebConstants.MSG_ERROR);
			}
		}
		return forward;
	}

	/**
	 * Sets the Display data for Flight Segment Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws Exception
	 *             the Exception
	 */
	public static void setDisplayData(HttpServletRequest request) throws Exception {
		setMsgScreen(request);
	}

	private static void setMsgScreen(HttpServletRequest request) throws Exception {
		// _X $ X $ X _
		String strFlightInfo = request.getParameter("strManageFltIds");
		String strSChedID = request.getParameter("strSchedId");

		String[] arrFltInfo = strFlightInfo.split("_");
		int actCount = 0;
		int inaCount = 0;
		int cnxCount = 0;
		int totalActCount = 0;
		int totalInaCount = 0;
		int totalCNXCount = 0;
		StringBuilder strCnxFlightInfo = new StringBuilder();
		boolean isAllowToActivatePastFlights = false;
		boolean isUpdatedAcivationFlag = false;
		for (int a = 0; a < arrFltInfo.length; a++) {
			String tmpSplit = arrFltInfo[a].trim();
			String[] arrIndFltInfo = tmpSplit.split("\\|");
			// int fltId = Integer.parseInt(arrIndFltInfo[0]); // id
			String strStatus = arrIndFltInfo[1]; // status
			String selected = arrIndFltInfo[2]; // selected T F
			boolean isPassedDepartureTime = Boolean.valueOf(arrIndFltInfo[4]);
			if (selected.equals("T")) {
				if (!isUpdatedAcivationFlag) {
					isAllowToActivatePastFlights = Boolean.valueOf(arrIndFltInfo[5]);
					isUpdatedAcivationFlag = true;
				} else {
					if (!Boolean.valueOf(arrIndFltInfo[5])) {
						isAllowToActivatePastFlights = Boolean.valueOf(arrIndFltInfo[5]);
					}
				}

			}
			if (strStatus.equalsIgnoreCase("ACT") && selected.equals("T") && !isPassedDepartureTime) {
				actCount++;
			} else if (strStatus.equalsIgnoreCase("CLS") && selected.equals("T") && !isPassedDepartureTime) {
				inaCount++;
			} else if (strStatus.equalsIgnoreCase("CNX") && selected.equals("T") && !isPassedDepartureTime) {
				strCnxFlightInfo.append(arrIndFltInfo[1]);
				cnxCount++;
			}

		}

		boolean isBelongsToSched = false;
		if (strSChedID != null && !strSChedID.equals("")) {
			Integer intSchedID = Integer.parseInt(strSChedID);
			Integer flightsInSchedule[] = ModuleServiceLocator.getFlightServiceBD().getNumberOfFlightsInSchedule(intSchedID);
			totalActCount = flightsInSchedule[0];
			totalInaCount = flightsInSchedule[1];
			totalCNXCount = flightsInSchedule[2];
			request.setAttribute("reqBelongstoShed", true);
			isBelongsToSched = true;
		} else {
			request.setAttribute("reqBelongstoShed", false);
			isBelongsToSched = false;
		}

		request.setAttribute("reqActCounts", actCount);
		request.setAttribute("reqInaCounts", inaCount);
		request.setAttribute("reqCnxCounts", cnxCount);
		// request.setAttribute("reqFltSelectInfo", strFlightCounts.toString());
		request.setAttribute("reqCanSkip", (actCount > 0 || inaCount > 0) ? true : false);
		request.setAttribute("reqCanCls", actCount > 0 ? true : false);
		request.setAttribute("reqCanAct", inaCount > 0 ? true : false);
		request.setAttribute("reqCanActCnx", cnxCount > 0 ? true : false);
		request.setAttribute("reqTotalActCounts", totalActCount);
		request.setAttribute("reqTotalInaCounts", totalInaCount);
		request.setAttribute("reqTotalCnxCounts", totalCNXCount);

		request.setAttribute("reqCanClsScd", (actCount > 0 && isBelongsToSched) ? true : false);
		request.setAttribute("reqCanActScd", (inaCount > 0 && isBelongsToSched) ? true : false);
		request.setAttribute("reqCanActCnxScd", (cnxCount > 0 && isBelongsToSched) ? true : false);
		request.setAttribute("reqSchdId", strSChedID);
		request.setAttribute("reqNoOptions", (actCount == 0 && inaCount == 0 && cnxCount == 0 ? true : false));
		request.setAttribute("reqUserMsg", "Selected flights cannot be modified");
		if (actCount == 0 && inaCount == 0 && cnxCount > 0 && !hasPrivilege(request, "plan.flight.reactivate")) {
			request.setAttribute("reqNoOptions", true);
			request.setAttribute("reqUserMsg", "Insufficient privileges");
		}
		if (isAllowToActivatePastFlights && hasPrivilege(request, "plan.flight.flt.activate.flown") && actCount == 0
				&& inaCount == 0 && cnxCount == 0) {
			request.setAttribute("reqNoOptions", false);
			request.setAttribute("reqOpnClsFlt", true);
		}
	}

}
