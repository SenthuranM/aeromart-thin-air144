package com.isa.thinair.airadmin.core.web.v2.action.tools;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.commons.api.dto.ModuleCriteria;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.invoicing.api.dto.BSPReportDTO;
import com.isa.thinair.invoicing.api.model.BSPFailedTnx;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ShowBSPReportAction extends BaseRequestAwareAction {
	private static final String PARAM_MODE = "hdnMode";
	private static final String PARAM_TXNID = "bspTxnLogID";
	private static final String PARAM_SELBSPEDITSTATUS_VALUE = "hdnSelBSPEditStatus";

	private Object[] rows;

	private int page;

	private int total;

	private int records;

	private String hdnMode;

	private String txtMsg = "";

	private boolean success;

	private BSPReportDTO bspReportSrch;

	private static Log log = LogFactory.getLog(ShowBSPReportAction.class);

	public String execute() {
		String strHdnMode = request.getParameter(PARAM_MODE);
		String forward = WebConstants.FORWARD_SUCCESS;

		try {
			if (strHdnMode.equals(WebConstants.ACTION_UPDATE)) {
				// Save the updated status field of the BSP Report.
				saveData(request);
				success = true;
			}
		} catch (Exception exception) {
			success = false;
			txtMsg = "Updating BSP Report Status Failed. Please try again later... ";
			log.error("Exception in BSPReportRequestHandler:execute()", exception);
			if (exception instanceof RuntimeException) {
				forward = WebConstants.FORWARD_ERROR;
				JavascriptGenerator.setServerError(request, exception.getMessage(), "", "");
			}
		}

		return forward;
	}

	private static void saveData(HttpServletRequest request) throws ModuleException {
		String tnxID = request.getParameter(PARAM_TXNID);
		String BSPReportStatus = request.getParameter(PARAM_SELBSPEDITSTATUS_VALUE);

		BSPFailedTnx savingBspFailedTnx = (BSPFailedTnx) ModuleServiceLocator.getInvoicingBD().getChangingBSPFailedTnx(tnxID);
		savingBspFailedTnx.setStatus(BSPReportStatus);
		ModuleServiceLocator.getInvoicingBD().saveOrUpdateBSPFailedTransaction(savingBspFailedTnx);
	}

	public String searchBSPReport() {
		// BSP Report searching method.
		List<ModuleCriterion> critrian = new ArrayList<ModuleCriterion>();
		if (bspReportSrch != null) {
			if (bspReportSrch.getCountryName() != null && !bspReportSrch.getCountryName().trim().equals("")) {
				ModuleCriteria criteriaBSPCountryName = new ModuleCriteria();
				List<String> lstcountryName = new ArrayList<String>();
				criteriaBSPCountryName.setFieldName("countryCode");
				criteriaBSPCountryName.setCondition(ModuleCriteria.CONDITION_EQUALS);
				lstcountryName.add(bspReportSrch.getCountryName());
				criteriaBSPCountryName.setValue(lstcountryName);
				critrian.add(criteriaBSPCountryName);
			}
			if (bspReportSrch.getAgentCode() != null && !bspReportSrch.getAgentCode().trim().equals("")) {
				ModuleCriteria criteriaBSPAgentCode = new ModuleCriteria();
				List<String> lstagentCode = new ArrayList<String>();
				criteriaBSPAgentCode.setFieldName("agentCode");
				criteriaBSPAgentCode.setCondition(ModuleCriteria.CONDITION_EQUALS);
				lstagentCode.add(bspReportSrch.getAgentCode());
				criteriaBSPAgentCode.setValue(lstagentCode);
				critrian.add(criteriaBSPAgentCode);
			}
			if (bspReportSrch.getStatus() != null && !bspReportSrch.getStatus().trim().equals("")) {
				ModuleCriteria criteriaBSPStatus = new ModuleCriteria();
				List<String> lststatus = new ArrayList<String>();
				criteriaBSPStatus.setFieldName("status");
				criteriaBSPStatus.setCondition(ModuleCriteria.CONDITION_EQUALS);
				lststatus.add(bspReportSrch.getStatus());
				criteriaBSPStatus.setValue(lststatus);
				critrian.add(criteriaBSPStatus);
			}
			if (bspReportSrch.getFrmDate() != null && !bspReportSrch.getFrmDate().trim().equals("")) {
				ModuleCriteria criteriaBSPFrmDate = new ModuleCriteria();
				List<Date> lstfrmDate = new ArrayList<Date>();
				criteriaBSPFrmDate.setFieldName("creatingDate");
				criteriaBSPFrmDate.setCondition(ModuleCriteria.CONDITION_GREATER_THAN_OR_EQUALS);
				Date bspFromDate = stringToDate(bspReportSrch.getFrmDate());
				lstfrmDate.add(bspFromDate);
				criteriaBSPFrmDate.setValue(lstfrmDate);
				critrian.add(criteriaBSPFrmDate);
			}
			if (bspReportSrch.getToDate() != null && !bspReportSrch.getToDate().trim().equals("")) {
				ModuleCriteria criterianBSPToDate = new ModuleCriteria();
				List<Date> lsttoDate = new ArrayList<Date>();
				criterianBSPToDate.setFieldName("creatingDate");
				criterianBSPToDate.setCondition(ModuleCriteria.CONDITION_LESS_THAN);
				Date bsptoDate = stringToDate(bspReportSrch.getToDate());
				lsttoDate.add(bsptoDate);
				criterianBSPToDate.setValue(lsttoDate);
				critrian.add(criterianBSPToDate);
			}
		}

		Page pgBSPReport = searchBSPReport(this.page, critrian);
		this.records = pgBSPReport.getTotalNoOfRecords();
		this.total = pgBSPReport.getTotalNoOfRecords() / 20;
		int mod = pgBSPReport.getTotalNoOfRecords() % 20;
		if (mod > 0)
			this.total = this.total + 1;
		if (this.page <= 0)
			this.page = 1;

		Collection<BSPFailedTnx> colBSP = pgBSPReport.getPageData();

		if (colBSP != null) {
			Object[] dataRow = new Object[colBSP.size()];
			int index = 1;
			Iterator<BSPFailedTnx> iter = colBSP.iterator();
			while (iter.hasNext()) {
				Map<String, Object> counmap = new HashMap<String, Object>();
				BSPFailedTnx grdBSPReport = (BSPFailedTnx) iter.next();

				counmap.put("id", new Integer(((page - 1) * 20) + index).toString());
				counmap.put("bspreport", grdBSPReport);
				dataRow[index - 1] = counmap;
				index++;
			}

			this.rows = dataRow;
		}
		return S2Constants.Result.SUCCESS;
	}

	public static Page searchBSPReport(int pageNo, List<ModuleCriterion> criterion) {
		Page tmpPage = null;

		try {
			if (criterion == null) {
				tmpPage = ModuleServiceLocator.getInvoicingBD().getBSPReports((pageNo - 1) * 20, 20);

			} else {
				if (pageNo == 0) {
					pageNo = 1;
				}

				tmpPage = ModuleServiceLocator.getInvoicingBD().getBSPReports((pageNo - 1) * 20, 20, criterion);

			}
		} catch (ModuleException e) {
			log.error(e);
		}
		return tmpPage;
	}

	public Date stringToDate(String strDate) {
		Date date = null;
		if (strDate != null && !"".equals(strDate)) {
			String[] arrDate = strDate.split("/");
			Calendar cal = new GregorianCalendar();
			cal.set(new Integer(arrDate[2]), new Integer(arrDate[1]) - 1, new Integer(arrDate[0]));
			date = new Date(cal.getTimeInMillis());
		}
		return date;
	}

	public void setHdnMode(String hdnMode) {
		this.hdnMode = hdnMode;
	}

	public String getHdnMode() {
		return hdnMode;
	}

	public void setTxtMsg(String txtMsg) {
		this.txtMsg = txtMsg;
	}

	public String getTxtMsg() {
		return txtMsg;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public boolean isSuccess() {
		return success;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public Object[] getRows() {
		return rows;
	}

	public void setRows(Object[] rows) {
		this.rows = rows;
	}

	public BSPReportDTO getBspReportSrch() {
		return bspReportSrch;
	}

	public void setBspReportSrch(BSPReportDTO bspReportSrch) {
		this.bspReportSrch = bspReportSrch;
	}

}
