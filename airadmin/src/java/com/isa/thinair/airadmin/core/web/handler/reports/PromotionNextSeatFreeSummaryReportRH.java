package com.isa.thinair.airadmin.core.web.handler.reports;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;

public class PromotionNextSeatFreeSummaryReportRH extends BasicRequestHandler {

	private static Log log = LogFactory.getLog(PromotionNextSeatFreeSummaryReportRH.class);

	public static String execute(HttpServletRequest request, HttpServletResponse response) {
		String strHdnMode = request.getParameter("hdnMode");
		String forward = AdminStrutsConstants.AdminAction.SUCCESS;

		try {
			setDisplayData(request);
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				return null;
			}
		} catch (ModuleException e) {
			forward = AdminStrutsConstants.AdminAction.ERROR;
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("PromotionNextSeatFreeSummaryReportRH setReportView Failed " + e.getMessageString());
		} catch (Exception e) {
			forward = AdminStrutsConstants.AdminAction.ERROR;
			log.error("Error in PromotionNextSeatFreeSummaryReportRH execute()" + e.getMessage());
		}
		return forward;
	}

	/**
	 * Sets the initial data for the report.
	 * 
	 * @param request
	 * @throws Exception
	 */
	public static void setDisplayData(HttpServletRequest request) throws ModuleException {
		setClientErrors(request);
	}

	/**
	 * Sets client error messages
	 * 
	 * @param request
	 */
	public static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * Gets the report data and sets to the corresponding view option.
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {
		ReportsSearchCriteria search = null;
		ResultSet resultSet = null;

		String id = "UC_REPM_083";
		String strLogo = AppSysParamsUtil.getReportLogo(false);
		String image = "../../images/" +  AppSysParamsUtil.getReportLogo(true);
		String strCarrier = ModuleServiceLocator.getGlobalConfig().getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);
		String reportTemplate = "PromotionsNextSeatFreeReqSummary.jasper";

		search = new ReportsSearchCriteria();
		String fromDateDep = request.getParameter("txtFromDate");
		String toDateDep = request.getParameter("txtToDate");
		String value = request.getParameter("radReportOption");
		String flightNo = request.getParameter("txtFlightNo");

		if (strlive != null) {
			if (strlive.equals(WebConstants.REP_LIVE)) {
				search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
			} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
				search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
			}
		}

		if (isNotEmptyOrNull(fromDateDep) && isNotEmptyOrNull(toDateDep)) {
			String strFromDate = "'" + fromDateDep + " 00:00:00'," + "'DD/MM/YYYY HH24:mi:ss'";
			String strToDate = "'" + toDateDep + " 23:59:00'," + "'DD/MM/YYYY HH24:mi:ss'";
			search.setDateRangeFrom(strFromDate);
			search.setDateRangeTo(strToDate);
		}

		if (isNotEmptyOrNull(flightNo)) {
			search.setFlightNumber(flightNo);
		} else {
			search.setFlightNumber("");
		}

		ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
				ReportsHTMLGenerator.getReportTemplate(reportTemplate));
		resultSet = ModuleServiceLocator.getDataExtractionBD().getPromotionNextSeatFreeSummary(search);

		Map<String, Object> parameters = new HashMap<String, Object>();

		parameters.put("FROM_DATE", fromDateDep);
		parameters.put("TO_DATE", toDateDep);
		parameters.put("ID", id);
		parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);

		// To provide Report Format Options
		String reportNumFormat = request.getParameter("radRptNumFormat");
		ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

		if (value.trim().equals("HTML")) {
			parameters.put("IMG", image);
			ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null, null,
					response);
		} else if (value.trim().equals("PDF")) {
			image = ReportsHTMLGenerator.getReportTemplate(strLogo);
			parameters.put("IMG", image);
			response.addHeader("Content-Disposition", "attachment;filename=PromotionNextSeatFreeSummary.pdf");
			ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);
		} else if (value.trim().equals("EXCEL")) {
			response.addHeader("Content-Disposition", "attachment;filename=PromotionNextSeatFreeSummary.xls");
			ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);
		} else if (value.trim().equals("CSV")) {
			response.reset();
			response.addHeader("Content-Disposition", "attachment;filename=PromotionNextSeatFreeSummary.csv");
			ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);
		}
	}

	private static boolean isNotEmptyOrNull(String str) {
		return !((str == null) || str.trim().equals("") || str.trim().equals("-1"));
	}

}
