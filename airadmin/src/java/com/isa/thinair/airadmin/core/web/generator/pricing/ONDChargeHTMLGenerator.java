package com.isa.thinair.airadmin.core.web.generator.pricing;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airpricing.api.criteria.ONDChargeSearchCriteria;
import com.isa.thinair.airpricing.api.dto.ondcharge.OndChargeRateTO;
import com.isa.thinair.airpricing.api.dto.ondcharge.OndChargeTO;
import com.isa.thinair.airpricing.api.dto.ondcharge.OriginDestinationTO;
import com.isa.thinair.airpricing.api.service.ChargeBD;
import com.isa.thinair.airpricing.api.util.AirPricingCustomConstants;
import com.isa.thinair.commons.api.dto.ChargeApplicabilityTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.GlobalConfig;

public class ONDChargeHTMLGenerator {

	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();

	DecimalFormat formatter = new DecimalFormat("#.00");
	public static String jsString;

	/**
	 * Gets the OND Deatis as per the Search Critiriea
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Grid Array
	 * @throws Exception
	 *             the Exception
	 */
	@SuppressWarnings("unchecked")
	public final String getONDFareRowHtml(HttpServletRequest request) throws Exception {

		String origin = request.getParameter("hdnOrigin");
		String destination = request.getParameter("hdnDestination");

		String via1 = request.getParameter("hdnVia1");
		String via2 = request.getParameter("hdnVia2");
		String via3 = request.getParameter("hdnVia3");
		String via4 = request.getParameter("hdnVia4");
		String SOD1 = request.getParameter("radSOD1");
		String SF1 = request.getParameter("rasSF1");
		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");

		String pos = request.getParameter("selPOS");
		Collection<OriginDestinationTO> colFares = null;
		SimpleDateFormat dateForamt = new SimpleDateFormat("dd/MM/yyyy");

		int RecNo = 1;
		List<String> viaList = new ArrayList<String>();
		List<String> order = new ArrayList<String>();
		order.add("ond.ONDCode");

		if (request.getParameter("hdnChargeRecNo") == null || request.getParameter("hdnChargeRecNo").equals("")) {
			RecNo = 1;
		} else {
			RecNo = Integer.parseInt(request.getParameter("hdnChargeRecNo")) - 1;
		}

		Page page = null;
		String state = request.getParameter("hdnState");
		ChargeBD chargeBD = ModuleServiceLocator.getChargeBD();
		ONDChargeSearchCriteria ondSearch = new ONDChargeSearchCriteria();

		if (state != null) {
			if (origin != null && !origin.equals("") && !origin.equals("undefined") && !origin.equals("Select")) {

				if (origin != null && !origin.equals("") && !origin.equals("Select")) {
					ondSearch.setOriginAirportCode(origin);
				}

				if (destination != null && !destination.equals("") && !destination.equals("Select")) {
					ondSearch.setDestinationAirportCode(destination);
				}

				if (via1 != null && !via1.equals("") && !via1.equals("Select")) {
					viaList.add(via1);
				}
				if (via2 != null && !via2.equals("") && !via2.equals("Select")) {
					viaList.add(via2);
				}
				if (via3 != null && !via3.equals("") && !via3.equals("Select")) {
					viaList.add(via3);
				}
				if (via4 != null && !via4.equals("") && !via4.equals("Select")) {
					viaList.add(via4);
				}
				if ((via1 != null && !via1.equals("") && !via1.equals("Select"))
						|| (via2 != null && !via2.equals("") && !via2.equals("Select"))
						|| (via3 != null && !via3.equals("") && !via3.equals("Select"))
						|| (via4 != null && !via4.equals("") && !via4.equals("Select"))) {

					ondSearch.setViaAirportCodes(viaList);
				}

				if (SOD1 != null) {
					if (SOD1.equals("AllComb")) {
						ondSearch.setExactAirportsCombinationOnly(false);

					} else {
						ondSearch.setExactAirportsCombinationOnly(true);

					}
				} else {
					ondSearch.setExactAirportsCombinationOnly(false);

				}

				if (SF1 != null) {
					if (SF1.equals("From")) {
						if (fromDate != null && !fromDate.equals("")) {
							String dateArr[] = fromDate.split("/");
							GregorianCalendar cal = new GregorianCalendar(Integer.parseInt(dateArr[2]),
									Integer.parseInt(dateArr[1]) - 1, Integer.parseInt(dateArr[0]));
							ondSearch.setEffectiveFromDate(cal.getTime());

						} else {
							fromDate = dateForamt.format(new Date()).toString();
							String dateArr[] = fromDate.split("/");
							GregorianCalendar cal = new GregorianCalendar(Integer.parseInt(dateArr[2]),
									Integer.parseInt(dateArr[1]) - 1, Integer.parseInt(dateArr[0]));
							ondSearch.setEffectiveFromDate(cal.getTime());
						}

						if (toDate != null && !toDate.equals("")) {

							String dateArr[] = toDate.split("/");
							GregorianCalendar cal = new GregorianCalendar(Integer.parseInt(dateArr[2]),
									Integer.parseInt(dateArr[1]) - 1, Integer.parseInt(dateArr[0]));
							ondSearch.setEffectiveToDate(cal.getTime());

						} else {
							toDate = dateForamt.format(new Date()).toString();
							String dateArr[] = toDate.split("/");
							GregorianCalendar cal = new GregorianCalendar(Integer.parseInt(dateArr[2]),
									Integer.parseInt(dateArr[1]) - 1, Integer.parseInt(dateArr[0]));
							ondSearch.setEffectiveToDate(cal.getTime());
						}

					} else {

						fromDate = dateForamt.format(new Date()).toString();
						String dateArr[] = fromDate.split("/");
						GregorianCalendar cal = new GregorianCalendar(Integer.parseInt(dateArr[2]),
								Integer.parseInt(dateArr[1]) - 1, Integer.parseInt(dateArr[0]));
						ondSearch.setEffectiveFromDate(cal.getTime());
					}
				}

				if (pos != null && !pos.equals("Select")) {
					ondSearch.setPosStation(pos);

				} else {

					ondSearch.setPosStation(null);
				}

				page = chargeBD.getONDCharges(ondSearch, RecNo, 100, order);
				colFares = page.getPageData();

			}
		}
		return getONDFareReportHtml(colFares);

	}

	/**
	 * Creates the OND Fare Detail Grid from a Collection of Fares
	 * 
	 * @param colFares
	 *            the Collection of Fares
	 * @return String the Created Grid Array
	 * @throws Exception
	 *             the Exception
	 */
	@SuppressWarnings("unused")
	public final String getONDFareReportHtml(Collection<OriginDestinationTO> colFares) throws Exception {

		SimpleDateFormat dateForamt = new SimpleDateFormat("dd/MM/yyyy");
		Collection<OriginDestinationTO> ondFaresCollection = colFares;
		StringBuffer sbFares = new StringBuffer();
		sbFares.append("var arrFareChargeData = new Array();");
		int chargeRateCount = 0;
		int i = 0;

		if (ondFaresCollection != null) {
			Iterator<OriginDestinationTO> iteONDFares = ondFaresCollection.iterator();
			int ONDBaseCount = 0;
			int rowCount = 0, tempRowCount = 0;
			while (iteONDFares.hasNext()) {
				ONDBaseCount++;

				OriginDestinationTO originDestination = (OriginDestinationTO) iteONDFares.next();

				List<OndChargeTO> chargeSet = originDestination.getCharges();
				Iterator<OndChargeTO> iteCharges = chargeSet.iterator();

				OndChargeTO charge = null;
				sbFares.append("arrFareChargeData[" + i + "] = new  Array();");
				sbFares.append("arrFareChargeData[" + i + "][0] = '" + i + "';");
				String replaceStr = "";

				String airportArr[] = originDestination.getOndCode().split("/");
				String replacedArr[] = new String[9];
				for (int r = 0; r < airportArr.length; r++) {
					if (airportArr[r].equals("***")) {
						replacedArr[r] = "All";
					} else {
						replacedArr[r] = airportArr[r];
					}
				}

				for (int t = 0; t < replacedArr.length; t++) {
					if (replacedArr[t] != null) {
						replaceStr += replacedArr[t] + "/";
					}
				}

				if (replaceStr.length() > 12) {
					String ondP1 = replaceStr.substring(0, 11);
					String ondP2 = replaceStr.substring(12, replaceStr.length() - 1);
					String ondFull = ondP1 + "/ " + ondP2;
					sbFares.append("arrFareChargeData[" + i + "][1] = '" + ondFull + "';");
				} else {

					sbFares.append("arrFareChargeData[" + i + "][1] = '" + replaceStr.substring(0, replaceStr.length() - 1)
							+ "';");
				}

				int ondCount = i;
				int chargeCountIndex = 0;
				OndChargeTO prevCharge = null;
				while (iteCharges.hasNext()) {

					charge = (OndChargeTO) iteCharges.next();
					List<OndChargeRateTO> chargeRate = charge.getChargeRates();
					Iterator<OndChargeRateTO> iteRates = chargeRate.iterator();
					chargeRateCount += chargeRate.size();

					if (i != ondCount) {
						sbFares.append("arrFareChargeData[" + i + "] = new  Array();");
						sbFares.append("arrFareChargeData[" + i + "][0] = '" + i + "';");
						sbFares.append("arrFareChargeData[" + i + "][1] = '';");
					}

					if (chargeCountIndex >= 1 && chargeCountIndex < chargeSet.size()) {
						tempRowCount = chargeCountIndex;
						prevCharge = (OndChargeTO) chargeSet.get(tempRowCount - 1);

					}

					if (prevCharge != null) {
						if (prevCharge.getChargeGroupCode().equals(charge.getChargeGroupCode())) {
							sbFares.append("arrFareChargeData[" + i + "][2] = '';");

						} else {
							sbFares.append("arrFareChargeData[" + i + "][2] = '" + charge.getChargeGroupCode() + "';");
						}
					} else {
						sbFares.append("arrFareChargeData[" + i + "][2] = '" + charge.getChargeGroupCode() + "';");

					}

					if (prevCharge != null) {
						if (prevCharge.getChargeGroupCode().equals(charge.getChargeGroupCode())) {
							if (prevCharge.getChargeCategoryCode().equals(charge.getChargeCategoryCode())) {
								sbFares.append("arrFareChargeData[" + i + "][3] = '';");

							} else {
								sbFares.append("arrFareChargeData[" + i + "][3] = '" + charge.getChargeCategoryCode() + "';");
							}
						} else {
							sbFares.append("arrFareChargeData[" + i + "][3] = '" + charge.getChargeCategoryCode() + "';");

						}

					} else {
						sbFares.append("arrFareChargeData[" + i + "][3] = '" + charge.getChargeCategoryCode() + "';");

					}

					sbFares.append("arrFareChargeData[" + i + "][4] = '" + charge.getChargeCode() + "';");
					if (charge.getChargeDescription() == null) {
						sbFares.append("arrFareChargeData[" + i + "][5] = '';");

					} else {
						if (charge.getChargeDescription().length() > 15) {
							String desc1 = charge.getChargeDescription().substring(0, 14);
							String desc2 = charge.getChargeDescription().substring(15);
							String descFull = desc1 + " " + desc2;
							sbFares.append("arrFareChargeData[" + i + "][5] = '" + descFull + "';");
						} else {
							sbFares.append("arrFareChargeData[" + i + "][5] = '" + charge.getChargeDescription() + "';");
						}

					}

					if (charge.getRefundableChargeFlag() == OndChargeTO.REFUNDABLE_CHARGE) {
						sbFares.append("arrFareChargeData[" + i + "][6] = 'Y';");
					} else {
						sbFares.append("arrFareChargeData[" + i + "][6] = 'N';");
					}

					appendChargesApplicability(charge, i, sbFares);

					OndChargeRateTO rate = null;
					int chargeCount = i;
					while (iteRates.hasNext()) {

						if (i != chargeCount) {
							sbFares.append("arrFareChargeData[" + i + "] = new  Array();");
							sbFares.append("arrFareChargeData[" + i + "][0] = '" + i + "';");
							sbFares.append("arrFareChargeData[" + i + "][1] = '';");

							sbFares.append("arrFareChargeData[" + i + "][2] = '';");
							sbFares.append("arrFareChargeData[" + i + "][3] = '';");
							sbFares.append("arrFareChargeData[" + i + "][4] = '';");
							sbFares.append("arrFareChargeData[" + i + "][5] = '';");
							sbFares.append("arrFareChargeData[" + i + "][6] = '';");
							sbFares.append("arrFareChargeData[" + i + "][7] = '';");

						}
						rate = (OndChargeRateTO) iteRates.next();
						if (rate.getChargeEffectiveFromDate() != null) {
							sbFares.append("arrFareChargeData[" + i + "][8] = '"
									+ dateForamt.format(rate.getChargeEffectiveFromDate()) + "';");
						} else {
							sbFares.append("arrFareChargeData[" + i + "][8] = '';");
						}

						if (rate.getChargeEffectiveToDate() != null) {
							sbFares.append("arrFareChargeData[" + i + "][9] = '"
									+ dateForamt.format(rate.getChargeEffectiveToDate()) + "';");
						} else {
							sbFares.append("arrFareChargeData[" + i + "][9] = '';");
						}
						if (AirPricingCustomConstants.ChargeTypes.ABSOLUTE_VALUE.getChargeTypes().equals(
								rate.getValuePercentageFlag())) {
							sbFares.append("arrFareChargeData[" + i + "][10] = 'V';");
						} else if (AirPricingCustomConstants.ChargeTypes.PERCENTAGE_OF_FARE_PF.getChargeTypes().equals(
								rate.getValuePercentageFlag())) {
							sbFares.append("arrFareChargeData[" + i + "][10] = 'PF';");
						} else if (AirPricingCustomConstants.ChargeTypes.PERCENTAGE_OF_FARE_AND_SUR_PFS.getChargeTypes().equals(
								rate.getValuePercentageFlag())) {
							sbFares.append("arrFareChargeData[" + i + "][10] = 'PTF';");
						} else {
							sbFares.append("arrFareChargeData[" + i + "][10] = 'V';");
						}

						if (rate.getChargeValuePercentage() != null && !rate.getChargeValuePercentage().equals("")) {
							sbFares.append("arrFareChargeData[" + i + "][11] = '"
									+ formatter.format(rate.getChargeValuePercentage()) + "';");
						} else {
							sbFares.append("arrFareChargeData[" + i + "][11] = '';");
						}
						i++;

					}// While Rate

					rowCount++;
					chargeCountIndex++;
				}// While charge

			}// While OND

		}// While OND not null
		String strJavascriptTotalNoOfRecs = "totalChargeRecs = " + i + ";";
		jsString = strJavascriptTotalNoOfRecs;

		return sbFares.toString();
	}

	/**
	 * Appends the charges applicability
	 * 
	 * @param charge
	 * @param i
	 * @param sbFares
	 * @throws ModuleException
	 */
	private void appendChargesApplicability(OndChargeTO charge, int i, StringBuffer sbFares) throws ModuleException {
		Map<Integer, ChargeApplicabilityTO> chargeApplicabilitiesMap = globalConfig.getChargeApplicabilities();
		ChargeApplicabilityTO chargeApplicabilityTO = chargeApplicabilitiesMap.get(charge.getAppliesTo());

		if (chargeApplicabilityTO == null) {
			throw new ModuleException("platform.data.chargeapplicabilities.inconsistentrecords");
		}

		sbFares.append("arrFareChargeData[" + i + "][7] = '" + chargeApplicabilityTO.getAppliesToDesc() + "';");
	}

	/**
	 * Creates the Client validations for Fare Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array of Client Validations
	 * @throws ModuleException
	 *             the ModuleException
	 */
	protected static String getClientErrors(HttpServletRequest request) throws ModuleException {

		String clientErrors = null;
		Properties moduleErrs = new Properties();

		moduleErrs.setProperty("um.viewfare.origin.required", "originRqrd");
		moduleErrs.setProperty("um.viewfare.destination.required", "destinationRqrd");
		moduleErrs.setProperty("um.viewfare.carrier.required", "carrierRqrd");
		moduleErrs.setProperty("um.viewfare.sod.required", "sodRqrd");
		moduleErrs.setProperty("um.viewfare.fc1.required", "fc1Rqrd");
		moduleErrs.setProperty("um.viewfare.todate.required", "todateRqrd");
		moduleErrs.setProperty("um.viewfare.fromdate.required", "fromdateRqrd");
		moduleErrs.setProperty("um.viewfare.dates.notvalid", "datesInvalid");
		moduleErrs.setProperty("um.viewfare.origindest.cantbe.equal", "originDestEqual");
		moduleErrs.setProperty("um.viewfare.via.not.equal", "viaPointsEqual");

		clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		return clientErrors;
	}

}