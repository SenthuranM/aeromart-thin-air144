package com.isa.thinair.airadmin.core.web.action.reports;

import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.handler.reports.OriginCountryOfCallReportRH;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

@Results({
		@Result(name = AdminStrutsConstants.AdminAction.SUCCESS, value = AdminStrutsConstants.AdminJSP.REPORTING_ORIGIN_COUNTRY_OF_CALL),
		@Result(name = AdminStrutsConstants.AdminAction.ERROR, value = AdminStrutsConstants.AdminJSP.ERROR_JSP) }) public class ShowOriginCountryOfCallReportAction
		extends BaseRequestResponseAwareAction {
	public String execute() {
		return OriginCountryOfCallReportRH.execute(request, response);
	}

}


