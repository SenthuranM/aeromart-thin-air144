package com.isa.thinair.airadmin.core.web.v2.action.promotion;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airadmin.core.web.util.BundledFareDisplayUtil;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.commons.api.constants.PriviledgeConstants;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.promotion.api.to.bundledFare.BundledFareConfigurationTO;
import com.isa.thinair.promotion.api.to.bundledFare.BundledFarePeriodTO;
import com.isa.thinair.promotion.api.to.bundledFare.BundledFareSearchCriteriaTO;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.JSONFETOParser;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * 
 * @author rumesh
 * 
 */
@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ManageBundledFaresConfigurationAction extends BaseRequestAwareAction {

	private Log log = LogFactory.getLog(ManageBundledFaresConfigurationAction.class);
	private static AiradminConfig airadminConfig = new AiradminConfig();

	private final int PAGE_LENGTH = 10;
	private final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

	private int page;
	private int totalRecords;
	private int totalPages;

	private boolean success = true;
	private String messageTxt;

	private boolean isNewSearch;

	private BundledFareSearchCriteriaTO searchCriteria = new BundledFareSearchCriteriaTO();

	private BundledFareConfigurationTO bundledFareTO = new BundledFareConfigurationTO();

	private Collection<BundledFareConfigurationTO> bundledFareCriterias;

	private String bundledFare;
	private String bookingClasses;
	private String ondCodes;
	private String agents;
	private String flightNumbers;
	private String flightDateFrom;
	private String flightDateTo;
	private String serviceTemplates;
	private String applicableSalesChannels;
	private String uploadImage;
	private String uploadImageStripe;
	private String bundledFareNamesHtml;
	private String pointOfSales;

	public String search() {
		try {
			checkPrivileges(PriviledgeConstants.ALLOW_VIEW_BUNDLED_FARE_CONFIG);

			if (isNewSearch) {
				page = 1;
			}

			int startingRecordIndex = (page - 1) * PAGE_LENGTH;
			Page<BundledFareConfigurationTO> resultsPage = ModuleServiceLocator.getBundledFareBD()
					.searchBundledFareConfiguration(searchCriteria, startingRecordIndex, PAGE_LENGTH);

			bundledFareCriterias = resultsPage.getPageData();
			totalRecords = resultsPage.getTotalNoOfRecords();
			totalPages = totalRecords % PAGE_LENGTH > 0 ? (totalRecords / PAGE_LENGTH) + 1 : (totalRecords / PAGE_LENGTH);
			bundledFareNamesHtml = "<option value=''>All</option>" + SelectListGenerator.createBundledFareNamesList();
		} catch (Exception x) {
			success = false;
			messageTxt = airadminConfig.getMessage("um.promotion.bundled.fare.search.failed");
			log.error(messageTxt, x);
		}
		return S2Constants.Result.SUCCESS;
	}

	public String add() {
		try {
			checkPrivileges(PriviledgeConstants.ALLOW_ADD_BUNLED_FARE_CONFIG);
			setupModelData();
			ModuleServiceLocator.getBundledFareBD().addBundledFareConfiguration(bundledFareTO);
			messageTxt = airadminConfig.getMessage("um.promotion.bundled.fare.save.success");

		} catch (ModuleException me) {
			success = false;
			messageTxt = airadminConfig.getMessage(me.getExceptionCode());
			log.error(messageTxt, me);
		} catch (ParseException pe) {
			success = false;
			messageTxt = airadminConfig.getMessage("um.promotion.bundled.correct.date.format");
			log.error(messageTxt, pe);
		} catch (Exception e) {
			success = false;
			messageTxt = airadminConfig.getMessage(e.getMessage());
			log.error(messageTxt, e);
		}
		return S2Constants.Result.SUCCESS;
	}

	public String edit() {
		try {
			checkPrivileges(PriviledgeConstants.ALLOW_EDIT_BUNLED_FARE_CONFIG);
			setupModelData();
			Integer sourceBundledFareId = bundledFareTO.getId();
			Integer targetBundledFareId = ModuleServiceLocator.getBundledFareBD().updateBundledFareConfiguration(bundledFareTO);

			// copy matching display settings from source to target
			if (targetBundledFareId != null) {
				BundledFareDisplayUtil.copyDisplaySettings(sourceBundledFareId, targetBundledFareId);
			}

			messageTxt = airadminConfig.getMessage("um.promotion.bundled.fare.edit.success");

		} catch (ModuleException me) {
			success = false;
			messageTxt = airadminConfig.getMessage(me.getExceptionCode());
			log.error(messageTxt, me);
		} catch (ParseException pe) {
			success = false;
			messageTxt = airadminConfig.getMessage("um.promotion.bundled.correct.date.format");
			log.error(messageTxt, pe);
		} catch (Exception e) {
			success = false;
			messageTxt = airadminConfig.getMessage(e.getMessage());
			log.error(messageTxt, e);
		}
		return S2Constants.Result.SUCCESS;
	}

	private void checkPrivileges(String privilege) {
		if (!BasicRequestHandler.hasPrivilege(request, privilege)) {
			log.error("Unauthorized operation:" + request.getUserPrincipal().getName() + ":"
					+ PriviledgeConstants.ALLOW_TERMS_N_CONDITIONS_EDITING);
			throw new ModuleRuntimeException(WebConstants.ERR_UNAUTHORIZED_ACCESS);
		}
	}

	public Collection<BundledFareConfigurationTO> getBundledFareCriterias() {
		return bundledFareCriterias;
	}

	public void setSearchCriteria(BundledFareSearchCriteriaTO searchCriteria) {
		this.searchCriteria = searchCriteria;
	}

	public BundledFareSearchCriteriaTO getSearchCriteria() {
		return searchCriteria;
	}

	public void setBundledFare(String bundledFare) {
		this.bundledFare = bundledFare;
	}

	public void setFlightDateFrom(String flightDateFrom) throws ParseException {
		this.flightDateFrom = flightDateFrom;
	}

	public void setFlightDateTo(String flightDateTo) throws ParseException {
		this.flightDateTo = flightDateTo;
	}

	public void setBookingClasses(String bookingClasses) {
		this.bookingClasses = bookingClasses;
	}

	public void setOndCodes(String ondCodes) {
		this.ondCodes = ondCodes;
	}

	public void setAgents(String agents) {
		this.agents = agents;
	}

	public void setFlightNumbers(String flightNumbers) {
		this.flightNumbers = flightNumbers;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(int totalRecords) {
		this.totalRecords = totalRecords;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setNewSearch(boolean isNewSearch) {
		this.isNewSearch = isNewSearch;
	}

	public void setApplicableSalesChannels(String applicableSalesChannels) {
		this.applicableSalesChannels = applicableSalesChannels;
	}

	public String getUploadImage() {
		return uploadImage;
	}

	public void setUploadImage(String uploadImage) {
		this.uploadImage = uploadImage;
	}

	public String getUploadImageStripe() {
		return uploadImageStripe;
	}

	public void setUploadImageStripe(String uploadImageStripe) {
		this.uploadImageStripe = uploadImageStripe;
	}

	public String getBundledFareNamesHtml() {
		return bundledFareNamesHtml;
	}

	public void setServiceTemplates(String serviceTemplates) {
		this.serviceTemplates = serviceTemplates;
	}

	public String getPointOfSales() {
		return pointOfSales;
	}

	public void setPointOfSales(String pointOfSales) {
		this.pointOfSales = pointOfSales;
	}

	@SuppressWarnings("unchecked")
	private void setupModelData() throws ParseException, ModuleException {

		if (isEmptyOrNull(flightDateFrom) || isEmptyOrNull(flightDateTo)) {
			throw new ModuleException("um.promotion.bundled.fare.range.invalid");
		}

		bundledFareTO = JSONFETOParser.parse(BundledFareConfigurationTO.class, bundledFare);
		bundledFareTO.setFlightDateFrom(new SimpleDateFormat(DATE_FORMAT).parse(flightDateFrom));
		bundledFareTO.setFlightDateTo(new SimpleDateFormat(DATE_FORMAT).parse(flightDateTo));

		if (CalendarUtil.isGreaterThan(bundledFareTO.getFlightDateFrom(), bundledFareTO.getFlightDateTo())) {
			throw new ModuleException("um.promotion.bundled.fare.invalid.date");
		}

		if (isEmptyOrNull(bundledFareTO.getBundledFareName())) {
			throw new ModuleException("um.promotion.bundled.fare.name.required");
		}

		Set<BundledFarePeriodTO> bunldedFarePeriods = JSONFETOParser.parseCollection(HashSet.class, BundledFarePeriodTO.class,
				serviceTemplates);

		validateBundleFareSubRanges(bunldedFarePeriods);

		bundledFareTO.setBundledFarePeriods(bunldedFarePeriods);

		Set<String> bookingClassSet = JSONFETOParser.parseCollection(HashSet.class, String.class, bookingClasses);
		bundledFareTO.setBookingClasses(bookingClassSet);

		Set<String> ondSet = JSONFETOParser.parseCollection(HashSet.class, String.class, ondCodes);
		bundledFareTO.setOndCodes(ondSet);

		Set<String> agentSet = JSONFETOParser.parseCollection(HashSet.class, String.class, agents);
		bundledFareTO.setAgents(agentSet);

		Set<String> flightNumberSet = JSONFETOParser.parseCollection(HashSet.class, String.class, flightNumbers);
		bundledFareTO.setFlightNumbers(flightNumberSet);

		Set<String> channelSet = JSONFETOParser.parseCollection(HashSet.class, String.class, applicableSalesChannels);
		bundledFareTO.setApplicableSalesChannels(channelSet);

		Set<String> countryCodes = JSONFETOParser.parseCollection(HashSet.class, String.class, pointOfSales);
		bundledFareTO.setPointOfSale(countryCodes);
	}

	private boolean isEmptyOrNull(String value) {
		return value == null || value.trim().isEmpty();
	}

	private void validateBundleFareSubRanges(Set<BundledFarePeriodTO> bunldedFarePeriods) throws ModuleException {

		Date flightDateFrom = bundledFareTO.getFlightDateFrom();
		Date flightDateTo = bundledFareTO.getFlightDateTo();

		if (bunldedFarePeriods == null || bunldedFarePeriods.isEmpty()) {
			throw new ModuleException("um.promotion.bundled.fare.no.services");
		}

		Set<BundledFarePeriodTO> sortedBundleFarePeriods = new TreeSet<BundledFarePeriodTO>(bunldedFarePeriods);
		Date lastServiceDate = null;

		for (Iterator<BundledFarePeriodTO> iterator = sortedBundleFarePeriods.iterator(); iterator.hasNext();) {
			BundledFarePeriodTO bundledFarePeriodTO = (BundledFarePeriodTO) iterator.next();
			Date serviceStartDate = bundledFarePeriodTO.getFlightFrom();
			Date serviceEndDate = bundledFarePeriodTO.getFlightTo();

			if (CalendarUtil.isGreaterThan(serviceStartDate, serviceEndDate)) {
				throw new ModuleException("um.promotion.bundled.fare.invalid.date");
			}

			if (!CalendarUtil.isBetweenIncludingLimits(serviceStartDate, flightDateFrom, flightDateTo)
					|| !CalendarUtil.isBetweenIncludingLimits(serviceEndDate, flightDateFrom, flightDateTo)) {
				throw new ModuleException("um.promotion.bundled.fare.range.invalid");
			}

			if (lastServiceDate != null) {
				if (CalendarUtil.isLessThan(serviceStartDate, lastServiceDate)) {
					throw new ModuleException("um.promotion.bundled.fare.overlap");
				}
			}
			lastServiceDate = serviceEndDate;
		}
	}

}
