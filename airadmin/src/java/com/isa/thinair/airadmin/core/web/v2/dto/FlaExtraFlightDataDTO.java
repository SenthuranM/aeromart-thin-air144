package com.isa.thinair.airadmin.core.web.v2.dto;

import java.io.Serializable;
import java.util.HashMap;

public class FlaExtraFlightDataDTO implements Serializable {

	private static final long serialVersionUID = -4822293415313106079L;

	private Integer flightID;
	private String departDate;
	private String status;
	HashMap<String, CosWiseDataDTO> cosWiseData;
	private boolean flown;

	public Integer getFlightID() {
		return flightID;
	}

	public String getDepartDate() {
		return departDate;
	}

	public HashMap<String, CosWiseDataDTO> getCosWiseData() {
		return cosWiseData;
	}

	public void setFlightID(Integer flightID) {
		this.flightID = flightID;
	}

	public void setDepartDate(String departDate) {
		this.departDate = departDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setCosWiseData(HashMap<String, CosWiseDataDTO> cosWiseData) {
		this.cosWiseData = cosWiseData;
	}

	public boolean isFlown() {
		return flown;
	}

	public void setFlown(boolean flown) {
		this.flown = flown;
	}

}
