package com.isa.thinair.airadmin.core.service;

import java.util.Date;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.commons.core.util.SystemPropertyUtil;
import com.isa.thinair.platform.core.controller.ModuleFramework;

/**
 * @author sudheera
 */
public class FrameworkStartupServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	public void init(ServletConfig servletConfig) throws ServletException {
		super.init(servletConfig);
		ModuleFramework.startup();
		setGlobals();
	}

	public void service(HttpServletRequest request, HttpServletResponse resp) throws ServletException, java.io.IOException {

	}

	private void setGlobals() {
		setCarrierName();
		setReleaseVersion();
		setSecureURL();
	}

	private void setSecureURL() {
		String secureUrl = AiradminModuleUtils.getConfig().getSecureUrl();
		if (secureUrl == null) {
			secureUrl = "";
		}
		getServletContext().setAttribute(WebConstants.SECURE_URL, secureUrl);
	}

	private void setCarrierName() {
		String strCarrName = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME);
		getServletContext().setAttribute(WebConstants.APP_CARRIER_NAME, strCarrName);
	}

	private void setReleaseVersion() {
		if (SystemPropertyUtil.isCreateNewBuildIdWithEveryRestart()) {
			getServletContext().setAttribute(WebConstants.APP_RELEASE_VERSION,
					WebConstants.APP_RELEASE_VERSION_URL_KEY + "=" + new Date().getTime());
		} else {
			getServletContext().setAttribute(WebConstants.APP_RELEASE_VERSION,
					WebConstants.APP_RELEASE_VERSION_URL_KEY + "=" + globalConfig.getLastBuildVersion());
		}
	}

}