package com.isa.thinair.airadmin.core.web.v2.action.GroupBooking;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONResult;
import org.apache.struts2.json.JSONUtil;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ManageUserAssignmentAction {
	private static Log log = LogFactory.getLog(ManageUserAssignmentAction.class);
	private String users;

	private String stations;

	private String msgType;

	private String stationsExist;

	private long requestID;

	public String execute() {
		try {
			stations = SelectListGenerator.createStationCodeList();
			// SelectListGenerator.createUsersList();
		} catch (Exception e) {
			log.error("execute ", e);
		}
		return S2Constants.Result.SUCCESS;
	}

	public String save() {
		try {
			Set<String> stationSet = new HashSet((List<String>) JSONUtil.deserialize(stations));
			ModuleServiceLocator.getGroupBookingBD().addUserStations(users, stationSet);
			setMsgType(S2Constants.Result.SUCCESS);
		} catch (ModuleException e) {

			setMsgType(S2Constants.Result.ERROR);
			e.printStackTrace();
		} catch (JSONException e) {

			setMsgType(S2Constants.Result.ERROR);
			e.printStackTrace();
		}

		return getMsgType();

	}

	public String get() {
		try {

			stationsExist = ModuleServiceLocator.getGroupBookingBD().getUserStation(users);

			setMsgType(S2Constants.Result.SUCCESS);
		} catch (ModuleException e) {

			setMsgType(S2Constants.Result.ERROR);
			e.printStackTrace();
		}

		return getMsgType();

	}

	public String assignGroupStations() {
		try {
			Set<String> stationSet = new HashSet((List<String>) JSONUtil.deserialize(stations));
			ModuleServiceLocator.getGroupBookingBD().addGroupBookingStations(requestID, stationSet);
			setMsgType(S2Constants.Result.SUCCESS);
		} catch (ModuleException e) {

			setMsgType(S2Constants.Result.ERROR);
			e.printStackTrace();
		} catch (JSONException e) {

			setMsgType(S2Constants.Result.ERROR);
			e.printStackTrace();
		}

		return getMsgType();

	}

	public String findGroupStations() {
		try {

			stationsExist = ModuleServiceLocator.getGroupBookingBD().getGroupBookingStation(requestID);

			setMsgType(S2Constants.Result.SUCCESS);
		} catch (ModuleException e) {

			setMsgType(S2Constants.Result.ERROR);
			e.printStackTrace();
		}

		return getMsgType();

	}

	public String getStations() {
		return stations;
	}

	public void setStations(String stations) {
		this.stations = stations;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public String getUsers() {
		return users;
	}

	public void setUsers(String users) {
		this.users = users;
	}

	public String getStationsExist() {
		return stationsExist;
	}

	public void setStationsExist(String stationsExist) {
		this.stationsExist = stationsExist;
	}

	public long getRequestID() {
		return requestID;
	}

	public void setRequestID(long requestID) {
		this.requestID = requestID;
	}

}
