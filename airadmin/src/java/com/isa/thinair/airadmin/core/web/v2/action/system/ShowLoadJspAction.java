package com.isa.thinair.airadmin.core.web.v2.action.system;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.apache.struts2.json.JSONException;
import org.apache.struts2.json.JSONUtil;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.airportTransfer.AirportTransferHTMLGenerator;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.master.DemoRH;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airadmin.core.web.v2.dto.mis.MISProcessParams;
import com.isa.thinair.airadmin.core.web.v2.generator.inventory.LogicalCabinClassHG;
import com.isa.thinair.airadmin.core.web.v2.generator.master.AirportMessageHG;
import com.isa.thinair.airadmin.core.web.v2.generator.master.AutoCheckinMessageHG;
import com.isa.thinair.airadmin.core.web.v2.generator.master.BaggageMessageHG;
import com.isa.thinair.airadmin.core.web.v2.generator.master.CityHG;
import com.isa.thinair.airadmin.core.web.v2.generator.master.DefaultAnciTemplateHG;
import com.isa.thinair.airadmin.core.web.v2.generator.master.ItineraryAdvertisementHG;
import com.isa.thinair.airadmin.core.web.v2.generator.master.MealCategoryHG;
import com.isa.thinair.airadmin.core.web.v2.generator.master.MealHG;
import com.isa.thinair.airadmin.core.web.v2.generator.master.SSRMessageHG;
import com.isa.thinair.airadmin.core.web.v2.generator.master.StateHG;
import com.isa.thinair.airadmin.core.web.v2.generator.promotion.PromotionMessageHG;
import com.isa.thinair.airadmin.core.web.v2.generator.reports.scheduling.ScheduledReportsManagementHG;
import com.isa.thinair.airadmin.core.web.v2.generator.tools.BlacklistPAXMessageHG;
import com.isa.thinair.airadmin.core.web.v2.generator.tools.DashboardMessageHG;
import com.isa.thinair.airadmin.core.web.v2.generator.tools.SearchRollforwardMessageHG;
import com.isa.thinair.airadmin.core.web.v2.handler.master.SSRChargesRH;
import com.isa.thinair.airadmin.core.web.v2.handler.master.SSRInfoRH;
import com.isa.thinair.airadmin.core.web.v2.handler.tools.ChangePAXandETStatusRequestHandler;
import com.isa.thinair.airadmin.core.web.v2.handler.tools.FlightSummaryRequestHandler;
import com.isa.thinair.airadmin.core.web.v2.util.CommonUtil;
import com.isa.thinair.airmaster.api.dto.MasterDataCriteriaDTO;
import com.isa.thinair.airmaster.api.dto.MasterDataDTO;
import com.isa.thinair.airmaster.api.model.SSR;
import com.isa.thinair.airmaster.api.model.SSRCategory;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.commons.api.dto.GDSStatusTO;
import com.isa.thinair.commons.api.dto.LogicalCabinClassDTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.promotion.api.to.constants.BundledFareConstants;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Results({
		@Result(name = S2Constants.Result.ERROR, value = AdminStrutsConstants.AdminJSP.ERROR_JSP),
		@Result(name = S2Constants.Jsp.Master.MEAL_LOAD_JSP, value = S2Constants.Jsp.Master.MEAL_LOAD_JSP),
		@Result(name = S2Constants.Jsp.Master.MEAL_TEMP_LOAD_JSP, value = S2Constants.Jsp.Master.MEAL_TEMP_LOAD_JSP),
		@Result(name = S2Constants.Jsp.Master.BAGGAGE_LOAD_JSP, value = S2Constants.Jsp.Master.BAGGAGE_LOAD_JSP),
		@Result(name = S2Constants.Jsp.Master.BAGGAGE_TEMP_LOAD_JSP, value = S2Constants.Jsp.Master.BAGGAGE_TEMP_LOAD_JSP),
		@Result(name = S2Constants.Jsp.Master.OND_BAGGAGE_TEMP_LOAD_JSP, value = S2Constants.Jsp.Master.OND_BAGGAGE_TEMP_LOAD_JSP),
		@Result(name = S2Constants.Jsp.Master.OND_TEMP_LOAD_JSP, value = S2Constants.Jsp.Master.OND_TEMP_LOAD_JSP),
		@Result(name = S2Constants.Jsp.MIS.MIS_REPORTS_JSP, value = S2Constants.Jsp.MIS.MIS_REPORTS_JSP),
		@Result(name = S2Constants.Jsp.MIS.MIS_REPORTS_CORE_JSP, value = S2Constants.Jsp.MIS.MIS_REPORTS_CORE_JSP),
		@Result(name = S2Constants.Jsp.MIS.MIS_REGIONAL_POPUP_JSP, value = S2Constants.Jsp.MIS.MIS_REGIONAL_POPUP_JSP),
		@Result(name = S2Constants.Jsp.ScheduleReports.SCHE_RPT_MANAGEMENT, value = S2Constants.Jsp.ScheduleReports.SCHE_RPT_MANAGEMENT),
		@Result(name = S2Constants.Jsp.Tools.MESSAGE_MANAGEMENT_JSP, value = S2Constants.Jsp.Tools.MESSAGE_MANAGEMENT_JSP),
		@Result(name = S2Constants.Jsp.Tools.BSPREPORT_LOAD_JSP, value = S2Constants.Jsp.Tools.BSPREPORT_LOAD_JSP),
		@Result(name = S2Constants.Jsp.Tools.UPDATE_OND_JSP, value = S2Constants.Jsp.Tools.UPDATE_OND_JSP),
		@Result(name = S2Constants.Jsp.Master.FARE360_JSP, value = S2Constants.Jsp.Master.FARE360_JSP),
		@Result(name = S2Constants.Jsp.Master.FARE360DATA_JSP, value = S2Constants.Jsp.Master.FARE360DATA_JSP),
		@Result(name = S2Constants.Jsp.Master.FARE360EDIT_JSP, value = S2Constants.Jsp.Master.FARE360EDIT_JSP),
		@Result(name = S2Constants.Jsp.Tools.UPDATE_OND_JSP, value = S2Constants.Jsp.Tools.UPDATE_OND_JSP),
		@Result(name = S2Constants.Jsp.Master.AIRPORT_MESSAGE_JSP, value = S2Constants.Jsp.Master.AIRPORT_MESSAGE_JSP),
		@Result(name = S2Constants.Jsp.Master.MEAL_CATEGORY_LOAD_JSP, value = S2Constants.Jsp.Master.MEAL_CATEGORY_LOAD_JSP),
		@Result(name = S2Constants.Jsp.Master.SSR_INFO_LOAD_JSP, value = S2Constants.Jsp.Master.SSR_INFO_LOAD_JSP),
		@Result(name = S2Constants.Jsp.Master.SSR_CHARGES_LOAD_JSP, value = S2Constants.Jsp.Master.SSR_CHARGES_LOAD_JSP),
		@Result(name = S2Constants.Jsp.Master.SSR_TEMPLATE_LOAD_JSP, value = S2Constants.Jsp.Master.SSR_TEMPLATE_LOAD_JSP),
		@Result(name = S2Constants.Jsp.Master.AIRPORT_TRANSFER_LOAD_JSP, value = S2Constants.Jsp.Master.AIRPORT_TRANSFER_LOAD_JSP),
		@Result(name = S2Constants.Jsp.Master.AUTOMATIC_CHECKIN_TEMPLATE_LOAD_JSP, value = S2Constants.Jsp.Master.AUTOMATIC_CHECKIN_TEMPLATE_LOAD_JSP),
		@Result(name = S2Constants.Jsp.Master.AIRPORT_TRANSFER_TEMPLATE_LOAD_JSP, value = S2Constants.Jsp.Master.AIRPORT_TRANSFER_TEMPLATE_LOAD_JSP),
		@Result(name = S2Constants.Jsp.Inventory.LOGICAL_CABIN_CLASS_LOAD_JSP, value = S2Constants.Jsp.Inventory.LOGICAL_CABIN_CLASS_LOAD_JSP),
		@Result(name = S2Constants.Jsp.Inventory.GROUP_BOOKING_CLASS_LOAD_JSP, value = S2Constants.Jsp.Inventory.GROUP_BOOKING_CLASS_LOAD_JSP),
		@Result(name = S2Constants.Jsp.Allocate.FLIGHT_INVANTORY_LOAD_JSP, value = S2Constants.Jsp.Allocate.FLIGHT_INVANTORY_LOAD_JSP),
		@Result(name = S2Constants.Jsp.Allocate.FLIGHT_ALLOCATE_LOAD_JSP, value = S2Constants.Jsp.Allocate.FLIGHT_ALLOCATE_LOAD_JSP),
		@Result(name = S2Constants.Jsp.Tools.PFS_HISTORY_JSP, value = S2Constants.Jsp.Tools.PFS_HISTORY_JSP),
		@Result(name = S2Constants.Jsp.Tools.CONFIG_TIME_LIMITS_JSP, value = S2Constants.Jsp.Tools.CONFIG_TIME_LIMITS_JSP),
		@Result(name = S2Constants.Jsp.Tools.FLIGHT_SUMMARY_JSP, value = S2Constants.Jsp.Tools.FLIGHT_SUMMARY_JSP),
		@Result(name = S2Constants.Jsp.Promotions.PROMO_TEMPLATES_MGMT_JSP, value = S2Constants.Jsp.Promotions.PROMO_TEMPLATES_MGMT_JSP),
		@Result(name = S2Constants.Jsp.Promotions.PROMO_REQUESTS_MGMT_JSP, value = S2Constants.Jsp.Promotions.PROMO_REQUESTS_MGMT_JSP),
		@Result(name = S2Constants.Jsp.Promotions.PROMO_REQUESTS_SMRY_JSP, value = S2Constants.Jsp.Promotions.PROMO_REQUESTS_SMRY_JSP),
		@Result(name = S2Constants.Jsp.Flight.FLIGHT_REPROTECTION_JSP, value = S2Constants.Jsp.Flight.FLIGHT_REPROTECTION_JSP),
		@Result(name = S2Constants.Jsp.Promotions.PROMO_CODE_MGMT_JSP, value = S2Constants.Jsp.Promotions.PROMO_CODE_MGMT_JSP),
		@Result(name = S2Constants.Jsp.Tools.PNRGOVREPORT_LOAD_JSP, value = S2Constants.Jsp.Tools.PNRGOVREPORT_LOAD_JSP),
		@Result(name = S2Constants.Jsp.Tools.GROUP_BOOKING_REQUEST, value = S2Constants.Jsp.Tools.GROUP_BOOKING_REQUEST),
		@Result(name = AdminStrutsConstants.AdminJSP.MANAGE_TERMS_TEMPLATES_JSP, value = AdminStrutsConstants.AdminJSP.MANAGE_TERMS_TEMPLATES_JSP),
		@Result(name = AdminStrutsConstants.AdminJSP.EDIT_TERMS_TEMPLATES_JSP, value = AdminStrutsConstants.AdminJSP.EDIT_TERMS_TEMPLATES_JSP),
		@Result(name = S2Constants.Jsp.Promotions.ANCI_OFFER_MGMT_JSP, value = S2Constants.Jsp.Promotions.ANCI_OFFER_MGMT_JSP),
		@Result(name = S2Constants.Jsp.Tools.USER_STATION_ASSIGNMENT, value = S2Constants.Jsp.Tools.USER_STATION_ASSIGNMENT),
		@Result(name = S2Constants.Jsp.Tools.GROUP_BOOKING_STATION_ASSIGNMENT, value = S2Constants.Jsp.Tools.GROUP_BOOKING_STATION_ASSIGNMENT),
		@Result(name = S2Constants.Jsp.Tools.BSPHOTFILE_LOAD_JSP, value = S2Constants.Jsp.Tools.BSPHOTFILE_LOAD_JSP),
		@Result(name = S2Constants.Jsp.Fare.PRORATION, value = S2Constants.Jsp.Fare.PRORATION),
		@Result(name = S2Constants.Jsp.Tools.OFFICER_NOTIFICATION_CONFIG, value = S2Constants.Jsp.Tools.OFFICER_NOTIFICATION_CONFIG),
		@Result(name = S2Constants.Jsp.Tools.CHANGE_PAX_ET_STATUS, value = S2Constants.Jsp.Tools.CHANGE_PAX_ET_STATUS),
		@Result(name = S2Constants.Jsp.Tools.AGENT_USER_ROLES, value = S2Constants.Jsp.Tools.AGENT_USER_ROLES),
		@Result(name = S2Constants.Jsp.Promotions.BUNDLED_FARES_MGMT_JSP, value = S2Constants.Jsp.Promotions.BUNDLED_FARES_MGMT_JSP),
		@Result(name = S2Constants.Jsp.Promotions.BUNDLED_DESCRIPTION_JSP, value = S2Constants.Jsp.Promotions.BUNDLED_DESCRIPTION_JSP),
		@Result(name = S2Constants.Jsp.Promotions.VOU_TEMPLATES_MGMT_JSP, value = S2Constants.Jsp.Promotions.VOU_TEMPLATES_MGMT_JSP),
		@Result(name = S2Constants.Jsp.Master.ADVERTISEMENT_JSP, value = S2Constants.Jsp.Master.ADVERTISEMENT_JSP),
        	@Result(name = S2Constants.Jsp.Promotions.CREATE_CAMPAIGN_MGMT_JSP, value = S2Constants.Jsp.Promotions.CREATE_CAMPAIGN_MGMT_JSP),
        	@Result(name = S2Constants.Jsp.Tools.BLACKLIST_PAX_MANAGEMENT_JSP, value = S2Constants.Jsp.Tools.BLACKLIST_PAX_MANAGEMENT_JSP),
        	@Result(name = S2Constants.Jsp.Tools.SEARCH_ROLL_FORWARD_JSP, value = S2Constants.Jsp.Tools.SEARCH_ROLL_FORWARD_JSP),
        	@Result(name = S2Constants.Jsp.Tools.BLACKLIST_RULES_MNG, value = S2Constants.Jsp.Tools.BLACKLIST_RULES_MNG),
        	@Result(name = AdminStrutsConstants.AdminJSP.MANAGE_VOUCHER_TERMS_TEMPLATES_JSP, value = AdminStrutsConstants.AdminJSP.MANAGE_VOUCHER_TERMS_TEMPLATES_JSP),
		@Result(name = AdminStrutsConstants.AdminJSP.EDIT_VOUCHER_TERMS_TEMPLATES_JSP, value = AdminStrutsConstants.AdminJSP.EDIT_VOUCHER_TERMS_TEMPLATES_JSP),
		@Result(name = S2Constants.Jsp.Master.ROUTEWISE_DEFAULT_ANCI_TEMPLATE, value = S2Constants.Jsp.Master.ROUTEWISE_DEFAULT_ANCI_TEMPLATE),
		@Result(name = S2Constants.Jsp.Master.STATE_ADMIN_JSP, value = S2Constants.Jsp.Master.STATE_ADMIN_JSP),
		@Result(name = AdminStrutsConstants.AdminJSP.CITY_ADMIN_JSP, value = AdminStrutsConstants.AdminJSP.CITY_ADMIN_JSP),
		@Result(name = S2Constants.Jsp.Master.BUNDLED_CATEGORY, value = S2Constants.Jsp.Master.BUNDLED_CATEGORY)})
public class ShowLoadJspAction extends BaseRequestAwareAction {
	private static Log log = LogFactory.getLog(ShowLoadJspAction.class);
	
	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();

	public String loadLogicalCabinClass() {
		try {
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, LogicalCabinClassHG.getClientErrors(request));
			String strHtmlCC = SelectListGenerator.createAllCabinClass();
			request.setAttribute(WebConstants.REQ_HTML_CABINCLASS_LIST, strHtmlCC);
			
			String strHtmlLCC = SelectListGenerator.createAllLogicalCabinClass();
			request.setAttribute(WebConstants.REQ_HTML_LOGICALCABINCLASS_LIST, strHtmlLCC);
			
			String BundleDescTemplateList = SelectListGenerator.createBundleDescTemplateList();
			request.setAttribute(WebConstants.REQ_HTML_BUNDLEDESC_TEMPLATE_LIST, BundleDescTemplateList);
			
			
			boolean enableValue=  AppSysParamsUtil.isBundleFareDescriptionTemplateV2Enabed();
			request.setAttribute(WebConstants.REQ_BUNDLE_TEMPLATE_ENABLE, enableValue);
			/*
			 * Load the language as an html list. eg: <OPTION>English</OPTION> <OPTION>Arabic</OPTION> etc. Will be
			 * loaded on JSP pages being called. In this instant LogicalCabinCalss.jsp
			 */
			String strHtml = SelectListGenerator.createLanguageList();
			request.setAttribute(WebConstants.REQ_HTML_LANGUAGE_LIST, strHtml);

			String strImgpath = AppSysParamsUtil.getImageUploadPath();
			request.setAttribute("reqImgPath", "var lccImagePath = '" + strImgpath + "';");
			request.setAttribute("reqLCCImagePrefix", "var tempLccPrefix = '" + LogicalCabinClassDTO.TEMP_LCC_PREFIX + "';");
			request.setAttribute("reqWithFlexiImagePrefix", "var tempWithFlexiPrefix = '"
					+ LogicalCabinClassDTO.TEMP_LCC_WITH_FLEXI_PREFIX + "';");

		} catch (Exception e) {
			log.error("cannot load Logical Cabin Classes :" + e);
			return S2Constants.Result.ERROR;
		}
		return S2Constants.Jsp.Inventory.LOGICAL_CABIN_CLASS_LOAD_JSP;
	}
	
	public String groupLogicalCabinClass() {
		return S2Constants.Jsp.Inventory.GROUP_BOOKING_CLASS_LOAD_JSP;
	}

	public String loadAllocateSearch() {
		// set requestScope params
		try {
			// Carrier codes
			User user = (User) request.getSession().getAttribute(WebConstants.SES_CURR_USER);
			StringBuilder usb = new StringBuilder();
			Set stCarriers = user.getCarriers();
			if (stCarriers != null) {
				int i = 0;
				Iterator stIter = stCarriers.iterator();
				while (stIter.hasNext()) {
					usb.append(" arrCarriers[" + i + "] = '" + (String) stIter.next() + "';");
					i++;
				}
			}
			request.setAttribute("reqFromPage", request.getParameter("hdnFrom"));
			request.setAttribute("hdnFlightSumm", request.getParameter("hdnFlightSumm"));
			request.setAttribute("reqHidString", request.getParameter("hdnFLString"));
			request.setAttribute("reqIsLogicalCCEnabled", AppSysParamsUtil.isLogicalCabinClassEnabled());
			request.setAttribute("reqIsPromotionEnabled", AppSysParamsUtil.isPromotionModuleEnabled());
			request.setAttribute("hideOneWayReturnPaxCount", !AppSysParamsUtil.showFCCSegBCInventoryOneWayReturnPaxCounts());
			// Setting whether the use has privileges to change bc allocation status
			request.setAttribute(WebConstants.REQ_HAS_BC_STATUS_CHANGE_PRIVILEGE,
					hasPrivilege(request, WebConstants.PLAN_INVN_BC_STATUS));
			String flightNumber = request.getParameter("hndCsOCFlightNo");
			Map<String, GDSStatusTO> gdsStatusMap = globalConfig.getActiveGdsMap();
			if (flightNumber != null && flightNumber.length() > 2 && gdsStatusMap != null && !gdsStatusMap.isEmpty()) {
				for (GDSStatusTO gdsStatusTO : gdsStatusMap.values()) {
					if (gdsStatusTO != null && flightNumber.substring(0, 2).equalsIgnoreCase(gdsStatusTO.getCarrierCode())
							&& !gdsStatusTO.isFlightInvModifiable()) {
						request.setAttribute("hdnCsFlightInvModifiable", "false");
					}
				}
			}
		} catch (Exception e) {
			log.error("cannot load required lists :" + e);
			return S2Constants.Result.ERROR;
		}
		return S2Constants.Jsp.Allocate.FLIGHT_INVANTORY_LOAD_JSP;
	}

	public String loadAllocate() {
		request.setAttribute("dataModel", request.getParameter("hidGridData"));
		request.setAttribute("dataSelRowData", request.getParameter("hidSelRowData"));
		return S2Constants.Jsp.Allocate.FLIGHT_ALLOCATE_LOAD_JSP;
	}

	public String loadRouteWiseDefaultAnciTemplate() {
		try {
			DefaultAnciTemplateHG.setHTMLComponents(request);

		} catch (ModuleException moduleException) {
			log.error("cannot load Default Anci Template :" + moduleException);
			return S2Constants.Result.ERROR;
		}

		return S2Constants.Jsp.Master.ROUTEWISE_DEFAULT_ANCI_TEMPLATE;
	}

	public String loadBundledCategory() {
		return S2Constants.Jsp.Master.BUNDLED_CATEGORY;
	}

	public String loadBundledDescription() {
		return S2Constants.Jsp.Promotions.BUNDLED_DESCRIPTION_JSP;
	}

	public String loadMeal() {
		try {
			// sets client error code
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, MealHG.getClientErrors(request));
			String strImgpath = AppSysParamsUtil.getImageUploadPath();
			request.setAttribute("reqMealImgPath", "mlImagePath = '" + strImgpath + "';");

			String strHtml = SelectListGenerator.createLanguageList();
			request.setAttribute(WebConstants.REQ_HTML_LANGUAGE_LIST, strHtml);

			String strMealCodes = SelectListGenerator.createMealCode();
			request.setAttribute(S2Constants.RequestConstant.MEAL_LIST, strMealCodes);

			String strMealCategoryCodes = SelectListGenerator.createMealCategory();
			request.setAttribute(S2Constants.RequestConstant.MEAL_CATEGORY_LIST, strMealCategoryCodes);

			String strCSList = SelectListGenerator.createCabinClassList();
			request.setAttribute(S2Constants.RequestConstant.MEAL_CC_CODE, strCSList);

			request.setAttribute(S2Constants.RequestConstant.ALL_COS_LIST,
					SelectListGenerator.createCabinClassAndLogicalCabinClassesList());

			String strIATACodeList = SelectListGenerator.createIATACodeList();
			request.setAttribute(S2Constants.RequestConstant.IATA_CODE_LIST, strIATACodeList);

			if (AppSysParamsUtil.isDisplayIBEListsDynamicallyEnabled()) {
				request.setAttribute(WebConstants.LIST_FOR_DISPLAY_ENABLED, "true");
			} else {
				request.setAttribute(WebConstants.LIST_FOR_DISPLAY_ENABLED, "false");
			}

			if (AppSysParamsUtil.isFOCMealSelectionEnabled()) {
				request.setAttribute(WebConstants.FOC_MEAL_SEL_ENABLED, "true");
			} else {
				request.setAttribute(WebConstants.FOC_MEAL_SEL_ENABLED, "false");
			}

		} catch (ModuleException moduleException) {
			log.error("cannot load Meal Codes :" + moduleException);
			return S2Constants.Result.ERROR;
		}

		return S2Constants.Jsp.Master.MEAL_LOAD_JSP;
	}

	public String loadBSPReportManagement() {
		try {
			String strHtml = SelectListGenerator.createLanguageList();
			request.setAttribute(WebConstants.REQ_HTML_LANGUAGE_LIST, strHtml);

			String strAgentCodes = SelectListGenerator.createAgents();
			request.setAttribute(S2Constants.RequestConstant.AGENT_CODE_LIST, strAgentCodes);

			String strBSPCountryCodes = SelectListGenerator.createBSPCountryList();
			request.setAttribute(S2Constants.RequestConstant.BSP_COUNTRY_LIST, strBSPCountryCodes);

			String strBSPCountryCodeAgentCodes = SelectListGenerator.getBSPAgentCodeCountryCodeList();
			request.setAttribute(WebConstants.REQ_HTML_BSPAGENT_COUNTRYCODE_LIST, strBSPCountryCodeAgentCodes);

		} catch (ModuleException moduleException) {
			log.error("cannot load BSP Reports :" + moduleException);
			return S2Constants.Result.ERROR;
		}

		return S2Constants.Jsp.Tools.BSPREPORT_LOAD_JSP;
	}

	public String loadPNRGOVReportManagement() {
		try {
			String strHtml = SelectListGenerator.createLanguageList();
			request.setAttribute(WebConstants.REQ_HTML_LANGUAGE_LIST, strHtml);

			String strBSPCountryCodes = SelectListGenerator.createPNRGOVCountryList();
			request.setAttribute(S2Constants.RequestConstant.PNRGOV_COUNTRY_LIST, strBSPCountryCodes);

		} catch (ModuleException moduleException) {
			log.error("cannot load BSP Reports :" + moduleException);
			return S2Constants.Result.ERROR;
		}

		return S2Constants.Jsp.Tools.PNRGOVREPORT_LOAD_JSP;
	}

	public String loadMealCategory() {

		try {
			// sets client error code
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, MealCategoryHG.getClientErrors(request));

			String strHtml = SelectListGenerator.createLanguageList();
			request.setAttribute(WebConstants.REQ_HTML_LANGUAGE_LIST, strHtml);

			if (AppSysParamsUtil.isDisplayIBEListsDynamicallyEnabled()) {
				request.setAttribute(WebConstants.LIST_FOR_DISPLAY_ENABLED, "true");
			} else {
				request.setAttribute(WebConstants.LIST_FOR_DISPLAY_ENABLED, "false");
			}

		} catch (ModuleException me) {
			log.error("cannot load Meal Categories :" + me);
			return S2Constants.Result.ERROR;
		}

		return S2Constants.Jsp.Master.MEAL_CATEGORY_LOAD_JSP;
	}

	public String loadBaggage() {

		try {
			// sets client error code
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, BaggageMessageHG.getClientErrors(request));
			request.setAttribute(S2Constants.RequestConstant.ALL_COS_LIST,
					SelectListGenerator.createCabinClassAndLogicalCabinClassesList());

			String strHtml = SelectListGenerator.createLanguageList();
			request.setAttribute(WebConstants.REQ_HTML_LANGUAGE_LIST, strHtml);

			String strCSList = SelectListGenerator.createCabinClassList();
			request.setAttribute(S2Constants.RequestConstant.BAGGAGE_CC_CODE, strCSList);

			String strIATACodeList = SelectListGenerator.createIATACodeListForBaggage();
			request.setAttribute(S2Constants.RequestConstant.IATA_CODE_LIST, strIATACodeList);

			if (AppSysParamsUtil.isDisplayIBEListsDynamicallyEnabled()) {
				request.setAttribute(WebConstants.LIST_FOR_DISPLAY_ENABLED, "true");
			} else {
				request.setAttribute(WebConstants.LIST_FOR_DISPLAY_ENABLED, "false");
			}

		} catch (ModuleException moduleException) {
			log.error("cannot load baggage page Codes :" + moduleException);
			return S2Constants.Result.ERROR;
		}

		return S2Constants.Jsp.Master.BAGGAGE_LOAD_JSP;
	}

	public String loadMealTemplate() {
		try {
			request.setAttribute(S2Constants.RequestConstant.ALL_COS_LIST,
					SelectListGenerator.createCabinClassAndLogicalCabinClassesList());
			// sets the meal codes
			String strCabinCode = SelectListGenerator.createCabinClassList();
			request.setAttribute(S2Constants.RequestConstant.MEAL_CC_CODE, strCabinCode);

			String strTmplCodes = SelectListGenerator.createMealTemplateList();
			request.setAttribute(S2Constants.RequestConstant.ML_TEMPLATE_CODE, strTmplCodes);

			String strMealCodes = MealHG.setMealOptHTML();
			request.setAttribute(S2Constants.RequestConstant.MEAL_LIST, strMealCodes);

			// sets client error code
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, MealHG.getTeplateClientErrors(request));
			
			String strMealCategories = SelectListGenerator.createMealCategoryArray();
			request.setAttribute(S2Constants.RequestConstant.MEAL_CATEGORY_LIST, strMealCategories);
			
			String baseCurrency = AppSysParamsUtil.getBaseCurrency();
			request.setAttribute(WebConstants.REQ_BASE_CURRENCY, "var localCurrency ='"+ baseCurrency+"'");	

		} catch (ModuleException moduleException) {
			log.error("cannot load Meal Codes :" + moduleException);
			return S2Constants.Result.ERROR;
		}

		return S2Constants.Jsp.Master.MEAL_TEMP_LOAD_JSP;
	}

	public String loadBaggageTemplate() {
		try {
			request.setAttribute(S2Constants.RequestConstant.ALL_COS_LIST,
					SelectListGenerator.createCabinClassAndLogicalCabinClassesList());

			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, BaggageMessageHG.getTeplateClientErrors(request));

			String strCabinCode = SelectListGenerator.createCabinClassList();
			request.setAttribute(S2Constants.RequestConstant.BAGGAGE_CC_CODE, strCabinCode);

			String strTmplCodes = SelectListGenerator.createBaggageTemplateList();
			request.setAttribute(S2Constants.RequestConstant.BG_TEMPLATE_CODE, strTmplCodes);

		} catch (ModuleException moduleException) {
			log.error("cannot load baggage template codes :" + moduleException);
			return S2Constants.Result.ERROR;
		}

		return S2Constants.Jsp.Master.BAGGAGE_TEMP_LOAD_JSP;
	}

	public String loadONDBaggageTemplate() {
		try {
			request.setAttribute(S2Constants.RequestConstant.ALL_COS_LIST,
					SelectListGenerator.createCabinClassAndLogicalCabinClassesList());

			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, BaggageMessageHG.getTeplateClientErrors(request));

			String strCabinCode = SelectListGenerator.createCabinClassList();
			request.setAttribute(S2Constants.RequestConstant.BAGGAGE_CC_CODE, strCabinCode);

			String strTmplCodes = SelectListGenerator.createONDBaggageTemplateList();
			request.setAttribute(S2Constants.RequestConstant.OND_BG_TEMPLATE_CODE, strTmplCodes);

			String baggageSeatFactorApplyConditionList = SelectListGenerator.createBaggageSeatFactorConditionApplyFor();
			request.setAttribute(S2Constants.RequestConstant.SEAT_FACTOR_CONDITION_APPLY_FOR, baggageSeatFactorApplyConditionList);
			
			request.setAttribute(WebConstants.REQ_BG_CHRG_LOCL_CURR, hasPrivilege(request, WebConstants.MAS_BG_CHRG_LOCL_CURR));

			String strCurrencyComboList = SelectListGenerator.createCurrencyList();
			request.setAttribute(WebConstants.REQ_HTML_CURRENCY_COMBO, strCurrencyComboList);

			String baseCurrency = AppSysParamsUtil.getBaseCurrency();
			request.setAttribute(WebConstants.REQ_BASE_CURRENCY, baseCurrency);


		} catch (ModuleException moduleException) {
			log.error("cannot load OND baggage template codes :" + moduleException);
			return S2Constants.Result.ERROR;
		}
		return S2Constants.Jsp.Master.OND_BAGGAGE_TEMP_LOAD_JSP;
	}

	public String loadONDTemplate() {
		MasterDataCriteriaDTO criteriaDTO;
		MasterDataDTO masterDataDTO;

		try {
			/*
			 * if (AppSysParamsUtil.isLogicalCabinClassEnabled()) { String cosList = SelectListGenerator.createAllCOS();
			 * request.setAttribute(S2Constants.RequestConstant.ALL_COS_LIST, cosList); } else { String ccList =
			 * SelectListGenerator.createCabinClassLists();
			 * request.setAttribute(S2Constants.RequestConstant.ALL_COS_LIST, ccList); }
			 */

			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, BaggageMessageHG.getTeplateClientErrors(request));

			String strONDCodes = SelectListGenerator.createONDCodeList();
			request.setAttribute(S2Constants.RequestConstant.OND_CODE_LIST, strONDCodes);

			// Sets the Airport With Status to the Request
			String strHtmlAirports = SelectListGenerator.createActiveAirportList();
			request.setAttribute(WebConstants.REQ_HTML_ACTIVE_AIRPORT_SELECT_LIST, strHtmlAirports);

			criteriaDTO = new MasterDataCriteriaDTO();
			criteriaDTO.setLoadBookingClasses(true);
			criteriaDTO.setLoadFlightNumbers(true);
			criteriaDTO.setLoadAgents(false);

			if (AppSysParamsUtil.isLCCConnectivityEnabled()) {
				masterDataDTO = ModuleServiceLocator.getLCCMasterDataBD().getLccAgreementMasterData(criteriaDTO);
			} else {
				masterDataDTO = new MasterDataDTO();

				if (criteriaDTO.isLoadFlightNumbers()) {
					masterDataDTO.setFlightNumbers(CommonUtil.sortByValue(ModuleServiceLocator.getCommonServiceBD()
							.getFlightNumbers()));
				}

				if (criteriaDTO.isLoadBookingClasses()) {
					masterDataDTO.setBookingClasses(CommonUtil.sortByValue(ModuleServiceLocator.getCommonServiceBD()
							.getBookingClasses()));
				}

				if (criteriaDTO.isLoadAgents()) {
					masterDataDTO.setAgents(CommonUtil.sortByValue(ModuleServiceLocator.getCommonServiceBD().getAgents()));
				}
			}

			String bookingClasses = SelectListGenerator.createDropDown(masterDataDTO.getBookingClasses());
			request.setAttribute(WebConstants.REQ_HTML_BOOKINGCODES_LIST, bookingClasses);

			String flightNumbers = SelectListGenerator.createDropDown(masterDataDTO.getFlightNumbers());
			request.setAttribute(WebConstants.REQ_FLIGHT_NO_LIST, flightNumbers);

			String agents = SelectListGenerator.createAgentCodeAgainstIdList();
			request.setAttribute(WebConstants.REQ_HTML_AGENT_LIST, agents);

			String agentsStations = SelectListGenerator.createAgentsAndStationsJson(true);
			request.setAttribute(WebConstants.REQ_AGENTS_STATION, agentsStations);

		} catch (ModuleException moduleException) {
			log.error("cannot load OND template :" + moduleException);
			return S2Constants.Result.ERROR;
		}
		return S2Constants.Jsp.Master.OND_TEMP_LOAD_JSP;
	}

	public String loadMISReportsCore() {
		try {
			// set the drop down list info
			request.setAttribute("req_Airports", DemoRH.createAirportList());
			request.setAttribute("req_Regions", SelectListGenerator.createRegionsArray("arrAllRegions"));

			MISProcessParams mpParams = new MISProcessParams(request);
			request.setAttribute("req_InitialParams", CommonUtil.convertToJSON(mpParams));

		} catch (ModuleException moduleException) {
			log.error("cannot load MIS reports :" + moduleException);
			return S2Constants.Result.ERROR;
		}

		return S2Constants.Jsp.MIS.MIS_REPORTS_CORE_JSP;
	}

	public String loadMISReports() {
		return S2Constants.Jsp.MIS.MIS_REPORTS_JSP;
	}

	public String misRegionalPopup() {
		return S2Constants.Jsp.MIS.MIS_REGIONAL_POPUP_JSP;
	}

	/**
	 * Load proration page.
	 * 
	 * @return
	 */
	public String loadProrationPopup() {
		return S2Constants.Jsp.Fare.PRORATION;
	}

	/**
	 * Load Scheduled Report Management
	 * 
	 * @return
	 */
	public String loadScheduleReportManagement() {
		try {
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, ScheduledReportsManagementHG.getClientErrors(request));
			request.setAttribute(WebConstants.REQ_SCHEDULED_REPORT_NAMES,
					ScheduledReportsManagementHG.getReportNamesFromReportMetaData(request));

		} catch (ModuleException moduleException) {
			log.error("cannot load MIS reports :" + moduleException);
			return S2Constants.Result.ERROR;
		}

		return S2Constants.Jsp.ScheduleReports.SCHE_RPT_MANAGEMENT;
	}

	public String displayDashboardMsgManagement() {
		try {
			DashboardMessageHG.getClientErrors(request);
			DashboardMessageHG.setDisplayData(request);
			request.setAttribute("msgTypeList", SelectListGenerator.createDashboardMsgTypes());

		} catch (Exception e) {
			log.error("cannot load Dashboard  Management:" + e);
			return S2Constants.Result.ERROR;
		}

		return S2Constants.Jsp.Tools.MESSAGE_MANAGEMENT_JSP;
	}

	public String displayAirportMessages() {
		try {
			AirportMessageHG.getClientErrors(request);
			AirportMessageHG.setDisplayData(request);

		} catch (Exception e) {
			log.error("Cannot Load Airport Message Management:" + e);
			return S2Constants.Result.ERROR;
		}

		return S2Constants.Jsp.Master.AIRPORT_MESSAGE_JSP;
	}
	
	public String displayAdvertisement() {
		try {
			// sets client error code need to load
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, ItineraryAdvertisementHG.getClientErrors(request));
			String strImgpath = AppSysParamsUtil.getItineraryAdvertisementUploadPath();
			request.setAttribute("reqAdvertisementImgPath", "advertisementImagePath = '" + strImgpath + "';");
			
			// Sets Advertisement Image Relate Parameters
			request.setAttribute("reqAdvertisementImgMaxHeight", AiradminModuleUtils.getConfig().getItineraryAdvertisementImagesMaxHeight());
			request.setAttribute("reqAdvertisementImgMinHeight", AiradminModuleUtils.getConfig().getItineraryAdvertisementImagesMinHeight());
			request.setAttribute("reqAdvertisementImgMaxWidth", AiradminModuleUtils.getConfig().getItineraryAdvertisementImagesMaxWidth());
			request.setAttribute("reqAdvertisementImgMinWidth", AiradminModuleUtils.getConfig().getItineraryAdvertisementImagesMinWidth());
			request.setAttribute("reqAdvertisementImgSize", AiradminModuleUtils.getConfig().getItineraryAdvertisementImagesSize());

			String strViaPointsList = SelectListGenerator.createActiveAirportCodeList();
			request.setAttribute(WebConstants.REQ_HTML_VIAPOINT_SELECT_LIST, strViaPointsList);
			
			String strHtml = SelectListGenerator.createLanguageList();
			request.setAttribute(WebConstants.REQ_HTML_LANGUAGE_LIST, strHtml);

		} catch (ModuleException moduleException) {
			log.error("cannot load Advertisement :" + moduleException);
			return S2Constants.Result.ERROR;
		}
		return S2Constants.Jsp.Master.ADVERTISEMENT_JSP;
	}

	public String updateOndListStatusPopup() {

		boolean updateStatus = false;

		try {
			updateStatus = ModuleServiceLocator.getLCCMasterDataPublisherBD().publishOndCombinationsViaLCC(
					AppSysParamsUtil.getCarrierCode());
		} catch (ModuleException e) {
			log.error("ond list updated failed:" + e);
		}

		request.setAttribute(WebConstants.REQ_OND_UPDATE_STATUS, updateStatus);

		return S2Constants.Jsp.Tools.UPDATE_OND_JSP;
	}

	public String loadFare360() {
		return S2Constants.Jsp.Master.FARE360_JSP;
	}

	public String loadFare360Data() {
		return S2Constants.Jsp.Master.FARE360DATA_JSP;
	}

	public String loadFare360Edit() {
		return S2Constants.Jsp.Master.FARE360EDIT_JSP;
	}

	public String loadSSRInfo() {

		try {

			String strImgpath = AppSysParamsUtil.getImageUploadPath();
			request.setAttribute("reqSSRImgPath", "ssrImagePath = '" + strImgpath + "';");

			String strHtml = SelectListGenerator.createLanguageList();
			request.setAttribute(WebConstants.REQ_HTML_LANGUAGE_LIST, strHtml);

			if (AppSysParamsUtil.isDisplayIBEListsDynamicallyEnabled()) {
				request.setAttribute(WebConstants.LIST_FOR_DISPLAY_ENABLED, "true");
			} else {
				request.setAttribute(WebConstants.LIST_FOR_DISPLAY_ENABLED, "false");
			}

			// sets client error code
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, SSRMessageHG.getSSRInfoClientErrors(request));

			// SSR Related
			// SSR Info Codes
			String strSSRCodeList = SelectListGenerator.createSSRCodeListAll();
			request.setAttribute(S2Constants.RequestConstant.SSR_CODE_ALL, strSSRCodeList);

			// SSR Category
			String strSSRCategoryList = SelectListGenerator.createSsrCategoryList();
			request.setAttribute(S2Constants.RequestConstant.SSR_CATEGORY, strSSRCategoryList);

			// SSR Sub Category
			// String strSSRSubCategoryList =
			// SelectListGenerator.createSsrSubCategoryList();
			String strSSRSubCategoryList = SSRInfoRH.generateSSRSubcategoryInfo();
			request.setAttribute(S2Constants.RequestConstant.SSR_SUB_CATEGORY, strSSRSubCategoryList);

			// Airport Multi Select
			String strList = SelectListGenerator.createAirportMultiSelect();
			request.setAttribute(S2Constants.RequestConstant.AIRPORT_MULTI_SELECT, strList);

			// NILI-RUMESH-CHECK Need to check this with Rumesh
			// This will use get the next sort order count when user adding new
			// service
			int totalAirportServiceCount = ModuleServiceLocator.getSsrServiceBD().getSSRCount(SSRCategory.ID_AIRPORT_SERVICE);
			request.setAttribute("airportServiceCount", totalAirportServiceCount);

			// This will use get the next sort order count when user adding new
			// service
			int totalInflightServiceCount = ModuleServiceLocator.getSsrServiceBD().getSSRCount(SSRCategory.ID_INFLIGHT_SERVICE);
			request.setAttribute("inflightServiceCount", totalInflightServiceCount);

			// Sets the Airport With Status to the Request
			String strHtmlAirports = SelectListGenerator.createAirportsWithStatusList();
			request.setAttribute(WebConstants.REQ_HTML_ACTIVE_AIRPORT_SELECT_LIST, strHtmlAirports);

			request.setAttribute(S2Constants.RequestConstant.SSR_INV_ENABLED, AppSysParamsUtil.isInventoryCheckForSSREnabled());
			request.setAttribute(S2Constants.RequestConstant.SSR_CUTOFF_TIME_ENABLED,
					AppSysParamsUtil.isMaintainSSRWiseCutoffTime());

		} catch (ModuleException moduleException) {
			log.error("cannot load ssr page Codes :" + moduleException);
			return S2Constants.Result.ERROR;
		} catch (JSONException je) {
			log.error("cannot load ssr page Codes :" + je);
			return S2Constants.Result.ERROR;
		}

		return S2Constants.Jsp.Master.SSR_INFO_LOAD_JSP;
	}

	public String loadSSRCharges() {
		try {
			// sets client error code
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, SSRMessageHG.getSSRChargesClientErrors(request));

			request.setAttribute(S2Constants.RequestConstant.ALL_COS_LIST,
					SelectListGenerator.createCabinClassAndLogicalCabinClassesList());

			// SSR Related
			// SSR Info Codes
			String strSSRCodeList = SelectListGenerator.createSSRCodeListAll();
			request.setAttribute(S2Constants.RequestConstant.SSR_CODE_ALL, strSSRCodeList);

			// SSR Category
			String strSSRCategoryList = SelectListGenerator.createSsrCategoryList();
			request.setAttribute(S2Constants.RequestConstant.SSR_CATEGORY, strSSRCategoryList);

			// sets the cabin class codes
			String strCabinCode = SelectListGenerator.createCabinClassList();
			request.setAttribute(S2Constants.RequestConstant.SSR_CC_CODE, strCabinCode);

			// SSR Sub Category
			String strSSRSubCategoryList = SelectListGenerator.createSsrSubCategoryList();
			request.setAttribute(S2Constants.RequestConstant.SSR_SUB_CATEGORY, strSSRSubCategoryList);

			request.setAttribute(S2Constants.RequestConstant.SSR_CAT_MAP, JSONUtil.serialize(SSRChargesRH.prepareSSRInfoMap()));

		} catch (ModuleException moduleException) {
			log.error("cannot load ssr charges codes :" + moduleException);
			return S2Constants.Result.ERROR;
		} catch (Exception e) {
			log.error("cannot load ssr charges codes :" + e);
			return S2Constants.Result.ERROR;
		}

		return S2Constants.Jsp.Master.SSR_CHARGES_LOAD_JSP;
	}

	public String loadSSRTemplate() {
		try {
			// sets client error code
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, SSRMessageHG.getSSRTemplateClientErrors(request));

			request.setAttribute(S2Constants.RequestConstant.ALL_COS_LIST,
					SelectListGenerator.createCabinClassAndLogicalCabinClassesList());

			// sets the cabin class codes
			String strCabinCode = SelectListGenerator.createCabinClassList();
			request.setAttribute(S2Constants.RequestConstant.SSR_CC_CODE, strCabinCode);

			// SSR template codes
			request.setAttribute(S2Constants.RequestConstant.SSR_TEMPLATE_ALL, SelectListGenerator.createSSRTemplateListAll());

			// SSR List
			request.setAttribute(S2Constants.RequestConstant.SSR_LIST, SSRMessageHG.setSSROptHTML());

		} catch (ModuleException me) {
			log.error("cannot load ssr template codes :" + me);
			return S2Constants.Result.ERROR;
		}

		return S2Constants.Jsp.Master.SSR_TEMPLATE_LOAD_JSP;
	}
	
	public String loadAutoCheckinTemplate() throws ModuleException {
		try {
			// sets client error code
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, AutoCheckinMessageHG.getACITemplateClientErrors(request));

			// Setting the Airport List
			request.setAttribute(S2Constants.RequestConstant.ACI_AIRPORT_LIST, SelectListGenerator.getAirportsList(false));

		} catch (ModuleException me) {
			log.error("cannot load Auto Checkin template codes :" + me);
			return S2Constants.Result.ERROR;
		}
		setAirportList(request);
		return S2Constants.Jsp.Master.AUTOMATIC_CHECKIN_TEMPLATE_LOAD_JSP;
	}
	
	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setAirportList(HttpServletRequest request) throws ModuleException {
		String strList = ReportsHTMLGenerator.getAirportListHtml();
		request.setAttribute(WebConstants.REQ_HTML_PAYMENTTYPE_LIST, strList);
	}

	public String loadAirportTransfer() {
		try {
			SSRMessageHG.setAirportTransferClientErrors(request);
			request.setAttribute(WebConstants.REQ_HTML_ACTIVE_AIRPORT_SELECT_LIST, SelectListGenerator.createActiveAirportList());
		} catch (Exception e) {
			log.error("Error while loading airport transfers ", e);
			return S2Constants.Result.ERROR;
		}
		return S2Constants.Jsp.Master.AIRPORT_TRANSFER_LOAD_JSP;
	}

	public String loadAirportTransferTemplate() {
		try {
			SSRMessageHG.setAirportTransferTemplateClientErrors(request);
			request.setAttribute(WebConstants.REQ_HTML_ACTIVE_AIRPORT_SELECT_LIST, SelectListGenerator.createActiveAirportList());
			request.setAttribute(S2Constants.RequestConstant.CABIN_CLASS_LIST, SelectListGenerator.createCabinClassList());
			request.setAttribute(WebConstants.SES_HTML_AIRCRAFTMODEL_LIST_DATA,
					SelectListGenerator.createActiveAircraftModelList_SG());
			request.setAttribute(WebConstants.REQ_HTML_FLIGHTNUMBER_LENGTH, AppSysParamsUtil.getMaximumFlightNumberLength()+2);
			request.setAttribute(WebConstants.REQ_HTML_FLIGHTNUMBER, JSONUtil.serialize(SelectListGenerator.getFlightNoList()));
			request.setAttribute(WebConstants.REQ_HTML_BOOKING_CLASS_LIST, AirportTransferHTMLGenerator.createBookingClassesHtml(null, true));
		} catch (Exception e) {
			log.error("Error while loading airport transfers Template", e);
			return S2Constants.Result.ERROR;
		}
		return S2Constants.Jsp.Master.AIRPORT_TRANSFER_TEMPLATE_LOAD_JSP;
	}

	public String loadPFSHistory() {

		return S2Constants.Jsp.Tools.PFS_HISTORY_JSP;
	}

	public String loadFlightSummary() throws ModuleException {

		FlightSummaryRequestHandler.setDisplayData(request);
		return S2Constants.Jsp.Tools.FLIGHT_SUMMARY_JSP;
	}

	public String loadConfigOnholdTimeLimits() {
		try {

			request.setAttribute(S2Constants.RequestConstant.ONLINE_AIRPORT_LIST,
					SelectListGenerator.createOnlineAirportListWOTag());

			request.setAttribute(S2Constants.RequestConstant.FLIGHT_TYPES_LIST, SelectListGenerator.createFlightTypeList(true));

			request.setAttribute(S2Constants.RequestConstant.AGENT_CODE_LIST, SelectListGenerator.createAgentCodes());

			request.setAttribute(S2Constants.RequestConstant.CABIN_CLASS_LIST, SelectListGenerator.createCabinClassList());

			request.setAttribute(S2Constants.RequestConstant.BOOKING_CODE_LIST,
					SelectListGenerator.createBookingClassCodesList(request));

			request.setAttribute(S2Constants.RequestConstant.ALL_BOOKING_CODES, SelectListGenerator.createBookingCodeList());

		} catch (ModuleException e) {
			log.error("cannot load config onhold time limits screen :" + e);
			return S2Constants.Result.ERROR;
		}
		return S2Constants.Jsp.Tools.CONFIG_TIME_LIMITS_JSP;
	}

	public String loadPromotionTemplatesManagement() {

		try {
			String strPromoTypes = SelectListGenerator.createPromotionsTypesTemplateList();
			request.setAttribute(S2Constants.RequestConstant.PROMOTION_TYPES_LIST, strPromoTypes);

			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, PromotionMessageHG.getTemplateClientErrors(request));

		} catch (ModuleException moduleException) {
			log.error("loadPromotionTemplatesManagement :", moduleException);
			return S2Constants.Result.ERROR;
		}

		return S2Constants.Jsp.Promotions.PROMO_TEMPLATES_MGMT_JSP;
	}

	public String loadPromotionRequestsManagement() {

		try {
			String cameFrom = null;
			String fltId = null;
			fltId = request.getParameter("fltId");
			if (fltId != null) {
				cameFrom = "ALLOC";
			}
			request.setAttribute("cameFrom", cameFrom);
			request.setAttribute("flID", fltId);
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, PromotionMessageHG.getTemplateClientErrors(request));

		} catch (ModuleException moduleException) {
			log.error("loadPromotionRequestsManagement :", moduleException);
			return S2Constants.Result.ERROR;
		}
		return S2Constants.Jsp.Promotions.PROMO_REQUESTS_MGMT_JSP;
	}

	public String loadPromotionRequestsSummary() {

		try {
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, PromotionMessageHG.getTemplateClientErrors(request));

		} catch (ModuleException moduleException) {
			log.error("loadPromotionRequestsSummary :", moduleException);
			return S2Constants.Result.ERROR;
		}
		return S2Constants.Jsp.Promotions.PROMO_REQUESTS_SMRY_JSP;
	}

	public String loadReprotect() throws ModuleException {
		request.setAttribute("hidFlightId", request.getParameter("hdnFlightId"));
		return S2Constants.Jsp.Flight.FLIGHT_REPROTECTION_JSP;
	}

	public String loadPromotionCodeManagement() {

		try {
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, PromotionMessageHG.getTemplateClientErrors(request));
			request.setAttribute(WebConstants.REQ_HTML_LANGUAGE_LIST, SelectListGenerator.createLanguageList());
		} catch (ModuleException moduleException) {
			log.error("loadPromotionRequestsSummary :", moduleException);
			return S2Constants.Result.ERROR;
		}
		return S2Constants.Jsp.Promotions.PROMO_CODE_MGMT_JSP;
	}

	public String loadVoucherTemplatesManagement() {
		try {
			String strVoucherTypes = SelectListGenerator
					.createVoucherTemplateList();
			request.setAttribute(
					S2Constants.RequestConstant.VOUCHER_TYPES_LIST,
					strVoucherTypes);
			
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES,
					PromotionMessageHG.getTemplateClientErrors(request));

		} catch (ModuleException moduleException) {
			log.error("loadVoucherTemplatesManagement :", moduleException);
			return S2Constants.Result.ERROR;
		}

		return S2Constants.Jsp.Promotions.VOU_TEMPLATES_MGMT_JSP;
	}

	public String loadGroupBookingRequest() {
		try {
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, PromotionMessageHG.getTemplateClientErrors(request));

		} catch (ModuleException moduleException) {
			log.error("loadPromotionRequestsSummary :", moduleException);
			return S2Constants.Result.ERROR;
		}
		return S2Constants.Jsp.Tools.GROUP_BOOKING_REQUEST;
	}

	public String loadTermsTemplateManagementPage() {
		try {
			request.setAttribute(WebConstants.REQ_HTML_LANGUAGE_LIST, SelectListGenerator.createLanguageList());
			request.setAttribute(WebConstants.REQ_HTML_TERMS_TEMPLATE_NAME_LIST, SelectListGenerator.getTermsTemplateNameList());
		} catch (ModuleException e) {
			log.error("loadTermsTemplateManagementPage :", e);
			return S2Constants.Result.ERROR;
		}
		return AdminStrutsConstants.AdminJSP.MANAGE_TERMS_TEMPLATES_JSP;
	}

	public String loadTermsTemplateEditPage() {
		return AdminStrutsConstants.AdminJSP.EDIT_TERMS_TEMPLATES_JSP;
	}
	
	public String loadVoucherTermsTemplateManagementPage() {
		try {
			request.setAttribute(WebConstants.REQ_HTML_LANGUAGE_LIST, SelectListGenerator.createLanguageList());
			request.setAttribute(WebConstants.REQ_HTML_VOUCHER_TERMS_TEMPLATE_NAME_LIST, SelectListGenerator.getVoucherTermsTemplateNameList());
		} catch (ModuleException e) {
			log.error("loadTermsTemplateManagementPage :", e);
			return S2Constants.Result.ERROR;
		}
		return AdminStrutsConstants.AdminJSP.MANAGE_VOUCHER_TERMS_TEMPLATES_JSP;
	}
	
	public String loadVoucherTermsTemplateEditPage() {
		return AdminStrutsConstants.AdminJSP.EDIT_VOUCHER_TERMS_TEMPLATES_JSP;
	}

	public String loadAnciOfferCriteriaManagement() {
		try {
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, PromotionMessageHG.getTemplateClientErrors(request));
			request.setAttribute(WebConstants.REQ_HTML_LOGICAL_CABIN_CLASS_LIST,
					SelectListGenerator.createLogicalCabinClassList());
			request.setAttribute(WebConstants.REQ_FLEXI_CODE_LIST, SelectListGenerator.createFlexiCodeHTMLList());
			request.setAttribute(WebConstants.REQ_HTML_BC_LIST,
					JSONUtil.serialize(SelectListGenerator.getOnlyBookingClassCodeList()));
			request.setAttribute(WebConstants.REQ_AIRPORT_LIST, SelectListGenerator.createActiveAirportCodeList());
			request.setAttribute(WebConstants.REQ_HTML_LANGUAGE_LIST, SelectListGenerator.createLanguageList());
			request.setAttribute(WebConstants.ANCI_OFFER_TEMPLATE_TYPES,
					JSONUtil.serialize(SelectListGenerator.getAnciOfferTypeNTemplates()));
		} catch (Exception moduleException) {
			log.error("loadAnciOfferCriteria :", moduleException);
			return S2Constants.Result.ERROR;
		}
		return S2Constants.Jsp.Promotions.ANCI_OFFER_MGMT_JSP;
	}

	public String loadBundledFaresManagement() {
		try {
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, PromotionMessageHG.getTemplateClientErrors(request));
			request.setAttribute(WebConstants.REQ_HTML_LANGUAGE_LIST, SelectListGenerator.createLanguageList());
			request.setAttribute(WebConstants.REQ_CHARGE_CODE_LIST, SelectListGenerator.createBundledFareChargeCodeList());
			request.setAttribute(WebConstants.SEAT_TEMPL_CODE, SelectListGenerator.createActiveSeatMapChargeTemplateList());
			request.setAttribute(WebConstants.MEAL_TEMPL_CODE, SelectListGenerator.createActiveMealTemplateList());
			request.setAttribute(WebConstants.REQ_HTML_BUNDLED_FARE_NAME_LIST, SelectListGenerator.createBundledFareNamesList());
			request.setAttribute(WebConstants.REQ_HTML_BUNDLED_CATEGORY_LIST, SelectListGenerator.createBundledCategoryList());
			request.setAttribute(WebConstants.REQ_HTML_BUNDLED_OND_LIST, SelectListGenerator.getBundledOndList());
			request.setAttribute(WebConstants.REQ_HTML_BUNDLED_BOOKING_CLASSES_LIST,
					SelectListGenerator.getBundledBookingClassesList());
			request.setAttribute(WebConstants.REQ_HTML_BUNDLED_STATUS_LIST, SelectListGenerator.getBundledStatusList());

			String strImgpath = AppSysParamsUtil.getImageUploadPath();
			request.setAttribute("reqImgPath", "var bundledFareImagePath = '" + strImgpath + "';");
			request.setAttribute("reqImagePrefix", "var imagePrefix = '" + BundledFareConstants.IMG_PREFIX + "';");
			request.setAttribute("reqImageStripePrefix", "var imageStripePrefix = '"
					+ BundledFareConstants.IMG_STRIPE_PREFIX + "';");

			List<SSR> activeAirportServices = ModuleServiceLocator.getSsrServiceBD().getActiveSSRs(null, null,
					SSRCategory.ID_AIRPORT_SERVICE);

			Map<String, String> activeAPSs = new HashMap<String, String>();
			if (activeAirportServices != null && !activeAirportServices.isEmpty()) {
				for (SSR airportService : activeAirportServices) {
					activeAPSs.put(Integer.toString(airportService.getSsrId()), airportService.getSsrName());
				}
			}

			request.setAttribute(S2Constants.RequestConstant.SSR_LIST, SelectListGenerator.createDropDown(activeAPSs));

			if (AppSysParamsUtil.isONDBaggaeEnabled()) {
				request.setAttribute(S2Constants.RequestConstant.BG_TEMPLATE_CODE,
						SelectListGenerator.createONDBaggageTemplateList());
			} else {
				request.setAttribute(S2Constants.RequestConstant.BG_TEMPLATE_CODE,
						SelectListGenerator.createBaggageTemplateList());
			}

		} catch (Exception moduleException) {
			log.error("loadAnciOfferCriteria :", moduleException);
			return S2Constants.Result.ERROR;
		}
		return S2Constants.Jsp.Promotions.BUNDLED_FARES_MGMT_JSP;
	}

	public String loadUserStationAssinment() {
		try {
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, PromotionMessageHG.getTemplateClientErrors(request));
			String strStationComboList = SelectListGenerator.createStationCodeList();
			request.setAttribute(WebConstants.REQ_HTML_AIRPORT_LIST, strStationComboList);
			String strUserIdList = SelectListGenerator.createUsersList();
			request.setAttribute(WebConstants.REQ_USER_ID_LIST, strUserIdList);

		} catch (ModuleException moduleException) {
			log.error("loadUserStationAssinmnet :", moduleException);
			return S2Constants.Result.ERROR;
		}
		return S2Constants.Jsp.Tools.USER_STATION_ASSIGNMENT;
	}

	public String loadGroupBookingStationAssinment() {
		try {
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, PromotionMessageHG.getTemplateClientErrors(request));
			String strStationComboList = SelectListGenerator.createStationCodeList();
			request.setAttribute(WebConstants.REQ_HTML_AIRPORT_LIST, strStationComboList);
			String strUserIdList = SelectListGenerator.createUsersList();
			request.setAttribute(WebConstants.REQ_USER_ID_LIST, strUserIdList);

		} catch (ModuleException moduleException) {
			log.error("loadUserStationAssinmnet :", moduleException);
			return S2Constants.Result.ERROR;
		}
		return S2Constants.Jsp.Tools.GROUP_BOOKING_STATION_ASSIGNMENT;
	}

	public String loadBSPHOTFileManagement() {
		try {

			String strBSPCountryCodes = SelectListGenerator.createBSPCountryList();
			request.setAttribute(S2Constants.RequestConstant.BSP_COUNTRY_LIST, strBSPCountryCodes);
			request.setAttribute(S2Constants.RequestConstant.BSP_TICKETING_AIRLINE_CODE, "var bspAirlineCode = '"
					+ AppSysParamsUtil.getBSPTicketingAirlineCode() + "';");
		} catch (ModuleException moduleException) {
			log.error("cannot load BSP HOT File Management :" + moduleException);
			return S2Constants.Result.ERROR;
		}

		return S2Constants.Jsp.Tools.BSPHOTFILE_LOAD_JSP;
	}

	/**
	 * Checks the Privileges of a User
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @param privilegeId
	 *            the Privilege needs to check
	 * @return true - if Privilege Exists
	 */
	@SuppressWarnings("unchecked")
	public static boolean hasPrivilege(HttpServletRequest request, String privilegeId) {

		Map<String, String> mapPrivileges = (Map<String, String>) request.getSession().getAttribute(
				WebConstants.SES_PRIVILEGE_IDS);
		boolean has = (mapPrivileges.get(privilegeId) != null) ? true : false;

		return has;
	}

	public String loadBlacklistPax() {
		try {
			
			String strNationalityList = SelectListGenerator.createNationalityList();;
			request.setAttribute(WebConstants.REQ_NATIONALITY_LIST, strNationalityList);
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, BlacklistPAXMessageHG.getTemplateClientErrors(request));

		} catch (ModuleException moduleException) {
			log.error("loadBlacklistPax :", moduleException);
			return S2Constants.Result.ERROR;
		}
		return S2Constants.Jsp.Tools.BLACKLIST_PAX_MANAGEMENT_JSP;
	}
	
	public String loadSearchRollForward() {
		log.info("loadSearchRollForward.....");
		try {
			String strStatusList = SelectListGenerator.createStatusList();;
			request.setAttribute(WebConstants.REQ_STATUS_LIST, strStatusList);
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, SearchRollforwardMessageHG.getTemplateClientErrors(request));
			request.setAttribute(WebConstants.REQ_HAS_ROLL_CANCLE_BATCH_PRIVILEGE,
					hasPrivilege(request, WebConstants.PLAN_ROLL_ADVANCE_CANCLE_BATCH));
		} catch (Exception moduleException) {
			log.error("loadSearchRollForward :", moduleException);
			return S2Constants.Result.ERROR;
		}
		return S2Constants.Jsp.Tools.SEARCH_ROLL_FORWARD_JSP;
	}

	public String OfficersNotificationsConfiguration() {
		request.setAttribute("reqIsMobileNumMandatory", AppSysParamsUtil.isOfficerContactInfoMobileNumMandatory());
		request.setAttribute("reqIsEmailMandatory", AppSysParamsUtil.isOfficerContactInfoEmailMandatory());
		return S2Constants.Jsp.Tools.OFFICER_NOTIFICATION_CONFIG;

	}
	public String ChangePAXandETStatus() throws ModuleException {
		
		ChangePAXandETStatusRequestHandler.setDisplayData(request);
		return S2Constants.Jsp.Tools.CHANGE_PAX_ET_STATUS;
	
	}
	public String AgentUserRolesReport() throws ModuleException {
		
		String strActPrivileges = SelectListGenerator.getActPrivilegesList();
		request.setAttribute(S2Constants.RequestConstant.ACT_PRIVILEGE_DATA, strActPrivileges);
		
		return S2Constants.Jsp.Tools.AGENT_USER_ROLES;
	}
	
	public String displayState() {
		try {
			// sets client error code need to load
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, StateHG.getClientErrors(request));
			
			String strHtml = SelectListGenerator.createCountryList();
			request.setAttribute(WebConstants.REQ_COUNTRY_LIST, strHtml);

		} catch (ModuleException moduleException) {
			log.error("cannot load State :" + moduleException);
			return S2Constants.Result.ERROR;
		}
		return S2Constants.Jsp.Master.STATE_ADMIN_JSP;
	}
	
	public String displayCity() {
		try {
			// sets client error code need to load
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, CityHG.getClientErrors(request));
			
			String strCountryHtml = SelectListGenerator.createCountryList();
			request.setAttribute(WebConstants.REQ_COUNTRY_LIST, strCountryHtml);
			
			String strCityCountryHtml = SelectListGenerator.getCityCodeNameCountryCodeList();
			request.setAttribute(WebConstants.REQ_CITY_COUNTRY_LIST, strCityCountryHtml);
			
			String strLangHtml = SelectListGenerator.createLanguageList();
			request.setAttribute(WebConstants.REQ_HTML_LANGUAGE_LIST, strLangHtml);
			
			if (AppSysParamsUtil.isDisplayIBEListsDynamicallyEnabled()) {
				request.setAttribute(WebConstants.LIST_FOR_DISPLAY_ENABLED, "true");
			} else {
				request.setAttribute(WebConstants.LIST_FOR_DISPLAY_ENABLED, "false");
			}
			
			boolean isLCCenabled = AppSysParamsUtil.isLCCConnectivityEnabled();
			request.setAttribute(WebConstants.REQ_HTML_LCC_ENABLE_STATUS, isLCCenabled);

		} catch (ModuleException moduleException) {
			log.error("cannot load City :" + moduleException);
			return S2Constants.Result.ERROR;
		}
		return AdminStrutsConstants.AdminJSP.CITY_ADMIN_JSP;
	}
}
