package com.isa.thinair.airadmin.core.web.v2.action.mis;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.v2.dto.mis.MISProcessParams;
import com.isa.thinair.airadmin.core.web.v2.handler.mis.MISEmailRH;
import com.isa.thinair.airadmin.core.web.v2.util.CommonUtil;
import com.isa.thinair.commons.core.util.AccelAeroCalculator;
import com.isa.thinair.reporting.api.criteria.MISReportsSearchCriteria;
import com.isa.thinair.reporting.api.model.AgentPerformanceTO;
import com.isa.thinair.reporting.api.model.RegionalProductSalesTO;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Result(name = AdminStrutsConstants.AdminAction.SUCCESS, type = JSONResult.class, value = "")
public class ShowMISRegionalAction extends BaseRequestResponseAwareAction {

	private static Log log = LogFactory.getLog(ShowMISRegionalAction.class);
	private boolean success = true;
	private String messageTxt;
	private String returnData = "";
	private MISReportsSearchCriteria misSearchCriteria;

	private Map<String, Map<String, String>> regionalSalesTrend;
	private Map<String, Long> regionYearlySales;
	private String yearlyHighestValue = "";
	private String monthlyHighestValue = "";
	private String sixMonths = "";
	private Collection<String> regions;
	private String avgAgentsSales = "";
	private Collection<AgentPerformanceTO> bestPerformingAgents;
	private Collection<AgentPerformanceTO> worstPerformingAgents;
	private Map<String, Map<String, RegionalProductSalesTO>> regionalProductSales;
	private String totalSales;

	public String execute() throws Exception {
		String strForward = WebConstants.FORWARD_SUCCESS;
		MISProcessParams mpParams = new MISProcessParams(request);
		request.setAttribute("req_InitialParams", CommonUtil.convertToJSON(mpParams));
		try {
			misSearchCriteria = new MISReportsSearchCriteria();
			misSearchCriteria.setFromDate(request.getParameter("fromDate"));
			misSearchCriteria.setToDate(request.getParameter("toDate"));
			if (!"ALL".equalsIgnoreCase(request.getParameter("region"))) {
				misSearchCriteria.setRegion(request.getParameter("region"));
			}
			if (!"ALL".equalsIgnoreCase(request.getParameter("pos"))) {
				misSearchCriteria.setPos(request.getParameter("pos"));
			}

			// Populates values to be shown in the UI
			regionalSalesTrend = getMISRegionalSalesTrend();
			regionYearlySales = getMISRegionYearlySales();
			bestPerformingAgents = getBestPerformingAgentsData();
			worstPerformingAgents = getLowPerformingAgentsData();
			regionalProductSales = getRegionalProductSalesData();
			avgAgentsSales = getAgentAverageSales();

		} catch (Exception e) {
			log.error("Generic exception caught.", e);
			success = false;
			messageTxt = e.getMessage();
		}
		return strForward;
	}

	/**
	 * Returns past six months rolling sales trend for all the regions
	 * 
	 * @return
	 */
	private Map<String, Map<String, String>> getMISRegionalSalesTrend() {

		Map<String, String> monthNames = CommonUtil.getMonthNames();
		Map<String, String> salesByRegion = new TreeMap<String, String>();
		Map<String, Map<String, String>> salesTrend = ModuleServiceLocator.getMISDataProviderBD().getMISRegionRollingSalesTrend(
				misSearchCriteria);
		Map<String, Map<String, String>> salesTrendSorted = new LinkedHashMap<String, Map<String, String>>();

		for (Map<String, String> salesByMonMap : salesTrend.values()) {
			for (String region : salesByMonMap.keySet()) {
				salesByRegion.put(region, null); // put all regions
			}
		}

		for (Map<String, String> salesByMonMap : salesTrend.values()) {
			for (String region : salesByRegion.keySet()) {
				if (!salesByMonMap.containsKey(region)) {
					salesByMonMap.put(region, "0");
				}
			}
		}
		for (String monthYearName : salesTrend.keySet()) {
			String year = monthYearName.substring(0, monthYearName.indexOf("*"));
			String mon = monthYearName.substring(monthYearName.indexOf("*") + 1);
			String newKey = monthNames.get(mon) + "-" + year;
			salesTrendSorted.put(newKey, salesTrend.get(monthYearName));
		}
		this.regions = salesByRegion.keySet();
		return salesTrendSorted;
	}

	/**
	 * Returns yearly sales for past 3 years.
	 * 
	 * @return
	 */
	private Map<String, Long> getMISRegionYearlySales() {
		Long highestVal = new Long(0);
		Map<String, Long> yearlySales = ModuleServiceLocator.getMISDataProviderBD().getMisRegionYearlySales(misSearchCriteria);
		for (String year : yearlySales.keySet()) {
			if (yearlySales.get(year).compareTo(highestVal) > 0) {
				highestVal = yearlySales.get(year).longValue();
			}
		}
		highestVal += highestVal / 100; // Add 10% of the highest value
		this.yearlyHighestValue = String.valueOf(highestVal);
		return yearlySales;
	}

	/**
	 * 
	 * @return
	 */
	private Collection<AgentPerformanceTO> getBestPerformingAgentsData() {
		return ModuleServiceLocator.getMISDataProviderBD().getMISRegionalBestPerformingAgents(misSearchCriteria);
	}

	private Collection<AgentPerformanceTO> getLowPerformingAgentsData() {
		return ModuleServiceLocator.getMISDataProviderBD().getMISRegionalLowPerformingAgents(misSearchCriteria);
	}

	private Map<String, Map<String, RegionalProductSalesTO>> getRegionalProductSalesData() {
		Collection<AgentPerformanceTO> regionalProductSalesCol = ModuleServiceLocator.getMISDataProviderBD()
				.getRegionalProductSalesData(misSearchCriteria);
		Map<String, Map<String, RegionalProductSalesTO>> regionalProductSalesMap = new TreeMap<String, Map<String, RegionalProductSalesTO>>();
		BigDecimal totalSales = AccelAeroCalculator.getDefaultBigDecimalZero();
		for (AgentPerformanceTO agentPerformanceTO : regionalProductSalesCol) {
			AgentPerformanceTO agentTO = agentPerformanceTO;
			agentTO.setTarget("0");
			agentTO.setYtdSales("0");
			agentTO.setYtdSalesVariance("0%");
			if (regionalProductSalesMap.containsKey(agentPerformanceTO.getRegionName())) {// This region is already in
																							// the map
				Map<String, RegionalProductSalesTO> regionWiseSales = regionalProductSalesMap.get(agentTO.getRegionName());

				// Check whether this location is already there in the the regional sales collection
				if (regionWiseSales.containsKey(agentTO.getAgentLocation())) {
					RegionalProductSalesTO regionalProductSalesTO = regionWiseSales.get(agentTO.getAgentLocation());
					Map<String, Collection<AgentPerformanceTO>> agentLocation = regionWiseSales.get(agentTO.getAgentLocation())
							.getAgentsRegionalSalesLocationWise();
					if (agentLocation.containsKey(agentTO.getAgentLocation())) {// There are other agents in this
																				// location
						agentLocation.get(agentTO.getAgentLocation()).add(agentTO);
					} else { // There are no other agents in this location
						Collection<AgentPerformanceTO> agentsList = new ArrayList<AgentPerformanceTO>();
						agentsList.add(agentTO);
						regionalProductSalesTO.getAgentsRegionalSalesLocationWise().put(agentTO.getAgentLocation(), agentsList);
					}
					// update the total actual sales for the region's location.
					regionalProductSalesTO.setActualSales(AccelAeroCalculator.add(regionalProductSalesTO.getActualSales(),
							agentTO.getSalesValue()));
					totalSales = AccelAeroCalculator.add(totalSales, agentTO.getSalesValue());
				} else { // A new agent in the same region

					Collection<AgentPerformanceTO> agentsTOList = new ArrayList<AgentPerformanceTO>();
					agentsTOList.add(agentTO);
					RegionalProductSalesTO regionalProductSalesTO = new RegionalProductSalesTO();
					regionalProductSalesTO.setLocation(agentTO.getAgentLocation());
					regionalProductSalesTO.setRegionName(agentTO.getRegionName());
					regionalProductSalesTO.setActualSales(agentTO.getSalesValue());
					regionalProductSalesTO.setTarget("0");
					regionalProductSalesTO.setYtdSales("0");
					regionalProductSalesTO.setYtdVariance("0%");
					Map<String, Collection<AgentPerformanceTO>> agentSalesLocationwiseMap = new TreeMap<String, Collection<AgentPerformanceTO>>();
					agentSalesLocationwiseMap.put(agentTO.getAgentLocation(), agentsTOList);
					regionalProductSalesTO.setAgentsRegionalSalesLocationWise(agentSalesLocationwiseMap);
					regionWiseSales.put(agentTO.getAgentLocation(), regionalProductSalesTO);
					totalSales = AccelAeroCalculator.add(totalSales, agentTO.getSalesValue());
				}
			} else { // This region is not in the map yet.
				Collection<AgentPerformanceTO> agentsTOList = new ArrayList<AgentPerformanceTO>();
				agentsTOList.add(agentTO);
				RegionalProductSalesTO regionalProductSalesTO = new RegionalProductSalesTO();
				regionalProductSalesTO.setLocation(agentTO.getAgentLocation());
				regionalProductSalesTO.setRegionName(agentTO.getRegionName());
				regionalProductSalesTO.setActualSales(agentTO.getSalesValue());
				regionalProductSalesTO.setTarget("0");
				regionalProductSalesTO.setYtdSales("0");
				regionalProductSalesTO.setYtdVariance("0%");
				Map<String, Collection<AgentPerformanceTO>> agentSalesLocationwiseMap = new TreeMap<String, Collection<AgentPerformanceTO>>();
				agentSalesLocationwiseMap.put(agentTO.getAgentLocation(), agentsTOList);
				regionalProductSalesTO.setAgentsRegionalSalesLocationWise(agentSalesLocationwiseMap);
				totalSales = AccelAeroCalculator.add(totalSales, agentTO.getSalesValue());

				Map<String, RegionalProductSalesTO> regionalSalesLocationwiseMap = new TreeMap<String, RegionalProductSalesTO>();
				regionalSalesLocationwiseMap.put(agentTO.getAgentLocation(), regionalProductSalesTO);
				regionalProductSalesMap.put(agentTO.getRegionName(), regionalSalesLocationwiseMap);

			}

		}
		this.totalSales = AccelAeroCalculator.formatAsDecimal(totalSales);
		// Format sales amount as decimal values
		for (String region : regionalProductSalesMap.keySet()) {
			Map<String, RegionalProductSalesTO> regionalSales = regionalProductSalesMap.get(region);
			for (String station : regionalSales.keySet()) {
				RegionalProductSalesTO locationData = regionalSales.get(station);
				locationData.setStrActualSales(AccelAeroCalculator.formatAsDecimal(locationData.getActualSales()));

				Map<String, Collection<AgentPerformanceTO>> agentSales = locationData.getAgentsRegionalSalesLocationWise();
				for (String agentLocation : agentSales.keySet()) {
					Collection<AgentPerformanceTO> agents = agentSales.get(agentLocation);
					for (AgentPerformanceTO agent : agents) {

						agent.setStrActualSales(AccelAeroCalculator.formatAsDecimal(agent.getSalesValue()));

					}
				}
			}

		}
		return regionalProductSalesMap;
	}

	public String sendEmail() {
		String strForward = WebConstants.FORWARD_SUCCESS;
		String emailID = request.getParameter("emailID");
		String feedBackContents = request.getParameter("comments");
		misSearchCriteria = new MISReportsSearchCriteria();
		misSearchCriteria.setFromDate(request.getParameter("fromDate"));
		misSearchCriteria.setToDate(request.getParameter("toDate"));
		if (!"ALL".equalsIgnoreCase(request.getParameter("region"))) {
			misSearchCriteria.setRegion(request.getParameter("region"));
		}
		if (!"ALL".equalsIgnoreCase(request.getParameter("pos"))) {
			misSearchCriteria.setPos(request.getParameter("pos"));
		}

		regionalProductSales = getRegionalProductSalesData();
		MISEmailRH.sendRegionalProductSalesEmail(emailID, feedBackContents, this.regionalProductSales);
		return strForward;
	}

	public String getAgentAverageSales() {

		Long avgSales = ModuleServiceLocator.getMISDataProviderBD().getAgentAverageSales(misSearchCriteria);
		return avgSales.toString();
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public String getReturnData() {
		return returnData;
	}

	public void setReturnData(String returnData) {
		this.returnData = returnData;
	}

	public MISReportsSearchCriteria getMisSearchCriteria() {
		return misSearchCriteria;
	}

	public void setMisSearchCriteria(MISReportsSearchCriteria misSearchCriteria) {
		this.misSearchCriteria = misSearchCriteria;
	}

	public Map<String, Map<String, String>> getRegionalSalesTrend() {
		return regionalSalesTrend;
	}

	public void setRegionalSalesTrend(Map<String, Map<String, String>> regionalSalesTrend) {
		this.regionalSalesTrend = regionalSalesTrend;
	}

	public Map<String, Long> getRegionYearlySales() {
		return regionYearlySales;
	}

	public void setRegionYearlySales(Map<String, Long> regionYearlySales) {
		this.regionYearlySales = regionYearlySales;
	}

	public String getYearlyHighestValue() {
		return yearlyHighestValue;
	}

	public void setYearlyHighestValue(String yearlyHighestValue) {
		this.yearlyHighestValue = yearlyHighestValue;
	}

	public String getMonthlyHighestValue() {
		return monthlyHighestValue;
	}

	public void setMonthlyHighestValue(String monthlyHighestValue) {
		this.monthlyHighestValue = monthlyHighestValue;
	}

	public String getSixMonths() {
		return sixMonths;
	}

	public Collection<String> getRegions() {
		return regions;
	}

	public void setSixMonths(String sixMonths) {
		this.sixMonths = sixMonths;
	}

	public void setRegions(Collection<String> regions) {
		this.regions = regions;
	}

	public String getAvgAgentsSales() {
		return avgAgentsSales;
	}

	public void setAvgAgentsSales(String avgAgentsSales) {
		this.avgAgentsSales = avgAgentsSales;
	}

	public Collection<AgentPerformanceTO> getBestPerformingAgents() {
		return bestPerformingAgents;
	}

	public void setBestPerformingAgents(Collection<AgentPerformanceTO> bestPerformingAgents) {
		this.bestPerformingAgents = bestPerformingAgents;
	}

	public Collection<AgentPerformanceTO> getWorstPerformingAgents() {
		return worstPerformingAgents;
	}

	public void setWorstPerformingAgents(Collection<AgentPerformanceTO> worstPerformingAgents) {
		this.worstPerformingAgents = worstPerformingAgents;
	}

	public Map<String, Map<String, RegionalProductSalesTO>> getRegionalProductSales() {
		return regionalProductSales;
	}

	public void setRegionalProductSales(Map<String, Map<String, RegionalProductSalesTO>> regionalProductSales) {
		this.regionalProductSales = regionalProductSales;
	}

	public String getTotalSales() {
		return totalSales;
	}

	public void setTotalSales(String totalSales) {
		this.totalSales = totalSales;
	}

}
