package com.isa.thinair.airadmin.core.web.v2.action.promotion;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airpricing.api.model.ChargeTemplate;
import com.isa.thinair.airpricing.api.model.MealTemplate;
import com.isa.thinair.airpricing.api.model.ONDBaggageTemplate;
import com.isa.thinair.commons.core.util.CalendarUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;


@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class FindBundledServiceTemplateAction extends BaseRequestAwareAction {

	private Log log = LogFactory.getLog(FindBundledServiceTemplateAction.class);

	private static final String SERVICE_NAME_BAGGAGE = "BAGGAGE";

	private static final String SERVICE_NAME_MEAL = "MEAL";

	private static final String SERVICE_NAME_SEAT = "SEAT";

	private Integer templateId;
	private String fromDate;
	private String toDate;
	private String templateName;
	private String serviceType;
	private boolean success = true;
	private String messageTxt;

	public String search() {

		try {

			if (SERVICE_NAME_BAGGAGE.equals(serviceType)) {
				ONDBaggageTemplate template = ModuleServiceLocator.getChargeBD().getONDBaggageTemplate(templateId);
				if(template != null) {
					this.templateName = template.getTemplateCode();
					this.fromDate = CalendarUtil.formatDate(template.getFromDate(), CalendarUtil.PATTERN1);
					this.toDate = CalendarUtil.formatDate(template.getToDate(), CalendarUtil.PATTERN1);
				} else {
					success = false;
					messageTxt = "No OnD baggage template found for the id '" + templateId + "'";
				}
			} else if (SERVICE_NAME_MEAL.equals(serviceType)) {
				MealTemplate mealTemplate = ModuleServiceLocator.getChargeBD().getMealTemplate(templateId);
				if(mealTemplate != null) {
					this.templateName = mealTemplate.getTemplateCode();
				} else {
					success = false;
					messageTxt = "No meal template found for the id '" + templateId + "'";
				}
			} else if (SERVICE_NAME_SEAT.equals(serviceType)) {
				ChargeTemplate chargeTemplate = ModuleServiceLocator.getChargeBD().getChargeTemplate(templateId);
				if(chargeTemplate != null) {
					this.templateName = chargeTemplate.getTemplateCode();
				} else {
					success = false;
					messageTxt = "No seat template found for the id '" + templateId + "'";
				}
			}

		} catch (Exception x) {

			success = false;
			messageTxt = "Failed to search bundled service template.";
			log.error(messageTxt, x);
		}

		return S2Constants.Result.SUCCESS;
	}

	public Integer getTemplateId() {
		return templateId;
	}

	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}
}
