package com.isa.thinair.airadmin.core.web.handler.reports;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

public class BookedSSRDetailsReportRH extends BasicRequestHandler {

	private static GlobalConfig globalConfig = AiradminModuleUtils.getGlobalConfig();

	private static Log log = LogFactory.getLog(BookedSSRDetailsReportRH.class);

	private static final String NOREC_STATUS = "NOREC";
	private static final String FLOWN_STATUS = "FLOWN";
	private static final String NOSHOW_STATUS = "NOSHOW";
	private static final String CONFIRM_STATUS = "CONFIRM";
	private static final String GOSHOW_STATUS = "GOSHOW";

	/**
	 * The constructor.
	 */
	public BookedSSRDetailsReportRH() {
		super();
	}

	/**
	 * @param request
	 * @return
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {
		String strHdnMode = request.getParameter("hdnMode");
		String forward = WebConstants.ACTION_FORWARD_SUCCESS;
		try {
			setDisplayData(request);
			log.debug("BookedSSRDetailsReportRH success");

		} catch (Exception e) {
			forward = AdminStrutsConstants.AdminAction.ERROR;
			log.error("BookedSSRDetailsReportRH execute()" + e.getMessage());
		}

		boolean isJapserReport = true;

		try {

			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportViewAll(request, response, isJapserReport);
				forward = AdminStrutsConstants.AdminAction.RES_SUCCESS;
				return null;

			} else {
				log.debug("BookedSSRDetailsReportRH setReportView not selected");
			}

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("BookedSSRDetailsReportRH setReportView Failed " + e.getMessageString());
		} catch (Exception e) {
			log.error("BookedSSRDetailsReportRH setReportView Failed ", e);
		}

		return forward;
	}

	/**
	 * 
	 * @param request
	 * @throws Exception
	 */
	public static void setDisplayData(HttpServletRequest request) throws Exception {

		setClientErrors(request);
		setAirportList(request);
		setSSRCodeList(request);
		setLiveStatus(request);
		setReportingPeriod(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);
		setServiceCategory(request);
		setSSRCodesWithCategory(request);
		setPaxStatus(request);
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	public static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString(), moduleException);
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	protected static void setReportViewAll(HttpServletRequest request, HttpServletResponse response, boolean isJapserReport)
			throws ModuleException {

		try {

			String fromDate = request.getParameter("txtFromDate");
			String toDate = request.getParameter("txtToDate");

			String bookedFromDate = request.getParameter("txtBookedFromDate");
			String bookedToDate = request.getParameter("txtBookedToDate");

			User curUser = (User) request.getSession().getAttribute("sesCurrUser");
			String strUsrerId = curUser.getUserId();

			String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);

			String flightNo = request.getParameter("txtFlightNumber");
			String ssrCodes = request.getParameter("hdnSsrCodes");

			String segments = request.getParameter("hdnSegments");

			String serviceCategory = request.getParameter("selCategory");
			String reportType = request.getParameter("selReportType");

			// To provide Report Format Options
			String strReportFormat = request.getParameter("radRptNumFormat");
			
			String pfsStatusList = request.getParameter("hdnPfsStatus");

			ReportsSearchCriteria search = new ReportsSearchCriteria();

			if (fromDate != null && !fromDate.equals("")) {
				search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate) + " 00:00:00");
			}

			if (toDate != null && !toDate.equals("")) {
				search.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate) + " 23:59:59");
			}

			if ((bookedFromDate != null && !bookedFromDate.isEmpty()) && (bookedToDate != null && !bookedToDate.isEmpty())) {
				search.setDateRange2From(ReportsHTMLGenerator.convertDate(bookedFromDate) + " 00:00:00");
				search.setDateRange2To(ReportsHTMLGenerator.convertDate(bookedToDate) + " 23:59:59");
			}

			if (strUsrerId != null) {
				search.setUserId(strUsrerId);
			}

			if (strReportFormat != null && !strReportFormat.equals("")) {
				if (strReportFormat.equalsIgnoreCase("IT")) {
					search.setReqReportFormat("FR");
				} else
					search.setReqReportFormat("");
			}

			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}
			if (flightNo != null && !flightNo.equals("")) {
				search.setFlightNumber(flightNo);
			}
			if (ssrCodes != null && !ssrCodes.equals("")) {
				String ssrCodesArr[] = ssrCodes.split(",");
				ArrayList<String> ssrCodesCol = new ArrayList<String>();
				for (String ssr : ssrCodesArr) {
					ssrCodesCol.add(ssr);
				}
				search.setSsrCodes(ssrCodesCol);
			}
			// AAN/ALA,AIR/ADE/ATZ/AMS
			if (segments != null && !segments.equals("")) {
				String segmentsArr[] = segments.split(",");
				ArrayList<String> segmentsCol = new ArrayList<String>();
				for (String seg : segmentsArr) {
					segmentsCol.add(seg);
				}
				// need to add new param if required
				search.setSegmentCodes(segmentsCol);
			}
			
			if (pfsStatusList != null && !pfsStatusList.equals("")) {
				String pfsStatusArr[] = pfsStatusList.split(",");
				ArrayList<String> pfsStatusCol = new ArrayList<String>();
				for (String pfsStatus : pfsStatusArr) {
					pfsStatusCol.add(pfsStatus);
				}
				search.setPaxStatusList(pfsStatusCol);
			}

			if (serviceCategory != null && !serviceCategory.equals("")) {
				search.setSsrCategory(serviceCategory);
			}

			if (isJapserReport) {
				// setting the parameter to execute the cursor to return a resultset.
				search.setIsCurSerRequired(ReportsSearchCriteria.IS_CUR_REQUIRED_Y);
				// setting to execute in US format only because PDF/HTML are only genegerated in US
				search.setReqReportFormat("");
			}

			boolean isIncludeCancelledPnr = ModuleServiceLocator.getGlobalConfig().isIncludeCancelledSSRDetailsInReport();
			search.setIncludeCancelledDataToReport(isIncludeCancelledPnr);
			
			ResultSet rs = null;
			if (reportType != null && reportType.equals(ReportsSearchCriteria.SSR_DETAIL_REPORT)) {
				rs = ModuleServiceLocator.getDataExtractionBD().getBookedSSRDetailDataforReports(search);
			} else {
				rs = ModuleServiceLocator.getDataExtractionBD().getBookedSSRSummaryDataforReports(search);
			}

			viewJasperReport(request, response, rs, search, fromDate, toDate);

		} catch (Exception e) {
			log.error("download method is failed :", e);
			throw new ModuleException("Error in report data retrieval");
		}
	}

	private static void setAirportList(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createAirportCodeList();
		request.setAttribute(WebConstants.REQ_HTML_AIRPORT_LIST, strHtml);
	}

	private static void setSSRCodeList(HttpServletRequest request) throws ModuleException {
		String strHtml = JavascriptGenerator.createSSRCodesHtml();
		request.setAttribute(WebConstants.REQ_HTML_SSR_CODE_LIST, strHtml);
	}

	private static void setSSRCodesWithCategory(HttpServletRequest request) throws ModuleException {

		List list = SelectListGenerator.createSSRCodeListWithCategory();

		Iterator itr = list.iterator();
		StringBuilder sb = new StringBuilder();
		while (itr.hasNext()) {
			Map codeMap = (Map) itr.next();

			sb.append(codeMap.get("SSR_CODE"));
			sb.append("|");
			sb.append(codeMap.get("SSR_CAT_ID"));
			sb.append(",");
		}

		request.setAttribute(WebConstants.REQ_SSR_CODE_WITH_CATEGORY, sb.toString());
	}

	/**
	 * Setting the reporting period to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static void setReportingPeriod(HttpServletRequest request) throws ModuleException {
		int period = new Integer(AiradminModuleUtils.getGlobalConfig().getBizParam(SystemParamKeys.REPORTING_PERIOD)).intValue();
		request.setAttribute(WebConstants.REP_RPT_START_DAY, ReportsHTMLGenerator.getReportingPeriod(period));
	}

	/**
	 * Sets the report medium to the request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleExceptions
	 */
	private static void setLiveStatus(HttpServletRequest request) throws ModuleException {
		String strLive = request.getParameter(WebConstants.REP_HDN_LIVE);
		if (strLive != null)
			request.setAttribute(WebConstants.REP_SET_LIVE, strLive);

	}

	private static void setServiceCategory(HttpServletRequest request) throws ModuleException {
		String strHtml = SelectListGenerator.createSsrCategoryList();
		request.setAttribute(WebConstants.REQ_HTML_SSR_CATEGORY_LIST, strHtml);
	}

	private static void viewJasperReport(HttpServletRequest request, HttpServletResponse response, ResultSet resultSet,
			ReportsSearchCriteria search, String fromDate, String toDate) throws ModuleException {
		String value = request.getParameter("radReportOption");
		String strLogo = AppSysParamsUtil.getReportLogo(false);
		String strPath = "../../images/" + AppSysParamsUtil.getReportLogo(true); 
		String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";
		String strBase = globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY);

		String strReportId = "UC_REPM_078";
		String reportTemplate = null;
		String reportName = null;
		String serviceCategory = request.getParameter("selCategory");
		String reportType = request.getParameter("selReportType");

		if (reportType != null && reportType.equals(ReportsSearchCriteria.SSR_DETAIL_REPORT)) {

			reportTemplate = "BookedSSRDetailsReport.jasper";

			if (serviceCategory != null && serviceCategory.equals(ReportsSearchCriteria.SSR_SERVICE_CAT)) {
				reportName = "BookedSSRDetails";
			} if(serviceCategory != null && serviceCategory.equals(ReportsSearchCriteria.AIRPORT_TRANSFER_CAT)){
				reportName = "BookedAirportTransferDetails";
			} else {
				reportName = "BookedAirportServicesDetails";
			}

		} else {
			reportTemplate = "BookedSSRSummaryReport.jasper";

			if (serviceCategory != null && serviceCategory.equals(ReportsSearchCriteria.SSR_SERVICE_CAT)) {
				reportName = "BookedSSRSummary";
			} if(serviceCategory != null && serviceCategory.equals(ReportsSearchCriteria.AIRPORT_TRANSFER_CAT)){
				reportName = "BookedAirportTransferSummary";
			} else {
				reportName = "BookedAirportServicesSummary";
			}
		}

		ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs(WebConstants.REPORT_REF,
				ReportsHTMLGenerator.getReportTemplate(reportTemplate));

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("REPORT_ID", strReportId);
		parameters.put("FROM_DATE", fromDate);
		parameters.put("SSR_CAT", serviceCategory);
		parameters.put("TO_DATE", toDate);
		parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
		parameters.put("CURRENCY", "(" + strBase + ")");

		String reportNumFormat = request.getParameter("radRptNumFormat");
		ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

		if (value.trim().equals(WebConstants.REPORT_HTML)) {
			parameters.put("IMG", strPath);
			ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport(WebConstants.REPORT_REF, parameters, resultSet, null,
					null, response);
		} else if (value.trim().equals(WebConstants.REPORT_PDF)) {
			strPath = ReportsHTMLGenerator.getReportTemplate(strLogo);
			parameters.put("IMG", strPath);
			response.reset();
			response.addHeader("Content-Disposition", String.format("attachment;filename=%s.pdf", reportName));
			ModuleServiceLocator.getReportingFrameworkBD().createPDFReport(WebConstants.REPORT_REF, parameters, resultSet,
					response);
		} else if (value.trim().equals(WebConstants.REPORT_CSV)) {
			response.reset();
			response.addHeader("Content-Disposition", String.format("attachment;filename=%s.csv", reportName));
			ModuleServiceLocator.getReportingFrameworkBD().createCSVReport(WebConstants.REPORT_REF, parameters, resultSet,
					response);
		} else if (value.trim().equals(WebConstants.REPORT_EXCEL)) {
			response.reset();
			response.addHeader("Content-Disposition", String.format("attachment;filename=%s.xls", reportName));
			ModuleServiceLocator.getReportingFrameworkBD().createXLSReport(WebConstants.REPORT_REF, parameters, resultSet,
					response);
		}
	}

	private static void setPaxStatus(HttpServletRequest request) throws ModuleException {
		String strHtml = createPFSStatusList();
		request.setAttribute(WebConstants.REQ_PFS_STATUS_LIST, strHtml);
	}

	private static String createPFSStatusList() throws ModuleException {

		StringBuilder jsb = new StringBuilder("var arrGroup1 = new Array();");
		jsb.append(" arrGroup1[0] = '' ; var arrData = new Array();");
		jsb.append(" var pfsStatus = new Array();");

		Map<String, String> paxStatus = new HashMap<String, String>();
		paxStatus.put(ReservationInternalConstants.PfsPaxStatus.FLOWN, FLOWN_STATUS);
		paxStatus.put(ReservationInternalConstants.PfsPaxStatus.CONFIRMED, CONFIRM_STATUS);
		paxStatus.put(ReservationInternalConstants.PfsPaxStatus.NO_SHORE, NOSHOW_STATUS);
		paxStatus.put(ReservationInternalConstants.PfsPaxStatus.NO_REC, NOREC_STATUS);
		paxStatus.put(ReservationInternalConstants.PfsPaxStatus.GO_SHORE, GOSHOW_STATUS);

		int temp = 0;
		if (paxStatus != null && !paxStatus.isEmpty()) {
			for (Entry<String, String> entry : paxStatus.entrySet()) {
				jsb.append("pfsStatus[" + temp + "] = new Array('" + entry.getValue() + "','" + entry.getKey() + "');");
				temp++;
			}
		}
		jsb.append("arrData[0] = pfsStatus; arrData[1] = new Array();");
		jsb.append("var arrGroup2 = new Array();");
		jsb.append("var arrData2 = new Array();");
		jsb.append("arrData2[0] = new Array();");
		jsb.append("var pfsStatuss = new Listbox('lstPFSStatus', 'lstSelectedPFSStatus', 'spnPFSStatus','pfsStatuss');");
		jsb.append("pfsStatuss.group1 = arrData[0];" + "pfsStatuss.list2 = arrData2;");
		jsb.append("pfsStatuss.headingLeft = '&nbsp;&nbsp;PFS Status';");
		jsb.append("pfsStatuss.headingRight = '&nbsp;&nbsp;Selected PFS Status';");

		return jsb.toString();

	}
}
