package com.isa.thinair.airadmin.core.web.v2.handler.master;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airmaster.api.model.City;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;

public class CityRH {

	private static Log log = LogFactory.getLog(CityRH.class);

	public static Page<City> searchCity(int pageNo, List<ModuleCriterion> criterion) {
		Page<City> page = null;
		try {
			if (criterion == null) {
				page = ModuleServiceLocator.getCommonServiceBD().getCities((pageNo - 1) * 20, 20);
			} else {
				if (pageNo == 0) {
					pageNo = 1;
				}
				page = ModuleServiceLocator.getCommonServiceBD().getCities((pageNo - 1) * 20, 20, criterion);
			}
		} catch (ModuleException e) {
			log.error(e);
		}
		return page;
	}

	public static boolean saveCity(City city) throws ModuleException {
		ModuleServiceLocator.getCommonServiceBD().saveCity(city);
		return true;
	}

	public static boolean deleteCity(Integer cityId) throws ModuleException {
		ModuleServiceLocator.getCommonServiceBD().deleteCity(cityId);
		return true;
	}

	public static City getCity(Integer cityId) throws ModuleException {
		City city = ModuleServiceLocator.getCommonServiceBD().getCity(cityId);
		return city;
	}

	public static List<City> getCities(String countryCode)
			throws ModuleException {
		List<City> cityList= ModuleServiceLocator.getCommonServiceBD().getCities(countryCode);
		return cityList;
	}
	
	public static City getCity(String cityCode, String countryCode) throws ModuleException {
		City city = ModuleServiceLocator.getCommonServiceBD().getCity(cityCode, countryCode);
		return city;
	}
	
}
