/**
 * 	mano
	May 5, 2011 
	2011
 */
package com.isa.thinair.airadmin.core.web.v2.action.master;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airadmin.core.web.v2.handler.master.BaggageRequestHandler;
import com.isa.thinair.airadmin.core.web.v2.util.CommonUtil;
import com.isa.thinair.airmaster.api.dto.BaggageSearchDTO;
import com.isa.thinair.airmaster.api.model.Baggage;
import com.isa.thinair.airmaster.api.model.BaggageCOS;
import com.isa.thinair.airmaster.api.model.I18nMessage;
import com.isa.thinair.airmaster.api.to.BaggageTO;
import com.isa.thinair.airpricing.api.util.DelimeterConstants;
import com.isa.thinair.airpricing.api.util.SplitUtil;
import com.isa.thinair.commons.api.dto.LogicalCabinClassDTO;
import com.isa.thinair.commons.api.dto.ModuleCriteria;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.MessagesUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.CommonsServices;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.webplatform.api.util.Constants;

/**
 * @author mano
 * 
 */

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ShowBaggageAction {

	private BaggageSearchDTO bagSrch;

	private int page;

	private int total;

	private int records;

	private Object[] rows;

	private String succesMsg;

	private String msgType;

	private Integer baggageId;

	private String baggageName;

	private String description;

	private String iataCode;

	private String status;

	private long version;

	private String createdBy;

	private Date createdDate;

	private String hdnMode;

	private BigDecimal baggageWeight;

	private String baggagesForDisplay;

	private String cos;

	private static AiradminConfig airadminConfig = new AiradminConfig();

	private static Log log = LogFactory.getLog(ShowBaggageAction.class);

	public String searchBaggage() {

		log.debug("inside searchBaggage");

		try {

			List<ModuleCriterion> critrian = new ArrayList<ModuleCriterion>();

			if (bagSrch != null) {

				if (bagSrch.getBaggageName() != null && !bagSrch.getBaggageName().trim().equals("")) {

					ModuleCriteria baggageNameCrt = new ModuleCriteria();
					List<String> bgNameList = new ArrayList<String>();
					baggageNameCrt.setFieldName("baggage.baggageName");
					baggageNameCrt.setCondition(ModuleCriteria.CONDITION_LIKE);
					bgNameList.add("%" + bagSrch.getBaggageName() + "%");
					baggageNameCrt.setValue(bgNameList);
					critrian.add(baggageNameCrt);

				}
				if (bagSrch.getStatus() != null && !bagSrch.getStatus().trim().equals("")) {
					if (bagSrch.getStatus().equals("ALL")) {
						bagSrch.setStatus("%");
					}
					ModuleCriteria statusCrt = new ModuleCriteria();
					List<String> bgStatusList = new ArrayList<String>();
					statusCrt.setFieldName("baggage.status");
					statusCrt.setCondition(ModuleCriteria.CONDITION_LIKE);
					bgStatusList.add(bagSrch.getStatus().toUpperCase());
					statusCrt.setValue(bgStatusList);
					critrian.add(statusCrt);

				}

				if (bagSrch.getIataCode() != null && !bagSrch.getIataCode().trim().equals("")) {

					ModuleCriteria iataCodeCrt = new ModuleCriteria();
					List<String> bgIataCodeList = new ArrayList<String>();
					iataCodeCrt.setFieldName("baggage.iataCode");
					iataCodeCrt.setCondition(ModuleCriteria.CONDITION_EQUALS);
					bgIataCodeList.add(bagSrch.getIataCode());
					iataCodeCrt.setValue(bgIataCodeList);
					critrian.add(iataCodeCrt);
				}
			}

			Page<Baggage> pgBaggage = BaggageRequestHandler.searchBaggage(this.page, critrian);
			this.records = pgBaggage.getTotalNoOfRecords();

			this.total = pgBaggage.getTotalNoOfRecords() / 20;
			int mod = pgBaggage.getTotalNoOfRecords() % 20;
			if (mod > 0) {
				this.total = this.total + 1;
			}
			if (this.page <= 0) {
				this.page = 1;
			}

			Collection<Baggage> colBaggage = pgBaggage.getPageData();

			if (colBaggage != null) {

				Object[] dataRow = new Object[colBaggage.size()];
				int index = 1;
				Iterator<Baggage> iter = colBaggage.iterator();
				while (iter.hasNext()) {
					Map<String, Object> counmap = new HashMap<String, Object>();
					Baggage grdBaggage = iter.next();
					Baggage jSonBaggage = new Baggage();

					String attachedToMT = "N";
					counmap.put("id", new Integer(((page - 1) * 20) + index).toString());

					jSonBaggage.setBaggageId(grdBaggage.getBaggageId());
					jSonBaggage.setBaggageName(grdBaggage.getBaggageName());
					jSonBaggage.setBaggageWeight(grdBaggage.getBaggageWeight());
					// jSonBaggage.setCabinClassCode(grdBaggage.getCabinClassCode());
					jSonBaggage.setModifiedBy(grdBaggage.getModifiedBy());
					jSonBaggage.setModifiedDate(grdBaggage.getModifiedDate());
					jSonBaggage.setCreatedBy(grdBaggage.getCreatedBy());
					jSonBaggage.setCreatedDate(grdBaggage.getCreatedDate());
					jSonBaggage.setVersion(grdBaggage.getVersion());
					jSonBaggage.setStatus(grdBaggage.getStatus());
					jSonBaggage.setIataCode(grdBaggage.getIataCode());
					jSonBaggage.setDescription(grdBaggage.getDescription());

					String langTraStr = "";

					try {
						if (grdBaggage.getI18nMessageKey() != null) {
							langTraStr = convertLanguageTranslations(grdBaggage.getI18nMessageKey().getI18nMessages());
						}
					} catch (Exception e) {
						langTraStr = "";
					}
					Map<String, LogicalCabinClassDTO> lccMap = CommonsServices.getGlobalConfig().getAvailableLogicalCCMap();
					ArrayList<String> ccList = new ArrayList<String>();
					ArrayList<String> lccList = new ArrayList<String>();
					for (BaggageCOS baggageCOSList : grdBaggage.getBaggageCOS()) {
						if (AppSysParamsUtil.isLogicalCabinClassEnabled()) {
							if (baggageCOSList.getLogicalCCCode() != null) {
								lccList.add(baggageCOSList.getLogicalCCCode() + DelimeterConstants.LOGICAL_CABIN_CLASS_APPENDER);
							}
							if (baggageCOSList.getCabinClassCode() != null) {
								ccList.add(baggageCOSList.getCabinClassCode());
							}
						} else {
							if (baggageCOSList.getCabinClassCode() != null) {
								ccList.add(baggageCOSList.getCabinClassCode());
							} else {
								ccList.add((lccMap.get(baggageCOSList.getLogicalCCCode())).getCabinClassCode());
							}
						}
					}

					counmap.put("baggage", jSonBaggage);
					counmap.put("baggageForDisplay", langTraStr);
					counmap.put("hasBaggageTemp", attachedToMT);
					counmap.put("ccList", ccList);
					counmap.put("lccList", lccList);

					dataRow[index - 1] = counmap;
					index++;
				}
				this.rows = dataRow;
			}

		} catch (ModuleException e) {
			this.succesMsg = e.getMessageString();
			this.msgType = WebConstants.MSG_ERROR;
		} catch (Exception ex) {
			this.succesMsg = "System Error Please Contact Administrator";
			this.msgType = WebConstants.MSG_ERROR;
			if (log.isErrorEnabled()) {
				log.error(ex);
			}
		}

		return S2Constants.Result.SUCCESS;
	}

	public String deleteBaggage() {

		if (log.isDebugEnabled()) {
			log.debug("inside deleteBaggage : " + baggageId);
		}
		try {
			BaggageRequestHandler.deleteBaggage(baggageId);
			this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_DELETE_SUCCESS);
			this.msgType = WebConstants.MSG_SUCCESS;
		} catch (ModuleException e) {
			this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_DELETE_FAIL);
			if (e.getMessageString().equals(MessagesUtil.getMessage("module.constraint.childrecord"))) {
				this.succesMsg = airadminConfig.getMessage("um.airadmin.childrecord");
			}

			this.msgType = WebConstants.MSG_ERROR;
		} catch (Exception ex) {
			this.succesMsg = "System Error Please Contact Administrator";
			this.msgType = WebConstants.MSG_ERROR;
			if (log.isErrorEnabled()) {
				log.error(ex);
			}
		}

		return S2Constants.Result.SUCCESS;
	}

	public String execute() {

		Baggage baggage = new Baggage();
		baggage.setBaggageName(baggageName.trim());
		baggage.setDescription(description.trim());
		baggage.setIataCode(iataCode.trim());
		baggage.setStatus(status != null ? status : Baggage.STATUS_INACTIVE);
		baggage.setVersion(version);
		baggage.setCreatedBy(createdBy);
		baggage.setCreatedDate(createdDate);
		baggage.setBaggageId(baggageId);
		baggage.setBaggageWeight(baggageWeight);
		baggage.setSortOrder(1);

		if (cos != null && !cos.isEmpty()) {
			String[] cosList = cos.split(Constants.COMMA_SEPARATOR);
			Set<BaggageCOS> baggageCOSList = new HashSet<BaggageCOS>();
			for (String element : cosList) {
				BaggageCOS baggageCOS = new BaggageCOS();
				baggageCOS.setBaggage(baggage);
				String[] splittedCabinClassAndLogicalCabinClass = SplitUtil.getSplittedCabinClassAndLogicalCabinClass(element);
				baggageCOS.setCabinClassCode(splittedCabinClassAndLogicalCabinClass[0]);
				baggageCOS.setLogicalCCCode(splittedCabinClassAndLogicalCabinClass[1]);
				baggageCOSList.add(baggageCOS);
			}
			baggage.setBaggageCOS(baggageCOSList);
		}

		if (this.hdnMode != null
				&& ((hdnMode.equals(WebConstants.ACTION_ADD)) || (hdnMode.equals(WebConstants.ACTION_EDIT) || (hdnMode
						.equals(WebConstants.ACTION_SAVE_LIST_FOR_DISPLAY))))) {

			try {
				BaggageRequestHandler.saveBaggage(baggage, getBaggageTO());
				this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS);
				this.msgType = WebConstants.MSG_SUCCESS;

			} catch (ModuleException e) {
				this.succesMsg = e.getMessageString();
				if (e.getMessageString().equals(MessagesUtil.getMessage("module.duplicate.key"))) {
					this.succesMsg = airadminConfig.getMessage("um.baggage.form.name.dulply");
				}
				this.msgType = WebConstants.MSG_ERROR;
			} catch (Exception ex) {
				this.succesMsg = "System Error Please Contact Administrator";
				this.msgType = WebConstants.MSG_ERROR;
				if (log.isErrorEnabled()) {
					log.error(ex);
				}
			}
		} else {

		}
		return S2Constants.Result.SUCCESS;
	}

	/**
	 * @return the bagSrch
	 */
	public BaggageSearchDTO getBagSrch() {
		return bagSrch;
	}

	/**
	 * @param bagSrch
	 *            the bagSrch to set
	 */
	public void setBagSrch(BaggageSearchDTO bagSrch) {
		this.bagSrch = bagSrch;
	}

	/**
	 * @return the page
	 */
	public int getPage() {
		return page;
	}

	/**
	 * @param page
	 *            the page to set
	 */
	public void setPage(int page) {
		this.page = page;
	}

	/**
	 * @return the total
	 */
	public int getTotal() {
		return total;
	}

	/**
	 * @param total
	 *            the total to set
	 */
	public void setTotal(int total) {
		this.total = total;
	}

	/**
	 * @return the records
	 */
	public int getRecords() {
		return records;
	}

	/**
	 * @param records
	 *            the records to set
	 */
	public void setRecords(int records) {
		this.records = records;
	}

	/**
	 * @return the rows
	 */
	public Object[] getRows() {
		return rows;
	}

	/**
	 * @param rows
	 *            the rows to set
	 */
	public void setRows(Object[] rows) {
		this.rows = rows;
	}

	/**
	 * @return the succesMsg
	 */
	public String getSuccesMsg() {
		return succesMsg;
	}

	/**
	 * @param succesMsg
	 *            the succesMsg to set
	 */
	public void setSuccesMsg(String succesMsg) {
		this.succesMsg = succesMsg;
	}

	/**
	 * @return the msgType
	 */
	public String getMsgType() {
		return msgType;
	}

	/**
	 * @param msgType
	 *            the msgType to set
	 */
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	/**
	 * @return the baggageId
	 */
	public Integer getBaggageId() {
		return baggageId;
	}

	/**
	 * @param baggageId
	 *            the baggageId to set
	 */
	public void setBaggageId(Integer baggageId) {
		this.baggageId = baggageId;
	}

	/**
	 * @return the baggageName
	 */
	public String getBaggageName() {
		return baggageName;
	}

	/**
	 * @param baggageName
	 *            the baggageName to set
	 */
	public void setBaggageName(String baggageName) {
		this.baggageName = baggageName;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the iataCode
	 */
	public String getIataCode() {
		return iataCode;
	}

	/**
	 * @param iataCode
	 *            the iataCode to set
	 */
	public void setIataCode(String iataCode) {
		this.iataCode = iataCode;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the version
	 */
	public long getVersion() {
		return version;
	}

	/**
	 * @param version
	 *            the version to set
	 */
	public void setVersion(long version) {
		this.version = version;
	}

	/**
	 * @return the createdBy
	 */
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate() {
		return createdDate;
	}

	/**
	 * @param createdDate
	 *            the createdDate to set
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return the hdnMode
	 */
	public String getHdnMode() {
		return hdnMode;
	}

	/**
	 * @param hdnMode
	 *            the hdnMode to set
	 */
	public void setHdnMode(String hdnMode) {
		this.hdnMode = hdnMode;
	}

	public BigDecimal getBaggageWeight() {
		return baggageWeight;
	}

	public void setBaggageWeight(BigDecimal baggageWeight) {
		this.baggageWeight = baggageWeight;
	}

	public BaggageTO getBaggageTO() {

		BaggageTO baggageTO = new BaggageTO();
		baggageTO.setBaggageId(baggageId);
		baggageTO.setBaggageName(baggageName);
		baggageTO.setDescription(description);
		baggageTO.setIataCode(iataCode);
		baggageTO.setStatus(status);
		baggageTO.setCabinClassCode("Y"); // Remove This
		baggageTO.setBaggageWeight(baggageWeight);
		baggageTO.setVersion(Long.toString(version));
		baggageTO.setSelectedLanguageTranslations(baggagesForDisplay);
		baggageTO.setUpdateLanguageTranslations((this.hdnMode != null && hdnMode
				.equals(WebConstants.ACTION_SAVE_LIST_FOR_DISPLAY)));

		return baggageTO;
	}

	public String convertLanguageTranslations(Set<I18nMessage> messages) {

		StringBuilder sb = new StringBuilder();
		if (messages != null) {
			for (I18nMessage message : messages) {
				if (message != null) {

					sb.append(message.getMessageId());
					sb.append(",");
					sb.append(message.getMessageLocale());
					sb.append(",");
					sb.append(StringUtil.getUnicode(message._getMsgContent()));
					sb.append("~");
				}

			}
		}
		return sb.toString();
	}

	/**
	 * @return the cos
	 */
	public String getCos() {
		return cos;
	}

	/**
	 * @param cos
	 *            the cos to set
	 */
	public void setCos(String cos) {
		this.cos = cos;
	}

	/**
	 * @return
	 */
	public String getBaggagesForDisplay() {
		return baggagesForDisplay;
	}

	/**
	 * @param baggagesForDisplay
	 */
	public void setBaggagesForDisplay(String baggagesForDisplay) {
		this.baggagesForDisplay = baggagesForDisplay;
	}
}
