package com.isa.thinair.airadmin.core.web.v2.dto.inventory;

import java.io.Serializable;

public class SeatMvSummaryTO implements Serializable {

	private static final long serialVersionUID = -1L;

	private String agentName;
	
	private String soldSeats;

	private String onholdSeats;

	private String cancelledSeats;

	private String soldSeatsSum;

	private String onholdSeatsSum;
	
	private String cancelledSeatsSum;

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getSoldSeats() {
		return soldSeats;
	}

	public void setSoldSeats(String soldSeats) {
		this.soldSeats = soldSeats;
	}

	public String getOnholdSeats() {
		return onholdSeats;
	}

	public void setOnholdSeats(String onholdSeats) {
		this.onholdSeats = onholdSeats;
	}

	public String getCancelledSeats() {
		return cancelledSeats;
	}

	public void setCancelledSeats(String cancelledSeats) {
		this.cancelledSeats = cancelledSeats;
	}

	public String getSoldSeatsSum() {
		return soldSeatsSum;
	}

	public void setSoldSeatsSum(String soldSeatsSum) {
		this.soldSeatsSum = soldSeatsSum;
	}

	public String getOnholdSeatsSum() {
		return onholdSeatsSum;
	}

	public void setOnholdSeatsSum(String onholdSeatsSum) {
		this.onholdSeatsSum = onholdSeatsSum;
	}

	public String getCancelledSeatsSum() {
		return cancelledSeatsSum;
	}

	public void setCancelledSeatsSum(String cancelledSeatsSum) {
		this.cancelledSeatsSum = cancelledSeatsSum;
	}
	
}
