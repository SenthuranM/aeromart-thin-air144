package com.isa.thinair.airadmin.core.web.action.reports;

import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.handler.reports.VoidReservationDetailsRH;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;

/**
 * 
 * @author asiri
 * 
 */
@Results({
	@Result(name = AdminStrutsConstants.AdminAction.SUCCESS, value = AdminStrutsConstants.AdminJSP.REPORTING_VOID_RESERVATION_DETAIL_REPORT_JSP),
	@Result(name = AdminStrutsConstants.AdminAction.ERROR, value = AdminStrutsConstants.AdminJSP.ERROR_JSP) })
public class ShowVoidReservationDetailsReportAction extends BaseRequestResponseAwareAction {
	public String execute() {
		return VoidReservationDetailsRH.execute(request, response);
	}
}
