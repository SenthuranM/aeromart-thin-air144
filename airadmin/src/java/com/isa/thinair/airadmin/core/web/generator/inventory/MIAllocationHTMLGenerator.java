package com.isa.thinair.airadmin.core.web.generator.inventory;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airinventory.api.dto.ExtendedFCCSegBCInventory;
import com.isa.thinair.airinventory.api.dto.FCCInventoryDTO;
import com.isa.thinair.airinventory.api.dto.FCCSegInventoryDTO;
import com.isa.thinair.airinventory.api.model.BookingClass;
import com.isa.thinair.airinventory.api.model.FCCSegBCInventory;
import com.isa.thinair.airinventory.api.model.FCCSegInventory;
import com.isa.thinair.airpricing.api.dto.FareDTO;
import com.isa.thinair.airpricing.api.dto.FareSummaryLightDTO;
import com.isa.thinair.airpricing.api.model.Fare;
import com.isa.thinair.airpricing.api.model.FareRule;
import com.isa.thinair.airpricing.api.model.FareRulePax;
import com.isa.thinair.commons.api.dto.PaxTypeTO;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.SalesChannelsUtil;
import com.isa.thinair.reporting.api.model.FCCAgentsSeatMvDTO;
import com.isa.thinair.reporting.api.model.SeatMvSummaryDTO;

public class MIAllocationHTMLGenerator {

	private static Log log = LogFactory.getLog(MIAllocationHTMLGenerator.class);
	private String strFlightStatusJS = "var strFlightStatus;";

	/**
	 * Gets the Fare Details for a Given Fare ID
	 * 
	 * @param strFareId
	 *            the Fare Id
	 * @return String the Array of Fare Details
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getFareDetails(String strFareId) throws ModuleException {

		StringBuffer strFare = new StringBuffer();
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy");
		FareDTO fareDTO = ModuleServiceLocator.getFareBD().getFare(Integer.parseInt(strFareId));
		FareRule fareRule = fareDTO.getFareRule();
		Fare fare = fareDTO.getFare();
		String strAllVisibleChannelNames = "";
		String strAllAgentCodes = "";

		strFare.append("var fareDetails = new Array();");
		strFare.append("fareDetails[0]  = '" + fareRule.getFareRuleId() + "';");
		strFare.append("fareDetails[1]  = '" + formatter.format(fare.getEffectiveFromDate()) + "';");
		strFare.append("fareDetails[2]  = '" + formatter.format(fare.getEffectiveToDate()) + "';");
		strFare.append("fareDetails[3]  = '" + fareRule.getFareBasisCode() + "';");
		strFare.append("fareDetails[4]  = '" + fareRule.getFareRuleDescription() + "';");
		strFare.append("fareDetails[5]  = '" + getPaxLevelRefundability(fareRule.getPaxDetails()) + "';");
		strFare.append("fareDetails[6]  = '" + fareRule.getReturnFlag() + "';");
		int advbkTime = (fareRule.getAdvanceBookingDays() == null) ? 0 : fareRule.getAdvanceBookingDays();
		String[] advBKTimeDDHHMM = AiradminUtils.getAdvBkDDHHMM(advbkTime);
		strFare.append("fareDetails[7]  = '" + advBKTimeDDHHMM[0] + ":" + advBKTimeDDHHMM[1] + ":" + advBKTimeDDHHMM[2] + "';");
		strFare.append("fareDetails[8]  = '" + fare.getFareId() + "';");
		strFare.append("fareDetails[9]  = '" + fareRule.getFareRuleCode() + "';");

		Set<String> setAgentCodes = fareRule.getVisibileAgentCodes();
		if (setAgentCodes != null) {
			Iterator<String> setAgentCodesIt = setAgentCodes.iterator();
			while (setAgentCodesIt.hasNext()) {
				if (!strAllAgentCodes.equals("")) {
					strAllAgentCodes += ", ";
				}
				strAllAgentCodes += setAgentCodesIt.next();
			}
		}

		Set<Integer> setVisibleChannel = fareRule.getVisibleChannelIds();
		if (setVisibleChannel != null) {
			Iterator<Integer> setVisibleChannelsIt = setVisibleChannel.iterator();
			while (setVisibleChannelsIt.hasNext()) {
				Integer visibleChannelId = setVisibleChannelsIt.next();
				String strVisibleChannelName = SalesChannelsUtil.getSalesChannelName(visibleChannelId.intValue());
				if (strVisibleChannelName != null) {
					if (!strAllVisibleChannelNames.equals("")) {
						strAllVisibleChannelNames += ", ";
					}
					strAllVisibleChannelNames += strVisibleChannelName;
				}
			}
		}

		strFare.append("fareDetails[10] = '" + strAllAgentCodes + "';");
		strFare.append("fareDetails[11] = '" + strAllVisibleChannelNames + "';");

		return strFare.toString();
	}

	private static String getPaxLevelRefundability(Collection<FareRulePax> paxDetails) {

		String adultRefundability = "";
		String childRefundability = "";
		String infantRefundability = "";
		String refundability = "";
		String seperator = "/";

		if (paxDetails != null) {
			Iterator<FareRulePax> paxItr = paxDetails.iterator();
			while (paxItr.hasNext()) {
				FareRulePax fareRulePax = paxItr.next();
				if (fareRulePax.getPaxType().equalsIgnoreCase(PaxTypeTO.ADULT)) {
					if (fareRulePax.getRefundableFares()) {
						adultRefundability = "Yes";
					} else {
						adultRefundability = "No";
					}
				}

				if (fareRulePax.getPaxType().equalsIgnoreCase(PaxTypeTO.CHILD)) {
					if (fareRulePax.getRefundableFares()) {
						childRefundability = "Yes";
					} else {
						childRefundability = "No";
					}
				}
				if (fareRulePax.getPaxType().equalsIgnoreCase(PaxTypeTO.INFANT)) {
					if (fareRulePax.getRefundableFares()) {
						infantRefundability = "Yes";
					} else {
						infantRefundability = "No";
					}
				}
			}
		}
		refundability = (adultRefundability + seperator + childRefundability + seperator + infantRefundability);
		return refundability;
	}

	/**
	 * Creates the Fligts Status as a js Variable
	 * 
	 * @param flightId
	 *            the Flight Id
	 * @return String the created js variable
	 */
	public String getFlightStatusAsJS(int flightId) {

		try {
			String strFlightStatus = ModuleServiceLocator.getFlightServiceBD().getFlight(flightId).getStatus();
			strFlightStatusJS = "var strFlightStatus = '" + strFlightStatus + "';";
			AiradminUtils.debug(log, "strFlightStatus: " + strFlightStatusJS);

		} catch (ModuleException moduleException) {
			AiradminUtils.error(log, "SeatAllocationHTMLGenerator.getFlightStatus() : " + moduleException.getMessageString(),
					moduleException);
		}
		return strFlightStatusJS;
	}

	/**
	 * Gets the Seat allocation Grid Data
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Created Inventory data
	 * @throws ModuleException
	 *             the ModuleException
	 */
	@SuppressWarnings("rawtypes")
	public final String getSegmentAllocationRowHtml(HttpServletRequest request) throws ModuleException {

		StringBuffer data = new StringBuffer();

		int flightId = 0;
		String classOfService = "";
		Integer overlappingFlightId = null;

		if (request.getParameter("hdnFlightID") != null) {
			flightId = Integer.parseInt(request.getParameter("hdnFlightID"));
		}

		classOfService = request.getParameter("hdnClassOfService");
		if (classOfService != null) {
			if (classOfService.equals("")) {
				// TODO to support call from flights screen without specifying a cabinclass

				// find all the cabinclass for the flight; if system default cabinclass
				// exists for the flight, then set it otherwise
				// set one from those cabinclasses in the flight

			}
		}

		if (request.getParameter("hdnOlFlightId") != null && !request.getParameter("hdnOlFlightId").equals("null")
				&& !request.getParameter("hdnOlFlightId").equals("")) {

			overlappingFlightId = new Integer(request.getParameter("hdnOlFlightId"));
		}

		String strHdnMode = request.getParameter("hdnMode");
		data.append("var sa = new Array();");
		data.append("var ba = new Array();");
		data.append("var sma = new Array();");
		data.append("var smaOwnerAgetnWise = new Array();");

		Collection<FCCInventoryDTO> fccInventoryDTOs = null;
		if (strHdnMode != null && (strHdnMode.equals("SAVEOL"))) {
			fccInventoryDTOs = ModuleServiceLocator.getFlightInventoryBD().getFCCInventory(overlappingFlightId.intValue(),
					classOfService, new Integer(flightId), false);
		} else {
			fccInventoryDTOs = ModuleServiceLocator.getFlightInventoryBD().getFCCInventory(flightId, classOfService,
					overlappingFlightId,false);
		}
		FCCAgentsSeatMvDTO fccSeatMvDTO = ModuleServiceLocator.getDataExtractionBD().getFCCSeatMovementSummary(flightId,
				classOfService, false);
		FCCAgentsSeatMvDTO fccOwnerAgentWiseSeatMvDTO = ModuleServiceLocator.getDataExtractionBD()
				.getOwnerAgentWiseFCCSeatMovementSummary(flightId, classOfService, false);

		if (fccInventoryDTOs != null) {
			Iterator<FCCInventoryDTO> fccInventoryDTOsIt = fccInventoryDTOs.iterator();

			// iterate flights
			int segIndex = 0;
			for (int fccIndex = 0; fccInventoryDTOsIt.hasNext(); ++fccIndex) {
				FCCInventoryDTO fccInventoryDTO = fccInventoryDTOsIt.next();
				// prepare fcc inventory array

				Collection<FCCSegInventoryDTO> fccSegInventoryDTOs = fccInventoryDTO.getFccSegInventoryDTOs();
				if (fccSegInventoryDTOs != null && fccSegInventoryDTOs.size() > 0) {
					// iterate segments in flight
					Iterator<FCCSegInventoryDTO> fccSegInventoryDTOsIt = fccSegInventoryDTOs.iterator();
					while (fccSegInventoryDTOsIt.hasNext()) {

						FCCSegInventoryDTO fccSegInventoryDTO = fccSegInventoryDTOsIt.next();
						FCCSegInventory fccSegInventory = fccSegInventoryDTO.getFccSegInventory();
						// prepare segment inventory array
						data.append("sa[" + segIndex + "] = new Array();");
						data.append("sa[" + segIndex + "][0] = '';");
						data.append("sa[" + segIndex + "][1] = '" + fccSegInventoryDTO.getFlightNumber() + "';");
						data.append("sa[" + segIndex + "][2] = '" + fccSegInventory.getSegmentCode() + "';");
						data.append("sa[" + segIndex + "][3] = '" + fccSegInventory.getAllocatedSeats() + "';");
						data.append("sa[" + segIndex + "][4] = '" + fccSegInventory.getOversellSeats() + "';");
						data.append("sa[" + segIndex + "][5] = '" + fccSegInventory.getCurtailedSeats() + "';");
						data.append("sa[" + segIndex + "][6] = '" + fccSegInventory.getInfantAllocation() + "';");
						data.append("sa[" + segIndex + "][7] = '" + fccSegInventory.getSeatsSold() + "/"
								+ fccSegInventory.getSoldInfantSeats() + "';");
						data.append("sa[" + segIndex + "][8] = '" + fccSegInventory.getOnHoldSeats() + "/"
								+ fccSegInventory.getOnholdInfantSeats() + "';");
						data.append("sa[" + segIndex + "][9] = '" + fccSegInventory.getAvailableSeats() + "/"
								+ fccSegInventory.getAvailableInfantSeats() + "';");

						// not shown segment information
						// flightId
						data.append("sa[" + segIndex + "][10] = '" + fccSegInventory.getFlightId() + "';");
						// fccInvId
						data.append("sa[" + segIndex + "][11] = '" + fccSegInventory.getFccInvId() + "';");
						// fccsInvId
						data.append("sa[" + segIndex + "][12] = '" + fccSegInventory.getFccsInvId() + "';");
						// version
						data.append("sa[" + segIndex + "][13] = '" + fccSegInventory.getVersion() + "';");
						// flight count
						data.append("sa[" + segIndex + "][14] = '" + fccIndex + "';");
						// late additions
						data.append("sa[" + segIndex + "][15] = '" + fccSegInventoryDTO.getFlightSegmentStatus() + "';");

						// segment status
						List<Integer> currentSegInvId = new ArrayList<Integer>();
						currentSegInvId.add(fccSegInventory.getFccsInvId());
						int totalSoldOhdCountInInterceptingSegments = ModuleServiceLocator.getFlightInventoryBD()
								.getMaximumEffectiveReservedSetsCountInInterceptingSegments(currentSegInvId);

						int overbookCount = (totalSoldOhdCountInInterceptingSegments
								+ fccSegInventory.getSoldAndOnholdAdultSeats() - fccSegInventory.getOversellSeats()
								- fccSegInventory.getAllocatedSeats() + fccSegInventory.getCurtailedSeats());
						data.append("sa[" + segIndex + "][16] = '" + (overbookCount < 0 ? 0 : overbookCount) + "';");
						// to retrieve allocations logical cabin class wise
						data.append("sa[" + segIndex + "][17] = '" + fccSegInventory.getFlightSegId() + "';");

						// prepare seat movement array
						data.append("sma[" + segIndex + "] = new Array();");

						if (fccSeatMvDTO != null) {
							Iterator<SeatMvSummaryDTO> seatMvInfoIt = fccSeatMvDTO.getSegAgentsSeatMvWithFCCAgentsSeatMv(
									new Integer(fccSegInventory.getFlightSegId())).iterator();

							for (int smIndex = 0; seatMvInfoIt.hasNext(); ++smIndex) {
								SeatMvSummaryDTO seatMvSummary = seatMvInfoIt.next();

								data.append("sma[" + segIndex + "][" + smIndex + "] = new Array();");
								data.append("sma[" + segIndex + "][" + smIndex + "][0] = '';");
								data.append("sma[" + segIndex + "][" + smIndex + "][1] = '" + seatMvSummary.getAgentName() + "';");
								data.append("sma[" + segIndex + "][" + smIndex + "][2] = '" + seatMvSummary.getSoldSeats() + "/"
										+ seatMvSummary.getSoldChildSeats() + "/" + seatMvSummary.getSoldInfantSeats() + "';");
								data.append("sma[" + segIndex + "][" + smIndex + "][3] = '" + seatMvSummary.getOnholdSeats()
										+ "/" + seatMvSummary.getOnHoldChildSeats() + "/" + seatMvSummary.getOnholdInfantSeats()
										+ "';");
								data.append("sma[" + segIndex + "][" + smIndex + "][4] = '" + seatMvSummary.getCancelledSeats()
										+ "/" + seatMvSummary.getCancelledChildSeats() + "/"
										+ seatMvSummary.getCancelledInfantSeats() + "';");
								// if (fccSegInventoryDTOs.size() >1 ){
								data.append("sma[" + segIndex + "][" + smIndex + "][5] = '" + seatMvSummary.getSoldSeatsSum()
										+ "/" + seatMvSummary.getSoldChildSeatsSum() + "/"
										+ seatMvSummary.getSoldInfantSeatsSum() + "';");
								data.append("sma[" + segIndex + "][" + smIndex + "][6] = '" + seatMvSummary.getOnholdSeatsSum()
										+ "/" + seatMvSummary.getOnHoldChildSeatsSum() + "/"
										+ seatMvSummary.getOnholdInfantSeatsSum() + "';");
								data.append("sma[" + segIndex + "][" + smIndex + "][7] = '"
										+ seatMvSummary.getCancelledSeatsSum() + "/" + seatMvSummary.getCancelledChildSeatsSum()
										+ "/" + seatMvSummary.getCancelledInfantSeatsSum() + "';");
								// }
							}
						}

						data.append("smaOwnerAgetnWise[" + segIndex + "] = new Array();");
						if (fccOwnerAgentWiseSeatMvDTO != null) {
							Iterator<SeatMvSummaryDTO> seatMvInfoIt = fccOwnerAgentWiseSeatMvDTO
									.getSegAgentsSeatMvWithFCCAgentsSeatMv(new Integer(fccSegInventory.getFlightSegId()))
									.iterator();

							for (int smIndex = 0; seatMvInfoIt.hasNext(); ++smIndex) {
								SeatMvSummaryDTO seatMvSummary = seatMvInfoIt.next();

								data.append("smaOwnerAgetnWise[" + segIndex + "][" + smIndex + "] = new Array();");
								data.append("smaOwnerAgetnWise[" + segIndex + "][" + smIndex + "][0] = '';");
								data.append("smaOwnerAgetnWise[" + segIndex + "][" + smIndex + "][1] = '"
										+ seatMvSummary.getAgentName() + "';");
								data.append("smaOwnerAgetnWise[" + segIndex + "][" + smIndex + "][2] = '"
										+ seatMvSummary.getSoldSeats() + "/" + seatMvSummary.getSoldChildSeats() + "/"
										+ seatMvSummary.getSoldInfantSeats() + "';");
								data.append("smaOwnerAgetnWise[" + segIndex + "][" + smIndex + "][3] = '"
										+ seatMvSummary.getOnholdSeats() + "/" + seatMvSummary.getOnHoldChildSeats() + "/"
										+ seatMvSummary.getOnholdInfantSeats() + "';");
								data.append("smaOwnerAgetnWise[" + segIndex + "][" + smIndex + "][4] = '"
										+ seatMvSummary.getCancelledSeats() + "/" + seatMvSummary.getCancelledChildSeats() + "/"
										+ seatMvSummary.getCancelledInfantSeats() + "';");
								// if (fccSegInventoryDTOs.size() >1 ){
								data.append("smaOwnerAgetnWise[" + segIndex + "][" + smIndex + "][5] = '"
										+ seatMvSummary.getSoldSeatsSum() + "/" + seatMvSummary.getSoldChildSeatsSum() + "/"
										+ seatMvSummary.getSoldInfantSeatsSum() + "';");
								data.append("smaOwnerAgetnWise[" + segIndex + "][" + smIndex + "][6] = '"
										+ seatMvSummary.getOnholdSeatsSum() + "/" + seatMvSummary.getOnHoldChildSeatsSum() + "/"
										+ seatMvSummary.getOnholdInfantSeatsSum() + "';");
								data.append("smaOwnerAgetnWise[" + segIndex + "][" + smIndex + "][7] = '"
										+ seatMvSummary.getCancelledSeatsSum() + "/" + seatMvSummary.getCancelledChildSeatsSum()
										+ "/" + seatMvSummary.getCancelledInfantSeatsSum() + "';");
								// }
							}
						}

						data.append("ba[" + segIndex + "] = new Array();");
						Collection<ExtendedFCCSegBCInventory> fccSegBCInventories = fccSegInventoryDTO.getFccSegBCInventories();
						if (fccSegBCInventories != null && fccSegBCInventories.size() > 0) {
							// iterate bc allocs in segment
							Iterator<ExtendedFCCSegBCInventory> fccSegBCInventoriesIt = fccSegBCInventories.iterator();

							for (int bcIndex = 0; fccSegBCInventoriesIt.hasNext(); ++bcIndex) {
								ExtendedFCCSegBCInventory exFCCSegBCInventory = fccSegBCInventoriesIt.next();
								FCCSegBCInventory fccSegBCInventory = exFCCSegBCInventory.getFccSegBCInventory();

								// prepare bc inventory array
								data.append("ba[" + segIndex + "][" + bcIndex + "] = new Array();");
								data.append("ba[" + segIndex + "][" + bcIndex + "][0] = '';");
								data.append("ba[" + segIndex + "][" + bcIndex + "][1] = '" + fccSegBCInventory.getBookingCode()
										+ "';");
								data.append("ba["
										+ segIndex
										+ "]["
										+ bcIndex
										+ "][2] = '"
										+ (exFCCSegBCInventory.isStandardCode()
												? BookingClass.STANDARD_CODE_Y
												: BookingClass.STANDARD_CODE_N) + "';");
								data.append("ba["
										+ segIndex
										+ "]["
										+ bcIndex
										+ "][3] = '"
										+ (exFCCSegBCInventory.isFixedFlag()
												? BookingClass.FIXED_FLAG_Y
												: BookingClass.FIXED_FLAG_N) + "';");
								data.append("ba[" + segIndex + "][" + bcIndex + "][4] = " + fccSegBCInventory.getPriorityFlag()
										+ ";");
								data.append("ba[" + segIndex + "][" + bcIndex + "][5] = '"
										+ fccSegBCInventory.getSeatsAllocated() + "';");
								data.append("ba[" + segIndex + "][" + bcIndex + "][6] = '"
										+ fccSegBCInventory.getActualSeatsSold() + "';");
								data.append("ba[" + segIndex + "][" + bcIndex + "][7] = '"
										+ fccSegBCInventory.getActualSeatsOnHold() + "';");
								data.append("ba[" + segIndex + "][" + bcIndex + "][8] = '"
										+ fccSegBCInventory.getSeatsCancelled() + "';");

								data.append("ba[" + segIndex + "][" + bcIndex + "][9] = '" + fccSegBCInventory.getSeatsAcquired()
										+ "';");
								data.append("ba[" + segIndex + "][" + bcIndex + "][10] = '"
										+ fccSegBCInventory.getSeatsAvailable() + "';");
								data.append("ba["
										+ segIndex
										+ "]["
										+ bcIndex
										+ "][11] = "
										+ (fccSegBCInventory.getStatus().endsWith(FCCSegBCInventory.Status.CLOSED) ? true : false)
										+ ";");

								Iterator faresIt = exFCCSegBCInventory.getLinkedEffectiveFares().iterator();
								data.append("ba[" + segIndex + "][" + bcIndex + "][12] = '");
								String fareLink = "";
								for (int fareIndex = 0; faresIt.hasNext(); ++fareIndex) {
									FareSummaryLightDTO fareSummaryDTO = (FareSummaryLightDTO) faresIt.next();
									int fareId = fareSummaryDTO.getFareId();
									double fareAmount = fareSummaryDTO.getFareAmount();

									if (fareIndex != exFCCSegBCInventory.getLinkedEffectiveFares().size() - 1) {
										fareLink = fareId + ":" + fareAmount + "|";
									} else {
										fareLink = fareId + ":" + fareAmount;
									}
									data.append(fareLink);
								}
								data.append("';");

								data.append("ba["
										+ segIndex
										+ "]["
										+ bcIndex
										+ "][13] = '"
										+ ((exFCCSegBCInventory.getLinkedEffectiveAgents() != null && exFCCSegBCInventory
												.getLinkedEffectiveAgents().size() > 0) ? Integer.toString(exFCCSegBCInventory
												.getLinkedEffectiveAgents().size()) : "0") + "';");

								// not shown parameters
								data.append("ba[" + segIndex + "][" + bcIndex + "][14] = '" + fccSegBCInventory.getFccsbInvId()
										+ "';");// fccsbInvId

								// manually closed flag
								data.append("ba[" + segIndex + "][" + bcIndex + "][15] = '"
										+ fccSegBCInventory.getStatusChangeAction() + "';");
								// version
								data.append("ba[" + segIndex + "][" + bcIndex + "][16] = '" + fccSegBCInventory.getVersion()
										+ "';");

								// Staus Edited or not - Default value = No
								data.append("ba[" + segIndex + "][" + bcIndex + "][17] = 'N';");
								// To enable the Fare Link
								data.append("ba[" + segIndex + "][" + bcIndex + "][18] = '';");
								// For front-end validations
								data.append("ba[" + segIndex + "][" + bcIndex + "][19] = '" + fccSegBCInventory.getSeatsSold()
										+ "';");
								// For front-end validations
								data.append("ba[" + segIndex + "][" + bcIndex + "][20] = '" + fccSegBCInventory.getOnHoldSeats()
										+ "';");
								data.append("ba["
										+ segIndex
										+ "]["
										+ bcIndex
										+ "][25] = "
										+ (fccSegBCInventory.getStatus().endsWith(FCCSegBCInventory.Status.CLOSED) ? true : false)
										+ ";");
								// manually closed flag
								data.append("ba[" + segIndex + "][" + bcIndex + "][26] = '"
										+ fccSegBCInventory.getStatusChangeAction() + "';");

								// nest rank
								if (exFCCSegBCInventory.getNestRank() != null) {
									data.append("ba[" + segIndex + "][" + bcIndex + "][27] = '"
											+ exFCCSegBCInventory.getNestRank() + "';");
								} else {
									data.append("ba[" + segIndex + "][" + bcIndex + "][27] = '&nbsp;';");
								}

								// nest rank in sold
								data.append("ba[" + segIndex + "][" + bcIndex + "][28] = '"
										+ fccSegBCInventory.getSeatsSoldAquiredByNesting() + "';");
								// nest rank out sold
								data.append("ba[" + segIndex + "][" + bcIndex + "][29] = '"
										+ fccSegBCInventory.getSeatsSoldNested() + "';");
								// nest rank in onhold
								data.append("ba[" + segIndex + "][" + bcIndex + "][30] = '"
										+ fccSegBCInventory.getSeatsOnHoldAquiredByNesting() + "';");
								// nest rank out onhold
								data.append("ba[" + segIndex + "][" + bcIndex + "][31] = '"
										+ fccSegBCInventory.getSeatsOnHoldNested() + "';");
								// Haider 14Sep08 gds id for the booking class
								data.append("ba[" + segIndex + "][" + bcIndex + "][32] = '" + exFCCSegBCInventory.getGdsId()
										+ "';");
								data.append("ba[" + segIndex + "][" + bcIndex + "][33] = '" + exFCCSegBCInventory.getBcType()
										+ "';");
								int overBookCount = Math.max(
										fccSegBCInventory.getSeatsAllocated() + fccSegBCInventory.getSeatsAcquired()
												- fccSegBCInventory.getSeatsCancelled(), 0)
										- (fccSegBCInventory.getOnHoldSeats() + fccSegBCInventory.getSeatsSold());
								data.append("ba[" + segIndex + "][" + bcIndex + "][34] = '"
										+ (overBookCount < 0 ? (-1 * overBookCount) : 0) + "';");
							}
						}
						++segIndex;
					}
				}
			}
		}
		return data.toString();
	}

	/**
	 * Create the Client Validations for Seat Allocation Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array of client validations
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getAllocationClientErrors(HttpServletRequest request) throws ModuleException {

		String clientErrors = null;
		Properties moduleErrs = new Properties();

		moduleErrs.setProperty("um.ManageInventory.form.segmentcurtailed.Invalid", "curtaildInvaid");
		moduleErrs.setProperty("um.ManageInventory.form.segmentoversell.Invalid", "oversellInvaid");
		moduleErrs.setProperty("um.ManageInventory.form.bcallocation.Less", "bcLess");
		moduleErrs.setProperty("um.ManageInventory.form.infantallocation.Less", "infantLess");
		moduleErrs.setProperty("um.ManageInventory.form.acquired.null", "acquirednull");
		moduleErrs.setProperty("um.ManageInventory.form.acquired.zro", "acquiredzero");
		moduleErrs.setProperty("um.searchinventory.form.fromdate.required", "departureDateFromRqrd");
		moduleErrs.setProperty("um.searchinventory.form.todate.required", "departureDateToRqrd");
		moduleErrs.setProperty("um.searchinventory.form.fromdatelessthancurrentdate.voilated", "departureDateFromBnsRleVltd");
		moduleErrs.setProperty("um.searchinventory.form.depdategreaterthanarrivaldate.voilated", "departureDateToBnsRleVltd");
		moduleErrs.setProperty("um.searchinventory.form.departureandarrivalsame.voilated", "departureAndArrivalBnsRleVltd");
		moduleErrs.setProperty("um.searchinventory.form.viapoints.invalid", "viapointsinvalid");
		moduleErrs.setProperty("um.flightschedule.search.invalidcarrier", "invalidcarrier");
		moduleErrs.setProperty("um.searchinventory.form.flight.null", "carriernull");

		clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);

		return clientErrors;

	}
}