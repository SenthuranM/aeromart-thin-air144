package com.isa.thinair.airadmin.core.web.v2.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.commons.api.exception.ModuleException;

/**
 * 
 * @author Pradeep Karunanayake
 * 
 */
public class ErrorMessageUtil {

	private static AiradminConfig airadminConfig = new AiradminConfig();

	public static Map<String, String> getFareRuleErrors(String language) throws ModuleException {
		Properties moduleErrs = new Properties();
		moduleErrs.setProperty("um.farerule.stayoverdays.shouldBe.empty", "StayOverShouldEmpty");
		moduleErrs.setProperty("um.farerule.arrivaltime.shouldBe.empty", "arrivaltimeShouldEmpty");
		moduleErrs.setProperty("um.farerule.arrivaldays.shouldBe.empty", "arrivaldaysShouldEmpty");
		moduleErrs.setProperty("um.farerule.farerule.required", "fareclassRqrd");
		moduleErrs.setProperty("um.farerule.description.required", "descriptionRqrd");
		moduleErrs.setProperty("um.farerule.visibility.required", "visibilityRqrd");
		moduleErrs.setProperty("um.farerule.visibility.Invalid", "visibilityInvalid");
		moduleErrs.setProperty("um.farerule.bookingcode.required", "bookingcodeRqrd");
		moduleErrs.setProperty("um.farerule.depttime.from.required", "depatureFromTimeRqrd");
		moduleErrs.setProperty("um.farerule.depttime.to.required", "depatureToTimeRqrd");
		moduleErrs.setProperty("um.farerule.arrivaltime.from.required", "arrivalFromTimeRqrd");
		moduleErrs.setProperty("um.farerule.arrivaltime.to.required", "arrivalToTimeRqrd");
		moduleErrs.setProperty("um.farerule.maxstay.shouldBe.empty", "maxstayOverShouldEmpty");
		moduleErrs.setProperty("um.farerule.minstay.shouldBe.empty", "minstayOverShouldEmpty");
		moduleErrs.setProperty("um.farerule.staydays.shouldnotBe.minus", "stayOverShouldnotBeminus");
		moduleErrs.setProperty("um.farerule.maxdays.shouldBe.greater", "maxstayOverShouldBeGreater");
		moduleErrs.setProperty("um.farerule.arrivaldays.required", "arrivaldaysRqrd");
		moduleErrs.setProperty("um.farerule.hours.max", "maxhours");
		moduleErrs.setProperty("um.farerule.Minutes.max", "maxMiniutes");
		moduleErrs.setProperty("um.farerule.deptdays.required", "deptdaysRqrd");
		moduleErrs.setProperty("um.farerule.validmonth", "valMonth");
		moduleErrs.setProperty("um.farerule.validminutes", "valMinutes");
		moduleErrs.setProperty("um.farerule.validhours", "valHours");
		moduleErrs.setProperty("um.farerule.validdays", "valDays");
		moduleErrs.setProperty("um.farerule.depttime.wrong", "depttimeFormat");
		moduleErrs.setProperty("um.farerule.arrivaltime.wrong", "arrivaltimeFormat");
		moduleErrs.setProperty("um.farerule.select.arow", "selectRow");
		moduleErrs.setProperty("um.farerule.restriction.required", "restrictionReqd");
		moduleErrs.setProperty("um.farerule.advBKdays.required", "advBKDaysReqd");
		moduleErrs.setProperty("um.farerule.faretype.required", "fareTypeReqd");
		moduleErrs.setProperty("um.farerule.CurrencyFormat.incorrect", "currFormatWrong");
		moduleErrs.setProperty("um.farerule.Comments.exceeds", "cmtsExceeds");
		moduleErrs.setProperty("um.farerule.modify.buffertime.shouldBe.greater", "modBufTimeShouldBeGreater");
		moduleErrs.setProperty("um.farerule.modify.buffertime.wrong", "modBufTimeFormat");
		moduleErrs.setProperty("um.farerule.ArrFrom.exceeds", "ArrFromTmExceeds");
		moduleErrs.setProperty("um.farerule.deptFrom.exceeds", "DeptFromTmExceeds");
		moduleErrs.setProperty("um.airadmin.delete.confirmation", "deleteMessage");
		moduleErrs.setProperty("um.fare.advanceBk.nan", "advanceBkNan");
		moduleErrs.setProperty("um.fare.cancellation.nan", "cancellationNaN");
		moduleErrs.setProperty("um.fare.mod.nan", "modNaN");
		moduleErrs.setProperty("um.fare.maxStay.nan", "maxStayNaN");
		moduleErrs.setProperty("um.fare.minStay.nan", "minStayNaN");
		moduleErrs.setProperty("um.fare.maxStay.required", "maxStayRequired");
		moduleErrs.setProperty("um.fare.confPeriod.nan", "confPeriodNaN");
		moduleErrs.setProperty("um.fare.confPeriod.larger", "confPeriodLarger");
		moduleErrs.setProperty("um.fare.agents.required", "agentsRqrd");
		moduleErrs.setProperty("um.flexi.alloctype.notallowed", "flexiallocfailed");
		moduleErrs.setProperty("um.farerule.comments.invalid.char", "invalidCharacter");
		moduleErrs.setProperty("um.fare.can.type.not.selected", "cancTypeNotSelected");
		moduleErrs.setProperty("um.fare.mod.type.not.selected", "modTypeNotSelected");
		moduleErrs.setProperty("um.fare.min.nan", "typeMinNaN");
		moduleErrs.setProperty("um.fare.max.nan", "typeMaxNaN");
		moduleErrs.setProperty("um.fare.mod.precentage", "modPrecentage");
		moduleErrs.setProperty("um.fare.canc.precentage", "cancPrecentage");
		moduleErrs.setProperty("um.fare.mod.min.max.empty", "modMinMaxEmpty");
		moduleErrs.setProperty("um.fare.canc.min.max.empty", "cancMinMaxEmpty");
		moduleErrs.setProperty("um.fare.mod.min.max.greater", "modMaxShouldBeGreater");
		moduleErrs.setProperty("um.fare.canc.min.max.greater", "cancMaxShouldBeGreater");
		moduleErrs.setProperty("um.fare.max.mod.greater.zero", "maxModZero");
		moduleErrs.setProperty("um.fare.max.canc.greater.zero", "maxCancZero");
		moduleErrs.setProperty("um.fare.lf.required", "lfReequired");
		moduleErrs.setProperty("um.fare.lf.min.greater.zero", "lfMinZero");
		moduleErrs.setProperty("um.fare.lf.max.greater.zero", "lfMaxZero");
		moduleErrs.setProperty("um.fare.lf.min.greater.max", "lfMinGrMax");
		moduleErrs.setProperty("um.fare.fareRuleCode.invalid", "invalidFareRuleCode");
		moduleErrs.setProperty("um.fare.fareRuleDescription.invalid", "invalidFareRuleDescription");
		moduleErrs.setProperty("um.currency.form.currencycode.empty", "localCurrencyCodeRqrd");
		moduleErrs.setProperty("um.fare.noshow.breakPoint.empty", "noShowBreakPointEmpty");
		moduleErrs.setProperty("um.fare.noshow.boundary.empty", "noShowBoundaryEmpty");
		moduleErrs.setProperty("um.fare.noshow.charge.percentage", "noShowChargePercentage");
		moduleErrs.setProperty("um.fare.noshow.charge.boundary.greater.breakpoint", "noShowBoundaryBreakPoint");
		moduleErrs.setProperty("um.farerule.agent.commission.value.required", "commissionValueReq");
		moduleErrs.setProperty("um.farerule.agent.commission.value.invalid", "commissionValueInvalid");
		moduleErrs.setProperty("um.farerule.agent.commission.value.exceeds", "commissionValueExceeds");
		moduleErrs.setProperty("um.farerule.agent.commission.type.required", "commissionTypeReq");
		moduleErrs.setProperty("um.farerule.visibility.invalid.selection", "visibilityInvalidSelection");
		moduleErrs.setProperty("um.farerule.pos.required", "posRequired");
		moduleErrs.setProperty("um.farerule.bulkticket.minimim.seats.required", "btMinSeatsRqrd");
		moduleErrs.setProperty("um.farerule.bulkticket.minimim.seats.incorrect", "btMinSeatsIncorrect");

		return createClientErrors(moduleErrs, language);
	}

	public static Map<String, String> getFareErrors(String language) throws ModuleException {
		Properties moduleErrs = new Properties();

		moduleErrs.setProperty("um.viewfare.origin.required", "originRqrd");
		moduleErrs.setProperty("um.viewfare.destination.required", "destinationRqrd");
		moduleErrs.setProperty("um.viewfare.carrier.required", "carrierRqrd");
		moduleErrs.setProperty("um.viewfare.sod.required", "sodRqrd");
		moduleErrs.setProperty("um.viewfare.fc1.required", "fc1Rqrd");
		moduleErrs.setProperty("um.viewfare.todate.required", "todateRqrd");
		moduleErrs.setProperty("um.viewfare.fromdate.required", "fromdateRqrd");
		moduleErrs.setProperty("um.viewfare.dates.notvalid", "datesInvalid");

		moduleErrs.setProperty("um.viewfare.origindest.cantbe.equal", "originDestEqual");
		moduleErrs.setProperty("um.fare.aed.zero", "aedZero");
		moduleErrs.setProperty("um.viewfare.AED.required", "AEDRqrd");

		moduleErrs.setProperty("um.viewfare.via.not.equal", "viaPointsEqual");
		moduleErrs.setProperty("um.farerule.stayoverdays.shouldBe.empty", "StayOverShouldEmpty");
		moduleErrs.setProperty("um.farerule.arrivaltime.shouldBe.empty", "arrivaltimeShouldEmpty");
		moduleErrs.setProperty("um.farerule.arrivaldays.shouldBe.empty", "arrivaldaysShouldEmpty");
		moduleErrs.setProperty("um.farerule.farerule.required", "fareclassRqrd");
		moduleErrs.setProperty("um.farerule.description.required", "descriptionRqrd");
		moduleErrs.setProperty("um.farerule.visibility.required", "visibilityRqrd");
		moduleErrs.setProperty("um.farerule.visibility.Invalid", "visibilityInvalid");
		moduleErrs.setProperty("um.farerule.bookingcode.required", "bookingcodeRqrd");
		moduleErrs.setProperty("um.farerule.depttime.from.required", "depatureFromTimeRqrd");
		moduleErrs.setProperty("um.farerule.depttime.to.required", "depatureToTimeRqrd");

		moduleErrs.setProperty("um.farerule.arrivaltime.from.required", "arrivalFromTimeRqrd");
		moduleErrs.setProperty("um.farerule.arrivaltime.to.required", "arrivalToTimeRqrd");
		moduleErrs.setProperty("um.farerule.maxstay.shouldBe.empty", "maxstayOverShouldEmpty");

		moduleErrs.setProperty("um.farerule.minstay.shouldBe.empty", "minstayOverShouldEmpty");
		moduleErrs.setProperty("um.farerule.staydays.shouldnotBe.minus", "stayOverShouldnotBeminus");
		moduleErrs.setProperty("um.farerule.maxdays.shouldBe.greater", "maxstayOverShouldBeGreater");
		moduleErrs.setProperty("um.farerule.arrivaldays.required", "arrivaldaysRqrd");
		moduleErrs.setProperty("um.fare.confPeriod.nan", "confPeriodNaN");
		moduleErrs.setProperty("um.fare.maxStay.required", "maxStayRequired");
		moduleErrs.setProperty("um.fare.confPeriod.larger", "confPeriodLarger");

		moduleErrs.setProperty("um.farerule.deptdays.required", "deptdaysRqrd");

		moduleErrs.setProperty("um.farerule.depttime.wrong", "depttimeFormat");
		moduleErrs.setProperty("um.farerule.arrivaltime.wrong", "arrivaltimeFormat");

		moduleErrs.setProperty("um.farerule.select.arow", "selectRow");
		moduleErrs.setProperty("um.fare.fromdate.exceeds.today", "FromDateExceedsCurrentDate");

		moduleErrs.setProperty("um.farerule.restriction.required", "restrictionReqd");
		moduleErrs.setProperty("um.farerule.advBKdays.required", "advBKDaysReqd");
		moduleErrs.setProperty("um.farerule.faretype.required", "fareTypeReqd");
		moduleErrs.setProperty("um.fare.bc.required", "BCnull");
		moduleErrs.setProperty("um.viewfare.arriavlVia.canNotBeSame", "arriavlViaSame");
		moduleErrs.setProperty("um.viewfare.depatureVia.canNotBeSame", "depatureViaSame");

		moduleErrs.setProperty("um.viewfare.via.shouldNotBeEqual", "viaEqual");

		moduleErrs.setProperty("um.viewfare.addHocFare.restriction", "addHocFareRest");

		moduleErrs.setProperty("um.farerule.ArrFrom.exceeds", "ArrFromTmExceeds");
		moduleErrs.setProperty("um.farerule.deptFrom.exceeds", "DeptFromTmExceeds");

		moduleErrs.setProperty("um.fare.masterfare.required", "masterFareRqrd");
		moduleErrs.setProperty("um.fare.fromdate.format.incorrect", "fromDateIncorrect");
		moduleErrs.setProperty("um.fare.todate.format.incorrect", "toDateIncorrect");

		moduleErrs.setProperty("um.viewfare.origin.notExists", "OriginDoesNotExists");
		moduleErrs.setProperty("um.viewfare.destination.notExists", "DestinationDoesNotExists");
		moduleErrs.setProperty("um.viewfare.vial.notExists", "Via1DoesNotExists");
		moduleErrs.setProperty("um.viewfare.via2.notExists", "Via2DoesNotExists");
		moduleErrs.setProperty("um.viewfare.via3.notExists", "Via3DoesNotExists");
		moduleErrs.setProperty("um.viewfare.via4.notExists", "Via4DoesNotExists");

		moduleErrs.setProperty("um.fare.Origin.not.active", "OriginNotActive");
		moduleErrs.setProperty("um.fare.Destination.not.active", "DestinationNotActive");
		moduleErrs.setProperty("um.fare.Via1.not.active", "Via1NotActive");
		moduleErrs.setProperty("um.fare.Via2.not.active", "Via2NotActive");
		moduleErrs.setProperty("um.fare.Via3.not.active", "Via3NotActive");
		moduleErrs.setProperty("um.fare.Via4.not.active", "Via4NotActive");

		moduleErrs.setProperty("um.fare.fareRule.inactive", "FareRuleInactive");
		moduleErrs.setProperty("um.airadmin.delete.confirmation", "deleteMessage");
		moduleErrs.setProperty("um.fare.bc.inactive", "bcInactive");

		moduleErrs.setProperty("um.fare.advanceBk.nan", "advanceBkNan");
		moduleErrs.setProperty("um.fare.cancellation.nan", "cancellationNaN");
		moduleErrs.setProperty("um.fare.mod.nan", "modNaN");
		moduleErrs.setProperty("um.fare.maxStay.nan", "maxStayNaN");
		moduleErrs.setProperty("um.fare.minStay.nan", "minStayNaN");
		moduleErrs.setProperty("um.fare.amount.nan", "amountNan");

		moduleErrs.setProperty("um.fare.toDate.less", "toDateLess");

		moduleErrs.setProperty("um.fare.agents.required", "agentsRqrd");
		moduleErrs.setProperty("um.fare.alloctype.noreturn", "noAlloctype");
		moduleErrs.setProperty("um.fare.alloctype.nooneway", "returnonly");
		moduleErrs.setProperty("um.fare.alloctype.segonly", "segonly");
		moduleErrs.setProperty("um.fare.alloctype.cononly", "cononly");
		moduleErrs.setProperty("um.flexi.alloctype.notallowed", "flexiallocfailed");

		moduleErrs.setProperty("um.farerule.hours.max", "maxhours");
		moduleErrs.setProperty("um.farerule.Minutes.max", "maxMiniutes");
		moduleErrs.setProperty("um.fare.paxcat.notmatch", "catNotmatch");
		moduleErrs.setProperty("um.fare.farecat.notmatch", "fareCatNotmatch");

		moduleErrs.setProperty("um.farerule.modify.buffertime.shouldBe.greater", "modBufTimeShouldBeGreater");
		moduleErrs.setProperty("um.farerule.modify.buffertime.wrong", "modBufTimeFormat");

		moduleErrs.setProperty("um.viewfare.slstodate.required", "slstodateRqrd");
		moduleErrs.setProperty("um.viewfare.slsfromdate.required", "slsfromdateRqrd");
		moduleErrs.setProperty("um.fare.slsfromdate.format.incorrect", "slsfromDateIncorrect");
		moduleErrs.setProperty("um.fare.slstodate.format.incorrect", "slstoDateIncorrect");
		moduleErrs.setProperty("um.fare.slstodate.exceeds.todate", "slstoDateexceeds");
		moduleErrs.setProperty("um.fare.slsfromdate.exceeds.todate", "slsfromDateexceeds");
		moduleErrs.setProperty("um.fare.slsfromdate.exceeds.current", "slsfrmDteexceedscurr");

		moduleErrs.setProperty("um.viewfare.rttodate.required", "rttodateRqrd");
		moduleErrs.setProperty("um.viewfare.rtfromdate.required", "rtfromdateRqrd");
		moduleErrs.setProperty("um.fare.rtfromdate.format.incorrect", "rtfromDateIncorrect");
		moduleErrs.setProperty("um.fare.rttodate.format.incorrect", "rttoDateIncorrect");
		moduleErrs.setProperty("um.fare.fromdate.exceeds.rtfromdate", "fromDateexceedsrtFrom");

		moduleErrs.setProperty("um.farerule.comments.invalid.char", "invalidCharacter");

		moduleErrs.setProperty("um.fare.can.type.not.selected", "cancTypeNotSelected");
		moduleErrs.setProperty("um.fare.mod.type.not.selected", "modTypeNotSelected");
		moduleErrs.setProperty("um.fare.min.nan", "typeMinNaN");
		moduleErrs.setProperty("um.fare.max.nan", "typeMaxNaN");
		moduleErrs.setProperty("um.fare.mod.precentage", "modPrecentage");
		moduleErrs.setProperty("um.fare.canc.precentage", "cancPrecentage");
		moduleErrs.setProperty("um.fare.mod.min.max.empty", "modMinMaxEmpty");
		moduleErrs.setProperty("um.fare.canc.min.max.empty", "cancMinMaxEmpty");
		moduleErrs.setProperty("um.fare.mod.min.max.greater", "modMaxShouldBeGreater");
		moduleErrs.setProperty("um.fare.canc.min.max.greater", "cancMaxShouldBeGreater");
		moduleErrs.setProperty("um.fare.max.mod.greater.zero", "maxModZero");
		moduleErrs.setProperty("um.fare.max.canc.greater.zero", "maxCancZero");
		/* fare rule modifications and load factor */
		moduleErrs.setProperty("um.fare.lf.required", "lfReequired");
		moduleErrs.setProperty("um.fare.lf.min.greater.zero", "lfMinZero");
		moduleErrs.setProperty("um.fare.lf.max.greater.zero", "lfMaxZero");
		moduleErrs.setProperty("um.fare.lf.min.greater.max", "lfMinGrMax");

		moduleErrs.setProperty("um.fare.fareRule.file.empty", "frFileEmpty");
		moduleErrs.setProperty("um.fare.fareRule.conflicted", "frConflicted");
		moduleErrs.setProperty("um.fare.upload.message", "frUploadSucess");

		moduleErrs.setProperty("um.proration.total.invalid", "totalInvalid");
		moduleErrs.setProperty("um.proration.value.invalid", "valueInvalid");
		moduleErrs.setProperty("um.proration.value.required", "allRequired");
		moduleErrs.setProperty("um.proration.value.requiredOriginDestAndViaPoint", "requiredOriginDestAndViaPoint");
		moduleErrs.setProperty("um.proration.value.hasSameValue", "hasSameValue");
		moduleErrs.setProperty("um.proration.value.mismatchOriginDest", "mismatch");
		moduleErrs.setProperty("um.fare.noshow.breakPoint.empty", "noShowBreakPointEmpty");
		moduleErrs.setProperty("um.fare.noshow.boundary.empty", "noShowBoundaryEmpty");
		moduleErrs.setProperty("um.fare.noshow.charge.percentage", "noShowChargePercentage");
		moduleErrs.setProperty("um.fare.noshow.charge.boundary.greater.breakpoint", "noShowBoundaryBreakPoint");
		moduleErrs.setProperty("um.farerule.agent.commission.value.required", "commissionValueReq");
		moduleErrs.setProperty("um.farerule.agent.commission.value.invalid", "commissionValueInvalid");
		moduleErrs.setProperty("um.farerule.agent.commission.value.exceeds", "commissionValueExceeds");
		moduleErrs.setProperty("um.farerule.agent.commission.type.required", "commissionTypeReq");
		moduleErrs.setProperty("um.farerule.visibility.invalid.selection", "visibilityInvalidSelection");
		moduleErrs.setProperty("um.farerule.pos.required", "posRequired");
		moduleErrs.setProperty("um.farerule.bulkticket.minimim.seats.required", "btMinSeatsRqrd");
		moduleErrs.setProperty("um.farerule.bulkticket.minimim.seats.incorrect", "btMinSeatsIncorrect");

		return createClientErrors(moduleErrs, language);
	}

	@SuppressWarnings("rawtypes")
	public static Map<String, String> createClientErrors(Properties moduleErrs, String strLanguage) throws ModuleException {
		Map<String, String> errorMap = new HashMap<String, String>();
		Set keySet = moduleErrs.keySet();
		for (Iterator iter = keySet.iterator(); iter.hasNext();) {
			String key = (String) iter.next();
			errorMap.put(moduleErrs.getProperty(key), airadminConfig.getMessage(key));
		}
		return errorMap;
	}

	public static String generateClientMessageInUploadingFares(List<String> msgList) {
		StringBuffer sb = new StringBuffer();
		int index = 0;
		for (String msg : msgList) {
			index++;
			String msgArr[] = msg.split("\\|");
			if (msgArr[0].equals(WebConstants.MSG_ERROR)) {
				sb.append(index + " - " + msgArr[1] + "<br>");
			} else {
				sb.append(index + " - " + msgArr[1] + "<br>");
			}

		}
		return sb.toString();
	}

}
