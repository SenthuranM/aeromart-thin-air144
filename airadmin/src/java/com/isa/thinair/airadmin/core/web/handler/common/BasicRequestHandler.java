package com.isa.thinair.airadmin.core.web.handler.common;

import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.AiradminModuleConfig;
import com.isa.thinair.airadmin.core.service.AiradminModuleUtils;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airsecurity.api.model.User;
import com.isa.thinair.auditor.api.model.Audit;
import com.isa.thinair.auditor.api.service.AuditorBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.exception.ModuleRuntimeException;

/**
 * @author Srikantha BasicRequestHandler is the base class for all other request handlers
 * 
 */

public abstract class BasicRequestHandler {
	private static Log log = LogFactory.getLog(BasicRequestHandler.class);
	private static AiradminConfig airadminConfig = new AiradminConfig();

	/**
	 * Sets the Message to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @param userMessage
	 *            the Message
	 * @param msgType
	 *            the Message Type- Error / Confirmation
	 */
	protected static void saveMessage(HttpServletRequest request, String userMessage, String msgType) {
		request.setAttribute(WebConstants.REQ_MESSAGE, userMessage);
		request.setAttribute(WebConstants.MSG_TYPE, msgType);
	}

	/**
	 * Sets the Client Messages to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @param strClientErrors
	 *            the Client Message
	 */
	public static void saveClientErrors(HttpServletRequest request, String strClientErrors) {
		if (strClientErrors == null)
			strClientErrors = "";

		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
	}

	/**
	 * Forward The Actions
	 * 
	 * @param request
	 *            the ServletRequest
	 * @param response
	 *            the ServletResponse
	 * @param page
	 *            the Forwading Page
	 * @throws ServletException
	 *             the ServletException
	 * @throws IOException
	 *             the IOException
	 */
	public static void forward(ServletRequest request, ServletResponse response, String page)
			throws ServletException, IOException {
		RequestDispatcher rd = request.getRequestDispatcher(page);

		if (rd != null) {
			rd.forward(request, response);
		} else {
			log.error("Cannot get RequestDispatcher for " + page);
			response.getWriter().println(airadminConfig.getMessage("um.default.server.error"));
		}
	}

	/**
	 * Sends Messages to Pages
	 * 
	 * @param response
	 *            the ServletResponse
	 * @param responseType
	 *            the Message Type
	 * @param responseData
	 *            the Message
	 * @throws IOException
	 *             the IOException
	 */
	public static void sendMessage(ServletResponse response, String responseType, String responseData) throws IOException {
		StringBuffer strData = new StringBuffer();
		strData.append("arrMsg = new Array();");
		strData.append("arrMsg[0]='" + responseType + "';");
		strData.append("arrMsg[1]='" + responseData + "';");
		response.getWriter().println(strData.toString());
	}

	/**
	 * Checks the Privileges of a User
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @param privilegeId
	 *            the Privilege needs to check
	 * @return true - if Privilege Exists
	 */
	@SuppressWarnings("unchecked")
	public static boolean hasPrivilege(HttpServletRequest request, String privilegeId) {

		Map<String, String> mapPrivileges = (Map<String, String>) request.getSession()
				.getAttribute(WebConstants.SES_PRIVILEGE_IDS);
		boolean has = (mapPrivileges.get(privilegeId) != null) ? true : false;

		return has;
	}

	/**
	 * Checks the Privileges of a User
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @param privilegeId
	 *            the Privilege needs to check
	 */
	@SuppressWarnings("unchecked")
	public static void checkPrivilege(HttpServletRequest request, String privilegeId) {
		Map<String, String> mapPrivileges = (Map<String, String>) request.getSession()
				.getAttribute(WebConstants.SES_PRIVILEGE_IDS);

		if (mapPrivileges.get(privilegeId) == null) {
			log.error("Unauthorize operation:" + request.getUserPrincipal().getName() + ":" + privilegeId);
			throw new ModuleRuntimeException(WebConstants.ERR_UNAUTHORIZED_ACCESS);
		}

	}

	/**
	 * Checks the Privileges of a User
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @param privilegeId
	 *            the Privilege needs to check
	 */
	@SuppressWarnings("unchecked")
	public static void checkOrConditionForPrivileges(HttpServletRequest request, String... privilegeIds) {

		if (privilegeIds.length > 0) {

			Map<String, String> mapPrivileges = (Map<String, String>) request.getSession()
					.getAttribute(WebConstants.SES_PRIVILEGE_IDS);
			boolean privilageFound = false;
			String privilageIdsString = "";

			for (String privilegeId : privilegeIds) {
				privilageIdsString += "," + privilegeId;
				if (mapPrivileges.get(privilegeId) != null) {
					privilageFound = true;
					break;
				}
			}

			if (!privilageFound) {

				privilageIdsString = privilageIdsString.substring(1);
				log.error("Unauthorize operation:" + request.getUserPrincipal().getName() + ":" + privilageIdsString);
				throw new ModuleRuntimeException(WebConstants.ERR_UNAUTHORIZED_ACCESS);
			}
		}

	}

	/**
	 * Checks the Privileges of a User
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @param privilegeId
	 *            the Privilege needs to check
	 */
	@SuppressWarnings("unchecked")
	public static void checkAndConditionForPrivileges(HttpServletRequest request, String... privilegeIds) {

		if (privilegeIds.length > 0) {

			Map<String, String> mapPrivileges = (Map<String, String>) request.getSession()
					.getAttribute(WebConstants.SES_PRIVILEGE_IDS);
			boolean allPrivilagesFound = true;

			String privilageIdsString = "";

			for (String privilegeId : privilegeIds) {
				privilageIdsString += "," + privilegeId;
				if (mapPrivileges.get(privilegeId) == null) {
					privilageIdsString = privilegeId;
					allPrivilagesFound = false;
					break;
				}
			}

			if (!allPrivilagesFound) {

				log.error("Unauthorize operation:" + request.getUserPrincipal().getName() + ":" + privilageIdsString);
				throw new ModuleRuntimeException(WebConstants.ERR_UNAUTHORIZED_ACCESS);
			}
		}

	}

	/**
	 * Sets Attributes to the Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @param attributeName
	 *            the name of the Attribute Name
	 * @param value
	 *            the Attribute value
	 */
	public static void setAttribInRequest(HttpServletRequest request, String attributeName, Object value) {
		request.setAttribute(attributeName, value);
	}

	/**
	 * Gets the Attribute Value from Request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @param attribute
	 *            the Name the name of the Attribute
	 * @return Object the Value of the Attribute
	 */
	public static Object getAttribInRequest(HttpServletRequest request, String attributeName) {
		Object objReturn = null;
		try {
			objReturn = request.getAttribute(attributeName);
		} catch (Exception e) {
			log.error("error in retriving attribute" + e.getMessage());
		}
		return objReturn;
	}

	/**
	 * Put a Audit to the Audit Trail
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @param taskCode
	 *            the Task Code
	 * @throws ModuleException
	 *             the ModuleException
	 */
	@SuppressWarnings("rawtypes")
	public static void audit(HttpServletRequest request, String taskCode) throws ModuleException {
		AuditorBD auditorBD = ModuleServiceLocator.getAuditorServiceBD();
		User user = (User) request.getSession().getAttribute(WebConstants.SES_CURR_USER);

		if (user != null) {
			Audit audit = new Audit();
			String userId = user.getUserId();
			LinkedHashMap mapContents = new LinkedHashMap();

			audit.setUserId(userId);
			audit.setTimestamp(new Date());
			audit.setAppCode(WebConstants.APP_CODE);
			audit.setTaskCode(taskCode);

			auditorBD.audit(audit, mapContents);
		}

	}

	/**
	 * Gets a secure URL for the Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Secure url
	 */
	public static String getSecureUrl(HttpServletRequest request) {
		StringBuffer sbRedirect = new StringBuffer("");
		AiradminModuleConfig config = AiradminModuleUtils.getConfig();
		String strRedirect = null;
		String pageUrl = request.getRequestURI();
		String server = request.getServerName();
		String port = null;
		if (config.isUseSecureLogin()) {
			sbRedirect.append(config.getSecureProtocol() + "://");
			port = config.getSslPort();
		} else {
			sbRedirect.append(config.getNonSecureProtocol() + "://");
			port = config.getPort();
		}

		sbRedirect.append(server);

		if (port != null && !"".equals(port)) {
			sbRedirect.append(":" + port);
		}
		sbRedirect.append(pageUrl);
		strRedirect = sbRedirect.toString();
		return strRedirect;
	}

	/**
	 * Gets a Non Secure URL
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the NON Secure URL
	 */
	public static String getNonSecureUrl(HttpServletRequest request) {
		StringBuffer sbRedirect = new StringBuffer("");
		AiradminModuleConfig config = AiradminModuleUtils.getConfig();
		String strRedirect = null;
		String pageUrl = request.getRequestURI();
		String server = request.getServerName();

		sbRedirect.append(config.getNonSecureProtocol() + "://");
		String httpsPort = config.getPort();
		sbRedirect.append(server);

		if (httpsPort != null && !"".equals(httpsPort)) {
			sbRedirect.append(":" + httpsPort);
		}
		sbRedirect.append(pageUrl);
		strRedirect = sbRedirect.toString();
		return strRedirect;
	}

	/**
	 * Get the Context Path
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Context path
	 */
	public static String getContextPath(HttpServletRequest request) {
		StringBuffer sbRedirect = new StringBuffer("");
		AiradminModuleConfig config = AiradminModuleUtils.getConfig();
		String server = request.getServerName();
		String port = config.getPort();
		String contextPath = null;

		sbRedirect.append(config.getNonSecureProtocol() + "://");
		sbRedirect.append(server);

		if (port != null && !"".equals(port)) {
			sbRedirect.append(":" + port);
		}

		sbRedirect.append(request.getContextPath());
		contextPath = sbRedirect.toString();
		return contextPath;
	}

	/**
	 * checks Both Secure and Non secure Ports are Same
	 * 
	 * @return boolen true-same false-not
	 */
	public static boolean isProtocolStatusEqual() {
		AiradminModuleConfig config = AiradminModuleUtils.getConfig();
		return config.getNonSecureProtocol().equals(config.getSecureProtocol());

	}

	public static Collection<String> getUserCarrierCodes(HttpServletRequest request) throws ModuleException {
		User user = (User) request.getSession().getAttribute(WebConstants.SES_CURR_USER);
		if (user.getCarriers() == null || user.getCarriers().size() < 1) {
			throw new ModuleException("module.user.carrier");
		}
		return user.getCarriers();
	}

	/**
	 * 
	 * 
	 * @param request
	 * @param userMessage
	 * @param msgType
	 */
	protected static void saveMessageForPopUp(HttpServletRequest request, String userMessage, String msgType) {
		request.setAttribute(WebConstants.REQ_POPUPMESSAGE, userMessage);
		request.setAttribute(WebConstants.MSG_TYPE, msgType);
	}

}
