package com.isa.thinair.airadmin.core.web.generator.inventory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airinventory.api.dto.seatmap.FlightSeatsDTO;
import com.isa.thinair.airinventory.api.dto.seatmap.SeatDTO;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.commons.api.exception.ModuleException;

public class LoadSegmentsHg {

	/**
	 * Creates the Attached Template codes
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getFlightData(HttpServletRequest request) throws ModuleException {
		String strFlightId = request.getParameter("fltId");
		StringBuilder sb = new StringBuilder();
		if (strFlightId != null && !strFlightId.equals("")) {
			Integer fltId = new Integer(strFlightId);
			Collection<FlightSegement> colSegs = ModuleServiceLocator.getFlightServiceBD().getSegments(fltId);
			Collection<FlightSeatsDTO> colSegtempl = ModuleServiceLocator.getSeatMapBD().getTempleateIds(fltId);
			Map<Integer,Integer> flightIdToTemplateIdMap = new HashMap<Integer, Integer>();
			
			for(FlightSeatsDTO dto : colSegtempl){
				flightIdToTemplateIdMap.put(dto.getFlightSegmentID(), dto.getTemplateId());
			}			
			
			if (colSegs != null) {
				int i = 0;
				for (FlightSegement segment : colSegs) {
					int segId = segment.getFltSegId();
					sb.append("arrSeatSegData[" + i + "] = new Array();");
					sb.append("arrSeatSegData[" + i + "][1] = '" + segment.getSegmentCode() + "';");
					sb.append("arrSeatSegData[" + i + "][2] = '" + segment.getFltSegId() + "';");
			//		sb.append("arrSeatSegData[" + i + "][3] = '';");
			//		sb.append("arrSeatSegData[" + i + "][4] = '';");					
					
										
					if (colSegtempl != null && colSegtempl.size() > 0) {
					//	FlightSeatsDTO fltSeatDTO = (FlightSeatsDTO) colSegtempl.toArray()[0];
					//	int seatSegId = fltSeatDTO.getFlightSegmentID();
					//	if (seatSegId == segId) {
						int templateId = flightIdToTemplateIdMap.get(segId);
							sb.append("arrSeatSegData[" + i + "][3] = '" + templateId + "';");
							sb.append("arrSeatSegData[" + i
									+ "][4] = '<a href = \"javascript:void(0)\" onclick=\"parent.showEdit("
									+ segment.getFltSegId() + "," + templateId + "," + strFlightId
									+ ")\">Edit</a>';");
					//	}
					}

					i++;
				}
			}
		}

		request.setAttribute("seatFlightId", strFlightId);
		return sb.toString();
	}

	/**
	 * Method to set the EditData
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	public static String editData(HttpServletRequest request) throws ModuleException {

		String strSegId = request.getParameter("segId");
		String strTemplateId = request.getParameter("templateId");
		String strFlightId = request.getParameter("flightId");
		String strTemplateName = request.getParameter("templateName");

		Collection<SeatDTO> seatCollection = null;
		Map<Integer, FlightSeatsDTO> seatMap = new HashMap<Integer, FlightSeatsDTO>();
		FlightSeatsDTO seatTo = null;
		if (strSegId != null) {
			List<Integer> flightSegIds = new ArrayList<Integer>();
			flightSegIds.add(new Integer(strSegId));
			seatMap = ModuleServiceLocator.getSeatMapBD().getFlightSeats(flightSegIds, null, false);
			seatTo = seatMap.get(new Integer(strSegId));
			if (seatTo != null) {
				seatCollection = seatTo.getSeats();
			}
		}
		request.setAttribute("reqTemplatId", strTemplateId);
		request.setAttribute("flightId", strFlightId);
		request.setAttribute("reqFltSegId", strSegId);
		request.setAttribute("reqTemplatName", strTemplateName);
		return createSeatDetailsHTML(seatCollection);
	}

	/**
	 * Create the Seat Details Array
	 * 
	 * @param seatCollection
	 *            the Collection of SeatDTo
	 * @return String the js array
	 * @throws ModuleException
	 *             the ModuleException
	 */
	@SuppressWarnings("unused")
	private static String createSeatDetailsHTML(Collection<SeatDTO> seatCollection) throws ModuleException {

		StringBuilder sb = new StringBuilder();
		StringBuilder ssb = new StringBuilder();
		SeatDTO aircraftSeatMod = null;
		String strCabinclassCode = null;
		String strLogicalCCCode = null;
		String strprevCabinclass = "";
		String strSeatCode = null;
		String strStatus = null;
		String seatVisibility = "Y";
		BigDecimal bdChrgeAmt = null;
		String strType = null;
		int intChargeId = 0;
		int intAmSeatId = 0;

		int colGroupId = 0;
		int prevColGrpId = -1;
		int columnId = 0;
		int prevColumnId = -1;
		int rowGroupId = 0;
		int prevRowGroupId = -1;
		int rowId = 0;
		int prevRowId = -1;
		int seatId = 0;

		int clsInc = -1;
		int rowgpInc = -1;
		int colgpInc = -1;
		int rowInc = -1;
		int colInc = -1;
		int seatInc = 0;

		sb.append("stMap = new Array();");

		if (seatCollection != null) {
			Iterator<SeatDTO> iter = seatCollection.iterator();
			while (iter.hasNext()) {
				aircraftSeatMod = iter.next();
				if (aircraftSeatMod != null) {
					strLogicalCCCode = aircraftSeatMod.getLogicalCabinClassCode();
					colGroupId = aircraftSeatMod.getColGroupId();
					columnId = aircraftSeatMod.getColId();
					rowGroupId = aircraftSeatMod.getRowGroupId();
					rowId = aircraftSeatMod.getRowId();
					strSeatCode = aircraftSeatMod.getSeatCode();
					seatId = aircraftSeatMod.getSeatID();
					strStatus = aircraftSeatMod.getStatus();
					bdChrgeAmt = aircraftSeatMod.getChargeAmount();
					intChargeId = aircraftSeatMod.getChargeID();
					intAmSeatId = aircraftSeatMod.getFlightAmSeatID();
					strType = aircraftSeatMod.getSeatType();
					seatVisibility = aircraftSeatMod.getSeatVisibility();

					strCabinclassCode = aircraftSeatMod.getCabinClassCode();

					if (!strCabinclassCode.equals(strprevCabinclass)) {
						clsInc++;
						colgpInc = -1;
						prevColGrpId = -1;
						strprevCabinclass = strCabinclassCode;
						sb.append(" stMap [" + clsInc + "] = new Array();");
					}
					if (prevColGrpId != colGroupId) {
						colgpInc++;
						rowgpInc = -1;
						prevRowGroupId = -1;
						prevColGrpId = colGroupId;
						sb.append(" stMap [" + clsInc + "][" + colgpInc + "] = new Array();");
					}
					if (prevRowGroupId != rowGroupId) {
						rowgpInc++;
						rowInc = -1;
						prevRowId = -1;
						prevRowGroupId = rowGroupId;
						sb.append(" stMap [" + clsInc + "][" + colgpInc + "][" + rowgpInc + "] = new Array();");
					}

					if (prevRowId != rowId) {
						rowInc++;
						colInc = -1;
						prevRowId = rowId;
						sb.append(" stMap [" + clsInc + "][" + colgpInc + "][" + rowgpInc + "][" + rowInc + "] = new Array();");
					}
					colInc++;
					sb.append(" stMap [" + clsInc + "][" + colgpInc + "][" + rowgpInc + "][" + rowInc + "][" + colInc
							+ "] = new Array('" + seatId + "','" + strSeatCode + "','" + strStatus + "','" + bdChrgeAmt + "','"
							+ intChargeId + "','" + intAmSeatId + "','" + strType + "','" + strLogicalCCCode + "','"
							+ seatVisibility + "');");

					ssb.append("seatCharges[" + seatInc + "] =new Array('" + intChargeId + "','" + seatId + "','','" + bdChrgeAmt
							+ "','" + strStatus.substring(0, 1) + "','');");
					seatInc++;
				}

			}
		}
		sb.append(ssb.toString());
		return sb.toString();
	}

}