package com.isa.thinair.airadmin.core.web.v2.action.pricing;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airpricing.api.criteria.AgentFareRuleSearchCriteria;
import com.isa.thinair.airpricing.api.dto.AgentFareRulesDTO;
import com.isa.thinair.airpricing.api.dto.FareRuleSummaryDTO;
import com.isa.thinair.airpricing.api.service.FareRuleBD;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.api.exception.ModuleRuntimeException;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

/**
 * 
 * @author Pradeep Karunanayake
 * 
 */
@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
@SuppressWarnings("rawtypes")
public class AgentFareRuleAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(AgentFareRuleAction.class);

	private boolean success = true;

	private String messageTxt;

	private String agentCode;

	private Collection rows;

	private List agentList;

	public String execute() {
		String forward = S2Constants.Result.SUCCESS;
		try {
			checkPrivilege(request, WebConstants.PLAN_FARES_RULE_AGENT);
			agentList = SelectListGenerator.createAgentCodesList();
		} catch (ModuleException ex) {
			log.error(ex);
			success = false;
			messageTxt = ex.getMessageString();
		} catch (Exception ex) {
			log.error(ex);
			success = false;
			messageTxt = ex.getMessage();
		}
		return forward;
	}

	public String searchAgentData() {
		String forward = S2Constants.Result.SUCCESS;
		try {
			Collection<AgentFareRulesDTO> colAgentFares = null;
			AgentFareRuleSearchCriteria agentCriteria = new AgentFareRuleSearchCriteria();
			FareRuleBD fareRuleBD = ModuleServiceLocator.getRuleBD();

			if (agentCode != null && !(agentCode.equals("")) && !(agentCode.equals("All"))) {
				agentCriteria.setAgentCode(agentCode);
			}
			if (agentCode != null && !(agentCode.equals(""))) {
				colAgentFares = fareRuleBD.getAgentFareRules(agentCriteria, null);
			}
			rows = createAgentFareRuleGridData(colAgentFares);
		} catch (ModuleException ex) {
			log.error(ex);
			success = false;
			messageTxt = ex.getMessageString();
		} catch (Exception ex) {
			log.error(ex);
			success = false;
			messageTxt = ex.getMessage();
		}
		return forward;
	}

	/**
	 * Checks the Privileges of a User
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @param privilegeId
	 *            the Privilege needs to check
	 */
	public static void checkPrivilege(HttpServletRequest request, String privilegeId) {
		Map mapPrivileges = (Map) request.getSession().getAttribute(WebConstants.SES_PRIVILEGE_IDS);

		if (mapPrivileges.get(privilegeId) == null) {
			log.error("Unauthorize operation:" + request.getUserPrincipal().getName() + ":" + privilegeId);
			throw new ModuleRuntimeException(WebConstants.ERR_UNAUTHORIZED_ACCESS);
		}

	}

	// TODO merge two fare rule collections
	@SuppressWarnings("unchecked")
	private Collection createAgentFareRuleGridData(Collection<AgentFareRulesDTO> agentFareRules) throws ModuleException {
		List fareRules = new ArrayList();
		Map ruleMap = null;
		int id = 0;
		String strFareExists = "";
		String agentName = "";
		String fareRuleStr = "";
		String fareRuleDes = "";
		String fareRuleBasicCode = "";

		if (agentFareRules != null) {
			FareRuleBD fareRuleBD = ModuleServiceLocator.getRuleBD();
			for (AgentFareRulesDTO agent : agentFareRules) {
				Collection<FareRuleSummaryDTO> fareRulesDto = agent.getFareRulesSummary();
				if (fareRulesDto != null) {
					for (FareRuleSummaryDTO summaryDTO : fareRulesDto) {
						if (summaryDTO != null) {
							ruleMap = new HashMap();
							ruleMap.put("id", ++id);
							ruleMap.put("agentCode", agent.getAgentCode() != null ? agent.getAgentCode() : "");
							if (agent.getAgentName() != null) {
								if (agent.getAgentName().length() > 25) {
									String name1 = agent.getAgentName().substring(0, 24);
									String name2 = agent.getAgentName().substring(25);
									String nameFullFull = name1 + " " + name2;
									agentName = nameFullFull;
								} else {
									agentName = agent.getAgentName();
								}
							}

							if (summaryDTO.isLinkedToNonExpiredFare()) {
								strFareExists = "**";
							} else {
								strFareExists = "";
							}
							if (summaryDTO.getFareRuleCode() == null) {
								fareRuleStr = "&nbsp;" + strFareExists;
							} else {
								fareRuleStr = summaryDTO.getFareRuleCode() + strFareExists;
							}
							if (summaryDTO.getDescription() == null) {
								fareRuleDes = "&nbsp;";
							} else {
								fareRuleDes = summaryDTO.getDescription();
							}
							if (summaryDTO.getFareBasisCode() == null) {
								fareRuleBasicCode = "&nbsp;";
							} else {
								fareRuleBasicCode = summaryDTO.getFareBasisCode();
							}

							ruleMap.put("agentName", agentName);
							ruleMap.put("fareRule", fareRuleStr);
							ruleMap.put("fareRuleDesc", fareRuleDes);
							ruleMap.put("basisCode", fareRuleBasicCode);
							fareRules.add(ruleMap);
						}
					}
				}

				Collection<FareRuleSummaryDTO> addHocrules = agent.getAdhocFareRulesSummary();
				if (addHocrules != null) {
					for (FareRuleSummaryDTO summaryDTO : addHocrules) {
						if (summaryDTO != null) {
							ruleMap = new HashMap();
							ruleMap.put("id", ++id);
							ruleMap.put("agentCode", agent.getAgentCode() != null ? agent.getAgentCode() : "");
							if (agent.getAgentName() != null) {
								if (agent.getAgentName().length() > 25) {
									String name1 = agent.getAgentName().substring(0, 24);
									String name2 = agent.getAgentName().substring(25);
									String nameFullFull = name1 + " " + name2;
									agentName = nameFullFull;
								} else {
									agentName = agent.getAgentName();
								}
							}

							if (summaryDTO.isLinkedToNonExpiredFare()) {
								strFareExists = "**";
							} else {
								strFareExists = "";
							}
							if (summaryDTO.getFareRuleCode() == null) {
								fareRuleStr = "&nbsp;" + strFareExists;
							} else {
								fareRuleStr = summaryDTO.getFareRuleCode() + strFareExists;
							}
							if (summaryDTO.getDescription() == null) {
								fareRuleDes = "&nbsp;";
							} else {
								fareRuleDes = summaryDTO.getDescription();
							}
							if (summaryDTO.getFareBasisCode() == null) {
								fareRuleBasicCode = "&nbsp;";
							} else {
								fareRuleBasicCode = summaryDTO.getFareBasisCode();
							}

							ruleMap.put("agentName", agentName);
							ruleMap.put("fareRule", fareRuleStr);
							ruleMap.put("fareRuleDesc", fareRuleDes);
							ruleMap.put("basisCode", fareRuleBasicCode);
							fareRules.add(ruleMap);
						}
					}
				}
			}
		}
		return fareRules;
	}

	public Collection getRows() {
		return rows;
	}

	public void setRows(Collection rows) {
		this.rows = rows;
	}

	public boolean isSuccess() {
		return success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public List getAgentList() {
		return agentList;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

}
