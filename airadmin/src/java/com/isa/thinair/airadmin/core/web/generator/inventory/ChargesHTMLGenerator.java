package com.isa.thinair.airadmin.core.web.generator.inventory;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airmaster.api.model.State;
import com.isa.thinair.airpricing.api.criteria.ChargeSearchCriteria;
import com.isa.thinair.airpricing.api.model.Charge;
import com.isa.thinair.airpricing.api.model.ChargeAgent;
import com.isa.thinair.airpricing.api.model.ChargeApplicableCharges;
import com.isa.thinair.airpricing.api.model.ChargePos;
import com.isa.thinair.airpricing.api.model.ChargeRate;
import com.isa.thinair.airpricing.api.service.ChargeBD;
import com.isa.thinair.commons.api.dto.ChargeApplicabilityTO;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

public class ChargesHTMLGenerator {
	private static String clientErrors;

	private static Log log = LogFactory.getLog(ChargesHTMLGenerator.class);

	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();

	/**
	 * Gets the Charge Row Html For Search Critiriea
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array of Charge Grid Rows
	 * @throws ModuleException
	 *             the ModuleException
	 */
	@SuppressWarnings("unchecked")
	public final String getChargesRowHtml(HttpServletRequest request) throws ModuleException {
		log.debug("Inside charges getChargesRowHtml() method");

		String chargeCode = request.getParameter("selChargeCode");
		String group = request.getParameter("selGroup");
		String category = request.getParameter("selCategory");
		String status = request.getParameter("selStatus");
		String from = request.getParameter("txtDateFrom");
		String to = request.getParameter("txtDateTo");
		String strMode = request.getParameter("hdnMode");
		String raetCat = request.getParameter("radRateCat");

		if (strMode != null) {
			if (strMode.equals("DELETE")) {
				chargeCode = "All";
				group = "All";
				category = "All";
				raetCat = "WithDateRange";
				to = "";
				from = "";
			}
		}

		String gridData = null;
		Collection<Charge> colCharges = null;
		ChargeSearchCriteria chargeSearchCriteria = new ChargeSearchCriteria();

		List<String> order = new ArrayList<String>();
		// order.add(InternalConstants.ChargesColumnNames.CHARGE_CODE);
		order.add("charge.chargeCode");

		int RecNo = 0;
		int totRec = 0;
		String strJavascriptTotalNoOfRecs = "";

		// try{
		ChargeBD chargeBD = ModuleServiceLocator.getChargeBD();
		Page page = null;

		if (request.getParameter("hdnRecNo") == null || request.getParameter("hdnRecNo").equals("")) {
			RecNo = 0;

		} else {
			RecNo = Integer.parseInt(request.getParameter("hdnRecNo")) - 1;
		}

		if ((chargeCode != null) && !(chargeCode.equals("")) && !(chargeCode.equals("All"))) {
			chargeSearchCriteria.setChargeCode(chargeCode);
		}

		if ((category != null) && !(category.equals("")) && !(category.equals("All"))) {
			chargeSearchCriteria.setChargeCategoryCode(category);
		}

		if (group != null && !(group.equals("")) && !(group.equals("All"))) {
			chargeSearchCriteria.setChargeGroupCode(group);
		}

		chargeSearchCriteria.setOnlyChargesWithoutRate(false);

		if (status != null && !status.equals("")) {
			chargeSearchCriteria.setChargeStatus(status);
		}

		if (raetCat != null) {
			if (raetCat.equals("NoCharges")) {
				chargeSearchCriteria.setOnlyChargesWithoutRate(true);

			} else if (raetCat.equals("WithDateRange")) {
				chargeSearchCriteria.setOnlyChargesWithoutRate(false);

				if (from != null && !(from.equals(""))) {
					String dateArr[] = from.split("/");
					GregorianCalendar cal = new GregorianCalendar(Integer.parseInt(dateArr[2]), Integer.parseInt(dateArr[1]) - 1,
							Integer.parseInt(dateArr[0]));
					chargeSearchCriteria.setFromDate(cal.getTime());
				} else {
					chargeSearchCriteria.setFromDate(null);
				}

				if (to != null && !to.equals("")) {
					String dateArr[] = to.split("/");
					GregorianCalendar cal = new GregorianCalendar(Integer.parseInt(dateArr[2]), Integer.parseInt(dateArr[1]) - 1,
							Integer.parseInt(dateArr[0]));
					chargeSearchCriteria.setToDate(cal.getTime());
				} else {
					chargeSearchCriteria.setToDate(null);
				}

				if ((from == null || from.equals("")) && (to == null || to.equals(""))) {
					chargeSearchCriteria.setOnlyChargesWithoutRate(false);
				}

			} else {
				chargeSearchCriteria.setOnlyChargesWithoutRate(false);
			}

		} else {
			chargeSearchCriteria.setOnlyChargesWithoutRate(false);
		}

		page = chargeBD.getCharges(chargeSearchCriteria, RecNo, 20, order);
		colCharges = page.getPageData();
		totRec = page.getTotalNoOfRecords();
		strJavascriptTotalNoOfRecs = "var totalRes = " + totRec + ";";

		request.setAttribute(WebConstants.REQ_TOTAL_RECORDS, strJavascriptTotalNoOfRecs);
		gridData = createChargesRowHtml(colCharges);
		JavascriptGenerator.createCabinClasses(request);
		log.debug(" charges getChargesRowHtml() method Finished");
		return gridData;
	}

	/**
	 * Creates the CHARGE Grid from a Collection of Charges
	 * 
	 * @param chg
	 *            the Collection of Charges
	 * @return String the Created Charge Grid Array
	 * @throws ModuleException
	 *             the ModuleException
	 */
	@SuppressWarnings({ "unchecked", "unused" })
	private static String createChargesRowHtml(Collection<Charge> chg) throws ModuleException {

		log.debug("Inside charges createChargesRowHtml(col) method");
		DecimalFormat formatter = new DecimalFormat("#.00");
		StringBuffer sb = new StringBuffer();
		Collection<Charge> charges = chg;
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		sb.append("var arrChargeData = new Array();");

		String showFareDefByDepCountryCurrency = globalConfig.getBizParam(SystemParamKeys.SHOW_FARE_DEF_BY_DEP_COUNTRY_CURRENCY)
				.equalsIgnoreCase("Y") ? "Y" : "N";

		if (charges != null) {
			Iterator<Charge> iteCharges = charges.iterator();
			int i = 0;
			while (iteCharges.hasNext()) {
				Charge objCharge = (Charge) iteCharges.next();
				Collection<ChargeRate> colrates1 = objCharge.getChargeRates();
				List<ChargeRate> colrates = new ArrayList<ChargeRate>();

				if (colrates1 != null) {
					Iterator<ChargeRate> colrates1Iterator = colrates1.iterator();
					while (colrates1Iterator.hasNext()) {
						colrates.add(colrates1Iterator.next());
					}

					if (colrates != null) {
						Collections.sort(colrates, new OldestFirstSortComparator());
					}
				}

				sb.append("arrChargeData[" + i + "] = new  Array();");
				sb.append("arrChargeData[" + i + "][0] = " + i + ";");
				sb.append("arrChargeData[" + i + "][1] = '" + objCharge.getChargeCode() + "';");

				if (objCharge.getChargeDescription() == null) {
					sb.append("arrChargeData[" + i + "][2] = '';");
				} else if (objCharge.getChargeDescription().equals("")) {
					sb.append("arrChargeData[" + i + "][2] = '';");
				} else {
					sb.append("arrChargeData[" + i + "][2] = '" + objCharge.getChargeDescription() + "';");
				}

				sb.append("arrChargeData[" + i + "][3] = '" + objCharge.getChargeGroupCode() + "';");
				String categoryValue = objCharge.getChargeCategoryValue();

				String onds = "Applied to OND";

				if (categoryValue == null) {
					if (objCharge.getChargeCategoryCode().equals("OND")) {
						sb.append("arrChargeData[" + i + "][4] ='" + onds + "';");
					} else {
						sb.append("arrChargeData[" + i + "][4] ='';");
					}
				} else {
					if (objCharge.getChargeCategoryCode().equals("OND")) {
						sb.append("arrChargeData[" + i + "][4] ='" + categoryValue + "/ " + onds + "';");

					} else if (objCharge.getChargeCategoryCode().equals("POS")) {
						Set<String> ondSet = objCharge.getOndCodes();
						if (ondSet.size() > 0) {
							sb.append("arrChargeData[" + i + "][4] ='" + categoryValue + "/ " + onds + "';");
						} else {
							sb.append("arrChargeData[" + i + "][4] ='" + categoryValue + "';");
						}
					} else {
						sb.append("arrChargeData[" + i + "][4] ='" + categoryValue + "';");
					}
				}

				sb.append("arrChargeData[" + i + "][5] = '" + objCharge.getChargeCategoryCode() + "';");

				if (colrates != null) {
					if (colrates.size() > 0) {
						sb.append("arrChargeData[" + i + "][6] = 'Y';");
					} else {
						sb.append("arrChargeData[" + i + "][6] = 'N';");
					}
				} else {
					sb.append("arrChargeData[" + i + "][6] = 'N';");
				}

				Map<Integer, ChargeApplicabilityTO> chargeApplicabilitiesMap = globalConfig.getChargeApplicabilities();
				ChargeApplicabilityTO chargeApplicabilityTO = chargeApplicabilitiesMap.get(objCharge.getAppliesTo());

				if (chargeApplicabilityTO == null) {
					throw new ModuleException("platform.data.chargeapplicabilities.inconsistentrecords");
				}

				sb.append("arrChargeData[" + i + "][7] = '" + chargeApplicabilityTO.getAppliesToDesc() + "';");

				int activeCount = 0;
				Iterator<ChargeRate> iteRate = colrates.iterator();
				while (iteRate.hasNext()) {
					ChargeRate rates = (ChargeRate) iteRate.next();
					if (rates.getStatus() != null) {
						if (rates.getStatus().equals(ChargeRate.Status_Active)) {
							activeCount += 1;
						}
					}
				}
				sb.append("arrChargeData[" + i + "][8] = '" + activeCount + "';");

				int refund = objCharge.getRefundableChargeFlag();
				if (refund == Charge.REFUNDABLE_CHARGE) {
					sb.append("arrChargeData[" + i + "][9] = 'Y';");
				} else {
					sb.append("arrChargeData[" + i + "][9] = 'N';");
				}

				Iterator<ChargeRate> rateIte = colrates.iterator();
				sb.append("var arrData = new Array();");
				int e = 0;
				String rateCodes = "";
				String currCode = "";
				while (rateIte.hasNext()) {

					ChargeRate rates = (ChargeRate) rateIte.next();
					sb.append("arrData[" + e + "] = new  Array();");
					sb.append("arrData[" + e + "][0] = '" + e + "';");
					String from = dateFormat.format(rates.getChargeEffectiveFromDate());
					String to = dateFormat.format(rates.getChargeEffectiveToDate());

					sb.append("arrData[" + e + "][1] = '" + from + "';");
					sb.append("arrData[" + e + "][2] = '" + to + "';");
					String flage = rates.getChargeBasis();
					sb.append("arrData[" + e + "][3] = '" + flage + "';");
					sb.append("arrData[" + e + "][4] = '" + rates.getChargeValuePercentage() + "';");
					sb.append("arrData[" + e + "][5] = '" + rates.getChargeValueMin() + "';");
					sb.append("arrData[" + e + "][6] = '" + rates.getChargeValueMax() + "';");
					if (rates.getValueInLocalCurrency() != null) {
						sb.append("arrData[" + e + "][7] = '" + formatter.format(rates.getValueInLocalCurrency()) + "';");
					} else {
						sb.append("arrData[" + e + "][7] = '';");
					}
					sb.append("arrData[" + e + "][8] ='" + rates.getStatus() + "';");// status
					sb.append("arrData[" + e + "][9] ='" + rates.getVersion() + "';");
					sb.append("arrData[" + e + "][10] ='" + rates.getChargeRateCode() + "';");
					sb.append("arrData[" + e + "][11] =" + i + ";");

					// CC charge change
					sb.append("arrData[" + e + "][12] = '" + rates.getOperationType() + "';");
					// HC change
					sb.append("arrData[" + e + "][13] = '" + rates.getJourneyType() + "';");
					// set cabin class code
					sb.append("arrData[" + e + "][14] = '"
							+ (rates.getCabinClassCode() == null ? "-1" : rates.getCabinClassCode()) + "';");
					sb.append("arrData[" + e + "][15] = '" + (rates.getBreakPoint() == null ? "" : rates.getBreakPoint()) + "';");
					sb.append("arrData[" + e + "][16] = '" + (rates.getBoundryValue() == null ? "" : rates.getBoundryValue())
							+ "';");

					String travelFrom = "";
					String travelTo = "";

					if (rates.getSalesFromDate() != null) {
						travelFrom = dateFormat.format(rates.getSalesFromDate());
					}

					if (rates.getSaleslToDate() != null) {
						travelTo = dateFormat.format(rates.getSaleslToDate());
					}

					sb.append("arrData[" + e + "][17] = '" + travelFrom + "';");
					sb.append("arrData[" + e + "][18] = '" + travelTo + "';");

					rateCodes += rates.getChargeRateCode() + ",";
					e++;
					currCode = rates.getCurrencyCode();
				}

				sb.append("arrChargeData[" + i + "][10] = arrData;");
				sb.append("arrChargeData[" + i + "][11] = '" + objCharge.getVersion() + "';");
				sb.append("arrChargeData[" + i + "][12] = '" + rateCodes + "';");
				// adding charge type
				sb.append("arrChargeData[" + i + "][16] = '" + objCharge.getDepartureOrArival() + "';");

				sb.append("arrChargeData[" + i + "][18] = '" + objCharge.getOndApplicability() + "';");

				if (objCharge.getStatus() == null) {
					sb.append("arrChargeData[" + i + "][13] = 'In-Active';");
				} else if (objCharge.getStatus().equals(Charge.STATUS_ACTIVE)) {
					sb.append("arrChargeData[" + i + "][13] = 'Active';");
				} else {
					sb.append("arrChargeData[" + i + "][13] = 'In-Active';");
				}
				if (objCharge.getChargeCategoryCode().equals("OND") || objCharge.getChargeCategoryCode().equals("POS")) {
					Set<String> ondSet = objCharge.getOndCodes();
					String ond = "";
					Iterator<String> iteOnd = ondSet.iterator();
					while (iteOnd.hasNext()) {
						ond += (String) iteOnd.next() + ",";
					}

					sb.append("arrChargeData[" + i + "][14] = '" + ond + "';");

				} else {
					sb.append("arrChargeData[" + i + "][14] = '';");
				}

				String displayStr = "arrChargeData[" + i + "][15] = '';";
				List<Map<String, String>> DisplayList = SelectListGenerator.createListcChargeDisplay();
				Iterator<Map<String, String>> iteDisplay = DisplayList.iterator();

				while (iteDisplay.hasNext()) {
					Map<String, String> keyValues = (Map<String, String>) iteDisplay.next();
					Set<String> keys = keyValues.keySet();

					Iterator<String> keyIterator = keys.iterator();
					while (keyIterator.hasNext()) {
						String display = keyValues.get(keyIterator.next()).toString();
						String group = keyValues.get(keyIterator.next()).toString();

						if (group.equals(objCharge.getChargeGroupCode())) {
							displayStr = "arrChargeData[" + i + "][15] = '" + display + "';";// Display mode 0/1
							break;

						}
					}
				}

				sb.append(displayStr);
				if (objCharge.getReportingChargeGroupId() != null) {
					sb.append("arrChargeData[" + i + "][17] = '" + objCharge.getReportingChargeGroupId() + "';");
				} else {
					sb.append("arrChargeData[" + i + "][17] = '';");// + objCharge.getReportingChargeGroupId() + "';");
				}
				sb.append("arrChargeData[" + i + "][19] = '" + objCharge.getJourneyType() + "';");
				sb.append("arrChargeData[" + i + "][20] = '" + objCharge.getAppliesTo() + "';");

				Collection<ChargePos> posChargSet = objCharge.getPosCharges();
				String pos = "";
				String chargeCode = "";
				String chargeInc = "I";
				Iterator<ChargePos> posItr = posChargSet.iterator();
				while (posItr.hasNext()) {
					ChargePos chargePos = (ChargePos) posItr.next();
					pos += chargePos.getPos() + ",";
					chargeInc = chargePos.getIncludeOrExclude();
					// chargeCode = chargePos.getChargeCode();

				}
				if (!("".equals(pos))) {
					pos = pos.substring(0, pos.lastIndexOf(","));
				}

				sb.append("arrChargeData[" + i + "][21] = '" + pos + "';");
				sb.append("arrChargeData[" + i + "][22] = '" + chargeInc + "';");

				Collection<ChargeAgent> agentChargeSet = objCharge.getAgentCharges();
				String agent = "";
				String agtChargeCode = "";
				String chargeAgtInc = "I";
				Iterator<ChargeAgent> agtItr = agentChargeSet.iterator();
				while (agtItr.hasNext()) {
					ChargeAgent chargeAgent = (ChargeAgent) agtItr.next();
					agent += chargeAgent.getAgentCode() + ",";
					chargeAgtInc = chargeAgent.getIncludeOrExclude();
					// chargeCode = chargePos.getChargeCode();

				}
				if (!("".equals(agent))) {
					agent = agent.substring(0, agent.lastIndexOf(","));
				}

				sb.append("arrChargeData[" + i + "][23] = '" + agent + "';");
				sb.append("arrChargeData[" + i + "][24] = '" + chargeAgtInc + "';");

				String revChk = objCharge.getSurRevenue();
				revChk = (revChk == null) ? "" : revChk.trim();

				String revPercTxt = objCharge.getSurRevenuePerc();
				revPercTxt = (revPercTxt == null) ? "" : revPercTxt.trim();

				sb.append("arrChargeData[" + i + "][25] = '" + revChk + "';");
				sb.append("arrChargeData[" + i + "][26] = '" + revPercTxt + "';");

				sb.append("arrChargeData[" + i + "][27] = '" + currCode + "';");
				if (currCode != null && !"".equals(currCode)) {
					sb.append("arrChargeData[" + i + "][28] = 'SEL_CURR';");
				} else {
					sb.append("arrChargeData[" + i + "][28] = 'BASE_CURR';");
				}

				sb.append("arrChargeData[" + i + "][29] = '" + objCharge.getHasInwardTransit() + "';");
				sb.append("arrChargeData[" + i + "][30] = '" + objCharge.getHasOutwardTransit() + "';");
				sb.append("arrChargeData[" + i + "][31] = '" + objCharge.getHasTransit() + "';");
				sb.append("arrChargeData[" + i + "][32] = '"
						+ (objCharge.getMaxTransitTime() == null ? "" : calcReadableTrnDuration(objCharge.getMaxTransitTime()))
						+ "';");
				sb.append("arrChargeData[" + i + "][33] = '"
						+ (objCharge.getMinTransitTime() == null ? "" : calcReadableTrnDuration(objCharge.getMinTransitTime()))
						+ "';");
				sb.append("arrChargeData[" + i + "][34] = '"
						+ (objCharge.getAirportTaxCategory() == null ? "" : objCharge.getAirportTaxCategory()) + "';");
				sb.append("arrChargeData[" + i + "][35] = '" + objCharge.getChannelId() + "';");
				if ("Y".equals(objCharge.getRefundableOnlyforModifications())) {
					sb.append("arrChargeData[" + i + "][36] = 'Y';");
				} else {
					sb.append("arrChargeData[" + i + "][36] = 'N';");
				}
				if ("Y".equals(objCharge.getExcludable())) {
					sb.append("arrChargeData[" + i + "][37] = 'Y';");
				} else {
					sb.append("arrChargeData[" + i + "][37] = 'N';");
				}
				if (objCharge.getMinFlightSearchGap() != null) {
					sb.append("arrChargeData[" + i + "][38] = '" + objCharge.getMinFlightSearchGap() + "';");
				} else {
					sb.append("arrChargeData[" + i + "][38] = '0';");
				}

				if (objCharge.getMaxFlightSearchGap() != null) {
					sb.append("arrChargeData[" + i + "][39] = '" + objCharge.getMaxFlightSearchGap() + "';");
				} else {
					sb.append("arrChargeData[" + i + "][39] = '0';");
				}
				if ("Y".equals(objCharge.getRefundableWhenExchange())) {
					sb.append("arrChargeData[" + i + "][40] = 'Y';");
				} else {
					sb.append("arrChargeData[" + i + "][40] = 'N';");
				}

				if ("Y".equals(objCharge.getBundledFareCharge())) {
					sb.append("arrChargeData[" + i + "][41] = 'Y';");
				} else {
					sb.append("arrChargeData[" + i + "][41] = 'N';");
				}
				String innerState = objCharge.getInterState();
				if(innerState != null && !"".equals(innerState)){
					sb.append("arrChargeData[" + i + "][44] = '" + innerState + "';");
				} else {
					sb.append("arrChargeData[" + i + "][44] = '';");
				}
				if (objCharge.getChargeCategoryCode().equals("SERVICE_TAX")) {
					String countryCode = objCharge.getCountryCode();
					Integer stateId = objCharge.getStateId();
					String stateCode = null;
					if(stateId != null){
						State state = ModuleServiceLocator.getCommonServiceBD().getState(stateId);
						if(state != null && state.getStateCode() != null){
							stateCode = state.getStateCode().trim();
						}
					}
					String invoiceTaxCategory = objCharge.getInvoiceTaxCategory();
					Collection<ChargeApplicableCharges>  applicableCharges = objCharge.getApplicableCharges();
					
					if(countryCode != null && !"".equals(countryCode)){
						sb.append("arrChargeData[" + i + "][42] = '" + countryCode + "';");
					} else {
						sb.append("arrChargeData[" + i + "][42] = '';");
					}
					
					if(stateCode != null && !"".equals(stateCode)){
						sb.append("arrChargeData[" + i + "][43] = '" + stateCode + "';");
					} else {
						sb.append("arrChargeData[" + i + "][43] = '';");
					}
					
					if(invoiceTaxCategory != null && !"".equals(invoiceTaxCategory)){
						sb.append("arrChargeData[" + i + "][45] = '" + invoiceTaxCategory + "';");
					} else {
						sb.append("arrChargeData[" + i + "][45] = '';");
					}

					String applicableChargeString = "";
					Iterator<ChargeApplicableCharges> iteApplicableCharges = applicableCharges.iterator();
					while (iteApplicableCharges.hasNext()) {
						applicableChargeString += ((ChargeApplicableCharges) iteApplicableCharges.next()).getApplicableChargeCode() + ",";
					}
					
					if(!"".equals(applicableChargeString)){
						sb.append("arrChargeData[" + i + "][46] = '" + applicableChargeString + "';");
					} else {
						sb.append("arrChargeData[" + i + "][46] = '';");
					}
					String ticketingServiceTax = objCharge.getTicketingServiceTax();
					if(ticketingServiceTax != null && !"".equals(ticketingServiceTax)){
						sb.append("arrChargeData[" + i + "][47] = '" + ticketingServiceTax + "';");
					} else {
						sb.append("arrChargeData[" + i + "][47] = '';");
					}

				} else {
					sb.append("arrChargeData[" + i + "][42] = '';");
					sb.append("arrChargeData[" + i + "][43] = '';");
					sb.append("arrChargeData[" + i + "][45] = '';");
					sb.append("arrChargeData[" + i + "][46] = '';");
					sb.append("arrChargeData[" + i + "][47] = '';");
				}

				sb.append("var arrData =null;");
				i++;
			}
		}
		log.debug(" charges createChargesRowHtml(col) method finished");
		sb.append("var showFareDefByDepCountryCurrency = '" + showFareDefByDepCountryCurrency + "';");
		return sb.toString();
	}

	private static String calcReadableTrnDuration(long trnDuration) {

		long trnDurationInMinutes = trnDuration / 60000;
		return String.valueOf(trnDurationInMinutes);
	}

	/**
	 * Gets the Modified Charges Row
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Modified charges row
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public final String getModifyChargesRowHtml(HttpServletRequest request) throws ModuleException {
		log.debug("Inside charges getModifyChargesRowHtml(request) method");

		StringBuffer sb = new StringBuffer();
		String charges = request.getParameter("hdnCharges");
		sb.append("var arrData = new Array();");

		if (charges == null || charges.equals((""))) {
			sb.append("arrData[0] = new  Array();");
			sb.append("arrData[0][0] = '';");
			sb.append("arrData[0][1] = '';");
			sb.append("arrData[0][2] = '';");
			sb.append("arrData[0][3] = 'V';");
			sb.append("arrData[0][4] = '';");
			sb.append("arrData[0][5] = '';");
			sb.append("arrData[0][6] = '';");
			sb.append("arrData[0][7] = '';");
			sb.append("arrData[0][8] = 'true';");
			sb.append("arrData[0][9] = '0';");
			sb.append("arrData[0][10] = '0';");
			sb.append("arrData[0][11] = 'Added';");
			sb.append("arrData[0][12] = '';");
			sb.append("arrData[0][13] ='1';");
			sb.append("arrData[0][14] ='-1';");
			sb.append("arrData[0][15] ='';");
			sb.append("arrData[0][16] ='';");
			sb.append("arrData[0][17] ='';");
			sb.append("arrData[0][18] ='';");
			sb.append("arrData[0][19] ='';");
		}
		log.debug(" charges getModifyChargesRowHtml(request) method Finished");
		return sb.toString();
	}

	/**
	 * Creates the Form Data from the Request
	 * 
	 * @param request
	 *            HttpServletRequest
	 * @return String the Array of Form Data
	 */
	public static String getChargesData(HttpServletRequest request) {
		log.debug("Inside charges getChargesData(request) method");

		StringBuffer sb = new StringBuffer();
		sb.append("var model= new Array();");

		sb.append("model[0] = '" + request.getParameter("txtChargeCode") + "';");
		sb.append("model[1] = '" + request.getParameter("hdnChargeCode") + "';");
		sb.append("model[2] = '" + request.getParameter("AddSegselGroup") + "';");
		sb.append("model[3] = '" + request.getParameter("txtDesc") + "';");
		sb.append("model[4] = '" + request.getParameter("chkRefundable") + "';");
		sb.append("model[5] = '" + request.getParameter("chkStatus") + "';");
		sb.append("model[6] = '" + request.getParameter("selApplyTo") + "';");
		sb.append("model[7] = '" + request.getParameter("radCat1") + "';");
		sb.append("model[8] = '" + request.getParameter("hdnStations") + "';");
		sb.append("model[9] = '" + request.getParameter("hdnSegments") + "';");
		sb.append("model[10] = '" + request.getParameter("hdnChargeData") + "';");
		sb.append("model[11] = '" + request.getParameter("hdnChargeCode") + "';");

		sb.append("model[12] = '" + request.getParameter("hdnRateIDs") + "';");
		sb.append("model[13] = '" + request.getParameter("hdnRateVersion") + "';");
		sb.append("model[14] = '" + request.getParameter("hdnVersion") + "';");
		sb.append("model[15] = '" + request.getParameter("radDept1") + "';");
		sb.append("model[16] = '" + request.getParameter("selReportingGroup") + "';");
		// keeping with the request data
		sb.append("model[19] = '" + request.getParameter("selJourney") + "';");
		sb.append("model[20] = '" + request.getParameter("hdnAgents") + "';");
		sb.append("model[21] = '" + request.getParameter("chkRevenue") + "';");
		sb.append("model[22] = '" + request.getParameter("txtRevenue") + "';");

		sb.append("model[23] = '" + request.getParameter("chkAirportTax") + "';");
		sb.append("model[24] = '" + request.getParameter("selAirportTaxType") + "';");

		sb.append("model[25] = '" + request.getParameter("txtRptChargeGroupCode") + "';");
		sb.append("model[26] = '" + request.getParameter("txtRptChargeGroupDesc") + "';");
		sb.append("model[27] = '" + request.getParameter("hdnRptChargeGroupType") + "';");

		log.debug(" charges getChargesData(request) method finished");

		return sb.toString();
	}

	/**
	 * Creates the Client Validations For the CHARGE Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array of client vallidations
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {
		if (clientErrors == null) {
			Properties moduleErrs = new Properties();

			moduleErrs.setProperty("um.charges.chargecode.required", "chargecodeRqrd");
			moduleErrs.setProperty("um.charges.group.required", "groupRqrd");
			moduleErrs.setProperty("um.charges.category.required", "categoryRqrd");
			moduleErrs.setProperty("um.charge.rate.toDate.required", "todateRqrd");
			moduleErrs.setProperty("um.charge.rate.todate.shouldbeGreater", "todateLess");
			moduleErrs.setProperty("um.charge.rate.fromDate.required", "fromdateRqrd");
			moduleErrs.setProperty("um.charges.description.required", "descRqrd");
			moduleErrs.setProperty("um.charges.segment.required", "segmentRqrd");
			moduleErrs.setProperty("um.charges.pos.required", "posRqrd");
			moduleErrs.setProperty("um.charges.pos.invalid.with.ibe", "posInvalidWithIBE");
			moduleErrs.setProperty("um.farerule.select.arow", "rowclickRqrd");
			moduleErrs.setProperty("um.charges.segment.exists", "segmetExists");
			moduleErrs.setProperty("um.charges.todate.format.incorrect", "toDateIncorrect");	
			moduleErrs.setProperty("um.charges.fromdate.format.incorrect", "fromDateIncorrect");

			// rates
			moduleErrs.setProperty("um.charges.rate.todate.format.incorrect", "reteToDateIncorrect");
			moduleErrs.setProperty("um.charges.rate.fromdate.format.incorrect", "rateFromDateIncorrect");
			moduleErrs.setProperty("um.charge.rate.value.required", "valueRqrd");
			moduleErrs.setProperty("um.charge.rate.currency.required", "currencyRqrd");
			moduleErrs.setProperty("um.charge.rate.percentage.shouldnotExceed", "percentageExceeds");
			moduleErrs.setProperty("um.charge.rate.percentage.shouldnotZero", "percentageZero");
			moduleErrs.setProperty("um.charge.rate.percentage.lessthanZero", "percentagelessThanZero");
			moduleErrs.setProperty("um.charge.rate.overlaps", "rateOverlaps");
			moduleErrs.setProperty("um.charge.rate.expired", "datesExpired");
			moduleErrs.setProperty("um.charge.rate.todate.shouldbeGreater.current.date", "ToDateExceedsCurrentDate");
			moduleErrs.setProperty("um.charge.rate.fromdate.shouldbeGreater.current.date", "FromDateExceedsCurrentDate");
			moduleErrs.setProperty("um.charge.rate.exceed.minvalue", "minExceedsMax");
			moduleErrs.setProperty("um.charge.rate.flag.invalid", "chargeBasisCodeInvalid");
			moduleErrs.setProperty("um.charges.depature.required", "depatureRqrd");
			moduleErrs.setProperty("um.charges.arrival.required", "arrivalRqrd");
			moduleErrs.setProperty("um.charges.via.required", "viaPointRqrd");
			moduleErrs.setProperty("um.charges.arrivalDepature.same", "depatureArriavlSame");
			moduleErrs.setProperty("um.charges.arrivalVia.same", "arriavlViaSame");
			moduleErrs.setProperty("um.charges.depatureVia.same", "depatureViaSame");
			moduleErrs.setProperty("um.charges.via.sequence", "vianotinSequence");
			moduleErrs.setProperty("um.charges.via.equal", "viaEqual");
			moduleErrs.setProperty("um.charges.departure.inactive", "departureInactive");
			moduleErrs.setProperty("um.charges.arrival.inactive", "arrivalInactive");
			moduleErrs.setProperty("um.charges.via.inactine", "viaInactive");
			moduleErrs.setProperty("um.charges.ond.required", "ondRequired");
			moduleErrs.setProperty("um.charges.date.formatIncorrect", "formatIncorrect");
			moduleErrs.setProperty("um.charge.pos.inactive", "posInactive");
			moduleErrs.setProperty("um.charges.group.inactine", "groupInactive");
			moduleErrs.setProperty("um.airadmin.delete.confirmation", "deleteMessage");
			moduleErrs.setProperty("um.charge.currency.nan", "currencyNan");
			moduleErrs.setProperty("um.charge.valuePerce.nan", "percNan");
			moduleErrs.setProperty("um.charge.journey.null", "journeynull");
			moduleErrs.setProperty("um.charge.revenue.null", "revenuenull");
			moduleErrs.setProperty("um.charge.selcurr.null", "selCurrencyNull");
			moduleErrs.setProperty("um.charge.boundry.break.point.both.needed", "boundryValueAndBreakPointNeeded");
			moduleErrs.setProperty("um.charge.boundry.value.should.be.positive", "boundryValuePositive");
			moduleErrs.setProperty("um.charge.break.point.should.be.positive", "breakPointPositive");
			moduleErrs.setProperty("um.charge.boundry.value.applicable.for.percentages",
					"boundryValueApplicableOnlyForPercentages");
			moduleErrs.setProperty("um.charge.break.point.applicable.for.percentages", "breakPointApplicableOnlyForPercentages");
			moduleErrs.setProperty("um.charge.trnsit.dur.null", "durNull");
			moduleErrs.setProperty("um.charge.trnsit.dur.format", "DurIncco");
			moduleErrs.setProperty("um.charge.trnsit.dur.zero", "durZero");
			moduleErrs.setProperty("um.charge.trnsit.dur.exceed.minvalue", "durMinExceedMax");
			moduleErrs.setProperty("um.currency.exrate.fromblank", "fromdateblank");
			moduleErrs.setProperty("um.currency.exrate.toblank", "todateblank");
			moduleErrs.setProperty("um.currency.exrate.invalidfromat", "formatincorrect");
			moduleErrs.setProperty("um.charges.travel.frm.date.incorrect", "travelToDateErr");
			moduleErrs.setProperty("um.charges.travel.to.date.incorrect", "travelFromDateErr");
			moduleErrs.setProperty("um.charges.travel.frm.shouldbeGreater.current.date", "travelFromDateGrt");
			moduleErrs.setProperty("um.charges.travel.to.shouldbeGreater.current.date", "travelToDateGrt");
			moduleErrs.setProperty("um.charges.travel.date.range.invalid", "travelDateRangeErr");
			moduleErrs.setProperty("um.charges.effective.date.range.invalid", "effectiveDateRangeErr");
			moduleErrs.setProperty("um.charges.reporting.charge.group.code.required", "reportinChargeGroupCodeReq");
			moduleErrs.setProperty("um.charges.reporting.charge.group.code.description.required", "reportinChargeGroupDescReq");
			moduleErrs.setProperty("um.charges.local.currency.with.percentage.notAllowed",
					"localCurrencyWithPercentageNotAllowed");
			moduleErrs.setProperty("um.charges.min.flight.gap.required", "minDayGapRqrd");
			moduleErrs.setProperty("um.charges.max.flight.gap.required", "maxDayGapRqrd");
			moduleErrs.setProperty("um.charges.min.less.than.max.dates", "maxLessThanMin");
			moduleErrs.setProperty("um.charges.min.max.greater.than.zero", "minMaxMoreThanZero");
			moduleErrs.setProperty("um.charges.serviceTax.message.taxCategory", "serviceTaxCategoryMessage");
			moduleErrs.setProperty("um.charges.serviceTax.counrty.required", "serviceTaxCountryRequired");
			moduleErrs.setProperty("um.charges.serviceTax.invoiceCategory.required", "serviceTaxInvoiceCategoryRequired");
			moduleErrs.setProperty("um.charges.invalid.value.calculation.method", "invalidValueCalculationMethod");
			clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}
		return clientErrors;
	}

	@SuppressWarnings("rawtypes")
	private static class OldestFirstSortComparator implements Comparator {

		@Override
		public int compare(Object first, Object second) {

			ChargeRate rate1 = (ChargeRate) first;
			ChargeRate rate2 = (ChargeRate) second;

			return (rate1.getChargeEffectiveFromDate() == null || rate2.getChargeEffectiveFromDate() == null) ? 0 : rate1
					.getChargeEffectiveFromDate().compareTo(rate2.getChargeEffectiveFromDate());
		}
	}

}
