package com.isa.thinair.airadmin.core.web.action.master;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.json.JSONResult;

import com.isa.thinair.airadmin.core.service.AiradminConfig;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airadmin.core.web.v2.dto.CityDTO;
import com.isa.thinair.airadmin.core.web.v2.handler.master.CityRH;
import com.isa.thinair.airmaster.api.model.City;
import com.isa.thinair.airmaster.api.model.CityForDisplay;
import com.isa.thinair.commons.api.dto.ModuleCriteria;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.MessagesUtil;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.StringUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.api.util.Constants;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class ShowCityAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(ShowCityAction.class);
	private static AiradminConfig airadminConfig = new AiradminConfig();
	private Object[] rows;
	private int page;
	private int total;
	private int records;
	private String succesMsg;
	private String msgType;

	private long version;
	private String createdBy;
	private Date createdDate;

	private String hdnMode;
	private String mode;

	private String selCountry;
	private String selCity;
	private String selStatus;

	private CityDTO cityDTO;
	private Integer cityId;

	private String cityForDisplay;
	private String strCityCountryHtml;

	public static final String STATUS_ACTIVE = "ACT";

	public static final String STATUS_INACTIVE = "INA";

	public String execute() throws Exception {
		try {

			City city = new City();
			City existCity = null;
			City savedCity = null;

			if (cityDTO != null) {
				if (cityDTO.getCityId() != null) {
					city.setCityId(cityDTO.getCityId());
				}
				city.setCityCode(cityDTO.getCityCode());
				city.setCityName(cityDTO.getCityName());
				city.setCountryCode(cityDTO.getCountryCode());

				if (cityDTO.getStatus() != null) {
					city.setStatus(cityDTO.getStatus());
				} else {
					city.setStatus(STATUS_INACTIVE);
				}

				if (this.hdnMode != null
						&& ((hdnMode.equals(WebConstants.ACTION_ADD)) || (hdnMode.equals(WebConstants.ACTION_EDIT)))) {
					if (hdnMode.equals(WebConstants.ACTION_EDIT)) {
						if (cityDTO.getCityId() != null) {
							existCity = CityRH.getCity(cityDTO.getCityId());
							existCity.setCityName(cityDTO.getCityName());
							existCity.setCountryCode(cityDTO.getCountryCode());
							if (cityDTO.getStatus() != null) {
								existCity.setStatus(cityDTO.getStatus());
							} else {
								existCity.setStatus(STATUS_INACTIVE);
							}
						}
						city = existCity;
					}

					if (cityDTO.getLccPublishStatus() != null && !"".equals(cityDTO.getLccPublishStatus())) {
						city.setLccPublishStatus(cityDTO.getLccPublishStatus());
					}

					try {
						CityRH.saveCity(city);
						savedCity = CityRH.getCity(city.getCityCode(), city.getCountryCode());
						this.strCityCountryHtml = SelectListGenerator.getCityCodeNameCountryCodeListJson();
						this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS);
						this.msgType = WebConstants.MSG_SUCCESS;

					} catch (ModuleException e) {
						this.succesMsg = e.getMessageString();
						if (e.getMessageString().equals(MessagesUtil.getMessage("module.duplicate.key"))) {
							this.succesMsg = airadminConfig.getMessage("um.City.form.id.already.exist");
						}
						this.msgType = WebConstants.MSG_ERROR;
					} catch (Exception ex) {
						this.succesMsg = airadminConfig.getMessage("um.City.system.error");
						this.msgType = WebConstants.MSG_ERROR;
						if (log.isErrorEnabled()) {
							log.error(ex);
						}
					}
				}

				try {
					List<CityForDisplay> cityFD = null;
					if (cityForDisplay != null && !cityForDisplay.equals("")) {
						cityFD = new ArrayList<CityForDisplay>();
						String[] arrCityTrl = cityForDisplay.split("~");
						String[] cityTrl = null;
						for (int i = 0; i < arrCityTrl.length; i++) {
							if (arrCityTrl[i] != null && !arrCityTrl[i].equals("")) {
								cityTrl = arrCityTrl[i].split(Constants.COMMA_SEPARATOR);
								CityForDisplay cfd = new CityForDisplay();
								cfd.setLanguageCode(cityTrl[0]);
								cfd.setCityNameOl(cityTrl[1]);
								if (savedCity != null) {
									cfd.setCityId(savedCity.getCityId());
								} else if (cityDTO.getCityId() != null) {
									cfd.setCityId(cityDTO.getCityId());
								} else {
									cfd.setCityId(Integer.parseInt(cityTrl[2]));
								}
								cfd.setId(new Integer(cityTrl[3]));
								cfd.setVersion(new Long(cityTrl[4]));
								cfd.setCityCodeOl(cityTrl[5]);
								cityFD.add(cfd);
							}
						}
					}
					if (cityFD != null && !cityFD.isEmpty()) {
						ModuleServiceLocator.getTranslationBD().saveOrUpdateCities(cityFD);
					}
					this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS);
					this.msgType = WebConstants.MSG_SUCCESS;

				} catch (ModuleException e) {
					if (this.hdnMode != null
							&& ((hdnMode.equals(WebConstants.ACTION_ADD)) || (hdnMode.equals(WebConstants.ACTION_EDIT)))) {
						this.succesMsg = this.succesMsg + ". Saving Description in Other languages failed.";
					} else {
						this.succesMsg = e.getMessageString();
						if (e.getMessageString().equals(MessagesUtil.getMessage("module.duplicate.key"))) {
							this.succesMsg = airadminConfig.getMessage("um.City.form.city.code.duplicate");
						}
						this.msgType = WebConstants.MSG_ERROR;
					}

				} catch (Exception ex) {
					if (this.hdnMode != null
							&& ((hdnMode.equals(WebConstants.ACTION_ADD)) || (hdnMode.equals(WebConstants.ACTION_EDIT)))) {
						this.succesMsg = this.succesMsg + ". Saving Description in Other languages failed.";
					} else {
						this.succesMsg = "System Error Please Contact Administrator";
						this.msgType = WebConstants.MSG_ERROR;
					}
					if (log.isErrorEnabled()) {
						log.error(ex);
					}

				}

			} else {
				this.succesMsg = airadminConfig.getMessage("um.City.form.fields.required");
				this.msgType = WebConstants.MSG_ERROR;
			}
		} catch (Exception e) {
			this.succesMsg = airadminConfig.getMessage("um.City.system.error");
			this.msgType = WebConstants.MSG_ERROR;
			log.error("Error in saving City:", e);
		}
		return S2Constants.Result.SUCCESS;
	}

	public String deleteCity() throws Exception {
		try {
			if (this.hdnMode != null && ((hdnMode.equals(WebConstants.ACTION_DELETE)) && cityId != null)) {
				try {
					ModuleServiceLocator.getTranslationBD().removeCities(cityId);
					CityRH.deleteCity(cityId);
					this.strCityCountryHtml = SelectListGenerator.getCityCodeNameCountryCodeListJson();
					this.succesMsg = airadminConfig.getMessage(WebConstants.KEY_SAVE_SUCCESS);
					this.msgType = WebConstants.MSG_SUCCESS;

				} catch (ModuleException e) {
					this.succesMsg = e.getMessageString();
					if (e.getMessageString().equals(MessagesUtil.getMessage("module.duplicate.key"))) {
						this.succesMsg = airadminConfig.getMessage("um.City.form.id.already.exist");
					}
					this.msgType = WebConstants.MSG_ERROR;
				} catch (Exception ex) {
					this.succesMsg = airadminConfig.getMessage("um.City.system.error");
					this.msgType = WebConstants.MSG_ERROR;
					if (log.isErrorEnabled()) {
						log.error(ex);
					}
				}

			} else {
				this.succesMsg = airadminConfig.getMessage("um.City.system.delete.required");
				this.msgType = WebConstants.MSG_ERROR;
			}
		} catch (Exception e) {
			this.succesMsg = airadminConfig.getMessage("um.City.system.error");
			this.msgType = WebConstants.MSG_ERROR;
			log.error("Error in saving City:", e);
		}
		return S2Constants.Result.SUCCESS;
	}

	public String searchCity() throws Exception {
		try {

			List<ModuleCriterion> critrian = null;
			critrian = new ArrayList<ModuleCriterion>();

			List<CityForDisplay> citiesForDisplayList = null;
			String strCities = null;
			try {
				citiesForDisplayList = ModuleServiceLocator.getTranslationBD().getCities();
			} catch (ModuleException e) {
				this.succesMsg = e.getMessageString();
				this.msgType = WebConstants.MSG_ERROR;
			}

			if (selCountry != null && !selCountry.equals("-1")) {
				ModuleCriteria criteriaCityCountry = new ModuleCriteria();
				List<String> lstCountryCodes = new ArrayList<String>();
				criteriaCityCountry.setFieldName("countryCode");
				criteriaCityCountry.setCondition(ModuleCriteria.CONDITION_LIKE);
				lstCountryCodes.add("%" + selCountry + "%");
				criteriaCityCountry.setValue(lstCountryCodes);
				critrian.add(criteriaCityCountry);
			}

			if (selCity != null && !selCity.equals("-1")) {
				ModuleCriteria criteriaCity = new ModuleCriteria();
				List<String> lstCityCodes = new ArrayList<String>();
				criteriaCity.setFieldName("cityCode");
				criteriaCity.setCondition(ModuleCriteria.CONDITION_LIKE);
				lstCityCodes.add("%" + selCity + "%");
				criteriaCity.setValue(lstCityCodes);
				critrian.add(criteriaCity);
			}

			if (selStatus != null && !selStatus.equals("-1")) {
				ModuleCriteria criteriaStatus = new ModuleCriteria();
				List<String> lstStatusCodes = new ArrayList<String>();
				criteriaStatus.setFieldName("status");
				criteriaStatus.setCondition(ModuleCriteria.CONDITION_LIKE);
				lstStatusCodes.add("%" + selStatus + "%");
				criteriaStatus.setValue(lstStatusCodes);
				critrian.add(criteriaStatus);
			}

			Page<City> pgCity = CityRH.searchCity(this.page, critrian);
			this.records = pgCity.getTotalNoOfRecords();
			this.total = pgCity.getTotalNoOfRecords() / 20;
			int mod = pgCity.getTotalNoOfRecords() % 20;
			if (mod > 0) {
				this.total = this.total + 1;
			}
			if (this.page <= 0) {
				this.page = 1;
			}

			Collection<City> colCity = pgCity.getPageData();

			if (!StringUtil.isNullOrEmpty(colCity.toString())) {
				Object[] dataRow = new Object[colCity.size()];
				int index = 1;
				Iterator<City> iter = colCity.iterator();
				while (iter.hasNext()) {
					Map<String, Object> counmap = new HashMap<String, Object>();
					City city = iter.next();
					strCities = formCityDisplayString(citiesForDisplayList, city);
					counmap.put("id", new Integer(((page - 1) * 20) + index).toString());
					counmap.put("city", city);
					counmap.put("cityForDisplay", strCities);

					dataRow[index - 1] = counmap;
					index++;
				}

				this.rows = dataRow;
			}
		} catch (Exception e) {
			this.succesMsg = airadminConfig.getMessage("um.City.system.error");
			this.msgType = WebConstants.MSG_ERROR;
			log.error("Error in search City", e);
		}
		return S2Constants.Result.SUCCESS;
	}

	private String formCityDisplayString(Collection<CityForDisplay> colCityForDisplay, City city) {
		Collection<CityForDisplay> chCol = new HashSet<CityForDisplay>();
		StringBuilder sb = new StringBuilder();
		if (colCityForDisplay != null && city != null) {
			for (CityForDisplay ctForDisplay : colCityForDisplay) {
				if (city.getCityId().intValue() == ctForDisplay.getCityId().intValue()) {
					chCol.add(ctForDisplay);
				}
			}
		}
		String ucs = "";

		if (chCol != null) {
			for (CityForDisplay ctfd : chCol) {
				ucs = StringUtil.getUnicode(ctfd.getCityNameOl());
				ucs = ucs.replace(",", "^");
				sb.append(ctfd.getLanguageCode());
				sb.append(",");
				sb.append(ucs);
				sb.append(",");
				sb.append(ctfd.getCityId());
				sb.append(",");
				sb.append(ctfd.getId());
				sb.append(",");
				sb.append(ctfd.getVersion());
				sb.append(",");
				ucs = "";
				ucs = StringUtil.getUnicode(ctfd.getCityCodeOl());
				ucs = ucs.replace(",", "^");
				sb.append(ucs);
				sb.append("~");

			}
		}
		return sb.toString();
	}

	public Object[] getRows() {
		return rows;
	}

	public void setRows(Object[] rows) {
		this.rows = rows;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public String getSuccesMsg() {
		return succesMsg;
	}

	public void setSuccesMsg(String succesMsg) {
		this.succesMsg = succesMsg;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getHdnMode() {
		return hdnMode;
	}

	public void setHdnMode(String hdnMode) {
		this.hdnMode = hdnMode;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getSelCountry() {
		return selCountry;
	}

	public void setSelCountry(String selCountry) {
		this.selCountry = selCountry;
	}

	public CityDTO getCityDTO() {
		return cityDTO;
	}

	public void setCityDTO(CityDTO cityDTO) {
		this.cityDTO = cityDTO;
	}

	public Integer getCityId() {
		return cityId;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	public String getSelCity() {
		return selCity;
	}

	public void setSelCity(String selCity) {
		this.selCity = selCity;
	}

	public String getSelStatus() {
		return selStatus;
	}

	public void setSelStatus(String selStatus) {
		this.selStatus = selStatus;
	}

	public String getStrCityCountryHtml() {
		return strCityCountryHtml;
	}

	public void setStrCityCountryHtml(String strCityCountryHtml) {
		this.strCityCountryHtml = strCityCountryHtml;
	}

	public String getCityForDisplay() {
		return cityForDisplay;
	}

	public void setCityForDisplay(String cityForDisplay) {
		this.cityForDisplay = cityForDisplay;
	}

}