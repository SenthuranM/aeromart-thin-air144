package com.isa.thinair.airadmin.core.web.generator.master;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airmaster.api.model.SSR;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.dto.SSRCategoryDTO;
import com.isa.thinair.commons.api.exception.ModuleException;

public class SsrHTMLGenerator {

	private static Log log = LogFactory.getLog(SsrHTMLGenerator.class);

	private static String clientErrors;

	/**
	 * Gets SSR Grid Data Row for the Page No
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array of SSR Data
	 * @throws ModuleException
	 *             the ModuleException
	 */
	@SuppressWarnings("unchecked")
	public final String getSsrRowHtml(HttpServletRequest request) throws ModuleException {

		log.debug("inside SsrHTMLGenerator.getSsrRowHtml()");
		List<SSR> list = null;

		int recordNo = 0;
		if (request.getParameter("hdnRecNo") != null && !request.getParameter("hdnRecNo").equals(""))
			recordNo = Integer.parseInt(request.getParameter("hdnRecNo")) - 1;

		Page page = null;

		//String strSSRCategoryJS = "var strSSRCategory=-1;";
		String strSSRCategory = request.getParameter("selSSRCat");
		//strSSRCategoryJS = "var strSSRCategory='" + strSSRCategory + "';";

		// If All option selected return all the airports
		if (strSSRCategory == null || strSSRCategory.equals("All"))
			page = ModuleServiceLocator.getSsrServiceBD().getSSRs(recordNo, 20, -1);
		else
			page = ModuleServiceLocator.getSsrServiceBD().getSSRs(recordNo, 20, Integer.parseInt(strSSRCategory));

		// page = ssrDelegate.getSSRs(recordNo, 20);

		int totalRecords = page.getTotalNoOfRecords();
		String strJavascriptTotalNoOfRecs = "var totalRecords = " + totalRecords + ";";
		request.setAttribute(WebConstants.REQ_TOTAL_RECORDS, strJavascriptTotalNoOfRecs);
		request.setAttribute("reqSsrCategory", strSSRCategory);
		list = (List<SSR>) page.getPageData();
		return createSsrRowHTML(list);
	}

	/**
	 * Creates the SSR Grid Array from a Collection of SSRs
	 * 
	 * @param HALA_SERVICE
	 *            the Collection of SSRs
	 * @return String the Created Array of grid rows
	 */
	private String createSsrRowHTML(Collection<SSR> SSRs) {

		List<SSR> list = (List<SSR>) SSRs;
		Object[] listArr = list.toArray();

		StringBuffer sb = new StringBuffer();
		SSR ssr = null;

		for (int i = 0; i < listArr.length; i++) {
			ssr = (SSR) listArr[i];

			sb.append("arrData[" + i + "] = new Array();");
			sb.append("arrData[" + i + "][1] = '" + ssr.getSsrCode() + "';");

			if (ssr.getSsrDesc() != null) {
				sb.append("arrData[" + i + "][2] = '" + ssr.getSsrDesc() + "';");
			} else {
				sb.append("arrData[" + i + "][2] = '';");
			}
			sb.append("arrData[" + i + "][3] = '" + ssr.getDefaultCharge() + "';");
			sb.append("arrData[" + i + "][4] = '" + ssr.getSsrCatId() + "';");
			sb.append("arrData[" + i + "][5] = '" + AiradminUtils.getModifiedStatus(ssr.getStatus()) + "';");
			sb.append("arrData[" + i + "][6] = '" + ssr.getVersion() + "';");
			sb.append("arrData[" + i + "][7] = '" + ssr.getSsrId() + "';");
			sb.append("arrData[" + i + "][8] = '" + getSsrCategoryName(String.valueOf(ssr.getSsrCatId())) + "';");
			sb.append("arrData[" + i + "][9] = new Array();");
			if (ssr.getVisibleChannelIds() != null && !ssr.getVisibleChannelIds().isEmpty()) {
				Set<Integer> setVisbileChannels = ssr.getVisibleChannelIds();
				int al = 0;
				String channelId = null;
				for (Iterator<Integer> vIter = setVisbileChannels.iterator(); vIter.hasNext();) {
					channelId = vIter.next().toString();
					sb.append("arrData[" + i + "][9][" + al + "] = '" + channelId + "';");
					al++;
				}
			}

		}

		return sb.toString();
	}

	/**
	 * Creates the Client Validations for SSR Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array Containing the Client Validations
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {

		if (clientErrors == null) {
			Properties moduleErrs = new Properties();
			moduleErrs.setProperty("um.airadmin.delete.confirmation", "deleteRecoredCfrm");
			moduleErrs.setProperty("um.ssr.form.code.required", "ssrCodeRqrd");
			moduleErrs.setProperty("um.ssr.form.desc.required", "ssrDescRqrd");
			moduleErrs.setProperty("um.ssr.form.category.required", "ssrCatRqrd");
			moduleErrs.setProperty("um.ssr.form.id.already.exist", "ssrIdExist");
			clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}
		return clientErrors;
	}

	/**
	 * Get the Ssr category Name for a Given SSR category Id
	 * 
	 * @param strSsrCatId
	 *            the SSRCategory Id
	 * @return the SSR Category Name
	 */
	private String getSsrCategoryName(String strSsrCatId) {
		String ssrCatName = "";
		Map<Integer, SSRCategoryDTO> ssrCategoryMap = ModuleServiceLocator.getGlobalConfig().getSsrCategoryMap();

		if (ssrCategoryMap != null) {
			if (ssrCategoryMap.containsKey(strSsrCatId)) {
				SSRCategoryDTO ssrCategory = (SSRCategoryDTO) ssrCategoryMap.get(strSsrCatId);
				// ssrCatName = (String) ssrCategories.get(strSsrCatId);
				ssrCatName = ssrCategory.getDescription();
			}
		}
		return ssrCatName;
	}

}
