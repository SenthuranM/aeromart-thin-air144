package com.isa.thinair.airadmin.core.web.v2.action.pricing;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airadmin.core.web.v2.util.FareUtil;
import com.isa.thinair.airpricing.api.model.FareRule;
import com.isa.thinair.airpricing.api.service.FareRuleBD;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;

/**
 * 
 * @author Pradeep Karunanayake
 * 
 */
@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class FareRuleSearchAction extends BaseRequestAwareAction {

	private static Log log = LogFactory.getLog(FareRuleSearchAction.class);

	private boolean success = true;

	private String messageTxt;

	private String fareRuleCode;

	private String status;

	private int page = 1;

	private int total;

	private int records;

	private Collection rows;

	@SuppressWarnings("unused")
	public String execute() {
		String forward = S2Constants.Result.SUCCESS;
		try {
			Collection<FareRule> colFareRules;
			Page pageInt = null;
			int totRec = 0;

			List<String> order = new ArrayList<String>();
			order.add("fareRuleCode");
			FareRuleBD fareRuleBD = ModuleServiceLocator.getRuleBD();

			List<ModuleCriterion> critrian = new ArrayList<ModuleCriterion>();
			ModuleCriterion moduleCriterionId = new ModuleCriterion();
			ModuleCriterion moduleCriterionStatus = new ModuleCriterion();
			List<String> valueStatus = new ArrayList<String>();
			moduleCriterionId.setCondition(ModuleCriterion.CONDITION_EQUALS);
			moduleCriterionStatus.setCondition(ModuleCriterion.CONDITION_EQUALS);
			moduleCriterionId.setFieldName("fareRuleCode");
			moduleCriterionStatus.setFieldName("status");

			if (fareRuleCode != null && !(fareRuleCode.equals("")) && !(fareRuleCode.equals("All"))) {
				List<String> valueId = new ArrayList<String>();
				valueId.add(fareRuleCode);
				moduleCriterionId.setValue(valueId);
				critrian.add(moduleCriterionId);
			}
			if (status != null && !(status.equals("")) && !(status.equals("All"))) {
				valueStatus.add(status);
				moduleCriterionStatus.setValue(valueStatus);
				critrian.add(moduleCriterionStatus);
			}

			if ((status == null || status.equals(""))) {
				valueStatus.add("ACT");
				moduleCriterionStatus.setValue(valueStatus);
				critrian.add(moduleCriterionStatus);
			}

			pageInt = fareRuleBD.getFareRules(critrian, (page - 1) * 10, 10, order);
			colFareRules = pageInt.getPageData();
			records = pageInt.getTotalNoOfRecords();
			int totalCount = pageInt.getTotalNoOfRecords() / 10;
			int mod = pageInt.getTotalNoOfRecords() % 10;
			if (mod > 0)
				totalCount = totalCount + 1;
			if (totalCount == 0) {
				page = 0;
			}
			total = totalCount;
			rows = createFareRuleGridData(colFareRules);
		} catch (Exception ex) {
			log.error(ex);
			success = false;
			messageTxt = ex.getMessage();
		}
		return forward;
	}

	@SuppressWarnings("unchecked")
	// Return front end specific data
			private
			Collection createFareRuleGridData(Collection<FareRule> fareRulesNew) throws ModuleException {
		List fareRules = new ArrayList();
		Map ruleMap = null;
		int id = page * 10 - 10;
		if (fareRulesNew != null) {
			SimpleDateFormat dateForamt = new SimpleDateFormat("HH:mm");
			for (FareRule fareRule : fareRulesNew) {
				ruleMap = new HashMap();
				ruleMap.put("id", ++id);
				FareUtil.addFareRuleData(fareRule, ruleMap, dateForamt);
				fareRules.add(ruleMap);
			}
		}
		return fareRules;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessageTxt() {
		return messageTxt;
	}

	public void setMessageTxt(String messageTxt) {
		this.messageTxt = messageTxt;
	}

	public String getFareRuleCode() {
		return fareRuleCode;
	}

	public void setFareRuleCode(String fareRuleCode) {
		this.fareRuleCode = fareRuleCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	public Collection getRows() {
		return rows;
	}

	public void setRows(Collection rows) {
		this.rows = rows;
	}

}
