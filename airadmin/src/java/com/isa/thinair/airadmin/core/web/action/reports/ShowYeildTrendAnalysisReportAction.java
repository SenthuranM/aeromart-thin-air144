package com.isa.thinair.airadmin.core.web.action.reports;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.reports.YieldTrendAnalysisReportRH;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.webplatform.api.base.BaseRequestResponseAwareAction;

@Namespace(AdminStrutsConstants.AdminNameSpace.PRIVATE_NAME_SPACE)
@Results({
		@Result(name = AdminStrutsConstants.AdminAction.RES_SUCCESS, value = AdminStrutsConstants.AdminJSP.REPORTING_YEILD_TREND_ANALYSIS_JSP),
		@Result(name = AdminStrutsConstants.AdminAction.VIEW, value = AdminStrutsConstants.AdminJSP.REPORTING_SHOW_YEILD_TREND_ANALYSIS_JSP),
		@Result(name = AdminStrutsConstants.AdminAction.SUCCESS, type = JSONResult.class, value = ""),
		@Result(name = AdminStrutsConstants.AdminAction.ERROR, value = AdminStrutsConstants.AdminJSP.ERROR_JSP) })
public class ShowYeildTrendAnalysisReportAction extends BaseRequestResponseAwareAction {

	private static Log log = LogFactory.getLog(ShowYeildTrendAnalysisReportAction.class);

	private List<String> yearList = new ArrayList<String>();

	private Map<String, Map<String, BigDecimal>> salesFactorMap;
	private Map<String, Map<String, BigDecimal>> yeildFactorMap;

	private boolean graph = false;

	private String viewType = null;

	private String reportType = null;

	private String radReportOption = null;

	private boolean saleFactor = false;

	public String execute() throws Exception {
		setAirlineCodeList(request);
		return YieldTrendAnalysisReportRH.execute(request, response);
	}

	private void setAirlineCodeList(HttpServletRequest request) throws ModuleException {
		int currYear = Calendar.getInstance().get(Calendar.YEAR);
		yearList.add("<option value='" + currYear + "' SELECTED>" + currYear + "</option>");
		for (int i = 1; i < (currYear - getReportingPeriodYear()) + 1; i++) {
			yearList.add("<option value='" + (currYear - i) + "'>" + (currYear - i) + "</option>");
		}
		request.setAttribute("reqAirlineList", yearList);
	}

	public String calledPopup() {
		String strForward = AdminStrutsConstants.AdminAction.VIEW;
		// String strViewType = request.getParameter("viewType");
		// String strReportType = request.getParameter("reportType");
		// String radReportOption = request.getParameter("radReportOption");
		// System.out.println(getReportType());
		// System.out.println(getViewType());
		// System.out.println(getRadReportOption());
		return strForward;
	}

	public String viewReport() {
		String strViewType = request.getParameter("viewType");
		String strReportType = request.getParameter("reportType");
		String selectedYear = request.getParameter("selectedYear");

		Map<String, List<Map<String, List<BigDecimal>>>> resulMap = null;
		int currentYear = Integer.valueOf(selectedYear).intValue();

		String strForward = AdminStrutsConstants.AdminAction.SUCCESS;

		if (strViewType != null && strViewType.equalsIgnoreCase("SF"))
			saleFactor = true;

		if (strReportType != null && strReportType.equalsIgnoreCase("GRP"))
			graph = true;

		try {
			resulMap = YieldTrendAnalysisReportRH.getReportRelatedInformation(request, response, currentYear);
		} catch (Exception e) {
			log.error(e);
		}

		if (resulMap != null && !resulMap.isEmpty()) {
			populateSFAndYields(resulMap, String.valueOf(currentYear));
		}

		return strForward;
	}

	private void populateSFAndYields(Map<String, List<Map<String, List<BigDecimal>>>> map, String currentYear) {
		salesFactorMap = new HashMap<String, Map<String, BigDecimal>>();
		yeildFactorMap = new HashMap<String, Map<String, BigDecimal>>();

		Map sfPrevYearMap = new HashMap<String, BigDecimal>();
		Map sfCurrYearMap = new HashMap<String, BigDecimal>();

		Map yieldPrevYearMap = new HashMap<String, BigDecimal>();
		Map yieldCurrYearMap = new HashMap<String, BigDecimal>();

		Map sfNewPrevYearMap = new HashMap<String, BigDecimal>();
		Map sfNewCurrYearMap = new HashMap<String, BigDecimal>();

		Map yieldNewPrevYearMap = new HashMap<String, BigDecimal>();
		Map yieldNewCurrYearMap = new HashMap<String, BigDecimal>();

		Map sfPrevYearMap1 = new HashMap<String, BigDecimal>();
		Map sfCurrYearMap1 = new HashMap<String, BigDecimal>();

		Map ylPrevYearMap1 = new HashMap<String, BigDecimal>();
		Map ylCurrYearMap1 = new HashMap<String, BigDecimal>();

		List<Map<String, List<BigDecimal>>> listPrevYear = new ArrayList<Map<String, List<BigDecimal>>>();
		List<Map<String, List<BigDecimal>>> listCurrYear = new ArrayList<Map<String, List<BigDecimal>>>();

		Set<String> keys = map.keySet();
		if (keys != null && keys.size() > 1) {
			Iterator<String> it = keys.iterator();
			if (it.hasNext()) {
				listPrevYear = map.get((String) it.next());

				Iterator itPrvYr = listPrevYear.iterator();
				if (itPrvYr.hasNext()) {
					Map<String, List<BigDecimal>> map2 = (Map<String, List<BigDecimal>>) itPrvYr.next();
					List<BigDecimal> list2 = null;
					list2 = map2.get("Jan");
					sfPrevYearMap1.put("Jan", new BigDecimal(list2.get(0).toString()));
					ylPrevYearMap1.put("Jan", new BigDecimal(list2.get(1).toString()));

					list2 = map2.get("Feb");
					sfPrevYearMap1.put("Feb", new BigDecimal(list2.get(0).toString()));
					ylPrevYearMap1.put("Feb", new BigDecimal(list2.get(1).toString()));

					list2 = map2.get("Mar");
					sfPrevYearMap1.put("Mar", new BigDecimal(list2.get(0).toString()));
					ylPrevYearMap1.put("Mar", new BigDecimal(list2.get(1).toString()));

					list2 = map2.get("Apr");
					sfPrevYearMap1.put("Apr", new BigDecimal(list2.get(0).toString()));
					ylPrevYearMap1.put("Apr", new BigDecimal(list2.get(1).toString()));

					list2 = map2.get("May");
					sfPrevYearMap1.put("May", new BigDecimal(list2.get(0).toString()));
					ylPrevYearMap1.put("May", new BigDecimal(list2.get(1).toString()));

					list2 = map2.get("Jun");
					sfPrevYearMap1.put("Jun", new BigDecimal(list2.get(0).toString()));
					ylPrevYearMap1.put("Jun", new BigDecimal(list2.get(1).toString()));

					list2 = map2.get("Jul");
					sfPrevYearMap1.put("Jul", new BigDecimal(list2.get(0).toString()));
					ylPrevYearMap1.put("Jul", new BigDecimal(list2.get(1).toString()));

					list2 = map2.get("Aug");
					sfPrevYearMap1.put("Aug", new BigDecimal(list2.get(0).toString()));
					ylPrevYearMap1.put("Aug", new BigDecimal(list2.get(1).toString()));

					list2 = map2.get("Sep");
					sfPrevYearMap1.put("Sep", new BigDecimal(list2.get(0).toString()));
					ylPrevYearMap1.put("Sep", new BigDecimal(list2.get(1).toString()));

					list2 = map2.get("Oct");
					sfPrevYearMap1.put("Oct", new BigDecimal(list2.get(0).toString()));
					ylPrevYearMap1.put("Oct", new BigDecimal(list2.get(1).toString()));

					list2 = map2.get("Nov");
					sfPrevYearMap1.put("Nov", new BigDecimal(list2.get(0).toString()));
					ylPrevYearMap1.put("Nov", new BigDecimal(list2.get(1).toString()));

					list2 = map2.get("Dec");
					sfPrevYearMap1.put("Dec", new BigDecimal(list2.get(0).toString()));
					ylPrevYearMap1.put("Dec", new BigDecimal(list2.get(1).toString()));
				}
			}

			if (it.hasNext()) {
				listCurrYear = map.get((String) it.next());
				Iterator itCurrYr = listCurrYear.iterator();

				if (itCurrYr.hasNext()) {
					Map<String, List<BigDecimal>> map2 = (Map<String, List<BigDecimal>>) itCurrYr.next();
					List<BigDecimal> list2 = null;
					list2 = map2.get("Jan");
					sfCurrYearMap1.put("Jan", new BigDecimal(list2.get(0).toString()));
					ylCurrYearMap1.put("Jan", new BigDecimal(list2.get(1).toString()));

					list2 = map2.get("Feb");
					sfCurrYearMap1.put("Feb", new BigDecimal(list2.get(0).toString()));
					ylCurrYearMap1.put("Feb", new BigDecimal(list2.get(1).toString()));

					list2 = map2.get("Mar");
					sfCurrYearMap1.put("Mar", new BigDecimal(list2.get(0).toString()));
					ylCurrYearMap1.put("Mar", new BigDecimal(list2.get(1).toString()));

					list2 = map2.get("Apr");
					sfCurrYearMap1.put("Apr", new BigDecimal(list2.get(0).toString()));
					ylCurrYearMap1.put("Apr", new BigDecimal(list2.get(1).toString()));

					list2 = map2.get("May");
					sfCurrYearMap1.put("May", new BigDecimal(list2.get(0).toString()));
					ylCurrYearMap1.put("May", new BigDecimal(list2.get(1).toString()));

					list2 = map2.get("Jun");
					sfCurrYearMap1.put("Jun", new BigDecimal(list2.get(0).toString()));
					ylCurrYearMap1.put("Jun", new BigDecimal(list2.get(1).toString()));

					list2 = map2.get("Jul");
					sfCurrYearMap1.put("Jul", new BigDecimal(list2.get(0).toString()));
					ylCurrYearMap1.put("Jul", new BigDecimal(list2.get(1).toString()));

					list2 = map2.get("Aug");
					sfCurrYearMap1.put("Aug", new BigDecimal(list2.get(0).toString()));
					ylCurrYearMap1.put("Aug", new BigDecimal(list2.get(1).toString()));

					list2 = map2.get("Sep");
					sfCurrYearMap1.put("Sep", new BigDecimal(list2.get(0).toString()));
					ylCurrYearMap1.put("Sep", new BigDecimal(list2.get(1).toString()));

					list2 = map2.get("Oct");
					sfCurrYearMap1.put("Oct", new BigDecimal(list2.get(0).toString()));
					ylCurrYearMap1.put("Oct", new BigDecimal(list2.get(1).toString()));

					list2 = map2.get("Nov");
					sfCurrYearMap1.put("Nov", new BigDecimal(list2.get(0).toString()));
					ylCurrYearMap1.put("Nov", new BigDecimal(list2.get(1).toString()));

					list2 = map2.get("Dec");
					sfCurrYearMap1.put("Dec", new BigDecimal(list2.get(0).toString()));
					ylCurrYearMap1.put("Dec", new BigDecimal(list2.get(1).toString()));
				}
			}
		}

		for (String key : keys) {
			List<Map<String, List<BigDecimal>>> list = map.get(key);
			if (log.isDebugEnabled()) {
				log.debug("================== key ===================>>" + key);
			}
			for (Iterator iterator = list.iterator(); iterator.hasNext();) {
				Map<String, List<BigDecimal>> map2 = (Map<String, List<BigDecimal>>) iterator.next();
				Set<String> keys2 = map2.keySet();
				for (String key2 : keys2) {
					List<BigDecimal> list2 = map2.get(key2);
					if (!currentYear.equalsIgnoreCase(key)) {
						sfPrevYearMap.put(keys2, list2.get(0));
						yieldPrevYearMap.put(keys2, list2.get(1));
					} else {
						sfCurrYearMap.put(keys2, list2.get(0));
						yieldCurrYearMap.put(keys2, list2.get(1));
					}
					if (log.isDebugEnabled()) {
						log.debug("Key:" + key + " |Key2:" + key2 + " |SF:" + list2.get(0) + "|Yield:" + list2.get(1));
					}

				}
			}
		}

		if (log.isDebugEnabled()) {
			log.debug("sfPrevYearMap -> size::" + sfPrevYearMap.size());
			log.debug("yieldPrevYearMap -> size::" + yieldPrevYearMap.size());
			log.debug("sfCurrYearMap -> size::" + sfCurrYearMap.size());
			log.debug("yieldCurrYearMap -> size::" + yieldCurrYearMap.size());
		}

		int prvYear = Integer.valueOf(currentYear).intValue() - 1;
		salesFactorMap.put(String.valueOf(prvYear), sfPrevYearMap1);
		salesFactorMap.put(currentYear, sfCurrYearMap1);

		yeildFactorMap.put(String.valueOf(prvYear), ylPrevYearMap1);
		yeildFactorMap.put(currentYear, ylCurrYearMap1);

	}

	/**
	 * @return the yearList
	 */
	public List<String> getYearList() {
		return yearList;
	}

	/**
	 * @param yearList
	 *            the yearList to set
	 */
	public void setYearList(List<String> yearList) {
		this.yearList = yearList;
	}

	/**
	 * @return the salesFactorMap
	 */
	public Map<String, Map<String, BigDecimal>> getSalesFactorMap() {
		return salesFactorMap;
	}

	/**
	 * @param salesFactorMap
	 *            the salesFactorMap to set
	 */
	public void setSalesFactorMap(Map<String, Map<String, BigDecimal>> salesFactorMap) {
		this.salesFactorMap = salesFactorMap;
	}

	/**
	 * @return the graph
	 */
	public boolean isGraph() {
		return graph;
	}

	/**
	 * @param graph
	 *            the graph to set
	 */
	public void setGraph(boolean graph) {
		this.graph = graph;
	}

	/**
	 * @return the saleFactor
	 */
	public boolean isSaleFactor() {
		return saleFactor;
	}

	/**
	 * @param saleFactor
	 *            the saleFactor to set
	 */
	public void setSaleFactor(boolean saleFactor) {
		this.saleFactor = saleFactor;
	}

	/**
	 * @return the viewType
	 */
	public String getViewType() {
		return viewType;
	}

	/**
	 * @param viewType
	 *            the viewType to set
	 */
	public void setViewType(String viewType) {
		this.viewType = viewType;
	}

	/**
	 * @return the reportType
	 */
	public String getReportType() {
		return reportType;
	}

	/**
	 * @param reportType
	 *            the reportType to set
	 */
	public void setReportType(String reportType) {
		this.reportType = reportType;
	}

	/**
	 * @return the yeildFactorMap
	 */
	public Map<String, Map<String, BigDecimal>> getYeildFactorMap() {
		return yeildFactorMap;
	}

	/**
	 * @param yeildFactorMap
	 *            the yeildFactorMap to set
	 */
	public void setYeildFactorMap(Map<String, Map<String, BigDecimal>> yeildFactorMap) {
		this.yeildFactorMap = yeildFactorMap;
	}

	/**
	 * @return the radReportOption
	 */
	public String getRadReportOption() {
		return radReportOption;
	}

	/**
	 * @param radReportOption
	 *            the radReportOption to set
	 */
	public void setRadReportOption(String radReportOption) {
		this.radReportOption = radReportOption;
	}

	private int getReportingPeriodYear() throws ModuleException {
		String reportingDate = ReportsHTMLGenerator.getReportingPeriod(AppSysParamsUtil.getReportingPeriod());
		String[] dateArray = reportingDate.split("/");
		return Integer.parseInt(dateArray[dateArray.length - 1]);
	}

}
