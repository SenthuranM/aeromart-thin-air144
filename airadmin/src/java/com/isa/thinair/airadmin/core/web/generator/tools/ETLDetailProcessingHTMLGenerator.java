/*
 * ==============================================================================
 * ISA Software License, Targeted Release Version 1.0
 *
 * Copyright (c) 2005 The Information Systems Associates.  All rights reserved.
 *
 * Redistribution and use of this code in source and binary forms, with or without
 * modification, is not permitted without prior approval from ISA.
 * 
 * Use is subjected to license terms. 
 * 
 * ===============================================================================
 */

package com.isa.thinair.airadmin.core.web.generator.tools;

import java.util.Collection;
import java.util.Iterator;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.util.AiradminUtils;
import com.isa.thinair.airreservation.api.utils.ReservationInternalConstants;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.messagepasser.api.model.ETLPaxEntry;
import com.isa.thinair.messagepasser.api.utils.ParserConstants;

public class ETLDetailProcessingHTMLGenerator {

	private static String clientErrors;
	private static final String PARAM_CURRENT_ETL = "hdnCurrentEtl";
	private static final String PARAM_SEARCH_PROCESS_STATUS = "selProcessType";

	private String strFormFieldsVariablesJS = "";

	/**
	 * Gets the ETL Passenger Details Rows for the Selected ETL
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array of ETL Detail Rows
	 * @throws ModuleException
	 *             the ModuleException
	 * @throws Exception
	 *             the Exception
	 */
	public final String getETLProcessingRowHtml(HttpServletRequest request) throws ModuleException, Exception {

		String strUIModeJS = "var isSearchMode = false; var strETLContent;";
		String strProcessStatus = "";
		strUIModeJS = "var isSearchMode = true; var strETLContent;";

		strProcessStatus = request.getParameter(PARAM_SEARCH_PROCESS_STATUS);
		strFormFieldsVariablesJS += "var processStatus ='" + strProcessStatus + "';";

		request.setAttribute(WebConstants.REQ_OPERATION_UI_MODE, strUIModeJS);
		setFormFieldValues(strFormFieldsVariablesJS);

		return createETLProcessingRowHTML(request);
	}

	/**
	 * Creates the ETL Processing Row for the Selected ETL
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array containig the ETL details
	 * @throws Exception
	 *             the Exception
	 */
	private String createETLProcessingRowHTML(HttpServletRequest request) throws Exception {

		String strCurrentETL = "";

		strCurrentETL = AiradminUtils.getNotNullString(request.getParameter(PARAM_CURRENT_ETL));
		StringBuffer sb = new StringBuffer("var arrData = new Array();");

		String tempDesc = "";
		String paxProcessedStatus = "";
		String paxErrorStatus = "";
		String[] arrCurrentETL = strCurrentETL.split(",");
		Collection<ETLPaxEntry> etlPaxEntries = ModuleServiceLocator.getEtlBD().getEtlParseEntries(
				Integer.valueOf(arrCurrentETL[3]).intValue());

		if (etlPaxEntries != null && !etlPaxEntries.isEmpty()) {
			int i = 0;
			String strTemp = "";
			for (Iterator<ETLPaxEntry> iterEtlPaxEntries = etlPaxEntries.iterator(); iterEtlPaxEntries.hasNext();) {
				ETLPaxEntry etlPaxEntry = (ETLPaxEntry) iterEtlPaxEntries.next();
				paxProcessedStatus = "";
				paxErrorStatus = "";

				sb.append("arrData[" + i + "] = new Array();");

				if (etlPaxEntry.getTitle() != null) {
					sb.append("arrData[" + i + "][0]= '" + etlPaxEntry.getTitle() + "';");
				} else {
					sb.append("arrData[" + i + "][0] = ''; ");
				}

				if (etlPaxEntry.getFirstName() != null) {
					sb.append("arrData[" + i + "][1]= '" + etlPaxEntry.getFirstName() + "'; ");
				} else {
					sb.append("arrData[" + i + "][1] = ''; ");
				}

				sb.append("arrData[" + i + "][16] = ''; ");

				if (etlPaxEntry.getLastName() != null) {
					sb.append("arrData[" + i + "][2] = '" + etlPaxEntry.getLastName() + "'; ");
				} else {
					sb.append("arrData[" + i + "][2] = ''; ");
				}

				sb.append("arrData[" + i + "][3] = '" + etlPaxEntry.getEtlPaxId() + "';");

				if (etlPaxEntry.getArrivalAirport() != null) {
					sb.append("arrData[" + i + "][4] = '" + etlPaxEntry.getArrivalAirport() + "'; ");
				} else {
					sb.append("arrData[" + i + "][4] = ''; ");
				}

				sb.append("arrData[" + i + "][5] = '" + etlPaxEntry.getPaxStatus() + "';");

				if (etlPaxEntry.getProcessedStatus() != null) {
					tempDesc = "";
					if (etlPaxEntry.getProcessedStatus().equals(ParserConstants.ETLProcessStatus.ERROR_OCCURED)
							|| paxErrorStatus != null && !paxErrorStatus.equals("")) {
						tempDesc = "Error Occured";
						if (etlPaxEntry.getErrorDescription() != null && !etlPaxEntry.getErrorDescription().trim().equals("")) {
							strTemp = "<a HREF='javascript:void(0);' title='"
									+ (etlPaxEntry.getErrorDescription() == null ? "" : etlPaxEntry.getErrorDescription().trim())
									+ "'>" + tempDesc + "</a>";
							char doublequote = '"';
							sb.append("arrData[" + i + "][6] = " + doublequote + strTemp + doublequote + ";");
						} else if (paxErrorStatus != null && !paxErrorStatus.trim().equals("")) {
							strTemp = "<a HREF='javascript:void(0);' title='"
									+ (paxErrorStatus == null ? "" : paxErrorStatus.trim()) + "'>" + tempDesc + "</a>";
							char doublequote = '"';
							sb.append("arrData[" + i + "][6] = " + doublequote + strTemp + doublequote + ";");

						} else {
							sb.append("arrData[" + i + "][6] = '" + tempDesc + "';");
						}
					} else if (etlPaxEntry.getProcessedStatus().equals(
							ReservationInternalConstants.PfsProcessStatus.NOT_PROCESSED)) {
						tempDesc = "Not Processed";
						sb.append("arrData[" + i + "][6] = '" + tempDesc + "';");
					} else if (etlPaxEntry.getProcessedStatus().equals(ParserConstants.ETLProcessStatus.PROCESSED)) {
						if (paxProcessedStatus.equals("")
								|| paxProcessedStatus.equals(ParserConstants.ETLProcessStatus.PROCESSED)) {
							tempDesc = "Processed";
							if (etlPaxEntry.getErrorDescription() != null && !etlPaxEntry.getErrorDescription().trim().equals("")) {
								strTemp = "<a HREF='javascript:void(0);' title='"
										+ (etlPaxEntry.getErrorDescription() == null ? "" : etlPaxEntry.getErrorDescription().trim())
										+ "'>" + tempDesc + "</a>";
								char doublequote = '"';
								sb.append("arrData[" + i + "][6] = " + doublequote + strTemp + doublequote + ";");
							}else{
								sb.append("arrData[" + i + "][6] = '" + tempDesc + "';");
							}
						} else {
							tempDesc = "Not Processed";
							sb.append("arrData[" + i + "][6] = '" + tempDesc + "';");
						}
					} else {
						tempDesc = "Unknown";
						sb.append("arrData[" + i + "][6] = '" + tempDesc + "';");
					}
				} else {
					sb.append("arrData[" + i + "][6] = '';");
				}

				sb.append("arrData[" + i + "][7] = '" + etlPaxEntry.getPaxStatus() + "';");

				if (etlPaxEntry.getProcessedStatus() != null) {
					sb.append("arrData[" + i + "][8] = '" + etlPaxEntry.getProcessedStatus() + "';");
				} else {
					sb.append("arrData[" + i + "][8] = '';");
				}
				sb.append("arrData[" + i + "][9] = '" + etlPaxEntry.getEtlId() + "';");
				sb.append("arrData[" + i + "][10] = '" + etlPaxEntry.getVersion() + "';");
				sb.append("arrData[" + i + "][11] = '" + etlPaxEntry.getEtlId().toString() + "';");
				sb.append("arrData[" + i + "][12] = '"
						+ (etlPaxEntry.getErrorDescription() == null ? "" : etlPaxEntry.getErrorDescription().trim()) + "';");
				sb.append("arrData[" + i + "][13] = '" + "" + "';");
				if (etlPaxEntry.getPaxType() != null) {
					sb.append("arrData[" + i + "][14]= '" + etlPaxEntry.getPaxType() + "'; ");
				} else {
					sb.append("arrData[" + i + "][14] = ''; ");
				}
				if (etlPaxEntry.getExternalEticketNumber() != null) {
					sb.append("arrData[" + i + "][15] = '" + etlPaxEntry.getExternalEticketNumber() + "';");
				} else if (etlPaxEntry.getEticketNumber() != null) {
					sb.append("arrData[" + i + "][15] = '" + etlPaxEntry.getEticketNumber() + "';");
				} else {
					sb.append("arrData[" + i + "][15] = ''; ");
				}
				sb.append("arrData[" + i + "][16] = '" + "N" + "';");
				sb.append("arrData[" + i + "][17] = '" + etlPaxEntry.getCoupNumber() + "';");
				sb.append("arrData[" + i + "][18] = '" + etlPaxEntry.getInfCoupNumber() + "';");
				if (etlPaxEntry.getExternalInfEticketNumber() != null) {
					sb.append("arrData[" + i + "][19] = '" + etlPaxEntry.getExternalInfEticketNumber() + "'; ");
				} else if (etlPaxEntry.getInfEticketNumber() != null) {
					sb.append("arrData[" + i + "][19] = '" + etlPaxEntry.getInfEticketNumber() + "'; ");
				} else {
					sb.append("arrData[" + i + "][19] = ''; ");
				}
				i++;
			}
		}
		return sb.toString();
	}

	/**
	 * Creates Client Validations for ETL Detail Page
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @return String the Array Containing the Client Validations
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String getClientErrors(HttpServletRequest request) throws ModuleException {

		if (clientErrors == null) {
			Properties moduleErrs = new Properties();
			moduleErrs.setProperty("um.airadmin.form.coupenNumberReq", "coupenNoReq");
			moduleErrs.setProperty("um.airadmin.form.eTicketReq", "eTicketReq");
			moduleErrs.setProperty("um.airadmin.delete.confirmation", "deleteMessage");
			moduleErrs.setProperty("um.process.form.title.required", "titleReq");
			clientErrors = JavascriptGenerator.createClientErrors(moduleErrs);
		}
		return clientErrors;
	}

	private void setFormFieldValues(String strFormFieldsVariablesJS) {
		this.strFormFieldsVariablesJS = strFormFieldsVariablesJS;
	}

	public String getFormFieldValues() {
		return strFormFieldsVariablesJS;
	}
}
