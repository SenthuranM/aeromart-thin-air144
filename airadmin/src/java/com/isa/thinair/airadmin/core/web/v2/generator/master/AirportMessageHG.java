package com.isa.thinair.airadmin.core.web.v2.generator.master;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.common.JavascriptGenerator;
import com.isa.thinair.airadmin.core.web.v2.util.CommonUtil;
import com.isa.thinair.airmaster.api.model.AirportMsgFlight;
import com.isa.thinair.airmaster.api.model.AirportMsgOND;
import com.isa.thinair.airmaster.api.model.AirportMsgSalesChannel;
import com.isa.thinair.airmaster.api.model.I18nMessage;
import com.isa.thinair.airpricing.api.model.Channel;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.v2.util.MultiSelectDataParser;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;
import com.isa.thinair.webplatform.core.util.MultiSelectConverterUtil;

/**
 * @author eric
 * 
 */
public class AirportMessageHG {

	private static Log log = LogFactory.getLog(AirportMessageHG.class);

	/**
	 * @param request
	 * @throws ModuleException
	 */
	public static void getClientErrors(HttpServletRequest request) throws ModuleException {

		Properties moduleErrs = new Properties();
		moduleErrs.setProperty("um.airportMsg.airport.empty", "megAirportEmpty");
		moduleErrs.setProperty("um.airportMsg.dep.Arr.type.empty", "msgDepArrTypeEmpty");
		moduleErrs.setProperty("um.airportMsg.message.type.empty", "msgTypeEmpty");
		moduleErrs.setProperty("um.airportMsg.display.from.date.empty", "msgDisplayFromDateEmpty");
		moduleErrs.setProperty("um.airportMsg.display.to.date.empty", "msgDisplayToDateEmpty");
		moduleErrs.setProperty("um.airportMsg.valid.from.date.empty", "msgValidFromDateEmpty");
		moduleErrs.setProperty("um.airportMsg.valid.to.date.empty", "msgValidToDateEmpty");
		moduleErrs.setProperty("um.airportMsg.message.empty", "msgEmpty");
		moduleErrs.setProperty("um.airportMsg.display.to.date.should.future", "msgDipalyToDateFuture");
		moduleErrs.setProperty("um.airportMsg.display.from.date.should.future", "msgDipalyFromDateFuture");
		moduleErrs.setProperty("um.airportMsg.valid.from.date.should.future", "msgValidFromDateFuture");
		moduleErrs.setProperty("um.airportMsg.valid.to.date.should.future", "msgValidToDateFuture");
		moduleErrs.setProperty("um.airportMsg.display.from.date.greater.to.date", "msgDisplayFromDateGrater");
		moduleErrs.setProperty("um.airportMsg.valid.from.date.greater.to.date", "msgValidFromDateGrater");

		moduleErrs.setProperty("um.airportMsg.flight.numers.inclusion", "msgSelectFlightInclusion");
		moduleErrs.setProperty("um.airportMsg.ond.inclusion", "msgSelectOndInclusion");

		moduleErrs.setProperty("um.airportMsg.ond.arrival.required", "ondArrivalReq");
		moduleErrs.setProperty("um.airportMsg.ond.depature.required", "ondDeparturereq");
		moduleErrs.setProperty("um.airportMsg.ond.exists", "ondExists");
		moduleErrs.setProperty("um.airportMsg.ond.arrivalDepature.same", "ondArrDepSame");

		moduleErrs.setProperty("um.airportMsg.ond.arrivalDepature.same", "ondArrDepSame");

		moduleErrs.setProperty("um.airportMsg.ond.arrivalVia.same", "ondArrViaSame");
		moduleErrs.setProperty("um.airportMsg.ond.depatureVia.same", "ondDepViaSame");
		moduleErrs.setProperty("um.airportMsg.ond.via.sequence", "ondViaSequnce");
		moduleErrs.setProperty("um.airportMsg.ond.via.equal", "ondVispointsEqual");

		request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, JavascriptGenerator.createClientErrors(moduleErrs));
	}

	/**
	 * @param request
	 * @throws ModuleException
	 */
	public static void setDisplayData(HttpServletRequest request) throws ModuleException {
		setFlightNumbersList(request);
		setSalesChannelsList(request);
		request.setAttribute(WebConstants.REQ_HTML_ACTIVE_AIRPORT_SELECT_LIST, SelectListGenerator.createActiveAirportList());
		request.setAttribute(WebConstants.REQ_HTML_LANGUAGE_LIST, SelectListGenerator.createLanguageList());
	}

	/**
	 * @param request
	 * @throws ModuleException
	 */
	private static void setSalesChannelsList(HttpServletRequest request) throws ModuleException {
		List<Map<String, String>> mapedCodes = SelectListGenerator.createBookingOriginatedSalesChannelList();
		request.setAttribute(WebConstants.REQ_HTML_SALES_CHANNEL_LIST, CommonUtil.convertToJSON(MultiSelectConverterUtil
				.convertMapedStringsFromColumnMap(mapedCodes, "SALES_CHANNEL_CODE", "SALES_CHANNEL", "DESCRIPTION")));

	}

	/**
	 * @param request
	 * @throws ModuleException
	 */
	private static void setFlightNumbersList(HttpServletRequest request) throws ModuleException {
		List<Map<String, String>> mapedCodes = SelectListGenerator.createFlightNumbers();
		request.setAttribute(WebConstants.REQ_HTML_FLIGHTNUMBER, CommonUtil.convertToJSON(MultiSelectConverterUtil
				.convertMapedStringsFromColumnMap(mapedCodes, "FLIGHT_NUMBER", "FLIGHT_NUMBER", "FLIGHT_NUMBER")));

	}

	/**
	 * @param flighstSet
	 * @param categoryCode
	 * @return
	 */
	public static Object[] convertFlightList(Set<AirportMsgFlight> flighstSet, String categoryCode) {
		String includeStatus = null;
		List<MultiSelectDataParser> multiSelectDataList = new ArrayList<MultiSelectDataParser>();
		for (AirportMsgFlight setItem : flighstSet) {
			MultiSelectDataParser parser = new MultiSelectDataParser();
			if (includeStatus == null)
				includeStatus = setItem.getApplyStatus();
			parser.setCategory(categoryCode);
			parser.setId(setItem.getFlightNo());
			parser.setText(setItem.getFlightNo());
			multiSelectDataList.add(parser);
		}
		Object[] data = new Object[2];
		data[0] = multiSelectDataList;
		data[1] = includeStatus;
		return data;
	}

	/**
	 * @param onds
	 * @param categoryCode
	 * @return
	 */
	public static Object[] convertONDList(Set<AirportMsgOND> onds, String categoryCode) {
		String includeStatus = null;
		List<MultiSelectDataParser> multiSelectDataList = new ArrayList<MultiSelectDataParser>();
		for (AirportMsgOND setItem : onds) {
			MultiSelectDataParser parser = new MultiSelectDataParser();
			if (includeStatus == null)
				includeStatus = setItem.getApplyStatus();
			parser.setCategory(categoryCode);
			parser.setId(setItem.getOndCode());
			parser.setText(setItem.getOndCode());
			multiSelectDataList.add(parser);
		}
		Object[] data = new Object[2];
		data[0] = multiSelectDataList;
		data[1] = includeStatus;
		return data;
	}

	/**
	 * @param salesChannels
	 * @param channelMap
	 * @param categoryCode
	 * @return
	 */
	public static Object[] convertSalesChannelList(Set<AirportMsgSalesChannel> salesChannels,
			HashMap<Integer, Channel> channelMap, String categoryCode) {
		List<MultiSelectDataParser> multiSelectDataList = new ArrayList<MultiSelectDataParser>();
		for (AirportMsgSalesChannel setItem : salesChannels) {
			MultiSelectDataParser parser = new MultiSelectDataParser();
			parser.setCategory(categoryCode);
			Channel channle = channelMap.get(setItem.getSalesChannelId());
			if (channle != null) {
				parser.setId(String.valueOf(channle.getChannelId()));
				parser.setText(channle.getChannelDescription() != null ? channle.getChannelDescription() : "");
				multiSelectDataList.add(parser);
			}
		}
		Object[] data = new Object[2];
		data[0] = multiSelectDataList;
		return data;
	}

	/**
	 * @param messages
	 * @return
	 * @throws IOException
	 * @throws SQLException
	 */
	public static List<MultiSelectDataParser> convertLanguageTranslations(Set<I18nMessage> messages) {
		List<MultiSelectDataParser> translations = new ArrayList<MultiSelectDataParser>();

		for (I18nMessage message : messages) {
			MultiSelectDataParser parser = new MultiSelectDataParser();
			parser.setId(message.getMessageLocale());
			parser.setText(message._getMsgContent());
			parser.setCategory(message.getMessageLocale());
			translations.add(parser);
		}
		return translations;
	}

	/**
	 * @return
	 * @throws ModuleException
	 */
	public static HashMap<Integer, Channel> getSaleasChannels() throws ModuleException {
		Collection<Channel> salesChannels = ModuleServiceLocator.getRuleBD().getAllSalesChannels();
		HashMap<Integer, Channel> salesChannelMap = new HashMap<Integer, Channel>();
		for (Channel channel : salesChannels) {
			salesChannelMap.put(channel.getChannelId(), channel);
		}
		return salesChannelMap;
	}

}
