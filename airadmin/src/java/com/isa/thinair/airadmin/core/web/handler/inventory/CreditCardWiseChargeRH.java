package com.isa.thinair.airadmin.core.web.handler.inventory;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airpricing.api.dto.CCChargeTO;
import com.isa.thinair.airpricing.api.model.PaymentGatewayWiseCharges;
import com.isa.thinair.commons.api.dto.ModuleCriterion;
import com.isa.thinair.commons.api.dto.Page;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

public class CreditCardWiseChargeRH {

	private static Log log = LogFactory.getLog(CreditCardWiseChargeRH.class);

	public static Page<CCChargeTO> searchCCWiseCharge(int pageNo, List<ModuleCriterion> criterion) {
		// call back end

		Page<CCChargeTO> page = null;

		if (pageNo == 0) {
			pageNo = 1;
		}
		page = ModuleServiceLocator.getChargeBD().getPaymentGatewayWiseCharge((pageNo - 1) * 20, 20, criterion);

		return page;
	}

	public static String getPaymentGateways() throws ModuleException {
		return SelectListGenerator.createPGWList();
	}

	public static boolean savePaymentGatewayWiseCharges(PaymentGatewayWiseCharges paymentGatewayWiseCharges)
			throws ModuleException {
		ModuleServiceLocator.getChargeBD().savePaymentGatewayWiseCharges(paymentGatewayWiseCharges);
		return true;
	}

	public static PaymentGatewayWiseCharges getPaymentGatewayWiseCharges(int chargeId) throws ModuleException {
		return ModuleServiceLocator.getChargeBD().getUniquePaymentGatewayWiseCharge(chargeId);

	}

}
