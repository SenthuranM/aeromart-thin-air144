package com.isa.thinair.airadmin.core.web.handler.reports;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.constants.AdminStrutsConstants;
import com.isa.thinair.airadmin.core.web.constants.WebConstants;
import com.isa.thinair.airadmin.core.web.generator.reports.ReportsHTMLGenerator;
import com.isa.thinair.airadmin.core.web.handler.common.BasicRequestHandler;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.constants.SystemParamKeys;
import com.isa.thinair.commons.core.util.AppSysParamsUtil;
import com.isa.thinair.commons.core.util.GlobalConfig;
import com.isa.thinair.reporting.api.criteria.ReportsSearchCriteria;

public class MealsDetailReportRH extends BasicRequestHandler {
	private static Log log = LogFactory.getLog(MealsDetailReportRH.class);

	private static GlobalConfig globalConfig = ModuleServiceLocator.getGlobalConfig();

	/**
	 * The constructor.
	 */
	public MealsDetailReportRH() {
		super();
	}

	/**
	 * @param request
	 * @return
	 */
	public static String execute(HttpServletRequest request, HttpServletResponse response) {
		String strHdnMode = request.getParameter("hdnMode");

		String forward = AdminStrutsConstants.AdminAction.SUCCESS;

		try {
			setDisplayData(request);
			log.debug("MealsDetailReportRH success");
		} catch (Exception e) {
			forward = "error";
			log.error("MealsDetailReportRH execute()" + e.getMessage());
		}

		try {
			if (strHdnMode != null && strHdnMode.equals(WebConstants.ACTION_VIEW)) {
				setReportView(request, response);
				log.error("MealsDetailReportRH setReportView Success");
				return null;
			} else {
				log.error("MealsDetailReportRH setReportView not selected");
			}

		} catch (ModuleException e) {
			saveMessage(request, e.getMessageString(), WebConstants.MSG_ERROR);
			log.error("MealsDetailReportRH setReportView Failed " + e.getMessageString());
		}

		return forward;
	}

	/**
	 * 
	 * @param request
	 * @throws Exception
	 */
	public static void setDisplayData(HttpServletRequest request) throws Exception {
		setClientErrors(request);
		setLiveStatus(request);
		ReportsHTMLGenerator.createPreferedReportOptions(request);

	}

	/**
	 * Sets the report medium to the request
	 * 
	 * @param request
	 *            the HttpServletRequest
	 * @throws ModuleException
	 *             the ModuleExceptions
	 */
	private static void setLiveStatus(HttpServletRequest request) throws ModuleException {
		String strLive = request.getParameter(WebConstants.REP_HDN_LIVE);
		if (strLive != null)
			request.setAttribute(WebConstants.REP_SET_LIVE, strLive);

	}

	/**
	 * sets the Status list
	 * 
	 * @param request
	 */

	public static void setClientErrors(HttpServletRequest request) {
		try {
			String strClientErrors = ReportsHTMLGenerator.getClientErrors(request);

			if (strClientErrors == null) {
				strClientErrors = "";
			}
			request.setAttribute(WebConstants.REQ_CLIENT_MESSAGES, strClientErrors);
		} catch (ModuleException moduleException) {
			log.error("setClientErrors() method is failed :" + moduleException.getMessageString());
			saveMessage(request, moduleException.getMessageString(), WebConstants.MSG_ERROR);
		}
	}

	/**
	 * 
	 * @param request
	 * @throws ModuleException
	 */
	protected static void setReportView(HttpServletRequest request, HttpServletResponse response) throws ModuleException {

		ResultSet resultSet = null;
		String agents = "";
		String fromDate = request.getParameter("txtFromDate");
		String toDate = request.getParameter("txtToDate");
		String flightNo = request.getParameter("txtFlightNumber");
		String reportType = request.getParameter("hdnRptType");
		String value = request.getParameter("radReportOption");

		String strlive = request.getParameter(WebConstants.REP_HDN_LIVE);

		flightNo = (flightNo == null) ? "" : flightNo.trim();

		String id = "UC_REPM_068";
		String reportTemplateSummary = "MealsSummaryReport.jasper";
		String reportTemplateDetails = "MealsDetailsReport.jasper";
		Map<String, Object> parameters = new HashMap<String, Object>();

		try {
			ReportsSearchCriteria search = new ReportsSearchCriteria();

			if (strlive != null) {
				if (strlive.equals(WebConstants.REP_LIVE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_DEFAULT);
				} else if (strlive.equals(WebConstants.REP_OFFLINE)) {
					search.setDataSoureType(ReportsSearchCriteria.DATASOURCE_TYPE_RPT);
				}
			}

			if (reportType.equals("Summary")) {
				ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
						ReportsHTMLGenerator.getReportTemplate(reportTemplateSummary));
				search.setReportType(ReportsSearchCriteria.MEALS_SUMMARY);

				if (toDate != null && !toDate.equals("")) {
					search.setDateRangeTo(ReportsHTMLGenerator.convertDate(toDate));
				}
				if (fromDate != null && !fromDate.equals("")) {
					search.setDateRangeFrom(ReportsHTMLGenerator.convertDate(fromDate));
				}
				search.setFlightNumber(flightNo);

				parameters.put("FLIGHT_NUMBER", flightNo.trim().toUpperCase());
				parameters.put("FROM_DATE", search.getDateRangeFrom());
				parameters.put("TO_DATE", search.getDateRangeTo());

			} else {

				String mealsname = request.getParameter("hdnMealsName");

				String fltNo = request.getParameter("hdnFltNo");

				String toDt = request.getParameter("hdnToDt");

				String fromDt = request.getParameter("hdnFromDt");

				fltNo = (fltNo == null) ? "" : fltNo.trim();
				ModuleServiceLocator.getReportingFrameworkBD().storeJasperTemplateRefs("WebReport1",
						ReportsHTMLGenerator.getReportTemplate(reportTemplateDetails));

				search.setReportType(ReportsSearchCriteria.MEALS_DETAILS);
				search.setMealsName(mealsname);

				search.setFlightNumber(fltNo.trim());
				search.setDateRangeTo(toDt);
				search.setDateRangeFrom(fromDt);
				parameters.put("FROM_DATE", fromDt);
				parameters.put("TO_DATE", toDt);

				// String strCredit = globalConfig.getBizParam(SystemParamKeys.BASE_CURRENCY);
				// parameters.put("AMT_1","("+strCredit+")");
			}

			resultSet = ModuleServiceLocator.getDataExtractionBD().getMealsSummaryData(search);

			String passengersSummary = getBundledPassengersSummary(ModuleServiceLocator.getDataExtractionBD().getPassengersMealSummary(search));
			parameters.put("PASSENGER_SUMMARY",passengersSummary);

			String strLogo = AppSysParamsUtil.getReportLogo(false); // globalConfig.getBizParam(SystemParamKeys.REPORT_LOGO);
			String reportsRootDir = "../../images/" + AppSysParamsUtil.getReportLogo(true); // AA145.jpg
			String strCarrier = globalConfig.getBizParam(SystemParamKeys.CARRIER_NAME) + " Reservation System";

			parameters.put("ID", id);
			parameters.put("OPTION", value);

			// To provide Report Format Options
			String reportNumFormat = request.getParameter("radRptNumFormat");
			ReportsHTMLGenerator.setPreferedReportFormat(reportNumFormat, parameters);

			parameters.put(WebConstants.REP_CARRIER_NAME, strCarrier);
			parameters.put("REPORT_MEDIUM", strlive);
			parameters.put("FOC_MEAL_ENABLED", Boolean.toString(AppSysParamsUtil.isFOCMealSelectionEnabled()));

			if (value.trim().equals("HTML")) {
				parameters.put("IMG", reportsRootDir);
				ModuleServiceLocator.getReportingFrameworkBD().createHTMLReport("WebReport1", parameters, resultSet, null, null,
						response);
			} else if (value.trim().equals("PDF")) {
				reportsRootDir = ReportsHTMLGenerator.getReportTemplate(strLogo);
				parameters.put("IMG", reportsRootDir);
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=MealsDetailReport.pdf");
				ModuleServiceLocator.getReportingFrameworkBD().createPDFReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("EXCEL")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=MealsDetailReport.xls");
				ModuleServiceLocator.getReportingFrameworkBD().createXLSReport("WebReport1", parameters, resultSet, response);
			} else if (value.trim().equals("CSV")) {
				response.reset();
				response.addHeader("Content-Disposition", "attachment;filename=MealsDetailReport.csv");
				ModuleServiceLocator.getReportingFrameworkBD().createCSVReport("WebReport1", parameters, resultSet, response);

			}
		} catch (Exception e) {
			log.error(e);
		}
	}
	
	private static String getBundledPassengersSummary(ResultSet resultSet) throws SQLException {
		boolean isEmpty =  !resultSet.isBeforeFirst();
		if(isEmpty){
			return "No passengers with bundled meals";
		}
		StringBuilder passengersSummary = new StringBuilder("");
		while(resultSet.next()){
			String bundledName = resultSet.getString("BUNDLE_NAME");
			String number = resultSet.getString("NUMBER_OF_PASSENGERS");
			passengersSummary.append(bundledName + ": " + number + " passengers,");
		}
		return passengersSummary.toString().replaceAll(",$", "");
	}

}
