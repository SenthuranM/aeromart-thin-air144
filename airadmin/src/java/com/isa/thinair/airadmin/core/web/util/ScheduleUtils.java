package com.isa.thinair.airadmin.core.web.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airmaster.api.model.Airport;
import com.isa.thinair.airmaster.api.model.AirportDST;
import com.isa.thinair.airschedules.api.model.CodeShareMCFlightSchedule;
import com.isa.thinair.airschedules.api.model.FlightSchedule;
import com.isa.thinair.airschedules.api.model.FlightScheduleLeg;
import com.isa.thinair.airschedules.api.model.FlightScheduleSegment;
import com.isa.thinair.airschedules.api.model.FlightSegement;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.commons.core.util.CalendarUtil;

public class ScheduleUtils {

	private static Log log = LogFactory.getLog(ScheduleUtils.class);

	/**
	 * Formats specified date with specified String format
	 * 
	 * @param utilDate
	 *            the Date need to be Formatted
	 * @param fmt
	 *            the Format
	 * @return String the Formatted Date
	 */
	public static String formatDate(Date utilDate, String fmt) {

		String strDate = "";
		if (utilDate != null) {
			SimpleDateFormat sdFmt = null;
			sdFmt = new SimpleDateFormat(fmt);
			strDate = sdFmt.format(utilDate);
		}

		return strDate;
	}

	/**
	 * Parses the Date
	 * 
	 * @param strDate
	 *            the String Date
	 * @return Date the Parsed Date
	 */
	public static Date parseToDate(String strDate) {
		Date date = null;
		SimpleDateFormat dateFormat = null;

		if (strDate.indexOf('-') != -1)
			dateFormat = new SimpleDateFormat("dd-MM-yy");
		if (strDate.indexOf('/') != -1)
			dateFormat = new SimpleDateFormat("dd/MM/yy");
		try {
			date = dateFormat.parse(strDate);
		} catch (Exception e) {
			log.error(e);
		}

		return date;
	}

	/**
	 * Method to Get the Segment From the Legs
	 * 
	 * @param legset
	 *            the Set Containg the Legs "CMB-SHJ","SHJ-DOH"
	 * @return HashSet the Set containg the Segments "CMB/SHJ","SHJ/DOH","CMB/SHJ/DOH"
	 */
	public static HashSet<FlightScheduleSegment> getSegments(Set<FlightScheduleLeg> legset, HashMap<String, FlightSegement> map) {

		HashSet<FlightScheduleSegment> segset = new HashSet<FlightScheduleSegment>();

		for (int i = 1; i < legset.size() + 1; i++) {

			Iterator<FlightScheduleLeg> legIt = legset.iterator();
			while (legIt.hasNext()) {

				FlightScheduleLeg leg1 = (FlightScheduleLeg) legIt.next();
				if (leg1.getLegNumber() == i) {

					String segmentCode = leg1.getOrigin() + "/" + leg1.getDestination();

					// create the segment
					FlightScheduleSegment seg1 = new FlightScheduleSegment();
					seg1.setSegmentCode(segmentCode);
					// This is to set terminal ids for the flight
					if (!map.isEmpty() && null != map.get(segmentCode)) {
						seg1.setArrivalTerminalId(((FlightSegement) map.get(segmentCode)).getArrivalTerminalId());
						seg1.setDepartureTerminalId(((FlightSegement) map.get(segmentCode)).getDepartureTerminalId());
					}
					segset.add(seg1);

					// creating following segments
					for (int j = i + 1; j < legset.size() + 1; j++) {

						Iterator<FlightScheduleLeg> restLegIt = legset.iterator();
						while (restLegIt.hasNext()) {

							FlightScheduleLeg leg2 = (FlightScheduleLeg) restLegIt.next();
							if (leg2.getLegNumber() == j) {

								segmentCode = segmentCode + "/" + leg2.getDestination();

								// create folllowing segment
								FlightScheduleSegment seg2 = new FlightScheduleSegment();
								seg2.setSegmentCode(segmentCode);
								// This is to set terminal ids for the flight
								if (!map.isEmpty() && null != map.get(segmentCode)) {
									seg2.setArrivalTerminalId(((FlightSegement) map.get(segmentCode)).getArrivalTerminalId());
									seg2.setDepartureTerminalId(((FlightSegement) map.get(segmentCode)).getDepartureTerminalId());
								}
								segset.add(seg2);

								break;
							}
						}
					}
					break;
				}
			}
		}
		return segset;
	}

	/**
	 * Method to Get Segmnets Validity
	 * 
	 * @param strArr
	 *            the Segment Array
	 * @return HashSet of Segments with Segments Validity
	 */
	public static HashSet<FlightScheduleSegment> getValidatedSegments(String strArr, HashMap<String, FlightSegement> map) {

		HashSet<FlightScheduleSegment> segset = new HashSet<FlightScheduleSegment>();

		StringTokenizer st = new StringTokenizer(strArr, ",");
		while (st.hasMoreTokens()) {

			FlightScheduleSegment seg = new FlightScheduleSegment();
			String strToken = st.nextToken();
			String[] strValSegs = strToken.split("_");
			seg.setSegmentCode(strValSegs[0]);
			seg.setValidFlag(strValSegs[1].equals("true"));
			// This is to set terminal ids for the flight
			if (!map.isEmpty() && null != map.get(strValSegs[0])) {
				seg.setArrivalTerminalId(((FlightSegement) map.get(strValSegs[0])).getArrivalTerminalId());
				seg.setDepartureTerminalId(((FlightSegement) map.get(strValSegs[0])).getDepartureTerminalId());
			}
			segset.add(seg);
		}
		return segset;

	}

	/**
	 * Get the Arranged,Validate Terminal set from a String
	 * 
	 * @param strArr
	 *            the Segment Array
	 * @return HashMap the map containg Terminals
	 */
	public static HashMap<String, FlightSegement> getValidatedTerminals(String strArr) {
		HashMap<String, FlightSegement> segmap = new HashMap<String, FlightSegement>();
		StringTokenizer st = new StringTokenizer(strArr, "|");
		try {
			while (st.hasMoreTokens()) {
				FlightSegement seg = new FlightSegement();
				String strToken = st.nextToken();
				String[] strValSegs = strToken.split("\\^");
				if (strValSegs.length >= 2 && !("").equals(strValSegs[1]))
					seg.setDepartureTerminalId(Integer.parseInt(strValSegs[1]));
				if (strValSegs.length == 3 && !("").equals(strValSegs[2]))
					seg.setArrivalTerminalId(Integer.parseInt(strValSegs[2]));
				segmap.put(strValSegs[0], seg);
			}
		} catch (Exception e) {
			log.error(e);
		}
		return segmap;
	}

	/**
	 * Method to Get a Single Segment form the legs
	 * 
	 * @param strLegs
	 *            the Legs "CMB,SHJ,SHJ,DOH,DOH,LUX,LUX,GNI"
	 * @return String the Single segment "CMB/SHJ/DOH/LUX/GNI"
	 */
	public static String getSegment(String strLegs) {

		ArrayList<String> arrList = new ArrayList<String>();
		String strSegment = "";
		String input = strLegs;
		StringTokenizer st = new StringTokenizer(input, ","); //
		String previousToken = "";
		while (st.hasMoreTokens()) {
			String currentToken = st.nextToken();

			if (!previousToken.equals(currentToken)) {
				arrList.add(currentToken);
			}

			previousToken = currentToken;

		}
		Object[] segments = arrList.toArray();
		for (int i = 0; i < segments.length; i++) {
			strSegment += (String) segments[i] + "/";
		}
		if (!("".equals(strSegment))) {
			strSegment = strSegment.substring(0, strSegment.lastIndexOf("/"));
		}

		return strSegment;
	}

	/**
	 * Gets Terminal String from a set
	 * 
	 * @param set
	 *            the Collection of Flight Segments
	 * @return String the segment String
	 */
	public static String getTerminal(Set<FlightScheduleSegment> set) {
		String strSeg = "";
		String strArrivalTerminal = "";
		String strDepartureTerminal = "";
		StringBuffer strTerminalArray = new StringBuffer();
		FlightScheduleSegment seg = null;
		if (set != null) {
			Iterator<FlightScheduleSegment> ite = set.iterator();

			if (ite != null) {
				while (ite.hasNext()) {
					seg = (FlightScheduleSegment) ite.next();
					strSeg = seg.getSegmentCode();

					if (seg.getArrivalTerminalId() == null)
						strArrivalTerminal = "";
					else
						strArrivalTerminal = seg.getArrivalTerminalId().toString();
					if (seg.getDepartureTerminalId() == null)
						strDepartureTerminal = "";
					else
						strDepartureTerminal = seg.getDepartureTerminalId().toString();
					strTerminalArray.append("|");
					strTerminalArray.append(strSeg);
					strTerminalArray.append("^");
					strTerminalArray.append(strDepartureTerminal);
					strTerminalArray.append("^");
					strTerminalArray.append(strArrivalTerminal);

				}
			}
		}
		return strTerminalArray.length() > 0 ? strTerminalArray.toString().substring(1) : "";
	}

	/**
	 * Method to Compare Segment Set & ValidSegment Set
	 * 
	 * @param legSeg
	 *            the Set of Legs
	 * @param validSeg
	 *            the Set of Valid Segments
	 * @return HashSet the Compared Segment Set
	 */
	public static HashSet<FlightScheduleSegment> getComparedSegments(HashSet<FlightScheduleSegment> legSeg, HashSet<FlightScheduleSegment> validSeg) {
		HashSet<FlightScheduleSegment> segset = new HashSet<FlightScheduleSegment>();
		Iterator<FlightScheduleSegment> legIte = legSeg.iterator();

		while (legIte.hasNext()) {
			FlightScheduleSegment leg = (FlightScheduleSegment) legIte.next();
			Iterator<FlightScheduleSegment> validIte = validSeg.iterator();
			while (validIte.hasNext()) {
				FlightScheduleSegment valid = (FlightScheduleSegment) validIte.next();
				if (leg.getSegmentCode().equals(valid.getSegmentCode())) {
					leg.setValidFlag(valid.getValidFlag());
					leg.setArrivalTerminalId(valid.getArrivalTerminalId());
					leg.setDepartureTerminalId(valid.getDepartureTerminalId());
				}
			}
			segset.add(leg);
		}

		return segset;
	}

	/**
	 * Method to get an Array of Segnets for a given Leg String
	 * 
	 * @param strLegs
	 *            the Leg String "CMB,SHJ,SHJ,DOH,DOH,LUX,LUX,GNI";
	 * @return ArrayList the List of Segments
	 */
	public static ArrayList<String> getSgements(String strLegs) {
		ArrayList<String> arrList = new ArrayList<String>();
		String input = strLegs;
		StringTokenizer st = new StringTokenizer(input, ","); //
		String previousToken = "";
		while (st.hasMoreTokens()) {
			String currentToken = st.nextToken();

			if (!previousToken.equals(currentToken)) {
				arrList.add(currentToken);
			}

			previousToken = currentToken;

		}

		// Next to get segments
		ArrayList<String> segmentArray = new ArrayList<String>();
		for (int i = 0; i < (arrList.size() - 1); i++) {
			segmentArray.add(arrList.get(i) + "/" + arrList.get(i + 1));
		}

		if (arrList.size() > 2) {
			for (int i = 0; i < (arrList.size() - 2); i++) {
				segmentArray.add(arrList.get(i) + "/" + arrList.get(i + 1) + "/" + arrList.get(i + 2));
			}
		}

		if (arrList.size() > 3) {
			for (int i = 0; i < (arrList.size() - 3); i++) {
				segmentArray.add(arrList.get(i) + "/" + arrList.get(i + 1) + "/" + arrList.get(i + 2) + "/" + arrList.get(i + 3));
			}
		}

		if (arrList.size() > 4) {
			for (int i = 0; i < (arrList.size() - 4); i++) {
				segmentArray.add(arrList.get(i) + "/" + arrList.get(i + 1) + "/" + arrList.get(i + 2) + "/" + arrList.get(i + 3)
						+ "/" + arrList.get(i + 4));
			}
		}

		if (arrList.size() > 5) {
			for (int i = 0; i < (arrList.size() - 5); i++) {
				segmentArray.add(arrList.get(i) + "/" + arrList.get(i + 1) + "/" + arrList.get(i + 2) + "/" + arrList.get(i + 3)
						+ "/" + arrList.get(i + 4) + "/" + arrList.get(i + 5));
			}
		}
		return segmentArray;
	}

	/**
	 * Method to Check Empty or not null
	 * 
	 * @param str
	 *            the String need to be Checked
	 * @return boolean true false
	 */
	public static boolean isNotEmptyOrNull(String str) {
		return !((str == null) || str.trim().equals("") || str.trim().equals("All") || str.trim().equals("-1"));
	}

	/**
	 * Method to get converted local time/date as zulu time/date for the given station
	 * 
	 * @param airport
	 *            the airport Code
	 * @param localDate
	 *            the Local Date
	 * @return Date the Zulu Date
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static Date getZuluDateTime(String airport, Date localDate) throws ModuleException {

		// add local time details to flight
		AirportDST aptDST = ModuleServiceLocator.getAirportServiceBD().getEffectiveAirportDST(airport, localDate);
		int gmtOffset = getGMTOffset(airport);
		int dstOffset = (aptDST != null) ? aptDST.getDstAdjustTime() : 0;

		// calculate the local date
		Date zuluDate = CalendarUtil.getZuluDate(localDate, gmtOffset, dstOffset);

		AirportDST aptDSTZulu = ModuleServiceLocator.getAirportServiceBD().getEffectiveAirportDST(airport, zuluDate);

		if ((aptDSTZulu != null && aptDST != null && aptDSTZulu.getDstCode() != aptDST.getDstCode())
				|| (aptDSTZulu != null && aptDST == null) || (aptDSTZulu == null && aptDST != null))
			throw new ModuleException("airschedules.arg.invalid.cannot.calculate.zulutime");

		return zuluDate;
	}

	/**
	 * Method to get Offset for an Airport
	 * 
	 * @param airport
	 *            the Airport code
	 * @return int the offset
	 * @throws ModuleException
	 *             the ModuleException
	 */
	private static int getGMTOffset(String airport) throws ModuleException {

		Airport apt = ModuleServiceLocator.getAirportServiceBD().getAirport(airport);
		int gmtOffset = 0;
		if (apt != null) {

			gmtOffset = (apt.getGmtOffsetAction().equals("-")) ? (-1 * apt.getGmtOffsetHours()) : apt.getGmtOffsetHours();
		}

		return gmtOffset;
	}

	/**
	 * Method to get converted local time/date as zulu time/date for the given station
	 * 
	 * @param airport
	 *            the Airport
	 * @param effectiveDate
	 *            the the Effective Date
	 * @param localDate
	 *            the Local Date
	 * @return Date the Converted Zulu date
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static Date getZuluDateTimeAsEffective(String airport, Date effectiveDate, Date localDate) throws ModuleException {

		// add local time details to flight
		AirportDST aptDST = ModuleServiceLocator.getAirportServiceBD().getEffectiveAirportDST(airport, effectiveDate);
		int gmtOffset = getGMTOffset(airport);
		int dstOffset = (aptDST != null) ? aptDST.getDstAdjustTime() : 0;

		// calculate the local date
		Date zuluDate = CalendarUtil.getZuluDate(localDate, gmtOffset, dstOffset);

		return zuluDate;
	}

	/**
	 * Method to get the First leg of the given Schedule leg list
	 * 
	 * @param legList
	 *            the Set of Flight Schedule Legs
	 * @return FlightScheduleLeg the first Flight Schedule Leg
	 */
	public static FlightScheduleLeg getFirstScheduleLeg(Set<FlightScheduleLeg> legList) {

		Iterator<FlightScheduleLeg> it = legList.iterator();
		FlightScheduleLeg leg = null;

		while (it.hasNext()) {

			leg = (FlightScheduleLeg) it.next();
			if (leg.getLegNumber() == 1)
				break;
		}

		return leg;
	}

	/**
	 * Gets the Ovelapeed or Overlappable schedule List for a given Schedule
	 * 
	 * @param sched
	 *            the FlightSchedule
	 * @return String the uverlappable Schedule
	 * @throws ModuleException
	 *             the ModuleException
	 */
	public static String createOverlapableScheduleList(FlightSchedule sched) throws ModuleException {
		StringBuffer sb = new StringBuffer();
		if (sched.getOverlapingScheduleId() != null) {
			sb.append("<option value='" + sched.getOverlapingScheduleId() + "'>" + sched.getOverlapingScheduleId() + "</option>");
		} else {
			Object[] overlapScheduleArr = ModuleServiceLocator.getScheduleServiceBD().getPossibleOverlappingSchedules(sched)
					.toArray();
			for (int i = 0; i < overlapScheduleArr.length; i++) {
				FlightSchedule schedule = (FlightSchedule) overlapScheduleArr[i];
				sb.append("<option value='" + schedule.getScheduleId() + "'>" + schedule.getScheduleId() + "</option>");
			}
		}
		return sb.toString();
	}
	/**
	 * Get CodeShare Carrier & Flight set from a String
	 * 
	 * @param strArr
	 *            the Segment Array
	 * @return HashSet the CodeShareMCFlightSchedules
	 */
	public static HashSet<CodeShareMCFlightSchedule> getCodeShareMCFlight(String strArr) {
		HashSet<CodeShareMCFlightSchedule> CodeShareMCFlightSh =new HashSet<CodeShareMCFlightSchedule>();
		StringTokenizer st = new StringTokenizer(strArr, ",");
		try {
			while (st.hasMoreTokens()) {				
				String strToken = st.nextToken();
				String[] strValSegs = strToken.split("\\|");
				String carrier = strValSegs[0];
				String flight = strValSegs[1];
				if (strValSegs.length == 2 && !("").equals(strValSegs[1])){
					CodeShareMCFlightSchedule codeShareMCFlight = new CodeShareMCFlightSchedule();
					codeShareMCFlight.setCsMCCarrierCode(carrier);
					codeShareMCFlight.setCsMCFlightNumber(flight);
				CodeShareMCFlightSh.add(codeShareMCFlight);				
				}
			}
		} catch (Exception e) {
			log.error(e);
		}
		return CodeShareMCFlightSh;
	}

}
