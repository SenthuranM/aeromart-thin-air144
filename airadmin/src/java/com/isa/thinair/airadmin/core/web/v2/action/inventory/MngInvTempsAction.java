package com.isa.thinair.airadmin.core.web.v2.action.inventory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.config.Namespace;
import org.apache.struts2.config.Result;

import org.apache.struts2.json.JSONResult;
import com.isa.thinair.airadmin.core.service.ModuleServiceLocator;
import com.isa.thinair.airadmin.core.web.generator.inventory.InventoryTemplateHTMLGenerator;
import com.isa.thinair.airadmin.core.web.v2.constants.S2Constants;
import com.isa.thinair.airinventory.api.dto.inventory.InvTempCCAllocDTO;
import com.isa.thinair.airinventory.api.dto.inventory.InvTempCCBCAllocDTO;
import com.isa.thinair.airinventory.api.dto.inventory.InvTempDTO;
import com.isa.thinair.airinventory.api.dto.inventory.UpdatedInvTempDTO;
import com.isa.thinair.airinventory.api.model.LogicalCabinClass;
import com.isa.thinair.airinventory.api.util.AirInventoryModuleUtils;
import com.isa.thinair.commons.api.exception.ModuleException;
import com.isa.thinair.webplatform.api.base.BaseRequestAwareAction;
import com.isa.thinair.webplatform.core.commons.SelectListGenerator;

@Namespace(S2Constants.Namespace.PRIVATE_NAME_SPACE)
@Result(name = S2Constants.Result.SUCCESS, type = JSONResult.class, value = "")
public class MngInvTempsAction extends BaseRequestAwareAction {
	Log log = LogFactory.getLog(MngInvTempsAction.class);

	private static final String ADD_BC = "A";
	private static final String DELETE_BC = "D";
	private static final String EDIT_BC = "E";
	private static final String YES = "Y";

	// Initial load parameters
	private String errorList;
	private String airCraftList;
	private String airportCodes;

	// Functional parameters
	private String cabinClassCode;
	private String logicalCabinClassCode;
	private String alreadySplited;
	private ArrayList<InvTempCCBCAllocDTO> bcDetailsList;
	private ArrayList<InvTempCCBCAllocDTO> updatedBcAllocGridList;
	private InvTempCCBCAllocDTO invTempCCBCAllocDTO;
	private InvTempCCAllocDTO invTempCCAllocDTO;
	private InvTempDTO invTempDTO;
	private ArrayList<LogicalCabinClass> lccListForCabinClass;
	
	// success responses
	private boolean saveSuccess;
	private boolean duplicate;

	public String execute() {

		if (invTempDTO.getInvTempID() == null || invTempDTO.getInvTempID() == 0) {
			// Check for duplicate templates
			if (!validateDuplicateTemplate()) {
				// Newly Added inventory Template
				try {
					invTempDTO = ModuleServiceLocator.getInventoryTemplateBD().addInventoryTemplate(this.invTempDTO);
					if (invTempDTO.getInvTempID() != null && invTempDTO.getInvTempID() > 0) {
						setSaveSuccess(true);
					} else {
						setSaveSuccess(false);
					}
				} catch (ModuleException e) {
					log.error("Exception in MngInvTempsAction.execute()", e);
					setSaveSuccess(false);
				}
			} else {
				setSaveSuccess(false);
				setDuplicate(true);
			}
		} else {
			// Update Existing Template
			ArrayList<InvTempCCAllocDTO> recievedCabinClassList = invTempDTO.getInvTempCCAllocDTOList();
			Iterator<InvTempCCAllocDTO> itarator = recievedCabinClassList.iterator();
			while (itarator.hasNext()) {
				InvTempCCAllocDTO ccAllocDTO = itarator.next();
				UpdatedInvTempDTO updatedBCSet = new UpdatedInvTempDTO();
				if ((ccAllocDTO.getInvTempCCBCAllocDTOList() != null) && (!ccAllocDTO.getInvTempCCBCAllocDTOList().isEmpty())) {
					updatedBCSet.setInvTmpCabinAllocID(ccAllocDTO.getInvTempCabinAllocID());
					ArrayList<InvTempCCBCAllocDTO> bookingClassListOfCabinClass = ccAllocDTO.getInvTempCCBCAllocDTOList();
					bookingClassListOfCabinClass.removeAll(Collections.singleton(null));
					Iterator<InvTempCCBCAllocDTO> iteratorBC = bookingClassListOfCabinClass.iterator();
					while (iteratorBC.hasNext()) {
						InvTempCCBCAllocDTO bcAllocDTO = iteratorBC.next();
						if (bcAllocDTO != null) {
							if (bcAllocDTO.getUpdateFlag().equalsIgnoreCase(DELETE_BC)) {
								updatedBCSet.getDeletedInvTempBCAllocs().add(bcAllocDTO);
							} else if (bcAllocDTO.getUpdateFlag().equalsIgnoreCase(ADD_BC)) {
								updatedBCSet.getAddedInvTempBCAllocs().add(bcAllocDTO);
							} else if (bcAllocDTO.getUpdateFlag().equalsIgnoreCase(EDIT_BC)) {
								updatedBCSet.getEditedInvTempBCAllocs().add(bcAllocDTO);
							}
						}
					}
					try {
						ModuleServiceLocator.getInventoryTemplateBD().updateInvTemplate(invTempDTO, updatedBCSet);
						setSaveSuccess(true);
					} catch (ModuleException e) {
						log.error("Exception in MngInvTempsAction ==> execute()", e);
						setSaveSuccess(false);
					}
				} else {
					// only status changed.Update changed status.
					try {
						ModuleServiceLocator.getInventoryTemplateBD().updateInvTempStatus(invTempDTO);
						setSaveSuccess(true);
					} catch (ModuleException e) {
						log.error("Exception in MngInvTempsAction ==> execute()", e);
						setSaveSuccess(false);
					}
				}
			}
		}
		return S2Constants.Result.SUCCESS;
	}

	public String loadInitData() {
		try {
			setErrorList(InventoryTemplateHTMLGenerator.getInventoryTemplatesClientErrors());
			String airCraftModels = SelectListGenerator.createActiveAircraftModelList_SG();
			setAirCraftList(airCraftModels);
			String airportCodeList = SelectListGenerator.createActiveAirportCodeList();
			setAirportCodes(airportCodeList);
		} catch (Exception e) {
			log.error("ManageInventoryTemplatesAction ==> loadInitData()", e);
		}
		return S2Constants.Result.SUCCESS;
	}

	public String viewBCDetailsforInvTmp() {

		String tmpCCcode = getCabinClassCode();
		bcDetailsList = ModuleServiceLocator.getInventoryTemplateBD().getBCDetailsForInvTemp(
				tmpCCcode);
		return S2Constants.Result.SUCCESS;
	}

	public String loadBCAllocateGridData() {
		if (invTempDTO.getInvTempID() != null && invTempDTO.getInvTempID() > 0) {
			updatedBcAllocGridList = ModuleServiceLocator.getInventoryTemplateBD().loadGridBCallocDetails(invTempDTO,
					cabinClassCode, logicalCabinClassCode);
		}
		bcDetailsList = ModuleServiceLocator.getInventoryTemplateBD().getBCDetailsForInvTemp(cabinClassCode);
		return S2Constants.Result.SUCCESS;
	}

	public String searchAllLccForCabinClass() {
		lccListForCabinClass = AirInventoryModuleUtils.getInventoryTemplateDAO()
				.getLogicalCabinClasses(cabinClassCode);
		return S2Constants.Result.SUCCESS;
	}

	public String saveSplitCCAllocs() {

		if (invTempDTO.getInvTempID() == null || invTempDTO.getInvTempID() == 0) {
			// splitting of newly created template.
			try {
				invTempDTO = ModuleServiceLocator.getInventoryTemplateBD().addInventoryTemplate(this.invTempDTO);
				if (invTempDTO.getInvTempID() != null && invTempDTO.getInvTempID() > 0) {
					setSaveSuccess(true);
				} else {
					setSaveSuccess(false);
				}
			} catch (ModuleException e) {
				log.error("ManageInventoryTemplatesAction ==> saveSplitCCAllocs()", e);
				setSaveSuccess(false);
			}
		} else {
			// splitting of existing template.
			try {
				ModuleServiceLocator.getInventoryTemplateBD().saveSplitExistCcAllocs(invTempDTO);
				setSaveSuccess(true);
			} catch (ModuleException e) {
				log.error("ManageInventoryTemplatesAction ==> saveSplitCCAllocs()", e);
				setSaveSuccess(false);
			}
		}
		return S2Constants.Result.SUCCESS;
	}

	private boolean validateDuplicateTemplate() {
		boolean isDuplicateTemp = false;
		isDuplicateTemp = ModuleServiceLocator.getInventoryTemplateBD().validateDuplicateTemplate(invTempDTO.getAirCraftModel(),
				invTempDTO.getSegment());
		return isDuplicateTemp;
	}
	public String getErrorList() {
		return errorList;
	}
	public void setErrorList(String errorList) {
		this.errorList = errorList;
	}
	public String getAirCraftList() {
		return airCraftList;
	}
	public void setAirCraftList(String airCraftList) {
		this.airCraftList = airCraftList;
	}
	public String getAirportCodes() {
		return airportCodes;
	}
	public void setAirportCodes(String airportCodes) {
		this.airportCodes = airportCodes;
	}
	public InvTempCCBCAllocDTO getInvTempCCBCAllocDTO() {
		return invTempCCBCAllocDTO;
	}
	public void setInvTempCCBCAllocDTO(InvTempCCBCAllocDTO invTempCCBCAllocDTO) {
		this.invTempCCBCAllocDTO = invTempCCBCAllocDTO;
	}
	public InvTempCCAllocDTO getInvTempCCAllocDTO() {
		return invTempCCAllocDTO;
	}
	public void setInvTempCCAllocDTO(InvTempCCAllocDTO invTempCCAllocDTO) {
		this.invTempCCAllocDTO = invTempCCAllocDTO;
	}
	public InvTempDTO getInvTempDTO() {
		return invTempDTO;
	}
	public void setInvTempDTO(InvTempDTO invTempDTO) {
		this.invTempDTO = invTempDTO;
	}
	public String getCabinClassCode() {
		return cabinClassCode;
	}
	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}

	public ArrayList<InvTempCCBCAllocDTO> getBcDetailsList() {
		return bcDetailsList;
	}

	public void setBcDetailsList(ArrayList<InvTempCCBCAllocDTO> bcDetailsList) {
		this.bcDetailsList = bcDetailsList;
	}

	public ArrayList<InvTempCCBCAllocDTO> getUpdatedBcAllocGridList() {
		return updatedBcAllocGridList;
	}

	public void setUpdatedBcAllocGridList(ArrayList<InvTempCCBCAllocDTO> updatedBcAllocGridList) {
		this.updatedBcAllocGridList = updatedBcAllocGridList;
	}

	public boolean isSaveSuccess() {
		return saveSuccess;
	}

	public void setSaveSuccess(boolean saveSuccess) {
		this.saveSuccess = saveSuccess;
	}

	public ArrayList<LogicalCabinClass> getLccListForCabinClass() {
		return lccListForCabinClass;
	}

	public void setLccListForCabinClass(ArrayList<LogicalCabinClass> lccListForCabinClass) {
		this.lccListForCabinClass = lccListForCabinClass;
	}

	public String getLogicalCabinClassCode() {
		return logicalCabinClassCode;
	}

	public void setLogicalCabinClassCode(String logicalCabinClassCode) {
		this.logicalCabinClassCode = logicalCabinClassCode;
	}

	public String getAlreadySplited() {
		return alreadySplited;
	}

	public void setAlreadySplited(String alreadySplited) {
		this.alreadySplited = alreadySplited;
	}

	public boolean isDuplicate() {
		return duplicate;
	}

	public void setDuplicate(boolean duplicate) {
		this.duplicate = duplicate;
	}

}
