package com.isa.thinair.airadmin.api;

import java.io.Serializable;

/**
 * 
 * @author asiri
 * 
 */
public class OnholdTimeConfigsResultsTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String origin;

	private String destination;

	private String bookingDate;

	private String flightDepartureDate;

	private String ondCode;

	private String bookingClass;

	private String fltType;

	private String cabinClass;

	private String agentCode;

	private String moduleCode;

	private int releaseTime;

	private int startCutoverTime;

	private int endCutoverTime;

	private boolean isReleaseTimeFromBookingDate;

	private int rank;

	private int releaseTimeId;

	public String getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}

	public String getFlightDepartureDate() {
		return flightDepartureDate;
	}

	public void setFlightDepartureDate(String flightDepartureDate) {
		this.flightDepartureDate = flightDepartureDate;
	}

	public String getOndCode() {
		return ondCode;
	}

	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}

	public String getBookingClass() {
		return bookingClass;
	}

	public void setBookingClass(String bookingClass) {
		this.bookingClass = bookingClass;
	}

	public String getCabinClass() {
		return cabinClass;
	}

	public void setCabinClass(String cabinClass) {
		this.cabinClass = cabinClass;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(String moduleCode) {
		this.moduleCode = moduleCode;
	}

	public boolean isReleaseTimeFromBookingDate() {
		return isReleaseTimeFromBookingDate;
	}

	public void setReleaseTimeFromBookingDate(boolean isReleaseTimeFromBookingDate) {
		this.isReleaseTimeFromBookingDate = isReleaseTimeFromBookingDate;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public String getFltType() {
		return fltType;
	}

	public void setFltType(String fltType) {
		this.fltType = fltType;
	}

	public int getReleaseTimeId() {
		return releaseTimeId;
	}

	public void setReleaseTimeId(int releaseTimeId) {
		this.releaseTimeId = releaseTimeId;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public int getReleaseTime() {
		return releaseTime;
	}

	public void setReleaseTime(int releaseTime) {
		this.releaseTime = releaseTime;
	}

	public int getStartCutoverTime() {
		return startCutoverTime;
	}

	public void setStartCutoverTime(int startCutoverTime) {
		this.startCutoverTime = startCutoverTime;
	}

	public int getEndCutoverTime() {
		return endCutoverTime;
	}

	public void setEndCutoverTime(int endCutoverTime) {
		this.endCutoverTime = endCutoverTime;
	}

}
