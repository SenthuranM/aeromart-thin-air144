package com.isa.thinair.airadmin.api.flight;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FlightCabinClassDTO implements Serializable {

	private static final long serialVersionUID = 6667180356049663181L;

	private String cabinClassName;

	private List<FlightSegmentAllocationDTO> segmentAllocationDTOs;

	public String getCabinClassName() {
		return cabinClassName;
	}

	public void setCabinClassName(String cabinClassName) {
		this.cabinClassName = cabinClassName;
	}

	public List<FlightSegmentAllocationDTO> getSegmentAllocationDTOs() {
		return segmentAllocationDTOs;
	}

	public void addSegmentAllocationDTO(FlightSegmentAllocationDTO segmentAllocationDTO) {
		if (this.segmentAllocationDTOs == null)
			this.segmentAllocationDTOs = new ArrayList<FlightSegmentAllocationDTO>();
		segmentAllocationDTOs.add(segmentAllocationDTO);
	}

}
