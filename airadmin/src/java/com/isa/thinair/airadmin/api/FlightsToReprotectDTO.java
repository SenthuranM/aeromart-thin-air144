package com.isa.thinair.airadmin.api;

import java.util.HashSet;
import java.util.Set;

public class FlightsToReprotectDTO {
	String flightNumber = "";
	String departure = "";
	String arrival = "";
	String departureDateTime = "";
	String arrivalDateTime = "";
	String segmentAvailable = "";
	String segmentSold = "";
	String segmentOnhold = "";
	String flightId = "";
	String scheduleId = "";
	String modelNumber = "";

	String segmentCode = "";
	String segmentId = "";
	String cc = "";
	String cabinClasses = "";
	String segments = "";
	String segmentIds = "";
	String ondCode;
	Set<String> cabinClassSet = null;

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getDeparture() {
		return departure;
	}

	public void setDeparture(String departure) {
		this.departure = departure;
	}

	public String getArrival() {
		return arrival;
	}

	public void setArrival(String arrival) {
		this.arrival = arrival;
	}

	public String getDepartureDateTime() {
		return departureDateTime;
	}

	public void setDepartureDateTime(String departureDateTime) {
		this.departureDateTime = departureDateTime;
	}

	public String getArrivalDateTime() {
		return arrivalDateTime;
	}

	public void setArrivalDateTime(String arrivalDateTime) {
		this.arrivalDateTime = arrivalDateTime;
	}

	public String getSegmentAvailable() {
		return segmentAvailable;
	}

	public void setSegmentAvailable(String segmentAvailable) {
		this.segmentAvailable = segmentAvailable;
	}

	public String getSegmentSold() {
		return segmentSold;
	}

	public void setSegmentSold(String segmentSold) {
		this.segmentSold = segmentSold;
	}

	public String getSegmentOnhold() {
		return segmentOnhold;
	}

	public void setSegmentOnhold(String segmentOnhold) {
		this.segmentOnhold = segmentOnhold;
	}

	public String getFlightId() {
		return flightId;
	}

	public void setFlightId(String flightId) {
		this.flightId = flightId;
	}

	public String getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(String scheduleId) {
		this.scheduleId = scheduleId;
	}

	public String getModelNumber() {
		return modelNumber;
	}

	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public String getSegmentId() {
		return segmentId;
	}

	public void setSegmentId(String segmentId) {
		this.segmentId = segmentId;
	}

	public String getCc() {
		return cc;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	public String getCabinClasses() {
		return cabinClasses;
	}

	public void setCabinClasses(String cabinClasses) {
		this.cabinClasses = cabinClasses;
	}

	public String getSegments() {
		return segments;
	}

	public void setSegments(String segments) {
		this.segments = segments;
	}

	public String getSegmentIds() {
		return segmentIds;
	}

	public void setSegmentIds(String segmentIds) {
		this.segmentIds = segmentIds;
	}

	public String getOndCode() {
		return ondCode;
	}

	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}

	public Set<String> getCabinClassSet() {
		if (cabinClassSet == null) {
			cabinClassSet = new HashSet<String>();
		}
		return cabinClassSet;
	}

	public void setCabinClassSet(Set<String> cabinClassSet) {
		this.cabinClassSet = cabinClassSet;
	}

}
