package com.isa.thinair.airadmin.api;

public class FarePaxTypeConfigDTO {

	private String paxTypeCode;

	private String description;

	private long version;

	private String fareAmountType;

	private String fareAmount;

	private boolean applyFare;

	private boolean applyModChg;

	private boolean applyCnxCharge;

	private boolean fareRefundable;

	private boolean xbeBookingsAllowed;

	private int cutOffAgeInYears;

	private String noShowChargeAmount;

	public String getPaxTypeCode() {
		return paxTypeCode;
	}

	public void setPaxTypeCode(String paxTypeCode) {
		this.paxTypeCode = paxTypeCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public String getFareAmountType() {
		return fareAmountType;
	}

	public void setFareAmountType(String fareAmountType) {
		this.fareAmountType = fareAmountType;
	}

	public String getFareAmount() {
		return fareAmount;
	}

	public void setFareAmount(String fareAmount) {
		this.fareAmount = fareAmount;
	}

	public boolean isApplyFare() {
		return applyFare;
	}

	public void setApplyFare(boolean applyFare) {
		this.applyFare = applyFare;
	}

	public boolean isApplyModChg() {
		return applyModChg;
	}

	public void setApplyModChg(boolean applyModChg) {
		this.applyModChg = applyModChg;
	}

	public boolean isApplyCnxCharge() {
		return applyCnxCharge;
	}

	public void setApplyCnxCharge(boolean applyCnxCharge) {
		this.applyCnxCharge = applyCnxCharge;
	}

	public boolean isFareRefundable() {
		return fareRefundable;
	}

	public void setFareRefundable(boolean fareRefundable) {
		this.fareRefundable = fareRefundable;
	}

	public boolean isXbeBookingsAllowed() {
		return xbeBookingsAllowed;
	}

	public void setXbeBookingsAllowed(boolean xbeBookingsAllowed) {
		this.xbeBookingsAllowed = xbeBookingsAllowed;
	}

	public int getCutOffAgeInYears() {
		return cutOffAgeInYears;
	}

	public void setCutOffAgeInYears(int cutOffAgeInYears) {
		this.cutOffAgeInYears = cutOffAgeInYears;
	}

	public String getNoShowChargeAmount() {
		return noShowChargeAmount;
	}

	public void setNoShowChargeAmount(String noShowChargeAmount) {
		this.noShowChargeAmount = noShowChargeAmount;
	}

}
