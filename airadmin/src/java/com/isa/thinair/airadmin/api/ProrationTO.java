package com.isa.thinair.airadmin.api;

 

public class ProrationTO {
	
	private Integer prorationId;
	private String segmentCode;
	private int proration;
	private long version;	
	
 
	public Integer getProrationId() {
		return prorationId;
	}
	public void setProrationId(Integer prorationId) {
		this.prorationId = prorationId;
	}
	public String getSegmentCode() {
		return segmentCode;
	}
	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public int getProration() {
		return proration;
	}
	public void setProration(int proration) {
		this.proration = proration;
	}
	public long getVersion() {
		return version;
	}
	public void setVersion(long version) {
		this.version = version;
	}
 
	
	 
}
