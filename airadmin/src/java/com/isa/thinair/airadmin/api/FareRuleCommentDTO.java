package com.isa.thinair.airadmin.api;

public class FareRuleCommentDTO {

	private Integer fareRuleCommentID;

	private Integer fareRuleID;

	private String languageCode;

	private String fareRuleComments;

	public Integer getFareRuleCommentID() {
		return fareRuleCommentID;
	}

	public void setFareRuleCommentID(Integer fareRuleCommentID) {
		this.fareRuleCommentID = fareRuleCommentID;
	}

	public Integer getFareRuleID() {
		return fareRuleID;
	}

	public void setFareRuleID(Integer fareRuleID) {
		this.fareRuleID = fareRuleID;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public String getFareRuleComments() {
		return fareRuleComments;
	}

	public void setFareRuleComments(String fareRuleComments) {
		this.fareRuleComments = fareRuleComments;
	}
}
