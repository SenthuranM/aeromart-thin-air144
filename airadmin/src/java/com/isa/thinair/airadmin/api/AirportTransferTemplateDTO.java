package com.isa.thinair.airadmin.api;

import java.math.BigDecimal;

/**
 * 
 * @author manoj
 *
 */
public class AirportTransferTemplateDTO {
	
	private Integer airportTransferTemplateId;
	
	private String ondCode;
	
	private String cabinClassCode;
	
	private String flightNumber;
	
	private String modelNumber;
	
	private String departureDate;
	
	private BigDecimal adultAmount;
	
	private BigDecimal childAmount;
	
	private BigDecimal infantAmount;
	
	private String status;
	
	private String bookingClasses;
	
	private String bookingClassesIds;
	
	public Integer getAirportTransferTemplateId() {
		return airportTransferTemplateId;
	}
	
	public String getOndCode() {
		return ondCode;
	}
	
	public String getCabinClassCode() {
		return cabinClassCode;
	}
	
	public String getFlightNumber() {
		return flightNumber;
	}
	
	public String getModelNumber() {
		return modelNumber;
	}
	
	public String getDepartureDate() {
		return departureDate;
	}

	public BigDecimal getChildAmount() {
		return childAmount;
	}
	
	public BigDecimal getInfantAmount() {
		return infantAmount;
	}
	
	public void setAirportTransferTemplateId(Integer airportTransferTemplateId) {
		this.airportTransferTemplateId = airportTransferTemplateId;
	}
	
	public void setOndCode(String ondCode) {
		this.ondCode = ondCode;
	}
	
	public void setCabinClassCode(String cabinClassCode) {
		this.cabinClassCode = cabinClassCode;
	}
	
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}
	
	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}
	
	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}

	public void setChildAmount(BigDecimal childAmount) {
		this.childAmount = childAmount;
	}
	
	public void setInfantAmount(BigDecimal infantAmount) {
		this.infantAmount = infantAmount;
	}
	
	public BigDecimal getAdultAmount() {
		return adultAmount;
	}
	
	public void setAdultAmount(BigDecimal adultAmount) {
		this.adultAmount = adultAmount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getBookingClasses() {
		return bookingClasses;
	}

	public void setBookingClasses(String bookingClasses) {
		this.bookingClasses = bookingClasses;
	}

	public String getBookingClassesIds() {
		return bookingClassesIds;
	}

	public void setBookingClassesIds(String bookingClassesIds) {
		this.bookingClassesIds = bookingClassesIds;
	}
		
}
