package com.isa.thinair.airadmin.api.flight;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FlightSegmentAllocationDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private String flightNo;

	private String segmentCode;

	private int allocatedAdult;

	private int oversellAdult;

	private int curtailedAdult;

	private int allocatedInfant;

	private String soldAdultInfant;

	private String onholdAdultInfant;

	private String availableAdultInfant;

	private int overbookAdult;

	private String status;

	private List<FlightBookingClassDTO> flightBookingClassDTOs;

	private List<FlightSeatMvSummaryDTO> flightSeatMvSummaryDTOs;
	
	private List<FlightSeatMvSummaryDTO> flightSeatMvSummaryDTOsWithOwnerAgent;

	public String getFlightNo() {
		return flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public String getSegmentCode() {
		return segmentCode;
	}

	public void setSegmentCode(String segmentCode) {
		this.segmentCode = segmentCode;
	}

	public int getAllocatedAdult() {
		return allocatedAdult;
	}

	public void setAllocatedAdult(int allocatedAdult) {
		this.allocatedAdult = allocatedAdult;
	}

	public int getOversellAdult() {
		return oversellAdult;
	}

	public void setOversellAdult(int oversellAdult) {
		this.oversellAdult = oversellAdult;
	}

	public int getCurtailedAdult() {
		return curtailedAdult;
	}

	public void setCurtailedAdult(int curtailedAdult) {
		this.curtailedAdult = curtailedAdult;
	}

	public int getAllocatedInfant() {
		return allocatedInfant;
	}

	public void setAllocatedInfant(int allocatedInfant) {
		this.allocatedInfant = allocatedInfant;
	}

	public String getSoldAdultInfant() {
		return soldAdultInfant;
	}

	public void setSoldAdultInfant(String soldAdultInfant) {
		this.soldAdultInfant = soldAdultInfant;
	}

	public String getOnholdAdultInfant() {
		return onholdAdultInfant;
	}

	public void setOnholdAdultInfant(String onholdAdultInfant) {
		this.onholdAdultInfant = onholdAdultInfant;
	}

	public String getAvailableAdultInfant() {
		return availableAdultInfant;
	}

	public void setAvailableAdultInfant(String availableAdultInfant) {
		this.availableAdultInfant = availableAdultInfant;
	}

	public int getOverbookAdult() {
		return overbookAdult;
	}

	public void setOverbookAdult(int overbookAdult) {
		this.overbookAdult = overbookAdult;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<FlightBookingClassDTO> getFlightBookingClassDTOs() {
		return flightBookingClassDTOs;
	}

	public void addFlightBookingClassDTO(FlightBookingClassDTO flightBookingClassDTO) {
		if (this.flightBookingClassDTOs == null)
			this.flightBookingClassDTOs = new ArrayList<FlightBookingClassDTO>();
		flightBookingClassDTOs.add(flightBookingClassDTO);
	}

	public List<FlightSeatMvSummaryDTO> getFlightSeatMvSummaryDTOs() {
		return flightSeatMvSummaryDTOs;
	}

	public void addFlightSeatMvSummaryDTO(FlightSeatMvSummaryDTO flightSeatMvSummaryDTO) {
		if (this.flightSeatMvSummaryDTOs == null)
			this.flightSeatMvSummaryDTOs = new ArrayList<FlightSeatMvSummaryDTO>();
		this.flightSeatMvSummaryDTOs.add(flightSeatMvSummaryDTO);
	}
	
	public List<FlightSeatMvSummaryDTO> getFlightSeatMvSummaryDTOsWithOwnerAgent() {
		return flightSeatMvSummaryDTOsWithOwnerAgent;
	}
	
	public void addFlightSeatMvSummaryDTOWithOwnerAgent(FlightSeatMvSummaryDTO flightSeatMvSummaryDTO) {
		if (this.flightSeatMvSummaryDTOsWithOwnerAgent == null)
			this.flightSeatMvSummaryDTOsWithOwnerAgent = new ArrayList<FlightSeatMvSummaryDTO>();
		this.flightSeatMvSummaryDTOsWithOwnerAgent.add(flightSeatMvSummaryDTO);
	}

}
