package com.isa.thinair.airadmin.api;

import java.math.BigDecimal;

import com.isa.thinair.airpricing.api.model.FareRule;

/**
 * 
 * @author Jagath
 * 
 */
public class FareRuleFeeDTO {

	private Long fareRuleFeeId;

	private String chargeType;

	private String chargeAmountType;

	private BigDecimal chargeAmount;

	private BigDecimal minimumPerChargeAmt;

	private BigDecimal maximumPerChargeAmt;

	private String compareAgainst;

	private String timeType;

	private String timeValue;

	private String status;

	private FareRule fareRule;

	private BigDecimal chargeAmountInLocal = BigDecimal.ZERO;

	/**
	 * @return the fareRuleFeeId
	 */
	public Long getFareRuleFeeId() {
		return fareRuleFeeId;
	}

	/**
	 * @param fareRuleFeeId
	 *            the fareRuleFeeId to set
	 */
	public void setFareRuleFeeId(Long fareRuleFeeId) {
		this.fareRuleFeeId = fareRuleFeeId;
	}

	/**
	 * @return the chargeType
	 */
	public String getChargeType() {
		return chargeType;
	}

	/**
	 * @param chargeType
	 *            the chargeType to set
	 */
	public void setChargeType(String chargeType) {
		this.chargeType = chargeType;
	}

	/**
	 * @return the chargeAmountType
	 */
	public String getChargeAmountType() {
		return chargeAmountType;
	}

	/**
	 * @param chargeAmountType
	 *            the chargeAmountType to set
	 */
	public void setChargeAmountType(String chargeAmountType) {
		this.chargeAmountType = chargeAmountType;
	}

	/**
	 * @return the chargeAmount
	 */
	public BigDecimal getChargeAmount() {
		return chargeAmount;
	}

	/**
	 * @param chargeAmount
	 *            the chargeAmount to set
	 */
	public void setChargeAmount(BigDecimal chargeAmount) {
		this.chargeAmount = chargeAmount;
	}

	/**
	 * @return the minimumPerChargeAmt
	 */
	public BigDecimal getMinimumPerChargeAmt() {
		return minimumPerChargeAmt;
	}

	/**
	 * @param minimumPerChargeAmt
	 *            the minimumPerChargeAmt to set
	 */
	public void setMinimumPerChargeAmt(BigDecimal minimumPerChargeAmt) {
		this.minimumPerChargeAmt = minimumPerChargeAmt;
	}

	/**
	 * @return the maximumPerChargeAmt
	 */
	public BigDecimal getMaximumPerChargeAmt() {
		return maximumPerChargeAmt;
	}

	/**
	 * @param maximumPerChargeAmt
	 *            the maximumPerChargeAmt to set
	 */
	public void setMaximumPerChargeAmt(BigDecimal maximumPerChargeAmt) {
		this.maximumPerChargeAmt = maximumPerChargeAmt;
	}

	/**
	 * @return the compareAgainst
	 */
	public String getCompareAgainst() {
		return compareAgainst;
	}

	/**
	 * @param compareAgainst
	 *            the compareAgainst to set
	 */
	public void setCompareAgainst(String compareAgainst) {
		this.compareAgainst = compareAgainst;
	}

	/**
	 * @return the timeType
	 */
	public String getTimeType() {
		return timeType;
	}

	/**
	 * @param timeType
	 *            the timeType to set
	 */
	public void setTimeType(String timeType) {
		this.timeType = timeType;
	}

	/**
	 * @return the timeValue
	 */
	public String getTimeValue() {
		return timeValue;
	}

	/**
	 * @param timeValue
	 *            the timeValue to set
	 */
	public void setTimeValue(String timeValue) {
		this.timeValue = timeValue;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the fareRule
	 */
	public FareRule getFareRule() {
		return fareRule;
	}

	/**
	 * @param fareRule
	 *            the fareRule to set
	 */
	public void setFareRule(FareRule fareRule) {
		this.fareRule = fareRule;
	}

	/**
	 * @return the chargeAmountInLocal
	 */
	public BigDecimal getChargeAmountInLocal() {
		return chargeAmountInLocal;
	}

	/**
	 * @param chargeAmountInLocal
	 *            the chargeAmountInLocal to set
	 */
	public void setChargeAmountInLocal(BigDecimal chargeAmountInLocal) {
		this.chargeAmountInLocal = chargeAmountInLocal;
	}

}
