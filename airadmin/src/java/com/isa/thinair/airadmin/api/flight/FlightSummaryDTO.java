package com.isa.thinair.airadmin.api.flight;

import java.io.Serializable;

public class FlightSummaryDTO  implements Serializable {	
	private static final long serialVersionUID = -4374839382525851522L;
	
	private String flightNo;
	private String model;
	private String departure;
	private String arrival;
	private String defaultCapacity;
	
	public String getFlightNo() {
		return flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getDeparture() {
		return departure;
	}

	public void setDeparture(String departure) {
		this.departure = departure;
	}

	public String getArrival() {
		return arrival;
	}

	public void setArrival(String arrival) {
		this.arrival = arrival;
	}

	public String getDefaultCapacity() {
		return defaultCapacity;
	}

	public void setDefaultCapacity(String defaultCapacity) {
		this.defaultCapacity = defaultCapacity;
	}

}
