package com.isa.thinair.airadmin.api;

/**
 * FareDTO for the front end
 * to fill the front end pages
 * 
 */
import java.util.List;
import java.util.Map;

public class FareInfoDTO extends FareRuleInfoDTO {

	private String systemDate;

	private List<String[]> viaPointList;

	private List<String[]> bookingCodeList;

	private List<String> hubList;

	private List<Map<String, String>> fareBasisCodeList;

	private List<Map<String, String>> cosList;

	private List<Map<String, String>> posList;

	private List<FarePaxTypeConfigDTO> farePaxConfig;

	private double modCharge;

	private double cnxCharge;

	private String bufferTime;

	private long bufTimeInMilSec;

	private long domBufferTimeInMilSec;

	private boolean OpenRTEnabled;

	private boolean isEnableSameFareChecking;

	private boolean isEnableLinkFare;

	private String defaultSelectedCos;

	private boolean enableFareRuleNCC;

	public String getSystemDate() {
		return systemDate;
	}

	public void setSystemDate(String systemDate) {
		this.systemDate = systemDate;
	}

	public List<String[]> getViaPointList() {
		return viaPointList;
	}

	public void setViaPointList(List<String[]> viaPointList) {
		this.viaPointList = viaPointList;
	}

	public List<String[]> getBookingCodeList() {
		return bookingCodeList;
	}

	public void setBookingCodeList(List<String[]> bookingCodeList) {
		this.bookingCodeList = bookingCodeList;
	}

	public double getModCharge() {
		return modCharge;
	}

	public void setModCharge(double modCharge) {
		this.modCharge = modCharge;
	}

	public double getCnxCharge() {
		return cnxCharge;
	}

	public void setCnxCharge(double cnxCharge) {
		this.cnxCharge = cnxCharge;
	}

	public String getBufferTime() {
		return bufferTime;
	}

	public void setBufferTime(String bufferTime) {
		this.bufferTime = bufferTime;
	}

	public long getBufTimeInMilSec() {
		return bufTimeInMilSec;
	}

	public void setBufTimeInMilSec(long bufTimeInMilSec) {
		this.bufTimeInMilSec = bufTimeInMilSec;
	}

	public long getDomBufferTimeInMilSec() {
		return domBufferTimeInMilSec;
	}

	public void setDomBufferTimeInMilSec(long domBufferTimeInMilSec) {
		this.domBufferTimeInMilSec = domBufferTimeInMilSec;
	}

	public boolean isOpenRTEnabled() {
		return OpenRTEnabled;
	}

	public void setOpenRTEnabled(boolean openRTEnabled) {
		OpenRTEnabled = openRTEnabled;
	}

	public List<String> getHubList() {
		return hubList;
	}

	public void setHubList(List<String> hubList) {
		this.hubList = hubList;
	}

	public List<FarePaxTypeConfigDTO> getFarePaxConfig() {
		return farePaxConfig;
	}

	public void setFarePaxConfig(List<FarePaxTypeConfigDTO> farePaxConfig) {
		this.farePaxConfig = farePaxConfig;
	}

	public List<Map<String, String>> getFareBasisCodeList() {
		return fareBasisCodeList;
	}

	public void setFareBasisCodeList(List<Map<String, String>> fareBasisCodeList) {
		this.fareBasisCodeList = fareBasisCodeList;
	}

	public List<Map<String, String>> getCosList() {
		return cosList;
	}

	public void setCosList(List<Map<String, String>> cosList) {
		this.cosList = cosList;
	}

	public List<Map<String, String>> getPosList() {
		return posList;
	}

	public void setPosList(List<Map<String, String>> posList) {
		this.posList = posList;
	}

	public boolean isEnableSameFareChecking() {
		return isEnableSameFareChecking;
	}

	public void setEnableSameFareChecking(boolean isEnableSameFareChecking) {
		this.isEnableSameFareChecking = isEnableSameFareChecking;
	}

	public boolean isEnableLinkFare() {
		return isEnableLinkFare;
	}

	public void setEnableLinkFare(boolean isEnableLinkFare) {
		this.isEnableLinkFare = isEnableLinkFare;
	}

	public String getDefaultSelectedCos() {
		return defaultSelectedCos;
	}

	public void setDefaultSelectedCos(String defaultSelectedCos) {
		this.defaultSelectedCos = defaultSelectedCos;
	}

	public boolean isEnableFareRuleNCC() {
		return enableFareRuleNCC;
	}

	public void setEnableFareRuleNCC(boolean enableFareRuleNCC) {
		this.enableFareRuleNCC = enableFareRuleNCC;
	}

}
