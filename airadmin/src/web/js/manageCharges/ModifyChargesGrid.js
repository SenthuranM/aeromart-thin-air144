var objModCol1 = new DGColumn();
objModCol1.columnType = "CUSTOM";
objModCol1.width = "10%";
objModCol1.arrayIndex = 1;
objModCol1.toolTip = "Effective From";
objModCol1.headerText = "Effect From";
objModCol1.ID = 'txtEffectiveFrom';
objModCol1.htmlTag = "<input type='text' id='txtEffectiveFrom' name='txtEffectiveFrom' :CUSTOM: size='13' maxlength='16' style='text-align:righ;'  onBlur='parent.checkChargeRateDateTime(:ROW:,1, this)' invalidText='true'>";
objModCol1.itemAlign = "center"
objModCol1.sort = "true";

var objModCol2 = new DGColumn();
objModCol2.columnType = "CUSTOM";
objModCol2.width = "10%";
objModCol2.arrayIndex = 2;
objModCol2.toolTip = "Effective To";
objModCol2.headerText = "Effect To";
objModCol2.ID = 'txtEffectiveTo';
objModCol2.htmlTag = "<input type='text' id='txtEffectiveTo' name='txtEffectiveTo' :CUSTOM: size='13' maxlength='16' onBlur='parent.checkChargeRateDateTime(:ROW:,2, this)' invalidText='true' >";
objModCol2.itemAlign = "center";

var objModCol3 = new DGColumn();
objModCol3.columnType = "CUSTOM";
objModCol3.width = "5%";
objModCol3.arrayIndex = 3;
objModCol3.toolTip = "Flag V=Value, PF=Percentage from Fare, PTF = Percentage from total fare";
objModCol3.headerText = "Flag";
objModCol3.ID = 'selPer';
objModCol3.htmlTag = "<select  id='selPer' name='selPer' style='width:54px' onchange = 'parent.CheckPercentage(:ROW:, this)':CUSTOM:><option value='V'>V</option><option value='PF'>PF</option><option value='PTF'>PTF</option><option value='PFS'>PFS</option><option value='PTFS'>PTFS</option><option value='PTFST'>PTFST</option></select>";
objModCol3.itemAlign = "center";

var objModCol4 = new DGColumn();
objModCol4.columnType = "CUSTOM";
objModCol4.width = "4%";
objModCol4.arrayIndex = 4;
objModCol4.toolTip = "Value";
objModCol4.headerText = "Value %";
objModCol4.ID = 'txtValPerc';
objModCol4.htmlTag = "<input type='text' id='txtValPerc' name='txtValPerc' size='3' onKeyUp='parent.KPValidateDecimel(this,5,2)' onKeyPress='parent.KPValidateDecimel(this,5,2)' class='rightText' :CUSTOM: >";
objModCol4.itemAlign = "center";

var objModCol5 = new DGColumn();
objModCol5.columnType = "CUSTOM";
objModCol5.width = "5%";
objModCol5.arrayIndex = 5;
objModCol5.toolTip = "Minimum Value";
objModCol5.headerText = "Min Value";
objModCol5.ID = 'txtMinVal';
objModCol5.htmlTag = "<input type='text' id='txtMinVal' name='txtMinVal' size='4' onKeyUp='parent.KPValidateDecimel(this,5,2)' onKeyPress='parent.KPValidateDecimel(this,5,2)' class='rightText' :CUSTOM: >";
objModCol5.itemAlign = "center";

var objModCol6 = new DGColumn();
objModCol6.columnType = "CUSTOM";
objModCol6.width = "5%";
objModCol6.arrayIndex = 6;
objModCol6.toolTip = "Maximum Value";
objModCol6.headerText = "Max Value";
objModCol6.ID = 'txtMaxVal';
objModCol6.htmlTag = "<input type='text' id='txtMaxVal' name='txtMaxVal' size='4' onKeyUp='parent.KPValidateDecimel(this,5,2)' onKeyPress='parent.KPValidateDecimel(this,5,2)' class='rightText' :CUSTOM: >";
objModCol6.itemAlign = "center";

var objModCol7 = new DGColumn();
objModCol7.columnType = "CUSTOM";
objModCol7.width = "5%";
objModCol7.arrayIndex = 7;
objModCol7.toolTip = "Local Currency";
objModCol7.headerText = "Loc. Curr.";
objModCol7.ID = 'txtLocalCurr';
objModCol7.htmlTag = "<input type='text' id='txtLocalCurr' name='txtLocalCurr'  size='4' onKeyUp='parent.KPValidateDecimel(this,10,2)' onKeyPress='parent.KPValidateDecimel(this,10,2)' class='rightText'  :CUSTOM: >";
objModCol7.itemAlign = "center";

var objModCol8 = new DGColumn();
objModCol8.columnType = "CHECKBOX";
objModCol8.width = "4%";
objModCol8.arrayIndex = 8;
objModCol8.toolTip = "Active";
objModCol8.headerText = "Act";
objModCol8.itemAlign = "center";

var objModCol9 = new DGColumn();
objModCol9.columnType = "CUSTOM";
objModCol9.width = "7%";
objModCol9.arrayIndex = 9;
objModCol9.toolTip = "Operation Type ANY=Any, MAKE=Make Value, MODIFY=Modify Value";
objModCol9.headerText = "Ope. type";
objModCol9.ID = 'OperationType';
objModCol9.htmlTag = "<select  id='OperationType' name='OperationType' style='width:70px; onchange = 'parent.setRateType(:ROW:, this)':CUSTOM:><option value='0'>ANY</option><option value='1'>MAKE</option><option value='2'>MODIFY</option></select>";
objModCol9.itemAlign = "center";

var objModCol10 = new DGColumn();
objModCol10.columnType = "CUSTOM";
objModCol10.width = "8%";
objModCol10.arrayIndex = 10;
objModCol10.toolTip = "Journey Type Any=Any, ONEWAY=Oneway, RETURN=Return";
objModCol10.headerText = "Jny. Type";
objModCol10.ID = 'journeyType';
objModCol10.htmlTag = "<select  id='journeyType' name='journeyType' style='width:80px; onchange = 'parent.setJourneyType(:ROW:, this)':CUSTOM:><option value='0'>ANY</option><option value='1'>ONEWAY</option><option value='2'>RETURN</option></select>";
objModCol10.itemAlign = "center";

var objModCol11 = new DGColumn();
objModCol11.columnType = "CUSTOM";
objModCol11.width = "5%";
objModCol11.arrayIndex = 14;
objModCol11.toolTip = "Cabin Class Code";
objModCol11.headerText = "CC";
objModCol11.ID = 'cabinClassCode';
objModCol11.htmlTag = getCabinClassCodeHtml();
objModCol11.itemAlign = "center";

var objModCol12 = new DGColumn();
objModCol12.columnType = "CUSTOM";
objModCol12.width = "5%";
objModCol12.arrayIndex = 15;
objModCol12.toolTip = "Break Point";
objModCol12.headerText = "Break Point";
objModCol12.ID = 'txtBreakPoint';
objModCol12.htmlTag = "<input type='text' id='txtBreakPoint' name='txtBreakPoint' size='5' onKeyUp='parent.KPValidateDecimel(this,5,2)' onKeyPress='parent.KPValidateDecimel(this,5,2)' class='rightText' :CUSTOM: >";
objModCol12.itemAlign = "center";

var objModCol13 = new DGColumn();
objModCol13.columnType = "CUSTOM";
objModCol13.width = "5%";
objModCol13.arrayIndex = 16;
objModCol13.toolTip = "Boundry Value";
objModCol13.headerText = "Boundry Value";
objModCol13.ID = 'txtBoundryValue';
objModCol13.htmlTag = "<input type='text' id='txtBoundryValue' name='txtBoundryValue' size='5' onKeyUp='parent.KPValidateDecimel(this,5,2)' onKeyPress='parent.KPValidateDecimel(this,5,2)' class='rightText' :CUSTOM: >";
objModCol13.itemAlign = "center";

var objModCol14 = new DGColumn();
objModCol14.columnType = "CUSTOM";
objModCol14.width = "10%";
objModCol14.arrayIndex = 17;
objModCol14.toolTip = "Sales From";
objModCol14.headerText = "Sales From";
objModCol14.ID = 'txtTravelFrom';
objModCol14.htmlTag = "<input type='text' id='txtTravelFrom' name='txtTravelFrom' :CUSTOM: size='13' maxlength='16' style='text-align:righ;'  onBlur='parent.checkChargeRateDateTime(:ROW:,17, this)' invalidText='true'>";
objModCol14.itemAlign = "center"

var objModCol15 = new DGColumn();
objModCol15.columnType = "CUSTOM";
objModCol15.width = "10%";
objModCol15.arrayIndex = 18;
objModCol15.toolTip = "Sales To";
objModCol15.headerText = "Sales To";
objModCol15.ID = 'txtTravelTo';
objModCol15.htmlTag = "<input type='text' id='txtTravelTo' name='txtTravelTo' :CUSTOM: size='13' maxlength='16' style='text-align:righ;'  onBlur='parent.checkChargeRateDateTime(:ROW:,18, this)' invalidText='true'>";
objModCol15.itemAlign = "center"


// ---------------- Grid
var objDGMOD = new DataGrid("spnTax");
objDGMOD.addColumn(objModCol1);
objDGMOD.addColumn(objModCol2);
objDGMOD.addColumn(objModCol14);
objDGMOD.addColumn(objModCol15);
objDGMOD.addColumn(objModCol3);
objDGMOD.addColumn(objModCol4);
objDGMOD.addColumn(objModCol5);
objDGMOD.addColumn(objModCol6);
objDGMOD.addColumn(objModCol7);
objDGMOD.addColumn(objModCol8);
objDGMOD.addColumn(objModCol9);
objDGMOD.addColumn(objModCol10);
objDGMOD.addColumn(objModCol11);
objDGMOD.addColumn(objModCol12);
objDGMOD.addColumn(objModCol13);


objDGMOD.width = "1124px";
objDGMOD.height = "100px";
objDGMOD.headerBold = false;
objDGMOD.rowSelect = true;
objDGMOD.arrGridData = arrData;
objDGMOD.seqNo = true;
objDGMOD.seqNoWidth = "2%";
objDGMOD.displayGrid();

function getCabinClassCodeHtml(){
	var cabinClassCodeHtml = "<select  id='cabinClassCode' name='cabinClassCode' style='width:50px; " +
			"onchange = 'parent.setCabinClassCode(:ROW:, this)':CUSTOM:><option value='-1'>ANY</option>";
	for(var i=0 ; i<cabinClassCodes.length; i++){
		cabinClassCodeHtml += "<option value='"+cabinClassCodes[i]+"'>"+cabinClassCodes[i]+"</option>";
	}
	cabinClassCodeHtml += "</select>";
	return cabinClassCodeHtml;
}

function addDataRow() {
	var x = arrData.length;

	arrData[x] = new Array();
	arrData[x][0] = x;
	arrData[x][1] = "";
	arrData[x][2] = "";
	arrData[x][3] = "V";
	arrData[x][4] = "";
	arrData[x][5] = "";
	arrData[x][6] = "true";
	arrData[x][7] = "";
	arrData[x][8] = "";
	arrData[x][9] = "0";
	arrData[x][10] = "0";
	arrData[x][14] = "0";
	arrData[x][15] = "";
	arrData[x][16] = "";
	arrData[x][17] = "";
	arrData[x][18] = "";
	objDGMOD.refresh();
}

function gridLoadCheck() {
	if (objDGMOD.loaded) {
		clearTimeout(objTempTimer);
		objDGMOD.setDisable("", "", true);
	}
}

function checkDate(intRowIndex, intColNo, control) {
	if (control.value != "") {
		var val = dateChk(control.value);
		if (val == "") {
			showERRMessage(formatIncorrect);
			control.focus();
		} else {
			control.value = val;
			arrData[intRowIndex][intColNo] = val;
		}

	}
}

function KPValidateDecimel(objCon, s, f) {
	setPageEdited(true);
	var strText = objCon.value;
	var blnVal = isLikeDecimal(strText);
	var blnVal2 = currencyValidate(strText, s, f);

	if (!blnVal) {
		customValidate(objCon, /[a-zA-Z`!@#$%\s&*?\[\]{}()|\\\/=:,;_^~<>]/g);
	} else if (!blnVal2) {
		customValidate(objCon, /[a-zA-Z`!@#$%\s&*?\[\]{}()|\\\/=:,;_^~<>]/g);
		objCon.focus();
	}

}

function CheckPercentage(row, objsel) {
	var strBaseNewVal = objsel.value;
	arrData[row][3] = strBaseNewVal;
	objDGMOD.setCellValue(row, 4, strBaseNewVal);	
	if(strBaseNewVal != 'V'){
		disableMinMaxColumns(row,false);
	} else {
		disableMinMaxColumns(row,true);
	}
}

function setRateType(row, objsel) {
	var strOperationNewVal = objsel.value;

	arrData[row][9] = strOperationNewVal;
	objDGMOD.setCellValue(row, 10, strOperationNewVal);
}

function setJourneyType(row, objsel) {
	var strJourneyNewVal = objsel.value;

	arrData[row][10] = strJourneyNewVal;
	objDGMOD.setCellValue(row, 11, strJourneyNewVal);
}

function setCabinClassCode(row, objsel) {
	var strCabinClassCodeVal = objsel.value;

	arrData[row][11] = strCabinClassCodeVal;
	objDGMOD.setCellValue(row, 12, strCabinClassCodeVal);
}

var objTempTimer = setInterval("gridLoadCheck()", 300)