var rowData = "";
var noOfCharges = 0;
var rowsDisabled = 0;
var flagRowclick = "";
var display = "";
var addClick = false;
var editClicked = false;
var ScreenId = "SC_ADMN_010";
var isPOS = false;
var isOND = false;
var chargeRateOperationTypeDisable = true ;
var chargeRateJourneytTypeDisable = true ;
var checkedBundleFare =  false;
var rowClickEnable = false;
var selectedAgents = "";
var isAgentsSelected = false;
var applicableCharges ="";

function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(ScreenId, isEdited);
}

function winOnLoad(strMsg, strMsgType) {
 isPOS = true;
 isOND = true;
	setPageEdited(false);
	// createPopUps();
	// createPopUpsAgt();
	
	if(!isAllowAssCookieCharges){
		var select=document.getElementById('selCategory');
		for (i=0;i<select.length;  i++) {
		   if (select.options[i].value=='COOKIE_BASED') {
		     select.remove(i);
		     break;
		   }
		}
	}
	PopUpData();
	setCurrentSystemTimeAndDate();
	setTimeout('loadList()', 2000);
	 
	enableCookieRelatedFields(false);
	showServiceTaxFields(false);
	disableServiceTaxFields(true);
	// setVisible("spnRevenue", false);
	Disable("chkRevenue", true);
	Disable("txtRevenue", true);
	Disable("chkRefundable", "");
	setField("chkRefundable", true);
	Disable("chkRefundable", "false");
	setField("chkRefundableOnlyMOD", false);
	Disable("chkRefundableOnlyMOD", "false");
	Disable("chkRefundableWhenExchange", "true");
	setField("chkRefundableWhenExchange", false);
	if(enableExcludeChargesFromFQ == "true"){
		setField("chkExcludeFromFareQuote", false);
		Disable("chkExcludeFromFareQuote", "false");
	}	
	Disable("selApplyTo", "");
	Disable("selOndSeg", "");
	Disable("selChannel", "");
	Disable("chkStatus", "");
	setField("chkStatus", true);
	Disable("chkStatus", "false");
	Disable("radDept1", "false");
	Disable("radRptChgCode", "false");
	Disable("radNewRptCHCode", "false");
	if (top[1].objTMenu.focusTab == ScreenId) {
		getFieldByID("selChargeCode").focus();
	}
	
	clearControls();
	setPageEdited(false);
	setField("radRateCat", "WithDateRange");
	setWithoutCharges();
	categoryClick();
	checkAirportTax();
	Disable("btnGEDit", "true");
	Disable("btnDelete", "true");
	Disable("btnEdit", "true");
	Disable("btnGAdd", "true");
	disableInputControls("false");
	loadDataOnSearch();
	isPOS = false;
	isOND = false;
	
	setVisible("radCookieBased", false);
	setVisible("lblCookieBased", false);
	
	if (showFareDefByDepCountryCurrency == "Y") {
		document.getElementById('divCurrSelect').style.display = 'block';
	} else {
		document.getElementById('divCurrSelect').style.display = 'none';
	}
	Disable("radCurr", "true");
	Disable("radSelCurr", "true");
	currencyClick();

	if(isBundledFaresEnabled){
		setVisible('trBundledFare', true);	
	} else {
		setVisible('trBundledFare', false);
	}
	if (isSuccessfulSaveMessageDisplayEnabled) {
		if (isSaveTransactionSuccessful) {
			alert("Record Successfully saved!");
		}
	}
	$('#selCountry').change(function() {
		var showState = false;
		$('#selState').find('option').remove().end();
		if($('#selCountry').val() != undefined && $('#selCountry').val() != ""){
			var selectedCountry = $('#selCountry').val();
			if(countryStateArry[selectedCountry] != undefined ){
				showState = true;
				$( 'select[name="selState"]' ).append('<option value=""></option>');
				$( 'select[name="selState"]' ).append(countryStateArry[selectedCountry]);
			}
			populateInvoiceMapping();
		}
		
		if(showState){
			setVisible("rowSelState", true);
			setVisible("rowInterState", true);
		} else {
			setVisible("rowSelState", false);
			setVisible("rowInterState", false);
		}
	});
	

 try{
		if (strMsg != null && strMsgType != null && strMsg != "") {
			showCommonError(strMsgType, strMsg);
			if (model != null && model.length > 0) {
				disableInputControls("");
				if (model[0] != "null") {
					setField("txtChargeCode", model[0]);
				} else {
					setField("txtChargeCode", model[11]);
				}
	
				setField("AddSegselGroup", model[2]);
				setField("txtDesc", model[3]);
				if (model[4] == "on") {
					setField("chkRefundable", true);
				} else {
					setField("chkRefundable", false);
				}
				if (model[5] == "on") {
					setField("chkStatus", true);
				} else {
					setField("chkStatus", false);
				}
	
				if (model[6] != "") {
					setField("selApplyTo", model[6]);
				}
	
				setField("radCat1", model[7]);
				categoryClick();
				if (model[8] != "null") {
					// set Multiselect here
				}
	
				if (model[9] != "" && model[9] != null) {
					var arrDt = model[9].split(",");
					for ( var r = 0; r < arrDt.length - 1; r++) {
						var strVal = arrDt[r];
						var strReplace = arrDt[r];
						if (strVal.indexOf("***") != -1) {
							strReplace = strVal.replace(/\*\*\*/g, "All");
						}
						addToList(strReplace, r);
					}
				}
	
				Disable("btnGAdd", "");
				Disable("btnGEDit", "");
	
				if (model[12] != null && model[12] != ""
						&& model[12] != "undefined") {
					setField("hdnRateIDs", model[12]);
				}
				if (model[13] != null && model[13] != ""
						&& model[13] != "undefined") {
					setField("hdnRateVersion", model[13]);
				}
				if (model[14] != null && model[14] != ""
						&& model[14] != "undefined") {
					setField("hdnVersion", model[14]);
					Disable("txtChargeCode", "true");
					getFieldByID("txtDesc").focus();
	
				} else {
					getFieldByID("txtChargeCode").focus();
	
				}
	
				var rateData = model[10];
				setField("radRptChgCode", model[15]);
				setField("selReportingGroup", model[16]);
				if (model[19] != null && model[19] != ""
						&& model[19] != "undefined") {
					setField("selJourney", model[19]);
					setField("hdnJourney", model[19]);
				}
				// JIRA-6929
				if(model[25] != null && model[25] != ""){
					setField("txtRptChargeGroupCode", model[25]);
				}
				if(model[26] != null && model[26] != ""){
					setField("txtRptChargeGroupDesc", model[26]);				
				}
				if(model[27] != null && model[27] != ""){
					setField("hdnRptChargeGroupType", model[27]);
					setField("radRptChgCode", model[27]);
				}
				
				if (rateData != "" && rateData != null && rateData != "undefined") {
					arrData.length = 0;
					var rateArr = rateData.split(";");
					for ( var i = 0; i < (rateArr.length - 1); i++) {
						var rateArrayData = rateArr[i].split(",");
						arrData[i] = new Array();
						arrData[i][0] = "'" + i + "'";
						arrData[i][1] = rateArrayData[0];
						arrData[i][2] = rateArrayData[1];						
						arrData[i][3] = rateArrayData[8];
						arrData[i][4] = rateArrayData[3];
						arrData[i][5] = rateArrayData[6];
						arrData[i][6] = rateArrayData[7];
						arrData[i][7] = rateArrayData[4];
						arrData[i][8] = rateArrayData[5];
						arrData[i][9] = rateArrayData[9];
						arrData[i][10] = rateArrayData[10];
						arrData[i][11] = "Added";
						arrData[i][12] = "";
						arrData[i][13] = i;
						arrData[i][14] =rateArrayData[11];
						arrData[i][15] =rateArrayData[12];
						arrData[i][16] =rateArrayData[13];
						arrData[i][17] =rateArrayData[14];
						arrData[i][18] =rateArrayData[15];
						arrData[i][19] = ""; 
	
					}
				}
				objDGMOD.arrGridData = arrData;
				objDGMOD.refresh();
			}
			top[2].ShowPageMessage();
		} else {
			top[2].HidePageMessage();
		}
 }catch(e){ alert("System Loading Please wait") }

} // end winload

function closeClick() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(ScreenId))) {
		setPageEdited(false);
		top[1].objTMenu.tabRemove(ScreenId);
	}
}

function replaceInvalids(control) {
	setPageEdited(true);
	var strCC = getText(control);
	var blnVal = isPositiveInt(strCC);
	var strLen = strCC.length;
	var val = removeInvalids(strCC);
	setField(control, val);
}

function KPValidate(control) {
	// setPageEdited(true);
	var strCC = getText(control);
	var strLen = strCC.length;
	var blnVal = isAlpha(strCC)
	if (blnVal) {
		setField(control, strCC.substr(0, strLen - 1));
		getFieldByID(control).focus();
	}

}
function kpAlphaNumeric(control) {
	setPageEdited(true);
	var strCC = getText(control);
	var strLen = strCC.length;
	var blnVal = isAlphaNumeric(strCC);
	if (!blnVal) {
		setField(control, strCC.substr(0, strLen - 1));
		getFieldByID(control).focus();
	}
}

function kpAlphaNumericWhiteSpace(control) {
	setPageEdited(true);
	var strCC = getText(control);
	var strLen = strCC.length;
	var blnVal = isAlphaNumericWhiteSpace(strCC);
	if (!blnVal) {
		setField(control, strCC.substr(0, strLen - 1));
		getFieldByID(control).focus();
	}
}
// To Handle Paging
function gridNavigations(intRecNo, intErrMsg) {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(ScreenId))) {
		setField("hdnMode", "PAGING");
		var strSearch = getSearchData();
		top[1].objTMenu.tabSetValue(ScreenId, strSearch + "~" + intRecNo);
		setField("hdnRecNo", intRecNo);
		if (intErrMsg != "") {
			top[2].objMsg.MessageText = "<li> " + intErrMsg;
			top[2].objMsg.MessageType = "Error";
			top[2].ShowPageMessage();
		} else {
			objDG.seqStartNo = intRecNo;
			document.forms[0].submit();
			top[2].ShowProgress();
		}
		setPageEdited(false);

	}
}

function loadDataOnSearch() {
	var strSearch = top[1].objTMenu.tabGetValue(ScreenId);
	if (strSearch != "") {
		if (isDelete != true) {
			var rarFields = strSearch.split(",");
			setField("selChargeCode", rarFields[0]);
			setField("selGroup", rarFields[1]);
			setField("selCategory", rarFields[2]);
			setField("radRateCat", rarFields[3]);
			setWithoutCharges();

			if (rarFields[4] != "NoData" && rarFields[4] != "null") {

				setField("txtDateTo", rarFields[4]);
			}

			if (rarFields[5] != "NoData" && rarFields[5] != "null") {

				setField("txtDateFrom", rarFields[5]);
			}
			setField("selStatus",rarFields[6]);
		}
	}
}

function getSearchData() {
	var strCC = getText("selChargeCode");
	var strGroup = getText("selGroup");
	var selCat = getText("selCategory");
	var status = getValue("selStatus");
	var strToDate = getText("txtDateTo");
	var strFeromDate = getText("txtDateFrom");
	var searchStr = strCC + "," + strGroup + "," + selCat + ","
			+ getText("radRateCat") + ",";

	if (strToDate == "") {
		searchStr += "NoData,";
	} else {
		searchStr += strToDate + ",";
	}

	if (strFeromDate == "") {
		searchStr += "NoData,";
	} else {
		searchStr += strFeromDate + ",";
	}
	searchStr += status + ","; 
	
	return searchStr;
}

function searchData() {
	var validated = false;
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(ScreenId))) {
		setPageEdited(false);
		var strCC = getText("selChargeCode");
		var strGroup = getText("selGroup");
		var selCat = getText("selCategory");
		var status = getText("selStatus");
		var strToDate = getText("txtDateTo");
		var strFeromDate = getText("txtDateFrom");
		var toDate = new Date(strToDate);
		var fromDate = new Date(strToDate);
		if (strCC == "") {
			showERRMessage(chargecodeRqrd);
			getFieldByID("selChargeCode").focus();

		} else if (strGroup == "") {
			showERRMessage(groupRqrd);
			getFieldByID("selGroup").focus();

		} else if (selCat == "") {
			showERRMessage(categoryRqrd);
			getFieldByID("selCategory").focus();

		} else if (strFeromDate != "" && strToDate != ""
				&& !CheckDates(strFeromDate, strToDate)) {
			showERRMessage(todateLess)
			getFieldByID("txtDateFrom").focus();

		} else if (strFeromDate != "" && !dateValidDate(strFeromDate)) {

			showERRMessage(fromDateIncorrect);
			getFieldByID("txtDateFrom").focus();

		} else if (strToDate != "" && !dateValidDate(strToDate)) {
			showERRMessage(toDateIncorrect);
			getFieldByID("txtDateFrom").focus();

		} else {
			var strSearch = getSearchData();
			setField("hdnRecNo", "1");
			top[1].objTMenu.tabSetValue(ScreenId, strSearch + "~1");
			top[2].HidePageMessage();
			validated = true;
		}

	}
	return validated;
}

function search_click() {
	if (!searchData())
		return;
	document.forms[0].submit();
}

function setFields(strRowNo) {
	top[2].HidePageMessage();
	strRowData = objDG.getRowValue(strRowNo);
	strGridRow = strRowNo;
	rowData = strRowNo;
	setField("txtChargeCode", strRowData[0]);
	setField("hdnChargeCode", strRowData[0]);
	setChargeSelectData("AddSegselGroup", strRowData[2]);
	setField("txtNoOfDays", strRowData[12]);
	
	if(strRowData[2] == "SUR" && isAllowAssCookieCharges){
		setVisible("radCookieBased", true);
		setVisible("lblCookieBased", true);
	} else {
		setVisible("radCookieBased", false);
		setVisible("lblCookieBased", false);
		setVisible("searchDays", false);
		setVisible("txtMinNoOfDays", false);
		setVisible("txtMaxNoOfDays", false);
	}
	
	generateAppliesToOption(strRowData[2]);	
	
	var charegegroupAndCode = strRowData[2] + "-" + strRowData[0];
	chargeRateOperationTypeDisable = !(charegegroupAndCode == "ESU-CC" || charegegroupAndCode == "ESU-HC" );
	chargeRateJourneytTypeDisable =  !(charegegroupAndCode == "ESU-HC" );
	// if (strRowData[2] == "SUR" || strRowData[2] == "TAX")
	// {
		setVisible("spnRevenue", true);
		
		if (arrChargeData[strRowNo][25] == "Y") {
			getFieldByID("chkRevenue").checked = true;
			Disable("chkRevenue", true);
			Disable("txtRevenue", true);
			setField("txtRevenue", arrChargeData[strRowNo][26]);
			
		} else {
			getFieldByID("chkRevenue").checked = false;
			setField("txtRevenue", arrChargeData[strRowNo][26]);
			Disable("chkRevenue", true);
			Disable("txtRevenue", true);
		}
		
	// }
	// else
	// {
		// setVisible("spnRevenue", false);
	
	// }
	
	
	var airport = strRowData[3];
	if (strRowData[1] != "&nbsp;") {
		setField("txtDesc", strRowData[1]);
	}
	var category = strRowData[5];

	var glob = strRowData[4].toUpperCase();
	if (glob == "GLOBAL") {
		setField("radCat1", "GLOBAL");

	}
	if (glob == "POS") {
		setField("radCat1", "POS");
		if (airport.indexOf("/") != -1) {
			var arrAirport = airport.split("/");
		}

	}
	if (glob == "MANUAL") {
		setField("radCat1", "MANUAL");

	}
	if (glob == "OND") {
		setField("radCat1", "OND");
	}
	if (glob == "COOKIE_BASED"){
		enableCookieRelatedFields(true);
		setField("radCat1","COOKIE_BASED");
	}
	
	if (glob == "SERVICE_TAX") {
		showServiceTaxFields(true);
		disableServiceTaxFields(true);
		setField("radCat1", "SERVICE_TAX");
	}

	if (strRowData[7] == "Y") {
		Disable("chkRefundable", "");
		setField("chkRefundable", true);
		Disable("chkRefundable", "false");
	} else {
		Disable("chkRefundable", "");
		setField("chkRefundable", false);
		Disable("chkRefundable", "false");
	}
	
	if (arrChargeData[strRowNo][36] == "Y") {
		Disable("chkRefundableOnlyMOD", "");
		setField("chkRefundableOnlyMOD", true);
		Disable("chkRefundableOnlyMOD", "false");
	} else {
		Disable("chkRefundableOnlyMOD", "");
		setField("chkRefundableOnlyMOD", false);
		if (strRowData[7] == "Y") {
			Disable("chkRefundableOnlyMOD", "false");
		} else {
			Disable("chkRefundableOnlyMOD", "true");
		}
	}
	
	if(enableExcludeChargesFromFQ == "true"){
		if (arrChargeData[strRowNo][37] == "Y") {
			Disable("chkExcludeFromFareQuote", "");
			setField("chkExcludeFromFareQuote", true);
			Disable("chkExcludeFromFareQuote", "false");
		} else {
			Disable("chkExcludeFromFareQuote", "");
			setField("chkExcludeFromFareQuote", false);	
			Disable("chkExcludeFromFareQuote", "false");
		}
	}	

	setField("selApplyTo", arrChargeData[strRowNo][20]);
	Disable("selApplyTo", "false");

	if (strRowData[11] == "A") {
		setField("selOndSeg", "A");
		Disable("selOndSeg", "false");

	} else if (strRowData[11] == "F") {
		setField("selOndSeg", "F");
		Disable("selOndSeg", "false");
	} else {
		setField("selOndSeg", "L");
		Disable("selOndSeg", "false");
	}

	setField("selChannel", arrChargeData[strRowNo][35]);
	Disable("selChannel", "false");
	
	if (arrChargeData[strRowNo][13] == "Active") {
		Disable("chkStatus", "");
		setField("chkStatus", true);
		Disable("chkStatus", "false");
	} else {
		Disable("chkStatus", "");
		setField("chkStatus", false);
		Disable("chkStatus", "false");
	}

	var onds = arrChargeData[strRowNo][14];
	if (onds != "") {
		if (onds.indexOf(",") != -1) {
			var arrOnd = onds.split(",");
			for ( var u = 0; u < arrOnd.length - 1; u++) {
				var strVal = arrOnd[u];
				var strReplace = arrOnd[u];
				if (strVal.indexOf("***") != -1) {
					strReplace = strVal.replace(/\*\*\*/g, "All");
				}
				addToList(strReplace, u);
			}
		}
	}

	setField("hdnVersion", arrChargeData[strRowNo][11]);
	setField("hdnRateIDs", arrChargeData[strRowNo][12]);
	if (arrChargeData[strRowNo][17] != "null") {
		setField("selReportingGroup", arrChargeData[strRowNo][17]);
		getFieldByID("radRptChgCode").checked = true;
		setField("txtRptChargeGroupCode", "");
		setField("txtRptChargeGroupDesc", "");
	} else {
		setField("selReportingGroup", "");
	}
	var arr = arrChargeData[strRowNo][10];

	// set charge rates
	if (arr != null) {
		setChargesData(arr);
	}

	display = arrChargeData[strRowNo][15];

	// setting payment type
	var payType = arrChargeData[strRowNo][16];
	if (trim(payType) == 'D') {
		getFieldByID("radDept1").checked = true;
	} else if (trim(payType) == 'A') {
		getFieldByID("radArr").checked = true;
	}
	if (arrChargeData[strRowNo][19] != "null") {
		setField("selJourney", arrChargeData[strRowNo][19]);
		setField("hdnJourney", arrChargeData[strRowNo][19]);
	} else {
		setField("selJourney", "A");
		setField("hdnJourney", "A");
	}

	if (arrChargeData[strRowNo][21] != "") {
		var pos = arrChargeData[strRowNo][21];
		lssta.selectedData(pos);
	}

	if (arrChargeData[strRowNo][22] != "") {
		var POSinc = arrChargeData[strRowNo][22];
		if (POSinc == "E") {
			getFieldByID("radInc2").checked = true;
		} else {
			getFieldByID("radInc1").checked = true;
		}
	}
	
	
	

	if (glob == "POS" || glob == "OND") {
		if (arrChargeData[strRowNo][23] != "") {
			selectedAgents = arrChargeData[strRowNo][23];
		} else {
			selectedAgents = "";
		}
	}
	
	if (arrChargeData[strRowNo][24] != "") {
		var Agtinc = arrChargeData[strRowNo][24];
		if (Agtinc == "E") {
			getFieldByID("radAgtInc2").checked = true;
		} else {
			getFieldByID("radAgtInc1").checked = true;
		}
	}
		
	if (arrChargeData[strRowNo][27] == "" || arrChargeData[strRowNo][27] == "null" ) {
			setField("selCurrencyCode", "-1");
		} else {
			setField("selCurrencyCode", arrChargeData[strRowNo][27]);
			setVisible("selCurrencyCode", true);
			Disable("selCurrencyCode", true);
		}
	
	if (arrChargeData[strRowNo][28] != "") {
		var baseOrSelc = arrChargeData[strRowNo][28];
		if (baseOrSelc == "BASE_CURR") {
			getFieldByID("radCurr").checked = true;
		} else {
			getFieldByID("radSelCurr").checked = true;
		}
	}

	// transit
	 
		fillTransit(arrChargeData[strRowNo][29],'Inw');
		fillTransit(arrChargeData[strRowNo][30],'Out');
		fillTransit(arrChargeData[strRowNo][31],'Trn');
		fillMaxTrnTime(arrChargeData[strRowNo][32]);
		fillMinTrnTime(arrChargeData[strRowNo][33]);
		
   // Airport tax
	var airportTaxType = arrChargeData[strRowNo][34];
	if (airportTaxType != "") {
		getFieldByID("chkAirportTax").checked = true;
		getFieldByID("selAirportTaxType").value = airportTaxType;
	} else {
		getFieldByID("chkAirportTax").checked =  false;
	}
	setField("txtMinNoOfDays",arrChargeData[strRowNo][38]);
	setField("txtMaxNoOfDays",arrChargeData[strRowNo][39]);		
	
	if (arrChargeData[strRowNo][40] == "Y") {
		Disable("chkRefundableWhenExchange", "");
		setField("chkRefundableWhenExchange", true);
		Disable("chkRefundableWhenExchange", "false");
	} else {
		Disable("chkRefundableWhenExchange", "");
		setField("chkRefundableWhenExchange", false);	
		Disable("chkRefundableWhenExchange", "false");
	}

	if (arrChargeData[strRowNo][41] == "Y") {
		Disable("chkBundledFareCharge", "");
		setField("chkBundledFareCharge", true);
	} else {
		Disable("chkBundledFareCharge", "");
		setField("chkBundledFareCharge", false);
	}	
	
	if(isBundledFaresEnabled){
		Disable("chkBundledFareCharge", "false");
	}
	
	if(top.arrPrivi['plan.fares.cookie.charge.rate.add'] != 1 && strRowData[4] == "COOKIE_BASED"){
		Disable("btnEdit", true);
	}
	
	if (arrChargeData[strRowNo][44] == "Y") {
		Disable("chkServiceTaxInterState", true);
		setField("chkServiceTaxInterState", true);
	} else {
		Disable("chkServiceTaxInterState", true);
		setField("chkServiceTaxInterState", false);
	}
	
  if (arrChargeData[strRowNo][47] == "Y") {
    Disable("chkServiceTaxTicketing", true);
    setField("chkServiceTaxTicketing", true);
  } else {
    Disable("chkServiceTaxTicketing", true);
    setField("chkServiceTaxTicketing", false);
  }
	
	if (arrChargeData[strRowNo][42] == "" || arrChargeData[strRowNo][42] == "null" ) {
		setField("selCountry", "");
	} else {
		setField("selCountry", arrChargeData[strRowNo][42]);
		setVisible("selCountry", true);
		Disable("selCountry", true);
		populateInvoiceMapping();		
	}
	
	if($('#selCountry').val() != undefined && $('#selCountry').val() != ""){
		$('#selState').find('option').remove().end();
		var selectedCountry = $('#selCountry').val();
		if(countryStateArry[selectedCountry] != undefined ){
			showState = true;
			$( 'select[name="selState"]' ).append('<option value=""></option>');
			$( 'select[name="selState"]' ).append(countryStateArry[selectedCountry]);
		}
		if(arrChargeData[strRowNo][43] != "" || arrChargeData[strRowNo][43] != "null"){
			$("#selState").val(arrChargeData[strRowNo][43]);
		}
		
		if(showState){
			setVisible("rowSelState", true);
			setVisible("rowInterState", true);
		} else {
			setVisible("rowSelState", false);
			setVisible("rowInterState", false);
		}
		
		
	}
	
	if (arrChargeData[strRowNo][44] == "Y") {
		Disable("chkServiceTaxInterState", true);
		getFieldByID("chkServiceTaxInterState").checked =  true;
	} else {
		Disable("chkServiceTaxInterState", true);
		getFieldByID("chkServiceTaxInterState").checked =  false;
	}
	
	if (arrChargeData[strRowNo][45] == "" || arrChargeData[strRowNo][45] == "null" ) {
		setField("selGenerateReport", "");
	} else {
		setField("selGenerateReport", arrChargeData[strRowNo][45]);
		setVisible("selGenerateReport", true);
		Disable("selGenerateReport", true);
	}	
	
	if (arrChargeData[strRowNo][46] != undefined && arrChargeData[strRowNo][46] != "") {
		applicableCharges = arrChargeData[strRowNo][46];
		lsCharge.selectedData(applicableCharges);
	} else {
		lsCharge.selectedData("");
	}

  if (arrChargeData[strRowNo][47] == "Y") {
    Disable("chkServiceTaxTicketing", true);
    getFieldByID("chkServiceTaxTicketing").checked =  true;
  } else {
    Disable("chkServiceTaxTicketing", true);
    getFieldByID("chkServiceTaxTicketing").checked =  false;
  }
}
	 
	var fillTransit = function(data, type){
		if(type == 'Inw'){
			Disable("chkInwardTrn", false);
			if(data == "I"){
				getFieldByID("chkInwardTrn").value = "I";
			}else if(data == "E"){
				getFieldByID("chkInwardTrn").value = "E";
			} else {
				getFieldByID("chkInwardTrn").value = "N";
			}
			Disable("chkInwardTrn", true);
		}
		if(type == 'Out'){
			Disable("chkOutwardTrn", false);
			if(data == "I"){
				getFieldByID("chkOutwardTrn").value = "I";
			}else if(data == "E"){
				getFieldByID("chkOutwardTrn").value = "E";
			} else{
				getFieldByID("chkOutwardTrn").value = "N";
			}
			Disable("chkOutwardTrn", true);
		}
		if(type == 'Trn'){
			Disable("chkAllTrn", false);
			if(data == "I"){
				getFieldByID("chkAllTrn").value = "I";
			}else if(data == "E"){
				getFieldByID("chkAllTrn").value = "E";
			}else{
				getFieldByID("chkAllTrn").value = "N";
			}
			Disable("chkAllTrn", true);
		}
	} // fillTransit
	
	var fillMaxTrnTime = function(maxTrnTime){		
		if(maxTrnTime != ""){
			var tmpTxtTime = parseInt(maxTrnTime);
			var days = tmpTxtTime / (24*60);
			var hours = (tmpTxtTime % (24 * 60)) / 60;
			var minutes = (tmpTxtTime % (24 * 60)) % 60;
			getFieldByID("maxTranDuration_DD").value = parseInt(days);
			getFieldByID("maxTranDuration_HR").value = parseInt(hours);
			getFieldByID("maxTranDuration_MI").value = parseInt(minutes);
		}
		
	}
	
	var fillMinTrnTime = function(minTrnTime){
		if(minTrnTime != ""){
			var tmpTxtTime = parseInt(minTrnTime);
			var days = tmpTxtTime / (24*60);
			var hours = (tmpTxtTime % (24 * 60)) / 60;
			var minutes = (tmpTxtTime % (24 * 60)) % 60;
			getFieldByID("minTranDuration_DD").value = parseInt(days);
			getFieldByID("minTranDuration_HR").value = parseInt(hours);
			getFieldByID("minTranDuration_MI").value = parseInt(minutes);
		}
		
	}

// on Grid Row click
function rowClick(strRowNo) {	
	setTimeout("rowClickWithDelay("+strRowNo+")",1000);	
}

function rowClickWithDelay(strRowNo){
	addClick = false;
	rowClickEnable = true;
	model = null;
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(ScreenId))) {
		flagRowclick = "rowClick";
		clearControls();
		Disable("btnDelete", "");
		Disable("btnEdit", "");
		disableInputControls("false");$("tabAddModCharge#selSelected").tabFocus('tabs');
		Disable("btnDelete", "");
		setFields(strRowNo);
		categoryClick();
		checkAirportTax();
		Disable("radNewRptCHCode", "true");
		Disable("btnGEDit", "true");
		Disable("btnGAdd", "true");
		Disable("btnUpdate", "true");
		Disable("chkAirportTax", true);
		checkAirportTax();
		Disable("selAirportTaxType", true);
		setPageEdited(false);
		if (display != "1") {
			Disable("btnDelete", "true");
		}
		objDGMOD.setDisable("", "", true);  // to avoid getting journyType and
											// OperationType editable, before
											// Edit button is clicked
		Disable("txtMinNoOfDays",true);
		Disable("txtMaxNoOfDays",true);
	}
}

function disableChargesData(disabled) {
	try{
		objDGMOD.setDisable("", "", disabled);
		// objDGMOD.setDisable(0, 4, true);
		// objDGMOD.setDisable(0, 5, true);
		if(arrData!=null && arrData.length > 0){
			for(i=0;i<arrData.length;i++){
				objDGMOD.setDisable(i, 10, chargeRateOperationTypeDisable);
				objDGMOD.setDisable(i, 11, chargeRateJourneytTypeDisable);
			}		
		}	
	}catch(e){
		//do nothing
	}
}

function resetCharges() {

	var validated = false;

	var size = arrData.length;
	var str = "";
	var t = 0;
	if (size != 0) {
		for ( var i = 0; i < size; i++) {
			var from = objDGMOD.getCellValue(i, 0);
			var to = objDGMOD.getCellValue(i, 1);
			var val = objDGMOD.getCellValue(i, 4);
			var valOrPer = objDGMOD.getCellValue(i, 5);

			var curr = objDGMOD.getCellValue(i, 8);
			var active = objDGMOD.getCellValue(i, 9);
			var operationType = objDGMOD.getCellValue(i, 10);
			var journeyType = objDGMOD.getCellValue(i, 11);

			arrData[i] = new Array();
			arrData[i][1] = from;
			arrData[i][2] = to;
			arrData[i][3] = val;
			arrData[i][4] = valOrPer;
			arrData[i][5] = objDGMOD.getCellValue(i, 6);
			arrData[i][6] = objDGMOD.getCellValue(i, 7);
			arrData[i][7] = objDGMOD.getCellValue(i, 8);
			if (active == "Active") {
				arrData[i][8] = "true";

			} else {
				arrData[i][8] = "false";
			}
			arrData[i][9]=operationType;
			arrData[i][10]=journeyType;
			arrData[i][11] = "";
			arrData[i][12] = "";
			arrData[i][13] = i;
			arrData[i][14] = objDGMOD.getCellValue(i, 12);
			arrData[i][15] = objDGMOD.getCellValue(i, 13);
			arrData[i][16] = objDGMOD.getCellValue(i, 14);
			arrData[i][17] = objDGMOD.getCellValue(i, 2);
			arrData[i][18] = objDGMOD.getCellValue(i, 3);
			arrData[i][19] = "";
		}
	}

}

function setChargesData(arrCharges) {
	var version = "";
	var rateIDs = "";

	arrData.length = 0;
	if (arrCharges.length >= 0) {
		for ( var i = 0; i < arrCharges.length; i++) {
			arrData[i] = new Array();
			arrData[i][0] = arrCharges[i][0];
			arrData[i][1] = arrCharges[i][1];
			arrData[i][2] = arrCharges[i][2];
			arrData[i][3] = arrCharges[i][3];
			arrData[i][4] = arrCharges[i][4];
			if(arrCharges[i][5] == "null"){
			arrData[i][5] = "";			
			} else {
			arrData[i][5] = arrCharges[i][5];
			}
			if(arrCharges[i][6] == "null"){
			arrData[i][6] = "";			
			} else {
			arrData[i][6] = arrCharges[i][6];
			}
			arrData[i][7] = arrCharges[i][7];
			if (arrCharges[i][8] == "ACT") {
				arrData[i][8] = "true";

			} else {
				arrData[i][8] = "false";
			}
			arrData[i][9] =arrCharges[i][12];
			arrData[i][10] =arrCharges[i][13];
			arrData[i][11] = "";
			arrData[i][12] = "";
			arrData[i][13] = i;
			arrData[i][14] = arrCharges[i][14];
			arrData[i][15] = arrCharges[i][15];
			arrData[i][16] = arrCharges[i][16];
			arrData[i][17] = arrCharges[i][17];
			arrData[i][18] = arrCharges[i][18];
			version += arrCharges[i][9] + ",";
			rateIDs += arrCharges[i][10] + ",";
			arrData[i][19] = "";
		}
		objDGMOD.arrGridData = arrData;
		objDGMOD.refresh();

		for ( var i = 0; i < arrCharges.length; i++) {
			if (arrCharges[i][8] == "ACT") {
				objDGMOD.setCellValue(i, 9, "true");
			} else {
				objDGMOD.setCellValue(i, 9, "false");
			}
		}
	} else {
		objDGMOD.refresh();
	}

	setField("hdnRateVersion", version);
	setField("hdnRateIDs", rateIDs);
	disableChargesData(true);
}

function disablechargesOnEdit() {
	for ( var e = 0; e < rowsDisabled; e++) {
		if (objDGMOD.getCellValue(0, 0) == "") {

		} else {
			objDGMOD.setDisable(e, "", true);
		}
	}
}

function getSelGroup() {
	setPageEdited(true);
	if (getFieldByID("AddSegselGroup").value == "") {
		
	} else {
		
		
		var segSelGrp = getFieldByID("AddSegselGroup");
		var selIndex = segSelGrp.selectedIndex;
		
		// 05Aug2011 - Rikaz
		// Added to hide the {Apply To Reservation-6} when Group is Not "ESU"
		generateAppliesToOption(segSelGrp.options[selIndex].text);
		
		if(selIndex != -1){
			var selText = segSelGrp.options[selIndex].text;
			if(selText == "SUR" && isAllowAssCookieCharges && top.arrPrivi['plan.fares.cookie.charge.rate.add'] == 1){
				setVisible("radCookieBased", true);
				setVisible("lblCookieBased", true);
			} else {
				setField("radCat1","");
				setVisible("radCookieBased", false);
				setVisible("lblCookieBased", false);
				setVisible("searchDays", false);
				setVisible("txtMinNoOfDays", false);
				setVisible("txtMaxNoOfDays", false);
			}
			
			// if (selText == "SUR" || selText == "TAX"){
				setVisible("spnRevenue", true);
				Disable("txtRevenue", true);
				Disable("chkRevenue", false);
				
				setField("txtRevenue", "");
				getFieldByID("chkRevenue").checked = false;
				
			// }
			// else
			// {
			// Disable("txtRevenue", true);
			// Disable("chkRevenue", true);
				
			// setField("txtRevenue", "");
			// getFieldByID("chkRevenue").checked = false;
			// setVisible("spnRevenue", false);
			// }
			
		}
		
		
		
		
		
		

	}
}

function checkRevenue(){
	setPageEdited(true);
	if (getFieldByID("chkRevenue").checked)
	{
		Disable("txtRevenue", false);
		setField("txtRevenue", "");
	}
	else
	{
		setField("txtRevenue", "");
		Disable("txtRevenue", true);
		
	}
}


function RevenuePress(objTextBox) {
	
	setPageEdited("true");
	var strStr = getText("txtRevenue");	
	var strLen = strStr.length;
	var blnVal = currencyValidate(strStr, 3, 2);	
	var wholeNumber;
	if (!blnVal) {		
		if (strStr.indexOf(".") != -1) {
			wholeNumber = strStr.substr(0, strStr.indexOf("."));
			if (wholeNumber.length > 15) {
				setField("txtRevenue", strStr.substr(0,
						wholeNumber.length - 1));
			} else {
				setField("txtRevenue", strStr.substr(0, strLen - 1));
			}
		} else {
			setField("txtRevenue", strStr.substr(0, strLen - 1));
		}
		getFieldByID("txtRevenue").focus();
	}
	if(strStr != null && parseInt(strStr) > 100){
		setField("txtRevenue", strStr.substr(0, strLen - 1));
		getFieldByID("txtRevenue").focus();
	} 
}

function editChargesData() {
	var size = arrData.length;
	var str = "";
	var t = 1;
	disablechargesOnEdit();

	for ( var i = 0; i < rowsDisabled; i++) {

		var fromDate = convertStringToDate(objDGMOD.getCellValue(i, 0));
		var toDate = convertStringToDate(objDGMOD.getCellValue(i, 1));
		var trvelFromDate = convertStringToDate(objDGMOD.getCellValue(i, 2));
		var valPer = objDGMOD.getCellValue(i, 4);
		var val = objDGMOD.getCellValue(i, 5);
		var curr = objDGMOD.getCellValue(i, 6);
		var active = objDGMOD.getCellValue(i, 9);
        var isFromDatePast = isDatePast(fromDate); 
		var isToDatePast = isDatePast(toDate);
		var isTravelFromDatePast = isDatePast(trvelFromDate);
		 arrData[i][19] = "";
		
		if (isFromDatePast && isToDatePast) {
			objDGMOD.setDisable(i, "", true);
		} else if (isFromDatePast && !isToDatePast) {
			objDGMOD.setDisable(i, "", true);
			objDGMOD.setDisable(i, 1, false);
			// objDGMOD.setDisable(i, 0, true);
			// objDGMOD.setDisable(i, 1, false);
			// objDGMOD.setDisable(i, 2, false);
			objDGMOD.setDisable(i, 3, false);
			// objDGMOD.setDisable(i, 4, false);
			// objDGMOD.setDisable(i, 5, false);
			// objDGMOD.setDisable(i, 6, false);
			// objDGMOD.setDisable(i, 7, false);
			// objDGMOD.setDisable(i, 8, false);
			objDGMOD.setDisable(i, 9, false);
			// objDGMOD.setDisable(i, 10, chargeRateOperationTypeDisable);
			// objDGMOD.setDisable(i, 11, chargeRateJourneytTypeDisable);
			// objDGMOD.setDisable(i, 12, false);
			// objDGMOD.setDisable(i, 13, false);
			// objDGMOD.setDisable(i, 14, false);
			 arrData[i][11] = "EditTo";	
			 
		} else if (!isFromDatePast && !isToDatePast) {
			objDGMOD.setDisable(i, "", false);
			objDGMOD.setDisable(i, 10, chargeRateOperationTypeDisable);
			objDGMOD.setDisable(i, 11, chargeRateJourneytTypeDisable);	
			arrData[i][11] = "Edit";
			if(!isTravelFromDatePast){
				arrData[i][19]= "editable";
				objDGMOD.setDisable(i, 2, false);
			} else{
				arrData[i][19]= "notEditable";
			}
			
		} else {
			objDGMOD.setDisable(i, "", true);
		}	
		// disableLocalCurrencyOnSelection(i);
	}
}

function checkChagesDates(from, to , operationType , journeyType, cClass, active, count) {
	
	var size = arrData.length;
	var validate = false;
	for ( var i = 0; i < size; i++) {
		var fromPrev, toPrev , applTypePrev , prevRow , currentRow ;

	    fromPrev = objDGMOD.getCellValue(i, 0);
		toPrev = objDGMOD.getCellValue(i, 1);
		activePrev = objDGMOD.getCellValue(i, 9);
		operationTypePrev = objDGMOD.getCellValue(i, 10);
		journeyTypePrev = objDGMOD.getCellValue(i, 11);
		cClasPrev = objDGMOD.getCellValue(i, 12);

		if (count != i) {
			if (compareDatesForOverLapLogic(fromPrev, from) && compareDatesForOverLapLogic(to, toPrev)) {
				validate = true;
			} else if (compareDatesForOverLapLogic(fromPrev, from)
					&& compareDatesForOverLapLogic(from, toPrev)) {
				validate = true;
			} else if (compareDatesForOverLapLogic(fromPrev, to) && compareDatesForOverLapLogic(to, toPrev)) {
				validate = true;				
			}	
						
			//Even if the effective date ranges are overlapped, user can define it if correspoding sales date ranges are not overlapped.
			if(validate){
				validate = false;
				var salesFrom,salesTo,salesFromPrev,salesToPrev;
				salesFromPrev = objDGMOD.getCellValue(i, 2);
				salesToPrev = objDGMOD.getCellValue(i, 3);
				salesFrom = objDGMOD.getCellValue(count,2);
				salesTo = objDGMOD.getCellValue(count,3);
				
				if (compareDatesForOverLapLogic(salesFromPrev, salesFrom) && compareDatesForOverLapLogic(salesTo, salesToPrev)) {
					validate = true;
				} else if (compareDatesForOverLapLogic(salesFromPrev, salesFrom)
						&& compareDatesForOverLapLogic(salesFrom, salesToPrev)) {
					validate = true;
				} else if (compareDatesForOverLapLogic(salesFromPrev, salesTo) && compareDatesForOverLapLogic(salesTo, salesToPrev)) {
					validate = true;
				}					
			}							
				if (validate){ // date range overlap occured
					validate = active && activePrev; // rates are active
					if (validate){
						prevRow = operationTypePrev + "" + journeyTypePrev;
						currentRow =  operationType + "" + journeyType ;
						validate = ( prevRow == currentRow ); // active ope
																// and jrny type
						
						if (!validate ){
							if (operationType == "0"){
								validate = ( operationTypePrev == "1" || operationTypePrev == "2")  ;
							}else if (operationType == "1" || operationType == "2")
							{
								validate = ( operationTypePrev == "0"  )  ;					
							}
						}
						
						if ( !validate ){
							if (journeyType == "0"){
								validate = ( journeyTypePrev == "1" || journeyTypePrev == "2")  ;
							}else if (journeyType == "1" || journeyType == "2")
							{
								validate = ( journeyTypePrev == "0"  )  ;					
							}
						}	
						
						if(validate){
							if(cClass == "-1"){
								validate = true;
							}else{
								if(cClasPrev == "-1"){
									validate = true;
								}else if(cClass == cClasPrev){
									validate = true;
								}else{
									validate = false;
								}
							}
							
						}
				}				
			}
		}		
		// break the loop
		if (validate){
			return true;
		}		
		// end of if
	}// end of for
	return validate;
}

function getChargesData() {

	var validated = true;
	ctrlSetStatus();	

	var size = arrData.length;
	var str = "";
	var t = 1;
	for ( var i = 0; i < size; i++) {
		var rowNo = Number(i) + 1;
		var chargeBasis = "V";
        var fromDateString = objDGMOD.getCellValue(i, 0);
		var toDateString = objDGMOD.getCellValue(i, 1);			
		var val = objDGMOD.getSelectedColumn(4)[i][1];
		var valOrPer = objDGMOD.getSelectedColumn(5)[i][1];
		var floatPer = parseFloat(valOrPer);
		var curr = objDGMOD.getSelectedColumn(8)[i][1];
		var intCurr = isNaN(curr);
		var intPerc = isNaN(valOrPer);
		var active = objDGMOD.getCellValue(i, 9);		
		
		// index was refering to the wrong index,
		// it leads some of the validation to skip while execution - Rikaz
		// var status = arrData[i][7];
		var status = arrData[i][11]; 
		var travelFromDateStatus = arrData[i][19];
		var operationType = objDGMOD.getSelectedColumn(10)[i][1];
		var journeyType = objDGMOD.getSelectedColumn(11)[i][1];
		var cabinClassCode = objDGMOD.getSelectedColumn(12)[i][1];
		var breakPoint = objDGMOD.getSelectedColumn(13)[i][1];
		var boundryValue = objDGMOD.getSelectedColumn(14)[i][1];	
		var travelFromDateString = objDGMOD.getCellValue(i, 2);
		var travelToDateString = objDGMOD.getCellValue(i, 3);
		var travelFromDate= convertStringToDate(travelFromDateString);
		var travelToDate= convertStringToDate(travelToDateString);			
		var minCharge = objDGMOD.getSelectedColumn(6)[i][1];
		var maxCharge = objDGMOD.getSelectedColumn(7)[i][1];
		
		$("#tabs").tabs('option', 'active', '#tabAddModChargeRate');
	
		
		if ((getText("AddSegselGroup") == "SUR"
			&& ((getText("radCat1") == "COOKIE_BASED" && val != "V")|| val == "PFS" || val == "PTFS")) || (getText("radCat1") == "SERVICE_TAX" && val != "PTFST") || (getText("radCat1") != "SERVICE_TAX" && val == "PTFST")) {
			showERRMessage(invalidValueCalculationMethod);
			validated = false;
			objDGMOD.setColumnFocus(i, 4);
			return false;
		}
		
		if(checkedBundleFare && val != "V"){
			showERRMessage(invalidValueCalculationMethod);
			validated = false;
			objDGMOD.setColumnFocus(i, 4);
			return false;
		}		
		
		if (trim(fromDateString) == "") {
			showERRMessage(fromdateblank + " in row " + rowNo);
			validated = false;
			objDGMOD.setColumnFocus(i, 0);
			return false;
		}
		
		if (trim(toDateString) == "") {
			showERRMessage(todateblank + " in row " + rowNo);
			validated = false;
			objDGMOD.setColumnFocus(i, 1);
			return false;
		}
		var fromDate = convertStringToDate(fromDateString);
		var toDate = convertStringToDate(toDateString);
		
		if(isNaN(fromDate)) {
			showERRMessage(rateFromDateIncorrect + " " + t);
			validated = false;
			objDGMOD.setColumnFocus(i, 0);
			return false;
    	}
				
		if(isNaN(toDate)) {
			showERRMessage(reteToDateIncorrect + " " + t);
			validated = false;
			objDGMOD.setColumnFocus(i, 1);
			return false;
	    }

		if (curr == "" && toDateString == "" && fromDateString == "" && status != "Added") {
			showERRMessage(fromdateRqrd + " " + t);
			validated = false;
			objDGMOD.setColumnFocus(i, 0);
			return false;
		}
		if (((toDateString != "") || (valOrPer != "") || (curr != "")) && (fromDateString == "")) {
			showERRMessage(fromdateRqrd + " " + t);
			validated = false;
			objDGMOD.setColumnFocus(i, 0);
			return false;
		}
	
		if (fromDateString != "" && toDateString == "") {
			showERRMessage(todateRqrd + " " + t);
			objDGMOD.setColumnFocus(i, 1);
			validated = false;
			return false;
		}
		
		if (toDate < fromDate) {
			showERRMessage(todateLess + " " + t);
			objDGMOD.setColumnFocus(i, 1);
			validated = false;
			return false;
		}
		
		if (fromDateString != "" && valOrPer == "" && !getFieldByID("radSelCurr").checked) {
			showERRMessage(valueRqrd + " " + t);
			objDGMOD.setColumnFocus(i, 5);
			validated = false;
			return false;
		}
		if (curr != "" && intCurr ) {
			showERRMessage(currencyNan + " " + t);
			objDGMOD.setColumnFocus(i, 5);
			validated = false;
			return false;
		}
		if (valOrPer != "" && intPerc) {
			showERRMessage(percNan + " " + t);
			objDGMOD.setColumnFocus(i, 5);
			validated = false;
			return false;
		}
		if ((fromDateString != "") && (status == "Added" || status == "Edit")
				&& (status != "EditTo") && (currentSystemDateAndTime > fromDate)) {
			showERRMessage(FromDateExceedsCurrentDate + " " + t);
			objDGMOD.setColumnFocus(i, 0);
			validated = false;
			return false;
		}
		if (val != "V" && floatPer > 99) {
			showERRMessage(percentageExceeds + " " + t);
			objDGMOD.setColumnFocus(i, 5);
			validated = false;
			return false;
		}
		if (val != "V" && floatPer == 0) {
			showERRMessage(percentageZero + " " + t);
			objDGMOD.setColumnFocus(i, 5);
			validated = false;
			return false;
		}
		if(floatPer < 0){
			showERRMessage(percentagelessThanZero + " " + rowNo);
			objDGMOD.setColumnFocus(i, 5);
			validated = false;
			return false;
		}
		if (toDate != "" && status == "EditTo" && isDatePast(toDate)) {
			showERRMessage(ToDateExceedsCurrentDate + " " + t);
			objDGMOD.setColumnFocus(i, 1);
			validated = false;
			return false;
		}
		if ( minCharge != "" && maxCharge != "") { // val != "V" &&
			var minf = parseFloat(minCharge);
			var maxf = parseFloat(maxCharge);			
			if(minf > maxf){			
				showERRMessage(minExceedsMax + " " + t);
				objDGMOD.setColumnFocus(i, 6);
				validated = false;
				return false;
			}
		}
		
		if (val == "V" && breakPoint != "" ) {
			showERRMessage(breakPointApplicableOnlyForPercentages + " " + t);
			objDGMOD.setColumnFocus(i, 13);
			validated = false;
			return false;
		}
		
		if (val == "V" && boundryValue != "") {
			showERRMessage(boundryValueApplicableOnlyForPercentages + " " + t);
			objDGMOD.setColumnFocus(i, 14);
			validated = false;
			return false;
		}
		
		if (breakPoint != "") {
			if (boundryValue == "") {
				showERRMessage(boundryValueAndBreakPointNeeded + " " + t);
				objDGMOD.setColumnFocus(i, 14);
				validated = false;
				return false;
			}
			var bPf = parseFloat(breakPoint);
			if (bPf <= 0) {
				showERRMessage(breakPointPositive + " " + t);
				objDGMOD.setColumnFocus(i, 13);
				validated = false;
				return false;
			}			
		}
		
		if (boundryValue != "") {
			if (breakPoint == "") {
				showERRMessage(boundryValueAndBreakPointNeeded + " " + t);
				objDGMOD.setColumnFocus(i, 14);
				validated = false;
				return false;
			}
			var bVf = parseFloat(boundryValue);
			if (bVf <= 0) {
				showERRMessage(boundryValuePositive + " " + t);
				objDGMOD.setColumnFocus(i, 14);
				validated = false;
				return false;
			}
		}
		
		if(val != "V"){
			chargeBasis = val;
			val = "P";
		} 
		
		if (isNaN(travelFromDate)) {
			showERRMessage(travelFromDateErr + " " + t);
			objDGMOD.setColumnFocus(i, 2);
			validated = false;
			return false;
		}	
		
		if (isNaN(travelToDate)) {
			showERRMessage(travelToDateErr + " " + t);
			objDGMOD.setColumnFocus(i, 3);
			validated = false;
			return false;
		}
		
		if (travelFromDate != ""  && (status == "Added") && isDatePast(travelFromDate)) {
			showERRMessage(travelFromDateGrt + " " + t);
			objDGMOD.setColumnFocus(i, 2);
			validated = false;
			return false;		
		}
		
		if (travelFromDate != ""  && (status == "Edit") && (travelFromDateStatus == "editable") && isDatePast(travelFromDate)) {
			showERRMessage(travelFromDateGrt + " " + t);
			objDGMOD.setColumnFocus(i, 2);
			validated = false;
			return false;		
		}
		
		if (travelFromDate != "" && travelToDate == "") {
			showERRMessage(travelToDateErr + " " + t);
			objDGMOD.setColumnFocus(i, 3);
			validated = false;
			return false;

		}

		if (travelFromDate != "" && travelToDate != "" && !compareDates(travelFromDateString, travelToDateString)) {
			showERRMessage(travelDateRangeErr + " " + t);
			objDGMOD.setColumnFocus(i, 3);
			validated = false;
			return false;
		}
		
		if (fromDate != "" && toDate != "" && !compareDates(fromDateString, toDateString)) {
			showERRMessage(effectiveDateRangeErr + " " + t);
			objDGMOD.setColumnFocus(i, 1);
			validated = false;
			return false;
		}
		
		if(getFieldByID("radSelCurr").checked && (val != "V")){
			showERRMessage(localCurrencyWithPercentageNotAllowed + " " + " in row " +t);
			objDGMOD.setDisable(i, 4, false); 
			objDGMOD.setColumnFocus(i, 4);
			validated = false;
			return false;
		}
		
		if(getFieldByID("radSelCurr").checked && (intCurr || curr == "") && (!intPerc && valOrPer != "")){ 			
			var selectedCurrency = getText("selCurrencyCode") ;
			var confirmMessage = "Charges are not defined with selected currency in row "+rowNo+ ". Do you want the amount "+valOrPer+ " to be saved in "+selectedCurrency+ "?";			
			if(!confirm(confirmMessage)){
				validated = false;
				return false;
			}			
		}
				
// else {
// minCharge = "";
// maxCharge = "";
// }

		if (validated) {
			ctrlSetStatus();
			str += fromDateString + "," + toDateString + "," + val + "," + valOrPer + "," + curr
					+ "," + active + "," + minCharge + "," + maxCharge + "," + chargeBasis +","+operationType + "," +journeyType + "," +cabinClassCode
					+ "," + breakPoint + "," + boundryValue + ","+travelFromDateString+","+travelToDateString+", ";
			str += ";";
			if (i == (size - 1)) {
				top[2].HidePageMessage();
				setField("hdnChargeData", str);
				return true;
			}
		}
		++t;
	}
	return validated;
}

function disableInputControls(disabled) {
	Disable("chkStatus", disabled);
	Disable("txtChargeCode", disabled);
	Disable("AddSegselGroup", disabled);
	Disable("txtDesc", disabled);
	Disable("chkRefundable", disabled);
	Disable("selApplyTo", disabled);
	Disable("selOndSeg", disabled);
	Disable("selChannel", disabled);
	Disable("radCat1", disabled);
	// Disable("selAirport",disabled);
	Disable("btnSave", disabled);
	Disable("btnReset", disabled);
	Disable("radDept1", disabled);

	Disable("selSelected", disabled);
	Disable("btnAddSeg", disabled);
	Disable("btnRemSeg", disabled);
	Disable("selDepature", disabled);
	Disable("selArrival", disabled);
	Disable("selVia1", disabled);
	Disable("selVia2", disabled);
	Disable("selVia3", disabled);
	Disable("selVia4", disabled);

	Disable("chkRefundableWhenExchange", disabled);
	Disable("chkRefundableOnlyMOD", disabled);
	if(enableExcludeChargesFromFQ == "true"){
		Disable("chkExcludeFromFareQuote", disabled);
	}
	
	Disable("selApplyTo", disabled);
	Disable("chkStatus", disabled);
	Disable("selReportingGroup", disabled);
	Disable("selJourney", disabled);
	Disable("radCurr", disabled);
	Disable("radSelCurr", disabled);
	
	Disable("chkInwardTrn", disabled);
	Disable("chkOutwardTrn", disabled);
	Disable("chkAllTrn", disabled);
	Disable("minTranDuration_DD", disabled);
	Disable("minTranDuration_HR", disabled);
	Disable("minTranDuration_MI", disabled);
	
	Disable("maxTranDuration_DD", disabled);
	Disable("maxTranDuration_HR", disabled);
	Disable("maxTranDuration_MI", disabled);
	
	Disable("chkRevenue", disabled);
	Disable("txtRevenue", disabled);
	
	Disable("chkAirportTax", disabled);
	Disable("selAirportTaxType", disabled);
	
	Disable("radRptChgCode", disabled);
	Disable("radNewRptCHCode", disabled);
	Disable("txtRptChargeGroupCode", disabled);
	Disable("txtRptChargeGroupDesc", disabled);
	Disable("txtMinNoOfDays",disabled);
	Disable("txtMaxNoOfDays",disabled);	
	Disable("chkBundledFareCharge", disabled);
}

function disableOnLoadOnly() {
	Disable("btnAddSeg", "true");

}

function clearControls() {
	setField("chkStatus", "");
	setField("txtChargeCode", "");
	setField("AddSegselGroup", "");
	setField("txtDesc", "");
	setField("chkRefundable", "");
	setField("selApplyTo", "All");
	setField("selOndSeg", "All");
	setField("selChannel", "-1");
	setField("selDepature", "");
	setField("selArrival", "");
	setField("selVia1", "");
	setField("selVia2", "");
	setField("selVia3", "");
	setField("selVia4", "");
	// setField("selAirport", "All");
	setField("selReportingGroup", "");
	setField("selJourney", "A");
	setField("hdnJourney", "A");

	setField("radCat1", "");
	setField("chkAirportTax", "");
	setField("txtMinNoOfDays", "");
	setField("txtMaxNoOfDays", "");
	setField("chkBundledFareCharge", "");
	clearServiceTax();
	clearAll();
	if (flagRowclick != "rowClick") {
		clearChargesData();
	}
}

function clearServiceTax(){
	setField("selCountry", "");
	setField("selState", "");
	setField("chkServiceTaxInterState", "");
	setField("chkServiceTaxTicketing", "");
	setField("selGenerateReport", "");
}

function loadCheck() {
	if (typeof (objDGMOD) == "object") {
		if (objDGMOD.loaded) {
			if (model == null || model.length <= 0) {
				clearTimeout(objTimer);
				objDGMOD.setDisable("", "", true);

			} else {
				for ( var t = 0; t < arrData.length; t++) {
					objDGMOD.setDisable(t, "0", false);
					objDGMOD.setDisable(t, "1", false);
					objDGMOD.setDisable(t, "2", false);
					objDGMOD.setDisable(t, "3", false);
					objDGMOD.setDisable(t, "4", true);
					objDGMOD.setDisable(t, "5", true);
					objDGMOD.setDisable(t, "6", false);
					objDGMOD.setDisable(t, "7", false);
					objDGMOD.setDisable(t, "8", false);
					objDGMOD.setDisable(t, "9", false);
				}
			}
		}
	}
}

var objTimer = setInterval("loadCheck()", 700);
var timer;

function loadCheck2() {
	if (objDGMOD.loaded) {
		clearTimeout(timer);
		objDGMOD.setCellValue(0, 9, "true");
	}
}

function clearChargesData() {

	arrData.length = 0;
	var arr = arrData;
	arr[0] = new Array();
	arr[0][0] = "";
	arr[0][1] = "";
	arr[0][2] = "";
	arr[0][3] = "V"
	arr[0][4] = "";
	arr[0][5] = "";
	arr[0][6] = "";
	arr[0][7] = "";
	arr[0][8] = "true";
	arr[0][9] = "0";
	arr[0][10] = "0";
	arr[0][11] = "Added";
	arr[0][12] = "";
	arr[0][13] = "0";
	arr[0][15] = "";
	arr[0][16] = "";
	arr[0][17] = "";
	arr[0][18] = "";
	objDGMOD.refresh();
	
	if (arrData.length >= 0) {
		timer = setInterval("loadCheck2()", 300);
	}
	disableChargesData(true);
}

function setChargeSelectData(strField, strCode) {
	var control = document.getElementById(strField);

	for ( var t = 0; t < (control.length); t++) {
		if (control.options[t].text == strCode) {
			control.options[t].selected = true;
			break;
		}
	}
}

function saveData() {	
	var validated = false;

	var strSearch = getSearchData();
	
	setField("hdnRecNo", "1");
	top[1].objTMenu.tabSetValue(ScreenId, strSearch + "~1");	
	var strCC = getText("txtChargeCode");
	var strGroup = getText("AddSegselGroup");
	var strDesce = getText("txtDesc");
	var strRefund = getText("chkRefundable");
	var strRefundOnlyForMOD = getText("chkRefundableOnlyMOD");
	var strRefundWhenExchange = getText("chkRefundableWhenExchange");
	var strServiceTaxInterState = getText("chkServiceTaxInterState");
	var strServiceTaxTicketing = getText("chkServiceTaxTicketing");
	var strBundledFareCharge = getText('chkBundledFareCharge');
	var strExludeFromFareQuote = "";
	if(enableExcludeChargesFromFQ == "true" && getFieldByID("chkExcludeFromFareQuote").checked == true){
		strExludeFromFareQuote = "on";
	}
	
	var strInfants = getText("selApplyTo");
	var strOndSeg = getText("selOndSeg");
	var strChannel = getText("selChannel");
	var strDisplay = getValue("AddSegselGroup");
	var strCat = getText("radCat1");
	var strInc = getText("radInc1");
	var strIncAgt = getText("radAgtInc1");
	
	setField("hdnSegments", getAll());
	var strSegments = getText("hdnSegments");
	var strDepature = getText("selDepature");
	var strArrival = getText("selArrival");
	var strPOS = lssta.getselectedData();
	var strAgt = "";
	if((strCat == "POS" || strCat == "OND") && lsagt != undefined && lsagt.getselectedData() != undefined){
		strAgt = lsagt.getselectedData();
	}
	var strCharges = lsCharge.getselectedData();
	var journey = getValue("hdnJourney");
	var minTrnDur = getTimeInMinitueValue("min");
	var maxTrnDur = getTimeInMinitueValue("max");
	
	var strAirportTax = getText("chkAirportTax");
	var strAirportCategoryType = getValue("selAirportTaxType");
	var strReportChargeGroupCode = getValue("txtRptChargeGroupCode");
	var strReportChargeGroupDescription = getValue("txtRptChargeGroupDesc");
	
	var minGap = getText("txtMinNoOfDays");
	var maxGap = getText("txtMaxNoOfDays");
	
	setField("hdnGroup", getText("AddSegselGroup"));
	setField("hdnStatus", getText("chkStatus"));
	setField("hdnDesc", strDesce);
	setField("hdnCat", strCat);
	setField("hdnApplyTo", getValue("selApplyTo"));
	setField("hdnOndSeg", getValue("selOndSeg"));
	setField("hdnChannel", getValue("selChannel"));
	setField("hdnRefund", strRefund);
	setField("hdnRefundOnlyForMOD", strRefundOnlyForMOD);
	setField("hdnRefundWhenExchange", strRefundWhenExchange);
	setField("hdnServiceTaxInterState", strServiceTaxInterState);
	setField("hdnServiceTaxTicketing", strServiceTaxTicketing);  
	setField("hdnExcludeFromFareQuote", strExludeFromFareQuote);
	setField("hdnBundledFareCharge", strBundledFareCharge);
	setField("hdnSelSelected", getText("selSelected"));
	setField("hdnpayType", getText("radDept1"));
	setField("hdnJourney", journey);
	setField("hdnInc", strInc);
	setField("hdnIncOnd", strIncAgt);
	setField("hdnAirportTax", strAirportTax);
	setField("hdnAirportTaxType", strAirportCategoryType);
	setField("hdnMinTrnDuration", minTrnDur);
	setField("hdnMaxTrnDuration" , maxTrnDur);
	if(getFieldByID("radNewRptCHCode").checked){
		setField("hdnRptChargeGroupType",getFieldByID("radNewRptCHCode").value);
	}else{
		setField("hdnRptChargeGroupType",getFieldByID("radRptChgCode").value);
	}
	
	if (strCC == "") {	
		showERRMessage(chargecodeRqrd);		
		$("tabAddModCharge#txtChargeCode").tabFocus('tabs');
	} else if (strGroup == "") {		
		showERRMessage(groupRqrd);
		$("tabAddModCharge#AddSegselGroup").tabFocus('tabs');
	} else if (strDesce == "") {		
		showERRMessage(descRqrd);
		$("tabAddModCharge#txtDesc").tabFocus('tabs');
	} else if (strCat == "") {		
		showERRMessage(categoryRqrd);
		$("tabAddModCharge#radCat1").tabFocus('tabs');
	} else if (strCat == "POS" && strPOS == "") {		
		showERRMessage(posRqrd);
		POSenable();
	} else if (strCat == "POS" && strChannel == "IBE") {		
		showERRMessage(posInvalidWithIBE);
		$("tabAddModCharge#selChannel").tabFocus('tabs');
	} else if (strCat == "OND" && strSegments == "") {		
		showERRMessage(segmentRqrd);
		$("tabAddModCharge#selSelected").tabFocus('tabs');
	} else if((strCat == "COOKIE_BASED") && minGap == ""){
			showERRMessage(minDayGapRqrd);
			getFieldByID("txtMinNoOfDays").focus();			
	} else if((strCat == "COOKIE_BASED") && maxGap == ""){
			showERRMessage(maxDayGapRqrd);
			getFieldByID("txtMaxNoOfDays").focus();			
	} else if((strCat == "COOKIE_BASED") && Number(minGap) == 0){
			showERRMessage(minMaxMoreThanZero);
			getFieldByID("txtMinNoOfDays").focus();
	}  else if((strCat == "COOKIE_BASED") && Number(maxGap) == 0){
			showERRMessage(minMaxMoreThanZero);
			getFieldByID("txtMaxNoOfDays").focus();
	} else if((strCat == "SERVICE_TAX") && getText("selCountry") == 0){
		showERRMessage(serviceTaxCountryRequired);
		getFieldByID("selCountry").focus();
	} else if((strCat == "SERVICE_TAX") && getText("selGenerateReport" == "")){
		showERRMessage(serviceTaxInvoiceCategoryRequired);
		getFieldByID("selGenerateReport").focus();
	} else if((strCat == "COOKIE_BASED") && (Number(minGap) > Number(maxGap))){
			showERRMessage(maxLessThanMin);
			setField("txtMinNoOfDays","");
			setField("txtMaxNoOfDays","");
			getFieldByID("txtMinNoOfDays").focus();
			
	} else if (strDisplay != "" && strDisplay != "1"
			&& document.getElementById("AddSegselGroup").disabled == false) {		
		if(!(strGroup == "SUR" && isBundledFaresEnabled && strBundledFareCharge == 'on')){			
			showERRMessage(groupInactive);
			$("tabAddModCharge#AddSegselGroup").tabFocus('tabs');
		} else {
			validated = true;
		}
	} else if(isBundledFaresEnabled && strGroup != "SUR" && strBundledFareCharge == 'on'){
		showERRMessage(groupInactive);
		$("tabAddModCharge#AddSegselGroup").tabFocus('tabs');
	} else if (journey == "") {				
		showERRMessage(journeynull);
		$("tabAddModCharge#selJourney").tabFocus('tabs');
	// }else if (strGroup == "SUR" && (getFieldByID("chkRevenue").checked ==
	// true && getText("txtRevenue") == "")){
	}else if (getFieldByID("chkRevenue").checked == true && getText("txtRevenue") == ""){
		showERRMessage(revenuenull);
		$("tabReporting#txtRevenue").tabFocus('tabs');
	}else if (getFieldByID("radSelCurr").checked && getText("selCurrencyCode") == ""){		
		showERRMessage(selCurrencyNull);
		$("tabReporting#txtRevenue").tabFocus('tabs');
	}
	else if (hasTrnSettings() && minTrnDur == ""){
		showERRMessage(durNull);
		$("tabAddModCharge#minTranDuration_DD").tabFocus('tabs');
	}else if (hasTrnSettings() && maxTrnDur == ""){
			showERRMessage(durNull);
			$("tabAddModCharge#maxTranDuration_DD").focus();
	}/*else if(minTrnDur != "" && isDurationWrong()){
		 showERRMessage(DurIncco);
		 $("tabAddModCharge#hdnMinTrnDuration").tabFocus('tabs');
	}else if(maxTrnDur != "" && isDurationWrong()){
		 showERRMessage(DurIncco);
		 $("tabAddModCharge#hdnMaxTrnDuration").focus();
	}else if(hasTrnSettings() && maxTrnDur == "00:00"){			
		 showERRMessage(durZero);
		 $("tabAddModCharge#hdnMaxTrnDuration").tabFocus('tabs');
	}*/
	else if(getFieldByID("radNewRptCHCode").checked && strReportChargeGroupCode ==""){
		 showERRMessage(reportinChargeGroupCodeReq);
		 $("tabReporting#txtRptChargeGroupCode").tabFocus('tabs');
	}else if(getFieldByID("radNewRptCHCode").checked && strReportChargeGroupDescription==""){
		 showERRMessage(reportinChargeGroupDescReq);
		 $("tabReporting#txtRptChargeGroupDesc").tabFocus('tabs');
	}else if (hasTrnSettings() && minTrnDur != "" && maxTrnDur != "") {				
		if(minTrnDur > maxTrnDur){			
			showERRMessage(durMinExceedMax);
			$("tabAddModCharge#minTranDuration_DD").tabFocus('tabs');
			$("tabAddModCharge#maxTranDuration_DD").tabFocus('tabs');
		}else {
			validated = true;
		}	
	} else {
		validated = true;
	}	
	return validated;
}

function getTimeInMinitueValue(val){
	var D,H,m;
	if (val!= undefined){
		if(getValue(val + "TranDuration_DD") != "" && getValue(val + "TranDuration_DD") != "DD"){
			D = getValue(val + "TranDuration_DD");
		} else {
			D = 0;
		}
		
		if(getValue(val + "TranDuration_HR") != "" && getValue(val + "TranDuration_HR") != "hh"){
			H = getValue(val + "TranDuration_HR");
		} else {
			H = 0;
		}
		
		if(getValue(val + "TranDuration_MI") != "" && getValue(val + "TranDuration_MI") != "mm"){
			m = getValue(val + "TranDuration_MI");
		} else {
			m = 0;
		}
		
		var retVal = parseInt(D) * 24 * 60 + 
					 parseInt(H) * 60 +
					 parseInt(m);
	}
	return retVal;
}

function trnDurationError(duration) {
	 if(duration.length < 4){
		 return true;
	 }else{
		 return false;
	 }
}

function convertToMinutes(duration){
	var arrTime = duration.split(":");
	return (parseFloat(arrTime[0]) * 60 + parseFloat(arrTime[1]));
}

/*function isDurationWrong() {
	 
	 
	var strMinCR = getText("hdnMinTrnDuration");
	var blnMinVal = IsValidDateTime(trim(strMinCR));
	var strMaxCR = getText("hdnMaxTrnDuration");
	var blnMaxVal = IsValidDateTime(trim(strMaxCR));
	if (!blnMinVal || !blnMaxVal) {
		return true;
	}
	
}*/

function hasTrnSettings(){
	
	if((document.getElementById("chkInwardTrn").value !="N")
		 || (document.getElementById("chkOutwardTrn").value !="N")
			||  (document.getElementById("chkAllTrn").value !="N")){
		return true;
	}else{ // in JS needs else
		return false;
	}
	
}

function ctrl_Editclick() {	
	addClick = false;
	editClicked = true;
	top[2].HidePageMessage();
	flagRowclick = "AddEdit";
	var strCC = getText("txtChargeCode");
	
	
	// var strGroup = getText("AddSegselGroup");
	// if (strGroup == "SUR") {
	// Disable("chkRevenue", false);
	// Disable("txtRevenue", false);
		
	// } else {
	// Disable("chkRevenue", true);
	// Disable("txtRevenue", true);
	// }
	
	
	Disable("btnUpdate", "");	
	if (strCC == "") {
		showERRMessage(rowclickRqrd);
		disableInputControls("true");
		disableChargesData(true);

	} else {
		if (display == "1") {
			disableInputControls("true");
			disableInputControls("");
			Disable("btnGAdd", "");
			Disable("btnGEDit", "");
			Disable("btnGAdd", "");
			getFieldByID("txtDesc").focus();
			Disable("txtChargeCode", "true");
			setField("hdnChargeCode", getText("txtChargeCode"));
			setField("hdnMode", "Edit");
			setField("hdnRateMode", "Edit");
			rowsDisabled = arrData.length;
			Disable("btnDelete", "true");
			currencyClick();
		} else {
			Disable("btnGEDit", "");
			rowsDisabled = arrData.length;
			Disable("btnGAdd", "");
			Disable("btnSave", "");
			Disable("btnDelete", "true");
		}
	}
	var category = getText("radCat1");
	if(category == "GLOBAL" || category == "POS"){
		getFieldByID("chkAirportTax").checked =  false;
		Disable("chkAirportTax", true);
	} else if (category == "SERVICE_TAX"){
		getFieldByID("chkAirportTax").checked =  false;
		Disable("chkAirportTax", true);
		disableServiceTaxFields(false);
		Disable("chkServiceTaxInterState", false);
		Disable("chkServiceTaxTicketing", false);
		Disable("selGenerateReport", false);
		Disable("AddSegselGroup", true);	
	}
}

function ctrl_GEdit_click() {
	addClick = false;
	editClicked = true;
	setField("hdnRateMode", "Edit");
	editChargesData();
}

function grid_Add() {
	var raetMode = getText("hdnRateMode");
	if (raetMode == "Add") {
		var x = arrData.length;
		arrData[x] = new Array();
		arrData[x][0] = x;
		arrData[x][1] = "";
		arrData[x][2] = "";
		arrData[x][3] = "V";
		arrData[x][4] = "";
		arrData[x][5] = "";
		arrData[x][6] = "";
		arrData[x][7] = "";
		arrData[x][8] = "false";
		arrData[x][9] = "0";
		arrData[x][10] = "0";
		arrData[x][11] = "Added";
		arrData[x][12] = "";
		arrData[x][13] = "+x+";
		arrData[x][14] = "-1";
		arrData[x][15] = "";
		arrData[x][16] = "";
		arrData[x][17] = "";
		arrData[x][18] = "";
		arrData[x][19] = "";
		objDGMOD.arrGridData = arrData;
		objDGMOD.refresh();
		// objDGMOD.setCellValue(x, 8, "true");
		disablechargesOnEdit();
		// objDGMOD.setDisable(x, 4, true);
		// objDGMOD.setDisable(x, 5, true);
		objDGMOD.setDisable(x, 10, chargeRateOperationTypeDisable);
		objDGMOD.setDisable(x, 11, chargeRateJourneytTypeDisable);
		disableLocalCurrencyOnSelection(x);
		disableMinMaxColumns(x,true);

	} else {
		var x = arrData.length;
		arrData[x] = new Array();
		arrData[x][0] = x;
		arrData[x][1] = "";
		arrData[x][2] = "";
		arrData[x][3] = "V";
		arrData[x][4] = "";
		arrData[x][5] = "";
		arrData[x][6] = "";
		arrData[x][7] = "";
		arrData[x][8] = "false";
		arrData[x][9] = "0";
		arrData[x][10] = "0";
		arrData[x][11] = "Added";
		arrData[x][12] = "";
		arrData[x][13] = "+x+";
		arrData[x][14] = "-1";
		arrData[x][15] = "";
		arrData[x][16] = "";
		arrData[x][17] = "";
		arrData[x][18] = "";
		arrData[x][19] = "";
		objDGMOD.arrGridData = arrData;
		objDGMOD.refresh();
		// objDGMOD.setCellValue(x, 8, "true");
		// objDGMOD.setDisable(x, 4, true);
		// objDGMOD.setDisable(x, 5, true);
		objDGMOD.setDisable(x, 10, chargeRateOperationTypeDisable);
		objDGMOD.setDisable(x, 11, chargeRateJourneytTypeDisable);
		editChargesData();
	}
	if(getText("radCat1") != undefined && getText("radCat1") == "SERVICE_TAX"){
		chargeRateRestrictionsForServiceTax(true);
	}
}

function ctrl_GAdd_click() {
	// clearAll(); //removing this in request of janaki

	var arrChkData = objDGMOD.getSelectedColumn(9);
	
	if(arrData.length == 1){
	arrData[0][3] = objDGMOD.getCellValue(0, 4);
	arrData[0][4] = objDGMOD.getCellValue(0, 5);
	arrData[0][5] = objDGMOD.getCellValue(0, 6);
	arrData[0][6] = objDGMOD.getCellValue(0, 7);
	arrData[0][7] = objDGMOD.getCellValue(0, 8);
	arrData[0][8] = objDGMOD.getCellValue(0, 9);
	arrData[0][9] = objDGMOD.getCellValue(0, 10);
	arrData[0][10] = objDGMOD.getCellValue(0, 11);
	arrData[0][14] = objDGMOD.getCellValue(0, 12);
	arrData[0][15] = objDGMOD.getCellValue(0, 13);
	arrData[0][16] = objDGMOD.getCellValue(0, 14);	
	}
	
	setField("hdnRateMode", "Add");
	grid_Add();
	

	for ( var i = 0; i < arrChkData.length; i++) {
		var fromPrev, toPrev;
		var travelFrom, travelTo;
		var flagPrev, perValPrev, minPrev, maxPrev, localPrev, actPrev, ccCodePrev, breakPoint, boundryValue;
		
		// if (objDGMOD.getCellValue(i, 0) != "") {
		// fromPrev = dateChk(objDGMOD.getCellValue(i, 0));
		// } else {
			fromPrev = objDGMOD.getCellValue(i, 0);
		// }

		// if (objDGMOD.getCellValue(i, 1) != "") {
		// toPrev = dateChk(objDGMOD.getCellValue(i, 1));
		// } else {
			toPrev = objDGMOD.getCellValue(i, 1);
		// }
		
		
		// if (objDGMOD.getCellValue(i, 2) != "") {
			// travelFrom = dateChk(objDGMOD.getCellValue(i, 2));
		// } else {
			travelFrom = objDGMOD.getCellValue(i, 2);
		// }

		// if (objDGMOD.getCellValue(i, 3) != "") {
		// travelTo = dateChk(objDGMOD.getCellValue(i, 3));
		// } else {
			travelTo = objDGMOD.getCellValue(i, 3);
		// }
		
		flagPrev = objDGMOD.getCellValue(i, 4);
		perValPrev = objDGMOD.getCellValue(i, 5);
		minPrev = objDGMOD.getCellValue(i, 6);
		maxPrev = objDGMOD.getCellValue(i, 7);
		localPrev = objDGMOD.getCellValue(i, 8);
		actPrev = objDGMOD.getCellValue(i, 9);
		operationTypePrev = objDGMOD.getCellValue(i, 10);
		journeyTypePrev = objDGMOD.getCellValue(i, 11);
		ccCodePrev = objDGMOD.getCellValue(i, 12);
		breakPoint = objDGMOD.getCellValue(i, 13);
		boundryValue = objDGMOD.getCellValue(i, 14);
		
		objDGMOD.setCellValue(i, 0, fromPrev);
		objDGMOD.setCellValue(i, 1, toPrev);
		objDGMOD.setCellValue(i, 4, flagPrev);
		objDGMOD.setCellValue(i, 5, perValPrev);
		objDGMOD.setCellValue(i, 6, minPrev);
		objDGMOD.setCellValue(i, 7, maxPrev);
		objDGMOD.setCellValue(i, 8, localPrev);
		objDGMOD.setCellValue(i, 12, ccCodePrev);
		objDGMOD.setCellValue(i, 13, breakPoint);
		objDGMOD.setCellValue(i, 14, boundryValue);
		objDGMOD.setCellValue(i, 2, travelFrom);
		objDGMOD.setCellValue(i, 3, travelTo);
		
// if (flagPrev == "V") {
// objDGMOD.setDisable(i, 4, true);
// objDGMOD.setDisable(i, 5, true);
// }

		if (actPrev == true) {
			objDGMOD.setCellValue(i, 9, "true");
		} else {
			objDGMOD.setCellValue(i, 9, "false");
		}
		objDGMOD.setCellValue(i, 10, operationTypePrev);
		objDGMOD.setCellValue(i, 11, journeyTypePrev);
		
		objDGMOD.setDisable(i, 10, chargeRateOperationTypeDisable);
		objDGMOD.setDisable(i, 11, chargeRateJourneytTypeDisable);
		
	}
}

function ctrlSetStatus() {
	var arrChkData = objDGMOD.getSelectedColumn(9);
	for ( var i = 0; i < arrChkData.length; i++) {
		if (arrChkData[i][1] == true) {
			objDGMOD.setCellValue(i, 9, "true");
		} else {
			objDGMOD.setCellValue(i, 9, "false");
		}
	}
}

function setApplyTo(strCode) {
	var control = document.getElementById("selApplyTo");
	for ( var t = 0; t < (control.length); t++) {
		if (control.options[t].text == strCode) {
			control.options[t].selected = true;
			break;
		}
	}
}

function setOndSeg(strCode) {
	var control = document.getElementById("selOndSeg");
	for ( var t = 0; t < (control.length); t++) {
		if (control.options[t].text == strCode) {
			control.options[t].selected = true;
			break;
		}
	}
}


function disableLocalCurrencyOnSelection(row){
	var objRad = getFieldByID("radSelCurr");
	if(objRad.checked){
		objDGMOD.setDisable(row, 4, true); // value % flag
		objDGMOD.setDisable(row, 5, true); // value %
		objDGMOD.setDisable(row, 6, true); // min value
		objDGMOD.setDisable(row, 7, true); // max value
		objDGMOD.setDisable(row, 13, true); // breakpoint
		objDGMOD.setDisable(row, 14, true); // boundry
		
		objDGMOD.setCellValue(row, 6, "");
		objDGMOD.setCellValue(row, 7, "");
	}else{
		objDGMOD.setDisable(row, 4, false); // value % flag
		objDGMOD.setDisable(row, 5, false); // value %
		objDGMOD.setDisable(row, 6, false); // min value
		objDGMOD.setDisable(row, 7, false); // max value
		objDGMOD.setDisable(row, 13, false); // breakpoint
		objDGMOD.setDisable(row, 14, false); // boundry
	}
		
}


function ctrl_Addclick() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(ScreenId))) {
		addClick = true;
		rowClickEnable = false;
		editClicked = false;
		selectedAgents = "";
		applicableCharges ="";
		top[2].HidePageMessage();
		flagRowclick = "AddEdit";
		setApplyTo("All");
		setOndSeg("All");
		top[2].HidePageMessage();
		disableInputControls("");
		clearControls();
		
		Disable("btnGAdd", "");
		Disable("btnGEDit", "true");
		Disable("btnDelete", "true");
		Disable("btnEdit", "true");
		setField("chkStatus", true);
		setField("hdnRateMode", "Add");
		setField("hdnMode", "Add");
		setField("hdnVersion", "");
		setField("hdnRateVersion", "");
		setField("chkRefundable", true);
		setField("chkRefundableWhenExchange", false);
		setField("chkRefundableOnlyMOD", false);
		if(enableExcludeChargesFromFQ == "true"){
			setField("chkExcludeFromFareQuote", false);
		}
		getFieldByID("radDept1").checked = true;
		getFieldByID("radInc1").checked = true;
		getFieldByID("radAgtInc1").checked = true;
		Disable("btnUpdate", "");
		Disable("chkRevenue", true);
		Disable("txtRevenue", true);
		// setVisible("spnRevenue", false);
		// enable grid add/edit
		categoryClick();
		checkAirportTax();
		currencyClick();
		getFieldByID("txtChargeCode").focus();
		
		chargeRateOperationTypeDisable = true;
		chargeRateJourneytTypeDisable = true ;		
		
		disableChargesData(false);
	    disableMinMaxColumns(0, true);
		setPageEdited(false);
		// disableLocalCurrencyOnSelection(1);
		setField("chkInwardTrn", "N");
		setField("chkOutwardTrn", "N");
		setField("chkAllTrn", "N");		
		
		setField("minTranDuration_DD", "");
		setField("minTranDuration_HR", "");
		setField("minTranDuration_MI", "");
		setField("maxTranDuration_DD", "");
		setField("maxTranDuration_HR", "");
		setField("maxTranDuration_MI", "");
		
		inputBlur(getFieldByID("minTranDuration_DD") , "DD");
		inputBlur(getFieldByID("minTranDuration_HR") , "hh");
		inputBlur(getFieldByID("minTranDuration_MI") , "mm");
		inputBlur(getFieldByID("maxTranDuration_DD") , "DD");
		inputBlur(getFieldByID("maxTranDuration_HR") , "hh");
		inputBlur(getFieldByID("maxTranDuration_MI") , "mm");
		
		getFieldByID("radRptChgCode").checked = true;
		disableServiceTaxFields(false);
		
		setTimeout('loadList()', 1000);
	}
}

function loadList(){
	lssta.drawListBox();
	//lsagt.drawListBox();
	lsCharge.drawListBox();
}

function ctrl_Deleteclick() {
	top[2].HidePageMessage();
	var status = confirm(deleteMessage);
	if (status == true) {
		setField("hdnMode", "DELETE");
		var strSearch = getSearchData();
		setField("hdnRecNo", "1");
		top[1].objTMenu.tabSetValue(ScreenId, strSearch + "~1");
		document.forms["frmManageCharges"].action = "";
		document.forms["frmManageCharges"].submit();
	}
}

function test_Save() {
	var size = arrData.length;
	var validated = true;

	if (!getChargesData()) {
		validated = false;
		return false;
	} else if(!areChargeRateDatesValid()) {
				validated = false;
				return false;		
	    }else {
			var str = "";
			var t = 1;
			for ( var i = 0; i < size; i++) {
				var effFrom, effTo , operationType , journeyType ,cClass, active,salesFrom,salesTo ;
			
			effFrom = objDGMOD.getCellValue(i, 0);
			effTo = objDGMOD.getCellValue(i, 1);			
			active = objDGMOD.getCellValue(i, 9);
			operationType = objDGMOD.getCellValue(i, 10);
			journeyType = objDGMOD.getCellValue(i, 11);
			cClass = objDGMOD.getCellValue(i, 12);
			
			if (checkChagesDates(effFrom, effTo, operationType ,journeyType , cClass ,  active , i)) {
				showERRMessage(rateOverlaps);
				objDGMOD.setColumnFocus(i, 0);
				validated = false;
				return false;
			}
		
		}
	}
	return validated;
}

function save_click() {	
	if(checkedBundleFare && !ifSurchargeSelect() ){
		showERRMessage(groupInactive);
		return;
	}
	if (!saveData()) {		
		return;
	} else if (!test_Save()) {		
		return;
	} else {		
		setField("hdnStations", lssta.getselectedData());
		var strCat = getText("radCat1");
		var strAgt = "";
		if ((strCat == "POS" || strCat == "OND") && lsagt != undefined && lsagt.getselectedData() != undefined) {
			if(lsagt.getselectedData().length > 0) {
				strAgt = lsagt.getselectedData();
			}
			else if(selectedAgents.length > 0 && !isAgentsSelected) {
				strAgt = selectedAgents;
			}
		}
		setField("hdnAgents", strAgt);
		setField("hdnCharges", lsCharge.getselectedData());	
		setField("hdnChargeCode", getText("txtChargeCode"));		
		// setField("hdnRecNo",(top[0].intLastRec));
		setField("hdnRecNo", "1");		
		var strSearch = getSearchData();		
		setField("hdnRecNo", "1");		
		top[1].objTMenu.tabSetValue(ScreenId, strSearch + "~1");		
		setField("hdnMode", "SAVE");
		document.forms["frmManageCharges"].submit();
		top[2].ShowProgress();
	}
}

function KPValidatePositiveInteger(control) {
	var strCC = getText(control);
	var strLen = strCC.length;
	var blnVal = validateInteger(strCC)
	if (!blnVal) {
		setField(control, strCC.substr(0, strLen - 1));
		getFieldByID(control).focus();
	}
}
function checkAirportTax(){
	if (getFieldByID("chkAirportTax").checked){
		setVisible("selAirportTaxType", true);
	}else{
		setVisible("selAirportTaxType", false);
	}
}

function airportTaxTypeChanged(){
	// TODO do the enabling disabling of transit tax based on the selection
}

function enableCookieRelatedFields(cond){
	setVisible("searchDays",cond);
	setVisible("txtMinNoOfDays",cond);
	setVisible("txtMaxNoOfDays",cond);
	setVisible("radCookieBased", cond);
	setVisible("lblCookieBased", cond);
}

function showServiceTaxFields(cond){
	setVisible("spnServiceTax",cond);
	setVisible("selCountry",cond);
	setVisible("selGenerateReport",cond);
	setVisible("spnCharge", cond);
	setVisible("rowSelState", cond);
	setVisible("rowInterState", cond);
}

function disableServiceTaxFields(cond){
	Disable("spnCharge",cond);
	Disable("selState",cond);
	Disable("selCountry",cond);
	Disable("selGenerateReport", cond);
	Disable("chkServiceTaxInterState", cond);
	Disable("chkServiceTaxTicketing", cond);
}

function categoryClick() {
	// setPageEdited(true);
	setVisible("spnSegment", false);
	setVisible("spnPOS", false);
	setVisible("spnCharge", false);
	setVisible("spnONDAgent", false);
	setVisible("selGenerateReport", false);
	var objRad1 = getFieldByID("radCat1");
	var objRad2 = getFieldByID("radPos");
	var objRad3 = getFieldByID("radSegment");
	var objRad4 = getFieldByID("radCookieBased");
	var objRad5 = getFieldByID("radServiceTax");
	showServiceTaxFields(false);
	disableServiceTaxFields(true);
	Disable("chkBundledFareCharge", false);	
	Disable("chkRefundable", false);	
	Disable("chkRefundableWhenExchange", false);
	Disable("chkRefundableOnlyMOD", false);
	chargeRateRestrictionsForServiceTax(false);
	Disable("AddSegselGroup",false);
	Disable("selOndSeg",false);

	if (objRad2.checked) {
		getFieldByID("chkAirportTax").checked =  false;
		Disable("chkAirportTax", true);
		checkAirportTax();
		setVisible("spnPOS", true);
		setVisible("spnONDAgent", true);
		isPOS = true;
		isOND = false;
		setVisible("spnSegment", true);
		enableCookieRelatedFields(false);
		if(getText("AddSegselGroup") == "SUR" && isAllowAssCookieCharges && top.arrPrivi['plan.fares.cookie.charge.rate.add'] == 1){
			setVisible("radCookieBased", true);
			setVisible("lblCookieBased", true);
			setField("txtMinNoOfDays","");
			setField("txtMaxNoOfDays","");
		}
	} else if (objRad3.checked) {
		Disable("chkAirportTax", false);
		checkAirportTax();
		setVisible("spnONDAgent", true);
		isPOS = false;
		isOND = true;
		
		setVisible("spnSegment", true);
		enableCookieRelatedFields(false);
		if(getText("AddSegselGroup") == "SUR" && isAllowAssCookieCharges && top.arrPrivi['plan.fares.cookie.charge.rate.add'] == 1){
			setVisible("radCookieBased", true);
			setVisible("lblCookieBased", true);
			setField("txtMinNoOfDays","");
			setField("txtMaxNoOfDays","");
		}
	} else if(objRad4.checked){
		enableCookieRelatedFields(true);
		setField("selChannel","4");
		Disable("selChannel",true);
	} else if(objRad5.checked){
		if(addClick){
			alert(serviceTaxCategoryMessage);
		}
		$('#AddSegselGroup').val("1");
		Disable("AddSegselGroup",true);
		var segSelGrp = getFieldByID("AddSegselGroup");
		var selIndex = segSelGrp.selectedIndex;
		generateAppliesToOption(segSelGrp.options[selIndex].text);
		
		getFieldByID("chkBundledFareCharge").checked =  false;	
		getFieldByID("chkRefundable").checked =  false;
		getFieldByID("chkRefundableWhenExchange").checked =  false;
		getFieldByID("chkRefundableOnlyMOD").checked =  false;
		
		Disable("chkBundledFareCharge", true);
		Disable("chkRefundableOnlyMOD", true);
		Disable("chkRefundable", true);
		Disable("chkRefundableWhenExchange", true);
		Disable("selOndSeg",true);
		
		showServiceTaxFields(true);	
		chargeRateRestrictionsForServiceTax(true);
		
		if(editClicked){
			disableServiceTaxFields(false);	
		} else {
			if(rowClickEnable){
				disableServiceTaxFields(true);	
			} else {
				disableServiceTaxFields(false);	
			}
		}
	} else {
		getFieldByID("chkAirportTax").checked =  false;
		Disable("chkAirportTax", true);
		checkAirportTax();
		setVisible("spnSegment", false);
		setVisible("spnONDAgent", false);
		setVisible("spnPOS", false);
		isPOS = false;
		isOND = false;
		enableCookieRelatedFields(false);
		if(getText("AddSegselGroup") == "SUR" && isAllowAssCookieCharges && top.arrPrivi['plan.fares.cookie.charge.rate.add'] == 1){
			setVisible("radCookieBased", true);
			setVisible("lblCookieBased", true);
			setField("txtMinNoOfDays","");
			setField("txtMaxNoOfDays","");
		}
		clearAll();
	}
}

function currencyClick() {

	setVisible("selCurrencyCode", false);
	var objRad1 = getFieldByID("radCurr");
	var objRad2 = getFieldByID("radSelCurr");

	if (objRad2.checked) {
		setVisible("selCurrencyCode", true);
		Disable("selCurrencyCode", false);
		
	} 
	else if (objRad1.checked) {
		setVisible("selCurrencyCode", false);
		setField("selCurrencyCode", "-1");
	}
	var mode = getText("hdnMode")
	if(mode == "Add"){
		disableLocalCurrencyOnSelection(0);
	}
	else{
		// disableLocalCurrencyOnSelection(1);
	}
		

}

function testDate(dt) {
	var validated = true;

	var dtC = new Date();
	var dtCM = dtC.getMonth() + 1;
	var dtCD = dtC.getDate();
	if (dtCM < 10) {
		dtCM = "0" + dtCM
	}
	if (dtCD < 10) {
		dtCD = "0" + dtCD
	}
	var strSysDate = systemDate;
	var valid = dateChk(strSysDate);

	var tempDay = dt.substring(0, 2);
	var tempMonth = dt.substring(3, 5);
	var tempYear = dt.substring(6, 10);
	var tempOStartDate = (tempYear + tempMonth + tempDay);

	tempDay = strSysDate.substring(0, 2);
	tempMonth = strSysDate.substring(3, 5);
	tempYear = strSysDate.substring(6, 10);
	var strSysODate = (tempYear + tempMonth + tempDay);

	if (valid) {
		// if(CheckDates(dt,strSysDate)){
		if (tempOStartDate >= strSysODate) {
			validated = false;
		}
	}
	return validated;
}

// Reset Button Click
function resetClick() {

	var strMode = getText("hdnMode");
	top[2].HidePageMessage();
	if (strMode == "Edit") {
		getFieldByID("txtDesc").focus();
		top[2].HidePageMessage();
		setFields(rowData);
		categoryClick();
		checkAirportTax();
		Disable("chkRefundable", "");
		Disable("selApplyTo", "");
		Disable("selOndSeg", "");
		Disable("selChannel", "");
		Disable("chkStatus", "");
		Disable("radRptChgCode", "true");
		Disable("txtRptChargeGroupCode", true);
		Disable("txtRptChargeGroupDesc", true);
	} else if (strMode == "Add") {
		getFieldByID("txtChargeCode").focus();
		clearControls();
		categoryClick();
		checkAirportTax();
		setField("chkRefundable", true);
		setField("selApplyTo", "All");
		Disable("selOndSeg", "All");
		Disable("selChannel", "-1");
		setField("chkStatus", true);
		disableChargesData(false);
	} else {
		clearControls();
		categoryClick();
		checkAirportTax();
		setField("chkRefundable", true);
		setField("selApplyTo", "All");
		setField("selOndSeg", "All");
		setField("selChannel", "-1");
		setField("chkStatus", true);
		disableChargesData(true);
		disableInputControls("true");
	}
}

function required_changed() {
	setPageEdited(true);

}

function addToList(str, r) {
	var isContained = false;
	top[2].HidePageMessage();
	var control = document.getElementById("selSelected");
	control.options[r] = new Option(str, str);

}

function btnAddSegment_click() {
	var isContained = false;
	top[2].HidePageMessage();
	var dept = getText("selDepature");
	var arr = getText("selArrival");
	var via1 = getText("selVia1");
	var via2 = getText("selVia2");
	var via3 = getText("selVia3");
	var via4 = getText("selVia4");

	var deptVal = getValue("selDepature");
	var arrVal = getValue("selArrival");
	var via1Val = getValue("selVia1");
	var via2Val = getValue("selVia2");
	var via3Val = getValue("selVia3");
	var via4Val = getValue("selVia4");

	if (dept == "") {
		showERRMessage(depatureRqrd);
		getFieldByID("selDepature").focus();

	} else if (dept != "" && deptVal == "INA") {
		showERRMessage(departureInactive);
		getFieldByID("selDepature").focus();

	} else if (arr == "") {
		showERRMessage(arrivalRqrd);
		getFieldByID("selArrival").focus();

	} else if (arr != "" && arrVal == "INA") {
		showERRMessage(arrivalInactive);
		getFieldByID("selArrival").focus();

	} else if (dept == arr && dept != "All") {
		showERRMessage(depatureArriavlSame);
		getFieldByID("selArrival").focus();

	} else if (dept == arr && dept == "All" && via1 == "") {
		showERRMessage(viaPointRqrd);
		getFieldByID("selVia1").focus();

	} else if (arr == via1 || arr == via2 || arr == via3 || arr == via4) {
		showERRMessage(arriavlViaSame);
		getFieldByID("selArrival").focus();

	} else if (dept == via1 || dept == via2 || dept == via3 || dept == via4) {
		showERRMessage(depatureViaSame);
		getFieldByID("selDepature").focus();

	} else if ((via2 != "" || via3 != "" || via4 != "") && via1 == "") {
		showERRMessage(vianotinSequence);
		getFieldByID("selVia1").focus();

	} else if ((via3 != "" || via4 != "") && via2 == "") {
		showERRMessage(vianotinSequence);
		getFieldByID("selVia1").focus();

	} else if ((via4 != "") && via3 == "") {
		showERRMessage(vianotinSequence);
		getFieldByID("selVia1").focus();

	} else if ((via1 != "" && via2 != "" && via3 != "")
			&& (via1 == via2 || via1 == via3 || via1 == via4)) {
		showERRMessage(viaEqual);
		getFieldByID("selVia1").focus();

	} else if ((via1 != "" && via2 != "" && via3 != "")
			&& (via2 == via3 || via2 == via4)) {
		showERRMessage(viaEqual);
		getFieldByID("selVia2").focus();

	} else if ((via1 != "" && via2 != "" && via3 != "") && (via3 == via4)) {
		showERRMessage(viaEqual);
		getFieldByID("selVia4").focus();

	} else if ((via1 != "" && via2 != "") && via1 == via2) {
		showERRMessage(viaEqual);
		getFieldByID("selVia1").focus();

	} else if (via1 != "" && via1Val == "INA") {
		showERRMessage(via1 + " " + viaInactive);
		getFieldByID("selVia1").focus();
	} else if (via2 != "" && via2Val == "INA") {
		showERRMessage(via2 + " " + viaInactive);
		getFieldByID("selVia2").focus();
	} else if (via3 != "" && via3Val == "INA") {
		showERRMessage(via3 + " " + viaInactive);
		getFieldByID("selVia3").focus();
	} else if (via4 != "" && via4Val == "INA") {
		showERRMessage(via4 + " " + viaInactive);
		getFieldByID("selVia4").focus();
	} else {
		var str = "";
		str += dept;

		if (via1 != "") {
			str += "/" + via1;
		}
		if (via2 != "") {
			str += "/" + via2;
		}
		if (via3 != "") {
			str += "/" + via3;
		}
		if (via4 != "") {
			str += "/" + via4;
		}
		str += "/" + arr;

		var control = document.getElementById("selSelected");
		for ( var r = 0; r < control.length; r++) {
			if (control.options[r].text == str) {
				isContained = true;
				showERRMessage(ondRequired);
				break;
			}
		}
		if (isContained == false) {
			control.options[control.length] = new Option(str, str);
		}
	}
}

function btnRemoveSegment_click() {
	var control = document.getElementById("selSelected");

	var selIndex = control.selectedIndex;
	if (selIndex != -1) {
		for (i = control.length - 1; i >= 0; i--) {
			if (control.options[i].selected) {
				control.options[i] = null;
			}
		}
		if (control.length > 0) {
			control.selectedIndex = selIndex == 0 ? 0 : selIndex - 1;
		}
	}

}

function clearAll() {
	var control = document.getElementById("selSelected");
	control.options.length = 0;
	for ( var t = 0; t <= (control.length); t++) {
		control.options[t] = null;
	}

}

function getAll() {
	var control = document.getElementById("selSelected");
	var values = "";
	for ( var t = 0; t < (control.length); t++) {
		var str = control.options[t].text;
		var strReplace = control.options[t].text;
		if (str.indexOf("All") != -1) {
			strReplace = str.replace(/All/g, "***");
		}
		values += strReplace + ",";
	}
	return values;
}

function setWithoutCharges() {
	var rateCat = getText("radRateCat");
	if (rateCat == "NoCharges") {
		Disable("txtDateFrom", "true");
		Disable("txtDateTo", "true");
	} else if (rateCat == "WithDateRange") {
		setField("txtDateFrom", "");
		setField("txtDateTo", "");
		Disable("txtDateFrom", "");
		Disable("txtDateTo", "");
	}
}

function depatureClicked() {

}
function PopUpData() {
	

	// if (isPOS == true)
	// {
 
		 var html = '';
	html += '<table width="99%" align="center" border="0" cellpadding="1" cellspacing="0" style="background-color: #5A5C63;">';
	html += '<tr><td>';
	html += '<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" ID="Table4">';
	html += '<tr><td width="10" height="20"><img src="../../images/form_top_left_corner_no_cache.gif"></td>';
	html += '<td height="20" class="FormHeadBackGround"><img src="../../images/bullet_small_no_cache.gif"> <font class="FormHeader">';
	html += 'Select POS';
	html += '</font></td><td width="11" height="20"><img src="../../images/form_top_right_corner_no_cache.gif"></td>';
	html += '</tr><tr><td class="FormBackGround"></td><td class="FormBackGround" valign="top">';
	html += '<table><tr><td align="left">';
	html += '<input type="radio" id="radInc1" name="radInc1" value="Include"  class="NoBorder"><font>Include / </font>';
	html += '<input type="radio" id="radInc2" name="radInc1" value="Exclude"  class="NoBorder"><font>Exclude the selected stations</font>';
	html += '</td></tr>';
	html += '<tr><td align="top"><span id="spnSta"></span></td></tr>';
	html += '<tr><td align="right"><input type="button" id="btnUpdate" value= "Update" class="Button" onClick="return POSdisable();">&nbsp;<input type="button" id="btnPopupClose" value= "Close" class="Button" onClick="return POSdisable();"></td></tr></table>';
	html += '</td><td class="FormBackGround"></td></tr><tr>';
	html += '<td height="12"><img src="../../images/form_bottom_left_corner_no_cache.gif"></td>';
	html += '<td height="12" class="FormBackGround"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td>';
	html += '<td height="12"><img src="../../images/form_bottom_right_corner_no_cache.gif"></td>';
	html += '</tr></table>';
	html += '</td></tr></table>';
	 
	DivWrite("pop", html);
	
	// }
	 
	 // if (isOND == true || isPOS == true)
	// {

		 var html = '';
		 html += '<table width="99%" align="center" border="0" cellpadding="1" cellspacing="0" style="background-color:#5A5C63;">'; 
	 html += '<tr><td>';
	 html += '<table  width="100%" cellpadding="0" cellspacing="0" border="0" align="center" ID="Table4">';
	 html += '<tr><td width="10" height="20"><img src="../../images/form_top_left_corner_no_cache.gif"></td>';
	 html += '<td height="20" class="FormHeadBackGround"><img src="../../images/bullet_small_no_cache.gif"> <font class="FormHeader">';
	 html += 'Select OND';
	 html += '</font></td><td width="11" height="20"><img src="../../images/form_top_right_corner_no_cache.gif"></td>'; 
	 html += '</tr><tr><td class="FormBackGround"></td><td class="FormBackGround" valign="top">';
	 html += '<table><tr><td align="left">'; 
	 html += '<input type="radio" id="radAgtInc1" name="radAgtInc1" value="Include"  class="NoBorder"><font>Include / </font>';
	 html += '<input type="radio" id="radAgtInc2" name="radAgtInc1" value="Exclude"  class="NoBorder"><font>Exclude the selected agents</font>';
	 html += '</td></tr>';
	 html += '<tr><td align="top"><span id="spnAgents"></span></td></tr>'; 
	 html += '<tr><td align="right"><input type="button" id="btnUpdate" value= "Update" class="Button" onClick="return ONDAgentdisable();">&nbsp;<input type="button" id="btnPopupClose" value= "Close" class="Button" onClick="return ONDAgentdisable();"></td></tr></table>'; 
	 html += '</td><td class="FormBackGround"></td></tr><tr>';
	 html += '<td height="12"><img src="../../images/form_bottom_left_corner_no_cache.gif"></td>'; 
	 html += '<td height="12" class="FormBackGround"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td>'; 
	 html += '<td height="12"><img src="../../images/form_bottom_right_corner_no_cache.gif"></td>'; 
	 html += '</tr></table>';
	 html += '</td></tr></table>'; 
	 DivWrite("popagent", html);
	 
	 var html = '';
	 html += '<table width="99%" align="center" border="0" cellpadding="1" cellspacing="0" style="background-color:#5A5C63;">'; 
	 html += '<tr><td>';
	 html += '<table  width="100%" cellpadding="0" cellspacing="0" border="0" align="center" ID="Table4">';
	 html += '<tr><td width="10" height="20"><img src="../../images/form_top_left_corner_no_cache.gif"></td>';
	 html += '<td height="20" class="FormHeadBackGround"><img src="../../images/bullet_small_no_cache.gif"> <font class="FormHeader">';
	 html += 'Select Charges';
	 html += '</font></td><td width="11" height="20"><img src="../../images/form_top_right_corner_no_cache.gif"></td>'; 
	 html += '</tr><tr><td class="FormBackGround"></td><td class="FormBackGround" valign="top">';
	 html += '<table><tr><td align="left">';
	 html += '<font>Exclude the selected Charges</font>';
	 html += '</td></tr>';
	 html += '<tr><td align="top"><span id="spnPopCharges"></span></td></tr>'; 
	 html += '<tr><td align="right"><input type="button" id="btnUpdate" value= "Update" class="Button" onClick="return ChargeDisable();">&nbsp;<input type="button" id="btnPopupClose" value= "Close" class="Button" onClick="return ChargeDisable();"></td></tr></table>'; 
	 html += '</td><td class="FormBackGround"></td></tr><tr>';
	 html += '<td height="12"><img src="../../images/form_bottom_left_corner_no_cache.gif"></td>'; 
	 html += '<td height="12" class="FormBackGround"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td>'; 
	 html += '<td height="12"><img src="../../images/form_bottom_right_corner_no_cache.gif"></td>'; 
	 html += '</tr></table>';
	 html += '</td></tr></table>'; 
	 DivWrite("popCharges", html);
	 
}

function POSenable() {
	 isPOS = true;
	 isOND = false;
	
	 setVisible("pop", true);
	 // showPopUp(1);
}

function POSdisable() {
	// hidePopUp(1);
	 isPOS = false;
	 setVisible("pop", false);
}


 function ONDAgentenable() {
	 isOND = true; 
	 isPOS = false; 
	 // showPopUp(2);
	 top[2].ShowProgress();
	 lsagt.drawListBox();
	 lsagt.selectedData(selectedAgents);
	 top[2].HideProgress();
	 setVisible("popagent", true);
	 }
  
 function ONDAgentdisable() {
	 isAgentsSelected =true;
	 // hidePopUp(2);
	 isOND = false;
	 setVisible("popagent", false);
}
 
 function ChargeEnable() {
	 isPOS = false;
	 isOND = false;

	 lsCharge.selectedData(applicableCharges)
	 setVisible("popCharges", true);
	 }
  
 function ChargeDisable() {
	 applicableCharges = lsCharge.getselectedData();
	 setVisible("popCharges", false);
}
 
 function appendOptionLast(textP,valueP) {
   var elOptNew = document.createElement('option');
   elOptNew.text = textP;
   elOptNew.value = valueP;
   var elSel = document.getElementById('selApplyTo');

   try {
     elSel.add(elOptNew, null); // standards compliant; doesn't work in IE
   }
   catch(ex) {
     elSel.add(elOptNew); // IE only
   }
 }
 
 function generateAppliesToOption(valGroup){	 
	 	getFieldByID("selApplyTo").options.length = 0;
		var fullAppliesToList= getValue("hdnAppliesToList");
		
		var appliesToFullArray = fullAppliesToList.split("^");

		for(var i=0;i<appliesToFullArray.length;i++){
			var appliesToParamArray = appliesToFullArray[i].split("|");			
			
			if(valGroup == 'ESU' && appliesToParamArray[0]=='6'){
				appendOptionLast(appliesToParamArray[1],appliesToParamArray[0]);
			}else if(appliesToParamArray[0]!='6'){
				appendOptionLast(appliesToParamArray[1],appliesToParamArray[0]);
			}			
	
		}
 }
 function checkChargeRateDateTime(intRowIndex, intColNo, control) {
		var strEnteredDateTime = control.value;
		var strFormattedDate = "";
		var isValid = false;

		// FIXME - BEGIN - Implement Data+Time validation
		if (strEnteredDateTime != null && strEnteredDateTime.length > 0) {

			strEnteredDateTime = replaceall(strEnteredDateTime, "/", "");
			strEnteredDateTime = replaceall(strEnteredDateTime, "-", "");
			strEnteredDateTime = replaceall(strEnteredDateTime, ".", "");
			strEnteredDateTime = replaceall(strEnteredDateTime, ":", "");
			strEnteredDateTime = replaceall(strEnteredDateTime, " ", "");

			if ((strEnteredDateTime.length <= 12) && !(isNaN(strEnteredDateTime))) {
				var dateInput = getSystemCurrentDateTime();

				if (trim(strEnteredDateTime) == "") {
					// nothing to do
					
				} else if (strEnteredDateTime.length <= 2) {
					dateInput.setDate(strEnteredDateTime);				 
				
				} else if (strEnteredDateTime.length <= 4) {
					// Reset Date
					dateInput.setMonth(0);
					dateInput.setDate(1);
									
					dateInput.setMonth(Number(strEnteredDateTime.substr(2)) - 1);
					dateInput.setDate(strEnteredDateTime.substr(0, 2));
					
				} else if (strEnteredDateTime.length <= 6) {
					// Reset Date
					dateInput.setMonth(0);
					dateInput.setDate(1);
					
					dateInput.setFullYear(Number(strEnteredDateTime.substr(4)) + 2000);
					dateInput.setMonth(Number(strEnteredDateTime.substr(2, 2)) - 1);
					dateInput.setDate(strEnteredDateTime.substr(0, 2));
				} else if (strEnteredDateTime.length <= 8) {
					// Reset Date
					dateInput.setMonth(0);
					dateInput.setDate(1);
					
					dateInput.setFullYear(strEnteredDateTime.substr(4));
					dateInput.setMonth(Number(strEnteredDateTime.substr(2, 2)) - 1);
					dateInput.setDate(strEnteredDateTime.substr(0, 2));
				} else if (strEnteredDateTime.length <= 10) {
					// Reset Date
					dateInput.setMonth(0);
					dateInput.setDate(1);
					
					dateInput.setHours(strEnteredDateTime.substr(8));
					dateInput.setFullYear(strEnteredDateTime.substr(4,4));
					dateInput.setMonth(Number(strEnteredDateTime.substr(2, 2)) - 1);
					dateInput.setDate(strEnteredDateTime.substr(0, 2));
			
				}else if (strEnteredDateTime.length <= 12) {
					// Reset Date
					dateInput.setMonth(0);
					dateInput.setDate(1);
					
					dateInput.setMinutes(strEnteredDateTime.substr(10));
					dateInput.setHours(strEnteredDateTime.substr(8,2));					
					dateInput.setFullYear(strEnteredDateTime.substr(4,4));
					dateInput.setMonth(Number(strEnteredDateTime.substr(2, 2)) - 1);
					dateInput.setDate(strEnteredDateTime.substr(0, 2));
				}
                 
				if (strEnteredDateTime.length < 12) {
					
						if (isCurrentDateEntered(strEnteredDateTime,currentSystemDateAndTime)						
							&& ((intColNo == 1) ||(intColNo == 17))){	
							
							strFormattedDate = dateToStringWithTime(dateInput);		
							
						} else{
														
							if ((intColNo == 1) ||(intColNo == 17)) {
								dateInput.setHours(0);
								dateInput.setMinutes(0);
								strFormattedDate =dateToStringWithTime(dateInput);
			
							} else {
								dateInput.setHours(23);
								dateInput.setMinutes(59);
								strFormattedDate = dateToStringWithTime(dateInput);						
							}
						}
												
				} else {					
					strFormattedDate = dateToStringWithTime(dateInput);
				}

				arrData[intRowIndex][intColNo] = strFormattedDate;
				objDGMOD.arrGridData = arrData;
				control.value = strFormattedDate;
				isValid = true;
			
			} else{
				isValid = false;
			}
				
			
		}
		
		if (!isValid) {
			control.focus();
		}
		return isValid;
	}
 
 function setCurrentSystemTimeAndDate() {
		var arrDateAndTime = currentSystemDateAndTime.split(" ");
		var arrYearMonthDate = arrDateAndTime[0].split("/");
		var arrHrsMinsec = arrDateAndTime[1].split(":");
		currentSystemDateAndTime = new Date(Number(arrYearMonthDate[2]), Number(arrYearMonthDate[1]) - 1,
				Number(arrYearMonthDate[0]), Number(arrHrsMinsec[0]), Number(arrHrsMinsec[1]),
				Number(arrHrsMinsec[2]));
	}
 
 function convertStringToDate(dateString){		
	 try{
	    var convertedDate;
	    var arrDateAndTime = dateString.split(" ");
		var arrYearMonthDate = arrDateAndTime[0].split("/");
		var arrHrsMin = arrDateAndTime[1].split(":");
		
		convertedDate = new Date(Number(arrYearMonthDate[2]), Number(arrYearMonthDate[1]) - 1,
				Number(arrYearMonthDate[0]), Number(arrHrsMin[0]), Number(arrHrsMin[1]),0);
		
			if (!isNaN(convertedDate)) {
				return convertedDate;
			}else{
				showCommonError("Error", formatincorrect);
				return ;
			}							
	 }catch (e) {
			showCommonError("Error", formatincorrect);
			return;
		}	 
 }
 
 function isDatePast(passedDate){
	 if(passedDate > currentSystemDateAndTime){
	   return false;
	   } else{
	     return true;
	 }
 }
 
 function areChargeRateDatesValid(){
	 var formDate;
	 var toDate;
	 var isValid = true;
	 var rowNum;
	 for ( var i = 0; i < arrData.length; i++) {		 
		 rownum = i+1;
		 if((arrData[i][11]=="Edit") ||(arrData[i][11]=="Added") ){
			 fromDate = convertStringToDate(objDGMOD.getCellValue(i, 0));
			 toDate = convertStringToDate(objDGMOD.getCellValue(i, 1));
			 if(isDatePast(fromDate) || isDatePast(toDate) ) {
				 showERRMessage(datesExpired + " in row " + rownum);
				 return false;				
				}			 
		 }else if(arrData[i][11]=="EditTo"){
			 toDate = convertStringToDate(objDGMOD.getCellValue(i, 1));
			 if(isDatePast(toDate)) {
				 showERRMessage(datesExpired + " in row " + rownum);
				 return false;				
				}				 
		 }
	}
	 return isValid;
 }
 
 function compareDates( startDateStr,endDateStr){	
	 var formattedStartDate = convertStringToDate(startDateStr);
	 var formattedEndDate = convertStringToDate(endDateStr);
	 
	 if ( formattedEndDate > formattedStartDate){
		return true;
	}else{
	    return false;
	}
 }
 
 function compareDatesForOverLapLogic( startDateStr,endDateStr){	
	 var formattedStartDate = convertStringToDate(startDateStr);
	 var formattedEndDate = convertStringToDate(endDateStr);
	 
	 if ( formattedEndDate > formattedStartDate){
		return true;
	}else{
	    return false;
	}
 }
 
 function getSystemCurrentDateTime(){
	 var tempCurrentTime = new Date();
	 tempCurrentTime.setFullYear(currentSystemDateAndTime.getFullYear());
	 tempCurrentTime.setMonth(currentSystemDateAndTime.getMonth());
	 tempCurrentTime.setDate(currentSystemDateAndTime.getDate());
	 tempCurrentTime.setHours(currentSystemDateAndTime.getHours());
	 tempCurrentTime.setMinutes(currentSystemDateAndTime.getMinutes()+3);	  
	 return tempCurrentTime;
 }
 
 // Check whether the user is attempting to enter the current date
 function isCurrentDateEntered(enteredDateString, currentDate){
	 
	 if(enteredDateString.length <= 2){
		 if(enteredDateString == currentDate.getDate()){
			 return true;
		
		 } else {
			 return false;
		   }
		 
	 } else if (enteredDateString.length <= 4){
		 if((enteredDateString.substr(0,2)==currentDate.getDate())
			&& (enteredDateString.substr(2)==(currentDate.getMonth()+1))	){
			 return true;
		
		 } else {
			 return false;
		   }
		 
	 }else if (enteredDateString.length <= 8){
		 if( (enteredDateString.substr(0,2)==currentDate.getDate())
			&& (enteredDateString.substr(2,2)==(currentDate.getMonth()+1))
			&& (enteredDateString.substr(4)==currentDate.getFullYear()) ){
			return true;
		  
		 } else {
			 return false;
		   }
		 
	 }else if (enteredDateString.length <= 10){
		 if( (enteredDateString.substr(0,2)==currentDate.getDate())
			 && (enteredDateString.substr(2,2)==(currentDate.getMonth()+1))
			 && (enteredDateString.substr(4,4)==currentDate.getFullYear())
			 && (enteredDateString.substr(8)==currentDate.getHours()) ){
			 return true;
			
			} else {
			   return false;
			  }
		 
	 }else if (enteredDateString.length <= 12){
		 if( (enteredDateString.substr(0,2)==currentDate.getDate())
			 && (enteredDateString.substr(2,2)==(currentDate.getMonth()+1))
			 && (enteredDateString.substr(4,4)==currentDate.getFullYear())
			 && (enteredDateString.substr(8,2)==currentDate.getHours()) 
			 && (enteredDateString.substr(10)==currentDate.getMinutes()) ){
			  return true;
			
		    } else {
			   return false;
		    }
	 }
	 
	 
 }
 
 function reportChargeGroupOnChange(){
	 if(getFieldByID("radRptChgCode").checked){
		 setField("txtRptChargeGroupCode", "");
		 setField("txtRptChargeGroupDesc", "");
		 Disable("selReportingGroup", false);
		 Disable("txtRptChargeGroupCode", true);
		 Disable("txtRptChargeGroupDesc", true);
	 }else{
		 setField("selReportingGroup", "");
		 Disable("selReportingGroup", true);
		 Disable("txtRptChargeGroupCode", false);
		 Disable("txtRptChargeGroupDesc", false);
	 }
 }
 
 function disableMinMaxColumns(rowNumber,disable) {	 
	 objDGMOD.setDisable(rowNumber, 6, disable);
	 objDGMOD.setDisable(rowNumber, 7, disable);			
 }
 
 function changeRefOnlyForMOD() {
	 if (getText("chkRefundable") == 'on') {
		Disable("chkRefundableOnlyMOD", false);
		setField("chkRefundableWhenExchange", false);
	 } else {
		setField("chkRefundableOnlyMOD", false);
		Disable("chkRefundableOnlyMOD", "true");		
		Disable("chkRefundableWhenExchange", false);
		setField("chkRefundableWhenExchange", true);
	 }
 }
 
 function clickBundleFareCharge(){
	
	 if (getFieldByID("chkBundledFareCharge").checked){ 
		 whenBundleFareChecked();
		 checkedBundleFare = true;
	 }else{
		whenBundleFareUNChecked();
		checkedBundleFare = false;
	 } 
 }
 
 function whenBundleFareChecked(){
	 required_changed();
	 
	 //Set non-refundable flags
	 //setField('chkRefundable', false);
	 //Disable('chkRefundable', 'true');
	 //setField('chkRefundableOnlyMOD', false);
	 //Disable('chkRefundableOnlyMOD', 'true');
	 //setField('chkRefundableWhenExchange', false);
	 //Disable('chkRefundableWhenExchange', 'true');
	 
	 getFieldByID("radPos").disabled = true;
	 getFieldByID("radSegment").disabled = true;
	 getFieldByID("radCookieBased").disabled = true; 
	 
	// Select 'All' in Apply OND Seg and disable it
	 getFieldByID("selOndSeg").selectedIndex = 0 ;
	 getFieldByID("selOndSeg").disabled = true;
	// Select 'All' in Sales channel and disable it
	 getFieldByID("selChannel").selectedIndex = 0 ;
	 getFieldByID("selChannel").disabled = true;
	 
	 selectSurcharge();
	 getSelGroup();
	 
	 //Preparing category radio buttons
	 getFieldByID("radCat1").checked = true;
	 categoryClick();
	 
	 var adtchildIndex = 0;
	 var selectApplyTo =   getFieldByID("selApplyTo");
	 for ( var intVal = 0; intVal < selectApplyTo.length; intVal++) {
		 if(selectApplyTo.options[intVal].text == "Adult-Child"){
			 adtchildIndex  = intVal;
			 break;
		 }
	 }
	 getFieldByID("selApplyTo").selectedIndex = adtchildIndex ;
	 getFieldByID("selApplyTo").disabled = true;
	 
 }
 
 function whenBundleFareUNChecked(){
	 Disable('chkRefundable', false);
	 setField("chkRefundable", true);
	 changeRefOnlyForMOD();
	 
	 getFieldByID("radPos").disabled = false;
	 getFieldByID("radSegment").disabled = false;
	 getFieldByID("radCookieBased").disabled = false; 
	 
	 getFieldByID("selOndSeg").disabled = false;
	 getFieldByID("selChannel").disabled = false;
	 getFieldByID("selApplyTo").disabled = false;
 }
 
 function selectSurcharge(){
	 
	 var surchargeIndex = 0;
	 var selectGroup =   getFieldByID("AddSegselGroup");
	 for ( var intVal = 0; intVal < selectGroup.length; intVal++) {
		 if(selectGroup.options[intVal].text == "SUR"){
			 surchargeIndex  = intVal;
			 break;
		 }
	 } 
	 getFieldByID("AddSegselGroup").selectedIndex = surchargeIndex ;
	
 }
 
function ifSurchargeSelect(){
	 
	 var surchargeIndex = getFieldByID("AddSegselGroup").selectedIndex;
	 var selectGroup =   getFieldByID("AddSegselGroup");
	 if(selectGroup.options[surchargeIndex].text == "SUR"){
		 return true;
	 }
	
	 return false;
 }

function inputFocus(object , val){
    if(object.value==val){ object.value=""; object.style.color="#000"; }
}
function inputBlur(object , val){
    if(object.value==""){ object.value=val; object.style.color="#888"; }
}

function chargeRateRestrictionsForServiceTax(disable){
	var size = arrData.length;
	try{
		for ( var i = 0; i < size; i++) {
			disableMinMaxColumns(i,disable);
			if(disable){
				$( 'select[name="selPer"]' ).append('<option value="PTFST">PTFST</option>');
				objDGMOD.setCellValue(i, 4, "PTFST");
			} else {
			  if(objDGMOD.getCellValue(i,4) == 'PTFST') {
			    objDGMOD.setCellValue(i, 4, "V");
			  }
				$('#selPer option[value="PTFST"]').remove();
			}
			
			objDGMOD.setDisable(i, 4, disable);
		}
	}catch(e){
		//do nothing
	}
}

function populateInvoiceMapping() {
	var reportCategoryPopulated = false;
	$('#selGenerateReport').find('option').remove().end();
	if ($('#selCountry').val() != undefined && $('#selCountry').val() != ""
			&& countryServiceTaxReportCategory !== undefined) {
		var selectedCountry = $('#selCountry').val();
		if (countryServiceTaxReportCategory[selectedCountry] != undefined) {
			$('select[name="selGenerateReport"]').append(
					'<option value=""></option>');
			$('select[name="selGenerateReport"]').append(
					countryServiceTaxReportCategory[selectedCountry]);
			reportCategoryPopulated = true;
		}
	}
	if (!reportCategoryPopulated) {
		$('select[name="selGenerateReport"]')
				.append(
						'<option value="TAX1">TAX1</option><option value="TAX2">TAX2</option><option value="TAX3">TAX3</option>');
	}
	setVisible("selGenerateReport", true);

}
