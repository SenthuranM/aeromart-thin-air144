var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtDateFrom", strDate);
		break;
	case "1":
		setField("txtDateTo", strDate);
		break;
	case "3":
		frames["frm_DE"].setDate(strDate, strID);
		break;
	case "4":
		frames["frm_DE"].setDate(strDate, strID);
		break;
	}
}

function LoadCalendar(strID, objEvent) {
	var status = getText("radRateCat");
	if (status == "WithDateRange") {
		objCal1.ID = strID;
		objCal1.top = 0;
		objCal1.left = 0;
		objCal1.onClick = "setDate";
		objCal1.showCalendar(objEvent);
	}
}

// ---------------------------------------

function AddTax() {
	frames["ModifyCharges"].addDataRow();
}

function controlTrnsitInputs(){
	if(document.getElementById("chkInwardTrn").value == "N" &&
			 document.getElementById("chkOutwardTrn").value == "N" &&
			document.getElementById("chkAllTrn").value == "N"){
		document.getElementById("minTranDuration_DD").value = "";
		document.getElementById("minTranDuration_HR").value = "";
		document.getElementById("minTranDuration_MI").value = "";
		document.getElementById("maxTranDuration_DD").value = "";
		document.getElementById("maxTranDuration_HR").value = "";
		document.getElementById("maxTranDuration_MI").value = "";
	}
		
}