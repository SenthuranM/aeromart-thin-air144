var strClass = strVar;
var arrClass = strClass.split(",");
var screenId = "SC_ADMN_020";
var valueSeperator = "~";

var objCol1 = new DGColumn();
objCol1.columnType = "label";
objCol1.width = "15%";
objCol1.arrayIndex = 1;
objCol1.headerText = "Fleet/Model";
objCol1.itemAlign = "left"
objCol1.sort = "true";

var objCol2 = new DGColumn();
objCol2.columnType = "label";
objCol2.width = "27%";
objCol2.arrayIndex = 2;
objCol2.headerText = "Model Description";
objCol2.itemAlign = "left"

var objCol3 = new DGColumn();
objCol3.columnType = "label";
objCol3.width = "10%";
objCol3.arrayIndex = 3;
objCol3.headerText = "Status";
objCol3.itemAlign = "left"

// ---------------- Grid
var objDG = new DataGrid("spnAircrafts");
objDG.addColumn(objCol1);
objDG.addColumn(objCol2);
objDG.addColumn(objCol3);

var cabinWidth = (45 / arrClass.length);
var objColC = new DGColumn();
for ( var x = 0; x < arrClass.length; x++) {
	objColC.columnType = "label";
	objColC.width = cabinWidth + "%";
	objColC.arrayIndex = x + 6;
	objColC.toolTip = "Cabin Class " + arrClass[x];
	objColC.headerText = "COS " + arrClass[x];
	objColC.itemAlign = "center"
	objDG.addColumn(objColC);
}

if (getTabValues(screenId, valueSeperator, "intLastRec") == "") {
	setTabValues(screenId, valueSeperator, "intLastRec", 1);
}

objDG.width = "99%";
objDG.height = "150px";
objDG.headerBold = false;
objDG.rowSelect = true;
objDG.arrGridData = arrData;
objDG.seqNo = true;
objDG.seqStartNo = getTabValues(screenId, valueSeperator, "intLastRec");
objDG.pgnumRecTotal = totalRecords;
objDG.paging = true;
objDG.pgnumRecPage = 20;
objDG.pgonClick = "gridNavigations";
objDG.rowClick = "rowClick";
objDG.displayGrid();

function RecMsgFunction(intRecNo, intErrMsg) {
	top[2].HidePageMessage();
	if (intErrMsg != "") {
		top[2].objMsg.MessageText = intErrMsg;
		top[2].objMsg.MessageType = "Error";
		top[2].ShowPageMessage();
	}
}