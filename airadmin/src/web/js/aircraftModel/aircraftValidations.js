// JavaScript
// Author -Shakir

var strRowData;
var strGridRow;
var intLastRec;
var intStat = 0;
var strCOSGridRow;
var objCOSDelay;
var blnCOSGridDisable;
var arrCOSData;
var prevArrCOSData;

var screenId = "SC_ADMN_020";
var valueSeperator = "~";
//var isNeedChecked=true;
function winOnLoad(strMsg, strMsgType) {

	arrCOSData = new Array();
	fillCOSGrid("");
	blnCOSGridDisable = true;
	clearControls();
	setPageEdited(false);

	if (strMsg != null && strMsgType != null)
		showCommonError(strMsgType, strMsg);
	setField("hdnModel", "");
	if (top[1].objTMenu.focusTab == screenId) {
		buttonSetFocus("btnAdd");
	}

	if (arrFormData != null && arrFormData.length > 0) {

		strGridRow = getTabValues(screenId, valueSeperator, "strSearchCriteria");
		setField("txtFleet", arrFormData[0][1]);
		setField("txtDesc", arrFormData[0][2]);
		// setField("masterModelCode",arrFormData[0][9]);
		setField("iataAircraftType", arrFormData[0][9]);
		setField("selSSRTemplate", arrFormData[0][10]);
		if (arrFormData[0][3] == "Active" || arrFormData[0][3] == "active"
				|| arrFormData[0][3] == "on") {
			getFieldByID("chkStatus").checked = true;
		} else {
			getFieldByID("chkStatus").checked = false;
		}
		if (arrFormData[0][4] != null && arrFormData[0][4] != "") {
			fillCOSGrid(String(arrFormData[0][4]));
		}
		setField("hdnVersion", arrFormData[0][6]);
		setField("hdnModel", arrFormData[0][7]);
		setField("hdnMode", arrFormData[0][8]);
		setField("txtItinDesc", arrFormData[0][11]);
		if(arrFormData[0][12]!=null){
			setField("selAircraftTypeCode", arrFormData[0][12]); 
		}
		if(arrFormData[0][13]!=null && arrFormData[0][13]!='null'){
			getFieldByID("spnFlights").innerHTML='<font>'+arrFormData[0][13]+'</font>';
		}
		else{
			getFieldByID("spnFlights").innerHTML='';
		}
		setPageEdited(true);

		if (isAddMode) {
			controlBehaviour("CA");
			if (top[1].objTMenu.focusTab == screenId) {
				getFieldByID("txtFleet").focus();
			}
		} else {
			controlBehaviour("CE");
			if (top[1].objTMenu.focusTab == screenId) {
				getFieldByID("txtDesc").focus();
			}
		}
		

	} else {
		controlBehaviour("PL");
	}

	if (isSuccessfulSaveMessageDisplayEnabled) {
		if (isSaveTransactionSuccessful) {
			alert("Record Successfully saved!");
		}
	}
	if(!isIATAAircraftTypeMandatory){
		document.getElementById("iataAircraftTypeMand").style.visibility = "hidden";
	}
}

// on Grid Row click
function rowClick(strRowNo) {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setPageEdited(false);
		setField("hdnMode", "");
		document.getElementById("frmPage").reset();
		intStat = 1;
		objOnFocus();
		strRowData = objDG.getRowValue(strRowNo);
		strGridRow = strRowNo;
		setTabValues(screenId, valueSeperator, "strSearchCriteria", strGridRow);
		setField("txtFleet", strRowData[0]);
		setField("txtDesc", strRowData[1]);

		var arrGridData = objDG.arrGridData;
		var len = arrGridData[strRowNo].length;
		// setField("masterModelCode",arrGridData[strRowNo][len-3]);
		setField("iataAircraftType", arrGridData[strRowNo][len - 6]);
		setField("selSSRTemplate", arrGridData[strRowNo][len - 5]);		
		setField("txtItinDesc", arrGridData[strRowNo][len - 4]);
		
		Enable("chkStatus", true);
		if (trim(strRowData[2]) == "Active" || strRowData[2] == "ACTIVE"
				|| strRowData[2] == "active") {
			setField("chkStatus", true);
		} else {
			setField("chkStatus", false);
		}
		isNeedChecked=true;
		fillCOSGrid(String(arrData[strRowNo][5]));
		
		Enable("defaultIataAircraft", true);
		if (arrGridData[strRowNo][len - 2] == "Y") {
			setField("defaultIataAircraft", true);
		} else {
			setField("defaultIataAircraft", false);
		}		

		setField("hdnVersion", arrData[strRowNo][4]);		
		
		setField("selAircraftTypeCode", arrData[strRowNo][len - 3]); 
		getFieldByID("spnFlights").innerHTML='';
		setPageEdited(false);
		controlBehaviour("SR");
		
	}
}

function fillCOSGrid(strValue) {
	arrCOSData = new Array();

	if (strValue.indexOf(":") != -1 || strValue.length >0) {
		var arrCOS = strValue.split(":");
		for ( var x = 0; x <= (arrCOS.length - 1); x++) {
			var strValue1 = String(arrCOS[x]);
			if(strValue1.length >0) {
				arrCOSData[x] = new Array();
				if (strValue1.indexOf("/") != -1) {
					var arrCOSDetails = strValue1.split("/");
					for ( var y = 0; y < (arrCOSDetails.length - 1); y++) {
						arrCOSData[x][y] = arrCOSDetails[y];
					
					}
				}
			}
		}
	}
	objDG2.arrGridData = arrCOSData;
	objDG2.refresh();
		
}

// on Add Button click
function addClick() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		intStat = 0;
		objOnFocus();
		clearControls();		
		var theCOSList = theActiveCOS.split(":");
		
		if (theCOSList.length == 1) {
			// Only one active COS
			fillCOSGrid(theActiveCOS);
		}
		setField("hdnMode", "ADD");
		setPageEdited(true);
		setField("hdnModel", "SAVE");
		setField("hdnVersion", "");
		controlBehaviour("CA");
		setField("chkStatus", true);
		setPageEdited(false);
		getFieldByID("txtFleet").focus();
	}
}

// on Edit Button CLick
function editClick() {
	objOnFocus();
	if (intStat == 1) {
		setField("hdnMode", "EDIT");
		setField("hdnModel", "SAVE");
		controlBehaviour("CE");
		getFieldByID("txtDesc").focus();
	} else {
		showCommonError("Error", SelectAircraftToEdit);
	}
}

// on Delete Button click
function deleteClick() {
	objOnFocus();
	if (intStat == 1) {
		// enableInputControls();
		var strBase = strRowData[2];
		var strConf = confirm(deleteConf);
		if (strConf == true) {
			setField("hdnModel", "DELETE");
			setTabValues(screenId, valueSeperator, "intLastRec", 1);
			setField("hdnRecNo", getTabValues(screenId, valueSeperator,
					"intLastRec"));
			Disable("txtFleet", false);
			document.forms[0].submit();
			top[2].ShowProgress();
		}

	} else {
		showCommonError("Error", SelectAircraftToDelete);
	}
}

// on Close Button click
function closeClick() {
	top[1].objTMenu.tabRemove(screenId);
}

// Save Button Click
function saveClick() {
	objOnFocus();
	var strModel = getText("txtFleet");
	var strDesc = getText("txtDesc");

	if (!validateAirCraftModel())
		return;
	if(isNeedChecked){
		if(isEnableDowngrade){
			if(getFieldByID("hdnMode").value=="EDIT"){
				setField("hdnRecNo", 0);
				setField("txtFleet", trim(getText("txtFleet")));
				Enable("txtFleet", true);
				setField("txtDesc", trim(getText("txtDesc")));
				var strValue = "";
				
							
				for ( var x = 0; x < arrCOSData.length; x++) {
					for ( var y = 0; y < arrCOSData[x].length; y++) {
						strValue += arrCOSData[x][y] + "/";
								
					}
					strValue += ":";
				}
				setField("hdnCOSCapacities", strValue);
				setField("hdnRecNo", getTabValues(screenId, valueSeperator, "intLastRec"));
				setField("hdnMainMode", "checkFlights");
				document.forms[0].submit();
				top[2].ShowProgress();
			}
		}
	}
	setField("hdnRecNo", 0);
	setField("txtFleet", trim(getText("txtFleet")));
	Enable("txtFleet", true);
	setField("txtDesc", trim(getText("txtDesc")));
	var strValue = "";
	for ( var x = 0; x < arrCOSData.length; x++) {
		for ( var y = 0; y < arrCOSData[x].length; y++) {
			strValue += arrCOSData[x][y] + "/";
		}
		strValue += ":";
	}
	setField("hdnCOSCapacities", strValue);
	setField("hdnRecNo", getTabValues(screenId, valueSeperator, "intLastRec"));
	document.forms[0].submit();
	top[2].ShowProgress();
}

// Reset Button Click
function resetClick() {
	objOnFocus();
	var strMode = getText("hdnMode");
	if (strMode == "ADD") {
		clearControls();
		setField("chkStatus", true);
		getFieldByID("txtFleet").focus();
		if (theActiveCOS.split(":").length == 1) {
			// Only one active COS
			fillCOSGrid(theActiveCOS);
		}
		setPageEdited(false);
		getFieldByID("txtFleet").focus();
	}

	if (strMode == "EDIT") {
		if (strGridRow < arrData.length
				&& arrData[strGridRow][1] == getText("txtFleet")) {
			// Record found in grid
			setField("txtFleet", arrData[strGridRow][1]);
			setField("txtDesc", arrData[strGridRow][2]);

			var arrGridData = objDG.arrGridData;
			var len = arrGridData[strGridRow].length;
			// setField("masterModelCode",arrGridData[strGridRow][len-3]);
			setField("iataAircraftType", arrGridData[strGridRow][len - 6]);
			setField("selSSRTemplate", arrGridData[strGridRow][len - 5]);
			setField("txtItinDesc", arrGridData[strGridRow][len - 4]);
			
			var preActiveStatus = arrData[strGridRow][3];

			if (preActiveStatus == "Active" || preActiveStatus == "ACTIVE"
					|| preActiveStatus == "active") {
				setField("chkStatus", true);
			} else {
				setField("chkStatus", "");
			}			
			
			if (arrGridData[strRowNo][len - 2] == "Y") {
				setField("defaultIataAircraft", true);
			} else {
				setField("defaultIataAircraft", false);
			}
			getFieldByID("txtDesc").focus();
			fillCOSGrid(String(arrData[strGridRow][5]));
			setField("hdnVersion", arrData[strGridRow][4]);
			
			
			
			
			setPageEdited(false);
			getFieldByID("txtDesc").focus();
		} else {
			// Record not found in grid
			clearControls();
			setPageEdited(false);
			controlBehaviour("PL");
			buttonSetFocus("btnAdd");
		}
	}

}

// To Handle Paging
function gridNavigations(intRecNo, intErrMsg) {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setField("hdnMode", "");
		objOnFocus();
		if (intRecNo <= 0)
			intRecNo = 1;

		setTabValues(screenId, valueSeperator, "intLastRec", intRecNo);
		setField("hdnRecNo", getTabValues(screenId, valueSeperator,
				"intLastRec"));
		if (intErrMsg != "") {
			top[2].objMsg.MessageText = "<li> " + intErrMsg;
			top[2].objMsg.MessageType = "Error";
			top[2].ShowPageMessage();
		} else {
			document.forms[0].submit();
			top[2].ShowProgress();
		}
	}
}

function valTextBox(objTextBox) {
	objOnFocus();
	setPageEdited(true);
	var strLen = objTextBox.value.length;
	var strText = objTextBox.value;
	var blnRet = isPositiveInt(strText);
	if (!blnRet) {
		objTextBox.value = strText.substr(0, strLen - 1);
	}
}

function codePress() {
	setPageEdited(true);
	objOnFocus();
	var strCC = getText("txtFleet");
	var strLen = strCC.length;
	var blnVal = isAlphaNumeric(strCC);
	if (!blnVal) {
		setField("txtFleet", strCC.substr(0, strLen - 1));
		getFieldByID("txtFleet").focus();
	}
}

function statusChanged() {
	setPageEdited(true);
}

function descPress(elem) {
	setPageEdited(true);
	objOnFocus();
	customValidate(elem, /[`!@#$%&*?\[\]{}()|\\\/+=:.,;^~]/g);
}

function clearControls() {
	setField("txtFleet", "");
	setField("txtDesc", "");
	setField("txtItinDesc", "");
	
	// setField("masterModelCode","");
	setField("iataAircraftType", "");
	setField("chkStatus", "");
	setField("selSSRTemplate", "-1");
	setField("defaultIataAircraft", "");
	arrCOSData = new Array();
	objDG2.arrGridData = arrCOSData;
	objDG2.refresh();

}

function disableInputControls() {
	Disable("btnEdit", true);
	Disable("btnDelete", true);
	Disable("txtFleet", true);
	Disable("txtDesc", true);
	Disable("txtItinDesc", true);	
	Disable("iataAircraftType", true);
	Disable("selSSRTemplate", true);
	Disable("selAircraftTypeCode", true);
	// Disable("masterModelCode", true);
	Disable("selClassOfService", true);
	Disable("chkStatus", true);
	Disable("btnSave", true);
	Disable("btnReset", true);
	Disable("btnAddCOS", true);
	Disable("btnDeleteCOS", true);
	Disable("defaultIataAircraft", true);	
}

function enableInputControls() {
	Disable("txtFleet", "");
	Disable("txtDesc", "");
	Disable("txtItinDesc", "");	
	// Disable("masterModelCode", "");
	Disable("iataAircraftType", "");
	Disable("chkStatus", "");
	Disable("selSSRTemplate", false);
	Disable("btnReset", "");
	Disable("selClassOfService", false);
	Disable("btnAddCOS", false);
	Disable("btnDeleteCOS", false);
	Disable("selAircraftTypeCode",false);
	Disable("defaultIataAircraft", "");
}

function objOnFocus() {
	top[2].HidePageMessage();
}

function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}

function saveButtonKeyPress() {
	buttonSetFocus("btnAdd");
}

function validateAirCraftModel() {
	if (validateTextField(getValue("txtFleet"), ModelNumberEmpty)) {
		getFieldByID("txtFleet").focus();
		return false;
	}
	if (!isAlphaNumeric(getValue("txtFleet"))) {
		getFieldByID("txtFleet").focus();
		showCommonError("Error", ModelNumberInvalid);
		return false;
	}

	if (validateTextField(getValue("txtDesc"), DescriptionFieldEmpty)) {
		getFieldByID("txtDesc").focus();
		return false;
	}	

	if (!isAlphaNumericWhiteSpace(getValue("txtDesc"))) {
		getFieldByID("txtDesc").focus();
		showCommonError("Error", DescriptionFieldInvalid);
		return false;
	}
	
	if (validateTextField(getValue("txtItinDesc"), itinDescEmpty)) {
		getFieldByID("txtItinDesc").focus();
		return false;
	}
	if(document.getElementById("selAircraftTypeCode").value==-1){
		showCommonError("Error", "TAIR-70065: Please select aircraft type");
		return false; 
	}
	
	if (!isAlphaNumericWhiteSpace(getValue("txtItinDesc"))) {
		getFieldByID("txtItinDesc").focus();
		showCommonError("Error", itinDescInvalid);
		return false;
	}
	if (isIATAAircraftTypeMandatory && validateTextField(getValue("iataAircraftType"), IATATypeEmpty)) {
		getFieldByID("iataAircraftType").focus();
		return false;
	}
	
	
	if (arrCOSData.length == 0) {
		getFieldByID("btnAddCOS").focus();
		buttonSetFocus("btnAddCOS", "");
		showCommonError("Error", COSMustBeEntered);
		return false;
	}
	var found = false;
	var strTemp1;
	for ( var m = 0; m < arrCOSData.length; m++) {
		strTemp1 = trim(arrCOSData[m][0]);
		if (strTemp1 == null || strTemp1.length <= 0) {
			objDG2.setColumnFocus(m, 2);
			showCommonError("Error", AdultCCFieldEmpty);
			return false;
		}
		if (!validateInteger(strTemp1)) {
			objDG2.setColumnFocus(m, 2);
			showCommonError("Error", AdultCCFieldInvalid);
			return false;
		}
		if (strTemp1 == 0) {
			objDG2.setColumnFocus(m, 2);
			showCommonError("Error", AdultCCFieldCannotZero);
			return false;
		}
		strTemp1 = trim(arrCOSData[m][1]);

		if (!validateInteger(strTemp1)) {
			objDG2.setColumnFocus(m, 3);
			showCommonError("Error", InfantCCFieldInvalid);
			return false;
		}
		var adultCap = 0;
		adultCap = parseInt(trim(arrCOSData[m][0]));
		var infantCap = 0;
		infantCap = parseInt(trim(arrCOSData[m][1]));
		if (adultCap < infantCap) {
			objDG2.setColumnFocus(m, 2);
			showCommonError("Error", AdultCCFieldLessThanInfants);
			return false;
		}
	}
	return true;
}

// on COS Add Button click
function addByClassOfService() {
	objOnFocus();
	var found = false;
	if (strGridRow == undefined) {
		strGridRow = 0;
	}
	var initCOSGridData;
	var arrCOS = new Array();
	var existFlag = false;
	if (arrData.length > 0) {
	  initCOSGridData = arrData[strGridRow][5];
	  if (initCOSGridData.indexOf(":") != -1) {
	    arrCOS = initCOSGridData.split(":");
	  }
	}
	for ( var m = 0; m < arrCOSData.length; m++) {
		if (getValue("selClassOfService") == arrCOSData[m][2] && !found) {
			found = true;
		}
	}
	if (!found) {

		for ( var x = 0; x < arrCOS.length; x++) {
			var arrEachCOS = arrCOS[x];
			if (arrEachCOS.indexOf("/") != -1) {
				var arrCOSDetails = arrEachCOS.split("/");
				if (arrCOSDetails[3] == getText("selClassOfService")) {
					existFlag = true;
					arrCOSData[arrCOSData.length] = new Array("0", "0",
							getValue("selClassOfService"),
							getText("selClassOfService"), arrCOSDetails[4],
							arrCOSDetails[5], "N", arrCOSData.length);
					break;
				}
			}

		}

		if (!existFlag) {
			arrCOSData[arrCOSData.length] = new Array("0", "0",
					getValue("selClassOfService"),
					getText("selClassOfService"), "", "", "N",
					arrCOSData.length);
		}
		objDG2.arrGridData = arrCOSData;
		objDG2.refresh();
		setPageEdited(true);
	} else {
		showCommonError("Error", COSAlreadyEntered);
	}
}

// on COS Grid Row click
function rowCOSClick(strCOSRowNo) {
	objOnFocus();
	strCOSRowData = objDG2.getRowValue(strCOSRowNo);
	strCOSGridRow = strCOSRowNo;
	controlBehaviour("SR2");
}

// on COS Delete Button click
function deleteByClassOfService() {

	objOnFocus();
	var arrTempDeletedArray = new Array();
	arrTempDeletedArray.length = 0;
	var l = 0;
	for ( var m = 0; m < arrCOSData.length; m++) {
		if (strCOSGridRow != m) {
			arrTempDeletedArray[l] = arrCOSData[m];
			arrTempDeletedArray[l][arrTempDeletedArray[l].length - 1] = l;
			l++;
		}
	}
	arrCOSData = arrTempDeletedArray;
	objDG2.arrGridData = arrCOSData;
	objDG2.refresh();
	setPageEdited(true);
}

// Hnadle control enable / dissable behaviour
function controlBehaviour(action) {
	// PL - Page Load
	// SR - Select grid row
	// SR2 - Select second grid row
	// CA - Click Add
	// CE - Click Edit
	if (action != "SR2") {
		Disable("btnAdd", false);
		Disable("btnClose", false);
		Disable("btnEdit", (action == "PL" || action == "CA"));
		Enable("btnDelete", (action == "SR"));
		Enable("btnSave", (action == "CA" || action == "CE"));
		Enable("btnReset", (action == "CA" || action == "CE"));
	}
	enableDataEntryControls(action);

}

function enableDataEntryControls(action) {

	var blnDisable = (action == "PL" || action == "SR");
	var blnEnable = !blnDisable;
	if (action != "SR2") {
		Enable("txtFleet", (action == "CA"));
		Disable("txtDesc", blnDisable);
		Disable("txtItinDesc", blnDisable);
		// Disable("masterModelCode",blnDisable);
		Disable("iataAircraftType", blnDisable);
		Disable("selSSRTemplate", blnDisable);
		Disable("chkStatus", blnDisable);
		Disable("selAircraftTypeCode",blnDisable);
		Disable("defaultIataAircraft", blnDisable);
		blnCOSGridDisable = blnDisable;

		if (action == "CE" || action == "SR") {
			if ((objDG2.arrGridData != null) && (objDG2.arrGridData.length > 0)) {
				// This a timer function to enable / disable second grid
				objCOSDelay = setInterval("cosGridDisable()", 250);
			}
		}

		if (theActiveCOS.split(":").length == 1) {
			// Only one active COS
			Disable("selClassOfService", true);
			Disable("btnAddCOS", true);
		} else {
			// More active COSs available
			Enable("selClassOfService", (action == "CA" || action == "CE"));
			Enable("btnAddCOS", (action == "CA" || action == "CE"));
		}
	}
	Enable(
			"btnDeleteCOS",
			((action == "SR2") && (arrCOSData.length > 1) && !blnCOSGridDisable));

}

function Enable(strControlId, blnEnable) {
	Disable(strControlId, !blnEnable);
}

// A timer function to enable / disable second grid
function cosGridDisable() {
	if (objDG2.loaded) {
		clearTimeout(objCOSDelay);
		if ((objDG2.arrGridData != null) && (objDG2.arrGridData.length > 0)) {
			objDG2.setDisable("", "", blnCOSGridDisable);
		}
	}
}

function capacityValidate(objControl) {
	setPageEdited(true);
	if (objControl.value == null || objControl.value == "") {
		return false;
	}
	return positiveWholeNumberValidate(objControl);
}
function flightValidate(objControl){
	isNeedChecked=true;
}
function copyDescription(){
	var modelDesc = getValue("txtDesc");
	var itinDesc = getValue("txtItinDesc");
	
	if((modelDesc!=null && trim(modelDesc)!="") && (itinDesc==null || trim(itinDesc)=="")){
		setField("txtItinDesc", modelDesc);
	}
	
}