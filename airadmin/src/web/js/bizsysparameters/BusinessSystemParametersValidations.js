/*
 * Author : Thushara
 */
var strRowData;
var strGridRow;

var screenId = "SC_ADMN_0022";
var valueSeperator = "~";

/*
 * Function Call for the Page Load
 */
function winOnLoad(strMsg, strMsgType) {	
	clearParam();
	setPageEdited(false);
	Disable("btnSave", "false");
	Disable("btnReset", "false");
	setField("hdnEditable", "Y");
	if (saveSuccess == 1) {
		alert("Record Successfully saved!");
		saveSuccess = 0;
	}
	var strTmpSearch = searchData;
	if (strTmpSearch != "" && strTmpSearch != null) { // sets the search data
		var strSearch = strTmpSearch.split(",");
		setField("selCarrierCode", strSearch[0]);		
		setField("txtParamName", strSearch[1]);
		setField("txtParameterKey", strSearch[2]);
	}

	if (arrFormData != null && arrFormData.length > 0) { // if Error
		strGridRow = svdGird;
		setField("hdnEditable", preEditable);
		setField("selCCode", arrFormData[0][1]);
		setField("txtParamName", arrFormData[0][2]);
		setField("txtParamKey", arrFormData[0][3]);

		var arrNewData = new Array('', '', '', '', '', arrFormData[0][5], '',
				'100', '');
		createParamValueField(-1, arrNewData);
		setField("txtParameterValue", arrFormData[0][4]);
		setField("selType", arrFormData[0][5]);

		if (isAddMode) {
			enableControls();
		} else {
			enableControls();
		}
		Disable("btnSave", "");
		Disable("btnReset", "");
		setPageEdited(true);
		setField("hdnAction", preAction);
		if (saveSuccess == 3) {
			saveSuccess = 0;
		}

	}

	if (strMsg != null && strMsgType != null) {
		if (strMsgType == "Error") {
			showPageERRMessage(strMsg);
		}
		if (strMsgType == "Confirmation") {
			showPageConfMessage(strMsg);
		}
	}
	setField("hdnMode", "DISPLAY");
	Disable("btnAdminSyncAll", "");
	top[2].HideProgress();
}

/*
 * Function to clear the input Fields
 */
function clearParam() {
	objOnFocus();
	setPageEdited(false);
	setField("selCCode", "");
	setField("txtParameterName", "");
	setField("txtParamKey", "");
	setField("txtParameterValue", "");
	setField("selType", "");
	disableInputControls();

}
/*
 * Function call for SEARCH Button Click
 */
function searchUser() {
	objOnFocus();
	setTabValues(screenId, valueSeperator, "intLastRec", 1);
	var tmpParamName;
	var tmpParamKey;
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setField("hdnMode", "SEARCH");
		if(getText("txtParamName") != ""){ // this is for refresh the selected value
		 	tmpParamName = getText("txtParamName");
		} else {
			tmpParamName = "ALL";
		}
		if(getText("txtParameterKey") != ""){ // this is for refresh the selected value
		 	tmpParamKey = getText("txtParameterKey");
		} else {
			tmpParamKey = "ALL";
		}	
		searchData = getValue("selCarrierCode") + "," + tmpParamName
				+ "," + tmpParamKey;
		setField("hdnRecNo", 1);
		setField("hdnSearchData", searchData);
		document.frmBizParam.submit();
		top[2].ShowProgress();
	}

}

/*
 * Function call for GRID ROW ITEM Click
 */
function rowClick(strRowNo) {
	objOnFocus();
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setPageEdited(false);
		strRowData = objDG.getRowValue(strRowNo);
		strGridRow = strRowNo;
		clearControls();

		setField("hdnEditable", paramData[strGridRow][6]);		
		
		setField("selCCode", paramData[strGridRow][1]);
		setField("txtParameterName", paramData[strGridRow][2]);
		setField("txtParamKey", paramData[strGridRow][3]);

		createParamValueField(strRowNo, null);

		setField("txtParameterValue", paramData[strGridRow][4]);
		
		setField("selType", paramData[strGridRow][5]);

		disableInputControls();
		
		Disable("txtParameterValue", "");
		readOnly("txtParameterValue", "true");
		document.getElementById("txtParameterValue").title = paramData[strGridRow][4];
		Disable("btnAdminEdit", "");	
		Disable("btnAdminSyncAll", "");
    Disable("btnAdminAdd", "");  

		if (getText("hdnEditable") == "Y") {
			Disable("btnEdit", "");
			Disable("btnSave", true);
			Disable("btnReset", true);
		} else {
			Disable("btnEdit", true);
			Disable("btnSave", true);
			Disable("btnReset", true);
		}
	}
}

function createParamValueField(strRowNo, newDataArr) {
	var strParamValue = "";
	var pname = "txtParameterValue";
	if (strRowNo <= -1 && newDataArr == null) {
		strParamValue += "<input name='" + pname + "' type='text' id='" + pname
				+ "' maxlength='100' size='50'>";
	} else if (strRowNo > -1 && newDataArr == null) {
		var row = paramData[strRowNo];
		var fieldLength = (new Number(strRowData[7]) > 50 ? new Number(
				strRowData[7]) : 50);
		var dunamicFunc = "validate(" + pname + ",'" + strRowNo + "')";

		if (row[5] == "S" || row[5] == "N") {// String or Numeric
			strParamValue += "<input name='" + pname + "' type='text' id='"
					+ pname + "' maxlength='" + strRowData[7] + "' size='"
					+ fieldLength + "' onKeyUp=" + dunamicFunc + " onKeyPress="
					+ dunamicFunc + " onChange='clickChange()'>";
		} else if (row[5] == "E") {// Email
			strParamValue += "<input name='" + pname + "' type='text' id='"
					+ pname + "' maxlength='" + strRowData[7] + "' size='"
					+ fieldLength + "' onKeyUp=" + dunamicFunc
					+ " onChange='clickChange()'>";
		} else if (row[5] == "T") {// Time
			fieldLength = ((row[4] != "" && row[4].length > 5) ? fieldLength : 5); 
			strParamValue += "<input name='"
					+ pname
					+ "' type='text' id='"
					+ pname
					+ "' maxlength='5' size='" + fieldLength 
					+ " onblur='setTimeWithColon(this)' onChange='clickChange()'><font>(HH:MM)</font>";
		}
	} else if (newDataArr != null) {
		createParamValueField(strRowNo, null)
	}

	DivWrite("spnParamValue", strParamValue);
}

function clickChange() {
	objOnFocus();
	setPageEdited(true);
}

function saveParamValueValidations(ctrlName, strRowNo) {
	var oldValue;
	var newValue;
	var isValueChanged = false;
	var newParamType;

	if (ctrlName != null) {
		newValue = getValue(ctrlName);
		if (newValue == null || newValue == "" || trim(newValue) == "") {
			setField(ctrlName, "");
			showCommonError("Error", paramData[strRowNo][2]
					+ " - Parameter value is Required");
			getFieldByID(ctrlName).focus();
			return;
		} else {
		  newParamType = getValue("selType");
			if (newParamType == "T") {

				if (!isTimeFormatValid(ctrlName, newValue)) {
					showCommonError("Error", paramData[strRowNo][2]
							+ " - Time Format is Invalid");
					getFieldByID(ctrlName).focus();
					return;
				}
			}
			if (newParamType == "E") {
				var isValidEmail = checkEmail(newValue);
				if (!isValidEmail) {
					showCommonError("Error", paramData[strRowNo][2]
							+ " - Email Format is Invalid");
					getFieldByID(ctrlName).focus();
					return;
				}
			}
		}

	}
	return true;
}

function validate(ctrlId, strRowNo) {
	var isValidated = false;
	var ctrlValue = getValue(ctrlId.name);
	if (ctrlValue == null || ctrlValue == "") {
		showCommonError("Error", paramData[strRowNo][2]
				+ " - Parameter value is Required");
		getFieldByID(ctrlId.name).focus();
		return isValidated;
	} else {
		pageOnChange();
		// number validate
		if (paramData[strRowNo][5] == "N") {
			isNumericVal(ctrlId);
			getFieldByID(ctrlId.name).focus();
			return;
		}

		// string validate
		if (paramData[strRowNo][5] == "S") {
			getFieldByID(ctrlId.name).focus();
			return;
		}

		// time validate
		if (paramData[strRowNo][5] == "T") {
			if (!IsValidTime(getValue(ctrlId.name))) {
				customValidate(ctrlId,
						/[a-zA-Z`!@#$%\s&*?\[\]{}()|\\\/+=.,;^~_-]/g);
				getFieldByID(ctrlId.name).focus();
				return;
			}
		}

		// email validate
		if (paramData[strRowNo][5] == "E") {
			if (!emailCheck(ctrlId)) {
				customValidate(ctrlId, /[`!\s\[\]{}()|:,;]/g);
				getFieldByID(ctrlId.name).focus();
				return;
			}
		}
		isValidated = true;
	}
	return isValidated;
}

// Checks if time is in HH:MM format - HH can be more than 24 hours. HH can have
// more than one digit upto 4 digits.
function isTimeFormatValid(objTime, objTimeValue) {
	var timePattern = /^(\d{1,}):(\d{2})$/;
	var matchArray = trim(objTimeValue).match(timePattern);

	if (matchArray == null) {
		return false;
	}
	var hour = matchArray[1];
	var min = matchArray[2];
	if ((min < 0) || (min > 59)) {
		return false;
	}
	if (hour.length < 2) {
		setField(objTime, "0" + trim(objTimeValue));
	}
	return true;
}

function emailCheck(obj) {
	pageOnChange();
	if (!checkEmail(trim(obj.value))) {
		getFieldByID(obj.name).focus();
		return false;
	}
	return true;
}

function isNumericVal(objTxt) {
	var strValue = objTxt.value;
	if (!isNumeric(strValue)) {
		customValidate(objTxt, /[a-zA-Z`!@#\s$%&*?\[\]{}()|\\\/=:,;^~_]/g);
	}
}

function pageOnChange() {
	top[2].HidePageMessage();
}

/*
 * Function Call for ADD Button Click
 */
function addParam() {
	objOnFocus();
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setPageEdited(false);
		setField("hdnEditable", "Y");
		enableControls();
		clearControls();
		Disable("selCCode", "");
		Disable("txtParameterName", "");
		Disable("txtParamKey", "");
		Disable("selType", "");
		getFieldByID("selCCode").focus();
		Disable("btnEdit", "false");
		Disable("btnAdminEdit", "false");
		Disable("btnAdminSyncAll", "false");
		setField("hdnMode", "ADD");
		setField("hdnAction", "ADD");
		Disable("btnSave", "");
		Disable("btnReset", "");
	}
}

/*
 * Function call for EDIT Button Click
 */
function editParam() {
	objOnFocus();
	enableControls();
	getFieldByID("txtParameterValue").focus();
	Disable("btnSave", "");
	Disable("btnReset", "");
	setField("hdnMode", "EDIT");
	setField("hdnAction", "EDIT");
}

function adminEditParam() {
  objOnFocus();
  enableControlsForAdminEdit();
  getFieldByID("txtParameterValue").focus();
  Disable("btnSave", "");
  Disable("btnReset", "");
  setField("hdnMode", "EDIT");
  setField("hdnAction", "EDIT");
}

function adminSyncAllParams() {
  setField("hdnMode", "SYNC_ALL");
  document.frmBizParam.submit();
}

/*
 * Function call for SAVE Button Click
 */
function saveData() {
	objOnFocus();
	setField("hdnMode", "SAVE");
	var selectedRows = objDG.getSelectedRows();

	if (!saveParamValueValidations("txtParameterValue", selectedRows[0])) {
		return false;
	} else {
		setField("txtParameterName", replaceall(
				trim(getText("txtParameterName")), ",", "_"));
		setField("txtParamKey", trim(getText("txtParamKey")));
		setField("txtParameterValue", trim(getText("txtParameterValue")));
		setField("hdnEditable", "Y");
		enableAllControls();
		setField("hdnRecNo", initialrec);
		setField("hdnSearchData", searchData);
		setField("hdnGridData", strGridRow);
		document.frmBizParam.submit();
		top[2].ShowProgress();
	}
}

/*
 * Function to set PAGE status
 */
function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
	if (isEdited) {
		Disable("btnSave", "");
	} else {
		Disable("btnSave", "false");
	}
}

/*
 * Function to Disable Inputt Controls
 */
function disableInputControls() {
	Disable("selCCode", "false");
	Disable("txtParameterName", "false");
	Disable("txtParamKey", "false");
	Disable("txtParameterValue", "false");
	Disable("selType", "false");
	Disable("btnEdit", "false");
	Disable("btnAdminEdit", "false");
	Disable("btnAdminSyncAll", "false");
}

/*
 * Function call for RESET Button Click
 */
function resetClick() {
	var strMode = getText("hdnAction");
	objOnFocus();
	if (strMode == "ADD") {
		clearControls();
		getFieldByID("selCCode").focus();
		setField("hdnEditable", "Y");
	}

	if (strMode == "EDIT") {
		if (strGridRow < paramData.length
				&& paramData[strGridRow][1] == getFieldByID("selCCode").value
				&& paramData[strGridRow][3] == getFieldByID("txtParamKey").value) {
			setField("hdnEditable", paramData[strGridRow][6]);
			setField("selCCode", paramData[strGridRow][1]);
			setField("txtParameterName", paramData[strGridRow][2]);
			setField("txtParamKey", paramData[strGridRow][3]);
			setField("txtParameterValue", paramData[strGridRow][4]);
			setField("selType", paramData[strGridRow][5]);
			getFieldByID("txtParameterValue").focus();
			enableControls();
		} else {
			// Grid row not found. Clear it off.
			clearParam();
			disableInputControls();
			Disable("btnSave", "false");
			Disable("btnReset", "false");
			setField("hdnEditable", "Y");
			buttonSetFocus("btnAdd");
		}
	}
	setPageEdited(false);
}

function enableAllControls() {
	Disable("selCCode", "");
	Disable("txtParameterName", "");
	Disable("txtParamKey", "");
	Disable("selType", "");
	Disable("txtParameterValue", "");
}

/*
 * Function to Enable INPUT Controls
 */
function enableControls() {
	Disable("selCCode", true);
	Disable("txtParameterName", true);
	Disable("txtParamKey", true);
	Disable("selType", true);

	if (getText("hdnEditable") == "Y") {
		readOnly("txtParameterValue", false);		
		Disable("txtParameterValue", "");
	} else {
		Disable("txtParameterValue", true);
	}
}

function enableControlsForAdminEdit() {
  Disable("selCCode", true);
  Disable("txtParameterName", true);
  Disable("txtParamKey", true);
  Disable("selType", true);
  readOnly("txtParameterValue", false);   
  Disable("txtParameterValue", "");
}

/*
 * Function to Clear the INPUT Fields
 */
function clearControls() {
	objOnFocus();
	setField("hdnEditable", "Y");
	setField("selCCode", "");
	setField("txtParameterName", "");
	setField("txtParamKey", "");
	setField("selType", "");
}

/*
 * Function to Reset the page messages
 */
function objOnFocus() {
	resetPageERRMessage();
}

/*
 * Function to Handle Page Navigation
 */
function gridNavigations(intRecNo, intErrMsg) {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		objOnFocus();
		if (intRecNo <= 0)
			intRecNo = 1;
		setField("hdnRecNo", intRecNo);
		setField("hdnSearchData", searchData);
		if (intErrMsg != "") {
			showPageERRMessage(intErrMsg);
		} else {
			document.forms[0].submit();
			top[2].ShowProgress();
		}
	}
}

/*
 * Function to set Confirmation Messages
 */
function showPageConfMessage(strMsg) {
	showCommonError("CONFIRMATION", strMsg);
}

/*
 * Function to set Error Messages
 */
function showPageERRMessage(strMsg) {
	showCommonError("ERROR", strMsg);
}

/*
 * Function to Reset the Error Messages
 */
function resetPageERRMessage() {
	top[2].HidePageMessage();
}

//var objCmb1 = new combo();
//var begin = [[" ALL", " ALL"]];

/*buildCombos(paramNameArr);

function buildCombos(arrName) {	
	objCmb1.id = "selParameterName";
	objCmb1.top = "27";
	objCmb1.left = "115";
	objCmb1.width = "250";
	objCmb1.textIndex = 1;
	objCmb1.valueIndex = 0;
	objCmb1.tabIndex = 1;
	objCmb1.selectedText = " ALL";
	objCmb1.selectedValue = " ALL";
	objCmb1.arrData = begin.concat(arrName); // add "ALL" element begin of the list	
	objCmb1.onChange = "pageOnChange";
	objCmb1.buildCombo();
}

var objCmb2 = new combo();

buildParamKeyCombos(paramKeyArr);

function buildParamKeyCombos(arrName1) {	
	objCmb2.id = "selParamKey";
	objCmb2.top = "27";
	objCmb2.left = "465";
	objCmb2.width = "125";
	objCmb2.textIndex = 1;
	objCmb2.valueIndex = 1;
	objCmb2.tabIndex = 1;
	objCmb2.selectedText = " ALL";
	objCmb2.selectedValue = " ALL";
	objCmb2.arrData = begin.concat(arrName1); // add "ALL" element begin of the list	
	objCmb2.onChange = "pageOnChange";
	objCmb2.buildCombo();
}*/