function UI_userAssignStations(){
	var dataMap = {
			Users: [ ],
			Stations: ["ABC","YOM"],
			
	}
	this.ready = function(){ 
	//	loadInitdata();
		$("#btnSave").click(saveUserAssignment);
		$('#selUser').change(getUserAssignment);
		$("#divUserStation").decoratePanel("User Station Assignment");
	}
	fillInitData = function(){
		$("#selUser").fillDropDown({firstEmpty:false, dataArray:dataMap.Stations , keyIndex:0, valueIndex:1});
		$("#selStation").fillDropDown({firstEmpty:false, dataArray:dataMap.Stations , keyIndex:0, valueIndex:1});
	}
	loadInitdata = function(){
		$.ajax({ 
			url : 'manageUserAssignment!get.action',
			beforeSend : ShowProgress(),
			type : "GET",
			async:false,
			dataType : "json",
			complete:function(response){
				 var response = eval("(" + response.responseText + ")");
			
				
				
				 HideProgress();
			}
		});
	}
	objectToArray = function(obj){
		rObj = [];
		$.each(obj, function(key, val){
			t = [];
			t[0] = key;
			t[1] = val;
			rObj[rObj.length] = t;
		})
		return rObj
	}
	function ShowProgress(){
		top[2].ShowProgress();
	}
	function HideProgress(){
		top[2].HideProgress();
	}
	function hideMessage() {
		top[2].HidePageMessage();
	}
	getSelectedSelectBoxData = function(selectBoxID) {
		var selector="#"+selectBoxID+" option:selected"
		var selectedItems=[];
		$(selector).each(function () {
		
			selectedItems.push(this.value);
			
		});
		for(i=0; i<selectedItems.length; i++) {
			if($("#spnStations").text()!=""){
				if($("#spnStations").text().indexOf(selectedItems[i])!=-1) {
					showERRMessage("Selected some stations already assign to user.")
					HideProgress();
					return null;
					break;
				}
			}
		}
		return JSON.stringify(selectedItems);
	}
	
	getUserAssignment = function(){
		var data={};
		data["users"] = $("#selUser :selected").val();
		var targetURL = 'manageUserAssignment!get.action';
			
			$.ajax({ 
				url : targetURL,
				beforeSend : ShowProgress(),
				data : data,
				type : "POST",
				async:false,
				dataType : "json",
				complete:function(response){
					 var responseData=$.parseJSON(response.response);
					 HideProgress();
					 if(responseData.msgType=="success"){
										
						 $("#spnStations").text(responseData.stationsExist);
						 
					 } else{
						
						 alert("System busy. Please try again later.");
						 
					 }
				}
			});
		
	}
	saveUserAssignment = function(){
		if (valadateForm()){
			var submitData = getDataFromPage();
			var targetURL;
		
		    targetURL = 'manageUserAssignment!save.action';
		
			
			
			$.ajax({ 
				url : targetURL,
				beforeSend : ShowProgress(),
				data : submitData,
				type : "POST",
				async:false,
				dataType : "json",
				complete:function(response){
					 var responseData=$.parseJSON(response.response);
					 HideProgress();
					 if(responseData.msgType=="success"){
						
						 getUserAssignment();
						 
						 alert("User station assignment successful!");
					 } else{
						 if(responseData.messageTxt=="DPR"){
							 showERRMessage(promoCodeUniqueValueRequired);
						 }
						 else{
							 alert("System busy. Please try again later.");
						 }
					 }
				}
			});
		}
	}
	valadateForm = function(){
		top[2].HidePageMessage();
		var valied = true;
		if($("#selFrom_1").val()==""){
			showERRMessage("Please select the user");
			valied = false;
		}
		else if($("#selStation").val()==null){
			showERRMessage("Please select stations");
			valied = false;
		}
		else if(getSelectedSelectBoxData("selStation")==null){
			showERRMessage("Selected some stations already assign to user.");
			valied = false;
		}
		return valied;
	}
	getDataFromPage = function(){
		var data={};
		if(getSelectedSelectBoxData("selStation")!=null){
			data["stations"] = getSelectedSelectBoxData("selStation");
		}
				
		data["users"] = $("#selUser :selected").val();
		return data;
	}
	
	function SaveMsg(msg) {
		top[2].objMsg.MessageText = msg;
		top[2].objMsg.MessageType = "Confirmation";
		top[2].ShowPageMessage();
	}
}
UI_userAssignStations = new UI_userAssignStations();
UI_userAssignStations.ready();