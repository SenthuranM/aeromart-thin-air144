$.fn.multiline = function(text){
    this.text(text);
    this.html(this.html().replace(/\n/g,'<br/>'));
    return this;
}
function convertDate(inputFormat) {
  var d = new Date(inputFormat);
  return [d.getDate(), d.getMonth()+1, d.getFullYear()].join('/');
}
function UI_groupBooking(){

	var bCurrency='AED';
	var status = [["0","Pending Approval"],["1","Approve"],["2","Reject"],["3","Re-Quote"],["4","Pending after Re-Quote"],["5","Request Release Seat"],["6","Withdraw"],["7","Released Seats"],["8","Booked"]];//
	var tabbedPaneDataMap = [	{
									gridName:"jqGridGrpPAData",
									pagerName:"jqGridGrpPAPages",
									statusIDs:0
								},
								{
									gridName:"jqGridGrpReqData",
									pagerName:"jqGridGrpReqPages",
									statusIDs:3
								},
								{
									gridName:"jqGridGrpRejData",
									pagerName:"jqGridGrpRejPages",
									statusIDs:2
								},
								{
									gridName:"jqGridGrpApprData",
									pagerName:"jqGridGrpApprPages",
									statusIDs:1
								},
								{
									gridName:"jqGridGrpRelData",
									pagerName:"jqGridGrpRelPages",
									statusIDs:7
								},
								{
									gridName:"jqGridGrpOthData",
									pagerName:"jqGridGrpOthPages",
									statusIDs:-1
								}];
	var dataMap = {
			Airport: [ "cmb" ]
	}
	var fareDate = 2 ;
	var paymenDate = 21 ;
	var selectedRowData = null;
	var routeData={};
	this.ready = function(){ 
		//alert("Test");
		var tLink = $("<a href='javascript:void(0)'><img src='../../images/tri-down1_no_cache.gif' alt=''>&nbsp;Search Group Request</a>");
		tLink.click(hideAddEditGrpB);
		$("#divGrpBSearch").decoratePanel(tLink);
		$("#grpBookingDetails").decoratePanel("Add/Edit Group Request");
		$("#grpBookingDetails").parent().hide();
		loadInitData();
		constructGrpReqGrid();
	
		$("#btnSave").click(saveGroupRequest);

		$("#btnSearch").click(searchGrpRequest);
		
		$("#btnEdit").click(function(){
			showAddEditGroupBooking();
		});
		
		 $("#rdo_Val").click(function(){
			// $("#minPayReq_Pct").val('');
			$("#pct").hide();
			$("#val").show();
		});
			$("#rdo_Pct").click(function(){
			// $("#minPayReq_Val").val('');
			$("#val").hide();
			$("#pct").show();
		}); 
		
		$(".addItem").click(function(e){
			addItemsToSelect(e.target.id);
		});
		$(".removeItem").click(function(e){
			removeItemsFromSelect(e.target.id);
		});
		$("#btnApprove").click(updateAdminApproval);
		$("#btnReject").click(updateAdminRejection);
		$("#btnRequote").click(updateAdminReQuote);
		$(" #tagetPayDate, #fareValidDate" ).datepicker({
			minDate: new Date(),
			showOn: "button",
			buttonImage: "../../images/calendar_no_cache.gif",
			buttonImageOnly: true,
			dateFormat: 'mm/dd/yy'
		}).keyup(function(e) {
		    if(e.keyCode == 8 || e.keyCode == 46) {
		        $.datepicker._clearDate(this);
		    }
		});
		
			
		$("#btnReleasedSeat").click(releasedSeat);
		$("#btnShare").click(loadShareStation);
		$("#btnHistory").click(loadHistoryData);
		
		$("#groubBookingTabs").tabs({
			select: function( event, ui ) {
				 triggerSearch(ui.index);
			 }
		});
		
		triggerSearch(0);
		 
		$("#subRequestGrid").jqGrid({
			dataType:'local',
			colNames:[ "Request ID",'OND' , 'Departure Date' ,'Flignt Numbers', 'AD/CH/IN',"OAL Fare", "Req. Fare","Remarks","Approved Fare","Select","Status",""],
			colModel:[
			    {name:'requestID', index:'requestID',  width:80, align:"center"},
			    {name:'ond', index:'ond', width:100, align:"center"},
				{name:'departureDate', index:'departureDate',  width:80, align:"center"},
				{name:'flightNumbers', index:'flightNumbers', width:80, align:"center"},
				{name:'paxCount', index:'paxCount', width:90, align:"center"},
				{name:'oalFare', index:'oalFare' , width:90, align:"center"},
				{name:'reqFare', index:'reqFare' ,width:60, align:'center'},
				{name:'remarks', index:'remarks', width:110, align:"center"},
				{name:'approvedFare', index:'approvedFare', width:90, align:"center",editable:true},
				{name:'select', index:'select', width:70, align:"center",editable:true,edittype:'checkbox'},
				{name:'status', index:'status', width:90, align:"center",formatter:statusFomatter},	
				{name:'hiddenStatus', index:'hiddenStatus',width:10,editable:true }
			],
			
			allowEdit:true, 
			afterEditCell:function(){
				alert(edited);
			},
			afterSaveCell:function(){
				alert(edited);
			}
		});
		
		$("#minPayReq_Val,#minPayReq_Pct,#agreedFare").numeric({allowDecimal:true});
		
		$("#minPayReq,#agreedFare").numeric({allowDecimal:true});
	}; 
	
	fillSubRequestData = function(){
		var subRequests = selectedRowData.groupRequest.subRequests;
		var rowsToAdd = new Array();
		var count = 1;
		
		var mainReq = selectedRowData.groupRequest;
		var mainOndCode="";
		var mainDepartureDate="";
		var mainReturnDate="";
		var mainFlightNumber="";
		/*
		 * Setting the main request to the sub grid.
		 */
		for(var k=0;k<mainReq.groupBookingRoutes.length;k++){
			var route=mainReq.groupBookingRoutes[k];
			
			mainOndCode=mainOndCode+"\n"+route.ondCode;
			mainDepartureDate=mainDepartureDate+"\n"+moment(route.departureDate).format("DD/MM/YYYY");
			mainReturnDate=mainReturnDate+"\n"+moment(route.returningDate).format("DD/MM/YYYY");
			mainFlightNumber=mainFlightNumber+"\n"+route.flightNumber;
		}
		
		var mainRowData = {
				requestID:mainReq.requestID,
				ond:mainOndCode,
				departureDate:mainDepartureDate,
				returningDate:mainReturnDate,
				flightNumbers:mainFlightNumber,
				paxCount:mainReq.adultAmount+"/"+mainReq.childAmount+"/"+mainReq.infantAmount,
				oalFare:mainReq.oalFare,
				reqFare:mainReq.requestedFare,
				remarks:mainReq.agentComments,
				approvedFare:mainReq.agreedFare == null ? 0.0 : mainReq.agreedFare,
				select:true,
				status:mainReq.statusID,
				hiddenStatus:mainReq.statusID,	
		};
		
		rowsToAdd.push(mainRowData);
		
		for(var i=0;i<subRequests.length;i++){
			var subRequest=subRequests[i];
			var routes=subRequest.groupBookingRoutes;
			var ondCode="";
			var departureDate="";
			var returnDate="";
			var flightNumber="";
			
			for(var j=0;j<routes.length;j++){
				var route=routes[j];
				ondCode=ondCode+"\n"+route.ondCode;
				departureDate=departureDate+"\n"+moment(route.departureDate).format("DD/MM/YYYY");
				returnDate=returnDate+"\n"+moment(route.returningDate).format("DD/MM/YYYY");
				flightNumber=flightNumber+"\n"+route.flightNumber;
			}
			
			var rowData = {
					requestID:subRequest.requestID,
					ond:ondCode,
					departureDate:departureDate,
					returningDate:returnDate,
					flightNumbers:flightNumber,
					paxCount:subRequest.adultAmount+"/"+subRequest.childAmount+"/"+subRequest.infantAmount,
					oalFare:subRequest.oalFare,
					reqFare:subRequest.requestedFare,
					remarks:subRequest.agentComments,
					approvedFare:subRequest.agreedFare == null ? 0.0 : subRequest.agreedFare,
					select:false,
					status:subRequest.statusID,
					hiddenStatus:subRequest.statusID,
			};
			rowsToAdd.push(rowData);
		}
		$("#subRequestGrid").clearGridData();
		fillGridData({id:"#subRequestGrid", data:rowsToAdd});
	}
	
	chkStatus = function(){
		var rowIds=$("#subRequestGrid").getDataIDs();
		if(rowIds.length>1){
			for(var i=1;i<rowIds.length;i++){
				$("#"+rowIds[0]+"_hiddenStatus").hide();
				if ($("#"+rowIds[0]+"_hiddenStatus").val()!= $("#"+rowIds[i]+"_hiddenStatus").val()){
					$("#"+rowIds[i]+"_hiddenStatus").hide();
					$("#"+rowIds[i]+"_select").attr('checked', false);
					$("#"+rowIds[i]+"_select").disable();
					$("#"+rowIds[i]+"_approvedFare").disable();
				}
				else{
					$("#"+rowIds[i]+"_hiddenStatus").hide();
				}
			}
		}
		else{
			$("#"+rowIds[0]+"_hiddenStatus").hide();
		}
	}
	
	fillGridData = function(p){
		var i = 0;
		p = $.extend({id:"",
					 data:null,
					 editable:false}, 
					p);
					
		$(p.id).clearGridData();
		
		if (p.data != null){
			$.each(p.data, function(){
				$(p.id).addRowData(i + 1, p.data[i]);
				if ($(p.editable)){
					$(p.id).editRow(String(i+1));
				}
				i++;
			});
			//fix to remove className
			if ($().jquery > '1.4.2'){ //TODO to remove this condition once the 1.7.2
				$.each( $(p.id).find("input, button, select"), function(){
					if ($(this).attr("className") != ""){
						$(this).addClass($(this).attr("className"));
						$(this).removeAttr("className");
					}
				});
			}
		}
	}
	
	changePageNumber = function(pageButton){
		var selectedTab=$("#groubBookingTabs").tabs('option', 'selected');
		var tabData = tabbedPaneDataMap[selectedTab];
		var lastPage = $("#"+tabData.gridName).jqGrid().getGridParam().lastpage;
		var postData = $("#"+tabData.gridName).jqGrid().getGridParam().postData;
		var currentPage = postData.page;
		
		if(pageButton=="prev"){
			currentPage--;
		}else if(pageButton=="next"){
			currentPage++;
		}else if(pageButton=="last"){
			currentPage = lastPage;
		}else{
			currentPage = 1;
		}
		$("#"+tabData.gridName).jqGrid().setGridParam({postData:{page: currentPage}});
	}
	
	triggerSearch = function(index){
		 
		 var tabData = tabbedPaneDataMap[index];
		 reqData=getSearchData();
		 reqData["grpBookingReq.statusID"]=tabData.statusIDs;
		 var postData = $("#"+tabData.gridName).jqGrid().getGridParam().postData;
		 var temGrid = $("#"+tabData.gridName).jqGrid();
		 
		 $.ajax({
	           url : 'manageGroupBooking!search.action',
	           beforeSend : ShowProgress(),
	           data: $.extend(postData, reqData),
	           dataType:"json",
	           type : "POST",
			   complete: function(jsonData,stat){	        	  
	              if(stat=="success") {
	            	  mydata = eval("("+jsonData.responseText+")");
	            	  if (mydata.msgType != "Error"){
	            		  $.data(temGrid[0], "gridData", mydata.rows);
			              temGrid[0].addJSONData(mydata);
	            	  }else{
	            		 alert(mydata.message);
	            	  }
	            	  HideProgress();	
	              }
	           }
	     });
	}


	fillInitdata = function(response){
		$("#selFrom, #selTo,#selFrom_1,#selTo_1").fillDropDown({firstEmpty:false, dataArray:dataMap.Airport , keyIndex:0, valueIndex:0});
		$("#status").fillDropDown({firstEmpty:false, dataArray:status , keyIndex:0, valueIndex:1});
		$("#travelAgent").append(response.agents);
		$("#station").append(response.stations);
				
	};
	
	loadInitData=function(){
		$.ajax({ 
			url : 'loadGroupBookingReqForInit.action',
		//	beforeSend : UI_commonSystem.showProgress(),
			type : "GET",
			async:false,
			dataType : "json",
			complete:function(response){
				var response = eval("(" + response.responseText + ")");
				dataMap.Airport =objectToArray(response.airports);				
				$("#spnCurrency,#spnCurrencyApprove,#spnCurrencyMR").text(response.baseCurrency);				 
				bCurrency=response.baseCurrency;
				fareDate = response.farePeriod ;
				paymenDate = response.paymentPeriod;
				fillInitdata(response);

			//	 HideProgress();
			}
		});
	}
	objectToArray = function(obj){
		rObj = [];
		$.each(obj, function(key, val){
			t = [];
			t[0] = key;
			t[1] = val;
			rObj[rObj.length] = t;
		})
		return rObj
	}
	
	saveGroupRequest=function(){
		var submitData=getSubmitData();
		$.ajax({ 
			url : 'manageGroupBooking!save.action',
			beforeSend : ShowProgress(),
			data : submitData,
			type : "POST",
			async:false,
			dataType : "json",
			complete:function(response){
				UI_commonSystem.hideProgress();		
			}
		});
	}
	
	getSelectedData = function(){
		var rowIDs=$("#subRequestGrid").getDataIDs();
		var data=[];
		for(var i=0;i<rowIDs.length;i++){
			var rowData=$("#subRequestGrid").getRowData(rowIDs[i]);
			if($("#"+rowIDs[i]+"_select").is(":checked")){
				data.push({key:rowData.requestID,value:$("#"+rowIDs[i]+"_approvedFare").val()});
			}
		}
		return data;
	}
	
	updateAdminApproval=function(){
		if(validateApproveForm()){
	
			var data={};
			data["selectedDetails"] = JSON.stringify(getSelectedData());
			data["grpBookingReq.version"] = selectedRowData.groupRequest.version;
			
			data["grpBookingReq.requestID"] =$("#requestID").val();
			data["grpBookingReq.agreedFare"] =$("#agreedFare").val();
			data["grpBookingReq.statusID"] =1; //Approval 
			data["grpBookingReq.approvalComments"] =$("#adminRemarks").val();
			
			 if ($('#rdo_Val').is(':checked')){
				 data["grpBookingReq.minPaymentRequired"] = $("#minPayReq_Val").val();
				 data["grpBookingReq.agreedFarePercentage"] = false;
				 }
			if ($('#rdo_Pct').is(':checked')) {
				 data["grpBookingReq.minPaymentRequired"] = $("#minPayReq_Pct").val();
				 data["grpBookingReq.agreedFarePercentage"] = true;
				 } 
			data["grpBookingReq.targetPaymentDateStr"] =convertDate($("#tagetPayDate").val());
			data["grpBookingReq.fareValidDateStr"] =convertDate($("#fareValidDate").val());
		
			$.ajax({ 
				url : 'manageGroupBooking!approve.action',
				beforeSend : ShowProgress(),
				data : data,
				type : "POST",
				async:false,
				dataType : "json",
				complete:function(response){
					InfoMsg("Group request approved successfully");
					resetFormValues();
					resetDate();
					searchGrpRequest();
					HideProgress();
				}
			});
		}
	}
	
	updateAdminRejection=function(){
		if(validateRejectForm()){
			var data={};
			
			data["selectedDetails"] = JSON.stringify(getSelectedData());
			data["grpBookingReq.version"] = selectedRowData.groupRequest.version;
			data["grpBookingReq.requestID"] =$("#requestID").val();
			data["grpBookingReq.statusID"] =2; //Rejection
			data["grpBookingReq.approvalComments"] =$("#adminRemarks").val();
			$.ajax({ 
				url : 'manageGroupBooking!adminStatusUpdate.action',
				beforeSend : ShowProgress(),
				data : data,
				type : "POST",
				async:false,
				dataType : "json",
				complete:function(response){
					InfoMsg("Group request reject successfully");
					resetFormValues();
					resetDate();
					searchGrpRequest();
					HideProgress();
				}
			});
		}
	}
	
	updateAdminReQuote=function(){
		if(validateRequoteForm()){
			var data={};
			
			data["selectedDetails"] = JSON.stringify(getSelectedData());
			data["grpBookingReq.version"] = selectedRowData.groupRequest.version;
			data["grpBookingReq.requestID"] =$("#requestID").val();
			data["grpBookingReq.statusID"] =3; //Re - Quote
			data["grpBookingReq.approvalComments"] =$("#adminRemarks").val();
			$.ajax({ 
				url : 'manageGroupBooking!adminStatusUpdate.action',
				beforeSend : ShowProgress(),
				data : data,
				type : "POST",
				async:false,
				dataType : "json",
				complete:function(response){
					InfoMsg("Group request re-quote successfully");
					resetFormValues();
					resetDate();
					searchGrpRequest();
					HideProgress();
				}
			});
		}
	}
	releasedSeat=function(){
	
		var data={};
		
		data["selectedDetails"] = JSON.stringify(getSelectedData());
		data["grpBookingReq.version"] = selectedRowData.groupRequest.version;
		data["grpBookingReq.requestID"] =$("#requestID").val();
		data["grpBookingReq.statusID"] =7; //Re - Quote
		data["grpBookingReq.approvalComments"] =$("#adminRemarks").val();
		$.ajax({ 
			url : 'manageGroupBooking!adminStatusUpdate.action',
			beforeSend : ShowProgress(),
			data : data,
			type : "POST",
			async:false,
			dataType : "json",
			complete:function(response){
				InfoMsg("Group booking released seat successfully");
				resetFormValues();
				resetDate();
				searchGrpRequest();
				HideProgress();
			}
		});
		
	}
	validateApproveForm = function(){
		
		var valied = true;
		
		if($("#agreedFare").val()==""){
			showERRMessage("Please enter agreed fare");
			return false;
		}
		if($("#adminRemarks").val()==""){
			showERRMessage("Please enter administrator comments");
			return false;
		}
		 if ($('#rdo_Val').is(':checked')) {
			 if($("#minPayReq_Val").val()==""){
				 showERRMessage("Please give the value for Required Minimum Payment ");
				 return false;
		 }
			 }
		 if ($('#rdo_Pct').is(':checked')) {
			 if($("#minPayReq_Pct").val()==""){
				 showERRMessage("Please give the Percentage for Required Minimum Payment ");
				 return false;
			 }
		 } 
		if($("#tagetPayDate").val()==""){
			showERRMessage("Please select target payment date");
			return false;
		}		
		if($("#fareValidDate").val()==""){
			showERRMessage("Please select fare valid date");
			return false;
		}	
		 if( Number($("#minPayReq_Val").val())>Number($("#agreedFare").val()) ){
			 showERRMessage("Minimum payment should be less than the agreed fare");
			 return false;
		 }
		 if(Number($("#minPayReq_Pct").val())>100 ){
			 showERRMessage("Minimum payment percentage should be less than 100 % ");
			 return false;
		 } 
		var rowIDs=$("#subRequestGrid").getDataIDs();
		for(var i=0;i<rowIDs.length;i++){
			var selIndex = i+1;
			if (($("#"+rowIDs[i]+"_approvedFare").val()==0)&&($("#"+selIndex+"_select").is(':checked'))){
				showERRMessage("The approved fare of the selected request cannot be zero");
				return false;
			}
		}
		 
		return valied;
	}

	
	validateRequoteForm = function(){
		
		var valied = true;
	
		if($("#adminRemarks").val()==""){
			showERRMessage("Please enter administrator comments");
			return false;
		}
		
		return valied;
	}
	validateRejectForm = function(){
		
		var valied = true;
		
		if($("#adminRemarks").val()==""){
			showERRMessage("Please enter administrator comments");
			return false;
		}
	
		
		return valied;
	}
	getSubmitData=function(){
		var data={};
	

		var routes = [":cmb,groupBookingRoutes.arrival:shj,groupBookingRoutes.departureDate:10/10/2014,groupBookingRoutes.arrivateDate:10/10/2014"];
		data["grpBookingReq.version"] = "02/10/2013";
		data["grpBookingReq.paymentStatus"] = 1;
		data["grpBookingReq.adultAmount"] =$("#adultAmount").val();
		data["grpBookingReq.childAmount"] =$("#childAmount").val();
		data["grpBookingReq.infantAmount"] =$("#infantAmount").val();
		data["grpBookingReq.adultFare"] =1;
		data["grpBookingReq.childFare"] =1;
		data["grpBookingReq.infantFare"] =1;
		data["grpBookingReq.isRollForward"] =0;
		data["grpBookingReq.requestID"] =-1;
	
		data["groupBookingRoutes[0].arrival"] ='shj';
		data["groupBookingRoutes[0].departure"] ='cmb';
		data["groupBookingRoutes[0].departureDate"]='10/10/2014';
		data["groupBookingRoutes[0].arrivateDate"]='10/10/2014';
		
		return data;
	};
	
		
	searchGrpRequest = function(){
		var selectedTab=$("#groubBookingTabs").tabs('option', 'selected');
		triggerSearch(selectedTab);
	};
	
	getSearchData=function(){
		var data={};
		data["groupBookingRoutes[0].arrival"] =$("#selTo").val();
		data["groupBookingRoutes[0].departure"] =$("#selFrom").val();
		data["grpBookingReq.statusID"] =$("#status").val();
		data["grpBookingReq.requestID"] =$("#txtReqId").val();
		data["grpBookingReq.craeteUserCode"] = $("#travelAgent").val();
		data["grpBookingReq.stationCode"] = $("#station").val();
		data["grpBookingReq.requestedAgentCode"] = $("#travelAgent").val();
		data["mainReqestOnly"] = $('#chkMainReqestOnly').is(":checked");
		data["sharedRequest"] = $("#chkShared").is(":checked");
		return data;
	}

	
	showAddEditGroupBooking = function(type){
		resetDate();
		if ($(".ui-state-highlight").length == 0){
			showERRMessage("Select a row to approve or reject");
		}else{
			setValuesToForm();
		}
		chkStatus();
	};
	
	hideAddEditGrpB = function(){
		$(this).find("img").attr("src", "../../images/tri-down1_no_cache.gif");
		$("#grpBookingDetails").parent().hide();
		$("#divGrpBSearch").show();
		$("#jqGridGrpBHistoryContainer").empty();
	};
	
	statusFomatter = function(cellVal, options, rowObject){
		var treturn = "&nbsp;";
		if (cellVal!=null){
			for(var i=0; i<status.length; i++) {
				if(status[i][0]==cellVal){
					treturn = status[i][1];
				}
			}
		}
		return treturn;
	}
	
	constructGrpReqGrid = function(){	
		var reqData=getSearchData();
		var dateFomatter = function(cellVal, options, rowObject){
			var treturn = "&nbsp;";
			if (cellVal!=null){
				treturn = cellVal.split("T")[0];
			}
			return treturn;
		}
		
		for(var di=0;di<tabbedPaneDataMap.length;di++){
			
			var dataObj = tabbedPaneDataMap[di];
			reqData["grpBookingReq.requestIDs"] = dataObj.statusIDs;
			$("#"+dataObj.gridName).jqGrid({
				datatype: 'json',
				width: '100%',
				colNames:[ 'Request ID' , 'OND' , 'Departure Date' , 'AD/CH/IN',
				           'OAL Fare', 'Req. Fare', 'Main Request ID', 'Requested Agent', "Status"],
				colModel:[
				    {name:'requestID', index:'promoCode', width:70,jsonmap:"groupRequest.requestID", align:"center"},
				    {name:'onds', width:100, align:"center",jsonmap:"groupRequest.firstRouteForDisplay.ondCode"},
				    {name:'departureDate', width:150, align:"center",jsonmap:"groupRequest.firstRouteForDisplay.departureDate",formatter:'date'},
					{name:'paxCount', index:'regFrom', width:60, align:"center",jsonmap:"groupRequest.paxCount"},
					{name:'oalFare',  width:60, align:"center",jsonmap:"groupRequest.oalFare"},
					{name:'reqFare', index:'reqFare', width:60, align:'center',jsonmap:"groupRequest.requestedFare"},
					{name:'mainRequestID',  width:60, align:"center",jsonmap:"groupRequest.mainRequestID"},
					{name:'requestedAgentCode', width:180, align:"center",jsonmap:"groupRequest.requestedAgentCode"},
					{name:'paymentStatus', index:'status', width:120, align:"center",jsonmap:"groupRequest.statusID",formatter:statusFomatter}
				],
				imgpath: "",
				url:"",
				pager: jQuery('#'+dataObj.pagerName),
				onPaging:function(pageButton){
					changePageNumber(pageButton);
					searchGrpRequest();
					return "stop";
				},
				multiselect: false,
				viewrecords: true,
				rowNum:20, 
				sortable:true,
				altRows:true,
				async:false,
				altclass:"GridAlternateRow",
				jsonReader: { 
					root: "rows",
					page: "page",
					total: "total",
					records: "records",
					repeatitems: false,
					id: "0" 
				},
				url:"",
				onSelectRow: function(rowid){
					var selectID=rowid%20;
					setSelectedRow(rowid-1);
				}
			
			}).navGrid("#"+dataObj.pagerName,{refresh: true, edit: false, add: false, del: false, search: false});
		}
	};
	
	setSelectedRow = function(id){
		var selectID=id%20;
		var selectedTab=$("#groubBookingTabs").tabs('option', 'selected');
		var tabName=tabbedPaneDataMap[selectedTab].gridName;
		selectedRowData = $.data($("#"+tabName)[0], "gridData")[selectID];
	};
	
	
	setValuesToForm = function(){
		
		resetFormValues();
		
		$("#grpBookingDetails").parent().show();
		$("#divGrpBSearch").parent().find("a>img").attr("src", "../../images/tri-right1_no_cache.gif");
		$("#divGrpBSearch").hide();
		$("#requestID").val(selectedRowData.groupRequest.requestID);		
		$("#agentRemarks").val(selectedRowData.groupRequest.agentComments);
		$("#adminRemarks").val(selectedRowData.groupRequest.approvalComments);
		$("#tagetPayDate").val(moment().add('days',paymenDate));
		$("#fareValidDate").val(moment().add('days',fareDate ));
		fillSubRequestData();

		var createUser=selectedRowData.groupRequest.craeteUserCode;
		

		if (selectedRowData.groupRequest.agreedFarePercentage) {
			$("#minPayReq_Pct").val(selectedRowData.groupRequest.minPaymentRequired);
			$("#rdo_Pct").trigger("click");
		}else{
			$("#minPayReq_Val").val(selectedRowData.groupRequest.minPaymentRequired);
			$("#rdo_Val").trigger("click");
		}  
		
		if(createUser!=null){
			$('#spanAgentDetails').multiline("Agent Name : "+ selectedRowData.groupRequest.agentName + "   |   Station : " + createUser.substring(3,6) );
			if(selectedRowData.groupRequest.statusID==8){
				var data={};
				data["userID"] =createUser;
				data["requestID"] =selectedRowData.groupRequest.requestID;
				getUserDetails(data);
			}
			else{
				 $("#spnPNR").text("");
				 $("#spnPayment").text("")
			}
		}
		
		if(selectedRowData.groupRequest.targetPaymentDate!=null){
			$("#tagetPayDate").val(selectedRowData.groupRequest.targetPaymentDate.split("T")[0]);
		}
		
		if(selectedRowData.groupRequest.fareValidDate!=null){
			$("#fareValidDate").val(selectedRowData.groupRequest.fareValidDate.split("T")[0]);
		}
		
		if(selectedRowData.groupRequest.statusID==0){
			
			$("#btnApprove,#btnReject,#btnRequote").show();
			$("#btnReleasedSeat").hide();
			setValuesToEdit();
			$("#spnPayment").multiline("");
		}
		else if(selectedRowData.groupRequest.statusID==1){
		
			$("#btnReject,#btnRequote").show();
			$("#btnApprove,#btnReleasedSeat").hide();
			setValuesToEdit();
			$("#spnPayment").multiline("");
		}
		else if(selectedRowData.groupRequest.statusID==2){
			
			$("#btnApprove,#btnRequote").show();
			$("#btnReject,#btnReleasedSeat").hide();
			setValuesToEdit();
			$("#spnPayment").multiline("");
			
		}
		else if(selectedRowData.groupRequest.statusID==3){ //  requote
			
			$("#btnApprove,#btnReject").show();
			$("#btnRequote,#btnReleasedSeat").hide();
			setValuesToEdit();
			$("#spnPayment").multiline("");
		}
		else if(selectedRowData.groupRequest.statusID==4){ // Pending after requote
		
			$("#btnApprove,#btnReject,#btnRequote").show();
			$("#btnReleasedSeat").hide();
			setValuesToEdit();
			$("#spnPayment").multiline("");
		
		}
		else if(selectedRowData.groupRequest.statusID==5){ // Request release seat
		
			$("#btnReleasedSeat,#btnReject,#btnRequote").show();
			$("#btnApprove").hide();
			setValuesToEdit();
			$("#spnPayment").multiline("");
		
		}
		else if(selectedRowData.groupRequest.statusID==6){ // withdraw
		
			$("#btnApprove,#btnReject,#btnRequote,#btnReleasedSeat").hide();
			setValuesToReadOnly();
			$("#spnPayment").multiline("");
		
		}
		else if(selectedRowData.groupRequest.statusID==7){ // Release seat request
		
			$("#btnReleasedSeat,#btnApprove,#btnReject,#btnRequote").hide();
			setValuesToReadOnly();
			$("#spnPayment").multiline("");
		}
		else if(selectedRowData.groupRequest.statusID==8){
		
			$("#btnApprove,#btnReject,#btnRequote,#btnReleasedSeat").hide();

		}
		$("#jqGridGrpBHistoryContainer").empty();
		
	}
	
	getUserDetails=function(data){
	
		$.ajax({ 
				url : 'manageGroupBooking!getUserDetails.action',
				beforeSend : ShowProgress(),
				data : data,
				type : "POST",
				async:false,
				dataType : "json",
				complete:function(response){
					 var response = eval("(" + response.responseText + ")");
					 if(response.user!=null){
							$('#spanAgentDetails').multiline("Agent Name : "+ response.user.displayName + "   |    Station : " + response.user.agentCode.substring(3,6) );
					 }
					 if(response.pnr!=null&&response.pnr!=""){
						 $('#spnPNR').text("PNR : "+ response.pnr );
						 $("#spnPayment").text(response.paymentDetails);
					 }
					 else{
						 $("#spnPNR").text("");
						 $("#spnPayment").text("")
					 }
					 HideProgress();
				
					 
				}
		});
		
	}
	resetFormValues = function(){
		
		$("#requestID").val('');
		$("#adultCount").val('');
		$("#childCount").val('');
		$("#infantCount").val('');
		$("#oalFare").val('');
		$("#requestedFare").val('');
		$("#agentRemarks").val('');
		$("#adminRemarks").val('');
		$("#agreedFare").val('');
		$("#minPayReq_Val").val('');
		$("#minPayReq_Pct").val(''); 
		$("#subRequestGrid").clearGridData();
	}
	resetDate = function(){
		$("#tagetPayDate").val('');
		$("#fareValidDate").val('');
		
		$("#subRequestGrid").clearGridData();
			
	}
	
	setValuesToReadOnly =function(){
		$("#agentRemarks").attr('readonly','readonly');
		$("#adminRemarks").attr('readonly','readonly');
		$("#agreedFare").attr('readonly','readonly');
		$("#minPayReq_Val").attr('readonly','readonly');
		$("#minPayReq_Pct").attr('readonly','readonly'); 
		$("#tagetPayDate").attr('readonly','readonly');
		$("#fareValidDate").attr('readonly','readonly');
		$("#fareValidDate, #tagetPayDate").datepicker("disable");
	
	}
	setValuesToEdit=function(){
		$("#agentRemarks").removeAttr('readonly');
		$("#adminRemarks").removeAttr('readonly');
		$("#agreedFare").removeAttr('readonly');
		$("#minPayReq_Val").removeAttr('readonly');
		$("#minPayReq_Pct").removeAttr('readonly'); 
	//	$("#tagetPayDate").removeAttr('readonly');
	//	$("#fareValidDate").removeAttr('readonly');
		$("#fareValidDate, #tagetPayDate").datepicker("enable");
	}
	function loadShareStation() {
		var intHeight = 300;
		var intWidth = 400;
		var strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width='
		+ intWidth
		+ ',height='
		+ intHeight
		+ ',resizable=no,top='
		+ ((window.screen.height - intHeight) / 2)
		+ ',left='
		+ (window.screen.width - intWidth) / 2;
		var requestId =$("#requestID").val();
		var strFilename = "showLoadJsp!loadGroupBookingStationAssinment.action?requestID="+requestId;
		objWindow1 = window.open(strFilename, "CWindow1", strProp);
		
		objWindow1.window.onload = function() {
			objWindow1.document.getElementById('txtReqId').value = $("#requestID").val();
			
	    }
	}
	
	loadHistoryData = function(){
		constructGrpReqHistoryGrid();
	};
	
	constructGrpReqHistoryGrid = function(){
		
		
		$("#jqGridGrpBHistoryContainer").empty();
		var gridTable = $("<table></table>").attr("id", "jqGridGrpBHistoryData");
		var gridPager = $("<div></div>").attr("id", "jqGridGrpBHistoryPages");
		$("#jqGridGrpBHistoryContainer").prepend(gridTable, gridPager);
		var data={};
		data["grpBookingReq.requestID"] =$("#requestID").val();
		var reqData=data;//
		var dateFomatter = function(cellVal, options, rowObject){
			var treturn = "&nbsp;";
			if (cellVal!=null){
				treturn = cellVal.split("T")[0];
			}
			return treturn;
		}
		var statusFomatter = function(cellVal, options, rowObject){
			var treturn = "&nbsp;";
			if (cellVal!=null){
				for(var i=0; i<status.length; i++) {
					if(status[i][0]==cellVal){
						treturn = status[i][1];
					}
				}
			}
			return treturn;
		}
		var temGrid = $("#jqGridGrpBHistoryData").jqGrid({
			datatype: function(postdata) {
		        $.ajax({
		           url : 'manageGroupBooking!searchGroupBookingHistory.action',
		           beforeSend : ShowProgress(),
		           data: $.extend(postdata, reqData),
		           dataType:"json",
		           type : "POST",
				   complete: function(jsonData,stat){	        	  
		              if(stat=="success") {
		            	  mydata = eval("("+jsonData.responseText+")");
		            	  if (mydata.msgType != "Error"){
			            	 $.data(temGrid[0], "gridData", mydata.rows);
			            	 temGrid[0].addJSONData(mydata);
			            	
		            	  }else{
		            		  alert(mydata.message);
		            	  }
		            	  HideProgress();
		              }
		           }
		        });
		    },
			width: '100%',
			colNames:[ '&nbsp;', 'Modify by' ,'Modify Date', 'Details' ,'Status'],
			colModel:[
			    {name:'id',  index:'id', width:30, align:"center"},
			    {name:'modifyBy', width:150,jsonmap:"groupRequestHostory.addedBy", align:"left"},
				{name:'modifyDate',  width:170,jsonmap:"groupRequestHostory.addedDate",formatter:dateFomatter, align:"center"},
			    {name:'details', width:438,jsonmap:"groupRequestHostory.groupBookingDetails", align:"left"},
				{name:'status', width:130, align:"center",jsonmap:"groupRequestHostory.newStatusID", formatter:statusFomatter,align:"center"}
							
			],
			imgpath: "",
			pager: jQuery('#jqGridGrpBHistoryPages'),
			multiselect: false,
			viewrecords: true,
			rowNum:20, 
			sortable:true,
			altRows:true,
			altclass:"GridAlternateRow",
			jsonReader: { 
				root: "rows",
				page: "page",
				total: "total",
				records: "records",
				repeatitems: false,
				id: "0" 
			},
			onSelectRow: function(rowid){
				var selectID=rowid%20;
				setSelectedRow(rowid-1);
			}
		
		}).navGrid("#jqGridGrpBHistoryPages",{refresh: true, edit: false, add: false, del: false, search: false});
	};
	
	addItemsToSelect = function(id){
		alert("Add");
	}
	removeItemsFromSelect= function(id){
		alert("remove");
	}
	
	function ShowProgress(){
		top[2].ShowProgress();
	}
	function HideProgress(){
		top[2].HideProgress();
	}
	function hideMessage() {
		top[2].HidePageMessage();
	}
	function InfoMsg(msg) {
		top[2].objMsg.MessageText = msg;
		top[2].objMsg.MessageType = "Confirmation";
		top[2].ShowPageMessage();
	}
}

UI_groupBooking = new UI_groupBooking();
UI_groupBooking.ready();