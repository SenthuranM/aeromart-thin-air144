function UI_groupBookingStations(){
	var dataMap = {
			Users: [ ],
			Stations: ["ABC","YOM"],
			
	}
	this.ready = function(){ 
		
		$("#btnSave").click(saveUserAssignment);
		$('#selUser').change(getUserAssignment);
		$(window).bind("load", function() {
			getUserAssignment();
		});
	}
	fillInitData = function(){
		$("#selUser").fillDropDown({firstEmpty:false, dataArray:dataMap.Stations , keyIndex:0, valueIndex:1});
		$("#selStation").fillDropDown({firstEmpty:false, dataArray:dataMap.Stations , keyIndex:0, valueIndex:1});
	}

	objectToArray = function(obj){
		rObj = [];
		$.each(obj, function(key, val){
			t = [];
			t[0] = key;
			t[1] = val;
			rObj[rObj.length] = t;
		})
		return rObj
	}
	function ShowProgress(){
	//	top[2].ShowProgress();
	}
	function HideProgress(){
	//	top[2].HideProgress();
	}
	function hideMessage() {
		top[2].HidePageMessage();
	}
	getSelectedSelectBoxData = function(selectBoxID) {
		var selector="#"+selectBoxID+" option:selected"
		var selectedItems=[];
		$(selector).each(function () {
		
			selectedItems.push(this.value);
			
		});
		for(i=0; i<selectedItems.length; i++) {
			if($("#spnStations").text()!=""){
				if($("#spnStations").text().indexOf(selectedItems[i])!=-1) {
					alert("Selected some stations already assign to user.")
					
					return null;
					break;
				}
			}
		}
		return JSON.stringify(selectedItems);
	}
	
	getUserAssignment = function(){
		var data={};
		data["requestID"] = $("#txtReqId").val();
		var targetURL = 'manageUserAssignment!findGroupStations.action';
			
			$.ajax({ 
				url : targetURL,
				beforeSend : ShowProgress(),
				data : data,
				type : "POST",
				async:false,
				dataType : "json",
				complete:function(response){
					 var responseData=$.parseJSON(response.response);
					 HideProgress();
					 if(responseData.msgType=="success"){
										
						 $("#spnStations").text(responseData.stationsExist);
						 
					 } else{
						 getGroupBookingBD
						 alert("System busy. Please try again later.");
						 
					 }
				}
			});
		
	}
	saveUserAssignment = function(){
		if (valadateForm()){
			var submitData = getDataFromPage();
			var targetURL;
		
		    targetURL = 'manageUserAssignment!assignGroupStations.action';
		
			
			
			$.ajax({ 
				url : targetURL,
				beforeSend : ShowProgress(),
				data : submitData,
				type : "POST",
				async:false,
				dataType : "json",
				complete:function(response){
					 var responseData=$.parseJSON(response.response);
					 HideProgress();
					 if(responseData.msgType=="success"){
						
						 getUserAssignment();
						 
						 alert("Group booking station assignment successful!");
					 } else{
						
							 alert("System busy. Please try again later.");
						 
					 }
					 HideProgress();
				}
			});
		}
	}
	valadateForm = function(){
	//	top[2].HidePageMessage();
		var valied = true;
		if($("#txtReqId").val()==""){
			alert("Please select the user");
			valied = false;
		}
		else if($("#selStation").val()==null){
			alert("Please select stations");
			valied = false;
		}
		return valied;
	}
	getDataFromPage = function(){
		var data={};
		if(getSelectedSelectBoxData("selStation")!=null){
			data["stations"] = getSelectedSelectBoxData("selStation");
		}
		data["requestID"] = $("#txtReqId").val();
		return data;
	}
	
	function SaveMsg(msg) {
		top[2].objMsg.MessageText = msg;
		top[2].objMsg.MessageType = "Confirmation";
		top[2].ShowPageMessage();
	}
}
UI_groupBookingStations = new UI_groupBookingStations();
UI_groupBookingStations.ready();