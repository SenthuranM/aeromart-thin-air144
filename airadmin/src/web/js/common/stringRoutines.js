<!--
//****************
//Sudheera Liyanage
//stringRoutines.js
//Common JS routines for string/array handling

//****************
function a(){
    return(new Array());
}

function rtrim( strValue ) {
	var objRegExp = /^([\w\W]*)(\b\s*)$/;
 
    if(objRegExp.test(strValue)) {
       strValue = strValue.replace(objRegExp, '$1');
    }
	return strValue;
}

function ltrim( strValue ) {
	var objRegExp = /^(\s*)(\b[\w\W]*)$/;
 
      if(objRegExp.test(strValue)) {
       strValue = strValue.replace(objRegExp, '$2');
    }
  return strValue;
}

function trim( strValue ) {
	var objRegExp = /^(\s*)$/;

    //check for all spaces
    if(objRegExp.test(strValue)) {
       strValue = strValue.replace(objRegExp, '');
       if( strValue.length == 0)
          return strValue;
    }
    
   //check for leading & trailing spaces
   objRegExp = /^(\s*)([\W\w]*)(\b\s*$)/;
   if(objRegExp.test(strValue)) {
       //remove leading and trailing whitespace characters
       strValue = strValue.replace(objRegExp, '$2');
    }
  return strValue;
}

//***********************************************
function hasSpace(str) {
    var len=str.length;
    for (var i=0;i<len;i++){
        if(str.substr(i,1)==' ') {
            return true;
        }
    }
    return false;
}

//***********************************************
function getIndex(ele,arr) {
    var len=arr.length;

    for (var i=0;i<len;i++) {
        if(arr[i]==ele){
            return i;
        }
    }
    return -1;
}

//***********************************************
function getDate(txt) {
	if(txt.disabled)return;
    window.dateField=txt;

    var y=window.screen.height;
    var x=window.screen.width;
	var w=275
	var h=175;
	var options;

    x=(x/2)-Math.round(w/2);
    y=y/2-Math.round(h/2);
    
	options="width="+w+"px,height="+h+"px,scrollbars=1,left=" + x + ",top="+ y + ",resizable=0,toolbar=0,menubar=0";
    //alert(options);

    if (window.winDate) {
        if(! window.winDate.closed) {
            winDate.focus();
            return;
        }
    }
    window.winDate = window.open('../calendar/calendar.html','newDate',options);
}

//***********************************************
function showWindow(mode,url,h,w,anyObj) {
    var options=getOptions(mode,h,w);
    if(mode.indexOf("MODAL")!=-1){
        window.showModalDialog(url,anyObj,options);
    }else{
        var win=window.open(url,anyObj,options);
		win.focus();
    }
}

//***********************************************
function showUserDialog(txtId,txtName,qStr) {
	var mode="WINDOW";
	var url="../dialog/getUser/getUser_frm.jsp?" + qStr;
    
     window.txtId=txtId;
     window.txtName=txtName;
     showWindow(mode,url,"500","270","getUser") 
}

//***********************************************
function setMessage(msg,left,top) {
    objmsg = new message(divmsg,divdata);
    objmsg.setMessage(msg);
    if (left && (left !='')) objmsg.left = left;
    if (top && (top !=''))  objmsg.top = top;
    objmsg.show();
}

//***********************************************
function isMember(ele,arr) {
    var len = arr.length;

    for (var i=0; i<len; i++) {
        if (arr[i] == ele) {
            return true;
        }
    }
    return false;
}

//***********************************************
function getSubArray(arr,ele) {
    var len=arr.length;

    for(var i=0;i<len;i++) {
        if (arr[i][0]==ele) {
            return arr[i];
        }
    }
    return null;
}

//***********************************************
function hasElement(arr,ele) {
    var len=arr.length;

    for (var i=0;i<len;i++) {
        if (arr[i]==ele) {
            return true;
        }
    }
    return false;
}

//***********************************************
function getMonthArray(type){
    var arr;
    var arrMonth = new Array("January","February","March","April","May","June","July","August","September","October","November","December");
    var arrShMonth = new Array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");


    if(type=="STR_FULL"){
        arr=arrMonth;
    }else if(type=="STR_SHORT"){
        arr=arrShMonth;
    }else{
        alert("getMonthArray:Invalid type - " + type);
    }

    return arr;
}

//***********************************************
function formatDate(dt,fmt1,fmt2){
    var dd1,mm1,yy1;
    var dd2,mm2,yy2;
    var dt2,sep;

    if(fmt1=="dd-mmm-yyyy"){
        dd1=dt.substr(0,2);
        mm1=dt.substr(3,3);
        yy1=dt.substr(7,4);
    }
    else if(fmt1=="dd-mmm-yy"){
        dd1=dt.substr(0,2);
        mm1=dt.substr(3,3);
        yy1=dt.substr(7,2);
    }
    else if((fmt1=="dd/mm/yyyy")||(fmt1=="dd-mm-yyyy")){
        dd1=dt.substr(0,2);
        mm1=dt.substr(3,2);
        yy1=dt.substr(6,4);
    }
    else if((fmt1=="dd/mm/yy")||(fmt1=="dd-mm-yy")){
        dd1=dt.substr(0,2);
        mm1=dt.substr(3,2);
        yy1=dt.substr(6,2);
    }
    else if((fmt1=="yyyy/mm/dd")||(fmt1=="yyyy-mm-dd")){
        dd1=dt.substr(8,2);
        mm1=dt.substr(5,2);
        yy1=dt.substr(0,4);
    }
    else if((fmt1=="yy/mm/dd")||(fmt1=="yy-mm-dd")){
        dd1=dt.substr(6,2);
        mm1=dt.substr(3,2);
        yy1=dt.substr(0,2);
    }
    else{
       // alert("formatDate:Invalid format1:" + fmt1);
    }
    //***********************

    //get seperator
    if(fmt2.indexOf("/")!=-1){
        sep="/";
    }else{
        sep="-";
    }

    //get year
    if(fmt2.indexOf("yyyy")!=-1){
        if(yy1.length==2){
            if(Number(yy1)<30){
                yy2=2000;
            }else{
                yy2=1900;
            }
            yy2+=Number(yy1);
        }else{
            yy2=yy1;
        }
    }else{
        if(yy1.length==4){
            yy2=yy1.substr(2,2);
        }else{
            yy2=yy1;
        }
    }

    //get month
    if(fmt2.indexOf("mmm")!=-1){
        if(mm1.length==2){
            mm2=getStrMonth(mm1);
        }else{
            mm2=mm1;
        }
    }
    else{
        if(mm1.length==3){
            mm2=getNoMonth(mm1);
            return (yy1+"/"+mm2+"/"+dd1);
        }else{
            mm2=mm1;
        }
    }

    //get day
    dd2=dd1;

    //date

    if(fmt2.indexOf("yy")<4){
        dt2=yy2+sep+mm2+sep+dd2;
    }else{
        dt2=dd2+sep+mm2+sep+yy2;
    }

    return dt2;
}

//***********************************************
function getNoMonth(mm){
    var arr=getMonthArray("STR_SHORT");

    for(var i=0;i<12;i++){
        if(arr[i]==mm){
            i++;
            if(i<10){
                i="0"+i;
            }
            return i;
        }
    }
  //  alert("getNoMonth:Invalid month:" + mm);
    return "00";
}

//***********************************************
function getStrMonth(mm){
    var arr=getMonthArray("STR_SHORT");

    mm=Number(mm);
    if(!(mm<=12 && mm>=1)){
    //    alert("getStrMonth:Invalid month:" + mm);
    }
    return arr[mm-1];

}

//***********************************************
function compareDates(dt1,dt2,opr){
    var rtn;

    if((!opr)||(opr=="")){
        opr="=";
    }
    dt1=formatDate(dt1,"dd-mmm-yyyy","yyyy/mm/dd");
    dt2=formatDate(dt2,"dd-mmm-yyyy","yyyy/mm/dd");
    switch (opr){
        case">":
            rtn=(dt1>dt2);
            break;
        case">=":
            rtn=(dt1>=dt2);
            break;
        case"<":
            rtn=(dt1<dt2);
            break;
        case"<=":
            rtn=(dt1<=dt2);
            break;
        default:
            rtn=false;
            alert("compareDates:Invaliod operator:"+opr);
    }
    return rtn;
}

//***********************************************
function getMaxVal(no,dec){
    var n="";

    for(var i=0;i<no;i++){
        n+="9";
    }

    if(dec){
        if(!isNaN(dec)){
            n+=".";
            for(var i=0;i<dec;i++){
                n+="9";
            }
        }
    }
    return n;
}

//**********************************
function getWholeNoSize(no){
    var idx=no.indexOf(".");
    return Number((idx==-1)?no.length:no.substr(0,idx).length);
}

//**********************************
function getDecimalSize(no){
    var idx=no.indexOf(".");
    return Number((idx==-1)?0:no.substr(idx+1).length);
}

//*****************
function getOptions(mode,h,w){
    var options;
    var width=800,height=580;
    var x=0,y=0;
    var ratio=screen.width/800;

    height=(!h)?"600":h;
	width=(!w)?"800":w;
	if(mode&&mode=="WINDOW")width-=10;

    height-=20;
    width=Math.round(width*ratio);
    height=Math.round(height*ratio);

    x=(screen.width-width)/2;
    y=(screen.height-height)/2;

	if(x<=10){
		x=0;
		y=0;
		if(mode&&mode=="WINDOW")height-=25;
	}

	if(mode&&mode=="MODAL"){
        options='toolbar:yes;menubar:yes;scrollbars:yes;status:no;help:no';
        options+=';dialogwidth:'+width+'px;dialogheight:'+height+'px';
        options+=';dialogtop:'+y+';dialogleft:'+x;
    }else{
        options='toolbar=0,menubar=0,scrollbars=1,status=0';
        options+=',left='+x+'px,top='+y+'px';
        options+=',width='+width+'px,height='+height+'px';
    }

    //alert(options);
    return options;
}

//*****************
function setTimeOptions(sel){
	var n=24;
	var idx=0;
	
	sel.options.length=n*4;
	
	for(var i=0;i<n;i++){
		var hh=((i<10)?"0":"")+i;
		
		sel.options[idx].value=hh+":00";
		sel.options[idx].text=hh+":00";
		idx++;
		
		sel.options[idx].value=hh+":15";
		sel.options[idx].text=hh+":15";
		idx++;
		
		sel.options[idx].value=hh+":30";
		sel.options[idx].text=hh+":30";
		idx++;
		
		sel.options[idx].value=hh+":45";
		sel.options[idx].text=hh+":45";
		idx++;
	}
	
	if(sel.defVal)setVal(sel.name,sel.defVal);
}

//-->