<!--
//validations.js
//Sudheera Liyanage

function validateUSPhone( strValue ) {
/************************************************
Ex. (999) 999-9999 or (999)999-9999
*/
	var objRegExp  = /^\([1-9][0-9]{2}\)\s?[0-9]{3}\-[0-9]{4}$/;

  	//check for valid us phone with or without space between
  	//area code
  	return objRegExp.test(strValue);
}

function  isNumeric( strValue ) {
	var objRegExp  =  /(^-?[0-9][0-9]*\.[0-9]*$)|(^-?[0-9][0-9]*$)|(^-?\.[0-9][0-9]*$)/;

  	//check for numeric characters
  	return objRegExp.test(strValue);
}

function validateInteger( strValue ) {
	var objRegExp  = /(^-?[0-9][0-9]*$)/;

  	//check for integer characters
  	return objRegExp.test(strValue);
}

function isValidIpAddress(strValue){
	var octet = '(?:25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]|[0-9])';
	var ip    = '(?:' + octet + '\\.){3}' + octet;
	var ipRE  = new RegExp( '^' + ip + '$' );
	return ipRE.test( strValue );
}


function validateValue(strValue,strMatchPattern ){
	var objRegExp = new RegExp( strMatchPattern);

 	//check if string matches pattern
 	return objRegExp.test(strValue);
}


function rightTrim(strValue){
	var objRegExp = /^([\w\W]*)(\b\s*)$/;

      if(objRegExp.test(strValue)) {
       //remove trailing a whitespace characters
       strValue = strValue.replace(objRegExp, '$1');
    }
  	return strValue;
}

function leftTrim(strValue){
	var objRegExp = /^(\s*)(\b[\w\W]*)$/;

  	if(objRegExp.test(strValue)) {
    	//remove leading a whitespace characters
       strValue = strValue.replace(objRegExp, '$2');
    }
  	return strValue;
}

function trim(strValue){
	var objRegExp = /^(\s*)$/;

    //check for all spaces
    if(objRegExp.test(strValue)) {
       strValue = strValue.replace(objRegExp, '');
       if( strValue.length == 0)
          return strValue;
    }

   //check for leading & trailing spaces
   objRegExp = /^(\s*)([\W\w]*)(\b\s*$)/;
   if(objRegExp.test(strValue)) {
       //remove leading and trailing whitespace characters
       strValue = strValue.replace(objRegExp, '$2');
    }

  	return strValue;
}

function currencyValidate(strValue, nValue, nDecimal){
	var strNValue = "*"
	var strDValue = "*" ;
	var strPattern = "" ;
	
	if (nValue != ""){
		strNValue = "{0," + nValue + "}";
	}
	
	if (nDecimal != ""){
		strDValue = "{0," + nDecimal + "}";
	}
	strPattern = "(^[0-9]" + strNValue + "\\.[0-9]" + strDValue + "$)|(^[0-9]" + strNValue + "$)|(^\\.[0-9]" + strDValue + "$)";
	return validateValue(strValue, strPattern);
}

function anyCurrencyValidate(strValue, nValue, nDecimal){
	var strNValue = "*"
	var strDValue = "*" ;
	var strPattern = "" ;
	
	if (nValue != ""){
		strNValue = "{0," + nValue + "}";
	}
	
	if (nDecimal != ""){
		strDValue = "{0," + nDecimal + "}";
	}
	strPattern = "(^[-+]?[0-9]" + strNValue + "\\.[0-9]" + strDValue + "$)|(^[-+]?[0-9]" + strNValue + "$)|(^\\.[0-9]" + strDValue + "$)";
	return validateValue(strValue, strPattern);
}

function isEmpty(s) {return (trim(s).length==0);}
function hasWSpace(s){return RegExp("^\s*$").test(s);}
function isAlpha(s){return RegExp("^[a-zA-Z]+$").test(s);}
function isAlphaWhiteSpace(s){return RegExp("^[a-zA-Z-/ \w\s]+$").test(s);}
function isInt(n){return RegExp("^[-+]?[0-9]+$").test(n);}
function isPositiveInt(n){return RegExp("^[+]?[0-9]+$").test( n );}
function isNegativeInt(n){return RegExp("^-[0-9]+$").test(n);}
function isAlphaNumeric(s) {return RegExp("^[a-zA-Z0-9-/]+$").test(s);} 
function isAlphaNumericWhiteSpace(s){return RegExp("^[a-zA-Z0-9-/ \w\s]+$").test(s);}
function isAlphaNumericWhiteSpaceAnper(s){return RegExp("^[a-zA-Z0-9-/ \& \w\s]+$").test(s);}
function isAlphaNumericWhiteSpaceUnderscore(s){return RegExp("^[a-zA-Z0-9_\& \w\s]+$").test(s);}
function isAlphaNumericUnderscore(s) { return RegExp("^[a-zA-Z0-9_]+$").test(s);} 

function isDecimal(n){return RegExp("^[-+]?[0-9]+[.][0-9]+$").test(n);}
function isLikeDecimal(n){
	//window.status=isInt(n)+'||'+RegExp("^[-+]?[0-9]+[.]+$").test(n)+'||'+isDecimal(n);
	return isInt(n)||RegExp("^[-+]?[0-9]+[.]$").test(n)||isDecimal(n);
}
function isPositiveDecimal(n){return (RegExp("^[+]?[0-9]+[.][0-9]+$").test(n));}
function isNegativeDecimal(n){return RegExp("^-[0-9]+[.][0-9]+$").test(n);}

function checkTime(s){ return RegExp( "^[012][0-9]:[0-5][0-9]$" ).test( s );}
function checkEmail(s){return RegExp("^[a-z0-9!#$%&'*+/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$").test(s);}

//**********************
function attachEvents(){
    document.onkeypress=checkOnKeyPress;
    //document.oncontextmenu=stopRightMouse;
	document.body.onpaste=validatePaste;
}	


//**********************
function checkOnKeyPress() {
    var e=event;
    var kCode=e.keyCode;
    var keys1=[34,39,60,62,94,126];
    //alert("validKeysOnly=" + kCode);

    if (kCode=="13"){
		onEnterKeyPressed();
		return;
	}
    keys1="_"+keys1.join("_")+"_";
    if(keys1.indexOf("_"+kCode+"_")!=-1)e.returnValue=false;
}


//**********************
function validatePaste() {
	
    var val=window.clipboardData.getData("Text");
    var obj=event.srcElement
    var tag=obj.tagName;

    if((tag=="INPUT")||(tag=="TEXTAREA")){
        if((obj.type.indexOf("text")>-1)||obj.type=="password"){
			window.clipboardData.setData("Text",removeInvalids(val));
        }
    }
}

//**********************
function removeInvalids(val){
    var re1=/'|"/g;
    var re2=/<|>|~|\^/g;

    val=val.replace(re1,"");
    val=val.replace(re2,"-");
    return val;
}


//**********************
function stopRightMouse(){
    var e=event;
    var obj=e.srcElement;
    var tag=obj.tagName;

    if((tag=="INPUT")||(tag=="TEXTAREA")){
        if((obj.type.indexOf("text")>-1)||obj.type=="password"){
            return true;
        }
    }
    return false;
}

//**********************
function doAfterLoad(){
	attachEvents();
}

//***********************************************
function isError(e,msg,id){
    var blnErr=false;
    var val=getVal(id);
    var obj=getField(id);

    if(e=="NULL"){
        if (isEmpty(val))blnErr=true;
    }else if(e=="NOT_ALPHA"){
        blnErr=!isAlpha(val);
    }else if(e=="NOT_ALPHANUMERIC"){
        blnErr=!isAlphaNumeric(val);
    }else if(e=="NaN"){
            blnErr=(isNaN(val));
    }else if(e=="NOT_FLOAT"){
            blnErr=(!isNaN(val))&&(val.indexOf(".")!=-1);
    }else if(e.substr(0,6)=="MAXVAL"){
        blnErr=(Number(val)>Number(e.substr(6)));
	}else if(e.substr(0,6)=="MINVAL"){
        blnErr=(Number(val)<Number(e.substr(6)));
    }else if(e.substr(0,6)=="MAXLEN"){
        blnErr=(val.length>Number(e.substr(6)));
    }else if(e.substr(0,6)=="MINLEN"){
        blnErr=(val.length<Number(e.substr(6)));
    }else if(e.substr(0,6)=="WHITESPACE"){
        blnErr=hasWSpace();
    }else{
        alert("isErr:"+e+":"+"invalid error type" );
    }

    if (blnErr){
        showERRMessage(msg);
		if(obj[0]){
			if (obj.tagName=="SELECT"){
				obj.focus();
			}else{
				obj[0].focus()
			}
		}else if(!(obj.readOnly||obj.disabled||obj.type=='hidden'||obj.style.visibility=='hidden'||obj.style.display=='none')){
			obj.focus();
		}
    }else{
        setField(id,trim(val));
    }

    return blnErr;
}


function onEnterKeyPressed(){

	//override this function if you need in you JS file
}

function removeChars(str,regX){
	return str.replace(new RegExp(regX),"");
}

function keyPressCheckEmail(){
	var obj=window.event.srcElement;
	if(!isEmpty(obj.value)&&!checkEmail(obj.value)){
		alert('Invalid Email Address');
		obj.focus();
	}
}

/*
 * Added  by Srikanth
 * This function is used to display the message in the proper place in the page.
*/
function validateTextField(textValue,msg) {
	var blnStatus = false;
	if(textValue == null || textValue == "")
	{
		top[2].objMsg.MessageText = msg ;
		top[2].objMsg.MessageType = "Error" ; 
		top[2].ShowPageMessage();
		blnStatus = true;
	}
	return blnStatus;
}

function showCommonError(msgType,msg) {
	top[2].objMsg.MessageText = msg ;
	top[2].objMsg.MessageType = msgType; 
	top[2].ShowPageMessage();
	return true;
}

function showCommonAlert(msgType,msg) {
	top[2].objMsg.MessageText = msg ;
	top[2].objMsg.MessageType = msgType; 
	top[2].ShowPageAlertMessage();
	return true;
}


function showWindowCommonError(msgType,msg) {
	objMsg.MessageText = msg;
	objMsg.MessageType = msgType; 
	ShowPageMessage();
	return true;
}	

// to show the error messages - default msgType is Error
function showERRMessage(strErrMessage) {
	top[2].objMsg.MessageText = strErrMessage;
	top[2].objMsg.MessageType = "Error" ; 
	top[2].ShowPageMessage();
	return true;
}

function clearERRMessage() {		
	top[2].MessageResetForStatusBar();
	return true;
}

//to show the error messages - default msgType is Error
function showERRAlertMessage(strErrMessage) {
	top[2].objMsg.MessageText = strErrMessage;
	top[2].objMsg.MessageType = "Error" ; 
	top[2].ShowPageAlertMessage();
	return true;
}

function hideERRMessage() {
	top[2].HidePageMessage();
}

//TIME Validation............
function IsValidTime(timeStr) {	
	
	// Checks if time is in HH:MM:SS AM/PM format. H can have more than one digit up to 4 digits.
	// The seconds and AM/PM are optional.
	var timePat = /^(\d{1,}):(\d{2})(:(\d{2}))?(\s?(AM|am|PM|pm))?$/;
	var matchArray = timeStr.match(timePat);
	if (matchArray == null) {
		//alert("Time is not in a valid format.");
		return false;
	}
	hour = matchArray[1];
	minute = matchArray[2];
	second = matchArray[4];
	ampm = matchArray[6];
	
	if (second=="") { second = null; }
	//if (ampm=="") { ampm = null }
	if (hour < 0  || hour > 23) {
		//alert("Hour must be between 1 and 12. (or 0 and 23 for military time)");
		return false;
	}
	//if (hour <= 12 && ampm == null) {
	//if (confirm("Please indicate which time format you are using.  OK = Standard Time, CANCEL = Military Time")) {
	//alert("You must specify AM or PM.");
	//return false;
	//   }
	//}
	//if  (hour > 12 && ampm != null) {
	//alert("You can't specify AM or PM for military time.");
	//return false;
	//}
	if (minute<0 || minute > 59) {
		//alert ("Minute must be between 0 and 59.");
		return false;
	}
	if (second != null && (second < 0 || second > 59)) {
		//alert ("Second must be between 0 and 59.");
		return false;
	}
	return true;
}

function IsValidDateTime(timeStr) {	
	
	// Checks if time is in HH:MM:SS AM/PM format. H can have more than one digit up to 4 digits.
	// The seconds and AM/PM are optional.
	var timePat = /^(\d{1,}):(\d{2})(:(\d{2}))?(\s?(AM|am|PM|pm))?$/;
	var matchArray = timeStr.match(timePat);
	if (matchArray == null) {
		//alert("Time is not in a valid format.");
		return false;
	}
	hour = matchArray[1];
	minute = matchArray[2];
	second = matchArray[4];
	ampm = matchArray[6];
	
	if (second=="") { second = null; }
	//if (ampm=="") { ampm = null }
	if (hour < 0  || hour > 9999) {
		 
		return false;
	}
	 
	if (minute<0 || minute > 59) {
		//alert ("Minute must be between 0 and 59.");
		return false;
	}
	if (second != null && (second < 0 || second > 59)) {
		//alert ("Second must be between 0 and 59.");
		return false;
	}
	return true;
}



function customValidate(objTxt,regEx){
	var strValue = objTxt.value;
		objTxt.value = removeChars(strValue,regEx);
}

function alphaValidate(objTxt){
	var strValue = objTxt.value;
	if (!isAlpha(strValue)) {
		objTxt.value = removeChars(strValue,/[0-9`!@#$\ss&*?\[\]{}()|\\\/+=:.,;^~_-]/g);
	}	
}

function alphaWhiteSpaceValidate(objTxt){
	var strValue = objTxt.value;
	if (!isAlphaWhiteSpace(strValue)) {
		objTxt.value = removeChars(strValue,/[0-9`!@#$%&*?\[\]{}()|\\\/+=:.,;^~_-]/g);
	}	
}

function alphaNumericValidate(objTxt){
	var strValue = objTxt.value;
	if (!isAlphaNumericWhiteSpace(strValue)) {
		objTxt.value = removeChars(strValue,/[`!@#$%\s&*?\[\]{}()|\\\/+=:.,;^~_-]/g);
	}	
}

function alphaNumericWithoutSpaceValidate(objTxt){
	var strValue = objTxt.value;
	if (!isAlphaNumeric(strValue)) {
		objTxt.value = removeChars(strValue,/[`!@#$%&*?\[\]{}()|\\\/+=:.,;^~_-]/g);
	}	
}

function commaValidate(objTxt){
	var strValue = objTxt.value;
	if (!isAlphaNumeric(strValue)) {
		objTxt.value = removeChars(strValue,/[,]/g);
	}	
}


function positiveWholeNumberValidate(objTxt){
	var strValue = objTxt.value;
	if (!isPositiveInt(strValue)) {
		objTxt.value = removeChars(strValue,/[a-zA-Z`!@#$%\s&*?\[\]{}()|\\\/+=:.,;^~_-]/g);
	}	
}

function positiveWholeNumberValidate2(objTxt){
	var strValue = objTxt.value;
	if (!isPositiveInt(strValue)) {
		objTxt.value = removeChars(strValue,/[a-zA-Z`!@#$%\s&*?\[\]{}()|\\\/+=.,;^~_-]/g);
	}	
}

function textCounter(field, countfield, maxlimit) {
	if (field.value.length > maxlimit) {// if too long...trim it! 
		field.value = field.value.substring(0, maxlimit);
	}// otherwise, update 'characters left' counter
	else {
		countfield.value = maxlimit - field.value.length;
	}
}

function validateTextArea(objControl) {
	var strValue = objControl.value;
	var strLength = strValue.length;
	/*if (!isAlphaNumericWhiteSpace(strValue)) {
		objControl.value = strValue.substr(0,strValue.length - 1);
	}*/	
	if (strLength > 255)
	{
		strValue = strValue.substr(0,255);
		objControl.value = strValue;
	}
}
function setCursorPos(el, st, end){
	if(el.setSelectionRange) {
		el.focus();
		el.setSelectionRange(st,end);
	}else {
		if(el.createTextRange) {
			range=el.createTextRange();
			range.collapse(true);
			range.moveEnd('character',end);
			range.moveStart('character',st);
			range.select();
		}
	}
}


function removeAvoidableChar(objControl){
	var strValue = objControl.value;
	var replaced =strValue.replace(/[~,]/,"")
	objControl.value=replaced;
	setCursorPos(objControl, replaced.length,replaced.length);
} 

function validateAlphaNumeric(objControl){
	var strValue = objControl.value;
	var strLength = strValue.length;
	if(!isAlphaNumeric(strValue)){
		objControl.value = removeChars(strValue,/[`!@#$%\s&*?\[\]{}()|\\\/+=:.,;^~_-]/g);
	}

}

function setTimeWithColon(txtField) {
	
	var strTime = txtField.value;
	
	if(trim(strTime) != "") {
		var index = strTime.length - 2;
		var hourindex = strTime.search(":");
		if (hourindex != -1) {
			txtField.value = strTime;
		} else {
			var mn = "00";
			if(strTime.length == 3 || strTime.length == 4)
				mn = strTime.substr(index,2);

			var hr = 0;
			if(strTime.length == 3) {
				hr =  strTime.substr(0,1); 
			}else {
				hr = strTime.substr(0,2); 
			}
			var timecolon = hr +":" + mn ;
			txtField.value = timecolon;
			strTime = timecolon;
		}
	}
	return IsValidTime(strTime);
}

function setTimeWithColon2(txtField) {
	
	var strTime = txtField.value;
	
	if(trim(strTime) != "") {
		var index = strTime.length - 2;
		var hourindex = strTime.search(":");
		if (hourindex != -1) {
			txtField.value = strTime;
		} else {
			var mn = "00";
			if(strTime.length == 4 || strTime.length == 5)
				mn = strTime.substr(index,2);

			var hr = 0;
			if(strTime.length == 3) {
				hr =  strTime.substr(0,1); 
			}
			if (strTime.length == 4) {
				hr =  strTime.substr(0,2); 
			}
			else {
				hr = strTime.substr(0,3); 
			}
			var timecolon = hr +":" + mn ;
			txtField.value = timecolon;
			strTime = timecolon;
		}
	}
	return IsValidTime(strTime);
}

function setNewTimeWithColon(txtField,maxLength) {
	
	var strTime = txtField.value;
	
	if(trim(strTime) != "") {
		var index = strTime.length - 2;
		var hourindex = strTime.search(":");
		if (hourindex != -1) {
			txtField.value = strTime;
		} else {
			var mn = "00";			
			
			var hr = 0;

			if(maxLength == 5) {
			
				if(strTime.length == 3 || strTime.length == 4) {
					mn = strTime.substr(index,2);
				}
				if(strTime.length == 3) {
					hr =  strTime.substr(0,1); 
				}else {
					hr = strTime.substr(0,2); 
				} 
			} else {
			
				if((strTime.length >= 3) && (strTime.length <= 7)) {					
					if(strTime.length == 7) {
			   			mn = strTime.substr(index-1,2);
			   		}
					else {
						mn = strTime.substr(index,2);
					}
				}
				if((strTime.length >= 3) && (strTime.length <= 7)) {
					
					if(strTime.length == 7) {
						hr =  strTime.substr(0, index-1); 
					}
					else {
						hr =  strTime.substr(0, index);
					}
					
				} else {
					hr =  strTime.substr(0, 2); 
				 } 
			}
			
			var timecolon = hr + ":" + mn ;
			txtField.value = timecolon;
			strTime = timecolon;
		}
	}
		
	return IsValidTime(strTime);
}

function setTimeWithColon1(txtField,maxLength) {
	
	var strTime = txtField.value;
	
	if(trim(strTime) != "") {
		var index = strTime.length - 2;
		var hourindex = strTime.search(":");
		if (hourindex != -1) {
			txtField.value = strTime;
		} else {
			var mn = "00";			
			
			var hr = 0;

			if(strTime.length < 5) {
			
				if(strTime.length == 3 || strTime.length == 4) {
					mn = strTime.substr(index,2);
				}
				if(strTime.length == 3) {
					hr =  strTime.substr(0,1); 
				}else {
					hr = strTime.substr(0,2); 
				} 
			} else {
			
				if((strTime.length >= 3) && (strTime.length <= 10)) {					
					if(strTime.length == 10) {
			   			mn = strTime.substr(index-1,2);
			   		}
					else {
						mn = strTime.substr(index,2);
					}
				}
				if(((strTime.length >= 3) && (strTime.length <= 10))) {
					
					if(strTime.length == 10) {
						hr =  strTime.substr(0, index-1); 
					}
					else {
						hr =  strTime.substr(0, index);
					}
					
				} else {
					hr =  strTime.substr(0, 2); 
				 } 
				 			 
				 
			}
			
			var timecolon = hr + ":" + mn ;
			txtField.value = timecolon;
			strTime = timecolon;
		}
	}
		
	return IsValidTime(strTime);
}

function validateTextArea(objControl, length) {
	var strValue = objControl.value;
	var strLength = strValue.length;
	if (strLength > length)
	{
		strValue = strValue.substr(0,length);
		objControl.value = strValue;
	}
}

function validateReportDate(strFromDate, strTodate, repStartDate){	
	var tempDay;
	var tempMonth;
	var tempYear;
	var validate = false;
	
	var dateFrom = getText(strFromDate);
	var dateTo = getText(strTodate);

	tempIStartDate = dateFrom;
	tempIEndDate = dateTo;
			
	tempDay=tempIStartDate.substring(0,2);
	tempMonth=tempIStartDate.substring(3,5);
	tempYear=tempIStartDate.substring(6,10); 	
	var tempOStartDate=(tempYear+tempMonth+tempDay);		
	
	tempDay=tempIEndDate.substring(0,2);
	tempMonth=tempIEndDate.substring(3,5);
	tempYear=tempIEndDate.substring(6,10); 	
	var tempOEndDate=(tempYear+tempMonth+tempDay);			

	var dtC = new Date();
	var dtCM = dtC.getMonth() + 1;
	var dtCD = dtC.getDate();
	if (dtCM < 10){dtCM = "0" + dtCM}
	if (dtCD < 10){dtCD = "0" + dtCD}
 
	var strSysDate = dtCD + "/" + dtCM + "/" + dtC.getFullYear(); 
	
	tempDay=strSysDate.substring(0,2);
	tempMonth=strSysDate.substring(3,5);
	tempYear=strSysDate.substring(6,10);
	
	strSysODate=(tempYear+tempMonth+tempDay); 
	
	if(tempOStartDate > tempOEndDate){
		showCommonError("Error",fromDtExceed);
		getFieldByID(strTodate).focus();
	} else if (!CheckDates(repStartDate,getText(strFromDate))) {
		showCommonError("Error",rptReriodExceeds +" "+ repStartDate);
		getFieldByID(strFromDate).focus();			
	} else {
		validate = true;				
	}
	return validate;	
}
function ValidateFlagText(textBoxObj,evt) {

    //skip events for space and control keys
   var keyID = evt.charCode;
   if ((keyID==8)||(keyID==13||keyID==46||keyID==0)||(keyID >47 && keyID <58)||(keyID >64 && keyID <91)||(keyID >96 && keyID <123)) {
	   return true;
   }
   else{
	   return false;
   }
}
//-->