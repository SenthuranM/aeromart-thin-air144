	/*
	*********************************************************
		Description		: Tab Generator (Fully client side)
		Author			: Rilwan A. Latiff
		Version			: 2.0
		Last Modified	: 25th Aug 2005
	*********************************************************	
	*/

	function Tab(strTabID){
		// Properties 
		this.height = 100;
		this.width = 200;
		this.top = 20;
		this.left = 100 
		this.align = "left"; //left; right; 
		this.vAlign = "top"; //top; bottom;
		
		this.SelectedTab = 0 ;
		this.SelecteTabClass = "fntSTab";
		this.UnSelecteTabClass = "fntTab";
		this.onClick = "" ;
		this.getTabID = "";					// retrun tab ID
		
		// ---------------------------
		this.ID = strTabID;
		this.TabText = new Array();
		this.TabWidth = new Array();
		this.TabURL = new Array();
		this.TabToolTip = new Array();
		this.TabScroll = new Array();
		
		if (!window.arrTabs) window.arrTabs = new Array();
		window.arrTabs[this.ID] = this;
		window.arrTabs[window.arrTabs.length] = this;
		window._tbsImgPath = "../../images/";
		window._tbsFiles = "show,save,about:blank" ;
		
		// Set the Images
		var arrTab_Img = new Array();
		arrTab_Img[0]  = window._tbsImgPath + "AA004_no_cache.gif"; // Select tab Left corner
		arrTab_Img[1]  = window._tbsImgPath + "AA005_no_cache.jpg"; // Select tab Background
		arrTab_Img[2]  = window._tbsImgPath + "AA006_no_cache.gif"; // Select tab right corner
		arrTab_Img[3]  = window._tbsImgPath + "AA001_no_cache.gif"; // Un-Select tab Left corner
		arrTab_Img[4]  = window._tbsImgPath + "AA002_no_cache.jpg"; // Un-Select tab Background
		arrTab_Img[5]  = window._tbsImgPath + "AA003_no_cache.gif"; // Un-Select tab right corner
		arrTab_Img[6]  = window._tbsImgPath + "AA016_no_cache.gif"; // Tab Line background
		arrTab_Img[7]  = window._tbsImgPath + "AA017_no_cache.gif"; // Tab Line right Corner
		arrTab_Img[8]  = window._tbsImgPath + "AA011_no_cache.jpg"; // Background
		arrTab_Img[9]  = window._tbsImgPath + "AA010_no_cache.jpg"; // Left Line
		arrTab_Img[10] = window._tbsImgPath + "AA012_no_cache.jpg"; // Right Line
		arrTab_Img[11] = window._tbsImgPath + "AA007_no_cache.gif"; // Bottom Left
		arrTab_Img[12] = window._tbsImgPath + "AA008_no_cache.jpg"; // Bottom Center
		arrTab_Img[13] = window._tbsImgPath + "AA009_no_cache.gif"; // Bottom Right
		arrTab_Img[14] = window._tbsImgPath + "AA022_no_cache.gif"; // Bottom Right
		arrTab_Img[15] = window._tbsImgPath + "BA004_no_cache.gif"; // Select tab Left corner
		arrTab_Img[16] = window._tbsImgPath + "BA005_no_cache.jpg"; // Select tab Background
		arrTab_Img[17] = window._tbsImgPath + "BA006_no_cache.gif"; // Select tab right corner
		arrTab_Img[18] = window._tbsImgPath + "BA001_no_cache.gif"; // Un-Select tab Left corner
		arrTab_Img[19] = window._tbsImgPath + "BA002_no_cache.jpg"; // Un-Select tab Background
		arrTab_Img[20] = window._tbsImgPath + "BA003_no_cache.gif"; // Un-Select tab right corner
		arrTab_Img[21] = window._tbsImgPath + "BA016_no_cache.gif"; // Tab Line background
		arrTab_Img[22] = window._tbsImgPath + "BA017_no_cache.gif"; // Tab Line right Corner
		arrTab_Img[23] = window._tbsImgPath + "BA011_no_cache.jpg"; // Background
		arrTab_Img[24] = window._tbsImgPath + "BA010_no_cache.jpg"; // Left Line
		arrTab_Img[25] = window._tbsImgPath + "BA012_no_cache.jpg"; // Right Line
		arrTab_Img[26] = window._tbsImgPath + "BA007_no_cache.gif"; // Bottom Left
		arrTab_Img[27] = window._tbsImgPath + "BA008_no_cache.jpg"; // Bottom Center
		arrTab_Img[28] = window._tbsImgPath + "BA009_no_cache.gif"; // Bottom Right
		arrTab_Img[29] = window._tbsImgPath + "BA022_no_cache.gif"; // Bottom Right
		
		this.arrTab_Img = arrTab_Img ;
		
		// Methods 
		this.addTab = addTab;
		this.BuildTabs = BuildTabs;
		this.getSelectedTabIndex = getSelectedTabIndex;
		
		// Events 
		this.TabClick = TabClick;
		this.TabRefresh = TabRefresh;
	}

	function addTab(strTabText, intTabWidth, strURL, strToolTip, strScroll){
		this.TabText[this.TabText.length] = strTabText ;
		this.TabWidth[this.TabWidth.length] = intTabWidth ;
		this.TabURL[this.TabURL.length] = strURL ;
		this.TabToolTip[this.TabToolTip.length] = strToolTip;
		this.TabScroll[this.TabScroll.length] = strScroll;
	}

	function BuildTabs(){
		// Write the Tab Div
		
		var strSpnID = "spnTabLayer_";
		document.writeln('<span id="' + strSpnID  + '" style="visibility:hidden;">?<\/span><span id="spnTemp" name="spntemp">');
		var objContainer = document.getElementById(strSpnID);
		objContainer.arrTabs = new Array();
		for (var i = 0 ; i < window.arrTabs.length ; i++){
			objContainer.arrTabs[i] = window.arrTabs[i];
		}
		
		var blnTabFirstLoad = true;
		for (var i = 0 ; i < objContainer.arrTabs.length ; i++){
			var objarrTab = objContainer.arrTabs[i];
			var intTabCount = objarrTab.TabText.length;
			objarrTab.blnFirst = true ;
			
			if (blnTabFirstLoad){
				objarrTab.getTabID = "" ; 
				blnTabFirstLoad = false;
			}
			
			var strTabHTMLText = '<span id="spnTab_' + objarrTab.ID + '" style="position:absolute;top:' + objarrTab.top + 'px;left:' + objarrTab.left + 'px;height:' + objarrTab.height + 'px;width:' + objarrTab.width + ';">';
			if (objarrTab.vAlign.toUpperCase() == "TOP"){
				strTabHTMLText += '<table width="100%" style="height:100%" border="0" cellpadding="0" cellspacing="0">'
				strTabHTMLText += '	<tr><td height="20px">'
				strTabHTMLText += '			<table width="100%" border="0" cellpadding="0" cellspacing="0">'
				strTabHTMLText += '				<tr>'
				if (objarrTab.align.toUpperCase() == "RIGHT"){
					strTabHTMLText += '					<td width="15" style="background-image: url(' + objarrTab.arrTab_Img[14] + ')" rowspan="2"><\/td>'
					strTabHTMLText += '	 				<td style="background-image: url(' + objarrTab.arrTab_Img[6] + ');height:20px;">'
					strTabHTMLText += '						<span id="spnTabList_' + objarrTab.ID + '"><\/span>'
					strTabHTMLText += '					<\/td>'
				}else{
					strTabHTMLText += '	 				<td width="*" style="background-image: url(' + objarrTab.arrTab_Img[6] + ');height:20px;">'
					strTabHTMLText += '						<span id="spnTabList_' + objarrTab.ID + '"><\/span>'
					strTabHTMLText += '					<\/td><td width="15" style="background-image: url(' + objarrTab.arrTab_Img[7] + ')" rowspan="2"><\/td>'
				}
				strTabHTMLText += '				<\/tr>'
				strTabHTMLText += '				<tr>'
				strTabHTMLText += '					<td style="background-image: url(' + objarrTab.arrTab_Img[8] + ')" align="' + objarrTab.align +  '">'
				if (objarrTab.align.toUpperCase() == "RIGHT"){
					strTabHTMLText += '					<img src="' + objarrTab.arrTab_Img[10] + '"><\/td>'
				}else{
					strTabHTMLText += '					<img src="' + objarrTab.arrTab_Img[9] + '"><\/td>'
				}
				strTabHTMLText += '				<\/tr>'
				strTabHTMLText += '			<\/table>'
				strTabHTMLText += '	<\/td><\/tr>'
				strTabHTMLText += '	<tr><td height="' + (objarrTab.height - 20) + 'px" valign="top">'
				strTabHTMLText += '<table width="100%" style="height:100%;" border="0" cellpadding="0" cellspacing="0">'
				strTabHTMLText += '	 <tr>'
				strTabHTMLText += '					<td width="15" style="background-image: url(' + objarrTab.arrTab_Img[9] + ')" height="' + (objarrTab.height - 45) + '">&nbsp;<\/td>'
				strTabHTMLText += '					<td style="background-image: url(' + objarrTab.arrTab_Img[8] + ')" height="' + (objarrTab.height - 45) + '" valign="top">'
				
				for (var x = 0 ; x < intTabCount ; x++){
					var strTabScroll = "auto";
					if (objarrTab.TabScroll[x] != null){
						strTabScroll = objarrTab.TabScroll[x];
					}
			
					strTabHTMLText += '				<div id="divTabs_' + objarrTab.ID + '_' + x + '" name="divTabs_' + objarrTab.ID + '_' + x + '" style="position:absolute;height:' + (objarrTab.height - 10) + 'px;width:' + (objarrTab.width - 18) + 'px;top:28px;left:10px;">'
					var strLinkPage = objarrTab.TabURL[x];
					var blnPage = false;
					var arrFileTypes = window._tbsFiles.split(",");
					for (n = 0 ; n < arrFileTypes.length ; n++){
						if (strLinkPage.indexOf(arrFileTypes[n]) != -1){
							blnPage = true;
						}
					}
					if (!blnPage){
						var arrValue = strLinkPage.split(".");
						if (arrValue.length > 1){
							blnPage = true;
						}
					}
					if (blnPage){
						if (objarrTab.getTabID != ""){objarrTab.getTabID += ",";}
						objarrTab.getTabID += "frmTabs_" + objarrTab.ID + "_" + x; 
						
						strTabHTMLText += '					<iframe id="frmTabs_' + objarrTab.ID + '_' + x + '" name="frmTabs_' + objarrTab.ID + '_' + x + '" src="' + strLinkPage + '" frameborder="no" scrolling="' + strTabScroll + '" width="' + (objarrTab.width - 18) + 'px" height="' + (objarrTab.height - 10) + 'px"><\/iframe>'
					}else{
						var objControl = getFieldByID(strLinkPage);
						objControl.style.position = "absolute" ;
						objControl.style.left = (objarrTab.left + 10) + "px";
						objControl.style.top = (objarrTab.top + 28) + "px";
						objControl.style.height = (objarrTab.height - 10) + "px";
						objControl.style.width = (objarrTab.width - 18) + "px";
						objControl.style.overflowY = "scroll";
						objControl.style.overflowX = "hidden";
						setVisible(strLinkPage, false)
					}
					strTabHTMLText += '				<\/div>'
				}
				
				strTabHTMLText += '					<\/td>'
				strTabHTMLText += '					<td width="15"style="background-image: url(' + objarrTab.arrTab_Img[10] + ')" height="' + (objarrTab.height - 45) + '">&nbsp;<\/td>'
				strTabHTMLText += '				<\/tr>'
				strTabHTMLText += '				<tr>'
				strTabHTMLText += '					<td width="15" style="background-image: url(' + objarrTab.arrTab_Img[11] + ');height:25px;">&nbsp;<\/td>'
				strTabHTMLText += '					<td style="background-image: url(' + objarrTab.arrTab_Img[12] + ');height:25px;">&nbsp;<\/td>'
				strTabHTMLText += '					<td width="15" style="background-image: url(' + objarrTab.arrTab_Img[13] + ');height:25px;">&nbsp;<\/td>'
				strTabHTMLText += '				<\/tr>'
				strTabHTMLText += '			<\/table>'
				
				strTabHTMLText += '<\/td><\/tr>'
				strTabHTMLText += '<\/table>'
				strTabHTMLText += '<\/span>'
			}else{
				strTabHTMLText += '<table width="100%" style="height:100%;" border="0" cellpadding="0" cellspacing="0">'
				strTabHTMLText += '	<tr><td height="' + (objarrTab.height - 20) + 'px" vAlign="top">'
				strTabHTMLText += '			<table width="100%" style="height:100%;" border="0" cellpadding="0" cellspacing="0">'
				strTabHTMLText += '				<tr>'
				strTabHTMLText += '					<td width="15" style="background-image: url(' + objarrTab.arrTab_Img[26] + ');height:25px;">&nbsp;<\/td>'
				strTabHTMLText += '					<td style="background-image: url(' + objarrTab.arrTab_Img[27] + ');height:25px;">&nbsp;<\/td>'
				strTabHTMLText += '					<td width="15" style="background-image: url(' + objarrTab.arrTab_Img[28] + ');height:25px;">&nbsp;<\/td>'
				strTabHTMLText += '				<\/tr>'
				strTabHTMLText += '				<tr>'
				strTabHTMLText += '					<td width="15" style="background-image: url(' + objarrTab.arrTab_Img[9] + ')" height="' + (objarrTab.height - 45) + '">&nbsp;<\/td>'
				strTabHTMLText += '					<td style="background-image: url(' + objarrTab.arrTab_Img[8] + ')" height="' + (objarrTab.height - 45) + '" vAlign="top">'
				
				for (var x = 0 ; x < intTabCount ; x++){
					strTabHTMLText += '				<div id="divTabs_' + objarrTab.ID + '_' + x + '" name="divTabs_' + objarrTab.ID + '_' + x + '" style="position:absolute;height:' + (objarrTab.height - 10) + 'px;width:' + (objarrTab.width - 18) + 'px;top:10px;left:10px;">'
					var strLinkPage = objarrTab.TabURL[x];
					var blnPage = false;
					var arrFileTypes = window._tbsFiles.split(",");
					for (n = 0 ; n < arrFileTypes.length ; n++){
						if (strLinkPage.indexOf(arrFileTypes[n]) != -1){
							blnPage = true;
						}
					}
					if (!blnPage){
						var arrValue = strLinkPage.split(".");
						if (arrValue.length > 1){
							blnPage = true;
						}
					}
					if (blnPage){
						if (objarrTab.getTabID != ""){objarrTab.getTabID += ",";}
						objarrTab.getTabID += "frmTabs_" + objarrTab.ID + "_" + x; 
						
						strTabHTMLText += '					<iframe id="frmTabs_' + objarrTab.ID + '_' + x + '" name="frmTabs_' + objarrTab.ID + '_' + x + '" src="' + strLinkPage + '" frameborder="no" scrolling="' + strTabScroll + '" width="' + (objarrTab.width - 18) + 'px" height="' + (objarrTab.height - 10) + 'px"><\/iframe>'
					}else{
						var objControl = getFieldByID(strLinkPage);
						objControl.style.position = "absolute" ;
						objControl.style.left = (objarrTab.left + 10) + "px";
						objControl.style.top = (objarrTab.top + 10) + "px";
						objControl.style.height = (objarrTab.height - 10) + "px";
						objControl.style.width = (objarrTab.width - 18) + "px";
						objControl.style.overflowY = "scroll";
						objControl.style.overflowX = "hidden";
						setVisible(strLinkPage, false)
					}
					strTabHTMLText += '				<\/div>'
				}
				strTabHTMLText += '					<\/td>'
				strTabHTMLText += '					<td width="15" style="background-image: url(' + objarrTab.arrTab_Img[10] + ')" height="' + (objarrTab.height - 45) + '">&nbsp;<\/td>'
				strTabHTMLText += '				<\/tr>'
				strTabHTMLText += '			<\/table>'
				strTabHTMLText += '<\/td><\/tr>'
				strTabHTMLText += '	<tr><td height="20px" vAlign="top">'
				strTabHTMLText += '			<table width="100%" border="0" cellpadding="0" cellspacing="0">'
				strTabHTMLText += '				<tr>'
				if (objarrTab.align.toUpperCase() == "RIGHT"){
					strTabHTMLText += '					<td width="15" style="background-image: url(' + objarrTab.arrTab_Img[29] + ')" rowspan="2"><\/td>'
					strTabHTMLText += '					<td style="background-image: url(' + objarrTab.arrTab_Img[23] + ')" align="' + objarrTab.align +  '" vAlign="baseline">'
					strTabHTMLText += '					<img src="' + objarrTab.arrTab_Img[25] + '"><\/td>'
				}else{
					strTabHTMLText += '					<td style="background-image: url(' + objarrTab.arrTab_Img[23] + ')" align="' + objarrTab.align +  '" vAlign="baseline">'
					strTabHTMLText += '					<img src="' + objarrTab.arrTab_Img[24] + '"><\/td>'
					strTabHTMLText += '					<\/td><td width="15" style="background-image: url(' + objarrTab.arrTab_Img[22] + ')" rowspan="2"><\/td>'
				}
				strTabHTMLText += '				<\/tr>'
				strTabHTMLText += '				<tr>'
				if (objarrTab.align.toUpperCase() == "RIGHT"){
					strTabHTMLText += '	 				<td style="background-image: url(' + objarrTab.arrTab_Img[21] + ');height:20px;">'
					strTabHTMLText += '						<span id="spnTabList_' + objarrTab.ID + '"><\/span><\/td>'
				}else{
					strTabHTMLText += '	 				<td style="background-image: url(' + objarrTab.arrTab_Img[21] + ');height:20px;">'
					strTabHTMLText += '						<span id="spnTabList_' + objarrTab.ID + '"><\/span><\/TD>'
				}
				strTabHTMLText += '				<\/tr>'
				strTabHTMLText += '			<\/table>'
				strTabHTMLText += '	<\/td><\/tr>'
				strTabHTMLText += '<\/table>'
				strTabHTMLText += '<\/span>'
			}
			
			document.writeln(strTabHTMLText);
			
			TabClick(objarrTab.SelectedTab, objarrTab.ID); 
			objarrTab.blnFirst = false ;
		}
	}

	function TabClick(strTabNo, strTabID){
		//alert("tab " + strTabID + " = " + strTabNo);
		
		if (strTabID == null){
			strTabID = this.ID;
		}
		var strSpnID = "spnTabLayer_";
		var objContainer = document.getElementById(strSpnID);
		var strTabHTMLText = "" ;
		for (var i = 0 ; i < objContainer.arrTabs.length ; i++){

			var objarrTab = objContainer.arrTabs[i];
			var intTabCount = objarrTab.TabText.length;
			var strSpanID = "spnTabList_" + objarrTab.ID
			if (objarrTab.ID == strTabID){
				var blnTabReturn = true ;
				if (objarrTab.onClick != "" && objarrTab.blnFirst == false){
					var strOnClickFunName = eval(objarrTab.onClick + "(" + strTabNo + ")")
					blnTabReturn = strOnClickFunName;
				}
				if (blnTabReturn){
					strTabHTMLText += "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='" + objarrTab.align  + "' ID='Table7'>"
					strTabHTMLText += "<tr>"
					if (objarrTab.align.toUpperCase() == "RIGHT"){
						strTabHTMLText += "<td><font>&nbsp;</font></td>"
					}
					for (var x = 0 ; x < intTabCount ; x++){
						var strTabDivID = 'divTabs_' + objarrTab.ID + '_' + x 
						var objControl = getFieldByID(strTabDivID);
						var intTabHeight = "20px;"
				
						if (strTabNo == x){
							if (objarrTab.vAlign.toUpperCase() == "TOP"){
								strTabHTMLText += '<td width="15"  onclick="TabClick(' + "'" + x + "','" + objarrTab.ID + "'" + ')" style="background-image: url(' + objarrTab.arrTab_Img[0] + ');height:' + intTabHeight + ';"><\/td>'
								strTabHTMLText += '<td width="' + objarrTab.TabWidth[x] + 'px"  height="20" onclick="TabClick(' + "'" + x + "','" + objarrTab.ID + "'" + ')" style="background-image: url(' + objarrTab.arrTab_Img[1] + ');height:' + intTabHeight + ';" align="center"><font style="cursor:hand;" title="' + objarrTab.TabToolTip[x] + '" class="' + objarrTab.SelecteTabClass + '">' + objarrTab.TabText[x] + '<\/font><\/td>'
								strTabHTMLText += '<td width="15"  onclick="TabClick(' + "'" + x + "','" + objarrTab.ID + "'" + ')" style="background-image: url(' + objarrTab.arrTab_Img[2] + ');height:' + intTabHeight + ';"><\/td>'
							}else{
								strTabHTMLText += '<td width="15"  onclick="TabClick(' + "'" + x + "','" + objarrTab.ID + "'" + ')" style="background-image: url(' + objarrTab.arrTab_Img[15] + ');height:' + intTabHeight + ';"><\/td>'
								strTabHTMLText += '<td width="' + objarrTab.TabWidth[x] + 'px"  height="20" onclick="TabClick(' + "'" + x + "','" + objarrTab.ID + "'" + ')" style="background-image: url(' + objarrTab.arrTab_Img[16] + ');height:' + intTabHeight + ';" align="center"><font style="cursor:hand;" title="' + objarrTab.TabToolTip[x] + '" class="' + objarrTab.SelecteTabClass + '" valign="top">' + objarrTab.TabText[x] + '<\/font><\/td>'
								strTabHTMLText += '<td width="15"  onclick="TabClick(' + "'" + x + "','" + objarrTab.ID + "'" + ')" style="background-image: url(' + objarrTab.arrTab_Img[17] + ');height:' + intTabHeight + ';"><\/td>'
							}
							
							objControl.style.height = (objarrTab.height - 45) + "px" ;
							objControl.style.width = (objarrTab.width - 30) + "px" ;
							setVisible(strTabDivID, true)
							objarrTab.SelectedTab = x
						}
						else{
							if (objarrTab.vAlign.toUpperCase() == "TOP"){
								strTabHTMLText += '<td width="15"  onclick="TabClick(' + "'" + x + "','" + objarrTab.ID + "'" + ')" style="background-image: url(' + objarrTab.arrTab_Img[3] + ');height:' + intTabHeight + ';"><\/td>'
								strTabHTMLText += '<td width="' + objarrTab.TabWidth[x] + 'px"  height="20" onclick="TabClick(' + "'" + x + "','" + objarrTab.ID + "'" + ')" style="background-image: url(' + objarrTab.arrTab_Img[4] + ');height:' + intTabHeight + ';" align="center"><font style="cursor:hand;" title="' + objarrTab.TabToolTip[x] + '" class="' + objarrTab.UnSelecteTabClass + '">' + objarrTab.TabText[x] + '<\/font><\/td>'
								strTabHTMLText += '<td width="15"  onclick="TabClick(' + "'" + x + "','" + objarrTab.ID + "'" + ')" style="background-image: url(' + objarrTab.arrTab_Img[5] + ');height:' + intTabHeight + ';"><\/td>'
							}else{
								strTabHTMLText += '<td width="15" onclick="TabClick(' + "'" + x + "','" + objarrTab.ID + "'" + ')" style="background-image: url(' + objarrTab.arrTab_Img[18] + ');height:' + intTabHeight + ';"><\/td>'
								strTabHTMLText += '<td width="' + objarrTab.TabWidth[x] + 'px"  height="20" onclick="TabClick(' + "'" + x + "','" + objarrTab.ID + "'" + ')" style="background-image: url(' + objarrTab.arrTab_Img[19] + ');height:' + intTabHeight + ';" align="center"><font style="cursor:hand;" title="' + objarrTab.TabToolTip[x] + '" class="' + objarrTab.UnSelecteTabClass + '">' + objarrTab.TabText[x] + '<\/font><\/td>'
								strTabHTMLText += '<td width="15" onclick="TabClick(' + "'" + x + "','" + objarrTab.ID + "'" + ')" style="background-image: url(' + objarrTab.arrTab_Img[20] + ');height:' + intTabHeight + ';"><\/td>'
							}
							
							objControl.style.height = "0px" ;
							objControl.style.width = "0px" ;
							setVisible(strTabDivID, false)
						}

						var strLinkPage = objarrTab.TabURL[x];
						var blnPage = false;
						var arrFileTypes = window._tbsFiles.split(",");
						for (n = 0 ; n < arrFileTypes.length ; n++){
							if (strLinkPage.indexOf(arrFileTypes[n]) != -1){
								blnPage = true;
							}
						}
						var arrValue = strLinkPage.split(".");
						if (arrValue.length == 1 && blnPage != true){
							setVisible(strLinkPage, false);
							if (strTabNo == x){
								setVisible(strLinkPage, true);
								var objControl = getFieldByID(strLinkPage);
								objControl.style.zIndex = 1;
							}
						}
					}
					
					if (objarrTab.align.toUpperCase() == "LEFT"){
						strTabHTMLText += "<td><font>&nbsp;</font></td>"
					}
					strTabHTMLText += "<\/tr>"
					strTabHTMLText += "<\/table>"
				
					DivWrite(strSpanID, strTabHTMLText)
				}
				break ;
			}
		}
	}


	function TabRefresh(intTabIndex){
		var strSpnID = "spnTabLayer_";
		var objContainer = document.getElementById(strSpnID);
		var strTabHTMLText = "" ;
		for (var i = 0 ; i < objContainer.arrTabs.length ; i++){
			var objarrTab = objContainer.arrTabs[i];
			if (objarrTab.ID == this.ID){
				var strLinkPage = objarrTab.TabURL[intTabIndex];
				var blnPage = false;
				var arrFileTypes = window._tbsFiles.split(",");
				for (n = 0 ; n < arrFileTypes.length ; n++){
					if (strLinkPage.indexOf(arrFileTypes[n]) != -1){
						blnPage = true;
					}
				}
				if (!blnPage){
					var arrValue = strLinkPage.split(".");
					if (arrValue.length > 1){
						blnPage = true;
					}
				}
				if (blnPage){
					var strFrameName = 'frmTabs_' + objarrTab.ID + '_' + intTabIndex
					frames[strFrameName].location.replace(strLinkPage);
				}
				break;
			}
		}
	}

	function getSelectedTabIndex(){
		var intTabIndex = 0 ;
		var strSpnID = "spnTabLayer_";
		var objContainer = document.getElementById(strSpnID);
		var strTabHTMLText = "" ;
		for (var i = 0 ; i < objContainer.arrTabs.length ; i++){
			var objarrTab = objContainer.arrTabs[i];
			if (objarrTab.ID == this.ID){
				intTabIndex = objarrTab.SelectedTab ;
				break;			
			}
		}
		return intTabIndex;
	}
	// ----------------------------------- End of Tab ----------------------------------------
