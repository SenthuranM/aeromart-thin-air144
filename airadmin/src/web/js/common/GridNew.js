	/*
	*********************************************************
		Description		: Data Grid Generator (Fully client side)
		Author			: Rilwan A. Latiff
		Version			: 1.1
		Careated on		: 04th June 2005
		Last Modified	: 06th June 2008
	*********************************************************	
	*/
	
	function DGColumn(strID){
		this.ID = strID;								// Column ID
		this.headerText = "";							// Header column text
		this.headerAlign = "center";					// Header column align
		this.itemAlign = "center";						// item column align
		this.vAlign = "middle";							// Vertical column align  / TOP / BOTTOM / BASELINE / MIDDLE
		this.width = "100px";							// column width
		this.toolTip = "";								// column tooltip
		this.sort = false;								// Column sort available or not 
		this.arrayIndex = -1;							// Data Array column index
		this.columnType = "";							// Column Type Eg: LABEL / CHECKBOX / RADIO / IMAGE / LINK / SYSLINK / IMAGELINK / CUSTOM
		this.checkAll = false;							// Check all available or not if the column type is checkbox
		this.linkTarget = "_parent";					// Link Target
		this.linkURL = "";								// Link URL
		this.linkDataKeyArray = "0";					// link value dataArray Index;
		this.linkOnClick = "";							// link onlcick custom function 
		this.onChange = "";								// onChange call the custom function for a custom control
		this.htmlTag = "";								// HTML Control Tag
		this.columnGroup = false;						// group column or not
		this.controlStatusIndex = "";					// Control Status Element
		this.mergeCols = 0;								// Merge Columns
		this.mergeColText = "";							// Merge Column Text
		this.displayToolTip = false;					// display Tooltip or not / true / false
	}
	
	function DataGrid(strID){
		// ----------------- Grid Properties
		this.ID = strID;								// Data Grid ID
		this.align = "center";							// Grid Align
		this.width = "100%";							// Grid width
		this.height = "160px";							// Grid Height
		this.seqNo = true;								// Sequence Number availabel or not
		this.seqNoWidth = "3%";							// Sequence column width
		this.seqStartNo = 1;							// Sequence starting Number
		this.alternateRowClass = "GridAlternateRow";	// Alternate row classateRow";
		this.setColors = "";							// Custom function name to Set the row text colors;
		this.setRowBGColors = "";						// Custom function name to set Row BackGround Color
		this.setHDBGColors = "";						// Custom function name to set Heading Background Color
		this.blnColBGColors = false;					// column wise background colors Change
		this.headerHeight = "18px";						// Header Text Height;
		this.rowSelect = false;							// Row selection available or not 
		this.rowMultiSelect = false;					// Multiple row selection available or not
		this.rowOver = false;							// Row mouse over effect avalialbe or not
		this.arrowClick = true;							// Arrow click pg down / pg up
		this.controlValue = 0;							// control Value to return the selected row IDS;
		this.headerValign = "middle";					// Header Vertical Align;
		this.headerBold = false;						// Header text bold or not
		this.headerVisible = true;						// Header Visisbility
		this.radioButtonGroup = "";						// Column Radio button Group 
		this.arrGridData = new Array();					// Data Array
		this.loaded = false ;							// Completed status ;			// New
		this.paging = false ;							// Allow paging or not			// New
		this.backGroundColor = "#ECECEC";				// Grid BackGround color		// New
		this.rowClick = "";								// Row Click Custom Function 
		this.rowDblClick = "";							// Row Double Click 
		this.noDataFoundMsg = "No Data found";			// No Data Found Message;
		this.tabIndex = 1;								// tabIndex;
		this.rowClickValidate = "";						// Row Click validate custom function before select the row
		this.rowDblClickValidate = "";					// Row Double Click validate custom function before select the row
		this.shortCutKeyFunction = "";					// Short Cut key Function;
		this.getFrameName = "";							// Return Frame name	
		this.blnMergeCols = false;						// Merge Columns True / false	
		this.gridStyle = 0;								// Grid style 0 = Flat / 1 - Vista / 2 - 
		this.gridScroll = "auto";						// Grid scrolling - Auto / none
		this.onMouseDown = "";							// On Mouse Client funciton call - Returns <Mouse Buttn 0/1/2, Row>
		this.toolTipOnMouseOver = "";					// Column tooltip client function name Returns <Row, Column> Function should return array with set of informatons
		this.scrollWidth = 17							// Firefox sroll gap
		
		// Paging 
		this.pgnumRecPage = 10;							// Paging size 
		this.pgnumRecTotal = 0;							// Total Record count
		this.pgonClick = "";							// Onclick client function
		this.pgalign = "left";							// Alignment
		this.pgstyle = 1 ;
		this.pgintPages = 10;							
		
		this.pgtabIndexPrev = 0;						// Tab Index for Prev Button
		this.pgtabIndexNext = 0;						// Tab Index for Next Button
		this.pgtabIndexNo	= 0;						// Tab Index for Text Box
		this.pgtabIndexGo = 0;							// Tab Index for Go Button

		this.pgtooltipPrevious = "Previous Page";		// Previous arrow tooltip
		this.pgtooltipNext = "Next Page";				// Next arrow tooltip
		this.pgtooltipGo = "Go to Record Number";		// Go tooltip
		
		// ----------------------------------------------------------------------------------------------
		// Modify this area as per your need if its required
		// ----------------------------------------------------------------------------------------------
		this.dataGridPage = "../../js/common/dataGrid.jsp";// Data Grid Page with the path
		this.imagePath = "../../images/";					// Image Path
		this.toolTipImagePath = "../../images/"			// Tooltip Image Path
		
		this.errMsgPreviousPage = "You are already in first record.";						// Previous Page error Message
		this.errMsgNextPage = "You are already in last record.";							// Next Page error messange
		this.errMsgZero = "Record number cannot be less than one.";							// Lowest record error message
		this.errMsgMax = "Record number should be less than the total number of records.";	// highest record error message
		this.errMsgInRecord = "You are already in record number ";							// Go error message
		this.errMsgInvalid = "Please enter a valid number";
		// ----------------------------------------------------------------------------------------------

		this.recNavigateRefresh = recNavigateRefresh ;	// Refresh the Navigate control
		this.recNavigateFocus = gridrecNavigateFocus;	// record navigation focus;		
		this.recNavigateClear = gridrecNavigateClear;	// record navigate Clear;	
		
		// Grid methods -------------------------------
		this.addColumn = addColumn;						// Add Columns
		this.displayGrid = displayGrid;					// Display the Grid
		this.getSelectedRows = getSelectedRows;			// Get slected Rows (Return Row No)
		this.refresh = gridRefresh;						// Refresh the Grid
		this.addRows = addRows;							// add Rows
		this.getHeight = getHeight;						// Get Height of the Row
		this.getSelectedColumn = getSelectedColumn;		// Get Selected Column Data (Return Array)
		this.getSelectedColumnValue = getSelectedColumnValue; // get selected column value;
		this.getCellValue = getCellValue;	
		this.getCellValueDup = getCellValueDup;// Get Cell Value 
		this.setCellValue = setCellValue;				// Set Particular Cell Value
		this.setDisable = setDisable;					// Set Disable (in Parametres  -- Row, Col, true/false 
		this.getRowValue = getRowValue;					// Get the particular Row values
		this.hidePaging = hidePaging;					// Hide / Visiable paging
		this.selectRow = selectRow;						// select rows throught a client function 
		this.visible = gridVisible;
		this.focus	= gridFocus;						// Grid Focus();
		this.setColumnFocus = setColumnFocus;			// focus to particular column
		this.setBGColor = _setBGColor;					// Background Color Function
		this.reSize = _gridReSize;						// Resize the Grid
		this.focusRow = _gridFocusRow;					// Focus to Row
		this.getGridPane = _getGridPane;				// Get Grind Frame Panel
		this.replaceColumn = replaceColumn;
		
		/*
		 *------------------------------------------------------------------------------------------------*
		 * **************************** DONT CHANGE ANYTHING BELOW THIS LINE **************************** *
		 *------------------------------------------------------------------------------------------------*
		*/
		// Grid Variables ----------------------------- 
		window._DGColumClass = null;
		window._DGLayer = "spnGridLayer_";
		if (!window._arrDG) window._arrDG = new Array();
		window._arrDG[this.ID] = this;
		window._arrDG[window._arrDG.length] = this;
		window._recNavigate = null;
		
				// ----------------- Grid Property Array
		this._arrDGID = new Array();
		this._arrDGHeaderText = new Array();
		this._arrDGHeaderAlign = new Array();
		this._arrDGItemAlign = new Array();
		this._arrDGvAlign = new Array();
		this._arrDGWidth = new Array();
		this._arrDGtoolTip = new Array();
		this._arrDGSort = new Array();
		this._arrDGArrayIndex = new Array();
		this._arrDGColumnType = new Array();
		this._arrDGCheckAll = new Array();
		this._arrlinkTarget = new Array();
		this._arrlinkURL = new Array();
		this._arrlinkDataKeyArray = new Array();
		this._arrlinkOnClick = new Array();
		this._arrOnChange = new Array();
		this._arrCustomTag = new Array();
		this._arrControlStatusIndex = new Array();
		this._arrDGColumnGroup = new Array();
		this._arrDGMergeColumns = new Array();
		this._arrDGMergeColumnTxt = new Array();
		this._arrDGBlnTooltip = new Array();
		
		this._DGSortHDIndex = "";
		this._DGSortOrder = "D";
		this._DGCheckAll = new Array();
		this._DGRowSelected = new Array();
		this._DGarrDisStatus = new Array();
		this._DGarrChkStatus = new Array();
		this._blnFirstLoad = true;
		this._DGLastRowClick = -1;
		this._DGSeqCol = "";
		this._DGIfrmLoaded = false;
		this._DGTimer = null;
		this._contentLocId = "";
		this.pgObject = null;							
		
		// ---------------------- Identify the Browser;
		window._DGBrowser = true ;
		ua = navigator.userAgent;
		s = "MSIE";
		if ((i = ua.indexOf(s)) >= 0) {
			window._DGBrowser = true;
		}

		s = "Netscape6/";
		if ((i = ua.indexOf(s)) >= 0) {
			window._DGBrowser = false;
		}
		
		s = "Gecko";
		if ((i = ua.indexOf(s)) >= 0) {
			window._DGBrowser = false;
		}
	}
	
	function addColumn(objColumn){
		this._arrDGID[this._arrDGID.length]= objColumn.ID;
		this._arrDGHeaderText[this._arrDGHeaderText.length] = objColumn.headerText;
		this._arrDGHeaderAlign[this._arrDGHeaderAlign.length] = objColumn.headerAlign;
		this._arrDGItemAlign[this._arrDGItemAlign.length] = objColumn.itemAlign;
		this._arrDGvAlign[this._arrDGvAlign.length] = objColumn.vAlign;
		this._arrDGWidth[this._arrDGWidth.length] = objColumn.width;
		this._arrDGtoolTip[this._arrDGtoolTip.length] = objColumn.toolTip;
		this._arrDGSort[this._arrDGSort.length] = objColumn.sort;
		this._arrDGArrayIndex[this._arrDGArrayIndex.length] = objColumn.arrayIndex;
		this._arrDGColumnType[this._arrDGColumnType.length] = objColumn.columnType;
		this._arrDGCheckAll[this._arrDGCheckAll.length] = objColumn.checkAll;
		this._arrlinkTarget[this._arrlinkTarget.length] = objColumn.linkTarget;
		this._arrlinkURL[this._arrlinkURL.length] = objColumn.linkURL;
		this._arrlinkDataKeyArray[this._arrlinkDataKeyArray.length] = objColumn.linkDataKeyArray;
		this._arrlinkOnClick[this._arrlinkOnClick.length] = objColumn.linkOnClick;
		this._arrOnChange[this._arrOnChange.length] = objColumn.onChange;
		this._arrCustomTag[this._arrCustomTag.length] = objColumn.htmlTag;
		this._arrControlStatusIndex[this._arrControlStatusIndex.length] = objColumn.controlStatusIndex;
		this._arrDGColumnGroup[this._arrDGColumnGroup.length] = objColumn.columnGroup;
		this._arrDGMergeColumns[this._arrDGMergeColumns.length] = objColumn.mergeCols;
		this._arrDGMergeColumnTxt[this._arrDGMergeColumnTxt.length] = objColumn.mergeColText;
		this._arrDGBlnTooltip[this._arrDGBlnTooltip.length] = objColumn.displayToolTip;
	}
	
	function replaceColumn(objColumn, position){
		this._arrDGID[position]= objColumn.ID;
		this._arrDGHeaderText[position] = objColumn.headerText;
		this._arrDGHeaderAlign[position] = objColumn.headerAlign;
		this._arrDGItemAlign[position] = objColumn.itemAlign;
		this._arrDGWidth[position] = objColumn.width;
		this._arrDGtoolTip[position] = objColumn.toolTip;
		this._arrDGSort[position] = objColumn.sort;
		this._arrDGArrayIndex[position] = objColumn.arrayIndex;
		this._arrDGColumnType[position] = objColumn.columnType;
		this._arrDGCheckAll[position] = objColumn.checkAll;
		this._arrlinkTarget[position] = objColumn.linkTarget;
		this._arrlinkURL[position] = objColumn.linkURL;
		this._arrlinkDataKeyArray[position] = objColumn.linkDataKeyArray;
		this._arrlinkOnClick[position] = objColumn.linkOnClick;
		this._arrOnChange[position] = objColumn.onChange;
		this._arrCustomTag[position] = objColumn.htmlTag;
		this._arrControlStatusIndex[position] = objColumn.controlStatusIndex;
	}
	
	
	function getHeight(){
		var objControl = getFieldByID("divDG_Items" + this.ID);
		return objControl.clientHeight;
	}
	
	function addRows(arrRow){
		this.arrGridData[this.arrGridData.length] = arrRow;
	}
	
	
	
	
	
	
	function displayGrid(){
		try{
			document.writeln('<span id="' + window._DGLayer  + '" style="visibility:hidden;">?<\/span>');
			var objContainer = document.getElementById(window._DGLayer);
			objContainer.arrDG = new Array();
			for (var i = 0 ; i < window._arrDG.length ; i++){
				objContainer.arrDG[i] = window._arrDG[i];
				var objDG = objContainer.arrDG[i];
				if (!objDG._DGIfrmLoaded){
					var strDataGridHTML = "";
					objDG.getFrameName = 'frm_' + objDG.ID ;
					objDG._contentLocId = 'tdCont_' + objDG.ID ;
					
					strDataGridHTML = '<table width="' + objDG.width + '" align="' + objDG.align + '" border="0" cellspacing="0" cellpadding="0">';
					if (objDG.headerVisible){
						strDataGridHTML += '<tr><td valign="top" colSpan="2"><span id="spn_HD_' + objDG.ID  +  '"><\/span>';
						strDataGridHTML += '<\/td><\/tr>';
					}
					strDataGridHTML += '<tr><td id="' + objDG._contentLocId + '" name="' + objDG._contentLocId + '" valign="top" style="height:' + objDG.height + ';" colSpan="2">';
					strDataGridHTML += '	<iframe src="' + objDG.dataGridPage + '" id="' + objDG.getFrameName + '" name="' + objDG.getFrameName + '" height="' + objDG.height + '" width="100%" frameborder="0" scrolling="' + objDG.gridScroll + '" class="GridOutBorder"><\/iframe>';
					strDataGridHTML += '<\/td><\/tr>';
					
					if (objDG.paging){
						strDataGridHTML += '<tr><td><font style="font-size:3px;">&nbsp;<\/font><\/td><\/tr>';
						strDataGridHTML += '<tr><td valign="top"><span id="spn_PG_' + objDG.ID  +  '"><\/span>';
						strDataGridHTML += '<\/td>';
						strDataGridHTML += '<td align="right"><font style="font-size:10px;"><span id="spn_SA_' + objDG.ID  +  '"><\/span></font></td>';
						strDataGridHTML += '<\/tr>';
					} 
					 
					
					strDataGridHTML += '<\/table>';
					DivWrite(objDG.ID, strDataGridHTML);
					
					objDG._DGTimer = setInterval("gridWriteAll()", 300);
					
					if (objDG.paging){
						objDG.pgObject = new recNavigate();
						objDG.pgObject.divID = 'spn_PG_' + objDG.ID;
						objDG.pgObject.numStart = objDG.seqStartNo;
						objDG.pgObject.numRecPage = objDG.pgnumRecPage;							
						objDG.pgObject.numRecTotal = objDG.pgnumRecTotal;							
						objDG.pgObject.onClick = objDG.pgonClick;							
						objDG.pgObject.align = objDG.pgalign;						
						objDG.pgObject.style = objDG.pgstyle;
						objDG.pgObject.intPages = objDG.pgintPages;							

						objDG.pgObject.tabIndexPrev = objDG.pgtabIndexPrev;
						objDG.pgObject.tabIndexNext = objDG.pgtabIndexNext;
						objDG.pgObject.tabIndexNo = objDG.pgtabIndexNo;
						objDG.pgObject.tabIndexGo = objDG.pgtabIndexGo;
						
						objDG.pgObject.tooltipPrevious = objDG.pgtooltipPrevious;		
						objDG.pgObject.tooltipNext = objDG.pgtooltipNext;				
						objDG.pgObject.tooltipGo = objDG.pgtooltipGo;		
						objDG.pgObject.errMsgPreviousPage = objDG.errMsgPreviousPage;		
						objDG.pgObject.errMsgNextPage = objDG.errMsgNextPage;			
						objDG.pgObject.errMsgZero = objDG.errMsgZero;							
						objDG.pgObject.errMsgMax = objDG.errMsgMax;	
						objDG.pgObject.errMsgInRecord = objDG.errMsgInRecord;	
						objDG.pgObject.errMsgInvalid = objDG.errMsgInvalid;
						objDG.pgObject.writeNavigate();
					}
				}
			}
			
			if (window._recNavigate != null){
				window._recNavigate.writeNavigate();
			}
			//gridWriteAll()
		}catch(e){}
	}
	
	
	function recNavigateRefresh(){
		this.pgObject.refreshNavigate();
	}
	
	function gridrecNavigateFocus(){
		this.pgObject.focus();
	}
	
	function gridrecNavigateClear(){
		try{
			var objContainer = document.getElementById(window._DGLayer);
			for (var y = 0 ; y < objContainer.arrDG.length ; y++){
				var objDG = objContainer.arrDG[y];
				if (this.ID == objDG.ID){
					var intColumnCount = objDG._arrDGHeaderText.length;
					for (var i = 0 ; i < intColumnCount ; i++){
						if ((objDG._arrDGColumnType[i].toUpperCase() == "CHECKBOX") && (objDG._arrDGCheckAll[i])){
							var objCheckBox =  document.getElementById("CHKAll_" + objDG.ID + "_" + i);
							objCheckBox.checked = false;
						}
					}
					
					for (var x = 0 ; x < objDG._DGCheckAll.length ; x++){
						objDG._DGCheckAll[x][1] = false;
					}
				}
			}
			if (this.paging){
				this.pgObject.recNavigateClear();
			}
		}catch (e){}
	}
	// -----------------
	
	function gridWriteAll(){
		var objContainer = document.getElementById(window._DGLayer);
		for (var y = 0 ; y < objContainer.arrDG.length ; y++){
			var objDG = objContainer.arrDG[y];
			var strFrmName = "frm_" + objDG.ID;
			if (!objDG._DGIfrmLoaded){
				if (frames[strFrmName].blnGridLoad){
					clearTimeout(objDG._DGTimer);
					objDG._DGIfrmLoaded = true;
					gridWrite(objDG.ID);
				}
			}
		}
	}
	
	function hidePaging(blnVisible){
		var objControl = document.getElementById("spn_PG_" + this.ID);
		if (blnVisible){
			objControl.style.visibility = "visible";
		}else{
			objControl.style.visibility = "hidden";
		}
	}
	
	
	function gridHDWrite(strID, intHeight, intWidth){
		var objContainer = document.getElementById(window._DGLayer);
		var strGridHTMLText = "" ;
		for (var i = 0 ; i < objContainer.arrDG.length ; i++){
			var objDG = objContainer.arrDG[i];
			var intColumnCount = objDG._arrDGHeaderText.length;
			if (objDG.ID == strID){
				var strDataGridHTML = "";
				if (objDG.headerVisible){
					var intHDWidth = intWidth;
					var strHDBGClass = "GridHeader";
					strHDBGClass = "GridHeader";
					if (objDG.gridStyle != 0){strHDBGClass = "GridHeader" + objDG.gridStyle;}
					strDataGridHTML += '		<table width="' + intHDWidth + '" cellpadding="0" cellspacing="0" border="0" align="left">';
					
					// Merge Header Writing
					if (objDG.blnMergeCols){
						strDataGridHTML += '<tr>'
						if (objDG.seqNo){
							strDataGridHTML += '<td';
							strDataGridHTML += ' rowspan="2"';
							strDataGridHTML += ' class="GridRow ' + strHDBGClass + ' GridHeaderBand GridHDBorder"';
							strDataGridHTML += ' style="' + objDG.headerHeight  + ';cursor:default;"' ;
							strDataGridHTML += ' width="' + objDG.seqNoWidth + '" align="center" valign="middle">';
							strDataGridHTML += '<font style="visibility:hidden;">' + (objDG.arrGridData.length + objDG.seqStartNo)  + '<\/font>';
							strDataGridHTML += '<\/td>';
						}
						var strColSpan = "";
						var strColID = "";
						var strRowSpan = "";
						for (var i = 0 ; i < intColumnCount ; i++){
							strColSpan = "";
							strRowSpan = "";
							strColID = "";
							if (objDG._arrDGMergeColumns[i] > 1){
								strColSpan = 'colspan = "' + objDG._arrDGMergeColumns[i] + '"' ;
								
								strHDBGClass = "GridHeader";
								if (objDG.gridStyle != 0){strHDBGClass = "GridHeader" + objDG.gridStyle;}
								
								if (objDG.setHDBGColors != ""){
									strHDBGClass = eval(objDG.setHDBGColors + "('" + i + "',true)");
									if (strHDBGClass == ""){
										if (objDG.gridStyle != 0){strHDBGClass = "GridHeader" + objDG.gridStyle;}else{strHDBGClass = "GridHeader";}
									}
								}
								strDataGridHTML += '<td '+ strColID ;
								strDataGridHTML += ' class="GridRow ' + strHDBGClass + ' GridHeaderBand GridHDBorder';
								if (objDG.headerBold){
									strDataGridHTML += ' GridBold';
								}
								strDataGridHTML += '"';
								strDataGridHTML += ' align="center"';
								strDataGridHTML += ' valign="' + objDG.headerValign + '"';
								strDataGridHTML += strColSpan;
								strDataGridHTML += '>';
								strDataGridHTML += ' <font>' 
								if (objDG._arrDGMergeColumnTxt[i] != ""){
									strDataGridHTML += objDG._arrDGMergeColumnTxt[i];
								}else{
									strDataGridHTML += "&nbsp;"; 
								}
								strDataGridHTML += '<\/font>';
								strDataGridHTML += '<\/td>';
							}else{
								strColID = 'id="TD_GridHD_' + objDG.ID + '_' + i +  '"';
								strRowSpan = ' rowspan = "2"';
								if (objDG._arrDGHeaderText[i] == ""){
									strDataGridHTML += '<td';
									strDataGridHTML += getColunmInformation(objDG, i, strRowSpan);
								}else{
									strDataGridHTML += '<td id="TD_GridHD_' + objDG.ID + '_' + i +  '"';
									strDataGridHTML += getColunmInformation(objDG, i, strRowSpan);
								}
							}
							
							if (objDG._arrDGMergeColumns[i] != 0){
								i = (i + objDG._arrDGMergeColumns[i]) - 1;
							}
						}
						strDataGridHTML += '<\/tr>'
					}
					
					// Header
					strDataGridHTML += '<tr>';
					if (!objDG.blnMergeCols){
						if (objDG.seqNo){
							strHDBGClass = "GridHeader";
							if (objDG.gridStyle != 0){strHDBGClass = "GridHeader" + objDG.gridStyle;}
							strDataGridHTML += '<td';
							strDataGridHTML += ' class="GridRow ' + strHDBGClass + ' GridHeaderBand GridHDBorder "';
							strDataGridHTML += ' style="' + objDG.headerHeight  + ';cursor:default;"' ;
							strDataGridHTML += ' width="' + objDG.seqNoWidth + '" align="center" valign="middle">';
							strDataGridHTML += '<font style="visibility:hidden;">' + (objDG.arrGridData.length + objDG.seqStartNo)  + '<\/font>';
							strDataGridHTML += '<\/td>';
						}
					}
					
					var blnProceed = false;
					var intLastMerge = -1;
					strColID = "";
					for (var i = 0 ; i < intColumnCount ; i++){
						strRowSpan = "";
						strColID = 'id="TD_GridHD_' + objDG.ID + '_' + i +  '"';
						blnProceed = true;
						
						if (objDG.blnMergeCols){
							blnProceed = false;
							if (objDG._arrDGMergeColumns[i] > 1){
								intLastMerge = (i + objDG._arrDGMergeColumns[i]) - 1;
							}
							
							if (intLastMerge != -1){
								if (i <= intLastMerge){
									blnProceed = true;
								}else{
									intLastMerge = -1;
								}
							}
						}
						
						
						if (blnProceed){
							if (objDG._arrDGHeaderText[i] == ""){
								strDataGridHTML += '<td';
								strDataGridHTML += getColunmInformation(objDG, i, strRowSpan);
							}else{
								strDataGridHTML += '<td ' + strColID;
								strDataGridHTML += getColunmInformation(objDG, i, strRowSpan);
							}
							strDataGridHTML += '<\/td>';
						}
					}
					strDataGridHTML += '<\/tr><\/table>';
					DivWrite("spn_HD_" + strID, strDataGridHTML);
					
					// Initialize the hearder sort images
					var strimgID = "";
					var strimgURL = "";
					for (var x = 0 ; x < objDG._arrDGHeaderText.length ; x++ ){
						if (objDG._arrDGHeaderText[x] != "" && objDG._arrDGColumnType[x].toUpperCase() != "IMAGE"){
							if (objDG._arrDGSort[x]){
								var strimgID = 'img_GridHD_' + objDG.ID + '_' + x;
								strimgURL = objDG.imagePath + "GA_no_cache.gif"; 
								setImage(strimgID, strimgURL);
								setVisible(strimgID, false);
								if (objDG._DGSortHDIndex != ""){
									if (x == objDG._DGSortHDIndex && (!objDG._DGSortHDIndex)){
										if (objDG._DGSortOrder == "D"){
											strimgURL = objDG.imagePath + "GD_no_cache.gif"; 
										}else{
											strimgURL = objDG.imagePath + "GA_no_cache.gif"; 
										}
										setImage(strimgID, strimgURL);
										setVisible(strimgID, true);
									}
								}
							}
						}
					}
					objDG.loaded = true;
					objDG._blnFirstLoad = false;
					
					if (objDG.paging){
						objDG.pgObject.numStart = objDG.seqStartNo;
						objDG.pgObject.refreshNavigate();
					}
				}
				break
			}
		}
		
		function getColunmInformation(objDG, i, strRowSpan){
			var strDataGridHTML = "";
			var strHDBGClass = "GridHeader";
			if (objDG._arrDGHeaderText[i] == ""){
				if (objDG.gridStyle != 0){strHDBGClass = "GridHeader" + objDG.gridStyle;}
				if (objDG.setHDBGColors != ""){
					strHDBGClass = eval(objDG.setHDBGColors + "('" + i + "',false)");
					if (strHDBGClass == ""){
						if (objDG.gridStyle != 0){strHDBGClass = "GridHeader" + objDG.gridStyle;}else{strHDBGClass = "GridHeader";}
					}
				}
				strDataGridHTML += ' class="GridRow ' + strHDBGClass + ' GridHeaderBand GridHDBorder "';
				strDataGridHTML += ' style="height:' + objDG.headerHeight  + ';"' ;
				strDataGridHTML += strRowSpan;
				strDataGridHTML += ' width="' + objDG._arrDGWidth[i] + '" align="' + objDG._arrDGHeaderAlign[i] + '">';
				if (objDG._arrDGColumnType[i].toUpperCase() == "CHECKBOX" && objDG._arrDGCheckAll[i]){
					strDataGridHTML += '<input type="checkbox" id="CHKAll_' + objDG.ID + '_' + i + '" onclick="gridChkAll(' + "'" + objDG.ID + "','" + i  + "'"  + ')" class="GridNoBorder" title="Select or de-select all"'; 
						
					if (!objDG._blnFirstLoad){
						for (var xx = 0 ; xx < objDG._DGCheckAll.length ; xx++){
							if (objDG._DGCheckAll[xx][0] == i){
								if (objDG._DGCheckAll[xx][1] != ""){
									strDataGridHTML += ' checked = "' + objDG._DGCheckAll[xx][1] +  '" ';
								}
							}
							break;
						}
					}
					strDataGridHTML += '>';
				}else{
					strDataGridHTML += '<font>&nbsp;';
					strDataGridHTML += '<\/font>';
				}
			}else{
				if (objDG.gridStyle != 0){strHDBGClass = "GridHeader" + objDG.gridStyle;}
				if (objDG.setHDBGColors != ""){
					strHDBGClass = eval(objDG.setHDBGColors + "('" + i + "',false)");
					if (strHDBGClass == ""){
						if (objDG.gridStyle != 0){strHDBGClass = "GridHeader" + objDG.gridStyle;}else{strHDBGClass = "GridHeader";}
					}
				}
				strDataGridHTML += ' class="GridRow ' + strHDBGClass + ' GridHeaderBand GridHDBorder ';
				if (objDG.headerBold){
					strDataGridHTML += ' GridBold';
				}
				strDataGridHTML += '"';
				if (!objDG._arrDGSort[i]){
					strDataGridHTML += ' style="' + objDG.headerHeight + ';cursor:default;"' ;
				}else{
					strDataGridHTML += ' style="' + objDG.headerHeight + ';"' ;
				}
				strDataGridHTML += ' width="' + objDG._arrDGWidth[i] + '" align="center"';
				
				if (objDG._arrDGSort[i]){
					strDataGridHTML += ' onmouseover="gridHDMouseOver(' + "'" + 'TD_GridHD_' + objDG.ID + '_' + i + "'" + ')"';
					strDataGridHTML += ' onmouseout="gridHDMouseOut(' + "'" + 'TD_GridHD_' + objDG.ID + '_' + i + "'" + ')"';
					strDataGridHTML += ' onclick="gridSort(' + "'" + objDG.ID + "','" + i + "'" + ')"';
				}
				
				strDataGridHTML += ' valign="' + objDG.headerValign + '"'
				strDataGridHTML += strRowSpan;
				strDataGridHTML += '>';
				strDataGridHTML += '<font';
				if (objDG._arrDGtoolTip[i] != ""){
					strDataGridHTML += ' title= "' + objDG._arrDGtoolTip[i] + '"';
				}
				strDataGridHTML += '>' ;
				strDataGridHTML += objDG._arrDGHeaderText[i] ;
				if (objDG._arrDGSort[i]){
					strDataGridHTML += ' <img id="img_GridHD_' + objDG.ID + '_' + i +  '" src="' + objDG.imagePath + 'GD_no_cache.gif" border="0">';
				}
				strDataGridHTML += '<\/font>';
			}
			
			return strDataGridHTML;
		}
	}
	
	function gridWrite(strID){
		try{
			var objContainer = document.getElementById(window._DGLayer);
			var strGridHTMLText = "" ;
			
			for (var i = 0 ; i < objContainer.arrDG.length ; i++){
				var objDG = objContainer.arrDG[i];
				var intColumnCount = objDG._arrDGHeaderText.length;
				if (objDG.ID == strID){
					
					var strDataGridHTML = "";
					strDataGridHTML += '<table width="100%" cellpadding="1" cellspacing="0" border="0" align="center">';
					
					// Items ---------------
					var intDataCount = objDG.arrGridData.length;
					var intAlternate = 0 ;
					var strRowID = 0 ;
					var strColID = 0 ;
					var intSeqStart = objDG.seqStartNo - 1;
					var intRowCount = 0 ;
					var blnCustomColumn = false;
					var radioButtonGroupName = "";
					var radioButtonGroupID = "";
					var strDisabled = "";
					var strChecked = "";
					var strRowStyle = "";
					var strRow
					
					for (var x = 0 ; x < intDataCount; x++){
						strRowStyle = "";
						if (objDG._blnFirstLoad){
							objDG._DGSeqCol = objDG.arrGridData[intRowCount].length;
							objDG.arrGridData[intRowCount][objDG._DGSeqCol] = intRowCount;
							objDG._DGarrDisStatus[intRowCount] = new Array();
							objDG._DGarrChkStatus[intRowCount] = new Array();
							for (var i = 0 ; i < intColumnCount ; i++){
								objDG._DGarrDisStatus[intRowCount][i] = "";
								objDG._DGarrChkStatus[intRowCount][i] = "";
							}
						}else{
							if (objDG._DGarrDisStatus.length <= x){
								objDG.arrGridData[intRowCount][objDG._DGSeqCol] = intRowCount;
								
								objDG._DGarrDisStatus[intRowCount] = new Array();
								objDG._DGarrChkStatus[intRowCount] = new Array();
								for (var i = 0 ; i < intColumnCount ; i++){
									objDG._DGarrDisStatus[intRowCount][i] = "";
									objDG._DGarrChkStatus[intRowCount][i] = "";
								}
							}
						}
						
						strRowID = "DG_" + objDG.ID + "_" + intRowCount;
						strColID = strRowID + "_0";
						
						var strFontColor = "";
						if (objDG.setColors != ""){
							var funGetColors = eval(objDG.setColors);
							var strRowColors = funGetColors(objDG.arrGridData[intRowCount]);
							if (strRowColors!= ""){
								strFontColor = 'style="color:' + strRowColors + ';"';
							}
						}
						
						// Alternate row color
						var strAlterClass = "" ;
						if (objDG.alternateRowClass != ""){
							if (intAlternate == 1){
								strAlterClass = objDG.alternateRowClass;
							}
						}
						
						strDataGridHTML += '<tr id="TR_' + strRowID + '"';
						var strRowClick = "" ;
						var strMouseDown = "";
						var strArrowClick = "" ;
						var strSeqStyle = 'style="cursor:default;"';
						var strRowClickFunction  = ",'";
						var strCursorPointer = "";
						if (objDG.rowSelect){
							if (objDG.rowClick != ""){
								strRowClickFunction += objDG.rowClick;
								strCursorPointer = " cursorPointer ";
							}
							strRowClickFunction += "'" ;
							
							strRowClick = ' onclick="parent.gridRowClick(' + "'" + strRowID + "','" + objDG.ID + "','" + intRowCount+ "'," + intColumnCount + strRowClickFunction +  ')"';

							if (objDG.arrowClick){
								//strArrowClick = ' onkeydown="gridKeyDown(event,' + "'"  + strRowID + "','" + objDG.ID + "','" + intRowCount+ "'," + intColumnCount + ')"';
							}
							strSeqStyle = '';
						}
						
						if (objDG.rowDblClick != ""){
							var strRowDblClickFunction  =  ",'";
							strRowDblClickFunction += objDG.rowDblClick;
							strRowDblClickFunction += "'" ;
							strRowClick += ' ondblclick="parent.gridRowDblClick(' + "'" + strRowID + "','" + objDG.ID + "','" + intRowCount+ "'," + intColumnCount + strRowDblClickFunction +  ')"';
						}
						
						if (objDG.onMouseDown != ""){
							strMouseDown = ' onmousedown="gridRowMouseDown(event,' + "'" + intRowCount + "','" + objDG.ID + "'" + ')"';
						}
						
						strDataGridHTML += '>';
						if (objDG.seqNo){
							intSeqStart++;
							strDataGridHTML += '<td id="TD_' + strColID + '"' ;
							strDataGridHTML += ' class="GridRow GridHeader GridHDBorder"';
							strDataGridHTML += strRowClick;
							strDataGridHTML += strArrowClick;
							strDataGridHTML += strSeqStyle;
							strDataGridHTML += ' width="' + objDG.seqNoWidth + '" align="center" valign="middle">';
							strDataGridHTML += '<a href="javascript:void(0)" id="idSeq_' + intRowCount + '" name="idSeq_' + intRowCount + '" style="cursor:default"><font';
							if (strSeqStyle == ""){
								strDataGridHTML += ' title="Select row"';
							}
							strDataGridHTML += '>' + intSeqStart + '<\/font><\/a>';
							strDataGridHTML += '<\/td>';
						}
						
						if (objDG.setRowBGColors != ""){
							if (!objDG.blnColBGColors){
								var funGetBGColors = eval(objDG.setRowBGColors );
								var strRowBGColors = funGetBGColors(objDG.arrGridData[intRowCount]);
								if (strRowBGColors != ""){
									strAlterClass = strRowBGColors ;
								}
							}
						}
						
						objDG._DGRowSelected[intRowCount] = new Array();
						objDG._DGRowSelected[intRowCount][0] = intRowCount;
						objDG._DGRowSelected[intRowCount][1] = "" ;
						objDG._DGRowSelected[intRowCount][2] = 'GridItemRow GridItem '+ strAlterClass + " " + strCursorPointer;
						objDG._DGRowSelected[intRowCount][3] = objDG.arrGridData[intRowCount][objDG.controlValue];
						
						var intSeqNo = intRowCount;
						var strColBGColor = "";
						var strTempAlterCClass = strAlterClass
						strRowStyle = "";
						for (var i = 0 ; i < intColumnCount ; i++){
							strAlterClass = strTempAlterCClass;
							
							strDisabled = "";
							strChecked = "" ;
							if (!objDG._blnFirstLoad){
								var intDisabledRowNo = 0;
								intDisabledRowNo = objDG.arrGridData[intRowCount][objDG._DGSeqCol];
								
								if (!isNaN(intDisabledRowNo)){
									if (objDG._DGarrDisStatus[intDisabledRowNo][i] != ""){
										strDisabled = ' disabled = "' + objDG._DGarrDisStatus[intDisabledRowNo][i] + '"';
									}
									if (objDG._DGarrChkStatus[intDisabledRowNo][i] != ""){
										if (objDG._DGarrDisStatus[intDisabledRowNo][i] != true){
											strChecked = ' checked = "' + objDG._DGarrChkStatus[intDisabledRowNo][i] + '"';
										}
									}
									intSeqNo = intDisabledRowNo;
								}
							}
							
							if (objDG._arrControlStatusIndex[i] != ""){
								if (objDG.arrGridData[intRowCount][objDG._arrControlStatusIndex[i]] == true){
									strChecked = ' checked = "true" ';
								}
							}
					
							strColID = strRowID + "_" + (i+1) ;
							
							if (objDG.setRowBGColors != ""){
								if (objDG.blnColBGColors){
									var funGetBGColors = eval(objDG.setRowBGColors );
									var strRowBGColors = funGetBGColors(objDG.arrGridData[intRowCount], i);
									if (strRowBGColors != ""){
										strAlterClass = strRowBGColors ;
									}
								}
							}
							
							strColBGColor = 'GridItemRow GridItem '+ strAlterClass + strCursorPointer
							strRowStyle  += strColBGColor + "^";
							strDataGridHTML += '<td id="TD_' + strColID + '"';
							strDataGridHTML += ' class="'+ strColBGColor + '"';
								
							strDataGridHTML += strArrowClick;
							
							strDataGridHTML += ' onmouseover="_gridMouseOver(event,' + "'" + strRowID + "','" + objDG.ID + "','" + intRowCount + "'," + intColumnCount + "," + i + ')"';
							strDataGridHTML += ' onmouseout="_gridMouseOut(event,' + "'" + strRowID + "','" + objDG.ID + "','" + intRowCount + "'," + intColumnCount + "," + i + ')"';
							
							if (objDG._arrDGColumnType[i].toUpperCase() == "LABEL" || objDG._arrDGColumnType[i].toUpperCase() == "IMAGE"){
								strDataGridHTML += strRowClick;
								strDataGridHTML += strMouseDown;
							}
							strDataGridHTML += ' width="' + objDG._arrDGWidth[i] + '"';
							strDataGridHTML += ' align="' + objDG._arrDGItemAlign[i] + '"';
							strDataGridHTML += ' valign="' + objDG._arrDGvAlign[i] + '">'
							
							var strtoolTip = "";
							if (objDG._arrDGtoolTip[i] != "" || objDG._arrDGtoolTip[i] != null){
								strtoolTip += ' title="' + objDG._arrDGtoolTip[i] + '"';  
							}		
							
							var strTabIndex = '';
							if (objDG._arrDGColumnType[i].toUpperCase() != "LABLE"){
								if (objDG.tabIndex > 0){strTabIndex = ' tabIndex="' + (objDG.tabIndex++) + '" ' ;}
							}					
							
							var strLink = ''
							strLink += '<a href=';
								
							if (objDG._arrlinkURL[i] != ""){
								strLink += '"' + objDG._arrlinkURL[i].replace(":ROW:", intRowCount);
								if (objDG._arrlinkDataKeyArray[i] != ""){
									strLink += '?ID="' + objDG.arrGridData[intRowCount][objDG._arrlinkDataKeyArray[i]];
								}
								strLink += '"' ;
								if (objDG.arrGridData[intRowCount][objDG._arrlinkTarget[i]] != ""){
									strLink += ' target="' + objDG._arrlinkTarget[i] + '"';
								}
							}else{
								if (objDG._arrlinkOnClick[i] != ""){
									strLink += '"javascript:void(0)" onclick="parent.' + objDG._arrlinkOnClick[i] + '(' + "'" + objDG.arrGridData[intRowCount][objDG._arrlinkDataKeyArray[i]] + "'" + ')"' ;
								}
							}
							strLink += strTabIndex;
							strLink += '>';
							
							
							switch (objDG._arrDGColumnType[i].toUpperCase()){
								case "LABEL" :
									if (objDG._arrDGColumnGroup[i]){
										if (typeof(objDG.arrGridData[intRowCount][objDG._arrDGArrayIndex[i]]) == "object"){
											strDataGridHTML += '<table width="100%" cellpadding="1" cellspacing="0" border="0" align="center">';
											var intGroupLength = objDG.arrGridData[intRowCount][objDG._arrDGArrayIndex[i]].length;
											for (var g = 0 ; g < intGroupLength ; g++){
												strDataGridHTML += '<tr>';
												strDataGridHTML += '	<td ';
												if (g < intGroupLength -1) {
													strDataGridHTML += ' class="GridGroupRowBottom ';
													if (g > 0){
														strDataGridHTML += ' GridGroupRowTop "';
													}else{
														strDataGridHTML += ' "';
													}
												}else{
													strDataGridHTML += ' class="GridGroupRowTop "';
												}
												strDataGridHTML += '	align="' + objDG._arrDGItemAlign[i] + '"';
												strDataGridHTML += '	valign="middle">';
												strDataGridHTML += '<span id="lblSpn_' + objDG.ID + '_' + i + '_' + g + '_' + intRowCount + '">';
												strDataGridHTML += '<font';
												strDataGridHTML += strtoolTip + strFontColor ;
												strDataGridHTML += '>' + objDG.arrGridData[intRowCount][objDG._arrDGArrayIndex[i]][g] ;
												if (objDG.arrGridData[intRowCount][objDG._arrDGArrayIndex[i]][g] == "" || objDG.arrGridData[intRowCount][objDG._arrDGArrayIndex[i]][g] == null){
													strDataGridHTML += '&nbsp;';
												}
												strDataGridHTML += '<\/font>';
												strDataGridHTML += '<\/span>';
												strDataGridHTML += '	</td>';
												strDataGridHTML += '</tr>';
											}
											strDataGridHTML += '</table>';
										}
									}else{
										strDataGridHTML += '<span id="lblSpn_' + objDG.ID + '_' + i + '_' + intRowCount + '">';
										strDataGridHTML += '<font';
										strDataGridHTML += strtoolTip + strFontColor ;
										strDataGridHTML += '>' + objDG.arrGridData[intRowCount][objDG._arrDGArrayIndex[i]] ;
										if (objDG.arrGridData[intRowCount][objDG._arrDGArrayIndex[i]] == "" || objDG.arrGridData[intRowCount][objDG._arrDGArrayIndex[i]] == null){
											strDataGridHTML += '&nbsp;';
										}
										strDataGridHTML += '<\/font>';
										strDataGridHTML += '<\/span>';
									}
									break;
								case "CHECKBOX":
									strDataGridHTML += '<input class="GridNoBorder" type="checkbox" id="CHK_' + objDG.ID + '_' + intRowCount + '_' + i + '" name="CHK_' + objDG.ID + '_0_' + i + '" value="' + objDG.arrGridData[intRowCount][objDG._arrDGArrayIndex[i]] + '" onclick="parent.gridChkBoxClick(this,' + "'" + intSeqNo + "','" + i + "','" + objDG.ID + "','" + objDG._arrlinkOnClick[i]  + "'" + ')" ';
									strDataGridHTML += strtoolTip;
									strDataGridHTML += strDisabled;
									strDataGridHTML += strChecked;
									strDataGridHTML += strTabIndex;
									strDataGridHTML += '>';
									break;
								case "RADIO":
									radioButtonGroupName = 'RAD_' + objDG.ID + '_0_' +  i;
									radioButtonGroupID = 'RAD_' + objDG.ID + '_' + intRowCount + '_' +  i;
									if (objDG.radioButtonGroup != ""){
										var arrRadGroup = objDG.radioButtonGroup.split("|");
										for (var n = 0 ; n < arrRadGroup.length ; n++){
											var arrRadCol = arrRadGroup[n].split(",");
											for (var m = 0 ; m < arrRadCol.length ; m++){
												if (i == arrRadCol[m]){
													radioButtonGroupName = 'RAD_' + objDG.ID + '_' + intRowCount + '_' + n + '_N';
													radioButtonGroupID = 'RAD_' + objDG.ID + '_' + intRowCount+ '_' + n + '_' + m  ;
												}
											}
										}
									}
									
									strDataGridHTML += '<input class="GridNoBorder" type="radio" id="' + radioButtonGroupID + '" name="' + radioButtonGroupName + '" value="' + objDG.arrGridData[intRowCount][objDG._arrDGArrayIndex[i]]+ '"  onclick="parent.gridRadioClick(this,' + "'" + intSeqNo + "','" + i + "','" + objDG.ID + "'" + ')"';
									strDataGridHTML += strtoolTip;
									strDataGridHTML += strDisabled;
									strDataGridHTML += strChecked;
									strDataGridHTML += strTabIndex;
									strDataGridHTML += '>';
									break;
								case "IMAGE":
									strDataGridHTML += '<img src="' + objDG.arrGridData[intRowCount][objDG._arrDGArrayIndex[i]] + '" border="0"';
									strDataGridHTML += strtoolTip;
									strDataGridHTML += strTabIndex;
									strDataGridHTML += '>';
									break;									
								case "LINK":
									strDataGridHTML += strLink ;
									strDataGridHTML += '<u><font';
									strDataGridHTML += strtoolTip + strFontColor;
									strDataGridHTML += '>' + objDG.arrGridData[intRowCount][objDG._arrDGArrayIndex[i]] + '<\/font><\/u>';
									strDataGridHTML += '<\/a>';
									break;																	
								case "SYSLINK":
									strDataGridHTML += strLink ;
									strDataGridHTML += '<u><font';
									strDataGridHTML += strtoolTip + strFontColor;
									strDataGridHTML += '>' + objDG._arrCustomTag[i] + '<\/font><\/u>';
									strDataGridHTML += '<\/a>';
									break;													
								case "IMAGELINK":
									strDataGridHTML += strLink + '<img src="' + objDG.arrGridData[intRowCount][objDG._arrDGArrayIndex[i]] + '" border="0"';
									strDataGridHTML += strtoolTip;
									strDataGridHTML += '><\/a>';
									break;		
								case "CUSTOM":															
									strDataGridHTML += '<font>' ;
									var strCustomControl = objDG._arrCustomTag[i];
									strCustomControl = strCustomControl.replace(":CUSTOM:", ' onChange="parent.gridCellOnChange(' + intRowCount + ',' + i + ',this,' + "'" +  objDG.ID + "'" + ')"' + strDisabled  + strTabIndex);
									strCustomControl = strCustomControl.replace(":ROW:", intRowCount);
									strDataGridHTML += strCustomControl ;
									strDataGridHTML += '<\/font>' ;
									blnCustomColumn = true;
									break;
								case "CUSTOM-SELECT":															                                   
								strDataGridHTML += '<font>' ;
								var strCustomControl = objDG._arrCustomTag[i];
								strCustomControl = strCustomControl.replace(":CUSTOM:", ' onChange="parent.gridCellOnChange(' + intRowCount + ',' + i + ',this,' + "'" +  objDG.ID + "'" + ')"' + strDisabled  + strTabIndex);
								strCustomControl = strCustomControl.replace(":ROW:", intRowCount);
                                                                    strCustomControl = strCustomControl.replace(":DATA:", objDG.arrGridData[intRowCount][objDG._arrDGArrayIndex[i]]);
								strDataGridHTML += strCustomControl ;
								strDataGridHTML += '<\/font>' ;
								blnCustomColumn = true;
								break;
							}
							strDataGridHTML += '<\/td>';
						}
						
						if (objDG.setRowBGColors != ""){
							if (objDG.blnColBGColors){
								objDG._DGRowSelected[intRowCount][2] = strRowStyle ;
							}
						}
						
						intAlternate++;
						if (intAlternate > 1){intAlternate = 0;}
						strDataGridHTML += '<\/tr>';
						intRowCount++;
					}
	
					if (intDataCount == 0){
						strDataGridHTML += '<tr><td align="center"><font><b>' + objDG.noDataFoundMsg	+ '<\/font><\/td><\/tr>';
					}
	
					
					strDataGridHTML += '<\/table>';
					
					var strFrmName = "frm_" + objDG.ID;
					frames[strFrmName].writeGrid(strDataGridHTML, objDG.ID, objDG.backGroundColor);
					
					if (blnCustomColumn){
						for (var x = 0 ; x < intDataCount; x++){
							for (var i = 0 ; i < intColumnCount ; i++){
								if (objDG._arrDGColumnType[i].toUpperCase() == "CUSTOM"){
									if (objDG._arrDGArrayIndex[i] !=  null){
										objDG.setCellValue(x, i, objDG.arrGridData[x][objDG._arrDGArrayIndex[i]], "");
									}
								}
							}
						}
					}
					
					
					break;
				}
			}
		}catch(e){}
	}
	
	function getSelectedRows(){
		var objContainer = document.getElementById(window._DGLayer);
		var strSetClass = "";
		var _arrReturnData = new Array();
		for (var i = 0 ; i < objContainer.arrDG.length ; i++){
			var objarrGrids = objContainer.arrDG[i];
			if (objarrGrids.ID == this.ID){
				var intCount = 0 ;
				for (var x = 0 ; x < objarrGrids._DGRowSelected.length ; x++){
					if (objarrGrids._DGRowSelected[x][1] == true){
						_arrReturnData[intCount] = objarrGrids._DGRowSelected[x][3]	;
						intCount++;
					}		
				}
				break;
			}
		}
		return _arrReturnData;
	}
	
	function getSelectedColumn(intColumnIndex){
		var arrReturnData = new Array();
		if (intColumnIndex != ""){
			var strID = this.ID ;
			var objContainer = document.getElementById(window._DGLayer);
			var strSetClass = "";
			for (var i = 0 ; i < objContainer.arrDG.length ; i++){
				var objarrGrids = objContainer.arrDG[i];
				var strFileName = "frm_" + objarrGrids.ID ;
				if (objarrGrids.ID == strID){		
					var intDataCount = objarrGrids.arrGridData.length;
					for (var x = 0 ; x < intDataCount ; x++){
						var intReturnLength = arrReturnData.length;
						arrReturnData[intReturnLength] = new Array();
						arrReturnData[intReturnLength][0] = (x + 1);
						switch (objarrGrids._arrDGColumnType[intColumnIndex].toUpperCase()){
							case "LABEL" :
							case "IMAGE":
							case "LINK":
							case "SYSLINK":
							case "IMAGELINK":
								arrReturnData[intReturnLength][1] = objarrGrids.arrGridData[x][objarrGrids._arrDGArrayIndex[intColumnIndex]];
								break;
							case "CHECKBOX":
								var strChkID = 'CHK_' + strID + '_' + x + '_' + intColumnIndex ;
								var objChkControl = frames[strFileName].getFieldByID(strChkID);
								arrReturnData[intReturnLength][1] = objChkControl.checked;								
								break;
							case "RADIO":
								var strChkID = "";
								if (objarrGrids.radioButtonGroup != ""){
									var arrRadGroup = objarrGrids.radioButtonGroup.split("|");
									for (var n = 0 ; n < arrRadGroup.length ; n++){
										var arrRadCol = arrRadGroup[n].split(",");
										for (var m = 0 ; m < arrRadCol.length ; m++){
											if (intColumnIndex == arrRadCol[m]){
												strChkID = 'RAD_' + strID + '_' + x + '_' + n + '_' + m  ;
												break;
											}
										}
									}
								}
								if (strChkID == ""){
									strChkID = 'RAD_' + strID + '_' + x + '_' + intColumnIndex ;
								}
								var objChkControl = frames[strFileName].document.getElementById(strChkID);
								arrReturnData[intReturnLength][1] = objChkControl.checked;		
								
								break;
							case "CUSTOM":	
								var objCusControl = frames[strFileName].document.getElementsByName(objarrGrids._arrDGID[intColumnIndex]);
								arrReturnData[intReturnLength][1] = objCusControl[x].value;								
								break;
						}
					}
				}
			}
		}
		return arrReturnData;
	}
	
	
	function setColumnFocus(intRow, intColumnIndex){
		var arrReturnData = new Array();
		if (intColumnIndex != ""){
			var strID = this.ID ;
			var objContainer = document.getElementById(window._DGLayer);
			var strSetClass = "";
			for (var i = 0 ; i < objContainer.arrDG.length ; i++){
				var objarrGrids = objContainer.arrDG[i];
				var strFileName = "frm_" + objarrGrids.ID ;
				if (objarrGrids.ID == strID){		
					var intDataCount = objarrGrids.arrGridData.length;
					var intReturnLength = arrReturnData.length;
					arrReturnData[intReturnLength] = new Array();
					arrReturnData[intReturnLength][0] = (intRow + 1);
					switch (objarrGrids._arrDGColumnType[intColumnIndex].toUpperCase()){
						case "LABEL" :
						case "IMAGE":
						case "LINK":
						case "SYSLINK":
						case "IMAGELINK":
							break;
						case "CHECKBOX":
							var strChkID = 'CHK_' + strID + '_' + intRow + '_' + intColumnIndex ;
							var objChkControl = frames[strFileName].getFieldByID(strChkID);
							objChkControl.focus();								
							break;
						case "RADIO":
							var strChkID = "";
							if (objarrGrids.radioButtonGroup != ""){
								var arrRadGroup = objarrGrids.radioButtonGroup.split("|");
								for (var n = 0 ; n < arrRadGroup.length ; n++){
									var arrRadCol = arrRadGroup[n].split(",");
									for (var m = 0 ; m < arrRadCol.length ; m++){
										if (intColumnIndex == arrRadCol[m]){
											strChkID = 'RAD_' + strID + '_' + intRow + '_' + n + '_' + m  ;
											break;
										}
									}
								}
							}
							if (strChkID == ""){
								strChkID = 'RAD_' + strID + '_' + intRow + '_' + intColumnIndex ;
							}
							var objChkControl = frames[strFileName].document.getElementById(strChkID);
							objChkControl.focus();		
							
							break;
						case "CUSTOM":	
							var objCusControl = frames[strFileName].document.getElementsByName(objarrGrids._arrDGID[intColumnIndex]);
							objCusControl[intRow].focus();								
							break;
					}
					break;
				}
			}
		}
		return arrReturnData;
	}
	
	function getSelectedColumnValue(intColumnIndex){
		var arrReturnData = new Array();
		if (intColumnIndex != ""){
			var strID = this.ID ;
			var objContainer = document.getElementById(window._DGLayer);
			var strSetClass = "";
			for (var i = 0 ; i < objContainer.arrDG.length ; i++){
				var objarrGrids = objContainer.arrDG[i];
				var strFileName = "frm_" + objarrGrids.ID ;
				if (objarrGrids.ID == strID){		
					var intDataCount = objarrGrids.arrGridData.length;
					for (var x = 0 ; x < intDataCount ; x++){
						var intReturnLength = arrReturnData.length;
						arrReturnData[intReturnLength] = new Array();
						arrReturnData[intReturnLength][0] = (x + 1);
						switch (objarrGrids._arrDGColumnType[intColumnIndex].toUpperCase()){
							case "LABEL" :
							case "IMAGE":
							case "LINK":
							case "SYSLINK":
							case "IMAGELINK":
								arrReturnData[intReturnLength][1] = objarrGrids.arrGridData[x][objarrGrids._arrDGArrayIndex[intColumnIndex]];
								break;
							case "CHECKBOX":
								var strChkID = 'CHK_' + strID + '_' + x + '_' + intColumnIndex ;
								var objChkControl = frames[strFileName].getFieldByID(strChkID);
								if (objChkControl.checked){
									arrReturnData[intReturnLength][1] = objChkControl.value;
								}else{
									arrReturnData[intReturnLength][1] = null;
								}
								break;
							case "RADIO":
								var strChkID = ""
								if (objarrGrids.radioButtonGroup != ""){
									var arrRadGroup = objarrGrids.radioButtonGroup.split("|");
									for (var n = 0 ; n < arrRadGroup.length ; n++){
										var arrRadCol = arrRadGroup[n].split(",");
										for (var m = 0 ; m < arrRadCol.length ; m++){
											if (intColumnIndex == arrRadCol[m]){
												strChkID = 'RAD_' + strID + '_' + x + '_' + n + '_' + m  ;
												break;
											}
										}
									}
								}
								if (strChkID == ""){
									strChkID = 'RAD_' + strID + '_' + x + '_' + intColumnIndex ;
								}
								var objChkControl = frames[strFileName].document.getElementById(strChkID);
								if (objChkControl.checked){
									arrReturnData[intReturnLength][1] = objChkControl.value;		
								}else{
									arrReturnData[intReturnLength][1] = null;		
								}
								break;
							case "CUSTOM":	
								var objCusControl = frames[strFileName].document.getElementsByName(objarrGrids._arrDGID[intColumnIndex]);
								arrReturnData[intReturnLength][1] = objCusControl[x].value;								
								break;
						}
					}
				}
			}
		}
		return arrReturnData;
	}
	
	function _gridReSize(){
		var _objDG = this;
		if (arguments.length == 1){
			_objDG = _getDGObject(arguments[0])
		}
		
		var objCont = document.getElementById(_objDG.getFrameName);
		objCont.style.height = _objDG.height;

		objCont = document.getElementById(_objDG._contentLocId);
		objCont.style.height = _objDG.height;
	}
	
	function gridRefresh(){
		this._DGCheckAll.length = 0;
		this._DGRowSelected.length = 0;
		this._DGarrDisStatus.length = 0;
		this._DGarrChkStatus.length = 0;
		this._blnFirstLoad = true;
		
		_gridReSize(this.ID);
		gridWrite(this.ID);
		
		if (this.paging){
			this.pgObject.numStart = this.seqStartNo;
			this.pgObject.numRecPage = this.pgnumRecPage;							
			this.pgObject.numRecTotal = this.pgnumRecTotal;		
			this.recNavigateRefresh();
		}
	}
	
	function gridRowClick(strRowID, strID, strRowNo, intColumnCount, strClientFunction){
		var objContainer = document.getElementById(window._DGLayer);
		var strSetClass = "";
		var strFrmName = "frm_" + strID;
		for (var i = 0 ; i < objContainer.arrDG.length ; i++){
			var objarrGrids = objContainer.arrDG[i];
			if (objarrGrids.ID == strID){
				var blnContinue = true;
				if (objarrGrids.rowClickValidate != ""){
					blnContinue = eval(objarrGrids.rowClickValidate + "(" + strRowNo + ")");
				}
				
				if (blnContinue){
					objarrGrids._DGLastRowClick = strRowNo;
					if (objarrGrids.rowSelect){
						if (!objarrGrids.rowMultiSelect){
							for (var x = 0 ; x < objarrGrids._DGRowSelected.length ; x++){
								if (objarrGrids._DGRowSelected[x][0] != strRowNo){
									objarrGrids._DGRowSelected[x][1] = false;
									strSetClass = objarrGrids._DGRowSelected[x][2];
									var arrTempColColors = null;
									if (objarrGrids.setRowBGColors != ""){
										if (objarrGrids.blnColBGColors){
											arrTempColColors = strSetClass.split("^");
										}
									}
									for (var a = 1 ; a <= intColumnCount ; a++){
										var strColID = "TD_" + "DG_" + objarrGrids.ID + "_" + x + "_" + a ;
										if (arrTempColColors != null){
											strSetClass = arrTempColColors[a-1];
										}
										frames[strFrmName].setStyleClass(strColID, strSetClass);
									}
								}
							}
						}
						
						
						for (var x = 0 ; x < objarrGrids._DGRowSelected.length ; x++){
							if (objarrGrids._DGRowSelected[x][0] == strRowNo){
								var arrTempColColors = null;
								if (objarrGrids._DGRowSelected[x][1] == true){
									objarrGrids._DGRowSelected[x][1] = false;
									strSetClass = objarrGrids._DGRowSelected[x][2];
									if (objarrGrids.setRowBGColors != ""){
										if (objarrGrids.blnColBGColors){
											arrTempColColors = strSetClass.split("^");
										}
									}
								}else{
									objarrGrids._DGRowSelected[x][1] = true;
									strSetClass = "GridItemSelected"
								}
								for (var a = 1 ; a <= intColumnCount ; a++){
									var strColID = "TD_" + strRowID + "_" + a ;
									if (arrTempColColors != null){
										strSetClass = arrTempColColors[a-1];
									}
									frames[strFrmName].setStyleClass(strColID, strSetClass);
								}		
								break;
							}
						}
					}
				}
				break;
			}
		}
		if (strClientFunction != ""){
			eval(strClientFunction + "(" + strRowNo + ")");
		}
	}
	
	function gridRowDblClick(strRowID, strID, strRowNo, intColumnCount, strClientFunction){
		var objContainer = document.getElementById(window._DGLayer);
		var strSetClass = "";
		var strFrmName = "frm_" + strID;
		for (var i = 0 ; i < objContainer.arrDG.length ; i++){
			var objarrGrids = objContainer.arrDG[i];
			if (objarrGrids.ID == strID){
				var blnContinue = true;
				if (objarrGrids.rowDblClickValidate != ""){
					blnContinue = eval(objarrGrids.rowDblClickValidate + "(" + strRowNo + ")");
				}
				
				if (blnContinue){
					objarrGrids._DGLastRowClick = strRowNo;
					if (objarrGrids.rowSelect){
						if (!objarrGrids.rowMultiSelect){
							for (var x = 0 ; x < objarrGrids._DGRowSelected.length ; x++){
								if (objarrGrids._DGRowSelected[x][0] != strRowNo){
									objarrGrids._DGRowSelected[x][1] = false;
									strSetClass = objarrGrids._DGRowSelected[x][2];
									var arrTempColColors = null;
									if (objarrGrids.setRowBGColors != ""){
										if (objarrGrids.blnColBGColors){
											arrTempColColors = strSetClass.split("^");
										}
									}
									for (var a = 1 ; a <= intColumnCount ; a++){
										var strColID = "TD_" + "DG_" + objarrGrids.ID + "_" + x + "_" + a ;
										if (arrTempColColors != null){
											strSetClass = arrTempColColors[a-1];
										}
										frames[strFrmName].setStyleClass(strColID, strSetClass);
									}
								}
							}
						}
						
						
						for (var x = 0 ; x < objarrGrids._DGRowSelected.length ; x++){
							if (objarrGrids._DGRowSelected[x][0] == strRowNo){
								var arrTempColColors = null;
								if (objarrGrids._DGRowSelected[x][1] == true){
									objarrGrids._DGRowSelected[x][1] = false;
									strSetClass = objarrGrids._DGRowSelected[x][2];
									if (objarrGrids.setRowBGColors != ""){
										if (objarrGrids.blnColBGColors){
											arrTempColColors = strSetClass.split("^");
										}
									}
								}else{
									objarrGrids._DGRowSelected[x][1] = true;
									strSetClass = "GridItemSelected"
								}
								for (var a = 1 ; a <= intColumnCount ; a++){
									var strColID = "TD_" + strRowID + "_" + a ;
									if (arrTempColColors != null){
										strSetClass = arrTempColColors[a-1];
									}
									frames[strFrmName].setStyleClass(strColID, strSetClass);
								}		
								break;
							}
						}
					}
				}
				break;
			}
		}
		
		if (strClientFunction != ""){
			eval(strClientFunction + "(" + strRowNo + ")");
		}
	}
	
	function gridChkAll(strID, strHDIndex){
		var strField = 'CHK_' + strID + '_0_' + strHDIndex; 
		var blnChk = false;
		var objContainer = document.getElementById(window._DGLayer);
		for (var i = 0 ; i < objContainer.arrDG.length ; i++){
			var objarrGrids = objContainer.arrDG[i];
			var strFrmName = "frm_" + objarrGrids.ID ;
			if (objarrGrids.ID == strID){
				var blnFound = false
				if (objarrGrids.arrGridData.length > 0){
					for (var x = 0 ; x < objarrGrids._DGCheckAll.length ; x++){
						if (objarrGrids._DGCheckAll[x][0] == strHDIndex){
							if (objarrGrids._DGCheckAll[x][1] == ""){
								objarrGrids._DGCheckAll[x][1] = false;
							}
							
							if (objarrGrids._DGCheckAll[x][1] == true){
								objarrGrids._DGCheckAll[x][1] = false;
							}else{
								objarrGrids._DGCheckAll[x][1] = true;
							}
							blnChk = objarrGrids._DGCheckAll[x][1];
							blnFound = true
							break;
						}
					}
					
					if (!blnFound){
						var intArrPos = objarrGrids._DGCheckAll.length ;
						objarrGrids._DGCheckAll[intArrPos] = new Array();
						objarrGrids._DGCheckAll[intArrPos][0] = strHDIndex;
						objarrGrids._DGCheckAll[intArrPos][1] = true ;
						blnChk = true;
					}
					frames[strFrmName].setField(strField, blnChk);
					var intRecCount = objarrGrids._DGarrChkStatus.length;
					for (var x = 0 ;  x < intRecCount ; x++){
						objarrGrids._DGarrChkStatus[x][strHDIndex] = blnChk;
						if (objarrGrids._arrlinkOnClick[strHDIndex] != ""){
							eval(objarrGrids._arrlinkOnClick[strHDIndex] + "(" + x + ")");
						}
					}
				}
				break;
			}
		}
	}
	
	function gridSort(strID, strHDIndex){
		var objContainer = document.getElementById(window._DGLayer);
		var strGridHTMLText = "" ;
		var strOrder = "";
		for (var i = 0 ; i < objContainer.arrDG.length ; i++){
			var objarrGrids = objContainer.arrDG[i];
			if (objarrGrids.ID == strID){
				var strimgID = "";
				var strimgURL = "";
				if (strHDIndex != ""){
					if (objarrGrids._DGSortHDIndex == ""){
						objarrGrids._DGSortHDIndex = strHDIndex;
					}
					
					if (objarrGrids._DGSortHDIndex != strHDIndex){
						objarrGrids._DGSortHDIndex = strHDIndex;
						objarrGrids._DGSortOrder = "D";
					}
					
					
					strimgID = 'img_GridHD_' + objarrGrids.ID + '_' + strHDIndex;
					strimgURL = objDG.imagePath;
					
					if (objarrGrids._DGSortOrder == "D"){
						objarrGrids._DGSortOrder = "A";
						strimgURL += "GA_no_cache.gif";
						strOrder = "ASC";
					}else{
						objarrGrids._DGSortOrder = "D";
						strimgURL += "GD_no_cache.gif";
						strOrder = "DESC";
					}
					// --------- Arrray Sort
					sortArray(objarrGrids._arrDGArrayIndex[strHDIndex], strOrder, objarrGrids.arrGridData);
					
					gridWrite(objarrGrids.ID);
					setImage(strimgID, strimgURL);
					setVisible(strimgID, true);
				}
			}
		}
	}
	
	function sortArray(intColumn, strOrder, strArrayName){
		strArrayName = eval(strArrayName);
		var intCount = strArrayName.length;
		for (var a = 1 ; a = 1;){
		//while (blnFound == false){
			var blnFound = false;
			for (var i = 0 ; i < intCount ; i++){
				if ((i+1) < intCount){
					var strCompareValue = strArrayName[i][intColumn];
					var strCompareTo = strArrayName[i+1][intColumn];
					
					// Date Check 
					var arrMTemp;
					var arrTemp;
					var strLastValues = ""
					if (strCompareValue.indexOf('/') != -1){
						arrMTemp = strCompareValue.split(" ");
						arrTemp = arrMTemp[0].split("/");
						if (arrTemp.length == 3){
							if (arrTemp[2].length == 4 && arrTemp[1].length == 2 && arrTemp[0].length == 2){
								strLastValues = "";
								if (arrMTemp.length == 2){
									strLastValues = arrMTemp[1].replace(":","");
								}
								strCompareValue = String(arrTemp[2]) + String(arrTemp[1]) + String(arrTemp[0]) + strLastValues;
							}
						}
					}
					
					if (strCompareValue.indexOf(',') != -1){
						strCompareValue = strCompareValue.replace(',',"");
					}
					
					if (strCompareTo.indexOf('/') != -1){
						arrMTemp = strCompareTo.split(" ");
						arrTemp = arrMTemp[0].split("/");
						if (arrTemp.length == 3){
							strLastValues = "";
								if (arrMTemp.length == 2){
									strLastValues = arrMTemp[1].replace(":","");
								}
								strCompareTo = String(arrTemp[2]) + String(arrTemp[1]) + String(arrTemp[0]) + strLastValues;
						}
					}
					
					if (strCompareTo.indexOf(',') != -1){
						strCompareTo = strCompareTo.replace(',',"")
					}
					if (!isNaN(Number(strCompareValue))){
						strCompareValue = Number(strCompareValue);
					}else{
						strCompareValue = strArrayName[i][intColumn];
					}
					if (!isNaN(Number(strCompareTo))){
						strCompareTo = Number(strCompareTo);
					}else{
						strCompareTo = strArrayName[i+1][intColumn];
					}
					if (strOrder.toUpperCase() == "ASC"){
						if (strCompareValue > strCompareTo){
							var arrCurrData = new Array();
							var intarrColumns = strArrayName[i].length ;
							for (var x = 0 ; x < intarrColumns ; x++){
								arrCurrData[x] = strArrayName[i][x];
							}
							
							for (var x = 0 ; x < intarrColumns ; x++){
								strArrayName[i][x] = strArrayName[i+1][x];
							}
							
							for (var x = 0 ; x < intarrColumns ; x++){
								strArrayName[i+1][x] = arrCurrData[x];
							}
							blnFound = true;
						}
					}else{
						if (strCompareValue < strCompareTo){
							var arrCurrData = new Array();
							var intarrColumns = strArrayName[i].length ;
							for (var x = 0 ; x < intarrColumns ; x++){
								arrCurrData[x] = strArrayName[i][x];
							}
							
							for (var x = 0 ; x < intarrColumns ; x++){
								strArrayName[i][x] = strArrayName[i+1][x];
							}
							
							for (var x = 0 ; x < intarrColumns ; x++){
								strArrayName[i+1][x] = arrCurrData[x];
							}
							blnFound = true;
						}				
					}
				}
			}
			if (blnFound == false){
				break;
			}
		}
	}
	
	function gridHDMouseOver(strID){
		window._GridColumClass = getStyleClass(strID);
		setStyleClass(strID, "GridHeadHover");
	}

	function gridHDMouseOut(strID){
		setStyleClass(strID, window._GridColumClass);
	}
	
	function gridMouseOver(strRowID, strID, strRowNo, intColumnCount){
		for (var i = 1 ; i <= intColumnCount ; i++){
			var strColID = "TD_" + strRowID + "_" + i ;
			var strFrmID = "frm_" + strID ;
			frames[strFrmID].document.getElementById(strColID).className = "GridItemHover";
			//setStyleClass(strColID, "GridItemHover");
		}
	}

	function gridMouseOut(strRowID, strID, strRowNo, intColumnCount){
		var objContainer = document.getElementById(window._DGLayer);
		var strStyleClass = window._GridColumClass;
		var arrTempColColors = null;
		for (var i = 0 ; i < objContainer.arrDG.length ; i++){
			var objarrGrids = objContainer.arrDG[i];
			if (objarrGrids.ID == strID){
				for (var x = 0 ; x < objarrGrids._DGRowSelected.length ; x++){
					if (objarrGrids._DGRowSelected[x][0] == strRowNo){
						if (objarrGrids._DGRowSelected[x][1] == ""){
							strStyleClass = objarrGrids._DGRowSelected[x][2];
							if (objDG.setRowBGColors != ""){
								if (objDG.blnColBGColors){
									arrTempColColors = strStyleClass.split("^");
								}
							}
						}else{
							strStyleClass = "GridItemSelected";
						}
						break;
					}
				}
				break;
			}
		}
		
		for (var i = 1 ; i <= intColumnCount ; i++){
			var strColID = "TD_" + strRowID + "_" + i;
			//setStyleClass(strColID, strStyleClass);
			var strFrmID = "frm_" + strID ;
			if (arrTempColColors != null){
				strStyleClass = arrTempColColors[i-1];
			}
			frames[strFrmID].document.getElementById(strColID).className = strStyleClass;

		}
	}
	
	function setDisable(intRow, intCol, blnDisable){
		if (String(intCol) == "" && String(intRow) == ""){
			var intColumnCount = this._arrDGHeaderText.length;
			for (var y = 0 ; y < intColumnCount ; y++){
				setCellDisable(this, "", y, blnDisable);
			}
		}else{
			if (String(intRow) != "" && String(intCol) == ""){
				var intColumnCount = this._arrDGHeaderText.length;
				for (var y = 0 ; y < intColumnCount ; y++){
					setCellDisable(this, intRow, y, blnDisable);
				}
			}else{
				if (String(intCol) != "" && String(intRow) == ""){
					setCellDisable(this, "", intCol,  blnDisable);
				}else{
					setCellDisable(this, intRow, intCol,  blnDisable);
				}
			}
		}
	}
	
	function setCellDisable(objDG, intRow, intCol, blnDisable){
		if (intCol < objDG._arrDGHeaderText.length ){
			if (objDG._arrDGColumnType[intCol].toUpperCase() == "CUSTOM" || objDG._arrDGColumnType[intCol].toUpperCase() == "CHECKBOX" || objDG._arrDGColumnType[intCol].toUpperCase() == "RADIO"){
				var intDataCount = objDG.arrGridData.length;
				var strFrmName = "frm_" + objDG.ID ;
				if (String(intRow) == ""){
					for (var x = 0 ; x < intDataCount ; x++){
						var objControl = "";
						switch (objDG._arrDGColumnType[intCol].toUpperCase()){
							case "CHECKBOX" :
								objControl =  frames[strFrmName].document.getElementById('CHK_' + objDG.ID + '_' + x + '_' + intCol); 
								objControl.disabled = blnDisable;
								break;
							case "RADIO" :
								var strChkID = "";
								var blnFound = true;
								if (objDG.radioButtonGroup != ""){
									var arrRadGroup = objDG.radioButtonGroup.split("|");
									for (var n = 0 ; n < arrRadGroup.length ; n++){
										var arrRadCol = arrRadGroup[n].split(",");
										for (var m = 0 ; m < arrRadCol.length ; m++){
											if (intCol == arrRadCol[m]){
												strChkID = 'RAD_' + objDG.ID + '_' + x + '_' + n + '_' + m  ;
												blnFound = false;
												break;
											}
										}
									}
									if (blnFound){
										strChkID = 'RAD_' + objDG.ID + '_' + x + '_' + intCol ;
									}
								}else{
									strChkID = 'RAD_' + objDG.ID + '_' + x + '_' + intCol ;
								}
								if (strChkID != ""){
									objControl =  frames[strFrmName].document.getElementById(strChkID);
									objControl.disabled = blnDisable;
								}
								break;
							case "CUSTOM" :
								objControl = frames[strFrmName].document.getElementsByName(objDG._arrDGID[intCol]);
								objControl[x].disabled = blnDisable;
								break;
						}
						
						// Store the disabled status in the array 
						try{
							var intRowNo = objDG.arrGridData[x][objDG.arrGridData[x].length - 1] ;
							objDG._DGarrDisStatus[intRowNo][intCol] = blnDisable;
						}catch(e){}
					}
				}else{
					switch (objDG._arrDGColumnType[intCol].toUpperCase()){
						case "CHECKBOX" :
							objControl = frames[strFrmName].document.getElementById('CHK_' + objDG.ID + '_' + intRow + '_' + intCol); 
							objControl.disabled = blnDisable;
							break;
						case "RADIO" :
							var strChkID = "";
							var blnFound = true;
							if (objDG.radioButtonGroup != ""){
								var arrRadGroup = objDG.radioButtonGroup.split("|");
								for (var n = 0 ; n < arrRadGroup.length ; n++){
									var arrRadCol = arrRadGroup[n].split(",");
									for (var m = 0 ; m < arrRadCol.length ; m++){
										if (intCol == arrRadCol[m]){
											strChkID = 'RAD_' + objDG.ID + '_' + intRow + '_' + n + '_' + m  ;
											blnFound = false;
											break;
										}
									}
								}
								
								if (blnFound){
									strChkID = 'RAD_' + objDG.ID + '_' + intRow + '_' + intCol ;
								}
							}else{
								strChkID = 'RAD_' + objDG.ID + '_' + intRow + '_' + intCol ;
							}
							if (strChkID != ""){
								objControl =  frames[strFrmName].document.getElementById(strChkID);
								objControl.disabled = blnDisable;
							}
							break;
						case "CUSTOM" :
							objControl = frames[strFrmName].document.getElementsByName(objDG._arrDGID[intCol]);
							objControl[intRow].disabled = blnDisable;
							break;
					}
					
					// Store the disabled status in the array 
					try{
						var intRowNo = objDG.arrGridData[intRow][objDG.arrGridData[intRow].length - 1] ;
						objDG._DGarrDisStatus[intRowNo][intCol] = blnDisable;
					}catch(e){}
				}
			}
		}
	}
	
	function gridCellOnChange(intRow, intCol, objControl, strID){
		var objContainer = document.getElementById(window._DGLayer);
		var strSetClass = "";
		for (var i = 0 ; i < objContainer.arrDG.length ; i++){
			var objDG = objContainer.arrDG[i];
			if (objDG.ID == strID){	
				if (objControl.type.toUpperCase() == "CHECKBOX"){
					var strTempValue = "";
					if (objControl.checked){
						strTempValue = objControl.value;
					}
					objDG.arrGridData[intRow][objDG._arrDGArrayIndex[intCol]] = strTempValue;
				}else{
					objDG.arrGridData[intRow][objDG._arrDGArrayIndex[intCol]] = objControl.value;
				}
				if (objDG._arrOnChange[intCol] != ""){
					eval(objDG._arrOnChange[intCol])(objControl, intRow, intCol);
				}
				break			
			}
		}
	}
	
	function getCellValue(intRow, intCol){
		var strReturn = "";
		if (intRow < this.arrGridData.length){
			strReturn = this.arrGridData[intRow][this._arrDGArrayIndex[intCol]];
		}
		return strReturn;
	}
	function getCellValueDup(intRow, intCol){
        var objDG = this;
		var strFrmName = "frm_" + objDG.ID ;
        switch (objDG._arrDGColumnType[intCol].toUpperCase()){
            case "CUSTOM-SELECT" :
            	var objControl = frames[strFrmName].document.getElementsByName(objDG._arrDGID[intCol]);
            	var strType = getFieldType(frames[strFrmName].document.getElementById(objDG._arrDGID[intCol]));						
            	return objControl[intRow].value;					
            break ;
        }
		
	}
	function setCellValue(intRow, intCol, strValue, strCheckValue){
		if (String(intRow) == "" || String(intCol) == ""){
			return ;
		}else{
			var objDG = this;
			var strFrmName = "frm_" + objDG.ID ;
			objDG.arrGridData[intRow][objDG._arrDGArrayIndex[intCol]] = strValue;
			switch (objDG._arrDGColumnType[intCol].toUpperCase()){
				case "LABEL" :
					var strCellHTML = "";
					var strCellID = 'lblSpn_' + objDG.ID + '_' + intCol + '_' + intRow;
					var strtoolTip = "";
					if (objDG._arrDGtoolTip[intCol] != "" || objDG._arrDGtoolTip[intCol] != null){
						strtoolTip += ' title="' + objDG._arrDGtoolTip[intCol] + '"';  
					}	
					
					var strFontColor = "";
					if (objDG.setColors != ""){
						var funGetColors = eval(objDG.setColors);
						var strRowColors = funGetColors(objDG.arrGridData[intRow]);
						if (strRowColors!= ""){
							strFontColor = 'style="color:' + strRowColors + ';"';
						}
					}
					
					strCellHTML += '<font';
					strCellHTML += strtoolTip + strFontColor ;
					strCellHTML += '>' + objDG.arrGridData[intRow][objDG._arrDGArrayIndex[intCol]] + '<\/font>';
					frames[strFrmName].DivWrite(strCellID, strCellHTML);
					break ;
					
				case "CHECKBOX" :
					if (strValue != true || strValue != false){
						switch (strValue.toUpperCase()){
							case "TRUE" : strValue = true; break;
							case "FALSE" : strValue = false; break;
						}
					}
					if (strValue == true || strValue == false){
						frames[strFrmName].document.getElementById('CHK_' + objDG.ID + '_' + intRow + '_' + intCol).checked = strValue;
						gridChkBoxClick(strValue, intRow, intCol, objDG.ID, objDG._arrlinkOnClick[intCol])
					}
					break;
					
				case "RADIO" :
					if (objDG.radioButtonGroup != ""){
						var arrRadGroup = objDG.radioButtonGroup.split("|");
						var blnFound = true;
						var strRadID = "";
						for (var n = 0 ; n < arrRadGroup.length ; n++){
							var arrRadCol = arrRadGroup[n].split(",");
							for (var m = 0 ; m < arrRadCol.length ; m++){
								if (intCol == arrRadCol[m]){
									strRadID = 'RAD_' + objDG.ID + '_' + intRow + '_' + n + '_' + m  ;
									blnFound = false;
									break;
								}
							}
							
							if (!blnFound){
								for (var m = 0 ; m < arrRadCol.length ; m++){
									strRadID = 'RAD_' + objDG.ID + '_' + intRow + '_' + n + '_' + m  ;
									frames[strFrmName].document.getElementById(strRadID).checked = false;
								}
							}
							
						}
						
						if (blnFound){
							strRadID = 'RAD_' + objDG.ID + '_' + intRow + '_' + intCol ;
						}
					}else{
						strRadID = 'RAD_' + objDG.ID + '_' + intRow + '_' + intCol ;
					}
					if (strRadID != ""){
						if (strValue != true || strValue != false){
							switch (strValue.toUpperCase()){
								case "TRUE" : strValue = true; break;
								case "FALSE" : strValue = false; break;
							}
						}
						if (strValue == true || strValue == false){
							frames[strFrmName].document.getElementById(strRadID).checked = strValue;
							gridRadioClick(strValue, intRow, intCol, objDG.ID);
						}
					}
					break;
					
				case "CUSTOM" :
				case "CUSTOM-SELECT" :
					var objControl = frames[strFrmName].document.getElementsByName(objDG._arrDGID[intCol]);
					var strType = getFieldType(frames[strFrmName].document.getElementById(objDG._arrDGID[intCol]));
					switch (strType){
						case "TEXT" :
							objControl[intRow].value = strValue;
							break;
						case "PASSWORD" :
							objControl[intRow].value = strValue;
							break;
						case "SELECT-ONE" :
							var intLengrh = objControl[intRow].length ;
							for (var y = 0 ; y < intLengrh ; y++){
								if (objControl[intRow].options[y].value == strValue){
									objControl[intRow].options[y].selected = true;
									break;
								}
							}
							break;
					}
					break ;
			}
		}
	}
	
	function gridChkBoxClick(objChk, intRowSeqNo, intCell, strID, objClientClick){
		var objContainer = document.getElementById(window._DGLayer);
		var strSetClass = "";
		for (var i = 0 ; i < objContainer.arrDG.length ; i++){
			var objDG = objContainer.arrDG[i];
			if (objDG.ID == strID){	
				if (objChk == true || objChk == false){
					objDG._DGarrChkStatus[intRowSeqNo][intCell] = objChk;
					objDG.arrGridData[intRowSeqNo][objDG._arrDGArrayIndex[intCell]] = objChk;
				}else{
					objDG._DGarrChkStatus[intRowSeqNo][intCell] = objChk.checked;
					objDG.arrGridData[intRowSeqNo][objDG._arrDGArrayIndex[intCell]] = objChk.checked;
				}
				break			
			}
		}
		if (objClientClick != ""){
			eval(objClientClick + "(" + intRowSeqNo + ")");
		}
	}
	
	function gridRadioClick(objChk, intRowSeqNo, intCell, strID){
		var objContainer = document.getElementById(window._DGLayer);
		var strSetClass = "";
		for (var i = 0 ; i < objContainer.arrDG.length ; i++){
			var objDG = objContainer.arrDG[i];
			if (objDG.ID == strID){	
				var blnRow = true;
				if (objDG.radioButtonGroup != ""){
					var arrRadGroup = objDG.radioButtonGroup.split("|");
					for (var n = 0 ; n < arrRadGroup.length ; n++){
						var arrRadCol = arrRadGroup[n].split(",");
						var blnGroup = false;
						for (var m = 0 ; m < arrRadCol.length ; m++){
							if (intCell == arrRadCol[m]){	
								blnGroup = true;
								break;
							}
						}				
						
						// Initlize all the column group elements
						if (blnGroup){
							for (var m = 0 ; m < arrRadCol.length ; m++){
								objDG._DGarrChkStatus[intRowSeqNo][arrRadCol[m]] = false;
								blnRow = false;
							}
						}
					}
				}
				// Initialize all the Column row elements
				if (blnRow){
					for (var x = 0 ; x < objDG._DGarrChkStatus.length ; x ++){
						objDG._DGarrChkStatus[x][intCell] = false;
					}
				}
				if (objChk == true || objChk == false){
					objDG._DGarrChkStatus[intRowSeqNo][intCell] = objChk;
				}else{
					objDG._DGarrChkStatus[intRowSeqNo][intCell] = objChk.checked;
				}
				
				if (objDG._arrOnChange[intCell] != ""){
					eval(objDG._arrOnChange[intCell])(objChk, intRowSeqNo, intCell);
				}
				break			
			}
		}
	}
	
	function getRowValue(intRow){
		var strReturn = "";
		var objDG = this;
		var intColumnCount = objDG._arrDGHeaderText.length;
		var intColCount = objDG.arrGridData[intRow].length - 1;
		strReturn = new Array();
		for (var i = 0 ; i < (intColumnCount) ; i++){
			strReturn[i] = objDG.arrGridData[intRow][objDG._arrDGArrayIndex[i]];
		}
		return strReturn;
	}
	
	function selectRow(intRowNo){
		var objContainer = document.getElementById(window._DGLayer);
		for (var i = 0 ; i < objContainer.arrDG.length ; i++){
			var objarrGrids = objContainer.arrDG[i];
			if (objarrGrids.ID == this.ID){
				var intDataCount = objarrGrids.arrGridData.length;
				for (var x = 0 ; x < intDataCount ; x++){
					var strRowID = "DG_" + objarrGrids.ID + "_" + x;
					if (intRowNo == null){
						if (arguments.length == 2){
							if (objarrGrids._DGRowSelected[x][1] != arguments[1]){
								gridRowClick(strRowID, objarrGrids.ID, x, objarrGrids._arrDGHeaderText.length, objarrGrids.rowClick);
							}
						}else{
							gridRowClick(strRowID, objarrGrids.ID, x, objarrGrids._arrDGHeaderText.length, objarrGrids.rowClick);
						}
					}else{
						if (intRowNo == x){
							if (arguments.length == 2){
								if (objarrGrids._DGRowSelected[x][1] != arguments[1]){
									gridRowClick(strRowID, objarrGrids.ID, x, objarrGrids._arrDGHeaderText.length, objarrGrids.rowClick);
									return;
								}
							}else{
								gridRowClick(strRowID, objarrGrids.ID, x, objarrGrids._arrDGHeaderText.length, objarrGrids.rowClick);
								return;
							}
						}
					}
				}
			}
		}
	}
	
	function gridVisible(blnStatus){
		var strFrmName = "frm_" + this.ID;
		var strID = this.ID;
		if (blnStatus){
			document.getElementById(strFrmName).style.visibility = "visible";
			document.getElementById(strID).style.visibility = "visible";
		}else{
			document.getElementById(strFrmName).style.visibility = "hidden";
			document.getElementById(strID).style.visibility = "hidden"

		}
	}
	
	function gridShortCutKey(objEvent, strID){
		var objContainer = document.getElementById(window._DGLayer);
		var strGridHTMLText = "" ;
		for (var i = 0 ; i < objContainer.arrDG.length ; i++){
			var objDG = objContainer.arrDG[i];
			if (objDG.ID == strID){
				if (objDG.shortCutKeyFunction != ""){
					eval(objDG.shortCutKeyFunction + "(objEvent)" );
				}
				break;
			}
		}
	}
	
	function gridFocus(){
		var objContainer = document.getElementById(window._DGLayer);
		for (var i = 0 ; i < objContainer.arrDG.length ; i++){
			var objarrGrids = objContainer.arrDG[i];
			if (objarrGrids.ID == this.ID){
				var objControl = "";
				var strFrame = objarrGrids.getFrameName  ;
				switch (objarrGrids._arrDGColumnType[0].toUpperCase()){
					case "CHECKBOX"	: objControl = 'CHK_' + objarrGrids.ID + '_0_0'; break;
					case "RADIO"	: objControl = 'RAD_' + objarrGrids.ID + '_0_0'; break;
					case "CUSTOM"	:
						objControl = frames[strFrame].document.getElementsByName(objarrGrids._arrDGID[0]);
						if (objControl.length >1){
							objControl[0].focus();
							objControl = "";
						}else{
							objControl = objarrGrids._arrDGID[0];
						}
						break;
				}				
				
				if (objControl != ""){
					frames[strFrame].document.getElementById(objControl).focus();
				}
				break;
			}
		}
	}
	
	function gridKeyDown(objEvent, strRowID, strID, strRowNo, intColumnCount){
		var objContainer = document.getElementById(window._DGLayer);
		var strSetClass = "";
		for (var i = 0 ; i < objContainer.arrDG.length ; i++){
			var objarrGrids = objContainer.arrDG[i];
			if (objarrGrids.ID == strID){	
				var intDataCount = objarrGrids.arrGridData.length;
				switch (objEvent.keyCode){
					case 38 :
						objarrGrids._DGLastRowClick = Number(objarrGrids._DGLastRowClick) - 1;
						if (objarrGrids._DGLastRowClick < 0){
							objarrGrids._DGLastRowClick = 0;
						}
						break ;
					case 40:
						objarrGrids._DGLastRowClick = Number(objarrGrids._DGLastRowClick) + 1;
						if (objarrGrids._DGLastRowClick > intDataCount){
							objarrGrids._DGLastRowClick = intDataCount;
						}
						break ;
				}
				strRowID = 'DG_' + strID + '_' + objarrGrids._DGLastRowClick;
				//gridRowClick(strRowID, strID, strRowNo, intColumnCount);
				break
			}
		}
	}
	
	/*
	 * Set the Column Background Color
	 * - for the Fixed colomb Background Colors
	 * Para 1 = Color  / Para 2 - Row
	 * - Column Wise Background colors Changes
	 * Para 1 = Color  / Para 2 - Row / Para 3 = Color
	 * PUBLIC
	 */
	function _setBGColor(strColor, intRow){
		var blnProceed = true;
		var intStartCol = 0;
		if (String(intRow) == ""){
			blnProceed = false;
		}
		
		if (arguments.length == 3){
			if (String(arguments[2]) != ""){
				intStartCol = Number(arguments[2]);
			}
		}
		
		if (blnProceed){
			var _objDG = _getDGObject(this.ID)
			if (_objDG.setRowBGColors == ""){return;}
			
			var intColumnCount = _objDG._arrDGHeaderText.length;
			if (strColor == ""){
				if ((intRow%2) != 0){
					strColor = _objDG.alternateRowClass;
				}
			}
			
			if (intStartCol > intColumnCount){return;}
			
			var arrTempColors = null;
			var strCursorPointer = "";
			if (_objDG.rowClick != ""){strCursorPointer = " cursorPointer ";}
			var strColBGColor = 'GridItemRow GridItem '+ strColor + " " + strCursorPointer;
			if (_objDG.setRowBGColors != ""){
				if (!objDG.blnColBGColors){
					_objDG._DGRowSelected[intRow][2] = strColBGColor;
				}else{
					arrTempColors = _objDG._DGRowSelected[intRow][2].split("^");
					if (String(arguments[2]) != ""){
						arrTempColors[intStartCol] = strColBGColor;
					}
					var strRowStyle = "";
					for (var i = 0 ; i < arrTempColors.length ; i++){
						if (String(arguments[2]) == ""){
							arrTempColors[i] = strColBGColor;
						}
						strRowStyle += arrTempColors[i] + "^";
					}		
					_objDG._DGRowSelected[intRow][2] = strRowStyle;
				}
			}
			
			
			var strRowID = "DG_" + _objDG.ID + "_" + intRow;
			var strColID = "";
			
			if (_objDG._DGRowSelected[intRow][1] == false){
				var strFrmName = "frm_" + _objDG.ID;
				for (var i = intStartCol ; i < intColumnCount ; i++){
					strColID = "TD_" + strRowID + "_" + (i+1) ;
					if (arrTempColors != null){
						frames[strFrmName].setStyleClass(strColID, arrTempColors[i]);
						if (String(arguments[2]) != ""){
							break;
						}
					}else{
						frames[strFrmName].setStyleClass(strColID, strColBGColor);
					}
				}
			}
		}
	}
	
	/*
	 * Focus to row
	 * PUBLIC
	 */
	function _gridFocusRow(intRow){
		var _objDG = this;
		if (arguments.length == 2){
			_objDG = _getDGObject(arguments[1])
		}
		try{
			frames[_objDG.getFrameName].getFieldByID("idSeq_" + intRow).focus();
		}catch(ex){}
	}
	
	/*
	 * Return the Grid Frame Object
	 * PUBLIC
	 */
	function _getGridPane(){
		var _objDG = this;
		if (arguments.length == 1){
			_objDG = _getDGObject(arguments[0])
		}
		
		return frames[_objDG.getFrameName];
	}
	
	/*
	 * Call the client mouse click function 
	 * PRIVATE
	 */
	function _gridRowMouseDown(intMouseBtn, intRow, strID){
		var _objDG = _getDGObject(strID);
		if (_objDG.onMouseDown != ""){
			eval(_objDG.onMouseDown + "(" + intMouseBtn + ",'" + intRow + "')");
		}
	}
	
	/*
	 * Get the Object
	 * PRIVATE
	 */
	 function _getDGObject(strID){
		var objCont = document.getElementById(window._DGLayer);
		var _objDG = null;
		for (var y = 0 ; y < objCont.arrDG.length ; y++){
			if (objCont.arrDG[y].ID == strID){
				_objDG = objCont.arrDG[y];
				break;
			}
		}
		return _objDG;
	 }
	// -------------------------------- End of File -----------------------------------