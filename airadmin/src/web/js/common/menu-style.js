addStylePad("pad", "item-offset:-1; offset-top:1;");
addStylePad("padSub", "pad-css:menuBar; offset-top:4; offset-left:6;");
addStylePad("Bar", "pad-css:menuBar;");
addStylePad("TopBar", "pad-css:menuTopBar;");


addStyleItem("itemTop", "css:menuTopItem, menuItemOn, menuItemDown, menuItemDown; width:actual; sub-menu:mouse-click;");
addStyleItem("itemSub", "css:menuItem, menuItemSub; width:actual;");

addStyleFont("fontTop", "css:menuFontOffBold, menuFontOnBold;");
addStyleFont("fontSub", "css:menuFontOff, menuFontOn;");

addStyleTag("tag", "css:tagOff, tagOn;");
addStyleSeparator("sep", "css:separatorT, separatorB;");

addStyleMenu("menu", "TopBar", "itemTop", "fontTop", "", "", "sep");
addStyleMenu("sub", "padSub", "itemSub", "fontSub", "tag", "", "sep");

addStyleGroup("group", "menu", "menu-top");
addStyleGroup("group", "sub", "A0000000000", "B0000000000", "C0000000000", "D0000000000", "E0000000000", "F0000000000", "G0000000000", "H0000000000");

addInstance("AAMenu", "AAMenu", "position:relative holder; menu-form:bar; offset-top:3px; offset-left:60px; style:group; sticky:yes;");
