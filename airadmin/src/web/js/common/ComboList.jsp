<%@ page language='java' contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<html>
<head>
<title></title>
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="-1">
<LINK rel="stylesheet" type="text/css" href="../../css/Cbo_Style_no_cache.css">
<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
</head>
<body class="cboScroll" style="overflow-X:hidden; overflow-Y:auto;" onmouseout="lstMouseOut()"  oncontextmenu="return false" ondrag='return false' onkeydown='return Body_onKeyDown(event)'>
<span id="spnList" style="width:100%;position:absolute;"></span>
<script type="text/javascript">
<!--
	var strLoadID = "";
	var strLastTD = null;
	var intRecCount = 0 
	var intCurNo = -1 
	var intDCount = 0 
	var intUCount = 0 
	var intHeight = 0 ;
	var intDefHeight = 152;
	var intScrollHeight = 19;
	var intLastKeyCode = null;
	
	function MoveCursor(intKeyCode){
		if (parent.window._cboRecfound){
			parent.window._cboOver = true;
			if (strLastTD != null){
				var objLControl = document.getElementById(strLastTD);
				objLControl.className = "cboItems cboItemsDefault";
			}
			
			if (intLastKeyCode != null){
				if (intLastKeyCode != intKeyCode){
					intUCount = intUCount + intScrollHeight;
					intDCount = intDCount + intScrollHeight;
				}
			}
			
			var strTDID = "";
			switch (intKeyCode){
				case 38 :
					intCurNo = Number(intCurNo) - 1; 
					if (intCurNo < 0){
						intCurNo = intRecCount - 1;
						
						intUCount = 0 ;
						window.scrollBy(0,intHeight) ;
					}
					intUCount = intUCount + intScrollHeight;
					if (intUCount > intDefHeight){
						window.scrollBy(0,-intScrollHeight)
					}
					
					break ;
				case 40 :
					intCurNo = Number(intCurNo) + 1;
					if (intCurNo >= intRecCount){
						intCurNo = 0;
						
						intDCount = 0 ;
						window.scrollTo(0,0);
					}
					intDCount = intDCount + intScrollHeight;
					if (intDCount > intDefHeight){
						window.scrollBy(0,intScrollHeight)
					}
					break ;
			}
		
			strTDID = "TD_" + intCurNo;
			strLastTD = strTDID;
			var objControl = document.getElementById(strTDID);
			objControl.className = "cboItems cboItemsSelected";
			//parent.window._cboLastRow = intCurNo;
			parent.window._cboLastRow =  parent.window._cboArrTemp[intCurNo];
			intLastKeyCode = intKeyCode;
		}
	}
	
	function comboItemsOut(objObject){
		lstMouseOut();
	}
	function lstMouseOut(){
		parent.window._cboOver = false;
	}
	
	function comboItemsOver(objObject, strID, intArrIndex){
		/*
		parent.window._cboOver = true;
		if (strLastTD != null){
			var objLControl = document.getElementById(strLastTD);
			objLControl.className = "cboItems cboItemsDefault";
		}
		strLastTD = objObject.id;
		var objControl = document.getElementById(strLastTD);
		objControl.className = "cboItems cboItemsSelected";
		parent.comboTxtFocus(strID, intArrIndex);
		var arrTD = strLastTD.split("_");
		intCurNo = Number(arrTD[1]);
		if (intCurNo > 0){
			intDCount = intCurNo * intScrollHeight;
			intUCount = intCurNo * intScrollHeight;
		}
		parent.window._cboLastRow = intCurNo;
		*/
		
		parent.window._cboOver = true;
		if (strLastTD != null){
			var objLControl = document.getElementById(strLastTD);
			objLControl.className = "cboItems cboItemsDefault";
		}
		strLastTD = objObject.id;
		var objControl = document.getElementById(strLastTD);
		objControl.className = "cboItems cboItemsSelected";
		
		var arrTD = strLastTD.split("_");
		intCurNo = Number(arrTD[1]);
		parent.comboTxtFocus(strID, intArrIndex);
		parent.window._cboLastRow = intCurNo;
	}
	
	function writeList(strHTML, strID, intCount, intHeight){
		intRecCount = intCount;
		intCurNo = -1 ;
		strLoadID = strID;
		strLastTD = null;
		intDCount = 0; 
		intUCount = 0; 
		intDefHeight = Number(intHeight);
		window.scrollTo(0,0);
		//intCurNo = parent.window._cboLastRow
				
		DivWrite("spnList", strHTML);
		var objControl = document.getElementById("spnList")
		if (strHTML == ""){
			parent.comboResize(0, strID);
		}else{
			parent.comboResize(Number(objControl.clientHeight), strID);
			intHeight = Number(objControl.clientHeight);
		}
	}
	function DivWrite(strDivID, strText){
		var objControl ;
		if (document.getElementById)
		{	objControl = document.getElementById(strDivID);
			objControl.innerHTML = "";
			objControl.innerHTML = strText;
		}
		else if (document.all)
		{
			objControl = document.all[strDivID];
			objControl.innerHTML = strText;
		}
		else if (document.layers)
		{
			objControl = document.layers[strDivID];
			objControl.document.open();
			objControl.document.write(strText);
			objControl.document.close();
		}
	}
//-->
</script>
</body>
</html>

