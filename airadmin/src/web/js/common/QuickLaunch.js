	/*
	*********************************************************
		Description		: Quick Launch
		Author			: Rilwan A. Latiff
		Version			: 1.0
		Last Modified	: 20th Jan 2006
	*********************************************************	
	*/
	
	var objQuickLuanch	= null;
	var intTopQuick		= -56 ; 
	var intMoveCounter	= 0;
	var blnQuickLaunch	= false;
	var intSpeed		= 2
	var objQLTimer		= null;

	function ShowQuickLaunch(){
		if (top.strQuickLaunch != ""){
			if (!blnQuickLaunch){
				DivWrite("spnQuickLaunch", top.strQuickLaunch);
				setVisible("spnQuickLaunch", true);
				intTopQuick = -56; 
				intMoveCounter = 0;
				if (objQuickLuanch != null){
					clearInterval(objQuickLuanch);
				}
				objQuickLuanch = setInterval("moveQuickLaunch(0)", intSpeed);
				blnQuickLaunch = true;
			}
		}
	}

	function hideQuickLaunch(){
		if (blnQuickLaunch){
			if (intTopQuick == intSpeed){
				intMoveCounter = 0 ; 
				intTopQuick = -1; 
				blnQuickLaunch = false;
				objQuickLuanch = setInterval("moveQuickLaunch(1)", intSpeed);
			}
		}
	}

	function moveQuickLaunch(strID){
		switch (strID){
			case 0 :
				if (intMoveCounter <= 56){
					var objSpan = getFieldByID("spnQuickLaunch");
					objSpan.style.top = intTopQuick;
					intTopQuick = intTopQuick + intSpeed;
					intMoveCounter = intMoveCounter + intSpeed;
				}else{
					clearTimeout(objQuickLuanch);
					objQLTimer = setInterval("hideQuickLaunch()", 10000);
				}
				break;
			case 1: 
				if (intMoveCounter <= 56){
					var objSpan = getFieldByID("spnQuickLaunch");
					objSpan.style.top = intTopQuick;
					intTopQuick = intTopQuick - intSpeed
					intMoveCounter = intMoveCounter + intSpeed
				}else{
					setVisible("spnQuickLaunch", false);
					clearTimeout(objQuickLuanch);
					clearTimeout(objQLTimer);
				}
				break;
			}
	}
	// --------------------------- End of Quick Laucnch ----------------------