	/*
	*********************************************************
		Description		: Common Routings to get the Control information
		Author			: Rilwan A. Latiff
		Version			: 1.0
		Last Modified	: 1st June 2005
	*********************************************************	
	*/
function replaceall(strValue, strRepValue, strNValue){
	var i = strValue.indexOf(strRepValue);
	while(i > -1){
		strValue = strValue.replace(strRepValue, strNValue);
		i = strValue.indexOf(strRepValue);
	}
	return strValue
}

var url=window.location.href;
if(window.name=='winAdmin'){
	if(url.indexOf('j_security_check')==-1&&url.indexOf('showLogin')==-1&&url.indexOf('showMenu.action')==-1){
		window.location='../../private/master/showMenu.action';
	}
}

var arrParam = new Array("strSearchCriteria","intLastRec","strFlightSearchCriteria","tempSearch", "airportDstCompination", "strGridRow", 
						"strInventoryFlightDataRow", "strInventoryFlightData","arrayData","status","arrayData","ruleStatus","airportDstVal","strGridData","rulemode");


function setModel(model){
    for(var key in model){
		var objControl = getFieldByID(key) ;
		var strType = getFieldType(objControl);
		if(strType=="CHECKBOX" ){
			if((model[key].match('on') == "on") || (model[key].match('true') == "true")) {				
				objControl.checked = true;
			}
			else{
				objControl.checked = false;				
			}
		} else if (strType == "SELECT-MULTIPLE") {
			var intLengrh = objControl.length;
			var arrConValue = model[key].split(",");
			var intArrLength = arrConValue.length;

			if (key == "sel_CodeShare") {
				for (var i = 0; i < arrConValue.length; i++) {
					objControl.options[i] = new Option(arrConValue[i],
							arrConValue[i]);
				}

			} else {

				if (model[key] == "") {
					for (var i = 0; iFlightInstance < intLengrh; i++) {
						objControl.options[i].selected = false;
					}
				}
				for (var x = 0; x < intArrLength; x++) {
					for (var i = 0; i < intLengrh; i++) {
						if (objControl.options[i].value == arrConValue[x]) {
							objControl.options[i].selected = true;
						}
					}
				}
			}
		} else{
		   if(model[key].match('null')){
    		  model[key]="";
		   } else{
			 setField(key,model[key]);
			}
		 }		 
	}  	
	}
	
	// Set the Background Color
	function setBGColor(strControlID, strColor){
		var objControl = getFieldByID(strControlID) ;
		objControl.style.backgroundColor = strColor;
	}	
	
	// Set the Navigate URL for a Anchor Tag
	function setLinkUrl(strControlID, strNavigateURL){
		var objControl = getFieldByID(strControlID) ;
		objControl.href= strNavigateURL;
	}	
	// Set Style Sheet
	function setStyleClass(strControlID, strClassName){
		var objControl = getFieldByID(strControlID) ;
		objControl.className = strClassName
	}
	
	// get Style Sheet
	function getStyleClass(strControlID){
		var objControl = getFieldByID(strControlID) ;
		return objControl.className;
	}
		
	// Write to Divs / span
	function DivWrite(strDivID, strText){
		var objControl ;
		if (document.getElementById)
		{	objControl = document.getElementById(strDivID);
			objControl.innerHTML = "";
			objControl.innerHTML = strText;
		}
		else if (document.all)
		{
			objControl = document.all[strDivID];
			objControl.innerHTML = strText;
		}
		else if (document.layers)
		{
			objControl = document.layers[strDivID];
			objControl.document.open();
			objControl.document.write(strText);
			objControl.document.close();
		}
	}
	
	// Read the div information
	function DivRead(strDivID){
		var objControl ;
		if (document.getElementById)
		{	objControl = document.getElementById(strDivID);
			return objControl.innerHTML
		}
		else if (document.all)
		{
			objControl = document.all[strDivID];
			return objControl.innerHTML;
		}
		else if (document.layers)
		{
			return "";
		}
	}
	
	// Set Image 	
	function setImage(strImageID, strImagePath){
		var objControl = getFieldByID(strImageID);
		objControl.src = strImagePath;
	}
	
	// set Background Image
	function setBGImage(strID, strImagePath){
		var objControl = getFieldByID(strID);
		objControl.style.backgroundImage = "url(" + strImagePath + ")";
	}
	
	// Set Visibility
	function setVisible(strControlID, blnVisible){
		var objControl = getFieldByID(strControlID);
		if (!blnVisible){
			objControl.style.visibility = "hidden";
		}else{
			objControl.style.visibility = "visible";
		}
	}
	
	// Set Display
	function setDisplay(strControlID, blnVisible){
		var objControl = getFieldByID(strControlID);
		if (!blnVisible){
			objControl.style.display = "none";
		}else{
			objControl.style.display = "block";
		}
	}
	
	// Get Display
	function getDisplay(strControlID){
		var objControl = getFieldByID(strControlID);
		if (objControl.style.display == "block"){
			return true;
		}else{
			return false;
		}
	}
	
	// Set Checked
	function setChecked(strControlID, blnStatus){
		var objControl = getFieldByID(strControlID) ;
		var strType = getFieldType(objControl);
		
		switch (strType){
			case "CHECKBOX" : 
				objControl = getFieldByName(strControlID);
				var intLengrh = objControl.length ;
				for (var i = 0 ; i < intLengrh ; i++){
					objControl[i].checked = blnStatus;
				}
				break;
			
			case "RADIO" : 
				objControl = getFieldByName(strControlID);
				var intLengrh = objControl.length ;
				for (var i = 0 ; i < intLengrh ; i++){
					objControl[i].checked = blnStatus;
				}
				break;
		}	
	}
	
	// Get Checked
	function getChecked(strControlID){
		var objControl = getFieldByID(strControlID) ;
		var strType = getFieldType(objControl);
		var blnStatus = false;
		switch (strType){
			case "CHECKBOX" : 
				if (objControl.checked){
					blnStatus = true;
				}
				break;
		}	
		return blnStatus
	}
	
	// Get Visibility
	function getVisible(strControlID){
		var objControl = getFieldByID(strControlID);
		if (objControl.style.visibility == "visible"){
			return true;
		}else{
			return false;
		}
	}

	// Get the Field Type
	function getFieldType(objControl){
	    if(objControl != null && objControl.type != null){
	        return objControl.type.toUpperCase();
	    }
	    return '';
	}
	
	// Get the control as Object
	function getFieldByID(strControlID){
		return document.getElementById(strControlID) ;
	}
	
	function getFieldByName(strControlName){
		return document.getElementsByName(strControlName) ;
	}
	
	
	// Set ReadOnly	
	function ReadOnly(strControlID, blnEnable){
		var objControl = getFieldByID(strControlID) ;
		var strType = getFieldType(objControl);
		
		switch (strType){
			case "TEXT" : objControl.readOnly = blnEnable ; break ;
			case "PASSWORD" : objControl.readOnly = blnEnable ; break ;
			case "TEXTAREA" : objControl.readOnly = blnEnable ; break ;
		}	
	}
	
	// Read Only
	function readOnly(strControlID, blnEnable){
		var objControl = getFieldByID(strControlID) ;
		var strType = getFieldType(objControl);
		
		switch (strType){
			case "TEXT" : objControl.readOnly = blnEnable; break;
			case "PASSWORD" : objControl.readOnly = blnEnable; break;
			case "TEXTAREA" : objControl.readOnly = blnEnable; break;
		}
	}
	
	// Enable / Disable a Conrol
	function Disable(strControlID, blnEnable){
		var objControl = getFieldByID(strControlID) ;
		if (objControl != null) {
		  var strType = getFieldType(objControl);		
		  switch (strType){
			case "CHECKBOX" : 
				objControl = getFieldByName(strControlID);
				var intLengrh = objControl.length ;
				for (var i = 0 ; i < intLengrh ; i++){
					objControl[i].disabled = blnEnable;
				}
				break;
			
			case "RADIO" : 
				objControl = getFieldByName(strControlID);
				var intLengrh = objControl.length ;
				for (var i = 0 ; i < intLengrh ; i++){
					objControl[i].disabled = blnEnable;
				}
				break;
			case "BUTTON":
				if(blnEnable){
					objControl.className = "ButtonDisabled";
				}else{
					objControl.className = "Button";
				}
				objControl.disabled = blnEnable ;
				//alert(objControl.className+","+blnEnable);
				break;
			default :
				objControl.disabled = blnEnable ; break ;
		  }
		}	
	}
	
	// Read Only
	function readOnly(strControlID, blnEnable){
		var objControl = getFieldByID(strControlID) ;
		var strType = getFieldType(objControl);
		
		switch (strType){
			case "TEXT" : objControl.readOnly = blnEnable; break;
			case "PASSWORD" : objControl.readOnly = blnEnable; break;
			case "TEXTAREA" : objControl.readOnly = blnEnable; break;
		}
	}
	
	// Set values to a control 
	function getText(strControlID){
		var objControl = getFieldByID(strControlID) ;
		var strType = getFieldType(objControl);
		var strReturn = "" ;
		
		switch (strType){
			case "TEXT" : strReturn = objControl.value; break;
			case "PASSWORD" : strReturn = objControl.value; break;
			case "HIDDEN" : strReturn = objControl.value; break;
			case "TEXTAREA" : strReturn = objControl.value; break;
			case "CHECKBOX" : 
				objControl = getFieldByName(strControlID);
				var intLengrh = objControl.length ;
				for (var i = 0 ; i < intLengrh ; i++){
					if (objControl[i].checked){
						if (strReturn != ""){strReturn += ","; }
						strReturn += objControl[i].value;
					}
				}				
				break;
			
			case "RADIO" : 
				objControl = getFieldByName(strControlID);
				var intLengrh = objControl.length ;
				for (var i = 0 ; i < intLengrh ; i++){
					if (objControl[i].checked){
						if (strReturn != ""){strReturn += ","; }
						strReturn += objControl[i].value;
					}
				}				
				break;
				
			case "SELECT-ONE" :
				strReturn = objControl.options[objControl.selectedIndex].text;
				break;
					
			case "SELECT-MULTIPLE" :
				var intLengrh = objControl.length ;
				for (var i = 0 ; i < intLengrh ; i++){
					if (objControl.options[i].selected){
						if (strReturn != ""){strReturn += ","; }
						strReturn += objControl.options[i].text;
					}
				}				
				break;
		}
		return strReturn;
	}
	
	// Set values to a control 
	function getValue(strControlID){
		var objControl = getFieldByID(strControlID) ;
		var strType = getFieldType(objControl);
		var strReturn = "" ;
		
		switch (strType){
			case "TEXT" : strReturn = objControl.value; break;
			case "PASSWORD" : strReturn = objControl.value; break;
			case "HIDDEN" : strReturn = objControl.value; break;
			case "TEXTAREA" : strReturn = objControl.value; break;
			case "CHECKBOX" : 
				objControl = getFieldByName(strControlID);
				var intLength = objControl.length ;
				if (intLength > 0){
					for (var i = 0 ; i < intLength ; i++){
						if (strReturn != ""){strReturn += ","; }
						strReturn += objControl[i].checked;
					}				
				}
				break;
			
			case "RADIO" : 
				objControl = getFieldByName(strControlID);
				var intLength = objControl.length ;
				for (var i = 0 ; i < intLength ; i++){
					if (objControl[i].checked){
						if (strReturn != ""){strReturn += ","; }
						strReturn += objControl[i].value;
					}
				}				
				break;
				
			case "SELECT-ONE" :
				strReturn = objControl.value ;
				break;
					
			case "SELECT-MULTIPLE" :
				var intLengrh = objControl.length ;
				for (var i = 0 ; i < intLengrh ; i++){
					if (objControl.options[i].selected){
						if (strReturn != ""){strReturn += ","; }
						strReturn += objControl.options[i].value;
					}
				}				
				break;
		}
		return strReturn;
	}
	
	// Set values to a control 
	function setField(strControlID, strControlValue){
		var objControl = getFieldByID(strControlID) ;
		var strType = getFieldType(objControl);
		switch (strType){
			case "TEXT" : objControl.value = strControlValue ; break ;
			case "PASSWORD" : objControl.value = strControlValue ; break ;
			case "HIDDEN" : objControl.value = strControlValue ; break ;
			case "TEXTAREA" : objControl.value = strControlValue ; break ;
			case "CHECKBOX" : 
				objControl = getFieldByName(strControlID);
				var intLength = objControl.length ;
				if (strControlValue != true && strControlValue != false){
					var arrConValue = strControlValue.split(",");
					var intArrLength = arrConValue.length ;
					if (strControlValue == ""){
						for (var i = 0 ; i < intLength ; i++){
							objControl[i].checked = false;
						}				
					}
					
					for (var x = 0; x < intArrLength ; x++){
						for (var i = 0 ; i < intLength ; i++){
							if (objControl[i].value == arrConValue[x]){
								if (!objControl[i].disabled){
									objControl[i].checked = true;
								}
								break;
							}
						}
					}
				}else{
					for (var i = 0 ; i < intLength ; i++){
						if (!objControl[i].disabled){
							objControl[i].checked = strControlValue;
						}
						
					}
				}
				break;
			
			case "RADIO" : 
				objControl = getFieldByName(strControlID);
				var intLengrh = objControl.length ;
				if (strControlValue == ""){
					for (var i = 0 ; i < intLengrh ; i++){
						objControl[i].checked = false;
					}				
				}
				for (var i = 0 ; i < intLengrh ; i++){
					if (objControl[i].value == strControlValue){
						objControl[i].checked = true;
						break;
					}
				}				
				break;
				
			case "SELECT-ONE" :
				var intLengrh = objControl.length ;
				for (var i = 0 ; i < intLengrh ; i++){
					if (objControl.options[i].value == strControlValue){
						objControl.options[i].selected = true;
						break;
					}
				}
				break;
					


			case "SELECT-MULTIPLE":
				var intLengrh = objControl.length;
				var arrConValue = strControlValue.split(",");
				var intArrLength = arrConValue.length;
		
				if (strControlID == "sel_CodeShare") {
					for (var i = 0; i < arrConValue.length; i++) {
						objControl.options[i] = new Option(arrConValue[i],arrConValue[i]);
					}
		
				} else {
		
					if (strControlValue == "") {
						for (var i = 0; i < intLengrh; i++) {
							objControl.options[i].selected = false;
						}
					}
					for (var x = 0; x < intArrLength; x++) {
						for (var i = 0; i < intLengrh; i++) {
							if (objControl.options[i].value == arrConValue[x]) {
								objControl.options[i].selected = true;
								break;
							}
						}
					}
					break;
				}
		}
	}
	
	// Set the Maxlength
	function MaxLength(objControl, objEvent, intLen){
		if ((objEvent.keyCode == 9) || (objEvent.keyCode == 46) || (objEvent.keyCode == 8) || (objEvent.keyCode == 37) || (objEvent.keyCode == 38) || (objEvent.keyCode == 39) || (objEvent.keyCode == 40)){
			return true
		}else{
			if (objControl.value.length >= intLen){
				var strValue = objControl.value
				objControl.value = strValue.substr(0, intLen);
				return false;
			}
		}
		return true;
	}
	
	// Number convert to Arabic
	function numberConvertToArabic(intNumber){
		var arrNumbers = new Array("&#1632;","&#1633;","&#1634;","&#1635;","&#1636;","&#1637;","&#1638;","&#1639;","&#1640;","&#1641;")
		intNumber = String(intNumber);
		var intLength = intNumber.length;
		var strReturn = "" ;
		for (var i = 0 ; i < intLength ; i++){
			if (intNumber.substr(i,1) != " "){
				if (!isNaN(intNumber.substr(i,1))){
					strReturn += arrNumbers[intNumber.substr(i,1)];
				}else{
					strReturn += intNumber.substr(i,1);
				}
			}else{
				strReturn += intNumber.substr(i,1);
			}
		}
		
		return strReturn;
	}
	
	// Currency Format
	function CurrencyFormat(num, decimals){
		/*
		if ((String(amount) == "") || (amount == ".00")){amount = 0;}
		amount = addDecimals(amount, decimals);
		var Num = amount.toString().replace(/\$|\,/g,'');
		var dec = Num.indexOf(".");
		var intCents = ((dec > -1) ? "" + Num.substring(dec,Num.length) : ".00");
		Num = "" + parseInt(Num);
		var intValue = "";
		var intReturnValue = "";
		if (intCents.length == 2) intCents += "0";
		if (intCents.length == 1) intCents += "00";
		if (intCents == "") intCents += ".00";
		var count = 0;
		for (var k = Num.length-1; k >= 0; k--) {
			var oneChar = Num.charAt(k);
			if (count == 3){
				intValue += ",";
				intValue += oneChar;
				count = 1;
				continue;
			}else {
				intValue += oneChar;
				count ++;
			}
		}
		for (var k = intValue.length-1; k >= 0; k--) {
			var oneChar = intValue.charAt(k);
			intReturnValue += oneChar;
		}
		intReturnValue = intReturnValue + intCents;
		if (intReturnValue.indexOf("-,") != -1)
		{
			intReturnValue = intReturnValue.replace("-,","-");
		}
		*/
		
		num = num.toString().replace(/\$|\,/g,'');
		if(isNaN(num))
		num = "0";
		sign = (num == (num = Math.abs(num)));
		num = Math.floor(num*100+0.50000000001);
		cents = num%100;
		num = String(Math.floor(num/100));
		if(cents<10)
		cents = "0" + cents;
		for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
		num = num.substring(0,num.length-(4*i+3))+','+num.substring(num.length-(4*i+3));
		return (((sign)?'':'-') + num + '.' + cents);
	}

	// Add Decimals
	function addDecimals(strValue, intDecimals){
		var strReturn = "";
		var intDevide = "1"
		for (var i = 0 ; i < Number(intDecimals) ; i++){
			intDevide += "0";
		}
		strValue = String(Math.round(Number(strValue) * Number(intDevide))/Number(intDevide))
		var intIndex = strValue.indexOf(".")
		var strDecimals = "";
		if (intIndex !=  -1){
			var arrData = strValue.split(".");
			if (arrData.length > 1){
				strDecimals = arrData[1]
				for (var i = strDecimals.length ; i < Number(intDecimals) ; i++){
					strDecimals += "0" ;
				}	
				strDecimals = strDecimals.substr(0, intDecimals);
			}else{
				for (var i = 0 ; i < Number(intDecimals) ; i++){
					strDecimals += "0" ;
				}	
			}
			strReturn = arrData[0] + "." + strDecimals ;		
		}else{
			for (var i = 0 ; i < Number(intDecimals) ; i++){
				strDecimals += "0" ;
			}	
			strReturn = strValue + "." + strDecimals ;
		}
		return strReturn;
	}
	
	
	function roundNumber(intNumber, intDecimals) {
    	intNumber += '';	//CONVERT NUMBER TO STRING
    	var arNUMS = intNumber.split('.');		//GET DECIMAL NUMBERS
    	//IF THERE IS NO DECIMAL, THEN return THE NUMBER


       	if( arNUMS.length < 2 ) {
       		return intNumber;
       	}
       	var intPower = Math.pow(10, arNUMS[1].length);
       	var intRound = intNumber;

        for( var i = 0; i <= arNUMS[1].length - intDecimals; i++ ) {
        	intRound = (Math.round((intRound * intPower)) / intPower);
        	intPower = intPower / 10;
        }
        return intRound;
	}
	
	// Generate the Error
	function raiseError(strErrNo){
		var strMsg = arrError[strErrNo];
		if (arguments.length >1){
			for (var i = 0 ; i < arguments.length - 1 ; i++){
				strMsg = strMsg.replace("#" + (i+1), arguments[i+1]);
			}
		}
		return strMsg;
	}	
	
	function buildError(strMessage){
		if (arguments.length >1){
			for (var i = 0 ; i < arguments.length - 1 ; i++){
				strMessage = strMessage.replace("#" + (i+1), arguments[i+1]);
			}
			strMessage = strMessage
		}
		return strMessage;
	}

	// Check Invalid Characters	
	function checkInvalidChar(strValue, strMessage, strControlText){
		var strChkEmpty = FindChar(strValue)
		if (strChkEmpty != "0"){
			return buildError(strMessage, strChkEmpty[0], strControlText);
		}else{
			return "";
		}
	}
	
	// Find invalid characters
	function FindChar(StringIn){
		// ------------- Check the standards characters
		/*
		\b 	Backspace
		\f 	Form feed
		\n 	New line
		\r 	Carriage return
		\t 	tab
		\" 	quotation mark
		\'  MS Office single quote 6
		\'  Ms Office singel Quote 9
		\\  Back Slash
		*/

		var CharInArray = new Array("'","<",">","^",'"',"~", "\n");
		var CharOutArray=new Array();
		for (var i=0;i<StringIn.length;i++){
			switch (StringIn.charCodeAt(i)){
				case 92 :
					CharOutArray[0]="\\ "
					CharOutArray[1]=eval(i+1);
					return (CharOutArray)
					break;
				case 8216 :
				case 8217 :
					CharOutArray[0]="' "
					CharOutArray[1]=eval(i+1);
					return (CharOutArray)
					break;
				default :
					for (var j=0;j<CharInArray.length;j++){
						if (StringIn.charAt(i)==CharInArray[j]){
							CharOutArray[0]=CharInArray[j]
							CharOutArray[1]=eval(i+1);
							return (CharOutArray);
						}
					}
					break;
			}
		}
		return "0";
	}
	
	// context
	function disablePage(){
		return true;
	}
	
	// Right Trim
	function rightTrim(strValue){
		var objRegExp = /^([\w\W]*)(\b\s*)$/;
	
	      if(objRegExp.test(strValue)) {
	       //remove trailing a whitespace characters
	       strValue = strValue.replace(objRegExp, '$1');
	    }
	  	return strValue;
	}
	
	// left trim
	function leftTrim(strValue){
		var objRegExp = /^(\s*)(\b[\w\W]*)$/;
	
	  	if(objRegExp.test(strValue)) {
	    	//remove leading a whitespace characters
	       strValue = strValue.replace(objRegExp, '$2');
	    }
	  	return strValue;
	}
	
	// trim
	function trim(strValue){
		var objRegExp = /^(\s*)$/;
	
	    //check for all spaces
	    if(objRegExp.test(strValue)) {
	       strValue = strValue.replace(objRegExp, '');
	       if( strValue.length == 0)
	          return strValue;
	    }
	
	   //check for leading & trailing spaces
	   objRegExp = /^(\s*)([\W\w]*)(\b\s*$)/;
	   if(objRegExp.test(strValue)) {
	       //remove leading and trailing whitespace characters
	       strValue = strValue.replace(objRegExp, '$2');
	    }
	
	  	return strValue;
	}
	
	// Replace all 
	function replaceall(strValue, strRepValue, strNValue){
		var i = strValue.indexOf(strRepValue);
		while(i > -1){
			strValue = strValue.replace(strRepValue, strNValue);
			i = strValue.indexOf(strRepValue);
		}
		return strValue
	}
	
	// clear cache
	function ClearCache(){
		try{
			window.clipboardData.clearData()
		}catch(e){}
	}
	
	// close child windows
	function CloseChildWindow(){
		try{
			if ((top.objCW) && (!top.objCW.closed)){
				top.objCW.close();
			}
		}catch (ex){}
	}
	
	// conver the text to Title Case
	function converToTitleCase(strValue){
		var arrValues = strValue.split(" ");
		strValue = "";
		var strFC = ""
		var strOC = ""
		for (var i = 0; i < arrValues.length ; i++){
			strOC = "";
			if (strValue != ""){strValue += " ";}
			arrValues[i] = trim(arrValues[i]);
			arrValues[i] = arrValues[i].toLowerCase();
			strFC = arrValues[i].substr(0,1).toUpperCase();
			if (arrValues[i].length > 1){
				strOC = arrValues[i].substr(1,arrValues[i].length - 1).toLowerCase()
			}
			strValue += strFC + strOC;
		}
		return strValue
	}

	// Copy array to a another array
	function arrayClone(arrSource, arrTarget){
		arrTarget.length = 0 ; 
		for (var i = 0 ; i < arrSource.length ; i++){
			arrTarget[i] = new Array();
			for (var m = 0 ; m < arrSource[i].length ; m++){
				arrTarget[i][m] = arrSource[i][m];
			}
		}
	}
	
	// copy key array to a another array
	function keyArrayClone(objArray, objTarget){
		objTarget = new Array();
		for(var key in objArray){
			if(objArray[key] != null){
				objTarget[key] = objArray[key];
			}
		}
		return objTarget; 
	}
	
	// focus to a control
	function setFocus(id){
		var objControl = getFieldByName(id);
		var intLength = objControl.length ;
		if (intLength > 1){
			objControl[0].focus();
		}else{
			document.getElementById(id).focus();
		}
	}
	
	// Ajax 
	// ---------------- Remote scripting
	var reqObj;
  	function retrieveURL(url) {
		if (window.XMLHttpRequest) { // Non-IE browsers
      		reqObj = new XMLHttpRequest();
      		reqObj.onreadystatechange = processStateChange;
      		try {
        		reqObj.open("GET", url, true);
      		}catch (e) {
        		showERRMessage(e);
      		}
      		reqObj.send(null);
    	}else if (window.ActiveXObject) { // IE
      		reqObj = new ActiveXObject("Microsoft.XMLHTTP");
      		if (reqObj) {
        		reqObj.onreadystatechange = processStateChange;
        		reqObj.open("GET", url, true);
        		reqObj.send();
      		}
    	}
  	}
  	
	var httpReq=false;
	function makePOST(url,params) {
		httpReq=false;
	    if(window.XMLHttpRequest){ // Mozilla, Safari,...
			httpReq=new XMLHttpRequest();
	    }else if(window.ActiveXObject){ // IE
			try{
	              httpReq=new ActiveXObject("Microsoft.XMLHTTP");
	        } catch (e) {}
	    }
	
	    if (!httpReq) {
	        alert('Cannot create XMLHTTP instance');
	        return false;
	    }
	     
	    httpReq.onreadystatechange=processStateChange;
	    httpReq.open('POST', url, true);
	    httpReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	    httpReq.setRequestHeader("Content-length",params.length);
	    httpReq.setRequestHeader("Connection","close");
	    httpReq.send(params);
	    
	    try{
		    if (top.strTopPage){
			    ResetTimeOut();
		   	}
		}catch(e){}
	}
	
	
	
	/*
	 * Check Key Array is available
	 */
	 function checkKeyArrayAvailable(objArray){
		var blnReturn = false;
		for(var key in objArray){
			blnReturn = true;
		}
		return blnReturn;
	}
	
	function getArrayCodes(objArray, intIndex,  strID, intElement){
		var strReturn = "";
		for (var i = 0 ; i < objArray.length ; i++){
			if (objArray[i][intIndex] == strID){
				strReturn = objArray[i][intElement];
				break;
			}
		}
		return strReturn;
	}
	
	// get The Disabled Status
	function getDisabled(strID){
		return getFieldByID(strID).disabled;
	}
	
	function getMouseX(objEvent){
		var x = 0;
		if (browser.isIE) {
			x = window.event.clientX + document.documentElement.scrollLeft + document.body.scrollLeft;
		}else{
			if (navigator.userAgent.indexOf("Safari") != -1){
				x = objEvent.clientX;
			}else if (navigator.userAgent.indexOf("Konqueror") != -1){
				x = window.event.clientX + document.documentElement.scrollLeft + document.body.scrollLeft;
			}else if (navigator.userAgent.indexOf("Opera") != -1){
				x = objEvent.clientX;
			}else{
				x = objEvent.clientX + window.scrollX;
			}
		}	
		return x;
	}
	
	function getMouseY(objEvent){
		var y = 0;
		if (browser.isIE) {
			y = window.event.clientY + document.documentElement.scrollTop + document.body.scrollTop;
		}else{
			if (navigator.userAgent.indexOf("Safari") != -1){
				y = objEvent.clientY;
			}else if (navigator.userAgent.indexOf("Konqueror") != -1){
				y = window.event.clientY + document.documentElement.scrollTop + document.body.scrollTop;
			}else if (navigator.userAgent.indexOf("Opera") != -1){
				y = objEvent.clientY + document.documentElement.scrollTop;
			}else{
				y = objEvent.clientY + window.scrollY;
			}
		}	
		return y;
	}
	
	/*
	 * Select All in the Text
	 */
	function selectAll(strID){
		getFieldByID(strID).select()
	}
	
	/*
	 * Window Screen Height
	 */
	function getPageHeight(){
		var y;
		if (self.innerHeight){
			// all except Explorer{
			y = self.innerHeight;
		}else if (document.documentElement && document.documentElement.clientHeight){
			// Explorer 6 Strict
			y = document.documentElement.clientHeight;
		}else if (document.body){
		 	// other Explorers
			y = document.body.clientHeight;
		}
		return y;
	}
	
	/*
	 * Window Screen Width
	 */
	function getPageWidth(){
		var x
		if (self.innerHeight){
		 	// all except Explorer{
			x = self.innerWidth;
		}else if (document.documentElement && document.documentElement.clientWidth){
			// Explorer 6 Strict
			x = document.documentElement.clientWidth;
		}else if (document.body){
			// other Explorers{
			x = document.body.clientWidth;
		}
		return x;
	}
	
	/*
	 * set Z-index 
	 */
	function setZIndex(strControlID, intIndex){
		var objControl = getFieldByID(strControlID) ;
		objControl.style.zIndex = intIndex;
	}
	
	// Set the Cursor
	function setCursor(strControlID, strCursor){
		var objControl = getFieldByID(strControlID) ;
		objControl.style.cursor = strCursor;
	}
	
	/*
	 * Mouse Button Click
	 */
	function getMouseButtonClick(objEvent){
		var intBtn = null;
		if (browser.isIE){
			objEvent = window.event;
		}
		intBtn = objEvent.button;
		return intBtn;
	}
	/* --------------------------------------------------------- end of Page --------------------------------------------------------- */	
	
	//------------------------------ Setting and retrieving Top page values
		function setTabValues(screenId, seperator, parameter, value){
			//Creates and sets parameter values for a screen
			//Supported parameters given in the arrParam array
			//alert("1");
			
			var strScreenVariables = "";
			var paramPos = -1;
			//Get position of parameter
			for (var i=0; i < arrParam.length; i++){
				if (parameter.toUpperCase() == arrParam[i].toUpperCase()) {
					paramPos = i;
					break;
				}
			}
			if (paramPos >= 0 ) {
				//Correct paramter
				strScreenVariables = top[1].objTMenu.tabGetValue(screenId);
				//strScreenVariables = testString; //Testing porposes
				arrScreenVariables = strScreenVariables.split(seperator);
				
				arrScreenVariables[paramPos] = value;
				strScreenVariables = "";

				
				//Make parameter value string
				for (var i=0; i < arrScreenVariables.length && i < arrParam.length ; i++) {
					if (arrScreenVariables[i] == null) {
						arrScreenVariables[i] = "";
					}
					strScreenVariables += arrScreenVariables[i] + seperator;
				}
				//Save parameter value string
				top[1].objTMenu.tabSetValue(screenId,strScreenVariables);
				//testString = strScreenVariables; //Testing porposes
				//alert ("Saved String: " + testString);

				return true;
			} else {
				return false;
			}
		}

		function getTabValues(screenId, seperator, parameter){
						//alert("2");
			//Get saved value of a parameter			
			//Supported parameters given in the arrParam array
			
			var strScreenVariables = "";
			var paramPos = -1;
			//Get position of parameter
			for (var i=0; i < arrParam.length; i++){
				if (parameter.toUpperCase() == arrParam[i].toUpperCase()) {
					paramPos = i;
					break;
				}
			}
			if (paramPos >= 0 ) {
				//Correct paramter
				strScreenVariables = top[1].objTMenu.tabGetValue(screenId);
				//strScreenVariables = testString; //Testing porposes
				arrScreenVariables = strScreenVariables.split(seperator);
				if (paramPos < arrScreenVariables.length) {
					return arrScreenVariables[paramPos];
				} else {
					return "";
				}
			} else {
				return "";
			}
		}
	
	//Sort an array
	function sort(arr, compPos){
		//Insertion Sort
		//For normal sort "compPos" should be -1, otherwise the sort position the internal array should be given.
		//Ex. Consider following array format.
		//		ParentArrayElement_1 -> InternalArrPos_1, InternalArrPos_2, InternalArrPos_3
		//		ParentArrayElement_2 -> InternalArrPos_1, InternalArrPos_2
		//		ParentArrayElement_2 -> InternalArrPos_1, InternalArrPos_2, InternalArrPos_3, InternalArrPos_4
		//
		//    In this case if we call as sort(ParentArray, 1), it will sort the ParentArray according to content 
		//    of "InternalArrPos_2" possition of the internal array.
		var temp;
		var i;
		var j;
		for (i=1; i<arr.length; i++)
		{
			temp = arr[i];
			if (compPos >=0)
			{
				//Need to sort depend on an internal array content
				for (j=i; (j>0) && (temp[compPos] < arr[j-1][compPos]); j--)
				{
					arr[j]=arr[j-1];
				}
			} else {
				//Normal sort
				for (j=i; (j>0) && (temp < arr[j-1]); j--)
				{
					arr[j]=arr[j-1];
				}
			}
			
			arr[j]=temp;
		}
		return arr;
	}
	
	/*----------
	Button focus is set depend on the button visibility sequence.
	This is useful when buttons are in-visible when limited privileges.
	
	strBtnName ? Id of the button that needs to set focus
	strAlternativeBtnSequence ? This is used to replace the default alternative button sequence.
	*/
	function buttonSetFocus(strBtnName, strAlternativeBtnSequence){
		try{
			getFieldByID(strBtnName).focus();
		}
		catch (ex)
		{
			if (strAlternativeBtnSequence==null)
			{
				strAlternativeBtnSequence = "btnAdd,btnEdit,btnDelete";
			}
			//alert("Button Array : " + strAlternativeBtnSequence);
			var arrBtns = strAlternativeBtnSequence.split(",");
			for (var i=0; i<arrBtns.length; i++)
			{
				try{
					getFieldByID(arrBtns[i]).focus();
				}
				catch (ex)
				{}
			}
		}
	}
	
var httpReq=false;
function makePOST(url,params) {
	httpReq=false;
    if(window.XMLHttpRequest){ // Mozilla, Safari,...
		httpReq=new XMLHttpRequest();
    }else if(window.ActiveXObject){ // IE
		try{
              httpReq=new ActiveXObject("Microsoft.XMLHTTP");
        } catch (e) {}
    }

    if (!httpReq) {
        alert('Cannot create XMLHTTP instance');
        return false;
    }
     
    httpReq.onreadystatechange=processStateChange;
    httpReq.open('POST', url, true);
    httpReq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
//    httpReq.setRequestHeader("Content-length",params.length);
//    httpReq.setRequestHeader("Connection","close");
    httpReq.send(params);
}

function showContextMenu(){
	return false;
}
//funtion to replace enter character
function replaceEnter(inputValue) {
	
	var output = "";
	for (var i = 0; i < inputValue.length; i++) {
		if (inputValue.charCodeAt(i) == 13) {
			output += " ";				
		}else if (inputValue.charCodeAt(i) == 10) {	
			output +=  " ";				
		} else {
			output += inputValue.charAt(i);
   		}
	}	
	return output;	
}

//function to search an array for the existence of a value
function isValueExist(arr,value){
	var x;
	for (x in arr){
		if(value==arr[x]){
			return true;
		}
	}
	return false;
}

//function to clone
function arrayClone(arrSource, arrTarget){
	arrTarget.length = 0 ; 
	for (var i = 0 ; i < arrSource.length ; i++){
		arrTarget[i] = new Array();
		for (var m = 0 ; m < arrSource[i].length ; m++){
			arrTarget[i][m] = arrSource[i][m];
		}
	}
}
//Haider
function getListLabel(listId, value){
	obj = getFieldByID(listId);
	var i=0;
	for(i=0;i<obj.options.length;i++){
		if(obj.options[i].value == value){
			return  obj.options[i].text;
		}
	}
	return null;
}
function addToList(objId, label,value){
	var obj = getFieldByID(objId);
	var option = new Option(label, value);
	try{
		obj.add(option,null);
	}catch(ex){
		obj.add(option);
	}
}
function getUnicode(str){
	var uc = "";
	var i=0;
	var c;
	for(i=0;i<str.length;i++){
		c = str.charCodeAt(i);
		var uch = c.toString(16);
		if(uch.length == 1)
			uc+="000"+uch;
		else if(uch.length == 2)
			uc += "00"+uch;
		else if(uch.length == 3)	
			uc += "0"+uch;
		else if(uch.length == 4)
			uc += uch;
	}
	return uc;	
}
function decodeFromHex(str){
    var r="";  
    for (var i = 0 ; i < str.length ;i=i+4 )
	{
		var subStr = str.substring(i,i+4);
		r = r + String.fromCharCode("0x"+ subStr );		
	}
    return r;
}


function findValueInList(value, listId){
	var list = getFieldByID(listId);
	var option = list.options;
	var i=0;
	for(i=0;i<list.length;i++){
		if(value == option[i].value)
			return true;
	}
		return false;
}

function getCurSelectedLabel(fieldId){
	if(getFieldByID(fieldId).selectedIndex <0){
		return null;
	}else{
		return getFieldByID(fieldId).options[getFieldByID(fieldId).selectedIndex].text;
	}
}

/** 
 * Validate Inputs onKeyPress
 * 
 */
function KeyPress(objC, objValType){
	var blnReturn = true;
	switch (objValType){
		case "ANW" : blnReturn = isAlphaNumericWhiteSpace(objC.value); break;
		case "AN" : blnReturn = isAlphaNumeric(objC.value); break;		
		case "A" : blnReturn = isAlpha(objC.value); break;				
		case "N" : blnReturn = validateInteger(objC.value); break;		
	}
	if (!blnReturn){
		objC.value = objC.value.substr(0,objC.value.length -1); 
		objC.focus();
	}
}

function compareTime(startTime, endTime)
{	
	var Todaydate = new Date();

	var StartTime = new Date ( Todaydate.getFullYear(), Todaydate.getMonth(), Todaydate.getDate(), startTime.split(':')[0], startTime.split(':')[1], 0 );
	var EndTime = new Date ( Todaydate.getFullYear(), Todaydate.getMonth(), Todaydate.getDate(), endTime.split(':')[0], endTime.split(':')[1], 0 );

	return EndTime.getTime() - StartTime.getTime();
}