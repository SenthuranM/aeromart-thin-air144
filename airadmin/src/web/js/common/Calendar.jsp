<%@ page language='java' contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<html>
	<head>
		<title>Calendar</title>
			<LINK rel="stylesheet" type="text/css" href="../../css/CalendarStyle_no_cache.css">
			<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
			<meta name="ProgId" content="VisualStudio.HTML">
			<meta name="Originator" content="Microsoft Visual Studio .NET 7.1">
			<meta http-equiv="pragma" content="no-cache">
			<meta http-equiv="cache-control" content="no-cache">
			<meta http-equiv="expires" content="-1">
			<script src="../../js/common/Common.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
			<script src="../../js/common/Browser.js?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript" ></script>
			<script src="../../js/common/SUPPRESSMOUSE.JS?<c:out value='${applicationScope.appAccelAeroRelVersion}' escapeXml='false' />"  type="text/javascript"></script>
			<script  type="text/javascript">
<!--
	/*
		Page Description : Calendar Control 
	*/
	var strinDate = ""; //opener.dateField;
	var isCalStartThisYear = false;
	//	if(top.carrier == "RAK Airways"){  // temporary fix
	//		isCalStartThisYear = true;
	//	}
	var arrMonth = ["January","February","March","April","May","June","July","August","September","October","November","December"];
	var arrDays = new Array(42);
	var now		= new Date();
	var day		= now.getDate();
	var month	= now.getMonth();
	var year	= now.getFullYear();
	// -----------------------------------------------
	var intSYear= year			// Years starting from
	var intNYear= 2				// Display no of years in the drop down 
	var intInDD = "";
	var intInMM = "";
	var intInYY = ""; 
	var objParent = "";
	var blnFirst = "";
	var intDisableUpto = "";
	
	function selMonth_onChange(){
		var objselMonth = document.getElementById("selMonth");
		var strSelMonth = objselMonth.options[objselMonth.selectedIndex].value
		
		strSelMonth = Number(strSelMonth) - 1;
		writeDiv("<b><font class='DefMonth'>" + arrMonth[strSelMonth] + "<\/font><\/b>", "divMonth");
		
		var objselYear = document.getElementById("selYear");
		var strSelYear = objselYear.options[objselYear.selectedIndex].value
		
		BuildCal(strSelMonth, strSelYear);
	}
	
	function SetDate(strDD, strMM, strYY, strYears, strObject, strDisableUpto){
		blnFirst = true;
		intSYear= year ;	
		objParent = strObject;
		strinDate = strDD + "/" + strMM + "/" + strYY + "/" + strYears;
		
		if (strDisableUpto != "" && strDisableUpto !=  null){
			var arrDUpto = strDisableUpto.split("/");
			if (Number(arrDUpto[0]) < 10){arrDUpto[0] = "0" + arrDUpto[0]}
			arrDUpto[1] = Number(arrDUpto[1]) - 1;
			if (Number(arrDUpto[1]) < 10){arrDUpto[1] = "0" + arrDUpto[1]}
			intDisableUpto = arrDUpto[2] + arrDUpto[1] + arrDUpto[0];
		}
		
		SetCurrDate();
	}
	
	function SetCurrDate(){
		var objselYear = document.getElementById("selYear");
		if (strinDate != ""){
			var arrInDate = strinDate.split("/");
			intInDD = Number(arrInDate[0]);
			intInMM = Number(arrInDate[1]) - 1;
			intInYY = Number(arrInDate[2]);
			if (arrInDate.length > 3){
			if(isCalStartThisYear){
				intNYear = Number(arrInDate[3]);
				intSYear = intSYear; // Change here to make the current year the starting year
				}else{
					intNYear = Number(arrInDate[3])* 2;
					intSYear = intSYear - Number(arrInDate[3]);
				}
			}
		}
		if (blnFirst){
			for (var i = 1; i <= intNYear; i ++){
				objselYear.length = i;
				objselYear.options[i-1].text =  intSYear;
				objselYear.options[i-1].value = intSYear;
				intSYear++;
			}
		}
		blnFirst = false;

		var objselMonth = document.getElementById("selMonth");
		objselMonth.options[month].selected = true;
		
		writeDiv("<b><font class='DefMonth'>" + arrMonth[month] + "<\/font><\/b>", "divMonth");
		writeDiv("<a href='javascript:void(0);' onclick='CDateClick(); return false;' title='Today'><b><font class='ToDate'>" + day + " " + arrMonth[month] + " " + year + "<\/font><\/b><\/a>", "divCD")
		
		BuildCal(month, year);
		if (strinDate != ""){
			objselMonth.options[intInMM].selected = true;
			for (var i = 0; i < objselYear.length; i++){
				if (objselYear.options[i].value == intInYY){
					objselYear.options[i].selected = true;
					break;
				}
			}
			selMonth_onChange();
		}
		
	}
	
	function BuildCal(strMonth, strYear){
	
		// Array Initialize 
		for (var i = 0; i <= 42; i++){
			arrDays[i] = "";
		}
		
		var intDays        = getDaysInMonth(strMonth + 1, strYear);
		var dtfirstDay     = new Date (strYear, strMonth, 1);
		var intArrStartPos = dtfirstDay.getDay();
		
		// Get preivous month Days 
		var intPMonth	= strMonth;
		var intPYear	= strYear;
		if (intPMonth <= 0){
			intPMonth = 12
			intPYear  = Number(intPYear) - 1
		}
		var intPDays    = getDaysInMonth(intPMonth, intPYear);
		var intPSDate   = (intPDays - intArrStartPos + 1)
		
		var intDCount = 1 
		var intNDCount = 1 
		for (var i = 0; i <= 42; i++){
			if (i >= intArrStartPos){
				if (intDCount <= intDays){
					arrDays[i] = intDCount; 
					intDCount++;
				}else{
					arrDays[i] = intNDCount; 
					intNDCount++;
				}
			}else{
				arrDays[i] = intPSDate
				intPSDate++;
			}
		}
		
		var intTDWidth = 36
		var strHTMLText  = "<table width='" + (intTDWidth * 7) + " cellpadding='0' cellspacing='1' border='1' class='BGColor' align='center'>"
		strHTMLText		+= "	<tr class='TblLineColor'>"
		strHTMLText		+= "		<td width='" + intTDWidth + "' align='center' class='SUHDBGColor'><font class='SUHDColor'><b>Su<\/b><\/font><\/td>"
		strHTMLText		+= "		<td width='" + intTDWidth + "' align='center' class='MOHDBGColor'><font class='MOHDColor'><b>Mo<\/b><\/font><\/td>"
		strHTMLText		+= "		<td width='" + intTDWidth + "' align='center' class='TUHDBGColor'><font class='TUHDColor'><b>Tu<\/b><\/font><\/td>"
		strHTMLText		+= "		<td width='" + intTDWidth + "' align='center' class='WEHDBGColor'><font class='WEHDColor'><b>We<\/b><\/font><\/td>"
		strHTMLText		+= "		<td width='" + intTDWidth + "' align='center' class='THHDBGColor'><font class='THHDColor'><b>Th<\/b><\/font><\/td>"
		strHTMLText		+= "		<td width='" + intTDWidth + "' align='center' class='FRHDBGColor'><font class='FRHDColor'><b>Fr<\/b><\/font><\/td>"
		strHTMLText		+= "		<td width='" + intTDWidth + "' align='center' class='SAHDBGColor'><font class='SAHDColor'><b>Sa<\/b><\/font><\/td>"
		strHTMLText		+= "	<\/tr>"
		
		var intCount = 0 
		var strLinkTag = "";
		var intDCount = 1 
		
		for (var i = 0; i < 6; i++){
			strHTMLText		+= "	<tr class='TblLineColor'>"
			for (var x = 0; x < 7; x++){
				strHTMLText		+= " <td width='" + intTDWidth + "' align='center' " 
				if (intCount >= intArrStartPos){
					if (intDCount <= intDays){
						strLinkTag = "<a href='javascript:void(0)' onclick='Day_onClick(" + arrDays[intCount] + ")'>";
						
						// Check is it current date 
						if (strMonth == intInMM && strYear == intInYY && arrDays[intCount] == intInDD){
							strHTMLText += "class='SelDateBGColor'>" + strLinkTag + "<font class='SelDate'><b>" + arrDays[intCount] + "<\/b>";
						}else{
							if (strMonth == month && strYear == year && arrDays[intCount] == day){
								strHTMLText += "class='ToDateBGColor'>" + strLinkTag + "<font class='ToDate'><b>" + arrDays[intCount] + "<\/b>";
							}else{
								var strDisableUpto = strYear;
								var strDisableMonth = strMonth
								var strDisableDay = arrDays[intCount] ;
								
								if (Number(strDisableMonth) < 10){
									strDisableMonth = "0" + strDisableMonth;
								}
								
								if (Number(strDisableDay) < 10){
									strDisableDay = "0" + strDisableDay;
								}
								strDisableUpto = strDisableUpto + String(strDisableMonth) + String(strDisableDay);
								if (intDisableUpto != "" && intDisableUpto != null && strDisableUpto < intDisableUpto){
									strHTMLText += "class='DisableBGUpto'><font class='DisableUpto'>" + arrDays[intCount] + "<\/font><\/td>";
								}else{
									switch (x) {
										case 0 : strHTMLText += "class='SUBGColor'>" + strLinkTag + "<font class='SUColor'>" + arrDays[intCount]; break;
										case 1 : strHTMLText += "class='MOBGColor'>" + strLinkTag + "<font class='MOColor'>" + arrDays[intCount]; break;
										case 2 : strHTMLText += "class='TUBGColor'>" + strLinkTag + "<font class='TUColor'>" + arrDays[intCount]; break;
										case 3 : strHTMLText += "class='WEBGColor'>" + strLinkTag + "<font class='WEColor'>" + arrDays[intCount]; break;
										case 4 : strHTMLText += "class='THBGColor'>" + strLinkTag + "<font class='THColor'>" + arrDays[intCount]; break;
										case 5 : strHTMLText += "class='FRBGColor'>" + strLinkTag + "<font class='FRColor'>" + arrDays[intCount]; break;
										case 6 : strHTMLText += "class='SABGColor'>" + strLinkTag + "<font class='SAColor'>" + arrDays[intCount]; break;
									}	
								}
							}
						}
						strHTMLText	+= "<\/font><\/a><\/td>"
						intDCount++;
					}else{
						strHTMLText += "class='DisableDaysBGColor'><font class='DisableDays'>" + arrDays[intCount] + "<\/font><\/td>";
					}
				}else{
					strHTMLText += "class='DisableDaysBGColor'><font class='DisableDays'>" + arrDays[intCount] + "<\/font><\/td>";
				}
				intCount++
			}
			strHTMLText		+= "	<\/tr>"
		}
		
		strHTMLText		+= "<\/table>"
		writeDiv(strHTMLText, "divCal");
	}
	
	function Day_onClick(strDay){
		var objselMonth = document.getElementById("selMonth");
		var strSelMonth = objselMonth.options[objselMonth.selectedIndex].value
		
		var objselYear = document.getElementById("selYear");
		var strSelYear = objselYear.options[objselYear.selectedIndex].value
		
		if (strDay < 10){strDay = "0" + strDay}
		if (strSelMonth < 10){strSelMonth = "0" + strSelMonth}
		//alert(strDay + "\n" + strSelMonth + "\n" + strSelYear + "\n" + objParent)
		parent.DateReturn(strDay, strSelMonth, strSelYear, objParent); 
		strinDate = strDay + "/" + strSelMonth + "/" + strSelYear;
		SetCurrDate();
		//window.close();
	}
	
	function PrevClick(){
		var objselMonth = document.getElementById("selMonth");
		var strSelMonth = objselMonth.options[objselMonth.selectedIndex].value;
		strSelMonth = Number(strSelMonth) - 1; 
		var blnYear = true; 
		if (strSelMonth == 0){
			strSelMonth = 12;
			blnYear = false;
			var objselYear = document.getElementById("selYear");
			var strSelYear = objselYear.options[objselYear.selectedIndex].value;
			strSelYear = Number(strSelYear) - 1
			for (var i = 0; i < objselYear.length; i++){
				if (objselYear.options[i].value == strSelYear){
					objselYear.options[i].selected = true;
					blnYear = true; 
					break;
				}
			}
		}
		
		if (blnYear){
			strSelMonth = Number(strSelMonth) - 1; 
			objselMonth.options[strSelMonth].selected = true;
			selMonth_onChange();
		}
	}
	
	function NextClick(){
		var objselMonth = document.getElementById("selMonth");
		var strSelMonth = objselMonth.options[objselMonth.selectedIndex].value;
		strSelMonth = Number(strSelMonth) + 1; 
		var blnYear = true; 
		if (strSelMonth == 13){
			strSelMonth = 1;
			
			blnYear = false;
			var objselYear = document.getElementById("selYear");
			var strSelYear = objselYear.options[objselYear.selectedIndex].value;
			strSelYear = Number(strSelYear) + 1
			for (var i = 0; i < objselYear.length; i++){
				if (objselYear.options[i].value == strSelYear){
					objselYear.options[i].selected = true;
					blnYear = true; 
					break;
				}
			}
		}
		if (blnYear){
			strSelMonth = Number(strSelMonth) - 1; 
			objselMonth.options[strSelMonth].selected = true;
			selMonth_onChange();
		}
	}
	
	function CDateClick(){
		var objselMonth = document.getElementById("selMonth");
		objselMonth.options[month].selected = true;
		
		var objselYear = document.getElementById("selYear");
		for (var i = 0; i < objselYear.length; i++){
			if (objselYear.options[i].value == year){
				objselYear.options[i].selected = true; 
				break;
			}
		}
		selMonth_onChange();
	}
	
	// GET NUMBER OF DAYS IN MONTH
	function getDaysInMonth(month,year)  {
		var days;
		if (month==1 || month==3 || month==5 || month==7 || month==8 ||
			month==10 || month==12)  days=31;
		else if (month==4 || month==6 || month==9 || month==11) days=30;
		else if (month==2)  {
			if (isLeapYear(year)) {
				days=29;
			}
			else {
				days=28;
			}
		}
		return (days);
	}


	// CHECK TO SEE IF YEAR IS A LEAP YEAR
	function isLeapYear (Year) {
		if (((Year % 4)==0) && ((Year % 100)!=0) || ((Year % 400)==0)) {
			return (true);
		}
		else {
			return (false);
		}
	}

	// ----------------------
	function writeDiv(text,id){
		if (document.getElementById)
		{	x = document.getElementById(id);
			x.innerHTML = "";
			x.innerHTML = text;
		}
		else if (document.all)
		{
			x = document.all[id];
			x.innerHTML = text;
		}
		else if (document.layers)
		{
			x = document.layers[id];
			x.document.open();
			x.document.write(text);
			x.document.close();
		}
	}
//-->
			</script>
	</head>
	<body  oncontextmenu="return false" ondrag='return false' onkeydown='return Body_onKeyDown(event)'>
		<table width="252" cellpadding="0" cellspacing="1" border="1" class="BGColor" align="center">
			<tr class="TblLineColor">
				<td width="67" valign="middle" style="height:20px;" class="ToDateBGColor">
					<select id="selMonth" name="selMonth" size="1" class="clsDD" onchange="selMonth_onChange()">
						<option value="1">Jan</option>
						<option value="2">Feb</option>
						<option value="3">Mar</option>
						<option value="4">Apr</option>
						<option value="5">May</option>
						<option value="6">Jun</option>
						<option value="7">Jul</option>
						<option value="8">Aug</option>
						<option value="9">Sep</option>
						<option value="10">Oct</option>
						<option value="11">Nov</option>
						<option value="12">Dec</option>
					</select>
				</td>
				<td align="center" valign="middle" class="ToDateBGColor">
					<div id="divMonth"></div>
				</td>
				<td width="66" valign="middle" align="right" class="ToDateBGColor">
					<select id="selYear" name="selYear" size="1" class="clsDD" onchange="selMonth_onChange()">
						<option value='2005'>2005</option>
					</select>
				</td>
			</tr>
		</table>
		<div id="divCal"></div>
		<table width="252" cellpadding="0" cellspacing="1" border="1" class="BGColor" align="center"
			ID="Table1">
			<tr class="TblLineColor">
				<td width="33" align='center' class='NavMonthBGColor'><span id='spnPMonth'></span></td>
				<td class='ToDateBGColor' colspan="5" align="center"><div id="divCD" class="ToDate"></div></td>
				<td width="33" align='center' class='NavMonthBGColor'><span id='spnNMonth'></span></td>
			</tr>
		</table>
		<script  type="text/javascript">
	<!--
		SetCurrDate();
		var blnCalendarLoaded = true;
	//-->
		</script>
	</body>
</html>
