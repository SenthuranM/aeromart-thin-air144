	/*
	*********************************************************
		Description		: Combo box (Fully client side)
		Author			: Rilwan A. Latiff
		Version			: 1.0
		Last Modified	: 6nd July 2005
	*********************************************************	
	*/
	
	function combo(){
		this.id = "cbo0";						// combobox ID
		this.top = 0;							// top location
		this.left = 0;							// left location
		this.width = "100";						// width
		this.height = "152";					// Height;
		this.className = "";					// combo input box class
		this.buttonColor = "#ECECEC"			// combo button class
		this.zindex = "50";						// z-index
		this.valueIndex = 0;					// Data Array
		this.textIndex = 1;						// Data array value array element number;
		this.enabled = false;					// Data array text array element number;
		this.arrData = new Array();				// combo enabled or not
		this.maxlength = 0;						// text box maxlength;
		this.tabIndex = 0 ;						// Tab Index
		this.textDisplayLength = 0 ;			// Number of characters to be display in the list box..
		this.onChange = ""						// Onchange Custom Funciton 
		this.softSearchEnable = true;			// Soft Search Enable
												
		this.selectedValue = "";				// selected value
		this.selectedText = "";					// selected Text
												
												
		this.buildCombo = buildCombo;			// Build the combo
		this.getComboValue = getComboValue;		// get the selected value
		this.getComboText = getComboText;		// get the selected text
		this.setSelectedText = setComboText;	// set the selected text
		this.comboValidate = comboValidate;		// validate the entred data with the data array.
		this.comboValidateAlternate = comboValidateAlternate; //using this to validate travel agent
		this.setText = comboSetText
		this.focus = setComboFocus;		
		this.getText = comboGetText										
		this.visible = setComboVisible;									
		this.setComboValue = setComboValue;		
		this.disable = setDisableCombo
				
		window._cboLayer = "comboLayer";
		window._cboImgPath = "../../images/";
		window._cboOver = false;
		window._cboIframe = "../../js/common/ComboList.jsp";
		window._cboTimerSeconds = 60000;
		window._cboArrTemp = new Array();
		window._cboLastRow = -1;
		window._cboRecfound = false;
		window._cboFirstValue = "";
		
		if (!window._arrCboObj) window._arrCboObj = new Array();
		window._arrCboObj[this.id] = this;
		window._arrCboObj[window._arrCboObj.length] = this;
		this._cboTimer;
		this.blnVisible = false ;
		this.firstTime = true;
		this.intArrIndex = null;
	}

	function buildCombo(){
		document.writeln('<span id="' + window._cboLayer  + '" style="visibility:hidden;">?<\/span>');
		var objContainer = document.getElementById(window._cboLayer);
		objContainer.arrCbo = new Array();
		
		var strCboHTMLText = "";
		for (var i = 0 ; i < window._arrCboObj.length ; i++){
			objContainer.arrCbo[i] = window._arrCboObj[i];
			var objCbo = window._arrCboObj[i];
			var strListSpnID = window._cboLayer + '_' + objCbo.id + '_data';
			
			var strReadonly = "";
			strCboHTMLText += '<span id="' + window._cboLayer + '_' + objCbo.id + '" style="position:absolute; top:' + objCbo.top + 'px; left:' + objCbo.left + 'px;height:18px;">'
			strCboHTMLText += '<table border="0" cellpadding="0" cellspacing="0">';
			strCboHTMLText += '<tr><td>';
			strCboHTMLText += '<input type="text" id="txt_' + window._cboLayer + '_' + objCbo.id +'" class="' + objCbo.inputCss + '" style="width:' + objCbo.width + 'px;" onkeyup="comboTxtKeyDown(event,' + "'" + objCbo.id + "'" + ');" onblur="comboTxtonBlur(' + "'" + objCbo.id + "'" + ');" ' + strReadonly + ' value="' + objCbo.selectedText + '" tabindex="' + objCbo.tabIndex + '"' ;
			if (objCbo.maxlength > 0){
				strCboHTMLText += ' maxlength="' + objCbo.maxlength + '"';
			}
			strCboHTMLText += '>';
			strCboHTMLText += '<\/td>';
			
			if (objCbo.softSearchEnable){
				strCboHTMLText += '<td style="background-color:'+ objCbo.buttonColor + ';" valign="top">';
				strCboHTMLText += '<table border="0" cellpadding="2" cellspacing="1">';
				strCboHTMLText += '<tr><td class="cboButton" align="center" ';
				strCboHTMLText += 'onclick="comboToggle(' + "'" + objCbo.id + "'" + ')" >';
				strCboHTMLText += '<img id="img_' + window._cboLayer + '_' + objCbo.id +'" src="' + window._cboImgPath + 'DD_no_cache.gif" border="0">';
				strCboHTMLText += '<\/td><\/tr><\/table><\/td>';
			}
			
			strCboHTMLText += '<\/tr><\/table>';
			strCboHTMLText += '<\/span>'
			strCboHTMLText += '<span id="' + window._cboLayer + '_' + objCbo.id + '_data" style="position:absolute; top:' + (Number(objCbo.top) + 20) + 'px; left:' + objCbo.left + 'px; z-index:' + objCbo.zindex + ';visibility:hidden;height:0px;">';
			strCboHTMLText += '	<iframe id="ifrm_' + window._cboLayer + '_' + objCbo.id + '_data" name="ifrm_' + window._cboLayer + '_' + objCbo.id + '_data" frameborder="0" width="' + (Number(objCbo.width) + 16) + '" src="' + window._cboIframe + '" class="cboListBorder"><\/iframe>';
			strCboHTMLText += '<\/span>'
		}
		document.writeln(strCboHTMLText);
	}

	function getComboValue(){
		return this.selectedValue;
	}

	function getComboText(){
		return this.selectedText;
	}

	function comboSetText(){
		document.getElementById('txt_' + window._cboLayer + '_' + this.id).value = "";
	}
	function comboGetText(){
		return document.getElementById('txt_' + window._cboLayer + '_' + this.id).value;
	}
	
	function setComboText(){
		document.getElementById('txt_' + window._cboLayer + '_' + this.id).value = this.selectedText ;
	}
	function setComboVisible(blnStatus){
		var strStatus = "hidden";
		if (blnStatus){
			strStatus = "visible";
		}
		document.getElementById(window._cboLayer + '_' + this.id).style.visibility = strStatus;
	}

	function setComboFocus(){
		document.getElementById('txt_' + window._cboLayer + '_' + this.id).focus();
	}
	
	function setDisableCombo(blnStatus){
		var objTxtC =  document.getElementById('txt_' + window._cboLayer + '_' + this.id);
		objTxtC.disabled = blnStatus;
		this.enabled = blnStatus;
	}

	function comboTxtonBlur(strID){
		var objContainer = document.getElementById(window._cboLayer);
		for (var i = 0 ; i < objContainer.arrCbo.length ; i++){
			var objCbo = objContainer.arrCbo[i];
			if (objCbo.id == strID){
				if (objCbo.softSearchEnable){
					objCbo.blnVisible = false;
					comboViewToggle(strID, false)
					window._cboLastRow = -1;
					clearTimeout(objCbo._cboTimer);
					if (window._cboFirstValue != ""){
						if (document.getElementById('txt_' + window._cboLayer + '_' + objCbo.id).value != ""){
							objCbo.blnVisible = true;
							comboItemClick(window._cboFirstValue, objCbo.id);
						}
					}
				}else{
					objCbo.comboValidate();
				}
				break;
			}
		}
	}

	function comboValidate(){
		var blnReturn = false;
		var intCount = this.arrData.length;
		var strTxtValue =  document.getElementById('txt_' + window._cboLayer + '_' + this.id).value;
		if (trim(strTxtValue) != ""){
			for (var i = 0 ; i < intCount ; i++){
				if (this.arrData[i][this.valueIndex].toUpperCase() == strTxtValue.toUpperCase()){
					this.selectedValue = this.arrData[i][this.valueIndex];
					this.selectedText = this.arrData[i][this.textIndex];
					this.setSelectedText();  
					blnReturn = true;
					break;
				}
			}
			
			if (!blnReturn){
				for (var i = 0 ; i < intCount ; i++){
					if (this.arrData[i][this.textIndex].toUpperCase() == strTxtValue.toUpperCase()){
						this.selectedValue = this.arrData[i][this.valueIndex];
						this.selectedText = this.arrData[i][this.textIndex];
						this.setSelectedText();  
						blnReturn = true;
						break;
					}
				}
			}
		}
		return blnReturn
	}

	function setComboValue(strValue){
		var blnReturn = false;
		var intCount = this.arrData.length;
		for (var i = 0 ; i < intCount ; i++){
			if (this.arrData[i][this.valueIndex].toUpperCase() == strValue.toUpperCase()){
				this.selectedValue = this.arrData[i][this.valueIndex];
				this.selectedText = this.arrData[i][this.textIndex];
				this.setSelectedText();  
				break;
			}
		}
	}

	function comboViewToggle(strID, blnView){
		var objControl  = document.getElementById(window._cboLayer + '_' + strID + '_data');
		var objIControl = document.getElementById('ifrm_' + window._cboLayer + '_' + strID + '_data');
		var strVisible = "hidden";
		if (blnView){
			strVisible = "visible";	
		}else{
			objControl.style.height = "0px";
		}
		objControl.style.visibility = strVisible;
		objIControl.style.visibility = strVisible;	
	}

	function HideListBox(strID){
		if (!window._cboOver){
			comboTxtonBlur(strID)
		}
	}


	function comboTxtFocus(strID, intArrIndex){
		document.getElementById('txt_' + window._cboLayer + '_' + strID).focus();
		var objContainer = document.getElementById(window._cboLayer);
		for (var i = 0 ; i < objContainer.arrCbo.length ; i++){
			var objCbo = objContainer.arrCbo[i];
			if (objCbo.id == strID){
				objCbo.intArrIndex = intArrIndex;
				break;
			}
		}
	}

	function comboTxtKeyDown(objEvent,strID){
		try{
			var objContainer = document.getElementById(window._cboLayer);
			for (var i = 0 ; i < objContainer.arrCbo.length ; i++){
				var objCbo = objContainer.arrCbo[i];
				var strSpnTagID = window._cboLayer + '_' + objCbo.id + '_data'
				if (objCbo.id == strID){
					if (objCbo.softSearchEnable){
						var strEnterdValue = document.getElementById('txt_' + window._cboLayer + '_' + objCbo.id).value;
						if (objEvent != null){
							if (objEvent.keyCode == "38" || objEvent.keyCode == "40"){
								if (objCbo.firstTime){
									comboList(strSpnTagID, objCbo.arrData, objCbo.valueIndex, objCbo.textIndex, objCbo.width, strID, strEnterdValue, objCbo.textDisplayLength, objCbo.height);				
								}
								var frmName = 'ifrm_' + window._cboLayer + '_' + strID + '_data';
								frames[frmName].MoveCursor(objEvent.keyCode);	
							}else{
								if (objEvent.keyCode == 13){
									window._cboFirstValue = "";
									if (window._cboLastRow >= 0){
										comboItemClick(window._cboLastRow, objCbo.id);
									}else{
										objCbo.blnVisible = false;
										comboViewToggle(objCbo.id, false)
									}
									break;
								}else{
									comboList(strSpnTagID, objCbo.arrData, objCbo.valueIndex, objCbo.textIndex, objCbo.width, strID, strEnterdValue, objCbo.textDisplayLength, objCbo.height);
								}
							}
						}else{
							comboList(strSpnTagID, objCbo.arrData, objCbo.valueIndex, objCbo.textIndex, objCbo.width, strID, "", objCbo.textDisplayLength, objCbo.height);
						}
						objCbo.firstTime = false;
						var strHideFunction = "HideListBox('" + objCbo.id + "')"
						objCbo._cboTimer = setInterval(strHideFunction, window._cboTimerSeconds);

						objCbo.blnVisible = true;
						comboViewToggle(strID, true)
						
						if (objCbo.onChange != ""){
							eval(objCbo.onChange + "('" + strID + "')");
						}
					}
						
					break;
				}else{
					objCbo.blnVisible = false;
					comboViewToggle(objCbo.id, false)
					clearTimeout(objCbo._cboTimer);
				}
			}
		}catch (ex){
		}
	}

	function comboResize(intHeight, strID){
		var intHt = 152
		var objContainer = document.getElementById(window._cboLayer);
		for (var i = 0 ; i < objContainer.arrCbo.length ; i++){
			var objCbo = objContainer.arrCbo[i];
			if (objCbo.id == strID){
				intHt = Number(objCbo.height);
			}
		}
		var objSpnControl = document.getElementById(window._cboLayer + '_' + strID + '_data');
		var objIfrmControl = document.getElementById('ifrm_' + window._cboLayer + '_' + strID + '_data');
		if (Number(intHeight) < intHt){
			intHt = intHeight;
		}
		objSpnControl.style.height = intHt ;
		objIfrmControl.style.height = intHt ;	
		if (intHt == 0){
			comboViewToggle(strID, false);
		}else{
			comboViewToggle(strID, true);
		}
		
	}

	function comboItemClick(intArrIndex, strID){
		try{
			var objContainer = document.getElementById(window._cboLayer);
			for (var i = 0 ; i < objContainer.arrCbo.length ; i++){
				var objCbo = objContainer.arrCbo[i];
				if (objContainer.arrCbo[i].id == strID){
					if (objCbo.blnVisible){
						window._cboFirstValue = "";
						document.getElementById('txt_' + window._cboLayer + '_' + objCbo.id).value = objCbo.arrData[intArrIndex][objCbo.textIndex];
						objCbo.selectedValue = objCbo.arrData[intArrIndex][objCbo.valueIndex];
						objCbo.selectedText = objCbo.arrData[intArrIndex][objCbo.textIndex];
						if (objCbo.onChange != ""){
							eval(objCbo.onChange + "('" + strID + "')");
						}
						objCbo.blnVisible = false;
						comboViewToggle(objCbo.id, false);
					}
					break;
				}
			}
		}catch (e){
		}
	}

	function comboToggle(strID){
		var objContainer = document.getElementById(window._cboLayer);
		for (var i = 0 ; i < objContainer.arrCbo.length ; i++){
			var objCbo = objContainer.arrCbo[i];
			var strSpnTagID = window._cboLayer + '_' + objCbo.id + '_data';
			clearTimeout(objCbo._cboTimer);
			if (objCbo.id == strID){
				if (objCbo.enabled){break;}
				if (objCbo.blnVisible){
					objCbo.blnVisible = false;
					comboViewToggle(objCbo.id, false);
				}else{
					objCbo.blnVisible = true;
					comboViewToggle(objCbo.id, true);
					//document.getElementById('txt_' + window._cboLayer + '_' + strID).focus();
					
					//comboList(strSpnTagID, objCbo.arrData, objCbo.valueIndex, objCbo.textIndex, objCbo.width, strID, "", objCbo.textDisplayLength);
					comboTxtKeyDown(null, strID);

	//				var strHideFunction = "HideListBox('" + objCbo.id + "')"
	//				objCbo._cboTimer = setInterval(strHideFunction, window._cboTimerSeconds);
				}
			}else{
				objCbo.blnVisible = false;
				comboViewToggle(objCbo.id, false);
			}
		}
	}	
	

	function comboList(strSpnID, arrData, intValueIndex, intTextIndex, intWidth, strID, strValue, intMaxLength, intHeight){
		var strCboHTMLTxt = "";
		var intArrCount = arrData.length ;
		intWidth = Number(intWidth);
		strCboHTMLTxt += '<table width="98%" border="0" cellpadding="2" cellspacing="0" align="center">'
		
		var blnFound  = false ;
		var intCount = 0 
		var strTitle = "";
		var blnDataFound = false;
		
		window._cboArrTemp.splice(0,window._cboArrTemp.length)
		window._cboLastRow = -1;
		window._cboRecfound = false;
		window._cboFirstValue = "";
		strValue = trim(strValue);
		
		for (var i = 0 ; i < intArrCount ; i++){			
			var strArrValue = arrData[i][intTextIndex] ;
			strArrValue = strArrValue.toUpperCase();
			strValue = strValue.toUpperCase();
			strTitle = '';
			
			blnDataFound = true;
			if (strValue != ""){				
				var arrCountryDesc = new Array();
				if (strArrValue.substring(0,strValue.length) != strValue){
					arrCountryDesc = strArrValue.split(/[ _-]/);
        			
        			if (arrCountryDesc != []){
        				var descLength = arrCountryDesc.length;	
						for(var j =0 ; j < descLength ; j++){
								if(arrCountryDesc[j].indexOf(strValue) > 0){
									blnDataFound = true;
									break;
								} else {
									blnDataFound = false;									
								}	
								
						}	
        			} else {
        				blnDataFound = false;
        			}
				} else {
					blnDataFound = true;					
				}			
			}			
			if (blnDataFound){
				window._cboArrTemp[intCount] = i;
				strArrValue = arrData[i][intTextIndex];
				if (intMaxLength > 0){
					if (strArrValue.length > intMaxLength){
						strTitle = ' title = "' +  strArrValue + '" ';
						strArrValue = strArrValue.substring(0,intMaxLength) + "..."
					}
				}
				strCboHTMLTxt += '<tr><td id="TD_' + intCount + '" class="cboItems cboItemsDefault" onmouseover="comboItemsOver(this,' + "'" + strID  + "'," + i + ')" onmouseout="comboItemsOut(this)" onmousedown="parent.comboItemClick(' + "'" + i + "','" + strID + "'" + ')">';
				strCboHTMLTxt += '<font class="cboFont">' + unescape(strArrValue) + '<\/font>';
				strCboHTMLTxt += '<\/td><\/tr>';
				
				if (strArrValue != ""){
					if (window._cboFirstValue == ""){
						window._cboFirstValue = i;
					}
				}
				
				blnFound = true;
				window._cboRecfound = true;
				intCount++
			}
		}
		
		strCboHTMLTxt += '<\/table>';
		if (!blnFound){
			strCboHTMLTxt = "";
		}
		window._cboOver = true;
		var frmName = 'ifrm_' + strSpnID;
		frames[frmName].writeList(strCboHTMLTxt, strID, intCount, intHeight);
	}

	function trim(strText) { 
		while (strText.substring(0,1) == ' ') 
			strText = strText.substring(1, strText.length);
		while (strText.substring(strText.length-1,strText.length) == ' ')
			strText = strText.substring(0, strText.length-1);
		return strText;
	} 
	
	
	// adding a new method to valudate travel agents

	function comboValidateAlternate(){
		var blnReturn = false;
		var intCount = this.arrData.length;
		var strTxtValue =  document.getElementById('txt_' + window._cboLayer + '_' + this.id).value;
		if (trim(strTxtValue) != ""){
			this.selectedText = trim(strTxtValue);
			this.setSelectedText();  
			blnReturn = true;
		}
		return blnReturn
	}
	
	// ----------------------------------- End of Tab ----------------------------------------