	/*
	*********************************************************
		Description		: Drop down filling (Fully client side)
		Author			: Rilwan A. Latiff
		Version			: 1.0
		Last Modified	: 16th August 2005
	*********************************************************	
	*/
	
	function listBox(){
		this.id = "lst1";								// List Box ID
		this.dataArray = new Array();					// Data Array
		this.valueIndex = 0;							// Value field array index
		this.textIndex = 1;								// Text field array index
		this.blnFirstEmpty = false;						// first value empty or not
		this.blnMergeTextValue = false;					// merge text field with the value field or not
		this.firstValue = ""							// first value 
		this.firstTextValue = "Please Select a value"	// first Text
		
		// methords
		this.fillListBox = fillListBox;					// filling the list box 
	}

	function fillListBox(){
		var objC = this ;
		var intLength = objC.dataArray.length ;
		var arrID = objC.id.split(",");
		var objDL; 
		
		// get the selectet box objects to array
		for (var x = 0 ; x < arrID.length ; x++){
			arrID[x] = document.getElementById(arrID[x]);
			objDL = arrID[x];
			objDL.length = 0 ; 
		}
		
		
		// fill the first value
		if (objC.blnFirstEmpty){
			for (var x = 0 ; x < arrID.length ; x++){
				objDL = arrID[x];
				objDL.length =  objDL.length + 1
				objDL.options[objDL.length - 1].text = objC.firstTextValue;
				objDL.options[objDL.length - 1].value = objC.firstValue;
			}
		}
		
		// fill the data
		for (var i = 0 ; i < intLength ; i++){
			for (var y = 0 ; y < arrID.length ; y++){
				objDL = arrID[y];
				objDL.length = objDL.length + 1
				if (objC.blnMergeTextValue){
					objDL.options[objDL.length - 1].text = objC.dataArray[i][objC.valueIndex] + " - " + objC.dataArray[i][objC.textIndex];
				}else{
					objDL.options[objDL.length - 1].text = objC.dataArray[i][objC.textIndex];
				}
				objDL.options[objDL.length - 1].value = objC.dataArray[i][objC.valueIndex];
			}
		}
	}
	// --------------------------------- End of Drop down filling