var screenId = "SC_ADMN_026";
var valueSeperator = "~";

if (getTabValues(screenId, valueSeperator, "intLastRec") == "") {
	setTabValues(screenId, valueSeperator, "intLastRec", 1);
}
var objCol1 = new DGColumn();
objCol1.columnType = "label";
objCol1.width = "10%";
objCol1.arrayIndex = 1;
objCol1.headerText = "GDS Code";
objCol1.itemAlign = "left"
objCol1.sort = "true";

var objCol2 = new DGColumn();
objCol2.columnType = "label";
objCol2.width = "24%";
objCol2.arrayIndex = 2;
objCol2.headerText = "GDS Description";
objCol2.itemAlign = "left"

var objCol3 = new DGColumn();
objCol3.columnType = "label";
objCol3.width = "28%";
objCol3.arrayIndex = 3;
// objCol3.toolTip = "Remarks" ;
objCol3.headerText = "Comments";
objCol3.itemAlign = "Left"

var objCol4 = new DGColumn();
objCol4.columnType = "label";
objCol4.width = "7%";
objCol4.arrayIndex = 4;
objCol4.headerText = "Status";
objCol4.itemAlign = "left"

var objCol5 = new DGColumn();
objCol5.columnType = "label";
objCol5.width = "10%";
objCol5.arrayIndex = 5;
objCol5.headerText = "Publish Mechanism";
objCol5.itemAlign = "left"

// ---------------- Grid
var objDG = new DataGrid("spnGDS");

objDG.addColumn(objCol1);
objDG.addColumn(objCol2);
objDG.addColumn(objCol3);
objDG.addColumn(objCol4);
objDG.addColumn(objCol5);

objDG.width = "99%";
objDG.height = "180px";
objDG.headerBold = false;
objDG.rowSelect = true;
objDG.arrGridData = arrData;
objDG.seqNo = true;
objDG.seqStartNo = getTabValues(screenId, valueSeperator, "intLastRec"); // Grid
objDG.pgnumRecTotal = totalRecords; // remove as per return record size
objDG.paging = true
objDG.pgnumRecPage = 20;
objDG.pgonClick = "gridNavigations";
objDG.rowClick = "RowClick";
objDG.displayGrid();
