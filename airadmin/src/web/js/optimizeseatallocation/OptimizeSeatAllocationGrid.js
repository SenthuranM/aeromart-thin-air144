var isMinus = false;

function BackCloseClick(btnVal) {
	var isChanged = false;

	for ( var i = 0; i < optimizeData.length; i++) {
		if (optimizeData[i][33] == 'Y') {
			isChanged = true;
		}
	}
	if (isChanged) {
		var isClosedPressed = confirm("Changes will be lost! Do you wish to Continue?");
		if (!isClosedPressed) {
		} else {
			if (btnVal == 'Back') {
				top[1].objTMenu.tabSetTabInfo("SC_INVN_005", "SC_INVN_004",
						"showOptimizeSearch.action", "Optimize", false);
				location.replace("showOptimizeSearch.action");
			} else {
				top[1].objTMenu.tabRemove('SC_INVN_005');
			}

		}
	} else {
		if (btnVal == 'Back') {
			top[1].objTMenu.tabSetTabInfo("SC_INVN_005", "SC_INVN_004",
					"showOptimizeSearch.action", "Optimize", false);
			location.replace("showOptimizeSearch.action");
		} else {
			top[1].objTMenu.tabRemove('SC_INVN_005');
		}
	}
}

function objOnFocus() {
	top[2].HidePageMessage();
}

function setPageEdit() {
	Disable('btnSave', false);
	setPageEdited(true);
}

function setPageEdited(isEdited) {
	top.pageEdited = isEdited;
}

function RecMsgFunction(intRecNo, intErrMsg) {
	top[2].HidePageMessage();
	if (intErrMsg != "") {
		top[2].objMsg.MessageText = intErrMsg;
		top[2].objMsg.MessageType = "Error";
		top[2].ShowPageMessage();
	}
}

var strPColor = "red"
function setColors(strRowData) {
	strRowData = Number(strRowData[0]);
	if (arrData[strRowData][1] != "") {
		if (strPColor == "red") {
			strPColor = "green";
		} else {
			strPColor = "red";
		}
	}
	strColor = strPColor;
	return strColor;
}

function winOnLoad(strMsg, strMsgType) {
	setPageEdited(false);
	if (saveSuccess != '' && saveSuccess == 1) {
		alert("Record Successfully saved!");
		saveSuccess = 0;
	}
	if (top[1].objTMenu.tabGetValue("SC_INVN_005") != ""
			&& top[1].objTMenu.tabGetValue("SC_INVN_005") != null) {
		var strSearch = top[1].objTMenu.tabGetValue("SC_INVN_005").split("@");
		setField("hdnMode", strSearch[0]);
		setField("hdnVersion", strSearch[1]);
		setField("txtFromDate", strSearch[2]);

		setField("txtToDate", strSearch[3]);
		setField("txtFlightNo", strSearch[4]);
		setField("hdnAssignOnD", strSearch[5]);

		setField("hdnBCDisType", strSearch[6]);
		setField("hdnAssignBC", strSearch[7]);
		setField("hdnAllocType", strSearch[8]);
		setField("txtSeats", strSearch[9]);

		setField("selMoreLess", strSearch[10]);
		setField("radSegLevel", strSearch[11]);
		setField("radBCLevel", strSearch[12]);

		setField("radShowBc", strSearch[13]);
		setField("radAgent", strSearch[14]);
		setField("selAgent", strSearch[15]);

		if (!document.all) {
			document.getElementById("idFromDate").textContent = strSearch[2];
			document.getElementById("idToDate").textContent = strSearch[3];
		} else {
			document.getElementById("idFromDate").innerText = strSearch[2];
			document.getElementById("idToDate").innerText = strSearch[3];
		}

		top.setFID('SC_INVN_005');
	}

	if (optimizeData.length > 0) {
		
		$("#divOpt3").scroll(function () { 
		        $("#divOpt1").scrollTop($("#divOpt3").scrollTop());
		        $("#divOpt1").scrollLeft($("#divOpt3").scrollLeft());

		 	$("#divOpt4").scrollTop($("#divOpt3").scrollTop());
		        $("#divOpt4").scrollLeft($("#divOpt3").scrollLeft());
		    });
		
	} else {
		document.getElementById("idGridTable").innerHTML = '<table width="100%"><tr><td style="text-align:center;"><font class="fntBold">No records to Display</font></td></tr></table>';
	}

	if (strSearch[4] == '' || strSearch[4].length == 2) {
		document.getElementById("idFlightLable").innerHTML = '&nbsp;';
	} else {
		if (!document.all) {
			document.getElementById("idFlightNo").textContent = strSearch[4];
		} else {
			document.getElementById("idFlightNo").innerText = strSearch[4];
		}				
	}
	if (strSearch[5] == '') {
		document.getElementById("idSegLable").innerHTML = '&nbsp;';
	} else {		
		var strOnDs = getAllSegments( strSearch[5]) ;
		if (!document.all) {
			document.getElementById("idSegs").textContent =strOnDs;
		} else {
			document.getElementById("idSegs").innerText = strOnDs;
		}
		
	}
	if (strSearch[15] == '') {
		if (!document.all) {
			document.getElementById("idAgtLable").textContent = '';
		} else {
			document.getElementById("idAgtLable").innerText = '';
		}
		
	} else {
		if (!document.all) {
			document.getElementById("idAgent").textContent = strSearch[16];
		} else {
			document.getElementById("idAgent").innerText = strSearch[16];
		}
	}
	Disable('btnSave', true);
	createDataRows();
	displayResult();
	objOnFocus();
	if (strMsg != null && strMsgType != null) {
		showCommonError(strMsgType, strMsg);
	}
}

function getAllSegments(strOnDs) {	
	var segments = strOnDs.split(",");
	var values = "";
	for ( var t = 0; t < (segments.length); t++) {
		var str = segments[t];
		var strReplace = segments[t];
		if (str.indexOf("***") != -1) {
			strReplace = str.replace("***", "All");
		}
		values += (strReplace + (t<(segments.length-1)? ",":""));		
		
	}
	return values;
}

function displayResult() {
	if (optiStatus != '' && optiStatus != null) {
		for ( var i = 0; i < optimizeResult.length; i++) {
			if (optimizeResult[i][1] == '1') // 1 for error - color the
												// records.
			{
				for ( var j = 0; j < optimizeData.length; j++) {
					if (optimizeResult[i][2] == 'segment'
							|| optimizeResult[i][2] == 'unidentified') {
						if (optimizeData[j][25] == optimizeResult[i][0]) {
							getFieldByID('optRow' + j + '').title = optimizeResult[i][3];
							for ( var x = 1; x < 22; x++) {
								setStyleClass('optRow' + j + '_' + x + '',
										'fltStatus03');
							}
						}
					} else if (optimizeResult[i][2] == 'bc') {
						if (optimizeData[j][27] == optimizeResult[i][4]) {
							getFieldByID('optRow' + j + '').title = optimizeResult[i][3];
							for ( var x = 1; x < 22; x++) {
								setStyleClass('optRow' + j + '_' + x + '',
										'fltStatus03');
							}
						}
					} else if (optimizeResult[i][2] == 'fixedbc') {
						if (optimizeData[j][25] == optimizeResult[i][0]) {
							if (optimizeData[j][16] == 'true') {
								getFieldByID('optRow' + j + '').title = optimizeResult[i][3];
								for ( var x = 1; x < 22; x++) {
									setStyleClass('optRow' + j + '_' + x + '',
											'fltStatus03');
								}
							}
						}
					}
				}
			}
		}
	}
}

function createDataRows() {

	var strHTMLTable1 = "";
	var strHTMLTable2 = "";
	var strTabCount = 0;
	var strFntcls = '';

	if (optimizeData.length > 0) {
		strHTMLTable1 += "<table border='0' cellspacing='1' cellpadding='2' bgcolor='black' width='727px'> ";
		strHTMLTable2 += "<table border='0' cellspacing='1' cellpadding='2' bgcolor='black' width='490px'> ";

		for ( var i = 0; i < optimizeData.length; i++) {
			
			var blnOverBooked = false;
			var blinkTag = "";
			if (optimizeData[i][2] != '&nbsp;') {
				var strSoldAduInf = optimizeData[i][8].split("/");
				var strSoldAdu = strSoldAduInf[0];
				var strSoldInf = strSoldAduInf[1];
	
				var strOnholdAduInf = optimizeData[i][9].split("/");
				var strOnholdAdu = strOnholdAduInf[0];
				var strOnholdInf = strOnholdAduInf[1];
				blnOverBooked = ((Number(strSoldAdu) + Number(strSoldInf) + Number(strOnholdAdu) + Number(strOnholdInf)) > Number(optimizeData[i][6]));

			}
		
			for ( var x = 1; x < 39; x++) {
				if (optimizeData[i][x] == '') {
					optimizeData[i][x] = '&nbsp;';
				}
			}

			strFntcls = 'fntOptDef';
			if (optimizeData[i][40] == 'M') {
				if (optimizeData[i][15] != '&nbsp;') {
					if (optimizeData[i][15] == 'OPN') {
						strFntcls = 'fntOptManopn';
					} else {
						strFntcls = 'fntOptMancls';
					}
				}

			} else if (optimizeData[i][15] != '&nbsp;'
					&& optimizeData[i][15] != 'OPN') {
				strFntcls = 'fntOptSyscls';
			} else if (optimizeData[i][40] == 'R'
					&& optimizeData[i][15] == 'OPN') {
				strFntcls = 'fntOptSysopn';
			}
			
			if(blnOverBooked) {
				blinkTag =  "<font class='mandatory'><blink>";
			} else {
				blinkTag =  "<font>";
			}

			// first table
			strHTMLTable1 += "<tr id='optRow"
					+ i
					+ "' Title=''>"
					+ "		<td id='optRow"
					+ i
					+ "_1' width='52px' class='GridItemRow GridItem' style='height:27px' style='text-align:center;'><font>"
					+ optimizeData[i][1]
					+ "</font></td> "
					+ "		<td id='optRow"
					+ i
					+ "_2' width='50px' class='GridItemRow GridItem' style='height:27px' style='text-align:center;'>"
					+ blinkTag
					+ optimizeData[i][2]
					+ "</blink></font></td> "
					+ "		<td id='optRow"
					+ i
					+ "_3' width='35px' class='GridItemRow GridItem' style='height:27px' style='text-align:center;'><font>"
					+ optimizeData[i][3]
					+ "</font></td> "
					+ "		<td id='optRow"
					+ i
					+ "_4' width='30px' class='GridItemRow GridItem' style='height:27px' style='text-align:center;'><font>"
					+ optimizeData[i][4]
					+ "</font></td> "
					+ "		<td id='optRow"
					+ i
					+ "_5' width='119px' class='GridItemRow GridItem' style='height:27px' style='text-align:left;'><font>"
					+ optimizeData[i][5]
					+ "</font></td> "
					+ "		<td id='optRow"
					+ i
					+ "_6' width='38px' class='GridItemRow GridItem' style='height:27px' style='text-align:center;'><font>"
					+ optimizeData[i][6] + "</font></td> ";
			if (optimizeData[i][7] != '&nbsp;') {
				strHTMLTable1 += "		<td id='optRow"
						+ i
						+ "_7' width='45px' class='GridItemRow GridItem' style='height:27px' align='center'><input type='text' id='txtInfAlloc"
						+ i
						+ "' name='txtInfAlloc"
						+ i
						+ "' tabIndex='"
						+ (strTabCount + 1)
						+ "' style='width:30px' onkeyUp='positiveWholeNumberValidate(this)' onChange='setPageEdit();changeInfAlloc("
						+ i
						+ ",txtInfAlloc"
						+ i
						+ ")' onkeyPress='positiveWholeNumberValidate(this)' size='3' maxLength='3' value='"
						+ optimizeData[i][7]
						+ "' style='text-align:center;'></td> ";
			} else {
				strHTMLTable1 += "		<td id='optRow"
						+ i
						+ "_7' width='45px' class='GridItemRow GridItem' style='height:27px' style='text-align:center;' ><font>"
						+ optimizeData[i][7] + "</font></td> ";
			}

			if (optimizeData[i][8] != '&nbsp;') {
				strHTMLTable1 += "		<td id='optRow"
						+ i
						+ "_8' width='48px' class='GridItemRow GridItem' style='height:27px' style='text-align:center;'><font>"
						+ optimizeData[i][8] + "</font></td> ";
			} else {
				strHTMLTable1 += "		<td id='optRow"
						+ i
						+ "_8' width='48px' class='GridItemRow GridItem' style='height:27px' style='text-align:center;'><font>"
						+ optimizeData[i][8] + "</font></td> ";
			}

			if (optimizeData[i][9] != '&nbsp;') {
				strHTMLTable1 += "		<td id='optRow"
						+ i
						+ "_9' width='48px' class='GridItemRow GridItem' style='height:27px' style='text-align:center;'><font>"
						+ optimizeData[i][9] + "</font></td> ";
			} else {
				strHTMLTable1 += "		<td id='optRow"
						+ i
						+ "_9' width='48px' class='GridItemRow GridItem' style='height:27px' style='text-align:center;'><font>"
						+ optimizeData[i][9] + "</font></td> ";
			}
			if (optimizeData[i][10] != '&nbsp;') {
				strHTMLTable1 += "		<td id='optRow"
						+ i
						+ "_10' with='45px' class='GridItemRow GridItem' style='height:27px' align='center' ><input type='text' id='txtOversell"
						+ i
						+ "' name='txtOversell"
						+ i
						+ "'tabIndex='"
						+ (strTabCount + 2)
						+ "' style='width:30px' onkeyUp='positiveWholeNumberValidate(this)' onChange='setPageEdit();changeOversell("
						+ i
						+ ",txtOversell"
						+ i
						+ ")' onkeyPress='positiveWholeNumberValidate(this)' size='3' maxLength='3' value='"
						+ optimizeData[i][10]
						+ "' style='text-align:center;'></td> ";
			} else {
				strHTMLTable1 += "		<td id='optRow"
						+ i
						+ "_10' width='45px' class='GridItemRow GridItem' style='height:27px' style='text-align:center;'><font>"
						+ optimizeData[i][10] + "</font></td> ";
			}
			if (optimizeData[i][11] != '&nbsp;') {
				strHTMLTable1 += "		<td id='optRow"
						+ i
						+ "_11' width='45px' class='GridItemRow GridItem' style='height:27px' align='center'><input type='text' id='txtCurtailed"
						+ i
						+ "' name='txtCurtailed"
						+ i
						+ "' tabIndex='"
						+ (strTabCount + 3)
						+ "' style='width:30px' onkeyUp='positiveWholeNumberValidate(this)' onChange='setPageEdit();changeCurtailed("
						+ i
						+ ",txtCurtailed"
						+ i
						+ ")' onkeyPress='positiveWholeNumberValidate(this)' maxLength='3' size='3' value='"
						+ optimizeData[i][11]
						+ "' style='text-align:center;'></td> ";
			} else {
				strHTMLTable1 += "		<td id='optRow"
						+ i
						+ "_11' width='45px' class='GridItemRow GridItem' style='height:27px' style='text-align:center;'><font>"
						+ optimizeData[i][11] + "</font></td> ";
			}
			strHTMLTable1 += "		<td id='optRow"
					+ i
					+ "_12' width='45px' class='GridItemRow GridItem' style='height:27px' style='text-align:center;'><font>"
					+ optimizeData[i][12] + "</font></td> ";
			strHTMLTable1 += "       <td id='optRow"
					+ i
					+ "_13' width='25px' class='GridItemRow GridItem' style='height:27px' style='text-align:center;'><font>"
					+ optimizeData[i][13] + "</font></td> ";
			strHTMLTable1 += "       <td id='optRow"
				+ i
				+ "_23' width='25px' class='GridItemRow GridItem' style='height:27px' style='text-align:center;'><font>"
				+ optimizeData[i][42] + "</font></td> ";
			"<\/tr>";

			// second table
			strHTMLTable2 += "<tr id='optRow" + i + "'>";
			if (optimizeData[i][14] != '&nbsp;') {
				strHTMLTable2 += "<td id='optRow"
						+ i
						+ "_14' width='45px' class='GridItemRow GridItem' style='height:27px' align='center'><input type='text' id='txtAllocated"
						+ i
						+ "' name='txtAllocated"
						+ i
						+ "' tabIndex='"
						+ (strTabCount + 4)
						+ "' style='width:30px' size='3' maxLength='3' onkeyUp='positiveWholeNumberValidate(this)' onChange='setPageEdit();changeAllocated("
						+ i
						+ ",txtAllocated"
						+ i
						+ ")' onkeyPress='positiveWholeNumberValidate(this)' value='"
						+ optimizeData[i][14]
						+ "' style='text-align:center;'></td> ";
			} else {
				strHTMLTable2 += "		<td id='optRow"
						+ i
						+ "_14' width='45px' class='GridItemRow GridItem' style='height:27px' ><font>"
						+ optimizeData[i][14] + "</font></td> ";
			}
			if (optimizeData[i][15] != '&nbsp;') {
				if (optimizeData[i][15] == 'OPN') {
					var chkVal = '';
				} else {
					chkVal = 'checked';
				}

				strHTMLTable2 += "<td id='optRow"
						+ i
						+ "_15' width='40px' class='GridItemRow GridItem' style='height:27px' align='center'><input type='checkbox' class='NoBorder' id='chkClosed"
						+ i + "' name='chkClosed" + i + "' tabIndex='"
						+ (strTabCount + 5) + "' onClick='clickedClosed(" + i
						+ ",chkClosed" + i + ")' size='3' " + chkVal
						+ " style='text-align:center;'></td> ";
			} else {
				strHTMLTable2 += "		<td id='optRow"
						+ i
						+ "_15' width='40px' class='GridItemRow GridItem' style='height:27px' ><font>"
						+ optimizeData[i][15] + "</font></td> ";
			}
			if (optimizeData[i][16] != '&nbsp;') {
				if (optimizeData[i][16] == 'true') {
					var isFix = 'Y';
				} else {
					isFix = 'N';
				}
			} else {
				isFix = '&nbsp;';
			}
			strHTMLTable2 += "<td id='optRow"
					+ i
					+ "_16' width='50px' class='GridItemRow GridItem' style='height:27px' style='text-align:center;'><font class='"
					+ strFntcls + "'>" + isFix + "</font></td> ";
			if (optimizeData[i][22] != '&nbsp;') {
				if (optimizeData[i][22] == 'false') {
					var chkVal = '';
				} else {
					chkVal = 'checked';
				}
				strHTMLTable2 += "<td id='optRow"
						+ i
						+ "_22' width='50px' class='GridItemRow GridItem' style='height:27px' align='center'><input type='checkbox' class='NoBorder' id='chkPriority"
						+ i + "' name='chkPriority" + i + "' tabIndex='"
						+ (strTabCount + 5) + "' onClick='clickedPriority(" + i
						+ ",chkPriority" + i + ")' size='3' " + chkVal
						+ " style='text-align:center;'></td> ";
			} else {
				strHTMLTable2 += "		<td id='optRow"
						+ i
						+ "_22' width='50px' class='GridItemRow GridItem' style='height:27px' ><font>&nbsp;</font></td> ";
			}
			strHTMLTable2 += "<td id='optRow"
					+ i
					+ "_17' width='35px' class='GridItemRow GridItem' style='height:27px' style='text-align:center;'><font class='"
					+ strFntcls
					+ "'>"
					+ optimizeData[i][17]
					+ "</font></td> "
					+ "<td id='optRow"
					+ i
					+ "_18' width='45px' class='GridItemRow GridItem' style='height:27px' style='text-align:center;'><font class='"
					+ strFntcls
					+ "'>"
					+ optimizeData[i][18]
					+ "</font></td> "
					+ "<td id='optRow"
					+ i
					+ "_19' width='55px' class='GridItemRow GridItem' style='height:27px' style='text-align:center;'><font class='"
					+ strFntcls
					+ "'>"
					+ optimizeData[i][19]
					+ "</font></td> "
					+ "<td id='optRow"
					+ i
					+ "_20' width='50px' class='GridItemRow GridItem' style='height:27px' style='text-align:center;'><font class='"
					+ strFntcls
					+ "'>"
					+ optimizeData[i][20]
					+ "</font></td> "
					+ "<td id='optRow"
					+ i
					+ "_21' width='55px' class='GridItemRow GridItem' style='height:27px' style='text-align:center;'><font class='"
					+ strFntcls + "'>" + optimizeData[i][21] + "</font></td> "
					+ "<\/tr>";
			strTabCount = strTabCount + 5;
		}

		strHTMLTable1 += "<\/table>";
		strHTMLTable2 += "<\/table>";

		DivWrite("spn1", strHTMLTable1);
		DivWrite("spn2", strHTMLTable2);
		shadeRows();
		//checkOverbooked();
		if (top[1].objTMenu.focusTab == "SC_INVN_005") {
			if (!getFieldByID('txtInfAlloc0').disabled) {
				getFieldByID('txtInfAlloc0').focus();
			}
		}
	}
}

function shadeRows() {
	var colorNo = 0;

	for ( var i = 0; i < optimizeData.length; i++) {
		if (optimizeData[i][2] != '&nbsp;') {
			if (Number(colorNo) == 2) {
				colorNo = 0;
			}
			colorNo = Number(colorNo) + 1;

			if (Number(optimizeData[i][3]) < 0) {
				isMinus = true;
			} else {
				isMinus = false;
			}
		} else {
			colorNo = Number(colorNo);
		}
		shadeColors(i, colorNo, isMinus);
	}
}

function shadeColors(i, colorNo, isMinus) {
	var paintColor = '';

	if (Number(colorNo) == 1) {
		paintColor = 'GridAlternateRow';
	} else {
		paintColor = 'fltStatus02';
	}
	for ( var x = 1; x < 24; x++) {
		setStyleClass('optRow' + i + '_' + x + '', paintColor);
	}

	if (isMinus) {
		if (getFieldByID('txtInfAlloc' + i + '')) {
			getFieldByID('txtInfAlloc' + i + '').disabled = 'true';
		}
		if (getFieldByID('txtOversell' + i + '')) {
			getFieldByID('txtOversell' + i + '').disabled = 'true';
		}
		if (getFieldByID('txtCurtailed' + i + '')) {
			getFieldByID('txtCurtailed' + i + '').disabled = 'true';
		}
		if (getFieldByID('txtAllocated' + i + '')) {
			getFieldByID('txtAllocated' + i + '').disabled = 'true';
		}
		if (getFieldByID('chkClosed' + i + '')) {
			getFieldByID('chkClosed' + i + '').disabled = 'true';
		}
		if (getFieldByID('chkPriority' + i + '')) {
			getFieldByID('chkPriority' + i + '').disabled = 'true';
		}
	}
}

function changeInfAlloc(rowNo, infAlloc) {
	optimizeData[rowNo][33] = 'Y';
	optimizeData[rowNo][34] = 'C';
	getFieldByID('txtInfAlloc' + rowNo + '').style.color = 'red';
	Disable('btnSave', false);
}

function changeOversell(rowNo, overSell) {
	optimizeData[rowNo][33] = 'Y';
	optimizeData[rowNo][35] = 'C';
	getFieldByID('txtOversell' + rowNo + '').style.color = 'red';
	Disable('btnSave', false);
}

function changeCurtailed(rowNo, curtailed) {
	optimizeData[rowNo][33] = 'Y';
	optimizeData[rowNo][36] = 'C';
	getFieldByID('txtCurtailed' + rowNo + '').style.color = 'red';
	Disable('btnSave', false);
}

function changeAllocated(rowNo, allocated) {
	optimizeData[rowNo][33] = 'Y';
	optimizeData[rowNo][37] = 'C';
	getFieldByID('txtAllocated' + rowNo + '').style.color = 'red';
	Disable('btnSave', false);
}

function clickedClosed(rowNo, closed) {
	optimizeData[rowNo][33] = 'Y';
	optimizeData[rowNo][41] = 'M';
	getFieldByID('chkClosed' + rowNo + '').style.color = 'red';
	Disable('btnSave', false);
}

function clickedPriority(rowNo, closed) {
	optimizeData[rowNo][33] = 'Y';
	getFieldByID('chkPriority' + rowNo + '').style.color = 'red';
	Disable('btnSave', false);
}

function blinkIt() {
	if (!document.all) {
		return;
	} else {
		for (i = 0; i < document.all.tags('blink').length; i++) {
			s = document.all.tags('blink')[i];
			s.style.visibility = (s.style.visibility == 'visible') ? 'hidden'
					: 'visible';
		}
	}
}

function checkOverbooked() {
	for ( var i = 0; i < optimizeData.length; i++) {
		if (optimizeData[i][2] != '&nbsp;') {
			var strSoldAduInf = optimizeData[i][8].split("/");
			var strSoldAdu = strSoldAduInf[0];
			var strSoldInf = strSoldAduInf[1];

			var strOnholdAduInf = optimizeData[i][9].split("/");
			var strOnholdAdu = strOnholdAduInf[0];
			var strOnholdInf = strOnholdAduInf[1];

			if ((Number(strSoldAdu) + Number(strSoldInf) + Number(strOnholdAdu) + Number(strOnholdInf)) > Number(optimizeData[i][6])) {
				setStyleClass('optRow' + i + '_f2', 'mandatory');
				setInterval('blinkIt()', 500);
			}
		}
	}
}

function CalculateClick() {
	for ( var i = 0; i < optimizeData.length; i++) {
		if (optimizeData[i][33] == 'Y') {
			if (optimizeData[i][5] != '&nbsp;') {
				var strSoldAduInf = optimizeData[i][8].split("/");
				var strSoldAdu = strSoldAduInf[0];
				var strSoldInf = strSoldAduInf[1];

				var strOnholdAduInf = optimizeData[i][9].split("/");
				var strOnholdAdu = strOnholdAduInf[0];
				var strOnholdInf = strOnholdAduInf[1];

				var strAvaiAduInf = optimizeData[i][12].split("/");
				var strAvaiAdu = strAvaiAduInf[0];
				var strAvaiInf = strAvaiAduInf[1];

				if ((leftTrim(rightTrim(getText('txtInfAlloc' + i + ''))) == '')
						|| Number(getValue('txtInfAlloc' + i + '')) < (Number(strSoldInf) + Number(strOnholdInf))) {
					showERRMessage(infantLess);
					getFieldByID('txtInfAlloc' + i + '').focus();
					return false;
				} else if ((leftTrim(rightTrim(getText('txtOversell' + i + ''))) == '')) {
					showERRMessage(oversellInvaid);
					getFieldByID('txtOversell' + i + '').focus();
					return false;
				} else if (Number(optimizeData[i][10]) > Number(getValue('txtOversell' + i + ''))) {
					if ((Number(optimizeData[i][10]) - Number(getValue('txtOversell' + i + ''))) > Number(strAvaiAdu)) {
						showERRMessage(oversellInvaid);
						getFieldByID('txtOversell' + i + '').focus();
						return false;
					}
				} else if ((leftTrim(rightTrim(getText('txtCurtailed' + i + '')))) == '') {
					showERRMessage(curtaildInvaid);
					getFieldByID('txtCurtailed' + i + '').focus();
					return false;
				} else if (((Number(getValue('txtCurtailed' + i + ''))) - Number(optimizeData[i][11])) > Number(strAvaiAdu)) {
					showERRMessage(curtaildInvaid);
					getFieldByID('txtCurtailed' + i + '').focus();
					return false;
				} else {
					objOnFocus();
					if (optimizeData[i][34] == 'C') {
						// alert('inf alloc changed');
					}
				}
			}
			if (optimizeData[i][13] != '&nbsp;') {
				if ((leftTrim(rightTrim(getText('txtAllocated' + i + ''))) == '')
						|| ((Number(getValue('txtAllocated' + i + '')) + Number(optimizeData[i][20])) < (Number(optimizeData[i][17]) + Number(optimizeData[i][18])))) {
					showERRMessage(bcLess);
					getFieldByID('txtAllocated' + i + '').focus();
					return false;
				} else {
					objOnFocus();
				}
			}
		}
	}
	return true;
}

function SaveClick() {
	var changedRowsVals = '';
	var blnStatusAction = "";

	setPageEdited(false);
	if (!CalculateClick()) {
		return;
	}
	for ( var i = 0; i < optimizeData.length; i++) {
		if (optimizeData[i][33] == 'Y') {

			if (optimizeData[i][25] != '&nbsp;') // Inventory Id 1
			{
				changedRowsVals += optimizeData[i][25] + ',';
			}
			if (optimizeData[i][23] != '&nbsp;') // FlightId 2
			{
				changedRowsVals += optimizeData[i][23] + ',';
			}
			if (optimizeData[i][24] != '&nbsp;') {
				changedRowsVals += optimizeData[i][24] + ','; // 3
			}
			if (optimizeData[i][26] != '&nbsp;') {
				changedRowsVals += optimizeData[i][26] + ','; // 4
			}
			if (document.getElementById('txtInfAlloc' + i + '')) {
				changedRowsVals += getValue('txtInfAlloc' + i + '') + ','; // 5
			} else {
				if (optimizeData[i][28] != '&nbsp;') {
					changedRowsVals += optimizeData[i][28] + ','; // 13
				}
			}
			if (document.getElementById('txtOversell' + i + '')) {
				changedRowsVals += getValue('txtOversell' + i + '') + ','; // 6
			} else {
				if (optimizeData[i][29] != '&nbsp;') {
					changedRowsVals += optimizeData[i][29] + ','; // 14
				}
			}
			if (document.getElementById('txtCurtailed' + i + '')) {
				changedRowsVals += getValue('txtCurtailed' + i + '') + ','; // 7
			} else {
				if (optimizeData[i][30] != '&nbsp;') {
					changedRowsVals += optimizeData[i][30] + ','; // 15
				}
			}
			if (document.getElementById('txtAllocated' + i + '')) {
				changedRowsVals += getValue('txtAllocated' + i + '') + ','; // 8
			}
			if (document.getElementById('chkClosed' + i + '')) {
				changedRowsVals += document
						.getElementById('chkClosed' + i + '').checked + ','; // 9
			}
			if (optimizeData[i][27] != '&nbsp;') {
				changedRowsVals += optimizeData[i][27] + ','; // 10
			}
			if (optimizeData[i][13] != '&nbsp;') {
				changedRowsVals += optimizeData[i][13] + ','; // 11
			}
			if (optimizeData[i][22] != '&nbsp;') {
				changedRowsVals += optimizeData[i][22] + ','; // 12
			}
			if (optimizeData[i][31] != '&nbsp;') {
				changedRowsVals += optimizeData[i][31] + ','; // 13
			}
			if (optimizeData[i][32] != '&nbsp;') {
				changedRowsVals += optimizeData[i][32] + ','; // 14
			}
			if (document.getElementById('chkPriority' + i + '')) {
				changedRowsVals += document
						.getElementById('chkPriority' + i + '').checked + ','; // 16
			}
			blnStatusAction = 'M';
			// manually change check
			if (optimizeData[i][40] != optimizeData[i][41]) {

				if (!document.getElementById('chkClosed' + i + '').checked
						&& optimizeData[i][40] == 'C') {
					blnStatusAction = 'C';
				} else if (!document.getElementById('chkClosed' + i + '').checked
						&& optimizeData[i][40] == 'R') {
					blnStatusAction = 'R';
				} else if (!document.getElementById('chkClosed' + i + '').checked
						&& optimizeData[i][40] == 'B') {
					blnStatusAction = 'B';
				}
			} else {
				blnStatusAction = optimizeData[i][40];
			}
			changedRowsVals += blnStatusAction + ','; // 17 manually changed
			changedRowsVals += optimizeData[i][20] + ','; // 18 Acquired seats
			changedRowsVals += optimizeData[i][42] + ','; // Logical CC
			changedRowsVals = changedRowsVals.substr(0,
					changedRowsVals.length - 1);
			changedRowsVals += ':';
		}
	}
	setField('hdnOptimizeValues', changedRowsVals.substr(0,
			changedRowsVals.length - 1));
	setField("hdnMode", "SAVE");
	document.frmOptimizeGrid.submit();
}

function ExportClick() {
	window
			.open(
					'showOptimize.action?hdnMode=EXPORT',
					'winAdmin',
					'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=no,width=900,height=600,resizable=no,top=1,left=1');
}