var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.buildCalendar();

function winOnLoad(strMsg, strMsgType) {
	setPageEdited(false);
	setField("txtFlightp1", defCarrCode);
	if (top[1].objTMenu.tabGetValue("SC_INVN_004") != ""
			&& top[1].objTMenu.tabGetValue("SC_INVN_004") != null) {
		var strSearch = top[1].objTMenu.tabGetValue("SC_INVN_004").split("@");
		setField("txtFromDate", strSearch[2]);
		setField("txtToDate", strSearch[3]);
		setField("txtFlightp1", strSearch[4].substr(0, 2));
		setField("txtFlightp2", strSearch[4].substr(2, strSearch[4].length));		

		var strOnDs = strSearch[5].split(",");
		var control = document.getElementById("selSegment");

		if (strOnDs != '') {
			for ( var i = 0; i < strOnDs.length; i++) {
				
				var strVal = strOnDs[i];
				var strReplace = strOnDs[i];
				if (strVal.indexOf("***") != -1) {
					strReplace = strVal.replace("***", "All");
				}			
				control.options[control.length] = new Option(strReplace,
						strReplace);
			}
		}

		setField("radAllBc", strSearch[6]);
		if (strSearch[6] != 'C') {
			bcls.disable(true);
		} else {
			bcls.selectedData(strSearch[7]);
		}

		setField("radAvailable", strSearch[8]);
		setField("txtSeats", strSearch[9]);
		setField("selMoreLess", strSearch[10]);

		setField("radSegLevel", strSearch[11]);
		setField("radShowBc", strSearch[13]);
		if (strSearch[13] != 'Agent') {
			Disable("selAgent", "true");
		}
		setField("selAgent", strSearch[15]);
		getFieldByID("selLogicalCC").value = strSearch[17];
	} else {
		setField("radAllBc", "A")
		setField("radShowBc", "BC");
		setField("radAvailable", "AA");
		setField("radSegLevel", "A");
		chkAvailability();
		disableControls();
	}
	if (top[1].objTMenu.focusTab == "SC_INVN_004") {
		getFieldByID("txtFromDate").focus();
	}

	if (strMsg != null && strMsgType != null)
		showCommonError(strMsgType, strMsg);
	top.setFID('SC_INVN_004');
	setField("hdnMode", "SEARCH");
}

function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited("SC_INVN_004", isEdited);
}

function setPageEdit() {
	setPageEdited(true);
}

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFromDate", strDate);
		break;
	case "1":
		setField("txtToDate", strDate);
		break;
	case "3":
		frames["frm_DE"].setDate(strDate, strID);
		break;
	case "4":
		frames["frm_DE"].setDate(strDate, strID);
		break;
	}
}

function LoadCalendar(strID, objEvent) {
	objCal1.ID = strID;
	objCal1.top = 0;
	objCal1.left = 0;
	objCal1.onClick = "setDate";
	objCal1.showCalendar(objEvent);
}

function ContinueClick() {
	if (!searchData())
		return;
	else {
		top[1].objTMenu.tabSetTabInfo("SC_INVN_004", "SC_INVN_005",
				"showOptimize.action", "Optimize", false);
		document.frmOptimize.submit();
	}
}

function objOnFocus() {
	top[2].HidePageMessage();
}

function searchData() {
	setPageEdited(false);
	var validated = false;
	var from = leftTrim(rightTrim(getText("txtFromDate")));
	var to = leftTrim(rightTrim(getText("txtToDate")));
	var flightp1 = leftTrim(rightTrim(getText("txtFlightp1")));
	var flightp2 = leftTrim(rightTrim(getText("txtFlightp2")));
	var seg = getAllSegments().substr(0, getAllSegments().length - 1);
	var standBCs = getValue("radAllBc");
	var showBC = getValue("radShowBc")
	var agents = getValue("selAgent");
	var seats = leftTrim(rightTrim(getText("txtSeats")));
	var selMore = getText("selMoreLess");
	var lsBCs = trim(bcls.getselectedData());
	var logicalCC = getValue("selLogicalCC");

	// seg = "CMB/SHJ";
	setField("txtFlightNo", flightp1 + flightp2);
	setField("hdnBCDisType", standBCs);
	setField("hdnAssignOnD", seg);
	setField("hdnAssignBC", lsBCs);
	setField("selMoreLess", selMore);
	setField("hdnAllocType", getValue("radAvailable"));
	setField("hdnLogicalCC", logicalCC);

	objOnFocus();
	if (top.loadCheck()) {
		if (from == "") {
			showERRMessage(fromdateRqrd);
			getFieldByID("txtFromDate").focus();
			setPageEdited(true);
			return false;
		}
		if (to == "") {
			showERRMessage(todateRqrd);
			getFieldByID("txtToDate").focus();
			setPageEdited(true);
			return false;
		}
		if (from != "" && !dateValidDate(from)) {
			showERRMessage(FromDateIncorrect);
			getFieldByID("txtFromDate").focus();
			setPageEdited(true);
			validated = false;
			return false;
		}
		if (to != "" && !dateValidDate(to)) {
			showERRMessage(ToDateIncorrect);
			getFieldByID("txtToDate").focus();
			setPageEdited(true);
			validated = false;
			return false;
		}
		if (from != "" && !CheckDates(from, to)) {
			showERRMessage(todateLess);
			getFieldByID("txtToDate").focus();
			validated = false;
			setPageEdited(true);
			return false;
		}
		if ((flightp1 == "" || flightp2 == "") && seg == "") {
			showERRMessage(SegmentOrFlightRqd);
			if (flightp2 == "") {
				getFieldByID("txtFlightp2").focus();
			}
			if (flightp1 == "") {
				getFieldByID("txtFlightp1").focus();
			}
			validated = false;
			setPageEdited(true);
			return false;
		}
		if (showBC == "Agent" && agents == "") {
			showERRMessage(AgentRqd);
			getFieldByID("selAgent").focus();
			validated = false;
			setPageEdited(true);
			return false;
		}
		if (standBCs == "C" && lsBCs == "") {
			showERRMessage(BCsReq);
			getFieldByID("lstBcs").focus();
			validated = false;
			setPageEdited(true);
			return false;
		}
		if (getValue("radAvailable") == 'AA' && selMore != "" && seats == "") {
			showERRMessage(SeatRqd);
			validated = false;
			getFieldByID("txtSeats").focus();
			setPageEdited(true);
			return false;
		} else {

			var schcarrier = trim(getVal("txtFlightp1"));
			var blnValid = false;
			for ( var cl = 0; cl < arrCarriers.length; cl++) {
				if (arrCarriers[cl] == schcarrier) {
					blnValid = true;
					break;
				}
			}
			if (schcarrier != "" && !blnValid) {
				showERRMessage(invalidcarrier);
				getFieldByID("txtFlightp1").focus();
				return false;
			}
			var objAgent = document.getElementById('selAgent');
			var strSearchCriteria = getValue("hdnMode") + "@"
					+ getValue("hdnVersion") + "@" + getValue("txtFromDate")
					+ "@" +

					getValue("txtToDate") + "@" + getValue("txtFlightNo") + "@"
					+ getValue("hdnAssignOnD") + "@" +

					getValue("hdnBCDisType") + "@" + getValue("hdnAssignBC")
					+ "@" + getValue("hdnAllocType") + "@"
					+ getValue("txtSeats") + "@" +

					getValue("selMoreLess") + "@" + getValue("radSegLevel")
					+ "@" + getValue("radBCLevel") + "@" +

					getValue("radShowBc") + "@" + getValue("radAgent") + "@"
					+ getValue("selAgent") + "@"
					+ objAgent.options[objAgent.selectedIndex].text + "@"
					+ getValue("selLogicalCC");
			top[1].objTMenu.tabSetValue("SC_INVN_004", strSearchCriteria);
			top[2].ShowProgress();
			return true;
		}
	}
}

function testDate(dt) {
	var validated = false;

	var dtC = new Date();
	var dtCM = dtC.getMonth() + 1;
	var dtCD = dtC.getDate();
	if (dtCM < 10) {
		dtCM = "0" + dtCM
	}
	if (dtCD < 10) {
		dtCD = "0" + dtCD
	}

	var strSysDate = dtCD + "/" + dtCM + "/" + dtC.getFullYear();

	var valid = dateChk(strSysDate);
	if (valid) {
		if (CheckDates(dt, strSysDate)) {
			validated = true;
		}
	}

	return validated;

}

function addToList() {
	var isContained = false;
	top[2].HidePageMessage();
	var dept = getValue("selDeparture");	
	var arr = getValue("selArrival");	
	var via1 = getValue("selVia1");
	var via2 = getValue("selVia2");
	var via3 = getValue("selVia3");
	var via4 = getValue("selVia4");

	if (dept == "") {
		showERRMessage(depatureRequired);
		getFieldByID("selDeparture").focus();

	} else if (arr == "") {
		showERRMessage(arrivalRequired);
		getFieldByID("selArrival").focus();

	} else if (dept == arr) {
		showERRMessage(depatureArriavlSame);
		getFieldByID("selArrival").focus();

	} else if (arr == via1 || arr == via2 || arr == via3 || arr == via4) {
		showERRMessage(arriavlViaSame);
		getFieldByID("selArrival").focus();

	} else if (dept == via1 || dept == via2 || dept == via3 || dept == via4) {
		showERRMessage(depatureViaSame);
		getFieldByID("selDeparture").focus();

	} else if ((via2 != "" || via3 != "" || via4 != "") && via1 == "") {
		showERRMessage(vianotinSequence);
		getFieldByID("selVia1").focus();

	} else if ((via3 != "" || via4 != "") && via2 == "") {
		showERRMessage(vianotinSequence);
		getFieldByID("selVia2").focus();

	} else if ((via4 != "") && via3 == "") {
		showERRMessage(vianotinSequence);
		getFieldByID("selVia3").focus();

	} else if ((via1 != "") && (via1 == via2 || via1 == via3 || via1 == via4)) {
		showERRMessage(viaSame);
		getFieldByID("selVia1").focus();

	} else if ((via2 != "") && (via2 == via3 || via2 == via4)) {
		showERRMessage(viaSame);
		getFieldByID("selVia2").focus();

	} else if ((via3 != "") && (via3 == via4)) {
		showERRMessage(viaSame);
		getFieldByID("selVia3").focus();
	} else {
		var str = "";
		str += dept;

		if (via1 != "") {
			str += "/" + via1;
		}
		if (via2 != "") {
			str += "/" + via2;
		}
		if (via3 != "") {
			str += "/" + via3;
		}
		if (via4 != "") {
			str += "/" + via4;
		}

		str += "/" + arr;

		var control = document.getElementById("selSegment");
		for ( var r = 0; r < control.length; r++) {
			if (control.options[r].text == str) {
				isContained = true;
				showERRMessage(OnDExists);
				break;
			}
		}
		if (isContained == false) {
			control.options[control.length] = new Option(str, str);
			clearStations();
		}
	}
}

function clearStations() {
	getFieldByID("selDeparture").value = '';
	getFieldByID("selArrival").value = '';
	getFieldByID("selVia1").value = '';
	getFieldByID("selVia2").value = '';
	getFieldByID("selVia3").value = '';
	getFieldByID("selVia4").value = '';
}

function removeFromList() {
	var control = document.getElementById("selSegment");

	var selIndex = control.selectedIndex;
	if (selIndex != -1) {
		for (i = control.length - 1; i >= 0; i--) {
			if (control.options[i].selected) {
				control.options[i] = null;
			}
		}
		if (control.length > 0) {
			control.selectedIndex = selIndex == 0 ? 0 : selIndex - 1;
			clearStations();
		}
	}

}

function getAllSegments() {
	var control = document.getElementById("selSegment");
	var values = "";
	for ( var t = 0; t < (control.length); t++) {
		var str = control.options[t].text;
		var strReplace = control.options[t].text;
		if (str.indexOf("All") != -1) {
			strReplace = str.replace("All", "***");
		}
		values += strReplace + ",";		
		//values += control.options[t].text + ",";
	}
	return values;
}

function disableControls() {
	bcls.disable(true);
	Disable("selAgent", "true");
	setField("radNegAvailable", "off");
}

function displayBC() {
	if (getValue("radAllBc") == "C") {
		bcls.disable(false);
	} else {
		bcls.disable(true);
		bcls.moveOptions("<<");
	}
}

function viewAllocations() {

	if (getValue("radShowBc") == "BC") {
		Disable("selAgent", "true");
		setField("selAgent", "");
	} else {
		Disable("selAgent", "")
	}
}

function chkAvailability() {
	if (getValue("radAvailable") == 'AA') {
		Disable("txtSeats", "");
		Disable("selMoreLess", "");
		Disable("radSegLevel", "");
		setField("radSegLevel", "A");
	} else {
		Disable("txtSeats", "true");
		setField("txtSeats", "");
		Disable("selMoreLess", "true");
		Disable("radSegLevel", "true");
		setField("radSegLevel", "");

	}
}

function KPValidate(control) {
	var strCC = getText(control);
	var strLen = strCC.length;
	var blnVal = isAlpha(strCC)
	if (blnVal) {
		setField(control, strCC.substr(0, strLen - 1));
		getFieldByID(control).focus();
	}

}

function KPValidatePositiveInteger(control) {
	var strCC = getText(control);
	var strLen = strCC.length;
	var blnVal = validateInteger(strCC)
	if (!blnVal) {
		setField(control, strCC.substr(0, strLen - 1));
		getFieldByID(control).focus();
	}
}