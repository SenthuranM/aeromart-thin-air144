//JavaScript Document
//Author -Shakir

var strRowData;
var strGridRow;
var intLastRec;
var intStat = 0;
var strExRateGridRowNo = -1;

var screenId = "SC_ADMN_007";
var valueSeperator = "~";
var srchValueSeperator = ":";
var blnBoundary = (currBoundary == 'true') ? true : false;
var arrEditData = new Array();
var vrSyaDate;

var exRateAutomationEnabled = (exRateAutomationStatus == 'true') ? true : false;

var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtDateFrom", strDate);
		break;
	case "1":
		setField("txtDateTo", strDate);
		break;
	case "3":
		setDate(strDate, strID);
		break;
	case "4":
		setDate(strDate, strID);
		break;
	}
}

function LoadCalendar(strID, objEvent) {
	var status = getText("radRateCat");
	if (status == "on") {
		objCal1.ID = strID;
		objCal1.top = 0;
		objCal1.left = 0;
		objCal1.onClick = "setDate";
		objCal1.showCalendar(objEvent);
	}
}

// On window load
function winOnLoad(strMsg, strMsgType) {

	if (exRateAutomationEnabled) {
		document.getElementById('exRateAutomateConfigDiv').style.display = 'block';
	} else {
		document.getElementById('exRateAutomateConfigDiv').style.display = 'none';
	}

	startTime();
	clearControls();
	createPopUps();
	PopUpData();
	disableInputControls();
	setPageEdited(false);
	setField("hdnModel", "DISPLAY");

	if (top[1].objTMenu.focusTab == screenId) {
		getFieldByID("txtCurrencyCodeSearch").focus();
	}
	var strTemp = getTabValues(screenId, valueSeperator, "strSearchCriteria");
	if (strTemp != null && strTemp != "") {
		if (strTemp.indexOf(srchValueSeperator) != -1) {
			var arrTemp = strTemp.split(srchValueSeperator);
			setField("txtCurrencyCodeSearch", arrTemp[0]);
			setField("txtDescriptionSearch", arrTemp[1]);
			setField("selStatus", arrTemp[2]);
			if (arrTemp[3] == "on") {
				getFieldByID("radRateCat").checked = true;
			} else {
				getFieldByID("radRateCat").checked = false;
			}
			setWithoutCharges();
			setField("txtDateFrom", arrTemp[4]);
			setField("txtDateTo", arrTemp[5]);
		}
	}

	if (arrFormData != null && arrFormData.length > 0) {
		strGridRow = getTabValues(screenId, valueSeperator, "strGridRow"); // Get
		// saved
		// grid
		// Row
		setPageEdited(true);
		setField("txtCurrencyCode", arrFormData[0][1]);
		setField("txtDescription", arrFormData[0][2]);
		setField("txtDecimal", arrFormData[0][16]);
		setField("hdnVersion", arrFormData[0][5]);

		if (arrFormData[0][4] == "Active" || arrFormData[0][4] == "active"
				|| arrFormData[0][4] == "on")
			getFieldByID("chkStatus").checked = true;
		else
			getFieldByID("chkStatus").checked = false;
		if (getFieldByID("chkItineraryFareBreakDownEnalbed") != null) {
			if (arrFormData[0][25] == "Y" || arrFormData[0][25] == "y"
					|| arrFormData[0][25] == "on")
				getFieldByID("chkItineraryFareBreakDownEnalbed").checked = true;
			else
				getFieldByID("chkItineraryFareBreakDownEnalbed").checked = false;
		}

		if (blnBoundary) {
			setField("txtBoundary", arrFormData[0][7]);
			setField("txtBreakPoint", arrFormData[0][8]);
		}

		if (arrFormData[0][9] == "Y") {
			getFieldByID("chkIBEVisibililty").checked = true;
		} else {
			getFieldByID("chkIBEVisibililty").checked = false;
		}
		if (arrFormData[0][10] == "Y") {
			getFieldByID("chkXBEVisibililty").checked = true;
		} else {
			getFieldByID("chkXBEVisibililty").checked = false;
		}

		if (arrFormData[0][11] == "Y") {
			getFieldByID("chkCardPayVisibililty").checked = true;

			setField("selPaymentGatewayIbe", arrFormData[0][14]);
			setField("selPaymentGatewayXbe", arrFormData[0][15]);

		} else {
			getFieldByID("chkCardPayVisibililty").checked = false;
			setField("selPaymentGatewayIbe", "");
			setField("selPaymentGatewayXbe", "");
		}

		var orgGridData = arrFormData[0][13].split(",");
		var edtGridData = arrFormData[0][12].split("|");
		if (trim(arrFormData[0][13]) == "")
			orgGridData = new Array();
		if (trim(arrFormData[0][12]) == "")
			edtGridData = new Array();
		setErrorFormGridArray(orgGridData, edtGridData);
		// FIXME - Set Exchange Rate Data

		enableInputControls();
		if (!isAddMode) {
			if (top[1].objTMenu.focusTab == screenId) {
				document.getElementById('txtDescription').focus();
			}
			setField("hdnMode", "EDIT");
			Disable("txtCurrencyCode", true);
		} else {
			if (top[1].objTMenu.focusTab == screenId) {
				document.getElementById('txtCurrencyCode').focus();
			}
			setField("hdnMode", "ADD");
		}
		Disable("btnSave", false);
		Disable("btnExRateAdd", false);
		if (getFieldByID("chkCardPayVisibililty").disabled == true) {
			Disable("selPaymentGatewayIbe", true);
			Disable("selPaymentGatewayXbe", true);
		} else {
			if (getFieldByID("chkCardPayVisibililty").checked == true) {
				Disable("selPaymentGatewayIbe", false);
				Disable("selPaymentGatewayXbe", false);
			} else {
				Disable("selPaymentGatewayIbe", true);
				Disable("selPaymentGatewayXbe", true);
			}
		}

	}
	if (saveSuccess == 1) {
		alert("Record Successfully saved!");
		saveSuccess = 0;
	}

	showCommonError(strMsgType, strMsg);
}

function setErrorFormGridArray(orginData, edtedData) {

	orgData = new Array();
	var edtData = new Array();
	var recs = orginData.length / 14;
	var recindex = 0;
	if (orginData.length > 0) {
		for (var ol = 0; ol < recs; ol++) {
			orgData[ol] = new Array();
			recindex = Number(ol) * 14;
			orgData[ol][0] = orginData[recindex + 0];
			orgData[ol][1] = orginData[recindex + 1];
			orgData[ol][2] = orginData[recindex + 2];
			orgData[ol][3] = orginData[recindex + 3];
			orgData[ol][4] = orginData[recindex + 4];
			orgData[ol][5] = orginData[recindex + 5];
			orgData[ol][6] = orginData[recindex + 6];
			orgData[ol][7] = new Array(orginData[recindex + 7],
					orginData[recindex + 8], orginData[recindex + 9],
					orginData[recindex + 10], orginData[recindex + 11]);
			orgData[ol][8] = orginData[recindex + 12];
			orgData[ol][9] = orginData[recindex + 13];
		}
	}
	arrayClone(orgData, arrEditData);
	recindex = 0;
	var tmpEdt = new Array();
	recs = edtedData.length;
	for (var el = 0; el < recs; el++) {
		tmpEdt = edtedData[el].split("^");
		edtData[el] = new Array();
		edtData[el][0] = tmpEdt[0];
		edtData[el][1] = tmpEdt[3];
		edtData[el][2] = tmpEdt[4];
		edtData[el][3] = tmpEdt[5];
		edtData[el][4] = tmpEdt[6]
		edtData[el][5] = tmpEdt[2];
		edtData[el][6] = tmpEdt[1];
		edtData[el][7] = tmpEdt[7];
		edtData[el][8] = tmpEdt[8]; // Purchase rate
	}
	var editIndex = 0;
	for (var al = 0; al < edtData.length; al++) {
		editIndex = edtData[al][7];
		if (orgData[editIndex]) {
			orgData[editIndex][1] = edtData[al][1];
			orgData[editIndex][2] = edtData[al][2];
			orgData[editIndex][3] = edtData[al][3];
			orgData[editIndex][4] = edtData[al][4];
			orgData[editIndex][5] = edtData[al][5];
			orgData[editIndex][6] = edtData[al][6];
			orgData[editIndex][8] = "CHANGED";
			orgData[editIndex][9] = edtData[al][8]; // Purchase rate
		} else {
			orgData[editIndex] = new Array();
			orgData[editIndex][0] = editIndex;
			orgData[editIndex][1] = edtData[al][1];
			orgData[editIndex][2] = edtData[al][2];
			orgData[editIndex][3] = edtData[al][3];
			orgData[editIndex][4] = edtData[al][4];
			orgData[editIndex][5] = edtData[al][5];
			orgData[editIndex][6] = edtData[al][6];
			orgData[editIndex][7] = new Array("Y", "Y", "Y", "Y", "Y");
			orgData[editIndex][8] = "NEW";
			orgData[editIndex][9] = edtData[al][8]; // Purchase rate
		}
	}
	setExRateData(orgData);
	arrExRateData = orgData;
	for (nl = 0; nl < orgData.length; nl++) {
		if (orgData[nl][8] != "UNCHANGED") {
			strExRateGridRowNo = orgData[nl][0];
			editExRateRow();
		}
	}
	strExRateGridRowNo = -1;
}

function searchCurrency() {
	objOnFocus();
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setField("hdnMode", "SEARCH");
		setField("hdnModel", "");
		setTabValues(screenId, valueSeperator, "strSearchCriteria",
				getText("txtCurrencyCodeSearch") + srchValueSeperator
						+ getText("txtDescriptionSearch") + srchValueSeperator
						+ getValue("selStatus") + srchValueSeperator
						+ getText("radRateCat") + srchValueSeperator
						+ getText("txtDateFrom") + srchValueSeperator
						+ getText("txtDateTo"));
		setTabValues(screenId, valueSeperator, "intLastRec", 1);
		document.forms[0].submit();
		top[2].ShowProgress();
	}
}

// on Grid Row click
function rowClick(strRowNo) {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		clearControls();
		intStat = 1;
		objOnFocus();
		disableInputControls();
		Disable("btnAdd", false);
		strRowData = objDG.getRowValue(strRowNo);
		strGridRow = strRowNo;
		Disable("btnCurrencyFD", false);
		if (arrData[strRowNo][4] == "N") {
			Disable("btnEdit", false);
			if (arrData[strGridRow][14] == "Y") {
				Disable("btnDelete", false);
			}
		}
		setCurrencyForDisplayList();
		setField("txtCurrencyCode", strRowData[0]);
		setField("txtDescription", strRowData[1]);
		if (arrData[strRowNo][5] == "Active") {
			getFieldByID("chkStatus").checked = true;
		} else {
			getFieldByID("chkStatus").checked = false;
		}
		if (getFieldByID("chkItineraryFareBreakDownEnalbed") != null) {
			if (arrData[strRowNo][26] == "Y") {
				getFieldByID("chkItineraryFareBreakDownEnalbed").checked = true;
			} else {
				getFieldByID("chkItineraryFareBreakDownEnalbed").checked = false;
			}
		}

		setField("hdnVersion", arrData[strRowNo][6]);
		if (blnBoundary) {
			setField("txtBoundary", arrData[strGridRow][8]);
			setField("txtBreakPoint", arrData[strGridRow][9]);
		}
		if (arrData[strGridRow][11] == "Y") {
			getFieldByID("chkIBEVisibililty").checked = true;
		} else {
			getFieldByID("chkIBEVisibililty").checked = false;
		}
		if (arrData[strGridRow][10] == "Y") {
			getFieldByID("chkXBEVisibililty").checked = true;
		} else {
			getFieldByID("chkXBEVisibililty").checked = false;
		}

		if (arrData[strGridRow][12] == "Y") {
			getFieldByID("chkCardPayVisibililty").checked = true;
		} else {
			getFieldByID("chkCardPayVisibililty").checked = false;
		}

		// Exchange Rate Maintenance
		var selectedArrExRateData = arrData[strGridRow][13];
		arrayClone(selectedArrExRateData, arrEditData);
		if (selectedArrExRateData != null) {
			setExRateData(selectedArrExRateData);
		}

		if (arrData[strRowNo][17] != null && !(arrData[strRowNo][17] == "")) {

			setField("selPaymentGatewayIbe", arrData[strRowNo][18]);

		} else {
			setField("selPaymentGatewayIbe", "");

		}

		if (arrData[strRowNo][19] != null && !(arrData[strRowNo][19] == "")) {

			setField("selPaymentGatewayXbe", arrData[strRowNo][20]);

		} else {
			setField("selPaymentGatewayXbe", "");

		}
		// setComboValue(arrData[strRowNo][18]);

		if (arrData[strGridRow][23] == "Y") {
			getFieldByID("exrateAutoUpdateStatus").checked = true;
		} else {
			getFieldByID("exrateAutoUpdateStatus").checked = false;
		}

		setField("exrateVariance", arrData[strRowNo][24]);
		setField("txtDecimal", arrData[strRowNo][25]);
		Disable("exrateAutoUpdateStatus", true);
		Disable("exrateVariance", true);

		setPageEdited(false);
	}
}

function setExRateData(selectedArrExRateData) {
	arrExRateData.length = 0;

	if (selectedArrExRateData.length > 0) {
		for (var i = 0; i < selectedArrExRateData.length; i++) {
			arrExRateData[i] = new Array();
			arrExRateData[i][0] = selectedArrExRateData[i][0]; // Index
			arrExRateData[i][1] = selectedArrExRateData[i][1]; // Effective
			// From
			arrExRateData[i][2] = selectedArrExRateData[i][2]; // Effective To
			arrExRateData[i][3] = selectedArrExRateData[i][3]; // Exchange Rate
			if (selectedArrExRateData[i][4] == "ACT") {
				arrExRateData[i][4] = true; // Status
			} else {
				arrExRateData[i][4] = false;
			}
			arrExRateData[i][5] = selectedArrExRateData[i][5]; // Version
			arrExRateData[i][6] = selectedArrExRateData[i][6]; // Exchange Rate
			// Id
			arrExRateData[i][7] = selectedArrExRateData[i][7]; // IsEditable
			arrExRateData[i][8] = selectedArrExRateData[i][8]; // ModificationStatus
			arrExRateData[i][9] = selectedArrExRateData[i][9]; // Purchase Rate
																// /** JIRA:
																// AARESAA-3087*/
		}
		objDGExr.arrGridData = arrExRateData;
		objDGExr.refresh();
		updateExRateCheckBoxes();

		for (var i = 0; i < arrExRateData.length; i++) {
			objDGExr.setDisable(i, 0, true);
			objDGExr.setDisable(i, 1, true);
			objDGExr.setDisable(i, 2, true);
			objDGExr.setDisable(i, 3, true);
			objDGExr.setDisable(i, 4, true);
		}
	} else {
		objDGExr.refresh();
	}
}

function updateExRateCheckBoxes() {
	for (var i = 0; i < arrExRateData.length; i++) {
		if (arrExRateData[i][4] == true) {
			objDGExr.setCellValue(i, 4, "true");
		} else {
			objDGExr.setCellValue(i, 4, "false");
		}
	}
}

// on Add Button click
function addClick() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setPageEdited(false);
		Disable("btnEdit", "false");
		Disable("btnDelete", "false");
		Disable("btnCurrencyFD", true);

		intStat = 0;
		objOnFocus();
		clearControls();
		getFieldByID("chkIBEVisibililty").checked = true;
		getFieldByID("chkXBEVisibililty").checked = true;
		getFieldByID("chkStatus").checked = true;
		if (getFieldByID("chkItineraryFareBreakDownEnalbed") != null) {
			getFieldByID("chkItineraryFareBreakDownEnalbed").checked = true;
		}

		if (getFieldByID("chkCardPayVisibililty").checked == true) {

			getFieldByID("selPaymentGatewayIbe").disabled = false;
			setField("selPaymentGatewayIbe", "");
			getFieldByID("selPaymentGatewayXbe").disabled = false;
			setField("selPaymentGatewayXbe", "");

			if (getFieldByID("chkCardPayVisibililty").disabled == true) {
				getFieldByID("selPaymentGatewayIbe").disabled = true;
				getFieldByID("selPaymentGatewayXbe").disabled = true;
			}

		} else {
			setField("selPaymentGatewayIbe", "");
			getFieldByID("selPaymentGatewayIbe").disabled = true;
			setField("selPaymentGatewayXbe", "");
			getFieldByID("selPaymentGatewayXbe").disabled = true;

		}
		enableInputControls();
		Disable("btnSave", false);
		setField("hdnMode", "ADD");
		setField("hdnModel", "SAVE");
		setField("hdnVersion", "");
		getFieldByID("txtCurrencyCode").focus();

		Disable("btnExRateAdd", false);
	}

	Disable("exrateAutoUpdateStatus", false);
	Disable("exrateVariance", false);
	setField("exrateVariance", "");

}

// on Edit Button CLick
function editClick() {
	objOnFocus();
	if (intStat == 1) {
		if (arrData[strGridRow][4] == "N") {
			enableInputControls();
			Disable("txtCurrencyCode", "false");
			Disable("btnDelete", true);
			Disable("btnEdit", false);
			Disable("btnSave", false);
			Disable("btnCurrencyFD", true);
			getFieldByID("txtDescription").focus();
			Disable("chkItineraryFareBreakDownEnalbed", false);
			
			// Exchange Rate Maintenance
			Disable("btnExRateAdd", false);
			if (getFieldByID("chkCardPayVisibililty").checked == true) {
				getFieldByID("selPaymentGatewayXbe").disabled = false;
				getFieldByID("selPaymentGatewayIbe").disabled = false;

				if (getFieldByID("chkCardPayVisibililty").disabled == true) {
					getFieldByID("selPaymentGatewayIbe").disabled = true;
					getFieldByID("selPaymentGatewayXbe").disabled = true;
				}
			} else {
				setField("selPaymentGatewayIbe", "");
				getFieldByID("selPaymentGatewayIbe").disabled = true;
				setField("selPaymentGatewayXbe", "");
				getFieldByID("selPaymentGatewayXbe").disabled = true;
			}
			setField("hdnMode", "EDIT");
		} else {
			showCommonError("Error", CannotEditBaseCurrency);
		}
		setField("hdnModel", "SAVE");
	} else {
		showCommonError("Error", SelectCurrencytoEdit);
	}

	Disable("exrateAutoUpdateStatus", false);
	Disable("exrateVariance", false);
	Disable("txtDecimal", false);
}

// on Delete Button click
function deleteClick() {
	objOnFocus();
	if (intStat == 1) {
		enableInputControls();
		var strBaseCurrency = strRowData[0];
		if (strBaseCurrency != strBaseCurrencyCode) {
			var strConf = confirm(deleteConf);
			if (strConf == true) {
				setField("hdnModel", "DELETE");

				setTabValues(screenId, valueSeperator, "intLastRec", 1);
				setTabValues(screenId, valueSeperator, "intLastRec", 1); // Delete
				setField("hdnRecNo", getTabValues(screenId, valueSeperator,
						"intLastRec"));
				setTabValues(screenId, valueSeperator, "strSearchCriteria",
						getText("txtCurrencyCodeSearch") + srchValueSeperator
								+ getText("txtDescriptionSearch")
								+ srchValueSeperator + getValue("selStatus")
								+ srchValueSeperator + getText("radRateCat")
								+ srchValueSeperator + getText("txtDateFrom")
								+ srchValueSeperator + getText("txtDateTo"));
				document.forms[0].submit();
				top[2].ShowProgress();
			} else {
				disableInputControls();
				Disable("btnEdit", false);
				Disable("btnCurrencyFD", false);
				Disable("btnDelete", false);
				Disable("btnExRateAdd", false);
			}
		} else {
			showCommonError("Error", CannotDeleteBaseCurrency);
		}
	} else {
		showCommonError("Error", SelectCurrencyToDelete);
	}
}

// on Save Button click
function saveCurrency() {
	var strCode = getText("txtCurrencyCode");
	var strDesc = getText("txtDescription");
	var intRegex = /^\d+$/;
	var srtDecimalPlaces = getText("txtDecimal");

	setField("hdnRecNo", getTabValues(screenId, valueSeperator, "intLastRec"));

	if (strCode == null || strCode == "") {
		showCommonError("Error", CurrencyCodeFieldIsEmpty);
		getFieldByID("txtCurrencyCode").focus();
		// top[2].ShowPageMessage();
		return;
	}
	if (strCode != null && strCode != "") {
		if (getText("txtCurrencyCode").length < 3) {
			getFieldByID("txtCurrencyCode").focus();
			showCommonError("Error", "Currency Code should be in 3 Digits");
			return;
		}
	}
	if (strDesc == "") {
		getFieldByID("txtDescription").focus();
		showCommonError("Error", DescriptionFieldIsEmpty);
		return;
	}

	if (srtDecimalPlaces != null && srtDecimalPlaces != ""
			&& !(intRegex.test(srtDecimalPlaces))) {
		getFieldByID("txtDecimal").focus();
		showCommonError("Error",
				"Invalid Number of Decimal Places for the Currency");
		return;
	}

	if (blnBoundary) {
		var scaleVal = trim(getText("txtBoundary"));
		var bP = trim(getText("txtBreakPoint"));
		if (scaleVal != "" && Number(scaleVal) <= 0) {
			getFieldByID("txtBoundary").focus();
			showCommonError("Error", boundGreater);
			return;
		}
		if (scaleVal != "" && scaleVal == ".") {
			getFieldByID("txtBoundary").focus();
			showCommonError("Error", boundInvalid);
			return;
		}
		if (bP != "" && Number(bP) < 0) {
			getFieldByID("txtBreakPoint").focus();
			showCommonError("Error", bpGreater);
			return;
		}
		if (bP != "" && bP == ".") {
			getFieldByID("txtBreakPoint").focus();
			showCommonError("Error", bpInvalid);
			return;
		}
		if (bP != "" && scaleVal == "") {
			getFieldByID("txtBoundary").focus();
			showCommonError("Error", boundNotEmpty);
			return;
		}
		if (bP == "" && scaleVal != "") {
			getFieldByID("txtBreakPoint").focus();
			showCommonError("Error", bpNotempty);
			return;
		}
		if (Number(bP) > Number(scaleVal)) {
			getFieldByID("txtBreakPoint").focus();
			showCommonError("Error", bpGreater);
			return;
		}
	}
	if (validateCurrencyForDisplay() == false) {
		return false;
	}
	setHdnCurrencyForDisplay();
	// Set Exchange Rate data
	var exRateLength = arrExRateData.length;
	var arrExRateRow;
	var strExRateData = "";
	var arrOldExRateRow;
	for (var i = 0; i < exRateLength; i++) {
		var rnNo = Number(i) + 1;
		arrExRateRow = arrExRateData[i];
		if (i < arrEditData.length) {
			arrOldExRateRow = arrEditData[i];
		} else {
			arrOldExRateRow = new Array();
		}
		if (arrExRateRow[8] != "UNCHANGED") {
			if (strExRateData != "") {
				strExRateData += "|";
			}
			// FIXME - Validate Exchange Rate Data
			// doing the validation over here
			if (trim(arrExRateRow[1]) == "") {
				showCommonError("Error", fromdateblank + " in row " + rnNo);
				return;
			}

			if (trim(arrExRateRow[2]) == "") {
				showCommonError("Error", todateblank + " in row " + rnNo);
				return;
			}

			var vrfrmDate;
			var vrToDate;
			try {
				var arrdt = arrExRateRow[1].split(" ");
				var arryear = arrdt[0].split("/");
				var arrhrs = arrdt[1].split(":");
				vrfrmDate = new Date(Number(arryear[2]),
						Number(arryear[1]) - 1, Number(arryear[0]),
						Number(arrhrs[0]), Number(arrhrs[1]), 00);

				/*
				 * arrdt = sysDate.split(" "); arryear = arrdt[0].split("/");
				 * arrhrs = arrdt[1].split(":"); vrSyaDate = new
				 * Date(Number(arryear[2]), Number(arryear[1])-1,
				 * Number(arryear[0]), Number(arrhrs[0]), Number(arrhrs[1]),
				 * 00);
				 */

				arrdt = arrExRateRow[2].split(" ");
				arryear = arrdt[0].split("/");
				arrhrs = arrdt[1].split(":");
				vrToDate = new Date(Number(arryear[2]), Number(arryear[1]) - 1,
						Number(arryear[0]), Number(arrhrs[0]),
						Number(arrhrs[1]), 00);
			} catch (e) {
				showCommonError("Error", formatincorrect + " in row " + rnNo);
				return;
			}

			if (vrfrmDate.getTime() <= vrSyaDate.getTime()) {
				if (arrOldExRateRow.length < 0
						|| arrOldExRateRow[1] != arrExRateRow[1]) {
					objDGExr.setColumnFocus(i, 0);
					showCommonError("Error", fromless + " in row " + rnNo);
					return;
				}
			}

			if (vrToDate.getTime() <= vrSyaDate.getTime()) {
				showCommonError("Error", toless + " in row " + rnNo);
				return;
			}

			if (vrToDate.getTime() <= vrfrmDate.getTime()) {
				showCommonError("Error", fromdategreater + " in row " + rnNo);
				return;
			}
			// Sales Rate Validations
			if (trim(arrExRateRow[3]) == "") {
				showCommonError("Error", rateblank + " in row " + rnNo);
				return;
			}
			if (trim(arrExRateRow[3]) == 0) {
				showCommonError("Error", ratezero + " in row " + rnNo
						+ " - column 3");
				return;
			}

			if (!currencyValidate(trim(arrExRateRow[3]), 6, 8)) {
				showCommonError("Error", rateInvalid + " in row " + rnNo
						+ " - column 3");
				return;
			}
			/** JIRA : AARESAA -3087 Lalanthi */
			// Purchase rate validations
			var purchaseRateRow = trim(arrExRateRow[9]);
			if (purchaseRateRow != "" && purchaseRateRow != 0) {
				if (!currencyValidate(purchaseRateRow, 6, 8)) {
					showCommonError("Error", rateInvalid + " in row " + rnNo
							+ " - column 4");
					return;
				}
			}
			if (purchaseRateRow == 0) { // JIRA: 3183 (Lalanthi)
				var answer = confirm(ratezero + ' in row ' + rnNo
						+ ' - column 4');
				if (answer)
					return;
				// else continue saving
			}

			// addModType^exRateId^version^effectiveFrom^effectiveTo^exchangeRate^status^index^purchaseRate
			strExRateData += arrExRateRow[8];
			strExRateData += "^" + arrExRateRow[6];
			strExRateData += "^" + arrExRateRow[5];
			strExRateData += "^" + arrExRateRow[1];
			strExRateData += "^" + arrExRateRow[2];
			strExRateData += "^" + arrExRateRow[3];
			if (arrExRateRow[4] == true) {
				strExRateData += "^" + "ACT";
			} else {
				strExRateData += "^" + "INA";
			}
			strExRateData += "^" + arrExRateRow[0];
			/** JIRA : AARESAA -3087 Lalanthi */
			if (arrExRateRow[9] == "") {
				strExRateData += "^" + "-1"; // If the purchase rate is
												// empty, added this flag to
												// identify it from the server
												// side.
			} else {
				strExRateData += "^" + arrExRateRow[9];
			}
		}
	}

	setField("hdnExRateData", strExRateData);
	setField("hdnExRateGrid", arrEditData);

	var strMode = getText("hdnMode");

	if (strMode == "ADD" && strExRateData == "") {
		showCommonError("Error", gridblank);
		return;
	}
	enableInputControls();
	setField("txtDescription", trim(strDesc));
	setField("txtCurrencyCode", trim(getText("txtCurrencyCode")));
	setField("txtDescription", trim(getText("txtDescription")));

	if (getFieldByID("chkCardPayVisibililty").checked == true) {
		var ibe = getValue("selPaymentGatewayIbe");
		var xbe = getValue("selPaymentGatewayXbe");

		if (ibe == "" && xbe == "") {
			showCommonError("Error", ibepg);
			return;
		}

		setField("selPaymentGatewayXbe", xbe);
		setField("selPaymentGatewayIbe", ibe);

	} else {
		setField("selPaymentGatewayXbe", "");
		getFieldByID("selPaymentGatewayXbe").disabled = true;
		setField("selPaymentGatewayIbe", "");
		getFieldByID("selPaymentGatewayIbe").disabled = true;
	}

	setField("hdnModel", "SAVE");
	if (strMode == 'SAVELISTFORDISP')
		setField("hdnModel", "SAVELISTFORDISP");

	getFieldByID("chkCardPayVisibililty").disabled = false;
	/** NAEEM 23AUG2010 AARESAA-4718 */
	getFieldByID("selPaymentGatewayIbe").disabled = false;
	getFieldByID("selPaymentGatewayXbe").disabled = false;

	setTabValues(screenId, valueSeperator, "strGridRow", strGridRow); // Save
	// the
	// grid
	// Row
	setTabValues(screenId, valueSeperator, "strSearchCriteria",
			getText("txtCurrencyCodeSearch") + srchValueSeperator
					+ getText("txtDescriptionSearch") + srchValueSeperator
					+ getValue("selStatus") + srchValueSeperator
					+ getText("radRateCat") + srchValueSeperator
					+ getText("txtDateFrom") + srchValueSeperator
					+ getText("txtDateTo"));
	clearTimeout(objSecs);
	document.forms[0].submit();
	top[2].ShowProgress();
}

var objSecs;
var localTime;
var sysLocDiff;
function startTime() {
	var arrdt = sysDate.split(" ");
	var arryear = arrdt[0].split("/");
	var arrhrs = arrdt[1].split(":");
	vrSyaDate = new Date(Number(arryear[2]), Number(arryear[1]) - 1,
			Number(arryear[0]), Number(arrhrs[0]), Number(arrhrs[1]),
			Number(arrhrs[2]));
	localTime = new Date();
	sysLocDiff = localTime.getTime() - vrSyaDate.getTime();
	objSecs = setInterval("addSeconds()", 1000);
}

var seconds = 0;
function addSeconds() {
	localTime = new Date();
	if (sysLocDiff >= 0) {
		vrSyaDate.setTime(localTime - sysLocDiff);
	} else if (sysLocDiff > 0) {
		vrSyaDate.setTime(localTime + sysLocDiff);
	}

	var clDate = vrSyaDate;
	getFieldByID("spnClock").innerHTML = clDate.toLocaleString();
}
// Reset Button Click
function resetClick() {
	var strMode = getText("hdnMode");
	if (strMode == "ADD") {
		clearControls();
		getFieldByID("chkStatus").checked = true;
		if (getFieldByID("chkItineraryFareBreakDownEnalbed") != null) {
			getFieldByID("chkItineraryFareBreakDownEnalbed").checked = true;
		}
		setPageEdited(false);
		Disable("btnSave", false);
		Disable("btnCurrencyFD", true);
		getFieldByID("txtCurrencyCode").focus();

		Disable("exrateAutoUpdateStatus", false);
		Disable("exrateVariance", false);
		setField("exrateVariance", "");
	}
	if (strMode == "EDIT") {
		if (strGridRow < arrData.length
				&& arrData[strGridRow][1] == getText("txtCurrencyCode")) {
			setField("txtCurrencyCode", arrData[strGridRow][1]);
			setField("txtDescription", arrData[strGridRow][2]);
			if (arrData[strGridRow][5] == "Active") {
				getFieldByID("chkStatus").checked = true;
			} else {
				getFieldByID("chkStatus").checked = false;
			}
			if (getFieldByID("chkItineraryFareBreakDownEnalbed") != null) {
				if (arrData[strGridRow][26] == "Y") {
					getFieldByID("chkItineraryFareBreakDownEnalbed").checked = true;
				} else {
					getFieldByID("chkItineraryFareBreakDownEnalbed").checked = false;
				}
			}
			setField("hdnVersion", arrData[strGridRow][6]);
			if (blnBoundary) {
				setField("txtBoundary", arrData[strGridRow][8]);
				setField("txtBreakPoint", arrData[strGridRow][9]);
			}
			if (arrData[strGridRow][10] == "Y") {
				getFieldByID("chkIBEVisibililty").checked = true;
			} else {
				getFieldByID("chkIBEVisibililty").checked = false;
			}
			if (arrData[strGridRow][11] == "Y") {
				getFieldByID("chkXBEVisibililty").checked = true;
			} else {
				getFieldByID("chkXBEVisibililty").checked = false;
			}

			if (arrData[strGridRow][12] == "Y") {
				getFieldByID("chkCardPayVisibililty").checked = true;

				if (arrData[strGridRow][17] != null
						&& !(arrData[strGridRow][17] == "")) {
					setField("selPaymentGatewayIbe", arrData[strGridRow][18]);
				} else {
					setField("selPaymentGatewayIbe", "");
				}

				if (arrData[strGridRow][19] != null
						&& !(arrData[strGridRow][19] == "")) {
					setField("selPaymentGatewayXbe", arrData[strGridRow][20]);
				} else {
					setField("selPaymentGatewayXbe", "");

				}

				if (getFieldByID("chkCardPayVisibililty").disabled == true) {
					getFieldByID("selPaymentGatewayIbe").disabled = true;
					getFieldByID("selPaymentGatewayXbe").disabled = true;
				}

			} else {
				getFieldByID("chkCardPayVisibililty").checked = false;
				setField("selPaymentGatewayIbe", "");
				setField("selPaymentGatewayXbe", "");
			}

			var selectedArrExRateData = arrData[strGridRow][13];
			if (selectedArrExRateData != null) {
				setExRateData(selectedArrExRateData);
			}

			if (arrData[strGridRow][23] == "Y") {
				getFieldByID("exrateAutoUpdateStatus").checked = true;
			} else {
				getFieldByID("exrateAutoUpdateStatus").checked = false;
			}

			setField("exrateVariance", arrData[strGridRow][24]);
			setField("txtDecimal", arrData[strGridRow][25]);

			setPageEdited(false);
			Disable("btnSave", true);
			Disable("btnDelete", false);
			getFieldByID("txtDescription").focus();
		} else {
			// Grid row not found. Clear it off.
			clearControls();
			disableInputControls();
			setField("hdnModel", "DISPLAY");
			setPageEdited(false);
			buttonSetFocus("btnAdd");
		}
	}
	objOnFocus();
}

// To Handle Paging
function gridNavigations(intRecNo, intErrMsg) {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		objOnFocus();
		if (intRecNo <= 0)
			intRecNo = 1;
		setField("hdnModel", "DISPLAY");
		setTabValues(screenId, valueSeperator, "intLastRec", intRecNo);
		setField("hdnRecNo", getTabValues(screenId, valueSeperator,
				"intLastRec"));
		if (intErrMsg != "") {
			top[2].objMsg.MessageText = "<li> " + intErrMsg;
			top[2].objMsg.MessageType = "Error";
			top[2].ShowPageMessage();
		} else {
			setTabValues(screenId, valueSeperator, "strSearchCriteria",
					getText("txtCurrencyCodeSearch") + srchValueSeperator
							+ getText("txtDescriptionSearch")
							+ srchValueSeperator + getValue("selStatus")
							+ srchValueSeperator + getText("radRateCat")
							+ srchValueSeperator + getText("txtDateFrom")
							+ srchValueSeperator + getText("txtDateTo"));
			document.forms[0].submit();
			top[2].ShowProgress();
		}
	}
}

function objOnFocus() {
	top[2].HidePageMessage();
}

function CCPress() {
	setPageEdited(true);
	objOnFocus();
	var strCC = getText("txtCurrencyCode");
	var blnVal = isAlpha(strCC);
	if (!blnVal) {
		alphaValidate(getFieldByID("txtCurrencyCode"));
		getFieldByID("txtCurrencyCode").focus();
	}
}

function CDSPress() {
	objOnFocus();
	var strCD = getText("txtDescriptionSearch");
	var blnVal = isEmpty(strCD);
	if (blnVal) {
		setField("txtDescriptionSearch", "");
		getFieldByID("txtDescriptionSearch").focus();
	}
	blnVal = isAlphaNumericWhiteSpace(strCD);
	if (!blnVal) {
		alphaNumericValidate(getFieldByID("txtDescriptionSearch"));
		getFieldByID("txtDescriptionSearch").focus();
	}
}

function CDPress() {
	setPageEdited(true);
	objOnFocus();
	var strCD = getText("txtDescription");
	var blnVal = isEmpty(strCD);
	if (blnVal) {
		setField("txtDescription", "");
		getFieldByID("txtDescription").focus();
	}
	blnVal = isAlphaNumericWhiteSpace(strCD);
	if (!blnVal) {
		alphaNumericValidate(getFieldByID("txtDescription"));
		getFieldByID("txtDescription").focus();
	}
}

function CRPress(fieldName) {
	setPageEdited(true);
	objOnFocus();
	var strCR = getText(fieldName);
	var strTemp = strCR.split(".");
	if (strTemp.length > 1) {
		if (strTemp[0].length > 4) {
			strTemp[0] = strTemp[0].substr(0, 4);
			strCR = String(strTemp[0]) + "." + String(strTemp[1]);
		}
	}
	var blnVal = currencyValidate(strCR, 6, 8);
	if (!blnVal) {
		customValidate(getFieldByID(fieldName),
				/[a-zA-Z`!@#$%\s&*?\[\]{}()|\\\/+=:,;^~_-]/g);
		getFieldByID(fieldName).focus();
	} else {
		if (getText(fieldName) != strCR) {
			setField(fieldName, strCR);
		}
	}
}

function chkboxChange() {
	setPageEdited(true);
	objOnFocus();
}

function clearControls() {
	setField("txtCurrencyCode", "");
	setField("txtDescription", "");
	setField("txtDecimal", "");
	getFieldByID("chkStatus").checked = false;
	if (getFieldByID("chkItineraryFareBreakDownEnalbed") != null) {
		getFieldByID("chkItineraryFareBreakDownEnalbed").checked = false;
	}
	if (blnBoundary) {
		setField("txtBoundary", "");
		setField("txtBreakPoint", "");
	}
	getFieldByID("chkIBEVisibililty").checked = false;
	getFieldByID("chkXBEVisibililty").checked = false;
	getFieldByID("chkCardPayVisibililty").checked = false;

	setField("selPaymentGatewayIbe", "");
	setField("selPaymentGatewayXbe", "");

	arrExRateData.length = 0;
	objDGExr.refresh();
}

function disableInputControls() {
	Disable("txtCurrencyCode", true);
	Disable("txtDescription", true);
	Disable("txtDecimal", true);
	getFieldByID("chkStatus").disabled = true;
	if (getFieldByID("chkItineraryFareBreakDownEnalbed") != null) {
		getFieldByID("chkItineraryFareBreakDownEnalbed").disabled = true;
	}
	if (blnBoundary) {
		Disable("txtBoundary", true);
		Disable("txtBreakPoint", true);
	}
	getFieldByID("chkIBEVisibililty").disabled = true;
	getFieldByID("chkXBEVisibililty").disabled = true;
	getFieldByID("chkCardPayVisibililty").disabled = true;

	getFieldByID("selPaymentGatewayIbe").disabled = true;
	getFieldByID("selPaymentGatewayXbe").disabled = true;

	Disable("btnSave", true);
	Disable("btnReset", true);
	Disable("btnEdit", true);
	Disable("btnCurrencyFD", true);
	Disable("btnDelete", true);

	// Exchange Rate Maintenance
	Disable("btnExRateAdd", true);
	Disable("btnExRateEdit", true);
	Disable("btnExRateSplit", true);
}

function enableInputControls() {
	Disable("txtCurrencyCode", false);
	Disable("txtDescription", false);
	Disable("txtDecimal", false);
	getFieldByID("chkStatus").disabled = false;
	if (getFieldByID("chkItineraryFareBreakDownEnalbed") != null) {
		getFieldByID("chkItineraryFareBreakDownEnalbed").disabled = false;
	}
	if (blnBoundary) {
		Disable("txtBoundary", false);
		Disable("txtBreakPoint", false);
	}

	if (blnPayble) {
		getFieldByID("chkCardPayVisibililty").disabled = false;
		getFieldByID("chkIBEVisibililty").disabled = false;
		getFieldByID("chkXBEVisibililty").disabled = false;
	}

	getFieldByID("chkIBEVisibililty").disabled = false;
	getFieldByID("chkXBEVisibililty").disabled = false;

	Disable("btnReset", false);
}

function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
	if (isEdited) {
		Disable("btnSave", false);
		Disable("btnReset", false);
	} else {
		Disable("btnSave", true);
		Disable("btnReset", true);
	}
}

function clickChange() {
	objOnFocus();
	setPageEdited(true);
}

function clickCreditChange() {
	objOnFocus();
	setPageEdited(true);
	if (getFieldByID("chkCardPayVisibililty").checked == true) {
		getFieldByID("selPaymentGatewayIbe").disabled = false;
		getFieldByID("selPaymentGatewayXbe").disabled = false;

		if (getFieldByID("chkCardPayVisibililty").disabled == true) {
			getFieldByID("selPaymentGatewayIbe").disabled = true;
			getFieldByID("selPaymentGatewayXbe").disabled = true;
		}
	} else {
		setField("selPaymentGatewayIbe", "");
		getFieldByID("selPaymentGatewayIbe").disabled = true;
		setField("selPaymentGatewayXbe", "");
		getFieldByID("selPaymentGatewayXbe").disabled = true;
	}
}

function writeRoundTable() {
	var strHTMLText = "";
	strHTMLText += '<table width="100%" border="0" cellpadding="0" cellspacing="2" ID="Table10" align="center">';
	strHTMLText += '<tr>';
	strHTMLText += '	<td width="20%"><font>Rounding Scale</font><\/td>';
	strHTMLText += '	<td><input type="text" name="txtBoundary" id="txtBoundary" size = "10" maxlength="20" onChange="clickChange()" onKeyUp="CRPress(\'txtBoundary\')" onkeyPress="CRPress(\'txtBoundary\')" tabindex="19" align="right">';
	strHTMLText += '	<\/td>';
	strHTMLText += '<\/tr>';
	strHTMLText += '<tr>';
	strHTMLText += '	<td><font>Break Point</font><\/td>';
	strHTMLText += '	<td><input type="text" name="txtBreakPoint" id="txtBreakPoint" size = "10" maxlength="20" onChange="clickChange()" onKeyUp="CRPress(\'txtBreakPoint\')" onkeyPress="CRPress(\'txtBreakPoint\')" tabindex="20" align="right">';
	strHTMLText += '	<\/td>';
	strHTMLText += '<\/tr>';
	strHTMLText += '<\/table>';
	DivWrite("spnRound", strHTMLText);
}
if (blnBoundary)
	writeRoundTable();

// Exchange Rate Maintenance Functionalities Starts From Here

function setWithoutCharges() {
	var rateCat = getText("radRateCat");
	if (rateCat == "on") {
		setField("txtDateFrom", "");
		setField("txtDateTo", "");
		Disable("txtDateFrom", "");
		Disable("txtDateTo", "");
	} else {
		setField("txtDateFrom", "");
		setField("txtDateTo", "");
		Disable("txtDateFrom", true);
		Disable("txtDateTo", true);
	}
}

function exRateGridRowClick(strRowNum) {
	strExRateGridRowNo = strRowNum;
	var strMode = getText("hdnMode");
	if (strMode == "EDIT") {
		Disable("btnExRateEdit", false);
		Disable("btnExRateSplit", false);
	}
}

function addExRateRow() {

	var x = arrExRateData.length;

	arrExRateData[x] = new Array();
	arrExRateData[x][0] = x;
	arrExRateData[x][1] = "";
	arrExRateData[x][2] = "";
	arrExRateData[x][3] = "";
	arrExRateData[x][4] = true;
	arrExRateData[x][5] = "-1";
	arrExRateData[x][6] = "-1";
	arrExRateData[x][7] = new Array("Y", "Y", "Y", "Y", "Y");
	arrExRateData[x][8] = "NEW";
	arrExRateData[x][9] = "";
	/** JIRA : 3087* */
	objDGExr.refresh();
	updateExRateCheckBoxes();

	setPageEdited(true);

	var arrExRateLength = arrExRateData.length;
	if (arrExRateLength > 1) {
		for (var i = 0; i < arrExRateLength; i++) {
			if (arrExRateData[i][8] == "UNCHANGED") {
				objDGExr.setDisable(i, "", true);
			}
		}
	}

	strExRateGridRowNo = -1;
}

function editExRateRow() {
	var i = strExRateGridRowNo;
	var arrEditability = arrExRateData[i][7];

	if (arrEditability[0] == "Y") {
		if (arrEditability[1] == "Y") {
			objDGExr.setDisable(i, 0, false);
		}

		if (arrEditability[2] == "Y") {
			objDGExr.setDisable(i, 1, false);
		}

		if (arrEditability[3] == "Y") {
			objDGExr.setDisable(i, 2, false);
		}

		if (arrEditability[4] == "Y") {
			objDGExr.setDisable(i, 3, false);
		}

		if (arrEditability[3] == "Y") {
			objDGExr.setDisable(i, 4, false);
		}
		if (arrExRateData[i][8] != "NEW")
			arrExRateData[i][8] = "CHANGED";

		setPageEdited(true);

		var arrExRateLength = arrExRateData.length;
		if (arrExRateLength > 1) {
			for (var j = 0; j < arrExRateLength; j++) {
				if (j != strExRateGridRowNo
						&& arrExRateData[j][8] == "UNCHANGED") {
					objDGExr.setDisable(j, "", true);
				}
			}
		}
	} else {
		showCommonError("Error", NotAllowedToEditExRate);
	}
}

function splitExRateRow() {
	var i = strExRateGridRowNo;
	var len = arrExRateData.length;
	var arrEditability = arrExRateData[i][7];

	if (arrEditability[0] == "Y") {
		if (arrEditability[3] == "Y") {
			objDGExr.setDisable(i, 2, false);
		}

		if (arrEditability[4] == "Y") {
			objDGExr.setDisable(i, 3, false);
		}

		var vrfrmDate;
		var curSrvtime;
		var tempSysDate = "";
		var strDateNewEffectiveTo = "";
		var strDateNewEffectiveFrom = "";
		try {
			var curSrvtime = new Date();
			curSrvtime.setTime(vrSyaDate.getTime());

			var arrdt = arrExRateData[i][1].split(" ");
			var arryear = arrdt[0].split("/");
			var arrhrs = arrdt[1].split(":");
			vrfrmDate = new Date(Number(arryear[2]), Number(arryear[1]) - 1,
					Number(arryear[0]), Number(arrhrs[0]), Number(arrhrs[1]),
					00);
		} catch (e) {
			showCommonError("Error", formatincorrect + " in row " + rnNo);
			return;
		}

		if (vrfrmDate.getTime() < curSrvtime.getTime()) {
			var dateNow = vrSyaDate;
			// var strDateNewEffectiveTo = DateToString(dateNow);
			strDateNewEffectiveTo = DateToString(dateNow);
			// var tempSysDate = curSrvtime;
			tempSysDate = curSrvtime;
			tempSysDate.setMinutes(tempSysDate.getMinutes() + 10);
			var curHr = tempSysDate.getHours();
			var curMin = tempSysDate.getMinutes();
			if (curMin < 10) {
				curMin = "0" + curMin;
			}
			strDateNewEffectiveTo += " " + curHr + ":" + curMin;

			var dateTomorrow = vrSyaDate;
			strDateNewEffectiveFrom = DateToString(dateTomorrow);
			tempSysDate.setMinutes(tempSysDate.getMinutes() + 1);
			var curHr = tempSysDate.getHours();
			var curMin = tempSysDate.getMinutes();
			if (curMin < 10) {
				curMin = "0" + curMin;
			}
			strDateNewEffectiveFrom += " " + curHr + ":" + curMin;
		}

		arrExRateData[i][2] = strDateNewEffectiveTo;
		arrExRateData[i][8] = "CHANGED";

		arrExRateData[len] = new Array();
		arrExRateData[len][0] = len;
		arrExRateData[len][1] = strDateNewEffectiveFrom;
		arrExRateData[len][2] = "";
		arrExRateData[len][3] = "";
		arrExRateData[len][4] = true;
		arrExRateData[len][5] = "-1";
		arrExRateData[len][6] = "-1";
		arrExRateData[len][7] = "Y";
		arrExRateData[len][8] = "NEW";
		arrExRateData[len][9] = "";
		/** JIRA : 3087 */

		objDGExr.refresh();
		updateExRateCheckBoxes();

		var arrExRateLength = arrExRateData.length;
		if (arrExRateLength > 1) {
			for (var j = 0; j < arrExRateLength; j++) {
				if (j != strExRateGridRowNo
						&& arrExRateData[j][8] == "UNCHANGED") {
					objDGExr.setDisable(j, "", true);
				}
			}
		}

	} else {
		showCommonError("Error", NotAllowedToEditExRate);
	}
	// startTime(); //JIRA : 3183
}

function checkExRateDate(intRowIndex, intColNo, control) {
	var strEnteredDateTime = control.value;
	var strFormattedDate = "";
	var isValid = false;

	// FIXME - BEGIN - Implement Data+Time validation
	if (strEnteredDateTime != null && strEnteredDateTime.length > 0) {

		strEnteredDateTime = replaceall(strEnteredDateTime, "/", "");
		strEnteredDateTime = replaceall(strEnteredDateTime, "-", "");
		strEnteredDateTime = replaceall(strEnteredDateTime, ".", "");
		strEnteredDateTime = replaceall(strEnteredDateTime, ":", "");
		strEnteredDateTime = replaceall(strEnteredDateTime, " ", "");

		if (strEnteredDateTime.length <= 12) {
			var dateInput = new Date();
			if (trim(strEnteredDateTime) == "") {
				// nothing to do

			} else if (strEnteredDateTime.length <= 2) {
				dateInput.setDate(strEnteredDateTime);
			} else if (strEnteredDateTime.length <= 4) {
				// Reset Date
				dateInput.setMonth(0);
				dateInput.setDate(1);

				dateInput.setMonth(Number(strEnteredDateTime.substr(2)) - 1);
				dateInput.setDate(strEnteredDateTime.substr(0, 2));

			} else if (strEnteredDateTime.length <= 6) {
				// Reset Date
				dateInput.setMonth(0);
				dateInput.setDate(1);

				dateInput.setYear(Number(strEnteredDateTime.substr(4)) + 2000);
				dateInput.setMonth(Number(strEnteredDateTime.substr(2, 2)) - 1);
				dateInput.setDate(strEnteredDateTime.substr(0, 2));
			} else if (strEnteredDateTime.length <= 8) {
				// Reset Date
				dateInput.setMonth(0);
				dateInput.setDate(1);

				dateInput.setYear(strEnteredDateTime.substr(4));
				dateInput.setMonth(Number(strEnteredDateTime.substr(2, 2)) - 1);
				dateInput.setDate(strEnteredDateTime.substr(0, 2));
			} else if (strEnteredDateTime.length = 12) {
				// Reset Date
				dateInput.setMonth(0);
				dateInput.setDate(1);

				dateInput.setYear(strEnteredDateTime.substr(4, 4));
				dateInput.setMonth(Number(strEnteredDateTime.substr(2, 2)) - 1);
				dateInput.setDate(strEnteredDateTime.substr(0, 2));
			}

			var arrColIndex = 1;
			if (strEnteredDateTime.length < 12) {
				if (intColNo == 1) {
					strFormattedDate = DateToString(dateInput);
					strFormattedDate += " 00:00";
				} else {
					strFormattedDate = DateToString(dateInput);
					strFormattedDate += " 23:59";
					arrColIndex = 2;
				}
			} else {
				if (intColNo != 1) {
					arrColIndex = 2;
				}
				strFormattedDate = DateToString(dateInput);
				strFormattedDate += " " + strEnteredDateTime.substr(8, 2) + ":"
						+ strEnteredDateTime.substr(10);
			}

			arrExRateData[intRowIndex][arrColIndex] = strFormattedDate;
			control.value = strFormattedDate;
		}
		isValid = true;
	}
	// FIXME - END

	if (!isValid) {
		control.focus();
	}
	return isValid;
}

function exRateValidateDecimal(objCon, s, f) {
	setPageEdited(true);
	var strText = objCon.value;
	var blnVal = currencyValidate(strText, s, f);

	if (!blnVal) {
		objCon.focus();
	}

	return blnVal;
}

// Haider

function PopUpData() {
	var html = '<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0"> <tr><td> <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" ID="Table4"> <tr><td width="10" height="20"><img src="../../images/form_top_left_corner_no_cache.gif"></td> <td height="20" class="FormHeadBackGround"><img src="../../images/bullet_small_no_cache.gif"> <font class="FormHeader"> Currency Description For Display </font></td> <td width="11" height="20"><img src="../../images/form_top_right_corner_no_cache.gif"></td> </tr><tr><td class="FormBackGround"></td><td class="FormBackGround" valign="top"> <table><tr><td width="663" align="left"><table width="98%" height="105"  border="0" cellpadding="0" cellspacing="0"> <tr align="left" valign="middle" class="fntMedium"> <td width="19%" class="fntMedium"><font>Language</font></td> <td width="33%" align="left" style="padding-bottom:3px; ">';
	html += '<select name="language" id="language"  style="width:150px; "> ';
	html += '<option value="-1"></option>' + languageHTML;
	html += '</select></td> <td width="13%" rowspan="2" align="center" valign="top"><input name="add" type="button" class="Button" id="add" style="width:50px; " title="Add Item"  onclick="addCurrencyForDisplay();" value="&gt;&gt;" /> <input name="remove" type="button" class="Button" id="remove" style="width:50px;" title="Remove Item" onclick="removeCurrencyForDisplay();" value="&lt;&lt;" /></td> <td width="35%" rowspan="2" align="left" valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0"> <tr> <td><select name="translation" size="5" id="translation" style="width:175px; height:100px; "> </select></td> </tr> <tr> </tr> </table></td> </tr> <tr align="left" valign="middle" class="fntMedium"> <td height="67" valign="top" class="fntMedium"><font>Currency Description* </font></td> <td valign="top"><input name="txtCurrencyForDisplayOL" type="text" id="txtCurrencyForDisplayOL" size="32" maxlength="255" /></td> </tr> </table></td> </tr> <tr><td align="top"><span id="spnSta"></span></td></tr> <tr><td align="right"><input type="button" id="btnUpdate" value= "Save" class="Button" onClick="saveCurrencyForDisplay();"> &nbsp;<input type="button" id="btnPopupClose" value= "Close" class="Button" onClick="return POSdisable(true);"></td></tr></table> </td><td class="FormBackGround"></td></tr><tr> <td height="12"><img src="../../images/form_bottom_left_corner_no_cache.gif"></td> <td height="12" class="FormBackGround"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td> <td height="12"><img src="../../images/form_bottom_right_corner_no_cache.gif"></td> </tr></table> </td></tr></table>';

	DivWrite("spnPopupData", html);
}

function POSenable() {
	if (!listForDisplayEnabled)
		return;
	if (getFieldByID("txtDescription").value == ""
			|| getFieldByID("txtDescription").value == null) {
		alert("No currency is selected, please select currency and try again");
		return;
	}
	setField("hdnMode", "SAVELISTFORDISP");
	setField("hdnModel", "SAVELISTFORDISP");
	// setCurrencyForDisplayList();
	showPopUp(1);
}

function POSdisable(bFromClose) {
	if (!listForDisplayEnabled)
		return;
	if (bFromClose) {
		setField("hdnModel", "");
		setField("hdnMode", "");
	}
	hidePopUp(1);
}

function addCurrencyForDisplay() {
	if (!listForDisplayEnabled)
		return;
	if (getValue('txtCurrencyForDisplayOL').length == 0) {
		return false;
	}
	var selLang = getValue("language");
	if (selLang == '-1')
		return false;
	if (findValueInList(selLang, "translation") == true) {
		alert("Currency description for the selected language already exist");
		return false;
	}

	var selLangName = getCurSelectedLabel("language");
	var currencyDesc = getValue("txtCurrencyForDisplayOL");
	addToList("translation", selLangName + "=>" + currencyDesc, selLang);
	setField("txtCurrencyForDisplayOL", "");
	setField("language", "-1");

}
function removeCurrencyForDisplay() {
	if (!listForDisplayEnabled)
		return;
	var currencyList = getFieldByID("translation");
	var selected = currencyList.selectedIndex;
	if (selected != -1) {
		var langCode = currencyList.options[selected].value;
		var label = currencyList.options[selected].text.split("=>");
		setField("language", langCode);
		setField("txtCurrencyForDisplayOL", label[1]);
		currencyList.remove(selected);
	}
}
function saveCurrencyForDisplay() {
	if (!listForDisplayEnabled)
		return;
	if (getValue('txtCurrencyForDisplayOL').length > 0
			&& getValue("language") != '-1') {
		if (confirm("The current edited language is not added to the list, do you want to continue?") == false)
			return false;
	}
	var strMode = getText("hdnMode");
	var currencyList = getFieldByID("translation");
	var size = currencyList.length;
	var langCode;
	var currencyName;
	var label;
	var str = "";
	var size;
	var i = 0;
	var j = 0;
	var newData = new Array();
	var count = 0;
	for (i = 0; i < currencyList.length; i++) {
		langCode = currencyList.options[i].value;
		label = currencyList.options[i].text.split("=>");
		currencyName = label[1];
		size = arrData[strGridRow][16].length;
		var found = false;
		for (j = 0; j < size; j++) {
			if (arrData[strGridRow][16][j][0] == langCode) {
				newData[count++] = new Array(langCode, currencyName,
						arrData[strGridRow][16][j][2],
						arrData[strGridRow][16][j][3]);
				found = true;
				break;
			}
		}
		if (!found) {
			newData[count++] = new Array(langCode, currencyName, "-1", "-1");// langCode,currencyName,id,ver
			// arrData[strGridRow][16][size] = new
			// Array(langCode,currencyName,'-1','-1');//langCode,currencyName,id,ver
		}
	}
	var langCode2;
	var size1 = arrData[strGridRow][16].length;
	var size2 = newData.length;
	for (i = 0; i < size1; i++) {// original list
		found = false;
		for (j = 0; j < size2; j++) {// new data
			langCode = arrData[strGridRow][16][i][0];
			langCode2 = newData[j][0];
			if (langCode == langCode2) {// the lang code exist in the new data
				found = true;
			}
		}
		if (!found) {
			newData[newData.length] = new Array(langCode, "null",
					arrData[strGridRow][16][i][2],
					arrData[strGridRow][16][i][3]);
		}
	}
	arrData[strGridRow][16] = newData;
	saveCurrency();
	return POSdisable(false);
}

function setHdnCurrencyForDisplay() {
	if (!listForDisplayEnabled)
		return;
	var strMode = getText("hdnMode");
	var currencyList = getFieldByID("translation");
	var langCode;
	var currencyName;
	var label;
	var ver;
	var id;
	var str = "";
	var i = 0;
	if (strMode == "ADD") {
		currencyName = getUnicode(getValue("txtDescription"));
		var size2 = getFieldByID("language").length;
		for (i = 1; i < size2; i++) {
			langCode = getFieldByID("language").options[i].value;
			if (str.length > 0)
				str += "|";
			str = str + langCode + "^" + currencyName + "^-1^-1^"
					+ getValue("txtCurrencyCode");
		}
	} else {
		var size = arrData[strGridRow][16].length;
		for (i = 0; i < size; i++) {
			langCode = arrData[strGridRow][16][i][0];
			label = arrData[strGridRow][16][i][1];
			currencyName = getUnicode(label);
			id = arrData[strGridRow][16][i][2];
			ver = arrData[strGridRow][16][i][3];
			if (str.length > 0)
				str += "|";
			str = str + langCode + "^" + currencyName + "^" + id + "^" + ver
					+ "^" + arrData[strGridRow][1];
		}
	}
	setField("hdnCurrencyForDisplay", str);
}
function setCurrencyForDisplayList() {
	if (!listForDisplayEnabled)
		return;

	if (strGridRow >= 0) {
		setField("txtCurrencyForDisplayOL", "");
		setField("language", "-1");

		var currencyList = getFieldByID("translation");
		currencyList.options.length = 0;
		var langCode;
		var currencyName;
		var langName;
		var i = 0;
		for (i = 0; i < arrData[strGridRow][16].length; i++) {
			langCode = arrData[strGridRow][16][i][0];
			currencyName = arrData[strGridRow][16][i][1];
			if (currencyName == "null")
				continue;
			langName = getListLabel("language", langCode);
			addToList("translation", langName + "=>" + currencyName, langCode);
		}
	}
}
function validateCurrencyForDisplay() {
	if (!listForDisplayEnabled)
		return;
	var strMode = getText("hdnMode");
	var size2 = getFieldByID("language").length;
	var valid = true;
	var size1;
	var i;
	var j;
	var langCode;
	var currencyName;
	var found = false;
	if (strMode == "ADD")
		valid = false;
	else {
		size1 = arrData[strGridRow][16].length;
		if (size1 != (size2 - 1))// -1 to ignore the first empty item
			valid = false;
		else {
			for (i = 0; i < size1; i++) {// original list
				currencyName = arrData[strGridRow][16][i][1];
				if (currencyName == "null") {
					valid = false;
					break;
				}
			}
		}
	}
	if (!valid) {
		alert("Currency description for display is missing for all or some languages, original currency description will copied to all languages");
		if (strMode != "ADD") {
			var descr = getValue("txtDescription");
			for (i = 1; i < size2; i++) {// lang list, start from 1 to ignore
											// the first item (empty)
				found = false;
				langCode = getFieldByID("language").options[i].value;
				for (j = 0; j < size1; j++) {
					currencyName = arrData[strGridRow][16][j][1];
					if (currencyName == "null") {
						arrData[strGridRow][16][j][1] = descr;
					}
					if (langCode == arrData[strGridRow][16][j][0]) {
						found = true;
						continue;
					}
				}
				if (!found)
					arrData[strGridRow][16][arrData[strGridRow][16].length] = new Array(
							langCode, descr, "-1", "-1");
			}
		}
	}
	return true;
}

function KPValidateDecimal(control) {
	var strCC = control.value;
	var strLen = strCC.length;
	var blnVal = isLikeDecimal(strCC)
	if (!blnVal) {
		setField(control.id, strCC.substr(0, strLen - 1));
		getFieldByID(control.id).focus();
	}
}
