var recNo = "1";
var strSearch = top[1].objTMenu.tabGetValue("SC_ADMN_005");
if (strSearch != null && strSearch != "") {
	var arrRecNo = strSearch.split("~");
	recNo = arrRecNo[1];
}

var objCol1 = new DGColumn();
objCol1.columnType = "label";
objCol1.width = "5%";
objCol1.arrayIndex = 1;
objCol1.toolTip = "Booking Class";
objCol1.headerText = "BC";
objCol1.itemAlign = "left"
objCol1.sort = "true";

var objCol2 = new DGColumn();
objCol2.columnType = "label";
objCol2.width = "15%";
objCol2.arrayIndex = 2;
objCol2.toolTip = "Booking Class Description";
objCol2.headerText = "Description";
objCol2.itemAlign = "left"

var objCol3 = new DGColumn();
objCol3.columnType = "label";
objCol3.width = "5%";
objCol3.arrayIndex = 3;
objCol3.toolTip = "Class of Service";
objCol3.headerText = "COS";
objCol3.itemAlign = "center"
	
var objCol4 = new DGColumn();
objCol4.columnType = "label";
objCol4.width = "5%";
objCol4.arrayIndex = 29;
objCol4.toolTip = "Logical Cabin Class";
objCol4.headerText = "Logical<br>CC";
objCol4.itemAlign = "center"

var objCol5 = new DGColumn();
objCol5.columnType = "label";
objCol5.width = "7%";
objCol5.arrayIndex = 4;
objCol5.toolTip = "Standard or Non-Standard Booking Class";
objCol5.headerText = "Standard<br>Y/N";
objCol5.itemAlign = "center"

var objCol6 = new DGColumn();
objCol6.columnType = "label";
objCol6.width = "5%";
objCol6.arrayIndex = 5;
objCol6.toolTip = "Nest Rank";
objCol6.headerText = "Nest<br>Rank";
objCol6.itemAlign = "right"

var objCol7 = new DGColumn();
objCol7.columnType = "label";
objCol7.width = "4%";
objCol7.arrayIndex = 6;
objCol7.toolTip = "Fixed or Non-Fixed Booking Class";
objCol7.headerText = "Fixed<br>Y/N";
objCol7.itemAlign = "center"

var objCol8 = new DGColumn();
objCol8.columnType = "label";
objCol8.width = "5%";
objCol8.arrayIndex = 7;
objCol8.toolTip = "Status";
objCol8.headerText = "Status";
objCol8.itemAlign = "center"

var objCol9 = new DGColumn();
objCol9.columnType = "label";
objCol9.width = "6%";
objCol9.arrayIndex = 8;
objCol9.toolTip = "Linked to Fare Rules";
objCol9.headerText = "Linked to Fare Rules";
objCol9.itemAlign = "center"

var objCol10 = new DGColumn();
objCol10.columnType = "label";
objCol10.width = "6%";
objCol10.arrayIndex = 15;
objCol10.toolTip = "Linked to Agents";
objCol10.headerText = "Linked to Agents";
objCol10.itemAlign = "center"

var objCol11 = new DGColumn();
objCol11.columnType = "label";
objCol11.width = "8%";
objCol11.arrayIndex = 16;
objCol11.toolTip = "Linked to BC Inventories";
objCol11.headerText = "Linked to BC Inventories";
objCol11.itemAlign = "center"

var objCol12 = new DGColumn();
objCol12.columnType = "label";
objCol12.width = "7%";
objCol12.arrayIndex = 18;
objCol12.toolTip = "Charge Group";
objCol12.headerText = "Charge Group";
objCol12.itemAlign = "left"

var objCol13 = new DGColumn();
objCol13.columnType = "label";
objCol13.width = "7%";
objCol13.arrayIndex = 17;
objCol13.toolTip = "BC Type";
objCol13.headerText = "BC Type";
objCol13.itemAlign = "left"

var objCol14 = new DGColumn();
objCol14.columnType = "label";
objCol14.width = "7%";
objCol14.arrayIndex = 22;
objCol14.toolTip = "Allocation Type";
objCol14.headerText = "Allocation<br> Type";
objCol14.itemAlign = "center"

var objCol15 = new DGColumn();
objCol15.columnType = "label";
objCol15.width = "4%";
objCol15.arrayIndex = 23;
objCol15.toolTip = "Pax Category";
objCol15.headerText = "Pax<br>Cat.";
objCol15.itemAlign = "center"

// ---------------- Grid
var objDG = new DataGrid("spnBkgClass");
objDG.addColumn(objCol1);
objDG.addColumn(objCol2);
objDG.addColumn(objCol3);
objDG.addColumn(objCol4);
objDG.addColumn(objCol5);
objDG.addColumn(objCol6);
objDG.addColumn(objCol7);
objDG.addColumn(objCol8);
objDG.addColumn(objCol14);
objDG.addColumn(objCol9);
objDG.addColumn(objCol10);
objDG.addColumn(objCol11);
objDG.addColumn(objCol12);
objDG.addColumn(objCol13);
objDG.addColumn(objCol15);

objDG.width = "100%";
objDG.height = "90px";
objDG.headerBold = false;
objDG.rowSelect = true;
objDG.arrGridData = a;
objDG.seqNo = true;
objDG.seqStartNo = recNo;
objDG.seqNoWidth = "4%";
objDG.pgnumRecTotal = totalRecords; // remove as per return record size
objDG.paging = true;
objDG.pgnumRecPage = 20;
objDG.pgonClick = "gridNavigations";
objDG.rowClick = "rowClick";
objDG.displayGrid();

function RecMsgFunction(intRecNo, intErrMsg) {
	top[2].HidePageMessage();
	if (intErrMsg != "") {
		top[2].objMsg.MessageText = intErrMsg;
		top[2].objMsg.MessageType = "Error";
		top[2].ShowPageMessage();
	}
}

function ViewFareClass(strID) {
	setVisible("spnFC", true);
}
