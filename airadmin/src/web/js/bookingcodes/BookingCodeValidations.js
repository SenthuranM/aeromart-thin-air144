/*
 * Modified on 	02/04/08
 */
var strRowData;
var strGridRow;
var intLastRec;
var screenId = "SC_ADMN_005";
var FARE_CAT_ID = 'R';
var relTimeInValid = false;
var relTimeGreater = false;

function closeClick() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setPageEdited(false);
		top[1].objTMenu.tabRemove(screenId);
	}
}

function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}

function required_changed() {
	setPageEdited(true);
}

function clearError() {
	top[2].HidePageMessage();
}

function winOnLoad(strMsg, strMsgType) {
	clearInputControls();
	disableInputControls(true);
	setPageEdited(false);
	Disable("btnEdit", true);
	
	if(top.logicalCCVisibility ==  "false"){
		document.getElementById('logicalCCData').style.visibility = 'hidden';
	}
	
	if (isSuccessfulSaveMessageDisplayEnabled) {
		if (isSaveTransactionSuccessful) {
			alert("Record Successfully saved!");
		}
	}
	if (strBookingCode != null) {
		selectChange(strBookingCode, "selBookingClass");
	}
	if (strCOS != null) {
		selectChange(strCOS, "selCOS");
	}
	if (strLogicalCC != null) {
		selectChange(strLogicalCC, "selLCC");
	}
	if (strAllocType != null) {
		selectChange(strAllocType, "selAllocSearch");
	}

	if (strStatus != null) {
		selectChange(strStatus, "selStatusSearch");
	}
	if (strBookingCode != null && strBookingCode == "-1") {
		Disable("selCOS", false);
		Disable("selBCType", false);
		Disable("selAllocSearch", false);
		Disable("selLCC",false);
	} else {
		Disable("selCOS", true);
		Disable("selBCType", true);
		Disable("selAllocSearch", true);
		Disable("selLCC",true);
	}
	if (strMsg != null && strMsgType != null)
		showCommonError(strMsgType, strMsg);

	getFieldByID("selBookingClass").focus();
	setFieldsAfterOp();

	// resetting the form field values if exception occurred
	if (arrFormData != null && arrFormData.length > 0) {
		setPageEdited(true);
		setField("txtBookingClass", arrFormData[0][1]);
		setField("txtBookingClassDescription", arrFormData[0][2]);
		if (arrFormData[0][3] == "on")
			getFieldByID("chkStatus").checked = true;
		else
			getFieldByID("chkStatus").checked = false;
		setField("selClassofService", arrFormData[0][4]);
		setField("selLogicalCC", arrFormData[0][23]);
		if (arrFormData[0][5] == "on")
			getFieldByID("chkStandardClass").checked = true;
		else
			getFieldByID("chkStandardClass").checked = false;
		setField("txtNestRank", arrFormData[0][6]);
		if (arrFormData[0][7] == "on")
			getFieldByID("chkNestShift").checked = true;
		else
			getFieldByID("chkNestShift").checked = false;
		setField("txtRulesComments", arrFormData[0][8]);
		if (arrFormData[0][9] == "on")
			getFieldByID("chkFixedClass").checked = true;
		else
			getFieldByID("chkFixedClass").checked = false;
		disableInputControls(false);
		setField("hdnMode", arrFormData[0][11]);
		if (arrFormData[0][11] == "UPDATE") {
			Disable("txtBookingClass", true);
			Disable("sltBCType", true);
			Disable("selAllocationType", true);
			setField("hdnVersion", arrFormData[0][12]);
		}

		setField("sltBCType", arrFormData[0][13]);
		ls.selectedData(String(arrFormData[0][14]));
		setField("selAllocationType", arrFormData[0][16]);
		setField("selPaxCat", arrFormData[0][17]);
		setField("selFareCat", arrFormData[0][18]);
		setField("hdnRowNum", arrFormData[0][15]);
		//setGDSPublishingDetails(arrFormData[0][21]));
		setField("hdnGroupId", a[0][30]);

		// release time
		setField("txtCutOverTime", arrFormData[0][19]);
		if (arrFormData[0][20] == "on")
			getFieldByID("chkReleaseTime").checked = true;
		else
			getFieldByID("chkReleaseTime").checked = true;
		if (arrFormData[0][22] == "on")
			getFieldByID("chkOnHold").checked = true;
		else
			getFieldByID("chkOnHold").checked = false;
	}
}

/** sets GDSPublishingDetails from an Array */
function setGDSPublishingDetails(arrCode) {
	if (getFieldByID("selGDSPublishing")) {
		var intCount = getFieldByID("selGDSPublishing").length;
		for ( var agl = 0; agl < arrCode.length; agl++) {
			for ( var i = 0; i < intCount; i++) {
				if (trim(getFieldByID("selGDSPublishing").options[i].value) == trim(arrCode[agl])) {
					getFieldByID("selGDSPublishing").options[i].selected = true;
					break;
				}
			}
		}
	}

}

function listLogicalCabinClasses(ccWiseLogicalCabinClassList, selCOS, selLCC) {
	
	var selectedCC = getText(selCOS);
	if (selectedCC != "-1") {
		if (ccWiseLogicalCabinClassList != null) {
			var cabinClassWiseStrs = ccWiseLogicalCabinClassList.split("~");
			for (var i = 0; i < cabinClassWiseStrs.length; i++) {
				if (cabinClassWiseStrs[i] != null && cabinClassWiseStrs[i] != "") {
					var ccAndLCCs = cabinClassWiseStrs[i].split(":");
					if (ccAndLCCs != null && ccAndLCCs.length > 1) {
						var cabinClass = ccAndLCCs[0];
						if (cabinClass == selectedCC) {
							var objSelect = getFieldByID(selLCC);
							removeLCCOptionList(selLCC);
							var logicalCCAndDesList = ccAndLCCs[1].split(",");
							for (var p = 0; p < logicalCCAndDesList.length; p++) {
								var objOption = document.createElement("option");
								if (logicalCCAndDesList[p] != null && logicalCCAndDesList[p] != "") {
									var lCCAndDes = logicalCCAndDesList[p].split("-");
									objOption.text = lCCAndDes[1];
									objOption.value = lCCAndDes[0];
									if(document.all && !window.opera) {
										objSelect.add(objOption);
									} else {
										objSelect.add(objOption, null);
									}
								}
							}
						}
					}
				}
			}
		}
	} else {
		var objSelect = getFieldByID(selLCC);
		removeLCCOptionList(selLCC);
		var objOption = document.createElement("option");
		objOption.text = "All";
		objOption.value = "-1";
		if(document.all && !window.opera) {
			objSelect.add(objOption);
		} else {
			objSelect.add(objOption, null);
		}
	}
}

function removeLCCOptionList(selLCC) {
	var elSel = document.getElementById(selLCC);
	for (var i = elSel.length - 1; i>=0; i--) {
	  	elSel.remove(i);
	}
}

function selectChange(strValue, strControl) {
	setField(strControl, strValue);
}

// on Grid Row click
function rowClick(strRowNo) {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setPageEdited(false);
		clearInputControls();
		strRowData = objDG.getRowValue(strRowNo);
		strGridRow = strRowNo;

		setField("txtBookingClass", strRowData[0]);
		setField("txtBookingClassDescription", strRowData[1]);
		setField("selClassofService", a[strRowNo][3]);
		setField("selLogicalCC", a[strRowNo][29]);
		if (strRowData[4] == "true" || strRowData[4] == "Y") {
			getFieldByID("chkStandardClass").checked = true;
			Disable("txtNestRank", false);
		} else {
			getFieldByID("chkStandardClass").checked = false;
			Disable("txtNestRank", true);
		}
		setField("txtNestRank", strRowData[5]);
		if (strRowData[6] == "true" || strRowData[6] == "TRUE"
				|| strRowData[6] == "Y") {
			getFieldByID("chkFixedClass").checked = true;
		} else {
			getFieldByID("chkFixedClass").checked = false;
		}
		if (strRowData[7] == "Active" || strRowData[7] == "ACTIVE") {
			getFieldByID("chkStatus").checked = true;
		} else {
			getFieldByID("chkStatus").checked = false;
		}
		getFieldByID("chkNestShift").checked = false;
		setField("txtRulesComments", a[strRowNo][9]);
		setField("hdnPAXType", a[strRowNo][10]);
		setField("hdnVersion", a[strRowNo][11]);

		//
		setField("txtCutOverTime", a[strRowNo][25]);
		if (a[strRowNo][26] == "true" || a[strRowNo][26] == "TRUE"
				|| a[strRowNo][26] == "Y") {
			getFieldByID("chkReleaseTime").checked = true;
		} else {
			getFieldByID("chkReleaseTime").checked = false;
		}

		if (a[strRowNo][28] == "true" || a[strRowNo][28] == "TRUE"
				|| a[strRowNo][28] == "Y") {
			getFieldByID("chkOnHold").checked = true;
		} else {
			getFieldByID("chkOnHold").checked = false;
		}

		disableInputControls(true);
		Disable("btnEdit", false);
		Disable("btnDelete", false);

		if (a[strRowNo][8] == "Y" || a[strRowNo][16] == "Y") {
			Disable("btnDelete", true);
		}
		if (a[strRowNo][10] == 'GOSHOW' || a[strRowNo][10] == 'INTERLINE') {
			Disable("btnEdit", true); // disable go show or interline editing
			Disable("btnDelete", true); // disable go show or interline deleting
		}
		if (a[strRowNo][17] == 'OPENRT' || a[strRowNo][17] == 'INTLNFS') {
			Disable("btnEdit", true); // disable open return/interline free sell editing
			Disable("btnDelete", true); // disable open return/interline free sell deleting
		}
		// DISABLE INPUT SESSION
		setField("hdnMode", "");
		ls.selectedData(String(a[strRowNo][19]));
		setField("sltBCType", a[strRowNo][17]);
		setField("selAllocationType", a[strRowNo][22]);
		setField("selPaxCat", a[strRowNo][23]);
		setField("selFareCat", a[strRowNo][24]);
		setGDSPublishingDetails(a[strRowNo][27]);
		setField("hdnRowNum", strRowNo);
		setField("hdnGroupId", a[strRowNo][30]);
		arrFormData[0] = a[strRowNo];
	}
}

function disableInputControls(disabled) {
	Disable("btnDelete", disabled);
	Disable("btnSave", disabled);
	Disable("btnReset", disabled);

	Disable("txtBookingClass", disabled);
	Disable("txtBookingClassDescription", disabled);
	Disable("chkStatus", disabled);
	Disable("selClassofService", disabled);
	Disable("selLogicalCC", disabled);
	Disable("chkStandardClass", disabled);
	Disable("txtNestRank", disabled);
	Disable("chkNestShift", disabled);

	Disable("txtRulesComments", disabled);
	Disable("chkFixedClass", disabled);
	Disable("sltChargeGroup", disabled);
	ls.disable(disabled);
	Disable("sltBCType", disabled);
	Disable("selAllocationType", disabled);
	Disable("selPaxCat", disabled);
	Disable("selGDSPublishing", disabled); // Haider
	Disable("selFareCat", disabled);
	Disable("txtCutOverTime", disabled);
	Disable("chkReleaseTime", disabled);
	Disable("chkOnHold", disabled);
}

function resetBookingClass() {
	var strMode = getText("hdnMode");
	var rowNum = getText("hdnRowNum");
	if (strMode == "ADD") {
		clearInputControls();
		getFieldByID("chkStatus").checked = "true";
		getFieldByID("txtBookingClass").focus();
		setField("hdnGroupId", '0');
	} else if (strMode == "UPDATE" && a[rowNum] == arrFormData[0]) {
		getFieldByID("txtBookingClassDescription").focus();
		setField("txtBookingClass", strRowData[0]);
		setField("txtBookingClassDescription", strRowData[1]);
		setField("selClassofService", strRowData[2]);
		setField("selLogicalCC", strRowData[3]);
		if (strRowData[4] == "Y") {
			getFieldByID("chkStandardClass").checked = true;
		} else {
			getFieldByID("chkStandardClass").checked = false;
		}
		setField("txtNestRank", strRowData[5]);
		if (strRowData[6] == "Y") {
			getFieldByID("chkFixedClass").checked = true;
		} else {
			getFieldByID("chkFixedClass").checked = false;
		}
		if (strRowData[7] == "Active") {
			getFieldByID("chkStatus").checked = true;
		} else {
			getFieldByID("chkStatus").checked = false;
		}
		setField("txtCutOverTime", a[rowNum][25]);
		if (a[rowNum][26] == "true" || a[rowNum][26] == "TRUE"
				|| a[rowNum][26] == "Y") {
			getFieldByID("chkReleaseTime").checked = true;
		} else {
			getFieldByID("chkReleaseTime").checked = false;
		}
		setField("txtRulesComments", a[rowNum][9]);
		setField("sltBCType", a[rowNum][17]);
		ls.selectedData(String(a[rowNum][19]));
		setField("selAllocationType", strRowData[21]);
		setField("selPaxCat", strRowData[22]);
		setField("selFareCat", strRowData[23]);
		setGDSPublishingDetails(a[rowNum][27]);
		if (a[rowNum][28] == "true" || a[rowNum][28] == "TRUE"
				|| a[rowNum][28] == "Y") {
			getFieldByID("chkOnHold").checked = true;
		} else {
			getFieldByID("chkOnHold").checked = false;
		}

		setField("hdnGroupId", a[rowNum][30]);
	} else {
		clearInputControls();
		getFieldByID("chkStatus").checked = "true";
		getFieldByID("txtBookingClassDescription").focus();
		setField("hdnGroupId", '0');
	}
	objOnFocus();
}

function clearInputControls() {
	ls.removeAllFromListbox();
	setField("txtBookingClass", "");
	setField("txtBookingClassDescription", "");
	setField("chkStatus", "");
	setField("chkStandardClass", "");
	setField("txtNestRank", "");
	setField("chkNestShift", "");
	setField("txtRulesComments", "");
	setField("chkFixedClass", "");
	setField("selAllocationType", "");
	setField("sltBCType", "");
	setField("selPaxCat", "");
	setField("selGDSPublishing", "");// Haider
	setField("selFareCat", "N");
	setField("txtCutOverTime", "");
	setField("chkReleaseTime", "");
	getFieldByID("chkOnHold").checked = false;
}

function validateBookingClass() {
	var nestRank = getText("txtNestRank");
	var intNestRank = isNaN(nestRank);
	if (validateTextField(getValue("txtBookingClass"), bookingClassIdRqrd)) {
		getFieldByID("txtBookingClass").focus();
		return;
	}
	if (validateTextField(getValue("txtBookingClassDescription"),
			bookingClassDesRqrd)) {
		getFieldByID("txtBookingClassDescription").focus();
		return;
	}
	if (validateTextField(getValue("selClassofService"), classOfServiceRqrd)) {
		getFieldByID("selClassofService").focus();
		return;
	}
	if (validateTextField(getValue("selLogicalCC"), logicalCCRqrd)) {
		getFieldByID("selLogicalCC").focus();
		return;
	}
	if (getFieldByID("chkStandardClass").checked) {
		if (getFieldByID("txtNestRank").value == null
				|| getFieldByID("txtNestRank").value == ""
				|| getFieldByID("txtNestRank").value == "null") {
			setField("txtNestRank", "");
			showCommonError("Error", standardClassRqrd);
			getFieldByID("txtNestRank").focus();
			return;
		}
	}
	if (getFieldByID("chkNestShift").checked) {
		if (getFieldByID("txtNestRank").value == null
				|| getFieldByID("txtNestRank").value == ""
				|| getFieldByID("txtNestRank").value == "null") {
			setField("txtNestRank", "");
			showCommonError("Error", nestRankBlank);
			getFieldByID("txtNestRank").focus();
			return;
		}
	}
	if (nestRank != "" && intNestRank) {
		showCommonError("Error", nestRankNaN);
		getFieldByID("txtNestRank").focus();
		return;
	}
	if (getFieldByID("sltBCType").disabled != true) {
		if (validateTextField(getValue("sltBCType"), BCTypeRqrd)) {
			getFieldByID("sltBCType").focus();
			return;
		}
	}
	if (getFieldByID("selAllocationType").disabled != true) {
		if (validateTextField(getValue("selAllocationType"), ALLOCTypeRqrd)) {
			getFieldByID("selAllocationType").focus();
			return;
		}
	}
	
	if (getFieldByID("txtCutOverTime").disabled != true) {
		if (getFieldByID("txtCutOverTime").value != ""
				&& !isTimeFormatValid("txtCutOverTime",getFieldByID("txtCutOverTime").value)) {
			
			if(relTimeGreater) {
				showCommonError("Error", rlseTimeGreater);
			}			
			if(relTimeInValid) {
				showCommonError("Error", rlseTimeInvalid);
			} 
				

			getFieldByID("txtCutOverTime").focus();
			return false;
		}
	}
	return true;
}

function saveBookingClass() {
	var bcDesc;
	var newBcDesc;
	var fareCategory;
	var remCharIndex = 0;

	setField("hdnCCOdes", ls.getselectedData());
	if (!validateBookingClass())
		return;
	else {
		if (isAddMode != null) {
			if (isAddMode == "Add") {
				setField("hdnMode", "ADD");
			} else if (isAddMode == "Edit") {
				setField("hdnMode", "UPDATE");

				/*
				 * Remove special character from the Booking Class Description
				 * when BC change from Restricted to other type
				 */
				fareCategory = getFieldByID("selFareCat").value;
				if (fareCategory != FARE_CAT_ID) {
					bcDesc = getText("txtBookingClassDescription");
					newBcDesc = removeInvalids(bcDesc);
					setField("txtBookingClassDescription", newBcDesc)
				}
			}
			var tempName = trim(getText("txtRulesComments"));
			tempName = replaceall(tempName, "'", "`");
			tempName = replaceEnter(tempName);
			setField("txtRulesComments", tempName);
			disableInputControls(false);
			setField("hdnRecNo", "1");
			setDataToSearchCriteria();
			document.forms[0].submit();
			top[2].ShowProgress();
		}
	}
}

// on Delete Button click
function deleteBookingClass() {

	if (getFieldByID("txtBookingClass").value == ""
			|| getFieldByID("txtBookingClass").value == null) {
		showCommonError("Error", selectaRow);
	} else {
		var confirmStr = confirm("Selected record will be Deleted. Do you wish to Continue?")
		if (!confirmStr)
			return;

		setField("hdnRecNo", "1");
		setDataToSearchCriteria();
		disableInputControls(false);
		setField("txtBookingClass", strRowData[0]);
		setField("hdnMode", "DELETE");
		document.forms[0].submit();
		disableInputControls(true);
	}
}

function editBookingClass() {

	// linked to fare rule or linked to BC inventories
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		top[2].HidePageMessage();
		isAddMode = "Edit";
		if (a[strGridRow][8] == "Y" || a[strGridRow][16] == "Y") {
			disableInputControls(true);
			Disable("txtBookingClassDescription", false);
			Disable("chkStatus", false);
			Disable("txtRulesComments", false);
			Disable("btnSave", false);
			Disable("btnReset", false);
			Disable("sltBCType", true);
			Disable("txtCutOverTime", false);
			Disable("chkReleaseTime", false);
			Disable("chkOnHold", false);
		} else {
			disableInputControls(false);
		}
		Disable("txtBookingClass", true);
		Disable("selGDSPublishing", true);
		getFieldByID("txtBookingClassDescription").focus();
		if (getFieldByID("chkStandardClass").checked) {
			Disable("txtNestRank", false);
			Disable("chkNestShift", false);
		} else {
			Disable("txtNestRank", true);
			Disable("chkNestShift", true);
		}
		if (a[strGridRow][6] != "Y") {
			Disable("txtCutOverTime", true);
			Disable("chkReleaseTime", true);
		}
		ls.disable(false);
		setPageEdited(false);
		setField("hdnMode", "UPDATE");
	}
}

function addBookingClass() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		isAddMode = "Add";
		top[2].HidePageMessage();
		setPageEdited(false);
		disableInputControls(false);
		Disable("txtNestRank", true);
		setField("hdnMode", "ADD");
		clearInputControls();
		getFieldByID("txtBookingClass").focus();
		getFieldByID("chkStatus").checked = "true";
		Disable("btnEdit", true);
		Disable("btnDelete", true);
		setField("sltBCType", "NORMAL");
		setField("selFareCat", "N");
		getFieldByID("chkOnHold").checked = false;
		Disable("txtCutOverTime", true);
		Disable("selGDSPublishing", true);
		setField("hdnGroupId", '0');
	}
}

function disableNestRank(objCheck) {
	if (objCheck.checked) {
		setPageEdited(false);
		Disable("txtNestRank", false);
		Disable("chkFixedClass", false);
		setField("chkFixedClass", false);
		Disable("chkFixedClass", true);
		getFieldByID("chkNestShift").checked = false;
		Disable("chkNestShift", false);
		setField("txtCutOverTime", "");
		Disable("txtCutOverTime", true);
	} else {
		Disable("txtNestRank", true);
		Disable("chkFixedClass", false);
		getFieldByID("chkNestShift").checked = false;
		setField("txtNestRank", "");
		Disable("chkNestShift", true);
	}
}

function disableStandardClass(objCheck) {
	getFieldByID("chkNestShift").checked = false;
	setField("txtNestRank", "");
	if (objCheck.checked) {
		Disable("txtNestRank", true);
		Disable("chkStandardClass", false);
		setField("chkStandardClass", false);
		Disable("chkStandardClass", true);
		Disable("chkNestShift", true);
		Disable("txtCutOverTime", false);
		Disable("chkReleaseTime", false);

	} else {
		Disable("txtNestRank", false);
		Disable("chkStandardClass", false);
		Disable("chkNestShift", false);
		getFieldByID("chkReleaseTime").checked = false;
		setField("txtCutOverTime", "");
		Disable("txtCutOverTime", true);
		Disable("chkReleaseTime", true);
	}
}

function save_click() {
	if (!saveData())
		return;
	document.forms["frmBookingCodes"].action = "";
	document.forms["frmBookingCodes"].submit();
}

function searchBookingClass() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		if (getText("selStatusSearch") != "All") {
			Disable("selBookingClass", false);
		}
		setPageEdited(false);
		top[0].initializeVar();
		setDataToSearchCriteria();
		document.forms[0].hdnMode.value = "SEARCH";
		document.forms[0].submit();
		top[2].ShowProgress();
	}
}

function myRecFunction(intRecNo, intErrMsg) {
	top[2].HidePageMessage();
	if (intErrMsg != "") {
		top[2].objMsg.MessageText = intErrMsg;
		top[2].objMsg.MessageType = "Error";
		top[2].ShowPageMessage();
	}
}

// To Handle Paging
function gridNavigations(intRecNo, intErrMsg) {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setPageEdited(false);
		setField("hdnMode", "SEARCH");
		objOnFocus();
		top[0].intLastRec = intRecNo;
		setField("hdnRecNo", intRecNo);
		setDataToSearchCriteria();
		if (intErrMsg != "") {
			top[2].objMsg.MessageText = intErrMsg;
			top[2].objMsg.MessageType = "Error";
			top[2].ShowPageMessage();
		} else {
			document.forms[0].submit();
			top[2].ShowProgress();
		}
	}
}

function objOnFocus() {
	top[2].HidePageMessage();
}

/*function disableSelections(selBookingClass) {
	if (selBookingClass.value != "-1") {
		selectChange("-1", "selCOS");
		selectChange("-1", "selBCType");
		selectChange("-1", "selAllocSearch");

		Disable("selCOS", true);
		Disable("selBCType", true);
		Disable("selAllocSearch", true);
		Disable("selStatusSearch", true);
	} else {
		Disable("selCOS", false);
		Disable("selBCType", false);
		Disable("selAllocSearch", false);
		Disable("selStatusSearch", false);
	}

}*/

function disableSelections(selOption){
	if(selOption == "selBookingClass") {
		if (selBookingClass.value != "-1") {
			selectChange("-1", "selCOS");
			selectChange("-1", "selBCType");
			selectChange("-1", "selAllocSearch");
			selectChange("-1","selLCC");
			setField("selStatusSearch", "All");
			
			Disable("selCOS", true);
			Disable("selBCType", true);
			Disable("selAllocSearch", true);
			Disable("selStatusSearch", true);
			Disable("selLCC",true);
			
		} else {
			Disable("selCOS", false);
			Disable("selBCType", false);
			Disable("selAllocSearch", false);
			Disable("selStatusSearch", false);
			Disable("selLCC",false);
		}
	} else {
		if (getText("selStatusSearch") != "All") {
			selectChange("-1", "selCOS");
			selectChange("-1", "selBCType");
			selectChange("-1", "selAllocSearch");
			selectChange("-1", "selBookingClass");
			selectChange("-1","selLCC");

			Disable("selCOS", true);
			Disable("selBCType", true);
			Disable("selAllocSearch", true);
			Disable("selBookingClass", true);
			Disable("selLCC",true);
		} else {
			Disable("selCOS", false);
			Disable("selBCType", false);
			Disable("selAllocSearch", false);
			Disable("selBookingClass", false);
			Disable("selLCC",false);
		}
	}
}

function setDataToSearchCriteria() {
	var strSearch = getText("selBookingClass") + "," + getText("selCOS") + ","
			+ getText("selBCType") + "," + getText("selStatusSearch");
	top[1].objTMenu
			.tabSetValue(screenId, strSearch + "~" + getText("hdnRecNo"));
}

function setFieldsAfterOp() {
	if (top[0].strSearchCriteria != "") {
		var arrFields = top[0].strSearchCriteria.split("~");
		setField("selBookingClass", arrFields[0]);
		setField("selCOS", arrFields[1]);
		setField("selBCType", arrFields[2]);
	}
}
function setTimeWithColon(field, strTime) {
	objOnFocus();
	if (trim(strTime) != "") {
		var index = strTime.length - 2;
		var hourindex = strTime.search(":");
		if (hourindex != -1) {
			setField(field, strTime);
		} else {
			var mn = strTime.substr(index, 2);
			var hr = "0";
			if (strTime.length == 1) {
				hr = "00";
				mn = "0" +strTime;
			}else if (strTime.length == 2) {
				hr = "00";
			} else if (strTime.length == 3) {
				hr = "0"+strTime.substr(0, 1);

			} else {
				hr = strTime.substr(0, 2);
			}
			var timecolon = hr + ":" + mn;
			setField(field, timecolon);
			strTime = timecolon;
		}
	}
}

function setTimeWithColonWithMax(txtField,maxLength) {
	
	var strTime = txtField.value;
	
	if(trim(strTime) != "") {
		var index = strTime.length - 2;
		var hourindex = strTime.search(":");
		if (hourindex != -1) {
			txtField.value = strTime;
		} else {
			
			var mn = strTime.substr(index, 2);
			var hr = "0";
			if (strTime.length == 1) {
				hr = "00";
				mn = "0" +strTime;
			}else if (strTime.length == 2) {
				hr = "00";
			} else if (strTime.length == 3) {
				hr = "0"+strTime.substr(0, 1);

			} else {
				if((strTime.length > 3) && (strTime.length <= 7)) {					
					if(strTime.length == 7) {
			   			mn = strTime.substr(index-1,2);
			   		}
					else {
						mn = strTime.substr(index,2);
					}
				}
				if((strTime.length > 3) && (strTime.length <= 7)) {
					
					if(strTime.length == 7) {
						hr =  strTime.substr(0, index-1); 
					}
					else {
						hr =  strTime.substr(0, index);
					}
					
				} else {
					hr =  strTime.substr(0, 2); 
				 }
			}
			
			var timecolon = hr + ":" + mn ;
			txtField.value = timecolon;
			strTime = timecolon;
		}

	}
}


// Checks if time is in HH:MM format - HH can be more than 24 hours. HH can have
// more than one digit upto 4 digits.
function isTimeFormatValid(objTime, objTimeValue) {
	
	 relTimeInValid = false;
	 relTimeGreater = false;
	
	var timePattern = /^(\d{1,}):(\d{2})$/;
	var matchArray = trim(objTimeValue).match(timePattern);

	if (matchArray == null) {
		return false;
	}
	var hour = matchArray[1];
	var min = matchArray[2];
	
	if (((hour == 1500) && (min > 0)) || ((hour > 1500) && (min >= 0))) {
		relTimeGreater = true;
		return false;		
	}
	if ((min < 0) || (min > 59)) {
		relTimeInValid = true;
		return false;
	}
	
	if (hour.length < 2) {
		setField(objTime, "0" + trim(objTimeValue));
	}
	return true;
}

//added by Suneth
//This function removes white spaces of the given field

function removeWhiteSpaces(txtObjct){
	
	var val = txtObjct.value;
	txtObjct.value = val.replace(/\s+/g, '');
	
}