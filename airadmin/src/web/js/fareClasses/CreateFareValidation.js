
var fromDate;
var slsfromDate;
var returnfromdate;
var data = "";
var addHocClicked = false;

var standard;
var btnClick;
var allocType;
var agentCodes = "";
var updateClicked = false;
var toDate;
var screenId = "SC_INVN_010";
var delimiter = "^";
var parentScreenId = "SC_INVN_007";
var editRuleClicked = false;
var preAdFare = "0.00";
var preChFare = "0.00";
var preInFare = "0.00";
var chAmp = "P";
var inAmp = "P";

var preAdFareLocal = "0.00";
var preChFareLocal = "0.00";
var preInFareLocal = "0.00";
var chAmpLocal = "V";
var inAmpLocal = "V";
var chldFareType = "V";
var infFareType = "P";
var objFC = new listBox();
var isAddFare = false;

var arrDisableControls = new Array('chkADRef', 'chkADApp', 'chkADMod', 'chkADCNX', 
									'chkCHRef', 'chkCHApp', 'chkCHMod', 'chkCHCNX', 
									'chkINRef',	'chkINApp', 'chkINMod', 'chkINCNX', 
									'txtADNoShoCharge','txtCHNoShoCharge', 'txtINNoShoCharge',
									'txtADNoShoChargeInLocal','txtCHNoShoChargeInLocal','txtINNoShoChargeInLocal');

var arrDays = new Array("chkInbSun", "chkInbMon", "chkInbTues", "chkInbWed", "chkInbThu", "chkInbFri", "chkInbSat");
var depDays = new Array("chkOutSun", "chkOutMon", "chkOutTues", "chkOutWed", "chkOutThu", "chkOutFri", "chkOutSat");
var selModeTyps = {
	v: "V",pf:"PF", pfs:"PFS"
};
var defaultCanAndModValueType = selModeTyps.v;

/*
 * sets the page edit status
 */
function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}

/*
 * enable/disable save reset buttons
 */
function disableSave(Cond) {
	Disable("btnSave", Cond);
	Disable("btnReset", Cond);
	Disable("btnOverWriteRule", Cond);
}

function clearMessages() {
	top[2].HidePageMessage();
}

function rmksChange(objCon) {
	setPageEdited(true);
	var strVal = objCon.value;
	var strLen = strVal.length;
	if (strLen > 255) {
		objCon.value = strVal.substr(0, 255);
		showERRMessage(cmtsExceeds);
		objCon.focus();
	} else {
		top[2].HidePageMessage();
	}
}

/*
 * sets the fare rule value from the text
 */
function setFareRule(strCode) {
	var control = document.getElementById("selFC");
	for ( var t = 0; t < (control.length); t++) {
		if (control.options[t].text == strCode) {
			control.options[t].selected = true;
			break;
		}
	}
}

/*
 * sets as per selected in bound out bound frequency
 */
function selectAll(isOut) {
	if (isOut) {
		if (getFieldByID("chkOutAll").checked == true) {
			for ( var t = 0; t < depDays.length; t++) {
				setChkControls(depDays[t], true);
			}
		} else {
			for ( var t = 0; t < depDays.length; t++) {
				setChkControls(depDays[t], false);
			}
		}
	} else {
		if (getFieldByID("chkInAll").checked == true) {
			for ( var t = 0; t < arrDays.length; t++) {
				setChkControls(arrDays[t], true);
			}
		} else {
			for ( var t = 0; t < arrDays.length; t++) {
				setChkControls(arrDays[t], false);
			}
		}
	}
}

/**
 * calendar control data manuplulations
 */
var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		if (!document.getElementById("txtFromDt").disabled) {
			setField("txtFromDt", strDate);
			getFieldByID("txtFromDt").focus();
		}
		break;
	case "1":
		setField("txtToDt", strDate);
		getFieldByID("txtToDt").focus();
		break;
	case "2":
		if (!document.getElementById("txtSlsStDate").disabled) {
			setField("txtSlsStDate", strDate);
		}
		break;
	case "3":
		setField("txtSlsEnDate", strDate);
		break;
	case "4":
		setField("txtFromDtInbound", strDate);
		break;
	case "5":
		setField("txtToDtInbound", strDate);
		break;
	}
}

function LoadCalendarDate(strID, objEvent) {
	switch (strID) {
	case 0:
		if (!document.getElementById("txtFromDt").disabled) {
			objCal1.ID = strID;
			objCal1.top = 0;
			objCal1.left = 0;
			objCal1.onClick = "setDate";
			objCal1.showCalendar(objEvent);
		}
		break;
	case 1:
		if (!document.getElementById("txtToDt").disabled) {
			objCal1.ID = strID;
			objCal1.ID = strID;
			objCal1.top = 0;
			objCal1.left = 0;
			objCal1.onClick = "setDate";
			objCal1.showCalendar(objEvent);
		}
		break;
	case 2:
		if (!document.getElementById("txtSlsStDate").disabled) {
			objCal1.ID = strID;
			objCal1.ID = strID;
			objCal1.top = 0;
			objCal1.left = 0;
			objCal1.onClick = "setDate";
			objCal1.showCalendar(objEvent);
		}
		break;
	case 3:
		if (!document.getElementById("txtSlsEnDate").disabled) {
			objCal1.ID = strID;
			objCal1.ID = strID;
			objCal1.top = 0;
			objCal1.left = 0;
			objCal1.onClick = "setDate";
			objCal1.showCalendar(objEvent);
		}
		break;
	case 4:
		if (!document.getElementById("txtFromDtInbound").disabled) {
			objCal1.ID = strID;
			objCal1.top = 0;
			objCal1.left = 0;
			objCal1.onClick = "setDate";
			objCal1.showCalendar(objEvent);
		}
		break;
	case 5:
		if (!document.getElementById("txtToDtInbound").disabled) {
			objCal1.ID = strID;
			objCal1.ID = strID;
			objCal1.top = 0;
			objCal1.left = 0;
			objCal1.onClick = "setDate";
			objCal1.showCalendar(objEvent);
		}
		break;
	}
}

/*
 * calls when a change happens in the page
 */
function required_changed() {
	setPageEdited(true);
	clearMessages();
	disableSave(false);
}

/*
 * On Load function executes when the page is loaded.
 */
function winOnLoad(strMsg, strMsgType) {
	// fare rule modifications and load factor
	disableFareRuleModifications();
	
	setComboSelection();
	addHocClicked = false;
	top[2].HidePageMessage();
	setField("hdnFareID", "");
	setPageEdited(false);
	Disable("btnLnkAgent", false);
	if(getValue("selDefineModType") == selModeTyps.v){
		getFieldByID("txtModification").readOnly = true;
		getFieldByID("txtModificationInLocal").readOnly = false;	
	}else{
		getFieldByID("txtModification").readOnly = false;
		if(isSelectedCurrencyOptionAvailable()){
			getFieldByID("txtModificationInLocal").readOnly = true;
		}
	}
	if(getValue("selDefineCanType") == selModeTyps.v){
		getFieldByID("txtCancellation").readOnly = true;
		getFieldByID("txtCancellationInLocal").readOnly = false;	
	}else{
		getFieldByID("txtCancellation").readOnly = false;
		if(isSelectedCurrencyOptionAvailable()){
			getFieldByID("txtCancellationInLocal").readOnly = true;
		}
	}
	
	// set tab exists
	setTabValues(screenId, delimiter, "strInventoryFlightData", "true");
	clearControls();
	disableControls(true);	
	getFieldByID("chkStatus").checked = true;
	getFieldByID("chkModToSameFare").checked = false;
	disableSave(true);
	// sets the amounts
	setFareAmounts();
	fareRuleCurrClick();
	
	DivWrite("tag1", "Adult");
	DivWrite("tag2", "Child");
	DivWrite("tag3", "Infant");
	
	var arrNewFlexi = new Array();
	arrNewFlexi[0] = new Array();
	arrNewFlexi[0][0] = "-1";
	arrNewFlexi[0][1] = "";
	var count = 1;
	if(getValue("selFareCat") == "R") {			
		for(var i = 1; i < arrFlexiJourney.length; i++){					
			if(arrFlexiJourney[i][2] == "RT" && arrFlexiJourney[i][3] == "ACT"){		
				arrNewFlexi[count] = new Array();
				arrNewFlexi[count][0] = arrFlexiJourney[i][0];
				arrNewFlexi[count][1] = arrFlexiJourney[i][1];
				arrNewFlexi[count][2] = arrFlexiJourney[i][2];
				count++;
			}
		}
	} else {		
		for(var i = 1; i < arrFlexiJourney.length; i++){					
			if(arrFlexiJourney[i][2] == "OW" && arrFlexiJourney[i][3] == "ACT"){		
				arrNewFlexi[count] = new Array();
				arrNewFlexi[count][0] = arrFlexiJourney[i][0];
				arrNewFlexi[count][1] = arrFlexiJourney[i][1];
				arrNewFlexi[count][2] = arrFlexiJourney[i][2];
				count++;
			}
		}
	}
	objFC.dataArray = new Array();
	objFC.dataArray = arrNewFlexi; 
	objFC.textIndex = "1";
	objFC.id = "selFlexiCode";
	objFC.fillListBox();

	if (isSuccessfulSaveMessageDisplayEnabled) {
		if (isSaveTransactionSuccessful) {
			if (saveMsg != null) {
				data = "";
				agentCodes = "";
				if (saveMsg == false) {					
					alert("Record Successfully saved!");					
				} else {					
					alert("Requested Fare Saved.Return Fare Save Failed, Due Existance of a Return Fare!!");					
				}
				setTabValues(screenId, delimiter, "status", "");
				setTabValues(screenId, delimiter, "strGridData", "");
			}			
		}		
	}
	disableInputControls(true);
	var status = getTabValues(screenId, delimiter, "status");
	var ruleStatus = getTabValues(screenId, delimiter, "ruleStatus");

	if(!fareDiscountVisibiliy){
		setDisplay("tblFareDiscount",false);
	}
	if(!agentFareCommissionVisibility){
		setDisplay("tblAgentCommission",false);
	}
	if(!linkedFaresVisibility){
		setDisplay("tblMasterLinkedFares",false);
	}
	if(!sameFareModificationVisibility){
		setDisplay("divModifyToSameFare",false);
	}
	if(multipleFltTypeVisibility.onlySingle){
		if(multipleFltTypeVisibility.enabledType=="INT"){
			setDisplay("trModBuffTimeDomestic",false);
			setDisplay("spanModBuffTimeInternationl",false);
		}else if(multipleFltTypeVisibility.enabledType=="DOM"){
			setDisplay("trModBuffTimeInternational",false);
			setDisplay("spanModBuffTimeDomestic",false);
		}
	}
	setData();
	if (status == "Add") {		
		setFareRuleData(masterRules);
		setFareData();		
		if (ruleStatus == "Edit") {
			Disable("chkAllowReturn", true);									
		} 
		setChkControls("chkInAll", false);		
		setChkControls("chkOutAll", false);				
		Disable("btnEdit", false);				
		disableSave(false);
		Disable("chkAllowReturn", true);
		setReturnEffectiveDates();
		setInBoundDatesOnChange();
	}
	if(status == "SetLocalCurr") {
		setFareData();	
		Disable("chkAllowReturn", true);
		setField("selCamtInLocal", getValue("selCamt"));
		setField("selIamtInLocal", getValue("selIamt"));
	}
	if (strMsg != null && strMsgType != null) {
		if (strMsgType == "Error") {
			
			setPageEdited(true);
			getFieldByID("selFareVisibility").options[0].selected = false;
			setModel(model);
			DivWrite("spnId", model['hdnFareID']);
			objCmb1.setComboValue(model['hdnOrigin']);
			objCmb2.setComboValue(model['hdnDestination']);
			objCmb3.setComboValue(model['hdnVia1']);
			objCmb4.setComboValue(model['hdnVia2']);
			objCmb5.setComboValue(model['hdnVia3']);
			objCmb6.setComboValue(model['hdnVia4']);
			
			var advBookDys = model['txtAdvBookDays'];
			if(advBookDys!=null && trim(advBookDys)!=""){
				var advBkTimeDDHHMM = trim(advBookDys).split(":");
				
				if(advBkTimeDDHHMM.length>2){
					setField("txtAdvBookDays", advBkTimeDDHHMM[0]);
					setField("txtAdvBookHours", advBkTimeDDHHMM[1]);
					setField("txtAdvBookMins", advBkTimeDDHHMM[2]);	
				}								
			}
					
			if (model['hdnTempMode'] == "Edit") {
				disableOnEdit();
			}
			
			disableInputControls(false);
			setBookingCode(model['selBc']);

			if (trim(model['chkAdvance']) != "on") {
				Disable("txtAdvBookDays", true);	
				Disable("txtAdvBookHours", true);
				Disable("txtAdvBookMins", true);
			}
						
			if (model['chkChrgRestrictions'] == "on") {				
				disableCnxMoDcontrols(false);				
			} else {
				disableCnxMoDcontrols(true);					
			}
			
			if (model['radFare'] == "OneWay") {
				disableOutBound(true);
				for ( var t = 0; t < arrDays.length; t++) {
					setChkControls(arrDays[t], false);
					Disable(arrDays[t], true);
				}
			}else {
				setField("radReturn","Return");
			}
			
			var arrayArrDays = model['hdnArrIntDays'].split(",");
			var arrayDeptDays = model['hdnDeptIntDays'].split(",");
			for ( var t = 0; t < depDays.length; t++) {
				if (arrayDeptDays[t] == "1") {
					setChkControls(depDays[t], true);
				} else {
					setChkControls(depDays[t], false);
				}
			}
			
			ctrl_visibilityAgents();
			data = model['hdnAgents'];
			agentCodes = model['hdnAgents'];			
			setFareRule(model['selFC']);			
			for ( var t = 0; t < arrDays.length; t++) {
				if (arrayArrDays[t] == "1") {
					setChkControls(arrDays[t], true);
				} else {
					setChkControls(arrDays[t], false);
				}
			}
			var tmpPaxArray = setErrorPaxGrid(model['hdnADDetails'].split("~"), model['hdnCHDetails'].split("~"), model['hdnINDetails'].split("~"));			
			fillPaxFareConditionsGrid(tmpPaxArray);
		}			
		
		if (getTabValues(screenId, delimiter, "ruleStatus") == "Edit") {
			Disable("chkAllowReturn", true);
			Disable("radCurr", true);
			Disable("radSelCurr", true);
		}
		if (getText("selFC") != "") {
			disableInputControls(true);
			Disable("btnSave", false);
			Disable("btnOverWriteRule", false);
			Disable("btnAdd", false);
		}
		showCommonError(strMsgType, strMsg);
	}
	if(chidVisibility == 'false') {	
		Disable("selCamt", true);
		Disable("txtCHL", true);
		Disable("selIamt", true);
		Disable("txtINF", true);
		Disable("selCamtInLocal", true);
		Disable("txtCHLInLocal", true);
		Disable("selIamtInLocal", true);
		Disable("txtINFInLocal", true);
		
		
	}
	if(isOpenRetSupport == 'false'){
		setDisplay("tdLblOpenRTConf", false);
		setDisplay("tdOpenRTConfMnth", false);
		setDisplay("tdOpenRTConfDays", false);
		setDisplay("tdOpenRTConfHrs", false);
		setDisplay("tdOpenRTConfMins", false);
		setDisplay("tdChkOPenRT", false);
		setDisplay("tdLblChkOpenRT", false);
	}
	disableAddhoc();
	if (showFareDefByDepCountryCurrency == "Y") {
		document.getElementById('divCurrSelect').style.display = 'block';
		document.getElementById('divFare').style.display = 'block';
		var objRad1 = getFieldByID("radCurr");
		var objRad2 = getFieldByID("radSelCurr");
		if(isCurrencyLoaded || objRad2.checked) {
			getFieldByID("radSelCurr").checked = true;
					
			getFieldByID("txtAED").readOnly = true;
			getFieldByID("txtCHL").readOnly = true;
			getFieldByID("txtINF").readOnly = true;
			getFieldByID("selCamt").readOnly = true;
			getFieldByID("selIamt").readOnly = true;
					
			getFieldByID("txtBaseInLocal").readOnly = false;
			getFieldByID("txtCHFareInLocal").readOnly = false;
			getFieldByID("txtINFInLocal").readOnly = false;
			getFieldByID("selCamtInLocal").readOnly = false;
			getFieldByID("selIamtInLocal").readOnly = false;
			
			if(getText("hdnFareID") != null && getText("hdnFareID") != "") {
				if(preAdFare != "0.00") {
					setField("txtAED",preAdFare);
					setField("txtCHL",preChFare);
					setField("txtINF",preInFare);
					setField("selCamt",chAmp);
					setField("selIamt",inAmp);
					
				}
			} else {
				setField("txtAED", "0.00");
				setField("txtCHL", "0.00");
				setField("txtINF", "0.00");
				
				if(getText("txtBaseInLocal") == "") {
					setField("txtBaseInLocal", "0.00");
				}
				if(getText("txtCHFareInLocal") == "") {
					setField("txtCHFareInLocal", "0.00");
				}
				if(getText("txtINFInLocal") == "") {
					setField("txtINFInLocal", "0.00");
				}
							
			}

		} 
		else if (objRad1.checked) {
			getFieldByID("txtAED").readOnly = false;
			getFieldByID("txtCHL").readOnly = false;
			getFieldByID("txtINF").readOnly = false;
			getFieldByID("selCamt").readOnly = false;
			getFieldByID("selIamt").readOnly = false;
					
			getFieldByID("txtBaseInLocal").readOnly = true;
			getFieldByID("txtCHFareInLocal").readOnly = true;
			getFieldByID("txtINFInLocal").readOnly = true;
			getFieldByID("selCamtInLocal").readOnly = true;
			getFieldByID("selIamtInLocal").readOnly = true;
		} 		
			
	} else {
		document.getElementById('divCurrSelect').style.display = 'none';
		document.getElementById('divFare').style.display = 'none';
	}
	
	if(showHalfReturn) {
		document.getElementById('halfReturnLabel').style.display = 'block';
		document.getElementById('halfReturnCheck').style.display = 'block';
	} else {
		document.getElementById('halfReturnLabel').style.display = 'none';
		document.getElementById('halfReturnCheck').style.display = 'none';
	}
	
	if(getFieldByID("chkLinkFare").checked == true){
		linkFareValidate();
	}
	disableNoShowChargeModelUIControls(true);
	top[2].HideProgress();
}

function setErrorPaxGrid(adArr, chArr, inArr) {
	 
	var tempArr = new Array();
//	tempArr[0] = new Array('AD', 'Adult',  '', '', '', adArr[2], adArr[3], adArr[4], adArr[1],'', '', adArr[5], '', adArr[7]);
//	tempArr[1] = new Array('CH', 'Child',  '', '', '', chArr[2], chArr[3], chArr[4], chArr[1],'', '', chArr[5], '', chArr[7]);
//	tempArr[2] = new Array('IN', 'Infant', '', '', '', inArr[2], inArr[3], inArr[4], inArr[1],'', '', inArr[5], '', inArr[7]);
	
	tempArr[0] = new Array('AD', 'Adult',  '', '', '', adArr[3], adArr[4], adArr[5], adArr[6], '', '', '-1', '', adArr[7],adArr[8],adArr[9],adArr[10],adArr[11]);
	tempArr[1] = new Array('CH', 'Child',  '', '', '', chArr[3], chArr[4], chArr[5], chArr[6], '', '', '-1', '', chArr[7],chArr[8],chArr[9],chArr[10],chArr[11]);
	tempArr[2] = new Array('IN', 'Infant', '', '', '', inArr[3], inArr[4], inArr[5], inArr[6], '', '', '-1', '', inArr[7],inArr[8],inArr[9],inArr[10],inArr[11]);

	
	return tempArr;
}

/*
 * Enable/Disable connection controls as per the need
 */
function disableOutBound(cond) {
	Disable("txtMaxStayMonths", cond);
	Disable("txtMinStayMonths", cond);
	Disable("txtMaxStayDays", cond);
	Disable("txtMinStayDays", cond);
	Disable("txtMaxStayHours",cond);
	Disable("txtMaxStayMins", cond);
	Disable("txtMinStayMins", cond);
	Disable("txtMinStayHours",cond);
	Disable("txtInbTmFrom", cond);
	Disable("txtInbTmTo", cond);
	Disable("txtConfirmMonths", cond);
	Disable("txtConfirmDays", cond);
	Disable("txtConfirmHours", cond);
	Disable("txtConfirmMins", cond);
	Disable("chkOpenRT", cond);		
	Disable("chkPrintExp", cond);
	Disable("chkHRT", cond);		
}

// enable disable modify & cancell controls
function disableCnxMoDcontrols(cond){
	Disable("txtModification", cond);
	Disable("txtCancellation", cond);
	Disable("txtModificationInLocal", cond);
	Disable("txtCancellationInLocal", cond);	
	
	Disable("selDefineModType", cond);
	Disable("selDefineCanType", cond);
	Disable("txtMin_canc", cond);
	Disable("txtMax_canc", cond);
	Disable("txtMin_mod", cond);
	Disable("txtMax_mod", cond);
}

/*
 * Enable/Disable pax grid
 */
function DisablePaxFareConditionsGrid(flag) {

	Disable("chkADRef", flag);
	Disable("chkADApp", flag);
	// if fare category is RESTRICTED (R) MOD and CNX are not expected to be
	// enabled
	// So flag has to be true
	if (getValue("selFareCat") == "R") {
		flag = true;
		setChkControls("chkADMod", false);
		setChkControls("chkADCNX", false);
		setChkControls("chkCHMod", false);
		setChkControls("chkCHCNX", false);
		setChkControls("chkINMod", false);
		setChkControls("chkINCNX", false);
	}
	disableChidGrid(flag);
	disableINFGrid(flag);
	Disable("chkADMod", flag);
	Disable("chkADCNX", flag);
	Disable("txtADNoShoCharge", flag);
	Disable("txtCHNoShoCharge", flag);
	Disable("txtINNoShoCharge", flag);
	Disable("txtADNoShoChargeInLocal", flag);
	Disable("txtCHNoShoChargeInLocal", flag);
	Disable("txtINNoShoChargeInLocal", flag);

}

/**
 * Set values in the pax grid
 */
function fillPaxFareConditionsGrid(gridData) {

	DivWrite("tag1", "Adult");
	DivWrite("tag2", "Child");
	DivWrite("tag3", "Infant");

	setField("hdnGrd1", gridData[0][0]);
	setField("hdnGrd2", gridData[1][0]);
	setField("hdnGrd3", gridData[2][0]);
	
	var isFillDataFromPaxTypeArray = isFillFareFromPaxTypeArray(gridData);
	if (arrType != null && arrType.length > 0) {
		setChkControls("chkADApp", gridData[0][5]);
		setChkControls("chkADMod", gridData[0][6]);
		setChkControls("chkADCNX", gridData[0][7]);
		setChkControls("chkADRef", gridData[0][8]);		

		setChkControls("chkCHApp", gridData[1][5]);
		setChkControls("chkCHMod", gridData[1][6]);
		setChkControls("chkCHCNX", gridData[1][7]);
		setChkControls("chkCHRef", gridData[1][8]);
		
		setChkControls("chkINApp", gridData[2][5]);
		setChkControls("chkINMod", gridData[2][6]);
		setChkControls("chkINCNX", gridData[2][7]);
		setChkControls("chkINRef", gridData[2][8]);
		
	} else {
		setChkControls("chkADRef", "off");
		setChkControls("chkADApp", "off");
		setChkControls("chkADMod", "off");
		setChkControls("chkADCNX", "off");		

		setChkControls("chkCHRef", "off");
		setChkControls("chkCHApp", "off");
		setChkControls("chkCHMod", "off");
		setChkControls("chkCHCNX", "off");		

		setChkControls("chkINRef", "off");
		setChkControls("chkINApp", "off");
		setChkControls("chkINMod", "off");
		setChkControls("chkINCNX", "off");
		
	}
	
	setField("hdnVersionAD",     gridData[0][11]);
	setField("hdnFRPAXIDAD",  	 gridData[0][12]);
	setField("txtADNoShoCharge", gridData[0][13]);	
	setField("txtADNoShoChargeInLocal", gridData[0][17]);
	
	setField("hdnVersionCH", 	 gridData[1][11]);
	setField("hdnFRPAXIDCH", 	 gridData[1][12]);
	setField("txtCHNoShoCharge", gridData[1][13]);
	setField("txtCHNoShoChargeInLocal", gridData[1][17]);
	
	setField("hdnVersionIN", 	 gridData[0][11]);
	setField("hdnFRPAXIDIN", 	 gridData[0][12]);
	setField("txtINNoShoCharge", gridData[2][13]);
	setField("txtINNoShoChargeInLocal", gridData[2][17]);
	
	/* no show fare percentage charge changes*/
	if(isFillDataFromPaxTypeArray){
		setField("selADCGTYP", "V");
		setField("txtADNoShoBreakPoint", "0.00");
		setField("txtADNoShoBoundary", "0.00");
		
		setField("selCHCGTYP", "V");
		setField("txtCHNoShoBreakPoint", "0.00");
		setField("txtCHNoShoBoundary", "0.00");
		
		setField("selINCGTYP", "V");
		setField("txtINNoShoBreakPoint", "0.00");
		setField("txtINNoShoBoundary", "0.00");
	}else{
		setField("selADCGTYP", gridData[0][14]);
		setField("txtADNoShoBreakPoint", gridData[0][15]);
		setField("txtADNoShoBoundary", gridData[0][16]);
		
		setField("selCHCGTYP", gridData[1][14]);
		setField("txtCHNoShoBreakPoint", gridData[1][15]);
		setField("txtCHNoShoBoundary", gridData[1][16]);
		
		setField("selINCGTYP", gridData[2][14]);
		setField("txtINNoShoBreakPoint", gridData[2][15]);
		setField("txtINNoShoBoundary", gridData[2][16]);
	}
}

function isFillFareFromPaxTypeArray(arr){
	//The array length is checked because the onChange() of the fare rule text box does not retrieve the fare rule details.
	if(arr[0].length > 14){
		return false;
	}else{
		return true;
	}
}
/**
 * Validate the Adult dependency as per pax grid
 */
function validateAdultDependancy(controlinput, controldisable) {
	var strADref = getValue(controlinput);
	var strMode = getText("hdnMode");
	
	if (controlinput == "chkADApp") {
		if ((strADref == "false" || strADref == "" || strADref == "off")) {
			setChkControls("chkADRef", false);
			setChkControls("chkADMod", false);
			setChkControls("chkADCNX", false);
			setChkControls("chkINRef", false);
			setChkControls("chkINMod", false);
			setChkControls("chkINCNX", false);
			Disable("chkADRef", true);
			Disable("chkADMod", true);
			Disable("chkADCNX", true);
			Disable("chkINRef", true);
			Disable("chkINMod", true);
			Disable("chkINCNX", true);
		} else {
			Disable("chkADRef", false);
			Disable("chkINRef", false);
			// if fare category is RESTRICTED (R) MOD and CNX are not expected
			// to be enabled
			if (getValue("selFareCat") != "R") {
				Disable("chkADMod", false);
				Disable("chkADCNX", false);
				Disable("chkINMod", false);
				Disable("chkINCNX", false);
			}
		}
	}

	if (chidVisibility == 'false') {
		getFieldByID("chkCHRef").checked = getFieldByID("chkADRef").checked;
		getFieldByID("chkCHApp").checked = getFieldByID("chkADApp").checked;
		getFieldByID("chkCHMod").checked = getFieldByID("chkADMod").checked;
		getFieldByID("chkCHCNX").checked = getFieldByID("chkADCNX").checked;
	}

	if (strADref == "false" || strADref == "" || strADref == "off") {
		for ( var a = 0; a < arrDisableControls.length; a++) {
			setField(controldisable, false);
			Disable(controldisable, true);
		}
	} else if (strADref == "true" || strADref == "on" || strADref == "ontrue") {
		Disable(controldisable, false);
	}
}

/**
 * Validate the Adult dependency for infant as per pax grid
 */
function validateInfant(control, maincontrol) {
	var strADmain = getValue(maincontrol)
	var strADref = getValue(control);
	var strMode = getText("hdnMode");

	if (strMode == "Edit") {
		return;
	}// no need validation

	if (control == "chkINApp") {
		if ((strADref == "false" || strADref == "" || strADref == "off")) {
			setChkControls("chkINRef", false);
			setChkControls("chkINMod", false);
			setChkControls("chkINCNX", false);
			Disable("chkINRef", true);
			Disable("chkINMod", true);
			Disable("chkINCNX", true);
		} else {
			Disable("chkINRef", false);
			// if fare category is RESTRICTED (R) MOD and CNX are not expected
			// to be enabled
			if (getValue("selFareCat") != "R") {
				Disable("chkINMod", false);
				Disable("chkINCNX", false);
			}
		}
	}
	if (strADmain == "false" || strADmain == "" || strADmain == "off") {
		setField(control, false);
	}
}

/**
 * Validate the Adult dependency for child as per pax grid
 */
function validateChild(control) {
	var isChecked = getValue(control);
	var strMode = getText("hdnMode");

	if (strMode == "Edit") {
		return;
	}// no need validation

	if (isChecked == "false" || isChecked == "" || isChecked == "off") {
		setChkControls("chkCHRef", false);
		setChkControls("chkCHMod", false);
		setChkControls("chkCHCNX", false);
		Disable("chkCHRef", true);
		Disable("chkCHMod", true);
		Disable("chkCHCNX", true);
	} else {
		Disable("chkCHRef", false);
		// if fare category is RESTRICTED (R) MOD and CNX are not expected to be
		// enabled
		if (getValue("selFareCat") != "R") {
			Disable("chkCHMod", false);
			Disable("chkCHCNX", false);
		}
	}
}

/**
 * sets the grid values when a fare is changed
 */
function selFareOnchange() {

	if (getValue("selFareCat") == "R") {
		setChkControls("chkADMod", false);
		setChkControls("chkCHMod", false);
		setChkControls("chkINMod", false);
		setChkControls("chkADCNX", false);
		setChkControls("chkCHCNX", false);
		setChkControls("chkINCNX", false);
		setChkControls("chkOpenRT", false);
		setChkControls("chkHRT", false);
		
		Disable("chkADMod", true);
		Disable("chkCHMod", true);
		Disable("chkINMod", true);
		Disable("chkADCNX", true);
		Disable("chkCHCNX", true);
		Disable("chkINCNX", true);
		Disable("chkOpenRT", true);
		Disable("chkHRT", true);
		createReturnFlexiCodes();
	} else {
		Disable("chkADMod", false);
		Disable("chkCHMod", false);
		Disable("chkINMod", false);
		Disable("chkADCNX", false);
		Disable("chkCHCNX", false);
		Disable("chkINCNX", false);
		if(getText("radFare") == "Return"){
			Disable("chkOpenRT", false);
			Disable("chkHRT", false);
		}
		createOnewayFlexiCodes();
	}
}

/*
 * sets the fare amounts in the grid
 */
function setFareAmounts() {
	if (arrType != null && arrType.length > 0) {
		var adult = getPaxType("AD");
		var child = getPaxType("CH");
		var infant = getPaxType("IN");

		setField("txtAED", adult[4]);
		setField("txtCHL", child[4]);
		setField("txtINF", infant[4]);

		setField("selCamt", child[3]);
		setField("selIamt", infant[3]);
	}
}

// returns the pax data array for a give pax type
function getPaxType(paxTypeCode) {
	for ( var x = 0; x < arrType.length; x++) {
		if (arrType[x][0] == paxTypeCode)
			return arrType[x];
	}
	return null;
}

// sets to 0
function BLValidateDecimel(obj) {
	if (obj.value == "") {
		obj.value = "0.00";
	}
}

/**
 * enable the fare rule controls for edit button click
 */
function setFieldsOnLoad() {
	var checked = getFieldByID("chkAdvance").checked;
	var checkedrest = getFieldByID("chkChrgRestrictions").checked;

	if (checked == true) {
		Disable("txtAdvBookDays", false);
		Disable("txtAdvBookHours", false);
		Disable("txtAdvBookMins", false);
	} else {
		Disable("txtAdvBookDays", true);
		Disable("txtAdvBookHours", true);
		Disable("txtAdvBookMins", true);
	}
	if (getText("radFare") == "Return") {
		Disable("chkInAll", false);
		Disable("chkOutAll", false);
		disableOutBound(false);
		
		if (((getText("txtMaxStayMonths") != "") && (getText("txtMaxStayMonths") != 0))
				|| ((getText("txtMaxStayDays") != "") && (getText("txtMaxStayDays") != 0))
				|| ((getText("txtMaxStayHours") != "") && (getText("txtMaxStayHours") != 0))
				|| ((getText("txtMaxStayMins") != "") && (getText("txtMaxStayMins") != 0))) {

			Disable("chkPrintExp", false);
		} else {
			Disable("chkPrintExp", true);
		}
	} else {
		Disable("chkInAll", true);
		Disable("chkOutAll", false);
		disableOutBound(true);
		for ( var t = 0; t < arrDays.length; t++) {			
			setChkControls(arrDays[t], false);			
		}
	}
	if (checkedrest == true) {
		disableCnxMoDcontrols(false)
	} else {
		disableCnxMoDcontrols(true);
	}
	ctrl_visibilityAgents();
}

//Function to make all the input fields in the local array readonly
function makeInputsReadOnly(readOnly)
{
	inputs = document.getElementsByTagName('input');
	for (index = 0; index < inputs.length; ++index) {
	    inputs[index].readOnly=readOnly;
	}
	//Disable Overwrite Fee button ass well.
	Disable("btnOverWriteRule",readOnly);
}
function disableDropDownControls(disable)
{
	inputs = document.getElementsByTagName('select');
	for (index = 0; index < inputs.length; ++index) {
	    inputs[index].disabled=disable;
	}
}
/*
 * Enable/Disable controls in the fare rule section
 */
function disableInputControls(disabled) {   // @@
	Disable("txtFBC", disabled);
	Disable("txtAdvBookDays", disabled);
	Disable("txtAdvBookHours", disabled);
	Disable("txtAdvBookMins", disabled);
	disableOutBound(disabled);
	disableCnxMoDcontrols(disabled);
	Disable("selPaxCat", disabled);
	Disable("selFareCat", disabled);
	Disable("selFlexiCode", disabled);
	Disable("chkOpenRT", disabled);
	Disable("chkHRT", disabled);

	Disable("chkPrintExp", disabled);
	Disable("txtaRulesCmnts", disabled);
	Disable("txtaInstructions", disabled);

	Disable("txtOutTmFrom", disabled);
	Disable("txtOutTmTo", disabled);

	
	for ( var t = 0; t < arrDays.length; t++) {
		Disable(arrDays[t], disabled);
		Disable(depDays[t], disabled);
	}

	Disable("selFareVisibility", disabled);
	Disable("chkRefundable", disabled);
	Disable("chkAdvance", disabled);

	Disable("chkChrgRestrictions", disabled);
	Disable("selFareVisibility", disabled);
	Disable("selFareRuleCode", disabled);
	Disable("selAgentComType", disabled);

	Disable("radFare", disabled);
	Disable("chkInAll", disabled);
	Disable("chkOutAll", disabled);
	Disable("btnReset", disabled);

	Disable("txtModBufDays", disabled);
	Disable("txtModBufHours", disabled);
	Disable("txtModBufMins", disabled);

	Disable("txtModBufDaysDom", disabled);
	Disable("txtModBufHoursDom", disabled);
	Disable("txtModBufMinsDom", disabled);

	DisablePaxFareConditionsGrid(disabled);

	Disable("radCurrFareRule", disabled);
	Disable("radSelCurrFareRule", disabled);
	Disable("selCurrencyCode", disabled);		

}

function KPValidateCurrency(control) {
	var strCC = getText(control);
	var trn = currencyValidate(strCC, 15, 2);
	if (trn == false) {
		getFieldByID(control).focus();
		showERRMessage(currFormatWrong);
	} else {
		top[2].HidePageMessage();
	}
}

/*
 * clear the fare rule controls
 */
function clearControls() {	
	setField("txtFBC", "");
	setField("selFareVisibility", "");
	setField("hdnVersion", "");
	setField("txtAdvBookDays", "");
	setField("txtAdvBookHours", "");
	setField("txtAdvBookMins", "");

	setChkControls("chkChrgRestrictions", false);
	data = "";
	agentCodes = "";
	setField("txtModification", "");
	setField("txtCancellation", "");
	setField("txtModificationInLocal", "");
	setField("txtCancellationInLocal", "");
	
	
	
	setField("selDefineModType", "");
	setField("selDefineCanType", "");
	setField("txtMin_mod", "");
	setField("txtMax_mod", "");
	setField("txtMin_canc", "");
	setField("txtMax_canc", "");
	
	setChkControls("chkAdvance", false);
	setField("radFare", "");
	setField("hdnMode", "");
	setField("txtMaxStayMonths", "");
	setField("txtMinStayMonths", "");
	setField("txtMaxStayDays", "");
	setField("txtMinStayDays", "");
	setField("txtMaxStayHours", "");
	setField("txtMaxStayMins", "");
	setField("txtMinStayHours", "");
	setField("txtMinStayMins", "");

	setChkControls("chkPrintExp", "");

	setField("txtOutTmFrom", "00:00");
	setField("txtOutTmTo", "23:59");

	setField("txtInbTmFrom", "");
	setField("txtInbTmTo", "");
	setField("txtaRulesCmnts", "");
	setField("selPaxCat", "A");
	setField("selFareCat", "N");
	setField("selFlexiCode", "-1");

	setField("txtModBufDays", "");
	setField("txtModBufHours", "");
	setField("txtModBufMins", "");
	
	setField("txtConfirmMonths", "");
	setField("txtConfirmDays", "");
	setField("txtConfirmHours", "");
	setField("txtConfirmMins", "");
	setField("hdnLocalCurrencySelected", "");

	//No Show Percentage Changes
	setField("selADCGTYP", "V");
	setField("selCHCGTYP", "V");
	setField("selINCGTYP", "V");
	setField("txtADNoShoBreakPoint", "");
	setField("txtADNoShoBoundary", "");
	setField("txtCHNoShoBreakPoint", "");
	setField("txtCHNoShoBoundary", "");
	setField("txtINNoShoBreakPoint", "");
	setField("txtINNoShoBoundary", "");
	setField("txtaInstructions", "");	
	setField("radCurrFareRule", "BASE_CURR");
	setField("selCurrencyCode", "-1");
	for ( var t = 0; t < arrDays.length; t++) {
		setChkControls(arrDays[t], false);
	}
	fareRuleCurrClick();
}

/**
 * needs to check the use
 */
function disableAddhoc() {
	if (getText("selFC") != "") {
		Disable("btnAddHocRule", true);
	} else {
		Disable("btnAddHocRule", false);
	}
}

/**
 * Intialize the fare rule section
 */
function defaultLoadCheck() {	
	setChkControls("chkOutAll", false);
	for ( var t = 0; t < depDays.length; t++) {
		setChkControls(depDays[t], true);		
	}
	setField("radFare", "OneWay");
	setField("txtOutTmFrom", "00:00");
	setField("txtOutTmTo", "23:59");	
	setChkControls("chkRefundable", true);	
	setField("txtFBC", "");
	Disable("btnLnkAgent", false);

	fillPaxFareConditionsGrid(arrType);
	createOnewayFlexiCodes();
	setField("selFlexiCode", "-1");
}

/**
 * execute when adhoc button is clicked
 */
function addhoc_click() {
	if (getText("selFC") != "") {
		showERRMessage(addHocFareRest);
	} else {
		btnClick = "AddEdit";
		addHocClicked = true;
		disableInputControls(false);
		defaultLoadCheck();		
		getFieldByID("selFareVisibility").options[0].selected = true;
		setField("txtAgentCommision", "0.00");
		
		for ( var t = 0; t < arrDays.length; t++) {			
			setChkControls(arrDays[t], false);			
		}
		setField("hdnFareRuleVersion", "-1");
		setField("hdnFareRuleID", "");	
		disableOutBound(true);

		Disable("chkPrintExp", true);
		
		// fare rule modifications and load factor
		enableFareRuleModifications();

		Disable("selPaxCat", true);
		Disable("selFareCat", true);
		Disable("selFlexiCode", false);
		setField("hdnMode", "Add");
		Disable("txtAdvBookDays", true);
		Disable("txtAdvBookHours", true);
		Disable("txtAdvBookMins", true);
		disableCnxMoDcontrols(true);
		setPageEdited(true);
		Disable("btnSave", false);
		Disable("btnOverWriteRule", false);
		Disable("chkInAll", true);
		Disable("chkOutAll", false);
		setChkControls("chkOutAll", true);
		Disable("btnLnkAgent", true);
		restrictionEnable();
		Disable("selADCGTYP", false);
		Disable("selCHCGTYP", false);
		Disable("selINCGTYP", false);
		fareRuleCurrClick();
	}

	if (chidVisibility == 'false') {
		disableINFGrid(true);
		disableChidGrid(true);
	}
	
	//Hide the chkUpdateCommentsOnly as it serves no purpose when adding a new record.
	document.getElementById('chkUpdateCommentsOnly').style.visibility = 'hidden';
	document.getElementById('lblUpdateCommentsOnly').style.visibility = 'hidden';
}

/*
 * disable the Advance booking days area calls when check box is clicked
 */
function DisableAdnvanceBKDays() {
	var checked = getFieldByID("chkAdvance").checked;
	if (checked == true) {
		Disable("txtAdvBookDays", false);
		Disable("txtAdvBookHours", false);
		Disable("txtAdvBookMins", false);
	} else {
		setField("txtAdvBookDays", "");
		Disable("txtAdvBookDays", true);
		setField("txtAdvBookHours", "");
		Disable("txtAdvBookHours", true);
		setField("txtAdvBookMins", "");
		Disable("txtAdvBookMins", true);
	}
}

/**
 * execute when the return flag radio button is clicked
 */
function DisableReturnDays() {  // radReturn
	
	var fare = getText("radFare");
	
	if (fare == "Return") {
		// Disable("chkOpenRT", false); // umesh@@
		Disable("chkOutAll", false);
		setChkControls("chkOutAll", true);
		Disable("chkInAll", false);
		setChkControls("chkInAll", true);
		disableOutBound(false);
		setField("selPaxCat", "A");
		setField("selFareCat", "N");
		setField("selFlexiCode", "-1");
		setField("txtInbTmTo", "23:59");
		setField("txtInbTmFrom", "00:00");
		Disable("chkInAll", false);
		Disable("chkOutAll", false);
		if(getValue("selFareCat") == "R"){
			Disable("chkOpenRT", true);
			Disable("chkHRT", true);
		}
		
		for ( var t = 0; t < arrDays.length; t++) {
			Disable(arrDays[t], false);
			setChkControls(arrDays[t], true);
		}

		for ( var t = 0; t < depDays.length; t++) {
			Disable(depDays[t], false);
			setChkControls(depDays[t], true);
		}
		
		createReturnFlexiCodes();
		
		
	} else {
		setChkControls("chkOutAll", true);		
		setChkControls("chkInAll", false);	
		Disable("txtMaxStayMonths", "");
		Disable("txtMinStayMonths", "");
		Disable("txtMaxStayDays", "");
		Disable("txtMinStayDays", "");
		setField("txtMaxStayMonths", "");
		setField("txtMinStayMonths", "");
		setField("txtMaxStayDays", "");
		setField("txtMinStayDays", "");
		setField("txtMaxStayHours", "");
		setField("txtMaxStayMins", "");
		setField("txtMinStayHours", "");
		setField("txtMinStayMins", "");
		setField("txtConfirmMonths", "");
		setField("txtConfirmDays", "");
		setField("txtConfirmHours", "");
		setField("txtConfirmMins", "");
		setField("txtModBufDays", "");
		setField("txtModBufHours", "");
		setField("txtModBufMins", "");
		setField("txtModBufDaysDom", "");
		setField("txtModBufHoursDom", "");
		setField("txtModBufMinsDom", "");
		setChkControls("chkOpenRT", false);
		setField("selPaxCat", "A");
		setField("selFareCat", "N");
		setField("selFlexiCode", "-1");
		setField("txtInbTmTo", "");
		setField("txtInbTmFrom", "");
		setChkControls("chkOpenRT", false);       
		setChkControls("chkPrintExp", false); 
		setChkControls("chkHRT", false);
		disableOutBound(true);
		Disable("chkOutAll", false);
		Disable("selPaxCat", true);
		Disable("selFareCat", true);
		Disable("selFlexiCode", false);

		for ( var t = 0; t < depDays.length; t++) {
			Disable(depDays[t], false);
			setChkControls(depDays[t], true);
		}

		for ( var t = 0; t < arrDays.length; t++) {
			setChkControls(arrDays[t], false);
			Disable(arrDays[t], true);
		}
		
		createOnewayFlexiCodes();
	}
}

function createReturnFlexiCodes(){
	var arrNewFlexi = new Array();
	arrNewFlexi[0] = new Array();
	arrNewFlexi[0][0] = "-1";
	arrNewFlexi[0][1] = "";
	var count = 1;
	for(var i = 1; i < arrFlexiJourney.length; i++){
		if(arrFlexiJourney[i][2] == "RT" && arrFlexiJourney[i][3] == "ACT"){		
			arrNewFlexi[count] = new Array();
			arrNewFlexi[count][0] = arrFlexiJourney[i][0];
			arrNewFlexi[count][1] = arrFlexiJourney[i][1];
			arrNewFlexi[count][2] = arrFlexiJourney[i][2];
			count++;
		}
	}
	
	objFC.dataArray = arrNewFlexi; 
	objFC.textIndex = "1";	
	objFC.fillListBox();
	
}

function createOnewayFlexiCodes(){
	var arrNewFlexi = new Array();
	arrNewFlexi[0] = new Array();
	arrNewFlexi[0][0] = "-1";
	arrNewFlexi[0][1] = "";
	var count = 1;
	for(var i = 1; i < arrFlexiJourney.length; i++){
		if(arrFlexiJourney[i][2] == "OW" && arrFlexiJourney[i][3] == "ACT"){		
			arrNewFlexi[count] = new Array();
			arrNewFlexi[count][0] = arrFlexiJourney[i][0];
			arrNewFlexi[count][1] = arrFlexiJourney[i][1];
			arrNewFlexi[count][2] = arrFlexiJourney[i][2];
			count++;
		}
	}
	
	objFC.dataArray = arrNewFlexi; 
	objFC.textIndex = "1";	
	objFC.fillListBox();
}

function KPValidateDecimel(objCon, s, f) {
	setPageEdited(true);
	var strText = objCon.value;
	var length = strText.length;
	var blnVal = isLikeDecimal(strText);
	var blnVal2 = currencyValidate(strText, s, f);
	var blnVal3 = isDecimal(strText);

	if (!blnVal) {
		customValidate(objCon, /[a-zA-Z`!@#$%\s&*?\[\]{}()|\\\/=:,;_^~]/g);
	} else if (!blnVal2) {
		customValidate(objCon, /[a-zA-Z`!@#$%\s&*?\[\]{}()|\\\/=:,;_^~]/g);
		objCon.focus();
	}
	if (!blnVal2) {
		if (strText.indexOf(".") != -1) {
			wholeNumber = strText.substr(0, strText.indexOf("."));
			if (wholeNumber.length > 13) {
				objCon.value= strText.substr(0, wholeNumber.length - 1);
			} else {
				objCon.value= strText.substr(0, length - 1);
			}
		} else {
			objCon.value= strText.substr(0, length - 1);
		}
		
	}
}

function KPValidateDecimelDup(objCon, s, f) {
	setPageEdited(true);
	var strText = objCon.value;
	var length = strText.length;
	var blnVal = isLikeDecimal(strText);
	var blnVal2 = false;
	var type = "";
	
	if (!blnVal) {
		customValidate(objCon, /[a-zA-Z`!@#$%\s&*?\[\]{}()|\\\/=:,;_^~]/g);
	} else if (!blnVal2) {
		customValidate(objCon, /[a-zA-Z`!@#$%\s&*?\[\]{}()|\\\/=:,;_^~]/g);
		objCon.focus();
	}	
	if(objCon.id == "txtCHL" || objCon.id == "txtCHFareInLocal"){
		type = chldFareType;
	}else{
		type = infFareType;
	}	
	if(type == "P"){
		blnVal2 = currencyValidate(strText, 3, 2);
		if (!blnVal2) {
			if (strText.indexOf(".") != -1) {
				wholeNumber = strText.substr(0, strText.indexOf("."));
				if (wholeNumber.length > 6) {
					objCon.value= strText.substr(0, wholeNumber.length - 1);
				} else {
					objCon.value= strText.substr(0, length - 1);
				}
			} else {
				objCon.value= strText.substr(0, length - 1);
			}	
			
		}
		if(strText > 100){
				objCon.value= strText.substr(0, length - 1);
		}
		
	}else{
		blnVal2 = currencyValidate(strText, 10, 2);
		if (!blnVal2) {
			if (strText.indexOf(".") != -1) {
				wholeNumber = strText.substr(0, strText.indexOf("."));
				if (wholeNumber.length > 13) {
					objCon.value= strText.substr(0, wholeNumber.length - 1);
				} else {
					objCon.value= strText.substr(0, length - 1);
				}
			} else {
				objCon.value= strText.substr(0, length - 1);
			}
		}
	}
	
}

function KPValidatePositiveInteger(control, type) {
	var strCC = getText(control);
	var strLen = strCC.length;
	var blnVal = isPositiveInt(strCC)
	if (!blnVal) {
		setField(control, strCC.substr(0, strLen - 1));
		getFieldByID(control).focus();
	}
	switch(type){
		case "MON": if(Number(getText(control)) > 12){
						setField(control, strCC.substr(0, strLen - 1));
						getFieldByID(control).focus();
					}
					break;
		case "DAY":if(Number(getText(control)) > 31){
						setField(control, strCC.substr(0, strLen - 1));
						getFieldByID(control).focus();
					}
					break;
		case "HRS":if(Number(getText(control)) > 23){
						setField(control, strCC.substr(0, strLen - 1));
						getFieldByID(control).focus();
					}
					break;
		case "MIN":	if(Number(getText(control)) > 59){
						setField(control, strCC.substr(0, strLen - 1));
						getFieldByID(control).focus();
					}
					break;
	
	}
}

/*
 * executes when charge restriction clicks
 */
function DisableRestrictions() {
	var checked = getText("chkChrgRestrictions");
	setField("txtMin_mod", "");
	setField("txtMax_mod", "");	
	setField("txtMin_canc", "");
	setField("txtMax_canc", "");	
	if (checked == "on") {
		disableCnxMoDcontrols(false);
		setField("txtModification", modChg);
		setField("txtCancellation", caclChg);
		setField("txtModificationInLocal", modChg);
		setField("txtCancellationInLocal", caclChg);
		
		
		
		setField("selDefineModType", defaultCanAndModValueType);
		setField("selDefineCanType", defaultCanAndModValueType);
		disableAndClearMinMaxForMod(true);
		disableAndClearMinMaxCanc(true);
	} else {
		setField("txtModification", "");
		setField("txtCancellation", "");
		setField("txtModificationInLocal", "");
		setField("txtCancellationInLocal", "");
		
		
		setField("selDefineModType", "");
		setField("selDefineCanType", "");
		
		disableCnxMoDcontrols(true);
	}
}

/**
 * execute when back button is clicked goes back tp the fare grid screen
 */
function back_click() {
	if ((top[1].objTMenu.tabIsAvailable(parentScreenId) && top[1].objTMenu.tabRemove(parentScreenId))
			|| (!top[1].objTMenu.tabIsAvailable(parentScreenId))) {		
		if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(parentScreenId))) {
			setPageEdited(false);
			var parentSearch = getTabValues(screenId, delimiter, "strSearchCriteria");			
			setTabValues(parentScreenId, delimiter, "strSearchCriteria", parentSearch);
			top[1].objTMenu.tabSetTabInfo(screenId, parentScreenId, "showFares.action", "SetUp Fare", false);
			setTabValues(parentScreenId, delimiter, "strInventoryFlightData", "false");
			setTabValues(parentScreenId, delimiter, "status", "Back");
			location.replace("showManageFareCharges.action");
		}
	}
}

/**
 * Executes when Fare Visibility is selected
 */
function ctrl_visibility_Select() {
	var intCount = getFieldByID("selFareVisibility").length;
	var blnSelected = getFieldByID("selFareVisibility").options[0].selected;
	for ( var i = 1; i < intCount; i++) {
		if (blnSelected) {
			getFieldByID("selFareVisibility").options[i].selected = false;
		}
	}
	required_changed();
}

/**
 * Executes in side the WinLoad method sets the data taken from fare grid screen
 * also sets some of the rule data.
 * 
 */
function setData() {
	var searchStr = getTabValues(screenId, delimiter, "strGridData");	
	setTabValues(screenId, delimiter, "ruleStatus", "");
	setTabValues(screenId, delimiter, "status", "");
	if (searchStr != "") {
		var arr = searchStr.split("~");
		var edBkclass  = arr[6];
		var fareRulCde = arr[9];
		var statusEdit = arr[9];		
		
		objCmb1.setComboValue(arr[0]);
		objCmb2.setComboValue(arr[1]);
		objCmb3.setComboValue(arr[2]);
		objCmb4.setComboValue(arr[3]);
		objCmb5.setComboValue(arr[4]);
		objCmb6.setComboValue(arr[5]);

		setField("hdnOrigin", arr[0]);
		// pass the booking class to be edited
		setBookingCode(edBkclass);
		setFareRule(fareRulCde);

		Disable("masterFareRefCode",false);
		Disable("selectedMasterFareID",true);
		//Disable("linkFarePercentage",true);
		Disable("btnLinkFares",true);
		
		bc_onSelect();
		if (statusEdit == "Add") {
			if (arr[0] != "") {
				Disable("btnSave", false);
				Disable("btnOverWriteRule", false);
			}
			if (top[1].objTMenu.focusTab == screenId) {
				objCmb1.focus();
			}			
			defaultLoadCheck();			
			setField("hdnTempMode", "Add");			
			setChkControls("chkOutAll", true);			
		} else if (statusEdit == "Edit") {
			Disable("chkAllowReturn", "true");
			if (top[1].objTMenu.focusTab == screenId) {
				getFieldByID("selBc").focus();
			}
			setTabValues(screenId, delimiter, "ruleStatus", "Edit");			
			setField("hdnFareVersion", arr[8]);
			if (arr[10].length >0) {
				var adhocRules = arr[10].split("/");
				adhocRules.splice(0,1);
				isAddFare = false;
				setFareRuleData(adhocRules);
			}
			setFareDataOnEdit(arr[7]);
			if(arr[21] != ""){
				setField("txtAED", "0.00");
				setField("selCamtInLocal", arr[14]);
				setField("selIamtInLocal", arr[16]);
			} else {
				setField("selCamt", arr[14]);
				setField("selIamt", arr[16]);
			}
			
			// Adult Fare
			if(arr[12]!=null){
				setField("txtAED",arr[12]);
				preAdFare = arr[12]
			}
			// Child Fare
			if(arr[13]!=null && arr[13] != ""){
				setField("txtCHL",arr[13]);
				preChFare = arr[13];
			}
			// Infant Fare
			if(arr[15]!=null && arr[15] != ""){
				setField("txtINF",arr[15]);
				preInFare = arr[15];
			}
			// setField("txtCHL", arr[13]);
			// preChFare = arr[13];
			// setField("selCamt", arr[14]);			
			chAmp = arr[14];
			// setField("txtINF", arr[15]);
			// preInFare = arr[15];
			// setField("selIamt", arr[16]);
			inAmp = arr[16];
			chldFareType = arr[14];
			infFareType = arr[16];
			setField("txtSlsStDate", arr[19]);
			slsfromDate = arr[19];
			setField("txtSlsEnDate", arr[20]);
			setApplicableGrid(arr[17]);
			setField("txtBaseInLocal", arr[21]);
			preAdFareLocal = arr[21];
			setField("txtCHFareInLocal", arr[22]);
			preChFareLocal = arr[22];
			setField("txtINFInLocal", arr[23]);
			preInFareLocal = arr[23];
			if(arr[21] != "" && arr[21] != ".00") {
				getFieldByID("radSelCurr").checked = true;
			}
			returnfromdate = arr[24];
			setField("txtFromDtInbound", arr[24]);
			setField("txtToDtInbound", arr[25]);
			disableOnEdit();
			var act = arr[11];
			if (act != null) {
				if (act == "ACT") {
					getFieldByID("chkStatus").checked = true;
				} else {
					getFieldByID("chkStatus").checked = false;
				}
			}					
			data = getTabValues(screenId, delimiter, "arrayData");
			if (data != "") {
				Disable("btnLnkAgent", false);
			} else {
				Disable("btnLnkAgent", true);
			}
			setField("hdnTempMode", "Edit");
			if (!testDate(toDate) && !testDate(fromDate)) {
				Disable("btnEdit", false);
				Disable("btnReset", false);
				if (testDate(slsfromDate)) {
					Disable("txtSlsStDate", true);
				}
			} else {
				if (testDate(fromDate)) {
					Disable("btnEdit", true);
					Disable("btnReset", true);
					Disable("txtFromDt", true);
					if (testDate(slsfromDate)) {
						Disable("txtSlsStDate", true); // if needed uncoment
					}
				}
				if (testDate(fromDate) && testDate(toDate)) {
					disableSelectArea();
					disableInputControls(true);
					Disable("btnSave", true);
					Disable("btnOverWriteRule", true);
				}
			}
			
			var modToSameFare = arr[30];
			if (modToSameFare != null) {
				if (modToSameFare == "Y") {
					getFieldByID("chkModToSameFare").checked = true;
				} else {
					getFieldByID("chkModToSameFare").checked = false;
				}
			}	

			resetFareDefType();
			
		}
		if(arr[10] == "Add" ){
			isAddFare = true;
		}
	}
	if (returnfromdate != null && returnfromdate != "" && testDate(returnfromdate)) {
		Disable("txtFromDtInbound", true); 
	}
}

/**
 * executes in side setData() method sets the pax grid data according to the
 * data taken from fare grid
 * 
 */
function setApplicableGrid(str) {
	var paxDetails = str.split("&");
	var ADDetails = paxDetails[0].split(",");
	var CHDetails = paxDetails[1].split(",");
	var INDetails = paxDetails[2].split(",");

	DisablePaxFareConditionsGrid(false);

	DivWrite("tag1", "Adult");
	DivWrite("tag2", "Child");
	DivWrite("tag3", "Infant");

	setChkControls("chkADApp", returnOn(ADDetails[1]));
	setChkControls("chkADMod", returnOn(ADDetails[2]));
	setChkControls("chkADCNX", returnOn(ADDetails[3]));
	setChkControls("chkADRef", returnOn(ADDetails[4]));
	setField("hdnVersionAD", ADDetails[6]);
	setField("hdnFRPAXIDAD", ADDetails[5]);
	setField("txtADNoShoCharge", ADDetails[7]);
	setField("selADCGTYP", ADDetails[8]);
	setField("txtADNoShoBreakPoint", ADDetails[9]);
	setField("txtADNoShoBoundary", ADDetails[10]);
	setField("txtADNoShoChargeInLocal", ADDetails[11]);
	
	setChkControls("chkCHApp", returnOn(CHDetails[1]));
	setChkControls("chkCHMod", returnOn(CHDetails[2]));
	setChkControls("chkCHCNX", returnOn(CHDetails[3]));
	setChkControls("chkCHRef", returnOn(CHDetails[4]));
	setField("hdnVersionCH", CHDetails[6]);
	setField("hdnFRPAXIDCH", CHDetails[5]);
	setField("txtCHNoShoCharge", CHDetails[7]);
	setField("selCHCGTYP", CHDetails[8]);
	setField("txtCHNoShoBreakPoint", CHDetails[9]);
	setField("txtCHNoShoBoundary", CHDetails[10]);
	setField("txtCHNoShoChargeInLocal", CHDetails[11]);
	
	setChkControls("chkINApp", returnOn(INDetails[1]));
	setChkControls("chkINMod", returnOn(INDetails[2]));
	setChkControls("chkINCNX", returnOn(INDetails[3]));
	setChkControls("chkINRef", returnOn(INDetails[4]));
	setField("hdnVersionIN", INDetails[6]);
	setField("hdnFRPAXIDIN", INDetails[5]);
	setField("txtINNoShoCharge", INDetails[7]);
	setField("selINCGTYP", INDetails[8]);
	setField("txtINNoShoBreakPoint", INDetails[9]);
	setField("txtINNoShoBoundary", INDetails[10]);
	setField("txtINNoShoChargeInLocal", INDetails[11]);	

	DisablePaxFareConditionsGrid(true);

}

// can get rid of this method
function returnOn(str) {
	if (str == "true" || str == "TRUE") {
		return 'on';
	} else if (str == "false" || str == "FALSE") {
		return 'off';
	}
}


/**
 * Executes in side setData() reset() methods sets In EDIT condition some of the
 * fare rule data and some of the fare data is sets need to investigate
 * duplicate settings
 * 
 */
function setFareDataOnEdit(str) {
	var fareDataArr = str.split(",");

	if (fareDataArr[0] != null) {
		setField("hdnFareID", fareDataArr[1]);
		DivWrite("spnId", fareDataArr[1]);
	}

	if (fareDataArr[2] == null) {
		Disable("btnAddHocRule", false);
	} else if (fareDataArr[2] == "") {		
		setFareRule(fareDataArr[2]);
		Disable("btnAddHocRule", false);
	} else {		
		setFareRule(fareDataArr[2]);
	}

	if (fareDataArr[3] != null) {
		setField("txtFBC", fareDataArr[3]);
	}
	if (fareDataArr[5] != null) {
		toDate = fareDataArr[5];
		setField("txtToDt", fareDataArr[5]);
	}

	if (fareDataArr[4] != null) {
		setField("txtFromDt", fareDataArr[4]);
		fromDate = fareDataArr[4];
	}
	
	if (fareDataArr[7] != null) {
		setBookingCode(fareDataArr[7]);
		bc_onSelect();
	}

	if (fareDataArr[11] != null) {
		var station = fareDataArr[11].split("/");
		objCmb1.setComboValue(station[0]);
		objCmb2.setComboValue(station[station.length - 1]);

		if (station.length > 2) {
			objCmb3.setComboValue(station[1]);
		}
		if (station.length > 3) {
			objCmb4.setComboValue(station[2]);
		}
		if (station.length > 4) {
			var len = station[3].length;
			var viaStation3 = station[3].substring(1, (len));
			objCmb5.setComboValue(viaStation3);
		}
		if (station.length > 5) {
			objCmb6.setComboValue(station[4]);
		}
		
		setField("hdnOrigin",station[0]);
	}

	if (fareDataArr[21] != null) {
		setField("selPaxCat", fareDataArr[21]);
	}

	if (fareDataArr[22] != null) {
		setField("selFareCat", fareDataArr[22]);
	}
	
	if (fareDataArr[28] != null) {
		setField("selFlexiCode", fareDataArr[28]);
	}
	
	if (fareDataArr[39] != null) {
		setField("hdnExistingFareRuleVersion", fareDataArr[39]);
	}
	
}

/**
 * disable controls for edit button click executes in error loading edit button
 * click reset click
 */
function disableOnEdit() {
	Disable("btnEdit", false);
	setChkControls("chkInAll", false);
	setChkControls("chkOutAll", false);
}

/**
 * executes when Fare Rule Code is selected
 */
function ruleCodeSelect() {

	var fare = getText("selFC");
	addHocClicked = false;
	Disable("btnEdit", true);

	if (fare != "") {
		Disable("btnAdd", false);
		setSearchDataForFare();
		Disable("btnAddHocRule", true);		
		Disable("btnSave", false);
		Disable("btnOverWriteRule", false);
	} else {		
		Disable("btnAdd", true);
		Disable("btnAddHocRule", false);
	}
	setPageEdited(true);
	disableInputControls(false);
	clearControls();
	defaultLoadCheck();
	disableInputControls(true);	
	setChkControls("chkChrgRestrictions", false);
	resetFareRuleModifications();
	disableSave(false);
	
}

/**
 * Disables add & edit button executes in winLoad
 */
function disableControls(status) {
	Disable("btnAdd", status);
	Disable("btnEdit", status);	
}

/**
 * executes in ruleCodeSelect(), save_click(), add_click() sets the upper pane
 * values of the page to retrieve after a submit
 * 
 */
function setSearchDataForFare() {
	var cos;
	var fixed;
	var strOrigin = objCmb1.getText();
	var selDest = objCmb2.getText();
	var via1 = objCmb3.getText()
	var via2 = objCmb4.getText();
	var via3 = objCmb5.getText();
	var via4 = objCmb6.getText();
	var strToDate = getText("txtToDt");
	var strFeromDate = getText("txtFromDt");
	var slsToDate = getText("txtSlsEnDate");
	var slsFromDate = getText("txtSlsStDate");
	var bc = getText("selBc");
	var bcValue = getValue("selBc");
	var version = getText("hdnFareVersion");
	var fareID = getText("hdnFareID");
	var arrFields = bcValue.split(",");
	var status = getFieldByID("chkStatus").checked;
	var srchSpnstmt = '';
	var tmpSpnStd = '';
	var baseFareInLocal = '';
	var chFareInLocal = '';
	var inFareInLocal = '';
	var selCHInLocal = '';
	var selINFInLocal = '';
	var strReturnToDate = getText("txtToDtInbound");
	var strReturnFromDate = getText("txtFromDtInbound");
	var modToSameFare = getFieldByID("chkModToSameFare").checked;

	if (bc != "" && arrFields[0] == "Y") {
		tmpSpnStd = "Standard";
	} else {
		tmpSpnStd = "Non Standard";
	}
	if (bc != "" && arrFields[1] == "Y") {
		fixed = "Fixed";
		allocType = "Y";
	} else {
		fixed = "Non Fixed";
	}
	var spnPax = (bc != "" && (trim(arrFields[6]) == "GOSHOW" || trim(arrFields[6]) == "INTERLINE")) ? " / " + arrFields[6]	: "";
	srchSpnstmt = tmpSpnStd + " / " + fixed + " / " + arrFields[2] + " / " + arrFields[5] + " / " + arrFields[4] + spnPax;

	cos = arrFields[2];

	var fc = getText("selFC");

	var aed = getText("txtAED");
	var aedCHL = getText("txtCHL");
	var aedINF = getText("txtINF");
	var selCHL = getValue("selCamt");
	var selINF = getValue("selIamt");

			
	Disable("chkAllowReturn", false);
	var allowReturn = getFieldByID("chkAllowReturn").checked;
	
	if (showFareDefByDepCountryCurrency == "Y") {
			
		var showBaseCurr = getFieldByID("radCurr").checked;
		var showDepAirPoCurr = getFieldByID("radSelCurr").checked;
		
		if(showDepAirPoCurr) {
			
			baseFareInLocal = getText("txtBaseInLocal");
		    selCHInLocal = getValue("selCamtInLocal");
		    selINFInLocal = getValue("selIamtInLocal");
		    
			// if(selCHInLocal == "V") {
				chFareInLocal = getText("txtCHFareInLocal");
			// }
			// if(selINFInLocal == "V") {
				 inFareInLocal = getText("txtINFInLocal");
			// }
					
		}
		if (chidVisibility == 'false') {
			aedCHL = '100';
			aedINF = '0';
			selCHL = 'P';
			selINF = 'P';
			chFareInLocal = '100';
			chFareInLocal = '0';
			selCHLInLocal = 'P';
			selINFInLocal = 'P';
		}
	}
	var strSearch = strOrigin + "," + selDest + "," + via1 + "," + via2 + ","
			+ via3 + "," + via4 + "," + strToDate + "," + strFeromDate + ","
			+ bc + "," + fc + "," + aed + "," + srchSpnstmt + "," + cos + ","
			+ version + "," + fareID + "," + status + "," + allowReturn + ","
			+ aedCHL + "," + aedINF + "," + selCHL + "," + selINF + ","
			+ slsFromDate + "," + slsToDate + "," 
			+ baseFareInLocal + "," + selCHInLocal + "," + selINFInLocal + ","
			+ chFareInLocal + "," + inFareInLocal + "," + showBaseCurr + "," + showDepAirPoCurr + "," 
			+ strReturnFromDate + "," + strReturnToDate + "," + modToSameFare;

	setTabValues(screenId, delimiter, "strSearchCriteria", strSearch);
}

/**
 * Sets the upper pane value in the onload() which were set at the time of
 * submit
 */
function setFareData() {
	var strSearch = getTabValues(screenId, delimiter, "strSearchCriteria");
	if (strSearch != "") {
		var fieldArr = strSearch.split(",");
		
		objCmb1.setComboValue(fieldArr[0]);
		objCmb2.setComboValue(fieldArr[1]);
		objCmb3.setComboValue(fieldArr[2]);
		objCmb4.setComboValue(fieldArr[3]);
		objCmb5.setComboValue(fieldArr[4]);
		objCmb6.setComboValue(fieldArr[5]);
		
		setField("hdnOrigin",fieldArr[0])
		
		setField("txtToDt", fieldArr[6]);
		setField("txtFromDt", fieldArr[7]);
		setField("txtSlsStDate", fieldArr[21]);
		setField("txtSlsEnDate", fieldArr[22]);
		setField("txtFromDtInbound", fieldArr[30]);
		setField("txtToDtInbound", fieldArr[31]);

		setBookingCode(fieldArr[8]);
		setFareRule(fieldArr[9]);
		setField("txtAED", fieldArr[10]);		
		setField("txtCHL", fieldArr[17]);
		setField("txtINF", fieldArr[18]);
		setField("selCamt", fieldArr[19]);
		setField("selIamt", fieldArr[20]);
		
		setField("txtBaseInLocal", fieldArr[23]);
		setField("selCamtInLocal", fieldArr[24]);
		setField("selIamtInLocal", fieldArr[25]);
		setField("txtCHFareInLocal", fieldArr[26]);
		setField("txtINFInLocal", fieldArr[27]);

		DivWrite("spnFixed", fieldArr[11]);

		setField("hdnFareVersion", fieldArr[13]);
		setField("hdnFareID", fieldArr[14]);
		DivWrite("spnId", fieldArr[14]);

		
		if (fieldArr[15] == "true") {
			setChkControls("chkStatus", true);
		} else {
			setChkControls("chkStatus", false);
		}
		
		if (fieldArr[16] == "true") {
			setChkControls("chkAllowReturn", true);
		} else {
			setChkControls("chkAllowReturn", false);
		}		
		
		if (fieldArr[28] == "true") {
			// setChkControls("radCurr", true);
			getFieldByID("radCurr").checked = true;
		} else {
			// setChkControls("radCurr", false);
			getFieldByID("radCurr").checked = false;
		}
		
		if (fieldArr[29] == "true") {
			// setChkControls("radSelCurr", true);
			getFieldByID("radSelCurr").checked = true;
		} else {
			// setChkControls("radSelCurr", false);
			getFieldByID("radSelCurr").checked = false;
		}	
		
		if (fieldArr[32] == "true") {
			setChkControls("chkModToSameFare", true);
		} else {
			setChkControls("chkModToSameFare", false);
		}
	}
}

/**
 * function to test the date
 */
function testDate(dt) {
	var validated = true;

	var dtC = new Date();
	var dtCM = dtC.getMonth() + 1;
	var dtCD = dtC.getDate();
	if (dtCM < 10) {
		dtCM = "0" + dtCM
	}
	if (dtCD < 10) {
		dtCD = "0" + dtCD
	}
	var strSysDate = systemDate;
	var valid = dateChk(strSysDate);

	var tempDay = dt.substring(0, 2);
	var tempMonth = dt.substring(3, 5);
	var tempYear = dt.substring(6, 10);
	var tempOStartDate = (tempYear + tempMonth + tempDay);

	tempDay = strSysDate.substring(0, 2);
	tempMonth = strSysDate.substring(3, 5);
	tempYear = strSysDate.substring(6, 10);
	var strSysODate = (tempYear + tempMonth + tempDay);

	if (valid) {
		if (tempOStartDate >= strSysODate) {
			validated = false;
		}
	}
	return validated;

}

/*
 * disable the upper pane values according to a condition executes in setData()
 * edit condition
 */
function disableSelectArea() {
	objCmb1.disable(true);
	objCmb2.disable(true);
	objCmb3.disable(true);
	objCmb4.disable(true);
	objCmb5.disable(true);
	objCmb6.disable(true);
	Disable("txtToDt", true);
	Disable("txtFromDt", true);
	Disable("txtSlsStDate", true);
	Disable("txtSlsEnDate", true);
	Disable("selBc", true);
	Disable("selFC", true);
	Disable("txtFBC", true);
	Disable("chkStatus", true);
	Disable("txtAED", true);

	Disable("txtCHL", true);
	Disable("txtINF", true);
	Disable("selCamt", true);
	Disable("selIamt", true);
	Disable("btnAddHocRule", true);
	Disable("btnEdit", true);
	Disable("chkModToSameFare", true);
}

/*
 * sets the pax grid values to be pass to the backe end before save
 */
function setPaxDetails() {
	var pdat = "";
	dat = getText("hdnGrd1") +"~" + getValue("chkADRef") +"~" + getValue("chkADApp") +"~" + getValue("chkADMod") +"~" + getValue("chkADCNX") 
			+"~" +  getText("hdnVersionAD") +"~" +getText("hdnFRPAXIDAD") +"~" +getText("txtADNoShoCharge")
			+"~" + getText("selADCGTYP")
			+"~" + getText("txtADNoShoBreakPoint")
			+"~" + getText("txtADNoShoBoundary")
			+"~" + getText("txtADNoShoChargeInLocal");
	setField("hdnADDetails", dat);
	
	dat = getText("hdnGrd2") +"~" + getValue("chkCHRef") +"~" + getValue("chkCHApp") +"~" + getValue("chkCHMod") +"~" + getValue("chkCHCNX") 
			+"~" +  getText("hdnVersionCH") +"~" +getText("hdnFRPAXIDCH") +"~" +getText("txtCHNoShoCharge")
			+"~" +  getText("selCHCGTYP")
			+"~" +  getText("txtCHNoShoBreakPoint")
			+"~" +  getText("txtCHNoShoBoundary")
			+"~" +  getText("txtCHNoShoChargeInLocal");
	setField("hdnCHDetails", dat);
	
	dat = getText("hdnGrd3") +"~" + getValue("chkINRef") +"~" + getValue("chkINApp") +"~" + getValue("chkINMod") +"~" + getValue("chkINCNX") 
			+"~" +  getText("hdnVersionIN") +"~" +getText("hdnFRPAXIDIN") +"~" +getText("txtINNoShoCharge") 
			+"~" +  getText("selINCGTYP")
			+"~" +  getText("txtINNoShoBreakPoint")
			+"~" +  getText("txtINNoShoBoundary")
			+"~" +  getText("txtINNoShoChargeInLocal");
	setField("hdnINDetails", dat);
}

/**
 * executes when save button is clicked validate and submit the form
 */
function save_click() {
	setField("hdnFareCode", getText("selFC"));
	if (getFieldByID("radSelCurr").checked) {
		setField("hdnLocalCurrencySelected", "true");
	}
	
	if (!saveData()){
		disableInputControls(false);
		return;
	} else {
		top[2].ShowProgress();
		Disable("txtFromDt", false);
		Disable("selCamt", false);
		Disable("txtCHL", false);
		Disable("selIamt", false);
		Disable("txtINF", false);
		setField("hdnfromDate", getText("txtFromDt"));
		setField("hdnfromDateInbound", getText("txtFromDtInbound"));
		Disable("txtSlsStDate", false);
		var strOrigin = objCmb1.getText();
		setField("hdnOrigin", strOrigin);
		var selDest = objCmb2.getText();
		setField("hdnDestination", selDest);
		setField("hdnSalesfromDate", getText("txtSlsStDate"));
		setTabValues(screenId, delimiter, "status", "Save");
		setTabValues(screenId, delimiter, "rulemode", "Save");
		setSearchDataForFare();
		setPaxDetails();
		setField("hdnMode", "SAVE");
		DisablePaxFareConditionsGrid(false);
		document.forms["frmCreateFare"].submit();
		top[2].HideProgress();
	}
}

/**
 * validate the form data executes in save_click()
 * 
 */
function saveData() {
	var validated = false;
	var strOrigin = objCmb1.getText();
	var selDest = objCmb2.getText();
	var via1 = objCmb3.getText();
	var via2 = objCmb4.getText();
	var via4 = objCmb6.getText();
	var via3 = objCmb5.getText();
	var strToDate = getText("txtToDt");
	var strFeromDate = getText("txtFromDt");
	var slsToDate = getText("txtSlsEnDate");
	var slsFeromDate = getText("txtSlsStDate");
	var strBC = getText("selBc");
	var strValBC = getValue("selBc").split(",");
	var fareType = strValBC[4]; // SEGMENT fare OR CONNECTION fare
	var agents = "";
	var strDeptTimeFrom = getText("txtOutTmFrom");
	var strDeptTimeTo = getText("txtOutTmTo");
	var strArrTimeFrom = getText("txtInbTmFrom");
	var strTimeTo = getText("txtInbTmTo");
	var returnFare = getText("radFare");
	var strVisibility = getText("selFareVisibility");
	var strPaxCat = getValue("selPaxCat");
	var strFareCat = getValue("selFareCat");
	var strToDateInbound = getText("txtToDtInbound");
	var strFromDateInbound = getText("txtFromDtInbound");
		
	var fruleStatus = getTabValues(screenId, delimiter, "ruleStatus");	
	
	var strTmpFlexi = getValue("selFlexiCode");
	
	// fare rule modifications and load factor
	var lfMax = getText("loadFactorMax");
	setField("hdnLoadFactorMax", lfMax);
	var lfMin = getText("loadFactorMin");
	setField("hdnLoadFactorMin", lfMin);
		
	var isLoadFactorEnabled = getText("loadFactorEnabled");
	var loadFactorMin = getText("loadFactorMin");
	var loadFactorMax = getText("loadFactorMax");

	//fare rule no show charging mode
	var adultNoShowChargeType = trim(getValue("selADCGTYP"));
	var childNoShowChargeType = trim(getValue("selCHCGTYP"));
	var infantNoShowChargeType = trim(getValue("selINCGTYP"));
	
	var adultNoShowChargeBreakPoint = trim(getText("txtADNoShoBreakPoint")) != "" ? trim(getValue("txtADNoShoBreakPoint")) : 0;
	var childNoShowChargeBreakPoint = trim(getText("txtCHNoShoBreakPoint")) != "" ? trim(getValue("txtCHNoShoBreakPoint")) : 0;
	var infantNoShowChargeBreakPoint = trim(getText("txtINNoShoBreakPoint")) != "" ? trim(getValue("txtINNoShoBreakPoint")) : 0;
	var adultNoShowChargeBoundary = trim(getText("txtADNoShoBoundary")) != "" ? trim(getValue("txtADNoShoBoundary")) : 0;
	var childNoShowChargeBoundary = trim(getText("txtCHNoShoBoundary")) != "" ? trim(getValue("txtCHNoShoBoundary")) : 0;
	var infantNoShowChargeBoundary = trim(getText("txtINNoShoBoundary")) != "" ? trim(getValue("txtINNoShoBoundary")) : 0;
	var adultNoShowCharge = trim(getText("txtADNoShoCharge"));
	var childNoShowCharge = trim(getText("txtCHNoShoCharge"));
	var infantNoShowCharge = trim(getText("txtINNoShoCharge"));

	// fare discount
	var isFareDiscountEnabled = (getText("fareDiscount").toUpperCase() == "Y");
	var fareDiscountMin = getText("fareDiscountMin");
	var fareDiscountMax = getText("fareDiscountMax");
	
	setField("hdnFareDiscountMin", fareDiscountMin);
	setField("hdnFareDiscountMax", fareDiscountMax);

	//agent commission
	var txtAgentCommision = getText("txtAgentCommision");
	
	/*link fares*/
	var masterFareStatus = getText("chkMasterFare");
	var isMasterFareSelected = false;
	
	if(masterFareStatus == 'ON' || masterFareStatus == 'on'){
		isMasterFareSelected = true;
		var masterFareRefCode = getText("masterFareRefCode"); 
		setField("hdnIsMasterFare", masterFareStatus);
		setField("hdnMasterFareRefCode", masterFareRefCode);
	}else{
		isMasterFareSelected = false;
		setField("hdnIsMasterFare", "");
		setField("hdnMasterFareRefCode", "");
	}
	
	var isLinkFareSelected = false;
	if(!isMasterFareSelected){
		var linkFareStatus = getText("chkLinkFare");		
		if(linkFareStatus == 'ON' || linkFareStatus == 'on'){
			isLinkFareSelected = true;
			setField("hdnMasterFareID",document.getElementById('selectedMasterFareID').value);
			setField("hdnMasterFarePercentage",getText("linkFarePercentage"));
		}else{
			setField("hdnMasterFareID","");
			setField("hdnMasterFarePercentage","");
		}
	}else{
		setField("hdnMasterFareID","");
		setField("hdnMasterFarePercentage","");
	}
	
	setONDCodes();
	getAllFareRuleData();
	
	top[2].HideProgress();
	if (updateClicked == true) {
		agents = agentCodes;
	} else {
		agents = data;
	}
	
	setField("hdnBC", strBC);
	var strFC = getText("selFC");
	var strFCVal = getValue("selFC").split(",");

	setField("hdnVisibility", getValue("selFareVisibility"));
	setField("hdnAgents", agents);
	chkBox_Clicked();

	if (fruleStatus != "Edit" && strOrigin == "") {
		showERRMessage(originRqrd);
		objCmb1.focus();		
		return false;
	
	} else if (fruleStatus != "Edit" && selDest == "") {
		showERRMessage(destinationRqrd);
		objCmb2.focus()		
		return false;
	
	} else if (strOrigin == selDest) {
		showERRMessage(originDestEqual);	
		return false;
	
	} else if (objCmb1.getText() != "" && objCmb1.comboValidate() != true) {
		showERRMessage(OriginDoesNotExists);
		objCmb1.focus();
		return false;
	
	} else if (objCmb2.getText() != "" && objCmb2.comboValidate() != true) {
		showERRMessage(DestinationDoesNotExists);
		objCmb2.focus();
		return false;
	
	} else if (getTabValues(screenId, delimiter, "ruleStatus")
			&& strOrigin == via1 || strOrigin == via2 || strOrigin == via3
			|| strOrigin == via4) {
		showERRMessage(arriavlViaSame);
		objCmb1.focus();	
		return false;
	
	} else if (getTabValues(screenId, delimiter, "ruleStatus")
			&& selDest == via1 || selDest == via2 || selDest == via3
			|| selDest == via4) {
		showERRMessage(depatureViaSame);
		objCmb2.focus();		
		return false;
	
	} else if (objCmb3.getText() != "" && objCmb3.comboValidate() != true) {
		showERRMessage(Via1DoesNotExists);
		objCmb3.focus();
		return false;
	
	} else if (objCmb4.getText() != "" && objCmb4.comboValidate() != true) {
		showERRMessage(Via2DoesNotExists);
		objCmb4.focus();
		return false;
	
	} else if (objCmb6.getText() != "" && objCmb6.comboValidate() != true) {
		showERRMessage(Via4DoesNotExists);
		objCmb6.focus();
		return false;
	
	} else if (objCmb5.getText() != "" && objCmb5.comboValidate() != true) {
		showERRMessage(Via3DoesNotExists);
		objCmb5.focus();
		return false;
	
	} else if ((via1 != "") && (via1 == via2 || via1 == via3 || via1 == via4)) {
		showERRMessage(viaEqual);	
		return false;
	
	} else if ((via2 != "") && (via2 == via3 || via2 == via4)) {
		showERRMessage(viaEqual);		
		return false;
	
	} else if ((via3 != "") && (via3 == via4)) {
		showERRMessage(viaEqual);		
		return false;
	
	} else if (strBC == "" || strBC == "Select") {
		showERRMessage(BCnull);
		getFieldByID("selBc").focus();
		return false;
	
	} else if (strValBC[3] != null && strValBC[3] == "INA") {
		showERRMessage(bcInactive);
		getFieldByID("selBc").focus();	
		return false;
	
	} else if ((addHocClicked || getTabValues(screenId, delimiter, "rulemode") == 'Add')
			&& strValBC[4] != null
			&& (strValBC[4] == "SEG" || strValBC[4] == "CON")
			&& (returnFare == 'Return')) {
		showERRMessage(noAlloctype);
		getFieldByID("selBc").focus();	
		return false;
	
	} else if ((addHocClicked || getTabValues(screenId, delimiter, "rulemode") == 'Add')
			&& strValBC[4] != null
			&& strValBC[4] == "RET"
			&& returnFare != 'Return') {
		showERRMessage(returnonly);
		getFieldByID("selBc").focus();	
		return false;
	
	} else if (strValBC[4] != null && strValBC[4] == "SEG" && via1 == "SHJ") {
		showERRMessage(segonly);
		getFieldByID("selBc").focus();	
		return false;
	
	} else if (strValBC[4] != null && strValBC[4] == "CON" && !checkHubs()) {
		showERRMessage(cononly);
		getFieldByID("selBc").focus();	
		return false;
	
	} else if (strValBC[7] != null && strPaxCat != null
			&& trim(strValBC[7]) != trim(strPaxCat)) {
		showERRMessage(catNotmatch);
		getFieldByID("selBc").focus();	
		return false;
	
	} else if (strValBC[8] != null && strFareCat != null
			&& trim(strValBC[8]) != trim(strFareCat)) {
		showERRMessage(fareCatNotmatch);
		getFieldByID("selBc").focus();		
		return false;

	}else if(strValBC[9] == "Y"){
		if(!(strFCVal[4] == "Y" || strTmpFlexi != "-1")){
			showERRMessage(freeFlexiEnabled);
			getFieldByID("selFlexiCode").focus();
			return false;
		}		
	}	
	
	if(isLinkFareSelected){
		if(document.getElementById('selectedMasterFareID').value == null || document.getElementById('selectedMasterFareID').value == ""){
			showERRMessage("Please select a master fare id");
			getFieldByID("selectedMasterFareID").focus();
			return false;
		}else if(document.getElementById('linkFarePercentage').value == null || document.getElementById('linkFarePercentage').value == ""){
			showERRMessage("Please enter link fare percentage");
			getFieldByID("selectedMasterFareID").focus();
			return false;
		}
	}else{
		if (strFeromDate == "") {
			showERRMessage(fromdateRqrd);
			getFieldByID("txtFromDt").focus();
			return false;
		}
	
		if (strFeromDate != "" && !dateValidDate(strFeromDate)) {
			showERRMessage(fromDateIncorrect);
			getFieldByID("txtFromDt").focus();		
			return false;
		
		} else if (!document.getElementById("txtFromDt").disabled
				&& testDate(strFeromDate)) {
			showERRMessage(FromDateExceedsCurrentDate);
			getFieldByID("txtFromDt").focus();	
			return false;
		
		} else if (strToDate == "") {
			showERRMessage(todateRqrd);
			getFieldByID("txtToDt").focus();	
			return false;
		
		} else if (strToDate != "" && !dateValidDate(strToDate)) {
			showERRMessage(toDateIncorrect);
			getFieldByID("txtToDt").focus();	
			return false;
		} else if ((strToDate != "" && strFeromDate != "") && (!CheckDates(strFeromDate, strToDate))) {
			showERRMessage(datesInvalid);
			getFieldByID("txtToDt").focus();
			return false;
		
		} else if (slsFeromDate == "") {
			showERRMessage(slstodateRqrd);
			getFieldByID("txtSlsStDate").focus();	
			return false;
	
		} else if (slsFeromDate != "" && !dateValidDate(slsFeromDate)) {
			showERRMessage(slsfromDateIncorrect);
			getFieldByID("txtSlsStDate").focus();
			return false;
		
		} else if (!document.getElementById("txtSlsStDate").disabled
				&& testDate(slsFeromDate)) {
			showERRMessage(slsfrmDteexceedscurr);
			getFieldByID("txtSlsStDate").focus();		
			return false;
		
		} else if (slsToDate == "") {
			showERRMessage(slsfromdateRqrd);
			getFieldByID("txtSlsEnDate").focus();	
			return false;
		
		} else if (slsToDate != "" && !dateValidDate(slsToDate)) {
			showERRMessage(slstoDateIncorrect);
			getFieldByID("txtSlsEnDate").focus();	
			return false;
	
		} else if ((slsToDate != "" && slsFeromDate != "")
				&& (!CheckDates(slsFeromDate, slsToDate))) {
			showERRMessage(slsfromDateexceeds);
			getFieldByID("txtSlsEnDate").focus();		
			return false;
		
		} else if ((strToDate != "" && slsToDate != "")
				&& (!CheckDates(slsToDate, strToDate))) {
			showERRMessage(slstoDateexceeds);
			getFieldByID("txtSlsEnDate").focus();	
			return false;
		
		} else if (strFromDateInbound == "" && (fareType == "RET" || returnFare == 'Return')) {
			showERRMessage(rtfromdateRqrd);
			getFieldByID("txtFromDtInbound").focus();
			return false;
		} else if(isMasterFareSelected && (masterFareRefCode == null || masterFareRefCode == "")){
			showERRMessage("Please enter master fare ref code");
			getFieldByID("masterFareRefCode").focus();
			return false;
		}
	
		if (strFromDateInbound != "" && !dateValidDate(strFromDateInbound)) {
			showERRMessage(rtfromDateIncorrect);
			getFieldByID("txtFromDtInbound").focus();		
			return false;
		
		} else if (strToDateInbound == "" && (fareType == "RET" || returnFare == 'Return')) {
			showERRMessage(rttodateRqrd);
			getFieldByID("txtToDtInbound").focus();	
			return false;
		
		} else if (strToDateInbound != "" && !dateValidDate(strToDateInbound)) {
			showERRMessage(rttoDateIncorrect);
			getFieldByID("txtToDtInbound").focus();	
			return false;
		} else if ((strToDateInbound != "" && strFromDateInbound != "") && (!CheckDates(strFromDateInbound, strToDateInbound))) {
			showERRMessage(datesInvalid);
			getFieldByID("txtToDtInbound").focus();
			return false;
		}else if ((strFeromDate != "" && strFromDateInbound != "") && (!CheckDates(strFeromDate, strFromDateInbound))) {
			showERRMessage(fromDateexceedsrtFrom);
			getFieldByID("txtFromDtInbound").focus();
			return false;
		}else if (getText("txtAED") == "") {
			showERRMessage(AEDRqrd);
			getFieldByID("txtAED").focus();	
			return false;
		}
		if (getText("txtAED") != "" && isNaN(getText("txtAED"))) {
			showERRMessage(amountNan);
			getFieldByID("txtAED").focus();		
			return false;
		
		} 
	}
	if (strFC == "" && strVisibility == "" && editRuleClicked == false) {
		showERRMessage(masterFareRqrd);
		getFieldByID("selFC").focus();	
		return false;
	
	} else if (getText("selFC") != "" && !isActiveFareRule()) {
		showERRMessage(FareRuleInactive);
		getFieldByID("selFC").focus();	
		return false;
	
	} else if (getText("selFareVisibility").toUpperCase().indexOf("AGENT") != -1
			&& (agents == "")) {
		showERRMessage(agentsRqrd);		
		return false;
	
	} else if (checkInvalidChar(getText("txtaRulesCmnts"), invalidCharacter, "Rules and Comments") != "") {
		showERRMessage(invalidCharacter);
		getFieldByID("txtaRulesCmnts").focus();
		return false;
	} else if (checkInvalidChar(getText("txtaInstructions"), invalidCharacter, "Agent Instructions") != "") {
		showERRMessage(invalidCharacter);
		getFieldByID("txtaInstructions").focus();
		return false;
	} else if (strTmpFlexi != "-1"){		
		for(var i = 1; i < objFC.dataArray.length; i++){			
			if(objFC.dataArray[i][0] == strTmpFlexi && objFC.dataArray[i][2] == "OW" && returnFare == 'Return'){			
			showERRMessage(flexiallocfailed);
			getFieldByID("selFlexiCode").focus();	
			return false;			
			} else if(objFC.dataArray[i][0] == strTmpFlexi && objFC.dataArray[i][2] == "RT" && returnFare != 'Return'){			
			showERRMessage(flexiallocfailed);
			getFieldByID("selFlexiCode").focus();	
			return false;
			}
		}
		
	}else if ( isLoadFactorEnabled == "ON" || isLoadFactorEnabled == "on" ) {
		// fare rule modifications and load factor
		if(loadFactorMin == "" && loadFactorMax == ""){			
			showERRMessage(lfReequired);
			getFieldByID("loadFactorMin").focus();
			return false;
		}else if(loadFactorMin != "" && loadFactorMax == ""){			
			if(parseInt(loadFactorMin,10) < 0){				
				showERRMessage(lfMinZero);
				getFieldByID("loadFactorMin").focus();
				return false;
			}
		}else if(loadFactorMax != "" && loadFactorMin == ""){			
			if(parseInt(loadFactorMax,10) < 0){				
				showERRMessage(lfMaxZero);
				getFieldByID("loadFactorMax").focus();
				return false;
			}
		}else if(loadFactorMin != "" && loadFactorMax != ""){			
			if(parseInt(loadFactorMin,10) < 0){				
				showERRMessage(lfMinZero);
				getFieldByID("loadFactorMin").focus();
				return false;
			}else if(parseInt(loadFactorMax,10) < 0){				
				showERRMessage(lfMaxZero);
				getFieldByID("loadFactorMax").focus();
				return false;
			}else if(parseInt(loadFactorMin,10) > parseInt(loadFactorMax,10)){				
				showERRMessage(lfMinGrMax);
				getFieldByID("loadFactorMin").focus();
				return false;
			}
		}		
		validated = true;
	} else if (fareDiscountVisibiliy && isFareDiscountEnabled){
		if(fareDiscountMin == ""){
			showERRMessage("Fare discount min is required");
			getFieldByID("fareDiscountMin").focus();
			return false;
		}
		if(fareDiscountMax == ""){
			showERRMessage("Fare discount max is required");
			getFieldByID("fareDiscountMin").focus();
			return false;
		}
		
		var fdMin = parseInt(fareDiscountMin, 10);
		var fdMax = parseInt(fareDiscountMax, 10);
		if(fdMin < 0 || fdMin>100){
			showERRMessage("Fare discount min is invalid");
			getFieldByID("fareDiscountMin").focus();
			return false;
		}
		
		if(fdMax < 0 || fdMax>100){
			showERRMessage("Fare discount max is invalid");
			getFieldByID("fareDiscountMax").focus();
			return false;
		}
		
		if(fdMax<fdMin){
			showERRMessage("Fare discount max cannot be less than min");
			getFieldByID("fareDiscountMax").focus();
			return false;
		}
		validated = true;
	}else if (agentFareCommissionVisibility){
		//TODO:CONFIRM: validation based on fare visibility: agent or public
		if(txtAgentCommision == ""){
			showERRMessage("Agent commission value is required");
			getFieldByID("txtAgentCommision").focus();
			return false;
		}
		validated = true;
	}else{
		validated = true;
	}
	
	//No show Charge Types validations
	if (isNoShowChargeTypeNotV(adultNoShowChargeType) && parseFloat(adultNoShowChargeBreakPoint) == 0){
		showERRMessage(noShowBreakPointEmpty);
		getFieldByID("txtADNoShoBreakPoint").focus();
		return false;
	} else if(isNoShowChargeTypeNotV(adultNoShowChargeType) && parseFloat(adultNoShowChargeBoundary) == 0){
		showERRMessage(noShowBoundaryEmpty);
		getFieldByID("txtADNoShoBoundary").focus();
		return false;
	} else if(parseFloat(adultNoShowChargeBreakPoint) > parseFloat(adultNoShowChargeBoundary)){
		showERRMessage(noShowBoundaryBreakPoint);
		getFieldByID("txtADNoShoBoundary").focus();
		return false;
	} else if(isNoShowChargeTypeNotV(adultNoShowChargeType) && (parseFloat(adultNoShowCharge)==0 || parseFloat(adultNoShowCharge)>100)){
		showERRMessage(noShowChargePercentage);
		getFieldByID("txtADNoShoCharge").focus();
		return false;
	} else if(isNoShowChargeTypeNotV(childNoShowChargeType) && parseFloat(childNoShowChargeBreakPoint) == 0){
		showERRMessage(noShowBreakPointEmpty);
		getFieldByID("txtCHNoShoBreakPoint").focus();
		return false;
	}  else if(isNoShowChargeTypeNotV(childNoShowChargeType) && parseFloat(childNoShowChargeBoundary) == 0){
		showERRMessage(noShowBoundaryEmpty);
		getFieldByID("txtCHNoShoBoundary").focus();
		return false;
	} else if(parseFloat(childNoShowChargeBreakPoint) > parseFloat(childNoShowChargeBoundary)){
		showERRMessage(noShowBoundaryBreakPoint);
		getFieldByID("txtCHNoShoBoundary").focus();
		return false;
	} else if(isNoShowChargeTypeNotV(childNoShowChargeType) && (parseFloat(childNoShowCharge)==0 ||parseFloat(childNoShowCharge)>100)){
		showERRMessage(noShowChargePercentage);
		getFieldByID("txtCHNoShoCharge").focus();
		return false;
	} else if(isNoShowChargeTypeNotV(infantNoShowChargeType) && parseFloat(infantNoShowChargeBreakPoint) == 0){
		showERRMessage(noShowBreakPointEmpty);
		getFieldByID("txtINNoShoBreakPoint").focus();
		return false;
	} else if(isNoShowChargeTypeNotV(infantNoShowChargeType) && parseFloat(infantNoShowChargeBoundary) == 0){
		showERRMessage(noShowBoundaryEmpty);
		getFieldByID("txtINNoShoBoundary").focus();
		return false;
	} else if(parseFloat(infantNoShowChargeBreakPoint) > parseFloat(infantNoShowChargeBoundary)){
		showERRMessage(noShowBoundaryBreakPoint);
		getFieldByID("txtINNoShoBoundary").focus();
		return false;
	} else if(isNoShowChargeTypeNotV(infantNoShowChargeType) && (parseFloat(infantNoShowCharge)==0 ||parseFloat(infantNoShowCharge)>100 )){
		showERRMessage(noShowChargePercentage);
		getFieldByID("txtINNoShoCharge").focus();
		return false;
	}

	if(!isLinkFareSelected){
		if (showFareDefByDepCountryCurrency == "Y") {
			
			var objRad1 = getFieldByID("radCurr");	
			var objRad2 = getFieldByID("radSelCurr");
			
			if (objRad1.checked && getText("txtAED") != "" && parseFloat(getText("txtAED")) <= 0) {
				var confirmed = confirm("Base Fare Amount is Zero. Do you wish to continue?");
				if (confirmed) {
					validated = true;
					setField("hdnOrigin", strOrigin);
					setField("hdnDestination", selDest);
					setField("hdnVia1", via1);
					setField("hdnVia2", via2);
					setField("hdnVia3", via3);
					setField("hdnVia4", via4);
				} else {			
					return false;
				}
			} else if (objRad2.checked && getText("txtBaseInLocal") != "" && parseFloat(getText("txtBaseInLocal")) <= 0) {
				var confirmed = confirm("Fare Amount in departing country currency is Zero. Do you wish to continue?");
				if (confirmed) {
					validated = true;
					setField("hdnOrigin", strOrigin);
					setField("hdnDestination", selDest);
					setField("hdnVia1", via1);
					setField("hdnVia2", via2);
					setField("hdnVia3", via3);
					setField("hdnVia4", via4);
				} else {			
					return false;
				} 
			} else {
				if (validateRule() == true) {
					setField("hdnOrigin", strOrigin);
					setField("hdnDestination", selDest);
					setField("hdnVia1", via1);
					setField("hdnVia2", via2);
					setField("hdnVia3", via3);
					setField("hdnVia4", via4);
					top[2].HidePageMessage();			
					return true;
				} else {			
					return false;
				}
			}
		} else {
			if (getText("txtAED") != "" && parseFloat(getText("txtAED")) <= 0) {
				var confirmed = confirm("Base Fare Amount is Zero. Do you wish to continue?");
				if (confirmed) {
					validated = true;
					setField("hdnOrigin", strOrigin);
					setField("hdnDestination", selDest);
					setField("hdnVia1", via1);
					setField("hdnVia2", via2);
					setField("hdnVia3", via3);
					setField("hdnVia4", via4);
				} else {			
					return false;
				}
			} else {
				if (validateRule() == true) {
					setField("hdnOrigin", strOrigin);
					setField("hdnDestination", selDest);
					setField("hdnVia1", via1);
					setField("hdnVia2", via2);
					setField("hdnVia3", via3);
					setField("hdnVia4", via4);
					top[2].HidePageMessage();			
					return true;
				} else {			
					return false;
				}
			  }
		}
	
	
		if (getText("txtCHL") == "" || isNaN(getText("txtCHL"))) {
			setField("txtCHL", "0.00");
		}
		if (getText("txtINF") == "" || isNaN(getText("txtINF"))) {
			setField("txtINF", "0.00");
		}
	}
	return validated;
}

//Checks if only the minimum stayover time is defined. 
function isOnlyMinimumStayOverDefined(){
	var intMaxStayMonths = getText("txtMaxStayMonths");
	var intMinStayMonths = getText("txtMinStayMonths");
	var intMaxStay = getText("txtMaxStayDays");
	var intMinStay = getText("txtMinStayDays");
	var intMaxHours = getText("txtMaxStayHours");
	var intMinHours = getText("txtMinStayHours");
	var intMaxMins = getText("txtMaxStayMins");
	var intMinMins = getText("txtMinStayMins");
	if((intMaxStayMonths == "" && intMaxStay == "" && intMaxHours =="" && intMaxMins == "")
			&& (intMinStayMonths != "" || intMinStay != "" || intMinHours != "" || intMinMins != "")){
		return true;
	}else{
		return false;
	}
}
/**
 * returns true if at least one via poit is a hub need checking of this logic
 * executes in side the savedata validation
 */
function checkHubs() {
	var via1 = objCmb3.getText();
	var via2 = objCmb4.getText();
	var via4 = objCmb6.getText();
	var via3 = objCmb5.getText();

	var flag = false;
	for ( var count = 0; count < hubs.length; count++) {
		if (hubs[count] == via1 || hubs[count] == via2 || hubs[count] == via3
				|| hubs[count] == via4) {
			flag = true;
		}
	}
	return flag;
}

/**
 * executes in side the savedata validation method validate the fare rule(lower
 * pane)
 * 
 */
function validateRule() {
	validated = false;
	var strVisibility = getText("selFareVisibility");
	var strMaxMonths = getText("txtMaxStayMonths");
	var strMinMonths = getText("txtMinStayMonths");
	var strmaxStay = getText("txtMaxStayDays");
	var strminStay = getText("txtMinStayDays");
	var strmaxHours = getText("txtMaxStayHours");
	var strminHours = getText("txtMinStayHours");
	var strmaxMins = getText("txtMaxStayMins");
	var strminMins = getText("txtMinStayMins");
	var chkAdv = getText("chkAdvance");
	var txtAdv = getText("txtAdvBookDays");
	var txtAdvHrs = getText("txtAdvBookHours");
	var txtAdvMins = getText("txtAdvBookMins");
	var chkRest = getText("chkChrgRestrictions");
	var strMod = getText("txtModification");
	var strCancel = getText("txtCancellation");
	var returnFare = getText("radFare");
	
	var selCancType = getValue("selDefineCanType");
	var selModType = getValue("selDefineModType");
	var txtMinVal_mod = getText("txtMin_mod");
	var txtMaxVal_mod = getText("txtMax_mod");
	var txtMinVal_canc = getText("txtMin_canc");
	var txtMaxVal_canc = getText("txtMax_canc");
	
	var chkOpenRT = getText("chkOpenRT"); 
	var strConfrmMonths = getText("txtConfirmMonths");
	var strConfrmDays = getText("txtConfirmDays");
	var strConfrmHours = getText("txtConfirmHours");
	var strConfrmMins = getText("txtConfirmMins");
	
	var chkHRT = getText("chkHRT");
	
	var intChgModInLocal = isNaN(getText("txtModificationInLocal"));
	var intChgCancelInLocal = isNaN(getText("txtCancellationInLocal"));
	
	var currencyType = getValue("radCurrFareRule");
	var selCurrencyCode = getValue("selCurrencyCode");
	var isLocalCurrency = false;
	
	if(currencyType=="SEL_CURR" && selCurrencyCode!="-1"){
		isLocalCurrency = true;
	}
	var strChgModInLocal = getText("txtModificationInLocal");
	var strChgCancelInLocal = getText("txtCancellationInLocal");
	
	
	var intConfirmMonths = (strConfrmMonths != "") ? (parseInt(strConfrmMonths) * 30 * 24 * 60 )	: 0;
	var intConfirmDays = (strConfrmDays != "") ? (parseInt(strConfrmDays) * 24 * 60 )	: 0;
	var intConfirmHours = (strConfrmHours != "") ? (parseInt(strConfrmHours) * 60 )	: 0;
	var intConfirmMins = (strConfrmMins != "") ? parseInt(strConfrmMins) 	: 0;
	
	var intConfirmTimeinMins = Number(intConfirmMonths) + Number(intConfirmDays) + Number(strConfrmHours) + Number(strConfrmMins);

	var strDeptTimeFrom = getText("txtOutTmFrom");
	var strDeptTimeTo = getText("txtOutTmTo");
	var strArrTimeFrom = getText("txtInbTmFrom");
	var strTimeTo = getText("txtInbTmTo");

	var strModBufDays = getText("txtModBufDays");
	var strModBudHours = getText("txtModBufHours");
	var strModBufMins = getText("txtModBufMins");

	var intModBufDaysInMilSec = (strModBufDays != "") ? (parseInt(strModBufDays) * 24 * 60 * 60 * 1000)	: 0;
	var intModBufHoursInMilSec = (strModBudHours != "") ? parseInt(strModBudHours) * 60 * 60 * 1000	: 0;
	var intModbufMinsInMilSec = (strModBufMins != "") ? parseInt(strModBufMins) * 60 * 1000	: 0;

	var strModBufTimeInMilSec = strModBufDays + strModBudHours + strModBufMins;

	var intModBufTimeinMilSec = intModBufDaysInMilSec + intModBufHoursInMilSec	+ intModbufMinsInMilSec;
	
	var strModBufDaysDom = getText("txtModBufDaysDom");
	var strModBudHoursDom = getText("txtModBufHoursDom");
	var strModBufMinsDom = getText("txtModBufMinsDom");

	var intModBufDaysDomInMilSec = (strModBufDaysDom != "") ? (parseInt(strModBufDaysDom) * 24 * 60 * 60 * 1000)	: 0;
	var intModBufHoursDomInMilSec = (strModBudHoursDom != "") ? parseInt(strModBudHoursDom) * 60 * 60 * 1000	: 0;
	var intModbufMinsDomInMilSec = (strModBufMinsDom != "") ? parseInt(strModBufMinsDom) * 60 * 1000	: 0;

	var strModBufTimeDomInMilSec = strModBufDaysDom + strModBudHoursDom + strModBufMinsDom;

	var intModBufTimeDominMilSec = intModBufDaysDomInMilSec + intModBufHoursDomInMilSec	+ intModbufMinsDomInMilSec;

	var advBooking = getText("txtAdvBookDays");
	var intadvBooking = isNaN(advBooking);
	var advBookingHrs = getText("txtAdvBookHours");
	var intadvBookingHrs = isNaN(advBookingHrs);
	var advBookingMins = getText("txtAdvBookMins");
	var intadvBookingMins = isNaN(advBookingMins);
	var intMaxMonths = isNaN(getText("txtMaxStayMonths"));
	var intMinMonths = isNaN(getText("txtMinStayMonths"));
	var intMaxStay = isNaN(getText("txtMaxStayDays"));
	var intMinStay = isNaN(getText("txtMinStayDays"));
	var intMaxHours = isNaN(getText("txtMaxStayHours"));
	var intMinHours = isNaN(getText("txtMinStayHours"));
	var intMaxMins = isNaN(getText("txtMaxStayMins"));
	var intMinMins = isNaN(getText("txtMinStayMins"));
	var intChgMod = isNaN(getText("txtModification"));
	var intChgCancel = isNaN(getText("txtCancellation"));

	var isReturn = (returnFare == "Return");
	var isOpenRT = getValue("chkOpenRT").checked;
	var isHRT = getValue("chkHRT").checked;
	
	var maxDays = 0;
	var minDays = 0;
	if (!intMaxMonths && (strMaxMonths != ""))
		maxDays = maxDays + (parseInt(strMaxMonths) * 30 * 24 * 60);
	if (!intMinMonths && (strMinMonths != ""))
		minDays = minDays + (parseInt(strMinMonths) * 30 * 24 * 60);
	if (!intMaxStay && strmaxStay != "")
		maxDays = maxDays + (parseInt(strmaxStay) * 24 * 60);
	if (!intMinStay && strminStay != "")
		minDays = minDays + (parseInt(strminStay) * 24 * 60);
	if (!intMaxHours && strmaxHours != "")
		maxDays = maxDays + (parseInt(strmaxHours) * 60);
	if (!intMinHours && strminHours != "")
		minDays = minDays + (parseInt(strminHours) * 60);
	if (!intMaxMins && strmaxMins != "")
		maxDays = maxDays + parseInt(strmaxMins);
	if (!intMinMins && strminMins != "")
		minDays = minDays + parseInt(strminMins);
	if (addHocClicked == true || editRuleClicked == true) {
		if (strVisibility == "") {
			showERRMessage(visibilityRqrd);
			getFieldByID("selFareVisibility").focus();
		}else if(currencyType=="SEL_CURR" && selCurrencyCode=="-1"){
			showERRMessage(localCurrencyCodeRqrd);
			getFieldByID("selCurrencyCode").focus();

		} else if ((chkRest == "ON" ||chkRest == "on") && !isLocalCurrency && (strMod == "" && strCancel == "")) {
			showERRMessage(restrictionReqd);
			getFieldByID("txtModification").focus();
		} else if ((chkRest == "ON" ||chkRest == "on") && isLocalCurrency && (strChgModInLocal == "" && strChgCancelInLocal == "")) {
			showERRMessage(restrictionReqd);
			getFieldByID("txtModificationInLocal").focus();
		} else if ((chkRest == "ON" ||chkRest == "on") && !isLocalCurrency && intChgMod) {
			showERRMessage(modNaN);
			getFieldByID("txtModification").focus();
		} else if ((chkRest == "ON" ||chkRest == "on") && isLocalCurrency && intChgModInLocal) {
			showERRMessage(modNaN);
			getFieldByID("txtModificationInLocal").focus();				
		} else if ((chkRest == "ON" || chkRest == "on") && selModType == "") {
			showERRMessage(modTypeNotSelected);
			getFieldByID("selDefineModType").focus();
			
		}else if ( selModType != selModeTyps.v && !isLocalCurrency && (Number(strMod) < 0 ||  Number(strMod) > 100)){
			showERRMessage(modPrecentage);
			getFieldByID("txtModification").focus();
		}else if ( selModType != selModeTyps.v && isLocalCurrency && (Number(strChgModInLocal) < 0 ||  Number(strChgModInLocal) > 100)){
			showERRMessage(modPrecentage);
			getFieldByID("txtModificationInLocal").focus();	
			
		}  else if ((chkRest == "ON" || chkRest == "on") && txtMinVal_mod != "" && isNaN(txtMinVal_mod)) {
			showERRMessage(typeMinNaN);
			getFieldByID("txtMin_mod").focus();
			
		}  else if ((chkRest == "ON" || chkRest == "on") && txtMaxVal_mod != "" && isNaN(txtMaxVal_mod)) {			
			showERRMessage(typeMaxNaN);			
			getFieldByID("txtMax_mod").focus();		
			
		}else if ((chkRest == "ON" || chkRest == "on")&& selModType != selModeTyps.v && txtMinVal_mod == "" && txtMaxVal_mod == ""){
			showERRMessage(modMinMaxEmpty);
			getFieldByID("txtMin_mod").focus();
			
		}else if ((chkRest == "ON" || chkRest == "on") && txtMaxVal_mod != "" && txtMinVal_mod != "" &&
				Number(txtMaxVal_mod)< Number(txtMinVal_mod)){
			showERRMessage(modMaxShouldBeGreater);
			getFieldByID("txtMax_mod").focus();
						
		} else if ((chkRest == "ON" ||chkRest == "on") && !isLocalCurrency && intChgCancel) {
			showERRMessage(cancellationNaN);
			getFieldByID("txtCancellation").focus();
		} else if ((chkRest == "ON" ||chkRest == "on") && isLocalCurrency && intChgCancelInLocal) {
			showERRMessage(cancellationNaN);
			getFieldByID("txtCancellationInLocal").focus();	
		
		} else if ((chkRest == "ON" || chkRest == "on") && selCancType == "") {
			showERRMessage(cancTypeNotSelected);
			getFieldByID("selDefineCanType").focus();
			
		}else if ((chkRest == "ON" || chkRest == "on")&& selModType != selModeTyps.v && txtMaxVal_mod <=0){
			showERRMessage(maxModZero);
			getFieldByID("txtMax_mod").focus();
			
		}else if (selCancType != selModeTyps.v && !isLocalCurrency && (Number(strCancel) < 0 || Number(strCancel) > 100)){
			showERRMessage(cancPrecentage);
			getFieldByID("txtCancellation").focus();
		}else if (selCancType != selModeTyps.v && isLocalCurrency && (Number(strChgCancelInLocal) < 0 || Number(strChgCancelInLocal) > 100)){
			showERRMessage(cancPrecentage);
			getFieldByID("txtCancellationInLocal").focus();	
			
		}  else if ( (chkRest == "ON" || chkRest == "on") && txtMinVal_canc != "" && isNaN(txtMinVal_canc)) {
			showERRMessage(typeMinNaN);
			getFieldByID("txtMin_canc").focus();
			
		}  else if ( (chkRest == "ON" || chkRest == "on") && txtMaxVal_canc != "" && isNaN(txtMaxVal_canc)) {
			showERRMessage(typeMaxNaN);
			getFieldByID("txtMax_canc").focus();
			
		}else if ((chkRest == "ON" || chkRest == "on")&& selCancType != selModeTyps.v && txtMaxVal_canc == "" && txtMinVal_canc == ""){
			showERRMessage(cancMinMaxEmpty);
			getFieldByID("txtMin_canc").focus();
			
		}else if ((chkRest == "ON" || chkRest == "on") && txtMaxVal_canc != "" && txtMinVal_canc != "" &&
				Number(txtMaxVal_canc)< Number(txtMinVal_canc)){
			showERRMessage(cancMaxShouldBeGreater);
			getFieldByID("txtMin_canc").focus();
			
		}else if ((chkRest == "ON" || chkRest == "on")&& selCancType != selModeTyps.v && txtMaxVal_canc <=0){
			showERRMessage(maxCancZero);
			getFieldByID("txtMax_canc").focus();	
			
		} else if (chkAdv == "on" && txtAdv == "" && txtAdvHrs == "" && txtAdvMins == "") {
			showERRMessage(advBKDaysReqd);
			getFieldByID("txtAdvBookDays").focus();

		} else if (chkAdv == "on" && intadvBooking && intadvBookingHrs && intadvBookingMins) {
			showERRMessage(advanceBkNan);
			getFieldByID("txtAdvBookDays").focus();

		} else if (strDeptTimeFrom == "") {
			showERRMessage(depatureFromTimeRqrd);
			getFieldByID("txtOutTmFrom").focus();

		} else if (strDeptTimeTo == "") {
			showERRMessage(depatureToTimeRqrd);
			getFieldByID("txtOutTmTo").focus();

		} else if (returnFare == "") {
			showERRMessage(fareTypeReqd);
			getFieldByID("txtOutTmTo").focus();
		
		} else if (isReturn && strArrTimeFrom == "") {
			showERRMessage(arrivalFromTimeRqrd);
			getFieldByID("txtInbTmFrom").focus();
		
		} else if (isReturn && strTimeTo == "") {
			showERRMessage(arrivalToTimeRqrd);
			getFieldByID("txtInbTmTo").focus();
		
		} else if (isReturn && (getText("txtMinStayMonths") != "" && intMinMonths)
				|| (getText("txtMinStayDays") != "" && intMinStay)
				|| (getText("txtMinStayHours") != "" && intMinHours)
				|| (getText("txtMinStayMins") != "" && intMinMins)) {
			showERRMessage(minStayNaN);
			getFieldByID("txtMinStayMonths").focus();
		
		} else if (returnFare != "Return"
				&& (trim(strMaxMonths) != "" || trim(strmaxStay) != "" || trim(strmaxHours) != "" || trim(strmaxMins) != "")) {
			showERRMessage(maxstayOverShouldEmpty)
			getFieldByID("txtMaxStayMonths").focus();
		
		} else if (returnFare != "Return"
				&& (trim(strMinMonths) != "" || trim(strminStay) != "" || trim(strminHours) != "" || trim(strminMins) != "")) {
			showERRMessage(minstayOverShouldEmpty);
			getFieldByID("txtMinStayMonths").focus();
		
		} else if (isReturn && (getText("txtMaxStayMonths") != "" && intMaxMonths)
				|| (getText("txtMaxStayDays") != "" && intMaxStay)
				|| (getText("txtMaxStayHours") != "" && intMaxHours)
				|| (getText("txtMaxStayMins") != "" && intMaxMins)) {
			showERRMessage(maxStayNaN);
			getFieldByID("txtMaxStayMonths").focus();
			
		} else if (chkOpenRT == "on" && maxDays == 0) {
			showERRMessage(maxStayRequired);
			getFieldByID("txtMaxStayMonths").focus();
		
		} else if (maxDays < intConfirmTimeinMins){
			showERRMessage(confPeriodLarger);
			getFieldByID("txtConfirmMonths").focus();
			
		} else if (isReturn && parseInt(strmaxMins) > 59) {
			showERRMessage(maxMiniutes);
			getFieldByID("txtMaxStayMins").focus();
		
		} else if (isReturn && parseInt(strminMins) > 59) {
			showERRMessage(maxMiniutes);
			getFieldByID("txtMinStayMins").focus();
		
		} else if (isReturn && parseInt(strmaxHours) > 23) {
			showERRMessage(maxhours);
			getFieldByID("txtMaxStayHours").focus();
		
		} else if (isReturn && parseInt(strminHours) > 23) {
			showERRMessage(maxhours);
			getFieldByID("txtMinStayHours").focus();
		
		} else if (returnFare != "Return" && (maxDays < 0)) {
			showERRMessage(stayOverShouldnotBeminus);
			getFieldByID("txtMaxStayDays").focus();
		
		} else if (isReturn && (minDays < 0)) {
			showERRMessage(stayOverShouldnotBeminus);
			getFieldByID("txtMinStayDays").focus();
		
		} else if (isReturn && (maxDays < minDays) && !isOnlyMinimumStayOverDefined()) {
			showERRMessage(maxstayOverShouldBeGreater)
			getFieldByID("txtMinStayMonths").focus();
			
		} else if (isReturn && (minDays < 0)) {
				showERRMessage(stayOverShouldnotBeminus);
				getFieldByID("txtMinStayMonths").focus();
			
		} 	else if (isReturn && !chk_Checked(arrDays)) {
			showERRMessage(arrivaldaysRqrd);
			getFieldByID("chkInbMon").focus();
		
		} else if (returnFare != "Return" && chk_Checked(arrDays)) {
			showERRMessage(arrivaltimeShouldEmpty);
			getFieldByID("chkOutMon").focus();
		
		} else if (!chk_Checked(depDays)) {
			showERRMessage(deptdaysRqrd)
			getFieldByID("chkOutMon").focus();
		
		} else if (!IsValidTime(trim(strDeptTimeFrom))) {
			showERRMessage(depttimeFormat)
			getFieldByID("txtOutTmFrom").focus();
		
		} else if (!IsValidTime(trim(strDeptTimeTo))) {
			showERRMessage(depttimeFormat)
			getFieldByID("txtOutTmTo").focus();
		
		} else if (isReturn && (!IsValidTime(trim(strArrTimeFrom)))) {
			showERRMessage(arrivaltimeFormat);
			getFieldByID("txtInbTmFrom").focus();
		
		} else if (isReturn && (!IsValidTime(trim(strTimeTo)))) {
			showERRMessage(arrivaltimeFormat);
			getFieldByID("txtInbTmTo").focus();
		
		} else if (isReturn && compareTime(strArrTimeFrom, strTimeTo) == false) {
			showERRMessage(ArrFromTmExceeds);
			getFieldByID("txtInbTmFrom").focus();
		
		} else if (compareTime(strDeptTimeFrom, strDeptTimeTo) == false) {
			showERRMessage(DeptFromTmExceeds);
			getFieldByID("txtOutTmTo").focus();

		} else if ((strModBudHours != "") && (parseInt(strModBudHours) > 23)) {
			showERRMessage(maxhours);
			getFieldByID("txtModBufHours").focus();

		} else if ((strModBufMins != "") && (parseInt(strModBufMins) > 59)) {
			showERRMessage(maxMiniutes);
			getFieldByID("txtModBufMins").focus();

		} else if ((strModBufTimeInMilSec != "")
				&& (intModBufTimeinMilSec < parseInt(bufTimeInMilSec))) {
			showERRMessage(modBufTimeShouldBeGreater + bufferTime);
			getFieldByID("txtModBufDays").focus();

		} else if ((strModBufTimeDomInMilSec != "")
				&& (intModBufTimeDominMilSec < parseInt(domesticBufTimeInMilSec))) {
			showERRMessage(domesticModBufTimeShouldBeGreater + domesticBufferTime);
			getFieldByID("txtModBufDaysDom").focus();

		} else {
			top[2].HidePageMessage();
			validated = true;
		}
	} else {
		validated = true;
	}
	return validated;
}

/**
 * returns true at least on day is selected used for both inbound & out bound in
 * side validation
 */
function chk_Checked(arr) {
	var validated = false;
	for ( var i = 0; i < arr.length; i++) {
		if (getText(arr[i]) == "on") {
			validated = true;
			break;
		}
	}
	return validated;
}

/**
 * Executes when add button is clicked sets the tab search data & load the fare
 * rule from back end
 */
function add_click() {	
	setTabValues(screenId, delimiter, "status", "Add");
	setTabValues(screenId, delimiter, "rulemode", "Add");
	setSearchDataForFare();
	setField("hdnFareCode", getText("selFC"));	
	setField("hdnMode", "ADD");
	addHocClicked = false;
	top[2].ShowProgress();	
	document.forms["frmCreateFare"].submit();
}

function isActiveFareRule(){
	if(getValue("selFC") != ""){
	var strValBC = getValue("selFC").split(",");
		if(strValBC[1] == "ACT"){
			return true;
		}else{
			return false;
		}
	} else {
		return false;
	}
}

function getAirportCurr(){
		
	setTabValues(screenId, delimiter, "status", "SetLocalCurr");
	setTabValues(screenId, delimiter, "rulemode", "SetLocalCurr");
	setSearchDataForFare();
	setField("hdnFareCode", getText("selFC"));	
	
	getFieldByID("radCurr").checked = false;
	if(getFieldByID("radSelCurr").checked) {

		getFieldByID("radSelCurr").checked = true;		
		
		getFieldByID("txtAED").readOnly = true;
		getFieldByID("txtCHL").readOnly = true;
		getFieldByID("txtINF").readOnly = true;
		getFieldByID("selCamt").readOnly = true;
		getFieldByID("selIamt").readOnly = true;
				
		getFieldByID("txtBaseInLocal").readOnly = false;
		getFieldByID("txtCHFareInLocal").readOnly = false;
		getFieldByID("txtINFInLocal").readOnly = false;
		getFieldByID("selCamtInLocal").readOnly = false;
		getFieldByID("selIamtInLocal").readOnly = false;
	} 
	
	var strOrigin = objCmb1.getText();
	setField("hdnOrigin", strOrigin);
	setField("hdnMode", "SET_LOCAL_CURR");
	addHocClicked = false;	
	document.forms["frmCreateFare"].submit();
	
}

/**
 * Sets the hidden variables for inbound & outbound days executes in savedata()
 * validation method before the validations
 */
function chkBox_Clicked() {
	var arr = "";
	var dept = "";
	var arrStr = "";
	var deptStr = "";

	for ( var i = 0; i < arrDays.length; i++) {
		if (getText(arrDays[i]) == "on") {
			arr += arrDays[i] + ",";
			arrStr += "1,"
		} else {
			arrStr += "0,";
		}
		if (getText(depDays[i]) == "on") {
			dept += depDays[i] + ",";
			deptStr += "1,";
		} else {
			deptStr += "0,"
		}
	}
	setField("hdndeptDays", dept);
	setField("hdnarrDays", arr);
	setField("hdnDeptIntDays", deptStr);
	setField("hdnArrIntDays", arrStr);
}

// sets the agent data passed from the fare grid and sets the agent link control
function setAgentData() {
	var arrayData = getTabValues(screenId, delimiter, "arrayData");
	if (arrayData != "" && arrayData != null) {
		data = arrayData;
		if (data != "") {
			Disable("btnLnkAgent", false);
		}
	}
}

/**
 * sets the fare rule data executes in reset(),setData() edit condition ,
 * winonload() add condition
 */
function setFareRuleData(arrFares) {
	var fareRules = arrFares;
	var maxStayOver = fareRules[2].split("&");
	var minStayOver = fareRules[3].split("&");
	var openConfirm = fareRules[25].split("&");
	
	var controlArr = new Array("txtAdvBookDays", "radFare", "txtMaxStayDays","txtMinStayDays",
			"txtModification", "txtCancellation",	"txtOutTmTo", "txtOutTmFrom", "txtInbTmTo",
			"txtInbTmFrom",	"txtaRulesCmnts", "chkRefundable", "chkStatus", "selFareVisibility",
			"txtMaxStayHours", "txtMinStayHours","txtMaxStayMins", "txtMinStayMins", "txtModBufDays",
			"txtModBufHours", "txtModBufMins", "chkPrintExp", "txtMaxStayMonths", "txtMinStayMonths",
			"chkOpenRT","txtConfirmMonths","txtConfirmDays","txtConfirmHours","txtConfirmMins","txtFBC",
			"selDefineModType","selDefineCanType", "txtMin_mod", "txtMax_mod", "txtMin_canc", "txtMax_canc",
			"txtModBufDaysDom", "txtModBufHoursDom", "txtModBufMinsDom","txtaInstructions","txtModificationInLocal",
			"txtCancellationInLocal");

	if (fareRules.length > 0) {	
		var advBkTimeDDHHMM = fareRules[0].split(":", 3);
		setField(controlArr[0], advBkTimeDDHHMM[0]);
		setField("txtAdvBookHours", advBkTimeDDHHMM[1]);
		setField("txtAdvBookMins", advBkTimeDDHHMM[2]);
		if(fareRules[0] != "" && fareRules[0] != "::")			
			setChkControls("chkAdvance", true);
		else
			setChkControls("chkAdvance", false);			
		
		if (fareRules[1] == "Y") {
			setField(controlArr[1], "Return");
		} else {
			setField(controlArr[1], "OneWay");
		}
		if (getValue("radFare") == "Return") {			
			setField(controlArr[22], maxStayOver[0]);
			setField(controlArr[2],  maxStayOver[1]);
			setField(controlArr[14], maxStayOver[2]);
			setField(controlArr[16], maxStayOver[3]);
			
			setField(controlArr[23], minStayOver[0]);
			setField(controlArr[3],  minStayOver[1]);
			setField(controlArr[15], minStayOver[2]);
			setField(controlArr[17], minStayOver[3]);
			
			if (fareRules[23] == 'Y') {
				getFieldByID(controlArr[21]).checked = true;
			}
			
			createReturnFlexiCodes();
		}
		
		if (fareRules[4] > 0 || fareRules[5] > 0) {				
			setChkControls("chkChrgRestrictions", true);		
			setField(controlArr[30], fareRules[28]);
			setField(controlArr[31], fareRules[29]);
			setField(controlArr[32], fareRules[30]);
			setField(controlArr[33], fareRules[31]);
			setField(controlArr[34], fareRules[32]);
			setField(controlArr[35], fareRules[33]);
		}
		if (fareRules[4] > 0) {
			setField(controlArr[4], fareRules[4]);
		}	
		if (fareRules[5] > 0) {
			setField(controlArr[5], fareRules[5]);
		}
		
		if (fareRules[47] > 0) {
			setField(controlArr[40], fareRules[47]);
		}	
		if (fareRules[48] > 0) {
			setField(controlArr[41], fareRules[48]);
		}
		
		
		setField(controlArr[6], fareRules[6]);
		setField(controlArr[7], fareRules[7]);
		setField(controlArr[8], fareRules[8]);
		setField(controlArr[9], fareRules[9]);
		setField(controlArr[10], fareRules[10]);
		setField(controlArr[39], fareRules[45])
	
		if (fareRules[11] != "null" && fareRules[11] == "Y" && fareRules[11] != null) {			
			setChkControls("chkRefundable", true);			
			setField(controlArr[11], fareRules[11]);
		} else {			
			setChkControls("chkRefundable", false);
		}

		var dayArr = new Array("Mo", "Tu", "We", "Th", "Fr", "Sa", "Su");
		
		var days = fareRules[13].split(":");
		// umesh@@
		var arrDaysTemp = new Array("chkInbMon", "chkInbTues", "chkInbWed", "chkInbThu", "chkInbFri", "chkInbSat","chkInbSun");
		var depDaysTemp = new Array("chkOutMon", "chkOutTues", "chkOutWed", "chkOutThu", "chkOutFri", "chkOutSat" , "chkOutSun");
		
		for ( var f = 0; f < depDays.length; f++) {
			if (days[f] == dayArr[f]) {				
				setChkControls(depDaysTemp[f], true);				
			} else {				
				setChkControls(depDaysTemp[f], false);				
			}
		}
		var days = fareRules[14].split(":");
		for ( var f = 0; f < arrDays.length; f++) {
			if (days[f] == dayArr[f]) {				
				setChkControls(arrDaysTemp[f], true);				
			} else {				
				setChkControls(arrDaysTemp[f], false);				
			}
		}
		
		for ( var t = 0; t < getFieldByID("selFareVisibility").options.length; t++) {
			getFieldByID("selFareVisibility").options[t].selected = false;
		}
		if (fareRules[15] != "null" && fareRules[15] != "" && fareRules[15] != null) {
			var visibility = fareRules[15].split(":");
			var vis = "";
			for ( var f = 0; f < visibility.length; f++) {
				vis += visibility[f] + ",";
			}
			if (vis.indexOf(",") != -1) {
				setField("selFareVisibility", vis.substring(0, vis.lastIndexOf(",")));
			} else {
				setField("selFareVisibility", vis);
			}
		}
		
		if (fareRules[16] != "null" && fareRules[16] != "" && fareRules[16] != null) {
			setTabValues(screenId, delimiter, "arrayData", fareRules[16]);
			setAgentData();
		} else {
			setTabValues(screenId, delimiter, "arrayData", "");
		}

		setField("selPaxCat", fareRules[17]);
		setField("selFareCat", fareRules[18]);

		var adArr = fareRules[19]["AD"];
		var chArr = fareRules[19]["CH"];
		var inArr = fareRules[19]["IN"];

		if (adArr == null || adArr == "" || adArr.length <= 0) {
			adArr = new Array('', 'AD', '', 'off', 'off', 'off', 'off','','','','','');
		}
		if (chArr == null || chArr == "" || chArr.length <= 0) {
			chArr = new Array('', 'CH', '', 'off', 'off', 'off', 'off','','','','','');
		}
		if (inArr == null || inArr == "" || inArr.length <= 0) {
			inArr = new Array('', 'IN', '', 'off', 'off', 'off', 'off','','','','','');
		}
		
		DisablePaxFareConditionsGrid(false);
		
		var tempArr = new Array();
		tempArr[0] = new Array('AD', 'Adult',  '', '', '', adArr[3], adArr[4], adArr[5], adArr[6], '', '', '-1', '', adArr[7],adArr[8],adArr[9],adArr[10],adArr[11]);
		tempArr[1] = new Array('CH', 'Child',  '', '', '', chArr[3], chArr[4], chArr[5], chArr[6], '', '', '-1', '', chArr[7],chArr[8],chArr[9],chArr[10],chArr[11]);
		tempArr[2] = new Array('IN', 'Infant', '', '', '', inArr[3], inArr[4], inArr[5], inArr[6], '', '', '-1', '', inArr[7],inArr[8],inArr[9],inArr[10],inArr[11]);

		
		fillPaxFareConditionsGrid(tempArr);
		if (chidVisibility == 'false') {
			disableINFGrid(true);
			disableChidGrid(true);
		}
		setField(controlArr[18], fareRules[20]);
		setField(controlArr[19], fareRules[21]);
		setField(controlArr[20], fareRules[22]);
		
		setField(controlArr[36], fareRules[39]);
		setField(controlArr[37], fareRules[40]);
		setField(controlArr[38], fareRules[41]);
		
		if (fareRules[24] != "null" && fareRules[24] == "Y" && fareRules[24] != null) {
			getFieldByID(controlArr[24]).checked = true;
					
			setField(controlArr[25], openConfirm[0]);
			setField(controlArr[26], openConfirm[1]);
			setField(controlArr[27], openConfirm[2]);
			setField(controlArr[28], openConfirm[3]);			
		}
		
		if(fareRules[27] != ""){
			var tmpFlxArray = fareRules[27].split(",");
			setField("selFlexiCode", tmpFlxArray[0]);  // At the moment add
														// first Flexi code,
														// Future will be more
		}
		
		// fare rule modifications and load factor
		var strAllowModifyDate = fareRules[34];
		var strAllowModifyRoute = fareRules[35];
		var loadFactorEnabled = fareRules[36];
		var loadFactorMax = fareRules[37];
		var loadFactorMin = fareRules[38];
		
		if(strAllowModifyDate == "Y"){
			getFieldByID("modifyByDate").checked = true;
		}else{
			getFieldByID("modifyByDate").checked = false;
		}
		
		if(strAllowModifyRoute == "Y"){
			getFieldByID("modifyByOND").checked = true;
		}else{
			getFieldByID("modifyByOND").checked = false;
		}
		
		if(loadFactorEnabled == "Y"){
			getFieldByID("loadFactorEnabled").checked = true;
			setField("loadFactorMin", loadFactorMin);
			setField("loadFactorMax",loadFactorMax);
		}else{
			getFieldByID("loadFactorEnabled").checked = false;
		}
		var fareDiscount = fareRules[42];
		if( fareDiscountVisibiliy && fareDiscount == "Y"){
			getFieldByID("fareDiscount").checked = true;
			setField("fareDiscountMin", fareRules[43]);
			setField("fareDiscountMax",fareRules[44]);
		} else {
			getFieldByID("fareDiscount").checked = false;
		}

		var halfReturn = fareRules[46];
		if( halfReturn == "Y"){
			getFieldByID("chkHRT").checked = true;
		} else {
			getFieldByID("chkHRT").checked = false;
		}
		
		//var fareRuleLocalCurrency = fareRules[49];
		
		if(fareRules[49]!=null && fareRules[49]!=""){
			setField("radCurrFareRule", "SEL_CURR");
			setField("selCurrencyCode", fareRules[49]);
		}else{
			setField("radCurrFareRule", "BASE_CURR");
			setField("selCurrencyCode","-1");
		}
		
		if(fareRules[51]!=null && fareRules[51]!=""){
			setField("selAgentComType", fareRules[50]);
			setField("txtAgentCommision", fareRules[51]);
		}
		fareRuleCurrClick();
	}
}

function setInBoundDatesOnChange(){
	var arrMinStayOver = new Array(getValue("txtMinStayMonths"),getValue("txtMinStayDays"),getValue("txtMinStayHours"),getValue("txtMinStayMins"));
	var arrMaxStayOver = new Array(getValue("txtMaxStayMonths"),getValue("txtMaxStayDays"),getValue("txtMaxStayHours"),getValue("txtMaxStayMins"));
	setInboundDates(arrMaxStayOver,arrMinStayOver);
}

function setInboundDates(maxStayOver,minStayOver){
	var totalMilliSeconds = 0;
	var validationDate = "";
	if(isAddFare){
		if(isEmptyArray(minStayOver) && !isEmptyArray(maxStayOver)){
			totalMilliSeconds = calculateAddedDate(maxStayOver,"txtToDt");
			if(totalMilliSeconds > 0){
				validationDate = new Date(totalMilliSeconds);
				setField("txtToDtInbound", DateToString(validationDate));
			}
		}else if(!isEmptyArray(minStayOver) && isEmptyArray(maxStayOver)){
			totalMilliSeconds = calculateAddedDate(minStayOver,"txtFromDt");
			if(totalMilliSeconds > 0){
				validationDate = new Date(totalMilliSeconds);
				setField("txtFromDtInbound", DateToString(validationDate));
			}
		}else if(!isEmptyArray(minStayOver) && !isEmptyArray(maxStayOver)){
			totalMilliSeconds = calculateAddedDate(minStayOver,"txtFromDt");
			if(totalMilliSeconds > 0){
				validationDate = new Date(totalMilliSeconds);
				setField("txtFromDtInbound", DateToString(validationDate));
			}
			totalMilliSeconds = calculateAddedDate(maxStayOver,"txtToDt");
			if(totalMilliSeconds > 0){
				validationDate = new Date(totalMilliSeconds);
				setField("txtToDtInbound", DateToString(validationDate));
			}
		}
	}
}

function calculateAddedDate(dataArr,objectID){
	var startDate = getValue(objectID);
	var totalMills = 0;
	if(startDate != ""){
		var dateArray = startDate.split("/");
		var validationDate = new Date(dateArray[2],dateArray[1]-1,dateArray[0]);
		var intMonths = Number(dataArr[0]) *30*24*60*60*1000;
		var intDays = Number(dataArr[1]) *24*60*60*1000;
		var intHours = Number(dataArr[2]) *60*60*1000 ;
		var intMinutes = Number(dataArr[3]) *60*1000;
		totalMills = validationDate.getTime() + intMonths + intDays + intHours + intMinutes;
	}
	return totalMills;
}

/**
 * Sets the Fields when booking class is selected
 * 
 */
function bc_onSelect() {
	var fields = getValue("selBc");
	if (fields != "") {
		var arrFields = fields.split(",");
		var spnTmpCos = arrFields[2];
		var spnTmpFixed = '';
		var spnTmpStd = '';
		var spnPaxgoshow = '';
		var fixed = arrFields[1];
		var standard = arrFields[0];
		var paxGoshow = arrFields[6];
		var paxCat, fareCat;

		spnTmpStd = (arrFields[0] == "Y") ? "Standard" : "Non Standard";

		if (fixed == "Y") {
			spnTmpFixed = "Fixed";
			allocType = "Y";
		} else {
			spnTmpFixed = "Non Fixed";
		}

		spnPaxgoshow = (trim(paxGoshow) == "GOSHOW" || trim(paxGoshow) == "INTERLINE") ? " / " + paxGoshow : "";

		DivWrite("spnFixed", spnTmpStd + " / " + spnTmpFixed + " / " + spnTmpCos + " / " + arrFields[5] + " / " + arrFields[4]	+ spnPaxgoshow);
		Disable("btnSave", false);
		Disable("btnOverWriteRule", false);
		setField("selPaxCat", arrFields[7]);
		setField("selFareCat", arrFields[8]);
	} else {
		DivWrite("spnFixed", "");		
		setField("selPaxCat", "A");
		setField("selFareCat", "N");
	}
	if (addHocClicked) {
		selFareOnchange();
	}
	setReturnEffectiveDates();

}

/*
 * gets the details of hidden refundable details
 */
function getRefundable() {
	var refundable = "";
	if (getText("chkRefundable") != "") {
		refundable = getText("chkRefundable");
	} else {
		refundable = "off"
	}
	return refundable;
}

/**
 * Execute in savedata() sets the all the lower pane data to a hidden filed
 */
function getAllFareRuleData() {

	var fareData = "";
	var visibility = getText("selFareVisibility");	

	fareData += getRefundable() + " ^";
	fareData += getText("chkAdvance") + " ^";
	fareData += getText("txtAdvBookDays") + ":" + getText("txtAdvBookHours") + ":" + getText("txtAdvBookMins") + " ^";
	fareData += getText("chkChrgRestrictions") + " ^";
	fareData += getText("txtModification") + " ^";
	fareData += getText("txtCancellation") + " ^";

	fareData += getText("radFare") + " ^";
	fareData += getText("txtMaxStayDays") + " ^";
	fareData += getText("txtMinStayDays") + " ^";

	fareData += getText("txtOutTmFrom") + " ^";
	fareData += getText("txtOutTmTo") + " ^";

	fareData += getText("txtInbTmFrom") + " ^";
	fareData += getText("txtInbTmTo") + " ^";
	fareData += getText("txtaRulesCmnts") + " ^";
	fareData += getValue("selPaxCat") + " ^";
	fareData += getText("txtMaxStayHours") + " ^";
	fareData += getText("txtMinStayHours") + " ^";
	fareData += getText("txtMaxStayMins") + " ^";
	fareData += getText("txtMinStayMins") + " ^";
	fareData += getValue("selFareCat") + " ^";
	fareData += getText("txtModBufDays") + " ^";
	fareData += getText("txtModBufHours") + " ^";
	fareData += getText("txtModBufMins") + " ^";

	fareData += getText("chkPrintExp") + " ^";
	
		
	fareData += getText("txtMaxStayMonths") + " ^";
	fareData += getText("txtMinStayMonths") + " ^";
	
	fareData += getText("chkOpenRT") + " ^";
	
	fareData += getText("txtConfirmMonths") + " ^";
	fareData += getText("txtConfirmDays") + " ^";
	fareData += getText("txtConfirmHours") + " ^";
	fareData += getText("txtConfirmMins") + " ^";
	
	
	fareData += getText("txtFBC") + " ^";
	
	fareData += getValue("selFlexiCode") + "^";

	fareData += getValue("selDefineModType") + " ^";
	fareData += getValue("selDefineCanType") + " ^";
	fareData += getValue("txtMin_mod") + " ^";
	fareData += getValue("txtMax_mod") + " ^";
	fareData += getValue("txtMin_canc") + " ^";
	fareData += getValue("txtMax_canc") + " ^";
	fareData += getText("txtModBufDaysDom") + " ^";
	fareData += getText("txtModBufHoursDom") + " ^";
	fareData += getText("txtModBufMinsDom") + " ^";
	fareData += getText("chkHRT") + " ^";
	fareData += getText("txtModificationInLocal") + " ^";
	fareData += getText("txtCancellationInLocal") + " ^";
	fareData += getText("radCurrFareRule") + " ^";
	fareData += getText("selCurrencyCode") + " ^";	
	
	
	setField("hdnFareData", fareData);

}

/**
 * executes when rule edit is clicked
 */
function edit_ruleClick() {
	if (getText("selFC") != "") {
		var confirmed = confirm("You can not edit a Fare Rule attached to a Master Fare Rule Code.\n It will be saved as an Ad-Hoc rule \n Do you want to continue ?");
		if (confirmed) {
			editRuleClicked = true;
			addHocClicked = true;
			setPageEdited(true);
			setField("selFC", "");
			setField("hdnFareRuleVersion", "-1");
			setField("hdnVersionAD", "-1");
			setField("hdnVersionCH", "-1");
			setField("hdnVersionIN", "-1");
			setField("hdnFareRuleID", "");
			
			disableInputControls(false);
			setFieldsOnLoad();
			btnClick = "AddEdit";
			setPageEdited(true);
			Disable("btnSave", false);
			Disable("btnOverWriteRule", false);
			ctrl_visibilityAgents();
			selFareOnchange();
			disableNoShowChargeModelUIControls(false);
			Disable("chkOpenRT", false);   // Umesh@@
			Disable("chkHRT", false);
			if (chidVisibility == 'false') {
				disableINFGrid(true);
				disableChidGrid(true);
			}
			if(getValue("selFareCat") == "R"){ // openRT doesn't support for
												// restriced fares
				Disable("chkOpenRT", true); 
				Disable("chkHRT", true);
			}
			// fare rule modifications and load factor
			enableFareRuleModifications();
			fareRuleCurrClick();
		}
	}else{
		disableInputControls(false);
		// fare rule modifications and load factor
		enableFareRuleModifications();
		disableNoShowChargeModelUIControls(false);
	}
	//Shows the chkUpdateCommentsOnly if it's hidden.
	document.getElementById('chkUpdateCommentsOnly').style.visibility = 'visible';
	document.getElementById('lblUpdateCommentsOnly').style.visibility = 'visible';
}

/**
 * Sets the booking code value according to the text
 */
function setBookingCode(strCode) {
	var control = document.getElementById("selBc");
	for ( var t = 0; t < (control.length); t++) {
		if (control.options[t].text == strCode) {
			control.options[t].selected = true;
			break;
		}
	}
}

/**
 * executes when Fare Visibility is changed also in edit_ruleClick()
 * setFieldsOnLoad() & winonLoad() if an error occured
 * 
 */
function ctrl_visibilityAgents() {
	var visibility = getText("selFareVisibility");
	if (visibility != null && !visibility == "") {
		var upperVisible = visibility.toUpperCase();
		if (upperVisible.indexOf("AGENT") != -1
				|| upperVisible.indexOf("GSA") != -1) {
			Disable("btnLnkAgent", false);
			addHocClicked = true;
		} else {
			data = "";
			agentCodes = "";
			Disable("btnLnkAgent", true);
		}
	}
}

/**
 * sets the ond codes with via points to a hidden field executes in saveData()
 * method
 */
function setONDCodes() {
	var strOrigin = objCmb1.getText();
	var selDest = objCmb2.getText();
	var via1 = objCmb3.getText();
	var via2 = objCmb4.getText();
	var via3 = objCmb5.getText();
	var via4 = objCmb6.getText();
	var str = "";
	str += strOrigin;

	if (via1 != "") {
		str += "/" + via1;
	}
	if (via2 != "") {
		str += "/" + via2;
	}
	if (via3 != "") {
		str += "/" + via3;
	}
	if (via4 != "") {
		str += "/" + via4;
	}

	str += "/" + selDest;
	setField("hdnOND", str);
}

// ?
function compareTime(outTmFrom, outTmTo) {
	if (outTmFrom == outTmTo) {
		return false;
	} else {
		return true;
	}

}

/**
 * Sets the sales days when a effective day is entered
 */
function setSalesDate(strValue) {
	var dtSttus = getTabValues(screenId, delimiter, "ruleStatus");
	if (trim(dtSttus) != "Edit") {
		if (strValue == "FROM") {
			setField("txtSlsStDate", ' ');
			dateChk('txtSlsStDate');
		} else {
			setField("txtSlsEnDate", getText("txtToDt"));
		}
		setReturnEffectiveDates();
	}
}

/**
 * when the reset button is clicked
 */
function resetClick() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		updateClicked = false;
		top[2].HidePageMessage();
		setPageEdited(false);
		clearControls();
		if (getTabValues(screenId, delimiter, "ruleStatus") == "Edit") {
			top[2].ShowProgress();
			var arr = getTabValues(screenId, delimiter, "strGridData").split("~");			
			getFieldByID("selBc").focus();
			agentCodes = getTabValues(screenId, delimiter, "arrayData");
			top[0].ruleStatus = "Edit";
			setAgentData();			
			setFareDataOnEdit(arr[7]);
			disableOnEdit();			
			setField("hdnFareVersion", arr[8]);
			if (getTabValues(screenId, delimiter, "status") != "Save") {				
				var adhocRules = arr[10].split("/");
				adhocRules.splice(0,1);
				setFareRuleData(adhocRules);			
			}		
			Disable("btnAdd", true);
			Disable("selADCGTYP", true);
			Disable("selCHCGTYP", true);
			Disable("selINCGTYP", true);
			resetFareDefType();
		} else {
			top[2].ShowProgress();
			document.getElementById("frmCreateFare").reset();			
			defaultLoadCheck();
			DivWrite("spnFixed", "");
			getFieldByID("chkStatus").checked = true;
			getFieldByID("chkModToSameFare").checked = false;
			setComboSelection();
			resetFareRuleModifications();
		}
		disableNoShowChargeModelUIControls(true);
		Disable("btnReset", false);
		disableInputControls(true);
		top[2].HideProgress();
	}
}

/**
 * The combo initialization
 */
var objCmb1 = new combo();
var objCmb2 = new combo();
var objCmb3 = new combo();
var objCmb4 = new combo();
var objCmb5 = new combo();
var objCmb6 = new combo();

buildCombos(arrAirports);

function buildCombos(arrName) {	
	objCmb1.id = "selAprportCmb1";
	objCmb1.top = "71";
	objCmb1.left = "110";
	objCmb1.width = "50";
	objCmb1.valueIndex = 0;
	objCmb1.textIndex = 1;
	objCmb1.tabIndex = 1;
	objCmb1.arrData = arrName; // 2D Data Array
	objCmb1.onChange = "required_changed";
	objCmb1.maxlength = 3;

	objCmb2.id = "selAprportCmb2";
	objCmb2.top = "71";
	objCmb2.left = "295";
	objCmb2.width = "50";
	objCmb2.valueIndex = 0;
	objCmb2.textIndex = 1;
	objCmb2.tabIndex = 2;
	objCmb2.arrData = arrName;
	objCmb2.onChange = "required_changed";
	objCmb2.maxlength = 3;

	objCmb3.id = "selAprportCmb3";
	objCmb3.top = "71";
	objCmb3.left = "440";
	objCmb3.width = "50";
	objCmb3.tabIndex = 3;
	objCmb3.arrData = arrName;
	objCmb3.onChange = "required_changed";
	objCmb3.maxlength = 3;

	objCmb4.id = "selAprportCmb4";
	objCmb4.top = "71";
	objCmb4.left = "560";
	objCmb4.width = "50";
	objCmb4.tabIndex = 4;
	objCmb4.arrData = arrName;
	objCmb4.onChange = "required_changed";
	objCmb4.maxlength = 3;

	objCmb5.id = "selAprportCmb5";
	objCmb5.top = "71";
	objCmb5.left = "680";
	objCmb5.width = "50";
	objCmb5.tabIndex = 5;
	objCmb5.arrData = arrName;
	objCmb5.onChange = "required_changed";
	objCmb5.maxlength = 3;

	objCmb6.id = "selAprportCmb6";
	objCmb6.top = "71";
	objCmb6.left = "800";
	objCmb6.width = "50";
	objCmb6.tabIndex = 6;
	objCmb6.arrData = arrName;
	objCmb6.onChange = "required_changed";
	objCmb6.maxlength = 3;

	objCmb6.buildCombo();

}

function setComboSelection() {
	objCmb1.setComboValue("");
	objCmb2.setComboValue("");
	objCmb3.setComboValue("");
	objCmb4.setComboValue("");
	objCmb5.setComboValue("");
	objCmb6.setComboValue("");
}

// when the close button is clicked
function closeClick() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setPageEdited(false);
		top[1].objTMenu.tabRemove(screenId);
	}
}

/**
 * disable the child grid
 */
function disableChidGrid(cond) {
	Disable("chkCHApp", true);
	Disable("chkCHRef", true);
	Disable("chkCHCNX", true);
	Disable("chkCHMod", true);	
}

/**
 * disable the infant grid
 */
function disableINFGrid(cond) {
	Disable("chkINApp", true);
	Disable("chkINRef", true);
	Disable("chkINMod", true);
	Disable("chkINCNX", true);
}

/**
 * sets the check control values
 */
function setChkControls(control, cond) {
	var conValue = false;
	if(cond == true || cond == 'on' || cond == 'true' || cond == 'TRUE')
		conValue = true;
	getFieldByID(control).checked = conValue;		
	
}

function EnableConfirmPeriod(obj){
	if(obj.checked){
	Disable("txtConfirmMonths", false);
	Disable("txtConfirmDays", false);
	Disable("txtConfirmHours", false);
	Disable("txtConfirmMins", false);
	}else{
	Disable("txtConfirmMonths", true);
	Disable("txtConfirmDays", true);
	Disable("txtConfirmHours", true);
	Disable("txtConfirmMins", true);
	}
}

function currencyClick() {


	var objRad1 = getFieldByID("radCurr");
	var objRad2 = getFieldByID("radSelCurr");
	
	if (isSelectedCurrencyChecked()) {
		
		getFieldByID("txtAED").readOnly = true;
		getFieldByID("txtCHL").readOnly = true;
		getFieldByID("txtINF").readOnly = true;
		getFieldByID("selCamt").readOnly = true;
		getFieldByID("selIamt").readOnly = true;
				
		getFieldByID("txtBaseInLocal").readOnly = false;
		getFieldByID("txtCHFareInLocal").readOnly = false;
		getFieldByID("txtINFInLocal").readOnly = false;
		getFieldByID("selCamtInLocal").readOnly = false;
		getFieldByID("selIamtInLocal").readOnly = false;
		
		setField("hdnLocalCurrencySelected","true");
	} 
	else if (objRad1.checked) {

		getFieldByID("txtAED").readOnly = false;
		getFieldByID("txtCHL").readOnly = false;
		getFieldByID("txtINF").readOnly = false;
		getFieldByID("selCamt").readOnly = false;
		getFieldByID("selIamt").readOnly = false;
				
		getFieldByID("txtBaseInLocal").readOnly = true;
		getFieldByID("txtCHFareInLocal").readOnly = true;
		getFieldByID("txtINFInLocal").readOnly = true;
		getFieldByID("selCamtInLocal").readOnly = true;
		getFieldByID("selIamtInLocal").readOnly = true;
		
		if(getText("hdnFareID") != null && getText("hdnFareID") != "") {
			if(preAdFareLocal != "0.00") {
				setField("txtBaseInLocal",preAdFareLocal);
				setField("txtCHFareInLocal",preChFareLocal);
				setField("txtINFInLocal",preInFareLocal);
				setField("selCamtInLocal",chAmpLocal);
				setField("selIamtInLocal",inAmpLocal);
				
			}
		} else {
			setField("txtBaseInLocal", "0.00");
			setField("txtCHFareInLocal", "0.00");
			setField("txtINFInLocal", "0.00");
			setField("txtCHL", "100.00");
		}
		

	}
	
}

function setFareType(objCon){
		if(objCon.id == "selCamt"){
			chldFareType = objCon.value;
			setField("txtCHL", "100.00");
			getFieldByID("txtCHL").focus();
		}else if(objCon.id == "selIamt"){
			infFareType = objCon.value;
			setField("txtINF","0.00");
			getFieldByID("txtINF").focus();
		}else if(objCon.id == "selCamtInLocal"){
			chldFareType = objCon.value;
			setField("txtCHFareInLocal", "100.00");
			getFieldByID("txtCHFareInLocal").focus();
		}else{
			infFareType = objCon.value;
			setField("txtINFInLocal","0.00");
			getFieldByID("txtINFInLocal").focus();
		}
	}

/**
 * Sets the return departure validity days when a effective day is entered
 */
function setReturnEffectiveDates() {
	
	var strValBC = getValue("selBc").split(",");
	var fareType = strValBC[4];
	
	var dtSttus = getTabValues(screenId, delimiter, "ruleStatus");
	if(trim(dtSttus) != "Edit"){ 
		if (fareType == "RET" || getText("radFare") == "Return") {
			Disable("txtFromDtInbound", false);
			Disable("txtToDtInbound", false);
			setField("txtFromDtInbound", getText("txtFromDt"));
			setField("txtToDtInbound", getText("txtToDt"));
		}else if(fareType == "COM"){
			Disable("txtFromDtInbound", false);
			Disable("txtToDtInbound", false);
		}
		else{
			setField("txtFromDtInbound", "");
			setField("txtToDtInbound", "");
			Disable("txtFromDtInbound", true);
			Disable("txtToDtInbound", true);
		}
	}
	else if(trim(dtSttus) == "Edit"){
		if (fareType == "RET" || getText("radFare") == "Return") {
			Disable("txtFromDtInbound", false);
			Disable("txtToDtInbound", false);
		}else if(fareType == "COM"){
			Disable("txtFromDtInbound", false);
			Disable("txtToDtInbound", false);
		}
		else{
			Disable("txtFromDtInbound", true);
			Disable("txtToDtInbound", true);
		}
	}
}
 function disableAndClearMinMaxForMod (blnType){
	 	setField("txtMin_mod","");
	 	setField("txtMax_mod","");
		Disable("txtMin_mod", blnType);
		Disable("txtMax_mod", blnType);
	}

	function disableAndClearMinMaxCanc (blnType){
		setField("txtMin_canc","");
		setField("txtMax_canc","");
		Disable("txtMin_canc", blnType);
		Disable("txtMax_canc", blnType);
	}
	
function restrictionEnable (){
	
	if(getFieldByID("chkChrgRestrictions").checked){
		Disable("selDefineCanType", false);
		Disable("selDefineModType", false);	
		Disable("txtModification", false);
		Disable("txtCancellation", false);
		Disable("txtModificationInLocal", false);
		Disable("txtCancellationInLocal", false);
		
	}
	if(getValue("selDefineCanType") != selModeTyps.v){
		Disable("txtMin_canc", false);
		Disable("txtMax_canc", false);	
	}
	if(getValue("selDefineModType") != selModeTyps.v){
		Disable("txtMin_mod", false);
		Disable("txtMax_mod", false);		
	}
}
 
function clearModTypeOnClick (){
	var objRad1 = getFieldByID("radCurrFareRule");
	var objRad2 = getFieldByID("radSelCurrFareRule");	

	if(getValue("selDefineModType") != selModeTyps.v){	
		setField("txtModificationInLocal","0.00");
		getFieldByID("txtModification").readOnly = false;
		getFieldByID("txtModificationInLocal").readOnly = true;
		disableAndClearMinMaxForMod(false);
	}else {
		if (isSelectedCurrencyChecked()){
			setField("txtModification","0.00");	
			getFieldByID("txtModification").readOnly = true;
			getFieldByID("txtModificationInLocal").readOnly = false;
		}else{
			if(isSelectedCurrencyOptionAvailable()){
				setField("txtModificationInLocal","0.00");
				getFieldByID("txtModificationInLocal").readOnly = true;
			}
			getFieldByID("txtModification").readOnly = false;
		}
		disableAndClearMinMaxForMod(true);
	}
}

function clearCancTypeOnClick (){
	var objRad1 = getFieldByID("radCurrFareRule");
	var objRad2 = getFieldByID("radSelCurrFareRule");	

	if(getValue("selDefineCanType") != selModeTyps.v){
		setField("txtCancellationInLocal","0.00");
		getFieldByID("txtCancellation").readOnly = false;
		getFieldByID("txtCancellationInLocal").readOnly = true;
		disableAndClearMinMaxCanc(false);
	}else {
		if (isSelectedCurrencyChecked()){
			setField("txtCancellation","0.00");	
			getFieldByID("txtCancellation").readOnly = true;
			getFieldByID("txtCancellationInLocal").readOnly = false;
		}else{
			if(isSelectedCurrencyOptionAvailable()()){
				setField("txtCancellationInLocal","0.00");
				getFieldByID("txtCancellationInLocal").readOnly = true;
			}
			getFieldByID("txtCancellation").readOnly = false;
		}
		disableAndClearMinMaxCanc(true);
	}
}

// fare rule modifications and load factor
function disableFareRuleModifications(){
	Disable("loadFactorEnabled", true);
	Disable("loadFactorMin", true);
	Disable("loadFactorMax", true);
	Disable("modifyByDate", true);
	Disable("modifyByOND", true);
	Disable("fareDiscount", true);
	Disable("fareDiscountMin", true);
	Disable("fareDiscountMax", true);
}

function resetFareRuleModifications(){
	Disable("loadFactorEnabled", true);
	Disable("loadFactorMin", true);	
	Disable("loadFactorMax", true);
	Disable("modifyByDate", true);
	Disable("modifyByOND", true);
	Disable("fareDiscount", true);
	Disable("fareDiscountMin", true);
	Disable("fareDiscountMax", true);
	
	getFieldByID("modifyByDate").checked = true;
	getFieldByID("modifyByOND").checked = true;
	getFieldByID("loadFactorEnabled").checked = false;	
	setField("loadFactorMin", "");	
	setField("loadFactorMax", "");	
	getFieldByID("fareDiscount").checked = false;
	setField("fareDiscountMin", "");
	setField("fareDiscountMax", "");
}

function enableDisableFareDiscount(){
	var fareDiscount = getText("fareDiscount");

	if (fareDiscount == "Y") {
		Disable("fareDiscountMin", false);
		Disable("fareDiscountMax", false);
	} else {
		setField("fareDiscountMin", "");
		setField("fareDiscountMax", "");
		Disable("fareDiscountMin", true);
		Disable("fareDiscountMax", true);
	}
}

function loadFactorValidation(){
	var checked = getText("loadFactorEnabled");

	if (checked == "ON") {
		Disable("loadFactorMin", false);
		Disable("loadFactorMax", false);
	} else {
		setField("loadFactorMin", "");
		setField("loadFactorMax", "");
		Disable("loadFactorMin", true);
		Disable("loadFactorMax", true);
	}
}

function enableFareRuleModifications(){
	var checked = getText("loadFactorEnabled");

	if (checked == "ON") {
		Disable("loadFactorMin", false);
		Disable("loadFactorMax", false);
	} else {
		setField("loadFactorMin", "");
		setField("loadFactorMax", "");
		Disable("loadFactorMin", true);
		Disable("loadFactorMax", true);
	}
	
	var fareDiscount = getText("fareDiscount");

	if (fareDiscountVisibiliy && fareDiscount == "Y") {
		Disable("fareDiscountMin", false);
		Disable("fareDiscountMax", false);
	} else {
		setField("fareDiscountMin", "");
		setField("fareDiscountMax", "");
		Disable("fareDiscountMin", true);
		Disable("fareDiscountMax", true);
	}
	
	Disable("loadFactorEnabled", false);	
	Disable("modifyByDate", false);
	Disable("modifyByOND", false);
	Disable("fareDiscount", false);	
}

function showOnlyFareRuleModifications(){
	var checked = getText("loadFactorEnabled");

	if (checked == "ON") {
		Disable("loadFactorMin", true);
		Disable("loadFactorMax", true);
	} else {
		setField("loadFactorMin", "");
		setField("loadFactorMax", "");
		Disable("loadFactorMin", true);
		Disable("loadFactorMax", true);
	}
	
	var fareDiscount = getText("fareDiscount");

	if (fareDiscountVisibiliy && fareDiscount == "Y") {
		Disable("fareDiscountMin", false);
		Disable("fareDiscountMax", false);
	} else {
		setField("fareDiscountMin", "");
		setField("fareDiscountMax", "");
		Disable("fareDiscountMin", true);
		Disable("fareDiscountMax", true);
	}
	
	Disable("loadFactorEnabled", true);	
	Disable("modifyByDate", true);
	Disable("modifyByOND", true);
}

function VaidateFactor(){
	
	var loadFactorMin = getText("loadFactorMin");
	var loadFactorMax = getText("loadFactorMax");
	if(loadFactorMin!= ""){
		var strCC = loadFactorMin;
		var strLen = strCC.length;
		var blnVal = isPositiveInt(strCC)
		if (!blnVal) {
			setField("loadFactorMin", strCC.substr(0, strLen - 1));
			getFieldByID("loadFactorMin").focus();
		}
	}
	
	if(loadFactorMax!= ""){
		var strCC = loadFactorMax;
		var strLen = strCC.length;
		var blnVal = isPositiveInt(strCC)
		if (!blnVal) {
			setField("loadFactorMax", strCC.substr(0, strLen - 1));
			getFieldByID("loadFactorMax").focus();
		}
	}
}

//Integrating the No show Charge Types modification 
function setDisplayBoundaryAndBreakPoints(obj) {
	var objRad1 = getFieldByID("radCurrFareRule");
	var objRad2 = getFieldByID("radSelCurrFareRule");	
	var id = obj.id;
	var chargeType = obj.value;
	if(id == "selADCGTYP"){
		if(isNoShowChargeTypeNotV(chargeType)){
			Disable("txtADNoShoBreakPoint", false);
			Disable("txtADNoShoBoundary", false);
			if(chargeType != "V"){
				setField("txtADNoShoChargeInLocal","0.00");
				getFieldByID("txtADNoShoCharge").readOnly = false;
				getFieldByID("txtADNoShoChargeInLocal").readOnly = true;
			}else{
				if (isSelectedCurrencyChecked()){
					setField("txtCancellation","0.00");	
					getFieldByID("txtADNoShoCharge").readOnly = true;
					getFieldByID("txtCancellationInLocal").readOnly = false;
				}else{
					if(isSelectedCurrencyOptionAvailable()){
						setField("txtADNoShoChargeInLocal","0.00");
						getFieldByID("txtADNoShoChargeInLocal").readOnly = true;
					}
					getFieldByID("txtADNoShoCharge").readOnly = false;
				}
			}
		}else{
			setField("txtADNoShoBreakPoint", "");
			setField("txtADNoShoBoundary", "");
			Disable("txtADNoShoBreakPoint", true);
			Disable("txtADNoShoBoundary", true);
		}
	}else if(id == "selCHCGTYP"){
		if(isNoShowChargeTypeNotV(chargeType)){
			Disable("txtCHNoShoBreakPoint", false);
			Disable("txtCHNoShoBoundary", false);
			if(chargeType != "V"){
				setField("txtCHNoShoChargeInLocal","0.00");
				getFieldByID("txtCHNoShoCharge").readOnly = false;
				getFieldByID("txtCHNoShoChargeInLocal").readOnly = true;
			}else{
				if (isSelectedCurrencyChecked()){
					setField("txtCHNoShoCharge","0.00");	
					getFieldByID("txtCHNoShoCharge").readOnly = true;
					getFieldByID("txtCHNoShoChargeInLocal").readOnly = false;
				}else{
					if(isSelectedCurrencyOptionAvailable()){
						setField("txtCHNoShoChargeInLocal","0.00");
						getFieldByID("txtCHNoShoChargeInLocal").readOnly = true;
					}
					getFieldByID("txtCHNoShoCharge").readOnly = false;
				}
			}
		}else{
			setField("txtCHNoShoBreakPoint", "");
			setField("txtCHNoShoBoundary", "");
			Disable("txtCHNoShoBreakPoint", true);
			Disable("txtCHNoShoBoundary", true);
		}
	}else if(id == "selINCGTYP"){
		if(isNoShowChargeTypeNotV(chargeType)){
			Disable("txtINNoShoBreakPoint", false);
			Disable("txtINNoShoBoundary", false);
			
			if(chargeType != "V"){
				setField("txtINNoShoChargeInLocal","0.00");
				getFieldByID("txtINNoShoCharge").readOnly = false;
				getFieldByID("txtINNoShoChargeInLocal").readOnly = true;
			}else{
				if (isSelectedCurrencyChecked()){
					setField("txtINNoShoCharge","0.00");	
					getFieldByID("txtINNoShoCharge").readOnly = true;
					getFieldByID("txtINNoShoChargeInLocal").readOnly = false;
				}else{
					if(isSelectedCurrencyOptionAvailable()){
						setField("txtINNoShoChargeInLocal","0.00");
						getFieldByID("txtINNoShoChargeInLocal").readOnly = true;
					}
					getFieldByID("txtINNoShoCharge").readOnly = false;
				}
			}
		}else{
			setField("txtINNoShoBreakPoint", "");
			setField("txtINNoShoBoundary", "");
			Disable("txtINNoShoBreakPoint", true);
			Disable("txtINNoShoBoundary", true);
		}
	}
}

function isNoShowChargeTypeNotV(chargeType){
	if(trim(chargeType) == "PF" || trim(chargeType) == "PFS" || trim(chargeType) == "PTF"){
		return true;
	}else{
		return false;
	}
		

}

function disableNoShowChargeModelUIControls(blnDisable){
	
	Disable("selADCGTYP",blnDisable);
	Disable("selCHCGTYP",blnDisable);
	Disable("selINCGTYP",blnDisable);
	
	Disable("txtADNoShoBreakPoint",blnDisable);
	Disable("txtADNoShoBoundary",blnDisable);
	
	Disable("txtCHNoShoBreakPoint",blnDisable);
	Disable("txtCHNoShoBoundary",blnDisable);
	
	Disable("txtINNoShoBreakPoint",blnDisable);
	Disable("txtINNoShoBoundary",blnDisable);
	
}


function allowOnlyPositiveInt(elementId){
	var validElement = removeChars(getText(elementId), '[^\\d]'); // remove all non digits
	setField(elementId, validElement);
}

function masterFareValidate(){
	var checked = getText("chkMasterFare");

	if (checked == "ON" || checked == "on") {
		Disable("selectedMasterFareID", true);
		Disable("linkFarePercentage", true);
		Disable("btnLinkFares", true);		
		getFieldByID("chkLinkFare").checked = false;
		Disable("chkLinkFare", true);
		//setField("selectedMasterFareID", "");
		getFieldByID("selectedMasterFareID").value = "";
		setField("linkFarePercentage", "");
	} else {
		Disable("selectedMasterFareID", false);
		Disable("linkFarePercentage", false);
		//Disable("btnLinkFares", false);
		setField("hdnMasterFareRefCode", "");
		setField("masterFareRefCode", "");
		Disable("chkLinkFare", false);
	}
}

function resetFareDefType(){
	/*link fares*/
	var arr = getTabValues(screenId, delimiter, "strGridData").split("~");
	var masterFareStatus = arr[26];
	var masterFareID = arr[27];
	var masterFarePercentage = arr[28];
	var masterFareRefCode = arr[29];
	enableDIV();
	if (masterFareStatus != null) {
		if (masterFareStatus == "Y") {
			getFieldByID("chkMasterFare").checked = true;
			setField("hdnIsMasterFare", "ON");
			setField("masterFareRefCode", masterFareRefCode);
			setField("hdnMasterFareRefCode", masterFareRefCode);
			Disable("selectedMasterFareID", true);
			Disable("linkFarePercentage", true);
			Disable("btnLinkFares", true);
			getFieldByID("chkLinkFare").checked = false;
			Disable("chkLinkFare", true);			
		} else {
			getFieldByID("chkMasterFare").checked = false;
			setField("hdnIsMasterFare", "");
			setField("masterFareRefCode", "");
			setField("hdnMasterFareRefCode", "");
		}
	}
	if((masterFareID != null && masterFareID != "") && (masterFarePercentage != null && masterFarePercentage != "")){				
		getFieldByID("chkMasterFare").checked = false;
		setField("hdnIsMasterFare", "");		
		setField("selectedMasterFareID", masterFareID);
		setField("hdnMasterFareID", masterFareID);
		setField("linkFarePercentage", masterFarePercentage);
		setField("hdnMasterFarePercentage", masterFarePercentage);
		Disable("chkLinkFare", false);
		getFieldByID("chkLinkFare").checked = true;
		linkFares_click();
		selectMasterFareAfterLoad(masterFareID);
		linkFareValidate();
	}
	Disable("btnLinkFares",true);
}

function linkFareValidate(){
	var checked = getText("chkLinkFare");

	if (checked == "ON" || checked == "on") {
		disableDIV();
		Disable("chkMasterFare",true);
		//Disable("masterFareRefCode",true);
		Disable("btnLinkFares",false);		
	} else {
		enableDIV();
		Disable("chkMasterFare",false);
		Disable("masterFareRefCode",false);
		Disable("btnLinkFares",true);
		getFieldByID("selectedMasterFareID").value = "";
		setField("linkFarePercentage", "");
		Disable("selectedMasterFareID",true);
		Disable("linkFarePercentage",true);
	}
	Disable("selectedMasterFareID",true);
	//Disable("linkFarePercentage",true);

}

function isEmptyArray(strArray){
	for(var i=0; i < strArray.length; i++){
		if(strArray[i] != ""){
			return false;
		}
	}
	return true;

}

function selectMasterFareAfterLoad(key){
	$("#selectedMasterFareID").val(key);
}

function linkFares_click(){
	var origin = objCmb1.getText();
	var destination = objCmb2.getText();
	var via1 = objCmb3.getText();
	var via2 = objCmb4.getText();
	var via4 = objCmb6.getText();
	var via3 = objCmb5.getText();

	var data = {};
	data.origin = origin;
	data.destination = destination;
	data.via1 = via1;
	data.via2 = via2;
	data.via3 = via3;
	data.via4 = via4;
	data.fareId = $("#hdnFareID").val();	

	if(origin == null || origin == ""){
		showERRMessage(OriginDoesNotExists);
		objCmb1.focus();		
	}else if (destination == null || destination == ""){
		showERRMessage(DestinationDoesNotExists);
		objCmb2.focus();		
	}else if (origin == destination){
		showERRMessage(originDestEqual);
		objCmb2.focus();		
	}else{
		var url = "showMasterLinkFares.action";
		$.ajax({type: "POST", dataType: 'json', data: data , url:url,		
		success: getMasterFaresSuccess, error:getMasterFaresError, cache : false, async: false});
	}
	
}

function getMasterFaresSuccess(response){
	if(response.success){
		$("#selectedMasterFareID").html("");
		$("#selectedMasterFareID").append("<option value='' selected='selected'></option>");
		$.each(response.masterFares, function(key, value){
			var t = $("<option>"+value+"</option>").attr({value:key});		
			$("#selectedMasterFareID").append(t);
		});
		Disable("selectedMasterFareID",false);
		Disable("linkFarePercentage",false);
	}else{		
		resetFareScreen();
		showERRMessage(response.messageTxt);
	}
}

function getMasterFaresError(response){
	showERRMessage(response.messageTxt);
};

function resetFareScreen(status){	
	enableDIV();
	Disable("chkMasterFare",false);
	Disable("chkLinkFare",false);
	Disable("masterFareRefCode",false);
	Disable("selectedMasterFareID",true);
	//Disable("linkFarePercentage",true);
	getFieldByID("chkLinkFare").checked = false;
	Disable("btnLinkFares",true);
}

function enableDIV(){
	document.getElementById("divCurrSelect").style.display="block";
	document.getElementById("divFareAmounts").style.display="block";
	document.getElementById("divFare").style.display="block";
	document.getElementById("divDateOutbound").style.display="block";
	document.getElementById("divDateInbound").style.display="block";
	document.getElementById("divSalesDates").style.display="block";
}

function disableDIV(){
	document.getElementById("divCurrSelect").style.display="none";
	document.getElementById("divFareAmounts").style.display="none";
	document.getElementById("divFare").style.display="none";
	document.getElementById("divDateOutbound").style.display="none";
	document.getElementById("divDateInbound").style.display="none";
	document.getElementById("divSalesDates").style.display="none";
}

function validateFarePercentage(){
	var percentage = getText("linkFarePercentage");	
	if(percentage != ""){
		var strCC = percentage;
		var strLen = strCC.length;
		var blnVal = isPositiveInt(strCC)
		if (!blnVal) {
			setField("linkFarePercentage", strCC.substr(0, strLen - 1));
			getFieldByID("linkFarePercentage").focus();
		}
	}
}

function extendToDateTenYears_OnBlur(target){
	if(true){//FIXME disable on a parameter
		var outBoundFromDate = document.getElementById("txtFromDt").value;
		var outBoundToDate = document.getElementById("txtToDt").value;
		var inBoundFromDate = document.getElementById("txtFromDtInbound").value;
		var inBoundToDate = document.getElementById("txtToDtInbound").value;
		var salesFromDate = document.getElementById("txtSlsStDate").value;
		var salesToDate = document.getElementById("txtSlsEnDate").value;
		
		if(target == 'OB' && isNotEmpty(outBoundFromDate) && !isNotEmpty(outBoundToDate) && dateChk("txtFromDt")){
			document.getElementById("txtToDt").value = addTenYears(outBoundFromDate);
		}

		if(target == 'IB' && isNotEmpty(inBoundFromDate) && !isNotEmpty(inBoundToDate) && dateChk("txtFromDtInbound")){
			document.getElementById("txtToDtInbound").value = addTenYears(inBoundFromDate);
		}	

		if(target == 'SL' && isNotEmpty(salesFromDate) && !isNotEmpty(salesToDate) && dateChk("txtSlsStDate")){
			document.getElementById("txtSlsEnDate").value = addTenYears(salesFromDate);
		}
	}
}

function isNotEmpty(str){
	return (str != null && str != '');
}

function extendToDateTenYears_Click(){
	var checked = getText("chkExtendToDate");
	if(checked == 'ON' || checked == 'on'){
		var outBoundFromDate = document.getElementById("txtFromDt").value;
		var inBoundFromDate = document.getElementById("txtFromDtInbound").value;
		var salesFromDate = document.getElementById("txtSlsStDate").value;
		
		if(outBoundFromDate != null  && outBoundFromDate != "" && dateChk("txtFromDt")){
			document.getElementById("txtToDt").value = addTenYears(outBoundFromDate);
		}

		if(inBoundFromDate != null  && inBoundFromDate != "" && dateChk("txtFromDtInbound")){
			document.getElementById("txtToDtInbound").value = addTenYears(inBoundFromDate);
		}	

		if(salesFromDate != null  && salesFromDate != "" && dateChk("txtSlsStDate")){
			document.getElementById("txtSlsEnDate").value = addTenYears(salesFromDate);
		}
	}
}

function addTenYears(date){
	var strRetDate = "";
	
	var dateArray = date.split("/");
	var updatingYear = parseInt(dateArray[2]) + 10;
	strRetDate = dateArray[0] + "/" + dateArray[1] + "/" + updatingYear;
	
	return strRetDate;
}

function fareRuleCurrClick() {

	setVisible("selCurrencyCode", false);
	var objRad1 = getFieldByID("radCurrFareRule");
	var objRad2 = getFieldByID("radSelCurrFareRule");	

	if (isSelectedCurrencyChecked()) {
		setVisible("selCurrencyCode", true);
		if(getValue("selDefineCanType") == selModeTyps.v){
			setField("txtCancellation","0.00");	
			getFieldByID("txtCancellation").readOnly = true;
			getFieldByID("txtCancellationInLocal").readOnly = false;
		}else{
			getFieldByID("txtCancellation").readOnly = false;
			getFieldByID("txtCancellationInLocal").readOnly = true;
		}
		if(getValue("selDefineModType") == selModeTyps.v){
			setField("txtModification","0.00");				
			getFieldByID("txtModification").readOnly = true;	
			getFieldByID("txtModificationInLocal").readOnly = false;
		}else{
			getFieldByID("txtModification").readOnly = false;	
			getFieldByID("txtModificationInLocal").readOnly = true;
		}  
		if(getValue("selADCGTYP") == selModeTyps.v){
			setField("txtADNoShoCharge","0.00");	
			getFieldByID("txtADNoShoCharge").readOnly = true;
			getFieldByID("txtADNoShoChargeInLocal").readOnly = false;
		}else{
			getFieldByID("txtADNoShoCharge").readOnly = false;
			getFieldByID("txtADNoShoChargeInLocal").readOnly = true;
		}if(getValue("selCHCGTYP") == selModeTyps.v){
			setField("txtCHNoShoCharge","0.00");	
			getFieldByID("txtCHNoShoCharge").readOnly = true;
			getFieldByID("txtCHNoShoChargeInLocal").readOnly = false;
		}else{
			getFieldByID("txtCHNoShoCharge").readOnly = false;
			getFieldByID("txtCHNoShoChargeInLocal").readOnly = true;
		}if(getValue("selINCGTYP") == selModeTyps.v){
			setField("txtINNoShoCharge","0.00");	
			getFieldByID("txtINNoShoCharge").readOnly = true;
			getFieldByID("txtINNoShoChargeInLocal").readOnly = false;
		}else{
			getFieldByID("txtINNoShoCharge").readOnly = false;
			getFieldByID("txtINNoShoChargeInLocal").readOnly = true;
		}
	} else if (objRad1.checked) {
		setVisible("selCurrencyCode", false);
		setField("selCurrencyCode", "-1");	
		setField("txtModificationInLocal","0.00");
		setField("txtCancellationInLocal","0.00");	
		setField("txtADNoShoChargeInLocal","0.00");
		setField("txtCHNoShoChargeInLocal","0.00");
		setField("txtINNoShoChargeInLocal","0.00");		
		getFieldByID("txtModification").readOnly = false;
		getFieldByID("txtCancellation").readOnly = false;
		getFieldByID("txtADNoShoCharge").readOnly = false;
		getFieldByID("txtCHNoShoCharge").readOnly = false;
		getFieldByID("txtINNoShoCharge").readOnly = false;	
		
		if(isSelectedCurrencyOptionAvailable()){
			getFieldByID("txtModificationInLocal").readOnly = true;
			getFieldByID("txtCancellationInLocal").readOnly = true;
			getFieldByID("txtADNoShoChargeInLocal").readOnly = true;
			getFieldByID("txtCHNoShoChargeInLocal").readOnly = true;
			getFieldByID("txtINNoShoChargeInLocal").readOnly = true;	
		}
	}	
}

function updateCommentAndDescription()
{
	var boxChecked=getFieldByID("chkUpdateCommentsOnly").checked;
	makeInputsReadOnly(boxChecked);
	disableDropDownControls(boxChecked);
	getFieldByID("txtaRulesCmnts").readOnly=false;
	getFieldByID("txtaInstructions").readOnly=false;
}

function isSelectedCurrencyChecked(){
	if(isSelectedCurrencyOptionAvailable()){
		var selectedCurrency = getFieldByID("radSelCurrFareRule");
		if(selectedCurrency.checked){
			return true;
		}else{
			return false;
		}
	}
	return false;
}

function isSelectedCurrencyOptionAvailable(){
	var selectedCurrency = getFieldByID("radSelCurrFareRule");
	if(selectedCurrency !=null){
		return true;
	}
	return false;
}

function validateSelectedCurrency(){
	if(isSelectedCurrencyOptionAvailable()){
		if(getText("selCurrencyCode") != ""){
			CWindowOpen(2);
		}else{
			showCommonError("Error","Please select currency code.");
		}
	}else{
		CWindowOpen(2);
	}
}

