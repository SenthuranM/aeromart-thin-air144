function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}
var recNo = "1";
var screenId = "SC_INVN_007";
var delimiter = "^";
var childScreenId = "SC_INVN_010";
var blnChildVisible = false;
var chdFareType = "P";
var infFareType = "P";
var blnValid = document.forms[0].radDt[0].checked;
if (chidVisibility == 'true') {
	blnChildVisible = true;
}
var editCheckBoxColumnNo = 19; // Column number of edit check box
if (showFareDefByDepCountryCurrency == "Y") {
	editCheckBoxColumnNo = 18;
}

if (!blnChildVisible) {
	editCheckBoxColumnNo = 15;
	if (showFareDefByDepCountryCurrency == "Y") {
		editCheckBoxColumnNo = 14;
	}
}

var arrFareData = new Array();
var arrFareDataTmp = new Array();
var isSaveTransactionSuccessful = false;
var isSuccessfulSaveMessageDisplayEnabled = false;
var model = new Array();
var fowardMsg;

function radClick() {
	if (document.forms[0].radDt[0].checked) {
		blnValid = true;
	} else {
		blnValid = false;
	}
}
function loadCheck1(model) {
	if (objDG.loaded) {
		top[2].HideProgress();
		var dataArr = arrFareData;
		if (model != null && model.length > 0) {
			setPageEdited(true);
			var size = arrFareData.length;
			for ( var g = 0; g < size; g++) {
				var modelArr = model[0].split("~");
				for ( var t = 0; t < modelArr.length; t++) {
					var arrDt = modelArr[t].split(",");
					if (dataArr[g][1] == arrDt[0]) {
						var validFrom = testDate(arrDt[3]);
						var validTo = testDate(arrDt[4]);
						if (validFrom && !validTo) {
							objDG.setDisable(g, 4, false);
							objDG.setDisable(g, 5, false);

							if (blnChildVisible) {
								objDG.setDisable("", "9", false);
								objDG.setDisable("", "7", false);
							}
							objDG.setCellValue(g, 4, arrDt[4]);
							objDG.setCellValue(g, 5, arrDt[5]);
							objDG.setCellValue(g, 15, "true");
							objDG.setDisable(g, 4, false);
							objDG.setDisable(g, 5, false);
							arrFareData[g][19] = "Changed";
							fareEditArr[g] = "";
							objDG.setColumnFocus(g, 4);

						}
						if (!validFrom && !validTo) {
							objDG.setDisable(g, 3, false);
							objDG.setDisable(g, 4, false);
							objDG.setDisable(g, 5, false);

							if (blnChildVisible) {
								objDG.setDisable("", "9", false);
								objDG.setDisable("", "7", false);
							}
							objDG.setCellValue(g, 3, arrDt[3]);
							objDG.setCellValue(g, 4, arrDt[4]);
							objDG.setCellValue(g, 5, arrDt[5]);
							objDG.setCellValue(g, 15, "true");
							objDG.setDisable(g, 3, false);
							objDG.setDisable(g, 4, false);
							objDG.setDisable(g, 5, false);
							arrFareData[g][19] = "Changed";
							fareEditArr[g] = "EditFrom";
							objDG.setColumnFocus(g, 4);
						}
					}
				}
			}

			Disable("btnSave", false);
			Disable("btnOverWriteRule", false);
			Disable("txtExtendFrom", false);
			Disable("txtExtendTo", false);
			Disable("btnExtend", false);
			Disable("btnReset", false);
		} else {
			setPageEdited(false);
			objDG.setDisable("", "3", true);
			objDG.setDisable("", "4", true);
			objDG.setDisable("", "5", true);
			if (blnChildVisible) {
				objDG.setDisable("", "7", true);
				objDG.setDisable("", "9", true);
			}
			Disable("btnSave", true);
			Disable("btnOverWriteRule", true);
			setField("txtExtendTo", "");
			setField("txtExtendFrom", "");
			Disable("txtExtendFrom", true);
			Disable("txtExtendTo", true);
			Disable("btnExtend", true);
			Disable("btnReset", true);
			Disable("btnEdit", true);
			Disable("btnEditRule", true);
		}
	}
}

var fareRows = "";
var fareEditArr = new Array();
function winOnLoad(strMsg, strMsgType) {
	// if tab exists
	if (getTabValues(childScreenId, delimiter, "strInventoryFlightData") == "true") {
		alert("Manage Fares Tab is already open.Can not Proceed!!!!");
		top[1].objTMenu.tabRemove(screenId);
		top[2].HideProgress();
	} else {
		// if tab not exists
		Disable("txtFromDate", true);
		Disable("txtToDate", true);
		if (top[1].objTMenu.focusTab == screenId) {
			objCmb1.focus();
		}
		disableControls(true);
		Disable("btnAdd", false);

		setData();
		
		if(!linkedFaresVisibility){
			setDisplay("tblFareTypes",false);
		}
		
		if(!sameFareModificationVisibility){
			setDisplay("divModifyToSameFare",false);
		}
		
		setField("hdnOrigin", objCmb1.getText());
		if (getTabValues(screenId, delimiter, "status") == "Back") {
			if (objCmb1.getText() != "") {
				setField("hdnRecNo", "1");
				var url = setURL("false");
				top[2].ShowProgress();
				retrieveURL(url);
			}
		}
		if (getText("rasSF1") == "EffectiveAll") {
			Disable("txtFromDate", true);
			Disable("txtToDate", true);
		}
		setPageEdited(false);
		top[0].status = "";
	}
}

function KPValidate(control) {
	var strCC = getText(control);
	var strLen = strCC.length;
	var blnVal = isAlpha(strCC)
	if (blnVal) {
		setField(control, strCC.substr(0, strLen - 1));
		getFieldByID(control).focus();
	}
}

// To Handle Paging
function gridNavigations(intRecNo, intErrMsg) {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setField("hdnMode", "PAGING");
		setField("hdnRecNo", intRecNo);

		setTabValues(screenId, delimiter, "intLastRec", getText("hdnRecNo"));
		if (intErrMsg != "") {
			top[2].objMsg.MessageText = "<li> " + intErrMsg;
			top[2].objMsg.MessageType = "Error";
			top[2].ShowPageMessage();
		} else {
			setField("hdnState", "false");
			setField("hdnOrigin", objCmb1.getText());
			setField("hdnDestination", objCmb2.getText());
			setField("hdnVia1", objCmb3.getText());
			setField("hdnVia2", objCmb4.getText());
			setField("hdnVia3", objCmb6.getText());
			setField("hdnVia4", objCmb5.getText());
			var url = setURL("false");
			retrieveURL(url);
			top[2].ShowProgress();
		}
		setPageEdited(false);
	}
}

function setSearchDataForFare() {
	var valid = true;
	Disable("btnAdd", false);
	var via1 = objCmb3.getText();
	var via2 = objCmb4.getText();
	var via3 = objCmb6.getText();
	var via4 = objCmb5.getText();
	var strToDate = getText("txtToDate");
	var strFeromDate = getText("txtFromDate");
	var status = getValue("selStatus");
	var basis = getText("selBasisCode");
	var strOrigin = objCmb1.getText();
	var selDest = objCmb2.getText();
	var strBkCls = getText("txtBkingClass");
	var strFareRule = getText("txtFareRule");
	
	var strSearch;

	if (getText("hdnFareMode") == "Add") {		
		strSearch = strOrigin + "~" + selDest + "~" + via1 + "~" + via2 + "~"
		+ via3 + "~" + via4 + "~" + strBkCls + "~" + strToDate + "~"+ strFeromDate + "~" + strFareRule + "~" + "Add";
		setSearchTabData();
		

	} else if (getText("hdnFareMode") == "Edit") {
		var size = arrFareData.length;
		var arrChkData = objDG.getSelectedColumn(editCheckBoxColumnNo);
		var rowNo = 0;
		var count = 0;
		for ( var i = 0; i < size; i++) {
			if (arrChkData[i][1] == true) {
				count++;
				rowNo = i;
			}
		}

		if (count > 1) {
			showERRMessage(selectOneRow);
			return false;
		} else if (count == 0) {
			showERRMessage(rowRqrd);
			return false;
		} else if (count == 1) {
			var fareRule = "/";
			var are = arrFareData[rowNo][30];    // The fare rule part which is going to populate most of the lower section
			var version = arrFareData[rowNo][16];
			var active = arrFareData[rowNo][13];
			var adultFare = arrFareData[rowNo][6];
			var childFare = arrFareData[rowNo][24];
			var childFareType = arrFareData[rowNo][25];
			var infantFare = arrFareData[rowNo][26];
			var infantFareType = arrFareData[rowNo][27];
			var paxDetails = arrFareData[rowNo][23];
			var fareCategory = arrFareData[rowNo][22];// Fare Category
			var salesStartDate = arrFareData[rowNo][28];
			var salesEndDate = arrFareData[rowNo][29];
			var adultFareInLocal = arrFareData[rowNo][31];
			var childFareInLocal = arrFareData[rowNo][32];
			var infantFareInLocal = arrFareData[rowNo][33];
			var currencyCode = arrFareData[rowNo][34];
			strBkCls = arrFareData[rowNo][7];
			var returnFrom = arrFareData[rowNo][35];
			var returnTo = arrFareData[rowNo][36];
			/*link fares*/
			var masterFareStatus = arrFareData[rowNo][42];
			var masterFareID = arrFareData[rowNo][43];
			var masterFarePercentage = arrFareData[rowNo][44];
			var masterFareRefCode = arrFareData[rowNo][45];
			var modToSameFare = arrFareData[rowNo][46];
			
			if(currencyCode != null && currencyCode != "" && arrFareData[rowNo][40] == "Y"){
				adultFare = arrFareData[rowNo][37];
				childFare = arrFareData[rowNo][38];
				infantFare = arrFareData[rowNo][39];
			}
			
			for ( var f = 0; f < 17; f++) {
				if(f == 2 || f ==3){
					fareRule += getPeroidsArray(are[0][f]) + "/"
					/**  2  - Maximum stay over in Minutes
				 	 *   3  - Minimum stay over in Minutes
				 	 */
				}else{
					fareRule += are[0][f] + "/"
					/**
					 * 	0  - No of advance booking days
					 *  1  - Return flag set as Y/N
					 *  4  - Modification charge amount
					 *  5  - Cancellation charge Amount
					 *  6  - Out bound departure time to
					 *  7  - Out bound departure time from
					 *  8  - In bound departure time to
					 *  9  - In bound departure time from
					 *  10 - Rule comments
					 *  11 - is set to off need to check
					 *  12 - Status
					 *  13 - Out frequency
					 *  14 - In frequency
					 *  15 - Visibility codes
					 *  16 - agent codes
					 */
				 }
			}
			fareRule += arrFareData[rowNo][18] + "/"
			fareRule += are[0][18] + "/"  // Modify Buffer Days
			fareRule += are[0][19] + "/"  // Modify Buffer Hours
			fareRule += are[0][20] + "/"  // Modify Buffer Minutes
			fareRule += are[0][21] + "/" // Print Exp days
			fareRule += are[0][22] + "/" // added to match the array index
			fareRule += are[0][23] + "/" // added to match the array index
			fareRule += are[0][26] + "/" // Open return flag
			fareRule += getPeroidsArray(are[0][27]) + "/" // confirm period
			fareRule += basis + "/" // confirm period

			if(are[0][28] != ""){
				var tmpFlxArray = are[0][28].split(",");
				fareRule += tmpFlxArray[0] + "/"  // At the moment add first Flexi code, Future will be more
			}else {
				fareRule += "/";
			}
			fareRule +=are[0][29]+= "/";
			fareRule +=are[0][30]+= "/";
			fareRule +=are[0][31]+= "/";
			fareRule +=are[0][32]+= "/";
			fareRule +=are[0][33]+= "/"; 
			fareRule +=are[0][34]+= "/";
			//fare rule modifications and load factor
			fareRule +=are[0][35]+= "/";
			fareRule +=are[0][36]+= "/";
			fareRule +=are[0][37]+= "/";
			fareRule +=are[0][38]+= "/";
			fareRule +=are[0][39]+= "/";
			//Domestic modify buffer time
			fareRule +=are[0][40]+= "/";
			fareRule +=are[0][41]+= "/";
			fareRule +=are[0][42]+= "/";
			//fare discount 
			fareRule +=are[0][43]+= "/"; //flag
			fareRule +=are[0][44]+= "/"; //min
			fareRule +=are[0][45]+= "/"; //max
			fareRule +=arrFareData[rowNo][41]+= "/";
			fareRule +=are[0][46]+= "/"; //half-retunr flag			
			fareRule +=are[0][47]+= "/"; //mod-charge amount in local
			fareRule +=are[0][48]+= "/"; //cnx-charge amount in local
			fareRule +=are[0][49]+= "/"; //fare-rule local currency code
			fareRule +=are[0][50]+= "/";
			fareRule +=are[0][51];
			
			
			top[0].arrayData = "";
			top[0].arrayData = are[0][16];   //agent codes
			setTabValues(screenId, delimiter, "arrayData", are[0][17]); //sets the agents
			setField("hdnOrigin", strOrigin);
			strSearch = strOrigin + "~" + selDest + "~" + via1 + "~" + via2
					+ "~" + via3 + "~" + via4 + "~" + strBkCls + "~" 
					+ arrFareData[rowNo] + "~" +  version + "~Edit~" + fareRule + "~"
					+ active + "~" + adultFare + "~" + childFare + "~"
					+ childFareType + "~" + infantFare + "~" + infantFareType
					+ "~" + paxDetails + "~" + fareCategory + "~"
					+ salesStartDate + "~" + salesEndDate + "~" + adultFareInLocal
					+ "~" + childFareInLocal + "~" + infantFareInLocal
					+ "~" + returnFrom + "~" + returnTo 
					+ "~" + masterFareStatus + "~" + masterFareID+ "~" + masterFarePercentage + "~" + masterFareRefCode 
					+ "~" + modToSameFare;
					//+ "~" + currencyCode; // +","+agentArr;

			
			
			setTabValues(childScreenId, delimiter, "arrayData", are[0][17]);
			setSearchTabData();
			
		}
	}	
	setTabValues(screenId, delimiter, "strGridData", strSearch);
	setTabValues(childScreenId, delimiter, "strGridData", strSearch);
	setTabValues(childScreenId, delimiter, "strSearchCriteria", getTabValues(screenId, delimiter, "strSearchCriteria"));	
	
	return valid;
}

function getPeroidsArray(array){
	var srtPeriod = array[0]+"&"+array[1]+"&"+array[2]+"&"+array[3];
	return srtPeriod;
}

function searchData() {
	var validated = false;
	var strOrigin = objCmb1.getText();
	var selDest = objCmb2.getText();
	var via1 = objCmb3.getText();
	var via2 = objCmb4.getText();
	var via3 = objCmb6.getText();
	var via4 = objCmb5.getText();
	var radSOD = getText("radSOD1");
	var radSF1 = getText("rasSF1");
	var strToDate = getText("txtToDate");
	var strFeromDate = getText("txtFromDate");
	var cos = getText("selCOS");
	var status = getText("selStatus");
	var basis = getValue("selBasisCode");

	if (strOrigin == "") {
		showERRMessage(originRqrd);
		objCmb1.focus();
		validated = false;
	} else if (selDest == "") {
		showERRMessage(destinationRqrd);
		objCmb2.focus();
		validated = false;
	} else if (objCmb1.getText() != "" && objCmb1.comboValidate() != true) {
		showERRMessage(OriginDoesNotExists);
		objCmb1.focus();
	} else if (objCmb1.getText() != "" && objCmb1.comboValidate() != true
			&& objCmb1.getText() == "Select") {
		showERRMessage(originRqrd);
		objCmb1.focus();
		validated = false;
	} else if (objCmb2.getText() != "" && objCmb2.comboValidate() != true) {
		showERRMessage(DestinationDoesNotExists);
		objCmb2.focus();
	} else if (objCmb2.getText() != "" && objCmb2.comboValidate() != true
			&& objCmb2.getText() == "Select") {
		showERRMessage(destinationRqrd);
		objCmb2.focus();
		validated = false;
	} else if (strOrigin == selDest) {
		showERRMessage(originDestEqual);
		objCmb2.focus();
		validated = false;
	} else if (objCmb3.getText() != "" && objCmb3.comboValidate() != true) {
		showERRMessage(Via1DoesNotExists);
		objCmb3.focus();
	} else if (objCmb4.getText() != "" && objCmb4.comboValidate() != true) {
		showERRMessage(Via2DoesNotExists);
		objCmb4.focus();
	} else if (objCmb6.getText() != "" && objCmb6.comboValidate() != true) {
		showERRMessage(Via3DoesNotExists);
		objCmb6.focus();
	} else if (objCmb5.getText() != "" && objCmb5.comboValidate() != true) {
		showERRMessage(Via4DoesNotExists);
		objCmb5.focus();
	} else if (strOrigin == via1 || strOrigin == via2 || strOrigin == via3
			|| strOrigin == via4) {
		showERRMessage(arriavlViaSame);
		objCmb1.focus();
	} else if (selDest == via1 || selDest == via2 || selDest == via3
			|| selDest == via4) {
		showERRMessage(depatureViaSame);
		objCmb2.focus();
	} else if ((via1 != "") && (via1 == via2 || via1 == via3 || via1 == via4)) {
		showERRMessage(viaEqual);
		objCmb3.focus();
		validated = false;
	} else if ((via2 != "") && (via2 == via3 || via2 == via4)) {
		showERRMessage(viaEqual);
		objCmb4.focus();
		validated = false;
	} else if ((via3 != "") && (via3 == via4)) {
		showERRMessage(viaEqual);
		objCmb6.focus();
		validated = false;
	} else if (radSOD == "") {
		showERRMessage(sodRqrd);
		validated = false;
	} else if (radSF1 == "") {
		showERRMessage(fc1Rqrd);
		validated = false;
	} else if (radSF1 == "From" && strToDate != "" && !dateValidDate(strToDate)) {
		showERRMessage(toDateIncorrect);
		getFieldByID("txtToDate").focus();
		validated = false;
	} else if (radSF1 == "From" && strFeromDate != ""
			&& !dateValidDate(strFeromDate)) {
		showERRMessage(fromDateIncorrect);
		getFieldByID("txtFromDate").focus();
		validated = false;
	} else if (radSF1 == "From" && strFeromDate != "" && strToDate != ""
			&& !CheckDates(strFeromDate, strToDate)) {
		showERRMessage(datesInvalid);
		getFieldByID("txtFromDate").focus();
		validated = false;
	} else {
		validated = true;
		top[2].HidePageMessage();
		var searchStr = strOrigin + "," + selDest + "," + via1 + "," + via2
				+ "," + via3 + "," + via4 + "," + strToDate + ","
				+ strFeromDate + "," + radSOD + "," + radSF1 + "," + cos + ","
				+ status + "," + basis + ",";
		setSearchTabData();
		var fields = radSOD + "," + radSF1;
	}

	return validated;
}

function setSearchTabData() {	
	var strOrigin = objCmb1.getText();
	var selDest = objCmb2.getText();
	var via1 = objCmb3.getText();
	var via2 = objCmb4.getText();
	var via3 = objCmb6.getText();
	var via4 = objCmb5.getText();
	var radSOD = getText("radSOD1");
	var radSF1 = getText("rasSF1");
	var strToDate = getText("txtToDate");
	var strFeromDate = getText("txtFromDate");
	var cos = getText("selCOS");
	var status = getText("selStatus");
	var basis = getValue("selBasisCode");
	var strBKCls = getText("txtBkingClass");
	var strFareRule = getText("txtFareRule");
	
	var searchStr = strOrigin + "," + selDest + "," + via1 + "," + via2 + ","
			+ via3 + "," + via4 + "," + strToDate + "," + strFeromDate + ","
			+ radSOD + "," + radSF1 + "," + cos + "," + status + "," + basis
			+ "," + strBKCls
			+ "," + strFareRule;	
	setTabValues(screenId, delimiter, "strSearchCriteria", searchStr);
	setTabValues(screenId, delimiter, "intLastRec", getText("hdnRecNo"));
	setTabValues(screenId, delimiter, "tempSearch", searchStr);
}

function processStateChange() {
	if (reqObj.readyState == 4) {
		if (reqObj.status == 200) {
			strSYSError = "";
			eval(reqObj.responseText);
			if (strSYSError == "") {
				if (fowardMsg == "OND") {
					objDG1.arrGridData = eval(arrFareChargeData);
					objDG1.pgnumRecTotal = totalChargeRecs;
					objDG1.noDataFoundMsg = "No Data Found";
					objDG1.refresh();
					top[2].HideProgress();
					loadCheck1(model);
				} else {
					if (document.forms[0].radDt[2].checked) {
						objDG.replaceColumn(objCol24, 3);
						objDG.replaceColumn(objCol25, 4);
					} 
					else if (document.forms[0].radDt[1].checked) {
						objDG.replaceColumn(objCol21, 3);
						objDG.replaceColumn(objCol22, 4);
					} else {
						objDG.replaceColumn(objCol4, 3);
						objDG.replaceColumn(objCol5, 4);
					}
					objDG.arrGridData = arrFareData;
					objDG.pgnumRecTotal = totalRes; // remove as per return
					// record size
					objDG.seqStartNo = getTabValues(screenId, delimiter,
							"intLastRec");
					objDG.noDataFoundMsg = "No Data Found";
					objDG.refresh();					
					isSuccessfulSaveMessageDisplayEnabled = isSuccessfulSaveMessageDisplayEnabled;
					isSaveTransactionSuccessful = isSaveTransactionSuccessful;
					if (isSuccessfulSaveMessageDisplayEnabled) {
						if (isSaveTransactionSuccessful) {
							showCommonError("Confirmation", strMsg);
							alert("Record Successfully saved!");
							top[2].HideProgress();
						} else {
							top[2].HideProgress();
						}

					}
					model = eval(model);
					loadCheck1(model);
					objDG1.arrGridData = eval(arrFareChargeData);
					objDG1.noDataFoundMsg = "";
					objDG1.refresh();
				}

			} else {
				showERRMessage(strSYSError);
				ShowPageMessage();
				top[2].HideProgress();
			}
		} else {
			showERRMessage(reqObj.statusText);
			ShowPageMessage();
			top[2].HideProgress();
		}
	}
}

function setURL(state) {
	var strOrigin = objCmb1.getText();
	var selDest = objCmb2.getText();
	var via1 = objCmb3.getText();
	var via2 = objCmb4.getText();
	var via3 = objCmb6.getText();
	var via4 = objCmb5.getText();
	var radSOD = getText("radSOD1");
	var radSF1 = getText("rasSF1");
	var strToDate = getText("txtToDate");
	var strFeromDate = getText("txtFromDate");
	var cos = getValue("selCOS").split("_")[0];
	var status = getValue("selStatus");
	var basis = getValue("selBasisCode");
	var bkCode = getValue("txtBkingClass");
	var fareRule = getValue("txtFareRule");
	var recNo = getText("hdnRecNo");
	var url = "loadData.action?&hdnState=" + state + "&hdnOrigin=" + strOrigin
			+ "&hdnDestination=" + selDest + "&hdnVia1=" + via1 + "&hdnVia2="
			+ via2 + "&hdnVia3=" + via3 + "&hdnVia4=" + via4 + "&radSOD1="
			+ radSOD + "&rasSF1=" + radSF1 + "&txtToDate=" + strToDate
			+ "&txtFromDate=" + strFeromDate + "&selCOS=" + cos + "&selStatus="
			+ status + "&selBasisCode=" + basis + "&txtBkingClass=" + bkCode
			+ "&txtFareRule=" + fareRule
			+ "&hdnRecNo=" + recNo;
	setTabValues(screenId, delimiter, "intLastRec", getText("hdnRecNo"));
	return url;
}

function search_click() {	
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {		
		if (!searchData())
			return;
		else {			
			if (getText("selCOS") == "") {
				showERRMessage(cosRequired);
				getFieldByID("selCOS").focus();
				validated = false;
			} else {				
				arrFareData.length = 0;
				setField("hdnState", "false");
				top[0].intLastRec = 1;
				setField("hdnRecNo", "1");
				setField("hdnOrigin", objCmb1.getText());
				setTabValues(screenId, delimiter, "intLastRec", getText("hdnRecNo"));
				var url = setURL("false");
				setField("hdnMode", "");
				top[2].ShowProgress();				
				retrieveURL(url);
			}
		}
		setPageEdited(false);
	}

}

function searchCharges_click() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		if (!searchData())
			return;
		else {
			setField("hdnState", "true");
			top[0].intLastRec = 1;
			setField("hdnRecNo", "1");
			var url = setURL("true");
			url += "&selPOS=" + getText("selPOS");
			top[2].ShowProgress();
			setField("hdnMode", "");
			retrieveURL(url);

		}
	}

}

function radEffective_click() {
	var radSF1 = getText("rasSF1");
	if (radSF1 == "From") {
		setField("txtFromDate", "");
		setField("txtToDate", "");
		Disable("txtFromDate", false);
		Disable("txtToDate", false);
	} else {
		setField("txtFromDate", "");
		setField("txtToDate", "");
		Disable("txtFromDate", true);
		Disable("txtToDate", true);
	}
}

var objCal1 = new Calendar("spnCalendarDG1");
objCal1.onClick = "setDate";
objCal1.buildCalendar();

function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField("txtFromDate", strDate);
		break;
	case "1":
		setField("txtToDate", strDate);
		break;
	case "3":
		setField("txtExtendFrom", strDate);
		break;
	case "4":
		frames["frm_DE"].setDate(strDate, strID);
		break;
	case "5":
		setField("txtExtendTo", strDate);
		break;
	}
}

function LoadCalendar(strID, objEvent) {
	var effective = getText("rasSF1");
	if (effective == "From") {
		objCal1.ID = strID;
		objCal1.top = 0;
		objCal1.left = 0;
		objCal1.onClick = "setDate";
		objCal1.showCalendar(objEvent);
	}

}

function LoadNewCalendar(strID, objEvent) {
	if (!document.getElementById("btnExtend").disabled) {
		objCal1.ID = strID;
		objCal1.top = 400;
		if(strID == 5)
			objCal1.left = 430;
		else
			objCal1.left = 250;
		objCal1.onClick = "setDate";
		objCal1.showCalendar(objEvent);
	}
}

function rule_click(state) {
	
	if ((top[1].objTMenu.tabIsAvailable(childScreenId) && top[1].objTMenu.tabRemove(childScreenId))			
			|| (!top[1].objTMenu.tabIsAvailable(childScreenId))) {		
		if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {		
			var cos = document.getElementById("selCOS").options[document.getElementById("selCOS").selectedIndex].value;
			if (setSearchDataForFare()) {				
				setField("hdnOrigin", objCmb1.getText());
				top[0].ruleStatus = "";
				top[1].objTMenu.tabSetTabInfo(screenId, childScreenId, "showFares.action", "Manage Fare", false);
				setTabValues(screenId, delimiter, "strInventoryFlightData",	"true");
				location.replace("showFares.action?cos="+cos);
			}
			setPageEdited(false);
		}
	}
}

function viewCharges() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		top[0].intLastRec = 1;
		top[0].ruleStatus = 1;
		document.forms["frmViewFares"].submit();

	}
}

function editFareData() {
	ctrlSetStatus();
	Disable("btnSave", false);
	Disable("btnOverWriteRule", false);
	var size = arrFareData.length;
	var str = "";
	var t;
	var count = 0;

	for ( var i = 0; i < size; i++) {
		
		var from = arrFareData[i][4] // objDG.getCellValue(i,3);
		var to = arrFareData[i][5] // objDG.getCellValue(i,4);
		var aed = arrFareData[i][6] // objDG.getCellValue(i,5);
		var salesfrm = arrFareData[i][28];
		var salesto = arrFareData[i][29];
		var returnFrom = arrFareData[i][35];
		var returnTo = arrFareData[i][36];
		var validFrom;
		var validTo;
		if (document.forms[0].radDt[2].checked) {
			validFrom = testDate(returnFrom);
			validTo = testDate(returnTo);
		} 
		else if (document.forms[0].radDt[1].checked) {
			validFrom = testDate(salesfrm);
			validTo = testDate(salesto);
		} else {
			validFrom = testDate(from);
			validTo = testDate(to);
		}
		// making can extend any fare
		// var validTo=false;
		var status = objDG.getCellValue(i, editCheckBoxColumnNo); // if check
		// box is
		// select
		// status
		// will be
		// true
		fareEditArr[i] = "";
 
		if (status) {
			var isLinkFare = false;
			if((arrFareData[i][43] != null && arrFareData[i][43] != "")  && (arrFareData[i][44] != null && arrFareData[i][44] != "")){							
				isLinkFare = true;
			}
			if(!isLinkFare){
				if (validFrom && validTo) {
					objDG.setDisable(i, 3, true);
					objDG.setDisable(i, 4, true);
					objDG.setDisable(i, 5, true);

					if (blnChildVisible) {
						objDG.setDisable(i, 9, true);
						objDG.setDisable(i, 7, true);
					}
					fareEditArr[i] = "EditTo";

				} else if (validFrom && !validTo) {


					//if (blnValid) {
					objDG.setDisable(i, 3, true);
					objDG.setDisable(i, 4, false);
					objDG.setDisable(i, 5, false);
					/*} else {
					objDG.setDisable(i, 3, true);
					objDG.setDisable(i, 4, false);
					objDG.setDisable(i, 5, false);
				}*///21122011 CJ--> Remove above if condition as I cant understand if and else doing the same. If somebody remembers for 
					//which purpose this has done or if there is any other reason beyond the concept of if/else pls let us know

					if (blnChildVisible) {
						objDG.setDisable(i, 9, false);
						objDG.setDisable(i, 7, false);
					}
					objDG.setColumnFocus(i, 4);
					fareEditArr[i] = "EditTo";

				} else if (!validFrom && !validTo) {
					//if (blnValid) {
					objDG.setDisable(i, 3, false);
					objDG.setDisable(i, 4, false);
					objDG.setDisable(i, 5, false);
					/*} else {
					objDG.setDisable(i, 3, false);
					objDG.setDisable(i, 4, false);
					objDG.setDisable(i, 5, false);
				}*///21122011 CJ--> Remove above if condition as I cant understand if and else doing the same. If somebody remembers for 
					//which purpose this has done or if there is any other reason beyond the concept of if/else pls let us know

					if (blnChildVisible) {
						objDG.setDisable(i, 9, false);
						objDG.setDisable(i, 7, false);
					}
					objDG.setColumnFocus(i, 3);
					fareEditArr[i] = "EditFrom";

				} else {
					objDG.setDisable(i, 3, true);
					objDG.setDisable(i, 4, true);
					objDG.setDisable(i, 5, true);

					if (blnChildVisible) {
						objDG.setDisable(i, 9, true);
						objDG.setDisable(i, 7, true);
					}
					fareEditArr[i] = "";
				}
				chdFareType = arrFareData[i][25];
				infFareType = arrFareData[i][27];
			}
		}
		count++;

	}
	if (count == 0) {
		showERRMessage(selectFare);

	} else {
		top[2].HidePageMessage();
	}

}

function disableData(disabled) {
	frames["grdViewFare"].objDG.setDisable("", "3", disabled);
	frames["grdViewFare"].objDG.setDisable("", "4", disabled);
	frames["grdViewFare"].objDG.setDisable("", "5", disabled);

}

function ctrlSetStatus() {
	var arrChkData = objDG.getSelectedColumn(editCheckBoxColumnNo);
	for ( var i = 0; i < arrChkData.length; i++) {
		if (arrChkData[i][1] == true) {
			objDG.setCellValue(i, editCheckBoxColumnNo, "true");
			arrFareData[i][19] = "Changed";
			fareRows += i;
		} else {
			objDG.setCellValue(i, editCheckBoxColumnNo, "false");
		}

	}
}

function edit_click() {
	editFareData();
	Disable("btnReset", "");
}

function testDate(dt) {
	var validated = true;

	var dtC = new Date();
	var dtCM = dtC.getMonth() + 1;
	var dtCD = dtC.getDate();
	if (dtCM < 10) {
		dtCM = "0" + dtCM
	}
	if (dtCD < 10) {
		dtCD = "0" + dtCD
	}
	var strSysDate = systemDate;
	var valid = dateChk(strSysDate);

	var tempDay = dt.substring(0, 2);
	var tempMonth = dt.substring(3, 5);
	var tempYear = dt.substring(6, 10);
	var tempOStartDate = (tempYear + tempMonth + tempDay);

	tempDay = strSysDate.substring(0, 2);
	tempMonth = strSysDate.substring(3, 5);
	tempYear = strSysDate.substring(6, 10);
	var strSysODate = (tempYear + tempMonth + tempDay);

	if (valid) {
		if (tempOStartDate >= strSysODate) {
			validated = false;
		}
	}
	return validated;

}

function extend_click() {
	Disable("btnSave", false);
	Disable("btnOverWriteRule", false);
	Disable("btnReset", false);
	ctrlSetStatus();
	var validated = false;
	var size = arrFareData.length;
	var arrChkData = objDG.getSelectedColumn(13);
	var arrSelData = objDG.getSelectedColumn(editCheckBoxColumnNo);

	var fromDate = getText("txtExtendFrom");
	var toDate = getText("txtExtendTo");
	for ( var i = 0; i < size; i++) {
		if (arrSelData[i][1] == true) {
			var grdToDate = objDG.getCellValue(i, "3");

			if (fromDate == "" && toDate == "") {
				showERRMessage(dateRqrd);
			} else if (fromDate != "" && testDate(fromDate)) {
				showERRMessage(extendGreater);
			} else if (toDate != "" && testDate(toDate)) {
				showERRMessage(todateShouldExceed);
			} else if (toDate != "" && fromDate != ""
					&& !CheckDates(fromDate, toDate)) {
				showERRMessage(todateShouldExceedFrom);

			} else {
				top[2].HidePageMessage();

				if (getText("txtExtendFrom") != "" && !testDate(grdToDate)) {
					objDG.setCellValue(i, "3", getText("txtExtendFrom"));
					setPageEdited(true);
					arrFareData[i][19] = "Changed";
					validated = true;
				}
				if (getText("txtExtendTo") != "") {
					objDG.setCellValue(i, "4", getText("txtExtendTo"));
					setPageEdited(true);
					arrFareData[i][19] = "Changed";
					validated = true;
				}
			}
		}
	}
	if (validated == true) {
		setField("txtExtendTo", "");
		setField("txtExtendFrom", "");
	}
}

function disableControls(status) {

	Disable("btnAdd", status);
	Disable("btnEdit", status);
	Disable("btnEditRule", status);
	Disable("btnSave", status)
	Disable("btnOverWriteRule", status);
	Disable("txtExtendFrom", status);
	Disable("txtExtendTo", status);
	Disable("btnReset", status);
	Disable("btnExtend", status);

}

function add_click() {
	setField("hdnFareMode", "Add");
}

function upload_click() {
	var intLeft = (window.screen.width - 800) / 2;
	var intTop = (window.screen.height - 500) / 2;	
	top[0].objWindow = window.open("showUploadFares.action","CWindow",'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width=600,height=270,resizable=no,top=' + intTop + ',left=' + intLeft);

}

function editRule_click() {
	setField("hdnFareMode", "Edit");
	setField("hdnOrigin", objCmb1.getText());
}

function save_click() {

	if (!validateFareData()) {
		return;

	} else {
		setField("hdnRecNo", (top[0].intLastRec));
		setTabValues(screenId, delimiter, "intLastRec", getText("hdnRecNo"));
		getFareData();
		var versions = getText("hdnVersion");
		var fareIds = getText("hdnFareIDs");
		var fareRules = getText("hdnFareRules");
		top[2].ShowProgress();
		var url = setURL("false");
		url += "&hdnFareRules=" + fareRules + "~&hdnFareIDs=" + fareIds	+ ",&hdnVersion=" + versions + ",&hdnMode=SAVE";
		retrieveURL(url);
	}
}

function getFareData() {

	var versions = "";
	var strData = "";
	var fareIDs = "";
	for ( var i = 0; i < arrFareData.length; i++) {
		if (arrFareData[i][19] == "Changed") {
			var from = dateChk(objDG.getSelectedColumn(3)[i][1]);
			var to = dateChk(objDG.getSelectedColumn(4)[i][1]);
			objDG.setCellValue(i, 3, from);
			objDG.setCellValue(i, 4, to);
			strData += arrFareData[i][1] + "," + arrFareData[i][4] + ","
					+ arrFareData[i][5] + "," + arrFareData[i][6] + ","
					+ arrFareData[i][7] + "," + arrFareData[i][11] + ","
					+ arrFareData[i][13] + "," + arrFareData[i][24] + ","
					+ arrFareData[i][25] + "," + arrFareData[i][26] + ","
					+ arrFareData[i][27] + "," + arrFareData[i][28] + ","
					+ arrFareData[i][29] + "," + arrFareData[i][35] + ","
					+ arrFareData[i][36] + "," ;
			if (arrFareData[i][34] == "") {
				strData += "null";
			} else {
				strData += arrFareData[i][34];
			}
			var masterFareRefCode = arrFareData[i][45];
			if(masterFareRefCode == null || masterFareRefCode == ""){
				masterFareRefCode = "null"
			}
			strData += "," + arrFareData[i][42] + "," + arrFareData[i][43] + ","
			+ arrFareData[i][44] + "," + masterFareRefCode + ",";
			strData += "~";
			versions += arrFareData[i][16] + ","
			var fareIDsArr = arrFareData[i][18].split("::");
			fareIDs += fareIDsArr[0] + ",";
		}
	}
	setField("hdnVersion", versions.substring(0, versions.lastIndexOf(",")));
	setField("hdnFareIDs", fareIDs.substring(0, fareIDs.lastIndexOf(",")));
	setField("hdnFareRules", strData.substring(0, strData.lastIndexOf("~")));
}

function rowNumbersOfChanges() {
	if (arrChkData[i][1] == true) {
		fareRows += i + ",";
	}
}

function setData() {	
	var objFrameChargeArr = frames["grdONDFare"];
	var strSearch;
	if (getTabValues(screenId, delimiter, "status") == "Back") {
		strSearch = getTabValues(screenId, delimiter, "tempSearch");
	} else {
		strSearch = getTabValues(screenId, delimiter, "strSearchCriteria");
	}	
	if (strSearch != "" && strSearch != null) {
		objDG1.noDataFoundMsg = "No Data Found";
		var arr = strSearch.split(",");
		objCmb1.setComboValue(arr[0]);
		objCmb2.setComboValue(arr[1]);

		if (arr[2] != null) {
			objCmb3.setComboValue(arr[2]);
		}
		if (arr[3] != null) {
			objCmb4.setComboValue(arr[3]);
		}
		if (arr[4] != null) {
			objCmb6.setComboValue(arr[4]);
		}
		if (arr[5] != null) {
			objCmb5.setComboValue(arr[5]);
		}
		if (arr[6] != null) {
			setField("txtToDate", arr[6]);
			Disable("txtFromDate", false);
			Disable("txtToDate", false);
		}
		if (arr[7] != null) {
			setField("txtFromDate", arr[7]);
		}
		setField("radSOD1", arr[8]);
		setField("rasSF1", arr[9]);
		if (arr[10] != null) {
			setCOS(arr[10]);
		}
		if (arr[11] != null) {
			if (arr[11] == "Inactive") {
				setField("selStatus", "INA");
			} if (arr[11] == "Active") {
				setField("selStatus", "ACT");
			} if (arr[11] == "All") {
				setField("selStatus", "ALL");
			}
		}
		if (arr[12] != null) {
			setField("selBasisCode", arr[12]);
		}
		if (arr[15] != null) {
			setField("selPos", arr[15]);
		}
		if (arr[13] != null) {
			setField("txtBkingClass", arr[13]);
		}
		if (arr[14] != null) {
			setField("txtFareRule", arr[14]);
		}
		Disable("btnGetCharges", false);
		Disable("selPOS", false);
		Disable("btnAdd", false);
	} else {
		setField("radSOD1", "AllComb");
		setField("rasSF1", "EffectiveAll");
		if (getFieldByID("selCOS").options.length == 1) {
			getFieldByID("selCOS").options[0].selected = true;
		} else {
			var control = document.getElementById("selCOS");
			for ( var t = 0; t < (control.length); t++) {
				if (control.options[t].text.toUpperCase().indexOf("ECONOMY") != -1) {
					control.options[t].selected = true;
					break;
				}
			}
		}
	}
}

function setCOS(strCode) {
	var control = document.getElementById("selCOS");
	for ( var t = 0; t < (control.length); t++) {
		if (control.options[t].text == strCode) {
			control.options[t].selected = true;
			break;
		}
	}
}

function validateFareData() {
	ctrlSetStatus();
	var arrChkData = objDG.getSelectedColumn(5);
	var from = "";
	var to = "";
	var salesFrom = "";
	var salesTo = "";
	var aed = "";
	var validated = true;
	var size = arrFareData.length;
	var rowCount = 1;

	if (getText("txtExtendTo") == "" && getText("txtExtendFrom") == "") {
		for ( var i = 0; i < arrFareData.length; i++) {
			if (arrFareData[i][19] == "Changed") {
				var from, to;
				//if (blnValid) {
					from = objDG.getSelectedColumn(3)[i][1];
					to = objDG.getSelectedColumn(4)[i][1];
					// salesFrom=arrFareData[i][28];
					// salesTo=arrFareData[i][29];
				/*} else {
					// from= arrFareData[i][4];
					// to = arrFareData[i][5];
					from = objDG.getSelectedColumn(3)[i][1];
					to = objDG.getSelectedColumn(4)[i][1];
				}*///21122011 CJ--> Remove above if condition as I cant understand if and else doing the same. If somebody remembers for 
				//which purpose this has done or if there is any other reason beyond the concept of if/else pls let us know
				var aed = objDG.getSelectedColumn(5)[i][1];
				var childFare = objDG.getSelectedColumn(7)[i][1];
				var infantFare = objDG.getSelectedColumn(9)[i][1];

				var intAmt = isNaN(aed);
				var intChildFareAmt = isNaN(childFare);
				var intInfantFareAmt = isNaN(infantFare);

				var validate = testDate(from);
				var validateDates = CheckDates(from, to);
				if (from == "") {
					showERRMessage(fromdateRqrd + "in row " + rowCount);
					validated = false;
					return false;
				}
				if (from != "" && !dateValidDate(from)) {
					showERRMessage(fromDateIncorrect + " in row" + rowCount);
					validated = false;
					return false;
				}
				if (fareEditArr[i] == "EditFrom" && from != ""
						&& testDate(from)) {
					showERRMessage(FromDateExceedsCurrentDate + " in row "
							+ rowCount);
					validated = false;
					return false;
				}
				if (fareEditArr[i] == "EditTo" && to != "" && testDate(to)) {
					showERRMessage(toDateLess + " in row " + rowCount);
					validated = false;
					return false;
				}
				if (fareEditArr[i] == "EditFrom" && from != "" && to != ""
						&& !CheckDates(from, to)) {
					showERRMessage(datesInvalid + " in row " + rowCount);
					validated = false;
					return false;
				}
				if (to == "") {
					showERRMessage(todateRqrd + " in row " + rowCount);
					validated = false;
					return false;
				}
				if (to != "" && !dateValidDate(to)) {
					showERRMessage(toDateIncorrect + " in row " + rowCount);
					validated = false;
					return false;
				}
				if (from != "" && to != "" && !CheckDates(from, to)) {
					showERRMessage(datesInvalid + " in row " + rowCount);
					validated = false;
					return false;
				}
				if (aed == "") {
					showERRMessage(AEDRqrd + " in row " + rowCount);
					validated = false;
					return false;
				}
				if (aed != "" && intAmt) {
					showERRMessage(amountNan + " in row " + rowCount);
					validated = false;
					return false;
				}
				if (aed != "" && parseFloat(aed) <= 0) {
					//var confirmed = confirm("Fare Amount(" + baseCurrency
							//+ ") is Zero in row " + rowCount
							//+ ". Do you wish to continue?");
					var confirmed = confirm("Fare Amount is Zero in row " + rowCount
							+ ". Do you wish to continue?");		
					if (confirmed) {
						validated = true;
					} else {
						validated = false;
						return false;
					}

				}// validation for child and infant fares
				if (blnChildVisible && childFare == "") {
					showERRMessage("Child fare cannot be empty" + " in row "
							+ rowCount);
					validated = false;
					return false;
				}
				if (blnChildVisible && childFare != "" && intChildFareAmt) {
					showERRMessage("Invalid child fare amount" + " in row "
							+ rowCount);
					validated = false;
					return false;
				}
				
				if (blnChildVisible && childFare != "" && (arrFareData[i][25]=="P" && childFare > 100)) {
					showERRMessage("Child fare percentage cannot be lager than 100 in row "
							+ rowCount);
					validated = false;
					return false;
				}
				
				if (blnChildVisible && childFare != ""
						&& parseFloat(childFare) <= 0) {
					//var confirmed = confirm("Child Fare Amount(" + baseCurrency
					//		+ ") is Zero in row " + rowCount
					//		+ ". Do you wish to continue?");
					var confirmed = confirm("Child Fare Amount is Zero in row " + rowCount
							+ ". Do you wish to continue?");
					if (confirmed) {
						validated = true;
					} else {
						validated = false;
						return false;
					}
				}
				if (blnChildVisible && infantFare == "") {
					showERRMessage("Infant fare cannot be empty" + " in row "
							+ rowCount);
					validated = false;
					return false;
				}
				if (blnChildVisible && infantFare != "" && intInfantFareAmt) {
					showERRMessage("Invalid infant fare amount" + " in row "
							+ rowCount);
					validated = false;
					return false;
				}
				
				if (blnChildVisible && infantFare != "" && (arrFareData[i][27]=="P" && infantFare > 100)) {
					showERRMessage("Infant fare percentage cannot be lager than 100 in row "
							+ rowCount);
					validated = false;
					return false;
				}
				
				if (blnChildVisible && infantFare != ""
						&& parseFloat(infantFare) <= 0) {
					//var confirmed = confirm("Infant Fare Amount("
					//		+ baseCurrency + ") is Zero in row " + rowCount
					//		+ ". Do you wish to continue?");
					var confirmed = confirm("Infant Fare Amount is Zero in row " + rowCount
							+ ". Do you wish to continue?");
					if (confirmed) {
						validated = true;
					} else {
						validated = false;
						return false;
					}
				}

				if (validated) {
					if (i == (size - 1)) {
						return true;
					}
				}
			}
			rowCount++;
		}
	} else {
		showERRMessage(transactionIncomplete);
		return false;
	}
	return validated;
}

function reset_click() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		top[2].HidePageMessage();
		var arrChkData = objDG.getSelectedColumn(editCheckBoxColumnNo);
		for ( var i = 0; i < arrChkData.length; i++) {
			if (arrChkData[i][1] == true) {
				objDG.setCellValue(i, 3, arrFareDataTmp[i][2]);
				objDG.setCellValue(i, 4, arrFareDataTmp[i][3]);
				objDG.setCellValue(i, 5, arrFareDataTmp[i][4]);
				objDG.setDisable(i, "3", true);
				objDG.setDisable(i, "4", true);
				objDG.setDisable(i, "5", true);
				if (chidVisibility) {
					objDG.setCellValue(i, 7, arrFareDataTmp[i][5]);
					objDG.setCellValue(i, 9, arrFareDataTmp[i][6]);
					objDG.setDisable(i, "7", true);
					objDG.setDisable(i, "9", true);
				}
				objDG.setCellValue(i, 15, "false");
				arrFareData[i][19] = "Reset";
			}
		}
	}
}

var objCmb1 = new combo();
var objCmb2 = new combo();
var objCmb3 = new combo();
var objCmb4 = new combo();
var objCmb5 = new combo();
var objCmb6 = new combo();

buildCombos();
var objInterval = setInterval("comboLoad()", 50);

function comboLoad() {
	objCmb1.visible(true);
	clearInterval(objInterval);
}

function buildCombos() {

	objCmb1.id = "selAgentCmb";
	objCmb1.top = "47";
	objCmb1.left = "65";
	objCmb1.width = "50";
	objCmb1.valueIndex = 0;
	objCmb1.textIndex = 1;
	objCmb1.tabIndex = 1;
	objCmb1.arrData = arrAirports; // 2D Data Array
	objCmb1.onChange = "combo1Change";
	objCmb1.maxlength = 3;

	objCmb2.id = "selGSACmb";
	objCmb2.top = "47";
	objCmb2.left = "280";
	objCmb2.width = "50";
	objCmb2.valueIndex = 0;
	objCmb2.textIndex = 1;
	objCmb2.tabIndex = 2;
	objCmb2.arrData = arrAirports;
	objCmb2.onChange = "combo2Change";
	objCmb2.maxlength = 3;

	objCmb3.id = "selAirportCmb";
	objCmb3.top = "47";
	objCmb3.left = "435";
	objCmb3.width = "50";
	objCmb3.tabIndex = 3;
	objCmb3.arrData = arrAirports;
	objCmb3.onChange = "combo3Change";
	objCmb3.maxlength = 3;

	objCmb4.id = "selTerritoryCmb";
	objCmb4.top = "47";
	objCmb4.left = "548";
	objCmb4.width = "50";
	objCmb4.tabIndex = 4;
	objCmb4.arrData = arrAirports;
	objCmb4.onChange = "combo4Change";
	objCmb4.maxlength = 3;

	objCmb6.id = "selTerritoryCmb1";
	objCmb6.top = "47";
	objCmb6.left = "711";
	objCmb6.width = "50";
	objCmb6.tabIndex = 5;
	objCmb6.arrData = arrAirports;
	objCmb6.onChange = "combo6Change";
	objCmb6.maxlength = 3;

	objCmb5.id = "selAirportCmb1";
	objCmb5.top = "47";
	objCmb5.left = "843";
	objCmb5.width = "50";
	objCmb5.tabIndex = 6;
	objCmb5.arrData = arrAirports;
	objCmb5.onChange = "combo5Change";
	objCmb5.maxlength = 3;
	objCmb5.buildCombo();

}

function combo1Change() {
	objCmb1.focus();
}

function combo2Change() {
	objCmb2.focus();
}

function combo3Change() {
	objCmb3.focus();
}

function combo4Change() {
	objCmb4.focus();
}

// --------------------------------------------------------------------------
// FIXME
var objCol1 = new DGColumn();
objCol1.columnType = "label";
objCol1.width = "5%";
objCol1.arrayIndex = 1;
objCol1.toolTip = "Fare ID";
objCol1.headerText = "Fare ID";
objCol1.itemAlign = "left"
objCol1.sort = "true";

var objCol2 = new DGColumn();
objCol2.columnType = "label";
objCol2.width = "7%";
objCol2.arrayIndex = 2
objCol2.toolTip = "Fare Rule Code";
objCol2.headerText = "Fare Rule";
objCol2.itemAlign = "left"
objCol2.sort = "true";

var objCol3 = new DGColumn();
objCol3.columnType = "label";
objCol3.width = "7%";
objCol3.arrayIndex = 3;
objCol3.toolTip = "Basis Code";
objCol3.headerText = "Basis<br>Code";
objCol3.itemAlign = "left"

var objCol4 = new DGColumn();
objCol4.columnType = "CUSTOM";
objCol4.width = "8%";
objCol4.arrayIndex = 4;
objCol4.toolTip = "Valid From";
objCol4.headerText = "Valid From<font class='mandatory'>*</font>";
objCol4.ID = 'txtValidFrom';
objCol4.htmlTag = "<input type='text' id='txtValidFrom' name='txtValidFrom' :CUSTOM: size='8' maxlength='10' style='text-align:right;'  onBlur='parent.checkDate(this);parent.validateFromDate(this,:ROW:)' invalidText='true' >";
objCol4.itemAlign = "center"

var objCol5 = new DGColumn();
objCol5.columnType = "CUSTOM";
objCol5.width = "8%";
objCol5.arrayIndex = 5;
objCol5.toolTip = "Valid To";
objCol5.headerText = "Valid To<font class='mandatory'>*</font>";
objCol5.ID = 'txtValidTo';
objCol5.htmlTag = "<input type='text' id='txtValidTo' name='txtValidTo' :CUSTOM: size='8' maxlength='10' style='text-align:right;'  onBlur='parent.checkDate(this);parent.validateToDate(this,:ROW:)' invalidText='true'  >";
objCol5.itemAlign = "center"
objCol5.sort = "true";

var objCol6 = new DGColumn();
objCol6.columnType = "CUSTOM";
objCol6.width = "6%";
objCol6.arrayIndex = 6;
objCol6.toolTip = "Fare ";// + baseCurrency;
//objCol6.headerText = "Fare ";// + baseCurrency
//+ "<font class='mandatory'>*</font>";
objCol6.headerText = "Fare " + "<font class='mandatory'>*</font>";
objCol6.ID = 'txtAED';
objCol6.htmlTag = "<input type='text' id='txtAED' name='txtAED' :CUSTOM: size='5' maxlength='13' style='text-align:right;' onKeyPress='parent.KPValidateDecimel(this,10,2)' onKeyUp='parent.KPValidateDecimel(this,10,2)' onBlur='parent.validateAED(:ROW:,this)' class='rightText'  >";
objCol6.itemAlign = "center"
objCol6.sort = "true";

var objCol7 = new DGColumn();
objCol7.columnType = "label";
objCol7.width = "4%";
objCol7.arrayIndex = 7;
objCol7.toolTip = "Booking Class";
objCol7.headerText = "BC";
objCol7.itemAlign = "left"
objCol7.sort = "true";

var objCol8 = new DGColumn();
objCol8.columnType = "label";
objCol8.width = "3%"; // FIXME aquired 3%
objCol8.arrayIndex = 8;
objCol8.toolTip = "Class Of Service";
objCol8.headerText = "COS";
objCol8.itemAlign = "center"

var objCol9 = new DGColumn();
objCol9.columnType = "label";
objCol9.width = "2%"; // FIXME aquired 4%
objCol9.arrayIndex = 9;
objCol9.toolTip = "One Way/Return";
objCol9.headerText = "OW/<br>RT";
objCol9.itemAlign = "left"

var objCol10 = new DGColumn();
objCol10.columnType = "label";
objCol10.width = "2%"; // FIXME aquired 2%
objCol10.arrayIndex = 10;
objCol10.toolTip = "Fixed Allocation";
objCol10.headerText = "Fix<br>All";
objCol10.itemAlign = "center"

var objCol14 = new DGColumn();
objCol14.columnType = "label";
objCol14.width = "3%";
objCol14.arrayIndex = 14;
objCol14.toolTip = "Expired Fare";
objCol14.headerText = "Exp";
objCol14.itemAlign = "center"

var objCol11 = new DGColumn();
objCol11.columnType = "label";
objCol11.width = "10%";
objCol11.arrayIndex = 11;
objCol11.toolTip = "OND";
objCol11.headerText = "OND";
objCol11.itemAlign = "left"

var objCol12 = new DGColumn();
objCol12.columnType = "label";
objCol12.width = "8%"; // FIXME aquired 2%
objCol12.arrayIndex = 12;
objCol12.toolTip = "Visible Channel";
objCol12.headerText = "Visible<br> Channels";
objCol12.itemAlign = "left"

var objCol13 = new DGColumn();
objCol13.columnType = "label";
objCol13.width = "3%";// FIXME aquired 3%
objCol13.arrayIndex = 13;
objCol13.toolTip = "Status";
objCol13.headerText = "Stat";
objCol13.itemAlign = "left"


var objCol15 = new DGColumn();
objCol15.columnType = "CHECKBOX";
objCol15.width = "4%";
objCol15.arrayIndex = 15;
objCol15.linkOnClick = "checkStatus";
objCol15.toolTip = "Select";
objCol15.headerText = "Sel";
objCol15.itemAlign = "center";

var objCol16 = new DGColumn();
objCol16.columnType = "label";
objCol16.width = "3%"; // FIXME aquired 1%
objCol16.arrayIndex = 21;
objCol16.toolTip = "Pax Category";
objCol16.headerText = "Pax<br>Cat";
objCol16.itemAlign = "center";

var objCol17 = new DGColumn();
objCol17.columnType = "CUSTOM";
objCol17.width = "6%";
objCol17.arrayIndex = 24;
objCol17.toolTip = "Child Fare V=Value/P=%";
objCol17.headerText = "CH Fare<font class='mandatory'>*</font>";
objCol17.ID = 'txtChildFare';
objCol17.htmlTag = "<input type='text' id='txtChildFare' name='txtChildFare' :CUSTOM: size='5' maxlength='13' style='text-align:right;'  onKeyPress='parent.KPValidateDecimelDup(this,10,2)' onKeyUp='parent.KPValidateDecimelDup(this,10,2)' onBlur='parent.validateChildFare(:ROW:,this)' class='rightText'  >";
objCol17.itemAlign = "center"

var objCol18 = new DGColumn();
objCol18.columnType = "CUSTOM";
objCol18.width = "6%";
objCol18.arrayIndex = 26;
objCol18.toolTip = "Infant Fare V=Value/P=%";
objCol18.headerText = "IN Fare<font class='mandatory'>*</font>";
objCol18.ID = 'txtInfantFare';
objCol18.htmlTag = "<input type='text' id='txtInfantFare' name='txtInfantFare' :CUSTOM: size='5' maxlength='13' style='text-align:right;'  onKeyPress='parent.KPValidateDecimelDup(this,10,2)' onKeyUp='parent.KPValidateDecimelDup(this,10,2)' onBlur='parent.validateInfantFare(:ROW:,this)' class='rightText'  >";
objCol18.itemAlign = "center"

var objCol19 = new DGColumn();
objCol19.columnType = "label";
objCol19.width = "1%";
objCol19.arrayIndex = 25;
objCol19.toolTip = "Expired Fare";
objCol19.itemAlign = "center"

var objCol20 = new DGColumn();
objCol20.columnType = "label";
objCol20.width = "1%";
objCol20.arrayIndex = 27;
objCol20.toolTip = "Expired Fare";
objCol20.itemAlign = "center"

var objCol21 = new DGColumn();
objCol21.columnType = "CUSTOM";
objCol21.width = "8%";
objCol21.arrayIndex = 28;
objCol21.toolTip = "Sales Start";
objCol21.headerText = "Sales Start<font class='mandatory'>*</font>";
objCol21.ID = 'txtSalesFrom';
objCol21.htmlTag = "<input type='text' id='txtSalesFrom' name='txtSalesFrom' :CUSTOM: size='8' maxlength='10' style='text-align:right;'  onBlur='parent.checkDate(this);parent.validateFromDate(this,:ROW:)' invalidText='true' >";
objCol21.itemAlign = "center"

var objCol22 = new DGColumn();
objCol22.columnType = "CUSTOM";
objCol22.width = "8%";
objCol22.arrayIndex = 29;
objCol22.toolTip = "Sales End";
objCol22.headerText = "Sales End<font class='mandatory'>*</font>";
objCol22.ID = 'txtSalesTo';
objCol22.htmlTag = "<input type='text' id='txtSalesTo' name='txtSalesTo' :CUSTOM: size='8' maxlength='10' style='text-align:right;'  onBlur='parent.checkDate(this);parent.validateToDate(this,:ROW:)' invalidText='true'  >";
objCol22.itemAlign = "center"

var objCo123 = new DGColumn();
objCo123.columnType = "label";
objCo123.width = "6%";
objCo123.arrayIndex = 34;
objCo123.toolTip = "Local Currency Code";
objCo123.headerText = "Curr Code";
objCo123.itemAlign = "left"
	
var objCol24 = new DGColumn();
objCol24.columnType = "CUSTOM";
objCol24.width = "8%";
objCol24.arrayIndex = 35;
objCol24.toolTip = "Return Valid From";
objCol24.headerText = "Return Valid From<font class='mandatory'>*</font>";
objCol24.ID = 'txtRtValidFrom';
objCol24.htmlTag = "<input type='text' id='txtRtValidFrom' name='txtRtValidFrom' :CUSTOM: size='8' maxlength='10' style='text-align:right;'  onBlur='parent.checkDate(this);parent.validateFromDate(this,:ROW:)' invalidText='true' >";
objCol24.itemAlign = "center"

var objCol25 = new DGColumn();
objCol25.columnType = "CUSTOM";
objCol25.width = "8%";
objCol25.arrayIndex = 36;
objCol25.toolTip = "Return Valid To";
objCol25.headerText = "Return Valid To<font class='mandatory'>*</font>";
objCol25.ID = 'txtRtValidTo';
objCol25.htmlTag = "<input type='text' id='txtRtValidTo' name='txtRtValidTo' :CUSTOM: size='8' maxlength='10' style='text-align:right;'  onBlur='parent.checkDate(this);parent.validateToDate(this,:ROW:)' invalidText='true'  >";
objCol25.itemAlign = "center"
objCol25.sort = "true";	

// ---------------- Grid Fares
var objDG = new DataGrid("spnSearch");
objDG.addColumn(objCol1);
objDG.addColumn(objCol2);
objDG.addColumn(objCol3);
if (document.forms[0].radDt[2].checked) {
	objDG.addColumn(objCol24);
	objDG.addColumn(objCol25);
}
else if (document.forms[0].radDt[1].checked) {
	objDG.addColumn(objCol21);
	objDG.addColumn(objCol22);
} else {
	objDG.addColumn(objCol4);
	objDG.addColumn(objCol5);
}
objDG.addColumn(objCol6);
if (blnChildVisible) {
	objDG.addColumn(objCol19);
	objDG.addColumn(objCol17);
	objDG.addColumn(objCol20);
	objDG.addColumn(objCol18);
}

objDG.addColumn(objCol7);
objDG.addColumn(objCol8);
objDG.addColumn(objCol9);
if (showFareDefByDepCountryCurrency == "N") {
	objDG.addColumn(objCol10);
}
objDG.addColumn(objCol11);
objDG.addColumn(objCol12);
objDG.addColumn(objCol13);
if (showFareDefByDepCountryCurrency == "N") {
	objDG.addColumn(objCol14);
}
objDG.addColumn(objCol16);

if (showFareDefByDepCountryCurrency == "Y") {
	objDG.addColumn(objCo123);
}
objDG.addColumn(objCol15);

objDG.width = "100%";
objDG.height = "137px";
objDG.seqNoWidth = "3%";
objDG.headerBold = false;
objDG.arrGridData = arrFareData;
objDG.seqNo = true;
objDG.seqStartNo = getTabValues(screenId, delimiter, "intLastRec");
objDG.pgnumRecTotal = totalRes; // remove as per return record size
objDG.paging = true;
objDG.pgnumRecPage = 10;
objDG.pgonClick = "gridNavigations";
var noDataFound = objDG.noDataFoundMsg;
objDG.noDataFoundMsg = "";
objDG.setColors = "setONDFareDGRowColor";

function checkDate(control) {
	if (control.value != "") {
		setPageEdited(true);
		var val = dateChk(control.value);
		control.value = val;
	}
}

function checkStatus(no) {
	var allunchecked = false;
	var arrChkData = objDG.getSelectedColumn(editCheckBoxColumnNo);
	for (t = 0; t < arrChkData.length; t++) {
		if (arrChkData[t][1] == true) {
			allunchecked = true;			 
			break;
		}
	}
	
	if (!allunchecked) {
		objDG.setDisable("", "3", true);
		objDG.setDisable("", "4", true);
		objDG.setDisable("", "5", true);
		objDG.setDisable("", "7", true);
		objDG.setDisable("", "9", true);
		Disable("btnEdit", true);
		Disable("btnEditRule", true);
		Disable("txtExtendFrom", true);
		Disable("txtExtendTo", true);
		Disable("btnExtend", true);
		setField("txtExtendTo", "");
		setField("txtExtendFrom", "");
		Disable("btnSave", true);
		Disable("btnOverWriteRule", true);
		Disable("btnReset", true);
	} else {
		Disable("btnEdit", false);
		Disable("btnEditRule", false);
		//if (blnValid) {
			Disable("txtExtendFrom", false);
			Disable("txtExtendTo", false);
			Disable("btnExtend", false);
		/*} else {
			Disable("txtExtendFrom", false);
			Disable("txtExtendTo", false);
			Disable("btnExtend", false);
		}*///21122011 CJ--> Remove above if condition as I cant understand if and else doing the same. If somebody remembers for 
				//which purpose this has done or if there is any other reason beyond the concept of if/else pls let us know		
	}
}

function KPValidateDecimel(objCon, s, f) {
	setPageEdited(true);
	var strText = objCon.value;
	var length = strText.length;
	var blnVal = isLikeDecimal(strText);
	var blnVal2 = currencyValidate(strText, s, f);
		
	if (!blnVal) {
		customValidate(objCon, /[a-zA-Z`!@#$%\s&*?\[\]{}()|\\\/=:,;_^~]/g);
	} else if (!blnVal2) {
		customValidate(objCon, /[a-zA-Z`!@#$%\s&*?\[\]{}()|\\\/=:,;_^~]/g);
		objCon.focus();
	}
	if (!blnVal2) {
		if (strText.indexOf(".") != -1) {
			wholeNumber = strText.substr(0, strText.indexOf("."));
			if (wholeNumber.length > 13) {
				objCon.value= strText.substr(0, wholeNumber.length - 1);
			} else {
				objCon.value= strText.substr(0, length - 1);
			}
		} else {
			objCon.value= strText.substr(0, length - 1);
		}
	}
}

function KPValidateDecimelDup(objCon, s, f) {
	setPageEdited(true);
	var strText = objCon.value;
	var length = strText.length;
	var blnVal = isLikeDecimal(strText);
	var blnVal2 = false;
	var type = "";
			
	if (!blnVal) {
		customValidate(objCon, /[a-zA-Z`!@#$%\s&*?\[\]{}()|\\\/=:,;_^~]/g);
	} else if (!blnVal2) {
		customValidate(objCon, /[a-zA-Z`!@#$%\s&*?\[\]{}()|\\\/=:,;_^~]/g);
		objCon.focus();
	}
	if(objCon.id == "txtChildFare"){
		type = chdFareType;
	}else{
		type = infFareType;
	}
	if(type == "P"){
		blnVal2 = currencyValidate(strText, 3, 2);
		if (!blnVal2) {
			if (strText.indexOf(".") != -1) {
				wholeNumber = strText.substr(0, strText.indexOf("."));
				if (wholeNumber.length > 6) {
					objCon.value= strText.substr(0, wholeNumber.length - 1);
				} else {
					objCon.value= strText.substr(0, length - 1);
				}
			} else {
				objCon.value= strText.substr(0, length - 1);
			}			
		}
		if(strText > 100){
			objCon.value= strText.substr(0, length - 1);
		}
	}else{
		blnVal2 = currencyValidate(strText, 10, 2);
		if (!blnVal2) {
			if (strText.indexOf(".") != -1) {
				wholeNumber = strText.substr(0, strText.indexOf("."));
				if (wholeNumber.length > 13) {
					objCon.value= strText.substr(0, wholeNumber.length - 1);
				} else {
					objCon.value= strText.substr(0, length - 1);
				}
			} else {
				objCon.value= strText.substr(0, length - 1);
			}
		}
	}
}

function validateToDate(control, rowNo) {
	if (validateTo == true) {
		setPageEdited(true);
		var strCC = control.value;
		var from = objDG.getCellValue(rowNo, 3);
		var validate = CheckDates(from, strCC);

		if (strCC == "") {
			showERRMessage(todateRqrd);
			control.focus();
		} else if (strCC != "" && !validate) {
			showERRMessage(datesInvalid);
			control.focus();
		} else {
			arrFareData[rowNo][19] = "Changed";
		}
	}
}

var validateTo = true;
var validateFrom = true;

function validateFromDate(control, rowNo) {

	var strCC = control.value;
	var validate = testDate(strCC);
	var to = objDG.getCellValue(rowNo, 4);
	var validateDates = CheckDates(strCC, to);
	setPageEdited(true);
	hideERRMessage();
	if (strCC == "") {
		showERRMessage(fromdateRqrd);
		control.focus();
		validateTo = false;
	} else if (strCC != "" && validate) {
		showERRMessage(FromDateExceedsCurrentDate);
		control.focus();
		validateTo = false;
	} else if (strCC != "" && !validateDates) {
		showERRMessage(datesInvalid);
		control.focus();
		validateTo = false;
	} else {
		arrFareData[rowNo][19] = "Changed";
		validateTo = true;
	}
}

function validateAED(rowNo, control) {
	if (validateFrom == true) {
		setPageEdited(true);
		var strCC = control.value;
		if (control.value == "") {
			showERRMessage(AEDRqrd);
			control.focus();
		} else {
			arrFareData[rowNo][19] = "Changed";
		}
	}

}
function validateChildFare(rowNo, control) {
	if (validateFrom == true) {
		setPageEdited(true);
		var strCC = control.value;
		if (control.value == "") {
			showERRMessage("Child fare cannot be empty");
			control.focus();
		} else {
			arrFareData[rowNo][19] = "Changed";
			if (arrFareData[rowNo][25]=="P" && control.value > 100) {
				showERRMessage("Child fare percentage cannot be lager than 100");
				control.focus();
			}
		}
	}

}
function validateInfantFare(rowNo, control) {
	if (validateFrom == true) {
		setPageEdited(true);
		var strCC = control.value;
		if (control.value == "") {
			showERRMessage("Infant fare cannot be empty");
			control.focus();
		} else {
			arrFareData[rowNo][19] = "Changed";
			if (arrFareData[rowNo][27]=="P" && control.value > 100) {
				showERRMessage("Infant fare percentage cannot be lager than 100");
				control.focus();
			}
		}
	}

}
function closeClick() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setPageEdited(false);
		top[1].objTMenu.tabRemove(screenId);
	}
}
// --------------------------------------- Grid OND

var objChargeCol1 = new DGColumn();
objChargeCol1.columnType = "label";
objChargeCol1.width = "18%";
objChargeCol1.arrayIndex = 1;
objChargeCol1.toolTip = "OND";
objChargeCol1.headerText = "OND";
objChargeCol1.itemAlign = "left"
objChargeCol1.sort = "true";

var objChargeCol2 = new DGColumn();
objChargeCol2.columnType = "label";
objChargeCol2.width = "7%";
objChargeCol2.arrayIndex = 2;
objChargeCol2.toolTip = "Group";
objChargeCol2.headerText = "Group";
objChargeCol2.itemAlign = "left"

var objChargeCol3 = new DGColumn();
objChargeCol3.columnType = "label";
objChargeCol3.width = "7%";
objChargeCol3.arrayIndex = 3;
objChargeCol3.toolTip = "Category";
objChargeCol3.headerText = "Category";
objChargeCol3.itemAlign = "left"

var objChargeCol4 = new DGColumn();
objChargeCol4.columnType = "label";
objChargeCol4.width = "8%";
objChargeCol4.arrayIndex = 4;
objChargeCol4.toolTip = "Charge Code";
objChargeCol4.headerText = "Charge<br>Code";
objChargeCol4.itemAlign = "left"

var objChargeCol5 = new DGColumn();
objChargeCol5.columnType = "label";
objChargeCol5.width = "14%";
objChargeCol5.arrayIndex = 5;
objChargeCol5.toolTip = "Charge Description";
objChargeCol5.headerText = "Charge Description";
objChargeCol5.itemAlign = "left"

var objChargeCol6 = new DGColumn();
objChargeCol6.columnType = "label";
objChargeCol6.width = "6%";
objChargeCol6.arrayIndex = 6;
objChargeCol6.toolTip = "Y=Refundable Charge";
objChargeCol6.headerText = "Refund<br> Y/N";
objChargeCol6.itemAlign = "center"

var objChargeCol7 = new DGColumn();
objChargeCol7.columnType = "label";
objChargeCol7.width = "8%";
objChargeCol7.arrayIndex = 7;
objChargeCol7.toolTip = "Y=Applicable to Infants";
objChargeCol7.headerText = "Apply<br>To";
objChargeCol7.itemAlign = "center"

var objChargeCol8 = new DGColumn();
objChargeCol8.columnType = "label";
objChargeCol8.width = "8%";
objChargeCol8.arrayIndex = 8;
objChargeCol8.toolTip = "Valid From";
objChargeCol8.headerText = "Valid From";
objChargeCol8.itemAlign = "center"

var objChargeCol9 = new DGColumn();
objChargeCol9.columnType = "label";
objChargeCol9.width = "8%";
objChargeCol9.arrayIndex = 9;
objChargeCol9.toolTip = "Valid To";
objChargeCol9.headerText = "Valid To";
objChargeCol9.itemAlign = "center"

var objChargeCol10 = new DGColumn();
objChargeCol10.columnType = "label";
objChargeCol10.width = "4%";
objChargeCol10.arrayIndex = 10;
objChargeCol10.toolTip = "P=Percentage V=Value";
objChargeCol10.headerText = "V/P Flag";
objChargeCol10.itemAlign = "center"

var objChargeCol11 = new DGColumn();
objChargeCol11.columnType = "label";
objChargeCol11.width = "9%";
objChargeCol11.arrayIndex = 11;
objChargeCol11.toolTip = "Percentage/Amount";
objChargeCol11.headerText = "Amount";
objChargeCol11.itemAlign = "right"

// ---------------- Grid
var objDG1 = new DataGrid("spnONDFare");
objDG1.addColumn(objChargeCol1);
objDG1.addColumn(objChargeCol2);
objDG1.addColumn(objChargeCol3);
objDG1.addColumn(objChargeCol4);
objDG1.addColumn(objChargeCol5);
objDG1.addColumn(objChargeCol6);
objDG1.addColumn(objChargeCol7);
objDG1.addColumn(objChargeCol8);
objDG1.addColumn(objChargeCol9);
objDG1.addColumn(objChargeCol10);
objDG1.addColumn(objChargeCol11);

objDG1.width = "99%";
objDG1.height = "79px";
objDG1.headerBold = false;
objDG1.seqStartNo = 1;
objDG1.arrGridData = arrFareChargeData;
objDG1.seqNo = true;
objDG1.pgnumRecTotal = totalChargeRecs; // remove as per return record size
objDG1.paging = false;
var noDataFound = objDG.noDataFoundMsg;
objDG1.noDataFoundMsg = "";
objDG1.displayGrid();

function RecMsgFunction(intRecNo, intErrMsg) {
	top[2].HidePageMessage();
	if (intErrMsg != "") {
		top[2].objMsg.MessageText = intErrMsg;
		top[2].objMsg.MessageType = "Error";
		top[2].ShowPageMessage();
	}
}

function setONDFareDGRowColor(strRowData) {	
	if (strRowData[42] == 'Y') {//master
		return '#FF6666';
	} else if ((strRowData[43] != null && strRowData[43] != "")  && (strRowData[44] != null && strRowData[44] != "")) {//LINK FARES
		return '#CC00FF';	
	} else {//NORMAL
		return 'black';
	}
	
}