function setPageEdited(isEdited) {
	top.pageEdited = isEdited;
}

function winOnLoad(strMsg, strMsgType) {
	setData(fareRules);
	disableInputControls("true");
}

function setData(fareRules) {
	var fareArr = fareRules;

	DivWrite("spnOrigin", fareArr[0]);
	DivWrite("spnDestination", fareArr[1]);

	var viaArr = fareArr[2];
	if (fareArr.length > 1) {
		if (viaArr[0] != null) {
			DivWrite("spnVia1", viaArr[0]);
		}
	}
	if (fareArr.length > 2) {
		if (viaArr[1] != null) {
			DivWrite("spnVia2", viaArr[1]);
		}
	}
	if (fareArr.length > 3) {
		if (viaArr[2] != null) {
			DivWrite("spnVia3", viaArr[2]);
		}
	}
	if (fareArr.length > 4) {
		if (viaArr[3] != null) {
			DivWrite("spnVia4", viaArr[3]);
		}
	}
	if (fareArr[3] != "undefined") {
		DivWrite("spnFrom", fareArr[3]);

	}
	if (fareArr[4] != "undefined") {
		DivWrite("spnTo", fareArr[4]);
	}

	DivWrite("spnAED", fareArr[5]);

	DivWrite("spnBc", fareArr[6]);
	DivWrite("spnFC", fareArr[7]);

	DivWrite("spnCOS", fareArr[8]);
	if (fareArr[9] == "Y") {
		DivWrite("spnFixed", "Fixed");
	} else {
		DivWrite("spnFixed", "Standard");
	}

	var arrDays = new Array("chkInbSun", "chkInbMon", "chkInbTues",
			"chkInbWed", "chkInbThu", "chkInbFri", "chkInbSat");
	var depDays = new Array("chkOutSun", "chkOutMon", "chkOutTues",
			"chkOutWed", "chkOutThu", "chkOutFri", "chkOutSat");
	var controlArr = new Array("txtAdvBookDays", "radFare", "txtMaxStayDays",
			"txtMinStayDays", "txtModification", "txtCancellation",
			"txtOutTmTo", "txtOutTmFrom", "txtInbTmTo", "txtInbTmFrom",
			"txtaRulesCmnts", "chkRefundable", "chkStatus", "selFareVisibility","selDefineModType","selDefineCanType",
			"txtMin_mod", "txtMax_mod","txtMin_canc", "txtMax_canc");

	if (fareArr[10] != "undefined" && fareArr[0] != "null") {
		setField(controlArr[0], fareArr[10]);
		Disable("chkAdvance", "");
		setField("chkAdvance", true);
		Disable("chkAdvance", "true");

	}

	if (fareArr[11] == "Y") {
		setField(controlArr[1], "Return");
	} else {
		setField(controlArr[1], "OneWay");
	}

	if (fareArr[12] != "null") {
		setField(controlArr[2], fareArr[12]);
	}
	if (fareArr[13] != "null") {
		setField(controlArr[3], fareArr[13]);
	}
	if (fareArr[14] != "null") {
		setField(controlArr[4], fareArr[14]);
	}
	if (fareArr[15] != "null") {
		setField(controlArr[5], fareArr[15]);
	}
	if (fareArr[16] != "null") {
		setField(controlArr[6], fareArr[16]);
	}
	if (fareArr[17] != "null") {
		setField(controlArr[7], fareArr[17]);
	}
	if (fareArr[18] != "null") {
		setField(controlArr[8], fareArr[18]);
	}
	if (fareArr[19] != "null") {
		setField(controlArr[9], fareArr[19]);
	}
	if (fareArr[20] != "null") {
		setField(controlArr[10], fareArr[20]);
	}
	if (fareArr[21] != "null" && fareArr[21] == "Y") {
		Disable("chkRefundable", "");
		setField("chkRefundable", true);
		Disable("chkRefundable", "true");

	}
	if (fareArr[22] != "null" && fareArr[22] == "Active") {
		Disable("chkStatus", "");
		setField("chkStatus", true);
		Disable("chkStatus", "true");

	}

	if (fareArr[23] != "null") {
		var days = fareRules[23].split(":");
		for ( var f = 0; f < days.length; f++) {
			if (days[f] == 1) {
				Disable(depDays[f], "");
				setField(depDays[f], true);
				Disable(depDays[f], "true");
			} else {
				Disable(depDays[f], "");
				setField(depDays[f], false);
				Disable(depDays[f], "true");
			}
		}
	}

	if (fareArr[24] != "null") {
		var days = fareRules[24].split(":");
		for ( var f = 0; f < days.length; f++) {
			if (days[f] == 1) {
				Disable(arrDays[f], "");
				setField(arrDays[f], true);
				Disable(arrDays[f], "true");
			} else {
				Disable(arrDays[f], "");
				setField(arrDays[f], false);
				Disable(arrDays[f], "true");
			}
		}
	}

	if (fareRules[25] != "null" || fareRules[25] != "undefined") {
		var visibility = fareRules[25].split(":");
		var vis = "";
		for ( var f = 0; f < visibility.length; f++) {
			vis += visibility[f] + ",";

		}
		if (vis.indexOf(",") != -1) {
			setField("selFareVisibility", vis
					.substring(0, vis.lastIndexOf(",")));

		} else {
			setField("selFareVisibility", vis);

		}

	}

	if (fareRules[26] != null || fareRules[26] != "undefined") {
		displyAgents(fareRules[26]);

	} else {
		var agentStr = "<select id='selAgents' name='selAgents' style='height:60px;width:120px;' multiple='true'>";
		agentStr += " <optgroup label='Agents'>";
		agentStr += " <optgroup label='GSA'>";
		agentStr += "</select>";
		DivWrite("spnAgents", agentStr);
	}
	setField("selDefineModType", fareRules[27]);
	setField("selDefineCanType", fareRules[28]);
	setField("txtMin_mod", fareRules[29]);
	setField("txtMax_mod", fareRules[30]);
	setField("txtMin_canc", fareRules[31]);
	setField("txtMax_canc", fareRules[32]);

}

function displyAgents(arr) {
	var agentArr = arr;
	var agentStr = "<select id='selAgents' name='selAgents' style='height:60px;width:200px;' multiple='true'>";
	agentStr += " <optgroup label='Agents'>";

	for ( var t = 0; t < agentArr[0].length; t++) {

		var agent = new String(agentArr[0][t]).split(",");
		agentStr += "<option value='" + agent[1] + "'>" + agent[0]
				+ "</option>"
	}

	agentStr += " <optgroup label='GSA'>";

	for ( var t = 0; t < agentArr[1].length; t++) {
		var agent = new String(agentArr[1][t]).split(",");
		agentStr += "<option value='" + agent[1] + "'>" + agent[0]
				+ "</option>"
	}
	agentStr += "</select>";
	DivWrite("spnAgents", agentStr);
}

function disableInputControls(disabled) {
	Disable("selFareVisibility", disabled);
	Disable("chkRefundable", disabled);
	Disable("chkAdvance", disabled);
	Disable("txtAdvBookDays", disabled);
	Disable("chkChrgRestrictions", disabled);
	Disable("selFareVisibility", disabled);
	Disable("radFare", disabled);
	Disable("txtMaxStayDays", disabled);
	Disable("txtMinStayDays", disabled);
	Disable("txtModification", disabled);
	Disable("txtCancellation", disabled);
	Disable("selDefineModType", disabled);
	Disable("selDefineCanType", disabled);	
	Disable("txtMin_mod", disabled);
	Disable("txtMax_mod", disabled);		
	Disable("txtMin_canc", disabled);
	Disable("txtMax_canc", disabled);	
	
	Disable("txtOutTmFrom", disabled);
	Disable("txtOutTmTo", disabled);
	Disable("txtInbTmFrom", disabled);
	Disable("txtInbTmTo", disabled);
	Disable("txtaRulesCmnts", disabled);
	var arrDays = new Array("chkInbSun", "chkInbMon", "chkInbTues",
			"chkInbWed", "chkInbThu", "chkInbFri", "chkInbSat");
	var depDays = new Array("chkOutSun", "chkOutMon", "chkOutTues",
			"chkOutWed", "chkOutThu", "chkOutFri", "chkOutSat");

	for ( var t = 0; t < arrDays.length; t++) {
		Disable(arrDays[t], disabled);
		Disable(depDays[t], disabled);

	}
	Disable("btnReset", disabled);

}