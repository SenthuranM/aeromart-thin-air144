var agentCodes = "";
var flexiCode = -1;
var rowNo;
var status;
var rowNum = 0;
var data = "";
var updateClicked = false;
var screenId = "SC_ADMN_008";
var delimiter = "@";
var arrDisableControls = new Array('chkADRef', 'chkADApp', 'chkADMod','chkADCNX', 'chkCHRef', 'chkCHApp', 'chkCHMod', 'chkCHCNX', 'chkINRef',
		'chkINApp', 'chkINMod', 'chkINCNX', 'txtADNoShoCharge', 'txtCHNoShoCharge', 'txtINNoShoCharge','txtADNoShoChargeInLocal','txtCHNoShoChargeInLocal','txtINNoShoChargeInLocal');
var arrDays = new Array("chkInbSun", "chkInbMon", "chkInbTues", "chkInbWed", "chkInbThu", "chkInbFri", "chkInbSat");
var depDays = new Array("chkOutSun", "chkOutMon", "chkOutTues", "chkOutWed", "chkOutThu", "chkOutFri", "chkOutSat");
var objFC = new listBox();
var selModeTyps = {
	v: "V",pf:"PF", pfs:"PFS"
};
var defaultCanAndModValueType = selModeTyps.v;

function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}

function closeClick() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setPageEdited(false);
		top[1].objTMenu.tabRemove(screenId);
	}
}

function kpAlphaNumeric(control) {
	var strCC = getText(control);
	var strLen = strCC.length;
	var blnVal = isAlphaNumeric(strCC);
	if (!blnVal) {
		setField(control, strCC.substr(0, strLen - 1));
		getFieldByID(control).focus();
	}
}

function disableReturn(cond){
	Disable("txtMaxStayMonths", cond);
	Disable("txtMinStayMonths", cond);
	Disable("txtMaxStayDays", cond);
	Disable("txtMinStayDays", cond);
	Disable("txtMinStayHours", cond);
	Disable("txtMaxStayHours", cond);
	Disable("txtMaxStayMins", cond);
	Disable("txtMinStayMins", cond);
	Disable("txtInbTmFrom", cond);
	Disable("txtInbTmTo", cond);	
}

function clearReturn() {
	setField("txtMaxStayMonths", "");
	setField("txtMinStayMonths", "");
	setField("txtMaxStayDays", "");
	setField("txtMinStayDays", "");
	setField("txtMinStayHours", "");
	setField("txtMaxStayHours", "");
	setField("txtMaxStayMins", "");
	setField("txtMinStayMins", "");
	setField("txtInbTmTo", "");
	setField("txtInbTmFrom", "");
	setField("txtConfirmMonths", "");
	setField("txtConfirmDays", "");
	setField("txtConfirmHours", "");
	setField("txtConfirmMins", "");
	setField("txtModBufDays", "");
	setField("txtModBufHours", "");
	setField("txtModBufMins", "");
	setField("txtModBufDaysDom", "");
	setField("txtModBufHoursDom", "");
	setField("txtModBufMinsDom", "");
}

function DisableReturnDays() {
	
	var fare = getText("radFare");	
	Disable("chkOutAll", false);
	setField("chkOutAll", true);
	for ( var t = 0; t < depDays.length; t++) {
		Disable(depDays[t], false);
		setChkControls(depDays[t], true);
	}

	if (fare == "Return") {
		Disable("chkInAll", false);
		setChkControls("chkInAll", true);
		disableReturn(false);
		Disable("chkOpenRT", false);
		Disable("chkPrintExp", false);
		Disable("chkHRT", false);
		setField("txtInbTmTo", "23:59");
		setField("txtInbTmFrom", "00:00");

		if (((getText("txtMaxStayMonths") != "") && (getText("txtMaxStayMonths") != 0))
				||((getText("txtMaxStayDays") != "") && (getText("txtMaxStayDays") != 0))
				|| ((getText("txtMaxStayHours") != "") && (getText("txtMaxStayHours") != 0))
				|| ((getText("txtMaxStayMins") != "") && (getText("txtMaxStayMins") != 0))) {
			Disable("chkPrintExp", false);
		}
		for(var t=0;t<arrDays.length;t++){
			Disable(arrDays[t],false);
			setChkControls(arrDays[t],true);
		}
		if(getValue("selFareCat") == "R"){
			Disable("chkOpenRT", true);
			Disable("chkHRT", true);
		}		
			
		createReturnFlexiCodes();
		
	} else {
		setChkControls("chkInAll", false);
		Disable("chkInAll", true);
		clearReturn();
		disableReturn(true);
		setChkControls("chkOpenRT", false); // umesh@@
		Disable("chkOpenRT", true);        //umesh@@
		Disable("chkHRT", true);  
		Disable("chkPrintExp", true);
		if (getFieldByID("chkPrintExp").checked) {
			getFieldByID("chkPrintExp").checked = false;
		}
		for ( var t = 0; t < arrDays.length; t++) {
			setChkControls(arrDays[t], false);
			Disable(arrDays[t], true);
		}
		
		createOnewayFlexiCodes();
	}
}

function createReturnFlexiCodes(){
	var arrNewFlexi = new Array();
	arrNewFlexi[0] = new Array();
	arrNewFlexi[0][0] = "-1";
	arrNewFlexi[0][1] = "";
	var count = 1;
	for(var i = 1; i < arrFlexiJourney.length; i++){
		if(arrFlexiJourney[i][2] == "RT" && arrFlexiJourney[i][3] == "ACT"){		
			arrNewFlexi[count] = new Array();
			arrNewFlexi[count][0] = arrFlexiJourney[i][0];
			arrNewFlexi[count][1] = arrFlexiJourney[i][1];
			count++;
		}
	}
	
	objFC.dataArray = arrNewFlexi; 
	objFC.textIndex = "1";	
	objFC.fillListBox();
	
}

function createOnewayFlexiCodes(){
	var arrNewFlexi = new Array();
	arrNewFlexi[0] = new Array();
	arrNewFlexi[0][0] = "-1";
	arrNewFlexi[0][1] = "";
	var count = 1;
	for(var i = 1; i < arrFlexiJourney.length; i++){
		if(arrFlexiJourney[i][2] == "OW" && arrFlexiJourney[i][3] == "ACT"){		
			arrNewFlexi[count] = new Array();
			arrNewFlexi[count][0] = arrFlexiJourney[i][0];
			arrNewFlexi[count][1] = arrFlexiJourney[i][1];
			count++;
		}
	}
	
	objFC.dataArray = arrNewFlexi; 
	objFC.textIndex = "1";	
	objFC.fillListBox();
}

function DisableRestrictions() {
	var objRad2 = getFieldByID("radSelCurrFareRule");	
	var checked = getText("chkChrgRestrictions");
	setField("txtMin_mod", "");
	setField("txtMax_mod", "");	
	setField("txtMin_canc", "");
	setField("txtMax_canc", "");
	if (checked) {
		disableCnxModTxt(false);
		setField("txtModification", modChg);
		setField("txtCancellation", caclChg);
		setField("txtModificationInLocal", modChg);		
		setField("txtCancellationInLocal", caclChg);
		
		setField("selDefineModType", defaultCanAndModValueType);
		setField("selDefineCanType", defaultCanAndModValueType);
		
		if (isSelectedCurrencyChecked()){
			if(getValue("selDefineModType") != selModeTyps.v){	
				setField("txtModificationInLocal","0.00");
				getFieldByID("txtModification").readOnly = false;
				getFieldByID("txtModificationInLocal").readOnly = true;
			}else{
				setField("txtModification","0.00");	
				getFieldByID("txtModification").readOnly = true;
				getFieldByID("txtModificationInLocal").readOnly = false;	
			}	
			if(getValue("selDefineCanType") != selModeTyps.v){
				setField("txtCancellationInLocal","0.00");
				getFieldByID("txtCancellation").readOnly = false;
				getFieldByID("txtCancellationInLocal").readOnly = true;
			}else{
				setField("txtCancellation","0.00");	
				getFieldByID("txtCancellation").readOnly = true;
				getFieldByID("txtCancellationInLocal").readOnly = false;	
			}	
		}else{
			if(isSelectedCurrencyOptionAvailable()){
				setField("txtCancellationInLocal","0.00");
				getFieldByID("txtCancellationInLocal").readOnly = true;
				setField("txtModificationInLocal","0.00");
				getFieldByID("txtModificationInLocal").readOnly = true;
				
			}
			getFieldByID("txtModification").readOnly = false;
			getFieldByID("txtCancellation").readOnly = false;

		}		
	} else {
		setField("txtModification", "");
		setField("txtCancellation", "");
		setField("txtModificationInLocal", "");
		setField("txtCancellationInLocal", "");
		
		setField("selDefineModType", "");
		setField("selDefineCanType", "");	
		
		disableCnxModTxt(true);
	}
}

function controlGrid(control) {
	disableAdultControls(control);
	Disable("chkADApp", control);
	Disable("txtADNoShoCharge", control);
	Disable("txtADNoShoChargeInLocal", control);	

	disableChildControls(control);
	Disable("chkCHApp", control);
	Disable("txtCHNoShoCharge", control);
	Disable("txtCHNoShoChargeInLocal", control);

	disableInfantControl(control);
	Disable("txtINNoShoCharge", control);
	Disable("txtINNoShoChargeInLocal", control);
}

function DisableAdnvanceBKDays() {
	var checked = getText("chkAdvance");

	if (checked == "ON") {
		Disable("txtAdvBookDays", false);
		Disable("txtAdvBookHours", false);
		Disable("txtAdvBookMins", false);
	} else {
		setField("txtAdvBookDays", "");
		Disable("txtAdvBookDays", true);
		setField("txtAdvBookHours", "");
		Disable("txtAdvBookHours", true);
		setField("txtAdvBookMins", "");
		Disable("txtAdvBookMins", true);
	}
}

function setTabs() {
	top[1].objTMenu.tabSetValue(screenId, getText("selFareRuleCode") + "~" + getText("hdnRecNo") + "~" + getValue("selStatus"));
}

// To Handle Paging
function gridNavigations(intRecNo, intErrMsg) {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setField("hdnMode", "PAGING");
		setField("hdnRecNo", intRecNo);
		setTabs();
		if (intErrMsg != "") {
			top[2].objMsg.MessageText = "<li> " + intErrMsg;
			top[2].objMsg.MessageType = "Error";
			top[2].ShowPageMessage();
		} else {
			document.forms["frmFareClasses"].action = "showFare.action";
			document.forms["frmFareClasses"].submit();
			objDG.seqStartNo = intRecNo;
		}
		setPageEdited(false);
	}
}

function setFields(strRowNo) {
	rowNo = strRowNo;

	strRowData = objDG.getRowValue(strRowNo);
	setField("txtFc", strRowData[0]);
	setField("txtDesc", strRowData[1]);
	setField("txtFBC", strRowData[2]);
	setField("hdnRuleCode", strRowData[0]);
	setField("selPaxCat", arrData[strRowNo][21]);
	setField("selFareCat", arrData[strRowNo][22]);	

	if (arrData[strRowNo][6] == 'Y') {
		getFieldByID("chkChrgRestrictions").checked = true;
	}
	if (arrData[strRowNo][7] != "") {
		var tmpArray = arrData[strRowNo][7].split(",");
		setField("txtModification", tmpArray[0]);
		setField("txtCancellation", tmpArray[1]);
		setField("selDefineModType", tmpArray[2]);
		setField("selDefineCanType", tmpArray[3]);
		setField("txtMin_mod", tmpArray[4]);
		setField("txtMax_mod", tmpArray[5]);
		setField("txtMin_canc", tmpArray[6]);
		setField("txtMax_canc", tmpArray[7]);
		setField("txtModificationInLocal", tmpArray[8]);
		setField("txtCancellationInLocal", tmpArray[9]);
	}
//	else{
//		setField("selDefineModType", "");
//		setField("selDefineCanType","");
//		setField("txtMin_mod", "");
//		setField("txtMax_mod", "");
//		setField("txtMin_canc", "");
//		setField("txtMax_canc", "");
//	}
	var adArr = arrData[strRowNo][23]["AD"];
	var chArr = arrData[strRowNo][23]["CH"];
	var inArr = arrData[strRowNo][23]["IN"];

	setField("hdnVersionAD", arrData[strRowNo][23]["AD"][7]);
	setField("hdnFRPAXIDAD", arrData[strRowNo][23]["AD"][2]);

	setField("hdnVersionCH", arrData[strRowNo][23]["CH"][7]);
	setField("hdnFRPAXIDCH", arrData[strRowNo][23]["CH"][2]);

	setField("hdnVersionIN", arrData[strRowNo][23]["IN"][7]);
	setField("hdnFRPAXIDIN", arrData[strRowNo][23]["IN"][2]);

	if (adArr == null || adArr == "" || adArr.length <= 0) {
		adArr = new Array('', 'AD', '', 'off', 'off', 'off', 'off', '');
	}
	if (chArr == null || chArr == "" || chArr.length <= 0) {
		chArr = new Array('', 'CH', '', 'off', 'off', 'off', 'off', '');
	}
	if (inArr == null || inArr == "" || inArr.length <= 0) {
		inArr = new Array('', 'IN', '', 'off', 'off', 'off', 'off', '');
	}

	var tempArr = new Array();
	tempArr[0] = new Array('AD', 'Adult', '', '', '', adArr[3], adArr[4], adArr[5], adArr[6], '', '','');
	tempArr[1] = new Array('CH', 'Child', '', '', '', chArr[3], chArr[4], chArr[5], chArr[6], '', '','');
	tempArr[2] = new Array('IN', 'Infant', '', '', '', inArr[3], inArr[4], inArr[5], inArr[6], '', '','');
	// app mod cnx ref

	/* pax fare conditions */	

	// IE
	setField("hdnPaxTypeAD", tempArr[0][0]);
	setField("hdnPaxTypeCH", tempArr[1][0]);
	setField("hdnPaxTypeIN", tempArr[2][0]);

	setChkControls("chkADRef", tempArr[0][8]);
	setChkControls("chkADApp", tempArr[0][5]);
	setChkControls("chkADMod", tempArr[0][6]);
	setChkControls("chkADCNX", tempArr[0][7]);
	setField("txtADNoShoCharge", arrData[strRowNo][23]["AD"][8]);
	setField("selADCGTYP", arrData[strRowNo][23]["AD"][9]);				//charge type
	setField("txtADNoShoBreakPoint", arrData[strRowNo][23]["AD"][10]);	//breakpoint
	setField("txtADNoShoBoundary", arrData[strRowNo][23]["AD"][11]);	//boundary
	setField("txtADNoShoChargeInLocal", arrData[strRowNo][23]["AD"][12]);	//charge in local

	setChkControls("chkCHRef", tempArr[1][8]);
	setChkControls("chkCHApp", tempArr[1][5]);
	setChkControls("chkCHMod", tempArr[1][6]);
	setChkControls("chkCHCNX", tempArr[1][7]);
	setField("txtCHNoShoCharge", arrData[strRowNo][23]["CH"][8]);
	setField("selCHCGTYP", arrData[strRowNo][23]["CH"][9]);				//charge type
	setField("txtCHNoShoBreakPoint", arrData[strRowNo][23]["CH"][10]);	//breakpoint
	setField("txtCHNoShoBoundary", arrData[strRowNo][23]["CH"][11]);	//boundary
	setField("txtCHNoShoChargeInLocal", arrData[strRowNo][23]["CH"][12]);	//charge in local

	setChkControls("chkINRef", tempArr[2][8]);
	setChkControls("chkINApp", tempArr[2][5]);
	setChkControls("chkINMod", tempArr[2][6]);
	setChkControls("chkINCNX", tempArr[2][7]);
	setField("txtINNoShoCharge", arrData[strRowNo][23]["IN"][8]);
	setField("selINCGTYP", arrData[strRowNo][23]["IN"][9]);				//charge type
	setField("txtINNoShoBreakPoint", arrData[strRowNo][23]["IN"][10]);	//breakpoint
	setField("txtINNoShoBoundary", arrData[strRowNo][23]["IN"][11]);	//boundary
	setField("txtINNoShoChargeInLocal", arrData[strRowNo][23]["IN"][12]);	//charge in local
	
	

	if (arrData[strRowNo][19] != null && arrData[strRowNo][19] != ","
			&& arrData[strRowNo][19] != "") {
		data = arrData[strRowNo][19];
	}
	var arrPeriods = arrData[strRowNo][5];
	
	var maxStayOver = arrPeriods[0];
	var minStayOver = arrPeriods[1];
	var openRetFlag = arrPeriods[2];
	var openConfirm = arrPeriods[3];

	if (strRowData[3] == "One Way") {		
		setField("radFare", "OneWay");
		createOnewayFlexiCodes();

	} else {		
		setField("radFare", "Return");
		
			setField("txtMaxStayMonths", maxStayOver[0]);
			setField("txtMaxStayDays",   maxStayOver[1]);
			setField("txtMaxStayHours",  maxStayOver[2]);
			setField("txtMaxStayMins",   maxStayOver[3]);
		
			setField("txtMinStayMonths", minStayOver[0]);
			setField("txtMinStayDays",   minStayOver[1]);
			setField("txtMinStayHours",  minStayOver[2]);
			setField("txtMinStayMins",   minStayOver[3]);		
			
			if (openRetFlag == 'Y') {
				getFieldByID("chkOpenRT").checked = true;										
				setField("txtConfirmMonths", openConfirm[0]);
				setField("txtConfirmDays",   openConfirm[1]);
				setField("txtConfirmHours",  openConfirm[2]);
				setField("txtConfirmMins",   openConfirm[3]);
			}else{
				getFieldByID("chkOpenRT").checked = false;
				setField("txtConfirmMonths", "");
				setField("txtConfirmDays", "");
				setField("txtConfirmHours", "");
				setField("txtConfirmMins", "");	
			}
		if (arrData[strRowNo][27] == 'Y') {			
			getFieldByID("chkPrintExp").checked = true;
		}		
		createReturnFlexiCodes();
	}

	if(arrData[strRowNo][28] != ""){
		var tmpFlxArray = arrData[strRowNo][28].split(",");
		setField("selFlexiCode", tmpFlxArray[0]); // At the moment add first Flexi code, Future will be more
	}

	var mod = arrData[strRowNo][7];
	var modArr = mod.split(",");

	var times = arrData[strRowNo][10];
	newTimes = times.split(",");
	if (newTimes[0] != null) {
		setField("txtOutTmFrom", newTimes[0]);
	}
	if (newTimes[1] != null) {
		setField("txtOutTmTo", newTimes[1]);
	}
	if (newTimes.length > 2) {
		if (newTimes[2] != null) {
			setField("txtInbTmFrom", newTimes[2]);
		}
		if (newTimes[3] != null) {
			setField("txtInbTmTo", newTimes[3]);
		}
	}
	
	var advance = arrData[strRowNo][8];	
	var advanceDDHHMM = advance.split(":");

	if (advance != "" && advance != "0" && advance != null
			&& advance != "&nbsp;") {		
		getFieldByID("chkAdvance").checked = true;
	}
	if (getFieldByID("chkAdvance").checked) {		
		setField("txtAdvBookDays", advanceDDHHMM[0]);
		setField("txtAdvBookHours", advanceDDHHMM[1]);
		setField("txtAdvBookMins", advanceDDHHMM[2]);

	} else {
		Disable("txtAdvBookDays", true);
		Disable("txtAdvBookHours", true);
		Disable("txtAdvBookMins", true);
	}
	setField("selFareVisibility", arrData[strRowNo][18]);	
	if (arrData[strRowNo][13] == "Active") {
		getFieldByID("chkStatus").checked = true;				
	}else {
		getFieldByID("chkStatus").checked = false;		
	}
	setField("hdnVersion", arrData[strRowNo][15]);
	setField("hdnFareID", arrData[strRowNo][16]);

	if (arrData[strRowNo][17] != "null") {
		setField("txtaRulesCmnts", arrData[strRowNo][17]);
	}

	setField("txtModBufDays", arrData[strRowNo][24]);
	setField("txtModBufHours", arrData[strRowNo][25]);
	setField("txtModBufMins", arrData[strRowNo][26]);
	
	setField("txtModBufDaysDom", arrData[strRowNo][34]);
	setField("txtModBufHoursDom", arrData[strRowNo][35]);
	setField("txtModBufMinsDom", arrData[strRowNo][36]);	
	
	if(arrData[strRowNo][42]!=null && arrData[strRowNo][42]!=""){
		setField("radCurrFareRule", "SEL_CURR");
		getFieldByID("radSelCurrFareRule").checked = true;
		setField("selCurrencyCode", arrData[strRowNo][42]);
	}else{
		setField("radCurrFareRule", "BASE_CURR");
		getFieldByID("radCurrFareRule").checked = true;
		setField("selCurrencyCode","-1");
	}
	

	var deptDOW = "";
	var arrDOW = "";
	var index = strRowData[6].indexOf("In");

	if (index != -1) {
		deptDOW = strRowData[6].substring(strRowData[6].indexOf("-") + 1,
				strRowData[6].indexOf("<br>"));
		arrDOW = strRowData[6].substring(index + 3);
	} else {
		deptDOW = strRowData[6].substring(strRowData[6].indexOf("-") + 1);
	}
	if (arrDOW != "") {
		var strIndex = arrDOW;

		var arrStr = strIndex.split("_");
		for ( var h = 0; h < arrStr.length; h++) {
			if (arrStr[h].indexOf("Sa") != -1) {				
				setChkControls("chkInbSat", true);
			}
			if (arrStr[h].indexOf("Mo") != -1) {				
				setChkControls("chkInbMon", true);
			}
			if (arrStr[h].indexOf("Tu") != -1) {
				setChkControls("chkInbTues", true);
			}
			if (arrStr[h].indexOf("We") != -1) {				
				setChkControls("chkInbWed", true);
			}
			if (arrStr[h].indexOf("Fr") != -1) {
				setChkControls("chkInbFri", true);
			}
			if (arrStr[h].indexOf("Su") != -1) {
				setChkControls("chkInbSun", true);
			}
			if (arrStr[h].indexOf("Th") != -1) {
				setChkControls("chkInbThu", true);
			}
		}
	}

	if (deptDOW != "") {
		var strIndex = deptDOW;
		var strOutArr = strIndex.split("_");
		for ( var t = 0; t < strOutArr.length; t++) {
			if (strOutArr[t].indexOf("Sa") != -1) {
				setChkControls("chkOutSat", true);
			}
			if (strOutArr[t].indexOf("Mo") != -1) {
				setChkControls("chkOutMon", true);
			}
			if (strOutArr[t].indexOf("Tu") != -1) {
				setChkControls("chkOutTues", true);
			}
			if (strOutArr[t].indexOf("We") != -1) {
				setChkControls("chkOutWed", true);
			}
			if (strOutArr[t].indexOf("Fr") != -1) {
				setChkControls("chkOutFri", true);
			}
			if (strOutArr[t].indexOf("Th") != -1) {
				setChkControls("chkOutThu", true);
			}
			if (strOutArr[t].indexOf("Su") != -1) {
				setChkControls("chkOutSun", true);
			}
		}
	}
	
	//fare rule modifications and load factor
	var strAllowModifyDate = arrData[strRowNo][29];
	var strAllowModifyRoute = arrData[strRowNo][30];
	var loadFactorEnabled = arrData[strRowNo][31];
	var loadFactorMax = arrData[strRowNo][32];
	var loadFactorMin = arrData[strRowNo][33];
	
	if(strAllowModifyDate == "Y"){
		getFieldByID("modifyByDate").checked = true;
	}else{
		getFieldByID("modifyByDate").checked = false;
	}
	
	if(strAllowModifyRoute == "Y"){
		getFieldByID("modifyByOND").checked = true;
	}else{
		getFieldByID("modifyByOND").checked = false;
	}
	
	if(loadFactorEnabled == "Y"){
		getFieldByID("loadFactorEnabled").checked = true;
		setField("loadFactorMin", loadFactorMin);
		setField("loadFactorMax",loadFactorMax);
	}else{
		getFieldByID("loadFactorEnabled").checked = false;
	}
	
	var fareDiscount = arrData[strRowNo][37];
	
	if(fareDiscountVisibiliy){
		if(fareDiscount == "Y"){
			getFieldByID("fareDiscount").checked = true;
			setField("fareDiscountMin", arrData[strRowNo][38]);
			setField("fareDiscountMax",arrData[strRowNo][39]);
		}else{
			getFieldByID("fareDiscount").checked = false;
		}
	}	
	setField("txtaInstructions",arrData[strRowNo][40]);
	
	if (arrData[strRowNo][41] == 'Y') {			
		getFieldByID("chkHRT").checked = true;
	}	
	
	
}
function selFareOnchange() {
	
	if (document.getElementById("selFareCat").value == "R") {
		setChkControls("chkADMod", false);
		setChkControls("chkCHMod", false);
		setChkControls("chkINMod", false);
		setChkControls("chkADCNX", false);
		setChkControls("chkCHCNX", false);
		setChkControls("chkINCNX", false);
		setChkControls("chkOpenRT", false);
		Disable("chkADMod", true);
		Disable("chkCHMod", true);
		Disable("chkINMod", true);
		Disable("chkADCNX", true);
		Disable("chkCHCNX", true);
		Disable("chkINCNX", true);
		Disable("chkOpenRT", true);
		Disable("chkHRT", true);
	} else {
		Disable("chkADMod", false);
		Disable("chkCHMod", false);
		Disable("chkINMod", false);
		Disable("chkADCNX", false);
		Disable("chkCHCNX", false);
		Disable("chkINCNX", false);
		if(getText("radFare") == "Return"){
			Disable("chkOpenRT", false);
			Disable("chkHRT", false);
		}		
	}

	if (chidVisibility == 'false') {
		Disable("chkINApp", true);
		disableInfantControl(true);
		Disable("chkCHApp", true);
		disableChildControls(true);

	}

}

function setData() {
	var strTxt = top[1].objTMenu.tabGetValue(screenId);
	if (strTxt != null && strTxt != "") {
		if (isDelete != true) {
			var arrTabData = strTxt.split("~");
			if (arrTabData[0] != null && arrTabData[0] != "") {
				setField("selFareRuleCode", arrTabData[0]);
			}
			if (arrTabData[2] != null && arrTabData[2] != "") {
				setField("selStatus", arrTabData[2]);
			}
		}
	}

}

function controlStatSearch(control) {
	if (control == "selStatus") {
		if (getText("selStatus") != "All") {
			setField("selFareRuleCode", "All")
			Disable("selFareRuleCode", true);

		} else {
			setField("selFareRuleCode", "All")
			Disable("selFareRuleCode", false);
		}
		ctrl_Search_click();

	} else {
		if (getText("selFareRuleCode") != "All") {
			setField("selStatus", "All")
			Disable("selStatus", true);
		} else {
			setField("selStatus", "All")
			Disable("selStatus", false);
		}
	}
}

// on Grid Row click
function rowClick(strRowNo) {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		updateClicked = false;
		agentCodes = "";
		data = "";
		rowNum = strRowNo;
		setTabs();
		//top[1].objTMenu.tabSetValue(screenId, getText("selFareRuleCode") + "~" + getText("hdnRecNo") + "~" + rowNum);
		document.getElementById("frmFareClasses").reset();
		setData();
		strRowData = objDG.getRowValue(strRowNo);
		strGridRow = strRowNo;

		setFields(strGridRow);
		disableInputControls(true);
		disableCnxModTxt(true);
		Disable("btnEdit", false);
		Disable("btnDelete", false);
		setPageEdited(false);
		//fare rule modifications and load factor
		disableFareRuleModifications();
		disableNoShowChargeModelUIControls(true);
		fareRuleCurrClick();
	}

}

function disableInputControls(disabled) {

	Disable("chkStatus", disabled);
	Disable("btnDelete", disabled);
	Disable("txtFc", disabled);
	Disable("txtFBC", disabled);
	Disable("txtDesc", disabled);
	Disable("selFareVisibility", disabled);
	
	Disable("chkAdvance", disabled);
	Disable("radFare", disabled);
	Disable("chkInAll", disabled);
	Disable("chkOutAll", disabled);

	Disable("txtAdvBookDays", disabled);
	Disable("txtAdvBookHours", disabled);
	Disable("txtAdvBookMins", disabled);
	Disable("chkChrgRestrictions", disabled);
	Disable("selFareVisibility", disabled);

	disableReturn(disabled);

	Disable("chkPrintExp", disabled);

	Disable("txtOutTmFrom", disabled);
	Disable("txtOutTmTo", disabled);

	Disable("txtaRulesCmnts", disabled);
	Disable("selPaxCat", disabled);
	Disable("selFareCat", disabled);
	Disable("selFlexiCode", disabled);

	Disable("txtModBufDays", disabled);
	Disable("txtModBufHours", disabled);
	Disable("txtModBufMins", disabled);
	
	Disable("txtModBufDaysDom", disabled);
	Disable("txtModBufHoursDom", disabled);
	Disable("txtModBufMinsDom", disabled);
	
	Disable("txtConfirmMonths", disabled);
	Disable("txtConfirmDays", disabled);
	Disable("txtConfirmHours", disabled);
	Disable("txtConfirmMins", disabled);
	Disable("chkOpenRT", disabled);
	Disable("chkHRT", disabled);

	for ( var t = 0; t < arrDays.length; t++) {
		Disable(arrDays[t], disabled);
		Disable(depDays[t], disabled);

	}
	Disable("btnSave", disabled);
	Disable("btnOverWriteRule", disabled);
	Disable("btnReset", disabled);
	Disable("btnLnkAgent", true);

	Disable("radCurrFareRule", disabled);
	Disable("radSelCurrFareRule", disabled);
	Disable("selCurrencyCode", disabled);			
}

function clearControls() {
	if (getFieldByID("chkOpenRT").checked) {
			getFieldByID("chkOpenRT").checked = false;
		}
	
	if (getFieldByID("chkHRT").checked) {
		getFieldByID("chkHRT").checked = false;
	}

	setField("chkStatus", "");
	setField("txtFc", "");
	setField("txtDesc", "");
	setField("hdnFareID", "");
	setField("hdnRuleCode", "");
	setField("selFareVisibility", "");
	
	setField("hdnVersion", "");
	setField("txtFBC", "");
	setField("txtAdvBookDays", "");
	setField("txtAdvBookHours", "");
	setField("txtAdvBookMins", "");
	setField("chkChrgRestrictions", "");
	setField("txtModification", "");
	setField("txtCancellation", "");	
	setField("txtModificationInLocal","");
	setField("txtCancellationInLocal", "");
	
	setField("selDefineModType", "");
	setField("selDefineCanType", "");
	setField("txtMin_mod", "");
	setField("txtMax_mod", "");
	setField("txtMin_canc", "");
	setField("txtMax_canc", "");
	
	setField("chkAdvance", "");
	setField("radFare", "");

	clearReturn();

	setField("chkPrintExp", "");

	setField("txtOutTmFrom", "");
	setField("txtOutTmTo", "");

	setField("txtaRulesCmnts", "");
	setField("selPaxCat", "A");
	setField("selFareCat", "N");
	setField("selFlexiCode", "-1");

	
	setField("txtModBufDays", "");
	setField("txtModBufHours", "");
	setField("txtModBufMins", "");
	
	setField("txtModBufDaysDom", "");
	setField("txtModBufHoursDom", "");
	setField("txtModBufMinsDom", "");

	setField("txtConfirmMonths", "");
	setField("txtConfirmDays", "");
	setField("txtConfirmHours", "");
	setField("txtConfirmMins", "");
	
	//No Show Percentage Changes
	setField("selADCGTYP", "V");
	setField("selCHCGTYP", "V");
	setField("selINCGTYP", "V");
	setField("txtADNoShoBreakPoint", "");
	setField("txtADNoShoBoundary", "");
	setField("txtCHNoShoBreakPoint", "");
	setField("txtCHNoShoBoundary", "");
	setField("txtINNoShoBreakPoint", "");
	setField("txtINNoShoBoundary", "");	
	setField("selCurrencyCode", "-1");

			
	for ( var t = 0; t < arrDays.length; t++) {
		setChkControls(arrDays[t], false);
		setChkControls(depDays[t], false);
	}
}

function validateAdultDependancy(controlinput, controldisable) {
	var strADref = getValue(controlinput);
	var strMode = getText("hdnMode");

	if (controlinput == "chkADApp") {
		if ((strADref == "false" || strADref == "" || strADref == "off")) {
			setChkControls("chkADRef", false);
			setChkControls("chkADMod", false);
			setChkControls("chkADCNX", false);
			setChkControls("chkINRef", false);
			setChkControls("chkINMod", false);
			setChkControls("chkINCNX", false);
			disableAdultControls(true);
			disableInfantControl(true);
		} else {
			disableAdultControls(false);
			disableInfantControl(false);
		}
	}

	if (chidVisibility == 'false') {
		getFieldByID("chkCHRef").checked = getFieldByID("chkADRef").checked;
		getFieldByID("chkCHApp").checked = getFieldByID("chkADApp").checked;
		getFieldByID("chkCHMod").checked = getFieldByID("chkADMod").checked;
		getFieldByID("chkCHCNX").checked = getFieldByID("chkADCNX").checked;
	}

	if (strADref == "false" || strADref == "" || strADref == "off") {
		for ( var a = 0; a < arrDisableControls.length; a++) {
			setField(controldisable, false);
			Disable(controldisable, true);
		}
	} else if (strADref == "true" || strADref == "on" || strADref == "ontrue") {
		Disable(controldisable, false);
	}
}

function validateInfant(control, maincontrol) {
	var strADmain = getValue(maincontrol)
	var strADref = getValue(control);
	var strMode = getText("hdnMode");

	if (strMode == "Edit") {
		return;
	}// no need validation

	if (control == "chkINApp") {
		if ((strADref == "false" || strADref == "" || strADref == "off")) {
			setField("chkINRef", false);
			setField("chkINMod", false);
			setField("chkINCNX", false);
			disableInfantControl(true);
		} else {
			disableInfantControl(false);
		}
	}

	if (strADmain == "false" || strADmain == "" || strADmain == "off") {
		setField(control, false);
	}
}

function validateChild(control) {
	var isChecked = getValue(control);
	var strMode = getText("hdnMode");

	if (strMode == "Edit") {
		return;
	}// no need validation

	if (isChecked == "false" || isChecked == "" || isChecked == "off") {
		setField("chkCHRef", false);
		setField("chkCHMod", false);
		setField("chkCHCNX", false);
		disableChildControls(true);
	} else {
		disableChildControls(false);
	}
}
function winOnLoad(strMsg, strMsgType) {
	//fare rule modifications and load factor
	
	disableFareRuleModifications();	
	
	top[2].HidePageMessage();

	clearControls();
	setField("chkStatus", true);
	setField("selStatus", "ACT");
	setData();
	disableInputControls(true);
	setPageEdited(false);
	disableCnxModTxt(true);	
	
	fareRuleCurrClick();
	if (top[1].objTMenu.focusTab == screenId) {
		getFieldByID("selFareRuleCode").focus();
	}
	
	if(!fareDiscountVisibiliy){
		setDisplay("tblFareDiscount",false);
	}
	if(!agentInstructionsVisibiliy){
		setDisplay("tblTextInstructions",false);
	}
	if(multipleFltTypeVisibility.onlySingle){
		if(multipleFltTypeVisibility.enabledType=="INT"){
			setDisplay("trModBuffTimeDomestic",false);
			setDisplay("spanModBuffTimeInternationl",false);
		}else if(multipleFltTypeVisibility.enabledType=="DOM"){
			setDisplay("trModBuffTimeInternational",false);
			setDisplay("spanModBuffTimeDomestic",false);
		}
	}
			
	getFieldByID("selFareVisibility").options[0].selected = true;
	Disable("btnEdit", true);
	setChkControls("chkOutAll", true);
	Disable("chkOutAll", true);

	for ( var t = 0; t < arrDays.length; t++) {
		setChkControls(arrDays[t], false);
		Disable(arrDays[t], true);
	}	
	setField("radFare", "OneWay");	
	
	setField("txtOutTmTo", "23:59");
	setField("txtOutTmFrom", "00:00");
	setField("selFlexiCode", "-1");

	setField("hdnPaxTypeAD", arrType[0][0]);
	setField("hdnPaxTypeCH", arrType[1][0]);
	setField("hdnPaxTypeIN", arrType[2][0]);

	DivWrite("tag1", arrType[0][1]);
	DivWrite("tag2", arrType[1][1]);
	DivWrite("tag3", arrType[2][1]);

	for ( var a = 0; a < arrDisableControls.length; a++) {
		Disable(arrDisableControls[a], true);
	}
	
	disableNoShowChargeModelUIControls(true);
	Disable("txtaInstructions", true);
	
	for ( var t = 0; t < depDays.length; t++) {		
		setChkControls(depDays[t], true);
		Disable(depDays[t], true);
	}

	
	var arrNewFlexi = new Array();
	arrNewFlexi[0] = new Array();
	arrNewFlexi[0][0] = "-1";
	arrNewFlexi[0][1] = "";
	var count = 1;
	for(var i = 1; i < arrFlexiJourney.length; i++){
		if(arrFlexiJourney[i][3] == "ACT"){		
			arrNewFlexi[count] = new Array();
			arrNewFlexi[count][0] = arrFlexiJourney[i][0];
			arrNewFlexi[count][1] = arrFlexiJourney[i][1];
			count++;
		}
	}
	objFC.dataArray = arrNewFlexi; 
	objFC.textIndex = "1";
	objFC.id = "selFlexiCode";
	objFC.fillListBox();
		
	if (strMsg != null && strMsgType != null) {
		showCommonError(strMsgType, strMsg);
		if (model != null && model.length > 0) {
			disableInputControls(false);
			Disable("btnDelete", "true");
			setPageEdited(true);		

			disableAdultControls(false);
			Disable("chkADApp", false);
			setChkControls("chkADRef", model[28]);
			setChkControls("chkADMod", model[29]);
			setChkControls("chkADCNX", model[30]);
			setChkControls("chkADApp", model[31]);
			setField("txtADNoShoCharge", model[47]);

			disableChildControls(false);
			Disable("chkCHApp", false);
			setChkControls("chkCHRef", model[32]);
			setChkControls("chkCHApp", model[35]);
			setChkControls("chkCHMod", model[33]);
			setChkControls("chkCHCNX", model[34]);
			setField("txtCHNoShoCharge", model[48]);

			disableInfantControl(false);
			Disable("chkINApp", false);
			setChkControls("chkINRef", model[36]);
			setChkControls("chkINApp", model[39]);
			setChkControls("chkINMod", model[37]);
			setChkControls("chkINCNX", model[38]);
			setField("txtINNoShoCharge", model[49]);

			// pax type
			setField("hdnPaxTypeAD", model[40]);
			setField("hdnPaxTypeCH", model[41]);
			setField("hdnPaxTypeIN", model[42]);

			if (chidVisibility == 'false') {
				Disable("chkINApp", true);
				disableInfantControl(true);
				Disable("chkCHApp", true);
				disableChildControls(true);

			}

			getFieldByID("selFareVisibility").options[0].selected = false;
			setField("txtFc", model[0]);

			setField("txtDesc", model[1]);
			
			
			if (model[2] == "on") {
				setField("chkAdvance", true);
				var advBkTimeDDHHMM = new Array("", "", "");
				if(model[3] != "null" && model[3] != "") {
					advBkTimeDDHHMM = model[3].split(":", 3);
				}
				setField("txtAdvBookDays", advBkTimeDDHHMM[0]);
				setField("txtAdvBookHours", advBkTimeDDHHMM[1]);
				setField("txtAdvBookMins", advBkTimeDDHHMM[2]);
			} else {
				setField("chkAdvance", false);
			}
			if (model[4] == "on") {
				setField("chkStatus", model[4]);
			}
			
			
			if(model[69]!=null && model[70]!=null){
				setField("radCurrFareRule", model[69]);
				setField("selCurrencyCode", model[70]);
			}else{
				setField("radCurrFareRule", "BASE_CURR");
				setField("selCurrencyCode","-1");
			}
			

			if (model[5] == "on") {
				setField("chkChrgRestrictions", true);
				disableCnxModTxt(false);
				if (model[6] != "null") {
					setField("txtModification", model[6]);
				}
				if (model[7] != "null") {
					setField("txtCancellation", model[7]);
				}
				if (model[58] != "null") {
					setField("selDefineModType", model[58]);
				}
				if (model[59] != "null") {
					setField("selDefineCanType", model[59]);
				}
				if (model[60] != "null") {
					setField("txtMin_mod", model[60]);
				}
				if (model[61] != "null") {
					setField("txtMax_mod", model[61]);
				}
				if (model[60] != "null") {
					setField("txtMin_canc", model[62]);
				}
				if (model[61] != "null") {
					setField("txtMax_canc", model[63]);
				}
				
				
				
				if(model[71]!="null"){
					setField("txtModificationInLocal",model[71]);
				}
				
				if(model[72]!="null"){
					setField("txtCancellationInLocal",model[72]);
				}
				if(model[73]!="null"){
					setField("txtADNoShoChargeInLocal",model[73]);
				}
				if(model[74]!="null"){
					setField("txtCHNoShoChargeInLocal",model[74]);
				}
				if(model[75]!="null"){
					setField("txtINNoShoChargeInLocal",model[75]);
				}
				
				
			} else {
				setField("chkChrgRestrictions", false);
			}

			setField("radFare", model[9]);

			if (model[9] == "OneWay") {
				disableReturn(true);
				Disable("chkPrintExp", true);
				for ( var t = 0; t < arrDays.length; t++) {
					setChkControls(arrDays[t], false);
					Disable(arrDays[t], true);
				}
			} else {
				setField("txtMaxStayMonths", model[50]);
				setField("txtMinStayMonths", model[51]);
				setField("txtMaxStayDays", model[10]);
				setField("txtMinStayDays", model[11]);
				setField("txtMaxStayHours", model[22]);
				setField("txtMinStayHours", model[23]);
				setField("txtMaxStayMins", model[24]);
				setField("txtMinStayMins", model[25]);
				if (model[52] == 'Y') {
					getFieldByID("chkOpenRT").checked = true;
					
					setField("txtConfirmMonths", model[53]);
					setField("txtConfirmDays", model[54]);
					setField("txtConfirmHours", model[55]);
					setField("txtConfirmMins", model[56]);
				}

				if (model[46] == 'Y') {
					getFieldByID("chkPrintExp").checked = true;
				}

			}
			setField("txtOutTmFrom", model[12]);
			setField("txtOutTmTo", model[13]);
			if (model[14] != "null") {
				setField("txtInbTmFrom", model[14]);
			}
			if (model[15] != "null") {
				setField("txtInbTmTo", model[15]);
			}

			var arrayArrDays = model[16].split(",");
			var arrayDeptDays = model[17].split(",");
			setField("selFareVisibility", model[18]);
			for ( var t = 0; t < depDays.length; t++) {
				if (arrayDeptDays[t] == "1") {
					setChkControls(depDays[t], true);
				} else {
					setChkControls(depDays[t], false);
				}
			}

			setField("selPaxCat", model[26]);
			setField("selFareCat", model[27]);

			if (model[19] != "") {
				setField("txtaRulesCmnts", model[19]);
			}
			if (model[67] != "") {
				setField("txtaInstructions", model[67]);
			}
			
			if (model[68] == 'Y') {
				getFieldByID("chkHRT").checked = true;
			}
			
			ctrl_visibilityAgents();
			if (model[20] != "" && model[20] != "null") {
				Disable("btnLnkAgent", false);
				agentCodes = model[20];
				data = model[20];
			}
			for ( var t = 0; t < arrDays.length; t++) {
				if (arrayArrDays[t] == "1") {
					setChkControls(arrDays[t], true);
				} else {
					setChkControls(arrDays[t], false);
				}
			}
			if (model[21] != null && model[21] != 'null' && model[21] != "") {
				setField("hdnVersion", model[21]);
				Disable("txtFc", true);
				getFieldByID("txtDesc").focus();
			} else {
				getFieldByID("txtFc").focus();
			}
			
			
			
		}

		if (isSuccessfulSaveMessageDisplayEnabled) {
			if (isSaveTransactionSuccessful) {
				alert("Record Successfully saved!");
			}
		}
	}
	setData();
	Disable("txtModBufDays", true);
	Disable("txtModBufHours", true);
	Disable("txtModBufMins", true);
	Disable("txtModBufDaysDom", true);
	Disable("txtModBufHoursDom", true);
	Disable("txtModBufMinsDom", true);
	if(isOpenRetSupport == 'false'){
		setDisplay("tdLblOpenRTConf", false);
		setDisplay("tdOpenRTConfMnth", false);
		setDisplay("tdOpenRTConfDays", false);
		setDisplay("tdOpenRTConfHrs", false);
		setDisplay("tdOpenRTConfMins", false);
		setDisplay("tdChkOPenRT", false);
		setDisplay("tdLblChkOpenRT", false);
	}
	
	if(showHalfReturn) {
		document.getElementById('halfReturnLabel').style.display = 'block';
		document.getElementById('halfReturnCheck').style.display = 'block';
	} else {
		document.getElementById('halfReturnLabel').style.display = 'none';
		document.getElementById('halfReturnCheck').style.display = 'none';
	}
	
}

function save_click() {
	if (!saveData())
		return;
	else {
		
//		if (getFieldByID("radSelCurrFareRule").checked) {
//			setField("hdnLocalCurrencySelected", "true");
//		}
		
		var isFlexiCodeRemoved = 0;
		if(flexiCode != "" && getText("selFlexiCode") == ""){
			isFlexiCodeRemoved = 1;
		}
		setField("isFlexiCodeRemoved",isFlexiCodeRemoved);
		
		var tempName = trim(getText("txtaRulesCmnts"));
		tempName = replaceall(tempName, "'", "`");
		tempName = replaceEnter(tempName);
		setField("txtaRulesCmnts", tempName);
		
		var tempAgentInstructions = trim(getText("txtaInstructions"));
		tempAgentInstructions = replaceall(tempAgentInstructions, "'", "`");
		tempAgentInstructions = replaceEnter(tempAgentInstructions);
		setField("txtaInstructions", tempAgentInstructions);
		
		
		document.forms["frmFareClasses"].action = "showFare.action";

		var str = top[1].objTMenu.tabGetValue(screenId).split("~");
		if (str[2] != "" && str[2] != null) {
			rowNum = str[2];
		}
		if (chidVisibility == 'false') {
			Disable("chkINApp", false);
			disableInfantControl(false);
			Disable("chkCHApp", false);
			disableChildControls(false);

		}
		top[1].objTMenu.tabSetValue(screenId, getText("selFareRuleCode") + "~"+ getText("hdnRecNo") + "~" + rowNum);
		document.forms["frmFareClasses"].submit();
		top[2].ShowProgress();
	}

}

function saveData() {

	var validated = false;
	var agents = "";
	var isLocalCurrency = false;

	if (updateClicked == true) {
		agents = agentCodes;
	} else {
		agents = data;
	}

	if ((getValue("chkADApp") == "off") || (getValue("chkADApp") == "false") || (getValue("chkADApp") == "")) {
		var confirmation = window.confirm("Applicability for primary passenger type is empty. Are you sure you want to save this fare rule ?");
		if (!confirmation) {
			return;
		}
	}
	
	//fare rule modifications and load factor
	var lfMax = getText("loadFactorMax");
	setField("hdnLoadFactorMax", lfMax);
	var lfMin = getText("loadFactorMin");
	setField("hdnLoadFactorMin", lfMin);
	
	// fare discount
	var isFareDiscountEnabled = (getText("fareDiscount").toUpperCase() == "Y");
	var fareDiscountMin = getText("fareDiscountMin");
	var fareDiscountMax = getText("fareDiscountMax");
	
	setField("hdnFareDiscountMin", fareDiscountMin);
	setField("hdnFareDiscountMax", fareDiscountMax);

	var strFc = getText("txtFc").toUpperCase();
	setField("hdnRuleCode", strFc);
	var strDesc = getText("txtDesc");
	var strVisibility = getText("selFareVisibility");
	setField("hdnVisibility", getValue("selFareVisibility"));
	var strmaxStayMonths = getText("txtMaxStayMonths");
	var strminStayMonths = getText("txtMinStayMonths");
	var strmaxStay = getText("txtMaxStayDays");
	var strminStay = getText("txtMinStayDays");
	var strmaxHours = getText("txtMaxStayHours");
	var strminHours = getText("txtMinStayHours");
	var strmaxMins = getText("txtMaxStayMins");
	var strminMins = getText("txtMinStayMins");
	var chkAdv = getText("chkAdvance");
	var chkOpenRT = getText("chkOpenRT");
	var blnchkOpenRT = getFieldByID("chkOpenRT").checked;
	var txtAdv = getText("txtAdvBookDays");
	var txtAdvHrs = getText("txtAdvBookHours");
	var txtAdvMins = getText("txtAdvBookMins");
	setField("hdnAgents", agents);
	var selFareCat = getValue("selFareCat");
	var selFlexiCode = getValue("selFlexiCode");
		
	var advBooking = getText("txtAdvBookDays");
	var intadvBooking = isNaN(advBooking);
	var advBookingHrs = getText("txtAdvBookHours");
	var intadvBookingHrs = isNaN(advBookingHrs);
	var advBookingMins = getText("txtAdvBookMins");
	var intadvBookingMins = isNaN(advBookingMins);
	var intMaxStayMonths = isNaN(getText("txtMaxStayMonths"));
	var intMinStayMonths = isNaN(getText("txtMinStayMonths"));
	var intMaxStay = isNaN(getText("txtMaxStayDays"));
	var intMinStay = isNaN(getText("txtMinStayDays"));
	var intMaxHours = isNaN(getText("txtMaxStayHours"));
	var intMinHours = isNaN(getText("txtMinStayHours"));
	var intMaxMins = isNaN(getText("txtMaxStayMins"));
	var intMinMins = isNaN(getText("txtMinStayMins"));
	var intChgMod = isNaN(getText("txtModification"));
	var intChgCancel = isNaN(getText("txtCancellation"));	
	var intChgModInLocal = isNaN(getText("txtModificationInLocal"));
	var intChgCancelInLocal = isNaN(getText("txtCancellationInLocal"));
	
	var currencyType = getValue("radCurrFareRule");
	var selCurrencyCode = getValue("selCurrencyCode");
	
	if(currencyType=="SEL_CURR" && selCurrencyCode!="-1"){
		isLocalCurrency = true;
	}
	
	
	var modType = getValue("selDefineModType");
	var cancType = getValue("selDefineCanType");
	var txtMinVal_mod = getText("txtMin_mod");
	var txtMaxVal_mod = getText("txtMax_mod");
	var txtMinVal_canc = getText("txtMin_canc");
	var txtMaxVal_canc = getText("txtMax_canc");
	
	var strOpRtMnth = getText("txtConfirmMonths");
	var strOpRtDays = getText("txtConfirmDays");
	var strOpRtHrs = getText("txtConfirmHours");
	var strOpRtMin = getText("txtConfirmMins");
	
	var intOpRtMnth = isNaN(getText("txtConfirmMonths"));
	var intOpRtDay = isNaN(getText("txtConfirmDays"));
	var intOpRtHrs = isNaN(getText("txtConfirmHours"));
	var intOpRtMin = isNaN(getText("txtConfirmMins"));
	
	var strOpRtConfInMin = 0;
	
	if (!intOpRtMnth && strOpRtMnth != "")
		strOpRtConfInMin = strOpRtConfInMin + (parseInt(strOpRtMnth) * 30 * 24 * 60);
	if (!intOpRtDay && strOpRtDays != "")
		strOpRtConfInMin = strOpRtConfInMin + (parseInt(strOpRtDays) * 24 * 60);
	if (!intOpRtHrs && strOpRtHrs != "")
		strOpRtConfInMin = strOpRtConfInMin + (parseInt(strOpRtHrs) * 60);
	if (!intOpRtMin && strOpRtMin != "")
		strOpRtConfInMin = strOpRtConfInMin + parseInt(strOpRtMin);
	
	setVisibilityForAll();

	var chkRest = getText("chkChrgRestrictions");
	var strMod = getText("txtModification");
	var strCancel = getText("txtCancellation");
	var strChgModInLocal = getText("txtModificationInLocal");
	var strChgCancelInLocal = getText("txtCancellationInLocal");
	
	var maxDays = 0;
	var minDays = 0;
	if (!intMaxStayMonths && strmaxStayMonths != "")
		maxDays = maxDays + (parseInt(strmaxStayMonths) * 30 * 24 * 60);
	if (!intMinStayMonths && strminStayMonths != "")
		minDays = minDays + (parseInt(strminStayMonths) * 30 * 24 * 60);
	if (!intMaxStay && strmaxStay != "")
		maxDays = maxDays + (parseInt(strmaxStay) * 24 * 60);
	if (!intMinStay && strminStay != "")
		minDays = minDays + (parseInt(strminStay) * 24 * 60);
	if (!intMaxHours && strmaxHours != "")
		maxDays = maxDays + (parseInt(strmaxHours) * 60);
	if (!intMinHours && strminHours != "")
		minDays = minDays + (parseInt(strminHours) * 60);
	if (!intMaxMins && strmaxMins != "")
		maxDays = maxDays + parseInt(strmaxMins);
	if (!intMinMins && strminMins != "")
		minDays = minDays + parseInt(strminMins);

	var returnFare = getText("radFare");
	var isReturn = (returnFare == "Return");
	var strDeptTimeFrom = getText("txtOutTmFrom");
	var strDeptTimeTo = getText("txtOutTmTo");
	var strArrTimeFrom = getText("txtInbTmFrom");
	var strTimeTo = getText("txtInbTmTo");

	var strModBufDays = getText("txtModBufDays");
	var strModBudHours = getText("txtModBufHours");
	var strModBufMins = getText("txtModBufMins");

	var intModBufDaysInMilSec = (strModBufDays != "") ? (parseInt(strModBufDays) * 24 * 60 * 60 * 1000)
			: 0;
	var intModBufHoursInMilSec = (strModBudHours != "") ? parseInt(strModBudHours) * 60 * 60 * 1000
			: 0;
	var intModbufMinsInMilSec = (strModBufMins != "") ? parseInt(strModBufMins) * 60 * 1000
			: 0;
			
	var strModBufTimeInMilSec = strModBufDays + strModBudHours + strModBufMins;

	var intModBufTimeinMilSec = intModBufDaysInMilSec + intModBufHoursInMilSec
			+ intModbufMinsInMilSec;
			
	var strModBufDaysDomestic = getText("txtModBufDaysDom");
	var strModBudHoursDomestic = getText("txtModBufHoursDom");
	var strModBufMinsDomestic = getText("txtModBufMinsDom");
			
	var intModBufDaysDomInMilSec = (strModBufDaysDomestic != "") ? (parseInt(strModBufDaysDomestic) * 24 * 60 * 60 * 1000)
			: 0;
	var intModBufHoursDomInMilSec = (strModBudHoursDomestic != "") ? parseInt(strModBudHoursDomestic) * 60 * 60 * 1000
			: 0;
	var intModbufMinsDomInMilSec = (strModBufMinsDomestic != "") ? parseInt(strModBufMinsDomestic) * 60 * 1000
			: 0;
			
	var strModBufTimeDomInMilSec = strModBufDaysDomestic + strModBudHoursDomestic + strModBufMinsDomestic;

	var intModBufTimeDominMilSec = intModBufDaysDomInMilSec + intModBufHoursDomInMilSec
			+ intModbufMinsDomInMilSec;
	
	//fare rule modifications and load factor
	var isLoadFactorEnabled = getText("loadFactorEnabled");
	var loadFactorMin = getText("loadFactorMin");
	var loadFactorMax = getText("loadFactorMax");
	
	//fare rule no show charging mode
	var adultNoShowChargeType = trim(getValue("selADCGTYP"));
	var adultNoShowChargeBreakPoint = trim(getText("txtADNoShoBreakPoint")) != "" ? trim(getValue("txtADNoShoBreakPoint")):0;
	var adultNoShowChargeBoundary = trim(getText("txtADNoShoBoundary"))!= "" ? trim(getValue("txtADNoShoBoundary")):0;
	var adultNoShowCharge = trim(getText("txtADNoShoCharge"));
	
	var childNoShowChargeType = trim(getValue("selCHCGTYP"));
	var childNoShowChargeBreakPoint = trim(getText("txtCHNoShoBreakPoint"))!= "" ? trim(getValue("txtCHNoShoBreakPoint")):0;
	var childNoShowChargeBoundary = trim(getText("txtCHNoShoBoundary"))!= "" ? trim(getValue("txtCHNoShoBoundary")):0;
	var childNoShowCharge = trim(getText("txtCHNoShoCharge"));
	
	var infantNoShowChargeType = trim(getValue("selINCGTYP"));
	var infantNoShowChargeBreakPoint = trim(getText("txtINNoShoBreakPoint"))!= "" ? trim(getValue("txtINNoShoBreakPoint")):0;
	var infantNoShowChargeBoundary = trim(getText("txtINNoShoBoundary"))!= "" ? trim(getValue("txtINNoShoBoundary")):0;
	var infantNoShowCharge = trim(getText("txtINNoShoCharge"));

	chkBox_Clicked();

	if (strFc == "") {
		showERRMessage(fareclassRqrd);
		getFieldByID("txtFc").focus();

	} else if (strDesc == "") {
		showERRMessage(descriptionRqrd);
		getFieldByID("txtDesc").focus();
	}else if(currencyType=="SEL_CURR" && selCurrencyCode=="-1"){
		showERRMessage(localCurrencyCodeRqrd);
		getFieldByID("selCurrencyCode").focus();
	} else if (isValueExist(fixedBCFareRuleArr, strFc)
			&& strVisibility != 'TravelAgent') {
		showERRMessage(visibilityInvalid);
		getFieldByID("selFareVisibility").focus();
	} else if (strVisibility == "") {
		showERRMessage(visibilityRqrd);
		getFieldByID("selFareVisibility").focus();
	} else if ((chkRest == "ON" || chkRest == "on") && !isLocalCurrency && (strMod == "" && strCancel == "")) {
		showERRMessage(restrictionReqd);
	} else if ((chkRest == "ON" || chkRest == "on") && isLocalCurrency && (strChgModInLocal == "" && strChgCancelInLocal == "")) {
		showERRMessage(restrictionReqd);

	} else if ((chkRest == "ON" || chkRest == "on") && !isLocalCurrency && intChgMod) {
		showERRMessage(modNaN);
		getFieldByID("txtModification").focus();
	} else if ((chkRest == "ON" || chkRest == "on") && isLocalCurrency && intChgModInLocal) {
		showERRMessage(modNaN);
		getFieldByID("txtModificationInLocal").focus();
	} else if ((chkRest == "ON" || chkRest == "on") && !isLocalCurrency && intChgCancel) {
		showERRMessage(cancellationNaN);
		getFieldByID("txtCancellation").focus();
	} else if ((chkRest == "ON" || chkRest == "on") && isLocalCurrency && intChgCancelInLocal) {
		showERRMessage(cancellationNaN);
		getFieldByID("txtCancellationInLocal").focus();	
	}  else if ((chkRest == "ON" || chkRest == "on") && modType == "") {
		showERRMessage(modTypeNotSelected);
		getFieldByID("selDefineModType").focus();
	
	}else if (modType != selModeTyps.v && !isLocalCurrency && (Number(strMod) < 0 ||  Number(strMod) > 100)){
		showERRMessage(modPrecentage);
		getFieldByID("txtCancellation").focus();
	}else if (modType != selModeTyps.v && isLocalCurrency && (Number(strChgModInLocal) < 0 ||  Number(strChgModInLocal) > 100)){
		showERRMessage(modPrecentage);
		getFieldByID("txtModificationInLocal").focus();	
		
	}  else if ( (chkRest == "ON" || chkRest == "on") && txtMinVal_mod != "" && isNaN(txtMinVal_mod)) {
		showERRMessage(typeMinNaN);
		getFieldByID("txtMin_mod").focus();
			
	}  else if ( (chkRest == "ON" || chkRest == "on") && txtMaxVal_mod != "" && isNaN(txtMaxVal_mod)) {
		showERRMessage(typeMaxNaN);
		getFieldByID("txtMax_mod").focus();
				
	}else if ((chkRest == "ON" || chkRest == "on") && txtMaxVal_mod != "" && txtMinVal_mod != "" && 
			Number(txtMaxVal_mod)< Number(txtMinVal_mod)){	
		showERRMessage(modMaxShouldBeGreater);
		getFieldByID("txtMax_mod").focus();		
				
	}  else if ( (chkRest == "ON" || chkRest == "on") && cancType == "") {
		showERRMessage(cancTypeNotSelected);
		getFieldByID("selDefineCanType").focus();
		
	}else if (cancType != selModeTyps.v && !isLocalCurrency && (Number(strCancel) < 0 || Number(strCancel) > 100)){
		showERRMessage(cancPrecentage);		
		getFieldByID("txtCancellation").focus();
	}else if (cancType != selModeTyps.v && isLocalCurrency && (Number(strChgCancelInLocal) < 0 || Number(strChgCancelInLocal) > 100)){
		showERRMessage(cancPrecentage);		
		getFieldByID("txtCancellationInLocal").focus();	
		
	}else if ((chkRest == "ON" || chkRest == "on")&& modType != selModeTyps.v && txtMinVal_mod == "" && txtMaxVal_mod == ""){
		showERRMessage(modMinMaxEmpty);		
		getFieldByID("txtMin_mod").focus();
		
	}else if ((chkRest == "ON" || chkRest == "on")&& modType != selModeTyps.v && txtMaxVal_mod <=0){
		showERRMessage(maxModZero);
		getFieldByID("txtMax_mod").focus();
		
	}  else if ((chkRest == "ON" || chkRest == "on") && txtMinVal_canc != "" && isNaN(txtMinVal_canc)) {		
		showERRMessage(typeMinNaN);
		getFieldByID("txtMin_canc").focus();
		
	}  else if ((chkRest == "ON" || chkRest == "on") && txtMaxVal_canc != "" && isNaN(txtMaxVal_canc)) {	
		showERRMessage(typeMaxNaN);		
		getFieldByID("txtMax_canc").focus();
		
	}else if ((chkRest == "ON" || chkRest == "on")&& cancType != selModeTyps.v && txtMaxVal_canc == "" && txtMinVal_canc == ""){
		showERRMessage(cancMinMaxEmpty);
		getFieldByID("txtMin_canc").focus();
		
	}else if ((chkRest == "ON" || chkRest == "on") && txtMaxVal_canc != "" && txtMinVal_canc != "" &&
			Number(txtMaxVal_canc)< Number(txtMinVal_canc)){
		showERRMessage(cancMaxShouldBeGreater);
		getFieldByID("txtMin_canc").focus();
		
	}else if ((chkRest == "ON" || chkRest == "on")&& cancType != selModeTyps.v && txtMaxVal_canc <=0){
		showERRMessage(maxCancZero);
		getFieldByID("txtMax_canc").focus();
		
	}  else if (chkAdv == "on" && txtAdv == "" && txtAdvHrs == "" && txtAdvMins == "") {
		showERRMessage(advBKDaysReqd);
		getFieldByID("txtAdvBookDays").focus();

	} else if (chkAdv == "on" && intadvBooking && intadvBookingHrs && intadvBookingMins) {
		showERRMessage(advanceBkNan);
		getFieldByID("txtAdvBookDays").focus();

	} else if (strDeptTimeFrom == "") {
		showERRMessage(depatureFromTimeRqrd);
		getFieldByID("txtOutTmFrom").focus();

	} else if (strDeptTimeTo == "") {
		showERRMessage(depatureToTimeRqrd);
		getFieldByID("txtOutTmTo").focus();

	} else if (returnFare == "") {
		showERRMessage(fareTypeReqd);
		getFieldByID("txtOutTmTo").focus();

	} else if (isReturn && strArrTimeFrom == "") {
		showERRMessage(arrivalFromTimeRqrd);
		getFieldByID("txtInbTmFrom").focus();

	} else if (isReturn && strTimeTo == "") {
		showERRMessage(arrivalToTimeRqrd);
		getFieldByID("txtInbTmTo").focus();

	} else if (returnFare != "Return"
			&& (strmaxStayMonths != "" || strmaxStay != "" || strmaxHours != "" || strmaxMins != "")) {
		showERRMessage(maxstayOverShouldEmpty)
		getFieldByID("txtMaxStayDays").focus();

	} else if (isReturn && (getText("txtMaxStayMonths") != "" && intMaxStayMonths)
			|| (getText("txtMaxStayDays") != "" && intMaxStay)
			|| (getText("txtMaxStayHours") != "" && intMaxHours)
			|| (getText("txtMaxStayMins") != "" && intMaxMins)) {
		showERRMessage(maxStayNaN);
		getFieldByID("txtMaxStayMonths").focus();

	} else if (returnFare != "Return"
			&& (strminStayMonths != "" || strminStay != "" || strminHours != "" || strminMins != "")) {
		showERRMessage(minstayOverShouldEmpty);
		getFieldByID("txtMinStayMonths").focus();

	} else if (isReturn && (getText("txtMinStayMonths") != "" && intMinStayMonths)
			|| (getText("txtMinStayDays") != "" && intMinStay)
			|| (getText("txtMinStayHours") != "" && intMinHours)
			|| (getText("txtMinStayMins") != "" && intMinMins)) {
		showERRMessage(minStayNaN);
		getFieldByID("txtMinStayMonths").focus();
		
	} else if (chkOpenRT == "on" && maxDays == 0) {
		showERRMessage(maxStayRequired);
		getFieldByID("txtMaxStayMonths").focus();		
		
	} else if (chkOpenRT == "on" && (getText("txtConfirmMonths") != "" && intOpRtMnth)
			|| (getText("txtConfirmDays") != "" && intOpRtDay)
			|| (getText("txtConfirmHours") != "" && intOpRtHrs)
			|| (getText("txtConfirmMins") != "" && intOpRtMin)) {
		showERRMessage(confPeriodNaN);
		getFieldByID("txtConfirmMonths").focus();
		
	} else if (maxDays < strOpRtConfInMin){
		showERRMessage(confPeriodLarger);
		getFieldByID("txtConfirmMonths").focus();
		
	} else if (isReturn && (parseInt(strmaxStayMonths) <  0 || parseInt(strmaxStayMonths) >  12)) {
		showERRMessage(valMonth);
		getFieldByID("txtMaxStayMonths").focus();	
		
	} else if (isReturn && (parseInt(strminStayMonths) < 0 || parseInt(strmaxStayMonths) >  12)) {
		showERRMessage(valMonth);
		getFieldByID("txtMinStayMonths").focus();
		
	} else if (isReturn && (parseInt(strminMins) > 59 || parseInt(strminMins) < 0)) {
		showERRMessage(valMinutes);
		getFieldByID("txtMinStayMins").focus();
		
	} else if (isReturn && ( parseInt(strmaxHours) > 23 || parseInt(strmaxHours) < 0) ) {
		showERRMessage(valHours);
		getFieldByID("txtMaxStayHours").focus();
		
	} else if (isReturn && parseInt(strminHours) > 23) {
		showERRMessage(valHours);
		getFieldByID("txtMinStayHours").focus();
		
	} else if (isReturn && (strmaxStay < 0 || strmaxStay > 31 )) {
		showERRMessage(valDays);
		getFieldByID("txtMaxStayDays").focus();

	} else if (isReturn && (strminStay < 0 || strminStay > 31)) {
		showERRMessage(valDays);
		getFieldByID("txtMinStayMonths").focus();

	} else if (isReturn && (maxDays < minDays) && !isOnlyMinimumStayOverDefined()) {
		showERRMessage(maxstayOverShouldBeGreater)
		getFieldByID("txtMinStayMonths").focus();

	} else if (isReturn && !chk_Checked(arrDays)) {
		showERRMessage(arrivaldaysRqrd);
		getFieldByID("chkInbMon").focus();

	} else if (returnFare != "Return" && chk_Checked(arrDays)) {
		showERRMessage(arrivaldaysShouldEmpty);
		getFieldByID("chkOutMon").focus();

	} else if (!chk_Checked(depDays)) {
		showERRMessage(deptdaysRqrd)
		getFieldByID("chkOutMon").focus();

	} else if (!IsValidTime(strDeptTimeFrom)) {
		showERRMessage(depttimeFormat)
		getFieldByID("txtOutTmFrom").focus();

	} else if (!IsValidTime(strDeptTimeTo)) {
		showERRMessage(depttimeFormat)
		getFieldByID("txtOutTmTo").focus();

	} else if (isReturn && (!IsValidTime(strArrTimeFrom))) {
		showERRMessage(arrivaltimeFormat);
		getFieldByID("txtInbTmFrom").focus();

	} else if (isReturn && (!IsValidTime(strTimeTo))) {
		showERRMessage(arrivaltimeFormat);
		getFieldByID("txtInbTmTo").focus();

	} else if (compareTime(strDeptTimeFrom, strDeptTimeTo) == false) {
		showERRMessage(DeptFromTmExceeds);
		getFieldByID("txtOutTmTo").focus();

	} else if (returnFare != "Return"
			&& (strArrTimeFrom != "" || strTimeTo != "")) {
		showERRMessage(arrivaltimeShouldEmpty);
		getFieldByID("txtInbTmFrom").focus();

	} else if (isReturn && compareTime(strArrTimeFrom, strTimeTo) == false) {
		showERRMessage(ArrFromTmExceeds);
		getFieldByID("txtInbTmFrom").focus();

	} else if (getText("selFareVisibility").toUpperCase().indexOf("AGENT") != -1
			&& (agents == "")) {
		showERRMessage(agentsRqrd);

	} else if ((strModBudHours != "") && (parseInt(strModBudHours) > 23)) {
		showERRMessage(maxhours);
		getFieldByID("txtModBufHours").focus();

	} else if ((strModBufMins != "") && (parseInt(strModBufMins) > 59)) {
		showERRMessage(maxMiniutes);
		getFieldByID("txtModBufMins").focus();

	} else if ((strModBufTimeInMilSec != "")
			&& (intModBufTimeinMilSec < parseInt(bufTimeInMilSec))) {
		showERRMessage(modBufTimeShouldBeGreater + bufferTime);
		getFieldByID("txtModBufDays").focus();

	} else if ((strModBufTimeDomInMilSec != "")
			&& (intModBufTimeDominMilSec < parseInt(domesticBufTimeInMilSec))) {
		showERRMessage(domesticModBufTimeShouldBeGreater + domesticBufferTime);
		getFieldByID("txtModBufDaysDom").focus();

	} else if (checkInvalidChar(getText("txtaRulesCmnts"), invalidCharacter,
			"Rules and Comments") != "") {
		showERRMessage(invalidCharacter);
		getFieldByID("txtaRulesCmnts").focus();
		return false;
	} else if (checkInvalidChar(getText("txtaInstructions"), invalidCharacter,
			"Agent Instruction") != "") {
		showERRMessage(invalidCharacter);
		getFieldByID("txtaInstructions").focus();
		return false;
	} else if ( isLoadFactorEnabled == "ON" || isLoadFactorEnabled == "on" ) {
		//fare rule modifications and load factor
		if(loadFactorMin == "" && loadFactorMax == ""){			
			showERRMessage(lfReequired);
			getFieldByID("loadFactorMin").focus();
			return false;
		}else if(loadFactorMin != "" && loadFactorMax == ""){			
			if(parseInt(loadFactorMin,10) < 0){				
				showERRMessage(lfMinZero);
				getFieldByID("loadFactorMin").focus();
				return false;
			}
		}else if(loadFactorMax != "" && loadFactorMin == ""){			
			if(parseInt(loadFactorMax,10) < 0){				
				showERRMessage(lfMaxZero);
				getFieldByID("loadFactorMax").focus();
				return false;
			}
		}else if(loadFactorMin != "" && loadFactorMax != ""){			
			if(parseInt(loadFactorMin,10) < 0){				
				showERRMessage(lfMinZero);
				getFieldByID("loadFactorMin").focus();
				return false;
			}else if(parseInt(loadFactorMax,10) < 0){				
				showERRMessage(lfMaxZero);
				getFieldByID("loadFactorMax").focus();
				return false;
			}else if(parseInt(loadFactorMin,10) > parseInt(loadFactorMax,10)){				
				showERRMessage(lfMinGrMax);
				getFieldByID("loadFactorMin").focus();
				return false;
			}
		}		
		validated = true;
	} else if (fareDiscountVisibiliy && isFareDiscountEnabled){
		if(fareDiscountMin == ""){
			showERRMessage("Fare discount min is required");
			getFieldByID("fareDiscountMin").focus();
			return false;
		}
		if(fareDiscountMax == ""){
			showERRMessage("Fare discount max is required");
			getFieldByID("fareDiscountMin").focus();
			return false;
		}
		
		var fdMin = parseInt(fareDiscountMin, 10);
		var fdMax = parseInt(fareDiscountMax, 10);
		if(fdMin < 0 || fdMin>100){
			showERRMessage("Fare discount min is invalid");
			getFieldByID("fareDiscountMin").focus();
			return false;
		}
		
		if(fdMax < 0 || fdMax>100){
			showERRMessage("Fare discount max is invalid");
			getFieldByID("fareDiscountMax").focus();
			return false;
		}
		
		if(fdMax<fdMin){
			showERRMessage("Fare discount max cannot be less than min");
			getFieldByID("fareDiscountMax").focus();
			return false;
		}
		validated = true;
	}else {
		validated = true;
		setField("hdnRecNo", "1");
		top[2].HidePageMessage();
	}
	
	//No show Charge Types validations
	if(isNoShowChargeTypeNotV(adultNoShowChargeType) && parseFloat(adultNoShowChargeBreakPoint) == 0){
		showERRMessage(noShowBreakPointEmpty);
		getFieldByID("txtADNoShoBreakPoint").focus();
		return false;
	} else if(isNoShowChargeTypeNotV(adultNoShowChargeType) && parseFloat(adultNoShowChargeBoundary) == 0){
		showERRMessage(noShowBoundaryEmpty);
		getFieldByID("txtADNoShoBoundary").focus();
		return false;
	} else if(parseFloat(adultNoShowChargeBreakPoint) > parseFloat(adultNoShowChargeBoundary)){
		showERRMessage(noShowBoundaryBreakPoint);
		getFieldByID("txtADNoShoBoundary").focus();
		return false;
	} else if(isNoShowChargeTypeNotV(adultNoShowChargeType) && (parseFloat(adultNoShowCharge)==0 || parseFloat(adultNoShowCharge)>100)){
		showERRMessage(noShowChargePercentage);
		getFieldByID("txtADNoShoCharge").focus();
		return false;
	} else if(isNoShowChargeTypeNotV(childNoShowChargeType) && parseFloat(childNoShowChargeBreakPoint) == 0){
		showERRMessage(noShowBreakPointEmpty);
		getFieldByID("txtCHNoShoBreakPoint").focus();
		return false;
	} else if(isNoShowChargeTypeNotV(childNoShowChargeType) && parseFloat(childNoShowChargeBoundary) == 0){
		showERRMessage(noShowBoundaryEmpty);
		getFieldByID("txtCHNoShoBoundary").focus();
		return false;
	} else if(parseFloat(childNoShowChargeBreakPoint) > parseFloat(childNoShowChargeBoundary)){
		showERRMessage(noShowBoundaryBreakPoint);
		getFieldByID("txtCHNoShoBoundary").focus();
		return false;
	} else if(isNoShowChargeTypeNotV(childNoShowChargeType) && (parseFloat(childNoShowCharge)==0 || parseFloat(childNoShowCharge)>100)){
		showERRMessage(noShowChargePercentage);
		getFieldByID("txtCHNoShoCharge").focus();
		return false;
	} else if(isNoShowChargeTypeNotV(infantNoShowChargeType) && parseFloat(infantNoShowChargeBreakPoint) == 0){
		showERRMessage(noShowBreakPointEmpty);
		getFieldByID("txtINNoShoBreakPoint").focus();
		return false;
	} else if(isNoShowChargeTypeNotV(infantNoShowChargeType) && parseFloat(infantNoShowChargeBoundary) == 0){
		showERRMessage(noShowBoundaryEmpty);
		getFieldByID("txtINNoShoBoundary").focus();
		return false;
	} else if(parseFloat(infantNoShowChargeBreakPoint) > parseFloat(infantNoShowChargeBoundary)){
		showERRMessage(noShowBoundaryBreakPoint);
		getFieldByID("txtINNoShoBoundary").focus();
		return false;
	} else if(isNoShowChargeTypeNotV(infantNoShowChargeType) && (parseFloat(infantNoShowCharge)==0 || parseFloat(infantNoShowCharge)>100)){
		showERRMessage(noShowChargePercentage);
		getFieldByID("txtINNoShoCharge").focus();
		return false;
	}
	return validated;
}

//Checks if only the minimum stayover time is defined. 
function isOnlyMinimumStayOverDefined(){
	var intMaxStayMonths = getText("txtMaxStayMonths");
	var intMinStayMonths = getText("txtMinStayMonths");
	var intMaxStay = getText("txtMaxStayDays");
	var intMinStay = getText("txtMinStayDays");
	var intMaxHours = getText("txtMaxStayHours");
	var intMinHours = getText("txtMinStayHours");
	var intMaxMins = getText("txtMaxStayMins");
	var intMinMins = getText("txtMinStayMins");
	if((intMaxStayMonths == "" && intMaxStay == "" && intMaxHours =="" && intMaxMins == "")
			&& (intMinStayMonths != "" || intMinStay != "" || intMinHours != "" || intMinMins != "")){
		return true;
	}else{
		return false;
	}
}

//fare rule modifications and load factor
function VaidateFactor(){
	
	var loadFactorMin = getText("loadFactorMin");
	var loadFactorMax = getText("loadFactorMax");
	if(loadFactorMin!= ""){
		var strCC = loadFactorMin;
		var strLen = strCC.length;
		var blnVal = isPositiveInt(strCC)
		if (!blnVal) {
			setField("loadFactorMin", strCC.substr(0, strLen - 1));
			getFieldByID("loadFactorMin").focus();
		}
	}
	
	if(loadFactorMax!= ""){
		var strCC = loadFactorMax;
		var strLen = strCC.length;
		var blnVal = isPositiveInt(strCC)
		if (!blnVal) {
			setField("loadFactorMax", strCC.substr(0, strLen - 1));
			getFieldByID("loadFactorMax").focus();
		}
	}
}

function KPValidatePositiveInteger(control, type) {
	var strCC = getText(control);
	var strLen = strCC.length;
	var blnVal = isPositiveInt(strCC)
	if (!blnVal) {
		setField(control, strCC.substr(0, strLen - 1));
		getFieldByID(control).focus();
	}
	switch(type){
		case "MON": if(Number(getText(control)) > 12){
						setField(control, strCC.substr(0, strLen - 1));
						getFieldByID(control).focus();
					}
					break;
		case "DAY":if(Number(getText(control)) > 31){
						setField(control, strCC.substr(0, strLen - 1));
						getFieldByID(control).focus();
					}
					break;
		case "HRS":if(Number(getText(control)) > 23){
						setField(control, strCC.substr(0, strLen - 1));
						getFieldByID(control).focus();
					}
					break;
		case "MIN":	if(Number(getText(control)) > 59){
						setField(control, strCC.substr(0, strLen - 1));
						getFieldByID(control).focus();
					}
					break;
	
	}

}

function chk_Checked(arr) {
	var validated = false;
	for ( var i = 0; i < arr.length; i++) {
		if (getText(arr[i]) == "on") {
			validated = true;
			break;
		}
	}
	return validated;
}

function ctrl_Editclick() {
	flexiCode = getText("selFlexiCode");

	for ( var a = 0; a < arrDisableControls.length; a++) {
		Disable(arrDisableControls[a], false);
	}
	disableNoShowChargeModelUIControlsEditClick();
	Disable("txtaInstructions", false);
	var strFc = getText("txtFc");
	setField("hdnMode", "Edit");
	Disable("btnDelete", true);
	status = "Edit";
	if (strFc == "") {
		showERRMessage(selectRow);
		disableInputControls(true);
	} else {
		if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
			top[2].HidePageMessage();
			disableInputControls(false);
			ctrl_visibility_Select();
			Disable("txtFc", "true");			
			getFieldByID("txtDesc").focus();

			var rest = getFieldByID("chkChrgRestrictions").checked;
			if (rest == true) {
				disableCnxModTxt(false);	
			} else {
				disableCnxModTxt(true);	
			}
			if (!getFieldByID("chkAdvance").checked) {
				Disable("txtAdvBookDays", true);
				Disable("txtAdvBookHours", true);
				Disable("txtAdvBookMins", true);
			}
			var checked = getText("radFare");
			
			if (checked != "Return") {
				Disable("chkInAll", true);
				Disable("chkHRT", true);
				Disable("chkOpenRT", true);
				disableReturn(true);
				for ( var t = 0; t < arrDays.length; t++) {
					Disable(arrDays[t], true);
				}
				Disable("chkPrintExp", true);
			} else {
				if (((getText("txtMaxStayMonths") != "") && (getText("txtMaxStayMonths") != 0))
						|| ((getText("txtMaxStayDays") != "") && (getText("txtMaxStayDays") != 0))
						|| ((getText("txtMaxStayHours") != "") && (getText("txtMaxStayHours") != 0))
						|| ((getText("txtMaxStayMins") != "") && (getText("txtMaxStayMins") != 0))) {

					Disable("chkPrintExp", false);
				} else {
					if (getFieldByID("chkPrintExp").checked) {
						getFieldByID("chkPrintExp").checked = false;
						
					}
					Disable("chkPrintExp", true);
				}
			}
			//fare rule modifications and load factor
			enableFareRuleModifications();
		}
		setField("hdnMode", "Edit");
		selFareOnchange();
		Disable("btnDelete", true);
	}
	setPageEdited(false);
	
	if(showNoFareSplitOption == 'true'){
		//Shows the chkUpdateCommentsOnly if it's hidden.
		document.getElementById('chkUpdateCommentsOnly').style.visibility = 'visible';
		document.getElementById('lblUpdateCommentsOnly').style.visibility = 'visible';
	}
	
	
	ctrl_visibilityAgents();
//	fareRuleCurrClick();
}

function selectAll(isOut) {
	if (isOut) {
		if (getFieldByID("chkOutAll").checked) {
			for ( var t = 0; t < depDays.length; t++) {
				setChkControls(depDays[t], true);
			}
		} else {
			for ( var t = 0; t < depDays.length; t++) {
				setChkControls(depDays[t], false);
			}
		}
	} else {
		if (getFieldByID("chkInAll").checked) {
			for ( var t = 0; t < arrDays.length; t++) {
				setChkControls(arrDays[t], true);
			}
		} else {
			for ( var t = 0; t < arrDays.length; t++) {
				setChkControls(arrDays[t], false);
			}
		}
	}
}

function ctrl_Search_click() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setPageEdited(false);
		setField("hdnRecNo", "1");
		setTabs();
		setField("hdnMode", "Search");
		document.forms["frmFareClasses"].action = "showFare.action";
		document.forms["frmFareClasses"].submit();
	}
}

function view_AgentWiseFR(){
	top[2].HidePageMessage();
	CWindowOpen(1);
}

function linkAgents(){
	top[2].HidePageMessage();
	CWindowOpen(0);
}

function viewAndOverwriteFee(){
	top[2].HidePageMessage();
	CWindowOpen(2);
}

function ctrl_Addclick() {
	top[2].HidePageMessage();
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setTabs();
		document.getElementById("frmFareClasses").reset();
		setData();
		updateClicked = false;
		rowNum = -1;
		agentCodes = "";
		data = "";
		Disable("chkOutAll", false);
		setField("chkOutAll", true);
		Disable("btnEdit", true);

		disableInputControls(false);
		Disable("txtConfirmMonths", true);
		Disable("txtConfirmDays", true);
		Disable("txtConfirmHours", true);
		Disable("txtConfirmMins", true);
		Disable("chkOpenRT", true);
		Disable("txtInbTmFrom", true);
		Disable("txtInbTmTo", true);
		Disable("txtAdvBookDays", true);
		Disable("txtAdvBookHours", true);
		Disable("txtAdvBookMins", true);
		status = "Add";
		getFieldByID("txtFc").focus();
		clearControls();
		Disable("btnDelete", true);
		Disable("chkInAll", true);
		setField("radFare", "OneWay");
		setField("selPaxCat", "A");
		setField("selFareCat", "N");
		createOnewayFlexiCodes();
		setField("selFlexiCode", "-1");
		
		Disable("selADCGTYP", false);
		Disable("selCHCGTYP", false);
		Disable("selINCGTYP", false);
		Disable("txtaInstructions", false);
		Disable("chkHRT", true);
		// set paxfare
		for ( var a = 0; a < arrDisableControls.length; a++) {
			Disable(arrDisableControls[a], false);
		}

		/*
		 * 0 - pax type 1 - description 2 - version 3 - amount 4 - fare amount 5 -
		 * apply fare 6 - mod 7 - CNX 8 - refundable 9 - xbe booking allowed 10 -
		 * age
		 */
		if (arrType[0][5] == "off") {
			disableAdultControls(true);
		} else {
			setChkControls("chkADRef", arrType[0][8]);
			setChkControls("chkADApp", arrType[0][5]);
			setChkControls("chkADMod", arrType[0][6]);
			setChkControls("chkADCNX", arrType[0][7]);
		}
		if (arrType[1][5] == "off") {
			disableChildControls(true);
		} else {
			setChkControls("chkCHRef", arrType[1][8]);
			setChkControls("chkCHApp", arrType[1][5]);
			setChkControls("chkCHMod", arrType[1][6]);
			setChkControls("chkCHCNX", arrType[1][7]);
		}
		if (arrType[2][5] == "off") {
			disableInfantControl(true);
		} else {
			setChkControls("chkINRef", arrType[2][8]);
			setChkControls("chkINApp", arrType[2][5]);
			setChkControls("chkINMod", arrType[2][6]);
			setChkControls("chkINCNX", arrType[2][7]);
		}

		setField("hdnPaxTypeAD", arrType[0][0]);
		setField("hdnPaxTypeCH", arrType[1][0]);
		setField("hdnPaxTypeIN", arrType[2][0]);
		setField("txtADNoShoCharge", arrType[0][13]);
		setField("txtCHNoShoCharge", arrType[1][13]);
		setField("txtINNoShoCharge", arrType[2][13]);

		for ( var t = 0; t < depDays.length; t++) {
			setChkControls(depDays[t], true);
		}
		ctrl_visibilityAgents();
		for ( var t = 0; t < arrDays.length; t++) {
			setChkControls(arrDays[t], false);
			Disable(arrDays[t], true);
		}
		var rest = getFieldByID("chkChrgRestrictions").checked;
		if (rest == true) {
			disableCnxModTxt(false);	
		} else {
			disableCnxModTxt(true);	
		}

		setField("txtOutTmTo", "23:59");
		setField("txtOutTmFrom", "00:00");

		disableReturn(true);
		Disable("txtInbTmFrom", false);
		Disable("txtInbTmTo", false);		
		setField("hdnMode", "Add");
		Disable("chkPrintExp", true);
		setField("chkStatus", true);
		getFieldByID("selFareVisibility").options[0].selected = true;
		setPageEdited(false);
		// child visibility
		if (chidVisibility == 'false') {
			Disable("chkINApp", true);
			disableInfantControl(true);
			Disable("chkCHApp", true);
			disableChildControls(true);
		}
		
		//fare rule modifications and load factor
		enableFareRuleModifications();
		fareRuleCurrClick();
	}
	setField("hdnMode", "SAVE");
	
	//Hide the chkUpdateCommentsOnly as it serves no purpose when adding a new record.
	document.getElementById('chkUpdateCommentsOnly').style.visibility = 'hidden';
	document.getElementById('lblUpdateCommentsOnly').style.visibility = 'hidden';
}

function chkBox_Clicked() {
	var arr = "";
	var dept = "";
	var arrStr = "";
	var deptStr = "";

	for ( var i = 0; i < arrDays.length; i++) {
		if (getText(arrDays[i]) == "on") {
			arr += arrDays[i] + ",";
			arrStr += "1,"
		} else {
			arrStr += "0,";
		}
		if (getText(depDays[i]) == "on") {
			dept += depDays[i] + ",";
			deptStr += "1,";
		} else {
			deptStr += "0,"
		}

	}
	setField("hdndeptDays", dept);
	setField("hdnarrDays", arr);
	setField("hdnDeptIntDays", deptStr);
	setField("hdnArrIntDays", arrStr);
}

function KPValidateDecimal(control) {
	var strCC = getText(control);
	var strLen = strCC.length;
	var blnVal = isLikeDecimal(strCC)
	if (!blnVal) {
		setField(control, strCC.substr(0, strLen - 1));
		getFieldByID(control).focus();
	}
}

function ctrl_Deleteclick() {
	var status = confirm(deleteMessage);
	if (status == true) {
		setField("hdnMode", "DELETE");
		document.forms["frmFareClasses"].submit();
	}
}

function ctrl_visibility_Select() {
	var intCount = getFieldByID("selFareVisibility").length;
	var blnSelected = getFieldByID("selFareVisibility").options[0].selected;

	for ( var i = 1; i < intCount; i++) {
		if (blnSelected) {
			getFieldByID("selFareVisibility").options[i].selected = false;
		}
	}
	required_changed();
}

function ctrl_visibilityAgents() {
	visibility = "";
	var visibility = getText("selFareVisibility");
	if (visibility != null && !visibility == "") {
		var upperVisible = visibility.toUpperCase();
		if (upperVisible.indexOf("AGENT") != -1
				|| upperVisible.indexOf("GSA") != -1) {
			Disable("btnLnkAgent", false);
		} else {
			Disable("btnLnkAgent", true);
		}
	}
}

// Reset Button Click
function resetClick() {
	var strMode = getText("hdnMode");
	setPageEdited(false);
	if (strMode == "Add") {
		top[2].HidePageMessage();
		resetFareRuleModifications();
		ctrl_Addclick();

	} else if (strMode == "Edit") {
		updateClicked = false;
		clearControls();
		getFieldByID("selFareVisibility").options[0].selected = false;
		agentCodes = "";
		top[2].HidePageMessage();
		document.getElementById("frmFareClasses").reset();
		disableInputControls("");
		setFields(strGridRow);
		ctrl_visibilityAgents();
		var rest = getFieldByID("chkChrgRestrictions").checked;
		if (rest == true) {
			disableCnxModTxt(false);	
		} else {
			disableCnxModTxt(true);	
		}
		if (!getFieldByID("chkAdvance").checked) {
			Disable("txtAdvBookDays", true);
			Disable("txtAdvBookHours", true);
			Disable("txtAdvBookMins", true);
		}

		Disable("txtFc", true);
		var checked = getText("radFare");
		
		if (checked != "Return") {
			disableReturn(true);
			Disable("chkPrintExp", true);
			for ( var t = 0; t < arrDays.length; t++) {
				Disable(arrDays[t], true);
			}
			Disable("chkInAll", true);
		}

	} else {
		resetToDefault();
	}
}

function resetToDefault() {
	top[2].HidePageMessage();
	clearControls();
	Disable("btnDelete", true);
	disableInputControls(true);	
	setField("radFare", "OneWay");
	
	for ( var t = 0; t < depDays.length; t++) {
		setChkControls(depDays[t], true);
	}
	
	for ( var t = 0; t < arrDays.length; t++) {
		setChkControls(arrDays[t], false);
		Disable(arrDays[t], true);
	}

	var rest = getFieldByID("chkChrgRestrictions").checked;
	if (rest == true) {
		disableCnxModTxt(false);
	} else {
		disableCnxModTxt(true);		
	}

	setField("txtOutTmTo", "23:59");
	setField("txtOutTmFrom", "00:00");
	setField("selPaxCat", "A");
	setField("selFareCat", "N");
	setField("selFlexiCode", "-1");
	disableReturn(true)
	setField("hdnMode", "Add");
	setField("chkStatus", true);
	getFieldByID("selFareVisibility").options[0].selected = true;
	Disable("txtAdvBookDays", false);
	Disable("txtAdvBookHours", false);
	Disable("txtAdvBookMins", false);
	Disable("chkPrintExp", true);
	resetFareRuleModifications();
}

function required_changed() {
	setPageEdited(true);

}

function setVisibilityForAll() {
	var strVisibility = getText("selFareVisibility");
	var strVisibile = "";

	if (strVisibility == "All") {
		var intCount = getFieldByID("selFareVisibility").length;
		var blnSelected = false;
		for ( var i = 1; i < intCount; i++) {
			strVisibile += getFieldByID("selFareVisibility").options[i].value
					+ ",";
		}
	}
	if (strVisibile.indexOf(",") != -1) {
		setField("hdnFareVisibilityIDs", strVisibile.substring(0, strVisibile
				.lastIndexOf(",")));
	} else {
		setField("hdnFareVisibilityIDs", strVisibile);
	}
}

function compareTime(outTmFrom, outTmTo) {
	if (outTmFrom == outTmTo) {
		return false;
	} else {
		return true;
	}
}

function KPValidateDecimel(objCon) {
	setPageEdited(true);
	var strText = objCon.value;
	var length = strText.length;
	var blnVal = isLikeDecimal(strText);
	var blnVal2 = currencyValidate(strText, 10, 2);
	var blnVal3 = isDecimal(strText);

	if (!blnVal) {
		customValidate(objCon, /[a-zA-Z`!@#$%\s&*?\[\]{}()|\\\/=:,;_^~]/g);
	} else if (!blnVal2) {
		customValidate(objCon, /[a-zA-Z`!@#$%\s&*?\[\]{}()|\\\/=:,;_^~]/g);
		objCon.focus();
	}
	
	if (!blnVal2) {
		if (strText.indexOf(".") != -1) {
			wholeNumber = strText.substr(0, strText.indexOf("."));
			if (wholeNumber.length > 13) {
				objCon.value= strText.substr(0, wholeNumber.length - 1);
			} else {
				objCon.value= strText.substr(0, length - 1);
			}
		} else {
			objCon.value= strText.substr(0, length - 1);
		}
		
	}
}

function valueChanged() {
	if (((getText("txtMaxStayMonths") != "") && (getText("txtMaxStayMonths") != 0))
			|| ((getText("txtMaxStayDays") != "") && (getText("txtMaxStayDays") != 0))
			|| ((getText("txtMaxStayHours") != "") && (getText("txtMaxStayHours") != 0))
			|| ((getText("txtMaxStayMins") != "") && (getText("txtMaxStayMins") != 0))) {
		Disable("chkPrintExp", false);

	} else {		
		if (getFieldByID("chkPrintExp").checked) {
			getFieldByID("chkPrintExp").checked = false;			
		}
		Disable("chkPrintExp", true);
	}
}

function disableCnxModTxt(cond){
	Disable("txtModification", cond);
	Disable("txtCancellation", cond);
	Disable("txtModificationInLocal", cond);	
	Disable("txtCancellationInLocal", cond);

	
	Disable("selDefineModType", cond);
	Disable("selDefineCanType", cond);
	if(getValue("selDefineModType") == selModeTyps.v){
		Disable("txtMin_mod", true);
		Disable("txtMax_mod", true);
	}else {
		Disable("txtMin_mod", cond);
		Disable("txtMax_mod", cond);
	}
	if(getValue("selDefineCanType") == selModeTyps.v){
		Disable("txtMin_canc", true);
		Disable("txtMax_canc", true);
	}else {
		Disable("txtMin_canc", cond);
		Disable("txtMax_canc", cond);
	}
}

function disableAdultControls(cond) {
	Disable("chkADRef", cond);
	Disable("chkADMod", cond);
	Disable("chkADCNX", cond);	
}

function disableChildControls(cond) {
	Disable("chkCHRef", true);
	Disable("chkCHCNX", true);
	Disable("chkCHMod", true);
}

function disableInfantControl(cond) {
	Disable("chkINRef", true);
	Disable("chkINMod", true);
	Disable("chkINCNX", true);
}

function setChkControls(control, cond) {
	var conValue = false;
	if(cond == true || cond == 'on' || cond == 'true')
		conValue = true;
	getFieldByID(control).checked = conValue;		
	
}

function EnableConfirmPeriod(obj){
	if(obj.checked){
		Disable("txtConfirmMonths", false);
		Disable("txtConfirmDays", false);
		Disable("txtConfirmHours", false);
		Disable("txtConfirmMins", false);
	}else{
		setField("txtConfirmMonths", "");
		setField("txtConfirmDays", "");
		setField("txtConfirmHours", "");
		setField("txtConfirmMins", "");
		Disable("txtConfirmMonths", true);
		Disable("txtConfirmDays", true);
		Disable("txtConfirmHours", true);
		Disable("txtConfirmMins", true);
	}
}

function disableAndClearMinMaxForMod (blnType){
	setField("txtMin_mod","");
	setField("txtMax_mod","");
	Disable("txtMin_mod", blnType);
	Disable("txtMax_mod", blnType);
}

function disableAndClearMinMaxCanc (blnType){
	setField("txtMin_canc","");
	setField("txtMax_canc","");
	Disable("txtMin_canc", blnType);
	Disable("txtMax_canc", blnType);
}

function clearModTypeOnClick (){
	var objRad1 = getFieldByID("radCurrFareRule");
	var objRad2 = getFieldByID("radSelCurrFareRule");	

	if(getValue("selDefineModType") != selModeTyps.v){	
		setField("txtModificationInLocal","0.00");
		getFieldByID("txtModification").readOnly = false;
		getFieldByID("txtModificationInLocal").readOnly = true;
		disableAndClearMinMaxForMod(false);
	}else {
		if (isSelectedCurrencyChecked()){
			setField("txtModification","0.00");	
			getFieldByID("txtModification").readOnly = true;
			getFieldByID("txtModificationInLocal").readOnly = false;
		}else{
			if(isSelectedCurrencyOptionAvailable()){
				setField("txtModificationInLocal","0.00");
				getFieldByID("txtModificationInLocal").readOnly = true;
			}
			getFieldByID("txtModification").readOnly = false;
		}
		disableAndClearMinMaxForMod(true);
	}
}

function clearCancTypeOnClick (){
	var objRad1 = getFieldByID("radCurrFareRule");
	var objRad2 = getFieldByID("radSelCurrFareRule");	

	if(getValue("selDefineCanType") != selModeTyps.v){
		setField("txtCancellationInLocal","0.00");
		getFieldByID("txtCancellation").readOnly = false;
		getFieldByID("txtCancellationInLocal").readOnly = true;
		disableAndClearMinMaxCanc(false);
	}else {
		if (isSelectedCurrencyChecked()){
			setField("txtCancellation","0.00");	
			getFieldByID("txtCancellation").readOnly = true;
			getFieldByID("txtCancellationInLocal").readOnly = false;
		}else{
			if(isSelectedCurrencyOptionAvailable()){
				setField("txtCancellationInLocal","0.00");
				getFieldByID("txtCancellationInLocal").readOnly = true;
			}
			getFieldByID("txtCancellation").readOnly = false;
		}
		disableAndClearMinMaxCanc(true);
	}	
}

//fare rule modifications and load factor
function disableFareRuleModifications(){
	Disable("loadFactorEnabled", true);
	Disable("loadFactorMin", true);
	Disable("loadFactorMax", true);
	Disable("modifyByDate", true);
	Disable("modifyByOND", true);
	Disable("fareDiscount", true);
	Disable("fareDiscountMin", true);
	Disable("fareDiscountMax", true);
}

function resetFareRuleModifications(){
	Disable("loadFactorEnabled", true);
	Disable("loadFactorMin", true);	
	Disable("loadFactorMax", true);
	Disable("modifyByDate", true);
	Disable("modifyByOND", true);
	Disable("fareDiscount", true);
	Disable("fareDiscountMin", true);
	Disable("fareDiscountMax", true);
	
	getFieldByID("modifyByDate").checked = true;
	getFieldByID("modifyByOND").checked = true;
	getFieldByID("loadFactorEnabled").checked = false;	
	setField("loadFactorMin", "");	
	setField("loadFactorMax", "");
	getFieldByID("fareDiscount").checked = false;	
	setField("fareDiscountMin", "");	
	setField("fareDiscountMax", "");
}

function enableDisableFareDiscount(){
	var fareDiscount = getText("fareDiscount");

	if (fareDiscountVisibiliy && fareDiscount == "Y") {
		Disable("fareDiscountMin", false);
		Disable("fareDiscountMax", false);
	} else {
		setField("fareDiscountMin", "");
		setField("fareDiscountMax", "");
		Disable("fareDiscountMin", true);
		Disable("fareDiscountMax", true);
	}
}

function loadFactorValidation(){
	var checked = getText("loadFactorEnabled");

	if (checked == "ON") {
		Disable("loadFactorMin", false);
		Disable("loadFactorMax", false);
	} else {
		setField("loadFactorMin", "");
		setField("loadFactorMax", "");
		Disable("loadFactorMin", true);
		Disable("loadFactorMax", true);
	}
}

function enableFareRuleModifications(){
	var checked = getText("loadFactorEnabled");

	if (checked == "ON") {
		Disable("loadFactorMin", false);
		Disable("loadFactorMax", false);
	} else {
		setField("loadFactorMin", "");
		setField("loadFactorMax", "");
		Disable("loadFactorMin", true);
		Disable("loadFactorMax", true);
	}
	
	Disable("loadFactorEnabled", false);	
	Disable("modifyByDate", false);
	Disable("modifyByOND", false);
	
	var fareDiscount = getText("fareDiscount");

	if (fareDiscount == "Y") {
		Disable("fareDiscountMin", false);
		Disable("fareDiscountMax", false);
	} else {
		setField("fareDiscountMin", "");
		setField("fareDiscountMax", "");
		Disable("fareDiscountMin", true);
		Disable("fareDiscountMax", true);
	}
	
	Disable("fareDiscount", false);	
}
//Integrating the No show Charge Types modification 
function setDisplayBoundaryAndBreakPoints(obj) {
	var id = obj.id;
	var chargeType = obj.value;
	
	var objRad1 = getFieldByID("radCurrFareRule");
	var objRad2 = getFieldByID("radSelCurrFareRule");	
		
	if(id == "selADCGTYP"){
		if(isNoShowChargeTypeNotV(chargeType)){
			Disable("txtADNoShoBreakPoint", false);
			Disable("txtADNoShoBoundary", false);
			if(chargeType != "V"){
				setField("txtADNoShoChargeInLocal","0.00");
				getFieldByID("txtADNoShoCharge").readOnly = false;
				getFieldByID("txtADNoShoChargeInLocal").readOnly = true;
			}else{
				if (isSelectedCurrencyChecked()){
					setField("txtCancellation","0.00");	
					getFieldByID("txtADNoShoCharge").readOnly = true;
					getFieldByID("txtCancellationInLocal").readOnly = false;
				}else{
					if(isSelectedCurrencyOptionAvailable()){
						setField("txtADNoShoChargeInLocal","0.00");
						getFieldByID("txtADNoShoChargeInLocal").readOnly = true;
					}					
					getFieldByID("txtADNoShoCharge").readOnly = false;					
				}
			}
		}else{
			setField("txtADNoShoBreakPoint", "");
			setField("txtADNoShoBoundary", "");
			Disable("txtADNoShoBreakPoint", true);
			Disable("txtADNoShoBoundary", true);
		}
	}else if(id == "selCHCGTYP"){
		if(isNoShowChargeTypeNotV(chargeType)){
			Disable("txtCHNoShoBreakPoint", false);
			Disable("txtCHNoShoBoundary", false);
			if(chargeType != "V"){
				setField("txtCHNoShoChargeInLocal","0.00");
				getFieldByID("txtCHNoShoCharge").readOnly = false;
				getFieldByID("txtCHNoShoChargeInLocal").readOnly = true;
			}else{
				if (isSelectedCurrencyChecked()){
					setField("txtCHNoShoCharge","0.00");	
					getFieldByID("txtCHNoShoCharge").readOnly = true;
					getFieldByID("txtCHNoShoChargeInLocal").readOnly = false;
				}else{
					if(isSelectedCurrencyOptionAvailable()){
						setField("txtCHNoShoChargeInLocal","0.00");
						getFieldByID("txtCHNoShoChargeInLocal").readOnly = true;
					}
					getFieldByID("txtCHNoShoCharge").readOnly = false;
				}
			}
		}else{
			setField("txtCHNoShoBreakPoint", "");
			setField("txtCHNoShoBoundary", "");
			Disable("txtCHNoShoBreakPoint", true);
			Disable("txtCHNoShoBoundary", true);
		}
	}else if(id == "selINCGTYP"){
		if(isNoShowChargeTypeNotV(chargeType)){
			Disable("txtINNoShoBreakPoint", false);
			Disable("txtINNoShoBoundary", false);
			
			if(chargeType != "V"){
				setField("txtINNoShoChargeInLocal","0.00");
				getFieldByID("txtINNoShoCharge").readOnly = false;
				getFieldByID("txtINNoShoChargeInLocal").readOnly = true;
			}else{
				if (isSelectedCurrencyChecked()){
					setField("txtINNoShoCharge","0.00");	
					getFieldByID("txtINNoShoCharge").readOnly = true;
					getFieldByID("txtINNoShoChargeInLocal").readOnly = false;
				}else{
					if(isSelectedCurrencyOptionAvailable()){
						setField("txtINNoShoChargeInLocal","0.00");
						getFieldByID("txtINNoShoChargeInLocal").readOnly = true;
					}
					getFieldByID("txtINNoShoCharge").readOnly = false;
				}
			}
		}else{
			setField("txtINNoShoBreakPoint", "");
			setField("txtINNoShoBoundary", "");
			Disable("txtINNoShoBreakPoint", true);
			Disable("txtINNoShoBoundary", true);
		}
	}
}

function isNoShowChargeTypeNotV(chargeType){
	if(trim(chargeType) == "PF" || trim(chargeType) == "PFS" || trim(chargeType) == "PTF"){
		return true;
	}else{
		return false;
	}
		

}

function disableNoShowChargeModelUIControls(blnDisable){
	Disable("selADCGTYP",blnDisable);
	Disable("selCHCGTYP",blnDisable);
	Disable("selINCGTYP",blnDisable);
	
	Disable("txtADNoShoBreakPoint",blnDisable);
	Disable("txtADNoShoBoundary",blnDisable);
	
	Disable("txtCHNoShoBreakPoint",blnDisable);
	Disable("txtCHNoShoBoundary",blnDisable);
	
	Disable("txtINNoShoBreakPoint",blnDisable);
	Disable("txtINNoShoBoundary",blnDisable);
}

function disableNoShowChargeModelUIControlsEditClick(){
	Disable("selADCGTYP",false);
	Disable("selCHCGTYP",false);
	Disable("selINCGTYP",false);
	if(isNoShowChargeTypeNotV(getValue("selADCGTYP"))){
		Disable("txtADNoShoBreakPoint",false);
		Disable("txtADNoShoBoundary",false);
	}else{
		Disable("txtADNoShoBreakPoint",true);
		Disable("txtADNoShoBoundary",true);
	}
	if(isNoShowChargeTypeNotV(getValue("selCHCGTYP"))){
		Disable("txtCHNoShoBreakPoint",false);
		Disable("txtCHNoShoBoundary",false);
	}else{
		Disable("txtCHNoShoBreakPoint",true);
		Disable("txtCHNoShoBoundary",true);
	}
	if(isNoShowChargeTypeNotV(getValue("selINCGTYP"))){
		Disable("txtINNoShoBreakPoint",false);
		Disable("txtINNoShoBoundary",false);
	}else{
		Disable("txtINNoShoBreakPoint",true);
		Disable("txtINNoShoBoundary",true);
	}
}

function allowOnlyPositiveInt(elementId){
	var validElement = removeChars(getText(elementId), '[^\\d]'); // remove all non digits
	setField(elementId, validElement);
}

function checkForAvoidableCharacters( textInput) {
	var strValue = textInput;
	var regAvoidChars = new RegExp(/[~,]/);
	
	if (regAvoidChars.exec(strValue)==null)
	{
		return false;
		
	}
	else
	{
		return true;
	}
}

function fareRuleCurrClick() {

	setVisible("selCurrencyCode", false);
	var objRad1 = getFieldByID("radCurrFareRule");
	var objRad2 = getFieldByID("radSelCurrFareRule");	

	//radSelCurrFareRule is only available in Mahan
	if (isSelectedCurrencyChecked()) {
//			setField("hdnLocalCurrencySelected", "true");
			setVisible("selCurrencyCode", true);
//			Disable("selCurrencyCode", false);

			if(getValue("selDefineCanType") == selModeTyps.v){
				setField("txtCancellation","0.00");	
				getFieldByID("txtCancellation").readOnly = true;
				getFieldByID("txtCancellationInLocal").readOnly = false;
			}else{
				getFieldByID("txtCancellation").readOnly = false;
				getFieldByID("txtCancellationInLocal").readOnly = true;
			}
			if(getValue("selDefineModType") == selModeTyps.v){
				setField("txtModification","0.00");				
				getFieldByID("txtModification").readOnly = true;	
				getFieldByID("txtModificationInLocal").readOnly = false;
			}else{
				getFieldByID("txtModification").readOnly = false;	
				getFieldByID("txtModificationInLocal").readOnly = true;
			}  
			if(getValue("selADCGTYP") == selModeTyps.v){
				setField("txtADNoShoCharge","0.00");	
				getFieldByID("txtADNoShoCharge").readOnly = true;
				getFieldByID("txtADNoShoChargeInLocal").readOnly = false;
			}else{
				getFieldByID("txtADNoShoCharge").readOnly = false;
				getFieldByID("txtADNoShoChargeInLocal").readOnly = true;
			}if(getValue("selCHCGTYP") == selModeTyps.v){
				setField("txtCHNoShoCharge","0.00");	
				getFieldByID("txtCHNoShoCharge").readOnly = true;
				getFieldByID("txtCHNoShoChargeInLocal").readOnly = false;
			}else{
				getFieldByID("txtCHNoShoCharge").readOnly = false;
				getFieldByID("txtCHNoShoChargeInLocal").readOnly = true;
			}if(getValue("selINCGTYP") == selModeTyps.v){
				setField("txtINNoShoCharge","0.00");	
				getFieldByID("txtINNoShoCharge").readOnly = true;
				getFieldByID("txtINNoShoChargeInLocal").readOnly = false;
			}else{
				getFieldByID("txtINNoShoCharge").readOnly = false;
				getFieldByID("txtINNoShoChargeInLocal").readOnly = true;
		} 
	}
	else if (objRad1.checked) {
//		setField("hdnLocalCurrencySelected", "false");
		setVisible("selCurrencyCode", false);
		setField("selCurrencyCode", "-1");	
		setField("txtModificationInLocal","0.00");
		setField("txtCancellationInLocal","0.00");	
		setField("txtADNoShoChargeInLocal","0.00");
		setField("txtCHNoShoChargeInLocal","0.00");
		setField("txtINNoShoChargeInLocal","0.00");
				
		if(isSelectedCurrencyOptionAvailable()){
			getFieldByID("txtADNoShoChargeInLocal").readOnly = true;
			getFieldByID("txtModificationInLocal").readOnly = true;
			getFieldByID("txtCancellationInLocal").readOnly = true;
			getFieldByID("txtCHNoShoChargeInLocal").readOnly = true;
			getFieldByID("txtINNoShoChargeInLocal").readOnly = true;	
		}
		getFieldByID("txtADNoShoCharge").readOnly = false;
		getFieldByID("txtModification").readOnly = false;
		getFieldByID("txtCancellation").readOnly = false;
		getFieldByID("txtCHNoShoCharge").readOnly = false;
		getFieldByID("txtINNoShoCharge").readOnly = false;		
	}
	var mode = getText("hdnMode")
	if(mode == "Add"){
//		disableLocalCurrencyOnSelection(0);
	}
	else{
		// disableLocalCurrencyOnSelection(1);
	}	

}

function updateCommentAndDescription()
{
	var boxChecked=getFieldByID("chkUpdateCommentsOnly").checked;
	makeInputsReadOnly(boxChecked);
	disableDropDownControls(boxChecked);
	getFieldByID("txtaRulesCmnts").readOnly=false;
	getFieldByID("txtaInstructions").readOnly=false;
	getFieldByID("txtDesc").readOnly=false;
}

function isSelectedCurrencyChecked(){
	if(isSelectedCurrencyOptionAvailable()){
		var selectedCurrency = getFieldByID("radSelCurrFareRule");
		if(selectedCurrency.checked){
			return true;
		}else{
			return false;
		}
	}
	return false;
}

function isSelectedCurrencyOptionAvailable(){
	var selectedCurrency = getFieldByID("radSelCurrFareRule");
	if(selectedCurrency !=null){
		return true;
	}
	return false;
}

//Function to make all the input fields in the local array readonly
function makeInputsReadOnly(readOnly)
{
	inputs = document.getElementsByTagName('input');
	for (index = 0; index < inputs.length; ++index) {
	    inputs[index].readOnly=readOnly;
	}
	//Disable Overwrite Fee button ass well.
	Disable("btnOverWriteRule",readOnly);
}
function disableDropDownControls(disable)
{
	inputs = document.getElementsByTagName('select');
	for (index = 0; index < inputs.length; ++index) {
	    inputs[index].disabled=disable;
	}
}