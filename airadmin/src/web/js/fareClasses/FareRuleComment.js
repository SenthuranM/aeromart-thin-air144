

function FareRuleComments(){};

jQuery(document).ready(function() { 
	
	showCommentsAndRulesTabIfEnabled();

	var grdArray = new Array();
	jQuery("#listFareRuleComment").jqGrid({
		url : '',
		datatype : "json",
		jsonReader : {
			root : "rows",
			page : "page",
			total : "total",
			records : "records",
			repeatitems : false,
			id : "0"
		},
		colNames : [ 'Language', 'Comment',  'Language Code', 'Fare Rule ID', 'Comment ID'],
		colModel : [ 
		            {name : 'language', index : 'language', width : 40, jsonmap : 'language'},
		            {name : 'comment', index : 'comment', width : 120, jsonmap : 'comment'},
		            {name : 'languageCode', index : 'languageCode', width : 40, jsonmap : 'languageCode', hidden:true},
		            {name : 'fareRuleID', index : 'fareRuleID', width : 40, jsonmap : 'fareRuleID', hidden:true},
		            {name : 'commentID', index : 'commentID', width : 40, jsonmap : 'commentID', hidden:true}
		            ],
		imgpath : '../../themes/default/images/jquery',
		multiselect : false,
		pager : jQuery('#Advertisementpager'),
		rowNum : 5,
		viewrecords : true,
		width : 900,
		loadui : 'block',
		onCellSelect : function(rowid, iCol, cellcontent) {

		},
		onSelectRow : function(rowid) {
			selectedRowId = rowid;
			var commentVal = $('#listFareRuleComment').jqGrid ('getCell', selectedRowId, 'comment');
			var languageCode = $('#listFareRuleComment').jqGrid ('getCell', selectedRowId, 'languageCode');
			document.getElementById('commentsLang').disabled = true;
			$("#selFareRuleLanguage").val(languageCode);
			$("#commentsLang").val(commentVal);
		},
		gridComplete : function() {

		},
		loadComplete : function() {

		}
	}).navGrid("#Advertisementpager", {
		refresh : true,
		edit : false,
		add : false,
		del : false,
		search : true
	});
	
	$("#btnAddFareRuleComment").click(function(){
		var mydata = null;
		var e = document.getElementById("selFareRuleLanguage");
		var selectedLanguage = e.options[e.selectedIndex].text;
		var selectedLanguageCode = e.options[e.selectedIndex].value;
		
		if (selectedLanguage != null && selectedLanguage != ''){
			if ($.trim($("#commentsLang").val()) != ''){
				if(isExist(selectedLanguage)){
					if (confirm('The selected language is already there. Do you want to update it ?')) {
						$("#listFareRuleComment").jqGrid('setCell', getExistingRowNumber(selectedLanguage), 'comment', $("#commentsLang").val());
						$("#commentsLang").val('');
					}
				}else{
					mydata = {language:selectedLanguage, comment:$("#commentsLang").val(), languageCode:selectedLanguageCode };
					var entryCount = $("#listFareRuleComment").getGridParam("records");
					$("#listFareRuleComment").addRowData(entryCount+1, mydata);
					$("#commentsLang").val('');
				}
			}else{
				alert('Comments can not be empty');
			}
		}else{
			alert('Please select a language');
		}
	});
	
	$("#btnEditFareRuleComment").click(function(){
		var row_id = $('#listFareRuleComment').getGridParam('selrow');
		
		if (row_id != null){
			document.getElementById('commentsLang').disabled = false;
		}else{
			alert('Please select an existing comment to edit');
		}
		
	});
	
	$("#btnRemoveFareRuleComment").click(function(){
		var row_id = $('#listFareRuleComment').getGridParam('selrow');
		
		if (row_id != null){
			$('#listFareRuleComment').jqGrid('delRowData',row_id);
			$("#commentsLang").val('');
		}else{
			alert('Please select an existing comment to remove');
		}
	});
	
});

FareRuleComments.loadExistingComments = function (commentList) {
	$("#listFareRuleComment").jqGrid("clearGridData");
	$.each(commentList, function(i,commentObj) {
		var mydata = null;
		mydata = {
			language : $("#selFareRuleLanguage option[value='"+ commentObj.languageCode + "']").text(),
			comment : commentObj.fareRuleComments,
			languageCode : commentObj.languageCode,
			fareRuleID : commentObj.fareRuleID,
			commentID : commentObj.fareRuleCommentID
		};
		var entryCount = $("#listFareRuleComment").getGridParam("records");
		$("#listFareRuleComment").addRowData(entryCount+1, mydata);
	});	
}

function isExist(text) {
	var flag = false;

	var rows = jQuery("#listFareRuleComment").getDataIDs();
	for (a = 0; a < rows.length; a++) {
		row = jQuery("#listFareRuleComment").getRowData(rows[a]);
		var language = row.language+'';
		
		if(language == text){
			flag = true;
		}
	}
	
	return flag;
}


function getExistingRowNumber(text) {
	var givenRowNum = -1;

	var rows = jQuery("#listFareRuleComment").getDataIDs();
	for (a = 0; a < rows.length; a++) {
		row = jQuery("#listFareRuleComment").getRowData(rows[a]);
		var language = row.language+'';
		
		if(language == text){
			givenRowNum = a+1;
		}
	}
	
	return givenRowNum;
}


function showCommentsAndRulesTabIfEnabled(){
	if (isMultilingualCommentEnabled){
		$("#tabFareRuleComments").show();
		$("#tabFareRuleCommentsTop").show();
		$("tr.rulesAndComments").hide();
	} else {
		$("#tabFareRuleComments").hide();
		$("#tabFareRuleCommentsTop").hide();
		$("tr.rulesAndComments").show();
	}
}
