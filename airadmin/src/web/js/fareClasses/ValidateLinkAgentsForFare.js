function update_Click() {
	document.getElementById("frmLinkAgents").reset();
	opener.agentCodes = ls.getselectedData();
	opener.updateClicked = true;
	window.close();

}

function winOnLoad() {

	if (opener.updateClicked == true) {
		if (opener.agentCodes != "") {
			ls.selectedData(String(opener.agentCodes));
		}
	} else {

		if (opener.data != "") {
			ls.selectedData(String(opener.data));
		}

	}

	if (opener.editRuleClicked || opener.addHocClicked) {
		Disable("btnUpdate", "");
	} else {
		Disable("btnUpdate", "true");

	}

}