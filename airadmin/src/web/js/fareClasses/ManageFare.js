var objWindow1;

function beforeUnload() {
	if ((objWindow1) && (!objWindow1.closed)) {
		objWindow1.close();
	}
}

function CWindowOpen(strIndex) {
	if ((objWindow1) && (!objWindow1.closed)) {
		objWindow1.close();
	}
	var intWidth = 0;
	var intHeight = 0;
	var strProp = '';

	var strFilename = ""
	switch (strIndex) {
	case 0:
		strFilename = "showLinkAgentForFare.action";
		intHeight = 400;
		intWidth = 600;
		break;
	case 2:
		var rqf = "creatFare";
		var fc = getValue("hdnFareID");		
		var version = getText("hdnExistingFareRuleVersion");
		
		var locCurrency = null;
		var currencyType = getValue("radCurrFareRule");
		var selCurrencyCode = getValue("selCurrencyCode");
		if(currencyType=="SEL_CURR" && selCurrencyCode!="-1"){
			locCurrency = selCurrencyCode;
		}
		
		strFilename = "showOverwriteFarerule.action?rqFrom="+rqf+"&frc="+fc+"&version="+version+"&locCurrency="+locCurrency;
		intHeight = 660;
		intWidth = 900;
		break;
	}
	strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width='
			+ intWidth
			+ ',height='
			+ intHeight
			+ ',resizable=no,top='
			+ ((window.screen.height - intHeight) / 2)
			+ ',left='
			+ (window.screen.width - intWidth) / 2;
	objWindow1 = window.open(strFilename, "CWindow1", strProp);
}

function beforeUnload() {
	if ((objWindow1) && (!objWindow1.closed)) {
		objWindow1.close();
	}
}