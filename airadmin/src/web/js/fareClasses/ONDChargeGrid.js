var objCol1 = new DGColumn();
objCol1.columnType = "label";
objCol1.width = "18%";
objCol1.arrayIndex = 1;
objCol1.toolTip = "OND";
objCol1.headerText = "OND";
objCol1.itemAlign = "left"
objCol1.sort = "true";

var objCol2 = new DGColumn();
objCol2.columnType = "label";
objCol2.width = "7%";
objCol2.arrayIndex = 2;
objCol2.toolTip = "Group";
objCol2.headerText = "Group";
objCol2.itemAlign = "left"

var objCol3 = new DGColumn();
objCol3.columnType = "label";
objCol3.width = "7%";
objCol3.arrayIndex = 3;
objCol3.toolTip = "Category";
objCol3.headerText = "Category";
objCol3.itemAlign = "left"

var objCol4 = new DGColumn();
objCol4.columnType = "label";
objCol4.width = "8%";
objCol4.arrayIndex = 4;
objCol4.toolTip = "Charge Code";
objCol4.headerText = "Charge<br>Code";
objCol4.itemAlign = "left"

var objCol5 = new DGColumn();
objCol5.columnType = "label";
objCol5.width = "14%";
objCol5.arrayIndex = 5;
objCol5.toolTip = "Charge Description";
objCol5.headerText = "Charge Description";
objCol5.itemAlign = "left"

var objCol6 = new DGColumn();
objCol6.columnType = "label";
objCol6.width = "6%";
objCol6.arrayIndex = 6;
objCol6.toolTip = "Y=Refundable Charge";
objCol6.headerText = "Ref<br> Y/N";
objCol6.itemAlign = "center"

var objCol7 = new DGColumn();
objCol7.columnType = "label";
objCol7.width = "8%";
objCol7.arrayIndex = 7;
objCol7.toolTip = "Y=Applicable to Infants";
objCol7.headerText = "App<br>Infants";
objCol7.itemAlign = "center"

var objCol8 = new DGColumn();
objCol8.columnType = "label";
objCol8.width = "8%";
objCol8.arrayIndex = 8;
objCol8.toolTip = "Valid From";
objCol8.headerText = "Valid From";
objCol8.itemAlign = "center"

var objCol9 = new DGColumn();
objCol9.columnType = "label";
objCol9.width = "8%";
objCol9.arrayIndex = 9;
objCol9.toolTip = "Valid To";
objCol9.headerText = "Valid To";
objCol9.itemAlign = "center"

var objCol10 = new DGColumn();
objCol10.columnType = "label";
objCol10.width = "4%";
objCol10.arrayIndex = 10;
objCol10.toolTip = "P=Percentage V=Value";
objCol10.headerText = "V/P Flag";
objCol10.itemAlign = "center"

var objCol11 = new DGColumn();
objCol11.columnType = "label";
objCol11.width = "9%";
objCol11.arrayIndex = 11;
objCol11.toolTip = "Percentage/Amount";
objCol11.headerText = "Amount";
objCol11.itemAlign = "right"

// ---------------- Grid
var objDG = new DataGrid("spnONDFare");
objDG.addColumn(objCol1);
objDG.addColumn(objCol2);
objDG.addColumn(objCol3);
objDG.addColumn(objCol4);
objDG.addColumn(objCol5);
objDG.addColumn(objCol6);
objDG.addColumn(objCol7);
objDG.addColumn(objCol8);
objDG.addColumn(objCol9);
objDG.addColumn(objCol10);
objDG.addColumn(objCol11);

alert(parent.totalChargeRecs);

objDG.width = "99%";
objDG.height = "79px";
objDG.headerBold = false;
objDG.seqStartNo = 1;
objDG.arrGridData = parent.arrFareChargeData;
objDG.seqNo = true;
objDG.pgnumRecTotal = parent.totalChargeRecs; // remove as per return record
												// size
objDG.paging = false;
var noDataFound = objDG.noDataFoundMsg;
objDG.noDataFoundMsg = "";
objDG.displayGrid();

function RecMsgFunction(intRecNo, intErrMsg) {
	top[2].HidePageMessage();
	if (intErrMsg != "") {
		top[2].objMsg.MessageText = intErrMsg;
		top[2].objMsg.MessageType = "Error";
		top[2].ShowPageMessage();
	}
}
