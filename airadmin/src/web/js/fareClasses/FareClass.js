var recNo = "1";
var strSearch = top[1].objTMenu.tabGetValue("SC_ADMN_008");
if (strSearch != null && strSearch != "") {
	var arrRecNo = strSearch.split("~");
	recNo = arrRecNo[1];
}

var objCol1 = new DGColumn();
objCol1.columnType = "label";
objCol1.width = "8%";
objCol1.arrayIndex = 1;
objCol1.toolTip = "Rule Code";
objCol1.headerText = "Rule Code";
objCol1.itemAlign = "left"
objCol1.sort = "true";

var objCol2 = new DGColumn();
objCol2.columnType = "label";
objCol2.width = "22%";
objCol2.arrayIndex = 2;
objCol2.toolTip = "Fare Rule Description";
objCol2.headerText = "Description";
objCol2.itemAlign = "left"

var objCol3 = new DGColumn();
objCol3.columnType = "label";
objCol3.width = "9%";
objCol3.arrayIndex = 3;
objCol3.toolTip = "Fare Basis Code";
objCol3.headerText = "Fare Basis<br> Code";
objCol3.itemAlign = "left"

var objCol4 = new DGColumn();
objCol4.columnType = "label";
objCol4.width = "7%";
objCol4.arrayIndex = 4;
objCol4.toolTip = "One Way/Return";
objCol4.headerText = "One Way/<br>Return";
objCol4.itemAlign = "left"

var objCol5 = new DGColumn();
objCol5.columnType = "label";
objCol5.width = "6%";
objCol5.arrayIndex = 6;
objCol5.toolTip = "Charge Restrictions";
objCol5.headerText = "Charge<br>Rest:";
objCol5.itemAlign = "center"

var objCol6 = new DGColumn();
objCol6.columnType = "label";
objCol6.width = "7%";
objCol6.arrayIndex = 8;
objCol6.toolTip = "Advance Booking";
objCol6.headerText = "Advance <br>Booking";
objCol6.itemAlign = "right"

var objCol7 = new DGColumn();
objCol7.columnType = "label";
objCol7.width = "15%";
objCol7.arrayIndex = 9;
objCol7.toolTip = "Valid Days of Week";
objCol7.headerText = "Valid Days of <br>Week";
objCol7.itemAlign = "left"

var objCol8 = new DGColumn();
objCol8.columnType = "label";
objCol8.width = "9%";
objCol8.arrayIndex = 11;
objCol8.toolTip = "Fare Visibility";
objCol8.headerText = "Visibility";
objCol8.itemAlign = "left"

var objCol9 = new DGColumn();
objCol9.columnType = "label";
objCol9.width = "4%";
objCol9.arrayIndex = 12;
objCol9.toolTip = "Agent Fare Rule";
objCol9.headerText = "Agt<br>FR";
objCol9.itemAlign = "center"

var objCol10 = new DGColumn();
objCol10.columnType = "label";
objCol10.width = "6%";
objCol10.arrayIndex = 13;
objCol10.toolTip = "Status";
objCol10.headerText = "Status";
objCol10.itemAlign = "left"

var objCol11 = new DGColumn();
objCol11.columnType = "label";
objCol11.width = "5%";
objCol11.arrayIndex = 21;
objCol11.toolTip = "Pax Category";
objCol11.headerText = "Pax<br>Cat.";
objCol11.itemAlign = "center";

// ---------------- Grid
var objDG = new DataGrid("spnBkgCodes");
objDG.addColumn(objCol1);
objDG.addColumn(objCol2);
objDG.addColumn(objCol3);
objDG.addColumn(objCol4);
objDG.addColumn(objCol5);
objDG.addColumn(objCol6);
objDG.addColumn(objCol7);
objDG.addColumn(objCol8);
objDG.addColumn(objCol9);
objDG.addColumn(objCol10);
objDG.addColumn(objCol11);
// objDG.addColumn(objCol12);

objDG.width = "99%";
objDG.height = "74px";
objDG.headerBold = false;
objDG.rowSelect = true;
objDG.arrGridData = arrData;
objDG.seqNo = true;
objDG.seqStartNo = recNo;
objDG.pgnumRecTotal = totalRes; // remove as per return record size
objDG.paging = true;
objDG.pgnumRecPage = 10;
objDG.rowOver = false;
objDG.pgonClick = "gridNavigations";
objDG.rowClick = "rowClick";
objDG.displayGrid();

function RecMsgFunction(intRecNo, intErrMsg) {
	top[2].HidePageMessage();
	if (intErrMsg != "") {
		top[2].objMsg.MessageText = intErrMsg;
		top[2].objMsg.MessageType = "Error";
		top[2].ShowPageMessage();
	}
}

