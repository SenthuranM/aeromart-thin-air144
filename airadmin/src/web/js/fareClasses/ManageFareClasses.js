/**
 * ManageFareClasses.js
 * 
 * @author Baladewa welathanthri
 *  
 */

function ManageFareClasses(){};

$(function(){
	ManageFareClasses.ready();
});

ManageFareClasses.rowEditFlag = false;
ManageFareClasses.objWindow = null;
ManageFareClasses.mode = "";
ManageFareClasses.FAREMODE = {SAVE:false, MODIFY:false, ADDHOCSAVE:false};
ManageFareClasses.visibleAgents = null;
ManageFareClasses.flexiList = [];
ManageFareClasses.selectedRow = null;
ManageFareClasses.gridDateCopy = null;
ManageFareClasses.fareDiscountDiplay = false;
ManageFareClasses.openReturnSupport=false;
ManageFareClasses.halfReturnSupport=false;
ManageFareClasses.enableFareInDepAirPCurr = false;
ManageFareClasses.domesticEnabled= false;
ManageFareClasses.prorationData=[];

ManageFareClasses.ready = function(){
	$(".currency-loacal, #tblFareDiscount, .selected-currency").hide();
	$(".open-return-sup").css("visibility", "hidden");
	$(".half-return-sup").css("visibility", "hidden");
	if (screeMode != "CREATEFARE"){
		CreateFareValidation.ready();
		ManageFareClasses.FAREMODE.SAVE = true;
		$("#newFareRow").show();
		$("#fareSearchRow").hide();
		$("#fareRuleOnly").hide();
		$("#btnAddFareRule").click(function(){ManageFareClasses.loadFareRule();});
		$("#btnAddHocRule").click(function(){ManageFareClasses.addHocRuleSetup();});
		$("#btnAddProration").click(function(){ManageFareClasses.addProration();});
		var fareID = $("#fareID").val();
		if (!isEmpty(fareID)) {
			ManageFareClasses.FAREMODE.MODIFY = true;
			var fareRuleCode = $("#selFC").val();
			if(isEmpty(fareRuleCode)){
				ManageFareClasses.FAREMODE.ADDHOCSAVE = true;
			}
		}
		//to hide the local currency
		ManageFareClasses.loadFarePageData();
		$("#btnBack").on("click", ManageFareClasses.goBack);
		$("#btnEditRule").on("click", function(){
			var x = confirm("You can not edit a Fare Rule attached to a Master Fare Rule Code. \nIt will be saved as an Ad-Hoc rule \nDo you want to continue ?")
			if (x){
				ManageFareClasses.mode = "EDIT";
				$("#selFC").val("");
				ManageFareClasses.editSelRow();
			}
			
		});
		$("#selBc").on("change", function(){
			//"N,N,Economy Class,ACT,COM,NORMAL,,A,A"
			var arrFields = this.value.split(",");
			var spnTmpCos = arrFields[2];
			var spnTmpFixed = '';
			var spnTmpStd = '';
			var spnPaxgoshow = '';
			var fixed = arrFields[1];
			var standard = arrFields[0];
			var paxGoshow = arrFields[6];
			var fareCat = arrFields[4];

			spnTmpStd = (arrFields[0] == "Y") ? "Standard" : "Non Standard";

			if (fixed == "Y") {
				spnTmpFixed = "Fixed";
			} else {
				spnTmpFixed = "Non Fixed";
			}
			
			if (fareCat == "RET" || fareCat == "COM") {
				$("#txtFromDtInbound").enable();
				$("#txtToDtInbound").enable();
			} else {
				$("#txtFromDtInbound").disable();
				$("#txtToDtInbound").disable();
				$("#txtFromDtInbound").val("");
				$("#txtToDtInbound").val("");
			}
			
			spnPaxgoshow = (trim(paxGoshow) == "GOSHOW" || trim(paxGoshow) == "INTERLINE") ? " / " + paxGoshow : "";

			var bcDetails =  spnTmpStd + " / " + spnTmpFixed + " / " + spnTmpCos + " / " + arrFields[5] + " / " + fareCat	+ spnPaxgoshow ;
			$("#spnFixed").text(bcDetails);
		})
	}else{		
		$("#btnEdit").click(function(){ManageFareClasses.mode = "EDIT"; ManageFareClasses.editSelRow();});
		$("#btnAddFare").click(function(){ManageFareClasses.mode = "ADD"; ManageFareClasses.addNewRule();});
		$("#btnViewAgntFC").click(function(){ManageFareClasses.CWindowOpen(1);});
		$("#newFareRow").hide();
		$("#fareRuleOnly").show();
		$("#fareSearchRow").show();
		$("#btnSearch").click(function(){ManageFareClasses.searchData();});
		$("#btnDelete").click(function(){ManageFareClasses.deleteFareRule(); });
		ManageFareClasses.loadPageInitialData();
		ManageFareClasses.loadGridDataProcess();
	}
	$("#btnSave").click(function(){
		if (ManageFareClasses.FAREMODE.SAVE) {
			ManageFareClasses.saveFare();			
		} else {
			ManageFareClasses.saveFareRule();
		}
	});
	$("input[name='radCurrFareRule']").on("click",ManageFareClasses.showHideSelectedCurrency);
	$("#btnReset").click(function(){ManageFareClasses.resetClick()});
	$("#btnClose").click(function(){ManageFareClasses.closeClick()});
	$("#chkMasterFare").click(ManageFareClasses.ckeckMasterFare);
	$("#chkLinkFare").click(ManageFareClasses.ckeckLinkFare);
		
}

ManageFareClasses.showHideSelectedCurrency = function(){
	if($("input[name='radCurrFareRule']:checked").attr("id")=="radSelCurrFareRule"){
		$("#selCurrencyCode").css("visibility", "visible");
		$(".baseCurr").val("0.00").prop("readonly", true);
		$(".selCurr").val("0.00").prop("readonly", false);
	}else{
		$("#selCurrencyCode").css("visibility", "hidden");
		$(".baseCurr").val("0.00").prop("readonly", false);
		$(".selCurr").val("0.00").prop("readonly", true);
	}
};

ManageFareClasses.ckeckMasterFare = function(e){
	if ($(e.target).prop("checked")){
		$("#masterFareRefCode").enable();
		$("#chkLinkFare, #btnLinkFares, #selectedMasterFareID, #linkFarePercentage").disable();
	}else{
		$("#masterFareRefCode").val("").enable();
		$("#chkLinkFare, #selectedMasterFareID, #linkFarePercentage").enable();
	}
	
};

ManageFareClasses.ckeckLinkFare = function(e){
	if ($(e.target).prop("checked")){
		$("#masterFareRefCode").val();
		$("#masterFareRefCode,#chkMasterFare").disable();
		$("#divDateOutbound,#divDateInbound,#divSalesDates,#divFareAmounts").hide();
	}else{
		$("#masterFareRefCode,#chkMasterFare").enable();
		$("#divDateOutbound,#divDateInbound,#divSalesDates,#divFareAmounts").show();
	}
	
};

ManageFareClasses.goBack = function(e){
	var delimiter = "^";
	var childScreenId = "SC_INVN_020"
	if ((top[1].objTMenu.tabIsAvailable(childScreenId) && top[1].objTMenu.tabRemove(childScreenId))			
			|| (!top[1].objTMenu.tabIsAvailable(childScreenId))) {		
		if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {	
			top[0].ruleStatus = "";
			var temp = getTabValues(screenId, delimiter, "strSearchCriteria");
			setTabValues(childScreenId, delimiter, "strSearchCriteria", temp);
			top[1].objTMenu.tabSetTabInfo(screenId, childScreenId, "showLoadFareJsp!showSetupFare.action", "Setup Fare", false);
			location.replace("showLoadFareJsp!showSetupFare.action");
			setPageEdited(false);
		}
	}
};

ManageFareClasses.getProrationData = function(){
	return ManageFareClasses.prorationData;
};
ManageFareClasses.UIChanges = function(){
	if (FareClassValidation.isLinkFareEnabled){
		$("#linkFareDiv").show();
	}
	if (ManageFareClasses.fareDiscountDiplay){
		$("#tblFareDiscount").show(); 
	}
	if(ManageFareClasses.openReturnSupport){
		$(".open-return-sup").css("visibility", "visible");
	}
	if(ManageFareClasses.halfReturnSupport){
		$(".half-return-sup").css("visibility", "visible");
	}
	if(ManageFareClasses.enableFareInDepAirPCurr){
		$(".currency-loacal").show();
		$(".selected-currency").show();
	}
	if (ManageFareClasses.domesticEnabled){
		$(".domestic-show").show();
	}
		
}

ManageFareClasses.loadPageInitialData = function() {
	//$("#tabs").hide();
	var url = "fareRule!createPageInitailData.action";
	var data = {};
	$.ajax({type: "POST", dataType: 'json',data: data , url:url, beforeSend: top[2].ShowProgress(),		
	success: ManageFareClasses.loadPageInitialDataProcess, error:ManageFareClasses.errorLoading, cache : false});
	return false;
}


ManageFareClasses.loadPageInitialDataProcess = function(response) {
	if (isEmpty(response)) {
		//Load Network connection failure error
		ManageFareClasses.errorLoading();
		return false;
	}
		
	if (response.success) {
		$("#selFareRuleCode").fillDropDown({dataArray:response.fareInfo.fareRuleList, keyIndex:0, valueIndex:1, firstEmpty:false});
		$("#visibility").fillDropDown({dataArray:response.fareInfo.fareVisibilityList, keyIndex:0, valueIndex:1, firstEmpty:false});
		$("#selFareCat").fillDropDown({dataArray:response.fareInfo.fareCategoryList, keyIndex:0, valueIndex:1, firstEmpty:false});
		ManageFareClasses.flexiList = response.fareInfo.flexiCategoryList;
		$("#selFlexiCode").empty();
		$("#selFlexiCode").fillDropDown({dataArray:filterFlexiList('OW'), keyIndex:0, valueIndex:1, firstEmpty:true});
		$("#selPaxCat").fillDropDown({dataArray:response.fareInfo.paxCategoryList, keyIndex:0, valueIndex:1, firstEmpty:false});		
		$("#selDefineModType, #selDefineCanType, #selADCGTYP, #selCHCGTYP, #selINCGTYP").fillDropDown({dataArray:response.fareInfo.fareModValueTypes, keyIndex:0, valueIndex:1, firstEmpty:false});
		FareClassValidation.errorInfo = response.errorInfo;
		FareClassValidation.milliBufferTime = {DOM:response.fareInfo.domesticModBufferTime, INT:response.fareInfo.internationalModBufferTime};
		FareClassValidation.isLinkFareEnabled = response.fareInfo.enableLinkFare;
		ManageFareClasses.enableFareInDepAirPCurr = response.fareInfo.enableFareInDepAirPCurr
		ManageFareClasses.fareDiscountDiplay = response.fareInfo.enableFareDiscount;
		ManageFareClasses.openReturnSupport = response.fareInfo.isOpenReturnSupport;
		ManageFareClasses.halfReturnSupport = response.fareInfo.isHalfReturnSupport;
		ManageFareClasses.domesticEnabled = response.fareInfo.enableDomesticBufferTime;
		ManageFareClasses.UIChanges();
	} else {
		ManageFareClasses.errorLoading(response.messageTxt);
	}
	top[2].HideProgress();
}

ManageFareClasses.loadFarePageData = function() {
	var url = "fare!createPageInitailData.action";
	var data = {};
	if (ManageFareClasses.FAREMODE.MODIFY) {
		data["fareID"] = $("#fareID").val();
	}
	$.ajax({type: "POST", dataType: 'json',data: data , url:url, beforeSend: top[2].ShowProgress(),		
	success: ManageFareClasses.loadFarePageInitialDataProcess, error:ManageFareClasses.errorLoading, cache : false});
	return false;
}

function filterFlexiList(flg){
	var t=[]
	for (var i = 0;i<ManageFareClasses.flexiList.length;i++){
		if (ManageFareClasses.flexiList[i][1].indexOf(flg)>-1){
			t[t.length] = ManageFareClasses.flexiList[i];
		}
	}
	return t;
}

ManageFareClasses.loadFarePageInitialDataProcess = function(response) {
	if (isEmpty(response)) {
		//Load Network connection failure error
		ManageFareClasses.errorLoading();
		return false;
	}
		
	if (response.success) {
		$("#selFC").fillDropDown({dataArray:response.fare.fareRuleList, keyIndex:0, valueIndex:1, firstEmpty:false});
		$("#visibility").fillDropDown({dataArray:response.fare.fareVisibilityList, keyIndex:0, valueIndex:1, firstEmpty:false});
		$("#selFareCat").fillDropDown({dataArray:response.fare.fareCategoryList, keyIndex:0, valueIndex:1, firstEmpty:false});
		ManageFareClasses.flexiList = response.fare.flexiCategoryList;
		$("#selFlexiCode").empty();
		$("#selFlexiCode").fillDropDown({dataArray:filterFlexiList('OW'), keyIndex:0, valueIndex:1, firstEmpty:true});
		$("#selPaxCat").fillDropDown({dataArray:response.fare.paxCategoryList, keyIndex:0, valueIndex:1, firstEmpty:false});		
		$("#selDefineModType, #selDefineCanType, #selADCGTYP, #selCHCGTYP, #selINCGTYP").fillDropDown({dataArray:response.fare.fareModValueTypes, keyIndex:0, valueIndex:1, firstEmpty:false});
		$("#selOrigin").fillDropDown({dataArray:response.fare.viaPointList, keyIndex:0, valueIndex:1, firstEmpty:false});
		$("#selDestination").fillDropDown({dataArray:response.fare.viaPointList, keyIndex:0, valueIndex:1, firstEmpty:false});
		$("#selVia1").fillDropDown({dataArray:response.fare.viaPointList, keyIndex:0, valueIndex:1, firstEmpty:false});
		$("#selVia2").fillDropDown({dataArray:response.fare.viaPointList, keyIndex:0, valueIndex:1, firstEmpty:false});
		$("#selVia3").fillDropDown({dataArray:response.fare.viaPointList, keyIndex:0, valueIndex:1, firstEmpty:false});
		$("#selVia4").fillDropDown({dataArray:response.fare.viaPointList, keyIndex:0, valueIndex:1, firstEmpty:false});
		$("#selBc").fillDropDown({dataArray:response.fare.bookingCodeList, keyIndex:0, valueIndex:1, firstEmpty:false});
		FareClassValidation.isLinkFareEnabled = response.fare.enableLinkFare;
		FareClassValidation.milliBufferTime = {DOM:response.fare.domBufferTimeInMilSec, INT:response.fare.bufTimeInMilSec};
		ManageFareClasses.enableFareInDepAirPCurr = response.fare.enableFareInDepAirPCurr
		ManageFareClasses.fareDiscountDiplay = response.fare.enableFareDiscount;
		ManageFareClasses.openReturnSupport = response.fare.isOpenReturnSupport;
		ManageFareClasses.halfReturnSupport = response.fare.isHalfReturnSupport;
		ManageFareClasses.domesticEnabled = response.fare.enableDomesticBufferTime;
		if (ManageFareClasses.FAREMODE.MODIFY) {
			ManageFareClasses.addFareRuleData(response.fareRuleData, null);
			ManageFareClasses.setFareRuleFormData(response.fareRuleData);
			ManageFareClasses.setFareAdd(response.fareTO);
		} else {
			var delimiter = "^";
			ManageFareClasses.setPaxConfigData(response.fare.farePaxConfig);
			var searchCri = getTabValues(screenId, delimiter, "strSearchCriteria");
			searchCri = eval("("+searchCri+")");
			if(!$.isEmptyObject(searchCri)){
				$("#selOrigin").val(searchCri.originAirport);
				$("#selDestination").val(searchCri.destinationAirport);
				$("#selVia1").val(searchCri.via1);
				$("#selVia2").val(searchCri.via2);
				$("#selVia3").val(searchCri.via3);
				$("#selVia4").val(searchCri.via4);
				$("#txtFBC").val(searchCri.fareBasisCode);
				if (searchCri.status=="ACT"){
					$("#chkStatus").prop("checked", true);
				}
				if (searchCri.bookingClassCode!=""){
					var dd = document.getElementById('selBc');
					dd.selectedIndex = 0;
					for (var i = 0; i < dd.options.length; i++) {
					    if (dd.options[i].text === searchCri.bookingClassCode) {
					        dd.selectedIndex = i;
					        break;
					    }
					}
					if (dd.selectedIndex!=0){
						$("#selBc").change();
					}
				}
			}
		}
		ManageFareClasses.UIChanges();
	} else {
		ManageFareClasses.errorLoading(response.messageTxt);
	}
	top[2].HideProgress();
}

ManageFareClasses.setPaxConfigData = function(paxConfigs) {
	if (paxConfigs != null) {
		for (var i = 0; i < paxConfigs.length; i++) {
			if (paxConfigs[i].paxTypeCode == 'AD') {
				$("#txtAED").val(paxConfigs[i].fareAmount);
			} else if (paxConfigs[i].paxTypeCode == 'CH') {
				$("#txtCHL").val(paxConfigs[i].fareAmount);
				$("#selCamt").val(paxConfigs[i].fareAmountType);
			} else if (paxConfigs[i].paxTypeCode == 'IN') {
				$("#txtINF").val(paxConfigs[i].fareAmount);
				$("#selIamt").val(paxConfigs[i].fareAmountType);
			}
		}		
	}
}


ManageFareClasses.loadFareRule = function() {
	var url = "fare!loadFareRule.action";
	var data = {};
	data['fareRuleID'] = $("#selFC").val();
	$.ajax({type: "POST", dataType: 'json',data: data , url:url,		
	success: ManageFareClasses.loadFareRuleProcess, error:ManageFareClasses.errorLoading, cache : false});
	return false;
}

ManageFareClasses.loadFareRuleProcess = function(response) {
	if (isEmpty(response)) {
		//Load Network connection failure error
		ManageFareClasses.errorLoading();
		return false;
	}
		
	if (response.success) {
		FareClassValidation.enableAll("NEW");
		ManageFareClasses.addFareRuleData(response.fareRuleData, null);
		ManageFareClasses.setFareRuleFormData(response.fareRuleData);
		FareClassValidation.disableAll();
		$("#btnEditRule").enable();
	} else {
		ManageFareClasses.errorLoading(response.messageTxt);
	}
	if (ManageFareClasses.FAREMODE.SAVE) {		
		$("#btnSave").enable();		
	}
}

ManageFareClasses.setFareRuleFormData = function(fare) {
	if (fare != null) {				
		$("#comments").val(fare.comments);	
		if(fare.minStayOver != null && fare.minStayOver != "0:0:0:0"){
			var minStay = fare.minStayOver.split(":");
			$("#minStayOver_MM").val(minStay[0]);
			$("#minStayOver_DD").val(minStay[1]);
			$("#minStayOver_HR").val(minStay[2]);
			$("#minStayOver_MI").val(minStay[3]);
		}
		if(fare.maxStayOver != null && fare.maxStayOver != "0:0:0:0"){
			var maxStay = fare.maxStayOver.split(":");
			$("#maxStayOver_MM").val(maxStay[0]);
			$("#maxStayOver_DD").val(maxStay[1]);
			$("#maxStayOver_HR").val(maxStay[2]);
			$("#maxStayOver_MI").val(maxStay[3]);
		}
		if(fare.ortConfirm != null && fare.ortConfirm != "0:0:0:0"){
			var ortCnf = fare.ortConfirm.split(":");
			$("#ortConfirm_MM").val(ortCnf[0]);
			$("#ortConfirm_DD").val(ortCnf[1]);
			$("#ortConfirm_HR").val(ortCnf[2]);
			$("#ortConfirm_MI").val(ortCnf[3]);
		}
			
		$("#selFareCat").val(fare.fareCat);
		$("#selPaxCat").val(fare.paxCat);
		$("#selFlexiCode").val(fare.flexiCode);
		$("#intnModBuffertime").val(fare.modBufferTimeInt);
		$("#domModBuffertime").val(fare.modBufferTimeDom);
		$("#txtFBC").val(fare.fareBasisCode);
	}
}

ManageFareClasses.setPageInitData = function() {
	$("select[name='visibility'] option[value='1']").attr("selected", true);
	$("#chkStatusActive").prop("checked", true);
}

ManageFareClasses.searchData = function() {	
	/*var newUrl = 'fareRuleSearch.action?';
 	newUrl += 'fareRuleCode=' + $("#selFareRuleCode").val();
 	newUrl += '&status=' + $("#selStatus").val();
 	
 	$("#fareRulePager").clearGridData();
 	$.getJSON(newUrl, function(response){ 
 		$("#fareRuleGrid")[0].addJSONData(response); 		
 	});*/
 	ManageFareClasses.loadGridDataProcess()
}

ManageFareClasses.errorLoading = function (response){
	alert(response);
};
ManageFareClasses.loadGridDataProcess = function(){
	var mydata = null;
	$("#fareRuleGridContainer")
	.empty()
	.append($('<table id="fareRuleGrid"></table>'),$('<div id="fareRulePager"></div>'));
	$("#fareRuleGrid").jqGrid({        
		//url:'fareRuleSearch.action',	
		//datatype: "local",
		datatype: function(postdata) {			
			postdata['fareRuleCode'] = $("#selFareRuleCode").val();
			postdata['status'] = $("#selStatus").val();				
	        jQuery.ajax({
	           url: 'fareRuleSearch.action',
	           data:postdata,
	           dataType:"json",
	           complete: function(jsonData,stat){	        	  
	              if(stat=="success") {
	            	 mydata = eval("("+jsonData.responseText+")");
	            	 ManageFareClasses.gridDateCopy = mydata.rows;
	            	 $("#fareRuleGrid")[0].addJSONData(mydata);            	
	              }
	           }
	        });
	    },
		//datatype: "json",
		colNames:['&nbsp;','Rule Code','Description', 'Fare Basis Code', 'One Way/Return','Charge Rest','Advance Booking',
		          'Valid Days of Week','Visibility','Agt FR','Status','Pax Cat.','fareid','validDaysOfTime',
		          'loadFactor','intnModBuffertime', 'domModBuffertime','ortConfirm','maxStayOver','minStayOver','flexiCode','fareCat',
		          'printExp','allowModDate','allowModOND','version','fareRuleID', 'comments'],
	   	colModel:[  {name:'id', width:30,label:"id"},
	   	            {name:'ruleCode',index:'ruleCode', width:55 , align:"left" },
	   	            {name:'description', index:'description', width:130 ,align:"left" },
	   	            {name:'fareBasisCode', width:80, align:"left" },
	   	            {name:'oneWayReturn', width:75, align:"center" },
	   	            {name:'chargeRest', width:80, align:"center" },
	   	            {name:'advanceBooking', width:70, align:"center" },		
	   	            {name:'validDaysOfWeek', width:150, align:"left" },
	   	            {name:'visibility', width:70, align:"center" },
	   	            {name:'agtFR', width:50, align:"center" },
	   	            {name:'status', width:70, align:"center" },
	   	            {name:'paxCat', width:50, align:"center" },
	   	            {name:'fareid', width:50, hidden:true },
	   	            {name:'validDaysOfTime', width:50, hidden:true},
	   	            {name:'loadFactor', width:50, hidden:true },
	   	            {name:'intnModBuffertime', width:50, hidden:true },
	   	            {name:'domModBuffertime', width:50, hidden:true },
	   	            {name:'ortConfirm', width:50, hidden:true },
	   	            {name:'maxStayOver', width:50, hidden:true },
	   	            {name:'minStayOver', width:50, hidden:true },
	   	            {name:'flexiCode', width:50, hidden:true },
	   	            {name:'fareCat', width:50, hidden:true },
	   	            {name:'printExp', width:50, hidden:true },
	   	            {name:'allowModDate', width:50, hidden:true },
	   	            {name:'allowModOND', width:50, hidden:true },
	   	            {name:'version', width:50, hidden:true },
	   	            {name:'fareRuleID', width:50, hidden:true },
	   	            {name:'comments', width:50, hidden:true }
	   	],
		height: 95,//height: 230 for full view
	   	rowNum:10,
	   	jsonReader: { 
			root: "rows",
		  	page: "page",
		  	total: "total",
		  	records: "records",
			repeatitems: false,
			id: "0" 
		},
	    //pager: '#fareRulePager',
	   	pager: jQuery('#fareRulePager'),
	   	sortname: 'id',
		multiselect: false,
		viewrecords: true,
	    sortorder: "desc",
	    caption:"Fare Rules",
	    loadui:'block',
	    onSelectRow: function(rowid){
	    	ManageFareClasses.selectedRow = rowid;
	    	ManageFareClasses.FillGridToForm(rowid, mydata.rows);
	    	//  });
	    }
	}).navGrid("#fareRulePager",{refresh: true, edit:false,add:false,del:false});	
	/*for(var i=0;i<=mydata.length;i++){
		jQuery("#fareRuleGrid").jqGrid('addRowData',i+1,mydata[i]);
	}*/
   
};


ManageFareClasses.editPage = function (){
	ManageFareClasses.rowEditFlag = true;
};

ManageFareClasses.editSelRow = function(){
	FareClassValidation.enableAll("EDIT");
};

ManageFareClasses.addNewRule = function(){
	if (ManageFareClasses.rowEditFlag){
		var x = confirm ("Changes will be lost! Do you wish to Continue?");
		if (x){
			ManageFareClasses.rowEditFlag = false;
		}
	}
	if (!ManageFareClasses.rowEditFlag){
		//$("#fareRuleGrid").parent().parent().animate({height:95},
	    	 // function() {
	    	//	$("#tabs").show();
				FareClassValidation.enableAll("NEW");
				ManageFareClasses.setPageInitData();
				FareClassValidation.disableEnable("radOneWay");
		//});
	}
};


ManageFareClasses.FillGridToForm = function(id,dataObj){
	if (id==null){
		ManageFareClasses.addHocRuleSetup();
	}else{
		if (ManageFareClasses.rowEditFlag){
			var x = confirm ("Changes will be lost! Do you wish to Continue?");
			if (x){
				ManageFareClasses.rowEditFlag = false;
			}
		}
		if (!ManageFareClasses.rowEditFlag){
			FareClassValidation.enableAll("NEW");
			var tempAppModel = jQuery.extend(true, {},
					ManageFareClasses.buildApplicableModel(dataObj[(getGridRowIndex(id)-1)].appModel));
			FareClassValidation.disableAll();
			FareClassValidation.screenInitilize();
			$("#fareRuleGrid").GridToForm(id, '#frmFareClasses');
			var rowData = jQuery("#fareRuleGrid").jqGrid('getRowData',id);
			ManageFareClasses.addFareRuleData(rowData,tempAppModel, id, dataObj)
		}
	}
};

ManageFareClasses.addFareRuleData = function(rowData, appModel,id, dataObj) {	
	if (rowData.oneWayReturn == "One Way"){
		$("#radOneWay").attr("checked", true);
		$("#selFlexiCode").empty();
		$("#selFlexiCode").fillDropDown({dataArray:filterFlexiList('OW'), keyIndex:0, valueIndex:1, firstEmpty:true});
	}else if (rowData.oneWayReturn == "Return"){
		$("#radReturn").attr("checked", true);
		$("#selFlexiCode").empty();
		$("#selFlexiCode").fillDropDown({dataArray:filterFlexiList('RT'), keyIndex:0, valueIndex:1, firstEmpty:true});
	}else{
		$("#radOpReturn").attr("checked", true);
		$("#selFlexiCode").empty();
		$("#selFlexiCode").fillDropDown({dataArray:filterFlexiList('RT'), keyIndex:0, valueIndex:1, firstEmpty:true});
	}
	
	if (rowData.status == "Active"){
		$("#chkStatusActive").attr("checked", true);
	}else{
		$("#chkStatusActive").attr("checked", false);
	}
	fillInOutDaysofWeek = function (value){
		$("#outBoundType").find("input[type='checkbox']").attr("checked", false);
		$("#inBoundType").find("input[type='checkbox']").attr("checked", false);
		var value = value.replace("<br>","|");
		var inWay = "";
		var outWay = "";
		if (value.indexOf("|") > -1){
			ways = value.split("|");
			inWay = ways[0];
			outWay = ways[1];
		}
		if (inWay != ""){
			if (inWay.indexOf("-") > -1){
				var temp = inWay.split("-");
				if (temp[0] == "Out"){
					if (temp[1].indexOf("Sa") != -1) {
						$("#chkOutSat").attr("checked", true);
					}
					if (temp[1].indexOf("Mo") != -1) {
						$("#chkOutMon").attr("checked", true);
					}
					if (temp[1].indexOf("Tu") != -1) {
						$("#chkOutTues").attr("checked", true);
					}
					if (temp[1].indexOf("We") != -1) {
						$("#chkOutWed").attr("checked", true);
					}
					if (temp[1].indexOf("Fr") != -1) {
						$("#chkOutFri").attr("checked", true);
					}
					if (temp[1].indexOf("Th") != -1) {
						$("#chkOutThu").attr("checked", true);
					}
					if (temp[1].indexOf("Su") != -1) {
						$("#chkOutSun").attr("checked", true);
					}
					if (temp[1].indexOf(":") != -1) {
						$("#outBoundFrom").val(temp[1]);
						$("#outBoundTo").val(temp[2]);
					}
				}
			}
		}
		if (outWay != ""){
			if (outWay.indexOf("-") > -1){
				var temp = outWay.split("-");
				if(temp[0] == "In"){
					if (temp[1].indexOf("Sa") != -1) {				
						$("#chkInbSat").attr("checked", true);
					}
					if (temp[1].indexOf("Mo") != -1) {				
						$("#chkInbMon").attr("checked", true);
					}
					if (temp[1].indexOf("Tu") != -1) {
						$("#chkInbTues").attr("checked", true);
					}
					if (temp[1].indexOf("We") != -1) {				
						$("#chkInbWed").attr("checked", true);
					}
					if (temp[1].indexOf("Fr") != -1) {
						$("#chkInbFri").attr("checked", true);
					}
					if (temp[1].indexOf("Su") != -1) {
						$("#chkInbSun").attr("checked", true);
					}
					if (temp[1].indexOf("Th") != -1) {
						$("#chkInbThu").attr("checked", true);
					}
					if (temp[1].indexOf(":") != -1) {
						$("#inBoundFrom").val(temp[1]);
						$("#inBoundTo").val(temp[2]);
					}
				}
			}
		}
	};
	if (rowData.allowModDate == "Y"){
		$("#modifyByDate").attr("checked", true);
	}else{
		$("#modifyByDate").attr("checked", false);
	}
	if (rowData.allowModOND == "Y"){
		$("#modifyByOND").attr("checked", true);
	}else{
		$("#modifyByOND").attr("checked", false);
	}
	if (rowData.printExp == "true"){
		$("#chkPrintExp").attr("checked", true);
	}else{
		$("#chkPrintExp").attr("checked", false);
	}
	fillInOutDaysofWeek(rowData.validDaysOfTime);
	fillInOutDaysofWeek(rowData.validDaysOfWeek);
	//Charge Restriction
	if (rowData.chargeRest=="Y"){
		$("#chkChrgRestrictions").prop("checked", true);
	}
	var fare = null;
	//appModel is null for the manage fare 
	if (appModel == null) {
		FareClassValidation.fareAppModel = ManageFareClasses.buildApplicableModel(rowData.appModel);
		fare = rowData;		
	} else {
		fare = dataObj[(getGridRowIndex(id)-1)];
		FareClassValidation.fareAppModel = appModel;		
	}
	$("#selFlexiCode").val( fare.flexiCode )
	FareClassValidation.fareCNXPeirodModel = fare.appModelCNXData;
	FareClassValidation.fareMODPeirodModel = fare.appModelMODData;
	
	var visibility = fare.visibilityCodes.split(",");		
	for(var i = 0; i < visibility.length; i++ ) {			
		$("select[name='visibility'] option[value='" + visibility[i] + "']").attr("selected", true);
	}
	var visibleAgents = fare.visibleAgents;
	FareClassValidation.agents = visibleAgents;
	if (visibleAgents != null && visibleAgents.length > 0) {		
		FareClassValidation.setFareVisibility($("#visibility"));
		if (FareClassValidation.isLoadAgents) {
			$.fn.multiSelect.loadRight(visibleAgents, 'lstAgents');
			ManageFareClasses.visibleAgents = null;			
		} else {
			ManageFareClasses.visibleAgents = visibleAgents; 
		}
	} else {
		//$.fn.multiSelect.allLeft();
	} 
	
	if(fare.advanceBooking != null && fare.advanceBooking != "0:0:0"){
		$("#chkAdvance").prop("checked", true);
		var advBkDays = fare.advanceBooking.split(":");
		$("#advanceBookingDays").val(advBkDays[0]);
		$("#advanceBookingHrs").val(advBkDays[1]);
		$("#advanceBookingMins").val(advBkDays[2]);
	}else{
		$("#advanceBookingDays").val(0);
		$("#advanceBookingHrs").val(0);
		$("#advanceBookingMins").val(0);
	}
	
	if(fare.minStayOver != null && fare.minStayOver != "0:0:0:0"){
		var minStay = fare.minStayOver.split(":");
		$("#minStayOver_MM").val(minStay[0]);
		$("#minStayOver_DD").val(minStay[1]);
		$("#minStayOver_HR").val(minStay[2]);
		$("#minStayOver_MI").val(minStay[3]);
	}
	if(fare.maxStayOver != null && fare.maxStayOver != "0:0:0:0"){
		var maxStay = fare.maxStayOver.split(":");
		$("#maxStayOver_MM").val(maxStay[0]);
		$("#maxStayOver_DD").val(maxStay[1]);
		$("#maxStayOver_HR").val(maxStay[2]);
		$("#maxStayOver_MI").val(maxStay[3]);
	}
	if(fare.ortConfirm != null && fare.ortConfirm != "0:0:0:0"){
		var ortCnf = fare.ortConfirm.split(":");
		$("#ortConfirm_MM").val(ortCnf[0]);
		$("#ortConfirm_DD").val(ortCnf[1]);
		$("#ortConfirm_HR").val(ortCnf[2]);
		$("#ortConfirm_MI").val(ortCnf[3]);
	}
	
	if (fare.validDaysOfTime != null) {
		var arrTime = fare.validDaysOfTime.split(",");
		if (arrTime.length > 2) {
			$("#inBoundFrom").val(arrTime[2]);
			$("#inBoundTo").val(arrTime[3]);					
		} 
		if (arrTime.length >= 2){
			$("#outBoundFrom").val(arrTime[0]);
			$("#outBoundTo").val(arrTime[1]);	
		}
	}
	if (!isEmpty(fare.loadFactor)) {
		$("#loadFactorEnabled").check();
		var  loadData = fare.loadFactor.split("-");
		$("#loadFactorMin").val(loadData[0]);
		$("#loadFactorMax").val(loadData[1]);
	}
	
	FareClassValidation.applyFareModelToCheckBox();
	//set chnages fields
	$("#txtModification").val(fare.modificationChargeAmount!=null?fare.modificationChargeAmount:"0.00");
	$("#txtCancellation").val(fare.cancellationChargeAmount!=null?fare.cancellationChargeAmount:"0.00");
	$("#txtMin_mod").val(fare.minModChargeAmount!=null?fare.minModChargeAmount:"");
	$("#txtMax_mod").val(fare.maxModChargeAmount!=null?fare.maxModChargeAmount:"");
	$("#selDefineModType").val(fare.modificationChargeType);
	$("#selDefineCanType").val(fare.cancellationChargeType);
	$("#txtCancellationInLocal").val(fare.cancellationChargeLocal!=null?fare.cancellationChargeLocal:"0.00");
	$("#txtModificationInLocal").val(fare.modificationChargeLocal!=null?fare.modificationChargeLocal:"0.00");
	$("#txtMin_canc").val(fare.minCnxChargeAmount!=null?fare.minCnxChargeAmount:"");
	$("#txtMax_canc").val(fare.maxCnxChargeAmount!=null?fare.maxCnxChargeAmount:"");
	$("#hdnRuleCode").val(fare.ruleCode);
	$("#hdnRuleId").val(fare.fareid);
	$("#selCurrencyCode").val("");
	if (fare.localCurrencyCode!=null){
		$("#selCurrencyCode").val(fare.localCurrencyCode);
		$("#radCurrFareRule").val("SEL_CURR");
	}
	$("#txtaInstructions").val(fare.agentInstructions!=null?fare.agentInstructions:"");
	$("#hdnVersion").val(fare.version);
	if (fare.appModel.length!=0){
		$.each(fare.appModel, function(){
			$("#sel"+this.paxType+"CGTYP").val(this.noshowBasis);
			$("#txt"+this.paxType+"NoShoCharge").val(this.noshow!=null?this.noshow:"0.00") ;
			$("#txt"+this.paxType+"NoShoChargeInLocal").val(this.noshowLocal!=null?this.noshowLocal:"0.00");
			$("#txt"+this.paxType+"NoShoBreakPoint").val(this.noshowBreak!=null?this.noshowBreak:"");
			$("#txt"+this.paxType+"NoShoBoundary").val(this.noshowBoundry!=null?this.noshowBoundry:"");
		})
	}
	
	
	
}

ManageFareClasses.buildApplicableModel = function(appModel) {
	var model = FareClassValidation.fareAppModel;
	for (var i = 0; i < appModel.length; i++) {
		if (appModel[i].paxType == "AD") {
			model[0].checked = appModel[i].fare;
			model[3].checked = appModel[i].ref;
			model[6].checked = appModel[i].mod;
			model[9].checked = appModel[i].cnx;
			model[12].checked = appModel[i].noshow;	
			$("#versionAD").val(appModel[i].paxVersion);
			$("#paxIDAD").val(appModel[i].paxID);
		} else if (appModel[i].paxType == "CH") {
			model[1].checked = appModel[i].fare;
			model[4].checked = appModel[i].ref;	
			model[7].checked = appModel[i].mod;	
			model[10].checked = appModel[i].cnx;
			model[13].checked = appModel[i].noshow;	
			$("#versionCH").val(appModel[i].paxVersion);
			$("#paxIDCH").val(appModel[i].paxID);
		}
		else if (appModel[i].paxType == "IN") {
			model[2].checked = appModel[i].fare;
			model[5].checked = appModel[i].ref;
			model[8].checked = appModel[i].mod;
			model[11].checked = appModel[i].cnx;	
			model[14].checked = appModel[i].noshow;	
			$("#versionIN").val(appModel[i].paxVersion);
			$("#paxIDIN").val(appModel[i].paxID);
		}
	}
	return model;
}

ManageFareClasses.saveFareRule = function(){
	if (FareClassValidation.befoarSaveData()){
		var url = "fareRule!saveFareRule.action";
		var data = {};
		data = ManageFareClasses.createFareRuleSubmitData(data);
		$.ajax({type: "POST", dataType: 'json',data: data , url:url,		
		success: ManageFareClasses.saveFareRuleProcess, error:ManageFareClasses.errorLoading, cache : false});
		return false;		
	}
}

ManageFareClasses.saveFareRuleProcess = function(response) {
	if (isEmpty(response)) {
		//Load Network connection failure error
		ManageFareClasses.errorLoading();
		return false;
	}
		
	if (response.success) {
		alert("Record Successfully saved!");
		window.location.replace(window.location.pathname+"?mode="+screeMode);
	} else {
		ManageFareClasses.errorLoading(response.messageTxt);
	}
}

ManageFareClasses.saveFare = function() {
	var url = "fare!saveFare.action";
	var data = ManageFareClasses.createFareSubmitData();
	$.ajax({type: "POST", dataType: 'json',data: data , url:url,		
	success: ManageFareClasses.saveFareProcess, error:ManageFareClasses.errorLoading, cache : false});
	return false;	
}

ManageFareClasses.saveFareProcess = function(response) {
	if (isEmpty(response)) {
		//Load Network connection failure error
		ManageFareClasses.errorLoading();
		return false;
	}
		
	if (response.success) {
		alert("Record Successfully saved!");
		window.location.replace(window.location.pathname+"?mode="+screeMode);	
	} else {
		ManageFareClasses.errorLoading(response.messageTxt);
	}
}

ManageFareClasses.deleteFareRule = function() {
	if (confirm("Selected record will be Deleted. Do you wish to Continue?")) {
		var url = "fareRule!deleteFareRule.action";
		var data = {};
		data['fareRule.fareRuleID'] = $("#fareRuleID").val();	
		$.ajax({type: "POST", dataType: 'json',data: data , url:url,		
		success: ManageFareClasses.deleteFareRuleProcess, error:ManageFareClasses.errorLoading, cache : false});
		return false;	
	}
}

ManageFareClasses.deleteFareRuleProcess = function(response) {
	if (isEmpty(response)) {
		//Load Network connection failure error
		ManageFareClasses.errorLoading();
		return false;
	}
		
	if (response.success) {
		alert("Record Successfully deleted!");
		top[2].objMsg.MessageText = response.messageTxt;
		top[2].objMsg.MessageType = "Success";
		top[2].ShowPageMessage();
		ManageFareClasses.searchData();
	} else {
		ManageFareClasses.errorLoading(response.messageTxt);
	}
}

ManageFareClasses.closeClick = function(){
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setPageEdited(false);
		top[1].objTMenu.tabRemove(screenId);
	}
}

ManageFareClasses.resetClick = function(){
	 ManageFareClasses.FillGridToForm(ManageFareClasses.selectedRow, ManageFareClasses.gridDateCopy);
}

ManageFareClasses.createFareSubmitData =  function() {
	var data = {};
	var activeStatus = 'INA';
	if (ManageFareClasses.FAREMODE.MODIFY) {
		data['fareTO.fareID'] =  $("#fareID").val();
		data['fareTO.version'] =  $("#versionFare").val();
	}
	data['fareTO.fareRuleID'] = $("#selFC").val(); 
	data['fareTO.fareRuleCode'] = $("#selFC option:selected").text();
	data['fareTO.effectiveFromDate'] = $("#txtFromDt").val();
	data['fareTO.effectiveToDate'] = $("#txtToDt").val();
	if (parseInt($("#txtAED").val(),10) == 0 ){
		x = confirm("Base Fare Amount is Zero. Do you wish to continue?");
		if (!x){
			return;
		}
	}
	data['fareTO.fareAmount'] = $("#txtAED").val();	
	if (isChecked('chkStatus')) {
		activeStatus = 'ACT';
	}
	data['fareTO.status'] = activeStatus;
	data['fareTO.ondCode'] = $("#selOrigin").val() + "/" + $("#selDestination").val() ;
	data['fareTO.bookingCode'] = $("#selBc option:selected").text(); 
	data['fareTO.childFareType'] = $("#selCamt").val();
	data['fareTO.childFare'] = $("#txtCHL").val();
	data['fareTO.infantFareType'] = $("#selIamt").val();
	data['fareTO.infantFare'] = $("#txtINF").val();
	data['fareTO.salesEffectiveFrom'] = $("#txtSlsStDate").val();
	data['fareTO.salesEffectiveTo'] = $("#txtSlsEnDate").val();
	data['fareTO.returnEffectiveFromDate'] = $("#txtFromDtInbound").val();
	data['fareTO.returnEffectiveToDate'] = $("#txtToDtInbound").val();
	data['fareTO.via1'] = $("#selVia1").val();
	data['fareTO.via2'] = $("#selVia2").val();
	data['fareTO.via3'] = $("#selVia3").val();
	data['fareTO.via4'] = $("#selVia4").val();
	if (isChecked('chkAllowReturn')) {
		data['fareTO.createReturnFare'] = "Y";
	} else {
		data['fareTO.createReturnFare'] = "N";
	}
	// data['fareTo.prorations']=ManageFareClasses.prorationData;
	 var gridJson = [	{ prorationId: 1, segmentCode: "A/B", proration: "30" },
	       				{ prorationId: 2, segmentCode: "B/C", proration: "40"}];
	
	 data['prorationData']=JSON.stringify(ManageFareClasses.prorationData, null);
	 
	//ADD Fare Rule Data if ADHoc Rule
	if (ManageFareClasses.FAREMODE.ADDHOCSAVE) {
		data = ManageFareClasses.createFareRuleSubmitData(data);
	}
	return data;
}

ManageFareClasses.createFareRuleSubmitData =  function(data) {	
	data['mode'] = ManageFareClasses.mode; 
	data['fareRule.fareRuleCode'] = $("#ruleCode").val();
	if (ManageFareClasses.mode == "EDIT") {
		data['fareRule.version'] = $("#version").val();
		data['fareRule.fareRuleID'] = $("#fareRuleID").val();
		data['versionAD'] = $("#versionAD").val();
		data['versionCH'] = $("#versionCH").val();
		data['versionIN'] = $("#versionIN").val();
		data['paxIDAD'] = $("#paxIDAD").val();
		data['paxIDCH'] = $("#paxIDCH").val();
		data['paxIDIN'] = $("#paxIDIN").val();		
	}
	data['fareRule.description'] = $("#description").val();
	var activeStatus = 'INA';
	if (isChecked('chkStatusActive')) {
		activeStatus = 'ACT';
	}
	data['fareRule.status'] = activeStatus;
	if (isChecked('chkAdvance')) {
		if($("#advanceBookingDays").val() != "" || $("#advanceBookingHrs").val() != "" || $("#advanceBookingMins").val() != ""){
			data['fareRule.advanceBookingDays'] = $("#advanceBookingDays").val() + ":" + $("#advanceBookingHrs").val() + ":" + $("#advanceBookingMins").val();
		}
	}	
	var isReturn = 'N';
	if (isChecked('radReturn')) {
		isReturn = 'Y';
	}
	data['fareRule.returnFlag'] = isReturn;
	if($("#minStayOver_MM").val() != "" || $("#minStayOver_DD").val() != "" || $("#minStayOver_HR").val() != "" || $("#minStayOver_MI").val() != ""){
		data['fareRule.minimumStayOver'] = $("#minStayOver_MM").val() + ":" + $("#minStayOver_DD").val() + ":" + $("#minStayOver_HR").val() + ":" + $("#minStayOver_MI").val();
	}
	if($("#maxStayOver_MM").val() != "" || $("#maxStayOver_DD").val() != "" || $("#maxStayOver_HR").val() != "" || $("#maxStayOver_MI").val() != ""){
		data['fareRule.maximumStayOver'] = $("#maxStayOver_MM").val() + ":" + $("#maxStayOver_DD").val() + ":" + $("#maxStayOver_HR").val() + ":" + $("#maxStayOver_MI").val();
	}	
	
	data['fareRule.outboundDepatureFreqencyString'] = createDelimitedValues(['chkOutMon', 'chkOutTues', 'chkOutWed', 'chkOutThu', 'chkOutFri', 'chkOutSat', 'chkOutSun']);
	data['fareRule.inboundDepatureFrequencyString'] = createDelimitedValues(['chkInbMon', 'chkInbTues', 'chkInbWed', 'chkInbThu', 'chkInbFri', 'chkInbSat', 'chkInbSun']);
	data['fareRule.outboundDepatureTimeFrom'] = $("#outBoundFrom").val();
	data['fareRule.outboundDepatureTimeTo'] = $("#outBoundTo").val();
	data['fareRule.inboundDepatureTimeFrom'] = $("#inBoundFrom").val();
	data['fareRule.inboundDepatureTimeTo'] = $("#inBoundTo").val();
	data['fareRule.rulesComments'] = $("#comments").val();
	data['fareRule.fareBasisCode'] = $("#fareBasisCode").val();
	data['fareRule.paxCategoryCode'] = $("#selPaxCat").val();
	data['fareRule.fareCategoryCode'] = $("#selFareCat").val();
	data['fareRule.intnModifyBufferTime'] = $("#intnModBuffertime").val();
	data['fareRule.domModifyBufferTime'] = $("#domModBuffertime").val();
	if (isChecked('chkPrintExp')) {
		data['fareRule.printExpDate'] = "Y";
	}
	if (isChecked('radOpReturn')) {
		data['fareRule.OpenReturnFlag'] = "Y";
	}
	
	if($("#ortConfirm_MM").val() != "" || $("#ortConfirm_DD").val() != "" || $("#ortConfirm_HR").val() != "" || $("#ortConfirm_MI").val() != ""){
		data['fareRule.openRetConfPeriod'] = $("#ortConfirm_MM").val() + ":" + $("#ortConfirm_DD").val() + ":" + $("#ortConfirm_HR").val() + ":" + $("#ortConfirm_MI").val();
	}	
	
	data['fareRule.flexiCodes'] = $("#selFlexiCode").val();
	if (isChecked('modifyByDate')) {
		data['fareRule.modifyByDate'] = "Y";
	}
	if (isChecked('modifyByOND')) {
		data['fareRule.modifyByRoute'] = "Y";
	}
	if (isChecked('loadFactorEnabled')) {
		data['fareRule.loadFactorMin'] = $("#loadFactorMin").val();
		data['fareRule.loadFactorMax'] = $("#loadFactorMax").val();
	}
	data['fareRule.visibleChannelIds'] = getMultiSelectBoxValues($("#visibility").val());
	if(FareClassValidation.isLoadAgents){
		data['fareRule.visibileAgentCodes'] = getAgentsList($.fn.multiSelect.getRightData('lstAgents'));
	} else{
		data['fareRule.visibileAgentCodes'] = getMultiSelectBoxValues(FareClassValidation.agents);
	}
	
	if(data['fareRule.visibileAgentCodes'] == ""){
	    data['fareRule.visibileAgentCodes'] = getMultiSelectBoxValues(FareClassValidation.agents);
	}
	
	
	paxApplicabilityJSON = [];	
	var paxTypes=["AD","CH","IN"];
	$.each(paxTypes, function(){
		var tempModel = {};
		tempModel.fareApplicable = $("#chk"+this+"App").is(':checked');
		tempModel.fareRefundable = $("#chk"+this+"Ref").is(':checked');
		tempModel.modChgApplicable = $("#chk"+this+"Mod").is(':checked');
		tempModel.cnxChgApplicable = $("#chk"+this+"CNX").is(':checked');
		tempModel.noshowChgType = $("#sel"+this+"CGTYP").val();
		tempModel.noshowChgAmount = $("#txt"+this+"NoShoCharge").val();
		tempModel.noshowChgLocal = $("#txt"+this+"NoShoChargeInLocal").val();
		tempModel.noshowChgBreakpoint = $("#txt"+this+"NoShoBreakPoint").val();
		tempModel.noshowChgBoudary = $("#txt"+this+"NoShoBoundary").val();
		paxApplicabilityJSON[paxApplicabilityJSON.length] = tempModel;		
	});	
	
	data['fareRule.paxApplicabilityJSON'] = JSON.stringify(paxApplicabilityJSON, null);	
	
	data['fareRule.modificationChargeAmountStr'] = $("#txtModification").val();
	data['fareRule.cancellationChargeAmountStr'] = $("#txtCancellation").val();
	data['fareRule.minModChargeAmountStr'] = $("#txtMin_mod").val();
	data['fareRule.maxModChargeAmountStr'] = $("#txtMax_mod").val();
	data['fareRule.modificationChargeType'] = $("#selDefineModType").val();
	data['fareRule.cancellationChargeType'] = $("#selDefineCanType").val();
	data['fareRule.cancellationChargeLocalStr'] = $("#txtCancellationInLocal").val();
	data['fareRule.modificationChargeLocalStr'] = $("#txtModificationInLocal").val();
	data['fareRule.minCnxChargeAmountStr'] = $("#txtMin_canc").val();
	data['fareRule.maxCnxChargeAmountStr'] = $("#txtMax_canc").val();	
	data['fareRule.agentComments'] = $("#txtaInstructions").val();
	
	ManageFareClasses.getFees(data);
	return data;
}

function getAgentsList(agentsData) {
	var data = "";
	if (agentsData != null) {
		var agentData = null;
		for (var  i = 0; i < agentsData.length; i++ ) {				
			agentData = agentsData[i].split(":");
			if (agentData[0] != agentData[1]) {
				data += agentData[1] + ",";
			}		
		}
	}	
	return data;
}

function getMultiSelectBoxValues(values) {	
	var data = "";
	if (values != null && values != "") {
		for (var i = 0; i < values.length; i++) {
			data += values[i] + ",";
		}
	}
	return data;
}

function isChecked(id) {
	return $('#'+ id).is(':checked');
}

function createDelimitedValues(IDS) {
	var frequencyBuild = "";
	for (var i = 0; i < IDS.length; i++) {
		if (isChecked(IDS[i])) {
			frequencyBuild += "1:";
		} else {
			frequencyBuild += "0:";
		}
	}	
	return frequencyBuild;
}

//Close Button Click
function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}

function getGridRowIndex(rowid){
	var rowCount = jQuery("#fareRuleGrid").getGridParam('rowNum');
	var pageID = jQuery("#fareRuleGrid").getGridParam('page');
	return (rowid > rowCount) ? rowid - ((pageID - 1) * rowCount) : rowid;
}

function beforeUnload() {
	if ((ManageFareClasses.objWindow) && (!ManageFareClasses.objWindow.closed)) {
		objWindow.close();
	}
}
ManageFareClasses.CWindowOpen = function(strIndex) {
	if ((ManageFareClasses.objWindow) && (!ManageFareClasses.objWindow.closed)) {
		ManageFareClasses.objWindow.close();
	}
	var intWidth = 0;
	var intHeight = 0;
	var strProp = '';

	var strFilename = "";
	switch (strIndex) {
	case 0:
		$("#hdnMode").val("LinkAgents");
		strFilename = "showLinkAgentMultiDDL.action"; // linkFares.do
		intHeight = 400;
		intWidth = 600;
		break;

	case 1:
		$("#hdnMode").val("AgentFares");
		strFilename = "showLoadFareJsp!showAgentFareRule.action"; // showAgentRules.action  -  showAgentwiseFC.do
		intHeight = 295;
		intWidth = 720;
		break;
		
	case 2:
		var rqf = "manageFare";
		var fc = $("#hdnRuleCode").val();
		var frId = $("#hdnRuleId").val();
		var version =  $("#hdnVersion").val();
		var locCurrency = null;
		var currencyType = $("#radCurrFareRule").val();
		var selCurrencyCode = $("#selCurrencyCode").val();
		if(currencyType=="SEL_CURR" && selCurrencyCode!="-1"){
			locCurrency = selCurrencyCode;
		}
		$("#hdnMode").val("OverwriteFares");
		strFilename = "showOverwriteFarerule.action?rqFrom="+rqf+"&frc="+fc+"&frId="+frId+"&version="+version+"&locCurrency="+locCurrency;
		intHeight = 660;
		intWidth = 900;
		break;
		
	case 3:
		var rqf = "manageFare";
		var fc = $("#hdnRuleCode").val();
		var frId = $("#hdnRuleId").val();
		var version =  $("#hdnVersion").val();
		$("#hdnMode").val("overRideOHD");
		strFilename = "showLoadFareJsp!overRideOHD.action?rqFrom="+rqf+"&frc="+fc+"&frId="+frId+"&version="+version;
		intHeight = 660;
		intWidth = 900;
		break;
	case 4:		 
		strFilename="showLoadJsp!loadProrationPopup.action";
		var gridJson = [
 		       			{ prorationId: 0, segmentCode: "A/B", proration: "30" },
 	       			{ prorationId: 1, segmentCode: "B/C", proration: "40"}];
		//UI_OverRideOHD.setData(gridJson);
	//	window.prorationData=ManageFareClasses.createSegmentData();
		
		ManageFareClasses.prorationData=ManageFareClasses.createSegmentData();
		alert('in parent:'+ManageFareClasses.prorationData);
		intHeight = 300;
		intWidth = 600;
		break;
	}
	
	strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width='
			+ intWidth
			+ ',height='
			+ intHeight
			+ ',resizable=no,top='
			+ ((window.screen.height - intHeight) / 2)
			+ ',left='
			+ (window.screen.width - intWidth) / 2;
	ManageFareClasses.objWindow = window.open(strFilename, "CWindow", strProp);
}

//Close Button Click
function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}

ManageFareClasses.getFees = function(data){
	var tabIDs = ['tabModification', 'tabCancelation','tabNoShow'];
	var tabId = "";
	var colCount = 0;
	var freeType = "";
	for (var tab = 0; tab < tabIDs.length; tab++) {
		tabId = tabIDs[tab];
		freeType = getFreeType(tabId);
		var tempID = 0;
		var rowArray = $("#"+tabId).find(".peirodRows").children();
		rowArray.removeClass("hight-light");
		var actualRowLength = rowArray.length - 1;
		var prefix = getPrefixByTabName(tabId);
		var ret = true;
		for (var i = 0; i < actualRowLength; i++) {
			tempID = i;
			var feeID = $("#"+prefix+"\\["+i+"\\]\\.feeID").val();
			var feeVersion = $("#"+prefix+"\\["+i+"\\]\\.feeVersion").val();
			var fromVal = $("#"+prefix+"\\["+i+"\\]\\.from").val();
			var toVal = $("#"+prefix+"\\["+i+"\\]\\.to").val();
			var adPrefObj = $("#"+prefix+"\\["+i+"\\]\\.preferdAD").val();
			var adVal = $("#"+prefix+"\\["+i+"\\]\\.valueAD").val();
			var adMaxVal = $("#"+prefix+"\\["+i+"\\]\\.maxAD").val();
			var adMinVal = $("#"+prefix+"\\["+i+"\\]\\.minAD").val();
			var chPrefObj = $("#"+prefix+"\\["+i+"\\]\\.preferdCH").val();
			var chVal = $("#"+prefix+"\\["+i+"\\]\\.valueCH").val();
			var chMaxVal = $("#"+prefix+"\\["+i+"\\]\\.maxCH").val();
			var chMinVal = $("#"+prefix+"\\["+i+"\\]\\.minCH").val();
			var inPrefObj = $("#"+prefix+"\\["+i+"\\]\\.preferdIN").val();
			var inVal = $("#"+prefix+"\\["+i+"\\]\\.valueIN").val();
			var inMaxVal = $("#"+prefix+"\\["+i+"\\]\\.maxIN").val();
			var inMinVal = $("#"+prefix+"\\["+i+"\\]\\.minIN").val();
			
			data['fees['+ colCount+ '].feeID'] = feeID;
			data['fees['+ colCount+ '].feeVersion'] = feeVersion;
			data['fees['+ colCount+ '].startCutOver'] = fromVal;
			data['fees['+ colCount+ '].endCutOver'] = toVal;
			data['fees['+ colCount+ '].feeType'] = freeType;
			data['fees['+ colCount+ '].adAmountSpecifiedAs'] = adPrefObj;
			data['fees['+ colCount+ '].adAmount'] = adVal;
			data['fees['+ colCount+ '].adMinAmountThresold'] = adMinVal;
			data['fees['+ colCount+ '].adMaxAmountThresold'] = adMaxVal;
			data['fees['+ colCount+ '].chAmountSpecifiedAs'] = chPrefObj;
			data['fees['+ colCount+ '].chAmount'] = chVal;
			data['fees['+ colCount+ '].chMinAmountThresold'] = chMinVal;
			data['fees['+ colCount+ '].chMaxAmountThresold'] = chMaxVal;
			data['fees['+ colCount+ '].inAmountSpecifiedAs'] = inPrefObj;
			data['fees['+ colCount+ '].inAmount'] = inVal;
			data['fees['+ colCount+ '].inMinAmountThresold'] = inMinVal;
			data['fees['+ colCount+ '].inMaxAmountThresold'] = inMaxVal;
			colCount++;		
		}
	}
}

function getFreeType(tabId) {
	var freeType = "";
	if (tabId == 'tabModification') {
		freeType = "MOD";
	} else if (tabId == 'tabCancelation') {
		freeType = "CNX";
	} else if (tabId == 'tabNoShow') {
		freeType = "NOSHOW";
	}
	return freeType;
}


ManageFareClasses.addHocRuleSetup = function() {
	ManageFareClasses.FAREMODE.ADDHOCSAVE = true;	
	FareClassValidation.enableAll("NEW");
	ManageFareClasses.setPageInitData();
	FareClassValidation.disableEnable("radOneWay");
}
/**
 * 
 */
ManageFareClasses.addProration = function() {
	var origin = $("#selOrigin").val();
	var dest = $("#selDestination").val();
	var via1 = $("#selVia1").val();
	var via2 = $("#selVia2").val();
	var via3 = $("#selVia3").val();
	var via4 = $("#selVia4").val();
	var isEnable = false;
	if (origin != null && origin != "" && dest != null && dest != "") {
		if ((via1 != null && via1 != "") || (via2 != null && via2 != "")
				|| (via3 != null && via3 != "") || (via4 != null && via4 != "")) {
			isEnable = true;
		}
	}
	
	if (isEnable) {
		ManageFareClasses.CWindowOpen(4);
		 
	} else {
		alert('Origin,destination and at least one via point should be selected to add prorations.');
	}
}

ManageFareClasses.createSegmentData = function() {
	var origin = $("#selOrigin").val();
	var dest = $("#selDestination").val();
	var via1 = $("#selVia1").val();
	var via2 = $("#selVia2").val();
	var via3 = $("#selVia3").val();
	var via4 = $("#selVia4").val();
	var isEnable = false;
	var ond=[];
	var ondJson=[]; 
	if (origin != null && origin != "" && dest != null && dest != "") {
		ond.push(origin);
		if (via1 != null && via1 != ""){
			ond.push(via1);
		}
		if (via2 != null && via2 != ""){
			ond.push(via2);
		}
		if (via3 != null && via3 != ""){
			ond.push(via3);
		}
		if (via4 != null && via4 != ""){
			ond.push(via4);
		}
		ond.push(dest);
	}
	if(ond.length >2){
		for(var i=0;i+1<ond.length;i++){
			var seg=ond[i]+"/"+ond[i+1];
			var obj={"prorationId":i,"segmentCode":seg,"proration":"50","prorationVersion":0};
			ondJson.push(obj);
		}
	}
	return ondJson;
}

ManageFareClasses.setFareAdd = function(fare) {
	$("#spnId").text(fare.fareID);
	$("#selFC").val(fare.fareRuleID); 
	$("#txtFromDt").val(fare.effectiveFromDate);
	$("#txtToDt").val(fare.effectiveToDate);
	$("#txtAED").val(fare.fareAmount);	
	if (fare.status == "ACT") {
		$("#chkStatus").check();
	} else {
		$("#chkStatus").uncheck();
	}
	var ondCodes = fare.ondCode.split("/");
	var origin = ondCodes[0];
	var destination = ondCodes[ondCodes.length-1];
	var via1 = "";
	var via2 = "";
	var via3 = "";
	var via4 = "";
	if (ondCodes.length == 3) {
		via1 = ondCodes[1];
	} else if (ondCodes.length == 4) {
		via1 = ondCodes[1];
		via2 = ondCodes[2];
	} else if (ondCodes.length == 5) {
		via1 = ondCodes[1];
		via2 = ondCodes[2];
		via3 = ondCodes[3];
	} else if (ondCodes.length == 6) {
		via1 = ondCodes[1];
		via2 = ondCodes[2];
		via3 = ondCodes[3];
		via4 = ondCodes[4];
	}
	$("#selOrigin").val(origin);
	$("#selDestination").val(destination);
	$("#selVia1").val(via1);
	$("#selVia2").val(via2);
	$("#selVia3").val(via3);
	$("#selVia4").val(via4);
	var dd = document.getElementById('selBc');
	for (var i = 0; i < dd.options.length; i++) {
	    if (dd.options[i].text === fare.bookingCode) {
	        dd.selectedIndex = i;
	        break;
	    }
	}
	$("#selBc").change();
	$("#selCamt").val(fare.childFareType);
	$("#txtCHL").val(fare.childFare);
	$("#selIamt").val(fare.infantFareType);
	$("#txtINF").val(fare.infantFare);
	$("#txtSlsStDate").val(fare.salesEffectiveFrom);
	$("#txtSlsEnDate").val(fare.salesEffectiveTo);
	$("#txtFromDtInbound").val(fare.returnEffectiveFromDate);
	$("#txtToDtInbound").val(fare.returnEffectiveToDate);	
	$("#versionFare").val(fare.version);
}

