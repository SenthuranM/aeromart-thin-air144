
var objCol1 = new DGColumn();
objCol1.columnType = "label";
objCol1.width = "10%";
objCol1.arrayIndex = 1;
objCol1.toolTip = "Agent Code";
objCol1.headerText = "Agent<br>Code";
objCol1.itemAlign = "left"
objCol1.sort = "true";

var objCol2 = new DGColumn();
objCol2.columnType = "label";
objCol2.width = "30%";
objCol2.arrayIndex = 2;
objCol2.toolTip = "Agent Name";
objCol2.headerText = "Agent Name";
objCol2.itemAlign = "left"

var objCol3 = new DGColumn();
objCol3.columnType = "label";
objCol3.width = "10%";
objCol3.arrayIndex = 3;
objCol3.toolTip = "Fare Rule";
objCol3.headerText = "Fare<br>Rule";
objCol3.itemAlign = "left"

var objCol4 = new DGColumn();
objCol4.columnType = "label";
objCol4.width = "35%";
objCol4.arrayIndex = 4;
objCol4.toolTip = "Fare Rule Description";
objCol4.headerText = "Fare Rule Description";
objCol4.itemAlign = "left"

var objCol5 = new DGColumn();
objCol5.columnType = "label";
objCol5.width = "10%";
objCol5.arrayIndex = 5;
objCol5.toolTip = "Basis Code";
objCol5.headerText = "Basis Code";
objCol5.itemAlign = "left"

// ---------------- Grid
var objDG = new DataGrid("spnFC");
objDG.addColumn(objCol1);
objDG.addColumn(objCol2);
objDG.addColumn(objCol3);
objDG.addColumn(objCol4);
objDG.addColumn(objCol5);

objDG.width = "99%";
objDG.height = "145px";
objDG.seqNoWidth = "5%";
objDG.seqNo = true;
objDG.headerBold = false;
objDG.rowOver = false;
objDG.paging = false;
objDG.arrGridData = parent.agentArrData;
objDG.seqNo = true;
objDG.pgnumRecPage = 100;
objDG.noDataFoundMsg = "No data Found";
objDG.displayGrid();
