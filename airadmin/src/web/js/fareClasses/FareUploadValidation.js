var isUploaded = false;
var strLog = "";
function winOnLoad(strMsg, strMsgType) {	
	if(strMsg != null && strMsgType != null)
		showWindowCommonError(strMsgType,strMsg);
	if(isUploaded)
		Disable("btnView", false);
	else
		Disable("btnView", true);
	createLog();
	createLogData(strLog);
}

function validation(){
	var validated = true;
	var fileName = document.getElementsByName("upload")[0].value;
    if('' == fileName){
		showWindowCommonError("Error",opener.fileRquired);            
		validated = false;        
    }
    else if(fileName.lastIndexOf(".csv")==-1){
    	showWindowCommonError("Error",opener.fileInvalid);            
        validated = false; 
    }
    return validated;
}

function saveFares(){
	if(validation()){
		setField("hdnMode",'UPLOAD');		
		document.forms[0].submit();
	}
}
function windowclose() {		
	window.close();	
}

function viewFareUploadStatus(){
	showPopUp(1);
}
function createLogData(strLog) {
	if(strLog != ""){
		DivWrite("spnPopupData",strLog);
	}else {
		DivWrite("spnPopupData", "Log is empty");
	}
}