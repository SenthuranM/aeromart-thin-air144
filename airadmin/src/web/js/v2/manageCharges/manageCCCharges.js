
function UI_manageCCCharges(){};

$(document).ready(function() {
	UI_manageCCCharges.ready();
});

var objWindow;
var logicalCCCode = "";
var pageEdited = false;
UI_manageCCCharges.validateBoolean = false;
UI_manageCCCharges.isParentOpener = false;
UI_manageCCCharges.otherGridRowNumber = -1;
UI_manageCCCharges.strGridRow = -1;
UI_manageCCCharges.newSegment = '';
UI_manageCCCharges.oldSegment = '';
UI_manageCCCharges.popupmessage = "";
UI_manageCCCharges.segmentIdsOfReprotectFlights = new Array();
UI_manageCCCharges.checkSelectRow = true;
UI_manageCCCharges.gridSelectedRow = -1;
UI_manageCCCharges.gridSelectedRowOnPage = false;
UI_manageCCCharges.reprotectSuccess = false;
UI_manageCCCharges.updatedRPData;


UI_manageCCCharges.ready = function(){
	
	$("#divSearch").decoratePanel("Search Payment Gateway Wise Charge");
	$("#divResultsPanel").decoratePanel("Payment Gateway Wise Charge");
	$("#divDispCCWiseCharge").decoratePanel("Add/Modify Payment Gateway Wise Charge");			
	$('#btnAdd').decorateButton();
	$('#btnEdit').decorateButton();
	$('#btnClose').decorateButton();
	$('#btnReset').decorateButton();
	$('#btnSave').decorateButton();
	$('#btnSearch').decorateButton();
	$('#btnReset').disableButton();
	$('#btnSave').disableButton();
	
	jQuery("#listCCWiseCharge").jqGrid({ 
		url:'showCreditCardCharge!searchCCWiseCharge.action',
		datatype: "json",
		postData: {
			effectiveDate: function() { return $("#selEffectiveDate").val(); },
			selPGWSearch: function() { return $("#selPGWSearch").val(); },
			selStatus: function() { return $("#selStatus").val(); },
		},
		jsonReader : {
			  root: "rows", 
			  page: "page",
			  total: "total",
			  records: "records",
			  repeatitems: false,					 
			  id: "0"				  
			},													
			colNames:['&nbsp;','PGW','CCWCId','PGWId','Description', 'Effective From','Effective To','Value Percentage','Amount','Status'],  
			colModel:[ 	{name:'Id', width:25, jsonmap:'id'},   
			        	{name:'PGW',index:'PGW', width:160, jsonmap:'PGW'}, 
			        	{name:'CCWCId',index:'CCWCId', width:75, hidden:true, jsonmap:'CCWCId'},
			        	{name:'txtPGW',index:'txtPGW', width:75, hidden:true, jsonmap:'PGWId'},
						{name:'description' ,index:'description' , width:270, align:"center", jsonmap:'description'},
					   	{name:'txtEffectiveDateFrom',index:'txtEffectiveDateFrom' , width:110, align:"center", jsonmap:'effectiveFrom'},
					   	{name:'txtEffectiveDateTo',index:'txtEffectiveDateTo' ,width:110,  align:"center", jsonmap:'effectiveTo'},
					   	{name:'valuePercentage',index:'valuePercentage' , width:85, align:"center", jsonmap:'valuePercentage'},
					   	{name:'amount',index:'amount' , width:50, align:"center", jsonmap:'amount'},
					   	{name:'status',index:'status' , width:75, align:"center", jsonmap:'status'},
					   ], imgpath: '../../themes/default/images/jquery', multiselect:false,
		pager: jQuery('#CCWiseChargepager'),
		rowNum:20,						
		viewrecords: true,
		height:210,
		width:930,
		loadui:'block',
		onSelectRow: function(rowid){
			UI_manageCCCharges.gridSelectedRow = rowid;
			document.getElementById("strCCWCId").value = jQuery("#listCCWiseCharge").getCell(rowid,'CCWCId');
			document.getElementById("strPGW").value = jQuery("#listCCWiseCharge").getCell(rowid,'txtPGW');
			disableControls(true);		
			searchFields(false);
			fillForm(rowid);
			$('#btnReset').disableButton();
			$('#btnSave').disableButton();
			
}
		  }).navGrid("#CCWiseChargepager",{refresh: true, edit: false, add: false, del: false, search: false});
	
	$("#selEffectiveDate").datepicker(
			{
				showOn : "button",
				buttonImage : "../../images/calendar_no_cache.gif",
				buttonImageOnly : true,
				dateFormat : 'dd/mm/yy'
			}
	);
	$("#txtEffectiveDateFrom, #txtEffectiveDateTo").datepicker(
			{
				showOn : "button",
				buttonImage : "../../images/calendar_no_cache.gif",
				buttonImageOnly : true,
				dateFormat : 'dd/mm/yy',
				minDate: 0
			}
	);
	
	$('#btnSearch').click(function() {

		clearData();
		$('#btnReset').disableButton();

	});
	
	$('#btnAdd').click(function() {
		disableControls(false);
		$("#btnReset").enableButton();
		$('#frmCCWiseCharge').clearForm();	
		$('#valuePercentage').val("V");	
		$('#status').val("INA");
		$('#btnEdit').disableButton();
		$("#btnSave").enableButton();
		$("#hdnMode").val("ADD");			
	}); 	
	
	$('#btnEdit').click(function() {				
		formFields(false);		
		$("#btnReset").enableButton();
		$("#btnSave").enableButton();
		$("#hdnMode").val("EDIT");
	}); 

	$('#btnReset').click(function() {				
		var mode = $("#hdnMode").val();
		if(mode == "EDIT"){
			jQuery("#listCCWiseCharge").GridToForm(UI_manageCCCharges.gridSelectedRow, '#frmCCWiseCharge');
		} else {
			$('#frmCCWiseCharge').clearForm();	
			$('#valuePercentage').val("V");	
			$('#status').val("INA");
		}
	});
	$('#btnSave').click(function() {

		if(UI_manageCCCharges.validateSearchCCCharges()){
			
			$('#frmCCWiseCharge').ajaxSubmit(options);
		}

	});
	
	$('#txtPGW').on('change', function() {
		$("#strPGW").val($("#txtPGW").val());
	});
	
	$("#selEffectiveDate").blur(function() { UI_manageCCCharges.dateOnBlur({id:0});});
	$("#txtEffectiveDateFrom").blur(function() { UI_manageCCCharges.dateOnBlur({id:1});});
	$("#txtEffectiveDateTo").blur(function() { UI_manageCCCharges.dateOnBlur({id:2});});
	
	var options = {
			cache: false,	//Fixed JIRA : 3042 - Lalanthi
			beforeSubmit:  showRequest,  // pre-submit callback - this can be used to do the validation part 
			success: showResponse,   // post-submit callback			 
			dataType:  'json'        // 'xml', 'script', or 'json' (expected server response type)  
		}; 
	
	// pre-submit callback 
	function showRequest(formData, jqForm, options) { 
		top[2].ShowProgress();
	    return true; 
	} 
	
	function showResponse(responseText, statusText)  {
		top[2].HideProgress();
		showCommonError(responseText['msgType'], responseText['succesMsg']);
	    if(responseText['msgType'] != "Error") {
	    				
			$("#btnAdd").attr('disabled', false); 
			disableForm(true);
			disableCCWiseChargeButton(true);
			$("#status").val('INA');	    	
			clearData();
	    }
	    
	} 
	
	disableControls(true);
	 $("#selEffectiveDate").attr('disabled', false); 
	 $("#selPGWSearch").attr('disabled', false); 
	 $("#selStatus").attr('disabled', false); 
	
	
}

/** *************************************************************************** */
/** **** V A L I D A T E - Credit Card Wise Charges ADD or EDIT***** */
/** *************************************************************************** */
UI_manageCCCharges.validateSearchCCCharges = function() {
	var strFromDate = document.getElementById("txtEffectiveDateFrom").value;
	var strToDate = document.getElementById("txtEffectiveDateTo").value;
	var strStatue = document.getElementById("status").value;
	var PGW =  document.getElementById("txtPGW").value;
	var valuePercentage =  document.getElementById("valuePercentage").value;
	var amount =  document.getElementById("amount").value;

	var isToDateLessthanFromDate = CheckDates(strToDate, strFromDate);
	
	if (validateWindowTextField(strFromDate, fromDateReqrd)) {
		getFieldByID("txtDepartureDateFrom").focus();
		return;
	}
	if (validateWindowTextField(strToDate, toDateReqrd)) {
		getFieldByID("txtEffectiveDateTo").focus();
		return;
	}
	
	if (validateWindowTextField(valuePercentage, "Value Percentage Required")) {
		getFieldByID("valuePercentage").focus();
		return;
	}
	
	if (validateWindowTextField(amount, amountReqrd)) {
		getFieldByID("amount").focus();
		return;
	}
	
	if (validateWindowTextField(PGW, pgwReqrd)) {
		getFieldByID("txtPGW").focus();
		return;
	}

	if (strFromDate != strToDate) {
		if (isToDateLessthanFromDate) {
			showCommonError("Error", dateValidate);
			document.getElementById("txtEffectiveDateFrom").focus();
			return;
		}
	}
	
	if(isNaN(amount)){
		showCommonError("Error", numberValidate);
		$("#amount").val('');
		document.getElementById("amount").focus();
		return;
	}
	
	return true;

}

function validateWindowTextField(textValue, msg) {
	var blnStatus = false;
	if (textValue == null || textValue == "") {
		objMsg.MessageText = msg;
		objMsg.MessageType = "Error";
		showCommonError(objMsg.MessageType, objMsg.MessageText);
		ShowPageMessage();
		blnStatus = true;
	}
	return blnStatus;
}

// auto date fix on blur event
UI_manageCCCharges.dateOnBlur = function(inParam){
		switch (inParam.id){
			case 0 :
				if($.trim($("#selEffectiveDate").val()) != ''){
					dateChk("selEffectiveDate");
				}else{
					$("#selEffectiveDate").val('');
				}
				break;
			case 1 : 
				if($.trim($("#txtEffectiveDateFrom").val()) != ''){
					dateChk("txtEffectiveDateFrom");
				}else{
					$("#txtEffectiveDateFrom").val('');
				}
				break;
			case 2 : 
				if($.trim($("#txtEffectiveDateTo").val()) != ''){
					dateChk("txtEffectiveDateTo");
				}else{
					$("#txtEffectiveDateTo").val('');
				}
				break;
		}
}

function disableForm(cond){
	$("#txtEffectiveDateFrom").attr('disabled',cond);
	$("#txtEffectiveDateTo").attr('disabled',cond);
	$("#status").attr('disabled',cond);
	$("#txtPGW").attr('disabled',cond);
	$("#valuePercentage").attr('disabled',cond);
	$("#amount").attr('disabled',cond); 	
}

function disableCCWiseChargeButton(){
	$("#btnEdit").disableButton();
	$("#btnReset").disableButton();
	$("#btnSave").disableButton();
}

function fillForm(rowid) {
	$("#hdnMode").val("");
	jQuery("#listCCWiseCharge").GridToForm(rowid, '#frmCCWiseCharge');											
	enableGridButtons();
}

function enableGridButtons(){
	$("#btnAdd").enableButton();
	$("#btnEdit").enableButton();
	
}

function formFields(cond){
	$("#txtEffectiveDateFrom").attr('disabled', cond);
	$("#txtEffectiveDateTo").attr('disabled', cond);
	$("#status").attr('disabled', cond);
	$("#valuePercentage").attr('disabled', cond);
	$("#amount").attr('disabled', cond);
	$("#strCCWCId").attr('disabled', cond);
	$("#strPGW").attr('disabled', cond);
	$("#hdnMode").attr('disabled', cond);
	if(cond == true){
		$("#txtEffectiveDateFrom").datepicker('disable');
		$("#txtEffectiveDateTo").datepicker('disable');
	} else {
		$("#txtEffectiveDateFrom").datepicker('enable');
		$("#txtEffectiveDateTo").datepicker('enable');
	}
	
}

function searchFields(cond){
	$("#selPGWSearch").attr('disabled', cond);
	$("#selEffectiveDate").attr('disabled', cond);
	$("#selStatus").attr('disabled', cond);	
}

function  disableControls(cond){
	$("input").each(function(){ 
	      $(this).attr('disabled', cond); 
	});	
	$("textarea").each(function(){ 
	      $(this).attr('disabled', cond); 
	});	
	$("select").each(function(){ 
	      $(this).attr('disabled', cond); 
	});
	if(cond == true){
		$("#txtEffectiveDateFrom").datepicker('disable');
		$("#txtEffectiveDateTo").datepicker('disable');
	} else {
		$("#txtEffectiveDateFrom").datepicker('enable');
		$("#txtEffectiveDateTo").datepicker('enable');
	}
	
	$("#btnClose").enableButton();
	
	$("#btnSearch").enableButton();
	
}

function clearData(){
	$("#listCCWiseCharge").clearGridData();
	UI_manageCCCharges.gridSelectedRow = -1;
	$("#listCCWiseCharge").trigger("reloadGrid");
	$('#frmCCWiseCharge').clearForm();
	$('#valuePercentage').val("V");
	$('#status').val("INA");
	$("#txtEffectiveDateFrom").datepicker('disable');
	$("#txtEffectiveDateTo").datepicker('disable');
}
