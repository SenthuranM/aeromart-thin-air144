/**
 * ChargesValidate.js
 * 
 * @author Baladewa welathanthri
 *  
 */

function ChargesValidate(){};
var ScreenId = "SC_ADMN_020";
$(function(){
	
});

ChargesValidate.validate = function() {	
	var validated = false;

	var strSearch = ManageCharges.getSearchData();
	
	$("#hdnRecNo").val("1");
	top[1].objTMenu.tabSetValue(ScreenId, strSearch + "~1");	
	var strCC = $("#txtChargeCode").val();
	var strGroup = $("#AddSegselGroup option:selected").text();
	var strDesce = $("#txtDesc").val();
	var strRefund = $("#chkRefundable").val();
	var strInfants = $("#selApplyTo option:selected").text();
	var strOndSeg = $("#selOndSeg option:selected").text();
	var strDisplay = $("#AddSegselGroup option:selected").val();
	var strCat = $("input[name='radCat1']:checked").val();
	var strInc = $("input[name='radInc1']:checked").val();
	var strIncAgt = $("input[name='radAgtInc1']:checked").val();
	
	$("#hdnSegments").val(getAll());
	var strSegments = $("#hdnSegments").val();
	var strDepature = $("#selDepature option:selected").text();
	var strArrival = $("#selArrival option:selected").text();
	//var strPOS = lssta.getselectedData();
	//var strAgt = lsagt.getselectedData();
	var journey = $("#selJourney").val();
	var minTrnDur = $("#hdnMinTrnDuration").val();
	var maxTrnDur = $("#hdnMaxTrnDuration").val();
	
	$("#hdnGroup").val($("#AddSegselGroup option:selected").text());
	$("#hdnStatus").val( $("input[name='chkStatus']:checked").val() );
	$("#hdnDesc").val(strDesce);
	$("#hdnCat").val(strCat);
	$("#hdnApplyTo").val($("#selApplyTo option:selected").val());
	$("#hdnOndSeg").val($("#selOndSeg option:selected").val());
	$("#hdnChannel").val($("#selChannel option:selected").val());
	$("#hdnRefund").val(strRefund);
	$("#hdnSelSelected").val($("#selSelected option:selected").text());
	$("#hdnpayType").val($("input[name='radDept1']:checked").val());
	$("#hdnJourney").val(journey);
	$("#hdnInc").val(strInc);
	$("#hdnIncOnd").val(strIncAgt);
	
	if (strCC == "") {	
		showERRMessage("charge code req");		
		$("#txtChargeCode").focus();
	} else if (strGroup == "") {		
		showERRMessage("Group code req");
		$("#AddSegselGroup").focus();
	} else if (strDesce == "") {		
		showERRMessage("description req");
		$("#txtDesc").focus();
	} else if (strCat == "") {		
		showERRMessage("category req");
		$("#radCat1").focus();
	} else if (strCat == "POS" && strPOS == "") {		
		showERRMessage("pos req");
		POSenable();
	} else if (strCat == "OND" && strSegments == "") {		
		showERRMessage("segment req");
		$("#selSelected").focus();
	} else if (strDisplay != "" && strDisplay != "1"
			&& $("#AddSegselGroup").attr("disabled") == false ){		
		showERRMessage("group inactive");
		$("#AddSegselGroup").focus();
	} else if (journey == "") {				
		showERRMessage("journey cant be null");
		$("#selJourney").focus();
	//}else if (strGroup == "SUR" && ($("#chkRevenue").checked == true && getText("txtRevenue") == "")){		
	}else if ( $("#chkRevenue").attr("checked") == true  && $("#txtRevenue").val() == ""){
		showERRMessage("Revenue cant be null");
		$("#txtRevenue").focus();
	}else if ($("#radSelCurr").attr("checked") == true && $("#selCurrencyCode option:selected").text() == ""){		
		showERRMessage("selected currency cant be null");
		$("#selCurrencyCode").focus();	
	}else if (hasTrnSettings() && minTrnDur == ""){
		showERRMessage("duration cant be null");
		$("#hdnMinTrnDuration").focus();
	}else if (hasTrnSettings() && maxTrnDur == ""){
			showERRMessage("duration cant be null");
			$("#hdnMaxTrnDuration").focus();
	}else if(minTrnDur != "" && isDurationWrong()){
		 showERRMessage(DurIncco);
		 $("#hdnMinTrnDuration").focus();
	}else if(maxTrnDur != "" && isDurationWrong()){
		 showERRMessage("Duration is incorrect");
		 $("#hdnMaxTrnDuration").focus();
	}else if(hasTrnSettings() && maxTrnDur == "00:00"){			
		 showERRMessage("Duration is zero");
		 $("#hdnMaxTrnDuration").focus();
	}else if (hasTrnSettings() && minTrnDur != "" && maxTrnDur != "") {
		var minTran = convertToMinutes(minTrnDur);
		var maxTran = convertToMinutes(maxTrnDur);			
		if(minTran > maxTran){			
			showERRMessage("duration min exceeds max");
			$("#hdnMinTrnDuration").focus();
			$("#hdnMaxTrnDuration").focus();
		}else {
			top[2].HidePageMessage();
			validated = true;
		}	
	}
	else {
		top[2].HidePageMessage();
		validated = true;
	}	
	return validated;
};

function hasTrnSettings(){
	
	if(($("#chkInwardTrn").val() !="N")
		 || ($("#chkOutwardTrn").val() !="N")
			||  ($("#chkAllTrn").val() !="N")){
		return true;
	}else{ // in JS needs else
		return false;
	}
	
}

ChargesValidate.replaceInvalids = function(control) {
	//TODO page edit true
	var strCC = control.val();
	var blnVal = isPositiveInt(strCC);
	var strLen = strCC.length;
	var val = removeInvalids(strCC);
	control.val(val);
}

function isDurationWrong() {
	 
	 
	var strMinCR = $("#hdnMinTrnDuration").val();
	var blnMinVal = IsValidDateTime($.trim(strMinCR));
	var strMaxCR = $("#hdnMaxTrnDuration").val();
	var blnMaxVal = IsValidDateTime($.trim(strMaxCR));
	if (!blnMinVal || !blnMaxVal) {
		return true;
	}
	
}

function convertToMinutes(duration){
	var arrTime = duration.split(":");
	return (parseFloat(arrTime[0]) * 60 + parseFloat(arrTime[1]));
}

function getAll() {
	var control = document.getElementById("selSelected");
	var values = "";
	for ( var t = 0; t < (control.length); t++) {
		var str = control.options[t].text;
		var strReplace = control.options[t].text;
		if (str.indexOf("All") != -1) {
			strReplace = str.replace("All", "***");
		}
		values += strReplace + ",";
	}
	return values;
}