/**
 * GroupBookingClass.js
 * 
 * @author Baladewa welathanthri
 *  
 */

GroupLCCalss = {};
var screenId = 'SC_ADMN_040';
GroupLCCalss.allLogicalBCs = [];
GroupLCCalss.startGroup = false;
GroupLCCalss.deleteGroup = false;
GroupLCCalss.BCGroups = [];
GroupLCCalss.lastGruopId = 0;
GroupLCCalss.availLogicalBCSPERBC = [];
GroupLCCalss.existingGruopIDs = [];
GroupLCCalss.ready = function(){
	$("#divGroupBookingClass").decoratePanel("Group Booking Classes");
	top[2].HideProgress();
	GroupLCCalss.loadBookingClass();
	$("#selBookingClass").change(function(){GroupLCCalss.showGroupGrid(this);});
	$("#btnClose").click(function(){GroupLCCalss.CloseClicked();});
	$("#btnGroup").click(function(){GroupLCCalss.GroupClicked();});
	$("#btnSave").click(function(){GroupLCCalss.SaveClicked();});
	$("#btnReload").click(function(){GroupLCCalss.RefreshClicked();});
	$("#btnDelGroup").click(function(){GroupLCCalss.DeleteGroupClicked();})
};

GroupLCCalss.CloseClicked = function(){
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		top[1].objTMenu.tabRemove(screenId);
	}
};

GroupLCCalss.DeleteGroupClicked = function(){
	GroupLCCalss.hidePagemessage();
	$(".ClS").removeClass("newGroup");
	if(!GroupLCCalss.deleteGroup){
		GroupLCCalss.deleteGroup = true;
		GroupLCCalss.startGroup = false;
	}else{
		showCommonError("Error", "You are in delete group state, you should save a group");
	}
};

GroupLCCalss.GroupClicked = function(){
	GroupLCCalss.hidePagemessage();
	$(".ClS").removeClass("newGroup");
	if(!GroupLCCalss.startGroup){
		GroupLCCalss.lastGruopId++;
		GroupLCCalss.startGroup = true;
		GroupLCCalss.deleteGroup = false;
	}else{
		showCommonError("Error", "You are in creating group state, you should save a group to create a new");
	}
};

GroupLCCalss.hidePagemessage = function(){
	top[2].HidePageMessage();
};

GroupLCCalss.RefreshClicked = function(){
	GroupLCCalss.loadBookingClass();
};
GroupLCCalss.SaveMode = null;
GroupLCCalss.SaveClicked = function(){
	GroupLCCalss.BCGroups = [];
	GroupLCCalss.hidePagemessage();
	if(!GroupLCCalss.startGroup && !GroupLCCalss.deleteGroup){
		showCommonError("Error", "Select a group to save");
	}else{
			$.each($(".newGroup"), function(){
			if (GroupLCCalss.startGroup){
				var temGroup = {"groupId":GroupLCCalss.lastGruopId, "bcCode":this.id.split("_")[1]};
				GroupLCCalss.BCGroups[GroupLCCalss.BCGroups.length] = temGroup;
				GroupLCCalss.SaveMode = "SAVE";
			}else if (GroupLCCalss.deleteGroup){
				var temGroup = {"groupId":this.id.split("_")[0], "bcCode":this.id.split("_")[1]};
				GroupLCCalss.BCGroups[GroupLCCalss.BCGroups.length] = temGroup;
				GroupLCCalss.SaveMode = "DELETE";
			}
		});
		GroupLCCalss.SaveBCGroups();
	}
};

GroupLCCalss.SaveBCGroups = function(){
	if (GroupLCCalss.BCGroups.length > 1){
		var data = {};
		data.mode = GroupLCCalss.SaveMode;
		data.bcGroupJSON = JSON.stringify(GroupLCCalss.BCGroups, null);
		$.ajax({
			url:'groupBookingClass.action',
		    beforeSend: top[2].ShowProgress(),
		    data: data,
		    dataType:"json",
		    complete: GroupLCCalss.loadBookingClassProcess,
		    error:GroupLCCalss.loadBookingClassError
		});
	}else{
		showCommonError("Error", "Group selection is not valied");
	}
};

GroupLCCalss.loadBookingClass = function(){
	$.ajax({
		url:'groupBookingClass.action',
	    beforeSend: top[2].ShowProgress(),
	    data: $.extend({}, {}),
	    dataType:"json",
	    complete: GroupLCCalss.loadBookingClassProcess,
	    error:GroupLCCalss.loadBookingClassError
	});
};
GroupLCCalss.loadBookingClassProcess = function(response){
	var response = eval("("+response.responseText+")");
	$("#selBookingClass").empty();
	$("#selBookingClass").fillDropDown({firstEmpty:false, dataArray:response.cabinClassList , keyIndex:0, valueIndex:1});
	$("#selBookingClass").val("Y");
	GroupLCCalss.allLogicalBCs = response.bcsForGrouping
	GroupLCCalss.showGroupGrid($("#selBookingClass"));
	if (GroupLCCalss.startGroup){
		GroupLCCalss.startGroup = false;
		showCommonError("Confirmation", "Group saved");
	}
	top[2].HideProgress();
};

GroupLCCalss.getLogicalBClass = function(BCCode){
	var availLBC = [];
	var tempAvailLBC = [];
	GroupLCCalss.existingGruopIDs = [];
	$("#groupLCClass").empty();
	$.each(GroupLCCalss.allLogicalBCs, function(i, j){
		GroupLCCalss.lastGruopId = (j.groupId > GroupLCCalss.lastGruopId)?j.groupId:GroupLCCalss.lastGruopId
		if (BCCode == this.ccCode){
			if (j.groupId != 0 && $.inArray( j.groupId, GroupLCCalss.existingGruopIDs ) == -1){
				GroupLCCalss.existingGruopIDs[GroupLCCalss.existingGruopIDs.length] = j.groupId;
			}
			var t = {"logicalCCCode":j.logicalCCCode, "logicalCCDescription":j.logicalCCDescription};
			if ($.inArray( j.logicalCCCode, tempAvailLBC ) == -1){
				tempAvailLBC[tempAvailLBC.length] = j.logicalCCCode;
				availLBC[availLBC.length] = t;
			}
		}
	});
	availLBC.sort(function sortinWay(data_A, data_B){
        return (data_A.logicalCCCode > data_B.logicalCCCode);
	});
	$.each(availLBC, function(i, j){
		var tLabel = $('<label>'+j.logicalCCDescription+' ['+j.logicalCCCode+']</label>');
		var tTxtBox = $('<input type="text"/>').attr({"id":"txt_"+j.logicalCCCode, "size":8});
		tTxtBox.keyup(function(){
			GroupLCCalss.searchByToText(j.logicalCCCode, tTxtBox.val());
		});
		var lCColHead = $('<div></div>').attr({"class":"LClassHead ui-state-default"});
		lCColHead.append(tLabel, tTxtBox);
		var lCColBody = $('<div></div>').attr({"class":"LClassBody"});
		var lCColmun = $('<div></div>').attr({"id":"groupLCClassColumn_"+this.logicalCCCode, "class":"LClass"});
		lCColmun.append(lCColHead, lCColBody);
		$("#groupLCClass").append(lCColmun);
	});
	var colWidth = Math.floor( (99)  / availLBC.length );
	$("#groupLCClass").find(".LClass").css({"width":colWidth+"%"});
	$("#groupLCClass").append($("<div></div>").css({"clear": "both", "height":"0px"}));
	return availLBC;
};

GroupLCCalss.loadBookingClassError = function(response){
	showCommonError("Error", "System Error, Please contact system administrator");
};

GroupLCCalss.showGroupGrid = function(obj){
	GroupLCCalss.availLogicalBCSPERBC = GroupLCCalss.getLogicalBClass($(obj).val());
	GroupLCCalss.makeGroupGrid($(obj).val());
	$("#colorCodes").empty();
	if (GroupLCCalss.existingGruopIDs.length > 0){
		GroupLCCalss.createGroupColorBlock();
	}
};

GroupLCCalss.searchByToText = function(lccCode,shTxt){
	if ( $("#groupLCClassColumn_"+lccCode).find(".newGroup").length > 0 ){
		var x = confirm("Selected items will remove, do you want to search?");
		if (x){
			GroupLCCalss.addLogicalBCS($("#selBookingClass").val(), lccCode, shTxt);
		}
	}else{
		GroupLCCalss.addLogicalBCS($("#selBookingClass").val(), lccCode, shTxt);
	}
};

GroupLCCalss.makeGroupGrid = function(CCCODE){
	$.each( GroupLCCalss.availLogicalBCSPERBC, function(){
		GroupLCCalss.addLogicalBCS(CCCODE, this.logicalCCCode, '');
	});
	var tempHeight = [];
	$.each($(".LClass"), function(){
		tempHeight[tempHeight.length] = parseInt($(this).css("height"), 10);
	});
	$(".LClass").css("height", Math.max.apply( Math, tempHeight ));
};




GroupLCCalss.createGroupColorBlock = function(){
	function sortinWay(data_A, data_B){
        return (data_A - data_B);
	}
	GroupLCCalss.existingGruopIDs.sort(sortinWay);
	$.each(GroupLCCalss.existingGruopIDs, function(){
		var t = $("<div>"+this+"</div>").css({
			"float":"left",
			"width":15,
			"height":15,
			"font-size":"9px",
			"verticle-align":"top",
			"text-align":"right",
			"margin":"0px 2px",
			"background":"#" + GroupLCCalss.getColorCode(this)
		});
		$("#colorCodes").append(t);
	});
	$("#colorCodes").append($("<div style='float:left; margin-left:10px'>Group colors are not a static term</div>"));
};

GroupLCCalss.getColorCode = function(id){
	String.prototype.replaceAt=function(index, chtr) {
	    return this.substr(0, index) + chtr + this.substr(index+chtr.length);
	}
	var baseCol = "FFFFFF";
	var t = id.toString(16);
	tk = (id%6 >= t.length)?(id%6 - (t.length-1)):id%6;
	return baseCol.replaceAt(tk, t);
};

GroupLCCalss.addLogicalBCS = function(ccCode, lbcCode, sort){
	var toObj = $("#groupLCClassColumn_"+lbcCode).find(".LClassBody");
	toObj.empty();
	$.each( GroupLCCalss.allLogicalBCs, function(){
		if(ccCode == this.ccCode && lbcCode == this.logicalCCCode){
			if (this.bcCode.indexOf($.trim(sort).toUpperCase()) > -1){
				var groupClass = this.groupId + "_GROUP";
				var groupable = this.editable?"groupable":"non-groupable";
				var blockID = this.groupId +"_"+ this.bcCode +"_"+ this.ccCode + "_" + this.logicalCCCode;
				var gpID = "&nbsp;";
				var colorCode = "FFFFFF";
				if (this.groupId != 0){
					gpID = this.groupId;
					colorCode = GroupLCCalss.getColorCode(this.groupId);
					// TODO bala, please check
					//<input type='button' id='btnShoAll' value='+'/>
				}
				var tabeTxt = "<table border='0' width='100%'><tr><td width='95%' class='bc'>"+this.bcCode+"</td><td valign='top' align='right'><span style='font-size:9px;'>"+gpID+"<span></td></tr></table>"
				var bcObj = $('<div>'+tabeTxt+'</div>').attr({
					"class":"ClS "+ groupClass + " " + groupable,
					"role":groupable,
					"id":blockID,
					"style":"background:#"+colorCode
				});
				bcObj.click(function(e){
					if (GroupLCCalss.startGroup || GroupLCCalss.deleteGroup){
						GroupLCCalss.groupUNGroup(bcObj[0].id);
					}
				});
				bcObj.mouseover(function(){
					GroupLCCalss.itmeRoleOver(bcObj);
				});
				bcObj.mouseout(function(){
					GroupLCCalss.itmeRoleOut(bcObj);
					
				});
				bcObj.appendTo(toObj);
			}
		}
	});
};

GroupLCCalss.itmeRoleOver = function(bcObj){
	if ( (GroupLCCalss.startGroup) || (GroupLCCalss.deleteGroup && bcObj[0].id.split("_")[0] !=0)){
		bcObj.addClass("mOveritem");
	}
}

GroupLCCalss.itmeRoleOut = function(bcObj){
	if ((GroupLCCalss.startGroup || GroupLCCalss.deleteGroup) && bcObj.hasClass("mOveritem")){
		bcObj.removeClass("mOveritem");
	}
}

GroupLCCalss.groupUNGroup = function(code){
	GroupLCCalss.hidePagemessage();
	if ( GroupLCCalss.startGroup ){
		if ( code.split("_")[0] != 0 ){
				if ($("#"+code).hasClass("newGroup")){
					$("#"+code).removeClass("newGroup");
				}else{
					var x = confirm("This booking class is already having a group, do you want to remove this from it?");
					if (x){
						$("#"+code).addClass("newGroup");
					}
				}
		}else{
			if ($("#"+code).hasClass("newGroup")){
				$("#"+code).removeClass("newGroup");
			}else{
				$("#"+code).addClass("newGroup");
			}
		}
	}else if ( GroupLCCalss.deleteGroup ){
		if ( code.split("_")[0] != 0 ){
			if ($("#"+code).hasClass("newGroup")){
				$("."+code.split("_")[0]+"_GROUP").removeClass("newGroup");
			}else{
				$(".ClS").removeClass("newGroup");
				$("."+code.split("_")[0]+"_GROUP").addClass("newGroup")
			}
		}
	}
};

$(function(){
	GroupLCCalss.ready();
});