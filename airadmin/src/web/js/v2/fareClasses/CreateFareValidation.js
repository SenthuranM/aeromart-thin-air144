/**
 * CreateFareValidation.js
 * 
 * @author Baladewa welathanthri
 *  
 */

function CreateFareValidation(){};


CreateFareValidation.ready = function(){
	$("#txtFromDt").datepicker({ minDate: new Date(), showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', buttonImageOnly: true,  dateFormat: 'dd/mm/yy', changeMonth: 'true' , changeYear: 'true', showButtonPanel: 'true', onClose: function() { this.focus(); }});
	$("#txtToDt").datepicker({ minDate: new Date(), showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', buttonImageOnly: true,  dateFormat: 'dd/mm/yy', changeMonth: 'true' , changeYear: 'true', showButtonPanel: 'true', onClose: function() { this.focus(); }});
	$("#txtFromDtInbound").datepicker({ minDate: new Date(), showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', buttonImageOnly: true,  dateFormat: 'dd/mm/yy', changeMonth: 'true' , changeYear: 'true', showButtonPanel: 'true', onClose: function() { this.focus(); }});
	$("#txtToDtInbound").datepicker({ minDate: new Date(), showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', buttonImageOnly: true,  dateFormat: 'dd/mm/yy', changeMonth: 'true' , changeYear: 'true', showButtonPanel: 'true', onClose: function() { this.focus(); }});
	$("#txtSlsStDate").datepicker({ minDate: new Date(), showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', buttonImageOnly: true,  dateFormat: 'dd/mm/yy', changeMonth: 'true' , changeYear: 'true', showButtonPanel: 'true', onClose: function() { this.focus(); }});
	$("#txtSlsEnDate").datepicker({ minDate: new Date(), showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', buttonImageOnly: true,  dateFormat: 'dd/mm/yy', changeMonth: 'true' , changeYear: 'true', showButtonPanel: 'true', onClose: function() { this.focus(); }});
	$("#txtFromDt, #txtToDt, #txtFromDtInbound, #txtToDtInbound, #txtSlsStDate, #txtSlsEnDate").on("blur", function(){
		dateChk(this.id);
	});
	$("#selFC").change(function(){CreateFareValidation.changeFareRuleCode();});
	$("#chkExtendToDate").click(extendToDateTenYears_Click);
	$("#txtFromDt").blur(setAutoDates);
};

CreateFareValidation.checkDateFasility = function(obj){
	dateChk(obj.val());
};

function setAutoDates(e){
	var outBoundFromDate = $("#txtFromDt").val();
	var salesStartDt = $("#txtSlsStDate").val();	
	if(salesStartDt==null || salesStartDt==""){
		if(ManageFareClasses.sysDate != null){
			$("#txtSlsStDate").val( ManageFareClasses.sysDate );
		}else{
			$("#txtSlsStDate").val(outBoundFromDate );
		}		
	}
	
	var outBoundFromDate = $("#txtFromDt").val();
	var inBoundFromDate = $("#txtFromDtInbound").val();
	var salesFromDate = $("#txtSlsStDate").val();
	if(outBoundFromDate != null && outBoundFromDate != "" && dateChk("txtFromDt")){
		$("#txtToDt").val(addTenYears(outBoundFromDate));
	}
	if(inBoundFromDate != null && inBoundFromDate != "" && dateChk("txtFromDtInbound")){
		$("#txtToDtInbound").val(addTenYears(inBoundFromDate));
	}
	if(salesFromDate != null && salesFromDate != "" && dateChk("txtSlsStDate")){
		$("#txtSlsEnDate").val( addTenYears(salesFromDate));
	} 
}

function extendToDateTenYears_Click(e){
	var checked = $("#chkExtendToDate").prop("checked");
	if(checked){
		var outBoundFromDate = $("#txtFromDt").val();
		var inBoundFromDate = $("#txtFromDtInbound").val();
		var salesFromDate = $("#txtSlsStDate").val();
		if(outBoundFromDate != null && outBoundFromDate != "" && dateChk("txtFromDt")){
			$("#txtToDt").val(addTenYears(outBoundFromDate));
		}
		if(inBoundFromDate != null && inBoundFromDate != "" && dateChk("txtFromDtInbound")){
			$("#txtToDtInbound").val(addTenYears(inBoundFromDate));
		}
		if(salesFromDate != null && salesFromDate != "" && dateChk("txtSlsStDate")){
			$("#txtSlsEnDate").val( addTenYears(salesFromDate));
		}
	}
}

function addTenYears(date){
	var strRetDate = "";
	var dateArray = date.split("/");
	var updatingYear = parseInt(dateArray[2]) + 10;
	strRetDate = dateArray[0] + "/" + dateArray[1] + "/" + updatingYear;
	return strRetDate;
} 

CreateFareValidation.changeFareRuleCode = function(){
	if ( $("#selFC").val() == ""){
		$("#btnAddFareRule").disable();
		$("#btnAddFareRule").addClass("ButtonDisabled");
		$("#btnAddHocRule").enable();
		$("#btnAddHocRule").removeClass("ButtonDisabled");
	}else{		
		$("#btnAddFareRule").enable();
		$("#btnAddFareRule").removeClass("ButtonDisabled");
		$("#btnAddHocRule").disable();
		$("#btnAddHocRule").addClass("ButtonDisabled");
		ManageFareClasses.FAREMODE.ADDHOCSAVE = false;
	}
}

function BLValidateDecimel(obj) {
	if (obj.value == "") {
		obj.value = "0.00";
	}
}

function KPValidateDecimel(objCon, s, f) {
	setPageEdited(true);
	var strText = objCon.value;
	var length = strText.length;
	var blnVal = isLikeDecimal(strText);
	var blnVal2 = currencyValidate(strText, s, f);
	var blnVal3 = isDecimal(strText);

	if (!blnVal) {
		customValidate(objCon, /[a-zA-Z`!@#$%\s&*?\[\]{}()|\\\/=:,;_^~]/g);
	} else if (!blnVal2) {
		customValidate(objCon, /[a-zA-Z`!@#$%\s&*?\[\]{}()|\\\/=:,;_^~]/g);
		objCon.focus();
	}
	if (!blnVal2) {
		if (strText.indexOf(".") != -1) {
			wholeNumber = strText.substr(0, strText.indexOf("."));
			if (wholeNumber.length > 13) {
				objCon.value= strText.substr(0, wholeNumber.length - 1);
			} else {
				objCon.value= strText.substr(0, length - 1);
			}
		} else {
			objCon.value= strText.substr(0, length - 1);
		}
		
	}
}

function KPValidateDecimelDup(objCon, s, f) {
	setPageEdited(true);
	var strText = objCon.value;
	var length = strText.length;
	var blnVal = isLikeDecimal(strText);
	var blnVal2 = false;
	var type = "";
	
	if (!blnVal) {
		customValidate(objCon, /[a-zA-Z`!@#$%\s&*?\[\]{}()|\\\/=:,;_^~]/g);
	} else if (!blnVal2) {
		customValidate(objCon, /[a-zA-Z`!@#$%\s&*?\[\]{}()|\\\/=:,;_^~]/g);
		objCon.focus();
	}	
	if(objCon.id == "txtCHL" || objCon.id == "txtCHFareInLocal"){
		type = chldFareType;
	}else{
		type = infFareType;
	}	
	if(type == "P"){
		blnVal2 = currencyValidate(strText, 3, 2);
		if (!blnVal2) {
			if (strText.indexOf(".") != -1) {
				wholeNumber = strText.substr(0, strText.indexOf("."));
				if (wholeNumber.length > 6) {
					objCon.value= strText.substr(0, wholeNumber.length - 1);
				} else {
					objCon.value= strText.substr(0, length - 1);
				}
			} else {
				objCon.value= strText.substr(0, length - 1);
			}	
			
		}
		if(strText > 100){
				objCon.value= strText.substr(0, length - 1);
		}
		
	}else{
		blnVal2 = currencyValidate(strText, 10, 2);
		if (!blnVal2) {
			if (strText.indexOf(".") != -1) {
				wholeNumber = strText.substr(0, strText.indexOf("."));
				if (wholeNumber.length > 13) {
					objCon.value= strText.substr(0, wholeNumber.length - 1);
				} else {
					objCon.value= strText.substr(0, length - 1);
				}
			} else {
				objCon.value= strText.substr(0, length - 1);
			}
		}
	}
	
}
