/**
 * ManageFareClasses.js
 * 
 * @author Baladewa welathanthri
 *  
 */

function ManageFareClasses(){};

$(function(){
	ManageFareClasses.ready();
});

ManageFareClasses.rowEditFlag = false;
ManageFareClasses.objWindow = null;
ManageFareClasses.mode = "";
ManageFareClasses.FAREMODE = {SAVE:false, MODIFY:false, ADDHOCSAVE:false};
ManageFareClasses.visibleAgents = null;
ManageFareClasses.flexiList = [];
ManageFareClasses.selectedRow = null;
ManageFareClasses.gridDateCopy = null;
ManageFareClasses.fareDiscountDiplay = false;
ManageFareClasses.openReturnSupport=false;
ManageFareClasses.halfReturnSupport=false;
ManageFareClasses.enableFareInDepAirPCurr = false;
ManageFareClasses.domesticEnabled= false;
ManageFareClasses.errorInfo = null;
ManageFareClasses.sysDate = null;
ManageFareClasses.isReturnFare = false;
ManageFareClasses.prorationData=[];
ManageFareClasses.isProrationModify=false;
ManageFareClasses.isProrationSaved=false;
ManageFareClasses.isProrationDeleted=false;
ManageFareClasses.isProDeleteEnable=false;
ManageFareClasses.savingProrations=[];
ManageFareClasses.segmentChanged=false;
ManageFareClasses.isEnableProration=false;
ManageFareClasses.enableAgentInstruction = false;
ManageFareClasses.enableAgentCommission = false;
ManageFareClasses.selectedCurrency =  null;
ManageFareClasses.pos = null;
ManageFareClasses.gridCommentsDataArray = null;
ManageFareClasses.isMultilingualCommentEnabled = false;

ManageFareClasses.ready = function(){
	$(".currency-loacal, #tblFareDiscount, .selected-currency").hide();
	$(".open-return-sup").css("visibility", "hidden");
	$(".half-return-sup").css("visibility", "hidden");
	$("#txtAED").numeric({allowDecimal:true});
	$("#minBTSeatCount").numeric({allowDecimal:false});
	$("#chkBulkTicket").parent().parent().find(".mandatory").hide();
	$("#txtCHL").numeric({allowDecimal:true});
	$("#txtINF").numeric({allowDecimal:true});
	$("#txtBaseInLocal").numeric({allowDecimal:true});
	$("#txtCHFareInLocal").numeric({allowDecimal:true});
	$("#txtINFInLocal").numeric({allowDecimal:true});
	$("#chkUpdateCommentsOnly,#lblUpdateCommentsOnly").hide();
	if (screeMode != "CREATEFARE"){
		CreateFareValidation.ready();
		ManageFareClasses.FAREMODE.SAVE = true;
		$("#newFareRow").show();
		$("#fareSearchRow").hide();
		$("#fareRuleOnly").hide();
		$("#btnAddFareRule").click(function(){ManageFareClasses.loadFareRule();});
		$("#btnAddHocRule").click(function(){ManageFareClasses.addHocRuleSetup();});
		$("#btnAddProration").click(function(){ManageFareClasses.addProration();});
		$("#btnAddProration").disable();
		$("#radSelCurr").click(function(){ManageFareClasses.getAirportCurrency($("#selOrigin").val());});
		$("#radCurr").click(function(){ManageFareClasses.clearAirportCurrency();});
		$("#radOpReturn").click(function(){ManageFareClasses.setOPENRTValue();});
		$("#txtBaseInLocal").disable();
		$("#txtCHFareInLocal").disable();
		$("#txtINFInLocal").disable();
		$("#btnLinkFares").click(function(){ManageFareClasses.linkFares_click();});
		$("#btnSave").enable();		
		
		var fareID = $("#fareID").val();
		if (!isEmpty(fareID)) {
			ManageFareClasses.FAREMODE.MODIFY = true;			
		}
		//to hide the local currency
		ManageFareClasses.loadFarePageData();
		$("#btnBack").on("click", ManageFareClasses.goBack);
		$("#btnEditRule").on("click", function(){
			var x = true;
			if (!ManageFareClasses.FAREMODE.ADDHOCSAVE){
				var x = confirm("You can not edit a Fare Rule attached to a Master Fare Rule Code. \nIt will be saved as an Ad-Hoc rule \nDo you want to continue ?")
			}
			if (x){
				ManageFareClasses.mode = "EDIT";
				ManageFareClasses.FAREMODE.ADDHOCSAVE = true;
				FareClassValidation.isAdHocFare = true;
				enabledDispableMultiselect('enable');
				$("#selFC").val("");
				FareClassValidation.enableAll("EDIT");
				$("#tabs").tabs("option", "active", 0);
			}
			
		});
		$("#selBc").on("change", function(){
			//"N,N,Economy Class,ACT,COM,NORMAL,,A,A"
			var arrFields = this.value.split(",");
			var spnTmpCos = arrFields[2];
			var spnTmpFixed = '';
			var spnTmpStd = '';
			var spnPaxgoshow = '';
			var fixed = arrFields[1];
			var standard = arrFields[0];
			var paxGoshow = arrFields[6];
			var fareCat = arrFields[4];

			spnTmpStd = (arrFields[0] == "Y") ? "Standard" : "Non Standard";

			if (fixed == "Y") {
				spnTmpFixed = "Fixed";
			} else {
				spnTmpFixed = "Non Fixed";
			}
			
			if (fareCat == "RET" || fareCat == "COM") {				
				if(getFareRuleTmpType() == "N"){
					ManageFareClasses.isReturnFare = false;
					$("#txtFromDtInbound").disable();
					$("#txtToDtInbound").disable();
				} else {
					ManageFareClasses.isReturnFare = true;
					$("#txtFromDtInbound").enable();
					$("#txtToDtInbound").enable();
				}
			} else {
				$("#txtFromDtInbound").disable();
				$("#txtToDtInbound").disable();
				$("#txtFromDtInbound").val("");
				$("#txtToDtInbound").val("");
				ManageFareClasses.isReturnFare = false;
			}
			
			spnPaxgoshow = (trim(paxGoshow) == "GOSHOW" || trim(paxGoshow) == "INTERLINE") ? " / " + paxGoshow : "";

			var bcDetails =  spnTmpStd + " / " + spnTmpFixed + " / " + spnTmpCos + " / " + arrFields[5] + " / " + fareCat	+ spnPaxgoshow ;
			$("#spnFixed").text(bcDetails);
		});
		
		$("#selFC").on("change", function(){
			if(getFareRuleTmpType() == "N"){
				ManageFareClasses.isReturnFare = false;
				$("#txtFromDtInbound").disable();
				$("#txtToDtInbound").disable();
			} else {
				ManageFareClasses.isReturnFare = true;
				$("#txtFromDtInbound").enable();
				$("#txtToDtInbound").enable();
			}
		})
		
	}else{	
		$("#btnEdit").click(function(){ManageFareClasses.mode = "EDIT"; ManageFareClasses.editSelRow();});
		$("#btnAddFare").click(function(){ManageFareClasses.mode = "ADD"; ManageFareClasses.addNewRule();});
		$("#btnViewAgntFC").click(function(){ManageFareClasses.CWindowOpen(1);});
		$("#radOpReturn").click(function(){ManageFareClasses.setOPENRTValue();});
		$("#newFareRow").hide();
		$("#fareRuleOnly").show();
		$("#fareSearchRow").show();
		$("#btnSearch").click(function(){ManageFareClasses.searchData();});
		$("#btnDelete").click(function(){ManageFareClasses.deleteFareRule(); });
		ManageFareClasses.loadPageInitialData();
	}
	
	$("#selDefineCanType").change(function(){
		ManageFareClasses.enableDisableChargeCurrencyFields($("#selDefineCanType").val(),"canc");			
	});
	$("#selDefineModType").change(function(){
		ManageFareClasses.enableDisableChargeCurrencyFields($("#selDefineModType").val(),"mod");			
	});
	$("#selDefineNCCType").change(function(){
		ManageFareClasses.enableDisableChargeCurrencyFields($("#selDefineNCCType").val(),"ncc");			
	});
	$("#selADCGTYP").change(function(){
		ManageFareClasses.enableDisableNoshoCurrencyFields("AD",$("#selADCGTYP").val());			
	});
	$("#selCHCGTYP").change(function(){
		ManageFareClasses.enableDisableNoshoCurrencyFields("CH",$("#selCHCGTYP").val());
	});	
	$("#selINCGTYP").change(function(){
		ManageFareClasses.enableDisableNoshoCurrencyFields("IN",$("#selINCGTYP").val());
	});	
	
	$('#fareDiscount').click(function(){
		if (isChecked('fareDiscount')) {
			$("#fareDiscountMin").enable();
			$("#fareDiscountMax").enable();
		} else {
			$("#fareDiscountMin").val("").disable();
			$("#fareDiscountMax").val("").disable();
		}
	});
	$("#btnSave").click(function(){
		var strConf = true;
		if (isChecked('chkUpdateCommentsOnly')) {
			var confirmMsg  = "With No Fare Split only following fields will be updated, ";
				confirmMsg += "\n1.Description.\n2.Rules & Comments.\n3.Agent Instructions.";
				confirmMsg += "\n4.Visible Agents.\n5.Link Country/POS."
				confirmMsg += "\n\n\t\tDo you wish to Continue?";
			strConf = confirm(confirmMsg);	
		}	
				
		if(strConf){
			$("#btnSave").disable();
			if (ManageFareClasses.FAREMODE.SAVE) {
				ManageFareClasses.saveFare();			
			} else {
				ManageFareClasses.saveFareRule();
			}
		}
	});
	$("input[name='radCurrFareRule']").on("click",ManageFareClasses.showHideSelectedCurrency);
	$("input[name='radSelCurrFareRule']").on("click",ManageFareClasses.showHideSelectedCurrency);
	$("#btnReset").click(function(){ManageFareClasses.resetClick()});
	$("#btnClose").click(function(){ManageFareClasses.closeClick()});
	$("#chkMasterFare").click(ManageFareClasses.ckeckMasterFare);
	$("#chkLinkFare").click(ManageFareClasses.ckeckLinkFare);
	$("#trFareRuleNCC").hide();
	if(!isChecked('chkChrgRestrictions')){
			$("#selDefineModType, #selDefineCanType").prop('readonly', true);
			$("#txtCancellationInLocal,#txtCancellation,#txtMin_canc,#txtMax_canc").prop('readonly',true);
			$("#txtModificationInLocal,#txtModification,#txtMin_mod,#txtMax_mod").prop('readonly',true);
			if(FareClassValidation.isEnableFareRuleNCC){
				$("#selDefineNCCType").prop('disabled',true).prop('readonly', true);
				$("#txtNCCInLocal,#txtNCC,#txtMin_NCC,#txtMax_NCC").prop('disabled',true).prop('readonly',true);
			}	
	}
	ManageFareClasses.addProrationHandler();
}

ManageFareClasses.enableDisableChargeCurrencyFields = function(chargeType,value) {
	
	if(!isChecked('chkChrgRestrictions')){
		$("#selDefineModType, #selDefineCanType").prop('disabled',true).prop('readonly', true);
		$("#txtCancellationInLocal,#txtCancellation,#txtMin_canc,#txtMax_canc").prop('disabled',true).prop('readonly',true);
		$("#txtModificationInLocal,#txtModification,#txtMin_mod,#txtMax_mod").prop('disabled',true).prop('readonly',true);
		
		if(FareClassValidation.isEnableFareRuleNCC){
			$("#selDefineNCCType").prop('disabled',true).prop('readonly', true);
			$("#txtNCCInLocal,#txtNCC,#txtMin_NCC,#txtMax_NCC").prop('disabled',true).prop('readonly',true);
		}		
	}else{
		if(chargeType == "V"){
			if($("input[name='radCurrFareRule']:checked").attr("id") == "radSelCurrFareRule"){
				if(value == "canc"){
					$("#txtCancellationInLocal").prop('disabled',false).prop("readonly",false);
					$("#txtCancellation,#txtMin_canc,#txtMax_canc").val("").prop("readonly",true);
				}
				if(value == "mod"){
					$("#txtModificationInLocal").prop('disabled',false).prop("readonly",false);
					$("#txtModification,#txtMin_mod,#txtMax_mod").val("").prop("readonly",true);
				}
				if(value == "ncc"){
					$("#txtNCCInLocal").prop('disabled',false).prop("readonly",false);
					$("#txtNCC,#txtMin_NCC,#txtMax_NCC").val("").prop("readonly",true);
				}
			} else {
				if(value == "canc"){
					$("#txtCancellation").prop('disabled',false).prop("readonly",false);
					$("#txtCancellationInLocal,#txtMin_canc,#txtMax_canc").val("").prop("readonly",true);
				}
				if(value == "mod"){
					$("#txtModification").prop('disabled',false).prop("readonly",false);
					$("#txtModificationInLocal,#txtMin_mod,#txtMax_mod").val("").prop("readonly",true);
				}
				if(value == "ncc"){
					$("#txtNCC").prop('disabled',false).prop("readonly",false);
					$("#txtNCCInLocal,#txtMin_NCC,#txtMax_NCC").val("").prop("readonly",true);
				}
			}
		} else {
			if(value == "canc"){
				$("#txtCancellationInLocal").val("").prop("readonly",true);
				$("#txtCancellation").prop('disabled',false).prop("readonly",false);
				$("#txtMin_canc,#txtMax_canc").val("").prop('disabled',false).prop("readonly",false);
			}
			if(value == "mod"){
				$("#txtModificationInLocal").val("");
				$("#txtModificationInLocal").prop('disabled',true).prop("readonly",true);
				$("#txtModification,#txtMin_mod,#txtMax_mod").prop('disabled',false).prop("readonly",false);
			}
			if(value == "ncc"){
				$("#txtNCCInLocal").val("");
				$("#txtNCCInLocal").prop('disabled',true).prop("readonly",true);
				$("#txtNCC,#txtMin_NCC,#txtMax_NCC").prop('disabled',false).prop("readonly",false);
			}
		}
	}
}

ManageFareClasses.enableDisableNoshoCurrencyFields = function(paxType,chargeType){
	if(chargeType == "V"){
		if($("input[name='radCurrFareRule']:checked").attr("id") == "radSelCurrFareRule"){
			ManageFareClasses.clearCurrencyFields(false,paxType);
			$("#txt"+paxType+"NoShoCharge").prop("readonly",true);
			$("#txt"+paxType+"NoShoChargeInLocal").prop("readonly",false);
			$("#txt"+paxType+"NoShoBreakPoint").prop("readonly",true);
			$("#txt"+paxType+"NoShoBoundary").prop("readonly",true);
		} else {
			ManageFareClasses.clearCurrencyFields(true,paxType);
			$("#txt"+paxType+"NoShoCharge").prop("readonly",false);
			$("#txt"+paxType+"NoShoChargeInLocal").prop("readonly",true);
			$("#txt"+paxType+"NoShoBreakPoint").prop("readonly",true);
			$("#txt"+paxType+"NoShoBoundary").prop("readonly",true);
		}
	} else {
		$("#txt"+paxType+"NoShoChargeInLocal").val("0.00");
		$("#txt"+paxType+"NoShoCharge").prop("readonly",false);
		$("#txt"+paxType+"NoShoChargeInLocal").prop("readonly",true);
		$("#txt"+paxType+"NoShoBreakPoint").prop("readonly",false);
		$("#txt"+paxType+"NoShoBoundary").prop("readonly",false);
	}
					
}

ManageFareClasses.addProrationHandler=function(){
	$("#selOrigin").on("change", function(){
		ManageFareClasses.controlProrationButton();
		if(isChecked('radSelCurr')){
			ManageFareClasses.getAirportCurrency($("#selOrigin").val());
		}
	});
	$("#selDestination").on("change", function(){
		ManageFareClasses.controlProrationButton();
	});
	$("#selVia1").on("change", function(){
		ManageFareClasses.controlProrationButton();
	});
	$("#selVia2").on("change", function(){
		ManageFareClasses.controlProrationButton();
	});
	$("#selVia3").on("change", function(){
		ManageFareClasses.controlProrationButton();
	});
	$("#selVia4").on("change", function(){
		ManageFareClasses.controlProrationButton();
	}); 
}

ManageFareClasses.showHideSelectedCurrency = function(){
	if($("input[name='radCurrFareRule']:checked").attr("id")=="radSelCurrFareRule"){
		$("#selCurrencyCode").css("visibility", "visible");
		$("#selCurrencyCode").prop('selectedIndex', 0);
		if($("#selADCGTYP").val() == "V"){
			ManageFareClasses.clearCurrencyFields(false,"AD");
		}
		if($("#selCHCGTYP").val() == "V"){
			ManageFareClasses.clearCurrencyFields(false,"CH");		
		}
		if($("#selINCGTYP").val() == "V"){
			ManageFareClasses.clearCurrencyFields(false,"IN");	
		}
	}else{
		$("#selCurrencyCode").css("visibility", "hidden");
		if($("#selADCGTYP").val() == "V"){
			ManageFareClasses.clearCurrencyFields(true,"AD");
		}
		if($("#selCHCGTYP").val() == "V"){
			ManageFareClasses.clearCurrencyFields(true,"CH");		
		}
		if($("#selINCGTYP").val() == "V"){
			ManageFareClasses.clearCurrencyFields(true,"IN");	
		}		
	}
	ManageFareClasses.enableDisableChargeCurrencyFields($("#selDefineCanType").val(),"canc");
	ManageFareClasses.enableDisableChargeCurrencyFields($("#selDefineModType").val(),"mod");	
	if(FareClassValidation.isEnableFareRuleNCC){
		ManageFareClasses.enableDisableChargeCurrencyFields($("#selDefineNCCType").val(),"ncc");
	}	
	ManageFareClasses.enableDisableNoshoCurrencyFields("AD",$("#selADCGTYP").val());	
	ManageFareClasses.enableDisableNoshoCurrencyFields("CH",$("#selCHCGTYP").val());
	ManageFareClasses.enableDisableNoshoCurrencyFields("IN",$("#selINCGTYP").val());
};

ManageFareClasses.ckeckMasterFare = function(e){
	if ($(e.target).prop("checked")){
		$("#masterFareRefCode").enable();
		$("#chkLinkFare, #btnLinkFares, #selectedMasterFareID, #linkFarePercentage").disable();
	}else{
		$("#masterFareRefCode").val("").enable();
		$("#chkLinkFare, #selectedMasterFareID, #linkFarePercentage").enable();
	}
	
};

ManageFareClasses.ckeckLinkFare = function(e){
	if ($(e.target).prop("checked")){
		$("#masterFareRefCode").val();
		$("#masterFareRefCode,#chkMasterFare").disable();
		$("#divDateOutbound,#divDateInbound,#divSalesDates,#divFareAmounts").hide();
		$("#btnLinkFares").enable();
	}else{
		$("#masterFareRefCode,#chkMasterFare").enable();
		$("#divDateOutbound,#divDateInbound,#divSalesDates,#divFareAmounts").show();
	}
	
};

ManageFareClasses.goBack = function(e){
	var delimiter = "^";
	var childScreenId = "SC_INVN_020"
	if ((top[1].objTMenu.tabIsAvailable(childScreenId) && top[1].objTMenu.tabRemove(childScreenId))			
			|| (!top[1].objTMenu.tabIsAvailable(childScreenId))) {		
		if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {	
			top[0].ruleStatus = "";
			var temp = getTabValues(screenId, delimiter, "strSearchCriteria");
			setTabValues(childScreenId, delimiter, "strSearchCriteria", temp);
			top[1].objTMenu.tabSetTabInfo(screenId, childScreenId, "showLoadFareJsp!showSetupFare.action", "Setup Fare", false);
			location.replace("showLoadFareJsp!showSetupFare.action");
			setPageEdited(false);
		}
	}
};

ManageFareClasses.getProrationData = function(){
	return ManageFareClasses.prorationData;
};
ManageFareClasses.UIChanges = function(){
	if (FareClassValidation.isLinkFareEnabled){
		$("#linkFareDiv").show();
	}
	if (ManageFareClasses.fareDiscountDiplay){
		$("#tblFareDiscount").show(); 
	}
	if(ManageFareClasses.openReturnSupport){
		$(".open-return-sup").css("visibility", "visible");
	}
	if(ManageFareClasses.halfReturnSupport){
		$(".half-return-sup").css("visibility", "visible");
	}
	if(ManageFareClasses.enableFareInDepAirPCurr){
		$(".currency-loacal").show();
		$(".selected-currency").show();
	}
	if (ManageFareClasses.domesticEnabled){
		$(".domestic-show").show();
	}
	if (ManageFareClasses.enableAgentInstruction){
		$("#tblTextInstructions").show(); 
	}
	if (ManageFareClasses.enableAgentCommission){
		$("#tblAgentCommission").show(); 
	}
	if (!ManageFareClasses.enableAgentCommission){
		$("#tblAgentCommission").hide(); 
	}
	if (FareClassValidation.isEnableSameFareChecking){
		$("#divModToSameFare").show(); 
	}
	if (FareClassValidation.isEnableFareRuleNCC){
		$("#trFareRuleNCC").show(); 
	}
	
	if(ManageFareClasses.isEnableProration){
		$("#btnAddProration").show();
		if(ManageFareClasses.prorationData!=null && ManageFareClasses.prorationData.length >0){
			$("#btnAddProration").attr('value','Modify Proration');				
			ManageFareClasses.prorationData.sort(ManageFareClasses.sortProration);
			ManageFareClasses.controlProrationButton();
			ManageFareClasses.isProrationModify=true;
			ManageFareClasses.isProDeleteEnable=true;
		}else{	
			$("#btnAddProration").attr('value','Add Proration');
			ManageFareClasses.isProrationModify=false;		
			ManageFareClasses.isProDeleteEnable=false;
			ManageFareClasses.controlProrationButton();
		}
		
	}else{
		$("#btnAddProration").hide();		
	}

		
}

ManageFareClasses.loadPageInitialData = function() {
	//$("#tabs").hide();
	var url = "fareRule!createPageInitailData.action";
	var data = {};
	$.ajax({type: "POST", dataType: 'json',data: data , url:url, beforeSend: top[2].ShowProgress(),		
	success: ManageFareClasses.loadPageInitialDataProcess, error:ManageFareClasses.errorLoading, cache : false});
	return false;
}


ManageFareClasses.loadPageInitialDataProcess = function(response) {
	if (isEmpty(response)) {
		//Load Network connection failure error
		ManageFareClasses.errorLoading();
		return false;
	}
		
	if (response.success) {
		$("#selFareRuleCode").fillDropDown({dataArray:response.fareInfo.fareRuleIdTypeList, keyIndex:1, valueIndex:0, firstEmpty:false});
		$("#visibility").fillDropDown({dataArray:response.fareInfo.fareVisibilityList, keyIndex:0, valueIndex:1, firstEmpty:false});
		$("#selFareCat").fillDropDown({dataArray:response.fareInfo.fareCategoryList, keyIndex:0, valueIndex:1, firstEmpty:false});
		ManageFareClasses.flexiList = response.fareInfo.flexiCategoryList;
		$("#selFlexiCode").empty();
		$("#selFlexiCode").fillDropDown({dataArray:filterFlexiList('OW'), keyIndex:0, valueIndex:1, firstEmpty:true});
		$("#selPaxCat").fillDropDown({dataArray:response.fareInfo.paxCategoryList, keyIndex:0, valueIndex:1, firstEmpty:false});		
		$("#selDefineModType, #selDefineCanType").fillDropDown({dataArray:response.fareInfo.fareModValueTypes, keyIndex:0, valueIndex:1, firstEmpty:false});
		$("#selADCGTYP, #selCHCGTYP, #selINCGTYP").fillDropDown({dataArray:response.fareInfo.paxChargeTypes, keyIndex:0, valueIndex:1, firstEmpty:false});
		$("#selCurrencyCode").fillDropDown({dataArray:response.fareInfo.currencyCodeList, keyIndex:1, valueIndex:0, firstEmpty:false});
		FareClassValidation.errorInfo = response.errorInfo;
		FareClassValidation.milliBufferTime = {DOM:response.fareInfo.domesticModBufferTime, INT:response.fareInfo.internationalModBufferTime};
		FareClassValidation.isLinkFareEnabled = response.fareInfo.enableLinkFare;
		FareClassValidation.isEnableSameFareChecking = response.fareInfo.enableSameFareChecking;
		FareClassValidation.chargeRestrictionDefault = {modtype:"V",cantype:"V", ncctype:"V", modval:response.fareInfo.defaultModCharge,canvalue:response.fareInfo.defaultCnxCharge, nccvalue: "0.00",modvallocal:"0.00",canvaluelocal:"0.00", nccvallocal:"0.00"};
		FareClassValidation.isEnableFareRuleNCC = response.fareInfo.enableFareRuleNCC;
		FareClassValidation.minRequiredSeatsForBTRes = response.fareInfo.minRequiredSeatsForBTRes;
		if(FareClassValidation.isEnableFareRuleNCC){
			$("#selDefineNCCType").fillDropDown({dataArray:response.fareInfo.fareModValueTypes, keyIndex:0, valueIndex:1, firstEmpty:false});
		}
		ManageFareClasses.enableFareInDepAirPCurr = response.fareInfo.enableFareInDepAirPCurr;
		ManageFareClasses.fareDiscountDiplay = response.fareInfo.enableFareDiscount;
		ManageFareClasses.openReturnSupport = response.fareInfo.openReturnSupport;
		ManageFareClasses.halfReturnSupport = response.fareInfo.halfReturnSupport;
		ManageFareClasses.domesticEnabled = response.fareInfo.enableDomesticBufferTime;
		ManageFareClasses.enableAgentInstruction = response.fareInfo.enableAgentInstruction;
		ManageFareClasses.enableAgentCommission = response.fareInfo.enableAgentCommission;
		ManageFareClasses.UIChanges();
		var delimiter = "^";
		var searchCri = getTabValues(screenId, delimiter, "strSearchCriteria");
		if (searchCri!=""){
			var fareRuleCode = searchCri.split(",")[0], status = searchCri.split(",")[1]
			$("#selFareRuleCode").val(fareRuleCode.split("|")[1]);
			$("#selStatus").val(status.split("|")[1]);
		}
		ManageFareClasses.loadGridDataProcess();
	} else {
		ManageFareClasses.errorLoading(response.messageTxt);
	}
	top[2].HideProgress();
}

ManageFareClasses.loadFarePageData = function() {
	var url = "fare!createPageInitailData.action";
	var data = {};
	if (ManageFareClasses.FAREMODE.MODIFY) {
		data["fareID"] = $("#fareID").val();
	}
	$.ajax({type: "POST", dataType: 'json',data: data , url:url, beforeSend: top[2].ShowProgress(),		
	success: ManageFareClasses.loadFarePageInitialDataProcess, error:ManageFareClasses.errorLoading, cache : false});
	return false;
}

function filterFlexiList(flg){
	var t=[]
	for (var i = 0;i<ManageFareClasses.flexiList.length;i++){
		if (ManageFareClasses.flexiList[i][1].indexOf(flg)>-1){
			t[t.length] = ManageFareClasses.flexiList[i];
		}
	}
	return t;
}

ManageFareClasses.loadFarePageInitialDataProcess = function(response) {
	if (isEmpty(response)) {
		//Load Network connection failure error
		ManageFareClasses.errorLoading();
		return false;
	}
		
	if (response.success) {
		$("#selFC").fillDropDown({dataArray:response.fare.fareRuleIdTypeList, keyIndex:1, valueIndex:0, firstEmpty:false});
		$("#visibility").fillDropDown({dataArray:response.fare.fareVisibilityList, keyIndex:0, valueIndex:1, firstEmpty:false});
		$("#selFareCat").fillDropDown({dataArray:response.fare.fareCategoryList, keyIndex:0, valueIndex:1, firstEmpty:false});
		ManageFareClasses.flexiList = response.fare.flexiCategoryList;
		$("#selFlexiCode").empty();
		$("#selFlexiCode").fillDropDown({dataArray:filterFlexiList('OW'), keyIndex:0, valueIndex:1, firstEmpty:true});
		$("#selPaxCat").fillDropDown({dataArray:response.fare.paxCategoryList, keyIndex:0, valueIndex:1, firstEmpty:false});		
		$("#selDefineModType, #selDefineCanType").fillDropDown({dataArray:response.fare.fareModValueTypes, keyIndex:0, valueIndex:1, firstEmpty:false});
		$("#selADCGTYP, #selCHCGTYP, #selINCGTYP").fillDropDown({dataArray:response.fare.paxChargeTypes, keyIndex:0, valueIndex:1, firstEmpty:false});
		$("#selOrigin").fillDropDown({dataArray:response.fare.viaPointList, keyIndex:0, valueIndex:1, firstEmpty:false});
		$("#selDestination").fillDropDown({dataArray:response.fare.viaPointList, keyIndex:0, valueIndex:1, firstEmpty:false});
		$("#selVia1").fillDropDown({dataArray:response.fare.viaPointList, keyIndex:0, valueIndex:1, firstEmpty:false});
		$("#selVia2").fillDropDown({dataArray:response.fare.viaPointList, keyIndex:0, valueIndex:1, firstEmpty:false});
		$("#selVia3").fillDropDown({dataArray:response.fare.viaPointList, keyIndex:0, valueIndex:1, firstEmpty:false});
		$("#selVia4").fillDropDown({dataArray:response.fare.viaPointList, keyIndex:0, valueIndex:1, firstEmpty:false});
		$("#selBc").fillDropDown({dataArray:response.fare.bookingCodeList, keyIndex:0, valueIndex:1, firstEmpty:false});
		$("#selCurrencyCode").fillDropDown({dataArray:response.fare.currencyCodeList, keyIndex:1, valueIndex:0, firstEmpty:false});
		FareClassValidation.isLinkFareEnabled = response.fare.enableLinkFare;
		FareClassValidation.isEnableSameFareChecking = response.fare.enableSameFareChecking;
		FareClassValidation.isEnableFareRuleNCC = response.fare.enableFareRuleNCC;
		if(FareClassValidation.isEnableFareRuleNCC){
			$("#selDefineNCCType").fillDropDown({dataArray:response.fare.fareModValueTypes, keyIndex:0, valueIndex:1, firstEmpty:false});
		}
		ManageFareClasses.enableFareInDepAirPCurr = response.fare.enableFareInDepAirPCurr
		ManageFareClasses.fareDiscountDiplay = response.fare.enableFareDiscount;
		ManageFareClasses.openReturnSupport = response.fare.openReturnSupport;
		ManageFareClasses.halfReturnSupport = response.fare.halfReturnSupport;
		ManageFareClasses.domesticEnabled = response.fare.enableDomesticBufferTime;
		ManageFareClasses.errorInfo = response.errorInfo;
		FareClassValidation.errorInfo = response.errorInfo;
		FareClassValidation.milliBufferTime = {DOM:response.fare.domBufferTimeInMilSec, INT:response.fare.bufTimeInMilSec};
		FareClassValidation.chargeRestrictionDefault = {modtype:"V",cantype:"V", ncctype:"V", modval:response.fare.modCharge,canvalue:response.fare.cnxCharge, nccvalue: "0.00",modvallocal:"0.00",canvaluelocal:"0.00", nccvallocal:"0.00"};
		ManageFareClasses.sysDate = response.fare.systemDate;
		ManageFareClasses.isEnableProration=response.enableProration;
		
		if (ManageFareClasses.FAREMODE.MODIFY) {
			ManageFareClasses.setFareAdd(response.fareTO);
			ManageFareClasses.addFareRuleData(response.fareRuleData, null);
			ManageFareClasses.setFareRuleFormData(response.fareRuleData);
			ManageFareClasses.prorationData=response.prorationData;
			
		} else {
			var delimiter = "^";
			ManageFareClasses.setPaxConfigData(response.fare.farePaxConfig);
			var searchCri = getTabValues(screenId, delimiter, "strSearchCriteria");
			if(searchCri != ""){
				searchCri = eval("("+searchCri+")");
			}			
			if(!$.isEmptyObject(searchCri)){
				$("#selOrigin").val(searchCri.originAirport);
				$("#selDestination").val(searchCri.destinationAirport);
				$("#selVia1").val(searchCri.via1);
				$("#selVia2").val(searchCri.via2);
				$("#selVia3").val(searchCri.via3);
				$("#selVia4").val(searchCri.via4);
				$("#txtFBC").val(searchCri.fareBasisCode);
				if (searchCri.status=="ACT" || searchCri.status=="All"){
					$("#chkStatus").prop("checked", true);
				}
				if (searchCri.bookingClassCode!=""){
					var dd = document.getElementById('selBc');
					dd.selectedIndex = 0;
					for (var i = 0; i < dd.options.length; i++) {
					    if (dd.options[i].text === searchCri.bookingClassCode) {
					        dd.selectedIndex = i;
					        break;
					    }
					}
					if (dd.selectedIndex!=0){
						$("#selBc").change();
					}
				}
				if (searchCri.fareRuleCode!=""){
					var dd = document.getElementById('selFC');
					dd.selectedIndex = 0;
					for (var i = 0; i < dd.options.length; i++) {
					    if (dd.options[i].text === searchCri.fareRuleCode) {
					        dd.selectedIndex = i;
					        break;
					    }
					}
					if (dd.selectedIndex!=0){
						$("#selFC").change();
					}
				}
			} 
		}
		ManageFareClasses.UIChanges();
	} else {
		ManageFareClasses.errorLoading(response.messageTxt);
	}
	top[2].HideProgress();
}

ManageFareClasses.setPaxConfigData = function(paxConfigs) {
	if (paxConfigs != null) {
		for (var i = 0; i < paxConfigs.length; i++) {
			if (paxConfigs[i].paxTypeCode == 'AD') {
				$("#txtAED").val(paxConfigs[i].fareAmount);
			} else if (paxConfigs[i].paxTypeCode == 'CH') {
				$("#txtCHL").val(paxConfigs[i].fareAmount);
				$("#selCamt").val(paxConfigs[i].fareAmountType);
			} else if (paxConfigs[i].paxTypeCode == 'IN') {
				$("#txtINF").val(paxConfigs[i].fareAmount);
				$("#selIamt").val(paxConfigs[i].fareAmountType);
			}
		}		
	}
}


ManageFareClasses.loadFareRule = function() {
	if($("#selFC>option:selected").text() == ""){
		showERRMessage(ManageFareClasses.errorInfo.fareclassRqrd);
		$("#selFC").focus();
	} else {
		var url = "fare!loadFareRule.action";
		var data = {};
		data['fareRuleID'] = getFareRuleTmpId();
		$.ajax({type: "POST", dataType: 'json',data: data , url:url,		
		success: ManageFareClasses.loadFareRuleProcess, error:ManageFareClasses.errorLoading, cache : false});
		return false;
	}
}

ManageFareClasses.loadFareRuleProcess = function(response) {
	if (isEmpty(response)) {
		//Load Network connection failure error
		ManageFareClasses.errorLoading();
		return false;
	}
		
	if (response.success) {
		FareClassValidation.enableAll("NEW");
		ManageFareClasses.addFareRuleData(response.fareRuleData, null);
		ManageFareClasses.setFareRuleFormData(response.fareRuleData);
		FareClassValidation.disableAll();
		$("#btnEditRule").enable();
	} else {
		ManageFareClasses.errorLoading(response.messageTxt);
	}
	if (ManageFareClasses.FAREMODE.SAVE) {		
		$("#btnSave").enable();		
	}
}

ManageFareClasses.setFareRuleFormData = function(fare) {
	if (fare != null) {	
		$("#fareRuleID").val(fare.fareRuleID);
		$("#comments").val(fare.comments);	
		if(fare.minStayOver != null){
			var minStay = fare.minStayOver.split(":");
			$("#minStayOver_MM").val(minStay[0]);
			$("#minStayOver_DD").val(minStay[1]);
			$("#minStayOver_HR").val(minStay[2]);
			$("#minStayOver_MI").val(minStay[3]);
		}
		if(fare.maxStayOver != null){
			var maxStay = fare.maxStayOver.split(":");
			$("#maxStayOver_MM").val(maxStay[0]);
			$("#maxStayOver_DD").val(maxStay[1]);
			$("#maxStayOver_HR").val(maxStay[2]);
			$("#maxStayOver_MI").val(maxStay[3]);
		}
		if(fare.ortPeriod != null && fare.ortPeriod != "0:0:0:0"){
			var ortCnf = fare.ortPeriod.split(":");
			$("#ortConfirm_MM").val(ortCnf[0]);
			$("#ortConfirm_DD").val(ortCnf[1]);
			$("#ortConfirm_HR").val(ortCnf[2]);
			$("#ortConfirm_MI").val(ortCnf[3]);
		}
			
		$("#selFareCat").val(fare.fareCat);
		$("#selPaxCat").val(fare.paxCat);
		$("#selFlexiCode").val(fare.flexiCode);
		
		if(fare.modBufferTimeInt != null && fare.modBufferTimeInt != "0:0:0"){
			var modBuffer = fare.modBufferTimeInt.split(":");
			$("#intnModBuffertime_DD").val(modBuffer[0]);
			$("#intnModBuffertime_HR").val(modBuffer[1]);
			$("#intnModBuffertime_MI").val(modBuffer[2]);
		}
		
		if(fare.modBufferTimeDom != null && fare.modBufferTimeDom != "0:0:0"){
			var domBuffer = fare.modBufferTimeDom.split(":");
			$("#domModBuffertime_DD").val(domBuffer[0]);
			$("#domModBuffertime_HR").val(domBuffer[1]);
			$("#domModBuffertime_MI").val(domBuffer[2]);
		}
		
		$("#txtFBC").val(fare.fareBasisCode);
	}
}

ManageFareClasses.setPageInitData = function() {
	$("select[name='visibility'] option[value='1']").attr("selected", true);
	$("#chkStatusActive").prop("checked", true);
	$("#radCurrFareRule").attr("checked", "checked");
	$("#addAllLeft_lstAgents").trigger('click');
	$("#addAllLeft_lstPOS").trigger('click');
	ManageFareClasses.showHideSelectedCurrency();
}

ManageFareClasses.searchData = function() {	
	/*var newUrl = 'fareRuleSearch.action?';
 	newUrl += 'fareRuleCode=' + $("#selFareRuleCode").val();
 	newUrl += '&status=' + $("#selStatus").val();
 	
 	$("#fareRulePager").clearGridData();
 	$.getJSON(newUrl, function(response){ 
 		$("#fareRuleGrid")[0].addJSONData(response); 		
 	});*/
 	ManageFareClasses.loadGridDataProcess()
}

ManageFareClasses.errorLoading = function (response){
	 top[2].HideProgress();
	alert(response);
};
ManageFareClasses.loadGridDataProcess = function(){
	var mydata = null;
	$("#fareRuleGridContainer")
	.empty()
	.append($('<table id="fareRuleGrid"></table>'),$('<div id="fareRulePager"></div>'));
	$("#fareRuleGrid").jqGrid({        
		//url:'fareRuleSearch.action',	
		//datatype: "local",
		datatype: function(postdata) {
			postdata['fareRuleCode'] = $("#selFareRuleCode>option:selected").text();
			postdata['status'] = $("#selStatus").val();				
	        jQuery.ajax({
	           url: 'fareRuleSearch.action',
	           data:postdata,
	           dataType:"json",
	           complete: function(jsonData,stat){	        	  
	              if(stat=="success") {
	            	 mydata = eval("("+jsonData.responseText+")");
	            	 ManageFareClasses.gridDateCopy = mydata.rows;
	            	 $("#fareRuleGrid")[0].addJSONData(mydata);            	
	              }
	           }
	        });
	    },
		//datatype: "json",
		colNames:['&nbsp;','Rule Code','Description', 'Fare Basis Code', 'One Way/Return','Charge Rest','Advance Booking',
		          'Valid Days of Week','Visibility','Agt FR','Status','Pax Cat.','fareid','validDaysOfTime',
		          'loadFactor','intnModBuffertime', 'domModBuffertime','allowHalfRT','ortConfirm','ortPeriod','maxStayOver','minStayOver','flexiCode','fareCat',
		          'printExp','allowModDate','allowModOND','version','fareRuleID', 'comments', 'Bulk Ticket', 'Min. Requested Seats'],
	   	colModel:[  {name:'id', width:30,label:"id"},
	   	            {name:'ruleCode',index:'ruleCode', width:55 , align:"left" },
	   	            {name:'description', index:'description', width:130 ,align:"left" },
	   	            {name:'fareBasisCode', width:80, align:"left" },
	   	            {name:'oneWayReturn', width:75, align:"center" },
	   	            {name:'chargeRest', width:80, align:"center" },
	   	            {name:'advanceBooking', width:70, align:"center" },		
	   	            {name:'validDaysOfWeek', width:150, align:"left" },
	   	            {name:'visibility', width:70, align:"center" },
	   	            {name:'agtFR', width:50, align:"center" },
	   	            {name:'status', width:70, align:"center" },
	   	            {name:'paxCat', width:50, align:"center" },
	   	            {name:'fareid', width:50, hidden:true },
	   	            {name:'validDaysOfTime', width:50, hidden:true},
	   	            {name:'loadFactor', width:50, hidden:true },
	   	            {name:'intnModBuffertime', width:50, hidden:true },
	   	            {name:'domModBuffertime', width:50, hidden:true },
	   	            {name:'allowHalfRT', width:50, hidden:true },
	   	            {name:'ortConfirm', width:50, hidden:true },
	   	            {name:'ortPeriod', width:50, hidden:true },
	   	            {name:'maxStayOver', width:50, hidden:true },
	   	            {name:'minStayOver', width:50, hidden:true },
	   	            {name:'flexiCode', width:50, hidden:true },
	   	            {name:'fareCat', width:50, hidden:true },
	   	            {name:'printExp', width:50, hidden:true },
	   	            {name:'allowModDate', width:50, hidden:true },
	   	            {name:'allowModOND', width:50, hidden:true },
	   	            {name:'version', width:50, hidden:true },
	   	            {name:'fareRuleID', width:50, hidden:true },
	   	            {name:'comments', width:50, hidden:true },
	   	            {name:'bulkTicket', width:50, hidden:true },
	   	            {name:'minBulkTicketSeatCount', width:50, hidden:true }
	   	],
		height: 95,//height: 230 for full view
	   	rowNum:10,
	   	jsonReader: { 
			root: "rows",
		  	page: "page",
		  	total: "total",
		  	records: "records",
			repeatitems: false,
			id: "0" 
		},
	    //pager: '#fareRulePager',
	   	pager: jQuery('#fareRulePager'),
	   	sortname: 'id',
		multiselect: false,
		viewrecords: true,
	    sortorder: "desc",
	    caption:"Fare Rules",
	    loadui:'block',
	    onSelectRow: function(rowid){
	    	ManageFareClasses.selectedRow = rowid;
	    	ManageFareClasses.mode = "";
	    	ManageFareClasses.FillGridToForm(rowid, mydata.rows);
	    	//  });
	    }
	}).navGrid("#fareRulePager",{refresh: true, edit:false,add:false,del:false});
	ManageFareClasses.selectedRow=null
	/*for(var i=0;i<=mydata.length;i++){
		jQuery("#fareRuleGrid").jqGrid('addRowData',i+1,mydata[i]);
	}*/
   
};


ManageFareClasses.editPage = function (){
	ManageFareClasses.rowEditFlag = true;
};

ManageFareClasses.editSelRow = function(){
	if (ManageFareClasses.selectedRow!=null){
		FareClassValidation.enableAll("EDIT");
		if($("#radOneWay").prop("checked")){
			$("#radOpReturn").disable();
			$("#radHalfReturn").disable();
			FareClassValidation.disableEnable("radOneWay");
		}
		ManageFareClasses.setOPENRTValue();
	}else{
		showERRMessage("Select a row to edit");
	}
};

ManageFareClasses.addNewRule = function(){
	if (ManageFareClasses.rowEditFlag){
		var x = confirm ("Changes will be lost! Do you wish to Continue?");
		if (x){
			ManageFareClasses.rowEditFlag = false;
		}
	}
	if (!ManageFareClasses.rowEditFlag){
		//$("#fareRuleGrid").parent().parent().animate({height:95},
	    	 // function() {
	    	//	$("#tabs").show();
				FareClassValidation.enableAll("NEW");
				ManageFareClasses.setPageInitData();
				ManageFareClasses.enableDisableNoshoCurrencyFields("AD","V");
				ManageFareClasses.enableDisableNoshoCurrencyFields("CH","V");
				ManageFareClasses.enableDisableNoshoCurrencyFields("IN","V");
				FareClassValidation.disableEnable("radOneWay");
				if(FareClassValidation.isLoadPOS){					
					$("#addAllRight_lstPOS").trigger('click');
				}
		//});
	}
	ManageFareClasses.enableDisableChargeCurrencyFields($("#selDefineCanType").val(),"canc");			
	ManageFareClasses.enableDisableChargeCurrencyFields($("#selDefineModType").val(),"mod");				
	if(FareClassValidation.isEnableFareRuleNCC){
		ManageFareClasses.enableDisableChargeCurrencyFields($("#selDefineNCCType").val(),"ncc");
	}	
	jQuery("#fareRuleGrid").jqGrid('resetSelection');
};


ManageFareClasses.FillGridToForm = function(id,dataObj){
	if (id==null){
		$("#chkChrgRestrictions").prop("checked",false);
		ManageFareClasses.addHocRuleSetup();
		$("#chkChrgRestrictions").click();
		$("#selADCGTYP").val("V");
		$("#selCHCGTYP").val("V");
		$("#selINCGTYP").val("V");
		$("#txtADNoShoCharge, #txtADNoShoBreakPoint,#txtADNoShoBoundary").val("");
		$("#txtCHNoShoCharge, #txtCHNoShoBreakPoint,#txtCHNoShoBoundary").val("");
		$("#txtINNoShoCharge, #txtINNoShoBreakPoint,#txtINNoShoBoundary").val("");
	
	}else{
		if (ManageFareClasses.rowEditFlag){
			var x = confirm ("Changes will be lost! Do you wish to Continue?");
			if (x){
				ManageFareClasses.rowEditFlag = false;
			}
		}
		if (!ManageFareClasses.rowEditFlag){
			FareClassValidation.enableAll("NEW");
			var tempAppModel = jQuery.extend(true, {},
					ManageFareClasses.buildApplicableModel(dataObj[(getGridRowIndex(id)-1)].appModel));
			FareClassValidation.disableAll();
			FareClassValidation.screenInitilize();
			$("#fareRuleGrid").GridToForm(id, '#frmFareClasses');
			var rowData = jQuery("#fareRuleGrid").jqGrid('getRowData',id);
			ManageFareClasses.addFareRuleData(rowData,tempAppModel, id, dataObj)
		}
	}
};

ManageFareClasses.addFareRuleData = function(rowData, appModel,id, dataObj) {	
	if (rowData.oneWayReturn == "One Way"){
		$("#radOneWay").attr("checked", true);
		$("#selFlexiCode").empty();
		$("#selFlexiCode").fillDropDown({dataArray:filterFlexiList('OW'), keyIndex:0, valueIndex:1, firstEmpty:true});
	}else if (rowData.oneWayReturn == "Return"){
		$("#radReturn").attr("checked", true);
		$("#selFlexiCode").empty();
		$("#selFlexiCode").fillDropDown({dataArray:filterFlexiList('RT'), keyIndex:0, valueIndex:1, firstEmpty:true});
	}else{
		$("#radOpReturn").attr("checked", true);
		$("#selFlexiCode").empty();
		$("#selFlexiCode").fillDropDown({dataArray:filterFlexiList('RT'), keyIndex:0, valueIndex:1, firstEmpty:true});
	}
	
	if (rowData.status == "Active"){
		$("#chkStatusActive").attr("checked", true);
	}else{
		$("#chkStatusActive").attr("checked", false);
	}
	if (rowData.ortConfirm == "Y"){
		$("#radOpReturn").attr("checked", true);
		FareClassValidation.isOpenReturn = true;
	}else{
		$("#radOpReturn").attr("checked", false);
	}
	if (rowData.allowHalfRT == "true" || rowData.allowHalfRT == true){
		$("#radHalfReturn").attr("checked", true);
	}else{
		$("#radHalfReturn").attr("checked", false);
	}
	fillInOutDaysofWeek = function (value){
		$("#outBoundType").find("input[type='checkbox']").attr("checked", false);
		$("#inBoundType").find("input[type='checkbox']").attr("checked", false);
		var value = value.replace("<br>","|");
		var inWay = "";
		var outWay = "";
		if (value.indexOf("|") > -1){
			ways = value.split("|");
			inWay = ways[0];
			outWay = ways[1];
		}
		if (inWay != ""){
			if (inWay.indexOf("-") > -1){
				var temp = inWay.split("-");
				if (temp[0] == "Out"){
					if (temp[1].indexOf("Sa") != -1) {
						$("#chkOutSat").attr("checked", true);
					}
					if (temp[1].indexOf("Mo") != -1) {
						$("#chkOutMon").attr("checked", true);
					}
					if (temp[1].indexOf("Tu") != -1) {
						$("#chkOutTues").attr("checked", true);
					}
					if (temp[1].indexOf("We") != -1) {
						$("#chkOutWed").attr("checked", true);
					}
					if (temp[1].indexOf("Fr") != -1) {
						$("#chkOutFri").attr("checked", true);
					}
					if (temp[1].indexOf("Th") != -1) {
						$("#chkOutThu").attr("checked", true);
					}
					if (temp[1].indexOf("Su") != -1) {
						$("#chkOutSun").attr("checked", true);
					}
					if (temp[1].indexOf(":") != -1) {
						$("#outBoundFrom").val(temp[1]);
						$("#outBoundTo").val(temp[2]);
					}
				}
			}
		}
		if (outWay != ""){
			if (outWay.indexOf("-") > -1){
				var temp = outWay.split("-");
				if(temp[0] == "In"){
					if (temp[1].indexOf("Sa") != -1) {				
						$("#chkInbSat").attr("checked", true);
					}
					if (temp[1].indexOf("Mo") != -1) {				
						$("#chkInbMon").attr("checked", true);
					}
					if (temp[1].indexOf("Tu") != -1) {
						$("#chkInbTues").attr("checked", true);
					}
					if (temp[1].indexOf("We") != -1) {				
						$("#chkInbWed").attr("checked", true);
					}
					if (temp[1].indexOf("Fr") != -1) {
						$("#chkInbFri").attr("checked", true);
					}
					if (temp[1].indexOf("Su") != -1) {
						$("#chkInbSun").attr("checked", true);
					}
					if (temp[1].indexOf("Th") != -1) {
						$("#chkInbThu").attr("checked", true);
					}
					if (temp[1].indexOf(":") != -1) {
						$("#inBoundFrom").val(temp[1]);
						$("#inBoundTo").val(temp[2]);
					}
				}
			}
		}
	};
	if (rowData.allowModDate == "Y"){
		$("#modifyByDate").attr("checked", true);
	}else{
		$("#modifyByDate").attr("checked", false);
	}
	if (rowData.allowModOND == "Y"){
		$("#modifyByOND").attr("checked", true);
	}else{
		$("#modifyByOND").attr("checked", false);
	}
	if (rowData.printExp == "true"){
		$("#chkPrintExp").attr("checked", true);
	}else{
		$("#chkPrintExp").attr("checked", false);
	}
	if(rowData.paxCat != undefined){
		$("#selPaxCat").val(rowData.paxCat);
	}
	fillInOutDaysofWeek(rowData.validDaysOfTime);
	fillInOutDaysofWeek(rowData.validDaysOfWeek);
	//Charge Restriction
	if (rowData.chargeRest=="Y"){
		$("#chkChrgRestrictions").prop("checked", true);
	}else{
		$("#chkChrgRestrictions").prop("checked", false);
	}
	var fare = null;
	//appModel is null for the manage fare 
	if (appModel == null) {
		FareClassValidation.fareAppModel = ManageFareClasses.buildApplicableModel(rowData.appModel);
		fare = rowData;		
	} else {
		fare = dataObj[(getGridRowIndex(id)-1)];
		FareClassValidation.fareAppModel = appModel;		
	}
	if (isMultilingualCommentEnabled && dataObj!=undefined) {
		FareRuleComments.loadExistingComments(dataObj[(getGridRowIndex(id)-1)].fareRuleComments);
	}
	$("#selFlexiCode").val( fare.flexiCode )
	FareClassValidation.fareCNXPeirodModel = fare.appModelCNXData;
	FareClassValidation.fareMODPeirodModel = fare.appModelMODData;
	FareClassValidation.fareNCCPeirodModel = fare.appModelNCCData;

	var visibility = fare.visibilityCodes.split(",");		
	for(var i = 0; i < visibility.length; i++ ) {			
		$("select[name='visibility'] option[value='" + visibility[i] + "']").attr("selected", true);
	}
	var visibleAgents = fare.visibleAgents;
	FareClassValidation.agents = visibleAgents;
	if (visibleAgents != null && visibleAgents.length > 0) {		
		FareClassValidation.setFareVisibility($("#visibility"));
		if (FareClassValidation.isLoadAgents) {
			$("#lstAgents").multiSelect.loadRight(visibleAgents, 'lstAgents');
			ManageFareClasses.visibleAgents = null;			
		} else {
			ManageFareClasses.visibleAgents = visibleAgents; 
		}
	} else {
		//$.fn.multiSelect.allLeft();
	} 
	
	var posList = fare.pointOfSale;
	FareClassValidation.pos = posList;
	if (posList != null) {		
		FareClassValidation.setFareVisibility($("#visibility"));
		if (FareClassValidation.isLoadPOS) {
			if(fare.applicableToCountries != null && fare.applicableToCountries){
				$("#addAllRight_lstPOS").trigger('click');
			} else {	
				$("#lstPOS").multiSelect.loadRight(posList, 'lstPOS');
			}
			ManageFareClasses.pos = null;			
		} else {
			ManageFareClasses.pos = posList; 
		}
	}
	
	$("#chkAdvance").prop("checked", true);
	if(fare.advanceBooking != null && fare.advanceBooking != "0:0:0"){
		var advBkDays = fare.advanceBooking.split(":");
		$("#advanceBookingDays").val(advBkDays[0]);
		$("#advanceBookingHrs").val(advBkDays[1]);
		$("#advanceBookingMins").val(advBkDays[2]);
	}else{
		$("#advanceBookingDays").val(0);
		$("#advanceBookingHrs").val(0);
		$("#advanceBookingMins").val(0);
	}
	
	if(fare.minStayOver != null){
		var minStay = fare.minStayOver.split(":");
		$("#minStayOver_MM").val(minStay[0]);
		$("#minStayOver_DD").val(minStay[1]);
		$("#minStayOver_HR").val(minStay[2]);
		$("#minStayOver_MI").val(minStay[3]);
	}
	if(fare.maxStayOver != null){
		var maxStay = fare.maxStayOver.split(":");
		$("#maxStayOver_MM").val(maxStay[0]);
		$("#maxStayOver_DD").val(maxStay[1]);
		$("#maxStayOver_HR").val(maxStay[2]);
		$("#maxStayOver_MI").val(maxStay[3]);
	}
	if(fare.ortPeriod != null && fare.ortPeriod != "0:0:0:0"){
		var ortCnf = fare.ortPeriod.split(":");
		$("#ortConfirm_MM").val(ortCnf[0]);
		$("#ortConfirm_DD").val(ortCnf[1]);
		$("#ortConfirm_HR").val(ortCnf[2]);
		$("#ortConfirm_MI").val(ortCnf[3]);
	}
	
	if (fare.validDaysOfTime != null) {
		var arrTime = fare.validDaysOfTime.split(",");
		if (arrTime.length > 2) {
			$("#inBoundFrom").val(arrTime[2]);
			$("#inBoundTo").val(arrTime[3]);					
		} 
		if (arrTime.length >= 2){
			$("#outBoundFrom").val(arrTime[0]);
			$("#outBoundTo").val(arrTime[1]);	
		}
	}
	if (!isEmpty(fare.loadFactor)) {
		$("#loadFactorEnabled").check();
		var  loadData = fare.loadFactor.split("-");
		$("#loadFactorMin").val(loadData[0]);
		$("#loadFactorMax").val(loadData[1]);
	}
	
	if(fare.modBufferTimeInt != null && fare.modBufferTimeInt != "0:0:0"){
		var modBuffer = fare.modBufferTimeInt.split(":");
		$("#intnModBuffertime_DD").val(modBuffer[0]);
		$("#intnModBuffertime_HR").val(modBuffer[1]);
		$("#intnModBuffertime_MI").val(modBuffer[2]);
	}
	
	if(fare.modBufferTimeDom != null && fare.modBufferTimeDom != "0:0:0"){
		var domBuffer = fare.modBufferTimeDom.split(":");
		$("#domModBuffertime_DD").val(domBuffer[0]);
		$("#domModBuffertime_HR").val(domBuffer[1]);
		$("#domModBuffertime_MI").val(domBuffer[2]);
	}
	
	if (rowData.bulkTicket == "Y"){
		$("#chkBulkTicket").attr("checked", true);
		$("#chkBulkTicket").parent().parent().find(".mandatory").show();
	}else{
		$("#chkBulkTicket").attr("checked", false);
		$("#chkBulkTicket").parent().parent().find(".mandatory").hide();
	}
	
	$("#minBTSeatCount").val(rowData.minBulkTicketSeatCount);
	
	FareClassValidation.applyFareModelToCheckBox();
	//set chnages fields
	$("#txtModification").val(fare.modificationChargeAmount!=null?fare.modificationChargeAmount:"0.00");
	$("#txtCancellation").val(fare.cancellationChargeAmount!=null?fare.cancellationChargeAmount:"0.00");
	$("#txtNCC").val(fare.nameChangeChargeAmount!=null?fare.nameChangeChargeAmount:"0.00");
	$("#txtMin_mod").val(fare.minModChargeAmount!=null?fare.minModChargeAmount:"");
	$("#txtMax_mod").val(fare.maxModChargeAmount!=null?fare.maxModChargeAmount:"");
	$("#selDefineModType").val(fare.modificationChargeType);
	$("#selDefineCanType").val(fare.cancellationChargeType);
	$("#selDefineNCCType").val(fare.nameChangeChargeType);
	$("#txtCancellationInLocal").val(fare.cancellationChargeLocal!=null?fare.cancellationChargeLocal:"0.00");
	$("#txtModificationInLocal").val(fare.modificationChargeLocal!=null?fare.modificationChargeLocal:"0.00");
	$("#txtNCCInLocal").val(fare.nameChangeChargeLocal!=null?fare.nameChangeChargeLocal:"0.00");
	$("#txtMin_canc").val(fare.minCnxChargeAmount!=null?fare.minCnxChargeAmount:"");
	$("#txtMax_canc").val(fare.maxCnxChargeAmount!=null?fare.maxCnxChargeAmount:"");
	$("#txtMin_NCC").val(fare.minNCCAmount!=null?fare.minNCCAmount:"");
	$("#txtMax_NCC").val(fare.maxNCCAmount!=null?fare.maxNCCAmount:"");
	$("#hdnRuleCode").val(fare.ruleCode);
	$("#hdnRuleId").val(fare.fareid);
	$("#selCurrencyCode").val("");
	if (fare.localCurrencyCode!=null){
		$("#selCurrencyCode").val(fare.localCurrencyCode);
		$("#selCurrencyCode").css("visibility", "visible");
		$("#radSelCurrFareRule").attr("checked", true);
	}
	$("#txtaInstructions").val(fare.agentInstructions!=null?fare.agentInstructions:"");
	

   if(fare.agentCommissionType != null && trim(fare.agentCommissionType) != ""){
	   $("#selAgentComType").val(fare.agentCommissionType);
	   $("#txtAgentCommision").val(fare.agentCommissionAmount != null ? fare.agentCommissionAmount : "");
	   $("#agentCommissionAmountLocal").val(fare.agentCommissionAmountLocal != null ? fare.agentCommissionAmountLocal : "");
   }

	$("#hdnVersion").val(fare.version);
	if(fare.fareDiscountMin!=null || fare.fareDiscountMax!=null){
		$("#fareDiscount").prop("checked", true);
	} else {
		$("#fareDiscount").prop("checked", false);
	}
	$("#fareDiscountMin").val(fare.fareDiscountMin!=null?fare.fareDiscountMin:"");
	$("#fareDiscountMax").val(fare.fareDiscountMax!=null?fare.fareDiscountMax:"");
	if (fare.appModel.length!=0){
		$.each(fare.appModel, function(){
			$("#sel"+this.paxType+"CGTYP").val(this.noshowBasis);
			$("#txt"+this.paxType+"NoShoCharge").val(this.noshow!=null?this.noshow:"0.00") ;
			$("#txt"+this.paxType+"NoShoChargeInLocal").val(this.noshowLocal!=null?this.noshowLocal:"0.00");
			$("#txt"+this.paxType+"NoShoBreakPoint").val(this.noshowBreak!=null?this.noshowBreak:"");
			$("#txt"+this.paxType+"NoShoBoundary").val(this.noshowBoundry!=null?this.noshowBoundry:"");
		})
	}
	
	$("#selCurrencyCode").val("");
	if (fare.localCurrencyCode != null){
		var myOptions = {
			    val1 : fare.localCurrencyCode,
			    val2 : fare.localCurrencyCode
			};
		$.each(myOptions, function(val, text) {
		    $('#selCurrencyCode').append( new Option(text,val) );
		});
		$("#selCurrencyCode").val(fare.localCurrencyCode);
	}	
	if(isChecked('radSelCurr')){
		$("#txtBaseInLocal").enable();
		$("#txtCHFareInLocal").enable();
		$("#txtINFInLocal").enable();
		$("#txtAED").disable();
		$("#txtCHL").disable();
		$("#txtINF").disable();
	}else{
		$("#txtBaseInLocal").disable();
		$("#txtCHFareInLocal").disable();
		$("#txtINFInLocal").disable();
		$("#txtAED").enable();
		$("#txtCHL").enable();
		$("#txtINF").enable();
	}
	if(ManageFareClasses.FAREMODE.MODIFY){
		$("#radSelCurr").disable();
		$("#radCurr").disable();
	}
	
	if(fare.modBufferTimeInt!=undefined && fare.modBufferTimeInt!=null 
			&& fare.modBufferTimeInt != ""){
		var bufferTime = fare.modBufferTimeInt.split(":");
		$("#intnModBuffertime_DD").val(bufferTime[0]);
		$("#intnModBuffertime_HR").val(bufferTime[1]);
		$("#intnModBuffertime_MI").val(bufferTime[2]);		
	}
	if(fare.domModBuffertime!=undefined && fare.domModBuffertime!=null 
			&& fare.domModBuffertime != ""){
		var bufferTime = fare.domModBuffertime.split(":");
		$("#domModBuffertime_DD").val(bufferTime[0]);
		$("#domModBuffertime_HR").val(bufferTime[1]);
		$("#domModBuffertime_MI").val(bufferTime[2]);		
	}
	if(isChecked('radOpReturn')){
		FareClassValidation.enableDisableORTTime(false);
	}
	$("#btnEdit").enable();
}

ManageFareClasses.buildApplicableModel = function(appModel) {
	var model = FareClassValidation.fareAppModel;
	for (var i = 0; i < appModel.length; i++) {
		if (appModel[i].paxType == "AD") {
			model[0].checked = appModel[i].fare;
			model[3].checked = appModel[i].ref;
			model[6].checked = appModel[i].mod;
			model[9].checked = appModel[i].cnx;
			model[12].checked = appModel[i].noshow;	
			$("#versionAD").val(appModel[i].paxVersion);
			$("#paxIDAD").val(appModel[i].paxID);
		} else if (appModel[i].paxType == "CH") {
			model[1].checked = appModel[i].fare;
			model[4].checked = appModel[i].ref;	
			model[7].checked = appModel[i].mod;	
			model[10].checked = appModel[i].cnx;
			model[13].checked = appModel[i].noshow;	
			$("#versionCH").val(appModel[i].paxVersion);
			$("#paxIDCH").val(appModel[i].paxID);
		}
		else if (appModel[i].paxType == "IN") {
			model[2].checked = appModel[i].fare;
			model[5].checked = appModel[i].ref;
			model[8].checked = appModel[i].mod;
			model[11].checked = appModel[i].cnx;	
			model[14].checked = appModel[i].noshow;	
			$("#versionIN").val(appModel[i].paxVersion);
			$("#paxIDIN").val(appModel[i].paxID);
		}
	}
	return model;
}

ManageFareClasses.saveFareRule = function(){
	if (FareClassValidation.befoarSaveData()){
		top[2].ShowProgress();
		var url = "fareRule!saveFareRule.action";
		var data = {};
		data = ManageFareClasses.createFareRuleSubmitData(data);
		$.ajax({type: "POST", dataType: 'json',data: data , url:url,		
		success: ManageFareClasses.saveFareRuleProcess, error:ManageFareClasses.errorLoading, cache : false});
		return false;		
	}else{
		$("#btnSave").enable();
	}
}

ManageFareClasses.saveFareRuleProcess = function(response) {
	if (isEmpty(response)) {
		//Load Network connection failure error
		ManageFareClasses.errorLoading();
		return false;
	}
		
	if (response.success) {
		alert("Record Successfully saved!");
		var srtSearchParams = 'fareRuleCode|' + $("#selFareRuleCode").val() + ',status|' + $("#selStatus").val();	
		var delimiter = "^";
		setTabValues(screenId, delimiter, "strSearchCriteria", srtSearchParams);
		window.location.replace(window.location.pathname+"?mode="+screeMode);
	} else {
		ManageFareClasses.errorLoading(response.messageTxt);
		$("#btnSave").enable();
	}	
    top[2].HideProgress()
}
ManageFareClasses.saveFare = function() {
	if(ManageFareClasses.beforeSaveFare()){
		$("#btnSave").disable();
		top[2].ShowProgress();
		var url = "fare!saveFare.action";
		
		var isValid=ManageFareClasses.isValidPorationOND();
		if(!isValid){		 
			showERRMessage(ManageFareClasses.errorInfo.mismatch);
			return;
    
		}
		var data = ManageFareClasses.createFareSubmitData();
		$.ajax({type: "POST", dataType: 'json',data: data , url:url,		
		success: ManageFareClasses.saveFareProcess, error:ManageFareClasses.errorLoading, cache : false});
		return false;
	}else{
		$("#btnSave").enable();
	}
}

ManageFareClasses.saveFareProcess = function(response) {
	if (isEmpty(response)) {
		//Load Network connection failure error
		ManageFareClasses.errorLoading();
		return false;
	}
		
	if (response.success) {
		var delimiter = "^"
		alert("Record Successfully saved!");		
		
		if(response.fareTO != null){
			var fareID = response.fareTO.fareID;
			var childScreenId = "SC_ADMN_019";
			top[0].ruleStatus = "";
			top[1].objTMenu.tabSetTabInfo("SC_INVN_020", childScreenId, "showLoadFareJsp!manageRule.action?mode=MANAGEFARE&fareId="+fareID, "Manage Fare", false);
			setTabValues(childScreenId, delimiter, "strInventoryFlightData",	"true");
			//setTabValues(childScreenId, delimiter, "strSearchCriteria",	JSON.stringify(UI_setupFares.fareSearchCriteria, null));
			location.replace("showLoadFareJsp!manageRule.action?mode=MANAGEFARE&fareId="+fareID);
		} else {
			setTabValues(screenId, delimiter, "strSearchCriteria", "");
			window.location.replace(window.location.pathname+"?mode="+screeMode);
		}
		
		
	} else {
		ManageFareClasses.errorLoading(response.messageTxt);
	}
	$("#btnSave").enable();
}

ManageFareClasses.deleteFareRule = function() {
	if (confirm("Selected record will be Deleted. Do you wish to Continue?")) {
		var url = "fareRule!deleteFareRule.action";
		var data = {};
		data['fareRule.fareRuleID'] = $("#fareRuleID").val();	
		$.ajax({type: "POST", dataType: 'json',data: data , url:url,		
		success: ManageFareClasses.deleteFareRuleProcess, error:ManageFareClasses.errorLoading, cache : false});
		return false;	
	}
}

ManageFareClasses.deleteFareRuleProcess = function(response) {
	if (isEmpty(response)) {
		//Load Network connection failure error
		ManageFareClasses.errorLoading();
		return false;
	}
		
	if (response.success) {
		alert("Record Successfully deleted!");
		top[2].objMsg.MessageText = response.messageTxt;
		top[2].objMsg.MessageType = "Success";
		top[2].ShowPageMessage();
		window.location.replace(window.location.pathname+"?mode="+screeMode);	
	} else {
		ManageFareClasses.errorLoading(response.messageTxt);
	}
}

ManageFareClasses.closeClick = function(){
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setPageEdited(false);
		top[1].objTMenu.tabRemove(screenId);
	}
}

ManageFareClasses.resetClick = function(){
	 ManageFareClasses.FillGridToForm(ManageFareClasses.selectedRow, ManageFareClasses.gridDateCopy);
}

ManageFareClasses.beforeSaveFare = function (){	
	
	var validated = false;
	var masterFareRefCode = $("#masterFareRefCode").val();
	
	if ($("#selOrigin").val() == "") {
		showERRMessage(ManageFareClasses.errorInfo.originRqrd);
		$("#selOrigin").focus();
	} else if ($("#selDestination").val() == "") {
		showERRMessage(ManageFareClasses.errorInfo.destinationRqrd);
		$("#selDestination").focus();
	} else if($("#selDestination").val() == $("#selOrigin").val()) {
		showERRMessage(ManageFareClasses.errorInfo.originDestEqual);
		$("#selOrigin").focus();
		$("#selDestination").focus();
	} else if ($("#selBc option:selected").val() == "") {
		showERRMessage(ManageFareClasses.errorInfo.bookingcodeRqrd);
		$("#selBc").focus();
	} else if(!ManageFareClasses.FAREMODE.ADDHOCSAVE && $("#selFC>option:selected").text() == ""){
		showERRMessage(ManageFareClasses.errorInfo.fareclassRqrd);
		$("#selFC").focus();
	} else if ($("#txtFromDt").val() == "") {
		showERRMessage(ManageFareClasses.errorInfo.fromdateRqrd);
		$("#txtFromDt").focus();
	} else if ($("#txtToDt").val() == "") {
		showERRMessage(ManageFareClasses.errorInfo.todateRqrd);
		$("#txtToDt").focus();
	} else if (!ManageFareClasses.FAREMODE.MODIFY && !CheckDates(ManageFareClasses.sysDate, $("#txtFromDt").val())){
		showERRMessage(ManageFareClasses.errorInfo.FromDateExceedsCurrentDate);
		$("#txtFromDt").focus();
	} else if (!ManageFareClasses.FAREMODE.MODIFY && !CheckDates(ManageFareClasses.sysDate, $("#txtToDt").val())){
		showERRMessage(ManageFareClasses.errorInfo.toDateIncorrect);
		$("#txtToDt").focus();
	} else if(!dateValidDate($("#txtFromDt").val())){
		showERRMessage(ManageFareClasses.errorInfo.fromDateIncorrect);
		$("#txtFromDt").focus();
	} else if(!dateValidDate($("#txtToDt").val())){
		showERRMessage(ManageFareClasses.errorInfo.toDateIncorrect);
		$("#txtToDt").focus();
	} else if (!CheckDates($("#txtFromDt").val(), $("#txtToDt").val())){
		showERRMessage(ManageFareClasses.errorInfo.DeptFromTmExceeds);
		$("#txtToDt").focus();
	} else if ($("#txtSlsStDate").val() == "") {
		showERRMessage(ManageFareClasses.errorInfo.slsfromdateRqrd);
		$("#txtSlsStDate").focus();
	} else if ($("#txtSlsEnDate").val() == "") {
		showERRMessage(ManageFareClasses.errorInfo.slstodateRqrd);
		$("#txtSlsEnDate").focus();
	} else if (!ManageFareClasses.FAREMODE.MODIFY && !CheckDates(ManageFareClasses.sysDate, $("#txtSlsStDate").val())){
		showERRMessage(ManageFareClasses.errorInfo.slsfromDateexceeds);
		$("#txtSlsStDate").focus();
	} else if (!ManageFareClasses.FAREMODE.MODIFY && !CheckDates(ManageFareClasses.sysDate, $("#txtSlsEnDate").val())){
		showERRMessage(ManageFareClasses.errorInfo.slstoDateexceeds);
		$("#txtSlsEnDate").focus();
	} else if(!dateValidDate($("#txtSlsStDate").val())){
		showERRMessage(ManageFareClasses.errorInfo.slsfromDateIncorrect);
		$("#txtSlsStDate").focus();
	} else if(!dateValidDate($("#txtSlsEnDate").val())){
		showERRMessage(ManageFareClasses.errorInfo.slstoDateIncorrect);
		$("#txtSlsEnDate").focus();
	} else if (!CheckDates($("#txtSlsStDate").val(), $("#txtSlsEnDate").val())){
		showERRMessage(ManageFareClasses.errorInfo.slsfromDateexceeds);
		$("#txtSlsEnDate").focus();
	} else if(isChecked('chkMasterFare') && (masterFareRefCode == null || masterFareRefCode == "")){
		showERRMessage("Please enter master fare ref code");
		$("#masterFareRefCode").focus();
		return false;
	} else if((!ManageFareClasses.FAREMODE.ADDHOCSAVE && ManageFareClasses.isReturnFare) || (ManageFareClasses.FAREMODE.ADDHOCSAVE && isChecked('radReturn'))){
		if ($("#txtFromDtInbound").val() == "") {
			showERRMessage(ManageFareClasses.errorInfo.rtfromdateRqrd);
			$("#txtFromDtInbound").focus();
		} else if ($("#txtToDtInbound").val() == "") {
			showERRMessage(ManageFareClasses.errorInfo.rttodateRqrd);
			$("#txtToDtInbound").focus();
		} else if (!ManageFareClasses.FAREMODE.MODIFY && !CheckDates(ManageFareClasses.sysDate, $("#txtFromDtInbound").val())){
			showERRMessage(ManageFareClasses.errorInfo.rtfromDateIncorrect);
			$("#txtFromDtInbound").focus();
		} else if (!ManageFareClasses.FAREMODE.MODIFY && !CheckDates(ManageFareClasses.sysDate, $("#txtToDtInbound").val())){
			showERRMessage(ManageFareClasses.errorInfo.rttoDateIncorrect);
			$("#txtToDtInbound").focus();
		} else if(!dateValidDate($("#txtFromDtInbound").val())){
			showERRMessage(ManageFareClasses.errorInfo.rtfromDateIncorrect);
			$("#txtFromDtInbound").focus();
		} else if(!dateValidDate($("#txtToDtInbound").val())){
			showERRMessage(ManageFareClasses.errorInfo.rttoDateIncorrect);
			$("#txtToDtInbound").focus();
		} else if (!CheckDates($("#txtFromDtInbound").val(), $("#txtToDtInbound").val())){
			showERRMessage(ManageFareClasses.errorInfo.fromDateexceedsrtFrom);
			$("#txtToDtInbound").focus();		
		} else if (parseInt($("#txtAED").val(),10) == 0 ){
			if(isChecked('radCurr')){
				x = confirm("Base Fare Amount is Zero. Do you wish to continue?");
				if (!x){
					return false;
				} else {
					validated = true;
				}
			}
			validated = true;
		} else {
			validated = true;
		}
	} else if (parseInt($("#txtAED").val(),10) != 0 ){
		validated = true;
	} else if ($("#fareDiscountMin").val() != "" && ($("#fareDiscountMin").val() < 0 || $("#fareDiscountMin").val()>100)){
		showERRMessage("The minimum discount must be between 100% and 0%");
		$("#fareDiscountMin").focus();
	}else if ($("#fareDiscountMax").val() != "" && ($("#fareDiscountMax").val() < 0 || $("#fareDiscountMax").val()>100)){
		showERRMessage("The maximum discount must be between 100% and 0%");
		$("#fareDiscountMax").focus();
	} else if(ManageFareClasses.selectedCurrency ==  null) {
		if (parseInt($("#txtAED").val(),10) == 0 ){
			x = confirm("Base Fare Amount is Zero. Do you wish to continue?");
			if (!x){
				return false;
			} else {
				validated = true;
			}
		}
	} else {
		if (parseInt($("#txtBaseInLocal").val(),10) == 0 ){
			x = confirm("Base Fare Amount is Zero. Do you wish to continue?");
			if (!x){
				return false;
			} else {
				validated = true;
			}
		}
		validated = true;
	}
	var adultNoShowChargeType = $("#selADCGTYP").val();
	var childNoShowChargeType = $("#selCHCGTYP").val();
	var infantNoShowChargeType =  $("#selINCGTYP").val();
	
	var adultNoShowChargeBreakPoint = $("#txtADNoShoBreakPoint").val() != "" ? $("#txtADNoShoBreakPoint").val() : 0;
	var childNoShowChargeBreakPoint = $("#txtCHNoShoBreakPoint").val() != "" ? $("#txtCHNoShoBreakPoint").val() : 0;
	var infantNoShowChargeBreakPoint = $("#txtINNoShoBreakPoint").val() != "" ? $("#txtINNoShoBreakPoint").val() : 0;
	var adultNoShowChargeBoundary = $("#txtADNoShoBoundary").val() != "" ? $("#txtADNoShoBoundary").val() : 0;
	var childNoShowChargeBoundary = $("#txtCHNoShoBoundary").val() != "" ? $("#txtCHNoShoBoundary").val() : 0;
	var infantNoShowChargeBoundary = $("#txtINNoShoBoundary").val() != "" ? $("#txtINNoShoBoundary").val() : 0;
	var adultNoShowCharge = $("#txtADNoShoCharge").val();
	var childNoShowCharge = $("#txtCHNoShoCharge").val();
	var infantNoShowCharge = $("#txtINNoShoCharge").val();
	
	
	if (isNoShowChargeTypeNotV(adultNoShowChargeType) && parseFloat(adultNoShowChargeBreakPoint) == 0){
		showERRMessage(ManageFareClasses.errorInfo.noShowBreakPointEmpty);
		$("#txtADNoShoBreakPoint").focus();
		return false;
	} else if(isNoShowChargeTypeNotV(adultNoShowChargeType) && parseFloat(adultNoShowChargeBoundary) == 0){
		showERRMessage(ManageFareClasses.errorInfo.noShowBoundaryEmpty);
		$("#txtADNoShoBoundary").focus();
		return false;
	} else if(parseFloat(adultNoShowChargeBreakPoint) > parseFloat(adultNoShowChargeBoundary)){
		showERRMessage(ManageFareClasses.errorInfo.noShowBoundaryBreakPoint);
		$("#txtADNoShoBoundary").focus();
		return false;
	} else if(isNoShowChargeTypeNotV(adultNoShowChargeType) && (parseFloat(adultNoShowCharge)==0 || parseFloat(adultNoShowCharge)>100)){
		showERRMessage(ManageFareClasses.errorInfo.noShowChargePercentage);
		$("#txtADNoShoCharge").focus();
		return false;
	} else if(isNoShowChargeTypeNotV(childNoShowChargeType) && parseFloat(childNoShowChargeBreakPoint) == 0){
		showERRMessage(ManageFareClasses.errorInfo.noShowBreakPointEmpty);
		$("#txtCHNoShoBreakPoint").focus();
		return false;
	}  else if(isNoShowChargeTypeNotV(childNoShowChargeType) && parseFloat(childNoShowChargeBoundary) == 0){
		showERRMessage(ManageFareClasses.errorInfo.noShowBoundaryEmpty);
		$("#txtCHNoShoBoundary").focus();
		return false;
	} else if(parseFloat(childNoShowChargeBreakPoint) > parseFloat(childNoShowChargeBoundary)){
		showERRMessage(ManageFareClasses.errorInfo.noShowBoundaryBreakPoint);
		$("#txtCHNoShoBoundary").focus();
		return false;
	} else if(isNoShowChargeTypeNotV(childNoShowChargeType) && (parseFloat(childNoShowCharge)==0 ||parseFloat(childNoShowCharge)>100)){
		showERRMessage(ManageFareClasses.errorInfo.noShowChargePercentage);
		$("#txtCHNoShoCharge").focus();
		return false;
	} else if(isNoShowChargeTypeNotV(infantNoShowChargeType) && parseFloat(infantNoShowChargeBreakPoint) == 0){
		showERRMessage(ManageFareClasses.errorInfo.noShowBreakPointEmpty);
		$("#txtINNoShoBreakPoint").focus();
		return false;
	} else if(isNoShowChargeTypeNotV(infantNoShowChargeType) && parseFloat(infantNoShowChargeBoundary) == 0){
		showERRMessage(ManageFareClasses.errorInfo.noShowBoundaryEmpty);
		$("#txtINNoShoBoundary").focus();
		return false;
	} else if(parseFloat(infantNoShowChargeBreakPoint) > parseFloat(infantNoShowChargeBoundary)){
		showERRMessage(ManageFareClasses.errorInfo.noShowBoundaryBreakPoint);
		$("#txtINNoShoBoundary").focus();
		return false;
	} else if(isNoShowChargeTypeNotV(infantNoShowChargeType) && (parseFloat(infantNoShowCharge)==0 ||parseFloat(infantNoShowCharge)>100 )){
		showERRMessage(ManageFareClasses.errorInfo.noShowChargePercentage);
		$("#txtINNoShoCharge").focus();
		return false;
	}
	
	//Check Fare Rule Data if ADHoc Rule
	if (validated && ManageFareClasses.FAREMODE.ADDHOCSAVE) {
		if (FareClassValidation.befoarSaveData()){
			validated = true;
		} else {
			validated = false;
		}
	}
	
	if (validated)
		top[2].HidePageMessage();
	
	return validated;
}


ManageFareClasses.createFareSubmitData =  function() {
	var data = {};
	var activeStatus = 'INA';
	if (ManageFareClasses.FAREMODE.MODIFY) {
		data['fareTO.fareID'] =  $("#fareID").val();
		data['fareTO.version'] =  $("#versionFare").val();
	}
	
	data['fareTO.fareRuleID'] = getFareRuleTmpId();
	data['fareTO.fareRuleCode'] = $("#selFC option:selected").text();
	data['fareTO.effectiveFromDate'] = $("#txtFromDt").val();
	data['fareTO.effectiveToDate'] = $("#txtToDt").val();
	data['fareTO.fareAmount'] = $("#txtAED").val();	
	if (isChecked('chkStatus')) {
		activeStatus = 'ACT';
	}
	data['fareTO.status'] = activeStatus;
	data['fareTO.ondCode'] = $("#selOrigin").val() + "/" + $("#selDestination").val() ;
	data['fareTO.bookingCode'] = $("#selBc option:selected").text(); 
	data['fareTO.childFare'] = $("#txtCHL").val();
	
	if(ManageFareClasses.selectedCurrency == null && $("#airportCurrency").val() != ""){
		ManageFareClasses.selectedCurrency = $("#airportCurrency").val();
	}
	if(ManageFareClasses.selectedCurrency != null){
		data['fareTO.childFareType'] = $("#selCamtInLocal").val();
		data['fareTO.infantFareType'] = $("#selIamtInLocal").val();		
	}else{
		data['fareTO.childFareType'] = $("#selCamt").val();
		data['fareTO.infantFareType'] = $("#selIamt").val();
	}
	data['fareTO.infantFare'] = $("#txtINF").val();
	data['fareTO.salesEffectiveFrom'] = $("#txtSlsStDate").val();
	data['fareTO.salesEffectiveTo'] = $("#txtSlsEnDate").val();
	data['fareTO.returnEffectiveFromDate'] = $("#txtFromDtInbound").val();
	data['fareTO.returnEffectiveToDate'] = $("#txtToDtInbound").val();
	data['fareTO.via1'] = $("#selVia1").val();
	data['fareTO.via2'] = $("#selVia2").val();
	data['fareTO.via3'] = $("#selVia3").val();
	data['fareTO.via4'] = $("#selVia4").val();
	if (isChecked('chkAllowReturn')) {
		data['fareTO.createReturnFare'] = "Y";
	} else {
		data['fareTO.createReturnFare'] = "N";
	}
	
	var isModifyToSameFare = 'N';
	if (isChecked('chkModToSameFare')) {
		isModifyToSameFare = 'Y';
	}
	data['fareTO.modifyToSameFare'] = isModifyToSameFare;

	if(!ManageFareClasses.isProrationSaved){
	   if(ManageFareClasses.isProrationModify){
	      ManageFareClasses.savingProrations=ManageFareClasses.prorationData;
	   }
	}
    data['prorationJSON']=JSON.stringify(ManageFareClasses.savingProrations, null);	;    
    data['fareTO.currencyCode'] =  ManageFareClasses.selectedCurrency; 

    data['fareTO.fareAmountLocal'] =  $("#txtBaseInLocal").val();
    data['fareTO.childFareLocal'] = $("#txtCHFareInLocal").val();
    data['fareTO.infantFareLocal'] = $("#txtINFInLocal").val();
    
    var masterFareStatus = getText("chkMasterFare");
	var isMasterFareSelected = false;
	
	if(masterFareStatus == 'ON' || masterFareStatus == 'on'){
		data['fareTO.fareDefType'] = 'MASTER';
		data['fareTO.masterFareRefCode'] = $("#masterFareRefCode").val();
	}
    
    //ADD Fare Rule Data if ADHoc Rule
	if (ManageFareClasses.FAREMODE.ADDHOCSAVE) {
		data = ManageFareClasses.createFareRuleSubmitData(data);		
	}
	return data;
}

ManageFareClasses.createFareRuleSubmitData =  function(data) {	
	data['mode'] = ManageFareClasses.mode; 
	data['fareRule.fareRuleCode'] = $("#ruleCode").val();
	if (ManageFareClasses.mode == "EDIT") {
		data['fareRule.version'] = $("#version").val();
		data['fareRule.fareRuleID'] = $("#fareRuleID").val();
		data['versionAD'] = $("#versionAD").val();
		data['versionCH'] = $("#versionCH").val();
		data['versionIN'] = $("#versionIN").val();
		data['paxIDAD'] = $("#paxIDAD").val();
		data['paxIDCH'] = $("#paxIDCH").val();
		data['paxIDIN'] = $("#paxIDIN").val();		
	}
	data['fareRule.description'] = $("#description").val();
	var activeStatus = 'INA';
	if (isChecked('chkStatusActive')) {
		activeStatus = 'ACT';
	}
	data['fareRule.status'] = activeStatus;
	if (isChecked('chkAdvance')) {
		if($("#advanceBookingDays").val() != "" || $("#advanceBookingHrs").val() != "" || $("#advanceBookingMins").val() != ""){
			data['fareRule.advanceBookingDays'] = $("#advanceBookingDays").val() + ":" + $("#advanceBookingHrs").val() + ":" + $("#advanceBookingMins").val();
		}
	}	
	var isReturn = 'N';
	if (isChecked('radReturn')) {
		isReturn = 'Y';
	}
	data['fareRule.returnFlag'] = isReturn;
	
	var isHalfReturn = 'N';
	if(isChecked('radHalfReturn')){
		isHalfReturn = 'Y';
	}
	data['fareRule.halfReturnFlag'] = isHalfReturn;
	

	if($("#minStayOver_MM").val() != "" || $("#minStayOver_DD").val() != "" || $("#minStayOver_HR").val() != "" || $("#minStayOver_MI").val() != ""){
		data['fareRule.minimumStayOver'] = $("#minStayOver_MM").val() + ":" + $("#minStayOver_DD").val() + ":" + $("#minStayOver_HR").val() + ":" + $("#minStayOver_MI").val();
	}
	if($("#maxStayOver_MM").val() != "" || $("#maxStayOver_DD").val() != "" || $("#maxStayOver_HR").val() != "" || $("#maxStayOver_MI").val() != ""){
		data['fareRule.maximumStayOver'] = $("#maxStayOver_MM").val() + ":" + $("#maxStayOver_DD").val() + ":" + $("#maxStayOver_HR").val() + ":" + $("#maxStayOver_MI").val();
	}	
	
	if (isChecked('radOpReturn')) {
		data['fareRule.OpenReturnFlag'] = "Y";
	}else{
		$("#minStayOver_MM").val('');
		$("#minStayOver_DD").val('');
		$("#minStayOver_HR").val('');
		$("#minStayOver_MI").val('');
		$("#maxStayOver_MM").val('');
		$("#maxStayOver_DD").val('');
		$("#maxStayOver_HR").val('');
		$("#maxStayOver_MI").val('');
		$("#ortConfirm_MM").val('');
		$("#ortConfirm_DD").val('');
		$("#ortConfirm_HR").val('');
		$("#ortConfirm_MI").val('');
	}
	
	data['fareRule.outboundDepatureFreqencyString'] = createDelimitedValues(['chkOutMon', 'chkOutTues', 'chkOutWed', 'chkOutThu', 'chkOutFri', 'chkOutSat', 'chkOutSun']);
	data['fareRule.inboundDepatureFrequencyString'] = createDelimitedValues(['chkInbMon', 'chkInbTues', 'chkInbWed', 'chkInbThu', 'chkInbFri', 'chkInbSat', 'chkInbSun']);
	data['fareRule.outboundDepatureTimeFrom'] = $("#outBoundFrom").val();
	data['fareRule.outboundDepatureTimeTo'] = $("#outBoundTo").val();
	data['fareRule.inboundDepatureTimeFrom'] = $("#inBoundFrom").val();
	data['fareRule.inboundDepatureTimeTo'] = $("#inBoundTo").val();
	data['fareRule.rulesComments'] = $("#comments").val();
	data['fareRule.fareBasisCode'] = ManageFareClasses.FAREMODE.SAVE ? $("#txtFBC").val() : $("#fareBasisCode").val();
	data['fareRule.paxCategoryCode'] = $("#selPaxCat").val();
	data['fareRule.fareCategoryCode'] = $("#selFareCat").val();
	
	if($("#intnModBuffertime_DD").val() != "" || $("#intnModBuffertime_HR").val() != "" || $("#intnModBuffertime_MI").val() != ""){
		if($("#intnModBuffertime_DD").val() == ""){
			$("#intnModBuffertime_DD").val("0");
		}
		if($("#intnModBuffertime_HR").val() == "" ){
			$("#intnModBuffertime_HR").val("0"); 
		}
		if($("#intnModBuffertime_MI").val() == "" ){
			$("#intnModBuffertime_MI").val("0") ; 
		}
		data['fareRule.intnModifyBufferTime'] = $("#intnModBuffertime_DD").val() + ":" + $("#intnModBuffertime_HR").val() + ":" + $("#intnModBuffertime_MI").val();
	}
	
	if($("#domModBuffertime_DD").val() != "" || $("#domModBuffertime_HR").val() != "" || $("#domModBuffertime_MI").val() != ""){
		data['fareRule.domModifyBufferTime'] = $("#domModBuffertime_DD").val() + ":" +$("#domModBuffertime_HR").val() + ":" + $("#domModBuffertime_MI").val();
	}
	
	if (isChecked('chkPrintExp')) {
		data['fareRule.printExpDate'] = "Y";
	}
	if (isChecked('radOpReturn')) {
		data['fareRule.OpenReturnFlag'] = "Y";
	}
	
	if($("#ortConfirm_MM").val() != "" || $("#ortConfirm_DD").val() != "" || $("#ortConfirm_HR").val() != "" || $("#ortConfirm_MI").val() != ""){
		data['fareRule.openRetConfPeriod'] = $("#ortConfirm_MM").val() + ":" + $("#ortConfirm_DD").val() + ":" + $("#ortConfirm_HR").val() + ":" + $("#ortConfirm_MI").val();
	}	
	
	data['fareRule.flexiCodes'] = $("#selFlexiCode").val();
	if (isChecked('modifyByDate')) {
		data['fareRule.modifyByDate'] = "Y";
	}
	if (isChecked('chkBulkTicket')) {
		data['fareRule.bulkTicket'] = "Y";
		data['fareRule.minBulkTicketSeatCount'] = $("#minBTSeatCount").val()
	}
	if (isChecked('modifyByOND')) {
		data['fareRule.modifyByRoute'] = "Y";
	}
	if (isChecked('loadFactorEnabled')) {
		data['fareRule.loadFactorMin'] = $("#loadFactorMin").val();
		data['fareRule.loadFactorMax'] = $("#loadFactorMax").val();
	}
	data['fareRule.visibleChannelIds'] = getMultiSelectBoxValues($("#visibility").val());
	if(FareClassValidation.isLoadAgents){
		data['fareRule.visibileAgentCodes'] = getAgentsList($("#lstAgents").multiSelect.getRightData('lstAgents'));
	} else{
		data['fareRule.visibileAgentCodes'] = getMultiSelectBoxValues(FareClassValidation.agents);
	}
	
	if(data['fareRule.visibileAgentCodes'] == ""){
	    data['fareRule.visibileAgentCodes'] = getMultiSelectBoxValues(FareClassValidation.agents);
	}	

	if (isChecked('chkUpdateCommentsOnly')) {
		data['fareRule.onlyUpdateComments'] = "Y";
	}
	
	if ($("#visibility option:selected").text().toUpperCase().indexOf("WEB") != -1){		
		if(FareClassValidation.isLoadPOS){
			var leftData = $("#lstPOS").multiSelect.getLeftData('lstPOS');
			if(leftData == null || leftData.length < 1){
				data['fareRule.visiblePOSStr'] = null;
				data['fareRule.applicableToCountries'] = true;
			} else {				
				data['fareRule.visiblePOSStr'] = getMultiSelectBoxValues($("#lstPOS").multiSelect.getRightData('lstPOS'));
				data['fareRule.applicableToCountries'] = false;
			}
		} else {
			data['fareRule.visiblePOSStr'] = getMultiSelectBoxValues(FareClassValidation.pos);
			if(FareClassValidation.pos == null || FareClassValidation.pos.length < 1){				
				data['fareRule.applicableToCountries'] = true;
			} else {
				data['fareRule.applicableToCountries'] = false;
			}
		}
		
		if(data['fareRule.visiblePOSStr'] == ""){
			data['fareRule.visiblePOSStr'] = getMultiSelectBoxValues(FareClassValidation.pos);
			if(FareClassValidation.pos == null || FareClassValidation.pos.length < 1){				
				data['fareRule.applicableToCountries'] = true;
			} else {
				data['fareRule.applicableToCountries'] = false;
			}
		}
	}
	
	paxApplicabilityJSON = [];	
	var paxTypes=["AD","CH","IN"];
	$.each(paxTypes, function(){
		var tempModel = {};
		tempModel.fareApplicable = $("#chk"+this+"App").is(':checked');
		tempModel.fareRefundable = $("#chk"+this+"Ref").is(':checked');
		tempModel.modChgApplicable = $("#chk"+this+"Mod").is(':checked');
		tempModel.cnxChgApplicable = $("#chk"+this+"CNX").is(':checked');
		tempModel.noshowChgType = $("#sel"+this+"CGTYP").val();
		tempModel.noshowChgAmount = $("#txt"+this+"NoShoCharge").val();
		tempModel.noshowChgLocal = $("#txt"+this+"NoShoChargeInLocal").val();
		tempModel.noshowChgBreakpoint = $("#txt"+this+"NoShoBreakPoint").val();
		tempModel.noshowChgBoudary = $("#txt"+this+"NoShoBoundary").val();
		paxApplicabilityJSON[paxApplicabilityJSON.length] = tempModel;		
	});	
	
	data['fareRule.paxApplicabilityJSON'] = JSON.stringify(paxApplicabilityJSON, null);	
	
	data['fareRule.modificationChargeAmountStr'] = $("#txtModification").val();
	data['fareRule.cancellationChargeAmountStr'] = $("#txtCancellation").val();
	data['fareRule.minModChargeAmountStr'] = $("#txtMin_mod").val();
	data['fareRule.maxModChargeAmountStr'] = $("#txtMax_mod").val();
	data['fareRule.modificationChargeType'] = $("#selDefineModType").val();
	data['fareRule.cancellationChargeType'] = $("#selDefineCanType").val();
	data['fareRule.cancellationChargeLocalStr'] = $("#txtCancellationInLocal").val();
	data['fareRule.modificationChargeLocalStr'] = $("#txtModificationInLocal").val();
	if (isChecked('radSelCurrFareRule')) {
		data['fareRule.localCurrency'] = $("#selCurrencyCode").val();
	} else {
		data['fareRule.localCurrency'] = "";
	}
	data['fareRule.minCnxChargeAmountStr'] = $("#txtMin_canc").val();
	data['fareRule.maxCnxChargeAmountStr'] = $("#txtMax_canc").val();	
	
	data['fareRule.nameChangeChargeAmountStr'] = $("#txtNCC").val();
	data['fareRule.nameChangeChargeType'] = $("#selDefineNCCType").val();
	data['fareRule.minNCCAmountStr'] = $("#txtMin_NCC").val();
	data['fareRule.maxNCCAmountStr'] = $("#txtMax_NCC").val();
	data['fareRule.nameChangeChargeLocalStr'] = $("#txtNCCInLocal").val();
	
	 var selectedCommisionType  = getText("selAgentComType");
	 if(selectedCommisionType != ""){
		 data['fareRule.agentCommissionType']  = getText("selAgentComType");  
		 data['fareRule.agentCommissionAmount'] =   getText("txtAgentCommision");                  
		 data['fareRule.agentCommissionAmountLocal']= getText("selAgentComType");     //fix me
	 }
	data['fareRule.agentComments'] = $("#txtaInstructions").val();  
	if (isChecked('fareDiscount')) {
		data['fareRule.minFareDiscountStr'] = $("#fareDiscountMin").val();
		data['fareRule.maxFareDiscountStr'] = $("#fareDiscountMax").val();
	} else {
		data['fareRule.minFareDiscountStr'] = "";
		data['fareRule.maxFareDiscountStr'] = "";
	}
	data['fareRule.intnModifyBufferTime'] = $("#intnModBuffertime_DD").val() + ':' + $("#intnModBuffertime_HR").val() + ':' + $("#intnModBuffertime_MI").val();
	data['fareRule.domModifyBufferTime'] = $("#domModBuffertime_DD").val() + ':' + $("#domModBuffertime_HR").val() + ':' + $("#domModBuffertime_MI").val();	
	ManageFareClasses.getFees(data);
	ManageFareClasses.getFareRuleComments(data);
	return data;
}

function isNoShowChargeTypeNotV(chargeType){
	if(trim(chargeType) == "PF" || trim(chargeType) == "PFS" || trim(chargeType) == "PTF"){
		return true;
	}else{
		return false;
	}
}

function getAgentsList(agentsData) {
	var data = "";
	if (agentsData != null) {
		var agentData = null;
		for (var  i = 0; i < agentsData.length; i++ ) {				
			agentData = agentsData[i].split(":");
			if (agentData[0] != agentData[1]) {
				data += agentData[1] + ",";
			}		
		}
	}	
	return data;
}

function getMultiSelectBoxValues(values) {	
	var data = "";
	if (values != null && values != "") {
		for (var i = 0; i < values.length; i++) {
			data += values[i] + ",";
		}
	}
	return data;
}

function isChecked(id) {
	return $('#'+ id).is(':checked');
}

function createDelimitedValues(IDS) {
	var frequencyBuild = "";
	for (var i = 0; i < IDS.length; i++) {
		if (isChecked(IDS[i])) {
			frequencyBuild += "1:";
		} else {
			frequencyBuild += "0:";
		}
	}	
	return frequencyBuild;
}

//Close Button Click
function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}

function getGridRowIndex(rowid){
	var rowCount = jQuery("#fareRuleGrid").getGridParam('rowNum');
	var pageID = jQuery("#fareRuleGrid").getGridParam('page');
	return (rowid > rowCount) ? rowid - ((pageID - 1) * rowCount) : rowid;
}

function beforeUnload() {
	if ((ManageFareClasses.objWindow) && (!ManageFareClasses.objWindow.closed)) {
		objWindow.close();
	}
}

function ObjListToArray(list) {
	var t = []
	for (var i=0;i<list.length;i++){
		 var x = [];
		 for (var key in list[i]) {
			 var obj = list[i];
			 x[0] = key;
			 x[1] = obj[key];
		}
		t[t.length] = x;
	 }
}

function getFareRuleTmpId(){
	var fareRuleID_Type = $("#selFC").val();
	if(fareRuleID_Type != ""){
		var fareRuleTmp = fareRuleID_Type.split("_");
		 return fareRuleTmp[0];
	}
	
	return "";
}

function getFareRuleTmpType(){
	var fareRuleID_Type = $("#selFC").val();
	if(fareRuleID_Type != ""){
		var fareRuleTmp = fareRuleID_Type.split("_");
		 return fareRuleTmp[1];
	}
	
	return "";
}
	

function getMasterFaresError(response){
	showERRMessage(response.messageTxt);
};

function resetFareScreen(status){
	$("#chkMasterFare").enable();
	$("#chkLinkFare").enable();
	$("#masterFareRefCode").enable();
	$("#selectedMasterFareID").disable();
	$("#btnLinkFares").disable();
	$('#chkLinkFare').prop('checked', false);
}

function getMasterFaresSuccess(response){
	if(response.success){
		$("#selectedMasterFareID").html("");
		$("#selectedMasterFareID").append("<option value='' selected='selected'></option>");
		$.each(response.masterFares, function(key, value){
			var t = $("<option>"+value+"</option>").attr({value:key});		
			$("#selectedMasterFareID").append(t);
		});
		$("#selectedMasterFareID").enable();
		$("#linkFarePercentage").enable();
	}else{		
		resetFareScreen();
		showERRMessage(response.messageTxt);
	}
}

ManageFareClasses.linkFares_click = function(){
	var origin = $("#selOrigin").val();
	var destination = $("#selDestination").val();
	var via1 = $("#selVia1").val();
	var via2 = $("#selVia2").val();
	var via3 = $("#selVia3").val();
	var via4 = $("#selVia4").val();

	var data = {};
	data.origin = origin;
	data.destination = destination;
	data.via1 = via1;
	data.via2 = via2;
	data.via3 = via3;
	data.via4 = via4;
	data.fareId = $("#hdnFareID").val();	

	if(origin == null || origin == ""){
		showERRMessage(ManageFareClasses.errorInfo.OriginDoesNotExists);
		$("#selOrigin").focus();		
	}else if (destination == null || destination == ""){
		showERRMessage(ManageFareClasses.errorInfo.DestinationDoesNotExists);
		$("#selDestination").focus();		
	}else if (origin == destination){
		showERRMessage(ManageFareClasses.errorInfo.originDestEqual);
		$("#selDestination").focus();		
	}else{
		var url = "showMasterLinkFares.action";
		$.ajax({type: "POST", dataType: 'json', data: data , url:url,		
		success: getMasterFaresSuccess, error:getMasterFaresError, cache : false, async: false});
	}	
}	

ManageFareClasses.CWindowOpen = function(strIndex) {
	if ((ManageFareClasses.objWindow) && (!ManageFareClasses.objWindow.closed)) {
		ManageFareClasses.objWindow.close();
	}
	var intWidth = 0;
	var intHeight = 0;
	var strProp = '';

	var strFilename = "";
	switch (strIndex) {
	case 0:
		$("#hdnMode").val("LinkAgents");
		strFilename = "showLinkAgentMultiDDL.action"; // linkFares.do
		intHeight = 400;
		intWidth = 600;
		break;

	case 1:
		$("#hdnMode").val("AgentFares");
		strFilename = "showLoadFareJsp!showAgentFareRule.action"; // showAgentRules.action  -  showAgentwiseFC.do
		intHeight = 295;
		intWidth = 720;
		break;
		
	case 2:
		var rqf = "manageFare";
		var fc = $("#hdnRuleCode").val();
		var frId = $("#hdnRuleId").val();
		var version =  $("#hdnVersion").val();
		var locCurrency = null;
		var selCurrencyCode = $("#selCurrencyCode").val();
		if(isChecked('radSelCurrFareRule') && selCurrencyCode!="-1"){
			locCurrency = selCurrencyCode;
		}
		$("#hdnMode").val("OverwriteFares");
		strFilename = "showOverwriteFarerule.action?rqFrom="+rqf+"&frc="+fc+"&frId="+frId+"&version="+version+"&locCurrency="+locCurrency;
		intHeight = 660;
		intWidth = 900;
		break;
		
	case 3:
		var rqf = "manageFare";
		var fc = $("#hdnRuleCode").val();
		var frId = $("#hdnRuleId").val();
		var version =  $("#hdnVersion").val();
		$("#hdnMode").val("overRideOHD");
		strFilename = "showLoadFareJsp!overRideOHD.action?rqFrom="+rqf+"&frc="+fc+"&frId="+frId+"&version="+version;
		intHeight = 660;
		intWidth = 900;
		break;
	case 4:		 
		strFilename="showLoadJsp!loadProrationPopup.action";

		if (!ManageFareClasses.isProrationModify) {

			if (ManageFareClasses.isProrationSaved) {
				ManageFareClasses.prorationData = ManageFareClasses.savingProrations;
				if(!ManageFareClasses.isProrationDeleted){
					ManageFareClasses.isProDeleteEnable=true;				 
				}else{
					ManageFareClasses.prorationData = ManageFareClasses.createSegmentData();
				} 
			} else {
				ManageFareClasses.prorationData = ManageFareClasses.createSegmentData();
				 
			}

		} else {
			var existingOnd = ManageFareClasses
					.createONDFromProrationData(ManageFareClasses.prorationData);
			var newProration = ManageFareClasses.createSegmentData();
			var newOnd = ManageFareClasses
					.createONDFromProrationData(newProration);
			
			if(!ManageFareClasses.isProrationDeleted){
				ManageFareClasses.isProDeleteEnable=true;				 
			}
			if (existingOnd != newOnd) {
				ManageFareClasses.segmentChanged = true;
				ManageFareClasses.oldProrationData = ManageFareClasses.prorationData;
				for ( var i = 0; i < ManageFareClasses.prorationData.length; i++) {
					if (i < newProration.length) {
						newProration[i].prorationId = ManageFareClasses.prorationData[i].prorationId;
						newProration[i].version = ManageFareClasses.prorationData[i].version;
					}
				}
				ManageFareClasses.prorationData = newProration;
			}
		}
		
		console.log('pro data:'+ManageFareClasses.prorationData);
		intHeight = 300;
		intWidth = 400;
		break;
	}
	
	strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width='
			+ intWidth
			+ ',height='
			+ intHeight
			+ ',resizable=no,top='
			+ ((window.screen.height - intHeight) / 2)
			+ ',left='
			+ (window.screen.width - intWidth) / 2;
	ManageFareClasses.objWindow = window.open(strFilename, "CWindow", strProp);
}

//Close Button Click
function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}
function setFees(feesObj,data,count){
	for(var i = 0; i < feesObj.length; i++){
		if(feesObj[i].fareRuleFeeID != ""){
			data['fees['+ count+ '].fareRuleFeeId'] = feesObj[i].fareRuleFeeID;
			data['fees['+ count+ '].chargeType'] = feesObj[i].chargeType;
			data['fees['+ count+ '].chargeAmountType'] = feesObj[i].chargeAmountType;
			data['fees['+ count+ '].chargeAmount'] = feesObj[i].chargeAmount;
			data['fees['+ count+ '].minimumPerChargeAmt'] = feesObj[i].minPerCharge;
			data['fees['+ count+ '].maximumPerChargeAmt'] = feesObj[i].maxPerCharge;
			data['fees['+ count+ '].compareAgainst'] = feesObj[i].compareAgainst;
			data['fees['+ count+ '].timeType'] = feesObj[i].timeType;
			data['fees['+ count+ '].timeValue'] = feesObj[i].timeValue;
			data['fees['+ count+ '].status'] = feesObj[i].status;
			data['fees['+ count+ '].chargeAmountInLocal'] = feesObj[i].chargeAmountLocal;
			count++
		}
	}
	return count;
}

ManageFareClasses.getFees = function(data){
	if(FareClassValidation.isAdHocFare){
		count = 0;
		if(FareClassValidation.fareCNXPeirodModel != null){
			count =setFees(FareClassValidation.fareCNXPeirodModel,data,count);
		}
		if (FareClassValidation.fareMODPeirodModel != null){
			count = setFees(FareClassValidation.fareMODPeirodModel,data,count);
		}
		if (FareClassValidation.fareNCCPeirodModel != null){
			count =setFees(FareClassValidation.fareNCCPeirodModel,data,count);
		}
		
	}
	else
	{
		var tabIDs = ['tabModification', 'tabCancelation','tabNoShow'];
		var tabId = "";
		var colCount = 0;
		var freeType = "";
		for (var tab = 0; tab < tabIDs.length; tab++) {
			tabId = tabIDs[tab];
			freeType = getFreeType(tabId);
			var tempID = 0;
			var rowArray = $("#"+tabId).find(".peirodRows").children();
			rowArray.removeClass("hight-light");
			var actualRowLength = rowArray.length - 1;
			var prefix = getPrefixByTabName(tabId);
			var ret = true;
			for (var i = 0; i < actualRowLength; i++) {
				tempID = i;
				var feeID = $("#"+prefix+"\\["+i+"\\]\\.feeID").val();
				var feeVersion = $("#"+prefix+"\\["+i+"\\]\\.feeVersion").val();
				var fromVal = $("#"+prefix+"\\["+i+"\\]\\.from").val();
				var toVal = $("#"+prefix+"\\["+i+"\\]\\.to").val();
				var adPrefObj = $("#"+prefix+"\\["+i+"\\]\\.preferdAD").val();
				var adVal = $("#"+prefix+"\\["+i+"\\]\\.valueAD").val();
				var adMaxVal = $("#"+prefix+"\\["+i+"\\]\\.maxAD").val();
				var adMinVal = $("#"+prefix+"\\["+i+"\\]\\.minAD").val();
				var chPrefObj = $("#"+prefix+"\\["+i+"\\]\\.preferdCH").val();
				var chVal = $("#"+prefix+"\\["+i+"\\]\\.valueCH").val();
				var chMaxVal = $("#"+prefix+"\\["+i+"\\]\\.maxCH").val();
				var chMinVal = $("#"+prefix+"\\["+i+"\\]\\.minCH").val();
				var inPrefObj = $("#"+prefix+"\\["+i+"\\]\\.preferdIN").val();
				var inVal = $("#"+prefix+"\\["+i+"\\]\\.valueIN").val();
				var inMaxVal = $("#"+prefix+"\\["+i+"\\]\\.maxIN").val();
				var inMinVal = $("#"+prefix+"\\["+i+"\\]\\.minIN").val();
			
				data['fees['+ colCount+ '].feeID'] = feeID;
				data['fees['+ colCount+ '].feeVersion'] = feeVersion;
				data['fees['+ colCount+ '].startCutOver'] = fromVal;
				data['fees['+ colCount+ '].endCutOver'] = toVal;
				data['fees['+ colCount+ '].feeType'] = freeType;
				data['fees['+ colCount+ '].adAmountSpecifiedAs'] = adPrefObj;
				data['fees['+ colCount+ '].adAmount'] = adVal;
				data['fees['+ colCount+ '].adMinAmountThresold'] = adMinVal;
				data['fees['+ colCount+ '].adMaxAmountThresold'] = adMaxVal;
				data['fees['+ colCount+ '].chAmountSpecifiedAs'] = chPrefObj;
				data['fees['+ colCount+ '].chAmount'] = chVal;
				data['fees['+ colCount+ '].chMinAmountThresold'] = chMinVal;
				data['fees['+ colCount+ '].chMaxAmountThresold'] = chMaxVal;
				data['fees['+ colCount+ '].inAmountSpecifiedAs'] = inPrefObj;
				data['fees['+ colCount+ '].inAmount'] = inVal;
				data['fees['+ colCount+ '].inMinAmountThresold'] = inMinVal;
				data['fees['+ colCount+ '].inMaxAmountThresold'] = inMaxVal;
				colCount++;		
			}
		}
	}
}

ManageFareClasses.getFareRuleComments = function(data){
	var ids = jQuery("#listFareRuleComment").jqGrid('getDataIDs');
	
	var colCount = 0;
	for (var i = 0; i < ids.length; i++) 
	{
	    var rowId = ids[i];
	    var rowData = jQuery('#listFareRuleComment').jqGrid ('getRowData', rowId);
	    
	    var fareRuleCommentID = rowData.commentID;
	    var fareRuleID = rowData.fareRuleID;
	    var languageCode = rowData.languageCode;
	    var fareRuleComments = rowData.comment;
	    
	    data['fareRuleComments['+ colCount+ '].fareRuleCommentID'] = fareRuleCommentID;
	    data['fareRuleComments['+ colCount+ '].fareRuleID'] = fareRuleID;
	    data['fareRuleComments['+ colCount+ '].languageCode'] = languageCode;
	    data['fareRuleComments['+ colCount+ '].fareRuleComments'] = fareRuleComments;
	    colCount++;
	}
}

function getFreeType(tabId) {
	var freeType = "";
	if (tabId == 'tabModification') {
		freeType = "MOD";
	} else if (tabId == 'tabCancelation') {
		freeType = "CNX";
	} else if (tabId == 'tabNoShow') {
		freeType = "NOSHOW";
	}
	return freeType;
}


ManageFareClasses.addHocRuleSetup = function() {
	ManageFareClasses.FAREMODE.ADDHOCSAVE = true;	
	FareClassValidation.isAdHocFare = true;
	FareClassValidation.enableAll("EDIT");
	//ManageFareClasses.setPageInitData();
	FareClassValidation.disableEnable("radOneWay");
	enabledDispableMultiselect('enable');
}

/**
 * This method handles add proration button click event.
 */
ManageFareClasses.addProration = function() {
	var origin = $("#selOrigin").val();
	var dest = $("#selDestination").val();
	var via1 = $("#selVia1").val();
	var via2 = $("#selVia2").val();
	var via3 = $("#selVia3").val();
	var via4 = $("#selVia4").val();
	
	var isEnable=false;
	if (origin != null && origin != "" && dest != null && dest != "") {
		if ((via1 != null && via1 != "") || (via2 != null && via2 != "")
				|| (via3 != null && via3 != "") || (via4 != null && via4 != "")) {
			isEnable = true;
		}
	}
	if(isEnable){
		if(!ManageFareClasses.validateSelectedPoints()){
			showERRMessage(ManageFareClasses.errorInfo.hasSameValue);
			isEnable=false;
		}
	}
	
	if (isEnable) {
		ManageFareClasses.CWindowOpen(4);		 
	} else {	  
		//should change name
		if(!ManageFareClasses.isViaPointSelected()){
			showERRMessage(ManageFareClasses.errorInfo.requiredOriginDestAndViaPoint);
		}		
	}
}
/**
 * Creates proration data for segments.
 */
ManageFareClasses.createSegmentData = function() {
	var origin = $("#selOrigin").val();
	var dest = $("#selDestination").val();
	var via1 = $("#selVia1").val();
	var via2 = $("#selVia2").val();
	var via3 = $("#selVia3").val();
	var via4 = $("#selVia4").val();
	var isEnable = false;
	var ond=[];
	var ondJson=[]; 
	if (origin != null && origin != "" && dest != null && dest != "") {
		ond.push(origin);
		if (via1 != null && via1 != ""){
			ond.push(via1);
		}
		if (via2 != null && via2 != ""){
			ond.push(via2);
		}
		if (via3 != null && via3 != ""){
			ond.push(via3);
		}
		if (via4 != null && via4 != ""){
			ond.push(via4);
		}
		ond.push(dest);
	}
	if(ond.length >2){
		for(var i=0;i+1<ond.length;i++){
			var seg=ond[i]+"/"+ond[i+1];
			var obj={"prorationId":null,"segmentCode":seg,"proration":"","version":0};
			ondJson.push(obj);			
		}
		if(ManageFareClasses.isReturnFare){
			for(var j=ond.length-1;j>0;j--){
				var retSeg=ond[j]+"/"+ond[j-1];
				var retObj={"prorationId":null,"segmentCode":retSeg,"proration":"","version":0};
				ondJson.push(retObj);
			}			
		}
	}
	return ondJson;
}
 
ManageFareClasses.setFareAdd = function(fare) {
	$("#spnId").text(fare.fareID);
	
	$("#selFC").val(fare.fareRuleID + "_" + fare.returnFlag);
	var fareRuleCode = $("#selFC>option:selected").text();
	if(isEmpty(fareRuleCode)){
		ManageFareClasses.FAREMODE.ADDHOCSAVE = true;
		FareClassValidation.isAdHocFare = true;
		enabledDispableMultiselect('enable');
	}else {
		ManageFareClasses.FAREMODE.ADDHOCSAVE = false;
		FareClassValidation.isAdHocFare = false;
		enabledDispableMultiselect('disable');
	}
	$("#txtFromDt").val(fare.effectiveFromDate);
	$("#txtToDt").val(fare.effectiveToDate);
	$("#txtAED").val(fare.fareAmount);	
	if (fare.status == "ACT") {
		$("#chkStatus").check();
	} else {
		$("#chkStatus").uncheck();
	}
	var ondCodes = fare.ondCode.split("/");
	var origin = ondCodes[0];
	var destination = ondCodes[ondCodes.length-1];
	var via1 = "";
	var via2 = "";
	var via3 = "";
	var via4 = "";
	if (ondCodes.length == 3) {
		via1 = ondCodes[1];
	} else if (ondCodes.length == 4) {
		via1 = ondCodes[1];
		via2 = ondCodes[2];
	} else if (ondCodes.length == 5) {
		via1 = ondCodes[1];
		via2 = ondCodes[2];
		via3 = ondCodes[3];
	} else if (ondCodes.length == 6) {
		via1 = ondCodes[1];
		via2 = ondCodes[2];
		via3 = ondCodes[3];
		via4 = ondCodes[4];
	}
	$("#selOrigin").val(origin);
	$("#selDestination").val(destination);
	$("#selVia1").val(via1);
	$("#selVia2").val(via2);
	$("#selVia3").val(via3);
	$("#selVia4").val(via4);
	var dd = document.getElementById('selBc');
	for (var i = 0; i < dd.options.length; i++) {
	    if (dd.options[i].text === fare.bookingCode) {
	        dd.selectedIndex = i;
	        break;
	    }
	}
	$("#selBc").change();
	$("#selCamt").val(fare.childFareType);
	$("#txtCHL").val(fare.childFare);
	$("#selIamt").val(fare.infantFareType);
	$("#txtINF").val(fare.infantFare);
	
	$("#selCamtInLocal").val(fare.childFareType);
	$("#selIamtInLocal").val(fare.infantFareType);	
	if(ManageFareClasses.enableFareInDepAirPCurr){
		$("#airportCurrency").val(fare.currencyCode);
		$("#txtBaseInLocal").val(fare.fareAmountLocal);
		if(fare.currencyCode != null){
			$("#airportCurrency").text(fare.currencyCode);	
		}
		if(fare.currencyCode != null){
			$("#radSelCurr").attr("checked", true);	
			$("#radCurr").attr("checked", false);
			$("#txtAED").disable();
			$("#txtBaseInLocal").enable();
			$("#selCamt").disable();
			$("#selIamt").disable();
		} else {
			$("#radSelCurr").attr("checked", false);	
			$("#radCurr").attr("checked", true);
			$("#txtBaseInLocal").disable();
			$("#selCamt").enable();
			$("#selIamt").enable();
			$("#txtAED").enable();
		}	
	}
	if(ManageFareClasses.enableFareInDepAirPCurr){
		$("#txtCHFareInLocal").val(fare.childFareLocal);
	}
	if(ManageFareClasses.enableFareInDepAirPCurr){
		$("#txtINFInLocal").val(fare.infantFareLocal);
	}
	
	$("#txtSlsStDate").val(fare.salesEffectiveFrom);
	$("#txtSlsEnDate").val(fare.salesEffectiveTo);
	$("#txtFromDtInbound").val(fare.returnEffectiveFromDate);
	$("#txtToDtInbound").val(fare.returnEffectiveToDate);	
	$("#versionFare").val(fare.version);
	$("#btnSave").enable();
	if (ManageFareClasses.FAREMODE.ADDHOCSAVE){
		$("#btnEditRule").enable();
	}
	if (fare.modifyToSameFare == "Y") {
		$("#chkModToSameFare").check();
	} else {
		$("#chkModToSameFare").uncheck();
	}
	
	if(fare.masterFareRefCode != null && fare.masterFareRefCode != ""){
		$("#chkMasterFare").prop('checked',true);
		$("#masterFareRefCode").val(fare.masterFareRefCode);
	}	
}

ManageFareClasses.createONDFromProrationData = function(dataArray) {
	var ond="";
		
	if(dataArray!=null && dataArray.length >0){
		for(var i =0;i<dataArray.length;i++){			
			ond=ond+dataArray[i].segmentCode;		
		}
	}
	return ond;
}

ManageFareClasses.isValidPorationOND = function() {
	if(ManageFareClasses.isEnableProration){
		var isValid=false;
		var proData=null;
		if(ManageFareClasses.isProrationSaved){
		   proData=ManageFareClasses.savingProrations;
		}else{
		   if(ManageFareClasses.isProrationModify){
			   if(ManageFareClasses.segmentChanged){
				   ManageFareClasses.prorationData=ManageFareClasses.oldProrationData;				   
			   } 
			   proData=ManageFareClasses.prorationData;		      
		   }
		}
		
	    if(proData!=null && proData.length >0){
	        var existingOnd=ManageFareClasses.createONDFromProrationData(proData);
		    var newProration=ManageFareClasses.createSegmentData();
		    var newOnd=ManageFareClasses.createONDFromProrationData(newProration);
	    	if(existingOnd == newOnd){
	    		isValid=true;
	    	} 
	    }else{
	        isValid=true;
	    }

		if(!ManageFareClasses.isViaPointSelected()){
			isValid=true;
			ManageFareClasses.savingProrations=[];
			ManageFareClasses.prorationData=[];
		}
		return isValid;
	}else{
		return true;
	}
}

/**
 * This checks wheather same value is not selected for origin/destinations
 *  and via points selected.
 */
ManageFareClasses.validateSelectedPoints = function() {
	var via1 = $("#selVia1").val();
	var via2 = $("#selVia2").val();
	var via3 = $("#selVia3").val();
	var via4 = $("#selVia4").val();
	var origin = $("#selOrigin").val();
	var dest = $("#selDestination").val();
	var selectedValues=[]
	if(via1!=null && via1!=""){
		selectedValues.push(via1);
	}
	if(via2!=null && via2!=""){
		selectedValues.push(via2);
	}
	if(via3!=null && via3!=""){
		selectedValues.push(via3);
	}
	if(via4!=null && via4!=""){
		selectedValues.push(via4);
	}
	if(origin!=null && origin!=""){
		selectedValues.push(origin);
	}
	if(dest!=null && dest!=""){
		selectedValues.push(dest);
	}
	var isValid=true;
	if(selectedValues.length>0){
		var temp=selectedValues;
		for(var i=0;i<selectedValues.length;i++){
			for(var j=0;j<selectedValues.length;j++){
				if(i!=j){
					if(selectedValues[i]== temp[j]){
						isValid=false;
						break;
					}
				}
			}
			if(!isValid){
				break;
			}
		}
	}else{
		isValid=false;
	}
	return isValid;
}

/**
 * This method enable/disble proration button according to the 
 * selected source dest and via points.
 */
ManageFareClasses.controlProrationButton=function(event){
	
	var via1 = $("#selVia1").val();
	var via2 = $("#selVia2").val();
	var via3 = $("#selVia3").val();
	var via4 = $("#selVia4").val();
	var origin = $("#selOrigin").val();
	var dest = $("#selDestination").val();
	
	var isValid=false;
	
	if (origin != null && origin != "" && dest != null && dest != "") {
		if ((via1 != null && via1 != "") || (via2 != null && via2 != "")
				|| (via3 != null && via3 != "") || (via4 != null && via4 != "")) {
			isValid=true;
		}
	}
 
	if(isValid){
		$("#btnAddProration").enable();
	}else{
		$("#btnAddProration").disable();
	}
}

ManageFareClasses.isViaPointSelected = function() {
	var via1 = $("#selVia1").val();
	var via2 = $("#selVia2").val();
	var via3 = $("#selVia3").val();
	var via4 = $("#selVia4").val();
 
	var selectedValues=[]
	if(via1!=null && via1!=""){
		selectedValues.push(via1);
	}
	if(via2!=null && via2!=""){
		selectedValues.push(via2);
	}
	if(via3!=null && via3!=""){
		selectedValues.push(via3);
	}
	if(via4!=null && via4!=""){
		selectedValues.push(via4);
	}
	var isValid=false;
	if(selectedValues.length>0){
		isValid=true;
	} 
	return isValid;
}

ManageFareClasses.sortProration = function(a,b) {
	 var id1 = a.prorationId;
	 var id2 = b.prorationId; 
	 if(id1== id2) return 0;
	 return id1 > id2? 1: -1;
	   
}

ManageFareClasses.getAirportCurrency = function(origin){
	var url = "fare!getAirportCurrency.action";
	var data = {};
	data['departingAirport'] = origin;
	$.ajax({type: "POST", dataType: 'json',data: data , url:url, beforeSend: top[2].ShowProgress(),		
	success: ManageFareClasses.setAirportCurrency, error:ManageFareClasses.errorLoading, cache : false});
	return false;
}

ManageFareClasses.clearCurrencyFields = function(enableBaseCurrency,paxType){
	if(enableBaseCurrency){
		$("#txt"+paxType+"NoShoChargeInLocal").val("0.00");
	}else{
		$("#txt"+paxType+"NoShoCharge").val("0.00");
	}
	$("#txt"+paxType+"NoShoBreakPoint").val("0.00");
	$("#txt"+paxType+"NoShoBoundary").val("0.00");
}

ManageFareClasses.setAirportCurrency = function(response){
	if (response.success) {
		$("#airportCurrency").text(response.departingAirportCurrency);
		ManageFareClasses.selectedCurrency = response.departingAirportCurrency;
		$("#txtAED").val("0.00");
		$("#txtCHL").val("0.00");
		$("#txtINF").val("0.00");
		$("#txtAED").disable();
		$("#txtCHL").disable();
		$("#txtINF").disable();
		$("#txtBaseInLocal").enable();
		$("#txtCHFareInLocal").enable();
		$("#txtINFInLocal").enable();
	}
	top[2].HideProgress();
}

ManageFareClasses.clearAirportCurrency = function(){
	var origin = $("#selOrigin").val();
	if(origin != "" && origin != null){
	$("#airportCurrency").text("");
	$("#txtBaseInLocal").val("0.00");
	$("#txtCHFareInLocal").val("0.00");
	$("#txtINFInLocal").val("0.00");
	$("#txtBaseInLocal").disable();
	$("#txtCHFareInLocal").disable();
	$("#txtINFInLocal").disable();
	$("#txtAED").enable();
	$("#txtCHL").enable();
	$("#txtINF").enable();
	ManageFareClasses.selectedCurrency = null;
	}
}

ManageFareClasses.setOPENRTValue = function(){
	if(isChecked('radOpReturn')){
		FareClassValidation.isOpenReturn = true;
		FareClassValidation.enableDisableORTTime(true);
	}else{
		FareClassValidation.isOpenReturn = false;
		FareClassValidation.enableDisableORTTime(false);
	}
}
