/**
 * AgentsFare.js
 * 
 * @author Baladewa welathanthri
 *  
 */

function AgentsFare(){};

$(function(){
	AgentsFare.ready();
});

AgentsFare.ready = function() {	
	$("#btnSearch").click(function(){AgentsFare.searchData();});
	AgentsFare.loadPageData();	
	jQuery("#spnFC").jqGrid({   
		//datatype: "local",
		datatype: function(postdata) {			
			postdata['agentCode'] = $("#selAgent").val();					
	        jQuery.ajax({
	           url: 'agentFareRule!searchAgentData.action',
	           data:postdata,
	           dataType:"json",
	           complete: function(jsonData,stat){	        	  
	              if(stat=="success") {
	            	 mydata = eval("("+jsonData.responseText+")");
	            	 $("#spnFC")[0].addJSONData(mydata);            	
	              }
	           }
	        });
	    },
		colNames:['&nbsp;','Agent Code ','Agent Name', 'Fare Rule', 'Fare Rule Description','Basis Code'],
	   	colModel:[  {name:'id', width:30,label:"id"},
	   	            {name:'agentCode',index:'agentCode', width:100 , align:"left" },
	   	            {name:'agentName', index:'agentName', width:150 ,align:"left" },
	   	            {name:'fareRule', width:80, align:"left" },
	   	            {name:'fareRuleDesc', width:190, align:"center" },
	   	            {name:'basisCode', width:100, align:"center" }
	   	         ],
	   			height: 100,
	   		   	rowNum:10,
	   		   	jsonReader: { 
	   				root: "rows",
	   			  	page: "page",
	   			  	total: "total",
	   			  	records: "records",
	   				repeatitems: false,
	   				id: "0" 
	   			},
	   		   	pager: '#spnFCPager',
	   		   	sortname: 'id',
	   			multiselect: false,
	   			viewrecords: true,
	   		    sortorder: "desc",
	   		    loadui:'block',
	   		    caption:"Fare Rules"
	   		}).navGrid("#spnFCPager",{edit:false,add:false,del:false});
};

AgentsFare.loadPageData = function() {
	var url = "agentFareRule.action";
	var data = {};
	$.ajax({type: "POST", dataType: 'json',data: data , url:url,		
	success: AgentsFare.loadPageDataProcess, error:AgentsFare.errorLoading, cache : false});
	return false;
}

AgentsFare.loadPageDataProcess = function(response) {
	if (isEmpty(response)) {
		//Load Network connection failure error
		AgentsFare.errorLoading();
		return false;
	}
		
	if (response.success) {
		$("#selAgent").fillDropDown({dataArray:response.agentList, keyIndex:0, valueIndex:1, firstEmpty:false});		
	} else {
		AgentsFare.errorLoading(response.messageTxt);
	}
}

AgentsFare.searchData = function() {	
	var newUrl = 'agentFareRule!searchAgentData.action?';
 	newUrl += 'agentCode=' + $("#selAgent").val(); 	
 	$("#spnFCPager").clearGridData();
 	$.getJSON(newUrl, function(response){ 
 		$("#spnFC")[0].addJSONData(response); 		
 	});	
}

AgentsFare.errorLoading = function(response){
	alert(response);
};