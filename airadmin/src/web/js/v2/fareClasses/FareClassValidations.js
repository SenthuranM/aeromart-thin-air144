/**
 * FareClassValidation.js
 * 
 * @author Baladewa welathanthri
 *  
 */
function FareClassValidation(){};

$(function(){
	FareClassValidation.ready();
});
FareClassValidation.errorInfo = null;
FareClassValidation.fareAppModel = null;
FareClassValidation.fareCNXPeirodModel = null;
FareClassValidation.fareMODPeirodModel = null;
FareClassValidation.fareNCCPeirodModel = null;
FareClassValidation.fareNOSHOWPeirodModel = null;
FareClassValidation.hasNoShow = false;
FareClassValidation.hasCNX = false;
FareClassValidation.hasMOD = false;

FareClassValidation.isOpenReturn = false;
FareClassValidation.isReturn = false;
FareClassValidation.isAdHocFare = false;
FareClassValidation.agents = false;
FareClassValidation.milliBufferTime = {DOM:'86400000', INT:'86400000'};
FareClassValidation.loadFactorEnable = false;
FareClassValidation.isLoadAgents = false;
FareClassValidation.isLinkFareEnabled = false;
FareClassValidation.isEnableSameFareChecking = false;
FareClassValidation.isEnableFareRuleNCC = false;
FareClassValidation.chargeRestrictionDefault = {modtype:"V",cantype:"V", ncctype:"V", modval:"0.00",canvalue:"0.00", nccvalue:"0.00",modvallocal:"0.00",canvaluelocal:"0.00", nccvallocal:"0.00"}
FareClassValidation.isLoadPOS = false;
FareClassValidation.pos = false;
FareClassValidation.minRequiredSeatsForBTRes = 1;

FareClassValidation.ready = function(){
	$("#tabs").tabs({
		activate: function(event, ui) {
			if(ui.newTab[0].id == "tabLinkAgentTop"){
				if (FareClassValidation.isLoadAgents == false)
					FareClassValidation.loadLinkAgent();
			}
			 
		}
	});
	FareClassValidation.screenInitilize();
	$("input[name='radFare']").click(function(){FareClassValidation.disableEnable(this.id)});
	$("#chkAdvance").click(function(){FareClassValidation.enebleAdvBookField();});
	$("#loadFactorEnabled").click(function(){FareClassValidation.loadFactorEnabled();});
	$("document").on("click", ".addNewRow", function(){FareClassValidation.addnewPeirodRow($(this))});
	$("#visibility").change(function(){FareClassValidation.setFareVisibility($(this))});
	$("#chkBulkTicket").change(function(){FareClassValidation.enableBulkTicket();});
	$(".groupClass").find("input[type='checkbox']").change(function(){FareClassValidation.updateFareModel(this);});
	FareClassValidation.disableAll();
	$(".preferd").on("change",function(){ FareClassValidation.tabsPreferdInputEnbleDisable(this)});
	$("document").on("mouseenter",".lastRow", function(){
		$(".lastRow").find(".deleteThis").fadeIn();
	});
	$("document").on("mouseleave",".lastRow", function(){
		$(".lastRow").find(".deleteThis").fadeOut();
	});
	$("document").on("click",".deleteThis", function(){
		FareClassValidation.deleteLatsPeirodRow($(this));
	});
	$(".desimalApply").numeric({allowDecimal:true});
	$(".numaricApply").numeric({allowDecimal:false});
	$("#tabs").show();
	$("#btnOverWriteRule").on("click", function(){ManageFareClasses.CWindowOpen(2)});
	$("#btnOverRideOHD").on("click", function(){ManageFareClasses.CWindowOpen(3)});
	$("#chkChrgRestrictions").on("click", FareClassValidation.setChargeRestriction);
	$("#selDefineModType, #selDefineCanType").on("change", function(){ FareClassValidation.tabsPreferdInputEnbleDisable(this)});
	if(FareClassValidation.isEnableFareRuleNCC){
		$("#selDefineNCCType").on("change", function(){ FareClassValidation.tabsPreferdInputEnbleDisable(this)});
	}
	
};

FareClassValidation.setChargeRestriction = function(e){
	if ($(e.target).prop("checked")){
		$("#selDefineModType").val(FareClassValidation.chargeRestrictionDefault.modtype).enable();
		$("#selDefineCanType").val(FareClassValidation.chargeRestrictionDefault.cantype).enable();
		$("#txtModification").val(FareClassValidation.chargeRestrictionDefault.modval).enable();
		$("#txtCancellation").val(FareClassValidation.chargeRestrictionDefault.canvalue).enable();
		$("#txtCancellationInLocal").val(FareClassValidation.chargeRestrictionDefault.modvallocal);
		$("#txtModificationInLocal").val(FareClassValidation.chargeRestrictionDefault.canvaluelocal);
		ManageFareClasses.enableDisableChargeCurrencyFields($("#selDefineCanType").val(),"canc");			
		ManageFareClasses.enableDisableChargeCurrencyFields($("#selDefineModType").val(),"mod");
		if(FareClassValidation.isEnableFareRuleNCC){
			$("#selDefineNCCType").val(FareClassValidation.chargeRestrictionDefault.ncctype).enable();
			$("#txtNCC").val(FareClassValidation.chargeRestrictionDefault.nccvalue).enable();
			$("#txtNCCInLocal").val(FareClassValidation.chargeRestrictionDefault.nccvallocal);
			ManageFareClasses.enableDisableChargeCurrencyFields($("#selDefineNCCType").val(),"ncc");
		}
	}else{
		$("#selDefineModType").val("").disable();
		$("#selDefineCanType").val("").disable();
		$("#txtModification").val("").disable();
		$("#txtCancellation").val("").disable();
		$("#txtCancellationInLocal").val("");
		$("#txtModificationInLocal").val("");
		$("#txtMin_canc").val("").disable();
		$("#txtMax_canc").val("").disable();
		$("#txtMin_mod").val("").disable();
		$("#txtMax_mod").val("").disable();
		
		if(FareClassValidation.isEnableFareRuleNCC){
			$("#selDefineNCCType").val("").disable();
			$("#txtNCC").val("").disable();
			$("#txtNCCInLocal").val("");
			$("#txtMin_NCC").val("").disable();
			$("#txtMax_NCC").val("").disable();
		}		
	}
};

FareClassValidation.screenInitilize = function(){
	   FareClassValidation.fareAppModel = [{name:"ADApp", checked:true},{name:"CHApp", checked:true},{name:"INApp", checked:true},
	   								 {name:"ADRef", checked:true},{name:"CHRef", checked:true},{name:"INRef", checked:false},
	   								 {name:"ADMod", checked:true},{name:"CHMod", checked:true},{name:"INMod", checked:false},
	   								 {name:"ADCNX", checked:true},{name:"CHCNX", checked:true},{name:"INCNX", checked:false},
	   								 {name:"ADNoShoCharge", checked:false},{name:"CHNoShoCharge", checked:false},{name:"INNoShoCharge", checked:false}];
	   FareClassValidation.fareCNXPeirodModel = [{rowid:"0", fareRuleFeeID:"", feeVersion:"",chargeAmount:"", chargeAmountLocal:"",chargeAmountType:"",chargeType:"", maxPerCharge:"", minPerCharge:"",compareAgainst:"",status:"", timeType:"", timeValue:""}];
	   FareClassValidation.fareMODPeirodModel = [{rowid:"0", fareRuleFeeID:"", feeVersion:"",chargeAmount:"", chargeAmountLocal:"",chargeAmountType:"",chargeType:"", maxPerCharge:"", minPerCharge:"",compareAgainst:"",status:"", timeType:"", timeValue:""}];
	   FareClassValidation.fareNCCPeirodModel = [{rowid:"0", fareRuleFeeID:"", feeVersion:"",chargeAmount:"", chargeAmountLocal:"",chargeAmountType:"",chargeType:"", maxPerCharge:"", minPerCharge:"",compareAgainst:"",status:"", timeType:"", timeValue:""}];
	   	$("#tabCancelation").showHideTabs(false, "Top");
		$("#tabModification").showHideTabs(false, "Top");
		$("#tabNoShow").showHideTabs(false, "Top");
		$("#tabLinkAgent").showHideTabs(false, "Top");
		$("#tabLinkPOS").showHideTabs(false, "Top");
		$("#tabs").tabs("option", "active", 0);
		if(isFlexiEnabled == "false"){
			$("#selFlexiCode").hide();
			$("#lblFlexiCode").hide();
		}
};

FareClassValidation.enebleAdvBookField = function (){
	if($("#chkAdvance").prop("checked")){
		$("#chkAdvance").parent().parent().find("input[type='text']").enable();
	}else{
		$("#chkAdvance").parent().parent().find("input[type='text']").disable("empty");
	}
};

FareClassValidation.enableBulkTicket = function(){
	if ($("#chkBulkTicket").prop("checked")){
		$("#chkBulkTicket").parent().parent().find("input[type='text']").enable();
		$("#chkBulkTicket").parent().parent().find(".mandatory").show();
	} else {
		$("#chkBulkTicket").parent().parent().find("input[type='text']").disable("empty");
		$("#chkBulkTicket").parent().parent().find(".mandatory").hide();
	}
}

FareClassValidation.disableAll = function(){
	$("#tabs").find("input").disable();
	$("#tabs").find("select").disable();
	$("#tabs").find("textarea").disable();
	$("#btnOverWriteRule, #btnOverRideOHD").disable();
	if (screeMode != "CREATEFARE"){
		$("#btnBack, #btnEditRule, #btnSave").show();
		$("#btnOverWriteRule, #btnOverRideOHD, #btnSave").enable();
	} else {
		$("#btnSave").disable();
	}
	$("#btnBack, #btnClose").enable();	
};

FareClassValidation.applyFareModelToCheckBox = function(){
	$.each(FareClassValidation.fareAppModel, function(key,obj){
		$("input#chk"+obj.name).attr("checked", obj.checked);
	});
	FareClassValidation.tabManupulation();
	FareClassValidation.tabsFieldsEnbleDisable();
};
FareClassValidation.tabsFieldsEnbleDisable = function(){
		//Cancelation
			
};

FareClassValidation.tabsPreferdInputEnbleDisable= function(obj){
	var item = $(obj);
	if (item.val() == "V"){
		item.parent().next().find("input").enable();
		//item.parent().next().next().find("input").disable();
		item.parent().next().next().next().find("input").disable();
		item.parent().next().next().next().next().find("input").disable();
	}else if ($(obj).val() == "PF" || $(obj).val() == "PFS" || $(obj).val() == "PTF"){
		item.parent().next().find("input").enable();
		//item.parent().next().next().find("input").enable();
		item.parent().next().next().next().find("input").enable();
		item.parent().next().next().next().next().find("input").enable();
	}else{
		item.parent().next().find("input").disable('empty');
		//item.parent().next().next().find("input").disable();
		item.parent().next().next().next().find("input").disable();
		item.parent().next().next().next().next().find("input").disable();
	}
}

FareClassValidation.tabManupulation = function(){
	if (FareClassValidation.fareAppModel[6].checked == true || 
		FareClassValidation.fareAppModel[7].checked == true || 
		FareClassValidation.fareAppModel[8].checked == true){
		if ($("#tabModification").css("display") == "none"){
			FareClassValidation.buildFareRulePeirod("MOD");
		}
		$("#tabModification").showHideTabs(true, "Top");	
		FareClassValidation.hasMOD = true;	
	}else{
		$("#tabModification").showHideTabs(false, "Top");
		FareClassValidation.hasMOD = false;
	}
	
	if(FareClassValidation.fareAppModel[9].checked == true || 
	   FareClassValidation.fareAppModel[10].checked == true || 
	   FareClassValidation.fareAppModel[11].checked == true){
		if ($("#tabCancelation").css("display") == "none"){
			FareClassValidation.buildFareRulePeirod("CNX");
		}
		$("#tabCancelation").showHideTabs(true, "Top");
		FareClassValidation.hasCNX = true;
	}else{
		$("#tabCancelation").showHideTabs(false, "Top");
		FareClassValidation.hasCNX = false;
	}
	
	if(FareClassValidation.fareAppModel[12].checked == true || 
	  FareClassValidation.fareAppModel[13].checked == true || 
 	  FareClassValidation.fareAppModel[14].checked == true){
		if ($("#tabNoShow").css("display") == "none"){
			FareClassValidation.buildFareRulePeirod("NOSHOW");
		}
		$("#tabNoShow").showHideTabs(true, "Top");
		FareClassValidation.hasNoShow = true;
	}else{
		$("#tabNoShow").showHideTabs(false, "Top");
		FareClassValidation.hasNoShow = false;
	}
}
FareClassValidation.updateFareModel = function(uObj){
	var key = (uObj.id.indexOf("chk") > -1)? uObj.id.substr(3,uObj.id.length): uObj.id;
	$.each(FareClassValidation.fareAppModel, function(i,obj){
		if (obj.name == key){
			obj.checked = uObj.checked;
		}
	});
	FareClassValidation.processLogic();
};

FareClassValidation.processLogic = function(){
	//CHECK AD Aplicable
	if (FareClassValidation.fareAppModel[0].checked == false){
		FareClassValidation.fareAppModel[2].checked = false;
		FareClassValidation.fareAppModel[3].checked = false;
		FareClassValidation.fareAppModel[5].checked = false;
		FareClassValidation.fareAppModel[6].checked = false;
		FareClassValidation.fareAppModel[6].disable = false;
		FareClassValidation.fareAppModel[8].checked = false;
		FareClassValidation.fareAppModel[9].checked = false;
		FareClassValidation.fareAppModel[11].checked = false;
		FareClassValidation.fareAppModel[12].checked = false;
		FareClassValidation.fareAppModel[12].disable = false;
		FareClassValidation.fareAppModel[14].checked = false;
	}

	//CHECK CH Aplicable
	if (FareClassValidation.fareAppModel[1].checked == false){
		FareClassValidation.fareAppModel[4].checked = false;
		FareClassValidation.fareAppModel[7].checked = false;
		FareClassValidation.fareAppModel[10].checked = false;
		FareClassValidation.fareAppModel[13].checked = false;
	}
	//CHECK INF Aplicable
	if (FareClassValidation.fareAppModel[2].checked == false){
		FareClassValidation.fareAppModel[5].checked = false;
		FareClassValidation.fareAppModel[8].checked = false;
		FareClassValidation.fareAppModel[11].checked = false;
		FareClassValidation.fareAppModel[14].checked = false;
	}
	FareClassValidation.applyFareModelToCheckBox();
	
};

FareClassValidation.enableAll = function(val){
	var inputs = $("#tabs").find("input[type!='button']");
	var txtAreas = $("#tabs").find("textarea");
	var listBoxes = $("#tabs").find("select");
	inputs.enable();
	//$("#txtMax_mod, #txtMin_mod, #txtMax_can, #txtMin_can, #selDefineModType, #txtModification, #selDefineCanType, #txtCancellation").disable();
	$("#tabs").find("input[type='button']").enable();
	txtAreas.enable();
	listBoxes.enable();
	FareClassValidation.tabsFieldsEnbleDisable();
	if (val == "NEW"){
		inputs.val("");
		txtAreas.val("");
		listBoxes.val("");
		FareClassValidation.screenInitilize();
		FareClassValidation.applyFareModelToCheckBox();
		$("#btnOverWriteRule, #btnOverRideOHD, #btnDelete, #btnViewAgntFC").disable();
		$("#fareDiscount").prop("checked",false);
		$("#chkUpdateCommentsOnly,#lblUpdateCommentsOnly").hide();
	} else {
		$("#ruleCode").disable();
		$("#chkUpdateCommentsOnly,#lblUpdateCommentsOnly").show();
	}
	inputs.on("change", function(){ManageFareClasses.editPage()});
	txtAreas.on("change",function(){ManageFareClasses.editPage()});
	listBoxes.on("change",function(){ManageFareClasses.editPage()});
	$("#chkInAll,#chkOutAll").click(function(){FareClassValidation.selectAllChecks(this);});
	FareClassValidation.loadFactorEnabled();
	
	if (!$("#chkAdvance").prop("checked")){
		$("#chkAdvance").parent().parent().find("input[type='text']").disable()
	};
	
	if (!$("#chkBulkTicket").prop("checked")){
		$("#chkBulkTicket").parent().parent().find("input[type='text']").disable()
	};
	
	$(".preferd").each(function(){
		FareClassValidation.tabsPreferdInputEnbleDisable(this)
	});
	if(!$("#fareDiscount").prop("checked")){
		$("#fareDiscountMin").disable();
		$("#fareDiscountMax").disable();
	}
	if(!isChecked('chkChrgRestrictions')){
		$("#selDefineModType, #selDefineCanType").prop('disabled', true).prop('readonly', true);
		$("#txtCancellationInLocal,#txtCancellation,#txtMin_canc,#txtMax_canc").prop('disabled', true).prop('readonly',true);
		$("#txtModificationInLocal,#txtModification,#txtMin_mod,#txtMax_mod").prop('disabled', true).prop('readonly',true);
		
		if(FareClassValidation.isEnableFareRuleNCC){
			$("#selDefineNCCType").prop('disabled',true).prop('readonly', true);
			$("#txtNCCInLocal,#txtNCC,#txtMin_NCC,#txtMax_NCC").prop('disabled',true).prop('readonly',true);
		}
	} else {		
		$("#txtCancellationInLocal,#txtCancellation,#txtMin_canc,#txtMax_canc").prop('disabled', false).prop('readonly',false);
		$("#txtModificationInLocal,#txtModification,#txtMin_mod,#txtMax_mod").prop('disabled', false).prop('readonly',false);
		
		if(FareClassValidation.isEnableFareRuleNCC){
			$("#selDefineNCCType").prop('disabled',false).prop('readonly', false);
			$("#txtNCCInLocal,#txtNCC,#txtMin_NCC,#txtMax_NCC").prop('disabled',false).prop('readonly',false);
		}
	}
};

FareClassValidation.enableDisableORTTime = function(enable){
	if(enable){
		$("#ortConfirm_MM").enable();
		$("#ortConfirm_DD").enable();
		$("#ortConfirm_HR").enable();
		$("#ortConfirm_MI").enable();
	}else{
		$("#ortConfirm_MM").val("").disable();
		$("#ortConfirm_DD").val("").disable();
		$("#ortConfirm_HR").val("").disable();
		$("#ortConfirm_MI").val("").disable();
	}
}

FareClassValidation.enableDisableStayOverTime = function(enable){
	if(enable){
		$("#minStayOver_MM").enable();
		$("#minStayOver_DD").enable();
		$("#minStayOver_HR").enable();
		$("#minStayOver_MI").enable();
		$("#maxStayOver_MM").enable();
		$("#maxStayOver_DD").enable();
		$("#maxStayOver_HR").enable();
		$("#maxStayOver_MI").enable();
	}else{
		$("#minStayOver_MM").disable();
		$("#minStayOver_DD").disable();
		$("#minStayOver_HR").disable();
		$("#minStayOver_MI").disable();
		$("#maxStayOver_MM").disable();
		$("#maxStayOver_DD").disable();
		$("#maxStayOver_HR").disable();
		$("#maxStayOver_MI").disable();
	}

}

FareClassValidation.disableEnable = function(jorneyType){
	if (jorneyType == "radOneWay"){
		$("#chkOutAll").attr("checked", true)
		$("#inBoundType").find("input[type='checkbox']").attr("checked", false);
		$("#inBoundType").find("input").val("");
		$("#inBoundType").find("input").disable();
		$("#divDateInbound").find("input").val("");
		$("#divDateInbound").find("input").disable();
		$("#chkPrintExp").disable(); 
		FareClassValidation.enableDisableStayOverTime(false);
		FareClassValidation.enableDisableORTTime(false);
		$("#outBoundFrom").val("00:00");
		$("#outBoundTo").val("23:59");
		$("#radHalfReturn").prop("checked", false).disable();
		$("#radOpReturn").prop("checked", false).disable();
		if(ManageFareClasses.mode != 'EDIT'){
			FareClassValidation.selectAllChecks({id:"chkOutAll",checked:true});	
			$("#outBoundFrom").val("00:00");
			$("#outBoundTo").val("23:59");
			$("#selFlexiCode").empty();
			$("#selFlexiCode").fillDropDown({dataArray:filterFlexiList('OW'), keyIndex:0, valueIndex:1, firstEmpty:true});
		}		
		FareClassValidation.isOpenReturn = false;
		FareClassValidation.isReturn = false;
		$("#radOneWay").attr("checked", true);
		
	}else if (jorneyType == "radReturn"){
		$("#inBoundType").find("input").enable();
		$("#divDateInbound").find("input").enable();
		$("#chkPrintExp").enable();
		$("#minStayOver").enable();
		$("#maxStayOver").enable();
		$("#ortConfirm").disable("empty");
		$("#radHalfReturn").enable();
		$("#radOpReturn").enable();
		
		FareClassValidation.enableDisableStayOverTime(true);
		if(ManageFareClasses.mode != 'EDIT'){
			FareClassValidation.selectAllChecks({id:"chkInAll",checked:true});
			$("#inBoundFrom").val("00:00");
			$("#inBoundTo").val("23:59");
			$("#selFlexiCode").empty();
			$("#selFlexiCode").fillDropDown({dataArray:filterFlexiList('RT'), keyIndex:0, valueIndex:1, firstEmpty:true});
		}
		FareClassValidation.isOpenReturn = false;
		FareClassValidation.isReturn = true;
		
		if(isChecked('radOpReturn')){
			FareClassValidation.enableDisableORTTime(true);
		}else{
			FareClassValidation.enableDisableORTTime(false);
		}
	}else if (jorneyType == "radHalfReturn"){
		$("#inBoundType").find("input").enable();
		$("#divDateInbound").find("input").enable();
		$("#chkPrintExp").enable();
		$("#minStayOver").enable();
		$("#maxStayOver").enable();
		$("#ortConfirm").enable();
		if(isChecked('radOpReturn')){
			FareClassValidation.isOpenReturn = true;	
		}else{
			FareClassValidation.isOpenReturn = false;
		}
		FareClassValidation.isReturn = true;
		$("#selFlexiCode").empty();
		$("#selFlexiCode").fillDropDown({dataArray:filterFlexiList('RT'), keyIndex:0, valueIndex:1, firstEmpty:true});		
	}else{
		$("#inBoundType").find("input").enable();
		$("#divDateInbound").find("input").enable();
		$("#chkPrintExp").enable();
		$("#minStayOver").enable();
		$("#maxStayOver").enable();
		$("#ortConfirm").enable();
		$("#selFlexiCode").empty();
		$("#selFlexiCode").fillDropDown({dataArray:filterFlexiList('RT'), keyIndex:0, valueIndex:1, firstEmpty:true});
		if(isChecked('radOpReturn')){
			FareClassValidation.isOpenReturn = true;
			FareClassValidation.enableDisableORTTime(true);
		}else{
			FareClassValidation.isOpenReturn = false;
			FareClassValidation.enableDisableORTTime(false);
		}
	}
};

FareClassValidation.loadFactorEnabled = function(){
	if ($("#loadFactorEnabled").attr("checked")){
		FareClassValidation.loadFactorEnable = true;
		$("#loadFactorMin").enable();
		$("#loadFactorMax").enable();
	}else{
		FareClassValidation.loadFactorEnable = false;
		$("#loadFactorMin").disable("empty");
		$("#loadFactorMax").disable("empty");
	}
};

FareClassValidation.setPeriodMask = function(item,itemVal){
	var retVal = "";
	if (itemVal == ""){
		retVal =  "";
	}else if(itemVal.toUpperCase() == "ANY"){
		retVal = "ANY";
	}else{
		var valArr = new Array();
		if (itemVal.indexOf(":") > -1){
			valArr = itemVal.split(":");
		}else{
			valArr[0] = $.trim(itemVal);
		}
		if (valArr.length > 4){
			alert("Invalid length");
			retVal =  "";
		}else{
			for (var i = 0; i < 4; i++){
				if (valArr[i] == undefined){
					valArr.push("0");
				}
				valArr[i] = $.trim(valArr[i]);
				if (valArr[i] == ""){
					valArr[i] = "0";
				}
			}
			if (valArr[1] >= 30){
				valArr[0] = parseInt(valArr[0]) + parseInt(valArr[1]/30);
				valArr[1] = parseInt(valArr[1]%30);
			}
			if (valArr[2] >= 24){
				valArr[1] = parseInt(valArr[1]) + parseInt(valArr[2]/24);
				valArr[2] = parseInt(valArr[2]%24);
			}
			if (valArr[3] >= 60){
				valArr[2] = parseInt(valArr[2]) + parseInt(valArr[3]/60);
				valArr[3] = parseInt(valArr[3]%60);
			}
			for(var j=0;j<valArr.length;j++){
				if ( (/[^0-9-]/g).test(String(valArr[j])) ){
					alert("Invalid values entered");
					$(item).focus();
					$(item).focus(function(){
						this.select();
					});
				}
			}
			retVal = valArr[0] + ":" + valArr[1] + ":" + valArr[2] + ":" +valArr[3];
		}
	}
	if ($(item).hasClass("toFarePeriod")){
		//FareClassValidation.addnewPeirodRow(item);
	}
	return retVal
};

FareClassValidation.buildFareRulePeirod = function (prefix){
	var objToRepeat = "";
	var type = "";
	var tabName = "";
	if (prefix == "CNX"){
		objToRepeat = (FareClassValidation.fareCNXPeirodModel == null) ? [{}] : FareClassValidation.fareCNXPeirodModel;
		type = "fareRuleCNXPeirodTemplate";
		tabName = "tabCancelation";
	}else if (prefix == "MOD"){
		objToRepeat = (FareClassValidation.fareMODPeirodModel == null) ? [{}] : FareClassValidation.fareMODPeirodModel;
		type = "fareRuleMODPeirodTemplate";
		tabName = "tabModification";
	}else if (prefix == "NOSHOW"){
		objToRepeat = (FareClassValidation.fareNOSHOWPeirodModel == null) ? [{}] : FareClassValidation.fareNOSHOWPeirodModel;
		type = "fareRuleNOSHOWPeirodTemplate";
		tabName = "tabNoShow";
	}else{
		objToRepeat = [];
		type = "";
		tabName = "";
	}
	$("#"+tabName).find(".peirodRows").html($("#"+type));
	$("#"+type).iterateTemplete({templeteName:type, data:objToRepeat, dtoName:prefix});
	$("#"+tabName).find(".peirodRows").find("tr:last").addClass("lastRow");
};
FareClassValidation.deleteLatsPeirodRow = function (obj){
	var t = obj.parent().parent().get(0).id.split("_");
	if (t[1] != 0){
		obj.parent().parent().prev("tr").addClass("lastRow");
		obj.parent().parent().remove();
	}else{
		alert("Can not remove the last row")
	}
	
};
FareClassValidation.addnewPeirodRow = function(obj){
	var type ="";
	var objectIndex = 0;
	var tabName = "";
	if (obj.get(0).tagName != "TD"){
		var t = obj.id.split("[");
		objectIndex = parseInt(t1[0],10);
		if (t[1] != undefined) var t1 = t[1].split("]");
		prefix = t[0];
		if (prefix == "CNX"){
			type = "fareRuleCNXPeirodTemplate";
			tabName = "tabCancelation";
		}else if (prefix == "MOD"){
			type = "fareRuleMODPeirodTemplate";
			tabName = "tabModification";
		}else if (prefix == "NOSHOW"){tempAppModel;
			type = "fareRuleNOSHOWPeirodTemplate";
			tabName = "tabNoShow";
		}else{
			type = "";
		}
	}else{
		var t = obj.parent().parent().prev(".peirodRows").find(".lastRow").get(0).id.split("_");
		type = t[0];
		objectIndex = (t[1] != undefined) ? parseInt(t[1],10) : 0;
		if (type == "fareRuleCNXPeirodTemplate"){
			prefix = "CNX";
			tabName = "tabCancelation";
		}else if (type == "fareRuleMODPeirodTemplate"){
			prefix = "MOD";
			tabName = "tabModification";
		}else if (type == "fareRuleNOSHOWPeirodTemplate"){
			type = "NOSHOW";
			tabName = "tabNoShow";
		}else{
			type = "";
		}
	}
	if (objectIndex != NaN){
		$("#"+tabName).find(".peirodRows").find("tr").removeClass("lastRow");
		$(".deleteThis").hide();
		var clonedTempId = type + "_" + (objectIndex+1);
		var clone = $( "#" + type + "").clone();
		clone.attr("id", clonedTempId );
		clone.appendTo($("#" + type).parent());
		elementIdAppend =  prefix + "[" + (objectIndex+1) + "].";
		$("#"+clonedTempId).addClass("lastRow");
		$("#"+clonedTempId).find("input").each(function(intIndex){
			$(this).attr("id",  elementIdAppend +  this.id);
		});
		$("#"+clonedTempId).find("select").each(function(intIndex){
			$(this).attr("id",  elementIdAppend +  this.id);
		});
		$("#"+clonedTempId).show();
	}
	
};
FareClassValidation.setFareVisibility = function(obj){
	var t = obj.val();
	$("#tabLinkAgent").showHideTabs(false, "Top");
	$("#tabLinkPOS").showHideTabs(false, "Top");
	try{
		$.each(t,function(i,j){
			blnShow = false;
			if (j == 3){
				FareClassValidation.displayLinkAgent();
				//From this line it is prevent '~' and ',' characters for "Agent Comments".
				// This SpecialCharactersPrevent method declared on jQuery.common.js
				$("#txtaInstructions").SpecialCharactersPrevent();
			} else if(j == 4){
				if(isPosWebFaresEnabled == "true"){
					$("#tabLinkPOS").showHideTabs(true, "Top");
				}
				
				if (FareClassValidation.isLoadPOS == false){					
					FareClassValidation.loadLinkPOS();
				}
				
				if(ManageFareClasses.mode == "ADD" || ManageFareClasses.FAREMODE.ADDHOCSAVE){
					var lstPOSMultiSel = null;
					if(FareClassValidation.isLoadPOS){
						lstPOSMultiSel = $("#lstPOS").multiSelect.getRightData('lstPOS');
						if(lstPOSMultiSel == null || lstPOSMultiSel == "" || lstPOSMultiSel.length == 0){							
							FareClassValidation.pos = true;
							$("#addAllRight_lstPOS").trigger('click');
						}
					}
					
				}
			}
			if (j == 1){
				obj.val(j);
			}
		});
	}catch(e){}
	

		
};

FareClassValidation.displayLinkAgent = function(){
	$("#tabLinkAgent").showHideTabs(true, "Top");
};

FareClassValidation.selectAllChecks = function(obj){
	if (obj.id =="chkInAll" && obj.checked){
		$("#inBoundType").find("input[type='checkbox']").attr("checked", true);
	}else if(obj.id =="chkInAll" && !obj.checked){
		$("#inBoundType").find("input[type='checkbox']").attr("checked", false);
	}else if(obj.id =="chkOutAll" && obj.checked){
		$("#outBoundType").find("input[type='checkbox']").attr("checked", true);
	}else{
		$("#outBoundType").find("input[type='checkbox']").attr("checked", false);
	}
}

FareClassValidation.befoarSaveData = function(){
var validated = false;
var isFareDiscountEnabled = $("#fareDiscount").prop('checked');
var fareDiscountMin = $("#fareDiscountMin").val();
var fareDiscountMax = $("#fareDiscountMax").val();

var lstPOSMultiSel = null;
if(FareClassValidation.isLoadPOS){
	lstPOSMultiSel = $("#lstPOS").multiSelect.getRightData('lstPOS');
}

if ((!isChecked('chkADApp')) || (!isChecked('chkADApp')) || (!isChecked('chkADApp'))) {
	var confirmation = window.confirm("Applicability for primary passenger type is empty. Are you sure you want to save this fare rule ?");
	if (!confirmation) {
		return;
	}
}

if (!FareClassValidation.isAdHocFare && $("#ruleCode").val() == "") {
	showERRMessage(FareClassValidation.errorInfo.fareclassRqrd);
	$("tabFareRule#ruleCode").tabFocus("tabs");
} else if (!FareClassValidation.isAdHocFare && $("#description").val() == "") {
	showERRMessage(FareClassValidation.errorInfo.descriptionRqrd);
	$("tabFareRule#description").tabFocus("tabs");
} else if ($("#visibility").val() == "") {
	showERRMessage(FareClassValidation.errorInfo.visibilityRqrd);
	$("tabFareRule#visibility").tabFocus("tabs");
}else if(isChecked('radSelCurrFareRule') && $("#selCurrencyCode").val() == ""){
	showERRMessage(FareClassValidation.errorInfo.localCurrencyCodeRqrd);
	$("tabFareRule#visibility").tabFocus("tabs");
}else if((FareClassValidation.isOpenReturn == true) && ($("#maxStayOver_MM").val() == "" || $("#maxStayOver_MM").val() == 0 ) 
		&& ($("#maxStayOver_DD").val() == "" || $("#maxStayOver_DD").val() == 0) 
		&& ($("#maxStayOver_HR").val() == "" || $("#maxStayOver_HR").val() == 0) 
		&& ($("#maxStayOver_MI").val() == "" || $("#maxStayOver_MI").val() == 0)){
	showERRMessage(FareClassValidation.errorInfo.maxStayRequired);
	$("tabFareRule#maxStayOver").tabFocus("tabs");
}else if( getTimeInMinitueValue("maxStayOver") < getTimeInMinitueValue("ortConfirm") &&
		FareClassValidation.isOpenReturn ){
	showERRMessage(FareClassValidation.errorInfo.confPeriodLarger);
	$("tabFareRule#ortConfirm").tabFocus("tabs");
}else if( ( FareClassValidation.isReturn || FareClassValidation.isOpenReturn ) &&
		getTimeInMinitueValue("maxStayOver") < getTimeInMinitueValue("minStayOver") ){
	showERRMessage(FareClassValidation.errorInfo.maxstayOverShouldBeGreater);
	$("tabFareRule#minStayOver").tabFocus("tabs");
}else if( ( FareClassValidation.isReturn || FareClassValidation.isOpenReturn ) &&
		!chk_Checked($("#inBoundType").find("input[type='checkbox']")) ){
		showERRMessage(FareClassValidation.errorInfo.arrivaldaysRqrd);
		$("tabFareRule#chkInbMon").tabFocus("tabs");
}else if( !chk_Checked($("#outBoundType").find("input[type='checkbox']")) ){
		showERRMessage(FareClassValidation.errorInfo.deptdaysRqrd);
		$("tabFareRule#chkInbMon").tabFocus("tabs");
}else if(!IsValidTime($("#outBoundFrom").val())){
	showERRMessage(FareClassValidation.errorInfo.depttimeFormat);
	$("tabFareRule#outBoundFrom").tabFocus("tabs");
}else if(!IsValidTime($("#outBoundTo").val())){
	showERRMessage(FareClassValidation.errorInfo.depttimeFormat);
	$("tabFareRule#outBoundTo").tabFocus("tabs");
}else if($("#outBoundFrom").val() == $("#outBoundTo").val()){
	showERRMessage(FareClassValidation.errorInfo.DeptFromTmExceeds);
	$("tabFareRule#inBoundTo").tabFocus("tabs");
}else if(( FareClassValidation.isReturn || FareClassValidation.isOpenReturn ) && 
		$("#inBoundFrom").val() == $("#inBoundTo").val() ){
	showERRMessage(FareClassValidation.errorInfo.ArrFromTmExceeds);
	$("tabFareRule#inBoundTo").tabFocus("tabs");
}else if( ( FareClassValidation.isReturn || FareClassValidation.isOpenReturn ) && 
		!IsValidTime($("#inBoundFrom").val())){
	showERRMessage(FareClassValidation.errorInfo.arrivaltimeFormat);
	$("tabFareRule#inBoundFrom").tabFocus("tabs");
}else if( ( FareClassValidation.isReturn || FareClassValidation.isOpenReturn ) && 
		!IsValidTime($("#inBoundTo").val()) ){
	showERRMessage(FareClassValidation.errorInfo.arrivaltimeFormat);
	$("tabFareRule#inBoundTo").tabFocus("tabs");
}else if($("#outBoundFrom").val() == $("#outBoundTo").val()){
	showERRMessage(FareClassValidation.errorInfo.DeptFromTmExceeds);
	$("tabFareRule#inBoundTo").tabFocus("tabs");
} else if ($("#visibility option:selected").text().toUpperCase().indexOf("AGENT") != -1 &&
		(FareClassValidation.agents == false)) {
	showERRMessage(FareClassValidation.errorInfo.agentsRqrd);
} else if ($("#visibility option:selected").text().toUpperCase().indexOf("WEB") != -1 &&
		(FareClassValidation.isLoadPOS && isPosWebFaresEnabled == "true" && (lstPOSMultiSel == null || lstPOSMultiSel == "" || lstPOSMultiSel.length == 0))) {
	showERRMessage(FareClassValidation.errorInfo.posRequired);
	return false;
}else if(FareClassValidation.validateBufferTime("intnModBuffertime") == false){
	return false;
} else if(FareClassValidation.validateBufferTime("domModBuffertime")== false){
	return false;
}else if (checkInvalidChar($("#comments").val(), FareClassValidation.errorInfo.invalidCharacter, "Rules and Comments") != "") {
	showERRMessage(FareClassValidation.errorInfo.invalidCharacter);
	$("#comments").focus();
	return false;
} else if (isChecked('chkBulkTicket') && $("#minBTSeatCount").val() == "") {
	showERRMessage(FareClassValidation.errorInfo.btMinSeatsRqrd);
	$("tabFareRule#minBTSeatCount").tabFocus("tabs");
} else if (isChecked('chkBulkTicket') && $("#minBTSeatCount").val() < FareClassValidation.minRequiredSeatsForBTRes) {
	showERRMessage(buildError(FareClassValidation.errorInfo.btMinSeatsIncorrect, (FareClassValidation.minRequiredSeatsForBTRes-1)));
} else {
	var selModeTyps = {
			v: "V",pf:"PF", pfs:"PFS"
		};
	var chkRest = isChecked('chkChrgRestrictions');
	var strMod = $("#txtModification").val();
	var strCancel = $("#txtCancellation").val();
	var strChgModInLocal = $("#txtModificationInLocal").val();
	var strChgCancelInLocal = $("#txtCancellationInLocal").val();
	var isLocalCurrency = isChecked('radSelCurrFareRule');
	var intChgMod = $("#txtModification").val();
	var intChgCancel = $("#txtCancellation").val();
	var intChgModInLocal = $("#txtModificationInLocal").val();
	var intChgCancelInLocal = $("#txtCancellationInLocal").val();
	var modType = $("#selDefineModType").val();
	var txtMaxVal_mod = $("#txtMax_mod").val();
	var txtMinVal_mod = $("#txtMin_mod").val();
	var txtMaxVal_canc = $("#txtMax_canc").val();
	var txtMinVal_canc = $("#txtMin_canc").val();
	var cancType = $("#selDefineCanType").val();

	if(!validateSpecialCharacters($("#ruleCode").val())){		
		showERRMessage(FareClassValidation.errorInfo.invalidFareRuleCode);
		$("tabFareRule#ruleCode").tabFocus("tabs");
		return false;
	}else if (!validateSpecialCharacters($("#description").val())){
		showERRMessage(FareClassValidation.errorInfo.invalidFareRuleDescription);
		$("tabFareRule#description").tabFocus("tabs");
		return false;
	} else if(chkRest && !isLocalCurrency && (strMod == "" && strCancel == "")) {
		showERRMessage(FareClassValidation.errorInfo.restrictionReqd);
		return false;
	} else if (modType == selModeTyps.v && chkRest && isLocalCurrency && (strChgModInLocal == "" && strChgCancelInLocal == "")) {
		showERRMessage(FareClassValidation.errorInfo.restrictionReqd);
		return false;
	} else if (chkRest && !isLocalCurrency && isNaN(intChgMod)) {
		showERRMessage(FareClassValidation.errorInfo.modNaN);
		$("tabFareRule#txtModification").tabFocus("tabs");
		return false;
	} else if (chkRest && isLocalCurrency && isNaN(intChgModInLocal)) {
		showERRMessage(FareClassValidation.errorInfo.modNaN);
		$("tabFareRule#txtModificationInLocal").tabFocus("tabs");
		return false;
	} else if (chkRest && !isLocalCurrency && isNaN(intChgCancel)) {
		showERRMessage(FareClassValidation.errorInfo.cancellationNaN);
		$("tabFareRule#txtCancellation").tabFocus("tabs");
		return false;
	} else if (chkRest && isLocalCurrency && isNaN(intChgCancelInLocal)) {
		showERRMessage(FareClassValidation.errorInfo.cancellationNaN);
		$("tabFareRule#txtCancellationInLocal").tabFocus("tabs");
		return false;
	}  else if (chkRest && modType == "") {
		showERRMessage(FareClassValidation.errorInfo.modTypeNotSelected);
		$("tabFareRule#selDefineModType").tabFocus("tabs");
		return false;
	}else if (modType != selModeTyps.v && !isLocalCurrency && (Number(strMod) < 0 ||  Number(strMod) > 100)){
		showERRMessage(modPrecentage);
		$("tabFareRule#txtCancellation").tabFocus("tabs");
		return false;
	}else if (modType != selModeTyps.v && isLocalCurrency && (Number(strChgModInLocal) < 0 ||  Number(strChgModInLocal) > 100)){
		showERRMessage(FareClassValidation.errorInfo.modPrecentage);
		$("tabFareRule#txtModificationInLocal").tabFocus("tabs");	
		return false;
	}  else if (chkRest && txtMinVal_mod != "" && isNaN(txtMinVal_mod)) {
		showERRMessage(FareClassValidation.errorInfo.typeMinNaN);
		$("tabFareRule#txtMin_mod").tabFocus("tabs");
		return false;	
	}  else if (chkRest && txtMaxVal_mod != "" && isNaN(txtMaxVal_mod)) {
		showERRMessage(FareClassValidation.errorInfo.typeMaxNaN);
		$("tabFareRule#txtMax_mod").tabFocus("tabs");
		return false;		
	}else if (chkRest && txtMaxVal_mod != "" && txtMinVal_mod != "" && 
			Number(txtMaxVal_mod)< Number(txtMinVal_mod)){	
		showERRMessage(FareClassValidation.errorInfo.modMaxShouldBeGreater);
		$("tabFareRule#txtMax_mod").tabFocus("tabs");	
		return false;		
	}  else if (chkRest && cancType == "") {
		showERRMessage(FareClassValidation.errorInfo.cancTypeNotSelected);
		$("tabFareRule#selDefineCanType").tabFocus("tabs");
		return false;
	}else if (cancType != selModeTyps.v && !isLocalCurrency && (Number(strCancel) < 0 || Number(strCancel) > 100)){
		showERRMessage(FareClassValidation.errorInfo.cancPrecentage);		
		$("tabFareRule#txtCancellation").tabFocus("tabs");
		return false;
	}else if (cancType != selModeTyps.v && isLocalCurrency && (Number(strChgCancelInLocal) < 0 || Number(strChgCancelInLocal) > 100)){
		showERRMessage(FareClassValidation.errorInfo.cancPrecentage);		
		$("tabFareRule#txtCancellationInLocal").tabFocus("tabs");
		return false;
	}else if (chkRest && modType != selModeTyps.v && txtMinVal_mod == "" && txtMaxVal_mod == ""){
		showERRMessage(FareClassValidation.errorInfo.modMinMaxEmpty);		
		$("tabFareRule#txtMin_mod").tabFocus("tabs");
		return false;
	}else if (chkRest && modType != selModeTyps.v && txtMaxVal_mod <=0){
		showERRMessage(FareClassValidation.errorInfo.maxModZero);
		$("tabFareRule#txtMax_mod").tabFocus("tabs");
		return false;
	}  else if (chkRest && txtMinVal_canc != "" && isNaN(txtMinVal_canc)) {		
		showERRMessage(FareClassValidation.errorInfo.typeMinNaN);
		$("tabFareRule#txtMin_canc").tabFocus("tabs");
		return false;
	}  else if (chkRest && txtMaxVal_canc != "" && isNaN(txtMaxVal_canc)) {	
		showERRMessage(FareClassValidation.errorInfo.typeMaxNaN);		
		$("tabFareRule#txtMax_canc").tabFocus("tabs");
		return false;
	}else if (chkRest && cancType != selModeTyps.v && txtMaxVal_canc == "" && txtMinVal_canc == ""){
		showERRMessage(FareClassValidation.errorInfo.cancMinMaxEmpty);
		$("tabFareRule#txtMin_canc").tabFocus("tabs");
		return false;
	}else if (chkRest && txtMaxVal_canc != "" && txtMinVal_canc != "" &&
			Number(txtMaxVal_canc)< Number(txtMinVal_canc)){
		showERRMessage(FareClassValidation.errorInfo.cancMaxShouldBeGreater);
		$("tabFareRule#txtMin_canc").tabFocus("tabs");
		return false;
	}else if (chkRest && cancType != selModeTyps.v && txtMaxVal_canc <=0){
		showERRMessage(FareClassValidation.errorInfo.maxCancZero);
		$("tabFareRule#txtMax_canc").tabFocus("tabs");
		return false;
	}
	if (isFareDiscountEnabled){
		if(fareDiscountMin == ""){
			showERRMessage("Fare discount min is required");
			$("#fareDiscountMin").focus();
			return false;
		}
		if(fareDiscountMax == ""){
			showERRMessage("Fare discount max is required");
			$("#fareDiscountMin").focus();
			return false;
		}
		
		var fdMin = parseInt(fareDiscountMin, 10);
		var fdMax = parseInt(fareDiscountMax, 10);
		if(fdMin < 0 || fdMin>100){
			showERRMessage("Fare discount min is invalid");
			$("#fareDiscountMin").focus();
			return false;
		}
		
		if(fdMax < 0 || fdMax>100){
			showERRMessage("Fare discount max is invalid");
			$("#fareDiscountMax").focus();
			return false;
		}
		
		if(fdMax<fdMin){
			showERRMessage("Fare discount max cannot be less than min");
			$("#fareDiscountMax").focus();
			return false;
		}
		validated = true;
	}
	
	if ( FareClassValidation.loadFactorEnable ) {
		//fare rule modifications and load factor
		var loadFactorMin = $("#loadFactorMin").val();
		var loadFactorMax = $("#loadFactorMax").val();
		if( loadFactorMin == "" && loadFactorMax == ""){			
			showERRMessage(FareClassValidation.errorInfo.lfReequired);
			$("tabValidityData#loadFactorMin").tabFocus("tabs");
			return false;
		}else if(loadFactorMin != "" && parseInt(loadFactorMin,10) < 0 && loadFactorMax == ""){
				showERRMessage(FareClassValidation.errorInfo.lfMinZero);
				$("tabValidityData#loadFactorMin").tabFocus("tabs");
		}else if(loadFactorMax != "" && parseInt(loadFactorMax,10) < 0 && loadFactorMin == ""){			
				showERRMessage(FareClassValidation.errorInfo.lfMaxZero);
				$("tabValidityData#loadFactorMax").tabFocus("tabs");
		}else if(loadFactorMin != "" && loadFactorMax != ""){			
			if(parseInt(loadFactorMin,10) < 0){				
				showERRMessage(FareClassValidation.errorInfo.lfMinZero);
				$("tabValidityData#loadFactorMin").tabFocus("tabs");
				return false;
			}else if(parseInt(loadFactorMax,10) < 0){				
				showERRMessage(FareClassValidation.errorInfo.lfMaxZero);
				$("tabValidityData#loadFactorMax").tabFocus("tabs");
				return false;
			}else if(parseInt(loadFactorMin,10) > parseInt(loadFactorMax,10)){				
				showERRMessage(FareClassValidation.errorInfo.lfMinGrMax);
				$("tabValidityData#loadFactorMin").tabFocus("tabs");
				return false;
			}
		}
	}
	var txtAgentCommision = getText("txtAgentCommision");
	var agentCommissionType = getText("selAgentComType");
	if (agentCommissionType != "") {
		if(txtAgentCommision == ""){
			showERRMessage(FareClassValidation.errorInfo.commissionValueReq);
			getFieldByID("txtAgentCommision").focus();
			return false;
		}else if (isNaN(parseFloat(trim(txtAgentCommision))) || parseFloat(trim(txtAgentCommision)) < 0){
			showERRMessage(FareClassValidation.errorInfo.commissionValueInvalid); 
			getFieldByID("txtAgentCommision").focus();
			return false;
		}else if(agentCommissionType == selModeTyps.pf && parseFloat(trim(txtAgentCommision)) > 100 ){
			showERRMessage(FareClassValidation.errorInfo.commissionValueExceeds);
			getFieldByID("txtAgentCommision").focus();
			return false;
		}
	}else if(txtAgentCommision != ""){
		showERRMessage(FareClassValidation.errorInfo.commissionTypeReq);
		return false;
	}
	
	var adultNoShowChargeType = $("#selADCGTYP").val();
	var childNoShowChargeType = $("#selCHCGTYP").val();
	var infantNoShowChargeType =  $("#selINCGTYP").val();

	var adultNoShowChargeBreakPoint = $("#txtADNoShoBreakPoint").val() != "" ? $("#txtADNoShoBreakPoint").val() : 0;
	var childNoShowChargeBreakPoint = $("#txtCHNoShoBreakPoint").val() != "" ? $("#txtCHNoShoBreakPoint").val() : 0;
	var infantNoShowChargeBreakPoint = $("#txtINNoShoBreakPoint").val() != "" ? $("#txtINNoShoBreakPoint").val() : 0;
	var adultNoShowChargeBoundary = $("#txtADNoShoBoundary").val() != "" ? $("#txtADNoShoBoundary").val() : 0;
	var childNoShowChargeBoundary = $("#txtCHNoShoBoundary").val() != "" ? $("#txtCHNoShoBoundary").val() : 0;
	var infantNoShowChargeBoundary = $("#txtINNoShoBoundary").val() != "" ? $("#txtINNoShoBoundary").val() : 0;
	var adultNoShowCharge = $("#txtADNoShoCharge").val();
	var childNoShowCharge = $("#txtCHNoShoCharge").val();
	var infantNoShowCharge = $("#txtINNoShoCharge").val();


	if (isNoShowChargeTypeNotV(adultNoShowChargeType) && parseFloat(adultNoShowChargeBreakPoint) == 0){
		showERRMessage(FareClassValidation.errorInfo.noShowBreakPointEmpty);
		$("#txtADNoShoBreakPoint").focus();
		return false;
	} else if(isNoShowChargeTypeNotV(adultNoShowChargeType) && parseFloat(adultNoShowChargeBoundary) == 0){
		showERRMessage(FareClassValidation.errorInfo.noShowBoundaryEmpty);
		$("#txtADNoShoBoundary").focus();
		return false;
	} else if(parseFloat(adultNoShowChargeBreakPoint) > parseFloat(adultNoShowChargeBoundary)){
		showERRMessage(FareClassValidation.errorInfo.noShowBoundaryBreakPoint);
		$("#txtADNoShoBoundary").focus();
		return false;
	} else if(isNoShowChargeTypeNotV(adultNoShowChargeType) && (parseFloat(adultNoShowCharge)==0 || parseFloat(adultNoShowCharge)>100)){
		showERRMessage(FareClassValidation.errorInfo.noShowChargePercentage);
		$("#txtADNoShoCharge").focus();
		return false;
	} else if(isNoShowChargeTypeNotV(childNoShowChargeType) && parseFloat(childNoShowChargeBreakPoint) == 0){
		showERRMessage(FareClassValidation.errorInfo.noShowBreakPointEmpty);
		$("#txtCHNoShoBreakPoint").focus();
		return false;
	}  else if(isNoShowChargeTypeNotV(childNoShowChargeType) && parseFloat(childNoShowChargeBoundary) == 0){
		showERRMessage(FareClassValidation.errorInfo.noShowBoundaryEmpty);
		$("#txtCHNoShoBoundary").focus();
		return false;
	} else if(parseFloat(childNoShowChargeBreakPoint) > parseFloat(childNoShowChargeBoundary)){
		showERRMessage(FareClassValidation.errorInfo.noShowBoundaryBreakPoint);
		$("#txtCHNoShoBoundary").focus();
		return false;
	} else if(isNoShowChargeTypeNotV(childNoShowChargeType) && (parseFloat(childNoShowCharge)==0 ||parseFloat(childNoShowCharge)>100)){
		showERRMessage(FareClassValidation.errorInfo.noShowChargePercentage);
		$("#txtCHNoShoCharge").focus();
		return false;
	} else if(isNoShowChargeTypeNotV(infantNoShowChargeType) && parseFloat(infantNoShowChargeBreakPoint) == 0){
		showERRMessage(FareClassValidation.errorInfo.noShowBreakPointEmpty);
		$("#txtINNoShoBreakPoint").focus();
		return false;
	} else if(isNoShowChargeTypeNotV(infantNoShowChargeType) && parseFloat(infantNoShowChargeBoundary) == 0){
		showERRMessage(FareClassValidation.errorInfo.noShowBoundaryEmpty);
		$("#txtINNoShoBoundary").focus();
		return false;
	} else if(parseFloat(infantNoShowChargeBreakPoint) > parseFloat(infantNoShowChargeBoundary)){
		showERRMessage(FareClassValidation.errorInfo.noShowBoundaryBreakPoint);
		$("#txtINNoShoBoundary").focus();
		return false;
	} else if(isNoShowChargeTypeNotV(infantNoShowChargeType) && (parseFloat(infantNoShowCharge)==0 ||parseFloat(infantNoShowCharge)>100 )){
		showERRMessage(FareClassValidation.errorInfo.noShowChargePercentage);
		$("#txtINNoShoCharge").focus();
		return false;
	}
	
	var MODValidate= true; var CNXValidate = true; var NOSHOWValidate = true;
	validated = MODValidate * CNXValidate * NOSHOWValidate;
	if (validated)
		top[2].HidePageMessage();
}
return validated;
};

FareClassValidation.validateBufferTime = function (id){
	var valid = true;
	
	var bufferLowerMillis = 0;
	var bufferLowerHours = 0;
	
	if(id == "domModBuffertime"){
		bufferLowerMillis= FareClassValidation.milliBufferTime.DOM;
		bufferLowerHours =  getFormatedHours(FareClassValidation.milliBufferTime.DOM);
	}else{
		bufferLowerMillis= FareClassValidation.milliBufferTime.INT;
		bufferLowerHours =  getFormatedHours(FareClassValidation.milliBufferTime.INT);
	}
	
	if($("#"+ id +"_DD").val() != ""  || $("#"+ id +"_HR").val() != ""  || $("#"+ id +"_MI").val() != ""  ){
		 if((getTimeInMinitueValue(id) * 60 * 1000)  < parseInt(bufferLowerMillis, 10)){
			valid = false;
		   	showERRMessage(FareClassValidation.errorInfo.modBufTimeShouldBeGreater + " " + bufferLowerHours);
		   	$("tabValidityData#" + id + "_DD").tabFocus("tabs");
	    }
   }
	return valid;
}

FareClassValidation.loadLinkAgent = function(){
	var url = "fareRule!loadAgentsData.action";
	var data = {};
	$.ajax({type: "POST", dataType: 'json',data: data , url:url, beforeSend:top[2].ShowProgress(),
	success: FareClassValidation.loadLinkAgentDataProcess, error:ManageFareClasses.errorLoading, cache : false});
	return false;
};

FareClassValidation.loadLinkAgentDataProcess = function(response) {
	if (isEmpty(response)) {
		//Load Network connection failure error
		ManageFareClasses.errorLoading();
		return false;
	}
		
	if (response.success) {	
		$("#lstAgents").multiSelect({
			data:response.agents,
			leftListBox:"lstAgents",
			label:"Agents",
			rightWidth:"400px",
			leftWidth:"400px",
			grouped:true
		});
		FareClassValidation.isLoadAgents = true;
		if (response.agents.length > 0) {
			FareClassValidation.agents = ManageFareClasses.visibleAgents;
			$("#lstAgents").multiSelect.loadRight(ManageFareClasses.visibleAgents, 'lstAgents');
		}
		if(!ManageFareClasses.FAREMODE.ADDHOCSAVE && ManageFareClasses.mode == ""){
			enabledDispableMultiselect('disable', 'agent');
		}
	} else {
		ManageFareClasses.errorLoading(response.messageTxt);
	}
	top[2].HideProgress();
}

FareClassValidation.loadLinkPOS = function(){
	var url = "fareRule!loadPOSData.action";
	var data = {};
	$.ajax({type: "POST", dataType: 'json',data: data , async: false, url:url, beforeSend:top[2].ShowProgress(),
	success: FareClassValidation.loadLinkPOSDataProcess, error:ManageFareClasses.errorLoading, cache : false});
	return false;
};

FareClassValidation.loadLinkPOSDataProcess = function(response) {
	if (isEmpty(response)) {
		//Load Network connection failure error
		ManageFareClasses.errorLoading();
		return false;
	}
		
	if (response.success) {	
		$("#lstPOS").multiSelect({
			data:response.pos,
			leftListBox:"lstPOS",
			label:"Country",
			rightWidth:"300px",
			leftWidth:"300px"
		});
		FareClassValidation.isLoadPOS = true;
		if (response.pos.length > 0) {
			FareClassValidation.pos = ManageFareClasses.pos;
			$("#lstPOS").multiSelect.loadRight(ManageFareClasses.pos, 'lstPOS');
		}
		if(!ManageFareClasses.FAREMODE.ADDHOCSAVE && ManageFareClasses.mode == ""){
			enabledDispableMultiselect('disable', 'pos');
		}
	} else {
		ManageFareClasses.errorLoading(response.messageTxt);
	}
	top[2].HideProgress();
}

function enabledDispableMultiselect(flag, element){
	if(flag=='enable'){
		if(element == 'agent'){			
			$("#lstAgents").enable();
			$("#lstAgentsAssigned").enable();
			$("#selectOptionslstAgents input[type='button']").enable();
		} else if(element == 'pos') {
			$("#lstPOS").enable();
			$("#lstPOSAssigned").enable();
			$("#selectOptionslstPOS input[type='button']").enable();
		}
	} else {
		if(element == 'agent'){			
			$("#lstAgents").disable();
			$("#lstAgentsAssigned").disable();
			$("#selectOptionslstAgents input[type='button']").disable();
		} else if(element == 'pos') {	
			$("#lstPOS").disable();
			$("#lstPOSAssigned").disable();
			$("#selectOptionslstPOS input[type='button']").disable();
		}
	}
}

function getTimeInMinitueValue(val){
	var M=0,D=0,H=0,m=0;
	if (val!= undefined){
		if($("#"+val+"_MM").length >0){
			M = $("#"+val+"_MM").val() != "" ? $("#"+val+"_MM").val() : 0;
		}
		D = $("#"+val+"_DD").val() != "" ? $("#"+val+"_DD").val() : 0;
		H = $("#"+val+"_HR").val() != "" ? $("#"+val+"_HR").val() : 0;
		m = $("#"+val+"_MI").val() != "" ? $("#"+val+"_MI").val() : 0;
		var retVal = parseInt(M) * 30 * 24 * 60 + 
					 parseInt(D) * 24 * 60 + 
					 parseInt(H) * 60 +
					 parseInt(m);
	}
	return retVal;
}

function getFormatedHours(milis){
	return milis/3600000 + " Hours ";
}

function getPrefixByTabName(name){
	var returnVal = null;
	if(name == "tabModification"){
		returnVal = "MOD";
	}else if(name == "tabCancelation"){
		returnVal = "CNX";
	}else if(name == "tabNoShow"){
		returnVal = "NOSHOW";
	}else{
		returnVal = null;
	}
	return returnVal;
}

function getTabNameByPrefix(prefix){
	var returnVal = null;
	if(prefix == "MOD"){
		returnVal = "tabModification";
	}else if(prefix == "CNX"){
		returnVal = "tabCancelation";
	}else if(prefix == "NOSHOW"){
		returnVal = "tabNoShow";
	}else{
		returnVal = null;
	}
	return returnVal;
}

function chk_Checked(arr) {
	var validated = false;
	for ( var i = 0; i < arr.length; i++) {
		if ($(arr[i]).attr("checked")) {
			validated = true;
			break;
		}
	}
	return validated;
}



//replace with new

//Reset Button Click
function resetClick() {
	var strMode = getText("hdnMode");
	setPageEdited(false);
	if (strMode == "Add") {
		top[2].HidePageMessage();
		resetFareRuleModifications();
		ctrl_Addclick();

	} else if (strMode == "Edit") {
		updateClicked = false;
		clearControls();
		getFieldByID("selFareVisibility").options[0].selected = false;
		agentCodes = "";
		top[2].HidePageMessage();
		document.getElementById("frmFareClasses").reset();
		disableInputControls("");
		setFields(strGridRow);
		ctrl_visibilityAgents();
		var rest = getFieldByID("chkChrgRestrictions").checked;
		if (rest == true) {
			disableCnxModTxt(false);	
		} else {
			disableCnxModTxt(true);	
		}
		if (!getFieldByID("chkAdvance").checked) {
			Disable("txtAdvBookDays", true);
		}

		Disable("txtFc", true);
		var checked = getText("radFare");
		
		if (checked != "Return") {
			disableReturn(true);
			Disable("chkPrintExp", true);
			for ( var t = 0; t < arrDays.length; t++) {
				Disable(arrDays[t], true);
			}
			Disable("chkInAll", true);
		}

	} else {
		resetToDefault();
	}
}

function validateSpecialCharacters(val){
	if(val.indexOf("'") != -1 || val.indexOf('"') != -1 || val.indexOf("-") != -1 || 
			val.indexOf("<") != -1 || val.indexOf(">") != -1 || val.indexOf("/") != -1){
		return false;
	}
	return true;
}

function KPValidatePositiveInteger(control, type) {
	var strCC = getText(control.id);
	var strLen = strCC.length;
	var blnVal = isPositiveInt(strCC)
	if (!blnVal) {
		setField(control.id, strCC.substr(0, strLen - 1));
		$("#"+control.id).focus();
	}
	switch(type){
		case "MON": if(Number(getText(control.id)) > 12){
						setField(control.id, strCC.substr(0, strLen - 1));
						$("#"+control.id).focus();
					}
					break;
		case "DAY":if(Number(getText(control.id)) > 31){
						setField(control.id, strCC.substr(0, strLen - 1));
						$("#"+control.id).focus();
					}
					break;
		case "HRS":if(Number(getText(control.id)) > 23){
						setField(control.id, strCC.substr(0, strLen - 1));
						$("#"+control.id).focus();
					}
					break;
		case "MIN":	if(Number(getText(control.id)) > 59){
						setField(control.id, strCC.substr(0, strLen - 1));
						$("#"+control.id).focus();
					}
					break;
	
	}
}



