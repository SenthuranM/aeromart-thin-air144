var tab_counter = 0;
var uri = "";
var vias =  "" ;
var origin = "";
var desitination = "";
var via1 = "";
var via2 = "";
var via3 = "";
var via4 = "";
var roots = new Array();
var jsfareProperties = [ ];
var jsTree = [ {roots:"", frs:"", bcs:""} ];
var jsYears = [	
          		{month:"Jan", nod:"31"},{month:"Feb", nod:"28"},{month:"Mar", nod:"31"},{month:"Apr", nod:"30"},
          		{month:"May", nod:"31"},{month:"Jun", nod:"30"},{month:"Jul", nod:"31"},{month:"Aug", nod:"31"},
          		{month:"Sep", nod:"30"},{month:"Oct", nod:"31"},{month:"Nov", nod:"30"},{month:"Dec", nod:"31"}
				];

var jsRoots = [	
               		{oriStationCode:"SHJ", dRoots:"SHJ-CMB,SHJ-KWI,SHJ-DOH", hRoots:"SHJ-CMB-DEL,SHJ-KWI-DOH,SHJ-KWI-EVN,SHJ-KWI-BAH"},
					{oriStationCode:"CMB", dRoots:"CMB-SHJ,CMB-KWI,CMB-DOH", hRoots:"CMB-SHJ-KWI,CMB-SHJ-DOH"},
					{oriStationCode:"DEL", dRoots:"DEL-SHJ,DEL-KWI,DEL-DOH", hRoots:"DEL-SHJ-DOH"},
					{oriStationCode:"KWI", dRoots:"KWI-SHJ,KWI-KWI,KWI-DOH", hRoots:"KWI-SHJ-CMB,KWI-SHJ-DOH"},
					{oriStationCode:"DOH", dRoots:"DOH-SHJ,DOH-CMB,DOH-DOH", hRoots:"DOH-SHJ-KWI"}
					];
var jsCntryCity = [
					{countryCode:"AE", countryName:"United Arab Emirates", stationCode:"DXB", stationDesc:"Dubai"},
					{countryCode:"PA", countryName:"Paris", stationCode:"CDG", stationDesc:"Paris"},
					{countryCode:"LE", countryName:"Mexico", stationCode:"LEN", stationDesc:"Leon"},
					{countryCode:"LE", countryName:"Marrakech", stationCode:"RAK", stationDesc:"Marrakech"}
				];
/*
var jsCntryCity = [
					{countryCode:"AF", countryName:"Afghanistan", stationCode:"KBL", stationDesc:"Kabul"},
					{countryCode:"AM", countryName:"Armenia", stationCode:"EVN", stationDesc:"Yerevan "},
					{countryCode:"BH", countryName:"Bahrain", stationCode:"BAH", stationDesc:"Bahrain"},
					{countryCode:"BD", countryName:"Bangladesh", stationCode:"CGP", stationDesc:"Chittagong"},
					{countryCode:"BD", countryName:"Bangladesh", stationCode:"DAC", stationDesc:"DHAKA"},
					{countryCode:"OL", countryName:"COUNTRY FOR OFFLINE AGENT", stationCode:"OLA", stationDesc:"STATION FOR OFFLINE AGENTS"},
					{countryCode:"EG", countryName:"Egypt", stationCode:"ALY", stationDesc:"Alexandria"},
					{countryCode:"EG", countryName:"Egypt", stationCode:"ATZ", stationDesc:"Assiut"},
					{countryCode:"EG", countryName:"Egypt", stationCode:"LXR", stationDesc:"Luxor"},
					{countryCode:"GR", countryName:"Greece", stationCode:"ATH", stationDesc:"Athens"},
					{countryCode:"IN", countryName:"India", stationCode:"AMD", stationDesc:"Ahmedabad"},
					{countryCode:"IN", countryName:"India", stationCode:"BLR", stationDesc:"BANGALORE"},
					{countryCode:"IN", countryName:"India", stationCode:"BOM", stationDesc:"Mumbai"},
					{countryCode:"IN", countryName:"India", stationCode:"CCJ", stationDesc:"Calicut"},
					{countryCode:"IN", countryName:"India", stationCode:"CJB", stationDesc:"Coimbatore"},
					{countryCode:"IN", countryName:"India", stationCode:"COK", stationDesc:"Cochin"},
					{countryCode:"IN", countryName:"India", stationCode:"DEL", stationDesc:"New Delhi"},
					{countryCode:"IN", countryName:"India", stationCode:"GOI", stationDesc:"Goa"},
					{countryCode:"IN", countryName:"India", stationCode:"HYD", stationDesc:"Hyderabad"},
					{countryCode:"IN", countryName:"India", stationCode:"JAI", stationDesc:"Jaipur"},
					{countryCode:"IN", countryName:"India", stationCode:"MAA", stationDesc:"Chennai"},
					{countryCode:"IN", countryName:"India", stationCode:"NAG", stationDesc:"Nagpur"},
					{countryCode:"IN", countryName:"India", stationCode:"TRV", stationDesc:"Trivandrum"},
					{countryCode:"IR", countryName:"Iran", stationCode:"IKA", stationDesc:"Tehran"},
					{countryCode:"IR", countryName:"Iran", stationCode:"SYZ", stationDesc:"Shiraz"},
					{countryCode:"JO", countryName:"Jordan", stationCode:"AMM", stationDesc:"Amman"},
					{countryCode:"KZ", countryName:"Kazakhstan", stationCode:"ALA", stationDesc:"Almaty"},
					{countryCode:"KZ", countryName:"Kazakhstan", stationCode:"TSE", stationDesc:"Astana"},
					{countryCode:"KE", countryName:"Kenya", stationCode:"NBO", stationDesc:"NAIROBI"},
					{countryCode:"KW", countryName:"Kuwait", stationCode:"KWI", stationDesc:"Kuwait"},
					{countryCode:"LB", countryName:"Lebanon", stationCode:"BEY", stationDesc:"Beirut"},
					{countryCode:"MY", countryName:"Malaysia", stationCode:"KUL", stationDesc:"Kuala Lumpur"},
					{countryCode:"MV", countryName:"Maldives", stationCode:"MLE", stationDesc:"Maldives"},
					{countryCode:"NP", countryName:"Nepal", stationCode:"KTM", stationDesc:"Nepal"},
					{countryCode:"OM", countryName:"Oman", stationCode:"MCT", stationDesc:"Muscat"},
					{countryCode:"PK", countryName:"Pakistan", stationCode:"KHI", stationDesc:"Karachi"},
					{countryCode:"PK", countryName:"Pakistan", stationCode:"PEW", stationDesc:"Peshawar"},
					{countryCode:"QA", countryName:"Qatar", stationCode:"DOH", stationDesc:"Doha"},
					{countryCode:"RU", countryName:"Russia", stationCode:"VKO", stationDesc:"Moscow"},
					{countryCode:"SA", countryName:"Saudi Arabia", stationCode:"DMM", stationDesc:"Dammam"},
					{countryCode:"SA", countryName:"Saudi Arabia", stationCode:"JED", stationDesc:"Jeddah"},
					{countryCode:"SA", countryName:"Saudi Arabia", stationCode:"KSC", stationDesc:"KSA Call Centre"},
					{countryCode:"SA", countryName:"Saudi Arabia", stationCode:"RUH", stationDesc:"Riyadh"},
					{countryCode:"SG", countryName:"Singapore", stationCode:"SIN", stationDesc:"SINGAPORE"},
					{countryCode:"LK", countryName:"Sri Lanka", stationCode:"CMB", stationDesc:"Colombo"},
					{countryCode:"SD", countryName:"Sudan", stationCode:"KRT", stationDesc:"Kartoum"},
					{countryCode:"SY", countryName:"Syria", stationCode:"ALP", stationDesc:"Aleppo"},
					{countryCode:"SY", countryName:"Syria", stationCode:"DAM", stationDesc:"Damascus"},
					{countryCode:"SY", countryName:"Syria", stationCode:"LTK", stationDesc:"Latakia"},
					{countryCode:"TR", countryName:"Turkey", stationCode:"IST", stationDesc:"Istanbul"},
					{countryCode:"TR", countryName:"Turkey", stationCode:"SAW", stationDesc:"Istanbul"},
					{countryCode:"UA", countryName:"Ukraine", stationCode:"KBP", stationDesc:"KIEV"},
					{countryCode:"AE", countryName:"United Arab Emirates", stationCode:"AAN", stationDesc:"Al Ain"},
					{countryCode:"AE", countryName:"United Arab Emirates", stationCode:"AJM", stationDesc:"Ajman"},
					{countryCode:"AE", countryName:"United Arab Emirates", stationCode:"AUH", stationDesc:"Abu Dhabi"},
					{countryCode:"AE", countryName:"United Arab Emirates", stationCode:"DXB", stationDesc:"Dubai"},
					{countryCode:"AE", countryName:"United Arab Emirates", stationCode:"FJR", stationDesc:"Fujairah"},
					{countryCode:"AE", countryName:"United Arab Emirates", stationCode:"RKT", stationDesc:"Ras Al Khaimah"},
					{countryCode:"AE", countryName:"United Arab Emirates", stationCode:"SHJ", stationDesc:"Sharjah"},
					{countryCode:"AE", countryName:"United Arab Emirates", stationCode:"TBD", stationDesc:"TBD"},
					{countryCode:"AE", countryName:"United Arab Emirates", stationCode:"UAQ", stationDesc:"Umm Al Qaiwan"},
					{countryCode:"YE", countryName:"Yemen", stationCode:"SAH", stationDesc:"Sanaa"}
				];
*/
