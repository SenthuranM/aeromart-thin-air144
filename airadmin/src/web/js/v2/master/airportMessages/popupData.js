
var win1;

/**
 * UI_popup related functionalities
 */


function UI_AirportMsg_LanguageData() {
}

UI_AirportMsg_LanguageData.showPopUp = function (i)
{
	var win=eval('win'+i);
	win.OpenWindow(); 
}

UI_AirportMsg_LanguageData.hidePopUp = function (i)
{
	var win=eval('win'+i);
	win.CloseWindow();
}

UI_AirportMsg_LanguageData.createPopup = function (title)
{
	var params = {
			BorderColor : 'Transparent', 
			BorderWidth : 0, 
			CloseBoxHeight : 15, 
			CloseBoxSrc : POPUP_IMAGE_DIR+'close15_no_cache.gif', 
			CloseBoxWidth : 15, 
			ContentColor : '#F4F4F4',  
			ContentHTML : '<span id="spnPopupData"></span>', 
			ContentLeftBorderColor : '#5A5C63', 
			ContentLeftBorderWidth : 4, 
			ContentRightBorderWidth : 4, 
			ContentRightBorderColor : '#5A5C63', 
			Height : 240, 
			InnerBorderWidth : 0, 
			OuterBorderColor : '#5A5C63',  
			OuterBorderWidth : 0, 
			Resizable : 'None', 
			StatusBarTextMargin : 6, 
			StatusColor : '#5A5C63', 
			StatusBarHTML :'',
			TitleBarHeight : 20, 
			TitleBarText : title,
			TitleBarAlign : 'left', 			
			TitleColor : '#5A5C63', 
			Width : 550, 
			ContentFontSize : 9,
			Id  : 'win1'	
	}
	
	win1=new DHTMLPopUp(params); 
}
 
  
 
  
 