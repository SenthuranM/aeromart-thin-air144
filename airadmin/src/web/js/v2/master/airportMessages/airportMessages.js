/**
 * Airport Message Management Main JS
 * @author EP
 */

UI_AirportMsg.screenID = "SC_ADMN_032";
UI_AirportMsg.addEdit; 


UI_AirportMsg.sizeOptions ={
	height:"130",width:"180"
}

/**
 * Onload function
 */
$( function() {

	$("#divMessagesSearch").decoratePanelWithAlign("Search Messages");
	$("#divResultsPanel").decoratePanelWithAlign("Search Results");
	$("#divDisplayAirportMsg").decoratePanelWithAlign("Add/Edit Message");
	
	$('#btnAdd').decorateButton();
	$('#btnEdit').decorateButton();
	$('#btnSearch').decorateButton();
	$('#btnSave').decorateButton();
	$('#btnClose').decorateButton();
	$('#btnReset').decorateButton();
	$('#btnMsgDisplayDes').decorateButton();
	
	$("#tblAirportMsg").jqGrid({ 
		url:'airportMessages!searchMessages.action',
		datatype: "json",	
		jsonReader : {
			  root: "rows", 
			  page: "page",
			  total: "total",
			  records: "records",
			  repeatitems: false,	
			  userdata : "userIDtoDisplayMap",
			  id: "0"				  
			},													
		colNames:['&nbsp;','ID','Message Type', 'Airport','Content', 'DepArr Type', 'Display From', 'Display To','Valid From', 'Valid To',
		          'Selected Flights', 'Selected Sales Channels','Selected ONDS','Version',
		          'Flight Numbers','OND s', 'Sales Channels','Flightt Numbers Inc', 'ONDs Inc', 
		          'languageList'], 
		colModel:[ 	{name:'id', width:5, jsonmap:'id' },  
		           	{name:'messageId',		index: 'messageId', 		width:15, align:"left", hidden:true },	
		           	{name:'messageType', 	index: 'messageType', 		width:30, align:"left" },
		           	{name:'airport', 	index: 'airport', 		width:30, align:"left" },
		           	{name:'message',	index: 'messageContent', 	width:100, align:"left"},
		           	{name:'depArrType',	index: 'depArrType', 	width:30, align:"left"},	
		           	{name:'displayFrom', 		index: 'displayFrom',			width:30, align:"left"},	
		           	{name:'dispalyTo', 			index: 'dispalyTo',			width:30, align:"left"},
		           	{name:'validFrom', 		index: 'validFrom',			width:30, align:"left"},	
		           	{name:'validTo', 			index: 'validTo',			width:30, align:"left"},		           		         	
		           	{name:'selectedflights',	index: 'selectedflights',	width:30, align:"left", hidden:true },
		           	{name:'selectedsalesChannels',	index: 'selectedsalesChannels',	width:30, align:"left", hidden:true },
		           	{name:'selectedonds',	index: 'selectedonds',	width:30, align:"left", hidden:true },
		           	{name:'version',				index: 'version',	width:30, align:"left", hidden:true },
		           	{name:'flightNumbers',		index: 'flightNumbers',			width:30, align:"left", hidden:true },		           	
		           	{name:'ONDs',	index: 'ONDs',	width:30, align:"left", hidden:true },
		           	{name:'salesChannels',		index: 'salesChannels',			width:30, align:"left", hidden:true },		           	
		           	{name:'flightNumbersInclusion',		index: 'flightNumbersInclusion',		width:30, align:"left", hidden:true },
		           	{name:'ONDsInclusion',	index: 'ONDsInclusion',	width:30, align:"left", hidden:true },      			           	
		           	{name:'languageList',	index: 'languageList',	width:30, align:"left", hidden:true }
					], 
		imgpath: '../../themes/default/images/jquery',
		multiselect:false,
		pager: $('#divAirportMsgPager'),
		rowNum:10,						
		viewrecords: true,
		height:70,
		width:920,	
		onSelectRow: function(rowid){
			UI_AirportMsg.gridRowOnClick(rowid);
			UI_AirportMsg_LanguagePopup.setLanguageTranslationList(); 
		},
	   	loadComplete: function (e){	   
			if(UI_Common.displayMsgObj.message){
				UI_Common.displayMessage(UI_Common.displayMsgObj);
				UI_Common.displayMsgObj.clearData();
			}
			UI_AirportMsg.clearEditArea();
	   	}
	}).navGrid("#divAirportMsgPager",{refresh: true, edit: false, add: false, del: false, search: false});

	$("#searchDisplayFrom").datepicker({showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', dateFormat: 'dd/mm/yy', buttonImageOnly: true});
	$("#searchDisplayTo").datepicker({showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', dateFormat: 'dd/mm/yy', buttonImageOnly: true});
	$("#searchValidFrom").datepicker({showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', dateFormat: 'dd/mm/yy', buttonImageOnly: true});
	$("#searchValidTo").datepicker({showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', dateFormat: 'dd/mm/yy', buttonImageOnly: true});

	
	$("#msgDisplayFromDate").datepicker({showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', dateFormat: 'dd/mm/yy', buttonImageOnly: true});
	$("#msgDisplaytoDate").datepicker({showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', dateFormat: 'dd/mm/yy', buttonImageOnly: true});
	$("#validFromDate").datepicker({showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', dateFormat: 'dd/mm/yy', buttonImageOnly: true});
	$("#validToDate").datepicker({showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', dateFormat: 'dd/mm/yy', buttonImageOnly: true});

	
	$('#btnSearch').click(function() { UI_AirportMsg.searchData(); });
	$('#btnEdit').click(function() { UI_AirportMsg.editOnClick(); });
	$('#btnAdd').click(function() { UI_AirportMsg.AddOnClick(); });
	$('#btnSave').click(function() { UI_AirportMsg.saveClick(); });
	$('#btnReset').click(function() { UI_AirportMsg.resetonClick(); });
	$('#btnMsgDisplayDes').click(function() { UI_AirportMsg_LanguagePopup.POSenable(); });
	
	
	$("#btnEdit").disableButton();
	$("#btnSave").disableButton();
	$("#btnMsgDisplayDes").disableButton();
	
    UI_Common.initilizeMessage();    
    $("#frmModify").disableForm();
    $('#msgDisplayFromDate').datepicker( "disable" );
	$('#msgDisplaytoDate').datepicker( "disable" );
	$('#validFromDate').datepicker( "disable" );
	$('#validToDate').datepicker( "disable" );   

    $('#divTabbedVisiblity').tabs();
    // language dialog
    UI_AirportMsg_LanguageData.createPopup(" Message Translation ");
    UI_AirportMsg_LanguagePopup.PopUpData(); 

});

/**
 * UI_AirportMsg for airport Msg UI related functionalities
 */
function UI_AirportMsg() {
}


UI_AirportMsg.onAddEditDataPopulate = function (status){
	UI_AirportMsg.addEdit = status;
}


UI_AirportMsg.initilizeVisibility = function (){	
	UI_AirportMsg_flightNumbers.initilize();
	UI_AirportMSG_SalesChannels.initilize();
	UI_AirportMSG_ONDs.initilize ();
	$('#divTabbedVisiblity').tabs('option','active',0);
	
}

UI_AirportMsg.searchData = function (){
	if(UI_AirportMsg.validateSearch()) {		
		var postData = {};
		postData['airportMsgSearchCriteria.airport']= $('#searchAirport').val();	
		postData['airportMsgSearchCriteria.depArrType']= $('#searchDepArrType').val() ;
		postData['airportMsgSearchCriteria.displayFromDate'] = $('#searchDisplayFrom').val();
		postData['airportMsgSearchCriteria.displayToDate'] = $('#searchDisplayTo').val();
		postData['airportMsgSearchCriteria.validFromDate'] = $('#searchValidFrom').val();
		postData['airportMsgSearchCriteria.validToDate'] = $('#searchValidTo').val();
		postData['airportMsgSearchCriteria.messageContent'] = $('#searchMessage').val();
	 		 	
		$("#tblAirportMsg").setGridParam({page:1,postData:postData});
	 	$("#tblAirportMsg").trigger("reloadGrid");
	}
}

UI_AirportMsg.validateSearch = function (){
	return true;
}

UI_AirportMsg.saveClick = function (){	
	
	if(!UI_AirportMsg.validateSave()){
		return false;
	}
	
	var data = {};
	
	var selectedFlightNumbers = UI_AirportMsg_flightNumbers.getMultiSelectData("flightNumbers");	
	var selectedOnds =  UI_AirportMSG_ONDs.getSelectedONDs();
	var selectedSalesChannels = UI_AirportMSG_SalesChannels.getMultiSelectData("salesChannels");	
	var flightNumbersInclusionStatus = UI_AirportMsg_flightNumbers.getInlusionStatus();
	var ondsInclusionStatus = UI_AirportMSG_ONDs.getInlusionStatus();
	
	data['airportMsgTO.flightApplyStatus'] = flightNumbersInclusionStatus;
	data['airportMsgTO.ondApplyStatus'] = ondsInclusionStatus;
	data['airportMsgTO.selectedflights'] = selectedFlightNumbers.join();
	data['airportMsgTO.selectedsalesChannels'] = selectedSalesChannels.join();
	data['airportMsgTO.selectedonds'] = selectedOnds.join();
	
	data['airportMsgTO.airport'] = $('#selMsgAirport').val();
	data['airportMsgTO.depArrType'] = $('#selDepArrType').val();
	data['airportMsgTO.messageType'] = $('#selMsgType').val();
	data['airportMsgTO.displayFrom'] = $('#msgDisplayFromDate').val();
	data['airportMsgTO.dispalyTo'] = $('#msgDisplaytoDate').val();
	data['airportMsgTO.validFrom'] = $('#validFromDate').val();
	data['airportMsgTO.validTo'] = $('#validToDate').val();
	data['airportMsgTO.message'] = $('#txtMessage').val();
	data['airportMsgTO.messageId'] = $('#hdnMessageID').val();
	data['airportMsgTO.version'] = $('#hdnVersion').val();
		
	$.ajax({
		  type: "POST",
		  url: 'airportMessages!addEditMessage.action',
		  data: data,
		  success: UI_AirportMsg.afterSave,
		  dataType: 'json'
		});

}



UI_AirportMsg.validateSave = function (){	
	//var airportType =	$('input:radio[name=selMsgDepArrAirport]:checked').val();
	if($("#selMsgAirport").isFieldEmpty()){	
		UI_Common.showCommonError("Error", megAirportEmpty);			
		return false;
	}
	else if($("#selDepArrType").isFieldEmpty()){	
		UI_Common.showCommonError("Error", msgDepArrTypeEmpty);			
		return false;
	}
	else if($("#selMsgType").isFieldEmpty()){	
		UI_Common.showCommonError("Error", msgTypeEmpty);			
		return false;	

	}else if($("#msgDisplayFromDate").isFieldEmpty()){
		UI_Common.showCommonError("Error", msgDisplayFromDateEmpty);
		return false;
	}else if($("#msgDisplaytoDate").isFieldEmpty()){
		UI_Common.showCommonError("Error", msgDisplayToDateEmpty);
		return false;
	}else if($("#validFromDate").isFieldEmpty()){
		UI_Common.showCommonError("Error", msgValidFromDateEmpty);
		return false;	
	}else if($("#validToDate").isFieldEmpty()){
		UI_Common.showCommonError("Error", msgValidToDateEmpty);
		return false;	
	}else if($("#txtMessage").isFieldEmpty()){
		UI_Common.showCommonError("Error", msgEmpty);			
		return false;
	}
	
	if(!CheckDates($("#msgDisplayFromDate").val(),$("#msgDisplaytoDate").val())){
		UI_Common.showCommonError("Error", msgDisplayFromDateGrater);			
		return false;
	}
	if(!CheckDates($("#validFromDate").val(),$("#validToDate").val())){
		UI_Common.showCommonError("Error", msgValidFromDateGrater);			
		return false;
	}if(!UI_AirportMSG_ONDs.validateInclusion())
	{
		UI_Common.showCommonError("Error", msgSelectOndInclusion);			
		return false;
	}
	if(!UI_AirportMsg_flightNumbers.validateInclusion())
	{
		UI_Common.showCommonError("Error", msgSelectFlightInclusion);			
		return false;
	}
	
	return true;
}

UI_AirportMsg.afterSave = function (response){
	if(response.msgType != 'Error'){
		var postData = {};
		$('#frmMessagesSearch').clearForm();
		$("#frmModify").clearForm();
		$('#searchAirport').val("");
		$('#searchDepArrType').val("");
		postData['airportMsgSearchCriteria.airport']= $('#searchAirport').val();	
		postData['airportMsgSearchCriteria.depArrType']= $('#searchDepArrType').val() ;
		postData['airportMsgSearchCriteria.displayFromDate'] = $('#searchDisplayFrom').val();
		postData['airportMsgSearchCriteria.displayToDate'] = $('#searchDisplayTo').val();
		postData['airportMsgSearchCriteria.validFromDate'] = $('#searchValidFrom').val();
		postData['airportMsgSearchCriteria.validToDate'] = $('#searchValidTo').val();
		postData['airportMsgSearchCriteria.messageContent'] = $('#searchMessage').val();
		
		$("#tblAirportMsg").setGridParam({page:1,postData:postData});
	 	$("#tblAirportMsg").trigger("reloadGrid");
	 	
	 	$("#frmModify").disableForm();
	 	$('#msgDisplayFromDate').datepicker( "disable" );
	 	$('#msgDisplaytoDate').datepicker( "disable" );
	 	$('#validFromDate').datepicker( "disable" );
	 	$('#validToDate').datepicker( "disable" );   
	}	
	UI_Common.displayMessage (response);	
}

UI_AirportMsg.clearEditArea = function (){
	$("#frmModify").clearForm();
	$("#btnEdit").disableButton();
	$("#btnSave").disableButton();	
	$("#btnMsgDisplayDes").disableButton();
	UI_AirportMsg.initilizeVisibility ();
}

UI_AirportMsg.refreshGrid = function (){	
	$("#frmModify").disableForm();
	$('#msgDisplayFromDate').datepicker( "disable" );
	$('#msgDisplaytoDate').datepicker( "disable" );
	$('#validFromDate').datepicker( "disable" );
	$('#validToDate').datepicker( "disable" );  
	UI_AirportMsg.clearEditArea();
	top[2].HideProgress();
	UI_Common.initilizeMessage();	
	$("#tblAirportMsg").trigger("reloadGrid");//This is an  Event
}

UI_AirportMsg.resetonClick = function (){	
	if($("#tblAirportMsg").getGridParam("selrow") != null){
		UI_AirportMsg.gridRowOnClick($("#tblAirportMsg").getGridParam("selrow"));
	} else {
		$("#frmModify").clearForm();
		$('#frmModify').disableForm();
		$("#frmMessagesSearch").clearForm();	
		$('#searchAirport').val("");
		$('#searchDepArrType').val("");
		UI_AirportMsg.initilizeVisibility();
		UI_AirportMsg.enableVisiblity (false);
		$("#tblAirportMsg").setGridParam({page:1,postData:{}});		
		$("#tblAirportMsg").trigger("reloadGrid");
	}	
}

UI_AirportMsg.getSelectedMessage = function (){
	if($("#tblAirportMsg").getGridParam("selrow") != null){
		var selectedRow = $("#tblAirportMsg").getRowData($("#tblAirportMsg").getGridParam("selrow"));
		return selectedRow['message'];
	}else
		return null;	
}

UI_AirportMsg.getSelectedLanguageList = function (){
	if($("#tblAirportMsg").getGridParam("selrow") != null){
		var selectedRow = $("#tblAirportMsg").getRowData($("#tblAirportMsg").getGridParam("selrow"));
		return eval(selectedRow['languageList']);
	}else
		return null;	
}


UI_AirportMsg.editOnClick = function (){
	UI_AirportMsg.onAddEditDataPopulate("EDIT");
	$("#btnSave").enableButton();
	$("#frmModify .frm_editable").enableFormSelective();
	UI_AirportMsg.enableVisiblity (true);
	
	$('#msgDisplayFromDate').datepicker( "enable" );
	$('#msgDisplaytoDate').datepicker( "enable" );
	$('#validFromDate').datepicker( "enable" );
	$('#validToDate').datepicker( "enable" );			
}

UI_AirportMsg.gridRowOnClick = function (rowid){
	UI_AirportMsg.onAddEditDataPopulate("EDIT");
	UI_Common.initilizeMessage();
	UI_AirportMsg.initilizeVisibility();
	$("#tblAirportMsg").GridToForm(rowid,"#frmModify");	
	
	$("#frmModify").disableForm();

	$("#btnEdit").enableButton();
	$("#btnMsgDisplayDes").enableButton();
	UI_AirportMsg.populateVisility(rowid);
	
}

UI_AirportMsg.populateVisility = function (rowid){
	var selectedRow = $("#tblAirportMsg").getRowData(rowid);

	//$('#selMsgDepAirport').val( selectedRow['airportMsgTO.airport'] ) ;
	//$('#selMsgType').val( selectedRow['airportMsgTO.messageType'] ) ;

	if(selectedRow['flightNumbersInclusion'] == 'INCLUDE'){
		$('#radFlight_inc').attr('checked',true);
		$('#radFlight_exc').attr('checked',false);
	}else if(selectedRow['flightNumbersInclusion'] == 'EXCLUDE'){
		$('#radFlight_inc').attr('checked',false);
		$('#radFlight_exc').attr('checked',true);
	}
	if(selectedRow['ONDsInclusion'] == 'INCLUDE'){
		$('#radOND_inc').attr('checked',true);
		$('#radOND_Exc').attr('checked',false);
	}else if(selectedRow['ONDsInclusion'] == 'EXCLUDE') {
		$('#radOND_inc').attr('checked',false);
		$('#radOND_Exc').attr('checked',true);
	}	
	UI_AirportMsg_flightNumbers.setSelectedItems(eval(selectedRow['flightNumbers']));
	UI_AirportMSG_SalesChannels.setSelectedItems(eval(selectedRow['salesChannels']));
	UI_AirportMSG_ONDs.setSelectedONDOptions(eval(selectedRow['ONDs']));
	
}

UI_AirportMsg.enableVisiblity = function (blnStatus){
	UI_AirportMsg_flightNumbers.enableMultiSelect(blnStatus);	
	UI_AirportMSG_SalesChannels.enableMultiSelect(blnStatus);
	UI_AirportMSG_ONDs.enableOndSelection(blnStatus);
}

UI_AirportMsg.AddOnClick = function (){
	UI_AirportMsg.onAddEditDataPopulate("ADD");
	UI_AirportMsg.clearEditArea();
	$("#btnSave").enableButton();
	$("#frmModify").enableForm();
	$('#msgDisplayFromDate').datepicker( "enable" );
	$('#msgDisplaytoDate').datepicker( "enable" );
	$('#validFromDate').datepicker( "enable" );
	$('#validToDate').datepicker( "enable" );
	
	$("#tblAirportMsg").resetSelection();
	UI_AirportMsg.enableVisiblity (true);
}

UI_AirportMsg.kpValidateContentDynamic = function(control){
	var strCC = $('#'+control).val();
	var strLen = strCC.length;
	var blnVal = UI_AirportMsg.isValidateContentDynamic(strCC);
	if (!blnVal) {
//		var startIndex = strCC.indexOf(strCC);
		$('#'+control).val(strCC.substr(0, strLen - 1));
		$('#'+control).focus();
	}
}

UI_AirportMsg.isValidateContentDynamic = function(s){return RegExp("^[a-zA-Z0-9_.',\& \w\s]+$").test(s);}


