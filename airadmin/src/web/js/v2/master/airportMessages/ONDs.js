
/**
 * Onload function
 */
$(function() {

	$('#btnAddSeg').decorateButton();
	$('#btnRemSeg').decorateButton();

	$('#btnAddSeg').click(function() {
		UI_AirportMSG_ONDs.addONDOption();
	});
	$('#btnRemSeg').click(function() {
		UI_AirportMSG_ONDs.removeONDOption();
	});

});

/**
 * UI_AirportONDs for airport Msg UI related functionalities
 */
function UI_AirportMSG_ONDs() {
}

UI_AirportMSG_ONDs.enableOndSelection = function (blnStatus){
	$('#divONDs :input').attr('disabled',!blnStatus);	
}

UI_AirportMSG_ONDs.getInlusionStatus = function (){
	 return $('input:radio[name=radOND_inc]:checked').val();
}

UI_AirportMSG_ONDs.addONDOption = function() {
	var isContained = false;

	var dept = $("#selDepature option:selected").text();
	var arr = $("#selArrival option:selected").text();
	var via1 = $("#selVia1 option:selected").text();
	var via2 = $("#selVia2 option:selected").text();
	var via3 = $("#selVia3 option:selected").text();
	var via4 = $("#selVia4 option:selected").text();

//	var deptVal = $("select#selDepature").val();
//	var arrVal = $("select#selArrival").val();
//	var via1Val = $("select#selVia1").val();
//	var via2Val = $("select#selVia2").val();
//	var via3Val = $("select#selVia3").val();
//	var via4Val = $("select#selVia4").val();
//	
//	var dept = $("select#selDepature").val();
//	var arr  = $("select#selArrival").val();
//	var via1 = $("select#selVia1").val();
//	var via2 = $("select#selVia2").val();
//	var via3 = $("select#selVia3").val();
//	var via4 = $("select#selVia4").val();

	if ($.trim(dept).length == 0) {
		UI_Common.showCommonError("Error", ondDeparturereq);
		$('#selDepature').focus();
		return false;
	}else if ($.trim(arr).length == 0) {
		UI_Common.showCommonError("Error", ondArrivalReq);
		$('#selArrival').focus();
		return false;
	}else if (dept == arr) {
		UI_Common.showCommonError("Error", ondArrDepSame);
		$('#selArrival').focus();
		return false;
	}else if (arr == via1 || arr == via2 || arr == via3 || arr == via4) {
		UI_Common.showCommonError("Error", ondArrViaSame);
		$('#selArrival').focus();
		return false;
	} else if (dept == via1 || dept == via2 || dept == via3 || dept == via4) {
		UI_Common.showCommonError("Error", ondDepViaSame);
		$('#selDepature').focus();
		return false;
	} else if ((via2 != "" || via3 != "" || via4 != "") && via1 == "") {
		UI_Common.showCommonError("Error", ondViaSequnce);
		$('#selVia1').focus();
		return false;
	}else if ((via3 != "" || via4 != "") && via2 == "") {
		UI_Common.showCommonError("Error", ondViaSequnce);
		$('#selVia1').focus();
		return false;

	} else if ((via4 != "") && via3 == "") {
		UI_Common.showCommonError("Error", ondViaSequnce);
		$('#selVia1').focus();
		return false;

	} else if ((via1 != "" && via2 != "" && via3 != "")
			&& (via1 == via2 || via1 == via3 || via1 == via4)) {
		UI_Common.showCommonError("Error", ondVispointsEqual);
		$('#selVia1').focus();
		return false;

	} else if ((via1 != "" && via2 != "" && via3 != "")
			&& (via2 == via3 || via2 == via4)) {
		UI_Common.showCommonError("Error", ondVispointsEqual);
		$('#selVia2').focus();
		return false;

	} else if ((via1 != "" && via2 != "" && via3 != "") && (via3 == via4)) {
		UI_Common.showCommonError("Error", ondVispointsEqual);
		$('#selVia4').focus();
		return false;

	} else if ((via1 != "" && via2 != "") && via1 == via2) {
		UI_Common.showCommonError("Error", ondVispointsEqual);
		$('#selVia1').focus();
		return false;

	} else {
		var str = "";
		str += dept;

		if (via1 != "") {
			str += "/" + via1;
		}
		if (via2 != "") {
			str += "/" + via2;
		}
		if (via3 != "") {
			str += "/" + via3;
		}
		if (via4 != "") {
			str += "/" + via4;
		}
		str += "/" + arr;

		$("#selSelected option").each(function() {
			if (this.text == str) {
				isContained = true;
				UI_Common.showCommonError("Error", ondExists);
				return false;
			}
		});

		if (isContained == false) {
			var strReplace = str; 
			if (str.indexOf("All") != -1) {
				strReplace = str.replace("All", "***");
			}	
			$('#selSelected').append(new Option(str,strReplace ));
			return true;
		}
	}
}

UI_AirportMSG_ONDs.setSelectedONDOptions = function(selectedONDs) {
	for ( var i = 0; i < selectedONDs.length; i++) {
		var option = selectedONDs[i];
		var strReplace = option.text; 
		if (strReplace.indexOf("***") != -1) {
			strReplace = strReplace.replace("***", "All");
		}
		$('#selSelected').append(new Option(strReplace, option.id));
	}
}

UI_AirportMSG_ONDs.getSelectedONDs = function() {
	var selectedOnds = new Array();
	var i = 0;
	$("#selSelected option").each(function() {
		var val = $(this).attr("value");
		selectedOnds[i] = val;
		i++;
	});
	return selectedOnds;
}

UI_AirportMSG_ONDs.removeONDOption = function() {
	$('#selSelected :selected').remove();
}

UI_AirportMSG_ONDs.initilize = function() {
	//$("#selSelected").children().remove();
	$('#selSelected option').each(function(i, option){ $(option).remove(); });
}

UI_AirportMSG_ONDs.validateInclusion = function (){
	if(UI_AirportMSG_ONDs.getSelectedONDs() != ""){
		if ( $('.radOND_inc').is(':checked') ) {
			return true;
		}
		return false;
	}else {
		return true;
	}	
}
