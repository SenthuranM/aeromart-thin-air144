/**
 * Airport Messages Management sales Channels JS
 * 
 */
UI_AirportMSG_SalesChannels.multiSelect = null;
/**
 * Onload function
 */
$( function() {
	UI_AirportMSG_SalesChannels.initilize();
});

/**
 * UI_DashboardMsg for Dashboard Msg UI related functionalities
 */
function UI_AirportMSG_SalesChannels() {
}
UI_AirportMSG_SalesChannels.getMultiSelectData = function (name){
	if(UI_AirportMSG_SalesChannels.multiSelect == null) return "";
	return UI_AirportMSG_SalesChannels.multiSelect.getSelectedItemsAsArray();
}

UI_AirportMSG_SalesChannels.setSelectedItems = function (objSelectedItems){
	if(UI_AirportMSG_SalesChannels.multiSelect == null) UI_AirportMSG_SalesChannels.setMultiSelect(allSalesChannels,objSelectedItems);
	UI_AirportMSG_SalesChannels.multiSelect.setSelectedItems(objSelectedItems);
}

UI_AirportMSG_SalesChannels.initilize = function (){
	UI_AirportMSG_SalesChannels.setMultiSelect(allSalesChannels,null);
}

UI_AirportMSG_SalesChannels.enableMultiSelect = function (blnStatus){
	$('#divsalesChannels :input').attr('disabled',!blnStatus);
	if(blnStatus){
		UI_AirportMSG_SalesChannels.multiSelect.enable();
	}else {
		UI_AirportMSG_SalesChannels.multiSelect.disable();
	}
}

UI_AirportMSG_SalesChannels.setMultiSelect = function (allOptions, selectedOptions){
	$('#salesChannelsMulti').empty();
	var objConfig = {allOptions:allOptions,
			 group:false,
			 optionValue:"id",
			 optionText:"text",
			 container:"salesChannelsMulti",
			 titleAllItems:"Sales Channels",
			 titleSelectedItems:"Selected Sales Channels",
			 boxWidth:UI_AirportMsg.sizeOptions.width,
			 boxHeight:UI_AirportMsg.sizeOptions.height}; 

	UI_AirportMSG_SalesChannels.multiSelect  = new RichSelector(objConfig); 
	UI_AirportMSG_SalesChannels.multiSelect.setSelectedItems(selectedOptions);
}