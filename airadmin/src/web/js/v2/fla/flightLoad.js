/*
 *********************************************************
	Description		: Flight Load dash-board screen for Demo
	Author			: Rilwan A. Latiff
	Version			: 1.0
	Created on		: 15th October 2009
	Edited by 		: Harsha Udayapriya
	Edited on		: 01st June 2010
	NOTE			: This code write only for the demo purpose 
					  DONT use it if you are palling to integrate with the back-end
 *********************************************************	
 */

var screenId = "SC_FLGT_002";
var objWindow1;
var intLastLoaded = -1;
var intLastRow = -1;
var intLastCol = -1;
var strLastColor = "";
var barsVisualization;
var flaStartDate;

var intSetStartNo = 1;
var intTotRecords = 25;
var intRecPerPage = 25;
var strDateStart = "";
var strDateCurr = "";
var strDateEnd = "";
var blnLastDetailsStatus = false;

var arrData = new Array();
var arrDates = new Array();
var arrDataOptimizeL = new Array();
var arrDataOptimizeR = new Array();
var arrDataAlert = new Array();

var jsCntrl = [ {
	id : "selFrom",
	desc : "From Location",
	required : false
}, {
	id : "selTo",
	desc : "To Location",
	required : false
}, {
	id : "txtFDate",
	desc : "From Date",
	required : true
}, {
	id : "txtTDate",
	desc : "To Date",
	required : true
} ];
/**
 * TODO
 * to keep top level search data
 */
var jsSearchVal = [ {
	orign : "",
	dest : "",
	fromDate : "",
	toDate : "",
	startDate : "",
	recNo : "",
	intLen : "",
	totRec : ""
} ];

/**
 * TODO
 * to keep clicked flight detail in table
 */
var jsClickedFlight = [ {
	flightID : "",
	departureDate : "",
	segCode : "",
	isFlown:"",
	flightNumber: ""
} ];

var objCal = new Calendar();
objCal.onClick = "setDate";
objCal.buildCalendar();

var objRecNav = new recNavigate();
objRecNav.divID = "spnRec";
objRecNav.numStart = intSetStartNo;
objRecNav.numRecTotal = intTotRecords;
objRecNav.numRecPage = intRecPerPage;
objRecNav.onClick = "gridNavigations";
objRecNav.writeNavigate();



// Alert Grid
var objColA1 = new DGColumn();
objColA1.columnType = "label";
objColA1.width = "8%";
objColA1.arrayIndex = 1;
objColA1.headerText = "Alert Type";
objColA1.itemAlign = "center";

var objColA2 = new DGColumn();
objColA2.columnType = "label";
objColA2.width = "8%";
objColA2.arrayIndex = 2;
objColA2.headerText = "Reference";
objColA2.itemAlign = "center";

var objColA3 = new DGColumn();
objColA3.columnType = "label";
objColA3.width = "12%";
objColA3.arrayIndex = 3;
objColA3.headerText = "Contact person";
objColA3.itemAlign = "center";

var objColA4 = new DGColumn();
objColA4.columnType = "label";
objColA4.width = "8%";
objColA4.arrayIndex = 4;
objColA4.headerText = "Sales Channel";
objColA4.itemAlign = "center";

var objColA5 = new DGColumn();
objColA5.columnType = "label";
objColA5.width = "11%";
objColA5.arrayIndex = 5;
objColA5.headerText = "CRM Note";
objColA5.itemAlign = "center";

var objColA6 = new DGColumn();
objColA6.columnType = "label";
objColA6.width = "4%";
objColA6.arrayIndex = 6;
objColA6.headerText = "BC";
objColA6.itemAlign = "center";

var objColA7 = new DGColumn();
objColA7.columnType = "label";
objColA7.width = "8%";
objColA7.arrayIndex = 7;
objColA7.headerText = "Bkg. Date GMT";
objColA7.itemAlign = "center";

var objColA8 = new DGColumn();
objColA8.columnType = "label";
objColA8.width = "7%";
objColA8.arrayIndex = 8;
objColA8.headerText = "Sys. Remarks";
objColA8.itemAlign = "center";

var objColA9 = new DGColumn();
objColA9.columnType = "label";
objColA9.width = "10%";
objColA9.arrayIndex = 9;
objColA9.headerText = "Action";
objColA9.itemAlign = "center";

var objColA10 = new DGColumn();
objColA10.columnType = "label";
objColA10.width = "10%";
objColA10.headerText = "Email";
objColA10.arrayIndex = 10;
objColA10.itemAlign = "center";

var objColA11 = new DGColumn();
objColA11.columnType = "label";
objColA11.width = "10%";
objColA11.headerText = "Print";
objColA11.arrayIndex = 11;
objColA11.itemAlign = "center";

var objDGA = new DataGrid("spnGridAlert");
objDGA.addColumn(objColA1);
objDGA.addColumn(objColA2);
objDGA.addColumn(objColA3);
objDGA.addColumn(objColA4);
objDGA.addColumn(objColA5);
objDGA.addColumn(objColA6);
objDGA.addColumn(objColA7);
objDGA.addColumn(objColA8);
objDGA.addColumn(objColA9);
objDGA.addColumn(objColA10);
objDGA.addColumn(objColA11);

objDGA.arrGridData = arrDataAlert;
objDGA.paging = false;
objDGA.seqNoWidth = "3%";
objDGA.height = "170px";

objDGA.displayGrid();

/*
 * Page Button Click
 */
function pgButtonClick(intIndex) {
	switch (intIndex) {
	case 0:
		if (validate()) {
			showSearchResults();
		}
		break;
	case 1:
		top[1].objTMenu.tabRemove(screenId)
		break;
	case 2:
		initialize();
		break;
	}
}

/*
 * Page Validations
 */
function validate() {
	initializeMessage();

	var intLen = jsCntrl.length;
	var i = 0;
	do {
		if (jsCntrl[i].required) {
			if (getValue(jsCntrl[i].id) == "") {
				showERRMessage(jsCntrl[i].desc + " cannot be blank.");
				setFocus(jsCntrl[i].id)
				return false;
			}
		}
		i++;
	} while (--intLen);

	if(getValue(jsCntrl[0].id) == "" && getValue(jsCntrl[1].id) == "" ) {
		showERRMessage("Select either from or to location");
		setFocus(jsCntrl[0].id)
		return false;
	}

	if (getValue(jsCntrl[0].id) == getValue(jsCntrl[1].id)) {
		showERRMessage("From & To Locations cannot be same");
		setFocus(jsCntrl[3].id)
		return false;
	}

	if (!dateChk(getValue(jsCntrl[2].id))) {
		showERRMessage(jsCntrl[2].desc + " invalid ");
		setFocus(jsCntrl[2].id);
		return false;
	}
		
	if (!dateChk(getValue(jsCntrl[3].id))) {
		showERRMessage(jsCntrl[3].desc + " invalid ");
		setFocus(jsCntrl[3].id);
		return false;
	}
	
	if (!CheckDates(getValue(jsCntrl[2].id), getValue(jsCntrl[3].id))) {
		showERRMessage(jsCntrl[2].desc + " should be less than "
				+ jsCntrl[3].desc);
		setFocus(jsCntrl[3].id)
		return false;
	}
	if (!CheckDates(flaStartDate, getValue(jsCntrl[2].id))) {
		showERRMessage(jsCntrl[2].desc + " should be greater than "
				+ flaStartDate);
		setFocus(jsCntrl[3].id)
		setDisplay("spnSrchResuls", false);
		return false;
	}

	clearERRMessage();
	
	return true;
}

/*
 * Calendar Click
 */
function setDate(strDate, strID) {
	switch (strID) {
	case "0":
		setField(jsCntrl[2].id, strDate);
		break;
	case "1":
		setField(jsCntrl[3].id, strDate);
		break;
	}
}

/*
 * Load Calendar
 */
function loadCalendar(strID, objEvent) {
	objCal.ID = strID;
	objCal.onClick = "setDate";
	objCal.showCalendar(objEvent);
}

/*
 * Column button click in the grid
 */
function clickGridEmail() {
	showPopUp(1);
}

/*
 * Date On Blur
 */
function dateChange(strID) {
	switch (strID) {
	case 0:
		dateOnBlur(jsCntrl[2].id, strID);
		break;
	case 1:
		dateOnBlur(jsCntrl[3].id, strID);
		break;
	}
}

/*
 * Date on Blur
 */
function dateOnBlur(strControl, intId) {
	if (!dateChk(strControl)) {
		showERRMessage("Invalid date");
	} 
}

function generateGrid(strStartDate, arrRetData, totRec, intLen, recNo) {
	arrDates = new Array();
	for ( var i = 0; i < 7; i++) {
		arrDates[i] = getNextDate(strStartDate, i);
	}

	var strHTMLText = "";
	strHTMLText += "<table width='904px' border='0' cellpadding='1' cellspacing='0'>"
	strHTMLText += "	<tr>";
	strHTMLText += "		<td rowspan='2' width='26px' class='GridRow GridHeader GridHeaderBand GridHDBorder'><font>&nbsp;<\/font><\/td>";
	strHTMLText += "		<td rowspan='2' width='61px' class='GridRow GridHeader GridHeaderBand GridHDBorder' align='center'><font>Flight No. <\/font><\/td>";
	strHTMLText += "		<td rowspan='2' width='53px' class='GridRow GridHeader GridHeaderBand GridHDBorder' align='center'><font>From<\/font><\/td>";
	strHTMLText += "		<td rowspan='2' width='53px' class='GridRow GridHeader GridHeaderBand GridHDBorder' align='center'><font>To<\/font><\/td>";
	//strHTMLText += "		<td rowspan='2' width='64px' class='GridRow GridHeader GridHeaderBand GridHDBorder' align='center'><font>Status<\/font><\/td>";
	strHTMLText += "		<td colspan='7' class='GridRow GridHeader GridHeaderBand GridHDBorder' align='center'>"
	strHTMLText += "			<table width='98%' border='0' cellpadding='0' cellspacing='0' align='center'>"
	strHTMLText += "				<tr>"
	strHTMLText += "					<td style='width:1%'><a href='#' onclick='nextPreivousClick(0); return false;'><img src='../../images/M005_no_cache.gif' alt='Previous' border='0'><\/a><\/td>";
	strHTMLText += "					<td style='width:1%'><font>&nbsp;<\/font><\/td>"
	strHTMLText += "					<td style='width:1%'><a href='#' onclick='nextPreivousClick(0); return false;'><font>Previous<\/font><\/a><\/td>"
	strHTMLText += "					<td style='width:94%;' align='center'><font>Dates<\/font><\/td>"
	strHTMLText += "					<td style='width:1%' align='right'><a href='#' onclick='nextPreivousClick(1); return false;'><font>Next<\/font><\/a><\/td>"
	strHTMLText += "					<td style='width:1%'><font>&nbsp;<\/font><\/td>"
	strHTMLText += "					<td style='width:1%' align='right'><a href='#' onclick='nextPreivousClick(1); return false;'><img src='../../images/M006_no_cache.gif' alt='Next' border='0'><\/a><\/td>";
	strHTMLText += "				<\/tr>";
	strHTMLText += "			<\/table>";
	strHTMLText += "		<\/td>";
	strHTMLText += "	<\/tr>";
	strHTMLText += "	<tr>";
	strHTMLText += "		<td width='92px' class='GridRow GridHeader GridHeaderBand GridHDBorder' align='center'><font>"
			+ getDateDesc(arrDates[0]) + "<\/font><\/td>";
	strHTMLText += "		<td width='92px' class='GridRow GridHeader GridHeaderBand GridHDBorder' align='center'><font>"
			+ getDateDesc(arrDates[1]) + "<\/font><\/td>";
	strHTMLText += "		<td width='91px' class='GridRow GridHeader GridHeaderBand GridHDBorder' align='center'><font>"
			+ getDateDesc(arrDates[2]) + "<\/font><\/td>";
	strHTMLText += "		<td width='91px' class='GridRow GridHeader GridHeaderBand GridHDBorder' align='center'><font>"
			+ getDateDesc(arrDates[3]) + "<\/font><\/td>";
	strHTMLText += "		<td width='91px' class='GridRow GridHeader GridHeaderBand GridHDBorder' align='center'><font>"
			+ getDateDesc(arrDates[4]) + "<\/font><\/td>";
	strHTMLText += "		<td width='91px' class='GridRow GridHeader GridHeaderBand GridHDBorder' align='center'><font>"
			+ getDateDesc(arrDates[5]) + "<\/font><\/td>";
	strHTMLText += "		<td width='91px' class='GridRow GridHeader GridHeaderBand GridHDBorder' align='center'><font>"
			+ getDateDesc(arrDates[6]) + "<\/font><\/td>";
	strHTMLText += "	<\/tr>";
	strHTMLText += "<\/table>";
	DivWrite("spnHD", strHTMLText);

	// Details
	strHTMLText = "<table id='tblData' width='100%' border='0' cellpadding='1' cellspacing='0'>"
	strHTMLText += "	<tbody><\/tbody> ";
	strHTMLText += "<\/table>";

	DivWrite("spnDT", strHTMLText);

	var objTbl = getFieldByID("tblData").tBodies[0];

	var tblRow = null;
	var intLen = intLen;
	var intRecNo = recNo;
	if(intRecNo == -1)
		intRecNo = 0;
	var i = 0;
	var strClassRow = "GridItemRow GridItem rowHeight";
	var strClass = "";
	if(arrData != null){
		do {
			
			if(arrData[i][1] == '' || arrData[i][1] == 'null'){
				i++;
				continue;
			}
			
			tblRow = document.createElement("TR");
			strClass = (i % 2) != 0 ? "GridAlternateRow" : "GridItem";
	
			tblRow.appendChild(createCell( {
				desc : intRecNo + 1,
				id : "td_" + i + "_0",
				align : "center",
				width : "3%",
				css : "GridRow GridHeader GridHeaderBand GridHDBorder"
			}));
			tblRow.appendChild(createCell( {
				desc : arrData[i][1],
				id : "td_" + i + "_1",
				align : "center",
				width : "7%",
				click : tdOnClick,
				css : strClassRow + " " + strClass
			}));
			tblRow.appendChild(createCell( {
				desc : arrData[i][2],
				id : "td_" + i + "_2",
				align : "center",
				width : "6%",
				click : tdOnClick,
				css : strClassRow + " " + strClass
			}));
			tblRow.appendChild(createCell( {
				desc : arrData[i][3],
				id : "td_" + i + "_3",
				align : "center",
				width : "6%",
				click : tdOnClick,
				css : strClassRow + " " + strClass
			}));
			//tblRow.appendChild(createCell( {
			//	desc : arrData[i][4],
			//	id : "td_" + i + "_4",
			//	align : "center",
			//	width : "7%",
			//	click : tdOnClick,
			//	css : strClassRow + " " + strClass
			//}));
	
			var strData = "";
			for ( var x = 0; x < 7; x++) {
				strData = "";
	
				if (arrData[i][5] != null) {
					if (typeof (arrData[i][5][arrDates[x]]) == "object") {//TODO check date match
						var arrColors = new Array("", "black");
						arrColors = getBGcolor(arrData[i][5][arrDates[x]][1]);
						if(arrData[i][5][arrDates[x]][1]!= null){
						strData += "<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
						strData += "	<tr>";
						strData += "		<td id='td_blk_"
								+ i
								+ "_"
								+ x
								+ "' align='center' class='blockRow cursorPointer' style='background-color:"
								+ arrColors[0] + ";' onclick='tdBlkClick(" + i
								+ "," + x + ",\"" + arrColors[0] + "\"" + ",\""
								+ arrDates[x] + "\");' onmouseover='showFlightDetailTT(" + i+ "," + x + ",\"" + arrDates[x] + "\", event)' onmouseout='nd(false)'><font style='color:"
								+ arrColors[1] + "'>"
								+ arrData[i][5][arrDates[x]][0] + " &nbsp; "
								+ arrData[i][5][arrDates[x]][2] + "<\/font><\/td>";
						strData += "	<\/tr>";
						strData += "<\/table>";
						}
					}
				}
	
				tblRow.appendChild(createCell( {
					desc : strData,
					id : "td_" + i + "_" + (x + 5),
					align : "center",
					width : "10%",
					click : tdOnClick,
					css : strClassRow + " " + strClass
				}));
			}
			objTbl.appendChild(tblRow);
			i++;
			intRecNo++;
		} while (--intLen);
	}
	if (jsSearchVal.recNo != 0) {
		if(jsSearchVal.recNo < 0){
			objRecNav.numStart = 1;
		}else{
			if(totRec < recNo){
				objRecNav.numStart = recNo;
			}else{
				objRecNav.numStart = jsSearchVal.recNo + 1;
			}
		}
	} else {
		objRecNav.numStart = 1;
	}
	
	objRecNav.numRecTotal = totRec;
	objRecNav.numRecPage = intRecPerPage;
	objRecNav.onClick = "gridNavigations";
	objRecNav.refreshNavigate();

	function getNextDate(strDate, intDay) {
		var strReturn = "";
		strReturn = DateToString(addDays(StringToDate(strDate), intDay));
		if (!CheckDates(strReturn, strDateEnd)) {
			strReturn = "<font style='visibility:hidden;'>28/12/0000<\/font>";
		}
		return strReturn;
	}

	function getDateDesc(strDate) {
		/*
		var strReturnDate = "";
		if (strDate != ""){
			strReturnDate = dateChk(strDate, "DD/MMM/YYYY");
		}
		 */
		return strDate
	}

	function getBGcolor(strStatus) {
		var strBgColor = "";
		var strTxtColor = "black";
		var blnColorPicked = false;
		var length = jsFlghtPerformance.length;
		for ( var i = 0; i < length; i++) {
			if(strStatus != ""){
				if (jsFlghtPerformance[i].per <= strStatus && !blnColorPicked) {
					strBgColor = "#" + jsFlghtPerformance[i].bg;
					strTxtColor = "#" + jsFlghtPerformance[i].fnt;
					blnColorPicked = true;
				}
			}
		}

		return new Array(strBgColor, strTxtColor);
	}
}

/*
 * Next Previous Click
 */
function nextPreivousClick(intNP) {
	var strDate = strDateCurr;
	switch (intNP) {
	case 0:
		strDate = DateToString(addDays(StringToDate(strDateCurr), -7));
		if (!CheckDates(strDateStart, strDate)) {
			strDate = strDateStart;
		}

		break;
	case 1:
		strDate = DateToString(addDays(StringToDate(strDateCurr), 7));
		if (!CheckDates(strDate, strDateEnd)) {
			strDate = strDateEnd;
		}
		break;
	}
	if (strDateCurr == strDate) {
		return false;
	}

	showHideDetails(false);
	UI_FLA.stopBlinkCurrentSelected();
	strDateCurr = strDate;

	var url = "searchFlightLoad!searchFlights.action";
	var data = {};
	data['strOrg'] = jsSearchVal.orign;
	data['strDest'] = jsSearchVal.dest;
	data['strDateStart'] = strDateCurr;
	data['strDateEnd'] = jsSearchVal.todate;
	data['intLen'] = jsSearchVal.intLen;
	data['recNo'] = jsSearchVal.recNo;
	data['totRec'] = jsSearchVal.totRec;
	UI_FLA.ajaxFlightSearch(url, data);
}

/*
 * TODO
 */
function tdOnClick() {
}

function showDetailsClick() {
	if (blnLastDetailsStatus) {
		showHideDetails(false);
	} else {
		if (intLastRow != -1) {
			showHideDetails(true);
		} else {
			showERRMessage("Please Select a flight and a Date");
		}
	}
}

/**
 * <p></p>
 * @param intRow
 * @param intCol
 * @param strColor
 * @return
 */
function tdBlkClick(intRow, intCol, strColor, depDate) {
	if (intLastRow != -1){
		getFieldByID("td_blk_" + intLastRow + "_" + intLastCol).style.backgroundColor = strLastColor;
	}
	getFieldByID("td_blk_" + intRow + "_" + intCol).style.backgroundColor = "gray";

	intLastRow = intRow, intLastCol = intCol;
	strLastColor = strColor;
	tblDetailClick(0);

	DivWrite("spnSltdInfo", "<font><b>" + arrData[intLastRow][1] + " - "
			+ arrData[intLastRow][2] + "/" + arrData[intLastRow][3] + " - "
			+ arrDates[intLastCol] + "<b><\/font>");

	// ajax submit for search inventory
	var url = "searchMIAllocation.action";
	var data = {};
	if (arrData[intLastRow][7][depDate][0] != "") {
		data['flightID'] = arrData[intLastRow][7][depDate][0];
		data['departureDate'] = depDate;
		jsClickedFlight.flightID = arrData[intLastRow][7][depDate][0];
		jsClickedFlight.departureDate = depDate;
		var additionalDet = UI_FLA.addtionalDataMap[jsClickedFlight.flightID];
		jsClickedFlight.isFlown = additionalDet.flown;
		jsClickedFlight.flightNumber = arrData[intLastRow][1];
		UI_FLA.ajaxInventorySearch(url, data);
	} else {
		showERRMessage("Please select valid flight.");
	}

}

function showFlightDetailTT(intRow, intCol, depDate, objE){
	try{
		if (arrData[intRow][7][depDate][0] != "") {
			var cosWiseCnt = "Y";
			var depTime = "00:00:00";
			var cnfOhdCnt = 23;
			var infCnt = 12;
			//UI_FLA.addtionalDataMap[arrData[intRow][7][depDate][0]] set dep time and put a for loop
			var flightData = UI_FLA.addtionalDataMap[arrData[intRow][7][depDate][0]];
			//var length = flightData.....length;
			var strHTMLText = "<table width='100%' border='1' cellpadding='1' cellspacing='0'>";
			strHTMLText += "	<tr><td colspan='4'><font>Departure Time : "+ flightData.departDate +"<\/font></td><\/tr>";
			strHTMLText += "	<tr><td colspan='4'><font>Status : "+ flightData.status +"<\/font></td><\/tr>";
			strHTMLText += "	<tr><td><font><b>COS</b><\/font><\/td>";
			strHTMLText += "		<td><font><b>CNF</b><\/font><\/td>";
			strHTMLText += "		<td><font><b>OHD</b><\/font><\/td>";
			//strHTMLText += "		<td><font><b>INF</b><\/font><\/td>";
			strHTMLText += "	<\/tr>";
			for(var key in flightData.cosWiseData){
				var breakDwn = flightData.cosWiseData[key];
				strHTMLText += "	<tr><td><font>" + breakDwn.cosClass + "<\/font><\/td>";
				strHTMLText += "		<td><font>" + breakDwn.cnfCnt + "/"	+ breakDwn.infCnfCnt + "<\/font><\/td>";
				strHTMLText += "		<td><font>" + breakDwn.ohdCnt + "/"	+ breakDwn.infOhdCnt + "<\/font><\/td>";
				//strHTMLText += " <td><font>"+ breakDwn.infCnt +"<\/font><\/td>";
				strHTMLText += "	<\/tr>";
			}
			strHTMLText += "<\/table>";
			if(intCol==6){
				return	overlib(strHTMLText, BGCOLOR,'#AFAFAF',FGCOLOR, '#DFDFDF', LEFT, 'bellow', WIDTH, 175, BORDER, 1);
			}else{
				return	overlib(strHTMLText, BGCOLOR,'#AFAFAF',FGCOLOR, '#DFDFDF', CENTER, 'bellow', WIDTH, 175, BORDER, 1);
			}
			//showTT(strHTMLText, "top", "left", objE, -320, 20, 300);
		}
	}catch(ex){}
}

UI_FLA.closePopup = function (){
	$('#spnResSummary').remove();
}


function showHideDetails(blnStatus) {
	initializeMessage();
	if (blnStatus) {
		getFieldByID("tdDetails").style.height = "150px";
		getFieldByID("spnDT").style.height = "150px";
		getFieldByID("spnDetails").style.top = "310px";
		setDisplay("spnDetailInfoTab", true);
		setDisplay("spnAnalyzeInfo", true);
		setDisplay("spnOptimizeInfo", false);
		setDisplay("spnFareReqInfo", false);
		setDisplay("spnAlertInfo", false);

		DivWrite("spnDetailInfoDesc", "Hide Details");

	} else {
		getFieldByID("tdDetails").style.height = "400px";
		getFieldByID("spnDT").style.height = "400px";
		getFieldByID("spnDetails").style.top = "580px";
		setDisplay("spnAnalyzeInfo", false);
		setDisplay("spnOptimizeInfo", false);
		setDisplay("spnDetailInfoTab", false);
		setDisplay("spnFareReqInfo", false);
		setDisplay("spnAlertInfo", false);

		setVisible("spnGridFareReq", false);
		setVisible("spnGridAlert", false);

		DivWrite("spnDetailInfoDesc", "Show Details");
	}
	if(!DATA_MIS.initialParams.flaOptimaization ==1){
		setDisplay("tdTab2", false);
	}
//	if(!DATA_MIS.initialParams.flaAnalyze !=1){
//		setDisplay("tdTab1", false);
//	}
	if(!DATA_MIS.initialParams.flaAlerts ==1){
		setDisplay("tdTab4", false);
	}
	blnLastDetailsStatus = blnStatus;
	UI_FLA.closePopup();
	UI_EmailSender.distroy();
}

function generateDetailInfo() {

	var strHTMLText = "";
	strHTMLText += "<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
	strHTMLText += "	<tr>";
	strHTMLText += "		<td width='60%' valign='top'><span id='spnAnaBreak' style='position:absolute;overflow-X:hidden; overflow-Y:scroll;height:220px;width:520px;'><\/td>";
	strHTMLText += "		<td width='40%'>";
	strHTMLText += "			<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
	strHTMLText += "				<tr>";
	strHTMLText += "					<td><font><b>Breakdown</b><\/font><\/td>";
	strHTMLText += "				<\/tr>";
	//strHTMLText += "				<tr>";
	//strHTMLText += "					<td><font>Filer by : "
	//strHTMLText += "						TA <input type='checkbox' id='chk1' name='chk1' checked>";
	//strHTMLText += "						WEB <input type='checkbox' id='chk2' name='chk2' checked>";
	//strHTMLText += "						CTO <input type='checkbox' id='chk3' name='chk3' checked>";
	//strHTMLText += "						C.H. <input type='checkbox' id='chk4' name='chk4' checked>";
	//strHTMLText += "						GDS <input type='checkbox' id='chk5' name='chk5' checked>";
	//strHTMLText += "						INT'L <input type='checkbox' id='chk6' name='chk6'>";
	//strHTMLText += "						OTR <input type='checkbox' id='chk7' name='chk7'>";
	//strHTMLText += "					<\/font><\/td>";
	//strHTMLText += "				<\/tr>";
	strHTMLText += "				<tr>";
	strHTMLText += "					<td colspan='6'><div id='visualization'></div><\/td>";//put adiv tag and append google url up
	strHTMLText += "				<\/tr>";
	strHTMLText += "				<tr>";
	strHTMLText += "					<td align='right' style='height:1px;'>"
	strHTMLText += "				<\/tr>";
	strHTMLText += "				<tr>";
	strHTMLText += "					<td align='right' >";
	strHTMLText += "						<table border='0' cellpadding='0' cellspacing='0'>";
	strHTMLText += "							<tr>";
	strHTMLText += "								<td class='lgndBd' width='100%' style='background-color:green'><font>&nbsp;</font></td>";
	strHTMLText += "							</tr>";
	strHTMLText += "						</table>";
	strHTMLText += "					</td>";
	strHTMLText += "					<td>";
	strHTMLText += "						<font>&nbsp;BC Last 7 Days</font>";
	strHTMLText += "					</td>";
	//strHTMLText += "					<td style='width:5px;'></td>";
	strHTMLText += "					<td>";
	strHTMLText += "						<table border='0' cellpadding='0' cellspacing='0'>";
	strHTMLText += "							<tr>";
	strHTMLText += "								<td class='lgndBd' width='100%' style='background-color:orange'><font>&nbsp;</font></td>";
	strHTMLText += "							</tr>";
	strHTMLText += "						</table>";
	strHTMLText += "					</td>";
	strHTMLText += "					<td>";
	strHTMLText += "						<font>BC Last 30 Days</font>";
	strHTMLText += "					</td>";
	strHTMLText += "					<td style='width:5px;'></td>";
	strHTMLText += "				<\/tr>";
	strHTMLText += "				<tr>";
	strHTMLText += "					<td align='right' style='height:4px;'>"
	strHTMLText += "				<\/tr>";
	//strHTMLText += "				<tr>";
	//strHTMLText += "					<td colspan='6' align='right'>"
	//strHTMLText += "						<input type='button' id='btn' name='btn' class='button' value='Cancel'>";
	//strHTMLText += "						<input type='button' id='btn' name='btn' class='button' value='Confirm'>";
	//strHTMLText += "					<\/td>";
	//strHTMLText += "				<\/tr>";
	strHTMLText += "			<\/table>";
	strHTMLText += "		<\/td>";
	strHTMLText += "	<\/tr>";
	strHTMLText += "<\/table>";
	DivWrite("spnAnalyzeInfo", strHTMLText);

}

function generateAnalyzeBreakDown() {
	var strHTMLText = "";

	var strClassRow = "GridItemRow GridItem rowHeight";

	strHTMLText += "<table width='100%' border='0' cellpadding='1' cellspacing='0'>";
	strHTMLText += "	<tr>";
	strHTMLText += "		<td width='16%' class='GridRow GridHeader GridHeaderBand GridHDBorder' align='center' rowspan='2'><font>Booking Class<\/font><\/td>";
	strHTMLText += "		<td class='GridRow GridHeader GridHeaderBand GridHDBorder' align='center' colspan='3'><font>TTL<\/font><\/td>";
	strHTMLText += "		<td class='GridRow GridHeader GridHeaderBand GridHDBorder' align='center' colspan='2'><font>Seat Sold<\/font><\/td>";
	strHTMLText += "		<td class='GridRow GridHeader GridHeaderBand GridHDBorder' align='center' colspan='3'><font>Rate of Seat Sale<\/font><\/td>";
	strHTMLText += "	<\/tr>";
	strHTMLText += "	<tr>";
	strHTMLText += "		<td width='8%' class='GridRow GridHeader GridHeaderBand GridHDBorder' align='center'><font>Aloc<\/font><\/td>";
	strHTMLText += "		<td width='8%' class='GridRow GridHeader GridHeaderBand GridHDBorder' align='center'><font>Sold<\/font><\/td>";
	strHTMLText += "		<td width='10%' class='GridRow GridHeader GridHeaderBand GridHDBorder' align='center'><font>On-Hold<\/font><\/td>";
	strHTMLText += "		<td width='15%' class='GridRow GridHeader GridHeaderBand GridHDBorder' align='center'><font>First<\/font><\/td>";
	strHTMLText += "		<td width='15%' class='GridRow GridHeader GridHeaderBand GridHDBorder' align='center'><font>Last<\/font><\/td>";
	strHTMLText += "		<td width='9%' class='GridRow GridHeader GridHeaderBand GridHDBorder' align='center'><font>Over 30 Days<\/font><\/td>";
	strHTMLText += "		<td width='10%' class='GridRow GridHeader GridHeaderBand GridHDBorder' align='center'><font>Last 8-30 Days<\/font><\/td>";
	strHTMLText += "		<td width='9%' class='GridRow GridHeader GridHeaderBand GridHDBorder' align='center'><font>Last 7 Days<\/font><\/td>";
	strHTMLText += "	<\/tr>";
	if (UI_FLA.fltInvInfoList[0].flightNo == null) {
		strHTMLText += "	<tr>";
		strHTMLText += "		<td colspan='9' align='right' style='padding:0px;'>"
		strHTMLText += "			<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
		strHTMLText += "				<tr>";
		strHTMLText += "					<td class='GridRow GridHeader GridHeaderBand GridHDBorder' >";
		strHTMLText += "						<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
		strHTMLText += "							<tr>";
		strHTMLText += "								<td width='2%'><font>&nbsp;<\/font><\/td>";
		strHTMLText += "								<td><font><b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </b><\/font><\/td>";
		strHTMLText += "								<td><font><b>No Data found for the selected flight</b><\/font><\/td>";
		//strHTMLText += "								<td><font><b>" + UI_FLA.fltInvInfoList[i].departDate + "</b><\/font><\/td>";			
		strHTMLText += "							<\/tr>";
		strHTMLText += "						<\/table>";
		strHTMLText += "					<\/td>";
		strHTMLText += "				<\/tr>";
	} else {

		var intLen = UI_FLA.fltInvInfoList.length;
		var i = 0;
		do {
			strHTMLText += "	<tr>";
			strHTMLText += "		<td colspan='9' align='right' style='padding:0px;'>"
			strHTMLText += "			<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
			//strHTMLText += "				<tr>";
			//strHTMLText += "					<td class='GridRow GridHeader GridHeaderBand GridHDBorder' >";
			//strHTMLText += "						<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
			//strHTMLText += "							<tr>";
			//strHTMLText += "								<td width='2%'><font>&nbsp;<\/font><\/td>";
			//strHTMLText += "								<td><font><b>" + UI_FLA.fltInvInfoList[i].flightNo + "</b><\/font><\/td>";
			//strHTMLText += "								<td><font><b>" + UI_FLA.fltInvInfoList[i].ond + "</b><\/font><\/td>";			
			//strHTMLText += "								<td><font><b>" + UI_FLA.fltInvInfoList[i].departDate + "</b><\/font><\/td>";			
			//strHTMLText += "							<\/tr>";			
			//strHTMLText += "						<\/table>";			
			//strHTMLText += "					<\/td>";			
			//strHTMLText += "				<\/tr>";			
			if (UI_FLA.fltInvInfoList[i].ondDetailsLst != null) {
				var intLenOND = UI_FLA.fltInvInfoList[i].ondDetailsLst.length;
				var x = 0;
				do {
					strHTMLText += "				<tr>";
					strHTMLText += "					<td class='GridRow GridHeader GridHeaderBand GridHDBorder' >";
					strHTMLText += "						<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
					strHTMLText += "							<tr>";
					strHTMLText += "								<td width='5%'><font>&nbsp;<\/font><\/td>";
					strHTMLText += "								<td><font><b>" + UI_FLA.fltInvInfoList[i].ondDetailsLst[x].ond + "</b><\/font><\/td>";
					strHTMLText += "							<\/tr>";			
					strHTMLText += "						<\/table>";
					strHTMLText += "					<\/td>";			
					strHTMLText += "				<\/tr>";			

					var intLenCOS = UI_FLA.fltInvInfoList[i].ondDetailsLst[x].cosLst.length;
					var y = 0;
					do {
						strHTMLText += "				<tr>";
						strHTMLText += "					<td class='GridRow GridHeader GridHeaderBand GridHDBorder' >";
						strHTMLText += "						<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
						strHTMLText += "							<tr>";
						strHTMLText += "								<td width='10%'><font>&nbsp;<\/font><\/td>";
						strHTMLText += "								<td><font>COS : <b>"
								+ UI_FLA.fltInvInfoList[i].ondDetailsLst[x].cosLst[y].cosClass
								+ "</b><\/font><\/td>";
						strHTMLText += "							<\/tr>";
						strHTMLText += "						<\/table>";
						strHTMLText += "					<\/td>";
						strHTMLText += "				<\/tr>";

						var intLenCOSD = UI_FLA.fltInvInfoList[i].ondDetailsLst[x].cosLst[y].bcDetailLst.length;
						var m = 0;
						var strClass = "";

						strHTMLText += "				<tr>";
						strHTMLText += "					<td>";
						strHTMLText += "						<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
						do {
							strClass = (m % 2) != 0 ? "GridAlternateRow"
									: "GridItem";

							strHTMLText += "							<tr>";
							strHTMLText += "								<td width='16%' class='"
									+ strClassRow
									+ " "
									+ strClass
									+ "' align='center'><font>"
									+ UI_FLA.fltInvInfoList[i].ondDetailsLst[x].cosLst[y].bcDetailLst[m].bkgClass
									+ "<\/font><\/td>";
							strHTMLText += "								<td width='8%'  class='"
									+ strClassRow
									+ " "
									+ strClass
									+ "' align='right'><font>"
									+ UI_FLA.fltInvInfoList[i].ondDetailsLst[x].cosLst[y].bcDetailLst[m].aloc
									+ "<\/font><\/td>";
							strHTMLText += "								<td width='8%'  class='"
									+ strClassRow
									+ " "
									+ strClass
									+ "' align='right'><font>"
									+ UI_FLA.fltInvInfoList[i].ondDetailsLst[x].cosLst[y].bcDetailLst[m].sold
									+ "<\/font><\/td>";
							strHTMLText += "								<td width='10%'  class='"
									+ strClassRow
									+ " "
									+ strClass
									+ "' align='right'><font>"
									+ UI_FLA.fltInvInfoList[i].ondDetailsLst[x].cosLst[y].bcDetailLst[m].hold
									+ "<\/font><\/td>";
							strHTMLText += "								<td width='15%' class='"
									+ strClassRow
									+ " "
									+ strClass
									+ "' align='center'><font>"
									+ UI_FLA.fltInvInfoList[i].ondDetailsLst[x].cosLst[y].bcDetailLst[m].first
									+ "<\/font><\/td>";
							strHTMLText += "								<td width='15%' class='"
									+ strClassRow
									+ " "
									+ strClass
									+ "' align='center'><font>"
									+ UI_FLA.fltInvInfoList[i].ondDetailsLst[x].cosLst[y].bcDetailLst[m].last
									+ "<\/font><\/td>";
							strHTMLText += "								<td width='9%' class='"
									+ strClassRow
									+ " "
									+ strClass
									+ "' align='right'><font>"
									+ UI_FLA.fltInvInfoList[i].ondDetailsLst[x].cosLst[y].bcDetailLst[m].over30
									+ "<\/font><\/td>";
							strHTMLText += "								<td width='10%' class='"
									+ strClassRow
									+ " "
									+ strClass
									+ "' align='right'><font>"
									+ UI_FLA.fltInvInfoList[i].ondDetailsLst[x].cosLst[y].bcDetailLst[m].last30
									+ "<\/font><\/td>";
							
							strHTMLText += "								<td width='9%' class='"
									+ strClassRow
									+ " "
									+ strClass
									+ "' align='right'><font>"
									+ UI_FLA.fltInvInfoList[i].ondDetailsLst[x].cosLst[y].bcDetailLst[m].last7
									+ "<\/font><\/td>";
							strHTMLText += "							<\/tr>";

							m++;
						} while (--intLenCOSD);

						strHTMLText += "						<\/table>";
						strHTMLText += "					<\/td>";
						strHTMLText += "				<\/tr>";

						y++;
					} while (--intLenCOS);
					//strHTMLText += "			<\/table>";
					x++;
				} while (--intLenOND);
				strHTMLText += "		<\/td>"; 
				strHTMLText += "	<\/tr>"; 
				i++;
			}
		} while (--intLen);
	}
	strHTMLText += "<\/table>";

	DivWrite("spnAnaBreak", strHTMLText);
}


function generateAlertData(jsArrayData) {
	arrDataAlert = new Array();

	var arrLenght = jsArrayData.length;
	for ( var i = 0; i < arrLenght; i++) {
		arrDataAlert[i] = new Array(1, jsArrayData[i].alertType,
				"<font style='color:blue'>" + jsArrayData[i].pnr
						+ "</font></u>", jsArrayData[i].passengerName,
				jsArrayData[i].bkngMode, jsArrayData[i].phoneNo +jsArrayData[i].mobileNo,
				jsArrayData[i].bkngClass, jsArrayData[i].bkngDate, "",
				jsArrayData[i].isActioned, "<input name='btnGrdEmail' type='button' class='Button' id='btnGrdEmail' value='Email' onclick='parent.UI_FLA.showEmailPopup(" + i + ");' style='width:60px'>",
				"<input name='btnGrdPrint' type='button' class='Button' id='btnGrdPrint' value='Print' onclick='parent.UI_FLA.printAlerts(" + i + ");' style='width:60px'>");				
	}
	objDGA.arrGridData = arrDataAlert;
	objDGA.refresh();
}

function tblDetailClick(intIndex) {
	getFieldByID("tdTab1").style.backgroundImage = "url(../../images/M008_no_cache.jpg)";
	getFieldByID("tdTab2").style.backgroundImage = "url(../../images/M008_no_cache.jpg)";
	//getFieldByID("tdTab3").style.backgroundImage = "url(../../images/M008_no_cache.jpg)";
	getFieldByID("tdTab4").style.backgroundImage = "url(../../images/M008_no_cache.jpg)";
	setDisplay("spnAnalyzeInfo", false);
	setDisplay("spnOptimizeInfo", false)
	setDisplay("spnFareReqInfo", false);
	setDisplay("spnAlertInfo", false);

	setVisible("spnGridFareReq", false);
	setVisible("spnGridAlert", false);

	switch (intIndex) {
	case 0:
		UI_EmailSender.distroy();
		getFieldByID("tdTab1").style.backgroundImage = "url(../../images/M007_no_cache.jpg)";
		setDisplay("spnAnalyzeInfo", true);
		setVisible("spnAnalyzeInfo", true);
		break;
	case 1:	
			getFieldByID("tdTab2").style.backgroundImage = "url(../../images/M007_no_cache.jpg)";
			setDisplay("spnAnalyzeInfo", false);
			setDisplay("spnOptimizeInfo", true)
			setVisible("spnOptimizeInfo", true);
			setDisplay("div_Optimize_Details", false)
			UI_FLA_Optimize.selectedRowId = 0;

			var url = "searchOptimize.action";
			var data = {};
			data['flightID'] = jsClickedFlight.flightID;
			UI_FLA.ajaxOptimizeSearch(url, data);
		
		break;
	case 2:
		//getFieldByID("tdTab3").style.backgroundImage = "url(../../images/M007_no_cache.jpg)";
		//setDisplay("spnFareReqInfo", true);
		//setVisible("spnGridFareReq", true);
		break;
	case 3:
		getFieldByID("tdTab4").style.backgroundImage = "url(../../images/M007_no_cache.jpg)";
		setVisible("spnAnalyzeInfo", false);
		var url = "searchAlerts.action";
		var data = {};
		data['flightID'] = jsClickedFlight.flightID;
		UI_FLA.ajaxAlertSearch(url, data);
		break;
	}
}

/*
 * TODO
 * Grid Navigations
 */
function gridNavigations(intRecNo, strErrMsg) {
	if (strErrMsg != "") {
		showERRMessage(strErrMsg);
	} else {
		var url = "searchFlightLoad!searchFlights.action";
		var data = {};
		data['strOrg'] = jsSearchVal.orign;
		data['strDest'] = jsSearchVal.dest;
		data['strDateStart'] = strDateCurr;
		data['strDateEnd'] = jsSearchVal.todate;
		data['intLen'] = jsSearchVal.intLen;
		data['recNo'] = intRecNo - 1;
		jsSearchVal.recNo = intRecNo;
		data['totRec'] = jsSearchVal.totRec;
		UI_FLA.ajaxFlightSearch(url, data);
	}
}

/*
 * Show Search Results
 */
function showSearchResults() {
	showHideDetails(false);

	strDateCurr = getValue(jsCntrl[2].id);
	strDateStart = strDateCurr;
	strDateEnd = getValue(jsCntrl[3].id);

	setDisplay("spnSrchResuls", true);
	setDisplay("spnDetails", true);

	DivWrite("spnSltdInfo", "");

	gererateGridData(true);
	generateGrid(strDateCurr);
}

/*
 * Set Default Focus
 */
function setDefaultFocus() {
	setFocus(jsCntrl[0].id);
}

/*
 * Fill Drop Down Data
 */
function fillDropDownData() {
	var objLB = new listBox();
	objLB.dataArray = arrAirports;
	objLB.id = jsCntrl[0].id;
	objLB.blnFirstEmpty = true;
	objLB.textIndex = 0;
	objLB.firstValue = "";
	objLB.firstTextValue = "";
	objLB.fillListBox();

	objLB = new listBox();
	objLB.dataArray = arrAirports;
	objLB.id = jsCntrl[1].id;
	objLB.blnFirstEmpty = true;
	objLB.textIndex = 0;
	objLB.firstValue = "";
	objLB.firstTextValue = "";
	objLB.fillListBox();
}

/*
 * Initialize 
 */
function initialize() {
	getFieldByID("frmAction").reset();
	setDefaultFocus();

	setDisplay("spnSrchResuls", false);
	setDisplay("spnAnalyzeInfo", false);
	setDisplay("spnOptimizeInfo", false);
	setDisplay("spnFareReqInfo", false);

	setDisplay("spnDetailInfoTab", false);
	setVisible("spnGridFareReq", false);

	setDisplay("spnDetails", false);

	DivWrite("spnSltdInfo", "");

	getFieldByID("tdSrchResults").style.top = "465px";

	intLastLoaded = -1;
	intLastRow = -1;
	intLastCol = -1;

	intSetStartNo = 1;

	intTotRecords = 25;
	intRecPerPage = 25;
	strDateStart = "";
	strDateCurr = "";
	strDateEnd = "";
	showHideDetails(false);
	blnLastDetailsStatus = false;
	initializeMessage();
}

UI_FLA.createLegends = function(){
	var 	strAppend  = "<table border='0' cellpadding='0' cellspacing='1'>";
		strAppend += " <tr>";
		var length = jsFlghtPerformance.length;
		for ( var i = 0; i < length; i++) {
			strAppend += "  <td>";
			strAppend += "   <table border='0' cellpadding='0' cellspacing='0'>";
			strAppend += "    <tr>";
			strAppend += "     <td class='lgndBd' width='100%' style='background-color:"+jsFlghtPerformance[i].bg+"'><font>&nbsp;</font></td>";
			strAppend += "    </tr>";
			strAppend += "   </table>";
			strAppend += "  </td>";
			strAppend += "  <td>";
			strAppend += "   <font>&nbsp;"+jsFlghtPerformance[i].rnk+"</font>";
			strAppend += "  </td>";
			strAppend += "  <td style='width:5px;'></td>";
		}
		strAppend += " </tr>";
		strAppend += "</table>";
	$('#spnLegend').append(strAppend);

}

/*
 *  On Load
 */
function onLoad() {
	top[2].HideProgress();
	fillDropDownData();
	initialize();
	UI_FLA.timerID = setInterval('UI_FLA.blinkCurrentSelected()', 500);
	UI_FLA.createLegends();
}

onLoad();

/**
 * UI_FLA for Flight Load Analysis UI related functionalities
 */
function UI_FLA() {
}

UI_FLA.fltInvInfoList = null;
UI_FLA.fltInvInfo = null;
UI_FLA.graphData = null;
UI_FLA.previousBlock = null;
UI_FLA.timerID = null;
UI_FLA.addtionalDataMap = null;
UI_FLA.lstGrphBarBrkDwn = null;
UI_FLA.jsArrayData = null;

/**
 * UI_FLA.flightSearch search Flight Deatils
 * Back End call goes here
 */
UI_FLA.flightSearch = function() {
	if (validate()) {
		showHideDetails(false);
		setDisplay("spnSrchResuls", false);
		var intLen = 25;
		strDateCurr = getValue(jsCntrl[2].id);
		strDateStart = strDateCurr;
		strDateEnd = getValue(jsCntrl[3].id);
		var strDest = "";
		var strOrg = getValue(jsCntrl[0].id);
		if (getValue(jsCntrl[1].id) != "") {
			strDest = getValue(jsCntrl[1].id);
			intLen = 7;//FIXME this is used, when Dest is not given
		}

		jsSearchVal.orign = strOrg;
		jsSearchVal.dest = strDest;
		jsSearchVal.fromDate = strDateStart;
		jsSearchVal.todate = strDateEnd;
		jsSearchVal.startdate = strDateCurr;
		jsSearchVal.recNo = 0;
		jsSearchVal.intLen = intLen;
		jsSearchVal.totRec = 0;

		setDisplay("spnDetails", true);

		DivWrite("spnSltdInfo", "");
		var url = "searchFlightLoad!searchFlights.action";
		var data = {};
		data['strOrg'] = strOrg;
		data['strDest'] = strDest;
		data['strDateStart'] = strDateStart;
		data['strDateEnd'] = strDateEnd;
		data['intLen'] = intLen;
		data['recNo'] = 0;
		UI_FLA.stopBlinkCurrentSelected();
		UI_FLA.ajaxFlightSearch(url, data);

	}
}

/**
 * After Ajax Flight Search
 */
UI_FLA.afterFlightSearch = function(response) {
	if (response.success) {
		eval(response.arrRetData);
		eval(response.totRec);
		eval(response.addtionalDetailMap);
		setDisplay("spnSrchResuls", true);
		//eval(response.totRec);
		UI_FLA.addtionalDataMap = response.addtionalDetailMap;
		jsSearchVal.totRec = response.totRec;
		jsSearchVal.intLen = response.intLen;
		jsSearchVal.recNo = response.recNo;
		generateGrid(strDateCurr, response.arrRetData, response.totRec, response.intLen, response.recNo);
		top[2].HideProgress();
	} else {
		showERRMessage("Search Flights Error.");
	}
}

/**
 * After Ajax Inventory Search
 */
UI_FLA.afterInvetorySearch = function(response) {

	if (response.success) {
		UI_FLA.fltInvInfoList = response.fltInvInfoList;
		UI_FLA.fltInvInfo = UI_FLA.fltInvInfoList[0];

		var channel = response.lstChannels;
		var seven = response.lstSeven;
		var thirty = response.lstThirty;
		
		var jsGrphData = {
			seven : seven,
			channel : channel,
			thirty : thirty
		};
		UI_FLA.graphData = jsGrphData;
	} else {
		showERRMessage("Invetory Search Error");
	}
	showHideDetails(true);
	generateDetailInfo();
	//createRevenueChannelWise(UI_FLA.graphData);
	drawVisualization(UI_FLA.graphData);
	generateAnalyzeBreakDown();
	top[2].HideProgress();
}

/**
 * Ajax submit for Flight Search
 */
UI_FLA.ajaxFlightSearch = function(url, data, mode) {
	top[2].ShowProgress();
	$.ajax( {
		url : url,
		type : 'POST',
		data : data,
		dataType : 'json',
		success : function(response) {
			UI_FLA.afterFlightSearch(response);
		},
		error : function(requestObj, textStatus, errorThrown) {
			UI_FLA.setErrorStatus(requestObj);
		}
	});
}

/**
 * Ajax submit for Inventory Search
 */
UI_FLA.ajaxInventorySearch = function(url, data, mode) {
	top[2].ShowProgress();
	$.ajax( {
		url : url,
		type : 'POST',
		data : data,
		dataType : 'json',
		success : function(response) {
			UI_FLA.afterInvetorySearch(response);
		},
		error : function(requestObj, textStatus, errorThrown) {
			UI_FLA.setErrorStatus(requestObj);
		}
	});
}

/**
 * Blink start current for selected
 */
UI_FLA.blinkCurrentSelected = function() {

	if (intLastCol != -1) {
		var selectedId = "#td_blk_" + intLastRow + "_" + intLastCol;

		if ($(selectedId).css('background-color') == "rgb(192, 192, 192)") {
			$(selectedId).css('background-color', strLastColor);
		} else {
			$(selectedId).css('background-color', '#C0C0C0');
		}
	}
}

/**
 * Stop blinking for selected
 */
UI_FLA.stopBlinkCurrentSelected = function() {
	intLastRow = -1;
	intLastCol = -1;	
}

/**
 * Ajax submit for Alert Search
 */
UI_FLA.ajaxAlertSearch = function(url, data, mode) {
	top[2].ShowProgress();
	$.ajax( {
		url : url,
		type : 'POST',
		data : data,
		dataType : 'json',
		success : function(response) {
			UI_FLA.afterAlertSearch(response);
		},
		error : function(requestObj, textStatus, errorThrown) {
			UI_FLA.setErrorStatus(requestObj);
		}
	});
}

/**
 * After Ajax Alert Search
 */
UI_FLA.afterAlertSearch = function(response) {

	if (response.success) {
		var jsArrayData = response.lstAlertDTO;
		UI_FLA.jsArrayData = response.lstAlertDTO;
		generateAlertData(jsArrayData);
		setDisplay("spnAlertInfo", true);
		setVisible("spnGridAlert", true);

	} else {
		showERRMessage("Alert Search Error.");
	}
	showHideDetails(true);
	generateDetailInfo();
	//createRevenueChannelWise(UI_FLA.graphData);
	drawVisualization(UI_FLA.graphData);
	generateAnalyzeBreakDown();
	top[2].HideProgress();
}


/**
 * Ajax submit for Optimize Search
 */
UI_FLA.ajaxOptimizeSearch = function(url, data, mode) {
	top[2].ShowProgress();
	$.ajax( {
		url : url,
		type : 'POST',
		data : data,
		dataType : 'json',
		success : function(response) {
			UI_FLA_Optimize.submitSuccess(response);
		},
		error : function(requestObj, textStatus, errorThrown) {
			UI_FLA.setErrorStatus(requestObj);
		}
	});
}


/**
 * Session timeout call back
 */
UI_FLA.setErrorStatus = function(resObj) {
	if (resObj.responseText != null) {
		//if user session expired then logout the user
		if (resObj.responseText
				.indexOf("adfsdfj123dfa893435#@!bnyufUYR345245!RirasdfnM#djf") != -1) {
			top[1].location.replace("showMain");
		}
	}
	if ((top.objCW) && (!top.objCW.closed)) {
		top.objCW.close();
	}
}


/**
 * Create the graph using google visualization API
 */
function drawVisualization() {
    // Create and populate the data table.
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Channel');
    data.addColumn('number', 'BC Last 7 Days');
    data.addColumn('number', 'BC Last 30 Days');
           
    data.addRows(UI_FLA.graphData.channel.length);
  
    for (var j = 0; j < UI_FLA.graphData.channel.length; ++j) {    
      data.setValue(j, 0, UI_FLA.graphData.channel[j].toString());    
    }
    for (var k = 0; k  < UI_FLA.graphData.seven.length; ++k) {
    	data.setValue(k, 1, UI_FLA.graphData.seven[k]);   
    }
    for (var l = 0; l  < UI_FLA.graphData.thirty.length; ++l) {
    	data.setValue(l, 2, UI_FLA.graphData.thirty[l]);   
    }
    
    // Create and draw the visualization.
    barsVisualization = new google.visualization.BarChart(document.getElementById('visualization'));
    barsVisualization.draw(data, 
            { width:400, height:200 ,
        	isStacked:true,
        	is3D:true,
        	colors:[{color:'#008800', darker:'#005500'}, {color:'#ffa500', darker:'#997733'}],
        	legend:'none'}                   
        );
    // Add our over/out handlers.
    google.visualization.events.addListener(barsVisualization, 'select', barSelect);
    
  }

/**
 * Execute after bar clicked in the graph  
 */
function barSelect(e) {
	var selection = barsVisualization.getSelection();
	  for (var i = 0; i < selection.length; i++) {
	    var item = selection[i];
	    if(item.row != null){
	       	var selectedChnl = UI_FLA.graphData.channel[item.row];
	       	var url = "searchMIAllocation!searchGraphData.action";
			var data = {};
			data['grphFlightID'] = jsClickedFlight.flightID;
			data['grphChannel'] = selectedChnl;
			UI_FLA.ajaxGraphDataSearch(url, data);
	    }
	  }
	 // barsVisualization.setSelection([{'row': null, 'column': null}]);
}
  
  
  /**
   * Ajax submit for Alert Search
   */
  UI_FLA.ajaxGraphDataSearch = function(url, data, mode) {
  	top[2].ShowProgress();
  	$.ajax( {
  		url : url,
  		type : 'POST',
  		data : data,
  		dataType : 'json',
  		success : function(response) {
  			UI_FLA.afterGraphDataSearch(response);
  		},
  		error : function(requestObj, textStatus, errorThrown) {
  			UI_FLA.setErrorStatus(requestObj);
  		}
  	});
  }
  
  /**
   * After Ajax Alert Search
   */
  UI_FLA.afterGraphDataSearch = function(response) {
	// $('graphPopup').clear();
  	if (response.successGrph) {
  		top[2].HideProgress();
  		UI_FLA.lstGrphBarBrkDwn = response.lstGrphBarBrkDwn;
  		
  			try{
  				if (UI_FLA.lstGrphBarBrkDwn != null) {
					$('#spnResSummary').remove();
					$('#graphPopup').append(UI_FLA.constructReservationSummaryPopup());  					
  				}
  			}catch(ex){}	
  	} else {
  		showERRMessage("Graph Data Search Error.");
  	}  	
  }

  /**
   * Create popup window for reservation charges and payments
   */
UI_FLA.constructReservationSummaryPopup = function() {

		var strAppend = "<span id='spnResSummary' style='position:absolute;top:-80px;left:20px;z-index:2500;width:480px;height:200px;'>";
		strAppend +=	"<table width='100%' border='0' cellpadding='0' cellspacing='0' style='background-color: yellow;'>";
		strAppend +=	"<tr>";
		strAppend +=	"<td valign='top'>";
		strAppend +=	"<table width='100%' cellpadding='0' cellspacing='0' border='0' align='center' ID='Table4' style='background-color: #FFFFFF;'>";
		strAppend +=	"<tr>";
		strAppend +=	"<td width='10' height='20' ><img src='../../images/form_top_left_corner_no_cache.gif'><\/td>";
		strAppend +=	"<td colspan='2' height='20' class='FormHeadBackGround'><img src='../../images/bullet_small_no_cache.gif'> <font class='FormHeader'>";
		strAppend +=	"Reservation Breakdown<\/font><\/td>";
		strAppend +=	"<td width='11' height='20'><img src='../../images/form_top_right_corner_no_cache.gif'><\/td>";
		strAppend +=	"<\/tr>";

		strAppend +=	"<tr>";
		strAppend +=	"<td ><\/td>";
		strAppend +=	"<td valign='top' style='height:160px;' colspan='2'>";
		strAppend +=   	"<span style='position:absolute; height:150px;width:460px;overflow-X:auto; overflow-Y:auto;' style='background-color: yellow;'>"
		strAppend +=	UI_FLA.constructReservationSummaryData();
		strAppend +=	"<\/span><\/td>";
		strAppend +=	"<td ><\/td>";
		strAppend +=	"<\/tr>";
		strAppend +=	"<tr>";

		strAppend +=	"<tr>";
		strAppend +=	"<td ><\/td>";
		strAppend +=	"<td align='left'>&nbsp;<\/td>";
		strAppend +=	"<td align='right'>";
		strAppend +=	"<input name='btnClose' type='button' class='button' id='btnClose' value='Close' onclick='UI_FLA.closePopup();'>";
		strAppend +=	"<\/td>";
		strAppend +=	"<td><\/td>";
		strAppend +=	"<\/tr>";

		strAppend +=	"<tr>";
		strAppend +=	"<td height='12'><img src='../../images/form_bottom_left_corner_no_cache.gif'><\/td>";
		strAppend +=	"<td height='12' colspan='2' ><img src='../../images/spacer_no_cache.gif' width='100%' height='1'><\/td>";
		strAppend +=	"<td height='12'><img src='../../images/form_bottom_right_corner_no_cache.gif'><\/td>";
		strAppend +=	"<\/tr>";

		strAppend +=	"<\/table>";
		strAppend +=	"<\/td>";
		strAppend +=	"<\/table>";
		strAppend +=	"<\/span>";
	return strAppend;
}

/**
 * Create table of payments and charges
 *  after clicking the column in the graph
 */
UI_FLA.constructReservationSummaryData = function(){
		 
	
		var strHTMLText = "<table width='100%' border='1' cellpadding='1' cellspacing='0'>";
		strHTMLText += "	<tr><td class='GridHeader'><font><b>Agent Code</b><\/font><\/td>";
		strHTMLText += "		<td class='GridHeader'><font><b>Agent Name</b><\/font><\/td>";
		strHTMLText += "		<td class='GridHeader'><font><b>PNR</b><\/font><\/td>";
		strHTMLText += "		<td class='GridHeader'><font><b>Status</b><\/font><\/td>";
		strHTMLText += "		<td class='GridHeader'><font><b>Pax Cnt</b><\/font><\/td>";
		strHTMLText += "		<td class='GridHeader'><font><b>Inf Cnt</b><\/font><\/td>";
		strHTMLText += "		<td class='GridHeader'><font><b>Total Payment</b><\/font><\/td>";
		strHTMLText += "		<td class='GridHeader'><font><b>Total Charges</b><\/font><\/td>";
		strHTMLText += "	<\/tr>";
				
		var dtCurr = new Date();
		var strDepDate = jsClickedFlight.departureDate;
		var dtDepDate = StringToDate(strDepDate);
		
		if(dtDepDate > dtCurr)
			dtDepDate = dtCurr;
		
		var dtLastSeven = addDays(dtDepDate, -7);
		var dtLastThirty = addMonthsLocal(dtDepDate, -1);
		
		
		for(var i=0; i < UI_FLA.lstGrphBarBrkDwn.length; i++){
			var dtLastModDate = StringToDate(UI_FLA.lstGrphBarBrkDwn[i].lastModDate);
			var bgColor = "";
			var fntColor = "";	
			if(dtLastModDate >= dtLastSeven){
				bgColor = "green";
				fntColor = "white";
			}else if(dtLastModDate >= dtLastThirty){
				bgColor = "orange";
				fntColor = "black";
			}			
			//Highlighting last seven days and last thirty days bookings
			if(bgColor != ""){
				strHTMLText += "	<tr><td bgcolor='"+ bgColor +"' align='center'><font style='color:"+fntColor+"';>"+ UI_FLA.lstGrphBarBrkDwn[i].agentCode +"<\/font><\/td>";
				strHTMLText += "		<td bgcolor='"+ bgColor +"' align='center'><font style='color:"+fntColor+"';>"+ UI_FLA.lstGrphBarBrkDwn[i].agentName +"<\/font><\/td>";
				strHTMLText += "		<td bgcolor='"+ bgColor +"' align='center'><font style='color:"+fntColor+"';>"+ UI_FLA.lstGrphBarBrkDwn[i].pnr +"<\/font><\/td>";
				strHTMLText += "		<td bgcolor='"+ bgColor +"' align='center'><font style='color:"+fntColor+"';>"+ UI_FLA.lstGrphBarBrkDwn[i].status +"<\/font><\/td>";
				strHTMLText += "		<td bgcolor='"+ bgColor +"' align='center'><font style='color:"+fntColor+"';>"+ UI_FLA.lstGrphBarBrkDwn[i].totPaxCnt +"<\/font><\/td>";
				strHTMLText += "		<td bgcolor='"+ bgColor +"' align='center'><font style='color:"+fntColor+"';>"+ UI_FLA.lstGrphBarBrkDwn[i].totInfCnt +"<\/font><\/td>";
				strHTMLText += "		<td bgcolor='"+ bgColor +"' align='right'><font style='color:"+fntColor+"';>"+ UI_FLA.lstGrphBarBrkDwn[i].totCharges +"<\/font><\/td>";
				strHTMLText += "		<td bgcolor='"+ bgColor +"' align='right'><font style='color:"+fntColor+"';>"+ UI_FLA.lstGrphBarBrkDwn[i].totPayments +"<\/font><\/td>";
				strHTMLText += "	<\/tr>";
				
			}else{
				strHTMLText += "	<tr><td class='GridItem' align='center'><font>"+ UI_FLA.lstGrphBarBrkDwn[i].agentCode +"<\/font><\/td>";
				strHTMLText += "		<td class='GridItem' align='center'><font>"+ UI_FLA.lstGrphBarBrkDwn[i].agentName +"<\/font><\/td>";
				strHTMLText += "		<td class='GridItem' align='center'><font>"+ UI_FLA.lstGrphBarBrkDwn[i].pnr +"<\/font><\/td>";
				strHTMLText += "		<td class='GridItem' align='center'><font>"+ UI_FLA.lstGrphBarBrkDwn[i].status +"<\/font><\/td>";
				strHTMLText += "		<td class='GridItem' align='center'><font>"+ UI_FLA.lstGrphBarBrkDwn[i].totPaxCnt +"<\/font><\/td>";
				strHTMLText += "		<td class='GridItem' align='center'><font>"+ UI_FLA.lstGrphBarBrkDwn[i].totInfCnt +"<\/font><\/td>";
				strHTMLText += "		<td class='GridItem' align='right'><font>"+ UI_FLA.lstGrphBarBrkDwn[i].totCharges +"<\/font><\/td>";
				strHTMLText += "		<td class='GridItem' align='right'><font>"+ UI_FLA.lstGrphBarBrkDwn[i].totPayments +"<\/font><\/td>";
				strHTMLText += "	<\/tr>";
			}
		}
		strHTMLText += "	<tr><td colspan='8'><font>*Note:Total payments & total charges" +
					" belongs only for this airline's reservation portion will be displayed " +
					"under LCC booking channel, </br>Over thirty days reservations will be displayed " +
					"in white colour.<\/font></td><\/tr>";
		strHTMLText += "<\/table>";

		return strHTMLText;
}

UI_FLA.showEmailPopup = function(index){

	var strPrepend = "\n\nAlert Type\t\t\t: " + UI_FLA.jsArrayData[index].alertType + "\n";
	   strPrepend += "PNR\t\t\t\t: " + UI_FLA.jsArrayData[index].pnr + "\n";
	   strPrepend += "Passenger Name\t: " + UI_FLA.jsArrayData[index].passengerName + "\n";
	   strPrepend += "Booking Date\t\t: " + UI_FLA.jsArrayData[index].bkngDate + "\n";
	   strPrepend += "Departure Date\t\t: " + jsClickedFlight.departureDate + "\n";
	   strPrepend += "Booking Class\t\t: " + UI_FLA.jsArrayData[index].bkngClass + "\n";

	var strSubject = UI_FLA.jsArrayData[index].alertType + " in flight " + jsClickedFlight.flightNumber;

	UI_EmailSender.displayForm({divName:'emailPopup', prepend:strPrepend, subject:strSubject, 
		errorCallback:function(message){
			showERRMessage(message);
		},
		successCallback:function(message){
			showERRMessage(message);
		}});
}

UI_FLA.printAlerts = function (index){
	var strPrepend = "<p><b>Alert Type</b>\t\t\t: " + UI_FLA.jsArrayData[index].alertType + "</p>";
	   strPrepend += "<p><b>PNR</b>\t\t\t\t: " + UI_FLA.jsArrayData[index].pnr + "</p>";
	   strPrepend += "<p><b>Passenger Name</b>\t: " + UI_FLA.jsArrayData[index].passengerName +"</p>";
	   strPrepend += "<p><b>Booking Date</b>\t\t: " + UI_FLA.jsArrayData[index].bkngDate + "</p>";
	   strPrepend += "<p><b>Departure Date</b>\t\t: " + jsClickedFlight.departureDate + "</p>";
	   strPrepend += "<p><b>Booking Class</b>\t\t: " + UI_FLA.jsArrayData[index].bkngClass + "</p>";
		
	var strSubject = UI_FLA.jsArrayData[index].alertType + " in flight " + jsClickedFlight.flightNumber;
   
	var printStr = '<p><b>'+strSubject+'</b></p>'+strPrepend;
	UI_FLA.printtext(printStr);
}

UI_FLA.printtext = function(elem) {
	var popup = window.open('','popup','toolbar=no,menubar=no,width=300,height=200');
	popup.document.open();
	popup.document.write("<html><head></head><body onload='print()'>");
	popup.document.write(elem);
	popup.document.write("</body></html>");
	popup.document.close();
} 

/**
 * Get graph Image from Google
 */
function getGraphImage(strParams) {
	return "http://chart.apis.google.com/chart?" + strParams;
}

/**
 * Before unload close child windows
 */
function beforeUnload() {
	if ((objWindow1) && (!objWindow1.closed)) {
		objWindow1.close();
	}
}

/**
 * Open child windows
 */
function CWindowOpen(strIndex) {
	var intLeft = (window.screen.width - 800) / 2;
	var intTop = (window.screen.height - 620) / 2;
	//TODO :note this url has been hard coded due to url auto replacement from server 
	objWindow1 = window
			.open(
					"../../private/master/showFlaAlertEmail.action",
					"CWindow",
					"toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width=600,height=300,top="
							+ intTop + ",left=" + intLeft);
}

/**
 * Add months to dtDate
 */
function addMonthsLocal(dtDate, intMonth){
	var modDate = new Date(dtDate.getTime());
	var month = modDate.getMonth();
	var modifiedMonth = month + Number(intMonth);
	modDate.setMonth(modifiedMonth);
	return modDate;
}

// ------------------------------------- end of File ---------------------------------
