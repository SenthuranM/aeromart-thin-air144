/* ***************************** */
/* Author: Harsha Ariyarathne    */
/* ***************************** */


function winOnLoad(strMsg, strMsgType) {

	getFieldByID("txtToAddress").focus();
	
	if (strMsg != null && strMsgType != null) {
		showWindowCommonError(strMsgType, strMsg);
	}

}
function sendClicked(){
	if (isEmpty(getText("txtToAddress"))) {

		showWindowCommonError("Error","Email address is required.");
		getFieldByID("txtToAddress").focus();
		return false;
	}
	if (isEmpty(getText("txtSubject"))) {
		showWindowCommonError("Subject is required.");
		getFieldByID("txtSubject").focus();
		return false;
	}
	
	getFieldByID("frmFlaEmail").target="_self";
	getFieldByID("frmFlaEmail").action = "showFlaAlertEmail.action"
	Disable("btnSave",true);
	setField("hdnMode","EMAIL");		
	getFieldByID("frmFlaEmail").submit();
	
	
}