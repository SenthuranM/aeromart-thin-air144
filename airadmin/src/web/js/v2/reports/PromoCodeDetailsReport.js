
function UI_PromoCodeDetailsRpt(){}
var routeArray = [];
UI_PromoCodeDetailsRpt.screenID = "UC_REPM_084";
UI_PromoCodeDetailsRpt.dataMap = {
	FlightNo: [],
}

setField("hdnLive",repLive);

//Onload function
$(function(){
	$("#divPromoCodeDetails").decoratePanel("Promotion Information Report");
	
	$("#departureDateFrm").datepicker({showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', dateFormat: 'dd/mm/yy', buttonImageOnly: true});
	$("#departureDateTo").datepicker({showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', dateFormat: 'dd/mm/yy', buttonImageOnly: true});
	$("#bookedDateFrm").datepicker({showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', dateFormat: 'dd/mm/yy', buttonImageOnly: true, maxDate: new Date()});
	$("#bookedDateTo").datepicker({showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', dateFormat: 'dd/mm/yy', buttonImageOnly: true, maxDate: new Date()});
	
	//setter
	$("#departureDateFrm").blur(function() { UI_PromoCodeDetailsRpt.dateOnBlur({field: 'departureDateFrm'});});
	$("#departureDateTo").blur(function() { UI_PromoCodeDetailsRpt.dateOnBlur({field: 'departureDateTo'});});
	$("#bookedDateFrm").blur(function() { UI_PromoCodeDetailsRpt.dateOnBlur({field: 'bookedDateFrm'});});
	$("#bookedDateTo").blur(function() { UI_PromoCodeDetailsRpt.dateOnBlur({field: 'bookedDateTo'});});
	
	$(".addItem").click(function(e){
		UI_PromoCodeDetailsRpt.addItemsToSelect(e.target.id);
	});
	$(".removeItem").click(function(e){
		UI_PromoCodeDetailsRpt.removeItemsFromSelect(e.target.id);
	});
	
	$("#btnClose").decorateButton();
	$("#btnView").decorateButton();
	$("#btnSched").decorateButton();
	if($("#hdnSchedulePromoReport").val() !== undefined && $("#hdnSchedulePromoReport").val() == "false" ){
		$("#btnSched").hide();
	}
		
	// register event handler for buttons
	$('#btnClose').click(function() { UI_PromoCodeDetailsRpt.closeClicked(); });
	$('#btnView').click(function() { UI_PromoCodeDetailsRpt.viewClicked(); });
	$('#btnSched').click(function() { UI_PromoCodeDetailsRpt.scheduleReport(); });
	
});

/**
 * Auto Date generation
 */
UI_PromoCodeDetailsRpt.dateOnBlur = function(inParam){
	dateChk(inParam.field);
}

UI_PromoCodeDetailsRpt.addItemsToSelect = function(id){
	
	var tID = id.split("_")[1];
	var boolAdd = true;
	switch (tID){
	case "Routes":
	
		var route = getViaPoints();
					
		var appenRouteItem = $("<option>"+route+"</option>").attr("value", route);
		if (route==""){
		//	alert("Select a valied OND");
			boolAdd = false;
			break;
		}
		alloption = $("#sel_"+tID).find("option");
		for(var i = 0; i < alloption.length; i++){
			if (alloption[i].value == route){
			//	alert("Already added!..");
				boolAdd = false;
				break;
			}
		}
		if(boolAdd){
			if(alloption.length > 0 && alloption[0].value==""){ 
				$("#sel_"+tID).empty();
			}
			$("#sel_"+tID).append(appenRouteItem);
		
		
			var routeData={};
			var routeVal='';
			for (var i = 0; i < routeArray.length; i++) {
		       if(i==0){
		    	   routeVal= routeArray[i];
		       }
		       else{
		    	   routeVal=routeVal+","+ routeArray[i];
		       }
		           		        
		    }
			routeData['selectedRoute']=routeVal;
			
			$.ajax({ 
				url : 'loadPromotionFormInitData!getSeletedFlights.action',
				//beforeSend : ShowProgress(),
				data:routeData,
				type : "GET",
				async:false,
				dataType : "json",
				complete:function(response){
					 var response = eval("(" + response.responseText + ")");
					 UI_PromoCodeDetailsRpt.dataMap['FlightNo'] = response.selectedFlights;
					 $("#FlightNo" ).autocomplete({
			            source: UI_PromoCodeDetailsRpt.dataMap['FlightNo']
			         });
					 HideProgress();
				}
			});
		}
		break;
	default:
		UI_PromoCodeDetailsRpt.addItemsFunction(tID, true);
	}
};

UI_PromoCodeDetailsRpt.removeItemsFromSelect = function(id){
	var tID = id.split("_")[1];
	
	alloption = $("#sel_"+tID).find("option");
	if(alloption[0].value!=""){ 
	
		var selItem=$("#sel_"+tID).find("option:selected")[0].value;
		$("#sel_"+tID).find("option:selected").remove();
		var splitItem=selItem.split("/");
		var arrayElement=[];
		for(var i=0;i<splitItem.length-1;i++){
			if(splitItem[i+1]!=""){
				arrayElement[i]="'"+splitItem[i]+"/"+splitItem[i+1]+"'";
			}
		}
		for(var a=0;a<arrayElement.length;a++){
			for(var i=0; i<routeArray.length;i++ )
			{ 
				if(routeArray[i]==arrayElement[a]){
					routeArray.splice(i,1);
					break;
				} 
			}
		}
		var routeVal='';
		for (var i = 0; i < routeArray.length; i++) {
	       if(i==0){
	    	   routeVal= routeArray[i];
	       }
	       else{
	    	   routeVal=routeVal+","+ routeArray[i];
	       }
	           		        
	    }
		var routeData={};
		routeData['selectedRoute']=routeVal;
		
		if(routeVal!=''){
			$.ajax({ 
				url : 'loadPromotionFormInitData!getSeletedFlights.action',
				//beforeSend : ShowProgress(),
				data:routeData,
				type : "GET",
				async:false,
				dataType : "json",
				complete:function(response){
					var response = eval("(" + response.responseText + ")");
					UI_PromoCodeDetailsRpt.dataMap['FlightNo'] = response.selectedFlights;
					$("#FlightNo" ).autocomplete({
						source: UI_PromoCodeDetailsRpt.dataMap['FlightNo']
			        });
					HideProgress();
				}
			});
		}
	}
};

UI_PromoCodeDetailsRpt.addItemsFunction=function(tID, checkList){
	var boolAdd = true;
	var addValue = $("#"+tID).val();
	var appenItem = $("<option>"+addValue+"</option>").attr("value", addValue);
	alloption = $("#sel_"+tID).find("option");
	if (addValue==""){
		boolAdd = false;
	}else if(checkList && $.inArray(addValue ,UI_PromoCodeDetailsRpt.dataMap[tID])<0){
		alert("Not a valid item!..");
		boolAdd = false;
	}
	
	for(var i = 0; i < alloption.length; i++){
		if (alloption[i].value == addValue){
			alert("Already added!..");
			boolAdd = false;
			break;
		}
	}
	
	
	if(boolAdd){
		$("#sel_"+tID).append(appenItem);
		$("#"+tID).val("");
	}
};

/**
 * new click event handler method.
 */
UI_PromoCodeDetailsRpt.closeClicked = function(){
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(UI_PromoCodeDetailsRpt.screenID))) {
		setPageEdited(false);
		top[1].objTMenu.tabRemove(UI_PromoCodeDetailsRpt.screenID);
	}
}

UI_PromoCodeDetailsRpt.viewClicked = function(){
	if(UI_PromoCodeDetailsRpt.validateFields()){
		$("#hdnMode").val("VIEW");
		$("#hdnOnds").val(getSelectBoxData('sel_Routes'));
		$("#hdnFlightNos").val(getSelectBoxData('sel_FlightNo'));
		$("#hdnPromoTypes").val($.trim(lsPromo1.getselectedData()));
		$("#hdnChannels").val($.trim(lspm1.getselectedData()));
		$("#hdnAgents").val(getAgents());
		var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
		top[0].objWindow = window.open("about:blank", "CWindow", strProp);
		var objForm = document.getElementById("frmPromoCodeDetailsReport");
		objForm.target = "CWindow";
		objForm.action = "showPromoCodeDetailsReport.action";
		objForm.submit();
	}
}

UI_PromoCodeDetailsRpt.scheduleReport = function(){
	UI_SchedRept.displayForm({divName:'divSchedFrom', formName:'frmPromoCodeDetailsReport', 
		composerName: 'promotionInfo',
		errorCallback:function(message){
			showERRMessage(message);
		},
		successCallback:function(message){
			showCommonError("Confirmation", message);
		}
	});
}

UI_PromoCodeDetailsRpt.validateFields = function(){
	var deptFromDate = $("#departureDateFrm").val();
	var deptToDate = $("#departureDateTo").val();
	
	var deptDateRst = dateValidation(deptFromDate, deptToDate, "#departureDateFrm", "#departureDateTo", true, true);
	var bkdateRst = true;
	if(deptDateRst){
		var bookedFromDate = $("#bookedDateFrm").val();
		var bookedToDate = $("#bookedDateTo").val();
		bkdateRst = dateValidation(bookedFromDate, bookedToDate, "#bookedDateFrm", "#bookedDateTo", false, false);
	}	
	
	return deptDateRst && bkdateRst;
	
}

dateValidation = function(fromDate, toDate, fromField, toField, fromReq, toReq){
	if(fromReq){
		if (fromDate == ""){
			showCommonError("Error", fromDtEmpty);
			return false;
		}
	}
	
	if(fromDate != ''){
		if ( !dateValidDate(fromDate) ) {
			showCommonError("Error", fromDtInvalid);
			$(fromField).val("");
			$(fromField).focus();
			return false;
		}
	}
	
	if(toReq){
		if (toDate == ""){
			showCommonError("Error", toDtEmpty);
			return false;
		}
	}
	if(toDate != ''){		
		if ( !dateValidDate(toDate) ) {
			showCommonError("Error", toDtinvalid);
			$(toField).val("");
			$(toField).focus();
			return false;
		}
	}
	
	if(fromDate != '' && toDate != ''){		
		if (!(fromDate == toDate)) {
			if (!CheckDates(fromDate, toDate)) {
				showCommonError("Error", fromDtExceed);
				return false;
			}
			var isSkipPeriod = false;
			if($("#hdnSkipPeriod").val() !== undefined && $("#hdnSkipPeriod").val() == "true" ){
				isSkipPeriod = true;
			}
			if( !isSkipPeriod && exceedsOneMonth(fromDate,toDate)) {
				showCommonError("Error", exceedOneMonth);
				$(toField).focus();
				return false;
			}
		}
	}
	
	return true;
}

changeAgencies = function() {
	ls.clear();
}

getAgents = function() {
	var strAgents = ls.getSelectedDataWithGroup();
	var newAgents;
	if (strAgents.indexOf(":") != -1) {
		strAgents = replaceall(strAgents, ":", ",");
	}
	if (strAgents.indexOf("|") != -1) {
		newAgents = replaceall(strAgents, "|", ",");
	} else {
		newAgents = strAgents
	}
	return newAgents;
}

setPageEdited = function(isEdited) {
	top[1].objTMenu.tabPageEdited(UI_PromoCodeDetailsRpt.screenID, isEdited);
}

getSelectBoxData = function(selectBoxID){
	var selector="#"+selectBoxID+" option"
	var selectedItems=[];
	$(selector).each(function () {
		if(this.value!=""){
			selectedItems.push(this.value);
		}
	});
	return JSON.stringify(selectedItems);
}

getSelectedSelectBoxData = function(selectBoxID) {
	var selector="#"+selectBoxID+" option:selected"
	var selectedItems=[];
	$(selector).each(function () {
		selectedItems.push(this.value);
	});
	return JSON.stringify(selectedItems);
}

getViaPoints = function(){
	top[2].HidePageMessage();
	var str = "";
	var dept = $("#selFrom_1 option:selected").text();
	var arr = $("#selTo_1 option:selected").text();
	var via1 = $("#selVia1 option:selected").text();
	var via2 = $("#selVia2 option:selected").text();
	var via3 = $("#selVia3 option:selected").text();
	var via4 = $("#selVia4 option:selected").text();

	var deptVal = $("#selFrom_1").val();
	var arrVal = $("#selTo_1").val();
	var via1Val = $("#selVia1").val();
	var via2Val = $("#selVia2").val();
	var via3Val = $("#selVia3").val();
	var via4Val = $("#selVia4").val();

	if (dept == "") {
		showERRMessage(depatureRqrd);
		$("#selFrom_1").focus();

	} else if (dept != "" && deptVal == "INA") {
		showERRMessage(departureInactive);
		$("#selFrom_1").focus();

	} else if (arr == "") {
		showERRMessage(arrivalRqrd);
		$("#selTo_1").focus();

	} else if (arr != "" && arrVal == "INA") {
		showERRMessage(arrivalInactive);
		$("#selTo_1").focus();

	} else if (dept == arr && (via1 == "" && via2 == "" && via3 == "" && via4 == "" )) {		
		showERRMessage(depatureArriavlSame);
		$("#selTo_1").focus();
	} else if (arr == via1 || arr == via2 || arr == via3 || arr == via4) {
		showERRMessage(arriavlViaSame);
		$("#selTo_1").focus();

	} else if (dept == via1 || dept == via2 || dept == via3 || dept == via4) {
		showERRMessage(depatureViaSame);
		$("#selFrom_1").focus();

	} else if ((via2 != "" || via3 != "" || via4 != "") && via1 == "") {
		showERRMessage(vianotinSequence);
		$("#selVia1").focus();

	} else if ((via3 != "" || via4 != "") && via2 == "") {
		showERRMessage(vianotinSequence);
		$("#selVia1").focus();

	} else if ((via4 != "") && via3 == "") {
		showERRMessage(vianotinSequence);
		$("#selVia1").focus();

	} else if ((via1 != "" && via2 != "" && via3 != "")
			&& (via1 == via2 || via1 == via3 || via1 == via4)) {
		showERRMessage(viaEqual);
		$("#selVia1").focus();

	} else if ((via1 != "" && via2 != "" && via3 != "")
			&& (via2 == via3 || via2 == via4)) {
		showERRMessage(viaEqual);
		$("#selVia2").focus();

	} else if ((via1 != "" && via2 != "" && via3 != "") && (via3 == via4)) {
		showERRMessage(viaEqual);
		$("#selVia4").focus();

	} else if ((via1 != "" && via2 != "") && via1 == via2) {
		showERRMessage(viaEqual);
		$("#selVia1").focus();

	} else if (via1 != "" && via1Val == "INA") {
		showERRMessage(via1 + " " + viaInactive);
		$("#selVia1").focus();
	} else if (via2 != "" && via2Val == "INA") {
		showERRMessage(via2 + " " + viaInactive);
		$("#selVia2").focus();
	} else if (via3 != "" && via3Val == "INA") {
		showERRMessage(via3 + " " + viaInactive);
		$("#selVia3").focus();
	} else if (via4 != "" && via4Val == "INA") {
		showERRMessage(via4 + " " + viaInactive);
		$("#selVia4").focus();
	} else {
		
		str += dept;
	
		if (via1 != "") {
			str += "/" + via1;
			var routeToAdd="'"+dept+"/"+via1+"'"
			routeArray.push(routeToAdd);
		}
		if (via2 != "") {
			str += "/" + via2;
			var routeToAdd="'"+via1+"/"+via2+"'"
			routeArray.push(routeToAdd);
		}
		if (via3 != "") {
			str += "/" + via3;
			var routeToAdd="'"+var2+"/"+via3+"'"
			routeArray.push(routeToAdd);
		}
		if (via4 != "") {
			str += "/" + via4;
			var routeToAdd="'"+var3+"/"+via4+"'"
			routeArray.push(routeToAdd);
		}
		str += "/" + arr;
		
		if(via1 ==""&& via2 ==""&&via3 ==""&&via4 ==""){
			var routeToAdd="'"+dept+"/"+arr+"'"
			routeArray.push(routeToAdd);
		}
		else if(via2 ==""&&via3 ==""&&via4 ==""){
				var routeToAdd="'"+via1+"/"+arr+"'"
				routeArray.push(routeToAdd);
		}
		else if(via3 ==""&&via4 ==""){
			var routeToAdd="'"+via2+"/"+arr+"'"
			routeArray.push(routeToAdd);
		}
		else if(via4 ==""){
			var routeToAdd="'"+via3+"/"+arr+"'"
			routeArray.push(routeToAdd);
		}
		else{
			var routeToAdd="'"+via4+"/"+arr+"'"
			routeArray.push(routeToAdd);
		}
		
	}
	return str;
};