
function UI_BundledFareRpt(){}

UI_BundledFareRpt.screenID = "UC_REPM_086";

setField("hdnLive",repLive);

//Onload function
$(function(){
	$("#divBundledFareReport").decoratePanel("Bundled Service Report");
	
	$("#salesDateFrm").datepicker({showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', dateFormat: 'dd/mm/yy', buttonImageOnly: true, maxDate: new Date()});
	$("#salesDateTo").datepicker({showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', dateFormat: 'dd/mm/yy', buttonImageOnly: true, maxDate: new Date()});
	$("#flightDateFrm").datepicker({showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', dateFormat: 'dd/mm/yy', buttonImageOnly: true});
	$("#flightDateTo").datepicker({showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', dateFormat: 'dd/mm/yy', buttonImageOnly: true});
	
	//setter
	$("#salesDateFrm").blur(function() { UI_BundledFareRpt.dateOnBlur({field: 'salesDateFrm'});});
	$("#salesDateTo").blur(function() { UI_BundledFareRpt.dateOnBlur({field: 'salesDateTo'});});
	$("#flightDateFrm").blur(function() { UI_BundledFareRpt.dateOnBlur({field: 'flightDateFrm'});});
	$("#flightDateTo").blur(function() { UI_BundledFareRpt.dateOnBlur({field: 'flightDateTo'});});
	
	$(".addItem").click(function(e){
		UI_BundledFareRpt.addItemsToSelect(e.target.id);
	});
	$(".removeItem").click(function(e){
		UI_BundledFareRpt.removeItemsFromSelect(e.target.id);
	});
	
	$("#btnClose").decorateButton();
	$("#btnView").decorateButton();
		
	// register event handler for buttons
	$('#btnClose').click(function() { UI_BundledFareRpt.closeClicked(); });
	$('#btnView').click(function() { UI_BundledFareRpt.viewClicked(); });
});

/**
 * Auto Date generation
 */
UI_BundledFareRpt.dateOnBlur = function(inParam){
	dateChk(inParam.field);
}

/**
 * close click event handler method.
 */
UI_BundledFareRpt.closeClicked = function(){
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(UI_BundledFareRpt.screenID))) {
		setPageEdited(false);
		top[1].objTMenu.tabRemove(UI_BundledFareRpt.screenID);
	}
}

UI_BundledFareRpt.viewClicked = function(){
	if(UI_BundledFareRpt.validateFields()){
		$("#hdnMode").val("VIEW");
		$("#hdnChannels").val($.trim(lspm1.getselectedData()));
		$("#hdnBundledFares").val($.trim(lsBundledFare.getselectedData()));
		$("#hdnSectors").val(getSelectBoxData('sel_Routes'));
		var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
		top[0].objWindow = window.open("about:blank", "CWindow", strProp);
		var objForm = document.getElementById("frmBundledFareReport");
		objForm.target = "CWindow";
		objForm.action = "showBundledFareReport.action";
		objForm.submit();
	}
}

UI_BundledFareRpt.validateFields = function(){
	var salesFromDate = $("#salesDateFrm").val();
	var salesToDate = $("#salesDateTo").val();
	
	var salesDateRst = dateValidation(salesFromDate, salesToDate, "#salesDateFrm", "#salesDateTo", true, true);
	
	if(exceedsOneMonth(salesFromDate,salesToDate)) {
		showCommonError("Error", exceedOneMonth);
		$("#salesDateTo").focus();
		salesDateRst = false;
	}
	
	var fltDateRst = true;
	if(salesDateRst){
		var flightFromDate = $("#flightDateFrm").val();
		var flightToDate = $("#flightDateTo").val();
		fltDateRst = dateValidation(flightFromDate, flightToDate, "#flightDateFrm", "#flightDateTo", false, false);
		
		if(flightFromDate != '' && flightToDate != '' && exceedsOneMonth(flightFromDate,flightToDate)) {
			showCommonError("Error", exceedOneMonth);
			$("#flightDateTo").focus();
			fltDateRst = false;
		}
	}	
	
	return salesDateRst && fltDateRst;
	
}

UI_BundledFareRpt.addItemsToSelect = function(id){
	
	var tID = id.split("_")[1];
	var boolAdd = true;
	var route = getViaPoints();
	
	var appenRouteItem = $("<option>"+route+"</option>").attr("value", route);
	if (route != ""){		
		alloption = $("#sel_"+tID).find("option");
		for(var i = 0; i < alloption.length; i++){
			if (alloption[i].value == route){
				boolAdd = false;
				break;
			}
		}
		if(boolAdd){
			if(alloption.length > 0 && alloption[0].value==""){ 
				$("#sel_"+tID).empty();
			}
			$("#sel_"+tID).append(appenRouteItem);
		}
	}
};

UI_BundledFareRpt.removeItemsFromSelect = function(id){
	var tID = id.split("_")[1];
	
	alloption = $("#sel_"+tID).find("option");
	if(alloption[0].value!=""){ 
	
		var selItem=$("#sel_"+tID).find("option:selected")[0].value;
		$("#sel_"+tID).find("option:selected").remove();
		var splitItem=selItem.split("/");
		var arrayElement=[];
		for(var i=0;i<splitItem.length-1;i++){
			if(splitItem[i+1]!=""){
				arrayElement[i]="'"+splitItem[i]+"/"+splitItem[i+1]+"'";
			}
		}
		for(var a=0;a<arrayElement.length;a++){
			for(var i=0; i<routeArray.length;i++ )
			{ 
				if(routeArray[i]==arrayElement[a]){
					routeArray.splice(i,1);
					break;
				} 
			}
		}
		var routeVal='';
		for (var i = 0; i < routeArray.length; i++) {
	       if(i==0){
	    	   routeVal= routeArray[i];
	       }
	       else{
	    	   routeVal=routeVal+","+ routeArray[i];
	       }
	           		        
	    }
	}
};