
function UI_JNTaxRpt(){}
UI_JNTaxRpt.screenID = "UC_REPM_085";

setField("hdnLive",repLive);

//Onload function
$(function(){
	$("#divJNTaxReport").decoratePanel("JN Tax Report");
	
	$("#paymentDateFrm").datepicker({showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', dateFormat: 'dd/mm/yy', buttonImageOnly: true, maxDate: new Date()});
	$("#paymentDateTo").datepicker({showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', dateFormat: 'dd/mm/yy', buttonImageOnly: true, maxDate: new Date()});
	$("#bookedDateFrm").datepicker({showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', dateFormat: 'dd/mm/yy', buttonImageOnly: true, maxDate: new Date()});
	$("#bookedDateTo").datepicker({showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', dateFormat: 'dd/mm/yy', buttonImageOnly: true, maxDate: new Date()});
	
	//setter
	$("#paymentDateFrm").blur(function() { UI_JNTaxRpt.dateOnBlur({field: 'paymentDateFrm'});});
	$("#paymentDateTo").blur(function() { UI_JNTaxRpt.dateOnBlur({field: 'paymentDateTo'});});
	$("#bookedDateFrm").blur(function() { UI_JNTaxRpt.dateOnBlur({field: 'bookedDateFrm'});});
	$("#bookedDateTo").blur(function() { UI_JNTaxRpt.dateOnBlur({field: 'bookedDateTo'});});
	
	$("#btnClose").decorateButton();
	$("#btnView").decorateButton();
		
	// register event handler for buttons
	$('#btnClose').click(function() { UI_JNTaxRpt.closeClicked(); });
	$('#btnView').click(function() { UI_JNTaxRpt.viewClicked(); });
});

/**
 * Auto Date generation
 */
UI_JNTaxRpt.dateOnBlur = function(inParam){
	dateChk(inParam.field);
}

/**
 * close click event handler method.
 */
UI_JNTaxRpt.closeClicked = function(){
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(UI_JNTaxRpt.screenID))) {
		setPageEdited(false);
		top[1].objTMenu.tabRemove(UI_JNTaxRpt.screenID);
	}
}

UI_JNTaxRpt.viewClicked = function(){
	if(UI_JNTaxRpt.validateFields()){
		$("#hdnMode").val("VIEW");
		$("#hdnChannels").val($.trim(lspm1.getselectedData()));
		$("#hdnAgents").val(getAgents());
		var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
		top[0].objWindow = window.open("about:blank", "CWindow", strProp);
		var objForm = document.getElementById("frmJNTaxReport");
		objForm.target = "CWindow";
		objForm.action = "showJNTaxReport.action";
		objForm.submit();
	}
}

UI_JNTaxRpt.validateFields = function(){
	var deptFromDate = $("#paymentDateFrm").val();
	var deptToDate = $("#paymentDateTo").val();
	
	var deptDateRst = dateValidation(deptFromDate, deptToDate, "#paymentDateFrm", "#paymentDateTo", true, true);
	var bkdateRst = true;
	if(deptDateRst){
		var bookedFromDate = $("#bookedDateFrm").val();
		var bookedToDate = $("#bookedDateTo").val();
		bkdateRst = dateValidation(bookedFromDate, bookedToDate, "#bookedDateFrm", "#bookedDateTo", false, false);
	}	
	
	return deptDateRst && bkdateRst;
	
}
changeAgencies = function() {
	ls.clear();
}

getAgents = function() {
	var strAgents = ls.getSelectedDataWithGroup();
	var newAgents;
	if (strAgents.indexOf(":") != -1) {
		strAgents = replaceall(strAgents, ":", ",");
	}
	if (strAgents.indexOf("|") != -1) {
		newAgents = replaceall(strAgents, "|", ",");
	} else {
		newAgents = strAgents
	}
	return newAgents;
}

setPageEdited = function(isEdited) {
	top[1].objTMenu.tabPageEdited(UI_JNTaxRpt.screenID, isEdited);
}

dateValidation = function(fromDate, toDate, fromField, toField, fromReq, toReq){
	if(fromReq){
		if (fromDate == ""){
			showCommonError("Error", fromDtEmpty);
			return false;
		}
	}
	
	if(fromDate != ''){
		if ( !dateValidDate(fromDate) ) {
			showCommonError("Error", fromDtInvalid);
			$(fromField).val("");
			$(fromField).focus();
			return false;
		}
	}
	
	if(toReq){
		if (toDate == ""){
			showCommonError("Error", toDtEmpty);
			return false;
		}
	}
	if(toDate != ''){		
		if ( !dateValidDate(toDate) ) {
			showCommonError("Error", toDtinvalid);
			$(toField).val("");
			$(toField).focus();
			return false;
		}
	}
	
	if(fromDate != '' && toDate != ''){		
		if (!(fromDate == toDate)) {
			if (!CheckDates(fromDate, toDate)) {
				showCommonError("Error", fromDtExceed);
				return false;
			}
			
			if(exceedsOneMonth(fromDate,toDate)) {
				showCommonError("Error", exceedOneMonth);
				$(toField).focus();
				return false;
			}
		}
	}
	
	return true;
}