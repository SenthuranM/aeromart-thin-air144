function PaxStatusReport(){
    this.ready = function(){

        $("#divPaxStatusReport").decoratePanel("Pax Status And Revenue Report");
        $("#txtFromDate").datepicker({showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', dateFormat: 'dd/mm/yy', buttonImageOnly: true});
        $("#txtToDate").datepicker({showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', dateFormat: 'dd/mm/yy', buttonImageOnly: true});
        $("#txtBookFromDate").datepicker({showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', dateFormat: 'dd/mm/yy', buttonImageOnly: true});
        $("#txtBookToDate").datepicker({showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', dateFormat: 'dd/mm/yy', buttonImageOnly: true});
        $("#btnGetAgent").click(function(){getAgentClick()});
        $("#btnClose").click(function(){closeClick()});
        $("#btnView").click(function(){viewReport()});
        $("#txtFromDate, #txtToDate","#txtBookFromDate","#txtBookToDate").blur(function(){dateChk(this.id)});
        init();
    }

 
    function init(){
        if (top[1].objTMenu.tabGetValue(UI_PaxStatusReport.screenID) != ""
            && top[1].objTMenu.tabGetValue(UI_PaxStatusReport.screenID) != null) {

            var strSearch = top[1].objTMenu.tabGetValue(UI_PaxStatusReport.screenID).split("#");
            $("#txtFromDate").val(strSearch[0]);
            $("#txtToDate").val(strSearch[1]);
            $("#txtBookFromDate").val(strSearch[2]);
            $("#txtBookToDate").val(strSearch[3]);
            $("#pfsStatus").val(strSearch[4]);
            $("#bookingClass").val(strSearch[5]);
            $("#fareBasis").val(strSearch[6]);
            $("#fareRule").val(strSearch[7]);
            $("#origin").val(strSearch[8]);
            $("#destination").val(strSearch[9]);
            $("#selAgencies").val(strSearch[10]);
            $("#flightNo").val(strSearch[11]);
            ls.selectedData(strSearch[12]);
            if(strSearch[13] =='true'){
            	$("#chkLocalTime").attr('checked', true);

            }
        }
    }

    function viewReport(){
        if(validateForm()){
            $("#hdnMode").val("VIEW");
            if (displayAgencyMode == 1 || displayAgencyMode == 2) {
            	$("#hdnAgents").val(getAgents());
        	} else {
        		$("#hdnAgents").val("");
        	}
            var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10';
            top[0].objWindow = window.open("about:blank", "CWindow", strProp);
            var objForm = document.getElementById("frmPage");
            objForm.target = "CWindow";
            objForm.action = "loadPaxStatusRevenueReport.action";
            objForm.submit();
        }
    }

    function validateForm(){
        var deptFromDate = $("#txtFromDate").val();
        var deptToDate = $("#txtToDate").val();
        var bookFromDate = $("#txtBookFromDate").val();
        var bookToDate = $("#txtBookToDate").val();
        var fltNo= $("#flightNo").val();
        var ori= $("#origin").val();
        var dest= $("#destination").val();


        if (deptFromDate == ""){
            showCommonError("Error", fromDtEmpty);
            $("#txtFromDate").focus();
            return false;
        }
        
        if (deptToDate == ""){
            showCommonError("Error", toDtEmpty);
            $("#txtToDate").focus();
            return false;
        }


        if(deptFromDate != ''){
            if ( !dateValidDate(deptFromDate) ) {
                showCommonError("Error", fromDtEmpty);
                $("#txtFromDate").focus();
                return false;
            }
        }
        
        if(deptToDate != ''){
            if ( !dateValidDate(deptToDate) ) {
                showCommonError("Error", toDtEmpty);
                $("#txtToDate").focus();
                return false;
            }
        }
        
        if(bookFromDate != ''){
            if ( !dateValidDate(bookFromDate) ) {
                showCommonError("Error", bookedFromDtInvalid);
                $("#txtBookFromDate").focus();
                return false;
            }
        }
        
        if(bookToDate != ''){
            if ( !dateValidDate(bookToDate) ) {
                showCommonError("Error", bookedToDtInvalid);
                $("#txtBookToDate").focus();
                return false;
            }
        }
        
        if((bookFromDate !='' && bookToDate =='') || (bookToDate !='' && bookFromDate =='')){
        	showCommonError("Error", dateRangeRequired);
        	$("#txtBookFromDate").focus();
        	return false;
        }
        
        if(!CheckDates(deptFromDate,deptToDate)) {
    		showCommonError("Error",fromDtExceed);
    		 $("#txtFromDate").focus();
    		return false;
        }
        
        if((bookFromDate !='' && bookToDate !='') && (!CheckDates(bookFromDate,bookToDate))){
        	showCommonError("Error",fromDtExceed);
        	 $("#txtBookFromDate").focus();
        	return false;
        }

        if((dest !='' && ori !='') && (dest == ori)){
            showCommonError("Error", depatureArriavlSame);
            $("#destination").val("");
            $("#destination").focus();
            return false;
        }
     
        return true;
    }

    function getAgents() {
        var strAgents = ls.getSelectedDataWithGroup();
        var newAgents;
        if (strAgents.indexOf(":") != -1) {
            strAgents = replaceall(strAgents, ":", ",");
        }
        if (strAgents.indexOf("|") != -1) {
            newAgents = replaceall(strAgents, "|", ",");
        } else {
            newAgents = strAgents
        }
        return newAgents;
    }

    function closeClick() {
        if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(UI_PaxStatusReport.screenID))) {
            setPageEdited(false);
            top[1].objTMenu.tabRemove(UI_PaxStatusReport.screenID);
        }
    }

    function setPageEdited(isEdited) {
        top[1].objTMenu.tabPageEdited(UI_PaxStatusReport.screenID, isEdited);
    }

    function getAgentClick() {
        if ($("#selAgencies").val() == "") {
         showCommonError("Error", agentTypeRqrd);
         $("#selAgencies").focus();
         } else {
         var strSearchCriteria = $("#txtFromDate").val() + "#"
         + $("#txtToDate").val() + "#"
         + $("#txtBookFromDate").val() + "#"
         + $("#txtBookToDate").val() + "#"
         + $("#pfsStatus").val() + "#" + $("#bookingClass").val() + "#"
         + $("#fareBasis").val() + "#" + $("#fareRule").val() + "#"
         + $("#origin").val() + "#" + $("#destination").val() + "#"
         + $("#selAgencies").val() + "#" +  $("#flightNo").val() + "#" + ls.getselectedData() + "#" + $("#chkLocalTime").attr('checked');

         top[1].objTMenu.tabSetValue(UI_PaxStatusReport.screenID, strSearchCriteria);
         $("#hdnAgents").val("");
         $("#hdnMode").val("SEARCH");
         var objForm = document.getElementById("frmPage");
         objForm.target = "_self";
         document.forms[0].submit();
         top[2].ShowProgress();
         }
    }
}
var agts = [];
setField("hdnLive",repLive);
var UI_PaxStatusReport = new PaxStatusReport();
UI_PaxStatusReport.screenID = "UC_REPM_087";
UI_PaxStatusReport.ready();