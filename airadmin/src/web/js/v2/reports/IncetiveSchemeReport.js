	
	UI_IncentiveRpt.screenID = "UC_REPM_054";
	
	var arrGroup2 = new Array();
	var arrData2 = new Array();
	arrData2[0] = new Array();
	var lsbx = new Listbox('lstSchemes', 'lstAssignedSchemes', 'spnIncentive','lsbx');
	lsbx.group1 = schemes;
	lsbx.height = '200px'; 
	lsbx.width = '300px';
	lsbx.headingLeft = '&nbsp;&nbsp;All Schemes';
	lsbx.headingRight = '&nbsp;&nbsp;Selected Schemes';
	lsbx.drawListBox();
	
	/**
	 * Onload function
	 */
	$(function() {
		
		$("#searchDateFrm").datepicker({showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', dateFormat: 'dd/mm/yy', buttonImageOnly: true});
		$("#searchDateTo").datepicker({showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', dateFormat: 'dd/mm/yy', buttonImageOnly: true});
				
		//setter
		$("#searchDateFrm").blur(function() { UI_IncentiveRpt.dateOnBlur({id:0});});
		$("#searchDateTo").blur(function() { UI_IncentiveRpt.dateOnBlur({id:1});});
		
		$("#divIncentiveRpt").decoratePanel("Incentive Scheme Report");
		
		$("#btnClose").decorateButton();
		$("#btnView").decorateButton();
			
		// register event handler for buttons
		$('#btnClose').click(function() { UI_IncentiveRpt.closeClicked(); });
		$('#btnView').click(function() { UI_IncentiveRpt.viewClicked(); });
			
	});
	
	
	/**
	 * UI_IncentiveRpt for IncentiveReport UI related functionalities
	 */
	function UI_IncentiveRpt(){}
		
	/**
	 * new click event handler method.
	 */
	UI_IncentiveRpt.closeClicked = function(){
		top.LoadHome();
	}
	
	/**
	 * view click event handler method.
	 */
	UI_IncentiveRpt.viewClicked = function(){
		if(UI_IncentiveRpt.validateFields()){
			setField("hdnMode", "VIEW");
			var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
			top[0].objWindow = window.open("about:blank", "CWindow", strProp);
			var objForm = document.getElementById("frmIncentiveSchRpt");
			objForm.target = "CWindow";
			objForm.action = "showIncentiveSchemeReport.action";
			objForm.submit();
		}
	}
	
	/**
	 * validate fields
	 */
	UI_IncentiveRpt.validateFields = function(){
		var selSchemes = lsbx.getselectedData();		
		
		if(selSchemes == ""){
			return false;
		}else if($("#searchDateFrm").val() == ""){
			return false;
		}else if(!dateValidDate($("#searchDateFrm").val())){
			return false;
		}else if($("#searchDateTo").val() == ""){
			return false;
		}else if(!dateValidDate($("#searchDateTo").val())){
			return false;
		}else{
			setField("hdnIncentives", selSchemes);
			return true;
		}//else if($("#txtSchemeName").val() == ""){
		//	return false;
		//}else if($("#txtSchemeName").val() == ""){
		//	return false;
		//}
		
	}
	
	/**
	 * Auto Date generation
	 */
	UI_IncentiveRpt.dateOnBlur = function(inParam){
		switch (inParam.id){
			case 0 : dateChk("searchDateFrm"); break;
			case 1 : dateChk("searchDateTo"); break;
			}
	}