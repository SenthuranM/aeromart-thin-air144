// Include all common methods related to report front end

setPageEdited = function(isEdited) {
	top[1].objTMenu.tabPageEdited(UI_BundledFareRpt.screenID, isEdited);
}

dateValidation = function(fromDate, toDate, fromField, toField, fromReq, toReq){
	if(fromReq){
		if (fromDate == ""){
			showCommonError("Error", fromDtEmpty);
			return false;
		}
	}
	
	if(fromDate != ''){
		if ( !dateValidDate(fromDate) ) {
			showCommonError("Error", fromDtInvalid);
			$(fromField).val("");
			$(fromField).focus();
			return false;
		}
	}
	
	if(toReq){
		if (toDate == ""){
			showCommonError("Error", toDtEmpty);
			return false;
		}
	}
	if(toDate != ''){		
		if ( !dateValidDate(toDate) ) {
			showCommonError("Error", toDtinvalid);
			$(toField).val("");
			$(toField).focus();
			return false;
		}
	}
	
	if(fromDate != '' && toDate != ''){		
		if (!(fromDate == toDate)) {
			if (!CheckDates(fromDate, toDate)) {
				showCommonError("Error", fromDtExceed);
				return false;
			}
		}
	}
	
	return true;
}

getSelectBoxData = function(selectBoxID){
	var selector="#"+selectBoxID+" option"
	var selectedItems=[];
	$(selector).each(function () {
		if(this.value!=""){
			selectedItems.push(this.value);
		}
	});
	return JSON.stringify(selectedItems);
}

getSelectedSelectBoxData = function(selectBoxID) {
	var selector="#"+selectBoxID+" option:selected"
	var selectedItems=[];
	$(selector).each(function () {
		selectedItems.push(this.value);
	});
	return JSON.stringify(selectedItems);
}

getViaPoints = function(){
	top[2].HidePageMessage();
	var str = "";
	var dept = $("#selFrom_1 option:selected").text();
	var arr = $("#selTo_1 option:selected").text();
	var via1 = $("#selVia1 option:selected").text();
	var via2 = $("#selVia2 option:selected").text();
	var via3 = $("#selVia3 option:selected").text();
	var via4 = $("#selVia4 option:selected").text();

	var deptVal = $("#selFrom_1").val();
	var arrVal = $("#selTo_1").val();
	var via1Val = $("#selVia1").val();
	var via2Val = $("#selVia2").val();
	var via3Val = $("#selVia3").val();
	var via4Val = $("#selVia4").val();

	if (dept == "") {
		showERRMessage(depatureRqrd);
		$("#selFrom_1").focus();

	} else if (dept != "" && deptVal == "INA") {
		showERRMessage(departureInactive);
		$("#selFrom_1").focus();

	} else if (arr == "") {
		showERRMessage(arrivalRqrd);
		$("#selTo_1").focus();

	} else if (arr != "" && arrVal == "INA") {
		showERRMessage(arrivalInactive);
		$("#selTo_1").focus();

	} else if (dept == arr && (via1 == "" && via2 == "" && via3 == "" && via4 == "" )) {		
		showERRMessage(depatureArriavlSame);
		$("#selTo_1").focus();
	} else if (arr == via1 || arr == via2 || arr == via3 || arr == via4) {
		showERRMessage(arriavlViaSame);
		$("#selTo_1").focus();

	} else if (dept == via1 || dept == via2 || dept == via3 || dept == via4) {
		showERRMessage(depatureViaSame);
		$("#selFrom_1").focus();

	} else if ((via2 != "" || via3 != "" || via4 != "") && via1 == "") {
		showERRMessage(vianotinSequence);
		$("#selVia1").focus();

	} else if ((via3 != "" || via4 != "") && via2 == "") {
		showERRMessage(vianotinSequence);
		$("#selVia1").focus();

	} else if ((via4 != "") && via3 == "") {
		showERRMessage(vianotinSequence);
		$("#selVia1").focus();

	} else if ((via1 != "" && via2 != "" && via3 != "")
			&& (via1 == via2 || via1 == via3 || via1 == via4)) {
		showERRMessage(viaEqual);
		$("#selVia1").focus();

	} else if ((via1 != "" && via2 != "" && via3 != "")
			&& (via2 == via3 || via2 == via4)) {
		showERRMessage(viaEqual);
		$("#selVia2").focus();

	} else if ((via1 != "" && via2 != "" && via3 != "") && (via3 == via4)) {
		showERRMessage(viaEqual);
		$("#selVia4").focus();

	} else if ((via1 != "" && via2 != "") && via1 == via2) {
		showERRMessage(viaEqual);
		$("#selVia1").focus();

	} else if (via1 != "" && via1Val == "INA") {
		showERRMessage(via1 + " " + viaInactive);
		$("#selVia1").focus();
	} else if (via2 != "" && via2Val == "INA") {
		showERRMessage(via2 + " " + viaInactive);
		$("#selVia2").focus();
	} else if (via3 != "" && via3Val == "INA") {
		showERRMessage(via3 + " " + viaInactive);
		$("#selVia3").focus();
	} else if (via4 != "" && via4Val == "INA") {
		showERRMessage(via4 + " " + viaInactive);
		$("#selVia4").focus();
	} else {
		
		str += dept;
	
		if (via1 != "") {
			str += "/" + via1;
			var routeToAdd="'"+dept+"/"+via1+"'"
		}
		if (via2 != "") {
			str += "/" + via2;
			var routeToAdd="'"+via1+"/"+via2+"'"
		}
		if (via3 != "") {
			str += "/" + via3;
			var routeToAdd="'"+var2+"/"+via3+"'"
		}
		if (via4 != "") {
			str += "/" + via4;
			var routeToAdd="'"+var3+"/"+via4+"'"
		}
		str += "/" + arr;
		
		if(via1 ==""&& via2 ==""&&via3 ==""&&via4 ==""){
			var routeToAdd="'"+dept+"/"+arr+"'"
		}
		else if(via2 ==""&&via3 ==""&&via4 ==""){
			var routeToAdd="'"+via1+"/"+arr+"'"
		}
		else if(via3 ==""&&via4 ==""){
			var routeToAdd="'"+via2+"/"+arr+"'"
		}
		else if(via4 ==""){
			var routeToAdd="'"+via3+"/"+arr+"'"
		}
		else{
			var routeToAdd="'"+via4+"/"+arr+"'"
		}
		
	}
	return str;
}