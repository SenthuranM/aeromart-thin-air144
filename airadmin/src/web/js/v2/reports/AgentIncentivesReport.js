	
	UI_AgentIncentiveRpt.screenID = "UC_REPM_055";
	var arrGroup2 = new Array();
	var arrData2 = new Array();
	arrData2[0] = new Array();	
	 
	var lsbx = new Listbox('lstSchemes', 'lstAssignedSchemes', 'spnIncentive','lsbx');
	lsbx.group1 = schemes;
	lsbx.height = '160px'; 
	lsbx.width = '300px';
	lsbx.headingLeft = '&nbsp;&nbsp;All Schemes';
	lsbx.headingRight = '&nbsp;&nbsp;Selected Schemes';
	lsbx.drawListBox();
	
	var ls = new Listbox('lstRoles', 'lstAssignedRoles', 'spn1','ls');
	ls.group1 = stns;ls.list1 = agentsArr;ls.height = '140px';
	 ls.width = '350px';ls.headingLeft = '&nbsp;&nbsp;All Agents';
	 ls.headingRight = '&nbsp;&nbsp;Selected Agents';
	 ls.drawListBox();
	
	/**
	 * Onload function
	 */
	$(function() {
		$("#searchDateFrm").datepicker({showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', dateFormat: 'dd/mm/yy', buttonImageOnly: true});
		$("#searchDateTo").datepicker({showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', dateFormat: 'dd/mm/yy', buttonImageOnly: true});
		
		//setter
		$("#searchDateFrm").blur(function() { UI_AgentIncentiveRpt.dateOnBlur({id:0});});
		$("#searchDateTo").blur(function() { UI_AgentIncentiveRpt.dateOnBlur({id:1});});
		
		$("#divAgentIncentive").decoratePanel("Incentive Scheme Report");
		
		$("#btnClose").decorateButton();
		$("#btnView").decorateButton();
			
		// register event handler for buttons
		$('#btnClose').click(function() { UI_AgentIncentiveRpt.closeClicked(); });
		$('#btnView').click(function() { UI_AgentIncentiveRpt.viewClicked(); });
			
	});
	
	
	/**
	 * UI_IncentiveRpt for AgentIncentiveReport UI related functionalities
	 */
	function UI_AgentIncentiveRpt(){}
		
	/**
	 * new click event handler method.
	 */
	UI_AgentIncentiveRpt.closeClicked = function(){
		top.LoadHome();
	}
	
	/**
	 * cancel click event handler method.
	 */
	UI_AgentIncentiveRpt.viewClicked = function(){
		if(UI_AgentIncentiveRpt.validateFields()){
			setField("hdnMode", "VIEW");
			var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
			top[0].objWindow = window.open("about:blank", "CWindow", strProp);
			var objForm = document.getElementById("frmAgentIncentiveReport");
			objForm.target = "CWindow";
			objForm.action = "showAgentIncentiveSchemeReport.action";
			objForm.submit();
		}
	}
	
	/**
	 * validation goes here.
	 */
	UI_AgentIncentiveRpt.validateFields = function(){
		var selSchemes = lsbx.getselectedData();
		var selAgents = ls.getselectedData();
		
		if(selSchemes == ""){
			return false;
		}else if(selAgents == ""){	
			return false;
		}else if($("#searchDateFrm").val() == ""){
			return false;
		}else if(!dateValidDate($("#searchDateFrm").val())){
			return false;
		}else if($("#searchDateTo").val() == ""){
			return false;
		}else if(!dateValidDate($("#searchDateTo").val())){
			return false;
		}else{
			setField("hdnAgents", selAgents);
			setField("hdnIncentives", selSchemes);
			return true;
		}
		//else if($("#txtSchemeName").val() == ""){
			
		//}else if($("#txtSchemeName").val() == ""){
			
		//}
	}
	
	/**
	 * Auto Date generation
	 */
	UI_AgentIncentiveRpt.dateOnBlur = function(inParam){
		switch (inParam.id){
			case 0 : dateChk("searchDateFrm"); break;
			case 1 : dateChk("searchDateTo"); break;
			}
	}