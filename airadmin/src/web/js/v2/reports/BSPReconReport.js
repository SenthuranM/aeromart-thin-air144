
function UI_BSPReconRpt(){}
UI_BSPReconRpt.screenID = "UC_REPM_087";

setField("hdnLive",repLive);

//Onload function
$(function(){
	$("#divBSPReconReport").decoratePanel("BSP Reconciliation Report");
	
	$("#tnxDateFrom").datepicker({showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', dateFormat: 'dd/mm/yy', buttonImageOnly: true, maxDate: new Date(), buttonText:'Date from'});
	$("#tnxDateTo").datepicker({showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', dateFormat: 'dd/mm/yy', buttonImageOnly: true, maxDate: new Date(), buttonText:'Date to'});
	$("#dpcProcessedDateFrom").datepicker({showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', dateFormat: 'dd/mm/yy', buttonImageOnly: true, maxDate: new Date(), buttonText:'Date from'});
	$("#dpcProcessedDateTo").datepicker({showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', dateFormat: 'dd/mm/yy', buttonImageOnly: true, maxDate: new Date(), buttonText:'Date to'});
	
	//setter
	$("#tnxDateFrom").blur(function() { UI_BSPReconRpt.dateOnBlur({field: 'paymentDateFrm'});});
	$("#tnxDateTo").blur(function() { UI_BSPReconRpt.dateOnBlur({field: 'paymentDateTo'});});
	$("#dpcProcessedDateFrom").blur(function() { UI_BSPReconRpt.dateOnBlur({field: 'bookedDateFrm'});});
	$("#dpcProcessedDateTo").blur(function() { UI_BSPReconRpt.dateOnBlur({field: 'bookedDateTo'});});
	
	$("#btnClose").decorateButton();
	$("#btnView").decorateButton();
		
	// register event handler for buttons
	$('#btnClose').click(function() { UI_BSPReconRpt.closeClicked(); });
	$('#btnView').click(function() { UI_BSPReconRpt.viewClicked(); });
});

/**
 * Auto Date generation
 */
UI_BSPReconRpt.dateOnBlur = function(inParam){
	dateChk(inParam.field);
}

/**
 * close click event handler method.
 */
UI_BSPReconRpt.closeClicked = function(){
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(UI_BSPReconRpt.screenID))) {
		setPageEdited(false);
		top[1].objTMenu.tabRemove(UI_BSPReconRpt.screenID);
	}
}

UI_BSPReconRpt.viewClicked = function(){
	if(UI_BSPReconRpt.validateFields()){
		var strProp = 'toolbar=no,location=no,status=Yes,menubar=yes,scrollbars=yes,width=900,height=600,resizable=yes,top=1,left=10'
		top[0].objWindow = window.open("about:blank", "CWindow", strProp);
		$("#hdnEntityText").val($("#selEntity option:selected").text())
		var objForm = document.getElementById("frmBSPReconReport");
		objForm.target = "CWindow";
		objForm.action = "showBSPReconciliationReport.action";
		objForm.submit();
	}
}

UI_BSPReconRpt.validateFields = function() {
	var tnxDateFrom = $("#tnxDateFrom").val();
	var tnxDateTo = $("#tnxDateTo").val();

	var deptFromDate = $("#paymentDateFrm").val();
	var deptToDate = $("#paymentDateTo").val();

	var tnxDateSet = dateValidation(tnxDateFrom, tnxDateTo, "#tnxDateFrom",
			"#tnxDateTo", true, true);
	var dpcDateSet = true;
	if (tnxDateSet) {
		var dpcDateFrom = $("#dpcProcessedDateFrom").val();
		var dpcDateTo = $("#dpcProcessedDateTo").val();
		dpcDateSet = dateValidation(dpcDateFrom, dpcDateTo,
				"#dpcProcessedDateFrom", "#dpcProcessedDateTo", false, false);
	}

	return tnxDateSet && dpcDateSet;
}

setPageEdited = function(isEdited) {
	top[1].objTMenu.tabPageEdited(UI_BSPReconRpt.screenID, isEdited);
}

dateValidation = function(fromDate, toDate, fromField, toField, fromReq, toReq){
	if(fromReq){
		if (fromDate == ""){
			showCommonError("Error", fromDtEmpty);
			$(fromField).focus();
			return false;
		}
	}
	
	if(fromDate != ''){
		if ( !dateValidDate(fromDate) ) {
			showCommonError("Error", fromDtInvalid);
			$(fromField).val("");
			$(fromField).focus();
			return false;
		}
	}
	
	if(toReq){
		if (toDate == ""){
			showCommonError("Error", toDtEmpty);
			$(toField).focus();
			return false;
		}
	}
	if(toDate != ''){		
		if ( !dateValidDate(toDate) ) {
			showCommonError("Error", toDtinvalid);
			$(toField).val("");
			$(toField).focus();
			return false;
		}
	}
	
	if(fromDate != '' && toDate != ''){		
		if (!(fromDate == toDate)) {
			if (!CheckDates(fromDate, toDate)) {
				showCommonError("Error", fromDtExceed);
				return false;
			}
			
			if(exceedsOneMonth(fromDate,toDate)) {
				showCommonError("Error", exceedOneMonth);
				$(toField).focus();
				return false;
			}
		}
	}
	
	return true;
}