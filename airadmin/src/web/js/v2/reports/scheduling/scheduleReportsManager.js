/*
 *********************************************************
	Description		: Scheduling Reports Related Javascript Behaviour
	Author			: Navod Ediriweera
	Version			: 1.0
	Created on		: 05 July 2010	
 *********************************************************	
 */

$(function() {
	$("#divSearch").decoratePanel("Search Scheduled");
	$("#divResultsPanel").decoratePanel("Scheduled Reports");
	$("#divTabbed").decoratePanelWithoutTitle();
	
	$('#btnSearch').decorateButton();
	$('#btnEdit').decorateButton();
	$('#btnDelete').decorateButton();
	$('#btnClose').decorateButton();
	$('#btnReset').decorateButton();
	$('#btnSave').decorateButton();
	$('#btnSentItems').decorateButton();
	
	$("#tblScheduled").jqGrid({ 
		url:'showScheduledReports!searchSchduled.action',
		datatype: "json",	
		jsonReader : {
			  root: "rows", 
			  page: "page",
			  total: "total",
			  records: "records",
			  repeatitems: false,					 
			  id: "0"				  
			},													
		colNames:['&nbsp;','ID', 'Name', 'Name', 'From Date', 'To Date','Scheduled By', 'Status','Corn Expression',
		          'Version','FromDate Editable', 'ToDate Editable', 'Email IDs'], 
		colModel:[ 	{name:'id', width:5, jsonmap:'id'},  
		           	{name:'reportsViewTO.scheduledReportID',	index: 'scheduledReportID', 	width:15, align:"left", jsonmap:'reportsViewTO.scheduledReportID'},	
		           	{name:'reportsViewTO.reportName', 				index: 'name', 				width:60, align:"left", jsonmap:'reportsViewTO.reportName', 	hidden:true },	
		           	{name:'reportsViewTO.reportDisplayName',		index: 'displayName', 		width:60, align:"left", jsonmap:'reportsViewTO.reportDisplayName'},	
		           	{name:'reportsViewTO.startDate', 		index: 'fromDate',	width:30, align:"center",  jsonmap:'reportsViewTO.startDate'},	
		           	{name:'reportsViewTO.endDate', 		index: 'toDate',		width:30, align:"center",  jsonmap:'reportsViewTO.endDate'},
		           	{name:'reportsViewTO.scheduledBy',	index: 'scheduledBy',	width:30, align:"center",  jsonmap:'reportsViewTO.scheduledBy'},
		         	{name:'reportsViewTO.status',	index: 'status',			width:30, align:"center",  jsonmap:'reportsViewTO.status'},
		         	{name:'reportsViewTO.cornExp',	index: 'cornExp',			width:40, align:"center",  jsonmap:'reportsViewTO.cornExp',hidden:true},	
		           	{name:'reportsViewTO.version',	index: 'version', 			width:40,align:"center",	jsonmap:'reportsViewTO.version', hidden:true},
		           	{name:'isFromDateEditable',		index: 'isFromDateEditable', 	width:40,align:"center",	jsonmap:'isToDateEditable', hidden:true},				   							   	
		           	{name:'isToDateEditable',		index: 'isToDateEditable', 		width:40,align:"center",	jsonmap:'isToDateEditable', hidden:true},
		           	{name:'reportsViewTO.emailIDs',		index: 'emailIDs', 		width:40,align:"center",	jsonmap:'reportsViewTO.emailIDs'}				   							   	

					], 
		imgpath: '../../themes/default/images/jquery',
		multiselect:false,
		pager: $('#scheduledReportsPager'),
		rowNum:10,						
		viewrecords: true,
		height:170,
		width:920,	
		onSelectRow: function(rowid){
			UI_ScheduledReports.reportGridRowOnClick (rowid);
		},
	   	loadComplete: function (e){
			if(UI_Common.displayMsgObj.message){
				UI_Common.displayMessage(UI_Common.displayMsgObj);
				UI_Common.displayMsgObj.clearData();
			}
			UI_ScheduledReports.clearEditArea();
	   	},
	}).navGrid("#scheduledReportsPager",{refresh: true, edit: false, add: false, del: false, search: false});

	$('#btnSearch').click(function() { UI_ScheduledReports.searchForScheduled(); });
	$('#btnEdit').click(function() { UI_ScheduledReports.editOnClick(); });
	$('#btnDelete').click(function() { UI_ScheduledReports.activateDeactivate(); });
	$('#btnSave').click(function() { UI_ScheduledReports.saveScheduledReport(); });
	$('#btnSentItems').click(function() { UI_ScheduledReports.viewRecordEmails(); });
	$('#btnReset').click(function() { UI_ScheduledReports.resetonClick(); });
	
	$("#btnClose").enableButton();
	$("#btnEdit").disableButton();
	$("#btnDelete").disableButton();
	$("#btnSave").disableButton();
	$("#btnSentItems").disableButton();
	$("#fromDate, #toDate").on("blur",function(){
		dateChk(this.id);
	});
    $("#srchPeriodFrom").datepicker({
        dateFormat: 'dd/mm/yy',
        changeMonth: 'true',
        changeYear: 'true',
        showButtonPanel: 'true',
        beforeShow: function(input, inst){
            inst.dpDiv.css({
                marginTop: "8px",
                marginLeft: input.offsetWidth - 120 + 'px',
                position: "fixed"
            });
        }
    });
    
    $("#srchPeriodTo").datepicker({
        dateFormat: 'dd/mm/yy',
        changeMonth: 'true',
        changeYear: 'true',
        showButtonPanel: 'true',
        beforeShow: function(input, inst){
            inst.dpDiv.css({
                marginTop: "8px",
                marginLeft: input.offsetWidth - 120 + 'px',
                position: "fixed"
            });
        }
    });
    
    var currDate = new Date();
    currDate.setDate(currDate.getDate()+1);
    
    $("#fromDate").datepicker({
        dateFormat: 'dd/mm/yy',
        changeMonth: 'true',
        changeYear: 'true',
        showButtonPanel: 'true',
        minDate: currDate
    });
    
    $("#toDate").datepicker({
        dateFormat: 'dd/mm/yy',
        changeMonth: 'true',
        changeYear: 'true',
        showButtonPanel: 'true',
        minDate: currDate
    });
    UI_ScheduledReports.setDateAutoFormatter();
    UI_Common.initilizeMessage();    
    $("#divTabbed").tabs({ disabled: [1] });
    $("#frmModify").disableForm();
    $('#btnSave').disableButton();
});

function UI_ScheduledReports(){}

UI_ScheduledReports.viewRecordEmails = function (){
	var postData = {};
	$('#divTabbed').tabs( "enable" , 1 );
	$('#divTabbed').tabs( "option", "active", 1 );
	postData['reportsViewTO.scheduledReportID']= $('#scheduledReportID').val();
 		 	
	$("#tblScheduledSent").setGridParam({page:1,postData:postData});
 	$("#tblScheduledSent").trigger("reloadGrid");
}

UI_ScheduledReports.editOnClick = function (){
	$("#frmModify .shpt_rpt_editable").enableFormSelective();
	var rowid = $("#tblScheduled").getGridParam('selrow');	
	var selectedRow = $("#tblScheduled").getRowData(rowid);
	var varEditFromDate = selectedRow["isFromDateEditable"];
	var varEditFromDate = selectedRow["isToDateEditable"];
	if(varEditFromDate === "true"){
		$('#fromDate').attr('disabled', false);
	}else {
		$('#fromDate').attr('disabled', true);
	}
	if(varEditFromDate === "true"){
		$('#toDate').attr('disabled', false);
	}else {
		$('#toDate').attr('disabled', true);
	}
}

UI_ScheduledReports.setDateAutoFormatter = function (){
//	$('#srchPeriodTo').change(function (){
//		var strTime = this.value;
//		dateChk('srchPeriodTo');	
//	});	
//	$('#srchPeriodFrom').change(function (){
//		var strTime = this.value;
//		dateChk('srchPeriodFrom');	
//	});
}

UI_ScheduledReports.resetonClick = function (){
	
	if($("#tblScheduled").getGridParam("selrow") != null){
		UI_ScheduledReports.reportGridRowOnClick($("#tblScheduled").getGridParam("selrow"));
	} else {
		$("#frmModify").clearForm();	
	}	
}

UI_ScheduledReports.searchForScheduled = function (){
	if(UI_ScheduledReports.validateSearch()) {
	
		var postData = {};
		postData['reportsSearchTO.reportName']= $('#serchSelReportName').val();
		postData['reportsSearchTO.reportUser'] = $('#srchScheduledBy').val();
		postData['reportsSearchTO.fromDate'] = $('#srchPeriodFrom').val();
		postData['reportsSearchTO.toDate'] = $('#srchPeriodTo').val();
	 		 	
		$("#tblScheduled").setGridParam({page:1,postData:postData});
	 	$("#tblScheduled").trigger("reloadGrid");
	}
}

UI_ScheduledReports.saveScheduledReport = function (){
	var url = 'showScheduledReports!saveScheduledReport.action';	
	$('#frmModify').attr('action',url );
	$('#frmModify').ajaxSubmit({ dataType: 'json', 
		beforeSubmit:UI_ScheduledReports.validateSave , 
		success: UI_ScheduledReports.afterSave		
	});
}

UI_ScheduledReports.validateSave = function (){
	var startDate = $('#fromDate').val();
	var endDate = $('#toDate').val();
	var intStartDate = parseInt(startDate.substring(6) + startDate.substring(3,5) + startDate.substring(0,2));
	var intEndDate = parseInt(endDate.substring(6) + endDate.substring(3,5) + endDate.substring(0,2));
	if(intStartDate > intEndDate){
		UI_Common.displayMessage( {msgType:"Error", message:"Start date cannot be grater than end date." } );
		return false;
	}
	
	if($("#txtSendTo").val() != null && $("#txtSendTo").val() != "" ) {
		var emailFilter=/^[A-Z0-9\._%-]+@[A-Z0-9\.-]+\.[A-Z]{2,4}(?:[,;][A-Z0-9\._%-]+@[A-Z0-9\.-]+\.[A-Z]{2,4})*$/i;
		if(!emailFilter.test($("#txtSendTo").val())){
			UI_Common.displayMessage( {msgType:"Error", message:"Please enter correct email adress. Email adresses should be comma seperated." } );
			return false;
		}
	} else {
		UI_Common.displayMessage({msgType:"Error", message:"Please enter send report to email adress(es)."});
		return false;
	}
	return true;
}
UI_ScheduledReports.clearEditArea = function (){
	$("#frmModify").clearForm();
	$('#lblReportDisplayName').empty();
	$('#lblScheduledBy').empty();
	$('#lblCornExp').empty();
	$("#btnDelete").disableButton();
	$("#btnEdit").disableButton();
	$("#btnSave").disableButton();
	$('#divTabbed').tabs( "option", "active", 0 );
	$('#divTabbed').tabs( "disable" , 1 );
}
UI_ScheduledReports.refreshGrid = function (){
	$("#frmModify").disableForm();

	UI_ScheduledReports.clearEditArea();
	top[2].HideProgress();
	UI_Common.initilizeMessage();
	$("#tblScheduled").trigger("reloadGrid");//This is an  Event
}

UI_ScheduledReports.afterSave = function (response){	
	if(response.msgType != 'Error'){
		UI_ScheduledReports.refreshGrid();
	}
	UI_Common.displayMessage (response);	
}

UI_ScheduledReports.activateDeactivate = function (){
	if(UI_ScheduledReports.validateDelete () ){
		var url = 'showScheduledReports!activateDeactivate.action';	
		$('#frmModify').attr('action',url );
		$('#frmModify').ajaxSubmit({ dataType: 'json', 
			beforeSubmit:UI_ScheduledReports.validateDelete , 
			success: UI_ScheduledReports.deleteSuccess
		});
	}
}

UI_ScheduledReports.deleteSuccess = function (response){	
	if(response.msgType != 'Error'){
		UI_ScheduledReports.refreshGrid();
	}	
	UI_Common.displayMessage (response);
}
UI_ScheduledReports.validateSearch = function (){
	UI_Common.initilizeMessage();
	return true;
}
UI_ScheduledReports.validateDelete = function (){
	UI_Common.initilizeMessage();
	return true;
}

UI_ScheduledReports.reportGridRowOnClick = function (rowid){
	UI_Common.initilizeMessage();
	$("#tblScheduled").GridToForm(rowid,"#frmModify");
	$('#divTabbed').tabs( "option", "active", 0 );
	$('#divTabbed').tabs( "disable" , 1 );	
	var selectedRow = $("#tblScheduled").getRowData(rowid);
	$('#lblReportDisplayName').text(selectedRow["reportsViewTO.reportDisplayName"]);
	$('#lblScheduledBy').text(selectedRow["reportsViewTO.scheduledBy"]);
	$('#lblCornExp').text(selectedRow["reportsViewTO.cornExp"]);
	
	$("#frmModify").disableForm();

	$("#btnDelete").enableButton();
	$("#btnEdit").enableButton();
	$("#btnSave").enableButton();
	$("#btnSentItems").enableButton();
}


