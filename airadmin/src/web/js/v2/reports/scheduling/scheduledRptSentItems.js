$(function() {

	$("#tblScheduledSent").jqGrid({ 
		url:'showScheduledReports!searchSRSentItems.action',
		datatype: "json",	
		jsonReader : {
			  root: "sentGridRows", 
			  page: "sentGridPage",
			  total: "sentGridTotal",
			  records: "sentGridRecords",
			  repeatitems: false,					 
			  id: "0"				  
			},													
		colNames:['&nbsp;', 'Sent Time','Notes', 'Status'], 
		colModel:[ 	{name:'id', width:5, jsonmap:'id'},  
		           	{name:'sentTime',		index: 'sentTo', 		width:60, align:"left"},	
		           	{name:'notes',		index: 'notes', 		width:150, align:"left"},
		         	{name:'status',		index: 'status', 		width:60, align:"left"}
					], 
		imgpath: '../../themes/default/images/jquery',
		multiselect:false,
		pager: $('#sentItemsPager'),
		rowNum:10,						
		viewrecords: true,
		height:100,
		width:880,	
		onSelectRow: function(rowid){			
		},
	   	loadComplete: function (e){
			
	   	},
	}).navGrid("#sentItemsPager",{refresh: true, edit: false, add: false, del: false, search: false});
	
});