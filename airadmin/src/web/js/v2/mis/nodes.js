USETEXTLINKS = 1
STARTALLOPEN = 0
USEFRAMES = 0
USEICONS = 0
WRAPTEXT = 1
PRESERVESTATE = 1
ICONPATH = "../../images/treemenu/";

foldersTree = gFld("<b>AccelAero Quick Reports</b>", "")

var arrReporMap = new Array();
var intMapRepo = 0;
var strPrimeMenu = "";

var intMenuLen = top.arrMenu.length;
for (var i = 0 ; i < intMenuLen ; i++){
	if (top.arrMenu[i][1].substr(0,1) == "E"){
		if (top.arrMenu[i][1].substr(1,1) == "0"){
			if (top.arrMenu[i][0] == "S"){
				var strPValue = top.arrMenu[i][5];
				eval("aux1 = insFld(foldersTree, gFld('" + top.arrMenu[i][6] + "', 'javascript:undefined'))");
				strPrimeMenu += "," + i;
				
				for (var x = 0 ; x < intMenuLen ; x++){
					if (top.arrMenu[x][1] == strPValue){
						if (top.arrMenu[x][0] == "S" || top.arrMenu[x][1].substr(1,1) == "3"){
							var strSValue = top.arrMenu[x][5];
							eval("aux2 = insFld(aux1, gFld('" + top.arrMenu[x][6] + "', 'javascript:undefined'))");
							if (top.arrMenu[x][0] == "S"){								
								for (var m = 0 ; m < intMenuLen ; m++){
									if (top.arrMenu[m][1] == strSValue){
										if (top.arrMenu[m][0] == "M"){
											eval("insDoc(aux2, gLnk('S', '" + top.arrMenu[m][6] + "', 'javascript:pgNodeClick(" + intMapRepo + ")'))");
											
											arrReporMap.push(new Array(intMapRepo, top.arrMenu[m][4]));
											intMapRepo++;
										}
									}
								}
							} else {
								if (top.arrMenu[x][0] == "M"){
									eval("insDoc(aux2, gLnk('S', '" + top.arrMenu[x][6] + "', 'javascript:pgNodeClick(" + intMapRepo + ")'))");
											
									arrReporMap.push(new Array(intMapRepo, top.arrMenu[x][4]));
									intMapRepo++;
								}						
							}
						}
					}
				}
			}
		}
	}
}

strPrimeMenu += strPrimeMenu + ",";

/*
		aux1 = insFld(foldersTree, gFld("Live", ""))
		
			aux2 = insFld(aux1, gFld("Agency", ""))
					insDoc(aux2, gLnk("S", "Company Payments", "javascript:pgNodeClick(0)"))
					
			aux2 = insFld(aux1, gFld("Planning", ""))
 					insDoc(aux2, gLnk("S", "Forward Bookings", "javascript:pgNodeClick(1)"))
					insDoc(aux2, gLnk("S", "Inbound/Outbound Connections", "javascript:pgNodeClick(2)"))
						
		aux1 = insFld(foldersTree, gFld("Off-Line", ""))
					aux2 = insFld(aux1, gFld("Reservations", ""))
						   insDoc(aux2, gLnk("S", "Reservations Breakdown", "javascript:pgNodeClick(3)"))
						   insDoc(aux2, gLnk("S", "Mode of Payments", "javascript:pgNodeClick(4)"))
						   insDoc(aux2, gLnk("S", "Refund", "javascript:pgNodeClick(5)"))
						   insDoc(aux2, gLnk("S", "Enplanement", "javascript:pgNodeClick(6)"))
						   insDoc(aux2, gLnk("S", "On-hold Passengers", "javascript:pgNodeClick(7)"))
						   insDoc(aux2, gLnk("S", "Revenue &amp; Tax", "javascript:pgNodeClick(8)"))
						   insDoc(aux2, gLnk("S", "Passenger Credits", "javascript:pgNodeClick(9)"))
						   insDoc(aux2, gLnk("S", "Tax &amp; Surcharges", "javascript:pgNodeClick(10)"))
						   insDoc(aux2, gLnk("S", "O n D", "javascript:pgNodeClick(11)"))
						   insDoc(aux2, gLnk("S", "Customer Existing Credit", "javascript:pgNodeClick(12)"))
						   insDoc(aux2, gLnk("S", "Interline Revenue &amp; Tax", "javascript:pgNodeClick(13)"))
						   insDoc(aux2, gLnk("S", "Credit Card Transactions", "javascript:pgNodeClick(14)"))
						   insDoc(aux2, gLnk("S", "Passenger Contact Details", "javascript:pgNodeClick(15)"))
						   insDoc(aux2, gLnk("S", "Forward Sales", "javascript:pgNodeClick(16)"))
						   insDoc(aux2, gLnk("S", "Insurance", "javascript:pgNodeClick(17)"))
						   insDoc(aux2, gLnk("S", "Passenger Status Details", "javascript:pgNodeClick(18)"))
						   insDoc(aux2, gLnk("S", "PName Change Details", "javascript:pgNodeClick(19)"))
						   insDoc(aux2, gLnk("S", "Top Performing Seg. Chart", "javascript:pgNodeClick(20)"))
						   insDoc(aux2, gLnk("S", "Meal Details for Finance", "javascript:pgNodeClick(21)"))
						   insDoc(aux2, gLnk("S", "Airport Service Commissions", "javascript:pgNodeClick(22)"))

					aux2 = insFld(aux1, gFld("Agency", ""))
						   insDoc(aux2, gLnk("S", "Outstanding Balance", "javascript:pgNodeClick(23)"))
						   insDoc(aux2, gLnk("S", "Invoice details", "javascript:pgNodeClick(24)"))
						   insDoc(aux2, gLnk("S", "Company Payments", "javascript:pgNodeClick(25)"))
						   insDoc(aux2, gLnk("S", "Agent/GSA List", "javascript:pgNodeClick(26)"))
						   insDoc(aux2, gLnk("S", "Transactions", "javascript:pgNodeClick(27)"))
						   insDoc(aux2, gLnk("S", "Company Payment by POS", "javascript:pgNodeClick(28)"))
						   insDoc(aux2, gLnk("S", "Agent Commission", "javascript:pgNodeClick(29)"))
						   insDoc(aux2, gLnk("S", "Interline Sales", "javascript:pgNodeClick(30)"))
						   insDoc(aux2, gLnk("S", "Agent Statement", "javascript:pgNodeClick(31)"))
						   insDoc(aux2, gLnk("S", "Agent Sales Status", "javascript:pgNodeClick(32)"))
						   insDoc(aux2, gLnk("S", "Invoice/Settlement history", "javascript:pgNodeClick(33)"))
						   
					aux2 = insFld(aux1, gFld("Performance", ""))
						   insDoc(aux2, gLnk("S", "Staff Performance", "javascript:pgNodeClick(34)"))
						   insDoc(aux2, gLnk("S", "Travel Agent/GSA Productivity", "javascript:pgNodeClick(35)"))
						   insDoc(aux2, gLnk("S", "Top Agents", "javascript:pgNodeClick(36)"))
						   insDoc(aux2, gLnk("S", "Top Segments", "javascript:pgNodeClick(37)"))
						   insDoc(aux2, gLnk("S", "Country/Segment Contributions", "javascript:pgNodeClick(38)"))
						   insDoc(aux2, gLnk("S", "Agent Sales Report", "javascript:pgNodeClick(39)"))
						   
					aux2 = insFld(aux1, gFld("Planning", ""))
						   insDoc(aux2, gLnk("S", "Forward Bookings", "javascript:pgNodeClick(40)"))
						   insDoc(aux2, gLnk("S", "Print Schedules", "javascript:pgNodeClick(41)"))
						   insDoc(aux2, gLnk("S", "In/Outbound Schedules", "javascript:pgNodeClick(42)"))
						   insDoc(aux2, gLnk("S", "Schedule Capacity Variance", "javascript:pgNodeClick(43)"))
						   insDoc(aux2, gLnk("S", "Print Flights", "javascript:pgNodeClick(44)"))
						   insDoc(aux2, gLnk("S", "Post Depature Cancellations", "javascript:pgNodeClick(45)"))
						   insDoc(aux2, gLnk("S", "AVS Seats", "javascript:pgNodeClick(46)"))
						   
					aux2 = insFld(aux1, gFld("Web", ""))
						   insDoc(aux2, gLnk("S", "Customer Profile/Travel Histroy", "javascript:pgNodeClick(47)"))						  
						   insDoc(aux2, gLnk("S", "Web Profile", "javascript:pgNodeClick(48)"))						  
						   
					aux2 = insFld(aux1, gFld("General Reports", ""))
						   insDoc(aux2, gLnk("S", "Admin Audit", "javascript:pgNodeClick(49)"))						  
						   insDoc(aux2, gLnk("S", "Reservation Audit", "javascript:pgNodeClick(50)"))	
						   insDoc(aux2, gLnk("S", "Agent User Privileges", "javascript:pgNodeClick(51)"))	
						   insDoc(aux2, gLnk("S", "Booking Class Report", "javascript:pgNodeClick(52)"))	
*/
