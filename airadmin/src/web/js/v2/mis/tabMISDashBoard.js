
	function UI_tabMISDashBoard(){}
	
	UI_tabMISDashBoard.chartParams                   = null;
	UI_tabMISDashBoard.channelWiseSalesForPassnegers = null;
	UI_tabMISDashBoard.totPassengerCount		     = "0";
	UI_tabMISDashBoard.barsVisual;
	UI_tabMISDashBoard.channelWiseSales              = null;
	UI_tabMISDashBoard.chartPaxCountParams           = null;
	UI_tabMISDashBoard.displayOption				 = 0;
	UI_tabMISDashBoard.totSalesValue                 = "0";
	
	$(document).ready(function(){

		$("#btnEmailGraph").click( function(){
			UI_tabMISDashBoard.showFeedBackPopUp();		
			return false;
		});
	});
		
	UI_tabMISDashBoard.showFeedBackPopUp =  function(){
		UI_MISReports.feedBackSource = "dashboard";
		showHideFeedBack(true);
	}
	
	UI_tabMISDashBoard.sendEmail = function(blnStatus){	
		initializeMessage();
		setDisplay("spnFeedBack", blnStatus);
		var myData = UI_tabMISDashBoard.chartParams.replace(/&/g,"#");
		var chartUrl = "http://chart.apis.google.com/chart?"+myData;	
		var chartPaxData = "http://chart.apis.google.com/chart?"+UI_tabMISDashBoard.chartPaxCountParams.replace(/&/g,"#");
		var data = {};
		if(validateEmail($("#txtFeedBackTo").val())){
			data["emailID"]   = $("#txtFeedBackTo").val();
			data["comments"]  = $("#txtFeedBack").val();
			data["chartData"] = chartUrl;
			data["fromDate"]  = $("#txtFDate").val();
			data["toDate"]    = $("#txtTDate").val();
			data["pos"]       = $("#selPOS option:selected").text();
			data["region"]    = $("#selRegion option:selected").text();
			data["chartPaxData"] = chartPaxData;

			$.ajax( {
				url : 'showMISDashBoard!sendEmail.action',
				type : 'POST',
				data : data,
				dataType : 'json',
				success : function(response) {
					UI_tabMISDashBoard.sendEmailSuccess(response);
				},
				error : function(requestObj, textStatus, errorThrown) {
					handleCommonAjaxInvocationErrors(requestObj);
				}
			});
			
			/*$.post('showMISDashBoard!sendEmail.action', data, function(response) {
				UI_tabMISDashBoard.sendEmailSuccess(response);
			},'json');*/
		}
	}
	
	UI_tabMISDashBoard.sendEmailSuccess =  function(response){
		if(response.success){
			alert('Email sent successfully');
		}
	}

	 UI_tabMISDashBoard.fillRevenuDetails = function(){
		DivWrite("divRevenueD", "<font><b>" + CurrencyFormat(jsRevenue.revenue, 2) + "<\/b>&nbsp;<\/font>");
		DivWrite("divFareD", "<font><b>" + CurrencyFormat(jsRevenue.fare, 2) + "<\/b>&nbsp;<\/font>");
		DivWrite("divTaxD", "<font><b>" + CurrencyFormat(jsRevenue.tax, 2) + "<\/b>&nbsp;<\/font>");
		DivWrite("divSurchargeD", "<font><b>" + CurrencyFormat(jsRevenue.surcharges, 2) + "<\/b>&nbsp;<\/font>");
	}
		
	UI_tabMISDashBoard.createRevenueGraph = function(){
		var fareValues = jsRevGraphData.fareValues;
		var chargeValues = jsRevGraphData.chargeValues;
		var strValues = "";
		var intLen = fareValues.length;
		var maxValue = 0;
		
		var data = new google.visualization.DataTable();
	    	data.addColumn('string', 'Month');
	    	data.addColumn('number', 'Fare');
	    	data.addColumn('number', 'Surcharges');
				
		if(fareValues.length>0 || chargeValues.length>0){
						
			var months = jsRevGraphData.months.split("|");
			data.addRows(months.length);
			var cnt = 0;
			for(var i = 0; i < months.length; i++) {
				data.setValue(i, 0, months[i]+' '+fareValues[i].year.substring(2,4));
				data.setValue(i, 1, Number(fareValues[i].amount));
				data.setValue(i, 2, Number(chargeValues[i].amount));
			}
			
			new google.visualization.ColumnChart(document.getElementById('imgDashRevB')).draw(data, 
		            { width:240, height:290 ,
		        	is3D:true,
		        	colors:[{color:'#936997', darker:'#79567c'}, {color:'#b0494c', darker:'#933d3f'}],
				legend: 'bottom', vAxis: {baseline : 0}, backgroundColor:'#f6f6f6'
		        	}                   
		        );
	
		}
		else{
			DivWrite("imgDashRevB", "");
		}
	}
	
	UI_tabMISDashBoard.createDashSalesChannelWise = function(){
		var strParams = "";
		var strValues = "";
		var strType ="";		
		var objData = UI_tabMISDashBoard.channelWiseSales;
		var intLen = 0;
		var maxValue ="0";
		var strChannel ="";
		var count = 0;

		
		if(objData!=null){
			for(strChannel in objData){
				if(UI_tabMISDashBoard.displayOption==1){
					var totPerVal = getPercentageValue(objData[strChannel],UI_tabMISDashBoard.totSalesValue);
					strValues += totPerVal+",";
					if(totPerVal>maxValue){
						maxValue = totPerVal;
					}
				}
				else{
					strValues += objData[strChannel]+",";
					if(objData[strChannel]>maxValue){
						maxValue = objData[strChannel];
					}
				}
				strType+=strChannel+"|"
				count++;
			}
			
			strValues = strValues.substring(0,strValues.length-1);
			strType   = strType.substring(0,strType.length-1);
			
			var axisLabelStr  = getChartAxisLabels(strType);
			maxValue = maxValue*1.1;

			strParams = "cht=bhg"   ;                            // Graph Type
			strParams += "&chs=280x100"	;						// Height x width
			strParams += "&chf=bg,s,f6f6f6|c,s,fdffe4|b0,lg,0,669933,0,90d74a,1";    // Bar Colors
			strParams += "	|c,s,ffffff";						// Graph area background color
			strParams += "&chd=t:" + strValues;					// Data		
			strParams += "&chbh=a";								// Auto width
			strParams += "&chg=20,10,1,2";						// Grid
			strParams += "&chds=0,"+maxValue;
			
			if(UI_tabMISDashBoard.displayOption ==0){
				strParams += "&chxt=x,y";							// Axis
				strParams += "&chxl=";								// Axis Data	
				strParams += "	1:|"+axisLabelStr;
				strParams += "&chxr=0,0,"+maxValue;
				strParams += "&chm=N,000000,-1,,12";
			}
			else{	
				strParams += "&chxt=x,y";							// Axis
				strParams += "&chxl=";								// Axis Data
				strParams += "	0:||";
				strParams += "	1:|"+axisLabelStr;
				strParams += "&chm=N*p0*,000000,-1,,12";
			}

			UI_tabMISDashBoard.chartParams = strParams;
			if(count>0){
				getFieldByID("imgDashSalesB").src = getGraphImage(strParams);
			}
			else{
				getFieldByID("imgDashSalesB").src ="../../images/spacer_no_cache.gif";
			}
		}
	}
	
	UI_tabMISDashBoard.createDashSalesChannelWiseForPassengers = function(){
		var strParams  = "";
		var strValues  = "";
		var strType    = "";
		var objData    = UI_tabMISDashBoard.channelWiseSalesForPassnegers;
		var strChannel = "";
		var maxValue   = 0;
		var count = 0;

		if(objData!=null){
			for(strChannel in objData){
				if(UI_tabMISDashBoard.displayOption==1){
					var totPerVal = getPercentageValue(objData[strChannel],UI_tabMISDashBoard.totPassengerCount);
					strValues += totPerVal+",";
					if(totPerVal>maxValue){
						maxValue = totPerVal;
					}
				}
				else{
					strValues += objData[strChannel]+",";
					if(objData[strChannel]>maxValue){
						maxValue = objData[strChannel];
					}
				}
				
				strType+=strChannel+"|"
				count++;
			}

			strValues = strValues.substring(0,strValues.length-1);
			strType   = strType.substring(0,strType.length-1);

			var axisLabelStr  = getChartAxisLabels(strType);
			maxValue  = maxValue*1.1;

			strParams = "cht=bhg"   ;                            // Graph Time
			strParams += "&chs=280x100"	;						//  width X Height 
			strParams += "&chf=bg,s,f6f6f6|c,s,fdffe4|b0,lg,0,669933,0,90d74a,1";					// Background Colors
			strParams += "	|c,s,ffffff";						// Graph area background color
//			strParams += "&chco=99CC00"	;						// Bar Colors
			strParams += "&chd=t:" + strValues;					// Data	
			strParams += "&chds=0,"+maxValue;
			strParams += "&chbh=a";								// Auto width				
			strParams += "&chg=20,10,1,2";						// Grid
		
			if(UI_tabMISDashBoard.displayOption ==0){
				strParams += "&chxt=x,y";							// Axis
				strParams += "&chxl=";								// Axis Data	
				strParams += "	1:|"+axisLabelStr;			
				strParams += "&chxr=0,0,"+maxValue;
				strParams += "&chm=N,000000,-1,,12";
			}
			else{	
				strParams += "&chxt=x,y";							// Axis
				strParams += "&chxl=";								// Axis Data
				strParams += "	0:||";
				strParams += "	1:|"+axisLabelStr;
				strParams += "&chm=N*p0*,000000,-1,,12";
			}
			
			UI_tabMISDashBoard.chartPaxCountParams = strParams;
			if(count>0){
				getFieldByID("imgDashSalesByPax").src = getGraphImage(strParams);
			}
			else{
				getFieldByID("imgDashSalesByPax").src ="../../images/spacer_no_cache.gif";
			}
		}
	}
	
	/**
	 * When a user changes radio selection for Display option, the graphs are regenerated with
	 * relevant label options.
	 */
	function changeDisplayOption(){
		UI_tabMISDashBoard.displayOption =$("input[name='displayOption']:checked").val()
		
		UI_tabMISDashBoard.createDashSalesChannelWise();
		UI_tabMISDashBoard.createDashSalesChannelWiseForPassengers();
	}
	
	 UI_tabMISDashBoard.createBkgYTD =function (){
		var intGraphWidth = 330;
		var intWidth = Math.round((jsBkgYTD.value / jsBkgYTD.budget) * intGraphWidth);
		getFieldByID("tdYTDAct").style.width = intWidth + "px";
		
		DivWrite("divBudgM", "<font>" + CurrencyFormat(Math.round(jsBkgYTD.budget / 2),0).split(".")[0] + "<\/font>");
		DivWrite("divBudgF", "<font>" + CurrencyFormat(jsBkgYTD.budget,0).split(".")[0] + "<\/font>");
		
		var strHTMLText = ""
		
	
		// Actual Value
		var strAlign = "right";
		var intLeft  = 0
		if (intWidth < 100){
			strAlign = "left";
			intLeft  = intWidth + 22;
		}else{
			if (intWidth > intGraphWidth){
				intWidth  = intGraphWidth; 
				strAlign = "right";
			}
		}
		strHTMLText = "<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
		strHTMLText += "	<tr>";
		strHTMLText += "		<td align='" + strAlign + "'>";
		strHTMLText += "			<font><b>" + CurrencyFormat(jsBkgYTD.value,0).split(".")[0] + "<\/b><\/font>";
		strHTMLText += "		</td>";
		strHTMLText += "	</tr>";
		strHTMLText += "</table>";
		DivWrite("divActualValue", strHTMLText);
	
		var objAct = getFieldByID("divActualValue");
		if (intLeft == 0){
			objAct.style.width = (intWidth + 18) + "px";
			objAct.style.left = intLeft + "px";
		}else{
			objAct.style.width = "200px";
			objAct.style.left = intLeft + "px";
		}
	}
	
	
	UI_tabMISDashBoard.retriveAndDisplayDashBoardData = function(){
		/*$.post('showMISDashBoard.action', hdnData, function(response) {
			UI_tabMISDashBoard.submitSuccess(response);
		},'json');*/
//		arrParamData['displayOption'] = $("#displayOption").val();
		$.ajax( {
			url : 'showMISDashBoard.action',
			type : 'POST',
			data : arrParamData,
			dataType : 'json',
			success : function(response) {
				UI_tabMISDashBoard.submitSuccess(response);
			},
			error : function(requestObj, textStatus, errorThrown) {
				handleCommonAjaxInvocationErrors(requestObj);
			}
		});		
	}
	
	
	UI_tabMISDashBoard.submitSuccess = function(response){
		eval(response.returnData);
		top[2].HideProgress();
		if(response.success){
			UI_tabMISDashBoard.channelWiseSalesForPassnegers = response.salesByChannelPaxCount;
			UI_tabMISDashBoard.totPassengerCount             = response.maxPassengerCount;
			UI_tabMISDashBoard.channelWiseSales              = response.channelWiseSales;
			UI_MISReports.baseCurrency						 = response.baseCurrency;
			UI_tabMISDashBoard.totSalesValue				 = response.totSalesValue;
			UI_MISReports.tblClick(1);
			UI_tabMISDashBoard.dashBoardOnLoad();
			setDisplay("divResultPane", true);
		}
		else{
			showERRMessage("Invalid response");	
		}
		
	}
	 UI_tabMISDashBoard.dashBoardOnLoad =function(){	
		UI_tabMISDashBoard.fillRevenuDetails();
		UI_tabMISDashBoard.createRevenueGraph();
		UI_tabMISDashBoard.createDashSalesChannelWise();
		UI_tabMISDashBoard.createBkgYTD();
		UI_tabMISDashBoard.createDashSalesChannelWiseForPassengers();
	}
	 
	 function getPercentageValue(intValue, intHighest){
			return(Number(intValue) / Number(intHighest));
		}
	

