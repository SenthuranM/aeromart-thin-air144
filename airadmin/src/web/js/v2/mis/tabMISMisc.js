	UI_tabMISAncillary.ancillarySales           = null;
//	UI_tabMISAncillary.ancillarySalesTotal      = null;
	UI_tabMISAncillary.ancillarySalesByRoute    = null;
	UI_tabMISAncillary.ancillarySalesByChannel  = null;
	UI_tabMISAncillary.ancillaryChannelTotals   = null;
	UI_tabMISAncillary.baseCurrency             = null;
	UI_tabMISAncillary.allSalesTotal			= null;
	UI_tabMISAncillary.displayByCount			= false;
	UI_tabMISAncillary.visualAncillary;
	
	function UI_tabMISAncillary(){}
	
	$(document).ready(function(){
		UI_tabMISAncillary.populateAncillaryCategories();
		$("#selAnsiPrdCat").change(function(){
			UI_tabMISAncillary.changeAncillaryCategory();
		});
		$("#selAnsiPrdCat option:first-child").attr("selected","selected");	
		$("#selAnciMode").change(function(){
			UI_tabMISAncillary.changeAncillaryMode();
		});
			
	});
	
	UI_tabMISAncillary.createKPIChannelInfo =  function(){
		hideTT();
		var objTbl = getFieldByID("tblAnsiChannel").tBodies[0];
		deleteTableRows(objTbl);
		var objData = UI_tabMISAncillary.ancillaryChannelTotals;
		
		var tblRow = null;
		var i = 0;
		var channel = null;
		var totChannelCount = 0;
		
		//creates the header row - sales channel names
		if(objData!=null){
			tblRow = document.createElement("TR");	
			tblRow.appendChild(createCell({desc:"", bold:true, align:"center", css:"thinRight"}));
			for(channel in objData ){		
				totChannelCount++;
				tblRow.appendChild(createCell({desc:channel+"<br>Sales ", bold:true, css:"thinRight", align:"center"}));			
			}
			tblRow.appendChild(createCell({desc:"Total", bold:true, align:"center", css:""}));	
			objTbl.appendChild(tblRow);
		}
		//creates the data rows -  sales for each category for all the sales channels
		var ancidata = UI_tabMISAncillary.ancillarySalesByChannel;
		if(ancidata!=null && ancidata.length>0){
			for(i=0;i<ancidata.length;i++){
				var ancillaryObj = ancidata[i];
				var strBGColor = "";
				var blnBold = false;
				tblRow = document.createElement("TR");
				strBGColor = getRowClass(i);
				tblRow.appendChild(createCell({desc:ancillaryObj.chargeCode, align:"left", css:"thinRight thinTop " + strBGColor, bold:blnBold}));
				var channelName ="";
				var channelSales = ancillaryObj.displaySalesByChannel;
				for(channelName in channelSales){
					var channelAmount = channelSales[channelName];	
//					if(channelAmount==0){
//						channelAmount = "0";
//					}
					tblRow.appendChild(createCell({desc:channelAmount, align:"right", css:"thinRight thinTop " + strBGColor, bold:blnBold}));
				}
				tblRow.appendChild(createCell({desc:ancillaryObj.totalAmount, align:"right", css:"thinTop " + strBGColor, bold:blnBold}));
				objTbl.appendChild(tblRow);

			}
			tblRow = document.createElement("TR");
			tblRow.appendChild(createCell({desc:"Total", bold:true, css:"thinRight thinTop ", align:"left"}));
			for(channel in objData ){			
				tblRow.appendChild(createCell({desc:objData[channel]+" "+UI_tabMISAncillary.baseCurrency, bold:true, css:"thinRight thinTop ", align:"right"}));	
			}
			tblRow.appendChild(createCell({desc:UI_tabMISAncillary.allSalesTotal+" "+UI_tabMISAncillary.baseCurrency, bold:true, css:"thinTop ", align:"right"}));
			objTbl.appendChild(tblRow);
		}
		
	}
		
	UI_tabMISAncillary.createAnciSalesGraph = function(){
		var strParams = "";
		var strAncCategory  = "";
		var strValues="";
		var strCategory = "";
		var objValues = UI_tabMISAncillary.ancillarySales;
		
		var data = new google.visualization.DataTable();
	    data.addColumn('string', 'Ancillary');
	    data.addColumn('number', 'Amount');
	   // data.addColumn('number', 'Refund');
	    var size = 0;
	    for(strAncCategory in objValues){
	    	size++;
	    }
	    data.addRows(size); 	//set column count
	    strAncCategory = "";
	    var i = 0;
	    for(strAncCategory in objValues){			
			if(UI_tabMISAncillary.ancillarySales[strAncCategory]>0){
				data.setValue(i, 0, strAncCategory);
				data.setValue(i, 1, UI_tabMISAncillary.ancillarySales[strAncCategory]); 
			}
			i++;
		}		
	    UI_tabMISAncillary.visualAncillary = new google.visualization.PieChart(document.getElementById('imgAnciSales'));
	    UI_tabMISAncillary.visualAncillary.draw(data, 
	            { width:370, height:200 ,
	        	isStacked:true,
	        	is3D: true, backgroundColor:'#f6f6f6',
	        	colors:[{color:'#b3daf8', darker:'#6f8697'},
	        	{color:'#f6c11d', darker:'#7c610e'},
	        	{color:'#0f9494', darker:'#005500'},
	        	{color:'#d85151', darker:'#ba4545'}]       	
	        	}                   
	        );	
	}
	
	/**
	 * Creates the Ancillary sales data segment wise for a given category
	 */
	UI_tabMISAncillary.createKPIInfo =  function(){
		hideTT();
		var objTbl = getFieldByID("tblAnsiSeat").tBodies[0];
		deleteTableRows(objTbl);
		
		var objData = UI_tabMISAncillary.ancillarySalesByRoute;
		if(objData!=null){
			var selectedType = $("#selAnsiPrdCat").val();
			var objValues = null;
			switch (selectedType){
			case "ANSI_S" : objValues = objData["SEAT"]; break;
			case "ANSI_M" : objValues =  objData["MEAL"]; break;
			case "ANSI_I" : objValues =  objData["INSURANCE"]; break;
			case "ANSI_H" : objValues =  objData["HALA"]; break;
			}

			var tblRow = null;
			var i = 0;

			tblRow = document.createElement("TR");

			tblRow.appendChild(createCell({desc:"Route", bold:true, align:"center", css:"thinRight"}));
			tblRow.appendChild(createCell({desc:"Revenue", bold:true, align:"center", css:""}));

			objTbl.appendChild(tblRow);

			var route = "";
			for(route in objValues){
				tblRow = document.createElement("TR");
				strBGColor = getRowClass(i);

				tblRow.appendChild(createCell({desc:route, width:"40%", align:"left", css:"thinRight thinTop " + strBGColor}));
				tblRow.appendChild(createCell({desc:objValues[route], width:"60%",  align:"right", css:" thinTop " + strBGColor}));

				objTbl.appendChild(tblRow);
				i++;
			}
		}

	}
	
	UI_tabMISAncillary.retriveAndDisplayAncillaryData = function(){
		/*$.post('showMISAncillary.action', hdnData, function(response) {
			UI_tabMISAncillary.submitAncillarySuccess(response);
		},'json');*/
		arrParamData['anciMode'] =  $("#selAnciMode").val();
		$.ajax( {
			url : 'showMISAncillary.action',
			type : 'POST',
			data : arrParamData,
			dataType : 'json',
			success : function(response) {
				UI_tabMISAncillary.submitAncillarySuccess(response);
			},
			error : function(requestObj, textStatus, errorThrown) {
				handleCommonAjaxInvocationErrors(requestObj);
			}
		});		
	}
	
	UI_tabMISAncillary.changeAncillaryCategory = function(){
		UI_tabMISAncillary.createKPIInfo();
	}
	
	UI_tabMISAncillary.changeAncillaryMode = function(){
		var data = {};
		data["fromDate"]  = $("#txtFDate").val();
		data["toDate"]    = $("#txtTDate").val();
		data["selPOS"]    = $("#txtTDate").val();
		data["region"]    = $("#selRegion").val();
		data["anciMode"]  = $("#selAnciMode").val();
		top[2].ShowProgress();
		/*$.post('showMISAncillary.action', data, function(response) {
			UI_tabMISAncillary.submitAncillarySuccess(response);
		},'json');*/
		
		$.ajax( {
			url : 'showMISAncillary.action',
			type : 'POST',
			data : data,
			dataType : 'json',
			success : function(response) {
				UI_tabMISAncillary.submitAncillarySuccess(response);
			},
			error : function(requestObj, textStatus, errorThrown) {
				handleCommonAjaxInvocationErrors(requestObj);
			}
		});	
		
	}
	
	UI_tabMISAncillary.submitAncillarySuccess = function(response){
		top[2].HideProgress();
		if(response.success) {
			UI_tabMISAncillary.ancillarySales          = response.ancillarySales;
//			UI_tabMISAncillary.ancillarySalesTotal     = response.ancillarySalesTotalValue;
			UI_tabMISAncillary.ancillarySalesByRoute   = response.ancillarySalesByRoute;
			UI_tabMISAncillary.ancillarySalesByChannel = response.ancillarySalesByChannel;
			UI_tabMISAncillary.ancillaryChannelTotals  = response.salesByChannelTotals;
			UI_tabMISAncillary.baseCurrency            = response.baseCurrency;
			UI_tabMISAncillary.allSalesTotal           = response.allChargesTotal;
			UI_tabMISAncillary.displayByCount          = response.anciByCount;
			
			UI_tabMISAncillary.createAnciSalesGraph();
			UI_tabMISAncillary.createKPIInfo();
			UI_tabMISAncillary.createKPIChannelInfo();
		}
		else{
			showERRMessage("Invalid response");	
		}
	}
	
	UI_tabMISAncillary.populateAncillaryCategories =  function(){
		var strHtml= '';
		if(DATA_MIS.initialParams.halaEnabled ==1){
			strHtml+= '<option value="ANSI_H">Hala</option>';
		}
		if(DATA_MIS.initialParams.insuranceEnabled ==1){
			strHtml+= '<option value="ANSI_I">Insurance</option>';
		}
		if(DATA_MIS.initialParams.mealsEnabled ==1){
			strHtml+= '<option value="ANSI_M">Meal</option>';
		}
		if(DATA_MIS.initialParams.seatEnabled ==1){
			strHtml+= '<option value="ANSI_S">Seat</option>';
		}
		$("#selAnsiPrdCat").html(strHtml);
	}
	
	

	
