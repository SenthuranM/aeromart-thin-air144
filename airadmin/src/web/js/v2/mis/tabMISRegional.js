	var strLastAgentType = "";
	
	UI_tabMISRegional.regionalSalesTrend   = null;
	UI_tabMISRegional.regionYearlySales    = null;
	UI_tabMISRegional.yearlyHighestValue   = null;
	UI_tabMISRegional.monthlyHighestValue  = null;
	UI_tabMISRegional.bestPerformingAgents = null;
	UI_tabMISRegional.lowPerformingAgents  = null;
	UI_tabMISRegional.productSalesData     = null;
	UI_tabMISRegional.avgAgentsSales       = null;
	UI_tabMISRegional.totalActualSales     = null;
	UI_tabMISRegional.barsVisual;
	UI_tabMISRegional.regions              = null;
	
	strTTImagePath = "../../images/";
	intTTHideDelay = 3200;

	function UI_tabMISRegional(){}
	
	$(document).ready(function(){
		$("#divPintSalesData").hide();
		
		$("#selTAgnts").change(function(){
			UI_tabMISRegional.createRegionalAgentPerformance();
		});
		
		$("#btnemail").click(function(){
			UI_tabMISRegional.showFeedBackPopUp();
			return false;
		});
		
		$("#btnExcel").click(function(){
			UI_tabMISRegional.loadExcelReport();
			return false;
		});
		
		$("#btnGraph").click(function(){
			Show_Popup();
			return false;
		});
		
		$("#btnPrint").click(function(){
			UI_tabMISRegional.printSalesData();
			return false;
		});
		
		
	
	});
	
	UI_tabMISRegional.showFeedBackPopUp =  function(){
		UI_MISReports.feedBackSource = "regional";
		showHideFeedBack(true);
	}
	
	
	UI_tabMISRegional.sendEmail =  function(blnStatus){
		setDisplay("spnFeedBack", blnStatus);
		var data = {};
		if(validateEmail($("#txtFeedBackTo").val())){
			data["emailID"]  = $("#txtFeedBackTo").val();
			data["comments"] = $("#txtFeedBack").val();
			data["fromDate"] = $("#txtFDate").val();
			data["toDate"] = $("#txtTDate").val();
			data["region"] = $("#selRegion").val();
			data["pos"] = $("#selPOS").val();

			/*$.post('showMISRegional!sendEmail.action', data, function(response) {
				UI_tabMISRegional.sendEmailSuccess(response);
			},'json');*/
			
			$.ajax( {
				url : 'showMISRegional!sendEmail.action',
				type : 'POST',
				data : data,
				dataType : 'json',
				success : function(response) {
					UI_tabMISRegional.sendEmailSuccess(response);
				},
				error : function(requestObj, textStatus, errorThrown) {
					handleCommonAjaxInvocationErrors(requestObj);
				}
			});				
	
		}
	}
	
	
	
	UI_tabMISRegional.sendEmailSuccess = function(response){
		if(response.success){
			alert("Email sent successfully");
		}
		else{
			showERRMessage("Error occurred while sending email");
		}
	}
	
	UI_tabMISRegional.loadExcelReport =  function(){
		$("#hdnFromDate").val($("#txtFDate").val());
		$("#hdnToDate").val($("#txtTDate").val());
		 var data = "showMISReports.action?hdnFromDate="+$("#txtFDate").val()+"&hdnToDate="+$("#txtTDate").val();		 
		 $("#frmExcelReport").attr('action',data);
		 $("#frmExcelReport").submit();
	}
	//*********** POP up ****************//

	function Show_Popup() {
		var intLeft = (window.screen.width - 700) / 2;
		var intTop = (window.screen.height - 380) / 2;
		window.open("showLoadJsp!misRegionalPopup.action","graphWindow",'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width=700,height=380,resizable=no,top=' + intTop + ',left=' + intLeft);
	}
	
	//*********** End pop up ****************//

	UI_tabMISRegional.printSalesData =  function(){		
		// Print the DIV.
		$(".printable" ).print();
		// Cancel click event.
		return false;
	}
		
	UI_tabMISRegional.createRegionalAgentPerformance = function(){
		var strType = $("#selTAgnts").val();
		hideTT();
		var objTbl = getFieldByID("tblReginalPerformance").tBodies[0];
		deleteTableRows(objTbl);
		
		strLastAgentType = strType;
		var objData = UI_tabMISRegional.bestPerformingAgents;
		if (strType != "REGIONAL_BP"){
			objData = UI_tabMISRegional.lowPerformingAgents;
		}
		
		var intLen = objData.length;
		var tblRow = null;
		var i = 0;
	
		tblRow = document.createElement("TR");
	
		tblRow.appendChild(createCell({desc:"", bold:true, align:"center", css:"thinRight"}));
		tblRow.appendChild(createCell({desc:"Location", bold:true, align:"center", css:"thinRight"}));
		tblRow.appendChild(createCell({desc:"Sales", bold:true, align:"center", css:""}));
	
		objTbl.appendChild(tblRow);
		
		if (intLen > 0){
			var strBGColor = "";
			do{
				tblRow = document.createElement("TR");
				strBGColor = getRowClass(i);
				tblRow.appendChild(createCell({desc:"<a href='#' onmouseover='UI_tabMISRegional.showAgentToolTip(" + i + ", event)' onmouseout='hideTT()'>" + objData[i].agentName + "<\/a>", css:"thinRight thinTop " + strBGColor}));
				tblRow.appendChild(createCell({desc:objData[i].agentLocation, align:"center", css:"thinRight thinTop " + strBGColor}));
				tblRow.appendChild(createCell({desc:addCommas(objData[i].amount), align:"right", css:" thinTop " + strBGColor}));
		
				objTbl.appendChild(tblRow);
		
				i++;
			}while(--intLen);
		}
		
		DivWrite("divAgntsSales", "<font><b>" + UI_MISReports.baseCurrency+" " +addCommas(UI_tabMISRegional.avgAgentsSales)  + "<\/b><\/font>");
	}
	
	/** Shows agent row's tool tip*/
	UI_tabMISRegional.showAgentToolTip = function(intRow, objE){
		try{
			var objData =UI_tabMISRegional.bestPerformingAgents;
			if (strLastAgentType != "REGIONAL_BP"){
				objData = UI_tabMISRegional.lowPerformingAgents;
			}
		
			var strSaleValue  = objData[intRow].yearlySales;
//			strSaleValue  =  replaceall(strSaleValue, ",", "");
			var intAvg = roundNumber(Number(strSaleValue) / 12, 0);
			strSaleValue = UI_MISReports.baseCurrency+" " + replaceall(CurrencyFormat(intAvg, 0), ".00", "");
		
			var strHTMLText = "<table width='100%' border='0' cellpadding='1' cellspacing='0'>";
			strHTMLText += "	<tr><td colspan='2'><font><b>" + objData[intRow].agentName + "</b><\/font><\/td><\/tr>";
			strHTMLText += "	<tr><td colspan='2'><hr><\/td><\/tr>";
			strHTMLText += "	<tr><td width='30%'><font>Address : <\/font><\/td><td width='70%'><font>" + objData[intRow].address1 + "<\/font><\/td><\/tr>";
			strHTMLText += "	<tr><td><\/td><td><font>" + objData[intRow].address2 + "<\/font><\/td><\/tr>";
			strHTMLText += "	<tr><td><\/td><td><font>" + objData[intRow].city + "<\/font><\/td><\/tr>";
			strHTMLText += "	<tr><td><\/td><td><font>" + objData[intRow].country + "<\/font><\/td><\/tr>";
			strHTMLText += "	<tr><td><font>Telephone No : <\/font><\/td><td><font class='txtBold'> " + objData[intRow].telNo + "<\/font><\/td><\/tr>";
			strHTMLText += "	<tr><td><font>Email : <\/font><\/td><td><font class='txtBold'><a href='mailto:" + objData[intRow].email + "' target='_blank' title='E-mail'>" + objData[intRow].email + "<\/a><\/font><\/td><\/tr>";
			if(objData[intRow].contact!=null){
			strHTMLText += "	<tr><td><font>Contact Person : <\/font><\/td><td><font class='txtBold'>" + objData[intRow].contact + "<\/font><\/td><\/tr>";
			}
			else{
				strHTMLText += "	<tr><td><font>Contact Person : <\/font><\/td><td><font class='txtBold'>--<\/font><\/td><\/tr>";
			}
			strHTMLText += "	<tr><td colspan='2'><hr><\/td><\/tr>";
			strHTMLText += "	<tr><td colspan='2'><font>Yearly Total Sales value : <\/font><font class='txtBold'>" +UI_MISReports.baseCurrency+" " +addCommas(objData[intRow].yearlySales) + "<\/font><\/td><\/tr>";
			strHTMLText += "	<tr><td colspan='2'><font>Monthly Average Sales value : <\/font><font class='txtBold'>" + strSaleValue + "<\/font><\/td><\/tr>";
			/*if(objData[intRow].lastSale!=null){
				strHTMLText += "	<tr><td colspan='2'><font>Last ticket sold : <\/font><font class='txtBold'>" + objData[intRow].lastSale + "<\/font><\/td><\/tr>";
			}
			else{
				strHTMLText += "	<tr><td colspan='2'><font>Last ticket sold : <\/font><font class='txtBold'> --<\/font><\/td><\/tr>";
			}*/
			strHTMLText += "<\/table>";
			showTT(strHTMLText, "top", "right", objE, -320, 20, 300);
		}catch(ex){}
		
	} 
	
	var arrTrrStatus = new Array();
	
	/**
	 * Creates the grid for product sales analysis
	 */
	UI_tabMISRegional.createRegionalProductSalesAnalysis = function(){
		
		var objTbl = getFieldByID("tblRegDisAna").tBodies[0];	
		deleteTableRows(objTbl);
		var objData = UI_tabMISRegional.productSalesData;
		var intLen = objData.length;
		var tblRow = null;
		var strHTMLText = "";
		var i = 0;
		//if (intLen > 0){
			tblRow = document.createElement("TR");
			tblRow.appendChild(createCell({desc:"Regions", align:"center", bold:true, width:"20%"}));
			
			strHTMLText += "<table width='100%' border='0' cellapdding='0' cellspacing='0'>";
			strHTMLText += "	<tr>"
			strHTMLText += "		<td align='left'  width='30%'><font class='txtBold'>Location<\/td>"
			strHTMLText += "		<td align='center' width='12%'><font class='txtBold'>Trgt.<\/td>"
			strHTMLText += "		<td align='center' width='12%'><font class='txtBold'>Act.<\/td>"
						strHTMLText += "		<td align='center' width='10%'><font class='txtBold'>YTD<\/td>"
			strHTMLText += "		<td align='center' width='10%'><font class='txtBold'>YTD Var.<\/td>"
			strHTMLText += "	<\/tr>"
			strHTMLText += "<\/table>"
			tblRow.appendChild(createCell({desc:strHTMLText, width:"80%"}));
			
			objTbl.appendChild(tblRow);
				
			var strBGColor = "";
			var tblMarket = null;
			var trMarket = null;
			var strRegion = "";
			for(strRegion in objData){
				strBGColor = "";
				tblRow = document.createElement("TR");
				strBGColor = getRowClass(i);				
				tblRow.appendChild(createCell({desc:strRegion, css:"thinRight thinTop " + strBGColor, width:"20%"}));
				tblRow.appendChild(createCell({desc:"<table id='tblDetails_" + i + "' width='100%' border='0' cellpadding='0' cellspacing='0'><tbody><\/tbody><\/table>", css:"thinRight thinTop " + strBGColor, width:"80%"}));
				objTbl.appendChild(tblRow);
				
				tblMarket = getFieldByID("tblDetails_" + i).tBodies[0];	
				tblMarket.className = "thinTop " + strBGColor;
				
				var objMarkets = objData[strRegion]; 
				strHTMLText = "";
				var x = 0;
				var strRowClass = "";
				var strLocWiseRegionalSales = "";
				for(strLocWiseRegionalSales in objMarkets){
					var locationSales = objMarkets[strLocWiseRegionalSales];
					strRowClass = "";
					if (x != 0){
						strRowClass = "thinTop";
					}
					
					trMarket = document.createElement("TR");
					strHTMLText = "<table width='100%' border='0' cellpadding='0' cellspacing='0'>"
					strHTMLText += "	<tr>";
					strHTMLText += "		<td width='20%' class='" + strRowClass + " cursorPointer' onclick='tablTreeOnClick(" + i + "," + x + ")'  style='height:19px;'><img src='../../images/M030_no_cache.gif' border='0' id='img_" + i + "_" + x + "'><font>&nbsp;&nbsp;" + locationSales.location + "</font><\/td>";
					strHTMLText += "		<td width='10%' class='" + strRowClass + " ' align='right'><font>" + locationSales.target  +"<\/font><\/td>";
					strHTMLText += "		<td width='10%' class='" + strRowClass + " ' align='right'><font>" + addCommas(locationSales.strActualSales)  +"<\/font><\/td>";
					strHTMLText += "		<td width='10%' class='" + strRowClass + " ' align='right'><font>" + locationSales.ytdSales  +"<\/font><\/td>";
					strHTMLText += "		<td width='10%' class='" + strRowClass + " ' align='right'><font>" + locationSales.ytdVariance  +"<\/font><\/td>";
					strHTMLText += "	<\/tr>";
					strHTMLText += "<\/table>";
					
					trMarket.appendChild(createCell({desc:strHTMLText}));
					tblMarket.appendChild(trMarket);
					
					var strLocation ="";
					var objLocaitons = null;
					var intLocaiton = locationSales.agentsRegionalSalesLocationWise.length;
					
					var strLocClass = "";
					strHTMLText = "<div id='div_" + i + "_" + x + "' style='display:none;'><table width='100%' border='0' cellpadding='0' cellspacing='0'>"
					for(strLocation in locationSales.agentsRegionalSalesLocationWise){
						objLocaitons = locationSales.agentsRegionalSalesLocationWise[strLocation];
						strRowClass = "thinTop";					
						var y=0;
						for(y=0;y <objLocaitons.length;y++ ){
						strHTMLText += "<tr>";
						strHTMLText += "	<td width='20%' class='" + strRowClass + "' style='height:19px;' ><font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + objLocaitons[y].agentName  +"<\/font><\/td>";
						strHTMLText += "	<td width='10%' class='" + strRowClass + " ' align='right'><font>" + objLocaitons[y].target  +"<\/font><\/td>";
						strHTMLText += "	<td width='10%' class='" + strRowClass + " ' align='right'><font>" + addCommas(objLocaitons[y].strActualSales)  +"<\/font><\/td>";
						strHTMLText += "	<td width='10%' class='" + strRowClass + " ' align='right'><font>" + objLocaitons[y].ytdSales  +"<\/font><\/td>";
						strHTMLText += "	<td width='10%' class='" + strRowClass + " ' align='right'><font>" + objLocaitons[y].ytdSalesVariance  +"<\/font><\/td>";
						strHTMLText += "<\/tr>";	
						}
						
					}
					strHTMLText += "<\/table><\/div>";
					
					trMarket = document.createElement("TR");
					trMarket.appendChild(createCell({desc:strHTMLText}));
					tblMarket.appendChild(trMarket);
					
					arrTrrStatus["div_" + i + "_" + x] = false;
					x++;
				}
		
				i++;
			}	
		
	}
	
	UI_tabMISRegional.createPrintSalesData = function(){
	
	var objPrintTbl = getFieldByID("tblProdSale").tBodies[0];
	deleteTableRows(objPrintTbl);
	var printData = UI_tabMISRegional.productSalesData;
	var intLen = printData.length;
	var tblPrintRow = null;
	var strHTMLText = "";
	var i = 0;
	//if (intLen > 0){
		tblPrintRow = document.createElement("TR");
		tblPrintRow.appendChild(createCell({desc:"Regions", align:"center", bold:true, width:"20%"}));
		
		strHTMLText += "<table width='100%' border='0' cellapdding='0' cellspacing='0'>";
		strHTMLText += "	<tr>"
		strHTMLText += "		<td align='left'  width='34%'><font><b>Location<\/b><\/font><\/td>"
		strHTMLText += "		<td align='center' width='10%'><font><b>Trgt.<\/b><\/font><\/td>"
		strHTMLText += "		<td align='center' width='10%'><font><b>Act.<\/b><\/font><\/td>"
					strHTMLText += "		<td align='center' width='10%'><font><b>YTD<\/b><\/font><\/td>"
		strHTMLText += "		<td align='center' width='10%'><font><b>YTD Var.<\/b><\/font<\/td>"
		strHTMLText += "	<\/tr>"
		strHTMLText += "<\/table>"
		tblPrintRow.appendChild(createCell({desc:strHTMLText, width:"80%"}));
		
		objPrintTbl.appendChild(tblPrintRow);
			
		var strBGColor = "";
		var tblMarket = null;
		var trMarket = null;
		var strRegion = "";
		for(strRegion in printData){
			strBGColor = "";
			tblPrintRow = document.createElement("TR");				
			tblPrintRow.appendChild(createCell({desc:strRegion,bold:true, width:"20%"}));
			tblPrintRow.appendChild(createCell({desc:"<table id='tblPrintDetails_" + i + "' width='100%'><tbody><\/tbody><\/table>", width:"80%"}));
			objPrintTbl.appendChild(tblPrintRow);
			
			tblMarket = getFieldByID("tblPrintDetails_" + i).tBodies[0];	
			tblMarket.className = "sample";
			
			var objMarkets = printData[strRegion]; 
			strHTMLText = "";
			var x = 0;
			var strRowClass = "";
			var strLocWiseRegionalSales = "";
			for(strLocWiseRegionalSales in objMarkets){
				var locationSales = objMarkets[strLocWiseRegionalSales];
				trMarket = document.createElement("TR");
				strHTMLText = "<table width='100%' class='sample'>"
				strHTMLText += "	<tr>";
				strHTMLText += "		<td width='30%' ><font><b>" + locationSales.location + "<\/b><\/font><\/td>";
				strHTMLText += "		<td width='10%' align='right'><font>" + locationSales.target  +"<\/font><\/td>";
				strHTMLText += "		<td width='10%'  align='right'><font>" + addCommas(locationSales.strActualSales)+"<\/font><\/td>";
				strHTMLText += "		<td width='10%'  align='right'><font>" + locationSales.ytdSales  +"<\/font><\/td>";
				strHTMLText += "		<td width='10%'  align='right'><font>" + locationSales.ytdVariance  +"<\/font><\/td>";
				strHTMLText += "	<\/tr>";
				strHTMLText += "<\/table>";
				
				trMarket.appendChild(createCell({desc:strHTMLText}));
				tblMarket.appendChild(trMarket);
				
				var strLocation ="";
				var objLocaitons = null;
				var intLocaiton = locationSales.agentsRegionalSalesLocationWise.length;
				
				var strLocClass = "";
				strHTMLText = "<table width='100%' class='sample'>"
				for(strLocation in locationSales.agentsRegionalSalesLocationWise){
					objLocaitons = locationSales.agentsRegionalSalesLocationWise[strLocation];					
					var y=0;
					for(y=0;y <objLocaitons.length;y++ ){
					strHTMLText += "<tr>";
					strHTMLText += "	<td width='30%'  ><font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + objLocaitons[y].agentName  +"<\/font><\/td>";
					strHTMLText += "	<td width='10%'  align='right'><font>" + objLocaitons[y].target  +"<\/font><\/td>";
					strHTMLText += "	<td width='10%' align='right'><font>" +  addCommas(objLocaitons[y].strActualSales)  +"<\/font><\/td>";
					strHTMLText += "	<td width='10%' align='right'><font>" + objLocaitons[y].ytdSales  +"<\/font><\/td>";
					strHTMLText += "	<td width='10%'  align='right'><font>" + objLocaitons[y].ytdSalesVariance  +"<\/font><\/td>";
					strHTMLText += "<\/tr>";	
					}
					
				}
				strHTMLText += "<\/table>";
				
				trMarket = document.createElement("TR");
				trMarket.appendChild(createCell({desc:strHTMLText}));
				tblMarket.appendChild(trMarket);
				
				
				x++;
			}
	
			i++;
		}
		
//		$("#divPintSalesData").show();
	}
	
	
	
	
	function tablTreeOnClick(intR, intM){
		var strID = "div_" + intR + "_" + intM;
		var strImg = "";
		if (arrTrrStatus[strID]){
			arrTrrStatus[strID] = false;
			strImg = "M030_no_cache.gif";
		}else{
			arrTrrStatus[strID] = true;
			strImg = "M031_no_cache.gif";
		}
		getFieldByID("img_" + intR + "_" + intM).src = "../../images/" + strImg;
		setDisplay(strID, arrTrrStatus[strID]);
	}
		
	UI_tabMISRegional.createRegional6Months3D =  function() {
		var objValues = UI_tabMISRegional.regionalSalesTrend;
		var regionNames = UI_tabMISRegional.regions;
		var strRegion="";
		var data = new google.visualization.DataTable(); 
		data.addColumn('string', 'Month'); 
		//Add Regions - Columns
		for (var i=0; i<regionNames.length;i++){
			data.addColumn('number', regionNames[i]); 
		}
		data.addRows(6); 
		//Add months
		var x = 0;
		for (strMonth in objValues){
			var regionalSales = objValues[strMonth];
			data.setValue(x, 0, strMonth); 
			var y = 1;
			for(region in regionalSales){
				data.setValue(x, y,Number(regionalSales[region])); 
				y++;
			}
			x++;
		}
		
	        var chart = new google.visualization.LineChart(document.getElementById('imgRegi6MonthsRoll')); 
	        chart.draw(data, {width: 260, height: 217, legend: 'bottom', backgroundColor:'#f6f6f6'}); 
	
		}
	
	UI_tabMISRegional.createRegionalYearlySales = function(){
		var objValues = UI_tabMISRegional.regionYearlySales;	
		var data = new google.visualization.DataTable();
		data.addColumn('string', 'Year');
		data.addColumn('number', 'Sales');
		var i=0;
		for (strYear in objValues) {  
			data.addRow();
			data.setValue(i, 0, strYear);
			data.setValue(i, 1,objValues[strYear]);
			i++;
		} 

		UI_tabMISRegional.barsVisual = new google.visualization.ColumnChart(document.getElementById('imgRegiYearlySales'));
		UI_tabMISRegional.barsVisual.draw(data, 
				{ width:150, height:182 ,
			is3D:true,
			colors:[{color:'#FF6600', darker:'#e05a01'}],
			legend: 'none', backgroundColor:'#f6f6f6'
				}                   
		);
	}
	/***
	 * This is the graph displayed in the popup window
	 */
	UI_tabMISRegional.createPopUpGraph = function(){
		var mydata = UI_tabMISRegional.productSalesData;
		var data = new google.visualization.DataTable();
		data.addColumn('string', 'Station');
		data.addColumn('number', 'Sales');
		var i=0;
		for(region in  mydata){
			var objRegionalSales = mydata[region];
			for (city in objRegionalSales) {  
				var objCitySales = objRegionalSales[city];
				data.addRow();
				data.setValue(i, 0, city);
				data.setValue(i, 1,Number(objCitySales.actualSales));
				i++;
			} 
		}
		new google.visualization.ColumnChart(document.getElementById('divPopupGraph')).draw(data, 
				{ width:550, height:200 ,
			is3D:true,
			colors:[{color:'#669933', darker:'#55802b'}],
			legend: 'none', backgroundColor:'#f6f6f6'
				}                   
		);
			
	}
	UI_tabMISRegional.retriveAndDisplayRegionalData = function(){
		/*$.post('showMISRegional.action', hdnData, function(response) {
			UI_tabMISRegional.submitSuccess(response);
		},'json');*/
		
		$.ajax( {
			url : 'showMISRegional.action',
			type : 'POST',
			data : arrParamData,
			dataType : 'json',
			success : function(response) {
				UI_tabMISRegional.submitSuccess(response);
			},
			error : function(requestObj, textStatus, errorThrown) {
				handleCommonAjaxInvocationErrors(requestObj);
			}
		});
	}
	
	
	UI_tabMISRegional.submitSuccess = function(response){
		top[2].HideProgress();
		if(response.success) {
			UI_tabMISRegional.regionalSalesTrend   = response.regionalSalesTrend;
			UI_tabMISRegional.regionYearlySales    = response .regionYearlySales;
			UI_tabMISRegional.yearlyHighestValue   = response.yearlyHighestValue;
			UI_tabMISRegional.monthlyHighestValue  = response.monthlyHighestValue;
			UI_tabMISRegional.regions              = response.regions;
			UI_tabMISRegional.bestPerformingAgents = response.bestPerformingAgents;
			UI_tabMISRegional.lowPerformingAgents  = response.worstPerformingAgents;
			UI_tabMISRegional.productSalesData     = response.regionalProductSales;
			UI_tabMISRegional.avgAgentsSales       = response.avgAgentsSales;
			UI_tabMISRegional.totalActualSales     = response.totalSales;
	
			UI_tabMISRegional.createRegionalYearlySales();
//			UI_tabMISRegional.createRegional6Months();
			UI_tabMISRegional.createRegional6Months3D();
			UI_tabMISRegional.createRegionalAgentPerformance();
			UI_tabMISRegional.createRegionalProductSalesAnalysis();
			UI_tabMISRegional.createPrintSalesData();
			//UI_tabMISRegional.createPopUpGraph();
		} else {
			showERRMessage("Invalid response");	
		}
	}