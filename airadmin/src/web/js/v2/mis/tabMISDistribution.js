	
	function UI_tabMISDistribution(){}
	
	UI_tabMISDistribution.countries = null;
	UI_tabMISDistribution.countryWiseFlights = null;
	UI_tabMISDistribution.highestValue = null;
	UI_tabMISDistribution.salesChannels = null;
	UI_tabMISDistribution.channelWiseBookingPerCountry =  null;
	UI_tabMISDistribution.revenueByChannel = null;
	UI_tabMISDistribution.barsVisual;
	UI_tabMISDistribution.columnChartSF;
	UI_tabMISDistribution.columnChartYield;

	UI_tabMISDistribution.salesFactorMap = null;
	UI_tabMISDistribution.yieldMap = null;
	UI_tabMISDistribution.monthsMap = null;
	UI_tabMISDistribution.currMonth = null;	
	UI_tabMISDistribution.prevMonth = null;

	$(document).ready(function(){
		setDisplay("tdPerformanceOfPortals", false);
	});
	
	UI_tabMISDistribution.createPortalPerformance = function(){
		var objTbl = getFieldByID("tblDisPerformance").tBodies[0];
		deleteTableRows(objTbl);
		
		var intLen = jsPP.length; 
		var tblRow = null;
		var i = 0;
		
		tblRow = document.createElement("TR");
			
		tblRow.appendChild(createCell({desc:"Portal", bold:true, align:"center", css:"thinRight"}));
		tblRow.appendChild(createCell({desc:"Sales", bold:true, align:"center", css:"thinRight"}));
		tblRow.appendChild(createCell({desc:"% Contrib.", bold:true, align:"center", css:"thinRight"}));
		tblRow.appendChild(createCell({desc:"Bookings", bold:true, align:"center", css:"thinRight"}));
		tblRow.appendChild(createCell({desc:"Conversion", bold:true, align:"center", css:"thinRight"}));
		tblRow.appendChild(createCell({desc:"Commission", bold:true, align:"center", css:""}));
		
		objTbl.appendChild(tblRow);
		
		if (intLen > 0){
			var strBGColor = "";
			do{
				tblRow = document.createElement("TR");
				strBGColor = getRowClass(i);
				
				tblRow.appendChild(createCell({desc:jsPP[i].porta, css:"thinRight thinTop " + strBGColor}));
				tblRow.appendChild(createCell({desc:jsPP[i].sales, align:"right", css:"thinRight thinTop " + strBGColor}));
				tblRow.appendChild(createCell({desc:jsPP[i].cont, align:"right", css:"thinRight thinTop " + strBGColor}));
				tblRow.appendChild(createCell({desc:jsPP[i].bkg, align:"right", css:"thinRight thinTop " + strBGColor}));
				tblRow.appendChild(createCell({desc:jsPP[i].conv, align:"right", css:"thinRight thinTop " + strBGColor}));
				tblRow.appendChild(createCell({desc:jsPP[i].comm, align:"right", css:" thinTop " + strBGColor}));
				
				objTbl.appendChild(tblRow);
				
				i++;
			}while(--intLen);
		}
	}
	
	UI_tabMISDistribution.createChnlBkgPerformance = function() {

		var strHTMLText = "<table id='tblBkgChnl' width='100%' border='0' cellpadding='0' cellspacing='0' style='border-bottom:1px solid grey'>";
		strHTMLText += "<tr>";
		strHTMLText += "<td class='thinRight' align='center' width='100px'><font><b>Country<\/b><\/font><\/td>";
		strHTMLText += "<td class='thinRight' align='center' width='30px'><font><b>&nbsp;Stns&nbsp;<\/b><\/font><\/td>";
		strHTMLText += "<td class='thinRight' align='center' width='40px'><font><b>&nbsp;Flights&nbsp;<\/b><\/font><\/td>";
		strHTMLText += "<td class='thinRight' align='center' width='40px'><font><b>&nbsp;Agents&nbsp;<\/b><\/font><\/td>";
		strHTMLText += "<td class='thinRight' align='center'><font><b>Passenger Segment Count by Channel<\/b><\/font><\/td>";
		strHTMLText += "<\/tr>"; 

		for(var i=0; i < UI_tabMISDistribution.countryWiseFlights.length; i++) {

			var countryName = UI_tabMISDistribution.countryWiseFlights[i].country;
			if(countryName.length>12){
				countryName =  countryName.substring(0,12);
				countryName+= "..";
			}
			var strBGColor = getRowClass(i);
			strHTMLText += "<tr>";
			strHTMLText += "<td class='thinRight thinTop " + strBGColor + "' align='left'><font>" + countryName + "<\/font><\/td>";
			strHTMLText += "<td class='thinRight thinTop " + strBGColor + "' align='right'><font>" + UI_tabMISDistribution.countryWiseFlights[i].cities + "<\/font><\/td>";
			strHTMLText += "<td class='thinRight thinTop " + strBGColor + "' align='right'><font>" + UI_tabMISDistribution.countryWiseFlights[i].flights + "<\/font><\/td>";
			strHTMLText += "<td class='thinRight thinTop " + strBGColor + "' align='right'><font>" + UI_tabMISDistribution.countryWiseFlights[i].agents + "<\/font><\/td>";
			strHTMLText += "<td class='thinRight " + getRowClass(0) + "' align='left' style='border-top:1px dotted lightgrey'>";
			strHTMLText += "<table width='100%' border='0' cellpadding='0' cellspacing='0'><tr>";

			var remainingPer = 100;
			for(strSalesChannelCode in UI_tabMISDistribution.channelWiseBookingPerCountry){
				var channelWiseBooking = UI_tabMISDistribution.channelWiseBookingPerCountry[strSalesChannelCode];
				var chnWiseForCountry = channelWiseBooking[i];				
				var perValue = getTotalPercentageValue(chnWiseForCountry.noOfBookings, UI_tabMISDistribution.highestValue);

				if(perValue== null || perValue.isNaN) perValue = 0;
				if(perValue != 0) {
					strHTMLText += "<td width='" + perValue + "%' style='background-color:" + chnWiseForCountry.salesChannelColor + "'>&nbsp;<\/td>";
					remainingPer = remainingPer - perValue;
				}
			}
			if(remainingPer < 0) remainingPer = 0;
			if(remainingPer != 0){
				strHTMLText += "<td width='" + remainingPer + "%'>&nbsp;<\/td>";
			}
			strHTMLText += "<\/tr><\/table>";
			strHTMLText += "<\/td>";
			strHTMLText += "<\/tr>"; 
		}

		strHTMLText += "<\/table>";
		
		$("#divBkgChnl").empty();
		$("#divBkgChnl").append(strHTMLText);

		UI_tabMISDistribution.createGraphLegend();
	}
	
	function createRevenueByChannelGrid(){
		
		var objTbl = getFieldByID("tblBkgRevenueChnl").tBodies[0];
		deleteTableRows(objTbl);
		
		var objJS = jsRevChnl.summary
		var intLen = objJS.length; 
		var tblRow = null;
		var i = 0;
		if (intLen > 0){
			var strImg = "";
			do{
				tblRow = document.createElement("TR");
				
				if (objJS[i].upDown == "U"){
					strImg = "M050_no_cache.jpg";
				}else{
					strImg = "M051_no_cache.jpg";
				}
				
				tblRow.appendChild(createCell({desc:"<img src='../../images/" + strImg + "'/>"}));
				tblRow.appendChild(createCell({desc:objJS[i].perValue, css:"htGraph", align:"right"}));
				
				objTbl.appendChild(tblRow);
				
				i++;
			}while(--intLen);
		}
	}
	
	
	/** Creates Revenue by channel graph */
	UI_tabMISDistribution.createRevenueByChannel = function(){

		var data = new google.visualization.DataTable();
	    	data.addColumn('string', 'Booking Channel');
	    	data.addColumn('number', 'Passenger Segment Count');

		var cnt = 0;
		for(strChannel in UI_tabMISDistribution.revenueByChannel) {
			data.addRow();
			data.setValue(cnt, 0, strChannel);
			data.setValue(cnt, 1, UI_tabMISDistribution.revenueByChannel[strChannel]);
			cnt++;
		}
		
		var barHeight = 200;
		if(cnt<4){
			barHeight = cnt * 40+70;
		}
		
		UI_tabMISDistribution.barsVisual = new google.visualization.BarChart(document.getElementById('imgDistReveByChannelWise'));
		UI_tabMISDistribution.barsVisual.draw(data, 
	            { width:670, height:barHeight , is3D:true, colors:[{color:'#87c945', darker:'#73ad3a'}],
		      legend: 'none', hAxis:{baseline:0}, backgroundColor:'#f6f6f6',tooltipFontSize:20
	            }                   
	        );

                if (UI_tabMISDistribution.monthsMap!=null) {
		   UI_tabMISDistribution.currMonth = UI_tabMISDistribution.monthsMap['currentMonth'];
		   UI_tabMISDistribution.prevMonth = UI_tabMISDistribution.monthsMap['prevMonth'];

		
		}

		calledSFGraph ();
		
		calledYieldGraph();
	}
		
	/** Creates sales by channel graph for each country */
	UI_tabMISDistribution.createBookingsByChannelGraph2 = function(){
		
//		var chartHeight = UI_tabMISDistribution.countries.length*20+15;
		var chartHeight = $('#tblBkgChnl').height();
 		
		var strParams = "";
		var bkgValues = UI_tabMISDistribution.channelWiseBookingPerCountry;
		var strValues = "";
		var strCntry ="";
		var strSalesChannels ="";
		var strChannelColors="";
		var strSalesChannelCode = "";
		var maxValue ="0";
		for(strSalesChannelCode in bkgValues){
			var channelWiseBooking = bkgValues[strSalesChannelCode];
			var channelColor = "";
			for(var j=0; j < channelWiseBooking.length ;j++){	
				var totPerValue = "0";
				if(channelColor==""){
					channelColor = channelWiseBooking[j].salesChannelColor;
				}
				if(channelWiseBooking[j].noOfBookings>0){
					totPerValue = channelWiseBooking[j].noOfBookings;
				}	
				if(totPerValue>maxValue){
					maxValue = totPerValue;
				}
				strValues += totPerValue + ",";		
			}
			strValues = strValues.substring(0,strValues.length-1);
			strValues += "|";
			strChannelColors+=channelColor+",";		
		}
		
		
		for(var y =0;y<UI_tabMISDistribution.countries.length;y++){
			var cntryName = UI_tabMISDistribution.countries[y];
			if(cntryName.length>15){
				cntryName =  cntryName.substring(0,10);
			}
			strCntry+= cntryName+"|";
		}
		
		if(strValues.length>0){
			strValues        = strValues.substring(0,strValues.length-1);
		}
		if(strCntry.length>0){
			strCntry         = strCntry.substring(0,strCntry.length-1);
		}
		if(strChannelColors.length>0){
			strChannelColors = strChannelColors.substring(0,strChannelColors.length-1);
		}

		maxValue = maxValue+10;
		
		strParams = "cht=bhs";											// Graph Type
		strParams += "&chs=450x"+chartHeight	;						// Height x width
		strParams += "&chf=bg,s,f6f6f6";								// Background Colors
		strParams += "|c,s,f6f6f6";										// Graph area background color
		strParams += "&chco="+strChannelColors;
		strParams += "&chd=t:" + strValues     ;                       // Data  	
//		strParams += "&chd=t:10,20,30,0,0|5,15,25,0,0|5,10,15,0,0";
		strParams += "&chbh=13";											// 10 pixels
		strParams += "&chma=5,10,12,5"
		strParams += "&chds=0,160";		// Highest data
		strParams += "&chg=20,5,1,3";									// Grid
		strParams += "&chxt=y,x"; 										// Axis Data
		strParams += "&chxl=";
//		strParams += "0:|"+strCntry;
		strParams += "0:||";
		strParams += "&chxr=1,0,"+maxValue;
		
		getFieldByID("imgDistChannelWise").src = getGraphImage(strParams);
		UI_tabMISDistribution.createGraphLegend();
	}
	
	UI_tabMISDistribution.createBookingsByChannelGraph = function(){
		
		var bkgValues = UI_tabMISDistribution.channelWiseBookingPerCountry;
		var colors = new Array();
		var data = new google.visualization.DataTable();
	    	data.addColumn('string', 'Country');

		for(strSalesChannelCode in bkgValues){
			data.addColumn('number', strSalesChannelCode);	
		}

		for(var y =0;y<UI_tabMISDistribution.countries.length;y++){

			data.addRow();
			var cntryName = UI_tabMISDistribution.countries[y];
			data.setValue(y, 0, cntryName);
			
			var cnt = 1;
			for(strSalesChannelCode in bkgValues){
				var channelWiseBooking = bkgValues[strSalesChannelCode];
				data.setValue(y, cnt, parseInt(channelWiseBooking[y].noOfBookings));
				if(y == 0){
					colors[cnt-1] = {color:channelWiseBooking[y].salesChannelColor};
				}
				cnt++;
			}
		}
		
		var chartHeight = $('#tblBkgChnl').height()+20;
		var graph = new google.visualization.BarChart(document.getElementById('imgDistChannelWise'));
		graph.draw(data, 
	            { fontSize:'10px', width:450, height:chartHeight , is3D:false, isStacked:true,  
		      legend: 'none', hAxis:{baseline:0, title: "Passenger Segment Count"}, backgroundColor:'#f6f6f6'
	            }                   
	        );
	}


	/**
	 * Manually creates the graph legend (Sales by Channel graph)
	 */
	UI_tabMISDistribution.createGraphLegend = function(){
		var objTbl = getFieldByID("tblGraphLegend");
		deleteTableRows(objTbl);
		var tblRow = null;
		var i = 0;
		var values = UI_tabMISDistribution.salesChannels;
		var strSalesChannel ="";
		var strRowClass = 'lgndBd';
		tblRow = document.createElement("TR");
		for(strSalesChannel in values){
			var salesChnlObj = values[strSalesChannel];
			strHTMLText = "<td><table border='0' cellpadding='0' cellspacing='0'>"
				strHTMLText += "	<tr>";	
			strHTMLText += "		<td width='100%' align='center' class='" + strRowClass + " ' style='background-color:#"+salesChnlObj+"'><font>&nbsp;<\/font><\/td>";
			strHTMLText += "	<td>";
			strHTMLText += "	<font>&nbsp;"+strSalesChannel+"<\/font>";
			strHTMLText += "	<\/td>";
			strHTMLText += "	<\/tr>";
			strHTMLText += "<\/table><\/td>";
			
			tblRow.appendChild(createCell({desc:strHTMLText}));
		}
		
		objTbl.appendChild(tblRow);
	}
	
	function createDistriRevenueChannelWise(){
		var strParams = "";
		var strValues = ""
		
		var objJS = jsRevChnl.summary
		var intLen = objJS.length;
		if (intLen > 0){
			var strValues
			var i = 0;
			do{
				if (i != 0){strValues += ",";}
				strValues += getTotalPercentageValue(objJS[i].value, jsRevChnl.highestValue);
				
				i++;
			}while(--intLen);
		}
		
		strParams = "cht=bhg";								// Graph Type
		strParams += "&chs=400x200"							// Height x width
		strParams += "&chf=bg,s,F7F7F7";					// Background Colors
		strParams += "	|c,s,ffffff";						// Graph area background color
		strParams += "&chco=A4BDEF"							// Bar Colors
		strParams += "&chd=t:" + strValues;					// Data
		strParams += "&chbh=a";								// Auto Width
		strParams += "&chg=20,10,1,2";						// Grid
		strParams += "&chxt=x,y";							// X & Y Axis
		strParams += "&chxl=";								// Axis Data	
		strParams += "	0:||";
		strParams += "	1:|Call Centre|Travel Agents|GSA|GDS|Corporates|Web|";
		
		getFieldByID("imgDistReveByChannelWise").src = getGraphImage(strParams);
	}

	UI_tabMISDistribution.retriveAndDisplayDistributionData = function(){
		/*$.post('showMISDistribution.action', hdnData, function(response) {
			UI_tabMISDistribution.submitDistributionSuccess(response);
		},'json');*/

                //alert('retriveAndDisplayDistributionData...........');
		$.ajax( {
			url : 'showMISDistribution.action',
			type : 'POST',
			data : arrParamData,
			dataType : 'json',
			success : function(response) {
				UI_tabMISDistribution.submitDistributionSuccess(response);
			},
			error : function(requestObj, textStatus, errorThrown) {
				handleCommonAjaxInvocationErrors(requestObj);
			}
		});	
	}
	
	
	UI_tabMISDistribution.submitDistributionSuccess= function(response){
		top[2].HideProgress();
		if(response.success) {
			UI_tabMISDistribution.countries          = response.countryList;
			UI_tabMISDistribution.countryWiseFlights = response.countryWiseFlights;
			UI_tabMISDistribution.highestValue       = response.highestValue;
			UI_tabMISDistribution.salesChannels      = response.salesChannelList;
			UI_tabMISDistribution.channelWiseBookingPerCountry =  response.channelWiseBookingsPerCountry;
			UI_tabMISDistribution.revenueByChannel   = response.channelWiseRevenue;
			UI_tabMISDistribution.revHighestValue    = response.revHighestValue;

			UI_tabMISDistribution.salesFactorMap   = response.salesFactorMap;
			UI_tabMISDistribution.yieldMap  = response.yieldMap;
			UI_tabMISDistribution.monthsMap  = response.monthsMap;
			
			
//			UI_tabMISDistribution.createPortalPerformance();
			UI_tabMISDistribution.createChnlBkgPerformance();		
			//UI_tabMISDistribution.createBookingsByChannelGraph();
			UI_tabMISDistribution.createRevenueByChannel();
		} else {
			showERRMessage("Invalid response");		
		}
	}
	
	UI_tabMISDistribution.onReady = function(){
		UI_tabMISDistribution.createPortalPerformance();
		UI_tabMISDistribution.createChnlBkgPerformance();
		UI_tabMISDistribution.createDistriChannelWise();
	}



function refreshGraph(arg) {
  if (arg == '1')
    calledSFGraph();
  else if (arg == '2')
    calledYieldGraph();
}

function calledYieldGraph () {
		cnt = 0;
                index = 0;
		inner = null;
		var maxYieldHeight =  100;
		var data1 = new google.visualization.DataTable();
		data1.addColumn('string', 'Year');
		data1.addColumn('number', 'Current Month Yield');
		data1.addColumn('number', 'Previous Month Yield');

                
		for(strChannel in UI_tabMISDistribution.yieldMap) {
			data1.addRow();
			//data1.setValue(cnt, 0, strChannel + " " + UI_tabMISDistribution.currMonth + " - " + (strChannel-1) + " " + UI_tabMISDistribution.prevMonth);
			data1.setValue(cnt, 0, strChannel);
			//alert('strChannel:' + strChannel);
			inner =  UI_tabMISDistribution.yieldMap[strChannel];
			index = 1;

			data1.setValue(cnt, index, inner[UI_tabMISDistribution.currMonth]);
			index++;
			data1.setValue(cnt, index, inner[UI_tabMISDistribution.prevMonth]);

			if (inner[UI_tabMISDistribution.currMonth]>100)
			   maxYieldHeight = inner[UI_tabMISDistribution.currMonth];
                        if (inner[UI_tabMISDistribution.prevMonth]>100)
			   maxYieldHeight = inner[UI_tabMISDistribution.prevMonth];
/*
			for(strIndex in inner) {
			    alert(strChannel +' | strIndex:' + strIndex + ' | ' + inner[strIndex]);
			    data1.setValue(cnt, index, inner[strIndex]);
			    if (inner[strIndex] > 100)
				   maxYieldHeight = inner[strIndex];
			    index++;
			}
*/
		   cnt++;
		}
		
		if (maxYieldHeight > 100 && maxYieldHeight <= 1000)  maxYieldHeight = 1000;
		else if (maxYieldHeight > 1000 && maxYieldHeight <= 10000) maxYieldHeight = 10000;
		else if (maxYieldHeight > 10000 && maxYieldHeight <= 100000) maxYieldHeight = 100000;
		else if (maxYieldHeight > 100000 && maxYieldHeight <= 1000000) maxYieldHeight = 1000000;
		else if (maxYieldHeight > 1000000 && maxYieldHeight <= 10000000) maxYieldHeight = 10000000;

                //alert(maxYieldHeight);
		UI_tabMISDistribution.columnChartSF = new google.visualization.ColumnChart(document.getElementById('imgDistReveByChannelWise2'));
		UI_tabMISDistribution.columnChartSF.draw(data1, {width: 600, max : maxYieldHeight, height: 240, is3D:true, vAxis: {logScale :true},
			  hAxis: {title: 'Year', titleTextStyle: {color: 'red'}} , backgroundColor:'#f6f6f6' ,
                                                  tooltipFontSize:20
                        });
}

function calledSFGraph () {

          var data2 = new google.visualization.DataTable();
			data2.addColumn('string', 'Year');
			data2.addColumn('number', 'Current Month SF%');
			data2.addColumn('number', 'Previous Month SF%');

		cnt = 0;
                var index = 0;
		var inner = null;
                var maxHeight =  100;
		for(strChannel in UI_tabMISDistribution.salesFactorMap) {
			data2.addRow();
			//data2.setValue(cnt, 0, strChannel + " " + UI_tabMISDistribution.currMonth + " - " + (strChannel-1) + " " + UI_tabMISDistribution.prevMonth);
			data2.setValue(cnt, 0, strChannel);
			//alert('strChannel:' + strChannel);
			inner =  UI_tabMISDistribution.salesFactorMap[strChannel];
			index = 1;

			data2.setValue(cnt, index, inner[UI_tabMISDistribution.currMonth]);
			index++;
			data2.setValue(cnt, index, inner[UI_tabMISDistribution.prevMonth]);

			if (inner[UI_tabMISDistribution.currMonth]>100)
			   maxHeight = inner[UI_tabMISDistribution.currMonth];
                        if (inner[UI_tabMISDistribution.prevMonth]>100)
			   maxHeight = inner[UI_tabMISDistribution.prevMonth];

			
			
		   cnt++;
		}
	
		if (maxHeight > 100) {
			maxHeight = (Math.round(maxHeight/10)*10);
		}
	  

		UI_tabMISDistribution.columnChartSF = new google.visualization.ColumnChart(document.getElementById('imgDistReveByChannelWise1'));
		UI_tabMISDistribution.columnChartSF.draw(data2, {width: 600, max : maxHeight, height: 240, is3D:true, 
			  hAxis: {title: 'Year', titleTextStyle: {color: 'red'}} , backgroundColor:'#f6f6f6' ,
                                                  tooltipFontSize:20
                        });
}


	