	/*
	*********************************************************
		Description		: Reports MIS
		Author			: Rilwan A. Latiff
		Version			: 1.0
		Created on		: 15th October 2009
		NOTE			: This code write only for the demo purpose 
						  DONT use it if you are palling to integrate with the back-end
	*********************************************************	
	*/

	var jsRetrieveData = ({tab1:false, tab2:true, tab3:true, tab4:true, tab5:true, tab6:true, tab7:true})

	var hdnCntryCode = "";
	var hdnAgentCode = "";
	var hdnBranchCode = "";
	var hdnSrchData = "";
	var hdnData = "";
	var hdnMode = "";
	var arrParamData = {};
	
	var jsCntrl = [
	               	{id:"selFrom", desc:"From", required:false},
	               	{id:"selTo", desc:"To", required:false},
					{id:"selRegion", desc:"Region", required:false},
					{id:"selPOS", desc:"POS", required:false},
					{id:"txtFDate", desc:"From Date", required:false},
	               	{id:"txtTDate", desc:"To Date", required:false},
					];
	
	var objCal = new Calendar();
	objCal.onClick = "setDate";
	objCal.buildCalendar();
	
	UI_MISReports.feedBackSource = null;
	UI_MISReports.baseCurrency = "";

	/*
	 * Onload Function
	 */
	$(document).ready(function(){		
		parent.applyTabStyle(0);
		top[2].HideProgress();
		UI_MISReports.initilaize();

		$("#btnSearch").click( function(){
			UI_MISReports.searchButtonClicked();
		});
		
		$("#btnReset").click( function(){
			UI_MISReports.resetButtonClicked();
		});
		
		$("#btnCancel").click( function(){
			UI_MISReports.cancelButtonClicked();
		});
		
		$("#selRegion").change(function(){
			UI_MISReports.onRegionChange();
		});	
			
		UI_MISReports.loadData();
		UI_tabMISAgents.onReady();
		UI_MISReports.initializeDates();
		UI_MISReports.disableTabs();

		$("#txtFDate").datepicker({ maxDate: new Date(), dateFormat: 'dd/mm/yy', changeMonth: 'true' , changeYear: 'true', showButtonPanel: 'true' });
		$("#txtTDate").datepicker({ maxDate: new Date(), dateFormat: 'dd/mm/yy', changeMonth: 'true' , changeYear: 'true', showButtonPanel: 'true' });

		$('#txtFDate').change(function (){
			var strTime = this.value;
			dateChk('txtFDate');	
		});	
		$('#txtTDate').change(function (){
			var strTime = this.value;
			dateChk('txtTDate');	
		});
	});
	
	
	function UI_MISReports(){}

	UI_MISReports.initilaize = function() {
		setDisplay("divResultPane", false);
		getFieldByID("frmAction").reset();		
		jsRetrieveData = ({tab1:false, tab2:true, tab3:true, tab4:true, tab5:true, tab6:true, tab7:false});	
	}

	
	UI_MISReports.searchButtonClicked = function() {
		if(UI_MISReports.validate()){
			hdnMode = "";
			UI_MISReports.buildSearchData();
			UI_MISReports.buildSubmitData();
			UI_MISReports.switchToTabSearch(hdnMode);	
		}
	}

	UI_MISReports.switchToTabSearch = function(hdnMode) {
		top[2].ShowProgress();
		switch (hdnMode){
		case "TAB_2" :
			UI_tabMISDistribution.retriveAndDisplayDistributionData();
			break;

		case "TAB_3" :
			UI_tabMISRegional.retriveAndDisplayRegionalData();
			break;
				
		case "TAB_4" :
			UI_tabMISAgents.retriveAndDisplayGSAList();
			break;

		case "TAB_5" :
			UI_tabMISAccounts.retriveAndDisplayAccountsData();
			break;

		case "TAB_6" :
			UI_tabMISAncillary.retriveAndDisplayAncillaryData();
			break;
			
		case "TAB_7" :
			alert("TAB_7")
			break;
		default :
			UI_tabMISDashBoard.retriveAndDisplayDashBoardData();
		}
		
	}
	
	UI_MISReports.resetButtonClicked=function(){
		UI_MISReports.initilaize();
		UI_MISReports.initializeDates();
	}
	
	UI_MISReports.cancelButtonClicked=function(){
		top[1].objTMenu.tabRemove(parent.screenId);
	}
	
	/*
	 * TODO
	 */
	UI_MISReports.validate = function() {

		if($('#txtFDate').val() == null || $('#txtFDate').val() == "") {
			showCommonError("Error", "Please enter start date.");
			return false;
		} else {
			var dateFilter = /^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/;
			if(!dateFilter.test($('#txtFDate').val())) {
				showCommonError("Error", "Please enter valid start date.");
				return false;
			}
		}

		if($('#txtTDate').val() == null || $('#txtTDate').val() == "") {
			showCommonError("Error", "Please enter end date.");
			return false;
		} else {
			var dateFilter = /^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/;
			if(!dateFilter.test($('#txtTDate').val())) {
				showCommonError("Error", "Please enter valid end date.");
				return false;
			}
		}
		
		var startDate = $('#txtFDate').val();		
		var endDate = $('#txtTDate').val();
		if(!checkForFutureDates(startDate)){
			showCommonError("Error", "Start date cannot be a future date.");
			return false;
		}
		
		if(!checkForFutureDates(endDate)){
			showCommonError("Error", "End date cannot be a future date.");
			return false;
		}
		
		var startDateYY = startDate.substring(6);
		var startDateMM = startDate.substring(3,5);
		var startDateDD = startDate.substring(0,2);

//		var dateStartDate = new Date(parseInt(startDate.substring(6)) , parseInt(startDate.substring(3,5)) , parseInt(startDate.substring(0,2)));
		var dateStartDate = new Date(startDateYY , startDateMM , startDateDD);
			
		var endDateYY = endDate.substring(6);
		var endDateMM = endDate.substring(3,5);
		var endDateDD = endDate.substring(0,2);
		
//		var dateEndDate = new Date(parseInt(endDate.substring(6)) , parseInt(endDate.substring(3,5)) , parseInt(endDate.substring(0,2)));
		var dateEndDate = new Date(endDateYY , endDateMM , endDateDD);
		
		if(DATA_MIS["initialParams"].misMaxSearchPeriod <  Math.floor((dateEndDate - dateStartDate) / 86400000)) {
			showCommonError("Error", "Maximum search period exceeded.");
			return false;
		}

		var intStartDate = parseInt(startDate.substring(6) + startDate.substring(3,5) + startDate.substring(0,2));
		var intEndDate = parseInt(endDate.substring(6) + endDate.substring(3,5) + endDate.substring(0,2));
		if(intStartDate > intEndDate){
			showCommonError("Error", "Start date cannot be grater than end date.");
			return false;
		}


		if ((getValue("selFrom") != "") && (getValue("selTo") != "")){
			if (getValue("selFrom") == getValue("selTo")) {
				showCommonError("Error", "From & To cannot be same");
				return false;
			}
		}
		
		if ((getValue(jsCntrl[4].id) != "") || (getValue(jsCntrl[5].id) != "")){
			if (!CheckDates(getValue(jsCntrl[4].id), getValue(jsCntrl[5].id))){
				showERRMessage(jsCntrl[4].desc +  " should be less than " + jsCntrl[5].desc);		
				setFocus(jsCntrl[5].id)
				return false;
			}
		}
		checkForFutureDates();
		return true;
	}
	
	UI_MISReports.buildSearchData = function(){
		hdnSrchData = ""
		hdnSrchData += "&fromLocation=" + getValue("selFrom");
		hdnSrchData += "&toLocation=" + getValue("selTo");
		hdnSrchData += "&region=" + getValue("selRegion");
		hdnSrchData += "&pos=" + getValue("selPOS");
		hdnSrchData += "&fromDate=" + getValue("txtFDate");
		hdnSrchData += "&toDate=" + getValue("txtTDate");
		
		arrParamData['fromLocation'] = getValue("selFrom");
		arrParamData['fromLocation'] = getValue("selFrom");
		arrParamData['toLocation'] = getValue("selTo");
		arrParamData['region'] = getValue("selRegion");
		arrParamData['pos'] = getValue("selPOS");
		arrParamData['fromDate'] = getValue("txtFDate");
		arrParamData['toDate'] = getValue("txtTDate");
		
		jsRetrieveData = ({tab1:false, tab2:true, tab3:true, tab4:true, tab5:true, tab6:true,tab7:false})
	}
	
	UI_MISReports.buildSubmitData = function() {
		
		hdnData = hdnSrchData;
		hdnData += "&hdnMode=" + hdnMode;
		hdnData += "&hdnAgentCode=" + hdnAgentCode;
		hdnData += "&hdnBranchCode=" + hdnBranchCode;
		hdnData += "&hdnCntryCode=" + hdnCntryCode;
		
		arrParamData['hdnMode'] = hdnMode;
		arrParamData['hdnAgentCode'] = hdnAgentCode;
		arrParamData['hdnBranchCode'] = hdnBranchCode;
		arrParamData['hdnCntryCode'] = hdnCntryCode;
	}
	
	
	UI_MISReports.submitSuccess = function(response){
		eval(response.returnData);
		top[2].HideProgress();
		UI_MISReports.tblClick(1);
		dashBoardOnLoad();
		setDisplay("divResultPane", true);
		
	}

	UI_MISReports.submitAgentsSuccess = function(response){
		eval(response.returnData);
		top[2].HideProgress();
		agentsOnLoad();
	}
	
		
	UI_MISReports.tblClick = function(intIndex){
		hideTT();
		applyTabStyle(intIndex)
		if (intIndex != 7){
			parent.applyTabStyle(intIndex - 1);
		}
		top[2].HidePageMessage();
		var blnTabStatus = eval("jsRetrieveData.tab" + intIndex);
		if (blnTabStatus){
			eval("jsRetrieveData.tab" + intIndex + " = false;");
			hdnMode = "TAB_" + intIndex;
			UI_MISReports.buildSubmitData();
			UI_MISReports.switchToTabSearch(hdnMode);
		}
	}
	
	UI_MISReports.disableTabs = function(){
		/* TODO: Check whether this also should be disabled from a privilege
		if(!DATA_MIS.initialParams.misDashboard ==1){
			setTabsDisplay("tdTab1", false);
		}*/
		if(!DATA_MIS.initialParams.misDistribution ==1){
			setTabsDisplay("tdTab2", false);
		}
		if(!DATA_MIS.initialParams.misRegional ==1){
			setTabsDisplay("tdTab3", false);
		}
		if(!DATA_MIS.initialParams.misAgents ==1){
			setTabsDisplay("tdTab4", false);
		}
		if(!DATA_MIS.initialParams.misAccounts ==1){
			setTabsDisplay("tdTab5", false);
		}
		if(!DATA_MIS.initialParams.misAncillary ==1){
			setTabsDisplay("tdTab6", false);
		}
		if(!DATA_MIS.initialParams.misProfitability ==1){
			setTabsDisplay("tdTab7", false);
		}
	}
	
	function setTabsDisplay(strControlID, blnVisible){
		var objControl = getFieldByID(strControlID);
		if(objControl!=null){
			if (!blnVisible){
				objControl.style.display = "none";
			}else{
				objControl.style.display = "block";
			}
		}
	}

	
	/***
	 * Initializes date fields to current date- 1 month and current date
	 */
	UI_MISReports.initializeDates =  function(){
		var currDate  =  new Date();
		var month = currDate.getMonth() + 1;
		if (Number(month) < 10){month =  "0" + month;}
		var day = currDate.getDate();
		if (Number(day) < 10){day =  "0" + day;}
		var year = currDate.getFullYear();
		var strToDate = day + "/" + month + "/" + year;
		var strFromDate = addMonths(-1);
		
		setDate(strFromDate,"0");
		setDate(strToDate,"1");

	}

	//Common methods
	function applyTabStyle(intIndex){
		showHideFeedBack(false);

		for (var i = 1 ; i < 8 ; i++){		
			setDisplay("spnTabPane" + i, false);
			getFieldByID("tdTab" + i).style.backgroundImage = "url(../../images/M013_no_cache.jpg)";		
		}
		setDisplay("spnTabPane" + intIndex, true);
		getFieldByID("tdTab" + intIndex).style.backgroundImage = "url(../../images/M012_no_cache.jpg)";
		
		if (intIndex == 7){
			initProfitability();
		}

	}
	
	function getRowClass(intRow){
		var strClass = ""
		if ((intRow%2)!=0){
			strClass = "rowBGColor";
		}
		return strClass;
	}
	
	function getGraphImage(strParams){
		return "http://chart.apis.google.com/chart?" + strParams;
	}
	
	function getChartAxisLabels(strLabels){
		var splittedStr = strLabels.split("|");
		var labelStr ="";
		var x=0;	
		for(x=splittedStr.length-1;x>=0;x--){
			labelStr+=splittedStr[x]+"|";
		}
		labelStr  = labelStr.substring(0,labelStr.length-1);
		return labelStr;
	}
	
	/*
	 * Calendar Click
	 */
	function setDate(strDate, strID) {
		switch (strID) {
			case "0": setField(jsCntrl[4].id, strDate); break;
			case "1": setField(jsCntrl[5].id, strDate); break;
		}
	}
	
	
	/*
	 * Load Calendar
	 */
	function loadCalendar(strID, objEvent) {
		objCal.ID = strID;
		objCal.onClick = "setDate";
		objCal.showCalendar(objEvent);
	}
	
	/*
	 * Date On Blur
	 */
	function dateChange(strID){
		switch (strID){
			case 0 : dateOnBlur(jsCntrl[4].id, strID) ; break;
			case 1 : dateOnBlur(jsCntrl[5].id, strID) ; break;
		}
	}
	
	/*
	 * Date on Blur
	 */
	function dateOnBlur(strControl, intId){
		if (!dateChk(strControl))	{
			showERRMessage("Invalid date");		
		}
	}
	
	function pgTab0ButtonClick(intIndex){
		switch (intIndex){
			case 0 :
				showHideFeedBack(false);
				break;
			case 1 :
				UI_MISReports.sendEmail(false);
				break;
		}
	}					
	
	function showHideFeedBack(blnStatus){
		setDisplay("spnFeedBack", blnStatus);
	}
	
	function clearFeedback(){
		 $("#txtFeedBackTo").val("");
		 $("#txtFeedBack").val("");
	}
	
	function validateEmail(email) {
		var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		var addresses = email.split(',');
		for (var i = 0; i < addresses.length; i++) {
			if(reg.test(addresses[i]) == false) {
				showERRMessage("One or more invalid email address(es)");
				return false;
			}
		}
		return true;
	}
	
	UI_MISReports.sendEmail = function(blnStatus){
		if(UI_MISReports.feedBackSource!=null){
			if(UI_MISReports.feedBackSource =="regional"){
				UI_tabMISRegional.sendEmail(blnStatus);
			}
			else if(UI_MISReports.feedBackSource =="dashboard"){
				UI_tabMISDashBoard.sendEmail(blnStatus);
			}
		}
		clearFeedback();
	}
	
	UI_MISReports.loadData = function(){
		UI_MISReports.fillRegion();
		$("#selRegion").val('ALL').attr("selected", "selected");
		UI_MISReports.fillPos();
		$("#selPOS").val('ALL').attr("selected", "selected");	
		
	}
	
	UI_MISReports.onRegionChange = function(){
		if($("#selRegion").val()=="ALL"){
			UI_MISReports.fillPos();
			$("#selPOS").val('ALL').attr("selected", "selected");
		}
		else{
			/*var selectedRegion = "&region=" + $("#selRegion").val();
			$.post('showMISDashBoard!getStations.action', selectedRegion, function(response) {
				UI_MISReports.submitRegionSuccess(response);
			},'json');*/
			
			arrParamData['region'] =  $("#selRegion").val();
			
			$.ajax( {
				url : 'showMISDashBoard!getStations.action',
				type : 'POST',
				data : arrParamData,
				dataType : 'json',
				success : function(response) {
					UI_MISReports.submitRegionSuccess(response);
				},
				error : function(requestObj, textStatus, errorThrown) {
					handleCommonAjaxInvocationErrors(requestObj);
				}
			});
		}
		
	}
	
	UI_MISReports.fillRegion = function(){		
		var strHtml= '<option value="ALL">ALL</option>';
		for(var i=0; i<arrAllRegions.length;i++){
			var myArr = arrAllRegions[i];
			strHtml+= '<option value='+myArr[0]+'>'+myArr[1]+'</option>';
		}	
		strHtml+= '<option value="NET">Others</option>';
		$("#selRegion").html(strHtml); 

	}
	
	UI_MISReports.fillPos = function(){
		var strHtml= '<option value="ALL">ALL</option>';
		$("#selPOS").html(strHtml); 
	}
	
	UI_MISReports.submitRegionSuccess = function(response){
		if(response.success){
			var stations = response.stationsList;		
			var strHtml= '<option value="ALL">ALL</option>';
			var strStationCode ="";
			for(strStationCode in stations){
				strHtml+= '<option value='+strStationCode+'>'+stations[strStationCode]+'</option>';
			}			
			$("#selPOS").html(strHtml); 
		}
	}
	
	function addCommas(nStr)
	{
		nStr += '';
		x = nStr.split('.');
		x1 = x[0];
		x2 = x.length > 1 ? '.' + x[1] : '';
		var rgx = /(\d+)(\d{3})/;
		while (rgx.test(x1)) {
			x1 = x1.replace(rgx, '$1' + ',' + '$2');
		}
		return x1 + x2;
	}
	
	function checkForFutureDates(strDate){

		var currDate = new Date();
		var blnReturn = true;
		
		var month = currDate.getMonth() + 1;
		if (Number(month) < 10){month =  "0" + month;}
		var day = currDate.getDate();
		if (Number(day) < 10){day =  "0" + day;}
		var year = currDate.getFullYear();
		var strCurrDate = day + "/" + month + "/" + year;
		
		blnReturn =CheckDates(strDate+"/",strCurrDate+"/");
		
		return blnReturn;

	}

	// ------------------------------------- end of File ---------------------------------