	
	UI_tabMISAccounts.salesSummary             = null;
	UI_tabMISAccounts.outStandingInvoices      = null;	
	UI_tabMISAccounts.totalSales               = null;
	UI_tabMISAccounts.totalRefunds             = null;
	UI_tabMISAccounts.outStandingByRegionTotal = null;
	UI_tabMISAccounts.outStandingByRegion      = null;
	UI_tabMISAccounts.totalOutstandingCredits  = null;
	UI_tabMISAccounts.barsVisual;

	function UI_tabMISAccounts(){}
	
	UI_tabMISAccounts.retriveAndDisplayAccountsData = function(){
		
		$.ajax( {
			url : 'showMISAccounts.action',
			type : 'POST',
			data : arrParamData,
			dataType : 'json',
			success : function(response) {
				UI_tabMISAccounts.submitAccountsSuccess(response);
			},
			error : function(requestObj, textStatus, errorThrown) {
				handleCommonAjaxInvocationErrors(requestObj);
			}
		});
		
		/*$.post('showMISAccounts.action', hdnData, function(response) {
			UI_tabMISAccounts.submitAccountsSuccess(response);
		},'json');*/
	
	}
	
	UI_tabMISAccounts.submitAccountsSuccess = function(response){
		
		top[2].HideProgress();
		if(response.success) {
			UI_tabMISAccounts.salesSummary             = response.accPaymentsNRefundsSummary;	
			UI_tabMISAccounts.outStandingInvoices      = response.outStandingInvoices
			UI_tabMISAccounts.totalSales               = response.totalSales;
			UI_tabMISAccounts.totalRefunds             = response.totalRefunds;
			UI_tabMISAccounts.outStandingByRegionTotal = response.outStandingByRegionTotal;
			UI_tabMISAccounts.outStandingByRegion      = response.outStandingByRegion;
			UI_tabMISAccounts.totalOutstandingCredits  = response.totalOutstandingCredits;
		
			UI_tabMISAccounts.createSalesSummaryInfo();
			UI_tabMISAccounts.createSalesSummaryGraph();
			UI_tabMISAccounts.createOutStandingInvoicesGrid();
			UI_tabMISAccounts.createSalesSummaryData();
			UI_tabMISAccounts.createOutStandingAmountsByRegion();
		}
		else{
			showERRMessage("Invalid response");	
		}
		
	}
	
	UI_tabMISAccounts.createSalesSummaryInfo = function(){
		var objTbl = getFieldByID("tblSalesSummary").tBodies[0];
		deleteTableRows(objTbl);
		var salesSummaryData = UI_tabMISAccounts.salesSummary;

		var tblRow = document.createElement("TR");
		var revenueData = salesSummaryData["revenue"];
		var refund      = salesSummaryData["refund"];
		
		var type = "";
		tblRow.appendChild(createCell({desc:"", bold:true, align:"center", css:"thinRight"}));
		tblRow.appendChild(createCell({desc:"Sales", bold:true, align:"center", css:"thinRight"}));
		tblRow.appendChild(createCell({desc:"Refund", bold:true, align:"center", css:"thinRight"}));
		objTbl.appendChild(tblRow);
		var i = 0;
		for(type in revenueData){
			var strBGColor = getRowClass(i);
			if(revenueData[type]>0 ||refund[type]>0){
				tblRow = document.createElement("TR");
				tblRow.appendChild(createCell({desc:type, bold:true, align:"left", css:"thinRight thinTop "+ strBGColor}));
				tblRow.appendChild(createCell({desc:addCommas(revenueData[type]),align:"right", css:"thinRight thinTop "+ strBGColor }));
				tblRow.appendChild(createCell({desc:addCommas(refund[type]),align:"right", css:"thinRight thinTop "+ strBGColor }));
				objTbl.appendChild(tblRow);
				i++;
			}
		}
		
	}
	
	UI_tabMISAccounts.createSalesSummaryGraph =  function(){
		var salesSummaryData = UI_tabMISAccounts.salesSummary;
		var strParams = "";
		var strValues = "";
		var revenueData = salesSummaryData["revenue"];
		var refund      = salesSummaryData["refund"];
		var typesStr = "";
		var type = "";	
		
		
		var data = new google.visualization.DataTable();
	    data.addColumn('string', 'Channel',8);
	    data.addColumn('number', 'Sales',8);
	    data.addColumn('number', 'Refund',8);
	    var l = 0;
	    for(type in revenueData){
	    	++l;
		}
	   	       
	    data.addRows(l);	//set column count	    
	    
	    //channel
	    var k = 0;
		type = "";	
	    for(type in revenueData){
	    	data.setValue(k, 0, type);		//set payment types
		data.setValue(k, 1, Number(revenueData[type]));	//set revenue 
		data.setValue(k, 2, Number(refund[type])); 	//set refund
	    	k++;
		}
	    	    
	    UI_tabMISAccounts.barsVisual = new google.visualization.BarChart(document.getElementById('imgSales'));
	    UI_tabMISAccounts.barsVisual.draw(data, 
	            { width:400, height:145 ,
	        	isStacked:true,
	        	is3D:true,
	        	colors:[{color:'#669933', darker:'#55802b'}, {color:'#ffd800', darker:'#ffc600'}],
			legend: 'bottom', backgroundColor:'#f6f6f6'
	        	}                   
	        );
	    	
	}
	
	UI_tabMISAccounts.createOutStandingInvoicesGrid =  function(){
		var invoiceData = UI_tabMISAccounts.outStandingInvoices;
		var objTbl = getFieldByID("tblPastInv").tBodies[0];
		deleteTableRows(objTbl);
		if(invoiceData.length>0){
			var tblRow = document.createElement("TR");
			tblRow.appendChild(createCell({desc:"Agency Name", bold:true, align:"center", css:"thinRight"}));
			tblRow.appendChild(createCell({desc:"Invoice No", bold:true, align:"center", css:"thinRight"}));
			tblRow.appendChild(createCell({desc:"O/S Invoice Amount", bold:true, align:"center", css:"thinRight"}));
			tblRow.appendChild(createCell({desc:"Date", bold:true, align:"center", css:"thinRight"}));
			objTbl.appendChild(tblRow);
			var i = 0;

			for(i=0;i<invoiceData.length;i++){
				tblRow = document.createElement("TR");
				var invoiceObj = invoiceData[i];
				var strBGColor = getRowClass(i);
				tblRow.appendChild(createCell({desc:invoiceObj.agentName,  align:"left",css:"thinRight thinTop "+ strBGColor}));
				tblRow.appendChild(createCell({desc:invoiceObj.invoiceNumber,  align:"right",css:"thinRight thinTop "+ strBGColor}));
				tblRow.appendChild(createCell({desc:addCommas(invoiceObj.invoiceAmount), align:"right", css:"thinRight thinTop "+ strBGColor}));
				tblRow.appendChild(createCell({desc:invoiceObj.invoiceDate,  align:"right", css:"thinRight thinTop "+ strBGColor}));
				objTbl.appendChild(tblRow);
			}
		}
	}
	
	UI_tabMISAccounts.createSalesSummaryData =  function(){
		$("#divTotalRefunds").text(UI_tabMISAccounts.totalRefunds);
		$("#divTotalSales").text(UI_tabMISAccounts.totalSales);
		$("#divTotalCreditAmt").text(UI_tabMISAccounts.totalOutstandingCredits);
		
	}
	
	UI_tabMISAccounts.createOutStandingAmountsByRegion =  function(){

		var objTbl = getFieldByID("tblOutPayments").tBodies[0];
		deleteTableRows(objTbl);
		var regionData = UI_tabMISAccounts.outStandingByRegion;
		if(regionData!=null && regionData!=""){
			var tblRow = document.createElement("TR");
			tblRow.appendChild(createCell({desc:"Region", bold:true, align:"center", css:"thinRight"}));
			tblRow.appendChild(createCell({desc:" % ", bold:true, align:"center", css:"thinRight"}));
			tblRow.appendChild(createCell({desc:"Amount", bold:true, align:"center", css:"thinRight"}));
			objTbl.appendChild(tblRow);
			var i = 0;
			var region = "";
			for(region in regionData ){
				tblRow = document.createElement("TR");
				var strBGColor = getRowClass(i);
				tblRow.appendChild(createCell({desc:region,  align:"left",css:"thinRight thinTop "+ strBGColor}));
				var percentageVal = 0; 
				if(regionData[region]>0){	
					percentageVal = getTotalPercentageValue(Math.ceil(regionData[region]),Math.ceil(UI_tabMISAccounts.outStandingByRegionTotal));
					if(percentageVal==null || percentageVal==""){
						percentageVal = "<0.1";
					}
				}
				tblRow.appendChild(createCell({desc:percentageVal,  align:"right",css:"thinRight thinTop "+ strBGColor}));
				tblRow.appendChild(createCell({desc:addCommas(regionData[region]), align:"right", css:"thinRight thinTop "+ strBGColor}));
				objTbl.appendChild(tblRow);
				i++;

			}
		}
	}
	
