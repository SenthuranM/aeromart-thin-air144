var termsTemplates = {};
var currentlySelectedTemplateID = null;
var openedWindow = null;
var newSearch = false;
jQuery(document).ready(
		function() {
			top[2].HideProgress();
			var typeFomatter = function(cellVal, options, rowObject) {
				var treturn = "&nbsp;";
				if (cellVal != null) {
					if (cellVal == "G") {
						treturn = "Gift Voucher";
					} else if (cellVal == "V") {
						treturn = "Voucher";
					}
				}
				return treturn;
			}
			jQuery('#termsTable').jqGrid(
					{
						url : "showVoucherTermsTemplate.action",
						postData : {
							'searchCriteria.name' : function() {
								return $("#selName").val();
							},
							'searchCriteria.language' : function() {
								return $("#selLanguage").val();
							},
							'newSearch' : function() {
								return newSearch;
							}
						},
						datatype : 'json',
						jsonReader : {
							root : 'termsTemplates',
							page : "page",
							total : "totalPages",
							records : "totalRecords",
							repeatitems : false,
							id : 'templateID'
						},
						colNames : [ 'Template Name', 'Language', 'Type',
								 'ID', 'Version' ],
						colModel : [ {
							name : 'templateName',
							jsonmap : "templateName"
						}, {
							name : 'language',
							jsonmap : 'language'
						}, {
							name : 'type',
							jsonmap : 'voucherType',
							formatter : typeFomatter
						}, {
							name : 'templateID',
							jsonmap : 'templateID',
							align : "center",
							hidden : true
						}, {
							name : 'version',
							jsonmap : 'version',
							align : "center",
							hidden : true
						} ],
						rowNum : 20,
						viewRecords : true,
						height : 450,
						width : 920,
						pager : '#termsPager',
						afterInsertRow : function(rowid, rowdata, rowelement) {
							termsTemplates[rowelement.templateID] = rowelement;
						},
						onSelectRow : function(rowid, status, e) {
							currentlySelectedTemplateID = rowid;
						}
					});

			$("#btnEdit").decorateButton();
			$("#btnSearch").decorateButton();
			$("#btnClose").decorateButton();

			$("#btnEdit").click(function() {
				editTermsTemplate();
			});

			$("#btnSearch").click(function() {
				top[2].HidePageMessage();
				newSearch = true;
				$('#termsTable').trigger("reloadGrid");
				newSearch = false;
			});

			$("#btnClose").click(function() {
				top[1].objTMenu.tabRemove("SC_SHDS_038");
			});

			$("#tabSearchTerms").decoratePanelWithAlign(
					"Search Terms and Conditions");
			$("#tabTermsGrid").decoratePanelWithAlign("Terms and Conditions");

		});

/*
 * Closes the editing window and displays response message.
 */
templateEditingCompleted = function(data) {
	openedWindow.close();
	if (data.success) {
		showMsg("Success", "Voucher Terms and Conditions Updated Successfully.");
		$('#termsTable').trigger("reloadGrid");
	} else {
		showMsg("Error", data.messageTxt);
	}
}

/*
 * Opens a new browser window and load the html editor.
 */
editTermsTemplate = function() {
	top[2].HidePageMessage();
	if (currentlySelectedTemplateID == null) {
		alert("Please select a template to edit.");
		return;
	}

	var pageTitle = "Edit "
			+ termsTemplates[currentlySelectedTemplateID].templateName;

	openedWindow = window.open(
			'showLoadJsp!loadVoucherTermsTemplateEditPage.action', pageTitle,
			'width=800,height=900');
}
/*
 * Show message in the AirAdmin interface.
 */
showMsg = function(strType, strMsg) {
	top[2].objMsg.MessageText = strMsg;
	top[2].objMsg.MessageType = strType;
	top[2].ShowPageMessage();

}