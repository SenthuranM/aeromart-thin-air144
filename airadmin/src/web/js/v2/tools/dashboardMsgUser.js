/**
 * Dashboard Message Management User Popup JS
 * @author Navod Ediriweera
 */

UI_DashboardMsg_User.selectedUSers = {};
/**
 * Onload function
 */
$( function() {
	$("#divUserSelectionWrapper").decoratePanelWithoutTitle();
//	$("#divUserSearch").decoratePanel("Select Users");
	$("#divBottom").decoratePanelWithoutTitle();

	$('#btnSearchPop').decorateButton();
	$('#btnClosePop').decorateButton();
	$('#btnSavePop').decorateButton();
	
	$("#tblUserResults").jqGrid({ 
		url:'dashBoardMessageManagement!searchUserGridData.action',
		datatype: "json",	
		jsonReader : {
			  root: "userRows", 
			  page: "userPage",
			  total: "userTotal",
			  records: "userRecords",
			  repeatitems: false,					 
			  id: "0"				  
			},
		prmNames:{page:"userPage",rows:"userRows"},
		colNames:['&nbsp;','User ID', 'Display Name','Agent Code', 'Select'], 
		colModel:[ 	{name:'id', width:5, jsonmap:'id' },  
		           	{name:'pagedUser.userId', width:30, index:'userId' },  
		           	{name:'pagedUser.displayName',		index: 'displayName', 		width:50, align:"left" },
		           	{name:'pagedUser.agentCode',		index: 'agentCode', 		width:40, align:"left", hidden:true },	
		           	{name:'checkedUser',	index: 'checkedUser', 	width:10, align:"left",editable:true, edittype:"checkbox", editoptions: {value:"0:1"}}
					], 
		imgpath: '../../themes/default/images/jquery',
		multiselect:false,
		pager: $('#divUserResultsPager'),
		rowNum:10,						
		viewrecords: true,
		height:80,
		width:420,	
		recordtext:"",
		onSelectRow : function (rowid,status){				
				UI_DashboardMsg_User.rowSelect(rowid,status);
			},
	   	loadComplete: function (e){	   
			if(UI_Common.displayMsgObj.message){
				UI_Common.displayMessage(UI_Common.displayMsgObj);
				UI_Common.displayMsgObj.clearData();
			}		
			UI_DashboardMsg_User.onLoad();
	   	}
	}).navGrid("#divUserResultsPager",{refresh: true, edit: false, add: false, del: false, search: false});
	
	$('#btnSearchPop').click(function() { UI_DashboardMsg_User.searchUser(); });
	$('#btnClosePop').click(function() { UI_DashboardMsg_User.closePopup(); });
	$("#tblUserResults").clearGridData();
});

/**
 * UI_DashboardMsg for Dashboard Msg UI related functionalities
 */
function UI_DashboardMsg_User() {
}

UI_DashboardMsg_User.initilizeSearchInputs = function (){
	$('#frmUsersSearch').clearForm();
}

UI_DashboardMsg_User.clearGridOnly = function (){
	$("#tblUserResults").clearGridData();
}

UI_DashboardMsg_User.enableInputs = function (blnStatus){
	if(blnStatus) {
		$('#frmUsersSearch').enableForm();
	}else {
		$('#frmUsersSearch').disableForm();
	}
}

UI_DashboardMsg_User.disableGridAndSearch = function (){
	$('#btnSearchPop').attr('disabled',true);
	UI_DashboardMsg_User.clearGridOnly();
}

UI_DashboardMsg_User.enableGridAndSearch = function (){
	$('#btnSearchPop').attr('disabled',false);
	UI_DashboardMsg_User.clearGridOnly();
}


UI_DashboardMsg_User.initilizePopup = function (){
	UI_DashboardMsg_User.clearGridOnly();
	UI_DashboardMsg_User.initilizeSearchInputs();
}

UI_DashboardMsg_User.initilize = function (){
	UI_DashboardMsg_User.initilizePopup();
}

UI_DashboardMsg_User.addSelectedUsers = function (selectedUSers){
	var count = 1;
	$("#tblUserResults").clearGridData();
	if(selectedUSers != null){
		for ( var key in selectedUSers) {
			if(key != ""){			
				var value = selectedUSers[key].value;
				var data = {id:count,'pagedUser.userId':key,'pagedUser.displayName':value};
				$("#tblUserResults").addRowData(count,data);
				$("#tblUserResults").editRow(count, false);	
				$("#"+(count)+"_checkedUser").attr('checked',true);
				count++;
			}
		}	
	}
}

UI_DashboardMsg_User.checkAllRows = function (){
	var records = $("#tblUserResults").getGridParam('reccount');
	for ( var i = 1; i <= records; i++) {	
		$("#tblUserResults").editRow(i, false);			
		$("#"+i+"_checkedUser").attr('checked',true);	
	}
}

UI_DashboardMsg_User.rowSelect = function (rowid,status){
	if($("#"+rowid+"_checkedUser").attr('checked')){
		UI_DashboardMsg_User.updateSelectedUsres(rowid);
	}else {
		UI_DashboardMsg_User.removeSelectedUsers(rowid);
	}
}

UI_DashboardMsg_User.onLoad = function (){
	var reccount = $("#tblUserResults").getGridParam('reccount');
	var rowNumb = $("#tblUserResults").getGridParam('rowNum');
	var page = $("#tblUserResults").getGridParam('page');
	var startRecord = page*rowNumb;
	var finalRecCount = startRecord + reccount;
	for ( var i = (startRecord - rowNumb); i <= finalRecCount; i++) {
		var userID = $("#tblUserResults").getCell(i,'pagedUser.userId');
		var agentCode = $("#tblUserResults").getCell(i,'pagedUser.agentCode');
		$("#tblUserResults").editRow(i, false);	
		if(UI_DashboardMsg_User.selectedUSers != null && 
				UI_DashboardMsg_User.selectedUSers[userID] != null){			
			$("#"+i+"_checkedUser").attr('checked',true);
		}	
	}
}

UI_DashboardMsg_User.getSelectedUsers = function (){
	return UI_DashboardMsg_User.selectedUSers;
}

UI_DashboardMsg_User.closePopup = function (){
	$('#divUserSelectionWrapper').hide();
}

UI_DashboardMsg_User.removeSelectedUsers = function (rowID){
	var userID = $("#tblUserResults").getCell(rowID,'pagedUser.userId');
	if (UI_DashboardMsg_User.selectedUSers[userID] != null) {
		UI_DashboardMsg_User.selectedUSers[userID] = null;
	}
}

UI_DashboardMsg_User.updateSelectedUsres = function (rowID){
	var userID = $("#tblUserResults").getCell(rowID,'pagedUser.userId');
	if(UI_DashboardMsg_User.selectedUSers == null){
		UI_DashboardMsg_User.selectedUSers = new Object();
	}
	if (UI_DashboardMsg_User.selectedUSers[userID] == null) {
		var agentCode = $("#tblUserResults").getCell(rowID,'pagedUser.agentCode');
		UI_DashboardMsg_User.selectedUSers[userID] = {value:userID, agentCode:agentCode,userID:userID};
		
	}
}

UI_DashboardMsg_User.searchUser = function (){
	var data= {};
	data['userSearchDTO.userID'] = $('#searchLoginID').val();
	data['userSearchDTO.displayName'] = $('#searchDisplayName').val();
	//data['userSearchDTO.stationCode'] = $('#selStation').val();
	//data['userSearchDTO.travelAgentCode'] = $('#selAgent').val();
	//data['userSearchDTO.reportingAgentCodes'] = $('#selAgent').val();
	UI_DashboardMsg_User.selectedUSers == null;
	$("#tblUserResults").setGridParam({page:1,postData:data});
 	$("#tblUserResults").trigger("reloadGrid");
}

UI_DashboardMsg_User.getInlusionStatus = function (){
	if( $('input:radio[name=radUser_inc]:checked').val() == "INCLIUDE"){ //$("input[@name=radUser_inc]:checked").val()
		return "Y";
	}
	return "N";
}

UI_DashboardMsg_User.validateInclusion = function (){
	if(UI_DashboardMsg_User.getSelectedUsers() != null && UI_DashboardMsg_User.getSelectedUsers().length){
		if ( $(".radUser_inc").is(':checked') ) {
			return true;
		}		
		return false;
	}else {
		return true;
	}
}