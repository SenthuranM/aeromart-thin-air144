
UI_blRules.templateList = null;
UI_blRules.agentList = null;
var arrError = new Array();
var isNew = true;

$(document).ready(function() {
	UI_blRules.ready();
});

function UI_blRules() {
}

UI_blRules.ready = function() {
	
	$("#divSearch").decoratePanel("Search PAX Blacklisting rules");
	$("#divResultsPanel").decoratePanel("Blacklist Rules");
	$("#divDispPNRGOV").decoratePanel("Add / Edit Blacklist Rules ");
	
	$("#btnClose").click(function() {
		top.LoadHome();
	});
	$("#txtLabelSearch").alphaWhiteSpace();
	$("#txtLabelDetails").alphaWhiteSpace();
	$("#txtLabelDetails").disable();
	$("#btnSave").hide();
	$("#btnCancel").hide();
	$("#btnReset").hide();
	$("#btnReset").click(function() {
		UI_blRules.formReset();
	});

	$("#btnSave").click(function() {
		UI_blRules.saveClick();
	});
	$("#btnAdd").click(function() {
		showAddEditBlacklistPax('add');
		isNew = true;
	});

	$("#btnEdit").click(function() {
		showAddEditBlacklistPax('edit');
		isNew = false;
	});

	UI_blRules.createGrid();

	$("#buttonSearch").click(function() {
		UI_blRules.createGrid();
	});
	$("#btnSave").hide();
	$("#btnCancel").hide();
	$("#btnReset").hide();

}

UI_blRules.blRulesDetailSuccess = function(response) {
	try {
		eval(response.errorList);
	} catch (err) {
		console.log("eval() error ==> errorList");
	}
	HidePopProgress();
}

UI_blRules.saveClick = function() {
	if (UI_blRules.formValidate()) {
		var data = {};
		if (!isNew) {
			var rowId = $("#blacklistRulesGrid").getGridParam('selrow');
			var selectedRowData = $.data($("#blacklistRulesGrid")[0], "gridData")[rowId - 1];
			data["blacklistPAXRuleTO.ruleId"] = selectedRowData.blacklistPAXRuleTO.ruleId;
		}
		data["blacklistPAXRuleTO.strRules"] = dataElements.getselectedData();
		
		$.ajax({
			url : 'showBlacklistPAXRulesDetail!check.action',
			dataType : "json",
			data : data,
			type : "POST",
			async : false,
			complete : function(response) {
				var responseData = $
						.parseJSON(response.responseText);
				HideProgress();
				if (responseData.blacklistRulesExsistence) {
					showERRMessage(arrError["duplicateRule"]);
				}else{
					UI_blRules.saveValidatedBlRule();
				}
			}
		});
	}
}

UI_blRules.saveValidatedBlRule = function() {
	var data = {};
	if (!isNew) {
		var rowId = $("#blacklistRulesGrid").getGridParam('selrow');
		var selectedRowData = $.data($("#blacklistRulesGrid")[0], "gridData")[rowId - 1];
		data["blacklistPAXRuleTO.version"] = selectedRowData.blacklistPAXRuleTO.version;
		data["blacklistPAXRuleTO.ruleId"] = selectedRowData.blacklistPAXRuleTO.ruleId;
	} else {
		data["blacklistPAXRuleTO.version"] = -1;
	}

	data["blacklistPAXRuleTO.label"] = $("#txtLabelDetails").val();
	data["blacklistPAXRuleTO.status"] = $("#statusDetails").val();
	data["blacklistPAXRuleTO.strRules"] = dataElements.getselectedData();

	$.ajax({
		url : 'showBlacklistPAXRulesDetail!save.action',
		dataType : "json",
		beforeSend : ShowPopProgress(),
		data : data,
		type : "POST",
		async : false,
		success : UI_blRules.blRuleSaveSuccess,
		complete : function(response) {
			var responseData = $.parseJSON(response.responseText);
			HideProgress();
			if (responseData.success) {
				$("#BlacklistRulesGridContainer").empty();
				if (!isNew) {
					SaveMsg("BlackListing rule updated successfully!")
					alert("BlackListing rule updated successfully!");
				} else {
					SaveMsg("BlackListing rule saved successfully!")
					alert("BlackListing rule saved successfully!");
				}
			} else {

				alert("System busy. Please try again later.");

			}
		}
	});
}

UI_blRules.blRuleSaveSuccess = function() {
	 UI_blRules.formReset();
	 HidePopProgress();
	 showConfirmation(arrError["saveSuccess"]);
}

UI_blRules.formReset = function() {
	$("#txtLabelDetails").val("");
	$("#statusDetails").val("ACT");
	dataElements.selectedData("");
	$("#btnSave").hide();
	 $("#btnCancel").hide();
	 $("#btnReset").hide();
}

UI_blRules.formValidate = function() {
	var valid = true;
	if ($("#txtLabelDetails").val() == "") {
		showERRMessage(arrError["ruleLabelRequired"]);
		return false;
	}
	if (dataElements.getselectedData() == "") {
		showERRMessage(arrError["ruleDataEleRequired"]);
		return false;
	}
	return true;
}

UI_blRules.dataElementsValidate = function() {
	
}

UI_blRules.createGrid = function() {

	var blRulestatusToString = function(cellVal, options, rowObject) {
		return JSON.stringify(cellVal);
	}
	UI_blRules.formReset();
	var statusFomatter = function(cellVal, options, rowObject) {
		var treturn = "&nbsp;";
		if (cellVal != null) {
			if (cellVal == "ACT") {
				treturn = "Active";
			} else if (cellVal == "INA") {
				treturn = "Inactive";
			}
		}
		return treturn;
	}

	$("#BlacklistRulesGridContainer")
			.empty()
			.append(
					'<table id="blacklistRulesGrid"></table><div id="blacklistRulesPages"></div>')
	var data = {};
	data['blacklistPAXRuleSearchTO.label'] = $("#txtLabelSearch").val();
	data['blacklistPAXRuleSearchTO.status'] = $("#statusSearch").val();

	var blRulesGrid = $("#blacklistRulesGrid")
			.jqGrid(
					{
						datatype : function(postdata) {
							$
									.ajax({
										url : 'showBlacklistPAXRulesDetail!search.action',
										data : $.extend(postdata, data),
										dataType : "json",
										beforeSend : ShowPopProgress(),
										type : "POST",
										complete : function(jsonData, stat) {
											if (stat == "success") {
												mydata = eval("("
														+ jsonData.responseText
														+ ")");
												if (mydata.msgType != "Error") {
													$
															.data(
																	blRulesGrid[0],
																	"gridData",
																	mydata.blacklistPAXRuleList);
													blRulesGrid[0]
															.addJSONData(mydata);
												} else {
													alert(mydata.message);
												}
												HidePopProgress();
											}
										}
									});
						},
						height : 200,

						colNames : [ 'ruleId','Label', 'Rule', 'Status',
								'FirstNameChecked', 'LaststNameChecked',
								'DateOfBirthChecked', 'PassportNoChecked',
								'NationalityChecked' ],
						colModel : [ {
							name : 'ruleId',
							jsonmap : 'blacklistPAXRuleTO.ruleId',
							hidden : true
						},{
							name : 'label',
							index : 'label',
							jsonmap : 'blacklistPAXRuleTO.label',
							width : 70,
							sorttype : "string"
						}, {
							name : 'strRules',
							index : 'strRules',
							jsonmap : 'blacklistPAXRuleTO.strRules',
							width : 70
						}, {
							name : 'status',
							index : 'status',
							jsonmap : 'blacklistPAXRuleTO.status',
							width : 70,
							formatter : statusFomatter,
							sorttype : "string"
						}, {
							name : 'firstNameChecked',
							jsonmap : 'blacklistPAXRuleTO.firstNameChecked',
							hidden : true
						}, {
							name : 'laststNameChecked',
							jsonmap : 'blacklistPAXRuleTO.laststNameChecked',
							hidden : true
						}, {
							name : 'dateOfBirthChecked',
							jsonmap : 'blacklistPAXRuleTO.dateOfBirthChecked',
							hidden : true
						}, {
							name : 'passportNoChecked',
							jsonmap : 'blacklistPAXRuleTO.passportNoChecked',
							hidden : true
						}, {
							name : 'nationalityChecked',
							jsonmap : 'blacklistPAXRuleTO.nationalityChecked',
							hidden : true
						}, ],
						caption : "Blacklist Rules",
						rowNum : 20,
						viewRecords : true,
						width : 920,
						pager : jQuery('#blacklistRulesPages'),
						jsonReader : {
							root : "blacklistPAXRuleList",
							page : "page",
							total : "total",
							records : "records",
							repeatitems : false,
							id : "0"
						},

					}).navGrid("#blacklistRulesPages", {
				refresh : true,
				edit : false,
				add : false,
				del : false,
				search : false
			});

	$("#blacklistRulesGrid").click(function() {
		UI_blRules.blRulesGridOnClick();
	});
}

UI_blRules.blRulesGridOnClick = function() {

	UI_blRules.formReset();

	var rowId = $("#blacklistRulesGrid").getGridParam('selrow');
	var selectedRowData = $.data($("#blacklistRulesGrid")[0], "gridData")[rowId-1];
	
	UI_blRules.populateBlRulesDetails(selectedRowData);

}

UI_blRules.populateBlRulesDetails = function(rowData) {

	$("#txtLabelDetails").val(rowData.blacklistPAXRuleTO.label);
	var statussel = "&nbsp;";
	if (rowData.blacklistPAXRuleTO.status != null) {
		$("#statusDetails").val(rowData.blacklistPAXRuleTO.status);
	}
	dataElements.selectedData(rowData.blacklistPAXRuleTO.strRules);
}

ShowPopProgress = function() {
	//setVisible("divLoadMsg", true);
	top[2].ShowProgress();
}

HidePopProgress = function() {
	//setVisible("divLoadMsg", false);
	top[2].HideProgress();
}

showAddEditBlacklistPax = function(type) {
	if ($(".ui-state-highlight").length == 0 && type == 'edit') {
		showERRMessage("Select a row to edit");
	} else {
		if (type == 'edit') {
			$("#txtLabelDetails").enable();
			$("#btnSave").show();
			$("#btnCancel").show();
			$("#btnReset").show();
		}
		if (type == 'add') {
			$("#txtLabelDetails").enable();
			$("#txtLabelDetails").val("");
			$("#statusDetails").val("ACT");
			dataElements.selectedData("");
			$("#btnSave").show();
			$("#btnCancel").show();
			$("#btnReset").show();
		}
	}

};

function ShowProgress() {
	top[2].ShowProgress();
}
function HideProgress() {
	top[2].HideProgress();
}
function hideMessage() {
	top[2].HidePageMessage();
}

function SaveMsg(msg) {
	top[2].objMsg.MessageText = msg;
	top[2].objMsg.MessageType = "Confirmation";
	top[2].ShowPageMessage();
}