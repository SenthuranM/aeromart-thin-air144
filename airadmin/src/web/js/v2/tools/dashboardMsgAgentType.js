/**
 * Dashboard Message Management Agent Type JS
 * @author Navod Ediriweera
 */
UI_DashboardMsg_AgentType.multiSelect = null;
/**
 * Onload function
 */
$( function() {
	UI_DashboardMsg_AgentType.initilize();
});

/**
 * UI_DashboardMsg for Dashboard Msg UI related functionalities
 */
function UI_DashboardMsg_AgentType() {
}
UI_DashboardMsg_AgentType.getMultiSelectData = function (name){
	if(UI_DashboardMsg_AgentType.multiSelect == null) return "";
	return UI_DashboardMsg_AgentType.multiSelect.getSelectedItemsAsArray();
}

UI_DashboardMsg_AgentType.setSelectedItems = function (objSelectedItems){
	if(UI_DashboardMsg_AgentType.multiSelect == null) UI_DashboardMsg_AgentType.setMultiSelect(allAgentTypes,objSelectedItems);
	UI_DashboardMsg_AgentType.multiSelect.setSelectedItems(objSelectedItems);
}

UI_DashboardMsg_AgentType.getInlusionStatus = function (){
	if( $('input:radio[name=radAgentType_inc]:checked').val() == "INCLIUDE"){ //$("input[@name=radAgentType_inc]:checked").val()
		return "Y";
	}	
	return "N";
}

UI_DashboardMsg_AgentType.initilize = function (){
	UI_DashboardMsg_AgentType.setMultiSelect(allAgentTypes,null);
}

UI_DashboardMsg_AgentType.enableMultiSelect = function (blnStatus){
	$('#divSearchAgentType :input').attr('disabled',!blnStatus);
	if(blnStatus){
		UI_DashboardMsg_AgentType.multiSelect.enable();
	}else {
		UI_DashboardMsg_AgentType.multiSelect.disable();
	}
}

UI_DashboardMsg_AgentType.setMultiSelect = function (allOptions, selectedOptions){
	$('#spnAgentTypeMulti').empty();
	var objConfig = {allOptions:allOptions,
			 group:false,
			 optionValue:"id",
			 optionText:"text",
			 container:"spnAgentTypeMulti",
			 titleAllItems:"Agent Types Unassinged",
			 titleSelectedItems:"Agent Types Assigned",
			 boxWidth:UI_DashboardMsg.sizeOptions.width,
			 boxHeight:UI_DashboardMsg.sizeOptions.height}; 

	UI_DashboardMsg_AgentType.multiSelect  = new RichSelector(objConfig); 
	UI_DashboardMsg_AgentType.multiSelect.setSelectedItems(selectedOptions);
}

UI_DashboardMsg_AgentType.validateInclusion = function (){
	if(UI_DashboardMsg_AgentType.getMultiSelectData() != ""){
		if ( $(".radAgentType_inc").is(':checked') ) {
			return true;
		}
		return false;
	}else {
		return true;
	}
	
}