/**
 * Dashboard Message Management POS JS
 * @author Navod Ediriweera
 */
UI_DashboardMsg_POS.privilegeMultiSelect = null;
UI_DashboardMsg_POS.addEdit = "ADD";
/**
 * Onload function
 */
$( function() {
	UI_DashboardMsg_POS.initilize();
});

/**
 * UI_DashboardMsg for Dashboard Msg UI related functionalities
 */
function UI_DashboardMsg_POS() {
}

UI_DashboardMsg_POS.getMultiSelectData = function (name){
	if(UI_DashboardMsg_POS.multiSelect == null) return "";
	return UI_DashboardMsg_POS.multiSelect.getSelectedItemsAsArray();
}

UI_DashboardMsg_POS.setSelectedItems = function (objSelectedItems){
	if(UI_DashboardMsg_POS.multiSelect == null){		
		UI_DashboardMsg_POS.initilize();
	}
	UI_DashboardMsg_POS.multiSelect.setSelectedItems(objSelectedItems);
}

UI_DashboardMsg_POS.getInlusionStatus = function (){
	if($('input:radio[name=radPOS_inc]:checked').val() == "INCLIUDE"){ //$('input[@name=radPOS_inc]:checked').val()
		return "Y";
	}
	return "N"; 
}

UI_DashboardMsg_POS.initilize = function (){
	if(UI_DashboardMsg_POS.addEdit == "EDIT"){
		UI_DashboardMsg_POS.setMultiSelect(allStations,null);
	}else if(UI_DashboardMsg_POS.addEdit == "ADD"){
		UI_DashboardMsg_POS.setMultiSelect(allStations_Act,null);
	}	
}

UI_DashboardMsg_POS.enableMultiSelect = function (blnStatus){
	$('#divSearchPOS :input').attr('disabled',!blnStatus);
	if(blnStatus){
		UI_DashboardMsg_POS.multiSelect.enable();
	}else {
		UI_DashboardMsg_POS.multiSelect.disable();
	}
}

UI_DashboardMsg_POS.setMultiSelect = function (allPOS, selectedPOS){
	$('#spmPOSMulti').empty();
	var objConfig = {allOptions:allPOS,
			 group:false,
			 optionValue:"id",
			 optionText:"text",
			 container:"spmPOSMulti",
			 titleAllItems:"POS Unassinged",
			 titleSelectedItems:"POS Assigned",
			 boxWidth:UI_DashboardMsg.sizeOptions.width,
			 boxHeight:UI_DashboardMsg.sizeOptions.height}; 

	UI_DashboardMsg_POS.multiSelect  = new RichSelector(objConfig); 
	UI_DashboardMsg_POS.multiSelect.setSelectedItems(selectedPOS);
}

UI_DashboardMsg_POS.validateInclusion = function (){
	if(UI_DashboardMsg_POS.getMultiSelectData() != ""){
		if ( $(".radPOS_inc").is(':checked') ) {
			return true;
		}		
		return false;
	}else {
		return true;
	}
}