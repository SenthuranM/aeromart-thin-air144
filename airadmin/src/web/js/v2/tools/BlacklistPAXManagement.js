function UI_blacklistPAX() {

	var selectedRowData = null;
	var isNew = true;
	var maxViaPoint = 3;
	this.ready = function() {
		//var tLink = $("<a href='javascript:void(0)'><img src='../../images/tri-down1_no_cache.gif' alt=''>&nbsp;Search</a>");
		//tLink.click(hideAddEditBLPAX);
		$("#divBlPaxSearch").decoratePanel("Search");
		$("#divBlPaxList").decoratePanel("Blacklisted Passenger List");
		$("#BlPaxDetails").decoratePanel("Add/Edit");
		//$("#BlPaxDetails").parent().hide();

		$("#btnAdd").click(function() {
			showAddEditBlacklistPax('add');
			isNew = true;
		});

		$("#btnEdit").click(function() {
			showAddEditBlacklistPax('edit');
			isNew = false;
		});
		$("#btnSearch").click(searchBlacklistPax);
		$("#btnSave").click(saveBlacklistPAX);
		$("#btnDelete").click(checkForDeleteBlacklistPax);
		$("#btnReset").click(resetBtnBlacklistPax);
		$("#blTypeDetails").on(
				'change',
				function() {
					if ($(this).val() == "T") {
						$("#blockPeriodFromDetails, #blockPeriodToDetails")
								.datepicker("enable");
					} else if ($(this).val() == "P") {
						$("#blockPeriodFromDetails, #blockPeriodToDetails")
								.datepicker("disable");
						$('#blockPeriodFromDetails').val("");
						$('#blockPeriodToDetails').val("");
					}
				})

		$(" #blockPeriodFromDetails, #blockPeriodToDetails").datepicker({
			minDate : new Date(),
			showOn : "button",
			dateFormat : 'dd/mm/yy',
			buttonImage : "../../images/calendar_no_cache.gif",
			yearRange : "0:+99",
			buttonImageOnly : true,
			buttonText : "Select date",
			changeMonth : true,
			changeYear : true,
			showButtonPanel : true
		}).keyup(function(e) {
			if (e.keyCode == 8 || e.keyCode == 46) {
				$.datepicker._clearDate(this);
			}
		});
		$(" #txtdobSearch,#txtdobDetails").datepicker({
				showOn : "button",
				dateFormat : 'dd/mm/yy',
				buttonImage : "../../images/calendar_no_cache.gif",
				yearRange : "-99:+99",
				buttonImageOnly : true,
				buttonText : "Select date",
				changeMonth : true,
				changeYear : true,
				showButtonPanel : true
		}).keyup(function(e) {
			if (e.keyCode == 8 || e.keyCode == 46) {
				$.datepicker._clearDate(this);
			}
		});
		
		constructBlPaxGrid();
		/*
		 * var removeDialog = $( "#dialog-remove" ).dialog({ autoOpen: false,
		 * height: 300, width: 350, modal: true, buttons: { "Remove":
		 * deleteBlacklistPAX, Cancel: function() { dialog.dialog( "close" ); } },
		 * close: function() { form[ 0 ].reset(); allFields.removeClass(
		 * "ui-state-error" ); } });
		 */
	};

	searchBlacklistPax = function() {
		constructBlPaxGrid();
	};

	objectFormat = function(obj, bind) {
		rObj = [];
		$.each(obj, function(key, val) {
			var t = val
			if (bind) {
				t = val + "|" + key;
			}
			rObj[rObj.length] = t;
		})
		return rObj
	}

	objectToArray = function(obj) {
		rObj = [];
		$.each(obj, function(key, val) {
			t = [];
			t[0] = key;
			t[1] = val;
			rObj[rObj.length] = t;
		})
		return rObj
	}

	constructBlPaxGrid = function() {
		var newUrl = "blacklistedPAXCriteria!search.action";
		$("#jqGridblacklistedPAXContainer").empty();
		var gridTable = $("<table></table>").attr("id",
				"jqGridblacklistedPAXData");
		var gridPager = $("<div></div>")
				.attr("id", "jqGridblacklistedPAXPages");
		$("#jqGridblacklistedPAXContainer").prepend(gridTable, gridPager);

		var reqData = [];
		reqData['searchDateOfBirth'] = ($('#txtdobSearch').val() == "") ? null: $('#txtdobSearch').val();
		reqData['blacklistPAXCriteriaSearchTO.paxFullName'] = ($('#txtFullnameSearch')
				.val() == "") ? null : $('#txtFullnameSearch').val();		

		/*reqData['blacklistPAXCriteriaSearchTO.fName'] = ($('#txtFnameSearch')
				.val() == "") ? null : $('#txtFnameSearch').val();
		reqData['blacklistPAXCriteriaSearchTO.lName'] = ($('#txtLnameSearch')
				.val() == "") ? null : $('#txtLnameSearch').val();*/
		reqData['blacklistPAXCriteriaSearchTO.passportNo'] = ($(
				'#txtPassportNo').val() == "") ? null : $('#txtPassportNo')
				.val();
		reqData['blacklistPAXCriteriaSearchTO.blacklistType'] = ($(
				'#blTypeSearch').val() == "-") ? null : $('#blTypeSearch')
				.val();
reqData['blacklistPAXCriteriaSearchTO.paxStatus'] = ($(
				'#blStatusSearch').val() == "-") ? null : $('#blStatusSearch')
				.val();

		reqData['blacklistPAXCriteriaSearchTO.nationality'] = ($(
				'#nationalitySearch').val() == "-") ? null : $(
				'#nationalitySearch').val();
		var dateFomatter = function(cellVal, options, rowObject) {
			var treturn = "&nbsp;";
			if (cellVal != null) {
				treturn = cellVal.split("T")[0];
			}
			return treturn;
		}
		/*
		 * var channelFomatter = function(cellVal, options, rowObject){ var
		 * treturn = "&nbsp;"; if (cellVal!=null){ for(var i=0;i<dataMap.Channels.length;i++){
		 * if ($.inArray(dataMap.Channels[i][0], cellVal)>-1){ treturn +=
		 * dataMap.Channels[i][1] +"<br>"; } } } return treturn; }
		 */

		var temGrid = $("#jqGridblacklistedPAXData").jqGrid(
				{
					datatype : function(postdata) {
						$.ajax({
							url : newUrl,
							beforeSend : ShowProgress(),
							data : $.extend(postdata, reqData),
							dataType : "json",
							complete : function(jsonData, stat) {
								if (stat == "success") {
									mydata = eval("(" + jsonData.responseText
											+ ")");
									if (mydata.msgType != "Error") {
										$.data(temGrid[0], "gridData",
												mydata.rows);
										temGrid[0].addJSONData(mydata);

									} else {
										alert(mydata.message);
									}
									HideProgress();
								}
							}
						});
					},
					width : '100%',
					colNames : [ '&nbsp;', /*'First Name', 'Last Name',*/'Passenger Name',
							'Passport No','Status' ,'Blacklist Type', 'Nationality',
							'Date of Birth', 'Removal Effective From', 'Reason for Removal' ],
					colModel : [ {
						name : 'id',
						index : 'id',
						width : 25
					},/* {
						name : 'fName',
						index : 'fName',
						width : 100,
						jsonmap : "criteria.fName"
					}, {
						name : 'lName',
						index : 'lName',
						width : 100,
						jsonmap : "criteria.lName"
					},*/ {
						name : 'fullName',
						index : 'fullName',
						width : 150,
						jsonmap : "criteria.paxFullName"
					}, {
						name : 'passportNo',
						index : 'passportNo',
						width : 90,
						jsonmap : "criteria.passportNo"
					}, {
						name : 'blacklistType',
						index : 'blacklistType',
						width : 100,
						jsonmap : "criteria.blacklistType"
					}, {
						name : 'paxStatus',
						index : 'paxStatus',
						width : 100,
						jsonmap : "criteria.paxStatus"
					}, {
						name : 'nationality',
						index : 'nationality',
						width : 70,
						jsonmap : "criteria.nationality"
					}, {
						name : 'dateOfBirth',
						index : 'dateOfBirth',
						width : 110,
						align : "center",
						jsonmap : "criteria.dateOfBirth",
						formatter : dateFomatter
					}, {
						name : 'validUntil',
						index : 'validUntil',
						width : 130,
						align : "center",
						jsonmap : "criteria.validUntil",
						formatter : dateFomatter
					}, {
						name : 'removedReason',
						index : 'removedReason',
						width : 170,
						align : "center",
						jsonmap : "criteria.removedReason",
						formatter : dateFomatter
					}],
					imgpath : "",
					pager : jQuery('#jqGridblacklistedPAXPages'),
					multiselect : false,
					viewrecords : true,
					rowNum : 20,
					sortable : true,
					altRows : true,
					altclass : "GridAlternateRow",
					jsonReader : {
						root : "rows",
						page : "page",
						total : "total",
						records : "records",
						repeatitems : false,
						id : "0"
					},
					onSelectRow : function(rowid) {
						setSelectedRow(rowid - 1);
						setField("hdnSelectID", rowid - 1);
					}

				}).navGrid("#jqGridblacklistedPAXPages", {
			refresh : true,
			edit : false,
			add : false,
			del : false,
			search : false
		});
	};

	hideAddEditBLPAX = function() {
		$(this).find("img").attr("src", "../../images/tri-down1_no_cache.gif");
		$("#BlPaxDetails").parent().hide();
		$("#divBlPaxSearch").show();
	};

	showAddEditBlacklistPax = function(type) {
		if ($(".ui-state-highlight").length == 0 && type == 'edit') {
			showERRMessage("Select a row to edit");
		} else {
			//$("#BlPaxDetails").parent().show();
			//$("#divBlPaxSearch").parent().find("a>img").attr("src","../../images/tri-right1_no_cache.gif");
			//$("#divBlPaxSearch").hide();
			if (type == 'edit') {
				setValuesToForm();
			}
		}

	};

	setSelectedRow = function(id) {
		var selectID = id % 20;
		selectedRowData = $.data($("#jqGridblacklistedPAXData")[0], "gridData")[selectID];

		var submitData = {};
		submitData["blacklistPAXCriteriaSearchTO.blacklistPAXCriteriaID"] = selectedRowData.criteria.blacklistPAXCriteriaID;

		$.ajax({
			url : 'blacklistedPAXCriteria!searchBlacklistPAX.action',
			beforeSend : ShowProgress(),
			data : submitData,
			type : "POST",
			async : false,
			dataType : "json",
			complete : function(response) {
				HideProgress();
				var responseData = $.parseJSON(response.responseText);
				if (responseData.success) {
					if (responseData.blacklistPAXAvailable == 'Y') {
						$("#btnEdit").enable();
						$("#btnDelete").enable();
					} else {
						$("#btnEdit").disable();
						$("#btnDelete").disable();
					}
				} else {
					alert("Please try again later");
				}
			}
		});

	};

	setValuesToForm = function() {
		var fillSelectBoxes = function(id, data, extraData) {
			extratExtraData = function(dataE, falg) {
				treturn = "";
				if (dataE != null) {
					for (var i = 0; i < dataE.length; i++) {
						if (dataE[i].split("|")[1] == falg) {
							treturn = dataE[i].split("|")[0]
							break;
						}
					}
				} else {
					treturn = falg;
				}
				return treturn;
			}
			$("#" + id).empty();
			if (data != '') {
				$.each(data, function() {
					var t = $(
							"<option>" + extratExtraData(extraData, this)
									+ "</option>").attr("value", this);
					$("#" + id).append(t);
				});
			} else {
				var t = $("<option>All</option>").attr("value", '');
				$("#" + id).append(t);
			}
		}
	/*	$("#txtFnameDetails").val(selectedRowData.criteria.fName);
		$("#txtLnameDetails").val(selectedRowData.criteria.lName);*/
		$("#txtFullnameDetails").val(selectedRowData.criteria.paxFullName);
		$("#txtPassportNoDetails").val(selectedRowData.criteria.passportNo);
		$("#txtRemarksDetails").val(selectedRowData.criteria.remarks);
		$("#nationalityDetails option").filter(function() {
			return $(this).text() == selectedRowData.criteria.nationality;
		}).prop('selected', true);

		$("#blTypeDetails").val(selectedRowData.criteria.blacklistType);
		$("#blTypeDetails").val(selectedRowData.criteria.paxStatus);
		if (selectedRowData.criteria.dateOfBirth != null) {
			$("#txtdobDetails").val(
					formatDate(selectedRowData.criteria.dateOfBirth,
							"yyyy-mm-dd", "mm/dd/yyyy"));
		}
		if (selectedRowData.criteria.effectiveFrom != null) {
			$("#blockPeriodFromDetails").val(
					formatDate(selectedRowData.criteria.effectiveFrom,
							"yyyy-mm-dd", "mm/dd/yyyy"));
		}
		if (selectedRowData.criteria.effectiveTo != null) {
			$("#blockPeriodToDetails").val(
					formatDate(selectedRowData.criteria.effectiveTo,
							"yyyy-mm-dd", "mm/dd/yyyy"));
		}
	}

	var saveBlacklistPAX = function() {
		if (validateForm()) {
			var submitData = getDataFromPage();
			var targetURL;
			if (isNew) {
				targetURL = 'blacklistedPAXCriteria!save.action';
			} else {
				targetURL = 'blacklistedPAXCriteria!update.action';
			}

			$.ajax({
				url : targetURL,
				beforeSend : ShowProgress(),
				data : submitData,
				type : "POST",
				async : false,
				dataType : "json",
				complete : function(response) {
					var responseData = $.parseJSON(response.responseText);
					HideProgress();
					if (responseData.success) {
						resetBlacklistedPAXGrid();
						SaveMsg("BlackListed PAX saved successfully!")
						alert("BlackListed PAX saved successfully!");
					} else {

						alert("System busy. Please try again later.");

					}
				}
			});
		}

	}

	deleteBlacklistPAX = function() {

		if (validatePopup()) {
			var submitData = getDataFromPage();
			var targetURL;
			targetURL = 'blacklistedPAXCriteria!update.action';

			$
					.ajax({
						url : targetURL,
						beforeSend : ShowProgress(),
						data : submitData,
						type : "POST",
						async : false,
						dataType : "json",
						complete : function(response) {
							var responseData = $
									.parseJSON(response.responseText);
							HideProgress();
							if (responseData.success) {
								$("#dialog-remove").dialog("close");
								resetBlacklistedPAXGrid();
								SaveMsg("BlackListed PAX removal updated successfully!")
								alert("BlackListed PAX removal updated successfully!");
							} else {

								alert("System busy. Please try again later.");

							}
						}
					});
		}
	}

	var getDataFromPage = function() {

		var data = {};

		if (!isNew) {
			data["blacklistPAXCriteriaTO.version"] = selectedRowData.criteria.version;
			data["blacklistPAXCriteriaTO.blacklistPAXCriteriaID"] = selectedRowData.criteria.blacklistPAXCriteriaID;
		} else {
			data["blacklistPAXCriteriaTO.version"] = -1;
		}
		data["blacklistPAXCriteriaTO.paxFullName"] = $("#txtFullnameDetails").val()
		data["blacklistPAXCriteriaTO.passportNo"] = $("#txtPassportNoDetails").val();
		data["blacklistPAXCriteriaTO.nationality"] = $("#nationalityDetails").val();
		data["dateOfBirth"] = $("#txtdobDetails").val();
		data["blacklistPAXCriteriaTO.remarks"] = $("#txtRemarksDetails").val();
		data["blacklistPAXCriteriaTO.blacklistType"] = $("#blTypeDetails").val();
		data["blacklistPAXCriteriaTO.paxStatus"] = $("#blStatusDetails").val();
		data["effectiveFrom"] = $("#blockPeriodFromDetails").val();
		data["effectiveTo"] = $("#blockPeriodToDetails").val();
		data["validUntil"] = $("#validuntil").val();
		data["blacklistPAXCriteriaTO.removedReason"] = $("#removalReason").val();

		return data;
	}

	getSelectBoxData = function(selectBoxID) {
		var selector = "#" + selectBoxID + " option"
		var selectedItems = [];
		$(selector).each(function() {
			if (this.value != "") {
				selectedItems.push(this.value);
			}
		});
		return JSON.stringify(selectedItems);
	}

	validateForm = function() {
		var valied = true;
		/*if ($("#txtFnameDetails").val() == "") {
			showERRMessage(firstNameRequired);
			return false;
		}
		if ($("#txtLnameDetails").val() == "") {
			showERRMessage(lastNameRequired);
			return false;
		}*/
		if($("#txtFullnameDetails").val()==""){
			showERRMessage(firstNameRequired);
			return false;
		}
		if ($("#txtPassportNoDetails").val() == "") {
			showERRMessage(passPortNoRequired);
			return false;
		}
		if ($("#nationalityDetails").val() == "") {
			showERRMessage(nationalityRequired);
			return false;
		}
		if ($("#blTypeDetails").val() == "") {
			showERRMessage(blacklistTypeRequired);
			return false;
		}
		return true;
	}
	
	validatePopup = function() {
		var valied = true;
		if ($("#validuntil").val() == "") {
			showERRMessage(validUntilRequired);
			return false;
		}
		if ($("#removalReason").val() == "") {
			showERRMessage(remReasonRequired);
			return false;
		}
		
		return true;
	}
	
	checkForDeleteBlacklistPax= function(){
		var data = {};		
				
		if (selectedRowData.criteria.blacklistPAXCriteriaID != null){
			data["blacklistPaxId"] = selectedRowData.criteria.blacklistPAXCriteriaID;
			var targetURL= 'blacklistedPAXCriteria!isblPaxHasReservations.action';

			$
					.ajax({
						url : targetURL,
						beforeSend : ShowProgress(),
						data : data,
						type : "POST",
						async : false,
						dataType : "json",
						complete : function(response) {
							var responseData = $.parseJSON(response.responseText);
							HideProgress();
							if (responseData.success) {
								havingReservations = responseData.blPaxHasReservations =="Y" ? true : false;  	
								popUpDelete(havingReservations);
							} else {
								alert("System busy. Please try again later.");
							}
						}
					});
		}else{
			showERRMessage("Select a row to edit");
		}
		
	}
	
	popUpDelete = function(havingReservations) {
		$("#dialog-remove").dialog({
			resizable : false,
			height : 300,
			width : 405,
			modal : true,
			buttons : {
				Cancel : function() {
					$(this).dialog("close");
				}
			}
		});
		
		if(havingReservations){
			$("#divConfirmDelete").hide();
			$("#addReasonForRemove").show();
			$("#btnDeletePopup").on("click", deleteBlacklistPAX);
			$("#validuntil").datepicker({
				minDate : new Date(),
				showOn : "button",
				dateFormat : 'dd/mm/yy',
				buttonImage : "../../images/calendar_no_cache.gif",
				yearRange : "0:+99",
				buttonImageOnly : true,
				buttonText : "Select date",
				changeMonth : true,
				changeYear : true,
				showButtonPanel : true
			}).keyup(function(e) {
				if (e.keyCode == 8 || e.keyCode == 46) {
					$.datepicker._clearDate(this);
				}
			});
		}else{
			$("#addReasonForRemove").hide();
			$("#divConfirmDelete").show();
			$("#permentlyDelete").on("click", confirmDeleteBlPax);
			$("#permentlyDeleteCancel").on("click", function() {
				$(this).dialog("close");
			});
		}
	}
	
	confirmDeleteBlPax = function(){
			
			var targetURL;
			targetURL = 'blacklistedPAXCriteria!deleteBlPax.action';

			$
					.ajax({
						url : targetURL,
						beforeSend : ShowProgress(),
						data : consructTOfromDataRow(),
						type : "POST",
						async : false,
						dataType : "json",
						complete : function(response) {
							var responseData = $
									.parseJSON(response.responseText);
							HideProgress();
							if (responseData.success) {
								$("#dialog-remove").dialog("close");
								resetBlacklistedPAXGrid();
								SaveMsg("BlackListed PAX successfully removed!")
								alert("BlackListed PAX successfully removed!");
							} else {
								alert("System busy. Please try again later.");
							}
						}
					});
		
	}
	
	consructTOfromDataRow = function(){
		var data = {};					
			data["blacklistPAXCriteriaTO.blacklistPAXCriteriaID"] =	selectedRowData.criteria.blacklistPAXCriteriaID;
			data["blacklistPAXCriteriaTO.blacklistType"] = selectedRowData.criteria.blacklistType;		
			data["blacklistPAXCriteriaTO.passportNo"] = selectedRowData.criteria.passportNo;
			data["blacklistPAXCriteriaTO.paxFullName"] = selectedRowData.criteria.paxFullName;
			data["blacklistPAXCriteriaTO.paxStatus"] = selectedRowData.criteria.paxStatus;		
			data["blacklistPAXCriteriaTO.version"] = selectedRowData.criteria.version;
			
			return data;
	}

	function ShowProgress() {
		top[2].ShowProgress();
	}
	function HideProgress() {
		top[2].HideProgress();
	}
	function hideMessage() {
		top[2].HidePageMessage();
	}
	
	function SaveMsg(msg) {
		top[2].objMsg.MessageText = msg;
		top[2].objMsg.MessageType = "Confirmation";
		top[2].ShowPageMessage();
	}

	function formatDate(dt, fmt1, fmt2) {
		var dd1, mm1, yy1;
		var dd2, mm2, yy2;
		var dt2, sep;

		if (fmt1 == "dd-mmm-yyyy") {
			dd1 = dt.substr(0, 2);
			mm1 = dt.substr(3, 3);
			yy1 = dt.substr(7, 4);
		} else if (fmt1 == "dd-mmm-yy") {
			dd1 = dt.substr(0, 2);
			mm1 = dt.substr(3, 3);
			yy1 = dt.substr(7, 2);
		} else if ((fmt1 == "dd/mm/yyyy") || (fmt1 == "dd-mm-yyyy")) {
			dd1 = dt.substr(0, 2);
			mm1 = dt.substr(3, 2);
			yy1 = dt.substr(6, 4);
		} else if ((fmt1 == "dd/mm/yy") || (fmt1 == "dd-mm-yy")) {
			dd1 = dt.substr(0, 2);
			mm1 = dt.substr(3, 2);
			yy1 = dt.substr(6, 2);
		} else if ((fmt1 == "yyyy/mm/dd") || (fmt1 == "yyyy-mm-dd")) {
			dd1 = dt.substr(8, 2);
			mm1 = dt.substr(5, 2);
			yy1 = dt.substr(0, 4);
		} else if ((fmt1 == "yy/mm/dd") || (fmt1 == "yy-mm-dd")) {
			dd1 = dt.substr(6, 2);
			mm1 = dt.substr(3, 2);
			yy1 = dt.substr(0, 2);
		} else {
			// alert("formatDate:Invalid format1:" + fmt1);
		}
		// ***********************

		// get seperator
		if (fmt2.indexOf("/") != -1) {
			sep = "/";
		} else {
			sep = "-";
		}

		// get year
		if (fmt2.indexOf("yyyy") != -1) {
			if (yy1.length == 2) {
				if (Number(yy1) < 30) {
					yy2 = 2000;
				} else {
					yy2 = 1900;
				}
				yy2 += Number(yy1);
			} else {
				yy2 = yy1;
			}
		} else {
			if (yy1.length == 4) {
				yy2 = yy1.substr(2, 2);
			} else {
				yy2 = yy1;
			}
		}

		// get month
		if (fmt2.indexOf("mmm") != -1) {
			if (mm1.length == 2) {
				mm2 = getStrMonth(mm1);
			} else {
				mm2 = mm1;
			}
		} else {
			if (mm1.length == 3) {
				mm2 = getNoMonth(mm1);
				return (yy1 + "/" + mm2 + "/" + dd1);
			} else {
				mm2 = mm1;
			}
		}

		// get day
		dd2 = dd1;

		// date

		if (fmt2.indexOf("yy") < 4) {
			dt2 = yy2 + sep + mm2 + sep + dd2;
		} else {
			dt2 = dd2 + sep + mm2 + sep + yy2;
		}

		return dt2;
	}

	resetBlacklistedPAXGrid = function() {
		//$("#jqGridblacklistedPAXContainer").empty();
		constructBlPaxGrid();
		$("#txtFullnameSearch,#txtPassportNo,#txtdobSearch").val('');
		$("#blStatusSearch").val("ACT");
		$("$nationalitySearch").val("-");
	}
	resetBtnBlacklistPax = function() {
		/*$("#txtFnameDetails").val("");
		$("#txtLnameDetails").val("");*/
		$("#txtFullnameDetails").val("");
		$("#txtPassportNoDetails").val("");
		$("#txtdobDetails").val("");
		$("#blockPeriodFromDetails").val("");
		$("#blockPeriodToDetails").val("");
		$("#txtRemarksDetails").val("");
	}
}
UI_blacklistPAX = new UI_blacklistPAX();

UI_blacklistPAX.ready();