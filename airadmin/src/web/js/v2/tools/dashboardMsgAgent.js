/**
 * Dashboard Message Management POS JS
 * @author Navod Ediriweera
 */

UI_DashboardMsg_Agent.multiSelect = null;
UI_DashboardMsg_Agent.loaded = false;
UI_DashboardMsg_Agent.addEdit = "ADD";
/**
 * Onload function
 */
$( function() {
	$('#divSearchAgent :button').removeClass('Button').addClass('clsButtonTiny').decorateButton();
});

/**
 * UI_DashboardMsg for Dashboard Msg UI related functionalities
 */
function UI_DashboardMsg_Agent() {
}

UI_DashboardMsg_Agent.initilize = function (){	
	ls.selectedData("");	
	if(UI_DashboardMsg_Agent.addEdit == "ADD"){
		ls.group1 = stns_act;
		ls.list1 = agentsArr_act;
		ls.drawListBox();
	}else if (UI_DashboardMsg_Agent.addEdit == "EDIT"){
		ls.group1 = stns;
		ls.list1 = agentsArr;
		ls.drawListBox();
	}		
}

UI_DashboardMsg_Agent.getMultiSelectData = function(name) {
	var strAgents = ls.getselectedData();		
	var newAgents;
	if (strAgents.indexOf(":") != -1) {
		strAgents = replaceall(strAgents, ":", ",");
	}
	if (strAgents.indexOf("|") != -1) {
		newAgents = replaceall(strAgents, "|", ",");
	} else {
		newAgents = strAgents
	}
	return newAgents;
}

UI_DashboardMsg_Agent.setSelectedItems = function (objSelectedItems){	
	ls.selectedData(objSelectedItems);		
}

UI_DashboardMsg_Agent.enableMultiSelect = function (blnStatus){	
	$('#divSearchAgent :input').attr('disabled',!blnStatus);
	ls.disable(!blnStatus);		
}

UI_DashboardMsg_Agent.getInlusionStatus = function (){
	if($('input:radio[name=radAgent_inc]:checked').val() == "INCLIUDE"){//$("input[@name=radAgent_inc]:checked").val()
		return "Y";
	}	
	return "N";
}

UI_DashboardMsg_Agent.validateInclusion = function (){
	if(UI_DashboardMsg_Agent.getMultiSelectData() != ""){
		if ( $(".radAgent_inc").is(':checked') ) {
			return true;
		}
		return false;
	}else {
		return true;
	}

	
}