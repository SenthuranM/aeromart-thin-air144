jQuery(document).ready(function(){
	$("#flightSearchPanel").decoratePanel("Flight Search");
	$("#flightSummary").decoratePanel("Flight Segment Summary");
	$("#flightBCSummary").decoratePanel("Flight BC Allocation Summary");
	$("#btnClose").decorateButton();

	$("#txtDepartureDate").datepicker({
		showOn: "button",
		buttonImage: "../../images/calendar_no_cache.gif",
		buttonImageOnly: true,
		dateFormat: 'dd/mm/yy'
	});
	
	jQuery('#tblFlightBCSummary').jqGrid({
		url : "showFlightSummary.action",
		datatype : 'json',
		jsonReader : {
			root: "bcRows", 
			page: "bcPage",
			total: "bcTotal",
			records: "bcRecords",
			repeatitems: false,	
			id: "0"	
		},
		colNames:['&nbsp','Cabin Class','Booking<br>Class', 'Std' , "Rank", 'Fixed' ,'Priority', 'Alloc','Sold','Ohd', 'Cnxd', 'Aquired', "Avail", "OverB",
		           "Closed"],
		colModel:[
		{name:'Id', jsonmap:"id",width:20,align:"center"},
		{name:'cos',  jsonmap:'cos', width:60},
		{name:'bookingCode',  jsonmap:'bookingCode', width:60},
		{name:'standardCode', jsonmap:'standardCode', width:40, align:"center"},
		{name:'nestRank', jsonmap:'nestRank', width:40, align:"center"},
		{name:'fixedFlag', jsonmap:'fixedFlag', width:40, align:"center"},
		{name:'priorityFlag', jsonmap:'priorityFlag', width:40, align:"center"},
		{name:'seatsAllocated', jsonmap:'seatsAllocated', width:40, align:"center", editable : true},
		{name:'actualSeatsSold', jsonmap:'actualSeatsSold', width:40, align:"center"},
		{name:'actualSeatsOnHold', jsonmap:'actualSeatsOnHold', width:40, align:"center"},
		{name:'seatsCancelled', jsonmap:'seatsCancelled', width:40, align:"center"},
		{name:'seatsAcquired', jsonmap:'seatsAcquired', width:50, align:"center"},
		{name:'seatsAvailable', jsonmap:'seatsAvailable', width:40, align:"center"},
		{name:'overBookCount', width:40, align:"center",jsonmap : "overBookCount"},
		{name:'statusClosed', width:30, align:"center" ,jsonmap : "statusClosed"}
	],
		rowNum : 20,
		viewRecords : true,
		height : 350,
		width : 925
	});
	
	jQuery('#tblFlightSummary').jqGrid({
		datatype : 'json',
		jsonReader : {
			  root: "rows", 
			  page: "page",
			  total: "total",
			  records: "records",
			  repeatitems: false,	
			  id: "0"				  
			},	
			colNames:[ '&nbsp' , 'Segment','COS', 'Allocated<br> Adult' ,'Oversell<br> Adult', 'Curtailed<br> Adult','Allocated <br>Infant',
			           'Sold <br>Adult/Infant', 'On Hold <br>Adult/Infant', 'Available <br>Adult/Infant', "Overbook<br> Adult", "Flight Segment<br> Status"],
			colModel:[
			    {name:'Id', jsonmap:"id",width:20,align:"center"},
				{name:'segmentCode', jsonmap:'segmentCode', width:75, align:"center"},
				{name:'cos', jsonmap:'cos', width:50, align:"center"},
				{name:'allocatedSeats', jsonmap:'allocatedSeats', width:50, align:"center"},
				{name:'oversellSeats', jsonmap:'oversellSeats', width:50,align:"center"},
				{name:'curtailedSeats', jsonmap:'curtailedSeats', width:50,align:"center"},
				{name:'infantAllocation', jsonmap:'infantAllocation', width:50, align:"center"},
				{name:'seatsSold', jsonmap:'seatsSold', width:50, align:"center"},
				{name:'onHoldSeats', jsonmap:'onHoldSeats', width:50, align:"center"},
				{name:'availableSeats',jsonmap:'availableSeats', width:50, align:"center"},
				{name:'overbookCount', jsonmap:'overbookCount', width:50, align:"center"},
				{name:'flightSegmentStatus', jsonmap:'flightSegmentStatus', width:100, align:"center"}
			],
		    rowNum:5,						
		    viewrecords: true,
		    height:75,
		    width:925,
		    refresh : true
	});
	
	$('#btnSearch').click(function(){
		if(searchClick()){
			$('#frmFlightSummary').ajaxSubmit({datatype : 'json',success : displayData});
		}
	});
	
	function searchClick(){
		var deparDate = getField("txtDepartureDate").value;
		var flightNo = getField("txtFlightNoSearch").value;
		
		if (deparDate == "")	{
			showERRMessage(mandatorysearch);		
			getFieldByID("txtDepartureDate").focus();		
			return false;
			
		} else if(!dateChk(deparDate)){
			showERRMessage(invaldate);		
			getFieldByID("txtDepartureDate").focus();		
			return false;
		}
		
		if (flightNo == "") {
			showERRMessage(mandatorysearch);
			getFieldByID("txtFlightNoSearch").focus();
			return false;
		}
		return true;
	}
	
	function displayData(response){
		if(response.rows == null){
			$("#tblFlightSummary").clearGridData();	
			$("#tblFlightBCSummary").clearGridData();
			showCommonError("Error","No flights available");
		}else{		
			$('#tblFlightBCSummary')[0].addJSONData(response);
			$('#tblFlightSummary')[0].addJSONData(response);	
		}
		HideProgress();
	}	
	HideProgress();
});