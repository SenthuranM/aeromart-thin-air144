/**
 * Dashboard Message Management Main JS
 * @author Navod Ediriweera
 */

UI_DashboardMsg.screenID = "SC_SHDS_006";
UI_DashboardMsg.allUsersForCurrent = null;
UI_DashboardMsg.userSelectionPopup = $('#divUserSelectionWrapper');
UI_DashboardMsg.userListSplitStr = ",";
UI_DashboardMsg.options = {
	ALL:"ALL",USER:"USER",
	//AGENT:"AGENT",
	agentSeperator:" ",userSeperator : ",",agentUserSeperator:":",
	agentAllFlag:"*"
}
UI_DashboardMsg.sizeOptions ={
	height:"130",width:"180"
}

/**
 * Onload function
 */
$( function() {

	$("#divMessagesSearch").decoratePanelWithAlign("Search Messages");
	$("#divResultsPanel").decoratePanelWithAlign("Search Results");
	$("#divDisplayDashboardMsg").decoratePanelWithAlign("Add/Edit Message");
	
	$('#btnAdd').decorateButton();
	$('#btnEdit').decorateButton();
	$('#btnSearch').decorateButton();
	$('#btnSave').decorateButton();
	$('#btnClose').decorateButton();
	$('#btnReset').decorateButton();
	
	$("#tblDashboardMsg").jqGrid({ 
		url:'dashBoardMessageManagement!searchMessages.action',
		datatype: "json",	
		jsonReader : {
			  root: "rows", 
			  page: "page",
			  total: "total",
			  records: "records",
			  repeatitems: false,	
			  userdata : "userIDtoDisplayMap",
			  id: "0"				  
			},													
		colNames:['&nbsp;','ID','Message Type', 'Content', 'From Date', 'To Date','Users', 'Priority',
		          'Selected Users', 'Not Selected Users','userAgentCode','version',
		          'Users','Agents','Agent Types', 'POS','Status','AgentInc', 'AgentTypeInc', 'UserInc','POSInc',
		          'userMap','EnableFrom','EnableTo'], 
		colModel:[ 	{name:'id', width:5, jsonmap:'id' },  
		           	{name:'dashboardMsgTO.messageID',		index: 'messageID', 		width:15, align:"left", hidden:true },	
		           	{name:'dashboardMsgTO.messageType', 	index: 'messageType', 		width:30, align:"left" },	
		           	{name:'dashboardMsgTO.messageContent',	index: 'messageContent', 	width:150, align:"left"},	
		           	{name:'dashboardMsgTO.fromDate', 		index: 'fromDate',			width:30, align:"left"},	
		           	{name:'dashboardMsgTO.toDate', 			index: 'toDate',			width:30, align:"left"},
		           	{name:'dashboardMsgTO.userList',		index: 'userList',			width:30, align:"left" , hidden:true },
		         	{name:'dashboardMsgTO.priority',		index: 'priority',			width:15, align:"center"},
		           	{name:'dashboardMsgTO.selectedUserIDs',	index: 'selectedUserIDs',	width:30, align:"left", hidden:true },
		           	{name:'dashboardMsgTO.notSelectedUserIDs',	index: 'selectedUserIDs',	width:30, align:"left", hidden:true },
		           	{name:'dashboardMsgTO.userAgentCode',		index: 'userAgentCode',	width:30, align:"left", hidden:true },
		           	{name:'dashboardMsgTO.version',				index: 'version',	width:30, align:"left", hidden:true },
		           	{name:'users',		index: 'users',			width:30, align:"left", hidden:true },
		           	{name:'agents',		index: 'agents',		width:30, align:"left", hidden:true },
		           	{name:'agentTypes',	index: 'agentTypes',	width:30, align:"left", hidden:true },
		           	{name:'pos',		index: 'pos',			width:30, align:"left", hidden:true },
		           	{name:'status',		index: 'status',	width:30, align:"left", hidden:true },
		           	{name:'agentInclusion',		index: 'agentInclusion',		width:30, align:"left", hidden:true },
		           	{name:'agentTypeInclusion',	index: 'agentTypeInclusion',	width:30, align:"left", hidden:true },
		           	{name:'userInclusion',		index: 'userInclusion',			width:30, align:"left", hidden:true },
		           	{name:'posInclusion',		index: 'posInclusion',			width:30, align:"left", hidden:true },
		           	{name:'userMap',		index: 'userMap',			width:30, align:"left", hidden:true },
		           	{name:'isFromDateEditable',	index: 'enableFromDate',	width:30, align:"left", hidden:true },
		           	{name:'isToDateEditable',	index: 'enableToDate',	width:30, align:"left", hidden:true }


					], 
		imgpath: '../../themes/default/images/jquery',
		multiselect:false,
		pager: $('#divDashboardMsgPager'),
		rowNum:10,						
		viewrecords: true,
		height:70,
		width:920,	
		onSelectRow: function(rowid){
				UI_DashboardMsg.gridRowOnClick(rowid);
		},
	   	loadComplete: function (e){	   
			if(UI_Common.displayMsgObj.message){
				UI_Common.displayMessage(UI_Common.displayMsgObj);
				UI_Common.displayMsgObj.clearData();
			}
			UI_DashboardMsg.clearEditArea();
	   	}
	}).navGrid("#divDashboardMsgPager",{refresh: true, edit: false, add: false, del: false, search: false});

	$("#fromDate").datepicker({showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', dateFormat: 'dd/mm/yy', buttonImageOnly: true});
	$("#toDate").datepicker({showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', dateFormat: 'dd/mm/yy', buttonImageOnly: true});
	$("#searchFrom").datepicker({showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', dateFormat: 'dd/mm/yy', buttonImageOnly: true});
	$("#searchTo").datepicker({showOn: 'button', buttonImage: '../../images/calendar_no_cache.gif', dateFormat: 'dd/mm/yy', buttonImageOnly: true});

	$('#btnSearch').click(function() { UI_DashboardMsg.searchData(); });
	$('#btnEdit').click(function() { UI_DashboardMsg.editOnClick(); });
	$('#btnAdd').click(function() { UI_DashboardMsg.AddOnClick(); });
	$('#btnSave').click(function() { UI_DashboardMsg.saveClick(); });
	$('#btnReset').click(function() { UI_DashboardMsg.resetonClick(); });
	
	$("#btnEdit").disableButton();
	$("#btnSave").disableButton();
	
    UI_Common.initilizeMessage();    
    $("#frmModify").disableForm();
	$('#fromDate').datepicker( "disable" );
	$('#toDate').datepicker( "disable" );
    $('#btnSave').disableButton();

    $('#divTabbedVisiblity').tabs();
    
//    $("#frmModify :input").bind('paste', function(e) {     
//        return false;
//    });
});

/**
 * UI_DashboardMsg for Dashboard Msg UI related functionalities
 */
function UI_DashboardMsg() {
}

UI_DashboardMsg.hideUserPop = function (){
	UI_DashboardMsg_User.initilizePopup();
}

UI_DashboardMsg.onAddEditDataPopulate = function (status){
	UI_DashboardMsg_Agent.addEdit = status;
}


UI_DashboardMsg.initilizeVisibility = function (){
	UI_DashboardMsg_POS.initilize();
	UI_DashboardMsg_AgentType.initilize();
	UI_DashboardMsg_Agent.initilize();
	UI_DashboardMsg_User.initilize();
	$('#divTabbedVisiblity').tabs('option', 'active',0);
}

UI_DashboardMsg.createSelectedUsrList = function (userStr){	
	var userDAtaMap = $("#tblDashboardMsg").getGridParam('userData');
	UI_DashboardMsg_User.selectedUSers = new Object();	
	if(userStr != null && userStr != ""){
		for ( var i = 0; i < userStr.length; i++) {
			UI_DashboardMsg_User.selectedUSers[userStr[i]] = {value:userDAtaMap[userStr[i]],userID:userStr[i]};
		}		
		UI_DashboardMsg_User.addSelectedUsers(UI_DashboardMsg_User.selectedUSers);
	}else {
		UI_DashboardMsg_User.addSelectedUsers(null);
	}

	
}

UI_DashboardMsg.searchData = function (){
	if(UI_DashboardMsg.validateSearch()) {		
		var postData = {};
		postData['msgSearchCriteria.messageType']= $('#selSearchMsgType').val();
		postData['msgSearchCriteria.fromDate']= $('#searchFrom').val();
		postData['msgSearchCriteria.toDate'] = $('#searchTo').val();
		postData['msgSearchCriteria.user'] = $('#searchUser').val();
		postData['msgSearchCriteria.messageContent'] = $('#searchMessage').val();
	 		 	
		$("#tblDashboardMsg").setGridParam({page:1,postData:postData});
	 	$("#tblDashboardMsg").trigger("reloadGrid");
	}
}

UI_DashboardMsg.validateSearch = function (){
	return true;
}

UI_DashboardMsg.saveClick = function (){
	if(!UI_DashboardMsg.validateSave()){
		return false;
	}
	UI_DashboardMsg.beforeSaveFormSubmit();	
	$('#frmModify').ajaxSubmit({ dataType: 'json',
		success: UI_DashboardMsg.afterSave		
	});
}

UI_DashboardMsg.beforeSaveFormSubmit = function (){	
	var status = 'INA';
	if($('#chkStatus').attr('checked')) status = 'ACT'; 
	$('#hdnStatus').val(status);
	
	var selectedAgents = UI_DashboardMsg_Agent.getMultiSelectData("agent");	
	var selectedAgentTypes = UI_DashboardMsg_AgentType.getMultiSelectData("agentType");
	var selectedPOS = UI_DashboardMsg_POS.getMultiSelectData("users");	
	var includeAgents = UI_DashboardMsg_Agent.getInlusionStatus();
	var includeAgentTypes = UI_DashboardMsg_AgentType.getInlusionStatus();
	var includePOS = UI_DashboardMsg_POS.getInlusionStatus();
	var includeUsers = UI_DashboardMsg_User.getInlusionStatus();
	
	$('#hdnAgentInclusionStatus').val(includeAgents);	
	$('#hdnUserInclusionStatus').val(includeUsers);
	$('#hdnAgentTypeInclusionStatus').val(includeAgentTypes);	
	$('#hdnPosInclusionStatus').val(includePOS);	
	$('#hdnSelectedAgents').val(selectedAgents);	
	$('#hdnSelectedAgentTypes').val(selectedAgentTypes);
	$('#hdnSelectedPOS').val(selectedPOS);	
	UI_DashboardMsg.setSaveDataForUsers();
}

UI_DashboardMsg.setSaveDataForUsers = function (){
	var selectedUsers = UI_DashboardMsg_User.getSelectedUsers();
	var count = 0;
	var userArr = new Array();
	for ( var key in selectedUsers) {
		if(selectedUsers[key] != null && selectedUsers[key] != ""){
			var userID = selectedUsers[key].userID;
			if(userID != null && userID != "")
				userArr.push(userID);
		}
	}	
	$('#hdnUserList').val(userArr);	
}


UI_DashboardMsg.validateSave = function (){
	if($("#selMsgType").isFieldEmpty()){
		UI_Common.showCommonError("Error", msgTypeEmpty);			
		return false;
	}else if($("#fromDate").isFieldEmpty()){
		UI_Common.showCommonError("Error", frmDateEmpty);
		return false;
	}else if($("#toDate").isFieldEmpty()){
		UI_Common.showCommonError("Error", toDateEmpty);
		return false;
	}else if($("#txtEditPriority").isFieldEmpty()){
		UI_Common.showCommonError("Error", priorityEmpty);
		return false;	
	}else if($("#txtMessage").isFieldEmpty()){
		UI_Common.showCommonError("Error", messageEmpty);			
		return false;
	}else if($("#txtMessage").val().length > 2000){
		UI_Common.showCommonError("Error", msgLimitLong);			
		return false;
	}	
	if(!CheckDates($("#fromDate").val(),$("#toDate").val())){
		UI_Common.showCommonError("Error", fromDateGreater);			
		return false;
	}
	if(!UI_DashboardMsg.isValidateContentDynamic($("#txtEditPriority").val())){
		UI_Common.showCommonError("Error", msgPriorityInvalid);			
		return false;
	}
	if(!UI_DashboardMsg.isValidateContentDynamic($("#txtMessage").val())){
		UI_Common.showCommonError("Error", msgInvalid);			
		return false;
	}
	if(!$("#fromDate").attr('disabled')){
		//validating from date only if date can be edited
		var dtToday = new Date();
		var dtCurrD	= dtToday.getDate();
		var dtCurrM	= (dtToday.getMonth() + 1);
		var dtCurrY	= dtToday.getFullYear();
		if(dtCurrD <10)dtCurrD = "0"+dtCurrD;
		if(dtCurrM <10)dtCurrM = "0"+dtCurrM;	
		if(!CheckDates( (dtCurrD+"/"+dtCurrM+"/"+dtCurrY),$("#fromDate").val())){
			UI_Common.showCommonError("Error", fromDtShdFurDt);			
			return false;
		}
	}

	if(!UI_DashboardMsg_POS.validateInclusion()){
		UI_Common.showCommonError("Error",posInclusion);			
		return false;
	}
	if(!UI_DashboardMsg_Agent.validateInclusion()){
		UI_Common.showCommonError("Error",agentInclusion);			
		return false;
	}
	if(!UI_DashboardMsg_AgentType.validateInclusion()){
		UI_Common.showCommonError("Error",agentTypeInclusion);			
		return false;
	}
	if(!UI_DashboardMsg_User.validateInclusion()){
		UI_Common.showCommonError("Error",userInclusion);			
		return false;
	}
	
	return true;
}

UI_DashboardMsg.afterSave = function (response){
	if(response.msgType != 'Error'){
		var postData = {};
		$('#frmMessagesSearch').clearForm();
		$("#frmModify").clearForm();
		$('#selSearchMsgType').val("");
		postData['msgSearchCriteria.messageType']= "";
		postData['msgSearchCriteria.fromDate']= "";
		postData['msgSearchCriteria.toDate'] = "";
		postData['msgSearchCriteria.user'] = "";
		postData['msgSearchCriteria.messageContent'] = "";
		$("#tblDashboardMsg").setGridParam({page:1,postData:postData});
	 	$("#tblDashboardMsg").trigger("reloadGrid");
	}	
	UI_Common.displayMessage (response);	
}

UI_DashboardMsg.clearEditArea = function (){
	$("#frmModify").clearForm();
	$("#btnEdit").disableButton();
	$("#btnSave").disableButton();
	UI_DashboardMsg.hideUserPop();
	UI_DashboardMsg.initilizeVisibility ();
}

UI_DashboardMsg.refreshGrid = function (){	
	$("#frmModify").disableForm();
	$('#fromDate').datepicker( "disable" );
	$('#toDate').datepicker( "disable" );
	UI_DashboardMsg.clearEditArea();
	top[2].HideProgress();
	UI_Common.initilizeMessage();	
	$("#tblDashboardMsg").trigger("reloadGrid");//This is an  Event
}

UI_DashboardMsg.resetonClick = function (){	
	if($("#tblDashboardMsg").getGridParam("selrow") != null){
		UI_DashboardMsg.gridRowOnClick($("#tblDashboardMsg").getGridParam("selrow"));
	} else {
		$("#frmModify").clearForm();
		$('#frmModify').disableForm();
		$("#frmMessagesSearch").clearForm();	
		$('#selSearchMsgType').val("");		
		UI_DashboardMsg.initilizeVisibility();
		UI_DashboardMsg.enableVisiblity (false);
		$("#tblDashboardMsg").setGridParam({page:1,postData:{}});
		$("#tblDashboardMsg").trigger("reloadGrid");
	}	
}

UI_DashboardMsg.editOnClick = function (){
	UI_DashboardMsg.onAddEditDataPopulate("EDIT");
	$("#btnSave").enableButton();
	$("#frmModify .frm_editable").enableFormSelective();
	UI_DashboardMsg.enableVisiblity (true);
	
	var selectedRow = $("#tblDashboardMsg").getRowData($("#tblDashboardMsg").getGridParam("selrow"));	
	var varEditFromDate = selectedRow["isFromDateEditable"];
	var varEditFromDate = selectedRow["isToDateEditable"];
	if(varEditFromDate === "true"){
		$('#fromDate').attr('disabled', false);
		$('#fromDate').datepicker( "enable" );
	}else {
		$('#fromDate').attr('disabled', true);
		$('#fromDate').datepicker( "disable" );
	}
	if(varEditFromDate === "true"){
		$('#toDate').attr('disabled', false);
		$('#toDate').datepicker( "enable" );
	}else {
		$('#toDate').attr('disabled', true);
		$('#toDate').datepicker( "disable" );
	}
}

UI_DashboardMsg.gridRowOnClick = function (rowid){
	UI_DashboardMsg.onAddEditDataPopulate("EDIT");
	UI_Common.initilizeMessage();
	UI_DashboardMsg.initilizeVisibility();
	$("#tblDashboardMsg").GridToForm(rowid,"#frmModify");
	var selectedRow = $("#tblDashboardMsg").getRowData(rowid);	
	if(selectedRow['status'] == 'ACT'){
		$('#chkStatus').attr('checked',true);
	}else {
		$('#chkStatus').attr('checked',false);
	}
	$("#frmModify").disableForm();
	$('#fromDate').datepicker( "disable" );
	$('#toDate').datepicker( "disable" );
	$("#btnEdit").enableButton();	
	UI_DashboardMsg.hideUserPop();
	UI_DashboardMsg.populateVisility(rowid);
	
}

UI_DashboardMsg.populateVisility = function (rowid){
	var selectedRow = $("#tblDashboardMsg").getRowData(rowid);	
	if(selectedRow['agentInclusion'] == 'Y'){
		$('#radAgent_inc').attr('checked',true);
		$('#radAgent_inc_e').attr('checked',false);
	}else if(selectedRow['agentInclusion'] == 'N'){
		$('#radAgent_inc').attr('checked',false);
		$('#radAgent_inc_e').attr('checked',true);
	}
	if(selectedRow['agentTypeInclusion'] == 'Y'){
		$('#radAgentType_inc').attr('checked',true);
		$('#radAgentType_inc_e').attr('checked',false);
	}else if(selectedRow['agentTypeInclusion'] == 'N') {
		$('#radAgentType_inc').attr('checked',false);
		$('#radAgentType_inc_e').attr('checked',true);
	}
	if(selectedRow['userInclusion'] == 'Y'){
		$('#radUser_inc').attr('checked',true);
		$('#radUser_inc_e').attr('checked',false);
	}else if(selectedRow['userInclusion'] == 'N'){
		$('#radUser_inc').attr('checked',false);
		$('#radUser_inc_e').attr('checked',true);
	}
	if(selectedRow['posInclusion'] == 'Y'){
		$('#radPOS_inc').attr('checked',true);
		$('#radPOS_inc_e').attr('checked',false);
	}else if(selectedRow['posInclusion'] == 'N'){
		$('#radPOS_inc').attr('checked',false);
		$('#radPOS_inc_e').attr('checked',true);
	}	
	UI_DashboardMsg.createSelectedUsrList(eval(selectedRow['users']));
	UI_DashboardMsg_Agent.setSelectedItems(selectedRow['agents']);
	UI_DashboardMsg_AgentType.setSelectedItems(eval(selectedRow['agentTypes']));
	UI_DashboardMsg_POS.setSelectedItems(eval(selectedRow['pos']));
}

UI_DashboardMsg.enableVisiblity = function (blnStatus){
	UI_DashboardMsg_AgentType.enableMultiSelect(blnStatus);	
	UI_DashboardMsg_POS.enableMultiSelect(blnStatus);
	UI_DashboardMsg_User.enableInputs(blnStatus);
	UI_DashboardMsg_Agent.enableMultiSelect(blnStatus);
}

UI_DashboardMsg.AddOnClick = function (){
	UI_DashboardMsg.onAddEditDataPopulate("ADD");
	UI_DashboardMsg.clearEditArea();
	$("#btnSave").enableButton();
	$("#frmModify").enableForm();
	$('#fromDate').datepicker( "enable" );
	$('#toDate').datepicker( "enable" );
	$("#tblDashboardMsg").resetSelection();
	UI_DashboardMsg.enableVisiblity (true);
}

UI_DashboardMsg.kpValidateContentDynamic = function(control){
	var strCC = $('#'+control).val();
	var strLen = strCC.length;
	var blnVal = UI_DashboardMsg.isValidateContentDynamic(strCC);
	if (!blnVal) {
//		var startIndex = strCC.indexOf(strCC);
		$('#'+control).val(strCC.substr(0, strLen - 1));
		$('#'+control).focus();
	}
}

UI_DashboardMsg.isValidateContentDynamic = function(s){return RegExp("^[a-zA-Z0-9_.', \@ \& \n\w\s]+$").test(s);}


