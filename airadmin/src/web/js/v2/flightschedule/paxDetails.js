/**
 * 
 */


function UI_paxDetails() {
}

$(document).ready(function() {
	UI_paxDetails.ready();
});

UI_paxDetails.ready = function(){
	jQuery("#listPaxDetails").jqGrid({ 
		url:'showPaxDetails.action',
		datatype: "json",
		postData: {
			hdnFlightId: function() { return hidFlightID; }
		},
		jsonReader : {
			  root: "rows", 
			  page: "page",
			  total: "total",
			  records: "records",
			  repeatitems: false,					 
			  id: "0"				  
			},													
			colNames:['&nbsp;','PNR','PAX Name', 'Frequent Flyer Y/N','Connection Flight','Reprotected','Pax Type'],  
			colModel:[ 	{name:'id', width:20},   
			        	{name:'pnr',index:'pnr', width:100, align:"center", jsonmap:'paxDetail.pnr', formatter: 
			        		function(cellVal, options, rowObject){
								return "PNR : " + cellVal
							}
			        	}, 
						{name:'paxName' ,index:'paxName' , width:225, align:"left", jsonmap:'paxName'},
					   	{name:'feaquentFlyer',index:'feaquentFlyer' , width:70, align:"center", jsonmap:'paxDetail.FFID'},
					   	{name:'connectionFlight',index:'connectionFlight' ,width:250,  align:"left", jsonmap:'conection'},
					   	{name:'alreadyReprotected',index:'alreadyReprotected' , width:75, align:"center", jsonmap:'reprotect'},
					   	{name:'paxType',index:'route' , width:70, align:"center", jsonmap:'paxDetail.paxType'}
					  ], imgpath: '../../themes/default/images/jquery', multiselect:false,
		pager: jQuery('#pagingPaxDetails'),
		rowNum:20,						
		viewrecords: true,
		height:460,
		width:950,
		groupingView: {
			groupField : ['pnr'],
			groupOrder : ['asc'],
			minusicon : 'ui-icon-triangle-1-n',
			plusicon : 'ui-icon-triangle-1-s',
			groupColumnShow : [false]
		},
		grouping : true,
		loadui:'block',
		loadComplete: function(data , statusText){
			responseTextObj = data;
			var error = responseTextObj['errorMsg'];
			if(error == null){
				
			} else {
				$("#listPaxDetails").clearGridData();
				showWindowCommonError("Error", "No Data Recieve, Please Contact System Admin");
			}
		}
	}).navGrid("#pagingPaxDetails",{refresh: true, edit: false, add: false, del: false, search: false});
	

	
}

UI_paxDetails.close = function(){	
	window.close();
}