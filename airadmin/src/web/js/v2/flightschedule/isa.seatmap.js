/**
 * @author Baladewa Welathanthri
 */
(function($) {
	$.seatMap = {
			cabinCls: null,
			seatCellConfig :null,
			intLen: null,
			intSegRow: null,
			mapHLayout: true,
			lagend:{"seatAvails":[{"key":"paxSelected","text":"Available"},{"key":"paxSelected","text":"Selected"},{"key":"inactiveSeat","text":"Occupied"}],
			        "seatTypes":[],
					"paxTypes":[]},
			availLCCArray:[],
			buildSeatmap : function(smContainer ,segmentSeatMapDTOs, extParams, callBack){
				//initialize SM
				init = function(){
					jsSMModel = segmentSeatMapDTOs.lccSegmentSeatMapDTOs;
					$.seatMap.cabinCls = jsSMModel[0].lccSeatMapDetailDTO.cabinClass;
					$.seatMap = $.extend($.seatMap, extParams);
				};
				//create SM Cells
				createExtCells = function(so){
					var tblCell = document.createElement("TD");
					var strBold = "";
					var strStyle = "style=";
					p = $.extend({ id : "", rowSpan : 0, colSpan : 0, bold : false, textCss : "", click : "", mouseOver : "", mouseOut : "",
						title : "", css : "", width: "70", height: "60", align : "center", vAlign : "middle" }, $.seatMap.seatCellConfig);
					if (so.seatObj){p.id=so.seatNumber}
					if (so.seatObj && p.bold) {	strBold = "txtBold";}
					if (p.width != "") {strStyle +="width:"+p.width+"px;"}
					if (p.height != "") {strStyle +="height:"+p.height+"px;"}
					if (p.textCss != "") {strStyle += p.textCss+";"}
					if (p.id != "") {tblCell.setAttribute("id", p.id);}
					if (p.title != "") {tblCell.setAttribute("title", p.title);}
					if (p.rowSpan > 1) {tblCell.setAttribute("rowSpan", p.rowSpan);}
					if (p.colSpan > 1) {tblCell.setAttribute("colSpan", p.colSpan);}
					if (p.css != "") {tblCell.className = p.css;}
					if (p.align != "") {tblCell.setAttribute("align", p.align);}
					if (p.vAlign != "") {tblCell.setAttribute("valign", p.vAlign);}
					//event
					if (p.click != "") {tblCell.onclick = p.click;}
					if (p.mouseOver != "") {tblCell.onmouseover = p.mouseOver;}
					if (p.mouseOut != "") {tblCell.onmouseout = p.mouseOut;}
					tblCell.className = ($.seatMap.mapHLayout?"HMap ":"VMap ")
					if (!so.seatObj){
						tblCell.innerHTML = "<p class='" + strBold + "'>" + so.desc + "<\/p>";
						tblCell.className = tblCell.className + " " + so.css;
					}else{
						var divSaetCell = createDivSeat(so);
						var createTipElements = function(obj, heig){
							var strPax = "No Pax Available";
							var paxObj = $(obj).find(".paxClass");
							var tipSeatItems = $.data($(obj)[0], "seatInfor");
							var tipPaxItems = $.data($(paxObj)[0], "paxInfor");
							if (tipPaxItems.pnr != undefined && tipPaxItems.pnr != ""){
								var pname =  tipPaxItems.paxTitle + " " + tipPaxItems.paxLastName + " " + tipPaxItems.paxFirstName
								strPax = "<table>" +
										"<tr><td colspan='2'><b><u>Pax Details</u></b></td></tr>"+
										"<tr><td width='25%'><b>PNR :</b></td><td> " + tipPaxItems.pnr + "</td></tr>"+
										"<tr><td><b>Pax Name:</b></td><td><div class='overflowed'>" + pname + " ["+ tipPaxItems.bookedPassengerType +"]</div></td></tr>"+
										"</table>" ;
							}
							var tipSNo = null;
							if (tipSeatItems.seatNumber){
								tipSNo = $("<div>"+tipSeatItems.seatNumber+" - Seat Info </div>").attr("class", "tip_topRow");
							}
							var paxDic = $("<div>"+strPax+"</div>").attr("class", "textBlock");
							var base = $("<div></div>").attr({"class": "tipBase","style":"height:"+heig+"px"});
							base.append(tipSNo, paxDic);
							return base;
						};
						
						
						$(divSaetCell).mouseenter(function(){
							if ($(this).find(".extToolTip").length==0 && $(this).find(".seatDrop").children().length!=0){
								var tipH = 80;
								var tipW = 250;
								var canW = parseInt($("#setFScroll").css("width"),10);
								var canH = parseInt($("#setFScroll").css("height"),10);
								var p = $(this).find(".tipItem").position();
								var tipTop = p.top;
								var tipLeft = p.left;
								var hC = "left";
								var vC = "Top";
								if (canW < (tipLeft + tipW)){
									hC = "right";
									tipLeft = tipLeft;
								}
								//if (canH < (tipTop + tipH)){
								//	 vC = "Bottom";
									// tipTop = tipTop;
								//}
								var pointerClass = hC + "" + vC;
								var tipDiv = $("<div></div>").attr({
									"id": so.seatNumber +"_seat",
									"class": "extToolTip " + pointerClass
								});
								/*tipDiv.click(function(){
									if ($(this).hasClass("pinned")){
										$(this).removeClass("pinned");
									}else{
										$(this).addClass("pinned");
									}
								});*/
								tipDiv.append(createTipElements(this,tipH));
								tipDiv.css({
									"position":"absolute",
									"display":"block",
									"top":tipTop,
									"width":tipW,
									"height":tipH,
									"left":tipLeft + 10
								});
								$(this).find(".tipItem").empty().append(tipDiv);
							}
						});
						$(divSaetCell).mouseleave(function(){
							if (!$(this).find(".extToolTip").hasClass("pinned")){
								$(this).find(".tipItem").empty();
							}
						})
						tblCell.appendChild(divSaetCell);
					}
					return tblCell;
				};
				
				createDivPax = function(seat){
					var strPaxClass = "";
					var strImgPaxClass = "";
					var strPaxGenClass = "";

					switch (seat.bookedPassengerType) {
					case "PA":
						strPaxClass = "paxParent";
						if (seat.gender == "M"){
							strImgPaxClass = "mPaxParent";
						}else if(seat.gender == "F"){
							strImgPaxClass = "fPaxParent";
						}else{
							strImgPaxClass = "uPaxParent";
						}
						break;
					case "AD":
						strPaxClass = "paxAdult";
						if (seat.gender == "M"){
							strImgPaxClass = "mPaxAdult";
						}else if(seat.gender == "F"){
							strImgPaxClass = "fPaxAdult";
						}else{
							strImgPaxClass = "uPaxAdult";
						}
						break;
					case "CH":
						strPaxClass = "paxChild";
						if (seat.gender == "M"){
							strImgPaxClass = "mPaxChild";
						}else if(seat.gender == "F"){
							strImgPaxClass = "fPaxChild";
						}else{
							strImgPaxClass = "uPaxChild";
						}
						break;
					default:
						strPaxClass = "paxUnknow";
						if (seat.gender == "M"){
							strImgPaxClass = "mPaxUnknow";
						}else if(seat.gender == "F"){
							strImgPaxClass = "fPaxUnknow";
						}else{
							strImgPaxClass = "uPaxUnknow";
						}
						break;
					}
					//select pax gender
					switch (seat.gender) {
					case "M":
						strPaxGenClass = "paxGenMale";
						break;
					case "F":
						strPaxGenClass = "paxGenFemale";
						break;
					default:
						strPaxGenClass = "paxGenUnknow";
						break;
					}
					var tpDiv = document.createElement("div");
					//var tpDiv = "<div class='paxClass draggable "+strImgPaxClass+" "+strPaxClass+" "+strPaxGenClass+"'><span></span></div>";
					tpDiv.className = "paxClass draggable " + strPaxClass + " " + strPaxGenClass;
					var paxData = {};
					paxData.pnr = seat.pnr;
					paxData.paxTitle = seat.paxTitle;
					paxData.paxLastName = seat.paxLastName;
					paxData.paxFirstName = seat.paxFirstName;
					paxData.bookedPassengerType = seat.bookedPassengerType;
					$.data(tpDiv, "paxInfor", paxData);//Set seat data
					$(tpDiv).mousedown(function(){
						
					})
					return tpDiv
				};
				
				createDivSeat = function(seat){
					//set SeatType 
					var setSeatTypeHTML = function(typs, seatNo){
						var tdDrop = $("<td align='left' valign='top' class='seatDrop droppable'></td>").css({
							"background":"#fff",
							"height":"16px",
							"width":"16px"
						})
						if (seat.pnr != null && seat.pnr != ""){
							tdDrop.append(createDivPax(seat));
						}
						var tr1 = $("<tr></tr>").append(tdDrop);
						var tr2 = $("<tr><td height='0'><span class='tipItem'></span></td></tr>");
						var objTable = $("<table cellpedding='0' cellspacing='1' width='100%' height='100%'></table>").append(tr1, tr2);
						return objTable;
	
					};
					
					var strSeatClass = "";
					//creating Seat
					var tsDiv = document.createElement("div");
					
					var sData = {};//TODO add seat specific data
					sData.seatNumber = seat.seatNumber;
					sData.seatAvailability = seat.seatAvailability;
					
					$.data(tsDiv, "seatInfor", sData);//Set seat data
					switch (seat.seatAvailability) {
					case "VAC":
						strSeatClass = "inactiveSeat";
						break;
					case "INA": 
						strSeatClass = "inactiveSeat"; 
					break;
					case "RES": 
						strSeatClass =  "paxSelected"; 
					break; // occupied
					default:
						strSeatClass = "paxSelected";
						break;
					}
					tsDiv.className = "seatClass " + ($.seatMap.mapHLayout?"horisontalSeat ":"verticalSeat ") + strSeatClass;
					$(tsDiv).append( setSeatTypeHTML(seat.seatType, seat.seatNumber) );
					return tsDiv;
				};
				
			
				seatRowComparator = function(first, second) {
					var numF = parseInt(first[0].rowNum, 10);
					var numS = parseInt(second[0].rowNum, 10);
					return (numF - numS);
				};
				
				seatRowLableComparator = function(first, second) {
					var numF = parseInt(first.rowNum, 10);
					var numS = parseInt(second.rowNum, 10);
					return (numF - numS);
				};
				
				createRefobject = function (seatStatus,seatNumber,seatType,seatZone) {
					
					var refObj = '{seatStatus:\"' + seatStatus + '\",seatNumber:\"' + seatNumber;
					
					if(seatType) {
						refObj = refObj + '\", seatType:\"' + seatType;	
					} 
					
					if(seatZone) {
						refObj = refObj + '\" , seatZone:\"'+ seatZone;	
					}
					 
					refObj = refObj + '\"}';
					return refObj;
				};
	
				//Process SM
				processSeatamp = function(){
					if ($.seatMap.cabinCls != null) {
						var ccLen = $.seatMap.cabinCls.length;				
						
						if(ccLen>0){
							/* Initialize the Details */
							$("#"+smContainer).find("tr").remove();
							//$("#tbdyWingRight").find("tr").remove();
							//$("#tbdyWingLeft").find("tr").remove();				
					
							$("#"+smContainer).show();
							
							var tblRow = null;
							var tblWingRowLeft = null;
							var tblWingRowRight = null;
							var isleRow = null;
							var blankIsle = {desc:"&nbsp;", 
									css:"aisle",
									textCss:"font-size:9px;",
									align:"right"};
							
							var columnLetters = new Array('A','B','C','D','E','F','G','H','I','J','K');
							var colLtrStr = "ABCDEFGHIJK";
							
							$("#"+smContainer).append("<tr id='trExtCabinClass_"+smContainer+"'></tr>");
							//$('#tbdyWingRight').append("<tr id='trRWCC'></tr>");
							//$('#tbdyWingLeft').append("<tr id='trLWCC'></tr>");
							var buildLetters = true;
							for(var ccI=0; ccI < ccLen ; ccI++){
								buildLetters = true;
								var cc  = $.seatMap.cabinCls[ccI];
								var activeCC = true;

								$('#trExtCabinClass_'+smContainer).append('<td><div id="dragHElper_'+smContainer+'"></div><div id="setFScroll_'+smContainer+'"><table cellspacing="0" cellpadding="0" id="setTScroll"><tbody class="cabinClass '+
										cc.cabinType+'" style="margin-right:5px;"><tr id="trExtCC_'+ccI+'_'+smContainer+'"></tr></tbody></table></div></td>');		
								$('#trExtRWCC').append('<td><table ><tbody  style="margin-right:5px;"><tr id="trExtCCRW_'+ccI+'_'+smContainer+'"></tr></tbody></table></td>');		
								$('#trExtLWCC').append('<td><table ><tbody  style="margin-right:5px;"><tr id="trExtCCLW_'+ccI+'_'+smContainer+'"></tr></tbody></table></td>');	
								tblWingRowRight = document.createElement("TR");
								tblWingRowLeft = document.createElement("TR");
								
								var rgDtos = cc.airRowGroupDTOs;
								var winExitRightArr = new Array(); 
								var winExitLeftArr = new Array();
								for(var rgI=0; rgI< rgDtos.length;rgI++){
									$('#trExtCC_'+ccI+'_'+smContainer).append('<td><table cellspacing="0" cellpadding="0"><tbody class="tbSeatMap" id="tbodyExtRG_'+ccI+'_'+rgI+'_'+smContainer+'"></tbody></table></td>');
									var cgDtos = rgDtos[rgI].airColumnGroups; 
									var trArr = new Array(); //to be delete
																/*
																 * array to contain td
																 * elements
																 */
									var trArrNew = new Array(); 
									
									/*
									 * additional windows required for letters rows -
									 * column grps
									 */
									var winCell = {desc:"&nbsp;", 
											css:'window',
											textCss:"font-size:9px;",
											rowNum:0,
											align:"center"};
									winExitRightArr[winExitRightArr.length] = winCell;
									winExitLeftArr[winExitLeftArr.length] = winCell;
									
									isleRow =new Array();
									var isleGrpSeatLetterId = {};
									var isleGrpId = 0;
									if(buildLetters){
										columnLetters = [];
										colLtrStr = "";
										for(var cgI=0; cgI< cgDtos.length; cgI++ ){
											if(cgDtos[cgI].airRows.length>0){
												var seats = cgDtos[cgI].airRows[0].airSeats;
												for(var sI = 0; sI < seats.length;sI++){
													var colLetter = ((new String(seats[sI].seatNumber)).replace(/[\d]+/g, '')).toUpperCase();
													columnLetters[columnLetters.length] = colLetter;
													isleGrpSeatLetterId[colLetter] =isleGrpId;
												}
												isleGrpId++;
											}
										}
										columnLetters = $.airutil.sort.quickSort(columnLetters,$.airutil.string.comparator);
										
										//make layout change vertical
										if (!$.seatMap.mapHLayout){
											columnLetters.reverse();
										}
										colLtrStr = columnLetters.join("");
										buildLetters = false;
									}
									
									for(var cgI=0; cgI< cgDtos.length; cgI++ ){
										var rows  = cgDtos[cgI].airRows;
										/* initialize tr arrays */
										if(cgI == 0){
											for(var k =0; k< rows.length; k++){
												trArr[k] = new Array();//to be delete
												trArrNew[k] = new Array();
											}
										}
										
										for(var rI =0; rI< rows.length; rI++){
											var lastSeatC = "matchCC";
											var seats = rows[rI].airSeats;		
											var seatLogicalCC = "";
											
											for(var sI = 0; sI < seats.length;sI++){
												var seatNo = seats[sI].seatNumber;
												var colLetter = ((new String(seatNo)).replace(/[\d]+/g, '')).toUpperCase();										
												var letterIndex = colLtrStr.indexOf(colLetter);
												var sdLtrIndex = letterIndex % seats.length;
												var tIndex = rI;
												//var totCols = seats.length*cgDtos.length;
												var totCols = colLtrStr.length;
												var colIndex = ( totCols- (letterIndex+1));
												seats[sI].rowNum = rows[rI].rowNumber;
												seats[sI].seatObj = true;
												seats[sI].seatLetter = colLetter;
												trArrNew[tIndex][colIndex] = seats[sI];
												
												var blnFirstRow = (cgI == 0 && (colIndex == 0));
												var blnLastRow = (cgI == (cgDtos.length-1)) && (colIndex == (totCols -1));
												
												
												if(blnFirstRow||blnLastRow){
													switch (seats[sI].seatType){
														case "EXIT" : strWindowClass =(blnFirstRow? "exitRight":"exitLeft"); break;
														default : strWindowClass = "window"; break;
													}				
													var winExitCell = {desc:"&nbsp;", 
															css:strWindowClass,
															textCss:"font-size:9px;",
															rowNum:rows[rI].rowNumber,
															align:"center"};
													if(blnFirstRow){
														winExitRightArr[winExitRightArr.length] = winExitCell;										
													}else{
														winExitLeftArr[winExitLeftArr.length] = winExitCell;			
													}
												}
												
											}
											// creating the isle row
											var cellIsle = {desc:(rows[rI].rowNumber), 
													css:"aisle numberRow "+lastSeatC,
													textCss:"font-size:9px;",
													rowNum:rows[rI].rowNumber,
													align:"right"};
											isleRow[rI] = cellIsle;
										}
									} /* end of column grp loop */
									
									
									/* populate table for rows per row group */
									/* cols = = = */
									/* rows | | | */
									
									if(trArrNew.length>0){//CHANGE
										trArrNew = $.airutil.sort.quickSort(trArrNew, seatRowComparator);//CHANGE
										var colNos = null;
										var rowNos = null;
										var colsPerCGrp = null;
										var letterIndex = null;
										if ($.seatMap.mapHLayout){
											colNos = trArrNew[0].length;//CHANGE
											rowNos = trArrNew.length;//CHANGE
											colsPerCGrp = parseInt(colNos/cgDtos.length,10);
											letterIndex = colNos;
											//** adding seats and isles for row groups *//
											for(var i=0; i < colNos; i++){							
												tblRow = document.createElement("TR");										
												
												for(var j=0; j< rowNos; j++ ){
													
													/*
													 * booking class seat column letters print
													 */
													 
													if(j==0 && letterIndex>0){
														var letterCell = {desc:columnLetters[--letterIndex], 
																css:"aisle",
																textCss:"font-size:9px;font-weight:bold;color:red;",
																align:"right"};
														tblRow.appendChild(createExtCells(letterCell));
													}
													// adding seat columns to TR 
													tdObj = createExtCells(trArrNew[j][i]);																					
													tblRow.appendChild(tdObj);
												}
												$('#tbodyExtRG_'+ccI+'_'+rgI + '_' + smContainer).append(tblRow);
												
												
												 // creating isles inbetweeen seat column grps
												 
												if( (i+1)% colsPerCGrp ==0 && i < colNos-1){
													isleRow = $.airutil.sort.quickSort(isleRow, seatRowLableComparator);
													isleRowtblRow = document.createElement("TR");	
													isleRowtblRow.appendChild(createExtCells(blankIsle)); 
													for (var k =0; k < rowNos; k++){
														isleRowtblRow.appendChild(createExtCells(isleRow[k]));
													}
													$('#tbodyExtRG_'+ccI+'_'+rgI + '_' + smContainer).append(isleRowtblRow);
												}
																					
											}	
									}else{
											colNos = trArrNew.length;
											rowNos = trArrNew[0].length;
											colsPerCGrp = parseInt(rowNos / cgDtos.length, 10);
											letterIndex = rowNos;
											/** adding seats and isles for row groups */
	
											tblRowColomn = document.createElement("TR");
											var lastIsleGrpId = 0;
											for ( var k = 0; k < rowNos ; k++) {
	
												/*
												 * booking class seat column letters print
												 */
												 
												if(k < rowNos){
													if(k == 0){
														lastIsleGrpId = isleGrpSeatLetterId[trArrNew[0][k].seatLetter];
													}
													if(lastIsleGrpId != isleGrpSeatLetterId[trArrNew[0][k].seatLetter]){
														tblRowColomn.appendChild(createExtCells(winCell));
														lastIsleGrpId = isleGrpSeatLetterId[trArrNew[0][k].seatLetter]
														k--;
													}else {
														if (letterIndex > 0) {
															var letterCell = { desc : columnLetters[--letterIndex], css : "aisle",
																textCss : "font-size:9px;font-weight:bold;color:red;", align : "center" };
															tblRowColomn.appendChild(createExtCells(letterCell));
														}
													}
												} else {
													tblRowColomn.appendChild(createExtCells(winCell));
												}
											}
	
											$('#tbodyExtRG_' + ccI + '_' + rgI + '_' + smContainer).append(tblRowColomn);
											for ( var i = 0; i < colNos; i++) {
												isleRow = $.airutil.sort.quickSort(isleRow, seatRowLableComparator);
												trArrNew[i][trArrNew[i].length] = isleRow[i];
	
												setRowNumberMiddle = function(op) {
													var o = op.pop();
													var lastId = -1;
													var oplength = op.length;
													for(var j = 0 ; j < oplength; j++){
														if(j == 0){
															lastId = isleGrpSeatLetterId[op[j].seatLetter];
														}
														if(lastId != isleGrpSeatLetterId[op[j].seatLetter]){
															lastId= isleGrpSeatLetterId[op[j].seatLetter]
															op.splice(j, 0, o);
															oplength++;
														}
													}
													return op;
												}
												trArrNew[i] = setRowNumberMiddle(trArrNew[i]);
												tblRow = document.createElement("TR");
												rowNos = trArrNew[0].length;
												for ( var j = 0; j < rowNos; j++) {
	
													/* adding seat columns to TR */
													tdObj = createExtCells(trArrNew[i][j]);
													tblRow.appendChild(tdObj);
												}
												$('#tbodyExtRG_' + ccI + '_' + rgI + '_' + smContainer).append(tblRow);
	
											}
										}

									}
								} /* end of row groups loop */
								
								/* creating left wing windows */				
								winExitRightArr = 	$.airutil.sort.quickSort(winExitRightArr, seatRowLableComparator);		
								for(var i=0; i < winExitRightArr.length ; i++){							 
									tblWingRowRight.appendChild(createExtCells(winExitRightArr[i]));
								}
								$('#trExtCCRW_'+ccI+'_'+smContainer).append(tblWingRowRight);
								
								/* creating right wing windows */
								winExitLeftArr = 	$.airutil.sort.quickSort(winExitLeftArr, seatRowLableComparator);							
								for(var i=0; i < winExitLeftArr.length ; i++){
									tblWingRowLeft.appendChild(createExtCells(winExitLeftArr[i]));
								}
								$('#trExtCCLW_'+ccI+'_'+smContainer).append(tblWingRowLeft);
							} /* end of cabin class loop */
							
						}
					}
				};
				
				createExtSMLegend = function(){
					/*var LGS = $.seatMap.lagend.seatTypes;
					var LGA = $.seatMap.lagend.seatAvails;
					var LGP = $.seatMap.lagend.paxTypes;
					var strHTML = "";
					if (true){
						strHTML += "<tr><td><table><tr>";
						for (var i=0;i<LGA.length; i++){
							strHTML += "<td><table><tr><td class='seatClass horisontalSeat "+LGA[i].key+" noBorder'></td></tr><tr><td align='center'>"+LGA[i].text+"</td></tr></table></td>";
						}
						strHTML += "</tr></table></td></tr><tr><td><hr/></td></tr><tr><td><table width='80%'><tr><td width='30'>&nbsp;</td>";
						for (var j=0;j<LGS.length; j++){
							strHTML += "<td><b>"+ LGS[j].key + "</b> - " + LGS[j].text +"</td>";
						}
						strHTML += "</tr></table></td></tr><tr><td><hr/></td></tr><tr><td><table class=''><tr>";
						for (var k=0;k<LGP.length; k++){
							strHTML += "<td><table><tr><td class='HMap' align='center' valign='middle' width='90' height='43'><div class='paxClass "+LGP[k].key+"'></div></td></tr><tr><td align='center'>"+LGP[k].text+"</td></tr></table></td>";
						}
						strHTML += "</tr></table></td></tr>";
					}
					var smLedgent = $("<table border='0' cellpadding='0' cellspacing='0' align='left' width='100%'>"+strHTML+"</table>");
					$("#"+smContainer).parent().parent().append(smLedgent);*/
					
					
					var selectedClass="select-to-drag", clickDelay = 600,
			        lastClick, diffClick; // timestamps
					
					$("#"+smContainer).find( ".draggable" )
					 // Script to deferentiate a click from a mousedown for drag event
				    .bind('mousedown mouseup', function(e) {
				        if (e.type == "mousedown") {
				            lastClick = e.timeStamp; // get mousedown time
				        } else {
				            diffClick = e.timeStamp - lastClick;
				            if (diffClick < clickDelay) {
				                // add selected class to group draggable objects
				                $(this).toggleClass(selectedClass);
				            }
				        }
				    })
					.draggable({
						revertDuration: 10,
						revert:true,
						start: function(e, ui) {
				            ui.helper.addClass(selectedClass);
				        },
				        stop: function(e, ui) {
				            // reset group positions
				            $('.' + selectedClass).css({
				                top: 0,
				                left: 0
				            });
				            $(".droppable").removeClass("ui-state-hover");
				        },
				        drag: function(e, ui) {
				            // set selected group position to main dragged object
				            // this works because the position is relative to the starting position
				            $('.' + selectedClass).css({
				                top: ui.position.top,
				                left: ui.position.left
				            });
				        }
					});
					
					$("#"+smContainer).find( ".droppable" ).droppable({
			        	accept: ".select-to-drag",
			           //activeClass: "ui-state-hover",
			            //hoverClass: "ui-state-active",
			            over: function( event, ui ){
			            	var t = ui.offset;
			            	if (t.left<150){
			            		$("#tbdyExtSeatMap_TO").find(".droppable").removeClass("ui-state-hover")
			            		$("#tbdyExtSeatMap_FROM").find(".droppable").addClass("ui-state-hover");
			            	}else{
			            		$("#tbdyExtSeatMap_FROM").find(".droppable").removeClass("ui-state-hover")
			            		$("#tbdyExtSeatMap_TO").find(".droppable").addClass("ui-state-hover");
			            	}
			            },
			            tolerance:"pointer",
			            drop: function( event, ui ) {
			            	$(".tipItem").empty()
			            	if ($( this ).children().length>0){
			            		return false;
			            	}else{
			            		var tm = $("#tbdyExtSeatMap_TO");
			            		if (ui.offset.left<150){
				            		tm = $("#tbdyExtSeatMap_FROM");
				            	}
			            		
			            		 var t = $(".select-to-drag");
				            	 var tdrop = $( this );
				            	 if ( t.length > 1){
				            		 var tempArray = [];
				            		 tm.find(".seatDrop:empty").each(function(){
				            			 tempArray[tempArray.length] = $(this).parent().parent().parent().parent().parent()[0];
				            		 });
				            		t.each(function(index){
				            			$(this).css({"top":0, "left":0});
				            			var sortting = function(a,b){return parseInt(b.id,10) - parseInt(a.id,10);}
				            			tempArray.sort(sortting);
				            			var toadd = this;
				            			for (var add = 0; add<tempArray.length;add++){
				            				if ($(tempArray[add]).find(".seatDrop").children().length==0){
				            					$(tempArray[add]).find(".seatDrop").append(toadd);
				            					break;
				            				}
					            		}
				            			
						            });
				            	 }else{
				            		 t.css({"top":0, "left":0});
				            		 tdrop.append(t);
				            	 }
			            	}
			            	$( ".draggable" ).each(function(){
			            		if ($(this).hasClass("select-to-drag")){
			            			$(this).removeClass("select-to-drag");
			            		}
			            	});
			            	$(".droppable").removeClass("ui-state-hover");
			            }
			        });
				};
				
				
				finalisedLayout = function(){
					var the = null;
					var twe = null;
					var overflowY = "auto";
					var overflowX = "auto";
					$("#setTScroll").css({
						"width": $("#setTScroll").css("width"),
						"height": $("#setTScroll").css("height")
					});
					if ($.seatMap.mapHLayout){
						twe = parseInt($("#"+smContainer).parent().parent().css("width"), 10) - 10;
						the = parseInt($("#setTScroll").css("height"), 10) + 18;
						overflowY = "hidden";
						overflowX = "auto";
					}else{
						twe = parseInt($("#setTScroll").css("width"), 10) + 18;
						the = parseInt($("#"+smContainer).parent().parent().css("height"), 10) - 0;
						overflowY = "auto";
						overflowX = "hidden";
					}
					$("#setFScroll").css({
						"width": twe,
						"height": the,
						"overflow-x":overflowX,
						"overflow-y":overflowY
					});
					$("#setFScroll").scroll(function(){
						$(".extToolTip").each(function(){
							var p = $(this).parent().position();
							 $(this).css({
								"top":p.top,
								"left":p.left
							});
						});
					});
				};
				//Start building Seat map
				$("#"+smContainer).hide();
				init();
				processSeatamp();
				finalisedLayout();
				createExtSMLegend();
				//call back if available
				if( callBack ) callBack();
				$("#"+smContainer).show();
		}

	}
	// Shortuct functions
	BuildSeatMap = function(smCon, smDTO, extParam, callback) {
		$.seatMap.buildSeatmap(smCon, smDTO, extParam, callback);
	}
})(jQuery);