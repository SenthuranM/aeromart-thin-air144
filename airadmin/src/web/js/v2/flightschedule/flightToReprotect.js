/**
 * 
 */
function UI_flightToReprotect() {
}

$(document).ready(function() {
	UI_flightToReprotect.ready();
});

var objWindow;
var logicalCCCode = "";
var pageEdited = false;
UI_flightToReprotect.validateBoolean = false;
UI_flightToReprotect.isParentOpener = false;
UI_flightToReprotect.otherGridRowNumber = -1;
UI_flightToReprotect.strGridRow = -1;
UI_flightToReprotect.newSegment = '';
UI_flightToReprotect.oldSegment = '';
UI_flightToReprotect.popupmessage = "";
UI_flightToReprotect.segmentIdsOfReprotectFlights = new Array();
UI_flightToReprotect.checkSelectRow = true;
UI_flightToReprotect.gridSelectedRow = -1;
UI_flightToReprotect.gridSelectedRowOnPage = false;
UI_flightToReprotect.reprotectSuccess = false;
UI_flightToReprotect.updatedRPData;


UI_flightToReprotect.ready = function(){
	jQuery("#listFlightRP").jqGrid({ 
		url:'showFlightsToReprotect!searchRP.action',
		datatype: "json",
		postData: {
			txtDepartureDateFrom: function() { return $("#txtDepartureDateFrom").val(); },
			txtDepartureDateTo: function() { return $("#txtDepartureDateTo").val(); },
			txtFlightNumber: function() { return $("#txtFlightNumber").val(); },
			selDeparture: function() { return $("#selDeparture").val(); },
			selArrival: function() { return $("#selArrival").val(); },
			selVia1: function() { return $("#selVia1").val(); },
			selVia2: function() { return $("#selVia2").val(); },
			selVia3: function() { return $("#selVia3").val(); },
			selVia4: function() { return $("#selVia4").val(); },
			hdnMode: function() { return "SEARCH" },
			hdnFlightId: function() { return $("#hdnFlightId").val(); }
//			hdnFlightId: function() { return parent.arrBaseArray[0][6] }hdnFlightId
		},
		jsonReader : {
			  root: "rows", 
			  page: "page",
			  total: "total",
			  records: "records",
			  repeatitems: false,					 
			  id: "0"				  
			},													
			colNames:['&nbsp;','Flight','Departure', 'Departure Date/Time','Arrival','Arrival Date/Time','Route','Sold &#13;&#10; Adult/Infant &#13;&#10; (F/J/Y)','Onhold &#13;&#10; Adult/Infant &#13;&#10; (F/J/Y)','Available &#13;&#10; Adult/Infant &#13;&#10; (F/J/Y)','FlightId','ScheduleId','ModelNumber','SegmentMap','LogicalLcc','SegIds','LogicalCabinClass','Segments','SegmentsID','DepDate','Connection','CabinClassDes','IBE Re-protect'],  
			colModel:[ 	{name:'Id', width:50, jsonmap:'id'},   
			        	{name:'flightNumber',index:'flightNumber', width:75, jsonmap:'flightNumber'}, 
						{name:'departure' ,index:'departure' , width:120, align:"center", jsonmap:'depature'},
					   	{name:'departureDateTime',index:'departureDateTime' , width:200, align:"center", jsonmap:'depatureDateTime'},
					   	{name:'arrival',index:'arrival' ,width:100,  align:"center", jsonmap:'arrival'},
					   	{name:'arrivalDateTime',index:'arrivalDateTime' , width:200, align:"center", jsonmap:'arrivalDateTime'},
					   	{name:'route',index:'route' , width:200, align:"center", jsonmap:'route'},
					   	{name:'segmentSold',index:'segmentSold' , width:120, align:"center", jsonmap:'segmentSold'},
					   	{name:'segmentOnHold', index:'segmentOnHold' , width:120, align:"center", jsonmap:'segmentOnHold'},	
					   	{name:'segmentAvailable', index:'segmentAvailable' , width:140, align:"center", jsonmap:'segmentAvailable'},	
					   	{name:'flightId', index:'flightId', width:100,hidden:true, align:"center", jsonmap:'flightId'},
						{name:'scheduleId', index:'scheduleId', width:100,hidden:true, align:"center", jsonmap:'scheduleId'},
						{name:'modelNumber', index:'modelNumber', width:100,hidden:true, align:"center", jsonmap:'modelNumber'},
						{name:'segmentMap', index:'segmentMap', width:225,hidden:true, align:"center", jsonmap:'segmentMap'},
						{name:'logicalLcc', index:'logicalLcc', width:150,hidden:true, align:"center", jsonmap:'logicalLcc'},
						{name:'segIds', index:'segIds', width:100, hidden:true, align:"center", jsonmap:'segIds'},
						{name:'logicalCabinClass', index:'logicalCabinClass', width:100, hidden:true, align:"center", jsonmap:'logicalCabinClass'},
						{name:'segments', index:'segments', width:225, hidden:true, align:"center", jsonmap:'segments'},
						{name:'segmentsID', index:'segmentsID', width:100, hidden:true, align:"center", jsonmap:'segmentsID'},
						{name:'depDate', index:'depDate', width:150, hidden:true, align:"center", jsonmap:'depDate'},
						{name:'connection', index:'connection', width:225, hidden:true, align:"center", jsonmap:'connection'},
						{name:'cabinClassDes', index:'cabinClassDes', width:100, hidden:true, align:"center", jsonmap:'cabinClassDes'},
                        {name: 'flightRPCheck', index: 'flightRPCheck', width: 60, align: "center", sortable: false, hidden : isSelfReprotectionAllowed == 'false'}
					  ], imgpath: '../../themes/default/images/jquery', multiselect:false,
		pager: jQuery('#FlightRPpager'),
		rowNum:20,						
		viewrecords: true,
		height:180,
		width:950,
		loadui:'block',
		onCellSelect : function(rowid, iCol, cellcontent) {
			if(iCol == 22){
				var id = "flightRPCheckBox"+rowid;
				var checkBoxSel = document.getElementById(id);
				var flightSecmentId = jQuery("#listFlightRP").getCell(rowid,'segmentsID');
				var res = flightSecmentId.split(",");
				if(res.length > 1){
					flightSecmentId = res[res.length - 1];
				}
				if(checkBoxSel != null && checkBoxSel.checked == true){
					UI_flightToReprotect.segmentIdsOfReprotectFlights.push(flightSecmentId);
				}else{
					if(UI_flightToReprotect.segmentIdsOfReprotectFlights.length > 0){
					 var index = UI_flightToReprotect.segmentIdsOfReprotectFlights.indexOf(flightSecmentId);
					 if (index > -1) {
						 UI_flightToReprotect.segmentIdsOfReprotectFlights.splice(index, 1);
					 }
					}
				}
				UI_flightToReprotect.setSelectedRowIdForSearchGrid();
			}else{
				
				UI_flightToReprotect.RowClick(rowid);
				if(UI_flightToReprotect.gridSelectedRow > -1){
					jQuery("#listFlightRP").setSelection(UI_flightToReprotect.gridSelectedRow , true);
				} else {
					jQuery("#listFlightRP").resetSelection();
				}
				
			}
		},
		onPaging: function (pgButton) {
//			if ((!isPageEdited()) || (isPageEdited() && isConfirmed())) {
			if (!isPageEdited() || isPageEdited()) {
				HidePageMessage();
				if (!UI_flightToReprotect.validateSearchFlightReprotect()) {
					UI_flightToReprotect.validateBoolean = false;
					return 'stop';
				}
				UI_flightToReprotect.validateBoolean = true;

			}
		},
		beforeRequest: function() {
			     if(!UI_flightToReprotect.validateBoolean){
			    	 return 'false';
			     }
		},
		loadComplete: function(data , statusText){
			responseTextObj = eval("("+data.responseText+")");
			UI_flightToReprotect.selectCheckBoxesForRP();
			var rows20 = responseTextObj['rows'];
			if(responseTextObj['rows'] == null){
//				alert("is null:" + responseTextObj['rows']);
				$("#listFlightRP").clearGridData();
			} else if(rows20['0'] == "20"){
				$("#listFlightRP").clearGridData();
			}
		},
		gridComplete: function(){ 
	        var ids = jQuery("#listFlightRP").getDataIDs(); 
	        for(var i=0;i<ids.length;i++){ 
	            var cl = ids[i]; 
	            CheckBox = "<input type="+'"checkbox"'+ "value="+'"'+cl+'"'+"id="+'"flightRPCheckBox'+cl+'"'+" >";
	            jQuery("#listFlightRP").setRowData(ids[i],{flightRPCheck:CheckBox}) 
	        } 
	        UI_flightToReprotect.setSelectedRowIdForSearchGrid();
	    }
		  }).navGrid("#FlightRPpager",{refresh: true, edit: false, add: false, del: false, search: false});
	
	
	$("#listFlightRPTO").jqGrid({
		datatype : "local",
		jsonReader : {
			root : "rows",
			page : "page",
			total : "total",
			records : "records",
			repeatitems : false,
			id : "0"
		},
		colNames : ['&nbsp;','Segment','Sold Adult/Infant', 'On Hold Adult/Infant', 'Available Adult/Infant', 'SegementID'],
		colModel : [	{name:'Id', index:'Id', width:20},
		            	{name:'segmentTO',  index:'segmentTO', width:100}, 
		            	{name : 'segmentSoldTO',index : 'segmentSoldTO',width : 75},
		            	{name : 'segmentOnHoldTO',index : 'segmentOnHoldTO',width : 75},
		            	{name : 'segmentAvailableTO',index : 'segmentAvailableTO',width : 75},
		            	{name : 'segmentIdTO',index : 'segmentIdTO',width : 75, hidden:true}
			       ],imgpath : '../../themes/default/images/jquery',multiselect : false,
			rowNum : 6,
			viewrecords : true,
			height : 150,
			width : 450,
			loadui : 'block',
			onSelectRow: function(rowid){
				UI_flightToReprotect.otherGridRowNumber =  rowid;
			}
		});
	UI_flightToReprotect.flightRPFROM = $("#listFlightRPFROM").jqGrid({
		datatype : "local",
		jsonReader : {
			root : "rows",
			page : "page",
			total : "total",
			records : "records",
			repeatitems : false,
			id : "0"
		},
		colNames : ['&nbsp;','Segment','Sold Adult/Infant', 'On Hold Adult/Infant', 'Transfer / All', 'Reprotect Adult' , 'PNR' , 'FlightID' , 'FlightSegmentID'],
		colModel : [	{name:'Id', index:'Id', width:20},
		            	{name:'SegmentFROM',  index:'SegmentFROM', width:125}, 
		            	{name : 'segmentSoldFROM',index : 'segmentSoldFROM',width : 115, align:"center"},
		            	{name : 'segmentOnHoldFROM',index : 'segmentOnHoldFROM',width : 115, align:"center"},
		            	{name : 'transferAll',index : 'transferAll',width : 80, align:"center"},
		            	{name : 'reprotectAdultFROM',index : 'reprotectAdultFROM',width : 90, align:"center"},
		            	{name : 'pnrFROM',index : 'pnrFROM',width : 50 , align:"center"},
		            	{name : 'fligthIdFROM',index : 'fligthIdFROM',width : 100, hidden:true},
		            	{name : 'flightSegemntIdFROM',index : 'flightSegemntIdFROM',width : 100, hidden:true}
			       ],imgpath : '../../themes/default/images/jquery',multiselect : true,
			rowNum : 6,
			viewrecords : true,
			height : 150,
			width : 450,
			loadui : 'block',
			onCellSelect : function(rowid, iCol, cellcontent) {
				if(iCol == 5){
					setPageEdited(true);
					var txtReprotectTemp = "#txtReprotect"+ rowid;
					var flightRPFROMCheckTemp = "#flightRPFROMCheck" + rowid;
					var flightRPFROMCheckAll = $(flightRPFROMCheckTemp).is(":checked");
					if (flightRPFROMCheckAll != false) {
						UI_flightToReprotect.addTextBox(rowid);
						$(txtReprotectTemp).prop('disabled', true);

					} else {
                        if(document.getElementById('hiddenPrivilege') && document.getElementById('hiddenPrivilege').value == "1") {
                            $(txtReprotectTemp).prop('disabled', false);
                        }
						var txtReprotectTemp = "#txtReprotect"+ rowid;
				    	$(txtReprotectTemp).val("");
					}	
					multiSelectOnFROM(rowid);
					
				} else if (iCol == 6 || iCol == 7){
					multiSelectOnFROM(rowid);
				}
		  }
		});
	
	UI_flightToReprotect.addTextBox = function (strRowNo) {
		var fromSold = jQuery("#listFlightRPFROM").getCell(strRowNo,'segmentSoldFROM');
		var fromOnHold = jQuery("#listFlightRPFROM").getCell(strRowNo,'segmentOnHoldFROM');

		fromSold = fromSold.split("/");
		fromOnHold = fromOnHold.split("/");

		var totAdult = parseInt(fromSold[0]) + parseInt(fromOnHold[0]);
		var totInfant = parseInt(fromSold[1]) + parseInt(fromOnHold[1]);

		var txtReprotectTemp = "#txtReprotect"+ strRowNo;
    	$(txtReprotectTemp).val(totAdult);
	}
	$("#txtDepartureDateTo, #txtDepartureDateFrom").datepicker(
			{
				showOn : "button",
				buttonImage : "../../images/calendar_no_cache.gif",
				buttonImageOnly : true,
				dateFormat : 'dd/mm/yy',
				minDate:systemDate
			});
	
	$('#btnSearch').click(function() {
		
		if ((!isPageEdited()) || (isPageEdited() && isConfirmed())) {
			HidePageMessage();
			if (UI_flightToReprotect.validateSearchFlightReprotect()) {
				UI_flightToReprotect.validateBoolean = true;
				$("#listFlightRP").clearGridData();
				UI_flightToReprotect.resetFROMInSearch();
				UI_flightToReprotect.gridSelectedRow = -1;
				UI_flightToReprotect.gridSelectedRowOnPage = false;
				UI_flightToReprotect.segmentIdsOfReprotectFlights = [];
				$("#listFlightRP").trigger("reloadGrid");

			}else{
				UI_flightToReprotect.validateBoolean = false;
			}
		}
	});
	$("#txtDepartureDateFrom").blur(function() { UI_flightToReprotect.dateOnBlur({id:0});});
	$("#txtDepartureDateTo").blur(function() { UI_flightToReprotect.dateOnBlur({id:1});});

}

/** ************************************************************************* */
/** ************ F L I G H T S R E - P R O T E C T S E A R C H *********** */
/** ************************************************************************* */
UI_flightToReprotect.searchFlightsToReprotect = function() {
	if ((!isPageEdited()) || (isPageEdited() && isConfirmed())) {
		HidePageMessage();
		if (!validateSearchFlightReprotect()) {
			return;
		}
		setField("hdnFlightId", parent.arrBaseArray[0][6]);
		var strActionUrl = "showFlightsToReprotect!newExecute.action";

		document.forms[0].action = strActionUrl;
		document.forms[0].target = "_self";
		document.forms[0].hdnMode.value = "SEARCH";
		document.forms[0].submit();
	}
}

/** *************************************************************************** */
/** **** V A L I D A T E F L I G H T S R E - P R O T E C T S E A R C H ***** */
/** *************************************************************************** */
UI_flightToReprotect.validateSearchFlightReprotect = function() {
	var strToDate = document.getElementById("txtDepartureDateTo").value;
	var strFromDate = document.getElementById("txtDepartureDateFrom").value;
	var isToDateLessthanFromDate = CheckDates(strToDate, strFromDate);

	if (validateWindowTextField(strFromDate, departureDateFromRqrd)) {
		getFieldByID("txtDepartureDateFrom").focus();
		return;
	}

	if ((allowReprotectPAX != "Y") && (systemDate != "null")
			&& (CheckDates(strFromDate, systemDate))) {
		showWindowCommonError("Error", departureDateFromBnsRleVltd);
		document.getElementById("txtDepartureDateFrom").focus();
		$(window.self).scrollTop(0);
		return;
	}

	if (validateWindowTextField(strToDate, departureDateToRqrd)) {
		getFieldByID("txtDepartureDateTo").focus();
		return;
	}

	if (strFromDate != strToDate) {
		if (isToDateLessthanFromDate) {
			showWindowCommonError("Error", departureDateToBnsRleVltd);
			document.getElementById("txtDepartureDateFrom").focus();
			$(window.self).scrollTop(0);
			return;
		}
	}

	if (document.getElementById("selDeparture").value != "-1"
			&& document.getElementById("selArrival").value != "-1") {
		if (document.getElementById("selDeparture").value == document
				.getElementById("selArrival").value) {
			showWindowCommonError("Error", departureAndArrivalBnsRleVltd);
			document.getElementById("selArrival").focus();
			$(window.self).scrollTop(0);
			return;
		}
	}

	var strDeparture = document.getElementById("selDeparture").value;
	var strArrival = document.getElementById("selArrival").value;

	var strVia1 = document.getElementById("selVia1").value;
	var strVia2 = document.getElementById("selVia2").value;
	var strVia3 = document.getElementById("selVia3").value;
	var strVia4 = document.getElementById("selVia4").value;

	var arrSegments = new Array();
	arrSegments[0] = strDeparture;
	arrSegments[1] = strArrival;

	arrSegments[2] = strVia1;
	arrSegments[3] = strVia2;
	arrSegments[4] = strVia3;
	arrSegments[5] = strVia4;

	var blnDuplicate = false;
	for ( var i = 0; i < arrSegments.length; i++) {
		if (arrSegments[i] != "-1") {
			for ( var m = i; m < arrSegments.length; m++) {
				if (arrSegments[m] != "-1") {
					if (arrSegments[m] == arrSegments[i]) {
						if (m != i) {
							blnDuplicate = true;
							break;
						}
					}
				}
			}
		}
		if (blnDuplicate) {
			break;
		}
	}

	if (blnDuplicate) {
		showWindowCommonError("Error", reprotectViaPointsInvalid);
		$(window.self).scrollTop(0);
		return;
	}
	return true;
}

var options = {
		cache: false,	
		beforeSubmit:  showRequest,  // pre-submit callback - this can be used to do the validation part 
		success: showResponse,   // post-submit callback
		error: showError,
		dataType:  'json'        // 'xml', 'script', or 'json' (expected server response type)  
	}; 

function showRequest(formData, jqForm, options) { 
	ShowPopProgress();
    return true; 
}

function showResponse(responseText, statusText)  {
	HidePopProgress();
	if(responseText['responseRP'] == "success"){
		UI_flightToReprotect.clearTO();
		UI_flightToReprotect.clearFROM();
//		parent.arrBaseArray = "";
		UI_flightToReprotect.updatedRPData = responseText['updatedRPData'];
		parent.arrBaseArray = UI_flightToReprotect.updatedRPData['baseArray'];
		UI_flightToReprotect.popupmessage = responseText['popupmessage'];
		var reqMessage = responseText['reqMessage'];
		var reqMsgType = responseText['reqMsgType'];
		UI_flightToReprotect.winOnLoad(reqMessage , reqMsgType);
		UI_flightToReprotect.segmentIdsOfReprotectFlights = [];
//		UI_flightToReprotect.reprotectSuccess = true;
		$("#listFlightRP").trigger("reloadGrid");
	} else {
		showWindowCommonError("Error", "REPROTECTION FAILED CONTACT SYSTEM ADMIN");
		$(window.self).scrollTop(0);
	}
	UI_flightToReprotect.resetPageStatus();
	
} 

UI_flightToReprotect.resetPageStatus = function() {
	setPageEdited(false);
}

function showError(data)  {
	showWindowCommonError("Error", "REPROTECTION FAILED CONTACT SYSTEM ADMIN");
	$(window.self).scrollTop(0);

}

/** ************************************************************************* */
/** ********* R O W C L I C K E V E N T O F M A I N G R I D ********** */
/** ************************************************************************* */
UI_flightToReprotect.RowClick = function (strRowNo) {
	if ((!isPageEdited()) || (isPageEdited() && isConfirmed())) {
		HidePageMessage();
		UI_flightToReprotect.initValues();
		UI_flightToReprotect.disableConfirmButtons(true);
		// Code to reset the Reprotect - From grid
		UI_flightToReprotect.resetChangeLogicalCC(parent.arrBaseArray,
				getValue("selLogicalCC"));
		Disable("selLogicalCC", false);
		setVisible("spnReprotectTo", false);
		setVisible("selfReprotectRow", false);
		
		var originClassFound = false;
		var sameFlight = false;
		var logicalCC = getValue("selLogicalCC");

		UI_flightToReprotect.newSegment = jQuery("#listFlightRP").getCell(strRowNo,'segmentMap').replace(",", "");
		var arrRowLogicalCC = jQuery("#listFlightRP").getCell(strRowNo,'logicalLcc');
		
		var arrLogicalCCes = arrRowLogicalCC.split(":");

		logicalCCCode = "";
		var logicalCCDesc = "";
		// Check if logicalCC in FROM found in TO
		for ( var logicalCCCount = 0; logicalCCCount < arrLogicalCCes.length; logicalCCCount++) {
			logicalCCCode = arrLogicalCCes[logicalCCCount];
			if (logicalCC == logicalCCCode) {
				originClassFound = true;
				break;
			}
		}
		flightSegID = jQuery("#listFlightRP").getCell(strRowNo,'segmentsID');
        flightSegID = flightSegID.replace('<br>','');
		var flightID = jQuery("#listFlightRP").getCell(strRowNo,'flightId');
		if (flightID == parent.arrBaseArray[0][6]) {
			sameFlight = true;
		}
		if ((!originClassFound) || (sameFlight)) {
			if (sameFlight) {
				showWindowCommonError("Error", sameFlightSelected);
				$(window.self).scrollTop(0);
			} else if (!originClassFound) {
				showWindowCommonError("Error", classOfServiceNotAvailable);
				$(window.self).scrollTop(0);
			}
			UI_flightToReprotect.gridSelectedRow = -1;
			return false;
		} else {
			// Enabling the FROM Grid
			var logicalCCLength = parent.arrBaseArray.length;
			if (logicalCCLength > 0) {
				Disable("selLogicalCC", false);
			} else {
				Disable("selLogicalCC", true);
			}
			Disable("btnMove", false);

			arrCabinClassesDesTemp =  jQuery("#listFlightRP").getCell(strRowNo,'cabinClassDes');
			var arrCabinClassesDes =  arrCabinClassesDesTemp.split("<br>");

			var strLogicalClassOfServiceHtmlOther = "";
			var selectOption = "";
			strLogicalClassOfServiceHtmlOther = '<select id="selLogicalCCOther" name="selLogicalCCOther" size="1" style="width:100px" onChange="UI_flightToReprotect.changeLogicalClassOfServiceOther(this.value , UI_flightToReprotect.strGridRow)">';
			var logicalCCLength = parent.arrBaseArray.length;
			for ( var logicalCCCount = 0; logicalCCCount < logicalCCLength; logicalCCCount++) {
				logicalCCCode = parent.arrBaseArray[logicalCCCount][0];
				logicalCCDesc = parent.arrBaseArray[logicalCCCount][1];
				if (logicalCCCode == logicalCC) {
					selectOption = "selected";
				} else {
					selectOption = "";
				}
				strLogicalClassOfServiceHtmlOther += '<option value="' + logicalCCCode + '" '
						+ selectOption + '>' + logicalCCDesc + "</option>";
			}
			strLogicalClassOfServiceHtmlOther += '</select>';

			// setting the values of Re-protect From labels
			flightNumber =  jQuery("#listFlightRP").getCell(strRowNo,'flightNumber');
			depature =  jQuery("#listFlightRP").getCell(strRowNo,'departure');
			arrival =  jQuery("#listFlightRP").getCell(strRowNo,'arrival');
			modelNumber =  jQuery("#listFlightRP").getCell(strRowNo,'modelNumber');
			depDate =  jQuery("#listFlightRP").getCell(strRowNo,'depDate');
			
			$("#spnFlightNoOther").text(flightNumber);
			$("#spnDepartureOther").text(depature);
			$("#spnArrivalOther").text(arrival);
			$("#spnCOSOther").html(strLogicalClassOfServiceHtmlOther);
			
			//Allow to change the cabin class while reprotecting.
			//Disable("selLogicalCCOther", true);
			/*
			 * if (arrRow[6] == null || arrRow[6] == "") {
			 * setVisible("spnViewViaOther", false);
			 * document.getElementById('spnViaOther').innerHTML = ""; } else {
			 * setVisible("spnViewViaOther", true);
			 * document.getElementById('spnViaOther').innerHTML = arrRow[6]; }
			 */
			$("#spnModelCap").text(modelNumber);
			$("#spnDepDateOther").text(depDate);
			Disable("btnReset", true);
			UI_flightToReprotect.changeLogicalClassOfServiceOther( logicalCC, strRowNo);
			setVisible("spnReprotectTo", true);
			setIBEselfReprotectionCheck();
			UI_flightToReprotect.strGridRow = strRowNo;
			UI_flightToReprotect.gridSelectedRow = strRowNo;
			if(agentReprotectEmailEnabled){
				setVisible("agentEmailRow", true);
			}else{
				setVisible("agentEmailRow", false);
			}
			UI_flightToReprotect.disableFROMGridInputs(false);
		}
	}
}

UI_flightToReprotect.initValues = function() {
	setField("hdnLogicalCabinClassCode", "");
	setField("hdnTargetFlightId", "");
	setField("hdnTargetSegmentCode", "");
	setField("hdnTargetFlightSegmentId", "");
	setField("hdnSourceFlightDistributions", "");
	setField("hdnSelectedPNR", "");
	setField("hdnSelectedSoldOhdCount", "");
	setField("hdnSelectedSoldOhdInfantCount", "");
}

UI_flightToReprotect.disableConfirmButtons = function (val) {
	Disable("btnConfirm", val);
	Disable("btnCRF", val);
}

/** *********************************************************************** */
/** ********** R E S E T R E L A T E D C L I C K E V E N T ************* */
/** *********************************************************************** */
// A function to reset the reprotect-from grid, when reset button was pressed
UI_flightToReprotect.resetChangeLogicalCC = function (arrayTemp, selCCCode) {
	var logicalCCLength = arrayTemp.length;
	var arrReprotectSeg = new Array();
	var rowNum = 0;
	$("#listFlightRPFROM").clearGridData();
	for ( var logicalCCCount = 0; logicalCCCount < logicalCCLength; logicalCCCount++) {
		if (selCCCode == arrayTemp[logicalCCCount][0]) {
			arrReprotectSeg[rowNum] = new Array();
			var row = arrayTemp[logicalCCCount][5];
			row[0][0] = rowNum;
			arrReprotectSeg[rowNum] = row[0];
			if(document.getElementById('hiddenPrivilege') && document.getElementById('hiddenPrivilege').value == "1") {
                CheckBox = "<input name=" + '"checkboxFROM"' + " type=" + '"checkbox"' + "value=" + '"' + rowNum + '"' + "id=" + '"flightRPFROMCheck' + rowNum + '"' + " >";
                ReprotectAdultInput = "<input :CUSTOM:  type='text' id='txtReprotect"+rowNum+"' name='txtReprotect' onKeyUp='parent.validateTxtBox(this)' onKeyPress='parent.validateTxtBox(this)' maxlength='8' align='right' style='width:50px'>";
            } else {
                CheckBox = "<input name=" + '"checkboxFROM"' + " type=" + '"checkbox"' + "value=" + '"' + rowNum + '"' + "id=" + '"flightRPFROMCheck' + rowNum + '"' + " disabled >";
                ReprotectAdultInput = "<input :CUSTOM:  type='text' id='txtReprotect"+rowNum+"' name='txtReprotect' onKeyUp='parent.validateTxtBox(this)' onKeyPress='parent.validateTxtBox(this)' maxlength='8' align='right' style='width:50px' disabled >";
            }
			pnrButton = "<input :CUSTOM: type='button' id='btnPaxPNR' name='btnPaxPNR' align='center' style='width:30px' onClick='parent.UI_flightToReprotect.selectReprotectPNR("+ rowNum +")' value='PNR'>";
			
			var temp = {
					Id : rowNum + 1,
					SegmentFROM : arrReprotectSeg[rowNum][1],
					segmentSoldFROM : arrReprotectSeg[rowNum][2],
					segmentOnHoldFROM : arrReprotectSeg[rowNum][3],
					transferAll: CheckBox,
					reprotectAdultFROM: ReprotectAdultInput,
					pnrFROM: pnrButton,
					fligthIdFROM: arrReprotectSeg[rowNum][6],
					flightSegemntIdFROM: arrReprotectSeg[rowNum][7]
				};
			$("#listFlightRPFROM").addRowData( rowNum , temp); 
			rowNum++;
		}
	}
	UI_flightToReprotect.disableFROMGridInputs(true);
}

/** ************************************************************************************ */
/** CHANGE THE REPROTECT_TO GRID BASED ON REPROTECT_FROM GRID LOGICAL CLASS OF SERVICE * */
/** ************************************************************************************ */
UI_flightToReprotect.changeLogicalClassOfServiceOther = function( selCCCode ,strRowNo) {
		setPageEdited(true);
		setField("selLogicalCCOther", selCCCode);
		$("#listFlightRPTO").clearGridData();
		arrSoldsTemp =  jQuery("#listFlightRP").getCell(strRowNo,'segmentSold');
		arrOnHoldTemp =  jQuery("#listFlightRP").getCell(strRowNo,'segmentOnHold');
		arrAvailableTemp =  jQuery("#listFlightRP").getCell(strRowNo,'segmentAvailable');
		arrLogicalCCesTemp =  jQuery("#listFlightRP").getCell(strRowNo,'logicalCabinClass');
		arrSegmentsTemp =  jQuery("#listFlightRP").getCell(strRowNo,'segments');
		arrSegIDsTemp =  jQuery("#listFlightRP").getCell(strRowNo,'segmentsID');
		connection =  jQuery("#listFlightRP").getCell(strRowNo,'connection');

		var arrSolds = arrSoldsTemp.split("<br>");
		var arrOnHold = arrOnHoldTemp.split("<br>");
		var arrAvailable = arrAvailableTemp.split("<br>");
		var arrLogicalCCes = arrLogicalCCesTemp.split("<br>");
		var arrSegments = arrSegmentsTemp.split("<br>");
		var arrSegIDs = arrSegIDsTemp.split("<br>");

		var arrReprotectSeg = new Array();
		if(connection == 'connection'){
			var i = 0;
			for ( var j = 0; j < arrLogicalCCes.length; j++) {
				var logicalCCes = arrLogicalCCes[j].split(",");
				for ( var k = 0; k < logicalCCes.length; k++) {
					if (logicalCCes[k] == selCCCode) {
						arrReprotectSeg[i] = new Array();
						arrReprotectSeg[i][0] = i;
						arrReprotectSeg[i][1] = arrSegments[j].split(",")[0]; 
						
						var segSold = '';
						for(var ind=0; ind < arrSolds.length; ind++){
							var segS = arrSolds[ind];
							if(segS.split("-")[0] == selCCCode){
								if(segSold != ''){
									segSold = segSold + "<br>" + segS.split("-")[1];
								}
								else{
									segSold = segS.split("-")[1];
								}
							}
						}
						arrReprotectSeg[i][2] = segSold;
										
						var segOnHold = '';
						for(var ind=0; ind < arrOnHold.length; ind++){
							var segOh = arrOnHold[ind];
							if(segOh.split("-")[0] == selCCCode){
								if(segOnHold != ''){
									segOnHold = segOnHold + "<br>" + segOh.split("-")[1];
								}
								else{
									segOnHold = segOh.split("-")[1];
								}
							}
						}
						arrReprotectSeg[i][3] = segOnHold;
						
						var segAvailability = '';
						for(var ind=0; ind < arrAvailable.length; ind++){
							var segAvail = arrAvailable[ind];
							if(segAvail.split("-")[0] == selCCCode){
								if(segAvailability != ''){
									segAvailability = segAvailability + "<br>" + segAvail.split("-")[1];
								}
								else{
									segAvailability = segAvail.split("-")[1];
								}
							}
						}													
						arrReprotectSeg[i][4] = segAvailability; 
																				// adult/infant
						arrReprotectSeg[i][5] = arrSegIDs[j].split(",")[0]; // segment
																			// id
						
						var temp = {
								Id : i + 1,
								segmentTO : arrReprotectSeg[i][1],
								segmentSoldTO : arrReprotectSeg[i][2],
								segmentOnHoldTO : arrReprotectSeg[i][3],
								segmentAvailableTO : arrReprotectSeg[i][4],
								segmentIdTO : arrReprotectSeg[i][5]
							};
						$("#listFlightRPTO").addRowData( arrReprotectSeg[i][0] , temp);  
						i++;
					}
				}
			}
		} else {
			var i = 0;
			for ( var j = 0; j < arrLogicalCCes.length; j++) {
				var logicalCCes = arrLogicalCCes[j].split(",");
				for ( var k = 0; k < logicalCCes.length; k++) {
					if (logicalCCes[k] == selCCCode) {
						arrReprotectSeg[i] = new Array();
						arrReprotectSeg[i][0] = i;
						arrReprotectSeg[i][1] = arrSegments[j].split(",")[k]; // segment
																				// code
						arrReprotectSeg[i][2] = arrSolds[j].split(",")[k]; // sold
																			// adult/infant
						arrReprotectSeg[i][3] = arrOnHold[j].split(",")[k]; // on
																			// hold
																			// adult/infant
						arrReprotectSeg[i][4] = arrAvailable[j].split(",")[k]; // available
																				// adult/infant
						arrReprotectSeg[i][5] = arrSegIDs[j].split(",")[k]; // segment
																			// id

						var temp = {
									Id : i + 1,
									segmentTO : arrReprotectSeg[i][1],
									segmentSoldTO : arrReprotectSeg[i][2],
									segmentOnHoldTO : arrReprotectSeg[i][3],
									segmentAvailableTO : arrReprotectSeg[i][4],
									segmentIdTO : arrReprotectSeg[i][5]
								};
						$("#listFlightRPTO").addRowData( arrReprotectSeg[i][0] , temp);  	

						i++;
					}
				}
			}
		}
		if(ckeckTOHasData()){
			UI_flightToReprotect.disableFROMGridInputs(false);
		} else {
			UI_flightToReprotect.disableFROMGridInputs(true);
		}
}

/** *************************************************************************** */
/** ****************** P A G E L O A D E V E N T ********************** */
/** *************************************************************************** */
UI_flightToReprotect.winOnLoad = function(strMsg, strMsgType) {
	if (typeof (opener.top[0].strReprotectFrom) == "string") {
		UI_flightToReprotect.isParentOpener = true; // Flight Screen Reprotect Button Click
	} else {
		UI_flightToReprotect.isParentOpener = false; // Intermediate Window
	}
	closeChildWindow();
	getFieldByID("txtDepartureDateFrom").focus();
	
	// preparing the COS combo list
	var logicalCCLength = parent.arrBaseArray.length;
	
	if (logicalCCLength == 0) {
		Disable("btnConfirm", true);
		Disable("btnPaxDetals", true);
	} else {

		// setting the values of Re-protect From labels
		$("#spnFlightNo").text(parent.arrBaseArray[0][2]);
		$("#spnDeparture").text(parent.arrBaseArray[0][4]);
		$("#spnArrival").text(parent.arrBaseArray[0][3]);
		$("#spnDepDate").text(parent.arrBaseArray[0][7]);
		
		var logicalCCDesc = "";
		var strLogicalClassOfServiceHtml = '<select id="selLogicalCC" ' + 'name="selLogicalCC" size="1" style="width:100px" ' + 'onChange="UI_flightToReprotect.changeLogicalCC(this.value)">';
		var isSelected = "";
		for ( var logicalCCCount = 0; logicalCCCount < logicalCCLength; logicalCCCount++) {
			if (logicalCCCode == "" || logicalCCCode != parent.arrBaseArray[logicalCCCount][0]) {
				logicalCCCode = parent.arrBaseArray[logicalCCCount][0];
				logicalCCDesc = parent.arrBaseArray[logicalCCCount][1];
				if (logicalCCCode == "Y") {
					isSelected = "selected";
				}
				strLogicalClassOfServiceHtml += '<option value="' + logicalCCCode + '" ' + isSelected
						+ '>' + logicalCCDesc + '</option>';
			}
		}
		strLogicalClassOfServiceHtml += '</select>';
	
		$("#spnCOSFrom").html(strLogicalClassOfServiceHtml);
		if (logicalCCLength == 1) {
			Disable("selLogicalCC", true);
		} else {
			Disable("selLogicalCC", false);
		}
	
		// setting the from data grid
		UI_flightToReprotect.resetChangeLogicalCC(parent.arrBaseArray,
				getValue("selLogicalCC"));
		setField("hdnFlightId", parent.arrBaseArray[0][6]);
	
		if ((strMsg != null) && (strMsgType != null)) {
			showWindowCommonError(strMsgType, strMsg);
			$(window.self).scrollTop(0);
		}
		
		if (autoDateFillEnabled == "true") {
			var currentDate = new Date();
			var yyyy = currentDate.getFullYear().toString();
			var mm = (currentDate.getMonth()+1).toString();
			var dd  = currentDate.getDate().toString();
			var dateAsString = (dd[1]?dd:"0"+1) +"/"+ (mm[1]?mm:"0"+mm[0]) + "/" + yyyy;
			setFieldValues("txtDepartureDateFrom", dateAsString);
			setFieldValues("txtDepartureDateTo", dateAsString);
		}
	
		// After search button was clicked
		if (isPostBack) {
			if (departureDateFrom != null)
				setFieldValues("txtDepartureDateFrom", departureDateFrom);
			if (departureDateTo != null)
				setFieldValues("txtDepartureDateTo", departureDateTo);
			if (flightNumber != null)
				setFieldValues("txtFlightNumber", flightNumber);
			if (departure != null)
				setFieldValues("selDeparture", departure);
			if (arrival != null)
				setFieldValues("selArrival", arrival);
			if (via1 != null)
				setFieldValues("selVia1", via1);
			if (via2 != null)
				setFieldValues("selVia2", via2);
			if (via3 != null)
				setFieldValues("selVia3", via3);
			if (via4 != null)
				setFieldValues("selVia4", via4);
	
		}
		Disable("btnMove", true);
		Disable("btnConfirm", true);
		// Disable("btnCRF", true);
		Disable("chkGA", true);
		Disable("btnReset", true);
		var strPageButtonMode;
		if (UI_flightToReprotect.isParentOpener) {
			strPageButtonMode = opener.top[0].strReprotectFrom;
		} else {
			strPageButtonMode = opener.opener.top[0].strReprotectFrom;
		}
		// if coming thru flight cancel
		if (strPageButtonMode == "FLIGHT") {
			setVisible("spnViewRollForwardBtn", false);
		} else {
			setVisible("spnViewRollForwardBtn", true);
			Disable("btnCRF", true);
		}
	}

	// show popup UI_flightToReprotect.popupmessage
	if (trim(UI_flightToReprotect.popupmessage).length != 0) {
		alert(UI_flightToReprotect.popupmessage);
		if (!UI_flightToReprotect.isParentOpener) {
			opener.opener.reprotectRefresh();
		}
	}

	if (sendSMSForPax == "Y") {
		Disable("chkSendSMS", false);
		Disable("chkSmsCnf", true);
		Disable("chkSmsOnH", true);

	} else {
		Disable("chkSendSMS", true);
		Disable("chkSmsCnf", true);
		Disable("chkSmsOnH", true);
	}
	if (sendEmailForPax == "Y") {
		Disable("chkSendEmail", false);
		Disable("chkIbeTrasferFlights", false);

	} else {
		Disable("chkSendEmail", true);
		Disable("chkIbeTrasferFlights", true);
	}
}

/** *************************************************************************** */
/** ****************** PNR BUTTON ON CLICK ********************** */
/** *************************************************************************** */
UI_flightToReprotect.selectReprotectPNR = function(strRowNo) {
	var selectedRowNum = String(strRowNo);
	if(selectedRowNum!=null && selectedRowNum!=""){
		var intLeft = (window.screen.width - 1000) / 2;
		var intTop = (window.screen.height - 765) / 2;
		var winheight = 750;
		if (externalIntlFlightDetailCaptureEnabled == "true") {
			var winWidth = 1180;
		} else {
			var winWidth = 800;
		}
		var checkedVal = "#flightRPFROMCheck" + strRowNo;
		var isTransferAll = $(checkedVal).is(":checked");
		var strFlightId =  jQuery("#listFlightRPFROM").getCell(strRowNo,'flightSegemntIdFROM');
		var selectCabinClass = getValue("selLogicalCC");
		

		
		var transferAll = 'N';
		
		if(isTransferAll){
			transferAll = 'Y';
		}				

		var strActionUrl = window.document.location.protocol;
		strActionUrl += "//"+window.document.location.host;
		strActionUrl += "/airadmin/private/master/";
		strActionUrl += "showFile!selectReprotectPNR_v2.action";
		strActionUrl += "?hdnFlightSegId=" + strFlightId+'&hdnCabinClass='+selectCabinClass+'&hdnTransferAll='+transferAll+'&rowNo='+selectedRowNum+'&hdnNewFlightSegId='+flightSegID;

		var strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width='
				+ winWidth+ ',height='+ winheight+ ',resizable=no,top='+ intTop+ ',left=' + intLeft;
		opener.top[0].objRollForwardWindow = window.open(strActionUrl,"selectPnr", strProp);
	}			
}

/** *************************************************************** */
/** ****** R E - P R O T E C T P A X V A L I D A T I O N ******** */
/** *************************************************************** */
UI_flightToReprotect.reprotectPassenger = function() {
	HidePageMessage();
	var strLogicalCCCode = getValue("selLogicalCC"); 
	var strTargetCabinClassCode = getValue("selLogicalCCOther");
	var tempFromSelectedRows = "";
	var tempOtherSelectedRows = "";

	var finalOtherAvailableAdult = 0;
	var finalOtherAvailableInfant = 0;

	var availableAdult = 0;
	var availableInfant = 0;

	var finalSoldAdult = 0;
	var finalSoldInfant = 0;

	var finalOnHoldAdult = 0;
	var finalOnHoldInfant = 0;

    selRowIdFROM = $("#listFlightRPFROM").getGridParam('selarrrow');
    selRowIdTO = $("#listFlightRPTO").getGridParam('selrow');

	var strSourceFlightDistributions = "";

	var totalFromSoldAdult = 0;
	var totalFromSoldInfant = 0;

	var totalFromOnHoldAdult = 0;
	var totalFromOnHoldInfant = 0;

	if ((selRowIdFROM != null) && (selRowIdTO != null)) {
		// contains the first grid selected row numbers
		var fromSelectedArray = selRowIdFROM;
		var selectedPnrStr = getValue("hdnSelectedPNR");
		var selectedPnrSoldOhdCount = getValue("hdnSelectedSoldOhdCount");
		var selectedPnrInfantSoldOhdCount = getValue("hdnSelectedSoldOhdInfantCount");
		
		var fromGrandTotal = 0;
		for ( var count = 0; count < fromSelectedArray.length; count++) {
			var selectedRowNum = fromSelectedArray[count];
			
			var tmpSold =  jQuery("#listFlightRPFROM").getCell(selectedRowNum,'segmentSoldFROM');
			tmpSold = tmpSold.split("/");
			
			var tmpOnHold =  jQuery("#listFlightRPFROM").getCell(selectedRowNum,'segmentOnHoldFROM');
			tmpOnHold = tmpOnHold.split("/");

			var soldAdult = parseInt(tmpSold[0]);
			var soldInfant = parseInt(tmpSold[1]);

			var onHoldAdult = parseInt(tmpOnHold[0]);
			var onHoldInfant = parseInt(tmpOnHold[1]);

			var fromTotal = soldAdult + onHoldAdult;
			var txtReprotectTempStr = "#txtReprotect"+ selectedRowNum;
			var tmpReprodectAdultTxt = $(txtReprotectTempStr).val();

			if ((trim(tmpReprodectAdultTxt) == "")
					|| (parseInt(trim(tmpReprodectAdultTxt)) == 0) || isNaN(trim(tmpReprodectAdultTxt))) {
				showWindowCommonError("Error", adultValueEmpty);
				$(txtReprotectTempStr).focus();
				$(window.self).scrollTop(0);
				return false;
			}
			if (tmpReprodectAdultTxt > fromTotal) {
				showWindowCommonError("Error", reprotectAdultValueGter);
				$(txtReprotectTempStr).focus();
				$(window.self).scrollTop(0);
				return false;
			}
			
			
			//validate re-protect adult value and selected PNR total adult count are equal 
			var strSourceFlightSegmentId = jQuery("#listFlightRPFROM").getCell(selectedRowNum,'flightSegemntIdFROM');
			var flightRPFROMCheckTemp = "#flightRPFROMCheck" + selectedRowNum;
			var isTransferAll = $(flightRPFROMCheckTemp).is(":checked");
			
			var tmpReprodectAdultTxt = $(txtReprotectTempStr).val();
			var transferSeatsCount = parseInt(tmpReprodectAdultTxt);
			
			if(!isTransferAll && selectedPnrStr!=null && selectedPnrStr!="") {
				var fltSegArray = selectedPnrStr.split("@");		
				
				for(i=0;i<fltSegArray.length;i++){
					var fltSeg = fltSegArray[i].split(":");
					if(strSourceFlightSegmentId==fltSeg[0]){
						var pnrAdultCount = 0;
						var pnrInfo = fltSeg[1].split(",");
						for(j=0;j<pnrInfo.length;j++){
							var pnr = null;
							
							if(pnrInfo[j]!=null)
								pnr = pnrInfo[j].split("#");
							
							if(pnr!=null && pnr.length > 1)
								pnrAdultCount = pnrAdultCount + parseInt(pnr[1]);
							
						}
						
						if(transferSeatsCount != pnrAdultCount){
							showWindowCommonError("Error",pnrAdultValueNotEqual);
							$(txtReprotectTempStr).focus();
							$(window.self).scrollTop(0);
							return false;
						}
						break;
					}
				}
				
				
				
				
			}

		}

		var isTransferAll = false;
		for ( var count = 0; count < fromSelectedArray.length; count++) {
			isTransferAll = false;
			var selectedRowNum = fromSelectedArray[count];
			var tmpSold = jQuery("#listFlightRPFROM").getCell(selectedRowNum,'segmentSoldFROM');
			tmpSold = tmpSold.split("/");

			var tmpOnHold = jQuery("#listFlightRPFROM").getCell(selectedRowNum,'segmentOnHoldFROM');
			tmpOnHold = tmpOnHold.split("/");

			var flightRPFROMCheckTemp = "#flightRPFROMCheck" + selectedRowNum;
			var isTransferAll = $(flightRPFROMCheckTemp).is(":checked");

			var txtReprotectTempStr = "#txtReprotect"+ selectedRowNum;
			var tmpReprodectAdultTxt = $(txtReprotectTempStr).val();
			
			tmpReprodectAdultTxt = parseInt(tmpReprodectAdultTxt);

			var soldAdult = parseInt(tmpSold[0]);
			var soldInfant = parseInt(tmpSold[1]);
			var onHoldAdult = parseInt(tmpOnHold[0]);
			var onHoldInfant = parseInt(tmpOnHold[1]);

			// laterly Added
			var transferSeatsCount = tmpReprodectAdultTxt;
			var strSourceFlightId = jQuery("#listFlightRPFROM").getCell(selectedRowNum,'fligthIdFROM');
			var strSourceSegmentCode = jQuery("#listFlightRPFROM").getCell(selectedRowNum,'SegmentFROM');
			UI_flightToReprotect.oldSegment = strSourceSegmentCode;
			var strSourceFlightSegmentId = jQuery("#listFlightRPFROM").getCell(selectedRowNum,'flightSegemntIdFROM');
			// var infantCount = 0;
			// creating the first row by adding the variables
			// and separates the individual variables by ','
			var strSourceFlightDistribution = strSourceFlightId + ",";
			strSourceFlightDistribution += strSourceSegmentCode + ",";
			strSourceFlightDistribution += strSourceFlightSegmentId + ",";
			strSourceFlightDistribution += transferSeatsCount + ",";
			strSourceFlightDistribution += isTransferAll;

			// creating the selected rows by adding the individual row and
			// separates by adding ':'
			// after every row
			strSourceFlightDistributions += strSourceFlightDistribution;
			if ((fromSelectedArray.length - 1) != count) {
				strSourceFlightDistributions += ":";
			}
			// end of new code

			var checkInfant = false;
			var totalCount = soldAdult + onHoldAdult;
			if (totalCount == transferSeatsCount) {
				checkInfant = true;
			}			
			
			if(!isTransferAll && selectedPnrStr!=null && selectedPnrStr!="" && selectedPnrSoldOhdCount != null && selectedPnrSoldOhdCount !="") {				
				var soldOhdCount = selectedPnrSoldOhdCount.split("#");	
				var soldOhdInfantCount = selectedPnrInfantSoldOhdCount.split("#");
				try {
					var pnrSoldAdult = parseInt(String(soldOhdCount[0]));
					var pnrOhdAdult = parseInt(String(soldOhdCount[1]));
					var pnrSoldInfant = parseInt(String(soldOhdInfantCount[0]));
					var pnrOhdInfant = parseInt(String(soldOhdInfantCount[1]));
					if(pnrSoldAdult > 0){
						totalFromSoldAdult = pnrSoldAdult;
						totalFromOnHoldAdult = (transferSeatsCount - pnrSoldAdult);
						totalFromSoldInfant += pnrSoldInfant;
						totalFromOnHoldInfant += pnrOhdInfant;
					} else if(pnrOhdAdult > 0){
						totalFromOnHoldAdult = pnrOhdAdult;
						totalFromSoldAdult = (transferSeatsCount - pnrOhdAdult);
					} else {
						var availSoldSeat = (transferSeatsCount - soldAdult);
						if (parseInt(availSoldSeat) >= 0) {
							totalFromSoldAdult += soldAdult;
							totalFromOnHoldAdult += availSoldSeat;
						} else {
							totalFromSoldAdult += transferSeatsCount;
						}
					}
				}catch(e) { }				
			} else {
				var availSoldSeat = (transferSeatsCount - soldAdult);
				if (parseInt(availSoldSeat) >= 0) {
					totalFromSoldAdult += soldAdult;
					totalFromOnHoldAdult += availSoldSeat;
				} else {
					totalFromSoldAdult += transferSeatsCount;
				}
			}

			if (checkInfant) {
				totalFromSoldInfant += soldInfant;
				totalFromOnHoldInfant += onHoldInfant;
			}
		}// end of for loop

		// preparing the target flight informations
		// referring the other grid - [ only one row can be selected ]UI_flightToReprotect.strGridRow
		var strTargetFlightId = jQuery("#listFlightRP").getCell(UI_flightToReprotect.strGridRow,'flightId');
		var strTargetSegmentCode = jQuery("#listFlightRPTO").getCell(UI_flightToReprotect.otherGridRowNumber,'segmentTO');
		var strTargetFlightSegmentId = jQuery("#listFlightRPTO").getCell(UI_flightToReprotect.otherGridRowNumber,'segmentIdTO');

		setField("hdnLogicalCabinClassCode", strLogicalCCCode);
		setField("hdnTargetCabinClassCode", strTargetCabinClassCode);
		setField("hdnTargetFlightId", strTargetFlightId);
		setField("hdnTargetSegmentCode", strTargetSegmentCode);
		setField("hdnTargetFlightSegmentId", strTargetFlightSegmentId);

		// the hidden variable contains the prepared source flight distribution
		// details
		setField("hdnSourceFlightDistributions", strSourceFlightDistributions);
		setField("hdnFlightId", parent.arrBaseArray[0][6]);

		// preparing the other grid's available figures
		var newAvailable ='';
		var tmpAvailable = jQuery("#listFlightRPTO").getCell(UI_flightToReprotect.otherGridRowNumber,'segmentAvailableTO');
		var tmpAvailableArr = tmpAvailable.split(":");
		var isOverbooked = false;
		for(var ind=0; ind < tmpAvailableArr.length; ind++){
			tmpAvailable = tmpAvailableArr[ind].split("/");
			finalOtherAvailableAdult = tmpAvailable[0];
			finalOtherAvailableInfant = tmpAvailable[1];
			// Setting the other grid's Available adult/infant column after
			// reproteced
			availableAdult = parseInt(finalOtherAvailableAdult)
					- parseInt(totalFromSoldAdult) - parseInt(totalFromOnHoldAdult);
			availableInfant = parseInt(finalOtherAvailableInfant)
					- parseInt(totalFromSoldInfant)
					- parseInt(totalFromOnHoldInfant);
			if(availableAdult < 0){
				isOverbooked = true;				
			}
			if(newAvailable == ''){
				newAvailable = availableAdult + "/" + availableInfant;
			}
			else{
				newAvailable = newAvailable + ":" + availableAdult + "/" + availableInfant;
			}
		}
		if(isOverbooked){
			alert('Please note that target flight will be overbooked!');
		}
		jQuery("#listFlightRPTO").setCell (UI_flightToReprotect.otherGridRowNumber,'segmentAvailableTO',newAvailable);
		var newSold = '';
		var tmpSold = jQuery("#listFlightRPTO").getCell(UI_flightToReprotect.otherGridRowNumber,'segmentSoldTO');
		var tmpSoldArr = tmpSold.split(":");
		for(var ind=0; ind< tmpSoldArr.length; ind++){
			tmpSold = tmpSoldArr[ind].split("/");
			finalOtherSoldAdult = tmpSold[0];
			finalOtherSoldInfant = tmpSold[1];
			// Setting the other grid's Sold adult/infant column after reproteced
			finalSoldAdult = parseInt(finalOtherSoldAdult)
					+ parseInt(totalFromSoldAdult);
			finalSoldInfant = parseInt(finalOtherSoldInfant)
					+ parseInt(totalFromSoldInfant);
			if(newSold == ''){
				newSold = finalSoldAdult + "/" + finalSoldInfant;
			}
			else{
				newSold = newSold + ":" + finalSoldAdult + "/" + finalSoldInfant;
			}
		}
		jQuery("#listFlightRPTO").setCell (UI_flightToReprotect.otherGridRowNumber,'segmentSoldTO',newSold);

		// preparing the other grid's onhold figures
		var newOnHold = '';
		var tmpOnHold = jQuery("#listFlightRPTO").getCell(UI_flightToReprotect.otherGridRowNumber,'segmentOnHoldTO');
		var tmpOnHoldArr = tmpOnHold.split(":");
		for(var ind=0; ind< tmpOnHoldArr.length; ind++){
			tmpOnHold = tmpOnHoldArr[ind].split("/");
			finalOtherOnHoldAdult = tmpOnHold[0];
			finalOtherOnHoldInfant = tmpOnHold[1];
			// Setting the other grid's on hold adult/infant column after reproteced
			finalOnHoldAdult = parseInt(finalOtherOnHoldAdult)
					+ parseInt(totalFromOnHoldAdult);
			;
			finalOnHoldInfant = parseInt(finalOtherOnHoldInfant)
					+ parseInt(totalFromOnHoldInfant);
			if(newOnHold == ''){
				newOnHold = finalOnHoldAdult + "/" + finalOnHoldInfant;
			}
			else{
				newOnHold = newOnHold + ":" + finalOnHoldAdult + "/" + finalOnHoldInfant;
			}
		}
		jQuery("#listFlightRPTO").setCell (UI_flightToReprotect.otherGridRowNumber,'segmentOnHoldTO',newOnHold);

		Disable("selLogicalCC", true);
		Disable("selLogicalCCOther", true);
		Disable("btnMove", true);
		Disable("btnReset", false);
		UI_flightToReprotect.disableConfirmButtons(false);
		Disable("chkGA", false);
		Disable("chkIbeTrasferFlights", false);
		Disable("selLogicalCC", true);
		Disable("btnPaxPNR", true);
		
		if (sendSMSForPax == "Y") {
			Disable("chkSendSMS", false);
			Disable("chkSmsCnf", true);
			Disable("chkSmsOnH", true);

		} else {
			Disable("chkSendSMS", true);
			Disable("chkSmsCnf", true);
			Disable("chkSmsOnH", true);
		}
		if (sendEmailForPax == "Y") {
			Disable("chkSendEmail", false);
			Disable("chkIbeTrasferFlights", false);

		} else {
			Disable("chkSendEmail", true);
			Disable("chkIbeTrasferFlights", true);
		}

	} else {
		showWindowCommonError("Error", gridNotSelected);
		$(window.self).scrollTop(0);
		return;
	}
}

/** *************************************************************** */
/** ****** RESET REPROTECT******** */
/** *************************************************************** */
UI_flightToReprotect.resetReprotect = function() {
	UI_flightToReprotect.initValues();
	UI_flightToReprotect.disableConfirmButtons(true);

	// Calling the RowClick method to reset the Reprotect - Other/To grid
	UI_flightToReprotect.RowClick(UI_flightToReprotect.strGridRow);

	// Code to reset the Reprotect - From grid
	UI_flightToReprotect.resetChangeLogicalCC(parent.arrBaseArray,
			getValue("selLogicalCC"));

	$("#chkGA").attr('checked', true);
	getFieldByID("chkIbeTrasferFlights").checked = false;
	getFieldByID("chkSendSMS").checked = false;
	getFieldByID("chkSendEmail").checked = false;

	getFieldByID("chkSmsCnf").checked = false;
	getFieldByID("chkSmsOnH").checked = false;

	Disable("chkGA", true);
	Disable("btnReset", true);
	Disable("chkIbeTrasferFlights", true);
	Disable("chkSendEmail", true);
	Disable("chkSendSMS", true);

	Disable("chkSmsCnf", true);
	Disable("chkSmsOnH", true);
	UI_flightToReprotect.disableFROMGridInputs(false);
}

/** *************************************************************************** */
/** ********** C O N F I R M B U T T O N C L I C K E V E N T *************** */
/** *************************************************************************** */
UI_flightToReprotect.confirmReprotectPAX = function() {
	var proceed = false;
	var proceedWithIBETransfer = true;
	checkIBETransfer = getFieldByID("chkIbeTrasferFlights").checked;
	if(checkIBETransfer != null && checkIBETransfer){
		var flightSegmentId = jQuery("#listFlightRP").getCell(UI_flightToReprotect.gridSelectedRow,'segmentsID');
		if (!UI_flightToReprotect.segmentIdsOfReprotectFlights.includes(flightSegmentId)) {
			proceedWithIBETransfer = false;
			showWindowCommonError("Error", "Please select the checkbox of the pre-selected flight option to reprotect on IBE");
			$(window.self).scrollTop(0);
		} else if(UI_flightToReprotect.segmentIdsOfReprotectFlights.length > 1){
			proceedWithIBETransfer = true;
			var stringData = JSON.stringify(UI_flightToReprotect.segmentIdsOfReprotectFlights);
			setField("hdnReprotectSegmentIDs",stringData);
		} else {
			proceedWithIBETransfer = false;
			showWindowCommonError("Error", "Please select more than one checkBoxes of desired flights to reprotect on IBE");
			$(window.self).scrollTop(0);
		}
	}
	if(UI_flightToReprotect.oldSegment != UI_flightToReprotect.newSegment && proceedWithIBETransfer){
		proceed = confirm("Original and New Flights have different routes!. Are you sure you want to proceed ?");
	}
	else{
		proceed = true;
	}
	if(proceed && proceedWithIBETransfer){
		if (getFieldByID("chkGA").checked) {
			setField("hdnAlert", true);
		} else {
			setField("hdnAlert", false);
		}
		if (getFieldByID("chkIbeTrasferFlights").checked) {
			setField("hdnIBETransferFlights", true);
		} else {
			setField("hdnIBETransferFlights", false);
		}
		if (getFieldByID("chkSendSMS").checked) {
			setField("hdnSMSAlert", true);
		} else {
			setField("hdnSMSAlert", false);
		}
		if (getFieldByID("chkSendEmail").checked) {
			setField("hdnEmailAlert", true);
		} else {
			setField("hdnEmailAlert", false);
		}
		if (getFieldByID("chkSmsCnf").checked) {
			setField("hdnSmsCnf", true);
		} else {
			setField("hdnSmsCnf", false);
		}
		if (getFieldByID("chkSmsOnH").checked) {
			setField("hdnSmsOnH", true);
		} else {
			setField("hdnSmsOnH", false);
		}
		if (getFieldByID("chkSendEmailOriginAgent").checked) {
			setField("hdnEmailOriginAgent", true);
		} else {
			setField("hdnEmailOriginAgent", false);
		}
		if (getFieldByID("chkSendEmailOwnerAgent").checked) {
			setField("hdnEmailOwnerAgent", true);
		} else {
			setField("hdnEmailOwnerAgent", false);
		}
	
		var strSmsAlert = getFieldByID("chkSendSMS").checked;
		var strSmsCnf = getFieldByID("chkSmsCnf").checked;
		var strSmsOnH = getFieldByID("chkSmsOnH").checked;
	
		if (strSmsAlert) {
			if (!strSmsCnf && !strSmsOnH) {
				showWindowCommonError("Error", smsempty);
				document.getElementById("chkSmsCnf").focus();
				$(window.self).scrollTop(0);
				return;
			}
		}
		logicalCCCode = "";
		setField("hdnMode", "CONFIRM.REPROTECT");
		$('#frmFlightReprotectSearch').ajaxSubmit(options);
	}
}

UI_flightToReprotect.clearFROM = function(){
	$("#listFlightRPFROM").clearGridData();
	// clearing  the values of Re-protect From labels
	$("#spnFlightNo").text("");
	$("#spnDeparture").text("");
	$("#spnArrival").text("");
	$("#spnDepDate").text("");
	$("#spnCOSFrom").text("");
	
}

UI_flightToReprotect.clearTO = function(){
	$("#listFlightRPTO").clearGridData();
	// clearing  the values of Re-protect From labels
	$("#spnFlightNoOther").text("");
	$("#spnDepartureOther").text("");
	$("#spnArrivalOther").text("");
	$("#spnCOSOther").text("");
	$("#spnModelCap").text("");
	$("#spnDepDateOther").text("");
	Disable("btnReset", true);
	setVisible("spnReprotectTo", false);
	setVisible("selfReprotectRow", false);
	UI_flightToReprotect.otherGridRowNumber = -1;
	setVisible("agentEmailRow", false);

	
}

UI_flightToReprotect.disableFROMGridInputs = function(val){
	$("input[name='checkboxFROM']").each( function () {
        if(document.getElementById('hiddenPrivilege') && document.getElementById('hiddenPrivilege').value == "1") {
            $(this).prop('disabled', val);
        }
	});
	$("input[name='txtReprotect']").each( function () {
        if(document.getElementById('hiddenPrivilege') && document.getElementById('hiddenPrivilege').value == "1") {
            $(this).prop('disabled', val);
        }
	});
	$("input[name='btnPaxPNR']").each( function () {
		$(this).prop('disabled', val);
	});
	
}

/** ************************************************************************** */
/** * C O N F I R M A N D R O L L F O R W A R D B U T T O N E V E N T *** */
/** ************************************************************************** */
UI_flightToReprotect.confirmReprotectRollForward = function() {
	if (getFieldByID("chkGA").checked) {
		setField("hdnAlert", true);
	} else {
		setField("hdnAlert", false);
	}
	if (getFieldByID("chkIbeTrasferFlights").checked) {
		setField("hdnIBETransferFlights", true);
	} else {
		setField("hdnIBETransferFlights", false);
	}
	if (getFieldByID("chkSendSMS").checked) {
		setField("hdnSMSAlert", true);
	} else {
		setField("hdnSMSAlert", false);
	}
	if (getFieldByID("chkSendEmail").checked) {
		setField("hdnEmailAlert", true);
	} else {
		setField("hdnEmailAlert", false);
	}
	if (getFieldByID("chkSmsCnf").checked) {
		setField("hdnSmsCnf", true);
	} else {
		setField("hdnSmsCnf", false);
	}
	if (getFieldByID("chkSmsOnH").checked) {
		setField("hdnSmsOnH", true);
	} else {
		setField("hdnSmsOnH", false);
	}
	if (getFieldByID("chkSendEmailOriginAgent").checked) {
		setField("hdnEmailOriginAgent", true);
	} else {
		setField("hdnEmailOriginAgent", false);
	}
	if (getFieldByID("chkSendEmailOwnerAgent").checked) {
		setField("hdnEmailOwnerAgent", true);
	} else {
		setField("hdnEmailOwnerAgent", false);
	}
	
	var strSmsAlert = getFieldByID("chkSendSMS").checked;
	var strSmsCnf = getFieldByID("chkSmsCnf").checked;
	var strSmsOnH = getFieldByID("chkSmsOnH").checked;

	if (strSmsAlert) {
		if (!strSmsCnf && !strSmsOnH) {
			showWindowCommonError("Error", smsempty);
			document.getElementById("chkSmsCnf").focus();
			$(window.self).scrollTop(0);
			return;
		}
	}
	var sourceScheduleId = parent.arrBaseArray[0][8];
	var targetScheduleId = jQuery("#listFlightRP").getCell(UI_flightToReprotect.gridSelectedRow,'scheduleId');
	if (sourceScheduleId == targetScheduleId) {
		showWindowCommonError("Error", sameScheduledFlight);
		$(window.self).scrollTop(0);
		return false;
	} else if ((sourceScheduleId == "") || (sourceScheduleId == "null")
			|| (targetScheduleId == "") || (targetScheduleId == "null")) {
		showWindowCommonError("Error", nonScheduledFlight);
		$(window.self).scrollTop(0);
		return false;
	} else {
		var intLeft = (window.screen.width - 1000) / 2;
		var intTop = (window.screen.height - 710) / 2;
		var winWidth = 850;
		var winheight = 380;
		var strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width='
				+ winWidth
				+ ',height='
				+ winheight
				+ ',resizable=no,top='
				+ intTop + ',left=' + intLeft;
		if (!flightToReprotect.isParentOpener) {
			opener.opener.top[0].objRollForwardWindow = window.open(
					'showFile!reprotectRollForwad.action', 'CRollWindow', strProp);
		}
	}
}

/** ************************************************************************ */
/** ************ CHANGE THE REPROTECT_FROM GRID LOGICALCLASS OF SERVICE **** */
/** ************************************************************************ */
var logicalCCCodeFrom = "";
UI_flightToReprotect.changeLogicalCC = function(selCCCode) {
	logicalCCCodeFrom = selCCCode;
	var logicalCCLength = parent.arrBaseArray.length;
	var arrReprotectSeg = new Array();
	var rowNum = 0;
	$("#listFlightRPFROM").clearGridData();
	for ( var logicalCCCount = 0; logicalCCCount < logicalCCLength; logicalCCCount++) {
		if (selCCCode == parent.arrBaseArray[logicalCCCount][0]) {
			// setting the from data grid
			arrReprotectSeg[rowNum] = new Array();
			var row = parent.arrBaseArray[logicalCCCount][5];
			row[0][0] = rowNum;
			arrReprotectSeg[rowNum] = row[0];
            if(document.getElementById('hiddenPrivilege') && document.getElementById('hiddenPrivilege').value == "1") {
                CheckBox = "<input name=" + '"checkboxFROM"' + " type=" + '"checkbox"' + "value=" + '"' + rowNum + '"' + "id=" + '"flightRPFROMCheck' + rowNum + '"' + " >";
                ReprotectAdultInput = "<input :CUSTOM:  type='text' id='txtReprotect"+rowNum+"' name='txtReprotect' onKeyUp='parent.validateTxtBox(this)' onKeyPress='parent.validateTxtBox(this)' maxlength='8' align='right' style='width:50px'>";
            } else {
                CheckBox = "<input name=" + '"checkboxFROM"' + " type=" + '"checkbox"' + "value=" + '"' + rowNum + '"' + "id=" + '"flightRPFROMCheck' + rowNum + '"' + " disabled >";
                ReprotectAdultInput = "<input :CUSTOM:  type='text' id='txtReprotect"+rowNum+"' name='txtReprotect' onKeyUp='parent.validateTxtBox(this)' onKeyPress='parent.validateTxtBox(this)' maxlength='8' align='right' style='width:50px' disabled >";
            }
			pnrButton = "<input :CUSTOM: type='button' id='btnPaxPNR' name='btnPaxPNR' align='center' style='width:30px' onClick='parent.UI_flightToReprotect.selectReprotectPNR("+ rowNum +")' value='PNR'>";
			
			var temp = {
					Id : rowNum + 1,
					SegmentFROM : arrReprotectSeg[rowNum][1],
					segmentSoldFROM : arrReprotectSeg[rowNum][2],
					segmentOnHoldFROM : arrReprotectSeg[rowNum][3],
					transferAll: CheckBox,
					reprotectAdultFROM: ReprotectAdultInput,
					pnrFROM: pnrButton,
					fligthIdFROM: arrReprotectSeg[rowNum][6],
					flightSegemntIdFROM: arrReprotectSeg[rowNum][7]
				};
			$("#listFlightRPFROM").addRowData( rowNum , temp); 
			rowNum++;
		}
	}
	UI_flightToReprotect.disableFROMGridInputs(true);
	UI_flightToReprotect.changeLogicalClassOfServiceOther(selCCCode,UI_flightToReprotect.strGridRow);
	if(ckeckTOHasData()){
		UI_flightToReprotect.disableFROMGridInputs(false);
	} else {
		UI_flightToReprotect.disableFROMGridInputs(true);
	}
}

ShowPopProgress = function() {
	setVisible("divLoadMsg", true);
}

HidePopProgress = function() {
	setVisible("divLoadMsg", false);
}

/** *************************************************************** */
/** ****** CHANGE SEARCH WHILE FLIGHT HAS SELECTED TO REPROTECT******** */
/** *************************************************************** */
UI_flightToReprotect.resetFROMInSearch = function() {
	UI_flightToReprotect.initValues();
	UI_flightToReprotect.disableConfirmButtons(true);

	// Code to reset the Reprotect - From grid
	UI_flightToReprotect.resetChangeLogicalCC(parent.arrBaseArray,
			getValue("selLogicalCC"));

	getFieldByID("chkGA").checked = false;
	getFieldByID("chkIbeTrasferFlights").checked = false;
	getFieldByID("chkSendSMS").checked = false;
	getFieldByID("chkSendEmail").checked = false;

	getFieldByID("chkSmsCnf").checked = false;
	getFieldByID("chkSmsOnH").checked = false;

	Disable("chkGA", true);
	Disable("btnReset", true);
	Disable("chkIbeTrasferFlights", true);
	Disable("chkSendEmail", true);
	Disable("chkSendSMS", true);

	Disable("chkSmsCnf", true);
	Disable("chkSmsOnH", true);
	UI_flightToReprotect.clearTO();
}

UI_flightToReprotect.selectCheckBoxesForRP = function() { 
	var rows = $("#listFlightRP")[0].rows;

//    if(idsOfSelectedAdvertisements.length > 0 && idsOfSelectedAdvertisements.length == advertisementIdArrayToDelete.length){
	if(UI_flightToReprotect.segmentIdsOfReprotectFlights.length > 0){
    	for (i = 0; i < rows.length ; i++) {
    		flightSecmentId = jQuery("#listFlightRP").getCell(rows[i].id,'segmentsID');
    		index = UI_flightToReprotect.segmentIdsOfReprotectFlights.indexOf(flightSecmentId);
    		if(index > -1){
	        	var id = "flightRPCheckBox"+rows[i].id;
				document.getElementById(id).checked = true;
    		}
        }
    }
    
} 
UI_flightToReprotect.setSelectedRowIdForSearchGrid = function() { 
	
	 var ids = jQuery("#listFlightRP").getDataIDs(); 
     for(var i=0;i<ids.length;i++){ 
         var cl = ids[i]; 
         if(UI_flightToReprotect.gridSelectedRow == cl){
       	  UI_flightToReprotect.gridSelectedRowOnPage = true;
       	  break;
         }
    } 
	if(UI_flightToReprotect.gridSelectedRow > -1 && UI_flightToReprotect.gridSelectedRowOnPage){
		jQuery("#listFlightRP").setSelection(UI_flightToReprotect.gridSelectedRow , true);
	} else {
		jQuery("#listFlightRP").resetSelection();
	}
	$("#chkGA").attr('checked', true);
}

/** *************************************************************************** */
/** ****************** SHOW PAX DETAILS BUTTON ON CLICK ********************** */
/** *************************************************************************** */
UI_flightToReprotect.showPaxDetails = function() {
	var flightId = $("#hdnFlightId").val();
	if(typeof(flightId) != 'undefined' &&  flightId != null){
		var intLeft = (window.screen.width - 1000) / 2;
		var intTop = (window.screen.height - 765) / 2;
		var winheight = 600;
		var winWidth = 970;

		var strFlightId =  flightId;

		var strActionUrl = window.document.location.protocol;
		strActionUrl += "//"+window.document.location.host;
		strActionUrl += "/airadmin/private/master/";
		strActionUrl += "showFile!paxDetails.action";
		strActionUrl += "?hdnFlightId=" + strFlightId;

		var strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width='
				+ winWidth+ ',height='+ winheight+ ',resizable=no,top='+ intTop+ ',left=' + intLeft;
		opener.top[0].objRollForwardWindow = window.open(strActionUrl,"paxDetials", strProp);
	}			
}

/** *************************************************** */
/** *************** COMMON FUNCTIONS ****************** */
/** *************************************************** */

function setFieldValues(strFieldName, strValue) {
	setField(strFieldName, strValue);
}

function showErrorMsg(strType, strMsg) {
	objMsg.MessageText = strMsg;
	objMsg.MessageType = strType;
	ShowPageMessage();
}

function validateWindowTextField(textValue, msg) {
	var blnStatus = false;
	if (textValue == null || textValue == "") {
		objMsg.MessageText = msg;
		objMsg.MessageType = "Error";
		ShowPageMessage();
		blnStatus = true;
	}
	return blnStatus;
}

function validateTxtBox(objTextBox) {
	setPageEdited(true);
	var strLen = objTextBox.value.length;
	var strText = objTextBox.value;
	var blnVal = isPositiveInt(strText);
	if (!blnVal) {
		objTextBox.value = strText.substr(0, strLen - 1);
		objTextBox.focus();
	}
}

function setPageEdited(isEdited) {
	pageEdited = isEdited;
}

function isPageEdited() {
	return pageEdited;
}

function closeClick() {
	if ((!isPageEdited()) || (isPageEdited() && isConfirmed())) {
		window.close();
	}
}

function isConfirmed() {
	var cnf = confirm("Changes will be lost! Do you wish to Continue?");
	if (cnf) {
		setPageEdited(false);
		return true;
	}
	return false;
}

function closeChildWindow() {
	if (!UI_flightToReprotect.isParentOpener) {
		objWindow = opener.opener.top[0].objRollForwardWindow;
		if ((objWindow != "") && (objWindow != null) && (!objWindow.closed)) {
			objWindow.close();
		}
	}
}

function enableSMSChk() {

	if (document.getElementById("chkSendSMS").checked == true) {
		Disable("chkSmsCnf", false);
		Disable("chkSmsOnH", false);
	} else {
		Disable("chkSmsCnf", true);
		Disable("chkSmsOnH", true);

		if (document.getElementById("chkSmsCnf").checked == true) {
			document.getElementById("chkSmsCnf").checked = false;
		}
		if (document.getElementById("chkSmsOnH").checked == true) {
			document.getElementById("chkSmsOnH").checked = false;
		}
	}

}

function ckeckTOHasData() {
	var ids = jQuery("#listFlightRPTO").getDataIDs();
	if(ids.length > 0){
		return true;
	} else {
		return false;
	}
}

function multiSelectOnFROM(rowid){
	var selectedRowArr = jQuery("#listFlightRPFROM").getGridParam('selarrrow');
	index = selectedRowArr.indexOf(rowid);
	if(index > -1){
		jQuery("#listFlightRPFROM").setSelection(rowid , true);
	} else {
		jQuery("#listFlightRPFROM").setSelection(rowid , false);
	}
}

function setIBEselfReprotectionCheck(){
	if(selfReprotectionEnable == "Y"){
		setVisible("selfReprotectRow", true);
	} else {
		setVisible("selfReprotectRow", false);
	}
}

// auto date fix on blur event
UI_flightToReprotect.dateOnBlur = function(inParam){
		switch (inParam.id){
			case 0 :
				if($.trim($("#txtDepartureDateFrom").val()) != ''){
					dateChk("txtDepartureDateFrom");
				}else{
					$("#txtDepartureDateFrom").val('');
				}
				break;
			case 1 : 
				if($.trim($("#txtDepartureDateTo").val()) != ''){
					dateChk("txtDepartureDateTo");
				}else{
					$("#txtDepartureDateTo").val('');
				}
				break;
		}
	}

