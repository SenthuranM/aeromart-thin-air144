manageInvantory = {};
var screenId = "SC_INVN_001_N";
var modelNo = "";
var valueSeperator = "^";
var blnShowSeatMovementGrid = false;
manageInvantory.selectedFlightID = null;
manageInvantory.selectedFlightNo = null;
manageInvantory.selectedFlightSegInvID = null;
manageInvantory.blnEditSegment = false;
manageInvantory.airCraftModelNo = null;
manageInvantory.defaultCabinCalss = null;
manageInvantory.cliendMessages = [];
manageInvantory.userCarriers = [];
manageInvantory.zuluTimeUsed = false;
manageInvantory.cabinClassCode = null;
manageInvantory.selectedFlightObj = null;
manageInvantory.overlappingFlightId = null;
manageInvantory.objTopUpWindow;
manageInvantory.hideLogicalCC = true;
manageInvantory.hideWaitListing = true;
manageInvantory.agentWiseSeatMovementDataMap = null;
manageInvantory.hideAllocScreenExtraDetails = false;
manageInvantory.splitAdultInfantRatio;
manageInvantory.fltSegInventories = null;
manageInvantory.showOverBookinAlloc = false;
manageInvantory.logicalCabinClassInventoryRestricted = true;
manageInvantory.hideNestedAvailableSeats = false;
manageInvantory.showFCCSegBCInventoryAllocGrouping = false;

manageInvantory.ready = function(){
	top[2].HideProgress();
	$("#SeatInventory").decoratePanel("Seat Inventory - Search Flights for Seat Allocation");
	$("#SeatAllocation").decoratePanel("Seat Allocation - Summary");
	$("#segmentAllocation").decoratePanel("Segment Allocations");
	$("#cabinClassSection").hide();
	$("#segmentAllocation").parent().hide();
	$("#SeatAllocation").parent().hide();
	$("#buttonSetTable").hide();
	$('#btnSaveBC').disable();
	$("input[name='checkAllPriority']").disable();
	$("input[name='checkAllClosed']").disable();
	$("#flightResults").decoratePanel("Flights");
	if(isLogicalCCEnabled == "false"){
		$("#btnSplit").hide();
	}
    if(isPromotionEnabled == 'false') {
        $("#btnPromotion").hide();
    }

	$("#txtDepartureDateFrom, #txtDepartureDateTo").datepicker({
		showOn: "button",
		buttonImage: "../../images/calendar_no_cache.gif",
		buttonImageOnly: true,
		dateFormat: 'dd/mm/yy'
	});
	$("#txtFlightNumber").blur(function(){
		$("#txtFlightNumber").val(this.value.toUpperCase());
	});
	$("#btnClear").click(function(){manageInvantory.resetSearch();});
	$("#btnSearch").click(function(){manageInvantory.doSearch();});
	$("#txtMaxSeatFactor, #txtMinSeatFactor").numeric({allowDecimal:true});
	$("#showFlightonPanel").click(function(){manageInvantory.showFlightResultonPanel();});
	$("#showRollforwardAudit").click(function(){manageInvantory.showRollforwardAudit();});
	$("#btnClose").click(function(){manageInvantory.CloseClicked();});
	$("#btnBack").click(function(){manageInvantory.BackClicked();});
	$(document).on("click","input[name='checkAllPriority']", function(e){manageInvantory.allPriority_Click(e);});
	$(document).on("click","input[name='checkAllClosed']", function(e){manageInvantory.allClosed_Click(e);});
	$(document).on("click","a[name='marketingCarrierSummary']", function(e){manageInvantory.showCarrierSummary(e);});
	$(document).on("click","a[name='seatMovementOptionInfo']", function(e){manageInvantory.showSeatMovementOptions(e);});
	$(document).on("click","input[name='btnAddBC']", function(e){manageInvantory.addBookingClassInvnentory(e);});
	$(document).on("click","input[name='btnEditBC']", function(e){manageInvantory.editByBookingCode(e);});
	$(document).on("click","input[name='btnDeleteBC']", function(e){manageInvantory.deleteBookingClassInvnentories(e);});
	$("#btnSplit").click(function(){manageInvantory.splitSegmentAlloc();});
	$("#btnUnView").click(function(){manageInvantory.viewUserNote();});
	$("#btnUnAdd").click(function(){manageInvantory.addUserNote();});
	$("#btnSegEdit").click(function(){manageInvantory.editSegmentAllocation();});
	$("#btnSaveBC").click(function(){manageInvantory.saveInventories();});
	$("#btnRollForward").click(function(){manageInvantory.loadRollForward();});
	$("#btnSeatCharge").click(function(){manageInvantory.loadSeatCharges();});
	$("#btnMealAllocation").click(function(){manageInvantory.loadMealCharges();});
	$("#btnBaggageAllocation").click(function(){manageInvantory.loadBaggageCharges();});
	$("#linkRollforwards").click(function(){manageInvantory.showRollForwardAudits();});
	$("#btnPrintBookingClasses").click(function(){manageInvantory.loadBookingClassPrint();});
	$("#btnClose_Aloc").click(function(){manageInvantory.CloseClicked();});
	$("#btnPromotion").click(function(){manageInvantory.loadPromotion();});
	$("#btnUnAdd").disable();
	$("#btnUnView").disable();
	if(csFlightInvModifiable != ""){		
		$('#btnAddBC').disable();
		$("#btnEditBC").disable();
		$("#btnDeleteBC").disable();
		$("#btnBaggageAllocation").disable();
		$("#btnMealAllocation").disable();
		$("#btnSeatCharge").disable();
		$("#btnRollForward").disable();
		$("#btnSaveBC").disable();
		$("#btnSegEdit").disable();
		$("#btnSplit").disable();
		$("#btnUnView").disable();
	}

	if (hidFromPage == "Flight"){
		var tF = hidFlightSearch;
		var tfArray = hidFlightSumm.split("|");
		var flightForSeatAllocationTO = {};
		flightForSeatAllocationTO.flightId = tF.FlightID;
		if (tF.OlFlightId == "null") {
			flightForSeatAllocationTO.overlappingFlightId = null;
		} else {
			flightForSeatAllocationTO.overlappingFlightId = tF.OlFlightId;
		}
		manageInvantory.hideLogicalCC = (tfArray[16] == "true");
		manageInvantory.hideWaitListing = (tfArray[17] == "true");
		flightForSeatAllocationTO.flightNumber = tfArray[0];
		flightForSeatAllocationTO.modelNumber = tF.ModelNo;
		manageInvantory.defaultCabinCalss = tF.ClassOfService;
		flightForSeatAllocationTO.departure = tfArray[1];
		flightForSeatAllocationTO.departureDateTime = tfArray[2];
		flightForSeatAllocationTO.arrival = tfArray[3];
		flightForSeatAllocationTO.arrivalDateTime = tfArray[4];
		flightForSeatAllocationTO.viaPoints = tfArray[5];
		flightForSeatAllocationTO.flightStatus = tfArray[6];
		//flightForSeatAllocationTO.segmentAllocation = tfArray[7];
		flightForSeatAllocationTO.segmentSold = tfArray[9];
		flightForSeatAllocationTO.segmentOnHold = tfArray[10];
		flightForSeatAllocationTO.seatFactor = tfArray[11];
		flightForSeatAllocationTO.fromFromPage = "Flight";
		var selectedFlight = {'id':'1', "flightForSeatAllocationTO":flightForSeatAllocationTO};
		manageInvantory.selectedFlightObj = selectedFlight;
		manageInvantory.loadSeatAllocate(selectedFlight);
		var toFlightGrid = {"page":1,"rows":[selectedFlight], "total":1}
		manageInvantory.constructFlightsGrid(null, toFlightGrid);
		screenId = "SC_INVN_002";
	}else{
		top.setFID(screenId);
		top[1].objTMenu.tabPageEdited(screenId, false);
		manageInvantory.constructFlightsGrid("",null)
		manageInvantory.pageLoadActionCall();
	}
	$("#selCOS").change(function(){manageInvantory.setDefaultCap(this.selectedIndex);});
};

manageInvantory.pageLoadActionCall = function(){
	$.ajax({
	    url:'loadFlightAllocation.action',
	    beforeSend: top[2].ShowProgress(),
	    data: $.extend({}, {}),
	    dataType:"json",
	    complete: manageInvantory.pageLoadActionCallDataProcess,
	    error:manageInvantory.pageLoadError
	 });
};

manageInvantory.pageLoadActionCallDataProcess = function(response){
	top[2].HideProgress();
	response = eval("("+response.responseText+")");
	manageInvantory.enableDisableAncillaryButtons(response);
	$("#txtMinSeatFactor").val(parseInt(response.seatFactorMin,10));
	$("#txtMaxSeatFactor").val(parseInt(response.seatFactorMax,10));
	manageInvantory.airCraftModelNo = response.aircraftModelNumber;
	manageInvantory.defaultCabinCalss = response.ccCode;
	manageInvantory.zuluTimeUsed = response.isTimeInZulu;
	if (manageInvantory.zuluTimeUsed){
		$("#radTime_Z").check();
	}else{
		$("#radTime_L").check();
	}
	var blOpt = $("<option></option>").attr("value", "-1");
	$("#selDeparture, #selArrival, #selVia1, #selVia2, #selVia3, #selVia4").append(blOpt.clone());
	$("#selDeparture, #selArrival, #selVia1, #selVia2, #selVia3, #selVia4").fillDropDown({firstEmpty:false, dataArray:response.airportList , keyIndex:0, valueIndex:1});
	var allOpt = $("<option>All</option>").attr("value", "-1");
	$("#selCabinClass").append(allOpt.clone());
	$("#selCabinClass").fillDropDown({firstEmpty:false, dataArray:response.cabinClassList , keyIndex:0, valueIndex:1}); 
	manageInvantory.cliendMessages = response.clientMessages;
	manageInvantory.hideLogicalCC  = response.hideLogicalCC;
	manageInvantory.hideWaitListing  = response.hideWaitListing;
	manageInvantory.hideAllocScreenExtraDetails = response.hideAllocScreenExtraDetails;
	$("#selFltStatus").append(allOpt.clone());
	$("#selFltStatus").fillDropDown({firstEmpty:false, dataArray:response.flightStatusList , keyIndex:0, valueIndex:1});
	manageInvantory.userCarriers = response.userCarrierCode;
	manageInvantory.showOverBookinAlloc = !response.showOverBookAlloc;
};

manageInvantory.pageLoadError = function(response){
	top[2].HideProgress();
	alert("ERROR");
};

manageInvantory.objOnFocus = function(){
	top[2].HidePageMessage();
};

manageInvantory.resetSearch = function(){
	
};

manageInvantory.CloseClicked = function(){
	if ( top[1].objTMenu.focusTab == screenId ){
		top[1].objTMenu.tabRemove(screenId);
	}
};

manageInvantory.loadPromotion = function(){
	//if (manageInvantory.selectedFlightObj == null) {//TODO check any promotion link to the Flight
		//showCommonError("Error", "No segment inventories for linking seat charges");
	//}
	
	manageInvantory.setUpPopupDetails(620, 935);
	//modelNo = manageInvantory.selectedFlightObj.flightForSeatAllocationTO.modelNumber;
	var objAllocationForm = document.getElementById("frmManageSeatAllocate");
	objAllocationForm.target = "CWindow";
	objAllocationForm.action = "showLoadJsp!loadPromotionRequestsManagement.action?fltId=" + manageInvantory.selectedFlightObj.flightForSeatAllocationTO.flightId	
		+ "&flOBJ=test";
	objAllocationForm.submit();
};

manageInvantory.setDefaultCap = function(index){
	var selData = $.data( $("#selCOS")[0], "selData");
	$("#idCapacity").html("<b>Default Capacity (Adult/Infant) : </b>"+selData[index][2]);
	manageInvantory.buildSegmentGrid($.data($("#segmentAllocation")[0], "FullModel"), selData[index][0]);
	manageInvantory.cabinClassCode = $("#selCOS").val();
};

manageInvantory.BackClicked = function(){
	var callFrom = hidFromPage;
	if (callFrom != 'Flight') {
		$("#SeatAllocation").parent().hide();
		$("#SeatInventory").parent().show();
		$("#flightResults").parent().show();
		$("#buttonSetTable").hide();
		$("#cabinClassSection").hide();
		$("#segmentAllocation").parent().hide();
		$("#SeatAllocation").parent().hide();
		$("#cabinClassTop").hide();
		$("#flightResults").prepend($("#tblSearchFlights"));
		$(".backToRemove").remove();
		$(".cabinCodeAllocTemplateC").hide();
	}else{

		if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
			screenId = "SC_FLGT_001";
			if ((top[1].objTMenu.tabIsAvailable(screenId) && top[1].objTMenu.tabRemove(screenId)) 
					|| (!top[1].objTMenu.tabIsAvailable(screenId))) {

					top[1].objTMenu.tabSetTabInfo("SC_INVN_002", "SC_FLGT_001",
							"showFlight.action", "Flights", false);
					var strFlightSearch = top[1].objTMenu.tabGetValue(screenId).split(valueSeperator);
					location.replace("showFlight.action?hdnFromPage=INVENTORY&hdnMode=SEARCH&hdnRecNo="
									+ strFlightSearch[0]
									+ "&searchCriteria="
									+ strFlightSearch[1]
									+ "&hdnFromFromPage="
									+ hidFromPage);
					top[2].ShowProgress();
				}
			}
		}
};

manageInvantory.validateSearchSeatInventory = function(){
	manageInvantory.objOnFocus();
	//manageInvantory.cliendMessages["departureDateFromRqrd"]
	//manageInvantory.cliendMessages.departureDateFromRqrd
	if ($("#txtDepartureDateFrom").val() == ""){
		showCommonError("Error", manageInvantory.cliendMessages[11].departureDateFromRqrd);
		return;
	}
	if ( !dateValidDate($("#txtDepartureDateFrom").val()) ) {
		showCommonError("Error", manageInvantory.cliendMessages[11].departureDateFromRqrd);
		$("#txtDepartureDateFrom").val("");
		$("#txtDepartureDateFrom").facus();
		return;
	}
	if ($("#txtDepartureDateTo").val() == ""){
		showCommonError("Error", manageInvantory.cliendMessages[1].departureDateToRqrd);
		return;
	}
	if ( !dateValidDate($("#txtDepartureDateTo").val()) ) {
		showCommonError("Error", manageInvantory.cliendMessages[1].departureDateToRqrd);
		$("#txtDepartureDateTo").val("");
		$("#txtDepartureDateTo").facus();
		return;
	}
	var strFromDate = document.getElementById("txtDepartureDateTo").value;
	var strToDate = document.getElementById("txtDepartureDateFrom").value;
	var isToDateLessthanFromDate = CheckDates(strFromDate, strToDate);
	if (!(strFromDate == strToDate)) {
		if (isToDateLessthanFromDate) {
			showCommonError("Error", manageInvantory.cliendMessages[10].departureDateFromBnsRleVltd);
			return;
		}
	}
	return true;
};

manageInvantory.doSearch = function(){
	if (!manageInvantory.validateSearchSeatInventory()){
		return;
	}else{
		var requsetData = {};
		requsetData['hdnMode'] = "SEARCH";
		requsetData['txtDepartureDateFrom'] = $("#txtDepartureDateFrom").val();
		requsetData['txtDepartureDateTo'] = $("#txtDepartureDateTo").val();
		requsetData['txtFlightNumber'] = $("#txtFlightNumber").val();
		requsetData['selFltStatus'] = $("#selFltStatus").val();
		requsetData['selDeparture'] = $("#selDeparture").val();
		requsetData['selArrival'] = $("#selArrival").val();
		requsetData['selVia1'] = $("#selVia1").val();
		requsetData['selVia2'] = $("#selVia2").val();
		requsetData['selVia3'] = $("#selVia3").val();
		requsetData['selVia4'] = $("#selVia4").val();
		requsetData['radTime'] = $("input[name='radTime']:checked").val();
		requsetData['txtMinSeatFactor'] = $("#txtMinSeatFactor").val();
		requsetData['txtMaxSeatFactor'] = $("#txtMaxSeatFactor").val();
		requsetData['selCabinClass'] = $("#selCabinClass").val();
		requsetData['chkSunday'] = $("input[name='chkSunday']:checked").val();
		requsetData['chkMonday'] = $("input[name='chkMonday']:checked").val();
		requsetData['chkTueday'] = $("input[name='chkTueday']:checked").val();
		requsetData['chkWedday'] = $("input[name='chkWedday']:checked").val();
		requsetData['chkThuday'] = $("input[name='chkThuday']:checked").val();
		requsetData['chkFriday'] = $("input[name='chkFriday']:checked").val();
		requsetData['chkSatday'] = $("input[name='chkSatday']:checked").val();
		manageInvantory.constructFlightsGrid('loadFlightsForSeatAllocation.action',requsetData)
	}
};

manageInvantory.constructFlightsGrid = function(newUrl, reqData){
	$("#tblSearchFlights").empty();
	var gridTable = $("<table></table>").attr("id", "tblGridFlights");
	var gridPager = $("<div></div>").attr("id", "flighGrdPager");
	$("#tblSearchFlights").prepend(gridTable, gridPager);
	var temGrid  = $("#tblGridFlights").jqGrid({
		datatype: function(postdata) {
	        $.ajax({
	           url: newUrl,
	           beforeSend:top[2].ShowProgress(),
	           data: $.extend(postdata, reqData),
	           dataType:"json",
	           complete: function(jsonData,stat){	        	  
	              if(stat=="success") {
	            	  mydata = eval("("+jsonData.responseText+")");
	            	  if (mydata.msgType != "Error"){
		            	 $.data(temGrid[0], "gridData", mydata.rows);
		            	 temGrid[0].addJSONData(mydata);
	            	  }else{
	            		  alert(mydata.message)
	            	  }
	            	  top[2].HideProgress();
	              }
	           }
	        });
	    },
		height: 300,
		width: '100%',
		colNames:[ '&nbsp;', 'flightId' ,'Flight No', 'Dep.', 'Dep. Date/Time' ,'Arr.', 'Arr. Date/Time',
		           'Via<br> Points', 'Aircraft <br>Model', 'Status', 'Overlap', "Allocated <br>Seats <br>(F/J/Y)",
		           "Sold Seats<br>(F/J/Y)", "On Hold Seats<br>(F/J/Y)", "Seat<br> Factor", "Lowest<br> fare", "COSList"],
		colModel:[
		    {name:'id',  index:'id', width:20},
		    {name:'flightId', hidden:true, jsonmap:"flightForSeatAllocationTO.flightId"},
			{name:'flightNumber',  index:'flightForSeatAllocationTO.flightNumber', width:50, jsonmap: "flightForSeatAllocationTO.flightNumber"},
			{name:'departure', index:'departure', width:35, align:"center",  jsonmap: "flightForSeatAllocationTO.departure"},
			{name:'departureDateTime', index:'flightForSeatAllocationTO.departureDateTime', width:115, align:"center", jsonmap: "flightForSeatAllocationTO.departureDateTime"},
			{name:'arrival', index:'arrival', width:35, align:"center", jsonmap: "flightForSeatAllocationTO.arrival"},
			{name:'arrivalDateTime', index:'flightForSeatAllocationTO.arrivalDateTime', width:115, align:"center", jsonmap: "flightForSeatAllocationTO.arrivalDateTime"},
			{name:'viaPoints', width:45, align:"right", jsonmap: "flightForSeatAllocationTO.viaPoints"},
			{name:'modelNumber', width:65, align:"right", jsonmap: "flightForSeatAllocationTO.modelNumber", hidden:manageInvantory.hideAllocScreenExtraDetails},
			{name:'flightStatus', index:'modelNumber', width:40, align:"center", jsonmap: "flightForSeatAllocationTO.flightStatus"},
			{name:'overlappingFlightId', width:50, align:"center", jsonmap: "flightForSeatAllocationTO.overlappingFlightId", formatter:function(cellVal, options, rowObject){
				return (cellVal==null)?"N":"Y";
			}},
			{name:'segmentAllocation', index:'flightForSeatAllocationTO.segmentAllocation', width:70, align:"right", jsonmap: "flightForSeatAllocationTO.segmentAllocation"},
			{name:'segmentSold', index:'flightForSeatAllocationTO.segmentSold', width:70, align:"right", jsonmap: "flightForSeatAllocationTO.segmentSold"},
			{name:'segmentOnHold', index:'segmentOnHold', width:80, align:"right", jsonmap: "flightForSeatAllocationTO.segmentOnHold"},
			{name:'seatFactor', index:'seatFactor', width:45, align:"right", jsonmap: "flightForSeatAllocationTO.seatFactor"},
			{name:'lowestFare', index:'lowestFare', width:85, align:"right", jsonmap: "flightForSeatAllocationTO.lowestPubFare"},
			{name:'COSList',  hidden:true, jsonmap: "flightForSeatAllocationTO.COSList"}
		],
		imgpath: "",
		pager: jQuery('#flighGrdPager'),
		multiselect: false,
		viewrecords: true,
		rowNum:20, 
		sortable:true,
		altRows:true,
		altclass:"GridAlternateRow",
		jsonReader: { 
			root: "rows",
			page: "page",
			total: "total",
			records: "records",
			repeatitems: false,
			id: "0" 
		},
		onSelectRow: function(rowid){
			var dataSelectedRow = temGrid.getGridRowDataV2(rowid);
			manageInvantory.selectedFlightObj = dataSelectedRow;
			manageInvantory.loadSeatAllocate(dataSelectedRow);
		}
	}).navGrid("#flighGrdPager",{refresh: true, edit: false, add: false, del: false, search: false});
	if (newUrl == null){temGrid[0].addJSONData(reqData);$.data(temGrid[0], "gridData", reqData.rows);}
	
};

manageInvantory.constructRollFwdAuditGrid = function(newUrl, reqData){
	$("#tblRollFwdAudit").empty();
	var gridTable = $("<table></table>").attr("id", "tblGridRFAudits");
	var gridPager = $("<div></div>").attr("id", "rollFwdAuditGrdPager");
	$("#tblRollFwdAudit").prepend(gridTable, gridPager);
	var temGrid  = $("#tblGridRFAudits").jqGrid({
		height: 150,
		width: '100%',
		colNames:[ '&nbsp;', 'flight Id', 'Flight No' ,'Departure', 'Departure Date/Time', 'Roll Forward Audit Notes', 'originAptCode', 'modelNumber', 'ccCode', 'arrivalDateZulu'],
		colModel:[
		    {name:'id',  index:'id', width:25},
		    {name:'rollFwdAuditTO.flightId', hidden:true },
			{name:'rollFwdAuditTO.flightNumber',  index:'rollFwdAuditTO.flightNumber', width:50,  },
			{name:'rollFwdAuditTO.departure', index:'rollFwdAuditTO.departure', width:50, align:"center"},
			{name:'rollFwdAuditTO.departureDateTime', index:'rollFwdAuditTO.departureDateTime', width:100, align:"center"},
			{name:'rollFwdAuditTO.notes', index:'rollFwdAuditTO.notes', width:250, align:"center"},
			{name:'rollFwdAuditTO.originAptCode',  hidden:true},
			{name:'rollFwdAuditTO.modelNumber',  hidden:true},
			{name:'rollFwdAuditTO.ccCode',  hidden:true},
			{name:'rollFwdAuditTO.arrivalDateZulu',  hidden:true}
		],
		imgpath: "",
		pager: jQuery('#rollFwdAuditGrdPager'),
		multiselect: false,
		viewrecords: true,
		altRows:true,
		altclass:"GridAlternateRow",
		rowNum:20, 
		jsonReader: { 
			root: "rows",
			page: "page",
			total: "total",
			records: "records",
			repeatitems: false,
			id: "0" 
		},
		onSelectRow: function(rowid){
			var dataSelectedRow = temGrid.getGridRowDataV2(rowid);
			manageInvantory.selectedFlightObj.flightForSeatAllocationTO.flightId = dataSelectedRow.rollFwdAuditTO.flightId;
			manageInvantory.selectedFlightObj.flightForSeatAllocationTO.ccCode = dataSelectedRow.rollFwdAuditTO.ccCode;
			manageInvantory.selectedFlightObj.flightForSeatAllocationTO.flightNumber = dataSelectedRow.rollFwdAuditTO.flightNumber;
			manageInvantory.selectedFlightObj.flightForSeatAllocationTO.modelNumber = dataSelectedRow.rollFwdAuditTO.modelNumber;
			manageInvantory.selectedFlightObj.flightForSeatAllocationTO.departure = dataSelectedRow.rollFwdAuditTO.departure;
			manageInvantory.selectedFlightObj.flightForSeatAllocationTO.departureDateTime = dataSelectedRow.rollFwdAuditTO.departureDateTime;
			manageInvantory.selectedFlightObj.flightForSeatAllocationTO.arrival = dataSelectedRow.rollFwdAuditTO.arrival;
			manageInvantory.selectedFlightObj.flightForSeatAllocationTO.arrivalDateTime = dataSelectedRow.rollFwdAuditTO.arrivalDateTime;

			manageInvantory.loadSeatAllocate(manageInvantory.selectedFlightObj);
		}
	}).navGrid("#rollFwdAuditGrdPager",{refresh: true, edit: false, add: false, del: false, search: false});
	if (newUrl == null){temGrid[0].addJSONData(reqData);$.data(temGrid[0], "gridData", reqData.rows);}
	
};

manageInvantory.updateLabels = function(dataRow, FlightInfo){
	manageInvantory.selectedFlightID = dataRow.flightId;
	manageInvantory.selectedFlightNo = dataRow.flightNo;
	$("#idFlightLable").html("<b>Flight No : </b>"+dataRow.flightNo);
	$("#idDepature").html("<b>Departure No : </b>"+ FlightInfo.departure + " " +FlightInfo.departureDateTime);
	$("#idArrival").html("<b>Arrival : </b>"+FlightInfo.arrival + " "+ FlightInfo.arrivalDateTime);
	$("#idModel").html("<b>Model : </b>"+dataRow.aircraftModelNumber);
	$("#idAsk").html("<b>ASK'000 : </b> "+dataRow.availableSeatKilometers);
	$("#selCOS").empty();
	manageInvantory.cabinClassCode = dataRow.ccCode;
	$("#selCOS").fillDropDown({firstEmpty:false, dataArray:dataRow.flightCCCodes, keyIndex:0, valueIndex:1});
	$.data( $("#selCOS")[0], "selData", dataRow.flightCCCodes);
	$("#selCOS").val(manageInvantory.cabinClassCode);
	getDefCap = function(ccCode){
		var t = null;
		for (var i=0; i < dataRow.flightCCCodes.length; i++){
			if (dataRow.flightCCCodes[i][0] == ccCode){	t = dataRow.flightCCCodes[i][2];break;}
		}
		return t;
	}
	$("#idCapacity").html("<b>Default Capacity (Adult/Infant) : </b>"+getDefCap(manageInvantory.cabinClassCode));
	if ( FlightInfo.overlappingFlightId != null && FlightInfo.overlappingFlightId != "null"){
		manageInvantory.overlappingFlightId = FlightInfo.overlappingFlightId;
		$("#idOverlap").html("<b>Overlapping Flight</b>"+FlightInfo.overlappingFlightId);
		$("#idOverlap").show();
	}
	manageInvantory.enableDisableAncillaryButtons(dataRow);	
}

manageInvantory.enableDisableAncillaryButtons = function(response){
	if (response.blnSeatMap == "false"){
		$("#btnSeatCharge").attr('disabled',true);
	}
	if (response.blnShowBaggage == "false" && !response.blnShowBaggageAllowanceRemarks){
		$("#btnBaggageAllocation").attr('disabled',true);
	}
	if (response.blnShowMeal == "false"){
		$("#btnMealAllocation").attr('disabled',true);
	}
};

manageInvantory.loadSeatAllocate = function(selectedFlight){
	$("#SeatAllocation").parent().show();
	$("#SeatInventory").parent().hide();
	$("#flightResults").parent().hide();
	$("#buttonSetTable").show();
	$("#cabinClassSection").show();
	$("#cabinClassTop").show();
	$("#segmentAllocation").parent().show();
	$("#spnFlightSearchResults").hide();
	$("#spnRollForwardAudits").hide();
	if ($("#spnFlightSearchResults").children().length <= 0){
		$("#spnFlightSearchResults").append($("#tblSearchFlights"));
	}
	if ($("#spnRollForwardAudits").children().length <= 0){
		$("#spnRollForwardAudits").append($("#tblRollFwdAudit"));
	}
	manageInvantory.loadSeatAllocateAction(selectedFlight);
};

manageInvantory.loadSeatAllocateAction = function(selectedFlight){
	var reqData = {};
	reqData.flightId = selectedFlight.flightForSeatAllocationTO.flightId;
	if(manageInvantory.cabinClassCode == null){
		reqData.ccCode = manageInvantory.defaultCabinCalss;
	}else{
		reqData.ccCode = manageInvantory.cabinClassCode;
	}
	reqData.flightNo = selectedFlight.flightForSeatAllocationTO.flightNumber;
	reqData.olFlightId = selectedFlight.flightForSeatAllocationTO.overlappingFlightId;
	reqData.aircraftModelNumber = selectedFlight.flightForSeatAllocationTO.modelNumber;
	reqData.fromFromPage = selectedFlight.flightForSeatAllocationTO.fromFromPage;
	reqData.mode = "SEARCH";
	reqData.flightSummary = "";
	reqData.flightRollForwardAuditSummary = $("#flightRollForwardAuditSummary").val();
	var selectedFlightData = selectedFlight.flightForSeatAllocationTO;
	manageInvantory.getFlightSegments('loadFlightAllocation.action', reqData, selectedFlightData);
};

manageInvantory.fillLocalGrid = function(p){
	var i = 0;
	p = $.extend({id:"",
				 data:null,
				 editable:false}, 
				p);
	$(p.id).clearGridData();
	if (p.data != null){
		$.each(p.data, function(){
			if ("D" != this.action){
				$(p.id).addRowData(i + 1, p.data[i]);
			}
			if ($(p.editable)){
				$(p.id).editRow(String(i+1));
			}
			i++;
		});
	}
};


manageInvantory.fillLocalGridBCSegs = function(p){
	var i = 0;
	p = $.extend({id:"",
				 data:null,
				 editable:false}, 
				p);
	$(p.id).clearGridData();
	if (p.data != null){
		$.each(p.data, function(){
			if ("D" != this.action){
				$(p.id).addRowData(i + 1, p.data[i]);
				var slodS = parseInt(this.seatsSold.split("/")[0], 10);
				var onHoldS = parseInt(this.onHoldSeats.split("/")[0], 10);
				var color = '';
				if (manageInvantory.overlappingFlightId != null && manageInvantory.overlappingFlightId != "null"){
					color = "green";
				}else if(this.allocatedSeats < (slodS + onHoldS)){
					color = 'red';
				}else if ("OPN" == this.flightSegmentStatus){
					color = "gray";
				}
				$(p.id).find("#"+(i + 1) + " > td").css('color',color);
			}
			if (p.editable){
				$(p.id).editRow(String(i+1));
			}
			i++;
		});
	}
};

manageInvantory.fillLocalGridLBCAlloc = function(p){
	var i = 0;
	p = $.extend({id:"",
				 data:null,
				 editable:false}, 
				p);
				
	$(p.id).clearGridData();
	
	if (p.data != null){
		$.each(p.data, function(){
			if ("D" != this.action){
				$(p.id).addRowData(i + 1, p.data[i]);
			}
			
			if (p.editable){
				$(p.id).editRow(String(i+1));
			}
			i++;
		});
	}
};

manageInvantory.buildSegmentGrid = function(res, dcc){
	manageInvantory.fltSegInventories = res.fccSegInventories;
	manageInvantory.hideNestedAvailableSeats = !res.showNestedAvailableSeats;
	$("#tblSegmentAllocations").empty();
	var gridTable = $("<table></table>").attr("id", "tblGridSegments");
	$("#tblSegmentAllocations").append(gridTable);
	var temGrid  = $("#tblGridSegments").jqGrid({
		datatype: 'local',
		height: 60,
		width: '100%',
		colNames:[ 'flightid' ,'fccInvId' , 'fccsInvId', 'flightSegId' ,'Flight No', 'Segment', 'Allocated <br>Adult' ,'Oversell <br>Adult', 'Curtailed <br>Adult','WL <br>Alloc.','Allocated <br>Infant',
		           'Sold <br>Adult/Infant', 'On Hold <br>Adult/Infant', 'Available <br>Adult/Infant', "Overbook <br>Adult","WL <br>Adult", "Flight Seg. <br>Status", "L. CC", ],
		colModel:[
		    {name:'flightid', hidden:true  },
		    {name:'fccInvId', hidden:true  },
		    {name:'fccsInvId', hidden:true  },
		    {name:'flightSegId', hidden:true  },
			{name:'flightNo',  index:'flightNo', width:65, sortable:true },
			{name:'segmentCode', index:'segmentCode', width:100, align:"center"},
			{name:'allocatedSeats', index:'allocatedSeats', width:70, align:"right"},
			{name:'oversellSeats', index:'oversellSeats', width:60, editable : true,
				editoptions : {
					className : "aa-input",
					disabled : "disabled",
					style : "font-size:11px;width:50px;text-align:right",
					dataEvents:[{
						type : 'change',
						fn: function(e){
							
						}
					}]}
			},
			{name:'curtailedSeats', index:'curtailedSeats', width:60, editable : true,
				editoptions : {
					className : "aa-input",
					disabled : "disabled",
					style : "font-size:11px;width:50px;text-align:right",
					dataEvents:[{
						type : 'change',
						fn: function(e){
							
						}
					}]}
			},
			{name:'waitListSeats', index:'waitListSeats', width:60, hidden:manageInvantory.hideWaitListing, editable : true,
				editoptions : {
					className : "aa-input",
					disabled : "disabled",
					style : "font-size:11px;width:50px;text-align:right",
					dataEvents:[{
						type : 'change',
						fn: function(e){
							
						}
					}]}
			},
			{name:'infantAllocation', index:'infantAllocation', width:70, align:"right"},
			{name:'seatsSold', index:'seatsSold', width:70, align:"right"},
			{name:'onHoldSeats', index:'onHoldSeats', width:70, align:"center"},
			{name:'availableSeats',index:'availableSeats', width:80, align:"right"},
			{name:'overbookCount', index:'overbookCount', width:50, align:"right"},
			{name:'waitListedSeats', index:'waitListedSeats', width:50, align:"right", hidden:manageInvantory.hideWaitListing},
			{name:'flightSegmentStatus', index:'flightSegmentStatus', width:90, align:"center"},
			{name:'logicalCCCode', width:50, align:"center", sortable: true, index:'logicalCCCode',  sorttype:"text"}
		],
		imgpath: "",
		viewrecords: true,
		rowNum:5,
		altRows:true,
		altclass:"GridAlternateRow",
		multiselect: true,
		sortable:true,
		sortorder: 'desc',
		sortname: 'logicalCCCode',
		onSelectRow: function(rowid){
			manageInvantory.showHideLogicalCCGrids(rowid-1);
		}
	});
	var defaultCC=(dcc==null)?res.ccCode:dcc;
	var ccRes = manageInvantory.getSegmentPerCabinClass(defaultCC, $.extend(true, {},res.fccSegInventories));
	manageInvantory.fillLocalGridBCSegs({"id":gridTable, "data":ccRes, "editable":true});
	$("#tblGridSegments").find("input[name='oversellSeats'], input[name='curtailedSeats'], input[name='infantAllocation'], input[name='waitListSeats']").numeric({allowDecimal:true});
	$.data( temGrid[0], "dataModel", ccRes );
	
	manageInvantory.buildAllocationGrid(ccRes);
	temGrid.setSelection(1, true);
};

manageInvantory.getSegmentPerCabinClass = function(cc, model){
	var ret =[];
	$.each(model, function(){
		if (this.ccCode == cc){
			ret[ret.length] = this;
		}
	});
	return ret;
};


manageInvantory.getFlightSegments = function(newUrl, reqData ,FlightInfo){

	$.ajax({
	     url: newUrl,
	     type: 'POST',
	     beforeSend: top[2].ShowProgress(),
	     data:reqData,
	     dataType: 'json',
	     success: function(response) {
	    	 manageInvantory.showOverBookinAlloc = !response.showOverBookAlloc;
	    	 manageInvantory.showFCCSegBCInventoryAllocGrouping = response.showFCCSegBCInventoryAllocGrouping;
	    	 manageInvantory.buildSegmentGrid(response, null);
	    	 $.data( $("#segmentAllocation")[0], "FullModel", response);
	    	 manageInvantory.updateLabels(response, FlightInfo);
	    	 manageInvantory.agentWiseSeatMovementDataMap = response.agentWiseSeatMovementDataMap;
	    	 top[2].HideProgress();
	     },
	     error: function(requestObj, textStatus, errorThrown){
		 	  showCommonError("Error", "System Error, Please contact system administrator");
		 	  top[2].HideProgress();
	     }
	});
};


manageInvantory.showHideLogicalCCGrids = function(rowid){
	var segDataModel = $.data( $("#tblGridSegments")[0], "dataModel" );
	var lccID = segDataModel[rowid].fccsInvId;
	if ( $("#cabinCodeAlloc_"+lccID).css("display") == "none" ){
		$("#cabinCodeAlloc_"+lccID).show();
	}else{
		$("#cabinCodeAlloc_"+lccID).hide();
	}
	var seletedRows = $("#tblGridSegments").getGridParam('selarrrow');
	if (seletedRows.length != 1){
		$("#btnUnAdd").disable();
		$("#btnUnView").disable()
	}else{
		$("#btnUnAdd").enable();
		$("#btnUnView").enable()
		manageInvantory.selectedFlightSegInvID = segDataModel[seletedRows[0] - 1].fccsInvId;
	}
};


manageInvantory.updateAllocationCheckVal = function(obj){
	var dMObj = $(obj).parent().parent().parent().parent();
	var tDmodel = $.data( dMObj[0], "dataModel");
	tDmodel[parseInt(obj.id.split("_")[0],10)-1][obj.id.split("_")[1]] = obj.checked;
	if (tDmodel[parseInt(obj.id.split("_")[0],10)-1].action != "A") {
		tDmodel[parseInt(obj.id.split("_")[0],10)-1].action = "E";
	}
	if (obj.value == "true"){
		obj.value = "false";
	}else{
		obj.value = "true";
	}
};

manageInvantory.fareRuleObj = {
		advBookingTime: "Adv Booking Time",
			agentCodes: "Agent Codes",
			effectiveFromDate: "Effective From Date",
			effectiveToDate: "Effective To Date",
			fareAmount: "Fare Amount",
			fareBasisCode: "Fare Basis Code",
			fareId: "Fare ID",
			fareRuleDescription: "Fare Rule Description",
			fareRuleId: "Fare Rule ID",
			fareRuleCode: "Fare Rule Code",
			paxLevelRefundability: "Pax Level Refundability",
			returnFlag: "Return Flag",
			visibleChannelNames: "Visible Channel Names"
}

manageInvantory.showPaxConSegments = function(id, obj, isReturn){
	var dMObj = $(obj).parent().parent().parent().parent();
	var tDmodel = $.data( dMObj[0], "dataModel");
	var connectionSegments = isReturn ? tDmodel[parseInt(id,10)-1].returnConnectionSegments : tDmodel[parseInt(id,10)-1].oneWayConnectionSegments;
	createConnectionSegmentsHTML = function(FT) {
		var outerDiv = $("<div></div>").css("clear", "both");
		var table = $('<table></table>').css({"align":"center", "border":"1px solid #999","border-collapse":"collapse", "width":"100%"});
		var headerRow = $('<tr><th style="border:1px solid #999">Segment</th><th style="border:1px solid #999">Pax Count</th></tr>');
		table.append(headerRow);
			$.each(FT, function(i, j){
				var row = $('<tr><td style="border:1px solid #999">'+i+'</td><td style="border:1px solid #999" align = '+'"center"'+'>'+j+'</td></tr>');
				table.append(row);
			});
		outerDiv.append(table);
		outerDiv.css({"width": 380, "height": 240, "overflow-y":"auto", "padding": "5px 5px"});
		return outerDiv;
	};

	$("#popUp").openMyPopUp({
		"width":400,
		"height":300,
		"appendobj":false,
		"headerHTML":$("<label>"+(isReturn ? "Return Connection": "Oneway Connection")+"</label>").css("color", "white"),
		"bodyHTML":createConnectionSegmentsHTML(connectionSegments),
		"footerHTML":""
	});
};

manageInvantory.showFareRulelink = function(id, obj){
	var dMObj = $(obj).parent().parent().parent().parent();
	var tDmodel = $.data( dMObj[0], "dataModel");
	var fareTOs = tDmodel[parseInt(id,10)-1].fareTOs;
	createFareDetailsHTML = function(FT){
		
		var tepDiv = $("<div></div>").css("clear", "both");
		$.each(FT, function(i, j){
			var bor = (i == 0)?0:1;
			var newDiv = $("<div></div>").css({"float": "left", "border-left":bor+"px solid #999",
				"padding": "0 5px", "width":225});
			for(var key in j) {
				if (j[key] != ""){
					var t = $("<div>"+manageInvantory.fareRuleObj[key]+" : "+  j[key] +"</div>");
					newDiv.append(t);
				}
			}
			tepDiv.append(newDiv);
		});
		var tempWidth = FT.length * 240;
		tepDiv.css("width",tempWidth);
		var outerDiv = $("<div></div>").append(tepDiv);
		outerDiv.css({"height":212, "overflow-y":"auto"});
		if (FT.length>3){outerDiv.css({"width": 705, "overflow-x":"auto"});}
		return outerDiv;
	};
	$("#popUp").openMyPopUp({
		"width":250 * ((fareTOs.length>3)?3:fareTOs.length),
		"height":257,
		"appendobj":false,
		"headerHTML":$("<lable>Fares & Rules</label>").css("color", "white"),
		"bodyHTML":createFareDetailsHTML(fareTOs),
		"footerHTML":""
	});
};

manageInvantory.setChecboxStatus = function(obj, model){
	$.each(model, function(i, j){
		var tID = parseInt(i, 10);
		obj.find("#"+tID).find("input[type='checkbox']").each(function(){
			if (this.value == true){
				this.checked = true;
			}
		});
		
	});
};


manageInvantory.updateAllocationVal = function(cellValue, lccID) {
	var tDmodel = $.data( $("#tblGridAllocation_"+lccID)[0], "dataModel");
	cellValue.id.split("_")[0];
	 $("#"+cellValue.id).val();
	tDmodel[parseInt(cellValue.id.split("_")[0],10) - 1].seatsAllocated = cellValue.value;
	if (tDmodel[parseInt(cellValue.id.split("_")[0],10) - 1].action != "A") {
		tDmodel[parseInt(cellValue.id.split("_")[0],10) - 1].action = "E";
	}
};

manageInvantory.updateWaitListAllocationVal = function(cellValue, lccID) {
	var tDmodel = $.data( $("#tblGridAllocation_"+lccID)[0], "dataModel");
	cellValue.id.split("_")[0];
	tDmodel[parseInt(cellValue.id.split("_")[0],10) - 1].allocatedWaitListSeats = cellValue.value;
	if (tDmodel[parseInt(cellValue.id.split("_")[0],10) - 1].action != "A") {
		tDmodel[parseInt(cellValue.id.split("_")[0],10) - 1].action = "E";
	}
};

manageInvantory.buildAllocationGrid = function(dataModel){
	var temp = $("#cabinCodeAllocTemplate");
	$("#cabinClassSection").empty();
	$("#cabinClassSection").append(temp);
	
	var checkValueAndUpdate = function(cellVal, options, rowObject){
		var checked = '';
		var objIndex = options.rowId;
		var objName = options.colModel.name;
		if(cellVal){
			checked = 'checked ="checked"';
		}
		var disabled = '';
		if(!rowObject.disable){
			disabled = 'disabled="disabled"';
		}
		
		if(objName == "statusClosed" && hasBCStatusChangePrivilege == "false"){
			return "<input type='checkbox' id='" + objIndex + "_" + objName +"' name='" + objName +
			"' " + checked + " " + disabled + "  value='" + cellVal + "'/>"
		} else {
		return "<input type='checkbox' id='" + objIndex + "_" + objName +"' name='" + objName +
		"' " + checked + " " + disabled + "  value='" + cellVal + "' onclick='manageInvantory.updateAllocationCheckVal(this)'/>"
		}
		
	};

	$.each(dataModel, function(){
		var lccID = this.fccsInvId;
		var newCodeID = "cabinCodeAlloc_"+lccID;
		var colneTempate = temp.clone();
		colneTempate.attr("id",newCodeID);
		colneTempate.find("label, input, select, span, a").each(function(){
			$(this).attr("id", this.id + "_" + lccID);
		});
		if (this.flightSegmentStatus == "CLS") {
			colneTempate.find("#btnAddBC_"+lccID).disable();
			colneTempate.find("#btnEditBC_"+lccID).disable();
			colneTempate.find("#btnDeleteBC_"+lccID).disable();
			$("#btnSplit").disable();
			$("#btnMealAllocation").disable();
			$("#btnSeatCharge").disable();
			$("#btnRollForward").disable();
			$("#btnSaveBC").disable();
			$("#btnPromotion").disable();
		}
		colneTempate.find("#selBookingCode_"+lccID).fillDropDown({firstEmpty:true, dataArray:this.bcCodeList , keyIndex:0, valueIndex:1}); 
		colneTempate.find("#idTotBC_"+lccID).text(this.totalBCAllocation);
		colneTempate.find("#idFixAllo_"+lccID).text(this.totalFixedAllocation);
		colneTempate.find("#spnBCSegmentCode_"+lccID).text(" " + this.segmentCode + " - " +this.logicalCCCode);
		$("#cabinCodeAllocTemplate").parent().append(colneTempate);
		var gridTable = $("<table></table>").attr("id", "tblGridAllocation_"+lccID).attr("style", "overflow-y:auto;");
		$("#BookingCodeAllocationGrid_"+lccID).append(gridTable);
		
		var temGrid  = $("#tblGridAllocation_"+lccID).jqGrid({
			datatype: 'local',
			height: 210,
			width: '100%',
			colNames:[ 'id', 'Booking<br>Class', 'Std' , "Rank", "Group", 'Fixed' ,'Priority', 'Alloc','WL<br>Alloc','Sold','Ohd','WL', 'Cnxd', 'Aquired', "Avail","Nes<br>Avail", "OverB",
				           "Closed", "Sold<br>In", "Sold<br>Out", "Ohd<br>In", "Ohd<br>Out", "OW<br>P2P", "RT<br>P2P", "OW<br>CONX", "RT<br>CONX", "Fares<br>& Rules", "Agents", "stausEdited", "version", "fccsbInvId" ,"action", "statusChangeAction", 'BC Type', 'Allocation Type'],
			colModel:[
			    	{name:'index', hidden:true},
				{name:'bookingCode',  index:'bookingCode', width:55},
				{name:'standardCode', index:'standardCode', width:30, align:"center"},
				{name:'nestRank', index:'nestRank', width:40, align:"right", sorttype: "int"},
				{name:'groupId', width:35, align:"right", hidden:manageInvantory.hideLogicalCC},
				{name:'fixedFlag', index:'fixedFlag', width:40, align:"center"},
				{name:'priorityFlag', index:'priorityFlag', width:40, align:"center", formatter : checkValueAndUpdate },
				{name:'seatsAllocated', index:'seatsAllocated', width:40, align:"center", formatter: function(cellVal, options, rowObject){
					return '<input type="text" classname="aa-input" '
					+ 'style : "font-size:11px;width:30px;text-align:right" disabled="disabled" id="'
					+ options.rowId + '_seatsAllocated" name="seatsAllocated" value="'
					+ cellVal + '" onchange="manageInvantory.updateAllocationVal(this,' + lccID + ')"/>';
				}},
				{name:'allocatedWaitListSeats', index:'allocatedWaitListSeats', width:40, align:"center", hidden:manageInvantory.hideWaitListing, formatter: function(cellVal, options, rowObject){
					return '<input type="text" classname="aa-input" '
					+ 'style : "font-size:11px;width:30px;text-align:right" disabled="disabled" id="'
					+ options.rowId + '_allocatedWaitListSeats" name="allocatedWaitListSeats" value="'
					+ cellVal + '" onchange="manageInvantory.updateWaitListAllocationVal(this,' + lccID + ')"/>';
				}},
				{name:'actualSeatsSold', index:'actualSeatsSold', width:35, align:"right"},
				{name:'actualSeatsOnHold', index:'actualSeatsOnHold', width:35, align:"right"},
				{name:'actualWaitListedSeats', index:'actualWaitListedSeats', width:35, align:"right", hidden:manageInvantory.hideWaitListing},
				{name:'seatsCancelled', index:'seatsCancelled', width:35, align:"center"},
				{name:'seatsAcquired', index:'seatsAcquired', width:50, align:"center"},
				{name:'seatsAvailable', index:'seatsAvailable', width:35, align:"center", formatter : function(cellVal, options, rowObject){
					return "<b>"+cellVal+"</b>";
				}},
				{name:'seatsAvailableNested', index:'seatsAvailableNested', width:40, align:"right",hidden:manageInvantory.hideNestedAvailableSeats},
				{name:'overBookCount', width:40, align:"right", hidden:manageInvantory.showOverBookinAlloc},
				{name:'statusClosed', width:45, align:"center",	formatter : checkValueAndUpdate},
				{name:'seatsSoldAquiredByNesting', width:35, align:"right", hidden:!hideOneWayReturnPaxCount},
				{name:'seatsSoldNested', width:35, align:"right", hidden:!hideOneWayReturnPaxCount},
				{name:'seatsOnHoldAquiredByNesting', width:35, align:"right", hidden:!hideOneWayReturnPaxCount},
				{name:'seatsOnHoldNested', width:35, align:"right", hidden:!hideOneWayReturnPaxCount},
				{name:'oneWayPaxCount', width:35, align:"right", hidden:hideOneWayReturnPaxCount},
				{name:'returnPaxCount', width:35, align:"right", hidden:hideOneWayReturnPaxCount},
				{name:'oneWayConnectionPaxCount', width:37, align:"right", hidden:hideOneWayReturnPaxCount, formatter: function(cellVal, options, rowObject){
						return cellVal == 0 ? cellVal : "<a href='javascript:void(0)' style='text-decoration:underline;color:#0000ff' onclick='manageInvantory.showPaxConSegments("+options.rowId+", this, false)'>"+cellVal+"</a>"
				}},
				{name:'returnConnectionPaxCount', width:37, align:"right", hidden:hideOneWayReturnPaxCount, formatter: function(cellVal, options, rowObject){
						return cellVal == 0 ? cellVal : "<a href='javascript:void(0)' style='text-decoration:underline;color:#0000ff' onclick='manageInvantory.showPaxConSegments("+options.rowId+", this, true)'>"+cellVal+"</a>"
				}},
				{name:'fareLink', width:manageInvantory.showOverBookinAlloc?100:80, align:"right", formatter: function(cellVal, options, rowObject){
						return "<a href='javascript:void(0)' style='text-decoration:underline;color:#0000ff' onclick='manageInvantory.showFareRulelink("+options.rowId+", this)'>"+cellVal+"</a>"
				}},
				{name:'linkedEffectiveAgents', width:50, align:"right"},
				{name:'stausEdited', hidden:true},
				{name:'version', hidden:true},
				{name:'fccsbInvId', hidden:true},
				{name:'action',hidden:true},
				{name:'statusChangeAction',hidden:true},
				{name:'bcType', hidden:true},
				{name:'allocationType',  hidden:true}
			],
			imgpath: "",
			multiselect: true,
			//Selected row correction with grouping
			beforeSelectRow: function (rowid, e) { 
			    var $self = $(this), selectedRowid = $self.jqGrid("getGridParam", "selrow");

			    if (selectedRowid === rowid) {
			        $self.jqGrid("resetSelection");
			    } else {
			        $self.jqGrid("setSelection", rowid, true, e);
			    }
			    return false; // Don't process the standard selection
			},
			multiboxonly: true,
			viewrecords: true,
			shrinkToFit: true,
		    	autowidth: true,
			groupingView: {
				groupField : ['bcType', 'allocationType'],
				groupOrder : ['asc', 'asc'],
				groupText :[
					//'<input type="checkbox" class="groupHeader_0"/> ' +
					'<p class="groupHeader_{0}">{0}</p>',
					//'<input type="checkbox" class="groupHeader_1"/> '+
					'{0}'],
				minusicon : 'ui-icon-minus',
				plusicon : 'ui-icon-plus',
				groupColumnShow : [false, false]
			},
			rowattr: function (rowData) {
				var colorClass = 'fntOptDef';
				if ("M" == rowData.statusChangeAction && true == rowData.statusClosed){
					colorClass = 'fntOptMancls';
				}else if ("M" == rowData.statusChangeAction && false == rowData.statusClosed){
					colorClass = 'fntOptManopn';
				}else if(true == rowData.statusClosed && ("A" == rowData.statusChangeAction || "N" == rowData.statusChangeAction) ){
					colorClass = 'fntOptSyscls';
				}else if(false == rowData.statusClosed && ("A" == rowData.statusChangeAction || "S" == rowData.statusChangeAction || "L" == rowData.statusChangeAction || "G" == rowData.statusChangeAction)){
					colorClass = 'fntOptManopn';
				}else if("R" == rowData.statusChangeAction && false == rowData.statusClosed){
					colorClass = 'fntOptSysopn';
				}else if("S" == rowData.statusChangeAction && true == rowData.statusClosed){
					colorClass = 'fntOptSyscls';
				}else if("L" == rowData.statusChangeAction && true == rowData.statusClosed){
					colorClass = 'fntOptLwrStdcls';
				}else if("G" == rowData.statusChangeAction && true == rowData.statusClosed){
					colorClass = 'fntOptGroupcls';
				}
				return {"class": colorClass};
			},
			grouping : manageInvantory.showFCCSegBCInventoryAllocGrouping,
			multipleGroup: true,
			altRows:true,
			altclass:"GridAlternateRow",
			sortable:true,
			sortname: 'nestRank asc, bcType asc',
	        sortorder: 'asc',
			cellEdit : true,
			rowNum:9999, 
			jsonReader: { 
				root: "rows",
				page: "page",
				total: "total",
				records: "records",
				repeatitems: false,
				id: "0" 
			},
			onSelectRow: function(rowid){
				
			}
		});
		manageInvantory.fillLocalGridLBCAlloc({"id":gridTable, "data":this.fccSegBCInventories, "editable":true});
		$.data( $("#tblGridAllocation_"+lccID)[0], "dataModel", this.fccSegBCInventories );
		manageInvantory.setChecboxStatus($("#tblGridAllocation_"+lccID), $.data( $("#tblGridAllocation_"+lccID)[0], "dataModel"));

		//Collapsing group when clicking on group header row
		$("#tblGridAllocation_"+lccID).click(function (e) {
		    var $target = $(e.target),
		        $groupHeader = $target.closest("tr.jqgroup");
		    if ($groupHeader.length > 0) {
		        if ((e.target.nodeName.toLowerCase() !== "span" || (!$target.hasClass('ui-icon-minus') && !$target.hasClass('ui-icon-plus'))) && !$target.hasClass('groupHeader_0') && !$target.hasClass('groupHeader_1')) {
		            $(this).jqGrid("groupingToggle", $groupHeader.attr("id"));
		            return false;
		        }
		    }
		});

// 		Select all rows of the Checked group
//		$("#tblGridAllocation_"+lccID).on("change", "input[type=checkbox]", function (e) {
//		    var currentCB = $(this);
//		    var grid = $("#tblGridAllocation_"+lccID);
//			var isChecked = this.checked;
//			//if group header is checked, to check all child checkboxes on the sub level
//			if (currentCB.is(".groupHeader_0")) {					
//				var checkboxes = currentCB.closest('tr').nextUntil('tr.tblGridAllocation_' + lccID + 'ghead_0').find('.cbox[type="checkbox"]');
//				checkboxes.each(function(){
//				    if (!this.checked || !isChecked) {                  
//		                grid.setSelection($(this).closest('tr').attr('id'), true);
//				    }
//				});			
//			} else if (currentCB.is(".groupHeader_1")) {
//				var checkboxes = currentCB.closest('tr').nextUntil('tr.tblGridAllocation_' + lccID + 'ghead_1').find('.cbox[type="checkbox"]');
//				checkboxes.each(function(){
//				    if (!this.checked || !isChecked) {                  
//		                grid.setSelection($(this).closest('tr').attr('id'), true);
//				    }
//				});			
//			} else {  //when child checkbox is checked	
//				var allCbs = currentCB.closest('tr').prevAll('tr.tblGridAllocation_' + lccID + 'ghead_0:first')
//						.nextUntil('tr.tblGridAllocation_' + lccID + 'ghead_0').andSelf().find('[type="checkbox"]');
//				var allSlaves = allCbs.filter('.cbox');
//				var master = allCbs.filter(".groupHeader");
//													
//				var allChecked = !isChecked ? false : allSlaves.filter(":checked").length === allSlaves.length;
//				master.prop("checked", allChecked);
//			}
//		});	
		
		//Reloading row after doing the grouping
		$("#tblGridAllocation_"+lccID).trigger('reloadGrid'); 
		
		$("#jqgh_tblGridAllocation_"+lccID+"_nestRank").click();
		//Collapse standby section by default
		if ($('.STANDBY').parent().parent() != null){
			$.each($('.STANDBY').parent().parent(), function(){
				$("#tblGridAllocation_"+lccID).jqGrid('groupingToggle', this.attributes.id.nodeValue);
			});
		}
		
		$("#tblGridAllocation_"+lccID).find("input[name='seatsAllocated']").numeric({allowDecimal:false});
		$("#tblGridAllocation_"+lccID).find("input[name='allocatedWaitListSeats']").numeric({allowDecimal:false});
	});
};

manageInvantory.showCarrierSummary = function(e){
	alert("Summarry in "+manageInvantory.getSufixFromId(e.target.id));
};

manageInvantory.showSeatMovementOptions = function(e){
	var t = manageInvantory.getSufixFromId(e.target.id);
	if(!blnShowSeatMovementGrid){			
		createSeatMovementHTML = function(){
			var sbutton = $("<input/>").attr({"type":"button", "value":"View", "id":"btnView"});	
			sbutton.click(function(){
				if($('input:radio').is(':checked')){
					var selOption = $("input[name='selAgentOption']:checked").val();
					popClose();
					manageInvantory.showSeatMovement(e,selOption);
				}							
			});
			var outerDiv = $("<div></div>").css({"width":250});
			var caption = $("<div>Select respective agent type :</div>").css({"width":200});
			var selAgentOption = $("<input type='radio' name='agentOption' id='radAgentOptionOrigin' value='originAgent' class='noBorder'/><font>Origin Agent</font><input type='radio' name='agentOption' id='radAgentOptionOwner' value='ownerAgent' class='noBorder'/><font>Owner Agent</font>").attr({"name":"selAgentOption", "id":"selAgentOption"});
			outerDiv.append(caption , '<br>',selAgentOption, '<br> <br>', sbutton);
			outerDiv.css({"width": 300, "height":50});
			return outerDiv;
		};
		$("#spnSeatMovementOptionInfo").openMyPopUp({
			"width" :250,
			"height" : 125,
			"headerHTML":$("<lable>Agent Options :</label>").css("color", "white"),
			"bodyHTML": createSeatMovementHTML,
			"footerHTML":""
		});
	}else{
		$("#spnSeatMovementsInfo_"+t).hide();
		blnShowSeatMovementGrid = false;
	}
};

manageInvantory.showSeatMovement = function(e,selOption){
	if(!blnShowSeatMovementGrid){
		var t = manageInvantory.getSufixFromId(e.target.id);
		var seatMovementData = manageInvantory.agentWiseSeatMovementDataMap;
		var seatMvSummaryTOArray = null;
		if(selOption == "originAgent"){
			seatMvSummaryTOArray = seatMovementData[t + "_AgentWise"]
		}else if(selOption == "ownerAgent"){
			seatMvSummaryTOArray = seatMovementData[t + "_OwnerWise"]
		}

		if (seatMvSummaryTOArray != null){
			if ( $("#spnSeatMovementsInfo_"+t).css("display") == "none" ){
				var topP = $(e.target).position().top;
				manageInvantory.constructSeatMovementGrid(seatMvSummaryTOArray , t);
				$("#spnSeatMovementsInfo_"+t).show();
				blnShowSeatMovementGrid = true;
				$("#spnSeatMovementsInfo_"+t).css({"top":topP-230,"width":680});
			}else{
				$("#spnSeatMovementsInfo_"+t).hide();
			}
		}else{
			showERRMessage("No seat movements to show");
		}
	}
};

manageInvantory.allClosed_Click = function(e){
	var t = manageInvantory.getSufixFromId(e.target.id);
	if ( $("#checkAllClosed_"+t).attr("checked") ) {
		$("#tblGridAllocation_"+t).find("input[name='statusClosed']").attr("checked", true);
		$("#tblGridAllocation_"+t).find("input[name='statusClosed']").attr("value", true);
	}else{
		$("#tblGridAllocation_"+t).find("input[name='statusClosed']").attr("checked", false);
		$("#tblGridAllocation_"+t).find("input[name='statusClosed']").attr("value", false);
	}
	
	var tabelData = $.data( $("#tblGridAllocation_"+t)[0], "dataModel" );
	$.each(tabelData, function(){
		if (!( this.action == "A" || this.action == "D")){
			this.action = "E";
		}
	});
};

manageInvantory.allPriority_Click = function(e){
	var t = manageInvantory.getSufixFromId(e.target.id);
	if ( $("#checkAllPriority_"+t).attr("checked") ) {
		$("#tblGridAllocation_"+t).find("input[name='priorityFlag']").attr("checked", true);
		$("#tblGridAllocation_"+t).find("input[name='priorityFlag']").attr("value", true);
	}else{
		$("#tblGridAllocation_"+t).find("input[name='priorityFlag']").attr("checked", false);
		$("#tblGridAllocation_"+t).find("input[name='priorityFlag']").attr("value", false);
	}
	
	var tabelData = $.data( $("#tblGridAllocation_"+t)[0], "dataModel" );
	$.each(tabelData, function(){
		if (!( this.action == "A" || this.action == "D")){
			this.action = "E";
		}
	});
};

manageInvantory.addBookingClassInvnentory = function(e){
	manageInvantory.objOnFocus();
	var t = manageInvantory.getSufixFromId(e.target.id);
	var newBookingCode = $("#selBookingCode_"+t).val();
	if (newBookingCode == ""){
		showERRMessage("Please select booking code");
	}else{
		var tabelData = $.data( $("#tblGridAllocation_"+t)[0], "dataModel" );
		var addRow = true;
		var reAddDeletedRow = false;
		var index = 0;
		$.each(tabelData, function(){
			if ( this.bookingCode == newBookingCode.split(",")[0] ){
				if (this.action == "D") {
					index = this.index;
					this.action = "A";
					reAddDeletedRow = true;
				} else {
					showERRMessage("Booking Code is alredy exist");
					addRow = false;
				}
			}
		});
		if (addRow){
			if (!reAddDeletedRow){
				index = tabelData.length+1
				var bcType = "";
				var allocationType = ""
				if (manageInvantory.showFCCSegBCInventoryAllocGrouping) {
					allocationType = newBookingCode.split(",")[4];
					if (newBookingCode.split(",")[3] == "STANDBY") {
						bcType = "3. " + newBookingCode.split(",")[3];
					} else {
						bcType = (newBookingCode.split(",")[1] == 'Y')? "1. STANDARD": "2. NON-STANDARD"
					} 
				}		
				
				var temp = {"index":index ,"bookingCode":newBookingCode.split(",")[0],
						"action":"A","standardCode":newBookingCode.split(",")[1],"fixedFlag":newBookingCode.split(",")[2], "version":0,
						"statusChangeAction":"C", "priorityFlag":false, "seatsAcquired":0, "statusClosed":false, "seatsAllocated":0, "fareLink":"",
						"actualSeatsSold":0, "actualSeatsOnHold":0, "seatsCancelled":0, "seatsAvailable":0, "linkedEffectiveAgents":0, "nestRank":0,
						"seatsSoldAquiredByNesting":0, "seatsSoldNested":0, "seatsOnHoldAquiredByNesting":0, "seatsOnHoldNested":0, "overBookCount":0,
						"allocatedWaitListSeats":0, "actualWaitListedSeats":0, 'bcType':bcType, 'allocationType':allocationType, 'oneWayPaxCount':0,
						'oneWayConnectionPaxCount':0, 'returnPaxCount':0, 'returnConnectionPaxCount':0};
				
				tabelData.push(temp);
			}
			$("#tblGridAllocation_"+t).jqGrid("clearGridData");
			manageInvantory.fillLocalGridLBCAlloc({"id":$("#tblGridAllocation_"+t), "data":tabelData, "editable":true});
			$("#tblGridAllocation_"+t).trigger('reloadGrid'); 
			$("#tblGridAllocation_"+t).find("#"+tabelData.length).find("input").enable();
			$("#tblGridAllocation_"+t).find("input[name='seatsAllocated']").numeric({allowDecimal:false});
			$("#tblGridAllocation_"+t).find("input[name='allocatedWaitListSeats']").numeric({allowDecimal:false});
			$('#btnSaveBC').enable();
		}
	}
};

manageInvantory.editByBookingCode = function(e){
	var t = manageInvantory.getSufixFromId(e.target.id);
	$("#tblGridAllocation_"+t).find("input").enable();
	if(hasBCStatusChangePrivilege == "false"){
		$("#tblGridAllocation_"+t).find("input[name='statusClosed']").disable();
	}
	$("#checkAllClosed_"+t).enable();
	$("#checkAllPriority_"+t).enable();
	$('#btnSaveBC').enable();
};

manageInvantory.deleteBookingClassInvnentories = function(e){
	manageInvantory.objOnFocus();
	var t = manageInvantory.getSufixFromId(e.target.id);
	var seletedRows = $("#tblGridAllocation_"+t).getGridParam('selarrrow');
	if (seletedRows.length <= 0){
		showERRMessage("Please select bookingcode to delete");
	}else{
		var tabelData = $.data( $("#tblGridAllocation_"+t)[0], "dataModel" );
		var delRow = false;
		for (var i = 0; i < tabelData.length; i++){
			if ( $.inArray( ""+tabelData[i].index+"" , seletedRows) > -1){
				 if ( parseInt(tabelData[i].actualSeatsSold, 10) > 0 || parseInt(tabelData[i].actualSeatsOnHold, 10) > 0 || parseInt(tabelData[i].seatsCancelled, 10) > 0){
					 showERRMessage("Cannot delete ["+tabelData[i].bookingCode+"]. Has sold, onhold or cancelled seats");
				 } else {
					 tabelData[i].action = "D";
					 delRow = true;
				 }
			}
		}
		if (delRow){
			$("#tblGridAllocation_"+t).jqGrid("clearGridData");
			
			manageInvantory.fillLocalGridLBCAlloc({"id":$("#tblGridAllocation_"+t), "data":tabelData, "editable":true});
			$('#btnSaveBC').enable();
		}
	}
	$("#tblGridAllocation_"+t).trigger('reloadGrid');
	
};

manageInvantory.editSegmentAllocation = function(){
	var selRows = $("#tblGridSegments").getGridParam('selarrrow');
	$.each(selRows, function(){
		$("#tblGridSegments").find("#"+this).find("input").attr("disabled", false);
	});
	manageInvantory.blnEditSegment = true;
	$("#btnSaveBC").enable();
};

manageInvantory.addUserNote = function(){
	var intHeight = 330;
	var intWidth = 600;
	var strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width='
			+ intWidth
			+ ',height='
			+ intHeight
			+ ',resizable=no,top='
			+ ((window.screen.height - intHeight) / 2)
			+ ',left='
			+ (window.screen.width - intWidth) / 2;
	var strFilename = "showINVNotes.action?hdnMode='ADD'";
	objWindow1 = window.open(strFilename, "CWindow1", strProp);
};

manageInvantory.viewUserNote = function(){
	var intHeight = 600;
	var intWidth = 800;
	var strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width='
			+ intWidth
			+ ',height='
			+ intHeight
			+ ',resizable=no,top='
			+ ((window.screen.height - intHeight) / 2)
			+ ',left='
			+ (window.screen.width - intWidth) / 2;
	var strFilename = "showINVNotes.action?hdnMode=VIEW&flightId="
			+ manageInvantory.selectedFlightID;
	objWindow1 = window.open(strFilename, "CWindow1", strProp);
};

manageInvantory.loadSplitAllocation = function(){
	$("#tempDiv").remove();
	var tepDiv = $("<div></div>").attr({"id": "tempDiv", "style":"display:none"});
	$("body").append(tepDiv);
	var table = $("<table></table>").attr("id", "tblSplitGrid");
	var hint = $("<div></div>").attr({"id": "divHint", "style":"margin-top:20px"});
	tepDiv.append(table, hint);
	
	
	var  updateInfantAllocationVal = function(cellValue) {
		if(manageInvantory.logicalCabinClassInventoryRestricted){
			var tDmodel = $.data( $("#tblSplitGrid")[0], "SplitResponse");
			var id = cellValue.id.split("_")[0];
			var infantAllocation = 0;
			if (id != 1) {
				infantAllocation = parseInt(cellValue.value/manageInvantory.splitAdultInfantRatio.split(":")[0], 10) * manageInvantory.splitAdultInfantRatio.split(":")[1];
				tDmodel.fccInventoryTOs[parseInt(id, 10) - 1].infantSeatAllocation = infantAllocation;
				$("#"+id+"_infantSeatAllocation").val(infantAllocation);
			}
		}
	};
	$("#tblSplitGrid").jqGrid({
		datatype: 'local',
		height: "auto",
		width: '100%',
		height:180,
		colNames:[ 'Logical Cabin Class', 'fccInvId' ,'Logical<br>CC. Code', 'Curr. ADT<br>Alloc.', 'Set ADT<br>Alloc.', 'Curr. INF<br>Alloc.', 'Set INF<br>Alloc.', 'Rank'],
		colModel:[
			{name:'logicalCabinClassDescription', index:'logicalCabinClassDescription', width:160 },
			{name:'fccInvId',  hidden:true, jsonmap:'flccInventoryTO.fccInvId'},
			{name:'logicalCCCode',  width:50, align:"center"},
			{name:'adultSeatAllocation',  width:60, align:"center"},
			{name:'adultSeatAllocation', width:65, align:"center", editable : true,
				editoptions : {
					className : "aa-input",
					style : "font-size:11px;width:40px;text-align:right",
					dataEvents:[{
						type : 'change',
						fn: function(e){		
							updateInfantAllocationVal(this);
						}
					}]}
			},
			{name:'infantSeatAllocation',  width:60, align:"center"},
			{name:'infantSeatAllocation', width:65, align:"center", editable : true,
				editoptions : {
					className : "aa-input",
					style : "font-size:11px;width:40px;text-align:right",
					dataEvents:[{
						type : 'change',
						fn: function(e){							
						}
					}]}
			},
			{name:'logicalCCRank',width:40, align:"center"}
			],
			imgpath: "",
			multiselect: false,
			viewrecords: true,
			altRows:true,
			altclass:"GridAlternateRow",
			rowNum:10,
			jsonReader: { 
				root: "rows",
				page: "page",
				total: "total",
				records: "records",
				repeatitems: false,
				id: "0" 
			},
			onSelectRow: function(rowid){
				
			}
	});
	$("#tblSplitGrid").find("input").numeric({allowDecimal:false});
	return tepDiv
};

manageInvantory.splitSaveAlloc = function(segID){
	
	if (!manageInvantory.splitSegmentAllocValidate(segID))
	{
		return;
	}
	var data = {};	
	var response =  $.data( $("#tblSplitGrid")[0], "SplitResponse");
	$.each(response.fccInventoryTOs, function(i,j){
		var objID = parseInt(i, 10)+1;		
		data["fccInventoryTOs["+i+"].adultSeatAllocation"] = $("#"+objID+"_adultSeatAllocation").val();
		data["fccInventoryTOs["+i+"].infantSeatAllocation"] = $("#"+objID+"_infantSeatAllocation").val();
		data["fccInventoryTOs["+i+"].logicalCCCode"] = this.logicalCCCode;	
		data["fccInventoryTOs["+i+"].logicalCCRank"] = this.logicalCCRank;	
		
	});	
	data["modelNo"] = manageInvantory.selectedFlightObj.flightForSeatAllocationTO.modelNumber;
	data["flightNumber"] = manageInvantory.selectedFlightNo;
	data["flightId"] = manageInvantory.selectedFlightID;
	data["cabinClassCode"] = manageInvantory.cabinClassCode;
	data["splitOption"] = $("#selSplitSeatTemp").val();
	$.ajax({
        url: 'logicalCabinClassWiseAllocation!splitAllocations.action',
        data: data,
        beforeSend: top[2].ShowProgress(),
        type:'post',
        dataType:"json",
        complete: function(jsonData,stat){	        	  
           if(stat=="success") {
         	  mydata = eval("("+jsonData.responseText+")");
         	  if (mydata.msgType != "Error"){
         		  manageInvantory.loadSeatAllocate(manageInvantory.selectedFlightObj);
         		  showCommonError("Confirmation", "Split Completed");
         		  popClose();
         	  }else{
         		 showERRMessage(mydata.message);
         	  }
      		 top[2].HideProgress();
           }
        }
     });
};

manageInvantory.splitSegmentAllocValidate = function(segID){	
	var totalAdultAllocation = 0;
	var totalInfantAllocation = 0;
	//to get the sold counts of slected segm
	var SegData = $.data( $("#tblGridSegments")[0], "dataModel");
	var tempSoldCount = SegData[parseInt(segID,10) - 1].seatsSold.split("/");
	var logicalCCCode = SegData[parseInt(segID,10) - 1].logicalCCCode;
	var adultSoldCount = tempSoldCount[0];
	var infSoldCount = tempSoldCount[1];
	
	var response =  $.data( $("#tblSplitGrid")[0], "SplitResponse");
	$.each(response.fccInventoryTOs, function(i,j){
		var objID = parseInt(i, 10)+1;
		var newAdultAllocation = parseInt($("#"+objID+"_adultSeatAllocation").val() ,10);
		var newInfantAllocation = parseInt($("#"+objID+"_infantSeatAllocation").val() ,10);
		// to validate
		if(manageInvantory.logicalCabinClassInventoryRestricted){
			totalAdultAllocation += newAdultAllocation;
			totalInfantAllocation += newInfantAllocation;
		} else {
			if((totalAdultAllocation == 0 || totalAdultAllocation == response.actualAdultCapacity)
					&& manageInvantory.eitherAdultAndInfantNewAllocationIsNotZero(newAdultAllocation, newInfantAllocation)){
				totalAdultAllocation  = newAdultAllocation;
			}
			if((totalInfantAllocation == 0 || totalInfantAllocation == response.actualInfantCapacity)
					&& manageInvantory.eitherAdultAndInfantNewAllocationIsNotZero(newAdultAllocation, newInfantAllocation)){
				totalInfantAllocation = newInfantAllocation;
			}
			//to get splited lcc
			if (j.logicalCCCode == logicalCCCode){
				if (parseInt($("#"+objID+"_adultSeatAllocation").val() ,10) < parseInt(adultSoldCount, 10)){
					showERRMessage("Adult allocation amount can not be less than Sold count ");
					return false;
				}
				if (parseInt($("#"+objID+"_infantSeatAllocation").val() ,10) < parseInt(infSoldCount, 10)){
					showERRMessage("INF allocation amount can not be less than Sold count");
					return false;
				}
			}
		}
		
		//to get splited lcc
		if (j.logicalCCCode == logicalCCCode){
			if (parseInt($("#"+objID+"_adultSeatAllocation").val() ,10) < parseInt(adultSoldCount, 10)){
				showERRMessage("Adult allocation amount can not be less than Sold count ");
				return false;
			}
			if (parseInt($("#"+objID+"_infantSeatAllocation").val() ,10) < parseInt(infSoldCount, 10)){
				showERRMessage("INF allocation amount can not be less than Sold count");
				return false;
			}
		}
		
	});	

	if (totalAdultAllocation != parseInt(response.actualAdultCapacity ,10) )
	{
		showERRMessage("Total adult allocation should be equal to actual adult capacity ");
		return false;
	}
	if (totalInfantAllocation != parseInt(response.actualInfantCapacity ,10) )
	{
		showERRMessage("Total infant allocation  should be equal to actual infant capacity ");
		return false;
	}
	return true;
}
manageInvantory.eitherAdultAndInfantNewAllocationIsNotZero =  function(newAdultAllocation, newInfantAllocation){
	return newAdultAllocation != 0 || newInfantAllocation != 0; 
} 

manageInvantory.splitSegmentAlloc = function(){
	var selRow = $("#tblGridSegments").getGridParam('selarrrow');
	if (selRow.length==1){
		var cbutton = $("<input/>").attr({"type":"button", "value":"Cancel", "id":"btnCancel"});
		var sbutton = $("<input/>").attr({"type":"button", "value":"Save", "id":"btnSplit"});
		
		var selSplit = $("<select><option value='0'>Skip spliting if there are overlaping seats</option><option value='2'>Split and override reserved seats</option></select>").attr({"name":"selSplitSeatTemp", "id":"selSplitSeatTemp"});
		sbutton.click(function(){
			manageInvantory.splitSaveAlloc(selRow[0]);
		});
		cbutton.click(function(){
			popClose();
		});
		var  spn1 = $("<span>Split Options : </span>");
		var  spn = $("<span></span>");
		spn1.append(selSplit);
		spn.append(spn1, '<br>', sbutton, cbutton);
		$("#popUp").openMyPopUp({
			"width":525,
			"height":300,
			"appendobj":false,
			"headerHTML":$("<lable>Split Segments</label>"),
			"bodyHTML":manageInvantory.loadSplitAllocation(),
			"footerHTML":spn
		});
		
		loadDataSplitData = function(){
			var reqData = {};
			reqData.flightId = manageInvantory.selectedFlightID;
			reqData.cabinClassCode = manageInvantory.cabinClassCode;
			$.ajax({
	           url: 'logicalCabinClassWiseAllocation!searchLogicalCabinClassAllocations.action',
	           data:  reqData,
	           dataType:"json",
	           complete: function(jsonData,stat){	        	  
	              if(stat=="success") {
	            	  mydata = eval("("+jsonData.responseText+")");
	            	  if (mydata.msgType != "Error"){
	            		  manageInvantory.fillLocalGrid({"id":$("#tblSplitGrid"), "data":mydata.fccInventoryTOs, "editable":false});
	            		  $.data( $("#tblSplitGrid")[0], "SplitResponse", mydata);
	            		  $("#divHint").html(mydata.hint);
	            		  manageInvantory.splitAdultInfantRatio = mydata.splitAdultInfantRatio;
	            		  manageInvantory.logicalCabinClassInventoryRestricted = mydata.logicalCabinClassInventoryRestricted;
	            	  }else{
	            		  alert(mydata.message)
	            	  }
	              }
	           }
	        });
		};
		loadDataSplitData();
		$("#tempDiv").show();
	}else{
		alert("Split Error, Please select one row to split");
	}
};


manageInvantory.setUpPopupDetails = function(height,width) {
	if ((manageInvantory.objTopUpWindow) && (!manageInvantory.objTopUpWindow.closed)) {
		manageInvantory.objTopUpWindow.close();
	}
	width = (width==undefined)?800:width;
	var intLeft = (window.screen.width - width) / 2;
	var intTop = (window.screen.height - height) / 2;
	var strProp = 'toolbar=no,location=no,status=Yes,menubar=no,scrollbars=no,width=' + width + ',height=' + height + ',resizable=no,top='+ intTop + ',left=' + intLeft;
	if ((top[0].objWindow) && (!top[0].objWindow.closed)) {
		top[0].objWindow.close();
	 	top[0].objWindow = window.open("about:blank", "CWindow", strProp);
	} else {
		top[0].objWindow = window.open("about:blank", "CWindow", strProp);
	}
}

manageInvantory.loadRollForward= function(){
	var flightSegmentInventory =  $.data( $("#segmentAllocation")[0], "FullModel"); 
 

	if (flightSegmentInventory == null &&  manageInvantory.selectedFlightObj == null) {
		showCommonError("Error", "No segment inventories for rollforwarding");
	}

   //443538,G90563,SHJ,KBL,Y,06/03/2012,06/03/2012,0,100	
	var hdnFCCInventoryInfo = manageInvantory.selectedFlightObj.flightForSeatAllocationTO.flightId+","
	            + manageInvantory.selectedFlightObj.flightForSeatAllocationTO.flightNumber+","
	            + manageInvantory.selectedFlightObj.flightForSeatAllocationTO.departure+","
	            + manageInvantory.selectedFlightObj.flightForSeatAllocationTO.arrival+","
	            + manageInvantory.cabinClassCode+","
	            + manageInvantory.selectedFlightObj.flightForSeatAllocationTO.departureDateTime+","
	            + manageInvantory.selectedFlightObj.flightForSeatAllocationTO.arrivalDateTime+","
	            + flightSegmentInventory.seatFactorMin+","
	            + flightSegmentInventory.seatFactorMax;

	manageInvantory.setUpPopupDetails(620,800);
	var objAllocationForm = document.getElementById("frmManageSeatAllocate");
	objAllocationForm.target = "CWindow";	
	objAllocationForm.action = "mIShowRollforward.action?hdnFCCInventoryInfo="+hdnFCCInventoryInfo;
	objAllocationForm.submit();
}

manageInvantory.loadSeatCharges = function() {
	if (manageInvantory.selectedFlightObj == null) {
		showCommonError("Error", "No segment inventories for linking seat charges");
	}
	
	manageInvantory.setUpPopupDetails(350, 800);
	modelNo = manageInvantory.selectedFlightObj.flightForSeatAllocationTO.modelNumber;
	var objAllocationForm = document.getElementById("frmManageSeatAllocate");
	objAllocationForm.target = "CWindow";
	objAllocationForm.action = "loadSegments.action?fltId=" + manageInvantory.selectedFlightObj.flightForSeatAllocationTO.flightId	
		+ "&modelNo=" + manageInvantory.selectedFlightObj.flightForSeatAllocationTO.modelNumber;
	objAllocationForm.submit();
}

manageInvantory.loadMealCharges = function() {

	var flightSegmentInventory =  $.data( $("#segmentAllocation")[0], "FullModel"); 

	if (flightSegmentInventory == null &&  manageInvantory.selectedFlightObj == null) {
		showCommonError("Error", "No segment inventories for linking meal Charges");
	}
	
	manageInvantory.setUpPopupDetails(350, 800);

	var objAllocationForm = document.getElementById("frmManageSeatAllocate");
	objAllocationForm.target = "CWindow";

	objAllocationForm.action = "loadMealSegments.action?fltId=" + manageInvantory.selectedFlightObj.flightForSeatAllocationTO.flightId
			+ "&modelNo=" + manageInvantory.selectedFlightObj.flightForSeatAllocationTO.modelNumber + "&cos=" + manageInvantory.cabinClassCode;
	objAllocationForm.submit();
}

manageInvantory.loadBaggageCharges = function() {

	var flightSegmentInventory =  $.data( $("#segmentAllocation")[0], "FullModel"); 

	if (flightSegmentInventory == null &&  manageInvantory.selectedFlightObj == null) {
		showCommonError("Error", "No segment inventories for linking baggage Charges");
	}
	
	manageInvantory.setUpPopupDetails(350, 800);


	var objAllocationForm = document.getElementById("frmManageSeatAllocate");
	objAllocationForm.target = "CWindow";

	objAllocationForm.action = "loadBaggageSegments.action?fltId="
			+ manageInvantory.selectedFlightObj.flightForSeatAllocationTO.flightId + "&modelNo=" + manageInvantory.selectedFlightObj.flightForSeatAllocationTO.modelNumber 
			+ "&cos=" + manageInvantory.cabinClassCode;
	objAllocationForm.submit();
}

manageInvantory.loadBookingClassPrint = function() {	
	
	if (manageInvantory.selectedFlightObj == null) {
		showCommonError("Error", "No segment inventories for print allocations");
	}
	
	manageInvantory.setUpPopupDetails(600, 800);

	var objAllocationForm = document.getElementById("frmManageSeatAllocate");
	objAllocationForm.target = "CWindow";
	
	var hdnFlightSumm = manageInvantory.selectedFlightObj.flightForSeatAllocationTO.flightNumber + '|'
			+ manageInvantory.selectedFlightObj.flightForSeatAllocationTO.departure + '|'
			+ manageInvantory.selectedFlightObj.flightForSeatAllocationTO.departureDateTime + '|'
			+ manageInvantory.selectedFlightObj.flightForSeatAllocationTO.arrival + '|'
			+ manageInvantory.selectedFlightObj.flightForSeatAllocationTO.arrivalDateTime + '|||||||'
			+ manageInvantory.selectedFlightObj.flightForSeatAllocationTO.modelNumber + '|||||@'
		
	objAllocationForm.action = "flightBookingClassPrint.action?fltId="
			+ manageInvantory.selectedFlightObj.flightForSeatAllocationTO.flightId 
			+ "&olFlightId=" + manageInvantory.selectedFlightObj.flightForSeatAllocationTO.overlappingFlightId
			+ "&hdnFlightSumm=" + hdnFlightSumm;
	objAllocationForm.submit();
}

manageInvantory.getSufixFromId = function(id){
	return id.split("_")[1];
};

manageInvantory.showFlightResultonPanel = function(){
	if ( $("#spnFlightSearchResults").css("display") == "none" ){
		$("#spnFlightSearchResults").show();
	}else{
		$("#spnFlightSearchResults").hide();
	}
};

manageInvantory.showRollforwardAudit = function(){
	if ( $("#spnRollForwardAudits").css("display") == "none" ){
		if ($("#tblRollFwdAudit").children().length <= 0)
		{
			$("#tblRollFwdAudit").append('<label style="border: 1px solid; top: -174px; left: -18px; text-align: center; font-size: 12px; background: none repeat scroll 0% 0% white; height: 119px; width: 518px; position: absolute;">No Data Available</label>');
		}
		$("#spnRollForwardAudits").show();
		$("#tblRollFwdAudit").show();
	}else{
		$("#spnRollForwardAudits").hide();
	}
};

manageInvantory.objOnFocus = function() {
	top[2].HidePageMessage();
};

manageInvantory.saveInventories = function(){
	var submit = true;
	
	collectBCInventories = function(id){
		var t = [];
		var segLBCData = $.data( $("#tblGridAllocation_"+id)[0], "dataModel");
		$.each(segLBCData, function(i){
			var tepModel = {};
			var tID = parseInt(i, 10) +1;
			tepModel.FccsbInvId = this.fccsbInvId;
			tepModel.BookingCode = this.bookingCode;
			tepModel.PriorityFlag = $("#tblGridAllocation_"+id).find("#"+tID+"_priorityFlag").val();
			tepModel.SeatsAllocated = $("#tblGridAllocation_"+id).find("#"+tID+"_seatsAllocated").val();
			tepModel.AllocatedWaitListSeats = $("#tblGridAllocation_"+id).find("#"+tID+"_allocatedWaitListSeats").val();
			tepModel.exposedToGDS = this.exposedToGDS;
			tepModel.allocatedWaitListSeats = this.allocatedWaitListSeats;
			tepModel.actualWaitListedSeats = this.actualWaitListedSeats;//sold
			tepModel.soldSeats = this.seatsSold;
			tepModel.onHoldSeats = this.actualSeatsOnHold;
			if(tepModel.actualWaitListedSeats > tepModel.allocatedWaitListSeats){
				showERRMessage("Wait List Allocation Less than Sold WaitListed Seat Count");
				submit = false;
			}
			
			if (tepModel.SeatsAllocated <= 0 && !tepModel.exposedToGDS){
				showERRMessage("BC Allocation Less than Zero or Null");
				submit = false;
			}
			if (tepModel.SeatsAllocated + this.seatsSoldAquiredByNesting < this.actualSeatsSold + this.actualSeatsOnHold){
				showERRMessage("BC Allocation Less than Sold + onhold Seat count in [" + tepModel.BookingCode +  "]");
				submit = false;
			}
			if (tepModel.AllocatedWaitListSeats < 0){
				showERRMessage("Wait List Allocation Less than Zero or Null");
				submit = false;
			}			
//			if (parseFloat(tepModel.AllocatedWaitListSeats) > parseFloat(tepModel.SeatsAllocated)){
//				showERRMessage("Wait List seat allocation can not be greater than Total seat allocation");
//				submit = false;
//			}
			tepModel.StatusClosed = $("#tblGridAllocation_"+id).find("#"+tID+"_statusClosed").val();
			if((this.statusChangeAction!='M' && this.statusChangeAction!='A' && this.statusChangeAction!='L' && this.statusChangeAction!='G' && $("#tblGridAllocation_"+id).find("#"+tID+"_statusClosed").attr("checked")) 
					|| ($("#tblGridAllocation_"+id).find("#"+tID+"_statusClosed").val() == "false" && (this.statusChangeAction == 'A' || this.statusChangeAction == 'L' || this.statusChangeAction == 'G'))){
				tepModel.StatusChangeAction = 'M';
			}else{
				tepModel.StatusChangeAction = this.statusChangeAction;
			}
			tepModel.SeatsAcquired = this.seatsAcquired;
			tepModel.Version = this.version;
			tepModel.Action = this.action;
			t[t.length] = tepModel;
		});
		return t;
	}
	fccSegInventoryJSON = [];
	var segFlightData = $.data( $("#tblGridSegments")[0], "dataModel");
	$.each(segFlightData, function(i){
		var objID = parseInt(i, 10)+1
		var tempModel = {}
		tempModel.FlightId = this.flightId;
		tempModel.FccsInvId = this.fccsInvId;
		tempModel.SegmentCode = this.segmentCode;
		tempModel.LogicalCCCode = this.logicalCCCode;
		tempModel.WaitListedSeats = this.waitListedSeats;
		tempModel.OversellSeats = $("#tblGridSegments").find("#"+objID+"_oversellSeats").val();
		tempModel.CurtailedSeats = $("#tblGridSegments").find("#"+objID+"_curtailedSeats").val();
		tempModel.WaitListSeats = $("#tblGridSegments").find("#"+objID+"_waitListSeats").val();
		tempModel.InfantAllocation = this.infantAllocation;
		tempModel.Version = this.version;		
		tempModel.fccSegBCInventories = collectBCInventories(this.fccsInvId);
		fccSegInventoryJSON[fccSegInventoryJSON.length] = tempModel;
		
		if(tempModel.WaitListedSeats > tempModel.WaitListSeats){
			showERRMessage("Wait List Allocation Less than Sold WaitListed Seat Count");
			submit = false;
		}
	});
	if (submit){
		var data = {};
		data.mode = "SAVE";
		data.fccSegInventoryJSON = JSON.stringify(fccSegInventoryJSON, null);
		$.ajax({
		     url: 'loadFlightAllocation.action',
		     type: 'POST',
		     data:data,
		     dataType: 'json',
		     complete: function(jsonData,stat) {
		    	if(stat=="success") {
	            	mydata = eval("("+jsonData.responseText+")");
	            	if (mydata.msgType != "Error"){
		            	hideERRMessage();
		            	alert(mydata.message);
		            	manageInvantory.loadSeatAllocateAction(manageInvantory.selectedFlightObj);
	            	}else{
	            		alert(mydata.message);
	            	}
	         	}
		     }
		});
	}
};

function winUnLoad(rollForwardAudit){
	
	if (rollForwardAudit != "") {
		var varFlightSumm = rollForwardAudit.split("@");
		var varFlightSummCols = "";
		var auditFlights = new Array();
		// user will be able to see roll forward operations many times without
		// coming back to the original page. So we should not clear the list.
		var currentLength = varFlightSumm.length;
		for ( var i = 0; i < varFlightSumm.length; i++) {
			varFlightSummCols = varFlightSumm[i].split("|");
			
			var flightAudit = new Object;
			var rollFwdAuditTO = {};
			rollFwdAuditTO.flightId = varFlightSummCols[0];
			rollFwdAuditTO.flightNumber = varFlightSummCols[1];
			rollFwdAuditTO.departure = varFlightSummCols[2].substring(0,10);
			rollFwdAuditTO.departureDateTime = varFlightSummCols[2].substring(10);
			rollFwdAuditTO.originAptCode = varFlightSummCols[3];
			rollFwdAuditTO.notes = varFlightSummCols[4];
			rollFwdAuditTO.modelNumber = varFlightSummCols[5];
			rollFwdAuditTO.ccCode = varFlightSummCols[6];
			rollFwdAuditTO.arrival = varFlightSummCols[7].substring(0,10);
			rollFwdAuditTO.arrivalDateTime = varFlightSummCols[7].substring(10);
			
			flightAudit.id = i + 1;
			flightAudit.rollFwdAuditTO = rollFwdAuditTO;
			auditFlights[i] = flightAudit;
		}
		
		setField("flightRollForwardAuditSummary", rollForwardAudit);
		
		var toRollFwdAuditGrid = {"page":1,"rows":auditFlights, "total":1}
		manageInvantory.constructRollFwdAuditGrid(null, toRollFwdAuditGrid);
		manageInvantory.loadSeatAllocate(manageInvantory.selectedFlightObj);
	}
	
};

manageInvantory.constructSeatMovemntOptionsGrid = function(){
	$("#spnSeatMovementOptionInfo").empty();
	var grid = $("<table><tr><td>Agent Options</td></tr></table>").attr({"id": "tblGridSeatMoveOptions","align":"left"});
	$("#spnSeatMovementOptionInfo").append(grid);
	$("#spnSeatMovementOptionInfo").append("<table id ='seatTest'><tbody><tr><td>Agent Options</td></tr><tbody></table>")
	
};

manageInvantory.constructSeatMovementGrid = function(dataModel, lccID){
	$("#spnSeatMovementsInfo_"+lccID).empty();
	var gridTable = $("<table></table>").attr({"id": "tblGridSeatMove_"+lccID,"align":"left"});
	$("#spnSeatMovementsInfo_"+lccID).append(gridTable);
	var temGrid = $("#tblGridSeatMove_"+lccID).jqGrid({
		datatype:"local",
		height: 200,
		width: 650,
		colNames:[ 'Channel/Agent Name', 'Sold<br> AD/CH/INF' ,'Onhold<br> AD/CH/INF', 'Cancelled<br> AD/CH/INF', 'Total Sold<br> AD/CH/INF', 'Total Onhold<br> AD/CH/INF', 'Total Cancelled<br> AD/CH/INF'],
		colModel:[
		    {name:'agentName', index:'agentName', width:80 },
			{name:'soldSeats',  index:'soldSeats', width:80  },
			{name:'onholdSeats', index:'onholdSeats', width:80, align:"center"},
			{name:'cancelledSeats', index:'cancelledSeats', width:80, align:"center"},
			{name:'soldSeatsSum', index:'soldSeatsSum', width:80, align:"center"},
			{name:'onholdSeatsSum', index:'onholdSeatsSum',  width:80, align:"center"},
			{name:'cancelledSeatsSum', index:'cancelledSeatsSum', width:80, align:"center"}
		],
		imgpath: "",
		multiselect: false,
		viewrecords: true,
		altRows:true,
		altclass:"GridAlternateRow",
		rowNum:20, 
		jsonReader: { 
			root: "rows",
			page: "page",
			total: "total",
			records: "records",
			repeatitems: false,
			id: "0" 
		},
		onSelectRow: function(rowid){

		}
	});
	manageInvantory.fillLocalGrid({"id":temGrid, "data":dataModel, "editable":false});
};

$(function() {
	manageInvantory.ready();
});