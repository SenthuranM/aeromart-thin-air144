$(function() {
	
	//setting common JQ Grid Properties
	$.extend($.jgrid.defaults,{
		loadui:'disable',
		mtype:'POST',
		onPaging:  function (e){
			if(Number(this.page) > Number(this.lastpage) ){
				UI_Common.displayMsgObj.message = "Record number should be less than the total number of records.";
				UI_Common.displayMsgObj.msgType = "Error";
				UI_Common.displayMessage(UI_Common.displayMsgObj);
				this.page = "1";				
				
			}
		},		
		loadError: function(requestObj, status, error){
			handleCommonAjaxInvocationErrors(requestObj);
		},
		beforeSelectRow: function (rowID,e){
			return true;
		}

	});
	
	//setting up progress bar for ajax events
	$(document).ajaxSend(function(){		
		  top[2].ShowProgress();
	});

	$(document).ajaxStop(function(){	
		top[2].HideProgress();
	});
	
	$(document).ajaxError(function(){	
		top[2].HideProgress();
	});
	
	$(document).ajaxSuccess(function(){	
		//top[0].$('.progressBar').hide();
	});	
	
});