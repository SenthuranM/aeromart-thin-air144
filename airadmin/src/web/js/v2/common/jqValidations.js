/**
 * Common JQuery based Validations
 * @author Navod Ediriweera
 */	



function JqValidations (){}

JqValidations.kpAlphaNumericWhiteSpaceUnderscore = function(control){
	var strCC = $('#'+control).val();
	var strLen = strCC.length;
	var blnVal = isAlphaNumericWhiteSpaceUnderscore(strCC);
	if (!blnVal) {
		$('#'+control).val(strCC.substr(0, strLen - 1));
		$('#'+control).focus();
	}
}

JqValidations.kpAlphaNumeric = function(control) {
	var strCC = $('#'+control).val();
	var strLen = strCC.length;
	var blnVal = isAlphaNumeric(strCC);
	if (!blnVal) {
		$('#'+control).val(strCC.substr(0, strLen - 1));
		$('#'+control).focus();
	}
}

JqValidations.kpNumeric = function(control) {
	var strCC = $('#'+control).val();
	var strLen = strCC.length;
	var blnVal = isInt(strCC);
	if (!blnVal) {
		$('#'+control).val(strCC.substr(0, strLen - 1));
		$('#'+control).focus();
	}
}

JqValidations.kpAlphaNumericWhiteSpaceUnderscoreDot = function(control){
	var strCC = $('#'+control).val();
	var strLen = strCC.length;
	var blnVal = JqValidations.isAlphaNumericWhiteSpaceUnderscoreDot(strCC);
	if (!blnVal) {
		$('#'+control).val(strCC.substr(0, strLen - 1));
		$('#'+control).focus();
	}
}

JqValidations.isAlphaNumericWhiteSpaceUnderscoreDot = function(s){return RegExp("^[a-zA-Z0-9_.\& \w\s]+$").test(s);}

