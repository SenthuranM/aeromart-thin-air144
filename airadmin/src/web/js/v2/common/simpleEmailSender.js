function UI_EmailSender(){}
UI_EmailSender.errorCallback = null;
UI_EmailSender.successCallback = null;
UI_EmailSender.prepend = null;
UI_EmailSender.subject = null;

UI_EmailSender.displayForm = function(params){
	if(params != null){

		UI_EmailSender.prepend = params.prepend;
		UI_EmailSender.subject = params.subject;

		var strAppend = "<span id='spnEmail' style='position:absolute;top:-80px;left:20px;z-index:2500;width:480px;height:200px;'>";
		strAppend +=	"<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
		strAppend +=	"<tr>";
		strAppend +=	"<td valign='top'>";
		strAppend +=	"<table width='100%' cellpadding='0' cellspacing='0' border='0' align='center' ID='Table4'>";
		strAppend +=	"<tr>";
		strAppend +=	"<td width='10' height='20'><img src='../../images/form_top_left_corner_no_cache.gif'><\/td>";
		strAppend +=	"<td colspan='2' height='20' class='FormHeadBackGround'><img src='../../images/bullet_small_no_cache.gif'> <font class='FormHeader'>";
		strAppend +=	"Send Email<\/font><\/td>";
		strAppend +=	"<td width='11' height='20'><img src='../../images/form_top_right_corner_no_cache.gif'><\/td>";
		strAppend +=	"<\/tr>";

		strAppend +=	"<tr>";
		strAppend +=	"<td class='FormBackGround'><\/td>";
		strAppend +=	"<td class='FormBackGround' valign='top' style='height:160px;' colspan='2'>";
		strAppend +=	UI_EmailSender.constructContent();
		strAppend +=	"</td>";
		strAppend +=	"<td class='FormBackGround'></td>";
		strAppend +=	"</tr>";
		strAppend +=	"<tr>";

		strAppend +=	"<tr>";
		strAppend +=	"<td class='FormBackGround'><\/td>";
		strAppend +=	"<td class='FormBackGround' align='left'>";
		strAppend +=	"<input name='btnCancel' type='button' class='button' id='btnCancel' value='Cancel' onClick='UI_EmailSender.distroy();'>";
		strAppend +=	"</td>";
		strAppend +=	"<td class='FormBackGround' align='right'>";
		strAppend +=	"&nbsp;<input name='btnSend' type='button' class='button' id='btnDone' value='Send' onClick='UI_EmailSender.sendEmail();'>";
		strAppend +=	"</td>";
		strAppend +=	"<td class='FormBackGround'></td>";
		strAppend +=	"</tr>";

		strAppend +=	"<tr>";
		strAppend +=	"<td height='12'><img src='../../images/form_bottom_left_corner_no_cache.gif'><\/td>";
		strAppend +=	"<td height='12' colspan='2' class='FormBackGround'><img src='../../images/spacer_no_cache.gif' width='100%' height='1'><\/td>";
		strAppend +=	"<td height='12'><img src='../../images/form_bottom_right_corner_no_cache.gif'><\/td>";
		strAppend +=	"<\/tr>";

		strAppend +=	"<\/table>";
		strAppend +=	"<\/td>";
		strAppend +=	"<\/table>";
		strAppend +=	"<\/span>";
		$("#"+params.divName).append(strAppend);
		UI_EmailSender.errorCallback = params.errorCallback;
		UI_EmailSender.successCallback = params.successCallback;		
	} else {
		alert("incorrect parameters");
	}
}

UI_EmailSender.constructContent = function(){

	var strAppend = "<table width='100%' border='0' cellpadding='1' cellspacing='0'>";	
	strAppend +=	"	<tr>";
	strAppend +=	"		<td colspan='2' style='height:5px;'><\/td>";
	strAppend +=	"	<\/tr>";
	strAppend +=	"	<tr>";
	strAppend +=	"		<td>";
	strAppend +=	"			<font>To :</font>";
	strAppend +=	"		<\/td>";
	strAppend +=	"		<td>";
	strAppend +=	"			<input type='text' id='txtToAddress' name='txtToAddress' size='45'><font class='mandatory'>&nbsp;*</font>";
	strAppend +=	"		<\/td>";
	strAppend +=	"	<\/tr>";

	var subjectContent = "";
	if(UI_EmailSender.subject) {
		subjectContent = UI_EmailSender.subject;
	}

	strAppend +=	"	<tr>";
	strAppend +=	"		<td>";
	strAppend +=	"			<font>Subject :</font>";
	strAppend +=	"		<\/td>";
	strAppend +=	"		<td>";
	strAppend +=	"			<input type='text' id='txtSubject' name='txtSubject' size='45' value='" + subjectContent + "'><font class='mandatory'>&nbsp;*</font>";
	strAppend +=	"		<\/td>";
	strAppend +=	"	<\/tr>";
	strAppend +=	"	<tr>";
	strAppend +=	"		<td>";
	strAppend +=	"			<font>&nbsp;</font>";
	strAppend +=	"		<\/td>";

	var textAreaContent = "";
	if(UI_EmailSender.prepend) {
		textAreaContent = UI_EmailSender.prepend;
	}

	strAppend +=	"		<td>";
	strAppend +=	"			<textarea name='txtMsgBody' id='txtMsgBody' cols='25' rows='6' style='height:100px;width:360px;' onkeyUp='' onkeyPress='' title='Can enter only up to 255 charactors'>" + textAreaContent + "</textarea>";
	strAppend +=	"		<\/td>";
	strAppend +=	"	<\/tr>";
	strAppend +=	"<\/table>";
	
	return strAppend;
}

UI_EmailSender.distroy = function(){
	$('#spnEmail').remove();
}

UI_EmailSender.sendEmail = function() {
	var valiMessage = UI_EmailSender.validateParams();
	if(valiMessage == null) {
		var data = {};
		data['toAddress'] = $('#txtToAddress').val();
		data['subject'] = $('#txtSubject').val();
		data['content'] = $('#txtMsgBody').val();
		data['strHdnMode']	= 'EMAIL';
		
		$.ajax(
			{ dataType: 'json', 
			  data:data, 
			  type : 'POST',
			  url:"showFlaAlertEmail.action",
			  success: UI_EmailSender.emailSentSuccess });
	} else {
		UI_EmailSender.displayError(valiMessage);
	}
}

UI_EmailSender.emailSentSuccess = function(response){
	if(response.success) {
		UI_EmailSender.displaySuccess("Email Successfully Sent.");
		UI_EmailSender.distroy();
	} else {
		UI_EmailSender.displayError(response.errorMessage);
	}
}

UI_EmailSender.displaySuccess = function(message){
	if(UI_EmailSender.successCallback != null){
		UI_EmailSender.successCallback.call(this, message);
	} else {
		alert(message);
	}	
}

UI_EmailSender.displayError = function(message){
	if(UI_EmailSender.errorCallback != null){
		UI_EmailSender.errorCallback.call(this, message);
	} else {
		alert(message);
	}	
}

UI_EmailSender.validateParams = function (){
	if (isEmpty(getText("txtToAddress"))) {
		return "Email address is required.";
	}if($("#txtSendTo").val() != null) {
		var emailFilter=/^[A-Z0-9\._%-]+@[A-Z0-9\.-]+\.[A-Z]{2,4}(?:[,;][A-Z0-9\._%-]+@[A-Z0-9\.-]+\.[A-Z]{2,4})*$/i;
		if(!emailFilter.test($("#txtSendTo").val())){
			return "Please enter correct email adress. Email adresses should be comma seperated."
		}
	} 
	if (isEmpty(getText("txtSubject"))) {
		return "Subject is required.";
	}
	return null;
}