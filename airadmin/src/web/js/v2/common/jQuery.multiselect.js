;(function($) {

    $.fn.multiSelect = function(settings) {
        //var optData = null;
        $.fn.leftSelectID = null;
        $.fn.rightSelectID = null;
        $.fn.leftData =  null;
        $.fn.rightData =  [];
        $.fn.rightWidth =  "170px";
        $.fn.leftWidth =  "170px";
        var eleLabel = null;
        return this.each(function() {

            $.fn.leftSelectID = settings.leftListBox;
            $.fn.rightSelectID = settings.leftListBox+"Assigned";
            eleLabel = settings.label;
            if(settings.rightWidth !=null){
                $.fn.rightWidth = settings.rightWidth;
            }
            if(settings.leftWidth !=null){
                $.fn.leftWidth = settings.leftWidth;
            }
            var addHtml = "";
            addHtml +=	'<table cellspacing="0" cellpadding="0" border="0" width="300px" class="fntDefault" id="Table6">'+
                '<tr><td><font><b>&nbsp;&nbsp;'+eleLabel+'</b></font></td><td><font><b>&nbsp;</b></font></td><td><font><b>&nbsp;&nbsp;Selected '+eleLabel+'</b></font></td></tr>'+
                '<tr>'+
                '<td align="center" valign="middle">'+
                '	<select id="'+$.fn.leftSelectID+'" class="ListBox" style="width:'+$.fn.leftWidth+'; height: 100px;" multiple=""></select>'+
                '</td>'+
                '<td align="center" width="45" style="height: 55px;">'+
                '<table cellspacing="0" cellpadding="0" border="0" align="center" width="80px" id="selectOptions'+$.fn.leftSelectID+'">'+
                '	<tr>'+
                '		<td align="center">'+
                '			<input type="button" style="width: 45px; height: 20px;" id="addAllRight_'+$.fn.leftSelectID+'" value="&gt;&gt;" class="Button navButton">'+
                '		</td>'+
                '	</tr>'+
                '	<tr>'+
                '		<td align="center">'+
                '			<input type="button" style="width: 45px; height: 20px;" id="addRight_'+$.fn.leftSelectID+'" value="&gt;" class="Button navButton">'+
                '		</td>'+
                '	</tr>'+
                '	<tr>'+
                '		<td align="center">'+
                '			<input type="button" style="width: 45px; height: 20px;" id="addLeft_'+$.fn.leftSelectID+'" value="&lt;" class="Button navButton">'+
                '		</td>'+
                '	</tr>'+
                '	<tr>'+
                '		<td align="center">'+
                '			<input type="button" style="width: 45px; height: 20px;" id="addAllLeft_'+$.fn.leftSelectID+'" value="&lt;&lt;" class="Button navButton">'+
                '		</td>'+
                '	</tr>'+
                '</table>'+
                '</td>'+
                '<td align="center" valign="middle">'+
                '<select id="'+$.fn.rightSelectID+'" class="ListBox" style="width:'+$.fn.rightWidth+'; height: 100px;" multiple=""></select>'+
                '</td>'+
                '</tr>'+
                '</table>';

            $(this).after(addHtml);
            var timeOut = 0;
            $("#selectOptions"+$.fn.leftSelectID).find(".navButton").unbind("click").bind("click", function(){
                var prefix = this.id.split("_")[0]
                $.fn.leftSelectID = this.id.split("_")[1];
                $.fn.rightSelectID = $.fn.leftSelectID+"Assigned";
                btnDisabled();
                timeOut = setTimeout(function(){
                    //optData = getOptionData();
                    if(prefix == "addAllRight"){
                        allRight();
                    }else if(prefix == "addRight"){
                        oneRight();
                    }else if(prefix == "addLeft"){
                        oneLeft();
                    }else if(prefix == "addAllLeft"){
                        allLeft();
                    }
                }, 10);
            });

            function getListData(id,dataLabel){
                return $.data($("#"+id)[0], dataLabel);
            }
            function setListData(id, dataLabel ,data){
                $.data($("#"+id)[0], dataLabel, data);
            }

            function btnDisabled(){
                $("#selectOptions"+$.fn.leftSelectID).find(".navButton").css("color","#666").disable();
            }
            function btnEnable(){
                $("#selectOptions"+$.fn.leftSelectID).find(".navButton").css("color","#000").enable();
                clearTimeout(timeOut);
            }
            $(this).remove();
            //var optData = settings.data;
            setListData(settings.leftListBox, "listData", settings.data);
            //var groupFlg = settings.grouped;
            setListData(settings.leftListBox, "grouped", settings.grouped);
            /*getOptionData = function(){
             dataOption = {};
             $("#" +$.fn.leftSelectID ).find("option").each(function(i,j){
             dataOption[i] = [j.value, j.value]
             });
             return dataOption;
             };*/
            emptyAll();


            allRight = function() {
                emptyAll();
                var optData = getListData($.fn.leftSelectID, 'listData');
                var groupFlg = getListData($.fn.leftSelectID, 'grouped');
                $("#" + $.fn.rightSelectID).fillDropDown({dataArray:optData, keyIndex:0, valueIndex:1, firstEmpty:false, grouped:groupFlg});
                $.fn.rightData = optData;
                btnEnable();
            };

            allLeft = function() {
                emptyAll();
                var optData = getListData($.fn.leftSelectID, 'listData');
                var groupFlg = getListData($.fn.leftSelectID, 'grouped');
                $("#" + $.fn.leftSelectID).fillDropDown({dataArray:optData, keyIndex:0, valueIndex:1, firstEmpty:false, grouped:groupFlg});
                $.fn.leftData = optData;
                btnEnable();
            };

            oneRight = function() {
                var selectedLeft = $("#" + $.fn.leftSelectID).val();
                if (selectedLeft != null && selectedLeft != "") {
                    oneLeftToRight(selectedLeft);
                }
                btnEnable();
            };

            oneLeft = function() {
                var selectedLeft = $("#" + $.fn.rightSelectID).val();
                if (selectedLeft != null && selectedLeft != "") {
                    oneRightToLeft(selectedLeft);
                }
                btnEnable();
            };

            $.fn.multiSelect.getRightData = function(id) {
                if(id!=undefined){
                    $.fn.leftSelectID = id;
                    $.fn.rightSelectID = $.fn.leftSelectID+"Assigned";
                }
                var listData = new Array();
                $("#"+$.fn.rightSelectID + " option").each(function()
                {
                    listData.push($(this).val());
                });
                return listData;
            };

            $.fn.multiSelect.getLeftData = function(id) {
            	if(id!=undefined){
                    $.fn.leftSelectID = id;
                }
                var listData = new Array();
                $("#"+$.fn.leftSelectID + " option").each(function()
                {
                    listData.push($(this).val());
                });
                return listData;
            };

            $.fn.multiSelect.loadRight = function(arrIDS, id) {
                emptyAll();
                if(id!=undefined){
                    $.fn.leftSelectID = id;
                    $.fn.rightSelectID = $.fn.leftSelectID+"Assigned";
                }
                var optData = getListData($.fn.leftSelectID, 'listData');
                var groupFlg = getListData($.fn.leftSelectID, 'grouped');
                $.fn.leftData = jQuery.extend(true, [], optData);
                $.fn.rightData=[];
                if (groupFlg){
                    //Loop leftData obj
                    for (var t=0;t<$.fn.leftData.length;t++){
                        $.each($.fn.leftData[t], function(k, L){
                            var tempObj = {}, arr = [];
                            if (arrIDS!=null){
                                for (var i = 0; i < arrIDS.length; i++) {
                                    if (arrIDS[i]!=""){
                                        for (var arI = 0; arI < L.length; arI++) {
                                            if (arrIDS[i]==L[arI][0]){
                                                arr[arr.length] = L[arI];
                                                L.splice(arI, 1);
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                            tempObj[k] = arr;
                            if (arr.length!=0){
                                $.fn.rightData[$.fn.rightData.length] = tempObj;
                            }
                        });
                    }
                    //remove un necessary data
                    var tempLeftData = [];
                    for (var t=0;t<$.fn.leftData.length;t++){
                        $.each($.fn.leftData[t], function(k, L){
                            if (!$.isEmptyObject($.fn.leftData[t]) && $.fn.leftData[t][k].length!=0){
                                tempLeftData[tempLeftData.length] = $.fn.leftData[t];
                            }
                        });
                    }
                    $.fn.leftData = tempLeftData;
                    //fill left and right
                    $("#" +$.fn.leftSelectID ).fillDropDown({dataArray:$.fn.leftData, keyIndex:0, valueIndex:1, firstEmpty:false, grouped:groupFlg});
                    sortSelect("#" + $.fn.leftSelectID);
                    $("#" + $.fn.rightSelectID).fillDropDown({dataArray:$.fn.rightData, keyIndex:0, valueIndex:1, firstEmpty:false, grouped:groupFlg});
                    sortSelect("#" + $.fn.rightSelectID);
                }else{
                    //fill left and right
                    $("#" +$.fn.leftSelectID ).fillDropDown({dataArray:$.fn.leftData, keyIndex:0, valueIndex:1, firstEmpty:false, grouped:groupFlg});
                    sortSelect("#" + $.fn.leftSelectID);
                    if (arrIDS != null) {
                        for (var i = 0; i < arrIDS.length; i++) {
                            var selectIDS = $.fn.multiSelect.getGroupData(arrIDS[i]);
                            $("#" + $.fn.leftSelectID +" option[value='" + selectIDS[0] +"']").attr("selected", true);
                            oneLeftToRight(selectIDS);
                        }
                        sortSelect("#" + $.fn.rightSelectID);
                    }
                }

            };



            $.fn.multiSelect.getGroupData = function(itemID) {
                var groupData = [];
                var rootID = null;
                var optData = getListData($.fn.leftSelectID, 'listData');
                for (var i = 0; i < optData.length; i++) {
                    rootID = optData[i][0].split(":");
                    if (rootID[1] == itemID || (rootID.length == 1 && rootID[0] == itemID)) {
                        groupData[0] = optData[i][0];
                        break;
                    }
                }
                return groupData;
            };


            function oneLeftToRight(selectedIDS) {
                var tempID = null;
                var rootID = null;
                var root = "";
                for (var i = 0;  i < selectedIDS.length; i++) {
                    if (selectedIDS[i].indexOf(":") > -1){
                        rootID = selectedIDS[i].split(":");
                        if (rootID[0] == rootID[1]) {
                            addGroup(rootID[0] , "toleft");
                        } else if (rootID[0] != null) {
                            addSingle(rootID[0], rootID[1], "toleft");
                        }
                    }else{
                        addSingle(null, null, "toleft");
                    }
                }
                // Root IDS
            };

            function oneRightToLeft(selectedIDS) {
                var tempID = null;
                var rootID = null;
                var root = "";
                for (var i = 0;  i < selectedIDS.length; i++) {
                    if (selectedIDS[i].indexOf(":") > -1){
                        rootID = selectedIDS[i].split(":");
                        if (rootID[0] == rootID[1]) {
                            addGroup(rootID[0], "toright");
                        } else if (rootID[0] != null) {
                            addSingle(rootID[0], rootID[1], "toright");
                        }
                    }else{
                        addSingle(null, null, "toright");
                    }
                }
                // Root IDS
            };

            function addGroup (groupID,dir){
                var from = (dir.toLowerCase() == "toleft") ? $.fn.leftSelectID : $.fn.rightSelectID;
                var to = (dir.toLowerCase() == "toleft") ? $.fn.rightSelectID : $.fn.leftSelectID;
                $.each( $("#" + to).find("option"), function(j,valJ){
                    if (groupID == valJ.value.split(":")[0] && groupID == valJ.value.split(":")[1]){
                        $(valJ).remove();
                    }
                });
                $.each( $("#" + from).find("option"), function(i,val){
                    if (groupID == val.value.split(":")[0]){
                        $("#" + to).append($(val).clone());
                        $(val).remove();
                    }
                });
                sortSelect("#" + to);
            };

            function addSingle (groupID, item, dir){
                var from = (dir.toLowerCase() == "toleft") ? $.fn.leftSelectID : $.fn.rightSelectID;
                var to = (dir.toLowerCase() == "toleft") ? $.fn.rightSelectID : $.fn.leftSelectID;
                var optToArray = $("#" + to).find("option");
                if (groupID != null){
                    if (optToArray.length == 0){
                        $("#" + to).append('<option value="'+groupID+':'+groupID+'">'+groupID+'</option>');
                        $("#" + to).append($("#" + from).find("option:selected").clone());
                        $("#" + from).find("option:selected").remove();
                    }else{
                        var groupExist = false;
                        for (var i = 0; i < optToArray.length; i++){
                            if (groupID == optToArray[i].value.split(":")[0]){
                                groupExist = true;
                                break;
                            }
                        }
                        if (groupExist){
                            $("#" + to).append($("#" + from).find("option:selected").clone());
                            $("#" + from).find("option:selected").remove();
                        }else{
                            $("#" + to).append('<option value="'+groupID+':'+groupID+'">'+groupID+'</option>');
                            $("#" + to).append($("#" + from).find("option:selected").clone());
                            $("#" + from).find("option:selected").remove();
                        }
                    }
                    var optFromArray = $("#" + from).find("option");
                    if (optFromArray.length > 0){
                        var itemCount = 0;
                        for (var i = 0; i < optFromArray.length; i++){
                            if (groupID == optFromArray[i].value.split(":")[0]){
                                itemCount++;
                            }
                        }
                    }
                    if (itemCount<=1){
                        $.each( $("#" + from).find("option"), function(i,val){
                            if (groupID == val.value.split(":")[0]){
                                $(val).remove();
                            }
                        });
                    }
                }else{
                    $("#" + to).append($("#" + from).find("option:selected").clone());
                    $("#" + from).find("option:selected").remove();
                }
                sortSelect("#" + to);
            };

            function sortSelect(selectToSort) {

                myOpt = $(selectToSort).find("option");
                var optVals = [], keys = [];
                $.each(myOpt, function(){
                    var tval=this.value.split(":");
                    if(tval[0]===tval[1]){
                        keys[keys.length] = tval[0];
                        var tadd = new Array();
                        tadd[0] = this.value;
                        tadd[1] = this.text;
                        optVals[optVals.length] = tadd;
                    }else{
                        var tadd = new Array();
                        tadd[0] = this.value;
                        tadd[1] = this.text;
                        optVals[optVals.length] = tadd;
                    }
                });
                keys.sort();
                $(selectToSort).empty();
                if (keys.length==0){
                    optVals.sort();
                    $.each(optVals, function(j, Y){
                        $(selectToSort).append(new Option(Y[1], Y[0]));
                    });
                }else{
                    $.each(keys, function(i,X){
                        $.each(optVals, function(j, Y){
                            if(X===Y[0].split(":")[0]){
                                $(selectToSort).append(new Option(Y[1], Y[0]));
                            }
                        });
                    });
                }
                //NOTE: custom sorting is not supporting in Chrome
                /*myOpt.sort(function(a,b){
                 var ta = a.value.split(":")[0].toLowerCase(), tb = b.value.split(":")[0].toLowerCase();
                 //var ta = a.value.substr(0,5), tb = b.value.substr(0,5);
                 return ta < tb ? -1 : ta > tb ? 1 : 0;
                 });*/
                //$(selectToSort).empty().append( myOpt );
            };

            function emptyAll() {
                $("#"+ $.fn.leftSelectID).empty();
                $("#"+ $.fn.rightSelectID).empty();
            };
        });
    };

})(jQuery);
