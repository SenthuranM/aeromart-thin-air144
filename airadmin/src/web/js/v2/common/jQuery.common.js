	/*
	*********************************************************
		Description		: Common prototype routings
		Author			: Rilwan A. Latiff
		Version			: 1.2
		Created         : 1st June 2005
		Last Modified	: 25th August 2009
		Modified by		: Baladewa Welathanthri
    ---------------------------------------------------------
    Ver 1.0 
    *********************************************************	
	*/
	
	/* Disable Any Control */
	$.fn.disable = function(t) {
		if (t == "empty")
			$(this).val("");
		$(this).attr('disabled', true);
	}
	
	/* Enable Any control */
	$.fn.enable = function(t) {
		if (t == "empty")
			$(this).val("");
		$(this).attr('disabled', false);
	}
	
	/* Read only Any Control */
	$.fn.readOnly = function(blnStatus) {$(this).attr('readOnly', blnStatus);}
	
	/* Enable Disable */
	$.fn.disableEnable = function(blnStatus) {
		if (blnStatus){$(this).disable();}else{$(this).enable();}
	}
	
	/* Reset Form Data */
	$.fn.extend({reset: function(){
		return this.each(function() {
				try{
					$(this).is('form') && this.reset();
				}catch(ex){}
			});
		}
	});
	
	/* Set Tab Index Any control */
	$.fn.setTabIndex = function(intIndex) {$(this).attr('tabindex', intIndex);}
	
	/* Set Form Action */
	$.fn.action = function(strFileName) {$(this).attr('action', strFileName);}
	
	/* Alpha Numeric Only Allowed */
	$.fn.alphaNumeric = function(p) { 
		p = $.extend({paste: true}, p);
		$(this).keypress(function (e){
			if ((e.which != 8) && (e.which != 0) && (e.which != 13)){
				var c = String.fromCharCode(e.which);
				if (!isAlphaNumeric(c)){e.preventDefault();};
				if (!p.paste){if (e.ctrlKey&&c=='v') e.preventDefault();}
			};
		});
	};
	  
	/* Alpha Only allowed */
	$.fn.alpha = function(p) { 
		p = $.extend({paste: true}, p);
		$(this).keypress(function (e){
			if ((e.which != 8) && (e.which != 0) && (e.which != 13)){
				var c = String.fromCharCode(e.which);
				if (!isAlpha(c)){e.preventDefault();};
				if (!p.paste){if (e.ctrlKey&&c=='v') e.preventDefault();}
			};
		});
	};
	
	/* Numeric Only Allowed */
	$.fn.numeric = function(p){
		return this.each(function() {
			$(this).keypress(function (e){
				var charCode = (e.which>=0) ? e.which : e.keyCode;
				if (p != undefined && p.allowDecimal == true){
				  if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) {
					  return false;
				  }
				  if($(this).val().indexOf(".") > -1 && e.which == 46){
					  return false; 
				  }
				  //if($(this).val().indexOf(".") > -1 && $(this).val().split(".")[1].length > 1 && charCode != 8 && charCode != 46 && charCode != 37 && charCode != 38 && charCode != 39 && charCode != 40){
					//  return false; 
				  //}
				}else{
					if (charCode > 31 && (charCode < 48 || charCode > 57)) {
					    return false;
					 }
				}
			});
		});
	};
	
  
	/* Alpha White Space allowed */
	$.fn.alphaWhiteSpace = function(p) {
		p = $.extend({paste: true}, p); 
		$(this).keypress(function (e){
			if ((e.which != 8) && (e.which != 0) && (e.which != 13)){
				var c = String.fromCharCode(e.which);
				if (!isAlphaWhiteSpace(c)){e.preventDefault();};
				if (!p.paste){if (e.ctrlKey&&c=='v') e.preventDefault();}
			};
		});
	};
	//show hide any jquery tab 
	$.fn.showHideTabs = function(status,tabTopSufix){
		if (status){
			//$(this).show();
			$(this.selector+tabTopSufix).show();
		}else{
			//$(this).hide();
			$(this.selector+tabTopSufix).hide();
		}
	};
	
	/* Alpha Numeric White space Allowed */
	$.fn.alphaNumericWhiteSpace = function(p) { 
		p = $.extend({paste: true}, p);
		$(this).keypress(function (e){
			if ((e.which != 8) && (e.which != 0) && (e.which != 13)){
				var c = String.fromCharCode(e.which);
				if (!isAlphaNumericWhiteSpace(c)){e.preventDefault();};
				if (!p.paste){if (e.ctrlKey&&c=='v') e.preventDefault();}
			};
		});
	};
	
	/* 
	 * This method prevents only '~' and ','
	 * The method calls on FareClassValidations.js
	 */
	$.fn.SpecialCharactersPrevent = function(p) { 
		p = $.extend({paste: true}, p);
		$(this).keypress(function (e){
			if ((e.which != 8) && (e.which != 0) && (e.which != 13)){
				var c = String.fromCharCode(e.which);
				if ((e.which == 44) || (e.which == 126)){e.preventDefault();};
				if (!p.paste){if (e.ctrlKey&&c=='v') e.preventDefault();}
			};
		});
	};
	
	
	/* get the HTML Popup  Properties  */

	$.fn.openMyPopUp =  function (p){
		var el = $(this)
		var winHeight = $(window).height();
		var winWidth = $(window).width();
		var config = {
			position:"fixed",
			zindex: 1001,
			width:300,
			height: 300,
			topPoint: 0,
			leftPoint: 0,
			closeImg:"../../images/Close_no_cache.jpg",
			appendobj:true,
			headerHTML:"Header",
			bodyHTML:"Body",
			footerHTML:""
		}
		p = $.extend(config, p);
		var pH = $("<div></div>").addClass("headerCont");
		var headerLabel = $("<td></td>").addClass("headeritems");
		var ingClose = $("<img src='"+p.closeImg+"' alt='close'/>").click(function(){popClose();});
		var headerClose = $("<td></td>").attr({"width":20, "align":"right"});
		headerClose.append(ingClose);
		var ptr = $("<tr></tr>");
		ptr.append(headerLabel, headerClose)
		var pClose = $("<table></table>").attr({"cellpadding":0,"cellspacing":0, "border":0, "width":"100%"})
		pClose.append(ptr);
		var pB = $("<div></div>").addClass("bodyitems");
		var pF = $("<div></div>").addClass("footeritems");
		pH.append(pClose);
		el.append(pH, pB, pF);
		$("#newpage-mask").remove();
   		$("body").append('<div id="newpage-mask"></div>');
		$("#newpage-mask").css({
			position: 'absolute',
			zIndex: 1000,
			top: '0px',
			left: '0px',
			width: '100%',
			height: $(document).height(),
			backgroundColor:'#000',
			opacity:0.8
		});
		$(this).css({
			position:p.position,
			zIndex: p.zindex,
			top: (p.topPoint == 0) ? (winHeight / 2 ) - (p.height / 2) : p.topPoint,
			left: (p.leftPoint == 0) ? (winWidth / 2 ) - (p.width / 2) : p.leftPoint,
			width:p.width
		});
		$(this).find(".bodyitems").css("height",p.height - 45);
		if (p.appendobj){
			$(this).find(".headeritems").append(p.headerHTML);
			$(this).find(".bodyitems").append(p.bodyHTML);
			$(this).find(".footeritems").append(p.footerHTML);
		}else{
			$(this).find(".headeritems").html(p.headerHTML);
			$(this).find(".bodyitems").html(p.bodyHTML);
			$(this).find(".footeritems").html(p.footerHTML);
		}

		$(this).show();
		popClose = function(){
			$("#newpage-mask").remove();
			el.empty();
			el.hide();
		}
	}
	$.fn.closeMyPopUp = function(){
		$("#newpage-mask").remove();
		$(this).hide()
	}
	
	/* get the Popup Window Properties  */
	$.fn.getPopWindowProp =  function (intHeigh, intWidth){
		var intTop 	= (window.screen.height - intHeigh) / 2;
		var intLeft = (window.screen.width -  intWidth) / 2;
		var strScroll = "no";
		if (arguments.length > 2){
			if (arguments[2] == true){
				strScroll = "yes";
			}
		}
		var strProp = 'toolbar=no,' +
		              'location=no,' +
		              'status=no,' +
		              'menubar=no,' +
		              'scrollbars=' + strScroll + ','+
		              'width=' + intWidth + ',' +
		              'height=' + intHeigh + ',' +
		              'resizable=no,' +
		              'top=' + intTop +',' +
		              'left=' + intLeft
		return strProp
	}
	//Right Click Disable
	
	$(document).bind("contextmenu",function(e){
        return false;
 	});
 	$(document).keydown(function(e) {
	  /*if (e.keyCode === 8 && (e.target.type != "text" && e.target.tagName != "TEXTAREA"))
	  {
	     return false;
	  }*/
	});
 	
 	//Tab Foucs has to give tab with elemet ID tabID#elementID
 	$.fn.tabFocus = function(tabsID){
 		if (this.selector!=undefined && tabsID!=undefined){
	 		var t = this.selector.split("#");
	 		$("#"+tabsID).tabs( "option", "active", "#"+t[0] );
	 		$("#"+t[1]).focus();
 		}
 	}
 	// Check box check
 	$.fn.check = function() {
 		$(this).attr("checked", true);
 	}
 	
 	$.fn.uncheck = function() {
 		$(this).attr("checked", false);
 	}
 	
 	// JQCommon Function
 	$.fn.getGridData = function(){
 		return $.data($(this)[0], "gridData");
 	}
 	
 // JQCommon Function
 	$.fn.getDataIndex = function(id){
 		var cPage = parseInt($(this).getGridParam('page'),10) - 1;
		var noPerPage = parseInt($(this).getGridParam('rowNum'), 10);
		return parseInt(id, 10) - (cPage * noPerPage) - 1;
 	}
 	
 	$.fn.getGridRowData = function(id){
 		var cPage = parseInt($(this).getGridParam('page'),10) - 1;
		var noPerPage = parseInt($(this).getGridParam('rowNum'), 10);
		var dataObjIndex = parseInt(id, 10) - (cPage * noPerPage) - 1;
		var dataSet = $.data($(this)[0], "gridData");
		return dataSet[dataObjIndex];
 	}
 	
 	$.fn.getGridRowDataV2 = function(id){
 		var cPage = parseInt($(this).getGridParam('page'),10) - 1;
		var dataObjIndex = parseInt(id, 10) - 1;
		var dataSet = $.data($(this)[0], "gridData");
		return dataSet[dataObjIndex];
 	}
 	
 	//this will be extensions  the functionality of jqGris GridToForm 
	// if GridToForm return an error please check whether this is included in the jsp
	$.fn.extend({
	 	GridToForm : function( rowid, formid ) {
	 		repDot = function(val){
	 			return val.replace(".","\\.");
	 		};
			return this.each(function(){
				var $t = this;
				if (!$t.grid) { return; } 
				var rowdata = $($t).getRowData(rowid);
				if (rowdata) {
					for(var i in rowdata) {
						if ( $("[name="+repDot(i)+"]",formid).is("input:radio") || $("[name="+repDot(i)+"]",formid).is("input:checkbox"))  {
							$("[name="+repDot(i)+"]",formid).each( function() {
								if( $(this).val() == rowdata[i] ) {
									$(this).prop("checked",true);
								} else {
									$(this).prop("checked",false);
								}
							});
						} else {
						// this is very slow on big table and form.
							$("[name="+repDot(i)+"]",formid).val(rowdata[i]);
						}
					}
				}
			});
		}
	});

 	//this will be extensions  the functionality of jqGris GridToForm 
	// if GridToForm return an error please check whether this is included in the jsp
	$.fn.extend({
	 	GridToForm : function( rowid, formid ) {
	 		repDot = function(val){
	 			return val.replace(".","\\.");
	 		};
			return this.each(function(){
				var $t = this;
				if (!$t.grid) { return; } 
				var rowdata = $($t).getRowData(rowid);
				if (rowdata) {
					for(var i in rowdata) {
						if ( $("[name="+repDot(i)+"]",formid).is("input:radio") || $("[name="+repDot(i)+"]",formid).is("input:checkbox"))  {
							$("[name="+repDot(i)+"]",formid).each( function() {
								if( $(this).val() == rowdata[i] ) {
									$(this).prop("checked",true);
								} else {
									$(this).prop("checked",false);
								}
							});
						} else {
						// this is very slow on big table and form.
							$("[name="+repDot(i)+"]",formid).val(rowdata[i]);
						}
					}
				}
			});
		}
	});
	
	$.fn.safeImageUrl = function(args){
		var element = this;
		if(args.url != null){			
			var img = new Image();
			img.src = args.url;
			img.onload = function(){	
				$(element).attr('src',args.url);
			}; 
			img.onerror = function(){
				$(element).attr('src','');
			};
		}
	};
 	
 