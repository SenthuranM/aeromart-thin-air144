jQuery(document).ready(function(){  //the page is ready				
			
			$("#divSearch").decoratePanel("Search Meal Categories");
			$("#divResultsPanel").decoratePanel("Meal Categories");
			$("#divDispMealCategory").decoratePanel("Add/Modify Meal Categories");			
			$('#btnAdd').decorateButton();
			$('#btnEdit').decorateButton();
			$('#btnDelete').decorateButton();
			$('#btnClose').decorateButton();
			$('#btnReset').decorateButton();
			$('#btnSave').decorateButton();
			$('#btnUpload').decorateButton();
			$('#btnSearch').decorateButton();
			$('#btnUploadThumbnail').decorateButton();
			$('#btnAddDescription').decorateButton();
			disableMealCategoryButton();
			createPopUps('Meal List For Display');
			PopUpData();
			var grdArray = new Array();	
			jQuery("#listMealCategory").jqGrid({ 
				url:'showMealCategory!searchMealCategory.action',
				datatype: "json",
				jsonReader : {
					  root: "rows", 
					  page: "page",
					  total: "total",
					  records: "records",
					  repeatitems: false,					 
					  id: "0"				  
					},													
				colNames:['&nbsp;','Meal Category','Version','createdBy', 'createdDate', 'modifiedBy', 'modifiedDate', 'mealCategoryId','mealCatDescriptionForDisplay'], 
				colModel:[ 	{name:'Id', width:40, jsonmap:'id'},   
						 	{name:'mealCategoryName',index:'mealCategoryName', width:180, jsonmap:'mealCategory.mealCategoryName'}, 
							{name:'version', width:225, align:"center",hidden:true,  jsonmap:'mealCategory.version'},	
						   	{name:'createdBy', width:225, align:"center",hidden:true,  jsonmap:'mealCategory.createdBy'},
						   	{name:'createdDate', width:225, align:"center",hidden:true,  jsonmap:'mealCategory.createdDate'},
						   	{name:'modifiedBy', width:225, align:"center",hidden:true,  jsonmap:'mealCategory.modifiedBy'},
						   	{name:'modifiedDate', width:225, align:"center",hidden:true,  jsonmap:'mealCategory.modifiedDate'},
							{name:'mealCategoryId', width:225, align:"center",hidden:true,  jsonmap:'mealCategory.mealCategoryId'},		
							{name:'mealCatDescriptionForDisplay', width:225, align:"center",hidden:true, jsonmap:'mealCatDescriptionForDisplay'},
						  ], imgpath: '../../themes/default/images/jquery', multiselect:false,
				pager: jQuery('#mealCategoryPager'),
				rowNum:20,						
				viewrecords: true,
				height:210,
				width:930,
				loadui:'block',
				onSelectRow: function(rowid){
						$("#rowNo").val(rowid);
						$('#dvUpLoad').html($('#dvUpLoad').html());
						$('#dvUpLoadThum').html($('#dvUpLoadThum').html());
						fillForm(rowid);
						disableControls(true);
						enableSearch();
						disableMealCategoryButton();			
						enableGridButtons();
						enablePopupControls();
						setMealCatForDisplayList();
						
			}}).navGrid("#mealCategoryPager",{refresh: true, edit: false, add: false, del: false, search: false});
			
			
			var options = {
				cache: false,	//Fixed JIRA : 3042 - Lalanthi
				beforeSubmit:  showRequest,  // pre-submit callback - this can be used to do the validation part 
				success: showResponse,   // post-submit callback			 
				dataType:  'json'        // 'xml', 'script', or 'json' (expected server response type)  
			}; 

			var delOptions = {	
				cache: false, //Fixed JIRA : 3042 - Lalanthi
				beforeSubmit:  showDelete,  // pre-submit callback - this can be used to do the validation part 
				success: showResponse,   // post-submit callback 				 
				url: 'showMealCategory!deleteMealCategory.action',         // override for form's 'action' attribute 
				dataType:  'json',        // 'xml', 'script', or 'json' (expected server response type) 
				resetForm: true        // reset the form after successful submit 
				 
			 }; 

		    
			// pre-submit callback 
			function showRequest(formData, jqForm, options) { 

			    if(	trim($("#mealCategoryName").val()) == "") {			    	
			    	showCommonError("Error", mealCategoryNameRqrd);
			    	return false; 
			    }
			    top[2].ShowProgress();
			    return true; 
			} 
			
			function showResponse(responseText, statusText)  {
				top[2].HideProgress();
				showCommonError(responseText['msgType'], responseText['succesMsg']);
			    if(responseText['msgType'] != "Error") {
			    	$('#frmMealCategory').clearForm();
			    	$("#imgMealCategory").attr('src', '');
			    	jQuery("#listMealCategory").trigger("reloadGrid");
			    	disableMealCategoryButton();
			    	$("#btnAdd").attr('disabled', false); 
			    	$("#btnSave").enableButton();
			    	alert(responseText['succesMsg']);
			    }			    
			} 			

			function showDelete(formData, jqForm, options) {				
			    return confirm(deleteRecoredCfrm); 
			} 
			$('#btnSave').click(function() {
				disableControls(false);
				$('#dvUpLoad').html($('#dvUpLoad').html());
				$('#dvUpLoadThum').html($('#dvUpLoadThum').html());			
				if(getText("hdnModel") == 'ADD') {
					setField("hdnMode","ADD");
				}
				$("#mealCatDescForDisplay").val(buildMealCatForDisplayStr());
				$('#frmMealCategory').ajaxSubmit(options);
				return false;	
			});
			 
			$('#btnAdd').click(function() {
				disableControls(false);
				$('#frmMealCategory').resetForm();				
				$("#imgMealCategory").attr('src', '');	
				$("#version").val('-1');
				$("#rowNo").val('');
				$('#dvUpLoad').html($('#dvUpLoad').html());
				$('#dvUpLoadThum').html($('#dvUpLoadThum').html());
				$('#btnEdit').disableButton();
				$('#btnDelete').disableButton();
				$('#btnUpload').enableButton();
				$('#btnUploadThumbnail').enableButton();
				$("#btnSave").enableButton();
				setField("hdnMode","ADD");						
			}); 	

			$('#btnEdit').click(function() {				
				disableControls(false);
				$("#uploadImage").val('');
				$('#btnDelete').disableButton();
				$('#btnUpload').enableButton();
				$("#btnSave").enableButton();
				setField("hdnMode","EDIT");
			}); 
			$('#btnReset').click(function() {
				if($("#rowNo").val() == '') {
					$('#frmMealCategory').resetForm();
					$("#imgMealCategory").attr('src', '');
					$('#dvUpLoad').html($('#dvUpLoad').html());
					$('#dvUpLoadThum').html($('#dvUpLoadThum').html());
				}else {
					
					fillForm($("#rowNo").val());
				}
					
			});
			$('#btnDelete').click(function() {
				disableControls(false);
				$('#dvUpLoad').html($('#dvUpLoad').html());	
				$('#dvUpLoadThum').html($('#dvUpLoadThum').html());
				$('#frmMealCategory').ajaxSubmit(delOptions);
				return false;			
			}); 
			
			$('#btnSearch').click(function() {
				//var newUrl = "showMeal!searchMeal.action?srchMealName="+$("#srchMealName").val() +"&srchMealCode="+$("#selMealCode").val()+"&srchStatus="+$("#selStatus").val();
				var newUrl = 'showMealCategory!searchMealCategory.action?';
			 	newUrl += 'mealCategorySrch.mealCategoryName=' + $("#srchMealCategoryName").val();
			 	$("#listMealCategory").clearGridData();
			 	$.getJSON(newUrl, function(response){
					$("#listMealCategory")[0].addJSONData(response);				
			 	});				
			});
		//////////////////////////////////////////////////	
			$('#btnAddDescription').click(function(){
				POSenable();
			});

			function fillForm(rowid) {
				setField("hdnMode","");
				jQuery("#listMealCategory").GridToForm(rowid, '#frmMealCategory');					
				if(jQuery("#listMealCategory").getCell(rowid,'status') == 'ACT'){
					$("#status").attr('checked', true);
				}else {
					$("#status").attr('checked', false);
				}
				
				var dbImageID = jQuery("#listMealCategory").getCell(rowid,'mealCategoryId');
				//No need to add the no_cache for meals since they are uploaded on runtime
				$("#imgMealCategory").attr('src', mlCategoryImagePath+'meal_'+dbImageID+'.jpg?rel='+new Date());  
				$("#imgMealThambCategory").attr('src', mlCategoryImagePath+'meal_category_Thumbnail_'+dbImageID+'.jpg?rel='+new Date()); 						
				enableGridButtons();
			}
			function  disableControls(cond){
				$("input").each(function(){ 
				      $(this).attr('disabled', cond); 
				});	
				$("textarea").each(function(){ 
				      $(this).attr('disabled', cond); 
				});	
				$("select").each(function(){ 
				      $(this).attr('disabled', cond); 
				});
				$("file").each(function(){ 
				      $(this).attr('disabled', cond); 
				});	
				$("#btnClose").enableButton();
				
				if (listForDisplayEnabled) {
					$("#btnAddDescription").enableButton();
				} else {
					$('#btnAddDescription').attr("disabled", true); 
				}
			}
			
			function enableSearch(){
				$('#srchMealCategoryName').attr("disabled", false); 
				$('#btnSearch').enableButton();
			}
			
			function disableMealCategoryButton(){
				$("#btnEdit").disableButton();
				$("#btnDelete").disableButton();
				$("#btnReset").disableButton();
				$("#btnSave").disableButton();
				$('#btnUpload').disableButton();
				$('#btnUploadThumbnail').disableButton();
			}

			function enableGridButtons(){
				$("#btnAdd").enableButton();
				$("#btnEdit").enableButton();
				$("#btnDelete").enableButton();
				$("#btnReset").enableButton();
			}
			function enablePopupControls(){
				$("#language").enableButton();
				$("#txtMealCategoryForDisplayOL").enableButton();
				$("#add").enableButton();
				$("#remove").enableButton();	
				$("#translation").enableButton();
				$("#btnUpdate").enableButton();
				$("#btnPopupClose").enableButton();
			}
			disableControls(true);
			$("#btnAdd").enableButton();
			$('#srchMealCategoryName').attr("disabled", false); 
			$('#btnSearch').enableButton();
			
			
			var upOptionsImage = {				 
				beforeSubmit:  showUpdate,  // pre-submit callback - this can be used to do the validation part 
				success: showUploadResponseImage,   // post-submit callback 				 
				url: 'showUpload.action?mode=mealCategoryImage',         // override for form's 'action' attribute 
				dataType:  'script'        // 'xml', 'script', or 'json' (expected server response type)				 
			 };
			
			var upOptionsThumbnailImage = {				 
					beforeSubmit:  showUpdate,  // pre-submit callback - this can be used to do the validation part 
					success: showUploadResponseThumbnail,   // post-submit callback 				 
					url: 'showUpload.action?mode=mealCategoryThumbnailImage',         // override for form's 'action' attribute 
					dataType:  'script'        // 'xml', 'script', or 'json' (expected server response type)				 
				 };

			function showUpdate(formData, jqForm, options) {			
				var newImg = new Image();
				newImg.src = $("#imgMealCategory").attr('src');				
				var height = newImg.height;
				var width = newImg.width;
				
				if(width == 1024 || height == 700){
					showCommonError("Error", "Image is too large ");
					return false;
				}
				top[2].ShowProgress();
			} 
		
		$('#btnUpload').click(function() {
			$('#frmMealCategory').ajaxSubmitWithFileInputs(upOptionsImage);
			return false;	
		});
		
		$('#btnUploadThumbnail').click(function() {
			$('#frmMealCategory').ajaxSubmitWithFileInputs(upOptionsThumbnailImage);
			return false;	
		});

		function showUploadResponseImage(responseText, statusText)  {
			top[2].HideProgress();
		    eval(responseText);		    		
		    showCommonError(responseText['msgType'], responseText['succesMsg']);
		    if(responseText['msgType'] != "Error"){		    	
		    	$('#dvUpLoad').html($('#dvUpLoad').html());
		    	$("#uploadImage").val('UPLOADED');	
		    	//No need to add the no_cache for meals since they are uploaded on runtime
		    	$("#imgMealCategory").attr('src', mlCategoryImagePath+'mealCategoryImage_test.jpg?rel='+new Date());		    	
		    }		    
		    
		} 
		
		function showUploadResponseThumbnail(responseText, statusText)  {
			top[2].HideProgress();
		    eval(responseText);		    		
		    showCommonError(responseText['msgType'], responseText['succesMsg']);
		    if(responseText['msgType'] != "Error"){		    	
		    	$('#dvUpLoadThum').html($('#dvUpLoadThum').html());
		    	$("#uploadImageThumbnail").val('UPLOADED');	
		    	//No need to add the no_cache for meals since they are uploaded on runtime
		    	$("#imgMealCategoryThamb").attr('src', mlCategoryImagePath+'mealCategoryThumbnailImage_test.jpg?rel='+new Date());		    	
		    }		    		    
		} 
			
		});	

		function setPageEdited(isEdited) {
			top[1].objTMenu.tabPageEdited(screenId, isEdited);
		}	
		
		// Dhanya
		//For Jira 4640 
		function PopUpData(){
			var html = '<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0"> <tr><td> <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" ID="Table4"> <tr><td width="10" height="20"><img src="../../images/form_top_left_corner_no_cache.gif"></td> <td height="20" class="FormHeadBackGround"><img src="../../images/bullet_small_no_cache.gif"> <font class="FormHeader"> Meal Categories For Display </font></td> <td width="11" height="20"><img src="../../images/form_top_right_corner_no_cache.gif"></td> </tr><tr><td class="FormBackGround"></td><td class="FormBackGround" valign="top"> <table><tr><td width="663" align="left"><table width="98%" height="105"  border="0" cellpadding="0" cellspacing="0"> <tr align="left" valign="middle" class="fntMedium"> <td width="19%" class="fntMedium"><font>Language</font></td> <td width="33%" align="left" style="padding-bottom:3px; ">';
				html += '<select name="language" id="language"  style="width:150px; "> ';
				html += '<option value="-1"></option>'+languageHTML;				
				html += '</select></td> <td width="13%" rowspan="3" align="center" valign="top"><input name="add" type="button" class="Button" id="add" style="width:50px; " title="Add Item"  onclick="addMealCategoryForDisplay();" value="&gt;&gt;" /> <input name="remove" type="button" class="Button" id="remove" style="width:50px;" title="Remove Item" onclick="removeMealCategoryForDisplay();" value="&lt;&lt;" /></td> <td width="35%" rowspan="3" align="left" valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0"> <tr> <td><select name="translation" size="5" id="translation" style="width:175px; height:100px; "> </select></td> </tr> <tr> </tr> </table></td> </tr> <tr align="left" valign="middle" class="fntMedium"> <td width="25%" valign="top" class="fntMedium"><font>Meal Category* </font></td> <td align="left" valign="top"><input name="txtMealCategoryForDisplayOL" type="text" id="txtMealCategoryForDisplayOL" size="32" maxlength="125" /></td> </tr> '; 
				//html += '<tr align="left" valign="middle" class="fntMedium"> <td width="25%" valign="top" class="fntMedium"><font>Meal Title* </font></td> <td valign="top"><input name="txtMealTitleForDisplayOL" type="text" id="txtMealTitleForDisplayOL" size="32" maxlength="125" /></td> </tr>'; 
				html +=	'</table></td> </tr><tr><td align="top"><span id="spnSta"></span></td></tr> <tr><td align="right"><input type="button" id="btnUpdate" value= "Save" class="Button" onClick="saveMealCategoryForDisplay();"> &nbsp;<input type="button" id="btnPopupClose" value= "Close" class="Button" onClick="return POSdisable(true);"></td></tr></table> </td><td class="FormBackGround"></td></tr><tr> <td height="12"><img src="../../images/form_bottom_left_corner_no_cache.gif"></td> <td height="12" class="FormBackGround"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td> <td height="12"><img src="../../images/form_bottom_right_corner_no_cache.gif"></td> </tr></table> </td></tr></table>';

			DivWrite("spnPopupData",html);
		}

		function POSenable(){			
			if(!listForDisplayEnabled)
				return;
			if(getFieldByID("mealCategoryName").value=="" || getFieldByID("mealCategoryName").value==null){
					alert("No meal is selected, please select meal and try again");
					return;
				}
			
			if(getValue('hdnModel') == 'ADD'){
				var mealList = getFieldByID("translation");
				mealList.options.length = 0;
				setField("txtMealCategoryForDisplayOL", "");
				//setField("txtMealTitleForDisplayOL", "");
				setField("language","-1");
			}
			
			setField("hdnMode", "SAVELISTFORDISP");
			showPopUp(1);
		}

		function POSdisable(bFromClose){			
			if(!listForDisplayEnabled)
				return;
			if(bFromClose){
				//setField("hdnModel","");
				setField("hdnMode","");
			}
			 hidePopUp(1);
		}
		
		
/////////////////////////////////////////////////////////////////		
/////////////////////////////////////////////////////////////////		
		
		//function prepareMealCategoryToSave(){
		function saveMealCategoryForDisplay(){
			var strMode = getText("hdnMode");
			var mealList = getFieldByID("translation");
			var size = mealList.length;
			var langCode;
			var mealCatName;
			var mealTitle;
			var label;
			var str="";
			var size;
			var i=0;
			var j=0;
			var newData = new Array();
			var count = 0;
			//////////
			//var mealCatTranslations = jQuery("#listMealCategory").getCell($("#rowNo").val(),'mealForDisplay');
			var mealCatTranslations = jQuery("#listMealCategory").getCell($("#rowNo").val(),'mealCatDescriptionForDisplay');
			var arrMealCatTrl = new Array();
			if(mealCatTranslations != null && mealCatTranslations != "")
				arrMealCatTrl = mealCatTranslations.split("~");
			var mealTrl;
			grdArray = new Array();
			for(var i =0;i<arrMealCatTrl.length-1;i++){
				grdArray[i] = new Array(); 
				mealTrl = arrMealCatTrl[i].split(",");
				grdArray[i][0] = mealTrl[0]; // id
				grdArray[i][1] = mealTrl[1]; // langcode
				grdArray[i][2] = mealTrl[2]; // meal category			
			}
			////////////
			for(i=0;i<mealList.length;i++){
				langCode = mealList.options[i].value;
				label = mealList.options[i].text.split("=>");
				
				
//				mealCatName = label[1];
//				if (label.length > 2 && label[2]!="")
//					mealTitle = label[2];
//				else
//					mealTitle = getValue("mealCatName");
				
				if(label.length>1){
					mealCatName = label[1];
				}else{
					mealCatName= getValue("");
				}
				
				size = grdArray.length;
				var found = false;
				for(j=0;j<size;j++){
					if(grdArray[j][0] == langCode){
						newData[count++] = new Array(grdArray[j][1],langCode, mealCatName);
						found = true;
						break;
					}
				}
				if(!found){
					newData[count++] = new Array("-1",langCode, mealCatName);
				}
			}
			var langCode2;
			var size1 = grdArray.length;
			var size2 = newData.length;
			for(i=0;i<size1;i++){//original list
				found = false;
				for(j=0;j<size2;j++){//new data
					langCode = grdArray[i][0];
					langCode2 = newData[j][0];
					if(langCode == langCode2){//the lang code exist in the new data
						found = true;
					}
				}
				if(!found){
					newData[newData.length] = new Array( grdArray[i][0],"null", grdArray[i][2],grdArray[i][3],grdArray[i][4], "null");
				}
			}
			grdArray = newData;		
			
			if(validateMealCatForDisplay() == false){
		        return false;
		    }
		    $('#btnSave').click();
		    return POSdisable(false);
		}
		
		
	
//////////////////////////////////aw//////////////		
		
		function addMealCategoryForDisplay(){			
			if(!listForDisplayEnabled)
				return;
			if(getValue('txtMealCategoryForDisplayOL').length ==0){
				return false;
			}
			var selLang = getValue("language");
			if(selLang == '-1')
				return false;
			if(findValueInList(selLang, "translation") == true){
				alert("Meal description for the selected language already exist");
				return false;
			}

			var selLangName = $("#language :selected").text();
			var mealCatDesc = $("#txtMealCategoryForDisplayOL").val();
			//var mealTitle = $("#txtMealTitleForDisplayOL").val();
			addToList("translation", selLangName+"=>"+mealCatDesc, selLang);	
			$("#txtMealCategoryForDisplayOL").text("");
			$("#language").val("-1");
		}
		
		
		function removeMealCategoryForDisplay(){			
			if(!listForDisplayEnabled)
				return;
			var mealList = getFieldByID("translation");
			var selected = mealList.selectedIndex;
			if(selected != -1){
				var langCode = mealList.options[selected].value;
				var label = mealList.options[selected].text.split("=>");
				$("#language").val(langCode);
				$("txtMealCategoryForDisplayOL").text(label[1]);
				
				mealList.remove(selected);
			}
		}
		
		

		function setMealCatForDisplayList(){			
			if(!listForDisplayEnabled)
				return;

			if(jQuery("#listMealCategory").getCell($("#rowNo").val())>=0 ){
				setField("txtMealCategoryForDisplayOL", "");
				setField("language","-1");

				var translationList = getFieldByID("translation");	
				translationList.options.length = 0;
				var langCode ;		
				var mealCatDescription;
				var langName;
				
				var i=0;
				
				var catTranslations = jQuery("#listMealCategory").getCell($("#rowNo").val(),'mealCatDescriptionForDisplay');
				
				var arrCategryTrl = new Array();
				if(catTranslations != null && catTranslations != "")
					arrCategryTrl = catTranslations.split("~");
				var ssrTrl;
				grdArray = new Array();
				for(var i =0;i<arrCategryTrl.length-1;i++){
					grdArray[i] = new Array(); 
					ssrTrl = arrCategryTrl[i].split(",");
					grdArray[i][0] = ssrTrl[0]; // id
					grdArray[i][1] = ssrTrl[1]; // langcode
					grdArray[i][2] = ssrTrl[2]; // desc
				}
				for(i=0;i<grdArray.length;i++){
					langCode = grdArray[i][1];

					mealCatDescription = grdArray[i][2];

					if(mealCatDescription == "null")
						continue;
					langName = getListLabel("language", langCode);
					addToList("translation",langName+"=>"+mealCatDescription,langCode);
				}
			}
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	///////////////////////////////////////////////////	
		
		
		
		function validateMealCatForDisplay(){
			if(!listForDisplayEnabled)
				return;
			var strMode = getText("hdnMode");
			var size2=getFieldByID("language").length;
			var valid = true;
			var size1;
			var i;
			var j;
			var langCode;						
			var mealCatDescription;
			var found = false;
			if(strMode == "ADD")
				valid = false;
			else{
				size1 = grdArray.length;
				if(size1 != (size2-1))// -1 to ignore the first empty item
					valid = false;
				else{
					for(i=0;i<size1;i++){
						mealCatDescription = grdArray[i][1];
						if(mealCatDescription == "null"){
							valid = false;
							break;
						}
					}
				}
			}
			if(!valid){
				alert("Meal Category description for display is missing for all or some languages, original SSR description will copied to all languages") ;
				if(strMode != "ADD"){

						var descr = getValue("mealCategoryName");

						for(i=1;i<size2;i++){// lang list, start from 1 to ignore the
												// first item (empty)
							found = false;
							langCode = getFieldByID("language").options[i].value;
							for(j=0;j<size1;j++){

								mealCatDescription = grdArray[j][2];
								if(mealCatDescription == "null"){
									grdArray[j][2] = descr;
								}
								if(langCode == grdArray[j][1]){
									found = true;
									continue;
								}
							}
							
							if(!found){
								grdArray[grdArray.length] = new Array("-1",langCode,descr);
							}

								
						}
				}
			}
			return true;
		}
		
		function buildMealCatForDisplayStr(){
			// ltid,langCode,lt
			var strMode = getText("hdnMode");
			var mealCatStr = "";
			
			if(strMode == "ADD"){												

				var mealCatName = getValue("mealCategoryName");		
				var size2=getFieldByID("language").length;

				for(i=1;i<size2;i++){
					langCode = getFieldByID("language").options[i].value;
					
					if(mealCatStr.length>0)
						mealCatStr += "~";

					mealCatStr=mealCatStr +"-1,"+ langCode+","+mealCatName;
				}
			}else {
				prepareMealCategoryToSave();
				if(grdArray != null){					
					for(var j=0;j<grdArray.length;j++){
						// id,langcode,desc
						mealCatStr += grdArray[j][0]+","; 
						mealCatStr += grdArray[j][1]+",";				
						mealCatStr += grdArray[j][2];									
						mealCatStr += "~";	
				   	}
				}	
			}		

			return mealCatStr;
		}
		
		
		function prepareMealCategoryToSave(){
			var strMode = getText("hdnMode");
			var translationList = getFieldByID("translation");
			var size = translationList.length;
			var langCode;
			var mealCatName;
			var label;
			var str="";
			var size;
			var i=0;
			var j=0;
			var newData = new Array();
			var count = 0;
			
			var mealCatTranslations = jQuery("#listMealCategory").getCell($("#rowNo").val(),'mealCatDescriptionForDisplay');
			var arrMealCatTrl = new Array();
			if(mealCatTranslations != null && mealCatTranslations != "")
				arrMealCatTrl = mealCatTranslations.split("~");
			var ssrTrl;
			grdArray = new Array();						
			for(var i =0;i<arrMealCatTrl.length-1;i++){
				grdArray[i] = new Array(); 
				ssrTrl = arrMealCatTrl[i].split(",");
				grdArray[i][0] = ssrTrl[0];  // id
				grdArray[i][1] = ssrTrl[1];  // langcode
				grdArray[i][2] = ssrTrl[2];  // meal category
							
			}

			for(i=0;i<translationList.length;i++){
				langCode = translationList.options[i].value;
				label = translationList.options[i].text.split("=>");
				
				if (label.length > 1 && label[1]!=""){
					mealCatName = label[1];
				}else{
					mealCatName = getValue("mealCategoryName");
				}
					
				size = grdArray.length;
				var found = false;
				for(j=0;j<size;j++){
					if(grdArray[j][1] == langCode){									
						newData[count++] = new Array(grdArray[j][0],langCode,mealCatName);
						found = true;
						break;
					}
				}
				if(!found){								
					newData[count++] = new Array( "-1",langCode,mealCatName);
					
				}
			}
			var langCode2;
			var size1 = grdArray.length;
			var size2 = newData.length;
			for(i=0;i<size1;i++){// original list
				found = false;
				for(j=0;j<size2;j++){// new data
					langCode = grdArray[i][1];
					langCode2 = newData[j][1];
					if(langCode == langCode2){// the lang code exist in the new data
						found = true;
					}
				}
				if(!found){								
					newData[newData.length] = new Array(grdArray[i][0],grdArray[i][1],getValue("mealCategoryName"));
				}
			}
			grdArray = newData;		
			
		}
		
		