<!DOCTYPE HTML>
<html>
<head>
	<title>Sample Validation</title>
	<link rel="stylesheet" type="text/css" href="../css/Style_no_cache.css"/>		
	<link rel="stylesheet" type="text/css" href="../themes/default/css/ui.all_no_cache.css"/>
</head>
<body style="background: #fff;">
<div>
	<div id="form-sample">
	
	</div>
</div>

<script type="text/javascript" src="../js/v2/jquery/jquery.js"  type="text/javascript"></script>
<script type="text/javascript" src="../js/v2/jquery/jquery.ui.js"  type="text/javascript"></script>
<script type="text/javascript" src="../js/v2/common/jQuery.common.js"  type="text/javascript"></script>
<script type="text/javascript" src="../js/v2/isalibs/isa.jquery.validator.js"  type="text/javascript"></script>
<script>
<!--
$("#form-sample").isaValidateDiv();
-->
</script>
</body>
</html>