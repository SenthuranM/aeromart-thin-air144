$(document).ready(function() {

	$('#scheduledDate').datepicker({
		minDate : new Date(),
		dateFormat : 'dd/mm/yy'
	});
	$('#scheduledTime').timepicker({

	});

	$('#scheduleStartDate').datepicker({
		minDate : new Date(),
		dateFormat : 'dd/mm/yy'
	});

	$('#scheduleEndDate').datepicker({
		minDate : new Date(),
		dateFormat : 'dd/mm/yy'
	});

	Disable("scheduleStartDate", true);
	Disable("scheduleEndDate", true);
	HideProgress();

});

function onClickAllSchedules() {
	Disable("scheduleStartDate", true);
	Disable("scheduleEndDate", true);
	setField("scheduleStartDate", "");
	setField("scheduleEndDate", "");
}

function onClickSelectedSchedules() {
	Disable("scheduleStartDate", false);
	Disable("scheduleEndDate", false);
}

function onClickBtnSchedule() {
	if (validateFields()) {
		var data = {};
		data['scheduledDate'] = $("#scheduledDate").val();
		data['scheduledTime'] = $("#scheduledTime").val();
		data['selectedScheduleRange'] = $(
				'input[name="selectedScheduleRange"]:checked').val();
		data['scheduleStartDate'] = $("#scheduleStartDate").val();
		data['scheduleEndDate'] = $("#scheduleEndDate").val();
		data['emailAddress'] = $("#emailAddress").val();
		data['sitaAddress'] = $("#sitaAddress").val();
		data['emailSelect'] = $("#emailRadio").is(":checked");
		data['sitaSelect'] = $("#sitaRadio").is(":checked");
		var url = "scheduleExternalSSMRecap.action";

		$.ajax({
			type : "POST",
			dataType : 'json',
			data : data,
			url : url,
			success : onSuccessSaveSchedule,
			error : onErrorSaveSchedule,
			cache : false
		});
	}
}

function onSuccessSaveSchedule(response) {
	if (response.success) {
		showWindowCommonError("Confirmation", "Job Succesfully Scheduled");
		clearAllValues();
	} else {
		showWindowCommonError("Error", response.messageTxt);
	}
}

function onErrorSaveSchedule(response) {
	showWindowCommonError("Error", response.messageTxt);
}

function validateFields() {

	if ("" == $.trim($("#scheduledDate").val())) {
		showWindowCommonError("Error", "Enter Schedule Date");
		$("#scheduledDate").focus();
		return false;
	}

	if ("" == $.trim($("#scheduledTime").val())) {
		showWindowCommonError("Error", "Enter Schedule Time");
		$("#scheduledTime").focus();
		return false;
	}

	if (document.getElementById('selectedScheduleRadio').checked) {

		if ("" == $.trim($("#scheduleStartDate").val())) {
			showWindowCommonError("Error", "Enter schedule start date");
			$("#scheduleStartDate").focus();
			return false;
		}

		if ("" == $.trim($("#scheduleEndDate").val())) {
			showWindowCommonError("Error", "Enter schedule end date");
			$("#scheduleEndDate").focus();
			return false;
		}

		if ($("#scheduleStartDate").datepicker('getDate') > $(
				"#scheduleEndDate").datepicker('getDate')) {
			showWindowCommonError("Error",
					"The From Date should be less than the To Date");
			$("#scheduleStartDate").focus();
			return false;
		}
	}

	if ($("#emailRadio").is(":checked")) {
		if ("" == $.trim($("#emailAddress").val())) {
			showWindowCommonError("Error", "Enter Email Address");
			return false;
		}
		if ($.trim($("#emailAddress").val()) != "") {
			if (!checkEmail($("#emailAddress").val())) {
				showWindowCommonError("Error", "Invalid Email Address");
				$("#emailAddress").focus();
				return false;
			}
		}
	}
	if ($("#sitaRadio").is(":checked")) {
		if ("" == $.trim($("#sitaAddress").val())) {
			showWindowCommonError("Error", "Enter sita Address");
			return false;
		}
	}
	return true;
}

function clearAllValues() {
	setField("scheduledDate", "");
	setField("scheduledTime", "");
	setField("scheduleStartDate", "");
	setField("scheduleEndDate", "");
	setField("emailAddress", "");
	setField("sitaAddress", "");
}

function onClickEmail() {
	setField("sitaAddress", "");
	Disable("sitaAddress", true);
	Disable("emailAddress", false);
	$("#sitaRadio").prop('checked', false);
}

function onClickSita() {
	setField("emailAddress", "");
	Disable("emailAddress", true);
	Disable("sitaAddress", false);
	$("#emailRadio").prop('checked', false);
}

function removeNow() {
	$(".ui-datepicker-current").hide();
}
