/*
 * Author : K.Srikanth
 */

var strRowData;
var strGridRow;
var intLastRec;

var screenId = "SC_ADMN_021";
var valueSeperator = "~";


function validateSitaAddress() {
	var sitaAddress = trim(getValue("txtFormSitaAddress"));
	var emailAdd = trim(getValue("txtEmail"));

	if ((sitaAddress == null || sitaAddress == "")
			&& (getValue("selDeliveryMethod") == "SITATEX")) {
		showCommonError("Error", sitaAddressRqrd);
		getFieldByID("txtFormSitaAddress").focus();
		return
	}
	
	if ((sitaAddress == null || sitaAddress == "")
			&& (emailAdd == null || emailAdd == "")) {
		showCommonError("Error", sitaAddressOrEmailRqrd);
		getFieldByID("txtFormSitaAddress").focus();
		return	
	}
	if (validateTextField(getValue("selFormAirport"), sitaAirportRqrd)) {
		getFieldByID("selFormAirport").focus();
		return;
	}

	if (validateTextField(trim(getValue("txtLocation")), sitaLocationRqrd)) {
		getFieldByID("txtLocation").focus();
		return;
	}
	if (validateTextField(trim(getValue("selCarrierCodes")), sitaCarrierCodeRqrd)) {
		getFieldByID("selCarrierCodes").focus();
		return;
	}
	
	if(isDeliveryMethodDisplayEnabled){
		if (validateTextField(trim(getValue("selDeliveryMethod")), deliveryMethodRqrd)) {
			getFieldByID("selDeliveryMethod").focus();
			return;
		}
	}	

	if(!getFieldByID("allowPnlAdl").checked && !getFieldByID("allowPalCal").checked){
		showCommonError("Error", pnladlOrpalcalRqrd);
		return false;
	}

	if ((emailAdd != "") && !validateEmail()) {

		if (emailAdd.indexOf(",") != -1) {
			showCommonError("Error", onlyOneEmail);
			getFieldByID("txtEmail").focus();
			return;
		} else {
			showCommonError("Error", invalidEmail);
			getFieldByID("txtEmail").focus();
			return;
		}
	}
	
	if (!validateOnd()){
		// create variable for this
		showCommonError("Error", "Invalid Ond value");
		return false;
	}
	
	return true;
}

// On Page loading make sure that all the page input fields are cleared
function winOnLoad(strMsg, strMsgType) {
	
	initialSelFlightNums = document.getElementById('sel_FlightNo_Initial').options;
	
	$('#sel_FlightNo').pairMaster();
    $('#add_FlightNo').click(function(){
    	if (validateSelectedFlightNumbersWithOrigin()){
    		$('#sel_FlightNo').addSelected('#sel_FlightNoAssigned');
    	} else {
    		showCommonError("Error", "Please select valid Flight Number for Depature Airport");
    	}
    });
    $('#del_FlightNo').click(function(){
        $('#sel_FlightNoAssigned').removeSelected('#sel_FlightNo');
    }); 
            
	// Menaka
	if (saveSuccess == 1) {
		alert("Record Successfully saved!");
		saveSuccess = 0;
	}
	// Keep the search variables remain same
	if (isSearchMode) {
		document.forms[0].hdnUIMode.value = "SEARCH";
	}
	// Menaka
	if (strMsg != null && strMsgType != null)
		showCommonError(strMsgType, strMsg);
	// to set the SitaAddress after search
	if (airportCode != null) {
		setField("selAirport", airportCode);
	}
	if (activeStatus != null) {
		setField("selActiveStatus", activeStatus);
	}
	if (sitaAddress != null) {
		setField("txtSitaAddress", sitaAddress);
	}
	if (carrierCode != null) {
		setField("selCarrierCode", carrierCode);
	}
	if (top[1].objTMenu.focusTab == screenId) {
		getFieldByID("selAirport").focus();
	}

	enableInputControls(true);
	Disable("btnEdit", true);
	Disable("btnDelete", true);
	setPageEdited(false);

	if (arrFormData != null && arrFormData.length > 0) {
		strGridRow = getTabValues(screenId, valueSeperator, "strGridRow"); // Get
																			// saved
																			// grid
																			// Row
		setPageEdited(true);
		enableInputControls(false);
		selectChange("selFormAirport", arrFormData[0][3]);
		setField("txtLocation", arrFormData[0][4]);
		setField("txtFormSitaAddress", arrFormData[0][1]);
		setField("selCarrierCodes", arrFormData[0][2]);
		if (arrFormData[0][5] == "Active" || arrFormData[0][5] == "active"
				|| arrFormData[0][5] == "on")
			getFieldByID("chkStatus").checked = true;
		else
			getFieldByID("chkStatus").checked = false;
		setField("txtEmail", arrFormData[0][6]);

		getFieldByID("allowPnlAdl").checked = arrFormData[0][7];
		getFieldByID("allowPalCal").checked = arrFormData[0][8];
		
		

		if (isAddMode) {
			setField("hdnMode", "ADD");
			if (top[1].objTMenu.focusTab == screenId) {
				getFieldByID("txtFormSitaAddress").focus();
			}
			// When invalid Airport selected focus should be set to Airport
			// combo
			if (isAirportFocus) {
				if (top[1].objTMenu.focusTab == screenId) {
					getFieldByID("selFormAirport").focus();
				}
			}
		} else {
			setField("hdnMode", "EDIT");
			// Disable("txtFormSitaAddress", true);
			if (top[1].objTMenu.focusTab == screenId) {
				getFieldByID("selFormAirport").focus();
			}
			setField("hdnVersion", prevVersion);
			setField("hdnSitaId", prevSitaId);
			setField("hdnGridRow", prevGridRow);
			strGridRow = prevGridRow;
			if (isAirportFocus) {
				if (top[1].objTMenu.focusTab == screenId) {
					getFieldByID("selFormAirport").focus();
				}
			}
		}

	}

	if (!isPalCalEnabled) {
		document.getElementById("allowPalCalRow").style.display = 'none';
	}

	if (!isDeliveryMethodDisplayEnabled) {
		document.getElementById("deliveryMethod").style.display = 'none';
	}
}

function selectChange(strControl, strCountryCode) {
	setField(strControl, strCountryCode);
}

// on Grid Row click
function RowClick(strRowNo) {
	objOnFocus();
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		strRowData = objDG.getRowValue(strRowNo);
		strGridRow = strRowNo;
		setField("selCarrierCodes", "");
		setField("selSegment", "");
		setField("sel_FlightNoAssigned", "");
		setField("hdnGridRow", strRowNo);
		setField("hdnSitaId", arrData[strRowNo][1]);

		var optAirportValue = arrData[strRowNo][4];
		for (selSize = 0; selSize < document.forms[0].selFormAirport.length; selSize = selSize + 1) {
			if (document.forms[0].selFormAirport.options[selSize].value == optAirportValue) {
				document.forms[0].selFormAirport.options[selSize].selected = true;
			}
		}

		setField("txtLocation", arrData[strRowNo][5]);
		setField("txtFormSitaAddress", arrData[strRowNo][2]);

		if (arrData[strRowNo][6] == "Active"
				|| arrData[strRowNo][6] == "active")
			getFieldByID("chkStatus").checked = true;
		else
			getFieldByID("chkStatus").checked = false;

		getFieldByID("allowPnlAdl").checked = arrData[strRowNo][10];
		getFieldByID("allowPalCal").checked = arrData[strRowNo][11];

		setField("hdnVersion", arrData[strRowNo][7]);
		setField("txtEmail", arrData[strRowNo][8]);
		setField("selCarrierCodes", arrData[strRowNo][3]);
		setField("selDeliveryMethod",arrData[strRowNo][9]);

		fillSegmentSelBox(arrData[strRowNo][12]);
		fillFlightNumSelBoxes(initialSelFlightNums, arrData[strRowNo][13]);
		
		setPageEdited(false);
		enableInputControls(true);
		Disable("btnEdit", false);
		Disable("btnDelete", false);
		setField("hdnMode", "");
	}
}

function searchSitaAddress() {
	objOnFocus();
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		clearSitaAddress();
		enableInputControls(true);
		document.forms[0].hdnUIMode.value = "SEARCH";
		document.forms[0].hdnMode.value = "";
		document.forms[0].submit();
	}
}

function enableInputControls(blnStatus) {
	Disable("selFormAirport", blnStatus);
	Disable("txtFormSitaAddress", blnStatus);
	Disable("txtLocation", blnStatus);
	Disable("btnSave", blnStatus);
	Disable("btnReset", blnStatus);
	Disable("chkStatus", blnStatus);
	Disable("txtEmail", blnStatus);
	Disable("selCarrierCodes", blnStatus);
	Disable("selDeliveryMethod", blnStatus);
	Disable("allowPnlAdl", blnStatus);
	if (isPalCalEnabled) {
		Disable("allowPalCal", blnStatus);
	} else {
		document.getElementById("allowPalCalRow").style.display = 'none';
	}
	Disable("radRoute",blnStatus);
	Disable("radFlightNum",blnStatus);
	disableFlightNumSelections(blnStatus);
	disableRouteSelections(blnStatus);
}

function clearSitaAddress() {
	setField("selFormAirport", "");
	setField("hdnVersion", "");
	setField("txtFormSitaAddress", "");
	setField("txtLocation", "");
	setField("txtEmail", "");
	setField("selCarrierCodes", "");
	setField("hdnCarrierCodes", "");
	getFieldByID("chkStatus").checked = false;
	setField("selDeliveryMethod", "");
	getFieldByID("allowPnlAdl").checked = false;
	getFieldByID("allowPalCal").checked = false;
	//setField("sel_FlightNoAssigned", "");
	setField("selSegment", "");
	setField("hdnOndCodes", "");
	setField("hdnFlightNumbers", "");
}

function saveSitaAddress() {
	if (!validateSitaAddress())
		return;

	// setField("hdnRecNo",(top[0].intLastRec));
	var tempName = trim(getText("txtLocation"));
	tempName = replaceall(tempName, "'", "`");
	tempName = replaceEnter(tempName);
	setField("txtLocation", tempName);
	setField("hdnRecNo", getTabValues(screenId, valueSeperator, "intLastRec"));

	Disable("selFormAirport", false);
	Disable("txtFormSitaAddress", false);
	Disable("selCarrierCodes", false);
	Disable("selDeliveryMethod", false);
	setTabValues(screenId, valueSeperator, "strGridRow", strGridRow); // Save
																		// the
																		// grid
																		// Row
	var strVisibile="";
	if(getFieldByID("selCarrierCodes")){
		var intCount = getFieldByID("selCarrierCodes").length ;
		 for (var i = 0; i < intCount; i++){
		 	if (getFieldByID("selCarrierCodes").options[i].selected){
		   	  strVisibile+=getFieldByID("selCarrierCodes").options[i].value+",";
		   	}
		 }
	}	

	setField("hdnCarrierCodes", strVisibile);
	
	var strOndCodes="";
	if(getFieldByID("selSegment")){
		var intCount = getFieldByID("selSegment").length ;
		 for (var i = 0; i < intCount; i++){
		 	strOndCodes+=getFieldByID("selSegment").options[i].value+",";
		 }
	}	

	setField("hdnOndCodes", strOndCodes);
	
	var strFlightNumbers="";
	if(getFieldByID("sel_FlightNoAssigned")){
		var intCount = getFieldByID("sel_FlightNoAssigned").length ;
		 for (var i = 0; i < intCount; i++){
		 	strFlightNumbers+=getFieldByID("sel_FlightNoAssigned").options[i].value+",";
		 }
	}	

	setField("hdnFlightNumbers", strFlightNumbers);
	
	document.forms[0].submit();
	setPageEdited(false);

}

function resetSitaAddress() {
	setPageEdited(false);
	var strMode = getText("hdnMode");
	if (strMode == "ADD") {
		clearSitaAddress();
		getFieldByID("txtFormSitaAddress").focus();
		getFieldByID("chkStatus").checked = "true";
		getFieldByID("allowPnlAdl").checked = true;
		getFieldByID("allowPalCal").checked = false;
		
		document.getElementById("radRoute").checked = false;
		document.getElementById("radFlightNum").checked = false;
		
		refreshSelectionBox("#sel_FlightNo", initialSelFlightNums);
		refreshSelectionBox("#sel_FlightNoAssigned", null);
		refreshSelectionBox("#selSegment", null);
		
		disableRouteSelections(true);
		disableFlightNumSelections(true);
	}

	if (strMode == "EDIT") {
		// alert(arrData[strGridRow][4]);
		if (strGridRow < arrData.length
				&& arrData[strGridRow][2] == getText("txtFormSitaAddress")) {
			getFieldByID("txtLocation").focus();
			setField("txtFormSitaAddress", arrData[strGridRow][2]);
			setField("txtLocation", arrData[strGridRow][5]);
			if (arrData[strGridRow][6] == "Active"
					|| arrData[strGridRow][6] == "active")
				getFieldByID("chkStatus").checked = true;
			else
				getFieldByID("chkStatus").checked = false;

			var optAirportValue = arrData[strGridRow][4];
			for (selSize = 0; selSize < document.forms[0].selFormAirport.length; selSize = selSize + 1) {
				if (document.forms[0].selFormAirport.options[selSize].value == optAirportValue) {
					document.forms[0].selFormAirport.options[selSize].selected = true;
				}
			}
			setField("hdnVersion", arrData[strGridRow][7]);
			setField("txtEmail", arrData[strGridRow][8]);
			getFieldByID("allowPnlAdl").checked = arrData[strGridRow][10];
			getFieldByID("allowPalCal").checked = arrData[strGridRow][11];
			
			document.getElementById("radRoute").checked = false;
			document.getElementById("radFlightNum").checked = false;
			fillSegmentSelBox(arrData[strGridRow][12]);
			fillFlightNumSelBoxes(initialSelFlightNums, arrData[strGridRow][13]);
			disableRouteSelections(true);
			disableFlightNumSelections(true);

		} else {
			// Record not found in grid
			clearSitaAddress();
			enableInputControls(true);
			Disable("btnEdit", true);
			Disable("btnDelete", true);
			// getFieldByID("btnAdd").focus();
			buttonSetFocus("btnAdd");
		}

	}
	objOnFocus();
}

function updateSitaAddress() {
	if (!validateSitaAddress())
		return;
	document.forms[0].submit();
}

// on Add Button click
function addClick() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setPageEdited(false);
		// Disable("selFormAirport",false);
		clearSitaAddress();
		enableInputControls(false);
		setField("hdnSitaId", 0);
		setField("hdnMode", "ADD");
		getFieldByID("txtFormSitaAddress").focus();
		getFieldByID("chkStatus").checked = "true";
		getFieldByID("allowPnlAdl").checked = true;
		getFieldByID("allowPalCal").checked = false;
		Disable("btnEdit", true);
		Disable("btnDelete", true);
		
		document.getElementById("radRoute").checked = false;
		document.getElementById("radFlightNum").checked = false;
		
		refreshSelectionBox("#sel_FlightNo", initialSelFlightNums);
		refreshSelectionBox("#sel_FlightNoAssigned", null);
		refreshSelectionBox("#selSegment", null);
		
		disableRouteSelections(true);
		disableFlightNumSelections(true);

		// top[2].ShowProgress();
	}
	objOnFocus();
}

// on Edit Button CLick
function editClick() {
	if (strGridRow == -1) {
		showCommonError("Error", "Please select row to edit!");
		enableInputControls(true);
		return;
	}
	setPageEdited(false);
	setField("hdnMode", "EDIT");
	enableInputControls(false);
	Disable("selFormAirport", true);
	// Disable("txtFormSitaAddress", true);
	Disable("btnDelete", "true");
	handleRadButtons();
	getFieldByID("txtLocation").focus();
	objOnFocus();
}

// on Delete Button click
function deleteClick() {

	if (strGridRow == -1) {
		showCommonError("Error", "Please select row to delete!");
	} else {
		var confirmStr = confirm(deleteConf)
		if (!confirmStr)
			return;
		setField("hdnRecNo", (top[0].intLastRec));
		enableInputControls(true);
		setField("selFormAirport", strRowData[0]);
		setField("hdnMode", "DELETE");
		setTabValues(screenId, valueSeperator, "intLastRec", 1); // Delete
		document.forms[0].submit();

		enableInputControls(false);
	}
}

function CCPress() {
	var strCC = getText("txtTerritoryId");
	var strLen = strCC.length;
	var blnVal = isAlphaNumeric(strCC);
	if (!blnVal) {
		setField("txtTerritoryId", strCC.substr(0, strLen - 1));
		getFieldByID("txtTerritoryId").focus();
	}
}

function CDPress() {
	var strCD = getText("txtDesc");
	var strLen = strCD.length;
	var blnVal = isAlphaNumeric(strCD);
	if (!blnVal) {
		setField("txtDesc", strCD.substr(0, strLen - 1));
		getFieldByID("txtDesc").focus();
	}
}

function Save(msg) {
	top[2].objMsg.MessageText = msg;
	top[2].objMsg.MessageType = "Confirmation";
	top[2].ShowPageMessage();
}

function Delete() {
	top[2].objMsg.MessageText = "Selected record will get deleted.";
	top[2].objMsg.MessageType = "Warning";
	top[2].ShowPageMessage();
}

function myRecFunction(intRecNo, intErrMsg) {
	top[2].HidePageMessage();
	if (intErrMsg != "") {
		top[2].objMsg.MessageText = intErrMsg;
		top[2].objMsg.MessageType = "Error";
		top[2].ShowPageMessage();
	}
}

// To Handle Paging
function gridNavigations(intRecNo, intErrMsg) {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setField("hdnMode", "PAGING");
		objOnFocus();
		// top[0].intLastRec=intRecNo;
		if (intRecNo <= 0)
			intRecNo = 1;
		setTabValues(screenId, valueSeperator, "intLastRec", intRecNo);
		// setField("hdnRecNo",(top[0].intLastRec));
		setField("hdnRecNo", getTabValues(screenId, valueSeperator,
				"intLastRec"));
		if (intErrMsg != "") {
			top[2].objMsg.MessageText = "<li> " + intErrMsg;
			top[2].objMsg.MessageType = "Error";
			top[2].ShowPageMessage();
		} else {
			document.forms[0].submit();
			top[2].ShowProgress();
		}
	}
}

function objOnFocus() {
	top[2].HidePageMessage();
}

function pageOnChange() {
	top[1].objTMenu.tabPageEdited(screenId, true);

}
function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}

function valAddress(objTextBox) {
	setPageEdited(true);
	objOnFocus();
	customValidate(objTextBox, /[\s,]/g);
}
function valLocation(objTextBox) {
	setPageEdited(true);
	objOnFocus();
	customValidate(objTextBox, /[,]/g);
}

function validateEmail() {
	setPageEdited(true);
	objOnFocus();
	var tempVar;
	var mailaddrs = trim(getText("txtEmail"));

	if (mailaddrs.indexOf(",") != -1) {
		var mailarray = mailaddrs.split(",");
		if (mailarray.length > 1) {
			return false;
		}
	} else {
		tempVar = checkEmail(trim(getText("txtEmail")));

		if (!tempVar) {
			setField("txtEmail", "");
		}
		return tempVar;
	}
}

function enableDisableAddressFields(){
	if(getValue("selDeliveryMethod") == "SITATEX"){
		Disable("txtEmail", true);
	} else {
		Disable("txtEmail", false);
	}
}

function addToList() {
	var isContained = false;
	top[2].HidePageMessage();
	var dept = getValue("selFormAirport");	
	var arr = getValue("selArrival");

	if (dept == "") {
		showCommonError("Error", depatureRequired);
		getFieldByID("selFormAirport").focus();

	} else if (arr == "") {
		showCommonError("Error", arrivalRequired);
		getFieldByID("selArrival").focus();

	} else if (dept == arr) {
		showCommonError("Error", depatureArriavlSame);
		getFieldByID("selArrival").focus();

	} else if (!validateOriginAirportSelSegment(dept)) {
		showCommonError("Error", depatureShouldSame);
		getFieldByID("selSegment").focus();
	} else {
		var str = "";
		str += dept;

		str += "/" + arr;

		var control = document.getElementById("selSegment");
		for ( var r = 0; r < control.length; r++) {
			if (control.options[r].text == str) {
				isContained = true;
				showERRMessage(OnDExists);
				break;
			}
		}
		if (isContained == false) {
			control.options[control.length] = new Option(str, str);
			clearStations();
		}
	}
}

function removeFromList() {
	var control = document.getElementById("selSegment");

	var selIndex = control.selectedIndex;
	if (selIndex != -1) {
		for (i = control.length - 1; i >= 0; i--) {
			if (control.options[i].selected) {
				control.options[i] = null;
			}
		}
		if (control.length > 0) {
			control.selectedIndex = selIndex == 0 ? 0 : selIndex - 1;
			clearStations();
		}
	}

}

function clearStations() {
	//getFieldByID("selFormAirport").value = '';
	getFieldByID("selArrival").value = '';
}

function radFlightNumChecked() {
	document.getElementById("radRoute").checked = false;
	disableFlightNumSelections(false);
	disableRouteSelections(true);
	clearSelectedSegmentList();
}

function radRouteChecked() {
	document.getElementById("radFlightNum").checked = false;
	disableRouteSelections(false);
	disableFlightNumSelections(true);
	refreshSelectionBox("#sel_FlightNo", initialSelFlightNums);
	refreshSelectionBox("#sel_FlightNoAssigned", null);
}

function disableRouteSelections(disable){
	Disable("selArrival", disable);
	Disable("selSegment", disable);
	Disable("add_Segment", disable);
	Disable("delete_Segment", disable);
}

function disableFlightNumSelections(disable){
	Disable("sel_FlightNo", disable);
	Disable("sel_FlightNoAssigned", disable);
	Disable("add_FlightNo", disable);
	Disable("del_FlightNo", disable);
}

function clearFlightNumbers(){
	//getFieldByID("sel_FlightNo").value = '';
	//getFieldByID("sel_FlightNoAssigned").value = '';
	//getFieldByID("sel_FlightNo").value = flightNumberList;
}

function clearSelectedSegmentList(){
	getFieldByID("selSegment").innerHTML = '';
}

function moveElementsFromOneSelBoxToAnother(fromID, toID, elements){
	if (elements != undefined && elements != ""){
		var elementsArray = elements.split(',');
		for (var i=0;i<elementsArray.length;i++){
			var str = elementsArray[i];
			var index = $(""+fromID+" > option:contains("+str+")").index();
			if (index != null){
				$(fromID).pairMaster();
				$(""+fromID+" option[value="+str+"]").prop("selected", true);
			    $(fromID).addSelected(toID);
			}
		}
	}
}

function addElementsToSelectionBox(id, elements){
	if (elements != undefined && elements != ""){
		var elementsArray = elements.split(',');
		for (var i=0;i<elementsArray.length;i++){
			var str = elementsArray[i];
			var opt = document.createElement('option');
		    opt.value = str;
		    opt.text = str;
		    $(id).append(opt);
		}
	}
}

function refreshSelectionBox(id, options){
	$(id).empty();
	if (options != null){
		for (var i=0;i<options.length;i++){
			var opt = document.createElement('option');
		    opt.value = options[i].value;
		    opt.text = options[i].text;
		    $(id).append(opt);
		}
	}
}

function fillFlightNumSelBoxes(initialSelFlightNums, alreadySelectedItems){
	refreshSelectionBox("#sel_FlightNo", initialSelFlightNums);
	refreshSelectionBox("#sel_FlightNoAssigned", null);
	moveElementsFromOneSelBoxToAnother("#sel_FlightNo", "#sel_FlightNoAssigned", alreadySelectedItems);
}

function fillSegmentSelBox(alreadySelectedItems){
	refreshSelectionBox("#selSegment", null);
	addElementsToSelectionBox("#selSegment", alreadySelectedItems);
}

function handleRadButtons(){
	if (document.getElementById('sel_FlightNoAssigned').options.length > 0){
		document.getElementById("radFlightNum").checked = true;
		document.getElementById("radRoute").checked = false;
		disableRouteSelections(true);
		disableFlightNumSelections(false);
	} else if (document.getElementById('selSegment').options.length > 0){
		document.getElementById("radFlightNum").checked = false;
		document.getElementById("radRoute").checked = true;
		disableRouteSelections(false);
		disableFlightNumSelections(true);
	} else {
		document.getElementById("radFlightNum").checked = false;
		document.getElementById("radRoute").checked = false;
		disableRouteSelections(true);
		disableFlightNumSelections(true);
	}
}

function validateOriginAirportSelSegment(selectedAirport){
	var valid = true;
	if (document.getElementById('selSegment').options.length > 0){
		var firstSelectedOnd = document.getElementById('selSegment').options[0].text;
		var firstSelectedOrigin = firstSelectedOnd.substr(0, 3);
		if (firstSelectedOrigin != selectedAirport){
			valid = false;
		}
	}
	return valid;
}

function validateOnd(){
	var validOnd = true;
	for (var i=0;i<document.getElementById('selSegment').options.length;i++){
		validOnd = false;
		for (var j=0;j<document.getElementById('sel_Ond_Data').options.length;j++){
			if (document.getElementById('selSegment').options[i].text == document.getElementById('sel_Ond_Data').options[j].text){
				validOnd = true;
				break;
			}
		}
	}
	return validOnd;
}

function validateSelectedFlightNumbersWithOrigin(){
	var validFlight = false;
	var values = $('#sel_FlightNo').val();
	var departureAiport = getValue("selFormAirport");
	var flightNumsWithOrigin = document.getElementById('sel_FlightNo_Origin_Initial');
	if (values != null){
		for (var i=0;i<values.length;i++){
			validFlight = false;
			for (var j=0;j<flightNumsWithOrigin.options.length;j++){
				if (values[i] == flightNumsWithOrigin.options[j].text){
					var originAirportsStr = flightNumsWithOrigin.options[j].value.split('|')[1];
					var originAirportsArray = originAirportsStr.split(',');
					for (var k=0;k<originAirportsArray.length;k++){
						if (originAirportsArray[k] == departureAiport){
							validFlight = true;
						}
					}
				}
			}
		}
	}
	return validFlight;
}

function fromAirportOnChange(){
	top[1].objTMenu.tabPageEdited(screenId, true);
	refreshSelectionBox("#sel_FlightNo", initialSelFlightNums);
	refreshSelectionBox("#sel_FlightNoAssigned", null);
}
