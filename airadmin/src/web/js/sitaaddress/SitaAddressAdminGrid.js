var screenId = "SC_ADMN_021";
var valueSeperator = "~";
if (getTabValues(screenId, valueSeperator, "intLastRec") == "") {
	setTabValues(screenId, valueSeperator, "intLastRec", 1);
}

var objCol1 = new DGColumn();
objCol1.columnType = "label";
objCol1.width = "14%";
objCol1.arrayIndex = 2;
objCol1.headerText = "SITA Address";
objCol1.itemAlign = "left";
objCol1.sort = "true";

var objCol5 = new DGColumn();
objCol5.columnType = "label";
objCol5.width = "15%";
objCol5.arrayIndex = 3;
objCol5.headerText = "Carrier Code";
objCol5.itemAlign = "left"
objCol5.sort = "true";

var objCol2 = new DGColumn();
objCol2.columnType = "label";
objCol2.width = "17%";
objCol2.arrayIndex = 4;
objCol2.headerText = "Airport Code";
objCol2.itemAlign = "left"
objCol2.sort = "true";

var objCol3 = new DGColumn();
objCol3.columnType = "label";
objCol3.width = "30%";
objCol3.arrayIndex = 5;
objCol3.headerText = "Location Description";
objCol3.itemAlign = "left"

var objCol4 = new DGColumn();
objCol4.columnType = "label";
objCol4.width = "15%";
objCol4.arrayIndex = 6;
objCol4.headerText = "Status";
objCol4.itemAlign = "Left"

// ---------------- Grid
var objDG = new DataGrid("spnSitaAddress");
objDG.addColumn(objCol1);
objDG.addColumn(objCol5);
objDG.addColumn(objCol2);
objDG.addColumn(objCol3);
objDG.addColumn(objCol4);

objDG.width = "99%";
objDG.height = "125px";
objDG.headerBold = false;
objDG.rowSelect = true;
objDG.arrGridData = arrData;
objDG.seqNo = true;
objDG.seqStartNo = getTabValues(screenId, valueSeperator, "intLastRec"); // Grid
objDG.pgnumRecTotal = totalRecords; // remove as per return record size
objDG.paging = true
objDG.pgnumRecPage = 20;
objDG.pgonClick = "gridNavigations";
objDG.rowClick = "RowClick";
objDG.displayGrid();
objDG.shortCutKeyFunction = "Body_onKeyDown";
