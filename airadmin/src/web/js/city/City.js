

var cityScreenId = "SC_ADMN_029";
var cityCountryMapJson = null;
var blnLCCEnable = true;

jQuery(document).ready(function(){  //the page is ready
	
			blnLCCEnable = (lccEnableStatus == 'true') ? true : false;
			
			$("#divSearch").decoratePanel("Search City");
			$("#divResultsPanel").decoratePanel("City");
			$("#divDispCity").decoratePanel("Add/Modify City");			
			$('#btnAdd').decorateButton();
			$('#btnEdit').decorateButton();
			$('#btnDelete').decorateButton();
			$('#btnClose').decorateButton();
			$('#btnReset').decorateButton();
			$('#btnSave').decorateButton();
			$('#btnSearch').decorateButton();
			$('#btnCityFD').decorateButton();
			disableCityButton();
			enableSearch();	
			populateCitySel(cityCountryMapJson, cityArr);
			
			var grdArray = new Array();	
			jQuery("#listCity").jqGrid({ 
				url:'showCity!searchCity.action',
				datatype: "json",
				postData: {
					selCountry: function() { return $("#selCountry").val(); },
					selCity: function() { 
						if ($("#selCity").val() == null){
							return "-1";
						} else {
							return $("#selCity").val();
						}
					},
					selStatus: function() { return $("#selStatus").val(); }
				},
				jsonReader : {
					  root: "rows", 
					  page: "page",
					  total: "total",
					  records: "records",
					  repeatitems: false,					 
					  id: "0"				  
					},													
				colNames:['&nbsp;','CityId' ,'Code','Name', 'Country','Status', 'citiesForDisplay', 'version','createdBy','createdDate','modifiedBy','modifiedDate', 'LCCPublishStatus'], 
				colModel:[ 	{name:'Id', width:20, jsonmap:'id'},   
				           	{name:'cityId',index:'cityId', align:"center",hidden:true, width:220, jsonmap:'city.cityId'}, 
						 	{name:'cityCode',index:'cityCode', width:220, jsonmap:'city.cityCode'}, 
						   	{name:'cityName' ,index:'cityName' ,width:225, align:"center", jsonmap:'city.cityName'},
						   	{name:'cityCountry',index:'cityCountry' ,width:90, align:"center",jsonmap:'city.countryCode'},
						   	{name:'status',index:'status' ,width:90, align:"center", jsonmap:'city.status'},
						   	{name:'cityForDisplay', width:225, align:"center",hidden:true},
						   	{name:'version', index:'version' ,width:225, align:"center",hidden:true,  jsonmap:'city.version'},						   							   	
						   	{name:'createdBy', index:'createdBy',width:225, align:"center",hidden:true,  jsonmap:'city.createdBy'},
						   	{name:'createdDate', index:'createdDate',width:225, align:"center",hidden:true,  jsonmap:'city.createdDate'},
						   	{name:'modifiedBy', index:'modifiedBy',width:225, align:"center",hidden:true,  jsonmap:'city.modifiedBy'},
						   	{name:'modifiedDate', index:'modifiedDate',width:225, align:"center",hidden:true,  jsonmap:'city.modifiedDate'},
						   	{name:'lccPublishStatus', index:'lccPublishStatus',width:225, align:"center",hidden:true,  jsonmap:'city.lccPublishStatus'}
						 ], 
				imgpath: '../../themes/default/images/jquery', 
				multiselect:false,
			    onSelectRow: function(rowid){
								fillForm(rowid);
								disableControls(true);
								enableSearch();			
								enableGridButtons();
								$("#rowNo").val(rowid);
								setCityForDisplayList();

				},		
				pager: jQuery('#listCitypager'),
				rowNum:20,						
				viewrecords: true,
				height:210,
				width:930,
				loadui:'block',
				  }).navGrid("#listCitypager",{refresh: true, edit: false, add: false, del: false, search: true});
			

		    
			// pre-submit callback 
			function showRequest(formData, jqForm, options) { 
			    top[2].ShowProgress();
			    return true; 
			} 			
			
			function showResponse(responseText, statusText)  {
				top[2].HideProgress();
//				clearForm();
				var response = JSON.parse(responseText);
				showCommonError(response.msgType, response.succesMsg);
			    if(response.msgType != "Error") {
			    	$('#frmCity').clearForm();
			    	jQuery("#listCity").trigger("reloadGrid");
			    	$("#btnAdd").attr('disabled', false); 
			    	$("#btnSave").disableButton();
			    	disableForm(true);
			    	disableCityButton();
					clearForm();
					if (JSON.parse(response.strCityCountryHtml) != null){
						cityCountryMapJson = JSON.parse(response.strCityCountryHtml).cityCountryCodes;
					}
					populateCitySel(cityCountryMapJson, cityArr);
			    }
			} 	
			
			function showError(responseText, statusText) { 
				    showCommonError("Error", sysError);
				    return true; 
			} 

			function showDelete(formData, jqForm, options) {				
				return confirm(deleteRecoredCfrm); 
			} 
			
			function disableForm(cond){
				$("#cityCode").attr('disabled',cond);
				$("#cityName").attr('disabled',cond);
				$("#cityCountry").attr('disabled',cond);
				$("#status").attr('disabled',cond);
				$("#btnSave").attr('disabled',cond); 	
			}
			
			
			$("#selCountry").on('change',function(){
				populateCitySel(cityCountryMapJson, cityArr);
			});
			
			function populateCitySel(cityCountryMapJson, cityArr){
				if ($("#selCountry").val() == "-1"){
					var cityCountryOptions = getAllCitiesStr();
					$("#selCity").html(cityCountryOptions);
				} else {
					if (cityCountryMapJson == null){
						var value = $("#selCountry").val();
				    	var cityCountryOptions = "'<option>   </option>'";
				    	for (var i=0;i<cityArr.length;i++){
				    		var countryCode = cityArr[i][2];
				    		if (countryCode === value){
				    			cityCountryOptions = cityCountryOptions + "'<option>" + cityArr[i][0] + "</option>'";
				    		}
				    	}
				    	$("#selCity").html(cityCountryOptions);
					} else {
						var cityCountryOptions = "'<option>   </option>'";
						var value = $("#selCountry").val();
						for (var i=0;i<cityCountryMapJson.length;i++){
							var countryCode = cityCountryMapJson[i][1];
							if (countryCode === value){
			    				cityCountryOptions = cityCountryOptions + "'<option>" + cityCountryMapJson[i][0] + "</option>'";
			    			}
						}
						$("#selCity").html(cityCountryOptions);
					}
				}
			}
			
			function getAllCitiesStr(){
				var cityCountryOptions = "'<option>   </option>'";
				
				if (cityCountryMapJson == null) {
			    	for (var i=0;i<cityArr.length;i++){
			    		cityCountryOptions = cityCountryOptions + "'<option>" + cityArr[i][0] + "</option>'";
			    	}
				} else {
					for (var i=0;i<cityCountryMapJson.length;i++){
						cityCountryOptions = cityCountryOptions + "'<option>" + cityCountryMapJson[i][0] + "</option>'";
					}
				}
				
				return cityCountryOptions;
			}


			$('#btnSearch').click(function() {
				 reloadGridForFirstPage();
				 clearForm();
			});
			
			$('#btnSave').click(function() {
				if(validateForm()){
					disableControls(false);
					$("#cityForDisplay").val(buildCityForDisplayStr());
					 var data = {};
					 data['cityDTO.cityId'] = $('#cityId').val();
					 data['cityDTO.cityCode'] =$('#cityCode').val();
					 data['cityDTO.cityName'] =$('#cityName').val();
					 data['cityDTO.status'] = $('#status').val();
					 data['cityDTO.countryCode'] =$('#cityCountry').val();	
					 if(blnLCCEnable){
						 data['cityDTO.lccPublishStatus'] = $('#selLccPublishStatus').val();
					 } else {
						 data['cityDTO.lccPublishStatus'] = "SKP";
					 }
					 data['cityForDisplay'] = buildCityForDisplayStr();
					 data['hdnMode']= $('#hdnMode').val();
			         $.ajax({
                         url : 'showCity.action',
                         beforeSend : top[2].ShowProgress(),
                         data : data,
                         type : "POST",
                         dataType : "json",
                         complete : function(resp) {
                        	 showResponse(resp.responseText, null );
                         }
                     });
				}

						
			});
			 
			$('#btnAdd').click(function() {
				disableControls(false);
				$('#frmCity').clearForm();		
				$("#version").val('-1');
				$("#rowNo").val('');
				$('#btnEdit').disableButton();
				$('#btnDelete').disableButton();
				$("#btnCityFD").disableButton();
				$("#btnSave").enableButton();
				setField("hdnMode","ADD");
				setField("hdnModel","ADD");
				$("#status").val('ACT');
			}); 	
			
			$('#btnClose').click(function() {
				closeCity();
			}); 	


			
			$('#btnEdit').click(function() {
				disableControls(false);	
				$("#btnSave").enableButton();
				$("#btnCityFD").disableButton();
				$("#cityCode").attr('disabled',true);
				$('#selLccPublishStatus').attr("disabled", false);
				setField("hdnMode","EDIT");
			}); 
			
			$('#btnReset').click(function() {
				if($('#rowNo').val() == '') {
					$('#frmCity').resetForm();
					clearForm();
				}else {
					rowid = $("#rowNo").val();
					mode = $("#hdnMode").val();
					fillForm($("#rowNo").val());
					$("#rowNo").val(rowid);
					$("#hdnMode").val(mode);
				}
					
			});
			$('#btnDelete').click(function() {
				if(confirm(deleteRecoredCfrm)){
					disableControls(false);
					$('#hdnMode').val("DELETE");
	
					if($('#cityId').val() != undefined && $('#cityId').val() != "" ){
						 var data = {};
						 data['cityId'] = $('#cityId').val();
						 data['hdnMode']= $('#hdnMode').val();
				         $.ajax({
	                         url : 'showCity!deleteCity.action',
	                         beforeSend : top[2].ShowProgress(),
	                         data : data,
	                         type : "POST",
	                         dataType : "json",
	                         complete : function(resp) {
	                        	 showResponse(resp.responseText, null );
	                         }
	                     });
					}else{
						showCommonError("Error",selectRecord);
					}
					disableForm(true);
					return false;	
				}
			}); 
			
			function fillForm(rowid) {
				setField("hdnMode","");
				clearForm();
				jQuery("#listCity").GridToForm(rowid, '#frmCity');
				
				if(jQuery("#listCity").getCell(rowid,'status') == 'ACT'){
					$("#status").val("ACT");								
				}else {																
					$("#status").val("INA");								
				}	
				
				if(blnLCCEnable) {
					if(jQuery("#listCity").getCell(rowid,'lccPublishStatus') == 'PUB'){
						$("#selLccPublishStatus").val("PUB");	
						$('#selLccPublishStatus').attr("disabled", false);
					}else if (jQuery("#listCity").getCell(rowid,'lccPublishStatus') == 'TBP'){																
						$("#selLccPublishStatus").val("TBP");								
					}else if (jQuery("#listCity").getCell(rowid,'lccPublishStatus') == 'TBR'){																
						$("#selLccPublishStatus").val("TBR");								
					}else if (jQuery("#listCity").getCell(rowid,'lccPublishStatus') == 'SKP'){																
						$("#selLccPublishStatus").val("SKP");								
					}
				}
			}
			
			
			function  disableControls(cond){
				$("input").each(function(){ 
				      $(this).attr('disabled', cond); 
				});	
				$("textarea").each(function(){ 
				      $(this).attr('disabled', cond); 
				});	
				$("select").each(function(){ 
				      $(this).attr('disabled', cond); 
				});
				$("#btnClose").enableButton();
				$('input[type=checkbox]').each(function () {
					 $(this).attr('disabled', false);
				});
				enableSearch();
			}
			
			function enableSearch(){
				$('#selCountry').attr("disabled", false); 
				$('#selCity').attr("disabled", false);
				$('#selStatus').attr("disabled", false);
				$('#btnSearch').enableButton();
				$("#btnCityFD").enableButton();
				
				//popup controls
				$('#txtCityForDisplayOL').attr("disabled", false);
				$('#language').attr("disabled", false);
				$('#translation').attr("disabled", false);
				$('#add').enableButton();
				$('#remove').enableButton();
				$('#btnUpdate').enableButton();
				$('#btnPopupClose').enableButton();
			}
			
			function disableCityButton(){
				$("#btnEdit").disableButton();
				$("#btnDelete").disableButton();
				$("#btnReset").disableButton();
				$("#btnSave").disableButton();

			}

			function enableGridButtons(){
				$("#btnAdd").enableButton();
				$("#btnEdit").enableButton();
				$("#btnDelete").enableButton();
				$("#btnReset").enableButton();
			}
			
			function validateForm(){
				
				if(trim($("#cityCode").val()) == ""){
					getFieldByID("cityCode").focus();
					showCommonError("Error", cityCodeRequire);
					return false;
				} else if(trim($('#cityName').val()) == ""){
					getFieldByID("cityName").focus();
					showCommonError("Error", cityNameRequire);
					return false;
				} else if($('#cityCountry').val() == null){
					getFieldByID("cityCountry").focus();
					showCommonError("Error", cityCountryRequire);
					return false;
				} else if(trim($("#cityCode").val()).length > 3){
					getFieldByID("cityCode").focus();
					showCommonError("Error", cityCodeLengthInvalid);
					return false;
				} else if(trim($("#cityName").val()).length > 20){
					getFieldByID("cityName").focus();
					showCommonError("Error", cityNameLengthInvalid);
					return false;
				} else if (getValue("selLccPublishStatus") == "-1" && blnLCCEnable) {
					getFieldByID("selLccPublishStatus").focus();
					showCommonError("Error", lccPublishStatusSelectionRequired);
					return false;
				} else if (isAddCity() && 
						isCityCodeAlreadyThere($('#cityCountry').val(), trim($("#cityCode").val()))) {
					getFieldByID("cityCode").focus();
					showCommonError("Error", cityCodeDuplicate);
					return false;
				}
				
				return true;				
			}
			
			disableControls(true);
			$("#btnAdd").enableButton();
			$('#btnSearch').enableButton();
			
			createPopUps();
			PopUpData();
			
			if(!blnLCCEnable){
				setVisible("fntLCCPublish", false);
				setVisible("selLccPublishStatus", false);
			} else {
				setVisible("fntLCCPublish", true);
			}
			
});

		
		function setPageEdited(isEdited) {
			top[1].objTMenu.tabPageEdited(cityScreenId, isEdited);
		}	

	
		function clearForm() {
			$('#frmCity').resetForm();
		}
		
		function closeCity() {
			top[1].objTMenu.tabRemove(cityScreenId);
		}
		
		function PopUpData(){
			var html = '<table width="99%" align="center" border="0" cellpadding="0" cellspacing="0"> <tr><td> <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" ID="Table4"> <tr><td width="10" height="20"><img src="../../images/form_top_left_corner_no_cache.gif"></td> <td height="20" class="FormHeadBackGround"><img src="../../images/bullet_small_no_cache.gif"> <font class="FormHeader"> City Name For Display </font></td> <td width="11" height="20"><img src="../../images/form_top_right_corner_no_cache.gif"></td> </tr><tr><td class="FormBackGround"></td><td class="FormBackGround" valign="top"> <table><tr><td width="663" align="left"><table width="98%" height="105"  border="0" cellpadding="0" cellspacing="0"> <tr align="left" valign="middle" class="fntMedium"> <td width="19%" class="fntMedium"><font>Language</font></td> <td width="33%" align="left" style="padding-bottom:3px; ">';
				html += '<select name="language" id="language"  style="width:150px; "> ';
				html += '<option value="-1"></option>'+languageHTML;				
				html += '</select></td> <td width="13%" rowspan="3" align="center" valign="top"><input name="add" type="button" class="Button" id="add" style="width:50px; " title="Add Item"  onclick="addCityForDisplay();" value="&gt;&gt;" /> <input name="remove" type="button" class="Button" id="remove" style="width:50px;" title="Remove Item" onclick="removeCityForDisplay();" value="&lt;&lt;" /></td> <td width="35%" rowspan="3" align="left" valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0"> <tr> <td><select name="translation" size="5" id="translation" style="width:175px; height:100px; "> </select></td> </tr> <tr> </tr> </table></td> </tr> <tr align="left" valign="middle" class="fntMedium"> <td width="25%" valign="top" class="fntMedium"><font>City Name* </font></td> <td align="left" valign="top"><input name="txtCityForDisplayOL" type="text" id="txtCityForDisplayOL" size="32" maxlength="125" /></td> </tr> '; 
				html +=	'</table></td> </tr><tr><td align="top"><span id="spnSta"></span></td></tr> <tr><td align="right"><input type="button" id="btnUpdate" value= "Save" class="Button" onClick="saveCityForDisplay();"> &nbsp;<input type="button" id="btnPopupClose" value= "Close" class="Button" onClick="return POSdisable(true);"></td></tr></table> </td><td class="FormBackGround"></td></tr><tr> <td height="12"><img src="../../images/form_bottom_left_corner_no_cache.gif"></td> <td height="12" class="FormBackGround"><img src="../../images/spacer_no_cache.gif" width="100%" height="1"></td> <td height="12"><img src="../../images/form_bottom_right_corner_no_cache.gif"></td> </tr></table> </td></tr></table>';

			DivWrite("spnPopupData",html);
		}

		function POSenable(){			
			if(!listForDisplayEnabled)
				return;
			if(getFieldByID("cityCode").value=="" || getFieldByID("cityCode").value==null){
					alert("No city is selected, please select city and try again");
					return;
				}
			
			if(getValue('hdnModel') == 'ADD'){
				var cityList = getFieldByID("translation");
				cityList.options.length = 0;
				setField("txtCityForDisplayOL", "");
				setField("language","-1");
			}
			
			//setField("hdnModel", "SAVELISTFORDISP");
			//setCityForDisplayList();
			showPopUp(1);
		}

		function POSdisable(bFromClose){			
			if(!listForDisplayEnabled)
				return;
			if(bFromClose){
				//setField("hdnModel","");
				setField("hdnMode","");
			}
			 hidePopUp(1);
		}


		function addCityForDisplay(){			
			if(!listForDisplayEnabled)
				return;
			if(getValue('txtCityForDisplayOL').length ==0){
				return false;
			}
			var selLang = getValue("language");
			if(selLang == '-1')
				return false;
			if(findValueInList(selLang, "translation") == true){
				alert("City name for the selected language already exist");
				return false;
			}

			var selLangName = getCurSelectedLabel("language");
			var cityDesc = getValue("txtCityForDisplayOL");
			addToList("translation", selLangName+"=>"+cityDesc, selLang);	
			setField("txtCityForDisplayOL", "");
			setField("language","-1");

		}
		function removeCityForDisplay(){			
			if(!listForDisplayEnabled)
				return;
			var cityList = getFieldByID("translation");
			var selected = cityList.selectedIndex;
			if(selected != -1){
				var langCode = cityList.options[selected].value;
				var label = cityList.options[selected].text.split("=>");
				setField("language", langCode);
				setField("txtCityForDisplayOL", label[1]);
				cityList.remove(selected);
			}
		}
		function saveCityForDisplay(){			
			if(!listForDisplayEnabled)
				return;
			if(getValue('txtCityForDisplayOL').length>0 && getValue("language")!='-1'){
				if(confirm("The current edited language is not added to the list, do you want to continue?")==false)
					return false;
			}
			var strMode = getText("hdnMode");
			var cityList = getFieldByID("translation");
			var size = cityList.length;
			var langCode;
			var cityName;
			var cityTitle;
			var label;
			var str="";
			var size;
			var i=0;
			var j=0;
			var newData = new Array();
			var count = 0;
			//////////
			var cityTranslations = jQuery("#listCity").getCell($("#rowNo").val(),'cityForDisplay');
			var arrCityTrl = new Array();
			if(cityTranslations != null && cityTranslations != "")
				arrCityTrl = cityTranslations.split("~");
			var cityTrl;
			grdArray = new Array();
			for(var i =0;i<arrCityTrl.length-1;i++){
				grdArray[i] = new Array(); 
				cityTrl = arrCityTrl[i].split(",");
				grdArray[i][0] = cityTrl[0];
				grdArray[i][1] = cityTrl[1];
				grdArray[i][2] = cityTrl[2];
				grdArray[i][3] = cityTrl[3];
				grdArray[i][4] = cityTrl[4];
			//	grdArray[i][5] = cityTrl[5];
			}
			////////////
			for(i=0;i<cityList.length;i++){
				langCode = cityList.options[i].value;
				label = cityList.options[i].text.split("=>");
				cityName = label[1];
				if (label.length > 2)
					cityTitle = label[2];
				else
					cityTitle = getValue("cityCode");;
				size = grdArray.length;
				var found = false;
				for(j=0;j<size;j++){
					if(grdArray[j][0] == langCode){
						newData[count++] = new Array(langCode, cityName, grdArray[j][2], grdArray[j][3],grdArray[j][4], cityTitle);
						found = true;
						break;
					}
				}
				if(!found){
					newData[count++] = new Array(langCode, cityName, "-1", "-1", "-1", cityTitle);//langCode,cityName,id,ver, citytitle
					//arrData[strGridRow][16][size] = new Array(langCode,cityName,'-1','-1');//langCode,cityName,id,ver
				}
			}
			var langCode2;
			var size1 = grdArray.length;
			var size2 = newData.length;
			for(i=0;i<size1;i++){//original list
				found = false;
				for(j=0;j<size2;j++){//new data
					langCode = grdArray[i][0];
					langCode2 = newData[j][0];
					if(langCode == langCode2){//the lang code exist in the new data
						found = true;
					}
				}
				if(!found){
					newData[newData.length] = new Array( grdArray[i][0],"null", grdArray[i][2],grdArray[i][3],grdArray[i][4], cityTitle);
				}
			}
			grdArray = newData;
			if(validateCityForDisplay() == false){
				return false;
			}
			$('#btnSave').click();
			return POSdisable(false);
		}
		
		function setCityForDisplayList(){			
			if(!listForDisplayEnabled)
				return;

			if(jQuery("#listCity").getCell($("#rowNo").val())>=0 ){
				setField("txtCityForDisplayOL", "");
				setField("language","-1");
			
				var cityList = getFieldByID("translation");				
				cityList.options.length = 0;
				var langCode ;
				var cityName ;
				var langName;
				var cityTitle;
				var i=0;
				
				var cityTranslations = jQuery("#listCity").getCell($("#rowNo").val(),'cityForDisplay');
				var arrCityTrl = new Array();
				if(cityTranslations != null && cityTranslations != "")
					arrCityTrl = cityTranslations.split("~");
				var cityTrl;
				grdArray = new Array();
				for(var i =0;i<arrCityTrl.length-1;i++){
					grdArray[i] = new Array(); 
					cityTrl = arrCityTrl[i].split(",");
					grdArray[i][0] = cityTrl[0];
					grdArray[i][1] = cityTrl[1].replace("\^",",");
					grdArray[i][2] = cityTrl[2];
					grdArray[i][3] = cityTrl[3];
					grdArray[i][4] = cityTrl[4];
					grdArray[i][5] = cityTrl[5];
				}
				for(i=0;i<grdArray.length;i++){
					langCode = grdArray[i][0];
					cityName = grdArray[i][1];
					cityTitle = grdArray[i][5];
					if(cityName == "null")
						continue;
					langName = getListLabel("language", langCode);
					addToList("translation", langName+"=>"+cityName, langCode);
				}
			}
		}
		function validateCityForDisplay(){
			if(!listForDisplayEnabled)
				return;
			var strMode = getText("hdnMode");
			var size2=getFieldByID("language").length;
			var valid = true;
			var size1;
			var i;
			var j;
			var langCode;
			var cityName;
			var found = false;
			if(strMode == "ADD")
				valid = false;
			else{
				size1 = grdArray.length;
				if(size1 != (size2-1))//-1 to ignore the first empty item
					valid = false;
				else{
					for(i=0;i<size1;i++){//original list
						cityName = grdArray[i][1];
						if(cityName == "null"){
							valid = false;
							break;
						}
					}
				}
			}
			if(!valid){
				alert("City name for display is missing for all or some languages, original city name will copied to all languages") ;
				if(strMode != "ADD"){
						var descr = getValue("cityName");
						var title = getValue("cityCode");
						for(i=1;i<size2;i++){//lang  list, start from 1 to ignore the first item (empty)
							found = false;
							langCode = getFieldByID("language").options[i].value;
							for(j=0;j<size1;j++){
								cityName = grdArray[j][1];
								if(cityName == "null"){
									grdArray[j][1] = descr;
									grdArray[j][5] = title;
								}
								if(langCode == grdArray[j][0]){
									found = true;
									continue;
								}
							}
							if(!found)
								grdArray[grdArray.length] = new Array(langCode,descr,"-1","-1","-1",title);
						}
				}
			}
			return true;
			}
		
		
		function buildCityForDisplayStr(){			
			var strMode = getText("hdnMode");
			var cityStr = "";
			
			if(strMode == "ADD"){					
				cityDesc = getUnicode(getValue("cityName"));
				cityTitle = getUnicode(getValue("cityCode"));
				
				var size2=getFieldByID("language").length;
				for(i=1;i<size2;i++){
					langCode = getFieldByID("language").options[i].value;
					if(cityStr.length>0)
						cityStr += "~";
					cityStr=cityStr + langCode+","+cityDesc+",-1,-1,-1"+","+cityTitle;
				}
			}else {
				prepareCityToSave();
				if(grdArray != null){					
					for(var j=0;j<grdArray.length;j++){
						cityStr += grdArray[j][0]+",";
						cityStr += getUnicode(grdArray[j][1])+",";
						cityStr += grdArray[j][2]+",";
						cityStr += grdArray[j][3]+",";
						cityStr += grdArray[j][4]+",";
						cityStr += getUnicode(grdArray[j][5])+",";
						cityStr += "~";	
				   	}
				}	
			}			
			return cityStr;
		}
		
		function prepareCityToSave(){
			var strMode = getText("hdnMode");
			var cityList = getFieldByID("translation");
			var size = cityList.length;
			var langCode;
			var cityName;
			var cityTitle;
			var label;
			var str="";
			var size;
			var i=0;
			var j=0;
			var newData = new Array();
			var count = 0;
			//////////
			var cityTranslations = jQuery("#listCity").getCell($("#rowNo").val(),'cityForDisplay');
			var arrCityTrl = new Array();
			if(cityTranslations != null && cityTranslations != "")
				arrCityTrl = cityTranslations.split("~");
			var cityTrl;
			grdArray = new Array();
			for(var i =0;i<arrCityTrl.length-1;i++){
				grdArray[i] = new Array(); 
				cityTrl = arrCityTrl[i].split(",");
				grdArray[i][0] = cityTrl[0];
				grdArray[i][1] = cityTrl[1];
				grdArray[i][2] = cityTrl[2];
				grdArray[i][3] = cityTrl[3];
				grdArray[i][4] = cityTrl[4];				
			}
			////////////
			for(i=0;i<cityList.length;i++){
				langCode = cityList.options[i].value;
				label = cityList.options[i].text.split("=>");
				cityName = label[1];
				if (label.length > 2 && label[2]!="")
					cityTitle = label[2];
				else
					cityTitle = getValue("cityCode");
				size = grdArray.length;
				var found = false;
				for(j=0;j<size;j++){
					if(grdArray[j][0] == langCode){
						newData[count++] = new Array(langCode, cityName, grdArray[j][2], grdArray[j][3],grdArray[j][4], cityTitle);
						found = true;
						break;
					}
				}
				if(!found){
					newData[count++] = new Array(langCode, cityName,  "-1", "-1", "-1", cityTitle);//langCode,cityName,id,ver, city title
					//arrData[strGridRow][16][size] = new Array(langCode,cityName,'-1','-1');//langCode,cityName,id,ver
				}
			}
			var langCode2;
			var size1 = grdArray.length;
			var size2 = newData.length;
			for(i=0;i<size1;i++){//original list
				found = false;
				for(j=0;j<size2;j++){//new data
					langCode = grdArray[i][0];
					langCode2 = newData[j][0];
					if(langCode == langCode2){//the lang code exist in the new data
						found = true;
					}
				}
				if(!found){
					newData[newData.length] = new Array( grdArray[i][0],getValue("cityName"), grdArray[i][2],grdArray[i][3],grdArray[i][4], getValue("cityCode"));
				}
			}
			grdArray = newData;		
			
		}
		
		function isCityCodeAlreadyThere(countryCode, cityCode){
			var cityCodeAlreadyThere = false;
			
			if (cityCountryMapJson == null){
		    	for (var i=0;i<cityArr.length;i++){
		    		if (cityCode.toLowerCase() == cityArr[i][0].toLowerCase()){
		    			cityCodeAlreadyThere = true;
		    			break;
		    		}
		    	}
			} else {
				for (var i=0;i<cityCountryMapJson.length;i++){
					if (cityCode.toLowerCase() == cityCountryMapJson[i][0].toLowerCase()){
						cityCodeAlreadyThere = true;
						break;
					}
				}
			}
			
			return cityCodeAlreadyThere;
		}
		
		function isAddCity(){
			var isAddCity = false;
			var strMode = getText("hdnMode");
			if (strMode == "ADD"){
				isAddCity = true;
			}
			return isAddCity;
		}
		
		function reloadGridForFirstPage(){
			var page = $("#listCity").getGridParam("page");
			page = 1;
			$("#listCity").setGridParam({
				datatype : 'json',
			    page : page         
			}).trigger('reloadGrid');
		}

	
		
		
