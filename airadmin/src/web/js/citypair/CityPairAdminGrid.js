var screenId = "SC_ADMN_013";
var valueSeperator = "~";
if (getTabValues(screenId, valueSeperator, "intLastRec") == "") {
	setTabValues(screenId, valueSeperator, "intLastRec", 1);
}

var objCol1 = new DGColumn();
objCol1.columnType = "label";
objCol1.width = "11%"; // 19
objCol1.arrayIndex = 1;
// objCol1.toolTip = "Origin" ;
objCol1.headerText = "Origin";
objCol1.itemAlign = "center"; // left

var objCol2 = new DGColumn();
objCol2.columnType = "label";
objCol2.width = "11%"; // 19
objCol2.arrayIndex = 2;
// objCol2.toolTip = "Destination" ;
objCol2.headerText = "Destination";
objCol2.itemAlign = "center";

var objCol3 = new DGColumn();
objCol3.columnType = "label";
objCol3.width = "11%";
objCol3.arrayIndex = 3;
// objCol3.toolTip = "Distance" ;
objCol3.headerText = "Distance <br> KM";
objCol3.itemAlign = "right"

var objCol4 = new DGColumn();
objCol4.columnType = "label";
objCol4.width = "11%";
objCol4.arrayIndex = 4;
// objCol4.toolTip = "Travel Duration" ;
objCol4.headerText = "Travel Duration <br> HH:MM";
objCol4.itemAlign = "right"

var objCol5 = new DGColumn();
objCol5.columnType = "label";
objCol5.width = "11%";
objCol5.arrayIndex = 5;
// objCol5.toolTip = "Duration Tolerance %" ;
objCol5.headerText = "Duration Tolerance <br> %";
objCol5.itemAlign = "right"

var objCol6 = new DGColumn();
objCol6.columnType = "label";
objCol6.width = "11%";
objCol6.arrayIndex = 6;
// objCol6.toolTip = "Status" ;
objCol6.headerText = "Status";
objCol6.itemAlign = "left"

var objCol7 = new DGColumn();
objCol7.columnType = "label";
objCol7.width = "26%";
objCol7.arrayIndex = 20;
objCol7.headerText = "Via Points";
objCol7.itemAlign = "left"

// ---------------- Grid
var objDG = new DataGrid("spnCityPairs");
objDG.addColumn(objCol1);
objDG.addColumn(objCol2);
objDG.addColumn(objCol7);
objDG.addColumn(objCol3);
objDG.addColumn(objCol4);
objDG.addColumn(objCol5);
objDG.addColumn(objCol6);

objDG.width = "99%";
objDG.height = "140px"; // 224 // 210
objDG.seqNoWidth = "8%";
objDG.headerBold = false;
objDG.rowSelect = true;
objDG.arrGridData = cityPairData;
objDG.seqNo = true;
objDG.seqStartNo = getTabValues(screenId, valueSeperator, "intLastRec"); // Grid
objDG.pgnumRecTotal = totalNoOfRecords; // remove as per return record size
objDG.paging = true
objDG.pgnumRecPage = 20;
objDG.pgonClick = "gridNavigations";
objDG.rowClick = "rowClick";
objDG.displayGrid();
objDG.shortCutKeyFunction = "Body_onKeyDown";

function myRecFunction(intRecNo, intErrMsg) {
	if (intErrMsg != "") {
		parent.objMsg.MessageText = "<li> " + intErrMsg;
		parent.objMsg.MessageType = "Error";
		parent.ShowPageMessage();
	}
}
