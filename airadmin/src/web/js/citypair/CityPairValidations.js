//JavaScript Document
//Author -Thushara 

var strRowData;
var strGridRow;
var screenId = "SC_ADMN_013";
var valueSeperator = "~";
var lccConStatusDiv = "divLCCConStatus";

// On window load
function winOnLoad(strMsg, strMsgType) {
	clearControls();
	disableInputControls();
	// Menaka
	if (saveSuccess == 1) {
		alert("Record Successfully saved!");
		saveSuccess = 0;
	}
	// Menaka
	if (strMsg != null && strMsgType != null)
		showCommonError(strMsgType, strMsg);

	if (top[1].objTMenu.focusTab == screenId) {
		getFieldByID("selOriginSearch").focus();
	}

	setPageEdited(false);
	Disable("btnEdit", true);
	Disable("btnDelete", true);
	Disable("btnSave", true);
	Disable("btnReset", true);

	setSearchCriteria();
	setLCCDisplayData();
	if (arrFormData != null && arrFormData.length > 0) {
		setPageEdited(true);
		strGridRow = getTabValues(screenId, valueSeperator, "strGridRow"); // Get
																			// saved
																			// grid
																			// Row
		enableInputControls(false);
		Disable("btnReset", false);
		Disable("btnSave", false);
		setField("selOrigin", arrFormData[0][1]);
		setField("selDestination", arrFormData[0][2]);
		setField("txtDistance", arrFormData[0][3]);
		setField("txtDuration", arrFormData[0][4]);
		setField("txtTolerance", arrFormData[0][5]);
		if (arrFormData[0][6] == "Active")
			getFieldByID("chkStatus").checked = true;
		else
			getFieldByID("chkStatus").checked = false;

		// Via points
		setField("selVia1", arrFormData[0][7]);
		setField("selVia2", arrFormData[0][8]);
		setField("selVia3", arrFormData[0][9]);
		setField("selVia4", arrFormData[0][10]);

		if (isAddMode) {
			setField("hdnMode", "ADD");
			setField("hdnAction", "ADD");
			if (top[1].objTMenu.focusTab == screenId) {
				getFieldByID("selOrigin").focus();
			}
		} else {
			setField("hdnMode", "EDIT");
			setField("hdnAction", "EDIT");
			if (top[1].objTMenu.focusTab == screenId) {
				getFieldByID("txtDistance").focus();
			}
			setField("hdnVersion", prevVersion);
			setField("hdnRouteId", prevRouteId);
			setField("hdnGridRow", prevGridRow);
			strGridRow = prevGridRow;
		}

		if (isOriginFocus) {
			if (top[1].objTMenu.focusTab == screenId) {
				getFieldByID("selOrigin").focus();
			}
		} else {
			if (isDestFocus) {
				if (top[1].objTMenu.focusTab == screenId) {
					getFieldByID("selDestination").focus();
				}
			}
		}

	}

}

function setSearchCriteria() {
	var strSearchTemp = top[1].objTMenu.tabGetValue("SC_ADMN_013");
	if ((strSearchTemp != "") && (strSearchTemp != null)) {
		var strSearch = strSearchTemp.split(",");
		setField("hdnRecNo", strSearch[0]);
		setField("selOriginSearch", strSearch[1]);
		setField("selDestinationSearch", strSearch[2]);
		setField("selVia1Search", strSearch[3]);
		setField("selVia2Search", strSearch[4]);
		setField("selVia3Search", strSearch[5]);
		setField("selVia4Search", strSearch[6]);
	}
}

// on Grid Row click
function rowClick(strRowNo) {
	objOnFocus();
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setPageEdited(false);
		strRowData = objDG.getRowValue(strRowNo);
		strGridRow = strRowNo;
		setField("hdnGridRow", strRowNo);
		setField("selOrigin", cityPairData[strRowNo][7]);
		setField("selDestination", cityPairData[strRowNo][8]);
		setField("selVia1", cityPairData[strRowNo][11]);
		setField("selVia2", cityPairData[strRowNo][12]);
		setField("selVia3", cityPairData[strRowNo][13]);
		setField("selVia4", cityPairData[strRowNo][14]);
		setField("txtDistance", strRowData[3]);
		setField("txtDuration", strRowData[4]);
		setField("txtTolerance", strRowData[5]);
		setField("hdnVersion", cityPairData[strRowNo][9]);
		setField("hdnROuteCode", cityPairData[strRowNo][10]);
		setField("hdnRouteId", cityPairData[strRowNo][10]);
		setField("hdnId", cityPairData[strRowNo][15]);
		setField("hdnVia1Id", cityPairData[strRowNo][16]);
		setField("hdnVia2Id", cityPairData[strRowNo][17]);
		setField("hdnVia3Id", cityPairData[strRowNo][18]);
		setField("hdnVia4Id", cityPairData[strRowNo][19]);
		if(blnLCCConnectivity){
			if(cityPairData[strRowNo][21] == "true"){
				getFieldByID("chkLCCPublishedStatus").checked = true;
			}else {
				getFieldByID("chkLCCPublishedStatus").checked = false;
			}				
		}

		if (strRowData[6] == "Active") {
			getFieldByID("chkStatus").checked = true;
		} else {
			getFieldByID("chkStatus").checked = false;
		}
		disableInputControls();
		Disable("btnEdit", false);
		Disable("btnDelete", false);
		Disable("btnSave", true);
		Disable("btnReset", true);

	}

}

// on Add Button click
function addClick() {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		objOnFocus();
		setPageEdited(false);
		clearControls();
		Disable("btnEdit", true);
		Disable("btnDelete", true);
		enableInputControls();
		setField("hdnMode", "ADD");
		setField("hdnAction", "ADD");
		setField("hdnRouteId", 0);
		setField("hdnId", "");
		setField("hdnVia1Id", "");
		setField("hdnVia2Id", "");
		setField("hdnVia3Id", "");
		setField("hdnVia4Id", "");
		Disable("btnSave", false);
		Disable("btnReset", false);
		getFieldByID("selOrigin").focus();
		getFieldByID("chkStatus").checked = true;
		getFieldByID("chkLCCPublishedStatus").checked = false;
	}
}

// on Edit Button CLick
function editClick() {
	objOnFocus();

	enableInputControls();
	getFieldByID("selOrigin").focus();
	setField("hdnMode", "EDIT");
	setField("hdnAction", "EDIT");
	Disable("btnDelete", true);
	Disable("btnSave", false);
	Disable("btnReset", false);

	if (isValueExist(directRoutUsageArr, getFieldByID("hdnId").value)
			|| isValueExist(routsInUse, getFieldByID("hdnId").value)) {
		Disable("selOrigin", true);
		Disable("selDestination", true);
		Disable("selVia1", true);
		Disable("selVia2", true);
		Disable("selVia3", true);
		Disable("selVia4", true);
	}
}

// on Save Button click
function saveCityPair() {
	objOnFocus();

	enableInputControls();
	if (isEmpty(getText("selOrigin"))) {
		showERRMessage(originRqrd);
		getFieldByID("selOrigin").focus();
	} else {
		if (isEmpty(getText("selDestination"))) {
			showERRMessage(destinationRqrd);
			getFieldByID("selDestination").focus();
		} else {

			// Validates the Via point order
			if (validateViaPointOrder()) {

				// Validates the Uniqueness of the Route Points
				if (validateUniqueRoutePoints()) {

					if (isEmpty(getText("txtDistance"))) {
						showERRMessage(distanceRqrd);
						getFieldByID("txtDistance").focus();
					} else {
						if ((getText("txtDistance") == "0.0")
								|| (getText("txtDistance") <= 0)) {
							showERRMessage(distanceZero);
							getFieldByID("txtDistance").focus();
						} else {
							if (checkDistance()) {
								showERRMessage(distanceFmt);
								getFieldByID("txtDistance").focus();
							} else {
								if (isEmpty(getText("txtDuration"))) {
									showERRMessage(durationRqrd);
									getFieldByID("txtDuration").focus();
								} else {
									if ((getText("txtDuration") == "0:00")
											|| (getText("txtDuration") == "00:00")
											|| (getText("txtDuration") <= 0)) {
										showERRMessage(durationZero);
										getFieldByID("txtDuration").focus();
									} else {
										if (checkDuration()) {
											showERRMessage(durationFmt);
											getFieldByID("txtDuration").focus();
										} else if (checkTolerance()) {
											showERRMessage(toleranceFmt);
											getFieldByID("txtTolerance")
													.focus();
										} else {
											var rowNumber = isRoutCodeDuplicated();
											if (rowNumber != null) {
												showERRMessage(routeExists);
												getFieldByID("selVia1").focus();
											} else {
												setField("hdnRecNo",
														getTabValues(screenId,
																valueSeperator,
																"intLastRec"));
												setField("hdnMode", "SAVE");
												setTabValues(screenId,
														valueSeperator,
														"strGridRow",
														strGridRow); // Save
																		// the
																		// grid
																		// Row
												document.frmCityPair.submit();
												top[2].ShowProgress();
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}

function isRoutCodeDuplicated() {
	if (getFieldByID("hdnAction").value != "EDIT") {
		return null;
	}

	var stSegmentFrom = trim(getFieldByID("selOrigin").value).toUpperCase()
			+ "/";
	var stSegmentTo = trim(getFieldByID("selDestination").value).toUpperCase();
	var stSegmentVia1 = (getFieldByID("selVia1").value != null && getFieldByID("selVia1").value != "") ? trim(
			getFieldByID("selVia1").value).toUpperCase()
			+ "/"
			: "";
	var stSegmentVia2 = (getFieldByID("selVia2").value != null && getFieldByID("selVia2").value != "") ? trim(
			getFieldByID("selVia2").value).toUpperCase()
			+ "/"
			: "";
	var stSegmentVia3 = (getFieldByID("selVia3").value != null && getFieldByID("selVia3").value != "") ? trim(
			getFieldByID("selVia3").value).toUpperCase()
			+ "/"
			: "";
	var stSegmentVia4 = (getFieldByID("selVia4").value != null && getFieldByID("selVia4").value != "") ? trim(
			getFieldByID("selVia4").value).toUpperCase()
			+ "/"
			: "";

	var routCode = trim(stSegmentFrom + stSegmentVia1 + stSegmentVia2
			+ stSegmentVia3 + stSegmentVia4 + stSegmentTo);

	var i;
	for (i in cityPairData) {
		if (routCode == cityPairData[i][10] && i != strGridRow) {
			return i;
		}
	}
	return null;
}

function searchCityPair() {

	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted("SC_ADMN_013"))) {
		setPageEdited(false);
		objOnFocus();

		setField("hdnRecNo", "1");
		setField("hdnMode", "SEARCH");

		var strTempSearch = getText("selOriginSearch") + ","
				+ getText("selDestinationSearch") + ","
				+ getText("selVia1Search") + "," + getText("selVia2Search")
				+ "," + getText("selVia3Search") + ","
				+ getText("selVia4Search");
		setField("hdnSearchCriteria", strTempSearch);

		setTabSearch();
		document.forms[0].target = "_self";
		document.forms[0].action = "showCityPair.action";
		document.forms[0].submit();
		top[2].ShowProgress();

	}
}

function setTabSearch() {
	var setValue = getText("hdnRecNo") + "," + getText("hdnSearchCriteria");
	top[1].objTMenu.tabSetValue("SC_ADMN_013", setValue);
}

// Reset Button Click
function resetClick() {
	objOnFocus();
	var strMode = getText("hdnAction");
	if (strMode == "ADD") {
		clearControls();
		getFieldByID("selOrigin").focus();
		getFieldByID("chkStatus").checked = true;
	}

	if (strMode == "EDIT") {
		if (strGridRow < cityPairData.length
				&& cityPairData[strGridRow][7] == getValue("selOrigin")
				&& cityPairData[strGridRow][8] == getValue("selDestination")) {
			setField("selOrigin", cityPairData[strGridRow][7]);
			setField("selDestination", cityPairData[strGridRow][8]);
			setField("txtDistance", cityPairData[strGridRow][3]);
			setField("txtDuration", cityPairData[strGridRow][4]);
			setField("txtTolerance", cityPairData[strGridRow][5]);
			if (cityPairData[strGridRow][6] == "Active") {
				getFieldByID("chkStatus").checked = true;
			} else {
				getFieldByID("chkStatus").checked = false;
			}

			setField("selVia1", cityPairData[strGridRow][11]);
			setField("selVia2", cityPairData[strGridRow][12]);
			setField("selVia3", cityPairData[strGridRow][13]);
			setField("selVia4", cityPairData[strGridRow][14]);

			setField("hdnVersion", cityPairData[strGridRow][9]);
			setField("hdnROuteCode", cityPairData[strGridRow][10]);
			setField("hdnRouteId", cityPairData[strGridRow][10]);
			setField("hdnId", cityPairData[strGridRow][15]);
			setField("hdnVia1Id", cityPairData[strGridRow][16]);
			setField("hdnVia2Id", cityPairData[strGridRow][17]);
			setField("hdnVia3Id", cityPairData[strGridRow][18]);
			setField("hdnVia4Id", cityPairData[strGridRow][19]);
			if(blnLCCConnectivity){
				if(cityPairData[strGridRow][21] == "true"){
					getFieldByID("chkLCCPublishedStatus").checked = true;
				}else {
					getFieldByID("chkLCCPublishedStatus").checked = false;
				}				
			}
			
			getFieldByID("selOrigin").focus();
		} else {
			// Clear the data entry area
			clearControls();
			disableInputControls();
			buttonSetFocus("btnAdd");
			Disable("btnEdit", true);
			Disable("btnDelete", true);
			Disable("btnSave", true);
			Disable("btnReset", true);
		}
	}
	setPageEdited(false);
}

// To Handle Paging
function gridNavigations(intRecNo, intErrMsg) {
	if (top.loadCheck(top[1].objTMenu.tabGetPageEidted(screenId))) {
		setField("hdnMode", "PAGING");
		objOnFocus();
		if (intRecNo <= 0)
			intRecNo = 1;

		setTabValues(screenId, valueSeperator, "intLastRec", intRecNo);
		setField("hdnRecNo", getTabValues(screenId, valueSeperator,
				"intLastRec"));
		if (intErrMsg != "") {
			top[2].objMsg.MessageText = "<li> " + intErrMsg;
			top[2].objMsg.MessageType = "Error";
			top[2].ShowPageMessage();
		} else {
			document.forms[0].submit();
			top[2].ShowProgress();
		}
	}
}

function clearControls() {
	setField("selOrigin", "");
	setField("selDestination", "");
	setField("selVia1", "");
	setField("selVia2", "");
	setField("selVia3", "");
	setField("selVia4", "");
	setField("txtDuration", "");
	setField("txtTolerance", "");
	setField("txtDistance", "");
	setField("hdnVersion", "");
	setField("chkStatus", "");
}

function disableInputControls() {
	Disable("selOrigin", true);
	Disable("selDestination", true);
	Disable("selVia1", true);
	Disable("selVia2", true);
	Disable("selVia3", true);
	Disable("selVia4", true);
	Disable("txtDuration", true);
	Disable("txtTolerance", true);
	Disable("txtDistance", true);
	Disable("chkStatus", true);
}

function enableInputControls() {
	Disable("selOrigin", false);
	Disable("selDestination", false);
	Disable("selVia1", false);
	Disable("selVia2", false);
	Disable("selVia3", false);
	Disable("selVia4", false);
	Disable("txtDuration", false);
	Disable("txtTolerance", false);
	Disable("txtDistance", false);
	Disable("chkStatus", false);
}

function clickChange() {
	objOnFocus();
	setPageEdited(true);
}

function setPageEdited(isEdited) {
	top[1].objTMenu.tabPageEdited(screenId, isEdited);
}

function objOnFocus() {
	top[2].HidePageMessage();
}

function deleteClick() {
	objOnFocus();
	var confirmMsg = deleteConf;

	if (isValueExist(routsInUse, getFieldByID("hdnId").value)) {
		showERRMessage(segmentRouteInUse);
		return;
	}

	if (isValueExist(directRoutUsageArr, getFieldByID("hdnId").value)) {
		confirmMsg = deleteConfSegRoute;
	}

	var strConfirm = confirm(confirmMsg);
	if (strConfirm == true) {
		setField("hdnMode", "DELETE");
		setTabValues(screenId, valueSeperator, "intLastRec", 1); // Delete
		setField("hdnRecNo", getTabValues(screenId, valueSeperator,
				"intLastRec"));
		document.frmCityPair.submit();
		top[2].ShowProgress();
	}
}

function validateString(field) {
	objOnFocus();
	var strStringVal = getText(field);
	var strLen = strStringVal.length;
	var blnVal = isAlphaNumeric(strStringVal);
	if (!blnVal) {
		setField(field, strStringVal.substr(0, strLen - 1));
		getFieldByID(field).focus();
	}
}

function checkDistance() {
	objOnFocus();
	var strCC = getText("txtDistance");
	var blnVal = currencyValidate(trim(strCC), '6', '2');
	if (!blnVal) {
		return true;
	}
}

function checkTolerance() {
	objOnFocus();
	clickChange();
	var strCD = getText("txtTolerance");
	var blnVal = currencyValidate(trim(strCD), '3', '2');
	if (!blnVal) {
		return true;
	}
}

function checkDuration() {
	objOnFocus();
	clickChange();
	var strCR = getText("txtDuration");
	var blnVal = IsValidTime(trim(strCR));
	if (!blnVal) {
		return true;
	}
}

function DistancePress(objTextBox) {
	objOnFocus();
	setPageEdited("true");
	var strStr = getText("txtDistance");
	var strLen = strStr.length;
	var blnVal = currencyValidate(strStr, 6, 2);
	var wholeNumber;
	if (!blnVal) {
		if (strStr.indexOf(".") != -1) {
			wholeNumber = strStr.substr(0, strStr.indexOf("."));
			if (wholeNumber.length > 15) {
				setField("txtDistance", strStr
						.substr(0, wholeNumber.length - 1));
			} else {
				setField("txtDistance", strStr.substr(0, strLen - 1));
			}
		} else {
			setField("txtDistance", strStr.substr(0, strLen - 1));
		}
		getFieldByID("txtDistance").focus();
	}
}

function DurationPress(objTextBox) {
	objOnFocus();
	setPageEdited(true);
	var strCR = getText("txtDuration");
	var strLen = strCR.length;
	var blnEmpty = isEmpty(strCR);
	if (blnEmpty) {
		setField("txtDuration", strCR.substr(0, strLen - 1));
		getFieldByID("txtDuration").focus();
	}
}

function TolerancePress(objTextBox) {
	objOnFocus();
	setPageEdited("true");
	var strStr = getText("txtTolerance");
	var strLen = strStr.length;
	var blnVal = currencyValidate(strStr, 2, 2);
	var wholeNumber;
	if (!blnVal) {
		if (strStr.indexOf(".") != -1) {
			wholeNumber = strStr.substr(0, strStr.indexOf("."));
			if (wholeNumber.length > 15) {
				setField("txtTolerance", strStr.substr(0,
						wholeNumber.length - 1));
			} else {
				setField("txtTolerance", strStr.substr(0, strLen - 1));
			}
		} else {
			setField("txtTolerance", strStr.substr(0, strLen - 1));
		}
		getFieldByID("txtTolerance").focus();
	}
}

/*
 * Validats the Via Points order
 */
function validateViaPointOrder() {
	var arrViaPoints = new Array();
	arrViaPoints[0] = new Array("selVia1", "Via1");
	arrViaPoints[1] = new Array("selVia2", "Via2");
	arrViaPoints[2] = new Array("selVia3", "Via3");
	arrViaPoints[3] = new Array("selVia4", "Via4");

	for (i = arrViaPoints.length - 1; i >= 0; i--) {
		if (!isEmpty(getText(arrViaPoints[i][0]))) {

			for (y = 0; y < i; y++) {
				if (isEmpty(getText(arrViaPoints[y][0]))) {
					showERRMessage(errViaOrder + " " + arrViaPoints[y][1]);
					getFieldByID(arrViaPoints[y][0]).focus();
					return false;
				}
			}
		}

	}
	return true;
}

/*
 * Validates the uniqueness of the Route points
 */
function validateUniqueRoutePoints() {
	var arrRoutePoints = new Array();
	arrRoutePoints[0] = new Array("selOrigin", "Origin");
	arrRoutePoints[1] = new Array("selDestination", "Destination");
	arrRoutePoints[2] = new Array("selVia1", "Via1");
	arrRoutePoints[3] = new Array("selVia2", "Via2");
	arrRoutePoints[4] = new Array("selVia3", "Via3");
	arrRoutePoints[5] = new Array("selVia4", "Via4");

	if (getText(arrRoutePoints[1][0]) == (getText(arrRoutePoints[0][0]))) {
		showERRMessage(errInvalidRoute);
		getFieldByID(arrRoutePoints[1][0]).focus();
		return false;
	}

	for (i = 2; i < arrRoutePoints.length; i++) {

		var strViaPointTxt = getText(arrRoutePoints[i][0]);
		var strOrignTxt = getText(arrRoutePoints[0][0]);
		var strDestText = getText(arrRoutePoints[1][0]);

		if (!isEmpty(strViaPointTxt)
				&& (strViaPointTxt == strOrignTxt || strViaPointTxt == strDestText)) {
			showERRMessage(errInvalidRoute);
			getFieldByID(arrRoutePoints[i][0]).focus();
			return false;
		}
	}

	return true;
}
 
function setLCCDisplayData(){
	setDisplay(lccConStatusDiv,blnLCCConnectivity);	

}
