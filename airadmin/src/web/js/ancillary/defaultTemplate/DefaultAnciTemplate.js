var assignedOndsBeforeUpdate;
var selectedRowId=-1;

jQuery(document)
		.ready(
				function() {

					var grdArray = new Array();

					$("#divSearchPanel").decoratePanel("Search Templates");
					$("#divDispDefAnciTemp").decoratePanel("Add/Modify Template");
					$("#divResultsPanel").decoratePanel("Ancillary Templates");
					$('#btnAdd').decorateButton();
					$('#btnEdit').decorateButton();
					$('#btnDelete').decorateButton();
					$('#btnClose').decorateButton();
					$('#btnReset').decorateButton();
					$('#btnSave').decorateButton();
					$('#btnSearch').decorateButton();

					enableGridButtons(false);
					enbleFormButtons(false);

					jQuery("#listAncilTemp")
							.jqGrid(
									{
										url : 'showDefaultAnciTemplate.action',
										datatype : "json",
										jsonReader : {
											root : "rows",
											page : "page",
											total : "total",
											records : "records",
											repeatitems : false,
											id : "0"
										},
										colNames : [ '&nbsp;',
												'Default Template Id',
												'Anci Type',
												'Anci Template Code', 'Status',
												'Anci Template ID', 'Version',
												'createdBy', 'createdDate',
												'ac model',
												'defAnciTemplateRoutes' ],
										colModel : [
												{
													name : 'id',
													width : 40,
													jsonmap : 'id'
												},
												{
													name : 'templateId',
													index : 'templateId',
													width : 125,
													hidden : true,
													jsonmap : 'defAnciTemplate.defaultAnciTemplateId'														
												},
												{
													name : 'templateType',
													width : 175,
													jsonmap : 'defAnciTemplate.ancillaryType'
												},
												{
													name : 'templateCode',
													width : 275,
													jsonmap : 'defAnciTemplate.anciTemplateCode'
												},
												{
													name : 'status',
													width : 150,
													align : "center",
													jsonmap : 'defAnciTemplate.status'
												},
												{
													name : 'anciTemplateId',
													width : 275,
													align : "center",
													hidden : true,
													jsonmap : 'defAnciTemplate.anciTemplateId'
												},
												{
													name : 'version',
													width : 225,
													align : "center",
													hidden : true,
													jsonmap : 'defAnciTemplate.version'
												},
												{
													name : 'createdBy',
													width : 225,
													align : "center",
													hidden : true,
													jsonmap : 'defAnciTemplate.createdBy'
												},
												{
													name : 'createdDate',
													width : 225,
													align : "center",
													hidden : true,
													jsonmap : 'defAnciTemplate.createdDate'
												},
												{
													name : 'acModel',
													width : 225,
													align : "center",
													hidden : true,
													jsonmap : 'defAnciTemplate.airCraftModelnumber'
												},
												{
													name : 'defAnciTemplateRoutes',
													width : 225,
													align : "center",
													hidden : true
												}

										],
										imgpath : '../../themes/default/images/jquery',
										multiselect : false,
										pager : jQuery('#temppager'),
										rowNum : 20,
										viewrecords : true,
										height : 175,
										width : 925,
										loadui : 'block',
										onSelectRow : function(rowid) {
											if (isPageEdited()) {
												selectedRowId = rowid;
												$('#frmDefAnciTempl')
														.clearForm();
												hideMessage();
												setPageEdited(false);
												$("#rowNo").val(rowid);
												fillForm();
												disableControls(true);
												enableSearch();
												enableGridButtons(true);
												enbleFormButtons(false);
											}
										}
									}).navGrid("#temppager", {
								refresh : true,
								edit : false,
								add : false,
								del : false,
								search : false
							});

					$('#btnSave').click(function() {
						buttonSaveOnClick();
					});

					$('#btnSearch').click(function() {
						buttonSearchOnClick();
					});

					$('#btnAdd').click(function() {
						buttonAddOnClick();
					});

					$('#btnEdit').click(function() {
						buttonEditOnClick();
					});
					$('#btnReset').click(function() {
						buttonResetOnClick();
					});
					$('#btnDelete').click(function() {
						buttonDeleteOnClick();
					});

					$('#sleectedAnciType').click(function() {
						anciTypeOnSelect();
					});

					$('#selectedAirCraftModel').click(function() {
						airCraftModelOnSelect();
					});

					function buttonSaveOnClick() {
						hideMessage();
						disableControls(false);
						createONDString();

						var formData = getSubmitData();
						$.ajax(
								{
								url : 'showDefaultAnciTemplate!saveDefaultAnciTemplate.action',
								type : 'post',
								data : formData,
								beforeSend : validateSaveRequet,
								success : showResponse											
							})
						return false;
					}
					
					function getSubmitData(){
						var data = {};
						
						data['defAnciTemplTO.defaultAnciTemplateId'] = $('#defaultAnciTemplateId').val();					
						data['defAnciTemplTO.ancillaryType'] =$('#sleectedAnciType').val();
						data['defAnciTemplTO.anciTemplateId'] =$('#selectedanciTemplate').val();
						data['defAnciTemplTO.status'] =$('#selectedTemplStatus').val();
						data['defAnciTemplTO.ondString'] = $('#ondString').val();
						data['defAnciTemplTO.createdBy'] = $('#createdBy').val();
						data['defAnciTemplTO.createdDate'] =$('#createdBy').val();
						data['defAnciTemplTO.version'] =$('#version').val();
						data['defAnciTemplTO.airCraftModelnumber'] = $('#sleectedAnciType').val() === "SEAT" ? $('#selectedAirCraftModel').val() : null; 						
						
						for(var i=0; i<assignedOndsBeforeUpdate.length; i++){
							data['defAnciTemplTO.assignedOnds['+i+'].defaultAnciTemplateRouteId'] =assignedOndsBeforeUpdate[i]['defaultAnciTemplateRouteId'];
							data['defAnciTemplTO.assignedOnds['+i+'].ondCode']=assignedOndsBeforeUpdate[i]['ondCode']
							data['defAnciTemplTO.assignedOnds['+i+'].version']=assignedOndsBeforeUpdate[i]['version']
						}
						return data;
					}

					function buttonSearchOnClick() {
						hideMessage();
						var strUrl = "showDefaultAnciTemplate.action?searchCriteria.anciType="
								+ $("#selTemplate").val()
								+ "&searchCriteria.status="
								+ $("#selStatus").val()
								+ "&searchCriteria.origin="
								+ $("#selRouteOrigin").val()
								+ "&searchCriteria.depature="
								+ $("#selRouteDestination").val();

						jQuery("#listAncilTemp").setGridParam({
							url : strUrl
						});
						jQuery("#listAncilTemp").trigger("reloadGrid");
						enbleFormButtons(false);
						enableGridButtons(false);
						clearForm();
						$("#version").val('-1');

						grdArray = new Array();
						disableControls(true);
						enableSearch();
						$("#btnAdd").attr('disabled', false);
					}

					function clearForm(){
						$('#frmDefAnciTempl').clearForm();
						$('#selSelected').html("");
						$('#selectedTemplStatus').val('ACT');
						assignedOndsBeforeUpdate =new Array();
					}
					
					function fillForm() {
						if(selectedRowId !=-1){
							rowid = selectedRowId;
						}else{
							return;
						}
						var ondStr = jQuery("#listAncilTemp").getCell(rowid,
								'defAnciTemplateRoutes');
						var arrONDS = ondStr.split("^");
						var defAnciRout;
						var grdArray = new Array();
						var ondSelectStr = "";

						for (var i = 0; i < arrONDS.length - 1; i++) {

							grdArray[i] = new Array();
							defAnciRout = arrONDS[i].split(",");
							grdArray[i]["defaultAnciTemplateRouteId"] = defAnciRout[0];
							grdArray[i]["ondCode"] = defAnciRout[1];
							grdArray[i]["version"] = defAnciRout[2];

							ondSelectStr = ondSelectStr + "<option value='"
									+ defAnciRout[1] + "'>" + defAnciRout[1]
									+ "</option>"
						}
						assignedOndsBeforeUpdate = grdArray;
						$('#assignedOnds').val(grdArray);
						$('#selSelected').html(ondSelectStr);
						$('#sleectedAnciType').val(
								jQuery("#listAncilTemp").getCell(rowid,
										'templateType'));
						$('#selectedAirCraftModel').val(
								jQuery("#listAncilTemp").getCell(rowid,
										'acModel'));

						// ---------------------------------------------------
						var selectedAnciType = jQuery("#listAncilTemp")
								.getCell(rowid, 'templateType');

						if (selectedAnciType == "MEAL") {

							$('#selectedanciTemplate').html(mealTemplSelect);

						} else if (selectedAnciType == "BAGGAGE") {

							$('#selectedanciTemplate').html(baggageTemplates);

						}else if (selectedAnciType == 'SEAT') {

							$('#selectedanciTemplate').html(
									getSeatTemplateHtmlStr());
						}
						// ----------------------------------------------------

						$('#selectedanciTemplate').val(
								jQuery("#listAncilTemp").getCell(rowid,
										'anciTemplateId'));
						$('#selectedTemplStatus').val(
								jQuery("#listAncilTemp").getCell(rowid,
										'status'));
						$('#version').val(
								jQuery("#listAncilTemp").getCell(rowid,
										'version'));
						$('#createdBy').val(
								jQuery("#listAncilTemp").getCell(rowid,
										'createdBy'));
						$('#createdDate').val(
								jQuery("#listAncilTemp").getCell(rowid,
										'createdDate'));
						$('#defaultAnciTemplateId').val(
								jQuery("#listAncilTemp").getCell(rowid,
										'templateId'));
					}

					function validateSaveRequet(formData, jqForm, options) {
						if ($('#sleectedAnciType').val()==null ||  trim($('#sleectedAnciType').val()) == "") {
							showCommonError("Error", typeReq);
							return false;
						}
						if ($('#selectedanciTemplate').val() ==null  || trim($('#selectedanciTemplate').val()) == "") {
							showCommonError("Error", templateReq);
							return false;
						}
						if (trim($('#sleectedAnciType').val()) == "SEAT" && ($('#selectedAirCraftModel').val()== null  ||$('#selectedAirCraftModel').val()=="")){
							showCommonError("Error", acModelReq);
							return false;
						}
						if($('#ondString').val() == null || $('#ondString').val().length < 7 ){
							showCommonError("Error", routesReq);
							return false;
						}

						top[2].ShowProgress();
						return true;
					}

					function showResponse(responseText, statusText) {
						top[2].HideProgress();
						showCommonError(responseText['msgType'],
								responseText['succesMsg']);
						if (responseText['msgType'] != "Error") {
							alert(responseText['succesMsg']);
							setPageEdited(false);
							clearForm();
							jQuery("#listAncilTemp").trigger("reloadGrid");
							enableGridButtons(false);
							enbleFormButtons(false);
						}
					}

					function buttonAddOnClick() {
						hideMessage();
						disableControls(false);
						enbleFormButtons(true);
						clearForm();
						selectedRowId =-1;
					}

					function buttonEditOnClick() {
						disableControls(false);
						enbleFormButtons(true);
						$('#sleectedAnciType').attr('disabled', true);						
					}

					function buttonResetOnClick() {
						hideMessage();
						clearForm();
						if(selectedRowId !=-1){
							fillForm();
						}
					}

					function buttonDeleteOnClick() {

						hideMessage();
						disableControls(false);

						var formData = getSubmitData();

						$.ajax(
								{
								url : 'showDefaultAnciTemplate!deleteDefaultAnciTemplate.action',
								type : 'post',
								data : formData,
								beforeSend : showDelete,
								success : showResponse
							})
						return false;
					}

					function anciTypeOnSelect() {
						$('#selectedAirCraftModel').attr('disabled', true);
						$('#selectedanciTemplate').attr('disabled', false);

						var selectedAnciType = $('#sleectedAnciType').val();
						if (selectedAnciType == "MEAL") {
							$('#selectedAirCraftModel').val("");
							$('#selectedanciTemplate').html(mealTemplSelect);

						} else if (selectedAnciType == "BAGGAGE") {
							$('#selectedAirCraftModel').val("");
							$('#selectedanciTemplate').html(baggageTemplates);

						} else if (selectedAnciType == 'SEAT') {
							//$('#selectedanciTemplate').attr('disabled', true);
							$('#selectedAirCraftModel').attr('disabled', false);
						}
					}

					function airCraftModelOnSelect() {
						var seatTemplatesHtmlStr = getSeatTemplateHtmlStr();
						$('#selectedanciTemplate').html(seatTemplatesHtmlStr);
						$('#selectedanciTemplate').attr('disabled', false);
					}

					function getSeatTemplateHtmlStr() {
						var ModelSelected = $('#selectedAirCraftModel').val();
						var arrSeatTemlStr = seatTemplates.split("|");

						var seatTemplatesHtml = "<option value=''></option>";
						for (var i = 0; i < arrSeatTemlStr.length - 1; i++) {
							var temlate = arrSeatTemlStr[i].split("&");
							if (temlate[0] == ModelSelected) {
								seatTemplatesHtml = seatTemplatesHtml
										+ "<option value='" + temlate[1] + "'>"
										+ temlate[2] + "</option>";
							}
						}
						return seatTemplatesHtml
					}

					function createONDString() {
						var ondStr = "";
						$('#selSelected').find('option').each(function() {
							ondStr = $(this).val() + "," + ondStr;
						});
						$("#ondString").val(ondStr);
					}

					function disableControls(cond) {
						$("input").each(function() {
							$(this).attr('disabled', cond);
						});
						$("textarea").each(function() {
							$(this).attr('disabled', cond);
						});
						$("select").each(function() {
							$(this).attr('disabled', cond);
						});
						$("#btnClose").attr('disabled', false);
						$("#btnAdd").attr('disabled', false);
						
						if($('#sleectedAnciType').val() != "SEAT"){
							$('#selectedAirCraftModel').attr('disabled', true);
						}
					}

					function enableGridButtons(cond) {
						if (cond) {
							$("#btnEdit").enableButton();
							$("#btnDelete").enableButton();
						} else {
							$("#btnEdit").disableButton();
							$("#btnDelete").disableButton();
						}

					}

					function enbleFormButtons(cond) {
						if (cond) {
							$("#btnReset").enableButton();
							$("#btnSave").enableButton();
						} else {
							$("#btnReset").disableButton();
							$("#btnSave").disableButton();
						}
					}

					function enableSearch() {
						$("#btnSearch").attr('disabled', false);
						$("#selTemplate").attr('disabled', false);
						$("#selStatus").attr('disabled', false);
						$("#selRouteDestination").attr('disabled', false);
						$("#selRouteOrigin").attr('disabled', false);
					}

					function setPageEdited(isEdited) {
						top[1].objTMenu.tabPageEdited(screenId, isEdited);
						hideMessage();
					}

					function isPageEdited() {
						return top.loadCheck(top[1].objTMenu
								.tabGetPageEidted(screenId));
					}
					function hideMessage() {
						top[2].HidePageMessage();
					}

					function disableForm(cond) {

					}

					function showDelete(formData, jqForm, options) {
						return confirm(deleteRecoredCfrm);
					}

				});

function btnAddSegment_click() {
	var isContained = false;
	top[2].HidePageMessage();
	var dept = getText("selDepature");
	var arr = getText("selArrival");
	var via1 = getText("selVia1");
	var via2 = getText("selVia2");
	var via3 = getText("selVia3");
	var via4 = getText("selVia4");

	var deptVal = getValue("selDepature");
	var arrVal = getValue("selArrival");
	var via1Val = getValue("selVia1");
	var via2Val = getValue("selVia2");
	var via3Val = getValue("selVia3");
	var via4Val = getValue("selVia4");

	if (dept == "") {
		showERRMessage(depatureRqrd);
		getFieldByID("selDepature").focus();

	} else if (dept != "" && deptVal == "INA") {
		showERRMessage(departureInactive);
		getFieldByID("selDepature").focus();

	} else if (arr == "") {
		showERRMessage(arrivalRqrd);
		getFieldByID("selArrival").focus();

	} else if (arr != "" && arrVal == "INA") {
		showERRMessage(arrivalInactive);
		getFieldByID("selArrival").focus();

	} else if (dept == arr && dept != "All") {
		showERRMessage(depatureArriavlSame);
		getFieldByID("selArrival").focus();

	} else if (dept == arr && dept == "All" && via1 == "") {
		showERRMessage(viaPointRqrd);
		getFieldByID("selVia1").focus();

	} else if (arr == via1 || arr == via2 || arr == via3 || arr == via4) {
		showERRMessage(arriavlViaSame);
		getFieldByID("selArrival").focus();

	} else if (dept == via1 || dept == via2 || dept == via3 || dept == via4) {
		showERRMessage(depatureViaSame);
		getFieldByID("selDepature").focus();

	} else if ((via2 != "" || via3 != "" || via4 != "") && via1 == "") {
		showERRMessage(vianotinSequence);
		getFieldByID("selVia1").focus();

	} else if ((via3 != "" || via4 != "") && via2 == "") {
		showERRMessage(vianotinSequence);
		getFieldByID("selVia1").focus();

	} else if ((via4 != "") && via3 == "") {
		showERRMessage(vianotinSequence);
		getFieldByID("selVia1").focus();

	} else if ((via1 != "" && via2 != "" && via3 != "")
			&& (via1 == via2 || via1 == via3 || via1 == via4)) {
		showERRMessage(viaEqual);
		getFieldByID("selVia1").focus();

	} else if ((via1 != "" && via2 != "" && via3 != "")
			&& (via2 == via3 || via2 == via4)) {
		showERRMessage(viaEqual);
		getFieldByID("selVia2").focus();

	} else if ((via1 != "" && via2 != "" && via3 != "") && (via3 == via4)) {
		showERRMessage(viaEqual);
		getFieldByID("selVia4").focus();

	} else if ((via1 != "" && via2 != "") && via1 == via2) {
		showERRMessage(viaEqual);
		getFieldByID("selVia1").focus();

	} else if (via1 != "" && via1Val == "INA") {
		showERRMessage(via1 + " " + viaInactive);
		getFieldByID("selVia1").focus();
	} else if (via2 != "" && via2Val == "INA") {
		showERRMessage(via2 + " " + viaInactive);
		getFieldByID("selVia2").focus();
	} else if (via3 != "" && via3Val == "INA") {
		showERRMessage(via3 + " " + viaInactive);
		getFieldByID("selVia3").focus();
	} else if (via4 != "" && via4Val == "INA") {
		showERRMessage(via4 + " " + viaInactive);
		getFieldByID("selVia4").focus();
	} else {
		var str = "";
		str += dept;

		if (via1 != "") {
			str += "/" + via1;
		}
		if (via2 != "") {
			str += "/" + via2;
		}
		if (via3 != "") {
			str += "/" + via3;
		}
		if (via4 != "") {
			str += "/" + via4;
		}
		str += "/" + arr;

		var control = document.getElementById("selSelected");
		for (var r = 0; r < control.length; r++) {
			if (control.options[r].text == str) {
				isContained = true;
				showERRMessage(ondRequired);
				break;
			}
		}
		if (isContained == false) {
			control.options[control.length] = new Option(str, str);
		}
	}
}

function btnRemoveSegment_click() {
	var control = document.getElementById("selSelected");

	var selIndex = control.selectedIndex;
	if (selIndex != -1) {
		for (i = control.length - 1; i >= 0; i--) {
			if (control.options[i].selected) {
				control.options[i] = null;
			}
		}
		if (control.length > 0) {
			control.selectedIndex = selIndex == 0 ? 0 : selIndex - 1;
		}
	}

}

function clearAll() {
	var control = document.getElementById("selSelected");
	control.options.length = 0;
	for (var t = 0; t <= (control.length); t++) {
		control.options[t] = null;
	}

}

function getAll() {
	var control = document.getElementById("selSelected");
	var values = "";
	for (var t = 0; t < (control.length); t++) {
		var str = control.options[t].text;
		var strReplace = control.options[t].text;
		if (str.indexOf("All") != -1) {
			strReplace = str.replace(/All/g, "***");
		}
		values += strReplace + ",";
	}
	return values;
}